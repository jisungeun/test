#include <iostream>

#include <QMessageBox>
#include <QDir>
#include <QDirIterator>

//Entities
#include <AppEvent.h>
#include <MenuEvent.h>
#include <Tool.h>

//presenters
#include <WorkBenchPresenter.h>
#include <ScenePresenter.h>

//Plugins
#include <ImageDataReader.h>
#include <MaskDataReader.h>
#include <MaskDataWriter.h>
#include <SceneManager.h>
#include <ScreenShotWidget.h>
#include <AiControlPanel.h>

//controllers
#include <ToolController.h>
#include <WorkBenchController.h>
#include <ImageDataController.h>

//From project manager
#include <ProjectDataReader.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::InterSeg::AppUI {
	using namespace TC::Framework;
	struct MainWindow::Impl {
		Ui::MainWindow* ui{ nullptr };

		Plugins::SceneManager* sceneManager{ nullptr };

		QString tcf_path;
		QString link_full;
		QString link_sender;
		QMap<QString, IParameter::Pointer> share_param;

		bool from_graph{ false };
		QString graph_hyper;//hypercube name for the graph
		QString graph_pg;//playground path (in history) for the graph
		int force_step{ -1 };

		QVariantMap appProperties;

		TC::ScreenShotWidget::Pointer screenshotWidget{ nullptr };
		Plugins::AiControlPanel::Pointer aiControls{ nullptr };
	};

	MainWindow::MainWindow(QWidget* parent)
		: IMainWindowTA("Mask Editor", "Mask Editor", parent)
		, d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "Mask Editor";
	}

	MainWindow::~MainWindow() {
		
	}

	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
		auto isSingleRun = true;
		auto isBatchRun = false;
		return std::make_tuple(isSingleRun, isBatchRun);
	}

	auto MainWindow::GetCurTCF() const -> QString {
		return d->tcf_path;
	}

	void MainWindow::resizeEvent(QResizeEvent* event) {
		Q_UNUSED(event)
	}

	auto MainWindow::Execute(const QVariantMap& params)->bool {
		if (params.isEmpty()) {
			return true;
		}
		if (false == params.contains("ExecutionType")) {
			return false;
		}
		if (params["ExecutionType"] == "LinkRun") {
			return this->ExecuteLinkRun(params);
		}
		if(params["ExecutionType"].toString()=="OpenTCF"){
			return this->ExecuteOpenTcf(params);
		}
		return true;
	}

	auto MainWindow::ExecuteLinkRun(const QVariantMap& params)->bool {
		auto sender = params["SenderName"].toString();
		auto full = params["SenderFull"].toString();
		d->link_sender = sender;
		d->link_full = full;

		Reset();

		auto ds = d->ui->GeneralPanel->GetDS();
		ds->standAlone = false;
		ds->linked_app = sender;
		auto idDS = d->ui->IDManagerPanel->GetDS();
		idDS->curUser = "System";

		if (sender.contains("Basic")) {
			auto tcfPath = params["TcfPath"].toString();
			auto maskPath = params["MaskPath"].toString();

			d->from_graph = false;

			OnLoadTcf(tcfPath);
			OnLoadFromLink(maskPath);
		}
		else if (sender.contains("Report")) {
			auto pgPath = params["PGPath"].toString();
			auto cubeName = params["CubeName"].toString();
			auto fileName = params["FileName"].toString();

			//Load playground & parse image & mask's full path
			const std::shared_ptr<ProjectDataReader> reader(new ProjectDataReader);

			PlaygroundInfo::Pointer pginfo = std::make_shared<PlaygroundInfo>();
			reader->ReadPlaygroundData(pgPath, pginfo);

			QString tcfPath;
			QString HyperName;
			for (auto hypercube : pginfo->GetHyperCubeList()) {
				auto isFound = false;
				for (auto cube : hypercube->GetCubeList()) {
					if (cube->GetName() == cubeName) {
						for (auto tcf : cube->GetTCFDirList()) {
							auto tcfchop = tcf->GetName().chopped(4);
							if (tcfchop == fileName) {
								tcfPath = tcf->GetPath();
								isFound = true;
								break;
							}
						}
						break;
					}
				}
				if (isFound) {
					HyperName = hypercube->GetName();
					break;
				}
			}

			if (tcfPath.isEmpty()) {
				return false;
			}

			QFileInfo pfile(pgPath);
			auto timesplit = pfile.fileName().chopped(5).split("_");
			auto timeStamp = timesplit[timesplit.count() - 1];

			QDir working_dir(pgPath);
			working_dir.cdUp();
			working_dir.cd(HyperName);
			working_dir.cd(timeStamp);
			working_dir.cd(cubeName);
			auto maskPath = working_dir.path() + "/" + fileName + ".msk";

			if (!QFile::exists(maskPath)) {
				QDirIterator it(working_dir.path(), QStringList() << "*.msk", QDir::Files);
				while (it.hasNext()) {
					auto mskFile = it.next();
					if (mskFile.contains(fileName)) {
						maskPath = mskFile;
						auto maskBack = maskPath.chopped(4) + "_bak.msk";
						QFile::copy(maskPath, maskBack);
						CopyPath(maskPath.chopped(4), maskBack.chopped(4));
						break;
					}
				}
			}

			d->graph_pg = pgPath;
			d->graph_hyper = HyperName;

			d->from_graph = true;

			OnLoadTcf(tcfPath);
			OnLoadFromLink(maskPath);

			auto ws = Entity::WorkingSet::GetInstance();

			//if time stamp exist
			auto time_step = params["TimeStep"].toString();
			d->force_step = 0;
			if (!time_step.isEmpty()) {
				auto time_idx = time_step.toInt();
				if (ws->GetCurTimeStep() != time_idx) {
					OnTimeChange(time_idx);
				}
				d->ui->PlayerPanel->setEnabled(false);
				d->force_step = time_idx;
			}
		}
		return true;
	}	
	auto MainWindow::CopyPath(QString src, QString dst) -> void {
		QDir dir(src);
		if (!dir.exists())
			return;

		foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
			QString dst_path = dst + QDir::separator() + dd;
			dir.mkpath(dst_path);
			CopyPath(src + QDir::separator() + dd, dst_path);
		}

		foreach(QString f, dir.entryList(QDir::Files)) {
			QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
		}
    }

	auto MainWindow::ExecuteOpenTcf(const QVariantMap& params)->bool {
		Reset();
		auto ds = d->ui->GeneralPanel->GetDS();
		ds->standAlone = true;
		ds->linked_app = QString();
		d->link_sender = QString();
		OnLoadTcf(params["TCFPath"].toString());
		return true;
	}
		
	void MainWindow::OnLassoErase(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::LASSODELETE) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}
		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate lasso delete tool");
				return;
			}
		}else {
			//force deactivate and activate current mask
			/*auto mname = ws->GetCurMaskName();
			if (!mname.isEmpty()) {
				OnDeactivate(mname);
				OnActivate(mname);
			}*/
			if (false == toolController.ActivateTool(Entity::ToolIdx::LASSODELETE)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate lasso delete tool");
				return;
			}
		}
    }

	void MainWindow::OnSizeIndexChanged(int idx) {
        //non-clean architecture
		d->sceneManager->SetSizeIndex(idx);
    }

	void MainWindow::OnSizeFilter(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != Entity::ToolIdx::None) {
			if (toolID != Entity::ToolIdx::SIZEFILTER) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}
		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate pick erase tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::SIZEFILTER)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate pick erase tool");
				return;
			}
		}
	}

	void MainWindow::OnDivide(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if(toolID !=-1) {
		    if(toolID != Entity::ToolIdx::DIVIDE) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}				
		    }
		}		
		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate divide tool");
				return;
			}
		}
		else {
			//force deactivate and activate current mask
			/*auto mname = ws->GetCurMaskName();
			if (!mname.isEmpty()) {
				OnDeactivate(mname);
				OnActivate(mname);
			}*/
			if (false == toolController.ActivateTool(Entity::ToolIdx::DIVIDE)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate divide tool");
				return;
			}
		}		
	}

	void MainWindow::OnBranch(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::BRANCH) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::BRANCH)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate branch tool");
				return;
			}
		}
	}


	void MainWindow::OnAddFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::ADD) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::ADD)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate add tool");
				return;
			}
		}
	}
	void MainWindow::OnSubFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::SUBTRACT) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::SUBTRACT)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate sub tool");
				return;
			}
		}
	}
	void MainWindow::OnPaintFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::PAINT) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::PAINT)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate paint tool");
				return;
			}
		}
	}
	void MainWindow::OnWipeFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::WIPE) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::WIPE)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate wipe tool");
				return;
			}
		}
	}

	void MainWindow::OnFillFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::Fill) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::Fill)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate fill tool");
				return;
			}
		}
	}
	void MainWindow::OnEraseFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::ERASE) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::ERASE)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate fill tool");
				return;
			}
		}
	}
	void MainWindow::OnPaintBrushSizeChanged(int size) {
		d->sceneManager->SetPaintBrushSize(size);
	}

	void MainWindow::OnWipeBrushSizeChanged(int size) {
		d->sceneManager->SetWipeBrushSize(size);
	}	

	void MainWindow::OnLabelValueChanged(int val) {
		auto ws = Entity::WorkingSet::GetInstance();
		ws->SetCurLabel(val);
		d->sceneManager->SetLabelValue(val);
	}
	auto MainWindow::Reset() -> void {
		//Reset UIs		
		if (nullptr != d->ui->CleanerPanel) {
			d->ui->CleanerPanel->Reset();
		}
		if (nullptr != d->ui->LabelControlPanel) {
			d->ui->LabelControlPanel->Reset();
		}
		if (nullptr != d->ui->DrawingToolPanel) {
			d->ui->DrawingToolPanel->Reset();
		}
		if (nullptr != d->ui->FLControlPanel) {
			d->ui->FLControlPanel->Reset();
		}
		if (nullptr != d->ui->GeneralPanel) {
			d->ui->GeneralPanel->Reset();
		}
		if (nullptr != d->ui->HistoryPanel) {
			d->ui->HistoryPanel->Reset();
		}
		if (nullptr != d->ui->IDManagerPanel) {
			d->ui->IDManagerPanel->Reset();
		}
		if (nullptr != d->ui->MaskControlPanel) {
			d->ui->MaskControlPanel->Reset();
		}
		if (nullptr != d->ui->MaskSelectionPanel) {
			d->ui->MaskSelectionPanel->Reset();
		}
		if (nullptr != d->ui->NavigatorPanel) {
			d->ui->NavigatorPanel->Reset();
		}
		if (nullptr != d->ui->PlayerPanel) {
			d->ui->PlayerPanel->Reset();
		}
		//Reset RenderWindow
		/*if (nullptr != d->ui->RenderWindow) {
			d->ui->RenderWindow->Reset();
		}*/		
		//Reset SceneManager
		if (nullptr != d->sceneManager) {
			d->sceneManager->Reset();
		}
	}
	auto MainWindow::OnLoadTcf(const QString& path) -> void {
		d->force_step = -1;
		d->ui->PlayerPanel->setEnabled(true);
		d->tcf_path = path;

		d->screenshotWidget->SetTcfPath(path);

		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, d->ui->GeneralPanel, d->ui->HistoryPanel, d->ui->IDManagerPanel, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->NavigatorPanel, d->ui->FLControlPanel, d->ui->MaskSelectionPanel, d->ui->SemiAutoPanel, d->ui->PlayerPanel));
		const std::shared_ptr<Plugins::ImageDataIO::ImageDataReader> imageReader(new Plugins::ImageDataIO::ImageDataReader());

		Interactor::ImageDataController imageDataController(presenter.get(), workPresenter.get(), imageReader.get(), nullptr, nullptr);
		if (false == imageDataController.LoadTcfImage(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to load tcf image");
			return;
		}

		auto metaReader = new TC::IO::TCFMetaReader;
		auto meta = metaReader->Read(path);

		d->ui->RenderWindow->SetHTMinMax(meta->data.data3D.riMin*10000.0, meta->data.data3D.riMax*10000.0);

		delete metaReader;
	}
	void MainWindow::OnTimeChange(int step) {
		auto ws = Entity::WorkingSet::GetInstance();
		auto working_mask = ws->GetCurMaskName();
		if (!working_mask.isEmpty()) {
			OnDeactivate(working_mask);
		}

		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(nullptr, d->ui->CleanerPanel, d->ui->DrawingToolPanel, d->ui->GeneralPanel, d->ui->HistoryPanel, d->ui->IDManagerPanel, d->ui->LabelControlPanel, d->ui->MaskControlPanel, nullptr, d->ui->FLControlPanel, d->ui->MaskSelectionPanel, d->ui->SemiAutoPanel, d->ui->PlayerPanel));

		const std::shared_ptr<Plugins::ImageDataIO::ImageDataReader> imageReader(new Plugins::ImageDataIO::ImageDataReader());
		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());
		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());

		Interactor::ImageDataController imageDataController(presenter.get(), workPresenter.get(), imageReader.get(), maskReader.get(), maskWriter.get());

		if (false == imageDataController.TimeStepChanged(step)) {
			QMessageBox::warning(nullptr, "Error", "Failed to change time step");
			return;
		}
	}		

	void MainWindow::OnLoadFromLink(QString path) {		
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, d->ui->GeneralPanel, d->ui->HistoryPanel, d->ui->IDManagerPanel, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->NavigatorPanel, d->ui->FLControlPanel, d->ui->MaskSelectionPanel, d->ui->SemiAutoPanel));

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());

		Interactor::ImageDataController imageDataController(presenter.get(), workPresenter.get(), nullptr, maskReader.get(), nullptr);
		if (false == imageDataController.LoadMask(path,true)) {
			QMessageBox::warning(nullptr, "Error", "Failed to load mask");
			return;
		}
    }


	void MainWindow::OnLoadFunction(QString path) {
		auto idDS = d->ui->IDManagerPanel->GetDS();
		auto wd = idDS->workindDir;
		if(wd.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Failed to load: there is no working directory");
			return;
		}
		if(idDS->curUser.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Failed to load: there is no user id");
			return;
		}
		if(idDS->curUser == "System") {
			QMessageBox::warning(nullptr, "Error", "Failed to load: cannot overwrite on system mask");
			return;
		}
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, d->ui->GeneralPanel, d->ui->HistoryPanel, d->ui->IDManagerPanel, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->NavigatorPanel, d->ui->FLControlPanel, d->ui->MaskSelectionPanel, d->ui->SemiAutoPanel));

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());

		Interactor::ImageDataController imageDataController(presenter.get(), workPresenter.get(), nullptr, maskReader.get(), nullptr);
		if (false == imageDataController.LoadMask(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to load mask");
			return;
		}
	}
	
	void MainWindow::OnStateChanged(int state, QString mask_name) {
		auto idds = d->ui->IDManagerPanel->GetDS();
		if (idds->curUser.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select User First!");
			return;
		}

		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, d->ui->MaskControlPanel, nullptr, nullptr, d->ui->MaskSelectionPanel));

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());

		Interactor::ImageDataController controller(presenter.get(), workpresenter.get(), nullptr, maskReader.get(), nullptr);

		if (false == controller.HandleStateChanges(mask_name, state)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to handle state change of  %1").arg(mask_name));
		}
	}

	void MainWindow::OnIdChanged(QString id,QString maskPath) {		
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(nullptr, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, nullptr, nullptr, d->ui->MaskSelectionPanel, d->ui->SemiAutoPanel));

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());

		Interactor::ImageDataController imageDataController(presenter.get(), workPresenter.get(), nullptr, maskReader.get(), maskWriter.get());
		if(false == imageDataController.HandleUserChange(id, maskPath)) {
			QMessageBox::warning(nullptr, "Error", "Failed to load mask");
			return;
		}
	}
	void MainWindow::OnIdCopy(QString id,QString srcInst,QString srtOrgan,QString targetInst,QString targetOrgan) {

	}

	void MainWindow::OnSaveLinkFunction() {
		if (d->link_sender.isEmpty()) {
			return;
		}
		//save current mask first
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());

		Interactor::ImageDataController imageDataController(presenter.get(), nullptr, nullptr, nullptr, maskWriter.get());
		
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel,nullptr,nullptr, nullptr, d->ui->LabelControlPanel,nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
		
		auto ws = Entity::WorkingSet::GetInstance();

		if (-1 != ws->GetCurToolIdx()) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed deactivate Flush mask");
				return;
			}
		}
				
		if (!ws->GetCurUser().isEmpty()) {
			if (ws->GetCurUser() != "System") {
				if (false == imageDataController.SaveMasks()) {
					QMessageBox::warning(nullptr, "Error", "Failed save current masks");
					return;
				}
			}
		}
				
	    if (d->from_graph) {
			//send user defined mask to history
			QStringList linkGraphSet;
			linkGraphSet.push_back(d->link_sender);
			linkGraphSet.push_back(d->link_full);
			linkGraphSet.push_back(d->graph_pg);
			linkGraphSet.push_back(d->graph_hyper);
			linkGraphSet.push_back(d->tcf_path);
			linkGraphSet.push_back(QString::number(d->force_step));
			linkGraphSet.push_back(ws->GetCurUser());			
			BroadcastLinkGraph(linkGraphSet);
		}
		else {
			//copy user defined mask to system mask
			//and delete user defined mask
			auto usrName = ws->GetCurUser();
			if (usrName != "System") {
				auto prevPath = ws->GetMaskPath();
				if (QFile::exists(prevPath)) {
					auto oriPath = prevPath.chopped(4 + usrName.length() + 1) + ".msk";
					if (QFile::exists(oriPath)) {
						QFile::remove(oriPath);
					}
					QDir dir(oriPath.chopped(4));
					if (dir.exists()) {
						dir.removeRecursively();
					}
					QFile::copy(prevPath, oriPath);
					CopyPath(prevPath.chopped(4), oriPath.chopped(4));
					QFile::remove(prevPath);
					QDir pdir(prevPath.chopped(4));
					if (pdir.exists()) {
						pdir.removeRecursively();
					}
				}
			}			
			BroadcastLinkSender(d->link_sender, d->link_full);
		}
	}
	void MainWindow::OnSaveFunction(QString path, QString type, QStringList organs) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());

		Interactor::ImageDataController imageDataController(presenter.get(), nullptr, nullptr, nullptr, maskWriter.get());

		if (false == imageDataController.SaveMask(path, type, organs)) {
			QMessageBox::warning(nullptr, "Error", "Failed to save mask");
			return;
		}
	}
	void MainWindow::OnUndoFunction() {		
		d->sceneManager->TryUndo();
	}
	void MainWindow::OnRedoFunction() {		
		d->sceneManager->TryRedo();
	}
	void MainWindow::OnRefreshFunction() {		
		d->sceneManager->TryRefresh();
	}
	void MainWindow::OnWindowFunction() {
		const std::shared_ptr<Interactor::WorkBenchPresenter> presenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow));

		Interactor::WorkBenchController workbenchController(presenter.get());
		if (false == workbenchController.ChangeLayout()) {
			QMessageBox::warning(nullptr, "Error", "Failed to change layout");
			return;
		}
		auto order_idx = d->ui->RenderWindow->GetCurrentLayout();
		switch (order_idx) {
		case 0:
			d->screenshotWidget->SetMultiLayerType(TC::MultiLayoutType::BigXY);
			d->sceneManager->SetCurLarge(0);
			break;
		case 1:
			d->screenshotWidget->SetMultiLayerType(TC::MultiLayoutType::BigYZ);
			d->sceneManager->SetCurLarge(2);
			break;
		case 2:
			d->screenshotWidget->SetMultiLayerType(TC::MultiLayoutType::BigXZ);
			d->sceneManager->SetCurLarge(1);
			break;
		case 3:
			d->screenshotWidget->SetMultiLayerType(TC::MultiLayoutType::Big3D);
			break;
		}
	}
	void MainWindow::OnFlushFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
		if(deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed deactivate Flush mask");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::FLUSH)) {
				QMessageBox::warning(nullptr, "Error", "Failed Flush current mask");
				return;
			}
		}
	}
	void MainWindow::OnFlushSelection(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed deactivate Flush mask");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::FLUSH_SEL)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate Flush mask");
				return;
			}
		}
	}		

	void MainWindow::OnLabelMergeFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::MERGE_LABEL) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if(deactivate) {
		    if(false == toolController.DeactivateTool(true)) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate picking tool");
				return;
		    }
			//force deactivate and activate current mask
			//auto mname = ws->GetCurMaskName();
			//OnDeactivate(mname);
			//OnActivate(mname);
		}else {
		    if(false == toolController.ActivateTool(Entity::ToolIdx::MERGE_LABEL)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate Label Merge tool");
				return;
		    }
		}
    }		
	void MainWindow::OnPickErase(bool deactivate) {		
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if(toolID != Entity::ToolIdx::None) {
		    if(toolID != Entity::ToolIdx::PICKDELETE) {
		        if(false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
		        }
		    }
		}
		if(deactivate) {
		    if(false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate pick erase tool");
				return;
		    }
		}else {
		    if(false == toolController.ActivateTool(Entity::ToolIdx::PICKDELETE)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate pick erase tool");
				return;
		    }
		}
    }

	void MainWindow::OnPickFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::PICK) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate picking tool");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::PICK)) {
				QMessageBox::warning(nullptr, "Error", "Failed to activate Picking tool");
				return;
			}
		}
	}
	void MainWindow::OnCleanFunction(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));		

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(),workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::CLEAN) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if(deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed deactivate clean selection");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::CLEAN)) {
				QMessageBox::warning(nullptr, "Error", "Failed Clean current slice");
				return;
			}
		}
	}
	void MainWindow::OnCleanSelection(bool deactivate) {
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto ws = Entity::WorkingSet::GetInstance();
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (toolID != Entity::ToolIdx::CLEAN_SEL) {
				if (false == toolController.DeactivateTool()) {
					QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
				}
			}
		}

		if (deactivate) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed deactivate clean selection");
				return;
			}
		}
		else {
			if (false == toolController.ActivateTool(Entity::ToolIdx::CLEAN_SEL)) {
				QMessageBox::warning(nullptr, "Error", "Failed Clean selection");
				return;
			}
		}
	}

	void MainWindow::OnSlicePositionChanged(int x, int y, int z) {
		d->sceneManager->SetSliceIndex(0, x);
		d->sceneManager->SetSliceIndex(1, y);
		d->sceneManager->SetSliceIndex(2, z);
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, d->ui->NavigatorPanel));

		Interactor::ImageDataController imageController(nullptr, workpresenter.get(), nullptr, nullptr, nullptr);

		if (false == imageController.ImageSliceChanged(x,y,z)) {
			QMessageBox::warning(nullptr, "Error", "Failed change sliceIndex");
			return;
		}
    }

	void MainWindow::OnSlicePhyChanged(float phyX, float phyY, float phyZ) {
		auto ws = Entity::WorkingSet::GetInstance();
		auto image = ws->GetImageData();
		double res[3];
	    image->GetResolution(res);		
		d->sceneManager->SetSliceIndex(0, (phyX / res[0] + 0.5));
		d->sceneManager->SetSliceIndex(1, (phyY/ res[1] + 0.5));
		d->sceneManager->SetSliceIndex(2, (phyZ / res[2] + 0.5));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, d->ui->NavigatorPanel));

		Interactor::ImageDataController imageController(nullptr, workpresenter.get(), nullptr, nullptr, nullptr);

		if (false == imageController.ImageSliceChanged((phyX / res[0]+0.5), (phyY / res[1] +0.5), (phyZ / res[2]+0.5))) {
			QMessageBox::warning(nullptr, "Error", "Failed change sliceIndex");
			return;
		}

    }

	void MainWindow::OnSliceIndexChanged(int viewIndex, int sliceIndex) {
		d->sceneManager->SetSliceIndex(viewIndex, sliceIndex);		
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(nullptr, nullptr, nullptr, nullptr,nullptr,nullptr,nullptr,nullptr,d->ui->NavigatorPanel));

		Interactor::ImageDataController imageController(nullptr, workpresenter.get(), nullptr, nullptr, nullptr);

		if(false == imageController.ImageSliceChanged(viewIndex,sliceIndex)) {
			QMessageBox::warning(nullptr, "Error", "Failed change sliceIndex");
			return;
		}
	}

	void MainWindow::OnPickSlice(float x, float y, float z, int axis) {
        if(true == d->sceneManager->isInAiAddPoint()) {
			d->sceneManager->OnPointPick(x, y, z, axis);
			return;
        }
		OnSlicePositionChanged(static_cast<int>(x), static_cast<int>(y), static_cast<int>(z));
    }

	void MainWindow::OnLabelAdd() {
		auto ws = Entity::WorkingSet::GetInstance();
		auto curUser = ws->GetCurUser();
		if(curUser == "System") {
			return;
		}
		auto curMask = ws->GetCurMaskName();
		if(curMask.isEmpty()) {
			return;
		}
		auto max = d->ui->LabelControlPanel->GetDS()->labelMax;		
		if (false == d->sceneManager->AddLabel(max+1)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add label");
			return;
		}
		d->ui->LabelControlPanel->ForceAddLabel();
	}
	void MainWindow::OnActivate(QString mask_name) {
		auto ws = Entity::WorkingSet::GetInstance();
		if (mask_name != "cellInst") {			
			ws->SetCurLabel(1);
			d->sceneManager->SetLabelValue(1);
		}
		//ws->SetCurToolIdx(-1);
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, nullptr, nullptr, d->ui->MaskSelectionPanel));

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());

		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
				return;
			}
		}
		ws->SetCurToolIdx(-1);		
		const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());		

		Interactor::ImageDataController controller(presenter.get(), workpresenter.get(), nullptr, maskReader.get(), nullptr);

		if (false == controller.SetWorkingData(mask_name)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to set working mask file %1").arg(mask_name));
		}				
	}
	void MainWindow::OnVizChanged(bool state, QString mask_name) {
		auto ws = Entity::WorkingSet::GetInstance();				

		const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->sceneManager));
		const std::shared_ptr<Interactor::WorkBenchPresenter> wpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, nullptr, nullptr, d->ui->MaskSelectionPanel));
		Interactor::ToolController toolController(spresenter.get(), wpresenter.get());
		auto pre_tool = ws->GetCurToolIdx();
		if (pre_tool != -1) {
			if (false == toolController.DeactivateTool()) {
				QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
				return;
			}
		}
		ws->SetCurToolIdx(-1);
		auto curMask = ws->GetCurMaskName();
		auto curUser = ws->GetCurUser();
		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());
		Interactor::ImageDataController controller(spresenter.get(), wpresenter.get(), nullptr, nullptr, maskWriter.get());
		if (!curMask.isEmpty()) {
			if (false == controller.UnsetWorkingData(curMask)) {
				QMessageBox::warning(nullptr, "Error", QString("Failed to unset working mask file %1").arg(curMask));
			}
		}		
		if(false == controller.ChangeMultiVizInfo(mask_name,state)) {
			QMessageBox::warning(nullptr, "Error", "Failed to perform multi-layer viz");
			return;
		}

	}
	void MainWindow::OnDeactivate(QString mask_name) {
		//save current mask first
		auto ws = Entity::WorkingSet::GetInstance();				
		const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
		
		const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, nullptr, nullptr, d->ui->MaskSelectionPanel));
		//const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader());

		const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter());

		Interactor::ToolController toolController(presenter.get(), workpresenter.get());
				
		auto toolID = ws->GetCurToolIdx();
		if (toolID != -1) {
		    if (false == toolController.DeactivateTool()) {
		        QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
					return;
		    }
		}
		ws->SetCurToolIdx(-1);

		Interactor::ImageDataController controller(presenter.get(), workpresenter.get(), nullptr, nullptr, maskWriter.get());

		if (false == controller.UnsetWorkingData(mask_name)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to unset working mask file %1").arg(mask_name));
		}
	}

	void MainWindow::OnFLSelected(int ch) {
		auto ws = Entity::WorkingSet::GetInstance();
		auto sizeZ = ws->GetMetaInfo()->data.data3DFL.sizeZ;
		auto resZ = ws->GetMetaInfo()->data.data3DFL.resolutionZ;
		auto of = ws->GetMetaInfo()->data.data3DFL.offsetZ;
		if(sizeZ > 1) {
			of = (of - (sizeZ * resZ / 2));
		}
		auto flData = ws->GetImageFLData(ch);
		d->sceneManager->SetFLImage(flData,ch,of);
		d->sceneManager->SetFLOpacity(ws->GetFLOpacity(ch));

		const std::shared_ptr<Interactor::WorkBenchPresenter> workPresenter(new Interactor::WorkBenchPresenter(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, d->ui->FLControlPanel));

		Interactor::ImageDataController controller(nullptr, workPresenter.get(), nullptr, nullptr, nullptr);

		if(false == controller.ActivateFL(ch)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to show FL slice"));
			return;
		}
    }

	void MainWindow::OnFLOpacity(int ch, double opacity) {
		//one-way panel to renderer
		auto ws = Entity::WorkingSet::GetInstance();
		ws->SetFLOpacity(ch, opacity);
		d->sceneManager->SetFLOpacity(opacity);
    }

	void MainWindow::OnFLColor(int ch, QColor color) {
		Q_UNUSED(ch)
		//one-way panel to renderer
		d->sceneManager->SetFLColor(color);
    }

	void MainWindow::OnDilate() {
		auto ws = Entity::WorkingSet::GetInstance();
		auto curMask = ws->GetCurMaskName();		
		if (curMask.isEmpty()) {			
			return;
		}
		auto curUser = ws->GetCurUser();
		if(curUser == "System") {
			return;
		}
		auto label = ws->GetCurLabel();
		ws->SetCurToolIdx(Entity::ToolIdx::DILATE);
		if(false == d->sceneManager->PerformDilate(label)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to perform dilation"));
			return;
		}
    }

	void MainWindow::OnErode() {
		auto ws = Entity::WorkingSet::GetInstance();		
		auto curMask = ws->GetCurMaskName();
		auto curUser = ws->GetCurUser();
		if(curMask.isEmpty()||curUser == "System") {			
			return;
		}
	    auto label = ws->GetCurLabel();
	    ws->SetCurToolIdx(Entity::ToolIdx::ERODE);
	    if (false == d->sceneManager->PerformErode(label)) {
	        QMessageBox::warning(nullptr, "Error", QString("Failed to perform erosion"));
	        return;
	    }		
    }

	void MainWindow::OnWaterShed() {
		auto ws = Entity::WorkingSet::GetInstance();		
		auto organ = ws->GetOrganData();
		if(nullptr == organ) {
			return;
		}
		auto inst = ws->GetInstData();
		if(nullptr == inst) {
			return;
		}
		auto user = ws->GetCurUser();
		if(user == "System") {
			return;
		}
		auto maskName = ws->GetCurMaskName();
		if(maskName.isEmpty()) {
			return;
		}
		if(false == organ->GetLayerNames().contains("membrane")) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to perform watershed algorithm"));
			return;
		}
		ws->SetCurToolIdx(Entity::ToolIdx::WATERSHED);
		if(false == d->sceneManager->PerformWaterShed(organ)) {
			QMessageBox::warning(nullptr, "Error", QString("Failed to perform watershed algorithm"));
			return;
		}
    }


	auto MainWindow::InitUI() -> bool {
		//Segmentation Button Calls        
		connect(d->ui->DrawingToolPanel, SIGNAL(sigAdd(bool)), this, SLOT(OnAddFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigSub(bool)), this, SLOT(OnSubFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigPaint(bool)), this, SLOT(OnPaintFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigWipe(bool)), this, SLOT(OnWipeFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigFill(bool)), this, SLOT(OnFillFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigErase(bool)), this, SLOT(OnEraseFunction(bool)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigBrushSizeChanged(int)), this, SLOT(OnPaintBrushSizeChanged(int)));
		connect(d->ui->DrawingToolPanel, SIGNAL(sigBrushSizeChanged(int)), this, SLOT(OnWipeBrushSizeChanged(int)));

		connect(d->ui->CleanerPanel, SIGNAL(sigClean(bool)), this, SLOT(OnCleanFunction(bool)));
		connect(d->ui->CleanerPanel, SIGNAL(sigCleanSel(bool)), this, SLOT(OnCleanSelection(bool)));
		connect(d->ui->CleanerPanel, SIGNAL(sigFlush(bool)), this, SLOT(OnFlushFunction(bool)));
		connect(d->ui->CleanerPanel, SIGNAL(sigFlushSel(bool)), this, SLOT(OnFlushSelection(bool)));

		connect(d->ui->LabelControlPanel, SIGNAL(sigPick(bool)), this, SLOT(OnPickFunction(bool)));
		connect(d->ui->LabelControlPanel, SIGNAL(sigLabelChange(int)), this, SLOT(OnLabelValueChanged(int)));
		connect(d->ui->LabelControlPanel, SIGNAL(sigAddLabel()), this, SLOT(OnLabelAdd()));
		connect(d->ui->LabelControlPanel, SIGNAL(sigMergeLabel(bool)), this, SLOT(OnLabelMergeFunction(bool)));

		//Global Button Calls
		connect(d->ui->GeneralPanel, SIGNAL(sigLoad(QString)), this, SLOT(OnLoadFunction(QString)));
		connect(d->ui->GeneralPanel, SIGNAL(sigSave(QString, QString, QStringList)), this, SLOT(OnSaveFunction(QString, QString, QStringList)));
		connect(d->ui->GeneralPanel, SIGNAL(sigSaveLink()), this, SLOT(OnSaveLinkFunction()));
		connect(d->ui->GeneralPanel, SIGNAL(sigWindow()), this, SLOT(OnWindowFunction()));

		connect(d->ui->HistoryPanel, SIGNAL(sigUndo()), this, SLOT(OnUndoFunction()));
		connect(d->ui->HistoryPanel, SIGNAL(sigRedo()), this, SLOT(OnRedoFunction()));
		connect(d->ui->HistoryPanel, SIGNAL(sigRefresh()), this, SLOT(OnRefreshFunction()));

		//ID Panel calls
		connect(d->ui->IDManagerPanel, SIGNAL(sigIdChanged(QString,QString)), this, SLOT(OnIdChanged(QString,QString)));
		connect(d->ui->IDManagerPanel, SIGNAL(sigIdCopy(QString,QString,QString,QString,QString)), this, SLOT(OnIdCopy(QString,QString,QString,QString,QString)));

		//Mask Control Panel calls
		connect(d->ui->MaskControlPanel, SIGNAL(sigState(int, QString)), this, SLOT(OnStateChanged(int, QString)));
		connect(d->ui->MaskControlPanel, SIGNAL(sigViz(bool, QString)), this, SLOT(OnVizChanged(bool, QString)));

		//Mask Selection Panel calls
		connect(d->ui->MaskSelectionPanel, SIGNAL(sigActivate(QString)), this, SLOT(OnActivate(QString)));
		connect(d->ui->MaskSelectionPanel, SIGNAL(sigDeactivate(QString)), this, SLOT(OnDeactivate(QString)));

		//Semi Auto Panel calls
		connect(d->ui->SemiAutoPanel, SIGNAL(sigDivide(bool)), this, SLOT(OnDivide(bool)));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigBranch(bool)), this, SLOT(OnBranch(bool)));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigDilate()), this, SLOT(OnDilate()));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigErode()), this, SLOT(OnErode()));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigWatershed()), this, SLOT(OnWaterShed()));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigPickDel(bool)), this, SLOT(OnPickErase(bool)));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigLassoDel(bool)), this, SLOT(OnLassoErase(bool)));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigSizeFilter(bool)), this, SLOT(OnSizeFilter(bool)));
		connect(d->ui->SemiAutoPanel, SIGNAL(sigSizeIndex(int)), this, SLOT(OnSizeIndexChanged(int)));

		//Time frame changed calls
		connect(d->ui->PlayerPanel, SIGNAL(timelapseIndexChanged(int)), this, SLOT(OnTimeChange(int)));

		//slice change calls
		connect(d->ui->NavigatorPanel, SIGNAL(positionChanged(int, int, int)), this, SLOT(OnSlicePositionChanged(int,int,int)));
		connect(d->ui->NavigatorPanel, SIGNAL(phyPositionChanged(float, float, float)), this, SLOT(OnSlicePhyChanged(float, float, float)));		

		// Render window calls
		connect(d->ui->RenderWindow, SIGNAL(sliceIndexChanged(int, int)), this, 
			SLOT(OnSliceIndexChanged(int, int)));
		//connect(d->ui->RenderWindow, SIGNAL(slicePicking(float, float, float,int)), d->sceneManager, SLOT(OnPointPick(float,float,float,int)));
		connect(d->ui->RenderWindow, SIGNAL(slicePicking(float, float, float, int)), this, SLOT(OnPickSlice(float, float, float, int)));

		//FL channel calls
		connect(d->ui->FLControlPanel, SIGNAL(flChSelected(int)), this, SLOT(OnFLSelected(int)));
		connect(d->ui->FLControlPanel, SIGNAL(flOpacity(int, double)), this, SLOT(OnFLOpacity(int, double)));
		connect(d->ui->FLControlPanel, SIGNAL(flColorChanged(int, QColor)), this, SLOT(OnFLColor(int,QColor)));

		auto root3d = d->sceneManager->Get3DSceneGraph();
		d->ui->RenderWindow->Set3DSceneGraph(root3d);

		d->sceneManager->Set3DCamera(d->ui->RenderWindow->Get3dCam());

		for (auto i = 0; i < 3; i++) {
			auto root2d = d->sceneManager->Get2DSceneGraph(i);
			d->ui->RenderWindow->Set2DSceneGraph(root2d, i);
		}

		d->aiControls = std::make_shared<Plugins::AiControlPanel>();
		d->aiControls->hide();

		connect(d->ui->SemiAutoPanel, SIGNAL(sigAiPanel(bool)), this, SLOT(ToggleAiPanel(bool)));
		connect(d->aiControls.get(), SIGNAL(sigSetImage()), this, SLOT(OnSetImageToAI()));
		connect(d->aiControls.get(), SIGNAL(sigAddPoint(bool,bool)), this, SLOT(OnAddAiPoint(bool,bool)));
		connect(d->aiControls.get(), SIGNAL(sigUpdateVol()), this, SLOT(OnUpdateAiVolume()));
		connect(d->aiControls.get(), SIGNAL(sigCloseAiPanel()), this, SLOT(OnAiPanelClosed()));
		connect(d->aiControls.get(), SIGNAL(sigSliceRecom()), this, SLOT(OnAiSliceRecommendation()));
		connect(d->aiControls.get(), SIGNAL(sigIs2D(bool)), this, SLOT(OnAiIs2D(bool)));
		connect(d->aiControls.get(), SIGNAL(sigIsPositive(bool)), this, SLOT(OnAiIsPositive(bool)));
		connect(d->aiControls.get(), SIGNAL(sigSetSlice()), this, SLOT(OnAiAddSlice()));
		connect(d->aiControls.get(), SIGNAL(sigFinishImage()), this, SLOT(OnFinishAI()));

		return true;
	}
	void MainWindow::OnAiAddSlice() {
		d->sceneManager->AiAddSlice();
		d->sceneManager->AiGenerateVolume();
	}
	void MainWindow::OnAddAiPoint(bool start,bool isSilent) {
		d->sceneManager->ToggleAiPoint(start);
		if(isSilent) {
			return;
		}
		if (start) {
			std::cout << "add ai point start" << std::endl;
			auto ws = Entity::WorkingSet::GetInstance();
			if (ws->GetCurToolIdx() != -1) {
				const std::shared_ptr<Interactor::ScenePresenter> presenter(new Interactor::ScenePresenter(d->sceneManager));
				const std::shared_ptr<Interactor::WorkBenchPresenter> workpresenter(new Interactor::WorkBenchPresenter(d->ui->RenderWindow, d->ui->CleanerPanel, d->ui->DrawingToolPanel, nullptr, nullptr, nullptr, d->ui->LabelControlPanel, nullptr, nullptr, nullptr, nullptr, d->ui->SemiAutoPanel));
				Interactor::ToolController toolController(presenter.get(), workpresenter.get());
				if (false == toolController.DeactivateTool()) {
					std::cout << "failed to deactivatetool on ai point tool " << std::endl;
				}
			}
		}else {
			std::cout << "add ai point finished" << std::endl;
		}
    }

	void MainWindow::OnAiPanelClosed() {
		d->ui->SemiAutoPanel->AiPanelHidden();
    }

	void MainWindow::OnFinishAI() {
		d->sceneManager->AiFinishImage();
    }

	void MainWindow::OnSetImageToAI() {
		//std::cout << "set ai image" << std::endl;
		d->sceneManager->AiSetImage();				
    }

	void MainWindow::OnAiIs2D(bool is2D) {
		d->sceneManager->ToggleAi2D(is2D);
    }

	void MainWindow::OnAiIsPositive(bool isPositive) {
		d->sceneManager->ToggleAiPositive(isPositive);
    }

	void MainWindow::OnAiSliceRecommendation() {		
		d->sceneManager->AiSliceRecommendation();
		//get slice and information from AI module and apply it to render window
    }
	
	void MainWindow::OnUpdateAiVolume() {
		d->sceneManager->AiGenerateVolume();
    }

	void MainWindow::OnAiSetSlide(int axis, int index) {
		auto cur_lay = d->ui->RenderWindow->GetCurrentLayout();		
		while(cur_lay != axis) {
			d->ui->RenderWindow->ChangeLayout();
			cur_lay = d->ui->RenderWindow->GetCurrentLayout();
		}
		auto ds = d->ui->NavigatorPanel->GetNavigatorDS();
		if(axis == 0) {
			ds->z = index;
		}else if(axis ==1) {
			ds->x = index;
		}else if(axis == 2) {
			ds->y = index;
		}
		d->ui->NavigatorPanel->RefreshWithCall();
    }

	void MainWindow::ToggleAiPanel(bool show) {
        if(show) {
			d->aiControls->show();
        }else {
			d->aiControls->hide();
        }
    }


	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
		d->share_param[name] = std::make_shared<IParameter>(param->Clone());
	}
	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
		if (d->share_param.contains(name)) {
			return d->share_param[name];
		}
		return nullptr;
	}
	auto MainWindow::TryDeactivate() -> bool {
		//TODO: clean part
		return true;
    }
	auto MainWindow::TryActivate() -> bool {
		d->ui = new Ui::MainWindow;
		d->ui->setupUi(this);				

		d->sceneManager = new Plugins::SceneManager;

		d->screenshotWidget = std::make_shared<TC::ScreenShotWidget>();
		d->screenshotWidget->SetVolumeName("volData");
		d->ui->RenderWindow->ConnectScreenShotWidget(d->screenshotWidget);
		auto slayout = new QVBoxLayout;
		slayout->setContentsMargins(0, 0, 0, 0);
		d->ui->screenshotContainer->setLayout(slayout);
		slayout->addWidget(d->screenshotWidget.get());		

		this->InitUI();
		connect(d->sceneManager, SIGNAL(sigLabel(int)), d->ui->LabelControlPanel, SLOT(SetLabelValue(int)));
		connect(d->sceneManager, SIGNAL(sigStart3dDrawing()), d->ui->RenderWindow, SLOT(onStartDrawing()));
		connect(d->sceneManager, SIGNAL(sigFinish3dDrawing()), d->ui->RenderWindow, SLOT(onFinishDrawing()));
		connect(d->sceneManager, SIGNAL(sigStartSizeFilter(int)), d->ui->SemiAutoPanel, SLOT(SetSizeMax(int)));
		connect(d->sceneManager, SIGNAL(sigAiSlide(int, int)), this, SLOT(OnAiSetSlide(int, int)));
		connect(d->sceneManager, SIGNAL(sigAiPointOff()), d->aiControls.get(), SLOT(OnAiPointOff()));

		//start global communication
		subscribeEvent("TabChange");
		subscribeEvent(MenuEvent::Topic());
		connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnTabFocused(ctkEvent)));
		return true;
    }
	auto MainWindow::IsActivate() -> bool {
		return (nullptr != d->ui);
    }
	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
    }

	auto MainWindow::GetFeatureName() const -> QString {
		return "org.tomocube.intersegPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}

	void MainWindow::OnTabFocused(const ctkEvent& ctkEvent) {
		using MenuType = TC::Framework::MenuTypeEnum;
		MenuType menuAction = MenuType::Action;
		MenuType menuChecked = MenuType::ActionChecked;
		MenuType menuUnchecked = MenuType::ActionUnchecked;
		MenuType sudden = MenuType::NONE;

		if (ctkEvent.getProperty("TabName").isValid()) {
			auto TabName = ctkEvent.getProperty("TabName").toString();
			if (TabName.compare("Segmentation") == 0) {
				auto tcf_path = GetCurTCF();
				MenuEvent menuEvt(MenuTypeEnum::TitleBar);
				menuEvt.scriptSet(tcf_path);
				publishSignal(menuEvt, "Segmentation");
			}else {
				ForceClosePopup();
			}
		}else if(ctkEvent.getProperty(sudden._to_string()).isValid()) {
			ForceClosePopup();
		}
    }
	auto MainWindow::ForceClosePopup() -> void {
        if(nullptr != d->aiControls) {
			d->aiControls->close();
        }
    }
	auto MainWindow::BroadcastLinkSender(QString sender, QString fullname) -> void {
		TC::Framework::AppEvent appEvent(AppTypeEnum::APP_UPDATE);
		appEvent.setFullName(fullname);
		appEvent.setAppName(sender);
		appEvent.addParameter("ExecutionType", "Update");
		appEvent.addParameter("UpdateType", "SingleRun");
		publishSignal(appEvent, "Mask Editor");
    }
	auto MainWindow::BroadcastLinkGraph(QStringList linkParams) -> void {
		TC::Framework::AppEvent appEvent(AppTypeEnum::APP_UPDATE);		
	    
		appEvent.setAppName(linkParams[0]);
		appEvent.setFullName(linkParams[1]);
		appEvent.addParameter("ExecutionType", "Update");
		appEvent.addParameter("UpdateType", "BatchRun");
		appEvent.addParameter("PGPath", linkParams[2]);
		appEvent.addParameter("HyperName", linkParams[3]);
		appEvent.addParameter("TCFPath", linkParams[4]);
		appEvent.addParameter("TimeStep", linkParams[5]);
		appEvent.addParameter("UserName", linkParams[6]);
		publishSignal(appEvent, "Mask Editor");
    }

}