#include <AppInterfaceTA.h>

#include <AppEvent.h>

#include "MainWindow.h"
#include "intersegPlugin.h"

namespace TomoAnalysis::InterSeg::AppUI {    
    struct intersegPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;        
        MainWindow* mainWindow{nullptr};
        AppInterfaceTA* appInterface{ nullptr };
        
    };

    intersegPlugin* intersegPlugin::instance = 0;

    intersegPlugin* intersegPlugin::getInstance(){
        return instance;
    }
    intersegPlugin::intersegPlugin() : d(new Impl) {                
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);        
    }
    intersegPlugin::~intersegPlugin() {
        
        //delete d->framework;        
    }

    auto intersegPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);
    }    
    /*auto intersegPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);        
    }*/
    auto intersegPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }        
}
