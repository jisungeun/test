#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::InterSeg::AppUI {
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto GetCurTCF()const->QString;

        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto Execute(const QVariantMap& parmas)->bool override;
        auto GetRunType() -> std::tuple<bool, bool> override;
        auto TryDeactivate() -> bool final;
        auto TryActivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;            

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

    protected:
        virtual void resizeEvent(QResizeEvent* event);

    protected slots:
        void OnAddFunction(bool);
        void OnSubFunction(bool);
        void OnPaintFunction(bool);
        void OnWipeFunction(bool);
        void OnFillFunction(bool);
        void OnEraseFunction(bool);
        void OnPickFunction(bool);
        void OnPaintBrushSizeChanged(int);
        void OnWipeBrushSizeChanged(int);
        void OnLabelValueChanged(int);        
        void OnCleanFunction(bool);
        void OnCleanSelection(bool);
        void OnFlushFunction(bool);
        void OnFlushSelection(bool);

        void OnLabelMergeFunction(bool);
        void OnLabelAdd();

        void OnBranch(bool);
        void OnDivide(bool);

        void OnPickSlice(float, float, float, int);

        //addtional 1.3.10        
        void OnPickErase(bool);
        void OnLassoErase(bool);
        void OnSizeFilter(bool);
        void OnSizeIndexChanged(int);

        void OnLoadFunction(QString);
        void OnSaveFunction(QString,QString,QStringList);
        void OnSaveLinkFunction();
        void OnWindowFunction();

        void OnUndoFunction();
        void OnRedoFunction();
        void OnRefreshFunction();

        void OnIdChanged(QString,QString);
        void OnIdCopy(QString,QString,QString,QString,QString);

        void OnStateChanged(int, QString);
        void OnVizChanged(bool, QString);

        void OnActivate(QString);
        void OnDeactivate(QString);

        void OnTimeChange(int);
        void OnSliceIndexChanged(int, int);
        void OnSlicePositionChanged(int, int, int);
        void OnSlicePhyChanged(float, float, float);

        void OnFLSelected(int);
        void OnFLOpacity(int, double);
        void OnFLColor(int, QColor);

        void OnDilate();
        void OnErode();
        void OnWaterShed();

        void ToggleAiPanel(bool);
        void OnSetImageToAI();
        void OnAddAiPoint(bool,bool);
        void OnAiSliceRecommendation();
        void OnAiIsPositive(bool);
        void OnAiIs2D(bool);
        void OnUpdateAiVolume();
        void OnAiSetSlide(int axis, int index);
        void OnAiPanelClosed();
        void OnAiAddSlice();
        void OnFinishAI();
        
        void OnTabFocused(const ctkEvent& ctkEvent);

    private:
        auto InitUI(void)->bool;
        auto OnLoadTcf(const QString& path)->void;
        auto Reset()->void;
        auto CopyPath(QString src, QString dst)->void;
        void OnLoadFromLink(QString);

        auto BroadcastLinkSender(QString, QString)->void;
        auto BroadcastLinkGraph(QStringList)->void;

        auto ForceClosePopup()->void;

        auto ExecuteLinkRun(const QVariantMap& params)->bool;
        auto ExecuteOpenTcf(const QVariantMap& params)->bool;
    
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}