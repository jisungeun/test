#include <AppInterfaceTA.h>

#include <MenuEvent.h>

#include "MainWindow.h"
#include "reportflPlugin.h"


namespace TomoAnalysis::Report::AppUI {    
    struct reportflPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;        
        MainWindow* mainWindow{nullptr};
        AppInterfaceTA* appInterface{ nullptr };
    };

    reportflPlugin* reportflPlugin::instance = 0;

    reportflPlugin* reportflPlugin::getInstance(){
        return instance;
    }
    reportflPlugin::reportflPlugin() : d(new Impl) {        
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);
        //UI module initialization                
    }
    reportflPlugin::~reportflPlugin() {
        
        //delete d->framework;        
    }

    auto reportflPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
        
        registerService(context, d->appInterface, plugin_symbolic_name);
    }

    /*auto reportflPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);        
    }*/
    auto reportflPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }        
}