#pragma once

#include <WorkingSet.h>

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API IReportPanel {
    public:
        IReportPanel();
        ~IReportPanel();
        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool = 0;
    };    
}
