#pragma once

#include <CubeDataList.h>
#include <WorkingSet.h>

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API IReportGraphPanel {
    public:
        IReportGraphPanel();
        ~IReportGraphPanel();

        virtual auto UpdateDataList(Entity::CubeDataList::Pointer cubeDataList)->bool = 0;
        virtual auto UpdateGraph(Entity::WorkingSet::Pointer workingset)->bool = 0;
    };    
}
