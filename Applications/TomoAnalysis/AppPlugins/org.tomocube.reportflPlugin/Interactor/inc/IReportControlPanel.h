#pragma once
#include <WorkingSet.h>

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API IReportControlPanel {
    public:
        IReportControlPanel();
        ~IReportControlPanel();
        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool = 0;
    };
}