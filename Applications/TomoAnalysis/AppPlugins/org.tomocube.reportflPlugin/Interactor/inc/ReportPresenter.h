#pragma once

#include <memory>
#include <IReportManagerPort.h>

#include "IReportControlPanel.h"
#include "IReportPanel.h"

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API ReportPresenter : public UseCase::IReportManagerPort {
    public:
        ReportPresenter(IReportPanel* report = nullptr, IReportControlPanel* control = nullptr);
        virtual ~ReportPresenter();

        auto Update(Entity::WorkingSet::Pointer workingset) -> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}