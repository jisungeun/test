#pragma once

#include <memory>
#include <QString>

#include <IMeasureReaderPort.h>
#include <IReportGraphPort.h>

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API GraphController {
    public:
        GraphController(UseCase::IReportGraphPort* port = nullptr, UseCase::IMeasureReaderPort* reader = nullptr);
        virtual ~GraphController();

        auto Request()->bool;

        auto UpdateDataList(Entity::WorkingSet::Pointer workingset)->bool;
        auto UpdateGraph(Entity::WorkingSet::Pointer workingset)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}