#pragma once

#include <memory>
#include <QString>

#include <WorkingSet.h>

#include <IMeasureReaderPort.h>
#include <IReportManagerPort.h>

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API TableController {
    public:
        TableController(UseCase::IReportManagerPort* port = nullptr,UseCase::IMeasureReaderPort* reader=nullptr);
        virtual ~TableController();

        auto Request()->bool;
        auto UpdateResult(const QString& reportPath,Entity::WorkingSet::Pointer workingset)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}