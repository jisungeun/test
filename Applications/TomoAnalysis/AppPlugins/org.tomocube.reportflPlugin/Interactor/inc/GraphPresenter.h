#pragma once

#include <memory>
#include <IReportGraphPort.h>

#include "IReportControlPanel.h"
#include "IReportGraphPanel.h"

#include "ReportInteractorExport.h"

namespace TomoAnalysis::Report::Interactor {
    class ReportInteractor_API GraphPresenter : public UseCase::IReportGraphPort {
    public:
        GraphPresenter(IReportGraphPanel* report = nullptr, IReportControlPanel* control = nullptr);
        virtual ~GraphPresenter();

        auto UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> void override;
        auto UpdateGraph(Entity::WorkingSet::Pointer workingset) -> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}