#include "ReportPresenter.h"

namespace TomoAnalysis::Report::Interactor {
    struct ReportPresenter::Impl {
        IReportPanel* reportPanel{nullptr};
        IReportControlPanel* controlPanel{nullptr};
    };
    ReportPresenter::ReportPresenter(IReportPanel* report, IReportControlPanel* control) : d{ new Impl } {
        d->reportPanel = report;
        d->controlPanel = control;
    }
    ReportPresenter::~ReportPresenter() {
        
    }
    auto ReportPresenter::Update(Entity::WorkingSet::Pointer workingset) -> void {
        if(nullptr!=d->reportPanel){
            d->reportPanel->Update(workingset);
        }        
    }
}