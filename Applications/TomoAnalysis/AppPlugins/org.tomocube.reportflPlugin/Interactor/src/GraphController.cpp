#include <UpdateDataList.h>
#include <DrawGraph.h>

#include "GraphController.h"

namespace TomoAnalysis::Report::Interactor {
    struct GraphController::Impl {
        UseCase::IReportGraphPort* outPort;
        UseCase::IMeasureReaderPort* reader;
    };

    GraphController::GraphController(UseCase::IReportGraphPort* port, UseCase::IMeasureReaderPort* reader)
        : d{ new Impl } {
        d->outPort = port;
        d->reader = reader;
    }

    GraphController::~GraphController() {

    }

    auto GraphController::Request() -> bool {
        return EXIT_SUCCESS;
    }

    auto GraphController::UpdateDataList(Entity::WorkingSet::Pointer workingset)->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::UpdateDataList();
        return useCase.Request(workingset, d->outPort);
    }

    auto GraphController::UpdateGraph(Entity::WorkingSet::Pointer workingset)->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::DrawGraph();
        return useCase.Request(workingset, d->outPort);
    }
}
