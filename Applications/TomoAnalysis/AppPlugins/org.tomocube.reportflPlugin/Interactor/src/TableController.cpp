#include "TableController.h"

#include <iostream>
#include <QDir>

#include "DrawTable.h"

namespace TomoAnalysis::Report::Interactor {
    struct TableController::Impl {
        UseCase::IReportManagerPort* outPort;
        UseCase::IMeasureReaderPort* reader;
    };

    TableController::TableController(UseCase::IReportManagerPort* port,UseCase::IMeasureReaderPort* reader) : d{ new Impl } {
        d->outPort = port;
        d->reader = reader;
    }
    TableController::~TableController() {
        
    }
    auto TableController::Request() -> bool {
        return EXIT_SUCCESS;
    }
    auto TableController::UpdateResult(const QString& reportPath,Entity::WorkingSet::Pointer workingset) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->reader) {
            return false;
        }
        
        QDir rDir(reportPath);
        if(false == rDir.exists()) {
            return false;
        }
        auto useCase = UseCase::DrawTable();
        return useCase.Request(reportPath,workingset,d->outPort,d->reader);
    }

}