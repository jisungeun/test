#include "GraphPresenter.h"

namespace TomoAnalysis::Report::Interactor {
    struct GraphPresenter::Impl {
        IReportGraphPanel* reportPanel{nullptr};
        IReportControlPanel* controlPanel{nullptr};
    };

    GraphPresenter::GraphPresenter(IReportGraphPanel* report, IReportControlPanel* control) : d{ new Impl } {
        d->reportPanel = report;
        d->controlPanel = control;
    }

    GraphPresenter::~GraphPresenter() {
        
    }

    auto GraphPresenter::UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> void {
        if (d->reportPanel == nullptr) {
            return;
        }

        d->reportPanel->UpdateDataList(cubeDataList);
    }

    auto GraphPresenter::UpdateGraph(Entity::WorkingSet::Pointer workingset) -> void {
        if (d->reportPanel == nullptr) {
            return;
        }

        d->reportPanel->UpdateGraph(workingset);        
    }
}