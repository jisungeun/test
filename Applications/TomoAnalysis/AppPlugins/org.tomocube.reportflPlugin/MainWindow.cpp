#include <iostream>
#include <QMessageBox>

#include <WorkingSet.h>
#include <AppEvent.h>
#include <MenuEvent.h>

#include <MeasureDataReader.h>

#include <TableController.h>
#include <GraphController.h>
#include <ReportPresenter.h>
#include <GraphPresenter.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::Report::AppUI {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        QMap<QString, IParameter::Pointer> share_param;
        Ui::MainWindow* ui{nullptr};

        Entity::WorkingSet::Pointer workingset;
        QVariantMap appProperties;

        bool isTime{ false };
    };

    MainWindow::MainWindow(QWidget* parent)
        : IMainWindowTA("ReportFL", "ReportFL", parent)
        , d{ new Impl }
    {
        d->appProperties["Parent-App"] = "FL Mask Generator";
        d->appProperties["AppKey"] = "ReportFL";
    }

    MainWindow::~MainWindow() {

    }

    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        auto singleRun = false;
        auto batchRun = false;
        return std::make_tuple(singleRun,batchRun);
    }


    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }
    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }
        if (params["ExecutionType"].toString() == "LinkRun") {
            //perform external basic analyzer execution        
            BroadcastRefreshMeasure(params["pg_path"].toString(), params["hyperName"].toString(), params["tcfPath"].toString(), params["userName"].toString());
        }
        else {
            return this->ExecuteUpdateReport(params["resultPath"].toString());
        }
        return true;
    }
    auto MainWindow::ExecuteUpdateReport(const QString& resultPath)->bool {
        d->workingset->ClearMeasure();
        if (resultPath.isEmpty())
            return false;

        std::shared_ptr<Plugins::MeasureDataReader> reader{ new Plugins::MeasureDataReader };
        const std::shared_ptr<Interactor::ReportPresenter> presenter{
            new Interactor::ReportPresenter(d->ui->dataTable)
        };
        Interactor::TableController controller(presenter.get(), reader.get());

        if (false == controller.UpdateResult(resultPath, d->workingset)) {
            QMessageBox::warning(nullptr, "Error", "Failed to update report table");
            return false;
        }

        // update graph ui        
        if (d->isTime) {
            const std::shared_ptr<Interactor::GraphPresenter> graphPresenter{
            new Interactor::GraphPresenter(d->ui->timeReportPanel)
            };

            Interactor::GraphController graphController(graphPresenter.get());
            if (EXIT_FAILURE == graphController.UpdateDataList(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to update graph");
                return false;
            }

            if (EXIT_FAILURE == graphController.UpdateGraph(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to update graph");
                return false;
            }
        }
        else {
            const std::shared_ptr<Interactor::GraphPresenter> graphPresenter{ new Interactor::GraphPresenter(d->ui->graphReportPanel) };

            Interactor::GraphController graphController(graphPresenter.get());
            if (EXIT_FAILURE == graphController.UpdateGraph(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to update graph");
                return false;
            }
        }
    }   
    auto MainWindow::InitUI() -> bool {
        connect(d->ui->dataTable, SIGNAL(dataContainsTimeLapse(bool)), this, SLOT(SelectGraphType(bool)));
               
        connect(d->ui->timeReportPanel, SIGNAL(drawGraph()), this, SLOT(UpdateGraphTime()));
        connect(d->ui->timeReportPanel, SIGNAL(sigEditMask(QString, QString, QString, int)), this, SLOT(EditMask(QString, QString, QString, int)));
        connect(d->ui->timeReportPanel, SIGNAL(graphSelected(QString, QString, QString, int, QString)), d->ui->dataTable, SLOT(OnGraphSelectionTime(QString, QString, QString, int, QString)));                

        d->ui->graphReportPanel->hide();        
        d->ui->timeReportPanel->hide();
        
        return true;
    }
    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        if (d->share_param.contains(name)) {
            return d->share_param[name];
        }
        return nullptr;
    }
    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
        d->share_param[name] = std::make_shared<IParameter>(param->Clone());
    }
    void MainWindow::SelectGraphType(bool isTime) {
        d->isTime = isTime;
        if (isTime) {
            std::cout << "Report Contains Time-laspe" << std::endl;
            d->ui->graphReportPanel->hide();
            d->ui->graphReportPanel->disconnect();
            d->ui->dataTable->disconnect();

            d->ui->timeReportPanel->show();

            connect(d->ui->timeReportPanel, SIGNAL(drawGraph()), this, SLOT(UpdateGraphTime()));
            connect(d->ui->timeReportPanel, SIGNAL(sigEditMask(QString, QString, QString, int)), this, SLOT(EditMask(QString, QString, QString, int)));
            connect(d->ui->timeReportPanel, SIGNAL(graphSelected(QString, QString, QString, int, QString)), d->ui->dataTable, SLOT(OnGraphSelectionTime(QString, QString, QString, int, QString)));
            connect(d->ui->timeReportPanel, SIGNAL(graphDeselected()), d->ui->dataTable, SLOT(OnGraphDeselection()));
            connect(d->ui->dataTable, SIGNAL(tableSelectionTime(QString, QString, QString, int, QString)), d->ui->timeReportPanel, SLOT(SelectionFromTable(QString, QString, QString, int, QString)));
            connect(d->ui->dataTable, SIGNAL(tableDeselectionTime()), d->ui->timeReportPanel, SLOT(OnTableDeselection()));
        }
        else {
            std::cout << "Show Single Graph" << std::endl;
            d->ui->timeReportPanel->hide();
            d->ui->timeReportPanel->disconnect();
            d->ui->dataTable->disconnect();

            d->ui->graphReportPanel->show();

            connect(d->ui->graphReportPanel, SIGNAL(keyChanged()), this, SLOT(UpdateGraph()));
            connect(d->ui->graphReportPanel, SIGNAL(sigCorrection(QString, QString, QString)), this, SLOT(OnMaskCorrection(QString, QString, QString)));
            connect(d->ui->graphReportPanel, SIGNAL(barSelection(QString, QString, QString, QString)), d->ui->dataTable, SLOT(OnGraphSelection(QString, QString, QString, QString)));
            connect(d->ui->dataTable, SIGNAL(tableSelection(QString, QString, QString, int, QString)), d->ui->graphReportPanel, SLOT(SelectionFromTable(QString, QString, QString, int, QString)));
        }
        connect(d->ui->dataTable, SIGNAL(dataContainsTimeLapse(bool)), this, SLOT(SelectGraphType(bool)));
    }
    void MainWindow::UpdateGraphTime() {
        const std::shared_ptr<Interactor::GraphPresenter> graphPresenter{
            new Interactor::GraphPresenter(d->ui->timeReportPanel)
        };

        Interactor::GraphController graphController(graphPresenter.get());
        if (EXIT_FAILURE == graphController.UpdateGraph(d->workingset)) {
            QMessageBox::warning(nullptr, "Error", "Failed to update graph");
            return;
        }
    }
    void MainWindow::UpdateGraph() {
        const std::shared_ptr<Interactor::GraphPresenter> graphPresenter{
            new Interactor::GraphPresenter(d->ui->graphReportPanel)
        };

        Interactor::GraphController graphController(graphPresenter.get());
        if (EXIT_FAILURE == graphController.UpdateGraph(d->workingset)) {
            QMessageBox::warning(nullptr, "Error", "Failed to update graph");
            return;
        }
    }
    void MainWindow::OnMaskCorrection(QString pg_path, QString cubeName, QString fileName) {        
        BroadcastMaskEditor(pg_path, cubeName, fileName);
    }

    void MainWindow::EditMask(QString playgroundPath, QString cubeName, QString fileName, int time_step) {
        BroadcastMaskEditor(playgroundPath, cubeName, fileName, time_step);
    }

    auto MainWindow::TryDeactivate() -> bool {
        //TODO clean part
        return true;
    }
    auto MainWindow::TryActivate() -> bool {
        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        d->workingset = std::make_shared<Entity::WorkingSet>();

        InitUI();

        //start global communication
        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnTabFocused(ctkEvent)));
        return true;
    }
    auto MainWindow::IsActivate() -> bool {        
        return (nullptr != d->ui);
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::GetFeatureName() const -> QString {
	    return "org.tomocube.reportflPlugin";
    }

    auto MainWindow::OnAccepted() -> void {
    }

    auto MainWindow::OnRejected() -> void {
    }

    void MainWindow::OnTabFocused(ctkEvent ctkEvent) {
        if (ctkEvent.getProperty("TabName").isValid()) {
            auto TabName = ctkEvent.getProperty("TabName").toString();
            if (TabName.compare("Report") == 0) {
                auto tcf_path = QString();
                TC::Framework::MenuEvent menuEvt(MenuTypeEnum::TitleBar);
                menuEvt.scriptSet(tcf_path);
                publishSignal(menuEvt, "Report");
            }
        }
    }
    auto MainWindow::BroadcastMaskEditor(QString pg_path, QString cubeName, QString fileName,int time_step) -> void {
        TC::Framework::AppEvent appEvent(AppTypeEnum::APP_WITH_SHARE);
        appEvent.setFullName("org.tomocube.intersegPlugin");
        appEvent.setAppName("Mask Editor");
        appEvent.addParameter("ExecutionType", "LinkRun");
        appEvent.addParameter("PGPath", pg_path);
        appEvent.addParameter("CubeName", cubeName);
        appEvent.addParameter("FileName", fileName);
        if (time_step > -1) {
            appEvent.addParameter("TimeStep", QString::number(time_step));
        }
        appEvent.addParameter("SenderFull", "org.tomocube.reportflPlugin");
        appEvent.addParameter("SenderName", "ReportFL");
        
        publishSignal(appEvent, "ReportFL");
    }
    auto MainWindow::BroadcastRefreshMeasure(QString pg_path, QString hyperName, QString fileName, QString userName) -> void {
        std::cout << "deprecated signal" << std::endl;
        TC::Framework::AppEvent appEvent(AppTypeEnum::APP_UPDATE);
        appEvent.setFullName("org.tomocube.basicanalysisPlugin");
        appEvent.setAppName("Basic Analyzer");
        appEvent.addParameter("PGPath", pg_path);
        appEvent.addParameter("HyperName", hyperName);
        appEvent.addParameter("TCFPath", fileName);
        appEvent.addParameter("UserName", userName);
        appEvent.addParameter("ExecutionType", "Update");
        appEvent.addParameter("UpdateType", "BatchRun");
        publishSignal(appEvent, "Report");
    }
}
