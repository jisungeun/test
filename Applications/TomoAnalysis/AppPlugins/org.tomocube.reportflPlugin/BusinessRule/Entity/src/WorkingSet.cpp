#include "WorkingSet.h"

namespace TomoAnalysis::Report::Entity {
    struct WorkingSet::Impl {
        QStringList organ_names;
        //Key: cube name
        QMap<QString, MeasureNode> reportList;
        QString pg_path;
        QStringList cube_names;
    };
    WorkingSet::WorkingSet() : d{ new Impl } {
        
    }
    WorkingSet::~WorkingSet() {
        
    }
    auto WorkingSet::GetPlayground() -> QString {
        return d->pg_path;
    }
    auto WorkingSet::SetPlayground(QString path) -> void {
        d->pg_path = path;
    }

    auto WorkingSet::AppendCubeSet(const QString& cubeName, MeasureNode node) -> void {
        d->reportList[cubeName] = node;
        d->cube_names.append(cubeName);
    }    
    auto WorkingSet::GetCubeSet(const QString& cubeName) -> MeasureNode {
        if(d->reportList.contains(cubeName)) {
            return d->reportList[cubeName];
        }
        return MeasureNode();
    }
    auto WorkingSet::GetCubeNames() -> QStringList {
        //return d->reportList.keys();
        return d->cube_names;
    }

    auto WorkingSet::ClearMeasure() -> void {
        d->reportList.clear();
        d->cube_names.clear();
    }

    auto WorkingSet::GetOrganNames() -> QStringList {
        return d->organ_names;
    }
    auto WorkingSet::SetOrganNames(QStringList org_names) -> void {
        d->organ_names = org_names;
    }

}