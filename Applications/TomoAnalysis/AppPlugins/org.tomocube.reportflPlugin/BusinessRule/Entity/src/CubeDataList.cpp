#include "CubeDataList.h"

namespace TomoAnalysis::Report::Entity {
    struct CubeDataList::Impl {
        QMap<QString, QStringList> data;
    };

    CubeDataList::CubeDataList() : d{ new Impl } {
        
    }

    CubeDataList::~CubeDataList() {
        
    }

    auto CubeDataList::Insert(const QString& cubeName, const QStringList& list) -> void {
        d->data.insert(cubeName, list);
    }

    auto CubeDataList::Keys()->QStringList {
        return d->data.keys();
    }

    auto CubeDataList::Value(const QString& key)->QStringList {
        return d->data.value(key);
    }
}