#include <QMap>

#include "Table.h"

namespace TomoAnalysis::Report::Entity {
    struct Table::Impl {
        //TCFDir list  
    };
    Table::Table() : d{ new Impl } {

    }
    Table::Table(const Table& other) : d{ new Impl } {
        *d = *other.d;
    }
    Table::~Table() {

    }
    auto Table::Init() -> void {

    }
}