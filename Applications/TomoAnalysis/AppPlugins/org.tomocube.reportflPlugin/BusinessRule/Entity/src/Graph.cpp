#include <QMap>

#include "Graph.h"

namespace TomoAnalysis::Report::Entity {
    struct Graph::Impl {
        //TCFDir list  
    };
    Graph::Graph() : d{ new Impl } {

    }
    Graph::Graph(const Graph& other) : d{ new Impl } {
        *d = *other.d;
    }
    Graph::~Graph() {

    }
    auto Graph::Init() -> void {

    }
}