#pragma once

#include <memory>

#include "ReportEntityExport.h"

namespace TomoAnalysis::Report::Entity {
	class ReportEntity_API Graph {
	public:
		typedef Graph Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		Graph();
		Graph(const Graph& other);
		virtual ~Graph();

		auto Init()->void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}