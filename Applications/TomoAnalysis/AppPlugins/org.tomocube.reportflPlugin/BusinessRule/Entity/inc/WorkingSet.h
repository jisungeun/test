#pragma once

#include <memory>
#include <QMap>
#include <QString>

#include <TCMeasure.h>

#include "ReportEntityExport.h"

namespace TomoAnalysis::Report::Entity {
    typedef struct cubeMeasure{
        QString parentDir;
        QMap<QString,QList<TCMeasure::Pointer>> measure;
    }MeasureNode;
    class ReportEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;

        WorkingSet();
        virtual ~WorkingSet();

        auto AppendCubeSet(const QString& cubeName,MeasureNode node)->void;        
        auto GetCubeSet(const QString& cubeName)->MeasureNode;
        auto GetCubeNames()->QStringList;

        auto SetOrganNames(QStringList org_names)->void;
        auto GetOrganNames()->QStringList;

        auto SetPlayground(QString path)->void;
        auto GetPlayground()->QString;

        auto ClearMeasure()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}