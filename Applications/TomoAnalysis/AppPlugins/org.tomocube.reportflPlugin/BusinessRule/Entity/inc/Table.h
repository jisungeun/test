#pragma once

#include <memory>

#include "ReportEntityExport.h"

namespace TomoAnalysis::Report::Entity {
	class ReportEntity_API Table {
	public:
		typedef Table Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		Table();
		Table(const Table& other);
		virtual ~Table();

		auto Init()->void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}