#pragma once

#include <memory>
#include <QMap>
#include <QString>

#include "ReportEntityExport.h"

namespace TomoAnalysis::Report::Entity {
    class ReportEntity_API CubeDataList {
    public:
        typedef CubeDataList Self;
        typedef std::shared_ptr<Self> Pointer;

        CubeDataList();
        virtual ~CubeDataList();

        auto Insert(const QString& cubeName, const QStringList& list) -> void;
        auto Keys()->QStringList;
        auto Value(const QString& key)->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}