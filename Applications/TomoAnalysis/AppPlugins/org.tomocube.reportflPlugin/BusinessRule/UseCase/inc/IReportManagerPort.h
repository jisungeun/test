#pragma once

#include <WorkingSet.h>

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
    class ReportUseCase_API IReportManagerPort {
    public:
        IReportManagerPort();
        virtual ~IReportManagerPort();

        virtual auto Update(Entity::WorkingSet::Pointer workingset)->void = 0;
    };
}