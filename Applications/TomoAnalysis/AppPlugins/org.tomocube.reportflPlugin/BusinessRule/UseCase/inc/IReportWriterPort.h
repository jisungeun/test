#pragma once

#include <QString>

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
    class ReportUseCase_API IReportWriterPort {
    public:
        IReportWriterPort();
        virtual ~IReportWriterPort();

        virtual auto Wrtie()->bool = 0;
    };
}