#pragma once

#include <QString>
#include <TCMeasure.h>

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
    class ReportUseCase_API IMeasureReaderPort {
    public:
        IMeasureReaderPort();
        virtual ~IMeasureReaderPort();

        virtual auto GetTimeStep(const QString& path)->int = 0;
        virtual auto GetTimePoints(const QString& path)->QList<int> = 0;
        virtual auto GetTimePoint(const QString& path, int time_step)->double=0;
        virtual auto Read(const QString& path, int time_step)->TCMeasure::Pointer = 0;
    };
}