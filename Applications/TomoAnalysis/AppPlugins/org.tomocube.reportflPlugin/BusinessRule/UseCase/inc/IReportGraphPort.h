#pragma once

#include <CubeDataList.h>
#include <WorkingSet.h>

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
    class ReportUseCase_API IReportGraphPort {
    public:
        IReportGraphPort();
        virtual ~IReportGraphPort();

        virtual auto UpdateDataList(Entity::CubeDataList::Pointer cubeDataList)->void = 0;
        virtual auto UpdateGraph(Entity::WorkingSet::Pointer workingset)->void = 0;
    };
}