#pragma once

#include <memory>

#include <WorkingSet.h>

#include "IReportManagerPort.h"
#include "IMeasureReaderPort.h"

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
	class ReportUseCase_API DrawTable {
	public:
		DrawTable();
		virtual ~DrawTable();

		auto Request(const QString& resultPath,Entity::WorkingSet::Pointer workingset,IReportManagerPort* port, IMeasureReaderPort* reader)->bool;

	private:

		auto DateTimeToMsec(QString datetime)->long long;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}