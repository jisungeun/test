#pragma once

#include <memory>

#include <WorkingSet.h>
#include "IReportGraphPort.h"
#include "IMeasureReaderPort.h"

#include "ReportUseCaseExport.h"

namespace TomoAnalysis::Report::UseCase {
	class ReportUseCase_API DrawGraph {
	public:
		DrawGraph();
		virtual ~DrawGraph();

		auto Request(Entity::WorkingSet::Pointer workingset, IReportGraphPort* port)->bool;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}