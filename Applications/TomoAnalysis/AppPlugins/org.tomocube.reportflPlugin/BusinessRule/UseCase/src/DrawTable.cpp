#include <iostream>
#include <QDir>
#include <QDirIterator>

#include "DrawTable.h"
#include <ProjectDataReader.h>

#include <QDateTime>


namespace TomoAnalysis::Report::UseCase {
    struct DrawTable::Impl {
        
    };
    DrawTable::DrawTable() : d{ new Impl } {
        
    }
    DrawTable::~DrawTable() {
        
    }
    auto DrawTable::DateTimeToMsec(QString datetime) -> long long {
        auto fsp = datetime.split(" ");
        const auto datepart = fsp[0];
        const auto timepart = fsp[1];
        auto dsp = datepart.split("-");
        auto tsp = timepart.split(".");

        const auto year = dsp[2].toInt(); 
        const auto month = dsp[1].toInt();
        const auto day = dsp[0].toInt();

        const auto hour = tsp[0].toInt();
        const auto minute = tsp[1].toInt();
        const auto second = tsp[2].toInt();
        const auto msec = tsp[3].toInt();

        const auto dayTotal = year * 365 + month * 30 + day;        
        const auto timeTotal = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000 + msec;
        const long long msecTotal = static_cast<long long>(dayTotal) * 24 * 60 * 60 * 1000 + timeTotal;                

        return msecTotal;
    }
    struct cubeNameInfo {
        QString cubeName;
        QString cubeDir;
        long long creationTime;
    };
    bool variantLessThan(const cubeNameInfo& v1, const cubeNameInfo& v2)
    {
        return v1.creationTime < v2.creationTime;
    }
    auto DrawTable::Request(const QString& resultPath,Entity::WorkingSet::Pointer workingset, IReportManagerPort* port, IMeasureReaderPort* reader) -> bool {
        //parse cube names        
                
        QStringList cubeDir;
        
        QList<cubeNameInfo> cubeInfo;                
        auto split = resultPath.split("/");
        auto timeStamp = split[split.size() - 1];
        auto hyper = split[split.size() - 2];
        QDir dir(resultPath);
        dir.cdUp();
        dir.cdUp();
        auto pg_path = dir.path() + "/" + hyper + "_" + timeStamp + ".tcpg";
        workingset->SetPlayground(pg_path);        

        auto preader = std::make_shared<ProjectDataReader>();
        auto pinfo = std::make_shared<PlaygroundInfo>();
        preader->ReadPlaygroundData(pg_path, pinfo);
        
        auto hypercube = pinfo->FindHypercube(hyper);
        if (nullptr == hypercube) {
            return false;
        }
        
        auto cubeList = hypercube->GetCubeList();        
        
        QDirIterator it(resultPath, QDir::Dirs|QDir::NoDotAndDotDot|QDir::NoSymLinks);
        while(it.hasNext()){
            auto cc = it.next();            
            QDir cDir(cc);
            QFileInfo cinfo(cc);
            auto cube = hypercube->FindCube(cDir.dirName());
            if (nullptr == cube) {
                return false;
            }
            cubeNameInfo info;
            info.cubeDir = cc;
            info.creationTime = cube->GetID();
            info.cubeName = cube->GetName();
            cubeInfo.append(info);            
        }

        QStringList cubeName;
        std::sort(cubeInfo.begin(),cubeInfo.end(), variantLessThan);
        for(auto i=0;i< cubeInfo.count();i++) {
            cubeName.append(cubeInfo[i].cubeName);
            cubeDir.append(cubeInfo[i].cubeDir);
        }

        QStringList organNames;
        //fill workingset with measure information
        for(auto i=0; i<cubeName.count();i++) {
            //parse every *rep files and read them            
            Entity::MeasureNode node;
            node.parentDir = cubeDir[i];
            QDirIterator repIt(cubeDir[i],QStringList()<<"*.rep", QDir::Files);
            while(repIt.hasNext()) {
                auto repDir = repIt.next();                
                //auto time_steps = reader->GetTimeStep(repDir);
                auto time_point = reader->GetTimePoints(repDir);                
                QList<TCMeasure::Pointer> single_measure;
                //for(auto t =0 ;t<time_steps;t++) {
                for(auto t: time_point){
                    auto real_point = reader->GetTimePoint(repDir, t);
                    auto measure = reader->Read(repDir, t);                    
                    measure->SetTimeIndex(t);                    
                    measure->SetTimePoint(real_point);
                    auto orgs = measure->GetOrganNames();
                    if (organNames.count() < orgs.count()) {
                        organNames = orgs;
                    }
                    single_measure.push_back(measure);
                }
                QFileInfo fileInfo(repDir);
                node.measure[fileInfo.fileName()] = single_measure;
            }
            workingset->AppendCubeSet(cubeName[i], node);
        }        
        workingset->SetOrganNames(organNames);

        //update table with current workingset
        port->Update(workingset);

        return true;
    }
}
