#include "DrawGraph.h"

namespace TomoAnalysis::Report::UseCase {
    struct DrawGraph::Impl {

    };
    DrawGraph::DrawGraph() : d{ new Impl } {

    }
    DrawGraph::~DrawGraph() {

    }
        
    auto DrawGraph::Request(Entity::WorkingSet::Pointer workingset, IReportGraphPort* port) -> bool {
        port->UpdateGraph(workingset);
        return EXIT_SUCCESS;
    }
}
