#include <iostream>

#include <CubeDataList.h>

#include "UpdateDataList.h"

const QString kReportExtension = ".rep";

namespace TomoAnalysis::Report::UseCase {
    struct UpdateDataList::Impl {

    };
    UpdateDataList::UpdateDataList() : d{ new Impl } {

    }
    UpdateDataList::~UpdateDataList() {

    }

    auto UpdateDataList::Request(Entity::WorkingSet::Pointer workingset, IReportGraphPort* port) -> bool {
        if (port == nullptr) {
            return false;
        }

        // get tcf file names of each cube from working set
        Entity::CubeDataList::Pointer cubeDataList = std::make_shared<Entity::CubeDataList>();

        auto cubeNames = workingset->GetCubeNames();
        for (auto cubeName : cubeNames) {
            auto cubeSet = workingset->GetCubeSet(cubeName);
            if (!cubeSet.measure.isEmpty()) {
                QStringList filenameList;

                auto keys = cubeSet.measure.keys();
                for (auto key : keys) {
                    auto reportFilename = key.left(key.length() - kReportExtension.length());
                    filenameList << reportFilename;
                }

                cubeDataList->Insert(cubeName, filenameList);
            }
        }

        port->UpdateDataList(cubeDataList);

        return EXIT_SUCCESS;
    }
}
