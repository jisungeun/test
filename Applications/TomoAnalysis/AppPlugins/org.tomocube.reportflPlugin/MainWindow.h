#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::Report::AppUI {
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto Execute(const QVariantMap& params)->bool override;
        auto GetRunType() -> std::tuple<bool, bool> override;
        auto TryDeactivate() -> bool final;
        auto TryActivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

    protected:
        virtual void resizeEvent(QResizeEvent* event);

    protected slots:
        void UpdateGraph();
        void UpdateGraphTime();
        void OnMaskCorrection(QString, QString, QString);
        void EditMask(QString, QString, QString, int);
        void OnTabFocused(ctkEvent ctkEvent);
        void SelectGraphType(bool isTime);

    private:
        auto InitUI(void)->bool;
        auto BroadcastMaskEditor(QString, QString, QString,int time_step =-1)->void;
        auto BroadcastRefreshMeasure(QString, QString, QString, QString)->void;
        auto ExecuteUpdateReport(const QString& resultPath)->bool;
    
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}