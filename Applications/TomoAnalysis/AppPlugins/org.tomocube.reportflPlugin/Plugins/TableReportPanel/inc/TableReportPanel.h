#pragma once

#include <memory> 

#include <QWidget>
#include <QLayout>
#include <QTableWidgetItem>
#include <QStandardItem>

#include <IReportPanel.h>

#include "TableReportPanelExport.h"


namespace TomoAnalysis::Report::Plugins {
    class DoubleTableItem : public QStandardItem {
    public:
        DoubleTableItem(const QString& text):QStandardItem(text) {
            
        }
        ~DoubleTableItem() = default;
        bool operator< (const QStandardItem& other)const {
            double valA = 0;
            double valB = 0;
            if (this->text() == "N/A")
                valA = -1;
            else
                valA = this->text().toDouble();
            if (other.text() == "N/A")
                valB = -1;
            else
                valB = other.text().toDouble();

            return valA < valB;
        }
    };
    class LabelTableItem : public QStandardItem {
    public:
        LabelTableItem(const QString& text) : QStandardItem(text) {
            
        }
        ~LabelTableItem() = default;
        bool operator<(const QStandardItem& other)const {
            auto strA = this->text().replace('_', '.');
            auto strB = other.text().replace('_', '.');
            auto valA = strA.toDouble();
            auto valB = strB.toDouble();
            return valA < valB;
        }
    };
    class TableReportPanel_API TableReportPanel : public QWidget, public Interactor::IReportPanel {
        Q_OBJECT
    public:
        typedef TableReportPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        TableReportPanel(QWidget* parent = nullptr);
        virtual ~TableReportPanel();

        auto Init()->void;
        auto Update(Entity::WorkingSet::Pointer workingset) -> bool override;

    signals:
        void tableSelection(QString, QString,QString, int,QString);
        void tableSelectionTime(QString, QString,QString, int, QString);
        void tableDeselectionTime();
        void dataContainsTimeLapse(bool);

    private:
        auto Clear() -> void;
        auto GetTableHorizontalLabels(const Entity::MeasureNode* cubeSet) -> QStringList;
        auto DataToPresentation(QString data)->QString;
        auto PresentationToData(QString presnetation)->QString;

    protected slots:
        void OnPreviousTable();
        void OnNextTable();
        void OnRowSelected(QString,QString,QString, int,QString);
        void OnRowSelectedTime(QString, QString, QString, int, QString);
        void OnGraphSelection(QString,QString,QString,QString);
        void OnGraphSelectionTime(QString, QString, QString, int,QString);
        void OnGraphDeselection();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}