#pragma once

#include <memory>
#include <QWidget>
#include <QTableWidget>

namespace TomoAnalysis::Report::Plugins {
    class InfoPanel : public QWidget {
        Q_OBJECT
    public:
        explicit InfoPanel(QWidget* parent = nullptr);
        virtual ~InfoPanel();

        auto Clear()->void;
        auto SetItems(QMap<QString,QList<int>> naList)->void;

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}