#pragma once

#include <memory>

#include <QStandardItemModel>
#include <QWidget>

#include "TableReportPanelExport.h"

namespace TomoAnalysis::Report::Plugins {
    class TableReportPanel_API TableItemWidget : public QWidget {
        Q_OBJECT
    public:
        typedef TableItemWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        TableItemWidget(QString name, QStandardItemModel* model, QWidget* parent = nullptr);
        virtual ~TableItemWidget();                

        auto ClearSelection()->void;
        auto GetName()->QString;
        auto SetSelection(QString fileName, QString cell_idx,QString organName)->void;
        auto SetSelectionTime(QString fileName, QString cell_idx, int time_step, QString organName)->void;

    signals:
        void rowSelected(QString cubeName,QString filename,QString cell_idx,int time_step,QString organName);
        void rowSelectedTime(QString cubeName, QString filename, QString cell_idx, int time_step,QString organName);

    protected slots:
        void OnExport();
        void OnPressed(const QModelIndex& index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
