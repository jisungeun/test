#include "ui_InfoPanel.h"
#include "InfoPanel.h"

namespace TomoAnalysis::Report::Plugins {
    struct InfoPanel::Impl {
        Ui::Info* ui{ nullptr };

        QStringList headers;
    };
    InfoPanel::InfoPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::Info();
        d->ui->setupUi(this);
        Init();
    }
    InfoPanel::~InfoPanel() = default;
    auto InfoPanel::Init() -> void {        
        setWindowTitle("SEGMENTATION FAULTS");
        d->ui->infoLabel->setObjectName("h6");
        d->headers.push_back("File path");
        d->headers.push_back("Time step");
        d->ui->infoTable->setProperty("styleVariant", 1);
        d->ui->infoTable->setColumnCount(d->headers.count());
        d->ui->infoTable->setHorizontalHeaderLabels(d->headers);
        d->ui->infoTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);
        d->ui->infoTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeMode::Stretch);
        d->ui->infoTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    }

    auto InfoPanel::Clear() -> void {
        d->ui->infoTable->clearContents();
    }

    auto InfoPanel::SetItems(QMap<QString, QList<int>> naList) -> void {
        auto keys = naList.keys();
        d->ui->infoTable->setRowCount(keys.count());
        for(auto i=0;i<keys.count();i++) {
            d->ui->infoTable->setItem(i, 0, new QTableWidgetItem(keys[i]));
            auto time_steps = naList[keys[i]];
            QString time_list = QString();
            for(auto j = 0; j< time_steps.count() -1; j++) {
                auto time = time_steps[j];
                time_list += QString::number(time+1);
                time_list += ",";
            }
            time_list += QString::number(time_steps[time_steps.count() - 1]+1);

            d->ui->infoTable->setItem(i, 1, new QTableWidgetItem(time_list));
        }
    }
}