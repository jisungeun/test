#include <iostream>

#include <QFileDialog>
#include <QTextStream>
#include <QDateTime>
#include <QStandardItemModel>

#include "TableItemWidget.h"
#include "InfoPanel.h"

#include "ui_TableReportPanel.h"
#include "TableReportPanel.h"

namespace TomoAnalysis::Report::Plugins {
    struct TableReportPanel::Impl {
        Ui::TableReport* ui{nullptr};

        QList<TableItemWidget*> tableItems;
        QMap<QString, QStringList> fullNames;
        int viewingIndex{ 0 }; 
        InfoPanel* infoPanel{nullptr};

        QString cur_cubeName;
        QString cur_shortName;
        QString cur_cellID;
        int cur_time_step{ -1 };
        QString cur_organ_name;
    };

    TableReportPanel::TableReportPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui = new Ui::TableReport;
        d->ui->setupUi(this);

        connect(d->ui->previousButton, SIGNAL(clicked()), this, SLOT(OnPreviousTable()));
        connect(d->ui->nextButton, SIGNAL(clicked()), this, SLOT(OnNextTable()));

        d->ui->previousButton->setObjectName("bt-arrow-left");
        d->ui->nextButton->setObjectName("bt-arrow-right");

        d->infoPanel = new InfoPanel;
        Init();
    }

    TableReportPanel::~TableReportPanel() {
        
    }

    auto TableReportPanel::Init() -> void {
        
    }

    auto TableReportPanel::Update(Entity::WorkingSet::Pointer workingset) -> bool {        
        Clear();

        auto cubeNames = workingset->GetCubeNames();
        if (cubeNames.isEmpty()) {
            return false;
        }
        bool isTime = false;
        // build tables
        Entity::MeasureNode mn;
        bool foundMeausre = false;
        //auto mn = workingset->GetCubeSet(cubeNames[0]);
        for(auto i=0;i<cubeNames.count();i++) {
            mn = workingset->GetCubeSet(cubeNames[i]);
            if(mn.measure.count()>0) {
                foundMeausre = true;
                break;
            }
        }
        if(false == foundMeausre) {
            return false;
        }
        auto headers = GetTableHorizontalLabels(&mn);

        for (auto cubeName : cubeNames) {
            auto model = new QStandardItemModel;
            model->setHorizontalHeaderLabels(headers);

            auto cubeSet = workingset->GetCubeSet(cubeName);
            auto fns = cubeSet.measure.keys();

            auto rowIdx = 0;            
            for (auto f = 0; f < fns.size(); f++) {
                auto fname = fns[f];                                

                auto singleFile = cubeSet.measure[fname];
                if (false == isTime) {
                    if (singleFile.count() > 1) {
                        isTime = true;
                        break;
                    }
                }
            }
        }

        QMap<QString, QList<int>> naList;

        for (auto cubeName : cubeNames) {
            auto model = new QStandardItemModel;
            model->setHorizontalHeaderLabels(headers);

            auto cubeSet = workingset->GetCubeSet(cubeName);
            auto fns = cubeSet.measure.keys();

            auto rowIdx = 0;
            QStringList fullNameList;
            for (auto f = 0; f < fns.size(); f++) {
                auto fname = fns[f];

                // get date and simple name
                QString date, simpleName;
                auto splitResult = fname.split(".");
                fullNameList.push_back(fname);
                if (splitResult.count() == 5) {
                    date = QDateTime::fromString(splitResult.first(), "yyyyMMdd").toString("yyyy.MM.dd");
                    simpleName = splitResult.at(splitResult.count() - 2);
                }
                else {
                    simpleName = fname;
                }

                auto singleFile = cubeSet.measure[fname];                 
                for (auto singleMeasure : singleFile) {
                    bool isNA = true;
                    auto organs = singleMeasure->GetOrganNames();
                    auto ccount = singleMeasure->GetCellCount();
                    auto measurekey = singleMeasure->GetKeys();
                    auto ts = singleMeasure->GetTimeIndex();
                    
                    for (auto o = 0; o < organs.size(); o++) {
                        for (auto c = 0; c < ccount; c++) {
                            int columnIdx = 0;
                            auto simplef = new QStandardItem(simpleName);
                            simplef->setWhatsThis(fname);

                            model->setItem(rowIdx, columnIdx++, new QStandardItem(date));   // date
                            model->setItem(rowIdx, columnIdx++, simplef); // file name
                            if (isTime) {
                                model->setItem(rowIdx, columnIdx++, new DoubleTableItem(QString::number(ts + 1)));    // time step
                            }                            
                            model->setItem(rowIdx, columnIdx++, new LabelTableItem(QString("%1_%2").arg(f + 1).arg(c + 1))); // cell idx
                            model->setItem(rowIdx, columnIdx++, new QStandardItem(DataToPresentation(organs[o]))); // organ name

                            for (auto k = 0; k < measurekey.size(); k++) {
                                auto final_key = organs[o] + "." + measurekey[k];
                                auto val = singleMeasure->GetMeasure(final_key, c);
                                if (val < 0) {
                                    model->setItem(rowIdx, columnIdx++, new DoubleTableItem(QString("N/A")));
                                }
                                else {
                                    isNA = false;
                                    if (measurekey[k].compare("MeanRI", Qt::CaseInsensitive) == 0) {
                                        val = round(val * 10000) / 10000;
                                    }
                                    else {
                                        val = round(val * 100) / 100;
                                    }                                                                        
                                    model->setItem(rowIdx, columnIdx++, new DoubleTableItem(QString::number(val)));
                                }
                            }
                            rowIdx++;
                        }
                    }
                    if (isNA) {
                    //if(true){
                        if(!naList.contains(fname)) {
                            naList[fname] = QList<int>();
                        }
                        naList[fname].push_back(ts);                        
                    }
                }                
            }
            d->fullNames[cubeName] = fullNameList;
            auto item = new TableItemWidget(cubeName, model);
            connect(item, SIGNAL(rowSelected(QString,QString,QString,int,QString)), this, SLOT(OnRowSelected(QString,QString,QString,int,QString)));
            connect(item, SIGNAL(rowSelectedTime(QString, QString, QString, int, QString)), this, SLOT(OnRowSelectedTime(QString, QString, QString, int, QString)));

            d->ui->tableItemLayout->addWidget(item);
            d->tableItems << item;

            if (d->ui->tableItemLayout->count() > 2) {
                item->hide();
            }
        }
        if(naList.count()>0) {
            d->infoPanel->SetItems(naList);
            d->infoPanel->showMaximized();
        }
        
        emit dataContainsTimeLapse(isTime);
        
        return true;
    }
    void TableReportPanel::OnRowSelected(QString cubeName, QString shortName,QString cellID, int time_step,QString organ_name) {
        auto senderItem = qobject_cast<TableItemWidget*>(sender());
        for(auto tableItem : d->tableItems) {
            if(tableItem != senderItem) {
                tableItem->ClearSelection();
            }
        }
        emit tableSelection(cubeName,shortName,cellID,time_step,organ_name);
    }
    void TableReportPanel::OnRowSelectedTime(QString cubeName, QString shortName, QString cellID, int time_step, QString organ_name) {
        if (d->cur_cubeName == cubeName && d->cur_shortName == shortName && d->cur_cellID == cellID && d->cur_time_step == time_step && d->cur_organ_name == organ_name) {
            for (auto tableItem : d->tableItems) {
                tableItem->ClearSelection();
            }
            d->cur_cubeName = QString();
            d->cur_shortName = QString();
            d->cur_cellID = QString();
            d->cur_time_step = -1;
            d->cur_organ_name = QString();
            emit tableDeselectionTime();
        }
        else {
            auto senderItem = qobject_cast<TableItemWidget*>(sender());
            for (auto tableItem : d->tableItems) {
                if (tableItem != senderItem) {
                    tableItem->ClearSelection();
                }
            }
            d->cur_cubeName = cubeName;
            d->cur_shortName = shortName;
            d->cur_cellID = cellID;
            d->cur_time_step = time_step;
            d->cur_organ_name = organ_name;
            emit tableSelectionTime(cubeName, shortName, cellID, time_step, organ_name);
        }
    }

    void TableReportPanel::OnGraphDeselection() {
        for(auto tableItem: d->tableItems) {
            tableItem->ClearSelection();
        }
        d->cur_cubeName = QString();
        d->cur_shortName = QString();
        d->cur_cellID = QString();
        d->cur_time_step = -1;
        d->cur_organ_name = QString();
    }

    void TableReportPanel::OnGraphSelectionTime(QString cubeName, QString imageName, QString cellID, int time_step,QString organ_name) {        
        for(auto tableItem : d->tableItems) {
            tableItem->ClearSelection();
        }
        for(auto tableItem : d->tableItems) {
            if(tableItem->GetName() == cubeName) {                
                tableItem->SetSelectionTime(imageName, cellID, time_step + 1, organ_name);
                break;
            }
        }
        d->cur_cubeName = cubeName;
        d->cur_shortName = imageName;
        d->cur_cellID = cellID;
        d->cur_time_step = time_step;
        d->cur_organ_name = organ_name;
    }

    void TableReportPanel::OnGraphSelection(QString cubeName,QString imageName,QString cellID,QString organ_name) {
        for (auto tableItem : d->tableItems) {
            tableItem->ClearSelection();            
        }        
        for(auto tableItem : d->tableItems) {
            if(tableItem->GetName() == cubeName) {
                tableItem->SetSelection(imageName, cellID,organ_name);
                break;
            }
        }
    }


    auto TableReportPanel::Clear() -> void {
        for (auto itemWidget : d->tableItems) {
            d->ui->tableItemLayout->removeWidget(itemWidget);
        }

        qDeleteAll(d->tableItems);
        d->tableItems.clear();

        d->viewingIndex = 0;

        d->fullNames.clear();

        d->infoPanel->Clear();
        d->infoPanel->hide();
    }

    auto TableReportPanel::DataToPresentation(QString tdata) -> QString {
        if(tdata == "membrane") {
            return QString("Whole Cell");
        }
        if(tdata == "lipid droplet") {
            return QString("Lipid droplet");
        }
        if(tdata == "nucleus") {
            return QString("Nucleus");
        }
        if(tdata =="nucleoli") {
            return QString("Nucleolus");
        }
        return tdata;
    }
    auto TableReportPanel::PresentationToData(QString presentation) -> QString {
        if(presentation == "Whole Cell") {
            return QString("membrane");
        }
        if(presentation == "Lipid droplet") {
            return QString("lipid droplet");
        }
        if(presentation == "Nucleus") {
            return QString("nucleus");
        }
        if(presentation == "Nucleolus") {
            return QString("nucleoli");
        }
        return presentation;
    }


    auto TableReportPanel::GetTableHorizontalLabels(const Entity::MeasureNode* cubeSet)->QStringList {
        QStringList labels;

        auto fns = cubeSet->measure.keys();
        auto sampleMeasure = cubeSet->measure[fns[0]][0];

        bool hasMultipleTimePoint = false;
        if (cubeSet->measure[fns[0]].size() > 1) {
            hasMultipleTimePoint = true;
        }

        labels << "Date" << "Name";
        if (hasMultipleTimePoint) {
            labels << "Time step";
        }
        labels << "Cell #" << "Organelle";

        auto measurekey = sampleMeasure->GetKeys();
        for (auto key : measurekey) {
            if (key.contains("Volume")) {
                labels << key + QString("(%1m%2)")
                    .arg(QString::fromWCharArray(L"\x00B5"))
                    .arg(QString::fromWCharArray(L"\x00B3"));
            } else if (key.contains("SurfaceArea") || key.contains("ProjectedArea")) {
                labels << key + QString("(%1m%2)")
                    .arg(QString::fromWCharArray(L"\x00B5"))
                    .arg(QString::fromWCharArray(L"\x00B2"));
            } else if (key.contains("Concentration")) {
                labels << key + QString("(pg/%1m%2)")
                    .arg(QString::fromWCharArray(L"\x00B5"))
                    .arg(QString::fromWCharArray(L"\x00B3"));
            } else if (key.contains("Dry mass")) {
                labels << key + "(pg)";
            } else {
                labels << key;
            }
        }

        return labels;
    }

    void TableReportPanel::OnPreviousTable() {
        if (d->viewingIndex == 0) {
            return;
        }

        if (d->viewingIndex + 1 < d->tableItems.size()) {
            d->tableItems.at(d->viewingIndex + 1)->hide();
        }

        if (d->viewingIndex - 1 >= 0) {
            d->tableItems.at(d->viewingIndex - 1)->show();
        }

        d->viewingIndex--;
    }

    void TableReportPanel::OnNextTable() {
        if (d->viewingIndex == d->tableItems.size() - 1) {
            return;
        }

        d->tableItems.at(d->viewingIndex)->hide();

        if (d->viewingIndex + 2 < d->tableItems.size()) {
            d->tableItems.at(d->viewingIndex + 2)->show();
        }

        d->viewingIndex++;
    }
}