#include <iostream>
#include <QFileDialog>
#include <QTextStream>
#include <QSettings>
#include <QStandardPaths>

#include "ui_TableItemWidget.h"
#include "TableItemWidget.h"

namespace TomoAnalysis::Report::Plugins {
    struct TableItemWidget::Impl {        
        Ui::TableItemWidget* ui{ nullptr };
        QString name;
    };

    TableItemWidget::TableItemWidget(QString name, QStandardItemModel* model, QWidget* parent)
    : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::TableItemWidget;
        d->ui->setupUi(this);
        d->name = name;
        connect(d->ui->exportButton, SIGNAL(clicked()), this, SLOT(OnExport()));

        d->ui->label->setText(name);
        if (model) {
            d->ui->tableView->setModel(model);
        }
        connect(d->ui->tableView, SIGNAL(pressed(const QModelIndex&)), this, SLOT(OnPressed(const QModelIndex&)));
        d->ui->tableView->setProperty("styleVariant", 1);
        d->ui->tableView->setSortingEnabled(true);
    }

    TableItemWidget::~TableItemWidget() {

    }    

    auto TableItemWidget::GetName() -> QString {
        return d->name;
    }

    void TableItemWidget::OnPressed(const QModelIndex& index) {        
        auto model = qobject_cast<QStandardItemModel*>(d->ui->tableView->model());        
        auto row = index.row();
        auto cubeName = d->ui->label->text();
        auto fullName = model->item(row, 1)->whatsThis().chopped(4);        
        auto secondheader = model->horizontalHeaderItem(2)->text();        
        if(secondheader == "Time step") {//time lapse graph
            auto cell_idx = model->item(row,3)->text();
            auto time_step = model->item(row, 2)->text().toInt() - 1;
            auto organ_name = model->item(row, 4)->text();
            emit rowSelectedTime(cubeName, fullName, cell_idx, time_step,organ_name);
        }else if(secondheader == "Cell #"){
            auto cell_idx = model->item(row, 2)->text();
            auto orgName = model->item(row, 3)->text();
            emit rowSelected(cubeName, fullName, cell_idx, -1,orgName);
        }
    }
    auto TableItemWidget::ClearSelection() -> void {
        d->ui->tableView->blockSignals(true);
        d->ui->tableView->clearSelection();
        d->ui->tableView->blockSignals(false);
    }
    auto TableItemWidget::SetSelectionTime(QString fileName, QString cell_idx, int time_step, QString organName) -> void {
        auto fidx = -1;
        auto model = qobject_cast<QStandardItemModel*>(d->ui->tableView->model());                

        for(auto i=0;i<model->rowCount();i++) {            
            if(model->item(i,1)->whatsThis().chopped(4) == fileName) {                
                if(model->item(i,2)->text().toInt() == time_step) {                    
                    if(model->item(i,3)->text() == cell_idx) {
                        if(model->item(i,4)->text() == organName) {
                            fidx = i;
                            break;
                        }
                    }
                }
            }
        }
        if (fidx > -1) {
            d->ui->tableView->blockSignals(true);
            d->ui->tableView->selectRow(fidx);
            d->ui->tableView->blockSignals(false);
        }
    }
    auto TableItemWidget::SetSelection(QString fileName, QString cell_idx,QString organName) -> void {
        auto fidx = -1;
        auto model = qobject_cast<QStandardItemModel*>(d->ui->tableView->model());
        for (auto i = 0; i < model->rowCount(); i++) {
            if(model->item(i, 1)->whatsThis().chopped(4) == fileName) {
                if(model->item(i,2)->text() == cell_idx) {
                    if (model->item(i, 3)->text() == organName) {
                        fidx = i;
                        break;
                    }
                }
            }
        }
        if(fidx > -1) {
            d->ui->tableView->blockSignals(true);
            d->ui->tableView->selectRow(fidx);
            d->ui->tableView->blockSignals(false);
        }
    }

    void TableItemWidget::OnExport() {
        auto desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QSettings qs("Report/Table/Export");
        auto prevPath = qs.value("prevPath", desktopPath).toString();

        auto filename = QFileDialog::getSaveFileName(
            nullptr,
            "Save Cube Report",
            prevPath+"/" + d->ui->label->text() + ".csv",
            "CSV File(*.csv)"
        );

        if (filename.isEmpty()) {
            return;
        }

        QFileInfo fileInfo(filename);
        qs.setValue("prevPath", fileInfo.dir().path());

        QFile file(filename);
        if (file.open(QFile::WriteOnly | QFile::Truncate)) {
            QTextStream stream(&file);
            QString contents;


            auto model = qobject_cast<QStandardItemModel*>(d->ui->tableView->model());

            QStringList headerList;
            for (auto col = 0; col < model->columnCount(); col++) {
                auto header = model->horizontalHeaderItem(col)->text();
                if (header.contains("Volume")) {
                    header = QString("Volume(um^3)");
                } else if (header.contains("SurfaceArea")) {
                    header = QString("Surface area(um^2)");
                } else if (header.contains("ProjectedArea")) {
                    header = QString("Projected area(um^2)");
                } else if (header.contains("Concentration")) {
                    header = QString("Concentration(pg/um^3)");
                }

                contents += header + ",";
            }

            contents += "\n";

            for (int r = 0; r < model->rowCount(); ++r) {
                for (int c = 0; c < model->columnCount(); ++c) {
                    auto item = model->item(r, c);
                    if (!item) {
                        continue;
                    }
                    auto str = item->text();
                    str.replace("N/A", "-1");
                    str.replace(",", ",""");
                    contents += str + ",";
                }
                contents += "\n";
            }
            stream << contents;
            file.close();
        }
    }
}
