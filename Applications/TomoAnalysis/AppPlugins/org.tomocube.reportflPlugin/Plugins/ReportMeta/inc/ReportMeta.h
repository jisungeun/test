#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "ReportMetaExport.h"

namespace TomoAnalysis::Report::Plugins {
	class ReportMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.reportflMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "ReportFL"; }
		auto GetFullName()const->QString override { return "org.tomocube.reportflPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}