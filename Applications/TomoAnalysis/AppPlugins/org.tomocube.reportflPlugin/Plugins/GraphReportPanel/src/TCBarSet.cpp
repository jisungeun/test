#include <iostream>
#include "TCBarSet.h"

struct TCBarSet::Impl {    
    QList<int> info;
    QString parent_title;
};

TCBarSet::TCBarSet(QString label, QObject* parent) : QBarSet(label, parent), d{ new Impl } {
    
}

TCBarSet::~TCBarSet() {
    
}

auto TCBarSet::GetBarInfo() -> QList<int> {
    return d->info;
}

auto TCBarSet::SetBarInfo(QList<int> info) -> void {
    d->info = info;
}

auto TCBarSet::SetParentTitle(QString title) -> void {
    d->parent_title = title;
}

auto TCBarSet::GetParentTitle() -> QString {
    return d->parent_title;
}