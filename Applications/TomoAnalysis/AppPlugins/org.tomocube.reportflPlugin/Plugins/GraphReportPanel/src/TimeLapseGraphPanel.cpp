#include <iostream>

#include <QtCharts>

#include "ui_TimeLapseGraphPanel.h"
#include "TimeLapseGraphPanel.h"

namespace TomoAnalysis::Report::Plugins {
    typedef struct PointInfo {
        double value;
        int index_in_cube;
        int cellID;
    }pInfo;
    struct TimeLapseGraphPanel::Impl {
        Ui::TimeLapseGraph* ui{ nullptr };

        ParameterId prevParameter{ ParameterId::MEAN_RI };
        ParameterId selectedParameter{ ParameterId::MEAN_RI };
        //OrganelleId selectedOrganelle{ OrganelleId::MEMBRANE };
        int selectedOrgan{ 0 };

        QMap<QString, QStringList> selectedDataList;
        QList<QScatterSeries*> scatterSeriesList;
        QList<QLineSeries*> lineSeriesList;
        QMap<QString, QList<int>> timeTable;
        QList<QMap<double, QList<pInfo>>> scatterTable;//cube,time, unstructured value                
        QMap<QString,QMap<int, double>> xAxisTimePoints;        
        //double xAxisTickInterval{ -1 };
        double xMinInterval{ -1 };

        QMap<QString, QColor> selectedChartColor;  // key: cube name, value: color

        QScatterSeries* markerSeries{ false };

        QString cur_pg{ QString() };
        QString cur_cube{ QString() };
        QString cur_file{ QString() };        
        int cur_timestep{ 0 };
        int cur_xMin{ 0 };
        int xAbsoluteMin{ 0 };
        int xStepMin{ 0 };
        int xStepMax{ 0 };
        int total_xCount{ 0 };

        int minOfHorizontalAxisTick = 1;
        int numberOfHorizontalAxisTick = 20;

        Entity::WorkingSet::Pointer curWS{ nullptr };
    };

    TimeLapseGraphPanel::TimeLapseGraphPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui = new Ui::TimeLapseGraph;
        d->ui->setupUi(this);

        d->ui->cubeName->setReadOnly(true);

        connect(d->ui->chartHorizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(OnChartHorScrollBarValueChanged(int)));
        connect(d->ui->editBtn, SIGNAL(clicked()), this, SLOT(OnEditBtn()));

        //d->ui->editBtn->setEnabled(false);

        connect(d->ui->selectDataTreeWidget, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SLOT(OnTreeItemChanged(QTreeWidgetItem*, int)));

        connect(d->ui->timeParameterWidget, &ParameterSelectionPanel::parameterChanged,
            [=](ParameterId id) {d->prevParameter = d->selectedParameter; d->selectedParameter = id; });
        //connect(d->ui->timeParameterWidget, &ParameterSelectionPanel::organlleChanged,
            //[=](OrganelleId id) {d->selectedOrganelle = id; });
        connect(d->ui->timeParameterWidget, &ParameterSelectionPanel::organIdxChanged, [=](int idx) {d->selectedOrgan = idx; });

        connect(d->ui->drawGraphButton, SIGNAL(clicked()), this, SLOT(OnDrawGraphClicked()));

        connect(d->ui->yAxisMinSpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnYRangeChanged()));
        connect(d->ui->yAxisMaxSpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnYRangeChanged()));

        connect(d->ui->xAxisMinSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnXMinChanged()));
        connect(d->ui->xAxisMaxSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnXMaxChanged()));

        connect(d->ui->exportGraphButton, SIGNAL(clicked()), this, SLOT(OnExportGraphClicked()));

        connect(d->ui->themeButton, SIGNAL(clicked(bool)), this, SLOT(OnGraphThemeSwitch(bool)));

        d->ui->themeButton->setObjectName("bt-switch");

        d->ui->yAxisMinSpinBox->setSingleStep(0.001);
        d->ui->yAxisMaxSpinBox->setSingleStep(0.001);

        Init();                
    }

    TimeLapseGraphPanel::~TimeLapseGraphPanel() {

    }

    auto TimeLapseGraphPanel::Init() -> void {
        d->ui->selectDataTreeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

        auto allTreeItem = new QTreeWidgetItem(d->ui->selectDataTreeWidget);
        allTreeItem->setText(0, "All");
        allTreeItem->setCheckState(0, Qt::Unchecked);

        d->ui->selectDataTreeWidget->addTopLevelItem(allTreeItem);

        // ���� ���� ui
        auto colorSettingLayout = new QHBoxLayout;
        colorSettingLayout->setContentsMargins(0, 0, 0, 0);
        colorSettingLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
        colorSettingLayout->setSpacing(10);

        d->ui->settingColorContents->setLayout(colorSettingLayout);

        // setup chart
        auto chart = new QChart;
        chart->legend()->hide();

        auto chartTitleFont = chart->titleFont();
        chartTitleFont.setBold(true);
        chart->setTitleFont(chartTitleFont);

        d->ui->chartView->setChart(chart);
        d->ui->chartView->setRenderHint(QPainter::Antialiasing);
    }

    auto TimeLapseGraphPanel::UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> bool {
        QTreeWidget *tree = d->ui->selectDataTreeWidget;
        auto allTreeItem = tree->topLevelItem(0);

        while (allTreeItem->childCount() > 0) {
            allTreeItem->removeChild(allTreeItem->child(0));
        }

        auto keys = cubeDataList->Keys();
        for (auto cube : keys) {
            auto cubeTreeItem = new QTreeWidgetItem(allTreeItem);
            cubeTreeItem->setText(0, cube);
            cubeTreeItem->setCheckState(0, Qt::Unchecked);

            allTreeItem->addChild(cubeTreeItem);

            auto values = cubeDataList->Value(cube);
            for (auto file : values) {
                auto fileTreeItem = new QTreeWidgetItem(cubeTreeItem);
                fileTreeItem->setText(0, file);
                fileTreeItem->setCheckState(0, Qt::Unchecked);

                cubeTreeItem->addChild(fileTreeItem);
            }
        }

        d->selectedChartColor.clear();
        //d->xAxisTickInterval = -1;
        d->xMinInterval = -1;

        RemoveAllColorSelector();
        ClearChart();

        allTreeItem->setCheckState(0, Qt::Checked);

        return true;
    }

    auto TimeLapseGraphPanel::UpdateGraph(Entity::WorkingSet::Pointer workingset) -> bool {
        d->curWS = workingset;
        RemoveAllColorSelector();        

        d->cur_pg = workingset->GetPlayground();
                
        if (d->ui->timeParameterWidget->UpdateOrgan2(workingset)) {
            //d->selectedOrganelle = d->ui->timeParameterWidget->GetCurrentOrgan();
            d->selectedOrgan = d->ui->timeParameterWidget->GetCurrentOrganID();
        }

        auto organ_list = workingset->GetOrganNames();
                
        auto allItem = d->ui->selectDataTreeWidget->topLevelItem(0);

        d->selectedDataList.clear();
        for (auto cubeIndex = 0; cubeIndex < allItem->childCount(); cubeIndex++) {
            auto cubeTreeItem = allItem->child(cubeIndex);

            QStringList dataList;
            for (auto dataIndex = 0; dataIndex < cubeTreeItem->childCount(); dataIndex++) {
                auto dataTreeItem = cubeTreeItem->child(dataIndex);
                if (dataTreeItem->checkState(0) == Qt::Checked) {
                    auto text = dataTreeItem->text(0);
                    dataList << text;
                }
            }

            if (!dataList.isEmpty()) {
                d->selectedDataList.insert(cubeTreeItem->text(0), dataList);
            }
        }

        // get selected value of parameter and organelle
        auto parameterValue = GraphParameters::parameterSet.value(
            ParameterId::_from_integral(d->selectedParameter),
            { "", "" }
        );

        /*auto organelleValue = GraphParameters::organelleSet.value(
            OrganelleId::_from_integral(d->selectedOrganelle),
            { "", "" }
        );*/
        
        //QString key = organelleValue.keyName + "." + parameterValue.keyName;
        QString key = organ_list[d->selectedOrgan] + "." + parameterValue.keyName;

        // add color picker of cubes

        // draw graph
        ClearChart();

        //clear time points
        d->xAxisTimePoints.clear();

        auto chart = d->ui->chartView->chart();
        auto cubeNames = workingset->GetCubeNames();
        if (cubeNames.isEmpty()) {
            return false;
        }
        {
            d->markerSeries = new QScatterSeries;
            d->markerSeries->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
            d->markerSeries->setMarkerSize(30);
            d->markerSeries->setObjectName("Star");
            QPainterPath starPath;
            starPath.moveTo(28, 15);
            for (int i = 1; i < 5; ++i) {
                starPath.lineTo(14 + 14 * qCos(0.8 * i * M_PI),
                    15 + 14 * qSin(0.8 * i * M_PI));
            }
            starPath.closeSubpath();

            QImage star(30, 30, QImage::Format_ARGB32);
            star.fill(Qt::transparent);

            QPainter painter(&star);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setPen(QRgb(0xf6a625));
            painter.setBrush(painter.pen().color());            
            painter.drawPath(starPath);

            d->markerSeries->setBrush(star);
            d->markerSeries->setPen(QColor(Qt::transparent));            
        }
        d->markerSeries->clear();
        chart->addSeries(d->markerSeries);//add overlap marker at the start

        double xMin,xMax, yMin, yMax;   
        xMin = xMax = yMin = yMax = -1.0;
        int xStepMin, xStepMax;
        xStepMin = xStepMax = -1;

        auto selectedCubeList = d->selectedDataList.keys();

        d->timeTable.clear();

        auto validCnt = 0;
        QMap<double, bool> wholeTimes;
        for (auto cubeIndex = 0; cubeIndex < selectedCubeList.size(); cubeIndex++) {
            QMap<double, QList<pInfo>> seriesDataSet;
            QList<int> time_LUT;
            auto cubeName = selectedCubeList.at(cubeIndex);
            d->xAxisTimePoints[cubeName] = QMap<int, double>();
            auto cubeSet = workingset->GetCubeSet(cubeName);
            auto reportFileNames = cubeSet.measure.keys();                
            for(auto img_idx = 0;img_idx<reportFileNames.count();img_idx++){
                auto report = reportFileNames[img_idx];
                auto cellName = report.left(report.length() - QString(".rep").length());
                auto dataList = d->selectedDataList.value(cubeName);
                if (dataList.indexOf(cellName) < 0) {
                    continue;
                }                                
                double timeInterval = -1;
                auto measureList = cubeSet.measure[report];
                for(auto m=0;m<measureList.count();m++){
                    auto measure = measureList[m];
                    auto tidx = measure->GetTimeIndex();
                    double time = measure->GetTimePoint();
                    if (timeInterval < 0) {
                        timeInterval = measure->GetTimeInterval();// *1000;  // sec to ms
                    }
                    if (time < 0) {
                        time = tidx * timeInterval;
                    }                    
                    if (d->xAxisTimePoints[cubeName].count() < measureList.count()) {
                        d->xAxisTimePoints[cubeName][tidx] = time;
                    }                    
                    if(img_idx == 0) {
                        time_LUT.push_back(tidx);
                        wholeTimes[time] = true;
                    }

                    if(xStepMin <0) {
                        xStepMin = tidx;
                    }else {
                        xStepMin = std::min(xStepMin, tidx);
                    }

                    if(xStepMax<0) {
                        xStepMax = tidx;
                    }else {
                        xStepMax = std::max(xStepMax, tidx);
                    }

                    if(xMin <0.0) {
                        xMin = time;
                    }else {
                        xMin = std::min(xMin, time);
                    }

                    if (xMax < 0.0) {
                        xMax = time;
                    } else {
                        xMax = std::max(xMax, time);
                    }

                    auto cellCount = measure->GetCellCount();                    
                    for (int cellIndex = 0; cellIndex < cellCount; cellIndex++) {
                        auto value = measure->GetMeasure(key, cellIndex);
                        if (value < 0) {
                            value = 0.0;
                            continue;
                        } else if (parameterValue.keyName.compare(GraphParameters::parameterSet[ParameterId::MEAN_RI].keyName) == 0) {
                            value = round(value * 10000) / 10000;
                            validCnt++;
                        }else {
                            value = round(value * 100) / 100;
                            validCnt++;
                        }

                        QList<pInfo> valueList;
                        if (seriesDataSet.contains(time)) {                            
                            valueList = seriesDataSet.value(time);
                        }
                        pInfo val;
                        val.value = value;
                        val.index_in_cube = img_idx;
                        val.cellID = cellIndex;
                        valueList.push_back(val);
                                                
                        seriesDataSet.insert(time, valueList);

                        if (yMin < 0.0) {
                            yMin = value;
                        }
                        else {
                            yMin = std::min(yMin, value);
                        }

                        if (yMax < 0.0) {
                            yMax = value;
                        }
                        else {
                            yMax = std::max(yMax, value);
                        }
                    }
                }
                
                /*if (d->xAxisTickInterval < 0) {
                    d->xAxisTickInterval = timeInterval;
                }else{
                    d->xAxisTickInterval = std::min(d->xAxisTickInterval, timeInterval);                
                }*/
            }

            if(validCnt ==0) {//if there is no valid value
                QMessageBox::warning(nullptr, "Error", QString("There is no valid measure in %1").arg(GraphParameters::parameterSet[d->selectedParameter].keyName));
                d->selectedParameter = d->prevParameter;
                d->ui->timeParameterWidget->SetCurrentKey(d->prevParameter);
                OnDrawGraphClicked();
                return false;
            }
            QColor chartColor;
            if (d->selectedChartColor.contains(cubeName)) {
                chartColor = d->selectedChartColor.value(cubeName);
            } else {
                chartColor = GraphColor::colorPalette.at(cubeIndex % GraphColor::colorPalette.size());
                d->selectedChartColor.insert(cubeName, chartColor);
            }
            
            AppendColorSelector(cubeName, chartColor.name());
                        
            auto scatterSeries = new QScatterSeries;            
            scatterSeries->setName(cubeName);
            scatterSeries->setMarkerSize(10);
            scatterSeries->setBrush(chartColor);            
            auto lineSeries = new QLineSeries;
            lineSeries->setName(cubeName);
            QPen linePen = QPen(chartColor);
            linePen.setWidth(3);
            lineSeries->setPen(linePen);

            connect(scatterSeries, SIGNAL(clicked(const QPointF&)), this, SLOT(OnScatterClicked(const QPointF&)));
            
            QMapIterator<double, QList<pInfo>> it(seriesDataSet);
            while (it.hasNext()) {
                it.next();
                double sum = 0.0;                
                for (auto v : it.value()) {                    
                    scatterSeries->append(it.key(), v.value);                    
                    sum += v.value;                    
                }                
                lineSeries->append(it.key(), sum / it.value().count());
            }
            chart->addSeries(lineSeries);
            chart->addSeries(scatterSeries);   

            d->scatterTable.push_back(seriesDataSet);
            d->scatterSeriesList.push_back(scatterSeries);
            d->lineSeriesList.push_back(lineSeries);
            d->timeTable[cubeName] = time_LUT;
        }        
        // create x axis
        auto axisX = new QCategoryAxis;
        axisX->setLabelsAngle(45);
        axisX->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);        
        
        auto keySet = wholeTimes.keys();
        d->xMinInterval = keySet[1] - keySet[0];
        double prev_key = keySet[1];
        for (auto i = 2; i < keySet.count(); i++) {
            d->xMinInterval = std::min(d->xMinInterval, keySet[i] - prev_key);
            prev_key = keySet[i];
        }
        //d->numberOfHorizontalAxisTick = xStepMax +1;
        d->numberOfHorizontalAxisTick = static_cast<int>((xMax - xMin) / d->xMinInterval) + 1;

        d->total_xCount = 0;
        double x = xMin;        
        while (x <= xMax) {
            auto time = QTime(0, 0, 0);
            time = time.addSecs(x);// .addMSecs(x);
            //if (x <= xMin + (d->numberOfHorizontalAxisTick-1) * d->xAxisTickInterval) {
            if (x <= xMin + (d->numberOfHorizontalAxisTick - 1) * d->xMinInterval) {
                axisX->append(time.toString("hh:mm:ss"), x);
            }            
            //x += d->xAxisTickInterval;
            x += d->xMinInterval;
            d->total_xCount++;
        }        

        //d->total_xCount = axisX->count();
        d->cur_xMin = xMin;        
        //if (axisX->count() > d->numberOfHorizontalAxisTick) {
        if(d->total_xCount > d->numberOfHorizontalAxisTick){
            //axisX->setRange(0, (d->numberOfHorizontalAxisTick - 1) * d->xAxisTickInterval);
            //axisX->setRange(xMin, xMin+(d->numberOfHorizontalAxisTick - 1)* d->xAxisTickInterval);                        
            axisX->setRange(xMin, xMin + (d->numberOfHorizontalAxisTick - 1) * d->xMinInterval);
            d->ui->chartHorizontalScrollBar->setSingleStep(1);
            d->ui->chartHorizontalScrollBar->setPageStep(d->numberOfHorizontalAxisTick);
            d->ui->chartHorizontalScrollBar->setRange(0, d->total_xCount - d->numberOfHorizontalAxisTick);            
            d->ui->chartHorizontalScrollBar->setValue(0);
            d->ui->chartHorizontalScrollBar->show();          
        } else {
            //axisX->setRange(0, (axisX->count() - 1) * d->xAxisTickInterval);
            //axisX->setRange(xMin, xMin+(axisX->count() - 1)* d->xAxisTickInterval);
            //axisX->setRange(xMin, xMin + (d->total_xCount - 1) * d->xAxisTickInterval);
            axisX->setRange(xMin, xMin + (d->total_xCount - 1) * d->xMinInterval);
            d->ui->chartHorizontalScrollBar->hide();
        }
        

        // create y axis
        auto yAxisTitle = parameterValue.displayName;
        switch (d->selectedParameter) {
        case ParameterId::VOLUME:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        case ParameterId::SURFACE_AREA:
        case ParameterId::PROJECTED_AREA:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B2"));
            break;
        case ParameterId::DRYMASS:
            yAxisTitle += "<br>(pg)";
            break;
        case ParameterId::PROTEIN_CONCENTRATION:
            yAxisTitle += QString("<br>(pg/%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        default:
            break;
        }

        auto axisY = new QValueAxis;
        axisY->setRange(yMin, yMax);
        axisY->setTitleText(yAxisTitle);

        chart->setTitle(DataToPresentation(organ_list[d->selectedOrgan]));
        chart->addAxis(axisX, Qt::AlignBottom);
        chart->addAxis(axisY, Qt::AlignLeft);
                
        for (auto series : chart->series()) {
            series->attachAxis(axisX);
            series->attachAxis(axisY);
        }

        d->ui->xAxisMinSpinBox->blockSignals(true);
        d->ui->xAxisMaxSpinBox->blockSignals(true);
        d->ui->yAxisMinSpinBox->blockSignals(true);
        d->ui->yAxisMaxSpinBox->blockSignals(true);

        d->ui->yAxisMinSpinBox->setValue(yMin);
        d->ui->yAxisMaxSpinBox->setValue(yMax);
        
        d->ui->xAxisMaxSpinBox->setRange(xStepMin, xStepMax);
        d->ui->xAxisMinSpinBox->setRange(xStepMin, xStepMax);

        d->xAbsoluteMin = xStepMin;
        d->xStepMin = xStepMin;
        d->xStepMax = xStepMax;

        d->ui->xAxisMinSpinBox->setValue(xStepMin);
        if(xStepMax-xStepMin >= d->numberOfHorizontalAxisTick) {
            d->ui->xAxisMaxSpinBox->setValue(xStepMin + d->numberOfHorizontalAxisTick - 1);
        }else {
            d->ui->xAxisMaxSpinBox->setValue(xStepMax);
        }        

        OnGraphThemeSwitch(d->ui->themeButton->isChecked());

        d->ui->xAxisMinSpinBox->blockSignals(false);
        d->ui->xAxisMaxSpinBox->blockSignals(false);
        d->ui->yAxisMinSpinBox->blockSignals(false);
        d->ui->yAxisMaxSpinBox->blockSignals(false);

        return true;
    }
    void TimeLapseGraphPanel::OnEditBtn() {
        if(d->ui->editMaskCombo->count()<1) {
            return;
        }

        d->cur_file = d->ui->editMaskCombo->currentText();

        emit sigEditMask(d->cur_pg, d->cur_cube, d->cur_file, d->cur_timestep);
    }
    void TimeLapseGraphPanel::SelectionFromTable(QString cubeName, QString tcf_name, QString cellID, int time_step,QString organ_name){
        d->ui->cubeName->setText(cubeName);
        d->cur_cube = cubeName;

        d->cur_timestep = time_step;

        d->ui->editMaskCombo->blockSignals(true);
        d->ui->editMaskCombo->clear();
        d->ui->editMaskCombo->addItem(tcf_name);
        d->ui->editMaskCombo->blockSignals(false);        
        d->markerSeries->clear();

        //add new marker series without signal

        //check organ name fisrt
        /*auto organelleValue = GraphParameters::organelleSet.value(
            OrganelleId::_from_integral(d->selectedOrganelle),
            { "", "" }
        );*/
        auto organ_list = d->curWS->GetOrganNames();

        if(DataToPresentation(organ_list[d->selectedOrgan]) != organ_name) {
            return;
        }

        auto cube_idx = 0;

        for (auto i = 0; i < d->scatterSeriesList.size(); i++) {
            if (d->scatterSeriesList[i]->name().compare(cubeName) == 0) {
                cube_idx = i;
                break;
            }
        }

        //auto target_time = d->cur_xMin + time_step * d->xAxisTickInterval;
        auto target_time = d->xAxisTimePoints[cubeName][time_step];
        auto time = QTime(0, 0, 0);
        time = time.addSecs(target_time);

        auto timeStamp = time.toString("hh:mm:ss");

        auto t = d->scatterTable[cube_idx];        

        QPointF point = QPointF(-1,-1);
        point.setX(target_time);        
        auto ff = t[target_time];
        auto tcf_names = d->selectedDataList[cubeName];
        for (auto f = 0; f < ff.size(); f++) {
            auto value = ff[f].value;
            auto img_idx = ff[f].index_in_cube;
            auto cell_idx = ff[f].cellID;
            QString cID = QString::number(img_idx+1) + "_" + QString::number(cell_idx+1);            
            if(cID == cellID) {
                point.setY(value);
                break;
            }
        }        

        if (point.x() != -1 && point.y() != -1) {
            d->markerSeries->append(point);
        }
    }
    auto TimeLapseGraphPanel::DataToPresentation(QString tdata) -> QString {
        if (tdata == "membrane") {
            return QString("Whole Cell");
        }
        if (tdata == "lipid droplet") {
            return QString("Lipid droplet");
        }
        if (tdata == "nucleus") {
            return QString("Nucleus");
        }
        if (tdata == "nucleoli") {
            return QString("Nucleolus");
        }
        return tdata;
    }
    auto TimeLapseGraphPanel::PresentationToData(QString presentation) -> QString {
        if (presentation == "Whole Cell") {
            return QString("membrane");
        }
        if (presentation == "Lipid droplet") {
            return QString("lipid droplet");
        }
        if (presentation == "Nucleus") {
            return QString("nucleus");
        }
        if (presentation == "Nucleolus") {
            return QString("nucleoli");
        }
        return presentation;
    }


    void TimeLapseGraphPanel::OnTableDeselection() {
        d->markerSeries->clear();
        d->ui->editMaskCombo->blockSignals(true);
        d->ui->editMaskCombo->clear();
        d->ui->editMaskCombo->blockSignals(false);

        d->ui->cubeName->blockSignals(true);
        d->ui->cubeName->clear();
        d->ui->cubeName->blockSignals(false);

        d->cur_timestep = -1;
    }
    void TimeLapseGraphPanel::mousePressEvent(QMouseEvent* event) {
        if (event->buttons() == Qt::LeftButton) {
            if (d->markerSeries) {
                d->markerSeries->clear();
                d->ui->editMaskCombo->blockSignals(true);
                d->ui->editMaskCombo->clear();
                d->ui->editMaskCombo->blockSignals(false);

                d->ui->cubeName->blockSignals(true);
                d->ui->cubeName->clear();
                d->ui->cubeName->blockSignals(false);

                d->cur_timestep = -1;

                emit graphDeselected();
            }
        }
    }    
    void TimeLapseGraphPanel::OnScatterClicked(const QPointF& point) {
        auto cubeName = static_cast<QScatterSeries*>(sender())->name();        
        auto time = QTime(0, 0, 0);
        time = time.addSecs(point.x());        
        auto cube_idx = 0;
        
        for(auto i=0;i<d->scatterSeriesList.size();i++) {
            if(d->scatterSeriesList[i]->name().compare(cubeName)==0) {
                cube_idx = i;
                break;
            }
        }
        
        d->ui->cubeName->setText(cubeName);
        d->cur_cube = cubeName;        
                
        //Update Mask Selection interface
        d->ui->editMaskCombo->blockSignals(true);
        d->ui->editMaskCombo->clear();
        auto t = d->scatterTable[cube_idx];
        auto time_idx = 0;
        for(auto i =0; i<t.keys().size();i++) {
            if(t.keys()[i] == point.x()) {                
                time_idx = i;
                break;
            }
        }        
        d->cur_timestep = d->timeTable[cubeName][time_idx];
        auto ff = t[point.x()];
        auto tcf_names = d->selectedDataList[cubeName];
        auto fidx = 0;        
        for (auto f = 0; f < ff.size(); f++) {
            auto value = ff[f].value;
            auto img_idx = ff[f].index_in_cube;            
            if (value + 0.0003 > point.y() && value - 0.0003 < point.y()) {
                if (d->ui->editMaskCombo->findText(tcf_names[img_idx]) < 0) {
                    d->ui->editMaskCombo->addItem(tcf_names[img_idx]);
                }                
                fidx = f;
            }
        }
        d->markerSeries->clear();
        d->markerSeries->append(point);
        d->ui->editMaskCombo->setCurrentIndex(0);
        d->ui->editMaskCombo->blockSignals(false);

        auto imgIdx= ff[fidx].index_in_cube;
        auto cell_idx = ff[fidx].cellID;

        auto cID = QString::number(imgIdx +1) + "_" + QString::number(cell_idx + 1);

        /*auto organelleValue = GraphParameters::organelleSet.value(
            OrganelleId::_from_integral(d->selectedOrganelle),
            { "", "" }
        );*/
        auto organ_list = d->curWS->GetOrganNames();
        
        emit graphSelected(cubeName,tcf_names[imgIdx],cID, d->cur_timestep,DataToPresentation(organ_list[d->selectedOrgan]));
    }
    void TimeLapseGraphPanel::OnTreeItemChanged(QTreeWidgetItem* item, int column) {
        Q_UNUSED(column)
        std::function<void(QTreeWidgetItem*)> setChildrenCheckState = [item, &setChildrenCheckState](QTreeWidgetItem* treeItem) {
            treeItem->setCheckState(0, item->checkState(0));
            for (auto i = 0; i < treeItem->childCount(); i++) {
                setChildrenCheckState(treeItem->child(i));
            }
        };

        QTreeWidget* tree = d->ui->selectDataTreeWidget;
        auto allItem = tree->topLevelItem(0);

        tree->blockSignals(true);

        if (item == allItem) {            
            setChildrenCheckState(item);
        } else {            
            if (item->checkState(0) == Qt::Unchecked) {
                allItem->setCheckState(0, Qt::Unchecked);
            }
            
            if (item->childCount() > 0) {
                for (int i = 0; i < allItem->childCount(); i++) {
                    if (allItem->child(i) == item) {
                        setChildrenCheckState(allItem->child(i));
                        break;
                    }
                }
            }                        
            bool isAllCubeChecked = true;
            for (int cubeIndex = 0; cubeIndex < allItem->childCount(); cubeIndex++) {
                auto cubeTreeItem = allItem->child(cubeIndex);

                bool isAllCubeDataChecked = true;
                for (int dataIndex = 0; dataIndex < cubeTreeItem->childCount(); dataIndex++) {
                    if (cubeTreeItem->child(dataIndex)->checkState(0) == Qt::Unchecked) {
                        isAllCubeDataChecked = false;
                        break;
                    }
                }
                
                if (isAllCubeDataChecked) {
                    cubeTreeItem->setCheckState(0, Qt::Checked);
                } else {
                    cubeTreeItem->setCheckState(0, Qt::Unchecked);
                    isAllCubeChecked = false;
                }
            }

            if (isAllCubeChecked) {
                allItem->setCheckState(0, Qt::Checked);
            } else {
                allItem->setCheckState(0, Qt::Unchecked);
            }
        }

        tree->blockSignals(false);
    }

    void TimeLapseGraphPanel::OnDrawGraphClicked() {
        emit drawGraph();
    }

    void TimeLapseGraphPanel::OnYRangeChanged() {
        auto chart = d->ui->chartView->chart();
        auto yAxis = qobject_cast<QValueAxis*>(chart->axes(Qt::Vertical).first());
        if (yAxis) {
            yAxis->setRange(
                d->ui->yAxisMinSpinBox->value(),
                d->ui->yAxisMaxSpinBox->value()
            );
        }
    }

    void TimeLapseGraphPanel::OnXMinChanged() {
        auto candidate = d->ui->xAxisMinSpinBox->value();
        auto curMax = d->ui->xAxisMaxSpinBox->value();
        if(curMax - candidate < d->minOfHorizontalAxisTick) {
            d->ui->xAxisMinSpinBox->blockSignals(true);
            d->ui->xAxisMinSpinBox->setValue(d->xStepMin);
            d->ui->xAxisMinSpinBox->blockSignals(false);
            return;
        }
        d->xStepMin = candidate;
        OnXRangeChanged();
    }

    void TimeLapseGraphPanel::OnXMaxChanged() {
        auto candidate = d->ui->xAxisMaxSpinBox->value();
        auto curMin = d->ui->xAxisMinSpinBox->value();
        if(candidate - curMin < d->minOfHorizontalAxisTick) {
            d->ui->xAxisMaxSpinBox->blockSignals(true);
            d->ui->xAxisMaxSpinBox->setValue(d->xStepMax);
            d->ui->xAxisMaxSpinBox->blockSignals(false);
            return;
        }
        d->xStepMax = candidate;
        OnXRangeChanged();
    }

    void TimeLapseGraphPanel::OnXRangeChanged() {
        auto chart = d->ui->chartView->chart();
        auto xAxis = qobject_cast<QCategoryAxis*>(chart->axes(Qt::Horizontal).first());
        if(xAxis) {
            //remove existing axis
            chart->removeAxis(xAxis);

            auto newAxis = new QCategoryAxis;
            newAxis->setLabelsAngle(45);
            newAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);

            //create new axis
            auto minTick = d->ui->xAxisMinSpinBox->value();
            auto maxTick = d->ui->xAxisMaxSpinBox->value();
            if (maxTick - minTick + 1 >= d->minOfHorizontalAxisTick) {
                d->numberOfHorizontalAxisTick = maxTick - minTick + 1;
            }                        

            double x = d->cur_xMin;
            //while (x < d->total_xCount * d->xAxisTickInterval + d->cur_xMin) {
            while (x < d->total_xCount * d->xMinInterval + d->cur_xMin) {
                auto time = QTime(0, 0, 0);
                time = time.addSecs(x);                
                //if (x <= minTick * d->xAxisTickInterval + (d->numberOfHorizontalAxisTick - 1) * d->xAxisTickInterval                    && x > minTick * d->xAxisTickInterval) {
                if (x <= minTick * d->xMinInterval + (d->numberOfHorizontalAxisTick - 1) * d->xMinInterval && x > minTick * d->xMinInterval) {
                    newAxis->append(time.toString("hh:mm:ss"), x);                    
                }
                else {                    
                    newAxis->append(QString(), x);                    
                }
                //x += d->xAxisTickInterval;                
                x += d->xMinInterval;
            }            
            //newAxis->setRange(minTick * d->xAxisTickInterval, maxTick * d->xAxisTickInterval);                        
            newAxis->setRange(minTick * d->xMinInterval, maxTick * d->xMinInterval);

            chart->addAxis(newAxis, Qt::AlignBottom);
            for (auto serie : chart->series()) {
                serie->attachAxis(newAxis);
            }

            d->ui->chartHorizontalScrollBar->blockSignals(true);
            d->ui->chartHorizontalScrollBar->setPageStep(d->numberOfHorizontalAxisTick);            
            d->ui->chartHorizontalScrollBar->setRange(0, d->total_xCount-d->numberOfHorizontalAxisTick);            
            d->ui->chartHorizontalScrollBar->setValue(d->ui->xAxisMinSpinBox->value() - d->xAbsoluteMin);
            d->ui->chartHorizontalScrollBar->blockSignals(false);

            if (d->numberOfHorizontalAxisTick < d->total_xCount) {
                d->ui->chartHorizontalScrollBar->show();
            }else {
                d->ui->chartHorizontalScrollBar->hide();
            }
        }
    }


    void TimeLapseGraphPanel::OnCubeColorChanged(const QString& title, QColor color) {
        auto chart = d->ui->chartView->chart();
        auto seriesList = chart->series();
        for (auto scatterSeries : d->scatterSeriesList) {
            if (title.compare(scatterSeries->name()) == 0) {
                scatterSeries->setBrush(QBrush(color));
            }
        }

        for (auto lineSeries : d->lineSeriesList) {
            if (title.compare(lineSeries->name()) == 0) {
                lineSeries->setPen(QPen(color));
            }
        }

        d->selectedChartColor.insert(title, color);
    }

    void TimeLapseGraphPanel::OnExportGraphClicked() {
        auto desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QSettings qs("Report/TimeGraph/Export");
        auto prevPath = qs.value("prevPath", desktopPath).toString();
        
        auto filename = QFileDialog::getSaveFileName(
            this,
            "Export Hypercube Graph",
            prevPath + "/untitled.png",
            "Images (*.png *.jpg)"
        );

        if (filename.isEmpty()) {
            return;
        }
        QFileInfo fileInfo(filename);        
        qs.setValue("prevPath", fileInfo.dir().path());

        QColor backgroundColor;
        QColor textColor;

        if (d->ui->themeButton->isChecked()) {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Dark].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Dark].text;
        }
        else {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Light].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Light].text;
        }

        // duplicate current chart to capture whole x range
        auto chart = d->ui->chartView->chart();        
        auto seriesList = chart->series();        
        auto duplChart = std::make_shared<QChart>();

        duplChart->legend()->hide();
        duplChart->setTitleFont(chart->titleFont());
        duplChart->setTitle(chart->title());
        duplChart->setBackgroundRoundness(chart->backgroundRoundness());
        duplChart->setTitleBrush(chart->titleBrush());
        duplChart->setBackgroundBrush(chart->backgroundBrush());

        // duplicate axes
        auto duplAxisX = std::make_shared<QCategoryAxis>();
        auto duplAxisY = std::make_shared<QValueAxis>();

        if (!chart->axes(Qt::Horizontal).isEmpty()) {
            auto axisX = dynamic_cast<QCategoryAxis*>(chart->axes(Qt::Horizontal).first());

            for (auto label : axisX->categoriesLabels()) {
                duplAxisX->append(label, axisX->endValue(label));
            }

            duplAxisX->setLabelsAngle(axisX->labelsAngle());
            duplAxisX->setLabelsPosition(axisX->labelsPosition());
            //duplAxisX->setRange(0, (duplAxisX->count() - 1) * d->xAxisTickInterval);
            duplAxisX->setRange(axisX->min(), axisX->max());

            duplAxisX->setLabelsColor(axisX->labelsColor());

            duplChart->addAxis(duplAxisX.get(), Qt::AlignBottom);
        }

        if (!chart->axes(Qt::Vertical).isEmpty()) {
            auto axisY = dynamic_cast<QValueAxis*>(chart->axes(Qt::Vertical).first());

            duplAxisY->setTitleText(axisY->titleText());
            duplAxisY->setTitleBrush(axisY->titleBrush());
            duplAxisY->setRange(axisY->min(), axisY->max());

            duplAxisY->setLabelsColor(axisY->labelsColor());
            
            duplChart->addAxis(duplAxisY.get(), Qt::AlignLeft);
        }

        // duplicate chart series
        for (auto series : seriesList) {            
            if (series->type() == QAbstractSeries::SeriesTypeScatter) {
                auto scatterSeries = dynamic_cast<QScatterSeries*>(series);
                if (scatterSeries == nullptr) {
                    continue;
                }
                if (scatterSeries->objectName() == "Star") {
                    auto duplScatterSeries = std::make_shared<QScatterSeries>();
                    duplScatterSeries->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
                    duplScatterSeries->setMarkerSize(50);
                    QPainterPath starPath;
                    starPath.moveTo(28/3.0*5.0, 15/3.0*5.0);
                    for (int i = 1; i < 5; ++i) {
                        starPath.lineTo(14/3.0*5.0 + 14/3.0*5.0 * qCos(0.8 * i * M_PI),
                            15/3.0*5.0 + 14/3.0*5.0 * qSin(0.8 * i * M_PI));
                    }
                    starPath.closeSubpath();

                    QImage star(50, 50, QImage::Format_ARGB32);
                    star.fill(Qt::transparent);

                    QPainter painter(&star);
                    painter.setRenderHint(QPainter::Antialiasing);
                    painter.setPen(QRgb(0xf6a625));
                    painter.setBrush(painter.pen().color());
                    painter.drawPath(starPath);

                    duplScatterSeries->setBrush(star);
                    duplScatterSeries->setPen(QColor(Qt::transparent));
                    duplScatterSeries->append(scatterSeries->points());
                                        
                    duplChart->addSeries(duplScatterSeries.get());

                    duplScatterSeries->attachAxis(duplAxisX.get());
                    duplScatterSeries->attachAxis(duplAxisY.get());
                }
                else {
                    auto duplScatterSeries = std::make_shared<QScatterSeries>();
                    duplScatterSeries->setName(scatterSeries->name());
                    auto ssize = scatterSeries->markerSize();
                    ssize = ssize / 3.0 * 5.0;
                    duplScatterSeries->setMarkerSize(ssize);
                    duplScatterSeries->setBrush(scatterSeries->brush());
                    duplScatterSeries->append(scatterSeries->points());

                    duplChart->addSeries(duplScatterSeries.get());

                    duplScatterSeries->attachAxis(duplAxisX.get());
                    duplScatterSeries->attachAxis(duplAxisY.get());
                }

            } else if (series->type() == QAbstractSeries::SeriesTypeLine) {
                auto lineSeries = dynamic_cast<QLineSeries*>(series);
                if (lineSeries == nullptr) {
                    continue;
                }

                auto duplLineSeries = std::make_shared<QLineSeries>();
                duplLineSeries->setName(lineSeries->name());
                auto ppen = lineSeries->pen();
                ppen.setWidth(5);
                duplLineSeries->setPen(ppen);
                duplLineSeries->append(lineSeries->points());

                duplChart->addSeries(duplLineSeries.get());

                duplLineSeries->attachAxis(duplAxisX.get());
                duplLineSeries->attachAxis(duplAxisY.get());
            }
        }        
        auto duplChartView = std::make_shared<QChartView>(duplChart.get());
        duplChartView->setRenderHint(QPainter::Antialiasing);

        // create temporary parent widget & layout for chart 
        auto layout = std::make_shared<QVBoxLayout>();
        layout->setContentsMargins(0, 0, 0, 0);
        layout->addWidget(duplChartView.get());

        QWidget widget;
        widget.setLayout(layout.get());
        widget.setFixedSize(1920, 1080);   // save as FHD resolution

        // save
        widget.grab().save(filename);        
    }

    void TimeLapseGraphPanel::OnGraphThemeSwitch(bool checked) {
        QColor backgroundColor;
        QColor textColor;
                
        if (checked) {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Dark].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Dark].text;
        }
        else {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Light].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Light].text;
        }

        auto chart = d->ui->chartView->chart();
        if (chart) {
            chart->setBackgroundBrush(QBrush(backgroundColor));
            chart->setTitleBrush(QBrush(textColor));

            if (!chart->axes(Qt::Horizontal).isEmpty()) {
                chart->axes(Qt::Horizontal).first()->setLabelsColor(textColor);
            }

            if (!chart->axes(Qt::Vertical).isEmpty()) {
                auto yAxis = chart->axes(Qt::Vertical).first();
                yAxis->setTitleBrush(textColor);
                yAxis->setLabelsColor(textColor);
            }
        }
    }

    void TimeLapseGraphPanel::OnChartHorScrollBarValueChanged(int value) {        
        auto chart = d->ui->chartView->chart();
        if (chart == nullptr) {
            return;
        }

        if (chart->axes(Qt::Horizontal).isEmpty()) {
            return;
        }

        auto axisX = qobject_cast<QCategoryAxis*>(chart->axes(Qt::Horizontal).first());

        //remove existing axis
        chart->removeAxis(axisX);                 

        //construct new axis
        auto newAxis = new QCategoryAxis;
        newAxis->setLabelsAngle(45);
        newAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);

        double x = d->cur_xMin;
        //while (x < d->total_xCount * d->xAxisTickInterval + d->cur_xMin) {
        while (x < d->total_xCount * d->xMinInterval + d->cur_xMin) {
            auto time = QTime(0, 0, 0);
            time = time.addSecs(x);// .addMSecs(x);
            //if (x <= value * d->xAxisTickInterval + d->cur_xMin + (d->numberOfHorizontalAxisTick - 1) * d->xAxisTickInterval                && x > value * d->xAxisTickInterval + d->cur_xMin) {
            if (x <= value * d->xMinInterval + d->cur_xMin + (d->numberOfHorizontalAxisTick - 1) * d->xMinInterval && x > value * d->xMinInterval + d->cur_xMin) {
                newAxis->append(time.toString("hh:mm:ss"), x);                
            }else {
                newAxis->append(QString(), x);                
            }
            //x += d->xAxisTickInterval;            
            x += d->xMinInterval;
        }        
        //axisX->setRange(value * d->xAxisTickInterval + d->cur_xMin, (value + d->numberOfHorizontalAxisTick - 1) * d->xAxisTickInterval + d->cur_xMin);
        //newAxis->setRange(value * d->xAxisTickInterval + d->cur_xMin, (value + d->numberOfHorizontalAxisTick - 1) * d->xAxisTickInterval + d->cur_xMin);                
        newAxis->setRange(value * d->xMinInterval + d->cur_xMin, (value + d->numberOfHorizontalAxisTick - 1) * d->xMinInterval + d->cur_xMin);

        chart->addAxis(newAxis, Qt::AlignBottom);
        for(auto serie : chart->series()) {
            serie->attachAxis(newAxis);
        }
    }

    auto TimeLapseGraphPanel::ClearChart() -> void {
        d->ui->yAxisMinSpinBox->clear();
        d->ui->yAxisMaxSpinBox->clear();

        d->ui->xAxisMinSpinBox->clear();
        d->ui->yAxisMaxSpinBox->clear();

        d->scatterTable.clear();
        d->scatterSeriesList.clear();
        d->lineSeriesList.clear();
                
        auto chart = d->ui->chartView->chart();
        chart->removeAllSeries();
        chart->setTitle("");
        while (!chart->axes(Qt::Horizontal | Qt::Vertical).isEmpty()) {
            chart->removeAxis(chart->axes(Qt::Horizontal | Qt::Vertical).first());
        }

        d->ui->chartHorizontalScrollBar->blockSignals(true);
        d->ui->chartHorizontalScrollBar->setRange(0, 0);
        d->ui->chartHorizontalScrollBar->hide();
        d->ui->chartHorizontalScrollBar->blockSignals(false);
    }

    auto TimeLapseGraphPanel::AppendColorSelector(const QString& text, const QString& color) -> void {
        auto colorSelector = new ColorSelector(text);

        connect(colorSelector, SIGNAL(colorChanged(const QString&, QColor)),
            this, SLOT(OnCubeColorChanged(const QString&, QColor)));

        colorSelector->SetColor(color);

        auto layout = qobject_cast<QHBoxLayout*>(d->ui->settingColorContents->layout());
        layout->addWidget(colorSelector);
    }

    auto TimeLapseGraphPanel::RemoveAllColorSelector() -> void {
        QLayoutItem* child = nullptr;
        while ((child = d->ui->settingColorContents->layout()->takeAt(0)) != nullptr) {
            delete child->widget();
            delete child;
        }
    }

    struct ColorSelector::Impl {
        QString title;
        QColor currentColor{ GraphColor::colorPalette.first() };
    };

    ColorSelector::ColorSelector(const QString& title, QWidget* parent)
        : QWidget(parent), d{ new Impl } {

        setFixedHeight(20);

        d->title = title;

        auto label = new QLabel(d->title);
        label->setFixedHeight(20);

        auto comboBox = new QComboBox;
        comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        comboBox->setMinimumWidth(80);
        comboBox->setFixedHeight(20);
        for (auto color : GraphColor::colorPalette) {
            QPixmap colorIcon(10, 10);
            colorIcon.fill(QColor(color));
            comboBox->addItem(colorIcon, "", color);
        }

        connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
            [=](int index) {
                emit colorChanged(d->title, comboBox->itemData(index).toString());
            }
        );
        
        auto layout = new QHBoxLayout;
        layout->setSpacing(3);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->addWidget(label);
        layout->addWidget(comboBox);

        setLayout(layout);
    }

    ColorSelector::~ColorSelector() {
        
    }

    auto ColorSelector::SetColor(const QString& color) -> void {
        auto comboBox = findChild<QComboBox*>();
        if (comboBox == nullptr) {
            return;
        }

        auto index = comboBox->findData(color);
        if (index < 0) {
            return;
        }

        comboBox->setCurrentIndex(index);
    }
}
