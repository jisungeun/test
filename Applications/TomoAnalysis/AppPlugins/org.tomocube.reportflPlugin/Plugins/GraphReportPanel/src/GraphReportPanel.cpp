#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>

#include <TCBarSet.h>
#include <TCChartView.h>

#include "ui_GraphReportPanel.h"
#include "GraphReportPanel.h"

const int numDetailsGraphPerPage = 2;   // 스크롤 한 페이지 당 표시되는 그래프 수

namespace TomoAnalysis::Report::Plugins {
    struct GraphReportPanel::Impl {
        Ui::GraphReport* ui{ nullptr };
        ParameterId prevParameter{ ParameterId::MEAN_RI };
        ParameterId selectedParameter{ ParameterId::MEAN_RI };
        int selectedOrgan{ 0 };

        QList<QStringList> detailCategories;
        QList<QWidget*> detailGraphList;
        QList<QScrollBar*> detailScroll;
        QList<QBarCategoryAxis*> detailAxis;
        int numberOfHorizontalAxisTick{ 10 };

        QMap<QString, QStringList> dataTable;
        QString playground_path;
        QString cur_pg;
        QString cur_cube;
        QString cur_file;

        QList<TCChartView*> charViewList;

        Entity::WorkingSet::Pointer curWS{ nullptr };
    };

    GraphReportPanel::GraphReportPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui = new Ui::GraphReport;
        d->ui->setupUi(this);

        connect(d->ui->selectParameterWidget, &ParameterSelectionPanel::parameterChanged,
            [=](ParameterId id) { d->prevParameter = d->selectedParameter;  d->selectedParameter = id; emit keyChanged(); });        
        connect(d->ui->selectParameterWidget, &ParameterSelectionPanel::organIdxChanged, [=](int idx) {d->selectedOrgan = idx; emit keyChanged(); });

        connect(d->ui->captureOverviewGraphButton, SIGNAL(clicked()), this, SLOT(OnOverviewGraphExport()));
        connect(d->ui->exportOverviewTableButton, SIGNAL(clicked()), this, SLOT(OnOverviewTableExport()));

        connect(d->ui->yMinInputBox, SIGNAL(editingFinished()),
            this, SLOT(OnDetailsPlotYMinChanged()));
        connect(d->ui->yMaxInputBox, SIGNAL(editingFinished()),
            this, SLOT(OnDetailsPlotYMaxChanged()));

        connect(d->ui->detailsGraphCaptureButton, SIGNAL(clicked()), this, SLOT(OnDetailsGraphExport()));

        connect(d->ui->themeButton, SIGNAL(clicked(bool)), this, SLOT(OnGraphThemeSwitch(bool)));

        connect(d->ui->detailGraphListScrollBar, SIGNAL(valueChanged(int)),
            this, SLOT(OnGraphListSliderValueChanged(int)));

        d->ui->tableWidget->setProperty("styleVariant",1);        

        d->ui->themeButton->setObjectName("bt-switch");

        d->ui->yMaxInputBox->setSingleStep(0.001);
        d->ui->yMinInputBox->setSingleStep(0.001);

        d->ui->fileName->setReadOnly(true);
        connect(d->ui->correctBtn, SIGNAL(clicked()), this, SLOT(OnCorrectionClicked()));

        //d->ui->correctBtn->setEnabled(false);

        Init();
    }

    GraphReportPanel::~GraphReportPanel() {
        
    }    

    auto GraphReportPanel::Init() -> void {
        // overview table
        const QStringList rowHeader = {
            "Average",
            "SD",
            "MIN",
            "MAX",
            "Number"
        };

        d->ui->tableWidget->setRowCount(rowHeader.size());
        d->ui->tableWidget->setVerticalHeaderLabels(rowHeader);

        // overview box-plot
        auto overviewChart = new QChart;
        overviewChart->layout()->setContentsMargins(0, 0, 0, 0);
        overviewChart->setBackgroundRoundness(0);

        auto chartTitleFont = overviewChart->titleFont();
        chartTitleFont.setBold(true);
        overviewChart->setTitleFont(chartTitleFont);

        d->ui->overviewGraphView->setChart(overviewChart);
    }

    auto GraphReportPanel::UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> bool {
        // unused
        return true;
    }
    auto GraphReportPanel::DataToPresentation(QString tdata) -> QString {
        if (tdata == "membrane") {
            return QString("Whole Cell");
        }
        if (tdata == "lipid droplet") {
            return QString("Lipid droplet");
        }
        if (tdata == "nucleus") {
            return QString("Nucleus");
        }
        if (tdata == "nucleoli") {
            return QString("Nucleolus");
        }
        return tdata;
    }
    auto GraphReportPanel::PresentationToData(QString presentation) -> QString {
        if (presentation == "Whole Cell") {
            return QString("membrane");
        }
        if (presentation == "Lipid droplet") {
            return QString("lipid droplet");
        }
        if (presentation == "Nucleus") {
            return QString("nucleus");
        }
        if (presentation == "Nucleolus") {
            return QString("nucleoli");
        }
        return presentation;
    }

    auto GraphReportPanel::UpdateGraph(Entity::WorkingSet::Pointer workingset) -> bool {        
        if(d->ui->selectParameterWidget->UpdateOrgan2(workingset)) {
            d->selectedOrgan = d->ui->selectParameterWidget->GetCurrentOrganID();            
        }
        d->curWS = workingset;
        d->cur_pg = workingset->GetPlayground();

        d->dataTable.clear();
        d->playground_path.clear();

        auto parameterValue = GraphParameters::parameterSet.value(d->selectedParameter);
        //auto organelleValue = GraphParameters::organelleSet.value(d->selectedOrganelle);

        auto organ_list = workingset->GetOrganNames();        

        auto chartTitle = parameterValue.displayName + " - " + DataToPresentation(organ_list[d->selectedOrgan]);
        d->ui->graphTabChartTitleLabel->setText(chartTitle);
        d->ui->tableTabTableTitleLabel->setText(chartTitle);
        d->ui->detailsChartTitleLabel->setText(chartTitle);

        QString key = organ_list[d->selectedOrgan] + "." + parameterValue.keyName;

        // calculate graph & table value
        auto cubeNames = workingset->GetCubeNames();
        if (cubeNames.isEmpty()) {
            return false;
        }

        GraphDataSet cubeSeriesData;  // key: cube name, value: cell measured values

        for (int cubeIndex = 0; cubeIndex < cubeNames.count(); cubeIndex++) {
            QList<CellData> plotValues;
                        
            auto cubeName = cubeNames[cubeIndex];            

            d->dataTable[cubeName] = QStringList();

            QList<double> measuredValues;
            auto cube = workingset->GetCubeSet(cubeName);
            if (cube.measure.isEmpty()) {
                continue;
            }
            d->playground_path = cube.parentDir.chopped(1+cubeName.length());
            auto reportFiles = cube.measure.keys();  // report file name
            for (auto reportIndex = 0; reportIndex < reportFiles.size(); reportIndex++) {
                auto report = reportFiles[reportIndex];
                d->dataTable[cubeName].push_back(report.chopped(4));
                CellData cdata;
                QString cellName = "Unknown";

                auto splittedReport = report.split(".");
                if (splittedReport.size() >= 2) {
                    cellName = splittedReport[splittedReport.size() - 2];
                }

                cdata.reportName = cellName;
                cdata.reportIndex = reportIndex;

                auto timeMeasure = cube.measure[report][0]; //time step
                auto cellCount = timeMeasure->GetCellCount();

                for (auto cellIndex = 0; cellIndex < cellCount; cellIndex++) {
                    auto value = timeMeasure->GetMeasure(key, cellIndex);
                    if (value < 0) {
                        continue;
                    }
                    
                    if (parameterValue.keyName.compare(GraphParameters::parameterSet[ParameterId::MEAN_RI].keyName) == 0) {
                        value = round(value * 10000) / 10000;
                    } else {
                        value = round(value * 100) / 100;
                    }                    
                    if (value > 0) {
                        cdata.measuredValues.push_back(value);
                    }
                }

                if (cdata.measuredValues.count() > 0) {
                    plotValues.push_back(cdata);
                }
            }
            if (plotValues.count() > 0) {
                cubeSeriesData.insert(cubeName, plotValues);
            }
        }

        UpdateOverview(cubeSeriesData,cubeNames);
        UpdateDetails(cubeSeriesData,cubeNames);

        OnGraphThemeSwitch(d->ui->themeButton->isChecked());                        

        return true;
    }

    void GraphReportPanel::OnOverviewGraphExport() {
        auto desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

        auto filename = QFileDialog::getSaveFileName(
            this,
            "Export Hypercube Graph",
            desktopPath + "/untitled.png",
            "Images (*.png *.jpg)"
        );

        if (filename.isEmpty()) {
            return;
        }

        auto pixmap = d->ui->overviewGraphView->grab();
        pixmap.save(filename);
    }

    void GraphReportPanel::OnOverviewTableExport() {
        if (d->ui->tableWidget->columnCount() < 1) {
            std::cerr << "No data to export." << std::endl;
            return;
        }

        auto desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QSettings qs("Report/Graph/Table");
        auto prevPath = qs.value("prevPath", desktopPath).toString();

        auto filename = QFileDialog::getSaveFileName(
            this,
            "Export Hypercube Report",
            prevPath + "/untitled.csv",
            "CSV File(*.csv)"
        );

        if (filename.isEmpty()) {
            return;
        }
        QFileInfo fileInfo(filename);
        qs.setValue("prevPath", fileInfo.dir().path());


        QFile file(filename);
        if (file.open(QFile::WriteOnly | QFile::Truncate)) {
            auto table = d->ui->tableWidget;

            QTextStream stream(&file);
            QString contents;

            // row header 획득
            QStringList rowHeaders;
            auto verticalHeader = table->verticalHeader();
            if (verticalHeader) {
                for (auto row = 0; row < verticalHeader->count(); row++) {
                    auto headerItem = table->verticalHeaderItem(row);
                    if (headerItem) {
                        rowHeaders << headerItem->text();
                    }
                }
            }

            // column header 추가
            auto horizontalHeader = table->horizontalHeader();
            if (horizontalHeader) {
                if (!rowHeaders.isEmpty()) {
                    contents += ",";
                }

                for (auto column = 0; column < horizontalHeader->count(); column++) {
                    auto headerItem = table->horizontalHeaderItem(column);
                    if (headerItem) {
                        contents += headerItem->text() + ",";
                    }
                }
                contents += "\n";
            }

            // row 값 추가
            for (auto row = 0; row < table->rowCount(); row++) {
                if (!rowHeaders.isEmpty()) {
                    contents += rowHeaders[row] + ",";
                }
                    
                for (auto column = 0; column < table->columnCount(); column++) {
                    auto item = table->item(row, column);
                    if (item) {
                        QString value = item->text();
                        //value.replace(",", " ");
                        contents += value + ",";
                    }
                }

                contents += "\n";
            }

            stream << contents;
            file.close();
        }
    }

    void GraphReportPanel::OnDetailsPlotYMinChanged() {
        auto value = d->ui->yMinInputBox->value();

        for (int i = 0; i < d->ui->detailChartsLayout->count(); i++) {
            auto widget = d->ui->detailChartsLayout->itemAt(i)->widget();
            if (widget == nullptr) {
                continue;
            }

            auto chartView = widget->findChild<QChartView*>();
            if (chartView == nullptr) {
                continue;
            }

            if (chartView->chart()->axes(Qt::Vertical).isEmpty()) {
                continue;
            }

            auto axis = qobject_cast<QValueAxis*>(chartView->chart()->axes(Qt::Vertical).first());
            axis->setMin(value);
        }
    }

    void GraphReportPanel::OnDetailsPlotYMaxChanged() {
        auto value = d->ui->yMaxInputBox->value();

        for (int i = 0; i < d->ui->detailChartsLayout->count(); i++) {
            auto widget = d->ui->detailChartsLayout->itemAt(i)->widget();
            if (widget == nullptr) {
                continue;
            }

            auto chartView = widget->findChild<QChartView*>();
            if (chartView == nullptr) {
                continue;
            }

            if (chartView->chart()->axes(Qt::Vertical).isEmpty()) {
                continue;
            }

            auto axis = qobject_cast<QValueAxis*>(chartView->chart()->axes(Qt::Vertical).first());
            axis->setMax(value);
        }
    }

    void GraphReportPanel::OnDetailsGraphExport() {
        auto desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QSettings qs("Report/Graph/Export");
        auto prevPath = qs.value("prevPath", desktopPath).toString();

        auto filename = QFileDialog::getSaveFileName(
            this,
            "Export Cube Graph",
            prevPath + "/untitled.png",
            "Images (*.png *.jpg)"
        );

        if (filename.isEmpty()) {
            return;
        }

        QFileInfo fileInfo(filename);
        qs.setValue("prevPath", fileInfo.dir().path());

        QColor backgroundColor;
        QColor textColor;
        if (d->ui->themeButton->isChecked()) {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Dark].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Dark].text;
        }
        else {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Light].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Light].text;
        }

        const int captureFontSize = 24;  // px
                
        QSize collageSize(0, 0);
        QList<QPixmap> grabbedPixmaps;

        for (int i = 0; i < d->ui->detailChartsLayout->count(); i++) {
            auto widget = d->ui->detailChartsLayout->itemAt(i)->widget();
            if (widget == nullptr) {
                continue;
            }

            auto view = widget->findChild<QChartView*>();
            if (view == nullptr) {
                continue;
            }

            auto chart = view->chart();
            if (chart->series().isEmpty()) {
                continue;
            }

            // duplicate charts to capture whole x range
            auto series = qobject_cast<QBarSeries*>(chart->series().first());
            if (series == nullptr) {
                continue;
            }

            QString cubeName;
            auto duplBarSeries = new QBarSeries;
            for (auto barset : series->barSets()) {
                cubeName = barset->label();
                auto duplBarset = new QBarSet(cubeName);
                duplBarset->setBrush(barset->brush());

                for (int j = 0; j < barset->count(); j++) {
                    *duplBarset << barset->at(j);
                }

                duplBarSeries->append(duplBarset);
            }

            auto duplChart = new QChart;
            duplChart->layout()->setContentsMargins(0, 0, 0, 0);
            duplChart->setBackgroundRoundness(chart->backgroundRoundness());
            duplChart->legend()->hide();
            duplChart->addSeries(duplBarSeries);
            duplChart->setTitle(chart->title());

            auto chartFont = chart->titleFont();
            chartFont.setPixelSize(captureFontSize);
            duplChart->setTitleFont(chartFont);
            duplChart->setBackgroundBrush(chart->backgroundBrush());
            duplChart->setTitleBrush(chart->titleBrush());

            if (!chart->axes(Qt::Horizontal).isEmpty()) {
                auto axisX = qobject_cast<QBarCategoryAxis*>(chart->axes(Qt::Horizontal).first());
                
                auto duplAxisX = new QBarCategoryAxis;
                duplAxisX->append(axisX->categories());

                duplAxisX->setLabelsColor(axisX->labelsColor());

                auto labelFont = duplAxisX->labelsFont();
                labelFont.setPixelSize(captureFontSize);
                duplAxisX->setLabelsFont(labelFont);                

                duplChart->addAxis(duplAxisX, Qt::AlignBottom);
                duplBarSeries->attachAxis(duplAxisX);
            }

            if (!chart->axes(Qt::Vertical).isEmpty()) {
                auto axisY = qobject_cast<QValueAxis*>(chart->axes(Qt::Vertical).first());

                auto duplAxisY = new QValueAxis;
                duplAxisY->setTitleText(axisY->titleText());
                duplAxisY->setRange(axisY->min(), axisY->max());

                duplAxisY->setLabelsColor(axisY->labelsColor());

                auto labelFont = duplAxisY->labelsFont();
                labelFont.setPixelSize(captureFontSize);
                duplAxisY->setLabelsFont(labelFont);

                auto axisTitleFont = duplAxisY->titleFont();
                axisTitleFont.setPixelSize(captureFontSize);
                duplAxisY->setTitleFont(axisTitleFont);
                duplAxisY->setTitleBrush(axisY->titleBrush());

                duplChart->addAxis(duplAxisY, Qt::AlignLeft);
                duplBarSeries->attachAxis(duplAxisY);
            }

            auto cubeIconLabel = new QLabel;
            cubeIconLabel->setPixmap(QIcon(":/img/ic-explorer-cube.svg").pixmap(32, 32));

            auto cubeTitleLabel = new QLabel(cubeName);
            cubeTitleLabel->setStyleSheet(QString("color: %1;font-size: %2px;").arg(textColor.name().arg(captureFontSize)));

            auto titleLayout = new QHBoxLayout;
            titleLayout->setContentsMargins(0, 0, 0, 0);
            titleLayout->addWidget(cubeIconLabel);
            titleLayout->addWidget(cubeTitleLabel);
            titleLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding));

            auto duplChartView = new QChartView(duplChart);

            auto duplLayout = new QVBoxLayout;
            duplLayout->setContentsMargins(0, 0, 0, 0);
            duplLayout->addLayout(titleLayout);
            duplLayout->addWidget(duplChartView);
            
            QWidget duplChartWidget;
            duplChartWidget.setLayout(duplLayout);
            duplChartWidget.setFixedWidth(1920);

            // add captured image and get image size
            auto chartPixmap = duplChartWidget.grab();
            grabbedPixmaps << chartPixmap;
            collageSize.setHeight(collageSize.height() + chartPixmap.height());
            if (i == 0) {
                collageSize.setWidth(chartPixmap.width());
            }
        }

        if (grabbedPixmaps.isEmpty()) {
            return;
        }

        // 모든 그래프 이미지를 하나의 이미지로 취합
        QPixmap collage(collageSize);
        collage.fill(backgroundColor);
        QPainter painter(&collage);

        int y = 0;
        for (auto pixmap : grabbedPixmaps) {
            painter.drawPixmap(
                QRectF(0, y, pixmap.width(), pixmap.height()),
                pixmap,
                QRectF(0, 0, pixmap.width(), pixmap.height())
            );

            y += pixmap.height();
        }

        collage.save(filename);
    }

    void GraphReportPanel::OnGraphThemeSwitch(bool checked) {
        QColor backgroundColor;
        QColor textColor;

        // 변경될 색상 지정
        if (checked) {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Dark].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Dark].text;
        } else {
            backgroundColor = GraphColor::graphThemeSet[GraphTheme::Light].background;
            textColor = GraphColor::graphThemeSet[GraphTheme::Light].text;
        }

        // overview 그래프 색상 설정
        auto overviewChart = d->ui->overviewGraphView->chart();
        if (overviewChart) {
            overviewChart->setBackgroundBrush(QBrush(backgroundColor));
            overviewChart->setTitleBrush(QBrush(textColor));

            if (!overviewChart->axes(Qt::Horizontal).isEmpty()) {
                overviewChart->axes(Qt::Horizontal).first()->setLabelsColor(textColor);
            }

            if (!overviewChart->axes(Qt::Vertical).isEmpty()) {
                overviewChart->axes(Qt::Vertical).first()->setLabelsColor(textColor);
                overviewChart->axes(Qt::Vertical).first()->setTitleBrush(QBrush(textColor));
            }
        }

        // detail view 그래프 색상 설정
        for (int i = 0; i < d->ui->detailChartsLayout->count(); i++) {
            auto widget = d->ui->detailChartsLayout->itemAt(i)->widget();
            if (widget == nullptr) {
                continue;
            }

            auto view = widget->findChild<QChartView*>();
            if (view == nullptr) {
                continue;
            }

            auto chart = view->chart();
            if (chart == nullptr) {
                continue;
            }

            chart->setBackgroundBrush(QBrush(backgroundColor));
            chart->setTitleBrush(QBrush(textColor));

            if (!chart->axes(Qt::Horizontal).isEmpty()) {
                chart->axes(Qt::Horizontal).first()->setLabelsColor(textColor);
            }

            if (!chart->axes(Qt::Vertical).isEmpty()) {
                chart->axes(Qt::Vertical).first()->setLabelsColor(textColor);
                chart->axes(Qt::Vertical).first()->setTitleBrush(QBrush(textColor));
            }
        }
    }

    void GraphReportPanel::OnGraphListSliderValueChanged(int value) {
        auto graphIndex = value * numDetailsGraphPerPage;

        for (auto item : d->detailGraphList) {
            item->hide();
        }

        for (auto i = 0; i < numDetailsGraphPerPage; i++) {
            if (graphIndex + i < d->detailGraphList.size()) {
                d->detailGraphList.at(graphIndex + i)->show();
            } else {
                break;
            }
        }
    }

    auto GraphReportPanel::CalcCubeData(QList<double> cdata)->resultData {
        CalcResult result;

        std::sort(cdata.begin(), cdata.end());

        result.cells = cdata.size();

        double sum = std::accumulate(cdata.constBegin(), cdata.constEnd(), 0.0);
        result.average = sum / result.cells;

        const auto [min, max] = std::minmax_element(cdata.constBegin(), cdata.constEnd());
        result.min = *min;
        result.max = *max;

        double variance = 0.0;
        for (auto val : cdata) {
            variance += pow(val - result.average, 2);
        }

        variance = variance / result.cells;
        result.stdev = sqrt(variance);

        // calculate quartile
        QList<double> lowerValues, upperValues;

        // get median(Q2)
        if (cdata.size() % 2 == 0) {
            result.q2 = (cdata[cdata.size() / 2 - 1] + cdata[cdata.size() / 2]) / 2.0;

            lowerValues = cdata.mid(0, cdata.size() / 2);
            upperValues = cdata.mid(cdata.size() / 2, cdata.size() - lowerValues.size());
        }
        else {
            result.q2 = cdata[cdata.size() / 2];

            lowerValues = cdata.mid(0, cdata.size() / 2);
            upperValues = cdata.mid(cdata.size() / 2 + 1, cdata.size() - lowerValues.size() - 1);
        }

        // get Q1
        if (lowerValues.size() % 2 == 0) {
            double val1 = lowerValues[lowerValues.size() / 2 - 1];
            double val2 = lowerValues[lowerValues.size() / 2];

            //lowerQuartile = val1 + (val2 - val1) * 0.25;
            result.q1 = (val1 + val2) / 2.0;
        }
        else {
            result.q1 = lowerValues[lowerValues.size() / 2];
        }

        // get Q3
        if (upperValues.size() % 2 == 0) {
            double val1 = upperValues[upperValues.size() / 2 - 1];
            double val2 = upperValues[upperValues.size() / 2];

            //upperQuartile = val1 + (val2 - val1) * 0.75;
            result.q3 = (val1 + val2) / 2.0;
        }
        else {
            result.q3 = upperValues[upperValues.size() / 2];
        }
        
        return result;
    }

    auto GraphReportPanel::UpdateOverview(const GraphDataSet& odata, const QStringList& nameOrder) -> bool {
        if (odata.isEmpty()) {
            QMessageBox::warning(nullptr, "Error", QString("There is no valid measure in %1").arg(GraphParameters::parameterSet[d->selectedParameter].keyName));
            return false;
        }

        auto overviewChart = d->ui->overviewGraphView->chart();
 
        // clear graph & table
        d->ui->tableWidget->clearContents();

        overviewChart->removeAllSeries();        

        // insert table values and draw box-plot
        d->ui->tableWidget->setColumnCount(odata.size());
        //d->ui->tableWidget->setHorizontalHeaderLabels(odata.keys());
        d->ui->tableWidget->setHorizontalHeaderLabels(nameOrder);

        int column = 0;

        double yMin, yMax, compareMin, compareMax;  // y축 값 범위 구하는 용도
        yMin = yMax = compareMin = compareMax = 0.0;

        QBoxPlotSeries* boxplotSeries = new QBoxPlotSeries;
        
        //GraphDataSet::const_iterator it = odata.constBegin();        
        for(auto i=0;i<nameOrder.count();i++){
            auto it = odata[nameOrder[i]];
            if (it.isEmpty()) {
                column++;
                continue;
            }

            QList<double> measuredValues;
            for (auto cellData : it) {
                measuredValues.append(cellData.measuredValues);
            }

            if (measuredValues.isEmpty()) {
                column++;
                continue;
            }

            CalcResult result = CalcCubeData(measuredValues);
                        
            if(i==0){
                compareMin = result.min;
                compareMax = result.max;

                auto iqr = result.q3 - result.q1;
                yMin = result.min - iqr / 3.0;// result.q1 - 1.5 * iqr;
                if(yMin < 0) {
                    yMin = 0;
                }
                yMax = result.max + iqr / 3.0;// result.q3 + 1.5 * iqr;
            }
            else {
                auto iqr = result.q3 - result.q1;
                if (compareMin > result.min) {
                    compareMin = result.min;
                    yMin = result.min - iqr / 3.0 ;// result.q1 - (3.0 * iqr);
                    if(yMin<0) {
                        yMin = 0;
                    }                    
                }

                if (compareMax < result.max) {
                    compareMax = result.max;
                    yMax = result.max + iqr / 3.0;// result.q3 + (3.0 * iqr);                    
                }
            }

            // table 값 입력
            d->ui->tableWidget->setItem(0, column, new QTableWidgetItem(QString::number(result.average)));
            d->ui->tableWidget->setItem(1, column, new QTableWidgetItem(QString::number(result.stdev)));
            d->ui->tableWidget->setItem(2, column, new QTableWidgetItem(QString::number(result.min)));
            d->ui->tableWidget->setItem(3, column, new QTableWidgetItem(QString::number(result.max)));
            d->ui->tableWidget->setItem(4, column, new QTableWidgetItem(QString::number(result.cells)));

            // graph 값 입력
            auto barColor = GraphColor::colorPalette.at(boxplotSeries->count() % GraphColor::colorPalette.size());
                        
            auto boxSet = new QBoxSet(result.min, result.q1, result.q2, result.q3, result.max, nameOrder[i]);
            //auto boxSet = new TCBoxSet(result.min, result.q1, result.q2, result.q3, result.max, it.key());
            boxSet->setBrush(QColor(barColor));
            boxplotSeries->append(boxSet);

            column++;                        
        }

        auto parameterValue = GraphParameters::parameterSet.value(
            ParameterId::_from_integral(d->selectedParameter),
            { "", "" }
        );

        auto organ_list = d->curWS->GetOrganNames();

        overviewChart->addSeries(boxplotSeries);

        overviewChart->legend()->hide();
        overviewChart->setTitle(DataToPresentation(organ_list[d->selectedOrgan]));

        // setup axes
        overviewChart->createDefaultAxes();

        auto axisY = overviewChart->axes(Qt::Vertical).first();
        axisY->setRange(yMin, yMax);

        auto yAxisTitle = parameterValue.displayName;
        switch (d->selectedParameter) {
        case ParameterId::VOLUME:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        case ParameterId::SURFACE_AREA:
        case ParameterId::PROJECTED_AREA:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B2"));
            break;
        case ParameterId::DRYMASS:
            yAxisTitle += "<br>(pg)";
            break;
        case ParameterId::PROTEIN_CONCENTRATION:
            yAxisTitle += QString("<br>(pg/%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        default:
            break;
        }

        axisY->setTitleText(yAxisTitle);
        
        return true;
    }
    void GraphReportPanel::OnCorrectionClicked() {
        auto text = d->ui->fileName->text();
        if (text.isEmpty())
            return;
        emit sigCorrection(d->cur_pg, d->cur_cube, d->cur_file);
    }

    void GraphReportPanel::OnBarSelected() {
        auto s = static_cast<TCChartView*>(sender());
        for(auto l:d->charViewList) {
            if(s != l) {
                l->HideBarSelection();
            }
        }
    }

    void GraphReportPanel::OnBarSetClicked(int idx,QBarSet* barSet) {
        auto tcBarSet = static_cast<TCBarSet*>(barSet);        
        auto sum = 0;
        auto prev_sum = 0;
        auto img_idx = 0;
        auto cell_idx = 0;
        auto bar_info = tcBarSet->GetBarInfo();

        for (auto i = 0; i<bar_info.count();i++) {
            sum += bar_info[i];
            if (idx < sum) {
                img_idx = i;
                cell_idx = idx - prev_sum;
                break;
            }
            prev_sum += bar_info[i];
        }                
        auto split = d->playground_path.split("/");
        auto time_stamp = split[split.size() - 1];
        auto hyperName = split[split.size() - 2];

        QString realPath = d->playground_path.chopped(time_stamp.length() + hyperName.length() + 1);
        realPath += (hyperName + "_" + time_stamp + ".tcpg");
                
        d->cur_pg = realPath;
        auto cubeName = barSet->label();        
        d->cur_cube = cubeName;
        auto imgName = d->dataTable[cubeName][img_idx];                
        d->cur_file = imgName;
        d->ui->fileName->setText(d->cur_file);
        
        QString cellID = QString::number(img_idx + 1) + "_" + QString::number(cell_idx + 1);
        QString organ_name = tcBarSet->GetParentTitle();
        emit barSelection(cubeName,imgName,cellID,organ_name);
    }
    void GraphReportPanel::SelectionFromTable(QString cubeName, QString filename, QString cellID,int time_step,QString organ_name) {
        Q_UNUSED(time_step)
        d->cur_cube = cubeName;
        d->cur_file = filename;

        d->ui->fileName->setText(d->cur_file);

        //remove existing selection
        for (auto l : d->charViewList) {
            l->HideBarSelection();            
        }
        //set new selection
        for(auto l : d->charViewList) {            
            if(l->whatsThis() == cubeName) {
                auto chart = l->chart();                
                if (chart->title() == organ_name) {
                    auto axisX = dynamic_cast<QBarCategoryAxis*>(chart->axes(Qt::Horizontal).first());
                    auto cat = axisX->categories();
                    auto index = cat.indexOf(cellID);
                    l->SetBarSelected(index);
                    break;
                }
            }
        }
        
    }
    auto GraphReportPanel::UpdateDetails(const GraphDataSet& cdata, const QStringList& nameOrder) -> bool {
        if (cdata.isEmpty()) {
            d->selectedParameter = d->prevParameter;
            d->ui->selectParameterWidget->SetCurrentKey(d->prevParameter);            
            return false;
        }

        auto parameterValue = GraphParameters::parameterSet.value(
            ParameterId::_from_integral(d->selectedParameter),
            { "", "" }
        );
        
        auto yAxisTitle = parameterValue.displayName;
        switch (d->selectedParameter) {
        case ParameterId::VOLUME:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        case ParameterId::SURFACE_AREA:
        case ParameterId::PROJECTED_AREA:
            yAxisTitle += QString("<br>(%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B2"));
            break;
        case ParameterId::DRYMASS:
            yAxisTitle += "<br>(pg)";
            break;
        case ParameterId::PROTEIN_CONCENTRATION:
            yAxisTitle += QString("<br>(pg/%1m%2)")
                .arg(QString::fromWCharArray(L"\x00B5"))
                .arg(QString::fromWCharArray(L"\x00B3"));
            break;
        default:
            break;
        }                

        // delete all detail chart        
        //while (!d->ui->detailChartsLayout->isEmpty()) {
        while(d->ui->detailChartsLayout->count() > 0){
            auto layoutItem = d->ui->detailChartsLayout->takeAt(0);
            auto widget = layoutItem->widget();
            auto children = widget->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly);

            qDeleteAll(children);
            children.clear();
                        
            delete widget;
            delete layoutItem;
        }
        
        d->detailGraphList.clear();
        d->detailScroll.clear();
        d->detailAxis.clear();
        d->detailCategories.clear();
        d->charViewList.clear();        
        double yMin, yMax;  // y축 값 범위 구하는 용도
        yMin = yMax = 0.0;

        // cube 별 bar chart 생성
        int dataIdx = -1;

        auto organ_list = d->curWS->GetOrganNames();

        for(auto j=0;j<nameOrder.count();j++){
            auto it = cdata[nameOrder[j]];
            if (it.isEmpty()) {                
                continue;
            }
            dataIdx++;
            QList<int> bar_info;
            //QStringList categories; // x-axis labels

            d->detailCategories.push_back(QStringList());

            auto series = new QBarSeries;            
            auto barset = new TCBarSet(nameOrder[j]);
            
            for (auto i = 0; i < it.count(); i++) {                
                auto cellData = it[i];                
                QList<double>* values = &cellData.measuredValues;
                auto cell_num = 0;
                for (auto index = 0; index < values->size(); index++) {                    
                    //d->detailCategories << QString::number(cellData.reportIndex+1) + "-" + QString::number(index + 1);
                    d->detailCategories[dataIdx] << QString::number(cellData.reportIndex + 1) + "_" + QString::number(index + 1);
                    *barset << (*values)[index];
                    cell_num++;
                }
                bar_info.push_back(cell_num);

                const auto [min, max] = std::minmax_element(values->begin(), values->end());
                if (j==0 && i == 0) {
                    yMin = *min;
                    yMax = *max;
                } else {
                    if (yMin > *min) {
                        yMin = *min;
                    }

                    if (yMax < *max) {
                        yMax = *max;
                    }
                }
            }
            barset->SetBarInfo(bar_info);
            QString barColor = GraphColor::colorPalette.at(d->ui->detailChartsLayout->count() % GraphColor::colorPalette.size());
            
            barset->setBrush(QColor(barColor));

            series->append(barset);
            barset->SetParentTitle(DataToPresentation(organ_list[d->selectedOrgan]));
            auto chart = new QChart;
            chart->layout()->setContentsMargins(0, 0, 0, 0);
            chart->setBackgroundRoundness(0);
            chart->legend()->hide();
            chart->addSeries(series);
            chart->setTitle(DataToPresentation(organ_list[d->selectedOrgan]));
            chart->setMinimumHeight(200);
            auto chartTitleFont = chart->titleFont();
            chartTitleFont.setBold(true);
            chart->setTitleFont(chartTitleFont);
            
            // setup axes
            QFont labelsFont;
            labelsFont.setPixelSize(9);

            auto axisX = new QBarCategoryAxis;
            axisX->append(d->detailCategories[dataIdx]);            
            //axisX->setLabelsFont(labelsFont);

            auto axisY = new QValueAxis;
            axisY->setTitleText(yAxisTitle);
            //axisY->setLabelsFont(labelsFont);
            chart->addAxis(axisX, Qt::AlignBottom);
            chart->addAxis(axisY, Qt::AlignLeft);

            series->attachAxis(axisX);
            series->attachAxis(axisY);

            // setup cube title, chart and scroll ui
            auto cubeIconLabel = new QLabel;
            cubeIconLabel->setPixmap(QIcon(":/img/ic-explorer-cube.svg").pixmap(16, 16));

            auto cubeLabelLayout = new QHBoxLayout;
            cubeLabelLayout->setContentsMargins(0, 0, 0, 0);
            cubeLabelLayout->addWidget(cubeIconLabel);
            cubeLabelLayout->addWidget(new QLabel(nameOrder[j]));
            cubeLabelLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding));
            
            auto cubeLabelFrame = new QFrame;
            cubeLabelFrame->setLayout(cubeLabelLayout);

            //auto chartView = new QChartView(chart);
            auto chartView = new TCChartView(chart);
            chartView->setWhatsThis(nameOrder[j]);
            d->charViewList << chartView;

            QScrollBar* scrollBar = nullptr;
            //if (axisX->count() > d->numberOfHorizontalAxisTick) {
                scrollBar = new QScrollBar(Qt::Horizontal);
                scrollBar->setSingleStep(1);
                scrollBar->setPageStep(d->numberOfHorizontalAxisTick);
                scrollBar->setRange(0, d->detailCategories[dataIdx].size() - d->numberOfHorizontalAxisTick);
                scrollBar->setValue(0);

                connect(scrollBar, &QScrollBar::valueChanged, [=](int value) {
                    int maxCategoryIndex = value + d->numberOfHorizontalAxisTick - 1;
                    if (maxCategoryIndex > d->detailCategories[dataIdx].size() - 1) {
                        maxCategoryIndex = d->detailCategories[dataIdx].size() - 1;
                    }

                    axisX->setRange(d->detailCategories[dataIdx].at(value), d->detailCategories[dataIdx].at(maxCategoryIndex));
                 });

                auto axis_max = d->numberOfHorizontalAxisTick - 1;
            if(axis_max > d->detailCategories[dataIdx].size()-1) {
                axis_max = d->detailCategories[dataIdx].size() - 1;
            }
            
                axisX->setRange(d->detailCategories[dataIdx].at(0), d->detailCategories[dataIdx].at(axis_max));
            //}
                connect(barset, SIGNAL(pressed(int)), chartView, SLOT(OnBarClicked(int)));
                connect(chartView, SIGNAL(sigBarSelected()), this, SLOT(OnBarSelected()));
                /*QGraphicsRectItem hoverItem;
                hoverItem.setBrush(QBrush(Qt::red));
                hoverItem.setPen(Qt::NoPen);
                QObject::connect(barset, &QBarSet::clicked, [&chartView,&hoverItem]( int index) {
                    QPoint p = chartView->mapFromGlobal(QCursor::pos());                    
                    std::cout << "Item clicked at " << p.x()<<" " <<p.y() << std::endl;
                        QGraphicsItem* it = chartView->itemAt(p);
                        hoverItem.setParentItem(it);
                        hoverItem.setRect(it->boundingRect());
                        hoverItem.show();                    
                });*/

            auto chartItemLayout = new QVBoxLayout;
            chartItemLayout->setContentsMargins(0, 0, 0, 0);
            chartItemLayout->setSpacing(0);
            chartItemLayout->addWidget(cubeLabelFrame);
            chartItemLayout->addWidget(chartView, 1);
            
            if (scrollBar) {                
                chartItemLayout->addWidget(scrollBar);                
                d->detailScroll << scrollBar;
                d->detailAxis << axisX;
            }

            auto chartItemWidget = new QWidget;
            chartItemWidget->setLayout(chartItemLayout);

            d->ui->detailChartsLayout->addWidget(chartItemWidget, 1);

            d->detailGraphList << chartItemWidget;            

            connect(series, SIGNAL(clicked(int, QBarSet*)), this, SLOT(OnBarSetClicked(int, QBarSet*)));
        }

        // 모든 bar chart의 y축 범위 같게 설정
        for (int i = 0; i < d->ui->detailChartsLayout->count(); i++) {
            auto widget = d->ui->detailChartsLayout->itemAt(i)->widget();
            if (widget == nullptr) {
                continue;
            }

            auto view = widget->findChild<QChartView*>();
            if (view == nullptr) {
                continue;
            }

            if (view->chart()->axes(Qt::Vertical).isEmpty()) {
                continue;
            }

            auto axis = qobject_cast<QValueAxis*>(view->chart()->axes(Qt::Vertical).first());
            axis->setRange(yMin, yMax);
        }

        d->ui->yMinInputBox->setValue(yMin);
        d->ui->yMaxInputBox->setValue(yMax);

        // set a vertical scroll bar for detail graphs
        if (d->detailGraphList.size() > numDetailsGraphPerPage) {
            auto max = d->detailGraphList.size() / numDetailsGraphPerPage;
            if (d->detailGraphList.size() % numDetailsGraphPerPage == 0) {
                max--;
            }

            d->ui->detailGraphListScrollBar->setRange(0, max);
            d->ui->detailGraphListScrollBar->show();

            OnGraphListSliderValueChanged(0);
        } else {
            d->ui->detailGraphListScrollBar->hide();
        }

        return true;
    }
    void GraphReportPanel::resizeEvent(QResizeEvent* event) {
        //std::cout << "Graph panel resized " << event->size().width() <<  " " <<event->size().height() << std::endl;

        auto minus = (2510 - event->size().width()) / 100 +2;
        if(minus < 0) {
            minus = 0;
        }
        d->numberOfHorizontalAxisTick = 20 - minus;
        auto value = 0;        
        for(auto i =0; i<d->detailScroll.count();i++){
            auto sc = d->detailScroll[i];
            sc->blockSignals(true);            
            sc->setPageStep(d->numberOfHorizontalAxisTick);
            sc->setRange(0, d->detailCategories[i].size() - d->numberOfHorizontalAxisTick);
            sc->setValue(0);
            sc->blockSignals(false);
        }
        for(auto i=0;i<d->detailAxis.count();i++){
            auto axis = d->detailAxis[i];
            axis->blockSignals(true);
            int maxCategoryIndex = value + d->numberOfHorizontalAxisTick - 1;
            if (maxCategoryIndex > d->detailCategories[i].size() - 1) {
                maxCategoryIndex = d->detailCategories[i].size() - 1;
            }
            //axis->setRange(d->detailCategories[i].at(value), d->detailCategories[i].at(maxCategoryIndex));            
            axis->setRange(d->detailCategories[i].at(value), d->detailCategories[i].at(maxCategoryIndex));
            axis->blockSignals(false);
        }

        QWidget::resizeEvent(event);
    }    
}
