#include "TCChart.h"

struct TCChart::Impl {
    
};

TCChart::TCChart() : QChart(), d{ new Impl } {
    
}

TCChart::~TCChart() {
    
}

void TCChart::resizeEvent(QGraphicsSceneResizeEvent* event) {
    QChart::resizeEvent(event);
}