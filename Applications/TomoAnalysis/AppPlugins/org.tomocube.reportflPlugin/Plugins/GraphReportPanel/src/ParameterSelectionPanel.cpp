#include <iostream>

#include <QButtonGroup>
#include <QRadioButton>
#include <QVBoxLayout>

#include "ui_ParameterSelectionPanel.h"
#include "ParameterSelectionPanel.h"

namespace TomoAnalysis::Report::Plugins {
    // Todo: rep list, parameter/organelle list 분리하여 singleshot/time-lapse 같이 사용
 
    struct ParameterSelectionPanel::Impl {
        Ui::ParameterSelectionPanel* ui{ nullptr };

        QButtonGroup parameterButtonGroup;
        QButtonGroup organelleButtonGroup;
        QSpacerItem* spacer;
        QStringList cur_organ_names;
    };

    ParameterSelectionPanel::ParameterSelectionPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ParameterSelectionPanel;
        d->ui->setupUi(this);

        connect(&d->parameterButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)),
            this, SLOT(OnParameterChanged(QAbstractButton*)));
        connect(&d->organelleButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)),
            this, SLOT(OnOrganelleChanged(QAbstractButton*)));

        Init();
    }

    ParameterSelectionPanel::~ParameterSelectionPanel() {

    }

    auto ParameterSelectionPanel::Init() -> void {
        // parameter 선택 ui
        QVBoxLayout* paremetersLayout = new QVBoxLayout;
        QMap<ParameterId, OptionSet>::const_iterator parameterIt = GraphParameters::parameterSet.constBegin();
        while (parameterIt != GraphParameters::parameterSet.constEnd()) {
            auto button = new QRadioButton(parameterIt.value().displayName);
            paremetersLayout->addWidget(button);
            d->parameterButtonGroup.addButton(button, parameterIt.key());            
            if (parameterIt.key() == +ParameterId::MEAN_RI) {
                button->setChecked(true);
            }

            ++parameterIt;
        }

        paremetersLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Fixed, QSizePolicy::Expanding));
        d->ui->parametersGroupBox->setLayout(paremetersLayout);

        // organelle 선택 ui 
        QVBoxLayout* organellesLayout = new QVBoxLayout;
        QMap<OrganelleId, OptionSet>::const_iterator organelleIt = GraphParameters::organelleSet.constBegin();
        while (organelleIt != GraphParameters::organelleSet.constEnd()) {
            auto button = new QRadioButton(organelleIt.value().displayName);
            organellesLayout->addWidget(button);
            d->organelleButtonGroup.addButton(button, organelleIt.key());

            if (organelleIt.key() == +OrganelleId::MEMBRANE) {
                button->setChecked(true);                
            }

            ++organelleIt;
        }
        d->spacer = new QSpacerItem(1, 1, QSizePolicy::Fixed, QSizePolicy::Expanding);
        organellesLayout->addSpacerItem(d->spacer);
        d->ui->organellesGroupBox->setLayout(organellesLayout);
    }

    void ParameterSelectionPanel::OnParameterChanged(QAbstractButton* button) {
        Q_UNUSED(button);
        
        emit parameterChanged(
            ParameterId::_from_integral(d->parameterButtonGroup.checkedId())
        );
    }

    void ParameterSelectionPanel::OnOrganelleChanged(QAbstractButton* button) {
        Q_UNUSED(button);        
        emit organlleChanged(
            OrganelleId::_from_integral(d->organelleButtonGroup.checkedId())
        );
        emit organIdxChanged(d->organelleButtonGroup.checkedId());
    }
    auto ParameterSelectionPanel::GetCurrentOrganID() -> int {        
        return d->organelleButtonGroup.checkedId();
    }
    auto ParameterSelectionPanel::GetCurrentOrgan() -> OrganelleId {
        return OrganelleId::_from_integral(d->organelleButtonGroup.checkedId());
    }
    auto ParameterSelectionPanel::DataToPresentation(QString tdata) -> QString {
        if (tdata == "membrane") {
            return QString("Whole Cell");
        }
        if (tdata == "lipid droplet") {
            return QString("Lipid droplet");
        }
        if (tdata == "nucleus") {
            return QString("Nucleus");
        }
        if (tdata == "nucleoli") {
            return QString("Nucleolus");
        }
        return tdata;
    }
    auto ParameterSelectionPanel::PresentationToData(QString presentation) -> QString {
        if (presentation == "Whole Cell") {
            return QString("membrane");
        }
        if (presentation == "Lipid droplet") {
            return QString("lipid droplet");
        }
        if (presentation == "Nucleus") {
            return QString("nucleus");
        }
        if (presentation == "Nucleolus") {
            return QString("nucleoli");
        }
        return presentation;
    }
    auto ParameterSelectionPanel::IsBasic(QStringList names) -> bool {
        if(names.count() != 4) {
            return false;
        }
        if(false == names.contains("membrane")) {
            return false;
        }
        if(false == names.contains("lipid droplet")) {
            return false;
        }
        if(false == names.contains("nucleus")) {
            return false;
        }
        if(false == names.contains("nucleoli")) {
            return false;
        }
        return true;
    }
    auto ParameterSelectionPanel::UpdateOrgan2(Entity::WorkingSet::Pointer workingset) -> bool {
        auto ori_org_list = workingset->GetOrganNames();        
        auto org_list = ori_org_list;
        if (IsBasic(org_list)) {
            org_list = QStringList{ "membrane","nucleus","nucleoli","lipid droplet" };
        }
        if (d->cur_organ_names == org_list) {
            return false;
        }               
        auto organellesLayout = static_cast<QVBoxLayout*>(d->ui->organellesGroupBox->layout());
        for (auto b : d->organelleButtonGroup.buttons()) {
            d->organelleButtonGroup.removeButton(b);
            delete b;
        }
        organellesLayout->removeItem(d->spacer);
        for (auto i = 0; i < org_list.count(); i++) {            
            auto button = new QRadioButton(DataToPresentation(org_list[i]));
            organellesLayout->addWidget(button);            
            d->organelleButtonGroup.addButton(button, ori_org_list.indexOf(org_list[i]));
            if (i == 0) {
                button->setChecked(true);
            }
        }
        organellesLayout->addSpacerItem(d->spacer);
        d->cur_organ_names = org_list;
        return true;
    }

    auto ParameterSelectionPanel::UpdateOrgan(Entity::WorkingSet::Pointer workingset) -> bool {        
        auto org_list = workingset->GetOrganNames();
        if(d->cur_organ_names == org_list) {
            return false;
        }
        auto organellesLayout = static_cast<QVBoxLayout*>(d->ui->organellesGroupBox->layout());
        for (auto b : d->organelleButtonGroup.buttons()) {
            d->organelleButtonGroup.removeButton(b);
            delete b;            
        }        
        organellesLayout->removeItem(d->spacer);
        QMap<OrganelleId, OptionSet>::const_iterator organelleIt = GraphParameters::organelleSet.constBegin();
        while (organelleIt != GraphParameters::organelleSet.constEnd()) {
            if (org_list.contains(organelleIt.value().keyName)) {
                auto button = new QRadioButton(organelleIt.value().displayName);
                organellesLayout->addWidget(button);
                d->organelleButtonGroup.addButton(button, organelleIt.key());                
                if (organelleIt.key() == +OrganelleId::MEMBRANE) {
                    button->setChecked(true);
                }                
            }
            ++organelleIt;
        }        
        //d->spacer = new QSpacerItem(1, 1, QSizePolicy::Fixed, QSizePolicy::Expanding);
        organellesLayout->addSpacerItem(d->spacer);
        //d->ui->organellesGroupBox->setLayout(organellesLayout);
        d->cur_organ_names = org_list;                

        return true;
    }        
    auto ParameterSelectionPanel::SetCurrentKey(ParameterId id) -> void {
        auto btn = d->parameterButtonGroup.button(id._to_integral());
        //btn->setChecked(true);
        btn->click();
        //d->parameterButtonGroup.idClicked(id._to_index());
    }
}
