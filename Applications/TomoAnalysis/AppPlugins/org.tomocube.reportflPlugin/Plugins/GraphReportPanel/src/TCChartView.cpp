#include <iostream>

#include "TCChartView.h"

struct TCChartView::Impl {
    QGraphicsRectItem hoverItem;
};

TCChartView::TCChartView(QChart* chart,QWidget* parent) : QChartView(chart,parent), d{ new Impl } {
    d->hoverItem.setBrush(QBrush(Qt::yellow));
    d->hoverItem.setPen(Qt::NoPen);
}

TCChartView::~TCChartView() {
    
}


void TCChartView::OnBarClicked(int index) {
    Q_UNUSED(index)
    auto p = mapFromGlobal(QCursor::pos());
    auto it = itemAt(p);
    //std::cout << p.x() << " " << p.y() << std::endl;
    d->hoverItem.setParentItem(it);
    d->hoverItem.setRect(it->boundingRect());
    d->hoverItem.show();    
    emit sigBarSelected();
}

auto TCChartView::SetBarSelected(int index) -> void {
    //auto axis = dynamic_cast<QBarCategoryAxis*>(chart()->axisX());
    auto series = dynamic_cast<QBarSeries*>(chart()->series()[0]);
    auto barset = series->barSets()[0];        

    auto inScene = chart()->plotArea();
    auto inChart = chart()->mapFromScene(inScene);

    auto plotArea = inChart.boundingRect();
    auto bot = plotArea.bottomLeft().y();
    auto left = plotArea.bottomLeft().x();
    //auto top = plotArea.topRight().y();
    auto right = plotArea.topRight().x();

    auto total_count = barset->count();

    auto unit_width = (right - left) / total_count;

    auto target_x = left + (index) * unit_width + unit_width /2;
    auto target_y = bot-5;        

    //std::cout << "t " << target_x << " " << target_y << std::endl;

    auto p = mapFromScene(QPointF(target_x, target_y));
    //std::cout << "From table: " << p.x() << " " << p.y() << std::endl;
    auto it = itemAt(p);
    if(it) {
        if (it->boundingRect().width() < unit_width) {
            d->hoverItem.setParentItem(it);
            d->hoverItem.setRect(it->boundingRect());
            d->hoverItem.show();
        }
    }
}

auto TCChartView::HideBarSelection() -> void {
    d->hoverItem.hide();
}