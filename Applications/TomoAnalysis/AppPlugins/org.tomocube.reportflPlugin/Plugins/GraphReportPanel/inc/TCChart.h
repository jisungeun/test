#pragma once

#include <QtCharts>

class TCChart : public QChart {
public:
	TCChart();
	~TCChart();

	void resizeEvent(QGraphicsSceneResizeEvent* event) override;		

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};