#pragma once

#include <QtCharts>

class TCBarSet : public QBarSet {
	Q_OBJECT
public:
	TCBarSet(QString label = QString(),QObject* parent = nullptr);	
	~TCBarSet();	

	auto SetBarInfo(QList<int> info)->void;
	auto GetBarInfo()->QList<int>;
	auto SetParentTitle(QString title)->void;
	auto GetParentTitle()->QString;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};