#pragma once

#include <QtCharts>

class TCScatterSeries : public QScatterSeries {
public:
	TCScatterSeries(QObject* parent = nullptr);
	~TCScatterSeries();
private:
	struct Impl;
	std::unique_ptr<Impl> d;
};