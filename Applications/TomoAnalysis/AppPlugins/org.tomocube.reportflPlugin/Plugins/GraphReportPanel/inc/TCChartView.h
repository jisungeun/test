#pragma once

#include <QtCharts>

class TCChartView : public QChartView {
	Q_OBJECT
public:
	TCChartView(QChart* chart,QWidget* parent=nullptr);
	~TCChartView();
	auto HideBarSelection()->void;
	auto SetBarSelected(int index)->void;

signals:
	void sigBarSelected();

protected slots:
	void OnBarClicked(int);	

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};