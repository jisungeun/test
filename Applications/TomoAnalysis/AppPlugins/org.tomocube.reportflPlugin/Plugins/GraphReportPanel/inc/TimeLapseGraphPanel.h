#pragma once

#include <QWidget>

#include <IReportGraphPanel.h>
#include "GraphReportCommon.h"

#include "GraphReportPanelExport.h"

class QTreeWidgetItem;
class QColor;

namespace TomoAnalysis::Report::Plugins {
    class GraphReportPanel_API TimeLapseGraphPanel : public QWidget, public Interactor::IReportGraphPanel {
        Q_OBJECT
    public:
        typedef TimeLapseGraphPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        TimeLapseGraphPanel(QWidget* parent = nullptr);
        virtual ~TimeLapseGraphPanel();

        auto Init()->void;

        auto UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> bool override;
        auto UpdateGraph(Entity::WorkingSet::Pointer workingset) -> bool override;

    signals:
        void drawGraph();
        void graphSelected(QString,QString,QString,int,QString);
        void graphDeselected();
        void sigEditMask(QString, QString, QString, int);//Playground,cubeName,fileName,timeStep

    protected:
        void mousePressEvent(QMouseEvent* event) override;

    protected slots:
        void OnTreeItemChanged(QTreeWidgetItem*, int);
        void OnDrawGraphClicked();
        void OnYRangeChanged();
        void OnXMinChanged();
        void OnXMaxChanged();
        void OnCubeColorChanged(const QString& title, QColor color);
        void OnExportGraphClicked();
        void OnGraphThemeSwitch(bool);
        void OnChartHorScrollBarValueChanged(int);

        void OnScatterClicked(const QPointF&);
        void OnEditBtn();
        void SelectionFromTable(QString cubeName, QString tcf_name, QString cellID, int time_step,QString organ_name);
        void OnTableDeselection();

    private:
        auto ClearChart() -> void;
        auto AppendColorSelector(const QString& text, const QString& color) -> void;
        auto RemoveAllColorSelector() -> void;
        void OnXRangeChanged();
        auto DataToPresentation(QString data)->QString;
        auto PresentationToData(QString presnetation)->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class ColorSelector : public QWidget {
        Q_OBJECT
    public:
        ColorSelector(const QString& title, QWidget* parent = nullptr);
        virtual ~ColorSelector();

        auto SetColor(const QString& color) -> void;

    signals:
        void colorChanged(const QString& title, QColor color);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}