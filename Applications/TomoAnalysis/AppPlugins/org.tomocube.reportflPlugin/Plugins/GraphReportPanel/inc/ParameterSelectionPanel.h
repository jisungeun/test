#pragma once
#include <QWidget>

#include <WorkingSet.h>

#include "GraphReportCommon.h"
#include "GraphReportPanelExport.h"

class QAbstractButton;

namespace TomoAnalysis::Report::Plugins {
    class GraphReportPanel_API ParameterSelectionPanel: public QWidget{
        Q_OBJECT

    public:
        typedef ParameterSelectionPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ParameterSelectionPanel(QWidget* parent = nullptr);
        virtual ~ParameterSelectionPanel();

        auto UpdateOrgan(Entity::WorkingSet::Pointer workingset) -> bool;
        auto UpdateOrgan2(Entity::WorkingSet::Pointer workingset)->bool;
        auto GetCurrentOrgan()->OrganelleId;
        auto GetCurrentOrganID()->int;
        auto SetCurrentKey(ParameterId id)->void;

    private:
        auto Init()->void;

    signals:
        void parameterChanged(ParameterId id);
        void organlleChanged(OrganelleId id);
        void organIdxChanged(int);

    protected slots:
        void OnParameterChanged(QAbstractButton*);
        void OnOrganelleChanged(QAbstractButton*);

    private:        
        auto DataToPresentation(QString data)->QString;
        auto PresentationToData(QString presnetation)->QString;
        auto IsBasic(QStringList names)->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}