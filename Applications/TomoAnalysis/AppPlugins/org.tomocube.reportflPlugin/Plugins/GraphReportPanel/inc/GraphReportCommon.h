#pragma once

#include "enum.h"

#include <QString>
#include <QMap>
#include <QColor>

#include "GraphReportPanelExport.h"

namespace TomoAnalysis::Report::Plugins {
    BETTER_ENUM(ParameterId, int,
        MEAN_RI,
        VOLUME,
        SURFACE_AREA,
        PROJECTED_AREA,
        DRYMASS,
        PROTEIN_CONCENTRATION,
        SPHERICITY,
        THRESHOLD_RI
    );

    BETTER_ENUM(OrganelleId, int,
        MEMBRANE,
        NUCLEUS,
        NUCLEOLI,
        LIPID_DROPLET
    );

    struct OptionSet {
        QString displayName;
        QString keyName;
    };

    class GraphParameters {
    public:
        inline static const QMap<ParameterId, OptionSet> parameterSet {
            { ParameterId::MEAN_RI,                 { "Mean RI",        "MeanRI"        } },
            { ParameterId::VOLUME,                  { "Volume",         "Volume"        } },
            { ParameterId::SURFACE_AREA,            { "Surface area",   "SurfaceArea"   } },
            { ParameterId::PROJECTED_AREA,          { "Projected area", "ProjectedArea" } },
            { ParameterId::DRYMASS,                 { "Dry mass",       "Drymass"       } },
            { ParameterId::PROTEIN_CONCENTRATION,   { "Concentration",  "Concentration" } },
            { ParameterId::SPHERICITY,              { "Sphericity",     "Sphericity"    } }
        };

        inline static const QMap<OrganelleId, OptionSet> organelleSet {
            { OrganelleId::MEMBRANE,      { "Whole Cell",       "membrane"  } },
            { OrganelleId::NUCLEUS,       { "Nucleus",        "nucleus"   } },
            { OrganelleId::NUCLEOLI,      { "Nucleolus",      "nucleoli"  } },
            { OrganelleId::LIPID_DROPLET, { "Lipid droplet",  "lipid droplet"     } }
        };
    };


    BETTER_ENUM(GraphTheme, int, Dark, Light);

    typedef struct themeSet {
        QColor background;
        QColor text;
    } GraphThemeColorSet;

    class GraphColor {
    public:
        inline static const QStringList colorPalette = {
            "#f44336",  // red
            "#03a9f4",  // light blue
            "#673ab7",  // deep purple
            "#4caf50",  // green
            "#ffeb3b",  // yellow
            "#ff80ab",  // pink
            "#3f51b5",  // indigo
            "#00bcd4",  // cyan
            "#8bc34a",  // light green
            "#ffc107",  // amber
            "#9c27b0",  // purple
            "#2196f3",  // blue
            "#009688",  // teal
            "#cddc39",  // lime
            "#ff9800",  // orange
        };

        inline static const QMap<GraphTheme, GraphThemeColorSet> graphThemeSet{
            { GraphTheme::Dark,     { "#1b2629", "#ffffff" } },
            { GraphTheme::Light,    { "#ffffff", "#1b2629" } }
        };

    };
}