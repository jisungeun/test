#pragma once

#include <QWidget>
#include <QtCharts>

#include <IReportGraphPanel.h>

#include "GraphReportCommon.h"

#include "GraphReportPanelExport.h"

namespace TomoAnalysis::Report::Plugins {
    typedef struct resultData {
        double min{ 0.0 };
        double max{ 0.0 };

        double average{ 0.0 };
        double stdev{ 0.0 };
        int cells{ 0 };

        double q1;  // lower quartile
        double q2;  // median
        double q3;  // upper quartile
    } CalcResult;

    typedef struct cellMeasuredData{
        QString reportName;
        int reportIndex;
        QList<double> measuredValues;    // key: cell index, value: measured value
    } CellData;

    typedef QMap<QString, QList<CellData>>    GraphDataSet;

    class GraphReportPanel_API GraphReportPanel : public QWidget, public Interactor::IReportGraphPanel {
        Q_OBJECT
    public:
        typedef GraphReportPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        GraphReportPanel(QWidget* parent = nullptr);
        virtual ~GraphReportPanel();

        auto Init()->void;

        auto UpdateDataList(Entity::CubeDataList::Pointer cubeDataList) -> bool override;
        auto UpdateGraph(Entity::WorkingSet::Pointer workingset) -> bool override;

        void resizeEvent(QResizeEvent* event) override;                

    signals:
        void keyChanged();
        void sigCorrection(QString, QString, QString);//Playground, cubeName, fileName
        void barSelection(QString,QString,QString,QString);

    protected slots:
        void OnOverviewGraphExport();
        void OnOverviewTableExport();
        void OnDetailsPlotYMinChanged();
        void OnDetailsPlotYMaxChanged();
        void OnDetailsGraphExport();
        void OnGraphThemeSwitch(bool);
        void OnGraphListSliderValueChanged(int);

        void OnBarSetClicked(int,QBarSet*);
        void OnCorrectionClicked();
        void OnBarSelected();
        void SelectionFromTable(QString, QString,QString, int,QString);

    private:
        auto CalcCubeData(QList<double> data) -> resultData;

        auto UpdateOverview(const GraphDataSet& data,const QStringList& nameOrder) -> bool;
        auto UpdateDetails(const GraphDataSet& data, const QStringList& nameOrder) -> bool;

        auto DataToPresentation(QString data)->QString;
        auto PresentationToData(QString presnetation)->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}