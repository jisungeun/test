#include "TCMeasureReader.h"
#include "MeasureDataReader.h"

#include <iostream>

namespace TomoAnalysis::Report::Plugins {
    MeasureDataReader::MeasureDataReader() {
        
    }
    MeasureDataReader::~MeasureDataReader() {
        
    }

    auto MeasureDataReader::GetTimeStep(const QString& path) -> int {        
        TC::IO::TCMeasureReader reader(path);
        auto oName = reader.GetOrganNames();
        if(oName.size()<1) {
            return 0;
        }
        auto timeCount = reader.GetTimeIndexCount("Measures/"+oName[0]);
        return timeCount;
    }    
    auto MeasureDataReader::GetTimePoints(const QString& path) -> QList<int> {
        TC::IO::TCMeasureReader reader(path);
        auto oName = reader.GetOrganNames();
        if(oName.size()<1) {
            return QList<int>();
        }
        QList<int> result = QList<int>();
        for(auto o : oName) {
            auto candidate = reader.GetTimePoints("Measures/"+o);
            if(candidate.size()>result.size()) {
                result = candidate;
            }
        }
        return result;        
    }
    auto MeasureDataReader::GetTimePoint(const QString& path, int time_step)->double {
        double point = -1;
        TC::IO::TCMeasureReader reader(path);
        auto organ_names = reader.GetOrganNames();//one time only
        auto organID = QString("Measures/") + organ_names[0];        
        return reader.GetTimePoint(organID, time_step);
    }
    auto MeasureDataReader::Read(const QString& path, int time_step) -> TCMeasure::Pointer {
        auto measure = std::make_shared<TCMeasure>();
        TC::IO::TCMeasureReader reader(path);
        auto organ_names = reader.GetOrganNames();//one time only
        auto measure_names = reader.GetMeasureNames();
        auto time_interval = reader.GetTimeInterval();

        measure->SetTimeInterval(time_interval);

        for (auto o = 0; o < organ_names.size(); o++) {
            auto organ = organ_names[o];
            auto organID = QString("Measures/") + organ;
            auto cell_count = reader.GetCellCount(organID, time_step);
            for (auto i = 0; i < cell_count; i++) {
                auto vals = reader.ReadMeasures(organID, time_step, i);
                for (auto m = 0; m < measure_names.size(); m++) {
                    auto key = measure_names[m];
                    auto final_key = organ + "." + key;
                    auto value = vals[m];                    
                    measure->AppendMeasure(final_key, i, value);
                }
            }
        }
        return measure;
    }
}
