#pragma once

#include <IMeasureReaderPort.h>

#include <ReportIOExport.h>

namespace TomoAnalysis::Report::Plugins {
    class ReportIO_API MeasureDataReader : public UseCase::IMeasureReaderPort {
    public:
        MeasureDataReader();
        ~MeasureDataReader();

        auto GetTimeStep(const QString& path) -> int override;        
        auto GetTimePoints(const QString& path) -> QList<int> override;
        auto GetTimePoint(const QString& path, int time_step)->double override;
        auto Read(const QString& path, int time_step) -> TCMeasure::Pointer override;
    };
}