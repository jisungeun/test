#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.reportflPlugin";

namespace TomoAnalysis::Report::AppUI {
    class reportflPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.reportflPlugin")            

    public:
        static reportflPlugin* getInstance();

        reportflPlugin();
        ~reportflPlugin();

        auto start(ctkPluginContext* context)->void override;
        //auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static reportflPlugin* instance;
    };
}