#pragma once
#include <memory>
#include <QWidget>

namespace TomoAnalysis::Viewer2D {
    class TCFListPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = TCFListPanel;

        TCFListPanel(QWidget* parent = nullptr);
        ~TCFListPanel() override;

    protected slots:
        void onUpdateTcfList(const QStringList& tcfList);
        void onTcfSelectionChanged(int, int);
    signals:
        void sigSelectTCF(int);
    private:
        auto Init()->void;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}