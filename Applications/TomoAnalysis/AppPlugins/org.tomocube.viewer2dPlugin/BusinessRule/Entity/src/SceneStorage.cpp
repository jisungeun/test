#include <map>
#include <mutex>

#include "SceneStorage.h"

#pragma warning( push )
#pragma warning(disable:4700)

namespace TomoAnalysis::Viewer2D::Entity {
	struct SceneStorage::Impl {
		uint32_t last{ 0 };
		std::mutex lock;
		std::map<Scene::ID, Scene::Pointer> scenes;
	};

	SceneStorage::SceneStorage()
		: d(new Impl()) {
	}

	SceneStorage::~SceneStorage() {
	}

	auto SceneStorage::GetInstance() -> Pointer {
		static Pointer theInstance(new SceneStorage());
		return theInstance;
	}

	auto SceneStorage::CreateScene() -> Scene::ID {
		std::lock_guard<std::mutex> guard(d->lock);
		auto id = d->last++;
		d->scenes[id] = std::make_shared<Scene>();
		return id;
	}

	auto SceneStorage::ReleseScene(const Scene::ID& sceneID)->void {
		try {
			d->scenes.erase(sceneID);
		} catch (...) {
		}
	}

	auto SceneStorage::GetScene(const Scene::ID& sceneID)->Scene::Pointer {
		Scene::Pointer scene;
		try {
			scene = d->scenes[sceneID];
		} catch (...) {
		}
		return scene;
	}
}

#pragma warning( pop )