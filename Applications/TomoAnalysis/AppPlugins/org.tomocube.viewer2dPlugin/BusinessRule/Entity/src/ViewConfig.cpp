#include <iterator>
#include <iostream>

#include "ViewConfig.h"

namespace TomoAnalysis::Viewer2D::Entity {
    struct ViewConfig::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;

		Index index = -1;
		Type type = Type::NONE;
		ImageType imageType = ImageType::Unknown;

		int sliceIndex = -1;

		Color backgroundColor;
    };

	ViewConfig::ViewConfig()
        : d(new Impl()) {
	}

	ViewConfig::ViewConfig(const ViewConfig& other)
        : d(new Impl(*other.d)) {
	}

	ViewConfig::~ViewConfig() = default;

	auto ViewConfig::GetIndex(void) const ->Index {
		return d->index;
	}

	auto ViewConfig::SetIndex(const Index& index) const ->void {
		d->index = index;
	}

	auto ViewConfig::GetViewType(void) const ->ViewType {
		return d->type;
	}

	auto ViewConfig::SetViewType(const ViewType& type) const ->void {
		d->type = type;
	}

	auto ViewConfig::GetImageType(void) const->ImageType {
		return d->imageType;
	}

	auto ViewConfig::SetImageType(const ImageType& type) const ->void {
		d->imageType = type;
	}

	auto ViewConfig::GetSliceIndex(void) const->int {
		return d->sliceIndex;
	}

	auto ViewConfig::SetSliceIndex(const int& index) const ->void {		
		d->sliceIndex = index;
	}
         
	auto ViewConfig::GetBackgroundColor(void) const ->Color {
		return d->backgroundColor;
	}

	auto ViewConfig::SetBackgroundColor(const Color& color) const ->void {
		d->backgroundColor = color;
	}
}
