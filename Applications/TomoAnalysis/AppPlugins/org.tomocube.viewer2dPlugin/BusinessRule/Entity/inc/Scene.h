#pragma once

#include <memory>
#include <map>

#include "Image.h"
#include "ViewConfig.h"
#include "TransferFunction.h"
//#include "TomoAnalysisEntityExport.h"

namespace TomoAnalysis::Viewer2D::Entity {
	BETTER_ENUM(LayoutType, uint8_t, UNKNOWN = 0, TwoByTwo, HSlicesBy3D, VSlicesBy3D, Only3D, XYPlane, YZPlane, XZPlane, HXY3D, VXY3D);
	BETTER_ENUM(Channel, uint8_t, CH1, CH2, CH3, All);

	enum Modality {
		None = 0x00,
		HTVolume = (0x01 << 1),
		FLVolume = (0x01 << 2),
		HTMIP = (0x01 << 3),
		FLMIP = (0x01 << 4),
		BF2D = (0x01 << 5),
	};

	enum DisplayType {
		DISPLAY_NONE	= 0x00,
	    DISPLAY_3D		= 0x01,
	    DISPLAY_2D		= 0x02,
	    DISPLAY_MIP		= 0x04
	};

	struct ChannelInfo {
		int min = 0;
		int max = 255;
		int opacity = 70;
		int gamma = 100;
		bool isGamma = false;
		int color[3] = { 0,0,0 };
		bool isValid = true;
		bool visible = true;
		std::string name;

		typedef std::shared_ptr<ChannelInfo> Pointer;
	};

	typedef int ViewIndex;

	typedef std::map<Channel, ChannelInfo::Pointer> FLChannelInfo;	
	typedef std::vector<ViewConfig::Pointer> ViewConfigList;
   
	class Viewer2dEntity_API Scene final {
	public:
		typedef int ID;
		typedef Scene Self;
		typedef std::shared_ptr<Self> Pointer;
		typedef int Index;

	public:
		Scene();
		explicit Scene(const Scene& other);
		~Scene();

		auto GetImage(void) const ->Image::Pointer;
		auto SetImage(Image::Pointer& image) const -> void;

		auto GetResolution(void)const->int;
		auto SetResolution(int res)const->void;

		auto GetLayoutType() const ->LayoutType;
		auto SetLayoutType(LayoutType type) const ->void;
		auto SetDefaultLayout(const Entity::Modality& modality) const->bool;

		auto GetViewConfigList(void) const ->ViewConfigList;
		auto GetViewConfig(const int& index) const ->ViewConfig::Pointer;
		auto AddViewCofig(const ViewConfig::Pointer& config) const ->void;
		auto SetViewConfig(const int& index, const ViewConfig::Pointer& config) const ->bool;
		auto SetViewConfigList(const ViewConfigList& viewConfigList) const ->void;
		auto GenerateViewConfigList()const->bool;

		auto GetFLChannelInfoList(void)->FLChannelInfo;
		auto GetFLChannelInfo(const Channel& channel) const ->ChannelInfo::Pointer;
		auto SetFLChannelInfo(const Channel& channel, const ChannelInfo::Pointer& info) const ->void;
		auto SetFLChannelInfoList(const FLChannelInfo& list) const ->void;

		auto GetTransferFunctionList() const ->TFItemList;
		auto SetTransferFunctionList(const TFItemList& list) const ->void;
		auto AddTransferFunctionItem(const TFItem::Pointer& item) const ->void;
		auto DeleteTransferFunctionItem(const int& index) const ->void;
        auto SetTransferFunctionItem(const int& index, const TFItem::Pointer& item) const ->void;
        auto GetTransferFunctionItem(const int& index) const ->TFItem::Pointer;
        		
		auto GetVisibleBoundaryBox(void) const ->bool;
		auto SetVisibleBoundaryBox(bool visible) const ->void;
		
		auto GetVisibleAxisGrid(void) const ->bool;
		auto SetVisibleAxisGrid(bool visible) const ->void;

		auto GetAxisGridFontSize(void)const->int;
		auto SetAxisGridFontSize(int size)const->void;

		auto GetAxisGridColor(int axis)const->std::tuple<int, int, int>;
		auto SetAxisGridColor(int axis, int r, int g, int b)const->void;

		auto GetVisibleOrientationMarker(void) const ->bool;
		auto SetVisibleOrientationMarker(bool visible)const->void;

		auto GetVisibleTimeStamp(void)const->bool;
		auto SetVisibleTimeStamp(bool visible)const->void;

		auto GetTimeStampColor()->std::tuple<int, int, int>;
		auto SetTimeStampColor(int r, int g, int b)->void;

		auto GetTimeStampSize()->int;
		auto SetTimeStampSize(int size)->void;

		auto GetEnableOverlay(void) const ->bool;
		auto SetEnableOverlay(bool enable) const ->void;				

		auto GetActivateModality() const->Modality;
        auto SetActivateModality(const Modality& modality) const -> void;

		auto GetTimelapseIndex() const -> int;
		auto SetTimelapseIndex(const int index)->void;

		auto GetTimelapseTime()const->double;
		auto SetTimelapseTime(const double time)->void;

		auto Clear(void)->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}