#pragma once

#include <memory>
#include <string>

#include <enum.h>
#include <vector>

//#include "TomoAnalysisEntityExport.h"
#include "Viewer2dEntityExport.h"

namespace TomoAnalysis::Viewer2D::Entity {
	enum ImageType {
		Unknown = 0,
		HT3D = 1,
		FL3D,
		HT2D,
		FL2D,
		PHASE,
		HT2DMIP,
		FL2DMIP,
		BF,
		Count,
	};
	
	class Viewer2dEntity_API Image final {
	public:
		typedef Image Self;
		typedef std::shared_ptr<Self> Pointer;

		typedef ImageType Type;

	    struct Dimension {
			uint32_t X, Y, Z;
		};

		struct Resolution {
			float X, Y, Z;
		};

        struct Size {
			float X, Y, Z;
        };

		struct Intensity {
			float max = -1;
			float min = -1;
		};

	    struct Channel { 
			bool isValid = false;
	        float max = -1;
			float min = -1;
			std::string name;
			int r = 0;
			int g = 0;
			int b = 0;
		};

        struct Meta {
			Dimension dimension;
			Resolution resolution;
			uint32_t count;
			uint32_t interval;  //msec			

			Intensity intensity;
			Channel channel[3]; // RGB
			std::vector<double> timepoints[3];
        };
		
	public:
		Image();
		Image(const Image& other);
		~Image();
		
		auto GetPath() const->std::string;
		auto HasType(ImageType type) const->bool;
		auto GetDimension(ImageType type) const ->Dimension;
		auto GetResolution(ImageType type) const ->Resolution;
		auto GetCount(ImageType type) const ->uint32_t;
		auto GetTimeInterval(ImageType type) const ->uint32_t;
		auto GetChannel(ImageType type) const->Channel*;
		auto GetIntensity(ImageType type) const->Intensity;
		auto GetTimePoints(ImageType type,int ch)const->std::vector<double>;
		auto GetOffsetZ()const->double;
		auto GetFirstTime()const->double;

		auto SetFirstTime(const double& ft)->void;
		auto SetOffsetZ(const double& offset)->void;
		auto SetPath(const std::string& path)->void;
		auto SetMeta(Type type, Meta meta)->void;
		
	private:
		struct Impl;
		std::shared_ptr<Impl> d;
	};
}