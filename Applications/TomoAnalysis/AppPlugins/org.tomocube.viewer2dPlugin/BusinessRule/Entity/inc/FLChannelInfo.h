#pragma once

#include <memory>
#include <map>
#include <vector>

#include <Image.h>

#include "TomoAnalysisEntityExport.h"

namespace TomoAnalysis::Entity {
	BETTER_ENUM(FLChannel, uint8_t, RED, GREEN, BLUE);

	struct ChannelInfo {
		uint8_t max;
		uint8_t min;
		uint8_t opacity;
		int gamma;
		bool visible;
	};

	class TomoAnalysisEntity_API FLChannelInfo final {
	public:
		typedef TransferFunction Self;
		typedef std::shared_ptr<Self> Pointer;

	public:
		TransferFunction();
		explicit TransferFunction(const TransferFunction& other);
		~TransferFunction();

		auto AddItem(const TFItem::Pointer& item) const ->bool;
        auto ModifyItem(const int& index, const TFItem::Pointer& item) const ->bool;
		auto DeleteItem(const int& index) const ->bool;
        auto SetVisibleItem(const int& index, bool visible) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}