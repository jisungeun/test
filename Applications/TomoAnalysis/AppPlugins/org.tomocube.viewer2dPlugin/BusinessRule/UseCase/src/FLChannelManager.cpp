#include <iostream>

#include <SceneStorage.h>

#include "FLChannelManager.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	FLChannelManager::FLChannelManager() = default;
	FLChannelManager::~FLChannelManager() = default;

	auto FLChannelManager::SetFLChannelInfo(const Entity::Channel& channel, const Entity::ChannelInfo::Pointer& info,
		                                    Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->SetFLChannelInfo(channel, info);

		// don't update

		return true;
	}

	auto FLChannelManager::SetRange(const Entity::Channel& channel, const int& min, const int& max,
		                            Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto setRange = [&](Entity::Channel ch)->Entity::ChannelInfo::Pointer {
			auto info = scene->GetFLChannelInfo(ch);
			if (info) {
				info->min = min;
				info->max = max;
			}

			return info;
		};

		Entity::FLChannelInfo list;

		switch (channel) {
		case Entity::Channel::All: {
			list[Entity::Channel::CH1] = setRange(Entity::Channel::CH1);
			list[Entity::Channel::CH2] = setRange(Entity::Channel::CH2);
			list[Entity::Channel::CH3] = setRange(Entity::Channel::CH3);
			break;
		}
		case Entity::Channel::CH1: 
		case Entity::Channel::CH2: 
		case Entity::Channel::CH3:
			list[channel] = setRange(channel);
			break;
		}

		port->Update(list);

		return true;
	}

	auto FLChannelManager::SetGamma(const Entity::Channel& channel, const int& value,const bool& isGamma,
		                            Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto setGamma = [&](Entity::Channel ch)->Entity::ChannelInfo::Pointer {
			auto info = scene->GetFLChannelInfo(ch);
			if (info) {
				info->gamma = value;
				info->isGamma = isGamma;
			}

			return info;
		};

		Entity::FLChannelInfo list;

		switch (channel) {
		case Entity::Channel::All: {
			list[Entity::Channel::CH1] = setGamma(Entity::Channel::CH1);
			list[Entity::Channel::CH2] = setGamma(Entity::Channel::CH2);
			list[Entity::Channel::CH3] = setGamma(Entity::Channel::CH3);
			break;
		}
		case Entity::Channel::CH1:
		case Entity::Channel::CH2:
		case Entity::Channel::CH3:
			list[channel] = setGamma(channel);
			break;
		}

		port->Update(list);

		return true;
	}

	auto FLChannelManager::SetColor(const Entity::Channel& channel, const int& r, const int& g, const int& b, Entity::Scene::ID sceneID, IFLChannelOutputPort* port) -> bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto setColor = [&](Entity::Channel ch)->Entity::ChannelInfo::Pointer {
			auto info = scene->GetFLChannelInfo(ch);
			if (info) {
				info->color[0] = r;
				info->color[1] = g;
				info->color[2] = b;
			}

			return info;
		};

		Entity::FLChannelInfo list;

		switch (channel) {
		case Entity::Channel::All: {
			list[Entity::Channel::CH1] = setColor(Entity::Channel::CH1);
			list[Entity::Channel::CH2] = setColor(Entity::Channel::CH2);
			list[Entity::Channel::CH3] = setColor(Entity::Channel::CH3);
			break;
		}
		case Entity::Channel::CH1:
		case Entity::Channel::CH2:
		case Entity::Channel::CH3:
			list[channel] = setColor(channel);
			break;
		}

		port->Update(list);

		return true;
    }


	auto FLChannelManager::SetOpacity(const Entity::Channel& channel, const int& value,
		                              Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto setOpacity = [&](Entity::Channel ch)->Entity::ChannelInfo::Pointer {
			auto info = scene->GetFLChannelInfo(ch);
			if (info) {
				info->opacity = value;
			}

			return info;
		};

		Entity::FLChannelInfo list;

		switch (channel) {
		case Entity::Channel::All: {
			list[Entity::Channel::CH1] = setOpacity(Entity::Channel::CH1);
			list[Entity::Channel::CH2] = setOpacity(Entity::Channel::CH2);
			list[Entity::Channel::CH3] = setOpacity(Entity::Channel::CH3);
			break;
		}
		case Entity::Channel::CH1:
		case Entity::Channel::CH2:
		case Entity::Channel::CH3:
			list[channel] = setOpacity(channel);
			break;
		}

		port->Update(list);

		return true;
	}

	auto FLChannelManager::SetVisible(const Entity::Channel& channel, const bool& visible, 
		                              Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto setVisible = [&](Entity::Channel ch)->Entity::ChannelInfo::Pointer {
			auto info = scene->GetFLChannelInfo(ch);
			if (info) {
				info->visible = visible;
			}

			return info;
		};
				
		Entity::FLChannelInfo list;
		
		switch (channel) {
		case Entity::Channel::All: {
			list[Entity::Channel::CH1] = setVisible(Entity::Channel::CH1);
			list[Entity::Channel::CH2] = setVisible(Entity::Channel::CH2);
			list[Entity::Channel::CH3] = setVisible(Entity::Channel::CH3);		
			break;
		}
		case Entity::Channel::CH1:
		case Entity::Channel::CH2:
		case Entity::Channel::CH3:
			list[channel] = setVisible(channel);
			break;
		}

		port->Update(list);

		return true;
	}
}
