#include <iostream>

#include <SceneStorage.h>
#include "ImageAccessor.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	ImageAccessor::ImageAccessor() = default;
	ImageAccessor::~ImageAccessor() = default;

	auto ImageAccessor::SetModality(const Entity::Modality& modality, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) const ->bool {
	    if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

        // set activate modality
		scene->SetActivateModality(modality);

        // set default layout by modality
		if(false == scene->SetDefaultLayout(modality)) {
		    return false;
		}

	    //auto layout = scene->GetLayoutType();
		if (false == scene->GenerateViewConfigList()) {
			return false;
		}

		port->Update(scene);

		return true;
    }

	auto ImageAccessor::SetTimelapseFrame(const double& frameTime, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) const -> bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}
				
		//scene->SetTimelapseIndex(frameIndex);
		scene->SetTimelapseTime(frameTime);

        port->Update(scene);

		return true;
    }
}
