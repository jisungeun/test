#include "IFileReaderPort.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	IFileReaderPort::IFileReaderPort() = default;
	IFileReaderPort::~IFileReaderPort() = default;
}