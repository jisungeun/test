#include "IVisualizationDataReadPort.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	IVisualizationDataReadPort::IVisualizationDataReadPort() = default;
	IVisualizationDataReadPort::~IVisualizationDataReadPort() = default;
}