#pragma once

#include <SceneStorage.h>
#include <ImageLoader.h>

#include "LayoutManager.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	LayoutManager::LayoutManager() = default;
	LayoutManager::~LayoutManager() = default;

	auto LayoutManager::SetLayoutType(const Entity::LayoutType& type, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool{
		if (nullptr == port) {
			return false;
		}

	    auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		scene->SetLayoutType(type);

		port->Update(scene);

		return true;
	}
}
