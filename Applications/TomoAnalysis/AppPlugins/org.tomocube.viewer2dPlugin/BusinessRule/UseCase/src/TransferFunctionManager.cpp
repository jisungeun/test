#include <memory>

#include <SceneStorage.h>

#include "TransferFunctionManager.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	TransferFunctionManager::TransferFunctionManager() = default;
	TransferFunctionManager::~TransferFunctionManager() = default;

	auto TransferFunctionManager::SetList(const Entity::TFItemList& list, const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->SetTransferFunctionList(list);

		// don't update

		return true;
	}

	auto TransferFunctionManager::Create(const Entity::TFItem::Pointer& item, const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->AddTransferFunctionItem(item);

		// don't update

		return true;
	}

	auto TransferFunctionManager::Modify(const int& itemIndex, const Entity::TFItem::Pointer& item,
		                                 const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->SetTransferFunctionItem(itemIndex, item);
		
		port->Update(itemIndex, item);

		return true;
	}

	auto TransferFunctionManager::Delete(const int& itemIndex, Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->DeleteTransferFunctionItem(itemIndex);
		       
		// don't update

		return true;
	}

	auto TransferFunctionManager::Clear(Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		scene->GetTransferFunctionList().clear();

		port->Clear();

	    return true;
	}

	auto TransferFunctionManager::SetVisible(const int& itemIndex, bool visible,
		                                     Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneID);
		if (!scene) {
			return false;
		}

		auto item = scene->GetTransferFunctionItem(itemIndex);
		item->visible = visible;
	  
		port->Update(itemIndex, item);

		return true;
	}
}