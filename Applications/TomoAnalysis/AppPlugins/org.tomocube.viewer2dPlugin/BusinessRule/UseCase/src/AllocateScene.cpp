#include <functional>

#include <SceneStorage.h>

#include "AllocateScene.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	AllocateScene::AllocateScene() {
	}

	AllocateScene::~AllocateScene() {
	}

	auto AllocateScene::Request() -> Entity::Scene::ID
	{
		auto storage = Entity::SceneStorage::GetInstance();
		return storage->CreateScene();
	}
}
