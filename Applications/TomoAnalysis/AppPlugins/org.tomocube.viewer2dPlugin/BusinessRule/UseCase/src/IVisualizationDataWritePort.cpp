#include "IVisualizationDataWritePort.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	IVisualizationDataWritePort::IVisualizationDataWritePort() = default;
	IVisualizationDataWritePort::~IVisualizationDataWritePort() = default;
}