#include <iostream>

#include <SceneStorage.h>

#include "ImageLoader.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	ImageLoader::ImageLoader() = default;
	ImageLoader::~ImageLoader() = default;

	auto ImageLoader::Load(const std::string& path, Entity::Scene::ID sceneId,
		                   ITCFInfoOutputPort* port, IFileReaderPort* reader) const ->bool {		
		if (nullptr == port){			
			return false;
		}

		if (nullptr == reader) {			
			return false;
		}

		if (true == path.empty()) {			
			return false;
		}

		// get scene
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene) {			
			return false;
		}

		scene->Clear();

		// read image
		auto image = reader->Read(path);
		if (!image) {			
			return false;
		}

		// set image to scene entity
	    scene->SetImage(image);
		
		// set fl info
		if (image->HasType(Entity::ImageType::FL3D))
		{
			for (uint8_t i = 0; i < 3; ++i) {
				auto info = std::make_shared<Entity::ChannelInfo>();
                const auto channel = image->GetChannel(Entity::ImageType::FL3D);

				info->isValid = channel[i].isValid;
				info->min = static_cast<int>(channel[i].min / 4);
				info->color[0] = channel[i].r;
				info->color[1] = channel[i].g;
				info->color[2] = channel[i].b;
				info->name = channel[i].name;
				
				int max = static_cast<int>(channel[i].max / 4);				
				info->max = max;								
				scene->SetFLChannelInfo(Entity::Channel::_from_integral(i), info);
			}
		}else if(image->HasType(Entity::ImageType::FL2D)){
			for (uint8_t i = 0; i < 3; ++i) {
				auto info = std::make_shared<Entity::ChannelInfo>();
				const auto channel = image->GetChannel(Entity::ImageType::FL2D);

				info->isValid = channel[i].isValid;
				info->min = static_cast<int>(channel[i].min / 4);
				info->color[0] = channel[i].r;
				info->color[1] = channel[i].g;
				info->color[2] = channel[i].b;
				info->name = channel[i].name;

				int max = static_cast<int>(channel[i].max / 4);
				info->max = max;
				scene->SetFLChannelInfo(Entity::Channel::_from_integral(i), info);
			}
		}
		scene->SetTimelapseTime(image->GetFirstTime());
		port->Update(image);

		return true;
	}
}
