#include <iostream>

#include <QFileInfo>
#include <QDir>

#include <SceneStorage.h>
#include "VisualizationPresetIO.h"

#include <QDir>
#include <QDirIterator>

namespace TomoAnalysis::Viewer2D::UseCase {
	VisualizationPresetIO::VisualizationPresetIO() = default;
	VisualizationPresetIO::~VisualizationPresetIO() = default;

	auto VisualizationPresetIO::LoadList(const std::string& presetFolderPath, IVisualizationPresetOutputPort* port)->bool {
		if(nullptr == port) {
			return false;
		}

		//std::filesystem::path p(path);
  //      const auto folderPath = p.parent_path().string();
  //      const auto fileName = p.filename().replace_extension("").string();

		//std::vector<std::string> list;

	 //   // get default
		//std::string defaultXmlPath;
	 //   const auto temp = folderPath + "\\" + fileName + ".xml";
		//if(true == std::filesystem::exists(temp)) {
		//	defaultXmlPath = temp;
		//}

	    // get save as list
		auto qPath = QString::fromLocal8Bit(presetFolderPath.c_str());
		if(false == QDir(qPath).exists()) {
			QDir dirmanager;
		    dirmanager.mkdir(qPath);			
		}
		/*if (false == std::filesystem::exists(presetFolderPath)) {
			std::filesystem::create_directories(presetFolderPath);
		}*/

		std::vector<std::string> list;
		QDirIterator it(qPath, QStringList() << "*.xml", QDir::Files);
		while (it.hasNext()) {
			auto file_path = it.next();
			list.push_back(file_path.toLocal8Bit().toStdString());
		}
		/*for (const auto& entry : std::filesystem::directory_iterator(presetFolderPath)) {
			if (".xml" == entry.path().extension()) {
				list.push_back(entry.path().string());
			}
		}*/

		port->LoadList(list);

	    return true;
	}
	auto VisualizationPresetIO::Delete(const std::string& path) -> bool {
		if(true== path.empty()) {
			return false;
		}

		auto qPath = QString::fromLocal8Bit(path.c_str());
		if (false == QFile::remove(qPath)) {
			return false;//remove existing xml
		}
		/*if (false == std::filesystem::remove(path)) {
			return false;//remove existing xml
		}*/

		//delete bakup files if exist
		auto baseName = QFileInfo(qPath).baseName().chopped(4);
		QDirIterator it(qPath, QStringList() << "*.xml", QDir::Files);
		while (it.hasNext()) {
			auto file_path = it.next();
			if (file_path.contains(baseName)) {
				QFile::remove(file_path);
			}
		}		

		return true;
    }
	auto VisualizationPresetIO::Load(const std::string& path, Entity::Scene::ID sceneId, IVisualizationPresetOutputPort* port, IVisualizationDataReadPort* reader)->bool {
		if (true == path.empty()) {
			return false;
		}

		if (nullptr == port) {
			return false;
		}

		if (nullptr == reader) {
			return false;
		}

		Entity::TFItemList tfItemList;
		Entity::FLChannelInfo channel;
		if(false == reader->Read(path, tfItemList, channel)) {
			return false;
		}

		// update visualization data to current scene
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene) {
			return false;
		}
		auto ori_channel = scene->GetFLChannelInfoList();
				
		if (ori_channel[Entity::Channel::CH1]) {
			if(!channel[Entity::Channel::CH1]) {
			    channel[Entity::Channel::CH1] = std::shared_ptr<Entity::ChannelInfo>(new Entity::ChannelInfo);
				channel[Entity::Channel::CH1]->visible = false;
				channel[Entity::Channel::CH1]->gamma = ori_channel[Entity::Channel::CH1]->gamma;
				channel[Entity::Channel::CH1]->max = ori_channel[Entity::Channel::CH1]->max;
				channel[Entity::Channel::CH1]->min = ori_channel[Entity::Channel::CH1]->opacity;
				channel[Entity::Channel::CH1]->opacity = ori_channel[Entity::Channel::CH1]->max;
				channel[Entity::Channel::CH1]->isValid = ori_channel[Entity::Channel::CH1]->isValid;
			}else if(false == ori_channel[Entity::Channel::CH1]->isValid){
				channel[Entity::Channel::CH1] = ori_channel[Entity::Channel::CH1];
			}else if(false == channel[Entity::Channel::CH1]->isValid) {				
				channel[Entity::Channel::CH1] = ori_channel[Entity::Channel::CH1];
				channel[Entity::Channel::CH1]->visible = false;
			}
		}
		if (ori_channel[Entity::Channel::CH2]) {
			if (!channel[Entity::Channel::CH2]) {
				channel[Entity::Channel::CH2] = std::shared_ptr<Entity::ChannelInfo>(new Entity::ChannelInfo);
				channel[Entity::Channel::CH2]->visible = false;
				channel[Entity::Channel::CH2]->gamma = ori_channel[Entity::Channel::CH2]->gamma;
				channel[Entity::Channel::CH2]->max = ori_channel[Entity::Channel::CH2]->max;
				channel[Entity::Channel::CH2]->min = ori_channel[Entity::Channel::CH2]->min;
				channel[Entity::Channel::CH2]->opacity = ori_channel[Entity::Channel::CH2]->opacity;
				channel[Entity::Channel::CH2]->isValid = ori_channel[Entity::Channel::CH2]->isValid;
			}
			else if (false == ori_channel[Entity::Channel::CH2]->isValid) {
				channel[Entity::Channel::CH2] = ori_channel[Entity::Channel::CH2];
			}else if(false == channel[Entity::Channel::CH2]->isValid){				
				channel[Entity::Channel::CH2] = ori_channel[Entity::Channel::CH2];
				channel[Entity::Channel::CH2]->visible = false;
			}
		}
		if (ori_channel[Entity::Channel::CH3]) {
			if (!channel[Entity::Channel::CH3]) {
				channel[Entity::Channel::CH3] = std::shared_ptr<Entity::ChannelInfo>(new Entity::ChannelInfo);
				channel[Entity::Channel::CH3]->visible = false;
				channel[Entity::Channel::CH3]->gamma = ori_channel[Entity::Channel::CH3]->gamma;
				channel[Entity::Channel::CH3]->max = ori_channel[Entity::Channel::CH3]->max;
				channel[Entity::Channel::CH3]->min = ori_channel[Entity::Channel::CH3]->min;
				channel[Entity::Channel::CH3]->opacity = ori_channel[Entity::Channel::CH3]->opacity;
				channel[Entity::Channel::CH3]->isValid = ori_channel[Entity::Channel::CH3]->isValid;
			}else if(false== ori_channel[Entity::Channel::CH3]->isValid) {
				channel[Entity::Channel::CH3] = ori_channel[Entity::Channel::CH3];
			}else if(false == channel[Entity::Channel::CH3]->isValid) {				
				channel[Entity::Channel::CH3] = ori_channel[Entity::Channel::CH3];
				channel[Entity::Channel::CH3]->visible = false;
			}
		}
		
		scene->SetTransferFunctionList(tfItemList);		
		scene->SetFLChannelInfoList(channel);

		port->LoadPreset(path, tfItemList, channel);

		return true;
	}

	auto VisualizationPresetIO::Save(const std::string& path, const bool& saveAs, Entity::Scene::ID sceneId, IVisualizationPresetOutputPort* port, IVisualizationDataWritePort* writer)->bool {
		if (true == path.empty()) {
			return false;
		}

		if (nullptr == port) {
			return false;
		}

		if (nullptr == writer) {
			return false;
		}

		// get visualization data from current scene
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene) {
			return false;
		}

		auto tfItemList = scene->GetTransferFunctionList();
		auto channel = scene->GetFLChannelInfoList();

	    if(false == writer->Write(path, tfItemList, channel)) {
			return false;
	    }

		port->SavePreset(path, saveAs);

		return true;
	}
}
