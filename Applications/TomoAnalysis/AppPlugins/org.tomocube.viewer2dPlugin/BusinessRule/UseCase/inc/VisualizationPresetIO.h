#pragma once

#include <string>

#include <Scene.h>

#include <IVisualizationPresetOutputPort.h>
#include <IVisualizationDataReadPort.h>
#include <IVisualizationDataWritePort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API VisualizationPresetIO{
	public:
		VisualizationPresetIO();
		~VisualizationPresetIO();

		auto LoadList(const std::string& presetFolderPath, IVisualizationPresetOutputPort* port)->bool;
		auto Load(const std::string& path, Entity::Scene::ID sceneId,
			      IVisualizationPresetOutputPort* port, IVisualizationDataReadPort* reader)->bool;
		auto Save(const std::string& path, const bool& saveAs, Entity::Scene::ID sceneId,
			      IVisualizationPresetOutputPort* port, IVisualizationDataWritePort* writer)->bool;
		auto Delete(const std::string& path)->bool;
	};
}