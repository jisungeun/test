#pragma once

#include <Image.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API IFileReaderPort {
	public:
		IFileReaderPort();
		virtual ~IFileReaderPort();
		
		virtual auto Read(const std::string& path)->Entity::Image::Pointer = 0;
	};
}