#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {

	class Viewer2dUseCase_API ISceneOutputPort {
	public:
		ISceneOutputPort();
		virtual ~ISceneOutputPort();
	
	    virtual void Update(Entity::Scene::Pointer& scene) = 0;
	};
}
