#pragma once

//#include "TomoAnalysisUseCaseExport.h"
#include <Scene.h>

#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API AllocateScene {
	public:
		AllocateScene();
		~AllocateScene();

		auto Request()->Entity::Scene::ID;
	};
}