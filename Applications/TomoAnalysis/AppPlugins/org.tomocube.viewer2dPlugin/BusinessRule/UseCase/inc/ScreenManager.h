#pragma once
#include <Scene.h>

#include <ISceneOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API ScreenManager {
	public:
		ScreenManager();
		~ScreenManager();

		auto SetVisibleBoundaryBox(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetVisibleAxisGrid(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetVisibleOrientationMarker(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetVisibleTimeStamp(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetTimeStampColor(int r, int g, int b, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetTimeStampSize(int size, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetResolution(int resolution, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetEnableOverlay(bool enable, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetAxisGridFontSize(int size, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
		auto SetAxisGridColor(int axis, int r, int g, int b, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
	};
}