#pragma once
#include <Scene.h>

#include <ISceneOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API LayoutManager {
	public:
		LayoutManager();
		~LayoutManager();

		auto SetLayoutType(const Entity::LayoutType& type, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
	};
}