#pragma once

#include <string>

#include <Scene.h>

#include <ISceneOutputPort.h>
#include <IFileReaderPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API ImageAccessor {
	public:
		ImageAccessor();
		~ImageAccessor();

		auto SetModality(const Entity::Modality& modality, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) const ->bool;
		auto SetTimelapseFrame(const double& frameTime, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) const -> bool;
	};
}