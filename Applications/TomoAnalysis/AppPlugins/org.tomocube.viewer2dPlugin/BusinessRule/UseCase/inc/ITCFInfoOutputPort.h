#pragma once

#include <Image.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API ITCFInfoOutputPort {
	public:
		ITCFInfoOutputPort();
		virtual ~ITCFInfoOutputPort();

		virtual void Update(Entity::Image::Pointer& image) = 0;
	};
}