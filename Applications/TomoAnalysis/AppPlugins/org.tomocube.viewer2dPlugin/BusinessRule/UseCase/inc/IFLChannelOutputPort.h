#pragma once

#include <Scene.h>

#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API IFLChannelOutputPort {
	public:
		IFLChannelOutputPort();
		virtual ~IFLChannelOutputPort();

		virtual void Update(const Entity::FLChannelInfo& info) = 0;
	};
}