#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API IVisualizationPresetOutputPort {
	public:
		IVisualizationPresetOutputPort();
		virtual ~IVisualizationPresetOutputPort();

		virtual void Update(const std::string& tcfFolderPath) = 0;
		virtual void LoadList(const std::vector<std::string>& list) = 0;
		virtual void LoadPreset(const std::string& path, const Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel) = 0;
		virtual void SavePreset(const std::string& path, const bool& saveAs) = 0;
	};
}