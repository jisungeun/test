#pragma once

#include <Scene.h>

#include <ISliceNavigateOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API PlaneNavigator {
	public:
		PlaneNavigator();
		~PlaneNavigator();

		auto MovePlane(const int&x, const int& y, const int& z, Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool;
		auto MovePhyPlane(const float& x, const float& y, const float& z, Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool;
        auto MoveSlice(const int& viewIndex, const int& sliceIndex, Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool;
		auto InitSlice(Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool;
	};
}