#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API ITransferFunctionOutputPort {
	public:
		ITransferFunctionOutputPort();
		virtual ~ITransferFunctionOutputPort();

		virtual void Update(const int& index, const Entity::TFItem::Pointer& item) = 0;
		virtual void Clear(void) = 0;
	};
}