#pragma once

#include <string>

#include <Scene.h>

#include <ITCFInfoOutputPort.h>
#include <IFileReaderPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API ImageLoader {
	public:
		ImageLoader();
		~ImageLoader();

		auto Load(const std::string& path, Entity::Scene::ID sceneId, 
			      ITCFInfoOutputPort* port, IFileReaderPort* reader) const ->bool;
	};
}