#pragma once

#include <TransferFunction.h>

#include <Scene.h>
//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {
	class Viewer2dUseCase_API IVisualizationDataReadPort {
	public:
		IVisualizationDataReadPort();
		virtual ~IVisualizationDataReadPort();

		virtual auto Read(const std::string& path, Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel)->bool = 0;
	};
}