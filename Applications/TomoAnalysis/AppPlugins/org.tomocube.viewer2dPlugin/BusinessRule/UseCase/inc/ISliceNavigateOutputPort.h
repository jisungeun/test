#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "Viewer2dUseCaseExport.h"

namespace TomoAnalysis::Viewer2D::UseCase {

	class Viewer2dUseCase_API ISliceNavigateOutputPort {
	public:
		ISliceNavigateOutputPort();
		virtual ~ISliceNavigateOutputPort();
	
	    virtual void Update(Entity::Scene::Pointer& scene, const int& posX, const int& posY, const int& posZ) = 0;
		virtual void UpdatePhy(Entity::Scene::Pointer& scene, const float& x, const float& y, const float& z) = 0;
	};
}
