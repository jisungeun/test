#pragma once

#include <memory>
#include <QDockWidget>
#include <QWidget>

#include <TransferFunction.h>
#include <ITransferFunctionPanel.h>

#include "TomoAnalysis2dTransferFunctionPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dTransferFunctionPanel_API TransferFunctionPanel : public QWidget, public Interactor::ITransferFunctionPanel {
        Q_OBJECT

    public:
        enum class Type {
            one = 1,
            two = 2,
        };

    public:
        typedef TransferFunctionPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        TransferFunctionPanel(QWidget* parent=nullptr);
        virtual ~TransferFunctionPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;
        auto Refresh()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;

        auto GetCanvasWidget(void) const ->QWidget*;

        auto SetType(const Type& type) const ->void;

        auto SetRIRange(const float& min, const float& max) const ->void;
        auto SetGradientRange(const float& min, const float& max) const ->void;
        auto SetTransferFunctionItems(const Entity::TFItemList& items) const ->void;                

        auto forceCloseDock()->void;

    signals:
        void callUpdate();
        void itemChanged(int index, Entity::TFItem::Pointer item);
        void histogramChanged(int index);
        void overlayChanged(bool isOverlay);
        void selectItem(int index);

        void clearItems();

    protected slots:
        void onUpdate();
        void on_grayscaleRadioButton_clicked(bool checked);
        void on_heatmapRadiobutton_clicked(bool checked);

        void on_RIMinSpinBox_valueChanged(double value);
        void on_RIMaxSpinBox_valueChanged(double value);

        void on_gradMinSpinBox_valueChanged(double value);
        void on_gradMaxSpinBox_valueChanged(double value);

        void on_overlayCheckBox_stateChanged(int state);
        void on_mappingCheckBox_stateChanged(int state);

        void on_opacitySlider_valueChanged(int value);
        void on_opacitySpinBox_valueChanged(double value);

        void on_itemTableWidget_itemSelectionChanged();

        void on_clearButton_clicked(bool checked);
        void on_copyButton_clicked(bool checked);
        void on_saveButton_clicked(bool checked);
       
        void onCurrentTransferFunctionChanged(int index);
                
        void OnDockRemoved(bool toplevel);

    private:
        auto SetEnableUI(const bool& enable) const -> void;
        auto SetEnableItemControlUI(const bool& enable) const -> void;
        auto SetEnableCommonUI(const bool& enable) const -> void;

        auto GetItemTableWidgetHeader(void) const ->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}