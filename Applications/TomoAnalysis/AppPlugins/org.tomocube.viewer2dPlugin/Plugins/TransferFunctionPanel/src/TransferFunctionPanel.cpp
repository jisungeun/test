#include <iostream>

#include <QMainWindow>
#include <QClipboard>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QTextStream>

#include <UIUtility.h>
#include <RetrieveDockWidget.h>

#include "ui_TransferFunctionPanel.h"
#include "TransferFunctionPanel.h"


namespace  TomoAnalysis::Viewer2D::Plugins {
    struct TransferFunctionPanel::Impl {
        Ui::TransferFunctionPanel* ui{ nullptr };
        Type type{ Type::two };
        int currentIndex = -1;

        Entity::TFItemList items;

        QMainWindow* dockWindow;
        TC::RetrieveDockWidget* dock;
        QWidget* canvasWidget;
    };

    TransferFunctionPanel::TransferFunctionPanel(QWidget* parent) : d{ new Impl } {
        Q_UNUSED(parent)
        d->ui = new Ui::TransferFunctionPanel();
        d->ui->setupUi(this);

        d->canvasWidget = new QWidget;
        d->canvasWidget->setContentsMargins(0, 0, 0, 0);

        Qt::DockWidgetArea area = Qt::TopDockWidgetArea;

        d->dock = new TC::RetrieveDockWidget;
        d->dock->setObjectName("dockwidget-inner");
        d->dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
        d->dock->setAllowedAreas(area);
        d->dock->setContentsMargins(0, 0, 0, 0);
        d->dock->SetFloatingSize(512, 512);
        d->dock->setWidget(d->canvasWidget);        

        d->dockWindow = new QMainWindow;
        d->dockWindow->setTabPosition(area, QTabWidget::TabPosition::North);
        d->dockWindow->setCentralWidget(nullptr);
        d->dockWindow->setContentsMargins(0, 0, 0, 0);
        d->dockWindow->addDockWidget(area, d->dock);
        d->dockWindow->setFixedSize(280, 280);

        d->ui->canvasLayout->addWidget(d->dockWindow);

        // set object names
        d->ui->baseWidget->setObjectName("panel-base");
        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->RIRangeLabel->setObjectName("h9");
        d->ui->gradRangeLabel->setObjectName("h9");
        d->ui->opacityLabel->setObjectName("h9");
        d->ui->clearButton->setObjectName("bt-square-gary");
        d->ui->copyButton->setObjectName("bt-square-gary");
        d->ui->saveButton->setObjectName("bt-square-primary");

        d->ui->clearButton->AddIcon(":/img/ic-clear.svg");
        d->ui->clearButton->AddIcon(":/img/ic-clear-d.svg", QIcon::Disabled);
        d->ui->clearButton->AddIcon(":/img/ic-clear-s.svg", QIcon::Active);
        d->ui->clearButton->AddIcon(":/img/ic-clear-s.svg", QIcon::Selected);

        d->ui->copyButton->AddIcon(":/img/ic-copy.svg");
        d->ui->copyButton->AddIcon(":/img/ic-copy-d.svg", QIcon::Disabled);
        d->ui->copyButton->AddIcon(":/img/ic-copy-s.svg", QIcon::Active);
        d->ui->copyButton->AddIcon(":/img/ic-copy-s.svg", QIcon::Selected);                

        connect(d->dock, SIGNAL(topLevelChanged(bool)), this, SLOT(OnDockRemoved(bool)));
        connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdate()));

        d->dock->hide();
    }

    TransferFunctionPanel::~TransferFunctionPanel() {        
        delete d->ui;
    }

    auto TransferFunctionPanel::forceCloseDock() -> void {
        if (d->dock) {
            d->dock->close();
            delete d->dock;
        }
    }

    void TransferFunctionPanel::OnDockRemoved(bool toplevel) {
        if(toplevel) {
            d->dock->setWindowTitle("2D Transfer Function Canvas");
        }else {
            d->dock->setWindowTitle("");
        }
    }

    auto TransferFunctionPanel::UpdateCall() -> bool {
        emit callUpdate();
        return true;
    }

    void TransferFunctionPanel::onUpdate() {
        this->Update();
    }

    auto TransferFunctionPanel::Update()->bool {
        const auto ds = GetHTMetaDS();

        if (false == ds->isExistHT) {
            return true;
        }

        this->SetEnableCommonUI(true);
        
        this->SetRIRange(ds->intensityMin, ds->intensityMax);

        return true;
    }

    auto TransferFunctionPanel::Refresh()->bool {
        /*const auto ds = GetTransferFunctionDS();
        this->SetTransferFunctionItems(ds->list);*/

        return true;
    }

    auto TransferFunctionPanel::Init(void) const ->bool {
        this->SetType(Type::two);

        // init item table widget
        const QStringList header = { "RI Min", "RI Max", "Opacity" };
	    d->ui->itemTableWidget->setColumnCount(3);
	    d->ui->itemTableWidget->setHorizontalHeaderLabels(header);
	    d->ui->itemTableWidget->setColumnWidth(0, 80);
	    d->ui->itemTableWidget->setColumnWidth(1, 80);
	    d->ui->itemTableWidget->setColumnWidth(2, 57);
        d->ui->itemTableWidget->setProperty("styleVariant",2);

        d->ui->RIMinSpinBox->setSingleStep(0.0001);
        d->ui->RIMaxSpinBox->setSingleStep(0.0001);

        d->ui->gradMinSpinBox->setSingleStep(0.05);
        d->ui->gradMinSpinBox->setDecimals(2);
        d->ui->gradMaxSpinBox->setSingleStep(0.05);
        d->ui->gradMaxSpinBox->setDecimals(2);

        TC::SilentCall(d->ui->grayscaleRadioButton)->setChecked(true);

        this->SetEnableUI(false);

        return true;
    }

    auto TransferFunctionPanel::Reset(void) const ->bool {
        d->type = Type::two;
        d->currentIndex = -1;

        d->items.clear();

        while (d->ui->itemTableWidget->rowCount() > 0)
        {
            TC::SilentCall(d->ui->itemTableWidget)->removeRow(0);
        }

        return true;
    }

    auto TransferFunctionPanel::GetCanvasWidget(void) const ->QWidget* {
        return d->canvasWidget;
    }

    auto TransferFunctionPanel::SetType(const Type& type) const ->void {
        d->type = type;

        bool isVisible = true;
        if(Type::one == d->type) {
            isVisible = false;
        }

        d->ui->grayscaleRadioButton->setVisible(isVisible);
        d->ui->heatmapRadiobutton->setVisible(isVisible);

        d->ui->gradRangeLabel->setVisible(isVisible);
        d->ui->gradMinSpinBox->setVisible(isVisible);
        d->ui->gradTextLabel->setVisible(isVisible);
        d->ui->gradMaxSpinBox->setVisible(isVisible);

        d->ui->mappingCheckBox->setVisible(!isVisible);
        d->ui->overlayCheckBox->setVisible(isVisible);
    }

    auto TransferFunctionPanel::SetRIRange(const float& min, const float& max) const ->void {
        TC::SilentCall(d->ui->RIMinSpinBox)->setRange(min, max);
        TC::SilentCall(d->ui->RIMaxSpinBox)->setRange(min, max);
    }

    auto TransferFunctionPanel::SetGradientRange(const float& min, const float& max) const ->void {
        TC::SilentCall(d->ui->gradMinSpinBox)->setRange(min, max);
        TC::SilentCall(d->ui->gradMaxSpinBox)->setRange(min, max);
    }

    auto TransferFunctionPanel::SetTransferFunctionItems(const Entity::TFItemList& items) const ->void {
        while (d->ui->itemTableWidget->rowCount() > 0)
        {
            TC::SilentCall(d->ui->itemTableWidget)->removeRow(0);
        }

        for(auto item : items) {
            const auto row = d->ui->itemTableWidget->rowCount();
            d->ui->itemTableWidget->insertRow(row);

            auto label1 = new QLabel(QString("%1").arg(item->intensityMin / 10000.0));
            auto label2 = new QLabel(QString("%1").arg(item->intensityMax / 10000.0));
            auto label3 = new QLabel(QString("%1").arg(item->transparency));

            QColor color(item->color.red, item->color.green, item->color.blue, item->color.alpha);
            label1->setStyleSheet(QString(" border-radius: 0px; background-color: %1;").arg(color.name(QColor::HexArgb)));
            label2->setStyleSheet(QString(" border-radius: 0px; background-color: %1;").arg(color.name(QColor::HexArgb)));
            label3->setStyleSheet(QString(" border-radius: 0px; background-color: %1;").arg(color.name(QColor::HexArgb)));

            d->ui->itemTableWidget->setCellWidget(row, 0, label1);
            d->ui->itemTableWidget->setCellWidget(row, 1, label2);
            d->ui->itemTableWidget->setCellWidget(row, 2, label3);
        }

        d->items = items;
    }

    void TransferFunctionPanel::on_grayscaleRadioButton_clicked(bool checked) {
        Q_UNUSED(checked)
        emit histogramChanged(0);
    }

    void TransferFunctionPanel::on_heatmapRadiobutton_clicked(bool checked) {
        Q_UNUSED(checked)
        emit histogramChanged(1);
    }

    void TransferFunctionPanel::on_RIMinSpinBox_valueChanged(double value) {
        if(0 > d->currentIndex) {
            return;
        }

        if(d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if(!item) {
            return;
        }

        item->intensityMin = value * 10000;

        // set item table
        const auto text = QString("%1").arg(value);
        
        auto tablelabel = (QLabel*)d->ui->itemTableWidget->cellWidget(d->currentIndex,0);
        tablelabel->setText(text);
                
        //TC::SilentCall(d->ui->itemTableWidget)->item(d->currentIndex, 0)->setText(text);

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_RIMaxSpinBox_valueChanged(double value) {
        if (0 > d->currentIndex) {
            return;
        }

        if (d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if (!item) {
            return;
        }

        item->intensityMax = value * 10000;

        // set item table
        const auto text = QString("%1").arg(value);

        auto itemLabel = (QLabel*)d->ui->itemTableWidget->cellWidget(d->currentIndex,1);
        itemLabel->setText(text);
        //TC::SilentCall(d->ui->itemTableWidget)->item(d->currentIndex, 1)->setText(text);

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_gradMinSpinBox_valueChanged(double value) {
        if (0 > d->currentIndex) {
            return;
        }

        if (d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if (!item) {
            return;
        }

        item->gradientMin = value;

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_gradMaxSpinBox_valueChanged(double value) {
        if (0 > d->currentIndex) {
            return;
        }

        if (d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if (!item) {
            return;
        }

        item->gradientMax = value;

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_overlayCheckBox_stateChanged(int state) {
        bool isOverlay = true;
        if (Qt::Unchecked == state) {
            isOverlay = false;
        }

        emit overlayChanged(isOverlay);
    }

    void TransferFunctionPanel::on_mappingCheckBox_stateChanged(int state) {
        Q_UNUSED(state)
        // to do
    }

    void TransferFunctionPanel::on_opacitySlider_valueChanged(int value) {
        if (0 > d->currentIndex) {
            return;
        }

        if (d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if (!item) {
            return;
        }

        item->transparency = value / 100.f;
        TC::SilentCall(d->ui->opacitySpinBox)->setValue(value / 100.0);

        // set item table
        auto temp = new QTableWidgetItem(QString("%1").arg(value / 100.f));
        temp->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        TC::SilentCall(d->ui->itemTableWidget)->setItem(d->currentIndex, 2, temp);

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_opacitySpinBox_valueChanged(double value) {
        if (0 > d->currentIndex) {
            return;
        }

        if (d->currentIndex >= d->items.size()) {
            return;
        }

        auto item = d->items[d->currentIndex];
        if (!item) {
            return;
        }

        item->transparency = value;
        TC::SilentCall(d->ui->opacitySlider)->setValue(value * 100);

        // set item table
        auto temp = new QTableWidgetItem(QString("%1").arg(value));
        temp->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        TC::SilentCall(d->ui->itemTableWidget)->setItem(d->currentIndex, 2, temp);

        emit itemChanged(d->currentIndex, item);
    }

    void TransferFunctionPanel::on_itemTableWidget_itemSelectionChanged() {
        auto row = d->ui->itemTableWidget->currentRow();

        emit selectItem(row);
    }

    void TransferFunctionPanel::on_clearButton_clicked(bool checked) {
        Q_UNUSED(checked)
        if (true == d->items.empty()) {
            return;
        }

        emit clearItems();
    }

    void TransferFunctionPanel::on_copyButton_clicked(bool checked) {
        Q_UNUSED(checked)
        if(true == d->items.empty()) {
            return;
        }

        auto table = d->ui->itemTableWidget;
        const auto columns = table->columnCount();
        const auto rows = table->rowCount();

        QStringList header;
        for (auto col = 0; col < columns; ++col) {
            header << table->horizontalHeaderItem(col)->text();
        }

        QStringList contents;

        QString line;
        for (auto idx = 0; idx < columns; idx++) {
            line.push_back(table->horizontalHeaderItem(idx)->text());
            if (idx < (columns - 1)) {
                line.push_back("\t");
            }
        }
        contents.push_back(line);

        for (auto row = 0; row < rows; row++) {
            line.clear();
            for (auto idx = 0; idx < columns; idx++) {
                const auto item = static_cast<QLabel*>(d->ui->itemTableWidget->cellWidget(row, idx));
                if (nullptr == item) {
                    continue;
                }

                line.push_back(item->text());
                if (idx < (columns - 1)) line.push_back("\t");
            }
            contents.push_back(line);
        }

        QString text;
        QTextStream out(&text);
        for (const auto content : contents) {
            out << content << "\n";
        }

        QClipboard* clipboard = QGuiApplication::clipboard();
        clipboard->setText(text);
    }

    void TransferFunctionPanel::on_saveButton_clicked(bool checked) {
        Q_UNUSED(checked);

        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentExportTF2CSV").toString();

        const auto path = QFileDialog::getSaveFileName(this, "Select a file to save volume visualization table", prev, "CSV (*.csv)");
        if(true == path.isEmpty()) {
            return;;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentExportTF2CSV", path);
        const auto table = d->ui->itemTableWidget;
        const auto columns = table->columnCount();
        const auto rows = table->rowCount();

        QStringList contents;
        // get header
        for(auto i = 0; i < columns; ++i) {
            contents << table->horizontalHeaderItem(i)->text();
            if(i == columns -1) {
                contents << "\n";
            }
            else{
                contents << ",";
            }
        }

        // get tf info
        for (auto i = 0; i < rows; ++i) {
            for (auto j = 0; j < columns; ++j) {
                const auto item = static_cast<QLabel*>(d->ui->itemTableWidget->cellWidget(i, j));
                if(nullptr == item) {
                    continue;
                }

                contents << item->text() << ",";
            }
            contents << "\n";
        }

        QFile file(path);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox::warning(this, "Save transfer function table", "Failed to write transfer function table");
            file.close();
            return;
        }

        QTextStream out(&file);
        for (const auto& content : contents) {
            out << content;
        }

        file.close();
    }

    void TransferFunctionPanel::onCurrentTransferFunctionChanged(int index) {
        d->currentIndex = index;

        bool enable = true;
        if(-1 == d->currentIndex ) {
            TC::SilentCall(d->ui->itemTableWidget)->clearSelection();
            enable = false;
        }
        else {
            const auto currentItem = d->items.at(index);

            TC::SilentCall(d->ui->RIMinSpinBox)->setValue(currentItem->intensityMin / 10000.0);
            TC::SilentCall(d->ui->RIMaxSpinBox)->setValue(currentItem->intensityMax / 10000.0);

            if (Type::two == d->type) {
                TC::SilentCall(d->ui->gradMinSpinBox)->setValue(currentItem->gradientMin);
                TC::SilentCall(d->ui->gradMaxSpinBox)->setValue(currentItem->gradientMax);
            }

            TC::SilentCall(d->ui->opacitySlider)->setValue(currentItem->transparency * 100);
            TC::SilentCall(d->ui->opacitySpinBox)->setValue(currentItem->transparency);

            TC::SilentCall(d->ui->itemTableWidget)->selectRow(index);
        }

        this->SetEnableItemControlUI(enable);
    }

    auto TransferFunctionPanel::SetEnableUI(const bool& enable) const -> void {
        SetEnableItemControlUI(enable);
        SetEnableCommonUI(enable);
    }

    auto TransferFunctionPanel::SetEnableItemControlUI(const bool& enable) const -> void {
        d->ui->RIRangeLabel->setEnabled(enable);
        d->ui->RIMinSpinBox->setEnabled(enable);
        d->ui->RIMaxSpinBox->setEnabled(enable);
        d->ui->gradRangeLabel->setEnabled(enable);
        d->ui->gradMinSpinBox->setEnabled(enable);
        d->ui->gradMaxSpinBox->setEnabled(enable);
        d->ui->opacityLabel->setEnabled(enable);
        d->ui->opacitySlider->setEnabled(enable);
        d->ui->opacitySpinBox->setEnabled(enable);
    }

    auto TransferFunctionPanel::SetEnableCommonUI(const bool& enable) const -> void {
        d->ui->grayscaleRadioButton->setEnabled(enable);
        d->ui->heatmapRadiobutton->setEnabled(enable);

        d->ui->overlayCheckBox->setEnabled(enable);
        d->ui->mappingCheckBox->setEnabled(enable);
    }

    auto TransferFunctionPanel::GetItemTableWidgetHeader(void) const ->QStringList {
        const auto table = d->ui->itemTableWidget;
        const auto columns = table->columnCount();

        QStringList header;
        for (auto col = 0; col < columns; ++col) {
            header << table->horizontalHeaderItem(col)->text();
        }

        return header;
    }
}
