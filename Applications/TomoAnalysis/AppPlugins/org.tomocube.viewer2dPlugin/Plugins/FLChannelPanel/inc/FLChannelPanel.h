#pragma once

#include <memory>
#include <QWidget>
#include <QTableWidget>

#include <VisualizationInfo.h>
#include <Scene.h>
#include <IFLChannelPanel.h>

#include "TomoAnalysis2dFLChannelPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dFLChannelPanel_API FLChannelPanel : public QWidget, public Interactor::IFLChannelPanel {
        Q_OBJECT
    public:
        typedef FLChannelPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        FLChannelPanel(QWidget* parent=nullptr);
        virtual ~FLChannelPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;

        auto Init(void) -> bool;
        auto Reset(void) ->bool;

        auto SetCh1(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible)  ->void;
        auto SetCh2(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible)  ->void;        
        auto SetCh3(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible)  ->void;
        auto SetOpacity(const int& opacity,const int& ch) const ->void;

        auto SetTcfPath(const QString& path)->void;

        auto SetCh1Meta(const AppEntity::ColormapInfo& meta)->void;
        auto SetCh2Meta(const AppEntity::ColormapInfo& meta)->void;
        auto SetCh3Meta(const AppEntity::ColormapInfo& meta)->void;

    signals:
        void callUpdate();
        void channelVisibleChanged(Entity::Channel channel, bool visible);
        void channelRangeChanged(Entity::Channel channel, int min, int max);
        void channelOpacityChanged(Entity::Channel channel, int opacity);
        void channelColorChanged(Entity::Channel channel, QColor color);
        void channelGammaChanged(Entity::Channel channel, bool enable, double gamma);
        void vizControlFocused(bool);        

    protected slots:
        void onUpdate();
        void onActivatedModalityChanged(Entity::Modality);
                
        void OnShowButton();
        void OnHideButton();

        void OnCh1CheckBox(int state);
        void OnCh2CheckBox(int state);
        void OnCh3CheckBox(int state);        
        void OnCh1OpacityChanged(double);
        void OnCh2OpacityChanged(double);
        void OnCh3OpacityChanged(double);

        void OnToggleCh1Gamma(bool);
        void OnToggleCh2Gamma(bool);
        void OnToggleCh3Gamma(bool);

        void OnCh1GammaValue(double);
        void OnCh2GammaValue(double);
        void OnCh3GammaValue(double);

        void OnCh1ResetRange();
        void OnCh2ResetRange();
        void OnCh3ResetRange();

        void OnFLRangeChanged(const QString& txt);
                
        void OnCh1NameBtn();
        void OnCh1ColorBtn();
        void OnCh2NameBtn();
        void OnCh2ColorBtn();
        void OnCh3NameBtn();
        void OnCh3ColorBtn();

        void OnCh1RangeChanged(double, double);
        void OnCh2RangeChanged(double, double);
        void OnCh3RangeChanged(double, double);

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    private:
        auto SetEnableUI(const bool& enable) const -> void;            
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}