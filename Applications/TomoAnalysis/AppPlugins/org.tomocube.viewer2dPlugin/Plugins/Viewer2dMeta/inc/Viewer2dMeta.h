#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "Viewer2dMetaExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
	class Viewer2dMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.viewre2dMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "2D View"; }
		auto GetFullName()const->QString override { return "org.tomocube.viewre2dPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}