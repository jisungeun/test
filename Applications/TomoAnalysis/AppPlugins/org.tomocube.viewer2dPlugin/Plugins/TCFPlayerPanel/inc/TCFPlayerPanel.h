#pragma once

#include <memory>
#include <QWidget>
#include <ITCFPlayerPanel.h>
#include "TomoAnalysis2dTCFPlayerPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
	class TomoAnalysis2dTCFPlayerPanel_API TCFPlayerPanel : public QWidget, public Interactor::ITCFPlayerPanel {
		Q_OBJECT
	public:
		typedef TCFPlayerPanel Self;
		typedef std::shared_ptr<Self> Pointer;

		TCFPlayerPanel(QWidget* parent = nullptr);
		~TCFPlayerPanel();

		auto UpdateCall() -> bool override;
		auto Update() -> bool override;

		auto Init(void) const -> bool;
		auto Reset(void) const -> bool;

		auto IsProgress(void) const -> bool;
		auto SetProgress(const bool& isProgress) -> void;
		auto SetSliceIndex(const int& index) const -> void;
		auto ForceTimeIndex(const int& index) const -> void;
		auto GetTimePoints() const -> std::vector<double>;
		auto GetCurTimeStep() const -> int;
		void onTimeStepPlayed(double time);
		/*static*/
		void Progress(void* instance, const int& start, const int& end, const float& interval);

	private:
		auto SetPlayingState(bool isPlaying) -> void;

	signals:
		void startPlaying();
		void finishPlaying();

		void callUpdate();
		//void timelapseIndexChanged(int index);
		void timelapseIndexChanged(double time);
		//void waitTimelapseIndexChanged(int index);
		void waitTimelapseIndexChanged(double time);

		void timelapseInfoChanged();

	protected slots:
		void onUpdate();

		void onRepeatClicked();

		void on_playButton_pressed();

		void on_progressSlider_valueChanged(int value);
		void on_progressSpinBox_valueChanged(int value);

		void onTimelapseChanged(Entity::Modality modality);
		void onFinishedPlay();

		//void SliderEnabler();        
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
