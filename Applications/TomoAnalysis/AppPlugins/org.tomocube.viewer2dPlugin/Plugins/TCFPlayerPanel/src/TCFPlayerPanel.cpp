#include <iostream>
#include <windows.h>

#include <QtConcurrent/QtConcurrent>
//#include <Inventor/SoDB.h>
//#include <Inventor/threads/SbThread.h>
#include <UIUtility.h>

#include "ui_TCFPlayerPanel.h"
#include "TCFPlayerPanel.h"

namespace TomoAnalysis::Viewer2D::Plugins {
	struct TCFPlayerPanel::Impl {
		Ui::TCFPlayerPanel* ui { nullptr };
		Interactor::TimelapesDS::Pointer timelapseInfoList;

		bool isProgress = false;
		int range = 0;
		int interval = 0;
		QVector<double> time_points;

		double wait_time { -1 };
		int cur_idx { -1 };

		QFuture<void> future;
		QFutureWatcher<void> watcher;

		bool inTermination { false };
		bool isRepeat { false };
	};

	TCFPlayerPanel::TCFPlayerPanel(QWidget* parent)
		: QWidget(parent)
		, Interactor::ITCFPlayerPanel()
		, d { new Impl } {
		d->ui = new Ui::TCFPlayerPanel();
		d->ui->setupUi(this);

		// set object names
		d->ui->contentsWidget->setObjectName("panel-base");
		d->ui->playButton->setObjectName("bt-square-gray");

		SetPlayingState(false);
	}

	TCFPlayerPanel::~TCFPlayerPanel() {
		d->inTermination = true;
	}

	auto TCFPlayerPanel::Update() -> bool {
		const auto ds = GetTimelapesDS();
		if (!ds) {
			return false;
		}

		d->timelapseInfoList = ds;

		return true;
	}

	auto TCFPlayerPanel::Init(void) const -> bool {
		const auto isEnabled = false;

		TC::SilentCall(d->ui->playButton)->setEnabled(isEnabled);
		TC::SilentCall(d->ui->progressSlider)->setEnabled(isEnabled);
		TC::SilentCall(d->ui->progressSpinBox)->setEnabled(isEnabled);

		connect(&d->watcher, SIGNAL(finished()), this, SLOT(onFinishedPlay()));
		connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdate()));
		connect(d->ui->repeatChk, SIGNAL(clicked()), this, SLOT(onRepeatClicked()));

		return true;
	}

	auto TCFPlayerPanel::UpdateCall() -> bool {
		emit callUpdate();
		return true;
	}


	void TCFPlayerPanel::onUpdate() {
		this->Update();
	}

	void TCFPlayerPanel::onRepeatClicked() {
		d->isRepeat = d->ui->repeatChk->isChecked();
	}


	auto TCFPlayerPanel::Reset(void) const -> bool {
		auto ds = GetTimelapesDS();
		ds.reset();
		d->isProgress = false;
		d->range = 1;
		d->interval = 0;

		d->timelapseInfoList = nullptr;

		TC::SilentCall(d->ui->progressSlider)->setValue(1);
		TC::SilentCall(d->ui->progressSpinBox)->setValue(1);

		return true;
	}

	auto TCFPlayerPanel::IsProgress(void) const -> bool {
		return d->isProgress;
	}

	auto TCFPlayerPanel::SetProgress(const bool& isProgress) -> void {
		d->isProgress = isProgress;

		SetPlayingState(false);
	}

	auto TCFPlayerPanel::SetSliceIndex(const int& index) const -> void {
		TC::SilentCall(d->ui->progressSpinBox)->setValue(index);
		TC::SilentCall(d->ui->progressSlider)->setValue(index);
	}

	auto TCFPlayerPanel::ForceTimeIndex(const int& index) const -> void {
		d->ui->progressSpinBox->setValue(index);
	}

	auto TCFPlayerPanel::GetTimePoints() const -> std::vector<double> {
		return std::vector(d->time_points.begin(), d->time_points.end());
	}

	auto TCFPlayerPanel::GetCurTimeStep() const -> int {
		return d->ui->progressSpinBox->value();
	}

	void TCFPlayerPanel::Progress(void* instance, const int& start, const int& end, const float& interval) {
		if (nullptr == instance) {
			return;
		}

		auto panel = static_cast<TCFPlayerPanel*>(instance);

		auto tp = panel->GetTimePoints();
		d->wait_time = -1;
		for (auto i = start; i < end; i++) {
			if (false == panel->IsProgress() || panel->d->inTermination) {
				return;
			}
			emit panel->waitTimelapseIndexChanged(tp[i - 1]);

			while (d->wait_time != tp[i - 1]) {
				QThread::msleep(100);
			}

			if (d->isRepeat && i == end - 1) {
				i = start - 1;
			}

			QThread::msleep(static_cast<unsigned long>(interval));
		}
	}

	auto TCFPlayerPanel::SetPlayingState(bool isPlaying) -> void {
		if (isPlaying) {
			d->ui->playButton->setText("Stop");

			d->ui->playButton->AddIcon(":/img/ic-stop.svg");
			d->ui->playButton->AddIcon(":/img/ic-stop-d.svg", QIcon::Disabled);
			d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Active);
			d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Selected);
		} else {
			d->ui->playButton->setText("Play");

			d->ui->playButton->AddIcon(":/img/ic-play.svg");
			d->ui->playButton->AddIcon(":/img/ic-play-d.svg", QIcon::Disabled);
			d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Active);
			d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Selected);
		}
	}

	void TCFPlayerPanel::on_playButton_pressed() {
		if (true == d->isProgress) {
			d->isProgress = false;
			SetPlayingState(false);

			return;
		}

		auto currentIndex = d->ui->progressSpinBox->value();

		if (currentIndex == d->ui->progressSpinBox->maximum()) {
			d->ui->progressSpinBox->setValue(d->ui->progressSpinBox->minimum());
			currentIndex = d->ui->progressSpinBox->minimum();
		}

		d->isProgress = true;
		SetPlayingState(true);

		d->future = QtConcurrent::run([=] {
			return Progress(this, currentIndex, d->range + 1, 700.f);
		});
		d->watcher.setFuture(d->future);
	}

	void TCFPlayerPanel::onTimeStepPlayed(double time) {
		d->wait_time = time;
		auto idx = d->time_points.indexOf(time);
		this->SetSliceIndex(idx + 1);
	}

	void TCFPlayerPanel::on_progressSlider_valueChanged(int value) {
		if (true == d->isProgress) {
			d->isProgress = false;
			SetPlayingState(false);
		}

		SetSliceIndex(value);
		emit timelapseIndexChanged(d->time_points[value - 1]);
	}

	void TCFPlayerPanel::on_progressSpinBox_valueChanged(int value) {
		if (true == d->isProgress) {
			d->isProgress = false;
			SetPlayingState(false);
		}

		SetSliceIndex(value);
		emit timelapseIndexChanged(d->time_points[value - 1]);
	}

	void TCFPlayerPanel::onTimelapseChanged(Entity::Modality modality) {
		const auto setTimelapse = [&](std::vector<Interactor::TimelapseInfo::Pointer> infos, double curTime) {
			auto isEnabled = false;
			d->time_points.clear();
			d->range = 1;
			d->interval = 0;

			if (infos.size() < 1) {
				return;
			}

			if (1 < infos[0]->dataCount) {
				isEnabled = true;
				d->interval = static_cast<int>(infos[0]->interval);
			}

			std::list<double> global_time_point;// = infos[0]->time_points;
			for (auto t : infos[0]->time_points) {
				for (auto tt : t) {
					global_time_point.push_back(tt);
				}
			}
			for (auto i = 1; i < infos.size(); i++) {
				auto info = infos[i];
				for (auto tps : info->time_points) {
					std::list<double>::iterator insertPos = global_time_point.begin();
					auto temp_s = 0;
					for (auto s = 0; s < tps.size() && insertPos != global_time_point.end();) {
						auto lval = tps[s];
						auto gval = *insertPos;
						if (gval > lval) {
							global_time_point.insert(insertPos, lval);
							s++;
						} else if (gval == lval) {
							insertPos++;
							s++;
							temp_s = s;
						} else {
							insertPos++;
							temp_s = s;
						}
					}
					for (auto ss = temp_s; ss < tps.size(); ss++) {
						global_time_point.push_back(tps[ss]);
					}
				}
			}

			d->range = static_cast<int>(global_time_point.size());

			for (auto l : global_time_point) {
				d->time_points.push_back(l);
			}

			TC::SilentCall(d->ui->playButton)->setEnabled(isEnabled);
			TC::SilentCall(d->ui->progressSlider)->setEnabled(isEnabled);
			TC::SilentCall(d->ui->progressSpinBox)->setEnabled(isEnabled);

			TC::SilentCall(d->ui->progressSlider)->setRange(1, d->range);
			TC::SilentCall(d->ui->progressSlider)->setValue(1);
			TC::SilentCall(d->ui->progressSpinBox)->setRange(1, d->range);
			TC::SilentCall(d->ui->progressSpinBox)->setValue(1);

			if (d->time_points.contains(curTime)) {
				const auto idx = d->time_points.indexOf(curTime);
				TC::SilentCall(d->ui->progressSlider)->setValue(idx + 1);
				TC::SilentCall(d->ui->progressSpinBox)->setValue(idx + 1);
			}
		};

		std::vector<Interactor::TimelapseInfo::Pointer> infos;

		if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP)) {
			Interactor::TimelapseInfo::Pointer info = std::make_shared<Interactor::TimelapseInfo>();
			info = d->timelapseInfoList->list[Entity::HT3D];
			infos.push_back(info);
		}
		if (((modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume ||
			(modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
			Interactor::TimelapseInfo::Pointer info = std::make_shared<Interactor::TimelapseInfo>();
			info = d->timelapseInfoList->list[Entity::FL3D];
			infos.push_back(info);
		}
		if ((modality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
			Interactor::TimelapseInfo::Pointer info = std::make_shared<Interactor::TimelapseInfo>();
			info = d->timelapseInfoList->list[Entity::BF];
			infos.push_back(info);
		}

		const auto currentIndex = d->ui->progressSpinBox->value();
		double curTimePoint = -1.0;
		if (d->time_points.count() > currentIndex) {
			curTimePoint = d->time_points[currentIndex - 1];
		}

		setTimelapse(infos, curTimePoint);

		emit timelapseInfoChanged();
	}

	void TCFPlayerPanel::onFinishedPlay() {
		SetProgress(false);

		d->isProgress = false;
		SetPlayingState(false);
	}
}
