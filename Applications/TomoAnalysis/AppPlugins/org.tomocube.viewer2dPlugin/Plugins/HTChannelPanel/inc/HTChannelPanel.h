#pragma once

#include <memory>
#include <QWidget>

#include <AnnotationInfo.h>
#include <VisualizationInfo.h>

#include "Tomoanalysis2dHTChannelPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct  VizMetaFlag {
        bool useHT;
        bool useFL[3];
        bool useCamera;
        auto isValid()const->bool {
            return useHT || useFL[0] || useFL[1] || useFL[2] || useCamera;
        }
    };
    struct AnnoMetaFlag {
        bool useScalar;
        bool useScale;
        bool useTimestamp;
        bool useDeviceInfo;
        auto isValid()const->bool {
            return useScalar || useScale || useTimestamp || useDeviceInfo;
        }
    };
    class TomoAnalysis2dHTChannelPanel_API HTChannelPanel : public QWidget {
        Q_OBJECT
    public:
        typedef HTChannelPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        HTChannelPanel(QWidget* parent = nullptr);
        virtual ~HTChannelPanel();

        auto SetTcfPath(const QString& path)->void;
        auto Reset()->void;

        auto SetMinResolution(int level)->void;

        auto ApplyScalarMeta(const AppEntity::ScalarBarInfo& meta)->void;
        auto ApplyScaleMeta(const AppEntity::ScaleBarInfo& meta)->void;
        auto ApplyTimeMeta(const AppEntity::TimeStampInfo& meta)->void;
        auto ApplyDeviceMeta(const AppEntity::DeviceInfo& meta)->void;

        auto ApplyHTMeta(const AppEntity::ColormapInfo& meta)->void;

        auto ForceLoadAnnoMeta(const QString& path)->void;
        auto ForceLoadVizMeta(const QString& path)->void;

    signals:
        void sigHTColormapChanged(bool isGamma, bool gamma, int predColormap);
        
        void sigTimeStampColor(QColor);
        void sigTimeStampSize(int);
        void sigTimeStampToggle(bool);

        void sigDeviceColor(QColor);
        void sigDeviceSize(int);
        void sigDeviceToggle(bool);

        void sigResolution(int);

        void sigScaleBarToggle(bool);
        void sigScalarBarToggle(bool);

        void sigMinimapToggle(bool);

        void sigColormapChanged(int, bool, double);

        void sigSaveVizMeta(Plugins::VizMetaFlag);
        void sigSaveAnnoMeta(Plugins::AnnoMetaFlag);
        void sigLoadVizMeta(QString, Plugins::VizMetaFlag);
        void sigLoadAnnoMeta(QString, Plugins::AnnoMetaFlag);

    protected slots:
        void OnToggleGamma(bool);
        void OnGammaValue(double);
        void OnColormapIdx(int);

        void OnTimeToggled();
        void OnScalarToggled();
        void OnDeviceToggled();
        void OnScaleToggled();
        void OnResolutionToggled();
        void OnMinimapToggled();

        void OnDeviceColor();
        void OnTimeStampColor();

        void OnDeviceFontSize(int);
        void OnTimeFontSize(int);
        void OnFixedResolution(int);

        void OnVizMetaInfoSave();
        void OnVizMetaInfoLoad();
        void OnAnnoMetaInfoSave();
        void OnAnnoMetaInfoLoad();

        auto OnWindowRange(double min, double max)->void;

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    private:
        auto InitUI()->void;
        auto InitConnections()->void;        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}