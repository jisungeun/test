#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::Viewer2D::Plugins {
    class SelectVisualizationMeta : public QDialog {
        Q_OBJECT
    public:
        explicit SelectVisualizationMeta(QWidget* parent, bool exist[5], bool isSave);
        virtual ~SelectVisualizationMeta();
    public:
        static auto Select(QWidget* parent, bool exist[5], bool isSave)->std::tuple<bool, bool, bool, bool, bool>;
        auto GetSelection()const->std::tuple<bool, bool, bool, bool, bool>;
    protected slots:
        void OnOkBtn();
        void OnCancelBtn();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}