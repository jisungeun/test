#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::Viewer2D::Plugins {
	class SelectAnnotationMeta : public QDialog {
		Q_OBJECT
	public:
		explicit SelectAnnotationMeta(QWidget* parent,bool exist[4], bool isSave);
		virtual ~SelectAnnotationMeta();
	public:
		static auto Select(QWidget* parent,bool exist[4], bool isSave)->std::tuple<bool, bool, bool, bool>;

		auto GetSelection()const->std::tuple<bool, bool, bool, bool>;
	protected slots:
		void OnOkBtn();
		void OnCancelBtn();
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}