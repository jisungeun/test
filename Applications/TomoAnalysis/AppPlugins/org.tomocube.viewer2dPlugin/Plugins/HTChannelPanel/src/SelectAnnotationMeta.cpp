#include <QStyle>

#include "ui_SelectAnnotationMeta.h"
#include "SelectAnnotationMeta.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct SelectAnnotationMeta::Impl {
        Ui::annoSelector ui;
    };
    SelectAnnotationMeta::SelectAnnotationMeta(QWidget* parent,bool exist[4], bool isSave) :QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        QString type = "LOAD";
        if(isSave) {
            type = "SAVE";
        }
        d->ui.label->setText(QString("Select target annotation property to %1").arg(type));
        if(false == exist[0]) {
            d->ui.scalarChk->setChecked(false);
            d->ui.scalarChk->setEnabled(false);
        }
        if(false == exist[1]) {
            d->ui.scaleChk->setChecked(false);
            d->ui.scaleChk->setEnabled(false);
        }
        if(false == exist[2]) {
            d->ui.timeChk->setChecked(false);
            d->ui.timeChk->setEnabled(false);
        }
        if(false == exist[3]) {
            d->ui.deviceChk->setChecked(false);
            d->ui.deviceChk->setEnabled(false);
        }
        connect(d->ui.okBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
        connect(d->ui.cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
    }
    SelectAnnotationMeta::~SelectAnnotationMeta() = default;
    auto SelectAnnotationMeta::GetSelection() const -> std::tuple<bool, bool, bool, bool> {
        return std::make_tuple(d->ui.scalarChk->isChecked(), d->ui.scaleChk->isChecked(), d->ui.timeChk->isChecked(), d->ui.deviceChk->isChecked());
    }
    auto SelectAnnotationMeta::Select(QWidget* parent, bool exist[4], bool isSave) -> std::tuple<bool, bool, bool, bool> {
        SelectAnnotationMeta dialog(parent,exist, isSave);
        dialog.setWindowTitle(QString("Select annotation meta information"));
        if(dialog.exec() != Accepted) {
            return std::make_tuple(false, false, false, false);
        }
        auto selection = dialog.GetSelection();

        return selection;
    }
    void SelectAnnotationMeta::OnCancelBtn() {
        reject();
    }
    void SelectAnnotationMeta::OnOkBtn() {
        accept();
    }
}