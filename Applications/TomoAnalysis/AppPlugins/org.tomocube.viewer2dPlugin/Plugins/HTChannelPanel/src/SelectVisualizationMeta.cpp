#include <QStyle>

#include "ui_SelectVisualizationMeta.h"
#include "SelectVisualizationMeta.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct SelectVisualizationMeta::Impl {
        Ui::Form ui;
    };
    SelectVisualizationMeta::SelectVisualizationMeta(QWidget* parent, bool exist[5], bool isSave) : QDialog(parent), d{ new Impl }{
        d->ui.setupUi(this);
        QString type = "LOAD";
        if(isSave) {
            type = "SAVE";
        }
        d->ui.label->setText(QString("Select target visualization property to %1").arg(type));
        if(false == exist[0]) {
            d->ui.htChk->setChecked(false);
            d->ui.htChk->setEnabled(false);
        }
        if(false == exist[1]) {
            d->ui.ch1Chk->setChecked(false);
            d->ui.ch1Chk->setEnabled(false);
        }
        if(false == exist[2]) {
            d->ui.ch2Chk->setChecked(false);
            d->ui.ch2Chk->setEnabled(false);
        }
        if(false == exist[3]) {
            d->ui.ch3Chk->setChecked(false);
            d->ui.ch3Chk->setEnabled(false);
        }
        if(false == exist[4]) {
            d->ui.cameraChk->setChecked(false);
            d->ui.cameraChk->setEnabled(false);
        }
        connect(d->ui.okBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
        connect(d->ui.cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
    }
    SelectVisualizationMeta::~SelectVisualizationMeta() = default;
    auto SelectVisualizationMeta::GetSelection() const -> std::tuple<bool, bool, bool, bool, bool> {
        return std::make_tuple(d->ui.htChk->isChecked(), d->ui.ch1Chk->isChecked(), d->ui.ch2Chk->isChecked(), d->ui.ch3Chk->isChecked(), d->ui.cameraChk->isChecked());
    }
    auto SelectVisualizationMeta::Select(QWidget* parent, bool exist[5], bool isSave) -> std::tuple<bool, bool, bool, bool, bool> {
        SelectVisualizationMeta dialog(parent, exist, isSave);
        dialog.setWindowTitle(QString("Select visualization meta information"));
        if(dialog.exec() != Accepted) {
            return std::make_tuple(false, false, false, false, false);
        }
        auto selection = dialog.GetSelection();
        return selection;
    }
    void SelectVisualizationMeta::OnCancelBtn() {
        reject();
    }
    void SelectVisualizationMeta::OnOkBtn() {
        accept();
    }

}