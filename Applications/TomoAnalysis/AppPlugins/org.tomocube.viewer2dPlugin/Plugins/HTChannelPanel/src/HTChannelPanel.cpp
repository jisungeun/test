#include <iostream>

#include <Hdf5Utilities.h>

#include <QIcon>
#include <QColorDialog>
#include <QFileDialog>
#include <QPixmap>

#include <H5Cpp.h>
#include <HDF5Mutex.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoTransferFunction.h>
#pragma warning(pop)

#include <TCFMetaReader.h>
#include <TcfH5GroupRoiLdmDataReader.h>

#include <ViewerMetaWriter.h>
#include <ViewerMetaReader.h>

#include "SelectAnnotationMeta.h"
#include "SelectVisualizationMeta.h"
#include "ui_HTChannelPanel.h"
#include "HTChannelPanel.h"


using namespace H5;
namespace TomoAnalysis::Viewer2D::Plugins {
    struct HTChannelPanel::Impl {
        Ui::HTChannel ui;

        int time_color[3] = { 255,255,255 };
        int device_color[3] = { 255,255,255 };
        QPixmap timePixmap;
        QPixmap devicePixmap;

        QString tcfPath;
        int htExist;
        bool flExist[3];
        int maxSignal;
        int minSignal;
        bool is8Bit{ false };
        int histogram_steps;
        auto ReadIsLdm()->bool;
        auto ReadOriginalData()->QList<int>;
        auto ReadOriginalDataLdm()->QList<int>;
        auto ReadOriginalData3D()->QList<int>;
        auto ReadOriginalData3DLdm()->QList<int>;        
        auto CalculateHistogram(QList<int> original_data,int div = 1)->QList<int>;
        SoTransferFunction* colormap{ nullptr };

        double gammaValue{ 1 };
        bool isGamma{ false };
        int colormapIdx{ 0 };

        auto ChangeColormap(int idx, bool isGamma, double gamma)->void;
        TC::IO::TCFMetaReader::Meta::Pointer metaInfo{ nullptr };
    };
   
    auto HTChannelPanel::Impl::ChangeColormap(int idx, bool isGamma, double gamma) -> void {
        switch (idx) {
        case 0:
            colormap->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
            break;
        case 1:
            colormap->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
            break;
        case 2:
            colormap->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
            break;
        case 3:
            colormap->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
            break;
        case 4:
            colormap->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
            break;
        }


        auto dummyTF = new SoTransferFunction;
        dummyTF->ref();
        dummyTF->predefColorMap = colormap->predefColorMap.getValue();
        auto steps = colormap->actualColorMap.getNum() / 4;
        colormap->actualColorMap.setNum(256 * 4);
        auto p = colormap->actualColorMap.startEditing();
        auto base = dummyTF->actualColorMap.getValues(0);
        auto actualGamma = gamma;
        if (false == isGamma) {
            actualGamma = 1;
        }
        for (auto i = 0; i < steps; i++) {
            auto r = *base++;
            auto g = *base++;
            auto b = *base++;
            *base++;
            float mod_r = pow(r, 1.0 / actualGamma);
            float mod_g = pow(g, 1.0 / actualGamma);
            float mod_b = pow(b, 1.0 / actualGamma);
            mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
            mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
            mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
            *p++ = 1;
        }
        dummyTF->unref();
        dummyTF = nullptr;
        colormap->actualColorMap.finishEditing();
    }

    auto HTChannelPanel::Impl::ReadOriginalData3DLdm() -> QList<int> {
        auto result = QList<int>();
        H5File file(tcfPath.toStdString(), H5F_ACC_RDONLY);
        auto htGroup = file.openGroup("Data/3D/000000");
        TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
        tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(htGroup);
        const TC::IO::DataRange roi(TC::IO::Point(0, 0, metaInfo->data.data3D.sizeZ/2), TC::IO::Count(metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY, 1));
        tcfH5GroupRoiLdmDataReader.SetReadingRoi(roi);


        auto base = 128;
        auto samplingstep = 0;
        const auto sizeX = metaInfo->data.data3D.sizeX;
        while (sizeX > base) {
            base *= 2;
            samplingstep++;
        }
        samplingstep -= 2;
        if (samplingstep < 0) {
            samplingstep = 0;
        }
    	samplingstep = static_cast<int>(pow(2, samplingstep));

        tcfH5GroupRoiLdmDataReader.SetSamplingStep(samplingstep);

        const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
        if (metaInfo->data.data3D.scalarType[0] == 1) {
            is8Bit = true;
            auto result1 = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                auto val = minSignal + static_cast<int>(*(result1 + i))*10;
                result.append(val);
            }
        }else {
            auto result1 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {                
                result.append(*(result1 + i));
            }
        }
        htGroup.close();
        file.close();
        return result;
    }

    auto HTChannelPanel::Impl::ReadOriginalDataLdm() -> QList<int> {
        auto result = QList<int>();        
        H5File file(tcfPath.toStdString(), H5F_ACC_RDONLY);
        auto htGroup = file.openGroup("/Data/2DMIP/000000");
        TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
        tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(htGroup);
        const TC::IO::DataRange roi(TC::IO::Point(0, 0), TC::IO::Count(metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY));
        tcfH5GroupRoiLdmDataReader.SetReadingRoi(roi);
        auto base = 128;
        auto samplingstep = 0;
        const auto sizeX = metaInfo->data.data2DMIP.sizeX;
        while(sizeX > base) {            
            base *= 2;
            samplingstep++;
        }
        samplingstep-= 2;
        if(samplingstep < 0) {
            samplingstep = 0;
        }
    	samplingstep = static_cast<int>(pow(2, samplingstep));                
        tcfH5GroupRoiLdmDataReader.SetSamplingStep(samplingstep);        

        const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();        
        if(metaInfo->data.data2DMIP.scalarType[0] == 1) {
            is8Bit = true;
            auto result1 = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                int val = minSignal + static_cast<int>(*(result1 + i)) * 10;
                result.append(val);
            }
        }else {
            auto result1 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                result.append(*(result1 + i));
            }
        }    
        htGroup.close();
        file.close();
        return result;
    }

    auto HTChannelPanel::Impl::ReadOriginalData() -> QList<int> {
        //TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
        auto result = QList<int>();
        H5File file(tcfPath.toStdString(), H5F_ACC_RDONLY);
        auto htGroup = file.openGroup("Data/2DMIP");
        auto dset = htGroup.openDataSet("000000");
        auto dataspace = dset.getSpace();
        hsize_t dims[2];
        dataspace.getSimpleExtentDims(dims);
        unsigned long long numberOfElements;

        const auto dataSizeX = dims[1];
        const auto dataSizeY = dims[0];
        numberOfElements = dataSizeX * dataSizeY;

        const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
        dset.read(rawData.get(), dset.getDataType());
        dataspace.close();
        dset.close();
        htGroup.close();
        file.close();
               
        for (auto i = 0; i < numberOfElements; i++) {
            auto value = rawData.get()[i];            
            result.append(value);
        }
        return result;
    }
    auto HTChannelPanel::Impl::ReadOriginalData3D() -> QList<int> {
        //TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
        auto result = QList<int>();
        H5File file(tcfPath.toStdString(), H5F_ACC_RDONLY);
        auto htGroup = file.openGroup("Data/3D");
        auto dset = htGroup.openDataSet("000000");
        auto dataspace = dset.getSpace();
        hsize_t dims[3];
        dataspace.getSimpleExtentDims(dims);
        hsize_t tcfOffset[3] = { (hsize_t)(dims[0] / 2), (hsize_t)(0), (hsize_t)0 };
        hsize_t tcfCount[3] = { (hsize_t)1, dims[1], dims[2] };

        dataspace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

        int out_rank = 2;
        const hsize_t out_dim[2] = { (hsize_t)(dims[1]), (hsize_t)(dims[2]) };

        H5::DataSpace memoryspace(out_rank, out_dim);

        hsize_t out_offset[2] = { 0, 0 };
        hsize_t out_count[2] = { (hsize_t)(dims[1]), (hsize_t)(dims[2]) };
        memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

        unsigned long long numberOfElements = dims[1] * dims[2];

        const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };

        dset.read(rawData.get(), H5::PredType::NATIVE_UINT16, memoryspace, dataspace);

        memoryspace.close();
        dataspace.close();
        dset.close();
        htGroup.close();
        file.close();
                
        for (auto i = 0; i < numberOfElements; i++) {
            auto value = rawData.get()[i];                        
            result.append(value);
        }

        return result;
    }
    auto HTChannelPanel::Impl::CalculateHistogram(QList<int> original_data,int div) -> QList<int> {
        auto result = QList<int>();
        for (auto i = 0; i < histogram_steps /div; i++) {
            result.append(0);
        }

        for (auto i = 0; i < original_data.count(); i++) {
            auto value = (original_data[i] - minSignal) / div;
            if (result.count() > value && value > -1) {
                result[value]++;
            }
        }

        for (auto i = 0; i < histogram_steps / div; i++) {
            if (result[i] > 0) {
                result[i] = static_cast<int>(log(result[i] + 1.5) / log(1.5));
            }
        }

        return result;
    }   
    HTChannelPanel::HTChannelPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        d->colormap = new SoTransferFunction;
        d->colormap->ref();
        d->colormap->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;        
        this->InitUI();
        this->InitConnections();
    }
    HTChannelPanel::~HTChannelPanel() {
        d->colormap->unref();
        d->colormap = nullptr;
    }

    auto HTChannelPanel::InitUI() -> void {
        d->ui.colormapCombo->addItem("Grayscale");
        d->ui.colormapCombo->addItem("Inverse Grayscale");
        d->ui.colormapCombo->addItem("Hot iron");
        d->ui.colormapCombo->addItem("JET");
        d->ui.colormapCombo->addItem("Rainbow");
        d->ui.colormapCombo->setCurrentIndex(0);

        d->ui.deviceColor->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.timeColor->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.loadVizBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.saveVizBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.loadAnnoBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.saveAnnoBtn->setStyleSheet("font-size: 11px; font-weight: bold;");

        d->devicePixmap = QPixmap(100, 100);
        d->devicePixmap.fill(QColor(d->device_color[0], d->device_color[1], d->device_color[2], 255));
        QIcon deviceIcon(d->devicePixmap);
        d->ui.deviceColor->setIcon(deviceIcon);

        d->ui.deviceFontSize->setRange(10, 40);
        d->ui.deviceFontSize->setValue(20);
        d->ui.deviceColor->hide();
        d->ui.deviceSizeLabel->hide();
        d->ui.deviceFontSize->hide();

        d->timePixmap = QPixmap(100, 100);
        d->timePixmap.fill(QColor(d->time_color[0], d->time_color[1], d->time_color[2], 255));
        QIcon timeIcon(d->timePixmap);
        d->ui.timeColor->setIcon(timeIcon);

        d->ui.timeFontSize->setRange(10, 40);
        d->ui.timeFontSize->setValue(20);
        d->ui.timeColor->hide();
        d->ui.timeSizeLabel->hide();
        d->ui.timeFontSize->hide();

        d->ui.resolutionLabel->hide();
        d->ui.resolutionSpin->setRange(0, 2);
        d->ui.resolutionSpin->hide();

        d->ui.resolutionChk->hide();        
        d->ui.resolutionChk->setChecked(true);

        d->ui.htHistogram->SetFixedColorMap(true);
        d->ui.htHistogram->SetFixedHistogram(true);
        d->ui.htHistogram->SetReadOnlyHistogram(true);
        d->ui.htHistogram->SetDecimals(3);
        d->ui.htHistogram->SetSingleStep(0.001);
        d->ui.scaleChk->setChecked(true);

        d->ui.minimapChk->setChecked(false);
    }


    auto HTChannelPanel::InitConnections() -> void {
        connect(d->ui.timeChk, SIGNAL(clicked()), this, SLOT(OnTimeToggled()));
        connect(d->ui.resolutionChk, SIGNAL(clicked()), this, SLOT(OnResolutionToggled()));
        connect(d->ui.deviceChk, SIGNAL(clicked()), this, SLOT(OnDeviceToggled()));
        connect(d->ui.scalarChk, SIGNAL(clicked()), this, SLOT(OnScalarToggled()));
        connect(d->ui.scaleChk, SIGNAL(clicked()), this, SLOT(OnScaleToggled()));
        connect(d->ui.minimapChk, SIGNAL(clicked()), this, SLOT(OnMinimapToggled()));

        connect(d->ui.deviceColor, SIGNAL(clicked()), this, SLOT(OnDeviceColor()));
        connect(d->ui.timeColor, SIGNAL(clicked()), this, SLOT(OnTimeStampColor()));

        connect(d->ui.deviceFontSize, SIGNAL(valueChanged(int)), this, SLOT(OnDeviceFontSize(int)));
        connect(d->ui.timeFontSize, SIGNAL(valueChanged(int)), this, SLOT(OnTimeFontSize(int)));
        connect(d->ui.resolutionSpin, SIGNAL(valueChanged(int)), this, SLOT(OnFixedResolution(int)));
        connect(d->ui.colormapCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColormapIdx(int)));
        connect(d->ui.htHistogram, SIGNAL(sigApplyGamma(bool)), this, SLOT(OnToggleGamma(bool)));
        connect(d->ui.htHistogram, SIGNAL(sigGammaValue(double)), this, SLOT(OnGammaValue(double)));

        connect(d->ui.loadVizBtn, SIGNAL(clicked()), this, SLOT(OnVizMetaInfoLoad()));
        connect(d->ui.saveVizBtn, SIGNAL(clicked()), this, SLOT(OnVizMetaInfoSave()));
        connect(d->ui.loadAnnoBtn, SIGNAL(clicked()), this, SLOT(OnAnnoMetaInfoLoad()));
        connect(d->ui.saveAnnoBtn, SIGNAL(clicked()), this, SLOT(OnAnnoMetaInfoSave()));
    }

    auto HTChannelPanel::Reset() -> void {
        this->blockSignals(true);

        d->ui.timeChk->setChecked(false);

        d->ui.scaleChk->setChecked(true);

        d->ui.scalarChk->setChecked(false);

        d->ui.deviceChk->setChecked(false);

        d->colormap->predefColorMap = SoTransferFunction::INTENSITY;

        d->ui.colormapCombo->setCurrentIndex(0);

        d->ui.timeFontSize->setValue(20);
        d->timePixmap.fill(Qt::white);
        d->ui.timeColor->setIcon(QIcon(d->timePixmap));
        d->ui.timeColor->hide();
        d->ui.timeSizeLabel->hide();
        d->ui.timeFontSize->hide();

        d->ui.deviceFontSize->setValue(20);
        d->devicePixmap.fill(Qt::white);
        d->ui.deviceColor->setIcon(QIcon(d->devicePixmap));
        d->ui.deviceColor->hide();
        d->ui.deviceSizeLabel->hide();
        d->ui.deviceFontSize->hide();

        d->ui.resolutionChk->setChecked(true);
        d->ui.resolutionLabel->hide();
        d->ui.resolutionSpin->hide();
        d->ui.resolutionSpin->setRange(0, 2);
        d->ui.resolutionSpin->setValue(0);

        d->ui.htGroup->show();
        d->ui.scalarChk->show();
        d->ui.htHistogram->Clear();
        this->blockSignals(false);

        d->colormap->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
        d->colormapIdx = 0;
        d->isGamma = false;
        d->gammaValue = 1.0;
    }

    auto HTChannelPanel::SetTcfPath(const QString& path) -> void {
        d->tcfPath = path;
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        d->metaInfo = metaReader->Read(path);
        auto isLdm = d->metaInfo->data.isLDM;

        d->htExist = -1;
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htExist = 0;
        }
        else if (d->metaInfo->data.data3D.exist) {
            d->htExist = 1;
        }
        for (auto i = 0; i < 3; i++) {
            d->flExist[i] = d->metaInfo->data.data3DFL.valid[i] || d->metaInfo->data.data2DFLMIP.valid[i];
        }
        if (d->htExist < 0) {
            d->ui.htGroup->hide();
            d->ui.scalarChk->hide();
            return;
        }
        QList<int> original_data;
        TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());        
        try {            
            if (d->htExist == 0) {
                d->minSignal = static_cast<int>(round(d->metaInfo->data.data2DMIP.riMin * 10000.0));
                d->maxSignal = static_cast<int>(round(d->metaInfo->data.data2DMIP.riMax * 10000.0));
                if (isLdm) {
                    original_data = d->ReadOriginalDataLdm();
                }
                else {
                    original_data = d->ReadOriginalData();
                }
            }
            else {
                d->minSignal = static_cast<int>(round(d->metaInfo->data.data3D.riMin * 10000.0));
                d->maxSignal = static_cast<int>(round(d->metaInfo->data.data3D.riMax * 10000.0));
                if (isLdm) {
                    original_data = d->ReadOriginalData3DLdm();
                }
                else {
                    original_data = d->ReadOriginalData3D();
                }
            }
        }
        catch (Exception& e) {
            std::cout << e.getDetailMsg() << std::endl;
            return;
        }
        d->histogram_steps = d->maxSignal - d->minSignal + 1;
        auto div = 1;
        if (d->is8Bit) div = 10;        
        auto histogram = d->CalculateHistogram(original_data,div);
        d->ui.htHistogram->SetColorMap(d->colormap->actualColorMap.getValues(0), d->colormap->actualColorMap.getNum() / 4);
        d->ui.htHistogram->SetDataRange(d->minSignal, d->maxSignal, 10000);
        d->ui.htHistogram->SetHistogram(QColor(Qt::lightGray), histogram);
        d->ui.htHistogram->SetWindowRange(d->minSignal + d->histogram_steps / 7.0, d->maxSignal - d->histogram_steps / 7.0);
    }

    auto HTChannelPanel::SetMinResolution(int level) -> void {
        d->ui.resolutionSpin->blockSignals(true);
        d->ui.resolutionSpin->setRange(0, level);
        d->ui.resolutionSpin->blockSignals(false);
    }

    bool HTChannelPanel::eventFilter(QObject* watched, QEvent* event) {
        return QObject::eventFilter(watched, event);
    }


    //slots
    void HTChannelPanel::OnVizMetaInfoSave() {
        //retrieve modality existence
        bool infoExist[5]{ d->htExist > -1,d->flExist[0],d->flExist[1],d->flExist[2],true };
        auto selectionSet = SelectVisualizationMeta::Select(nullptr, infoExist, true);
        VizMetaFlag flag;
        flag.useHT = std::get<0>(selectionSet);;
        flag.useFL[0] = std::get<1>(selectionSet);
        flag.useFL[1] = std::get<2>(selectionSet);
        flag.useFL[2] = std::get<3>(selectionSet);
        flag.useCamera = std::get<4>(selectionSet);

        if (false == flag.isValid()) {
            return;
        }
        emit sigSaveVizMeta(flag);
    }
    auto HTChannelPanel::ApplyHTMeta(const AppEntity::ColormapInfo& meta) -> void {
        if (d->htExist < 0) {
            return;
        }
        d->ui.htHistogram->SetWindowRange(std::get<0>(meta.GetXRange()), std::get<1>(meta.GetXRange()));
        d->ui.htHistogram->SetGammaEnabled(meta.isGamma());
        d->ui.htHistogram->SetGammaValue(meta.GetGamma());
        d->ChangeColormap(meta.GetPredefinedColormap(), meta.isGamma(), meta.GetGamma());
        d->ui.htHistogram->SetColorMap(d->colormap->actualColorMap.getValues(0), d->colormap->actualColorMap.getNum() / 4);
    }
    auto HTChannelPanel::ForceLoadVizMeta(const QString& path) -> void {
        AppComponents::IO::ViewerMetaReader reader;
        auto visualizatoinInformation = reader.ReadVizInfo(path);
        if (nullptr == visualizatoinInformation) {
            return;
        }
        bool infoExist[5];
        infoExist[0] = visualizatoinInformation->GetIsRiInfoExist() && (d->htExist > -1);
        for (auto i = 1; i < 4; i++) {
            infoExist[i] = visualizatoinInformation->GetIsFLInfoExist(i - 1) && d->flExist[i - 1];
        }
        infoExist[4] = visualizatoinInformation->hasCamera2dMetaInfo();
        VizMetaFlag flag;
        flag.useHT = infoExist[0];
        flag.useFL[0] = infoExist[1];
        flag.useFL[1] = infoExist[2];
        flag.useFL[2] = infoExist[3];
        flag.useCamera = infoExist[4];
        if (false == flag.isValid()) {
            return;
        }
        emit sigLoadVizMeta(path, flag); // apply loaded meta information to viewer
        //apply loaded meta information to UI
        if (flag.useHT) {
            this->ApplyHTMeta(visualizatoinInformation->GetHTInfo());
        }
    }
    void HTChannelPanel::OnVizMetaInfoLoad() {
        const auto path = QFileDialog::getOpenFileName(this, "Select a file to open visualization meta information", QString(),
            "Visualization meta files (*.tcviz)");
        if (path.isEmpty()) {
            return;
        }
        AppComponents::IO::ViewerMetaReader reader;
        auto visualizatoinInformation = reader.ReadVizInfo(path);
        if (nullptr == visualizatoinInformation) {
            return;
        }
        bool infoExist[5];
        infoExist[0] = visualizatoinInformation->GetIsRiInfoExist() && (d->htExist > -1);
        for (auto i = 1; i < 4; i++) {
            infoExist[i] = visualizatoinInformation->GetIsFLInfoExist(i - 1) && d->flExist[i - 1];
        }
        infoExist[4] = visualizatoinInformation->hasCamera2dMetaInfo();
        auto selectionSet = SelectVisualizationMeta::Select(nullptr, infoExist, false);

        VizMetaFlag flag;
        flag.useHT = std::get<0>(selectionSet);;
        flag.useFL[0] = std::get<1>(selectionSet);
        flag.useFL[1] = std::get<2>(selectionSet);
        flag.useFL[2] = std::get<3>(selectionSet);
        flag.useCamera = std::get<4>(selectionSet);

        if (false == flag.isValid()) {
            return;
        }
        emit sigLoadVizMeta(path, flag); // apply loaded meta information to viewer
        //apply loaded meta information to UI
        if (flag.useHT) {
            this->ApplyHTMeta(visualizatoinInformation->GetHTInfo());
        }                
    }

    void HTChannelPanel::OnAnnoMetaInfoSave() {
        bool infoExist[4]{ true,true,true,true };
        auto selectionSet = SelectAnnotationMeta::Select(nullptr, infoExist, true);
        AnnoMetaFlag flag;
        flag.useScalar = std::get<0>(selectionSet);
        flag.useScale = std::get<1>(selectionSet);
        flag.useTimestamp = std::get<2>(selectionSet);
        flag.useDeviceInfo = std::get<3>(selectionSet);
        if (false == flag.isValid()) {
            return;
        }
        emit sigSaveAnnoMeta(flag);
    }
    auto HTChannelPanel::ForceLoadAnnoMeta(const QString& path) -> void {
        AppComponents::IO::ViewerMetaReader reader;
        auto annotationInformation = reader.ReadAnnoInfo(path);
        if (nullptr == annotationInformation) {
            return;
        }
        AnnoMetaFlag flag;
        flag.useScalar = annotationInformation->hasScalarMetaInfo();
        flag.useScale = annotationInformation->hasScaleMetaInfo();
        flag.useTimestamp = annotationInformation->hasTimeMetaInfo();
        flag.useDeviceInfo = annotationInformation->hasDeviceMetaInfo();
        if (false == flag.isValid()) {
            return;
        }
        emit sigLoadAnnoMeta(path, flag);//apply loaded meta information to viewer
        //apply loaded meta information to UI
        if (flag.useScalar) {
            this->ApplyScalarMeta(annotationInformation->GetScalarMetaInfo());
        }
        if (flag.useScale) {
            this->ApplyScaleMeta(annotationInformation->GetScaleMetaInfo());
        }
        if (flag.useTimestamp) {
            this->ApplyTimeMeta(annotationInformation->GetTimeMetaInfo());
        }
        if (flag.useDeviceInfo) {
            this->ApplyDeviceMeta(annotationInformation->GetDeviceMetaInfo());
        }
    }
    void HTChannelPanel::OnAnnoMetaInfoLoad() {
        const auto path = QFileDialog::getOpenFileName(this, "Select a file to open annotation meta information", QString(),
            "Annotation meta files (*.tcanno)");
        if (path.isEmpty()) {
            return;
        }
        AppComponents::IO::ViewerMetaReader reader;
        auto annotationInformation = reader.ReadAnnoInfo(path);
        if (nullptr == annotationInformation) {
            return;
        }
        bool infoExist[4];
        infoExist[0] = annotationInformation->hasScalarMetaInfo();
        infoExist[1] = annotationInformation->hasScaleMetaInfo();
        infoExist[2] = annotationInformation->hasTimeMetaInfo();
        infoExist[3] = annotationInformation->hasDeviceMetaInfo();
        auto selectionSet = SelectAnnotationMeta::Select(nullptr, infoExist, false);
        AnnoMetaFlag flag;
        flag.useScalar = std::get<0>(selectionSet);
        flag.useScale = std::get<1>(selectionSet);
        flag.useTimestamp = std::get<2>(selectionSet);
        flag.useDeviceInfo = std::get<3>(selectionSet);
        if (false == flag.isValid()) {
            return;
        }
        emit sigLoadAnnoMeta(path, flag);//apply loaded meta information to viewer
        //apply loaded meta information to UI
        if (flag.useScalar) {
            this->ApplyScalarMeta(annotationInformation->GetScalarMetaInfo());
        }
        if (flag.useScale) {
            this->ApplyScaleMeta(annotationInformation->GetScaleMetaInfo());
        }
        if (flag.useTimestamp) {
            this->ApplyTimeMeta(annotationInformation->GetTimeMetaInfo());
        }
        if (flag.useDeviceInfo) {
            this->ApplyDeviceMeta(annotationInformation->GetDeviceMetaInfo());
        }
    }
    auto HTChannelPanel::ApplyScalarMeta(const AppEntity::ScalarBarInfo& meta) -> void {
        d->ui.scalarChk->blockSignals(true);
        d->ui.scalarChk->setChecked(meta.isVisible());
        d->ui.scalarChk->blockSignals(false);
    }
    auto HTChannelPanel::ApplyScaleMeta(const AppEntity::ScaleBarInfo& meta) -> void {
        d->ui.scaleChk->blockSignals(true);
        d->ui.scaleChk->setChecked(meta.isVisible());
        d->ui.scaleChk->blockSignals(false);
    }
    auto HTChannelPanel::ApplyTimeMeta(const AppEntity::TimeStampInfo& meta) -> void {
        d->ui.timeChk->blockSignals(true);
        d->ui.timeChk->setChecked(meta.isVisible());
        d->ui.timeChk->blockSignals(false);

        d->ui.timeColor->setVisible(meta.isVisible());
        d->ui.timeFontSize->setVisible(meta.isVisible());
        d->ui.timeSizeLabel->setVisible(meta.isVisible());

        d->timePixmap.fill(QColor(255.0 * std::get<0>(meta.GetColor()), 255.0 * std::get<1>(meta.GetColor()), 255.0 * std::get<2>(meta.GetColor())));
        d->ui.timeColor->setIcon(QIcon(d->timePixmap));

        d->ui.timeFontSize->blockSignals(true);
        d->ui.timeFontSize->setValue(meta.GetFontSize());
        d->ui.timeFontSize->blockSignals(false);
    }
    auto HTChannelPanel::ApplyDeviceMeta(const AppEntity::DeviceInfo& meta) -> void {
        d->ui.deviceChk->blockSignals(true);
        d->ui.deviceChk->setChecked(meta.isVisible());
        d->ui.deviceChk->blockSignals(false);

        d->ui.deviceColor->setVisible(meta.isVisible());
        d->ui.deviceFontSize->setVisible(meta.isVisible());
        d->ui.deviceSizeLabel->setVisible(meta.isVisible());

        d->devicePixmap.fill(QColor(255.0 * std::get<0>(meta.GetColor()), 255.0 * std::get<1>(meta.GetColor()), 255.0 * std::get<2>(meta.GetColor())));
        d->ui.deviceColor->setIcon(QIcon(d->devicePixmap));

        d->ui.deviceFontSize->blockSignals(true);
        d->ui.deviceFontSize->setValue(meta.GetFontSize());
        d->ui.deviceFontSize->blockSignals(false);
    }
    void HTChannelPanel::OnWindowRange(double min, double max) {
        if (d->ui.htGroup->isHidden()) {
            return;
        }
        d->ui.htHistogram->SetWindowRange(min, max);
    }
    void HTChannelPanel::OnColormapIdx(int idx) {
        auto gamma = d->ui.htHistogram->GetGammaValue();
        auto isGamma = d->ui.htHistogram->GetGammaEnabled();
        emit sigColormapChanged(idx, isGamma, gamma);
        d->ChangeColormap(idx, isGamma, gamma);
        d->ui.htHistogram->SetColorMap(d->colormap->actualColorMap.getValues(0), d->colormap->actualColorMap.getNum() / 4);
    }
    void HTChannelPanel::OnGammaValue(double value) {
        auto isGamma = d->ui.htHistogram->GetGammaEnabled();
        auto colormapIdx = d->ui.colormapCombo->currentIndex();
        emit sigColormapChanged(colormapIdx, isGamma, value);
        d->ChangeColormap(colormapIdx, isGamma, value);
        d->ui.htHistogram->SetColorMap(d->colormap->actualColorMap.getValues(0), d->colormap->actualColorMap.getNum() / 4);
    }
    void HTChannelPanel::OnToggleGamma(bool isGamma) {
        auto gamma = d->ui.htHistogram->GetGammaValue();
        auto colormapIdx = d->ui.colormapCombo->currentIndex();
        emit sigColormapChanged(colormapIdx, isGamma, gamma);
        d->ChangeColormap(colormapIdx, isGamma, gamma);
        d->ui.htHistogram->SetColorMap(d->colormap->actualColorMap.getValues(0), d->colormap->actualColorMap.getNum() / 4);
    }

    void HTChannelPanel::OnFixedResolution(int level) {
        emit sigResolution(level);
    }

    void HTChannelPanel::OnTimeFontSize(int size) {
        emit sigTimeStampSize(size);
    }

    void HTChannelPanel::OnDeviceFontSize(int size) {
        emit sigDeviceSize(size);
    }

    void HTChannelPanel::OnTimeToggled() {
        auto showTime = d->ui.timeChk->isChecked();
        d->ui.timeSizeLabel->setVisible(showTime);
        d->ui.timeFontSize->setVisible(showTime);
        d->ui.timeColor->setVisible(showTime);
        emit sigTimeStampToggle(showTime);
    }
    void HTChannelPanel::OnResolutionToggled() {
        return;
        auto fixedResolution = !d->ui.resolutionChk->isChecked();
        d->ui.resolutionLabel->setVisible(fixedResolution);
        d->ui.resolutionSpin->setVisible(fixedResolution);
        if (!fixedResolution) {
            emit sigResolution(-1);
        }
        else {            
            emit sigResolution(d->ui.resolutionSpin->value());
        }
    }
    void HTChannelPanel::OnDeviceToggled() {
        auto showDevice = d->ui.deviceChk->isChecked();
        d->ui.deviceSizeLabel->setVisible(showDevice);
        d->ui.deviceColor->setVisible(showDevice);
        d->ui.deviceFontSize->setVisible(showDevice);
        emit sigDeviceToggle(showDevice);
    }
    void HTChannelPanel::OnScalarToggled() {
        emit sigScalarBarToggle(d->ui.scalarChk->isChecked());
    }
    void HTChannelPanel::OnScaleToggled() {
        emit sigScaleBarToggle(d->ui.scaleChk->isChecked());
    }
    void HTChannelPanel::OnMinimapToggled() {
        emit sigMinimapToggle(d->ui.minimapChk->isChecked());
    }
    void HTChannelPanel::OnTimeStampColor() {
        QColor new_col = QColorDialog::getColor(QColor(d->time_color[0], d->time_color[1], d->time_color[2], 255), nullptr, "Select Color");
        if (new_col.isValid()) {
            emit sigTimeStampColor(new_col);
            d->timePixmap.fill(new_col);
            d->ui.timeColor->setIcon(QIcon(d->timePixmap));
        }
    }
    void HTChannelPanel::OnDeviceColor() {
        QColor new_col = QColorDialog::getColor(QColor(d->device_color[0], d->device_color[1], d->device_color[2], 255), nullptr, "Select Color");
        if (new_col.isValid()) {
            emit sigDeviceColor(new_col);
            d->device_color[0] = new_col.red();
            d->device_color[1] = new_col.green();
            d->device_color[2] = new_col.blue();

            d->devicePixmap.fill(new_col);
            d->ui.deviceColor->setIcon(QIcon(d->devicePixmap));
        }
    }
}