#pragma once

#include <memory>
#include <QWidget>
#include <QSlider>
#include <QWheelEvent>
#include <QProxyStyle>
#include <INavigatorPanel.h>

#include "TomoAnalysis2dNavigatorPanelExport.h"

class TcCustomSlider : public QSlider {
public:
	TcCustomSlider(QWidget* parent = nullptr) : QSlider(parent) {}

protected:
	void wheelEvent(QWheelEvent* event) override {
		int num_degrees = event->delta() / 8;
		int num_steps = num_degrees / 15;  // Each step corresponds to 15 degrees


		setValue(value() + num_steps * singleStep());

		event->accept();
	}
};

class MyProxyStyle : public QProxyStyle {
public:
	int styleHint(StyleHint hint, const QStyleOption* option, const QWidget* widget, QStyleHintReturn* returnData) const override {
		if (hint == QStyle::SH_SpinBox_ClickAutoRepeatRate) {
			return 10000;
		}
		if (hint == SH_SpinBox_ClickAutoRepeatRate) {
			return 10000;
		}
		if (hint == SH_SpinBox_ClickAutoRepeatThreshold) {
			return 10000;
		}
		return QProxyStyle::styleHint(hint, option, widget, returnData);
	}
};


namespace TomoAnalysis::Viewer2D::Plugins {
	class TomoAnalysis2dNavigatorPanel_API NavigatorPanel : public QWidget, public Interactor::INavigatorPanel {
		Q_OBJECT

	public:
		typedef NavigatorPanel Self;
		typedef std::shared_ptr<Self> Pointer;

		NavigatorPanel(QWidget* parent = nullptr);
		~NavigatorPanel();

		auto UpdateCall() -> bool override;
		auto Update() -> bool override;
		auto Refresh() -> bool override;

		auto Init(void) const -> bool;
		auto Reset(void) const -> bool;

	signals:
		void callUpdate();
		void positionChanged(int x, int y, int z);
		void phyPositionChanged(float x, float y, float z);

	protected slots:
		void onUpdate();

		void OnPhyChanged(QString value);
		void OnSpinChanged(QString value);
		void OnSliderChanged(int value);

		void onCurrentModalityChanged(Entity::Modality modality);

		void onPositionChanged(int axis, int value);
		void OnResolutionChanged(int res);

	private:
		void NotifyChangedAxisSpinBox();
		void NotifyChangedAxisSlider();
		void NotifyChangedPhySpinBox();

		auto SetEnableUI(const bool& enable) const -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
