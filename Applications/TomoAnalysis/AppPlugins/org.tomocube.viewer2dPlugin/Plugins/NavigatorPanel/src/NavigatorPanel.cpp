#include <iostream>

#include <UIUtility.h>

#include "ui_NavigatorPanel.h"
#include "NavigatorPanel.h"

#include <QThread>

namespace TomoAnalysis::Viewer2D::Plugins {
	struct NavigatorPanel::Impl {
		Ui::NavigatorPanel* ui { nullptr };

		Interactor::NavigatorDS::Pointer navigatorDS;

		bool initialized { false };

		int curRes { 0 };

		Entity::Modality prevModality = Entity::Modality::None;
	};

	NavigatorPanel::NavigatorPanel(QWidget* parent)
		: QWidget(parent), d { new Impl } {
		d->ui = new Ui::NavigatorPanel();
		d->ui->setupUi(this);

		// set object names
		d->ui->contentsWidget->setObjectName("panel-contents");
		d->ui->locationLabel->setObjectName("h8");
		d->ui->locationUnitlabel->setObjectName("label-unit");
		d->ui->xPhyLabel->setObjectName("h9");
		d->ui->yPhyLabel->setObjectName("h9");
		d->ui->zPhyLabel->setObjectName("h9");
		d->ui->zAxisLabel->setObjectName("h9");
		d->ui->xAxisLabel->setObjectName("h9");
		d->ui->yAxisLabel->setObjectName("h9");

		d->ui->xAxisSpinBox->hide();
		d->ui->xAxisSlider->hide();
		d->ui->xPhySpinBox->hide();
		d->ui->xAxisLabel->hide();
		d->ui->xPhyLabel->hide();

		d->ui->yAxisSpinBox->hide();
		d->ui->yAxisSlider->hide();
		d->ui->yPhySpinBox->hide();
		d->ui->yAxisLabel->hide();
		d->ui->yPhyLabel->hide();

		d->ui->zAxisSpinBox->setStyle(new MyProxyStyle);
		d->ui->zPhySpinBox->setStyle(new MyProxyStyle);

		connect(d->ui->zAxisSpinBox, SIGNAL(valueChanged(QString)), this, SLOT(OnSpinChanged(QString)));
		connect(d->ui->zAxisSlider, SIGNAL(valueChanged(int)), this, SLOT(OnSliderChanged(int)));
		connect(d->ui->zPhySpinBox, SIGNAL(valueChanged(QString)), this, SLOT(OnPhyChanged(QString)));
	}

	NavigatorPanel::~NavigatorPanel() {
		delete d->ui;
	}

	void NavigatorPanel::OnResolutionChanged(int res) {
		d->curRes = res;

		//TC::SilentCall(d->ui->xAxisSpinBox)->setSingleStep(pow(2, d->curRes));
		//TC::SilentCall(d->ui->yAxisSpinBox)->setSingleStep(pow(2, d->curRes));
		//TC::SilentCall(d->ui->zAxisSpinBox)->setSingleStep(pow(2, d->curRes));
	}

	auto NavigatorPanel::Update() -> bool {
		auto ds = GetNavigatorDS();

		d->navigatorDS = ds;

		return true;
	}

	auto NavigatorPanel::Refresh() -> bool {
		auto ds = GetNavigatorDS();
		d->navigatorDS = ds;

		TC::SilentCall(d->ui->xAxisSlider)->setValue(ds->x);
		TC::SilentCall(d->ui->yAxisSlider)->setValue(ds->y);
		TC::SilentCall(d->ui->zAxisSlider)->setValue(ds->z);
		TC::SilentCall(d->ui->xAxisSpinBox)->setValue(ds->x);
		TC::SilentCall(d->ui->yAxisSpinBox)->setValue(ds->y);
		TC::SilentCall(d->ui->zAxisSpinBox)->setValue(ds->z);
		TC::SilentCall(d->ui->xPhySpinBox)->setValue(ds->phyX);
		TC::SilentCall(d->ui->yPhySpinBox)->setValue(ds->phyY);
		TC::SilentCall(d->ui->zPhySpinBox)->setValue(ds->phyZ);

		return true;
	}

	auto NavigatorPanel::Init(void) const -> bool {
		this->SetEnableUI(false);

		connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdate()));

		return true;
	}

	auto NavigatorPanel::UpdateCall() -> bool {
		emit callUpdate();
		return true;
	}

	void NavigatorPanel::onUpdate() {
		this->Update();
	}

	auto NavigatorPanel::Reset() const -> bool {
		d->initialized = false;
		return true;
	}

	void NavigatorPanel::OnSpinChanged(QString /*value*/) {
		this->NotifyChangedAxisSpinBox();
	}

	void NavigatorPanel::OnSliderChanged(int /*value*/) {
		this->NotifyChangedAxisSlider();
	}

	void NavigatorPanel::OnPhyChanged(QString /*value*/) {
		this->NotifyChangedPhySpinBox();
	}

	void NavigatorPanel::onCurrentModalityChanged(Entity::Modality modality) {
		if ((d->prevModality & Entity::Modality::None) != Entity::Modality::None) {
			return;
		}

		auto prevFLOnly = true;
		if (((d->prevModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(d->prevModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
			// volume
			if ((d->prevModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
				prevFLOnly = false;
			}
		}
		auto is2d = false;
		auto isSwitched = false;
		Interactor::NavigatorInfo::Pointer info;
		if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
			// volume
			if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
				info = d->navigatorDS->list[Entity::HT3D];
				if (prevFLOnly) {
					isSwitched = true;
				}
			} else {
				info = d->navigatorDS->list[Entity::FL3D];
				if (false == prevFLOnly) {
					isSwitched = true;
				}
			}
		} else if (((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
			(modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
			// mip
			is2d = true;
			if ((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				info = d->navigatorDS->list[Entity::HT2DMIP];
			} else {
				info = d->navigatorDS->list[Entity::FL2DMIP];
			}
		}

		if (nullptr == info.get()) {
			return;
		}

		d->prevModality = modality;
		auto ht3dInfo = d->navigatorDS->list[Entity::HT3D];
		auto htExist = nullptr != ht3dInfo.get();

		this->SetEnableUI(true);

		if (htExist && false == is2d) {
			TC::SilentCall(d->ui->xAxisSlider)->setRange(1, ht3dInfo->width);
			TC::SilentCall(d->ui->xAxisSpinBox)->setRange(1, ht3dInfo->width);
			TC::SilentCall(d->ui->yAxisSlider)->setRange(1, ht3dInfo->height);
			TC::SilentCall(d->ui->yAxisSpinBox)->setRange(1, ht3dInfo->height);
			TC::SilentCall(d->ui->zAxisSlider)->setRange(1, ht3dInfo->depth);
			TC::SilentCall(d->ui->zAxisSpinBox)->setRange(1, ht3dInfo->depth);

			TC::SilentCall(d->ui->xPhySpinBox)->setRange(ht3dInfo->resolutionX, (float)ht3dInfo->width * ht3dInfo->resolutionX);
			TC::SilentCall(d->ui->yPhySpinBox)->setRange(ht3dInfo->resolutionY, (float)ht3dInfo->height * ht3dInfo->resolutionY);
			TC::SilentCall(d->ui->zPhySpinBox)->setRange(ht3dInfo->resolutionZ, (float)ht3dInfo->depth * ht3dInfo->resolutionZ);

			TC::SilentCall(d->ui->xPhySpinBox)->setSingleStep(ht3dInfo->resolutionX);
			TC::SilentCall(d->ui->yPhySpinBox)->setSingleStep(ht3dInfo->resolutionY);
			TC::SilentCall(d->ui->zPhySpinBox)->setSingleStep(ht3dInfo->resolutionZ);

			if (false == d->initialized) {
				TC::SilentCall(d->ui->xAxisSlider)->setValue(ht3dInfo->width / 2);
				TC::SilentCall(d->ui->xAxisSpinBox)->setValue(ht3dInfo->width / 2);

				TC::SilentCall(d->ui->yAxisSlider)->setValue(ht3dInfo->height / 2);
				TC::SilentCall(d->ui->yAxisSpinBox)->setValue(ht3dInfo->height / 2);

				TC::SilentCall(d->ui->zAxisSlider)->setValue(ht3dInfo->depth / 2);
				TC::SilentCall(d->ui->zAxisSpinBox)->setValue(ht3dInfo->depth / 2);

				TC::SilentCall(d->ui->xPhySpinBox)->setValue((float)ht3dInfo->width * ht3dInfo->resolutionX / 2.f);
				TC::SilentCall(d->ui->yPhySpinBox)->setValue((float)ht3dInfo->height * ht3dInfo->resolutionY / 2.f);
				TC::SilentCall(d->ui->zPhySpinBox)->setValue((float)ht3dInfo->depth * ht3dInfo->resolutionZ / 2.f);
				d->initialized = true;
				this->NotifyChangedAxisSpinBox();
				return;
			}

			auto phyX = d->ui->xPhySpinBox->value();
			auto phyY = d->ui->yPhySpinBox->value();
			auto phyZ = d->ui->zPhySpinBox->value();

			auto idxX = d->ui->xAxisSpinBox->value();
			auto idxY = d->ui->yAxisSpinBox->value();
			auto idxZ = d->ui->zAxisSpinBox->value();


			TC::SilentCall(d->ui->xAxisSlider)->setValue(idxX);
			TC::SilentCall(d->ui->xAxisSpinBox)->setValue(idxX);

			TC::SilentCall(d->ui->yAxisSlider)->setValue(idxY);
			TC::SilentCall(d->ui->yAxisSpinBox)->setValue(idxY);

			TC::SilentCall(d->ui->zAxisSlider)->setValue(idxZ);
			TC::SilentCall(d->ui->zAxisSpinBox)->setValue(idxZ);

			TC::SilentCall(d->ui->xPhySpinBox)->setValue(phyX);
			TC::SilentCall(d->ui->yPhySpinBox)->setValue(phyY);
			TC::SilentCall(d->ui->zPhySpinBox)->setValue(phyZ);
		} else {
			TC::SilentCall(d->ui->xAxisSlider)->setRange(1, info->width);
			TC::SilentCall(d->ui->xAxisSpinBox)->setRange(1, info->width);
			TC::SilentCall(d->ui->yAxisSlider)->setRange(1, info->height);
			TC::SilentCall(d->ui->yAxisSpinBox)->setRange(1, info->height);
			TC::SilentCall(d->ui->zAxisSlider)->setRange(1, info->depth);
			TC::SilentCall(d->ui->zAxisSpinBox)->setRange(1, info->depth);

			TC::SilentCall(d->ui->xPhySpinBox)->setRange(info->resolutionX, (float)info->width * info->resolutionX);
			TC::SilentCall(d->ui->yPhySpinBox)->setRange(info->resolutionY, (float)info->height * info->resolutionY);
			TC::SilentCall(d->ui->zPhySpinBox)->setRange(info->resolutionZ, (float)info->depth * info->resolutionZ);

			TC::SilentCall(d->ui->xPhySpinBox)->setSingleStep(info->resolutionX);
			TC::SilentCall(d->ui->yPhySpinBox)->setSingleStep(info->resolutionY);
			TC::SilentCall(d->ui->zPhySpinBox)->setSingleStep(info->resolutionZ);

			if (false == d->initialized) {
				TC::SilentCall(d->ui->xAxisSlider)->setValue(info->width / 2);
				TC::SilentCall(d->ui->xAxisSpinBox)->setValue(info->width / 2);

				TC::SilentCall(d->ui->yAxisSlider)->setValue(info->height / 2);
				TC::SilentCall(d->ui->yAxisSpinBox)->setValue(info->height / 2);

				TC::SilentCall(d->ui->zAxisSlider)->setValue(info->depth / 2);
				TC::SilentCall(d->ui->zAxisSpinBox)->setValue(info->depth / 2);

				TC::SilentCall(d->ui->xPhySpinBox)->setValue((float)info->width * info->resolutionX / 2.f);
				TC::SilentCall(d->ui->yPhySpinBox)->setValue((float)info->height * info->resolutionY / 2.f);
				TC::SilentCall(d->ui->zPhySpinBox)->setValue((float)info->depth * info->resolutionZ / 2.f);
				d->initialized = true;
				this->NotifyChangedAxisSpinBox();
				return;
			}

			auto phyX = d->ui->xPhySpinBox->value();
			auto phyY = d->ui->yPhySpinBox->value();
			auto phyZ = d->ui->zPhySpinBox->value();
			int idxX, idxY, idxZ;
			if (isSwitched) {
				auto ds = GetNavigatorDS();
				auto flinfo = d->navigatorDS->list[Entity::FL3D];
				auto offsetZ = ds->zOffset - (flinfo->depth * flinfo->resolutionZ / 2);
				if (prevFLOnly && false == htExist) {
					phyZ = offsetZ + d->ui->zPhySpinBox->value();
				} else if (false == htExist) {
					phyZ = d->ui->zPhySpinBox->value() - offsetZ;
				}
				if (phyX < info->resolutionX) {
					phyX = info->resolutionX;
				} else if (phyX > info->resolutionX * info->width) {
					phyX = info->resolutionX * info->width;
				}
				if (phyY < info->resolutionY) {
					phyY = info->resolutionY;
				} else if (phyY > info->resolutionY * info->height) {
					phyY = info->resolutionY * info->height;
				}
				if (phyZ < info->resolutionZ) {
					phyZ = info->resolutionZ;
				} else if (phyZ > info->resolutionZ * info->depth) {
					phyZ = info->resolutionZ * info->depth;
				}
				idxX = static_cast<int>(phyX / info->resolutionX);
				idxY = static_cast<int>(phyY / info->resolutionY);
				idxZ = static_cast<int>(phyZ / info->resolutionZ);
			} else {
				idxX = d->ui->xAxisSpinBox->value();
				idxY = d->ui->yAxisSpinBox->value();
				idxZ = d->ui->zAxisSpinBox->value();
			}

			TC::SilentCall(d->ui->xAxisSlider)->setValue(idxX);
			TC::SilentCall(d->ui->xAxisSpinBox)->setValue(idxX);

			TC::SilentCall(d->ui->yAxisSlider)->setValue(idxY);
			TC::SilentCall(d->ui->yAxisSpinBox)->setValue(idxY);

			TC::SilentCall(d->ui->zAxisSlider)->setValue(idxZ);
			TC::SilentCall(d->ui->zAxisSpinBox)->setValue(idxZ);

			TC::SilentCall(d->ui->xPhySpinBox)->setValue(phyX);
			TC::SilentCall(d->ui->yPhySpinBox)->setValue(phyY);
			TC::SilentCall(d->ui->zPhySpinBox)->setValue(phyZ);
		}
		this->NotifyChangedAxisSpinBox();

	}

	void NavigatorPanel::onPositionChanged(int axis, int value) {
		switch (axis) {
			case 0:
				d->ui->zAxisSlider->setValue(value);
				break;
			case 1:
				d->ui->xAxisSlider->setValue(value);
				break;
			case 2:
				d->ui->yAxisSlider->setValue(value);
				break;
			default: ;
		}
	}

	void NavigatorPanel::NotifyChangedAxisSpinBox() {
		emit positionChanged(d->ui->xAxisSpinBox->value(),
							d->ui->yAxisSpinBox->value(),
							d->ui->zAxisSpinBox->value());
	}

	void NavigatorPanel::NotifyChangedAxisSlider() {
		emit positionChanged(d->ui->xAxisSlider->value(),
							d->ui->yAxisSlider->value(),
							d->ui->zAxisSlider->value());
	}

	void NavigatorPanel::NotifyChangedPhySpinBox() {
		emit phyPositionChanged(d->ui->xPhySpinBox->value(),
								d->ui->yPhySpinBox->value(),
								d->ui->zPhySpinBox->value());
	}

	auto NavigatorPanel::SetEnableUI(const bool& enable) const -> void {
		d->ui->xAxisLabel->setEnabled(enable);
		d->ui->xAxisSlider->setEnabled(enable);
		d->ui->xAxisSpinBox->setEnabled(enable);

		d->ui->yAxisLabel->setEnabled(enable);
		d->ui->yAxisSlider->setEnabled(enable);
		d->ui->yAxisSpinBox->setEnabled(enable);

		d->ui->zAxisLabel->setEnabled(enable);
		d->ui->zAxisSlider->setEnabled(enable);
		d->ui->zAxisSpinBox->setEnabled(enable);

		d->ui->xPhySpinBox->setEnabled(enable);
		d->ui->yPhySpinBox->setEnabled(enable);
		d->ui->zPhySpinBox->setEnabled(enable);
	}
}
