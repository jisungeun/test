#pragma once

#include <QOivRenderWindow.h>

#include "Viewer2dRenderWindow3DExport.h"

class SoEventCallback;

namespace TomoAnalysis::Viewer2D::Plugins {
    class Viewer2dRenderWindow3D_API ViewerRenderWindow3d : public QOivRenderWindow {
        Q_OBJECT
    public:
        ViewerRenderWindow3d(QWidget* parent);
        ~ViewerRenderWindow3d();

        auto closeEvent(QCloseEvent* unused) -> void override;;

        auto reset3DView(bool fromFunc = false)->void;
        auto clearOrbit()->void;
        auto rotate(int axis, float val)->void;
        auto rotate(float x, float y, float z, float radian)->void;
        auto tilting(int axis, int orbit, float tic, float inter)->void;
        auto setViewDirection(int axis)->void;
        auto resetOnLoad()->void;

    signals:
        void isWhite(bool);
        void sigMouseMove();

    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;
        auto ChangeBackGround(int type)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}