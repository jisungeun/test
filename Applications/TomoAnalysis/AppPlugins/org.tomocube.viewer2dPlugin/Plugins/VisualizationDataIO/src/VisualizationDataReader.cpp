#include <QString>

#include "tinyxml.h"

#include "VisualizationDataReader.h"

namespace TomoAnalysis::Viewer2D::Plugins::VisualizationDataIO {
	VisualizationDataReader::VisualizationDataReader() {

	}

	VisualizationDataReader::~VisualizationDataReader() = default;

	auto VisualizationDataReader::Read(const std::string& path, Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel)->bool {
		if(true == path.empty()) {
			return false;
		}

	    TiXmlDocument document(path.c_str());
		if (false == document.LoadFile()) {
			return false;
		}

		auto root = document.FirstChildElement("TransferFunction");
		if(nullptr == root) {
			return false;
		}

		// ri
		auto riElement = root->FirstChildElement("RI_Tomogram");
		if(nullptr != riElement) {
		    auto dataElement = riElement->FirstChildElement("data");
			while(dataElement) {
				auto item = std::make_shared<Entity::TFItem>();
				item->intensityMin = QString(dataElement->Attribute("x1")).toDouble() * 10.0;
				item->intensityMax = QString(dataElement->Attribute("x2")).toDouble() * 10.0;
				item->gradientMin = QString(dataElement->Attribute("y1")).toDouble();
				item->gradientMax = QString(dataElement->Attribute("y2")).toDouble();
				item->transparency = QString(dataElement->Attribute("opacity")).toFloat();

				int r = QString(dataElement->Attribute("cr")).toFloat() * 255;
				int g = QString(dataElement->Attribute("cg")).toFloat() * 255;
				int b = QString(dataElement->Attribute("cb")).toFloat() * 255;
				item->color = Entity::Color(r, g, b, 0);

				tfItemList.push_back(item);

				dataElement = dataElement->NextSiblingElement();
			}
		}

		// fl
		int channelIndex = 0;				
		auto flElement = root->FirstChildElement("FL_Tomogram");
		if (nullptr != flElement) {
			auto dataElement = flElement->FirstChildElement("data");
			while (dataElement) {
				auto info = std::make_shared<Entity::ChannelInfo>();
				info->min = QString(dataElement->Attribute("x1")).toDouble();
				info->max = QString(dataElement->Attribute("x2")).toDouble();
				info->opacity = QString(dataElement->Attribute("opacity")).toFloat() * 100;

				channel[Entity::Channel::_from_integral(channelIndex)] = info;

				auto validNum = QString(dataElement->Attribute("v")).toInt();
				if(validNum>0) {
					info->isValid = true;
					info->visible = true;
				}else {
					info->isValid = false;
					info->visible = false;
				}
				++channelIndex;
				dataElement = dataElement->NextSiblingElement();
			}
		}

	    return true;
	}
}

