#pragma once

#include <IVisualizationDataWritePort.h>

#include "TomoAnalysis2dVisualizationDataIOExport.h"

namespace TomoAnalysis::Viewer2D::Plugins::VisualizationDataIO {
	class TomoAnalysis2dVisualizationDataIO_API VisualizationDataWriter : public UseCase::IVisualizationDataWritePort {
	public:
		VisualizationDataWriter();
		~VisualizationDataWriter();

		auto Write(const std::string& path, Entity::TFItemList tfItemList, Entity::FLChannelInfo channel)->bool override;

	private:
		auto GenerateBackup(const std::string& path)->bool;
	};
}