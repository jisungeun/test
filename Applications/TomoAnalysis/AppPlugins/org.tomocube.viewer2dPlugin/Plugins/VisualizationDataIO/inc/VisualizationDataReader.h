#pragma once

#include <string>
#include <IVisualizationDataReadPort.h>

#include "TomoAnalysis2dVisualizationDataIOExport.h"

namespace TomoAnalysis::Viewer2D::Plugins::VisualizationDataIO {
	class TomoAnalysis2dVisualizationDataIO_API VisualizationDataReader : public UseCase::IVisualizationDataReadPort {
	public:
		VisualizationDataReader();
		~VisualizationDataReader();

		auto Read(const std::string& path, Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel)->bool override;
	};
}