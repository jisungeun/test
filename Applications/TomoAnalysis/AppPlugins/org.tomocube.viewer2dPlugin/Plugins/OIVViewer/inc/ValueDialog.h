#pragma once

#include <memory>
#include <QString>
#include <QWidget>

#include "CustomDialog.h"
#include "TCFMetaReader.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class ValueDialog : public TC::CustomDialog
    {
        Q_OBJECT
    public:
        explicit ValueDialog(QWidget* parent = nullptr);
        ~ValueDialog();

        auto SetValue(const float& value) const ->void;
        auto GetValue() const ->float;

    protected:
        auto GetMinimumWidth() const -> int override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
