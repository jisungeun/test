#pragma once

#include <memory>

#include <QWidget>
#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/opencv.hpp>
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

#include <IImageViewer.h>

//#include <OivTcfReader.h>
#include <ScreenShotWidget.h>
#include <ScreenShot.h>
#include <ScalarBarInfo.h>
#include <ScaleBarInfo.h>
#include <TimeStampInfo.h>
#include <DeviceInfo.h>
#include <QOivRenderWindow.h>
#include <ColormapInfo.h>
#include <Camera2DInfo.h>

#include <TCImage.h>
//#include <RecordVideoPanel.h>
#include <VideoRecorderWidget.h>

#include "TomoAnalysis2dViewerExport.h"

class RoiXY;

namespace TomoAnalysis::Viewer2D::Plugins {
	class TomoAnalysis2dViewer_API OIVViewer : public QWidget, public Interactor::IImageViewer {
		Q_OBJECT

	public:
		typedef OIVViewer Self;
		typedef std::shared_ptr<Self> Pointer;

		OIVViewer(SoNode* node, SoSeparator* node2d[3], QWidget* parent = nullptr);
		OIVViewer(QWidget* parent = nullptr);
		~OIVViewer();

		auto UpdateCall() -> bool override;
		auto Update() -> bool override;
		auto Refresh() -> bool override;
		auto UpdateTransferFunction(const int& index,
									const Entity::TFItem::Pointer& item) -> bool override;
		auto UpdateChannelInfo(const Entity::FLChannelInfo& info) -> bool override;
		auto UpdateVisualizationData(const Entity::TFItemList& tfItemList,
									const Entity::FLChannelInfo& info) -> bool override;
		auto ClearTransferFunction() -> bool override;

		auto Init(void) -> bool;
		auto Reset(void) -> bool;

		auto RequestRender(void) -> void;

		auto SetRootNode(SoNode* node, SoSeparator* node2d[3]) const -> void;
		auto SetSceneID(const int& id) const -> void;
		auto SetTFWidget(QWidget* widget) const -> void;
		auto SetMeasureToolSceneGraph(SoSeparator* measureRoot) -> void;

		auto ResetLayout(const int& type = -1) -> void; // -1 = reset
		auto SaveLayout(const QString& path) const -> bool;
		auto LoadLayout(const QString& path) const -> bool;
		auto GetCurrentLayoutType(void) const -> int;

		//non-clean architecture
		auto SetCurrentProjectPath(const QString& path) -> void;
		auto SetDisplayType(bool is2D) -> void;
		auto SetTimePoints(std::vector<double>) -> void;
		auto ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void;
		auto ConnectRecorderWidget(TC::VideoRecorderWidget::Pointer widget) -> void;

		//Save/Load annotation information
		auto SetScalarBarInfo(const AppEntity::ScalarBarInfo& info) -> void;
		auto GetScalarBarInfo() const -> AppEntity::ScalarBarInfo;
		auto SetScaleBarInfo(const AppEntity::ScaleBarInfo& info) -> void;
		auto GetScaleBarInfo() const -> AppEntity::ScaleBarInfo;
		auto SetTimeStampInfo(const AppEntity::TimeStampInfo& info) -> void;
		auto GetTimeStampInfo() const -> AppEntity::TimeStampInfo;
		auto SetDeviceInfo(const AppEntity::DeviceInfo& info) -> void;
		auto GetDeviceInfo() const -> AppEntity::DeviceInfo;

		//Save/Load Visualization information
		auto SetHTInfo(const AppEntity::ColormapInfo& info) -> void;
		auto GetHTInfo() const -> AppEntity::ColormapInfo;
		auto SetFLInfo(int ch, const AppEntity::ColormapInfo& info) -> void;
		auto GetFLInfo(int ch) const -> AppEntity::ColormapInfo;
		auto SetCam2dInfo(const AppEntity::Camera2DInfo& info) -> void;
		auto GetCam2dInfo() const -> AppEntity::Camera2DInfo&;

		auto RefreshVoxelInfo() -> void;
	signals:
		void callUpdate();

		void usableModalityChanged(Entity::Modality modality);

		void gradientRangeLoaded(double min, double max);

		void transferFunctionChanged(Entity::TFItemList list);
		void currentTransferFunctionChanged(int index);

		void sliceIndexChanged(int viewIndex, int sliceIndex);
		void timeStepChanged(double step);

		void finishSimulate();
		void captureScreen(int);

		void moveSlice(int viewIndex, int sliceIndex);

		void timeStepPlayed(double);

		void sigNewRoi(float, float, float, float);
		void sigModifyRoi(int, float, float, float, float);
		//void sigRoiCrop(int, int, int, int,int,int,int,int);
		void sigRoiCrop(RoiXY, RoiXY);

		void sigTimeColor(QColor);
		void sigLevelChanged(double, double);

		void sigZoomMeasureHandle(float);
		void sigVolumeMaxLen(float);
		void sigResChange(int);

	protected slots:
		void OnStartPlaying();
		void OnFinishPlaying();
		void OnResolutionChanged(int, float);

		void onUpdateCall();

		void onCanvasMouse(bool);
		void onAddTransferFunction();
		void onSelectTFItem(int idx,
							double min, double max,
							double grad_min, double grad_max,
							double opacity);
		void onSelectTransferFunctionItem(int index);
		void onTransferFunctionHistogramChanged(int index) const;
		void onMoveSlice(float value);
		void onLevelChanged(float ri_min, float ri_max);
		void onColormapChanged(int colormap_idx);

		void onScaleBarTick(const int& value);
		void onScaleBarLength(const int& value);

		void onVoxelInfo(TCVoxelInfo);

		void onDirectionChanged(TC::DirectionType type);

		void onSimulate(QList<TC::RecordAction*> process);
		void onStopSimulate() const;
		void onFinishedSimulate();
		void onStartRecord(QString path, TC::RecordType type, QList<TC::RecordAction*> process);
		void onFinishedRecord();
		void onCaptureScreen(int type);
		void on3DBackgroundChanged(bool isWhite);
		void onRowRes(bool inProgress);

		void mouseMoveIn3D();
		void mouseMoveIn2D();

		//for ROI control
		void onToggleROI(bool);
		void onActivateROI();
		void onROIAdded();
		void onModifyROI(bool);
		void onROIModified(int);
		void onRemoveROI(int);
		void onClearROI();
		void onCropROI(int);
		void onFocusROI(int);

		//for Device Information
		void SetDeviceSize(int);
		void SetDeviceColor(QColor);
		void SetVisibleDevice(bool);
		void SetVisibleScaleBar(bool);
		void SetVisibleScalarBar(bool);
		void SetVisibleMinimap(bool);

		void SetGammaColormap(int, bool, double);

	private:
		auto InitUI(void) -> bool;
		auto InitOIV(void) -> bool;
		auto InitRender(void) -> bool;

		auto BuildUp(const int& modality) -> bool;
		auto BuildHiddenScene() const -> bool;
		auto Build3DScene(void) const -> bool;
		auto Build2DScene(void) const -> bool;
		auto BuildHTSlice(void) const -> bool;
		auto BuildFLSlice(void) const -> bool;
		auto BuildHTVolume(void) -> bool;
		auto BuildFLVolume(void) -> bool;
		auto BuildBF(void) -> bool;
		auto BuildTFCanvas(void) -> bool;
		auto BuildCamera(void) const -> bool;
		auto BuildScaleBar(void) -> bool;
		auto BuildScalarBar(void) -> bool;
		auto BuildRoiCropper(void) -> bool;

		auto SetMIP(const bool& isMIP) const -> void;
		auto SetHTMIP(const bool& isHTMIP) const -> void;
		auto SetFLMIP(const bool& isFLMIP) const -> void;

		auto Render(const Interactor::SceneDS::Pointer& ds) -> bool;
		auto SetTimeStep(const double& timeIndex) -> void;

		auto AnimateSlice(const int& sliceType,
						const float& startTime, const float& endTime,
						const int& begin, const int& finish,
						const bool& reverse, const bool& last = false) -> void;

		auto AnimateTimelapse(const int& modality,
							const float& startTime, const float& endTime,
							const int& begin, const int& finish,
							const bool& reverse, const bool& last = false) -> void;

		auto RecordScreen(QString path, TC::RecordType type, QList<TC::RecordAction*> process) -> bool;

		auto WindowIndex2MedicalHelperAxis(const int& windowIndex) -> int;

		auto ChangeLayout(const Entity::LayoutType& layoutType) const -> void;

		auto Grab() -> QPixmap;

		void Delay(const LARGE_INTEGER& frequency, const float& millisecond);

		auto MergeScreens(std::list<cv::Mat>& cvBuffer) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
