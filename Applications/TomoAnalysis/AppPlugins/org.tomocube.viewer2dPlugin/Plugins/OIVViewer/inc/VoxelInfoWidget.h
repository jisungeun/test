#pragma once

#include <memory>

#include <QWidget>

namespace TomoAnalysis::Viewer2D::Plugins {
	class VoxelInfoWidget : public QWidget {
	    Q_OBJECT
	public:
		VoxelInfoWidget(QWidget* parent = nullptr);
		~VoxelInfoWidget();

		auto SetPosition(double x, double y, double z,double timePoint)->void;
		auto SetRIValue(double value)->void;
		auto SetFLValue(double ch1Value, double ch2Value, double ch3Value)->void;
		auto ToggleVisible(bool isVisible)->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}