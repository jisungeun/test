#pragma once

#include <QMainWindow>
#include <QGridLayout>
#include <QWheelEvent>
#include <QString>
#include <QSettings>

#include <Scene.h>
#include <ViewConfig.h>

namespace TomoAnalysis::Viewer2D::Plugins {
	typedef QPair<Entity::ViewType, QWidget*> View;
	typedef QPair<QString, QSize> ViewGeometry;

    class LayoutView : public QWidget {		
	public:
		LayoutView(QWidget* parent = nullptr);
		~LayoutView();

	public:
		auto SaveLayout(const QString& path) const ->bool;
        auto LoadLayout(const QString& path)->bool;

		auto SaveCurrent() const ->void;
		auto LoadCurrent()->bool;

        auto GetLayoutType(void) const ->Entity::LayoutType;
	    auto SetLayoutType(const Entity::LayoutType& layoutType, QList<ViewGeometry>* geometryList = nullptr)->void;

	    auto AddWidget(const Entity::ViewType& viewType, QWidget* widget) ->void;

		auto GetActivatedWidget(void) const ->QWidget*;
		auto GetActivatedWidgetIndex(void) const ->int;
		auto GetActivatedWidgetType(void) const ->Entity::ViewType;

		auto FindWidgetType(QWidget* widget) const ->Entity::ViewType;

    protected:
		auto eventFilter(QObject* obj, QEvent* event) -> bool override;

	private:
		auto FindWidget(const Entity::ViewType& viewType) const ->QWidget*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;

	};
}