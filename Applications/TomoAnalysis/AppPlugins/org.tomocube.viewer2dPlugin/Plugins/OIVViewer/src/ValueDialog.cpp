#include "ValueDialog.h"
#include "ui_ValueDialog.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct ValueDialog::Impl {
        QFrame* frame;
        Ui::ExportDialog ui;
    };

    ValueDialog::ValueDialog(QWidget* parent) : CustomDialog(parent), d{ new Impl } {
        d->frame = new QFrame(this);
        d->ui.setupUi(d->frame);
        SetContext(d->frame);
        SetStandardButtons(StandardButton::NoButton);
    }

    ValueDialog::~ValueDialog() {
    }

    auto ValueDialog::SetValue(const float& value) const ->void {
        d->ui.lineEdit->setText(QString::number(value));
    }

    auto ValueDialog::GetValue() const ->float {
        return d->ui.lineEdit->text().toFloat();
    }

    int ValueDialog::GetMinimumWidth() const {
        return d->frame->minimumSize().width();
    }
}
