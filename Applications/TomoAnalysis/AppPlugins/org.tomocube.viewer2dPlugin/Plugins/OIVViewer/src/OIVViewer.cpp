#include <utility>

#include <qfuture.h>
#include <QFileInfo>
#include <QtConcurrent/qtconcurrentrun.h>
#include <QTimer>
#include <QDateTime>
#include <QFutureWatcher>
#include <QColorDialog>
#include <QLabel>
#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/SoOffscreenRenderArea.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/nodes/SoTranslation.h>

#include <Inventor/nodes/SoScale.h>
#include <Inventor/Axis.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/TextBox.h>
#pragma warning(pop)

#include <OivSceneXY.h>

#include <HiddenScene.h>
#include <OivBufferManager.h>
#include <ViewerRenderWindow2d.h>
#include <QTransferFunctionCanvas2D.h>
#include <ScalarBarSwitch.h>
#include <ScaleBarSwitch.h>
#include <OivMinimap.h>
#include <TimeStampSwitch.h>
#include <RoiCropSep.h>
#include <OivAxisGrid.h>
#include <Oiv2dBFSlice.h>
#include "VoxelInfoWidget.h"

#include <TCFInfoPresenter.h>

#include <ToastMessageBox.h>
#include <TCVideoWriter.h>
#include <TCFMetaReader.h>
#include <LevelWindow.h>

#include <QtMath>
#include "ValueDialog.h"
#include "LayoutView.h"
#include "OIVViewer.h"

#include <qbitarray.h>
#include <QDir>
#include <QPushButton>
#include <Inventor/engines/SoTransformVec3f.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/engines/SoCalculator.h>
#include <Inventor/engines/SoCompose.h>

namespace TomoAnalysis::Viewer2D::Plugins {
#define SAFE_DELETE( p ) { if( p ) { delete ( p ); ( p ) = NULL; } }

#define NUMBER_OF_FL_CHANNEL        3
#define NUMBER_OF_2D_RENDER_WINDOW  1

#define HT_INDEX 0
#define FL_INDEX 1
#define BF_INDEX 2

	struct OIVViewer::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		// Common
		int sceneID = -1;
		QString filePath;

		Entity::LayoutType layoutType = Entity::LayoutType::UNKNOWN;
		int activatedModality = Entity::Modality::None;

		// reader
		TC::IO::TCFMetaReader::Meta::Pointer metaInfo;
		double htValueDiv { 1 };
		double htValueOffset { 0 };
		double flValueOffset[3] { 0, 0, 0 };
		double curLevelMin { 0 };
		double curLevelMax { 0 };

		SoRef<SoGroup> infoGroup = nullptr;
		QList<SoRef<SoInfo>> infos;//current metainformation container
		TC::LevelWindow* levelWindow { nullptr };
		//buffer reader
		OivBufferManager* bufferManager;
		QList<QString> bufferTileName;

		// scene graph
		//OivSliceContainer* sliceContainer = nullptr;
		TC::OivSceneXY* sliceConXY = nullptr;
		//OivVolumeContainer* volumeContainer = nullptr;
		HiddenScene* hiddenScene = nullptr;

		Oiv2dBFSlice* bfContainer = nullptr;

		SoRef<SoSwitch> sliceSwitch[NUMBER_OF_2D_RENDER_WINDOW] { nullptr, };

		SoRef<SoSeparator> sliceRenderRoot[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		bool isBuildHT2D = false;
		bool isBuildFL2D = false;
		bool isBuildHT3D = false;
		bool isBuildFL3D = false;
		bool isBuildBF = false;
		bool isBuildTFCanvas = false;
		bool isBuildCamera = false;
		bool isBuildScaleBar = false;
		bool isBuildScalarBar = false;
		bool isBuildRoiCropper = false;
		bool isBuildScene = false;

		// render window
		LayoutView* layoutView = nullptr;
		ViewerRenderWindow2d* qOiv2DRenderWindow[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		QWidget* tfCanvasWidget = nullptr;
		QTransferFunctionCanvas2D* tfCanvas = nullptr;

		bool isOverlay = false;

		SoRef<SoSwitch> timestampSwitch = nullptr;
		SoRef<SoMaterial> annoMatl { nullptr };
		SoRef<TextBox> annoText { nullptr };

		SoRef<SoSwitch> scaleBarSwitch[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		TC::ScaleBarSwitch* scaleBar[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSeparator> manipulatorRoot[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSwitch> scalarBarSwitch[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		SoRef<SoSeparator> scalarBarSep[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		TC::ScalarBarSwitch* scalarBar[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSwitch> roiSwitch[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		TC::RoiCropSep* roiCropper[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSwitch> minimapSwitch[NUMBER_OF_2D_RENDER_WINDOW] { nullptr, };
		SoRef<SoSeparator> minimapSep[NUMBER_OF_2D_RENDER_WINDOW] { nullptr, };
		SoRef<OivMinimap> miniMap[NUMBER_OF_2D_RENDER_WINDOW] { nullptr, };

		SoRef<SoSwitch> deviceInfoSwitch = { nullptr, };
		SoRef<SoMaterial> deviceMatl { nullptr };
		SoRef<TextBox> htxInfo { nullptr };

		int volumeChildIndex = 0;
		int sliceChildIndex = 0;
		int sliceTraversalMode = SoMultiSwitch::INCLUDE;
		int childIndex;
		int curRes = 0;

		bool simulating = false;
		QMutex simulationMutex;
		QFutureWatcher<bool> simulationWatcher;

		QList<QPixmap> recordBuffer;
		QList<QPixmap> recordBufferXY;
		QString recordSavePath;
		QFutureWatcher<bool> recordWatcher;

		int text_unit { 0 };
		//SoRef<SoSeparator> rootHolder;

		QString curProjPath;

		bool sliceInited { false };
		std::vector<double> time_points;

		bool isIn3D { true };
		bool curChVisible[3] { true, true, true };
		bool curChVisiblePhy[3] { true, true, true };
		bool chIsGamma[3] { false, false, false };
		double chGamma[3] { 1.0, 1.0, 1.0 };
		double chRange[3][2] { { 0.0, 0.0 }, { 0.0, 0.0 }, { 0.0, 0.0 } };
		double chOpacity[3] { 1.0, 1.0, 1.0 };
		double chRGB[3][3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

		bool isGamma { false };
		double gamma { 1.0 };

		bool visibleScalarBar { false };

		double curTimePoint { 0 };
		VoxelInfoWidget* voxelInfo { nullptr };
		auto BuildDeviceInfo(const QString& path) -> void;
		auto ApplyColormap(SoTransferFunction* tf, int colormap_idx, bool isGamma, double gamma) -> void;
	};

	auto OIVViewer::Impl::ApplyColormap(SoTransferFunction* tf, int colormap_idx, bool isGamma, double gamma) -> void {
		switch (colormap_idx) {
			case 0:
				tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
				break;
			case 1:
				tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
				break;
			case 2:
				tf->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
				break;
			case 3:
				tf->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
				break;
			case 4:
				tf->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
				break;
		}
		auto dummyTF = new SoTransferFunction;
		dummyTF->ref();
		dummyTF->predefColorMap = tf->predefColorMap.getValue();
		auto steps = tf->actualColorMap.getNum() / 4;
		tf->actualColorMap.setNum(256 * 4);
		auto p = tf->actualColorMap.startEditing();
		auto base = dummyTF->actualColorMap.getValues(0);
		auto actualGamma = gamma;
		if (false == isGamma) {
			actualGamma = 1;
		}
		for (auto i = 0; i < steps; i++) {
			auto r = *base++;
			auto g = *base++;
			auto b = *base++;
			*base++;
			float mod_r = pow(r, 1.0 / actualGamma);
			float mod_g = pow(g, 1.0 / actualGamma);
			float mod_b = pow(b, 1.0 / actualGamma);
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = 1;
		}
		dummyTF->unref();
		dummyTF = nullptr;
		tf->actualColorMap.finishEditing();

	}

	auto OIVViewer::Impl::BuildDeviceInfo(const QString& path) -> void {
		if ("HTX" == metaInfo->common.deviceModelType) {
			auto tokens = path.split(".");
			QString well = "";
			QString specimen = "";
			if (tokens.count() > 4) {
				well = tokens[tokens.length() - 3];
			}
			if (tokens.count() > 5) {
				specimen = tokens[tokens.length() - 4];
			}

			auto deviceInfo = QString();

			//try scan device info
			QFileInfo fileInfo(path);
			auto file_dir = fileInfo.dir();
			file_dir.cdUp();//parent dir
			QString exp_name = "";
			if (false == file_dir.entryList({ "*.tcxexp" }, QDir::NoDotAndDotDot | QDir::Files).isEmpty()) {
				auto exps = file_dir.entryList({ "*.tcxexp" }, QDir::NoDotAndDotDot | QDir::Files)[0].split(".");
				for (auto i = 0; i < exps.count() - 1; i++) {
					exp_name += exps[i];
				}
			}

			file_dir.cdUp();//ancestor dir
			QString prj_name = "";
			if (false == file_dir.entryList({ "*.tcxpro" }, QDir::NoDotAndDotDot | QDir::Files).isEmpty()) {
				auto projs = file_dir.entryList({ "*.tcxpro" }, QDir::NoDotAndDotDot | QDir::Files)[0].split(".");
				for (auto i = 0; i < projs.count() - 1; i++) {
					prj_name += projs[i];
				}
			}
			htxInfo->deleteAll();
			if (false == prj_name.isEmpty()) {
				htxInfo->addLine(QString("Project: %1 ").arg(prj_name).toStdString());
			}
			if (false == exp_name.isEmpty()) {
				htxInfo->addLine(QString("Experiment: %1 ").arg(exp_name).toStdString());
			}
			if (false == specimen.isEmpty()) {
				htxInfo->addLine(QString("Specimen: %1 ").arg(specimen).toStdString());
			}
			if (false == well.isEmpty()) {
				htxInfo->addLine(QString("Well: %1").arg(well).toStdString());
			}
		}
	}

	OIVViewer::OIVViewer(SoNode* node, SoSeparator* node2d[3], QWidget* parent)
		: OIVViewer(parent) {
		SetRootNode(node, node2d);
	}

	OIVViewer::OIVViewer(QWidget* parent)
		: QWidget(parent)
		, Interactor::IImageViewer()
		, d(new Impl()) {

		SoLDMGlobalResourceParameters::setSliceEqualResolution(true);
		SoLDMGlobalResourceParameters::setNumIO(20);
	}

	OIVViewer::~OIVViewer() { }

	auto OIVViewer::SetTimePoints(std::vector<double> tps) -> void {
		d->time_points = tps;
	}

	auto OIVViewer::ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void {
		widget->SetRenderWindow(d->qOiv2DRenderWindow[0], "XY");

		widget->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
	}

	auto OIVViewer::ConnectRecorderWidget(TC::VideoRecorderWidget::Pointer widget) -> void {
		widget->SetRenderWindow(d->qOiv2DRenderWindow[0], "XY");

		widget->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
	}

	auto OIVViewer::SetDisplayType(bool is2D) -> void {
		if (is2D) {
			d->sliceConXY->SetHTOverlay(false);
		} else {
			d->sliceConXY->SetHTOverlay(d->isOverlay);
		}
	}

	auto OIVViewer::UpdateCall() -> bool {
		emit callUpdate();
		return true;
	}

	void OIVViewer::OnStartPlaying() {
		d->qOiv2DRenderWindow[0]->toggleFPS(true);
		d->qOiv2DRenderWindow[0]->StartCalculateFPS(30);
	}

	void OIVViewer::OnFinishPlaying() {
		auto avgFPS = d->qOiv2DRenderWindow[0]->FinishCalculateFPS();
		d->qOiv2DRenderWindow[0]->toggleFPS(false);
	}


	auto OIVViewer::Update() -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.000001;
		};
		const auto ds = GetFileDS();
		const QString path = QString::fromUtf8(ds->path.c_str());

		if (true == path.isEmpty()) {
			return false;
		}

		d->filePath = path;
		if (nullptr == d->hiddenScene) {
			d->hiddenScene = new HiddenScene;
		}
		d->hiddenScene->SetFilePath(d->filePath);
		// read info
		auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		d->metaInfo = metaReader->Read(path);
		if (d->metaInfo->data.data3D.scalarType.count() > 0) {
			if (d->metaInfo->data.data3D.scalarType[0] == 1) {
				d->htValueOffset = d->metaInfo->data.data3D.riMinList[0] * 1000.0;
				d->htValueDiv = 10;
			}
		} else if (d->metaInfo->data.data2DMIP.scalarType.count() > 0) {
			if (d->metaInfo->data.data2DMIP.scalarType[0] == 1) {
				d->htValueOffset = d->metaInfo->data.data2DMIP.riMinList[0] * 1000.0;
				d->htValueDiv = 10;
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (d->metaInfo->data.data3DFL.scalarType[i].count() > 0) {
				if (d->metaInfo->data.data3DFL.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->metaInfo->data.data3DFL.minIntensityList[i][0];
				}
			} else if (d->metaInfo->data.data2DFLMIP.scalarType[i].count() > 0) {
				if (d->metaInfo->data.data2DFLMIP.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->metaInfo->data.data2DFLMIP.minIntensityList[i][0];
				}
			}
		}
		d->voxelInfo->ToggleVisible(false);
		d->BuildDeviceInfo(path);

		d->levelWindow->SetMinMax(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		auto isLdm = d->metaInfo->data.isLDM;

		//d->bufferManager = new OivBufferManager;
		d->bufferManager = new OivBufferManager(true, true);
		d->bufferManager->setMetaInfo(d->metaInfo);
		d->bufferManager->setNumberOfBuffer(1);
		d->bufferManager->setFilePath(path);

		d->bufferManager->setNumberOfImage(d->metaInfo->data.data3D.dataCount);
		d->bufferManager->setNumberOfFLImage(d->metaInfo->data.data3DFL.dataCount);
		d->bufferManager->setNumberOfBFImage(d->metaInfo->data.dataBF.dataCount);
		//existance added
		d->bufferManager->initialize();
		d->bufferManager->setDefaultReader();

		//set LDM option for buffer manager 20.09.04 Jose T. Kim			    
		//d->bufferManager->enableFixedResolution(false);		
		d->bufferManager->enableFixedResolution(true, false);

		d->infoGroup->removeAllChildren();
		d->infos.clear();

		auto HT_TS_info = new SoInfo;
		HT_TS_info->setName("HT_TIME_STEPS");
		HT_TS_info->string.setValue(std::to_string(d->metaInfo->data.data3D.dataCount));
		d->infos.push_back(HT_TS_info);
		d->infoGroup->addChild(HT_TS_info);

		auto HT_CURT_info = new SoInfo;
		HT_CURT_info->setName("HT_CUR_TIME");
		HT_CURT_info->string.setValue(std::to_string(0));
		d->infos.push_back(HT_CURT_info);
		d->infoGroup->addChild(HT_CURT_info);

		auto TCF_name_info = new SoInfo;
		TCF_name_info->setName("TCF_NAME");
		TCF_name_info->string.setValue(path.toStdString());
		d->infos.push_back(TCF_name_info);
		d->infoGroup->addChild(TCF_name_info);

		auto HT_RIMin_info = new SoInfo;
		HT_RIMin_info->setName("HTMin");
		HT_RIMin_info->string.setValue(std::to_string(d->metaInfo->data.data3D.riMin * 10000.0));
		d->infos.push_back(HT_RIMin_info);
		d->infoGroup->addChild(HT_RIMin_info);

		auto HT_RIMax_info = new SoInfo;
		HT_RIMax_info->setName("HTMax");
		HT_RIMax_info->string.setValue(std::to_string(d->metaInfo->data.data3D.riMax * 10000.0));
		d->infos.push_back(HT_RIMax_info);
		d->infoGroup->addChild(HT_RIMax_info);

		for (int i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->setHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);
				d->qOiv2DRenderWindow[i]->refreshRangeSlider();
			}
		}

		//Check FL & HT uniformity and change shader
		if (nullptr != d->sliceConXY) {
			if (d->sliceConXY->GetBuilt()) {
				auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
				auto metaInfo = metaReader->Read(path);
				auto dimSame = false;
				auto resSame = false;
				if (metaInfo->data.data3D.exist && metaInfo->data.data3DFL.exist) {
					if (AreSame(metaInfo->data.data3D.resolutionX, metaInfo->data.data3DFL.resolutionX)) {
						if (AreSame(metaInfo->data.data3D.resolutionY, metaInfo->data.data3DFL.resolutionY)) {
							if (AreSame(metaInfo->data.data3D.resolutionZ, metaInfo->data.data3DFL.resolutionZ)) {
								resSame = true;
							}
						}
					}
					if (metaInfo->data.data3D.sizeX == metaInfo->data.data3DFL.sizeX) {
						if (metaInfo->data.data3D.sizeY == metaInfo->data.data3DFL.sizeY) {
							if (metaInfo->data.data3D.sizeZ == metaInfo->data.data3DFL.sizeZ) {
								dimSame = true;
							}
						}
					}
				}
				d->sliceConXY->SetUniform(dimSame && resSame);
			}
		}

		if (d->metaInfo->data.data3D.exist || d->metaInfo->data.data2DMIP.exist) {
			d->levelWindow->show();
		} else {
			d->levelWindow->hide();
		}

		return true;
	}

	auto OIVViewer::Refresh() -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		const auto ds = GetSceneDS();
		if (!ds) {
			return false;
		}
		auto changedBF = false;
		if (d->activatedModality != ds->activatedModality) {
			if (false == this->BuildUp(ds->activatedModality)) {
				return false;
			}

			// MIP
			bool isHTMIP = false;
			if ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				isHTMIP = true;
			}
			this->SetHTMIP(isHTMIP);
			bool isFLMIP = false;
			if ((ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
				isFLMIP = true;
			}
			this->SetFLMIP(isFLMIP);

			this->SetMIP(isHTMIP || isFLMIP);


			if ((ds->activatedModality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
				changedBF = d->qOiv2DRenderWindow[0]->resetViewBF(true);
			} else {
				changedBF = d->qOiv2DRenderWindow[0]->resetViewBF(false);
			}
			d->activatedModality = ds->activatedModality;
		}

		if (d->layoutType != ds->layoutType) {
			this->ResetLayout(ds->layoutType);
			d->layoutType = ds->layoutType;
		}

		auto vol = d->bufferManager->getDefaultVolume();
		if (vol) {
			auto dim = vol->getDimension();
			auto res = vol->getVoxelSize();
			auto xSize = dim[0] * res[0];
			auto ySize = dim[1] * res[1];
			d->miniMap[0]->sizeX.setValue(xSize);
			d->miniMap[0]->sizeY.setValue(ySize);
			auto focalPos = d->qOiv2DRenderWindow[0]->getCamera()->getFocalPoint();
			d->miniMap[0]->movingX.setValue(focalPos[0]);
			d->miniMap[0]->movingY.setValue(focalPos[1]);
		}

		auto levelMin = std::get<0>(d->levelWindow->GetMinMax());
		auto levelMax = std::get<1>(d->levelWindow->GetMinMax());
		onLevelChanged(levelMin, levelMax);

		if (false == this->Render(ds)) {
			return false;
		}

		if (changedBF) {
			d->qOiv2DRenderWindow[0]->reset2DView();
		}

		return true;
	}

	auto OIVViewer::UpdateTransferFunction(const int& index,
											const Entity::TFItem::Pointer& item) -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		if (!item) {
			return false;
		}

		d->tfCanvas->ChangeItem(index,
								item->intensityMin, item->intensityMax,
								item->gradientMin, item->gradientMax,
								item->transparency);

		return true;
	}

	auto OIVViewer::UpdateChannelInfo(const Entity::FLChannelInfo& list) -> bool {
		auto ds = GetSceneDS();
		auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2);
		auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
		auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 0.5) - offsetZ;
		auto idxFLz = static_cast<int>(round(phyZ / resZ));

		if (resZ < 0.01 || d->metaInfo->data.data3DFL.sizeZ == 1 ) {//exception for only on slice
			idxFLz = -1;
			auto phyComp = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex()) - d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2 - offsetZ + d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2 + offsetZ;
			if (phyComp < d->metaInfo->data.data3D.resolutionZ && phyComp > 0) {
				idxFLz = 0;
			}
		}

		auto flIndexValid = true;
		if ((d->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
			if (idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1) {
				flIndexValid = false;
			}
		}
		if (d->metaInfo->data.isLDM) {
			flIndexValid = true;
		}
		for (const auto& channelInfo : list) {
			const auto channel = channelInfo.first._to_integral();
			const auto info = channelInfo.second;
			if (!info) {
				continue;
			}

			if (false == info->isValid) {
				continue;
			}

			const float transparency = 1.f - static_cast<float>(info->opacity / 100.f);
			if (true == d->isBuildFL3D) {
				d->sliceConXY->ToggleFLChannel(channel, info->visible && flIndexValid);
				d->sliceConXY->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConXY->SetFLColor(info->color[0], info->color[1], info->color[2], channel);
				d->sliceConXY->SetFLTrans(transparency, channel);
			} else if (nullptr != d->sliceConXY && true == d->isBuildFL2D) {
				d->sliceConXY->ToggleFLChannel(channel, info->visible);
				d->sliceConXY->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConXY->SetFLTrans(transparency, channel);
			}
			d->sliceConXY->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
			//save meta informations
			d->curChVisible[channel] = info->visible;
			d->chOpacity[channel] = info->opacity / 100.0f;
			for (auto i = 0; i < 3; i++) {
				d->chRGB[channel][i] = static_cast<double>(info->color[i]) / 255.0;
			}
			d->chRange[channel][0] = info->min;
			d->chRange[channel][1] = info->max;
			d->chGamma[channel] = static_cast<double>(info->gamma) / 100.0;
			d->chIsGamma[channel] = info->isGamma;
		}

		return true;
	}

	auto OIVViewer::UpdateVisualizationData(const Entity::TFItemList& tfItemList,
											const Entity::FLChannelInfo& info) -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		d->tfCanvas->DeleteAllItem();

		for (const auto item : tfItemList) {
			d->tfCanvas->AddItem(item->intensityMin, item->intensityMax,
								item->gradientMin, item->gradientMax,
								item->color.red, item->color.green, item->color.blue,
								item->transparency);
		}

		UpdateChannelInfo(info);

		return true;
	}

	auto OIVViewer::ClearTransferFunction() -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		d->tfCanvas->DeleteAllItem();

		Entity::TFItemList list;
		emit transferFunctionChanged(list);

		return true;
	}

	auto OIVViewer::Init(void) -> bool {
		if (false == this->InitOIV()) {
			return false;
		}

		if (false == this->InitUI()) {
			return false;
		}

		if (false == this->InitRender()) {
			return false;
		}

		return true;
	}

	auto OIVViewer::Reset(void) -> bool {
		// Common		
		//d->sceneID = -1;
		d->htValueDiv = 1;
		d->htValueOffset = 0;
		d->visibleScalarBar = false;
		for (auto i = 0; i < 3; i++) {
			d->flValueOffset[i] = 0;
			d->curChVisible[i] = true;
			d->curChVisiblePhy[i] = true;
			d->chIsGamma[i] = false;
			d->chGamma[i] = 1.0;
			d->chRange[i][0] = d->chRange[i][1] = 0.0;
			d->chRGB[i][0] = d->chRGB[i][1] = d->chRGB[i][2] = 0;
			d->chOpacity[i] = 1.0;
		}
		d->gamma = 1.0;
		d->isGamma = false;

		d->filePath = "";
		d->isBuildScene = false;
		d->activatedModality = Entity::Modality::None;
		d->curRes = 0;

		d->isBuildHT2D = false;;
		d->isBuildFL2D = false;
		d->isBuildHT3D = false;;
		d->isBuildFL3D = false;
		d->isBuildBF = false;
		d->isBuildTFCanvas = false;
		d->isBuildCamera = false;
		d->isBuildScaleBar = false;
		d->isBuildScalarBar = false;
		d->isBuildRoiCropper = false;

		d->isOverlay = false;

		d->layoutType = Entity::LayoutType::UNKNOWN;

		d->sliceConXY->ClearMIPVolume();
		d->sliceConXY->ClearMIPFLVolume();
		d->sliceConXY->ClearVolume();
		d->sliceConXY->ClearFLVolume();

		auto ds = GetSceneDS();

		for (int i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->scalarBarSwitch[i]->removeAllChildren();
			d->manipulatorRoot[i]->removeAllChildren();
			d->scaleBarSwitch[i]->removeAllChildren();
			d->roiSwitch[i]->removeAllChildren();

			d->sliceSwitch[i]->removeAllChildren();
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->whichChild = 0;

			d->minimapSep[i]->removeAllChildren();
		}
		d->htxInfo->deleteAll();
		d->htxInfo->addLine("No X1 Device Info.");
		d->htxInfo->fontSize.setValue(20);
		d->deviceInfoSwitch->whichChild = -1;
		d->deviceMatl->diffuseColor.setValue(1, 1, 1);

		d->metaInfo = nullptr;
		SAFE_DELETE(d->sliceConXY)
		SAFE_DELETE(d->bfContainer);

		SAFE_DELETE(d->bufferManager);
		SAFE_DELETE(d->tfCanvas);

		if (nullptr != d->tfCanvasWidget->layout()) {
			QLayoutItem* child;
			while ((child = d->tfCanvasWidget->layout()->takeAt(0)) != nullptr) {
				child->widget()->hide();
				d->tfCanvasWidget->layout()->removeItem(child);
				SAFE_DELETE(child);
			}
			delete d->tfCanvasWidget->layout();
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->resetOnLoad();
			}
		}

		return true;
	}

	auto OIVViewer::SetRootNode(SoNode* node, SoSeparator* node2d[3]) const -> void {
		Q_UNUSED(node)
		Q_UNUSED(node2d)
	}

	auto OIVViewer::SetSceneID(const int& id) const -> void {
		d->sceneID = id;
	}

	auto OIVViewer::SetTFWidget(QWidget* widget) const -> void {
		d->tfCanvasWidget = widget;
	}

	auto OIVViewer::SetMeasureToolSceneGraph(SoSeparator* measureRoot) -> void {
		d->sliceRenderRoot[0]->addChild(measureRoot);
	}

	auto OIVViewer::ResetLayout(const int& type) -> void {
		auto layoutType = d->layoutType;

		if (-1 < type) {
			layoutType = Entity::LayoutType::_from_integral(type);
		}

		d->layoutView->SetLayoutType(layoutType);

		for (auto window : d->qOiv2DRenderWindow) {
			if (nullptr != window) {
				window->reset2DView();
			}
		}
	}

	auto OIVViewer::SaveLayout(const QString& path) const -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		return d->layoutView->SaveLayout(path);
	}

	auto OIVViewer::LoadLayout(const QString& path) const -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		if (false == d->layoutView->LoadLayout(path)) {
			return false;
		}

		d->layoutType = d->layoutView->GetLayoutType();

		return true;
	}

	auto OIVViewer::GetCurrentLayoutType(void) const -> int {
		if (nullptr == d->layoutView) {
			return -1;
		}

		return d->layoutView->GetLayoutType()._to_integral();
	}

	void OIVViewer::onModifyROI(bool finish) {
		if (!d->roiCropper[0]) {
			return;
		}
		if (finish) {
			d->qOiv2DRenderWindow[0]->toggleNavigation(true);
			d->roiCropper[0]->ToggleModify(false);
		} else {
			d->qOiv2DRenderWindow[0]->toggleNavigation(false);
			d->roiCropper[0]->ToggleAdd(false);
			d->roiCropper[0]->ToggleModify(true);
			d->roiSwitch[0]->whichChild = 0;
		}
	}

	void OIVViewer::onROIModified(int idx) {
		if (!d->roiCropper[0]) {
			return;
		}
		auto roi_list = d->roiCropper[0]->GetROIs();
		auto modified_roi = roi_list[idx];

		float xmin = static_cast<float>(INT_MAX);
		float xmax = -static_cast<float>(INT_MAX);
		float ymin = static_cast<float>(INT_MAX);
		float ymax = -static_cast<float>(INT_MAX);
		for (auto i = 0; i < modified_roi->vertex.getNum(); i++) {
			auto pt = modified_roi->vertex.getValues(0)[i];
			if (xmin > pt[0])
				xmin = pt[0];
			if (xmax < pt[0])
				xmax = pt[0];
			if (ymin > pt[1])
				ymin = pt[1];
			if (ymax < pt[1])
				ymax = pt[1];
		}
		auto xhalf = d->metaInfo->data.data3D.resolutionX * d->metaInfo->data.data3D.sizeX / 2.0;
		auto yhalf = d->metaInfo->data.data3D.resolutionY * d->metaInfo->data.data3D.sizeY / 2.0;
		xmin += xhalf;
		xmax += xhalf;
		ymin += yhalf;
		ymax += yhalf;
		emit sigModifyRoi(idx, xmin, xmax, ymin, ymax);
	}

	void OIVViewer::onActivateROI() {
		if (!d->roiCropper[0]) {
			return;
		}
		if (false == d->metaInfo->data.data3D.exist) {
			emit sigNewRoi(-1, -1, -1, -1);
		}
		d->qOiv2DRenderWindow[0]->toggleNavigation(false);
		d->roiCropper[0]->ToggleAdd(true);
		d->roiCropper[0]->ToggleModify(false);
		d->roiSwitch[0]->whichChild = 0;
	}

	void OIVViewer::onROIAdded() {
		if (!d->roiCropper[0]) {
			return;
		}
		d->qOiv2DRenderWindow[0]->toggleNavigation(true);
		d->roiCropper[0]->ToggleAdd(false);

		auto roi_list = d->roiCropper[0]->GetROIs();

		auto last_roi = roi_list.last();

		float xmin = static_cast<float>(INT_MAX);
		float xmax = -static_cast<float>(INT_MAX);
		float ymin = static_cast<float>(INT_MAX);
		float ymax = -static_cast<float>(INT_MAX);
		for (auto i = 0; i < last_roi->vertex.getNum(); i++) {
			auto pt = last_roi->vertex.getValues(0)[i];
			if (xmin > pt[0])
				xmin = pt[0];
			if (xmax < pt[0])
				xmax = pt[0];
			if (ymin > pt[1])
				ymin = pt[1];
			if (ymax < pt[1])
				ymax = pt[1];
		}
		auto xhalf = d->metaInfo->data.data3D.resolutionX * d->metaInfo->data.data3D.sizeX / 2.0;
		auto yhalf = d->metaInfo->data.data3D.resolutionY * d->metaInfo->data.data3D.sizeY / 2.0;
		xmin += xhalf;
		xmax += xhalf;
		ymin += yhalf;
		ymax += yhalf;

		emit sigNewRoi(xmin, xmax, ymin, ymax);
	}

	void OIVViewer::onFocusROI(int roiIdx) {
		if (!d->roiCropper[0]) {
			return;
		}
		d->roiCropper[0]->SetFocus(roiIdx);
	}

	void OIVViewer::onClearROI() {
		if (!d->roiCropper[0]) {
			return;
		}
		d->roiCropper[0]->ClearROIs();
	}

	void OIVViewer::onRemoveROI(int roiIdx) {
		if (!d->roiCropper[0]) {
			return;
		}
		d->roiCropper[0]->RemoveROI(roiIdx);
	}

	void OIVViewer::onToggleROI(bool showROI) {
		if (!d->roiSwitch[0]) {
			return;
		}
		if (showROI) {
			d->roiSwitch[0]->whichChild = 0;
		} else {
			d->roiSwitch[0]->whichChild = 1;
		}
	}

	void OIVViewer::onCropROI(int roiIdx) {
		if (!d->roiCropper[0]) {
			return;
		}
		//Crop ROI & link array buffer to 3D visualizer		
		auto roiList = d->roiCropper[0]->GetROIs();
		if (roiList.count() <= roiIdx) {
			return;
		}
		auto roi = roiList[roiIdx];
		auto volDataXY = d->bufferManager->getHTXYSlice(0);

		auto htROI = RoiXY();

		if (volDataXY) {
			int xMin = INT_MAX;
			int xMax = -INT_MAX;
			int yMin = INT_MAX;
			int yMax = -INT_MAX;
			for (auto i = 0; i < 4; i++) {
				auto val = roi->vertex.getValues(0)[i];
				auto coordXYZ = volDataXY->XYZToVoxel(val);
				if (static_cast<float>(xMin) > coordXYZ[0])
					xMin = static_cast<int>(coordXYZ[0]);
				if (static_cast<float>(xMax) < coordXYZ[0])
					xMax = static_cast<int>(coordXYZ[0]);
				if (static_cast<float>(yMin) > coordXYZ[1])
					yMin = static_cast<int>(coordXYZ[1]);
				if (static_cast<float>(yMax) < coordXYZ[1])
					yMax = static_cast<int>(coordXYZ[1]);
			}

			htROI.setOffset(xMin, yMin);
			htROI.setSize(xMax - xMin + 1, yMax - yMin + 1);
		}

		SoVolumeData* volDataFL = nullptr;

		auto flROI = RoiXY();

		for (auto i = 0; i < 3; i++) {
			if (d->metaInfo->data.data3DFL.valid[i]) {
				volDataFL = d->bufferManager->getFLXYSlice(0, i);
				break;
			}
		}

		if (volDataFL) {
			int xMin = INT_MAX;
			int xMax = -INT_MAX;
			int yMin = INT_MAX;
			int yMax = -INT_MAX;
			for (auto i = 0; i < 4; i++) {
				auto val = roi->vertex.getValues(0)[i];
				auto coordXYZ = volDataFL->XYZToVoxel(val);
				if (static_cast<float>(xMin) > coordXYZ[0])
					xMin = static_cast<int>(coordXYZ[0]);
				if (static_cast<float>(xMax) < coordXYZ[0])
					xMax = static_cast<int>(coordXYZ[0]);
				if (static_cast<float>(yMin) > coordXYZ[1])
					yMin = static_cast<int>(coordXYZ[1]);
				if (static_cast<float>(yMax) < coordXYZ[1])
					yMax = static_cast<int>(coordXYZ[1]);
			}

			flROI.setOffset(xMin, yMin);
			flROI.setSize(xMax - xMin + 1, yMax - yMin + 1);
		}
		emit sigRoiCrop(htROI, flROI);
	}

	void OIVViewer::onRowRes(bool inProgress) { }

	void OIVViewer::onAddTransferFunction() {
		if (nullptr == d->tfCanvas) {
			return;
		}

		Entity::TFItemList list;
		for (auto i = 0; i < d->tfCanvas->getColorList().size(); i++) {
			auto item = std::make_shared<Entity::TFItem>();

			item->intensityMin = d->tfCanvas->getMinList()[i];
			item->intensityMax = d->tfCanvas->getMaxList()[i];
			item->gradientMin = d->tfCanvas->getGradMinList()[i];
			item->gradientMax = d->tfCanvas->getGradMaxList()[i];
			item->color.red = d->tfCanvas->getColorList()[i].redF() * 255;
			item->color.green = d->tfCanvas->getColorList()[i].greenF() * 255;
			item->color.blue = d->tfCanvas->getColorList()[i].blueF() * 255;
			item->color.alpha = d->tfCanvas->getTransparencyList()[i] * 255;;
			item->transparency = d->tfCanvas->getTransparencyList()[i];
			item->visible = d->tfCanvas->getHiddenList()[i];

			list.push_back(item);
		}
		emit transferFunctionChanged(list);
	}

	void OIVViewer::onSelectTFItem(int idx,
									double min, double max,
									double grad_min, double grad_max,
									double opacity) {
		Q_UNUSED(min)
		Q_UNUSED(max)
		Q_UNUSED(grad_min)
		Q_UNUSED(grad_max)
		Q_UNUSED(opacity)
		emit currentTransferFunctionChanged(idx);
	}

	void OIVViewer::onSelectTransferFunctionItem(int index) {
		if (nullptr == d->tfCanvas) {
			return;
		}

		d->tfCanvas->setCurrentItem(index);
	}

	void OIVViewer::onTransferFunctionHistogramChanged(int index) const {
		if (nullptr == d->tfCanvas) {
			return;
		}

		d->tfCanvas->ChangeHistogram(index);
	}

	void OIVViewer::onColormapChanged(int colormap_idx) {
		//0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->scalarBar[i]) {
				//d->scalarBar[i]->setInverse(colormap_idx == 1);
				const auto tf = d->scalarBar[i]->GetTransferFunction();
				if (nullptr == tf)
					return;

				//default color is white
				d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);
				switch (colormap_idx) {
					case 0:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
						break;
					case 1:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
					//change label color as black
						d->scaleBar[i]->SetColor(0.f, 0.f, 0.f);
						break;
					case 2:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
						break;
					case 3:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
						break;
					case 4:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
						break;
				}
			}
		}
	}

	void OIVViewer::onLevelChanged(float ri_min, float ri_max) {
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (nullptr != d->sliceRenderRoot[i]) {
				auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[i].ptr(), "HTSliceRange");
				if (range) {
					range->min = ri_min / d->htValueDiv - d->htValueOffset;
					range->max = ri_max / d->htValueDiv - d->htValueOffset;
					d->curLevelMin = ri_min;
					d->curLevelMax = ri_max;
				}
			}
			if (nullptr != d->scalarBar[i]) {
				d->scalarBar[i]->SetImageWindow(ri_min / 10000.0, ri_max / 10000.0);
			}
		}
		emit sigLevelChanged(ri_min, ri_max);
	}

	void OIVViewer::onMoveSlice(float value) {
		if (nullptr == d->layoutView) {
			return;
		}
		auto sender = QObject::sender();
		auto renderWidget = (QWidget*)sender;


		MedicalHelper::Axis axis = MedicalHelper::Axis::AXIAL;
		int widgetIndex = -1;

		const auto activatedWidgetType = d->layoutView->FindWidgetType(renderWidget);

		switch (activatedWidgetType) {
			case Entity::ViewType::XY2D:
				axis = MedicalHelper::Axis::AXIAL;
				widgetIndex = 0;
				break;
			case Entity::ViewType::YZ2D:
				axis = MedicalHelper::Axis::SAGITTAL;
				widgetIndex = 1;
				break;
			case Entity::ViewType::ZX2D:
				axis = MedicalHelper::Axis::CORONAL;
				widgetIndex = 2;
				break;
			default: ;
		}

		int currentSlice = -1;

		if ((d->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(d->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			if (axis == MedicalHelper::Axis::AXIAL) {
				currentSlice = d->bufferManager->getHTSliceIndexZ() + 1;
			} else if (axis == MedicalHelper::Axis::SAGITTAL) {
				currentSlice = d->bufferManager->getHTSliceIndexX() + 1;
			} else {
				currentSlice = d->bufferManager->getHTSliceIndexY() + 1;
			}
		} else if ((d->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume ||
			(d->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			if (false == d->metaInfo->data.data3D.exist) {
				if (axis == MedicalHelper::Axis::AXIAL) {
					currentSlice = d->bufferManager->getFLSliceIndexZ() + 1;
				} else if (axis == MedicalHelper::Axis::SAGITTAL) {
					currentSlice = d->bufferManager->getFLSliceIndexX() + 1;
				} else {
					currentSlice = d->bufferManager->getFLSliceIndexY() + 1;
				}
			} else {
				if (axis == MedicalHelper::Axis::AXIAL) {
					currentSlice = d->bufferManager->getHTSliceIndexZ() + 1;
				} else if (axis == MedicalHelper::Axis::SAGITTAL) {
					currentSlice = d->bufferManager->getHTSliceIndexX() + 1;
				} else {
					currentSlice = d->bufferManager->getHTSliceIndexY() + 1;
				}
			}
		}
		if (0 < value) {
			//currentSlice += pow(2,d->curRes);
			currentSlice++;
		} else if (0 > value) {
			//currentSlice -= pow(2, d->curRes);
			currentSlice--;
		}

		emit sliceIndexChanged(widgetIndex, currentSlice);
	}

	void OIVViewer::onDirectionChanged(TC::DirectionType type) {
		return;
		int axis = -1;

		switch (type) {
			case TC::DirectionType::X:
				axis = 0;
				break;
			case TC::DirectionType::Y:
				axis = 1;
				break;
			case TC::DirectionType::Z:
				axis = 2;
				break;
		}
	}

	void OIVViewer::onSimulate(QList<TC::RecordAction*> process) {
		d->simulating = true;

		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen("", TC::RecordType::None, process);
		});

		d->simulationWatcher.setFuture(future);
	}

	void OIVViewer::onStopSimulate() const {
		d->simulating = false;
	}

	void OIVViewer::onFinishedSimulate() {
		d->simulating = false;
		emit finishSimulate();
	}

	void OIVViewer::onStartRecord(QString path, TC::RecordType type, QList<TC::RecordAction*> process) {
		if (true == path.isEmpty()) {
			return;
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->qOiv2DRenderWindow[i]->setSettingIconViz(false);
		}

		d->recordBuffer.clear();
		d->recordBufferXY.clear();
		d->recordSavePath = path;

		d->simulating = true;

		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen(path, type, process);
		});

		d->recordWatcher.setFuture(future);
	}

	void OIVViewer::onFinishedRecord() {
		QString text;
		if (true == d->recordWatcher.result()) {
			text = QString("The recorded video file has been saved successfully.\n%1").arg(d->recordSavePath);
		} else {
			text = QString("Failed to save recorded video file.");
		}
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->qOiv2DRenderWindow[i]->setSettingIconViz(true);
		}

		auto toastMessageBox = new TC::ToastMessageBox(nullptr);
		toastMessageBox->Show(text);
	}

	void OIVViewer::onCaptureScreen(int type) {
		QMutexLocker locker(&d->simulationMutex);

		switch (type) {
			case 0: {
				d->qOiv2DRenderWindow[0]->reset2DView();
				d->recordBuffer.push_back(d->qOiv2DRenderWindow[0]->getRenderBuffer());
				break;
			}
			case 4: {
				d->qOiv2DRenderWindow[0]->reset2DView();
				d->recordBufferXY.push_back(d->qOiv2DRenderWindow[0]->getRenderBuffer(1));
				break;
			}
			default: ;
		}
	}

	//private
	auto OIVViewer::InitUI(void) -> bool {
		this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

		// init layout		
		d->layoutView = new LayoutView(nullptr);
		d->levelWindow = new TC::LevelWindow;

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->qOiv2DRenderWindow[i] = new ViewerRenderWindow2d(d->layoutView);
			auto n2 = QString("TwoD%1").arg(i);
			d->qOiv2DRenderWindow[i]->setName(n2);
			if (i == 0) {
				d->qOiv2DRenderWindow[i]->setRIPick(true);
			}
			d->qOiv2DRenderWindow[i]->setRenderWindowID(0);
			d->qOiv2DRenderWindow[i]->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,
																QSizePolicy::Expanding));
			d->qOiv2DRenderWindow[i]->forceArrowCursor(true);
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigWheel(float)), this, SLOT(onMoveSlice(float)), Qt::UniqueConnection);
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigMouseMove()), this, SLOT(mouseMoveIn2D()));
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigColorMap(int)), this, SLOT(onColormapChanged(int)));
			d->layoutView->AddWidget(Entity::ViewType::_from_integral(i), d->qOiv2DRenderWindow[i]);
		}
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigScaleTick(int)), this, SLOT(onScaleBarTick(int)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigScaleLength(int)), this, SLOT(onScaleBarLength(int)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigTimeStampColor(QColor)), this, SIGNAL(sigTimeColor(QColor)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigVoxelInfo(TCVoxelInfo)), this, SLOT(onVoxelInfo(TCVoxelInfo)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigZoomFactor(float)), this, SIGNAL(sigZoomMeasureHandle(float)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigLargeLength(float)), this, SIGNAL(sigVolumeMaxLen(float)));
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigResolution(int,float)), this, SLOT(OnResolutionChanged(int,float)));

		connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdateCall()));

		auto layout = new QHBoxLayout(this);

		auto botLayout = new QVBoxLayout(this);

		layout->addLayout(botLayout);
		botLayout->addWidget(d->layoutView, 1);

		d->voxelInfo = new VoxelInfoWidget;

		botLayout->addWidget(d->voxelInfo);

		connect(d->levelWindow, SIGNAL(levelChanged(float, float)), this, SLOT(onLevelChanged(float, float)));

		layout->addWidget(d->levelWindow);

		this->setLayout(botLayout);

		connect(&d->simulationWatcher, SIGNAL(finished()), this, SLOT(onFinishedSimulate()));

		connect(this, SIGNAL(captureScreen(int)), this, SLOT(onCaptureScreen(int)), Qt::BlockingQueuedConnection);
		connect(&d->recordWatcher, SIGNAL(finished()), this, SLOT(onFinishedRecord()));

		show();

		return true;
	}

	void OIVViewer::OnResolutionChanged(int res, float zoomFactor) {
		if (d->bufferManager) {
			if (res > 30) {
				return;
			}
			d->curRes = res;
			d->bufferManager->setFixedResolution(d->curRes,false);
			/*if (zoomFactor>1) {
				//d->bufferManager->enableFixedResolution(false);
				d->bufferManager->setUpperResThreshold(0);
			}
			else {
				//d->bufferManager->enableFixedResolution(true);				
				//d->bufferManager->setFixedResolution(d->curRes);
				d->bufferManager->setUpperResThreshold(d->curRes);
			}*/
			emit sigResChange(d->curRes);
		}
	}

	void OIVViewer::onUpdateCall() {
		this->Update();
	}

	void OIVViewer::mouseMoveIn2D() {
		if (d->isIn3D) {
			d->isIn3D = false;
		}
	}

	void OIVViewer::mouseMoveIn3D() {
		if (false == d->isIn3D) {
			d->isIn3D = true;
		}
	}

	void OIVViewer::SetDeviceSize(int size) {
		if (d->htxInfo) {
			d->htxInfo->fontSize.setValue(size);
		}
	}

	void OIVViewer::SetDeviceColor(QColor color) {
		if (d->deviceMatl) {
			d->deviceMatl->diffuseColor.setValue(color.redF(), color.greenF(), color.blueF());
		}
	}

	void OIVViewer::SetGammaColormap(int colormap_idx, bool isGamma, double gamma) {
		//0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			//change colormap of render window;
			const auto tf = MedicalHelper::find<SoTransferFunction>(d->sliceRenderRoot[i].ptr(), "colormap2D_TF");
			if (nullptr == tf) {
				continue;
			}
			d->ApplyColormap(tf, colormap_idx, isGamma, gamma);

			d->isGamma = isGamma;
			d->gamma = gamma;

			if (d->scalarBar[i]) {
				//change colormap of scalarbar
				const auto bartf = d->scalarBar[i]->GetTransferFunction();
				if (nullptr == bartf) {
					continue;
				}
				//default color is white
				d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);
				d->ApplyColormap(bartf, colormap_idx, isGamma, gamma);
				if (1 == colormap_idx) {
					d->scaleBar[i]->SetColor(0.f, 0.f, 0.f);
				}
			}
		}
	}

	void OIVViewer::SetVisibleMinimap(bool isVisible) {
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->minimapSwitch[i]) {
				if (isVisible) {
					d->minimapSwitch[i]->whichChild = 0;
				} else {
					d->minimapSwitch[i]->whichChild = -1;
				}
			}
		}
	}

	void OIVViewer::SetVisibleScalarBar(bool isVisible) {
		auto ds = GetSceneDS();
		d->visibleScalarBar = isVisible;
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->scalarBarSwitch[i]) {
				if (isVisible) {
					if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
						|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
						d->scalarBarSwitch[i]->whichChild = -3;
					}
				} else {
					d->scalarBarSwitch[i]->whichChild = -1;
				}
			}
		}
	}

	void OIVViewer::SetVisibleScaleBar(bool isVisible) {
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->scaleBarSwitch[i]) {
				if (isVisible) {
					d->scaleBarSwitch[i]->whichChild = -3;
				} else {
					d->scaleBarSwitch[i]->whichChild = -1;
				}
			}
		}
	}

	void OIVViewer::SetVisibleDevice(bool isVisible) {
		if (d->deviceInfoSwitch) {
			if (isVisible) {
				d->deviceInfoSwitch->whichChild = -3;
			} else {
				d->deviceInfoSwitch->whichChild = -1;
			}
		}
	}

	auto OIVViewer::RefreshVoxelInfo() -> void {
		auto info = d->qOiv2DRenderWindow[0]->GetCurPosInfo();
		if (false == info.valid) {
			d->voxelInfo->ToggleVisible(false);
			return;
		}
		d->voxelInfo->ToggleVisible(true);
		auto ds = GetSceneDS();
		double phyZ;
		if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
			phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex());
		} else if ((ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume) {
			phyZ = d->metaInfo->data.data3DFL.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex());
		} else if ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP || (ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			phyZ = 0;
		} else {
			d->voxelInfo->ToggleVisible(false);
			return;
		}

		for (auto i = 0; i < 3; i++) {
			if (false == d->curChVisiblePhy[i]) {
				info.flIntensity[i] = -1;
			}
		}

		d->voxelInfo->SetPosition(info.x, info.y, phyZ, d->curTimePoint);
		d->voxelInfo->SetRIValue(info.htIntensity / 10000.0);
		d->voxelInfo->SetFLValue(info.flIntensity[0], info.flIntensity[1], info.flIntensity[2]);
	}

	void OIVViewer::onVoxelInfo(TCVoxelInfo info) {
		if (false == info.valid) {
			d->voxelInfo->ToggleVisible(false);
			return;
		}
		d->voxelInfo->ToggleVisible(true);
		auto ds = GetSceneDS();
		double phyZ;
		if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
			phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex());
		} else if ((ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume) {
			phyZ = d->metaInfo->data.data3DFL.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex());
		} else if ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP || (ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			phyZ = 0;
		} else {
			d->voxelInfo->ToggleVisible(false);
			return;
		}

		for (auto i = 0; i < 3; i++) {
			if (false == d->curChVisiblePhy[i]) {
				info.flIntensity[i] = -1;
			}
		}

		d->voxelInfo->SetPosition(info.x, info.y, phyZ, d->curTimePoint);
		d->voxelInfo->SetRIValue((d->htValueOffset * 10 + info.htIntensity * d->htValueDiv) / 10000.0);
		d->voxelInfo->SetFLValue(info.flIntensity[0], info.flIntensity[1], info.flIntensity[2]);
	}

	void OIVViewer::onScaleBarLength(const int& value) {
		if (nullptr == d->qOiv2DRenderWindow[0])
			return;

		ValueDialog widget;
		widget.setModal(true);
		widget.SetValue(value);
		widget.SetTitle("Set scale bar length");
		widget.SetStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
		if (QDialog::Accepted != widget.exec())
			return;

		d->qOiv2DRenderWindow[0]->SetScaleLength(widget.GetValue());
	}

	void OIVViewer::onScaleBarTick(const int& value) {
		if (nullptr == d->qOiv2DRenderWindow[0])
			return;

		ValueDialog widget;
		widget.setModal(true);
		widget.SetValue(value);
		widget.SetTitle("Set scale bar tick");
		widget.SetStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
		if (QDialog::Accepted != widget.exec())
			return;

		d->qOiv2DRenderWindow[0]->SetScaleTick(widget.GetValue());
	}

	void OIVViewer::on3DBackgroundChanged(bool isWhite) {
		/*if (d->axisGridHT) {
			if (isWhite) {
				d->axisGridHT->SetColor(0, 0, 0);
			}else {
				d->axisGridHT->SetColor(0.8, 0.8, 0.8);
			}
		}
		if(d->axisGridFL) {
			if(isWhite) {
				d->axisGridFL->SetColor(0, 0, 0);
			}else {
				d->axisGridFL->SetColor(0.8, 0.8, 0.8);
			}
		}*/
		//DO not change color due to background
	}

	auto OIVViewer::InitOIV(void) -> bool {
		return true;
	}

	auto OIVViewer::InitRender(void) -> bool {
		for (int i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				return false;
			}
		}

		//init root separators
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->sliceRenderRoot[i] = new SoSeparator;
		}

		// init reader

		// init volume scene graph

		d->infoGroup = new SoGroup;
		d->infoGroup->setName("TCF_MetaData");

		d->timestampSwitch = new SoSwitch;

		d->annoMatl = new SoMaterial;
		d->annoMatl->diffuseColor.setValue(1, 1, 1);
		d->annoMatl->setName("TimeStampMat");

		d->annoText = new TextBox;
		d->annoText->setName("TimeStamp");
		d->annoText->position.setValue(0.0f, -0.95f, 0);
		d->annoText->alignmentH = TextBox::RIGHT;
		d->annoText->alignmentV = TextBox::BOTTOM;
		d->annoText->fontSize.setValue(20);
		d->annoText->border = FALSE;

		d->timestampSwitch->addChild(d->annoMatl.ptr());
		d->timestampSwitch->addChild(d->annoText.ptr());
		d->timestampSwitch->whichChild = -1;

		d->deviceInfoSwitch = new SoSwitch;

		d->deviceMatl = new SoMaterial;
		d->deviceMatl->diffuseColor.setValue(1, 1, 1);

		d->htxInfo = new TextBox;
		d->htxInfo->setName("HTX_Proj_Info");
		d->htxInfo->addLine("No X1 Device Info.");
		d->htxInfo->position.setValue(-0.95f, 0.95f, 0);
		d->htxInfo->alignmentH = TextBox::LEFT;
		d->htxInfo->alignmentV = TextBox::TOP;
		d->htxInfo->fontSize.setValue(20);
		d->htxInfo->border = FALSE;

		d->deviceInfoSwitch->addChild(d->deviceMatl.ptr());
		d->deviceInfoSwitch->addChild(d->htxInfo.ptr());


		// init slice scene graph		
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->sliceSwitch[i] = new SoSwitch;
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->whichChild = 0;

			d->sliceRenderRoot[i]->addChild(d->sliceSwitch[i].ptr());

			d->sliceRenderRoot[i]->addChild(d->infoGroup.ptr());
			d->sliceRenderRoot[i]->addChild(d->timestampSwitch.ptr());
			d->sliceRenderRoot[i]->addChild(d->deviceInfoSwitch.ptr());
		}
		d->sliceConXY = new TC::OivSceneXY;
		d->bfContainer = new Oiv2dBFSlice;

		// set slice scene graph to 2D render windows
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->qOiv2DRenderWindow[i]->setSceneGraph(d->sliceRenderRoot[i].ptr());
			d->qOiv2DRenderWindow[i]->refreshRangeSlider();
			d->qOiv2DRenderWindow[i]->hide();
		}

		//set window title info		
		d->qOiv2DRenderWindow[0]->setWindowTitle("XY");
		d->qOiv2DRenderWindow[0]->setWindowTitleColor(0.8f, 0.2f, 0.2f);
		d->qOiv2DRenderWindow[0]->setRIPick(false);//disable RI Picker (No Volume Rendering in 2D Viewer)

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->scaleBarSwitch[i] = new SoSwitch;
			d->scalarBarSwitch[i] = new SoSwitch;
			d->manipulatorRoot[i] = new SoSeparator;
			d->roiSwitch[i] = new SoSwitch;
			d->roiSwitch[i]->setName(("RoiCropSwitch" + std::to_string(i)).c_str());
			d->minimapSep[i] = new SoSeparator;
			d->minimapSep[i]->setName(("MinimapSep" + std::to_string(i)).c_str());
			d->minimapSwitch[i] = new SoSwitch;
			d->minimapSwitch[i]->setName("MinimapSwitch");
			d->minimapSwitch[i]->whichChild = -1;
			d->minimapSwitch[i]->addChild(d->minimapSep[i].ptr());
		}

		return true;
	}

	auto OIVViewer::BuildUp(const int& modality) -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.000001;
		};
		if (!d->isBuildScene) {
			BuildHiddenScene();
			Build3DScene();
			Build2DScene();
			d->isBuildScene = true;
			auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
			auto metaInfo = metaReader->Read(d->filePath);
			auto dimSame = false;
			auto resSame = false;
			if (metaInfo->data.data3D.exist && metaInfo->data.data3DFL.exist) {
				if (AreSame(metaInfo->data.data3D.resolutionX, metaInfo->data.data3DFL.resolutionX)) {
					if (AreSame(metaInfo->data.data3D.resolutionY, metaInfo->data.data3DFL.resolutionY)) {
						if (AreSame(metaInfo->data.data3D.resolutionZ, metaInfo->data.data3DFL.resolutionZ)) {
							resSame = true;
						}
					}
				}
				if (metaInfo->data.data3D.sizeX == metaInfo->data.data3DFL.sizeX) {
					if (metaInfo->data.data3D.sizeY == metaInfo->data.data3DFL.sizeY) {
						if (metaInfo->data.data3D.sizeZ == metaInfo->data.data3DFL.sizeZ) {
							dimSame = true;
						}
					}
				}
			}
			d->sliceConXY->SetUniform(dimSame && resSame);
		}

		if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			if (false == this->BuildHTVolume()) {
				return false;
			}
			if (false == this->BuildHTSlice()) {
				return false;
			}
			if (false == this->BuildTFCanvas()) {
				return false;
			}
		}

		if (d->metaInfo->data.data3DFL.exist || d->metaInfo->data.data2DFLMIP.exist) {
			if (false == this->BuildFLVolume()) {
				return false;
			}

			if (false == this->BuildFLSlice()) {
				return false;
			}
		}
		//BF MIP modality Deprecated 21.07.28 Jose T. Kim

		if ((modality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
			if (false == this->BuildBF()) {
				return false;
			}
		}

		if (false == this->BuildCamera()) {
			return false;
		}

		if (false == this->BuildScaleBar()) {
			return false;
		}

		if (false == this->BuildScalarBar()) {
			return false;
		}

		if (false == this->BuildRoiCropper()) {
			return false;
		}

		return true;
	}

	auto OIVViewer::Build2DScene() const -> bool {
		if (nullptr == d->sliceConXY) {
			d->sliceConXY = new TC::OivSceneXY;
		}
		const auto steps = d->metaInfo->data.data3D.dataCount;

		auto isLDM = d->metaInfo->data.isLDM;
		d->sliceConXY->SetHiddenSep(d->hiddenScene->GetSceneGraph());
		d->sliceConXY->buildSceneGraph();
		d->sliceConXY->SetMetaInfo(d->metaInfo);
		d->sliceConXY->SetLDM(isLDM);
		d->sliceConXY->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		d->sliceSwitch[0]->replaceChild(0, d->sliceConXY->GetRenderRoot());

		return true;
	}

	auto OIVViewer::BuildHTSlice(void) const -> bool {
		if (true == d->isBuildHT2D) {
			return true;
		}

		if (nullptr == d->sliceConXY) {
			return false;
		}

		auto buf_step = 1;

		d->sliceInited = true;

		for (auto i = 0; i < buf_step; i++) {
			if (d->metaInfo->data.data2DMIP.exist) {
				d->sliceConXY->SetMIPVolume(d->bufferManager->getHT2dVolume(i), true);
			}
		}

		auto isLDM = d->metaInfo->data.isLDM;
		d->sliceConXY->SetLDM(isLDM);

		d->isBuildHT2D = true;

		return true;
	}

	auto OIVViewer::BuildFLSlice(void) const -> bool {
		if (true == d->isBuildFL2D) {
			return true;
		}
		if (nullptr == d->sliceConXY) {
			return false;
		}

		bool flChExist[3] = { false, };

		if (d->metaInfo->data.data3DFL.exist) {
			for (int i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					flChExist[i] = true;
				}
			}
		}

		d->sliceConXY->setFLExist(flChExist[0], flChExist[1], flChExist[2]);

		auto isLDM = d->metaInfo->data.isLDM;
		d->sliceConXY->SetLDM(isLDM);

		auto buf_step = 1;

		for (auto i = 0; i < buf_step; i++) {
			if (false == d->metaInfo->data.data2DFLMIP.exist) {
				continue;
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data2DFLMIP.valid[ch]) {
					continue;
				}
				d->sliceConXY->SetMIPFLVolume(ch, d->bufferManager->getFL2dVolume(i, ch), true);
			}
		}

		d->isBuildFL2D = true;

		return true;
	}

	auto OIVViewer::BuildHiddenScene() const -> bool {
		if (nullptr == d->hiddenScene) {
			d->hiddenScene = new HiddenScene;
		}
		d->hiddenScene->SetImageParentDir(d->curProjPath);
		return true;
	}

	auto OIVViewer::Build3DScene() const -> bool {
		return true;
	}

	auto OIVViewer::BuildHTVolume(void) -> bool {
		if (true == d->isBuildHT3D) {
			return true;;
		}

		auto buf_step = 1;

		//calculate 2D histogram		
		for (auto i = 0; i < buf_step; i++) {
			d->sliceConXY->SetVolume(d->bufferManager->getHTXYSlice(i), true);
		}

		d->isBuildHT3D = true;

		return true;
	}

	auto OIVViewer::SetCurrentProjectPath(const QString& path) -> void {
		d->curProjPath = path;
	}


	auto OIVViewer::BuildFLVolume(void) -> bool {
		if (true == d->isBuildFL3D) {
			return true;;
		}

		bool flChExist[3] = { false, };

		if (d->metaInfo->data.data3DFL.exist) {
			for (auto i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					flChExist[i] = true;
				}
			}
		}
		const bool isFLExist = flChExist[0] || flChExist[1] || flChExist[2];
		if (false == isFLExist) {
			return false;
		}

		auto buf_step = 1;

		for (auto i = 0; i < buf_step; i++) {
			for (auto ch = 0; ch < 3; ch++) {
				if (false == flChExist[ch]) {
					continue;
				}
				d->sliceConXY->SetFLVolume(ch, d->bufferManager->getFLXYSlice(i, ch), true);
			}
		}

		d->isBuildFL3D = true;

		return true;
	}

	auto OIVViewer::BuildBF(void) -> bool {
		//redesign 22.04.26		
		if (true == d->isBuildBF) {
			return true;
		}
		if (nullptr == d->bufferManager) {
			return false;
		}

		if (nullptr == d->bfContainer) {
			d->bfContainer = new Oiv2dBFSlice;
		}

		const int steps = d->metaInfo->data.dataBF.dataCount;
		auto buf_step = 1;

		d->bfContainer->buildBFSlice(steps);

		for (auto i = 0; i < buf_step; i++) {
			d->bfContainer->setSingleBF(d->bufferManager->getBF2dVolume(i, 0),
										d->bufferManager->getBF2dVolume(i, 1),
										d->bufferManager->getBF2dVolume(i, 2), i);
		}

		d->bfContainer->setTransparency(0.0);
		d->bfContainer->setTimeStep(0);

		d->sliceConXY->setBFRoot(d->bfContainer->getRenderRootNode());

		d->isBuildBF = true;

		return true;
	}

	auto OIVViewer::BuildTFCanvas(void) -> bool {
		if (true == d->isBuildTFCanvas) {
			return true;
		}

		if (false == d->isBuildHT2D) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			d->tfCanvas = new QTransferFunctionCanvas2D(nullptr);
			d->tfCanvas->setCurProjPath(d->curProjPath);
			auto layout = new QHBoxLayout();
			layout->setContentsMargins(0, 0, 0, 0);
			layout->addWidget(d->tfCanvas);
			d->tfCanvasWidget->setLayout(layout);

			//d->tfCanvas->setVolCon(d->volumeCon);			
			d->tfCanvas->setHidden(d->hiddenScene);

			connect(d->tfCanvas, SIGNAL(sigRender()), this, SLOT(onAddTransferFunction()));
			connect(d->tfCanvas, SIGNAL(sigSelectItem(int, double, double, double, double, double)),
					this, SLOT(onSelectTFItem(int, double, double, double, double, double)));
			connect(d->tfCanvas, SIGNAL(sigCanvasMouse(bool)), this, SLOT(onCanvasMouse(bool)));
		}

		auto min = -1.0;
		auto max = -1.0;
		auto gmin = -1.0;
		auto gmax = -1.0;

		d->tfCanvas->setDataXdecimation(4);
		d->tfCanvas->setDataXSingleStep(0.0001);
		d->tfCanvas->setDataXdivider(10000.0);
		d->tfCanvas->setDataXname("RI");
		d->tfCanvas->setDataXRange(min, max);

		d->tfCanvas->setDataYdecimation(2);
		d->tfCanvas->setDataYSingleStep(0.05);
		d->tfCanvas->setDataYdivider(1.0);
		d->tfCanvas->setDataYname("Gradient");

		d->hiddenScene->GetGradMinMax(gmin, gmax);
		d->tfCanvas->setDataYRange(gmin, gmax);

		d->isBuildTFCanvas = true;

		emit gradientRangeLoaded(gmin, gmax);

		return true;
	}

	auto OIVViewer::BuildCamera(void) const -> bool {
		if (true == d->isBuildCamera) {
			return true;
		}

		if (nullptr == d->bufferManager->getDefaultVolume()) {
			return false;
		}

		if (nullptr == d->sliceConXY) {
			return false;
		}

		// init camera
		MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,
									MedicalHelper::SAGITTAL,
									MedicalHelper::CORONAL
		};

		const auto volData = d->bufferManager->getDefaultVolume();

		if (nullptr != d->sliceSwitch[0]) {
			d->sliceSwitch[0]->replaceChild(0, d->sliceConXY->GetRenderRoot());
			auto vol = MedicalHelper::find<SoVolumeData>(d->sliceConXY->GetRenderRoot());
			MedicalHelper::orientView(ax[0], d->qOiv2DRenderWindow[0]->getCamera(), vol);
			auto name = std::string("Camera2D_0"); // _2D_Camera_i
			d->qOiv2DRenderWindow[0]->getCamera()->setName(name.c_str());
		}

		d->isBuildCamera = true;

		return true;
	}

	auto OIVViewer::BuildScaleBar(void) -> bool {
		if (true == d->isBuildScaleBar) {
			return true;
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				continue;
			}

			d->scaleBarSwitch[i]->whichChild = -3;

			d->scaleBar[i] = new TC::ScaleBarSwitch(d->qOiv2DRenderWindow[i]->getCamera());
			d->scaleBar[i]->SetPosition(0.95f, -0.95f);
			d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);
			d->scaleBar[i]->SetTick(5);
			d->scaleBarSwitch[i]->addChild(d->scaleBar[i]->GetRoot());

			d->manipulatorRoot[i]->setName(("Manip2D_" + std::to_string(0)).c_str());
			d->manipulatorRoot[i]->addChild(new SoSeparator);

			if (d->sliceRenderRoot[i]->findChild(d->manipulatorRoot[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->manipulatorRoot[i].ptr());
			}
			if (d->sliceRenderRoot[i]->findChild(d->scaleBarSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->scaleBarSwitch[i].ptr());
			}
		}

		d->isBuildScaleBar = true;

		return true;
	}

	auto OIVViewer::BuildRoiCropper() -> bool {
		if (true == d->isBuildRoiCropper) {
			return true;
		}
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				continue;
			}
			d->roiCropper[i] = new TC::RoiCropSep;
			connect(d->roiCropper[i], SIGNAL(roiAdded()), this, SLOT(onROIAdded()));
			connect(d->roiCropper[i], SIGNAL(roiModified(int)), this, SLOT(onROIModified(int)));

			d->roiSwitch[i]->whichChild = -1;
			d->roiSwitch[i]->addChild(d->roiCropper[i]->GetRoot());
			if (d->sliceRenderRoot[i]->findChild(d->roiSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->roiSwitch[i].ptr());
			}

			d->miniMap[i] = new OivMinimap;
			d->minimapSep[i]->addChild(d->miniMap[i].ptr());
			//if(d->sliceRenderRoot[i]->findChild(d->minimapSep[i])<0) {
			//d->sliceRenderRoot[i]->addChild(d->minimapSep[i]);
			//}
			if (d->sliceRenderRoot[i]->findChild(d->minimapSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->minimapSwitch[i].ptr());
			}
		}
		d->isBuildRoiCropper = true;
		return true;
	}

	auto OIVViewer::BuildScalarBar(void) -> bool {
		if (true == d->isBuildScalarBar) {
			return true;
		}

		auto ri_min = d->metaInfo->data.data3D.riMin * 10000.0;
		auto ri_max = d->metaInfo->data.data3D.riMax * 10000.0;

		auto tf = d->sliceConXY->getSliceTF();


		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				continue;;
			}

			auto wsize = d->qOiv2DRenderWindow[i]->getViewportRegion().getWindowSize();
			auto wmin = wsize[0];
			if (wmin > wsize[1])
				wmin = wsize[1];

			d->scalarBarSwitch[i]->whichChild = -1;
			d->scalarBarSwitch[i]->setName(("ScalarBarSwitch" + std::to_string(0)).c_str());
			d->scalarBarSep[i] = new SoSeparator;
			d->scalarBarSep[i]->setName(("ScalarBar" + std::to_string(0)).c_str());
			d->scalarBarSwitch[i]->addChild(d->scalarBarSep[i].ptr());

			d->scalarBar[i] = new TC::ScalarBarSwitch;
			auto tcFunc = new SoTransferFunction;
			tcFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			d->scalarBar[i]->SetTransferFunction(tcFunc);
			d->scalarBar[i]->SetMajorLength(4);
			d->scalarBar[i]->SetNumberOfAnnotation(2);
			d->scalarBar[i]->SetRatio(12);
			d->scalarBar[i]->SetHorizontal(false);
			d->scalarBar[i]->SetPosition(-0.95f, -0.95f);
			d->scalarBar[i]->SetSize(0.02f, 0.3f);
			d->scalarBar[i]->SetFontSize(13.0f);
			d->scalarBar[i]->SetLastAnnotationMargin(340);
			d->scalarBar[i]->SetPrecision(3);
			//d->scalarBar[i]->SetAnnotationDiv(10000.0);
			d->scalarBar[i]->GetRoot()->whichChild = 0;

			int dim[3];
			double res[3];
			dim[0] = d->metaInfo->data.data3D.sizeX;
			dim[1] = d->metaInfo->data.data3D.sizeY;
			dim[2] = d->metaInfo->data.data3D.sizeZ;
			res[0] = d->metaInfo->data.data3D.resolutionX;
			res[1] = d->metaInfo->data.data3D.resolutionY;
			res[2] = d->metaInfo->data.data3D.resolutionZ;

			d->scalarBarSep[i]->addChild(d->scalarBar[i]->GetRoot());
			//d->scalarBar[i]->SetImageWindow(d->metaInfo->data.data3D.riMin, d->metaInfo->data.data3D.riMax);
			d->scalarBar[i]->SetImageRange(d->metaInfo->data.data3D.riMin, d->metaInfo->data.data3D.riMax);

			if (d->sliceRenderRoot[i]->findChild(d->scalarBarSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->scalarBarSwitch[i].ptr());
			}
		}

		d->isBuildScalarBar = true;

		return true;
	}

	auto OIVViewer::SetHTMIP(const bool& isHTMIP) const -> void {
		if (true == d->isBuildHT2D) {
			d->sliceConXY->ShowHTMIP(isHTMIP);
			//d->qOiv2DRenderWindow[0]->reset2DView();
		}
	}

	auto OIVViewer::SetFLMIP(const bool& isFLMIP) const -> void {
		if (true == d->isBuildFL2D) {
			d->sliceConXY->ShowFLMIP(isFLMIP);
			//d->qOiv2DRenderWindow[0]->reset2DView();
		}
	}

	auto OIVViewer::SetMIP(const bool& isMIP) const -> void {
		d->sliceConXY->ShowMIP(isMIP);
		//d->qOiv2DRenderWindow[0]->reset2DView();
	}

	auto OIVViewer::Render(const Interactor::SceneDS::Pointer& ds) -> bool {
		if (nullptr == d->sliceConXY) {
			return false;
		}

		if (nullptr == d->bufferManager) {
			return false;
		}


		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->sliceSwitch[i]) {
				return false;
			}
		}

		// set overlay
		if (d->isOverlay != ds->isOverlay) {
			d->sliceConXY->SetHTOverlay(ds->isOverlay);
		}
		d->isOverlay = ds->isOverlay;

		// set SoSwitch node
		d->volumeChildIndex = -1;
		d->sliceChildIndex = -1;
		d->sliceTraversalMode = SoMultiSwitch::INCLUDE;

		auto file = GetFileDS();
		d->sliceConXY->toggleBF(false);
		if (((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume &&
				(ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)
			|| ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP &&
				(ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
			d->levelWindow->SetSpaceOnly(false);
			d->volumeChildIndex = SO_SWITCH_ALL;

			d->sliceTraversalMode = SoMultiSwitch::EXCLUDE;
			d->sliceChildIndex = BF_INDEX;

			d->sliceConXY->ToggleHTIntensity(true);

			d->childIndex = SO_SWITCH_ALL;

			const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;			
			
			d->bufferManager->setHTSliceIndexZ(absoluteSliceIndexZ);
			auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2);
			auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
			auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 0.5) - offsetZ;
			auto idxFLz = static_cast<int>(round(phyZ / resZ));

			if (resZ < 0.01 || d->metaInfo->data.data3DFL.sizeZ == 1) {//exception for only on slice
				idxFLz = -1;
				auto phyComp = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex()) - offsetZ;
				if (phyComp < d->metaInfo->data.data3D.resolutionZ && phyComp > 0) {
					idxFLz = 0;					
				}
			}
			if (idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1) {
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, false);
					}
					if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
						d->curChVisiblePhy[i] = false;
					} else {
						d->curChVisiblePhy[i] = true;
					}
				}
			} else {
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, d->curChVisible[i]);
					}
					d->curChVisiblePhy[i] = true;
				}
				d->bufferManager->setFLSliceIndexZ(idxFLz);
			}
			if ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				d->sliceConXY->SetSliceIndexHT(0);
			}else if(d->metaInfo->data.isLDM) {
				d->sliceConXY->SetSliceIndexHT(ds->viewConfigList[0]->GetSliceIndex() - 1);
				d->sliceConXY->SetSliceIndexHTFL(idxFLz);
			}else {
				d->sliceConXY->SetSliceIndexHT(0);
			}
			if ((ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data2DFLMIP.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, true);
					}
				}				
			}
		} else if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
			|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			d->levelWindow->SetSpaceOnly(false);
			d->volumeChildIndex = HT_INDEX;
			d->sliceChildIndex = HT_INDEX;

			d->sliceConXY->ToggleHTIntensity(true);

			d->bufferManager->setHTSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
			if (d->metaInfo->data.isLDM) {
				const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;
				const auto htTileSizeZ = d->metaInfo->data.data3D.tileSizeZ;
				const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / htTileSizeZ));
				d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ * htTileSizeZ); //For OIV > 2023.1
				//d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ); //For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHT(absoluteSliceIndexZ);
			} else {
				d->sliceConXY->SetSliceIndexHT(0);
				auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2);
				auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
				auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 0.5) - offsetZ;
				auto idxFLz = static_cast<int>(round(phyZ / resZ));

				if ((idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1) && (ds->activatedModality & Entity::Modality::HTMIP) != Entity::Modality::HTMIP) {
					for (auto i = 0; i < 3; i++) {
						d->curChVisiblePhy[i] = false;
					}
				} else {
					for (auto i = 0; i < 3; i++) {
						d->curChVisiblePhy[i] = true;
					}
				}
			}
		} else if ((ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume
			|| (ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			d->levelWindow->SetSpaceOnly(true);
			d->volumeChildIndex = FL_INDEX;
			d->sliceChildIndex = FL_INDEX;
			for (auto i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					d->sliceConXY->ToggleFLChannel(i, d->curChVisible[i]);
				}
			}
			if (d->metaInfo->data.data3D.exist) {
				d->bufferManager->setHTSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
				auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2);
				auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
				auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 0.5) - offsetZ;
				auto idxFLz = static_cast<int>(round(phyZ / resZ));

				if (resZ < 0.01 || d->metaInfo->data.data3DFL.sizeZ == 1) {//exception for only on slice
					idxFLz = -1;
					auto phyComp = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex()) - offsetZ;
					if (phyComp < d->metaInfo->data.data3D.resolutionZ && phyComp > 0) {
						idxFLz = 0;
					}
				}

				if (idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1) {
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConXY->ToggleFLChannel(i, false);
						}
						d->curChVisiblePhy[i] = false;
					}
				} else {
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConXY->ToggleFLChannel(i, d->curChVisible[i]);
						}
						d->curChVisiblePhy[i] = true;
					}
					d->bufferManager->setFLSliceIndexZ(idxFLz);
				}
				if (d->metaInfo->data.isLDM) {
					const auto absoluteSliceIndexZ = idxFLz;
					const auto flTileSizeZ = d->metaInfo->data.data3DFL.tileSizeZ;
					const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / flTileSizeZ));
					d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ * flTileSizeZ);//For OIV > 2023.1
					//d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ);//For OIV > 2023.2
					//d->sliceConXY->SetSliceIndexHTFL(absoluteSliceIndexZ);
				} else {
					d->sliceConXY->SetSliceIndexHTFL(0);
				}
			} else {
				d->bufferManager->setFLSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
				if (d->metaInfo->data.isLDM) {
					const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;
					const auto flTileSizeZ = d->metaInfo->data.data3DFL.tileSizeZ;
					const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / flTileSizeZ));
					d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ * flTileSizeZ); //For OIV > 2023.1
					//d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ); //For OIV > 2023.2
					//d->sliceConXY->SetSliceIndexHTFL(absoluteSliceIndexZ);
				} else {
					d->sliceConXY->SetSliceIndexHTFL(0);
				}
			}
			d->sliceConXY->ToggleHTIntensity(false);
			if ((ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data2DFLMIP.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, true);
					}
				}
			}
		} else if ((ds->activatedModality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
			d->levelWindow->SetSpaceOnly(true);
			d->volumeChildIndex = -1;
			d->sliceChildIndex = BF_INDEX;
			d->sliceConXY->toggleBF(true);
		}

		d->timestampSwitch->whichChild = -1;

		SetTimeStep(ds->timelapseTime);
		const auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[0].ptr(), "HTSliceRange");
		if (range) {
			range->min = d->curLevelMin / d->htValueDiv - d->htValueOffset;
			range->max = d->curLevelMax / d->htValueDiv - d->htValueOffset;
		}
		//Time stamp
		if (ds->visibleTimeStamp) {
			d->timestampSwitch->whichChild = -3;
			d->annoText->fontSize = ds->timestampSize;
			auto rr = ds->timestampR / 255.0;
			auto gg = ds->timestampG / 255.0;
			auto bb = ds->timestampB / 255.0;
			d->annoMatl->diffuseColor.setValue(rr, gg, bb);
		}

		//ScalarBar
		if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
			|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			if (d->visibleScalarBar) {
				d->scalarBarSwitch[0]->whichChild = -3;
			}
		} else {
			d->scalarBarSwitch[0]->whichChild = -1;
		}

		return true;
	}

	auto OIVViewer::SetTimeStep(const double& timeIndex) -> void {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.00001;
		};
		d->curTimePoint = timeIndex;
		//find index of each modality
		auto htIndex = -1;
		auto ht_time = d->metaInfo->data.data3D.timePoints;
		for (auto i = 0; i < ht_time.size(); i++) {
			if (AreSame(ht_time[i], timeIndex)) {
				htIndex = i;
			}
		}
		int flIndex[3] = { -1, -1, -1 };
		for (auto ch = 0; ch < 3; ch++) {
			auto fl_time = d->metaInfo->data.data3DFL.timePoints[ch];
			for (auto i = 0; i < fl_time.size(); i++) {
				if (AreSame(fl_time[i], timeIndex)) {
					flIndex[ch] = i;
				}
			}
		}

		auto bfIndex = -1;
		auto bf_time = d->metaInfo->data.dataBF.timePoints;
		for (auto i = 0; i < bf_time.size(); i++) {
			if (AreSame(bf_time[i], timeIndex)) {
				bfIndex = i;
			}
		}

		auto totalIndex = -1;
		auto total_time = d->metaInfo->data.total_time;
		for (auto i = 0; i < total_time.size(); i++) {
			if (AreSame(total_time[i], timeIndex)) {
				totalIndex = i;
			}
		}
		const int hour = static_cast<int>(timeIndex / 3600.0);
		auto numberTxt = QString::number(hour);
		if (numberTxt.length() == 1) {
			numberTxt = "0" + numberTxt;
		}

		const int minHour = static_cast<int>(total_time.first() / 3600.0);
		auto minTxt = QString::number(minHour);
		if (minTxt.length() == 1) {
			minTxt = "0" + minTxt;
		}
		auto minTck = minTxt + ":" + QDateTime::fromTime_t(total_time.first()).toUTC().toString("mm:ss");
		auto minLength = minTck.length();

		auto tick = numberTxt + ":";
		tick += QDateTime::fromTime_t(timeIndex).toUTC().toString("mm:ss");

		auto stepTxt = QString("#(%1/%2)").arg(totalIndex + 1).arg(total_time.size());
		auto finalTime = QString("%1").arg(tick, minLength + 3, ' ');

		auto time_txt = stepTxt + finalTime;

		d->annoText->deleteAll();
		d->annoText->addLine(time_txt.toStdString());

		const auto ds = GetSceneDS();

		if (htIndex > -1) {//change precision for HT
			if (ds->activatedModality & Entity::Modality::HTVolume) {
				if (d->metaInfo->data.data3D.scalarType[htIndex] == 1) {
					d->htValueOffset = d->metaInfo->data.data3D.riMinList[htIndex] * 1000.0;
					d->htValueDiv = 10;
				} else {
					d->htValueOffset = 0;
					d->htValueDiv = 1;
				}
			} else if (ds->activatedModality & Entity::Modality::HTMIP) {
				if (d->metaInfo->data.data2DMIP.scalarType[htIndex] == 1) {
					d->htValueOffset = d->metaInfo->data.data2DMIP.riMinList[htIndex] * 1000.0;
					d->htValueDiv = 10;
				} else {
					d->htValueOffset = 0;
					d->htValueDiv = 1;
				}
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (flIndex[i] > -1) {//change precision for FL
				if (ds->activatedModality & Entity::Modality::FLVolume) {
					if (d->metaInfo->data.data3DFL.scalarType[i][flIndex[i]] == 1) {
						d->flValueOffset[i] = d->metaInfo->data.data3DFL.minIntensityList[i][flIndex[i]];
					} else {
						d->flValueOffset[i] = 0;
					}

				} else if (ds->activatedModality & Entity::Modality::FLMIP) {

					if (d->metaInfo->data.data2DFLMIP.scalarType[i][flIndex[i]] == 1) {
						d->flValueOffset[i] = d->metaInfo->data.data2DFLMIP.minIntensityList[i][flIndex[i]];
					} else {
						d->flValueOffset[i] = 0;
					}

				}
			}
		}

		if (nullptr != d->sliceConXY) {
			if (htIndex > -1) {
				d->bufferManager->setTimeStepHT(htIndex);
				d->infos[1]->string.setValue(std::to_string(htIndex));
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (flIndex[ch] > -1) {
					d->bufferManager->setTimeStepFL(flIndex[ch], ch);
				}
			}
		}

		if (nullptr != d->sliceConXY) {
			if (htIndex > -1) {
				d->sliceConXY->SetHTTimeStep(htIndex);
			} else {
				d->sliceConXY->SetHTTimeStep(-1);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (flIndex[ch] > -1) {
					d->sliceConXY->SetFLTimeStep(flIndex[ch], ch);
				} else {
					d->sliceConXY->SetFLTimeStep(-1, ch);
				}
			}
		}

		if (nullptr != d->bfContainer) {
			d->bufferManager->setTimeStepBF(bfIndex);
			d->bfContainer->setTimeStep(bfIndex);
		}

		//emit timeStepPlayed(timeIndex);
	}

	auto OIVViewer::AnimateSlice(const int& sliceType,
								const float& startTime, const float& endTime,
								const int& begin, const int& finish,
								const bool& reverse, const bool& last) -> void {
		// millisecond
		const float interval = (float(endTime - startTime) / float(finish - begin)) * 1000.f;

		auto index = begin;
		auto endIndex = finish;
		auto iterator = 1;
		if (true == reverse) {
			index = finish;
			endIndex = begin;
			iterator = -1;
		}

		Sleep(startTime * 1000.f);

		while (true) {
			if (false == d->simulating) {
				return;
			}

			auto tpStart = std::chrono::system_clock::now();
			{
				emit moveSlice(sliceType, index);

				if (index == endIndex) {
					break;
				}

				index += iterator;
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		if (true == last) {
			d->simulating = false;
			emit finishSimulate();
		}
	}

	auto OIVViewer::AnimateTimelapse(const int& modality,
									const float& startTime, const float& endTime,
									const int& begin, const int& finish,
									const bool& reverse, const bool& last) -> void {
		Q_UNUSED(modality)
		if (nullptr == d->bufferManager) {
			return;
		}

		// millisecond
		const float interval = (float(endTime - startTime) / float(finish - begin)) * 1000.f;

		auto index = begin;
		auto endIndex = finish;
		auto iterator = 1;
		if (true == reverse) {
			index = finish;
			endIndex = begin;
			iterator = -1;
		}

		Sleep(startTime * 1000.f);

		while (true) {
			if (false == d->simulating) {
				return;
			}

			auto tpStart = std::chrono::system_clock::now();
			{
				SetTimeStep(index);

				if (index == endIndex) {
					break;
				}

				index += iterator;
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		if (true == last) {
			d->simulating = false;
			emit finishSimulate();
		}
	}

	auto OIVViewer::RecordScreen(QString path, TC::RecordType type, QList<TC::RecordAction*> process) -> bool {
		auto totalEndTime = 0.f;
		const auto fps = 30.f;

		struct SimulationAction {
			enum ActiveWindow {
				NONE = -1,
				XY = 0,
				YZ = 1,
				XZ = 2,
				VOLUME = 3,
				ALL = 4,
			};

			struct OrbitInfo {
				OrbitInfo(float a1, float a2, float a3, float v)
					: x(a1), y(a2), z(a3), value(v) { }

				float x = 0.f;
				float y = 0.f;
				float z = 0.f;
				float value = 0.f;
			};

			ActiveWindow window = ActiveWindow::NONE;

			QList<OrbitInfo> orbit; // axis x, y, z, orbit
			QList<int> slice; // plane, index,
			QList<int> timelapse; // time step
		};

		// make simulation per frame
		QList<SimulationAction> simulationActionList;
		for (auto item : process) {
			if (nullptr == item) {
				continue;
			}

			if (totalEndTime < item->endTime) {
				totalEndTime = item->endTime;
			}
		}

		const auto totalFrame = totalEndTime * fps;

		for (auto item : process) {
			if (nullptr == item) {
				continue;
			}

			const auto actionFrame = (item->endTime - item->startTime) * fps;

			SimulationAction action;
			if (item->type == +TC::ActionType::Orbit) {
				action.window = SimulationAction::VOLUME;

				float axis[3];

				if (item->axis == +TC::ActionAxis::T) {
					const auto direction = 4;

					// when orbit is 360(default), tilt is 2 degree
					const auto tilt = qDegreesToRadians(static_cast<float>(item->finish) / 180.f) * 10.f;
					auto tic = tilt * direction / totalFrame;

					if (true == item->reverse) {
						tic *= -1;
					}

					// start frame
					for (auto i = 1; i < item->startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					const auto repeat = totalFrame / direction / 2;
					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				} else {
					if (item->axis == +TC::ActionAxis::X) {
						axis[0] = 0.f;
						axis[1] = 1.f;
						axis[2] = 0.f;
					} else if (item->axis == +TC::ActionAxis::Y) {
						axis[0] = 1.f;
						axis[1] = 0.f;
						axis[2] = 0.f;
					} else if (item->axis == +TC::ActionAxis::Z) {
						axis[0] = 0.f;
						axis[1] = 0.f;
						axis[2] = 1.f;
					}

					const auto radian = qDegreesToRadians(static_cast<float>(item->finish));
					auto tic = radian / actionFrame;
					if (true == item->reverse) {
						tic *= -1;
					}

					// start frame
					const auto startRadian = qDegreesToRadians(static_cast<float>(item->begin - 180));
					action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], startRadian));

					for (auto i = 1; i < item->startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					for (auto i = 0; i < item->endTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				}
			} else if (item->type == +TC::ActionType::Slice) {
				if (item->windowType == +TC::ActionWindow::XY) {
					action.window = SimulationAction::XY;
				} else if (item->windowType == +TC::ActionWindow::YZ) {
					action.window = SimulationAction::YZ;
				} else if (item->windowType == +TC::ActionWindow::XZ) {
					action.window = SimulationAction::XZ;
				}

				// start frame
				for (auto i = 0; i < item->startTime * fps; ++i) {
					action.slice.append(-1);
				}

				// time frame
				auto startIndex = item->begin;
				auto endIndex = item->finish;
				float tic = (endIndex - startIndex + 1) / (actionFrame - 2);

				if (true == item->reverse) {
					startIndex = item->finish;
					endIndex = item->begin;
				}

				// time frame -> first
				action.slice.append(startIndex);

				for (auto i = 1; i < actionFrame - 1; ++i) {
					auto index = startIndex + ceil(i * tic);

					if (true == item->reverse) {
						index = startIndex - ceil(i * tic);
					}

					if (index < item->begin) {
						index = item->begin;
					}

					if (index > item->finish) {
						index = item->finish;
					}

					action.slice.append(index);
				}

				// time frame -> last
				action.slice.append(endIndex);

				// end frame
				for (auto i = 0; i < totalFrame - actionFrame; ++i) {
					action.slice.append(-1);
				}
			} else if (item->type == +TC::ActionType::Timelapse) {
				action.window = SimulationAction::ALL;
				// start frame
				for (auto i = 0; i < item->startTime * fps; ++i) {
					action.timelapse.append(-1);
				}

				auto startIndex = item->begin;
				auto endIndex = item->finish;
				float tic = (endIndex - startIndex + 1) / (actionFrame);

				if (true == item->reverse) {
					startIndex = item->finish;
					endIndex = item->begin;
				}

				// time frame -> first
				for (auto i = 0; i < actionFrame; i++) {
					auto index = startIndex + i * tic;

					if (true == item->reverse) {
						index = startIndex - i * tic;
					}

					if (index < item->begin) {
						index = item->begin;
					}

					if (index > item->finish) {
						index = item->finish;
					}
					action.timelapse.append((int)index);
				}


				// time frame -> last

				// end frame
				for (auto i = 0; i < totalFrame - actionFrame - 1; ++i) {
					action.timelapse.append(-1);
				}
			}

			simulationActionList.append(action);
		}

		// save buffer
		auto interval = 1000.f / fps;

		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);

		auto prevTimeStep = -1;


		for (auto i = 0; i < totalFrame; ++i) {
			if (false == d->simulating) {
				break;
			}
			auto tpStart = std::chrono::system_clock::now();
			{
				for (const auto& action : simulationActionList) {
					if (false == d->simulating) {
						break;
					}
					if (action.window == SimulationAction::VOLUME) { } else if (action.window == SimulationAction::ALL) {
						if (0 <= action.timelapse.at(i)) {
							if (prevTimeStep != action.timelapse.at(i)) {
								emit timeStepChanged(d->time_points[action.timelapse.at(i)]);

								prevTimeStep = action.timelapse.at(i);
							}
						}
					} else {
						if (0 <= action.slice.at(i)) {
							emit moveSlice(action.window, action.slice.at(i));
						}
					}
				}
				if (type != +TC::RecordType::None) {
					emit captureScreen(type._to_integral());
				}
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		d->simulating = false;

		// save file

		if (true == path.isEmpty()) {
			return true;
		}
		std::list<cv::Mat> cvBuffer;
		if (type._to_integral() != 4) {
			for (const auto& frame : d->recordBuffer) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		} else {//combine multiple buffers					
			//merge after buffering to avoid mis matching
			MergeScreens(cvBuffer);
		}
		TC::IO::TCVideoWriter writer;
		auto result = writer.Write(path.toStdString(), cvBuffer);

		return result;
	}

	auto OIVViewer::MergeScreens(std::list<cv::Mat>& cvBuffer) -> void {
		if (d->layoutType._to_index() == Entity::LayoutType::XYPlane) {
			for (const auto& frame : d->recordBufferXY) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		}
	}


	auto OIVViewer::WindowIndex2MedicalHelperAxis(const int& windowIndex) -> int {
		MedicalHelper::Axis axis = MedicalHelper::Axis::AXIAL;

		switch (windowIndex) {
			case Entity::ViewType::XY2D:
				axis = MedicalHelper::Axis::AXIAL;
				break;
			case Entity::ViewType::YZ2D:
				axis = MedicalHelper::Axis::SAGITTAL;
				break;
			case Entity::ViewType::ZX2D:
				axis = MedicalHelper::Axis::CORONAL;
				break;
			default: ;
		}

		return axis;
	}

	auto OIVViewer::ChangeLayout(const Entity::LayoutType& layoutType) const -> void {
		d->layoutView->SetLayoutType(layoutType);
		for (auto window : d->qOiv2DRenderWindow) {
			if (nullptr != window) {
				window->reset2DView();
			}
		}
		d->layoutType = layoutType;
	}

	auto OIVViewer::Grab() -> QPixmap {
		auto GetOffWidget = [=](const QPixmap& pixmap, QWidget* parent) {
			auto label = new QLabel;
			label->setPixmap(pixmap);
			label->setScaledContents(true);

			auto layout = new QHBoxLayout;
			layout->setContentsMargins(0, 0, 0, 0);
			layout->setSpacing(0);
			layout->addWidget(label);

			auto widget = new QWidget(parent);
			widget->setContentsMargins(0, 0, 0, 0);
			widget->setLayout(layout);

			return widget;
		};

		if (nullptr == d->layoutView) {
			return QPixmap();
		}

		d->layoutView->SaveCurrent();

		const auto bufferXY = d->qOiv2DRenderWindow[0]->getRenderBuffer(1);

		auto offscreenWidget = new QWidget(nullptr);
		offscreenWidget->setFixedSize(this->size());

		auto layoutView = new LayoutView(offscreenWidget);
		layoutView->AddWidget(Entity::ViewType::XY2D, GetOffWidget(bufferXY, layoutView));
		layoutView->LoadCurrent();

		auto layout = new QHBoxLayout(offscreenWidget);
		layout->addWidget(layoutView);

		offscreenWidget->setLayout(layout);

		auto pixmap = offscreenWidget->grab();

		delete offscreenWidget;
		offscreenWidget = nullptr;

		return pixmap;
	}

	auto OIVViewer::RequestRender() -> void {
		for (int i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->qOiv2DRenderWindow[i])
				d->qOiv2DRenderWindow[i]->immediateRender();
		}
	}

	void OIVViewer::onCanvasMouse(bool pressed) { }

	void OIVViewer::Delay(const LARGE_INTEGER& frequency, const float& millisecond) {
		static LARGE_INTEGER BeginTime;
		static LARGE_INTEGER EndTime;
		QueryPerformanceCounter(&BeginTime);

		while (true) {
			QueryPerformanceCounter(&EndTime);
			if ((static_cast<float>(EndTime.QuadPart - BeginTime.QuadPart) /
				static_cast<float>(frequency.QuadPart)) * 1000.0f >= millisecond)
				break;
		}
	}

	auto OIVViewer::SetHTInfo(const AppEntity::ColormapInfo& info) -> void {
		const auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[0].ptr(), "HTSliceRange");
		auto levelmin = std::get<0>(info.GetXRange());
		auto levelmax = std::get<1>(info.GetXRange());
		if (range) {
			range->min = levelmin / d->htValueDiv - d->htValueOffset;
			range->max = levelmax / d->htValueDiv - d->htValueOffset;
			d->curLevelMin = levelmin;
			d->curLevelMax = levelmax;
		}
		d->levelWindow->SetLevelMinMax(levelmin, levelmax);
		auto predColormap = info.GetPredefinedColormap();
		this->SetGammaColormap(info.GetPredefinedColormap(), info.isGamma(), info.GetGamma());
	}

	auto OIVViewer::GetHTInfo() const -> AppEntity::ColormapInfo {
		AppEntity::ColormapInfo info;
		double riMin = 0;
		double riMax = 0;
		const auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[0].ptr(), "HTSliceRange");
		if (range) {
			riMin = (range->min.getValue() + d->htValueOffset) * d->htValueDiv;
			riMax = (range->max.getValue() + d->htValueOffset) * d->htValueDiv;
		}
		info.SetXRange(riMin, riMax);
		info.SetIsVisible(d->volumeChildIndex == HT_INDEX || d->volumeChildIndex == SO_SWITCH_ALL);
		const auto tf = MedicalHelper::find<SoTransferFunction>(d->sliceRenderRoot[0].ptr(), "colormap2D_TF");
		int predIdx;
		if (tf->predefColorMap.getValue() == SoTransferFunction::PredefColorMap::INTENSITY) {
			predIdx = 0;
		} else if (tf->predefColorMap.getValue() == SoTransferFunction::PredefColorMap::INTENSITY_REVERSED) {
			predIdx = 1;
		} else if (tf->predefColorMap.getValue() == SoTransferFunction::PredefColorMap::GLOW) {
			predIdx = 2;
		} else if (tf->predefColorMap.getValue() == SoTransferFunction::PredefColorMap::PHYSICS) {
			predIdx = 3;
		} else {
			predIdx = 4;
		}
		info.SetPredefinedColormap(predIdx);
		info.SetGamma(d->gamma);
		info.SetIsGamma(d->isGamma);

		return info;
	}

	auto OIVViewer::SetFLInfo(int ch, const AppEntity::ColormapInfo& info) -> void {
		Q_UNUSED(ch)
		Q_UNUSED(info)
		//apply fl information using FL channel control UI signal
	}

	auto OIVViewer::GetFLInfo(int ch) const -> AppEntity::ColormapInfo {
		AppEntity::ColormapInfo info;
		info.SetIsGamma(d->chIsGamma[ch]);
		info.SetGamma(d->chGamma[ch]);
		info.SetIsVisible(d->curChVisible[ch]);
		info.SetOpacity(d->chOpacity[ch]);
		info.SetXRange(d->chRange[ch][0] + d->flValueOffset[ch], d->chRange[ch][1] + d->flValueOffset[ch]);
		info.SetRgb(d->chRGB[ch][0], d->chRGB[ch][1], d->chRGB[ch][2]);
		return info;
	}

	auto OIVViewer::SetCam2dInfo(const AppEntity::Camera2DInfo& info) -> void {
		auto orthoCam = MedicalHelper::find<SoOrthographicCamera>(d->qOiv2DRenderWindow[0]->getSceneGraph());
		auto pos = info.GetPosition();
		orthoCam->position.setValue(std::get<0>(pos), std::get<1>(pos), std::get<2>(pos));
		orthoCam->height.setValue(info.GetHeight());

		auto focalPos = orthoCam->getFocalPoint();
		d->miniMap[0]->movingX = focalPos[0];
		d->miniMap[0]->movingY = focalPos[1];
		d->miniMap[0]->zoomFactor = info.GetHeight();
	}

	auto OIVViewer::GetCam2dInfo() const -> AppEntity::Camera2DInfo& {
		AppEntity::Camera2DInfo info;
		auto orthoCam = MedicalHelper::find<SoOrthographicCamera>(d->qOiv2DRenderWindow[0]->getSceneGraph());
		auto pos = orthoCam->position.getValue();
		info.SetPosition(pos[0], pos[1], pos[2]);
		auto height = orthoCam->height.getValue();
		info.SetHeight(height);
		return info;
	}

	auto OIVViewer::SetScalarBarInfo(const AppEntity::ScalarBarInfo& info) -> void {
		auto ds = GetSceneDS();
		d->visibleScalarBar = info.isVisible();
		if (info.isVisible()) {
			if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
				|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				d->scalarBarSwitch[0]->whichChild = -3;
			}
		} else {
			d->scalarBarSwitch[0]->whichChild = -1;
		}
		auto pos = info.GetCoordinate();
		d->scalarBar[0]->SetPosition(std::get<0>(pos), std::get<1>(pos));
		d->scalarBar[0]->SetHorizontal(info.GetOrientation() == AppEntity::HORIZONTAL);
	}

	auto OIVViewer::GetScalarBarInfo() const -> AppEntity::ScalarBarInfo {
		AppEntity::ScalarBarInfo info;
		info.SetIsVisible(d->scalarBarSwitch[0]->whichChild.getValue() == -3);
		auto pos = d->scalarBar[0]->GetPosition();
		info.SetCoordinate(std::get<0>(pos), std::get<1>(pos));
		if (d->scalarBar[0]->IsHorizontal()) {
			info.SetOrientation(AppEntity::Orientation::HORIZONTAL);
		} else {
			info.SetOrientation(AppEntity::Orientation::VERTICAL);
		}
		return info;
	}

	auto OIVViewer::SetScaleBarInfo(const AppEntity::ScaleBarInfo& info) -> void {
		if (info.isVisible()) {
			d->scaleBarSwitch[0]->whichChild = -3;
		} else {
			d->scaleBarSwitch[0]->whichChild = -1;
		}
		auto color = info.GetColor();
		d->scaleBar[0]->SetColor(std::get<0>(color), std::get<1>(color), std::get<2>(color));
		auto pos = info.GetCoordinate();
		d->scaleBar[0]->SetPosition(std::get<0>(pos), std::get<1>(pos));
		auto length = info.GetLength();
		d->scaleBar[0]->SetLength(length);
		auto ori = info.GetOrientation();
		d->scaleBar[0]->SetHorizontal(ori == AppEntity::HORIZONTAL);
		auto tick = info.GetTick();
		d->scaleBar[0]->SetTick(tick);
		auto isTextVisible = info.isTextVisible();
		d->scaleBar[0]->SetTextVisible(isTextVisible);
	}

	auto OIVViewer::GetScaleBarInfo() const -> AppEntity::ScaleBarInfo {
		AppEntity::ScaleBarInfo info;
		info.SetIsVisible(d->scaleBarSwitch[0]->whichChild.getValue() == -3);
		auto color = d->scaleBar[0]->GetColor();
		info.SetColor(std::get<0>(color), std::get<1>(color), std::get<2>(color));
		auto pos = d->scaleBar[0]->GetPosition();
		info.SetCoordinate(std::get<0>(pos), std::get<1>(pos));
		auto length = d->scaleBar[0]->GetLength();
		info.SetLength(length);
		auto ori = d->scaleBar[0]->IsHorizontal();
		if (ori) {
			info.SetOrientation(AppEntity::HORIZONTAL);
		} else {
			info.SetOrientation(AppEntity::VERTICAL);
		}
		auto tick = d->scaleBar[0]->GetTick();
		info.SetTick(tick);
		auto isTextVisible = d->scaleBar[0]->GetTextVisible();
		info.SetIsTextVisible(isTextVisible);
		return info;
	}

	auto OIVViewer::SetTimeStampInfo(const AppEntity::TimeStampInfo& info) -> void {
		auto ds = GetSceneDS();
		ds->visibleTimeStamp = info.isVisible();
		if (info.isVisible()) {
			d->timestampSwitch->whichChild = -3;
		} else {
			d->timestampSwitch->whichChild = -1;
		}
		auto color = info.GetColor();
		d->annoMatl->diffuseColor.setValue(std::get<0>(color), std::get<1>(color), std::get<2>(color));
		auto fontSize = info.GetFontSize();
		d->annoText->fontSize = fontSize;
		auto pos = info.GetCoordinate();
		auto zPos = d->annoText->position.getValue()[2];
		d->annoText->position.setValue(std::get<0>(pos), std::get<1>(pos), zPos);
	}

	auto OIVViewer::GetTimeStampInfo() const -> AppEntity::TimeStampInfo {
		AppEntity::TimeStampInfo info;
		info.SetIsVisible(d->timestampSwitch->whichChild.getValue() == -3);
		auto color = d->annoMatl->diffuseColor.getValues(0)[0];
		info.SetColor(color[0], color[1], color[2]);
		auto pos = d->annoText->position.getValue();
		info.SetCoordinate(pos[0], pos[1]);
		auto fontSize = d->annoText->fontSize.getValue();
		info.SetFontSize(fontSize);
		return info;
	}

	auto OIVViewer::SetDeviceInfo(const AppEntity::DeviceInfo& info) -> void {
		if (info.isVisible()) {
			d->deviceInfoSwitch->whichChild = -3;
		} else {
			d->deviceInfoSwitch->whichChild = -1;
		}
		auto color = info.GetColor();
		d->deviceMatl->diffuseColor.setValue(std::get<0>(color), std::get<1>(color), std::get<2>(color));
		auto fontSize = info.GetFontSize();
		d->htxInfo->fontSize.setValue(fontSize);
	}

	auto OIVViewer::GetDeviceInfo() const -> AppEntity::DeviceInfo {
		AppEntity::DeviceInfo info;
		info.SetIsVisible(d->deviceInfoSwitch->whichChild.getValue() == -3);
		auto color = d->deviceMatl->diffuseColor.getValues(0)[0];
		info.SetColor(color[0], color[1], color[2]);
		auto fontSize = d->htxInfo->fontSize.getValue();
		info.SetFontSize(fontSize);
		return info;
	}

}
