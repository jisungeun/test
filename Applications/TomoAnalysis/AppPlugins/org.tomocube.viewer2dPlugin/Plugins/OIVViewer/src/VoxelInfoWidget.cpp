
#include "ui_VoxelInfoWidget.h"
#include "VoxelInfoWidget.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct VoxelInfoWidget::Impl {
        Ui::VoxelPanel ui;
    };
    VoxelInfoWidget::VoxelInfoWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        this->ToggleVisible(false);
    }
    VoxelInfoWidget::~VoxelInfoWidget() {
        
    }
    auto VoxelInfoWidget::SetPosition(double x, double y, double z, double timePoint) -> void {
        d->ui.xValue->setText(QString::number(x, 'f', 2));
        d->ui.yValue->setText(QString::number(y, 'f', 2));
        d->ui.zValue->setText(QString::number(z, 'f', 2));

        auto intTime = static_cast<int>(timePoint);
        auto sec = (intTime % 3600) % 60;
        auto min = (intTime % 3600) / 60;
        auto hour = intTime / 3600;

        d->ui.tValue->setText(QString("%1:%2:%3").arg(QString::number(hour).rightJustified(3,'0')).arg(QString::number(min).rightJustified(2, '0')).arg(QString::number(sec).rightJustified(2, '0')));
    }
    auto VoxelInfoWidget::SetRIValue(double value) -> void {
        if(value<0) {
            d->ui.riValue->setText("N/A");
        }else {
            d->ui.riValue->setText(QString::number(value, 'f', 3));
        }
    }
    auto VoxelInfoWidget::SetFLValue(double ch1Value, double ch2Value, double ch3Value) -> void {
        if(ch1Value < 0) {
            d->ui.fl1Value->setText("N/A");
        }else {
            d->ui.fl1Value->setText(QString::number(ch1Value));
        }
        if(ch2Value<0) {
            d->ui.fl2Value->setText("N/A");
        }else {
            d->ui.fl2Value->setText(QString::number(ch2Value));
        }
        if(ch3Value<0) {
            d->ui.fl3Value->setText("N/A");
        }else {
            d->ui.fl3Value->setText(QString::number(ch3Value));
        }
    }
    auto VoxelInfoWidget::ToggleVisible(bool isVisible) -> void {
        if(false == isVisible) {
            d->ui.xLabel->setText("");
            d->ui.yLabel->setText("");
            d->ui.zLabel->setText("");
            d->ui.tLabel->setText("");
            d->ui.xValue->setText("");
            d->ui.yValue->setText("");
            d->ui.zValue->setText("");
            d->ui.tValue->setText("");

            d->ui.rilabel->setText("");
            d->ui.riValue->setText("");

            d->ui.fl1Label->setText("");
            d->ui.fl1Value->setText("");

            d->ui.fl2Label->setText("");
            d->ui.fl2Value->setText("");

            d->ui.fl3Label->setText("");
            d->ui.fl3Value->setText("");
        }else {
            d->ui.xLabel->setText("X: ");
            d->ui.yLabel->setText("Y: ");
            d->ui.zLabel->setText("Z: ");
            d->ui.tLabel->setText("T: ");

            d->ui.rilabel->setText("RI: ");

            d->ui.fl1Label->setText("FL#1: ");

            d->ui.fl2Label->setText("FL#2: ");

            d->ui.fl3Label->setText("FL#3: ");
        }
    }
}
