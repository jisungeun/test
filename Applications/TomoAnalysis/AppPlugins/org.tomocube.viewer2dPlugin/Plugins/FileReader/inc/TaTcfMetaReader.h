#pragma once

#include <string>

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include <IFileReaderPort.h>

#include "TomoAnalysis2dFileReaderExport.h"

namespace TomoAnalysis::Viewer2D::Plugins::FileReader {
	class TomoAnalysis2dFileReader_API TcfMetaReader : public UseCase::IFileReaderPort {
	public:
		struct HTMeta {
			int dataCount = 0;
			float riMax = -1.f;
			float riMin = -1.f;
			float resolutionX = -1.f;
			float resolutionY = -1.f;
			float resolutionZ = -1.f;
			int sizeX = -1;
			int sizeY = -1;
			int sizeZ = -1;
			float timeInterval = -1.f;

			float positionC = -1.f;
			float positionX = -1.f;
			float positionY = -1.f;
			float positionZ = -1.f;

			std::vector<double> time_points;

			typedef std::shared_ptr<HTMeta> Pointer;
		};

		struct FLMeta {
			int dataCount = 0;

			bool channelValid[3] = {false, };
			float intensityMin[3] = {-1, }; // rgb( tcf�� bgr)
			float intensityMax[3] = { -1, }; // min�� ����

			float resolutionX = -1.f;
			float resolutionY = -1.f;
			float resolutionZ = -1.f;
			int sizeX = -1;
			int sizeY = -1;
			int sizeZ = -1;
			float timeInterval = -1.f;

			float offsetZ = -1.f;
			float offsetZCompensation = -1.f;

			float positionC = -1.f;
			float positionX = -1.f;
			float positionY = -1.f;
			float positionZ = -1.f;

			std::vector<double> time_points;

			typedef std::shared_ptr<FLMeta> Pointer;
		};

		struct BFMeta {
			int dataCount = 0;
			float resolutionX = -1.f;
			float resolutionY = -1.f;
			int sizeX = -1;
			int sizeY = -1;
			float timeInterval = -1.f;

			float positionC = -1.f;
			float positionX = -1.f;
			float positionY = -1.f;
			float positionZ = -1.f;

			std::vector<double> time_points;

			typedef std::shared_ptr<BFMeta> Pointer;
		};

		struct Dimension {
			int x = 0;
			int y = 0;
			int z = 0;
		};


	public:
		TcfMetaReader();
		TcfMetaReader(const TcfMetaReader& other);
		~TcfMetaReader();
		
		auto Read(const std::string& path)->Entity::Image::Pointer override;

	private:
		auto ReadHT3D(const std::string& path, const int& index = 0)->HTMeta::Pointer;
		auto ReadHTMIP(const std::string& path, const int& index = 0)->HTMeta::Pointer;
		auto ReadFL3D(const std::string& path, const int& index = 0)->FLMeta::Pointer;
		auto ReadFLMIP(const std::string& path, const int& index = 0)->FLMeta::Pointer;
		auto ReadBF(const std::string& path, const int& index = 0)->BFMeta::Pointer;

		auto ReadAttribute(H5::Group& group, const char* name, double& dValue) const->bool;
		auto ReadAttribute(H5::Group& group, const char* name, int& nValue) const->bool;

		auto ReadAttribute(H5::DataSet& dataset, const char* name, double& dValue) const->bool;
		auto ReadAttribute(H5::DataSet& dataset, const char* name, int& nValue) const->bool;

		auto ReadAttribute(H5::Attribute& attr, double& dValue) const->bool;
		auto ReadAttribute(H5::Attribute& attr, int& nValue) const->bool;

		auto ReadTimePoint(H5::DataSet& dataset,double& point)const->bool;
		auto ReadTimePoint(H5::Group& grp, double& point)const->bool;

		auto ConvertStr2ImageType(const std::string& str)->Entity::ImageType;

		auto GetDimension(const H5::DataSet& dataSet)->Dimension;
	};
}