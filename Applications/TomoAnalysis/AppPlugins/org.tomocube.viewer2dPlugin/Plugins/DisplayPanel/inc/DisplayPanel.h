#pragma once

#include <memory>
#include <QWidget>

#include <IDisplayPanel.h>
#include <Scene.h>

#include "TomoAnalysis2dDisplayPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dDisplayPanel_API DisplayPanel : public QWidget, public Interactor::IDisplayPanel {
        Q_OBJECT

    public:
        typedef DisplayPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        DisplayPanel(QWidget* parent=nullptr);
        ~DisplayPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;

    signals:
        void callUpdate();
        void displayTypeChanged(Entity::DisplayType);        

    protected slots:
        void onUpdate();
        void on_display3DButton_toggled(bool);
        void on_display2DButton_toggled(bool);
        void onActivatedModalityChanged(Entity::Modality);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}