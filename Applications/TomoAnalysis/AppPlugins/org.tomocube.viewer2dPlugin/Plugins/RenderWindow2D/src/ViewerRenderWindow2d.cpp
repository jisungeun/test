#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QColorDialog>
#include <QVBoxLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/draggers/SoScale2Dragger.h>
#include <Inventor/draggers/SoScale2UniformDragger.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>
#include <Inventor/engines/SoCompose.h>
#include <Inventor/engines/SoCalculator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoRotation.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/TextBox.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#pragma warning(pop)

#include <RangeSlider.h>
#include <SpinBoxAction.h>
#include <OivRangeBar.h>
#include <OivScaleBar.h>
#include <OivMinimap.h>

#include "ViewerRenderWindow2d.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    struct ViewerRenderWindow2d::Impl {
        //colormap
        int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow

        //scalarBar
        bool is_scalarBar{ false };
        bool is_scalarTrans{ false };
        bool is_scalarScale{ false };

        //levelWindow
        int level_min{ 0 };
        int level_max{ 100 };

        //spuit
        bool is_reload{ false };

        bool is_delete{ false };
        int ID{ -1 };
        bool isRIpick{ false };

        bool right_mouse_pressed{ false };
        bool middle_mouse_pressed{ false };
        bool left_mouse_pressed{ false };
        double right_stamp{ 0.0 };
        double prev_scale{ 1.0 };
        bool is2D{ false };
        bool isBF{ false };

        bool isXY{ false };
        bool isFirstResize{ false };

        bool scalar_pressed{ false };
        float scalar_pressed_pos[2]{ 0,0 };
        bool time_pressed{ false };
        float time_pressed_pos[2]{ 0,0 };
        bool scale_pressed{ false };
        float scale_pressed_pos[2]{ 0,0 };
        SbVec2s curPos;

        float zoomBase{ 1 };
        float zoomFactor{ 1 };
        int sizeX{ 1 };
        int sizeY{ 1 };
    };    

    ViewerRenderWindow2d::ViewerRenderWindow2d(QWidget* parent) :
        QOivRenderWindow(parent, true), d{ new Impl } {
        setDefaultWindowType();
    }

    ViewerRenderWindow2d::~ViewerRenderWindow2d() {

    }        

    auto ViewerRenderWindow2d::setRIPick(bool isRI)->void {
        d->isRIpick = isRI;
    }

    auto ViewerRenderWindow2d::resetOnLoad() -> void {
        d->is_scalarBar = false;
        d->colorMap = 0;
    }

    auto ViewerRenderWindow2d::setType(bool is2D) -> void {
        d->is2D = is2D;
    }

    auto ViewerRenderWindow2d::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        //auto new_y = getViewerExaminer()->getRenderArea()->getSize()[1] - pos[1];
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        /*
        QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
        QAction* cmOptions[COLORMAP_COUNT];
        for(auto i=0;i<COLORMAP_COUNT;i++) {
            cmOptions[i] = cmMenu->addAction(colorMapLabels[i]);
        }

        for (int i = 0; i < COLORMAP_COUNT; i++) {
            cmOptions[i]->setCheckable(true);
        }
        cmOptions[d->colorMap]->setChecked(true);*/

        //Dynamic addition of color scalarbar
        /*QMenu* sbMenu = myMenu.addMenu(tr("ScalarBar"));
        auto toggleSB = sbMenu->addAction("Show ScalarBar");
        toggleSB->setCheckable(true);
        toggleSB->setChecked(d->is_scalarBar);*/
        
        if (d->is_reload) {
            d->is_reload = false;
        }
        
        myMenu.addAction("Reset View");

        //if (d->ID == 0) {
        if(d->isRIpick){
            myMenu.addAction("RI Picker");
        }

        QAction* selectedItem = myMenu.exec(globalPos);

        if (selectedItem) {
            auto text = selectedItem->text();
            for (auto i = 0; i < COLORMAP_COUNT; i++) {
                if(text == colorMapLabels[i]) {
                    change2DColorMap(i);
                    return;
                }
            }            
            if (text.contains("Reset View")) {
                reset2DView(true);
            }           
            else if (text.contains("Show ScalarBar")) {
                toggleScalarBar();
            } 
        }
    }

    auto ViewerRenderWindow2d::toggleScalarBar() -> void {
        auto swname = "ScalarBarSwitch" + std::to_string(d->ID);
        auto scalarSwitch = MedicalHelper::find<SoSwitch>(getSceneGraph(), swname);

        if (scalarSwitch) {
            if (d->is_scalarBar) {
                scalarSwitch->whichChild = -1;
                d->is_scalarBar = false;
            }
            else {
                scalarSwitch->whichChild = 0;
                d->is_scalarBar = true;
            }
        }
    }
    
    auto ViewerRenderWindow2d::setRenderWindowID(int idx) -> void {
        d->ID = idx;
    }
       
    auto ViewerRenderWindow2d::resetViewBF(bool isBF) -> bool {
        auto changed = false;
        if (d->isBF != isBF) {
            changed = true;
        }
        d->isBF = isBF;
        return changed;
    }

    auto ViewerRenderWindow2d::reset2DView(bool fromFunc) -> void {
        //find which axis
        MedicalHelper::Axis ax;
        auto root = getSceneGraph();

        //reset scalar bar position
        auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
        if (scalarSep) {
            scalarSep->position.setValue(-0.95f, -0.95f);
        }
        //reset time stamp position
        auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
        if (timeSep) {
            timeSep->position.setValue(0.15f, -0.985f, 0);
        }
        //reset scale position
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (scaleSep) {
            scaleSep->position.setValue(0.95f, -0.95f);
        }

        SoVolumeData* volData{ nullptr };// = MedicalHelper::find<SoVolumeData>(root);//find any volume Data

        if (d->isBF) {
            volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
        }
        else {
            //find volume data except BF
            auto nodes = MedicalHelper::findNodes<SoVolumeData>(root);
            for (auto n : nodes) {
                QString nname(n->getName().getString());
                if (nname.contains("BF_CH")) {
                    continue;
                }
                volData = n;
                break;
            }
        }

        if (volData) {
            float slack;
            auto dims = volData->getDimension();
            auto spacing = volData->getVoxelSize();

            float x_len = dims[0] * spacing[0];
            float y_len = dims[1] * spacing[1];
            float z_len = dims[2] * spacing[2];

            auto wh = static_cast<float>(size().height());
            auto ww = static_cast<float>(size().width());
            auto windowSlack = wh > ww ? ww / wh : wh / ww;
            if (wh < 50) {
                d->isFirstResize = true;
            }
            if (d->ID == 0) {
                ax = MedicalHelper::Axis::AXIAL;
                slack = y_len < x_len ? y_len / x_len : x_len / y_len;
            }
            else if (d->ID == 1) {
                ax = MedicalHelper::SAGITTAL;
                slack = z_len / y_len;
            }
            else if (d->ID == 2) {
                ax = MedicalHelper::CORONAL;
                slack = z_len / x_len;
            }
            else {
                return;
            }
                        
            float largerLen = (x_len > y_len) ? x_len : y_len;
            
            if (windowSlack > slack && windowSlack > 0) {
                slack = windowSlack > 1 ? 1.0 : windowSlack;
            }
            MedicalHelper::orientView(ax, getCamera(), volData, slack);
            if (getName() == "TwoD1") {
                getCamera()->orientation.setValue(SbVec3f(0, 1, 0), M_PI);
            }
            if (getName() == "TwoD2") {
                getCamera()->orientation.setValue(SbVec3f(1, 0, 0), M_PI * 2);
                //getLight()->direction.setValue(0, 0, 1);
            }

            auto minimap = MedicalHelper::find<OivMinimap>(getSceneGraph());
            minimap->zoomBase = getCameraZoom();
            d->sizeX = dims[0];
            d->sizeY = dims[1];
            d->zoomBase = getCameraZoom();
            d->zoomFactor = 1;
            minimap->zoomFactor = getCameraZoom();
            minimap->movingX = 0;
            minimap->movingY = 0;

            CalcPixelPerRaster();

            emit sigLargeLength(largerLen);
            emit sigZoomFactor(1);

            if (fromFunc) {                
                //reset colormap
                change2DColorMap(0);
            }            
        }
    }

    void ViewerRenderWindow2d::resizeEvent(QResizeEvent* event) {
        if (d->isFirstResize) {
            reset2DView();
            d->isFirstResize = false;
        }
        QWidget::resizeEvent(event);
        CalcPixelPerRaster();
    }

    auto ViewerRenderWindow2d::change2DColorMap(int idx) -> void {
        auto rootScene = getSceneGraph();        
        if (rootScene) {
            auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colormap2D_TF");
            if (tf) {
                d->colorMap = idx;
                auto isInverse = false;
                int pred = 0;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    isInverse = true;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                d->colorMap = idx;
                tf->predefColorMap = pred;

                //change scalar bar text color at inverse grayscale
                emit sigColorMap(d->colorMap);
            }
        }
    }
    
    auto ViewerRenderWindow2d::refreshRangeSlider() -> void {
        d->is_reload = true;
    }

    auto ViewerRenderWindow2d::setHTRange(double min, double max) -> void {
        d->level_min = min;
        d->level_max = max;
    }
    auto ViewerRenderWindow2d::GetCurPosInfo() -> TCVoxelInfo {
        SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
        rayPick.setPoint(d->curPos);
        rayPick.apply(getSceneGraph());
        SoPickedPoint* pickedPt = rayPick.getPickedPoint();
        auto vols = MedicalHelper::findNodes<SoVolumeData>(getSceneGraph());
        TCVoxelInfo info;
        if (pickedPt) {
            auto cur_seed = pickedPt->getPoint();
            info.x = static_cast<double>(cur_seed[0]);
            info.y = static_cast<double>(cur_seed[1]);
            info.z = static_cast<double>(cur_seed[2]);
            info.valid = true;
            for (auto volData : vols) {
                auto idx = volData->XYZToVoxel(cur_seed);
                const auto dx = static_cast<int32_t>(idx[0]);
                const auto dy = static_cast<int32_t>(idx[1]);
                //const auto dz = static_cast<int32_t>(idx[2]);
                const auto dz = 0;
                auto pos = SbVec3i32(dx, dy, dz);
                auto value = volData->getValueD(pos);
                auto volName = QString(volData->getName().getString());
                if (volName.contains("volData") || volName.contains("HTMIP")) {
                    info.htIntensity = value;
                }
                else if (volName.contains("CH0") || volName.contains("CH_0")) {
                    info.flIntensity[0] = value;
                }
                else if (volName.contains("CH1") || volName.contains("CH_1")) {
                    info.flIntensity[1] = value;
                }
                else if (volName.contains("CH2") || volName.contains("CH_2")) {
                    info.flIntensity[2] = value;
                }
            }
        }
        return info;
    }
    auto ViewerRenderWindow2d::MouseMoveEvent(SoEventCallback* node) -> void {
        emit sigMouseMove();
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();

        //voxel information retrieval
        auto action = node->getAction();
        auto viewportRegion = action->getViewportRegion();
        const SoEvent* event = node->getEvent();
        auto mousePos = event->getPosition(viewportRegion);
        d->curPos = mousePos;
        SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
        rayPick.setPoint(mousePos);
        rayPick.apply(action->getPickRoot());
        SoPickedPoint* pickedPt = rayPick.getPickedPoint();

        auto vols = MedicalHelper::findNodes<SoVolumeData>(getSceneGraph());
        TCVoxelInfo info;
        if (pickedPt) {
            auto cur_seed = pickedPt->getPoint();
            info.x = static_cast<double>(cur_seed[0]);
            info.y = static_cast<double>(cur_seed[1]);
            info.z = static_cast<double>(cur_seed[2]);
            info.valid = true;
            for (auto volData : vols) {
                auto idx = volData->XYZToVoxel(cur_seed);
                const auto dx = static_cast<int32_t>(idx[0]);
                const auto dy = static_cast<int32_t>(idx[1]);
                //const auto dz = static_cast<int32_t>(idx[2]);
                const auto dz = 0;
                auto pos = SbVec3i32(dx, dy, dz);
                auto value = volData->getValueD(pos);
                auto volName = QString(volData->getName().getString());
                if (volName.contains("volData") || volName.contains("HTMIP")) {
                    info.htIntensity = value;
                }
                else if (volName.contains("CH0")||volName.contains("CH_0")) {
                    info.flIntensity[0] = value;
                }
                else if (volName.contains("CH1")||volName.contains("CH_1")) {
                    info.flIntensity[1] = value;
                }
                else if (volName.contains("CH2")||volName.contains("CH_2")) {
                    info.flIntensity[2] = value;
                }
            }
        }
    
        emit sigVoxelInfo(info);

        if (d->scalar_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
            if (scalarSep) {
                scalarSep->position.setValue(d->scalar_pressed_pos[0] + move_x, d->scalar_pressed_pos[1] + move_y);
            }
            return;
        }
        if (d->time_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
            if (timeSep) {
                timeSep->position.setValue(move_x, d->time_pressed_pos[1] + move_y, 0);
            }
            return;
        }
        if (d->scale_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
            if (scaleSep) {
                scaleSep->position.setValue(d->scale_pressed_pos[0] + move_x, -d->scale_pressed_pos[1] + move_y);
            }
            return;
        }
        if (isNavigation()) {      
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->right_mouse_pressed) {
                auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
                auto factor = 1.0 - diff * 1.5;
                if (factor > 0) {                    
                    getCamera()->scaleHeight(factor / d->prev_scale);
                    auto minimap = MedicalHelper::find<OivMinimap>(getSceneGraph());                    
                    minimap->zoomFactor = getCameraZoom();
                    d->prev_scale = factor;
                    d->zoomFactor = d->zoomBase / getCameraZoom();
                    CalcPixelPerRaster();
                    emit sigZoomFactor(1./d->zoomFactor);                    
                }
            }
            if (d->left_mouse_pressed) {                
                node->setHandled();
            }
            if (d->middle_mouse_pressed) {
                if (!d->is2D) {                    
                    panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
                    auto minimap = MedicalHelper::find<OivMinimap>(getSceneGraph());
                    auto focalPos = getCamera()->getFocalPoint();
                    minimap->movingX = focalPos[0];
                    minimap->movingY = focalPos[1];
                }
            }
        }
    }        
    auto ViewerRenderWindow2d::CalcPixelPerRaster() -> void {
        auto pixelX = static_cast<float>(d->sizeX) / d->zoomFactor;
        auto pixelY = static_cast<float>(d->sizeY) / d->zoomFactor;
        auto windowX = width();
        auto windowY = height();

        auto resolution_base = std::max(pixelX / static_cast<float>(windowX), pixelY / static_cast<float>(windowY));
        auto resolution = log(resolution_base);
        int intRes = static_cast<int>(resolution);
        if(intRes < 0) {
            intRes = 0;
        }

        emit sigResolution(intRes,d->zoomFactor);
	}

    auto ViewerRenderWindow2d::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        emit sigWheel(delta);
    }

    auto ViewerRenderWindow2d::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();        
        if (!isNavigation()) {//selection mode
            
        }
        else {//navigation mode
            auto pos = mouseButton->getPositionFloat();
            auto vr = getViewportRegion();
            auto normpos = mouseButton->getNormalizedPosition(vr);
            auto viewport_size = vr.getViewportSizePixels();
            auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
            if (SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                reset2DView();                
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                if (true == mouse_in_setting) return;

                const auto pick = PickAnnotation(mouseButton);
                if (pick == AnnotationObject::ScalarBar) {
                    auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
                    if (nullptr == scalarSep) return;

                    ShowRangeBarContextMenu(scalarSep, pos);
                }
                else if (pick == AnnotationObject::TimeStamp) {
                    auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
                    if (nullptr == timeSep) return;

                    ShowTimeStampContextMenu(timeSep, pos);
                }
                else if (pick == AnnotationObject::ScaleBar) {
                    auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
                    if (nullptr == scaleSep) return;

                    ShowScaleContextMenu(scaleSep, pos);
                }else{
                    d->right_mouse_pressed = true;
                    d->right_stamp = normpos[1];
                    d->prev_scale = 1.0;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = false;
                d->right_stamp = 0.0;
                d->prev_scale = 1.0;                
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                auto mouse_in_scalar = false;
                auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
                auto vp = getViewportRegion();
                if (scalarSep) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    auto x_min = (vvs[0][0] + 1.0f) / 2.0f;
                    auto x_max = (vvs[2][0] + 1.0f) / 2.0f;
                    auto y_min = (vvs[0][1] + 1.0f) / 2.0f;
                    auto y_max = (vvs[2][1] + 1.0f) / 2.0f;

                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scalar = true;
                    }
                }
                auto mouse_in_time = false;
                auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
                if (timeSep) {
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(timeSep->getTextNode());
                    auto bbox = action->getBoundingBox();

                    auto size = bbox.getSize();
                    auto tpos = timeSep->position.getValue();
                    auto x_min = (tpos[0] + 1.0f) / 2.0f - size[0] / 2;
                    auto x_max = (tpos[0] + 1.0f) / 2.0f + size[0] / 2;
                    auto y_min = (tpos[1] + 1.0f) / 2.0f;
                    auto y_max = (tpos[1] + 1.0f) / 2.0f + size[1];
                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_time = true;
                    }
                }
                auto mouse_in_scale = false;
                auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
                if (scaleSep) {
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    auto x_min = (lbbox.getMin()[0] + 1.0f) / 2.0f;
                    auto x_max = (lbbox.getMax()[0] + 1.0f) / 2.0f;
                    auto y_min = (lbbox.getMin()[1] + 1.0f) / 2.0f;
                    auto y_max = (tbbox.getMax()[1] + 1.0f) / 2.0f;
                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scale = true;
                    }
                }
                if (mouse_in_scalar) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    d->scalar_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - vvs[0][0]);
                    d->scalar_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - vvs[0][1]);
                    d->scalar_pressed = true;
                }
                else if (mouse_in_time) {
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(timeSep->getTextNode());
                    auto bbox = action->getBoundingBox();

                    auto size = bbox.getSize();

                    auto tpos = timeSep->position.getValue();
                    d->time_pressed = true;
                    d->time_pressed_pos[1] = -size[1] / 2;
                }
                else if (mouse_in_scale) {
                    d->scale_pressed = true;
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    d->scale_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - lbbox.getMax()[0]);
                    d->scale_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - tbbox.getMax()[1]);
                }
                else {
                    d->middle_mouse_pressed = true;
                    startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {                
                d->time_pressed = false;
                d->scalar_pressed = false;
                d->scale_pressed = false;
                d->middle_mouse_pressed = false;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (!mouse_in_setting) {
                    d->left_mouse_pressed = true;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (mouse_in_setting) {
                    showContextMenu(pos);                 
                }
                d->left_mouse_pressed = false;
            }
        }
    }

    auto ViewerRenderWindow2d::CalcVolumeCoord(SbVec2f norm_point) -> SbVec3f {
        SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
        SbVec2f normPoint = norm_point;

        auto root = getSceneGraph();

        rayPick.setNormalizedPoint(normPoint);
        rayPick.apply(root);

        SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
        if (pickedPoint) {
            //SoVolumeData* volData = MedicalHelper::find<SoVolumeData>(root,"volData0");//find any volume Data
            SoVolumeData* volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
            if (volData) {
                auto pt = pickedPoint->getPoint();
                auto dim = volData->getDimension();
                auto coord = volData->XYZToVoxel(pt);
                if (d->isXY) {
                    coord[0] += (static_cast<float>(dim[0]) / 2.f);
                    coord[1] += (static_cast<float>(dim[1]) / 2.f);
                }
                return coord;
            }
        }
        return SbVec3f();
    }
    auto ViewerRenderWindow2d::setXY(bool isXY) -> void {
        d->isXY = isXY;
    }

    auto ViewerRenderWindow2d::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback        
            //set handled를 통해 event를 scene의 하위 callback으로 전달하지 않고 terminate
            //instance->setSeekMode(true);
            //seek mode는 우선 지원하지 않는것으로 20.10.16 Jose T. Kim
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode가 selection 일때 alt키를 누르는 동안 임시로 navigation mode로의 전환
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ENTER)) {
            
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            /*if (isNavigation()) {
                setInteractionMode(false);
            }else {
                setInteractionMode(true);
            }*/
        }
        //Shift + F12는 native viewer event callback이 해결하도록        
    }
    auto ViewerRenderWindow2d::toggleNavigation(bool isNavi) -> void {
        setInteractionMode(isNavi);
    }

    auto ViewerRenderWindow2d::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto ViewerRenderWindow2d::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.0,0.0,0.0 };
        SbVec3f end_color = { 0.0,0.0,0.0 };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(true);//orthographic

        //set default navigation type
        //setNavigationMode(false);//plane
    }

    auto ViewerRenderWindow2d::PickAnnotation(const SoMouseButtonEvent* mouse)->AnnotationObject {
        auto vp = getViewportRegion();
        auto normpos = mouse->getNormalizedPosition(vp);

        auto mouse_in_scalar = false;
        auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
        if (scalarSep) {
            auto geom = scalarSep->getBorderGeometry();
            auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
            auto vvs = vertices->vertex.getValues(0);
            auto x_min = (vvs[0][0] + 1.0f) / 2.0f;
            auto x_max = (vvs[2][0] + 1.0f) / 2.0f;
            auto y_min = (vvs[0][1] + 1.0f) / 2.0f;
            auto y_max = (vvs[2][1] + 1.0f) / 2.0f;

            if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                mouse_in_scalar = true;
            }
        }

        auto mouse_in_time = false;
        auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
        if (timeSep) {
            SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
            action->apply(timeSep->getTextNode());
            auto bbox = action->getBoundingBox();

            auto size = bbox.getSize();
            auto tpos = timeSep->position.getValue();
            auto x_min = (tpos[0] + 1.0f) / 2.0f - size[0] / 2;
            auto x_max = (tpos[0] + 1.0f) / 2.0f + size[0] / 2;
            auto y_min = (tpos[1] + 1.0f) / 2.0f;
            auto y_max = (tpos[1] + 1.0f) / 2.0f + size[1];
            if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                mouse_in_time = true;
            }
        }

        auto mouse_in_scale = false;
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (scaleSep) {
            auto lineSep = scaleSep->GetLineSep();
            SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
            laction->apply(lineSep);
            auto lbbox = laction->getBoundingBox();
            auto textSep = scaleSep->GetTextSep();
            SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
            taction->apply(textSep);
            auto tbbox = taction->getBoundingBox();

            auto x_min = (lbbox.getMin()[0] + 1.0f) / 2.0f;
            auto x_max = (lbbox.getMax()[0] + 1.0f) / 2.0f;
            auto y_min = (lbbox.getMin()[1] + 1.0f) / 2.0f;
            auto y_max = (tbbox.getMax()[1] + 1.0f) / 2.0f;

            if (OivScaleBar::VERTICAL == scaleSep->orientation.getValue()) {
                x_min = (tbbox.getMin()[0] + 1.0f) / 2.0f;
                x_max = (lbbox.getMax()[0] + 1.0f) / 2.0f;
                y_min = (lbbox.getMin()[1] + 1.0f) / 2.0f;
                y_max = (lbbox.getMax()[1] + 1.0f) / 2.0f;
            }

            if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                mouse_in_scale = true;
            }
        }

        if (mouse_in_scalar) return AnnotationObject::ScalarBar;
        if (mouse_in_time) return AnnotationObject::TimeStamp;
        if (mouse_in_scale) return AnnotationObject::ScaleBar;

        return AnnotationObject::None;
    }

    auto ViewerRenderWindow2d::ShowRangeBarContextMenu(OivRangeBar* rangeBar, const SbVec2f& pos) -> void {
        if (nullptr == rangeBar) return;

        const auto new_y = getQtRenderArea()->size().height() - pos[1];
        const QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu menu;
        menu.addAction("Toggle Orientation");

        QAction* selectedItem = menu.exec(globalPos);
        if (nullptr == selectedItem) return;

        const auto text = selectedItem->text();
        if (text.contains("Toggle Orientation")) {
            if (OivRangeBar::Orientation::HORIZONTAL == rangeBar->orientation.getValue()) {
                rangeBar->orientation.setValue(OivRangeBar::Orientation::VERTICAL);
            }
            else {
                rangeBar->orientation.setValue(OivRangeBar::Orientation::HORIZONTAL);
            }

            auto x = 0.f, y = 0.f;
            rangeBar->barSize.getValue().getValue(x, y);
            rangeBar->barSize.setValue(y, x);
            //rangeBar->rebuildAnnotation();
        }
    }

    auto ViewerRenderWindow2d::ShowScaleContextMenu(OivScaleBar* scaleBar, const SbVec2f& pos) -> void {
        if (nullptr == scaleBar) return;

        const auto new_y = getQtRenderArea()->size().height() - pos[1];
        const QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu menu;
        menu.addAction("Color");
        menu.addAction("Toggle Orientation");
        menu.addAction("Set Length");
        //menu.addAction("Set Increment");
        menu.addAction("Show/Hide Text");

        QAction* selectedItem = menu.exec(globalPos);
        if (nullptr == selectedItem) return;

        const auto text = selectedItem->text();
        if (text.contains("Color")) {
            auto mat = MedicalHelper::find<SoMaterial>(getSceneGraph(), "ScaleBarMat");
            if (nullptr == mat) return;

            auto old = mat->diffuseColor[0].getValue();
            QColor old_col(old[0] * 255, old[1] * 255, old[2] * 255);
            QColor new_col = QColorDialog::getColor(old_col, nullptr, "Select Color");
            if (false == new_col.isValid()) return;
            mat->diffuseColor.setValue(new_col.redF(), new_col.greenF(), new_col.blueF());
        }
        else if (text.contains("Toggle Orientation")) {
            if (OivScaleBar::HORIZONTAL == scaleBar->orientation.getValue())
                scaleBar->orientation.setValue(OivScaleBar::VERTICAL);
            else
                scaleBar->orientation.setValue(OivScaleBar::HORIZONTAL);
        }
        else if (text.contains("Set Length")) {
            emit sigScaleLength(scaleBar->GetLength());
        }
        else if (text.contains("Set Increment")) {
            emit sigScaleTick(int(scaleBar->tick.getValue()));
        }
        else if (text.contains("Show/Hide Text")) {
            scaleBar->SetTextVisible(!scaleBar->GetTextVisible());
            scaleBar->touch();
        }
    }
    auto ViewerRenderWindow2d::ShowTimeStampContextMenu(TextBox* timeStamp, const SbVec2f& pos) -> void {
        if (nullptr == timeStamp) return;

        const auto new_y = getQtRenderArea()->size().height() - pos[1];
        const QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu menu;
        menu.addAction("Color");

        QAction* selectedItem = menu.exec(globalPos);
        if (nullptr == selectedItem) return;

        const auto text = selectedItem->text();
        if (text.contains("Color")) {
            auto mat = MedicalHelper::find<SoMaterial>(getSceneGraph(), "TimeStampMat");
            if (nullptr == mat) return;

            auto old = mat->diffuseColor[0].getValue();
            QColor old_col(old[0] * 255, old[1] * 255, old[2] * 255);
            QColor new_col = QColorDialog::getColor(old_col, nullptr, "Select Color");
            mat->diffuseColor.setValue(new_col.redF(), new_col.greenF(), new_col.blueF());

            emit sigTimeStampColor(new_col);
        }
    }    
    auto ViewerRenderWindow2d::SetScaleLength(const float& length) -> void {
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (nullptr == scaleSep) return;

        scaleSep->SetRealLength(length);
        scaleSep->touch();
    }
    auto ViewerRenderWindow2d::SetScaleTick(const int& tick) -> void {
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (nullptr == scaleSep) return;

        scaleSep->tick.setValue(tick);
        scaleSep->touch();
    }
}
