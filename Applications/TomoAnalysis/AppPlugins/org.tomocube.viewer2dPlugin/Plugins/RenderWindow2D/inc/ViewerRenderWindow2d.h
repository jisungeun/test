#pragma once

#include <QOivRenderWindow.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Medical/nodes/TextBox.h>
#pragma warning(pop)

#include <QDoubleSpinBox>
#include <QWidgetAction>

#include <OivRangeBar.h>
#include <OivScaleBar.h>

#include "Viewer2dRenderWindow2DExport.h"

class SoEventCallback;
class SoVolumeData;

enum class AnnotationObject {
    ScaleBar,
    TimeStamp,
    ScalarBar,
    None,
};

namespace TomoAnalysis::Viewer2D::Plugins {
    class Viewer2dRenderWindow2D_API ViewerRenderWindow2d : public QOivRenderWindow {
        Q_OBJECT
    public:
        ViewerRenderWindow2d(QWidget* parent);
        ~ViewerRenderWindow2d();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset2DView(bool fromFunc = false)->void;
        auto resetViewBF(bool isBF)->bool;
        auto setHTRange(double min, double max)->void;
        auto setRenderWindowID(int idx)->void;
        auto setRIPick(bool setRI)->void;
        auto refreshRangeSlider()->void;
        auto setType(bool is2D)->void;
        auto resetOnLoad()->void;
        auto setXY(bool isXY)->void;
        auto toggleNavigation(bool isNavi)->void;

        auto SetScaleLength(const float& length)->void;       
        auto SetScaleTick(const int& tick)->void;
        auto GetCurPosInfo()->TCVoxelInfo;

    signals:
        void sigWheel(float);
        void sigMouseMove();
        void sigColorMap(int);

        void sigScaleLength(const int& value);
        void sigScaleTick(const int& value);
        void sigTimeStampColor(QColor);
        void sigVoxelInfo(TCVoxelInfo);

        void sigZoomFactor(float zoom);
        void sigLargeLength(float len);
        void sigResolution(int,float);

    protected slots:

    private:
        void resizeEvent(QResizeEvent* event) override;

        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;
        //void wheelEvent(QWheelEvent* event) override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;

        auto change2DColorMap(int idx)->void;
        auto initRangeSlider()->void;        

        auto toggleScalarBar()->void;

        auto CalcVolumeCoord(SbVec2f norm_point)->SbVec3f;
        auto CalcPixelPerRaster()->void;

        auto PickAnnotation(const SoMouseButtonEvent* mouse)->AnnotationObject;
        auto ShowRangeBarContextMenu(OivRangeBar* rangeBar, const SbVec2f& pos) -> void;
        auto ShowScaleContextMenu(OivScaleBar* scaleBar, const SbVec2f& pos) -> void;
        auto ShowTimeStampContextMenu(TextBox* timeStamp, const SbVec2f& pos) -> void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}