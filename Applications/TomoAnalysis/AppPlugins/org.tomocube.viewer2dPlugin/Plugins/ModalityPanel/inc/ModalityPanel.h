#pragma once

#include <memory>
#include <QWidget>
#include <Scene.h>
#include <IModalityPanel.h>

#include "TomoAnalysis2dModalityPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dModalityPanel_API ModalityPanel : public QWidget, public Interactor::IModalityPanel {
        Q_OBJECT

    public:
        enum class Modality {
            Volume = 0,
            MIP = 1,
            Count
        };

        enum class ImageType {
            HT = 0,
            FL = 1,
            Count
        };
        
    public:
        typedef ModalityPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ModalityPanel(QWidget* parent=nullptr);
        ~ModalityPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;

        auto SetFLVisibility(bool visible)->void;

    signals:
        void callUpdate();
        void activatedModalityChanged(Entity::Modality modality);

    protected slots:
        void onUpdate();
        void onDisplayTypeChanged(Entity::DisplayType);

        void on_HTCheckBox_toggled(bool checked);
        void on_FLCheckBox_toggled(bool checked);
        void on_BFCheckBox_toggled(bool checked);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}