#pragma once

#include <memory>
#include <QWidget>
#include <IVisualizationListPanel.h>

#include "TomoAnalysis2dVisualizationListPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dVisualizationListPanel_API VisualizationListPanel : public QWidget, public Interactor::IVisualizationListPanel {
        Q_OBJECT
    public:
        VisualizationListPanel(QWidget* parent=nullptr);
        ~VisualizationListPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;
        auto Refresh()->bool override;
        auto Load()->bool override;
        auto Save(std::string path, bool saveAs)->bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;                

    signals:
        void callUpdate();
        void loadPreset(QString path);
        void deletePreset(QString path);
        void savePreset(QString path, bool saveAs);

    protected slots:
        void onUpdate();
        void on_listComboBox_currentIndexChanged(int index);
        void on_saveButton_clicked(bool clicked);
        void on_saveAsButton_clicked(bool clicked);
        void on_loadButton_clicked(bool clicked);
        void on_deleteButton_clicked(bool clicked);

    private:
        auto SetEnableUI(const bool& enable) const -> void;
        auto SetCurrentLabel(const QString& text) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
