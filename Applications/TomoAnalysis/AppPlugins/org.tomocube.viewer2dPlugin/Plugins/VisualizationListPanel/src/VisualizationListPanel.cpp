#include <iostream>

#include <QFileDialog>
#include <QFontMetrics>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>

#include <UIUtility.h>

#include "ui_VisualizationListPanel.h"
#include "VisualizationListPanel.h"

namespace  TomoAnalysis::Viewer2D::Plugins {
    struct VisualizationListPanel::Impl {
        Ui::VisualizationListPanel* ui{ nullptr };

        QString tcfFolderPath;
        QString currentPresetPath;
        QVector<QString> list;
    };

    VisualizationListPanel::VisualizationListPanel(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->ui = new Ui::VisualizationListPanel();
        d->ui->setupUi(this);

        // set object names
        d->ui->baseWidget->setObjectName("panel-base");
        d->ui->saveAsButton->setObjectName("bt-round-gray700");
        d->ui->loadButton->setObjectName("bt-round-gray700");
        d->ui->deleteButton->setObjectName("bt-round-gray700");
        d->ui->saveButton->setObjectName("bt-round-gray500");

        connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdate()));
    }

    VisualizationListPanel::~VisualizationListPanel() {
        
    }
    auto VisualizationListPanel::UpdateCall() -> bool {
        emit callUpdate();
        return true;
    }

    void VisualizationListPanel::onUpdate() {
        this->Update();
    }

    auto VisualizationListPanel::Update()->bool {
        this->SetEnableUI(true);

        auto ds = GetCommonDS();
        d->tcfFolderPath = ds->tcfPath.c_str();

        return true;
    }

    auto VisualizationListPanel::Refresh()->bool {        
        d->list.clear();        
        TC::SilentCall(d->ui->listComboBox)->clear();
        
        TC::SilentCall(d->ui->listComboBox)->addItem("Choose a TF preset");
        TC::SilentCall(d->ui->listComboBox)->setCurrentIndex(0);

        auto ds = GetListDS();
        for (const auto& item : ds->list) {
            auto baseName = QFileInfo(QString::fromLocal8Bit(item.c_str())).baseName();

            TC::SilentCall(d->ui->listComboBox)->addItem(baseName);

            d->list.append(item.c_str());
        }
        if (!d->currentPresetPath.isEmpty()) {
            SetCurrentLabel(d->currentPresetPath.toStdString().c_str());
        }        
        return true;
    }

    auto VisualizationListPanel::Load()->bool {
        auto ds = GetPresetDS();

        d->currentPresetPath = ds->currentPreset.c_str();

        SetCurrentLabel(ds->currentPreset.c_str());
        //TC::SilentCall(d->ui->listComboBox)->setCurrentIndex(0);        
        return true;
    }

    auto VisualizationListPanel::Save(std::string path, bool saveAs)->bool {
        if(true == saveAs) {
            d->currentPresetPath = path.c_str();            
        }
        //d->currentPresetPath = path.c_str();
        //SetCurrentLabel(d->currentPresetPath);
        return true;
    }

    auto VisualizationListPanel::Init(void) const ->bool {
        this->SetEnableUI(false);

        return true;
    }

    auto VisualizationListPanel::Reset(void) const ->bool {
        this->SetEnableUI(false);

        TC::SilentCall(d->ui->listComboBox)->clear();
        TC::SilentCall(d->ui->listComboBox)->addItem("Choose a TF preset");
        TC::SilentCall(d->ui->listComboBox)->setCurrentIndex(0);

        d->currentPresetPath = QString();

        //TC::SilentCall(d->ui->currentLabel)->setText("");

        return true;
    }
    
    void VisualizationListPanel::on_listComboBox_currentIndexChanged(int index) {
        if(1 > index) {
            // choose a tf preset
            d->currentPresetPath = QString();
            return;
        }
        const auto path = d->list.at(index - 1);        
        emit loadPreset(path);
    }    
    void VisualizationListPanel::on_saveButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        emit savePreset(d->currentPresetPath, false);
    }
    void VisualizationListPanel::on_deleteButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        if(d->currentPresetPath.isEmpty()) {
            return;
        }
        auto copy = d->currentPresetPath;
        d->currentPresetPath = QString();
        emit deletePreset(copy);        
    }
    void VisualizationListPanel::on_saveAsButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        //const auto defaultPath = QString("%1/preset").arg(QApplication::applicationDirPath());
        const auto defaultPath = QString("%1/preset").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset",defaultPath).toString();

        if (false == QDir().exists(prev)) {
            if (false == QDir().mkpath(prev)) {
                //QMessageBox::warning(nullptr, "Error", QString("Failed to make preset folder"));
                //return;
                prev = defaultPath;
            }
        }
        

        const auto path = QFileDialog::getSaveFileName(this, "Choose a file name to save the transfer function", prev, "TransferFunction (*.xml)");
        if(true == path.isEmpty()) {
            return;
        }

        for(auto i=0;i<path.length();i++) {
            if(path.at(i).unicode() >127) {
                QMessageBox::warning(nullptr, "Error", QString("Preset path contains non-english characters"));
                return;
            }
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recent/preset", QFileInfo(path).absoluteDir().absolutePath());

        emit savePreset(path, true);

        d->currentPresetPath = path;
    }

    void VisualizationListPanel::on_loadButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        //const auto defaultPath = QString("%1/preset").arg(QApplication::applicationDirPath());
        const auto defaultPath = QString("%1/preset").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();

        if (false == QDir().exists(prev)) {
            if (false == QDir().mkpath(prev)) {
                //QMessageBox::warning(nullptr, "Error", QString("Failed to make preset folder").arg(prev));
                //return;
                prev = defaultPath;
            }
        }

        const auto path = QFileDialog::getOpenFileName(this, "Select a preset file", prev, "TransferFunction (*.xml)");
        if (true == path.isEmpty()) {
            return;
        }

        for (auto i = 0; i < path.length(); i++) {
            if (path.at(i).unicode() > 127) {
                QMessageBox::warning(nullptr, "Error", QString("Preset path contains non-english characters"));
                return;
            }
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recent/preset", QFileInfo(path).absoluteDir().absolutePath());

        emit loadPreset(path);
    }
    
    auto VisualizationListPanel::SetEnableUI(const bool& enable) const ->void{
        d->ui->listComboBox->setEnabled(enable);
        d->ui->saveAsButton->setEnabled(enable);
        d->ui->saveButton->setEnabled(enable);
        d->ui->loadButton->setEnabled(enable);
    }

    auto VisualizationListPanel::SetCurrentLabel(const QString& text) const ->void {        
        //QFontMetrics fontMetrics(d->ui->currentLabel->font());
        //QString elidedText = fontMetrics.elidedText(text, Qt::ElideMiddle, this->width() - 30);

        //TC::SilentCall(d->ui->currentLabel)->setText(elidedText);
        //TC::SilentCall(d->ui->currentLabel)->setToolTip(elidedText);
        //Current Index를 지정해 주는 함수로 활용        
        auto name = QFileInfo(text).baseName();
        auto idx = 0;
        for(auto i=1 ; i< d->ui->listComboBox->count();i++) {
            auto ttext = d->ui->listComboBox->itemText(i);
            if(ttext == name) {
                idx = i;
                break;
            }
        }
        if(idx > 0) {
            TC::SilentCall(d->ui->listComboBox)->setCurrentIndex(idx);
        }
    }
}
