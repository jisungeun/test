#pragma once

#include <memory>
#include <QWidget>
#include <QAbstractButton>
#include <Scene.h>
#include <IViewingToolPanel.h>

#include "TomoAnalysis2dViewingToolPanelExport.h"

namespace TomoAnalysis::Viewer2D::Plugins {
    class TomoAnalysis2dViewingToolPanel_API ViewingToolPanel : public QWidget, public Interactor::IViewingToolPanel {
        Q_OBJECT

    public:
        typedef ViewingToolPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ViewingToolPanel(QWidget* parent=nullptr);
        ~ViewingToolPanel();

        auto UpdateCall() -> bool override;
        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void)->bool;

        void resizeEvent(QResizeEvent* event) override;

        auto force2D()->void;
        auto restore3D()->void;

    signals:
        void callUpdate();
        void layoutChanged(Entity::LayoutType);
        void resetLayoutTriggered();
        void saveLayoutTriggered(const QString&);
        void loadLayoutTriggered(const QString&);
        void orientationMarkerVisibilityChanged(bool);
        void boundaryBoxVisibilityChanged(bool);
        void axisGridVisibilityChanged(bool);
        void timestampVisibilityChanged(bool);
        void deviceVisibilityChanged(bool);        
        void timestampColorChanged(QColor);
        void deviceColorChanged(QColor);
        void timestampSizeChanged(int);
        void deviceSizeChanged(int);
        void titleVisibilityChanged(bool);
        void resolutionChanged(int);
        void axisGridFontSize(int);
        void axisGridColor(int, QColor);//0: x 1: y 2: z

    protected slots:
        void onUpdate();
        void onLayoutToolClicked(bool);
        void onLayoutChanged(QAbstractButton*);
        void onResetLayoutTriggered();
        void onSaveLayoutTriggered();
        void onLoadLayoutTriggered();
        void onViewToolClicked(bool);
        void onTimeStampColorClicked(bool);
        void onTimeStampSizeChanged(int);
        void onDeviceColorClicked(bool);
        void onDeviceStateChanged(int);
        void onDeviceSizeChanged(int);
        void onResolutionChanged(int);

    private:
        auto CreateViewingTool(bool is3D)->void;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}