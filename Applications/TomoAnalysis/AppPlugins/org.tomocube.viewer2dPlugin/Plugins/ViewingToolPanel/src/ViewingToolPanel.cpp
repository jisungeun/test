#include <iostream>
#include <windows.h>

#include <QMenu>
#include <QActionGroup>
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QButtonGroup>
#include <QWidgetAction>
#include <QSpinBox>
#include <QColorDialog>

#include <UIUtility.h>

#include "ui_ViewingToolPanel.h"
#include "ViewingToolPanel.h"

namespace  TomoAnalysis::Viewer2D::Plugins {
    using LayoutMapType = QMap<Entity::LayoutType, QString>;
    static const LayoutMapType layoutMap {
        { Entity::LayoutType::TwoByTwo,     "2x2 layout"  },
        { Entity::LayoutType::HSlicesBy3D,  "2D image left, 3D right" },
        { Entity::LayoutType::VSlicesBy3D,  "2D image top, 3D bottom" },
        { Entity::LayoutType::Only3D,       "Big 3D" },
        { Entity::LayoutType::XYPlane,      "XY Plane" },
        { Entity::LayoutType::YZPlane,      "YZ Plane" },
        { Entity::LayoutType::XZPlane,      "XZ Plane" },
        { Entity::LayoutType::HXY3D,        "XY Plane left, 3D right" },
        { Entity::LayoutType::VXY3D,        "XY Plane top, 3D bottom" }
    };

    struct ViewingToolPanel::Impl {
        Ui::ViewingToolPanel* ui{ nullptr };

        QButtonGroup* layoutButtons{ nullptr };
        
        QMenu* timestampSubmenu{ nullptr };
        QCheckBox* timestampCheckBox{ nullptr };
        QAction* timestampSeparator{ nullptr };
        QWidgetAction* timestampSize{ nullptr };
        QWidget* timestampSizeWidget{ nullptr };
        QSpinBox* timestampSizeSpin{ nullptr };
        QAction* timestampColor{ nullptr };

        QMenu* deviceInfoSubmenu{ nullptr };
        QCheckBox* deviceCheckBox{ nullptr };
        QAction* deviceSeperator{ nullptr };
        QWidgetAction* deviceSize{ nullptr };
        QWidget* deviceSizeWidget{ nullptr };
        QSpinBox* deviceSizeSpin{ nullptr };
        QAction* deviceColor{ nullptr };

        QMenu* mainMenu{nullptr};

        QWidget* resolutionWidget{ nullptr };
        QWidgetAction* resolutionSize{ nullptr };
        QSpinBox* resolutionSpin{ nullptr };        
        int color[3] = { 255,255,255 };
        int device_color[3] = { 255,255,255 };

        QPixmap timePixmap;
        QPixmap devicePixmap;
        bool showDevice{ false };

        bool is3D{ true };

        //Entity::LayoutType cur_3d_type{ Entity::LayoutType::HSlicesBy3D };
        Entity::LayoutType cur_3d_type{ Entity::LayoutType::XYPlane };
    };

    ViewingToolPanel::ViewingToolPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IViewingToolPanel()
    , d{ new Impl } {
        d->ui = new Ui::ViewingToolPanel();
        d->ui->setupUi(this);        
        
        d->ui->contentsWidget->setObjectName("panel-contents");        
        d->timePixmap = QPixmap(100, 100);
        d->timePixmap.fill(QColor(255, 255, 255, 255));

        d->devicePixmap = QPixmap(100, 100);
        d->devicePixmap.fill(QColor(255, 255, 255, 255));

        // add layout actions
        auto layoutMenu = new QMenu;        
        d->layoutButtons = new QButtonGroup;
        d->layoutButtons->setExclusive(true);

        LayoutMapType::const_iterator layoutIt = layoutMap.constBegin();
        while (layoutIt != layoutMap.constEnd()) {
            auto radioButton = new QRadioButton(layoutMenu);
            radioButton->setText(layoutIt.value());

            auto action = new QWidgetAction(layoutMenu);
            action->setDefaultWidget(radioButton);
            layoutMenu->addAction(action);

            d->layoutButtons->addButton(radioButton, layoutIt.key());

            // set default layout
            //if (layoutIt.key() == +Entity::LayoutType::HXY3D) {
            if (layoutIt.key() == +Entity::LayoutType::HSlicesBy3D) {
                radioButton->setChecked(true);
            }

            ++layoutIt;
        }

        connect(d->layoutButtons, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(onLayoutChanged(QAbstractButton*)));

        layoutMenu->addSeparator();
        auto resetLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-reset.svg"), "Reset");
        connect(resetLayoutAction, SIGNAL(triggered()), this, SLOT(onResetLayoutTriggered()));

        auto saveLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-save.svg"), "Save");
        connect(saveLayoutAction, SIGNAL(triggered()), this, SLOT(onSaveLayoutTriggered()));

        auto loadLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-load.svg"), "Load");
        connect(loadLayoutAction, SIGNAL(triggered()), this, SLOT(onLoadLayoutTriggered()));

        d->ui->layoutToolButton->setMenu(layoutMenu);
        
        connect(d->ui->layoutToolButton, SIGNAL(clicked(bool)), this, SLOT(onLayoutToolClicked(bool)));
        connect(layoutMenu, &QMenu::aboutToHide, [=]() {
            d->ui->layoutToolButton->blockSignals(true);
            d->ui->layoutToolButton->setChecked(false);
            d->ui->layoutToolButton->blockSignals(false);
            });
        connect(layoutMenu, &QMenu::aboutToShow, [=]() {
            d->ui->layoutToolButton->blockSignals(true);
            d->ui->layoutToolButton->setChecked(true);
            d->ui->layoutToolButton->blockSignals(false);
            });

        // add view actions
        //auto d->mainMenu = new QMenu;
        d->mainMenu = new QMenu;

        CreateViewingTool(true);

        d->ui->viewToolButton->setMenu(d->mainMenu);                

        connect(d->ui->viewToolButton, SIGNAL(clicked(bool)), this, SLOT(onViewToolClicked(bool)));
        connect(d->mainMenu, &QMenu::aboutToHide, [=]() {
            d->ui->viewToolButton->blockSignals(true);
            d->ui->viewToolButton->setChecked(false);
            d->ui->viewToolButton->blockSignals(false);
        });
        connect(d->mainMenu, &QMenu::aboutToShow, [=]() {
            d->ui->viewToolButton->blockSignals(true);
            d->ui->viewToolButton->setChecked(true);
            d->ui->viewToolButton->blockSignals(false);
        });

        // set object names
        d->ui->layoutToolButton->setObjectName("bt-tool-round");
        d->ui->viewToolButton->setObjectName("bt-tool-round");

        const auto ds = GetViewingToolDS();

        d->ui->layoutToolButton->hide();

        connect(this, SIGNAL(callUpdate()), this, SLOT(onUpdate()));
    }

    auto ViewingToolPanel::UpdateCall() -> bool {
        emit callUpdate();
        return true;
    }

    void ViewingToolPanel::onUpdate() {
        this->Update();
    }

    ViewingToolPanel::~ViewingToolPanel() {
        delete d->ui;
    }

    void ViewingToolPanel::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
        //auto d->mainMenu = d->ui->viewToolButton->menu();
        if (nullptr != d->mainMenu) {
            d->mainMenu->setMinimumWidth(d->ui->viewToolButton->width());
        }
        auto layMenu = d->ui->layoutToolButton->menu();
        if(nullptr != layMenu) {
            layMenu->setMinimumWidth(d->ui->layoutToolButton->width());
        }                
    }

    auto ViewingToolPanel::force2D() -> void {
        CreateViewingTool(false);
        d->is3D = false;
        emit layoutChanged(Entity::LayoutType::XYPlane);
        d->ui->layoutToolButton->setEnabled(false);        
        //d->ui->viewToolButton->setEnabled(false);        
    }

    auto ViewingToolPanel::restore3D()->void {        
        CreateViewingTool(true);
        d->is3D = true;
        emit layoutChanged(d->cur_3d_type);
        d->ui->layoutToolButton->setEnabled(true);        
        d->ui->viewToolButton->setEnabled(true);        
    }

    auto ViewingToolPanel::CreateViewingTool(bool is3D) -> void {
        d->mainMenu->clear();
        // add timestamp check box
        d->timestampCheckBox = new QCheckBox(d->mainMenu);
        d->timestampCheckBox->setText("Show TimeStamp");
        connect(d->timestampCheckBox, &QCheckBox::stateChanged,
            [=](int state) { emit timestampVisibilityChanged(state == Qt::Checked); });

        auto timestampAction = new QWidgetAction(d->mainMenu);
        timestampAction->setDefaultWidget(d->timestampCheckBox);

        d->timestampSubmenu = new QMenu(d->mainMenu);
        d->timestampSubmenu->setTitle("TimeStamp");

        d->timestampSubmenu->addAction(timestampAction);

        d->timestampSeparator = d->timestampSubmenu->addSeparator();

        //QIcon textIcon()
        QIcon texIcon(d->timePixmap);
        d->timestampColor = new QAction(texIcon, "Change Color");
        d->timestampSubmenu->addAction(d->timestampColor);

        d->timestampSize = new QWidgetAction(d->mainMenu);
        d->timestampSizeSpin = new QSpinBox(d->mainMenu);
        d->timestampSizeSpin->setRange(10, 40);
        d->timestampSizeSpin->setValue(20);

        connect(d->timestampSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(onTimeStampSizeChanged(int)));
        connect(d->timestampColor, SIGNAL(triggered(bool)), this, SLOT(onTimeStampColorClicked(bool)));

        d->timestampSizeWidget = new QWidget(d->mainMenu);
        auto hlayout = new QHBoxLayout;
        d->timestampSizeWidget->setLayout(hlayout);
        auto label = new QLabel("Font Size ");
        auto font = label->font();
        font.setBold(true);
        label->setFont(font);
        label->setObjectName("h7");
        hlayout->addWidget(label);
        hlayout->addWidget(d->timestampSizeSpin);

        d->timestampSize->setDefaultWidget(d->timestampSizeWidget);

        d->mainMenu->addMenu(d->timestampSubmenu);

        //device info control
        d->deviceCheckBox = new QCheckBox(d->mainMenu);
        d->deviceCheckBox->setText("Show device Info.");
        connect(d->deviceCheckBox, &QCheckBox::stateChanged,
            [=](int state) { onDeviceStateChanged(state); });

        auto deviceAction = new QWidgetAction(d->mainMenu);
        deviceAction->setDefaultWidget(d->deviceCheckBox);

        d->deviceInfoSubmenu = new QMenu(d->mainMenu);
        d->deviceInfoSubmenu->setTitle("Device");

        d->deviceInfoSubmenu->addAction(deviceAction);

        d->deviceSeperator = d->deviceInfoSubmenu->addSeparator();

        //QIcon textIcon()
        QIcon deviceIcon(d->devicePixmap);
        d->deviceColor = new QAction(deviceIcon, "Change Color");
        d->deviceInfoSubmenu->addAction(d->deviceColor);

        d->deviceSize = new QWidgetAction(d->mainMenu);
        d->deviceSizeSpin = new QSpinBox(d->mainMenu);
        d->deviceSizeSpin->setRange(10, 40);
        d->deviceSizeSpin->setValue(20);                

        connect(d->deviceSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(onDeviceSizeChanged(int)));
        connect(d->deviceColor, SIGNAL(triggered(bool)), this, SLOT(onDeviceColorClicked(bool)));

        d->deviceSizeWidget = new QWidget(d->mainMenu);
        auto dhlayout = new QHBoxLayout;
        d->deviceSizeWidget->setLayout(dhlayout);
        auto dlabel = new QLabel("Font Size ");
        auto dfont = dlabel->font();
        dfont.setBold(true);
        dlabel->setFont(dfont);
        dlabel->setObjectName("h7");
        dhlayout->addWidget(dlabel);
        dhlayout->addWidget(d->deviceSizeSpin);

        d->deviceSize->setDefaultWidget(d->deviceSizeWidget);

        d->mainMenu->addMenu(d->deviceInfoSubmenu);

        //add resolution control
        d->resolutionSize = new QWidgetAction(d->mainMenu);
        d->resolutionSpin = new QSpinBox(d->mainMenu);
        d->resolutionSpin->setRange(-1, 0);
        d->resolutionSpin->setValue(-1);
        connect(d->resolutionSpin, SIGNAL(valueChanged(int)), this, SLOT(onResolutionChanged(int)));

        d->resolutionWidget = new QWidget(d->mainMenu);
        auto resolayout = new QHBoxLayout;
        d->resolutionWidget->setLayout(resolayout);
        auto resolabel = new QLabel("Resolution");
        auto resofont = resolabel->font();
        resofont.setBold(true);
        resolabel->setFont(resofont);
        resolabel->setObjectName("h7");
        resolayout->addWidget(resolabel);
        resolayout->addWidget(d->resolutionSpin);

        d->resolutionSize->setDefaultWidget(d->resolutionWidget);
        d->mainMenu->addAction(d->resolutionSize);
    }

    auto ViewingToolPanel::Update()->bool {        
        const auto ds = GetViewingToolDS();
        if (!ds) {
            return false;
        }

        blockSignals(true);

        //auto menu = d->ui->viewToolButton->menu();

        auto layoutText = layoutMap[ds->layoutType];        
        for (auto l : d->layoutButtons->buttons()) {
            if(l->text().compare(layoutText)==0) {
                auto r = static_cast<QRadioButton*>(l);
                r->setChecked(true);
            }            
        }

        if (ds->changeRes) {
            d->resolutionSpin->setRange(-1, ds->maxRes);
            ds->changeRes = false;
            d->resolutionSpin->setValue(-1);
        }

        if(ds->showTimeStamp) {
            d->timestampCheckBox->setChecked(true);
            d->timestampSubmenu->addAction(d->timestampSeparator);
            d->timestampSubmenu->addAction(d->timestampColor);
            d->timestampSubmenu->addAction(d->timestampSize);
        }else {
            d->timestampCheckBox->setChecked(false);
            d->timestampSubmenu->removeAction(d->timestampSeparator);
            d->timestampSubmenu->removeAction(d->timestampColor);
            d->timestampSubmenu->removeAction(d->timestampSize);
        }

        if(d->showDevice) {
            d->deviceCheckBox->setChecked(true);
            d->deviceInfoSubmenu->addAction(d->deviceSeperator);
            d->deviceInfoSubmenu->addAction(d->deviceColor);
            d->deviceInfoSubmenu->addAction(d->deviceSize);
        }else {
            d->deviceCheckBox->setChecked(false);
            d->deviceInfoSubmenu->removeAction(d->deviceSeperator);
            d->deviceInfoSubmenu->removeAction(d->deviceColor);
            d->deviceInfoSubmenu->removeAction(d->deviceSize);
        }

        for(auto i=0;i<3;i++) {
            d->color[i] = ds->timestampColor[i];
        }
        d->timePixmap.fill(QColor(d->color[0], d->color[1], d->color[2], 255));
        d->timestampColor->setIcon(QIcon(d->timePixmap));

        d->devicePixmap.fill(QColor(d->device_color[0], d->device_color[1], d->device_color[2], 255));
        d->deviceColor->setIcon(QIcon(d->devicePixmap));
        
        blockSignals(false);
        return true;
    }

    auto ViewingToolPanel::Init(void) const ->bool {
        return true;
    }

    auto ViewingToolPanel::Reset(void) ->bool {
        blockSignals(true);

        auto ds = GetViewingToolDS();
        ds->Clear();
        d->timestampCheckBox->setChecked(false);
        d->timestampSizeSpin->setValue(20);
        
        for(auto i=0;i<3;i++) {
            d->color[i] = 255;
            d->device_color[i] = 255;
        }
        QColor white_col = QColor(255, 255, 255);
       
        d->timePixmap.fill(white_col);
        d->timestampColor->setIcon(QIcon(d->timePixmap));

        d->devicePixmap.fill(white_col);
        d->deviceColor->setIcon(QIcon(d->devicePixmap));

        d->showDevice = false;
        d->deviceSizeSpin->setValue(20);

        auto layoutText = layoutMap[Entity::LayoutType::HSlicesBy3D];
        for (auto l : d->layoutButtons->buttons()) {
            if (l->text().compare(layoutText) == 0) {
                auto r = static_cast<QRadioButton*>(l);// ->clicked(true);
                r->setChecked(true);
            }
        }

        blockSignals(false);
        return true;
    }

    void ViewingToolPanel::onLayoutToolClicked(bool checked) {
        if (checked) {
            d->ui->layoutToolButton->showMenu();
        }
        else {
            auto menu = d->ui->layoutToolButton->menu();
            menu->hide();
        }
    }

    void ViewingToolPanel::onLayoutChanged(QAbstractButton* button) {
        emit layoutChanged(Entity::LayoutType::_from_integral(d->layoutButtons->id(button)));
        d->cur_3d_type = Entity::LayoutType::_from_integral(d->layoutButtons->id(button));
    }

    void ViewingToolPanel::onResetLayoutTriggered() {
        const auto answer = QMessageBox::question(
            nullptr,
            "Reset layout",
            "Are you sure you want to reset layout?",
            QMessageBox::Yes | QMessageBox::No
        );

        if (QMessageBox::No == answer) {
            return;
        }

        emit resetLayoutTriggered();
    }


    void ViewingToolPanel::onSaveLayoutTriggered() {
        const auto prevPath = QSettings("Tomocube", "TomoAnalysis").value("recentSaveLayout").toString();

        const QString fileName = QFileDialog::getSaveFileName(
            nullptr,
            tr("Save layout file"),
            prevPath,
            tr("Layout file (*.layout)")
        );

        if (fileName.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentSaveLayout", fileName);

        emit saveLayoutTriggered(fileName);
    }

    void ViewingToolPanel::onLoadLayoutTriggered() {
        const auto prevPath = QSettings("Tomocube", "TomoAnalysis").value("recentLoadLayout").toString();

        const auto fileName = QFileDialog::getOpenFileName(
            nullptr,
            "Select a layout file.",
            prevPath,
             "*.layout"
        );

        if (fileName.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentLoadLayout", fileName);

        emit loadLayoutTriggered(fileName);
    }

    void ViewingToolPanel::onViewToolClicked(bool checked) {
        if (checked) {
            d->ui->viewToolButton->showMenu();
        } else {
            auto menu = d->ui->viewToolButton->menu();
            menu->hide();
        }
    }

    void ViewingToolPanel::onDeviceColorClicked(bool /*nouse*/) {
        QColor new_col = QColorDialog::getColor(QColor(d->device_color[0], d->device_color[1], d->device_color[2], 255), nullptr, "Select Color");
        if (new_col.isValid()) {
            emit deviceColorChanged(new_col);
            d->device_color[0] = new_col.red();
            d->device_color[1] = new_col.green();
            d->device_color[2] = new_col.blue();

            this->Update();
        }
    }


    void ViewingToolPanel::onTimeStampColorClicked(bool not_used) {
        Q_UNUSED(not_used)
        QColor new_col = QColorDialog::getColor(QColor(d->color[0],d->color[1],d->color[2],255), nullptr, "Select Color");
        if(new_col.isValid()) {
            emit timestampColorChanged(new_col);
            d->timePixmap.fill(new_col);
            d->timestampColor->setIcon(QIcon(d->timePixmap));
        }
    }

    void ViewingToolPanel::onDeviceStateChanged(int /*unused*/) {
        d->showDevice = d->deviceCheckBox->isChecked();
        emit deviceVisibilityChanged(d->showDevice);
        this->Update();
    }

    void ViewingToolPanel::onDeviceSizeChanged(int size) {
        emit deviceSizeChanged(size);
    }

    void ViewingToolPanel::onTimeStampSizeChanged(int size) {        
        emit timestampSizeChanged(size);
    }

    void ViewingToolPanel::onResolutionChanged(int res) {
        emit resolutionChanged(res);        
    }             
}
