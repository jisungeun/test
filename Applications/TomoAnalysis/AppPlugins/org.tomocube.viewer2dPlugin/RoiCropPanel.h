#pragma once
#include <memory>
#include <QWidget>

namespace TomoAnalysis::Viewer2D {
    class RoiCropPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = RoiCropPanel;

        RoiCropPanel(QWidget* parent = nullptr);
        ~RoiCropPanel()override;

        auto ClearTable(bool sendSignal)->void;
        auto SetIsLDM(bool isLDM)->void;

    protected slots:
        void onAddButtonClicked();        
        void onRemoveButtonClicked();
        void onClearButtonClicked();
        void on3dButtonClicked();
        void onRoiVizClicked();
        void onModifyBtnClicked();

        void onTurnOffModification();
                
        void onRoiInfoAdded(float xmin, float xmax, float ymin, float ymax);
        void onRoiInfoChanged(int idx, float xmin, float xmax, float ymin, float ymax);

        void onSelectionChanged();

    signals:
        void sigRoiToggle(bool);
        void sigModifyRoi(bool);
        void sigAddRoi();
        void sigRemoveRoi(int idx);
        void sigClearRoi();
        void sig3dViewer(int idx);
        void sig3dViewerAll();
        void sigFocusRoi(int idx);

        void sigTurnOffModify(bool);

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}