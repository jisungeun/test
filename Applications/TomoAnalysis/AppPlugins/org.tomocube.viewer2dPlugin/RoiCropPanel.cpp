#define LOGGER_TAG "[RoiCropPanel]"

#include <iostream>

#include <TCLogger.h>
#include <MessageDialog.h>

#include "RoiCropPanel.h"
#include "ui_RoiCropPanel.h"

namespace TomoAnalysis::Viewer2D {
    struct RoiCropPanel::Impl {
        Ui::RoiCropUi ui;
        QStringList headers{ "xRange","yRange"};
        bool isLDM{ nullptr };
        auto getSelectionID()->int {
            auto selected_items = ui.roiTable->selectedItems();
            if (selected_items.count() < 1) {
                return -1;
            }
            auto idx = ui.roiTable->selectedItems()[0]->row();
            return idx;
        }
        auto clearTable()->void {
            ui.roiTable->clearContents();
            ui.roiTable->setRowCount(1);
            for (auto i = 0; i < 2; i++) {
                auto item = new QTableWidgetItem("All");
                item->setTextAlignment(Qt::AlignCenter);
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                ui.roiTable->setItem(0, i, item);
            }
        }
    };
    RoiCropPanel::RoiCropPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        this->Init();
    }
    RoiCropPanel::~RoiCropPanel() {
        
    }
    auto RoiCropPanel::Init() -> void {
        setObjectName("panel-base");
        d->ui.roiTable->setColumnCount(2);
        d->ui.roiTable->setHorizontalHeaderLabels(d->headers);        
        d->ui.roiTable->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.roiTable->setSelectionBehavior(QAbstractItemView::SelectRows);

        d->ui.modifyRoiBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.addRoiBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.vizBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.clearRoiBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.removeRoiBtn->setStyleSheet("font-size: 11px; font-weight: bold;");

        d->ui.modifyRoiBtn->setCheckable(true);

        connect(d->ui.addRoiBtn, SIGNAL(clicked()), this, SLOT(onAddButtonClicked()));
        connect(d->ui.removeRoiBtn, SIGNAL(clicked()), this, SLOT(onRemoveButtonClicked()));
        connect(d->ui.clearRoiBtn, SIGNAL(clicked()), this, SLOT(onClearButtonClicked()));
        connect(d->ui.vizBtn, SIGNAL(clicked()), this, SLOT(on3dButtonClicked()));
        connect(d->ui.roiVizChk, SIGNAL(clicked()), this, SLOT(onRoiVizClicked()));        
        connect(d->ui.modifyRoiBtn, SIGNAL(clicked()), this, SLOT(onModifyBtnClicked()));        

        d->clearTable();
        connect(d->ui.roiTable, SIGNAL(itemSelectionChanged()), this, SLOT(onSelectionChanged()));
    }
    void RoiCropPanel::onSelectionChanged() {
        auto selectionID = d->getSelectionID();
        if(selectionID<1) {
            emit sigFocusRoi(-1);
            return;
        }
        emit sigFocusRoi(selectionID - 1);
    }
    void RoiCropPanel::onRoiVizClicked() {
        auto showROI = d->ui.roiVizChk->isChecked();
        emit sigRoiToggle(showROI);
    }
    void RoiCropPanel::onAddButtonClicked() {
        d->ui.modifyRoiBtn->setStyleSheet("");
        d->ui.modifyRoiBtn->setChecked(false);
        d->ui.addRoiBtn->setStyleSheet("background:rgb(75,180,30)");
        d->ui.roiVizChk->blockSignals(true);
        d->ui.roiVizChk->setChecked(true);
        d->ui.roiVizChk->blockSignals(false);
        emit sigAddRoi();
    }
    void RoiCropPanel::onModifyBtnClicked() {
        auto underModification = !d->ui.modifyRoiBtn->isChecked();
        if(false == underModification) {
            d->ui.addRoiBtn->setStyleSheet("");
            d->ui.roiVizChk->blockSignals(true);
            d->ui.roiVizChk->setChecked(true);
            d->ui.roiVizChk->blockSignals(false);
            d->ui.modifyRoiBtn->setStyleSheet("background:rgb(125,125,0)");
        }else {
            d->ui.modifyRoiBtn->setStyleSheet("");
        }        
        emit sigModifyRoi(underModification);
    }

    void RoiCropPanel::onTurnOffModification() {
        auto underModification = d->ui.modifyRoiBtn->isChecked();
        if(underModification) {
            d->ui.modifyRoiBtn->setChecked(false);
            d->ui.modifyRoiBtn->setStyleSheet("");
            emit sigTurnOffModify(underModification);
        }        
    }


    void RoiCropPanel::onRemoveButtonClicked() {
        auto selectionID = d->getSelectionID();
        if(selectionID<1) {
            return;
        }

        d->ui.roiTable->removeRow(selectionID);

        //ID 0 is for Whole Image Range
        emit sigRemoveRoi(selectionID - 1);
    }
    auto RoiCropPanel::ClearTable(bool sendSignal) -> void {
        d->clearTable();
        if (sendSignal) {
            emit sigClearRoi();
        }
    }

    auto RoiCropPanel::SetIsLDM(bool isLDM) -> void {
        d->isLDM = isLDM;
        d->ui.addRoiBtn->setEnabled(!isLDM);
        d->ui.clearRoiBtn->setEnabled(!isLDM);
        d->ui.modifyRoiBtn->setEnabled(!isLDM);
        d->ui.removeRoiBtn->setEnabled(!isLDM);
        d->ui.roiVizChk->setEnabled(!isLDM);
    }

    void RoiCropPanel::onClearButtonClicked(){
        d->clearTable();
        emit sigClearRoi();
    }
    void RoiCropPanel::on3dButtonClicked() {
        auto selectionID = d->getSelectionID();
        d->ui.roiTable->clearSelection();
        if(selectionID<1) {
            emit sig3dViewerAll();
            return;
        }        
        emit sig3dViewer(selectionID-1);
    }
    void RoiCropPanel::onRoiInfoAdded(float xmin, float xmax, float ymin, float ymax) {
        d->ui.addRoiBtn->setStyleSheet("");
        if(xmin < 0) {
            return;
        }
        auto cur_row_count = d->ui.roiTable->rowCount();
        d->ui.roiTable->setRowCount(cur_row_count+1);
        auto xText = QString("[%1 um ~ %2 um]").arg(QString::number(xmin,'f',2)).arg(QString::number(xmax,'f',2));
        xText.replace("u", QString::fromWCharArray(L"\x00B5"));
        auto xItem = new QTableWidgetItem(xText);
        xItem->setTextAlignment(Qt::AlignCenter);
        xItem->setFlags(xItem->flags() ^ Qt::ItemIsEditable);
        d->ui.roiTable->setItem(cur_row_count, 0, xItem);

        auto yText = QString("[%1 um ~ %2 um]").arg(QString::number(ymin,'f',2)).arg(QString::number(ymax,'f',2));
        yText.replace("u", QString::fromWCharArray(L"\x00B5"));
        auto yItem = new QTableWidgetItem(yText);
        yItem->setTextAlignment(Qt::AlignCenter);
        yItem->setFlags(yItem->flags() ^ Qt::ItemIsEditable);
        d->ui.roiTable->setItem(cur_row_count, 1, yItem);

        d->ui.roiTable->selectRow(cur_row_count);
        d->ui.roiTable->resizeColumnsToContents();
    }
    void RoiCropPanel::onRoiInfoChanged(int idx, float xmin, float xmax, float ymin, float ymax) {
        auto cur_row_count = d->ui.roiTable->rowCount();
        if(idx >= cur_row_count) {
            return;
        }
        auto xText = QString("[%1 um ~ %2 um]").arg(QString::number(xmin,'f',2)).arg(QString::number(xmax,'f',2));
        xText.replace("u", QString::fromWCharArray(L"\x00B5"));
        auto yText = QString("[%1 um ~ %2 um]").arg(QString::number(ymin,'f',2)).arg(QString::number(ymax,'f',2));
        yText.replace("u", QString::fromWCharArray(L"\x00B5"));
        d->ui.roiTable->item(idx+1, 0)->setText(xText);
        d->ui.roiTable->item(idx+1, 1)->setText(yText);

        d->ui.roiTable->selectRow(idx+1);
    }
}