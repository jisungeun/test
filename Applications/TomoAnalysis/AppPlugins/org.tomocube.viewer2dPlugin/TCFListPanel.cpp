#define LOGGER_TAG "[TCFListPanel]"

#include <iostream>

#include <TCLogger.h>
#include <MessageDialog.h>

#include <QFileInfo>
#include <QDateTime>

#include <TCFMetaReader.h>

#include "TCFListPanel.h"
#include "ui_TCFListPanel.h"

namespace TomoAnalysis::Viewer2D {
    struct TCFListPanel::Impl {
        Ui::tcfListUi ui;
        QStringList headers{"Date","Time","Name","HT","FL","BF"};
        
        auto ParseTcfName(const QString& path)->QStringList {
            auto result = QStringList({ "","","","0","0","0" });
            if (true == path.isEmpty()) {
                return result;
            }
            auto GetSimpleName = [=](const QString& name) {
                QFileInfo info(name);
                auto simple = info.fileName();
                if (20 > simple.size()) {
                    return simple;
                }

                const auto list = simple.split(".");
                if (3 > list.size()) {
                    return simple;
                }

                simple = simple.remove(0, 20);
                return simple.chopped(4);
            };
            auto GetSimpleNameHTX = [=](const QString& name) {
                QFileInfo info(name);
                auto simple = info.fileName();
                if (13 > simple.size()) {
                    return simple;
                }

                const auto list = simple.split(".");
                if (7 > list.size()) {
                    return simple;
                }                

                QString result = QString();
                result += list[2];
                for (auto i = 3; i < list.count() - 1;i++) {
                    result += ".";
                    result += list[i];
                }
                return result;
            };
            auto reader = TC::IO::TCFMetaReader();            
            const auto meta = reader.Read(path);            
            result[0] = QDateTime::fromString(meta->common.recordingTime, "yyyy-MM-dd hh:mm:ss.zzz").toString("yyyy.MM.dd");
            result[1] = QDateTime::fromString(meta->common.recordingTime, "yyyy-MM-dd hh:mm:ss.zzz").toString("hh:mm:ss");
            if (meta->common.deviceModelType == "HTX") {
                result[2] = GetSimpleNameHTX(path);
            }else {
                result[2] = GetSimpleName(path);
            }
            if (meta->data.data2DMIP.dataCount > 0) {
                result[3] = QString::number(meta->data.data2DMIP.dataCount);
            }else {
                result[3] = QString::number(meta->data.data3D.dataCount);
            }
            if(meta->data.data2DFLMIP.dataCount>0) {
                result[4] = QString::number(meta->data.data2DFLMIP.dataCount);
            }else {
                result[4] = QString::number(meta->data.data3DFL.dataCount);
            }
            result[5] = QString::number(meta->data.dataBF.dataCount);
            
            return result;
        }
    };
    TCFListPanel::TCFListPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        this->Init();
    }
    TCFListPanel::~TCFListPanel() {
        
    }

    auto TCFListPanel::Init() -> void {
        d->ui.tcfListTable->setColumnCount(6);
        d->ui.tcfListTable->setHorizontalHeaderLabels(d->headers);
        d->ui.tcfListTable->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.tcfListTable->setSelectionBehavior(QAbstractItemView::SelectRows);

        connect(d->ui.tcfListTable, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(onTcfSelectionChanged(int, int)));
    }
    void TCFListPanel::onTcfSelectionChanged(int row, int column) {
        emit sigSelectTCF(row);
    }

    void TCFListPanel::onUpdateTcfList(const QStringList& tcfList) {
        d->ui.tcfListTable->clearContents();
        d->ui.tcfListTable->setRowCount(tcfList.count());

        for (auto i = 0; i < tcfList.count();i++) {
            auto tcf = tcfList[i];
            auto result = d->ParseTcfName(tcf);            
            for(auto j=0;j<d->headers.count();j++) {
                auto item = new QTableWidgetItem(result[j]);
                item->setWhatsThis(tcf);
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                d->ui.tcfListTable->setItem(i, j, item);
            }
        }
        d->ui.tcfListTable->resizeColumnsToContents();            
    }
}