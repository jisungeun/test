#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.viewer2dPlugin";

namespace TomoAnalysis::Viewer2D::AppUI {
	class viewerPlugin : public TC::Framework::IAppActivator {
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.viewer2dPlugin")
			
	public:
		static viewerPlugin* getInstance();

		viewerPlugin();
		~viewerPlugin();

		auto start(ctkPluginContext* context)->void override;
		auto stop(ctkPluginContext* context)->void override;
		auto getPluginContext()->ctkPluginContext* const override;				
		
	private:		
		struct Impl;
		std::unique_ptr<Impl> d;

		static viewerPlugin* instance;		
	};
}