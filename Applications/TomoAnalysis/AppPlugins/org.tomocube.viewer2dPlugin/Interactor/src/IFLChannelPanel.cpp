#include "IFLChannelPanel.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct IFLChannelPanel::Impl {
        FLMetaDS::Pointer flMetaDS = std::make_shared<FLMetaDS>();
    };

    IFLChannelPanel::IFLChannelPanel() : d{ new Impl } {
    }

    IFLChannelPanel::~IFLChannelPanel() {
    }

    auto IFLChannelPanel::GetFLMetaDS() const->FLMetaDS::Pointer {
        return d->flMetaDS;
    }
}