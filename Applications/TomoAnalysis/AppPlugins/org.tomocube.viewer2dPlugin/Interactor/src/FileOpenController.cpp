#include <iostream>

#include <ImageLoader.h>

#include "FileOpenController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct FileOpenController::Impl {
		Impl() {}
		Impl(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort)
	    : outPort(outPort)
	    , readerPort(readerPort) {}

		UseCase::ITCFInfoOutputPort* outPort{ nullptr };
		UseCase::IFileReaderPort* readerPort{ nullptr };
	};

	FileOpenController::FileOpenController()
		: d(new Impl()) {
	}

	FileOpenController::FileOpenController(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort)
		: d(new Impl(outPort, readerPort)) {
	}

	FileOpenController::~FileOpenController() {
	}

	auto FileOpenController::LoadImages(Entity::Scene::ID sceneID, const std::string& path) const -> bool {
		auto use_case = UseCase::ImageLoader();
		return use_case.Load(path, sceneID, d->outPort, d->readerPort);
	}
}
