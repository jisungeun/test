#include "IVisualizationListPanel.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct IVisualizationListPanel::Impl {
        CommonDS::Pointer commonDS = std::make_shared<CommonDS>();
        ListDS::Pointer listDS = std::make_shared<ListDS>();
        PresetDS::Pointer presetDS = std::make_shared<PresetDS>();
    };

    IVisualizationListPanel::IVisualizationListPanel() : d{ new Impl } {
    }

    IVisualizationListPanel::~IVisualizationListPanel() {
    }

    auto IVisualizationListPanel::GetCommonDS()->CommonDS::Pointer {
        return d->commonDS;
    }

    auto IVisualizationListPanel::GetListDS()->ListDS::Pointer {
        return d->listDS;
    }

    auto IVisualizationListPanel::GetPresetDS()->PresetDS::Pointer {
        return d->presetDS;
    }
}