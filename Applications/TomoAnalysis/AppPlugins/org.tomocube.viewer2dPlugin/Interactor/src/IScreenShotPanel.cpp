#include "IScreenShotPanel.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct IScreenShotPanel::Impl {
        ScreenShotDS::Pointer screenShotDS = std::make_shared<ScreenShotDS>();
    };

    IScreenShotPanel::IScreenShotPanel() : d{ new Impl } {
    }

    IScreenShotPanel::~IScreenShotPanel() {
    }

    auto IScreenShotPanel::GetScreenShotDS() const ->ScreenShotDS::Pointer {
        return d->screenShotDS;
    }
}