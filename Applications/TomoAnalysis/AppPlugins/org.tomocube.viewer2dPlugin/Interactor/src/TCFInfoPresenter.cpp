#include <memory>
#include <QDir>
#include <QFileInfo>

#include "TCFInfoPresenter.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct TCFInfoPresenter::Impl {
        Impl(){}

		IImageViewer* viewerPanel = nullptr;
        IModalityPanel* modalityPanel = nullptr;
		IDisplayPanel* displayPanel = nullptr;
        ITransferFunctionPanel* transferFunctionPanel = nullptr;
		IFLChannelPanel* flChannelPanel = nullptr;
        ITCFPlayerPanel* tcfPlayerPanel = nullptr;
		//IScreenShotPanel* screenShotPanel = nullptr;
		//IRecordVideoPanel* recordVideoPanel = nullptr;
		INavigatorPanel* navigatorPanel = nullptr;
		IVisualizationListPanel* visualizationListPanel = nullptr;
		IViewingToolPanel* viewingToolPanel = nullptr;
    };

    TCFInfoPresenter::TCFInfoPresenter()
        : d(new Impl()) {
    }

	TCFInfoPresenter::TCFInfoPresenter(IImageViewer* viewerPanel,
		                               IModalityPanel* modalityPanel, IDisplayPanel* displayPanel,
									   ITransferFunctionPanel* transferFunctionPanel,
									   IFLChannelPanel* flChannelPanel, ITCFPlayerPanel* tcfPlayerPanel,
		                               /*IScreenShotPanel* screenShotPanel,*/ /*IRecordVideoPanel* recordVideoPanel,*/
		                               INavigatorPanel* navigatorPanel, IVisualizationListPanel* visualizationListPanel,IViewingToolPanel* viewingToolPanel)
									   : d(new Impl()) {
		d->viewerPanel = viewerPanel;
        d->modalityPanel = modalityPanel;
		d->displayPanel = displayPanel;
        d->transferFunctionPanel = transferFunctionPanel;
		d->flChannelPanel = flChannelPanel;
        d->tcfPlayerPanel = tcfPlayerPanel;
		//d->screenShotPanel = screenShotPanel;
		//d->recordVideoPanel = recordVideoPanel;
		d->navigatorPanel = navigatorPanel;
		d->visualizationListPanel = visualizationListPanel;
		d->viewingToolPanel = viewingToolPanel;
    }

    TCFInfoPresenter::~TCFInfoPresenter() {

    }

    void TCFInfoPresenter::Update(Entity::Image::Pointer& image) {
		if (nullptr == d->viewerPanel) {
			return;
		}

        if(nullptr == d->modalityPanel) {
		    return;
		}

		if (nullptr == d->displayPanel) {
			return;
		}

		if (nullptr == d->transferFunctionPanel) {
			return;
		}

		if (nullptr == d->flChannelPanel) {
			return;
		}

		if (nullptr == d->tcfPlayerPanel) {
			return;
		}

		/*if (nullptr == d->screenShotPanel) {
			return;
		}*/

		/*if (nullptr == d->recordVideoPanel) {
			return;
		}*/

		if (nullptr == d->navigatorPanel) {
			return;
		}

		if (nullptr == d->visualizationListPanel) {
			return;
		}

		if(nullptr == d->viewingToolPanel) {
			return;
		}

		// set image path for reader
		auto fileDS = d->viewerPanel->GetFileDS();
		fileDS->path = image->GetPath();
        // get usable modality
		auto modalityDS = d->modalityPanel->GetModalityDS();

		int usableModality = Entity::Modality::None;
		if (image->HasType(Entity::ImageType::HT3D)) {
			usableModality = static_cast<Entity::Modality>(usableModality | Entity::Modality::HTVolume);
		}

		if (image->HasType(Entity::ImageType::FL3D)) {
			usableModality = static_cast<Entity::Modality>(usableModality | Entity::Modality::FLVolume);
		}

		if (image->HasType(Entity::ImageType::HT2D)) {
			usableModality = static_cast<Entity::Modality>(usableModality | Entity::Modality::HTMIP);
		}

		if (image->HasType(Entity::ImageType::FL2D)) {
			usableModality = static_cast<Entity::Modality>(usableModality | Entity::Modality::FLMIP);
		}

		if(image->HasType(Entity::ImageType::BF)) {
			usableModality = static_cast<Entity::Modality>(usableModality | Entity::Modality::BF2D);
		}

		modalityDS->usableModality = usableModality;
		
		// set usable display type
		int usableDisplayType = Entity::Modality::None;
		if (modalityDS->usableModality & Entity::Modality::HTVolume || modalityDS->usableModality & Entity::Modality::FLVolume) {
			usableDisplayType |= Entity::DisplayType::DISPLAY_3D;
		}

		if (modalityDS->usableModality & Entity::Modality::HTMIP
			|| modalityDS->usableModality & Entity::Modality::FLMIP
			|| modalityDS->usableModality & Entity::Modality::BF2D) {			
			usableDisplayType |= Entity::DisplayType::DISPLAY_2D;
		}
		auto displayDS = d->displayPanel->GetDisplayDS();
		displayDS->usableDisplayType = static_cast<Entity::DisplayType>(usableDisplayType);
		// get FL channel min, max
		auto flMetaDS = d->flChannelPanel->GetFLMetaDS();

		Entity::Image::Channel* channel{nullptr};
		if (true == image->HasType(Entity::ImageType::FL3D)) {
			channel = image->GetChannel(Entity::ImageType::FL3D);

			flMetaDS->isExistFL = true;
		}
		else if (true == image->HasType(Entity::ImageType::FL2D)) {
			channel = image->GetChannel(Entity::ImageType::FL2D);

			flMetaDS->isExistFL = true;
		}
		else {
			flMetaDS->isExistFL = false;
		}

		if(true == flMetaDS->isExistFL) {
			for (int i = 0; i < 3; ++i) {
				if (false == channel[i].isValid) {
					flMetaDS->flIntensity[i].isVisible = false;
					flMetaDS->flIntensity[i].isValid = false;
					flMetaDS->flIntensity[i].min = -1;
					flMetaDS->flIntensity[i].max = -1;
					flMetaDS->flIntensity[i].opacity = -1;
					flMetaDS->flName[i] = "";
					flMetaDS->color[i][0] = 255;
					flMetaDS->color[i][1] = 255;
					flMetaDS->color[i][2] = 255;
					continue;
				}

				flMetaDS->flIntensity[i].isValid = true;			

			    auto max = channel[i].max;
				auto min = channel[i].min;								

				flMetaDS->flRange[i][0] = static_cast<int>(min);
				flMetaDS->flRange[i][1] = static_cast<int>(max);
				flMetaDS->flIntensity[i].min = min;
				flMetaDS->flIntensity[i].max = max / 4;
				flMetaDS->flIntensity[i].opacity = 70;
				flMetaDS->flName[i] = channel[i].name;
				flMetaDS->color[i][0] = channel[i].r;
				flMetaDS->color[i][1] = channel[i].g;
				flMetaDS->color[i][2] = channel[i].b;
			}
		}
		// get HT min, max
		auto htMetaDS = d->transferFunctionPanel->GetHTMetaDS();
		Entity::Image::Intensity intensity;

		if (true == image->HasType(Entity::ImageType::HT3D)) {
			intensity = image->GetIntensity(Entity::ImageType::HT3D);
			htMetaDS->isExistHT = true;
		}
		else if (true == image->HasType(Entity::ImageType::HT2D)) {
			intensity = image->GetIntensity(Entity::ImageType::HT2D);
			htMetaDS->isExistHT = true;
		}
		else {
			htMetaDS->isExistHT = false;
		}

		if(true == htMetaDS->isExistHT) {
			htMetaDS->intensityMin = intensity.min;
			htMetaDS->intensityMax = intensity.max;
		}
		// get timelapes info
		auto timelapesDS = d->tcfPlayerPanel->GetTimelapesDS();
		timelapesDS->list.clear();

		auto getTimelapesInfo = [&](Entity::ImageType type,int ch) {
			TimelapseInfo::Pointer timelapseInfo = std::make_shared<TimelapseInfo>();

			timelapseInfo->dataCount = image->GetCount(type);
			timelapseInfo->interval = static_cast<float>(image->GetTimeInterval(type));
			timelapseInfo->time_points[ch] = image->GetTimePoints(type,ch);

			timelapesDS->list.insert(std::make_pair(type, timelapseInfo));
		};

		for(int i = 1; i < Entity::ImageType::Count; ++i) {
			auto type = static_cast<Entity::ImageType>(i);

		    if (false == image->HasType(type)) {
			    continue;
			}

			if (type == Entity::ImageType::FL3D || type == Entity::ImageType::FL2DMIP) {
				for (auto ch = 0; ch < 3; ch++) {
					getTimelapesInfo(type, ch);
				}
			}else {
				getTimelapesInfo(type,0);
			}
		}		
		//get navigator info
		auto navigatorDS = d->navigatorPanel->GetNavigatorDS();
		navigatorDS->list.clear();
		navigatorDS->zOffset = image->GetOffsetZ();
		for (int i = 1; i < Entity::ImageType::Count; ++i) {
			auto type = static_cast<Entity::ImageType>(i);

			if (false == image->HasType(type)) {
				continue;
			}

			NavigatorInfo::Pointer info = std::make_shared<NavigatorInfo>();

			auto size = image->GetDimension(type);
			info->width = size.X;
			info->height = size.Y;
			info->depth = size.Z;

			auto resolution = image->GetResolution(type);
			info->resolutionX = resolution.X;
			info->resolutionY = resolution.Y;
			info->resolutionZ = resolution.Z;						

			navigatorDS->list.insert(std::make_pair(type, info));
		}
		auto viewingToolDS = d->viewingToolPanel->GetViewingToolDS();
		uint32_t max_dim = 1;
		if(image->HasType(Entity::ImageType::HT3D)) {
			auto dim = image->GetDimension(Entity::ImageType::HT3D);
			if (max_dim < dim.X)
				max_dim = dim.X;
			if (max_dim < dim.Y)
				max_dim = dim.Y;
		}else if(image->HasType(Entity::ImageType::FL3D)) {
			auto dim = image->GetDimension(Entity::ImageType::FL3D);
			if (max_dim < dim.X)
				max_dim = dim.X;
			if (max_dim < dim.Y)
				max_dim = dim.Y;
		}
		//calcluate posible resolution
		auto power = 0;
		while(pow(2,power) * 128 < max_dim) {
			power++;
		}
		if(power == 0) {
			power = 1;
		}
		viewingToolDS->maxRes = power - 1;
		viewingToolDS->changeRes = true;
		// visualizationListPanel
		QFileInfo finfo(QString::fromLocal8Bit(image->GetPath().c_str()));
		const auto folderPath = finfo.dir().path().toLocal8Bit().toStdString();

		auto visualizationDataDS = d->visualizationListPanel->GetCommonDS();
		visualizationDataDS->tcfPath = folderPath;
		// select modality event와 연결된 tcfPlayerPanel, navigatorPanel, recordVideoPanel를
		// modalityPanel 보다 먼저 Update 해야한다.
		d->viewerPanel->UpdateCall();
		d->tcfPlayerPanel->UpdateCall();
		d->navigatorPanel->UpdateCall();
		//d->recordVideoPanel->Update();
		d->transferFunctionPanel->UpdateCall();
		d->flChannelPanel->UpdateCall();
		d->modalityPanel->UpdateCall();
		d->displayPanel->UpdateCall();		
		//d->transferFunctionPanel->Update();
		//d->flChannelPanel->Update();
		d->visualizationListPanel->UpdateCall();
		d->viewingToolPanel->UpdateCall();
    }
}

