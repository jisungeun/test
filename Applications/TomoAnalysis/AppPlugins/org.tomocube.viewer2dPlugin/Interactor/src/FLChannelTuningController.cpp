#include "FLChannelTuningController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct FLChannelTuningController::Impl {
		Impl() {}
		Impl(UseCase::IFLChannelOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::IFLChannelOutputPort* outPort{ nullptr };
	};

	FLChannelTuningController::FLChannelTuningController()
		: d(new Impl()) {
	}

	FLChannelTuningController::FLChannelTuningController(UseCase::IFLChannelOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	FLChannelTuningController::~FLChannelTuningController() {
	}

	auto FLChannelTuningController::SetRange(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& min, const int& max) const ->bool {
		auto use_case = UseCase::FLChannelManager();
		return use_case.SetRange(channel, min, max, sceneID, d->outPort);
	}

	auto FLChannelTuningController::SetGamma(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& value,const bool& isGamma) const ->bool {
		auto use_case = UseCase::FLChannelManager();
		return use_case.SetGamma(channel, value, isGamma, sceneID, d->outPort);
	}

	auto FLChannelTuningController::SetOpacity(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& value) const ->bool {
		auto use_case = UseCase::FLChannelManager();
		return use_case.SetOpacity(channel, value, sceneID, d->outPort);
	}

	auto FLChannelTuningController::SetColor(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& r, const int& g, const int& b) const -> bool {
		auto use_case = UseCase::FLChannelManager();
		return use_case.SetColor(channel, r, g, b, sceneID, d->outPort);
    }


	auto FLChannelTuningController::SetVisible(Entity::Scene::ID sceneID, const Entity::Channel& channel, const bool& visible) const ->bool {
		auto use_case = UseCase::FLChannelManager();
		return use_case.SetVisible(channel, visible, sceneID, d->outPort);
	}
}