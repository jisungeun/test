#include "IDisplayPanel.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct IDisplayPanel::Impl {
        DisplayDS::Pointer displayDS = std::make_shared<DisplayDS>();
    };

    IDisplayPanel::IDisplayPanel() : d{ new Impl } {
    }

    IDisplayPanel::~IDisplayPanel() {
    }

    auto IDisplayPanel::GetDisplayDS() const -> DisplayDS::Pointer {
        return d->displayDS;
    }
}