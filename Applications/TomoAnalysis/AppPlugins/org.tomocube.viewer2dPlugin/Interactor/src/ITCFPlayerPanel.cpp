#include <map>

#include "ITCFPlayerPanel.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct ITCFPlayerPanel::Impl {
        TimelapesDS::Pointer timelapesDS = std::make_shared<TimelapesDS>();
    };

    ITCFPlayerPanel::ITCFPlayerPanel() : d{ new Impl } {
    }

    ITCFPlayerPanel::~ITCFPlayerPanel() {
    }

    auto ITCFPlayerPanel::GetTimelapesDS() const ->TimelapesDS::Pointer {
        return d->timelapesDS;
    }
}