#include <VisualizationPresetIO.h>

#include "VisualizationPresetController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct VisualizationPresetController::Impl {
		Impl() {}
		Impl(UseCase::IVisualizationPresetOutputPort* outPort,
			UseCase::IVisualizationDataWritePort* writePort,
			UseCase::IVisualizationDataReadPort* readPort)
	    : outPort(outPort)
	    , writePort(writePort)
		, readPort(readPort) {
	    }

		UseCase::IVisualizationPresetOutputPort* outPort{ nullptr };
		UseCase::IVisualizationDataWritePort* writePort{ nullptr };
		UseCase::IVisualizationDataReadPort* readPort{ nullptr };
	};

	VisualizationPresetController::VisualizationPresetController()
		: d(new Impl()) {
	}

	VisualizationPresetController::VisualizationPresetController(UseCase::IVisualizationPresetOutputPort* outPort,
	                                                             UseCase::IVisualizationDataWritePort* writer,
			                                                     UseCase::IVisualizationDataReadPort* reader)
		: d(new Impl(outPort, writer, reader)) {
	}

	VisualizationPresetController::~VisualizationPresetController() {
	}

	auto VisualizationPresetController::LoadList(const std::string& presetFolderPath) const ->bool {
		auto use_case = UseCase::VisualizationPresetIO();
	    return use_case.LoadList(presetFolderPath, d->outPort);
	}

	auto VisualizationPresetController::Load(Entity::Scene::ID sceneID, const std::string& path) const ->bool {
		auto use_case = UseCase::VisualizationPresetIO();
		return use_case.Load(path, sceneID, d->outPort, d->readPort);
	}

	auto VisualizationPresetController::Delete(Entity::Scene::ID , const std::string& path) const -> bool {		
		auto use_case = UseCase::VisualizationPresetIO();
		return use_case.Delete(path);
    }


	auto VisualizationPresetController::Save(Entity::Scene::ID sceneID, const std::string& path, const bool& saveAs) const ->bool {
		auto use_case = UseCase::VisualizationPresetIO();
		return use_case.Save(path, saveAs, sceneID, d->outPort, d->writePort);
	}
}