#include <iostream>

#include "SliceNavigatePresenter.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct SliceNavigatePresenter::Impl {
       IImageViewer* viewer = nullptr;
	   INavigatorPanel* navigatorPanel = nullptr;
    };

    SliceNavigatePresenter::SliceNavigatePresenter( )
        : d(new Impl()) {
    }

    SliceNavigatePresenter::SliceNavigatePresenter(IImageViewer* viewer, INavigatorPanel* navigatorPanel)
		: d(new Impl()) {
		d->viewer = viewer;
		d->navigatorPanel = navigatorPanel;
    }

    SliceNavigatePresenter::~SliceNavigatePresenter() {
        
    }

    void SliceNavigatePresenter::Update(Entity::Scene::Pointer& scene, const int& posX, const int& posY, const int& posZ) {
        if(!scene.get()) {
			return;
        }

		if(nullptr == d->viewer) {
		    return;
		}

		if (nullptr == d->navigatorPanel) {
			return;
		}

		const auto image = scene->GetImage();
		if (nullptr == image) {
			return;
		}

        auto viewerScene = d->viewer->GetSceneDS();
        if(nullptr == viewerScene) {
            return;
        }

		viewerScene->viewConfigList = scene->GetViewConfigList();
		const auto activatedModality = viewerScene->activatedModality;
        auto imageType = Entity::Unknown;

        if(activatedModality == Entity::Modality::None) {
            return;
        }
        
        if (((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
            // volume
            if ((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
                imageType = Entity::HT3D;
            }
            else {
                imageType = Entity::FL3D;
            }

        }
        else if (((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
            (activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {

            // mip
            if ((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
                imageType = Entity::HT2DMIP;
            }
            else {
                imageType = Entity::FL2DMIP;
            }
        }

		auto navigatorDS = d->navigatorPanel->GetNavigatorDS();
        if (posX > 0) {
            navigatorDS->x = posX;
            navigatorDS->phyX = posX * image->GetResolution(imageType).X;
        }
        if (posY > 0) {
            navigatorDS->y = posY;
            navigatorDS->phyY = posY * image->GetResolution(imageType).Y;
        }
        if (posZ > 0) {
            navigatorDS->z = posZ;
            if(image->HasType(Entity::ImageType::HT3D)&& imageType == Entity::ImageType::FL3D) {
                navigatorDS->phyZ = posZ * image->GetResolution(Entity::ImageType::HT3D).Z;
            }else {
                navigatorDS->phyZ = posZ * image->GetResolution(imageType).Z;
            }            
        }                
                
		d->viewer->Refresh();
        d->navigatorPanel->Refresh();
    }

    void SliceNavigatePresenter::UpdatePhy(Entity::Scene::Pointer& scene, const float& x, const float& y, const float& z) {
        if (!scene.get()) {
            return;
        }

        if (nullptr == d->viewer) {
            return;
        }

        if (nullptr == d->navigatorPanel) {
            return;
        }

        const auto image = scene->GetImage();
        if (nullptr == image) {
            return;
        }

        auto viewerScene = d->viewer->GetSceneDS();
        if (nullptr == viewerScene) {
            return;
        }

        viewerScene->viewConfigList = scene->GetViewConfigList();
        const auto activatedModality = viewerScene->activatedModality;
        auto imageType = Entity::Unknown;

        if (activatedModality == Entity::Modality::None) {
            return;
        }

        if (((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
            // volume
            if ((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
                imageType = Entity::HT3D;
            }
            else {
                imageType = Entity::FL3D;
            }

        }
        else if (((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
            (activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {

            // mip
            if ((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
                imageType = Entity::HT2DMIP;
            }
            else {
                imageType = Entity::FL2DMIP;
            }
        }

        auto navigatorDS = d->navigatorPanel->GetNavigatorDS();
        if (x > 0) {
            navigatorDS->x = static_cast<int>(x / image->GetResolution(imageType).X);
            navigatorDS->phyX = x;
        }
        if (y > 0) {
            navigatorDS->y = static_cast<int>(y / image->GetResolution(imageType).Y);
            navigatorDS->phyY = y;
        }
        if (z > 0) {
            navigatorDS->z = static_cast<int>(z / image->GetResolution(imageType).Z);
            navigatorDS->phyZ = z;
        }
                
        d->viewer->Refresh();
        d->navigatorPanel->Refresh();
    }
}
