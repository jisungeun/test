#include "IImageViewer.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    struct IImageViewer::Impl {
        FileDS::Pointer fileDS = std::make_shared<FileDS>();
        SceneDS::Pointer sceneDS = std::make_shared<SceneDS>();
    };

	IImageViewer::IImageViewer()
		: d(new Impl()) {
    }

    IImageViewer::~IImageViewer() {
    }

    auto IImageViewer::GetFileDS() const->FileDS::Pointer {
        return d->fileDS;
    }

    auto IImageViewer::GetSceneDS() const -> SceneDS::Pointer {
		return d->sceneDS;
    }
}