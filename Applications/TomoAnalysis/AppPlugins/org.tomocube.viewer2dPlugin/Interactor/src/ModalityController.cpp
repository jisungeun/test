#include <ImageAccessor.h>

#include "ModalityController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct ModalityController::Impl {
		Impl() {}
		Impl(UseCase::ISceneOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ISceneOutputPort* outPort{ nullptr };
	};

	ModalityController::ModalityController()
		: d(new Impl()) {
    }

	ModalityController::ModalityController(UseCase::ISceneOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	ModalityController::~ModalityController() {
	}

	auto ModalityController::SelectModality(const Entity::Scene::ID& sceneID, const Entity::Modality& modality) const -> bool {
		auto use_case = UseCase::ImageAccessor();
		return use_case.SetModality(modality, sceneID, d->outPort);
	}
}