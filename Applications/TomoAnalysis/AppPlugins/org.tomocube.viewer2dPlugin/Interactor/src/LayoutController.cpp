#include <LayoutManager.h>

#include "LayoutController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct LayoutController::Impl {
		Impl() {}
		Impl(UseCase::ISceneOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ISceneOutputPort* outPort{ nullptr };
	};

	LayoutController::LayoutController()
		: d(new Impl()) {
	}

	LayoutController::LayoutController(UseCase::ISceneOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	LayoutController::~LayoutController() {
	}


	auto LayoutController::SetLayoutType(Entity::Scene::ID sceneID, const Entity::LayoutType& type) const ->bool {
		auto use_case = UseCase::LayoutManager();
		return use_case.SetLayoutType(type, sceneID, d->outPort);
	}
}