#include <ScreenManager.h>

#include "ScreenUtilController.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct ScreenUtilController::Impl {
		Impl() {}
		Impl(UseCase::ISceneOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ISceneOutputPort* outPort{ nullptr };
	};

	ScreenUtilController::ScreenUtilController()
		: d(new Impl()) {
	}

	ScreenUtilController::ScreenUtilController(UseCase::ISceneOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	ScreenUtilController::~ScreenUtilController() {
	}

	auto ScreenUtilController::SetVisibleAxisGrid(Entity::Scene::ID sceneID, bool visible) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetVisibleAxisGrid(visible, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetAxisGridFontSize(Entity::Scene::ID sceneID, int size) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetAxisGridFontSize(size,sceneID, d->outPort);
    }

	auto ScreenUtilController::SetAxisGridColor(Entity::Scene::ID sceneID, int axisIdx, int r, int g, int b) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetAxisGridColor(axisIdx, r, g, b, sceneID, d->outPort);
    }


	auto ScreenUtilController::SetVisibleBoundaryBox(Entity::Scene::ID sceneID, bool visible) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetVisibleBoundaryBox(visible, sceneID, d->outPort);
	}

	auto ScreenUtilController::SetVisibleOrientationMarker(Entity::Scene::ID sceneID, bool visible) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetVisibleOrientationMarker(visible, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetVisibleTimeStamp(Entity::Scene::ID sceneID, bool visible) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetVisibleTimeStamp(visible, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetTimeStampColor(Entity::Scene::ID sceneID, int r,int g,int b) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetTimeStampColor(r, g, b, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetTimeStampSize(Entity::Scene::ID sceneID, int size) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetTimeStampSize(size, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetResolution(Entity::Scene::ID sceneID, int resolution) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetResolution(resolution, sceneID, d->outPort);
    }

	auto ScreenUtilController::SetEnableTransferFunctionOverlay(Entity::Scene::ID sceneID, bool enable) const -> bool {
		auto use_case = UseCase::ScreenManager();
		return use_case.SetEnableOverlay(enable, sceneID, d->outPort);
	}
}