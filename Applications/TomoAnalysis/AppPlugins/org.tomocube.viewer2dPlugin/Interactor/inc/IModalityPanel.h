#pragma once

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API ModalityDS {
		int usableModality = Entity::Modality::None;
		int ForceModality = Entity::Modality::None;
		typedef std::shared_ptr<ModalityDS> Pointer;
	};

	class Viewer2dInteractor_API IModalityPanel {
	public:
		IModalityPanel();
		virtual ~IModalityPanel();

		auto GetModalityDS() const->ModalityDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;		

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}