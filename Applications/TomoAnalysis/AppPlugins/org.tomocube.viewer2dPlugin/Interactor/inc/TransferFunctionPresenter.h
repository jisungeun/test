#pragma once

#include <memory>
#include <ITransferFunctionOutputPort.h>

#include "IImageViewer.h"

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    class Viewer2dInteractor_API TransferFunctionPresenter : public UseCase::ITransferFunctionOutputPort {
    public:
        TransferFunctionPresenter();
        TransferFunctionPresenter(IImageViewer* viewer);
        ~TransferFunctionPresenter();

        void Update(const int& index, const Entity::TFItem::Pointer& item) override;
        void Clear(void) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
