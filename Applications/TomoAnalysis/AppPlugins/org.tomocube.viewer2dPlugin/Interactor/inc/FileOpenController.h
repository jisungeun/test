#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ITCFInfoOutputPort.h>
#include <IFileReaderPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	class Viewer2dInteractor_API FileOpenController final {
	public:
		FileOpenController();
		FileOpenController(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort);
		FileOpenController(const FileOpenController& other) = delete;
		~FileOpenController();

		auto LoadImages(Entity::Scene::ID sceneID, const std::string& path) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}