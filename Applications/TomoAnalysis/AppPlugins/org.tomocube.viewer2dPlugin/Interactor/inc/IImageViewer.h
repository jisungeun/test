#pragma once

#include <memory>
#include <vector>
#include <map>
#include <string>

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
//#include "TCFInfoPresenter.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API FileDS {
		std::string path;
		//std::map<Entity::ImageType, TimelapseInfo::Pointer> time_list;
		typedef std::shared_ptr<FileDS> Pointer;
	};

    struct Viewer2dInteractor_API SceneDS {
		int activatedModality = Entity::Modality::None;

		Entity::LayoutType layoutType = Entity::LayoutType::UNKNOWN;
		Entity::ViewConfigList viewConfigList;

		Entity::TFItemList tfItemList;
		Entity::FLChannelInfo flChannelList;

		int timelapseIndex = 0;
		double timelapseTime = 0.0;		

		bool visibleBoundaryBox = false;
		bool visibleAxisGrid = false;
		int axisGridColor[3][3] = { {0,255,0}, {0,0,255 } ,{255,0,0 } };
		bool visibleOrientationMarker = false;
		bool visibleTimeStamp = false;
		int timestampR = 255;
		int timestampG = 255;
		int timestampB = 255;
		int timestampSize = 20;
		bool isOverlay = false;
		int axisGridFontSize = 18;
		int resolution = -1;//-1 for interactive

        void Clear() {
			activatedModality = Entity::Modality::None;
			layoutType = Entity::LayoutType::UNKNOWN;			

			viewConfigList.clear();

			tfItemList.clear();
			flChannelList.clear();

			timelapseIndex = 0;
			timelapseTime = 0.0;

			visibleBoundaryBox = false;
			visibleAxisGrid = false;
			isOverlay = false;
        }

		typedef std::shared_ptr<SceneDS> Pointer;
    };

    class Viewer2dInteractor_API IImageViewer {
    public:
		IImageViewer();
		virtual ~IImageViewer();

		auto GetFileDS() const->FileDS::Pointer;
		auto GetSceneDS() const->SceneDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;
		virtual auto UpdateTransferFunction(const int& index, const Entity::TFItem::Pointer& item)->bool = 0;
		virtual auto UpdateChannelInfo(const Entity::FLChannelInfo& info)->bool = 0;
		virtual auto UpdateVisualizationData(const Entity::TFItemList& tfItemList, const Entity::FLChannelInfo& info)->bool = 0;
		virtual auto ClearTransferFunction()->bool = 0;
		
		
    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}