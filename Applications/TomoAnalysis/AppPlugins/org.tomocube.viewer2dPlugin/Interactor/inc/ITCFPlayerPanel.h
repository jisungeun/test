#pragma once

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API TimelapseInfo {
		int dataCount = -1;
		float interval = -1.f;
		std::vector<double> time_points[3];
		typedef std::shared_ptr<TimelapseInfo> Pointer;
	};

	struct Viewer2dInteractor_API TimelapesDS {
		std::map<Entity::ImageType, TimelapseInfo::Pointer> list;

	    typedef std::shared_ptr<TimelapesDS> Pointer;
	};

	class Viewer2dInteractor_API ITCFPlayerPanel {
	public:
		ITCFPlayerPanel();
		virtual ~ITCFPlayerPanel();

		auto GetTimelapesDS() const ->TimelapesDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}