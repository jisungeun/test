#pragma once

#include <memory>
#include <ISliceNavigateOutputPort.h>

#include "IImageViewer.h"
#include "INavigatorPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    class Viewer2dInteractor_API SliceNavigatePresenter : public UseCase::ISliceNavigateOutputPort {
    public:
		SliceNavigatePresenter();
		SliceNavigatePresenter(IImageViewer* viewer, INavigatorPanel* navigatorPanel);
		~SliceNavigatePresenter();

		void Update(Entity::Scene::Pointer& scene, const int& posX, const int& posY, const int& posZ) override;
		void UpdatePhy(Entity::Scene::Pointer& scene, const float& x, const float& y, const float& z) override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}