#pragma once

#include <string>
#include <memory>

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API RecordVideoInfo {
		int width = 0;
		int height = 0;
		int depth = 0;

		int count;
		int interval;  //msec

		typedef std::shared_ptr<RecordVideoInfo> Pointer;
	};

	struct Viewer2dInteractor_API RecordVideoDS {
		std::string tcfName;
	    std::map<Entity::ImageType, RecordVideoInfo::Pointer> list;

		typedef std::shared_ptr<RecordVideoDS> Pointer;
	};

	class Viewer2dInteractor_API IRecordVideoPanel {
	public:
		IRecordVideoPanel();
		virtual ~IRecordVideoPanel();

		auto GetRecordVideoDS() const->RecordVideoDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}