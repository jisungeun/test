#pragma once

#include <memory>
#include <ISceneOutputPort.h>

#include "IImageViewer.h"
#include "IViewingToolPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    class Viewer2dInteractor_API ScenePresenter : public UseCase::ISceneOutputPort {
    public:
		ScenePresenter();
		ScenePresenter(IImageViewer* viewer, IViewingToolPanel* panel=nullptr);
		~ScenePresenter();

		void Update(Entity::Scene::Pointer& scene) override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}