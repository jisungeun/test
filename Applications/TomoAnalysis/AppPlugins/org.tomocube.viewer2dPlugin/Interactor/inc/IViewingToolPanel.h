#pragma once

#include <Scene.h>

#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API ViewingToolDS {
		typedef std::shared_ptr<ViewingToolDS> Pointer;
		//viewing tool
		bool showBoundaryBox{ false };
		bool showAxisGrid{ false };
		bool showOrientationMarker{ false };

		int axisGridFontSize{ 18 };
		int axisXColor[3]{ 0,255,0 };//green
		int axisYColor[3]{ 0,0,255 };//blue
		int axisZColor[3]{ 255,0,0 };//red

		bool showTimeStamp{ false };
		int timestampColor[3]{ 255,255,255 };		
		float timestampSize{ 14.0 };
		
		int maxRes{ 0 };
		bool changeRes{ false };

		//layout
		//Entity::LayoutType layoutType{ Entity::LayoutType::HSlicesBy3D };
		Entity::LayoutType layoutType{ Entity::LayoutType::XYPlane };

		auto Clear()->void {
			showBoundaryBox = false;
			showAxisGrid = false;
			showOrientationMarker = false;
			axisGridFontSize = 18;
			showTimeStamp = false;
			for(auto i=0;i<3;i++) {
				timestampColor[i] = 255;
			}
			timestampSize = 14.0;
			maxRes = 0;
			changeRes = false;
			//layoutType = Entity::LayoutType::HSlicesBy3D;
			layoutType = Entity::LayoutType::XYPlane;
		}
	};

	class Viewer2dInteractor_API IViewingToolPanel {
	public:
		IViewingToolPanel();
		virtual ~IViewingToolPanel();

		auto GetViewingToolDS() const->ViewingToolDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}