#pragma once

#include <memory>

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API NavigatorInfo {
		int width = 0;
		int height = 0;
		int depth = 0;

		float resolutionX = 0.f;
		float resolutionY = 0.f;
		float resolutionZ = 0.f;				

		typedef std::shared_ptr<NavigatorInfo> Pointer;
	};

	struct Viewer2dInteractor_API NavigatorDS {
		std::map<Entity::ImageType, NavigatorInfo::Pointer> list;

		int x = 0;
		int y = 0;
		int z = 0;

		float phyX = 0.f;
		float phyY = 0.f;
		float phyZ = 0.f;

		double zOffset = 0.0f;

		typedef std::shared_ptr<NavigatorDS> Pointer;
	};

	class Viewer2dInteractor_API INavigatorPanel {
	public:
		INavigatorPanel();
		virtual ~INavigatorPanel();

		auto GetNavigatorDS() const->NavigatorDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}