#pragma once

//#include "TomoAnalysisInteractorExport.h"

#include <TransferFunction.h>
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API HTMetaDS {
		bool isExistHT = false;

		float intensityMin = -1.f;
		float intensityMax = -1.f;

		float gradientMin = -1.f;
		float gradientMax = -1.f;

		typedef std::shared_ptr<HTMetaDS> Pointer;
	};

	struct Viewer2dInteractor_API TransferFunctionDS {
		Entity::TFItemList list;

		typedef std::shared_ptr<TransferFunctionDS> Pointer;
	};

	class Viewer2dInteractor_API ITransferFunctionPanel {
	public:
		ITransferFunctionPanel();
		virtual ~ITransferFunctionPanel();

		auto GetHTMetaDS() const->HTMetaDS::Pointer;
		auto GetTransferFunctionDS() const->TransferFunctionDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}