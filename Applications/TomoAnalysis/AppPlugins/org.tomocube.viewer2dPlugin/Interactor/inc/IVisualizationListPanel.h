#pragma once

#include <Scene.h>
#include <memory>
//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API CommonDS {
		std::string tcfPath;

	    typedef std::shared_ptr<CommonDS> Pointer;
	};

	struct Viewer2dInteractor_API ListDS {
	    std::vector<std::string> list;

		typedef std::shared_ptr<ListDS> Pointer;
	};

	struct Viewer2dInteractor_API PresetDS {
		std::string currentPreset;
	    Entity::TFItemList tfItemList;
		Entity::FLChannelInfo channel;

		typedef std::shared_ptr<PresetDS> Pointer;
	};

	class Viewer2dInteractor_API IVisualizationListPanel {
	public:
		IVisualizationListPanel();
		virtual ~IVisualizationListPanel();

		auto GetCommonDS()->CommonDS::Pointer;
		auto GetListDS()->ListDS::Pointer;
		auto GetPresetDS()->PresetDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;
		virtual auto Load()->bool = 0;
	    virtual auto Save(std::string path, bool saveAs)->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}