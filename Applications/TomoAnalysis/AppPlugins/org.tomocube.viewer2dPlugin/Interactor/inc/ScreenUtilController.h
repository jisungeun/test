#pragma once
#include <memory>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	class Viewer2dInteractor_API ScreenUtilController final {
	public:
		ScreenUtilController();
		ScreenUtilController(UseCase::ISceneOutputPort* outPort);
		ScreenUtilController(const ScreenUtilController& other) = delete;
		~ScreenUtilController();

		auto SetVisibleAxisGrid(Entity::Scene::ID sceneID, bool visible) const ->bool;
		auto SetVisibleBoundaryBox(Entity::Scene::ID sceneID, bool visible) const -> bool;
		auto SetVisibleOrientationMarker(Entity::Scene::ID sceneID, bool visible)const->bool;
		auto SetVisibleTimeStamp(Entity::Scene::ID sceneID, bool visible)const->bool;
		auto SetTimeStampColor(Entity::Scene::ID sceneID, int r,int g,int b)const->bool;
		auto SetTimeStampSize(Entity::Scene::ID sceneID, int size)const->bool;
		auto SetResolution(Entity::Scene::ID sceneID, int resolution)const->bool;
		auto SetEnableTransferFunctionOverlay(Entity::Scene::ID sceneID, bool enable) const -> bool;
		auto SetAxisGridFontSize(Entity::Scene::ID sceneID, int size)const->bool;
		auto SetAxisGridColor(Entity::Scene::ID sceneID, int axisIdx, int r,int g,int b)const->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}