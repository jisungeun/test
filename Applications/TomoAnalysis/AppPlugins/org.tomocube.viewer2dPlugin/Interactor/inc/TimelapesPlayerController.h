#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	class Viewer2dInteractor_API TimelapsePlayerController final {
	public:
		TimelapsePlayerController();
		TimelapsePlayerController(UseCase::ISceneOutputPort* outPort);
		TimelapsePlayerController(const TimelapsePlayerController& other) = delete;
		~TimelapsePlayerController();

		auto Play(Entity::Scene::ID sceneID, const double& frameTime) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}