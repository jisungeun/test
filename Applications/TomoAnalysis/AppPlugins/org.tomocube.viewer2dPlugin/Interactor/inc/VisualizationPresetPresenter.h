#pragma once

#include <memory>

#include <IVisualizationPresetOutputPort.h>

#include "IImageViewer.h"
#include "ITransferFunctionPanel.h"
#include "IFLChannelPanel.h"
#include "IVisualizationListPanel.h"
#include "IModalityPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    class Viewer2dInteractor_API VisualizationPresetPresenter : public UseCase::IVisualizationPresetOutputPort {
    public:
        VisualizationPresetPresenter();
        VisualizationPresetPresenter(IImageViewer* viewerPanel, ITransferFunctionPanel* transferFunctionPanel,
                                     IFLChannelPanel* flChannelPanel, IVisualizationListPanel* visualizationListPanel,IModalityPanel* modalityPanel);
        ~VisualizationPresetPresenter();

        void Update(const std::string& tcfFolderPath) override;
        void LoadList(const std::vector<std::string>& list) override;
        void LoadPreset(const std::string& path, const Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel) override;
        void SavePreset(const std::string& path, const bool& saveAs) override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
