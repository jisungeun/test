#pragma once

#include <string>
#include <memory>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API ScreenShotDS {
		std::string tcfName;

		typedef std::shared_ptr<ScreenShotDS> Pointer;
	};

	class Viewer2dInteractor_API IScreenShotPanel {
	public:
		IScreenShotPanel();
		virtual ~IScreenShotPanel();

		auto GetScreenShotDS() const->ScreenShotDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}