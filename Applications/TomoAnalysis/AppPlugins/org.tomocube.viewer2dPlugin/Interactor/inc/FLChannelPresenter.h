#pragma once

#include <memory>
#include <IFLChannelOutputPort.h>

#include "IImageViewer.h"

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
    class Viewer2dInteractor_API FLChannelPresenter : public UseCase::IFLChannelOutputPort {
    public:
        FLChannelPresenter();
        FLChannelPresenter(IImageViewer* viewer);
        ~FLChannelPresenter();

        void Update(const Entity::FLChannelInfo& info) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
