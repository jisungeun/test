#pragma once

#include <Scene.h>

#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	struct Viewer2dInteractor_API DisplayDS {
		Entity::DisplayType usableDisplayType = Entity::DisplayType::DISPLAY_NONE;
		typedef std::shared_ptr<DisplayDS> Pointer;
	};

	class Viewer2dInteractor_API IDisplayPanel {
	public:
		IDisplayPanel();
		virtual ~IDisplayPanel();

		auto GetDisplayDS() const->DisplayDS::Pointer;

		virtual auto UpdateCall()->bool = 0;
		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}