#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	class Viewer2dInteractor_API ModalityController final {

	public:
		ModalityController();
		ModalityController(UseCase::ISceneOutputPort* outPort);
		ModalityController(const ModalityController& other) = delete;
		~ModalityController();

		auto SelectModality(const Entity::Scene::ID& sceneID, const Entity::Modality& modality) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}