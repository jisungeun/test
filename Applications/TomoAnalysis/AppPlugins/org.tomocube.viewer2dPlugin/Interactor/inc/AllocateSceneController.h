#pragma once
#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "Viewer2dInteractorExport.h"

namespace TomoAnalysis::Viewer2D::Interactor {
	class Viewer2dInteractor_API AllocateSceneController final {
	public:
		AllocateSceneController();
		AllocateSceneController(const AllocateSceneController& other) = delete;
		~AllocateSceneController();
		
		auto RequestSceneAllocation() const -> Entity::Scene::ID;
	};
}