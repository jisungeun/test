#define LOGGER_TAG "[3D Visualizer]"
#include <TCLogger.h>

#include <QElapsedTimer>
#include <QFileDialog>
#include <QSettings>
#include <QTimer>

#include <AppInterfaceTA.h>

#include "MainWindow.h"
#include "viewer2dPlugin.h"


namespace TomoAnalysis::Viewer2D::AppUI {    
    struct viewerPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;

        MainWindow* mainWindow{nullptr};
        AppInterfaceTA* appInterface{ nullptr };
        
        bool opened = false;        

        QElapsedTimer timer;
        int start_time{ 0 };
        int end_time{ 0 };
    };

    viewerPlugin* viewerPlugin::instance = 0;

    viewerPlugin* viewerPlugin::getInstance() {

        return instance;
    }

    viewerPlugin::viewerPlugin() : d(new Impl) {        
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);        
    }

    viewerPlugin::~viewerPlugin() {        
        
    }

    auto viewerPlugin::start(ctkPluginContext* context) -> void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);                        
    }

    auto viewerPlugin::stop(ctkPluginContext* context) -> void {
        Q_UNUSED(context);        
    }

    auto viewerPlugin::getPluginContext() -> ctkPluginContext* const {
        return d->context;
    }            
}
