#include <QtPlugin>

#include <ctkPluginFrameworkLauncher.h>
#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#include <AppEvent.h>
#include <MenuEvent.h>

#include "MainWindow.h"
#include "flmaskgeneratorPlugin.h"

namespace TomoAnalysis::FLMaskGenerator::AppUI {
	struct flmaskgeneratorPlugin::Impl {
		Impl() = default;
		~Impl() = default;

		ctkPluginContext* context;
		MainWindow* mainWindow{ nullptr };
		AppInterfaceTA* appInterface{ nullptr };
	};

	flmaskgeneratorPlugin* flmaskgeneratorPlugin::instance = 0;
	flmaskgeneratorPlugin* flmaskgeneratorPlugin::getInstance() {
		return instance;
	}
	flmaskgeneratorPlugin::flmaskgeneratorPlugin() :d{ new Impl } {
		d->mainWindow = new MainWindow;
		d->appInterface = new AppInterfaceTA(d->mainWindow);
	}
	flmaskgeneratorPlugin::~flmaskgeneratorPlugin() {
	    
	}	
	auto flmaskgeneratorPlugin::start(ctkPluginContext* context)->void {
		instance = this;
		d->context = context;

		registerService(context, d->appInterface, plugin_symbolic_name);
	}
	auto flmaskgeneratorPlugin::getPluginContext() -> ctkPluginContext* const {
		return d->context;
    }
}
