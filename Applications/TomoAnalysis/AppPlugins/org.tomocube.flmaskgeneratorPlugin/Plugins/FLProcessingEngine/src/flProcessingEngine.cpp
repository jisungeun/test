#include <TCDataConverter.h>

#include <DataList.h>
#include <ScalarData.h>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>

#include <ImageViz/Engines/SoImageVizEngine.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoLabelingProcessing.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoMeasureImageProcessing.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalImageProcessing.h>
#include <Medical/helpers/MedicalHelper.h>

#include "flProcessingEngine.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct Processor::Impl {

    };
    Processor::Processor() :d{ new Impl } {

    }
    Processor::~Processor() {

    }
    auto Processor::CreateMeasure(IBaseImage::Pointer input, IBaseMask::Pointer mask, QString chName, float baselineRI, float RII,DataList::Pointer result) -> IBaseData::Pointer {
        auto converter = std::make_shared<TCDataConverter>();
        auto timage = std::dynamic_pointer_cast<TCImage>(input);
        auto tmask = std::dynamic_pointer_cast<TCMask>(mask);
        auto oivImage = converter->ImageToSoVolumeData(timage);
        auto oivMask = converter->MaskToSoVolumeData(tmask);

        double min, max;
        oivMask->getMinMax(min, max);

        //MedicalHelper::dicomAdjustVolume(oivImage);
        auto imageAdapter = MedicalHelper::getImageDataAdapter(oivImage);
        //MedicalHelper::dicomAdjustVolume(oivMask);
        auto maskAdapter = MedicalHelper::getImageDataAdapter(oivMask);
        maskAdapter->interpretation = SoMemoryDataAdapter::LABEL;

        auto analysis = new SoLabelAnalysisQuantification;
        analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
        analysis->measureList.set1Value(1, new SoDataMeasurePredefined(SoDataMeasurePredefined::AREA_3D));
        analysis->measureList.set1Value(2, new SoDataMeasurePredefined(SoDataMeasurePredefined::INTENSITY_MEAN));
        analysis->inIntensityImage = imageAdapter;
        analysis->inLabelImage = maskAdapter;
                
        auto localRI = baselineRI;
        auto localRII = RII;

        auto analysisOutput = analysis->outAnalysis.getValue();        
        DataSet::Pointer dset = DataSet::New();
        auto layerName = ScalarData::New(chName);
        dset->AppendData(layerName, "Name");
        auto numLabels = static_cast<int>(max);
        auto labelNum = ScalarData::New(numLabels);
        dset->AppendData(labelNum, "CellCount");
        for(auto i=0;i<numLabels;i++) {
            auto volume = analysisOutput->getValueAsDouble(i, 0, 0);
            auto volumeScalar = ScalarData::New(volume);
            volumeScalar->setLabelIdx(i);
            dset->AppendData(volumeScalar, "cell" + QString::number(i) + ".Volume");

            auto surface_area = analysisOutput->getValueAsDouble(i, 1, 0);
            auto saScalar = ScalarData::New(surface_area);
            saScalar->setLabelIdx(i);
            dset->AppendData(saScalar, "cell" + QString::number(i) + ".SurfaceArea");

            auto mean_ri = analysisOutput->getValueAsDouble(i, 2, 0) / 10000.0;            
            auto mrScalar = ScalarData::New(mean_ri);
            mrScalar->setLabelIdx(i);
            dset->AppendData(mrScalar, "cell" + QString::number(i) + ".MeanRI");

            auto phi = pow(3.14, 1.0 / 3.0);
            auto factor = 36.0 * volume * volume;
            auto sa = pow(factor, 1.0 / 3.0);
            auto sphericity = phi * sa / surface_area;            
            auto spScalar = ScalarData::New(sphericity);
            spScalar->setLabelIdx(i);
            dset->AppendData(spScalar, "cell" + QString(std::to_string(i).c_str()) + ".Sphericity");

            auto projected_area = CalcProjectedArea(maskAdapter, i+1);// *unitSize;            
            auto paScalar = ScalarData::New(projected_area);
            paScalar->setLabelIdx(i);
            dset->AppendData(paScalar, "cell" + QString::number(i) + ".ProjectedArea");

            auto drymass = (mean_ri - localRI) * volume / localRII;            
            auto dmScalar = ScalarData::New(drymass);
            if (drymass < 0) {
                dmScalar = ScalarData::New(-1);
            }
            dmScalar->setLabelIdx(i);
            dset->AppendData(dmScalar, "cell" + QString::number(i) + ".Drymass");

            auto concentration = drymass / volume;            
            auto ccScalar = ScalarData::New(concentration);
            if (concentration < 0) {
                ccScalar = ScalarData::New(-1);
            }
            ccScalar->setLabelIdx(i);
            dset->AppendData(ccScalar, "cell" + QString::number(i) + ".Concentration");
        }                        
        result->Append(dset);
        return nullptr;
    }
    auto Processor::CalcProjectedArea(SoImageDataAdapter* mask, int label_idx) -> double {
        SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
        threshold->inImage = mask;
        threshold->thresholdLevel.setValue((float)label_idx, (float)label_idx + 0.1);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&threshold->outBinaryImage);

        SoRef<SoVolumeData> vol = new SoVolumeData;
        vol->setReader(*reader, TRUE);

        auto dim = vol->getDimension();

        SbBox3i32 bBox(SbVec3i32(0, 0, 0),
            SbVec3i32(dim[0] - 1, dim [1] - 1, dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        uint8_t* buffer = static_cast<uint8_t*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

        uint8_t* result = new uint8_t[dim[0] * dim[1]]();
        for (auto i = 0; i < dim[0]; i++) {
            for (auto j = 0; j < dim[1]; j++) {
                for (auto k = 0; k < dim[2]; k++) {
                    auto res_idx = i * dim[1] + j;
                    auto target_idx = i * dim[1] * dim[2] + j * dim[2] + k;
                    auto val = *(buffer + target_idx);
                    if (val != 0) {
                        *(result + res_idx) = 1;
                    }
                }
            }
        }

        dataBufferObj->unmap();
        dataBufferObj = nullptr;

        auto cnt = 0;
        auto res = vol->getVoxelSize();
        for (auto i = 0; i < dim[0]; i++) {
            for (auto j = 0; j < dim[1]; j++) {
                auto res_idx = i * dim[1] + j;
                if (*(result + res_idx) > 0) {
                    cnt++;
                }
            }
        }
        delete[] result;
        auto proj = res[0] * res[1] * cnt;               

        return proj;
    }

    auto Processor::CreateFLMask(IBaseImage::Pointer input, IBaseImage::Pointer refImage, int upper, int lower) -> IBaseMask::Pointer {
        auto converter = std::make_shared<TCDataConverter>();
        auto image = std::dynamic_pointer_cast<TCImage>(input);
        auto rawData = converter->ImageToSoVolumeData(image);                
        
        auto rImage = std::dynamic_pointer_cast<TCImage>(refImage);
        auto refData = converter->ImageToSoVolumeData(rImage);

        auto volData = convertFL(refData, rawData,0.0);        

        //MedicalHelper::dicomAdjustVolume(volData);
        auto adapter = MedicalHelper::getImageDataAdapter(volData);

        auto threshold = new SoThresholdingProcessing;
        threshold->inImage = adapter;        
        threshold->thresholdLevel.setValue(lower+0.5, INT_MAX);

        auto labeling = new SoLabelingProcessing;
        labeling->inObjectImage.connectFrom(&threshold->outBinaryImage);        

        auto imageToSize = new SoMeasureImageProcessing;
        imageToSize->inIntensityImage = adapter;
        imageToSize->inLabelImage.connectFrom(&labeling->outLabelImage);
        imageToSize->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
        
        auto thresholding = new SoThresholdingProcessing;        
        thresholding->inImage.connectFrom(&imageToSize->outMeasureImage);
        thresholding->thresholdLevel.setValue(100.0, static_cast<float>(INT_MAX));

        auto upperThresh = new SoThresholdingProcessing;
        upperThresh->inImage = adapter;
        upperThresh->thresholdLevel.setValue(upper+0.5, INT_MAX);

        auto logic = new SoLogicalImageProcessing;
        logic->logicalOperator = SoLogicalImageProcessing::LogicalOperator::OR;
        logic->inImage1.connectFrom(&thresholding->outBinaryImage);
        logic->inImage2.connectFrom(&upperThresh->outBinaryImage);

        auto relabeling = new SoLabelingProcessing;
        //relabeling->inObjectImage.connectFrom(&thresholding->outBinaryImage);
        relabeling->inObjectImage.connectFrom(&logic->outImage);
        relabeling->computeMode.setValue(SoLabelingProcessing::ComputeMode::MODE_3D);

        auto ireader = new SoVRImageDataReader;
        //ireader->imageData.connectFrom(&labeling->outLabelImage);
        ireader->imageData.connectFrom(&relabeling->outLabelImage);

        auto labelData = new SoVolumeData;
        labelData->setReader(*ireader, TRUE);        

        double min, max;
        labelData->getMinMax(min, max);
        
        QList<SoVolumeData*> rset;
        rset.push_back(labelData);
        auto result = converter->SoVolumeDataToMask(rset, MaskTypeEnum::MultiLabel);
        result->SetTimeStep(image->GetTimeStep());
        /*
        while (labelData->getRefCount() > 0) {
            labelData->unref();
        }
        labelData = nullptr;

        ireader = nullptr;

        
        while(relabeling->getRefCount()>0) {
            relabeling->unref();
        }
        relabeling = nullptr;

        while(thresholding->getRefCount()>0) {
            thresholding->unref();
        }
        thresholding = nullptr;

        while(imageToSize->getRefCount()>0) {
            imageToSize->unref();
        }
        imageToSize = nullptr;

        while (labeling->getRefCount() > 0) {
            labeling->unref();
        }
        labeling = nullptr;

        while (threshold->getRefCount() > 0) {
            threshold->unref();
        }
        threshold = nullptr;

        while (adapter->getRefCount() > 0) {
            adapter->unref();
        }
        adapter = nullptr;

        while (volData->getRefCount() > 0) {
            volData->unref();
        }
        volData = nullptr;
        */
        converter = nullptr;

        return result;
    }
    auto Processor::convertFL(SoVolumeData* oriHT, SoVolumeData* oriFL, float offset) -> SoVolumeData* {
        Q_UNUSED(offset)
            auto convFL = new SoVolumeData;
        convFL->ref();

        auto dim = oriHT->getDimension();
        std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
        auto fl_dim = oriFL->getDimension();
        SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(fl_dim[0] - 1, fl_dim[1] - 1, fl_dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            oriFL->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        oriFL->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned short* fl_pointer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

        for (auto k = 0; k < dim[2]; k++) {
            for (auto j = 0; j < dim[1]; j++) {
                for (auto i = 0; i < dim[0]; i++) {
                    SbVec3f index;
                    index[0] = i;
                    index[1] = j;
                    index[2] = k;
                    auto key = k * dim[0] * dim[1] + j * dim[0] + i;
                    auto phys = oriHT->voxelToXYZ(index);
                    //phys[2] -= offset;
                    auto voxel = oriFL->XYZToVoxel(phys);
                    int vv[3] = { static_cast<int>(voxel[0]),static_cast<int>(voxel[1]),static_cast<int>(voxel[2]) };
                    if (vv[0] >= 0 && vv[0] < fl_dim[0]
                        && vv[1] >= 0 && vv[1] < fl_dim[1]
                        && vv[2] >= 0 && vv[2] < fl_dim[2]) {
                        int vKey = vv[2] * fl_dim[0] * fl_dim[1] + vv[1] * fl_dim[0] + vv[0];
                        auto val = *(fl_pointer + vKey);
                        *(volume.get() + key) = val;
                    }
                }
            }
        }
        dataBufferObj->unmap();
        SbDataType type = SbDataType::UNSIGNED_SHORT;
        convFL->data.setValue(dim, type, 0, (void*)volume.get(), SoSFArray::COPY);
        volume = nullptr;
        convFL->extent.setValue(oriHT->extent.getValue());

        convFL->unrefNoDelete();
        return convFL;
    }
}
