#pragma once

#include <memory>

#include <IFLProcessingPort.h>
#include <IBaseMask.h>
#include <IBaseImage.h>

#include "FLProcessingEngineExport.h"

class SoVolumeData;
class SoImageDataAdapter;

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLProcessingEngine_API Processor : public UseCase::IProcessingPort {
    public:
        typedef Processor Self;
        typedef std::shared_ptr<Self> Pointer;

        Processor();
        ~Processor();

        auto CreateFLMask(IBaseImage::Pointer input,IBaseImage::Pointer refImage, int upper, int lower) -> IBaseMask::Pointer override;
        auto CreateMeasure(IBaseImage::Pointer input, IBaseMask::Pointer mask, QString chName, float baselineRI, float RII,DataList::Pointer result) -> IBaseData::Pointer override;

    protected:
        auto convertFL(SoVolumeData* oriHT, SoVolumeData* oriFL, float offset)->SoVolumeData*;
        auto CalcProjectedArea(SoImageDataAdapter* mask, int label_idx)->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}