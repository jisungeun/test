#pragma once

#include <memory>

#include <QWidget>

#include <IFLVizControlPanel.h>

#include "FLVizControlExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLVizControl_API VizControlPanel : public QWidget, public Interactor::IVizControlPanel {
        Q_OBJECT
    public:
        typedef VizControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        VizControlPanel(QWidget* parent = nullptr);
        ~VizControlPanel();

        auto Update() -> bool override;
        auto Modify() -> bool override;

        auto SetChannelName(int ch, QString name)->void;

    signals:
        void sigFLMaskOpa(float);
        void sigFLMaskViz(bool, bool, bool);

    protected slots:        
        void OnChannelChk();
        void OnNoneChk();
        void OnOpacitySpin(double val);

    private:
        auto Init()->void;
        auto InitConnections()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}