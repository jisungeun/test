#include "ui_flVizControlPanel.h"
#include "flVizControlPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct VizControlPanel::Impl {
        Ui::flvzForm* ui{ nullptr };
    };
    VizControlPanel::VizControlPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::flvzForm();
        d->ui->setupUi(this);

        Init();        
    }
    VizControlPanel::~VizControlPanel() {
        
    }

    auto VizControlPanel::Init() -> void {
        //set object name
        d->ui->maskLabel->setObjectName("h5");
        d->ui->opaLabel->setObjectName("h5");

        d->ui->opaSpin->setRange(0.0, 1.0);
        d->ui->opaSpin->setSingleStep(0.01);
        d->ui->opaSpin->setDecimals(2);
        d->ui->opaSpin->setValue(0.7);

        InitConnections();
    }

    auto VizControlPanel::InitConnections() -> void {
        d->ui->nonRad->setChecked(true);
        connect(d->ui->ch1Rad, SIGNAL(clicked()), this, SLOT(OnChannelChk()));
        connect(d->ui->ch2Rad, SIGNAL(clicked()), this, SLOT(OnChannelChk()));
        connect(d->ui->ch3Rad, SIGNAL(clicked()), this, SLOT(OnChannelChk()));
        connect(d->ui->opaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnOpacitySpin(double)));
        connect(d->ui->nonRad, SIGNAL(clicked()), this, SLOT(OnNoneChk()));
    }
    
    auto VizControlPanel::Update() -> bool {
        //update interface based on channel existence
        auto ds = GetVizControlDS();
        auto exist0 = ds->chExist[0] && ds->chSelected[0];
        d->ui->ch1Rad->blockSignals(true);
        d->ui->ch1Rad->setChecked(false);
        d->ui->ch1Rad->blockSignals(false);
        if(exist0) {
            d->ui->ch1Rad->show();
        }else {            
            d->ui->ch1Rad->hide();
        }
        auto exist1 = ds->chExist[1] && ds->chSelected[1];
        d->ui->ch2Rad->blockSignals(true);
        d->ui->ch2Rad->setChecked(false);
        d->ui->ch2Rad->blockSignals(false);
        if(exist1) {
            d->ui->ch2Rad->show();
        }else {            
            d->ui->ch2Rad->hide();
        }
        auto exist2 = ds->chExist[2] && ds->chSelected[2];
        d->ui->ch3Rad->blockSignals(true);
        d->ui->ch3Rad->setChecked(false);
        d->ui->ch3Rad->blockSignals(false);
        if(exist2) {
            d->ui->ch3Rad->show();
        }else {            
            d->ui->ch3Rad->hide();
        }
        d->ui->nonRad->blockSignals(true);
        d->ui->nonRad->setChecked(true);
        d->ui->nonRad->blockSignals(false);
        return true;
    }

    auto VizControlPanel::Modify() -> bool {
        auto ds = GetVizControlDS();
        blockSignals(true);
        switch(ds->showChMaskIdx) {
        case 0:            
            d->ui->ch1Rad->setChecked(true);            
            break;
        case 1:            
            d->ui->ch2Rad->setChecked(true);            
            break;
        case 2:
            d->ui->ch3Rad->setChecked(true);
            break;
        }
        blockSignals(false);
        return true;
    }

    //SLOTS
    void VizControlPanel::OnNoneChk() {
        emit sigFLMaskViz(false, false, false);
    }
    void VizControlPanel::OnChannelChk() {        
        emit sigFLMaskViz(d->ui->ch1Rad->isChecked(), d->ui->ch2Rad->isChecked(), d->ui->ch3Rad->isChecked());
    }
    void VizControlPanel::OnOpacitySpin(double val) {
        emit sigFLMaskOpa(static_cast<float>(val));
    }
    auto VizControlPanel::SetChannelName(int ch, QString name) -> void {
        switch(ch) {
        case 0:
            d->ui->ch1Rad->setText(name);
            break;
        case 1:
            d->ui->ch2Rad->setText(name);
            break;
        case 2:
            d->ui->ch3Rad->setText(name);
            break;
        }
    }

}