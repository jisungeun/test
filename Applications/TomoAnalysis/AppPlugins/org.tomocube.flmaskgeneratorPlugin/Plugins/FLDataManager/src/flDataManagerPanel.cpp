#include <QDateTime>
#include <QFileInfo>

#include <TCFMetaReader.h>

#include "ui_flDataManagerPanel.h"
#include "flDataManagerPanel.h"
#include <flmgMeasureDataReader.h>

#include <iostream>

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct DataManagerPanel::Impl {
        Ui::fldmForm* ui{ nullptr };

        QStringList tableHeader;
        QString nextText;
        auto GetSimpleName(const QString& name)->QString {
            auto split = name.split("/");
            auto simple = split[split.size() - 1];

            if (20 > simple.size()) {
                return simple;
            }

            const auto list = simple.split(".");
            if (3 > list.size()) {
                return simple;
            }

            simple = simple.remove(0, 20);
            return simple.chopped(4);
        };
    };
    DataManagerPanel::DataManagerPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::fldmForm();
        d->ui->setupUi(this);
        d->nextText = d->ui->nextBtn->text();
        Init();
    }
    DataManagerPanel::~DataManagerPanel() {

    }
    auto DataManagerPanel::ClearSelection() -> void {
        d->ui->tableWidget->clearSelection();
    }

    auto DataManagerPanel::Modified(double time_point) -> bool {
        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < 0.0001;
        };
        auto ds = GetDataManagerDS();
        
        TC::IO::TCFMetaReader reader;
        auto meta = reader.Read(ds->tcfList[ds->cur_idx]);
        auto prev_idx = ds->cur_idx;
        ds->cur_idx = 0;
        for (auto i = 0; i < prev_idx; i++) {
            ds->cur_idx += ds->tcfSteps[i];
        }
        for (auto i = 0; i < meta->data.data3DFL.timePoints.count(); i++) {
            if (AreSame(meta->data.data3DFL.timePoints[i], time_point)) {
                ds->cur_idx += i;
                break;
            }
        }

        auto total = 0;
        for (auto s : ds->tcfSteps) {
            total += s;
        }

        //d->ui->curFileLabel->setText(QString("Current File: %1/%2").arg(ds->cur_idx+1).arg(ds->tcfList.count()));
        d->ui->curFileLabel->setText(QString("Current File: %1/%2").arg(ds->cur_idx + 1).arg(total));
        
        d->ui->tableWidget->blockSignals(true);        
        d->ui->tableWidget->selectRow(ds->cur_idx);
        d->ui->tableWidget->setFocus();
        if (ds->visited[ds->cur_idx]) {
            for (auto i = 0; i < 5; i++) {
                auto item = d->ui->tableWidget->item(ds->cur_idx, i);
                item->setBackground(QColor(30, 102, 30));
            }
        }
        d->ui->tableWidget->blockSignals(false);        
        return true;
    }
    auto DataManagerPanel::Update() -> bool {
        auto ds = GetDataManagerDS();
        ds->visited.clear();
        ds->tcfSteps.clear();
                
        auto reader = TC::IO::TCFMetaReader();
        auto time_max = 0;
        for (auto i = 0; i < ds->tcfList.count(); i++) {
            const auto meta = reader.Read(ds->tcfList[i]);
            auto timePoints = meta->data.data3DFL.timePoints;
            ds->tcfSteps.push_back(timePoints.count());
            time_max += timePoints.count();        
            QFileInfo tcfInfo(ds->tcfList[i]);
            auto maskPath = ds->playgroundPath.chopped(5) + "/" + ds->hyperName + "/" + tcfInfo.fileName().chopped(4) + ".rep";
            for (auto j = 0; j < timePoints.count(); j++) {
                auto tp = timePoints[j];
                auto ht_idx = meta->data.data3D.timePoints.indexOf(tp);
                auto mreader = MeasureDataReader();
                auto measure = mreader.Read(maskPath, ht_idx);
                if (measure->GetKeys().count() > 0) {
                    std::cout << maskPath.toStdString() << " at time " << ht_idx << " already exist" << std::endl;
                    ds->visited.push_back(true);
                }
                else {
                    ds->visited.push_back(false);
                }                
            }
        }
        //d->ui->tableWidget->setRowCount(ds->tcfList.count());
        d->ui->tableWidget->setRowCount(time_max);
        auto real_cnt = 0;
        for(auto i=0;i<ds->tcfList.count();i++) {
            const auto meta = reader.Read(ds->tcfList[i]);
            auto timePoints = meta->data.data3DFL.timePoints;            
            for (auto t = 0; t < timePoints.count(); t++) {
                auto date = QDateTime::fromString(meta->common.createDate, "yyyy-MM-dd hh:mm:ss").toString("yyyy.MM.dd");
                auto dateItem = new QTableWidgetItem(date);
                dateItem->setFlags(dateItem->flags() ^ Qt::ItemIsEditable);
                d->ui->tableWidget->setItem(real_cnt, 0, dateItem);
                auto nameItem = new QTableWidgetItem(d->GetSimpleName(ds->tcfList[i]));
                nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
                d->ui->tableWidget->setItem(real_cnt, 1, nameItem);
                auto timeItem = new QTableWidgetItem(QString::number(timePoints[t]));
                timeItem->setFlags(timeItem->flags() ^ Qt::ItemIsEditable);
                d->ui->tableWidget->setItem(real_cnt, 2, timeItem);
                auto cubeItem = new QTableWidgetItem(ds->cubeList[i]);
                cubeItem->setFlags(cubeItem->flags() ^ Qt::ItemIsEditable);
                d->ui->tableWidget->setItem(real_cnt, 3, cubeItem);
                auto hyperItem = new QTableWidgetItem(ds->hyperName);
                hyperItem->setFlags(hyperItem->flags() ^ Qt::ItemIsEditable);
                d->ui->tableWidget->setItem(real_cnt, 4, hyperItem);
                if (ds->visited[real_cnt]) {
                    std::cout << real_cnt << " visited change color" << std::endl;
                    dateItem->setBackground(QColor(30, 102, 30));
                    nameItem->setBackground(QColor(30, 102, 30));
                    timeItem->setBackground(QColor(30, 102, 30));
                    cubeItem->setBackground(QColor(30, 102, 30));
                    hyperItem->setBackground(QColor(30, 102, 30));
                }
                real_cnt++;
            }
        }
        d->ui->curFileLabel->setText(QString("Current File: %1/%2").arg(0).arg(real_cnt));
        d->ui->tableWidget->resizeColumnsToContents();
        return true;
    }
        
    auto DataManagerPanel::Init() -> void {
        InitStyle();
        InitTable();
        InitConnection();
    }
    auto DataManagerPanel::InitStyle() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->nextBtn->setObjectName("bt-square-line");
        d->ui->prevBtn->setObjectName("bt-square-line");

        d->ui->curFileLabel->setObjectName("h3");
        d->ui->allBtn->setObjectName("bt-square-primary");
    }

    auto DataManagerPanel::InitTable() -> void {
        d->tableHeader << "Date" << "Name" << "Time"<< "Cube" << "Hypercube";
        d->ui->tableWidget->setColumnCount(d->tableHeader.count());
        d->ui->tableWidget->setHorizontalHeaderLabels(d->tableHeader);
        d->ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        d->ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);        
    }

    auto DataManagerPanel::InitConnection() -> void {
        connect(d->ui->tableWidget, SIGNAL(cellClicked(int, int)), this, SLOT(OnTableSelectionChanged(int, int)));

        connect(d->ui->prevBtn, SIGNAL(clicked()), this, SLOT(OnPrevBtnClicked()));
        connect(d->ui->nextBtn, SIGNAL(clicked()), this, SLOT(OnNextBtnClicked()));
        connect(d->ui->allBtn, SIGNAL(clicked()), this, SLOT(OnAllBtnClicked()));
    }

    void DataManagerPanel::OnPrevBtnClicked() {        
        auto ds = GetDataManagerDS();
        /*for (auto i = 0; i < 4; i++) {
            d->ui->tableWidget->item(ds->cur_idx, i)->setBackground(QColor(30, 150, 20));
        }*/
        if (ds->cur_idx > 0) {
            auto image_idx = 0;
            auto step_sum = 0;
            for (auto i = 0; i < ds->tcfSteps.count(); i++) {
                step_sum += ds->tcfSteps[i];
                if (ds->cur_idx - 1 < step_sum) {
                    image_idx = i;
                    break;
                }
            }
            auto time_point = d->ui->tableWidget->item(ds->cur_idx - 1, 2)->text().toDouble();
            d->ui->nextBtn->setText(d->nextText);
            d->ui->nextBtn->setObjectName("bt-square-line");
            style()->unpolish(d->ui->nextBtn);
            style()->polish(d->ui->nextBtn);
                        
            emit sigOpenTcf(image_idx,time_point);
        }
    }

    void DataManagerPanel::OnNextBtnClicked() {
        if (d->ui->nextBtn->text() == "Batch run") {
            OnAllBtnClicked();
            return;
        }
        auto ds = GetDataManagerDS();
        
        auto total_cnt = 0;
        for (auto tps : ds->tcfSteps) {
            total_cnt += tps;
        }
        if(ds->cur_idx < total_cnt-1){
            auto image_idx = 0;
            auto step_sum = 0;
            for (auto i = 0; i < ds->tcfSteps.count(); i++) {
                step_sum += ds->tcfSteps[i];
                if (ds->cur_idx + 1 < step_sum) {
                    image_idx = i;
                    break;
                }
            }            
            auto time_point = d->ui->tableWidget->item(ds->cur_idx + 1, 2)->text().toDouble();
            if (ds->cur_idx + 1 == total_cnt - 1) {
                d->ui->nextBtn->setText("Batch run");
                d->ui->nextBtn->setObjectName("bt-square-primary");
                style()->unpolish(d->ui->nextBtn);
                style()->polish(d->ui->nextBtn);
            }            
            emit sigOpenTcf(image_idx,time_point);
        }
    }

    void DataManagerPanel::OnAllBtnClicked() {
        //auto ds = GetDataManagerDS();
        /*for (auto j = 0; j < ds->tcfList.count(); j++) {
            for (auto i = 0; i < 4; i++) {
                d->ui->tableWidget->item(j, i)->setBackground(QColor(30, 150,
                    20));
            }
        }*/
        emit sigAllOperation();
    }

    void DataManagerPanel::OnTableSelectionChanged(int row, int column) {
        Q_UNUSED(column)
        d->ui->tableWidget->blockSignals(true);
        d->ui->tableWidget->selectRow(row);
        d->ui->tableWidget->blockSignals(false);

        auto time_point = d->ui->tableWidget->item(row, 2)->text().toDouble();

        auto ds = GetDataManagerDS();
        if(ds->cur_idx!=row) {
            auto image_idx = 0;
            auto step_sum = 0;
            for (auto i = 0; i < ds->tcfSteps.count(); i++) {
                step_sum += ds->tcfSteps[i];
                if (row < step_sum) {
                    image_idx = i;
                    break;
                }
            }
            if (row == d->ui->tableWidget->rowCount() - 1) {
                d->ui->nextBtn->setText("Batch run");
                d->ui->nextBtn->setObjectName("bt-square-primary");                
            }
            else {
                d->ui->nextBtn->setText(d->nextText);
                d->ui->nextBtn->setObjectName("bt-square-line");                
            }
            style()->unpolish(d->ui->nextBtn);
            style()->polish(d->ui->nextBtn);
            emit sigOpenTcf(image_idx,time_point);
        }        
    }
}
