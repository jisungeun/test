#pragma once

#include <memory>

#include <QWidget>

#include <IFLDataManagerPanel.h>

#include "FLDataManagerExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLDataManager_API DataManagerPanel : public QWidget, public Interactor::IDataManagerPanel {
        Q_OBJECT
    public:
        typedef DataManagerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        DataManagerPanel(QWidget* parent = nullptr);
        ~DataManagerPanel();

        auto Update() -> bool override;        
        auto Modified(double tp) -> bool override;

        auto ClearSelection()->void;        

    signals:
        void sigOpenTcf(int step,double time);
        void sigAllOperation();

    protected slots:
        void OnPrevBtnClicked();
        void OnNextBtnClicked();
        void OnAllBtnClicked();
        void OnTableSelectionChanged(int, int);
            
    private:
        auto Init()->void;
        auto InitStyle()->void;
        auto InitTable()->void;
        auto InitConnection()->void;        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}