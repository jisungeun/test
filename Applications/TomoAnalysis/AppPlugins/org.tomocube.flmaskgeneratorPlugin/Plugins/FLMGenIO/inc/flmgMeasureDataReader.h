#pragma once

#include <IFLMeasureReaderPort.h>

#include "FLMGenIOExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLMGenIO_API MeasureDataReader : public UseCase::IMeasureReaderPort {
    public:
        MeasureDataReader();
        ~MeasureDataReader();

        auto Read(const QString& path, const int& time_step) -> TCMeasure::Pointer override;
    };
}