#pragma once

#include <IFLMeasureWriterPort.h>

#include "FLMGenIOExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLMGenIO_API MeasureDataWriter : public UseCase::IMeasureWriterPort {
    public:
        MeasureDataWriter();
        ~MeasureDataWriter();

        auto Write(TCMeasure::Pointer data, const QString& path) -> bool override;
        auto Modify(TCMeasure::Pointer data, const QString& path) -> bool override;
    };
}