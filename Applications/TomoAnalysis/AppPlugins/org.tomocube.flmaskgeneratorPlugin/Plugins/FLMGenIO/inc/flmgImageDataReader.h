#pragma once

#include <IFLImageReaderPort.h>

#include "FLMGenIOExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLMGenIO_API ImageDataReader : public UseCase::IImageReaderPort {
    public:
        ImageDataReader();
        ~ImageDataReader();

        auto Read(const QString& path, const int& time_step) const -> TCImage::Pointer override;
        auto ReadFL(const QString& path, const int& ch, const int& time_step) -> TCImage::Pointer override;
        auto GetTimeStep(const QString& path) const -> int override;
    };
}