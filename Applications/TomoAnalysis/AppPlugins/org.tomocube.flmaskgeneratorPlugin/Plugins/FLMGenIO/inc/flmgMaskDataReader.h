#pragma once

#include <IFLMaskReaderPort.h>

#include "FLMGenIOExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLMGenIO_API MaskDataReader : public UseCase::IMaskReaderPort {
    public:
        MaskDataReader();
        ~MaskDataReader();

        auto Read(const QString& path, const QString& chName, const int& time_idx) -> TCMask::Pointer override;
        auto GetMaskList(const QString& path) -> QList<MaskMeta> override;
        auto Create(TCImage::Pointer image, QString mask_name, int typeIdx) -> TCMask::Pointer override;
    };
}