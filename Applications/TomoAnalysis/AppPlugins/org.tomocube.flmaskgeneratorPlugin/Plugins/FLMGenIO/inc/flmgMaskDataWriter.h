#pragma once

#include <IFLMaskWriterPort.h>

#include "FLMGenIOExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLMGenIO_API MaskDataWriter : public UseCase::IMaskWriterPort {
    public:
        MaskDataWriter();
        ~MaskDataWriter();

        auto Write(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch) -> bool override;
        auto Modify(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch) -> bool override;
    };
}