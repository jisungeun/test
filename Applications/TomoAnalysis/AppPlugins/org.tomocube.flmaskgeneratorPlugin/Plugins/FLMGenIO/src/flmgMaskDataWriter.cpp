#include <TCMask.h>

#include <iostream>
#include <TCUMaskWriter.h>
#include <TCMaskWriterPort.h>

#include "flmgMaskDataWriter.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    MaskDataWriter::MaskDataWriter() {
        
    }
    MaskDataWriter::~MaskDataWriter() {
        
    }
    auto MaskDataWriter::Modify(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch) -> bool {
        TC::IO::TCMaskWriterPort writer;        
        if(false == writer.Modify(data, path, "FL", organName, ch)) {
            return false;
        }
        if(false == writer.WriteIntAttrb(data, path, "FL", organName, "ChNumber", ch)) {
            return false;
        }
        return true;
        /*auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path, time_idx);
        const auto indexes = data->GetBlobIndexes();
        writer.ClearBlobCount("FL", organName, time_idx);
        writer.ClearWhole("FL", organName, time_idx);
        writer.WriteIntAttrb("FL", organName, "ChNumber", ch);
        QString type = "Label";
        if (data->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {
            return false;
        }
        writer.WriteName("FL", ch, organName, type);        
        for (auto i = 0; i < indexes.size(); i++) {
            auto index = indexes[i];

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob("FL", organName, time_idx, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();
            if (!writer.WriteMask("FL", organName, time_idx, index, bbox, maskVolume)) return false;
        }
        return true;*/
    }
    auto MaskDataWriter::Write(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch) -> bool {
        TC::IO::TCMaskWriterPort writer;
        if(false ==  writer.Write(data,path,"FL",organName,ch)) {
            return false;
        }
        if(false == writer.WriteIntAttrb(data,path,"FL",organName,"ChNumber",ch)) {
            return false;
        }
        return true;

        /*auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path, time_idx);
        const auto indexes = data->GetBlobIndexes();
        writer.SetNumberOfBlob(indexes.count());
        writer.ClearBlobCount("FL", organName, time_idx);
        writer.WriteIntAttrb("FL", organName, "ChNumber", ch);
        QString type = "Label";
        if (data->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {
            return false;
        }
        writer.WriteName("FL", ch, organName, type);
        for (auto i = 0; i < indexes.count(); i++) {
            auto index = indexes[i];

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob("FL", organName, time_idx, index, bbox, code)) {
                return false;
            }

            auto maskVolume = blob.GetMaskVolume();
            if (!writer.WriteMask("FL", organName, time_idx, index, bbox, maskVolume)) {
                return false;
            }
        }
        const auto size = data->GetSize();
        writer.WriteSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        const auto res = data->GetResolution();
        writer.WriteResolution(std::get<0>(res), std::get<1>(res), std::get<2>(res));
        writer.WriteVersion(2, 0, 0);
        return true;
        */
    }
}
