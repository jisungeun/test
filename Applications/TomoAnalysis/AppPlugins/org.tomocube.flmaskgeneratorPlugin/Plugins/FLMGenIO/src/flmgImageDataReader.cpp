#include <QStringList>

#include <TCFSimpleReader.h>
#include <TCFMetaReader.h>
#include <OivLdmReader.h>

#include "flmgImageDataReader.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    ImageDataReader::ImageDataReader() {
        
    }
    ImageDataReader::~ImageDataReader() {
        
    }
    auto ImageDataReader::Read(const QString& path, const int& time_step) const -> TCImage::Pointer {
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        auto meta = metaReader->Read(path);
        auto vers = meta->common.formatVersion.split(".");
        auto minor_ver = vers[1].toInt();
        auto isFloat = (minor_ver < 3);
        auto isLDM = meta->data.isLDM;
        TCImage::Pointer tcImage = std::make_shared<TCImage>();
                
        if(isLDM) {
            OivLdmReader* reader = new OivLdmReader;
            reader->ref();
            reader->setDataGroupPath("/Data/3D");
            reader->setTileDimension(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
            QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
            reader->setFilename(path.toStdString());
            reader->setTileName(tileName.toStdString());
            SoVolumeData* vol = new SoVolumeData;
            vol->ref();
            vol->texturePrecision = 16;
            vol->ldmResourceParameters.getValue()->tileDimension.setValue(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
            vol->setReader(*reader, TRUE);
            reader->touch();

            auto dim = vol->getDimension();
            auto res = vol->getVoxelSize();

            auto cnt = dim[0] * dim[1] * dim[2];

            std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

            SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
            SoLDMDataAccess::DataInfoBox dataInfoBox =
                vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

            SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
            dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

            vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

            unsigned short* ptr = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

            memcpy(arr.get(), ptr, cnt * sizeof(unsigned short));

            dataBufferObj->unmap();

            tcImage->SetImageVolume(arr);

            double rres[3];
            for (auto i = 0; i < 3; i++) {
                rres[i] = res[i];
            }

            tcImage->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
            tcImage->SetResolution(rres);

            while (vol->getRefCount() > 0) {
                vol->unref();
            }
            vol = nullptr;
            while (reader->getRefCount() > 0) {
                reader->unref();
            }
            reader = nullptr;
        }else {
            auto reader = new TC::IO::TCFSimpleReader;
            reader->SetFilePath(path);
            auto result = reader->ReadHT3D(time_step, isFloat);
            auto dim = result.GetDimension();
            auto res = result.GetResolution();

            auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

            std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
            if (isFloat) {
                auto base = result.GetDataAsDouble();
                for (auto i = 0; i < static_cast<int>(cnt); i++) {
                    arr.get()[i] = base.get()[i] * 10000.0;
                }
            }
            else {
                auto base = result.GetDataAsUInt16();
                memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));
            }

            tcImage->SetImageVolume(arr);

            double rres[3];
            rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
            tcImage->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
            tcImage->SetResolution(rres);
            delete reader;
        }

        tcImage->SetMinMax(meta->data.data3D.riMin*10000.0, meta->data.data3D.riMax * 10000.0);

        return tcImage;
    }
    auto ImageDataReader::ReadFL(const QString& path, const int& ch, const int& time_step) -> TCImage::Pointer {
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        auto meta = metaReader->Read(path);
        auto vers = meta->common.formatVersion.split(".");
        auto minor_ver = vers[1].toInt();
        auto isFloat = (minor_ver < 3);
        auto isLDM = meta->data.isLDM;
        if(false == meta->data.data3DFL.valid[ch]) {
            return nullptr;
        }
        TCImage::Pointer flImage = std::make_shared<TCImage>();                

        if (isLDM) {
            OivLdmReader* reader = new OivLdmReader;
            reader->ref();
            reader->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
            reader->setTileDimension(meta->data.data3DFL.tileSizeX, meta->data.data3DFL.tileSizeY, meta->data.data3DFL.tileSizeZ);
            QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
            reader->setFilename(path.toStdString());
            reader->setTileName(tileName.toStdString());
            SoVolumeData* vol = new SoVolumeData;
            vol->ref();
            vol->texturePrecision = 16;
            vol->ldmResourceParameters.getValue()->tileDimension.setValue(meta->data.data3DFL.tileSizeX, meta->data.data3DFL.tileSizeY, meta->data.data3DFL.tileSizeZ);
            vol->setReader(*reader, TRUE);
            reader->touch();

            auto dim = vol->getDimension();
            auto res = vol->getVoxelSize();

            auto cnt = dim[0] * dim[1] * dim[2];

            std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

            SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
            SoLDMDataAccess::DataInfoBox dataInfoBox =
                vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

            SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
            dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

            vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

            unsigned short* ptr = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

            memcpy(arr.get(), ptr, cnt * sizeof(unsigned short));

            dataBufferObj->unmap();

            flImage->SetImageVolume(arr);

            double rres[3];
            for (auto i = 0; i < 3; i++) {
                rres[i] = res[i];
            }

            flImage->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
            flImage->SetResolution(rres);

            while (vol->getRefCount() > 0) {
                vol->unref();
            }
            vol = nullptr;
            while (reader->getRefCount() > 0) {
                reader->unref();
            }
            reader = nullptr;
        }
        else {
            auto reader = new TC::IO::TCFSimpleReader;
            reader->SetFilePath(path);
            auto result = reader->ReadFL3D(ch,time_step);
            auto dim = result.GetDimension();
            auto res = result.GetResolution();

            auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

            std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

            auto base = result.GetDataAsUInt16();
            memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));            

            flImage->SetImageVolume(arr);            

            double rres[3];
            rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
            flImage->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
            flImage->SetResolution(rres);
            delete reader;
        }

        return flImage;
    }

    auto ImageDataReader::GetTimeStep(const QString& path) const -> int {
        return 0;
    }

}