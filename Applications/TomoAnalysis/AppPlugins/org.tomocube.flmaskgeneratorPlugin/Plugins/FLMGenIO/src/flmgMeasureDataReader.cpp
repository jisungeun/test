#include <iostream>
#include <TCMeasureReader.h>

#include "flmgMeasureDataReader.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    MeasureDataReader::MeasureDataReader() {
        
    }
    MeasureDataReader::~MeasureDataReader() {
        
    }
    auto MeasureDataReader::Read(const QString& path, const int& time_step) -> TCMeasure::Pointer {
        auto measure = std::make_shared<TCMeasure>();

        TC::IO::TCMeasureReader reader(path);        
        auto organ_names = reader.GetOrganNames();//one time only

        auto measure_names = reader.GetMeasureNames();

        for (auto o = 0; o < organ_names.size(); o++) {
            auto organ = organ_names[o];
            auto organID = QString("Measures/") + organ;
            auto cell_count = reader.GetCellCount(organID, time_step);
            for (auto i = 0; i < cell_count; i++) {
                auto vals = reader.ReadMeasures(organID, time_step, i);
                for (auto m = 0; m < measure_names.size(); m++) {
                    auto key = measure_names[m];
                    auto final_key = organ + "." + key;
                    auto value = vals[m];
                    measure->AppendMeasure(final_key, i, value);
                }
            }
        }

        return measure;
    }
}