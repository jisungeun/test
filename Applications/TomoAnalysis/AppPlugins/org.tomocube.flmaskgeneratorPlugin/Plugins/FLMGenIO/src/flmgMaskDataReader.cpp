#include <TCUMaskReader.h>
#include <TCMaskReaderPort.h>

#include "flmgMaskDataReader.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    MaskDataReader::MaskDataReader() {
        
    }
    MaskDataReader::~MaskDataReader() {
        
    }
    auto MaskDataReader::GetMaskList(const QString& path) -> QList<MaskMeta> {        
        auto reader = TC::IO::TCMaskReaderPort();
        auto maskList = reader.GetMaskList(path, "FL");

        for(auto i=0;i<maskList.count();i++) {
            auto ch = reader.ReadIntAttrb(path, "FL", maskList[i].name, "ChNumber");
            maskList[i].ch = ch;
        }

        return maskList;

        /*auto reader = TC::IO::TCUMaskReader(path);
        if (!reader.Exist()) {
            return QList<MaskMeta>();
        }
        auto version = reader.GetVersion();
        if (version.isEmpty()) {
            return QList<MaskMeta>();
        }
        if (version.at(0) == '1') {
            return QList<MaskMeta>();
        }
        auto nameList = reader.GetNameList("FL");
        auto typeList = reader.GetTypeList("FL");

        if (nameList.count() != typeList.count()) {
            return QList<MaskMeta>();
        }
        auto result = QList <MaskMeta>();
        for (auto i = 0; i < nameList.count(); i++) {
            MaskMeta meta;
            meta.name = nameList[i];
            auto ch = reader.ReadIntAttrb("FL", meta.name, "ChNumber");
            meta.ch = ch;
            if (typeList[i] == "Binary") {
                meta.type = 1;
            }
            else {
                meta.type = 0;
            }
            result.append(meta);
        }
        return result;
        */
    }
    auto MaskDataReader::Read(const QString& path, const QString& chName, const int& time_idx) -> TCMask::Pointer {
        auto reader = TC::IO::TCMaskReaderPort();
        return reader.Read(path, "FL", chName, time_idx);

        //auto reader = TC::IO::TCUMaskReader(path);
        //if (!reader.Exist()) return nullptr;
        //auto version = reader.GetVersion();
        //if (version.isEmpty()) {
        //    return nullptr;
        //}
        //if (version.at(0) == '1') {
        //    return nullptr;
        //}        
        //bool realLabel = false;
        //TCMask::Pointer maskData(new TCMask());
        //maskData->SetTimeStep(time_idx);
        //const auto blobs = reader.GetBlobCount("FL", chName, time_idx);
        //if (blobs < 1) {
        //    return nullptr;
        //}
        //auto nameList = reader.GetNameList("FL");
        //auto typeList = reader.GetTypeList("FL");
        //auto nameKey = -1;

        //for (auto i = 0; i < nameList.count(); i++) {
        //    if (nameList[i] == chName) {
        //        nameKey = i;
        //        break;
        //    }
        //}
        //if (nameKey < 0) {
        //    return nullptr;
        //}
        //maskData->SetType(MaskTypeEnum::MultiLabel);
        //realLabel = true;
        //if (typeList[nameKey] == "Binary") {            
        //    return nullptr;//no binary type in fl-mask
        //}
        //maskData->SetTypeText("Label");

        //for (int idx = 0; idx < blobs; idx++) {
        //    auto data = reader.ReadBlob("FL", chName, time_idx, idx);
        //    auto index = std::get<0>(data);
        //    auto bbox = std::get<1>(data);
        //    auto code = std::get<2>(data);
        //    auto offset = bbox.GetOffset();
        //    auto size = bbox.GetSize();
        //    MaskBlob blob;
        //    blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
        //    blob.SetCode(code);
        //    if (realLabel) {
        //        maskData->AppendBlob(index, blob);
        //    }
        //    else {
        //        maskData->AppendLayer(nameList[idx], blob, index);
        //    }            
        //    auto maskVolume = reader.ReadMask("FL", chName, time_idx, index);
        //    if (realLabel) {
        //        maskData->AppendMaskVolume(index, maskVolume);
        //    }
        //    else {
        //        maskData->AppendLayerVolume(nameList[nameKey], maskVolume, index);
        //    }
        //}
        //const auto size = reader.GetMaskSize();
        //maskData->SetSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        //const auto res = reader.GetMaskResolution();
        //double ress[3] = { std::get<0>(res), std::get<1>(res), std::get<2>(res) };
        //maskData->SetResolution(ress);
        //maskData->SetTimeStep(time_idx);
        //return maskData;        
    }
    auto MaskDataReader::Create(TCImage::Pointer image, QString mask_name, int typeIdx) -> TCMask::Pointer {
        auto ssize = image->GetSize();
        int size[3]{ std::get<0>(ssize),std::get<1>(ssize),std::get<2>(ssize) };
        //auto res = oivImage->getVoxelSize();
        double res[3];
        image->GetResolution(res);

        MaskTypeEnum type = MaskTypeEnum::MultiLabel;
        if (typeIdx == 0) {
            return nullptr;            
        }

        double rres[3];
        rres[0] = res[0];
        rres[1] = res[1];
        rres[2] = res[2];

        auto resultMask = std::make_shared<TCMask>();
        resultMask->SetResolution(rres);
        resultMask->SetSize(size[0], size[1], size[2]);
        resultMask->SetValid(true);
        resultMask->SetType(type);
        resultMask->SetTypeText("Label");

        std::shared_ptr<unsigned char> maskArr(new unsigned char[size[0] * size[1] * size[2]](), std::default_delete<unsigned char[]>());
        MaskBlob blob;
        blob.SetBoundingBox(0, 0, 0, size[0] - 1, size[1] - 1, size[2] - 1);
        blob.SetMaskVolume(maskArr);

        resultMask->AppendLayer(mask_name, blob);

        return resultMask;
    }
}