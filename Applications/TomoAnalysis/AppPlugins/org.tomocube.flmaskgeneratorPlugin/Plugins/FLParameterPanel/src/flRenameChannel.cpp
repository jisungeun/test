#include <QStyle>
#include <QMessageBox>
#include <QRegExpValidator>

#include "ui_flRenameChannel.h"
#include "flRenameChannel.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct RenameChannel::Impl {
        Ui::RenameChannel* ui{ nullptr };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
    };
    RenameChannel::RenameChannel(QWidget* parent,const QString& name,const QString& type)
        :QDialog(parent)
    , d{new Impl} {
        d->ui = new Ui::RenameChannel;
        d->ui->setupUi(this);
        d->ui->label->setText("Rename the " + type);

        d->ui->oriName->setText(name);
        d->ui->oriName->setReadOnly(true);

        d->ui->newName->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->newName, &QLineEdit::textChanged, [=] { d->ui->newName->style()->polish(d->ui->newName); });

        d->ui->newName->setPlaceholderText("Enter new name");

        connect(d->ui->okBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));

        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->newName->setValidator(new QRegExpValidator(re));
    }
    RenameChannel::~RenameChannel() = default;


    auto RenameChannel::Rename(QWidget* parent, const QString& name, const QString& type) -> QString {
        RenameChannel dialog(parent, name, type);
        if(dialog.exec()!=QDialog::Accepted) {
            return QString();
        }
        const auto newName = dialog.GetNewName();

        return newName;
    }

    auto RenameChannel::GetNewName() const -> QString {
        auto result = d->ui->newName->text();
        result = d->rstrip(result);
        return result;
    }

    void RenameChannel::OnCancelBtn() {
        reject();
    }
    void RenameChannel::OnOkBtn() {
        if (true == d->ui->newName->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a new name", "Empty or invalid name");
            return;
        }
        auto resultName = d->ui->newName->text();
        resultName = d->rstrip(resultName);
        if(d->ui->oriName->text().compare(resultName)==0) {
            QMessageBox::warning(nullptr, "Enter a new name", "Empty or invalid name");
            return;
        }
        accept();
    }
}
