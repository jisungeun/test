#include <QComboBox>

#include "flRenameChannel.h"

#include "ui_flParameterPanel.h"
#include "flParameterPanel.h"

#include <iostream>

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct ParameterPanel::Impl {
        Ui::flparamForm* ui{ nullptr };

        int chMin[3]{ 0,0,0 };
        int chMax[3]{ 200,200,200 };
        float chOpa[3]{ 0.7,0.7,0.7 };


        QCheckBox* chActive[3]{ nullptr,nullptr,nullptr };
        QLabel* chLabel[3]{ nullptr,nullptr,nullptr };
        QLabel* chMinLabel[3]{ nullptr,nullptr,nullptr };
        QLabel* chMaxLabel[3]{ nullptr,nullptr,nullptr };
        QLabel* chOpaLabel[3]{ nullptr,nullptr,nullptr };

        QPushButton* maskBtn[3]{ nullptr,nullptr,nullptr };
        QPushButton* nameBtn[3]{ nullptr,nullptr,nullptr };

        QSlider* minSlider[3]{ nullptr,nullptr,nullptr };
        QSlider* maxSlider[3]{ nullptr,nullptr,nullptr };
        QSpinBox* minSpin[3]{ nullptr,nullptr,nullptr };
        QSpinBox* maxSpin[3]{ nullptr,nullptr,nullptr };

        QCheckBox* vizToggle[3]{ nullptr,nullptr,nullptr };
        QSlider* opaSlider[3]{ nullptr,nullptr,nullptr };
        QDoubleSpinBox* opaSpin[3]{ nullptr,nullptr,nullptr };

        //Table elements
        QStringList horiHeader{ "Ch 1","Ch 2","Ch 3" };
        QStringList vertHeader{ "Preset","RII" };
        QList<QComboBox*> riiCombo;

        bool showResult{ false };
    };
    ParameterPanel::ParameterPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::flparamForm();
        d->ui->setupUi(this);
        LinkUI();
        Init();
        d->ui->correctionBtn->hide();
    }
    ParameterPanel::~ParameterPanel() {
        
    }    
    auto ParameterPanel::LinkUI() -> void {
        d->chActive[0] = d->ui->ch1Chk;
        d->chActive[1] = d->ui->ch2Chk;
        d->chActive[2] = d->ui->ch3Chk;

        d->chLabel[0] = d->ui->ch1Label;
        d->chLabel[1] = d->ui->ch2Label;
        d->chLabel[2] = d->ui->ch3Label;

        d->chMinLabel[0] = d->ui->ch1MinLabel;
        d->chMinLabel[1] = d->ui->ch2MinLabel;
        d->chMinLabel[2] = d->ui->ch3MinLabel;

        d->chMaxLabel[0] = d->ui->ch1MaxLabel;
        d->chMaxLabel[1] = d->ui->ch2MaxLabel;
        d->chMaxLabel[2] = d->ui->ch3MaxLabel;

        d->chOpaLabel[0] = d->ui->ch1OpaLabel;
        d->chOpaLabel[1] = d->ui->ch2OpaLabel;
        d->chOpaLabel[2] = d->ui->ch3OpaLabel;

        d->maskBtn[0] = d->ui->ch1MaskBtn;
        d->maskBtn[1] = d->ui->ch2MaskBtn;
        d->maskBtn[2] = d->ui->ch3MaskBtn;

        d->nameBtn[0] = d->ui->ch1NameBtn;
        d->nameBtn[1] = d->ui->ch2NameBtn;
        d->nameBtn[2] = d->ui->ch3NameBtn;

        d->minSlider[0] = d->ui->ch1MinSlider;
        d->minSlider[1] = d->ui->ch2MinSlider;
        d->minSlider[2] = d->ui->ch3MinSlider;

        d->minSpin[0] = d->ui->ch1MinSpin;
        d->minSpin[1] = d->ui->ch2MinSpin;
        d->minSpin[2] = d->ui->ch3MinSpin;

        d->maxSlider[0] = d->ui->ch1MaxSlider;
        d->maxSlider[1] = d->ui->ch2MaxSlider;
        d->maxSlider[2] = d->ui->ch3MaxSlider;

        d->maxSpin[0] = d->ui->ch1MaxSpin;
        d->maxSpin[1] = d->ui->ch2MaxSpin;
        d->maxSpin[2] = d->ui->ch3MaxSpin;

        d->vizToggle[0] = d->ui->ch1Toggle;
        d->vizToggle[1] = d->ui->ch2Toggle;
        d->vizToggle[2] = d->ui->ch3Toggle;

        d->opaSlider[0] = d->ui->ch1OpaSlider;
        d->opaSlider[1] = d->ui->ch2OpaSlider;
        d->opaSlider[2] = d->ui->ch3OpaSlider;

        d->opaSpin[0] = d->ui->ch1OpaSpin;
        d->opaSpin[1] = d->ui->ch2OpaSpin;
        d->opaSpin[2] = d->ui->ch3OpaSpin;
    }        

    auto ParameterPanel::Init() -> void {       
        for (auto i = 0; i < 3; i++) {
            d->chActive[i]->setEnabled(false);
            d->chActive[i]->setChecked(true);

            d->minSlider[i]->setRange(0, 254);
            d->minSlider[i]->setValue(0);
            d->maxSlider[i]->setRange(1, 255);
            d->maxSlider[i]->setValue(200);
            d->minSpin[i]->setRange(0, 254);
            d->minSpin[i]->setValue(0);
            d->maxSpin[i]->setRange(1, 255);
            d->maxSpin[i]->setValue(200);

            d->vizToggle[i]->setChecked(true);
            d->opaSlider[i]->setRange(0, 100);
            d->opaSlider[i]->setValue(70);
            d->opaSpin[i]->setRange(0.0, 1.0);
            d->opaSpin[i]->setSingleStep(0.01);
            d->opaSpin[i]->setDecimals(2);
            d->opaSpin[i]->setValue(0.7);
        }
        d->ui->mRISpin->setDecimals(3);        
        d->ui->mRISpin->setRange(1.0, 3.0);
        d->ui->mRISpin->setValue(1.337);
        d->ui->mRISpin->setSingleStep(0.001);
        d->ui->mRISlider->setRange(0, 2000);
        d->ui->mRISlider->setValue(337);        

        //Init Table
        d->ui->measureTable->setColumnCount(d->horiHeader.count());
        d->ui->measureTable->setHorizontalHeaderLabels(d->horiHeader);
        d->ui->measureTable->setRowCount(d->vertHeader.count());
        d->ui->measureTable->setVerticalHeaderLabels(d->vertHeader);

        for(auto i=0;i<d->horiHeader.count();i++) {
            auto combo = new QComboBox;
            d->riiCombo.push_back(combo);
            combo->addItem("Protein");
            combo->addItem("Hemoglobin");
            combo->addItem("Lipid");
            combo->addItem("Customize");
            combo->setWhatsThis(QString::number(i));
            connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRIIComboChanged(int)));
            d->ui->measureTable->setCellWidget(0, i, combo);            
        }
        d->ui->measureTable->setItemDelegateForRow(1, new DoubleDelegate);
        for(auto i=0;i<d->horiHeader.count();i++) {
            auto defaultRII = new QTableWidgetItem(QString::number(0.19));
            defaultRII->setFlags(defaultRII->flags() ^ Qt::ItemIsEditable);
            d->ui->measureTable->setItem(1, i, defaultRII);
        }
        InitStyle();
        InitConnection();

        d->ui->mRILabel->hide();
        d->ui->mRISpin->hide();
        d->ui->mRISlider->hide();
    }
    auto ParameterPanel::InitStyle() -> void {
        d->ui->scrollAreaWidgetContents->setObjectName("panel-contents");
        d->ui->channelCollapse->setChecked(true);
        d->ui->channelCollapse->setObjectName("bt-collapse");
        d->ui->measureCollapse->setChecked(true);
        d->ui->measureCollapse->setObjectName("bt-collapse");

        d->ui->ch1Label->setObjectName("h9");
        d->ui->ch2Label->setObjectName("h9");
        d->ui->ch3Label->setObjectName("h9");
    }

    auto ParameterPanel::GetBaselineRI() -> float {
        if (false == d->ui->ch1Chk->isChecked()) {
            auto bri = static_cast<float>(d->ui->mRISpin->value());            
            return bri;
        }        
        return -1.0;        
    }

    auto ParameterPanel::GetRII(int ch) -> float {
        auto rii = d->ui->measureTable->item(1, ch)->text().toFloat();        
        return rii;
    }

    auto ParameterPanel::InitConnection() -> void {
        for(auto i=0;i<3;i++) {
            connect(d->minSlider[i], SIGNAL(valueChanged(int)), this, SLOT(MinSliderChanged(int)));
            connect(d->minSpin[i], SIGNAL(valueChanged(int)), this, SLOT(MinSpinChanged(int)));
            connect(d->maxSlider[i], SIGNAL(valueChanged(int)), this, SLOT(MaxSliderChanged(int)));
            connect(d->maxSpin[i], SIGNAL(valueChanged(int)), this, SLOT(MaxSpinChanged(int)));
            connect(d->opaSlider[i], SIGNAL(valueChanged(int)), this, SLOT(OpaSliderChanged(int)));
            connect(d->opaSpin[i], SIGNAL(valueChanged(double)), this, SLOT(OpaSpinChanged(double)));
            connect(d->maskBtn[i], SIGNAL(clicked()), this, SLOT(OnMaskBtnClicked()));
            connect(d->nameBtn[i], SIGNAL(clicked()), this, SLOT(OnNameBtnClicked()));
            connect(d->vizToggle[i], SIGNAL(stateChanged(int)), this, SLOT(VizToggleChanged(int)));
        }
        connect(d->ui->measureBtn, SIGNAL(clicked()), this, SLOT(OnMeasureBtnClicked()));
        connect(d->ui->resultBtn, SIGNAL(clicked()), this, SLOT(OnResultBtnClicked()));
        connect(d->ui->correctionBtn, SIGNAL(clicked()), this, SLOT(OnCorrectionBtnClicked()));

        connect(d->ui->mRIChk, SIGNAL(stateChanged(int)), this, SLOT(CustomRIChanged(int)));
        connect(d->ui->mRISlider, SIGNAL(valueChanged(int)), this, SLOT(MRISliderChanged(int)));
        connect(d->ui->mRISpin, SIGNAL(valueChanged(double)), this, SLOT(MRISpinChanged(double)));
        connect(d->ui->measureTable, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(OnTableItemChanged(QTableWidgetItem*)));
    }

    /////////Slots////////
    void ParameterPanel::OnTableItemChanged(QTableWidgetItem* item) {        
        emit sigChRIIValue(item->column(), item->text().toDouble());
    }

    void ParameterPanel::OnRIIComboChanged(int idx) {
        if(idx > 3) {
            return;
        }
        auto com = qobject_cast<QComboBox*>(sender());
        auto colIdx = com->whatsThis().toInt();
        double val = 0.19;
        switch(idx) {
        case 0://protein
            val = 0.19;
            break;
        case 1://hemoglobin
            val = 0.15;
            break;
        case 2://lipid
            val = 0.135;
            break;
        case 3://customize
            SetCustomRII(colIdx);
            return;
            break;
        }
        auto riiItem = d->ui->measureTable->item(1, colIdx);
        riiItem->setFlags(riiItem->flags() ^ Qt::ItemIsEditable);
        riiItem->setText(QString::number(val));

        emit sigChRII(colIdx, idx);
    }
    auto ParameterPanel::SetCustomRII(int column) -> void {                
        auto customItem = d->ui->measureTable->item(1, column);
        customItem->setFlags(customItem->flags() | Qt::ItemIsEditable);
    }
    void ParameterPanel::CustomRIChanged(int state) {
        Q_UNUSED(state);
        if(d->ui->mRIChk->isChecked()) {
            //show Custom Medium RI UI
            d->ui->mRILabel->show();
            d->ui->mRISpin->show();
            d->ui->mRISlider->show();
        }else {
            //hide Custom Medium RI UI
            d->ui->mRILabel->hide();
            d->ui->mRISpin->hide();
            d->ui->mRISlider->hide();
        }

        emit sigCustomRI(d->ui->mRIChk->isChecked());
    }
    void ParameterPanel::MRISliderChanged(int val) {
        d->ui->mRISpin->blockSignals(true);
        auto double_val = 1.0 + static_cast<double>(val) / 1000.0;
        d->ui->mRISpin->setValue(double_val);
        d->ui->mRISpin->blockSignals(false);
        emit sigRIValue(double_val);
    }
    void ParameterPanel::MRISpinChanged(double val) {
        d->ui->mRISlider->blockSignals(true);
        auto int_val = static_cast<int>(val * 1000.0) -1000;
        d->ui->mRISlider->setValue(int_val);
        d->ui->mRISlider->blockSignals(false);
        emit sigRIValue(val);
    }
    void ParameterPanel::VizToggleChanged(int state) {
        Q_UNUSED(state)
        auto chk = qobject_cast<QCheckBox*>(sender());
        if(chk) {
            auto idx = -1;
            for(auto i=0;i<3;i++) {
                if(chk == d->vizToggle[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx < 0) {
                return;
            }
            auto enable = d->vizToggle[idx]->isChecked();
            d->opaSlider[idx]->setEnabled(enable);
            d->opaSpin[idx]->setEnabled(enable);

            emit sigToggleFLViz(idx, enable);
        }
    }
    void ParameterPanel::OnCorrectionBtnClicked() {
        emit sigMaskCorrection();
    }
    void ParameterPanel::OnResultBtnClicked() {
        d->showResult = !d->showResult;
        if(d->showResult) {
            d->ui->resultBtn->setText("Hide Result");
        }else {
            d->ui->resultBtn->setText("Show Result");
        }
        emit sigShowResult(d->showResult);
    }
    void ParameterPanel::OnMeasureBtnClicked() {
        emit sigPerformMeasure();
    }
    void ParameterPanel::OnNameBtnClicked() {
        auto btn = qobject_cast<QPushButton*>(sender());
        if(btn) {
            auto idx = -1;
            for(auto i=0;i<3;i++) {
                if (btn == d->nameBtn[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx<0) {
                return;
            }            
            auto curName = d->chActive[idx]->text();
            //Change Name Dialog
            auto newName = RenameChannel::Rename(nullptr, curName, "Channel");
            if (false == newName.isEmpty()) {
                RenameChannel(idx, newName);
            }
        }
    }
    auto ParameterPanel::RenameChannel(int ch, QString newName) -> void {
        d->chActive[ch]->setText(newName);
        d->chLabel[ch]->setText(QString("%1 intensity").arg(newName));
        d->horiHeader[ch] = newName;
        d->ui->measureTable->setHorizontalHeaderLabels(d->horiHeader);
        emit sigChannelRenamed(ch, newName);
    }
    void ParameterPanel::OnMaskBtnClicked() {
        auto btn = qobject_cast<QPushButton*>(sender());
        if(btn) {
            auto idx = -1;
            for(auto i=0;i<3;i++) {
                if(btn == d->maskBtn[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx<0) {
                return;
            }            
            emit sigPerformFLMask(idx,d->minSpin[idx]->value(),d->maxSpin[idx]->value());
        }
    }
    void ParameterPanel::MinSliderChanged(int val) {
        auto slider = qobject_cast<QSlider*>(sender());
        if(slider) {
            auto idx = -1;
            for(auto i=0;i<3;i++) {
                if(slider == d->minSlider[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx<0) {
                return;
            }
            auto maxChanged = false;
            auto max_val = d->maxSlider[idx]->value();
            blockSignals(true);
            if (val >= max_val) {                
                d->maxSlider[idx]->setValue(val + 1);                
                d->maxSpin[idx]->setValue(val + 1);
                maxChanged = true;
            }            
            d->minSpin[idx]->setValue(val);
            blockSignals(false);

            if(maxChanged) {
                emit sigFLMinMaxChanged(idx, val, val + 1);
            }else {
                emit sigFLMinMaxChanged(idx, val, max_val);
            }
        }
    }

    void ParameterPanel::MinSpinChanged(int val) {
        auto spin = qobject_cast<QSpinBox*>(sender());
        if(spin) {
            auto idx = -1;
            for (auto i = 0; i < 3; i++) {
                if (spin == d->minSpin[i]) {
                    idx = i;
                    break;
                }
            }
            if (idx < 0) {
                return;
            }
            auto maxChanged = false;
            auto max_val = d->maxSpin[idx]->value();
            blockSignals(true);
            if (val >= max_val) {
                d->maxSlider[idx]->setValue(val + 1);
                d->maxSpin[idx]->setValue(val + 1);
                maxChanged = true;
            }
            d->minSlider[idx]->setValue(val);
            blockSignals(false);

            if (maxChanged) {
                emit sigFLMinMaxChanged(idx, val, val + 1);
            }
            else {
                emit sigFLMinMaxChanged(idx, val, max_val);
            }
        }
    }

    void ParameterPanel::MaxSliderChanged(int val) {
        auto slider = qobject_cast<QSlider*>(sender());
        if (slider) {
            auto idx = -1;
            for (auto i = 0; i < 3; i++) {
                if (slider == d->maxSlider[i]) {
                    idx = i;
                    break;
                }
            }
            if (idx < 0) {
                return;
            }
            auto minChanged = false;
            auto min_val = d->minSlider[idx]->value();
            blockSignals(true);
            if (val <= min_val) {
                d->minSlider[idx]->setValue(val - 1);
                d->minSpin[idx]->setValue(val - 1);
                minChanged = true;
            }
            d->maxSpin[idx]->setValue(val);
            blockSignals(false);

            if (minChanged) {
                emit sigFLMinMaxChanged(idx, val-1, val );
            }
            else {
                emit sigFLMinMaxChanged(idx, min_val, val);
            }
        }
    }

    void ParameterPanel::MaxSpinChanged(int val) {
        auto spin = qobject_cast<QSpinBox*>(sender());
        if (spin) {
            auto idx = -1;
            for (auto i = 0; i < 3; i++) {
                if (spin == d->maxSpin[i]) {
                    idx = i;
                    break;
                }
            }
            if (idx < 0) {
                return;
            }
            auto minChanged = false;
            auto min_val = d->minSpin[idx]->value();
            blockSignals(true);
            if (val <= min_val) {
                d->minSlider[idx]->setValue(val - 1);
                d->minSpin[idx]->setValue(val - 1);
                minChanged = true;
            }
            d->maxSlider[idx]->setValue(val);
            blockSignals(false);

            if (minChanged) {
                emit sigFLMinMaxChanged(idx, val - 1, val);
            }
            else {
                emit sigFLMinMaxChanged(idx, min_val, val);
            }
        }
    }

    void ParameterPanel::OpaSliderChanged(int val) {
        auto slider = qobject_cast<QSlider*>(sender());
        if (slider) {
            auto idx = -1;
            for (auto i = 0; i < 3; i++) {
                if(slider == d->opaSlider[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx < 0) {
                return;
            }
            auto dVal = static_cast<float>(val) / 100.0f;
            d->opaSpin[idx]->blockSignals(true);
            d->opaSpin[idx]->setValue(dVal);
            d->opaSpin[idx]->blockSignals(false);

            emit sigFLOpacityChanged(idx, dVal);
        }
    }

    auto ParameterPanel::SetMeasureParam(int ch, bool customRI, int key, double RIIvalue, double riVal) -> void {        
        d->ui->mRIChk->blockSignals(true);
        d->ui->mRIChk->setChecked(customRI);
        d->ui->mRIChk->blockSignals(false);
        d->ui->mRISpin->setVisible(customRI);
        d->ui->mRISlider->setVisible(customRI);        
        d->ui->mRISpin->blockSignals(true);
        d->ui->mRISpin->setValue(riVal);
        d->ui->mRISpin->blockSignals(false);
        d->riiCombo[ch]->blockSignals(true);        
        d->riiCombo[ch]->setCurrentIndex(key);        
        d->riiCombo[ch]->blockSignals(false);
        d->ui->measureTable->blockSignals(true);
        auto item = d->ui->measureTable->item(1, ch);
        if (key == 3) {
            item->setFlags(item->flags() | Qt::ItemIsEditable);
        }
        else {
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        }
        item->setText(QString::number(RIIvalue));
        d->ui->measureTable->blockSignals(false);
    }

    void ParameterPanel::OpaSpinChanged(double val) {
        auto spin = qobject_cast<QDoubleSpinBox*>(sender());
        if(spin) {
            auto idx = -1;
            for(auto i=0;i<3;i++) {
                if (spin == d->opaSpin[i]) {
                    idx = i;
                    break;
                }
            }
            if(idx<0) {
                return;
            }
            auto iVal = static_cast<int>(val * 100.0);
            d->opaSlider[idx]->blockSignals(true);
            d->opaSlider[idx]->setValue(iVal);
            d->opaSlider[idx]->blockSignals(false);

            emit sigFLOpacityChanged(idx, val);
        }
    }

    //////////////////////
    auto ParameterPanel::toggleChannel(int ch,bool show) -> void {
        if(ch<0 || ch>2) {
            return;
        }
        d->chLabel[ch]->setVisible(show);
        d->chMaxLabel[ch]->setVisible(show);
        d->chMinLabel[ch]->setVisible(show);
        d->chActive[ch]->setVisible(show);
        d->minSlider[ch]->setVisible(show);
        d->minSpin[ch]->setVisible(show);
        d->maxSlider[ch]->setVisible(show);
        d->maxSpin[ch]->setVisible(show);
        d->vizToggle[ch]->setVisible(show);
        d->opaSpin[ch]->setVisible(show);
        d->opaSlider[ch]->setVisible(show);
        d->chOpaLabel[ch]->setVisible(show);
        d->maskBtn[ch]->setVisible(show);
        d->nameBtn[ch]->setVisible(show);
    }
    auto ParameterPanel::SetULThreshold(int ch, int min, int max) -> void {
        if(d->minSpin[ch]->maximum() >= min) {
            d->minSpin[ch]->setValue(min);
        }
        if(d->maxSpin[ch]->maximum() >= max) {
            d->maxSpin[ch]->setValue(max);
        }        
    }
    auto ParameterPanel::Update() -> bool {
        //update interface based on channel existence
        auto ds = GetParameterDS();
        for(auto i=0;i<3;i++) {
            auto existance = ds->chExist[i] && ds->chSelected[i];
            d->chLabel[i]->setHidden(!existance);
            d->chMaxLabel[i]->setHidden(!existance);
            d->chMinLabel[i]->setHidden(!existance);
            d->chActive[i]->setHidden(!existance);
            d->minSlider[i]->setHidden(!existance);
            d->minSpin[i]->setHidden(!existance);
            d->maxSlider[i]->setHidden(!existance);
            d->maxSpin[i]->setHidden(!existance);
            d->vizToggle[i]->setHidden(!existance);
            d->opaSpin[i]->setHidden(!existance);
            d->opaSlider[i]->setHidden(!existance);
            d->chOpaLabel[i]->setHidden(!existance);
            d->maskBtn[i]->setHidden(!existance);
            d->nameBtn[i]->setHidden(!existance);            
            if(existance) {
                d->maxSlider[i]->blockSignals(true);                
                d->maxSlider[i]->setRange(ds->chMinMax[i][0] + 1, ds->chMinMax[i][1]);
                d->maxSlider[i]->setValue(ds->curMinMax[i][1]);
                d->maxSlider[i]->blockSignals(false);
                d->maxSpin[i]->blockSignals(true);
                d->maxSpin[i]->setRange(ds->chMinMax[i][0] + 1, ds->chMinMax[i][1]);
                d->maxSpin[i]->setValue(ds->curMinMax[i][1]);
                d->maxSpin[i]->blockSignals(false);
                d->minSlider[i]->blockSignals(true);
                d->minSlider[i]->setRange(ds->chMinMax[i][0], ds->chMinMax[i][1]-1);
                d->minSlider[i]->setValue(ds->curMinMax[i][0]);
                d->minSlider[i]->blockSignals(false);
                d->minSpin[i]->blockSignals(true);
                d->minSpin[i]->setRange(ds->chMinMax[i][0], ds->chMinMax[i][1] - 1);
                d->minSpin[i]->setValue(ds->curMinMax[i][0]);
                d->minSpin[i]->blockSignals(false);
                d->ui->measureTable->showColumn(i);
            }else {
                d->ui->measureTable->hideColumn(i);
            }
        }
        return true;
    }
    auto ParameterPanel::SetResultToggle(bool show) -> void {
        if(show) {
            d->showResult = true;
            d->ui->resultBtn->setText("Hide Result");
        }else {
            d->showResult = false;
            d->ui->resultBtn->setText("Show Result");
        }
    }
}
