#pragma once

#include <memory>

#include <QWidget>
#include <QItemDelegate>
#include <QLineEdit>
#include <QTableWidgetItem>

#include <IParameter.h>

#include <IFLParameterPanel.h>

#include "FLParameterExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class DoubleDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
            const QModelIndex& index) const override {
            Q_UNUSED(option)
                Q_UNUSED(index)
                QLineEdit* lineEdit = new QLineEdit(parent);
            QDoubleValidator* validator = new QDoubleValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };
    class FLParameter_API ParameterPanel : public QWidget, public Interactor::IParameterPanel {
        Q_OBJECT
    public:
        typedef ParameterPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ParameterPanel(QWidget* parent = nullptr);
        ~ParameterPanel();

        auto GetRII(int ch)->float;
        auto GetBaselineRI()->float;

        auto Update() -> bool override;

        auto SetResultToggle(bool)->void;        
        auto SetULThreshold(int ch, int min, int max)->void;
        auto SetMeasureParam(int ch,bool customRI,int key, double RIIvalue = 0.19,double riVal = 1.337)->void;
        auto RenameChannel(int ch, QString newName)->void;

    signals:
        void sigFLMinMaxChanged(int ch, int min,int max);
        void sigFLOpacityChanged(int ch, float opacity);        
        void sigPerformFLMask(int ch,int min,int max);
        void sigToggleFLViz(int ch, bool viz);
        void sigPerformMeasure();
        void sigMaskCorrection();
        void sigShowResult(bool);
        void sigChannelRenamed(int, QString);
        void sigChRII(int, int);
        void sigChRIIValue(int, double);
        void sigCustomRI(bool);
        void sigRIValue(double);

    protected slots:
        void MinSliderChanged(int val);
        void MinSpinChanged(int val);
        void MaxSliderChanged(int val);
        void MaxSpinChanged(int val);
        void OpaSliderChanged(int val);
        void OpaSpinChanged(double val);
        void VizToggleChanged(int state);
        void CustomRIChanged(int state);
        void MRISliderChanged(int val);
        void MRISpinChanged(double val);
        void OnRIIComboChanged(int idx);
        void OnMaskBtnClicked();
        void OnNameBtnClicked();
        void OnMeasureBtnClicked();
        void OnResultBtnClicked();
        void OnCorrectionBtnClicked();
        void OnTableItemChanged(QTableWidgetItem*);

    private:
        auto LinkUI()->void;
        auto Init()->void;
        auto InitStyle()->void;
        auto InitConnection()->void;
        auto toggleChannel(int ch,bool show)->void;
        auto SetCustomRII(int column)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}