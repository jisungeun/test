#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class RenameChannel : public QDialog {
        Q_OBJECT
    public:
        explicit RenameChannel(QWidget* parent, const QString& name, const QString& type);
        virtual ~RenameChannel();

    public:
        static auto Rename(QWidget* parent, const QString& name, const QString& type)->QString;

        auto GetNewName(void)const->QString;

    protected slots:
        void OnOkBtn();
        void OnCancelBtn();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}