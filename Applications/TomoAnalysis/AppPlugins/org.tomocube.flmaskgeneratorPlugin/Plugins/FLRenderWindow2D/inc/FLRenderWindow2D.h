#pragma once

#include <QOivRenderWindow.h>

#include "FLRenderWindow2DExport.h"

class SoEventCallback;

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLRenderWindow2D_API RenderWindow2D : public QOivRenderWindow {
        Q_OBJECT
    public:
        RenderWindow2D(QWidget* parent);
        ~RenderWindow2D();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset2DView()->void;
        auto requestUpdate()->void;
        auto setHTRange(double min, double max)->void;
        auto setRenderWindowID(int idx)->void;
        auto getRenderWindowID()->int;
        auto refreshRangeSlider()->void;

    signals:
        void sigWheel(float);
        void sig2dCoord(float, float, float);
        void sigGlobalPos(int, int);

    protected slots:
        void lowerLevelChanged(int val);
        void upperLevelChanged(int val);
        void lowerSpinChanged(double val);
        void upperSpinChanged(double val);
        
    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;        
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        //void wheelEvent(QWheelEvent* event) override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;

        auto change2DColorMap(int idx)->void;
        auto initRangeSlider()->void;

        auto CalcVolumeCoord(SbVec2f norm_point)->SbVec3f;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}