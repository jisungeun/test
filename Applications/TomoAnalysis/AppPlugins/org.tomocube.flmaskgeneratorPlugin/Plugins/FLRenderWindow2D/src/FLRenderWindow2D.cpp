#include <QLineEdit>
#include <QMenu>
#include <QVBoxLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <RangeSlider.h>
#include <SpinBoxAction.h>

#include "FLRenderWindow2D.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct RenderWindow2D::Impl {
        //colormap
        int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow                

        //levelWindow
        TC::SpinBoxAction* levelWindow;//
        int level_min{ 0 };
        int level_max{ 100 };

        bool is_reload{ false };

        bool is_delete{ false };
        int ID{ -1 };

        bool right_mouse_pressed{ false };
        bool left_mouse_pressed{ false };
        bool middle_mouse_pressed{ false };
        double right_stamp{ 0.0 };
        double prev_scale{ 1.0 };
    };

    RenderWindow2D::RenderWindow2D(QWidget* parent) :
        QOivRenderWindow(parent, true), d{ new Impl } {
        setDefaultWindowType();

        d->levelWindow = new TC::SpinBoxAction("RI Range");
        connect(d->levelWindow->spinBox(), SIGNAL(lowerValueChanged(int)), this, SLOT(lowerLevelChanged(int)));
        connect(d->levelWindow->spinBox(), SIGNAL(upperValueChanged(int)), this, SLOT(upperLevelChanged(int)));
        connect(d->levelWindow, SIGNAL(sigMinChanged(double)), this, SLOT(lowerSpinChanged(double)));
        connect(d->levelWindow, SIGNAL(sigMaxChanged(double)), this, SLOT(upperSpinChanged(double)));        
    }

    RenderWindow2D::~RenderWindow2D() {

    }

    auto RenderWindow2D::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
        QAction* cmOptions[5];
        cmOptions[0] = cmMenu->addAction("Grayscale");
        cmOptions[1] = cmMenu->addAction("Inverse grayscale");
        cmOptions[2] = cmMenu->addAction("Hot iron");
        cmOptions[3] = cmMenu->addAction("JET");
        cmOptions[4] = cmMenu->addAction("Rainbow");

        for (int i = 0; i < 5; i++) {
            cmOptions[i]->setCheckable(true);
        }
        cmOptions[d->colorMap]->setChecked(true);

        QMenu* levelMenu = myMenu.addMenu(tr("Level Window"));
        levelMenu->addAction(d->levelWindow);

        myMenu.addAction("Reset View");

        QAction* selectedItem = myMenu.exec(globalPos);

        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Inverse")) {
                change2DColorMap(1);
            }
            else if (text.contains("Grayscale")) {
                change2DColorMap(0);
            }
            else if (text.contains("Hot")) {
                change2DColorMap(2);
            }
            else if (text.contains("JET")) {
                change2DColorMap(3);
            }
            else if (text.contains("Rainbow")) {
                change2DColorMap(4);
            }
            else if (text.contains("Reset View")) {
                reset2DView();
            }
        }
    }    
    auto RenderWindow2D::setRenderWindowID(int idx) -> void {
        d->ID = idx;
    }
    auto RenderWindow2D::getRenderWindowID() -> int {
        return d->ID;
    }
    auto RenderWindow2D::requestUpdate() -> void {
        //getQtRenderArea()->render();
        getQtRenderArea()->update();
    }

    auto RenderWindow2D::reset2DView() -> void {
        //find which axis
        MedicalHelper::Axis ax;
        auto root = getSceneGraph();

        SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root, "volData");//find any volume Data

        if (volData) {
            float slack;
            auto dims = volData->getDimension();
            auto spacing = volData->getVoxelSize();

            float x_len = dims[0] * spacing[0];
            float y_len = dims[1] * spacing[1];
            float z_len = dims[2] * spacing[2];
            auto wh = static_cast<float>(size().height());
            auto ww = static_cast<float>(size().width());
            auto windowSlack = wh > ww ? ww / wh : wh / ww;
            if (d->ID == 0) {
                ax = MedicalHelper::Axis::AXIAL;
                slack = y_len / x_len;
            }
            else if (d->ID == 1) {
                ax = MedicalHelper::SAGITTAL;
                slack = z_len / y_len;
            }
            else if (d->ID == 2) {
                ax = MedicalHelper::CORONAL;
                slack = z_len / x_len;
            }
            else {
                return;
            }

            if (windowSlack < slack) {
                slack = windowSlack > 1 ? 1.0 : windowSlack;
            }

            MedicalHelper::orientView(ax, getCamera(), volData, slack);

            change2DColorMap(0);
            d->levelWindow->setLower(d->level_min);
            d->levelWindow->setUpper(d->level_max);
        }
    }

    void RenderWindow2D::lowerSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->min = int_val;
            }
        }
    }

    void RenderWindow2D::lowerLevelChanged(int val) {
        d->levelWindow->setLower(val, false);
        //d->level_min = val;

        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->min = val;// d->level_min;
            }
            /*
            auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
            if (mipRange) {
                mipRange->min = d->level_min;
            }*/
        }
    }

    void RenderWindow2D::upperSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->max = int_val;// d->level_max;
            }
        }
    }

    void RenderWindow2D::upperLevelChanged(int val) {
        d->levelWindow->setUpper(val, false);
        //d->level_max = val;
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->max = val;// d->level_max;
            }
            /*
            auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
            if (mipRange) {
                mipRange->max = d->level_max;
            }
            */
        }
    }

    auto RenderWindow2D::change2DColorMap(int idx) -> void {
        auto rootScene = getSceneGraph();

        if (rootScene) {
            auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colormapHTSlice");
            if (tf) {
                int pred = 0;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                d->colorMap = idx;
                tf->predefColorMap = pred;
            }
        }
    }

    auto RenderWindow2D::initRangeSlider() -> void {
        auto newScene = getSceneGraph();
        if (newScene) {
            //auto volName = "volData";
            auto volData = MedicalHelper::find<SoVolumeData>(newScene);
            if (volData) {
                d->levelWindow->setMinMax(d->level_min, d->level_max);
                d->levelWindow->setLower(d->level_min);
                d->levelWindow->setUpper(d->level_max);
            }
        }
    }
    auto RenderWindow2D::refreshRangeSlider() -> void {
        d->is_reload = true;
    }

    auto RenderWindow2D::setHTRange(double min, double max) -> void {
        d->level_min = min;
        d->level_max = max;
        d->levelWindow->blockSignals(true);
        d->levelWindow->setMinMax(d->level_min, d->level_max);
        d->levelWindow->setLower(d->level_min);
        d->levelWindow->setUpper(d->level_max);
        d->levelWindow->blockSignals(false);
    }

    auto RenderWindow2D::MouseMoveEvent(SoEventCallback* node) -> void {
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (isNavigation()) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->right_mouse_pressed) {
                auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
                auto factor = 1.0 - diff * 1.5;
                if (factor > 0) {
                    getCamera()->scaleHeight(factor / d->prev_scale);
                    d->prev_scale = factor;
                }
            }
            if (d->left_mouse_pressed) {
                auto coord = CalcVolumeCoord(moveEvent->getNormalizedPosition(getViewportRegion()));
                emit sig2dCoord(coord[0], coord[1], coord[2]);
                node->setHandled();
            }
            if (d->middle_mouse_pressed) {
                //panCamera(moveEvent->getPositionFloat());
                panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
        }
    }

    /*void RenderWindow2D::wheelEvent(QWheelEvent* event) {
        auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
        emit sigWheel(delta);
    }*/

    auto RenderWindow2D::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        emit sigWheel(delta);
    }
        
    auto RenderWindow2D::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();        

        if(SoMouseButtonEvent::isButtonPressEvent(mouseButton,SoMouseButtonEvent::BUTTON1)) {
            //Press Event
            auto cx = mouseButton->getPositionFloat()[0] / devicePixelRatio();
            auto cy = -mouseButton->getPositionFloat()[1] / devicePixelRatio() + height() - 1;
            QPoint localc(cx, cy);
            auto globalc = mapToGlobal(localc);
            emit sigGlobalPos(globalc.x(), globalc.y());
        }
                
        //Release Event
        if (!isNavigation()) {//selection mode
            
        }
        else {//navigation mode
            auto pos = mouseButton->getPositionFloat();
            auto vr = getViewportRegion();
            auto normpos = mouseButton->getNormalizedPosition(vr);
            auto viewport_size = vr.getViewportSizePixels();
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = true;
                d->right_stamp = normpos[1];
                d->prev_scale = 1.0;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = false;
                d->right_stamp = 0.0;
                d->prev_scale = 1.0;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->middle_mouse_pressed = true;
                //startPan(mouseButton->getPosition());
                startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->middle_mouse_pressed = false;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                    showContextMenu(pos);
                    //node->setHandled();
                }
                d->left_mouse_pressed = false;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (false == (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25)) {
                    auto coord = CalcVolumeCoord(normpos);
                    emit sig2dCoord(coord[0], coord[1], coord[2]);
                    d->left_mouse_pressed = true;
                }
            }
        }
    }
    auto RenderWindow2D::CalcVolumeCoord(SbVec2f norm_point) -> SbVec3f {
        SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
        SbVec2f normPoint = norm_point;

        auto root = getSceneGraph();

        rayPick.setNormalizedPoint(normPoint);
        rayPick.apply(root);

        SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
        if (pickedPoint) {
            auto pt = pickedPoint->getPoint();
            if (d->ID == 1) {
                auto temp = pt[1];
                pt[1] = pt[2];
                pt[2] = temp;
            }
            SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root,"volData");//find any volume Data
            auto coord = volData->XYZToVoxel(pt);
            return coord;
        }
        return SbVec3f();
    }

    auto RenderWindow2D::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback        
            //set handled를 통해 event를 scene의 하위 callback으로 전달하지 않고 terminate
            //instance->setSeekMode(true);
            //seek mode는 우선 지원하지 않는것으로 20.10.16 Jose T. Kim
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode가 selection 일때 alt키를 누르는 동안 임시로 navigation mode로의 전환
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation()) {
                setInteractionMode(false);
            }
            else {
                setInteractionMode(true);
            }
        }
        //Shift + F12는 native viewer event callback이 해결하도록
    }

    auto RenderWindow2D::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto RenderWindow2D::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.0,0.0,0.0 };
        SbVec3f end_color = { 0.0,0.0,0.0 };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(true);//orthographic

        //set default navigation type
        setNavigationMode(false);//plane
    }
}
