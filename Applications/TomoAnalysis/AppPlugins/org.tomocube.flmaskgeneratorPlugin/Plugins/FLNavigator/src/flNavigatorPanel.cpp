#include <UIUtility.h>

#include "ui_flNavigatorPanel.h"
#include "flNavigatorPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct NavigatorPanel::Impl {
        Ui::flnvForm* ui{ nullptr };

        Interactor::NavigatorDS::Pointer navigatorDS;

        float resolutionX = 0.f;
        float resolutionY = 0.f;
        float resolutionZ = 0.f;
    };
    NavigatorPanel::NavigatorPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::flnvForm();
        d->ui->setupUi(this);

        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->locationLabel->setObjectName("h8");
        d->ui->locationUnitlabel->setObjectName("label-unit");
        d->ui->xPhyLabel->setObjectName("h9");
        d->ui->yPhyLabel->setObjectName("h9");
        d->ui->zPhyLabel->setObjectName("h9");        
        d->ui->xAxisLabel->setObjectName("h9");
        d->ui->yAxisLabel->setObjectName("h9");
        d->ui->zAxisLabel->setObjectName("h9");

        InitConnections();
    }
    NavigatorPanel::~NavigatorPanel() {
        delete d->ui;
    }
    auto NavigatorPanel::InitConnections() -> void {
        connect(d->ui->xPhySpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnXphySpinChanged(double)));
        connect(d->ui->yPhySpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnYphySpinChnaged(double)));
        connect(d->ui->zPhySpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnZphySpinChanged(double)));
        connect(d->ui->xAxisSlider, SIGNAL(valueChanged(int)), this, SLOT(OnXsliderChanged(int)));
        connect(d->ui->yAxisSlider, SIGNAL(valueChanged(int)), this, SLOT(OnYsliderChanged(int)));
        connect(d->ui->zAxisSlider, SIGNAL(valueChanged(int)), this, SLOT(OnZsliderChanged(int)));
        connect(d->ui->xAxisSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnXspinChanged(int)));
        connect(d->ui->yAxisSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnYspinChanged(int)));
        connect(d->ui->zAxisSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnZspinChanged(int)));
    }
    auto NavigatorPanel::Update() -> bool {
        auto ds = GetNavigatorDS();
        d->navigatorDS = ds;

        TC::SilentCall(d->ui->xAxisSlider)->setRange(0, ds->rangeX - 1);
        TC::SilentCall(d->ui->xAxisSpinBox)->setRange(0, ds->rangeX - 1);

        TC::SilentCall(d->ui->yAxisSlider)->setRange(0, ds->rangeY - 1);
        TC::SilentCall(d->ui->yAxisSpinBox)->setRange(0, ds->rangeY - 1);

        TC::SilentCall(d->ui->zAxisSlider)->setRange(0, ds->rangeZ - 1);
        TC::SilentCall(d->ui->zAxisSpinBox)->setRange(0, ds->rangeZ - 1);

        TC::SilentCall(d->ui->xAxisSlider)->setValue(ds->x);
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(ds->x);

        TC::SilentCall(d->ui->yAxisSlider)->setValue(ds->y);
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(ds->y);

        TC::SilentCall(d->ui->zAxisSlider)->setValue(ds->z);
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(ds->z);

        TC::SilentCall(d->ui->xPhySpinBox)->setRange(0, (float)ds->rangeX * ds->resX);
        TC::SilentCall(d->ui->yPhySpinBox)->setRange(0, (float)ds->rangeY * ds->resY);
        TC::SilentCall(d->ui->zPhySpinBox)->setRange(0, (float)ds->rangeZ * ds->resZ);

        TC::SilentCall(d->ui->xPhySpinBox)->setSingleStep(ds->resX);
        TC::SilentCall(d->ui->yPhySpinBox)->setSingleStep(ds->resY);
        TC::SilentCall(d->ui->zPhySpinBox)->setSingleStep(ds->resZ);

        TC::SilentCall(d->ui->xPhySpinBox)->setValue(ds->resX * ds->x);
        TC::SilentCall(d->ui->yPhySpinBox)->setValue(ds->resY * ds->y);
        TC::SilentCall(d->ui->zPhySpinBox)->setValue(ds->resZ * ds->z);

        d->resolutionX = ds->resX;
        d->resolutionY = ds->resY;
        d->resolutionZ = ds->resZ;

        return true;
    }

    auto NavigatorPanel::Refresh() -> bool {
        auto ds = GetNavigatorDS();
        d->navigatorDS = ds;

        TC::SilentCall(d->ui->xAxisSlider)->setValue(ds->x);
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(ds->x);

        TC::SilentCall(d->ui->yAxisSlider)->setValue(ds->y);
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(ds->y);

        TC::SilentCall(d->ui->zAxisSlider)->setValue(ds->z);
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(ds->z);

        TC::SilentCall(d->ui->xPhySpinBox)->setValue(ds->resX * static_cast<float>(ds->x));
        TC::SilentCall(d->ui->yPhySpinBox)->setValue(ds->resY * static_cast<float>(ds->y));
        TC::SilentCall(d->ui->zPhySpinBox)->setValue(ds->resZ * static_cast<float>(ds->z));

        return true;
    }
    auto NavigatorPanel::Init() -> bool {
        SetEnableUI(false);

        return true;
    }

    auto NavigatorPanel::Reset() -> bool {
        auto ds = GetNavigatorDS();
        ds->x = 0;
        ds->y = 0;
        ds->z = 0;
        ds->resX = 0.f;
        ds->resY = 0.f;
        ds->resZ = 0.f;

        Refresh();
        return true;
    }
    auto NavigatorPanel::SetEnableUI(const bool& enable) -> void {
        d->ui->xAxisLabel->setEnabled(enable);
        d->ui->xAxisSlider->setEnabled(enable);
        d->ui->xAxisSpinBox->setEnabled(enable);

        d->ui->yAxisLabel->setEnabled(enable);
        d->ui->yAxisSlider->setEnabled(enable);
        d->ui->yAxisSpinBox->setEnabled(enable);

        d->ui->zAxisLabel->setEnabled(enable);
        d->ui->zAxisSlider->setEnabled(enable);
        d->ui->zAxisSpinBox->setEnabled(enable);

        d->ui->xPhySpinBox->setEnabled(enable);
        d->ui->yPhySpinBox->setEnabled(enable);
        d->ui->zPhySpinBox->setEnabled(enable);
    }
    auto NavigatorPanel::NotifyChangedAxisSlider() -> void {
        emit positionChanged(d->ui->xAxisSpinBox->value(),
            d->ui->yAxisSpinBox->value(),
            d->ui->zAxisSpinBox->value());
    }
    auto NavigatorPanel::NotifyChangedAxisSpinBox() -> void {
        emit positionChanged(d->ui->xAxisSlider->value(),
            d->ui->yAxisSlider->value(),
            d->ui->zAxisSlider->value());
    }
    auto NavigatorPanel::NotifyChangedPhySpinBox() -> void {        
        emit positionChanged(static_cast<int>(d->ui->xPhySpinBox->value() / static_cast<double>(d->resolutionX)),
            static_cast<int>(d->ui->yPhySpinBox->value() / static_cast<double>(d->resolutionY)),
            static_cast<int>(d->ui->zPhySpinBox->value() / static_cast<double>(d->resolutionZ)));
    }

    //SLOTS
    void NavigatorPanel::OnXphySpinChanged(double val) {        
        auto iVal = static_cast<int>(val / d->resolutionX);
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(iVal);
        TC::SilentCall(d->ui->xAxisSlider)->setValue(iVal);
        NotifyChangedPhySpinBox();
    }
    void NavigatorPanel::OnYphySpinChnaged(double val) {        
        auto iVal = static_cast<int>(val / d->resolutionY);
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(iVal);
        TC::SilentCall(d->ui->yAxisSlider)->setValue(iVal);
        NotifyChangedPhySpinBox();
    }
    void NavigatorPanel::OnZphySpinChanged(double val) {        
        auto iVal = static_cast<int>(val / d->resolutionZ);
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(iVal);
        TC::SilentCall(d->ui->zAxisSlider)->setValue(iVal);
        NotifyChangedPhySpinBox();
    }
    void NavigatorPanel::OnXsliderChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionX;
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(val);
        TC::SilentCall(d->ui->xPhySpinBox)->setValue(fval);
        NotifyChangedAxisSlider();
    }
    void NavigatorPanel::OnYsliderChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionY;
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(val);
        TC::SilentCall(d->ui->yPhySpinBox)->setValue(fval);
        NotifyChangedAxisSlider();
    }
    void NavigatorPanel::OnZsliderChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionZ;
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(val);
        TC::SilentCall(d->ui->zPhySpinBox)->setValue(fval);
        NotifyChangedAxisSlider();
    }
    void NavigatorPanel::OnXspinChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionX;
        TC::SilentCall(d->ui->xAxisSlider)->setValue(val);
        TC::SilentCall(d->ui->xPhySpinBox)->setValue(fval);
        NotifyChangedAxisSpinBox();
    }
    void NavigatorPanel::OnYspinChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionY;
        TC::SilentCall(d->ui->yAxisSlider)->setValue(val);
        TC::SilentCall(d->ui->yPhySpinBox)->setValue(fval);
        NotifyChangedAxisSpinBox();
    }
    void NavigatorPanel::OnZspinChanged(int val) {
        auto fval = static_cast<float>(val) * d->resolutionZ;
        TC::SilentCall(d->ui->zAxisSlider)->setValue(val);
        TC::SilentCall(d->ui->zPhySpinBox)->setValue(fval);
        NotifyChangedAxisSpinBox();
    }
}