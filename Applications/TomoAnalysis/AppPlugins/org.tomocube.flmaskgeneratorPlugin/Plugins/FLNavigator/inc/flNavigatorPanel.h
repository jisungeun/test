#pragma once

#include <memory>

#include <QWidget>

#include <IFLNavigatorPanel.h>

#include "FLNavigatorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLNavigator_API NavigatorPanel : public QWidget, public Interactor::INavigatorPanel {
        Q_OBJECT
    public:
        typedef NavigatorPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        NavigatorPanel(QWidget* parent = nullptr);
        ~NavigatorPanel();

        auto Update() -> bool override;
        auto Refresh()->bool override;

        auto Init()->bool;
        auto Reset()->bool;
    signals:
        void positionChanged(int x, int y, int z);
        void phyPositionChanged(float x, float y, float z);
    protected slots:
        void OnXphySpinChanged(double val);
        void OnYphySpinChnaged(double val);
        void OnZphySpinChanged(double val);

        void OnXsliderChanged(int val);
        void OnYsliderChanged(int val);
        void OnZsliderChanged(int val);
        void OnXspinChanged(int val);
        void OnYspinChanged(int val);
        void OnZspinChanged(int val);                

    private:
        auto NotifyChangedAxisSpinBox()->void;
        auto NotifyChangedAxisSlider()->void;
        auto NotifyChangedPhySpinBox()->void;

        auto InitConnections()->void;
        auto SetEnableUI(const bool& enable)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}