#pragma once

#include <memory>

#include <QObject>

#include <IFLSceneManagerWidget.h>

#include "FLSceneManagerExport.h"

class SoSeparator;
class SoTransferFunction;
class SoVolumeData;
class SbColor;

namespace TomoAnalysis::FLMaskGenerator::Plugins {    
    class FLSceneManager_API SceneManagerWidget : public QObject, public Interactor::ISceneManagerWidget {
        Q_OBJECT
    public:
        typedef SceneManagerWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        SceneManagerWidget(QObject* parent = nullptr);
        ~SceneManagerWidget();

        auto Update() -> bool override;
        auto UpdateImage() -> bool override;
        auto UpdateMask() -> bool override;
        
        auto ClearSceneGraph()->void;
        auto GetRenderRoot(int idx)->SoSeparator*;
        auto SetSliceIndex(int viewIdx, int sliceIdx)->void;
        auto SetFLOpacity(int ch, float opacity)->void;
        auto SetFLToggle(int ch, bool viz)->void;
        auto SetMaskOpacity(float opacity)->void;
        auto SetMaskToggle(bool ch1Viz, bool ch2Viz, bool ch3Viz)->void;

        auto CloseController()->void;
        auto SetGlobalPos(int posX, int posY)->void;        

    protected slots:
        void OnLabelPicking(int);
        void OnXYUp();
        void OnXYDown();
        void OnXYRight();
        void OnXYLeft();
        void OnXYOk();
        void OnDilate();
        void OnErode();
        void OnFlush();

        void OnUp();
        void OnDown();
        void OnOk();

        
    protected:
        auto makeColorMap(SbColor color,SbColor subColor, int id)->SoTransferFunction*;
        auto modifyColorMap(SbColor color, SbColor subColor, int id, int lower, int upper)->void;
        auto modifyColorMapOpacity(int id,float opacity, float toggle = 1.0)->void;
        auto makeMaskColorMap()->SoTransferFunction*;
        auto convertFL(SoVolumeData* oriHT,SoVolumeData* oriFL, float offset)->SoVolumeData*;
        auto ShowController(int posX, int posY, int winType)->void;

    private:
        auto InitSceneGraph()->void;        
        auto InitHTDataGroup()->void;
        auto InitFLDataGroup()->void;        
        auto InitMaskDataGroup()->void;
        auto InitModifyDataGroup()->void;

        //manipulation
        auto CreateModifyVolume(int val)->void;
        auto ApplyPreviousModification(bool save = false)->void;


        struct Impl;
        std::unique_ptr<Impl> d;
    };
}