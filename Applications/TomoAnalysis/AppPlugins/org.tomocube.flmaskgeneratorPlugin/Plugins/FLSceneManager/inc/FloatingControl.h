#pragma once

#include <memory>

#include <QWidget>

#include "FLSceneManagerExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins{
	class FLSceneManager_API FloatingControl : public QWidget {
		Q_OBJECT
	public:
		FloatingControl(QWidget* parent);
		~FloatingControl();

	signals:
		void sigUp();
		void sigDown();
		void sigOK();

	protected slots:
		void OnOkBtn();
		void OnUpBtn();
		void OnDownBtn();

	protected:		
		bool eventFilter(QObject* watched, QEvent* event) override;

	private:
		auto Init()->void;
		auto InitIcons()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}