#pragma once

#include <memory>

#include <QWidget>

#include "FLSceneManagerExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLSceneManager_API FloatingControlXY : public QWidget {
        Q_OBJECT
    public:
        FloatingControlXY(QWidget* parent);
        ~FloatingControlXY();

    signals:
        void sigUp();
        void sigDown();
        void sigLeft();
        void sigRight();
        void sigOK();
        void sigErode();
        void sigDilate();
        void sigFlush();

    protected slots:
        void OnUpBtn();
        void OnDownBtn();
        void OnLeftBtn();
        void OnRightBtn();
        void OnOkBtn();
        void OnErodeBtn();
        void OnDilateBtn();
        void OnFlushBtn();

    protected:        
        bool eventFilter(QObject* watched, QEvent* event) override;

    private:
        auto Init()->void;
        auto InitIcons()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}