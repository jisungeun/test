// RGB fragment shader

//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
	vec4 clr1 = vec4(0.0);
	vec4 clr2 = vec4(0.0);
	vec4 clr3 = vec4(0.0);
	if (isCh0Exist > 0)
	{
		VVIZ_DATATYPE data_value1 = VVizGetData(data1, texCoord);
		clr1 = VVizTransferFunction(data_value1, 1);
	}	
	if (isCh1Exist > 0)
	{
		VVIZ_DATATYPE data_value2 = VVizGetData(data2, texCoord);
		clr2 = VVizTransferFunction(data_value2, 2);
	}	
	if (isCh2Exist > 0)
	{
		VVIZ_DATATYPE data_value3 = VVizGetData(data3, texCoord);
		clr3 = VVizTransferFunction(data_value3, 3);
	}	

	float w = max(clr1.w, max(clr2.w, clr3.w));

	float rr = clr1.r + clr2.r + clr3.r;
	if (rr > 1) {
		rr = 1;
	}
	float gg = clr1.g + clr2.g + clr3.g;
	if (gg > 1) {
		gg = 1;
	}
	float bb = clr1.b + clr2.b + clr3.b;
	if (bb > 1) {
		bb = 1;
	}

	vec4 color = vec4(rr, gg, bb, w);

	return color;
}
