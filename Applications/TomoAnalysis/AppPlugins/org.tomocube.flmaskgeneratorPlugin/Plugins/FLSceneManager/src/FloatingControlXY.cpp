#include <QIcon>
#include <QMouseEvent>

#include "ui_FloatingControlXY.h"
#include "FloatingControlXY.h"



namespace TomoAnalysis::FLMaskGenerator::Plugins {
	struct FloatingControlXY::Impl {
		Ui::FControlXY* ui{ nullptr };
		QPoint startPos = QPoint();
	};
	FloatingControlXY::FloatingControlXY(QWidget* parent) : QWidget(parent), d{ new Impl } {
		d->ui = new Ui::FControlXY;
		d->ui->setupUi(this);

		Init();
		InitIcons();
		installEventFilter(this);
	}
	FloatingControlXY::~FloatingControlXY() {

	}
	void FloatingControlXY::OnDilateBtn() {
		emit sigDilate();
	}
	void FloatingControlXY::OnDownBtn() {
		emit sigDown();
	}
	void FloatingControlXY::OnErodeBtn() {
		emit sigErode();
	}
	void FloatingControlXY::OnFlushBtn() {
		emit sigFlush();
	}
	void FloatingControlXY::OnLeftBtn() {
		emit sigLeft();
	}
	void FloatingControlXY::OnRightBtn() {
		emit sigRight();
	}
	void FloatingControlXY::OnOkBtn() {
		emit sigOK();
	}
	void FloatingControlXY::OnUpBtn() {
		emit sigUp();
	}	
	bool FloatingControlXY::eventFilter(QObject* watched, QEvent* event) {		
		if (event->type() == QMouseEvent::MouseButtonPress) {
			const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
			if (mouseEvent->button() == Qt::MiddleButton) {
				d->startPos = mouseEvent->pos();
				return true;
			}
		}
		else if (event->type() == QMouseEvent::MouseMove) {
			const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
			if (false == d->startPos.isNull()) {
				this->move(pos() + mouseEvent->pos() - d->startPos);
				return true;
			}
		}
		else if (event->type() == QMouseEvent::MouseButtonRelease) {
			const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
			if (mouseEvent->button() == Qt::MiddleButton) {
				d->startPos = QPoint();
				return true;
			}
		}

		return QObject::eventFilter(watched, event);
	}
	auto FloatingControlXY::Init() -> void {
		connect(d->ui->OkBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
		connect(d->ui->DilateBtn, SIGNAL(clicked()), this, SLOT(OnDilateBtn()));
		connect(d->ui->DownBtn, SIGNAL(clicked()), this, SLOT(OnDownBtn()));
		connect(d->ui->ErodeBtn, SIGNAL(clicked()), this, SLOT(OnErodeBtn()));
		connect(d->ui->FlushBtn, SIGNAL(clicked()), this, SLOT(OnFlushBtn()));
		connect(d->ui->LeftBtn, SIGNAL(clicked()), this, SLOT(OnLeftBtn()));
		connect(d->ui->RightBtn, SIGNAL(clicked()), this, SLOT(OnRightBtn()));
		connect(d->ui->UpBtn, SIGNAL(clicked()), this, SLOT(OnUpBtn()));
		setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
		setAttribute(Qt::WA_TranslucentBackground);
		d->ui->OkBtn->setStyleSheet("background-color: #334449;");
		d->ui->DilateBtn->setStyleSheet("background-color: #334449;");
		d->ui->DownBtn->setStyleSheet("background-color: #334449;");
		d->ui->ErodeBtn->setStyleSheet("background-color: #334449;");
		d->ui->FlushBtn->setStyleSheet("background-color: #334449;");
		d->ui->LeftBtn->setStyleSheet("background-color: #334449;");
		d->ui->RightBtn->setStyleSheet("background-color: #334449;");
		d->ui->UpBtn->setStyleSheet("background-color: #334449;");
	}
	auto FloatingControlXY::InitIcons() -> void {
		d->ui->UpBtn->setIcon(QIcon(":/image/images/Up.png"));
		d->ui->UpBtn->setIconSize(QSize(45, 45));
		d->ui->DownBtn->setIcon(QIcon(":/image/images/Down.png"));
		d->ui->DownBtn->setIconSize(QSize(45, 45));
		d->ui->RightBtn->setIcon(QIcon(":/image/images/Right.png"));
		d->ui->RightBtn->setIconSize(QSize(45, 45));
		d->ui->LeftBtn->setIcon(QIcon(":/image/images/Left.png"));
		d->ui->LeftBtn->setIconSize(QSize(45, 45));
		d->ui->OkBtn->setIcon(QIcon(":/image/images/OK.png"));
		d->ui->OkBtn->setIconSize(QSize(45, 45));
		d->ui->DilateBtn->setIcon(QIcon(":/image/images/Dilate.png"));
		d->ui->DilateBtn->setIconSize(QSize(45, 45));
		d->ui->ErodeBtn->setIcon(QIcon(":/image/images/Erode.png"));
		d->ui->ErodeBtn->setIconSize(QSize(45, 45));
		d->ui->FlushBtn->setIcon(QIcon(":/image/images/FlushSelection.png"));
		d->ui->FlushBtn->setIconSize(QSize(45, 45));
	}
}
