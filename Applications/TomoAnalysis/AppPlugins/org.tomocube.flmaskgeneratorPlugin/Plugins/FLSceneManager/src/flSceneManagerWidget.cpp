#include <QCoreApplication>

#include <TCDataConverter.h>
#include "FloatingControl.h"
#include "FloatingControlXY.h"
#include <OivPickHandler.h>

#pragma warning(push)
#pragma warning(disable:4819)
//Open Inventor
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoResetImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoCombineByMaskProcessing.h>
#include <ImageViz/Engines/GeometryAndMatching/GeometricTransforms/SoTranslateProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoDilationCubeProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionCubeProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoReorderLabelsProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)


#include "flSceneManagerWidget.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    float color_table[20][3]{
    230,25,75, //red
    60,180,75, //green
    255,255,25, //yellow
    0,130,200, //blue
    245,130,48, //Orange
    145,30,180, //Purple
    70,240,240, //Cyan
    240,50,230, //Magenta
    210,245,60, //Lime
    250,190,212, //Pink
    0,128,128, //Teal
    220,190,255, //Lavender
    170,110,40, //Brown
    255,250,200, //Beige
    128,0,0,//Maroon
    170,255,195, //Mint
    128,128,0, //Olive
    255,215,180, //Apricot
    0,0,128, //Navy
    128,128,128 //Gray
    };
    struct SceneManagerWidget::Impl {
        SoSeparator* renderRootSep[3]{ nullptr,nullptr,nullptr };                        
        SoSeparator* htSeparator[3]{ nullptr };
        SoGroup* htGroup{ nullptr };
        SoGroup* infoGroup{ nullptr };
        SoInfo* htMinInfo{ nullptr };
        SoInfo* htMaxInfo{ nullptr };
        SoMaterial* htMatl{ nullptr };
        SoSwitch* htSocket{ nullptr };
        SoDataRange* htRange{ nullptr };
        SoTransferFunction* tfImage{ nullptr };
        SoOrthoSlice* htSlice[3]{ nullptr };
        SoRotationXYZ* yzRotation{ nullptr };

        SoSeparator* flSeparator[3]{ nullptr };
        SoTranslation* flTrans[3]{ nullptr,nullptr,nullptr };
        SoMultiDataSeparator* flMDS{ nullptr};
        SoSwitch* flSokcet[3]{ nullptr,nullptr,nullptr };
        SoDataRange* flRange[3]{ nullptr,nullptr,nullptr };
        SoTransferFunction* flTF[3]{ nullptr,nullptr,nullptr };
        
        SoFragmentShader* flFragmentShader[3]{ nullptr ,nullptr,nullptr};
        SoVolumeShader* flShader[3]{ nullptr,nullptr,nullptr,};
        SoMaterial* flMatl{ nullptr };
        SoGroup* flSliceGroup[3]{ nullptr,nullptr,nullptr };
        SoOrthoSlice* flSlice[3]{ nullptr,nullptr,nullptr };
        SoSwitch* flIndi[3]{ nullptr,nullptr,nullptr };
        SoSwitch* flInhe{ nullptr };

        SoSeparator* maskSeparator[3]{ nullptr,nullptr,nullptr };
        SoTranslation* maskTrans[3]{ nullptr,nullptr,nullptr };
        SoMaterial* maskMatl{ nullptr };
        SoGroup* maskGroup{ nullptr };
        SoSwitch* maskSocket{ nullptr};
        SoDataRange* maskRange{ nullptr };
        SoTransferFunction* maskTF{nullptr};        
        SoOrthoSlice* maskSlice[3]{ nullptr,nullptr,nullptr };

        SoSeparator* modifySeparator[3]{ nullptr,nullptr,nullptr };        
        SoMaterial* modifyMatl{ nullptr };
        SoGroup* modifyGroup{ nullptr };
        SoSwitch* modifySocket{ nullptr };
        SoDataRange* modifyRange{ nullptr };
        SoTransferFunction* modifyTF{ nullptr };
        SoOrthoSlice* modifySlice[3]{ nullptr,nullptr,nullptr };
        SoVolumeData* modifyVol{ nullptr };
        SoVolumeData* curMaskVol{ nullptr };

        int curDim[3];
        float flOpacity[3]{ 0.7f,0.7f,0.7f };
        bool flViz[3]{ true,true,true };
        float maskOpacity{ 0.7f };

        float chMinMax[3][2];
        float chUpperRatio[3];
        float chLowerRatio[3];

        FloatingControl* fcontrol{ nullptr };
        FloatingControlXY* fcontrolXY{ nullptr };
        int curWinType{ -1 };
        OivPickHandler* picker[3]{ nullptr };
        int curPosX{ 0 };
        int curPosY{ 0 };
        int curLabelValue{ 0 };
        float transSingleStep{ 3 };
    };
    SceneManagerWidget::SceneManagerWidget(QObject* parent) :d{ new Impl }, QObject(parent) {             
        InitSceneGraph();        
        d->fcontrol = new FloatingControl(nullptr);
        d->fcontrolXY = new FloatingControlXY(nullptr);

        for(auto i=0;i<3;i++) {
            d->picker[i] = new OivPickHandler;
            d->renderRootSep[i]->addChild(d->picker[i]->GetSceneGraph());            
            connect(d->picker[i], qOverload<int>(&OivPickHandler::pickValue), this, &SceneManagerWidget::OnLabelPicking);
        }
        d->picker[0]->SetAxis(MedicalHelper::AXIAL);
        d->picker[1]->SetAxis(MedicalHelper::SAGITTAL);
        d->picker[2]->SetAxis(MedicalHelper::CORONAL);
        d->picker[1]->SetRotation(true);

        connect(d->fcontrol, SIGNAL(sigOK()), this, SLOT(OnOk()));
        connect(d->fcontrol, SIGNAL(sigDown()), this, SLOT(OnDown()));
        connect(d->fcontrol, SIGNAL(sigUp()), this, SLOT(OnUp()));
        connect(d->fcontrolXY, SIGNAL(sigUp()), this, SLOT(OnXYUp()));
        connect(d->fcontrolXY, SIGNAL(sigDown()), this, SLOT(OnXYDown()));
        connect(d->fcontrolXY, SIGNAL(sigLeft()), this, SLOT(OnXYLeft()));
        connect(d->fcontrolXY, SIGNAL(sigRight()), this, SLOT(OnXYRight()));
        connect(d->fcontrolXY, SIGNAL(sigOK()), this, SLOT(OnXYOk()));
        connect(d->fcontrolXY, SIGNAL(sigDilate()), this, SLOT(OnDilate()));
        connect(d->fcontrolXY, SIGNAL(sigErode()), this, SLOT(OnErode()));
        connect(d->fcontrolXY, SIGNAL(sigFlush()), this, SLOT(OnFlush()));
    }
    SceneManagerWidget::~SceneManagerWidget() {        
        delete d->fcontrol;
        delete d->fcontrolXY;
    }
    void SceneManagerWidget::OnXYDown() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        translate->translationVector.setValue(0, d->transSingleStep, 0);
        

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnXYUp() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        translate->translationVector.setValue(0, -d->transSingleStep, 0);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnXYLeft() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        translate->translationVector.setValue(-d->transSingleStep, 0, 0);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }    
    void SceneManagerWidget::OnXYRight() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        translate->translationVector.setValue(d->transSingleStep, 0, 0);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }    
    void SceneManagerWidget::OnXYOk() {
        ApplyPreviousModification(true);
        d->fcontrolXY->hide();
    }
    void SceneManagerWidget::OnDilate() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);        

        SoRef<SoDilationCubeProcessing> dilate = new SoDilationCubeProcessing;
        dilate->elementSize = 1;
        dilate->inImage = adap.ptr();

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&dilate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnErode() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoErosionCubeProcessing> erode = new SoErosionCubeProcessing;
        erode->elementSize = 1;
        erode->inImage = adap.ptr();

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&erode->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnFlush() {
        d->curLabelValue = 0;
        d->modifySocket->whichChild = -1;

        //MedicalHelper::dicomAdjustVolume(d->curMaskVol);
        SoRef<SoMemoryDataAdapter> oriAdap = MedicalHelper::getImageDataAdapter(d->curMaskVol);
        oriAdap->interpretation = SoMemoryDataAdapter::LABEL;

        SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;
        reorder->inLabelImage = oriAdap.ptr();

        SoRef<SoConvertImageProcessing> convert = new SoConvertImageProcessing;
        convert->inImage.connectFrom(&reorder->outLabelImage);
        convert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

        SoRef<SoVRImageDataReader> mreader = new SoVRImageDataReader;
        mreader->imageData.connectFrom(&convert->outImage);

        d->curMaskVol->setReader(*mreader, TRUE);
        d->curMaskVol->touch();

        double min, max;
        d->curMaskVol->getMinMax(min, max);

        //create colormap for new mask
        d->maskTF->colorMap.setNum(256 * 4);
        float* p = d->maskTF->colorMap.startEditing();
        for (int i = 0; i < 256; i++) {
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
            else {
                float r = color_table[(idx - 1) % 20][0] / 255.0;
                float g = color_table[(idx - 1) % 20][1] / 255.0;
                float b = color_table[(idx - 1) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 0.7;
            }
        }
        d->maskTF->colorMap.finishEditing();
        //set mask data range                

        MedicalHelper::dicomAdjustDataRange(d->maskRange, d->curMaskVol);
        auto rangemin = d->maskRange->min.getValue();
        d->maskRange->min.setValue(rangemin - 0.1);
    }
    void SceneManagerWidget::OnDown() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        if (d->curWinType == 1) {
            translate->translationVector.setValue(0, d->transSingleStep, 0);
        }
        else if (d->curWinType == 2) {
            translate->translationVector.setValue(0, 0, -d->transSingleStep);
        }

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnUp() {
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(d->modifyVol);

        SoRef<SoTranslateProcessing> translate = new SoTranslateProcessing;        
        translate->backgroundValue = 0;
        translate->inImage = adap.ptr();
        if (d->curWinType == 1) {
            translate->translationVector.setValue(0,-d->transSingleStep,0);
        }else if(d->curWinType == 2){
            translate->translationVector.setValue(0,0, d->transSingleStep);
        }

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&translate->outImage);

        d->modifyVol->setReader(*reader, TRUE);
        d->modifyVol->data.touch();
    }
    void SceneManagerWidget::OnOk() {
        ApplyPreviousModification(true);
        d->fcontrol->hide();
    }
    void SceneManagerWidget::OnLabelPicking(int val) {
        auto p = dynamic_cast<OivPickHandler*>(sender());
        d->fcontrol->hide();
        d->fcontrolXY->hide();        
        if(val > 0) {            
            auto winType = 0;
            if(p->GetAxis() == MedicalHelper::SAGITTAL) {
                winType = 1;
            }else if(p->GetAxis() == MedicalHelper::CORONAL) {
                winType = 2;
            }
            ShowController(d->curPosX, d->curPosY, winType);
            CreateModifyVolume(val);
        }else {
            d->modifySocket->whichChild = -1;
            if (d->curLabelValue > 0) {
                ApplyPreviousModification();
            }
        }
        d->curLabelValue = val;
    }

    auto SceneManagerWidget::CreateModifyVolume(int val) -> void {
        if(nullptr == d->curMaskVol) {
            return;
        }
        if (d->curLabelValue > 0 && d->curLabelValue != val) {
            ApplyPreviousModification();
        }
        //MedicalHelper::dicomAdjustVolume(d->curMaskVol);
        SoRef<SoMemoryDataAdapter> maskAdap = MedicalHelper::getImageDataAdapter(d->curMaskVol);
        //create modifiable mask
        SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
        threshold->inImage = maskAdap.ptr();
        threshold->thresholdLevel.setValue(static_cast<float>(val), static_cast<float>(val + 0.5));
        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&threshold->outBinaryImage);                
        d->modifyVol->setReader(*reader, TRUE);                
        d->modifySocket->whichChild = 0;

        //remove selected label
        SoRef<SoLogicalNotProcessing> not = new SoLogicalNotProcessing;
        not->inImage.connectFrom(&threshold->outBinaryImage);
        SoRef<SoMaskImageProcessing> masking = new SoMaskImageProcessing;
        masking->inBinaryImage.connectFrom(&not->outImage);
        masking->inImage = maskAdap.ptr();

        SoRef<SoVRImageDataReader> oriReader = new SoVRImageDataReader;
        oriReader->imageData.connectFrom(&masking->outImage);
        d->curMaskVol->setReader(*oriReader, TRUE);
        d->curMaskVol->data.touch();
    }
    auto SceneManagerWidget::ApplyPreviousModification(bool save) -> void {
        //MedicalHelper::dicomAdjustVolume(d->curMaskVol);
        SoRef<SoMemoryDataAdapter> maskAdap = MedicalHelper::getImageDataAdapter(d->curMaskVol);
        //MedicalHelper::dicomAdjustVolume(d->modifyVol);
        SoRef<SoMemoryDataAdapter> modiAdap = MedicalHelper::getImageDataAdapter(d->modifyVol);
        modiAdap->interpretation = SoMemoryDataAdapter::BINARY;

        SoRef<SoResetImageProcessing> reset = new SoResetImageProcessing;
        reset->inImage = maskAdap.ptr();
        reset->intensityValue = static_cast<float>(d->curLabelValue);

        SoRef<SoCombineByMaskProcessing> combine = new SoCombineByMaskProcessing;
        combine->inImage1.connectFrom(&reset->outImage);
        combine->inImage2 = maskAdap.ptr();
        combine->inBinaryImage = modiAdap.ptr();

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&combine->outImage);

        d->curMaskVol->setReader(*reader, TRUE);
        d->curMaskVol->data.touch();

        if(save) {
            d->curLabelValue = 0;
            d->modifySocket->whichChild = -1;
        }
    }
    auto SceneManagerWidget::CloseController() -> void {
        d->fcontrol->close();
        d->fcontrolXY->close();
    }

    auto SceneManagerWidget::SetGlobalPos(int posX, int posY) -> void {
        d->curPosX = posX;
        d->curPosY = posY;
    }

    auto SceneManagerWidget::ShowController(int posX, int posY, int winType) -> void {                
        d->curWinType = winType;
        if(winType==0) {
            d->fcontrolXY->show();
            d->fcontrolXY->move(posX, posY);

        }else {
            d->fcontrol->show();
            d->fcontrol->move(posX, posY);
        }
    }
    auto SceneManagerWidget::Update() -> bool {
        //update parameters
        SbColor rgb[3] = { SbColor(0,0,1),SbColor(0,1,0),SbColor(1,0,0) };
        SbColor srgb[3] = { SbColor(1,1,0),SbColor(1,0,1),SbColor(0,1,1) };
        auto ds = GetSceneManagerDS();
        for(auto i=0;i<3;i++) {
            if(nullptr != ds->fl_image[i]) {
                //d->flRange[i]->min = ds->fl_range[i][0];
                //d->flRange[i]->max = ds->fl_range[i][1];                
                //d->flRange[i]->max = d->chMinMax[i][1];
                modifyColorMap(rgb[i], srgb[i], i, ds->fl_range[i][0], ds->fl_range[i][1]);                
                //d->flTF[i]->minValue = ds->fl_range[i][0];
                //d->flTF[i]->maxValue = ds->fl_range[i][1];
            }
        }        
        return true;
    }
    auto SceneManagerWidget::UpdateMask() -> bool {
        auto ds = GetSceneManagerDS();
        d->maskSocket->removeAllChildren();
        d->maskSocket->whichChild = -1;
        if(nullptr == ds->fl_mask) {
            return false;
        }
        auto converter = std::make_shared<TCDataConverter>();
        auto volData = converter->MaskToSoVolumeData(std::dynamic_pointer_cast<TCMask>(ds->fl_mask));
        volData->setName("MaskVolume");
        d->curMaskVol = volData;
        d->maskSocket->addChild(volData);
        d->maskSocket->whichChild = 0;

        double min, max;
        volData->getMinMax(min, max);                                

        for(auto i=0;i<3;i++) {
            d->picker[i]->SetTargetVolume(volData);
        }

        //create colormap for new mask
        d->maskTF->colorMap.setNum(256 * 4);
        float* p = d->maskTF->colorMap.startEditing();
        for(int i=0;i<256;i++) {
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
            else {
                float r = color_table[(idx - 1) % 20][0] / 255.0;
                float g = color_table[(idx - 1) % 20][1] / 255.0;
                float b = color_table[(idx - 1) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 0.7;
            }
        }
        d->maskTF->colorMap.finishEditing();
        //set mask data range                

        MedicalHelper::dicomAdjustDataRange(d->maskRange, volData);
        auto rangemin = d->maskRange->min.getValue();
        d->maskRange->min.setValue(rangemin - 0.1);        

        for(auto i=0;i<3;i++) {
            d->maskSlice[i]->sliceNumber = d->htSlice[i]->sliceNumber.getValue();
        }

        return true;
    }
    auto SceneManagerWidget::UpdateImage() -> bool {
        //clear mask        
        auto ds = GetSceneManagerDS();
        
        d->htSocket->removeAllChildren();
        d->htSocket->whichChild = -1;
        for(auto i=0;i<3;i++) {
            d->flSokcet[i]->removeAllChildren();            
            d->flSokcet[i]->whichChild = -1;
        }
        d->maskSocket->removeAllChildren();
        d->maskSocket->whichChild = -1;
        d->curMaskVol = nullptr;
        for (auto i =0 ; i < 3; i++) {
            d->picker[i]->SetTargetVolume(nullptr);
        }
        d->modifySocket->whichChild = -1;
        if(nullptr == ds->ht_image) {
            return false;
        }

        auto converter = std::make_shared<TCDataConverter>();
        SoRef<SoVolumeData> volData = converter->ImageToSoVolumeData(std::dynamic_pointer_cast<TCImage>(ds->ht_image));
        volData->setName("volData");
        d->htSocket->addChild(volData.ptr());
        d->htSocket->whichChild = 0;

        //set data range
        auto tcimage = std::dynamic_pointer_cast<TCImage>(ds->ht_image);
        auto min = std::get<0>(tcimage->GetMinMax());
        auto max = std::get<1>(tcimage->GetMinMax());

        //volData->getMinMax(min, max);
        d->htMinInfo->string = QString::number(min).toStdString();
        d->htMaxInfo->string = QString::number(max).toStdString();
        //d->htRange->min = min;
        //d->htRange->max = max;
        auto dim = volData->getDimension();
        d->htSlice[0]->sliceNumber = dim[2] / 2;
        d->htSlice[1]->sliceNumber = dim[0] / 2;
        d->htSlice[2]->sliceNumber = dim[1] / 2;
        for(auto i=0;i<3;i++) {
            d->curDim[i] = dim[i];
        }        
        //MedicalHelper::dicomAdjustDataRange(d->htRange, volData);
        d->htRange->min = min;
        d->htRange->max = max;
        MedicalHelper::dicomCheckMonochrome1(d->tfImage, volData.ptr());                
        SbColor rgb[3] = { SbColor(0,0,1),SbColor(0,1,0),SbColor(1,0,0) };
        SbColor srgb[3] = { SbColor(1,1,0),SbColor(1,0,1),SbColor(0,1,1) };
        auto flExist = false;
        for(auto i=0;i<3;i++) {
            for (auto j = 0; j < 3; j++) {
                d->flFragmentShader[j]->setShaderParameter1i(QString("isCh%1Exist").arg(i).toStdString(), 0);
            }
            if(nullptr != ds->fl_image[i]) {
                auto flData = converter->ImageToSoVolumeData(std::dynamic_pointer_cast<TCImage>(ds->fl_image[i]));
                SoRef<SoVolumeData> convertedFL = convertFL(volData.ptr(),flData,ds->fl_offset);                
                convertedFL->dataSetId = i + 1;                
                d->flSokcet[i]->addChild(convertedFL.ptr());
                d->flSokcet[i]->whichChild = 0;

                double fmin, fmax;
                convertedFL->getMinMax(fmin, fmax);
                d->chMinMax[i][0] = static_cast<float>(fmin);
                d->chMinMax[i][1] = static_cast<float>(fmax);

                d->flRange[i]->min = fmin;
                d->flRange[i]->max = fmax;
                modifyColorMap(rgb[i], srgb[i], i,ds->fl_range[i][0],ds->fl_range[i][1]);
                //d->flTF[i]->minValue = fmin;
                //d->flTF[i]->maxValue = fmax;
                for (auto j = 0; j < 3; j++) {
                    d->flFragmentShader[j]->setShaderParameter1i(QString("isCh%1Exist").arg(i).toStdString(), 1);
                }
                while(flData->getRefCount()>0) {
                    flData->unref();
                }
                flData = nullptr;
                flExist = true;
            }
        }
        if(flExist) {
            d->flSlice[0]->sliceNumber = dim[2] / 2;
            d->flSlice[1]->sliceNumber = dim[0] / 2;
            d->flSlice[2]->sliceNumber = dim[1] / 2;
        }
        return true;
    }
    auto SceneManagerWidget::SetSliceIndex(int viewIdx, int sliceIdx) -> void {
        int sID;
        if(viewIdx == 0) {
            sID = 2;
        }else if(viewIdx == 1) {
            sID = 1;
        }else {
            sID = 0;
        }

        if (d->curDim[sID] > sliceIdx && sliceIdx > -1) {
            d->htSlice[viewIdx]->sliceNumber = sliceIdx;
            d->flSlice[viewIdx]->sliceNumber = sliceIdx;
            d->maskSlice[viewIdx]->sliceNumber = sliceIdx;
            d->modifySlice[viewIdx]->sliceNumber = sliceIdx;
        }
    }    
    auto SceneManagerWidget::GetRenderRoot(int idx) -> SoSeparator* {
        return d->renderRootSep[idx];
    }
    auto SceneManagerWidget::InitSceneGraph() -> void {
        InitHTDataGroup();
        InitFLDataGroup();
        InitMaskDataGroup();
        InitModifyDataGroup();
        d->yzRotation = new SoRotationXYZ;
        d->yzRotation->axis = SoRotationXYZ::X;
        d->yzRotation->angle = -1.57f;
        for(auto i=0;i<3;i++) {
            d->renderRootSep[i] = new SoSeparator;
            if(i==1) {
                d->renderRootSep[i]->addChild(d->yzRotation);
            }
            d->renderRootSep[i]->addChild(d->htSeparator[i]);
            d->renderRootSep[i]->addChild(d->flSeparator[i]);
            d->renderRootSep[i]->addChild(d->maskSeparator[i]);
            d->renderRootSep[i]->addChild(d->modifySeparator[i]);
        }
    }
    auto SceneManagerWidget::InitMaskDataGroup() -> void {
        d->maskMatl = new SoMaterial;
        d->maskMatl->ambientColor.setValue(1, 1, 1);
        d->maskMatl->diffuseColor.setValue(1, 1, 1);
        d->maskMatl->transparency = 0.3f;
        d->maskRange = new SoDataRange;
        d->maskSocket = new SoSwitch;
        d->maskSocket->whichChild = -1;
        d->maskTF = new SoTransferFunction;
        d->maskTF->predefColorMap = SoTransferFunction::NONE;
        d->maskGroup = new SoGroup;
        d->maskGroup->addChild(d->maskMatl);
        d->maskGroup->addChild(d->maskRange);
        d->maskGroup->addChild(d->maskTF);
        d->maskGroup->addChild(d->maskSocket);
        for(auto i=0;i<3;i++) {
            d->maskSeparator[i] = new SoSeparator;
            d->maskTrans[i] = new SoTranslation;
            d->maskSlice[i] = new SoOrthoSlice;
            d->maskSlice[i]->setName("MaskSlice");
            d->maskSlice[i]->interpolation = SoOrthoSlice::Interpolation::NEAREST;
            d->maskSlice[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->maskSeparator[i]->addChild(d->maskTrans[i]);            
            d->maskSeparator[i]->addChild(d->maskGroup);            
            d->maskSeparator[i]->addChild(d->maskSlice[i]);            
        }        
        d->maskSlice[0]->axis = SoOrthoSlice::Axis::Z;
        d->maskSlice[1]->axis = SoOrthoSlice::Axis::X;
        d->maskSlice[2]->axis = SoOrthoSlice::Axis::Y;
    }
    auto SceneManagerWidget::InitModifyDataGroup() -> void {
        d->modifyMatl = new SoMaterial;
        d->modifyMatl->ambientColor.setValue(1, 1, 1);
        d->modifyMatl->diffuseColor.setValue(1, 1, 1);
        d->modifyMatl->transparency = 0.15f;
        d->modifyRange = new SoDataRange;
        d->modifyRange->min = -0.1;
        d->modifyRange->max = 1.0;
        d->modifySocket = new SoSwitch;
        d->modifySocket->whichChild = -1;

        d->modifyVol = new SoVolumeData;
        d->modifySocket->addChild(d->modifyVol);
        //change reader of this volume

        d->modifyTF = new SoTransferFunction;
        d->modifyTF->predefColorMap = SoTransferFunction::NONE;
        d->modifyTF->colorMap.setNum(256 * 4);
        float* p = d->modifyTF->colorMap.startEditing();
        for (int i = 0; i < 256; ++i) {
            if (i > 128) {
                *p++ = 1.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 1.0;
            }
            else {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
        }
        d->modifyTF->colorMap.finishEditing();

        d->modifyGroup = new SoGroup;
        d->modifyGroup->addChild(d->modifyMatl);
        d->modifyGroup->addChild(d->modifyRange);
        d->modifyGroup->addChild(d->modifyTF);
        d->modifyGroup->addChild(d->modifySocket);

        for(auto i=0;i<3;i++) {
            d->modifySeparator[i] = new SoSeparator;
            d->modifySlice[i] = new SoOrthoSlice;
            d->modifySlice[i]->setName("ModifySlice");
            d->modifySlice[i]->interpolation = SoOrthoSlice::NEAREST;
            d->modifySlice[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->modifySeparator[i]->addChild(d->modifyGroup);
            d->modifySeparator[i]->addChild(d->modifySlice[i]);
        }
        d->modifySlice[0]->axis = SoOrthoSlice::Z;
        d->modifySlice[1]->axis = SoOrthoSlice::X;
        d->modifySlice[2]->axis = SoOrthoSlice::Y;
    }
    auto SceneManagerWidget::InitHTDataGroup() -> void {
        d->htMatl = new SoMaterial;
        d->htMatl->ambientColor.setValue(1, 1, 1);
        d->htMatl->diffuseColor.setValue(1, 1, 1);
        d->htMaxInfo = new SoInfo;
        d->htMaxInfo->setName("HTMax");
        d->htMinInfo = new SoInfo;
        d->htMinInfo->setName("HTMin");
        d->infoGroup = new SoGroup;
        d->htRange = new SoDataRange;
        d->htRange->setName("HTSliceRange");
        d->htSocket = new SoSwitch;        
        d->htSocket->whichChild = -1;
        d->tfImage = new SoTransferFunction;
        d->tfImage->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
        d->tfImage->setName("colormapHTSlice");
        d->htGroup = new SoGroup;
        d->infoGroup->addChild(d->htMaxInfo);
        d->infoGroup->addChild(d->htMinInfo);        
        d->htGroup->addChild(d->htMatl);
        d->htGroup->addChild(d->htRange);
        d->htGroup->addChild(d->tfImage);
        d->htGroup->addChild(d->htSocket);
        for (auto i = 0; i < 3; i++) {
            d->htSeparator[i] = new SoSeparator;            
            d->htSlice[i] = new SoOrthoSlice;
            d->htSlice[i]->setName("ImageSlice");
            d->htSeparator[i]->addChild(d->infoGroup);
            d->htSeparator[i]->addChild(d->htGroup);
            d->htSeparator[i]->addChild(d->htSlice[i]);
        }
        d->htSlice[0]->axis = SoOrthoSlice::Axis::Z;
        d->htSlice[1]->axis = SoOrthoSlice::Axis::X;
        d->htSlice[2]->axis = SoOrthoSlice::Axis::Y;
    }
    auto SceneManagerWidget::InitFLDataGroup() -> void {
        MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
        SbColor rgb[3] = { SbColor(0,0,1),SbColor(0,1,0),SbColor(1,0,0) };
        SbColor srgb[3] = { SbColor(0,0.8,1),SbColor(0.8,1,0),SbColor(1,0,0.8) };
        d->flMatl = new SoMaterial;
        d->flMatl->diffuseColor.setValue(1, 1, 1);
        d->flMatl->ambientColor.setValue(1, 1, 1);
        d->flMatl->transparency.setValue(0.3);
        for (auto i = 0; i < 3; i++) {
            d->flFragmentShader[i] = new SoFragmentShader;
            QString currentPath = QCoreApplication::applicationDirPath();
            QString fragmentShaderFilePath = currentPath + "/shader/FL2D_color_frag.glsl";
            d->flFragmentShader[i]->sourceProgram.setValue(fragmentShaderFilePath.toStdString());
            d->flFragmentShader[i]->addShaderParameter<SoShaderParameter1i>("data1", 1);
            d->flFragmentShader[i]->addShaderParameter1i("isCh0Exist", 0);
            d->flFragmentShader[i]->addShaderParameter<SoShaderParameter1i>("data2", 2);
            d->flFragmentShader[i]->addShaderParameter1i("isCh1Exist", 0);
            d->flFragmentShader[i]->addShaderParameter<SoShaderParameter1i>("data3", 3);
            d->flFragmentShader[i]->addShaderParameter1i("isCh2Exist", 0);
            d->flShader[i] = new SoVolumeShader;
            d->flShader[i]->forVolumeOnly = FALSE;
            d->flShader[i]->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->flFragmentShader[i]);
        }

        d->flMDS = new SoMultiDataSeparator;        

        d->flInhe = new SoSwitch;
        d->flInhe->whichChild = -2;
        for(auto i=0;i<3;i++) {
            d->flTF[i] = makeColorMap(rgb[i], srgb[i], i);            
            d->flTF[i]->transferFunctionId = i + 1;
            d->flRange[i] = new SoDataRange;
            d->flRange[i]->dataRangeId = i + 1;
            d->flRange[i]->min = 0;
            d->flRange[i]->max = 200;
            d->flSokcet[i] = new SoSwitch;            
            d->flSokcet[i]->whichChild = -1;

            d->flMDS->addChild(d->flSokcet[i]);
            d->flMDS->addChild(d->flTF[i]);
            d->flMDS->addChild(d->flRange[i]);

            d->flSeparator[i] = new SoSeparator;            
            d->flSlice[i] = new SoOrthoSlice;
            d->flSlice[i]->axis = ax[i];
            //d->flSlice[i]->sliceNumber = 0;
            //d->flSlice[i]->interpolation = SoSlice::MULTISAMPLE_12;
            d->flSlice[i]->interpolation = SoOrthoSlice::NEAREST;
            d->flSlice[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;            
            d->flTrans[i] = new SoTranslation;

            d->flSeparator[i]->addChild(d->flTrans[i]);
            d->flSeparator[i]->addChild(d->flMatl);
            d->flIndi[i] = new SoSwitch;
            d->flIndi[i]->whichChild = i;
            for(auto j=0;j<i;j++) {
                d->flIndi[i]->addChild(new SoSeparator);
            }

            d->flSliceGroup[i] = new SoGroup;
            d->flSeparator[i]->addChild(d->flIndi[i]);
            d->flIndi[i]->addChild(d->flMDS);
            //d->flInhe->addChild(d->flSlice[i]);
            d->flInhe->addChild(d->flSliceGroup[i]);
            d->flSliceGroup[i]->addChild(d->flShader[i]);
            d->flSliceGroup[i]->addChild(d->flSlice[i]);
        }
        //d->flTrans[0]->translation.setValue(0,0,-0.1);
        //d->flMDS->addChild(d->flShader);
        d->flMDS->addChild(d->flInhe);
    }
    auto SceneManagerWidget::SetMaskOpacity(float opacity) -> void {
        d->maskOpacity = opacity;
        d->maskMatl->transparency.setValue(1.0 - opacity);
    }
    auto SceneManagerWidget::SetMaskToggle(bool ch1Viz, bool ch2Viz, bool ch3Viz) -> void {
        if(false == (ch1Viz || ch2Viz || ch3Viz)) {
            d->maskMatl->transparency.setValue(1.0);
        }else {
            d->maskMatl->transparency.setValue(1.0-d->maskOpacity);
        }
    }
    auto SceneManagerWidget::SetFLOpacity(int ch, float opacity) -> void {
        modifyColorMapOpacity(ch,opacity);
        d->flOpacity[ch] = opacity;
    }
    auto SceneManagerWidget::SetFLToggle(int ch, bool viz) -> void {
        if (viz) {
            for (auto i = 0; i < 3; i++) {                
                d->flFragmentShader[i]->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 1);
            }
            modifyColorMapOpacity(ch,d->flOpacity[ch], 1.0);
            d->flSokcet[ch]->whichChild = 0;            
        }else {
            for (auto i = 0; i < 3; i++) {
                d->flFragmentShader[i]->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 0);
            }
            modifyColorMapOpacity(ch,d->flOpacity[ch], 0.0);
            d->flSokcet[ch]->whichChild = -1;            
        }                
    }
    auto SceneManagerWidget::convertFL(SoVolumeData* oriHT,SoVolumeData* oriFL, float offset) -> SoVolumeData* {
        Q_UNUSED(offset)
        auto convFL = new SoVolumeData;
        convFL->ref();

        auto dim = oriHT->getDimension();
        std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
        auto fl_dim = oriFL->getDimension();
        SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(fl_dim[0] - 1, fl_dim[1] - 1, fl_dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            oriFL->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        oriFL->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned short* fl_pointer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

        for (auto k = 0; k < dim[2]; k++) {
            for (auto j = 0; j < dim[1]; j++) {
                for (auto i = 0; i < dim[0]; i++) {
                    SbVec3f index;
                    index[0] = i;
                    index[1] = j;
                    index[2] = k;
                    auto key = k * dim[0] * dim[1] + j * dim[0] + i;
                    auto phys = oriHT->voxelToXYZ(index);
                    //phys[2] -= offset;
                    auto voxel = oriFL->XYZToVoxel(phys);
                    int vv[3] = { static_cast<int>(voxel[0]),static_cast<int>(voxel[1]),static_cast<int>(voxel[2]) };
                    if (vv[0] >= 0 && vv[0] < fl_dim[0]
                        && vv[1] >= 0 && vv[1] < fl_dim[1]
                        && vv[2] >= 0 && vv[2] < fl_dim[2]) {
                        int vKey = vv[2] * fl_dim[0] * fl_dim[1] + vv[1] * fl_dim[0] + vv[0];
                        auto val = *(fl_pointer + vKey);
                        *(volume.get() + key) = val;
                    }
                }
            }
        }
        dataBufferObj->unmap();
        SbDataType type = SbDataType::UNSIGNED_SHORT;
        convFL->data.setValue(dim, type, 0, (void*)volume.get(), SoSFArray::COPY);
        volume = nullptr;
        convFL->extent.setValue(oriHT->extent.getValue());

        convFL->unrefNoDelete();
        return convFL;
    }
    auto SceneManagerWidget::makeMaskColorMap() -> SoTransferFunction* {
        return nullptr;
    }

    auto SceneManagerWidget::modifyColorMapOpacity(int id,float opacity, float toggle) -> void {
        auto ds = GetSceneManagerDS();        
        float* p = d->flTF[id]->colorMap.startEditing();
        SbColor rgb[3] = { SbColor(0,0,1),SbColor(0,1,0),SbColor(1,0,0) };
        SbColor srgb[3] = { SbColor(1,1,0),SbColor(1,0,1),SbColor(0,1,1) };        
        float r = rgb[id][0]; float g = rgb[id][1]; float b = rgb[id][2];
        float rr = srgb[id][0]; float gg = srgb[id][1]; float bb = srgb[id][2];
        for(int i=0;i<256;i++) {
            auto curRatio = static_cast<float>(i) / 255.0f;
            if(curRatio > d->chUpperRatio[id]) {
                *p++ = rr * toggle;
                *p++ = gg * toggle;
                *p++ = bb * toggle;
                *p++ = opacity * toggle;
            }
            else if (curRatio >= d->chLowerRatio[id]) {
                *p++ = r * toggle;
                *p++ = g * toggle;
                *p++ = b * toggle;
                *p++ = opacity * toggle;
            }else {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
        }
        d->flTF[id]->colorMap.finishEditing();
    }

    auto SceneManagerWidget::modifyColorMap(SbColor color, SbColor subColor, int id, int lower, int upper) -> void {        
        float R = color[0];
        float G = color[1];
        float B = color[2];

        float RR = subColor[0];
        float GG = subColor[1];
        float BB = subColor[2];
        
        float* p = d->flTF[id]->colorMap.startEditing();

        auto chmax = d->chMinMax[id][1];
        auto chmin = d->chMinMax[id][0];

        //auto ratio = (static_cast<float>(upper)-static_cast<float>(lower)) / (chmax - static_cast<float>(lower));
        auto ratio = (static_cast<float>(upper)-chmin) / (chmax - chmin);        
        d->chUpperRatio[id] = ratio;
        auto minratio = (static_cast<float>(lower)-chmin) / (chmax - chmin);
        d->chLowerRatio[id] = minratio;

        for (int i = 0; i < 256; ++i) {
            auto cur_ratio = static_cast<float>(i) / 255;
            if (cur_ratio > ratio) {
                *p++ = RR;
                *p++ = GG;
                *p++ = BB;
                *p++ = d->flOpacity[id];
            }else if (cur_ratio >= minratio) {
                *p++ = R;
                *p++ = G;
                *p++ = B;
                *p++ = d->flOpacity[id];
            }else {
                *p++ = 0;
                *p++ = 0;
                *p++ = 0;
                *p++ = 0;// skip opacity
            }
            
        }
        d->flTF[id]->colorMap.finishEditing();
    }
    
    auto SceneManagerWidget::makeColorMap(SbColor color,SbColor subColor, int id) -> SoTransferFunction* {
        SoTransferFunction* pTF = new SoTransferFunction();
        pTF->transferFunctionId = id + 1;

        // Color map will contain 256 RGBA values -> 1024 float values.
        // setNum pre-allocates memory so we can edit the field directly.
        pTF->colorMap.setNum(256 * 4);

        float R = color[0];
        float G = color[1];
        float B = color[2];        

        float RR = subColor[0];
        float GG = subColor[1];
        float BB = subColor[2];

        // Get an edit pointer, assign values, then finish editing.
        float* p = pTF->colorMap.startEditing();
        for (int i = 0; i < 256; ++i) {
            if(i>128){
                *p++ = RR;
                *p++ = GG;
                *p++ = BB;
                *p++ = 0.7;                
            }else {
                *p++ = R;
                *p++ = G;
                *p++ = B;
                *p++ = 0.7;
            }
        }
        pTF->colorMap.finishEditing();
        return pTF;
    }
        
    auto SceneManagerWidget::ClearSceneGraph() -> void {
        
    }
}
