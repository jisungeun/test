#include <QIcon>
#include <QMouseEvent>

#include "ui_FloatingControl.h"
#include "FloatingControl.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct FloatingControl::Impl {
        Ui::FControl* ui{ nullptr };
        QPoint startPos{ QPoint() };
    };
    FloatingControl::FloatingControl(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::FControl;
        d->ui->setupUi(this);

        Init();
        InitIcons();
        installEventFilter(this);
    }
    FloatingControl::~FloatingControl() {
        
    }
    void FloatingControl::OnOkBtn() {
        emit sigOK();
    }
    void FloatingControl::OnDownBtn() {
        emit sigDown();
    }
    void FloatingControl::OnUpBtn() {
        emit sigUp();
    }    
    bool FloatingControl::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QMouseEvent::MouseButtonPress) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::MiddleButton) {
                d->startPos = mouseEvent->pos();
                return true;
            }
        }
        else if (event->type() == QMouseEvent::MouseMove) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (false == d->startPos.isNull()) {
                this->move(pos() + mouseEvent->pos() - d->startPos);
                return true;
            }
        }
        else if (event->type() == QMouseEvent::MouseButtonRelease) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::MiddleButton) {
                d->startPos = QPoint();
                return true;
            }
        }

        return QObject::eventFilter(watched, event);
    }
    auto FloatingControl::Init()->void {
        connect(d->ui->UpBtn, SIGNAL(clicked()), this, SLOT(OnUpBtn()));
        connect(d->ui->DownBtn, SIGNAL(clicked()), this, SLOT(OnDownBtn()));
        connect(d->ui->OkBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
        setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
        setAttribute(Qt::WA_TranslucentBackground);
        d->ui->UpBtn->setStyleSheet("background-color: #334449;");
        d->ui->OkBtn->setStyleSheet("background-color: #334449;");        
        d->ui->DownBtn->setStyleSheet("background-color: #334449;");
    }
    auto FloatingControl::InitIcons()->void {
        d->ui->UpBtn->setIcon(QIcon(":/image/images/Up.png"));
        d->ui->UpBtn->setIconSize(QSize(45, 45));
        d->ui->DownBtn->setIcon(QIcon(":/image/images/Down.png"));
        d->ui->DownBtn->setIconSize(QSize(45, 45));
        d->ui->OkBtn->setIcon(QIcon(":/image/images/OK.png"));
        d->ui->OkBtn->setIconSize(QSize(45, 45));
    }
}