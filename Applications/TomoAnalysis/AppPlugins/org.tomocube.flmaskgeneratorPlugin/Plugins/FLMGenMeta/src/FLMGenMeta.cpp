#include "FLMGenMeta.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "None";
		d->appProperties["AppKey"] = "FL Mask Generator";		
		QStringList processor_path;
		processor_path.push_back("/processor/basicanalysisfl");
		d->appProperties["Processors"] = processor_path;
		d->appProperties["hasSingleRun"] = false;
		d->appProperties["hasBatchRun"] = true;
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}