#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "FLMGenMetaExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
	class FLMGenMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.flmaskgeneratorMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "FL Mask Generator"; }
		auto GetFullName()const->QString override { return "org.tomocube.flmaskgeneratorPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}