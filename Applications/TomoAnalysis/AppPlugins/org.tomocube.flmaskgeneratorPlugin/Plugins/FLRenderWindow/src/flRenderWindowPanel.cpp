#include <FLRenderWindow2D.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "ui_flRenderWindowPanel.h"
#include "flRenderWindowPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    struct RenderWindowPanel::Impl {
        Ui::flrwForm* ui{ nullptr };

        RenderWindow2D* renderwindow2d[3]{ nullptr,nullptr,nullptr };

        SoSeparator* sceneRoot[3]{ nullptr,nullptr,nullptr };
    };
    RenderWindowPanel::RenderWindowPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::flrwForm();
        d->ui->setupUi(this);

        Init();
    }
    RenderWindowPanel::~RenderWindowPanel() {
        
    }
    auto RenderWindowPanel::Update() -> bool {
        auto ds = GetRenderWindowDS();

        auto cam1 = d->renderwindow2d[0]->getCamera();
        auto cam2 = d->renderwindow2d[1]->getCamera();
        auto cam3 = d->renderwindow2d[2]->getCamera();
        auto curImage = MedicalHelper::find<SoVolumeData>(d->renderwindow2d[0]->getSceneGraph(),"volData");
        if (curImage) {
            MedicalHelper::orientView(MedicalHelper::AXIAL, cam1, curImage);
            MedicalHelper::orientView(MedicalHelper::SAGITTAL, cam2, curImage);
            MedicalHelper::orientView(MedicalHelper::CORONAL, cam3, curImage);

            //double min, max;
            //curImage->getMinMax(min, max);

            for (auto i = 0; i < 3; i++) {
                d->renderwindow2d[i]->setHTRange(ds->htMin, ds->htMax);
                d->renderwindow2d[i]->reset2DView();
            }
        }
        return true;
    }
    auto RenderWindowPanel::Render() -> bool {        
        for (auto i = 0; i < 3; i++) {
            d->renderwindow2d[i]->immediateRender();
        }
        return true;
    }
    auto RenderWindowPanel::GetRenderWindow(int idx) -> QOivRenderWindow* {
        return d->renderwindow2d[idx];
    }
    auto RenderWindowPanel::SetSceneGraph(SoSeparator* scene, int idx) -> void {
        d->renderwindow2d[idx]->setSceneGraph(scene);
    }
    auto RenderWindowPanel::Init() -> void {
        for(auto i=0;i<3;i++) {            
            d->renderwindow2d[i] = new RenderWindow2D(nullptr);
            auto name = QString("TwoD%1").arg(i);
            d->renderwindow2d[i]->setName(name);
            d->renderwindow2d[i]->setRenderWindowID(i);
            connect(d->renderwindow2d[i], SIGNAL(sigWheel(float)), this, SLOT(onMoveSlice(float)));
            connect(d->renderwindow2d[i], SIGNAL(sig2dCoord(float, float, float)), this, SLOT(onPickSlice(float, float, float)));
            connect(d->renderwindow2d[i], SIGNAL(sigGlobalPos(int, int)), this, SLOT(onGlobalPos(int, int)));
        }
        d->renderwindow2d[0]->setWindowTitle("XY");
        d->renderwindow2d[0]->setWindowTitleColor(0.8f, 0.2f, 0.2f);
        d->renderwindow2d[1]->setWindowTitle("ZY");
        d->renderwindow2d[1]->setWindowTitleColor(0.2f, 0.8f, 0.2f);
        d->renderwindow2d[2]->setWindowTitle("XZ");
        d->renderwindow2d[2]->setWindowTitleColor(0.2f, 0.2f, 0.8f);
        
        auto xyLay = new QVBoxLayout;
        xyLay->setContentsMargins(0, 0, 0, 0);
        xyLay->setSpacing(0);        
        d->ui->xySliceSocket->setLayout(xyLay);
        d->ui->xySliceSocket->layout()->addWidget(d->renderwindow2d[0]);

        auto yzLay = new QVBoxLayout;
        yzLay->setContentsMargins(0, 0, 0, 0);
        yzLay->setSpacing(0);
        d->ui->yzSliceSocket->setLayout(yzLay);
        d->ui->yzSliceSocket->layout()->addWidget(d->renderwindow2d[1]);

        auto xzLay = new QVBoxLayout;
        xzLay->setContentsMargins(0, 0, 0, 0);
        xzLay->setSpacing(0);
        d->ui->xzSliceSocket->setLayout(xzLay);
        d->ui->xzSliceSocket->layout()->addWidget(d->renderwindow2d[2]);       
    }
    void RenderWindowPanel::onPickSlice(float phyX, float phyY, float phyZ) {
        emit slicePickChanged(phyX, phyY, phyZ);
    }
    void RenderWindowPanel::onGlobalPos(int x, int y) {        
        auto window = dynamic_cast<RenderWindow2D*>(sender());        
        auto renWinID = window->getRenderWindowID();
        
        emit globalPositionChanged(x, y, renWinID);
    }
    void RenderWindowPanel::onMoveSlice(float delta) {
        auto eventSender = dynamic_cast<RenderWindow2D*>(sender());
                
        auto orthoSlice = MedicalHelper::find<SoOrthoSlice>(eventSender->getSceneGraph(), "ImageSlice");
        if (orthoSlice == nullptr) {
            return;
        }

        int sliceIndex, viewIndex;
        sliceIndex = viewIndex = -1;

        viewIndex = eventSender->getRenderWindowID();//orthoSlice->axis.getValue();
        sliceIndex = orthoSlice->sliceNumber.getValue();

        if (delta < 0) {
            sliceIndex--;
        }
        else if (delta > 0) {
            sliceIndex++;
        }
        else {
            return;
        }

        emit sliceIndexChanged(viewIndex, sliceIndex);
    }

}
