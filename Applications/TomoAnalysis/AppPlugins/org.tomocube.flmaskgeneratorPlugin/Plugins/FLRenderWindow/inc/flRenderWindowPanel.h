#pragma once

#include <memory>

#include <QWidget>

#include <IFLRenderWindowPanel.h>
#include "FLRenderWindowExport.h"

class QOivRenderWindow;
class SoSeparator;

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLRenderWindow_API RenderWindowPanel : public QWidget, public Interactor::IRenderWindowPanel {
        Q_OBJECT
    public:
        typedef RenderWindowPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        RenderWindowPanel(QWidget* parent = nullptr);
        ~RenderWindowPanel();

        auto Update() -> bool override;

        auto Render() -> bool override;

        //Non-clean architecture
        auto GetRenderWindow(int idx)->QOivRenderWindow*;
        auto SetSceneGraph(SoSeparator* scene, int idx)->void;

    signals:
        void sliceIndexChanged(int viewIndex, int sliceIndex);
        void slicePickChanged(float, float, float);
        void globalPositionChanged(int, int,int);

    protected slots:
        void onMoveSlice(float);
        void onPickSlice(float, float, float);
        void onGlobalPos(int, int);

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}