﻿#include <iostream>
#include <fstream>

#include <QTableWidgetItem>
#include <QAbstractItemView>
#include <QFileDialog>
#include <QCloseEvent>
#include <QGridLayout>

#include <ScalarData.h>
#include <TCMeasure.h>

#include "ui_FLResultPanel.h"
#include "FLResultPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
#define SAFE_DELETE( p ) { if( p ) { delete ( p ); ( p ) = NULL; } }
	float color_table[20][3]{
		230,25,75, //red
		60,180,75, //green
		255,255,25, //yellow
		0,130,200, //blue
		245,130,48, //Orange
		145,30,180, //Purple
		70,240,240, //Cyan
		240,50,230, //Magenta
		210,245,60, //Lime
		250,190,212, //Pink
		0,128,128, //Teal
		220,190,255, //Lavender
		170,110,40, //Brown
		255,250,200, //Beige
		128,0,0,//Maroon
		170,255,195, //Mint
		128,128,0, //Olive
		255,215,180, //Apricot
		0,0,128, //Navy
		128,128,128 //Gray
	};
	//https://sashamaps.net/docs/tools/20-colors/
	QString measureNames[7] = {
		".Volume",
		".SurfaceArea",
		".MeanRI",
		".Sphericity",
		".ProjectedArea",
		".Drymass",
		".Concentration"
	};
	struct ResultPanel::Impl {
		Ui::FLResultPanel* ui{ nullptr };

		int totalLabelCnt{ 0 };
		QList<int> labelCnt;
		int layerCnt{ 0 };
		QStringList TableHeader;
		QMap<QString,QString> convertingTable;
		QMap<int,int> layer_order;
		int writeLayerCnt{ 0 };
		QList<int> writeLabelCnt;

		QList<DataList::Pointer> result;

		auto MeasureToDataList(TCMeasure::Pointer measure)->DataList::Pointer {
			auto dl = std::make_shared<DataList>();
			auto organ_names = measure->GetOrganNames();//organ 종류
			auto cellcount = measure->GetCellCount();//cell instance 숫자
			auto keys = measure->GetKeys();//measure 종류
			for(auto i=0;i<organ_names.size();i++) {
				//auto ds = std::make_shared<DataSet>();
				DataSet::Pointer ds = DataSet::New();
				auto organ_name = ScalarData::New(organ_names[i]);
				ds->AppendData(organ_name, "Name");

				auto labelNum = ScalarData::New(cellcount);				
				ds->AppendData(labelNum, "CellCount");
			    for(auto j=0;j<cellcount;j++) {				
			        for(auto k=0;k<keys.size();k++) {
						auto key = keys[k];
						auto final_key = organ_names[i] + "." + key;
						auto value = measure->GetMeasure(final_key, j);

						auto scalar = ScalarData::New(value);
						scalar->setLabelIdx(j);						
						ds->AppendData(scalar, "cell" + QString::number(j) + "." + key);
			        }
			    }
				dl->Append(ds);
			}
			return dl;
		}
	};
	ResultPanel::ResultPanel(QWidget* parent) : d{ new Impl }, QWidget(parent) {
		d->ui = new Ui::FLResultPanel;
		d->ui->setupUi(this);

	    setStyleSheet("QTableWidget {background: #172023;}"
					  "QHeaderView::section {background: #2d3e43;}"
					  "QHeaderView {background: #172023;}"
					  "QTableCornerButton::section {background: #2d3e43;}");
		setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Window);
		InitPanel();

		d->convertingTable["lipid droplet"] = "Lipid droplet";
		d->convertingTable["membrane"] = "Whole Cell";
		d->convertingTable["nucleoli"] = "Nucleolus";
		d->convertingTable["nucleus"] = "Nucleus";

		//hide time controlls
		d->ui->label->hide();
		d->ui->timeStepSpin->hide();
	}
	ResultPanel::~ResultPanel() {
		delete d->ui;
	}
	auto ResultPanel::Update() -> bool {
		auto ds = GetResultDS();
		QList<DataList::Pointer> results;		
		auto m = ds->measure;
		if(nullptr == m ) {
			hide();
			return false;
		}
	    auto list = d->MeasureToDataList(m);
		//auto list = m;
		results.push_back(list);
		setResults(results);
		clearResult();
		updateTable(list);
		updateResult(list);

		if (list.get()->Count() < 1) {
			hide();
			return false;
		}

		show();

	    return true;
    }
	auto ResultPanel::InitPanel() -> void {
		connect(d->ui->csvBtn, SIGNAL(clicked()), this, SLOT(OnSaveCsv()));
		connect(d->ui->timeStepSpin, SIGNAL(valueChanged(int)), this, SLOT(OnTimeStepChanged(int)));
		d->ui->timeStepSpin->setEnabled(false);
		QString volume("Volume(um^3)");
		volume.replace("^3", QString::fromWCharArray(L"\x00B3"));
		volume.replace("(u", QString("(") + QString::fromWCharArray(L"\x00B5"));
		QString surface_area("Surface area(um^2)");
		surface_area.replace("^2", QString::fromWCharArray(L"\x00B2"));
		surface_area.replace("(u", QString("(") + QString::fromWCharArray(L"\x00B5"));
		QString projected_area("Projected area(um^2)");
		projected_area.replace("^2", QString::fromWCharArray(L"\x00B2"));
		projected_area.replace("(u", QString("(") + QString::fromWCharArray(L"\x00B5"));
		QString concent("Concentration(pg/um^3)");
		concent.replace("^3", QString::fromWCharArray(L"\x00B3"));
		concent.replace("/u", QString("/") + QString::fromWCharArray(L"\x00B5"));
		d->TableHeader <<"Organelle" << "Cell#" << volume << surface_area << "Mean RI" << "Sphericity" << projected_area << "Dry mass(pg)" << concent;
	}

	auto ResultPanel::setResults(QList<DataList::Pointer> dls) -> void {
		d->result = dls;
		auto time_steps = d->result.size();
		d->ui->timeStepSpin->setRange(0, time_steps - 1);
		d->ui->timeStepSpin->setEnabled(true);
	}
	void ResultPanel::OnTimeStepChanged(int step) {
		if (d->result.size() > step) {
			clearResult();
			updateTable(d->result[step]);
			updateResult(d->result[step]);
		}
	}
	void ResultPanel::closeEvent(QCloseEvent* event) {
        if(event->spontaneous()) {
			emit sigCloseResult();
        }else {
			QWidget::closeEvent(event);
        }
    }

	void ResultPanel::OnSaveCsv() {
		auto path = qApp->applicationDirPath();
		QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save File"),
			path,
			tr("Excel csv file (*.csv)"));

		if(fileName.isEmpty()) {
			return;
		}
		std::ofstream new_csv;		
		new_csv.open(fileName.toStdWString());
		new_csv << "Organelle,Cell#,Volume(um^3),Surface area(um^2),Mean RI,Sphericity,Projected area(um^2),Dry mass(pg),Concentration(pg/um^3)";
		new_csv << "\n";

		auto prevOrg = QString();
		for (auto j = 0; j < d->ui->tableWidget->rowCount(); j++) {
			auto orgItem = d->ui->tableWidget->item(j, 0);
			if (orgItem) {
				auto orgName = orgItem->text();
				prevOrg = orgName;
			}
			new_csv << prevOrg.toStdString() << ",";

			auto cellText = d->ui->tableWidget->item(j, 1)->text();
			auto numText = cellText.split("cell")[1];
			new_csv << numText.toStdString() << ",";

			for (auto i = 2; i < d->ui->tableWidget->columnCount(); i++) {
				auto val = d->ui->tableWidget->item(j, i)->text().toFloat();
				if (i == 4) {
					new_csv << QString::number(val, 'f', 4).toStdString();
				}else{					
					new_csv << QString::number(val, 'f', 2).toStdString();
				}
				if (i < d->ui->tableWidget->columnCount() - 1) {
					new_csv << ",";
				}				
			}
			new_csv << "\n";
		}		
		new_csv.close();
	}

	auto ResultPanel::clearResult() -> void {
		d->ui->tableWidget->clear();
		d->ui->tableWidget->setColumnCount(0);
		d->labelCnt.clear();
		d->layerCnt = 0;
		d->writeLabelCnt.clear();
		d->writeLayerCnt = 0;		
		//d->result.clear();
	}
	auto ResultPanel::nameConverter(QString measurekey) -> QString {
		auto conv = d->convertingTable[measurekey];
		if(conv.isEmpty()) {
		    return measurekey;
		}
		return conv;
    }

	auto ResultPanel::updateTable(DataList::Pointer dl)->void {
		d->layer_order.clear();
		d->layerCnt = dl->Count();
		if (d->layerCnt > 0) {
			//int total_cnt = 0;
			d->totalLabelCnt = 0;
			for (int i = 0; i < d->layerCnt; i++) {
				auto cnt = dl->GetData(i)->Count();				
				auto layer_name = std::dynamic_pointer_cast<ScalarData>(dl->GetData(i)->GetData(0))->ValueAsString();				
				if(layer_name.compare("membrane")==0) {
				    d->layer_order[i] = 0;
				}else if(layer_name.compare("lipid droplet")==0) {
				    d->layer_order[i] = 3;
				}else if(layer_name.compare("nucleus")==0) {
				    d->layer_order[i] =1;
				}else if(layer_name.compare("nucleoli")==0) {
				    d->layer_order[i] = 2;
				}
				auto tmpLabelCnt = (cnt - 2) / 7;
				d->totalLabelCnt += tmpLabelCnt;
				d->labelCnt.push_back(tmpLabelCnt);
			}
			//build table row column
			d->ui->tableWidget->setColumnCount(9);
			d->ui->tableWidget->setRowCount(d->totalLabelCnt);
			d->ui->tableWidget->setHorizontalHeaderLabels(d->TableHeader);
			d->ui->tableWidget->verticalHeader()->setVisible(false);
			d->ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
			d->ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
			d->ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
			d->ui->tableWidget->setShowGrid(true);
		}
	}

	auto ResultPanel::updateResult(DataList::Pointer dl) -> void {
		int layer_spacing = 0;
		for (int i = 0; i < d->layerCnt; i++) {
			auto single_layer = dl->GetData(i);

			auto cur_layer_name = std::dynamic_pointer_cast<ScalarData>(single_layer->GetData(0))->ValueAsString();
			if(d->layer_order.size()>0){
			    layer_spacing = 0;
			    auto actual_idx = d->layer_order[i];
			    for(auto k = 0;k<d->layerCnt;k++) {
			        if(d->layer_order[k] != actual_idx && d->layer_order[k] < actual_idx) {
			            layer_spacing+=d->labelCnt[k];
			        }
			    } 
			}
		    d->ui->tableWidget->setItem(layer_spacing, 0, new QTableWidgetItem(nameConverter(cur_layer_name)));
			d->ui->tableWidget->item(layer_spacing,0)->setTextAlignment(Qt::AlignCenter);

			for (int j = 0; j < d->labelCnt[i]; j++) {
				QString curLabelName("cell");
				auto name_idx = 2 + 7 * j;// (single_layer->Count() - 1)
				auto single_label = std::dynamic_pointer_cast<ScalarData>(single_layer->GetData(name_idx));
				curLabelName = curLabelName + QString::number(single_label->getLabelIdx()+1);
				d->ui->tableWidget->setItem(layer_spacing + j, 1, new QTableWidgetItem(curLabelName));
				auto label_idx = single_label->getLabelIdx();
				auto color_idx = label_idx % 20;
				d->ui->tableWidget->item(layer_spacing + j, 1)->setBackground(QBrush(QColor(color_table[color_idx][0], color_table[color_idx][1], color_table[color_idx][2], 128)));
				d->ui->tableWidget->item(layer_spacing + j, 1)->setTextAlignment(Qt::AlignRight);
			}

			for (int j = 2; j < single_layer->Count(); j++) {
				auto cur_measure = (j - 2) % 7;
				auto single_label = std::dynamic_pointer_cast<ScalarData>(single_layer->GetData(j));
				QString curLabelName("cell");				
				curLabelName = curLabelName + QString::number(single_label->getLabelIdx());
				QString curMeasureName = curLabelName + measureNames[cur_measure];

				auto value = single_label->ValueAsDouble();				
				auto cur_label = (j - 2) / 7;
				if(value < 0.0){
					d->ui->tableWidget->setItem(layer_spacing + cur_label, 2 + cur_measure, new QTableWidgetItem(QString("N/A")));
				}else{
					auto col_idx = 2+ cur_measure;
					QString prec_str;
					if(col_idx == 4) {
					    prec_str = QString::number(value,'f',4); 
					}else {
					    prec_str = QString::number(value,'f',2);
					}
				    d->ui->tableWidget->setItem(layer_spacing + cur_label, col_idx, new QTableWidgetItem(prec_str));
				}				
				auto label_idx = single_label->getLabelIdx();
				auto color_idx = label_idx % 20;
				d->ui->tableWidget->item(layer_spacing + cur_label, 2 + cur_measure)->setBackground(QBrush(QColor(color_table[color_idx][0], color_table[color_idx][1], color_table[color_idx][2], 128)));
				d->ui->tableWidget->item(layer_spacing + cur_label, 2 + cur_measure)->setTextAlignment(Qt::AlignRight);
			}
			if(d->layer_order.size()<1){
			    layer_spacing += d->labelCnt[i];
			}
		}
		d->ui->tableWidget->resizeColumnsToContents();
	}
}