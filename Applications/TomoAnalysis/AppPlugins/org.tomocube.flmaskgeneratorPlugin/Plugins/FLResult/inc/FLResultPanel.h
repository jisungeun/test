#pragma once

#include <memory> 

#include <QWidget>
#include <QLayout>

#include <DataList.h>

#include <IFLResultPanel.h>

#include "FLResultPanelExport.h"

namespace TomoAnalysis::FLMaskGenerator::Plugins {
    class FLResultPanel_API ResultPanel : public QWidget,public Interactor::IResultPanel {
        Q_OBJECT
    public:
        typedef ResultPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ResultPanel(QWidget* parent = nullptr);
        ~ResultPanel();

        auto Update() -> bool override;
        auto InitPanel(void)->void;
        auto updateTable(DataList::Pointer dl)->void;
        auto updateResult(DataList::Pointer dl)->void;
        auto setResults(QList<DataList::Pointer> dls)->void;
        auto clearResult(void)->void;

        
    signals:
        //void sigSaveCsv(void);
        void sigCloseResult(void);

    protected slots:
        void OnSaveCsv(void);
        void OnTimeStepChanged(int);

    protected:
        void closeEvent(QCloseEvent* event) override;

    private:
        auto nameConverter(QString measurekey)->QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}