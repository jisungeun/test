#include <iostream>

//Qt
#include <QMessageBox>
#include <QDir>
#include <QJsonArray>
#include <QProgressDialog>
#include <QMessageBox>

//Entity
#include <IParameter.h>
#include <FLMGWorkingSet.h>

//Controller
#include <FLSceneController.h>
#include <FLMaskController.h>

//Presenter
#include <FLPanelPresenter.h>
#include <FLScenePresenter.h>

//Plugins
#include <flmgImageDataReader.h>
#include <flmgMaskDataReader.h>
#include <flmgMaskDataWriter.h>
#include <flmgMeasureDataReader.h>
#include <flmgMeasureDataWriter.h>
#include <flProcessingEngine.h>

//Panels
#include <flDataManagerPanel.h>
#include <flNavigatorPanel.h>
#include <flParameterPanel.h>
#include <flRenderWindowPanel.h>
#include <flSceneManagerWidget.h>
#include <FLResultPanel.h>

//Component Widget
#include <ScreenShotWidget.h>

//Externals
#include <FileValidInfo.h>
#include <MenuEvent.h>
#include <AppEvent.h>
#include <TCFMetaReader.h>
#include <TCParameterReader.h>
#include <TCParameterWriter.h>

//OIV
#include <ImageViz/SoImageViz.h>
#include <Inventor/SoDB.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <Inventor/threads/SbThread.h>
#include <Medical/InventorMedical.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"
#include <OivTimer.h>

#include <QCloseEvent>

#include "TCLogger.h"


namespace TomoAnalysis::FLMaskGenerator::AppUI {
    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };
        QVariantMap appProperties;
        bool isBatValid{ false };
        bool isChSelected[3]{ false,false,false };

        //Processing Engine
        Plugins::Processor::Pointer engine{ nullptr };
        Plugins::SceneManagerWidget::Pointer sceneManager{ nullptr };
        Plugins::ResultPanel::Pointer result{ nullptr };

        //Workingset
        Entity::WorkingSet::Pointer workingset{ nullptr };

        //External widgets
        TC::ScreenShotWidget::Pointer screenshot{nullptr};
        FileValidInfo* fileValids{ nullptr };

        //Batch Run Parameters
        QString cur_playgroundpath;
        QString cur_hyperName;
        QString cur_procName;

        QStringList cur_cubes;
        QStringList cur_tcfs;

        QStringList cur_ignore_tcf_cube;
        QStringList cur_ignore_tcfs;
        QList<int> cur_ignore_tcf_type;

        QProgressDialog* dialog{ nullptr };
        QMessageBox* waitMsg{ nullptr };
        MainWindow* thisPointer{ nullptr };
        SbThread* thread{nullptr};
        QString oTimeStamp;
        bool cancelEmit{ false };
        int cur_image_step;
        int cur_batch_step;
        QString cur_algo_name;
        QList<int> cur_cubeCounts;

        QList<bool> visited;

        QMap<QString, IParameter::Pointer> share_param;
    };
    MainWindow::MainWindow(QWidget* parent) : IMainWindowTA("FL Mask Generator","FL Mask Generator",parent), d{new Impl} {
        //d->appProperties["Parent-App"] = "Project Manager";
        //for ETH
        d->appProperties["Parent-App"] = "None";

        d->appProperties["AppKey"] = "FL Mask Generator";
        //TODO create & connect processor
        QStringList processor_path;
        processor_path.push_back("/processor/basicanalysisfl");
        d->appProperties["Processors"] = processor_path;
        d->appProperties["hasSingleRun"] = false;
        d->appProperties["hasBatchRun"] = true;
        d->thisPointer = this;
    }
    MainWindow::~MainWindow() {
        if(d->fileValids) {
            delete d->fileValids;
        }        
    }
    auto MainWindow::ForceClosePopup() -> void {
        if(nullptr != d->result) {
            d->result->close();
        }
        if(nullptr != d->sceneManager) {
            d->sceneManager->CloseController();
        }
    }

    auto MainWindow::GetFeatureName() const -> QString {
	    return "org.tomocube.flmaskgeneratorPlugin";
    }

    auto MainWindow::OnAccepted() -> void {
    }

    auto MainWindow::OnRejected() -> void {
    }

    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        return std::make_tuple(true, true);
    }
    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }
        if (params["ExecutionType"].toString() == "BatchRun") {
            return this->ExecuteBatchRun(params);
        }
        return true;
    }
    auto MainWindow::ExecuteBatchRun(const QVariantMap& params)->bool {
        if (false == d->isBatValid) {
            OnRejectValidity();
            return true;
        }
        d->cur_playgroundpath = params["Playground path"].toString();
        d->cur_hyperName = params["Hypercube name"].toString();
        d->cur_procName = params["Processor path"].toString();

        d->cur_cubes.clear();
        d->cur_tcfs.clear();
        d->cur_ignore_tcfs.clear();
        d->cur_ignore_tcf_cube.clear();
        d->cur_ignore_tcf_type.clear();
        d->cur_cubeCounts.clear();

        auto metaReader = new TC::IO::TCFMetaReader;

        int idx = 1;
        while (1) {
            auto cn = "Cube" + QString::number(idx);
            auto cube = params[cn].toStringList();
            auto real_cube_cnt = 0;
            if (cube.isEmpty()) {
                break;
            }
            auto cubeContents = cube;
            auto cubeName = cubeContents[0];
            d->cur_cubes.push_back(cubeName);
            for (auto i = 1; i < cubeContents.count(); i++) {
                auto tcfPath = cubeContents[i];
                auto meta = metaReader->Read(tcfPath);

                if (false == meta->data.data3DFL.exist) {
                    d->cur_ignore_tcfs.append(tcfPath);
                    d->cur_ignore_tcf_cube.append(cubeName);
                    d->cur_ignore_tcf_type.append(0);
                }
                else {
                    auto chValid = false;
                    for (auto i = 0; i < 3; i++) {
                        chValid |= d->isChSelected[i] && meta->data.data3DFL.valid[i];
                    }
                    if (false == chValid) {
                        d->cur_ignore_tcfs.append(tcfPath);
                        d->cur_ignore_tcf_cube.append(cubeName);
                        d->cur_ignore_tcf_type.append(0);
                    }
                    else {
                        d->cur_tcfs.append(tcfPath);
                        real_cube_cnt++;
                    }
                }
            }
            idx++;
            d->cur_cubeCounts.push_back(real_cube_cnt);
        }

        delete metaReader;

        if (d->cur_ignore_tcfs.count() > 0) {
            d->fileValids->SetTcfPaths(d->cur_ignore_tcfs);
            d->fileValids->SetTcfCubes(d->cur_ignore_tcf_cube);
            d->fileValids->SetTcfTypes(d->cur_ignore_tcf_type);
            d->fileValids->ComposeTable();
            d->fileValids->showMaximized();
        }
        else {
            OnAcceptValidity();
        }
    }    
    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {        
        d->isBatValid = false;
        d->cur_procName = name;
        d->share_param[d->cur_procName] = param;
        //Init Parameter Panel
        const std::shared_ptr<Interactor::FLPanelPresenter> scenePresenter{ new Interactor::FLPanelPresenter(nullptr,nullptr,d->ui->parameterPanel,d->ui->vizcontrolPanel) };        
        Interactor::FLSceneController controller(scenePresenter.get());

        if (false == controller.InitBatchRunControlUI(param)) {
            QMessageBox::warning(nullptr, "Error", "Select one or more channels");
            return;
        }
        auto workingset = Entity::WorkingSet::GetInstance();            

        auto procSelection = param->GetValue("ProcSelection").toArray();
        for(auto i=0;i<3;i++) {
            auto val = procSelection[i].toBool();            
            d->isChSelected[i] = val;            
            workingset->SetFLSelected(i, val);
        }        
        d->isBatValid = true;        
    }
    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        return nullptr;
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }
    auto MainWindow::TryActivate() -> bool {
        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        //set object name
        d->ui->fileNameLabel->setObjectName("h3");
        d->ui->fileName->setObjectName("h5");

        d->screenshot = std::make_shared<TC::ScreenShotWidget>();
        auto slayout = new QVBoxLayout;
        slayout->setContentsMargins(0, 0, 0, 0);
        slayout->setSpacing(0);

        d->ui->screenshotSocket->setLayout(slayout);
        slayout->addWidget(d->screenshot.get());

        //connect render window with screenshot
        d->screenshot->SetRenderWindow(d->ui->renderwindowPanel->GetRenderWindow(0), "XY");
        d->screenshot->SetRenderWindow(d->ui->renderwindowPanel->GetRenderWindow(1), "YZ");
        d->screenshot->SetRenderWindow(d->ui->renderwindowPanel->GetRenderWindow(2), "XZ");        
        d->screenshot->SetMultiLayerType(TC::MultiLayoutType::FLXY);
        d->screenshot->ToggleTab(1, false);

        d->engine = std::make_shared<Plugins::Processor>();
        d->sceneManager = std::make_shared<Plugins::SceneManagerWidget>();
        d->result = std::make_shared<Plugins::ResultPanel>();

        d->fileValids = new FileValidInfo;        
        d->fileValids->setWindowTitle("File validity");
        d->fileValids->SetInformationText("Following files will be ignored caused by absence of 3D FL image");
        connect(d->fileValids, SIGNAL(sigAccept()), this, SLOT(OnAcceptValidity()));
        connect(d->fileValids, SIGNAL(sigReject()), this, SLOT(OnRejectValidity()));
        connect(d->ui->datamanagerPanel, SIGNAL(sigOpenTcf(int,double)), this, SLOT(DmOpenTcfbyIndex(int,double)));
        connect(d->ui->datamanagerPanel, SIGNAL(sigAllOperation()), this, SLOT(DmPerformAll()));
        connect(d->ui->renderwindowPanel, SIGNAL(sliceIndexChanged(int, int)), this, SLOT(RwSliceIndexChanged(int, int)));
        connect(d->ui->renderwindowPanel, SIGNAL(slicePickChanged(float, float, float)), this, SLOT(RwSlicePickChanged(float, float, float)));
        connect(d->ui->renderwindowPanel, SIGNAL(globalPositionChanged(int, int,int)), this, SLOT(RwGlobalPosChanged(int,int,int)));
        connect(d->ui->navigatorPanel, SIGNAL(positionChanged(int, int, int)), this, SLOT(NvSliceIndexChanged(int,int,int)));        
        connect(d->ui->parameterPanel, SIGNAL(sigFLMinMaxChanged(int, int, int)), this, SLOT(PaFLMinMaxChanged(int, int, int)));
        connect(d->ui->parameterPanel, SIGNAL(sigPerformFLMask(int,int,int)), this, SLOT(PaPerformMask(int,int,int)));
        connect(d->ui->parameterPanel, SIGNAL(sigFLOpacityChanged(int, float)), this, SLOT(PaOpaChanged(int, float)));
        connect(d->ui->parameterPanel, SIGNAL(sigPerformMeasure()), this, SLOT(PaPerformMeasure()));
        connect(d->ui->parameterPanel, SIGNAL(sigToggleFLViz(int, bool)), this, SLOT(PaVizToggled(int, bool)));
        connect(d->ui->parameterPanel, SIGNAL(sigChannelRenamed(int, QString)), this, SLOT(PaNameChanged(int, QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigShowResult(bool)), this, SLOT(PaToggleResult(bool)));
        connect(d->ui->parameterPanel, SIGNAL(sigChRII(int, int)), this, SLOT(PaChRII(int, int)));
        connect(d->ui->parameterPanel, SIGNAL(sigChRIIValue(int, double)), this, SLOT(PaChRIIValue(int, double)));
        connect(d->ui->parameterPanel, SIGNAL(sigCustomRI(bool)), this, SLOT(PaCustomRI(bool)));
        connect(d->ui->parameterPanel, SIGNAL(sigRIValue(double)), this, SLOT(PaRIValue(double)));
        connect(d->ui->parameterPanel, SIGNAL(sigMaskCorrection()), this, SLOT(PaMaskCorrection()));

        connect(d->result.get(), SIGNAL(sigCloseResult()), this, SLOT(RsCloseEvent()));
        connect(d->ui->vizcontrolPanel, SIGNAL(sigFLMaskViz(bool, bool, bool)), this, SLOT(VcFLMaskViz(bool, bool, bool)));
        connect(d->ui->vizcontrolPanel, SIGNAL(sigFLMaskOpa(float)), this, SLOT(VcFLMaskOpa(float)));

        connect(this, SIGNAL(batchStepChanged(int)), this, SLOT(OnBatchStepChanged(int)));
        connect(this, SIGNAL(batchFinished(int)), this, SLOT(OnBatchFinished(int)));
        connect(this, SIGNAL(batchStarted()), this, SLOT(OnBatchStepStarted()));

        //connect SceneGraph
        for (auto i = 0; i < 3; i++) {
            d->ui->renderwindowPanel->SetSceneGraph(d->sceneManager->GetRenderRoot(i),i);
        }

        subscribeEvent("TabChange");
        subscribeEvent(TC::Framework::MenuEvent::Topic());
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnTabFocused(ctkEvent)));

        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    void MainWindow::OnTabFocused(ctkEvent ctkEvent) {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType sudden = MenuType::NONE;
        if (ctkEvent.getProperty("TabName").isValid()) {
            auto TabName = ctkEvent.getProperty("TabName").toString();
            if (TabName.compare("FL Mask Generator") != 0) {
                ForceClosePopup();
            }            
        }else if(ctkEvent.getProperty(sudden._to_string()).isValid()) {
            ForceClosePopup();
        }
    }
    auto MainWindow::IsActivate() -> bool {
        return (nullptr != d->ui);
    }
    void MainWindow::resizeEvent(QResizeEvent* event) {
        
    }

    //SLOTS    
    void MainWindow::OnAcceptValidity() {
        if (d->cur_tcfs.count() < 1) {
            QMessageBox::warning(nullptr, "Error", "No valid tcf file that contains selected FL channel");
            OnRejectValidity();
            return;
        }
        d->fileValids->hide();

        auto workingset = Entity::WorkingSet::GetInstance();
        workingset->ClearData();
        workingset->SetPlayground(d->cur_playgroundpath);
        workingset->SetHypercubeName(d->cur_hyperName);
        workingset->SetImageList(d->cur_tcfs);

        std::shared_ptr<Plugins::ImageDataReader> ireader = std::make_shared<Plugins::ImageDataReader>();
        std::shared_ptr<Plugins::MaskDataReader> mreader = std::make_shared<Plugins::MaskDataReader>();        
        std::shared_ptr<Plugins::MeasureDataReader> rreader = std::make_shared<Plugins::MeasureDataReader>();

        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(d->ui->datamanagerPanel,d->ui->navigatorPanel,d->ui->parameterPanel,d->ui->vizcontrolPanel,d->result.get()) };
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(d->ui->renderwindowPanel,d->sceneManager.get(),d->ui->parameterPanel) };

        Interactor::FLSceneController controller(panelPresenter.get(), scenePresenter.get(),ireader.get(),mreader.get(),rreader.get());
        if (false == controller.OpenDataSet()) {
            QMessageBox::warning(nullptr, "Error", "Failed to load tcf file list");
            OnRejectValidity();
            return;
        }
        d->result->clearResult();
        d->result->hide();
        d->ui->parameterPanel->SetResultToggle(false);
        //open first singleshot volume set
        if(false == controller.ChangeDataIndex(0,0)){
            QMessageBox::warning(nullptr, "Error", "Failed to change current tcf file");
            return;
        }        
        if(nullptr !=  workingset->GetMeasure()) {
            d->result->show();
            d->ui->parameterPanel->SetResultToggle(true);            
        }
        auto chNames = d->share_param[d->cur_procName]->GetValue("DupNames").toArray();        
        for (auto i = 0; i < 3; i++) {
            if (d->isChSelected[i]) {
                //parse threshold parameter & set
                auto chName = QString("Ch %1 intensity Parameter").arg(i + 1);
                auto child = d->share_param[d->cur_procName]->GetChild(chName);
                auto upper = static_cast<int>(child->GetValue("UThreshold").toDouble() * 10000.0);
                auto lower = static_cast<int>(child->GetValue("LThreshold").toDouble() * 10000.0);
                d->ui->parameterPanel->SetULThreshold(i, lower, upper);

                //parse measure paramter & set
                auto meName = QString("Basic Measurement Parameter!Ch %1").arg(i+1);
                auto meChild = d->share_param[d->cur_procName]->GetChild(meName);
                auto customRI = meChild->GetValue("CustomRI").toBool();
                auto preset = meChild->GetValue("Preset").toInt();                
                auto ri = meChild->GetValue("RI").toDouble();
                auto rii = meChild->GetValue("RII").toDouble();
                d->ui->parameterPanel->SetMeasureParam(i, customRI, preset, rii, ri);
                                
                auto val = chNames[i].toString();
                d->ui->parameterPanel->RenameChannel(i, val);
            }
        }
    }
    void MainWindow::OnRejectValidity() {
        d->fileValids->hide();
        //close tab
        TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::AbortTab);
        QString sender = "org.tomocube.flmaskgeneratorPlugin";
        menuEvt.scriptSet(sender);
        publishSignal(menuEvt, "FL Mask Generator");
    }    
    void MainWindow::NvSliceIndexChanged(int xIdx, int yIdx, int zIdx) {        
        d->sceneManager->SetSliceIndex(1, xIdx);
        d->sceneManager->SetSliceIndex(2, yIdx);
        d->sceneManager->SetSliceIndex(0, zIdx);
    }    
    void MainWindow::RwGlobalPosChanged(int globalX, int globalY,int winType) {
        Q_UNUSED(winType)
        //d->sceneManager->ShowController(globalX, globalY, winType);
        d->sceneManager->SetGlobalPos(globalX, globalY);
    }
    void MainWindow::RwSlicePickChanged(float phyX, float phyY, float phyZ) {
        auto ws = Entity::WorkingSet::GetInstance();
        auto refImg = ws->GetHTImage();
        if(nullptr != refImg) {
            double res[3];
            refImg->GetResolution(res);
            auto xIdx = static_cast<int>(phyX);
            auto yIdx = static_cast<int>(phyY);
            auto zIdx = static_cast<int>(phyZ);

            d->sceneManager->SetSliceIndex(1, xIdx);
            d->sceneManager->SetSliceIndex(2, yIdx);
            d->sceneManager->SetSliceIndex(0, zIdx);
            const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(nullptr,d->ui->navigatorPanel) };

            Interactor::FLSceneController sceneController(panelPresenter.get());

            if (false == sceneController.ImageSliceChanged(xIdx,yIdx,zIdx)) {
                QMessageBox::warning(nullptr, "Error", "Failed change sliceIndex");
                return;
            }
        }
    }
    void MainWindow::RwSliceIndexChanged(int viewIndex, int sliceIndex) {
        d->sceneManager->SetSliceIndex(viewIndex, sliceIndex);
        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(nullptr,d->ui->navigatorPanel) };

        Interactor::FLSceneController sceneController(panelPresenter.get());

        if(false == sceneController.ImageSliceChanged(viewIndex,sliceIndex)) {
            QMessageBox::warning(nullptr, "Error", "Failed change sliceIndex");
            return;
        }
    }
    void MainWindow::RsCloseEvent() {        
        d->ui->parameterPanel->SetResultToggle(false);
    }
    void MainWindow::PaFLMinMaxChanged(int ch, int min, int max) {
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(nullptr,d->sceneManager.get(),d->ui->parameterPanel) };
        Interactor::FLSceneController sceneController(nullptr , scenePresenter.get());
        if(false == sceneController.FLRangeChanged(ch,min,max)) {
            QMessageBox::warning(nullptr, "Error", "Failed change FL range");
        }        
        auto chName = QString("Ch %1 intensity Parameter").arg(ch + 1);
        auto chParam = d->share_param[d->cur_procName]->GetChild(chName);
        chParam->SetValue("LThreshold", static_cast<double>(min) / 10000.0);
        chParam->SetValue("UThreshold", static_cast<double>(max) / 10000.0);
        //d->share_param[d->cur_procName]->SetChild(chName, chParam);
    }
    void MainWindow::PaOpaChanged(int ch, float opa) {
        d->sceneManager->SetFLOpacity(ch,opa);
    }
    void MainWindow::PaChRIIValue(int ch, double val) {
        QString chName = QString("Basic Measurement Parameter!Ch %1").arg(ch+1);
        auto child = d->share_param[d->cur_procName]->GetChild(chName);
        child->SetValue("RII", val);
    }    
    void MainWindow::PaChRII(int ch, int idx) {
        QString chName = QString("Basic Measurement Parameter!Ch %1").arg(ch+1);
        auto child = d->share_param[d->cur_procName]->GetChild(chName);        
        child->SetValue("Preset", idx);
    }
    void MainWindow::PaRIValue(double val) {
        for (auto i = 0; i < 3; i++) {
            QString chName = QString("Basic Measurement Parameter!Ch %1").arg(i+1);
            auto child = d->share_param[d->cur_procName]->GetChild(chName);
            child->SetValue("RI", val);
        }
    }
    void MainWindow::PaMaskCorrection() {
        auto ws = Entity::WorkingSet::GetInstance();

        for (auto i = 0; i < 3; i++) {
            auto mask = ws->GetMask(i);
            std::cout << "ch" << i;
            if (nullptr == mask) {
                std::cout  << " mask is empty" << std::endl;
            }
            else {
                std::cout << " mask exist" << std::endl;
            }
        }
    }
    void MainWindow::PaCustomRI(bool isCustom) {
        for (auto i = 0; i < 3; i++) {
            QString chName = QString("Basic Measurement Parameter!Ch %1").arg(i+1);
            auto child = d->share_param[d->cur_procName]->GetChild(chName);
            child->SetValue("CustomRI", isCustom);
        }
    }
    void MainWindow::PaToggleResult(bool show) {
        if (show) {
            d->result->show();
        }else {
            d->result->hide();
        }
    }
    void MainWindow::PaNameChanged(int ch, QString name) {
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetFLName(name, ch);        
        d->ui->vizcontrolPanel->SetChannelName(ch, name);
        auto nameList = d->share_param[d->cur_procName]->GetValue("DupNames").toArray();
        nameList[ch] = name;
        d->share_param[d->cur_procName]->SetValue("DupNames", nameList);
    }
    void MainWindow::PaVizToggled(int ch, bool viz) {
        auto ds = d->ui->parameterPanel->GetParameterDS();        
        ds->chShow[ch] = viz;        
        d->sceneManager->SetFLToggle(ch, viz);
        d->ui->renderwindowPanel->Render();
    }    
    void MainWindow::DmOpenTcfbyIndex(int idx,double time_point) {
        std::shared_ptr<Plugins::ImageDataReader> ireader = std::make_shared<Plugins::ImageDataReader>();
        std::shared_ptr<Plugins::MaskDataReader> mreader = std::make_shared<Plugins::MaskDataReader>();
        std::shared_ptr<Plugins::MaskDataWriter> mwriter = std::make_shared<Plugins::MaskDataWriter>();
        std::shared_ptr<Plugins::MeasureDataReader> rreader = std::make_shared<Plugins::MeasureDataReader>();

        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(d->ui->datamanagerPanel,d->ui->navigatorPanel,d->ui->parameterPanel,d->ui->vizcontrolPanel,d->result.get()) };
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(d->ui->renderwindowPanel,d->sceneManager.get(),d->ui->parameterPanel) };

        //save current mask first
        Interactor::FLMaskController maskController(nullptr,panelPresenter.get(),mwriter.get());

        if(false == maskController.SaveCurrentMask()) {
            QMessageBox::warning(nullptr, "Error", "Failed to save current FL mask");
            return;
        }
                
        //change current image index;
        Interactor::FLSceneController controller(panelPresenter.get(), scenePresenter.get(), ireader.get(), mreader.get(), rreader.get());

        d->result->clearResult();
        d->result->hide();
        d->ui->parameterPanel->SetResultToggle(false);

        if(false == controller.ChangeDataIndex(idx,time_point)) {
            QMessageBox::warning(nullptr, "Error", "Failed to change current tcf file");
            return;
        }

        auto workingset = Entity::WorkingSet::GetInstance();

        if (nullptr != workingset->GetMeasure()) {
            d->result->show();
            d->ui->parameterPanel->SetResultToggle(true);            
        }

        auto paramDS = d->ui->parameterPanel->GetParameterDS();

        for (auto i = 0; i < 3; i++) {
            d->sceneManager->SetFLToggle(i, paramDS->chShow[i]&&paramDS->chSelected[i]);
        }
        d->ui->renderwindowPanel->Render();
    }
    void MainWindow::VcFLMaskOpa(float opacity) {
        d->sceneManager->SetMaskOpacity(opacity);
    }
    void MainWindow::VcFLMaskViz(bool ch1Viz, bool ch2Viz, bool ch3Viz) {
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(d->ui->renderwindowPanel,d->sceneManager.get()) };

        Interactor::FLMaskController maskController(scenePresenter.get());

        auto idx = -1;

        if(ch1Viz) {
            idx = 0;
        }else if(ch2Viz) {
            idx = 1;
        }else if(ch3Viz) {
            idx = 2;
        }        

        if(false == maskController.ChangeMemoryMask(idx)) {
            QMessageBox::warning(nullptr, "Error", "Failed to open existing mask");           
        }
        if (idx > -1) {
            d->sceneManager->SetMaskToggle(ch1Viz, ch2Viz, ch3Viz);
        }
    }
    void MainWindow::DmPerformAll(){
        d->ui->datamanagerPanel->ClearSelection();
        d->cancelEmit = false;

        auto param_reader = std::make_shared<TC::IO::TCParameterReader>(d->cur_playgroundpath);
        auto prev_tup = param_reader->Read("FL Mask Generator", d->cur_hyperName);
        auto prev_proc_name = std::get<0>(prev_tup);
        auto prev_param = std::get<1>(prev_tup);

        auto cur_param_changed = false;
        QString paramMessage = QString();
        if (prev_proc_name.compare(d->cur_procName) != 0) {//processor changed
            QLOG_INFO() << "Processor changed, flush exiting results";
            paramMessage = "Processor changed, flush exiting results";
            cur_param_changed = true;
        }
        else if (nullptr == prev_param) {
            QLOG_INFO() << "Empty previous parameter, flush existing results";
            paramMessage = "Empty previous parameter, flush existing results";
            cur_param_changed = true;
        }
        else if (!prev_param->IsValueEqual(d->share_param[d->cur_procName])) {
            QLOG_INFO() << "Parameter changed, flush existing results";
            paramMessage = "Parameter changed, flush existing results";
            cur_param_changed = true;
        }               

        auto current = QDateTime::currentDateTime();        
        d->oTimeStamp = current.toString("yyMMdd.hhmmss");

        if (cur_param_changed) {            
            auto param_writer = std::make_shared<TC::IO::TCParameterWriter>(d->cur_playgroundpath);
            //Write to temp folder
            param_writer->Write(d->share_param[d->cur_procName], "FL Mask Generator", "ReportFL", d->cur_hyperName, d->cur_procName, QString());
            //Write to hitory folder
            param_writer->Write(d->share_param[d->cur_procName], "FL Mask Generator", "ReportFL", d->cur_hyperName, d->cur_procName, d->oTimeStamp);
        }

        d->waitMsg = new QMessageBox(QMessageBox::Icon::Information, "Information", "Wait for job finished..");
        d->waitMsg->setStandardButtons(nullptr);
        d->waitMsg->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        
        if(d->dialog) {
            delete d->dialog;
            d->dialog = nullptr;
        }                

        d->dialog = new QProgressDialog;
        d->dialog->setLabelText(QString("Progressing %1 tcf files..").arg(d->cur_tcfs.count()));

        auto ds = d->ui->datamanagerPanel->GetDataManagerDS();

        auto total_count = 0;
        for (auto step : ds->tcfSteps) {
            total_count += step;
        }        

        //d->dialog->setRange(0, d->cur_tcfs.count());
        d->dialog->setRange(0, total_count);
        connect(d->dialog, SIGNAL(canceled()), this, SLOT(OnBatchCanceled()));

        d->cur_algo_name = "FL Mask Generation";

        d->thread = SbThread::create(ThreadFunc, (void*)d.get());
        d->dialog->open();
    }
    void MainWindow::OnBatchCanceled() {
        d->cancelEmit = true;
        d->dialog->finished(0);
        d->waitMsg->exec();
    }
    void MainWindow::OnBatchStepChanged(int step) {
        if(false == d->cancelEmit) {
            d->dialog->setValue(step);
            d->ui->datamanagerPanel->Update();
        }
    }
    void MainWindow::OnBatchStepStarted() {
        if(d->cancelEmit) {
            return;
        }
        auto tcf_idx = d->cur_image_step;
        auto tcf_name = d->cur_tcfs[tcf_idx];
        QFileInfo file_name(tcf_name);
        auto fn = file_name.fileName();
        d->dialog->setLabelText(QString("Current file: %1 (%2/%3)\nPerforming %4")
            .arg(fn).arg(tcf_idx + 1).arg(d->cur_tcfs.count()).arg(d->cur_algo_name));
    }
    void MainWindow::OnBatchFinished(int result_step) {
        if(result_step == -1) {
            d->dialog->finished(0);            

            auto  oWriter = std::make_shared<TC::IO::TCParameterWriter>(d->cur_playgroundpath);
            oWriter->Write(d->share_param[d->cur_procName], "FL Mask Generator", "ReportFL", d->cur_hyperName, d->cur_procName, d->oTimeStamp);

            auto repFolderPath = d->cur_playgroundpath.chopped(5);
            QDir pdir(repFolderPath);
            pdir.cdUp();//playground folder
            if(false == pdir.exists("history")) {
                pdir.mkdir("history");
            }
            pdir.cd("history");            
            if(false == pdir.exists(d->cur_hyperName)) {
                pdir.mkdir(d->cur_hyperName);
            }
            pdir.cd(d->cur_hyperName);            
            if(false == pdir.exists(d->oTimeStamp)) {
                pdir.mkdir(d->oTimeStamp);
            }
            pdir.cd(d->oTimeStamp);            

            repFolderPath += "/";
            repFolderPath += d->cur_hyperName;

            auto targetFolderPath = pdir.path();
            QDir cdir(targetFolderPath);
            for (auto c : d->cur_cubes) {
                if (false == cdir.exists(c)) {
                    cdir.mkdir(c);
                }
            }
            auto cubeCntIdx = 0;
            auto curCnt = 0;
            for (auto tcfName : d->cur_tcfs) {                
                QFileInfo info(tcfName);
                auto filename = info.fileName().chopped(4);
                auto reportPath = repFolderPath + "/" + filename + ".rep";
                auto maskPath = repFolderPath + "/" + filename + ".msk";
                if (QFileInfo::exists(reportPath)) {
                    //if report file exist copy it to target folder                
                    QString ccc;
                    if (d->cur_cubeCounts[cubeCntIdx] > curCnt) {
                        curCnt++;
                        ccc = d->cur_cubes[cubeCntIdx];
                    }
                    else {
                        curCnt = 1;
                        ccc = d->cur_cubes[++cubeCntIdx];
                    }
                    auto targetPath = targetFolderPath + "/" + ccc + "/" + filename + ".rep";
                    auto targetMaskPath = targetFolderPath + "/" + ccc + "/" + filename + ".msk";
                    QFile::copy(reportPath, targetPath);
                    QFile::copy(maskPath, targetMaskPath);                    
                    //copy mask paths
                    auto maskDir = maskPath.chopped(4);
                    auto targetMaskDir = targetMaskPath.chopped(4);
                    CopyPath(maskDir, targetMaskDir);
                }
            }
            //copy current playground
            auto copyPGPath = d->cur_playgroundpath.chopped(5);
            QDir zdir(copyPGPath);
            zdir.cdUp();
            zdir.cd("history");

            copyPGPath = zdir.path();
            copyPGPath += "/";
            copyPGPath += d->cur_hyperName;
            copyPGPath += "_";
            copyPGPath += d->oTimeStamp;
            copyPGPath += ".tcpg";
            QFile::copy(d->cur_playgroundpath, copyPGPath);
            BroadcastFinishBatch();
        }else {
            BroadcastAbortBatch();
        }
        SbThread::destroy(d->thread);
        if(d->waitMsg->isVisible()) {
            d->waitMsg->accept();
        }

        delete d->waitMsg;
        d->waitMsg = nullptr;

        DmOpenTcfbyIndex(0,0.0);
    }
    auto MainWindow::CopyPath(QString src, QString dst) -> void {
        QDir dir(src);
        if (!dir.exists())
            return;

        foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString dst_path = dst + QDir::separator() + dd;
            dir.mkpath(dst_path);
            CopyPath(src + QDir::separator() + dd, dst_path);
        }

        foreach(QString f, dir.entryList(QDir::Files)) {
            QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
        }
    }

    auto MainWindow::BroadcastFinishBatch() -> void {
        TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::BatchFinish);
        QString target = "org.tomocube.projectmanagerPlugin";
        QString sender = "org.tomocube.flmaskgeneratorPlugin";
        menuEvt.scriptSet(sender + "!" + target + "!NoClose");
        publishSignal(menuEvt, "FL Mask Generator");
    }
    auto MainWindow::BroadcastAbortBatch() -> void {
        TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::AbortTab);
        QString sender = "org.tomocube.flmaskgeneratorPlugin";
        menuEvt.scriptSet(sender + "!NoClose");
        publishSignal(menuEvt, "FL Mask Generator");
    }
    void MainWindow::PaPerformMask(int ch, int min, int max) {
        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(d->ui->datamanagerPanel,nullptr,nullptr,d->ui->vizcontrolPanel) };
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(d->ui->renderwindowPanel,d->sceneManager.get()) };
        Interactor::FLMaskController maskController(scenePresenter.get(), panelPresenter.get(), nullptr, nullptr, d->engine.get());
        if (false == maskController.CreateMask(ch, min, max)) {
            QMessageBox::warning(nullptr, "Error", "Failed create FL mask");
        }
    }
    void MainWindow::PaPerformMeasure() {
        std::shared_ptr<Plugins::MaskDataWriter> mwriter = std::make_shared<Plugins::MaskDataWriter>();
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter = std::make_shared<Plugins::MeasureDataWriter>();
        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(d->ui->datamanagerPanel,nullptr,nullptr,nullptr,d->result.get()) };
        Interactor::FLMaskController maskController(nullptr, panelPresenter.get(), mwriter.get(), rwriter.get(), d->engine.get());

        //set parameter for measurement
        auto ws = Entity::WorkingSet::GetInstance();
        for (auto i = 0; i < 3; i++) {
            auto rii = d->ui->parameterPanel->GetRII(i);
            ws->SetRII(i, rii);
        }
        auto bri = d->ui->parameterPanel->GetBaselineRI();
        if (bri < 0) {
            auto reader = TC::IO::TCFMetaReader();
            auto bri_from_file = static_cast<float>(reader.ReadMediumRI(ws->GetCurrentImage()));
            if (bri_from_file == 0.0) {
                bri_from_file = 1.337f;
            }
            ws->SetBaselineRI(bri_from_file);
        }
        else {
            ws->SetBaselineRI(bri);
        }

        if (false == maskController.PerformMeasure()) {
            QMessageBox::warning(nullptr, "Error", "Failed to measure with FL mask");
        }

        d->ui->parameterPanel->SetResultToggle(true);
    }
    void* MainWindow::ThreadFunc(void* userData) {
        SoDB::init();
        SoImageViz::init();
        SoVolumeRendering::init();
        InventorMedical::init();

        SoDB::setSystemTimer(OivTimer::GetInstance());

        SoPreferences::setValue("LDM_USE_IN_MEM_COMPRESSION", "0");
        SoPreferences::setValue("LDM_USE_BUILDTILE_BY_SLICE", "1");

        auto dd = static_cast<Impl*>(userData);        
        std::shared_ptr<Plugins::ImageDataReader> ireader = std::make_shared<Plugins::ImageDataReader>();
        std::shared_ptr<Plugins::MaskDataReader> mreader = std::make_shared<Plugins::MaskDataReader>();
        std::shared_ptr<Plugins::MaskDataWriter> mwriter = std::make_shared<Plugins::MaskDataWriter>();
        std::shared_ptr<Plugins::MeasureDataReader> rreader = std::make_shared<Plugins::MeasureDataReader>();
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter = std::make_shared<Plugins::MeasureDataWriter>();
        const std::shared_ptr<Interactor::FLPanelPresenter> panelPresenter{ new Interactor::FLPanelPresenter(dd->ui->datamanagerPanel,dd->ui->navigatorPanel,dd->ui->parameterPanel,dd->ui->vizcontrolPanel,dd->result.get()) };
        const std::shared_ptr<Interactor::FLScenePresenter> scenePresenter{ new Interactor::FLScenePresenter(nullptr,dd->sceneManager.get(),dd->ui->parameterPanel) };

        Interactor::FLMaskController maskController(scenePresenter.get(), panelPresenter.get(), mwriter.get(), rwriter.get(), dd->engine.get());
        Interactor::FLSceneController controller(panelPresenter.get(), scenePresenter.get(), ireader.get(), mreader.get(), rreader.get());
        auto dds = dd->ui->datamanagerPanel->GetDataManagerDS();
        auto pds = dd->ui->parameterPanel->GetParameterDS();

        dd->cur_batch_step = 0;
        dd->visited = dds->visited;

        auto ws = Entity::WorkingSet::GetInstance();
        for (auto i = 0; i < 3; i++) {
            auto rii = dd->ui->parameterPanel->GetRII(i);
            ws->SetRII(i, rii);
        }
        auto bri = dd->ui->parameterPanel->GetBaselineRI();
        if (bri < 0) {
            auto reader = TC::IO::TCFMetaReader();
            auto bri_from_file = static_cast<float>(reader.ReadMediumRI(ws->GetCurrentImage()));
            if (bri_from_file == 0.0) {
                bri_from_file = 1.337f;
            }
            ws->SetBaselineRI(bri_from_file);
        }
        else {
            ws->SetBaselineRI(bri);
        }        
        auto metareader = TC::IO::TCFMetaReader();        
        auto real_idx = 0;
        for (auto i = 0; i < dds->tcfList.count(); i++) {
            dd->cur_image_step = i;
            auto meta = metareader.Read(dds->tcfList[i]);
            for (auto j = 0; j < meta->data.data3DFL.timePoints.count(); j++) {                
                emit dd->thisPointer->batchStarted();
                if (dd->visited[real_idx]) {
                    emit dd->thisPointer->batchStepChanged(real_idx + 1);
                    real_idx++;
                    continue;
                }
                if (dd->cancelEmit) {
                    emit dd->thisPointer->batchFinished(real_idx);
                }
                controller.ChangeInternalDataIndex(i, meta->data.data3DFL.timePoints[j]);
                for (auto ch = 0; ch < 3; ch++) {
                    if (false == dd->isChSelected[ch]) {
                        continue;
                    }
                    if (dd->cancelEmit) {
                        emit dd->thisPointer->batchFinished(real_idx);
                    }
                    auto min = pds->curMinMax[ch][0];
                    auto max = pds->curMinMax[ch][1];
                    maskController.CreateInternalMask(ch, min, max);

                    maskController.PerformInternalMeasure();
                }
                dd->visited[real_idx] = true;
                emit dd->thisPointer->batchStepChanged(real_idx + 1);
                dd->cur_batch_step = real_idx + 1;
                real_idx++;

                SoDB::processEvents();
                SbThread::sleep_ms(1000);
            }            
        }
        emit dd->thisPointer->batchFinished(-1);

        InventorMedical::finish();
        SoVolumeRendering::finish();
        SoImageViz::finish();
        SoDB::finish();
        return nullptr;
    }
}
