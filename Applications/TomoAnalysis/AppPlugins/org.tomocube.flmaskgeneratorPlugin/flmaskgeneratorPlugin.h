#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.flmaskgeneratorPlugin";

namespace TomoAnalysis::FLMaskGenerator::AppUI {
	class flmaskgeneratorPlugin : public TC::Framework::IAppActivator {
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.flmaskgeneratorPlugin")

	public:
		static flmaskgeneratorPlugin* getInstance();

		flmaskgeneratorPlugin();
		~flmaskgeneratorPlugin();

		auto start(ctkPluginContext* context)->void override;
		auto getPluginContext()->ctkPluginContext* const override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
		static flmaskgeneratorPlugin* instance;
	};
}