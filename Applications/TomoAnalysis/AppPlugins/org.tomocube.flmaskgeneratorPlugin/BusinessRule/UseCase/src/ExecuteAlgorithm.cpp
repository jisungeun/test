#include <TCMeasure.h>
#include <DataList.h>
#include <ScalarData.h>

#include "ExecuteAlgorithm.h"

#include <iostream>

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    struct ExecuteAlgorithm::Impl {
        auto DataListToMeausre(DataList::Pointer dl)->TCMeasure::Pointer {
            auto measure = std::make_shared<TCMeasure>();
            for (const auto& dataSet : dl->GetList()) {
                auto name = std::static_pointer_cast<ScalarData>(dataSet->GetData("Name"));
                
                auto organ_name = name->ValueAsString();
                auto cellcount = std::static_pointer_cast<ScalarData>(dataSet->GetData("CellCount"));

                for (auto j = 0; j < cellcount->ValueAsInt(); j++) {
                    //auto real_idx = std::static_pointer_cast<ScalarData>(dataSet->GetData("real_idx" + QString::number(j)))->ValueAsInt();
                    auto real_idx = j;
                    auto volume = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Volume"));
                    measure->AppendMeasure(organ_name + ".Volume", j, volume->ValueAsDouble());
                    auto sa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".SurfaceArea"));
                    measure->AppendMeasure(organ_name + ".SurfaceArea", j, sa->ValueAsDouble());
                    auto mri = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".MeanRI"));
                    measure->AppendMeasure(organ_name + ".MeanRI", j, mri->ValueAsDouble());
                    auto sph = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Sphericity"));
                    measure->AppendMeasure(organ_name + ".Sphericity", j, sph->ValueAsDouble());
                    auto pa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".ProjectedArea"));
                    measure->AppendMeasure(organ_name + ".ProjectedArea", j, pa->ValueAsDouble());
                    auto dm = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Drymass"));
                    measure->AppendMeasure(organ_name + ".Drymass", j, dm->ValueAsDouble());
                    auto ct = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Concentration"));
                    measure->AppendMeasure(organ_name + ".Concentration", j, ct->ValueAsDouble());
                }
            }
            return measure;
        }
    };
    ExecuteAlgorithm::ExecuteAlgorithm() : d{new Impl} {
        
    }
    ExecuteAlgorithm::~ExecuteAlgorithm() {
        
    }
    auto ExecuteAlgorithm::CreateFLThreshMask(IFLScenePort* sport, IFLPanelPort* pport, IProcessingPort* engine, int ch,int lower,int upper) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();

        auto input = ws->GetFLImage(ch);
        auto refImage = ws->GetHTImage();

        if(nullptr == input) {
            return false;
        }        

        auto mask = engine->CreateFLMask(input,refImage, upper, lower);                
        ws->SetMask(mask,ch);

        if (nullptr != sport) {
            sport->LoadMask(mask);
        }
        if (nullptr != pport) {
            pport->UpdateMask(ch);
        }
        
        return true;
    }
    auto ExecuteAlgorithm::PerformMeasurement(IFLPanelPort* pport, IProcessingPort* engine, IMaskWriterPort* mwriter, IMeasureWriterPort* rwriter) -> bool {
        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < std::numeric_limits<double>::epsilon();
        };
        auto ws = Entity::WorkingSet::GetInstance();        
        auto input = ws->GetHTImage();
        DataList::Pointer result = DataList::New();
        for(auto i=0;i<3;i++) {            
            auto flMask = std::dynamic_pointer_cast<TCMask>(ws->GetMask(i));            
            if(nullptr != flMask) {                
                //Write current mask                
                mwriter->Write(flMask, ws->GetMaskPath(),ws->GetFLName(i),i);
                engine->CreateMeasure(input, flMask,ws->GetFLName(i),ws->GetBaselineRI(),ws->GetRII(i), result);
            }
        }        
        auto time_step = input->GetTimeStep();
        std::cout << "time step from image : " << time_step << std::endl;
        //write current measure
        auto measure = d->DataListToMeausre(result);
        measure->SetTimeIndex(time_step);
        measure->SetTimePoint(ws->GetTimePoints()[time_step]);
        ws->SetMeasure(measure);
        rwriter->Write(measure, ws->GetMeasurePath());
        if (nullptr != pport) {
            pport->UpdateMeasure();
        }
        return true;
    }

}
