#include <QJsonArray>

#include <FLMGWorkingSet.h>

#include "ManageScene.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    struct ManageScene::Impl {
        
    };
    ManageScene::ManageScene() : d{ new Impl } {
        
    }
    ManageScene::~ManageScene() {
        
    }
    auto ManageScene::ChangeSliceIndex(IFLPanelPort* port, int viewIndex, int sliceIndex) -> bool {
        if(viewIndex == 0) {
            port->ChangeSlice(-1, -1, sliceIndex);
        }else if(viewIndex == 1) {
            port->ChangeSlice(sliceIndex, -1, -1);
        }else {
            port->ChangeSlice(-1, sliceIndex, -1);
        }                
        return true;
    }
    auto ManageScene::ChangeFLDataRange(IFLScenePort* port, int ch, int min, int max) -> bool {
        port->ChangeFLRange(ch,min,max);
        return true;
    }
    auto ManageScene::ChangeSliceIndex(IFLPanelPort* port, int xIdx, int yIdx, int zIdx) -> bool {
        port->ChangeSlice(xIdx,yIdx,zIdx);
        return true;
        
    }
    auto ManageScene::ChangeFLMask(IFLScenePort* port, int ch) -> bool {
        auto workingset = Entity::WorkingSet::GetInstance();
                
        if(ch<0) {
            port->LoadMask(nullptr);
        }
        else {
            port->LoadMask(workingset->GetMask(ch));
        }

        return true;
    }
    auto ManageScene::UpdateBatchControlUI(IFLPanelPort* port,IParameter::Pointer param) -> bool {
        auto procSelection = param->GetValue("ProcSelection").toArray();

        bool channelExist[3];        
        for (auto i = 0; i < procSelection.count(); i++) {
            channelExist[i] = procSelection[i].toBool();            
        }
                
        if (false == (channelExist[0] ||channelExist[1] || channelExist[2])) {
            return false;
        }

        port->UpdateChannel(channelExist[0],channelExist[1],channelExist[2]);
        return true;
    }        
}