#include <iostream>

#include <QDir>
#include <TCFMetaReader.h>
#include <ProjectDataReader.h>

#include "LoadDataSet.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
	struct LoadDataSet::Impl {

	};
	LoadDataSet::LoadDataSet() :d{ new Impl } {

	}
	LoadDataSet::~LoadDataSet() {

	}
	auto LoadDataSet::LoadTcfPaths(IFLPanelPort* port, QStringList tcfs, QString playgroundpath, QString hypername) -> bool {
		PlaygroundInfo::Pointer pInfo = std::make_shared<PlaygroundInfo>();
		auto reader = new ProjectDataReader;
		reader->ReadPlaygroundData(playgroundpath, pInfo);

		HyperCube::Pointer hyperPtr{ nullptr };
		for (auto i = 0; i < pInfo->GetHyperCubeList().count(); i++) {
			auto hypercube = pInfo->GetHyperCubeList()[i];
			if (hypercube->GetName() == hypername) {
				hyperPtr = hypercube;
				break;
			}
		}
		if (nullptr == hyperPtr) {
			return false;
		}

		QStringList cubeList;
		for (auto i = 0; i < tcfs.count(); i++) {
			auto tcfPath = tcfs[i];
			for (auto j = 0; j < hyperPtr->GetCubeList().count(); j++) {
				auto cube = hyperPtr->GetCubeList()[j];
				auto cube_found = false;
				for (auto p : cube->GetTCFDirList()) {
					if (p->GetPath() == tcfPath) {
						cubeList.push_back(cube->GetName());
						cube_found = true;
						break;
					}
				}
				if (cube_found) {
					break;
				}
			}
		}

		if (cubeList.count() != tcfs.count()) {
			return false;
		}

		port->UpdateFileList(tcfs, cubeList, hypername);

		delete reader;
		return true;
	}
	auto LoadDataSet::SetCurrentTcf(IFLPanelPort* port, IFLScenePort* dport, IImageReaderPort* ireader, IMaskReaderPort* mreader, IMeasureReaderPort* rredear, QString path, int idx, double time_point) -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};

		if (time_point < 0) {
			return false;
		}
		auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		auto metaInfo = metaReader->Read(path);
		auto ht_tps = metaInfo->data.data3D.timePoints;
		std::cout << "time point: " << time_point << std::endl;
		if (false == ht_tps.contains(time_point)) {
			return false;
		}
		auto time_step = -1;
		for (auto i = 0; i < ht_tps.count(); i++) {
			if (AreSame(ht_tps[i], time_point)) {
				time_step = i;
				break;
			}
		}

		if (ht_tps.count() <= time_step || time_step <0) {
			return false;
		}

		auto workingset = Entity::WorkingSet::GetInstance();
		workingset->ClearData();				
		workingset->SetCurrentImage(idx);
		//read time points
		workingset->SetTimePoints(metaInfo->data.data3D.timePoints);
		for (auto i = 0; i < 3; i++) {
			workingset->SetFLTimePoints(metaInfo->data.data3DFL.timePoints, i);
		}				

		workingset->SetCurTimePoint(time_point);
		//create playground folder if required
		auto pg = workingset->GetPlayground();
		QDir pgDir(pg.chopped(5));
		if (false == pgDir.exists()) {
			auto dirName = pgDir.dirName();
			pgDir.cdUp();
			pgDir.mkdir(dirName);
			pgDir.cd(dirName);
		}

		if (false == pgDir.exists(workingset->GetHypercubeName())) {
			pgDir.mkdir(workingset->GetHypercubeName());
		}

		//create mask path
		auto mask_path = pg.chopped(5) + "/" +workingset->GetHypercubeName() +"/" + QFileInfo(path).fileName().chopped(4) + ".msk";
		workingset->SetMaskPath(mask_path);
		//create measure path
		auto measure_path = pg.chopped(5) + "/" + workingset->GetHypercubeName() + "/" + QFileInfo(path).fileName().chopped(4) + ".rep";		workingset->SetMeasurePath(measure_path);

		//read tcf file
		auto img = ireader->Read(path, time_step);
		img->SetMinMax(static_cast<int>(metaInfo->data.data3D.riMin * 10000.0), static_cast<int>(metaInfo->data.data3D.riMax * 10000.0));
		img->SetTimeStep(time_step);
		workingset->SetHTImage(img);
		auto fl_offset = 0.0;
		for (auto i = 0; i < 3; i++) {			
			auto mval = metaInfo->data.data3DFL.valid[i];
			auto wval = workingset->GetFLSelected(i);
			if (mval&&wval) {
				auto fl_tps = workingset->GetFLTimePoints(i);
				for (auto j = 0; j < fl_tps.count(); j++) {			
					if (AreSame(fl_tps[j], time_point)) {
						auto fl_time_step = j;
						auto flImg = ireader->ReadFL(path, i, fl_time_step);
						flImg->SetMinMax(metaInfo->data.data3DFL.min[i], metaInfo->data.data3DFL.max[i]);
						flImg->SetTimeStep(fl_time_step);
						fl_offset = metaInfo->data.data3DFL.offsetZ;
						workingset->SetFLImage(flImg, i);
						break;
					}
				}
			}
		}

		auto maskExist = false;
		//read mask file if exist
		QFileInfo maskFileInfo(mask_path);
		if (maskFileInfo.exists()) {
			auto metaList = mreader->GetMaskList(mask_path);
			for(auto i=0;i<metaList.count();i++){
				auto msk = mreader->Read(mask_path, metaList[i].name,time_step);
				if (nullptr != msk) {
					workingset->SetMask(msk, metaList[i].ch);
				}
				else {
					workingset->SetMask(nullptr, metaList[i].ch);
				}
			}			
			maskExist = true;
		}

		//read measure file if exist
		QFileInfo measureFileInfo(measure_path);
		if (measureFileInfo.exists()) {
			auto measure = rredear->Read(measure_path, time_step);
			if (measure->GetKeys().count() > 0) {
				workingset->SetMeasure(measure);
			}
			else {
				workingset->SetMeasure(nullptr);
			}
		}

		//Update current Index in data manager panel
		if (nullptr != port) {
			port->UpdateFileList(QStringList(), QStringList(), QString(), idx,time_point);
		}
		if (nullptr != dport) {
			//set loaded data into render-window
			dport->LoadImage(img, workingset->GetFLImage(0), workingset->GetFLImage(1), workingset->GetFLImage(2), static_cast<float>(fl_offset));
		}
		if (nullptr != port) {
			if (measureFileInfo.exists()) {
				port->UpdateMeasure();
			}
		}

		return true;
	}
}