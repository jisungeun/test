#pragma once

#include <memory>

#include "IFLScenePort.h"
#include "IFLPanelPort.h"
#include "IFLMaskWriterPort.h"
#include "IFLMeasureWriterPort.h"
#include "IFLProcessingPort.h"

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API ExecuteAlgorithm {
    public:
        ExecuteAlgorithm();
        virtual ~ExecuteAlgorithm();

        auto CreateFLThreshMask(IFLScenePort* sport,IFLPanelPort* pport,IProcessingPort* engine,int ch,int upper,int lower)->bool;
        auto PerformMeasurement(IFLPanelPort* pport, IProcessingPort* engine,IMaskWriterPort* mwriter,IMeasureWriterPort* rwriter )->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}