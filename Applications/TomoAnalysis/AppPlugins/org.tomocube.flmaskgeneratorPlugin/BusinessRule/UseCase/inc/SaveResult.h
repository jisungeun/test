#pragma once

#include <memory>

#include <FLMGWorkingSet.h>

#include "IFLMaskWriterPort.h"
#include "IFLMeasureWriterPort.h"

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API SaveResult {
    public:
        SaveResult();
        virtual ~SaveResult();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}