#pragma once

#include <TCMeasure.h>
#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IMeasureReaderPort {
    public:
        IMeasureReaderPort();
        virtual ~IMeasureReaderPort();

        virtual auto Read(const QString& path, const int& time_step)->TCMeasure::Pointer = 0;
    };
}