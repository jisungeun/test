#pragma once

#include <IBaseMask.h>
#include <IBaseImage.h>
#include <IBaseData.h>
#include <DataList.h>

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IProcessingPort {
    public:
        IProcessingPort();
        virtual ~IProcessingPort();

        virtual auto CreateFLMask(IBaseImage::Pointer input,IBaseImage::Pointer refImage,int uppper,int lower)->IBaseMask::Pointer=0;
        virtual auto CreateMeasure(IBaseImage::Pointer input, IBaseMask::Pointer mask,QString chName,float baselineRI,float RII,DataList::Pointer result)->IBaseData::Pointer = 0;
    };
};