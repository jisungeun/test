#pragma once

#include <qstring.h>
#include <TCImage.h>

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
	class FLMaskGeneratorUseCase_API IImageReaderPort {
	public:
	    IImageReaderPort();
		virtual ~IImageReaderPort();

		virtual auto Read(const QString& path, const int& time_step = 0)const->TCImage::Pointer = 0;
		virtual auto ReadFL(const QString& path, const int& ch = 0, const int& time_step = 0)->TCImage::Pointer = 0;
		virtual auto GetTimeStep(const QString& path)const->int = 0;
	};
}