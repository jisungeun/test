#pragma once

#include <memory>

#include <IParameter.h>

#include "IFLPanelPort.h"
#include "IFLScenePort.h"

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API ManageScene {
    public:
        ManageScene();
        virtual ~ManageScene();

        auto UpdateBatchControlUI(IFLPanelPort* port,IParameter::Pointer param)->bool;        
        auto ChangeSliceIndex(IFLPanelPort* port, int viewIndex,int sliceIndex)->bool;
        auto ChangeSliceIndex(IFLPanelPort* port, int xIdx, int yIdx,int zIdx)->bool;
        auto ChangeFLDataRange(IFLScenePort* port, int ch, int min, int max)->bool;
        auto ChangeFLMask(IFLScenePort* port, int ch)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}