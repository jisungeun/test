#pragma once

#include <TCMask.h>
#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IMaskWriterPort {
    public:
        IMaskWriterPort();
        virtual ~IMaskWriterPort();

        virtual auto Write(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch)->bool = 0;
        virtual auto Modify(TCMask::Pointer data, const QString& path, const QString& organName,const int&ch)->bool = 0;
    };
}