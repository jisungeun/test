#pragma once

#include <TCMeasure.h>
#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IMeasureWriterPort {
    public:
        IMeasureWriterPort();
        virtual ~IMeasureWriterPort();

        virtual auto Write(TCMeasure::Pointer data, const QString& path)->bool = 0;
        virtual auto Modify(TCMeasure::Pointer data, const QString& path)->bool = 0;
    };
}
