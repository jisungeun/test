#pragma once

#include <FLMGWorkingSet.h>
#include <IBaseImage.h>
#include <IBaseMask.h>

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IFLScenePort {
    public:
        IFLScenePort();
        virtual ~IFLScenePort();

        virtual auto Update()->void = 0;
        virtual auto ChangeFLRange(int ch, int min, int max)->void = 0;
        virtual auto LoadImage(IBaseImage::Pointer ht_image,IBaseImage::Pointer fl_image1,IBaseImage::Pointer fl_image2,IBaseImage::Pointer fl_image3,float fl_offset = 0.0)->void = 0;
        virtual auto LoadMask(IBaseMask::Pointer fl_mask)->void = 0;
    };
}