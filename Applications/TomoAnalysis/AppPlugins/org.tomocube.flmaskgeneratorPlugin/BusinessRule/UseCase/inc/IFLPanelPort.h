#pragma once

#include <FLMGWorkingSet.h>

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IFLPanelPort {
    public:
        IFLPanelPort();
        virtual ~IFLPanelPort();

        virtual auto UpdateMeasure()->void = 0;
        virtual auto ChangeSlice(int xIdx, int yIdx,int zIdx)->void = 0;
        virtual auto UpdateChannel(bool ch1Exist,bool ch2Exist, bool ch3Exist)->void = 0;
        virtual auto UpdateMask(int selection)->void = 0;
        virtual auto UpdateFileList(const QStringList& path, const QStringList& cubes, const QString& hyper, const int& idx = -1,const double& time_point = -1.0)->void = 0;
    };
}