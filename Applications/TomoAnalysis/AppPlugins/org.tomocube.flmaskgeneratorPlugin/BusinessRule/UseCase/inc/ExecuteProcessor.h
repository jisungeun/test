#pragma once

#include <memory>

#include "IProcessor.h"

#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API ExecuteProcessor {
    public:
        ExecuteProcessor();
        virtual ~ExecuteProcessor();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}