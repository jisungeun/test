#pragma once

#include <FLMGWorkingSet.h>
#include <IBaseMask.h>
#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API IMaskReaderPort {
    public:
        IMaskReaderPort();
        virtual ~IMaskReaderPort();

        virtual auto Read(const QString& path, const QString& chName,const int& time_idx = 0) ->TCMask::Pointer= 0;
        virtual auto GetMaskList(const QString& path)->QList<MaskMeta> = 0;
        virtual auto Create(TCImage::Pointer image,QString mask_name,int typeIdx)->TCMask::Pointer = 0;
    };
}