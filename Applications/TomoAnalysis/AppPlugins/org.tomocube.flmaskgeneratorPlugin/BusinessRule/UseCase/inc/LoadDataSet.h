#pragma once

#include <memory>

#include <FLMGWorkingSet.h>

#include "IFLScenePort.h"
#include "IFLPanelPort.h"
#include "IFLImageReaderPort.h"
#include "IFLMaskReaderPort.h"
#include "IFLMeasureReaderPort.h"


#include "FLMaskGeneratorUseCaseExport.h"

namespace TomoAnalysis::FLMaskGenerator::UseCase {
    class FLMaskGeneratorUseCase_API LoadDataSet {
    public:
        LoadDataSet();
        virtual ~LoadDataSet();

        auto LoadTcfPaths(IFLPanelPort* port,QStringList tcfs,QString playgroundpath,QString hypername)->bool;
        auto SetCurrentTcf(IFLPanelPort*port, IFLScenePort* dport,IImageReaderPort* ireader,IMaskReaderPort* mreader,IMeasureReaderPort* rredear,QString path, int idx,double time_point)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}