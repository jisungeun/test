#pragma once

#include <memory>
#include <QMap>

#include <TCImage.h>
#include <TCMask.h>
#include <TCMeasure.h>
#include <DataList.h>
#include <IParameter.h>

#include "FLMaskGeneratorEntityExport.h"

namespace TomoAnalysis::FLMaskGenerator::Entity {    
    class FLMaskGeneratorEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;
        
        WorkingSet();
        virtual ~WorkingSet();

        static auto GetInstance()->Pointer;

        auto SetWorkingDir(const QString& path)->bool;
        auto GetWorkingDir()->QString;
        auto SetHypercubeName(const QString& name)->bool;
        auto GetHypercubeName()->QString;
        auto SetImageList(const QStringList& imageList)->bool;
        auto AppendImage(const QString& imagePath)->bool;
        auto GetImageList()->QStringList;
        auto SetCurrentImage(const QString& path)->bool;
        auto SetCurrentImage(const int& idx)->bool;
        auto GetCurrentImage()->QString;                
        auto SetCurTimePoint(const double& step)->bool;
        auto GetCurTimePoint()->double;
        auto SetMeasurePath(const QString& path)->bool;
        auto GetMeasurePath()->QString;
        auto SetMaskPath(const QString& path)->bool;
        auto GetMaskPath()->QString;
        auto SetPlayground(const QString& pg)->bool;
        auto GetPlayground()->QString;
        auto SetTimePoints(const QList<double>& tps)->bool;
        auto GetTimePoints()->QList<double>;
        auto SetFLTimePoints(const QList<double>& tps, const int& chIdx)->bool;
        auto GetFLTimePoints(const int& chIdx)->QList<double>;
        auto SetFLSelected(const int& chIdx, bool selected)->void;
        auto GetFLSelected(const int& chIdx)->bool;
        auto SetRII(const int& chIdx, const float& RII)->void;
        auto GetRII(const int& chIdx)->float;
        auto SetBaselineRI(const float& BRI)->void;
        auto GetBaselineRI()->float;

        auto SetHTImage(TCImage::Pointer htImage)->void;
        auto GetHTImage()->TCImage::Pointer;
        auto SetFLImage(TCImage::Pointer flImage,const int& chIdx)->bool;
        auto GetFLImage(const int& chIdx)->TCImage::Pointer;
        auto SetFLName(const QString& name, const int& chIdx)->bool;
        auto GetFLName(const int& chIdx)->QString;

        auto SetMask(IBaseMask::Pointer mask,int ch)->void;
        auto GetMask(int ch)->IBaseMask::Pointer;

        auto SetParameter(IParameter::Pointer param)->void;
        auto GetParameter()->IParameter::Pointer;

        auto SetMeasure(TCMeasure::Pointer measure)->void;
        auto GetMeasure()->TCMeasure::Pointer;
        
        auto Clear()->void;
        auto ClearData()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}