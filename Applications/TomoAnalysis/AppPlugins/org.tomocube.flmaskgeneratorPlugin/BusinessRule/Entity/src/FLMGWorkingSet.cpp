#include <QDir>
#include <QFileInfo>

#include "FLMGWorkingSet.h"

namespace TomoAnalysis::FLMaskGenerator::Entity {
    struct WorkingSet::Impl {
        QString workingDir;
        QString hypercubeName;
        QStringList imagePath;
        QString curImagePath;
        QString playgroundPath;
                
        QString maskPath;
        QString measurePath;        

        QList<double> ht_tps;
        QList<double> fl_tps[3];
        double cur_time_point{ 0.0 };
        bool flSelected[3]{ false,false,false };
        
        TCImage::Pointer htImage{ nullptr };
        TCImage::Pointer flImage[3]{nullptr,};
        QString flName[3] = {"CH1","CH2","CH3"};
        bool flExist[3] = { false, };
        IBaseMask::Pointer mask[3]{nullptr,nullptr,nullptr};
        TCMeasure::Pointer measure{nullptr};

        IParameter::Pointer param{nullptr};
                
        float baselineRI;
        float RII[3];
    };
    WorkingSet::WorkingSet() : d{ new Impl } {
        
    }
    WorkingSet::~WorkingSet() {
        
    }
    auto WorkingSet::GetInstance() -> Pointer {
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }
    auto WorkingSet::GetBaselineRI() -> float {
        return d->baselineRI;
    }
    auto WorkingSet::SetBaselineRI(const float& BRI) -> void {
        d->baselineRI = BRI;
    }
    auto WorkingSet::GetRII(const int& chIdx) -> float {
        return d->RII[chIdx];
    }
    auto WorkingSet::SetRII(const int& chIdx, const float& RII) -> void {
        d->RII[chIdx] = RII;
    }
    auto WorkingSet::SetFLSelected(const int& chIdx, bool selected) -> void {
        d->flSelected[chIdx] = selected;
    }
    auto WorkingSet::GetFLSelected(const int& chIdx) -> bool {
        return d->flSelected[chIdx];
    }
    auto WorkingSet::GetPlayground() -> QString {
        return d->playgroundPath;
    }
    auto WorkingSet::SetPlayground(const QString& pg) -> bool {
        d->playgroundPath = pg;
        return true;
    }
    auto WorkingSet::GetWorkingDir()-> QString {
        return d->workingDir;
    }
    auto WorkingSet::SetWorkingDir(const QString& path) -> bool {
        QDir wDir(path);
        if(false == wDir.exists()) {            
            const auto success = QDir().mkpath(path);
            if(false == success) {
                return false;
            }
        }
        d->workingDir = path;
        return true;
    }

    auto WorkingSet::GetHypercubeName()-> QString {
        return d->hypercubeName;
    }
    auto WorkingSet::SetHypercubeName(const QString& name) -> bool {
        if(name.isEmpty()) {
            return false;
        }
        d->hypercubeName = name;
        return true;
    }
    auto WorkingSet::GetImageList()-> QStringList {
        return d->imagePath;
    }
    auto WorkingSet::SetImageList(const QStringList& imageList) -> bool {
        if(imageList.count()<1) {
            return false;
        }
        for(auto i=0;i<imageList.count();i++) {
            QFileInfo fileInfo(imageList[i]);
            if(false == fileInfo.exists()) {
                return false;
            }
        }
        d->imagePath = imageList;
        return true;
    }
    auto WorkingSet::AppendImage(const QString& imagePath) -> bool {
        if(imagePath.isEmpty()) {
            return false;
        }
        QFileInfo fileInfo(imagePath);
        if(false == fileInfo.exists()) {
            return false;
        }
        d->imagePath.append(imagePath);
        return true;
    }
    auto WorkingSet::GetCurrentImage() -> QString {
        return d->curImagePath;
    }
    auto WorkingSet::SetCurrentImage(const QString& path) -> bool {
        if(path.isEmpty()) {
            return false;
        }
        QFileInfo fileInfo(path);
        if(false == fileInfo.exists()) {
            return false;
        }
        d->curImagePath = path;
        return true;
    }
    auto WorkingSet::SetCurrentImage(const int& idx) -> bool {
        if(d->imagePath.count() <= idx) {
            return false;
        }        
        d->curImagePath = d->imagePath[idx];
        return true;
    }    
    auto WorkingSet::SetCurTimePoint(const double& step) -> bool {
        if(step < 0) {
            return false;
        }
        if(false == d->ht_tps.contains(step)) {
            return false;
        }
        d->cur_time_point = d->ht_tps.indexOf(step);

        return true;
    }
    auto WorkingSet::GetCurTimePoint() -> double {
        return d->cur_time_point;
    }
    auto WorkingSet::SetFLTimePoints(const QList<double>& tps, const int& chIdx) -> bool {
        d->fl_tps[chIdx] = tps;
        return true;
    }
    auto WorkingSet::GetFLTimePoints(const int& chIdx) -> QList<double> {
        return d->fl_tps[chIdx];
    }
    auto WorkingSet::GetTimePoints() -> QList<double> {
        return d->ht_tps;
    }
    auto WorkingSet::SetTimePoints(const QList<double>& tps) -> bool {
        d->ht_tps = tps;
        return true;
    }
    auto WorkingSet::SetHTImage(TCImage::Pointer htImage) -> void {
        d->htImage = htImage;
    }
    auto WorkingSet::GetHTImage() -> TCImage::Pointer {
        return d->htImage;
    }
    auto WorkingSet::SetFLImage(TCImage::Pointer flImage,const int& chIdx) -> bool {
        if(chIdx < 0 || chIdx > 2) {
            return false;
        }
        d->flImage[chIdx] = flImage;
        d->flExist[chIdx] = true;
        return true;
    }
    auto WorkingSet::GetFLImage(const int& chIdx) -> TCImage::Pointer {
        if(chIdx < 0 || chIdx > 2) {
            return nullptr;
        }
        if(false == d->flExist[chIdx]) {
            return nullptr;
        }
        return d->flImage[chIdx];
    }
    auto WorkingSet::SetFLName(const QString& name, const int& chIdx) -> bool {
        if(chIdx < 0 || chIdx > 2) {
            return false;
        }
        if(name.isEmpty()) {
            return false;
        }
        d->flName[chIdx] = name;
        return true;
    }
    auto WorkingSet::GetFLName(const int& chIdx) -> QString {
        if(chIdx < 0 || chIdx > 2) {
            return false;
        }
        return d->flName[chIdx];
    }
    auto WorkingSet::SetMeasure(TCMeasure::Pointer measure) -> void {
        d->measure = measure;
    }
    auto WorkingSet::GetMeasure() -> TCMeasure::Pointer {
        return d->measure;
    }
    auto WorkingSet::SetMaskPath(const QString& path) -> bool {
        if(path.isEmpty()) {
            return false;
        }
        d->maskPath = path;
        return true;
    }
    auto WorkingSet::GetMaskPath() -> QString {
        return d->maskPath;
    }
    auto WorkingSet::GetMeasurePath() -> QString {
        return d->measurePath;
    }
    auto WorkingSet::SetMeasurePath(const QString& path) -> bool {
        if(path.isEmpty()) {
            return false;
        }
        d->measurePath = path;
        return true;
    }
    auto WorkingSet::GetMask(int ch) -> IBaseMask::Pointer {
        return d->mask[ch];
    }
    auto WorkingSet::SetMask(IBaseMask::Pointer mask,int ch) -> void {
        d->mask[ch] = mask;
    }
    auto WorkingSet::GetParameter() -> IParameter::Pointer {
        return d->param;
    }
    auto WorkingSet::SetParameter(IParameter::Pointer param) -> void {
        d->param = param;
    }
    auto WorkingSet::Clear() -> void {
        d.reset(new Impl);
    }
    auto WorkingSet::ClearData() -> void {
        for (auto i = 0; i < 3; i++) {
            d->flExist[i] = false;
            d->flImage[i] = nullptr;
            //d->flName[i] = QString("Ch%1").arg(i+1);
            d->fl_tps[i].clear();
            d->mask[i] = nullptr;                        
            d->RII[i] = 0.19f;
        }
        //d->baselineRI = 1.337;
        d->measure = nullptr;
        d->htImage = nullptr;
        d->ht_tps.clear();                
    }

}