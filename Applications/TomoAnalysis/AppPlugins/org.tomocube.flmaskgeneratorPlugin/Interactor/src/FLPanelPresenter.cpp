#include <iostream>

#include <QJsonArray>
#include <QFileInfo>

#include "IFLDataManagerPanel.h"
#include "IFLVizControlPanel.h"
#include "IFLNavigatorPanel.h"
#include "IFLParameterPanel.h"
#include "IFLResultPanel.h"

#include "FLPanelPresenter.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLPanelPresenter::Impl {
        IDataManagerPanel* datamanager{ nullptr };
        IVizControlPanel* vizcontrol{ nullptr };
        INavigatorPanel* navigator{ nullptr };
        IParameterPanel* paramcontrol{ nullptr };
        IResultPanel* result{ nullptr };
    };
    FLPanelPresenter::FLPanelPresenter(IDataManagerPanel* datamanager, INavigatorPanel* navigator, IParameterPanel* paramcontrol, IVizControlPanel* vizcontrol,IResultPanel* result) : d{ new Impl } {
        d->datamanager = datamanager;
        d->vizcontrol = vizcontrol;
        d->navigator = navigator;
        d->paramcontrol = paramcontrol;
        d->result = result;
    }
    FLPanelPresenter::~FLPanelPresenter() {
        
    }
    auto FLPanelPresenter::UpdateMeasure() -> void {
        if(nullptr != d->result) {
            auto rds = d->result->GetResultDS();
            auto ws = Entity::WorkingSet::GetInstance();
            rds->measure = ws->GetMeasure();
            d->result->Update();
        }
        if(nullptr != d->datamanager) {
            auto dds = d->datamanager->GetDataManagerDS();
            dds->visited[dds->cur_idx] = true;
            d->datamanager->Update();
        }
    }
    auto FLPanelPresenter::UpdateMask(int selection) -> void {
        if(nullptr != d->vizcontrol) {
            auto vds = d->vizcontrol->GetVizControlDS();
            vds->showChMaskIdx = selection;
            d->vizcontrol->Modify();
        }
    }
    auto FLPanelPresenter::ChangeSlice(int xIdx, int yIdx,int zIdx) -> void {
        if(nullptr != d->navigator) {
            auto naviDS = d->navigator->GetNavigatorDS();
            if(xIdx > -1 && xIdx < naviDS->rangeX) {
                naviDS->x = xIdx;
            }
            if(yIdx > -1 && yIdx < naviDS->rangeY) {
                naviDS->y = yIdx;
            }
            if(zIdx > -1 && zIdx < naviDS->rangeZ) {
                naviDS->z = zIdx;
            }            
            d->navigator->Refresh();
        }
    }
    auto FLPanelPresenter::UpdateChannel(bool ch1Exist, bool ch2Exist, bool ch3Exist) -> void {        
        if (nullptr != d->vizcontrol) {
            auto vizDS = d->vizcontrol->GetVizControlDS();
            vizDS->chSelected[0] = ch1Exist;
            vizDS->chSelected[1] = ch2Exist;
            vizDS->chSelected[2] = ch3Exist;
            d->vizcontrol->Update();
        }        
        if (nullptr != d->paramcontrol) {
            auto paramDS = d->paramcontrol->GetParameterDS();
            paramDS->chSelected[0] = ch1Exist;            
            paramDS->chSelected[1] = ch2Exist;            
            paramDS->chSelected[2] = ch3Exist;
            paramDS->chShow[0] = ch1Exist;
            paramDS->chShow[1] = ch2Exist;
            paramDS->chShow[2] = ch3Exist;
            d->paramcontrol->Update();
        }        
	}
	auto FLPanelPresenter::UpdateFileList(const QStringList& path, const QStringList& cubes, const QString& hyper,const int& idx,const double& time_point) -> void {        
        auto workingset = Entity::WorkingSet::GetInstance();        
        if (nullptr != d->datamanager) {
            auto dataDS = d->datamanager->GetDataManagerDS();
            if (idx == -1) {
                //initialize batch run workspace with files                            
                dataDS->tcfList = path;                
                dataDS->cubeList = cubes;
                dataDS->hyperName = hyper;
                dataDS->playgroundPath = workingset->GetPlayground();
                dataDS->cur_idx = -1;
                d->datamanager->Update();
            }
            else {                
                //dataDS->visited[idx] = true;                
                dataDS->cur_idx = idx;
                d->datamanager->Modified(time_point);
            }
        }        
        if(nullptr != d->navigator) {
            auto naviDS = d->navigator->GetNavigatorDS();
            auto refImg = workingset->GetHTImage();
            if(nullptr != refImg) {
                naviDS->rangeX = std::get<0>(refImg->GetSize());
                naviDS->rangeY = std::get<1>(refImg->GetSize());
                naviDS->rangeZ = std::get<2>(refImg->GetSize());
                double res[3];
                refImg->GetResolution(res);
                naviDS->resX = res[0];
                naviDS->resY = res[1];
                naviDS->resZ = res[2];
                naviDS->x = naviDS->rangeX / 2;
                naviDS->y = naviDS->rangeY / 2;
                naviDS->z = naviDS->rangeZ / 2;

                d->navigator->Update();
            }
        }
        if(nullptr != d->paramcontrol) {
            auto paramDS = d->paramcontrol->GetParameterDS();
            if(idx != -1) {                
                for(auto i=0;i<3;i++) {
                    paramDS->chExist[i] = (nullptr != workingset->GetFLImage(i));
                    if(paramDS->chExist[i]) {
                        auto fmin = std::get<0>(workingset->GetFLImage(i)->GetMinMax());
                        auto fmax = std::get<1>(workingset->GetFLImage(i)->GetMinMax());
                        paramDS->chMinMax[i][0] = fmin;
                        paramDS->chMinMax[i][1] = fmax;                        
                    }
                }                
                d->paramcontrol->Update();
            }else {
                for (auto i = 0; i < 3; i++) {
                    paramDS->curMinMax[i][0] = 0;
                    paramDS->curMinMax[i][1] = 200;
                }
            }
        }
        if(nullptr != d->vizcontrol) {
            if(idx!= -1) {
                auto vizDS = d->vizcontrol->GetVizControlDS();
                for(auto i=0;i<3;i++) {
                    vizDS->chExist[i] = (nullptr != workingset->GetFLImage(i));                    
                }
                vizDS->showChMaskIdx = -1;
                d->vizcontrol->Update();
            }
        }
	}
}
