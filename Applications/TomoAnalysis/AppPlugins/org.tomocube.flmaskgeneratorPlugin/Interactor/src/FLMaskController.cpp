#include <FLMGWorkingSet.h>

#include <ManageScene.h>
#include <ExecuteAlgorithm.h>

#include "FLMaskController.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLMaskController::Impl {
        UseCase::IFLScenePort* sport;
        UseCase::IFLPanelPort* pport;
        UseCase::IMaskWriterPort* port;
        UseCase::IMeasureWriterPort* mport;
        UseCase::IProcessingPort* engine;
    };
    FLMaskController::FLMaskController(UseCase::IFLScenePort* sport, UseCase::IFLPanelPort* pport, UseCase::IMaskWriterPort* port, UseCase::IMeasureWriterPort* mport,UseCase::IProcessingPort* engine) :d{ new Impl } {
        d->sport = sport;
        d->pport = pport;
        d->port = port;
        d->mport = mport;
        d->engine = engine;
    }
    
    FLMaskController::~FLMaskController() {
        
    }
    auto FLMaskController::CreateInternalMask(int ch, int min, int max) -> bool {
        if (nullptr == d->engine) {
            return false;
        }
        UseCase::ExecuteAlgorithm useCase;
        return useCase.CreateFLThreshMask(nullptr, nullptr, d->engine, ch, min, max);
    }
    auto FLMaskController::CreateMask(int ch,int min,int max) -> bool {
        if(nullptr == d->sport) {
            return false;
        }
        if(nullptr == d->pport) {
            return false;
        }
        if(nullptr == d->engine) {
            return false;
        }
        UseCase::ExecuteAlgorithm useCase;
        return useCase.CreateFLThreshMask(d->sport,d->pport,d->engine,ch,min,max);
    }
    auto FLMaskController::ChangeMemoryMask(int ch) -> bool {
        if(nullptr == d->sport) {
            return false;
        }
        UseCase::ManageScene useCase;
        return useCase.ChangeFLMask(d->sport,ch);
    }
    auto FLMaskController::PerformInternalMeasure() -> bool {
        if (nullptr == d->port) {
            return false;
        }
        if (nullptr == d->mport) {
            return false;
        }
        if (nullptr == d->engine) {
            return false;
        }
        UseCase::ExecuteAlgorithm useCase;
        return useCase.PerformMeasurement(nullptr, d->engine, d->port, d->mport);
    }
    auto FLMaskController::PerformMeasure() -> bool {
        if(nullptr == d->pport) {
            return false;
        }
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr == d->mport) {
            return false;
        }
        if(nullptr == d->engine) {
            return false;
        }
        UseCase::ExecuteAlgorithm useCase;
        return useCase.PerformMeasurement(d->pport, d->engine, d->port, d->mport);
    }
    auto FLMaskController::SaveCurrentMask() -> bool {        
        if(nullptr == d->port) {
            return false;
        }        
        auto workingset = Entity::WorkingSet::GetInstance();        
        
        for(auto i=0;i<3;i++) {
            auto mask = workingset->GetMask(i);
            if(nullptr != mask) {
                auto tcmask = std::dynamic_pointer_cast<TCMask>(mask);
                d->port->Write(tcmask, workingset->GetMaskPath(), workingset->GetFLName(i), i);
            }
        }
        return true;
    }

}