#include "IFLDataManagerPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct IDataManagerPanel::Impl {
        DataManagerDS::Pointer datamanagerDS = std::make_shared<DataManagerDS>();
    };
    IDataManagerPanel::IDataManagerPanel() : d{ new Impl } {
        
    }
    IDataManagerPanel::~IDataManagerPanel() {       

    }
    auto IDataManagerPanel::GetDataManagerDS() const -> DataManagerDS::Pointer {
        return d->datamanagerDS;
    }
}