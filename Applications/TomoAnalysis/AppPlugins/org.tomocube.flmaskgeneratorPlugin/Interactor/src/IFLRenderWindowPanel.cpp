#include "IFLRenderWindowPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct IRenderWindowPanel::Impl {
        RenderWindowDS::Pointer renderwindowDS = std::make_shared<RenderWindowDS>();
    };
    IRenderWindowPanel::IRenderWindowPanel() : d{ new Impl } {
        
    }
    IRenderWindowPanel::~IRenderWindowPanel() {
        
    }
    auto IRenderWindowPanel::GetRenderWindowDS() const -> RenderWindowDS::Pointer {
        return d->renderwindowDS;
    }
}