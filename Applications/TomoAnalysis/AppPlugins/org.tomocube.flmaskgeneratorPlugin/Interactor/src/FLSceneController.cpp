#include <iostream>
#include <QJsonArray>
#include <QDir>

//UseCases
#include <ManageScene.h>
#include <LoadDataSet.h>

#include "FLSceneController.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLSceneController::Impl {
        UseCase::IFLPanelPort* port;
        UseCase::IFLScenePort* dport;
        UseCase::IImageReaderPort* ireaderPort;
        UseCase::IMaskReaderPort* mreaderPort;        
        UseCase::IMeasureReaderPort* rreaderPort;        
    };
    FLSceneController::FLSceneController(UseCase::IFLPanelPort* port, UseCase::IFLScenePort* dport, UseCase::IImageReaderPort* ireader, UseCase::IMaskReaderPort* mreader, UseCase::IMeasureReaderPort* rreader) : d{ new Impl } {
        d->port = port;
        d->dport = dport;
        d->ireaderPort = ireader;        
        d->mreaderPort = mreader;        
        d->rreaderPort = rreader;        
    }

    FLSceneController::~FLSceneController() {
        
    }

    auto FLSceneController::InitBatchRunControlUI(IParameter::Pointer param) -> bool {
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr ==param) {
            return false;
        }
                
        UseCase::ManageScene useCase;
        return useCase.UpdateBatchControlUI(d->port,param);
    }
    auto FLSceneController::FLRangeChanged(int ch, int min, int max) -> bool {
        if(nullptr == d->dport) {
            return false;
        }        
        UseCase::ManageScene useCase;
        return useCase.ChangeFLDataRange(d->dport, ch, min, max);
    }
    auto FLSceneController::OpenDataSet() -> bool {
        auto workingset = Entity::WorkingSet::GetInstance();
        auto tcfs = workingset->GetImageList();
        auto playground = workingset->GetPlayground();
        auto hyper = workingset->GetHypercubeName();
        if(nullptr == d->port) {            
            return false;
        }
        if(tcfs.count()<1) {            
            return false;
        }
        if(playground.isEmpty()) {            
            return false;
        }
        if(hyper.isEmpty()) {            
            return false;
        }
        UseCase::LoadDataSet useCase;
        return useCase.LoadTcfPaths(d->port,tcfs,playground,hyper);
    }
    auto FLSceneController::ImageSliceChanged(int viewIndex, int sliceIndex) -> bool {
        if(nullptr == d->port) {
            return false;
        }        
        UseCase::ManageScene useCase;
        return useCase.ChangeSliceIndex(d->port,viewIndex,sliceIndex);
    }
    auto FLSceneController::ImageSliceChanged(int xIdx, int yIdx, int zIdx) -> bool {
        if(nullptr == d->port) {
            return false;
        }
        UseCase::ManageScene useCase;
        return useCase.ChangeSliceIndex(d->port, xIdx, yIdx, zIdx);
    }
    auto FLSceneController::ChangeInternalDataIndex(const int& index, const double& time_point) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto path = ws->GetImageList()[index];
        if(nullptr == d->ireaderPort) {
            return false;
        }
        if (nullptr == d->mreaderPort) {
            return false;
        }
        if (nullptr == d->rreaderPort) {
            return false;
        }
        UseCase::LoadDataSet useCase;
        return useCase.SetCurrentTcf(nullptr, nullptr, d->ireaderPort, d->mreaderPort, d->rreaderPort, path, index, time_point);
    }
    auto FLSceneController::ChangeDataIndex(const int& index,const double& time_point) -> bool {
        auto workingset = Entity::WorkingSet::GetInstance();        
        auto path = workingset->GetImageList()[index];        
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr == d->dport) {
            return false;
        }
        if(nullptr == d->ireaderPort) {
            return false;
        }
        if(nullptr == d->mreaderPort) {
            return false;
        }        
        if(nullptr == d->rreaderPort) {
            return false;
        }
        if(index < 0) {
            return false;
        }       
        UseCase::LoadDataSet useCase;        
        return useCase.SetCurrentTcf(d->port,d->dport,d->ireaderPort,d->mreaderPort,d->rreaderPort,path,index,time_point);
    }    
}
