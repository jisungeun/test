#include "IFLVizControlPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct IVizControlPanel::Impl {
        VizControlDS::Pointer vizcontrolDS = std::make_shared<VizControlDS>();
    };
    IVizControlPanel::IVizControlPanel() : d{ new Impl } {
        
    }
    IVizControlPanel::~IVizControlPanel() {
        
    }
    auto IVizControlPanel::GetVizControlDS() const -> VizControlDS::Pointer {
        return d->vizcontrolDS;
    }
}