#include "IFLParameterPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct IParameterPanel::Impl {
        ParameterDS::Pointer parameterDS = std::make_shared<ParameterDS>();        
    };
    IParameterPanel::IParameterPanel() : d{ new Impl } {
        
    }
    IParameterPanel::~IParameterPanel() {
        
    }
    auto IParameterPanel::GetParameterDS() const -> ParameterDS::Pointer {
        return d->parameterDS;
    }
}