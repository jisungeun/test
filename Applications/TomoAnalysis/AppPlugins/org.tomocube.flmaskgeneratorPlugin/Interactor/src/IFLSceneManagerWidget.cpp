#include "IFLSceneManagerWidget.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct ISceneManagerWidget::Impl {
        SceneManagerDS::Pointer scenemanagerDS = std::make_shared<SceneManagerDS>();
    };
    ISceneManagerWidget::ISceneManagerWidget() : d{ new Impl } {
        
    }
    ISceneManagerWidget::~ISceneManagerWidget() {
        
    }
    auto ISceneManagerWidget::GetSceneManagerDS() const -> SceneManagerDS::Pointer {
        return d->scenemanagerDS;
    }
}