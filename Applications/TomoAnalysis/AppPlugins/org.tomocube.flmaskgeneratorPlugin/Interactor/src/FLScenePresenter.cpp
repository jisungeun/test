#include "FLScenePresenter.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLScenePresenter::Impl {        
        IRenderWindowPanel* renderwindow{ nullptr };
        ISceneManagerWidget* sceneWidget{ nullptr };
        IParameterPanel* paramPanel{ nullptr };
    };

    FLScenePresenter::FLScenePresenter(IRenderWindowPanel* render,ISceneManagerWidget* widget,IParameterPanel* param) : d{ new Impl } {
        d->renderwindow = render;
        d->sceneWidget = widget;
        d->paramPanel = param;
    }
    FLScenePresenter::~FLScenePresenter() {
        
    }
    auto FLScenePresenter::Update() -> void {
        //TODO
    }

    auto FLScenePresenter::ChangeFLRange(int ch, int min, int max) -> void {
        auto sceneDS = d->sceneWidget->GetSceneManagerDS();
        sceneDS->fl_range[ch][0] = min;
        sceneDS->fl_range[ch][1] = max;

        auto paramDS = d->paramPanel->GetParameterDS();
        paramDS->curMinMax[ch][0] = min;
        paramDS->curMinMax[ch][1] = max;

        d->sceneWidget->Update();
    }

    auto FLScenePresenter::LoadImage(IBaseImage::Pointer ht_image, IBaseImage::Pointer fl_image1, IBaseImage::Pointer fl_image2, IBaseImage::Pointer fl_image3,float fl_offset) -> void {
        auto sceneDS = d->sceneWidget->GetSceneManagerDS();
        sceneDS->ht_image = ht_image;
        sceneDS->fl_image[0] = fl_image1;
        sceneDS->fl_image[1] = fl_image2;
        sceneDS->fl_image[2] = fl_image3;
        sceneDS->fl_offset = fl_offset;                

        auto paramDS = d->paramPanel->GetParameterDS();
        for(auto i=0;i<3;i++) {
            sceneDS->fl_range[i][0] = paramDS->curMinMax[i][0];
            sceneDS->fl_range[i][1] = paramDS->curMinMax[i][1];            
        }                
        
        d->sceneWidget->UpdateImage();
        if (nullptr != d->renderwindow) {
            auto tcimage = std::dynamic_pointer_cast<TCImage>(ht_image);
            auto renderDS = d->renderwindow->GetRenderWindowDS();
            renderDS->htMin = std::get<0>(tcimage->GetMinMax());
            renderDS->htMax = std::get<1>(tcimage->GetMinMax());
            d->renderwindow->Update();
        }
        
    }
    auto FLScenePresenter::LoadMask(IBaseMask::Pointer fl_mask) -> void {
        auto sceneDS = d->sceneWidget->GetSceneManagerDS();
        sceneDS->fl_mask = fl_mask;

        d->sceneWidget->UpdateMask();
        d->renderwindow->Render();
    }

}