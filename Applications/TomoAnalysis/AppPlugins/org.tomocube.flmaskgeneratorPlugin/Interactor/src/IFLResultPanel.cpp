#include "IFLResultPanel.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct IResultPanel::Impl {
        ResultDS::Pointer resultDS = std::make_shared<ResultDS>();
    };
    IResultPanel::IResultPanel() : d{ new Impl } {
        
    }
    IResultPanel::~IResultPanel() {
        
    }
    auto IResultPanel::GetResultDS() const -> ResultDS::Pointer {
        return d->resultDS;
    }
}