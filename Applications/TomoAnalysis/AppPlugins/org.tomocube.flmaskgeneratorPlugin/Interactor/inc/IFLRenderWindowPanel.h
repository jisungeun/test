#pragma once

#include <memory>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
	struct FLMaskGeneratorInteractor_API RenderWindowDS {
		typedef std::shared_ptr<RenderWindowDS> Pointer;
		int htMin;
		int htMax;
	};
	class FLMaskGeneratorInteractor_API IRenderWindowPanel {
	public:
		IRenderWindowPanel();
		virtual ~IRenderWindowPanel();

		auto GetRenderWindowDS()const->RenderWindowDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Render()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}