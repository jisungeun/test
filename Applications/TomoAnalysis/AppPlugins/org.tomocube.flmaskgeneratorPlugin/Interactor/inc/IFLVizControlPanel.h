#pragma once

#include <memory>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
	struct FLMaskGeneratorInteractor_API VizControlDS {
		typedef std::shared_ptr<VizControlDS> Pointer;
		bool chExist[3]{ false,false,false };
		bool chSelected[3]{ false,false,false };
		int showChMaskIdx{ -1 };
	};
	class FLMaskGeneratorInteractor_API IVizControlPanel {
	public:
		IVizControlPanel();
		virtual ~IVizControlPanel();

		auto GetVizControlDS()const->VizControlDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Modify()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}