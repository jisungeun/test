#pragma once

#include <memory>

#include <IFLMaskWriterPort.h>
#include <IFLMeasureWriterPort.h>

#include <IFLPanelPort.h>
#include <IFLScenePort.h>
#include <IFLProcessingPort.h>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    class FLMaskGeneratorInteractor_API FLMaskController {
    public:
        FLMaskController(UseCase::IFLScenePort* sport=nullptr,UseCase::IFLPanelPort* pport =nullptr,UseCase::IMaskWriterPort* port = nullptr, UseCase::IMeasureWriterPort* mport = nullptr,UseCase::IProcessingPort* engine = nullptr);
        virtual ~FLMaskController();

        auto SaveCurrentMask()->bool;
        auto CreateInternalMask(int ch, int min, int max)->bool;
        auto CreateMask(int ch,int min,int max)->bool;
        auto PerformInternalMeasure()->bool;
        auto PerformMeasure()->bool;
        auto ChangeMemoryMask(int ch)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}