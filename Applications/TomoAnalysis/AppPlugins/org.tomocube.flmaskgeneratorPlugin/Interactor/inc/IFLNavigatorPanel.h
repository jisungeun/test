#pragma once

#include <memory>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
	struct FLMaskGeneratorInteractor_API NavigatorDS {
		int rangeX = 1;
		int rangeY = 1;
		int rangeZ = 1;

		int x = 0;
		int y = 0;
		int z = 0;

		float resX = 0.f;
		float resY = 0.f;
		float resZ = 0.f;

		typedef std::shared_ptr<NavigatorDS> Pointer;
	};
	class FLMaskGeneratorInteractor_API INavigatorPanel {
	public:
		INavigatorPanel();
		virtual ~INavigatorPanel();

		auto GetNavigatorDS()const->NavigatorDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}