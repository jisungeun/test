#pragma once

#include <memory>

#include <IBaseImage.h>
#include <IBaseMask.h>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLMaskGeneratorInteractor_API SceneManagerDS {
        typedef std::shared_ptr<SceneManagerDS> Pointer;
        IBaseImage::Pointer ht_image;
        IBaseImage::Pointer fl_image[3];
        IBaseMask::Pointer fl_mask;
        float fl_offset{ 0.0 };
        int fl_range[3][2]{ {0,200},{0,200},{0,200} };
    };
    class FLMaskGeneratorInteractor_API ISceneManagerWidget {
    public:
        ISceneManagerWidget();
        virtual ~ISceneManagerWidget();

        auto GetSceneManagerDS()const->SceneManagerDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto UpdateImage()->bool = 0;
        virtual auto UpdateMask()->bool = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}