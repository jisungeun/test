#pragma once

#include <memory>

#include <IFLPanelPort.h>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    class IDataManagerPanel;
    class INavigatorPanel;
    class IParameterPanel;    
    class IVizControlPanel;
    class IResultPanel;
    class FLMaskGeneratorInteractor_API FLPanelPresenter : public UseCase::IFLPanelPort {
    public:
        FLPanelPresenter(IDataManagerPanel* datamanager = nullptr, INavigatorPanel* navigator = nullptr, IParameterPanel* paramcontrol = nullptr, IVizControlPanel* vizcontrol = nullptr,IResultPanel* result=nullptr);
        virtual ~FLPanelPresenter();

        auto UpdateMeasure() -> void override;
        auto UpdateMask(int selection) -> void override;
        auto ChangeSlice(int xIdx, int yIdx,int zIdx) -> void override;
        auto UpdateChannel(bool ch1Exist, bool ch2Exist, bool ch3Exist) -> void override;
        auto UpdateFileList(const QStringList& path, const QStringList& cubes, const QString& hyper,const int& idx,const double& time_point) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}