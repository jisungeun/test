#pragma once

#include <memory>

#include <TCMeasure.h>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    struct FLMaskGeneratorInteractor_API ResultDS {
        typedef std::shared_ptr<ResultDS> Pointer;
        TCMeasure::Pointer measure{ nullptr };
    };
    class FLMaskGeneratorInteractor_API IResultPanel {
    public:
        IResultPanel();
        virtual ~IResultPanel();

        auto GetResultDS()const->ResultDS::Pointer;

        virtual auto Update()->bool = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}