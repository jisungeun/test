#pragma once

#include <memory>

#include <IParameter.h>
#include <IFLScenePort.h>
#include <IFLPanelPort.h>
#include <IFLImageReaderPort.h>
#include <IFLMaskReaderPort.h>
#include <IFLMeasureReaderPort.h>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    class FLMaskGeneratorInteractor_API FLSceneController {
    public:
        FLSceneController(UseCase::IFLPanelPort* port=nullptr,UseCase::IFLScenePort* dport=nullptr,UseCase::IImageReaderPort* ireader = nullptr,UseCase::IMaskReaderPort* mreader = nullptr, UseCase::IMeasureReaderPort* rreader = nullptr);
        virtual ~FLSceneController();

        auto InitBatchRunControlUI(IParameter::Pointer param)->bool;
        auto OpenDataSet()->bool;
        auto FLRangeChanged(int ch, int min, int max)->bool;
        auto ChangeInternalDataIndex(const int& index, const double& time_point)->bool;
        auto ChangeDataIndex(const int& index,const double& time_point)->bool;
        auto ImageSliceChanged(int viewIndex, int sliceIndex)->bool;
        auto ImageSliceChanged(int xIdx, int yIdx, int zIdx)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}