#pragma once

#include <memory>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
	struct FLMaskGeneratorInteractor_API ParameterDS {	
		typedef std::shared_ptr<ParameterDS> Pointer;		
		bool chExist[3] = { false,false,false };
		bool chSelected[3] = { false,false,false };
		int chMinMax[3][2]{ {0,255},{0,255},{0,255} };
		int curMinMax[3][2]{ {0,200},{0,200},{0,200} };
		bool chShow[3] = { false,false,false };
	};
	class FLMaskGeneratorInteractor_API IParameterPanel {
	public:
		IParameterPanel();
		virtual ~IParameterPanel();

		auto GetParameterDS()const->ParameterDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}