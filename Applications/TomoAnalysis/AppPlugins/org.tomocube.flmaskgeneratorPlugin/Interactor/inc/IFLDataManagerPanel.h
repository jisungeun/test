#pragma once

#include <memory>

#include <QStringList>

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
	struct FLMaskGeneratorInteractor_API DataManagerDS {
		typedef std::shared_ptr<DataManagerDS> Pointer;
		QList<int> tcfSteps;
		QStringList tcfList;
		QStringList cubeList;
		QString hyperName;
		QString playgroundPath;
		QList<bool> visited;
		int cur_idx;		
	};
	class FLMaskGeneratorInteractor_API IDataManagerPanel {
	public:
		IDataManagerPanel();
		virtual ~IDataManagerPanel();

		auto GetDataManagerDS()const->DataManagerDS::Pointer;

		virtual auto Update()->bool = 0;		
		virtual auto Modified(double tp)->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}