#pragma once

#include <memory>

#include <IFLScenePort.h>

#include "IFLRenderWindowPanel.h"
#include "IFLSceneManagerWidget.h"
#include "IFLParameterPanel.h"

#include "FLMaskGeneratorInteractorExport.h"

namespace TomoAnalysis::FLMaskGenerator::Interactor {
    class FLMaskGeneratorInteractor_API FLScenePresenter : public UseCase::IFLScenePort {
    public:
        FLScenePresenter(IRenderWindowPanel* render= nullptr,ISceneManagerWidget* widget =nullptr,IParameterPanel* panel = nullptr);
        virtual ~FLScenePresenter();

        auto Update() -> void override;        
        auto LoadImage(IBaseImage::Pointer ht_image, IBaseImage::Pointer fl_image1, IBaseImage::Pointer fl_image2, IBaseImage::Pointer fl_image3,float fl_offset) -> void override;
        auto LoadMask(IBaseMask::Pointer fl_mask) -> void override;
        auto ChangeFLRange(int ch, int min, int max) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}