#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::FLMaskGenerator::AppUI {
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto Execute(const QVariantMap& params)->bool override;
        auto GetRunType() -> std::tuple<bool, bool> override;                
        auto TryActivate() -> bool override;
        auto TryDeactivate() -> bool override;
        auto IsActivate() -> bool override;
        auto GetMetaInfo() -> QVariantMap override;

        auto ForceClosePopup()->void;

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

        static void* ThreadFunc(void* userData);

    signals:
        void batchStepChanged(int);
        void batchFinished(int);
        void batchStarted();
    protected:
        void resizeEvent(QResizeEvent* event) override;
        auto BroadcastFinishBatch()->void;
        auto BroadcastAbortBatch()->void;
        auto CopyPath(QString src, QString dst)->void;

    protected slots:
        void OnTabFocused(ctkEvent);

        void OnAcceptValidity();
        void OnRejectValidity();

        void DmOpenTcfbyIndex(int,double);
        void DmPerformAll();

        void RwSliceIndexChanged(int, int);
        void RwSlicePickChanged(float, float, float);
        void RwGlobalPosChanged(int, int,int);

        void NvSliceIndexChanged(int, int, int);

        void PaFLMinMaxChanged(int ch, int min, int max);
        void PaPerformMask(int ch,int min,int max);
        void PaPerformMeasure();
        void PaOpaChanged(int ch, float opa);
        void PaVizToggled(int ch, bool viz);
        void PaNameChanged(int ch, QString);
        void PaToggleResult(bool);
        void PaChRII(int ch, int idx);
        void PaChRIIValue(int ch, double val);
        void PaCustomRI(bool isCustom);
        void PaRIValue(double val);
        void PaMaskCorrection();

        void RsCloseEvent();

        void VcFLMaskViz(bool, bool, bool);
        void VcFLMaskOpa(float);

        void OnBatchStepStarted();
        void OnBatchCanceled();
        void OnBatchStepChanged(int step);
        void OnBatchFinished(int result_step);    

        auto ExecuteBatchRun(const QVariantMap& params)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}