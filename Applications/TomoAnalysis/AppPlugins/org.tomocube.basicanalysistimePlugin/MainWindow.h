#pragma once
#include <QMutexLocker>

#include <Inventor/threads/SbThread.h>

#include <IMainWindowTA.h>

#include <BaTimeProcessor.h>
#include <WorkingSet.h>

#include "ILicensed.h"

namespace TomoAnalysis::BasicAnalysisTime::AppUI {
    class WorkerThread : public QObject {
        Q_OBJECT
    public:
        explicit WorkerThread(QObject* parent = nullptr);
        virtual ~WorkerThread();

        auto SetPGPath(const QString& path)->void;
        auto SetProc(const QString& proc)->void;
        auto SetTcfs(const QStringList& tcfs)->void;
        auto SetCancel()->void;
        auto SetWorkingSet(Entity::WorkingSet::Pointer ws)->void;
        auto SetTimePoints(QList<QList<int>> tps)->void;
        auto SetParameter(IParameter::Pointer param)->void;

    public slots:
        void doWork();
        void emitStart();
    signals:
        void start();
        void batchTimeChanged(int);
        void batchTimeStarted(int, int);
        void batchFinished(int);
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto GetCurTCF()const->QString;
        
        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto GetRunType() -> std::tuple<bool, bool> override;
        auto ForceClosePopup()->void;
        auto Execute(const QVariantMap& params)->bool override;

        auto TryDeactivate() -> bool final;
        auto TryActivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo() -> QVariantMap final;

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

    signals:
        void singleTimeChanged(int);
        void singleTimeStarted(int);
        void singleFinished(int);

        void batchTimeChanged(int);
        void batchTimeStarted(int,int);
        void batchFinished(int);

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void HandleProcValChange(double, QString);
        void HandleProcChange(QString);
        void HandleTimeChange(int);
        void HandleAlgoExecution(QString,QString);        
        //void HandleMaskSelection(QString);

        void OnAcceptValidity();
        void OnRejectValidity();
        void OnAcceptBatchRun();
        void OnRejectBatchRun();
        void HandleWholeExecution();
        void HandleSingleCSV();
        void HandleCloseResult();
        void HandleRefreshHighlight();

        void HandleToggleResult(bool);
        void HandleApplyParameter(QString);

        void HandleMaskViz(QString, bool);
        void HandleMultiMaskViz(QStringList);
        void HandleMaskOpacity(double);
        void HandleMaskCellFilter(bool);

        void HandleMaskCorrection();

        void OnChangedTimePointCheck(QList<int>);
        void OnChangedSingleTimePoints(QList<int>);
        void OnChangedTimePoints(QList<int>);
        void OnChangedTimePointAll(QList<int>);

        void OnChangeMinTime(QString);
        void OnTabFocused(const ctkEvent& ctkEvent);
        
        //single run thread
        void OnSingleTimeStarted(int);
        void OnSingleFinished(int);
        void OnSingleCanceled();
        void OnSingleTimeChanged(int);
        //batch run thread
        void OnBatchTimeChanged(int);
        void OnBatchCanceled();
        void OnBatchFinished(int);
        void OnBatchTimeStarted(int,int);

    private:
        auto InitUI(void)->bool;
        auto HandleSingleExecution(QString,QString)->void;
        auto HandleTimeLapseExecution(QString,QString)->void;
        auto HandleInternalExecution(QString, QString)->void;

        auto ExecuteBatchRun(const QVariantMap& params)->bool;
        auto ExecuteOpenTCF(const QVariantMap& params)->bool;
        auto ExecuteUpdate(const QVariantMap& params)->bool;

        auto UpdateApplication()->void;
        auto CopyPath(QString src, QString dst)->void;                

        auto BroadcastApplyParam(QString)->void;
        auto BroadcastFinishBatch()->void;
        auto BroadcastAbortBatch()->void;
        auto BroadcastCorrection(QString, QString, int)->void;

        static void* SinglerunThread(void*);
        static void* BatchrunThread(void*);
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
