#define LOGGER_TAG "[BasicAnalyzerT]"

#include <Python.h>
#include <TCLogger.h>

#include <iostream>

#include <Inventor/nodes/SoInfo.h>

#include <QFileDialog>
#include <QMessageBox>

//Thread
#include <QtConcurrent/QtConcurrent>
#include <QProgressDialog>
#include <QThread>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/Xt/SoXt.h>
#include <Inventor/SoDB.h>
#include <ImageViz/SoImageViz.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Medical/InventorMedical.h>
#pragma warning(pop)

#include <IParameter.h>
#include <AppEvent.h>
#include <ScalarData.h>
#include <MenuEvent.h>
#include <ParameterList.h>
#include <PluginRegistry.h>
#include <ProjectStorage.h>
#include <FileChangeInfo.h>
#include <FileValidInfo.h>

#include <SceneController.h>
#include <ProcessingController.h>

//Presenter
#include <ScenePresenter.h>

//Plugins
#include <BaTimeImageDataReader.h>
#include <BaTimeMaskDataReader.h>
#include <BaTimeMaskDataWriter.h>
#include <BaTimeMeasureDataWriter.h>
#include <BaTimeMeasureDataReader.h>
#include <BaTimeProcessor.h>
#include <BaTimeImageDataReader.h>

//Panel
#include <BaTimeViewerPanel.h>
#include <BaTimeParameterPanel.h>
#include <BaTimeResultPanel.h>
#include <BaTimePlayerPanel.h>

#include <TCParameterReader.h>
#include <TCParameterWriter.h>
#include <TCFMetaReader.h>
#include <ScreenShotWidget.h>

#include "DeleteDialog.h"
#include "ui_MainWindow.h"
#include "MainWindow.h"

#include <OivTimer.h>

namespace TomoAnalysis::BasicAnalysisTime::AppUI {
    using namespace TC::Framework;

    void OnTopWarning(QString msg) {
        QMessageBox* msgBox = new QMessageBox;
        msgBox->setIcon(QMessageBox::Warning);
        msgBox->setParent(nullptr);
        msgBox->setWindowTitle("ERROR");
        msgBox->setText(msg);
        msgBox->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Dialog | Qt::WindowTitleHint);
        msgBox->exec();
    }

    auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };   

    struct WorkerThread::Impl {
        PyGILState_STATE pyGILState;
        QString fromBatchPGPath;
        QString fromBatchProc;
        QStringList cur_tcfs;
        bool batch_cancelEmit{ false };
        Entity::WorkingSet::Pointer workingset{ nullptr };
        QList<QList<int>> cur_timePoints;
        IParameter::Pointer batch_param{ nullptr };
    };

    void WorkerThread::emitStart() {
        emit start();
    }

    auto WorkerThread::SetPGPath(const QString& path)->void {
        d->fromBatchPGPath = path;
    }

    auto WorkerThread::SetProc(const QString& proc)->void {
        d->fromBatchProc = proc;
    }

    auto WorkerThread::SetTcfs(const QStringList& tcfs)->void {
        d->cur_tcfs = tcfs;
    }

    auto WorkerThread::SetCancel() ->void {
        d->batch_cancelEmit = true;
    }

    auto WorkerThread::SetWorkingSet(Entity::WorkingSet::Pointer ws)->void {
        d->workingset = ws;
    }

    auto WorkerThread::SetTimePoints(QList<QList<int>> tps)->void {
        d->cur_timePoints = tps;
    }

    auto WorkerThread::SetParameter(IParameter::Pointer param)->void {
        d->batch_param = param;
    }
    WorkerThread::WorkerThread(QObject* parent) : QObject(parent), d{ new Impl } {
        SoDB::init();
        SoImageViz::init();
        SoVolumeRendering::init();
        InventorMedical::init();

        SoPreferences::setValue("LDM_USE_IN_MEM_COMPRESSION", "0");
        SoPreferences::setValue("LDM_USE_BUILDTILE_BY_SLICE", "1");
    }

    WorkerThread::~WorkerThread() {

        InventorMedical::finish();
        SoVolumeRendering::finish();
        SoImageViz::finish();
        SoDB::finish();
    }

    void WorkerThread::doWork() {
        d->pyGILState = PyGILState_Ensure();
        auto cur_step = 0;
        emit batchTimeChanged(0);

        auto calc_step = 0;

        for (auto idx = 0; idx < d->cur_tcfs.count(); idx++) {
            std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader,std::default_delete<Plugins::ImageDataReader>() };
            std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader,std::default_delete<Plugins::MaskDataReader>() };
            std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader,std::default_delete<Plugins::MeasureDataReader>() };
            std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter,std::default_delete<Plugins::MaskDataWriter>() };
            std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter ,std::default_delete<Plugins::MeasureDataWriter>() };
            std::shared_ptr<Plugins::Processor> processor{ new Plugins::Processor,std::default_delete<Plugins::Processor>() };
            Interactor::ProcessingController procController(processor.get(),
                reader.get(), mreader.get(), mwriter.get(), rreader.get(), rwriter.get());
            std::shared_ptr<TC::IO::TCFMetaReader> metaReader{ new TC::IO::TCFMetaReader,std::default_delete<TC::IO::TCFMetaReader>() };

            if (d->batch_cancelEmit) {
                emit batchFinished(cur_step);
                return;
            }
            if (false == procController.OpenReferenceImage(d->cur_tcfs[idx], d->fromBatchPGPath, d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
                OnTopWarning("Failed to open tcf.");
                return;
            }
            auto metaInfo = metaReader->Read(d->cur_tcfs[idx]);
            d->workingset->SetTimeInterval(metaInfo->data.data3D.timeInterval);
            for (auto t = 0; t < d->cur_timePoints[idx].count(); t++) {
                //SbThreadAutoLock autoLock(dd->oiv_mutex);
                if (d->batch_cancelEmit) {
                    emit batchFinished(cur_step);
                    return;
                }
                auto timePoints = d->cur_timePoints[idx];
                auto tt = timePoints[t];
                calc_step = idx * timePoints.count() + t;

                if (false == procController.ChangeTimeStep(d->workingset, tt)) {
                    QLOG_ERROR() << "Failed to change time step to " << tt;
                }
                {
                    emit batchTimeStarted(idx, t);

                }


                if (!d->workingset->GetMeausreValid() || !d->workingset->GetMaskValid()) {
                    if (false == procController.Process(d->fromBatchProc, d->batch_param, d->workingset)) {
                        QLOG_ERROR() << "Failed to process " << d->fromBatchProc;
                    }
                }
                {
                    emit batchTimeChanged(calc_step + 1);
                }


                cur_step = calc_step + 1;
            }
        }
        {
            emit batchFinished(-1);
        }

        PyGILState_Release(d->pyGILState);
    }

    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };        
        PyGILState_STATE pyGILState;
        PyThreadState* pyThState{ nullptr };
        //Impl() = default;
        //~Impl() = default;
        
        QString tcf_path;

        QString fromBatchPGPath;
        QString fromBatchHyperName;
        QString fromBatchProc;
        QList<QStringList> fromBatchCubeInfo;
        IParameter::Pointer global_param{ nullptr };
        QMap<QString, IParameter::Pointer> share_param;

        //Processing enging
        Plugins::Processor::Pointer processor{ nullptr };

        // working set contains image, mask, measurements
        Entity::WorkingSet::Pointer workingset{ nullptr };

        //pop-up report panel
        Plugins::ResultPanel::Pointer resultPanel{ nullptr };

        //screen shot widget
        TC::ScreenShotWidget::Pointer screenshotWidget{ nullptr };

        QVariantMap appProperties;

        //file changes
        FileChangeInfo* fileChanges{ nullptr };
        FileValidInfo* fileValid{ nullptr };
        QStringList cur_tcfs;
        QStringList cur_ignore_tcfs;
        QList<int> cur_ignore_tcf_type;
        QStringList cur_ignore_tcf_cube;
        QList<QList<int>> cur_timePoints;
        QList<int> cur_cubeCounts;
        QStringList cur_cubes;
        bool cur_paramchanged{ false };

        //shared variables        
        QMessageBox* waitMsg{ nullptr };
        MainWindow* thisPointer{ nullptr };
        SbThreadMutex oiv_mutex;
        QMutex q_mutex;
        QWaitCondition q_wait;
        bool signal_processed{ false };
        //Parameter for single run stepper
        QProgressDialog* dialog{ nullptr };
        QList<int> single_time_steps;
        QString single_algo_name;
        IParameter::Pointer single_param;
        bool single_cancelEmit{ false };
        SbThread* single_thread{ nullptr };
        //Parameter for batch run stepper;
        QProgressDialog* batch_dialog{ nullptr };
        QString batch_timeStamp;
        std::shared_ptr<TC::IO::TCParameterWriter> batch_writer{ nullptr };
        IParameter::Pointer batch_param{nullptr};
        SbThread* batch_thread{ nullptr };
        bool batch_cancelEmit{ false };
        QList<double> batch_intervals;
        QString batch_displayName;

        WorkerThread* myWorker{ nullptr };
        QThread myQThread;
    };
    MainWindow::MainWindow(QWidget* parent)
        : IMainWindowTA("Basic Analyzer[T]", "Basic Analyzer[T]", parent)
        , d{new Impl}
    {        
        d->appProperties["Parent-App"] = "Project Manager";
        d->appProperties["AppKey"] = "Basic Analyzer[T]";
        QStringList processor_path;
        processor_path.push_back("/processor/basicanalysis");
        d->appProperties["Processors"] = processor_path;
        d->appProperties["hasSingleRun"] = true;
        d->appProperties["hasBatchRun"] = true;
        d->thisPointer = this;
    }
    MainWindow::~MainWindow() {
        //SoDB::finish();
        //SoQt::finish();
        if (d->fileChanges) {
            delete d->fileChanges;
        }
        if(d->fileValid) {
            delete d->fileValid;
        }
    }
    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    auto MainWindow::ForceClosePopup() -> void {
        if (nullptr != d->resultPanel) {
            d->resultPanel->close();
        }
    }


    auto MainWindow::GetCurTCF() const -> QString {
        return d->tcf_path;
    }

    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }
        if (params["ExecutionType"].toString() == "BatchRun") {
            return this->ExecuteBatchRun(params);
        }
        if (params["ExecutionType"].toString() == "OpenTCF") {
            return this->ExecuteOpenTCF(params);
        }
        if (params["ExecutionType"].toString() == "Update") {
            return this->ExecuteUpdate(params);
        }
        return true;
    }
    auto MainWindow::ExecuteBatchRun(const QVariantMap& params)->bool {
        d->ui->tabWidget->setTabEnabled(0, false);
        d->ui->tabWidget->setTabEnabled(1, true);
        d->ui->tabWidget->setCurrentIndex(1);

        d->fromBatchPGPath = params["Playground path"].toString();
        d->fromBatchHyperName = params["Hypercube name"].toString();
        d->fromBatchProc = params["Processor path"].toString();
        d->fromBatchCubeInfo.clear();

        if (d->fromBatchPGPath.isEmpty() || d->fromBatchHyperName.isEmpty() || d->fromBatchProc.isEmpty()) {
            return false;
        }
        QString cubeName;
        int idx = 1;
        while (1) {
            cubeName = "Cube" + QString::number(idx);
            auto cube = params[cubeName].toStringList();
            if (cube.isEmpty()) {
                break;
            }
            d->fromBatchCubeInfo.push_back(cube);
            idx++;
        }

        d->cur_tcfs.clear();
        d->cur_ignore_tcf_cube.clear();
        d->cur_ignore_tcf_type.clear();
        d->cur_ignore_tcfs.clear();
        d->cur_cubeCounts.clear();
        d->cur_cubes.clear();

        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();

        for (auto cubeEntry : d->fromBatchCubeInfo) {
            auto cubeInfo = cubeEntry;
            auto cubeName = cubeInfo[0];
            auto real_cube_cnt = 0;
            d->cur_cubes.push_back(cubeName);
            for (int j = 1; j < cubeInfo.size(); j++) {
                auto tcfPath = cubeInfo[j];

                auto arrSize = metaReader->ReadArrSize(tcfPath);
                if (d->fromBatchProc.contains(".ai")) {
                    if (false) {
                        //if(arrSize > 2500 * 2500 * 200){
                        //if(arrSize > INT_MAX/4) {
                        d->cur_ignore_tcfs.append(tcfPath);
                        d->cur_ignore_tcf_cube.append(cubeName);
                        if (arrSize >= INT_MAX) {
                            d->cur_ignore_tcf_type.push_back(0);
                        }
                        else {
                            d->cur_ignore_tcf_type.push_back(1);
                        }
                    }
                    else {
                        d->cur_tcfs.append(tcfPath);
                        real_cube_cnt++;
                    }
                }
                else {
                    if (arrSize >= INT_MAX) {
                        d->cur_ignore_tcfs.append(tcfPath);
                        d->cur_ignore_tcf_type.push_back(0);
                        d->cur_ignore_tcf_cube.append(cubeName);
                    }
                    else {
                        d->cur_tcfs.append(tcfPath);
                        real_cube_cnt++;
                    }
                }
            }
            d->cur_cubeCounts.push_back(real_cube_cnt);
        }
        
        if (d->cur_ignore_tcfs.count() > 0) {
            d->fileValid->SetTcfPaths(d->cur_ignore_tcfs);
            d->fileValid->SetTcfCubes(d->cur_ignore_tcf_cube);
            d->fileValid->SetTcfTypes(d->cur_ignore_tcf_type);
            d->fileValid->ComposeTable();
            d->fileValid->showMaximized();
        }
        else {
            OnAcceptValidity();
        }

        return true;
    }
    void MainWindow::OnAcceptValidity() {
        d->fileValid->hide();

        auto playgroundDir = QFileInfo(d->fromBatchPGPath).absoluteDir();
        playgroundDir.cdUp();


        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = ProjectManager::Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return;
        }
        const auto playground = parentProject->FindPlaygroundInfo(d->fromBatchPGPath);
        if (nullptr == playground) {
            return;
        }

        auto hyperList = playground->GetHyperCubeList();
        HyperCube::Pointer found{ nullptr };
        for (const auto& hypercube : hyperList) {
            if (hypercube->GetName().compare(d->fromBatchHyperName) == 0) {
                found = hypercube;
                break;
            }
        }
        if (nullptr == found) {
            return;
        }
        d->ui->batchListPanel->SetCurrentProcessor(d->fromBatchProc);
        d->ui->batchListPanel->Init(found);
        if (!d->ui->batchParameterPanel->SetCurrentProcessor(d->fromBatchProc)) {
            QLOG_ERROR() << "Failed to change proc";
        }
        d->ui->batchParameterPanel->LockProcessor(true);
    }

    void MainWindow::OnRejectValidity() {
        d->fileValid->hide();
        BroadcastAbortBatch();
    }

    auto MainWindow::ExecuteOpenTCF(const QVariantMap& params)->bool {
        auto tcf = params["TCFPath"].toString();
        auto proc = params["Processor path"].toString();
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        auto metaInfo = metaReader->Read(tcf);
        auto HT_cnt = metaInfo->data.data3D.dataCount;
        if (HT_cnt < 2) {
            //QMessageBox::warning(nullptr, "Error", "TCF is single shot, select time-framed data");
            OnTopWarning("TCF is single shot, select time-framed data");
            return false;
        }

        auto arrsize = static_cast<uint64_t>(metaInfo->data.data3D.sizeX) * static_cast<uint64_t>(metaInfo->data.data3D.sizeY) * static_cast<uint64_t>(metaInfo->data.data3D.sizeZ);
        auto procFolderlist = d->appProperties["Processors"].toStringList();
        if (false) {
            //if (arrsize > INT_MAX / 4) {//not for AI
            d->ui->parameterPanel->SetDefaultProcessors(procFolderlist, QStringList{ ".ai" });
        }
        else {
            d->ui->parameterPanel->SetDefaultProcessors(procFolderlist);
        }

        //disable batch run tab first
        d->ui->tabWidget->setCurrentIndex(0);
        d->ui->tabWidget->setTabEnabled(1, false);
        d->ui->tabWidget->setTabEnabled(0, true);

        d->tcf_path = tcf;
        d->screenshotWidget->SetTcfPath(tcf);

        QString playgroundpath;
        if (params.contains("Playground")) {
            playgroundpath = params["Playground"].toString();
        }

        //read tcf
        d->workingset = std::make_shared<Entity::WorkingSet>();//clear workingset
        if (params.contains("Hypercube")) {
            d->workingset->SetCurrentHyperCubeName(params["Hypercube"].toString());
        }        

        d->workingset->SetTimeInterval(metaInfo->data.data3D.timeInterval);

        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(nullptr,
            reader.get(), mreader.get(), nullptr, rreader.get(), nullptr);

        if (false == procController.OpenReferenceImage(tcf, playgroundpath, d->workingset)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
            OnTopWarning("Failed to open tcf.");
            return false;
        }//image is set to working set variable        

        if (false == procController.ChangeTimeStep(d->workingset, 0)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to change time_step.");
            OnTopWarning("Failed to change time_step.");
            return false;
        }

        //show tcf on viewer panel
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
            new Interactor::ScenePresenter(d->ui->viewerPanel,d->resultPanel.get(),
            d->ui->playerPanel,d->ui->parameterPanel,d->ui->VizControlPanel) };

        Interactor::BaSceneController sceneController(scenePresenter.get());

        if (false == sceneController.SetReferenceImage(d->workingset)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
            OnTopWarning("Failed to visualize reference image");
        }

        if (d->workingset->GetMaskValid()) {
            d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
            if (false == sceneController.SetMaskList(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
                OnTopWarning("Failed to update mask list");
            }
            if (false == sceneController.SetMaskImage(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                OnTopWarning("Failed to visualize mask image");
            }
        }
        d->resultPanel->clearResult();
        d->resultPanel->hide();
        if (d->workingset->GetMeausreValid()) {
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
            if (false == sceneController.SetResultData(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
                OnTopWarning("Failed to visualize mask image");
                d->resultPanel->hide();
                d->ui->parameterPanel->SetResultToggle(false);
            }
        }
        else {
            d->ui->parameterPanel->SetResultToggle(false);
        }

        if (!d->ui->parameterPanel->SetCurrentProcessor(proc)) {
            QLOG_ERROR() << "Failed to change proc";
        }

        d->ui->parameterPanel->LockProcessor(false);
        d->ui->playerPanel->SetSliceIndex(1);

        d->ui->parameterPanel->SetNoHighlight();
        if (d->ui->parameterPanel->GetTimePoints().isEmpty() && false == d->ui->parameterPanel->GetCurrentMode()) {
            d->ui->parameterPanel->SetTimeHighlight();
        }
        else if (!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }
        else if (!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }
        else {
            d->ui->parameterPanel->SetApplyHighlight();
        }
    }
        
    auto MainWindow::ExecuteUpdate(const QVariantMap& params)->bool {
        if (params["UpdateType"].toString() == "SingleRun") {
            UpdateApplication();
            return true;
        }
        auto pg_path = params["PGPath"].toString();
        auto hypername = params["Hypercube"].toString();
        auto tcf_path = params["TCFPath"].toString();
        auto time_step = params["TimeStep"].toInt();
        auto user_name = params["UserName"].toString();

        auto workingset = std::make_shared<Entity::WorkingSet>();

        std::shared_ptr<Plugins::ImageDataReader> ireader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader };
        std::shared_ptr <Plugins::MeasureDataWriter> mwriter{ new Plugins::MeasureDataWriter };

        Interactor::ProcessingController procController(d->processor.get(), ireader.get(), mreader.get(), nullptr, nullptr, mwriter.get());

        if (false == procController.RecomputeSelectedImage(pg_path, hypername, tcf_path, time_step, user_name)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to modify measure");
            OnTopWarning("Failed to modify measure");
            return false;
        }

        QFile loadFile(pg_path);
        if (loadFile.open(QIODevice::ReadOnly)) {
            const QByteArray data = loadFile.readAll();
            const  QJsonDocument doc(QJsonDocument::fromJson(data));
            auto object = doc.object();
            auto pgName = object["name"].toString();//pg name
            QFileInfo pgFile(pg_path);
            auto pgBase = pgFile.fileName().chopped(5);
            auto osplit = pgBase.split("_");
            auto originalTime = osplit[osplit.count() - 1];//time
            auto original_pgPath = pg_path;
            QDir fpdir(original_pgPath);
            fpdir.cdUp();//history
            fpdir.cdUp();//playground
            fpdir.cdUp();//projectname
            auto projName = fpdir.dirName();

            if (true == DeleteDialog::Delete(nullptr, projName, pgName, hypername, originalTime)) {
                //delete previous information
                auto paramPath = pg_path.chopped(5) + ".param";
                QFile::remove(paramPath);
                QFile::remove(pg_path);

                QDir deldir(original_pgPath);
                deldir.cdUp();//history
                deldir.cd(hypername);
                deldir.cd(originalTime);

                deldir.removeRecursively();
            }
        }

        BroadcastFinishBatch();
        return true;
    }
    
    auto MainWindow::InitUI()->bool {
        //processor parameter panel
        connect(d->ui->parameterPanel, SIGNAL(sigAlgoValue(double,QString)), this, SLOT(HandleProcValChange(double,QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigProc(QString)), this, SLOT(HandleProcChange(QString)));
        connect(d->ui->parameterPanel,SIGNAL(sigChangedTimePoints(QList<int>)),this,SLOT(OnChangedSingleTimePoints(QList<int>)));
        connect(d->ui->parameterPanel, SIGNAL(sigAlgoExe(QString,QString)), this, SLOT(HandleAlgoExecution(QString,QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigRefHigh()), this, SLOT(HandleRefreshHighlight()));

        connect(d->ui->parameterPanel, SIGNAL(sigToggleResult(bool)), this, SLOT(HandleToggleResult(bool)));
        connect(d->ui->parameterPanel, SIGNAL(sigApplyParam(QString)), this, SLOT(HandleApplyParameter(QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigMaskCorrection()), this, SLOT(HandleMaskCorrection()));

        //player panel
        connect(d->ui->playerPanel, SIGNAL(timelapseIndexChanged(int)), this, SLOT(HandleTimeChange(int)));

        //report panel
        connect(d->resultPanel.get(), SIGNAL(sigSaveCsv()), this, SLOT(HandleSingleCSV()));
        connect(d->resultPanel.get(), SIGNAL(sigCloseResult()), this, SLOT(HandleCloseResult()));

        //rendering panel
        connect(d->ui->VizControlPanel, SIGNAL(sigMask(QString, bool)), this, SLOT(HandleMaskViz(QString, bool)));
        connect(d->ui->VizControlPanel, SIGNAL(sigMultiMask(QStringList)), this, SLOT(HandleMultiMaskViz(QStringList)));
        connect(d->ui->VizControlPanel,SIGNAL(sigMaskOpa(double)),this,SLOT(HandleMaskOpacity(double)));
        connect(d->ui->VizControlPanel,SIGNAL(sigCellFilter(bool)),this,SLOT(HandleMaskCellFilter(bool)));

        connect(d->ui->batchParameterPanel,SIGNAL(sigCheckedTimePoints(QList<int>)),this,SLOT(OnChangedTimePointCheck(QList<int>)));
        connect(d->ui->batchParameterPanel, SIGNAL(sigChangedTimePoints(QList<int>)), this, SLOT(OnChangedTimePoints(QList<int>)));
        connect(d->ui->batchParameterPanel,SIGNAL(sigAllTimePoints(QList<int>)),this,SLOT(OnChangedTimePointAll(QList<int>)));
        connect(d->ui->batchParameterPanel,SIGNAL(sigWholeExe()),this,SLOT(HandleWholeExecution()));
        connect(d->ui->batchListPanel,SIGNAL(minPathChanged(QString)),this,SLOT(OnChangeMinTime(QString)));        

        d->ui->parameterPanel->SetUIForBatch(false);
        d->ui->batchParameterPanel->SetUIForBatch(true);

        return true;
    }
    void MainWindow::HandleMaskCellFilter(bool cellwise) {
        d->ui->viewerPanel->SetCellFilter(!cellwise);
    }
    void MainWindow::HandleMaskOpacity(double opacity) {
        d->ui->viewerPanel->SetMaskOpacity(opacity);
    }
    void MainWindow::HandleRefreshHighlight() {        
        if (d->ui->parameterPanel->GetTimePoints().isEmpty() && false == d->ui->parameterPanel->GetCurrentMode()) {
            d->ui->parameterPanel->SetTimeHighlight();
        }
        else if (!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }
        else if (!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }
        else {
            d->ui->parameterPanel->SetApplyHighlight();
        }
    }            

    void MainWindow::OnTabFocused(const ctkEvent& ctkEvent) {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType sudden = MenuType::NONE;
        if (ctkEvent.getProperty("TabName").isValid()) {
            auto TabName = ctkEvent.getProperty("TabName").toString();
            if (TabName.compare("Basic Analyzer[T]") == 0) {
                auto tcf_path = GetCurTCF();
                MenuEvent menuEvt(MenuTypeEnum::TitleBar);
                menuEvt.scriptSet(tcf_path);
                publishSignal(menuEvt, "Basic Analyzer[T]");
                if (d->ui->tabWidget->currentIndex() == 0) {
                    HandleToggleResult(d->ui->parameterPanel->GetResultToggle());
                }
            }else {
                ForceClosePopup();
            }
        }
        else if (ctkEvent.getProperty(sudden._to_string()).isValid()) {
            ForceClosePopup();
        }
    }

    auto MainWindow::BroadcastApplyParam(QString procName) -> void {
        MenuEvent menuEvt(MenuTypeEnum::ParameterSet);
        QString target = "org.tomocube.projectmanagerPlugin";
        target += "*";
        target += procName;

        QString sender = "org.tomocube.basicanalysistimePlugin";
        menuEvt.scriptSet(sender + "!" + target);
        publishSignal(menuEvt, "Basic Analyzer[T]");
    }

    auto MainWindow::BroadcastFinishBatch() -> void {
        MenuEvent menuEvt(MenuTypeEnum::BatchFinish);
        QString target = "org.tomocube.projectmanagerPlugin";
        QString sender = "org.tomocube.basicanalysistimePlugin";
        menuEvt.scriptSet(sender + "!" + target);
        publishSignal(menuEvt, "Basic Analyzer[T]");
    }

    auto MainWindow::BroadcastAbortBatch() -> void {
        MenuEvent menuEvt(MenuTypeEnum::AbortTab);
        QString sender = "org.tomocube.basicanalysistimePlugin";
        menuEvt.scriptSet(sender);
        publishSignal(menuEvt, "Basic Analyzer[T]");
    }

    auto MainWindow::BroadcastCorrection(QString tcfPath, QString maskPath, int time_step) -> void {
        Q_UNUSED(time_step)
        AppEvent appEvent(AppTypeEnum::APP_WITH_SHARE);
        appEvent.setFullName("org.tomocube.intersegPlugin");
        appEvent.setAppName("Mask Editor");
        appEvent.addParameter("ExecutionType", "LinkRun");
        appEvent.addParameter("TcfPath", tcfPath);
        appEvent.addParameter("MaskPath", maskPath);
        appEvent.addParameter("SenderFull", "org.tomocube.basicanalysistimePlugin");
        appEvent.addParameter("SenderName", "Basic Analyzer[T]");
        publishSignal(appEvent, "Basic Analyzer[T]");
    }

    void MainWindow::HandleMaskCorrection() {
        auto procName = d->ui->parameterPanel->GetCurrentProcessor();
        auto maskPath = d->workingset->GetMaskPath();

        if(d->workingset->GetMasks().isEmpty()) {
            //QMessageBox::warning(nullptr, "Error", "There is no temporal mask");
            OnTopWarning("There is no temporal mask");
            return;
        }

        //if(!procName.contains("AI")) {
        if(true){
            std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter,std::default_delete<Plugins::MaskDataWriter>() };
            Interactor::ProcessingController procController(nullptr, nullptr, nullptr, mwriter.get());
            if(false == procController.SaveTemporalMasks(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to save temporal masks");
                OnTopWarning("Failed to save temporal masks");
                return;
            }
        }
        auto tcfPath = d->workingset->GetImagePath();        
        BroadcastCorrection(tcfPath, maskPath, d->workingset->GetImageTimeStep());
    }

    void MainWindow::HandleApplyParameter(QString procName) {
        d->share_param[procName] = d->ui->parameterPanel->GetParameter();
        BroadcastApplyParam(procName);
    }

    void MainWindow::HandleToggleResult(bool show) {
        if(show) {            
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
        }else {
            d->resultPanel->hide();
        }
    }

    void MainWindow::HandleMaskViz(QString name, bool state) {
        if (state) {
            const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                    new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr) };

            Interactor::BaSceneController sceneController(scenePresenter.get());

            if (false == sceneController.SetMaskImage(d->workingset, name)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                OnTopWarning("Failed to visualize mask image");
                return;
            }
        }else {
        }
    }

    void MainWindow::HandleMultiMaskViz(QStringList masks) {
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{ new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr) };
        Interactor::BaSceneController sceneController(scenePresenter.get());
        if(false == sceneController.SetMultiMask(d->workingset,masks)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to visuzlize multi-layered mask");
            OnTopWarning("Failed to visuzlize multi-layered mask");
            return;
        }
    }


    void MainWindow::HandleCloseResult() {
        d->ui->parameterPanel->SetResultToggle(false);
    }
    void MainWindow::HandleSingleCSV() {
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(d->processor.get(),
            nullptr, nullptr, nullptr, rreader.get());
        auto path = qApp->applicationDirPath();
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
            path,
            tr("Excel csv file (*.csv)"));
        if(d->workingset->GetMeausreValid()) {
            if(false == procController.SaveCSVFile(fileName,d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to save CSV");
                OnTopWarning("Failed to save CSV");
                return;
            }
        }
    }

    auto MainWindow::UpdateApplication() -> void {
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader, std::default_delete<Plugins::MaskDataReader>() };

        Interactor::ProcessingController procController(nullptr, nullptr, mreader.get(), nullptr, nullptr);        
        if(false == procController.UpdateApplication(d->workingset)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to update Application");
            OnTopWarning("Failed to update Application");
            return;
        }
        d->ui->playerPanel->SetSliceIndex(1);
        HandleTimeChange(0);
    }


    auto MainWindow::HandleSingleExecution(QString param_name,QString algo_name) -> void {
        Q_UNUSED(param_name)
        std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter };
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter };

        Interactor::ProcessingController procController(d->processor.get(),
            nullptr,
            nullptr, mwriter.get(), nullptr, rwriter.get());

        auto param = d->ui->parameterPanel->GetParameter();

        if(algo_name.contains("measurement")) {
            if (false == procController.Process(algo_name, param, d->workingset,false)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                return;            
            }
        }else if(algo_name.contains("masking")) {
            d->ui->viewerPanel->ClearMask();
            if (false == procController.Process(algo_name, param, d->workingset,false)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                return;
            }            
        }else if(algo_name.contains("labeling")) {
            d->ui->viewerPanel->ClearMask();
            if(false == procController.Process(algo_name,param,d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                return;
            }
        }else if(algo_name.contains("postprocessing")) {
            d->ui->viewerPanel->ClearMask();
            if(false == procController.Process(algo_name,param,d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                return;
            }
        }

        d->ui->parameterPanel->SetNextHighlight(param_name);

        if (algo_name.contains("measurement") && d->workingset->GetMeausreValid()) {
            //show result on report panel
            d->resultPanel->clearResult();
            d->resultPanel->hide();
            d->ui->parameterPanel->SetResultToggle(false);
            const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                new Interactor::ScenePresenter(nullptr,d->resultPanel.get()) };

            Interactor::BaSceneController sceneController(scenePresenter.get());

            if (false == sceneController.SetResultData(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to show result");
                OnTopWarning("Failed to show result");
                return;
            }            
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
        }    
        
        if ((algo_name.contains("masking") || algo_name.contains("labeling") || algo_name.contains("postprocessing")) && d->workingset->GetMaskValid()) {
            //show tcf on viewer panel
            const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr,nullptr,d->ui->parameterPanel,d->ui->VizControlPanel) };

            Interactor::BaSceneController sceneController(scenePresenter.get());
            d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
            if (false == sceneController.SetMaskList(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
                OnTopWarning("Failed to update mask list");
                return;
            }

            if (false == sceneController.SetMaskImage(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                OnTopWarning("Failed to visualize mask image");
                return;
            }
        }        
    }

    void* MainWindow::SinglerunThread(void* userData) {
        SoDB::init();
        SoImageViz::init();
        SoVolumeRendering::init();             
        InventorMedical::init();

        SoDB::setSystemTimer(OivTimer::GetInstance());

        SoPreferences::setValue("LDM_USE_IN_MEM_COMPRESSION", "0");
        SoPreferences::setValue("LDM_USE_BUILDTILE_BY_SLICE", "1");

        //SoDB::setSystemTimer(new OivTimer);
        /*auto timer = new OivTimer;
        timer->startTimer(100, Qt::TimerType::CoarseTimer);
        SoDB::setSystemTimer(timer);*/
        auto dd = static_cast<Impl*>(userData);

        dd->pyGILState = PyGILState_Ensure();
        {            
            emit dd->thisPointer->singleTimeChanged(0);
            /*QMutexLocker locker(&dd->q_mutex);
            if (false == dd->signal_processed) {
                dd->q_wait.wait(&dd->q_mutex);
            }
            dd->signal_processed = false;*/
        }
        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader ,std::default_delete<Plugins::ImageDataReader>() };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader ,std::default_delete<Plugins::MaskDataReader>() };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader ,std::default_delete<Plugins::MeasureDataReader>() };
        std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter ,std::default_delete<Plugins::MaskDataWriter>() };
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter ,std::default_delete<Plugins::MeasureDataWriter>() };
        Interactor::ProcessingController procController(dd->processor.get(),
            reader.get(), mreader.get(), mwriter.get(), rreader.get(), rwriter.get());

        if (dd->single_algo_name.contains("measurement")) {
            QFile::remove(dd->workingset->GetMeasurePath());
        }

        for (auto i = 0; i < dd->single_time_steps.count(); i++) {
            //SbThreadAutoLock autoLock(dd->oiv_mutex);
            if (dd->single_cancelEmit) {
                if (i != 0) {                    
                    emit dd->thisPointer->singleFinished(dd->single_time_steps[i - 1]);
                    /*QMutexLocker locker(&dd->q_mutex);
                    if (false == dd->signal_processed) {
                        dd->q_wait.wait(&dd->q_mutex);
                    }
                    dd->signal_processed = false;*/
                }
                else {                    
                    emit dd->thisPointer->singleFinished(dd->single_time_steps[i]);
                    /*QMutexLocker locker(&dd->q_mutex);
                    if (false == dd->signal_processed) {
                        dd->q_wait.wait(&dd->q_mutex);
                    }
                    dd->signal_processed = false;*/
                }                
                return nullptr;
            }
            auto time_point = dd->single_time_steps[i];
            if (false == procController.ChangeTimeStep(dd->workingset, time_point - 1)) {
                QLOG_ERROR() << "Failed to open tcf step " << time_point - 1;
                std::cout << "Failed to open Tcf step" << std::endl;
            }
            {                
                emit dd->thisPointer->singleTimeStarted(i + 1);
                /*QMutexLocker locker(&dd->q_mutex);
                if (false == dd->signal_processed) {
                    dd->q_wait.wait(&dd->q_mutex);
                }
                dd->signal_processed = false;*/
            }                        

            if (dd->single_algo_name.contains("measurement") || dd->single_algo_name.contains("masking") || dd->single_algo_name.contains("labeling") || dd->single_algo_name.contains("postprocessing")) {
                if (false == procController.Process(dd->single_algo_name, dd->single_param, dd->workingset)) {
                    QLOG_ERROR() << "failed to process " << dd->single_algo_name;
                }
            }
            {                
                emit dd->thisPointer->singleTimeChanged(i + 1);
                /*QMutexLocker locker(&dd->q_mutex);
                if (false == dd->signal_processed) {
                    dd->q_wait.wait(&dd->q_mutex);
                }
                dd->signal_processed = false;*/
            }            
            //SoDB::writelock();
            //QLOG_INFO() << "Force process event to release memory";
            SoDB::processEvents();
            SbThread::sleep_ms(1000);
        }
        {            
            emit dd->thisPointer->singleFinished(dd->single_time_steps[dd->single_time_steps.count() - 1]);
            /*QMutexLocker locker(&dd->q_mutex);
            if (false == dd->signal_processed) {
                dd->q_wait.wait(&dd->q_mutex);
            }
            dd->signal_processed = false;*/
        }
        PyGILState_Release(dd->pyGILState);

        InventorMedical::finish();
        SoVolumeRendering::finish();
        SoImageViz::finish();
        SoDB::finish();

        //delete timer;

        return nullptr;
    }
    void MainWindow::OnSingleTimeStarted(int step) {
        if(d->single_cancelEmit) {            
            return;
        }
        auto interval = d->workingset->GetTimeInterval();
        auto real_time = (d->single_time_steps[step - 1] - 1) * interval;
        auto timeTxt = QDateTime::fromTime_t(real_time).toUTC().toString("hh:mm:ss");
        d->dialog->setLabelText(QString("Progressing time point at %1 (%2 / %3)").arg(timeTxt).arg(step).arg(d->single_time_steps.count()));
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }
    void MainWindow::OnSingleFinished(int end_time) {
        if (end_time != d->single_time_steps.last()) {
            d->dialog->finished(0);
        }
        d->ui->playerPanel->SetSliceIndex(end_time);
        HandleTimeChange(end_time - 1);

        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/

        SbThread::destroy(d->single_thread);                

        d->waitMsg->accept();                
                
        delete d->dialog;
        d->dialog = nullptr;
        delete d->waitMsg;
        d->waitMsg = nullptr;

        PyEval_RestoreThread(d->pyThState);
    }
    void MainWindow::OnSingleCanceled() {
        d->single_cancelEmit = true;
        d->myWorker->SetCancel();
        d->dialog->finished(0);
        d->waitMsg->exec();
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }
    void MainWindow::OnSingleTimeChanged(int time) {        
        if(d->dialog->isVisible()) {
            if (d->dialog->maximum() > time) {
                d->dialog->setValue(time);
            }
        }
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/

    }
    auto MainWindow::HandleTimeLapseExecution(QString param_name,QString algo_name) -> void {
        Q_UNUSED(param_name)
        auto timeSteps = d->ui->parameterPanel->GetTimePoints();
        if(timeSteps.isEmpty()) {
            //QMessageBox::warning(nullptr, "Error", "Set Time steps First!");
            OnTopWarning("Set Time steps First!");
            return;
        }                

        d->ui->viewerPanel->ClearMask();
                                        
        auto count = timeSteps.count();//set customized time steps                             

        d->single_time_steps = timeSteps;
        d->single_algo_name = algo_name;
        d->single_param = d->ui->parameterPanel->GetParameter();
        d->single_cancelEmit = false;

        d->waitMsg = new QMessageBox(QMessageBox::Icon::Information,"Information","Wait for job finished..");
        d->waitMsg->setStandardButtons(QMessageBox::NoButton);
        d->waitMsg->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        d->dialog = new QProgressDialog;
        d->dialog->setLabelText(QString("Progressing %1 time_steps").arg(count));
        d->dialog->setRange(0, count);       

        connect(d->dialog, SIGNAL(canceled()), this, SLOT(OnSingleCanceled()));

        d->pyThState = PyEval_SaveThread();
        d->single_thread = SbThread::create(SinglerunThread, (void*)d.get());        
        d->dialog->open();

        d->ui->parameterPanel->SetNextHighlight(param_name);
    }    

    void MainWindow::OnBatchCanceled() {
        d->batch_cancelEmit = true;
        d->batch_dialog->finished(0);
        d->waitMsg->exec();
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }

    void MainWindow::OnBatchFinished(int result_step) {
        std::cout << "On batch finished" << std::endl;
        if (result_step == -1) {
            d->batch_dialog->finished(0);
            ////save batch Run parameter
            ///Create required history folder here
            d->batch_writer->Write(d->share_param[d->fromBatchProc], "Basic Analyzer[T]", "Report[T]", d->fromBatchHyperName, d->fromBatchProc, d->batch_timeStamp);
            //copy existing *.rep files to batchRun folder
            auto repFolderPath = d->fromBatchPGPath.chopped(5);

            QDir pdir(repFolderPath);
            pdir.cdUp();//playground folder
            pdir.cd("history");
            pdir.cd(d->fromBatchHyperName);
            pdir.cd(d->batch_timeStamp);

            repFolderPath += "/";
            repFolderPath += d->fromBatchHyperName;

            //auto targetFolderPath = repFolderPath + "/" + timeStamp;
            auto targetFolderPath = pdir.path();

            QDir cdir(targetFolderPath);
            for (auto c : d->cur_cubes) {
                if (!cdir.exists(c)) {
                    cdir.mkdir(c);
                }
            }

            auto cubeCntIdx = 0;
            auto curCnt = 0;
            for (auto tcfName : d->cur_tcfs) {
                QFileInfo info(tcfName);
                auto filename = info.fileName().chopped(4);
                auto reportPath = repFolderPath + "/" + filename + ".rep";
                //auto instPath = repFolderPath + "/" + filename + "_MultiLabel.msk";
                //auto organPath = repFolderPath + "/" + filename + "_MultiLayer.msk";
                auto maskPath = repFolderPath + "/" + filename + ".msk";
                if (QFileInfo::exists(reportPath)) {
                    //if report file exist copy it to target folder                
                    QString ccc;
                    if (d->cur_cubeCounts[cubeCntIdx] > curCnt) {
                        curCnt++;
                        ccc = d->cur_cubes[cubeCntIdx];
                    }
                    else {
                        curCnt = 1;
                        ccc = d->cur_cubes[++cubeCntIdx];
                    }
                    auto targetPath = targetFolderPath + "/" + ccc + "/" + filename + ".rep";
                    //auto targetInstPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLabel.msk";
                    //auto targetOrganPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLayer.msk";
                    auto targetMaskPath = targetFolderPath + "/" + ccc + "/" + filename + ".msk";
                    QFile::copy(reportPath, targetPath);
                    QFile::copy(maskPath, targetMaskPath);
                    //QFile::copy(instPath, targetInstPath);
                    //QFile::copy(organPath, targetOrganPath);

                    //copy mask paths
                    auto maskDir = maskPath.chopped(4);
                    auto targetMaskDir = targetMaskPath.chopped(4);
                    CopyPath(maskDir, targetMaskDir);
                    /*auto instDir = instPath.chopped(4);
                    /auto targetInstDir = targetInstPath.chopped(4);
                    CopyPath(instDir, targetInstDir);
                    auto organDir = organPath.chopped(4);
                    auto targetOrganDir = targetOrganPath.chopped(4);
                    CopyPath(organDir, targetOrganDir);*/
                }
            }
            //copy current playground
            auto copyPGPath = d->fromBatchPGPath.chopped(5);
            QDir zdir(copyPGPath);
            zdir.cdUp();
            zdir.cd("history");

            copyPGPath = zdir.path();
            copyPGPath += "/";
            copyPGPath += d->fromBatchHyperName;
            copyPGPath += "_";
            copyPGPath += d->batch_timeStamp;
            copyPGPath += ".tcpg";
            QFile::copy(d->fromBatchPGPath, copyPGPath);

            BroadcastFinishBatch();
            std::cout << "broadCase batch" << std::endl;
        }
        else {
            BroadcastAbortBatch();
        }
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/

        std::cout << "Unlock locker" << std::endl;
        SbThread::destroy(d->batch_thread);
        //d->myQThread.quit();
        if (d->waitMsg->isVisible()) {
            d->waitMsg->accept();
        }

        //delete d->batch_dialog;
        //d->batch_dialog = nullptr;
        delete d->waitMsg;
        d->waitMsg = nullptr;

        PyEval_RestoreThread(d->pyThState);
    }

    void MainWindow::OnBatchTimeChanged(int time) {
        if(false == d->batch_cancelEmit) {
            d->batch_dialog->setValue(time);
        }
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }

    void MainWindow::OnBatchTimeStarted(int tcf_idx,int time_idx) {
        if(d->batch_cancelEmit) {
            /*QMutexLocker locker(&d->q_mutex);
            d->signal_processed = true;
            d->q_wait.wakeAll();*/
            return;
        }

        auto tcf_name = d->cur_tcfs[tcf_idx];
        QFileInfo file_name(tcf_name);
        auto fn = file_name.fileName();

        auto timeFrame = d->cur_timePoints[tcf_idx][time_idx];
        auto actualTime = timeFrame * d->batch_intervals[tcf_idx];
        auto timeTotal = d->cur_timePoints[tcf_idx].count();

        auto timeTxt = QDateTime::fromTime_t(actualTime).toUTC().toString("hh:mm:ss");

        d->batch_dialog->setLabelText(QString("Current file: %1 (%2/%3)\nTime point: frame at %4 (%5/%6)\nPerforming %7")
            .arg(fn).arg(tcf_idx + 1).arg(d->cur_tcfs.count())
            .arg(timeTxt).arg(time_idx + 1).arg(timeTotal)
            .arg(d->batch_displayName));
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }


    void* MainWindow::BatchrunThread(void* userData) {
        auto dd = static_cast<Impl*>(userData);
        SoDB::init();
        SoImageViz::init();
        SoVolumeRendering::init();
        InventorMedical::init();

        SoDB::setSystemTimer(OivTimer::GetInstance());                

        dd->pyGILState = PyGILState_Ensure();

        auto cur_step = 0;
        {   
            emit dd->thisPointer->batchTimeChanged(0);
            /*QMutexLocker locker(&dd->q_mutex);
            if (false == dd->signal_processed) {
                dd->q_wait.wait(&dd->q_mutex);
            }
            dd->signal_processed = false;*/
        }
                       
        auto calc_step = 0;
        
        for (auto idx = 0; idx < dd->cur_tcfs.count(); idx++) {
            std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader,std::default_delete<Plugins::ImageDataReader>() };
            std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader,std::default_delete<Plugins::MaskDataReader>() };
            std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader,std::default_delete<Plugins::MeasureDataReader>() };
            std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter,std::default_delete<Plugins::MaskDataWriter>() };
            std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter ,std::default_delete<Plugins::MeasureDataWriter>() };
            std::shared_ptr<Plugins::Processor> processor{ new Plugins::Processor,std::default_delete<Plugins::Processor>() };
            Interactor::ProcessingController procController(processor.get(),
                reader.get(), mreader.get(), mwriter.get(), rreader.get(), rwriter.get());
            std::shared_ptr<TC::IO::TCFMetaReader> metaReader{ new TC::IO::TCFMetaReader,std::default_delete<TC::IO::TCFMetaReader>() };

            if (dd->batch_cancelEmit) {                
                emit dd->thisPointer->batchFinished(cur_step);                
                return nullptr;
            }
            if (false == procController.OpenReferenceImage(dd->cur_tcfs[idx], dd->fromBatchPGPath, dd->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
                OnTopWarning("Failed to open tcf.");
                return nullptr;
            }
            auto metaInfo = metaReader->Read(dd->cur_tcfs[idx]);
            dd->workingset->SetTimeInterval(metaInfo->data.data3D.timeInterval);
            for (auto t = 0; t < dd->cur_timePoints[idx].count(); t++) {
                //SbThreadAutoLock autoLock(dd->oiv_mutex);
                if (dd->batch_cancelEmit) {                                        
                    emit dd->thisPointer->batchFinished(cur_step);                    
                    return nullptr;
                }
                auto timePoints = dd->cur_timePoints[idx];
                auto tt = timePoints[t];
                calc_step = idx * timePoints.count() + t;

                if (false == procController.ChangeTimeStep(dd->workingset, tt)) {
                    QLOG_ERROR() << "Failed to change time step to " << tt;
                }
                {   
                    emit dd->thisPointer->batchTimeStarted(idx, t);
                    /*QMutexLocker locker(&dd->q_mutex);
                    if (false == dd->signal_processed) {
                        dd->q_wait.wait(&dd->q_mutex);
                    }
                    dd->signal_processed = false;*/
                }                
                

                if (!dd->workingset->GetMeausreValid() || !dd->workingset->GetMaskValid()) {
                    if (false == procController.Process(dd->fromBatchProc, dd->batch_param, dd->workingset)) {
                        QLOG_ERROR() << "Failed to process " << dd->fromBatchProc;
                    }
                }
                {   
                    emit dd->thisPointer->batchTimeChanged(calc_step + 1);
                    /*QMutexLocker locker(&dd->q_mutex);
                    if (false == dd->signal_processed) {
                        dd->q_wait.wait(&dd->q_mutex);
                    }
                    dd->signal_processed = false;*/
                }
                
                
                cur_step = calc_step + 1;
                //QLOG_INFO() << "Force process event to release memory";
                SoDB::processEvents();
                SbThread::sleep_ms(1000);
            }            
        }
        {
            emit dd->thisPointer->batchFinished(-1);            
        }

        PyGILState_Release(dd->pyGILState);

        InventorMedical::finish();
        SoVolumeRendering::finish();
        SoImageViz::finish();
        SoDB::finish();
        //SoQt::finish();
        
        //delete timer;

        return nullptr;
    }

    void MainWindow::OnAcceptBatchRun() {        
        d->fileChanges->hide();
        auto current = QDateTime::currentDateTime();
        d->batch_timeStamp = current.toString("yyMMdd.hhmmss");

        d->waitMsg = new QMessageBox(QMessageBox::Icon::Information, "Information", "Wait for job finished..");
        d->waitMsg->setStandardButtons(QMessageBox::NoButton);
        d->waitMsg->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        if(d->batch_dialog) {
            delete d->batch_dialog;
            d->batch_dialog = nullptr;
        }
        d->batch_dialog = new QProgressDialog;
        d->batch_dialog->setLabelText(QString("Progressing %1 tcf files...").arg(d->cur_tcfs.count()));
        d->batch_cancelEmit = false;
        d->workingset = std::make_shared<Entity::WorkingSet>();
        d->workingset->SetCurrentHyperCubeName(d->fromBatchHyperName);

        auto moduleInstance = PluginRegistry::GetPlugin(d->fromBatchProc);
        d->batch_displayName = moduleInstance->GetName();

        d->batch_writer = std::make_shared<TC::IO::TCParameterWriter>(d->fromBatchPGPath);
        if (d->cur_paramchanged) {
            auto hyper_path = d->fromBatchPGPath.chopped(5) + "/" + d->fromBatchHyperName;
            QDir dir(hyper_path);
            auto file_list = dir.entryList(QDir::Filter::Files);
            for (auto f : file_list) {
                auto file_path = hyper_path + "/" + f;
                QFile::remove(file_path);
                QDir dirPath(file_path.chopped(4));
                if (dirPath.exists()) {
                    dirPath.removeRecursively();
                }
            }
            d->workingset->SetProcChanged(true);
            d->batch_writer->Write(d->share_param[d->fromBatchProc], "Basic Analyzer[T]", "Report[T]", d->fromBatchHyperName, d->fromBatchProc);
        }

        d->batch_param = d->share_param[d->fromBatchProc];                

        auto count = 0;
        for (int tcf_idx = 0; tcf_idx < d->cur_tcfs.count(); tcf_idx++) {
            auto tps = d->cur_timePoints[tcf_idx].count();
            count += tps;
        }
        d->batch_dialog->setRange(0, count);
        d->batch_intervals.clear();
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        for (auto tc : d->cur_tcfs) {
            auto interval = metaReader->Read(tc)->data.data3D.timeInterval;
            d->batch_intervals.push_back(interval);
        }
                
        connect(d->batch_dialog, SIGNAL(canceled()), this, SLOT(OnBatchCanceled()));

        d->pyThState = PyEval_SaveThread();

        /*d->myWorker = new WorkerThread();
        d->myWorker->moveToThread(&d->myQThread);
        d->myQThread.start();
        d->myWorker->SetParameter(d->batch_param);
        d->myWorker->SetTcfs(d->cur_tcfs);
        d->myWorker->SetTimePoints(d->cur_timePoints);
        d->myWorker->SetPGPath(d->fromBatchPGPath);
        d->myWorker->SetProc(d->fromBatchProc);
        d->myWorker->SetWorkingSet(d->workingset);

        connect(d->myWorker, &WorkerThread::start, d->myWorker, &WorkerThread::doWork);
        connect(&d->myQThread, &QThread::finished, d->myWorker, &QObject::deleteLater);
        connect(d->myWorker, SIGNAL(batchTimeChanged(int)), this, SLOT(OnBatchTimeChanged(int)));
        connect(d->myWorker, SIGNAL(batchTimeStarted(int, int)), this, SLOT(OnBatchTimeStarted(int, int)));
        connect(d->myWorker, SIGNAL(batchFinished(int)), this, SLOT(OnBatchFinished(int)));*/

        d->batch_thread = SbThread::create(BatchrunThread, (void*)d.get());

        //emit d->myWorker->start();

        d->batch_dialog->open();
    }
    void MainWindow::OnRejectBatchRun() {
        d->fileChanges->hide();
        BroadcastAbortBatch();
    }
    void MainWindow::HandleWholeExecution() {
        auto isTimeSet = d->ui->batchListPanel->GetValidity();
        if(!isTimeSet) {            
            //QMessageBox::warning(nullptr, "Error", "Set time steps for all image first!");
            OnTopWarning("Set time steps for all image first!");
            return;
        }

        d->cur_timePoints.clear();        

        for(auto tcf: d->cur_tcfs) {
            auto tps = d->ui->batchListPanel->GetTimeSteps(tcf);
            d->cur_timePoints.push_back(tps);
        }
        
        auto param_reader = std::make_shared<TC::IO::TCParameterReader>(d->fromBatchPGPath);        
        auto prev_tup = param_reader->Read("Basic Analyzer[T]", d->fromBatchHyperName);
        auto prev_proc_name = std::get<0>(prev_tup);
        auto prev_param = std::get<1>(prev_tup);

        //d->share_param[d->fromBatchProc]->SetValue("Log", Log);
        //create time stamp
                 
        d->cur_paramchanged = false;
        QString paramMsg;
        if (prev_proc_name.compare(d->fromBatchProc) != 0) {//processor changed
            QLOG_INFO() << "Processor changed, flush exiting results";
            paramMsg = "Processor changed, flush exiting results";
            d->cur_paramchanged = true;
        }
        else if (nullptr == prev_param) {
            QLOG_INFO() << "Empty previous parameter, flush existing results";
            paramMsg = "Empty previous parameter, flush existing results";
            d->cur_paramchanged = true;
        }
        else if (!prev_param->IsValueEqual(d->share_param[d->fromBatchProc])) {
            QLOG_INFO() << "Parameter changed, flush existing results";
            paramMsg = "Parameter changed, flush existing results";
            d->cur_paramchanged = true;
        }
        QDir hyperDir(d->fromBatchPGPath.chopped(5) + "/" + d->fromBatchHyperName);
        if (false == hyperDir.exists()) {
            OnAcceptBatchRun();
        }
        else {
            auto total_count = hyperDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot).count() + hyperDir.entryList(QDir::Files).count();
            if (total_count < 1) {
                OnAcceptBatchRun();
            }
            else {
                d->fileChanges->showMaximized();
                d->fileChanges->SetParentPath(d->fromBatchPGPath.chopped(5) + "/" + d->fromBatchHyperName);

                if (d->cur_paramchanged) {
                    d->fileChanges->SetMessage(paramMsg);
                }
                else {
                    d->fileChanges->SetTcfPaths(d->cur_tcfs);
                    d->fileChanges->SetTimePoints(d->cur_timePoints);
                    d->fileChanges->ComposeTable();
                }
            }
        }        
    }
    auto MainWindow::CopyPath(QString src, QString dst) -> void {
        QDir dir(src);
        if (!dir.exists())
            return;

        foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString dst_path = dst + QDir::separator() + dd;
            dir.mkpath(dst_path);
            CopyPath(src + QDir::separator() + dd, dst_path);
        }

        foreach(QString f, dir.entryList(QDir::Files)) {
            QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
        }
    }

    auto MainWindow::HandleInternalExecution(QString param_name,QString algo_name)->void {
        //auto param = d->ui->parameterPanel->GetParameter(algo_name);
        auto param = d->ui->parameterPanel->GetParameter(param_name);

        auto connect_info = d->ui->parameterPanel->GetConnection();

        if(connect_info.contains(param_name)) {
            d->processor->SetReferenceImage(d->workingset->GetImage());
            auto info = connect_info[param_name];;
            auto targetAlgo = std::get<0>(info);
            auto variables = std::get<1>(info);

            auto value = d->processor->ImageToValue(algo_name, param);
            auto data = std::dynamic_pointer_cast<DataSet>(value->GetData(0));
            auto whole_param = std::shared_ptr<IParameter>(d->ui->parameterPanel->GetParameter()->Clone());
            auto child = whole_param->GetChild(targetAlgo + " Parameter");
            for (auto i = 0; i < variables.size(); i++) {
                auto variable = std::dynamic_pointer_cast<ScalarData>(data->GetData(variables[i]));
                auto val = variable->ValueAsDouble();
                child->SetValue(variables[i], val);
            }
            d->ui->parameterPanel->SetParameter(whole_param);

            d->ui->parameterPanel->SetNextHighlight(param_name);
        }      
    }
    void MainWindow::HandleAlgoExecution(QString param_name,QString algo_name) {
        auto isManual = d->ui->parameterPanel->GetCurrentMode();
        if(d->workingset->GetImageTimeSteps()>1) {
            if (!isManual) {
                HandleTimeLapseExecution(param_name,algo_name);
            }else {
                if(algo_name.contains("value")) {
                    HandleInternalExecution(param_name,algo_name);
                }
                else {
                    HandleSingleExecution(param_name,algo_name);
                }
            }
        }else {
            HandleSingleExecution(param_name,algo_name);
        }                
    }
    
    void MainWindow::HandleTimeChange(int time_step) {
        d->resultPanel->clearResult();
        d->resultPanel->hide();
                
        d->ui->viewerPanel->ClearMask();

        d->ui->parameterPanel->SetResultToggle(false);
        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(nullptr,
            reader.get(),mreader.get(),nullptr,rreader.get(),nullptr);

        if (false == procController.ChangeTimeStep(d->workingset,time_step)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
            OnTopWarning("Failed to open tcf.");
            return;
        }//change image and mask in workingset                

        //present changed information into panels
        //업데이트 된 working set에서 data가 존재하는 경우에만 presentation
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
            new Interactor::ScenePresenter(d->ui->viewerPanel,d->resultPanel.get(),
            d->ui->playerPanel,d->ui->parameterPanel,d->ui->VizControlPanel) };

        Interactor::BaSceneController sceneController(scenePresenter.get());

        if (false == sceneController.SetReferenceImage(d->workingset)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
            OnTopWarning("Failed to visualize reference image");
        }

        if (d->workingset->GetMaskValid()) {
            d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
            if (false == sceneController.SetMaskList(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
                OnTopWarning("Failed to update mask list");
            }

            if (false == sceneController.SetMaskImage(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                OnTopWarning("Failed to visualize mask image");
            }
        }

        if (d->workingset->GetMeausreValid()) {            
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
            if (false == sceneController.SetResultData(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
                OnTopWarning("Failed to visualize reference image");
                d->resultPanel->hide();
                d->ui->parameterPanel->SetResultToggle(false);
            }            
        }else {
            d->resultPanel->hide();
            d->ui->parameterPanel->SetResultToggle(false);
        }

        //mask나 measure가 하나라도 없으면        
        d->ui->parameterPanel->SetNoHighlight();
        if(d->ui->parameterPanel->GetTimePoints().isEmpty() && false == d->ui->parameterPanel->GetCurrentMode()) {
            d->ui->parameterPanel->SetTimeHighlight();
        }
        else if(!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }else if(!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }else {
            d->ui->parameterPanel->SetApplyHighlight();
        }
    }
    void MainWindow::HandleProcValChange(double val, QString sender) {
        Q_UNUSED(val)
        if(sender.contains("Manual")){//Manual인 경우에만
            auto module_name = sender.split("!")[0];
            auto variable_name = sender.split("!")[1];
                        
            auto moduleInstance = PluginRegistry::GetPlugin(module_name);
            auto param = d->ui->parameterPanel->GetParameter()->GetChild(moduleInstance->GetName() + " Parameter");
            d->processor->SetParameterValue(param);
        }
    }

    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        auto singleRun = true;
        auto batchRun = true;
        return std::make_tuple(singleRun,batchRun);
    }


    void MainWindow::HandleProcChange(QString proc_name) {
        QLOG_INFO() << "Current processor changed, flush existing results";
        //flush existing temporal files
        auto hyper_name = d->workingset->GetCurrentHyperCubeName();
        auto pg_path = d->workingset->GetWorkingDirectory();

        if(hyper_name.isEmpty()) {
            return;
        }
        if(pg_path.isEmpty()) {
            return;
        }
        auto file_base_name = d->workingset->GetCurrentImageName();
        auto hyper_path = pg_path.chopped(5) + "/" + hyper_name;
        QDir dir(hyper_path);
        auto file_list = dir.entryList(QDir::Filter::Files);
        for (auto f : file_list) {
            if (f.contains(file_base_name)) {
                auto file_path = hyper_path + "/" + f;
                QFile::remove(file_path);
            }
        }
        auto folder_list = dir.entryList(QDir::Filter::Dirs);
        for (auto fo : folder_list) {
            if (fo.contains(file_base_name)) {
                QDir dd = dir;
                dd.cd(fo);
                dd.removeRecursively();
            }
        }

        HandleTimeChange(d->workingset->GetImageTimeStep());
    }
    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        if(d->share_param.contains(name)) {
            return d->share_param[name];
        }
        return nullptr;
    }
    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
        d->share_param[name] = std::shared_ptr<IParameter>(param->Clone());
        d->ui->parameterPanel->SetProcessor(name);
        d->ui->batchParameterPanel->SetProcessor(name);
        d->ui->parameterPanel->SetParameter(d->share_param[name]);
        d->ui->batchParameterPanel->SetParameter(d->share_param[name]);
    }

    void MainWindow::OnChangedSingleTimePoints(QList<int> points) {
        //UI button 강조 update만
        d->ui->parameterPanel->SetNoHighlight();
        if(d->ui->parameterPanel->GetTimePoints().isEmpty()) {
            d->ui->parameterPanel->SetTimeHighlight();
        }
        d->ui->parameterPanel->SetDefaultHighlight();
        /*else if(!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }else if(!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }else {
            d->ui->parameterPanel->SetApplyHighlight();
        }*/
    }

    void MainWindow::OnChangedTimePoints(QList<int> points) {
        d->ui->batchListPanel->SetTimePoints(points);
    }

    void MainWindow::OnChangedTimePointAll(QList<int> points) {
        d->ui->batchListPanel->SetTimePointsAll(points);
    }

    void MainWindow::OnChangedTimePointCheck(QList<int> points) {
        d->ui->batchListPanel->SetTimePointsCheck(points);
    }


    void MainWindow::OnChangeMinTime(QString minPath) {
        auto workingset = std::make_shared<Entity::WorkingSet>();
        workingset->SetImagePath(minPath);

        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        auto cnt = metaReader->Read(minPath)->data.data3D.dataCount;
        workingset->SetImageTimeSteps(cnt);

        d->ui->batchParameterPanel->Update(workingset);
    }
    auto MainWindow::TryDeactivate() -> bool {
        //TODO clean part
        return true;
    }
    auto MainWindow::TryActivate() -> bool {
        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        d->fileChanges = new FileChangeInfo;
        d->fileChanges->setWindowTitle("File changes");

        d->fileValid = new FileValidInfo;
        d->fileValid->setWindowTitle("File validity");
        
        d->processor = std::make_shared<Plugins::Processor>();
        d->workingset = std::make_shared<Entity::WorkingSet>();
        d->resultPanel = std::make_shared<Plugins::ResultPanel>();
        d->resultPanel->setWindowTitle("Measurement");
        this->InitUI();

        d->screenshotWidget = std::make_shared<TC::ScreenShotWidget>();
        auto slayout = new QVBoxLayout;
        slayout->setContentsMargins(0, 0, 0, 0);        
        d->ui->screenshowContainer->setLayout(slayout);
        slayout->addWidget(d->screenshotWidget.get());

        d->ui->viewerPanel->ConnectScreenShowWidget(d->screenshotWidget);

        d->ui->tabWidget->tabBar()->setEnabled(false);

        auto procFolderList = d->appProperties["Processors"].toStringList();
        d->ui->parameterPanel->SetDefaultProcessors(procFolderList);
        d->ui->batchParameterPanel->SetDefaultProcessors(procFolderList);

        //start global communication
        subscribeEvent("TabChange");
        subscribeEvent(TC::Framework::MenuEvent::Topic());
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnTabFocused(ctkEvent)));

        connect(d->fileChanges, SIGNAL(sigAccept()), this, SLOT(OnAcceptBatchRun()));
        connect(d->fileChanges, SIGNAL(sigReject()), this, SLOT(OnRejectBatchRun()));

        connect(d->fileValid, SIGNAL(sigAccept()), this, SLOT(OnAcceptValidity()));
        connect(d->fileValid, SIGNAL(sigReject()), this, SLOT(OnRejectValidity()));

        connect(this, SIGNAL(singleTimeChanged(int)), this, SLOT(OnSingleTimeChanged(int)));
        connect(this, SIGNAL(singleTimeStarted(int)), this, SLOT(OnSingleTimeStarted(int)));
        connect(this, SIGNAL(singleFinished(int)), this, SLOT(OnSingleFinished(int)));

        connect(this, SIGNAL(batchTimeChanged(int)), this, SLOT(OnBatchTimeChanged(int)));
        connect(this, SIGNAL(batchTimeStarted(int,int)), this, SLOT(OnBatchTimeStarted(int,int)));
        connect(this, SIGNAL(batchFinished(int)), this, SLOT(OnBatchFinished(int)));

        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return (nullptr != d->ui);
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::GetFeatureName() const -> QString {
	    return "org.tomocube.basicanalysistimePlugin";
    }

    auto MainWindow::OnAccepted() -> void {
    }

    auto MainWindow::OnRejected() -> void {
    }

}
