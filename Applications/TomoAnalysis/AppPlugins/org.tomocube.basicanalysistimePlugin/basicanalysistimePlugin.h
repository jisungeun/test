#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.basicanalysistimePlugin";

namespace TomoAnalysis::BasicAnalysisTime::AppUI {
    class basicanalysistimPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.basicanalysistimePlugin")

    public:
        static basicanalysistimPlugin* getInstance();

        basicanalysistimPlugin();
        ~basicanalysistimPlugin();

        auto start(ctkPluginContext* context)->void override;
        //auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                        

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static basicanalysistimPlugin* instance;
    };
}