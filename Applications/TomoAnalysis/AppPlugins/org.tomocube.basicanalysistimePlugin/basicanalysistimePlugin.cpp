#include <QtPlugin>
#include <ctkPluginFrameworkLauncher.h>
#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoInfo.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoSeparator.h>
#include <SoTransferFunction2D.h>
#pragma warning(pop)

#include <AppEvent.h>
#include <MenuEvent.h>

#include "MainWindow.h"
#include "basicanalysistimePlugin.h"

namespace TomoAnalysis::BasicAnalysisTime::AppUI {
    struct basicanalysistimPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;        
        MainWindow* mainWindow{nullptr};
        AppInterfaceTA* appInterface{ nullptr };

        //Scene graph
        std::string cur_tcfname = "";
    };

    basicanalysistimPlugin* basicanalysistimPlugin::instance = 0;

    basicanalysistimPlugin* basicanalysistimPlugin::getInstance(){
        return instance;
    }
    basicanalysistimPlugin::basicanalysistimPlugin() : d(new Impl) {        
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);
    }
    basicanalysistimPlugin::~basicanalysistimPlugin() {
        
    }

    auto basicanalysistimPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);
    }    

    /*auto basicanalysistimPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);        
    }*/
    auto basicanalysistimPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }    
}
