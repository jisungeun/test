#pragma once

#include <QString>
#include <TCMeasure.h>
#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API IMeasureWriterPort {
    public:
        IMeasureWriterPort();
        virtual ~IMeasureWriterPort();

        virtual auto Write(TCMeasure::Pointer data, const QString& path)->bool = 0;
        virtual auto Modify(TCMeasure::Pointer data, const QString& path,int time_idx)->bool = 0;
    };
}