#pragma once

#include <memory>
#include <WorkingSet.h>

#include "IProcessingEngine.h"
#include "IImageReaderPort.h"
#include "IMaskReaderPort.h"
#include "IMeasureWriterPort.h"

#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
	class BasicAnalysisTimeUseCase_API ExecuteMeasure {
	public:
		ExecuteMeasure();
		virtual ~ExecuteMeasure();

		auto Request(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param,
			IProcessingEngine* engine, IMeasureWriterPort* writer,bool saveResult)->bool;

		auto Refresh(QString playground_path, QString image_path, QString hypercube_name,int time_step, QString userName,IProcessingEngine* engine, IImageReaderPort* ireader, IMaskReaderPort* mreader, IMeasureWriterPort* writer)->bool;
	private:
		auto CopyPath(QString src, QString dst)->void;
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}