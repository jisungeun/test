#pragma once

#include <WorkingSet.h>

#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API ISceneManagerPort {
    public:        
        ISceneManagerPort();
        virtual ~ISceneManagerPort();

        virtual auto UpdateImage(const IBaseImage::Pointer& image)->void = 0;
        virtual auto UpdateMask(const IBaseMask::Pointer& mask,const QString& organ_name)->bool = 0;
        virtual auto UpdateMultiMasks(const IBaseMask::Pointer& instMask, const IBaseMask::Pointer& organMask, const QStringList& organ_names)->bool = 0;
        virtual auto UpdateMeasure(const IBaseData::Pointer& measure)->void = 0;
        virtual auto UpdateRange(const Entity::WorkingSet::Pointer& workingset)->void = 0;
        virtual auto Update(const Entity::WorkingSet::Pointer& workingset) ->void = 0;
    };
}