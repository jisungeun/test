#pragma once

#include <QString>
#include <TCMeasure.h>
#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API IMeasureReaderPort {
    public:
        IMeasureReaderPort();
        virtual ~IMeasureReaderPort();

        virtual auto Read(const QString& path,int time_step)->TCMeasure::Pointer = 0;
    };
}