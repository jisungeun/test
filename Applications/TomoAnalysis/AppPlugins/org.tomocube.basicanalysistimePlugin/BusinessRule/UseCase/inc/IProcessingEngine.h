#pragma once

#include <memory>

#include <IBaseData.h>
#include <IBaseImage.h>
#include <IBaseMask.h>
#include <IParameter.h>
#include <DataList.h>

#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API IProcessingEngine {        
    public:
        IProcessingEngine();
        virtual ~IProcessingEngine();

        virtual auto SetReferenceImage(IBaseImage::Pointer image)->void = 0;
                
        //virtual auto ImageToBinaryMask(const QString& module_name, IParameter::Pointer param)->TCMask::Pointer = 0;
        virtual auto ImageToSingleMask(const QString& module_name,IParameter::Pointer param)->std::tuple<IBaseMask::Pointer,IBaseMask::Pointer> =0;
        virtual auto ImageToBoth(const QString& module_name, IParameter::Pointer param)->std::tuple<IBaseMask::Pointer, IBaseMask::Pointer> = 0;        
        virtual auto BinaryMaskToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param)->IBaseMask::Pointer = 0;        
        virtual auto Measurement(const QString& module_name, IBaseMask::Pointer organ_mask, IBaseMask::Pointer label_mask, IParameter::Pointer param)->IBaseData::Pointer = 0;
        virtual auto ImageToValue(const QString& module_name,IParameter::Pointer param)->DataList::Pointer=0;
        virtual auto LabelToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param)->IBaseMask::Pointer = 0;
    };
}
