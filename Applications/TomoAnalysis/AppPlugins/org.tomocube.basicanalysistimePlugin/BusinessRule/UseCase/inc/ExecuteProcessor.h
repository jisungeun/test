#pragma once

#include <memory>

#include <WorkingSet.h>

#include "IMaskWriterPort.h"
#include "IMeasureWriterPort.h"
#include "IProcessingEngine.h"

#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API ExecuteProcessor {
    public:
        ExecuteProcessor();
        virtual ~ExecuteProcessor();

        auto Request(Entity::WorkingSet::Pointer workingset,const QString& proc_name,IParameter::Pointer param,
            IProcessingEngine* engine,IMaskWriterPort* maskWriter,IMeasureWriterPort* measureWriter,bool saveResult = true)->bool;
    private:
        auto parseResultFormat(const QString& formats)->void;        
        auto handleMask(Entity::WorkingSet::Pointer workingset,IBaseData::Pointer data,QString name,
            QString savePath,QStringList layer_list,IMaskWriterPort* writer)->void;
        auto handleMeasure(Entity::WorkingSet::Pointer workingset,IBaseData::Pointer data,QString savePath,
            IMeasureWriterPort* writer)->void;
    };
}