#pragma once

#include <memory>
#include <QString>

#include <WorkingSet.h>

#include "IMaskWriterPort.h"
#include "IProcessingEngine.h"
#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
	class BasicAnalysisTimeUseCase_API ExecuteSegmentation {
	public:
		ExecuteSegmentation();
		virtual ~ExecuteSegmentation();
				
		auto Request(Entity::WorkingSet::Pointer workingset,const QString& algo_name,IParameter::Pointer param,
			IProcessingEngine* engine,IMaskWriterPort* writer,bool saveMask = true)->bool;
		auto RequestLabeling(Entity::WorkingSet::Pointer workingset,const QString& algo_name,IParameter::Pointer param,
			IProcessingEngine* engine,IMaskWriterPort* writer,bool saveMask = true)->bool;
		auto RequestPostProcessing(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* writerPort, bool saveMask = true)->bool;

	private:
		auto GenerateTwoMasks(Entity::WorkingSet::Pointer workingset,const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine,IMaskWriterPort* writer)->void;

	};
}