#pragma once

#include <QString>
#include <IBaseMask.h>
#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API IMaskReaderPort {
    public:
        IMaskReaderPort();
        virtual ~IMaskReaderPort();

        virtual auto Read(const QString& path,int time_idx =0, const bool loadMaskVolume = false,const bool isLabel = true) const->IBaseMask::Pointer = 0;
        virtual auto Create(int x0, int y0, int z0, int x1, int y1, int z1,int code)->IBaseMask::Pointer = 0;
    };
}