#pragma once

#include <QString>
#include <TCImage.h>

#include "BasicAnalysisTimeUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    class BasicAnalysisTimeUseCase_API IImageReaderPort {
    public:
        IImageReaderPort();
        virtual ~IImageReaderPort();

        virtual auto Read(const QString& path, const bool loadImage = false,int time_step =0)const->TCImage::Pointer=0;
        virtual auto GetTimeSteps(const QString& path)const ->int = 0;
    };
}