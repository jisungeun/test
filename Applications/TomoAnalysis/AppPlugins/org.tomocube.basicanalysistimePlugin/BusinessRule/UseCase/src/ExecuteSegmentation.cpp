#include <iostream>

#include <QDir>

#include "ISegmentationAlgorithm.h"

#include <PluginRegistry.h>
#include <TCMask.h>

#include "ExecuteSegmentation.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {    
    ExecuteSegmentation::ExecuteSegmentation(){        
    }
    ExecuteSegmentation::~ExecuteSegmentation() {
        
    }
    auto ExecuteSegmentation::RequestPostProcessing(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* writerPort, bool saveMask) -> bool {
        workingset->SetParameter(param);
        auto moduleInstance = PluginRegistry::GetPlugin(algo_name);
        if (nullptr == moduleInstance) {
            return false;
        }

        auto borderParam = param->GetChild(moduleInstance->GetName() + " Parameter");
        bool isBorderKill{ false };
        if (borderParam->ExistNode("Method!Enabler")) {
            isBorderKill = borderParam->GetValue("Method!Enabler").toBool();
        }
        else {
            isBorderKill = borderParam->GetValue("Method").toBool();
        }
        if (!isBorderKill) {
            return true;
        }

        auto segmentationModule = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
        auto outputType = moduleInstance->GetOutputFormat();
        workingset->SetCurrentOutputFormat(outputType);

        auto split = outputType.split("!");
        if (split.size() < 1) {
            return false;
        }

        auto input_mask = workingset->GetMask("CellInst");
        if (nullptr == input_mask) {
            return false;
        }

        auto instance = std::dynamic_pointer_cast<TCMask>(engine->LabelToLabel(algo_name, input_mask, param));
        workingset->RemoveMask(instance->GetName());
        workingset->AddMask(instance, instance->GetName());
        workingset->SetMaskValid(true);
        if (saveMask) {
            auto parent_path = workingset->GetWorkingDirectory();

            auto hyperName = workingset->GetCurrentHyperCubeName();

            auto parent_folder = parent_path.chopped(5);
            QDir pdir(parent_folder);
            if (false == pdir.exists()) {
                QDir().mkdir(parent_folder);
            }
            if (false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);

            workingset->ClearMaskPath();
            for (const auto mask : workingset->GetMasks()) {
                auto oivmask = std::dynamic_pointer_cast<TCMask>(mask);
                oivmask->SetTimeStep(workingset->GetImageTimeStep());
                auto path = pdir.path() + "/" + workingset->GetCurrentImageName() + ".msk";
                auto namelist = segmentationModule->GetLayerNames(oivmask->GetName());
                writerPort->SetLayerNames(namelist);
                writerPort->Write(mask, path);
                workingset->SetMaskPath(path);
            }
        }
        segmentationModule = nullptr;
        moduleInstance = nullptr;
        return true;
    }

    auto ExecuteSegmentation::RequestLabeling(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* writer, bool saveMask) -> bool {
        workingset->SetParameter(param);
        auto moduleInstance = PluginRegistry::GetPlugin(algo_name);
        if(nullptr == moduleInstance) {
            return false;
        }

        auto segmentationModule = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
        auto outputType = moduleInstance->GetOutputFormat();
        workingset->SetCurrentOutputFormat(outputType);

        auto split = outputType.split("!");
        if(split.size()<2) {
            return false;
        }
        
        auto input_mask = std::dynamic_pointer_cast<TCMask>(workingset->GetMask("CellInst"));

        if(nullptr == input_mask) {
            return false;
        }

        auto instance = std::dynamic_pointer_cast<TCMask>(engine->BinaryMaskToLabel(algo_name,input_mask,param));
        
        workingset->RemoveMask(instance->GetName());
        workingset->AddMask(instance,instance->GetName());
        workingset->SetMaskValid(true);

        if(saveMask){        
            auto parent_path = workingset->GetWorkingDirectory();

            auto hyperName = workingset->GetCurrentHyperCubeName();

            auto parent_folder = parent_path.chopped(5);
            QDir pdir(parent_folder);
            if(false == pdir.exists()) {
                QDir().mkdir(parent_folder);
            }
            if(false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);

            workingset->ClearMaskPath();
            for(const auto mask : workingset->GetMasks()) {
                auto oivmask = std::dynamic_pointer_cast<TCMask>(mask);
                oivmask->SetTimeStep(workingset->GetImageTimeStep());
                auto path = pdir.path() + "/" + workingset->GetCurrentImageName() + ".msk";
                auto namelist = segmentationModule->GetLayerNames(oivmask->GetName());
                writer->SetLayerNames(namelist);
                writer->Write(mask, path);
                workingset->SetMaskPath(path);
            }
        }
        segmentationModule = nullptr;
        moduleInstance = nullptr;
        return true;
    }
    auto ExecuteSegmentation::Request(Entity::WorkingSet::Pointer workingset,const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* writer,bool saveMask) -> bool {        
        workingset->SetParameter(param);                        
                
        auto moduleInstance = PluginRegistry::GetPlugin(algo_name);
        if(nullptr == moduleInstance) {
            return false;
        }

        auto segmentationModule = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
        
        auto outputType = moduleInstance->GetOutputFormat();
        workingset->SetCurrentOutputFormat(outputType);
        auto split = outputType.split("!");
        if(split.size()<2) {
            return false;
        }

        if(split.size()==2) {
            engine->SetReferenceImage(workingset->GetImage());
            auto masks = engine->ImageToSingleMask(algo_name, param);
            auto cellinst = std::dynamic_pointer_cast<TCMask>(std::get<0>(masks));
            auto cellorgan = std::dynamic_pointer_cast<TCMask>(std::get<1>(masks));            
            workingset->RemoveMask(cellinst->GetName());
            workingset->AddMask(cellinst, cellinst->GetName());
            workingset->RemoveMask(cellorgan->GetName());
            workingset->AddMask(cellorgan, cellorgan->GetName());
            workingset->SetMaskValid(true);
        }else if(split.size()==3){
            engine->SetReferenceImage(workingset->GetImage());            
            auto masks = engine->ImageToBoth(algo_name, param);
            auto cellinst = std::dynamic_pointer_cast<TCMask>(std::get<0>(masks));
            auto cellorgan = std::dynamic_pointer_cast<TCMask>(std::get<1>(masks));            
            if(nullptr == cellinst || nullptr == cellorgan) {
                return false;
            }
            if(cellinst->GetLayerNames().isEmpty() || cellorgan->GetLayerNames().isEmpty()) {
                return false;
            }

            workingset->RemoveMask(cellinst->GetName());
            workingset->AddMask(cellinst,cellinst->GetName());
            workingset->RemoveMask(cellorgan->GetName());
            workingset->AddMask(cellorgan,cellorgan->GetName());
            workingset->SetMaskValid(true);
        }
        if (saveMask) {
            auto parent_path = workingset->GetWorkingDirectory();

            auto hyperName = workingset->GetCurrentHyperCubeName();

            auto parent_folder = parent_path.chopped(5);
            QDir pdir(parent_folder);
            if (false == pdir.exists()) {
                QDir().mkdir(parent_folder);
            }
            if (false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);

            workingset->ClearMaskPath();

            for (const auto mask : workingset->GetMasks()) {
                auto oivmask = std::dynamic_pointer_cast<TCMask>(mask);
                auto time_step = workingset->GetImageTimeStep();
                oivmask->SetTimeStep(time_step);
                auto path = pdir.path() + "/" + workingset->GetCurrentImageName() + ".msk";
                auto namelist = segmentationModule->GetLayerNames(oivmask->GetName());
                writer->SetLayerNames(namelist);
                writer->Write(mask, path);
                workingset->SetMaskPath(path);
            }
        }
        return true;
    }
    auto ExecuteSegmentation::GenerateTwoMasks(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* writer) -> void {
        Q_UNUSED(writer)
        auto masks = engine->ImageToBoth(algo_name, param);
        auto inst_mask = std::dynamic_pointer_cast<TCMask>(std::get<0>(masks));
        auto organ_mask = std::dynamic_pointer_cast<TCMask>(std::get<1>(masks));

        //update working set
        workingset->AddMask(inst_mask, "cellInst");
        workingset->AddMask(organ_mask, "CellOrgan");
    }

}
