#include "SaveResult.h"

#include <iostream>
#include <fstream>
#include <QFileInfo>
#include <QDir>

namespace TomoAnalysis::BasicAnalysisTime::UseCase {    
    SaveResult::SaveResult(){        
    }
    SaveResult::~SaveResult() {
        
    }
    auto SaveResult::SaveBatchMeasure(const QString& path, const QString& hyperName, Entity::WorkingSet::Pointer workingset, IMeasureReaderPort* port) -> bool {
        Q_UNUSED(path)
        Q_UNUSED(hyperName)
        if(nullptr == workingset) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        auto playgroundPath = workingset->GetWorkingDirectory() + ".tcpg";

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();        
        return true;
    }
    auto SaveResult::SaveMasks(Entity::WorkingSet::Pointer workingset, IMaskWriterPort* port) -> bool {
        if(nullptr == port) {
            return false;
        }
        if (workingset->GetMaskPath().isEmpty()) {
            return false;
        }
        if(workingset->GetMasks().size()<1) {
            return false;
        }
        auto maskPath = workingset->GetMaskPath();

        auto instmask = workingset->GetMask("CellInst");
        if (nullptr != instmask) {
            port->SetLayerNames(instmask->GetLayerNames());
            port->Write(instmask, maskPath, workingset->GetImageTimeStep());
        }
        auto organmask = workingset->GetMask("CellOrgan");
        if (nullptr != organmask) {
            port->SetLayerNames(organmask->GetLayerNames());
            port->Write(organmask, maskPath, workingset->GetImageTimeStep());
        }
        return true;        
    }

    auto SaveResult::addMetric(QString measure) -> QString {
        if (measure.contains("Volume")) {
            return QString("Volume(um^3)");
        }
        if (measure.contains("SurfaceArea")) {
            return QString("Surface area(um^2)");
        }
        if (measure.contains("ProjectedArea")) {
            return QString("Projected area(um^2)");
        }
        if (measure.contains("Concentration")) {
            return QString("Concentration(pg/um^3)");
        }
        if (measure.contains("Drymass")) {
            return QString("Dry mass(pg)");
        }

        return measure;
    }

    auto SaveResult::SaveCurrentMeasure(const QString& path, Entity::WorkingSet::Pointer workingset, IMeasureReaderPort* port) -> bool {
        if(nullptr == workingset) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        auto measurePath = workingset->GetMeasurePath();
        auto time_steps = workingset->GetImageTimeSteps();
        auto refMeasure = port->Read(measurePath, 0);
        auto measureNames = refMeasure->GetKeys();
        auto organNames = refMeasure->GetOrganNames();

        //write table header
        std::ofstream new_csv;
        new_csv.open(path.toStdWString());
        new_csv << "Time step,Cell#,Organ";
        for(const auto& measure : measureNames) {
            auto with_metric = addMetric(measure);
            new_csv << "," << with_metric.toStdString();
        }
        new_csv << "\n";

        QMap<QString, QString> convertingTable;
        convertingTable["lipid droplet"] = "Lipid droplet";
        convertingTable["membrane"] = "Whole Cell";
        convertingTable["nucleoli"] = "Nucleolus";
        convertingTable["nucleus"] = "Nucleus";

        for(auto i=0;i<time_steps;i++) {
            auto singleMeasure = port->Read(measurePath, i);
            auto cellCount = singleMeasure->GetCellCount();            
            for(auto cc = 0 ;cc < cellCount;cc++) {
                for(const auto& organ : organNames) {
                    auto real_organ = organ;
                    if(convertingTable.contains(real_organ)) {
                        real_organ = convertingTable[real_organ];
                    }
                    new_csv << i << "," << (cc + 1) << "," << real_organ.toStdString();
                    for(const auto& measure : measureNames) {
                        auto final_key = organ + "." + measure;
                        auto value = singleMeasure->GetMeasure(final_key, cc);
                        QString val_str = QString::number(value, 'f', 2);
                        //new_csv <<","<< value;
                        new_csv << "," << val_str.toStdString();
                    }
                    new_csv << "\n";
                }
            }
        }
        new_csv.close();
        return true;
    }    
}
