#define LOGGER_TAG "[BasicAnalyzerT]"
#include <TCLogger.h>

#include <iostream>
#include <QFileInfo>

#include <TCMask.h>
#include <WorkingSet.h>

#include <TCFMetaReader.h>

#include "OpenDataSet.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    struct OpenDataSet::Impl {        
    };
    OpenDataSet::OpenDataSet() : d{ new Impl } {        
    }
    OpenDataSet::~OpenDataSet() {
        
    }
    auto OpenDataSet::OpenTcfImage(const QString& path,const QString& playPath, IImageReaderPort* reader, Entity::WorkingSet::Pointer workingset,int time_step) const -> bool {
        auto time_steps = reader->GetTimeSteps(path);
        if(time_steps<0) {
            return false;
        }
        workingset->SetImageTimeSteps(time_steps);
        workingset->SetImageTimeStep(time_step);
        workingset->SetImagePath(path);
        auto image = reader->Read(path,true,time_step);
        workingset->SetImage(image);        
        QFileInfo info(path);
        auto filename = info.fileName().chopped(4);
        workingset->SetCurrentImageName(filename);
        workingset->SetWorkingDirectory(playPath);               

        TC::IO::TCFMetaReader mreader;
        auto meta = mreader.Read(path);

        workingset->SetImageTimePoints(meta->data.data3D.timePoints);

        return true;
    }
    auto OpenDataSet::OpenTimedTcf(Entity::WorkingSet::Pointer workingset, IImageReaderPort* reader) const -> bool {        
        auto time_step = workingset->GetImageTimeStep();
        auto path = workingset->GetImagePath();
        auto image = reader->Read(path, true, time_step);

        if(nullptr == image) {
            return false;
        }

        workingset->SetImage(image);
        return true;
    }
    auto OpenDataSet::OpenTimedMask(Entity::WorkingSet::Pointer workingset, IMaskReaderPort* reader) const -> bool {       
        auto time_step = workingset->GetImageTimeStep();        
        auto maskPath = workingset->GetMaskPath();
        auto img = std::dynamic_pointer_cast<TCImage>(workingset->GetImage());
        if (nullptr == img) {
            return false;
        }

        if(maskPath.isEmpty()) {
            return false;
        }

        bool isMask = false;
        TCMask::Pointer labelMask = nullptr;
        labelMask = std::dynamic_pointer_cast<TCMask>(reader->Read(maskPath, time_step, true));
        if (nullptr != labelMask) {
            workingset->RemoveMask("CellInst");
            workingset->AddMask(labelMask, "CellInst");
            isMask = true;
        }
        TCMask::Pointer organMask = nullptr;
        organMask = std::dynamic_pointer_cast<TCMask>(reader->Read(maskPath, time_step, true, false));
        if (nullptr != organMask) {
            workingset->RemoveMask("CellOrgan");
            workingset->AddMask(organMask, "CellOrgan");
            isMask = true;
        }

        return isMask;
    }
    auto OpenDataSet::OpenTimedResult(Entity::WorkingSet::Pointer workingset, IMeasureReaderPort* reader) const -> bool {
        auto time_step = workingset->GetImageTimeStep();
        auto measurePath = workingset->GetMeasurePath();
        if(false == QFileInfo::exists(measurePath)) {
            return false;
        }
        auto measure = reader->Read(measurePath, time_step);                
        if (measure->GetCellCount() < 1) {
            QLOG_ERROR() << "There is no pre computed measurement result";
            return false;
        }
        measure->SetTimeIndex(time_step);
        measure->SetTimePoint(workingset->GetImageTimePoints()[time_step]);
        workingset->SetMeasure(measure);        
        return true;
    }


}