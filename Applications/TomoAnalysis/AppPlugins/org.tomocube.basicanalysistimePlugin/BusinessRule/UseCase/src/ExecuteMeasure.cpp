#pragma once

#include <iostream>
#include <QDir>
#include <QThread>
#include <QDirIterator>

#include <PluginRegistry.h>
#include <ProjectDataReader.h>
#include <ParameterReader.h>
#include <TCParameterWriter.h>
#include <IProcessor.h>

#include "ExecuteMeasure.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {
    struct ExecuteMeasure::Impl {
        QThread* thread;
    };
    ExecuteMeasure::ExecuteMeasure() : d{ new Impl } {        
    }
    ExecuteMeasure::~ExecuteMeasure() {

    }
    auto ExecuteMeasure::Refresh(QString playground_path, QString image_path, QString hypercube_name, int time_step,QString userName, IProcessingEngine* engine, IImageReaderPort* ireader, IMaskReaderPort* mreader, IMeasureWriterPort* writer) -> bool {
        auto current = QDateTime::currentDateTime();
        auto timeStamp = current.toString("yyMMdd.hhmmss");

        //load playground
        auto pgReader = std::make_shared<ProjectDataReader>();
        auto pgData = std::make_shared<PlaygroundInfo>();
        pgReader->ReadPlaygroundData(playground_path, pgData);
        auto pgName = pgData->GetName();
        QFileInfo pgFile(playground_path);
        auto pgBase = pgFile.fileName().chopped(5);
        auto osplit = pgBase.split("_");
        auto originalTime = osplit[osplit.count()-1];

        auto original_pgPath = playground_path;        
        QDir fpdir(original_pgPath);
        fpdir.cdUp();
        fpdir.cd(hypercube_name);
        fpdir.cd(originalTime);

        auto historyPath = fpdir.path();

        auto cubeName = QString();
        for(auto hy : pgData->GetHyperCubeList()) {
            if(hy->GetName() == hypercube_name) {
                for(auto cu : hy->GetCubeList()) {
                    bool findCube = false;
                    for(auto f : cu->GetTCFDirList()) {
                        if(f->GetPath() == image_path) {
                            cubeName = cu->GetName();
                            findCube = true;
                            break;
                        }
                    }
                    if(findCube) {
                        break;
                    }
                }
                break;
            }
        }

        QFileInfo tcfInfo(image_path);
        auto tcfBase = tcfInfo.fileName().chopped(4);
        auto organ_path = historyPath + "/" + cubeName + "/" + tcfBase + "_MultiLayer_" + userName + ".msk";        
        auto inst_path = historyPath + "/" + cubeName + "/" + tcfBase + "_MultiLabel_" + userName + ".msk";        
        auto ori_rep = historyPath + "/" + cubeName + "/" + tcfBase + ".rep";
        auto report_path = historyPath + "/" + cubeName + "/" + tcfBase + "_" + userName + ".rep";        
        QFile::copy(ori_rep, report_path);
                
        //load paraemter
        auto paramPath = playground_path.chopped(5) + ".param";
        auto paramReader = std::make_shared<ParameterReader>();
        paramReader->SetPath(paramPath);
        auto procName = paramReader->ReadProp("Processor");

        auto procInstance = PluginRegistry::GetPlugin(procName);
        if(nullptr == procInstance) {
            return false;
        }
        auto procModule = std::dynamic_pointer_cast<IProcessor>(procInstance);
        if (!procModule) {
            return false;
        }
        QString measureName;
        for(auto n :procModule->Parameter()->GetNames()){
            if(n.contains("Measurement")) {
                measureName = n;
                break;
            }
        }
        if(measureName.isEmpty()) {
            return false;
        }

        auto algo_name = procModule->Parameter()->GetValue(measureName).toString();

        auto param = paramReader->Read();
        param->SetFullName(procName);

        auto image = ireader->Read(image_path, true,time_step);

        auto organ_mask = mreader->Read(organ_path, time_step, true, false);

        auto inst_mask = mreader->Read(inst_path, time_step, true, true);

        engine->SetReferenceImage(image);
        auto measure = std::dynamic_pointer_cast<TCMeasure>(engine->Measurement(algo_name, organ_mask, inst_mask, param));

        writer->Modify(measure, report_path,time_step);        

        QDir ddir(original_pgPath);
        ddir.cdUp();
        ddir.cdUp();
        auto oripg = ddir.path() + "/" + pgName + ".tcpg";
        auto pwriter = std::make_shared<TC::IO::TCParameterWriter>(oripg);
        pwriter->Write(param, "Basic Analyzer[T]", "Report[T]", hypercube_name, procName, timeStamp);

        QStringList tcfs;
        QVector<int> cubeCounts;
        QStringList cubes;
        HyperCube::Pointer selectedhyper = nullptr;
        for (auto h : pgData->GetHyperCubeList()) {
            if (h->GetName() == hypercube_name) {
                selectedhyper = h;
                break;
            }
        }
        if (nullptr == selectedhyper) {
            return false;
        }
        for (auto c : selectedhyper->GetCubeList()) {
            if (c->GetTCFDirList().count() > 0) {
                cubes.push_back(c->GetName());
                for (auto t : c->GetTCFDirList()) {
                    auto pp = t->GetPath();                    
                    tcfs.push_back(t->GetPath());
                }
                cubeCounts.push_back(c->GetTCFDirList().count());
            }
        }

        auto repFolderPath = original_pgPath.chopped(5);        
        QDir pdir(repFolderPath);
        pdir.cdUp();//playground folder        
        pdir.cd("history");
        pdir.cd(hypercube_name);
        pdir.cd(timeStamp);        

        auto targetFolderPath = pdir.path();

        QDir cdir(targetFolderPath);
        for (auto c : cubes) {
            if (!cdir.exists(c)) {
                cdir.mkdir(c);
            }
        }

        auto cubeCntIdx = 0;
        auto curCnt = 0;
        for (auto tcfName : tcfs) {
            QString ccc;
            if (cubeCounts[cubeCntIdx] > curCnt) {
                curCnt++;
                ccc = cubes[cubeCntIdx];
            }
            else {
                curCnt = 1;
                ccc = cubes[++cubeCntIdx];
            }
            QFileInfo info(tcfName);
            auto filename = info.fileName().chopped(4);
            auto reportPath = historyPath + "/" + ccc + "/" + filename + ".rep";
            auto instPath = historyPath + "/" + ccc + "/" + filename + "_MultiLabel.msk";
            auto organPath = historyPath + "/" + ccc + "/" + filename + "_MultiLayer.msk";
            if (filename == tcfBase) {
                instPath = historyPath + "/" + ccc + "/" + filename + "_MultiLabel_"+userName+".msk";
                organPath = historyPath + "/" + ccc + "/" + filename + "_MultiLayer_"+userName+".msk";
                reportPath = historyPath + "/" + ccc + "/" + filename +"_" + userName + ".rep";
            }
            if (QFileInfo::exists(reportPath)) {
                //if report file exist copy it to target folder                
               
                auto targetPath = targetFolderPath + "/" + ccc + "/" + filename + ".rep";
                auto targetInstPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLabel.msk";
                auto targetOrganPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLayer.msk";
                if(filename == tcfBase) {
                    targetInstPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLabel_" + userName + ".msk";
                    targetOrganPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLayer_" + userName + ".msk";
                }
                QFile::copy(reportPath, targetPath);                
                QFile::copy(instPath, targetInstPath);
                QFile::copy(organPath, targetOrganPath);
                CopyPath(instPath.chopped(4), targetInstPath.chopped(4));
                CopyPath(organPath.chopped(4), targetOrganPath.chopped(4));
            }
        }
        //copy current playground
        auto copyPGPath = original_pgPath.chopped(5);
        QDir zdir(copyPGPath);
        zdir.cdUp();
        zdir.cd("history");

        copyPGPath = zdir.path();
        copyPGPath += "/";
        copyPGPath += hypercube_name;
        copyPGPath += "_";
        copyPGPath += timeStamp;
        copyPGPath += ".tcpg";
        QFile::copy(playground_path, copyPGPath);        
        QFile::remove(inst_path);
        QFile::remove(organ_path);
        QFile::remove(report_path);
        QDir idir(inst_path.chopped(4));
        if (idir.exists()) {
            idir.removeRecursively();
        }
        QDir odir(organ_path.chopped(4));
        if(odir.exists()) {
            odir.removeRecursively();
        }        

        //check backup
        QDirIterator it(historyPath + "/" + cubeName, QStringList() << "*.msk", QDir::Files);
        while (it.hasNext()) {
            auto ff = it.next();
            if (ff.contains("_bak.msk")) {
                auto restore = ff.chopped(8) + ".msk";
                QFile::copy(ff, restore);
                CopyPath(ff.chopped(4), restore.chopped(4));
                QFile::remove(ff);
                QDir fdir(ff.chopped(4));
                fdir.removeRecursively();
            }
        }

        return true;
    }
    auto ExecuteMeasure::CopyPath(QString src, QString dst) -> void {
        QDir dir(src);
        if (!dir.exists())
            return;

        foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString dst_path = dst + QDir::separator() + dd;
            dir.mkpath(dst_path);
            CopyPath(src + QDir::separator() + dd, dst_path);
        }

        foreach(QString f, dir.entryList(QDir::Files)) {
            QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
        }
    }

    auto ExecuteMeasure::Request(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param, IProcessingEngine* engine, IMeasureWriterPort* writer,bool saveResult) -> bool {        
        workingset->SetParameter(param);

        auto moduleInstance = PluginRegistry::GetPlugin(algo_name);
        if(nullptr == moduleInstance) {
            return false;
        }
        auto outputType = moduleInstance->GetOutputFormat();
        workingset->SetCurrentOutputFormat(outputType);
        auto type = outputType.split("!")[0];
        if(type.compare("Measure")!=0) {
            return false;
        }

        engine->SetReferenceImage(workingset->GetImage());        
        auto cell_organ = workingset->GetMask(QString("CellOrgan"));
        auto cell_inst = workingset->GetMask(QString("CellInst"));
        if(nullptr == cell_organ || nullptr == cell_inst) {
            return false;
        }
        auto names = cell_organ->GetLayerNames();        
        auto measure = std::dynamic_pointer_cast<TCMeasure>(engine->Measurement(algo_name,cell_organ,cell_inst,param));

        workingset->SetMeasure(measure);
        workingset->SetMeasureValid(true);
        if (saveResult) {
            auto parent_path = workingset->GetWorkingDirectory();

            auto hyperName = workingset->GetCurrentHyperCubeName();

            auto parent_folder = parent_path.chopped(5);
            QDir pdir(parent_folder);
            if (false == pdir.exists()) {
                QDir().mkdir(parent_folder);
            }
            if (false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);

            auto path = pdir.path() + "/" + workingset->GetCurrentImageName() + ".rep";

            auto time_idx = workingset->GetImageTimeStep();
            auto time_interval = workingset->GetTimeInterval();

            measure->SetTimeIndex(time_idx);
            measure->SetTimeInterval(time_interval);
            measure->SetTimePoint(workingset->GetImageTimePoints()[time_idx]);

            writer->Write(measure, path);            
        }
        return true;
    }    
}
