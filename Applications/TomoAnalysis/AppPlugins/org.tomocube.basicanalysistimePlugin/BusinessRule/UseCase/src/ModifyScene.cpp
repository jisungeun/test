#include <iostream>

#include <QMap>

#include "ModifyScene.h"

namespace TomoAnalysis::BasicAnalysisTime::UseCase {    
    ModifyScene::ModifyScene() {        
    }

    ModifyScene::~ModifyScene() {
        
    }

    auto ModifyScene::SetMaskList(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port) -> bool {
        if(true == workingset->GetMasks().isEmpty()) {
            return false;
        }
        port->Update(workingset);
        return true;
    }

    auto ModifyScene::SetImage(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port) -> bool {
        if(nullptr == workingset->GetImage()) {
            return false;
        }
        port->UpdateImage(workingset->GetImage());
        port->UpdateRange(workingset);
        return true;
    }
    auto ModifyScene::SetMask(Entity::WorkingSet::Pointer workingset,const QString& mask_name, ISceneManagerPort* port) -> bool {
        if(true == workingset->GetMasks().isEmpty()) {
            return false;
        }

        QString name = mask_name;        
        if(mask_name.compare("cellInst")==0 || mask_name.compare("Cell Instance")==0) {
            name = "CellInst";
        }else {
            name = "CellOrgan";
        }
        auto mask = workingset->GetMask(name);

        return port->UpdateMask(mask, mask_name);        
    }

    auto ModifyScene::SetMultiMask(Entity::WorkingSet::Pointer workingset, const QStringList& multi_mask, ISceneManagerPort* port) -> bool {
        if(true == workingset->GetMasks().isEmpty()) {
            return false;
        }
        IBaseMask::Pointer instMask{ nullptr };
        IBaseMask::Pointer organMask{ nullptr };
        for(auto m : multi_mask) {
            if(m.compare("cellInst")==0 || m.compare("Cell Instance")==0) {
                instMask = workingset->GetMask("CellInst");
            }
            else {
                organMask = workingset->GetMask("CellOrgan");
            }
        }

        return port->UpdateMultiMasks(instMask, organMask, multi_mask);
    }


    auto ModifyScene::SetReport(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port) -> bool {
        if(nullptr == workingset->GetMeasure()) {
            return false;
        }
        port->UpdateMeasure(workingset->GetMeasure());
        return true;
    }


}