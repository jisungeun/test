#include <QMap>

#include "Algorithm.h"

namespace TomoAnalysis::BasicAnalysisTime::Entity {
    struct Algorithm::Impl {
      //TCFDir list  
    };
    Algorithm::Algorithm() : d{ new Impl } {
        
    }
    Algorithm::Algorithm(const Algorithm& other) : d{ new Impl } {
        *d = *other.d;
    }
    Algorithm::~Algorithm() {
        
    }
    auto Algorithm::Init() -> void {
        
    }
}