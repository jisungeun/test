#include <iostream>
#include <QFileInfo>
#include <QMutexLocker>

#include "WorkingSet.h"

namespace TomoAnalysis::BasicAnalysisTime::Entity {
    struct WorkingSet::Impl {
        QString workingDir;
        QString hypercubeName;
        QString imageName;
        QString outputFormat;
        double timeInterval;

        QString maskDir;
        QString imageDir;
        TCImage::Pointer image;
        int time_steps{0};
        int cur_step{0};
        QMap<QString, TCMask::Pointer> mask;
        QList<double> tps;

        bool maskValid{ false };
        bool measureValid{ false };
        bool procChanged{ false };

        QVector<int> time_points;

        Algorithm::Pointer processing_algorithm;//contains 1 or more algorithms
        IParameter::Pointer parameter;
        TCMeasure::Pointer measure;

        QMutex mutex;
    };
    WorkingSet::WorkingSet():d{new Impl} {
        
    }
    WorkingSet::~WorkingSet() {
        
    }
    auto WorkingSet::SetImageTimePoints(QList<double> tps)->void {
        QMutexLocker lock(&d->mutex);
        d->tps = tps;
    }
    auto WorkingSet::GetImageTimePoints()->QList<double> {
        QMutexLocker lock(&d->mutex);
        return d->tps;
    }
    auto WorkingSet::SetProcChanged(bool changed) -> void {
        QMutexLocker lock(&d->mutex);
        d->procChanged = changed;
    }
    auto WorkingSet::GetProcChanged() -> bool {
        QMutexLocker lock(&d->mutex);
        return d->procChanged;
    }
    auto WorkingSet::GetTimeSteps() -> QVector<int> {        
        QMutexLocker lock(&d->mutex);
        return d->time_points ;
    }
    auto WorkingSet::SetTimeSteps(QVector<int> steps) -> void {        
        QMutexLocker lock(&d->mutex);
        d->time_points = steps;
    }
    auto WorkingSet::ClearMask() -> void {        
        QMutexLocker lock(&d->mutex);
        d->mask.clear();        
    }
    auto WorkingSet::ClearMeasure() -> void {        
        QMutexLocker lock(&d->mutex);
        d->measure =nullptr;
    }
    auto WorkingSet::GetInstance() -> Pointer {        
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }

    auto WorkingSet::GetMaskValid() -> bool {
        QMutexLocker lock(&d->mutex);
        return d->maskValid;
    }

    auto WorkingSet::SetMaskValid(bool valid) -> void {
        QMutexLocker lock(&d->mutex);
        d->maskValid = valid;
    }

    auto WorkingSet::GetMeausreValid() -> bool {
        QMutexLocker lock(&d->mutex);
        return d->measureValid;
    }

    auto WorkingSet::SetMeasureValid(bool valid) -> void {
        QMutexLocker lock(&d->mutex);
        d->measureValid = valid;
    }

    auto WorkingSet::SetMaskPath(const QString& path) -> void {        
        QMutexLocker lock(&d->mutex);
        d->maskDir = path;
    }

    auto WorkingSet::ClearMaskPath() -> void {        
        QMutexLocker lock(&d->mutex);
        d->maskDir = QString();
    }

    auto WorkingSet::GetMeasurePath() const -> QString {
        QMutexLocker lock(&d->mutex);
        auto measure_name = d->workingDir.chopped(5);
        measure_name += "/";
        measure_name += d->hypercubeName;
        measure_name += "/";
        measure_name += d->imageName;
        measure_name += ".rep";

        return measure_name;        
    }

    auto WorkingSet::GetMaskPath() const -> QString{
        QMutexLocker lock(&d->mutex);
        auto mask_header = d->workingDir.chopped(5);
        mask_header += "/";
        mask_header += d->hypercubeName;
        mask_header += "/";
        mask_header += d->imageName;
        mask_header += ".msk";

        d->maskDir = mask_header;

        return d->maskDir;
    }

    auto WorkingSet::GetImagePath() const -> QString {
        QMutexLocker lock(&d->mutex);
        return d->imageDir;
    }
    auto WorkingSet::SetImagePath(const QString& path) -> void {
        QMutexLocker lock(&d->mutex);
        d->imageDir = path;
    }
    auto WorkingSet::GetCurrentImageName() const -> QString {
        QMutexLocker lock(&d->mutex);
        return d->imageName;
    }

    auto WorkingSet::SetCurrentImageName(const QString& name) -> void {
        QMutexLocker lock(&d->mutex);
        d->imageName = name;
    }
    auto WorkingSet::GetCurrentOutputFormat() const -> QString {
        QMutexLocker lock(&d->mutex);
        return d->outputFormat;
    }
    auto WorkingSet::SetCurrentOutputFormat(const QString& format) -> void {
        QMutexLocker lock(&d->mutex);
        d->outputFormat = format;
    }

    auto WorkingSet::GetWorkingDirectory() const -> QString {
        QMutexLocker lock(&d->mutex);
        return d->workingDir;
    }
    auto WorkingSet::SetWorkingDirectory(const QString& path) -> void {
        QMutexLocker lock(&d->mutex);
        d->workingDir = path;
    }
    auto WorkingSet::GetCurrentHyperCubeName() const -> QString {
        QMutexLocker lock(&d->mutex);
        return d->hypercubeName;
    }
    auto WorkingSet::SetCurrentHyperCubeName(const QString& name) -> void {
        QMutexLocker lock(&d->mutex);
        d->hypercubeName = name;
    }

    auto WorkingSet::SetImage(TCImage::Pointer image) -> void {
        QMutexLocker lock(&d->mutex);
        d->image = image;
    }
    auto WorkingSet::GetImageTimeStep() const -> int {
        QMutexLocker lock(&d->mutex);
        return d->cur_step;
    }
    auto WorkingSet::SetImageTimeStep(const int& ts) -> void {
        QMutexLocker lock(&d->mutex);
        d->cur_step = ts;
    }
    auto WorkingSet::GetImageTimeSteps() const -> int {
        QMutexLocker lock(&d->mutex);
        return d->time_steps;
    }

    auto WorkingSet::SetImageTimeSteps(const int& steps) -> void {
        QMutexLocker lock(&d->mutex);
        d->time_steps = steps;
    }

    auto WorkingSet::GetImage() -> TCImage::Pointer {
        QMutexLocker lock(&d->mutex);
        return d->image;
    }
    auto WorkingSet::SetProcessingAlgorithm(Algorithm::Pointer procAlgo) -> void {
        QMutexLocker lock(&d->mutex);
        d->processing_algorithm = procAlgo;
    }
    auto WorkingSet::GetProcessingAlgorithm() -> Algorithm::Pointer {
        QMutexLocker lock(&d->mutex);
        return d->processing_algorithm;
    }
    auto WorkingSet::SetParameter(IParameter::Pointer param) -> void {
        QMutexLocker lock(&d->mutex);
        d->parameter = param;
    }
    auto WorkingSet::GetParameter() -> IParameter::Pointer {
        QMutexLocker lock(&d->mutex);
        return d->parameter;
    }
    auto WorkingSet::SetMeasure(TCMeasure::Pointer measure) -> void {
        QMutexLocker lock(&d->mutex);
        d->measure = measure;
    }
    auto WorkingSet::GetMeasure() -> TCMeasure::Pointer {
        QMutexLocker lock(&d->mutex);
        return d->measure;
    }
    auto WorkingSet::AddMask(TCMask::Pointer mask, const QString& key) -> void {
        QMutexLocker lock(&d->mutex);
        d->mask[key] = mask;        
    }
    auto WorkingSet::RemoveMask(const QString& key) -> void {
        QMutexLocker lock(&d->mutex);
        d->mask[key] = nullptr;
        d->mask.remove(key);
    }
    
    auto WorkingSet::GetMask(const QString& key) -> TCMask::Pointer {
        QMutexLocker lock(&d->mutex);
        return d->mask[key];
    }
    auto WorkingSet::GetMasks() -> QMap<QString, TCMask::Pointer> {
        QMutexLocker lock(&d->mutex);
        return d->mask;
    }
    auto WorkingSet::SetTimeInterval(const double& timeInterval) -> void {
        QMutexLocker lock(&d->mutex);
        d->timeInterval = timeInterval;
    }
    auto WorkingSet::GetTimeInterval() -> double {
        QMutexLocker lock(&d->mutex);
        return d->timeInterval;
    }
}
