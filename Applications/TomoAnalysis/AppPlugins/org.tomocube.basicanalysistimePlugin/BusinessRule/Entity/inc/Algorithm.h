#pragma once

#include <memory>

#include "BasicAnalysisTimeEntityExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Entity {
	class BasicAnalysisTimeEntity_API Algorithm {
	public :
		typedef Algorithm Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		Algorithm();
		Algorithm(const Algorithm& other);
		virtual ~Algorithm();

		auto Init()->void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}