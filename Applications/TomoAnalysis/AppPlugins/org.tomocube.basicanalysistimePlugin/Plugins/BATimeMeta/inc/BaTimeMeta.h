#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "BasicAnalysisTimeMetaExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
	class BasicAnalysisTimeMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.basicanalysistimeMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "Basic Analyzer[T]"; }
		auto GetFullName()const->QString override { return "org.tomocube.basicanalysistimePlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}