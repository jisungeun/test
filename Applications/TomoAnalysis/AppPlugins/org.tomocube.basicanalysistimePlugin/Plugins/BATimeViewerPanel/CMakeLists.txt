project(BasicAnalysisTimeViewerPanel)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/bin/Release)
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/bin/Debug)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug)

#Header files for external use
set(INTERFACE_HEADERS
	inc/BaTimeViewerPanel.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/BaTimeViewerPanel.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TomoAnalysis::BasicAnalysisTime::Plugins::ViewerPanel ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>        
        $<INSTALL_INTERFACE:inc>        
    PRIVATE                
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        ${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
)

target_link_directories(${PROJECT_NAME}
	PRIVATE
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC        		    
        ${VIEWER_COMPONENT_LINK_LIBRARY}		
        Qt5::Core
        Qt5::Widgets
		Qt5::Gui
        TomoAnalysis::BasicAnalysisTime::Interactor
    PRIVATE        
        TC::Components::MaskContainer
        TC::Components::SliceContainer 
		TC::Components::VolumeContainer        
        TC::Components::MaskIO
        TC::Rendering::Annotation::InformativeUI
        TC::Components::SceneManager
        TC::Rendering::Tools::RenderCapturer
        TomoAnalysis::BasicAnalysisTime::Plugins::RenderWindow2D
        TomoAnalysis::BasicAnalysisTime::Plugins::RenderWindow3D        
        TomoAnalysis::BasicAnalysisTime::Plugins::IO
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/TomoAnalysis/AppPlugins/BasicAnalysisTime/Plugins")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT application_ta_ie)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)