#pragma once

#include <memory> 

#include <QWidget>
#include <QLayout>
#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoNode.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <ScreenShotWidget.h>
#include <IRenderWindowPanel.h>

#include "BasicAnalysisTimeViewerPanelExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BasicAnalysisTimeViewerPanel_API ViewerPanel : public QWidget, public Interactor::IRenderWindowPanel {
        Q_OBJECT
    public:
        typedef ViewerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ViewerPanel(QWidget* parent = nullptr);
        ~ViewerPanel();

        auto Update(IBaseImage::Pointer image,IBaseMask::Pointer mask,const QString& organ_name) -> bool override;
        auto UpdateMasks(const QStringList& organ_names, IBaseMask::Pointer instMask, IBaseMask::Pointer organMask) -> bool override;
        auto UpdateVisibility(int blob_index, bool visibility) -> bool override;

        auto Update(SoVolumeData* mask)->bool;
        auto ClearMask(bool clearMask = true)->void;
        auto SetCellFilter(bool cellwise)->void;
        auto SetMaskOpacity(double opac)->void;
        auto ConnectScreenShowWidget(TC::ScreenShotWidget::Pointer widget)->void;

    signals:
        void sliceIndexChanged(int, int);
    protected slots:
        void onMoveSlice(float value);

    private:                
        auto ImageToOivVolume(IBaseImage::Pointer image,int time_idx)const->SoVolumeData*;        

        auto Switch2DRenderWindow()const->void;

        auto Init()->void;
        auto InitDataGroups()->void;
        auto Init3dScene()->void;
        auto Init2dScene()->void;
        auto InitConnections()->void;
        auto UpdateSingle()->void;
        auto UpdateMulti()->void;
        auto UpdateImage()->void;
        auto MakeCellWiseOrgan()->bool;

        auto Reset()->void;
        auto ResetMask()->void;

        auto GetRealMaskName(QString)->QString;
        auto CreateLabelTextMap()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}