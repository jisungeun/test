#include "ui_BaTimeBatchListWidget.h"
#include "BaTimeBatchListWidget.h"

#include <iostream>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QTableWidget>

#include "TCFMetaReader.h"

namespace  TomoAnalysis::BasicAnalysisTime::Plugins {
    struct BatchListWidget::Impl {
        Ui::BaTimeBatchListWidget* ui{ nullptr };

        TC::IO::TCFMetaReader* metaReader = nullptr;

        QString minPath;
        int minCount{-1};
        QString cube_name;

        uint64_t sizeBound{ INT_MAX };
    };

    BatchListWidget::BatchListWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {

        d->ui = new Ui::BaTimeBatchListWidget;
        d->ui->setupUi(this);

        this->InitUI();

        d->metaReader = new TC::IO::TCFMetaReader;
    }

    BatchListWidget::~BatchListWidget() {

    }
    auto BatchListWidget::SetSizeBound(const uint64_t& bound) -> void {
        d->sizeBound = bound;
    }

    void BatchListWidget::SetCube(const Cube::Pointer& cube) {
        if (nullptr == cube) {
            return;
        }
        d->cube_name = cube->GetName();
        auto CreateItem = [=](const QString& str) {
            auto item = new QTableWidgetItem(str);
            item->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            return item;
        };

        d->ui->titleCheckBox->setText(cube->GetName());

        for(const auto& tcfDir : cube->GetTCFDirList()) {
            if(nullptr == tcfDir) {
                continue;
            }

            auto meta = d->metaReader->Read(tcfDir->GetPath());
            if(nullptr == meta) {
                continue;
            }

            if(1 >= meta->data.data2DMIP.dataCount) {
                continue;
            }

            if(false == meta->data.data3D.exist) {
                continue;
            }

            uint64_t arrsize = static_cast<uint64_t>(meta->data.data3D.sizeX) * static_cast<uint64_t>(meta->data.data3D.sizeY) * static_cast<uint64_t>(meta->data.data3D.sizeZ);

            if(arrsize > d->sizeBound) {
                continue;
            }

            const auto row = d->ui->tableWidget->rowCount();
            d->ui->tableWidget->insertRow(row);

            d->ui->tableWidget->setItem(row, 0, CreateItem(tcfDir->GetPath()));
            d->ui->tableWidget->setCellWidget(row, 1, new QCheckBox());
            d->ui->tableWidget->setItem(row, 2, CreateItem(tcfDir->GetName()));
            d->ui->tableWidget->setItem(row, 3, CreateItem(QString::number(meta->data.data2DMIP.dataCount)));
            d->ui->tableWidget->setItem(row, 4, CreateItem(QString::number(0)));
            d->ui->tableWidget->setItem(row, 5, CreateItem(""));
        }
    }
    auto BatchListWidget::ParseTimePoints(QString timeSet) -> QList<int> {
        auto split = timeSet.split(",");
        QList<int> result;
        for(auto s : split) {
            result.push_back(s.toInt()-1);
        }
        return result;
    }
    auto BatchListWidget::GetTimePoints(const QString& filePath) -> QList<int> {
        QList<int> result = QList<int>();
        for(auto i=0;i<d->ui->tableWidget->rowCount();i++) {
            auto entry = d->ui->tableWidget->item(i,0)->text();            
            if(entry.compare(filePath)==0) {
                auto pointStr = d->ui->tableWidget->item(i,5)->text();                
                result = ParseTimePoints(pointStr);
                break;
            }
        }
        return result;
    }
    auto BatchListWidget::GetValid() -> bool {
        auto rowCnt = d->ui->tableWidget->rowCount();
        if(rowCnt < 1) {
            return false;
        }
        for(auto i=0;i<rowCnt;i++) {
            auto item = d->ui->tableWidget->item(i,5)->text();            
            if(item.isEmpty()) {
                return false;
            }
        }
        return true;
    }
    void BatchListWidget::SetTimePoints(const QList<int>& points, bool all,bool single) {
        for(auto row  = 0; row < d->ui->tableWidget->rowCount(); ++row) {
            if(false == all) {
                auto checkBox = static_cast<QCheckBox*>(d->ui->tableWidget->cellWidget(row, 1));
                if(Qt::Unchecked == checkBox->checkState() && false == single) {
                    continue;
                }
                if(single){
                    if(d->ui->tableWidget->currentRow()!=row) {
                        continue;
                    }
                }
            }

            auto dataCount = d->ui->tableWidget->item(row, 3)->text();

            QStringList pointList;
            for (auto point : points) {
                if(dataCount.toInt() >= point) {
                    pointList << QString::number(point);
                }
            }

            d->ui->tableWidget->item(row, 4)->setText(QString::number(pointList.size()));
            d->ui->tableWidget->item(row, 5)->setText(pointList.join(", "));
        }
    }
    void BatchListWidget::DeselectAll() {
        d->ui->tableWidget->clearSelection();        
    }
    void BatchListWidget::SetSelectedAll(bool selected) {
        auto state = Qt::Unchecked;
        if(true == selected) {
            state = Qt::Checked;
        }

        for (auto i = 0; i < d->ui->tableWidget->rowCount(); ++i) {
            d->ui->titleCheckBox->setCheckState(state);
            //auto checkBox = static_cast<QCheckBox*>(d->ui->tableWidget->cellWidget(i, 1));
            //checkBox->setCheckState(state);
        }
        UpdateMinPath(true);
    }

    void BatchListWidget::on_titleCheckBox_stateChanged(int state) {
        for (auto i = 0; i < d->ui->tableWidget->rowCount(); ++i) {
            auto checkBox = static_cast<QCheckBox*>(d->ui->tableWidget->cellWidget(i, 1));
            checkBox->setCheckState(static_cast<Qt::CheckState>(state));
        }
        UpdateMinPath();
    }
    auto BatchListWidget::GetCubeName() -> QString {
        return d->cube_name;
    }

    void BatchListWidget::on_tableWidget_cellClicked(int row, int column) {
        Q_UNUSED(column)
        auto checkBox = static_cast<QCheckBox*>(d->ui->tableWidget->cellWidget(row, 1));
        auto state = checkBox->checkState();
        if(state == Qt::Unchecked) {
            checkBox->setCheckState(Qt::Checked);
        }
        else{
            checkBox->setCheckState(Qt::Unchecked);
            d->ui->titleCheckBox->blockSignals(true);
            d->ui->titleCheckBox->setCheckState(Qt::Unchecked);
            d->ui->titleCheckBox->blockSignals(false);
        }        
        UpdateMinPath();
        emit OnSelection(d->cube_name);
    }

    // private
    void BatchListWidget::UpdateMinPath(bool fromOutside) {
        d->minCount = -1;
        d->minPath = QString();
        for(auto i=0;i<d->ui->tableWidget->rowCount();++i) {
            auto checkBox = static_cast<QCheckBox*>(d->ui->tableWidget->cellWidget(i, 1));
            auto path = d->ui->tableWidget->item(i,0)->text();
            if(checkBox->isChecked()) {
                auto cnt = d->metaReader->Read(path)->data.data3D.dataCount;
                if(d->minCount<0 || d->minCount > cnt) {
                    d->minCount = cnt;
                    d->minPath = path;
                }
            }             
        }
        if(!fromOutside){
            emit OnMinRangeChanged(d->minPath);
        }
    }

    auto BatchListWidget::GetMinCount() -> int {
        return d->minCount;
    }

    auto BatchListWidget::GetMinPath() -> QString {
        return d->minPath;
    }


    void BatchListWidget::InitUI() {
        d->ui->titleCheckBox->setIcon(QIcon(QPixmap(":/image/images/CubeFill.png")));
        d->ui->titleCheckBox->setIconSize(QSize(16, 16));

        const QStringList headers{ "Path", "", "File name", "HT", "Counts", "Selected time points" };
        d->ui->tableWidget->setColumnCount(headers.size());
        d->ui->tableWidget->setHorizontalHeaderLabels(headers);
        d->ui->tableWidget->verticalHeader()->setVisible(false);
        d->ui->tableWidget->hideColumn(0);

        d->ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        d->ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
        d->ui->tableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        d->ui->tableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
        d->ui->tableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    }
}
