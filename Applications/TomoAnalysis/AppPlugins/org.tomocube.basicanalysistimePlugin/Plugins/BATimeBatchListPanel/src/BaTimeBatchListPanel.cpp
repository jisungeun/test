#include "ui_BaTimeBatchListPanel.h"
#include "BaTimeBatchListPanel.h"

#include <iostream>
#include <QResizeEvent>

#include "BaTimeBatchListWidget.h"

namespace  TomoAnalysis::BasicAnalysisTime::Plugins {
    struct BatchListPanel::Impl {
        Ui::BaTimeBatchListPanel* ui{ nullptr };

        QList<BatchListWidget*> listWidgets;
        QString cur_cube_info;
        QString procName;
        uint64_t arrBound{ INT_MAX };
    };

    BatchListPanel::BatchListPanel(QWidget* parent)
    : d{ new Impl } ,QWidget(parent) {        
        d->ui = new Ui::BaTimeBatchListPanel;
        d->ui->setupUi(this);
    }

    BatchListPanel::~BatchListPanel() {
        delete d->ui;
    }

    auto BatchListPanel::Update(Entity::WorkingSet::Pointer workingset)->bool {
        return true;
    }
    auto BatchListPanel::SetCurrentProcessor(QString name) -> void {
        d->procName = name;        
        if(name.contains(".ai")) {
            d->arrBound = INT_MAX / 4;
        }else{
            d->arrBound = INT_MAX / 3 * 2;
        }
    }
    void BatchListPanel::Init(const HyperCube::Pointer& hypercube) {
        if (nullptr == hypercube) {
            return;
        }

        d->listWidgets.clear();

        auto mainWidget = new QWidget;
        mainWidget->setContentsMargins(0, 0, 0, 0);

        auto layout = new QVBoxLayout;
        layout->setMargin(0);

        for (const auto& cube : hypercube->GetCubeList()) {
            auto widget = new Plugins::BatchListWidget(this);
            widget->SetSizeBound(d->arrBound);
            widget->SetCube(cube);
            layout->addWidget(widget);

            d->listWidgets.append(widget);
            connect(widget,SIGNAL(OnMinRangeChanged(QString)),this,SLOT(OnMinPathFromWidget(QString)));
            connect(widget,SIGNAL(OnSelection(QString)),this,SLOT(OnCurrentCube(QString)));
        }

        layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

        mainWidget->setLayout(layout);

        d->ui->scrollArea->setWidget(mainWidget);
        d->ui->scrollArea->setContentsMargins(0, 0, 0, 0);

        auto resizeEvent = std::make_shared<QResizeEvent>(this->size(), this->size());

        this->resizeEvent(resizeEvent.get());
    }
    void BatchListPanel::OnCurrentCube(QString info) {
        d->cur_cube_info = info;
        //deselect other cube
        for(auto widget: d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            if(d->cur_cube_info.compare(widget->GetCubeName())==0) {
                continue;
            }
            widget->DeselectAll();
        }
    }
    auto BatchListPanel::GetValidity()->bool {                
        auto count = 0;
        for(auto widget:d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            auto widgetValid = widget->GetValid();            
            if(false == widgetValid) {
                return false;//if there exist one non valid widget return false
            }
            count++;
        }
        if (count > 0) {
            return true;//every existing widget is valid
        }
        return false;//when there is no widget to process
    }
    auto BatchListPanel::GetTimeSteps(QString file_name) -> QList<int> {
        QList<int> result = QList<int>();
        for(auto widget: d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            result = widget->GetTimePoints(file_name);
            if(result.size()>0) {
                break;
            }

        }
        return result;
    }
    void BatchListPanel::SetTimePointsAll(const QList<int>& points) {
        for(auto widget : d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            widget->SetTimePoints(points,true);
        }
    }

    void BatchListPanel::SetTimePoints(const QList<int>& points) {
        //for selected widget only
        for(auto widget : d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            if(d->cur_cube_info.compare(widget->GetCubeName())!=0) {
                continue;
            }
            widget->SetTimePoints(points,false,true);
        }
    }
    void BatchListPanel::SetTimePointsCheck(const QList<int>& points) {
        for(auto widget : d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            widget->SetTimePoints(points);
        }
    }

    void BatchListPanel::on_allCheckBox_stateChanged(int state) {
        auto selected = true;
        if (Qt::Unchecked == state) {
            selected = false;
        }

        for(auto widget : d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }

            widget->SetSelectedAll(selected);
        }
        FindMinTimeCount();
    }

    void BatchListPanel::OnMinPathFromWidget(QString path) {
        FindMinTimeCount();
    }

    void BatchListPanel::FindMinTimeCount() {
        auto temp_min = -1;
        QString temp_path;
        for(auto widget : d->listWidgets) {
            if(nullptr == widget) {
                continue;
            }
            auto count = widget->GetMinCount();
            if(count<0)
                continue;
            if(temp_min == -1 || temp_min > count) {
                temp_min = count;
                temp_path = widget->GetMinPath();
            }
        }
        if(false == temp_path.isEmpty()) {            
            emit minPathChanged(temp_path);
        }
    }

    void BatchListPanel::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
        const auto height = d->ui->scrollArea->height() / 2; 

        for (auto widget : d->listWidgets) {
            if (nullptr == widget) {
                continue;
            }

            widget->setMinimumHeight(height);
        }
    }
}
