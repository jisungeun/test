#pragma once

#include <memory>
#include <QWidget>

#include <Cube.h>

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BatchListWidget : public QWidget {
        Q_OBJECT
    public:
        typedef BatchListWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        BatchListWidget(QWidget* parent=nullptr);
        ~BatchListWidget();

        void SetCube(const Cube::Pointer& cube);

        void SetTimePoints(const QList<int>& points, bool all = false,bool single = false);

        void SetSelectedAll(bool selected);
        void DeselectAll();

        auto GetMinCount()->int;
        auto GetMinPath()->QString;
        auto GetValid()->bool;
        auto GetTimePoints(const QString& filePath)->QList<int>;
        auto GetCubeName()->QString;
        auto SetSizeBound(const uint64_t& bound)->void;

    signals:
        void OnMinRangeChanged(QString);
        void OnSelection(QString);

    protected slots:
        void on_titleCheckBox_stateChanged(int state);
        void on_tableWidget_cellClicked(int row, int column);

    private:
        void InitUI();
        void UpdateMinPath(bool fromOutSide = false);
        auto ParseTimePoints(QString timeSet)->QList<int>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}