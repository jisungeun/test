#pragma once

#include <memory>
#include <QWidget>

#include "IBatchListPanel.h"
#include "BasicAnalysisTimeBatchListPanelExport.h"

#include <HyperCube.h>

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BasicAnalysisTimeBatchListPanel_API BatchListPanel : public QWidget, public Interactor::IBatchListPanel {
        Q_OBJECT
    public:
        typedef BatchListPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        BatchListPanel(QWidget* parent=nullptr);
        ~BatchListPanel();

        auto Update(Entity::WorkingSet::Pointer workingset)->bool override;

        void Init(const HyperCube::Pointer& hypercube);
        auto SetCurrentProcessor(QString name)->void;
        void SetTimePoints(const QList<int>& points);
        void SetTimePointsAll(const QList<int>& points);
        void SetTimePointsCheck(const QList<int>& points);
        auto GetValidity()->bool;
        auto GetTimeSteps(QString file_name)->QList<int>;

    signals:
        void minPathChanged(QString);

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void on_allCheckBox_stateChanged(int state);
        void OnMinPathFromWidget(QString path);
        void OnCurrentCube(QString info);

    private:
        void FindMinTimeCount();        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}