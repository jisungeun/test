#pragma once

#include <memory>
#include <QWidget>

#include "BasicAnalysisTimeParameterPanelExport.h"

#include "IParameterPanel.h"
#include "VolumeViz/nodes/SoVolumeData.h"

#include "DataList.h"
#include "IParameter.h"
#include "MaskRaw.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BasicAnalysisTimeParameterPanel_API ParameterPanel : public QWidget, public Interactor::IParameterPanel {
        Q_OBJECT
    public:
        typedef ParameterPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ParameterPanel(QWidget* parent = nullptr);
        ~ParameterPanel();
        auto Init(void)->bool;        
        auto Update(Entity::WorkingSet::Pointer workingset) -> bool override;
        auto GetParameter()->IParameter::Pointer;
        auto GetParameter(QString algo_name)->IParameter::Pointer;

        auto SetProcessor(const QString& proc_name)->void;
        auto SetParameter(IParameter::Pointer param)->void;
        auto GetTimePoints()->QList<int>;
        auto GetConnection()->QMap<QString, std::tuple<QString, QStringList>>&;

        //Non-clean architecture
        auto SetResultToggle(bool)->void;
        auto SetNextHighlight(QString algo_name)->void;
        auto SetUIForBatch(bool isBatch)->void;        
        auto SetNoHighlight()->void;
        auto SetDefaultHighlight()->void;
        auto SetMeasureHighlight()->void;
        auto SetApplyHighlight()->void;
        auto SetTimeHighlight()->void;
        auto LockProcessor(bool lock)->void;
        auto SetCurrentProcessor(QString procName)->bool;        
        auto GetCurrentMode()->bool;
        auto GetCurrentProcessor()->QString;
        auto SetDefaultProcessors(QStringList procPaths)->void;
        auto SetDefaultProcessors(QStringList procPaths, QStringList invalidKey)->void;
        auto GetResultToggle()->bool;

    signals:
        void sigProc(QString);
        void sigAlgoValue(double, QString);
        void sigWholeExe();
        void sigAlgoExe(QString,QString);
        void sigMaskSelected(QString);
        void sigRefHigh();
        void sigMaskCorrection();

        void sigToggleResult(bool);
        void sigApplyParam(QString);

        void sigChangedTimePoints(QList<int> points);
        void sigCheckedTimePoints(QList<int> points);
        void sigAllTimePoints(QList<int> points);

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void OnCurrentProcChanged(int);
        void OnProcessorValueChanged(double, QString);
        void OnAlgorithmExecuted(QString,QString);
        void OnWholeExcuted();                
        void OnHighlightApply(bool hi);
        void OnRefreshHighlight();

        void OnApplyParameter(void);
        void OnMaskCorrection(void);
        void OnToggleResult(void);
        void OnCustomTime(void);
        void OnTimePropChanged(int);
        void OnApplyTime(void);
        void OnApplyTimeCheck(void);
        void OnApplyTimeAll(void);

    private:        
        auto DefaultSetting()->void;
        auto ParseTimePoints()->QList<int>;
        auto GenerateTimePoints()->void;
        
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}