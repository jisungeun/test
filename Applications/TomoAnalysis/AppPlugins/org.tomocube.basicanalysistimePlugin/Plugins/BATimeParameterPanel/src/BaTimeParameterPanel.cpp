#include "BaTimeParameterPanel.h"

#include <QDirIterator>
#include <QProgressDialog>
#include <QElapsedTimer>
#include <QMessageBox>
#include <QApplication>

#include "ui_BaTimeParameterPanel.h"

#include <IProcessor.h>
#include <IMetaParameter.h>
#include <IParameter.h>
#include "ISegmentationAlgorithm.h"
#include "IProcessingAlgorithm.h"
#include "ICustomAlgorithm.h"
#include "PluginRegistry.h"
#include "ImageRaw.h"
//#include "MaskRaw.h"
//#include "ResultTable.h"

#include <QLayout>

#include "VolumeViz/nodes/SoVolumeData.h"
#include <ParamControl.h>
#include <ProcControl.h>
#include "ParameterRegistry.h"


namespace TomoAnalysis::BasicAnalysisTime::Plugins {
	struct ParameterPanel::Impl {
		Ui::BaTimeParameterPanel* ui{ nullptr };			
		TC::ProcControl* procControl{nullptr};
		QList<QString> procPath;
		QList<QString> fullName;
		

		bool showResult{ false };
		QList<int> timePoints;

		QString cur_imagePath;

		bool isManual{ false };
		bool isBatch{ false };				
	};
	ParameterPanel::ParameterPanel(QWidget* parent) :QWidget(parent),
		d{ new Impl }{
		d->ui = new Ui::BaTimeParameterPanel;
		d->ui->setupUi(this);
		Init();
	}
	ParameterPanel::~ParameterPanel() {
		delete d->ui;
	}	
	auto ParameterPanel::LockProcessor(bool lock) -> void {
		d->ui->procCombo->setEnabled(!lock);
	}	
	auto ParameterPanel::SetCurrentProcessor(QString procName) -> bool {
		for (auto i = 0; i < d->ui->procCombo->count(); i++) {
			if (d->fullName[i].compare(procName) == 0) {
				d->ui->procCombo->setCurrentIndex(i);
				return true;
			}
		}
		return false;
	}
	auto ParameterPanel::GetCurrentMode() -> bool {
		return d->isManual;
    }

	bool ParameterPanel::eventFilter(QObject* watched, QEvent* event) {
		if (event->type() == QEvent::Resize) {
			int height = 0;
			if (d->isManual) {
				height = this->height() - 82 - d->ui->noticeGroup->height() - d->ui->procCombo->height() - d->ui->resultBtn->height() - d->ui->paramBtn->height();
			}
			else {
				height = this->height() - 82 - d->ui->timeGroup->height() - d->ui->procCombo->height() - d->ui->resultBtn->height() - d->ui->paramBtn->height();
			}

			if (height > 0) {
				d->procControl->SetCompetableMinSize(height);
			}
		}
		return QObject::eventFilter(watched, event);
    }


	auto ParameterPanel::Init()-> bool {
		installEventFilter(this);
		connect(d->ui->procCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnCurrentProcChanged(int)));
				
		d->procControl = new TC::ProcControl(nullptr);		
		auto layout = new QVBoxLayout;
		layout->setSpacing(0);
		layout->setContentsMargins(0, 0, 0, 0);
		d->ui->paramSocket->setLayout(layout);
		layout->addWidget(d->procControl);
		//d->ui->procLayout->addWidget(d->procControl);		

		connect(d->procControl, SIGNAL(valueCall(double, QString)), this, SLOT(OnProcessorValueChanged(double, QString)));
		connect(d->procControl, SIGNAL(executeWhole()), this, SLOT(OnWholeExcuted()));
		connect(d->procControl, SIGNAL(executeCall(QString,QString)), this, SLOT(OnAlgorithmExecuted(QString,QString)));
		connect(d->procControl, SIGNAL(highlightApply(bool)), this, SLOT(OnHighlightApply(bool)));
		connect(d->procControl, SIGNAL(refreshHighlight()), this, SLOT(OnRefreshHighlight()));

		d->ui->verticalLayout->setAlignment(Qt::AlignTop);
		d->ui->procLayout->setAlignment(Qt::AlignTop);
		d->ui->groupBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

		connect(d->ui->resultBtn, SIGNAL(clicked()), this, SLOT(OnToggleResult()));
		connect(d->ui->correctionBtn, SIGNAL(clicked()), this, SLOT(OnMaskCorrection()));
		connect(d->ui->paramBtn, SIGNAL(clicked()), this, SLOT(OnApplyParameter()));

		//d->ui->correctionBtn->setEnabled(false);

		d->procControl->hide();		

		d->ui->noticeGroup->hide();		

		DefaultSetting();

		//init time step UI with proper validators						
		QRegExp re("^(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*))(, *(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*)))*$");
		d->ui->timePoints->setValidator(new QRegExpValidator(re));
		d->ui->timePoints->setReadOnly(true);

		connect(d->ui->customTime, SIGNAL(clicked()), this, SLOT(OnCustomTime()));
		connect(d->ui->startTime, SIGNAL(valueChanged(int)), this, SLOT(OnTimePropChanged(int)));
		connect(d->ui->endTime, SIGNAL(valueChanged(int)), this, SLOT(OnTimePropChanged(int)));
		connect(d->ui->timeInterval, SIGNAL(valueChanged(int)), this, SLOT(OnTimePropChanged(int)));
		connect(d->ui->applyBtn, SIGNAL(clicked()), this, SLOT(OnApplyTime()));
		connect(d->ui->applyChkBtn,SIGNAL(clicked()),this,SLOT(OnApplyTimeCheck()));
		connect(d->ui->applyAllBtn, SIGNAL(clicked()), this, SLOT(OnApplyTimeAll()));

		d->ui->startTime->setMinimum(1);
		d->ui->endTime->setMinimum(1);
		d->ui->timeInterval->setMinimum(1);

		//d->procControl->setDisableControl(true);
		d->ui->verticalLayout->setAlignment(Qt::AlignTop);
		//TODO
		d->ui->warngroup->hide();

		return true;
	}
	void ParameterPanel::OnRefreshHighlight() {
		emit sigRefHigh();
	}
	void ParameterPanel::OnHighlightApply(bool hi) {
		if (hi) {
			d->ui->paramBtn->setStyleSheet("background-color: rgb(91,139,151);");
		}
		else {
			d->ui->paramBtn->setStyleSheet("");
		}

	}
	auto ParameterPanel::SetUIForBatch(bool isBatch) -> void {
		d->isBatch = isBatch;
		if (isBatch) {
			d->ui->resultBtn->hide();
			d->ui->paramBtn->hide();
			d->ui->correctionBtn->hide();
			d->ui->applyAllBtn->show();
			d->ui->applyChkBtn->show();
			
			//d->procControl->setDisableControl(true);
			d->procControl->SetDisableControl(true);
			//d->procControl->setHidePramExecute(true);
			d->procControl->SetHideParamExecute(true);
			//d->procControl->setHideExecute(false);
			d->procControl->SetHideExecute(false);

		}
		else {
			d->ui->resultBtn->show();
			d->ui->paramBtn->show();
			d->ui->correctionBtn->show();
			d->ui->applyAllBtn->hide();
			d->ui->applyChkBtn->hide();

			//d->procControl->setDisableControl(false);
			d->procControl->SetDisableControl(false);
			//d->procControl->setHidePramExecute(false);
			d->procControl->SetHideParamExecute(false);
			//d->procControl->setHideExecute(true);
			d->procControl->SetHideExecute(true);
		}
		DefaultSetting();
	}


	auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
		ParameterNode node;
		node.name = name;
		node.displayName = param->DisplayName(name);
		node.description = param->Description(name);
		node.unit = param->Unit(name);
		node.value = param->GetValue(name);
		node.defaultValue = param->GetDefault(name);
		auto range = param->GetRange(name);
		node.minValue = std::get<0>(range);
		node.maxValue = std::get<1>(range);

		return node;
	};

	void ParameterPanel::OnTimePropChanged(int changed) {
		//무조건 다시 generate
		GenerateTimePoints();
	}

	void ParameterPanel::OnApplyTime() {
		d->timePoints = ParseTimePoints();

		if (d->timePoints.count() > 0) {
			emit sigChangedTimePoints(d->timePoints);
		}
	}
	void ParameterPanel::OnApplyTimeCheck() {
        d->timePoints = ParseTimePoints();

		if (d->timePoints.count() > 0) {
			emit sigCheckedTimePoints(d->timePoints);
		}
    }
	void ParameterPanel::OnApplyTimeAll() {
		d->timePoints = ParseTimePoints();

		if (d->timePoints.count() > 0) {
			emit sigAllTimePoints(d->timePoints);
		}
	}

	auto ParameterPanel::GetTimePoints() -> QList<int> {
		return d->timePoints;
	}
	void ParameterPanel::OnCustomTime() {
		auto isCustom = d->ui->customTime->isChecked();
		d->ui->timePoints->setReadOnly(!isCustom);
		d->ui->startTime->setEnabled(!isCustom);
		d->ui->endTime->setEnabled(!isCustom);
		d->ui->timeInterval->setEnabled(!isCustom);
	}
	auto ParameterPanel::SetProcessor(const QString& proc_name) -> void {		
		if(d->fullName.contains(proc_name)) {
			d->ui->procCombo->setCurrentIndex(d->fullName.indexOf(proc_name));
		}		
    }
	auto ParameterPanel::SetParameter(IParameter::Pointer param) -> void {		
		d->procControl->SetParameterValue(param);
	}
	auto ParameterPanel::SetTimeHighlight() -> void {
		d->ui->applyBtn->setStyleSheet("background-color: rgb(91,139,151);");
	}

	auto ParameterPanel::SetNoHighlight() -> void {
		d->ui->applyBtn->setStyleSheet("");
		d->ui->paramBtn->setStyleSheet("");
		//auto param = d->procControl->getParameter();
		auto param = d->procControl->GetParameter();
		if (nullptr != param) {
			for (auto name : param->GetNames()) {
				if (!name.contains("Mask Selector")) {
					auto fullname = param->GetValue(name).toString();
					if (fullname.contains("masking")) {						
						d->procControl->SetProcHighlight(false, fullname);
					}
					else if (fullname.contains("measurement")) {						
						d->procControl->SetProcHighlight(false, fullname);
					}
				}
			}
		}
	}

	auto ParameterPanel::SetDefaultHighlight() -> void {
		//d->procControl->setDefaultHighlight();
		d->procControl->SetDefaultHighlight();
	}

	auto ParameterPanel::SetMeasureHighlight() -> void {		
		//auto param = d->procControl->getParameter();
		auto param = d->procControl->GetParameter();
		if (nullptr != param) {
			for (auto name : param->GetNames()) {
				if (!name.contains("Mask Selector")) {
					auto fullname = param->GetValue(name).toString();
					if (fullname.contains("measurement")) {
						//d->procControl->setProcHighlight(true, fullname);
						d->procControl->SetProcHighlight(true, fullname);
					}
				}
			}
		}
	}

	auto ParameterPanel::SetApplyHighlight() -> void {
		d->ui->paramBtn->setStyleSheet("background-color: rgb(91,139,151);");
	}

	auto ParameterPanel::GenerateTimePoints() -> void {
		auto min = d->ui->startTime->value();
		auto max = d->ui->endTime->value();
		auto interval = d->ui->timeInterval->value();

		if (max == 0 || interval == 0) {
			return;
		}

		QString resultText = QString();
		for (auto i = min; i <= max; i += interval) {
			resultText += QString::number(i);
			if (i + interval <= max) {
				resultText += ",";
			}
		}
		auto prev = d->ui->timePoints->text();
		if (prev.compare(resultText) != 0) {
			d->timePoints.clear();
		}
		d->ui->timePoints->setText(resultText);


		//emit sigChangedTimePoints(resultText);

		//ParseTimePoints();
	}
	auto ParameterPanel::ParseTimePoints() -> QList<int> {

		if(d->ui->timePoints->text().isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Time step list is empty");
			return QList<int>();
		}

		auto split = d->ui->timePoints->text().split(",");
		auto prev = -1;
		QList<int> result;
		auto maximum = d->ui->endTime->maximum();
				
		for (const auto& timestr : split) {
			if (timestr.contains("-")) {//range
				auto begin = timestr.split("-")[0].toInt();
				auto end = timestr.split("-")[1].toInt();
				if (begin <= prev) {
					QMessageBox::warning(nullptr, "Error", "Wrong time_step order\n (put numbers in ascending order)");
					return QList<int>();
				}
				if (begin > end) {
					QMessageBox::warning(nullptr, "Error", "Wrong sub-time_step range\n (put numbers in ascending order)");
					return QList<int>();
				}
				if (end > maximum) {
					QMessageBox::warning(nullptr, "Error", QString("Out of range\n (put numbers less than image time_steps %1)").arg(maximum));
					return QList<int>();
				}
				for (auto i = begin; i <= end; i++) {					
					result.push_back(i);
				}
				prev = end;
			}
			else {
				auto timePoint = timestr.toInt();
				if (timePoint <= prev) {
					QMessageBox::warning(nullptr, "Error", "Wrong time_step order\n (put numbers in ascending order)");
					return QList<int>();
				}
				if (timePoint > maximum) {
					QMessageBox::warning(nullptr, "Error", QString("Out of range\n (put numbers less than image time_steps %1)").arg(maximum));
					return QList<int>();
				}

			    result.push_back(timePoint);
			    prev = timePoint;				
			}
		}

		return result;
	}
	auto ParameterPanel::Update(Entity::WorkingSet::Pointer workingset) -> bool {
		auto path = workingset->GetImagePath();
		if (path.compare(d->cur_imagePath) != 0) {//if image changed, update parameter interface
			//DefaultSetting();
			d->timePoints.clear();

			//update time interface due to update time step
			auto image_cnt = workingset->GetImageTimeSteps();
			d->ui->startTime->blockSignals(true);
			d->ui->startTime->setRange(1, image_cnt - 1);
			d->ui->startTime->setValue(1);
			d->ui->startTime->blockSignals(false);
			d->ui->endTime->blockSignals(true);
			d->ui->endTime->setRange(1, image_cnt);
			d->ui->endTime->setValue(image_cnt);
			d->ui->endTime->blockSignals(false);
			d->ui->timeInterval->blockSignals(true);
			d->ui->timeInterval->setRange(1, image_cnt - 1);
			d->ui->timeInterval->setValue(1);
			d->ui->timeInterval->blockSignals(false);
			GenerateTimePoints();
			d->ui->customTime->setChecked(false);
			OnCustomTime();
			d->cur_imagePath = path;
		}
		return true;
	}
	auto ParameterPanel::SetDefaultProcessors(QStringList procPaths, QStringList invalidKey) -> void {
		blockSignals(true);
		d->procControl->ClearInterface();
		d->ui->procCombo->blockSignals(true);
		d->ui->procCombo->clear();
		d->procPath.clear();
		d->fullName.clear();
		for (auto p : procPaths) {
			//parse processor path
			QDir procDir(qApp->applicationDirPath() + p);
			auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
			for (auto f : files) {
				auto fullPath = qApp->applicationDirPath() + p + "/" + f;
				auto plugin = PluginRegistry::GetPlugin(fullPath, true);
				auto isValid = true;
				for(auto ikey : invalidKey) {
				    if(plugin->GetFullName().contains(ikey)) {
						isValid = false;
						break;
				    }
				}
				if (isValid) {
					d->ui->procCombo->addItem(plugin->GetName());
					d->fullName.push_back(plugin->GetFullName());
					d->procPath.push_back(fullPath);
				}
			}
		}

		d->ui->procCombo->setCurrentIndex(0);
		this->OnCurrentProcChanged(0);

		d->ui->procCombo->blockSignals(false);
		blockSignals(false);
    }

	auto ParameterPanel::SetDefaultProcessors(QStringList procPaths) -> void {
		blockSignals(true);
		d->procControl->ClearInterface();
		d->ui->procCombo->blockSignals(true);
		d->ui->procCombo->clear();
		d->procPath.clear();
		d->fullName.clear();		
		for (auto p : procPaths) {
			//parse processor path
			QDir procDir(qApp->applicationDirPath() + p);
			auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
			for (auto f : files) {
				auto fullPath = qApp->applicationDirPath() + p + "/" + f;
				auto plugin = PluginRegistry::GetPlugin(fullPath, true);
				d->ui->procCombo->addItem(plugin->GetName());				
				d->fullName.push_back(plugin->GetFullName());				
				d->procPath.push_back(fullPath);				
			}
		}

		d->ui->procCombo->setCurrentIndex(0);
		this->OnCurrentProcChanged(0);

		d->ui->procCombo->blockSignals(false);
		blockSignals(false);
    }

	auto ParameterPanel::DefaultSetting() -> void {
		blockSignals(true);		
		d->procControl->ClearInterface();

	    if (d->ui->procCombo->count() > 0) {
			d->ui->procCombo->setCurrentIndex(0);
			d->procControl->show();
			this->OnCurrentProcChanged(0);
		}
		else {
			d->procControl->hide();
		}		
		blockSignals(false);
	}
	auto ParameterPanel::GetCurrentProcessor() -> QString {
		return d->ui->procCombo->currentText();
    }
	auto ParameterPanel::GetConnection() -> QMap<QString, std::tuple<QString, QStringList>>& {
		return d->procControl->GetConnections();
    }


    //slots

	void ParameterPanel::OnApplyParameter() {
		auto cur_idx = d->ui->procCombo->currentIndex();
		emit sigApplyParam(d->fullName[cur_idx]);
	}

	void ParameterPanel::OnMaskCorrection() {
		emit sigMaskCorrection();
	}

	void ParameterPanel::OnToggleResult() {
		d->showResult = !d->showResult;
		emit sigToggleResult(d->showResult);

		if (d->showResult) {
			d->ui->resultBtn->setText("Hide Result");
		}
		else {
			d->ui->resultBtn->setText("Show Result");
		}
	}

	void ParameterPanel::OnCurrentProcChanged(int idx) {
		//change processor
		auto proc_name = d->ui->procCombo->currentText();
		auto selectedProcessor = d->fullName[idx];		
		IPluginModule::Pointer processor = PluginRegistry::GetPlugin(selectedProcessor);
		if (nullptr == processor) {
			if (PluginRegistry::LoadPlugin(d->procPath[idx]) ==-1) {
				auto appWrite = d->procPath[idx].split("/processor/")[0];
				auto appRemain = d->procPath[idx].split("/processor/")[1];

				auto realPath = qApp->applicationDirPath() + "/processor/" + appRemain;
				if (PluginRegistry::LoadPlugin(realPath) == -1) {					
					return;
				}				
			}			
			processor = PluginRegistry::GetPlugin(selectedProcessor);
		}		
		auto param = processor->Parameter();
		//load algorithms if required
		for (auto nnt : param->GetNameAndTypes()) {
			auto name = std::get<0>(nnt);
			auto desp = param->Description(name);
			auto algoName = param->GetValue(name).toString();
			auto algoPath = qApp->applicationDirPath() + "/" + desp;

			auto algorithm = PluginRegistry::GetPlugin(algoName);
			if (nullptr == algorithm) {
				PluginRegistry::LoadPlugin(algoPath);
			}
		}

		auto procModule = std::dynamic_pointer_cast<IProcessor>(processor);
		auto metaParam = procModule->MetaParameter();

		d->isManual = false;
		for(auto name: param->GetNames()) {
			auto full_name = param->GetValue(name).toString();
			if(full_name.contains("Manual")) {
				d->isManual = true;
				break;
			}
		}

		if(d->isManual && !d->isBatch) {
			d->ui->noticeGroup->show();
			d->ui->timeGroup->hide();
		}else {
			d->ui->noticeGroup->hide();
			d->ui->timeGroup->show();
		}


		QStringList realname_list{ "membrane","nucleus","nucleoli","lipid droplet" };
		QStringList displayname_list{ "Whole cell","Nucleus","Nucleolus","Lipid droplet" };

		//d->procControl->setConverter(displayname_list, realname_list);
		d->procControl->SetConverter(displayname_list, realname_list);		
		d->procControl->SetProcessingParameter(param);
		d->procControl->SetMetaParameter(metaParam);
		d->procControl->BuildInterface();
		d->procControl->show();		

		emit sigProc(proc_name);
	}

	void ParameterPanel::OnProcessorValueChanged(double val, QString sender) {
		emit sigAlgoValue(val, sender);
	}

	void ParameterPanel::OnWholeExcuted() {
		emit sigWholeExe();
	}


	void ParameterPanel::OnAlgorithmExecuted(QString param_name,QString algo_name) {
		emit sigAlgoExe(param_name,algo_name);
	}
	auto ParameterPanel::SetNextHighlight(QString algo_name) -> void {
		d->procControl->SetNextHighlight(algo_name);
	}
	/*
	void ParameterPanel::OnCurrentMaskChanged(int idx) {
		auto text = d->ui->maskCombo->currentText();
		emit sigMaskSelected(text);
	}*/
	auto ParameterPanel::GetParameter() -> IParameter::Pointer {
		//return d->procControl->getParameter();
		return d->procControl->GetParameter();
	}
	auto ParameterPanel::GetParameter(QString algo_name) -> IParameter::Pointer {
		auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
			ParameterNode node;
			node.name = name;
			node.displayName = param->DisplayName(name);
			node.description = param->Description(name);
			node.unit = param->Unit(name);
			node.value = param->GetValue(name);
			node.defaultValue = param->GetDefault(name);
			auto range = param->GetRange(name);
			node.minValue = std::get<0>(range);
			node.maxValue = std::get<1>(range);

			return node;
		};

		//auto param = d->procControl->getParameter(algo_name);
		auto param = d->procControl->GetParameter(algo_name);

		return param;
	}
	
	auto ParameterPanel::SetResultToggle(bool show) -> void {
		d->showResult = show;
		if (show) {
			d->ui->resultBtn->setText("Hide Result");
		}
		else {
			d->ui->resultBtn->setText("Show Result");
		}
	}
	auto ParameterPanel::GetResultToggle() -> bool {
		return d->showResult;
    }
}