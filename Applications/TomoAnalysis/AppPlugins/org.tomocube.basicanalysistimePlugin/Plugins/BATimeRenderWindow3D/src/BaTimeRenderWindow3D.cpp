#include <QMenu>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoImage.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#pragma warning(pop)

#include "BaTimeRenderWindow3D.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    struct BaRenderWindow3D::Impl {
        bool is_Black{ false };
        bool is_Gradient{ true };
        bool is_White{ false };

        bool mouseLeftPressed{ false };
        bool mouseMiddlePressed{ false };
    };

    BaRenderWindow3D::BaRenderWindow3D(QWidget* parent) :
        QOivRenderWindow(parent), d{ new Impl } {
        setDefaultWindowType();
    }

    BaRenderWindow3D::~BaRenderWindow3D() {

    }

    auto BaRenderWindow3D::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* bgMenu = myMenu.addMenu(tr("Background Options"));
        auto gradient = bgMenu->addAction("Gradient");
        auto black = bgMenu->addAction("Black");
        auto white = bgMenu->addAction("White");

        gradient->setCheckable(true);
        black->setCheckable(true);
        white->setCheckable(true);

        if (d->is_Gradient)
            gradient->setChecked(true);
        else if (d->is_Black)
            black->setChecked(true);
        else if (d->is_White)
            white->setChecked(true);

        QMenu* viewMenu = myMenu.addMenu(tr("View"));
        auto view_x = viewMenu->addAction("View X");
        auto view_y = viewMenu->addAction("View Y");
        auto view_z = viewMenu->addAction("View Z");
        auto view_reset = viewMenu->addAction("Reset View");

        view_x->setCheckable(false);
        view_y->setCheckable(false);
        view_z->setCheckable(false);
        view_reset->setCheckable(false);

        QAction* selectedItem = myMenu.exec(globalPos);
        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Gradient")) {
                d->is_Gradient = true;
                d->is_Black = false;
                d->is_White = false;
                ChangeBackGround(0);
            }
            else if (text.contains("Black")) {
                d->is_Gradient = false;
                d->is_Black = true;
                d->is_White = false;
                ChangeBackGround(1);
            }
            else if (text.contains("White")) {
                d->is_Gradient = false;
                d->is_Black = false;
                d->is_White = false;
                ChangeBackGround(2);
            }
            else if (text.contains("Reset View")) {
                reset3DView();
            }
            else if (text.contains("View X")) {
                setViewDirection(0);
            }
            else if (text.contains("View Y")) {
                setViewDirection(1);
            }
            else if (text.contains("View Z")) {
                setViewDirection(2);
            }
        }
    }

    auto BaRenderWindow3D::requestUpdate() -> void {
        //getQtRenderArea()->render();
        getQtRenderArea()->update();
    }


    auto BaRenderWindow3D::reset3DView() -> void {
        auto root = getSceneGraph();
        auto volData = MedicalHelper::find<SoVolumeData>(root);
        if (volData) {
            resetCameraZoom();
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            auto zoom = getCameraZoom();
            setCameraZoom(zoom * 0.7f);
        }
    }

    auto BaRenderWindow3D::ChangeBackGround(int type) -> void {
        SbVec3f start, end;
        switch (type) {
        case 0://gradient
            start.setValue(0.7f, 0.7f, 0.8f);
            end.setValue(0.0, 0.1f, 0.3f);
            break;
        case 1://black
            start.setValue(0.0, 0.0, 0.0);
            end.setValue(0.0, 0.0, 0.0);
            break;
        case 2://white
            start.setValue(1.0, 1.0, 1.0);
            end.setValue(1.0, 1.0, 1.0);
            break;
        }
        setGradientBackground(start, end);
    }

    auto BaRenderWindow3D::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.7f,0.7f,0.8f };
        SbVec3f end_color = { 0.0,0.1f,0.3f };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(false);//perpective

        //set default navigation type
    }

    auto BaRenderWindow3D::MouseMoveEvent(SoEventCallback* node) -> void {
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (isNavigation()) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->mouseLeftPressed) {
                //spinCamera(moveEvent->getPositionFloat());
                spinCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
            if (d->mouseMiddlePressed) {
                //panCamera(moveEvent->getPositionFloat());
                panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
        }
    }

    auto BaRenderWindow3D::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }

    /*void BaRenderWindow3D::wheelEvent(QWheelEvent* event) {
        auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }*/

    auto BaRenderWindow3D::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();
        //Press Event

        //Release Event
        if (isNavigation()) {
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (!(pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25)) {
                    //startSpin(mouseButton->getPosition());
                    startSpin(mouseButton->getNormalizedPosition(getViewportRegion()));
                    d->mouseLeftPressed = true;
                }
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                //startPan(mouseButton->getPosition());
                startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                d->mouseMiddlePressed = true;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                    showContextMenu(pos);
                }
                else {
                    d->mouseLeftPressed = false;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->mouseMiddlePressed = false;
            }
        }
    }

    auto BaRenderWindow3D::setViewDirection(int axis) -> void {
        const auto root = getSceneGraph();
        const auto volData = MedicalHelper::find<SoVolumeData>(root);
        if (nullptr == volData) {
            return;
        }
        switch (axis) {
        case 0:
            MedicalHelper::orientView(MedicalHelper::SAGITTAL, getCamera(), volData);
            break;
        case 1:
            MedicalHelper::orientView(MedicalHelper::CORONAL, getCamera(), volData);
            break;
        case 2:
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            break;
        }

        setCameraZoom(getCameraZoom() * 0.7f);
    }


    auto BaRenderWindow3D::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback        
            //set handled를 통해 event를 scene의 하위 callback으로 전달하지 않고 terminate
            //instance->setSeekMode(true);
            //seek mode는 우선 지원하지 않는것으로 20.10.16 Jose T. Kim
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode가 selection 일때 alt키를 누르는 동안 임시로 navigation mode로의 전환
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation())
                setInteractionMode(false);
            else
                setInteractionMode(true);
            node->setHandled();
        }
        //Shift + F12는 native viewer event callback이 해결하도록
    }

    auto BaRenderWindow3D::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }
}
