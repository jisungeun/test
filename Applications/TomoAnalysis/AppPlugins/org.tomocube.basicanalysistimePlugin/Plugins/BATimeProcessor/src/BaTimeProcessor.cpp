#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Engines/SoImageVizEngineHeaders.h>
#pragma warning(pop)

#include <ScalarData.h>
#include <TCMeasure.h>
#include <TCMask.h>
#include <TCImage.h>

#include <ICustomAlgorithm.h>
#include <ISegmentationAlgorithm.h>
#include <IProcessor.h>

#include <ParameterList.h>
#include <ParameterRegistry.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>

#include "BaTimeProcessor.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    struct Processor::Impl {
        TCImage::Pointer refImage{ nullptr };        
        auto param2node(IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };
        auto DataListToMeausre(DataList::Pointer dl)->TCMeasure::Pointer {
            auto measure = std::make_shared<TCMeasure>();
            for(const auto &dataSet : dl->GetList()) {
                auto name = std::static_pointer_cast<ScalarData>(dataSet->GetData("Name"));
                auto organ_name = name->ValueAsString();
                auto cellcount = std::static_pointer_cast<ScalarData>(dataSet->GetData("CellCount"));                
                for(auto j=0;j<cellcount->ValueAsInt();j++) {
                    auto real_idx = std::static_pointer_cast<ScalarData>(dataSet->GetData("real_idx" + QString::number(j)))->ValueAsInt();
                    auto volume = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Volume"));
                    measure->AppendMeasure(organ_name + ".Volume", j, volume->ValueAsDouble());
                    auto sa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".SurfaceArea"));
                    measure->AppendMeasure(organ_name + ".SurfaceArea", j, sa->ValueAsDouble());
                    auto mri = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".MeanRI"));
                    measure->AppendMeasure(organ_name + ".MeanRI", j, mri->ValueAsDouble());
                    auto sph = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Sphericity"));
                    measure->AppendMeasure(organ_name + ".Sphericity", j, sph->ValueAsDouble());
                    auto pa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".ProjectedArea"));
                    measure->AppendMeasure(organ_name + ".ProjectedArea", j, pa->ValueAsDouble());
                    auto dm = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Drymass"));
                    measure->AppendMeasure(organ_name + ".Drymass", j, dm->ValueAsDouble());
                    auto ct = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Concentration"));
                    measure->AppendMeasure(organ_name + ".Concentration", j, ct->ValueAsDouble());
                    auto sc = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Score"));
                    measure->AppendMeasure(organ_name + ".Score", j, sc->ValueAsDouble());
                }                
            }
            return measure;
        }
    };    
    Processor::Processor() : d{ new Impl } {
        
    }
    Processor::~Processor() {
        
    }    
    Processor::Pointer Processor::GetInstance() {
        static Pointer theInstance(new Processor());
        return theInstance;
    }
    auto Processor::SetReferenceImage(IBaseImage::Pointer image) -> void {
        d->refImage = std::dynamic_pointer_cast<TCImage>(image);
    }    
     
    auto Processor::ImageToBoth(const QString& module_name, IParameter::Pointer param) -> std::tuple<IBaseMask::Pointer, IBaseMask::Pointer> {
        if (module_name.isEmpty()) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }
        
        if (nullptr == d->refImage) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }        
        auto moduleInstance = PluginRegistry::GetPlugin(module_name);
        auto segmentation_module = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
        
        //get segmentation parameter
        auto ai_seg_param = param->GetChild(moduleInstance->GetName() + " Parameter");
        segmentation_module->SetInput(d->refImage);
        segmentation_module->Parameter(ai_seg_param);
        if(!segmentation_module->Execute()) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }

        auto resulting_org = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput(1));
        auto resulting_inst = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput(0));

        if(nullptr == resulting_org || nullptr == resulting_inst) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }

        d->refImage = nullptr;
        
        moduleInstance = nullptr;        
        segmentation_module = nullptr;

        return std::make_tuple(resulting_inst, resulting_org);
    }
    auto Processor::ImageToValue(const QString& module_name, IParameter::Pointer param) -> DataList::Pointer {

        if(module_name.isEmpty()) {
            return DataList::New();
        }
        if(nullptr == d->refImage) {
            return DataList::New();
        }
        auto moduleInstance = PluginRegistry::GetPlugin(module_name);
        auto custom_module = std::dynamic_pointer_cast<ICustomAlgorithm>(moduleInstance);
        custom_module->SetInput(d->refImage);
        custom_module->Parameter(param);
        custom_module->Execute();

        auto result = std::dynamic_pointer_cast<DataList>(custom_module->GetOutput());

        d->refImage = nullptr;

        moduleInstance = nullptr;
        custom_module = nullptr;

        return result;
    }
    auto Processor::SetParameterValue(IParameter::Pointer param) -> void {
        //TODO 연계된 processing module에 값을 넣어 수행
    }

    auto Processor::ImageToSingleMask(const QString& module_name, IParameter::Pointer param) -> std::tuple<IBaseMask::Pointer, IBaseMask::Pointer> {
        if(module_name.isEmpty()) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }
        if(nullptr == d->refImage) {
            return std::make_tuple(TCMask::Pointer(), TCMask::Pointer());
        }
        auto moduleInstance = PluginRegistry::GetPlugin(module_name);
        auto segmentation_module = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
                
        auto pp = param->GetChild(moduleInstance->GetName() + " Parameter");        
        segmentation_module->SetInput(d->refImage);
        segmentation_module->Parameter(pp);
        segmentation_module->Execute();

        auto resulting_org = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput(1));
        auto resulting_inst = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput(0));

        d->refImage = nullptr;

        moduleInstance = nullptr;
        segmentation_module = nullptr;

        return std::make_tuple(resulting_inst,resulting_org);
    }
    auto Processor::LabelToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param) -> IBaseMask::Pointer {
        if(module_name.isEmpty()) {
            return nullptr;
        }
        if(nullptr == mask) {
            return nullptr;
        }
        auto moduleInstance = PluginRegistry::GetPlugin(module_name);
        auto segmentation_module = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);

        segmentation_module->SetInput(mask);

        auto seg_param = param->GetChild(moduleInstance->GetName() + " Parameter");

        segmentation_module->Parameter(seg_param);
        segmentation_module->Execute();

        auto resulting_label = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput());

        moduleInstance = nullptr;

        return resulting_label;
    }

    auto Processor::BinaryMaskToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param) -> IBaseMask::Pointer {
        if (module_name.isEmpty()) {            
            return nullptr;
        }        
        if(nullptr == mask) {
            return nullptr;
        }

        auto moduleInstance = PluginRegistry::GetPlugin(module_name);
        auto segmentation_module = std::dynamic_pointer_cast<ISegmentationAlgorithm>(moduleInstance);
        
        segmentation_module->SetInput(mask);

        auto label_seg_param = param->GetChild(moduleInstance->GetName() + " Parameter");

        segmentation_module->Parameter(label_seg_param);
        segmentation_module->Execute();

        auto resulting_label = std::dynamic_pointer_cast<TCMask>(segmentation_module->GetOutput());

        moduleInstance = nullptr;

        return resulting_label;
    }
    auto Processor::Measurement(const QString& module_name, IBaseMask::Pointer organ_mask, IBaseMask::Pointer label_mask, IParameter::Pointer param) -> IBaseData::Pointer {           
        if (module_name.isEmpty()) {
            return nullptr;
        }
        if (nullptr == d->refImage) {
            return nullptr;
        }
        if(nullptr == organ_mask) {
            if(nullptr == label_mask) {
                return nullptr;
            }else {
                //label only measure
                //return LabelOnlyMeasure(module_name,label_mask,param);
            }
        }else {
            if(nullptr==label_mask) {                
                //not supported for now 21.01.08 Jose T. Kim
                return nullptr;                
            }else {
                //cell-wise measure
                return BothMeasure(module_name,organ_mask, label_mask,param);
            }
        }
        return nullptr;
    }
   
    auto Processor::MaskOnlyMeasure(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param) -> IBaseData::Pointer {
        Q_UNUSED(module_name)
        Q_UNUSED(mask)
        Q_UNUSED(param)
        return nullptr;
    }
    auto Processor::BothMeasure(const QString& module_name, IBaseMask::Pointer mask, IBaseMask::Pointer label, IParameter::Pointer param) -> IBaseData::Pointer {
        auto plugin_module = PluginRegistry::GetPlugin(module_name);
        auto measure_module = std::dynamic_pointer_cast<ICustomAlgorithm>(plugin_module);                

        measure_module->SetInput(label);        
        measure_module->SetInput2(d->refImage);
        measure_module->SetInput3(mask);
                
        auto minmax = d->refImage->GetMinMax();                

        //parse processor parameters        
        if(param->ExistNode("Mask Selector")) {
            auto div = param->GetValue("Mask Selector").toString().split("!");
            QStringList dupNames;
            for (auto di : div) {
                if (!di.contains("*")) {
                    dupNames.push_back(di);
                }            
            }
            measure_module->DuplicateParameter(dupNames);
            for(auto du : dupNames) {
                auto dup_param = param->GetChild(plugin_module->GetName() + " Parameter!" + du);
                measure_module->Parameter(dup_param, du);
            }
        }
        else {
            auto procFullname = param->GetFullName();            
            auto proc = std::dynamic_pointer_cast<IProcessor>(PluginRegistry::GetPlugin(procFullname));
            auto metaParam = proc->MetaParameter();
            auto dupList = metaParam->GetDuplicatorNames();
            if (dupList.count()>0) {
                auto dupNames = std::get<0>(metaParam->GetDuplicator(dupList[0]));
                measure_module->DuplicateParameter(dupNames);
                for (auto du : dupNames) {
                    auto dup_param = param->GetChild(plugin_module->GetName() + " Parameter!" + du);
                    measure_module->Parameter(dup_param, du);
                }
            }
            else {
                auto global_param = param->GetChild(plugin_module->GetName() + " Parameter");
                global_param->SetValue("RIMin", std::get<0>(minmax));
                global_param->SetValue("RIMax", std::get<1>(minmax));
                measure_module->Parameter(global_param);
            }
        }
        
        measure_module->Execute();

        auto result = std::dynamic_pointer_cast<DataList>(measure_module->GetOutput());

        auto measure = d->DataListToMeausre(result);

        d->refImage = nullptr;
        
        measure_module = nullptr;
        plugin_module = nullptr;
        
        return measure;
    }
}
