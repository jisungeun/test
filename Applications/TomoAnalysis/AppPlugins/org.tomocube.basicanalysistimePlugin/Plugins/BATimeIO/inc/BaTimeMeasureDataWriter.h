#pragma once

#include <IMeasureWriterPort.h>

#include "BasicAnalysisTimeIOExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BasicAnalysisTimeIO_API MeasureDataWriter : public UseCase::IMeasureWriterPort {
    public:
        MeasureDataWriter();
        ~MeasureDataWriter();

        auto Write(TCMeasure::Pointer data, const QString& path) -> bool override;
        auto Modify(TCMeasure::Pointer data, const QString& path,int time_idx) -> bool override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}