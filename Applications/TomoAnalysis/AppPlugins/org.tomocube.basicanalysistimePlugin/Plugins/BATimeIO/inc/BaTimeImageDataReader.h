#pragma once

#include <IImageReaderPort.h>

#include "BasicAnalysisTimeIOExport.h"

class SoVolumeData;

namespace TomoAnalysis::BasicAnalysisTime::Plugins{
    class BasicAnalysisTimeIO_API ImageDataReader : public UseCase::IImageReaderPort {
    public:
        ImageDataReader();
        ~ImageDataReader();

        auto Read(const QString& path, const bool loadImage = false,int time_step=0) const -> TCImage::Pointer override;
        auto GetTimeSteps(const QString& path) const -> int override;
        auto ReadOiv(const QString& path)const->SoVolumeData*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}