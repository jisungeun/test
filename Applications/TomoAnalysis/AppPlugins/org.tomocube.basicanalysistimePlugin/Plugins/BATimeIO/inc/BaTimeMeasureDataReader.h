#pragma once

#include <IMeasureReaderPort.h>

#include "BasicAnalysisTimeIOExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    class BasicAnalysisTimeIO_API MeasureDataReader : public UseCase::IMeasureReaderPort {
    public:
        MeasureDataReader();
        ~MeasureDataReader();

        auto Read(const QString& path,int time_step)->TCMeasure::Pointer override;
    };
}