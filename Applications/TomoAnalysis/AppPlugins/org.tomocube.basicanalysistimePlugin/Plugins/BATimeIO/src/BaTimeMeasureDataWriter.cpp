#include <iostream>
#include <QFile>
#include <QList>

#include <TCMeasureWriter.h>
#include <TCMeasureReader.h>

#include "BaTimeMeasureDataWriter.h"


namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    struct MeasureDataWriter::Impl {
        
    };
    MeasureDataWriter::MeasureDataWriter() : d{ new Impl } {
        
    }
    MeasureDataWriter::~MeasureDataWriter() {
        
    }    
    auto MeasureDataWriter::Write(TCMeasure::Pointer data, const QString& path) -> bool {        
        auto count = data->GetCellCount();
        auto organ_names = data->GetOrganNames();
        auto keys = data->GetKeys();
        auto time_idx = data->GetTimeIndex();
        auto time_interval = data->GetTimeInterval();
        auto time_point = data->GetTimePoint();
        TC::IO::TCMeasureWriter writer(path, count, keys.size());
        auto measureText = data->GetKeys(0);        
        //write key for measure names        
        writer.WriteMeasuresMetaInfo(time_interval);
        writer.WriteMeasureNames(keys); 
        for(auto i=0;i<organ_names.size();i++) {
            writer.ClearMeausreCount(organ_names[i], time_idx);
        }
        for(auto i=0;i<count;i++) {//cell idx
            for(auto j=0;j<organ_names.size();j++) {
                auto organ = organ_names[j];
                QList<double> vals;
                for(auto k=0;k<keys.size();k++) {
                    auto key = keys[k];
                    auto final_key = organ + "." + key;
                    auto value = data->GetMeasure(final_key, i);
                    vals.push_back(value);
                }
                writer.WriteMeasures(organ, (i + 1), time_idx,time_point, vals);
            }            
        }        
        return true;
    }
    auto MeasureDataWriter::Modify(TCMeasure::Pointer data, const QString& path,int time_idx) -> bool {
        //read original measure
        TC::IO::TCMeasureReader reader(path);
        if(!reader.Exist()) {            
            return false;
        }
                
        auto organ_names = reader.GetOrganNames();
        auto measure_names = reader.GetMeasureNames();
        auto time_interval = reader.GetTimeInterval();
        
        //create temporal measure file
        auto temp_path = path.chopped(4);
        temp_path += "_temp.rep";
        
        TC::IO::TCMeasureWriter writer(temp_path, 0, measure_names.size());
        writer.WriteMeasuresMetaInfo(time_interval);
        writer.WriteMeasureNames(measure_names);                
        
        auto time_point = data->GetTimePoint();

        for(auto o = 0;o<organ_names.count();o++) {
            auto dataID = QString("Measures/") + organ_names[o];             
            auto time_points = reader.GetTimePoints(dataID);
            for(auto t=0;t<time_points.count();t++) {
                auto real_t = time_points[t];
                writer.ClearMeausreCount(organ_names[o], real_t);
                if(real_t ==time_idx) {
                    //write data from new data                    
                    auto count = data->GetCellCount();
                    writer.SetCellCount(count);
                    for(auto c= 0; c<count;c++) {
                        QList<double> vals;
                        for (auto k = 0; k < measure_names.size(); k++) {
                            auto key = measure_names[k];
                            auto final_key = organ_names[o] + "." + key;
                            auto value = data->GetMeasure(final_key, c);
                            vals.push_back(value);                            
                        }                        
                        writer.WriteMeasures(organ_names[o], (c + 1), real_t,time_point, vals);
                    }
                }else {
                    //write data from old measure
                    auto count = reader.GetCellCount(dataID, real_t);
                    writer.SetCellCount(count);
                    for(auto c =0;c<count;c++) {
                        auto vals = reader.ReadMeasures(dataID, real_t, c);                        
                        writer.WriteMeasures(organ_names[o], (c + 1), real_t,time_point, vals);
                    }
                }
            }
        }

        QFile::remove(path);
        QFile::copy(temp_path, path);
        QFile::remove(temp_path);
        
        return true;
    }
}