#include <TCMaskWriter.h>
#include <TCHMaskWriter.h>
#include <TCMaskWriterPort.h>
#include <TCMask.h>
#include <TCDataConverter.h>

#include "BaTimeMaskDataWriter.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins{
    struct MaskDataWriter::Impl {
        QStringList layer_names;
    };
    MaskDataWriter::MaskDataWriter() : d{ new Impl } {
        
    }
    MaskDataWriter::~MaskDataWriter() {
        
    }
    auto MaskDataWriter::SetLayerNames(QStringList name) -> void {
        d->layer_names = name;
    }

    auto MaskDataWriter::Write(IBaseMask::Pointer data, const QString& path,int time_step) -> bool {
        Q_UNUSED(time_step)

        TC::IO::TCMaskWriterPort writer;

        auto tcmask = std::dynamic_pointer_cast<TCMask>(data);
        auto names = tcmask->GetLayerNames();

        auto success = true;
        for (auto i = 0; i < names.count(); i++) {
            success &= writer.Write(tcmask, path, "HT",names[i], time_step);
        }

        return success;

        /*TC::IO::TCHMaskWriter writer(path);
        
        auto tcmask = std::dynamic_pointer_cast<TCMask>(data);

        auto names = tcmask->GetLayerNames();
        auto time_idx = tcmask->GetTimeStep();                

        const auto indexes = tcmask->GetBlobIndexes();        
        writer.ClearBlobCount("HT", time_idx);
                
        for(auto i=0;i<d->layer_names.size();i++) {
            writer.WriteName("HT",i,d->layer_names[i]);
        }

        for(auto i=0;i<indexes.size();i++){
            auto index = indexes[i];
            auto real_idx = 0;            
            
            if(tcmask->GetType()._to_integral() == MaskTypeEnum::MultiLayer){                
                for(auto r =0;r<d->layer_names.size();r++) {
                    if(d->layer_names[r].compare(names[i])==0) {
                        real_idx = r;
                        break;
                    }
                }
            }else {
                real_idx = index;
            }
            auto blob = tcmask->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            //if (!writer.WriteBlob("HT", time_idx, index,  bbox, code)) return false;
            if (!writer.WriteBlob("HT", time_idx, real_idx,  bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();
            //if (!writer.WriteMask("HT", time_idx, index, bbox, maskVolume)) return false;
            if (!writer.WriteMask("HT", time_idx, real_idx, bbox, maskVolume)) return false;            
        }
        const auto size = tcmask->GetSize();
        const auto res = tcmask->GetResolution();
        writer.WriteSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        writer.WriteResolution(std::get<0>(res),std::get<1>(res),std::get<2>(res));                
        writer.WriteVersion(1, 0, 0);
        return true;
        */
    }
}