#include <iostream>

#include <TCMeasureReader.h>

#include "BaTimeMeasureDataReader.h"

namespace TomoAnalysis::BasicAnalysisTime::Plugins {
    MeasureDataReader::MeasureDataReader() {
        
    }
    MeasureDataReader::~MeasureDataReader() {
        
    }
    auto MeasureDataReader::Read(const QString& path,int time_step) -> TCMeasure::Pointer {
        auto measure = std::make_shared<TCMeasure>();

        TC::IO::TCMeasureReader reader(path);

        auto organ_names = reader.GetOrganNames();//one time only

        auto measure_names = reader.GetMeasureNames();

        auto time_interval = reader.GetTimeInterval();

        //organ별로 cell이 없는 경우가 있기 때문에 organ level에서 먼저 수행해야함

        for(auto o = 0;o<organ_names.size();o++) {
            auto organ = organ_names[o];
            auto organID = QString("Measures/") + organ;
            auto cell_count = reader.GetCellCount(organID, time_step);
            for(auto i=0;i<cell_count;i++) {
                auto vals = reader.ReadMeasures(organID, time_step, i);
                for(auto m = 0;m<measure_names.size();m++) {
                    auto key = measure_names[m];
                    auto final_key = organ + "." + key;
                    auto value = vals[m];                    
                    measure->AppendMeasure(final_key, i, value);
                }
            }
        }

        measure->SetTimeIndex(time_step);
        measure->SetTimeInterval(time_interval);

        return measure;
    }
}
