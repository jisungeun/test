#pragma once

#include <IBaseData.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API IResultPanel {
    public:
        IResultPanel();
        ~IResultPanel();;
        virtual auto Update(const IBaseData::Pointer& measure)->bool = 0;
    };
}