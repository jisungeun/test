#pragma once

#include <memory>

#include <WorkingSet.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API IParameterPanel {
    public:
        IParameterPanel();
        virtual ~IParameterPanel();

        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool = 0;    
    };
}