#pragma once

#include <memory>

#include <WorkingSet.h>
#include <IParameter.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API IVizControlPanel {
    public:
        IVizControlPanel();
        virtual ~IVizControlPanel();

        virtual auto Update(IParameter::Pointer param)->bool = 0;
        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool = 0;
        virtual auto Clear()->void=0;
    };
}