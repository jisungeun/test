#pragma once

#include <memory>

#include <WorkingSet.h>
#include <ISceneManagerPort.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API BaSceneController {
    public:
        BaSceneController(UseCase::ISceneManagerPort* outPort);
        virtual ~BaSceneController();;
                
        auto SetReferenceImage(Entity::WorkingSet::Pointer workingset)->bool;
        auto SetMaskImage(Entity::WorkingSet::Pointer workingset,const QString& mask_name = QString("cellInst"))->bool;
        auto SetMultiMask(Entity::WorkingSet::Pointer working, const QStringList& multi_mask)->bool;
        auto SetResultData(Entity::WorkingSet::Pointer workingset)->bool;
        auto SetMaskList(Entity::WorkingSet::Pointer workingset)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}