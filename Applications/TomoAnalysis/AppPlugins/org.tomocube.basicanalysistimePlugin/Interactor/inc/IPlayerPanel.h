#pragma once

#include <memory>

#include <WorkingSet.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API IPlayerPanel {
    public:
        IPlayerPanel();
        virtual ~IPlayerPanel();

        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool= 0;        
    };
}