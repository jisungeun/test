#pragma once

#include <memory>

#include <QString>

#include <IBaseImage.h>
#include <IBaseMask.h>

#include "BasicAnalysisTimeInteractorExport.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    class BasicAnalysisTimeInteractor_API IRenderWindowPanel {
    public:
        IRenderWindowPanel();
        virtual ~IRenderWindowPanel();

        virtual auto Update(IBaseImage::Pointer image,IBaseMask::Pointer mask = nullptr,const QString& organ_name = QString())->bool = 0;
        virtual auto UpdateMasks(const QStringList& organ_names, IBaseMask::Pointer instMask, IBaseMask::Pointer organMask)->bool = 0;;
        virtual auto UpdateVisibility(int blob_index, bool visibility)->bool = 0;
    };
}