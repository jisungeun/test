//UseCaess
#include <ModifyScene.h>

#include "SceneController.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    struct BaSceneController::Impl {
        UseCase::ISceneManagerPort* outPort{ nullptr };        
    };
    BaSceneController::BaSceneController(UseCase::ISceneManagerPort* outPort) : d{ new Impl } {
        d->outPort = outPort;        
    }
    BaSceneController::~BaSceneController() {
        
    }

    auto BaSceneController::SetReferenceImage(Entity::WorkingSet::Pointer workingset) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::ModifyScene();
        return useCase.SetImage(workingset,d->outPort);
    }
    auto BaSceneController::SetMaskImage(Entity::WorkingSet::Pointer workingset,const QString& mask_name) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::ModifyScene();
        return useCase.SetMask(workingset, mask_name,d->outPort);
    }
    auto BaSceneController::SetMultiMask(Entity::WorkingSet::Pointer working, const QStringList& multi_mask) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ModifyScene();
        return useCase.SetMultiMask(working, multi_mask, d->outPort);
    }

    auto BaSceneController::SetMaskList(Entity::WorkingSet::Pointer workingset) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ModifyScene();
        return useCase.SetMaskList(workingset, d->outPort);
    }

    auto BaSceneController::SetResultData(Entity::WorkingSet::Pointer workingset) -> bool {
        if(nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::ModifyScene();
        return useCase.SetReport(workingset, d->outPort);
    }
}