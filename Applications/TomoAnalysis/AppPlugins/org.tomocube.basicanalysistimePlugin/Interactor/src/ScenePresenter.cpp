#include <iostream>

#include "IRenderWindowPanel.h"

#include "ScenePresenter.h"

namespace TomoAnalysis::BasicAnalysisTime::Interactor {
    struct ScenePresenter::Impl {
        IRenderWindowPanel* renderPanel{ nullptr };
        IResultPanel* resultPanel{ nullptr };
        IParameterPanel* paramPanel{ nullptr };
        IPlayerPanel* playerPanel{ nullptr };
        IVizControlPanel* vizPanel{ nullptr };
    };
    ScenePresenter::ScenePresenter(IRenderWindowPanel* panel,IResultPanel* result,IPlayerPanel* player,IParameterPanel* param,IVizControlPanel* viz) : d{ new Impl } {
        d->renderPanel = panel;
        d->resultPanel = result;
        d->paramPanel = param;
        d->playerPanel = player;
        d->vizPanel = viz;
    }
    ScenePresenter::~ScenePresenter() {
        
    }
    auto ScenePresenter::Update(const Entity::WorkingSet::Pointer& workingset) -> void {
        if (d->paramPanel) {
            d->paramPanel->Update(workingset);
        }
        if (d->vizPanel) {
            //d->vizPanel->Update(workingset->GetParameter());
            d->vizPanel->Update(workingset);
        }

    }
    auto ScenePresenter::UpdateImage(const IBaseImage::Pointer& image) -> void {
        d->renderPanel->Update(image, nullptr);
        if(d->vizPanel)
            d->vizPanel->Clear();        
    }
    auto ScenePresenter::UpdateMask(const IBaseMask::Pointer& mask,const QString& organ_name) -> bool {        
        return d->renderPanel->Update(nullptr, mask,organ_name);
    }
    auto ScenePresenter::UpdateMultiMasks(const IBaseMask::Pointer& instMask, const IBaseMask::Pointer& organMask, const QStringList& organ_names) -> bool {
        if(nullptr == instMask && nullptr == organMask) {
            return false;
        }
        return d->renderPanel->UpdateMasks(organ_names, instMask, organMask);
    }
    auto ScenePresenter::UpdateMeasure(const IBaseData::Pointer& measure) -> void {
        
        d->resultPanel->Update(measure);
    }
    auto ScenePresenter::UpdateRange(const Entity::WorkingSet::Pointer& workingset) -> void {
        if(nullptr == d->playerPanel) {
            return;
        }
        d->playerPanel->Update(workingset);
        if(nullptr != d->paramPanel) {            
            d->paramPanel->Update(workingset);
        }
    }

}