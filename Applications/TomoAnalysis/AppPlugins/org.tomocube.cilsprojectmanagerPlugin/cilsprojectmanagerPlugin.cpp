#include "cilsprojectmanagerPlugin.h"

#include <ctkPluginFrameworkLauncher.h>
#include <QtPlugin>

#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#include <MenuEvent.h>
#include <AppEvent.h>

#include "CilsProjectManagerWindow.h"

namespace TomoAnalysis::CilsProjectManager::AppUI {
	struct AppUI::cilsprojectmanagerPlugin::Impl {
		ctkPluginContext* context{ nullptr };
		CilsProjectManagerWindow* window{ nullptr };
		AppInterfaceTA* appInterface{ nullptr };
	};

	cilsprojectmanagerPlugin* cilsprojectmanagerPlugin::instance = nullptr;

	cilsprojectmanagerPlugin* cilsprojectmanagerPlugin::getInstance() {
		return instance;
	}

	cilsprojectmanagerPlugin::cilsprojectmanagerPlugin() : d(new Impl) {
		d->window = new CilsProjectManagerWindow;
		d->appInterface = new AppInterfaceTA(d->window);
		d->appInterface->SetProjectManager(true);
	}

	cilsprojectmanagerPlugin::~cilsprojectmanagerPlugin() = default;

	auto cilsprojectmanagerPlugin::start(ctkPluginContext* context)->void {
		instance = this;
		d->context = context;

		registerService(context, d->appInterface, SYMBOL);
	}

	auto cilsprojectmanagerPlugin::stop(ctkPluginContext* context)->void {
		Q_UNUSED(context);
		//Never destroy this plugin
	}

	auto cilsprojectmanagerPlugin::getPluginContext()->ctkPluginContext* const {
		return d->context;
	}
}
