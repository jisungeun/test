#pragma once

#include <memory>

#include <QString>

#include "CpmEntityExport.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	class CpmEntity_API CilsUser {
	public:
		CilsUser();
		CilsUser(CilsUser&&) noexcept;
		CilsUser(const CilsUser&);
		auto operator=(const CilsUser&)->CilsUser&;
		auto operator=(CilsUser&&) noexcept -> CilsUser&;
		auto operator==(const CilsUser&) const -> bool;
		auto operator!=(const CilsUser&) const -> bool;
		virtual ~CilsUser();

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetUsername() const -> const QString&;
		[[nodiscard]] auto GetName() const -> const QString&;

		auto SetId(int id) -> void;
		auto SetUsername(const QString& username) -> void;
		auto SetName(const QString& name) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}