#pragma once

#include <memory>
#include <optional>

#include <QMap>
#include <QFlags>
#include <QFile>

#include "CpmEntityExport.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	enum class FileState {
		None = 0x0,

		Downloading = 0x1,
		DownloadPaused = 0x2,
		Uploading = 0x4,

		TcfReady = 0x10,
		OutputReady = 0x20,
		OutputOutdated = 0x40
	};

	enum class UserType {
		None = 0x0,
		Assignee = 0x1,
		Reviewer = 0x2
	};

	enum class ExecutionStatus {
	    NotStarted,
		Executing,
		Executed
	};

	Q_DECLARE_FLAGS(FileStates, FileState);
	Q_DECLARE_FLAGS(UserTypes, UserType);

	class CpmEntity_API CilsExecution {
	public:
		CilsExecution();
		virtual ~CilsExecution();
		CilsExecution(CilsExecution&&) noexcept;
		CilsExecution(const CilsExecution&);
		auto operator=(const CilsExecution&)->CilsExecution&;
		auto operator=(CilsExecution&&) noexcept -> CilsExecution&;
		auto operator==(const CilsExecution&) const -> bool;
		auto operator!=(const CilsExecution&) const -> bool;

		auto GetId() const -> int;
		auto GetProjectId() const -> int;
		auto GetTitle() const -> const QString&;
		auto GetDataId() const -> const QString&;
		auto GetAssignee() const -> const std::optional<QString>&;
		auto GetAssignStatusAsString() const->QString;
		auto GetAssignStatus() const->ExecutionStatus;
		auto GetReviewer() const -> const std::optional<QString>&;
		auto GetReviewStatusAsString() const->QString;
		auto GetReviewStatus() const->ExecutionStatus;
		auto GetFileState() const->FileStates;
		auto GetProgress() const -> int;
		auto GetCategory() const->UserTypes;
		auto GetWorkset() const -> const QStringList&;
		auto GetTcfPath() const -> const QString&;
		auto GetOutputPath() const -> const QString&;

		auto SetId(int id) -> void;
		auto SetProjectId(int id) -> void;
		auto SetDataId(const QString& id) -> void;
		auto SetTitle(const QString& title) -> void;
		auto SetAssignee(const QString& assignee) -> void;
		auto SetAssignStatus(const QString& status) -> void;
		auto SetAssignStatus(ExecutionStatus status) -> void;
		auto SetReviewer(const QString& reviewer) -> void;
		auto SetReviewStatus(const QString& status) -> void;
		auto SetReviewStatus(ExecutionStatus status) -> void;
		auto SetFileState(FileStates status) -> void;
		auto SetProgress(int value)->void;
		auto SetCategory(UserTypes category) -> void;
		auto SetWorkset(const QStringList& workset) -> void;
		auto SetTcfPath(const QString& path) -> void;
		auto SetOutputPath(const QString& path) -> void;

		static auto UserTypeToString(UserType category)->QString;
		static auto ExecutionStatusToString(ExecutionStatus status)->QString;
		static auto StringToExecutionStatus(const QString& status)->ExecutionStatus;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}