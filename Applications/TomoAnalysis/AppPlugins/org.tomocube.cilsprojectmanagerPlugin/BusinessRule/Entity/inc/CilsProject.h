#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include "CpmEntityExport.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	class CpmEntity_API CilsProject {
	public:
		CilsProject();
		CilsProject(CilsProject&&) noexcept;
		CilsProject(const CilsProject&);
		auto operator=(const CilsProject&)->CilsProject&;
		auto operator=(CilsProject&&) noexcept -> CilsProject&;
		auto operator==(const CilsProject&) const -> bool;
		auto operator!=(const CilsProject&) const -> bool;
		virtual ~CilsProject();

		[[nodiscard]] auto GetName() const -> const QString&;
		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetType() const -> const QString&;

		auto SetName(const QString& name) -> void;
		auto SetId(int id) -> void;
		auto SetType(const QString& type) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}