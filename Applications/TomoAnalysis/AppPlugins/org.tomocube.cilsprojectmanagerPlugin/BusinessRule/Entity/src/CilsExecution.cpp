#include "CilsExecution.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	struct CilsExecution::Impl {
		int id = -1;
		int projectId = -1;
		int progress = 0;
		QString dataId;
		QString title;
		std::optional<QString> assignee;
		ExecutionStatus assignStatus = ExecutionStatus::NotStarted;
		std::optional<QString> reviewer;
		ExecutionStatus reviewStatus = ExecutionStatus::NotStarted;
		FileStates fileStatus = FileState::None;
		UserTypes category = UserType::None;
		QStringList workset;
		QString tcfPath;
		QString outputPath;
	};

	CilsExecution::CilsExecution() : d(new Impl) {}

	CilsExecution::~CilsExecution() = default;

	CilsExecution::CilsExecution(CilsExecution&& obj) noexcept : d(std::move(obj.d)) {}

	CilsExecution::CilsExecution(const CilsExecution& obj) : d(new Impl{ *obj.d }) {}

	auto CilsExecution::operator=(const CilsExecution& obj) -> CilsExecution& {
		if (this == &obj) return *this;

		if (d)
			d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	auto CilsExecution::operator=(CilsExecution&& obj) noexcept -> CilsExecution& {
		if (this == &obj) return *this;

		if (d)
			d.reset();
		d = std::move(obj.d);
		return *this;
	}

	auto CilsExecution::operator==(const CilsExecution& obj) const  -> bool {
		return (d->id == obj.d->id) && (d->progress == obj.d->progress) && (d->assignee == obj.d->assignee) && (d->reviewer == obj.d->reviewer) && (d->fileStatus == obj.d->fileStatus) && (d->assignStatus == obj.d->assignStatus) && (d->reviewStatus == obj.d->reviewStatus) && (d->outputPath == obj.d->outputPath);
	}

	auto CilsExecution::operator!=(const CilsExecution& obj) const -> bool {
		return !(this != &obj);
	}

	auto CilsExecution::GetId() const -> int {
		return d->id;
	}

	auto CilsExecution::GetProjectId() const -> int {
		return d->projectId;
	}

	auto CilsExecution::GetDataId() const -> const QString& {
		return d->dataId;
	}

	auto CilsExecution::GetTitle() const -> const QString& {
		return d->title;
	}

	auto CilsExecution::GetAssignee() const -> const std::optional<QString>& {
		return d->assignee;
	}

	auto CilsExecution::GetAssignStatusAsString() const ->QString {
		return ExecutionStatusToString(d->assignStatus);
	}

	auto CilsExecution::GetAssignStatus() const -> ExecutionStatus {
		return d->assignStatus;
	}

	auto CilsExecution::GetReviewStatusAsString() const -> QString {
		return ExecutionStatusToString(d->reviewStatus);
	}

	auto CilsExecution::GetReviewStatus() const -> ExecutionStatus {
		return d->reviewStatus;
	}

	auto CilsExecution::GetFileState() const -> FileStates {
		return d->fileStatus;
	}

	auto CilsExecution::GetProgress() const -> int {
		return d->progress;
	}

	auto CilsExecution::GetCategory() const -> UserTypes {
		return d->category;
	}

	auto CilsExecution::GetWorkset() const -> const QStringList& {
		return d->workset;
	}

	auto CilsExecution::GetTcfPath() const -> const QString& {
		return d->tcfPath;
	}

	auto CilsExecution::GetOutputPath() const -> const QString& {
		return d->outputPath;
	}

	auto CilsExecution::GetReviewer() const -> const std::optional<QString>& {
		return d->reviewer;
	}

	auto CilsExecution::SetId(int id) -> void {
		d->id = id;
	}

	auto CilsExecution::SetProjectId(int id) -> void {
		d->projectId = id;
	}

	auto CilsExecution::SetDataId(const QString& id) -> void {
		d->dataId = id;
	}

	auto CilsExecution::SetTitle(const QString& title) -> void {
		d->title = title;
	}

	auto CilsExecution::SetAssignee(const QString& assignee) -> void {
		d->assignee = assignee;
	}

	auto CilsExecution::SetAssignStatus(const QString& status) -> void {
		d->assignStatus = StringToExecutionStatus(status);
	}

	auto CilsExecution::SetAssignStatus(ExecutionStatus status) -> void {
		d->assignStatus = status;
	}

	auto CilsExecution::SetReviewStatus(const QString& status) -> void {
		d->reviewStatus = StringToExecutionStatus(status);
	}

	auto CilsExecution::SetReviewStatus(ExecutionStatus status) -> void {
		d->reviewStatus = status;
	}

	auto CilsExecution::SetFileState(FileStates status) -> void {
		d->fileStatus = status;
	}

	auto CilsExecution::SetProgress(int value) -> void {
		d->progress = value;
	}

	auto CilsExecution::SetCategory(UserTypes category) -> void {
		d->category = category;
	}

	auto CilsExecution::SetWorkset(const QStringList& workset) -> void {
		d->workset = workset;
	}

	auto CilsExecution::SetTcfPath(const QString& path) -> void {
		d->tcfPath = path;
	}

	auto CilsExecution::SetOutputPath(const QString& path) -> void {
		d->outputPath = path;
	}

	auto CilsExecution::UserTypeToString(UserType category) -> QString {
		switch (category) {
		case UserType::None:
			return "None";
		case UserType::Assignee:
			return "Assignee";
		case UserType::Reviewer:
			return "Reviewer";
		}

		return {};
	}

	auto CilsExecution::ExecutionStatusToString(ExecutionStatus status) -> QString {
		switch (status) {
		case ExecutionStatus::NotStarted:
			return "Not Started";
		case ExecutionStatus::Executing:
			return "Executing";
		case ExecutionStatus::Executed:
			return "Executed";
		}

		return {};
	}

	auto CilsExecution::StringToExecutionStatus(const QString& status) -> ExecutionStatus {
		const auto low = status.toLower();

		if (low == "executing")
			return ExecutionStatus::Executing;
		else if (low == "executed")
			return ExecutionStatus::Executed;
		else
			return ExecutionStatus::NotStarted;
	}

	auto CilsExecution::SetReviewer(const QString& reviewer) -> void {
		d->reviewer = reviewer;
	}
}
