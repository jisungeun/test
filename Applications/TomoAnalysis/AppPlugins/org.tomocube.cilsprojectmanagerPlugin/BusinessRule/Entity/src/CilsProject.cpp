#include "CilsProject.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	struct CilsProject::Impl {
		QString name;
		QString type;
		int id = -1;
	};

	CilsProject::CilsProject() : d(new Impl) {}

	CilsProject::~CilsProject() = default;

	CilsProject::CilsProject(CilsProject&& obj) noexcept : d(std::move(obj.d)) {}

	CilsProject::CilsProject(const CilsProject& obj) : d(new Impl{ *obj.d }) {}

	auto CilsProject::operator=(const CilsProject& obj) -> CilsProject& {
		if (this == &obj) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	auto CilsProject::operator=(CilsProject&& obj) noexcept -> CilsProject& {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	auto CilsProject::operator==(const CilsProject& obj) const  -> bool {
		return ((d->id == obj.d->id) && (d->name == obj.d->name) && (d->type == obj.d->type));
	}

	auto CilsProject::operator!=(const CilsProject& obj) const -> bool {
		return !(this == &obj);
	}

	auto CilsProject::GetName() const -> const QString& {
		return d->name;
	}

	auto CilsProject::GetId() const -> int {
		return d->id;
	}

	auto CilsProject::GetType() const -> const QString& {
		return d->type;
	}

	auto CilsProject::SetName(const QString& name) -> void {
		d->name = name;
	}

	auto CilsProject::SetId(int id) -> void {
		d->id = id;
	}

	auto CilsProject::SetType(const QString& type) -> void {
		d->type = type;
	}
}
