#include "CilsUser.h"

namespace TomoAnalysis::CilsProjectManager::Entity {
	struct CilsUser::Impl {
		int id = -1;
		QString username;
		QString name;
	};

	CilsUser::CilsUser() : d(new Impl) {}

	CilsUser::CilsUser(CilsUser&& obj) noexcept : d(std::move(obj.d)) {}

	CilsUser::CilsUser(const CilsUser& obj) : d(new Impl{ *obj.d }) {}

	auto CilsUser::operator=(const CilsUser& obj) -> CilsUser& {
		if (this == &obj) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	auto CilsUser::operator=(CilsUser&& obj) noexcept -> CilsUser& {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	auto CilsUser::operator==(const CilsUser& obj) const  -> bool {
		return (d->id == obj.d->id);
	}

    auto CilsUser::operator!=(const CilsUser& obj) const -> bool {
		return !(this == &obj);
    }

    CilsUser::~CilsUser() = default;

	auto CilsUser::GetId() const -> int {
		return d->id;
	}

	auto CilsUser::GetUsername() const -> const QString& {
		return d->username;
	}

	auto CilsUser::GetName() const -> const QString& {
		return d->name;
	}

	auto CilsUser::SetId(int id) -> void {
		d->id = id;
	}

	auto CilsUser::SetUsername(const QString& username) -> void {
		d->username = username;
	}

	auto CilsUser::SetName(const QString& name) -> void {
		d->name = name;
	}
}
