#pragma once

#include <typeindex>

#include <QString>
#include <QVector>

#include "IInjectable.h"

#include "CpmFrameworkExport.h"
#include "InstanceContainer.h"

#define BEGIN									auto current = 0
#define END										index = -1; return nullptr
#define ADD_DEPENDENCY(interface, instance)		if (current++ == index)\
														return CreateInstance<instance>(type, typeid(interface), key)

namespace TomoAnalysis::CilsProjectManager::Framework {
	class CpmFramework_API IDependencyInjector {
	public:
		static auto GetInstance()->IDependencyInjector*;

		template<typename T>
		auto InjectAll()->QVector<T*>;
		template<typename T>
		auto Inject(const QString& key = QString())->QVector<T*>;

		auto InjectAll(const std::type_index& type)->QVector<IInjectable*>;
		auto Inject(const std::type_index& type, const QString& key = QString())->QVector<IInjectable*>;

	protected:
		IDependencyInjector();
		virtual ~IDependencyInjector();

		template<typename T>
		auto CreateInstance(const std::type_index& requiredType, const std::type_index& interfaceType, const QString* key)->T*;
		virtual auto GetDependentInstance(const std::type_index& type, int& index, const QString* key = nullptr)->IInjectable* = 0;

	private:
		Plugins::InstanceContainer* container;
	};

	template <typename T>
	auto IDependencyInjector::InjectAll() -> QVector<T*> {
		QVector<T*> instances;
		auto interfaces = InjectAll(typeid(T));

		for (auto* i : interfaces) {
			if (auto* casted = dynamic_cast<T*>(i))
				instances.push_back(casted);
			else if (i->GetLifeTime() == LifeTime::Transient)
				delete i;
		}

		return instances;
	}

	template <typename T>
	auto IDependencyInjector::Inject(const QString& key) -> QVector<T*> {
		QVector<T*> instances;
		auto interfaces = Inject(typeid(T), key);

		for (auto* i : interfaces) {
			if (auto* casted = dynamic_cast<T*>(i))
				instances.push_back(casted);
			else if (i->GetLifeTime() == LifeTime::Transient)
				delete i;
		}

		return instances;
	}

	template <typename T>
	auto IDependencyInjector::CreateInstance(const std::type_index& requiredType, const std::type_index& interfaceType, const QString* key) -> T* {
		if (requiredType == interfaceType || requiredType == typeid(T)) {
			T* instance = nullptr;

			if (key == nullptr && !container->Contains(interfaceType, typeid(T))) {
				instance = new T;
			} else if (key != nullptr && !container->Contains(interfaceType, *key)) {
				instance = new T;

				if (instance->GetKey() != *key) {
					delete instance;
					instance = nullptr;
				}
			}

			if (instance) {
				container->Add(interfaceType, instance);
				return instance;
			}
		}

		return nullptr;
	}
}
