#pragma once

#include <memory>
#include <typeindex>

#include <QVector>

#include "IInjectable.h"

#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmFramework_API InstanceContainer final {
	public:
		auto Add(const std::type_index& type, Framework::IInjectable* injectable) -> bool;
		auto Remove(const Framework::IInjectable* injectable) -> void;
		auto Dispose(const Framework::IInjectable* injectable) -> void;

		auto GetAll(const std::type_index& type) const->QVector<Framework::IInjectable*>;
		auto GetByKey(const std::type_index& type, const QString& key = QString())const->QVector<Framework::IInjectable*>;

		auto Contains(const std::type_index& type, const QString& key = QString()) const -> bool;
		auto Contains(const std::type_index& type, Framework::IInjectable* injectable) const -> bool;
		auto Contains(const std::type_index& interfaceType, const std::type_index& instanceType) const -> bool;

		static auto GetInstance()->InstanceContainer*;

	private:
		InstanceContainer();
		~InstanceContainer();

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}