#pragma once

#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	class CpmFramework_API IUseCase {
	public:
		IUseCase();
		virtual ~IUseCase();
	};
}