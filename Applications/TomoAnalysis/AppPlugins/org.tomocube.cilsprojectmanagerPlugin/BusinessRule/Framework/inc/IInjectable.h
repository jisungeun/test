#pragma once

#include <memory>

#include <QString>

#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	enum class LifeTime {
	    Transient,
		Singleton
	};

	class CpmFramework_API IInjectable {
	public:
		IInjectable(LifeTime lifetime = LifeTime::Transient, QString key = QString());
		virtual ~IInjectable();

		auto GetLifeTime() const->LifeTime;
		auto GetKey() const -> const QString&;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}