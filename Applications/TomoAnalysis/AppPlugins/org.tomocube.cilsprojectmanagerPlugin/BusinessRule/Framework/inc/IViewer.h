#pragma once

#include <typeindex>

#include "IInjectable.h"

#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	class CpmFramework_API IViewer : public IInjectable {
	public:
		IViewer(LifeTime lifetime = LifeTime::Singleton);
		explicit IViewer(const std::type_index& type, LifeTime lifetime = LifeTime::Singleton);
		~IViewer() override;
	};
}
