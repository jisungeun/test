#pragma once

#include <memory>

namespace TomoAnalysis::CilsProjectManager::Framework {
	template<typename T>
	class ControllerHandler {
	public:
		ControllerHandler();
		~ControllerHandler() = default;

		auto operator->()->T*;

	private:
		std::unique_ptr<T> controller;
	};

	template <typename T>
	ControllerHandler<T>::ControllerHandler() : controller(new T) {}
	
	template <typename T>
	auto ControllerHandler<T>::operator->() -> T* {
		return controller.get();
	}
}