#pragma once

#include <memory>

#include "IDependencyInjector.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	template<typename T>
	class InputPortHandler {
	public:
		InputPortHandler();
		~InputPortHandler() = default;

		auto operator->()->std::unique_ptr<T>;

	private:
		IDependencyInjector* injector = nullptr;
	};

	template <typename T>
	InputPortHandler<T>::InputPortHandler() {
		injector = IDependencyInjector::GetInstance();
	}

	template <typename T>
	auto InputPortHandler<T>::operator->() -> std::unique_ptr<T> {
		auto instances = injector->Inject<T>();

		for (auto* i : instances) {
			std::unique_ptr<T> ptr(i);
			return ptr;
		}

		return {};
	}
}