#pragma once

#include "IInjectable.h"
#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	class CpmFramework_API IInputPort : public IInjectable {
	public:
		IInputPort();
		~IInputPort() override;
	};
}
