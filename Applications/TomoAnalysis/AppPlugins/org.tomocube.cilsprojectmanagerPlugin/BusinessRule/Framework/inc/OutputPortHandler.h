#pragma once

#include "IDependencyInjector.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	template<typename T>
	class OutputPortHandler {
	public:
		OutputPortHandler();
		~OutputPortHandler() = default;

		auto operator->() -> T*;

	private:
		IDependencyInjector* injector = nullptr;
	};

	template <typename T>
	OutputPortHandler<T>::OutputPortHandler() {
		injector = IDependencyInjector::GetInstance();
	}
	
    template <typename T>
	auto OutputPortHandler<T>::operator->() -> T* {
		return injector->Inject<T>().first();
	}
}
