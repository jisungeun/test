#pragma once

#pragma once

#include <memory>

#include "IDependencyInjector.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	template<typename T>
	class ViewerHandler {
	public:
		auto operator->()->T*;
		auto operator()(std::function<void(T*)> p) -> void;
		auto operator()(const QString& key, std::function<void(T*)> p) -> void;
		auto GetAll()->QVector<T*>;
		auto GetByKey(const QString& key)->QVector<T*>;
	};

	template <typename T>
	auto ViewerHandler<T>::operator->() -> T* {
		return GetAll().first();
	}

	template <typename T>
	auto ViewerHandler<T>::operator()(std::function<void(T*)> p) -> void {
		auto instances = GetAll();

		for (auto* i : instances)
			p(i);
	}

	template <typename T>
	auto ViewerHandler<T>::operator()(const QString& key, std::function<void(T*)> p) -> void {
		auto instances = GetByKey(key);

		for (auto* i : instances)
			p(i);
	}

	template <typename T>
	auto ViewerHandler<T>::GetAll() -> QVector<T*> {
		auto* injector = IDependencyInjector::GetInstance();
		return injector->InjectAll<T>();
	}

	template <typename T>
	auto ViewerHandler<T>::GetByKey(const QString& key) -> QVector<T*> {
		auto* injector = IDependencyInjector::GetInstance();
		return injector->Inject<T>(key);
	}

}
