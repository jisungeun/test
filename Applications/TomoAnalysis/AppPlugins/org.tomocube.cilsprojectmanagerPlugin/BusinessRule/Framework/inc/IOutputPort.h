#pragma once

#include "IInjectable.h"

#include "CpmFrameworkExport.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	class CpmFramework_API IOutputPort : public IInjectable {
	public:
		IOutputPort();
		~IOutputPort() override;
	};
}
