#include "IDependencyInjector.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	static IDependencyInjector* instance = nullptr;

	IDependencyInjector::IDependencyInjector() : container(Plugins::InstanceContainer::GetInstance()) {
		instance = this;
	}

	IDependencyInjector::~IDependencyInjector() = default;

	auto IDependencyInjector::GetInstance() -> IDependencyInjector* {
		return instance;
	}

	auto IDependencyInjector::InjectAll(const std::type_index& type) -> QVector<IInjectable*> {
		auto instances = container->GetAll(type);

		for (auto i = 0; ; i++) {
			if (auto* obj = GetDependentInstance(type, i))
				instances << obj;

			if (i == -1)
				break;
		}

		return instances;
	}

	auto IDependencyInjector::Inject(const std::type_index& type, const QString& key) -> QVector<IInjectable*> {
		auto instances = container->GetByKey(type, key);

		for (auto i = 0; ; i++) {
			if (key.isEmpty()) {
				if (auto* obj = GetDependentInstance(type, i))
					instances << obj;
			} else {
				if (auto* obj = GetDependentInstance(type, i, &key))
					instances << obj;
			}

			if (i == -1)
				break;
		}

		return instances;
	}
}