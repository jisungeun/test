#include "IUseCase.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
    IUseCase::IUseCase() = default;
    IUseCase::~IUseCase() = default;
}
