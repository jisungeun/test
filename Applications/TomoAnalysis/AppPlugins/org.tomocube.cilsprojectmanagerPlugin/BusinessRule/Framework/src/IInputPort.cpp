#include "IInputPort.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	IInputPort::IInputPort() : IInjectable(LifeTime::Transient) {}

	IInputPort::~IInputPort() = default;
}
