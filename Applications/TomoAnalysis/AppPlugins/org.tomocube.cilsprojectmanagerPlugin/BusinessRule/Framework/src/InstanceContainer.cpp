#include <QMap>

#include "InstanceContainer.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct InstanceContainer::Impl {
		QMap<std::type_index, QVector<Framework::IInjectable*>> container;
	};

	InstanceContainer::InstanceContainer() : d(new Impl) {}

	InstanceContainer::~InstanceContainer() = default;

	auto InstanceContainer::Add(const std::type_index& type, Framework::IInjectable* injectable) -> bool {
		if (injectable->GetLifeTime() == Framework::LifeTime::Singleton) {
			auto& instances = d->container[type];

			for (const auto* i : instances) {
				if (i == injectable)
					return false;
			}

			d->container[type].push_back(injectable);
			return true;
		}
		
		return false;
	}

	auto InstanceContainer::Remove(const Framework::IInjectable* injectable) -> void {
		for (const auto& key : d->container.keys()) {
			auto& instances = d->container[key];

			for (int i = 0; i < instances.length(); i++) {
				if (instances[i] == injectable) {
					instances.removeAt(i);
					return;
				}
			}
		}
	}

	auto InstanceContainer::Dispose(const Framework::IInjectable* injectable) -> void {
		for (const auto& key : d->container.keys()) {
			auto& instances = d->container[key];

			for (int i = 0; i < instances.length(); i++) {
				if (instances[i] == injectable) {
					const auto* instance = instances.takeAt(i);
					delete instance;
					return;
				}
			}
		}
	}

	auto InstanceContainer::GetInstance() -> InstanceContainer* {
		static InstanceContainer* instance = nullptr;

		if (!instance)
			instance = new InstanceContainer;

		return instance;
	}

	auto InstanceContainer::GetAll(const std::type_index& type) const -> QVector<Framework::IInjectable*> {
		if (d->container.contains(type))
			return d->container[type];

		return {};
	}

	auto InstanceContainer::GetByKey(const std::type_index& type, const QString& key) const -> QVector<Framework::IInjectable*> {
		QVector<Framework::IInjectable*> instances;

		if (d->container.contains(type)) {
			for (auto* i : d->container[type]) {
				if (i->GetKey() == key)
					instances.push_back(i);
			}
		}

		return instances;
	}

	auto InstanceContainer::Contains(const std::type_index& type, const QString& key) const -> bool {
		if (d->container.contains(type)) {
			for (const auto* i : d->container[type]) {
				if (i->GetKey() == key)
					return true;
			}
		}

		return false;
	}

	auto InstanceContainer::Contains(const std::type_index& type, Framework::IInjectable* injectable) const -> bool {
		if (d->container.contains(type)) {
			for (const auto* i : d->container[type]) {
				if (i == injectable)
					return true;
			}
		}

		return false;
	}

	auto InstanceContainer::Contains(const std::type_index& interfaceType, const std::type_index& instanceType) const -> bool {
		if (d->container.contains(interfaceType)) {
			for (const auto* i : d->container[interfaceType]) {
				if (instanceType == typeid(*i))
					return true;
			}
		}

		return false;
	}
}
