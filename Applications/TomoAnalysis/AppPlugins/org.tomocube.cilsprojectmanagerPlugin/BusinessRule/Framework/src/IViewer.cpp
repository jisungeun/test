#include "IViewer.h"

#include "InstanceContainer.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	IViewer::IViewer(LifeTime lifetime) : IInjectable(lifetime) {}

	IViewer::IViewer(const std::type_index& type, LifeTime lifetime) : IInjectable(lifetime) {
		auto* container = Plugins::InstanceContainer::GetInstance();

		container->Add(type, this);
	}

	IViewer::~IViewer() = default;
}
