#include "IOutputPort.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	IOutputPort::IOutputPort() : IInjectable(LifeTime::Singleton) {}

	IOutputPort::~IOutputPort() = default;
}
