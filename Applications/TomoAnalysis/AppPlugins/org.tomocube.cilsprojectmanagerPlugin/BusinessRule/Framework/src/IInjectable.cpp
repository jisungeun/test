#include "IInjectable.h"

#include "InstanceContainer.h"

namespace TomoAnalysis::CilsProjectManager::Framework {
	struct IInjectable::Impl {
		LifeTime lifetime = LifeTime::Transient;
		QString key;
	};

	IInjectable::IInjectable(LifeTime lifetime, QString key) : d(new Impl) {
		d->lifetime = lifetime;
		d->key = std::move(key);
	}

	IInjectable::~IInjectable() {
		Plugins::InstanceContainer* container = Plugins::InstanceContainer::GetInstance();
		container->Remove(this);
	}

	auto IInjectable::GetLifeTime() const -> LifeTime {
		return d->lifetime;
	}

	auto IInjectable::GetKey() const -> const QString& {
		return d->key;
	}
}
