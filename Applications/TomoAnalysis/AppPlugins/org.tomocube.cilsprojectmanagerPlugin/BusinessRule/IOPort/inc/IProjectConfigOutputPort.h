#pragma once

#include "IOutputPort.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "CilsExecution.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API IProjectConfigOutputPort : public Framework::IOutputPort {
	public:
		virtual auto SetCurrentProject(const std::optional<Entity::CilsProject>& project) -> void = 0;
		virtual auto SetCurrentUser(const std::optional<Entity::CilsUser>& user) -> void = 0;
		virtual auto SetCurrentPackage(const QString& packageName) -> void = 0;

		virtual auto GetCurrentProject()->std::optional<Entity::CilsProject> = 0;
		virtual auto GetCurrentUser()->std::optional<Entity::CilsUser> = 0;
		virtual auto GetCurrentPackage()->QString = 0;
	};
}
