#pragma once

#include <optional>

#include <QVector>
#include <QString>

#include "IInputPort.h"
#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API IAppInputPort : public Framework::IInputPort {
	public:
		// CILS Features
		virtual auto LoadProjects() -> void = 0;
		virtual auto LoadUser() -> void = 0;
		virtual auto LoadExecutions(int projectId) -> void = 0;

		virtual auto GetProjects()->QVector<Entity::CilsProject> = 0;
		virtual auto GetUser()->std::optional<Entity::CilsUser> = 0;
		virtual auto GetExecutions(int projectId)->QVector<Entity::CilsExecution> = 0;

		virtual auto StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void = 0;
		virtual auto DownloadExecution(const Entity::CilsExecution& execution) -> void = 0;

		virtual auto DownloadPreview(const QString& dataId) -> void = 0;

		virtual auto GetAssigneeOutputPath(const Entity::CilsExecution& execution)->QString = 0;
		virtual auto GetReviewerOutputPath(const Entity::CilsExecution& execution)->QString = 0;

		// Config Features
		virtual auto SetCurrentProject(std::optional<Entity::CilsProject> project) -> void = 0;
		virtual auto SetCurrentUser(std::optional<Entity::CilsUser> user) -> void = 0;
		virtual auto SetCurrentPackage(const QString& package) -> void = 0;

		virtual auto GetCurrentProject()->std::optional<Entity::CilsProject> = 0;
		virtual auto GetCurrentUser()->std::optional<Entity::CilsUser> = 0;
		virtual auto GetCurrentPackage()->QString = 0;

		// Package Features
		virtual auto GetPackageNames()->QStringList = 0;
		virtual auto GetAppSymbol()->QString = 0;

		// Setting Features
		virtual auto SetOutputPath(const QString& path) -> void = 0;
		virtual auto GetOutputPath()->QString = 0;
	};
}
