#pragma once

#include <QObject>
#include <QVector>
#include <QString>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "IOutputPort.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API IAppOutputPort : public Framework::IOutputPort {
	public:
		virtual auto UpdateProjects(const QVector<Entity::CilsProject>& projects) -> void = 0;
		virtual auto UpdateExecutions(int projectId, const QVector<Entity::CilsExecution>& executions) -> void = 0;
		virtual auto UpdateUser(const std::optional<Entity::CilsUser>& user) -> void = 0;

		virtual auto ExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void = 0;
		virtual auto ExecutionDownloaded(const Entity::CilsExecution& execution, const QString& result) -> void = 0;

		virtual auto PreviewDownloaded(const QString& dataId, const QByteArray& image) -> void = 0;

		virtual auto NotConnected() -> void = 0;
	};
}
