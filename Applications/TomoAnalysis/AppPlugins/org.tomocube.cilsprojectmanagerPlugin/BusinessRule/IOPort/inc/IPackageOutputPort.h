#pragma once

#include <QStringList>

#include "IOutputPort.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API IPackageOutputPort : public Framework::IOutputPort {
	public:
		virtual auto GetPackageNames()->QStringList = 0;
		virtual auto GetAppSymbol(const QString& packageName)->QString = 0;
	};
}
