#pragma once

#include <QVector>
#include <QString>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "IOutputPort.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API ICilsOutputPort : public Framework::IOutputPort {
	public:
		virtual auto LoadProjects() -> void = 0;
		virtual auto LoadUser() -> void = 0;
		virtual auto LoadExecutions(int projectId) -> void = 0;

		virtual auto GetProjects()->QVector<Entity::CilsProject> = 0;
		virtual auto GetUser()->std::optional<Entity::CilsUser> = 0;
		virtual auto GetExecutions(int projectId)->QVector<Entity::CilsExecution> = 0;

		virtual auto StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void = 0;
		virtual auto DownloadExecution(const Entity::CilsExecution& execution) -> void = 0;

		virtual auto DownloadPreview(const QString& dataId) -> void = 0;

		virtual auto StartUpdateExecutions() -> void = 0;
		virtual auto StopUpdateExecutions() -> void = 0;
		virtual auto SetUpdateInterval(int interval) -> void = 0;
	};
}
