#pragma once

#include "IOutputPort.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API ISettingOutputPort : public Framework::IOutputPort {
	public:
		virtual auto GetOutputPath()->QString = 0;
		virtual auto SetOutputPath(const QString& path) -> void = 0;
	};
}
