#pragma once

#include <optional>

#include <QString>
#include <QVector>
#include <QByteArray>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "IInputPort.h"

#include "CpmIOPortExport.h"

namespace TomoAnalysis::CilsProjectManager::IOPort {
	class CpmIOPort_API ICilsInputPort : public Framework::IInputPort {
	public:
		virtual auto ProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void = 0;
		virtual auto ExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void = 0;
		virtual auto UserLoaded(const std::optional<Entity::CilsUser>& user) -> void = 0;

		virtual auto StartExecutionResponded(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void = 0;
		virtual auto DownloadExecutionResponded(const Entity::CilsExecution& execution, const QString& result) -> void = 0;

		virtual auto DownloadPreviewResponded(const QString& execution, const QByteArray& image) -> void = 0;

		virtual auto NotConnected() -> void = 0;

		virtual auto UpdateExecutionsRequested() -> void = 0;
	};
}
