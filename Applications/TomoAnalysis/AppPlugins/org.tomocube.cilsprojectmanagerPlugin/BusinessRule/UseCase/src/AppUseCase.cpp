#include "AppUseCase.h"

#include <QDir>

#include "ICilsOutputPort.h"
#include "IPackageOutputPort.h"
#include "IProjectConfigOutputPort.h"
#include "ISettingOutputPort.h"
#include "OutputPortHandler.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
	struct AppUseCase::Impl {
		Framework::OutputPortHandler<IOPort::ICilsOutputPort> cils;
		Framework::OutputPortHandler<IOPort::IProjectConfigOutputPort> projectConfig;
		Framework::OutputPortHandler<IOPort::IPackageOutputPort> package;
		Framework::OutputPortHandler<IOPort::ISettingOutputPort> setting;
	};

	AppUseCase::AppUseCase() : IUseCase(), IAppInputPort(), d(new Impl) {}

	AppUseCase::~AppUseCase() = default;

	auto AppUseCase::LoadProjects() -> void {
		d->cils->LoadProjects();
	}

	auto AppUseCase::LoadUser() -> void {
		d->cils->LoadUser();
	}

	auto AppUseCase::LoadExecutions(int projectId) -> void {
		d->cils->LoadExecutions(projectId);
	}

	auto AppUseCase::GetProjects() -> QVector<Entity::CilsProject> {
		return d->cils->GetProjects();
	}

	auto AppUseCase::GetUser() -> std::optional<Entity::CilsUser> {
		return d->cils->GetUser();
	}

	auto AppUseCase::GetExecutions(int projectId) -> QVector<Entity::CilsExecution> {
		return d->cils->GetExecutions(projectId);
	}

	auto AppUseCase::StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void {
		d->cils->StartExecution(execution, userType);
	}

	auto AppUseCase::DownloadExecution(const Entity::CilsExecution& execution) -> void {
		d->cils->DownloadExecution(execution);
	}

	auto AppUseCase::DownloadPreview(const QString& dataId) -> void {
		d->cils->DownloadPreview(dataId);
	}

	auto AppUseCase::SetCurrentProject(std::optional<Entity::CilsProject> project) -> void {
		d->projectConfig->SetCurrentProject(project);

		if(project.has_value()) {
			d->cils->StartUpdateExecutions();
		}
	}

	auto AppUseCase::SetCurrentUser(std::optional<Entity::CilsUser> user) -> void {
		d->projectConfig->SetCurrentUser(user);
	}

	auto AppUseCase::SetCurrentPackage(const QString& package) -> void {
		d->projectConfig->SetCurrentPackage(package);
	}

	auto AppUseCase::GetCurrentProject() -> std::optional<Entity::CilsProject> {
		return d->projectConfig->GetCurrentProject();
	}

	auto AppUseCase::GetCurrentUser() -> std::optional<Entity::CilsUser> {
		return d->projectConfig->GetCurrentUser();
	}

	auto AppUseCase::GetCurrentPackage() -> QString {
		return d->projectConfig->GetCurrentPackage();
	}

	auto AppUseCase::GetPackageNames() -> QStringList {
		return d->package->GetPackageNames();
	}

	auto AppUseCase::GetAppSymbol() -> QString {
		const auto package = d->projectConfig->GetCurrentPackage();
		return d->package->GetAppSymbol(package);
	}

	auto AppUseCase::SetOutputPath(const QString& path) -> void {
		d->setting->SetOutputPath(path);
	}

	auto AppUseCase::GetOutputPath() -> QString {
		return d->setting->GetOutputPath();
	}

	auto AppUseCase::GetAssigneeOutputPath(const Entity::CilsExecution& execution) -> QString {
		const auto path = GetOutputPath();
		const auto user = GetCurrentUser();

		auto fullpath = QString("%1/%2/%3").arg(path).arg(user->GetUsername()).arg(execution.GetId());
		return fullpath;
	}

	auto AppUseCase::GetReviewerOutputPath(const Entity::CilsExecution& execution) -> QString {
		return execution.GetOutputPath();
	}
}