#include "SettingUseCase.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    struct SettingUseCase::Impl {

    };

    SettingUseCase::SettingUseCase() : IUseCase(), d(new Impl) {}

    SettingUseCase::~SettingUseCase() = default;
}