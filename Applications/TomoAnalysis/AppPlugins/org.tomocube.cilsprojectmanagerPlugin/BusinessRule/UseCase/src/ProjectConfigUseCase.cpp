#include "ProjectConfigUseCase.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    struct ProjectConfigUseCase::Impl {

    };

    ProjectConfigUseCase::ProjectConfigUseCase() : IUseCase(), d(new Impl) {}

    ProjectConfigUseCase::~ProjectConfigUseCase() = default;
}
