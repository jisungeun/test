#include "CilsUseCase.h"

#include "IAppOutputPort.h"
#include "ICilsOutputPort.h"
#include "IProjectConfigOutputPort.h"
#include "OutputPortHandler.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
	struct CilsUseCase::Impl {
		Framework::OutputPortHandler<IOPort::IAppOutputPort> app;
		Framework::OutputPortHandler<IOPort::IProjectConfigOutputPort> projectConfig;
		Framework::OutputPortHandler<IOPort::ICilsOutputPort> cils;
	};

	CilsUseCase::CilsUseCase() : IUseCase(), ICilsInputPort(), d(new Impl) {}

	CilsUseCase::~CilsUseCase() = default;

	auto CilsUseCase::ProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void {
		d->app->UpdateProjects(projects);
	}

	auto CilsUseCase::ExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void {
		d->app->UpdateExecutions(projectId, executions);
	}

	auto CilsUseCase::UserLoaded(const std::optional<Entity::CilsUser>& user) -> void {
		d->app->UpdateUser(user);
	}

	auto CilsUseCase::StartExecutionResponded(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {
		d->app->ExecutionStarted(execution, userType, result);
	}

	auto CilsUseCase::DownloadExecutionResponded(const Entity::CilsExecution& execution, const QString& result) -> void {
		d->app->ExecutionDownloaded(execution, result);
	}

	auto CilsUseCase::DownloadPreviewResponded(const QString& execution, const QByteArray& image) -> void {
		d->app->PreviewDownloaded(execution, image);
	}

	auto CilsUseCase::NotConnected() -> void {
		d->app->NotConnected();
	}

	auto CilsUseCase::UpdateExecutionsRequested() -> void {
		const auto proj = d->projectConfig->GetCurrentProject();

		if (proj.has_value())
			d->cils->LoadExecutions(proj->GetId());
	}
}
