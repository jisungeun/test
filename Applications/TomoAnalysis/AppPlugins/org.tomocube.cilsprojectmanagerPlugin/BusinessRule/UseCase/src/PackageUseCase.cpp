#include "PackageUseCase.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    struct PackageUseCase::Impl {

    };

    PackageUseCase::PackageUseCase() : IUseCase(), d(new Impl) {}

    PackageUseCase::~PackageUseCase() = default;
}
