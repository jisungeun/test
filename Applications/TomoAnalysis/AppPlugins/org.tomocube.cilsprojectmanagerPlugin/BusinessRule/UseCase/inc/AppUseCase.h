#pragma once

#include <memory>
#include <optional>

#include "IUseCase.h"
#include "IAppInputPort.h"

#include "CpmUseCaseExport.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
	class CpmUseCase_API AppUseCase final : public Framework::IUseCase, public IOPort::IAppInputPort{
	public:
		AppUseCase();
		~AppUseCase() override;

		// CILS Features
		auto LoadProjects() -> void override;
		auto LoadUser() -> void override;
		auto LoadExecutions(int projectId) -> void override;

		auto GetProjects()->QVector<Entity::CilsProject> override;
		auto GetUser()->std::optional<Entity::CilsUser> override;
		auto GetExecutions(int projectId)->QVector<Entity::CilsExecution> override;

		auto StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void override;
		auto DownloadExecution(const Entity::CilsExecution& execution) -> void override;
		auto DownloadPreview(const QString& dataId) -> void override;

		// Config Features
		auto SetCurrentProject(std::optional<Entity::CilsProject> project) -> void override;
		auto SetCurrentUser(std::optional<Entity::CilsUser> user) -> void override;
		auto SetCurrentPackage(const QString& package) -> void override;

		auto GetCurrentProject()->std::optional<Entity::CilsProject> override;
		auto GetCurrentUser()->std::optional<Entity::CilsUser> override;
		auto GetCurrentPackage()->QString override;

		// Package Features
		auto GetPackageNames()->QStringList override;
		auto GetAppSymbol()->QString override;

		// Setting Features
		auto SetOutputPath(const QString& path) -> void override;
		auto GetOutputPath()->QString override;

		auto GetAssigneeOutputPath(const Entity::CilsExecution& execution)->QString override;
		auto GetReviewerOutputPath(const Entity::CilsExecution& execution)->QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
