#pragma once

#include <memory>

#include "IUseCase.h"
#include "IPackageInputPort.h"

#include "CpmUseCaseExport.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    class CpmUseCase_API PackageUseCase : public Framework::IUseCase, public IOPort::IPackageInputPort {
    public:
        PackageUseCase();
        ~PackageUseCase() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
