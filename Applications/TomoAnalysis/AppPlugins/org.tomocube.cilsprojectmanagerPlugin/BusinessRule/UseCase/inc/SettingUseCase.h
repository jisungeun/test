#pragma once

#include <memory>

#include "IUseCase.h"
#include "ISettingInputPort.h"

#include "CpmUseCaseExport.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    class CpmUseCase_API SettingUseCase : public Framework::IUseCase, public IOPort::ISettingInputPort {
    public:
        SettingUseCase();
        ~SettingUseCase() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}   