#pragma once

#include <memory>

#include "IUseCase.h"
#include "ICilsInputPort.h"

#include "CpmUseCaseExport.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    class CpmUseCase_API CilsUseCase final : public Framework::IUseCase, public IOPort::ICilsInputPort {
    public:
        CilsUseCase();
        ~CilsUseCase() override;

        auto ProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void override;
        auto ExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void override;
        auto UserLoaded(const std::optional<Entity::CilsUser>& user) -> void override;

        auto StartExecutionResponded(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void override;
        auto DownloadExecutionResponded(const Entity::CilsExecution& execution, const QString& result) -> void override;

        auto DownloadPreviewResponded(const QString& execution, const QByteArray& image) -> void override;
        auto NotConnected() -> void override;

        auto UpdateExecutionsRequested() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
