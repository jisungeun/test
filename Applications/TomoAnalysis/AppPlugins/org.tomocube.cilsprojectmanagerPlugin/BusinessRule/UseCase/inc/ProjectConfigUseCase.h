#pragma once

#include <memory>

#include "IUseCase.h"
#include "IProjectConfigInputPort.h"

#include "CpmUseCaseExport.h"

namespace TomoAnalysis::CilsProjectManager::UseCase {
    class CpmUseCase_API ProjectConfigUseCase : public Framework::IUseCase, public IOPort::IProjectConfigInputPort {
    public:
        ProjectConfigUseCase();
        ~ProjectConfigUseCase() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
