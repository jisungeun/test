#pragma once

#include <IMainWindowTA.h>

#include "CilsExecution.h"

#include "ILicensed.h"

namespace TomoAnalysis::CilsProjectManager::AppUI {
	class CilsProjectManagerWindow final : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
		Q_OBJECT

	public:
		explicit CilsProjectManagerWindow(QWidget* parent = nullptr);
		~CilsProjectManagerWindow() override;

		auto GetParameter(QString name)->IParameter::Pointer override;
		auto SetParameter(QString name, IParameter::Pointer param) -> void override;

		auto Execute(const QVariantMap& params)->bool override;
		auto GetRunType()->std::tuple<bool, bool> override;
        auto TryActivate() -> bool override;
        auto TryDeactivate() -> bool override;
        auto IsActivate() -> bool override;
        auto GetMetaInfo() -> QVariantMap override;

		auto GetFeatureName() const->QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	protected slots:
		auto OnAppStarted(const QString& symbol, const QString& packageName, const QString& tcfPath, const QString& outputPath, const QString& userType, int executionId, const QString& dataId, const QString& title) -> void;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d{};
	};
}
