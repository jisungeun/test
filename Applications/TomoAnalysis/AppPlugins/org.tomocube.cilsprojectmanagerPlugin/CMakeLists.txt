project(org_tomocube_cilsprojectmanagerPlugin)

find_package(CTK REQUIRED)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/bin/Release/PluginApp)
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/bin/Debug/PluginApp)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/PluginApp)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/PluginApp)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/PluginApp)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/PluginApp)

set(CMAKE_CXX_FLAGS_RELEASE "-Od -Ob0")

set(PLUGIN_export_directive "org_tomocube_cilsprojectmanagerPlugin_EXPORT")

set(PLUGIN_SRCS
	cilsprojectmanagerPlugin.cpp
	CilsProjectManagerWindow.cpp
)

set(PLUGIN_MOC_SRCS
	cilsprojectmanagerPlugin.h
	CilsProjectManagerWindow.h
)

set(PLUGIN_UI_FORMS
	CilsProjectManagerWindow.ui
)

set(PLUGIN_resources

)

ctkFunctionGetTargetLibraries(PLUGIN_target_libraries)

ctkMacroBuildPlugin(
  NAME ${PROJECT_NAME}
  EXPORT_DIRECTIVE ${PLUGIN_export_directive}
  SRCS ${PLUGIN_SRCS} ${PLUGIN_MOC_SRCS}
  MOC_SRCS ${PLUGIN_MOC_SRCS}
  UI_FORMS ${PLUGIN_UI_FORMS}
  RESOURCES ${PLUGIN_resources}      
  TARGET_LIBRARIES 
    ${PLUGIN_target_libraries} 
    ${QT_LIBRARIES} ${CTK_LIBRARIES} 
    TC::Components::UiFramework
    TC::Components::TCFIO
    TC::Components::ParameterIO
    TC::Components::QtWidgets
    #TA Components
    Tomocube::ComponentModel
	TomoAnalysis::Components::License::LicenseModel
    TomoAnalysis::Components::Entity
    TomoAnalysis::Components::Framework

    TomoAnalysis::CilsProjectManager::Plugins::Application
    TomoAnalysis::CilsProjectManager::Plugins::DependencyInjector
 )

target_include_directories(${PROJECT_NAME}    
    PRIVATE     
        ${CMAKE_CURRENT_SOURCE_DIR} 
        ${CTK_INCLUDE_DIRS} 
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/TomoAnalysis/AppPlugins/CilsProjectManager/App") 

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}/PluginApp COMPONENT application_ta_ie)
	
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

add_subdirectory(BusinessRule)
add_subdirectory(Interactor)
add_subdirectory(Plugins)