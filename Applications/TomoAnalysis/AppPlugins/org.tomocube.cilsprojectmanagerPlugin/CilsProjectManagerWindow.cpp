#define LOGGER_TAG "[CILS Project Manager]"

#include <TCLogger.h>

#include "CilsProjectManagerWindow.h"

#include "AppEvent.h"
#include "DependencyInjector.h"
#include "ui_CilsProjectManagerWindow.h"

namespace TomoAnalysis::CilsProjectManager::AppUI {
	struct CilsProjectManagerWindow::Impl {
		Ui::CilsProjectManagerWindow* ui = nullptr;
		QVariantMap appProperties;

	};

	CilsProjectManagerWindow::CilsProjectManagerWindow(QWidget* parent) : IMainWindowTA("CILS Project Manager", "CILS Project Manager", parent), d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "CILS Project Manager";
		d->appProperties["Symbol"] = "org.tomocube.cilsprojectmanagerPlugin";
		d->appProperties["ExecutionSymbol"] = "org.tomocube.cilsexecutionPlugin";
		d->appProperties["ExecutionName"] = "CILS Executor";
	}

	CilsProjectManagerWindow::~CilsProjectManagerWindow() = default;

	auto CilsProjectManagerWindow::GetParameter(QString name) -> IParameter::Pointer {
		return nullptr;
	}

	auto CilsProjectManagerWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
		if (name == "ExecutionClosed") {
			d->ui->widget->AppClosed();
		}
	}

	auto CilsProjectManagerWindow::GetRunType() -> std::tuple<bool, bool> {
		return std::make_tuple(false, false);
	}

	auto CilsProjectManagerWindow::TryActivate() -> bool {
		static Plugins::DependencyInjector* injector = nullptr;

		if (!injector)
			injector = new Plugins::DependencyInjector;

		d->ui = new Ui::CilsProjectManagerWindow;
		d->ui->setupUi(this);

		if (!d->ui->widget->TryInitialize()) {
			TC::Framework::AppEvent openApp(TC::Framework::AppTypeEnum::APP_WITHOUT_ARG);
			openApp.setAppName("Project Selctor");
			openApp.setFullName("org.tomocube.projectselectorPlugin");			
			openApp.setClosable(false);
			publishSignal(openApp, d->appProperties["AppKey"].toString());

			TryDeactivate();

			TC::Framework::AppEvent closeThis(TC::Framework::AppTypeEnum::APP_CLOSE);
			closeThis.setFullName(d->appProperties["Symbol"].toString());
			closeThis.setAppName(d->appProperties["AppKey"].toString());
			publishSignal(closeThis, d->appProperties["Symbol"].toString());

			return false;
		}

		connect(d->ui->widget, &Plugins::Application::AppStarted, this, &CilsProjectManagerWindow::OnAppStarted);
		return true;
	}

	auto CilsProjectManagerWindow::Execute(const QVariantMap& params)->bool {
		return true;
	}

	auto CilsProjectManagerWindow::TryDeactivate() -> bool {
		auto ch = children();

		while (!ch.isEmpty()) {
			const auto* child = ch.takeFirst();
			delete child;
		}

		delete d->ui;
		d->ui = nullptr;
		return true;
	}

	auto CilsProjectManagerWindow::IsActivate() -> bool {
		return (nullptr != d->ui);
	}

	auto CilsProjectManagerWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
	}

	auto CilsProjectManagerWindow::GetFeatureName() const -> QString {
		return "org.tomocube.cilsprojectmanagerPlugin";
	}

	auto CilsProjectManagerWindow::OnAccepted() -> void {
	}

	auto CilsProjectManagerWindow::OnRejected() -> void {
	}

	auto CilsProjectManagerWindow::OnAppStarted(const QString& symbol, const QString& packageName, const QString& tcfPath, const QString& outputPath, const QString& userType, int executionId, const QString& dataId, const QString& title) -> void {
		TC::Framework::AppEvent openApp(TC::Framework::AppTypeEnum::APP_WITH_SHARE);
		openApp.setClosable(true);
		openApp.setAppName(d->appProperties["ExecutionName"].toString());
		openApp.setFullName(d->appProperties["ExecutionSymbol"].toString());

		openApp.addParameter("ExecutionRole", userType);
		openApp.addParameter("TcfPath", tcfPath);
		openApp.addParameter("OutputPath", outputPath);

		openApp.addParameter("ParentSymbol", d->appProperties["Symbol"].toString());
		openApp.addParameter("ExecutionId", QString::number(executionId));
		openApp.addParameter("ExecutionDataId", dataId);
		openApp.addParameter("ExecutionTitle", title);

		openApp.addParameter("AppNames", symbol);
		openApp.addParameter("PackageName", packageName);
		openApp.addParameter("ExecutionType", "BatchRun");

		publishSignal(openApp, d->appProperties["AppKey"].toString());
	}
}
