#include <QFileDialog>
#include <QMessageBox>

#include "CilsUser.h"

#include "ProjectConfigDialog.h"

#include <QRadioButton>

#include "AppController.h"
#include "AppPresenter.h"
#include "ControllerHandler.h"

#include "ui_ProjectConfigDialog.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct ProjectConfigDialog::Impl {
		Ui::ProjectConfigDialog ui{};

		Framework::ControllerHandler<Interactor::AppController> controller;
		int appIndex = -1;

		auto SetProjects(const QVector<Entity::CilsProject>& projects) -> void {
			ui.tableWidget->setRowCount(projects.count());

			for (int i = 0; i < projects.count(); i++) {
				ui.tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(projects[i].GetId())));
				ui.tableWidget->setItem(i, 1, new QTableWidgetItem(projects[i].GetName()));
				ui.tableWidget->setItem(i, 2, new QTableWidgetItem(projects[i].GetType()));
			}

			for (int i = 0; i < projects.length(); i++) {
				if (projects[i].GetId() == controller->GetCurrentProjectId()) {
					ui.tableWidget->setCurrentCell(i, 0);
					break;
				}
			}
		}

		auto SetUsername(const std::optional<Entity::CilsUser>& user) const -> void {
			if (user) {
				const QString message = "Welcome, %1!";
				const auto& name = user->GetName();

				ui.messageLabel->setText(message.arg(name));
			} else {
				const QString message = "User credentials not found.";
				ui.messageLabel->setText(message);
			}
		}

		auto SetOutputPath(const QString& path) const -> void {
			ui.outputLine->setText(path);
		}

		auto UpdateBtn() const -> void {
			auto f = ui.tableWidget->selectedItems().count() > 0;
			const auto isReady = ui.tableWidget->selectedItems().count() > 0 && appIndex > -1 && ui.outputLine->text().length() > 1;
			ui.initializeBtn->setEnabled(isReady);
		}
	};

	ProjectConfigDialog::ProjectConfigDialog(QWidget* parent) : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint), AppViewer(), d(new Impl) {
		d->ui.setupUi(this);

		d->ui.tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
		d->ui.tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

		connect(d->ui.tableWidget, &QTableWidget::itemSelectionChanged, this, &ProjectConfigDialog::OnItemSelectionChanged);
		connect(d->ui.initializeBtn, &QPushButton::clicked, this, &ProjectConfigDialog::OnInitializeBtnClicked);
		connect(d->ui.reloadBtn, &QPushButton::clicked, this, &ProjectConfigDialog::OnReloadBtnClicked);
		connect(d->ui.outputBtn, &QPushButton::clicked, this, &ProjectConfigDialog::OnOutputBtnClicked);

		d->SetProjects(d->controller->GetProjects());
		d->SetOutputPath(d->controller->GetOutputPath());

		d->controller->LoadUser();
		d->controller->LoadProjects();

		auto packages = d->controller->GetPackageNames();

		for (int i = 0; i < packages.count(); i++) {
			auto* radio = new QRadioButton(packages[i], this);

			connect(radio, &QRadioButton::toggled, this,
				[this, i](bool toggled) -> void {
					if (toggled) {
						d->appIndex = i; d->UpdateBtn();
					}
				}
			);

			radio->setChecked(packages[i] == d->controller->GetCurrentPackage());
			d->ui.appFrame->layout()->addWidget(radio);
		}
	}

	ProjectConfigDialog::~ProjectConfigDialog() = default;

	auto ProjectConfigDialog::OnInitializeBtnClicked() -> void {
		const auto row = d->ui.tableWidget->currentRow();

		if (row >= 0) {
			auto pid = d->ui.tableWidget->item(row, 0)->text().toInt();
			d->controller->SetCurrentUser();
			d->controller->SetCurrentProject(pid);
			d->controller->SetCurrentPackage(d->appIndex);
			d->controller->SetOutputPath(d->ui.outputLine->text());

			accept();
		}
	}

	auto ProjectConfigDialog::OnReloadBtnClicked() -> void {
		d->controller->LoadUser();
		d->controller->LoadProjects();
	}

	auto ProjectConfigDialog::OnOutputBtnClicked() -> void {
		const auto path = QFileDialog::getExistingDirectory(this, "Set output path");

		if (!path.isEmpty()) {
			const QFileInfo info(path);

			if (!info.exists() || !info.isDir()) {
				if (QMessageBox::information(this, "No path found", "The output path does not exist. Do you want to create?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
					if (!info.absoluteDir().mkpath(".")) {
						QMessageBox::information(this, "Cannot create path", "Failed in creating path.");
						return;
					}
				}
			}

			if (!info.isReadable() || !info.isWritable()) {
				QMessageBox::information(this, "Output Path Error", "The output path is not accessible.");
				return;
			}

			d->ui.outputLine->setText(path);
			d->UpdateBtn();
		}
	}

	auto ProjectConfigDialog::OnItemSelectionChanged() -> void {
		d->UpdateBtn();
	}

	auto ProjectConfigDialog::OnProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void {
		d->SetProjects(projects);
	}

	auto ProjectConfigDialog::OnUserLoaded(const std::optional<Entity::CilsUser>& user) -> void {
		d->SetUsername(user);
	}

	void ProjectConfigDialog::OnNotConnected() {
		d->SetUsername({});
	}
}
