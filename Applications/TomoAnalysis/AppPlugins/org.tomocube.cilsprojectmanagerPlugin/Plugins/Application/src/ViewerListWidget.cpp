#include "ViewerListWidget.h"
#include "ui_ViewerListWidget.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
    struct ViewerListWidget::Impl {
        Ui::ViewerListWidget ui;
    };

    ViewerListWidget::ViewerListWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
        d->ui.setupUi(this);
    }

    ViewerListWidget::~ViewerListWidget() = default;
}
