#include "Application.h"

#include <QDir>
#include <QMessageBox>

#include "AppController.h"
#include "AppPresenter.h"
#include "ControllerHandler.h"
#include "ProjectConfigDialog.h"

#include "ui_Application.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct Application::Impl {
		Ui::Application ui{};
		int selectedId = -1;

		Framework::ControllerHandler<Interactor::AppController> controller;

		auto UpdateBtns() -> void {
			ui.downloadBtn->setVisible(false);
			ui.assigneeBtn->setVisible(false);
			ui.reviewerBtn->setVisible(false);
		}

		auto UpdateBtns(Entity::FileStates state, Entity::UserTypes type) -> void {
			UpdateBtns();

			if (state.testFlag(Entity::FileState::Downloading) || state.testFlag(Entity::FileState::Uploading))
				return;

			if (state.testFlag(Entity::FileState::None) && !type.testFlag(Entity::UserType::None))
				ui.downloadBtn->setVisible(true);

			if (state.testFlag(Entity::FileState::TcfReady)) {
				if (type.testFlag(Entity::UserType::Assignee))
					ui.assigneeBtn->setVisible(true);
				if (type.testFlag(Entity::UserType::Reviewer)) {
					if (state.testFlag(Entity::FileState::OutputOutdated))
						ui.downloadBtn->setVisible(true);
					else
						ui.reviewerBtn->setVisible(true);
				}
			}
		}

		auto UpdatePreviews() -> void {
			if (auto id = ui.preview->GetNonPreviewDataId(); !id.isEmpty())
				controller->DownloadPreview(id);
		}
	};

	Application::Application(QWidget* parent) : QWidget(parent), AppViewer(), d(new Impl) {
		d->ui.setupUi(this);

		d->ui.assigneeBtn->setVisible(false);
		d->ui.reviewerBtn->setVisible(false);
		d->ui.downloadBtn->setVisible(false);
		d->ui.assigneeBtn->setObjectName("bt-square-primary");
		d->ui.reviewerBtn->setObjectName("bt-square-primary");
		d->ui.downloadBtn->setObjectName("bt-square-primary");

		connect(d->ui.treeView, &TreeViewWidget::ExecutionSelected, this, &Application::OnExecutionSelected);
		connect(d->ui.preview, &PreviewWidget::ExecutionSelected, this, &Application::OnExecutionSelected);

		connect(d->ui.prefBtn, &QPushButton::clicked, this, &Application::OnPrefBtnClicked);
		connect(d->ui.downloadBtn, &QPushButton::clicked, this, &Application::OnDownloadBtnClicked);
		connect(d->ui.assigneeBtn, &QPushButton::clicked, this, &Application::OnAssigneeBtnClicked);
		connect(d->ui.reviewerBtn, &QPushButton::clicked, this, &Application::OnReviewerBtnClicked);
	}

	Application::~Application() = default;

	auto Application::TryInitialize() -> bool {
		if (ProjectConfigDialog dialog; dialog.exec() != QDialog::Accepted)
			return false;

		d->controller->LoadExecutions();

		return true;
	}

	auto Application::AppClosed() -> void {
		d->ui.prefBtn->setEnabled(true);
		d->ui.assigneeBtn->setEnabled(true);
		d->ui.reviewerBtn->setEnabled(true);
	}

	auto Application::OnExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void {
		if (projectId == d->controller->GetCurrentProjectId()) {
			d->ui.treeView->SetExecutions(executions);
			d->ui.preview->SetExecutions(executions);

			d->UpdatePreviews();
		}
	}

	auto Application::OnExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {
		if (result.isNull() || result.isEmpty()) {
			const auto packageName = d->controller->GetCurrentPackage();
			const auto symbol = d->controller->GetAppSymbol();
			const auto tcfPath = execution.GetTcfPath();
			const auto outputPath = d->controller->GetOutputPath(execution.GetId(), userType);
			const auto type = Entity::CilsExecution::UserTypeToString(userType);

			emit AppStarted(symbol, packageName, tcfPath, outputPath, type, execution.GetId(), execution.GetDataId(), execution.GetTitle());
		} else {
			d->ui.prefBtn->setEnabled(true);
			d->ui.assigneeBtn->setEnabled(true);
			d->ui.reviewerBtn->setEnabled(true);

			QMessageBox::warning(this, "Error", result);
		}
	}

	auto Application::OnDownloadStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {
		d->ui.downloadBtn->setEnabled(true);

		if (!result.isNull() && !result.isEmpty())
			QMessageBox::warning(this, "Error", result);
	}

	auto Application::OnPreviewDownloaded(const QString& dataId, const QByteArray& image) -> void {
		d->ui.preview->UpdateExecutionPreview(dataId, image);

		d->UpdatePreviews();
	}

	void Application::OnNotConnected() {
		d->UpdatePreviews();
	}

	auto Application::OnExecutionSelected(QWidget* sender, int executionId) -> void {
		d->selectedId = executionId;

		if (sender == d->ui.preview)
			d->ui.treeView->Select(executionId);
		else if (sender == d->ui.treeView)
			d->ui.preview->Select(executionId);

		if (executionId > -1) {
			for (auto& ex : d->controller->GetExecutions()) {
				if (ex.GetId() == executionId) {
					d->ui.execView->SetExecution(&ex);
					d->UpdateBtns(ex.GetFileState(), ex.GetCategory());

					return;
				}
			}
		} else {
			d->UpdateBtns();
			d->ui.execView->SetExecution(nullptr);
		}
	}

	auto Application::OnPrefBtnClicked() -> void {
		if (ProjectConfigDialog dialog; dialog.exec() == QDialog::Accepted) {
			d->controller->LoadExecutions();

			const auto executions = d->controller->GetExecutions();
			d->ui.treeView->SetExecutions(executions);
			d->ui.preview->SetExecutions(executions);

			d->UpdatePreviews();
		}
	}

	auto Application::OnDownloadBtnClicked() -> void {
		if (d->selectedId > -1) {
			d->ui.downloadBtn->setEnabled(false);
			d->controller->DownloadExecution(d->selectedId);
		}
	}

	auto Application::OnAssigneeBtnClicked() -> void {
		if (d->selectedId > -1) {
			d->ui.prefBtn->setEnabled(false);
			d->ui.assigneeBtn->setEnabled(false);
			d->ui.reviewerBtn->setEnabled(false);
			d->controller->StartExecution(d->selectedId, Entity::UserType::Assignee);
		}
	}

	auto Application::OnReviewerBtnClicked() -> void {
		if (d->selectedId > -1) {
			d->ui.prefBtn->setEnabled(false);
			d->ui.assigneeBtn->setEnabled(false);
			d->ui.reviewerBtn->setEnabled(false);
			d->controller->StartExecution(d->selectedId, Entity::UserType::Reviewer);
		}
	}
}
