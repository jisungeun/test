#include <QTreeWidgetItem>
#include <QAction>
#include <QMenu>
#include <QProcess>
#include <QDir>

#include "TreeViewWidget.h"

#include "ui_TreeViewWidget.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct TreeViewWidget::Impl {
		Ui::TreeViewWidget ui{};

		QTreeWidgetItem* project = nullptr;
		QTreeWidgetItem* assign = nullptr;
		QTreeWidgetItem* review = nullptr;
		QTreeWidgetItem* execution = nullptr;

		QIcon cloud;
		QIcon up;
		QIcon down;

		int selectedId = -1;
		bool preventSignals = false;

		auto GetItemById(int id) const -> const QTreeWidgetItem* {
			auto items = GetChildItems();

			for (const auto* i : items) {
				if (i->text(2).toInt() == id)
					return i;
			}

			return nullptr;
		}

		auto GetChildItems() const -> QVector<QTreeWidgetItem*> {
			QVector<QTreeWidgetItem*> items;

			for (int i = 0; i < project->childCount(); i++) {
				const auto* child = project->child(i);

				for (int j = 0; j < child->childCount(); j++) {
					auto* ex = child->child(j);

					items.push_back(ex);
				}
			}

			return items;
		}

		auto CreateTreeItemOfExecution(QTreeWidgetItem* parent, const Entity::CilsExecution& ex) const -> void {
			auto* sub = new QTreeWidgetItem(parent);
			sub->setText(0, ex.GetTitle());
			sub->setText(2, QString::number(ex.GetId()));
			sub->setText(3, QDir::toNativeSeparators(ex.GetTcfPath()));
			sub->setText(4, QDir::toNativeSeparators(ex.GetOutputPath()));

			if (ex.GetFileState().testFlag(Entity::FileState::Downloading)) {
				sub->setIcon(1, down);
			} else if (ex.GetFileState().testFlag(Entity::FileState::Uploading)) {
				sub->setIcon(1, up);
			} else if (!ex.GetFileState().testFlag(Entity::FileState::TcfReady)) {
				sub->setIcon(1, cloud);
			}

			if (QString::number(ex.GetId()) == selectedId)
				sub->setSelected(true);
		}
	};

	TreeViewWidget::TreeViewWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);

		d->cloud = QPixmap::fromImage(QImage(":/app/icons/cloud_white.png"));
		d->up = QPixmap::fromImage(QImage(":/app/icons/up_white.png"));
		d->down = QPixmap::fromImage(QImage(":/app/icons/down_white.png"));

		connect(d->ui.treeWidget, &QTreeWidget::currentItemChanged, this, &TreeViewWidget::OnCurrentItemChanged);
		connect(d->ui.treeWidget, &QTreeWidget::itemSelectionChanged, this, &TreeViewWidget::OnItemSelectionChanged);

		d->ui.treeWidget->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
		d->ui.treeWidget->header()->setSectionResizeMode(1, QHeaderView::Fixed);
		d->ui.treeWidget->header()->resizeSection(1, 15);
		d->ui.treeWidget->header()->setSectionHidden(2, true);
		d->ui.treeWidget->header()->setSectionHidden(3, true);
		d->ui.treeWidget->header()->setSectionHidden(4, true);

		d->project = new QTreeWidgetItem(d->ui.treeWidget);
		d->assign = new QTreeWidgetItem(d->project);
		d->review = new QTreeWidgetItem(d->project);
		d->execution = new QTreeWidgetItem(d->project);

		d->project->setText(0, "ProjectId");
		d->assign->setText(0, "Assignee");
		d->review->setText(0, "Review");
		d->execution->setText(0, "Execution");
		d->project->setText(2, "-1");
		d->assign->setText(2, "-1");
		d->review->setText(2, "-1");
		d->execution->setText(2, "-1");

		d->ui.treeWidget->addTopLevelItem(d->project);
		d->ui.treeWidget->expandAll();
		d->ui.treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
		connect(d->ui.treeWidget, &QTreeView::customContextMenuRequested, this, &TreeViewWidget::OnContextLoaded);
	}

	TreeViewWidget::~TreeViewWidget() = default;

	auto TreeViewWidget::GetProjectName() const -> QString {
		return d->project->text(0);
	}

	auto TreeViewWidget::SetProjectName(const QString& name) -> void {
		d->project->setText(0, name);
	}

	auto TreeViewWidget::SetExecutions(const QVector<Entity::CilsExecution>& executions) -> void {
		d->preventSignals = true;
		for (auto i : d->assign->takeChildren()) delete i;
		for (auto i : d->review->takeChildren()) delete i;
		for (auto i : d->execution->takeChildren()) delete i;
		d->preventSignals = false;

		for (auto& ex : executions) {
			d->CreateTreeItemOfExecution(d->execution, ex);

			if (ex.GetCategory().testFlag(Entity::UserType::Assignee))
				d->CreateTreeItemOfExecution(d->assign, ex);

			if (ex.GetCategory().testFlag(Entity::UserType::Reviewer))
				d->CreateTreeItemOfExecution(d->review, ex);
		}

		d->ui.treeWidget->expandAll();
		OnItemSelectionChanged();
	}

	auto TreeViewWidget::Select(int executionId) -> void {
		d->selectedId = executionId;

		OnItemSelectionChanged();
	}

	auto TreeViewWidget::OnContextLoaded(const QPoint& pos) -> void {
		const auto axis = d->ui.treeWidget->mapToGlobal(pos);

		if (d->selectedId > -1) {
			QMenu menu;

			auto* item = d->GetItemById(d->selectedId);

			QAction openTcf("Show TCF in explorer");
			QAction openOutput("Show Output Files in explorer");

			if (const QDir dir(item->text(3)); !item->text(3).isEmpty() && dir.exists()) {
				menu.addAction(&openTcf);
			}

			if (const QDir dir(item->text(4)); !item->text(4).isEmpty() && dir.exists()) {
				menu.addAction(&openOutput);
			}

			if (menu.actions().count() > 0) {
				const auto *result = menu.exec(axis);

				if (result == &openTcf)
					QProcess::startDetached("explorer.exe", { item->text(3) });

				if (result == &openOutput)
					QProcess::startDetached("explorer.exe", { item->text(4) });
			}
		}
	}

	auto TreeViewWidget::OnCurrentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void {
		if (!d->preventSignals) {
			d->selectedId = (current) ? current->text(2).toInt() : -1;

			OnItemSelectionChanged();
		}
	}

	auto TreeViewWidget::OnItemSelectionChanged() -> void {
		d->project->setSelected(false);
		d->execution->setSelected(false);
		d->review->setSelected(false);
		d->assign->setSelected(false);

		auto allItems = d->GetChildItems();
		for (auto* i : allItems) {
			if (i->isSelected() == false && i->text(2).toInt() == d->selectedId) {
				i->setSelected(true);
				return;
			}

			if (i->isSelected() == true && i->text(2).toInt() != d->selectedId) {
				i->setSelected(false);
				return;
			}
		}

		emit ExecutionSelected(this, d->selectedId);
	}
}
