#include "ExecutionViewWidget.h"
#include "ui_ExecutionViewWidget.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct ExecutionViewWidget::Impl {
		Ui::ExecutionViewWidget ui;
	};

	ExecutionViewWidget::ExecutionViewWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
	}

	ExecutionViewWidget::~ExecutionViewWidget() = default;

	auto ExecutionViewWidget::SetExecution(const Entity::CilsExecution* execution) -> void {
		Clear();

		if (execution) {
			d->ui.dataId->setText(QString(execution->GetDataId()).replace("-", " ").replace(".", " "));
			d->ui.dataTitle->setText(QString(execution->GetTitle()).replace("-", " ").replace(".", " "));
			d->ui.assigneeStatus->setText(execution->GetAssignStatusAsString());
			d->ui.reviewerStatus->setText(execution->GetReviewStatusAsString());

			for (const auto& w : execution->GetWorkset())
				d->ui.workset->setText(d->ui.workset->text() + w + "\n");

			if (execution->GetAssignee().has_value())
				d->ui.assignee->setText(execution->GetAssignee().value());

			if (execution->GetReviewer().has_value())
				d->ui.reviewer->setText(execution->GetReviewer().value());
		}
	}

	auto ExecutionViewWidget::Clear() -> void {
		d->ui.dataId->setText(" ");
		d->ui.dataTitle->setText(" ");
		d->ui.workset->setText(" ");
		d->ui.assignee->setText(" ");
		d->ui.assigneeStatus->setText(" ");
		d->ui.reviewer->setText(" ");
		d->ui.reviewerStatus->setText(" ");
	}
}
