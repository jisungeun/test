#include <QPainter>
#include <QScrollBar>
#include <QWheelEvent>

#include "PreviewWidget.h"
#include "ui_PreviewWidget.h"
#include "GalleryView.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct PreviewWidget::Impl {
		Ui::PreviewWidget ui{};
		TC::GalleryView view{};
		QVector<Entity::CilsExecution> executions;
		QMap<QString, QPixmap> images;
		QPixmap cloud;

		int selectedIndex = -1;
		bool preventSignals = false;

		mutable bool ctrlPressed = false;

		static auto CreatePixmap(const QColor& color) -> QPixmap {
			QPixmap pixmap(1, 1);
			pixmap.fill(Qt::transparent);
			QPainter painter(&pixmap);
			painter.setPen(Qt::NoPen);
			painter.setBrush(color);
			painter.drawRect(0, 0, 1, 1);
			painter.end();
			return pixmap;
		}

		static auto CreateMark(const QColor& color) -> QPixmap {
			QPixmap pixmap(10, 10);
			pixmap.fill(Qt::transparent);
			QPainter painter(&pixmap);
			painter.setPen(Qt::NoPen);
			painter.setBrush(color);
			painter.drawRect(0, 0, 1, 1);
			painter.end();
			return pixmap;
		}

		auto CreateCloudIcon() -> QPixmap {
			QPixmap pixmap(250, 250);
			pixmap.fill(Qt::transparent);
			QPainter painter(&pixmap);
			painter.setPen(Qt::NoPen);
			painter.drawPixmap(10, 5, 54, 49, cloud);
			painter.end();
			return pixmap;
		}

		static auto CreateProgress(int value) -> QPixmap {
			QPixmap pixmap(100, 1);
			pixmap.fill(Qt::transparent);
			QPainter painter(&pixmap);
			painter.setPen(Qt::NoPen);
			painter.setBrush(QColor(38, 182, 202, 144));
			painter.drawRect(value, 0, 100 - value, 1);
			painter.end();
			return pixmap;
		}

		auto SetHoveredVisibility(const TC::GalleryContent* content, bool visibility) -> void {
			if (auto* layer = content->GetLayer("hovered"))
				layer->SetVisibility(visibility);
		}

		auto SetSelectedVisibility(const TC::GalleryContent* content, bool visibility) -> void {
			if (auto* layer = content->GetLayer("selected"))
				layer->SetVisibility(visibility);
		}

		auto GetCurrentPixmaps(const QVector<Entity::CilsExecution>& executions) -> QMap<int, QPixmap> {
			QMap<int, QPixmap> images;

			for (int i = 0; i < executions.count(); i++) {
				for (int j = 0; j < this->executions.count(); j++) {
					if (executions[i].GetId() == this->executions[j].GetId()) {
						const auto* item = this->view.GetItemAt(j);
						images[i] = item->GetPixmap();
					}
				}
			}

			return images;
		}
	};

	PreviewWidget::PreviewWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->cloud = QPixmap::fromImage(QImage(":/app/icons/cloud_white.png"));

		d->view.AddDefaultLayer("hovered", d->CreatePixmap(QColor(255, 255, 255, 30)), false);
		d->view.AddDefaultLayer("selected", d->CreatePixmap(QColor(255, 255, 255, 60)), false);
		d->view.SetColumnCount(8);
		d->view.InstallEventFilterOnScrollViewer(this);

		connect(&d->view, &TC::GalleryView::ItemClicked, this, &PreviewWidget::OnSelected);
		connect(&d->view, &TC::GalleryView::ItemHovered, this, &PreviewWidget::OnHovered);

		this->layout()->addWidget(&d->view);
	}

	PreviewWidget::~PreviewWidget() = default;

	auto PreviewWidget::SetExecutions(const QVector<Entity::CilsExecution>& executions) -> void {
		int selectedId = -1;
		if (d->selectedIndex > -1)
			selectedId = d->executions[d->selectedIndex].GetId();

		auto images = d->GetCurrentPixmaps(executions);

		d->executions = executions;
		d->selectedIndex = -1;
		d->view.Clear();
		d->view.AddItems(executions.size());

		for (const auto& idx : images.keys())
			d->view.GetItemAt(idx)->SetPixmap(images[idx]);

		for (int i = 0; i < d->executions.count(); i++) {
			if (d->executions[i].GetFileState().testFlag(Entity::FileState::Downloading)) {
				auto* progress = d->view.GetItemAt(i)->CreateLayer("icon");
				progress->SetVisibility(true);
				progress->SetPixmap(d->CreateProgress(d->executions[i].GetProgress()));
			} else if (d->executions[i].GetFileState().testFlag(Entity::FileState::Uploading)) {
				auto* layer = d->view.GetItemAt(i)->CreateLayer("icon");
				layer->SetVisibility(true);
				layer->SetPixmap(d->CreatePixmap({ 38, 182, 202, 50 }));
			} else if (!d->executions[i].GetFileState().testFlag(Entity::FileState::TcfReady) ||
				d->executions[i].GetFileState().testFlag(Entity::FileState::OutputOutdated)) {
				auto* progress = d->view.GetItemAt(i)->CreateLayer("icon");
				progress->SetVisibility(true);
				progress->SetPixmap(d->CreateCloudIcon());
			}

			if (d->images.contains(d->executions[i].GetDataId()))
				d->view.GetItemAt(i)->SetPixmap(d->images[d->executions[i].GetDataId()]);
		}

		if (selectedId > -1) {
			for (const auto& ex : d->executions) {
				if (ex.GetId() == selectedId) {
					Select(ex.GetId());
					break;
				}
			}
		}
	}

	auto PreviewWidget::UpdateExecutionPreview(const QString& dataId, const QByteArray& image) -> void {
		for (int i = 0; i < d->executions.count(); i++) {
			if (d->executions[i].GetDataId() == dataId) {
				auto* item = d->view.GetItemAt(i);
				QPixmap pixmap;
				pixmap.loadFromData(image);
				d->images[dataId] = pixmap;
				item->SetPixmap(pixmap);
			}
		}
	}

	auto PreviewWidget::GetNonPreviewDataId() const -> QString {
		auto [from, to] = d->view.GetVisibleIndex();

		// Prior to visible execution.
		for (int i = from; i < to; i++) {
			if (d->view.GetItemAt(i)->GetPixmap().isNull()) {
				return d->executions[i].GetDataId();
			}
		}

		// Last ones
		for (int i = 0; i < d->executions.count(); i++) {
			if (i >= from || i < to) continue;
			if (d->view.GetItemAt(i)->GetPixmap().isNull()) {
				return d->executions[i].GetDataId();
			}
		}

		return {};
	}

	auto PreviewWidget::Select(int executionId) -> void {
		if (executionId > -1) {
			for (int i = 0; i < d->executions.count(); i++) {
				if (d->executions[i].GetId() == executionId) {
					if (d->selectedIndex != i) {
						QSignalBlocker blocker(this);
						OnSelected(i, nullptr);
						d->view.ScrollToIndex(i);
						d->preventSignals = false;
					}

					return;
				}
			}
		} else if (d->selectedIndex > -1) {
			OnSelected(d->selectedIndex, nullptr);
		}
	}

	auto PreviewWidget::OnSelected(int index, QMouseEvent* event) -> void {
		if (d->selectedIndex != index) {
			if (const auto* item = d->view.GetItemAt(d->selectedIndex))
				d->SetSelectedVisibility(item, false);
			d->selectedIndex = index;
			if (const auto* item = d->view.GetItemAt(index))
				d->SetSelectedVisibility(item, true);

			emit ExecutionSelected(this, d->executions[index].GetId());
		} else {
			if (const auto* item = d->view.GetItemAt(d->selectedIndex))
				d->SetSelectedVisibility(item, false);
			d->selectedIndex = -1;
			emit ExecutionSelected(this, -1);
		}
	}

	auto PreviewWidget::OnHovered(int index, bool hovered) -> void {
		if (const auto* item = d->view.GetItemAt(index))
			if (auto* layer = item->GetLayer("hovered"))
				layer->SetVisibility(hovered);
	}

	void PreviewWidget::wheelEvent(QWheelEvent* event) {
		QWidget::wheelEvent(event);

		if (d->ctrlPressed) {
			if (event->angleDelta().y() > 0 && d->view.GetColumnCount() > 1)
				d->view.SetColumnCount(d->view.GetColumnCount() - 1);
			else if (event->angleDelta().y() < 0)
				d->view.SetColumnCount(d->view.GetColumnCount() + 1);
		}
	}

	void PreviewWidget::keyPressEvent(QKeyEvent* event) {
		QWidget::keyPressEvent(event);

		if (event->key() == Qt::Key_Control)
			d->ctrlPressed = true;
	}

	void PreviewWidget::keyReleaseEvent(QKeyEvent* event) {
		QWidget::keyReleaseEvent(event);

		if (event->key() == Qt::Key_Control)
			d->ctrlPressed = false;
	}

	bool PreviewWidget::eventFilter(QObject* watched, QEvent* event) {
		if (d->ctrlPressed && event->type() == QEvent::Wheel) {
			if (dynamic_cast<QScrollBar*>(watched)) {
				auto* wheel = dynamic_cast<QWheelEvent*>(event);
				wheelEvent(wheel);
				return true;
			}
		}

		return QWidget::eventFilter(watched, event);
	}
}
