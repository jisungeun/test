#pragma once

#include <QWidget>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"

#include "AppViewer.h"

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API Application final : public QWidget, public Interactor::AppViewer{
		Q_OBJECT

	public:
		explicit Application(QWidget * parent = nullptr);
		~Application() override;

		auto TryInitialize() -> bool;
		auto AppClosed() -> void;

	protected:
		auto OnExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void override;

		auto OnExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void override;
		auto OnDownloadStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void override;

		auto OnPreviewDownloaded(const QString& dataId, const QByteArray& image) -> void override;
		auto OnNotConnected() -> void override;

	signals:
		auto AppStarted(const QString& symbol, const QString& packageName, const QString& tcfPath, const QString& outputPath, const QString& userType, int executionId, const QString& dataId, const QString& title) -> void;

	protected slots:
		auto OnExecutionSelected(QWidget* sender, int executionId) -> void;
		auto OnPrefBtnClicked() -> void;
		auto OnDownloadBtnClicked() -> void;
		auto OnAssigneeBtnClicked() -> void;
		auto OnReviewerBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
