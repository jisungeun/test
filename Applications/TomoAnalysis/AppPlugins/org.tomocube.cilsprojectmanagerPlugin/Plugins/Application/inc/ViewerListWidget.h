#pragma once

#include <QWidget>

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API ViewerListWidget : public QWidget {
	public:
		ViewerListWidget(QWidget* parent = nullptr);
		~ViewerListWidget() override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}