#pragma once

#include <QWidget>
#include <QTreeWidgetItem>

#include "CilsExecution.h"

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API TreeViewWidget : public QWidget {
		Q_OBJECT

	public:
		TreeViewWidget(QWidget* parent = nullptr);
		~TreeViewWidget() override;

		auto GetProjectName() const -> QString;
		auto SetProjectName(const QString& name) -> void;

		auto SetExecutions(const QVector<Entity::CilsExecution>& executions) -> void;

		auto Select(int executionId) -> void;

	signals:
		auto ExecutionSelected(QWidget* sender, int executionId) -> void;

	protected slots:
		auto OnContextLoaded(const QPoint& pos) -> void;
		auto OnCurrentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;
		auto OnItemSelectionChanged() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}