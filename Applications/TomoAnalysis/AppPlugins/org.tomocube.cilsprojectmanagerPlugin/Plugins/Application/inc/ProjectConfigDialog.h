#pragma once

#include <QDialog>

#include "AppViewer.h"
#include "CilsExecution.h"
#include "CilsProject.h"

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API ProjectConfigDialog final : public QDialog, public Interactor::AppViewer {
		Q_OBJECT

	public:
		explicit ProjectConfigDialog(QWidget* parent = nullptr);
		~ProjectConfigDialog() override;

	protected:
		auto OnProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void override;
		auto OnUserLoaded(const std::optional<Entity::CilsUser>& user) -> void override;
		auto OnNotConnected() -> void override;

	protected slots:
		auto OnInitializeBtnClicked() -> void;
		auto OnReloadBtnClicked() -> void;
		auto OnOutputBtnClicked() -> void;
		auto OnItemSelectionChanged() -> void;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d{};
	};
}
