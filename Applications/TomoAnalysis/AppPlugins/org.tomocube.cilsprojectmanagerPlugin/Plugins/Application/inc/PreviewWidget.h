#pragma once

#include <QWidget>

#include "CilsExecution.h"
#include "GalleryContent.h"

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API PreviewWidget final : public QWidget{
		Q_OBJECT

	public:
		PreviewWidget(QWidget * parent = nullptr);
		~PreviewWidget() override;

		auto SetExecutions(const QVector<Entity::CilsExecution>& executions) -> void;

		auto UpdateExecutionPreview(const QString& dataId, const QByteArray& image) -> void;
		auto GetNonPreviewDataId() const ->QString;

		auto Select(int executionId) -> void;

	protected slots:
		auto OnSelected(int index, QMouseEvent* event) -> void;
		auto OnHovered(int index, bool hovered) -> void;

	signals:
		auto ExecutionSelected(QWidget* sender, int executionId) -> void;

	protected:
		void wheelEvent(QWheelEvent* event) override;
		void keyPressEvent(QKeyEvent* event) override;
		void keyReleaseEvent(QKeyEvent* event) override;
		auto eventFilter(QObject* watched, QEvent* event) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
