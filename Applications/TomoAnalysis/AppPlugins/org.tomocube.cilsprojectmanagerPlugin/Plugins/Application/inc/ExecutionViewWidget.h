#pragma once

#include <QWidget>

#include "CilsExecution.h"

#include "CpmApplicationExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmApplication_API ExecutionViewWidget : public QWidget {
	public:
		ExecutionViewWidget(QWidget* parent = nullptr);
		~ExecutionViewWidget() override;
		
		auto SetExecution(const Entity::CilsExecution* execution)->void;
		auto Clear() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}