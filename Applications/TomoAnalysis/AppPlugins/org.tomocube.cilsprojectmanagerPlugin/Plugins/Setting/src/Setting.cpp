#include <QSettings>

#include "Setting.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct Setting::Impl {
		std::unique_ptr<QSettings> settings;
	};

	Setting::Setting() : SettingViewer(), d(new Impl) {
		d->settings = std::make_unique<QSettings>("Tomocube", "TomoAnalysis");
	}

	Setting::~Setting() = default;

	auto Setting::OnOutputPathRequested() -> QString {
		const auto path = d->settings->value("OutputPath");
		return path.toString();
	}

	auto Setting::OnOutputPathChanged(const QString& path) -> void {
		d->settings->setValue("OutputPath", path);
	}
}