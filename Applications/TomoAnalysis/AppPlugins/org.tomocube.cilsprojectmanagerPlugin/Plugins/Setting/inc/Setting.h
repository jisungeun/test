#pragma once

#include <memory>

#include "SettingViewer.h"

#include "CpmSettingExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmSetting_API Setting final : public Interactor::SettingViewer {
	public:
		Setting();
		~Setting() override;

		auto OnOutputPathRequested() -> QString override;
		auto OnOutputPathChanged(const QString& path) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}