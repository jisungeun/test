#include "InstanceContainer.h"

#include "DependencyInjector.h"

#include "Application.h"
#include "AppPresenter.h"
#include "AppUseCase.h"
#include "AppViewer.h"
#include "CilsPresenter.h"
#include "CilsProjectClient.h"
#include "CilsProjectUpdater.h"
#include "CilsUpdaterViewer.h"
#include "CilsUseCase.h"
#include "IAppInputPort.h"
#include "IAppOutputPort.h"
#include "ICilsOutputPort.h"
#include "IProjectConfigInputPort.h"
#include "ISettingInputPort.h"
#include "PackageManager.h"
#include "PackagePresenter.h"
#include "PackageUseCase.h"
#include "PackageViewer.h"
#include "ProjectConfig.h"
#include "ProjectConfigDialog.h"
#include "ProjectConfigPresenter.h"
#include "ProjectConfigUseCase.h"
#include "Setting.h"
#include "SettingPresenter.h"
#include "SettingUseCase.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	auto DependencyInjector::GetDependentInstance(const std::type_index& type, int& index, const QString* key) -> Framework::IInjectable* {
		BEGIN;

		// Input Ports
		ADD_DEPENDENCY(IOPort::IAppInputPort, UseCase::AppUseCase);
		ADD_DEPENDENCY(IOPort::ICilsInputPort, UseCase::CilsUseCase);
		ADD_DEPENDENCY(IOPort::IPackageInputPort, UseCase::PackageUseCase);
		ADD_DEPENDENCY(IOPort::IProjectConfigInputPort, UseCase::ProjectConfigUseCase);
		ADD_DEPENDENCY(IOPort::ISettingInputPort, UseCase::SettingUseCase);

		// Output Ports
		ADD_DEPENDENCY(IOPort::IAppOutputPort, Interactor::AppPresenter);
		ADD_DEPENDENCY(IOPort::ICilsOutputPort, Interactor::CilsPresenter);
		ADD_DEPENDENCY(IOPort::IPackageOutputPort, Interactor::PackagePresenter);
		ADD_DEPENDENCY(IOPort::IProjectConfigOutputPort, Interactor::ProjectConfigPresenter);
		ADD_DEPENDENCY(IOPort::ISettingOutputPort, Interactor::SettingPresenter);

		// Plugins
		ADD_DEPENDENCY(Interactor::AppViewer, Plugins::Application);
		ADD_DEPENDENCY(Interactor::AppViewer, Plugins::ProjectConfigDialog);
		ADD_DEPENDENCY(Interactor::CilsViewer, Plugins::CilsProjectClient);
		ADD_DEPENDENCY(Interactor::CilsUpdaterViewer, Plugins::CilsProjectUpdater);
		ADD_DEPENDENCY(Interactor::PackageViewer, Plugins::PackageManager);
		ADD_DEPENDENCY(Interactor::ProjectConfigViewer, Plugins::ProjectConfig);
		ADD_DEPENDENCY(Interactor::SettingViewer, Plugins::Setting);

		END;
	}
}
