#pragma once

#include <memory>

#include "IDependencyInjector.h"

#include "CpmDependencyInjectorExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmDependencyInjector_API DependencyInjector final : public Framework::IDependencyInjector {
    public:
        auto GetDependentInstance(const std::type_index& type, int& index, const QString* key) -> Framework::IInjectable* override;
    };
}
