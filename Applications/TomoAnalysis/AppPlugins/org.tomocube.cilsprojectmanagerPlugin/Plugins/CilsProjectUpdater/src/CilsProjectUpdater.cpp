#include <QTimer>

#include "CilsProjectUpdater.h"

#include "CilsController.h"
#include "ControllerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct CilsProjectUpdater::Impl {
		Framework::ControllerHandler<Interactor::CilsController> controller;

		QTimer timer;
	};

	CilsProjectUpdater::CilsProjectUpdater(QObject* parent) : QObject(parent), CilsUpdaterViewer(), d(new Impl) {
		connect(&d->timer, &QTimer::timeout, this, &CilsProjectUpdater::OnTimeOut);

		d->timer.setInterval(2000);
	}

	CilsProjectUpdater::~CilsProjectUpdater() = default;

	auto CilsProjectUpdater::OnUpdateExecutionsStarted() -> void {
		d->timer.start();
	}

	auto CilsProjectUpdater::OnUpdateExecutionsStopped() -> void {
		d->timer.stop();
	}

	auto CilsProjectUpdater::OnUpdateIntervalChanged(int interval) -> void {
		d->timer.setInterval(interval);
	}
	
	auto CilsProjectUpdater::OnTimeOut() -> void {
		d->controller->UpdateExecutions();
	}
}
