#pragma once

#include <memory>

#include <QObject>

#include "CilsUpdaterViewer.h"

#include "CpmCilsProjectUpdaterExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmCilsProjectUpdater_API CilsProjectUpdater final : public QObject, public Interactor::CilsUpdaterViewer {
	public:
		CilsProjectUpdater(QObject * parent = nullptr);
		~CilsProjectUpdater() override;

		auto OnUpdateExecutionsStarted() -> void override;
		auto OnUpdateExecutionsStopped() -> void override;
		auto OnUpdateIntervalChanged(int interval) -> void override;

	protected slots:
		auto OnTimeOut()->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d{};
	};
}
