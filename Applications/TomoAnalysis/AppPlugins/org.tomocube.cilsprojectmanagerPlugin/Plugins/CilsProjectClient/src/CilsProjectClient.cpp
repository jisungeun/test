#include <QVector>
#include <QNetworkAccessManager>
#include <QObject>
#include <utility>
#include <QDebug>

#include "CilsClient.h"
#include "CilsProjectClient.h"

#include <QFileInfo>

#include "CilsController.h"
#include "ControllerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct CilsProjectClient::Impl {
		CilsClient client;

		Framework::ControllerHandler<Interactor::CilsController> controller;

		std::optional<User> user;
		QVector<Project> projects;
		QMap<int, QVector<ItemExecution>> itemExecutions;
		QMap<int, QVector<OnExecution>> onExecutions;
		QMap<int, QVector<SyncExecution>> syncExecutions;
		int recentProjectId = -1;

		bool executionReq = false;
		bool previewReq = false;
	};

	CilsProjectClient::CilsProjectClient(QObject* parent) : QObject(parent), CilsViewer(), d(new Impl) {}

	CilsProjectClient::~CilsProjectClient() = default;

	auto CilsProjectClient::OnLoadProjectsRequested() -> void {
		auto* response = d->client.GetProjects();

		connect(response, &IResponse::Initialized, this,
			[this, response] {
				OnProjectsLoaded(response);
			}
		);
	}

	auto CilsProjectClient::OnLoadUserRequested() -> void {
		auto* response = d->client.GetLoginInfo();

		connect(response, &IResponse::Initialized, this,
			[this, response] {
				OnUserLoaded(response);
			}
		);
	}

	auto CilsProjectClient::OnLoadExecutionsRequested(int projectId) -> void {
		if (!d->executionReq) {
			LoadExecutionsRecursive(projectId, {}, 1);
		}
	}

	auto CilsProjectClient::OnProjectsRequested(QVector<Project>* projects) -> void {
		*projects = d->projects;
	}

	auto CilsProjectClient::OnUserRequested(std::optional<User>* user) -> void {
		*user = d->user;
	}

	auto CilsProjectClient::OnExecutionsRequested(int projectId, QVector<ItemExecution>* itemExecutions, QVector<OnExecution>* onExecutions, QVector<SyncExecution>* itemSync) -> void {
		*itemExecutions = d->itemExecutions[projectId];
		*onExecutions = d->onExecutions[projectId];
		*itemSync = d->syncExecutions[projectId];
	}

	auto CilsProjectClient::OnStartExecutionRequested(int projectId, int executionId, UserType userType) -> void {
		const auto* response = d->client.StartExecution(executionId, userType);

		connect(response, &RequestResponse::Initialized, this,
			[this, response, projectId, executionId, userType] {
				OnStartExecutionLoaded(response, projectId, executionId, userType);
			}
		);
	}

	auto CilsProjectClient::OnDownloadExecutionRequested(int projectId, int executionId, UserType userType) -> void {
		auto* response = d->client.PrepareExecution(projectId, executionId, userType, true);

		connect(response, &RequestResponse::Initialized,
			[this, response, executionId, projectId, userType] {
				OnDownloadExecutionLoaded(response, projectId, executionId, userType);
			}
		);
	}

	auto CilsProjectClient::OnDownloadPreviewRequested(const QString& dataId) -> void {
		if (!d->previewReq) {
			d->previewReq = true;
			auto* response = d->client.GetItemPreview(dataId);

			connect(response, &ImageResponse::Initialized, this,
				[response, this, dataId] {
					OnPreviewDownloaded(response, dataId);
				}
			);
		}
	}

	auto CilsProjectClient::OnStartExecutionLoaded(const RequestResponse* response, int projectId, int executionId, UserType userType) -> void {
		QString error;

		if (response->IsError())
			error = response->GetError()->GetMessage();

		if (error == "NO_ITEM_FOUND") {
			auto* response = d->client.PrepareExecution(projectId, executionId, userType, true);

			connect(response, &RequestResponse::Initialized,
				[this, response, projectId, executionId, userType] {
					if (!response->IsError()) {
						const auto* response = d->client.StartExecution(executionId, userType);

						connect(response, &RequestResponse::Initialized, this,
							[this, response, projectId, executionId, userType] {
								QString error;

								if (response->IsError())
									error = response->GetError()->GetMessage();

								d->controller->StartExecutionResponded(executionId, userType, error);
								delete response;
							}
						);
					} else {
						d->controller->StartExecutionResponded(executionId, userType, "NONE_ITEM_FOUND");
					}
					delete response;
				}
			);
		} else {
			d->controller->StartExecutionResponded(executionId, userType, error);
		}

		delete response;
	}

	auto CilsProjectClient::OnDownloadExecutionLoaded(const RequestResponse* response, int projectId, int executionId, UserType userType) -> void {
		QString error;

		if (response->IsError())
			error = response->GetError()->GetMessage();

		d->controller->DownloadExecutionResponded(projectId, executionId, userType, error);

		delete response;
	}

	auto CilsProjectClient::OnUserLoaded(const SingleResponse<User>* response) const -> void {
		if (!response->IsError()) {
			d->user = response->GetValue();
			d->controller->UserLoaded(d->user);
		} else {
			d->controller->NotConnected();
		}

		delete response;
	}

	auto CilsProjectClient::OnProjectsLoaded(const ListResponse<Project>* response) -> void {
		if (!response->IsError()) {
			d->projects = response->GetList();
			d->controller->ProjectsLoaded(d->projects);
		} else {
			d->controller->NotConnected();
		}

		delete response;
	}

	auto CilsProjectClient::OnExecutionsLoaded(const ListResponse<ItemExecution>* response, int projectId, QVector<ItemExecution> itemExecutions, int page) -> void {
		if (!response->IsError()) {
			const auto& executions = response->GetList();

			for (const auto& ex : executions)
				itemExecutions.push_back(ex);

			if (page >= response->GetPageLength()) {
				d->itemExecutions[projectId] = itemExecutions;
				LoadExecutionsDetail(projectId, itemExecutions);
			} else {
				LoadExecutionsRecursive(projectId, itemExecutions, ++page);
			}
		} else {
			d->controller->NotConnected();
		}

		delete response;
	}

	auto CilsProjectClient::OnExecutionsDetailLoaded(const ArrayResponse<OnExecution>* response, int projectId, QVector<ItemExecution> itemExecutions) -> void {
		if (!response->IsError()) {
			d->onExecutions[projectId] = response->GetArray();

			LoadExecutionsFileStatusRecursive(projectId, std::move(itemExecutions), d->onExecutions[projectId], {}, 1);
		} else {
			d->controller->NotConnected();
		}

		delete response;
	}

	auto CilsProjectClient::OnExecutionsFileStatusLoaded(const ListResponse<SyncExecution>* response, int projectId, QVector<ItemExecution> itemExecutions, QVector<OnExecution> onExecutions, QVector<SyncExecution> syncExecutions, int page) -> void {
		if (!response->IsError()) {
			const auto& executions = response->GetList();

			for (const auto& ex : executions)
				syncExecutions.push_back(ex);

			if (page >= response->GetPageLength()) {
				d->executionReq = false;
				d->syncExecutions[projectId] = syncExecutions;
				d->controller->ExecutionsLoaded(projectId, itemExecutions, onExecutions, syncExecutions);
			} else {
				LoadExecutionsFileStatusRecursive(projectId, std::move(itemExecutions), onExecutions, syncExecutions, ++page);
			}
		} else {
			d->controller->NotConnected();
		}

		delete response;
	}


	auto CilsProjectClient::OnPreviewDownloaded(const ImageResponse* response, const QString& dataId) -> void {
		d->previewReq = false;

		if (!response->IsError())
			d->controller->DownloadPreviewResponded(dataId, response->GetImage());
		
		delete response;
	}

	auto CilsProjectClient::LoadExecutionsRecursive(int projectId, QVector<ItemExecution> itemExecutions, int page) -> void {
		d->executionReq = true;
		auto* response = d->client.GetItemExecutions(projectId, {}, {}, {}, {}, {}, page);

		connect(response, &IResponse::Initialized,
			[this, response, projectId, itemExecutions, page]() mutable {
				OnExecutionsLoaded(response, projectId, itemExecutions, page);
			}
		);
	}

	auto CilsProjectClient::LoadExecutionsDetail(int projectId, QVector<ItemExecution> itemExecutions) -> void {
		auto* response = d->client.GetOnExecution(projectId);

		connect(response, &IResponse::Initialized,
			[this, projectId, response, itemExecutions] {
				OnExecutionsDetailLoaded(response, projectId, itemExecutions);
			}
		);
	}

	auto CilsProjectClient::LoadExecutionsFileStatusRecursive(int projectId, QVector<ItemExecution> itemExecutions, QVector<OnExecution> onExecutions, QVector<SyncExecution> syncExecutions, int page) -> void {
		auto response = d->client.GetDownloadingItems();

		connect(response, &IResponse::Initialized,
			[this, response, projectId, onExecutions, itemExecutions, syncExecutions, page]() mutable {
				OnExecutionsFileStatusLoaded(response, projectId, itemExecutions, onExecutions, syncExecutions, page);
			}
		);
	}
}
