#pragma once

#include <memory>

#include <QObject>

#include "ArrayResponse.h"
#include "SingleResponse.h"
#include "ListResponse.h"
#include "ImageResponse.h"
#include "RequestResponse.h"

#include "Project.h"
#include "User.h"
#include "ItemExecution.h"
#include "OnExecution.h"
#include "SyncExecution.h"

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "CilsViewer.h"

#include "CpmCilsProjectClientExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	using namespace TC::Cils::Client;

	class CpmCilsProjectClient_API CilsProjectClient final : public QObject, public Interactor::CilsViewer{
	public:
		CilsProjectClient(QObject * parent = nullptr);
		~CilsProjectClient() override;

		auto OnLoadProjectsRequested() -> void override;
		auto OnLoadUserRequested() -> void override;
		auto OnLoadExecutionsRequested(int projectId) -> void override;

		auto OnProjectsRequested(QVector<Project>* projects) -> void override;
		auto OnUserRequested(std::optional<User>* user) -> void override;
		auto OnExecutionsRequested(int projectId, QVector<ItemExecution>* itemExecutions, QVector<OnExecution>* onExecutions, QVector<SyncExecution>* itemSync) -> void override;

		auto OnStartExecutionRequested(int projectId, int executionId, UserType userType) -> void override;
		auto OnDownloadExecutionRequested(int projectId, int executionId, UserType userType) -> void override;

		auto OnDownloadPreviewRequested(const QString& dataId) -> void override;

	protected:
		auto OnStartExecutionLoaded(const RequestResponse* response, int projectId, int executionId, UserType userType) -> void;
		auto OnDownloadExecutionLoaded(const RequestResponse* response, int projectId, int executionId, UserType userType) -> void;

		auto OnUserLoaded(const SingleResponse<User>* response) const -> void;
		auto OnProjectsLoaded(const ListResponse<Project>* response) -> void;
		auto OnExecutionsLoaded(const ListResponse<ItemExecution>* response, int projectId, QVector<ItemExecution> itemExecutions, int page) -> void;
		auto OnExecutionsDetailLoaded(const ArrayResponse<OnExecution>* response, int projectId, QVector<ItemExecution> itemExecutions) -> void;
		auto OnExecutionsFileStatusLoaded(const ListResponse<SyncExecution>* response, int projectId, QVector<ItemExecution> itemExecutions, QVector<OnExecution> onExecutions, QVector<SyncExecution> syncExecutions, int page) -> void;

		auto OnPreviewDownloaded(const ImageResponse* response, const QString& dataId) -> void;

	private:
		auto LoadExecutionsRecursive(int projectId, QVector<ItemExecution> itemExecutions, int page) -> void;
		auto LoadExecutionsDetail(int projectId, QVector<ItemExecution> itemExecutions) -> void;
		auto LoadExecutionsFileStatusRecursive(int projectId, QVector<ItemExecution> itemExecutions, QVector<OnExecution> onExecutions, QVector<SyncExecution> syncExecutions, int page) -> void;

		struct Impl;
		std::unique_ptr<Impl> d{};
	};
}
