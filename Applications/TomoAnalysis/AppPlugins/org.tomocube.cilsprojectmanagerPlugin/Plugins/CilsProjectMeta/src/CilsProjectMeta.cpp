#include "CilsProjectMeta.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "CILS Project Manager";
		d->appProperties["Symbol"] = "org.tomocube.cilsprojectmanagerPlugin";
		d->appProperties["ExecutionSymbol"] = "org.tomocube.cilsexecutionPlugin";
		d->appProperties["ExecutionName"] = "CILS Executor";
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}