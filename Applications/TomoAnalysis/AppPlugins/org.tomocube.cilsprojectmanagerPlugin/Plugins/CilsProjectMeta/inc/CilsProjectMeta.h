#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "CilsProjectMetaExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CilsProjectMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.cilsprojectmanagerMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "CILS Project Manager"; }
		auto GetFullName()const->QString override { return "org.tomocube.cilsprojectmanagerPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}