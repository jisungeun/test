#pragma once

#include <memory>

#include "ProjectConfigViewer.h"

#include "CpmProjectConfigExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmProjectConfig_API ProjectConfig final : public Interactor::ProjectConfigViewer {
	public:
		ProjectConfig();
		~ProjectConfig() override;

		auto OnCurrentProjectChanged(const std::optional<Entity::CilsProject>& project) -> void override;
		auto OnCurrentUserChanged(const std::optional<Entity::CilsUser>& user) -> void override;
		auto OnCurrentPackageChanged(const QString& app) -> void override;

		auto OnCurrentProjectRequested() -> const std::optional<Entity::CilsProject>& override;
		auto OnCurrentUserRequested() -> const std::optional<Entity::CilsUser>& override;
		auto OnCurrentPackageRequested() -> const QString& override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
