#include "ProjectConfig.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct ProjectConfig::Impl {
		std::optional<Entity::CilsProject> project;
		std::optional<Entity::CilsUser> user;
		QString app;
	};

	ProjectConfig::ProjectConfig() : ProjectConfigViewer(), d(new Impl) {}

	ProjectConfig::~ProjectConfig() = default;

	auto ProjectConfig::OnCurrentProjectChanged(const std::optional<Entity::CilsProject>& project) -> void {
		d->project = project;
	}

	auto ProjectConfig::OnCurrentUserChanged(const std::optional<Entity::CilsUser>& user) -> void {
		d->user = user;
	}

	auto ProjectConfig::OnCurrentPackageChanged(const QString& app) -> void {
		d->app = app;
	}

	auto ProjectConfig::OnCurrentProjectRequested() -> const std::optional<Entity::CilsProject>& {
		return d->project;
	}

	auto ProjectConfig::OnCurrentUserRequested() -> const std::optional<Entity::CilsUser>& {
		return d->user;
	}

	auto ProjectConfig::OnCurrentPackageRequested() -> const QString& {
		return d->app;
	}
}
