#pragma once

#include "CpmPackageExport.h"
#include "PackageViewer.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmPackage_API PackageManager : public Interactor::PackageViewer {
	public:
		PackageManager();
		~PackageManager() override;

		auto GetPackageNames() -> QStringList override;
		auto GetAppSymbol(const QString& packageName) -> QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
