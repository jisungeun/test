#pragma once

#include <memory>

#include "IPackage.h"

#include "CpmPackageExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmPackage_API BasicAnalyzer : public IPackage {
	public:
		BasicAnalyzer();
		~BasicAnalyzer() override;

		auto GetPackageName() -> QString override;
		auto GetAppSymbol() -> QString override;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}