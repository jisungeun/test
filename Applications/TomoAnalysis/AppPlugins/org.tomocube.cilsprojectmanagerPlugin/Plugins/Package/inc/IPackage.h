#pragma once

#include <QString>

#include "CpmPackageExport.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	class CpmPackage_API IPackage {
	public:
		virtual ~IPackage() = default;

		virtual auto GetPackageName()->QString = 0;
		virtual auto GetAppSymbol()->QString = 0;
	};
}