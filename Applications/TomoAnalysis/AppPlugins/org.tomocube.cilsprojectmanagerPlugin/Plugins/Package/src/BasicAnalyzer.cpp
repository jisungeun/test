#include "BasicAnalyzer.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct BasicAnalyzer::Impl {
		QString packageName;
		QString symbol;
	};

	BasicAnalyzer::BasicAnalyzer() : IPackage(), d(new Impl) {
		d->packageName = "Basic Analyzer";
		d->symbol = "org.tomocube.basicanalysisPlugin";
	}

	BasicAnalyzer::~BasicAnalyzer() = default;

	auto BasicAnalyzer::GetPackageName() -> QString {
		return d->packageName;
	}

	auto BasicAnalyzer::GetAppSymbol() -> QString {
		return d->symbol;
	}
}
