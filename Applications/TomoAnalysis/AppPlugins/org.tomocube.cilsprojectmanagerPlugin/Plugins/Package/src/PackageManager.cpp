#include "PackageManager.h"
#include "BasicAnalyzer.h"
#include "MaskEditor.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct PackageManager::Impl {
		QVector<std::shared_ptr<IPackage>> packages;
	};

	PackageManager::PackageManager() : PackageViewer(), d(new Impl) {
		d->packages << std::make_shared<BasicAnalyzer>();
		d->packages << std::make_shared<MaskEditor>();
	}

	PackageManager::~PackageManager() = default;

	auto PackageManager::GetPackageNames() -> QStringList {
		QStringList list;

		for (const auto& ptr : d->packages) {
			list << ptr->GetPackageName();
		}

		return list;
	}

	auto PackageManager::GetAppSymbol(const QString& packageName) -> QString {
		for (const auto& ptr : d->packages) {
			if (ptr->GetPackageName() == packageName)
				return ptr->GetAppSymbol();
		}

		return {};
	}
}