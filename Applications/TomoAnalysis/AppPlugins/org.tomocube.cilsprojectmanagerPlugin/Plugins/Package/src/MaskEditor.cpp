#include "MaskEditor.h"

namespace TomoAnalysis::CilsProjectManager::Plugins {
	struct MaskEditor::Impl {
		QString packageName;
		QString symbol;
	};

	MaskEditor::MaskEditor() : IPackage(), d(new Impl) {
		d->packageName = "Mask Editor";
		d->symbol = "org.tomocube.intersegPlugin";
	}

	MaskEditor::~MaskEditor() = default;

	auto MaskEditor::GetPackageName() -> QString {
		return d->packageName;
	}

	auto MaskEditor::GetAppSymbol() -> QString {
		return d->symbol;
	}
}
