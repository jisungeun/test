#pragma once

#include <IAppActivator.h>

#define SYMBOL "org.tomocube.cilsprojectmanagerPlugin"

namespace TomoAnalysis::CilsProjectManager::AppUI {
	class cilsprojectmanagerPlugin : public TC::Framework::IAppActivator {
		Q_OBJECT
			Q_PLUGIN_METADATA(IID SYMBOL)
	public:
		static cilsprojectmanagerPlugin* getInstance();

		cilsprojectmanagerPlugin();
		~cilsprojectmanagerPlugin();

		auto start(ctkPluginContext* context)->void override;
		auto stop(ctkPluginContext* context)->void override;
		auto getPluginContext()->ctkPluginContext* const override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
		static cilsprojectmanagerPlugin* instance;
	};
}