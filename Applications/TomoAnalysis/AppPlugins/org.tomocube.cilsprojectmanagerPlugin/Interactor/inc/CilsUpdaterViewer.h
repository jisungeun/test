#pragma once

#include <optional>

#include "IViewer.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API CilsUpdaterViewer : public Framework::IViewer {
	public:
		virtual auto OnUpdateExecutionsStarted() -> void = 0;
		virtual auto OnUpdateExecutionsStopped() -> void = 0;
		virtual auto OnUpdateIntervalChanged(int interval) -> void = 0;
	};
}
