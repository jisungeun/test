#pragma once

#include <memory>

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API ProjectConfigController {
	public:
		ProjectConfigController();
		~ProjectConfigController();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}