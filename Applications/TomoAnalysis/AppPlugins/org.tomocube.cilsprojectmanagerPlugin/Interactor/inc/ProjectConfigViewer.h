#pragma once

#include <optional>

#include "IViewer.h"

#include "CilsProject.h"
#include "CilsUser.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API ProjectConfigViewer : public Framework::IViewer {
	public:
		ProjectConfigViewer();
		~ProjectConfigViewer() override;

		virtual auto OnCurrentProjectChanged(const std::optional<Entity::CilsProject>& project) -> void = 0;
		virtual auto OnCurrentUserChanged(const std::optional<Entity::CilsUser>& user) -> void = 0;
		virtual auto OnCurrentPackageChanged(const QString& packageName) -> void = 0;

		virtual auto OnCurrentProjectRequested()->const std::optional<Entity::CilsProject> & = 0;
		virtual auto OnCurrentUserRequested()->const std::optional<Entity::CilsUser> & = 0;
		virtual auto OnCurrentPackageRequested()->const QString & = 0;
	};
}
