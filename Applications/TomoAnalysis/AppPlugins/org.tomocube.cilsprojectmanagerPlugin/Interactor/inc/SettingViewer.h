#pragma once

#include <QString>

#include "IViewer.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API SettingViewer : public Framework::IViewer {
	public:
		SettingViewer();
		~SettingViewer() override;

		virtual auto OnOutputPathRequested()->QString = 0;

		virtual auto OnOutputPathChanged(const QString& path) -> void = 0;
	};
}
