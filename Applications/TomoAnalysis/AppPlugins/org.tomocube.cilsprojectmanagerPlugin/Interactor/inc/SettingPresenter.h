#pragma once

#include "ISettingOutputPort.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API SettingPresenter : public IOPort::ISettingOutputPort {
	public:
		SettingPresenter();
		~SettingPresenter() override;

		auto GetOutputPath() -> QString override;
		auto SetOutputPath(const QString& path) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
