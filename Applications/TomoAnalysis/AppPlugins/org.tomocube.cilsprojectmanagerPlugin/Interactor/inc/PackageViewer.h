#pragma once

#include <QStringList>

#include "IViewer.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API PackageViewer : public Framework::IViewer {
	public:
		PackageViewer();
		~PackageViewer() override;

		virtual auto GetPackageNames()->QStringList = 0;
		virtual auto GetAppSymbol(const QString& packageName)->QString = 0;
	};
}
