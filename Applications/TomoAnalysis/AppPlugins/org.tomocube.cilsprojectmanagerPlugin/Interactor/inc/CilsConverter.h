#pragma once

#include <QVector>

#include "ItemExecution.h"
#include "OnExecution.h"
#include "SyncExecution.h"

#include "CilsExecution.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CilsConverter {
	public:
		static auto ToExecutions(int userId, const QVector<TC::Cils::JsonEntity::ItemExecution>& itemExecutions, const QVector<TC::Cils::JsonEntity::OnExecution>& onExecutions, const QVector<TC::Cils::JsonEntity::SyncExecution>& syncExecutions) ->QVector<Entity::CilsExecution>;
	};
}