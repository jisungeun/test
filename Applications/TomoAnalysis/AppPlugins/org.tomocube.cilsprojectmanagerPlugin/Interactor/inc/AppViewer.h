#pragma once

#include "IViewer.h"

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API AppViewer : public Framework::IViewer {
	public:
		AppViewer();
		~AppViewer() override;
		
		virtual auto OnProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void;
		virtual auto OnExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void;
		virtual auto OnUserLoaded(const std::optional<Entity::CilsUser>& user) -> void;

		virtual auto OnExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void;
		virtual auto OnDownloadStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void;

		virtual auto OnPreviewDownloaded(const QString& dataId, const QByteArray& image) -> void;
		virtual auto OnNotConnected() -> void;
	};
}
