#pragma once

#include <memory>

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API SettingController {
	public:
		SettingController();
		~SettingController();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}