#pragma once

#include <QObject>

#include "IProjectConfigOutputPort.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API ProjectConfigPresenter final : public IOPort::IProjectConfigOutputPort{
	public:
		ProjectConfigPresenter();
		~ProjectConfigPresenter() override;

		auto SetCurrentProject(const std::optional<Entity::CilsProject>& project) -> void override;
		auto SetCurrentUser(const std::optional<Entity::CilsUser>& user) -> void override;
		auto SetCurrentPackage(const QString& packageName) -> void override;

		auto GetCurrentProject()->std::optional<Entity::CilsProject> override;
		auto GetCurrentUser()->std::optional<Entity::CilsUser> override;
		auto GetCurrentPackage() -> QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
