#pragma once

#include <memory>
#include <optional>

#include <QString>
#include <QVector>
#include <QByteArray>

#include "CilsClient.h"
#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"

#include "Project.h"
#include "User.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API CilsController {
	public:
		CilsController();
		~CilsController();

		auto ProjectsLoaded(const QVector<TC::Cils::JsonEntity::Project>& projects) -> void;
		auto UserLoaded(const std::optional<TC::Cils::JsonEntity::User>& user) -> void;
		auto ExecutionsLoaded(int projectId, const QVector<TC::Cils::JsonEntity::ItemExecution>& itemExecutions, const QVector<TC::Cils::JsonEntity::OnExecution>& onExecutions, const QVector<TC::Cils::JsonEntity::SyncExecution>& syncExecutions) -> void;

		auto StartExecutionResponded(int executionId, TC::Cils::Client::UserType userType, const QString& result) -> void;
		auto DownloadExecutionResponded(int projectId, int executionId, TC::Cils::Client::UserType userType, const QString& result) -> void;

		auto DownloadPreviewResponded(const QString& dataId, const QByteArray& image) -> void;

		auto NotConnected() -> void;

		auto UpdateExecutions() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
