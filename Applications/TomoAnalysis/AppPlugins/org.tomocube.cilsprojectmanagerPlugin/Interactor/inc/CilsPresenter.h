#pragma once

#include <optional>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "ICilsOutputPort.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API CilsPresenter : public IOPort::ICilsOutputPort{
	public:
		CilsPresenter();
		~CilsPresenter() override;

		auto LoadProjects() -> void override;
		auto LoadUser() -> void override;
		auto LoadExecutions(int projectId) -> void override;

		auto GetProjects()->QVector<Entity::CilsProject> override;
		auto GetUser()->std::optional<Entity::CilsUser> override;
		auto GetExecutions(int projectId)->QVector<Entity::CilsExecution> override;

		auto StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void override;
		auto DownloadExecution(const Entity::CilsExecution& execution) -> void override;

		auto DownloadPreview(const QString& dataId) -> void override;

		auto StartUpdateExecutions() -> void override;
		auto StopUpdateExecutions() -> void override;
		auto SetUpdateInterval(int interval) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
