#pragma once

#include "IPackageOutputPort.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API PackagePresenter final : public IOPort::IPackageOutputPort {
	public:
		PackagePresenter();
		~PackagePresenter() override;

		auto GetPackageNames() -> QStringList override;
		auto GetAppSymbol(const QString& packageName) -> QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
