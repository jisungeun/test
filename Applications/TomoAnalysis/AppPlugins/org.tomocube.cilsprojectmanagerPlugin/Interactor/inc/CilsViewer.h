#pragma once

#include <optional>

#include "CilsClient.h"

#include "Project.h"
#include "User.h"
#include "ItemExecution.h"
#include "OnExecution.h"
#include "SyncExecution.h"
#include "IViewer.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API CilsViewer : public Framework::IViewer {
	public:
		CilsViewer();
		~CilsViewer() override;
		
		virtual auto OnLoadProjectsRequested() -> void;
		virtual auto OnLoadUserRequested() -> void;
		virtual auto OnLoadExecutionsRequested(int projectId) -> void;

		virtual auto OnProjectsRequested(QVector<TC::Cils::JsonEntity::Project>* projects) -> void;
		virtual auto OnUserRequested(std::optional<TC::Cils::JsonEntity::User>* user) -> void;
		virtual auto OnExecutionsRequested(int projectId,
			QVector<TC::Cils::JsonEntity::ItemExecution>* itemExecutions, 
			QVector<TC::Cils::JsonEntity::OnExecution>* onExecutions, 
			QVector<TC::Cils::JsonEntity::SyncExecution>* itemSync) -> void;

		virtual auto OnStartExecutionRequested(int projectId, int executionId, TC::Cils::Client::UserType userType) -> void;
		virtual auto OnDownloadExecutionRequested(int projectId, int executionId, TC::Cils::Client::UserType userType) -> void;

		virtual auto OnDownloadPreviewRequested(const QString& dataId) -> void;
	};
}
