#pragma once

#include <memory>

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API PackageController {
	public:
		PackageController();
		~PackageController();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}