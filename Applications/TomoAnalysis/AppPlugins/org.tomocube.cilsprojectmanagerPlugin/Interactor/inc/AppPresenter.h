#pragma once

#include <QObject>
#include <QVector>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "IAppOutputPort.h"

#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API AppPresenter : public IOPort::IAppOutputPort {
	public:
		AppPresenter();
		~AppPresenter() override;

		auto UpdateProjects(const QVector<Entity::CilsProject>& projects) -> void override;
		auto UpdateExecutions(int projectId, const QVector<Entity::CilsExecution>& executions) -> void override;
		auto UpdateUser(const std::optional<Entity::CilsUser>& user) -> void override;

		auto ExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void override;
		auto ExecutionDownloaded(const Entity::CilsExecution& execution, const QString& result) -> void override;

		auto PreviewDownloaded(const QString& dataId, const QByteArray& image) -> void override;
		auto NotConnected() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
