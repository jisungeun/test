#pragma once

#include <memory>
#include <optional>

#include <QVector>
#include <QString>

#include "CilsExecution.h"
#include "CilsProject.h"
#include "CilsUser.h"
#include "CpmInteractorExport.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	class CpmInteractor_API AppController {
	public:
		AppController();
		virtual ~AppController();

		// CILS Features
		auto LoadProjects() -> void;
		auto LoadUser() -> void;
		auto LoadExecutions() -> void;

		auto GetProjects()->QVector<Entity::CilsProject>;
		auto GetUser()->std::optional<Entity::CilsUser>;
		auto GetExecutions()->QVector<Entity::CilsExecution>;

		auto StartExecution(int id, Entity::UserType userType) -> void;
		auto DownloadExecution(int id) -> void;

		auto DownloadPreview(const QString& dataId) -> void;

		// Config Features
		auto SetCurrentProject(int projectId) -> void;
		auto SetCurrentUser() -> void;
		auto SetCurrentPackage(int index) -> void;

		auto GetCurrentProjectName()->QString;
		auto GetCurrentProjectId() -> int;
		auto GetCurrentUsername()->QString;
		auto GetCurrentPackage()->QString;

		// Package Features
		auto GetPackageNames()->QStringList;
		auto GetAppSymbol()->QString;

		// Setting Features
		auto SetOutputPath(const QString& path) -> void;
		auto GetOutputPath()->QString;

		auto GetOutputPath(int id, Entity::UserType userType) -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}