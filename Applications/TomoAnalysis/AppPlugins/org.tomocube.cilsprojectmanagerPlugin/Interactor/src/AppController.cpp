#include "AppController.h"

#include <QDir>

#include "IAppInputPort.h"
#include "InputPortHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct AppController::Impl {
		Framework::InputPortHandler<IOPort::IAppInputPort> handler;
	};

	AppController::AppController() : d(new Impl) {}

	AppController::~AppController() = default;

	auto AppController::LoadProjects() -> void {
		d->handler->LoadProjects();
	}

	auto AppController::LoadUser() -> void {
		d->handler->LoadUser();
	}

	auto AppController::LoadExecutions() -> void {
		const auto project = d->handler->GetCurrentProject();
		d->handler->LoadExecutions(project->GetId());
	}

	auto AppController::GetProjects() -> QVector<Entity::CilsProject> {
		return d->handler->GetProjects();
	}

	auto AppController::GetUser() -> std::optional<Entity::CilsUser> {
		return d->handler->GetUser();
	}

	auto AppController::GetExecutions() -> QVector<Entity::CilsExecution> {
		const auto project = d->handler->GetCurrentProject();
		return d->handler->GetExecutions(project->GetId());
	}

	auto AppController::StartExecution(int id, Entity::UserType userType) -> void {
		for (auto& ex : GetExecutions()) {
			if (ex.GetId() == id)
				d->handler->StartExecution(ex, userType);
		}
	}

	auto AppController::DownloadExecution(int id) -> void {
		for (auto& ex : GetExecutions()) {
			if (ex.GetId() == id) {
				d->handler->DownloadExecution(ex);
			}
		}
	}

	auto AppController::DownloadPreview(const QString& dataId) -> void {
		d->handler->DownloadPreview(dataId);
	}

	auto AppController::SetCurrentProject(int projectId) -> void {
		auto projects = d->handler->GetProjects();

		for (const auto& p : projects) {
			if (p.GetId() == projectId) {
				d->handler->SetCurrentProject(p);
				return;
			}
		}
	}

	auto AppController::SetCurrentUser() -> void {
		const auto user = d->handler->GetUser();
		d->handler->SetCurrentUser(user);
	}

	auto AppController::SetCurrentPackage(int index) -> void {
		auto packages = d->handler->GetPackageNames();
		d->handler->SetCurrentPackage(packages[index]);
	}

	auto AppController::GetCurrentProjectName() -> QString {
		if (const auto project = d->handler->GetCurrentProject(); project.has_value())
			return project->GetName();

		return {};
	}

	auto AppController::GetCurrentProjectId() -> int {
		if (const auto project = d->handler->GetCurrentProject(); project.has_value())
			return project->GetId();

		return -1;
	}

	auto AppController::GetCurrentUsername() -> QString {
		if (const auto& user = d->handler->GetCurrentUser(); user.has_value())
			return user->GetName();
		return {};
	}

	auto AppController::GetCurrentPackage() -> QString {
		return d->handler->GetCurrentPackage();
	}

	auto AppController::GetPackageNames() -> QStringList {
		return d->handler->GetPackageNames();
	}

	auto AppController::GetAppSymbol() -> QString {
		return d->handler->GetAppSymbol();
	}

	auto AppController::SetOutputPath(const QString& path) -> void {
		return d->handler->SetOutputPath(path);
	}

	auto AppController::GetOutputPath() -> QString {
		return d->handler->GetOutputPath();
	}

	auto AppController::GetOutputPath(int id, Entity::UserType userType) -> QString {
		for (auto& ex : GetExecutions()) {
			if (ex.GetId() == id) {
				switch (userType) {
				case Entity::UserType::Assignee:
					return QDir::toNativeSeparators(d->handler->GetAssigneeOutputPath(ex));
				case Entity::UserType::Reviewer:
					return QDir::toNativeSeparators(d->handler->GetReviewerOutputPath(ex));
				}
			}
		}

		return {};
	}
}
