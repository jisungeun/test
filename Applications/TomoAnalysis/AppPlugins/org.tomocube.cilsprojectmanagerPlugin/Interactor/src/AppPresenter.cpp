#include "AppPresenter.h"

#include "AppViewer.h"
#include "ViewerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct AppPresenter::Impl {
		Framework::ViewerHandler<AppViewer> handler;
	};

	AppPresenter::AppPresenter() : IAppOutputPort(), d(new Impl) {}

	AppPresenter::~AppPresenter() = default;

	auto AppPresenter::UpdateProjects(const QVector<Entity::CilsProject>& projects) -> void {
		d->handler([&](AppViewer* p) { p->OnProjectsLoaded(projects); });
	}

	auto AppPresenter::UpdateExecutions(int projectId, const QVector<Entity::CilsExecution>& executions) -> void {
		d->handler([&](AppViewer* p) { p->OnExecutionsLoaded(projectId, executions); });
	}

	auto AppPresenter::UpdateUser(const std::optional<Entity::CilsUser>& user) -> void {
		d->handler([&](AppViewer* p) { p->OnUserLoaded(user); });
	}

	auto AppPresenter::ExecutionDownloaded(const Entity::CilsExecution& execution, const QString& result) -> void {
		if (execution.GetCategory().testFlag(Entity::UserType::Assignee))
			d->handler([&](AppViewer* p) { p->OnDownloadStarted(execution, Entity::UserType::Assignee, result); });
		else if (execution.GetCategory().testFlag(Entity::UserType::Reviewer))
			d->handler([&](AppViewer* p) { p->OnDownloadStarted(execution, Entity::UserType::Reviewer, result); });
		else
			d->handler([&](AppViewer* p) { p->OnDownloadStarted(execution, Entity::UserType::None, result); });
	}

	auto AppPresenter::ExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {
		d->handler([&](AppViewer* p) { p->OnExecutionStarted(execution, userType, result); });
	}

	auto AppPresenter::PreviewDownloaded(const QString& dataId, const QByteArray& image) -> void {
		d->handler([&](AppViewer* p) { p->OnPreviewDownloaded(dataId, image); });
	}

	auto AppPresenter::NotConnected() -> void {
		d->handler([&](AppViewer* p) { p->OnNotConnected(); });
	}
}
