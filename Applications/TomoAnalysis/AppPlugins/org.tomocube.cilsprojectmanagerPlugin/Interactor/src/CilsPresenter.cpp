#include "CilsPresenter.h"

#include "CilsConverter.h"
#include "CilsUpdaterViewer.h"
#include "CilsViewer.h"
#include "ViewerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct CilsPresenter::Impl {
		Framework::ViewerHandler<CilsViewer> handler;
		Framework::ViewerHandler<CilsUpdaterViewer> updaterHandler;
	};

	CilsPresenter::CilsPresenter() : ICilsOutputPort(), d(new Impl) {}

	CilsPresenter::~CilsPresenter() = default;

	auto CilsPresenter::LoadProjects() -> void {
		d->handler([&](CilsViewer* p) { p->OnLoadProjectsRequested(); });
	}

	auto CilsPresenter::LoadUser() -> void {
		d->handler([&](CilsViewer* p) { p->OnLoadUserRequested(); });
	}

	auto CilsPresenter::LoadExecutions(int projectId) -> void {
		d->handler([&](CilsViewer* p) { p->OnLoadExecutionsRequested(projectId); });
	}

	auto CilsPresenter::GetProjects() -> QVector<Entity::CilsProject> {
		QVector<TC::Cils::JsonEntity::Project> result;
		d->handler([&](CilsViewer* p) { p->OnProjectsRequested(&result); });

		QVector<Entity::CilsProject> projects;

		for (const auto& r : result) {
			Entity::CilsProject p;

			p.SetId(r.GetId());
			p.SetName(r.GetName());
			p.SetType(r.GetType());

			projects.push_back(p);
		}

		return projects;
	}

	auto CilsPresenter::GetUser() -> std::optional<Entity::CilsUser> {
		std::optional<TC::Cils::JsonEntity::User> result;
		d->handler([&](CilsViewer* p) { p->OnUserRequested(&result); });

		if (result.has_value()) {
			Entity::CilsUser user;

			user.SetId(result->GetId());
			user.SetUsername(result->GetUsername().value());
			user.SetName(result->GetName().value());

			return user;
		}

		return {};
	}

	auto CilsPresenter::GetExecutions(int projectId) -> QVector<Entity::CilsExecution> {
		QVector<TC::Cils::JsonEntity::ItemExecution> itemExecutions;
		QVector<TC::Cils::JsonEntity::OnExecution> onExecutions;
		QVector<TC::Cils::JsonEntity::SyncExecution> syncExecutions;

		d->handler([&](CilsViewer* p) { p->OnExecutionsRequested(projectId, &itemExecutions, &onExecutions, &syncExecutions); });
		
		const auto user = GetUser();
		const auto executions = CilsConverter::ToExecutions(user->GetId(), itemExecutions, onExecutions, syncExecutions);

		return executions;
	}

	auto CilsPresenter::StartExecution(const Entity::CilsExecution& execution, Entity::UserType userType) -> void {
		switch (userType) {
		case Entity::UserType::None:
			break;
		case Entity::UserType::Assignee:
			d->handler([&](CilsViewer* p) { p->OnStartExecutionRequested(execution.GetProjectId(), execution.GetId(), TC::Cils::Client::UserType::Assignee); });
			break;
		case Entity::UserType::Reviewer:
			d->handler([&](CilsViewer* p) { p->OnStartExecutionRequested(execution.GetProjectId(), execution.GetId(), TC::Cils::Client::UserType::Reviewer); });
			break;
		}
	}

	auto CilsPresenter::DownloadExecution(const Entity::CilsExecution& execution) -> void {
		if (execution.GetCategory().testFlag(Entity::UserType::Assignee)) {
			d->handler([&](CilsViewer* p) { p->OnDownloadExecutionRequested(execution.GetProjectId(), execution.GetId(), TC::Cils::Client::UserType::Assignee); });
		} else if (execution.GetCategory().testFlag(Entity::UserType::Reviewer)) {
			d->handler([&](CilsViewer* p) { p->OnDownloadExecutionRequested(execution.GetProjectId(), execution.GetId(), TC::Cils::Client::UserType::Reviewer); });
		}
	}

	auto CilsPresenter::DownloadPreview(const QString& dataId) -> void {
		d->handler([&](CilsViewer* p) { p->OnDownloadPreviewRequested(dataId); });
	}

	auto CilsPresenter::StartUpdateExecutions() -> void {
		d->updaterHandler->OnUpdateExecutionsStarted();
	}

	auto CilsPresenter::StopUpdateExecutions() -> void {
		d->updaterHandler->OnUpdateExecutionsStopped();
	}

	auto CilsPresenter::SetUpdateInterval(int interval) -> void {
		d->updaterHandler->OnUpdateIntervalChanged(interval);
	}
}
