#include "PackageViewer.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	PackageViewer::PackageViewer() = default;

	PackageViewer::~PackageViewer() = default;
}
