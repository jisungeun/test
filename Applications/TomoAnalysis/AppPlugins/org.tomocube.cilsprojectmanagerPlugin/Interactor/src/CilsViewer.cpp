#include "CilsViewer.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	CilsViewer::CilsViewer() : IViewer() {}

	CilsViewer::~CilsViewer() = default;

	auto CilsViewer::OnLoadProjectsRequested() -> void {}

	auto CilsViewer::OnLoadUserRequested() -> void {}

	auto CilsViewer::OnLoadExecutionsRequested(int projectId) -> void {}

	auto CilsViewer::OnProjectsRequested(QVector<TC::Cils::JsonEntity::Project>* projects) -> void {}

	auto CilsViewer::OnUserRequested(std::optional<TC::Cils::JsonEntity::User>* user) -> void {}

	auto CilsViewer::OnExecutionsRequested(int projectId,
		QVector<TC::Cils::JsonEntity::ItemExecution>* itemExecutions,
		QVector<TC::Cils::JsonEntity::OnExecution>* onExecutions,
		QVector<TC::Cils::JsonEntity::SyncExecution>* itemSync) -> void {}

	auto CilsViewer::OnStartExecutionRequested(int projectId, int executionId, TC::Cils::Client::UserType userType) -> void {}

	auto CilsViewer::OnDownloadExecutionRequested(int projectId, int executionId,
		TC::Cils::Client::UserType userType) -> void {}

	auto CilsViewer::OnDownloadPreviewRequested(const QString& dataId) -> void {}
}
