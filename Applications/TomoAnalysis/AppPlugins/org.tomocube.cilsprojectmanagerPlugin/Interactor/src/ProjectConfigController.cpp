#include "ProjectConfigController.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
    struct ProjectConfigController::Impl {

    };

    ProjectConfigController::ProjectConfigController() : d(new Impl) {}

    ProjectConfigController::~ProjectConfigController() = default;
}
