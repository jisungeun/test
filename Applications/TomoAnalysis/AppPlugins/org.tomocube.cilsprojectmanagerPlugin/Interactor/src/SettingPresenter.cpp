#include "SettingPresenter.h"

#include "SettingViewer.h"
#include "ViewerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct SettingPresenter::Impl {
		Framework::ViewerHandler<SettingViewer> handler;
	};

	SettingPresenter::SettingPresenter() : ISettingOutputPort(), d(new Impl) {}

	SettingPresenter::~SettingPresenter() = default;

	auto SettingPresenter::GetOutputPath() -> QString {
		return d->handler->OnOutputPathRequested();
	}

	auto SettingPresenter::SetOutputPath(const QString& path) -> void {
		return d->handler->OnOutputPathChanged(path);
	}
}
