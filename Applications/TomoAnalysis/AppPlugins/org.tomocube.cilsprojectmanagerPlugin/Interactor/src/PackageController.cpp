#include "PackageController.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
    struct PackageController::Impl {

    };

    PackageController::PackageController() : d(new Impl) {}

    PackageController::~PackageController() = default;
}
