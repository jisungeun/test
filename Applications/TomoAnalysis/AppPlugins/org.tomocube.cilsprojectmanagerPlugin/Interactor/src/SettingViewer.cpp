#include "SettingViewer.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	SettingViewer::SettingViewer() = default;

	SettingViewer::~SettingViewer() = default;
}
