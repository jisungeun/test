#include "CilsConverter.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	auto CilsConverter::ToExecutions(int userId, const QVector<TC::Cils::JsonEntity::ItemExecution>& itemExecutions,
		const QVector<TC::Cils::JsonEntity::OnExecution>& onExecutions,
		const QVector<TC::Cils::JsonEntity::SyncExecution>& syncExecutions) -> QVector<Entity::CilsExecution> {
		QVector< Entity::CilsExecution> executions;

		for (const auto& ie : itemExecutions) {
			Entity::CilsExecution ex;

			ex.SetId(ie.GetId());
			ex.SetDataId(ie.GetDataId());
			ex.SetProjectId(ie.GetProjectId());
			if (ie.GetTitle().has_value())
				ex.SetTitle(ie.GetTitle().value());
			if (ie.GetAssignee().has_value())
				if (ie.GetAssignee().value().GetName().has_value())
					ex.SetAssignee(ie.GetAssignee().value().GetName().value());
			ex.SetAssignStatus(TC::Cils::JsonEntity::ItemExecution::ExecutionStatusToString(ie.GetAssignStatus()));
			if (ie.GetReviewer().has_value())
				if (ie.GetReviewer().value().GetName().has_value())
					ex.SetReviewer(ie.GetReviewer().value().GetName().value());
			ex.SetReviewStatus(TC::Cils::JsonEntity::ItemExecution::ExecutionStatusToString(ie.GetReviewStatus()));

			if (ie.GetAssignee().has_value() && ie.GetAssignee()->GetId() == userId)
				ex.SetCategory(Entity::UserType::Assignee);
			if (ie.GetReviewer().has_value() && ie.GetReviewer()->GetId() == userId) {
				ex.SetCategory(ex.GetCategory() | Entity::UserType::Reviewer);
			}

			QStringList worksets;
			for (const auto& w : ie.GetWorksets()) {
				worksets << w.GetName();
			}
			ex.SetWorkset(worksets);

			executions << ex;
		}

		for (auto& ex : executions) {
			for (const auto& oe : onExecutions) {
				if (ex.GetId() == oe.GetItemExecution().GetId()) {
					ex.SetTcfPath(oe.GetTcfPath());

					switch (oe.GetTcfStatus()) {
					case ItemStatus::Queued:
					case ItemStatus::RequestingAgentDownload:
					case ItemStatus::AgentDownloadQueued:
					case ItemStatus::DownloadStarted:
						ex.SetFileState(ex.GetFileState() | Entity::FileState::Downloading);
						break;
					}

					switch (oe.GetTcfStatus()) {
					case ItemStatus::OutputUploadComplete:
					case ItemStatus::DownloadComplete:
					case ItemStatus::ExecutionStarted:
					case ItemStatus::ExecutionComplete:
					case ItemStatus::FinishExecution:
					case ItemStatus::AutoDeleteQueue:
						ex.SetFileState(ex.GetFileState() | Entity::FileState::TcfReady);
						break;
					}

					switch (oe.GetTcfStatus()) {
					case ItemStatus::OutputUploadStarted:
					case ItemStatus::GenerateUploadItemInfo:
					case ItemStatus::OutputUploadQueue:
						ex.SetFileState(ex.GetFileState() | Entity::FileState::Uploading);
						break;
					}

					if (ex.GetCategory().testFlag(Entity::UserType::Reviewer)) {
						const auto& local = oe.GetOutputResult();
						const auto& remote = oe.GetItemExecution().GetResult();

						if (local.has_value()) {
							switch (oe.GetOutputStatus().value()) {
							case ItemStatus::Queued:
							case ItemStatus::RequestingAgentDownload:
							case ItemStatus::AgentDownloadQueued:
							case ItemStatus::DownloadStarted:
								ex.SetFileState(ex.GetFileState() | Entity::FileState::Downloading);
								break;
							}
							switch (oe.GetOutputStatus().value()) {
							case ItemStatus::OutputUploadComplete:
							case ItemStatus::DownloadComplete:
							case ItemStatus::ExecutionStarted:
							case ItemStatus::ExecutionComplete:
							case ItemStatus::FinishExecution:
							case ItemStatus::AutoDeleteQueue:
								ex.SetFileState(ex.GetFileState() | Entity::FileState::OutputReady);
								ex.SetOutputPath(oe.GetOutputPath().value());
								break;
							}

							switch (oe.GetOutputStatus().value()) {
							case ItemStatus::OutputUploadStarted:
							case ItemStatus::GenerateUploadItemInfo:
							case ItemStatus::OutputUploadQueue:
								ex.SetFileState(ex.GetFileState() | Entity::FileState::Uploading);
								break;
							}
						}

						if (remote.has_value() && local.has_value()) {
							if (remote->GetId() != local->GetId()) {
								ex.SetFileState(ex.GetFileState() | Entity::FileState::OutputOutdated);
							}
						}

						if (ex.GetAssignStatus() == Entity::ExecutionStatus::Executed && !local.has_value()) {
							ex.SetFileState(ex.GetFileState() | Entity::FileState::OutputOutdated);
						}
					}
				}
			}
		}

		for (auto& ex : executions) {
			for (const auto& se : syncExecutions) {
				for (const auto& p : se.GetProgress()) {
					if (ex.GetDataId() == p.GetDataId()) {
						ex.SetProgress(p.GetPercentage());
						goto OutOfFor;
					}
				}
			}
		OutOfFor:;
		}

		return executions;
	}
}
