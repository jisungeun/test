#include "CilsController.h"

#include "CilsConverter.h"
#include "ICilsInputPort.h"
#include "InputPortHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct CilsController::Impl {
		Framework::InputPortHandler<IOPort::ICilsInputPort> handler;

		QVector<Entity::CilsProject> projects;
		std::optional<Entity::CilsUser> user;
		QMap<int, QVector<Entity::CilsExecution>> executions;
	};

	CilsController::CilsController() : d(new Impl) {}

	CilsController::~CilsController() = default;

	auto CilsController::ProjectsLoaded(const QVector<TC::Cils::JsonEntity::Project>& projects) -> void {
		d->projects.clear();

		for (const auto& p : projects) {
			Entity::CilsProject e;
			e.SetId(p.GetId());
			e.SetName(p.GetName());
			e.SetType(p.GetType());

			d->projects.push_back(e);
		}

		d->handler->ProjectsLoaded(d->projects);
	}

	auto CilsController::UserLoaded(const std::optional<TC::Cils::JsonEntity::User>& user) -> void {
		if (user.has_value()) {
			d->user = Entity::CilsUser();
			d->user->SetId(user->GetId());
			if (user->GetName().has_value())
				d->user->SetName(user->GetName().value());
			if (user->GetUsername().has_value())
				d->user->SetUsername(user->GetUsername().value());
		} else {
			d->user = std::nullopt;
		}

		d->handler->UserLoaded(d->user);
	}

	auto CilsController::ExecutionsLoaded(int projectId, const QVector<TC::Cils::JsonEntity::ItemExecution>& itemExecutions, const QVector<TC::Cils::JsonEntity::OnExecution>& onExecutions, const QVector<TC::Cils::JsonEntity::SyncExecution>& syncExecutions) -> void {
		const auto executions = CilsConverter::ToExecutions(d->user->GetId(), itemExecutions, onExecutions, syncExecutions);

		d->executions[projectId].clear();
		d->executions[projectId] = executions;
		d->handler->ExecutionsLoaded(projectId, d->executions[projectId]);
	}

	auto CilsController::StartExecutionResponded(int executionId, TC::Cils::Client::UserType userType, const QString& result) -> void {
		for (const auto& key : d->executions.keys()) {
			for (const auto& ex : d->executions[key]) {
				if (ex.GetId() == executionId) {
					switch (userType) {
					case TC::Cils::Client::UserType::None:
						return;
					case TC::Cils::Client::UserType::Assignee:
						d->handler->StartExecutionResponded(ex, Entity::UserType::Assignee, result);
						return;
					case TC::Cils::Client::UserType::Reviewer:
						d->handler->StartExecutionResponded(ex, Entity::UserType::Reviewer, result);
						return;
					}
				}
			}
		}
	}

	auto CilsController::DownloadExecutionResponded(int projectId, int executionId, TC::Cils::Client::UserType userType, const QString& result) -> void {
		for (const auto& ex : d->executions[projectId]) {
			if (ex.GetId() == executionId) {
				d->handler->DownloadExecutionResponded(ex, result);
				return;
			}
		}
	}

	auto CilsController::DownloadPreviewResponded(const QString& dataId, const QByteArray& image) -> void {
		d->handler->DownloadPreviewResponded(dataId, image);
	}

	auto CilsController::NotConnected() -> void {
		d->handler->NotConnected();
	}

	auto CilsController::UpdateExecutions() -> void {
		d->handler->UpdateExecutionsRequested();
	}
}
