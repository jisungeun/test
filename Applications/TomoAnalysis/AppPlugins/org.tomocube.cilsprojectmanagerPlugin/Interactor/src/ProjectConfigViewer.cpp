#include "ProjectConfigViewer.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	ProjectConfigViewer::ProjectConfigViewer() = default;

	ProjectConfigViewer::~ProjectConfigViewer() = default;
}
