#include "AppViewer.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	AppViewer::AppViewer() : IViewer(typeid(AppViewer)) {};

	AppViewer::~AppViewer() = default;

	auto AppViewer::OnProjectsLoaded(const QVector<Entity::CilsProject>& projects) -> void {}

	auto AppViewer::OnExecutionsLoaded(int projectId, const QVector<Entity::CilsExecution>& executions) -> void {}

	auto AppViewer::OnUserLoaded(const std::optional<Entity::CilsUser>& user) -> void {}

	auto AppViewer::OnExecutionStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {}

	auto AppViewer::OnDownloadStarted(const Entity::CilsExecution& execution, Entity::UserType userType, const QString& result) -> void {}

	auto AppViewer::OnPreviewDownloaded(const QString& dataId, const QByteArray& image) -> void {}

	auto AppViewer::OnNotConnected() -> void {}
}
