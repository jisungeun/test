#include "PackagePresenter.h"

#include "PackageViewer.h"
#include "ViewerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct PackagePresenter::Impl {
		Framework::ViewerHandler<PackageViewer> handler;
	};

	PackagePresenter::PackagePresenter() : IPackageOutputPort(), d(new Impl) {}

	PackagePresenter::~PackagePresenter() = default;

	auto PackagePresenter::GetPackageNames() -> QStringList {
		return d->handler->GetPackageNames();
	}

	auto PackagePresenter::GetAppSymbol(const QString& packageName) -> QString {
		return d->handler->GetAppSymbol(packageName);
	}

}
