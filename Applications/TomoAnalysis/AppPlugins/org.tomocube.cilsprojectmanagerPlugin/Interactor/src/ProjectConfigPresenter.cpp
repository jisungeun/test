#include "ProjectConfigPresenter.h"

#include "ProjectConfigViewer.h"
#include "ViewerHandler.h"

namespace TomoAnalysis::CilsProjectManager::Interactor {
	struct ProjectConfigPresenter::Impl {
		Framework::ViewerHandler<ProjectConfigViewer> handler;
	};

	ProjectConfigPresenter::ProjectConfigPresenter() : IProjectConfigOutputPort(), d(new Impl) {}

	ProjectConfigPresenter::~ProjectConfigPresenter() = default;

	auto ProjectConfigPresenter::SetCurrentProject(const std::optional<Entity::CilsProject>& project) -> void {
		d->handler->OnCurrentProjectChanged(project);
	}

	auto ProjectConfigPresenter::SetCurrentUser(const std::optional<Entity::CilsUser>& user) -> void {
		d->handler->OnCurrentUserChanged(user);
	}

	auto ProjectConfigPresenter::SetCurrentPackage(const QString& packageName) -> void {
		d->handler->OnCurrentPackageChanged(packageName);
	}

	auto ProjectConfigPresenter::GetCurrentProject() -> std::optional<Entity::CilsProject> {
		return d->handler->OnCurrentProjectRequested();
	}

	auto ProjectConfigPresenter::GetCurrentUser() -> std::optional<Entity::CilsUser> {
		return d->handler->OnCurrentUserRequested();
	}

	auto ProjectConfigPresenter::GetCurrentPackage() -> QString {
		return d->handler->OnCurrentPackageRequested();
	}
}
