#pragma once

#include "IServiceProvider.h"
#include "IDisplayInputPort.h"

#include "TA.DataNav.BusinessRule.UseCaseExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	class TA_DataNav_BusinessRule_UseCase_API DisplayUseCase : public IOPort::IDisplayInputPort {
	public:
		explicit DisplayUseCase(Tomocube::IServiceProvider* provider);
		~DisplayUseCase() override;

		auto AddRoot(const QString& url) -> std::shared_ptr<Entity::UrlNode> override;
		auto UpdateRoot(const QString& url) -> void override;
		auto RemoveRoot(const QString& url) -> void override;
		auto RemoveTcf(const QString& url) -> void override;

		auto GetTcfMetadata(const QString& url) -> std::shared_ptr<Entity::TcfMetadata> override;

		auto ReadTcfMetadata(const QString& url) -> void override;
		auto ReadTcfThumbnail(const QString& url, const QString& modality) -> void override;
		auto ReadTcfThumbnails(const QString& url, const QString& modality) -> void override;

		auto SetTcfVisibility(bool value) -> void override;
		auto SetTimelapsePlay(bool value) -> void override;
		auto SetLastRootDir(const QString& value) -> void override;
		auto SetLastExportDir(const QString& value) -> void override;

		auto GetTcfVisibility() -> bool override;
		auto GetTimelapsePlay() -> bool override;
		auto GetLastRootDir() -> QString override;
		auto GetLastExportDir() -> QString override;

		auto Export(const QString& source, const QString& target, const QString& format) -> bool override;
		auto Export(const QStringList& sources, const QString& target, const QString& format) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}