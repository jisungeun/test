#pragma once

#include "IServiceProvider.h"
#include "ITcfReaderInputPort.h"

#include "TA.DataNav.BusinessRule.UseCaseExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	class TA_DataNav_BusinessRule_UseCase_API TcfReaderUseCase : public IOPort::ITcfReaderInputPort {
	public:
		TcfReaderUseCase(Tomocube::IServiceProvider* provider);
		~TcfReaderUseCase() override;

		auto UpdateTcf(const std::shared_ptr<Entity::TcfMetadata>& metadata) -> void override;
		auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void override;
		auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}