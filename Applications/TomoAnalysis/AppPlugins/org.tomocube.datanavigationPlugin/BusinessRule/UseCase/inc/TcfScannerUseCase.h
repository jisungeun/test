#pragma once

#include "IServiceProvider.h"
#include "ITcfScannerInputPort.h"

#include "TA.DataNav.BusinessRule.UseCaseExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	class TA_DataNav_BusinessRule_UseCase_API TcfScannerUseCase : public IOPort::ITcfScannerInputPort {
	public:
		TcfScannerUseCase(Tomocube::IServiceProvider* provider);
		~TcfScannerUseCase() override;

		auto UpdateNode(const std::shared_ptr<Entity::UrlNode>& node) -> void override;
		auto UpdateNodeState(const QString& url, bool busy) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}