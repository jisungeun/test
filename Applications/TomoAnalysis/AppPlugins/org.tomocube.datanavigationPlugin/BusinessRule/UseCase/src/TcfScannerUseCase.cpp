#include "TcfScannerUseCase.h"

#include "IDisplayOutputPort.h"
#include "ITcfScannerOutputPort.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	struct TcfScannerUseCase::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	TcfScannerUseCase::TcfScannerUseCase(Tomocube::IServiceProvider* provider) : ITcfScannerInputPort(), d(new Impl) {
		d->provider = provider;
	}

	TcfScannerUseCase::~TcfScannerUseCase() = default;
	
	auto TcfScannerUseCase::UpdateNode(const std::shared_ptr<Entity::UrlNode>& node) -> void {
		const auto display = d->provider->GetService<IOPort::IDisplayOutputPort>();

		display->UpdateNode(node);
	}

	auto TcfScannerUseCase::UpdateNodeState(const QString& url, bool busy) -> void {
		const auto display = d->provider->GetService<IOPort::IDisplayOutputPort>();

		display->UpdateNodeState(url, busy);
	}
}
