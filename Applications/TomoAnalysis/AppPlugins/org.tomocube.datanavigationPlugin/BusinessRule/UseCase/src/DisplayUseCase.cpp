#include "DisplayUseCase.h"

#include "IDisplayOutputPort.h"
#include "IExporterOutputPort.h"
#include "IPreferenceOutputPort.h"
#include "ITcfReaderOutputPort.h"
#include "ITcfScannerOutputPort.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	struct DisplayUseCase::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	DisplayUseCase::DisplayUseCase(Tomocube::IServiceProvider* provider) : IDisplayInputPort(), d(new Impl) {
		d->provider = provider;
	}

	DisplayUseCase::~DisplayUseCase() = default;
	
	auto DisplayUseCase::AddRoot(const QString& url) -> std::shared_ptr<Entity::UrlNode> {
		const auto scanner = d->provider->GetService<IOPort::ITcfScannerOutputPort>();

		return scanner->AddRoot(url);
	}

	auto DisplayUseCase::UpdateRoot(const QString& url) -> void {
		const auto scanner = d->provider->GetService<IOPort::ITcfScannerOutputPort>();

		scanner->UpdateRoot(url);
	}

	auto DisplayUseCase::RemoveRoot(const QString& url) -> void {
		const auto scanner = d->provider->GetService<IOPort::ITcfScannerOutputPort>();

		scanner->RemoveRoot(url);
	}

	auto DisplayUseCase::RemoveTcf(const QString& url) -> void {
		const auto reader = d->provider->GetService<IOPort::ITcfReaderOutputPort>();

		reader->Remove(url);
	}

	auto DisplayUseCase::GetTcfMetadata(const QString& url) -> std::shared_ptr<Entity::TcfMetadata> {
		const auto reader = d->provider->GetService<IOPort::ITcfReaderOutputPort>();

		return reader->GetMetadataDash(url);
	}

	auto DisplayUseCase::ReadTcfMetadata(const QString& url) -> void {
		const auto reader = d->provider->GetService<IOPort::ITcfReaderOutputPort>();

		reader->ReadMetadata(url);
	}

	auto DisplayUseCase::ReadTcfThumbnail(const QString& url, const QString& modality) -> void {
		const auto reader = d->provider->GetService<IOPort::ITcfReaderOutputPort>();

		reader->ReadThumbnail(url, modality);
	}

	auto DisplayUseCase::ReadTcfThumbnails(const QString& url, const QString& modality) -> void {
		const auto reader = d->provider->GetService<IOPort::ITcfReaderOutputPort>();

		reader->ReadThumbnails(url, modality);
	}

	auto DisplayUseCase::SetTcfVisibility(bool value) -> void {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		pref->SetValue("TCF Visibility", value);
	}

	auto DisplayUseCase::SetTimelapsePlay(bool value) -> void {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		pref->SetValue("Timelapse Play", value);
	}

	auto DisplayUseCase::SetLastRootDir(const QString& value) -> void {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		pref->SetValue("Last Root Dir", value);
	}

	auto DisplayUseCase::SetLastExportDir(const QString& value) -> void {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		pref->SetValue("Last Export Dir", value);
	}

	auto DisplayUseCase::GetTcfVisibility() -> bool {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		if (const auto value = pref->GetValue("TCF Visibility"))
			return *value == "true";

		return true;
	}

	auto DisplayUseCase::GetTimelapsePlay() -> bool {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		if (const auto value = pref->GetValue("Timelapse Play"))
			return *value == "true";

		return true;
	}

	auto DisplayUseCase::GetLastRootDir() -> QString {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		if (const auto value = pref->GetValue("Last Root Dir"))
			return *value;

		return {};
	}

	auto DisplayUseCase::GetLastExportDir() -> QString {
		const auto pref = d->provider->GetService<IOPort::IPreferenceOutputPort>();

		if (const auto value = pref->GetValue("Last Export Dir"))
			return *value;

		return {};
	}

	auto DisplayUseCase::Export(const QString& source, const QString& target, const QString& format) -> bool {
		const auto exporter = d->provider->GetService<IOPort::IExporterOutputPort>();

		return exporter->Export(source, target, format);
	}

	auto DisplayUseCase::Export(const QStringList& sources, const QString& target, const QString& format) -> bool {
		const auto exporter = d->provider->GetService<IOPort::IExporterOutputPort>();

		return exporter->Export(sources, target, format);
	}
}
