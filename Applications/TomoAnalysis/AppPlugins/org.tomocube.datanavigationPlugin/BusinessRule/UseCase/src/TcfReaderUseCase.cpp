#include "TcfReaderUseCase.h"

#include "IDisplayOutputPort.h"
#include "ITcfReaderOutputPort.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::UseCase {
	struct TcfReaderUseCase::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	TcfReaderUseCase::TcfReaderUseCase(Tomocube::IServiceProvider* provider) : ITcfReaderInputPort(), d(new Impl) {
		d->provider = provider;
	}

	TcfReaderUseCase::~TcfReaderUseCase() = default;

	auto TcfReaderUseCase::UpdateTcf(const std::shared_ptr<Entity::TcfMetadata>& metadata) -> void {
		const auto display = d->provider->GetService<IOPort::IDisplayOutputPort>();

		display->UpdateTcf(metadata);
	}
	
	auto TcfReaderUseCase::UpdateThumbnail(const QString & url, const QString & modality, const QPixmap & thumbnail) -> void {
		const auto display = d->provider->GetService<IOPort::IDisplayOutputPort>();

		display->UpdateThumbnail(url, modality, thumbnail);
	}

	auto TcfReaderUseCase::UpdateThumbnails(const QString & url, const QString & modality, const QVector<QPixmap>&thumbnails) -> void {
		const auto display = d->provider->GetService<IOPort::IDisplayOutputPort>();

		display->UpdateThumbnails(url, modality, thumbnails);
	}
}
