#pragma once

#include <QPixmap>

#include "IService.h"
#include "UrlNode.h"
#include "TcfMetadata.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API IDisplayInputPort : public Tomocube::IService {
	public:
		virtual auto AddRoot(const QString& url)->std::shared_ptr<Entity::UrlNode> = 0;
		virtual auto UpdateRoot(const QString& url) -> void = 0;
		virtual auto RemoveRoot(const QString& url)-> void = 0;
		virtual auto RemoveTcf(const QString& url)-> void = 0;

		virtual auto GetTcfMetadata(const QString& url)->std::shared_ptr<Entity::TcfMetadata> = 0;

		virtual auto ReadTcfMetadata(const QString& url) -> void = 0;
		virtual auto ReadTcfThumbnail(const QString& url, const QString& modality) -> void = 0;
		virtual auto ReadTcfThumbnails(const QString& url, const QString& modality) -> void = 0;

		virtual auto SetTcfVisibility(bool value) -> void = 0;
		virtual auto SetTimelapsePlay(bool value) -> void = 0;
		virtual auto SetLastRootDir(const QString& value) -> void = 0;
		virtual auto SetLastExportDir(const QString& value) -> void = 0;

		virtual auto GetTcfVisibility() -> bool = 0;
		virtual auto GetTimelapsePlay() -> bool = 0;
		virtual auto GetLastRootDir()->QString = 0;
		virtual auto GetLastExportDir()->QString = 0;

		virtual auto Export(const QString& source, const QString& target, const QString& format) -> bool = 0;
		virtual auto Export(const QStringList& sources, const QString& target, const QString& format) -> bool = 0;
	};
}
