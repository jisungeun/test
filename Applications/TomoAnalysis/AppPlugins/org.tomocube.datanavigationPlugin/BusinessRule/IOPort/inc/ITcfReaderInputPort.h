#pragma once

#include <QPixmap>

#include "IService.h"
#include "TcfMetadata.h"
#include "UrlNode.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API ITcfReaderInputPort : public Tomocube::IService {
	public:
		virtual auto UpdateTcf(const std::shared_ptr<Entity::TcfMetadata>& metadata) -> void = 0;
		virtual auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void = 0;
		virtual auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void = 0;

	};
}
