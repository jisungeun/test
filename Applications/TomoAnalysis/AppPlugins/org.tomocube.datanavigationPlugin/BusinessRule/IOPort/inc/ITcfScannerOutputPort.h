#pragma once

#include <memory>

#include "IService.h"
#include "UrlNode.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API ITcfScannerOutputPort : public Tomocube::IService {
	public:
		virtual auto AddRoot(const QString& url)->std::shared_ptr<Entity::UrlNode> = 0;
		virtual auto UpdateRoot(const QString& url) -> void = 0;
		virtual auto RemoveRoot(const QString& url) -> void = 0;
	};
}