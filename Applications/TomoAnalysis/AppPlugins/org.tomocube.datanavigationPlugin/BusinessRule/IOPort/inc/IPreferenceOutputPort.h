#pragma once

#include <optional>

#include <QVariant>

#include "IService.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API IPreferenceOutputPort : public Tomocube::IService {
	public:
		virtual auto SetValue(const QString& name, const QVariant& value) -> void = 0;
		virtual auto GetValue(const QString& name)->std::optional<QString> = 0;
	};
}