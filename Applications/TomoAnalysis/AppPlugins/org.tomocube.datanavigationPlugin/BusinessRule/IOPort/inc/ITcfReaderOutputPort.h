#pragma once

#include "IService.h"
#include "TcfMetadata.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API ITcfReaderOutputPort : public Tomocube::IService {
	public:
		virtual auto GetMetadataDash(const QString& url)->std::shared_ptr<Entity::TcfMetadata> = 0;

		virtual auto ReadMetadata(const QString& url) -> void = 0;
		virtual auto ReadThumbnail(const QString& url, const QString& modality) -> void = 0;
		virtual auto ReadThumbnails(const QString& url, const QString& modality) -> void = 0;

		virtual auto Remove(const QString& url) -> void = 0;
	};
}