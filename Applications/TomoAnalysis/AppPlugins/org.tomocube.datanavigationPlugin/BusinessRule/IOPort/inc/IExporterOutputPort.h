#pragma once

#include <QStringList>

#include "IService.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API IExporterOutputPort : public Tomocube::IService {
	public:
		virtual auto Export(const QString& source, const QString& target, const QString& format) -> bool = 0;
		virtual auto Export(const QStringList& sources, const QString& target, const QString& format) -> bool = 0;
	};
}