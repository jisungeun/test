#pragma once

#include <memory>

#include "IService.h"
#include "UrlNode.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API ITcfScannerInputPort : public Tomocube::IService {
	public:
		virtual auto UpdateNode(const std::shared_ptr<Entity::UrlNode>& node) -> void = 0;
		virtual auto UpdateNodeState(const QString& url, bool busy) -> void = 0;

	};
}