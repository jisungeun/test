#pragma once

#include <QPixmap>

#include "IService.h"
#include "UrlNode.h"
#include "TcfMetadata.h"

#include "TA.DataNav.BusinessRule.IOPortExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::IOPort {
	class TA_DataNav_BusinessRule_IOPort_API IDisplayOutputPort : public Tomocube::IService {
	public:
		virtual auto UpdateNode(const std::shared_ptr<Entity::UrlNode>& node) -> void = 0;
		virtual auto UpdateTcf(const std::shared_ptr<Entity::TcfMetadata>& metadata) -> void = 0;
		virtual auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void = 0;
		virtual auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void = 0;
		virtual auto UpdateNodeState(const QString& url, bool busy) -> void = 0;
	};
}