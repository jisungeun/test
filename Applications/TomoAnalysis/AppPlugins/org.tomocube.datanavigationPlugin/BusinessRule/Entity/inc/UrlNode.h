#pragma once

#include <memory>

#include <QList>

#include "TA.DataNav.BusinessRule.EntityExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::Entity {
	class TA_DataNav_BusinessRule_Entity_API UrlNode {
	public:
		enum class Type {
			Directory,
			HtxProject,
			HtxExperiment
		};

		UrlNode();
		UrlNode(const QString& url, Type type);
		~UrlNode();

		auto GetUrl() const -> const QString&;
		auto GetType() const->Type;
		auto GetNodes() const -> const QList<std::shared_ptr<UrlNode>>&;
		auto GetTcfs() const -> const QStringList&;

		auto SetUrl(const QString& url) -> void;
		auto SetType(Type type) -> void;

		auto AddNode(const std::shared_ptr<UrlNode>& node) -> void;
		auto AddNode(const QString& url, Type type) -> void;
		auto RemoveNode(const std::shared_ptr<UrlNode>& node) -> void;
		auto RemoveNode(const QString& url) -> void;
		auto ClearNodes() -> void;

		auto AddTcf(const QString& url) -> void;
		auto RemoveTcf(const QString& url) -> void;
		auto ClearTcfs() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}