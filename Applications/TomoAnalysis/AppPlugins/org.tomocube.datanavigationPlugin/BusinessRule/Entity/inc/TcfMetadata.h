#pragma once

#include <memory>

#include <QMap>

#include "TA.DataNav.BusinessRule.EntityExport.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::Entity {
	class TA_DataNav_BusinessRule_Entity_API TcfMetadata {
	public:
		TcfMetadata();
		explicit TcfMetadata(const QString& url);
		~TcfMetadata();

		auto GetUrl() const -> const QString&;
		auto GetDataId() const -> QString;
		auto GetTitle() const -> QString;
		auto GetMetadata() const -> const QMap<QString, QString>&;
		auto GetModalities() const -> QStringList;
		auto GetDataCount(const QString& modality) const -> int;
		auto IsTimelapse() const -> bool;

		auto SetUrl(const QString& url) -> void;
		auto AddMetadata(const QString& key, const QString& value) -> void;
		auto RemoveMetadata(const QString& key) -> void;
		auto ClearMetadata() -> void;
		auto AddModality(const QString& modality, int dataCount) -> void;
		auto RemoveModality(const QString& modality) -> void;
		auto ClearModalities() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}