#include "UrlNode.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::Entity {
	struct UrlNode::Impl {
		QString url;
		Type type = Type::Directory;
		QList<std::shared_ptr<UrlNode>> nodes;
		QStringList tcfs;

		auto ContainsNode(const QString& url) const -> bool;
	};

	auto UrlNode::Impl::ContainsNode(const QString& url) const -> bool {
		return std::any_of(nodes.cbegin(), nodes.cend(), [url](const std::shared_ptr<UrlNode>& node) {
			return node->GetUrl() == url;
		});
	}

	UrlNode::UrlNode() : d(new Impl) {}

	UrlNode::UrlNode(const QString& url, Type type) : d(new Impl) {
		d->url = url;
		d->type = type;
	}

	UrlNode::~UrlNode() = default;

	auto UrlNode::GetUrl() const -> const QString& {
		return d->url;
	}

	auto UrlNode::GetType() const -> Type {
		return d->type;
	}

	auto UrlNode::GetNodes() const -> const QList<std::shared_ptr<UrlNode>>& {
		return d->nodes;
	}

	auto UrlNode::GetTcfs() const -> const QStringList& {
		return d->tcfs;
	}

	auto UrlNode::SetUrl(const QString& url) -> void {
		d->url = url;
	}

	auto UrlNode::SetType(Type type) -> void {
		d->type = type;
	}

	auto UrlNode::AddNode(const std::shared_ptr<UrlNode>& node) -> void {
		if (node && !d->nodes.contains(node) && !d->ContainsNode(node->GetUrl()))
			d->nodes.push_back(node);
	}

	auto UrlNode::AddNode(const QString& url, Type type) -> void {
		if (!d->ContainsNode(url))
			d->nodes.push_back(std::make_shared<UrlNode>(url, type));
	}

	auto UrlNode::RemoveNode(const std::shared_ptr<UrlNode>& node) -> void {
		d->nodes.removeOne(node);
	}

	auto UrlNode::RemoveNode(const QString& url) -> void {
		const auto node = std::find_if(d->nodes.begin(), d->nodes.end(), [url](const std::shared_ptr<UrlNode>& node) {
			return node->GetUrl() == url;
		});

		if (node != d->nodes.end())
			d->nodes.removeOne(*node);
	}

	auto UrlNode::ClearNodes() -> void {
		d->nodes.clear();
	}

	auto UrlNode::AddTcf(const QString& url) -> void {
		d->tcfs.push_back(url);
	}

	auto UrlNode::RemoveTcf(const QString& url) -> void {
		d->tcfs.removeOne(url);
	}

	auto UrlNode::ClearTcfs() -> void {
		d->tcfs.clear();
	}
}
