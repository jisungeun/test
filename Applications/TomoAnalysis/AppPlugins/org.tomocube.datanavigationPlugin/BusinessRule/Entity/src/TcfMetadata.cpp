#include "TcfMetadata.h"

namespace TomoAnalysis::DataNavigation::BusinessRule::Entity {
	struct TcfMetadata::Impl {
		QString url;
		QMap<QString, QString> metadata;
		QMap<QString, int> modalities;
	};

	TcfMetadata::TcfMetadata() : d(new Impl) {}

	TcfMetadata::TcfMetadata(const QString& url) : d(new Impl) {
		d->url = url;
	}

	TcfMetadata::~TcfMetadata() = default;

	auto TcfMetadata::GetUrl() const -> const QString& {
		return d->url;
	}

	auto TcfMetadata::GetDataId() const -> QString {
		if (d->metadata.contains("DataID"))
			return d->metadata["DataID"];

		return {};
	}

	auto TcfMetadata::GetTitle() const -> QString {
		if (d->metadata.contains("Title"))
			return d->metadata["Title"];

		return {};
	}

	auto TcfMetadata::GetMetadata() const -> const QMap<QString, QString>& {
		return d->metadata;
	}

	auto TcfMetadata::GetModalities() const -> QStringList {
		return d->modalities.keys();
	}

	auto TcfMetadata::GetDataCount(const QString& modality) const -> int {
		if (d->modalities.contains(modality))
			return d->modalities[modality];

		return -1;
	}

	auto TcfMetadata::IsTimelapse() const -> bool {
		return std::any_of(d->modalities.begin(), d->modalities.end(), [](int count) {
			return count > 1;
		});
	}

	auto TcfMetadata::SetUrl(const QString& url) -> void {
		d->url = url;
	}

	auto TcfMetadata::AddMetadata(const QString& key, const QString& value) -> void {
		if (!key.isEmpty() && !value.isEmpty())
			d->metadata[key] = value;
	}

	auto TcfMetadata::RemoveMetadata(const QString& key) -> void {
		d->metadata.remove(key);
	}

	auto TcfMetadata::ClearMetadata() -> void {
		d->metadata.clear();
	}

	auto TcfMetadata::AddModality(const QString& modality, int dataCount) -> void {
		if (!modality.isEmpty() && dataCount > 0)
			d->modalities[modality] = dataCount;
	}

	auto TcfMetadata::RemoveModality(const QString& modality) -> void {
		d->modalities.remove(modality);
	}

	auto TcfMetadata::ClearModalities() -> void {
		d->modalities.clear();
	}
}
