#include <QMap>
#include <QDir>
#include <QDateTime>
#include <QStack>
#include <QMutexLocker>

#include "Directories.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Model {
	struct Directories::Impl {
		uint64_t updatedAt = 0;
		QStringList updatedList;
		QStringList roots;

		QMutex mutex;

		QMap<QString, std::shared_ptr<DirectoryNode>> nodes;

		auto AddUpdated(const QString& path) -> void;
	};

	auto Directories::Impl::AddUpdated(const QString& path) -> void {
		bool duplicated = false;

		for (int i = 0; i < updatedList.count(); i++) {
			if (!duplicated && path.contains(updatedList[i]))
				duplicated = true;

			if (updatedList[i] != path && updatedList[i].contains(path)) {
				updatedList.removeAt(i);
				i--;
			}
		}

		if (!duplicated) {
			updatedList.push_back(path);
			updatedAt = QDateTime::currentSecsSinceEpoch();
		}
	}

	Directories::Directories() : d(new Impl) {}

	Directories::~Directories() = default;

	auto Directories::AddRoot(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		if (!d->roots.contains(path))
			d->roots.push_back(path);
	}

	auto Directories::RemoveRoot(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		d->roots.removeOne(path);

		if (d->nodes.contains(path)) {
			QStack<std::shared_ptr<DirectoryNode>> stack;
			stack.push(d->nodes[path]);

			while (!stack.isEmpty()) {
				const auto node = stack.pop();
				bool removable = true;

				for (const auto& r : d->roots) {
					if (node->GetPath().contains(r))
						removable = false;
				}

				if (removable) {
					d->nodes.remove(node->GetPath());
				}

				for (const auto& n : node->GetNodes()) {
					if (d->nodes.contains(n))
						stack.push(d->nodes[n]);
				}
			}
		}
	}

	auto Directories::AddDirectory(const QString& path, DirectoryNode::Type type) -> void {
		QMutexLocker locker(&d->mutex);

		if (const QFileInfo info(path); info.isDir()) {
			if (QDir dir(path); dir.cdUp() && d->nodes.contains(dir.absolutePath())) {
				const auto& parent = d->nodes[dir.absolutePath()];

				if (!parent->Contains(path)) {
					parent->GetNodes().push_back(path);
					d->AddUpdated(parent->GetPath());
				}
			}

			if (!d->nodes.contains(path) || d->nodes[path]->GetPath() != path || d->nodes[path]->GetType() != type) {
				const auto node = std::make_shared<DirectoryNode>(path, type);
				d->nodes[path] = node;
				d->AddUpdated(path);
			}
		}
	}

	auto Directories::AddTcf(const QString& filename) -> void {
		QMutexLocker locker(&d->mutex);

		if (const QFileInfo info(filename); info.isFile()) {
			const auto path = info.absolutePath();

			if (d->nodes.contains(path)) {
				if (auto& tcfs = d->nodes[path]->GetTcfs(); !tcfs.contains(filename)) {
					tcfs.push_back(filename);

					d->AddUpdated(path);
				}
			}
		}
	}

	auto Directories::RemoveDirectory(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		if (const QDir info(path); d->nodes.contains(info.absolutePath())) {
			if (QDir dir(info); dir.cdUp() && d->nodes.contains(dir.absolutePath())) {
				const auto& parent = d->nodes[dir.absolutePath()];

				parent->GetNodes().removeOne(path);
				d->AddUpdated(parent->GetPath());
			}

			d->nodes.remove(path);
		}
	}

	auto Directories::RemoveTcf(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		if (const QFileInfo info(path); d->nodes.contains(info.absolutePath())) {
			d->nodes[info.absolutePath()]->GetTcfs().removeOne(path);
			d->AddUpdated(path);
		}
	}

	auto Directories::ContainsNode(const QString& path) const -> bool {
		QMutexLocker locker(&d->mutex);

		return d->nodes.contains(path);
	}

	auto Directories::GetNode(const QString& path) const -> std::shared_ptr<DirectoryNode> {
		QMutexLocker locker(&d->mutex);

		if (d->nodes.contains(path))
			return d->nodes[path];

		return {};
	}

	auto Directories::GetLastUpdatedTime() const -> uint64_t {
		QMutexLocker locker(&d->mutex);

		return d->updatedAt;
	}

	auto Directories::TakeUpdatedList() const -> QStringList {
		QMutexLocker locker(&d->mutex);

		auto temp = d->updatedList;
		d->updatedList.clear();

		return temp;
	}
}
