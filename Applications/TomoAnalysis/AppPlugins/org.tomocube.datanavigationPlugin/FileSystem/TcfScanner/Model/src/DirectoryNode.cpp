#include "DirectoryNode.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Model {
	struct DirectoryNode::Impl {
		QString path;
		Type type = Type::Directory;
		QStringList tcfs;
		QStringList nodes;
	};

	DirectoryNode::DirectoryNode(const QString& path, Type type) : d(new Impl) {
		d->path = path;
		d->type = type;
	}

	auto DirectoryNode::operator==(const DirectoryNode* rhs) const -> bool {
		return d->path == rhs->d->path && d->type == rhs->d->type;
	}

	DirectoryNode::~DirectoryNode() = default;

	auto DirectoryNode::GetPath() const -> const QString& {
		return d->path;
	}

	auto DirectoryNode::GetType() const -> Type& {
		return d->type;
	}

	auto DirectoryNode::GetTcfs() const -> QStringList& {
		return d->tcfs;
	}

	auto DirectoryNode::GetNodes() const -> QStringList& {
		return d->nodes;
	}

	auto DirectoryNode::Contains(const QString& path) const -> bool {
		return d->nodes.contains(path);
	}
}
