#pragma once

#include <memory>

#include "DirectoryNode.h"

#include "TA.DataNav.FileSystem.TcfScanner.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Model {
	class TA_DataNav_FileSystem_TcfScanner_Model_API Directories final {
	public:
		Directories();
		~Directories();

		auto AddRoot(const QString& path) -> void;
		auto RemoveRoot(const QString& path) -> void;

		auto AddDirectory(const QString& path, DirectoryNode::Type type) -> void;
		auto AddTcf(const QString& filename) -> void;
		auto RemoveDirectory(const QString& path) -> void;
		auto RemoveTcf(const QString& path) -> void;

		auto ContainsNode(const QString& path) const -> bool;
		auto GetNode(const QString& path) const -> std::shared_ptr<DirectoryNode>;

		auto GetLastUpdatedTime() const -> uint64_t;
		auto TakeUpdatedList() const->QStringList;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}