#pragma once

#include <memory>

#include <QStringList>
#include <QVector>

#include "TA.DataNav.FileSystem.TcfScanner.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Model {
	class TA_DataNav_FileSystem_TcfScanner_Model_API DirectoryNode {
	public:
		enum class Type {
			Directory,
			HtxProject,
			HtxExperiment
		};

		DirectoryNode(const QString& path, Type type);
		auto operator==(const DirectoryNode* rhs) const -> bool;
		~DirectoryNode();

		auto GetPath() const -> const QString&;
		auto GetType() const->Type&;
		auto GetTcfs() const -> QStringList&;
		auto GetNodes() const -> QStringList&;
		auto Contains(const QString& path) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}