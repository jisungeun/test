#pragma once

#include <QString>

#include "Directories.h"
#include "IService.h"

#include "TA.DataNav.FileSystem.TcfScanner.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Model {
	class TA_DataNav_FileSystem_TcfScanner_Model_API IScanner : public Tomocube::IService {
	public:
		virtual auto GetDirectories() const->const std::shared_ptr<Directories>& = 0;
		virtual auto ScanPathRecursively(const QString& path) -> void = 0;

		virtual auto AddRoot(const QString& path) -> void = 0;
		virtual auto RemoveRoot(const QString& path) -> void = 0;
	};
}