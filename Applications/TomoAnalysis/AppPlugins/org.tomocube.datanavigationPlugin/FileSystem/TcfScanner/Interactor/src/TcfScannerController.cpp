#include <QQueue>
#include <QDateTime>
#include <QThreadPool>

#include "TcfScannerController.h"

#include "IScanner.h"
#include "ITcfScannerInputPort.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Interactor {
	struct TcfScannerController::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		std::shared_ptr<Model::Directories> model = nullptr;
		QThreadPool pool;

		static auto ToEntityType(Model::DirectoryNode::Type type)->BusinessRule::Entity::UrlNode::Type;
		auto ToEntityNode(const std::shared_ptr<Model::DirectoryNode>& node)->std::shared_ptr<BusinessRule::Entity::UrlNode>;
	};

	auto TcfScannerController::Impl::ToEntityType(Model::DirectoryNode::Type type) -> BusinessRule::Entity::UrlNode::Type {
		switch (type) {
			case Model::DirectoryNode::Type::Directory:
				return BusinessRule::Entity::UrlNode::Type::Directory;
			case Model::DirectoryNode::Type::HtxProject:
				return BusinessRule::Entity::UrlNode::Type::HtxProject;
			case Model::DirectoryNode::Type::HtxExperiment:
				return BusinessRule::Entity::UrlNode::Type::HtxExperiment;
		}

		return BusinessRule::Entity::UrlNode::Type::Directory;
	}

	auto TcfScannerController::Impl::ToEntityNode(const std::shared_ptr<Model::DirectoryNode>& node) -> std::shared_ptr<BusinessRule::Entity::UrlNode> {
		auto entity = std::make_shared<BusinessRule::Entity::UrlNode>(node->GetPath(), ToEntityType(node->GetType()));
		QQueue<std::pair<std::shared_ptr<BusinessRule::Entity::UrlNode>, std::shared_ptr<Model::DirectoryNode>>> queue;
		queue.enqueue({ entity, node });

		while (!queue.isEmpty()) {
			const auto [qentity, qnode] = queue.dequeue();

			for (const auto& t : qnode->GetTcfs())
				qentity->AddTcf(t);

			for (const auto& p : qnode->GetNodes()) {
				if (const auto chNode = model->GetNode(p)) {
					const auto child = std::make_shared<BusinessRule::Entity::UrlNode>(p, ToEntityType(chNode->GetType()));
					qentity->AddNode(child);

					queue.enqueue({ child, chNode });
				}
			}
		}

		return entity;
	}

	TcfScannerController::TcfScannerController(Tomocube::IServiceProvider* provider, const std::shared_ptr<Model::Directories>& model) : d(new Impl) {
		d->provider = provider;
		d->model = model;
		d->pool.waitForDone();
	}

	TcfScannerController::~TcfScannerController() = default;

	auto TcfScannerController::Flush() -> void {
		const auto updated = d->model->TakeUpdatedList();

		for (const auto& b : updated) {
			if (const auto node = d->model->GetNode(b)) {
				d->pool.start([this, node] {
					const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfScannerInputPort>();
					usecase->UpdateNode(d->ToEntityNode(node));
				});
			}
		}
	}

	auto TcfScannerController::TryFlush() -> void {
		auto value = QDateTime::currentSecsSinceEpoch() - d->model->GetLastUpdatedTime();
		if (d->model->GetLastUpdatedTime() > 0 && QDateTime::currentSecsSinceEpoch() - d->model->GetLastUpdatedTime() >= 3)
			Flush();
	}

	auto TcfScannerController::ScanPath(const QString& path) -> void {
		const auto scanner = d->provider->GetService<Model::IScanner>();

		scanner->ScanPathRecursively(path);
	}

	auto TcfScannerController::StartScan(const QString& path) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfScannerInputPort>();

		usecase->UpdateNodeState(path, true);
	}

	auto TcfScannerController::FinishScan(const QString& path) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfScannerInputPort>();

		usecase->UpdateNodeState(path, false);
	}
}
