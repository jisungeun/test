#include <QThreadPool>
#include <QQueue>

#include "TcfScannerPresenter.h"

#include "IScanner.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Interactor {
	struct TcfScannerPresenter::Impl {
		QThreadPool pool;
		Tomocube::IServiceProvider* provider = nullptr;

		static auto ToEntityType(Model::DirectoryNode::Type type)->BusinessRule::Entity::UrlNode::Type;
		auto ToEntityNode(const std::shared_ptr<Model::DirectoryNode>& node)->std::shared_ptr<BusinessRule::Entity::UrlNode>;
	};

	auto TcfScannerPresenter::Impl::ToEntityType(Model::DirectoryNode::Type type) -> BusinessRule::Entity::UrlNode::Type {
		switch (type) {
			case Model::DirectoryNode::Type::Directory:
				return BusinessRule::Entity::UrlNode::Type::Directory;
			case Model::DirectoryNode::Type::HtxProject:
				return BusinessRule::Entity::UrlNode::Type::HtxProject;
			case Model::DirectoryNode::Type::HtxExperiment:
				return BusinessRule::Entity::UrlNode::Type::HtxExperiment;
		}

		return BusinessRule::Entity::UrlNode::Type::Directory;
	}

	auto TcfScannerPresenter::Impl::ToEntityNode(const std::shared_ptr<Model::DirectoryNode>& node) -> std::shared_ptr<BusinessRule::Entity::UrlNode> {
		const auto scanner = provider->GetService<Model::IScanner>();
		auto entity = std::make_shared<BusinessRule::Entity::UrlNode>(node->GetPath(), ToEntityType(node->GetType()));
		QQueue<std::pair<std::shared_ptr<BusinessRule::Entity::UrlNode>, std::shared_ptr<Model::DirectoryNode>>> queue;
		queue.enqueue({ entity, node });

		while (!queue.isEmpty()) {
			const auto [qentity, qnode] = queue.dequeue();

			for (const auto& t : qnode->GetTcfs())
				qentity->AddTcf(t);

			for (const auto& p : qnode->GetNodes()) {
				const auto& model = scanner->GetDirectories();
				const auto chNode = model->GetNode(p);
				const auto child = std::make_shared<BusinessRule::Entity::UrlNode>(p, ToEntityType(chNode->GetType()));
				qentity->AddNode(child);

				queue.enqueue({ child, chNode });
			}
		}

		return entity;
	}

	TcfScannerPresenter::TcfScannerPresenter(Tomocube::IServiceProvider* provider) : ITcfScannerOutputPort(), d(new Impl) {
		d->provider = provider;
	}

	TcfScannerPresenter::~TcfScannerPresenter() = default;

	auto TcfScannerPresenter::AddRoot(const QString& url) -> std::shared_ptr<BusinessRule::Entity::UrlNode> {
		const auto scanner = d->provider->GetService<Model::IScanner>();
		const auto model = scanner->GetDirectories();

		d->pool.start([scanner, url] {
			scanner->AddRoot(url);
			});

		if (const auto node = model->GetNode(url))
			return d->ToEntityNode(node);
		else {
			d->pool.start([scanner, url] {
				scanner->ScanPathRecursively(url);
				});
		}

		return {};
	}

	auto TcfScannerPresenter::UpdateRoot(const QString& url) -> void {
		const auto scanner = d->provider->GetService<Model::IScanner>();
		const auto model = scanner->GetDirectories();

		d->pool.start([scanner, url] {
			scanner->ScanPathRecursively(url);
			});
	}

	auto TcfScannerPresenter::RemoveRoot(const QString& url) -> void {
		d->pool.start([this, url] {
			const auto scanner = d->provider->GetService<Model::IScanner>();

			scanner->RemoveRoot(url);
			});
	}
}
