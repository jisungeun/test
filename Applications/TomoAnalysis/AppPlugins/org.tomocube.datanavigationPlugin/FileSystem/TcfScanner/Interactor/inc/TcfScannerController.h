#pragma once

#include "IServiceProvider.h"
#include "Directories.h"

#include "TA.DataNav.FileSystem.TcfScanner.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Interactor {
	class TA_DataNav_FileSystem_TcfScanner_Interactor_API TcfScannerController {
	public:
		TcfScannerController(Tomocube::IServiceProvider* provider, const std::shared_ptr<Model::Directories>& model);
		~TcfScannerController();

		auto Flush() -> void;
		auto TryFlush() -> void;

		auto ScanPath(const QString& path) -> void;

		auto StartScan(const QString& path) -> void;
		auto FinishScan(const QString& path) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
