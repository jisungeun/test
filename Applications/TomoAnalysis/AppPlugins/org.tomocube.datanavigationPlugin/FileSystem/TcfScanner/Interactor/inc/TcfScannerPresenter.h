#pragma once

#include "IServiceProvider.h"
#include "ITcfScannerOutputPort.h"

#include "TA.DataNav.FileSystem.TcfScanner.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Interactor {
	class TA_DataNav_FileSystem_TcfScanner_Interactor_API TcfScannerPresenter final : public BusinessRule::IOPort::ITcfScannerOutputPort {
	public:
		explicit TcfScannerPresenter(Tomocube::IServiceProvider* provider);
		~TcfScannerPresenter() override;

		auto AddRoot(const QString& url) ->std::shared_ptr<BusinessRule::Entity::UrlNode> override;
		auto UpdateRoot(const QString& url) -> void override;
		auto RemoveRoot(const QString& url) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}