#include <QDirIterator>
#include <QMutexLocker>
#include <QFileSystemWatcher>
#include <QThreadPool>

#include "FileSystemTcfScanner.h"
#include "TcfScannerController.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Scanner {
	struct FileSystemTcfScanner::Impl {
		std::shared_ptr<Model::Directories> model;
		Tomocube::IServiceProvider* provider = nullptr;

		QThreadPool pool;
		QStringList scanning;
		QStringList stopScan;
		QMutex mutex;

		auto ScanPath(const QDir& dir) -> void;
	};

	auto FileSystemTcfScanner::Impl::ScanPath(const QDir& dir) -> void {
		const auto isProject = !dir.entryList({ "*.tcxpro" }, QDir::Filter::Files | QDir::NoDotAndDotDot).isEmpty();
		const auto isExperiment = !dir.entryList({ "*.tcxexp" }, QDir::Filter::Files | QDir::NoDotAndDotDot).isEmpty();
		const auto tcfs = dir.entryList({ "*.tcf" }, QDir::Filter::Files | QDir::NoDotAndDotDot);

		QMutexLocker locker(&mutex);
		locker.unlock();

		if (const auto node = model->GetNode(dir.absolutePath())) {
			const auto subdirs = dir.entryList(QDir::Filter::Dirs | QDir::NoDotAndDotDot);
			const auto subnodes = node->GetNodes();
			const auto subtcfs = node->GetTcfs();

			for (const auto& l : subnodes) {
				if (!subdirs.contains(QDir(l).dirName()))
					model->RemoveDirectory(l);
			}

			for (const auto& l : subtcfs) {
				if (!tcfs.contains(QDir(l).dirName()))
					model->RemoveTcf(l);
			}
		}

		if (isProject)
			model->AddDirectory(dir.absolutePath(), Model::DirectoryNode::Type::HtxProject);
		else if (isExperiment)
			model->AddDirectory(dir.absolutePath(), Model::DirectoryNode::Type::HtxExperiment);
		else
			model->AddDirectory(dir.absolutePath(), Model::DirectoryNode::Type::Directory);

		for (const auto& t : tcfs)
			model->AddTcf(dir.filePath(t));
	}

	FileSystemTcfScanner::FileSystemTcfScanner(Tomocube::IServiceProvider* provider) : QObject(), IScanner(), d(new Impl) {
		d->model = std::make_shared<Model::Directories>();
		d->provider = provider;
	}

	FileSystemTcfScanner::~FileSystemTcfScanner() = default;

	auto FileSystemTcfScanner::GetDirectories() const -> const std::shared_ptr<Model::Directories>& {
		return d->model;
	}

	auto FileSystemTcfScanner::ScanPathRecursively(const QString& path) -> void {
		const auto controller = std::make_shared<Interactor::TcfScannerController>(d->provider, d->model);

		QMutexLocker locker(&d->mutex);
		if (d->stopScan.contains(path))
			d->stopScan.removeOne(path);

		const bool duplicated = std::any_of(d->scanning.cbegin(), d->scanning.cend(), [path] (const QString& s) {
			return path.contains(s);
			}
		);

		locker.unlock();

		if (!duplicated) {
			controller->StartScan(path);

			locker.relock();
			d->scanning.push_back(path);
			locker.unlock();

			if (const auto node = d->model->GetNode(path)) {
				node->GetNodes().clear();
				node->GetTcfs().clear();
			}

			d->ScanPath(path);

			QDirIterator iter(path, QDir::Filter::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
			while (iter.hasNext()) {

				locker.relock();
				if (d->stopScan.contains(path))
					break;
				locker.unlock();

				d->ScanPath(iter.next());

				controller->TryFlush();
			}

			locker.relock();
			d->scanning.removeOne(path);
			locker.unlock();
		}

		controller->FinishScan(path);
		controller->Flush();
	}

	auto FileSystemTcfScanner::AddRoot(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);
		d->model->AddRoot(path);
	}

	auto FileSystemTcfScanner::RemoveRoot(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		if (!d->stopScan.contains(path))
			d->stopScan.push_back(path);

		d->model->RemoveRoot(path);
	}
}