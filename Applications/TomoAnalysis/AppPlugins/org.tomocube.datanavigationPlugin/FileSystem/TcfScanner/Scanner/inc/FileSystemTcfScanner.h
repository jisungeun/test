#pragma once

#include "IScanner.h"
#include "IServiceProvider.h"

#include "TA.DataNav.FileSystem.TcfScanner.ScannerExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfScanner::Scanner {
	class TA_DataNav_FileSystem_TcfScanner_Scanner_API FileSystemTcfScanner final : public QObject, public Model::IScanner {
	public:
		explicit FileSystemTcfScanner(Tomocube::IServiceProvider* provider);
		~FileSystemTcfScanner() override;

		auto GetDirectories() const -> const std::shared_ptr<Model::Directories>& override;
		auto ScanPathRecursively(const QString& path) -> void override;

		auto AddRoot(const QString& path) -> void override;
		auto RemoveRoot(const QString& path) -> void override;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}