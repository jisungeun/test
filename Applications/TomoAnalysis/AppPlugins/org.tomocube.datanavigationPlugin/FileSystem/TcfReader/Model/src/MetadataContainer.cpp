#include <QMap>
#include <QDateTime>
#include <QMutexLocker>

#include "MetadataContainer.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Model {
	struct MetadataContainer::Impl {
		QMutex mutex;

		QMap<QString, QMap<QString, QString>> metadata;
		QMap<QString, QMap<QString, int>> modality;

		uint64_t updatedAt = 0;
		QStringList updatedList;

		auto AddUpdated(const QString& filename) -> void;
		auto RemoveUpdated(const QString& filename) -> void;
	};

	auto MetadataContainer::Impl::AddUpdated(const QString& filename) -> void {
		if (!updatedList.contains(filename)) {
			updatedAt = QDateTime::currentSecsSinceEpoch();
			updatedList.push_back(filename);
		}
	}

	auto MetadataContainer::Impl::RemoveUpdated(const QString& filename) -> void {
		updatedList.removeOne(filename);
	}

	MetadataContainer::MetadataContainer() : d(new Impl) {}

	MetadataContainer::~MetadataContainer() = default;

	auto MetadataContainer::Contains(const QString& filename) const -> bool {
		QMutexLocker locker(&d->mutex);
		return d->metadata.contains(filename);
	}

	auto MetadataContainer::GetMetadata(const QString& filename) const -> QMap<QString, QString> {
		QMutexLocker locker(&d->mutex);

		if (d->metadata.contains(filename))
			return d->metadata[filename];

		return {};
	}

	auto MetadataContainer::GetModality(const QString& filename) const -> QMap<QString, int> {
		QMutexLocker locker(&d->mutex);

		if (d->modality.contains(filename))
			return d->modality[filename];

		return {};
	}

	auto MetadataContainer::Update(const QString& filename, const QMap<QString, QString>& metadata, const QMap<QString, int>& modality) -> void {
		QMutexLocker locker(&d->mutex);

		d->metadata[filename] = metadata;
		d->modality[filename] = modality;

		d->AddUpdated(filename);
	}

	auto MetadataContainer::Remove(const QString& filename) -> void {
		QMutexLocker locker(&d->mutex);

		d->metadata.remove(filename);
		d->modality.remove(filename);

		d->RemoveUpdated(filename);
	}

	auto MetadataContainer::GetLastUpdatedTime() const -> uint64_t {
		QMutexLocker locker(&d->mutex);

		return d->updatedAt;
	}

	auto MetadataContainer::TakeUpdatedList() const -> QStringList {
		QMutexLocker locker(&d->mutex);

		auto temp = d->updatedList;
		d->updatedList.clear();

		return temp;
	}
}
