#include <QMap>
#include <QDateTime>

#include "ThumbnailContainer.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Model {
	struct ThumbnailContainer::Impl {
		QMap<QString, QMap<QString, QVector<QPixmap>>> thumbnails;
	};

	ThumbnailContainer::ThumbnailContainer() : d(new Impl) {}

	ThumbnailContainer::~ThumbnailContainer() = default;

	auto ThumbnailContainer::Contains(const QString& dataId, const QString& modality, int index) const -> bool {
		return d->thumbnails.contains(dataId) && d->thumbnails[dataId].contains(modality) && d->thumbnails[dataId][modality].count() > index;
	}

	auto ThumbnailContainer::GetThumbnails(const QString& dataId, const QString& modality) const -> QVector<QPixmap> {
		if (d->thumbnails.contains(dataId) && d->thumbnails[dataId].contains(modality))
			return d->thumbnails[dataId][modality];

		return {};
	}

	auto ThumbnailContainer::GetThumbnail(const QString& dataId, const QString& modality, int index) const -> std::optional<QPixmap> {
		if (d->thumbnails.contains(dataId) && d->thumbnails[dataId].contains(modality) && d->thumbnails[dataId][modality].count() > index && index >= 0)
			return d->thumbnails[dataId][modality][index];

		return {};
	}

	auto ThumbnailContainer::Update(const QString& dataId, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		d->thumbnails[dataId][modality] = thumbnails;
	}

	auto ThumbnailContainer::Update(const QString& dataId, const QString& modality, const QPixmap& image, int index) -> void {
		if (d->thumbnails[dataId][modality].count() <= index)
			d->thumbnails[dataId][modality].resize(index + 1);

		if (index >= 0)
			d->thumbnails[dataId][modality][index] = image;
	}

	auto ThumbnailContainer::Remove(const QString& dataId) -> void {
		d->thumbnails.remove(dataId);
	}

	auto ThumbnailContainer::Remove(const QString& dataId, const QString& modality) -> void {
		if (d->thumbnails.contains(dataId))
			d->thumbnails[dataId].remove(modality);
	}
}
