#pragma once

#include <memory>
#include <optional>

#include <QString>
#include <QtGui/QPixmap>

#include "TA.DataNav.FileSystem.TcfReader.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Model {
	class TA_DataNav_FileSystem_TcfReader_Model_API ThumbnailContainer {
	public:
		ThumbnailContainer();
		~ThumbnailContainer();

		auto Contains(const QString& dataId, const QString& modality, int index) const -> bool;
		auto GetThumbnails(const QString& dataId, const QString& modality) const->QVector<QPixmap>;
		auto GetThumbnail(const QString& dataId, const QString& modality, int index) const->std::optional<QPixmap>;

		auto Update(const QString& dataId, const QString& modality, const QVector<QPixmap>& thumbnails) -> void;
		auto Update(const QString& dataId, const QString& modality, const QPixmap& image, int index) -> void;
		auto Remove(const QString& dataId) -> void;
		auto Remove(const QString& dataId, const QString& modality) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}