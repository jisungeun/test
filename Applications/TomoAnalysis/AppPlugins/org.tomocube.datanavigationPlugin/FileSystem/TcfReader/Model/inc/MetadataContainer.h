#pragma once

#include <memory>

#include <QString>

#include "TA.DataNav.FileSystem.TcfReader.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Model {
	class TA_DataNav_FileSystem_TcfReader_Model_API MetadataContainer {
	public:
		MetadataContainer();
		~MetadataContainer();

		auto Contains(const QString& filename) const -> bool;
		auto GetMetadata(const QString& filename) const->QMap<QString, QString>;
		auto GetModality(const QString& filename) const->QMap<QString, int>;

		auto Update(const QString& filename, const QMap<QString, QString>& metadata, const QMap<QString, int>& modality) -> void;
		auto Remove(const QString& filename) -> void;

		auto GetLastUpdatedTime() const->uint64_t;
		auto TakeUpdatedList() const->QStringList;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}