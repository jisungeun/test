#pragma once

#include <memory>

#include "IService.h"
#include "MetadataContainer.h"
#include "ThumbnailContainer.h"

#include "TA.DataNav.FileSystem.TcfReader.ModelExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Model {
	class TA_DataNav_FileSystem_TcfReader_Model_API IReader : public Tomocube::IService {
	public:
		virtual auto GetMetadataContainer() const -> const std::shared_ptr<MetadataContainer>& = 0;
		virtual auto GetThumbnailContainer() const -> const std::shared_ptr<ThumbnailContainer>& = 0;

		virtual auto ReadMetadata(const QString& filename) -> void = 0;
		virtual auto ReadThumbnail(const QString& filename, const QString& modality) -> void = 0;
		virtual auto ReadThumbnails(const QString& filename, const QString& modality) -> void = 0;
	};
}