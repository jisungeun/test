#include <iostream>
#include "fcntl.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QSettings>

#include <H5Cpp.h>

#include "HDF5Mutex.h"
#include "MetadataReader.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	struct MetadataReader::Impl {
		QString filename;

		QMap<QString, QString> metadata;
		QMap<QString, int> modalities;

		static auto ReadDouble(const H5::Attribute& attr) -> double;
		static auto ReadString(const H5::Attribute& attr) -> QString;

		static auto ToSizeString(qint64 bytes) -> QString;
		static auto ToTimeString(qint64 seconds) -> QString;

		static auto ReadMetadata(const H5::H5File& file) -> QMap<QString, QString>;
		static auto ReadModalities(const H5::H5File& file) -> QMap<QString, int>;
	};

	auto MetadataReader::Impl::ReadDouble(const H5::Attribute& attr) -> double {
		double value;
		attr.read(H5::PredType::NATIVE_DOUBLE, &value);
		return value;
	}

	auto MetadataReader::Impl::ReadString(const H5::Attribute& attr) -> QString {
		std::string value;
		attr.read(attr.getDataType(), value);
		return QString::fromStdString(value);
	}

	auto MetadataReader::Impl::ToSizeString(qint64 bytes) -> QString {
		const auto gb = bytes / 1073741824;
		const auto mb = bytes % 1073741824 / 1048576;
		const auto kb = bytes % 1048576 / 1024;

		if (gb > 0)
			return QString("%1.%2 GB").arg(gb).arg(mb);
		else if (mb > 0)
			return QString("%1.%2 MB").arg(mb).arg(kb);
		else if (kb > 0)
			return QString("%1 KB").arg(kb);

		return QString("%1 bytes").arg(bytes);
	}

	auto MetadataReader::Impl::ToTimeString(qint64 seconds) -> QString {
		const auto hh = seconds / 3600;
		const auto mm = seconds % 3600 / 60;
		const auto ss = seconds % 60;

		return QString("%1:%2:%3").arg(hh, 2, 10, QLatin1Char('0')).arg(mm, 2, 10, QLatin1Char('0')).arg(ss, 2, 10, QLatin1Char('0'));
	}

	auto MetadataReader::Impl::ReadMetadata(const H5::H5File& file) -> QMap<QString, QString> {
		QMap<QString, QString> attr;

		attr["Data Path"] = QString::fromStdString(file.getFileName());
		attr["Data Size"] = ToSizeString(QFileInfo(attr["Data Path"]).size());

		if (file.attrExists("DataID"))
			attr["DataID"] = ReadString(file.openAttribute("DataID"));
		if (file.attrExists("Title"))
			attr["Data Title"] = ReadString(file.openAttribute("Title"));
		if (file.attrExists("RecordingTime"))
			attr["Acquisition Date"] = ReadString(file.openAttribute("RecordingTime"));
		if (file.attrExists("DeviceHost"))
			attr["Operating Workstation"] = ReadString(file.openAttribute("DeviceHost"));
		if (file.attrExists("DeviceModelType"))
			attr["Instrument Type"] = ReadString(file.openAttribute("DeviceModelType"));
		if (file.attrExists("DeviceSoftwareVersion"))
			attr["Operating Software"] = ReadString(file.openAttribute("DeviceSoftwareVersion"));
		if (file.attrExists("CreateDate"))
			attr["Processing Date"] = ReadString(file.openAttribute("CreateDate"));
		if (file.attrExists("UserID"))
			attr["User Account"] = ReadString(file.openAttribute("UserID"));

		try {
			const auto device = file.openGroup("Info/Device");

			if (device.attrExists("RI"))
				attr["Medium RI"] = QString::number(ReadDouble(device.openAttribute("RI")), 'g', 4);
		} catch (...) { }

		try {
			const auto data = file.openGroup("Data/3DFL");

			for (int i = 0; i < data.getNumObjs(); i++) {
				if (data.getObjTypeByIdx(i) == H5G_GROUP) {
					const auto channel = data.openGroup(data.getObjnameByIdx(i));
					const auto key = QString("FL Channel#%1").arg(i);
					const auto value = channel.attrExists("Name") ? ReadString(channel.openAttribute("Name")) : QString::fromStdString(data.getObjnameByIdx(i));
					const auto ex = ReadDouble(channel.openAttribute("Excitation"));
					const auto em = ReadDouble(channel.openAttribute("Emission"));

					attr[key] = QString("%1, %2 (ex) %3 (em)").arg(value).arg(ex).arg(em);
				}
			}

		} catch (...) { }

		const QDir dir(QFileInfo(attr["Data Path"]).absolutePath());

		if (QFile parent(dir.filePath(".parent")); parent.open(QIODevice::ReadOnly)) {
			const auto serial = parent.readAll();
			const auto splits = serial.split('/');

			if (splits.count() == 2) {
				attr["Project Name"] = splits[0];
				attr["Experiment Name"] = splits[1];
			}

			parent.close();
		}

		if (QFile experiment(dir.filePath(".experiment")); experiment.open(QIODevice::ReadOnly)) {
			const auto doc = QJsonDocument::fromJson(experiment.readAll());
			const auto map = doc.object().toVariantMap();

			attr["Sample Type"] = map["sampleType"].toString();
			attr["Vessel Type"] = map["vesselModel"].toString();

			if (const auto settings = map["experimentSettings"].toList(); !settings.isEmpty()) {
				const auto scenario = settings.first().toMap()["imagingScenario"].toMap();
				const auto sequences = scenario["imagingSequences"].toList();
				const auto seqTimes = scenario["seqStartTimes"].toList();

				attr["Hetero Time-lapse"] = (scenario["seqTitles"].toList().count() > 1) ? "True" : "False";

				if (!sequences.isEmpty()) {
					int count = 0;

					for (int i = 0; i < sequences.count(); i++)
						count += sequences[i].toMap()["timelapseCount"].toInt();

					attr["Time-lapse Count"] = QString::number(count);
				}

				if (!sequences.isEmpty() && !seqTimes.isEmpty()) {
					const auto seqMap = sequences.last().toMap();
					const auto firstTime = seqTimes.first().toInt();
					const auto lastTime = seqTimes.last().toInt();
					const auto lastCount = seqMap["timelapseCount"].toInt();
					const auto lastInterval = seqMap["timelapseInterval"].toInt();

					attr["Time-lapse Duration"] = ToTimeString(lastTime + ((lastCount - 1) * lastInterval) - firstTime);
				}
			}

			experiment.close();
		}

		if (const QSettings settings(dir.filePath("config.dat"), QSettings::IniFormat); settings.status() == QSettings::NoError) {
			{
				const auto x = settings.value("TileInfo/Tile_Number_X").toInt();
				const auto y = settings.value("TileInfo/Tile_Number_Y").toInt();

				if (x > 0 && y > 0)
					attr["Tile Size"] = QString("%1 x %2").arg(x).arg(y);
			}

			{
				const auto x = settings.value("AcquisitionSize/Acquisition_Size_X_Micrometer").toDouble();
				const auto y = settings.value("AcquisitionSize/Acquisition_Size_Y_Micrometer").toDouble();

				if (x > 0 && y > 0)
					attr["Imaging Area"] = QString("%1 x %2").arg(x).arg(y);
			}
		}

		if (const auto splits = QFileInfo(attr["Data Path"]).fileName().split('.'); splits.count() == 8) {
			attr["Specimen"] = splits[4];
			attr["Well"] = splits[5];
		}

		return attr;
	}

	auto MetadataReader::Impl::ReadModalities(const H5::H5File& file) -> QMap<QString, int> {
		QMap<QString, int> modalities;
		static QMap<QString, QString> dataname = {
			{ "BF", "BF" },
			{ "2DFLMIP", "FL" },
			{ "2DMIP", "HT" }
		};

		const auto group = file.openGroup("Data");

		for (int i = 0; i < group.getNumObjs(); i++) {
			const auto type = group.getObjTypeByIdx(i);

			if (type == H5G_GROUP) {
				auto name = group.getObjnameByIdx(i);
				auto qname = QString::fromStdString(name);

				if (dataname.contains(qname)) {
					auto modality = group.openGroup(name);

					if (modality.attrExists("DataCount")) {
						const auto attr = modality.openAttribute("DataCount");
						int64_t count = -1;
						attr.read(attr.getDataType(), &count);

						modalities[dataname[qname]] = count;
					}
				}
			}
		}

		return modalities;
	}

	MetadataReader::MetadataReader(const QString& filename) : d(new Impl) {
		d->filename = filename;
	}

	MetadataReader::~MetadataReader() = default;

	auto MetadataReader::Read() -> bool {
		try {
			TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
			const H5::H5File file(d->filename.toStdString(), H5F_ACC_RDONLY);

			d->metadata = d->ReadMetadata(file);
			d->modalities = d->ReadModalities(file);

			for (const auto& k : d->modalities.keys()) {
				d->metadata["Imaging Mode"].push_front(k + " ");
			}

			return true;
		} catch (...) {
			return false;
		}
	}

	auto MetadataReader::GetMetadata() const -> QMap<QString, QString> {
		return d->metadata;
	}

	auto MetadataReader::GetModalities() const -> QMap<QString, int> {
		return d->modalities;
	}
}
