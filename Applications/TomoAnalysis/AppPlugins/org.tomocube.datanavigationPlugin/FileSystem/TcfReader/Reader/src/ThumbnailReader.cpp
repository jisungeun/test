#include <iostream>
#include <optional>
#include "fcntl.h"

#include <H5Cpp.h>

#include <QPixmap>
#include <QMap>

#include "ThumbnailReader.h"

#include "HDF5Mutex.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	constexpr int thumbSize = 512;

	struct ThumbnailReader::Impl {
		QString filename;
		QString modality;
		int index = -1;
		QVector<QPixmap> thumbnails;

		static auto ReadAsUInt8(const H5::Attribute& attr) -> uint8_t;
		static auto ReadAsInt64(const H5::Attribute& attr) -> int64_t;
		static auto ReadAsDouble(const H5::Attribute& attr) -> double_t;

		auto ReadHtThumbnail(const H5::DataSet& dataset) -> QPixmap;
		auto ReadBfThumbnail(const H5::DataSet& dataset) -> QPixmap;
		auto ReadFlThumbnail(const H5::DataSet& dataset) -> QPixmap;

		auto ReadHt(const H5::Group& group, const H5::DataSet& dataset) -> QPixmap;
		auto ReadBf(const H5::Group& group, const H5::DataSet& dataset) -> QPixmap;
		auto ReadFl(const H5::Group& group, const QVector<std::tuple<H5::DataSet, QColor>>& datasets) -> QPixmap;

		auto GetDataGroupName() const -> std::string;
	};

	auto ThumbnailReader::Impl::ReadAsUInt8(const H5::Attribute& attr) -> uint8_t {
		uint8_t value;
		attr.read(attr.getDataType(), &value);
		return value;
	}

	auto ThumbnailReader::Impl::ReadAsInt64(const H5::Attribute& attr) -> int64_t {
		int64_t value;
		attr.read(attr.getDataType(), &value);
		return value;
	}

	auto ThumbnailReader::Impl::ReadAsDouble(const H5::Attribute& attr) -> double_t {
		double_t value;
		attr.read(attr.getDataType(), &value);
		return value;
	}

	auto ThumbnailReader::Impl::ReadHtThumbnail(const H5::DataSet& dataset) -> QPixmap {
		const auto sizeX = ReadAsInt64(dataset.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(dataset.openAttribute("SizeY"));
		const auto size = sizeX * sizeY;
		const auto data = std::make_unique<uint8_t[]>(size);

		dataset.read(data.get(), H5::PredType::NATIVE_UINT8);

		QImage image(data.get(), sizeX, sizeY, sizeX, QImage::Format_Grayscale8);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);
	}

	auto ThumbnailReader::Impl::ReadBfThumbnail(const H5::DataSet& dataset) -> QPixmap {
		const auto sizeX = ReadAsInt64(dataset.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(dataset.openAttribute("SizeY"));
		const auto size = sizeX * sizeY * 3;
		const auto data = std::make_unique<uint8_t[]>(sizeof(uint8_t) * size);
		const auto dataTemp = std::make_unique<uint8_t[]>(sizeof(uint8_t) * size);

		dataset.read(data.get(), H5::PredType::NATIVE_UINT8);

		// xxxx yyyy -> yyyy xxxx
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				for (auto z = 0; z < 3; ++z) {
					const auto src = x + (y * sizeX) + (z * sizeX * sizeY);
					const auto dest = y + (x * sizeY) + (z * sizeX * sizeY);

					dataTemp[dest] = data[src];
				}
			}
		}

		// rrrgggbbb -> rgb rgb rgb
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				const auto srcR = y + x * sizeY + 0 * (sizeX * sizeY);
				const auto srcG = y + x * sizeY + 1 * (sizeX * sizeY);
				const auto srcB = y + x * sizeY + 2 * (sizeX * sizeY);

				const auto destR = 3 * (x + y * sizeX) + 0;
				const auto destG = 3 * (x + y * sizeX) + 1;
				const auto destB = 3 * (x + y * sizeX) + 2;

				data[destR] = dataTemp[srcR];
				data[destG] = dataTemp[srcG];
				data[destB] = dataTemp[srcB];
			}
		}

		QImage image(data.get(), sizeX, sizeY, sizeX * 3, QImage::Format_RGB888);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);
	}

	auto ThumbnailReader::Impl::ReadFlThumbnail(const H5::DataSet& dataset) -> QPixmap {
		const auto sizeX = ReadAsInt64(dataset.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(dataset.openAttribute("SizeY"));
		const auto size = sizeX * sizeY * 3;
		const auto data = std::make_unique<uint8_t[]>(size);
		const auto dataTemp = std::make_unique<uint8_t[]>(size);

		dataset.read(data.get(), H5::PredType::NATIVE_UINT8);

		// xxxx yyyy -> yyyy xxxx
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				for (auto z = 0; z < 3; ++z) {
					const auto src = x + (y * sizeX) + (z * sizeX * sizeY);
					const auto dest = y + (x * sizeY) + (z * sizeX * sizeY);

					dataTemp[dest] = data[src];
				}
			}
		}

		// rrrgggbbb -> rgb rgb rgb
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				const auto srcR = y + x * sizeY + 0 * (sizeX * sizeY);
				const auto srcG = y + x * sizeY + 1 * (sizeX * sizeY);
				const auto srcB = y + x * sizeY + 2 * (sizeX * sizeY);

				const auto destR = 3 * (x + y * sizeX) + 0;
				const auto destG = 3 * (x + y * sizeX) + 1;
				const auto destB = 3 * (x + y * sizeX) + 2;

				data[destR] = dataTemp[srcR];
				data[destG] = dataTemp[srcG];
				data[destB] = dataTemp[srcB];
			}
		}

		QImage image(data.get(), sizeX, sizeY, sizeX * 3, QImage::Format_RGB888);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);
	}

	auto ThumbnailReader::Impl::ReadHt(const H5::Group& group, const H5::DataSet& dataset) -> QPixmap {
		const auto sizeX = ReadAsInt64(group.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(group.openAttribute("SizeY"));
		const auto size = sizeX * sizeY;
		const auto data = std::make_unique<uint16_t[]>(size);
		const auto convertedData = std::make_unique<uint8_t[]>(size);
		auto min = 0;
		auto max = UINT32_MAX;

		if (dataset.attrExists("RIMin"))
			min = static_cast<uint32_t>(ReadAsDouble(dataset.openAttribute("RIMin")) * 10000.0);
		if (dataset.attrExists("RIMax"))
			max = static_cast<uint32_t>(ReadAsDouble(dataset.openAttribute("RIMax")) * 10000.0);

		dataset.read(data.get(), H5::PredType::NATIVE_UINT16);

		for (auto i = 0; i < size; i++) {
			convertedData[i] = (data[i] - min) / static_cast<double>(max - min) * 255;
		}

		QImage image(convertedData.get(), sizeX, sizeY, sizeX, QImage::Format_Grayscale8);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);
	}

	auto ThumbnailReader::Impl::ReadBf(const H5::Group& group, const H5::DataSet& dataset) -> QPixmap {
		const auto sizeX = ReadAsInt64(group.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(group.openAttribute("SizeY"));
		const auto size = sizeX * sizeY * 3;
		const auto data = std::make_unique<uint8_t[]>(sizeof(uint8_t) * size);
		const auto dataTemp = std::make_unique<uint8_t[]>(sizeof(uint8_t) * size);

		dataset.read(data.get(), H5::PredType::NATIVE_UINT8);

		// xxxx yyyy -> yyyy xxxx
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				for (auto z = 0; z < 3; ++z) {
					const auto src = x + (y * sizeX) + (z * sizeX * sizeY);
					const auto dest = y + (x * sizeY) + (z * sizeX * sizeY);

					dataTemp[dest] = data[src];
				}
			}
		}

		// rrrgggbbb -> rgb rgb rgb
		for (auto x = 0; x < sizeX; ++x) {
			for (auto y = 0; y < sizeY; ++y) {
				const auto srcR = y + x * sizeY + 0 * (sizeX * sizeY);
				const auto srcG = y + x * sizeY + 1 * (sizeX * sizeY);
				const auto srcB = y + x * sizeY + 2 * (sizeX * sizeY);

				const auto destR = 3 * (x + y * sizeX) + 0;
				const auto destG = 3 * (x + y * sizeX) + 1;
				const auto destB = 3 * (x + y * sizeX) + 2;

				data[destR] = dataTemp[srcR];
				data[destG] = dataTemp[srcG];
				data[destB] = dataTemp[srcB];
			}
		}

		QImage image(data.get(), sizeX, sizeY, sizeX * 3, QImage::Format_RGB888);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);
	}

	auto ThumbnailReader::Impl::ReadFl(const H5::Group& group, const QVector<std::tuple<H5::DataSet, QColor>>& datasets) -> QPixmap {
		const auto sizeX = ReadAsInt64(group.openAttribute("SizeX"));
		const auto sizeY = ReadAsInt64(group.openAttribute("SizeY"));
		const auto size = sizeX * sizeY;
		const auto result = std::make_unique<uint8_t[]>(size * 3);

		for (const auto& [dataset, color] : datasets) {
			const auto data = std::make_unique<uint16_t[]>(size);
			const auto dataTemp = std::make_unique<uint8_t[]>(size);

			dataset.read(data.get(), H5::PredType::NATIVE_UINT16);

			if (dataset.attrExists("MinIntensity") && dataset.attrExists("MaxIntensity")) {
				const auto min = static_cast<uint32_t>(ReadAsDouble(dataset.openAttribute("MinIntensity")));
				const auto max = static_cast<uint32_t>(ReadAsDouble(dataset.openAttribute("MaxIntensity")));

				for (auto j = 0; j < size; j++)
					data[j] = static_cast<uint16_t>((data[j] - min) / static_cast<double>(max - min) * 255);
			}

			// xxxx yyyy -> yyyy xxxx
			for (auto x = 0; x < sizeX; ++x) {
				for (auto y = 0; y < sizeY; ++y) {
					const auto src = x + (y * sizeX);
					const auto dest = y + (x * sizeY);

					if (dest < size && src < size)
						dataTemp[dest] = static_cast<uint8_t>(data[src]);
				}
			}

			for (auto x = 0; x < sizeX; ++x) {
				for (auto y = 0; y < sizeY; ++y) {
					const auto src = y + x * sizeY;

					const auto destR = 3 * (x + y * sizeX) + 0;
					const auto destG = 3 * (x + y * sizeX) + 1;
					const auto destB = 3 * (x + y * sizeX) + 2;

					if (src < size) {
						if (destR < (size * 3))
							result[destR] = (result[destR] * ((255 - color.red()) / 255.0)) + (dataTemp[src] * (color.red() / 255.0));
						if (destG < (size * 3))
							result[destG] = (result[destG] * ((255 - color.green()) / 255.0)) + (dataTemp[src] * (color.green() / 255.0));
						if (destB < (size * 3))
							result[destB] = (result[destB] * ((255 - color.blue()) / 255.0)) + (dataTemp[src] * (color.blue() / 255.0));
					}
				}
			}
		}

		QImage image(result.get(), sizeX, sizeY, sizeX * 3, QImage::Format_RGB888);
		if (sizeX > thumbSize)
			image = image.scaledToWidth(thumbSize);
		return QPixmap::fromImage(image);

	}

	auto ThumbnailReader::Impl::GetDataGroupName() const -> std::string {
		if (modality == "HT")
			return "2DMIP";
		if (modality == "FL")
			return "2DFLMIP";
		if (modality == "BF")
			return "BF";

		return {};
	}

	ThumbnailReader::ThumbnailReader(const QString& filename, const QString& modality, int index) : d(new Impl) {
		d->filename = filename;
		d->modality = modality;
		d->index = index;
	}

	ThumbnailReader::ThumbnailReader(const QString& filename, const QString& modality) : d(new Impl) {
		d->filename = filename;
		d->modality = modality;
	}

	ThumbnailReader::~ThumbnailReader() = default;

	auto ThumbnailReader::Read() -> bool {
		try {
			TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
			const H5::H5File file(d->filename.toStdString(), H5F_ACC_RDONLY);

			if (file.exists("Thumbnail")) {
				const auto thumbGroup = file.openGroup("Thumbnail");
				const auto modalityGroup = thumbGroup.openGroup(d->modality.toStdString());

				for (hsize_t i = 0; i < modalityGroup.getNumObjs(); i++) {
					if (d->index != -1 && i != d->index)
						continue;

					if (modalityGroup.getObjTypeByIdx(i) == H5G_DATASET) {
						const auto name = modalityGroup.getObjnameByIdx(i);

						if (d->modality == "HT")
							d->thumbnails.push_back(d->ReadHtThumbnail(modalityGroup.openDataSet(name)));
						else if (d->modality == "FL")
							d->thumbnails.push_back(d->ReadFlThumbnail(modalityGroup.openDataSet(name)));
						else if (d->modality == "BF")
							d->thumbnails.push_back(d->ReadBfThumbnail(modalityGroup.openDataSet(name)));
					}
				}
			} else {
				const auto dataGroup = file.openGroup("Data");
				const auto modalityGroup = dataGroup.openGroup(d->GetDataGroupName());

				if (d->modality == "HT" || d->modality == "BF") {
					for (hsize_t i = 0; i < modalityGroup.getNumObjs(); i++) {
						if (d->index != -1 && i != d->index)
							continue;

						if (modalityGroup.getObjTypeByIdx(i) == H5G_DATASET) {
							const auto name = modalityGroup.getObjnameByIdx(i);

							if (d->modality == "HT")
								d->thumbnails.push_back(d->ReadHt(modalityGroup, modalityGroup.openDataSet(name)));
							else if (d->modality == "BF")
								d->thumbnails.push_back(d->ReadBf(modalityGroup, modalityGroup.openDataSet(name)));
						}
					}
				} else if (d->modality == "FL") {
					const auto dataCount = d->ReadAsInt64(modalityGroup.openAttribute("DataCount"));

					for (int i = 0; i < dataCount; i++) {
						if (d->index != -1 && i != d->index)
							continue;

						QVector<std::tuple<H5::DataSet, QColor>> datasets;

						for (int j = 0; j < modalityGroup.getNumObjs(); j++) {
							const auto channelName = modalityGroup.getObjnameByIdx(j);
							auto channelGroup = modalityGroup.openGroup(channelName);

							QColor color;

							if (channelGroup.attrExists("ColorB"))
								color.setBlue(d->ReadAsUInt8(channelGroup.openAttribute("ColorB")));
							else if (channelName == "CH0")
								color.setBlue(255);
							if (channelGroup.attrExists("ColorG"))
								color.setGreen(d->ReadAsUInt8(channelGroup.openAttribute("ColorG")));
							else if (channelName == "CH1")
								color.setGreen(255);
							if (channelGroup.attrExists("ColorR"))
								color.setRed(d->ReadAsUInt8(channelGroup.openAttribute("ColorR")));
							else if (channelName == "CH2")
								color.setRed(255);

							if (i < channelGroup.getNumObjs() && channelGroup.getObjTypeByIdx(i) == H5G_DATASET) {
								auto name = channelGroup.getObjnameByIdx(i);

								if (channelGroup.exists(name))
									datasets.push_back({ channelGroup.openDataSet(name), color });
							}
						}

						if (!datasets.isEmpty())
							d->thumbnails.push_back(d->ReadFl(modalityGroup, datasets));
					}
				}
			}

			return true;
		} catch (...) {
			return false;
		}
	}

	auto ThumbnailReader::GetThumbnails() const -> const QVector<QPixmap>& {
		return d->thumbnails;
	}
}
