#include <QQueue>

#include "FileSystemTcfReader.h"

#include "TcfReaderController.h"
#include "MetadataContainer.h"
#include "MetadataReader.h"
#include "ThumbnailContainer.h"
#include "ThumbnailReader.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	struct FileSystemTcfReader::Impl {
		enum class RequestType {
			Metadata,
			SingleThumbnail,
			Thumbnails
		};

		Tomocube::IServiceProvider* provider = nullptr;
		QQueue<std::tuple<RequestType, QString, QString>> queue;

		bool isRunning = false;

		std::shared_ptr<Model::MetadataContainer> metadataContainer = nullptr;
		std::shared_ptr<Model::ThumbnailContainer> thumbnailContainer = nullptr;

		auto Start() -> void;
		auto Contains(RequestType type, const QString& path, const QString& modality) const -> bool;
	};

	auto FileSystemTcfReader::Impl::Start() -> void {
		if (isRunning)
			return;
		isRunning = true;

		const auto controller = std::make_shared<Interactor::TcfReaderController>(provider, metadataContainer, thumbnailContainer);

		while (!queue.isEmpty()) {
			QString dataId;

			switch (const auto& [type, filename, modality] = queue.first(); type) {
				case RequestType::Metadata:
					if (MetadataReader reader(filename); reader.Read())
						metadataContainer->Update(filename, reader.GetMetadata(), reader.GetModalities());
					else
						metadataContainer->Update(filename, { {"Error", "Read Failed"} }, {});
					controller->TryFlushMetadata();

					break;

				case RequestType::SingleThumbnail:
					if (const auto metadata = metadataContainer->GetMetadata(filename); metadata.contains("DataID"))
						dataId = metadata["DataID"];

					if (const auto m = metadataContainer->GetModality(filename); !m.contains(modality))
						break;

					if (thumbnailContainer->Contains(dataId, modality, 0))
						controller->FlushThumbnail(filename, modality, *thumbnailContainer->GetThumbnail(dataId, modality, 0));
					else if (ThumbnailReader reader(filename, modality, 0); reader.Read() && !reader.GetThumbnails().isEmpty()) {
						thumbnailContainer->Update(dataId, modality, reader.GetThumbnails().first(), 0);
						controller->FlushThumbnail(filename, modality, reader.GetThumbnails().first());
					}

					break;

				case RequestType::Thumbnails:
					int dataCount = 0;
					if (const auto metadata = metadataContainer->GetMetadata(filename); metadata.contains("DataID"))
						dataId = metadata["DataID"];

					if (const auto m = metadataContainer->GetModality(filename); m.contains(modality))
						dataCount = m[modality];
					else
						break;

					if (thumbnailContainer->Contains(dataId, modality, dataCount))
						controller->FlushThumbnails(filename, modality, thumbnailContainer->GetThumbnails(dataId, modality));
					else if (ThumbnailReader reader(filename, modality); reader.Read() && !reader.GetThumbnails().isEmpty() && dataCount) {
						thumbnailContainer->Update(dataId, modality, reader.GetThumbnails());
						controller->FlushThumbnails(filename, modality, reader.GetThumbnails());
					}

					break;
			}

			queue.dequeue();
		}

		controller->FlushMetadata();
		isRunning = false;
	}

	auto FileSystemTcfReader::Impl::Contains(RequestType type, const QString& path, const QString& modality) const -> bool {
		return std::any_of(queue.cbegin(), queue.cend(), [type, path, modality](const std::tuple<RequestType, QString, QString>& value) {
			const auto& [t, f, m] = value;
			return type == t && f == path && m == modality;
		});
	}

	FileSystemTcfReader::FileSystemTcfReader(Tomocube::IServiceProvider* provider) : QObject(), IReader(), d(new Impl) {
		d->provider = provider;
		d->metadataContainer = std::make_shared<Model::MetadataContainer>();
		d->thumbnailContainer = std::make_shared<Model::ThumbnailContainer>();
	}

	FileSystemTcfReader::~FileSystemTcfReader() = default;

	auto FileSystemTcfReader::GetMetadataContainer() const -> const std::shared_ptr<Model::MetadataContainer>& {
		return d->metadataContainer;
	}

	auto FileSystemTcfReader::GetThumbnailContainer() const -> const std::shared_ptr<Model::ThumbnailContainer>& {
		return d->thumbnailContainer;
	}

	auto FileSystemTcfReader::ReadMetadata(const QString& filename) -> void {
		if (!d->Contains(Impl::RequestType::Metadata, filename, {}))
			d->queue.enqueue({ Impl::RequestType::Metadata, filename, {} });
		d->Start();
	}

	auto FileSystemTcfReader::ReadThumbnail(const QString& filename, const QString& modality) -> void {
		if (!d->Contains(Impl::RequestType::SingleThumbnail, filename, modality) && !d->Contains(Impl::RequestType::Thumbnails, filename, modality))
			d->queue.enqueue({ Impl::RequestType::SingleThumbnail, filename, modality });
		d->Start();
	}

	auto FileSystemTcfReader::ReadThumbnails(const QString& filename, const QString& modality) -> void {
		if (!d->Contains(Impl::RequestType::Thumbnails, filename, modality))
			d->queue.enqueue({ Impl::RequestType::Thumbnails, filename, modality });
		d->Start();
	}
}
