#pragma once

#include <memory>

#include <QObject>

#include "IReader.h"
#include "IServiceProvider.h"

#include "TA.DataNav.FileSystem.TcfReader.ReaderExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	class TA_DataNav_FileSystem_TcfReader_Reader_API FileSystemTcfReader final : public QObject, public Model::IReader {
	public:
		explicit FileSystemTcfReader(Tomocube::IServiceProvider* provider);
		~FileSystemTcfReader() override;

		auto GetMetadataContainer() const -> const std::shared_ptr<Model::MetadataContainer> & override;
		auto GetThumbnailContainer() const -> const std::shared_ptr<Model::ThumbnailContainer> & override;

		auto ReadMetadata(const QString& filename) -> void override;
		auto ReadThumbnail(const QString& filename, const QString& modality) -> void override;
		auto ReadThumbnails(const QString& filename, const QString& modality) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
