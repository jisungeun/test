#pragma once

#include <memory>

#include <QMap>

#include "TA.DataNav.FileSystem.TcfReader.ReaderExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	class TA_DataNav_FileSystem_TcfReader_Reader_API MetadataReader {
	public:
		explicit MetadataReader(const QString& filename);
		~MetadataReader();

		auto Read() -> bool;

		auto GetMetadata() const->QMap<QString, QString>;
		auto GetModalities() const->QMap<QString, int>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}