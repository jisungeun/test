#pragma once

#include <memory>

#include <QPixmap>

#include "TA.DataNav.FileSystem.TcfReader.ReaderExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Reader {
	class TA_DataNav_FileSystem_TcfReader_Reader_API ThumbnailReader {
	public:
		ThumbnailReader(const QString& filename, const QString& modality, int index);
		ThumbnailReader(const QString& filename, const QString& modality);
		~ThumbnailReader();

		auto Read() -> bool;
		auto GetThumbnails() const -> const QVector<QPixmap>&;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}