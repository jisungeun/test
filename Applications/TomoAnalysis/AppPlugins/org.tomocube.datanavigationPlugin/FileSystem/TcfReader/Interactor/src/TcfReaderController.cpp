#include <QDateTime>

#include "TcfReaderController.h"

#include "ITcfReaderInputPort.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Interactor {
	struct TcfReaderController::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		std::shared_ptr<Model::MetadataContainer> metadata = nullptr;
		std::shared_ptr<Model::ThumbnailContainer> thumbnail = nullptr;
	};

	TcfReaderController::TcfReaderController(Tomocube::IServiceProvider* provider, const std::shared_ptr<Model::MetadataContainer>& metadata, const std::shared_ptr<Model::ThumbnailContainer>& thumbnail) : QObject(), d(new Impl) {
		d->provider = provider;
		d->metadata = metadata;
		d->thumbnail = thumbnail;
	}

	TcfReaderController::~TcfReaderController() = default;

	auto TcfReaderController::FlushMetadata() -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();

		for (const auto& url : d->metadata->TakeUpdatedList()) {
			auto entity = std::make_shared<BusinessRule::Entity::TcfMetadata>(url);
			const auto metadata = d->metadata->GetMetadata(url);
			const auto modality = d->metadata->GetModality(url);

			for (const auto& m : metadata.keys())
				entity->AddMetadata(m, metadata[m]);
			for (const auto& m : modality.keys())
				entity->AddModality(m, modality[m]);

			usecase->UpdateTcf(entity);
		}
	}

	auto TcfReaderController::TryFlushMetadata() -> void {
		if (d->metadata->GetLastUpdatedTime() > 0 && QDateTime::currentSecsSinceEpoch() - d->metadata->GetLastUpdatedTime() >= 1)
			FlushMetadata();
	}

	auto TcfReaderController::FlushThumbnail(const QString& filename, const QString& modality, const QPixmap& thumbnail) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();

		usecase->UpdateThumbnail(filename, modality, thumbnail);
	}

	auto TcfReaderController::FlushThumbnails(const QString& filename, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();

		usecase->UpdateThumbnails(filename, modality, thumbnails);
	}
}
