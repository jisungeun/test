#include <QThreadPool>

#include "TcfReaderPresenter.h"

#include "ITcfReaderInputPort.h"
#include "IReader.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Interactor {
	struct TcfReaderPresenter::Impl {
		QThreadPool pool;
		Tomocube::IServiceProvider* provider = nullptr;
		std::shared_ptr<Model::IReader> reader = nullptr;

		auto GetMetadata(const QString& url) const->std::shared_ptr<BusinessRule::Entity::TcfMetadata>;
		auto GetThumbnails(const QString& dataId, const QString& modality) const->QVector<QPixmap>;
	};

	auto TcfReaderPresenter::Impl::GetMetadata(const QString& url) const -> std::shared_ptr<BusinessRule::Entity::TcfMetadata> {
		const auto& container = reader->GetMetadataContainer();
		const auto metadata = container->GetMetadata(url);
		const auto modality = container->GetModality(url);

		if (!metadata.isEmpty() && !modality.isEmpty()) {
			const auto entity = std::make_shared<BusinessRule::Entity::TcfMetadata>(url);

			for (const auto& m : metadata.keys())
				entity->AddMetadata(m, metadata[m]);
			for (const auto& m : modality.keys())
				entity->AddModality(m, modality[m]);

			return entity;
		}

		return {};
	}

	auto TcfReaderPresenter::Impl::GetThumbnails(const QString& dataId, const QString& modality) const -> QVector<QPixmap> {
		const auto container = reader->GetThumbnailContainer();
		const auto thumbnails = container->GetThumbnails(dataId, modality);

		if (!thumbnails.isEmpty())
			return thumbnails;

		return {};
	}

	TcfReaderPresenter::TcfReaderPresenter(Tomocube::IServiceProvider* provider) : ITcfReaderOutputPort(), d(new Impl) {
		d->provider = provider;
		d->reader = provider->GetService<Model::IReader>();
		d->pool.setMaxThreadCount(1);
		d->pool.waitForDone();
	}

	TcfReaderPresenter::~TcfReaderPresenter() = default;

	auto TcfReaderPresenter::GetMetadataDash(const QString& url) -> std::shared_ptr<BusinessRule::Entity::TcfMetadata> {
		return d->GetMetadata(url);
	}

	auto TcfReaderPresenter::ReadMetadata(const QString& url) -> void {
		d->pool.start([this, url] {
			if (const auto metadata = d->GetMetadata(url)) {
				const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();
				usecase->UpdateTcf(metadata);
			} else {
				d->reader->ReadMetadata(url);
			}
		});
	}

	auto TcfReaderPresenter::ReadThumbnail(const QString& url, const QString& modality) -> void {
		d->pool.start([this, url, modality] {
			QString dataId;

			if (const auto metadata = d->GetMetadata(url)) {
				dataId = metadata->GetDataId();

				if (const auto thumbnails = d->GetThumbnails(metadata->GetDataId(), modality); !thumbnails.isEmpty()) {
					const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();

					usecase->UpdateThumbnail(url, modality, thumbnails.first());
					return;
				}

				if (!metadata->GetModalities().contains(modality))
					return;
			}

			if (dataId.isEmpty())
				d->reader->ReadMetadata(url);

			d->reader->ReadThumbnail(url, modality);
		});
	}

	auto TcfReaderPresenter::ReadThumbnails(const QString& url, const QString& modality) -> void {
		d->pool.start([this, url, modality] {
			QString dataId;

			if (const auto metadata = d->GetMetadata(url)) {
				dataId = metadata->GetDataId();

				if (const auto thumbnails = d->GetThumbnails(metadata->GetDataId(), modality); !thumbnails.isEmpty() && thumbnails.count() == metadata->GetDataCount(modality)) {
					const auto usecase = d->provider->GetService<BusinessRule::IOPort::ITcfReaderInputPort>();

					usecase->UpdateThumbnails(url, modality, thumbnails);
					return;
				}

				if (!metadata->GetModalities().contains(modality))
					return;
			}

			if (dataId.isEmpty())
				d->reader->ReadMetadata(url);

			d->reader->ReadThumbnails(url, modality);
		});
	}

	auto TcfReaderPresenter::Remove(const QString& url) -> void {
		const auto& container = d->reader->GetMetadataContainer();

		container->Remove(url);
	}
}
