#pragma once

#include <QStringList>
#include <QPixmap>

#include "IServiceProvider.h"
#include "MetadataContainer.h"
#include "ThumbnailContainer.h"

#include "TA.DataNav.FileSystem.TcfReader.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Interactor {
	class TA_DataNav_FileSystem_TcfReader_Interactor_API TcfReaderController : public QObject {
	public:
		TcfReaderController(Tomocube::IServiceProvider* provider, const std::shared_ptr<Model::MetadataContainer>& metadata, const std::shared_ptr<Model::ThumbnailContainer>& thumbnail);
		~TcfReaderController() override;

		auto FlushMetadata() -> void;
		auto TryFlushMetadata() -> void;

		auto FlushThumbnail(const QString& filename, const QString& modality, const QPixmap& thumbnail) -> void;
		auto FlushThumbnails(const QString& filename, const QString& modality, const QVector<QPixmap>& thumbnails) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
