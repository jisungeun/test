#pragma once

#include "ITcfReaderOutputPort.h"
#include "IServiceProvider.h"

#include "TA.DataNav.FileSystem.TcfReader.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::FileSystem::TcfReader::Interactor {
	class TA_DataNav_FileSystem_TcfReader_Interactor_API TcfReaderPresenter final : public BusinessRule::IOPort::ITcfReaderOutputPort {
	public:
		explicit TcfReaderPresenter(Tomocube::IServiceProvider* provider);
		~TcfReaderPresenter() override;

		auto GetMetadataDash(const QString& url) -> std::shared_ptr<BusinessRule::Entity::TcfMetadata> override;

		auto ReadMetadata(const QString& url) -> void override;
		auto ReadThumbnail(const QString& url, const QString& modality) -> void override;
		auto ReadThumbnails(const QString& url, const QString& modality) -> void override;

		auto Remove(const QString& url) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
