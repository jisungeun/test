#pragma once

#include <IAppActivator.h>

#define SYMBOL "org.tomocube.datanavigationPlugin"

namespace TomoAnalysis::DataNavigation::AppUI {
	class datanavigationPlugin : public IAppActivator {
		Q_OBJECT
			Q_PLUGIN_METADATA(IID SYMBOL)

	public:
		static datanavigationPlugin* getInstance();

		datanavigationPlugin();
		~datanavigationPlugin() override;

		auto start(ctkPluginContext* context)->void override;
		auto stop(ctkPluginContext* context)->void override;
		auto getPluginContext()->ctkPluginContext* const override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;

		static datanavigationPlugin* instance;
	};
}