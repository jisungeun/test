#include <QSettings>

#include "Registry.h"

namespace TomoAnalysis::DataNavigation::WindowsRegistry {
	struct Registry::Impl {
		std::shared_ptr<QSettings> settings;
	};

	Registry::Registry() : IPreferenceOutputPort(), d(new Impl) {
		d->settings = std::make_shared<QSettings>("Tomocube", "TomoAnalysis");
	}

	Registry::~Registry() = default;

	auto Registry::SetValue(const QString& name, const QVariant& value) -> void {
		d->settings->setValue(name, value);
	}

	auto Registry::GetValue(const QString& name) -> std::optional<QString> {
		const auto value = d->settings->value(name);

		if (value.isNull())
			return {};
		return value.toString();
	}
}
