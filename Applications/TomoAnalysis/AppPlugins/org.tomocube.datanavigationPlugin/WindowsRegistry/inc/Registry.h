#pragma once

#include <memory>

#include "IPreferenceOutputPort.h"

#include "TA.DataNav.WindowsRegistryExport.h"

namespace TomoAnalysis::DataNavigation::WindowsRegistry {
	class TA_DataNav_WindowsRegistry_API Registry final : public BusinessRule::IOPort::IPreferenceOutputPort {
	public:
		Registry();
		~Registry() override;

		auto SetValue(const QString& name, const QVariant& value) -> void override;
		auto GetValue(const QString& name) -> std::optional<QString> override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}