#include "DataNavigationMeta.h"

namespace TomoAnalysis::DataNavigation::Meta {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};

	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "Standalone";
		d->appProperties["AppKey"] = "Data Manager";
	}

	AppMeta::~AppMeta() = default;

	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}