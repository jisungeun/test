#pragma once

#include <QObject>

#include "IAppMeta.h"

#include "TA.DataNav.MetaExport.h"

namespace TomoAnalysis::DataNavigation::Meta {
	class TA_DataNav_Meta_API AppMeta : public QObject, public IAppMeta {
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.datanavigationMeta")
		Q_INTERFACES(IAppMeta)

	public:
		AppMeta();
		~AppMeta() override;

		auto GetName()const->QString override { return "Data Manager"; }
		auto GetFullName()const->QString override { return "org.tomocube.datanavigationPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}