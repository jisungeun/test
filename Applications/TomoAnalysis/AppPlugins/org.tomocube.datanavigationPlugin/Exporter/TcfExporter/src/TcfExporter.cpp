#include <QCoreApplication>
#include <QDir>
#include <QQueue>
#include <QThreadPool>

#include "TcfExporter.h"


namespace TomoAnalysis::DataNavigation::Exporter {
	struct TcfExporter::Impl {
		QThreadPool pool;

		const QString exporter = QDir(QCoreApplication::applicationDirPath()).filePath("DataExporter.exe");
	};

	TcfExporter::TcfExporter() : QObject(), IExporterOutputPort(), d(new Impl) {
		d->pool.setMaxThreadCount(1);
	}

	TcfExporter::~TcfExporter() = default;

	auto TcfExporter::Export(const QString& source, const QString& target, const QString& format) -> bool {
		d->pool.start([this, source, target, format] {
			auto* proc = new QProcess;
			proc->start(d->exporter, { "-src", source, "-dest", target, "-format", format });

			connect(proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, [proc] {
				delete proc;
			});

			proc->waitForFinished(1000);
		});
		return true;
	}

	auto TcfExporter::Export(const QStringList& sources, const QString& target, const QString& format) -> bool {
		for (const auto& s : sources)
			Export(s, target, format);

		return true;
	}
}
