#pragma once

#include <QProcess>

#include "IExporterOutputPort.h"

#include "TA.DataNav.Exporter.TcfExporterExport.h"

namespace TomoAnalysis::DataNavigation::Exporter {
	class TA_DataNav_Exporter_TcfExporter_API TcfExporter : public QObject, public BusinessRule::IOPort::IExporterOutputPort {
	public:
		TcfExporter();
		~TcfExporter() override;

		auto Export(const QString& source, const QString& target, const QString& format) -> bool override;
		auto Export(const QStringList& sources, const QString& target, const QString& format) -> bool override;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}