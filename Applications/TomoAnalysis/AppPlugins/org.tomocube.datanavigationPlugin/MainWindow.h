#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::DataNavigation::AppUI {
	class MainWindow final : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		~MainWindow() override;
		
		auto GetParameter(QString name)->IParameter::Pointer override;
		auto SetParameter(QString name, IParameter::Pointer param) -> void override;
		auto GetRunType()->std::tuple<bool, bool> override;
        auto TryActivate() -> bool override;
        auto TryDeactivate() -> bool override;
        auto IsActivate() -> bool override;
        auto GetMetaInfo() -> QVariantMap override;
		auto Execute(const QVariantMap& params) -> bool override;

		auto GetFeatureName() const->QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	protected slots:
		auto OnImageViewerRequested(const QStringList& tcfList) -> void;
		auto OnCloseProgram()->void;
		void OnCtkEvent(const ctkEvent& ctkEvent);

	private:
		struct Impl;
		std::unique_ptr<Impl> d{};
	};
}
