#pragma once

#include "IServiceCollection.h"

#include "TA.DataNav.DependencyInjectionExport.h"

namespace TomoAnalysis::DataNavigation::DependencyInjection {
	class TA_DataNav_DependencyInjection_API ServiceCollection final : public BusinessRule::Framework::IServiceCollection {
	public:
		ServiceCollection();
		explicit ServiceCollection(const IServiceCollection* collection);
		~ServiceCollection() override;

		auto BuildProvider() ->BusinessRule::Framework::IServiceProvider* override;
	};
}