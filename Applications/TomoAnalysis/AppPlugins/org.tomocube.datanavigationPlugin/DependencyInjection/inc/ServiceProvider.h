#pragma once

#include "IServiceProvider.h"

#include "TA.DataNav.DependencyInjectionExport.h"

namespace TomoAnalysis::DataNavigation::DependencyInjection {
	class TA_DataNav_DependencyInjection_API ServiceProvider final : public BusinessRule::Framework::IServiceProvider {
	public:
		explicit ServiceProvider(const QMap<size_t, QVector<std::shared_ptr<BusinessRule::Framework::IServiceFactory>>>& types);
		explicit ServiceProvider(const ServiceProvider* provider);
		~ServiceProvider() override;

	private:
		auto GetServiceByTypeId(size_t typeId) -> std::shared_ptr<BusinessRule::Framework::IService> override;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}