#include "ServiceCollection.h"

#include "ServiceProvider.h"

namespace TomoAnalysis::DataNavigation::DependencyInjection {
	ServiceCollection::~ServiceCollection() = default;

	ServiceCollection::ServiceCollection() : IServiceCollection() { }

	ServiceCollection::ServiceCollection(const IServiceCollection* collection) : IServiceCollection(collection) { }

	auto ServiceCollection::BuildProvider() -> BusinessRule::Framework::IServiceProvider* {
		return new ServiceProvider(cached);
	}
}
