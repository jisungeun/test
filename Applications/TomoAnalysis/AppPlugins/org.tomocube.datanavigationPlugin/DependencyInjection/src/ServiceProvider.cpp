#include "ServiceProvider.h"

namespace TomoAnalysis::DataNavigation::DependencyInjection {
	struct ServiceProvider::Impl {
		QMap<size_t, QVector<std::shared_ptr<BusinessRule::Framework::IServiceFactory>>> types;
	};

	ServiceProvider::ServiceProvider(const QMap<size_t, QVector<std::shared_ptr<BusinessRule::Framework::IServiceFactory>>>& types) : IServiceProvider(), d(new Impl) {
		d->types = types;
	}

	ServiceProvider::ServiceProvider(const ServiceProvider* provider) : IServiceProvider(), d(new Impl) {
		d->types = provider->d->types;
	}

	ServiceProvider::~ServiceProvider() = default;

	auto ServiceProvider::GetServiceByTypeId(size_t typeId) -> std::shared_ptr<BusinessRule::Framework::IService> {
		if (d->types.contains(typeId)) {
			return d->types[typeId].first()->CreateService(this);
		}

		return {};
	}
}
