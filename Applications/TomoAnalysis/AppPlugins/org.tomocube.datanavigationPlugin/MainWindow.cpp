#include <QStandardPaths>

#include "TCLogger.h"

#include "MainWindow.h"
#include "MenuEvent.h"
#include "AppEvent.h"

#include "ServiceCollection.h"

#include "DisplayUseCase.h"
#include "TcfReaderUseCase.h"
#include "TcfScannerUseCase.h"

#include "DisplayPresenter.h"
#include "TcfReaderPresenter.h"
#include "TcfScannerPresenter.h"

#include "DataNavMainWindow.h"
#include "DisplayItemModel.h"
#include "FileSystemTcfReader.h"
#include "FileSystemTcfScanner.h"
#include "GalleryReader.h"
#include "Registry.h"
#include "SelectionModel.h"
#include "TcfExporter.h"

#include "ui_MainWindow.h"

namespace TomoAnalysis::DataNavigation::AppUI {
	using namespace BusinessRule;

	struct MainWindow::Impl {
		bool initialized = false;
		Ui::MainWindow ui{};
		QVariantMap appProperties;

		//Framework::IServiceProvider* provider = nullptr;
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
	};

	MainWindow::MainWindow(QWidget* parent) : IMainWindowTA("Data Manager", "Data Manager", parent), d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "Data Manager";
	}

	MainWindow::~MainWindow() {
		delete d->provider->GetService<IOPort::ITcfReaderOutputPort>().get();
	}

	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
		return nullptr;
	}

	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {}

	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
		return std::make_tuple(false, false);
	}

	auto MainWindow::TryActivate() -> bool {
		if (!d->initialized) {
			d->ui.setupUi(this);

			if (!d->provider) {
				Tomocube::DependencyInjection::ServiceCollection col;

				col.AddTransient<UseCase::DisplayUseCase, IOPort::IDisplayInputPort>()
					->AddTransient<UseCase::TcfReaderUseCase, IOPort::ITcfReaderInputPort>()
					->AddTransient<UseCase::TcfScannerUseCase, IOPort::ITcfScannerInputPort>()

					->AddSingleton<Display::Interactor::DisplayPresenter, IOPort::IDisplayOutputPort>()
					->AddSingleton<FileSystem::TcfReader::Interactor::TcfReaderPresenter, IOPort::ITcfReaderOutputPort>()
					->AddSingleton<FileSystem::TcfScanner::Interactor::TcfScannerPresenter, IOPort::ITcfScannerOutputPort>()
					->AddSingleton<Exporter::TcfExporter, IOPort::IExporterOutputPort>()
					->AddSingleton<WindowsRegistry::Registry, IOPort::IPreferenceOutputPort>()

					->AddSingleton<Display::Graphics::DataNavMainWindow, Display::Model::IWindow>()
					->AddSingleton<Display::Graphics::DisplayItemModel, Display::Model::IDisplayItemModel>()
					->AddSingleton<Display::Graphics::SelectionModel, Display::Model::ISelectionModel>()
					->AddSingleton<Display::Graphics::GalleryReader, Display::Model::IGalleryModel>()
					->AddSingleton<FileSystem::TcfReader::Reader::FileSystemTcfReader, FileSystem::TcfReader::Model::IReader>()
					->AddSingleton<FileSystem::TcfScanner::Scanner::FileSystemTcfScanner, FileSystem::TcfScanner::Model::IScanner>();

				d->provider = col.BuildProvider();
			}

			const auto window = d->provider->GetService<Display::Graphics::DataNavMainWindow>();
			connect(window.get(), &Display::Graphics::DataNavMainWindow::ImageViewerRequested, this, &MainWindow::OnImageViewerRequested);
			connect(window.get(), &Display::Graphics::DataNavMainWindow::CloseProgramRequested, this, &MainWindow::OnCloseProgram);
			d->ui.vboxLayout->addWidget(window.get());

			subscribeEvent("TabChange");
			subscribeEvent(TC::Framework::MenuEvent::Topic());
			connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnCtkEvent(ctkEvent)));
		}

		return true;
	}

	auto MainWindow::TryDeactivate() -> bool {
		auto ch = children();

		while (!ch.isEmpty()) {
			const auto* child = ch.takeFirst();
			delete child;
		}
						
		d->provider = nullptr;
		d->initialized = false;

		return true;
	}

	auto MainWindow::IsActivate() -> bool {
		return d->initialized;
	}

	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
	}

	auto MainWindow::Execute(const QVariantMap& params) -> bool {
		if (const auto window = d->provider->GetService<Display::Graphics::DataNavMainWindow>()) {
			auto paths = params["RootPaths"].toStringList();

			for (const auto& p : paths) {
				window->AddRoot(p);
			}

			return !paths.isEmpty();
		}

		return false;
	}

	auto MainWindow::OnCloseProgram() -> void {
		TC::Framework::MenuEvent closeProgram(TC::Framework::MenuTypeEnum::CloseProgram);

		publishSignal(closeProgram, d->appProperties["AppKey"].toString());
    }

	auto MainWindow::OnImageViewerRequested(const QStringList& tcfList) -> void {
		if (!tcfList.isEmpty()) {
			const auto temp = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

			TC::Framework::AppEvent openApp(TC::Framework::AppTypeEnum::APP_WITHOUT_ARG);

			openApp.setAppName("2D View");
			openApp.setFullName("org.tomocube.viewer2dPlugin");
			openApp.setClosable(true);

			openApp.addParameter("TCFPath", tcfList);
			openApp.addParameter("Playground", temp);
			openApp.addParameter("ExecutionType", "OpenTCF");

			publishSignal(openApp, d->appProperties["AppKey"].toString());
		}
	}
	void MainWindow::OnCtkEvent(const ctkEvent& ctkEvent) {
		using MenuType = TC::Framework::MenuTypeEnum;
		if (ctkEvent.getProperty("TabName").isValid()) {
			auto TabName = ctkEvent.getProperty("TabName").toString();
			if (TabName.compare("Data Manager") == 0) {
				TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::TitleBar);
				menuEvt.scriptSet("");
				publishSignal(menuEvt, "Data Manager");
			}
		}
	}
	auto MainWindow::GetFeatureName() const -> QString {
		return "org.tomocube.datanavigationPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}
}
