#include <QMetaObject>
#include <QGuiApplication>
#include <QMutexLocker>
#include <QThread>
#include <QStack>

#include "DisplayPresenter.h"

#include "IDisplayItemModel.h"
#include "IGalleryModel.h"
#include "ISelectionModel.h"
#include "IWindow.h"

namespace TomoAnalysis::DataNavigation::Display::Interactor {
	struct DisplayPresenter::Impl {
		QMutex mutex;
		Tomocube::IServiceProvider* provider = nullptr;

		static auto GetType(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node)->Model::DisplayItem::Type;
		static auto GetChildrenList(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node)->QStringList;
		static auto GetChildrenList(const Model::DisplayItem* item)->QStringList;
		static auto TakesAway(const QStringList& lhs, const QStringList& rhs)->QStringList;

		auto UpdateNode(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node, const std::shared_ptr<bool>& invoked) -> void;
	};

	auto DisplayPresenter::Impl::GetType(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node) -> Model::DisplayItem::Type {
		switch (node->GetType()) {
			case BusinessRule::Entity::UrlNode::Type::Directory:
				return Model::DisplayItem::Type::Directory;
			case BusinessRule::Entity::UrlNode::Type::HtxProject:
				return Model::DisplayItem::Type::HtxProject;
			case BusinessRule::Entity::UrlNode::Type::HtxExperiment:
				return Model::DisplayItem::Type::HtxExperiment;
		}

		return Model::DisplayItem::Type::Directory;
	}

	auto DisplayPresenter::Impl::GetChildrenList(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node) -> QStringList {
		QStringList children = node->GetTcfs();

		for (const auto& ch : node->GetNodes())
			children.push_back(ch->GetUrl());

		return children;
	}

	auto DisplayPresenter::Impl::GetChildrenList(const Model::DisplayItem* item) -> QStringList {
		QStringList children;

		for (const auto* ch : item->GetChildren())
			children.push_back(ch->GetUrl());

		return children;
	}

	auto DisplayPresenter::Impl::TakesAway(const QStringList& lhs, const QStringList& rhs) -> QStringList {
		QStringList result;

		for (const auto& l : lhs) {
			if (!rhs.contains(l))
				result.push_back(l);
		}

		return result;
	}

	auto DisplayPresenter::Impl::UpdateNode(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node, const std::shared_ptr<bool>& invoked) -> void {
		const auto itemModel = provider->GetService<Model::IDisplayItemModel>();
		const auto selectionModel = provider->GetService<Model::ISelectionModel>();

		for (int i = 0; i < itemModel->GetItems(node->GetUrl()).count(); i++) {
			auto* item = itemModel->GetItems(node->GetUrl())[i];
			bool updated = false;

			for (const auto& p : TakesAway(GetChildrenList(item), GetChildrenList(node))) {
				if (auto* child = item->GetChild(p)) {
					QStack<Model::DisplayItem*> stack;
					stack.push(child);

					while (!stack.isEmpty()) {
						auto* it = stack.pop();

						itemModel->RemoveItem(it);
						selectionModel->Remove(it);

						for (auto* ch : it->GetChildren())
							stack.push(ch);
					}

					item->RemoveChild(p);
				}
			}

			if (item->SetType(GetType(node)))
				updated = true;

			if(node->GetNodes().count() != item->GetChildren().count()) {
				updated = true;
			}

			for (const auto& childTcf : node->GetTcfs()) {
				if (!item->Contains(childTcf)) {
					if (auto* childItem = item->AddChild(childTcf, Model::DisplayItem::Type::ScannableTcf))
						itemModel->AddItem(childItem);

					updated = true;
				}
			}

			for (const auto& childNode : node->GetNodes()) {
				if (!item->Contains(childNode->GetUrl())) {
					if (auto* childItem = item->AddChild(childNode->GetUrl(), Model::DisplayItem::Type::Directory))
						itemModel->AddItem(childItem);

					updated = true;
				}
			}

			if (updated) {
				item->Sort();
				if (item->GetParent())
					item->GetParent()->Sort();
				itemModel->UpdateLayout(item);
			}

			for (const auto& childNode : node->GetNodes())
				UpdateNode(childNode, nullptr);
		}

		if (invoked)
			*invoked = true;
	}

	DisplayPresenter::DisplayPresenter(Tomocube::IServiceProvider* provider) : IDisplayOutputPort(), d(new Impl) {
		d->provider = provider;
	}

	DisplayPresenter::~DisplayPresenter() = default;

	auto DisplayPresenter::UpdateNode(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node) -> void {
		const auto invoked = std::make_shared<bool>(false);
		QMutexLocker locker(&d->mutex);

		QMetaObject::invokeMethod(qApp, [this, node, &invoked] {
			d->UpdateNode(node, invoked);
		});

		while (*invoked == false)
			QThread::msleep(1);
	}

	auto DisplayPresenter::UpdateTcf(const std::shared_ptr<BusinessRule::Entity::TcfMetadata>& metadata) -> void {
		const auto itemModel = d->provider->GetService<Model::IDisplayItemModel>();

		for (auto* tcf : itemModel->GetItems(metadata->GetUrl())) {
			bool updated = false;

			if (metadata->GetMetadata().isEmpty())
				updated = tcf->SetType(Model::DisplayItem::Type::ScannableTcf);
			else if (metadata->GetMetadata().contains("Error"))
				updated = tcf->SetType(Model::DisplayItem::Type::CrashedTcf);
			else if (metadata->IsTimelapse())
				updated = tcf->SetType(Model::DisplayItem::Type::TimelapseTcf);
			else
				updated = tcf->SetType(Model::DisplayItem::Type::Tcf);

			if (updated) {
				QMetaObject::invokeMethod(qApp, [itemModel, tcf] {
					itemModel->UpdateLayout(tcf);
				});
			}
		}
	}

	auto DisplayPresenter::UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void {
		QMetaObject::invokeMethod(qApp, [this, url, modality, thumbnail] {
			const auto galleryModel = d->provider->GetService<Model::IGalleryModel>();

			galleryModel->UpdateThumbnail(url, modality, thumbnail);
		});
	}

	auto DisplayPresenter::UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		QMetaObject::invokeMethod(qApp, [this, url, modality, thumbnails] {
			const auto galleryModel = d->provider->GetService<Model::IGalleryModel>();
			const auto window = d->provider->GetService<Model::IWindow>();

			galleryModel->UpdateThumbnails(url, modality, thumbnails);
			window->UpdateThumbnails(url, modality, thumbnails);
		});
	}

	auto DisplayPresenter::UpdateNodeState(const QString& url, bool busy) -> void {
		QMetaObject::invokeMethod(qApp, [this, url, busy] {
			const auto itemModel = d->provider->GetService<Model::IDisplayItemModel>();

			for (auto* item : itemModel->GetItems(url)) {
				if (item->IsRoot()) {
					item->SetBusy(busy);
					itemModel->UpdateData(item);
				}
			}
		});
	}
}
