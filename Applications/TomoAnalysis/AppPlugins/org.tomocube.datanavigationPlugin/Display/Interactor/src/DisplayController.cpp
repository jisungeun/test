#include <QProcess>
#include <QDir>
#include <QStack>
#include <QThreadPool>

#include "IWindow.h"
#include "DisplayController.h"

#include "IDisplayInputPort.h"
#include "IDisplayItemModel.h"
#include "IDisplayOutputPort.h"
#include "ISelectionModel.h"

namespace TomoAnalysis::DataNavigation::Display::Interactor {
	struct DisplayController::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QThreadPool pool;
	};

	DisplayController::DisplayController(Tomocube::IServiceProvider* provider) : d(new Impl) {
		d->provider = provider;
	}

	DisplayController::~DisplayController() = default;

	auto DisplayController::AddRootInPath(const QString& path) -> void {
		const auto itemModel = d->provider->GetService<Model::IDisplayItemModel>();

		if (itemModel->AddRoot(path)) {
			const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

			if (const auto node = usecase->AddRoot(path)) {
				d->pool.start([this, node] {
					const auto display = d->provider->GetService<BusinessRule::IOPort::IDisplayOutputPort>();

					display->UpdateNode(node);
					});
			}
		}
	}

	auto DisplayController::RefreshRoot(const QString& path) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->UpdateRoot(path);
	}

	auto DisplayController::RemoveRoot(Model::DisplayItem* item) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();
		const auto itemModel = d->provider->GetService<Model::IDisplayItemModel>();
		const auto selectionModel = d->provider->GetService<Model::ISelectionModel>();

		QStack<Model::DisplayItem*> stack;
		stack.push(item);

		while (!stack.isEmpty()) {
			auto* i = stack.pop();

			itemModel->RemoveItem(i);
			selectionModel->Remove(i);

			if (itemModel->GetItems(i->GetUrl()).count() == 1)
				usecase->RemoveTcf(i->GetUrl());

			for (auto* ch : i->GetChildren())
				stack.push(ch);
		}

		usecase->RemoveRoot(item->GetUrl());
		itemModel->RemoveRoot(item);
		selectionModel->Update();

		delete item;
	}

	auto DisplayController::SetTcfVisibility(bool value) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->SetTcfVisibility(value);
	}

	auto DisplayController::SetTimelapsePlay(bool value) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->SetTimelapsePlay(value);
	}

	auto DisplayController::SetLastRootDir(const QString& value) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->SetLastRootDir(value);
	}

	auto DisplayController::SetLastExportDir(const QString& value) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->SetLastExportDir(value);
	}

	auto DisplayController::GetTcfVisibility() const -> bool {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		return usecase->GetTcfVisibility();
	}

	auto DisplayController::GetTimelapsePlay() const -> bool {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		return usecase->GetTimelapsePlay();
	}

	auto DisplayController::GetLastRootDir() const -> QString {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		return usecase->GetLastRootDir();
	}

	auto DisplayController::GetLastExportDir() const -> QString {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		return usecase->GetLastExportDir();
	}

	auto DisplayController::IsTcfMetadataLoaded(const Model::DisplayItem* item) const -> bool {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		if (item->IsTcf())
			return usecase->GetTcfMetadata(item->GetUrl()) != nullptr;
		else if (item->IsPairedTcf())
			return usecase->GetTcfMetadata(item->GetPairedTcf()->GetUrl()) != nullptr;

		return false;
	}

	auto DisplayController::IsTcfTimelapse(const Model::DisplayItem* item) const -> bool {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();
		std::shared_ptr<BusinessRule::Entity::TcfMetadata> metadata;

		if (item->IsTcf())
			metadata = usecase->GetTcfMetadata(item->GetUrl());
		else if (item->IsPairedTcf())
			metadata = usecase->GetTcfMetadata(item->GetPairedTcf()->GetUrl());

		if (metadata)
			return metadata->IsTimelapse();

		return false;
	}

	auto DisplayController::GetTcfMetadata(const Model::DisplayItem* item) const -> QMap<QString, QString> {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		if (const auto metadata = usecase->GetTcfMetadata(item->GetUrl()))
			return metadata->GetMetadata();

		return {};
	}

	auto DisplayController::GetTcfModalities(const Model::DisplayItem* item) const -> QStringList {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		if (const auto metadata = usecase->GetTcfMetadata(item->GetUrl()))
			return metadata->GetModalities();

		return {};
	}

	auto DisplayController::GetTcfDataCount(const Model::DisplayItem* item, const QString& modality) const -> int {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();
		std::shared_ptr<BusinessRule::Entity::TcfMetadata> metadata;

		if (item->IsTcf())
			metadata = usecase->GetTcfMetadata(item->GetUrl());
		else if (item->IsPairedTcf())
			metadata = usecase->GetTcfMetadata(item->GetPairedTcf()->GetUrl());

		if (metadata)
			return metadata->GetDataCount(modality);

		return {};
	}

	auto DisplayController::ContainsModality(const Model::DisplayItem* item, const QString& modality) const -> bool {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();
		std::shared_ptr<BusinessRule::Entity::TcfMetadata> metadata;

		if (item->IsTcf())
			metadata = usecase->GetTcfMetadata(item->GetUrl());
		else if (item->IsPairedTcf())
			metadata = usecase->GetTcfMetadata(item->GetPairedTcf()->GetUrl());

		if (metadata)
			return metadata->GetModalities().contains(modality);

		return false;
	}

	auto DisplayController::RequestTcfMetadata(const Model::DisplayItem* item) const -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		if (item->IsTcf())
			usecase->ReadTcfMetadata(item->GetUrl());
		else if (item->IsPairedTcf())
			usecase->ReadTcfMetadata(item->GetPairedTcf()->GetUrl());
	}

	auto DisplayController::RequestTcfThumbnail(const Model::DisplayItem* item, const QString& modality) const -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->ReadTcfThumbnail(item->GetUrl(), modality);
	}

	auto DisplayController::RequestTcfThumbnails(const Model::DisplayItem* item, const QString& modality) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->ReadTcfThumbnails(item->GetUrl(), modality);
	}

	auto DisplayController::ExportAsRaw(const QStringList& urls, const QString& save) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->Export(urls, save, "raw");
	}

	auto DisplayController::ExportAsTiff(const QStringList& urls, const QString& save) -> void {
		const auto usecase = d->provider->GetService<BusinessRule::IOPort::IDisplayInputPort>();

		usecase->Export(urls, save, "tiff");
	}

	auto DisplayController::ShowInExplorer(const QString& path, QObject* parent) -> void {
		const QFileInfo info(path);

		if (info.exists()) {
			auto* process = new QProcess(parent);
			QObject::connect(process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), [process] {
				delete process;
				});

			if (info.isFile())
				process->start("explorer.exe", { "/select", ",", QDir::toNativeSeparators(info.absoluteFilePath()) });
			else if (info.isDir())
				process->start("explorer.exe", { QDir::toNativeSeparators(info.absoluteFilePath()) });
		}
	}
}
