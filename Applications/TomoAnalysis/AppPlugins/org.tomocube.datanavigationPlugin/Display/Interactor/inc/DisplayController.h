#pragma once

#include <QString>
#include <QPixmap>
#include <QMap>

#include "DisplayItem.h"
#include "IServiceProvider.h"

#include "TA.DataNav.Display.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::Display::Interactor {
	class TA_DataNav_Display_Interactor_API DisplayController {
	public:
		explicit DisplayController(Tomocube::IServiceProvider* provider);
		~DisplayController();

		auto AddRootInPath(const QString& path) -> void;
		auto RefreshRoot(const QString& path) -> void;
		auto RemoveRoot(Model::DisplayItem* item) -> void;

		auto SetTcfVisibility(bool value) -> void;
		auto SetTimelapsePlay(bool value) -> void;
		auto SetLastRootDir(const QString& value) -> void;
		auto SetLastExportDir(const QString& value) -> void;

		auto GetTcfVisibility() const -> bool;
		auto GetTimelapsePlay() const -> bool;
		auto GetLastRootDir() const ->QString;
		auto GetLastExportDir() const ->QString;

		auto IsTcfMetadataLoaded(const Model::DisplayItem* item) const -> bool;
		auto IsTcfTimelapse(const Model::DisplayItem* item) const -> bool;
		auto GetTcfMetadata(const Model::DisplayItem* item) const -> QMap<QString, QString>;
		auto GetTcfModalities(const Model::DisplayItem* item) const -> QStringList;
		auto GetTcfDataCount(const Model::DisplayItem* item, const QString& modality) const -> int;
		auto ContainsModality(const Model::DisplayItem* item, const QString& modality) const -> bool;

		auto RequestTcfMetadata(const Model::DisplayItem* item) const -> void;
		auto RequestTcfThumbnail(const Model::DisplayItem* item, const QString& modality) const -> void;
		auto RequestTcfThumbnails(const Model::DisplayItem* item, const QString& modality) -> void;

		auto ExportAsRaw(const QStringList& urls, const QString& save) -> void;
		auto ExportAsTiff(const QStringList& urls, const QString& save) -> void;

		static auto ShowInExplorer(const QString& path, QObject* parent) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
