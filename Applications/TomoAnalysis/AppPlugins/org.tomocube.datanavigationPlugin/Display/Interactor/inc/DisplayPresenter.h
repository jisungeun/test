#pragma once

#include "DisplayItem.h"
#include "IDisplayOutputPort.h"
#include "IServiceProvider.h"

#include "TA.DataNav.Display.InteractorExport.h"

namespace TomoAnalysis::DataNavigation::Display::Interactor {
	class TA_DataNav_Display_Interactor_API DisplayPresenter final : public BusinessRule::IOPort::IDisplayOutputPort {
	public:
		explicit DisplayPresenter(Tomocube::IServiceProvider* provider);
		~DisplayPresenter() override;

		auto UpdateNode(const std::shared_ptr<BusinessRule::Entity::UrlNode>& node) -> void override;
		auto UpdateTcf(const std::shared_ptr<BusinessRule::Entity::TcfMetadata>& metadata) -> void override;
		auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void override;
		auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void override;
		auto UpdateNodeState(const QString& url, bool busy) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
