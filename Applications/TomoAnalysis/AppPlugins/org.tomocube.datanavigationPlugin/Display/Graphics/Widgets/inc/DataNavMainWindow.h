#pragma once

#include <memory>

#include <QWidget>

#include "IServiceProvider.h"
#include "DisplayItem.h"
#include "IWindow.h"

#include "TA.DataNav.Display.Graphics.WidgetsExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_Widgets_API DataNavMainWindow final : public QWidget, public Model::IWindow {
		Q_OBJECT

	public:
		explicit DataNavMainWindow(Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~DataNavMainWindow() override;
		
		auto AddRoot(const QString& path) -> void;

		auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void override;

	signals:
		auto ImageViewerRequested(const QStringList& tcfList) -> void;
		auto CloseProgramRequested()->void;

	protected slots:
		auto OnImageViewerRequested(const QStringList& tcfList) -> void;
		auto OnDetailWindowRequested(Model::DisplayItem* item) -> void;
		auto OnDetailWindowModalityRequested(Model::DisplayItem* item, const QString& modality) -> void;
		auto OnDetailWindowFinished(int exitCode) -> void;
		auto OnCloseWindowRequested()->void;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}