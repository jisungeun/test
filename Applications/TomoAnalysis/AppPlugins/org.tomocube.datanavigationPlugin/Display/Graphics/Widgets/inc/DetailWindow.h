#pragma once

#include <memory>

#include <QDialog>

#include "DisplayController.h"
#include "DisplayItem.h"

#include "TA.DataNav.Display.Graphics.WidgetsExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_Widgets_API DetailWindow : public QDialog {
		Q_OBJECT

	public:
		DetailWindow(Model::DisplayItem* item, Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~DetailWindow() override;

		auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void;
		auto RequestThumbnails() -> void;
		auto SetModality(const QString& modality) -> void;

	protected:
		auto focusOutEvent(QFocusEvent* event) -> void override;
		auto resizeEvent(QResizeEvent* resize_event) -> void override;

	protected slots:
		auto OnItemUpdated(const Model::DisplayItem* item) -> void;

		auto OnHtBtnClicked() -> void;
		auto OnFlBtnClicked() -> void;
		auto OnBfBtnClicked() -> void;
		auto OnSliderValueChanged(int value) -> void;
		auto OnSplitterMoved(int pos, int index) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}