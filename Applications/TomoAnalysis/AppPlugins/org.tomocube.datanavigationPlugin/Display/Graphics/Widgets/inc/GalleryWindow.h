#pragma once

#include <memory>

#include <QWidget>

#include "DisplayController.h"
#include "DisplayPresenter.h"
#include "ISelectionModel.h"

#include "TA.DataNav.Display.Graphics.WidgetsExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_Widgets_API GalleryWindow final : public QWidget {
		Q_OBJECT

	public:
		explicit GalleryWindow(Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~GalleryWindow() override;

	protected:
		auto eventFilter(QObject* watched, QEvent* event) -> bool override;
		auto dragEnterEvent(QDragEnterEvent* event) -> void override;
		auto dropEvent(QDropEvent* event) -> void override;
		
	protected slots:
		auto OnSelectionChanged(const QList<Model::DisplayItem*>& items) -> void;

		auto OnThumbnailDoubleClicked(Model::DisplayItem* item) -> void;
		auto OnTooltipUpdated(const QString& text) -> void;

		auto OnOpenBtnClicked() -> void;
		auto OnSelectAllBtnClicked() -> void;
		auto OnUnselectAllBtnClicked() -> void;
		auto OnZoomOutBtnClicked() -> void;
		auto OnZoomInBtnClicked() -> void;
		auto OnShowDetailsBtnClicked() -> void;
		auto OnExplorerBtnClicked() -> void;
		auto OnTimelapseBtnToggled(bool checked) -> void;
		auto OnModalityChanged(int index) -> void;
		
		auto OnGalleryCustomContextMenuRequested(const QPoint& point) -> void;
		auto OnGalleryTooltipUpdated(const QString& text) -> void;
		auto OnExportRawBtnClicked(bool checked) -> void;
		auto OnExportTiffBtnClicked(bool checked) -> void;

	signals:
		auto ImageViewerRequested(const QStringList& tcfList) -> void;
		auto DetailWindowRequested(Model::DisplayItem* item, const QString& modality) -> void;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
