#pragma once

#include <memory>

#include <QWidget>
#include <QItemSelection>

#include "DisplayController.h"
#include "DisplayPresenter.h"
#include "ISelectionModel.h"

#include "TA.DataNav.Display.Graphics.WidgetsExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_Widgets_API ExplorerWindow : public QWidget {
		Q_OBJECT

	public:
		explicit ExplorerWindow(Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~ExplorerWindow() override;

	protected:
		auto dragEnterEvent(QDragEnterEvent* event) -> void override;
		auto dropEvent(QDropEvent* event) -> void override;

	protected slots:
		auto OnRootAdded(Model::DisplayItem* item) -> void;
		auto OnItemUpdated(Model::DisplayItem* item) -> void;
		auto OnSelectionChanged(const QList<Model::DisplayItem*>& items) -> void;

		auto OnTreeViewCustomContextMenuRequested(const QPoint& point) -> void;
		auto OnTreeViewSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) -> void;
		auto OnTreeViewItemExpanded(const QModelIndex& index) -> void;
		auto OnAddRootBtnClicked() -> void;
		auto OnRemoveRootBtnClicked() -> void;
		auto OnCollapseBtnClicked() -> void;
		auto OnShowTcfBtnClicked(bool checked) -> void;
		auto OnSearchTextChanged(const QString& text) -> void;
		auto OnShowInExplorerBtnClicked(bool checked) -> void;
		auto OnOpenImageViewerBtnClicked(bool checked) -> void;
		auto OnShowDetailsBtnClicked(bool checked) -> void;
		auto OnHomeBtnClicked()->void;
		auto OnRefreshBtnClicked() -> void;

	signals:
		auto ImageViewerRequested(const QStringList& tcfList) -> void;
		auto DetailWindowRequested(Model::DisplayItem* item) -> void;
		auto CloseRequested()->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
