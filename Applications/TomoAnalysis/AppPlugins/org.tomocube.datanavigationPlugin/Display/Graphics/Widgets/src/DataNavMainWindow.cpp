#include <QLayout>
#include <QDir>

#include "DisplayController.h"
#include "DataNavMainWindow.h"

#include "DetailWindow.h"
#include "ExplorerWindow.h"
#include "GalleryWindow.h"
#include "ISelectionModel.h"

#include "ui_DataNavMainWindow.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct DataNavMainWindow::Impl {
		Ui::DataNavMainWindow ui;

		Tomocube::IServiceProvider* provider = nullptr;

		std::shared_ptr<ExplorerWindow> explorer = nullptr;
		std::shared_ptr<GalleryWindow> gallery = nullptr;

		std::shared_ptr<Interactor::DisplayController> controller = nullptr;

		DetailWindow* detailWindow = nullptr;
	};

	DataNavMainWindow::DataNavMainWindow(Tomocube::IServiceProvider* provider, QWidget* parent) : QWidget(parent), IWindow(), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->controller = std::make_shared<Interactor::DisplayController>(provider);
		d->explorer = std::make_shared<ExplorerWindow>(provider);
		d->gallery = std::make_shared<GalleryWindow>(provider);
		d->gallery->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
		d->ui.splitter->addWidget(d->explorer.get());
		d->ui.splitter->addWidget(d->gallery.get());

		d->ui.splitter->setStretchFactor(0, 0);
		d->ui.splitter->setStretchFactor(1, 1);

		connect(d->explorer.get(), &ExplorerWindow::ImageViewerRequested, this, &DataNavMainWindow::OnImageViewerRequested);
		connect(d->explorer.get(), &ExplorerWindow::DetailWindowRequested, this, &DataNavMainWindow::OnDetailWindowRequested);
		connect(d->explorer.get(), &ExplorerWindow::CloseRequested, this, &DataNavMainWindow::OnCloseWindowRequested);
		connect(d->gallery.get(), &GalleryWindow::ImageViewerRequested, this, &DataNavMainWindow::OnImageViewerRequested);
		connect(d->gallery.get(), &GalleryWindow::DetailWindowRequested, this, &DataNavMainWindow::OnDetailWindowModalityRequested);
	}

	DataNavMainWindow::~DataNavMainWindow() = default;

	auto DataNavMainWindow::AddRoot(const QString& path) -> void {
		d->controller->AddRootInPath(QDir(path).absolutePath());
	}

	auto DataNavMainWindow::UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		if (d->detailWindow) {
			d->detailWindow->UpdateThumbnails(url, modality, thumbnails);
		}
	}

	auto DataNavMainWindow::OnImageViewerRequested(const QStringList& tcfList) -> void {
		emit ImageViewerRequested(tcfList);
	}

	auto DataNavMainWindow::OnCloseWindowRequested() -> void {
		emit CloseProgramRequested();
    }

	auto DataNavMainWindow::OnDetailWindowRequested(Model::DisplayItem* item) -> void {
		d->detailWindow = new DetailWindow(item, d->provider, this);
		d->detailWindow->RequestThumbnails();
		d->detailWindow->show();

		connect(d->detailWindow, &QDialog::finished, this, &DataNavMainWindow::OnDetailWindowFinished);
	}

	auto DataNavMainWindow::OnDetailWindowModalityRequested(Model::DisplayItem* item, const QString& modality) -> void {
		d->detailWindow = new DetailWindow(item, d->provider, this);
		d->detailWindow->RequestThumbnails();
		d->detailWindow->show();
		d->detailWindow->SetModality(modality);

		connect(d->detailWindow, &QDialog::finished, this, &DataNavMainWindow::OnDetailWindowFinished);
	}

	auto DataNavMainWindow::OnDetailWindowFinished(int exitCode) -> void {
		d->detailWindow = nullptr;
	}
}
