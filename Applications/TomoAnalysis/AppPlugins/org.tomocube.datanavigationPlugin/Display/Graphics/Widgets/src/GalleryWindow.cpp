#include <QScrollBar>
#include <QMenu>
#include <QFileDialog>
#include <QMimeData>

#include "GalleryWindow.h"

#include "DetailWindow.h"
#include "GalleryReader.h"
#include "ui_GalleryWindow.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct GalleryWindow::Impl {
		Ui::GalleryWindow ui;
		Tomocube::IServiceProvider* provider = nullptr;

		bool isSelectedOnly = false;
		bool isSelected = false;

		std::shared_ptr<Interactor::DisplayController> controller = nullptr;
		std::shared_ptr<GalleryReader> reader = nullptr;
		std::shared_ptr<Model::ISelectionModel> selectionModel = nullptr;

		auto GetCurrentModality() const->QString;
	};

	auto GalleryWindow::Impl::GetCurrentModality() const -> QString {
		switch (ui.modalityBox->currentIndex()) {
			case 0:
				return "HT";
			case 1:
				return "FL";
			case 2:
				return "BF";
		}

		return {};
	}

	GalleryWindow::GalleryWindow(Tomocube::IServiceProvider* provider, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->reader = provider->GetService<GalleryReader>();
		d->controller = std::make_shared<Interactor::DisplayController>(provider);
		d->selectionModel = provider->GetService<Model::ISelectionModel>();

		d->ui.selectBtn->setIcon(QIcon(":/DataNavigation/Icon/select_all_white.svg"));
		d->ui.unselectBtn->setIcon(QIcon(":/DataNavigation/Icon/unselect_all_white.svg"));
		d->ui.zoomInBtn->setIcon(QIcon(":/DataNavigation/Icon/zoom_in_white.svg"));
		d->ui.zoomOutBtn->setIcon(QIcon(":/DataNavigation/Icon/zoom_out_white.svg"));
		d->ui.timelapseBtn->setIcon(QIcon(":/DataNavigation/Icon/picture_long_white.svg"));

		d->ui.detailsBtn->setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/show_details_white.svg") : QIcon(":/DataNavigation/Icon/show_details_grayed.svg"));
		d->ui.detailsBtn->setEnabled(d->isSelectedOnly);
		d->ui.explorerBtn->setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/folder_search_white.svg") : QIcon(":/DataNavigation/Icon/folder_search_grayed.svg"));
		d->ui.explorerBtn->setEnabled(d->isSelectedOnly);
		d->ui.openBtn->setIcon((d->isSelected) ? QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg") : QIcon(":/DataNavigation/Icon/open_image_viewer_grayed.svg"));
		d->ui.openBtn->setEnabled(d->isSelected);

		d->ui.scrollArea->verticalScrollBar()->installEventFilter(this);
		d->ui.gallery->SetScrollArea(d->ui.scrollArea);
		d->ui.gallery->SetSpacing(5);
		d->ui.gallery->SetReader(d->reader.get());
		d->ui.timelapseBtn->setChecked(d->controller->GetTimelapsePlay());
		d->reader->SetTimelapsePlay(d->controller->GetTimelapsePlay());
		d->reader->SetModality(d->GetCurrentModality());

		connect(d->ui.openBtn, &QPushButton::clicked, this, &GalleryWindow::OnOpenBtnClicked);
		connect(d->ui.selectBtn, &QPushButton::clicked, this, &GalleryWindow::OnSelectAllBtnClicked);
		connect(d->ui.unselectBtn, &QPushButton::clicked, this, &GalleryWindow::OnUnselectAllBtnClicked);
		connect(d->ui.zoomInBtn, &QPushButton::clicked, this, &GalleryWindow::OnZoomInBtnClicked);
		connect(d->ui.zoomOutBtn, &QPushButton::clicked, this, &GalleryWindow::OnZoomOutBtnClicked);
		connect(d->ui.detailsBtn, &QPushButton::clicked, this, &GalleryWindow::OnShowDetailsBtnClicked);
		connect(d->ui.explorerBtn, &QPushButton::clicked, this, &GalleryWindow::OnExplorerBtnClicked);
		connect(d->ui.timelapseBtn, &QPushButton::toggled, this, &GalleryWindow::OnTimelapseBtnToggled);
		connect(d->ui.modalityBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GalleryWindow::OnModalityChanged);
		connect(d->ui.gallery, &QWidget::customContextMenuRequested, this, &GalleryWindow::OnGalleryCustomContextMenuRequested);
		connect(d->reader.get(), &GalleryReader::DoubleClicked, this, &GalleryWindow::OnThumbnailDoubleClicked);
		connect(d->reader.get(), &GalleryReader::TooltipUpdated, this, &GalleryWindow::OnTooltipUpdated);
		connect(d->selectionModel.get(), &Model::ISelectionModel::SelectionChanged, this, &GalleryWindow::OnSelectionChanged);
	}

	GalleryWindow::~GalleryWindow() = default;

	auto GalleryWindow::eventFilter(QObject* watched, QEvent* event) -> bool {
		if (QGuiApplication::keyboardModifiers().testFlag(Qt::ControlModifier) && event->type() == QEvent::Wheel) {
			if (dynamic_cast<QScrollBar*>(watched)) {
				if (const auto* wheel = dynamic_cast<QWheelEvent*>(event); wheel->angleDelta().y() > 0 && d->ui.gallery->GetColumnCount() > 1) {
					d->ui.gallery->SetColumnCount(d->ui.gallery->GetColumnCount() - 1);
				} else if (wheel->angleDelta().y() < 0)
					d->ui.gallery->SetColumnCount(d->ui.gallery->GetColumnCount() + 1);

				return true;
			}
		}

		return QWidget::eventFilter(watched, event);
	}

	auto GalleryWindow::dragEnterEvent(QDragEnterEvent* event) -> void {
		QWidget::dragEnterEvent(event);
		const auto* mime = event->mimeData();

		if (mime->hasUrls()) {
			auto filenames = mime->urls();

			for (const auto& f : filenames) {
				QFileInfo file(f.toLocalFile());

				if (file.isDir())
					event->accept();
				else if (file.isFile() && file.suffix().toLower() == "tcf")
					event->accept();
			}
		}
	}

	auto GalleryWindow::dropEvent(QDropEvent* event) -> void {
		QWidget::dropEvent(event);
		const auto* mime = event->mimeData();

		if (mime->hasUrls()) {
			auto filenames = mime->urls();

			for (const auto& f : filenames) {
				const auto filename = f.toLocalFile();
				const QFileInfo file(filename);

				if (file.isDir())
					d->controller->AddRootInPath(filename);
				else if (file.isFile() && file.suffix().toLower() == "tcf")
					d->controller->AddRootInPath(file.path());
			}
		}
	}

	auto GalleryWindow::OnSelectionChanged(const QList<Model::DisplayItem*>& items) -> void {
		const auto selectedTcfs = d->selectionModel->Get(Model::DisplayItem::Type::TcfType);

		d->isSelectedOnly = selectedTcfs.count() == 1;
		d->isSelected = !selectedTcfs.empty();

		if (!selectedTcfs.isEmpty()) {
			if (const auto idx = d->reader->GetIndex(selectedTcfs.last()); idx > -1)
				d->ui.gallery->ScrollTo(idx);
		}

		d->ui.detailsBtn->setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/show_details_white.svg") : QIcon(":/DataNavigation/Icon/show_details_grayed.svg"));
		d->ui.detailsBtn->setEnabled(d->isSelectedOnly);
		d->ui.explorerBtn->setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/folder_search_white.svg") : QIcon(":/DataNavigation/Icon/folder_search_grayed.svg"));
		d->ui.explorerBtn->setEnabled(d->isSelectedOnly);
		d->ui.openBtn->setIcon((d->isSelected) ? QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg") : QIcon(":/DataNavigation/Icon/open_image_viewer_grayed.svg"));
		d->ui.openBtn->setEnabled(d->isSelected);
	}

	auto GalleryWindow::OnThumbnailDoubleClicked(Model::DisplayItem* item) -> void {
		emit ImageViewerRequested({ item->GetUrl() });
	}

	auto GalleryWindow::OnTooltipUpdated(const QString& text) -> void {
		d->ui.gallery->setToolTip(text);
	}

	auto GalleryWindow::OnOpenBtnClicked() -> void {
		QStringList tcfList;

		for (const auto& i : d->selectionModel->Get(Model::DisplayItem::Type::TcfType)) {
			if (QFileInfo(i->GetUrl()).exists()) {
				tcfList.push_back(i->GetUrl());
			}
		}

		if (false == tcfList.isEmpty()) {
			emit ImageViewerRequested(tcfList);
		}
		d->selectionModel->Clear(Model::DisplayItem::Type::TcfType);
	}

	auto GalleryWindow::OnSelectAllBtnClicked() -> void {
		d->reader->SelectAll();
	}

	auto GalleryWindow::OnUnselectAllBtnClicked() -> void {
		d->reader->UnselectAll();
	}

	auto GalleryWindow::OnZoomOutBtnClicked() -> void {
		d->ui.gallery->SetColumnCount(d->ui.gallery->GetColumnCount() + 1);
	}

	auto GalleryWindow::OnZoomInBtnClicked() -> void {
		if (d->ui.gallery->GetColumnCount() > 1)
			d->ui.gallery->SetColumnCount(d->ui.gallery->GetColumnCount() - 1);
	}

	auto GalleryWindow::OnShowDetailsBtnClicked() -> void {
		const auto items = d->selectionModel->Get(Model::DisplayItem::Type::TcfType);

		if (!items.isEmpty()) {
			if (auto* item = items.last())
				emit DetailWindowRequested(item, d->GetCurrentModality());
		}
	}

	auto GalleryWindow::OnExplorerBtnClicked() -> void {
		const auto items = d->selectionModel->Get(Model::DisplayItem::Type::TcfType);

		if (!items.isEmpty()) {
			const auto& selected = items.first();

			Interactor::DisplayController::ShowInExplorer(selected->GetUrl(), this);
		}
	}

	auto GalleryWindow::OnTimelapseBtnToggled(bool checked) -> void {
		d->reader->SetTimelapsePlay(checked);
		d->controller->SetTimelapsePlay(checked);
	}

	auto GalleryWindow::OnModalityChanged(int index) -> void {
		d->reader->SetModality(d->GetCurrentModality());
	}

	auto GalleryWindow::OnGalleryCustomContextMenuRequested(const QPoint& point) -> void {
		QMenu menu(this);

		QAction details(QIcon(":/DataNavigation/Icon/show_details_white.svg"), "Show Details", &menu);
		QAction explorer(QIcon(":/DataNavigation/Icon/folder_search_white.svg"), "Show In Explorer", &menu);
		QAction open(QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg"), "Open with Image Viewer", &menu);
		QAction exportRaw("Export as Raw", &menu);
		QAction exportTiff("Export as TIFF", &menu);

		connect(&explorer, &QAction::triggered, this, &GalleryWindow::OnExplorerBtnClicked);
		connect(&details, &QAction::triggered, this, &GalleryWindow::OnShowDetailsBtnClicked);
		connect(&open, &QAction::triggered, this, &GalleryWindow::OnOpenBtnClicked);
		connect(&exportRaw, &QAction::triggered, this, &GalleryWindow::OnExportRawBtnClicked);
		connect(&exportTiff, &QAction::triggered, this, &GalleryWindow::OnExportTiffBtnClicked);

		menu.addAction(&open);
		menu.addAction(&details);
		menu.addSeparator();
		menu.addAction(&explorer);
		menu.addSeparator();
		menu.addAction(&exportRaw);
		menu.addAction(&exportTiff);

		details.setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/show_details_white.svg") : QIcon(":/DataNavigation/Icon/show_details_grayed.svg"));
		details.setEnabled(d->isSelectedOnly);
		explorer.setIcon((d->isSelectedOnly) ? QIcon(":/DataNavigation/Icon/folder_search_white.svg") : QIcon(":/DataNavigation/Icon/folder_search_grayed.svg"));
		explorer.setEnabled(d->isSelectedOnly);
		open.setIcon((d->isSelected) ? QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg") : QIcon(":/DataNavigation/Icon/open_image_viewer_grayed.svg"));
		open.setEnabled(d->isSelected);
		exportRaw.setEnabled(d->isSelected);
		exportTiff.setEnabled(d->isSelected);

		menu.exec(QCursor::pos());
	}

	auto GalleryWindow::OnGalleryTooltipUpdated(const QString& text) -> void {
		d->ui.gallery->setToolTip(text);
	}

	auto GalleryWindow::OnExportRawBtnClicked(bool checked) -> void {
		const auto lastPath = d->controller->GetLastExportDir();
		const auto path = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, "Add export folder", lastPath));
		const auto selection = d->selectionModel->Get(Model::DisplayItem::Type::TcfType);

		if (selection.isEmpty() || path.isEmpty())
			return;

		QStringList tcfs;
		for (const auto& s : selection)
			tcfs.push_back(s->GetUrl());

		d->controller->SetLastExportDir(path);
		d->controller->ExportAsRaw(tcfs, path);
	}

	auto GalleryWindow::OnExportTiffBtnClicked(bool checked) -> void {
		const auto lastPath = d->controller->GetLastExportDir();
		const auto path = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, "Add export folder", lastPath));
		const auto selection = d->selectionModel->Get(Model::DisplayItem::Type::TcfType);

		if (selection.isEmpty() || path.isEmpty())
			return;

		QStringList tcfs;
		for (const auto& s : selection)
			tcfs.push_back(s->GetUrl());

		d->controller->SetLastExportDir(path);
		d->controller->ExportAsTiff(tcfs, path);
	}
}
