#include "DetailWindow.h"
#include "IDisplayItemModel.h"

#include "ui_detailWindow.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct DetailWindow::Impl {
		Ui::DetailWindow ui;

		std::shared_ptr<Interactor::DisplayController> controller = nullptr;
		std::shared_ptr<Model::IDisplayItemModel> itemModel = nullptr;

		QString modality;
		QMap<QString, QVector<QPixmap>> thumbnails;

		Model::DisplayItem* item = nullptr;

		auto UpdateUi() -> void;
		auto UpdateMetadata(const QMap<QString, QString>& metadata) const -> void;
		auto UpdateThumbnail() -> void;
	};

	auto DetailWindow::Impl::UpdateUi() -> void {
		UpdateThumbnail();

		ui.slider->setMaximum(std::max(0, controller->GetTcfDataCount(item, modality) - 1));
		ui.sliderFrame->setVisible(ui.slider->maximum() > 0);
		ui.indexLabel->setText(QString("%0 / %1").arg(ui.slider->value() + 1).arg(ui.slider->maximum() + 1));
	}

	auto DetailWindow::Impl::UpdateMetadata(const QMap<QString, QString>& metadata) const -> void {
		int index = 0;
		ui.table->setRowCount(metadata.count());

		for (const auto& name : metadata.keys()) {
			if (name == "DataID")
				continue;

			auto* tableItem = new QTableWidgetItem(name);
			auto font = tableItem->font();
			font.setBold(true);
			tableItem->setFont(font);

			ui.table->setItem(index, 0, tableItem);
			ui.table->setItem(index, 1, new QTableWidgetItem(metadata[name]));

			index++;
		}
	}

	auto DetailWindow::Impl::UpdateThumbnail() -> void {
		if (thumbnails.contains(modality) && thumbnails[modality].count() > ui.slider->value()) {
			const auto pixmap = thumbnails[modality][ui.slider->value()];
			const auto size = std::min(ui.thumbnail->width(), ui.thumbnail->height());

			ui.thumbnail->setPixmap(pixmap.scaled({ size, size }, Qt::KeepAspectRatioByExpanding));
		} else {
			ui.thumbnail->setPixmap({});
		}
	}

	DetailWindow::DetailWindow(Model::DisplayItem* item, Tomocube::IServiceProvider* provider, QWidget* parent) : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint), d(new Impl) {
		d->ui.setupUi(this);
		d->item = item;
		
		this->raise();
		this->setFocus();

		d->controller = std::make_shared<Interactor::DisplayController>(provider);
		d->itemModel = provider->GetService<Model::IDisplayItemModel>();

		d->ui.table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
		d->ui.table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		d->ui.sliderFrame->setVisible(false);
		d->ui.displayName->setText(item->GetDisplayName());
		d->ui.thumbLoadFrame->setVisible(true);
		d->ui.errorFrame->setVisible(!item->IsReadable());

		connect(d->ui.splitter, &QSplitter::splitterMoved, this, &DetailWindow::OnSplitterMoved);
		connect(d->ui.slider, &QAbstractSlider::valueChanged, this, &DetailWindow::OnSliderValueChanged);
		connect(d->ui.HtBtn, &QPushButton::clicked, this, &DetailWindow::OnHtBtnClicked);
		connect(d->ui.FlBtn, &QPushButton::clicked, this, &DetailWindow::OnFlBtnClicked);
		connect(d->ui.BfBtn, &QPushButton::clicked, this, &DetailWindow::OnBfBtnClicked);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::LayoutUpdated, this, &DetailWindow::OnItemUpdated);
	}

	DetailWindow::~DetailWindow() = default;

	auto DetailWindow::UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		if (d->item->GetUrl() == url && thumbnails.count() == d->controller->GetTcfDataCount(d->item, modality)) {
			d->thumbnails[modality] = thumbnails;

			d->UpdateThumbnail();
		}
	}

	auto DetailWindow::RequestThumbnails() -> void {
		OnItemUpdated(d->item);
	}

	auto DetailWindow::SetModality(const QString& modality) -> void {
		d->ui.HtBtn->setChecked(false);
		d->ui.FlBtn->setChecked(false);
		d->ui.BfBtn->setChecked(false);

		if (modality == "HT")
			d->ui.HtBtn->click();
		else if (modality == "FL")
			d->ui.FlBtn->click();
		else if (modality == "BF")
			d->ui.BfBtn->click();
	}

	auto DetailWindow::focusOutEvent(QFocusEvent* event) -> void {
		QDialog::focusOutEvent(event);
		this->finished(0);
		this->close();
		this->deleteLater();
	}
	
	auto DetailWindow::resizeEvent(QResizeEvent* resize_event) -> void {
		QDialog::resizeEvent(resize_event);

		d->UpdateThumbnail();
	}

	auto DetailWindow::OnItemUpdated(const Model::DisplayItem* item) -> void {
		if (item == d->item) {
			if (const auto modalities = d->controller->GetTcfModalities(item); !modalities.isEmpty()) {
				d->ui.thumbnailFrame->setVisible(true);
				d->ui.errorFrame->setVisible(false);

				d->ui.HtBtn->setVisible(modalities.contains("HT"));
				d->ui.FlBtn->setVisible(modalities.contains("FL"));
				d->ui.BfBtn->setVisible(modalities.contains("BF"));
			} else {
				d->ui.thumbnailFrame->setVisible(false);
				d->ui.errorFrame->setVisible(true);
			}

			if (const auto metadata = d->controller->GetTcfMetadata(d->item); metadata.isEmpty()) {
				d->controller->RequestTcfMetadata(d->item);
				d->ui.thumbLoadFrame->setVisible(true);
			} else {
				d->UpdateMetadata(metadata);
				d->ui.thumbLoadFrame->setVisible(false);

				for (const auto& m : d->controller->GetTcfModalities(d->item))
					d->controller->RequestTcfThumbnails(d->item, m);
			}
		}
	}

	auto DetailWindow::OnHtBtnClicked() -> void {
		d->modality = "HT";
		d->UpdateUi();

		d->ui.HtBtn->setChecked(true);
		d->ui.FlBtn->setChecked(false);
		d->ui.BfBtn->setChecked(false);
	}

	auto DetailWindow::OnFlBtnClicked() -> void {
		d->modality = "FL";
		d->UpdateUi();

		d->ui.HtBtn->setChecked(false);
		d->ui.FlBtn->setChecked(true);
		d->ui.BfBtn->setChecked(false);
	}

	auto DetailWindow::OnBfBtnClicked() -> void {
		d->modality = "BF";
		d->UpdateUi();

		d->ui.FlBtn->setChecked(false);
		d->ui.HtBtn->setChecked(false);
		d->ui.BfBtn->setChecked(true);
	}

	auto DetailWindow::OnSliderValueChanged(int value) -> void {
		d->UpdateThumbnail();
		d->ui.indexLabel->setText(QString("%0 / %1").arg(value + 1).arg(d->ui.slider->maximum() + 1));
	}

	auto DetailWindow::OnSplitterMoved(int pos, int index) -> void {
		d->UpdateThumbnail();
	}
}