#include <QMenu>
#include <QDropEvent>
#include <QMimeData>
#include <QFileDialog>
#include <QProcess>

#include "ExplorerWindow.h"

#include "DetailWindow.h"
#include "IDisplayItemModel.h"
#include "IGalleryModel.h"
#include "IGalleryReader.h"
#include "TreeViewModel.h"

#include "ui_ExplorerWindow.h"

namespace CellAnalyzer {
    namespace UI {
        namespace Widget {
            class IGalleryReader;
        }
    }
}

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct ExplorerWindow::Impl {
		Ui::ExplorerWindow ui;

		Tomocube::IServiceProvider* provider = nullptr;
		std::shared_ptr<Interactor::DisplayController> controller = nullptr;
		std::shared_ptr<TreeViewModel> viewModel = nullptr;
		std::shared_ptr<Model::IDisplayItemModel> itemModel = nullptr;
		std::shared_ptr<Model::ISelectionModel> selectionModel = nullptr;

		bool tcfSelected = false;
		bool nodeSelected = false;
		bool rootSelected = false;

		QStringList rootUrls;
	};

	ExplorerWindow::ExplorerWindow(Tomocube::IServiceProvider* provider, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->controller = std::make_shared<Interactor::DisplayController>(provider);
		d->viewModel = std::make_shared<TreeViewModel>(provider);
		d->selectionModel = provider->GetService<Model::ISelectionModel>();
		d->itemModel = provider->GetService<Model::IDisplayItemModel>();

		d->ui.treeView->setModel(d->viewModel.get());
		d->ui.addRootBtn->setIcon(QIcon(":/DataNavigation/Icon/folder_white.svg"));
		d->ui.removeRootBtn->setIcon(QIcon(":/DataNavigation/Icon/cancel_grayed.svg"));
		d->ui.collapseBtn->setIcon(QIcon(":/DataNavigation/Icon/collapse_white.svg"));
		d->ui.showTcfBtn->setIcon(QIcon(":/DataNavigation/Icon/picture_white.svg"));
		d->ui.homeBtn->setIcon(QIcon(":/DataNavigation/Icon/home.svg"));
		d->ui.refreshBtn->setIcon(QIcon(":/DataNavigation/Icon/refresh_white.svg"));
		d->ui.lineEdit->addAction(QIcon(":/DataNavigation/Icon/search_white.svg"), QLineEdit::TrailingPosition);
		d->ui.showTcfBtn->setChecked(d->controller->GetTcfVisibility());
		d->viewModel->SetTcfVisibility(d->controller->GetTcfVisibility());

		connect(d->ui.treeView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ExplorerWindow::OnTreeViewSelectionChanged);
		connect(d->ui.treeView, &QTreeView::expanded, this, &ExplorerWindow::OnTreeViewItemExpanded);
		connect(d->ui.treeView, &QTreeView::customContextMenuRequested, this, &ExplorerWindow::OnTreeViewCustomContextMenuRequested);
		connect(d->ui.lineEdit, &QLineEdit::textChanged, this, &ExplorerWindow::OnSearchTextChanged);
		connect(d->ui.addRootBtn, &QPushButton::clicked, this, &ExplorerWindow::OnAddRootBtnClicked);
		connect(d->ui.removeRootBtn, &QPushButton::clicked, this, &ExplorerWindow::OnRemoveRootBtnClicked);
		connect(d->ui.collapseBtn, &QPushButton::clicked, this, &ExplorerWindow::OnCollapseBtnClicked);
		connect(d->ui.showTcfBtn, &QPushButton::clicked, this, &ExplorerWindow::OnShowTcfBtnClicked);
		connect(d->ui.homeBtn, &QPushButton::clicked, this, &ExplorerWindow::OnHomeBtnClicked);
		connect(d->ui.refreshBtn, &QPushButton::clicked, this, &ExplorerWindow::OnRefreshBtnClicked);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::RootAdded, this, &ExplorerWindow::OnRootAdded);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::LayoutUpdated, this, &ExplorerWindow::OnItemUpdated);
		connect(d->selectionModel.get(), &Model::ISelectionModel::SelectionChanged, this, &ExplorerWindow::OnSelectionChanged);
	}

	ExplorerWindow::~ExplorerWindow() = default;

	auto ExplorerWindow::dragEnterEvent(QDragEnterEvent* event) -> void {
		QWidget::dragEnterEvent(event);
		const auto* mime = event->mimeData();

		if (mime->hasUrls()) {
			auto filenames = mime->urls();

			for (const auto& f : filenames) {
				QFileInfo file(f.toLocalFile());

				if (file.isDir())
					event->accept();
				else if (file.isFile() && file.suffix().toLower() == "tcf")
					event->accept();
			}
		}
	}

	auto ExplorerWindow::dropEvent(QDropEvent* event) -> void {
		QWidget::dropEvent(event);
		const auto* mime = event->mimeData();

		if (mime->hasUrls()) {
			auto filenames = mime->urls();

			for (const auto& f : filenames) {
				const auto filename = f.toLocalFile();
				const QFileInfo file(filename);

				if (file.isDir())
					d->controller->AddRootInPath(filename);
				else if (file.isFile() && file.suffix().toLower() == "tcf")
					d->controller->AddRootInPath(file.path());
			}
		}
	}

	auto ExplorerWindow::OnRootAdded(Model::DisplayItem* item) -> void {
		if (item->IsRoot()) {
			d->rootUrls.append(item->GetUrl());
			d->ui.treeView->selectionModel()->select(d->viewModel->GetIndex(item), QItemSelectionModel::ClearAndSelect);
		}
	}

	auto ExplorerWindow::OnItemUpdated(Model::DisplayItem* item) -> void {
		if (item->IsDirectory() && d->ui.treeView->isExpanded(d->viewModel->GetIndex(item))) {
			for (const auto* ch : item->GetChildren())
				d->controller->RequestTcfMetadata(ch);
		} else if (item->IsTcf() && !d->controller->IsTcfMetadataLoaded(item))
			d->controller->RequestTcfMetadata(item);
	}

	auto ExplorerWindow::OnTreeViewCustomContextMenuRequested(const QPoint& point) -> void {
		QMenu menu(this);

		QAction add(QIcon(":/DataNavigation/Icon/folder_white.svg"), "Add Directory", &menu);
		QAction remove(QIcon(":/DataNavigation/Icon/cancel_white.svg"), "Remove Directory", &menu);
		QAction explorer(QIcon(":/DataNavigation/Icon/folder_search_white.svg"), "Show In Explorer", &menu);
		QAction detail(QIcon(":/DataNavigation/Icon/show_details_white.svg"), "Show Details", &menu);
		QAction open(QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg"), "Open with Image Viewer", &menu);

		connect(&add, &QAction::triggered, this, &ExplorerWindow::OnAddRootBtnClicked);
		connect(&remove, &QAction::triggered, this, &ExplorerWindow::OnRemoveRootBtnClicked);
		connect(&explorer, &QAction::triggered, this, &ExplorerWindow::OnShowInExplorerBtnClicked);
		connect(&detail, &QAction::triggered, this, &ExplorerWindow::OnShowDetailsBtnClicked);
		connect(&open, &QAction::triggered, this, &ExplorerWindow::OnOpenImageViewerBtnClicked);

		menu.addAction(&add);
		menu.addAction(&remove);
		menu.addSeparator();
		menu.addAction(&open);
		menu.addAction(&detail);
		menu.addSeparator();
		menu.addAction(&explorer);

		remove.setEnabled(d->rootSelected);
		remove.setIcon((d->rootSelected) ? QIcon(":/DataNavigation/Icon/cancel_white.svg") : QIcon(":/DataNavigation/Icon/cancel_grayed.svg"));
		detail.setEnabled(d->tcfSelected);
		detail.setIcon((d->tcfSelected) ? QIcon(":/DataNavigation/Icon/show_details_white.svg") : QIcon(":/DataNavigation/Icon/show_details_grayed.svg"));
		explorer.setEnabled(d->tcfSelected || d->nodeSelected);
		explorer.setIcon((d->tcfSelected || d->nodeSelected) ? QIcon(":/DataNavigation/Icon/folder_search_white.svg") : QIcon(":/DataNavigation/Icon/folder_search_grayed.svg"));
		open.setEnabled(d->tcfSelected);
		open.setIcon((d->tcfSelected) ? QIcon(":/DataNavigation/Icon/open_image_viewer_white.svg") : QIcon(":/DataNavigation/Icon/open_image_viewer_grayed.svg"));

		menu.exec(QCursor::pos());
	}

	auto ExplorerWindow::OnSelectionChanged(const QList<Model::DisplayItem*>& items) -> void {
		if (!items.isEmpty()) {
			if (auto* item = items.last(); item->IsTcf())
				d->ui.treeView->scrollTo(d->viewModel->GetIndex(item));
		}
	}

	auto ExplorerWindow::OnTreeViewSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) -> void {
		d->tcfSelected = false;
		d->nodeSelected = false;
		d->rootSelected = false;

		if (!selected.indexes().isEmpty()) {
			if (auto* item = static_cast<Model::DisplayItem*>(selected.indexes().last().internalPointer())) {				
				d->tcfSelected = item->IsTcf();
				d->nodeSelected = item->IsDirectory();
				d->rootSelected = item->IsRoot();

				if (item->IsDirectory()) {
					d->selectionModel->Add(item);
					d->selectionModel->Update();
				} else if (item->IsTcf()) {
					if (auto* parent = item->GetParent()) {
						if (parent->IsPairedTcf() && !parent->IsRoot())
							d->selectionModel->Add(parent->GetParent());
						else
							d->selectionModel->Add(parent);
						d->selectionModel->Update();						
					}
				}				
				OnItemUpdated(item);
			}
		} else {			
			d->selectionModel->Clear(Model::DisplayItem::Type::DirectoryType);
			d->selectionModel->Update();
		}

		d->ui.removeRootBtn->setEnabled(d->rootSelected);
		d->ui.removeRootBtn->setIcon((d->rootSelected) ? QIcon(":/DataNavigation/Icon/cancel_white.svg") : QIcon(":/DataNavigation/Icon/cancel_grayed.svg"));		
	}

	auto ExplorerWindow::OnTreeViewItemExpanded(const QModelIndex& index) -> void {
		if (auto* item = static_cast<Model::DisplayItem*>(index.internalPointer())) {
			OnItemUpdated(item);
		}
	}

	auto ExplorerWindow::OnAddRootBtnClicked() -> void {
		const auto lastPath = d->controller->GetLastRootDir();
		const auto path = QFileDialog::getExistingDirectory(this, "Add root folder", lastPath);

		if (!path.isEmpty()) {
			d->controller->SetLastRootDir(path);
			d->controller->AddRootInPath(path);
		}
	}

	auto ExplorerWindow::OnRefreshBtnClicked() -> void {
		const auto items = d->selectionModel->Get(Model::DisplayItem::Type::DirectoryType);

		if (!items.isEmpty()) {
			if (const auto& item = items.last(); item && item->IsRoot()) {
				d->controller->RefreshRoot(item->GetUrl());
			}
		}
		/*for (auto rootUrl : d->rootUrls) {
			d->controller->RefreshRoot(rootUrl);
		}*/
	}

	auto ExplorerWindow::OnRemoveRootBtnClicked() -> void {
		const auto items = d->selectionModel->Get(Model::DisplayItem::Type::DirectoryType);

		if (!items.isEmpty()) {
			if (const auto& item = items.last(); item && item->IsRoot()) {
				d->rootUrls.removeOne(item->GetUrl());
				d->ui.treeView->selectionModel()->clear();
				d->controller->RemoveRoot(item);
			}
		}
	}

	auto ExplorerWindow::OnCollapseBtnClicked() -> void {
		d->ui.treeView->collapseAll();
	}

	auto ExplorerWindow::OnShowTcfBtnClicked(bool checked) -> void {
		d->viewModel->SetTcfVisibility(checked);
		d->controller->SetTcfVisibility(checked);
	}

	auto ExplorerWindow::OnHomeBtnClicked() -> void {
		if (QProcess::startDetached(QString("%1/%2").arg(QCoreApplication::applicationDirPath()).arg("CellAnalyzer.exe"), QStringList())) {
			emit CloseRequested();
		}
	}
	
	auto ExplorerWindow::OnSearchTextChanged(const QString& text) -> void {
		const auto gallery = d->provider->GetService<Model::IGalleryModel>();

		d->viewModel->SetSearch(text.toLower());
		gallery->SetSearch(text.toLower());

	}

	auto ExplorerWindow::OnShowInExplorerBtnClicked(bool checked) -> void {
		if (const auto indexes = d->ui.treeView->selectionModel()->selectedIndexes(); !indexes.isEmpty()) {
			if (const auto* item = static_cast<Model::DisplayItem*>(indexes.last().internalPointer())) {
				Interactor::DisplayController::ShowInExplorer(item->GetUrl(), this);
			}
		}
	}

	auto ExplorerWindow::OnOpenImageViewerBtnClicked(bool checked) -> void {
		if (const auto indexes = d->ui.treeView->selectionModel()->selectedIndexes(); !indexes.isEmpty()) {
			if (const auto* item = static_cast<Model::DisplayItem*>(indexes.last().internalPointer()); item && item->IsTcf()) {
				emit ImageViewerRequested({ item->GetUrl() });
			}
		}
	}

	auto ExplorerWindow::OnShowDetailsBtnClicked(bool checked) -> void {
		if (const auto indexes = d->ui.treeView->selectionModel()->selectedIndexes(); !indexes.isEmpty()) {
			if (auto* item = static_cast<Model::DisplayItem*>(indexes.last().internalPointer()); item && item->IsTcf()) {
				emit DetailWindowRequested(item);
			}
		}
	}
}
