#include <QTimer>
#include <QPainter>
#include <QIcon>
#include <QGuiApplication>
#include <QStack>

#include <iostream>
#include "GalleryReader.h"

#include "DisplayController.h"

#include "ISelectionModel.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct GalleryReader::Impl {
		std::shared_ptr<Interactor::DisplayController> controller = nullptr;

		std::shared_ptr<Model::IDisplayItemModel> itemModel = nullptr;
		std::shared_ptr<Model::ISelectionModel> selectionModel = nullptr;

		QString modality;
		QString keyword;
		const Model::DisplayItem* parent = nullptr;
		QList<Model::DisplayItem*> tcfs;
		QMap<void*, QPixmap> thumbnails;
		QMap<void*, QVector<QPixmap>> timelapseThumbnails;

		QPixmap hoverPixmap;
		QPixmap pressionPixmap;
		QPixmap selectionPixmap;
		QPixmap timelapsePixmap;
		QPixmap noPixmap;
		QPixmap errorPixmap;

		int hoveredIndex = -1;
		int pressedIndex = -1;
		int clickedIndex = -1;

		QTimer timer;
		int timelapseIndex = 0;
		bool timelapsePlay = false;

		mutable int visibleFrom = 0;
		mutable int visibleTo = 0;
		mutable int size = 0;

		static auto DrawRectangle(int size, int opacity)->QPixmap;
		static auto DrawOverlay(int size, int opacity)->QPixmap;
		static auto DrawProgress(int index, int count, int size)->QPixmap;
		static auto DrawTimelapseIcon(int size)->QPixmap;
		static auto DrawIconPixmap(int size, const QIcon& icon)->QPixmap;
		static auto DrawText(int size, const QString& message)->QPixmap;

		auto RequestThumbnails() -> void;
		auto RequestTimelapseThumbnails() -> void;
		auto GetIndex(const QString& url) const -> int;
	};

	auto GalleryReader::Impl::DrawRectangle(int size, int opacity) -> QPixmap {
		if(size<1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		QPainter painter(&pixmap);
		painter.setPen(QPen(QColor(0, 194, 211, opacity), 10));
		painter.drawRect(0, 0, size, size);
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::DrawOverlay(int size, int opacity) -> QPixmap {
		if(size<1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		QPainter painter(&pixmap);
		painter.fillRect(0, 0, size, size, QColor(144, 144, 144, opacity));
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::DrawProgress(int index, int count, int size) -> QPixmap {
		if(size<1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		const auto pos = std::min(7, static_cast<int>(size * 0.05));

		QPainter painter(&pixmap);
		painter.setPen(QColor(48, 66, 115));
		painter.setBrush(QColor(97, 133, 232));
		painter.drawRect(pos, size - pos - 5, static_cast<int>(((index + 1) / static_cast<double>(count)) * (size - pos * 2)), 5);
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::DrawTimelapseIcon(int size) -> QPixmap {
		if (size < 1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		const auto iconSize = std::min(30, static_cast<int>(size * 0.2));
		const auto pos = std::min(7, static_cast<int>(size * 0.05));

		QPainter painter(&pixmap);
		painter.setPen(QColor(48, 66, 115));
		painter.setBrush(QColor(97, 133, 232));
		painter.drawRect(pos, pos, iconSize, iconSize);

		auto font = painter.font();
		font.setBold(true);
		font.setPixelSize(iconSize * 0.8);
		painter.setPen(QColor(255, 255, 255));
		painter.setFont(font);
		painter.drawText(pos, pos, iconSize, iconSize, Qt::AlignCenter, "T");
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::DrawIconPixmap(int size, const QIcon& icon) -> QPixmap {
		if (size < 1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		QPainter painter(&pixmap);
		painter.setPen(Qt::NoPen);
		painter.setBrush(QColor(50, 50, 50, 15));
		painter.drawRect(0, 0, size, size);

		painter.setOpacity(0.1);
		icon.paint(&painter, size * 0.25, size * 0.25, size * 0.5, size * 0.5);
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::DrawText(int size, const QString& message) -> QPixmap {
		if(size < 1) {
			QPixmap dummyPixmap(1, 1);
			return dummyPixmap;
		}
		QPixmap pixmap(size, size);
		pixmap.fill(Qt::transparent);

		QPainter painter(&pixmap);
		painter.setPen(QColor(91, 139, 151));
		auto font = painter.font();		
		font.setPixelSize(size / 5);
		painter.setFont(font);
		painter.drawText(QRect(0, 0, size, size), Qt::AlignCenter, message);
		painter.end();

		return pixmap;
	}

	auto GalleryReader::Impl::RequestThumbnails() -> void {
		for (int i = visibleFrom; i < visibleTo; i++) {
			if (i >= tcfs.count())
				break;

			if (!thumbnails.contains(tcfs[i]) && tcfs[i]->IsReadable())
				controller->RequestTcfThumbnail(tcfs[i], modality);
		}
	}

	auto GalleryReader::Impl::RequestTimelapseThumbnails() -> void {
		const auto isTimelapse = hoveredIndex > -1 && timelapseIndex > -1 && controller->IsTcfTimelapse(tcfs[hoveredIndex]);

		if (isTimelapse && (!timelapseThumbnails.contains(tcfs[hoveredIndex]) || timelapseThumbnails[tcfs[hoveredIndex]].isEmpty()) && tcfs[hoveredIndex]->IsReadable())
			controller->RequestTcfThumbnails(tcfs[hoveredIndex], modality);
	}

	auto GalleryReader::Impl::GetIndex(const QString& url) const -> int {
		for (int i = 0; i < tcfs.count(); i++) {
			if (tcfs[i]->GetUrl() == url)
				return i;
		}

		return -1;
	}

	GalleryReader::GalleryReader(Tomocube::IServiceProvider* provider) : IGalleryReader(), IGalleryModel(), d(new Impl) {
		d->controller = std::make_shared<Interactor::DisplayController>(provider);
		d->itemModel = provider->GetService<Model::IDisplayItemModel>();
		d->selectionModel = provider->GetService<Model::ISelectionModel>();
		d->timer.setInterval(500);

		connect(&d->timer, &QTimer::timeout, this, &GalleryReader::OnTimerTimeOut);
		connect(d->selectionModel.get(), &Model::ISelectionModel::SelectionChanged, this, &GalleryReader::OnSelectionChanged);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::RootRemoved, this, &GalleryReader::OnRootRemoved);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::LayoutUpdated, this, &GalleryReader::OnItemUpdated);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::ItemRemoved, this, &GalleryReader::OnItemRemoved);
	}

	GalleryReader::~GalleryReader() = default;

	auto GalleryReader::GetIndex(const Model::DisplayItem* item) const -> int {
		for (int i = 0; i < d->tcfs.count(); i++) {
			if (d->tcfs[i] == item)
				return i;
		}

		return -1;
	}

	auto GalleryReader::SetModality(const QString& modality) -> void {
		d->modality = modality;

		d->thumbnails.clear();
		d->timelapseThumbnails.clear();
		emit Updated();

		d->noPixmap = d->DrawText(d->size, QString("No %1").arg(d->modality));

		d->RequestThumbnails();
	}

	auto GalleryReader::SetTimelapsePlay(bool timelapse) -> void {
		d->timelapsePlay = timelapse;
	}

	auto GalleryReader::SelectAll() -> void {
		for (auto* tcf : d->tcfs)
			d->selectionModel->Add(tcf);

		d->selectionModel->Update();
	}

	auto GalleryReader::UnselectAll() -> void {
		for (auto* tcf : d->tcfs)
			d->selectionModel->Remove(tcf);

		d->selectionModel->Update();
	}

	auto GalleryReader::UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void {
		if (d->modality == modality && !thumbnail.isNull()) {
			if (const auto idx = d->GetIndex(url); idx > -1)
				d->thumbnails[d->tcfs[idx]] = thumbnail;

			emit Updated();
		}
	}

	auto GalleryReader::UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void {
		if (d->modality == modality) {
			if (const auto idx = d->GetIndex(url); idx > -1)
				d->timelapseThumbnails[d->tcfs[idx]] = thumbnails;

			emit Updated();
		}
	}

	auto GalleryReader::SetSearch(const QString& keyword) -> void {
		d->keyword = keyword;
		OnItemUpdated(d->parent);
	}

	auto GalleryReader::GetImageCount() const -> int {
		return d->tcfs.count();
	}

	auto GalleryReader::GetLayerCount() const -> int {
		return 5;
	}

	auto GalleryReader::IsLoaded(int index) const -> bool {
		if (d->tcfs.count() <= index)
			return false;

		const auto isTimelapse = d->hoveredIndex == index && d->timelapseIndex > -1 && d->controller->IsTcfTimelapse(d->tcfs[index]);
		const auto isCrashed = d->tcfs[index]->GetType() == Model::DisplayItem::Type::CrashedTcf;
		const auto modalityValid = d->controller->ContainsModality(d->tcfs[index], d->modality);
		const auto metadataLoaded = d->controller->IsTcfMetadataLoaded(d->tcfs[index]);
		const auto timelapseThumbnailLoaded = d->timelapseThumbnails.contains(d->tcfs[index]) && d->timelapseThumbnails[d->tcfs[index]].count() > d->timelapseIndex && d->timelapseIndex > -1;

		if (isCrashed)
			return true;

		if (isTimelapse && modalityValid && !timelapseThumbnailLoaded)
			return false;

		return metadataLoaded;
	}

	auto GalleryReader::IsLayerLoaded(int imageIndex, int layerIndex) const -> bool {
		if (d->tcfs.count() <= imageIndex)
			return false;

		const auto hovered = d->hoveredIndex == imageIndex;
		const auto pressed = d->pressedIndex == imageIndex;
		const auto selected = d->selectionModel->Contains(d->tcfs[imageIndex]);
		const auto isTimelapse = d->controller->IsTcfTimelapse(d->tcfs[imageIndex]);
		const auto modalityValid = d->controller->ContainsModality(d->tcfs[imageIndex], d->modality);
		const auto timelapseThumbnailLoaded = d->timelapseThumbnails.contains(d->tcfs[imageIndex]) && d->timelapseThumbnails[d->tcfs[imageIndex]].count() > d->timelapseIndex && d->timelapseIndex > -1;

		switch (layerIndex) {
			case 0:
				return hovered && d->pressedIndex == -1;
			case 1:
				return pressed;
			case 2:
				return selected;
			case 3:
				return isTimelapse;
			case 4:
				return modalityValid && timelapseThumbnailLoaded && hovered && d->timelapseIndex > -1;
		}

		return false;
	}

	auto GalleryReader::GetImage(int index, int size) const -> QPixmap {
		if (d->tcfs.count() <= index)
			return {};

		const auto isTimelapse = d->hoveredIndex == index && d->controller->IsTcfTimelapse(d->tcfs[index]) && d->timelapseIndex > -1;
		const auto isCrashed = d->tcfs[index]->GetType() == Model::DisplayItem::Type::CrashedTcf;
		const auto modalityValid = d->controller->ContainsModality(d->tcfs[index], d->modality);
		const auto thumbnailLoaded = d->thumbnails.contains(d->tcfs[index]) && !d->thumbnails[d->tcfs[index]].isNull();
		const auto timelapseThumbnailLoaded = d->timelapseThumbnails.contains(d->tcfs[index]) && d->timelapseThumbnails[d->tcfs[index]].count() > d->timelapseIndex;

		if (isCrashed)
			return d->errorPixmap;

		if (isTimelapse && modalityValid) {
			if (!timelapseThumbnailLoaded)
				return {};

			return d->timelapseThumbnails[d->tcfs[index]][d->timelapseIndex];
		}

		if (thumbnailLoaded)
			return d->thumbnails[d->tcfs[index]];

		if (!modalityValid)
			return d->noPixmap;

		return {};
	}

	auto GalleryReader::GetLayerImage(int imageIndex, int layerIndex, int size) const -> QPixmap {
		switch (layerIndex) {
			case 0:
				return d->hoverPixmap;
			case 1:
				return d->pressionPixmap;
			case 2:
				return d->selectionPixmap;
			case 3:
				return d->timelapsePixmap;
			case 4:
				if (d->tcfs.count() > d->hoveredIndex && d->hoveredIndex > -1 && d->controller->IsTcfTimelapse(d->tcfs[d->hoveredIndex]))
					return d->DrawProgress(d->timelapseIndex, d->controller->GetTcfDataCount(d->tcfs[d->hoveredIndex], d->modality), size);
				return {};
		}

		return {};
	}

	void GalleryReader::OnMouseHovered(int index, bool hovered) {
		IGalleryReader::OnMouseHovered(index, hovered);

		if (hovered) {
			d->hoveredIndex = index;

			if (d->timelapsePlay && d->controller->IsTcfTimelapse(d->tcfs[index])) {
				d->timelapseIndex = 0;
				d->timer.start();
				OnTimerTimeOut();

				if (!d->timelapseThumbnails.contains(d->tcfs[index]) || d->timelapseThumbnails[d->tcfs[index]].isEmpty())
					d->controller->RequestTcfThumbnails(d->tcfs[index], d->modality);
			}

			emit TooltipUpdated(d->tcfs[index]->GetDisplayName());
		} else {
			if (d->hoveredIndex == index)
				d->hoveredIndex = -1;

			d->timelapseIndex = -1;
			d->timer.stop();

			emit TooltipUpdated({});
		}

		emit Updated();
	}

	void GalleryReader::OnMousePressed(int index, bool pressed, QMouseEvent* event) {
		IGalleryReader::OnMousePressed(index, pressed, event);

		if (pressed)
			d->pressedIndex = index;
		else
			d->pressedIndex = -1;

		emit Updated();
	}

	void GalleryReader::OnClicked(int index, QMouseEvent* event) {
		IGalleryReader::OnClicked(index, event);

		if ((event->button() == Qt::LeftButton && (!QGuiApplication::keyboardModifiers().testFlag(Qt::ControlModifier) && !QGuiApplication::keyboardModifiers().testFlag(Qt::ShiftModifier))) || (event->button() == Qt::RightButton && !d->selectionModel->Contains(d->tcfs[index]))) {
			d->selectionModel->Clear(Model::DisplayItem::Type::TcfType);
			d->selectionModel->Update();
		}

		if (QGuiApplication::keyboardModifiers().testFlag(Qt::ShiftModifier) && d->clickedIndex > -1) {
			for (int i = std::min(d->clickedIndex, index); i <= std::max(d->clickedIndex, index); i++) {
				if (!d->selectionModel->Contains(d->tcfs[i])) {
					d->selectionModel->Add(d->tcfs[i]);
					d->selectionModel->Update();
				}
			}
		} else if (event->button() == Qt::LeftButton || (event->button() == Qt::RightButton && !d->selectionModel->Contains(d->tcfs[index]))) {
			if (d->selectionModel->Contains(d->tcfs[index]))
				d->selectionModel->Remove(d->tcfs[index]);
			else
				d->selectionModel->Add(d->tcfs[index]);

			d->selectionModel->Update();
		}

		d->clickedIndex = index;

		emit Updated();
	}

	void GalleryReader::OnDoubleClicked(int index) {
		IGalleryReader::OnDoubleClicked(index);

		if (d->tcfs.count() > index && index > -1)
			emit DoubleClicked(d->tcfs[index]);
	}

	void GalleryReader::OnVisibleIndexChanged(int from, int to) {
		d->visibleFrom = from;
		d->visibleTo = to;

		d->RequestThumbnails();
	}

	void GalleryReader::OnItemResized(int size) {
		d->size = size;

		d->timelapsePixmap = d->DrawTimelapseIcon(size);
		d->hoverPixmap = d->DrawOverlay(size, 30);
		d->pressionPixmap = d->DrawOverlay(size, 90);
		d->selectionPixmap = d->DrawRectangle(size, 150);
		d->noPixmap = d->DrawText(size, QString("No %1").arg(d->modality));
		d->errorPixmap = d->DrawIconPixmap(size, QIcon(":/DataNavigation/Icon/error_white.svg"));
	}

	auto GalleryReader::OnTimerTimeOut() -> void {
		if (d->hoveredIndex < 0) {
			d->timer.stop();
			d->timelapseIndex = -1;
			return;
		}

		if (d->controller->GetTcfDataCount(d->tcfs[d->hoveredIndex], d->modality) <= ++d->timelapseIndex)
			d->timelapseIndex = 0;

		emit Updated();
	}

	auto GalleryReader::OnSelectionChanged(const QList<Model::DisplayItem*>& selection) -> void {
		if (!selection.isEmpty()) {
			const auto* s = selection.last();

			if (s->IsDirectory() && d->parent != s) {
				d->parent = s;
				OnItemUpdated(s);
			} else
				emit Updated();
		} else {
			d->parent = nullptr;

			d->tcfs.clear();
			d->thumbnails.clear();
			d->timelapseThumbnails.clear();

			emit ImageCountChanged();
			emit Updated();
		}
	}

	auto GalleryReader::OnRootRemoved(const Model::DisplayItem* item) -> void {
		if (item == d->parent) {
			d->tcfs.clear();
			d->thumbnails.clear();
			d->timelapseThumbnails.clear();
		}
	}

	auto GalleryReader::OnItemUpdated(const Model::DisplayItem* item) -> void {
		const Model::DisplayItem* parent = item;

		while (true) {
			if (parent == d->parent)
				break;

			if (!parent->GetParent())
				return;

			parent = parent->GetParent();
		}

		if (parent) {
			d->tcfs.clear();

			QStack<const Model::DisplayItem*> stack;
			stack.push(parent);

			while (!stack.isEmpty()) {
				QStack<const Model::DisplayItem*> currentStack;
				const auto* i = stack.pop();

				for (auto* ch : i->GetChildren()) {
					if ((d->keyword.isEmpty() || ch->GetDisplayName().toLower().contains(d->keyword)) && ch->IsPairedTcf())
						d->tcfs.push_back(ch->GetPairedTcf());
					else if ((d->keyword.isEmpty() || ch->GetDisplayName().toLower().contains(d->keyword)) && ch->IsTcf())
						d->tcfs.push_back(ch);
					else if (ch->ContainsTcf())
						currentStack.push(ch);
				}

				while (!currentStack.isEmpty())
					stack.push(currentStack.pop());
			}

			d->RequestThumbnails();
			d->RequestTimelapseThumbnails();

			emit ImageCountChanged();
			emit Updated();
		}
	}

	auto GalleryReader::OnItemRemoved(const Model::DisplayItem* item) -> void {
		if (d->parent == item) {
			d->parent = nullptr;

			d->tcfs.clear();
			d->thumbnails.clear();
			d->timelapseThumbnails.clear();

			emit ImageCountChanged();
			emit Updated();
		} else
			OnItemUpdated(item);
	}
}
