#pragma once

#include <memory>

#include "IDisplayItemModel.h"
#include "IGalleryModel.h"
#include "IGalleryReader.h"
#include "IServiceProvider.h"

#include "TA.DataNav.Display.Graphics.GalleryReaderExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_GalleryReader_API GalleryReader final : public Components::IGalleryReader, public Model::IGalleryModel {
		Q_OBJECT

	public:
		explicit GalleryReader(Tomocube::IServiceProvider* provider);
		~GalleryReader() override;

		auto GetIndex(const Model::DisplayItem* item) const -> int;

		auto SetModality(const QString& modality) -> void;
		auto SetTimelapsePlay(bool timelapse) -> void;

		auto SelectAll() -> void;
		auto UnselectAll() -> void;

		auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void override;
		auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void override;
		auto SetSearch(const QString& keyword) -> void override;

		auto GetImageCount() const -> int override;
		auto GetLayerCount() const -> int override;

		auto IsLoaded(int index) const -> bool override;
		auto IsLayerLoaded(int imageIndex, int layerIndex) const -> bool override;

		auto GetImage(int index, int size) const->QPixmap override;
		auto GetLayerImage(int imageIndex, int layerIndex, int size) const->QPixmap override;

	protected:
		auto OnMouseHovered(int index, bool hovered) -> void override;
		auto OnMousePressed(int index, bool pressed, QMouseEvent* event) -> void override;
		auto OnClicked(int index, QMouseEvent* event) -> void override;
		auto OnDoubleClicked(int index) -> void override;
		auto OnVisibleIndexChanged(int from, int to) -> void override;
		auto OnItemResized(int size) -> void override;

	protected slots:
		auto OnTimerTimeOut() -> void;
		auto OnSelectionChanged(const QList<Model::DisplayItem*>& selection) -> void;
		auto OnRootRemoved(const Model::DisplayItem* item) -> void;
		auto OnItemUpdated(const Model::DisplayItem* item) -> void;
		auto OnItemRemoved(const Model::DisplayItem* item) -> void;

	signals:
		auto DoubleClicked(Model::DisplayItem* item) -> void;
		auto TooltipUpdated(const QString& text) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
