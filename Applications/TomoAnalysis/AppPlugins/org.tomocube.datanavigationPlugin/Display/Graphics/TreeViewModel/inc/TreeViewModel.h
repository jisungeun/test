#pragma once

#include <memory>

#include <QAbstractItemModel>

#include "IServiceProvider.h"
#include "DisplayItem.h"
#include "TA.DataNav.Display.Graphics.TreeViewModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_TreeViewModel_API TreeViewModel final : public QAbstractItemModel {
	public:
		explicit TreeViewModel(Tomocube::IServiceProvider* provider);
		~TreeViewModel() override;

		auto index(int row, int column, const QModelIndex& parent) const -> QModelIndex override;
		auto parent(const QModelIndex& child) const -> QModelIndex override;
		auto rowCount(const QModelIndex& parent) const -> int override;
		auto columnCount(const QModelIndex& parent) const -> int override;
		auto data(const QModelIndex& index, int role) const -> QVariant override;
		auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;
		auto flags(const QModelIndex& index) const->Qt::ItemFlags override;

		auto GetIndex(Model::DisplayItem* item) const ->QModelIndex;
		auto SetTcfVisibility(bool visibility) -> void;
		auto SetSearch(const QString& keyword) -> void;

	protected slots:
		auto OnRootAdded(Model::DisplayItem* item) -> void;
		auto OnRootRemoved(Model::DisplayItem* item) -> void;
		auto OnItemRemoved(Model::DisplayItem* item) -> void;
		auto OnLayoutUpdated(Model::DisplayItem* item) -> void;
		auto OnDataUpdated(Model::DisplayItem* item) -> void;

		auto OnSelectionChanged(Model::DisplayItem* item) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
