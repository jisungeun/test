#include <QIcon>
#include <QStack>

#include "TreeViewModel.h"

#include "IDisplayItemModel.h"
#include "ISelectionModel.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct TreeViewModel::Impl {
		std::shared_ptr<Model::IDisplayItemModel> itemModel = nullptr;
		std::shared_ptr<Model::ISelectionModel> selectionModel = nullptr;

		QString searchKeyword;

		QIcon folder = QIcon(":/DataNavigation/Icon/folder_closed_white.svg");
		QIcon project = QIcon(":/DataNavigation/Icon/tsx_project_white.svg");
		QIcon experiment = QIcon(":/DataNavigation/Icon/tsx_experiment_white.svg");
		QIcon loading = QIcon(":/DataNavigation/Icon/timer_blue.svg");

		QIcon unknown = QIcon(":/DataNavigation/Icon/unknown_white.svg");
		QIcon crashed = QIcon(":/DataNavigation/Icon/crashed_white.svg");
		QIcon tcf = QIcon(":/DataNavigation/Icon/picture_white.svg");
		QIcon timelapseTcf = QIcon(":/DataNavigation/Icon/picture_long_white.svg");

		bool tcfVisibility = false;

		auto GetIndex(const Model::DisplayItem* ptr) const -> int;
		auto GetChildAt(const Model::DisplayItem* ptr, int index) const->Model::DisplayItem*;
		auto GetChildrenCount(const Model::DisplayItem* item) const -> int;
	};

	auto TreeViewModel::Impl::GetIndex(const Model::DisplayItem* ptr) const -> int {
		if (!ptr)
			return 0;

		int index = -1;
		QVector<Model::DisplayItem*> children;

		if (ptr->IsRoot()) {
			children = itemModel->GetRoots();
		} else {
			if (itemModel->Contains(ptr->GetParent()))
				children = ptr->GetParent()->GetChildren();
		}

		for (const auto* ch : children) {
			bool searched = ch->IsRoot() || searchKeyword.isEmpty();

			QStack<const Model::DisplayItem*> stack;
			if (!searched)
				stack.push(ch);

			while (!stack.isEmpty()) {
				const auto* i = stack.pop();

				if (i->GetDisplayName().toLower().contains(searchKeyword)) {
					searched = true;
					break;
				}

				for (const auto* c : i->GetChildren())
					if (c->ContainsTcf())
						stack.push(c);
			}

			if (searched) {
				const auto isTcf = (tcfVisibility && (ch->IsTcf() || ch->IsPairedTcf()));
				const auto isDir = (!ch->IsPairedTcf() && ch->IsDirectory() && ch->ContainsTcf());

				if (ch->IsRoot() || isTcf || isDir)
					index++;

				if (ch == ptr)
					break;
			}
		}

		return index;
	}

	auto TreeViewModel::Impl::GetChildAt(const Model::DisplayItem* ptr, int index) const -> Model::DisplayItem* {
		QVector<Model::DisplayItem*> children;

		if (!ptr) {
			children = itemModel->GetRoots();
		} else {
			if (itemModel->Contains(ptr))
				children = ptr->GetChildren();
		}

		for (int i = 0, j = -1; i < children.count(); i++) {
			bool searched = children[i]->IsRoot() || searchKeyword.isEmpty();

			QStack<const Model::DisplayItem*> stack;
			if (!searched)
				stack.push(children[i]);

			while (!stack.isEmpty()) {
				const auto* i = stack.pop();

				if (i->GetDisplayName().toLower().contains(searchKeyword)) {
					searched = true;
					break;
				}

				for (const auto* c : i->GetChildren())
					if (c->ContainsTcf())
						stack.push(c);
			}

			if (searched) {
				const auto isTcf = (tcfVisibility && (children[i]->IsTcf() || children[i]->IsPairedTcf()));
				const auto isDir = (!children[i]->IsPairedTcf() && children[i]->IsDirectory() && children[i]->ContainsTcf());

				if (children[i]->IsRoot() || isTcf || isDir)
					j++;

				if (j == index) {
					if (!children[i]->IsRoot())
						if (auto* paired = children[i]->GetPairedTcf())
							return paired;

					return children[i];
				}
			}
		}

		return {};
	}

	auto TreeViewModel::Impl::GetChildrenCount(const Model::DisplayItem* item) const -> int {
		if (!item || item->IsTcf())
			return 0;

		int count = 0;

		for (const auto* ch : item->GetChildren()) {
			bool searched = ch->IsRoot() || searchKeyword.isEmpty();

			QStack<const Model::DisplayItem*> stack;
			if (!searched)
				stack.push(ch);

			while (!stack.isEmpty()) {
				const auto* i = stack.pop();

				if (i->GetDisplayName().toLower().contains(searchKeyword)) {
					searched = true;
					break;
				}

				for (const auto* c : i->GetChildren())
					if (c->ContainsTcf())
						stack.push(c);
			}

			if (searched) {
				const auto isTcf = (tcfVisibility && (ch->IsTcf() || ch->IsPairedTcf()));
				const auto isDir = (!ch->IsPairedTcf() && ch->IsDirectory() && ch->ContainsTcf());

				if (isTcf || isDir)
					count++;
			}
		}

		return count;
	}

	TreeViewModel::TreeViewModel(Tomocube::IServiceProvider* provider) : QAbstractItemModel(), d(new Impl) {
		d->itemModel = provider->GetService<Model::IDisplayItemModel>();
		d->selectionModel = provider->GetService<Model::ISelectionModel>();

		connect(d->itemModel.get(), &Model::IDisplayItemModel::RootAdded, this, &TreeViewModel::OnRootAdded);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::RootRemoved, this, &TreeViewModel::OnRootRemoved);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::LayoutUpdated, this, &TreeViewModel::OnLayoutUpdated);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::DataUpdated, this, &TreeViewModel::OnDataUpdated);
		connect(d->itemModel.get(), &Model::IDisplayItemModel::ItemRemoved, this, &TreeViewModel::OnItemRemoved);
		connect(d->selectionModel.get(), &Model::ISelectionModel::ItemRemoved, this, &TreeViewModel::OnSelectionChanged);
		connect(d->selectionModel.get(), &Model::ISelectionModel::ItemAdded, this, &TreeViewModel::OnSelectionChanged);
	}

	TreeViewModel::~TreeViewModel() = default;

	auto TreeViewModel::index(int row, int column, const QModelIndex& parent) const -> QModelIndex {
		if (row < 0 || column < 0 || !hasIndex(row, column, parent))
			return {};

		const auto child = d->GetChildAt(static_cast<Model::DisplayItem*>(parent.internalPointer()), row);
		const auto index = d->GetIndex(child);

		return createIndex(index, 0, child);
	}

	auto TreeViewModel::parent(const QModelIndex& child) const -> QModelIndex {
		if (!child.isValid())
			return {};

		if (!child.internalPointer())
			return {};

		if (const auto* item = static_cast<Model::DisplayItem*>(child.internalPointer()); d->itemModel->Contains(item) && !item->IsRoot()) {
			const auto idx = d->GetIndex(item->GetParent());
			return createIndex(idx, 0, item->GetParent());
		}

		return createIndex(0, 0, nullptr);
	}

	auto TreeViewModel::rowCount(const QModelIndex& parent) const -> int {
		if (parent.isValid() && parent.internalPointer()) {
			if (const auto* item = static_cast<Model::DisplayItem*>(parent.internalPointer()); d->itemModel->Contains(item))
				return d->GetChildrenCount(item);
		}

		return d->itemModel->GetRoots().count();
	}

	auto TreeViewModel::columnCount(const QModelIndex& parent) const -> int {
		return 1;
	}

	auto TreeViewModel::data(const QModelIndex& index, int role) const -> QVariant {
		if (!index.isValid())
			return {};

		if (auto* item = static_cast<Model::DisplayItem*>(index.internalPointer()); item && d->itemModel->Contains(item)) {
			switch (role) {
				case Qt::DisplayRole:
					return item->GetDisplayName();
				case Qt::DecorationRole:
					switch (item->GetType()) {
						case Model::DisplayItem::Type::Directory:
							return d->folder;
						case Model::DisplayItem::Type::Tcf:
							return d->tcf;
						case Model::DisplayItem::Type::CrashedTcf:
							return d->crashed;
						case Model::DisplayItem::Type::ScannableTcf:
							return d->unknown;
						case Model::DisplayItem::Type::TimelapseTcf:
							return d->timelapseTcf;
						case Model::DisplayItem::Type::HtxExperiment:
							return d->experiment;
						case Model::DisplayItem::Type::HtxProject:
							return d->project;
						case Model::DisplayItem::Type::ScanningDirectory:
							return d->loading;
					}

					break;
				case Qt::CheckStateRole:
					if (item->IsTcf())
						return d->selectionModel->Contains(item) ? Qt::Checked : Qt::Unchecked;

					break;
			}
		}

		return {};
	}

	auto TreeViewModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
		switch (role) {
			case Qt::CheckStateRole:
				if (auto* item = static_cast<Model::DisplayItem*>(index.internalPointer())) {
					if (d->selectionModel->Contains(item))
						d->selectionModel->Remove(item);
					else
						d->selectionModel->Add(item);

					d->selectionModel->Update();
				}

				return true;
		}

		return QAbstractItemModel::setData(index, value, role);
	}

	auto TreeViewModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
		if (const auto* item = static_cast<Model::DisplayItem*>(index.internalPointer())) {
			if (d->itemModel->Contains(item) && item->IsTcf())
				return { Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable };

			return { Qt::ItemIsSelectable | Qt::ItemIsEnabled };
		}

		return {};
	}

	auto TreeViewModel::GetIndex(Model::DisplayItem* item) const -> QModelIndex {
		if (d->itemModel->Contains(item)) {
			const auto idx = d->GetIndex(item);
			return createIndex(idx, 0, item);
		}

		return {};
	}

	auto TreeViewModel::SetTcfVisibility(bool visibility) -> void {
		d->tcfVisibility = visibility;

		emit layoutChanged();
	}

	auto TreeViewModel::SetSearch(const QString& keyword) -> void {
		d->searchKeyword = keyword;

		emit layoutChanged();
	}

	auto TreeViewModel::OnRootAdded(Model::DisplayItem* item) -> void {
		const auto index = d->GetIndex(item);
		const auto parent = createIndex(index, 0, item).parent();

		emit beginInsertRows({ parent }, index, index);
		emit endInsertRows();
		emit layoutChanged({ parent });
	}

	auto TreeViewModel::OnRootRemoved(Model::DisplayItem* item) -> void {
		const auto index = d->GetIndex(item);
		const auto parent = createIndex(index, 0, item).parent();

		emit beginRemoveRows({ parent }, index, index);
		emit endRemoveRows();
		emit layoutChanged({ parent });
	}

	auto TreeViewModel::OnItemRemoved(Model::DisplayItem* item) -> void {
		const auto index = d->GetIndex(item);
		const auto parent = createIndex(index, 0, item).parent();

		if (index > -1) {
			emit beginRemoveRows({ parent }, index, index);
			emit endRemoveRows();
			emit layoutChanged({ parent });
		}
	}

	auto TreeViewModel::OnLayoutUpdated(Model::DisplayItem* item) -> void {
		const int index = d->GetIndex(item);

		emit layoutChanged({ createIndex(index, 0, item) });
	}

	auto TreeViewModel::OnDataUpdated(Model::DisplayItem* item) -> void {
		const auto idx = d->GetIndex(item);

		emit dataChanged(createIndex(idx, 0, item), createIndex(idx, 0, item), { Qt::DisplayRole | Qt::DecorationRole });
	}

	auto TreeViewModel::OnSelectionChanged(Model::DisplayItem* item) -> void {
		const auto idx = d->GetIndex(item);
		emit dataChanged(createIndex(idx, 0, item), createIndex(idx, 0, item), { Qt::CheckStateRole });
	}
}
