#include <QMap>
#include <QStack>

#include "DisplayItemModel.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct DisplayItemModel::Impl {
		QMap<const void*, Model::DisplayItem*> ptrMap;
		QMap<QString, QVector<Model::DisplayItem*>> urlMap;
		QVector<Model::DisplayItem*> roots;
	};

	DisplayItemModel::DisplayItemModel() : IDisplayItemModel(), d(new Impl) {}

	DisplayItemModel::~DisplayItemModel() = default;

	auto DisplayItemModel::AddRoot(const QString& url) -> Model::DisplayItem* {
		if (std::any_of(d->roots.cbegin(), d->roots.cend(), [url](const Model::DisplayItem* item) {
			return item->GetUrl() == url;
		}))
			return {};

		auto* root = new Model::DisplayItem(url);
		d->roots.push_back(root);
		AddItem(root);

		emit RootAdded(root);
		return root;
	}

	auto DisplayItemModel::AddItem(Model::DisplayItem* item) -> void {
		if (!d->urlMap[item->GetUrl()].contains(item))
			d->urlMap[item->GetUrl()].push_back(item);
		d->ptrMap[item] = item;
	}

	auto DisplayItemModel::RemoveRoot(Model::DisplayItem* item) -> void {
		emit RootRemoved(item);
		emit ItemRemoved(item);

		d->roots.removeOne(item);
		d->ptrMap.remove(item);

		if (d->urlMap.contains(item->GetUrl()) && d->urlMap[item->GetUrl()].count() == 1)
			RemoveItem(item);
	}

	auto DisplayItemModel::RemoveItem(Model::DisplayItem* item) -> void {
		emit ItemRemoved(item);

		if (d->urlMap.contains(item->GetUrl())) {
			if (d->urlMap[item->GetUrl()].count() == 1 && d->urlMap[item->GetUrl()].first() == item)
				d->urlMap.remove(item->GetUrl());
			else
				d->urlMap[item->GetUrl()].removeOne(item);
		}

		d->ptrMap.remove(item);
	}

	auto DisplayItemModel::Contains(const Model::DisplayItem* ptr) const -> bool {
		return d->ptrMap.contains(ptr);
	}

	auto DisplayItemModel::GetItems(const QString& url) const -> QVector<Model::DisplayItem*> {
		if (d->urlMap.contains(url))
			return d->urlMap[url];

		return {};
	}

	auto DisplayItemModel::GetItemCount(const QString& url) const -> int {
		return GetItems(url).count();
	}

	auto DisplayItemModel::GetRoots() const -> const QVector<Model::DisplayItem*>& {
		return d->roots;
	}

	auto DisplayItemModel::UpdateData(Model::DisplayItem* item) -> void {
		if (d->ptrMap.contains(item))
			emit DataUpdated(item);
	}

	auto DisplayItemModel::UpdateLayout(Model::DisplayItem* item) -> void {
		if (d->ptrMap.contains(item))
			emit LayoutUpdated(item);
	}
}