#include "SelectionModel.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	struct SelectionModel::Impl {
		QList<Model::DisplayItem*> items;
	};

	SelectionModel::SelectionModel() : ISelectionModel(), d(new Impl) {}

	SelectionModel::~SelectionModel() = default;

	auto SelectionModel::Add(Model::DisplayItem* item) -> void {
		if (item->IsDirectory())
			Clear(Model::DisplayItem::Type::DirectoryType);

		if (!d->items.contains(item)) {
			d->items.push_back(item);

			emit ItemAdded(item);
		}
	}

	auto SelectionModel::Remove(Model::DisplayItem* item) -> void {
		if (d->items.removeOne(item))
			emit ItemRemoved(item);
	}

	auto SelectionModel::Remove(const QString& url) -> void {
		const auto& item = std::find_if(d->items.cbegin(), d->items.cend(), [url](const Model::DisplayItem* item) {
			return item->GetUrl() == url;
		});

		if (item != d->items.end()) {
			d->items.removeOne(*item);

			emit ItemRemoved(*item);
		}
	}

	auto SelectionModel::Clear() -> void {
		auto items = d->items;
		d->items.clear();

		for (auto* i : items)
			emit ItemRemoved(i);
	}

	auto SelectionModel::Clear(Model::DisplayItem::Types type) -> void {
		QList<Model::DisplayItem*> list;

		for (auto* i : d->items) {
			if (type.testFlag(i->GetType()))
				list.push_back(i);
		}

		for (auto* i : list) {
			d->items.removeOne(i);
			emit ItemRemoved(i);
		}
	}

	auto SelectionModel::Get() const -> const QList<Model::DisplayItem*>& {
		return d->items;
	}

	auto SelectionModel::Get(Model::DisplayItem::Types type) const -> QList<Model::DisplayItem*> {
		QList<Model::DisplayItem*> list;

		for (auto* i : d->items) {
			if (type.testFlag(i->GetType()))
				list.push_back(i);
		}

		return list;
	}

	auto SelectionModel::Contains(Model::DisplayItem* item) const -> bool {
		return d->items.contains(item);
	}

	auto SelectionModel::Contains(const QString& url) const -> bool {
		return std::any_of(d->items.cbegin(), d->items.cend(), [url](const Model::DisplayItem* item) {
			return item->GetUrl() == url;
		});
	}

	auto SelectionModel::Update() -> void {
		emit SelectionChanged(d->items);
	}
}
