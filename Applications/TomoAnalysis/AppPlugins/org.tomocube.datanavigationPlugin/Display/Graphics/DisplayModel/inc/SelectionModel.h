#pragma once

#include <memory>

#include "DisplayItem.h"
#include "ISelectionModel.h"

#include "TA.DataNav.Display.Graphics.DisplayModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_DisplayModel_API SelectionModel final : public Model::ISelectionModel{
	public:
		SelectionModel();
		~SelectionModel() override;

		auto Add(Model::DisplayItem* item)->void override;
		auto Remove(Model::DisplayItem* item)-> void override;
		auto Remove(const QString& url) -> void override;
		auto Clear() -> void override;
		auto Clear(Model::DisplayItem::Types type) -> void override;

		auto Get() const -> const QList<Model::DisplayItem*> & override;
		auto Get(Model::DisplayItem::Types type) const -> QList<Model::DisplayItem*> override;
		auto Contains(Model::DisplayItem* item) const -> bool override;
		auto Contains(const QString& url) const -> bool override;

		auto Update() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
