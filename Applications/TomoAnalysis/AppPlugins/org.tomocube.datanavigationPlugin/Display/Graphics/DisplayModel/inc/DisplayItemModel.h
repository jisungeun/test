#pragma once

#include <memory>

#include "IDisplayItemModel.h"
#include "DisplayItem.h"

#include "TA.DataNav.Display.Graphics.DisplayModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Graphics {
	class TA_DataNav_Display_Graphics_DisplayModel_API DisplayItemModel : public Model::IDisplayItemModel {
	public:
		DisplayItemModel();
		~DisplayItemModel() override;
		
		auto AddRoot(const QString& url) -> Model::DisplayItem* override;
		auto AddItem(Model::DisplayItem* item) -> void override;
		auto RemoveRoot(Model::DisplayItem* item) -> void override;
		auto RemoveItem(Model::DisplayItem* item) -> void override;

		auto Contains(const Model::DisplayItem* ptr) const -> bool override;
		auto GetItems(const QString& url) const -> QVector<Model::DisplayItem*> override;
		auto GetItemCount(const QString& url) const -> int override;
		auto GetRoots() const -> const QVector<Model::DisplayItem*>& override;

		auto UpdateData(Model::DisplayItem* item) -> void override;
		auto UpdateLayout(Model::DisplayItem* item) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}