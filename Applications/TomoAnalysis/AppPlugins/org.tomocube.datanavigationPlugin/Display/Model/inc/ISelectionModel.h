#pragma once

#include <memory>

#include <QObject>

#include "DisplayItem.h"
#include "IService.h"

#include "TA.DataNav.Display.ModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	class TA_DataNav_Display_Model_API ISelectionModel : public QObject, public Tomocube::IService {
		Q_OBJECT

	public:
		virtual auto Add(DisplayItem* item)->void = 0;
		virtual auto Remove(DisplayItem* item)-> void = 0;
		virtual auto Remove(const QString& url) -> void = 0;
		virtual auto Clear() -> void = 0;
		virtual auto Clear(DisplayItem::Types type) -> void = 0;

		virtual auto Get() const -> const QList<DisplayItem*>& = 0;
		virtual auto Get(DisplayItem::Types type) const -> QList<DisplayItem*> = 0;
		virtual auto Contains(Model::DisplayItem* item) const -> bool = 0;
		virtual auto Contains(const QString& url) const -> bool = 0;

		virtual auto Update() -> void = 0;

	signals:
		auto ItemAdded(DisplayItem* item) -> void;
		auto ItemRemoved(DisplayItem* item) -> void;
		auto SelectionChanged(const QList<DisplayItem*>& items) -> void;
	};
}
