#pragma once

#include <QDialog>

#include "IService.h"

#include "TA.DataNav.Display.ModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	class TA_DataNav_Display_Model_API IWindow : public Tomocube::IService{
	public:
		virtual auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void = 0;
	};
}