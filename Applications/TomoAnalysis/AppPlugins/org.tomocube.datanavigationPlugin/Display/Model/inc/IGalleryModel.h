#pragma once

#include <QString>
#include <QPixmap>

#include "IService.h"

#include "TA.DataNav.Display.ModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	class TA_DataNav_Display_Model_API IGalleryModel : public Tomocube::IService {
	public:
		virtual auto UpdateThumbnail(const QString& url, const QString& modality, const QPixmap& thumbnail) -> void = 0;
		virtual auto UpdateThumbnails(const QString& url, const QString& modality, const QVector<QPixmap>& thumbnails) -> void = 0;

		virtual auto SetSearch(const QString& keyword) -> void = 0;
	};
}
