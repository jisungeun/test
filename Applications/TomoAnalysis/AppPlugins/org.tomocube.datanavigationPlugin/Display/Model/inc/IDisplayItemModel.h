#pragma once

#include <QObject>
#include <QString>

#include "IService.h"
#include "DisplayItem.h"

#include "TA.DataNav.Display.ModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	class TA_DataNav_Display_Model_API IDisplayItemModel : public QObject, public Tomocube::IService {
		Q_OBJECT

	public:
		virtual auto AddRoot(const QString& url) -> DisplayItem* = 0;
		virtual auto AddItem(DisplayItem* item) -> void = 0;
		virtual auto RemoveRoot(DisplayItem* item) -> void = 0;
		virtual auto RemoveItem(DisplayItem* item) -> void = 0;

		virtual auto Contains(const DisplayItem* ptr) const -> bool = 0;
		virtual auto GetItems(const QString& url) const -> QVector<DisplayItem*> = 0;
		virtual auto GetItemCount(const QString& url) const -> int = 0;
		virtual auto GetRoots() const -> const QVector<DisplayItem*>& = 0;

		virtual auto UpdateData(DisplayItem* item) -> void = 0;
		virtual auto UpdateLayout(DisplayItem* item) -> void = 0;

	signals:
		auto RootAdded(DisplayItem* item) -> void;
		auto RootRemoved(DisplayItem* item) -> void;
		auto ItemRemoved(DisplayItem* item) -> void;
		auto LayoutUpdated(DisplayItem* item) -> void;
		auto DataUpdated(DisplayItem* item) -> void;
	};
}