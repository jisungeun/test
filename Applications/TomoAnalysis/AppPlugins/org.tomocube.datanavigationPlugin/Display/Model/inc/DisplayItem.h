#pragma once

#include <memory>

#include <QVector>

#include "TA.DataNav.Display.ModelExport.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	class TA_DataNav_Display_Model_API DisplayItem {
	public:
		enum class Type {
			Tcf = 0x001,
			TimelapseTcf = 0x002,
			CrashedTcf = 0x004,
			ScannableTcf = 0x008,
			HtxProject = 0x010,
			HtxExperiment = 0x020,
			Directory = 0x040,
			ScanningDirectory = 0x080,
			TcfType = Tcf | TimelapseTcf | CrashedTcf | ScannableTcf,
			DirectoryType  = HtxProject | HtxExperiment | Directory | ScanningDirectory
		};

		Q_DECLARE_FLAGS(Types, Type)

		explicit DisplayItem(const QString& url);
		DisplayItem(const QString& url, Type type);
		~DisplayItem();
		
		auto operator<(const DisplayItem& rhs) const -> bool;
		
		auto SetType(Type type) -> bool;
		auto SetBusy(bool busy) -> void;
		auto AddChild(const QString& url, Type type) ->DisplayItem*;
		auto RemoveChild(const QString& url) -> void;
		auto ClearChildren() -> void;
		auto Sort() -> void;

		auto GetParent() const -> DisplayItem*;
		auto GetUrl() const -> const QString&;
		auto GetType() const->Type;
		auto IsReadable() const -> bool;
		auto IsBusy() const -> bool;
		auto GetDisplayName() const -> const QString&;
		auto GetChild(const QString& url) const -> DisplayItem*;
		auto GetChildren() const -> const QVector<DisplayItem*>&;
		auto GetPairedTcf() const -> DisplayItem*;
		auto ContainsTcf() const -> bool;
		auto Contains(const QString& url) const -> bool;
		auto Contains(DisplayItem* item) const -> bool;
		auto IsPairedTcf() const -> bool;
		auto IsRoot() const -> bool;
		auto IsTcf() const -> bool;
		auto IsDirectory() const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}

Q_DECLARE_OPERATORS_FOR_FLAGS(TomoAnalysis::DataNavigation::Display::Model::DisplayItem::Types)