#include <QFileInfo>
#include <QDateTime>

#include "DisplayItem.h"

namespace TomoAnalysis::DataNavigation::Display::Model {
	struct DisplayItem::Impl {
		DisplayItem* parent = nullptr;
		QVector<DisplayItem*> children;
		QString url;
		QString name;
		bool busy = false;
		Type type = Type::Directory;
		bool containsTcf = false;

		auto UpdateDisplayName() -> void;
		auto UpdateContainsTcf(bool isTcf) -> void;
	};

	auto DisplayItem::Impl::UpdateDisplayName() -> void {
		const QFileInfo info(url);
		name = info.isFile() ? info.completeBaseName() : info.fileName();

		if (name.isEmpty())
			name = info.absolutePath();
	}

	auto DisplayItem::Impl::UpdateContainsTcf(bool isTcf) -> void {
		const bool old = containsTcf;
		containsTcf = (isTcf) ? true : std::any_of(children.cbegin(), children.cend(), [](const DisplayItem* item) {
			return item->IsTcf();
		});

		if (old != containsTcf) {
			const DisplayItem* p = parent;

			while (p) {
				p->d->containsTcf = std::any_of(p->d->children.cbegin(), p->d->children.cend(), [](const DisplayItem* item) {
					return item->ContainsTcf();
				});
				p = p->d->parent;
			}
		}
	}
	
	DisplayItem::DisplayItem(const QString& url) : d(new Impl) {
		d->url = url;

		d->UpdateDisplayName();
	}

	DisplayItem::DisplayItem(const QString& url, Type type) : d(new Impl) {
		d->url = url;
		d->type = type;

		d->UpdateDisplayName();
		d->containsTcf = IsTcf();
	}

	DisplayItem::~DisplayItem() {
		qDeleteAll(d->children);
	}

	auto DisplayItem::operator<(const DisplayItem& rhs) const -> bool {
		const auto isLhsTcf = this->IsTcf() || this->IsPairedTcf();
		const auto isRhsTcf = rhs.IsTcf() || rhs.IsPairedTcf();

		if (isLhsTcf && !isRhsTcf)
			return false;
		if (!isLhsTcf && isRhsTcf)
			return true;

		const QRegExp regex(R"(\d+(\.\d+)*$)");
		const auto lhsName = this->GetDisplayName().startsWith("20") ? this->GetDisplayName().mid(0, 15).remove('.') : "20" + this->GetDisplayName().mid(0, 13).remove('.');
		const auto rhsName = rhs.GetDisplayName().startsWith("20") ? rhs.GetDisplayName().mid(0, 15).remove('.') : "20" + rhs.GetDisplayName().mid(0, 13).remove('.');
		const auto lhsValid = regex.exactMatch(lhsName);
		const auto rhsValid = regex.exactMatch(rhsName);

		if (lhsValid && rhsValid)
			return lhsName < rhsName;
		if (lhsValid && !rhsValid)
			return true;
		if (!lhsValid && rhsValid)
			return false;

		return this->GetDisplayName() < rhs.GetDisplayName();
	}

	auto DisplayItem::SetType(Type type) -> bool {
		if (d->type != type) {
			d->type = type;
			d->UpdateContainsTcf(IsTcf());

			return true;
		}

		return false;
	}

	auto DisplayItem::SetBusy(bool busy) -> void {
		d->busy = busy;
	}

	auto DisplayItem::AddChild(const QString& url, Type type) -> DisplayItem* {
		if (std::any_of(d->children.cbegin(), d->children.cend(), [url](const DisplayItem* item) {
			return item->GetUrl() == url;
		}))
			return nullptr;

		auto* item = new DisplayItem(url, type);
		item->d->parent = this;
		d->children.push_back(item);

		d->UpdateContainsTcf(IsTcf());

		return item;
	}

	auto DisplayItem::RemoveChild(const QString& url) -> void {
		for (int i = 0; i < d->children.count(); i++) {
			if (d->children[i]->GetUrl() == url) {
				const auto* ch = d->children.takeAt(i);
				delete ch;
				break;
			}
		}
	}

	auto DisplayItem::ClearChildren() -> void {
		qDeleteAll(d->children);
		d->children.clear();
	}

	auto DisplayItem::Sort() -> void {
		std::sort(d->children.begin(), d->children.end(), [](const DisplayItem* lhs, const DisplayItem* rhs) {
			return *lhs < *rhs;
		});
	}

	auto DisplayItem::GetParent() const -> DisplayItem* {
		return d->parent;
	}

	auto DisplayItem::GetUrl() const -> const QString& {
		return d->url;
	}

	auto DisplayItem::GetType() const -> Type {
		return (d->busy) ? Type::ScanningDirectory : d->type;
	}

	auto DisplayItem::IsReadable() const -> bool {
		return d->type == Type::Tcf || d->type == Type::TimelapseTcf || d->type == Type::ScannableTcf;
	}

	auto DisplayItem::IsBusy() const -> bool {
		return d->busy;
	}

	auto DisplayItem::GetDisplayName() const -> const QString& {
		return d->name;
	}

	auto DisplayItem::GetChild(const QString& url) const -> DisplayItem* {
		for (const auto& ch : d->children) {
			if (ch->GetUrl() == url)
				return ch;
		}

		return {};
	}

	auto DisplayItem::GetChildren() const -> const QVector<DisplayItem*>& {
		return d->children;
	}

	auto DisplayItem::GetPairedTcf() const -> DisplayItem* {
		if (IsDirectory()) {
			DisplayItem* tcf = nullptr;

			for (auto* ch : GetChildren()) {
				if (ch->IsTcf()) {
					if (tcf)
						return {};

					tcf = ch;
				}
			}

			if (tcf && tcf->GetDisplayName() == GetDisplayName())
				return tcf;
		}

		return {};
	}

	auto DisplayItem::ContainsTcf() const -> bool {
		return d->containsTcf;
	}

	auto DisplayItem::Contains(const QString& url) const -> bool {
		return std::any_of(d->children.cbegin(), d->children.cend(), [url](const DisplayItem* item) {
			return item->GetUrl() == url;
		});
	}

	auto DisplayItem::Contains(DisplayItem* item) const -> bool {
		return d->children.contains(item);
	}

	auto DisplayItem::IsPairedTcf() const -> bool {
		return GetPairedTcf() != nullptr;
	}

	auto DisplayItem::IsRoot() const -> bool {
		return d->parent == nullptr;
	}

	auto DisplayItem::IsTcf() const -> bool {
		switch (d->type) {
			case Type::Tcf:
				[[fallthrough]];
			case Type::TimelapseTcf:
				[[fallthrough]];
			case Type::CrashedTcf:
				[[fallthrough]];
			case Type::ScannableTcf:
				return true;
			default:
				return false;
		}
	}

	auto DisplayItem::IsDirectory() const -> bool {
		switch (d->type) {
			case Type::HtxProject:
				[[fallthrough]];
			case Type::HtxExperiment:
				[[fallthrough]];
			case Type::Directory:
				[[fallthrough]];
			case Type::ScanningDirectory:
				return true;
			default:
				return false;
		}
	}
}
