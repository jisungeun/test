#include "datanavigationPlugin.h"

#include <ctkPluginFrameworkLauncher.h>
#include <QtPlugin>

#include <service/event/ctkEvent.h>

#include <H5Epublic.h>

#include <AppInterfaceTA.h>

#include <MenuEvent.h>
#include <AppEvent.h>
#include <H5Exception.h>

#include "MainWindow.h"

namespace TomoAnalysis::DataNavigation::AppUI {
	struct datanavigationPlugin::Impl {
		ctkPluginContext* context = nullptr;
		MainWindow* window = nullptr;
		AppInterfaceTA* appInterface = nullptr;
	};

	datanavigationPlugin* datanavigationPlugin::instance = nullptr;

	datanavigationPlugin* datanavigationPlugin::getInstance() {
		return instance;
	}

	datanavigationPlugin::datanavigationPlugin() : d(new Impl) {
		d->window = new MainWindow;
		d->appInterface = new AppInterfaceTA(d->window);
		d->appInterface->SetProjectManager(true);

#ifndef _DEBUG
		H5::Exception::dontPrint();
#endif
	}

	datanavigationPlugin::~datanavigationPlugin() = default;

	auto datanavigationPlugin::start(ctkPluginContext* context) -> void {
		instance = this;
		d->context = context;

		registerService(context, d->appInterface, SYMBOL);
	}

	auto datanavigationPlugin::stop(ctkPluginContext* context) -> void {
		Q_UNUSED(context)
		//Never destroy this plugin
	}

	auto datanavigationPlugin::getPluginContext() -> ctkPluginContext* const {
		return d->context;
	}
}
