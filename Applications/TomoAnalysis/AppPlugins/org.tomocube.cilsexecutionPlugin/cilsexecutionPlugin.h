#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.cilsexecutionPlugin";

namespace TomoAnalysis::CilsExecution::AppUI {
    class CilsExecutionPlugin : public IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.cilsexecutionPlugin")            
    public:
        static CilsExecutionPlugin* getInstance();

        CilsExecutionPlugin();
        ~CilsExecutionPlugin() override;

        auto start(ctkPluginContext* context)->void override;
        auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                
            
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static CilsExecutionPlugin* instance;
    };
}