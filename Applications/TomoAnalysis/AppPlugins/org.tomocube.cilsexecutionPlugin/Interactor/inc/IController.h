#pragma once

#include "CilsExecutionInteractorExport.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API IController {
	public:
		IController() = default;
		virtual ~IController() = default;
	};
}