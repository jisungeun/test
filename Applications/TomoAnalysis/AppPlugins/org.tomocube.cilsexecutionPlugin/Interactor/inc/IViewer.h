#pragma once

#include "CilsExecutionInteractorExport.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API IViewer {
	public:
		IViewer() = default;
		virtual ~IViewer() = default;
	};
}