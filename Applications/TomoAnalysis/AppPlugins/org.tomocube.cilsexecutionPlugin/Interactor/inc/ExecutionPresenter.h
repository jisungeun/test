#pragma once

#include <memory>

#include "ExecutionViewer.h"
#include "IPresenter.h"
#include "IFinishOutput.h"
#include "INextAppOutput.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API ExecutionPresenter final : public IPresenter, public UseCase::IFinishOutput, public UseCase::INextAppOutput {
	public:
		ExecutionPresenter(ExecutionViewer * viewer = nullptr);
		~ExecutionPresenter() override;

		auto SetViewer(ExecutionViewer* viewer) -> void;

        auto Finished(int id) -> void override;
        auto FinishFailed(int id, QString reason) -> void override;

        auto CurrentAppChanged(QString appName) -> void override;
        auto AppChangeFailed(QString reason) -> void override;
    };
}
