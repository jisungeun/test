#pragma once

#include <memory>
#include <functional>

#include "IViewer.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API IPresenter {
	public:
		IPresenter(IViewer* viewer = nullptr);
		virtual ~IPresenter();

		auto SetViewer(IViewer* viewer) -> void;
		[[nodiscard]] auto GetViewer() const -> IViewer*;
		template<typename T>
		auto GetViewer() const -> T*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

    template <typename T>
    auto IPresenter::GetViewer() const -> T* {
		return dynamic_cast<T*>(GetViewer());
    }
}
