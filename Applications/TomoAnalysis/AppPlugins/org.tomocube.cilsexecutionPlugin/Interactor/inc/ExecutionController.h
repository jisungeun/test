#pragma once

#include <memory>
#include <QVariantMap>

#include <QVector>

#include "ExecutionViewer.h"
#include "IController.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API ExecutionController final : public IController{
	public:
		ExecutionController(ExecutionViewer * viewer = nullptr);
		~ExecutionController() override;

		auto InitAppPackage(const QString& packageName, const QStringList& appNames) -> void;

		auto SetViewer(ExecutionViewer* viewer) -> void;

		auto SetExecutionId(int id) -> void;
		auto SetExecutionDataId(const QString& id) -> void;
		auto SetExecutionTitle(const QString& title) -> void;
		auto SetRole(const QString& role) -> void;
		auto SetTcfPath(const QString& path) -> void;
		auto SetOutputPath(const QString& path) -> void;
		
		[[nodiscard]] auto GetExecutionId() const -> int;
		[[nodiscard]] auto GetExecutionDataId() const -> const QString&;
		[[nodiscard]] auto GetExecutionTitle() const -> const QString&;
		[[nodiscard]] auto GetRole() const -> QString;
		[[nodiscard]] auto GetTcfPath() const -> const QString&;
		[[nodiscard]] auto GetOutputPath() const -> const QString&;

		[[nodiscard]] auto GetAppNames() const -> const QStringList&;
		[[nodiscard]] auto HasNextApp() const -> bool;
		[[nodiscard]] auto GetCurrentAppIndex() const -> int;
		[[nodiscard]] auto GetCurrentAppName() const -> const QString&;
		[[nodiscard]] auto GetPackageName() const -> const QString&;

		[[nodiscard]] auto GetTcfFilePath() const->QString;
		[[nodiscard]] auto GetProcessorPath(QVariantMap meta) const->QString;

		auto NextApp() -> void;
		auto Finish(const QMap<QString, QString>& map) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
