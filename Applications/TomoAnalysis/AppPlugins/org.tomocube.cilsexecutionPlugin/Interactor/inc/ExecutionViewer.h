#pragma once

#include <QVector>

#include "IViewer.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	class CilsExecutionInteractor_API ExecutionViewer : public IViewer {
	public:
		ExecutionViewer() = default;
		~ExecutionViewer() override = default;

		virtual auto Finished(int id) -> void = 0;
		virtual auto FinishFailed(int id, QString reason) -> void = 0;

		virtual auto CurrentAppChanged(QString appName) -> void = 0;
		virtual auto AppChangeFailed(QString reason) -> void = 0;
	};
}
