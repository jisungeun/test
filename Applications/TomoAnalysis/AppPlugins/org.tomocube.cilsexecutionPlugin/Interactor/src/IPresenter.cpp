#include <QVector>

#include "IPresenter.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	struct IPresenter::Impl {
		IViewer* viewer = nullptr;
	};

	IPresenter::IPresenter(IViewer* viewer) : d(new Impl) {
		d->viewer = viewer;
	}

	IPresenter::~IPresenter() = default;

	auto IPresenter::SetViewer(IViewer* viewer) -> void {
		d->viewer = viewer;
	}

	auto IPresenter::GetViewer() const -> IViewer* {
		return d->viewer;
	}
}
