#include "ICilsExecutionFinisher.h"
#include "ExecutionController.h"
#include "ExecutionPresenter.h"

#include "FinishUseCase.h"
#include "GetProcessorPathUseCase.h"
#include "GetTcfFilePathUseCase.h"
#include "InitAppPackageUseCase.h"
#include "SetRoleUseCase.h"
#include "SetOutputPathUseCase.h"
#include "SetTcfPathUseCase.h"
#include "SetExecutionIdUseCase.h"
#include "NextAppUseCase.h"
#include "SetExecutionDataIdUseCase.h"
#include "SetExecutionTitleUseCase.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	struct ExecutionController::Impl {
		UseCase::ICilsExecutionFinisher* finisher = nullptr;
		Entity::AppPackage* app = nullptr;
		Entity::Execution* exec = nullptr;

		ExecutionPresenter presenter;

        static auto RoleConverter(const QString& role) -> Entity::RoleType {
			const auto lowered = role.toLower();

			if (lowered == "assignee") {
				return Entity::RoleType::Assignee;
			} else if (lowered == "reviewer") {
				return Entity::RoleType::Reviewer;
			} else {
				return Entity::RoleType::Any;
			}
		}
	};

	ExecutionController::ExecutionController(ExecutionViewer* viewer) : IController(), d(new Impl) {
		d->finisher = UseCase::ICilsExecutionFinisher::GetInstance();
		d->app = Entity::AppPackage::GetInstance();
		d->exec = Entity::Execution::GetInstance();

		d->presenter.SetViewer(viewer);
		d->finisher->GetFinishOutputHandler().AddObserver(&d->presenter);
	}

	ExecutionController::~ExecutionController() {
		d->finisher->GetFinishOutputHandler().RemoveObserver(&d->presenter);
	}

	auto ExecutionController::SetViewer(ExecutionViewer* viewer) -> void {
		d->presenter.SetViewer(viewer);
	}

	auto ExecutionController::SetExecutionId(int id) -> void {
		UseCase::SetExecutionIdUseCase usecase(id);
		usecase.Do();
	}

    auto ExecutionController::SetExecutionDataId(const QString& id) -> void {
		UseCase::SetExecutionDataIdUseCase usecase(id);
		usecase.Do();
    }

    auto ExecutionController::SetExecutionTitle(const QString& title) -> void {
		UseCase::SetExecutionTitleUseCase usecase(title);
		usecase.Do();
    }

    auto ExecutionController::GetExecutionDataId() const -> const QString& {
		return d->exec->GetExecutionDataId();
    }

    auto ExecutionController::GetExecutionTitle() const -> const QString& {
		return d->exec->GetExecutionTitle();
    }

    auto ExecutionController::SetRole(const QString& role) -> void {
		UseCase::SetRoleUseCase usecase(d->RoleConverter(role));
		usecase.Do();
	}

	auto ExecutionController::SetTcfPath(const QString& path) -> void {
		UseCase::SetTcfPathUseCase usecase(path);
		usecase.Do();
	}

	auto ExecutionController::SetOutputPath(const QString& path) -> void {
		UseCase::SetOutputPathUseCase usecase(path);
		usecase.Do();
	}

	auto ExecutionController::InitAppPackage(const QString& packageName, const QStringList& appNames) -> void {
		auto names = appNames;
		names.removeAll(",");
		names.removeAll("");

		UseCase::InitAppPackageUseCase app(packageName, names);
		app.Do();
	}

	auto ExecutionController::GetExecutionId() const -> int {
		return d->exec->GetExecutionId();
	}

	auto ExecutionController::GetRole() const -> QString {
		switch (d->exec->GetType()) {
		case Entity::RoleType::Any:
			return "Any";
		case Entity::RoleType::Assignee:
			return "Assignee";
		case Entity::RoleType::Reviewer:
			return "Reviewer";
		}

		return {};
	}

	auto ExecutionController::GetTcfPath() const -> const QString& {
		return d->exec->GetTcfPath();
	}

	auto ExecutionController::GetOutputPath() const -> const QString& {
		return d->exec->GetOutputPath();
	}

    auto ExecutionController::GetAppNames() const -> const QStringList& {
		return d->app->GetAppNames();
    }

    auto ExecutionController::HasNextApp() const -> bool {
		return d->app->HasNext();
    }

    auto ExecutionController::GetCurrentAppIndex() const -> int {
		return d->app->GetIndex();
    }

    auto ExecutionController::GetCurrentAppName() const -> const QString& {
		return d->app->GetCurrentAppName();
    }

    auto ExecutionController::GetPackageName() const -> const QString& {
		return d->app->GetPackageName();
    }

    auto ExecutionController::GetTcfFilePath() const -> QString {
		const UseCase::GetTcfFilePathUseCase usecase(d->exec->GetTcfPath());
		return usecase.GetFilePath();
    }

    auto ExecutionController::GetProcessorPath(QVariantMap meta) const -> QString {
		const UseCase::GetProcessorPathUseCase usecase(meta);
		return usecase.GetProcessorPath();
    }

    auto ExecutionController::NextApp() -> void {
		UseCase::NextAppUseCase usecase(&d->presenter);
		usecase.Do();
    }

    auto ExecutionController::Finish(const QMap<QString, QString>& map) -> void {
		UseCase::FinishUseCase usecase(map, &d->presenter);
		usecase.Do();
	}
}
