#include "ExecutionPresenter.h"
#include "ExecutionViewer.h"

namespace TomoAnalysis::CilsExecution::Interactor {
	ExecutionPresenter::ExecutionPresenter(ExecutionViewer* viewer) : IPresenter(viewer), IFinishOutput(), INextAppOutput() {}

	ExecutionPresenter::~ExecutionPresenter() = default;

	auto ExecutionPresenter::SetViewer(ExecutionViewer* viewer) -> void {
		IPresenter::SetViewer(viewer);
	}

	auto ExecutionPresenter::Finished(int id) -> void {
		auto* p = dynamic_cast<ExecutionViewer*>(GetViewer());
		p->Finished(id);
	}

	auto ExecutionPresenter::FinishFailed(int id, QString reason) -> void {
		auto* p = dynamic_cast<ExecutionViewer*>(GetViewer());
		p->FinishFailed(id, reason);
	}

	auto ExecutionPresenter::CurrentAppChanged(QString appName) -> void {
		auto* p = dynamic_cast<ExecutionViewer*>(GetViewer());
		p->CurrentAppChanged(appName);
	}

	auto ExecutionPresenter::AppChangeFailed(QString reason) -> void {
		auto* p = dynamic_cast<ExecutionViewer*>(GetViewer());
		p->AppChangeFailed(reason);
	}
}
