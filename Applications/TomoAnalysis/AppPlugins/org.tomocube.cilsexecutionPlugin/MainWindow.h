#pragma once

#include <IMainWindowTA.h>

#include "ExecutionViewer.h"

#include "ILicensed.h"

namespace TomoAnalysis::CilsExecution::AppUI {
	class MainWindow final : public IMainWindowTA, public Interactor::ExecutionViewer, public TomoAnalysis::License::ILicensed {
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		~MainWindow() override;

		auto GetParameter(QString name)->IParameter::Pointer override;
		auto SetParameter(QString name, IParameter::Pointer param) -> void override;

		auto Execute(const QVariantMap& params)->bool override;
		auto GetRunType()->std::tuple<bool, bool> override;
		auto TryActivate() -> bool override;
		auto TryDeactivate() -> bool override;
		auto IsActivate() -> bool override;
		auto GetMetaInfo()->QVariantMap override;

		auto GetFeatureName() const->QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	protected slots:
		auto OnFinishBtnClicked() -> void;
		auto OnNextBtnClicked() -> void;
		auto OnCloseBtnClicked() -> void;
		auto OnDiscardBtnClicked() -> void;
		auto OnOpenTcfBtnClicked() -> void;
		auto OnOpenOutputBtnClicked() -> void;

	protected:
		auto Finished(int id) -> void override;
		auto FinishFailed(int id, QString reason) -> void override;
		auto CurrentAppChanged(QString appName) -> void override;
		auto AppChangeFailed(QString reason) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
