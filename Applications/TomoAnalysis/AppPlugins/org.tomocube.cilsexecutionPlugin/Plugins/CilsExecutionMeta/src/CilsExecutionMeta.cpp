#include "CilsExecutionMeta.h"

namespace TomoAnalysis::CilsExecution::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "CILS Project Manager";
		d->appProperties["AppKey"] = "CILS Executor";
		d->appProperties["Symbol"] = "org.tomocube.cilsexecutionPlugin";
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}