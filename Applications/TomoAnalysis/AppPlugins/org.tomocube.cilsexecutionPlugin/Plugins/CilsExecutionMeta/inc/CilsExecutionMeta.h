#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "CilsExecutionMetaExport.h"

namespace TomoAnalysis::CilsExecution::Plugins {
	class CilsExecutionMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.cilsexecutionMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "CILS Executor"; }
		auto GetFullName()const->QString override { return "org.tomocube.cilsexecutionPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}