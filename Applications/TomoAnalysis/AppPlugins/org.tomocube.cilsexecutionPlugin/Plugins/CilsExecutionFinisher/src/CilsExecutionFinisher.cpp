#include "CilsExecutionFinisher.h"

#include "CilsClient.h"

namespace TomoAnalysis::CilsExecution::Plugin {
	struct CilsExecutionFinisher::Impl {
		TC::Cils::Client::CilsClient client;

		static auto StringToRole(const QString& type) -> TC::Cils::Client::UserType {
			if (type == "assignee")
				return TC::Cils::Client::UserType::Assignee;
			else if (type == "reviewer")
				return TC::Cils::Client::UserType::Reviewer;

			return TC::Cils::Client::UserType::None;
		}
	};

	CilsExecutionFinisher::CilsExecutionFinisher() : ICilsExecutionFinisher(), d(new Impl) {}

	CilsExecutionFinisher::~CilsExecutionFinisher() = default;

	auto CilsExecutionFinisher::FinishExecution(int id, const QString& type, const QString& path) -> void {
		const auto* response = d->client.FinishExecution(id, path, d->StringToRole(type));

		QObject::connect(response, &TC::Cils::Client::RequestResponse::Initialized, [response, this, id] {
			if (response->IsDone()) {
				GetFinishOutputHandler().Notify([id](UseCase::IFinishOutput* viewer) {
					viewer->Finished(id);
					});
			} else {
				if (auto* error = response->GetError()) {
					auto msg = error->GetMessage();
					GetFinishOutputHandler().Notify([id, msg](UseCase::IFinishOutput* viewer) {
						viewer->FinishFailed(id, msg);
						});
				} else {
					GetFinishOutputHandler().Notify([id](UseCase::IFinishOutput* viewer) {
						viewer->FinishFailed(id, "Unknown");
						});
				}
			}

			delete response;
			}
		);
	}
}
