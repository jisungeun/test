#pragma once

#include <memory>

#include "ICilsExecutionFinisher.h"
#include "CilsExecutionFinisherExport.h"

namespace TomoAnalysis::CilsExecution::Plugin {
    class CilsExecutionFinisher_API CilsExecutionFinisher final : public UseCase::ICilsExecutionFinisher{
    public:
        CilsExecutionFinisher();
        ~CilsExecutionFinisher() override;

        auto FinishExecution(int id, const QString& type, const QString& path) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
