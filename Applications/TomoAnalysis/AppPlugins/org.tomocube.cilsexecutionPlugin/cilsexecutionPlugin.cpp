#include "cilsexecutionPlugin.h"

#include <ctkPluginFrameworkLauncher.h>
#include <QtPlugin>

#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#include <MenuEvent.h>
#include <AppEvent.h>

#include "MainWindow.h"

namespace TomoAnalysis::CilsExecution::AppUI {
	struct CilsExecutionPlugin::Impl {
		ctkPluginContext* context{ nullptr };
		MainWindow* mainWindow{ nullptr };
		AppInterfaceTA* appInterface{ nullptr };
	};

	CilsExecutionPlugin* CilsExecutionPlugin::instance = nullptr;

	CilsExecutionPlugin* CilsExecutionPlugin::getInstance() {
		return instance;
	}

	CilsExecutionPlugin::CilsExecutionPlugin() : d(new Impl) {
		d->mainWindow = new MainWindow;
		d->appInterface = new AppInterfaceTA(d->mainWindow);
	}

	CilsExecutionPlugin::~CilsExecutionPlugin() = default;

	auto CilsExecutionPlugin::start(ctkPluginContext* context)->void {
		instance = this;
		d->context = context;

		registerService(context, d->appInterface, plugin_symbolic_name);
	}

	auto CilsExecutionPlugin::stop(ctkPluginContext* context)->void {
		Q_UNUSED(context);
		//Never destroy this plugin
	}

	auto CilsExecutionPlugin::getPluginContext()->ctkPluginContext* const {
		return d->context;
	}
}
