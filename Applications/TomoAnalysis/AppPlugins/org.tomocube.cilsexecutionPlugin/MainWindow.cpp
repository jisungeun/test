#include "MainWindow.h"

#include <QDir>
#include <QDirIterator>
#include <QMessageBox>
#include <QProcess>

#include "AppEvent.h"
#include "AppInterfaceTA.h"
#include "AppManagerTA.h"
#include "ExecutionController.h"
#include "CilsExecutionFinisher.h"
#include "MenuEvent.h"
#include "ui_MainWindow.h"

namespace TomoAnalysis::CilsExecution::AppUI {
	struct MainWindow::Impl {
		QString parentSymbol;

		Ui::MainWindow* ui = nullptr;
		QVariantMap appProperties;

		Plugin::CilsExecutionFinisher* plugin = nullptr;

		std::unique_ptr<Interactor::ExecutionController> controller = nullptr;

		auto InitAppLabels() const -> void {
			const auto manager = AppManagerTA::GetInstance();
			auto* app = dynamic_cast<AppInterfaceTA*>(manager->GetAppInterface(controller->GetCurrentAppName()));
			const auto* widget = dynamic_cast<IMainWindowTA*>(app->GetInterfaceWidget());

			//const QString context("%1 (%2) as %3");
			const QString title("[%1]");
			//ui->contextLabel->setText(context.arg(controller->GetExecutionTitle()).arg(controller->GetExecutionDataId()).arg(controller->GetRole()));
			ui->titleLabel->setText(title.arg(widget->GetDisplayTitle()));
			ui->currentLabel->setText(controller->GetPackageName());
		}
	};

	MainWindow::MainWindow(QWidget* parent) : IMainWindowTA("CILS Executor", "CILS Executor", parent), d{ new Impl() } {
		d->appProperties["Parent-App"] = "CILS Project Manager";
		d->appProperties["AppKey"] = "CILS Executor";
		d->appProperties["Symbol"] = "org.tomocube.cilsexecutionPlugin";
	}

	MainWindow::~MainWindow() = default;

	auto MainWindow::Execute(const QVariantMap& params)->bool {
		if (params.isEmpty()) {
			return true;
		}
		if (false == params.contains("ExecutionType")) {
			return false;
		}
		if (params["ExecutionType"].toString() == "BatchRun") {
			d->controller->InitAppPackage(params["PackageName"].toString(), params["AppNames"].toStringList()); 

			d->controller->SetExecutionId(params["ExecutionId"].toInt());
			d->controller->SetExecutionDataId(params["ExecutionDataId"].toString());
			d->controller->SetExecutionTitle(params["ExecutionTitle"].toString());
			d->controller->SetRole(params["ExecutionRole"].toString());
			d->controller->SetTcfPath(params["TcfPath"].toString());
			d->controller->SetOutputPath(params["OutputPath"].toString());
			d->parentSymbol = params["ParentSymbol"].toString();

			if (const QDir dir(d->controller->GetOutputPath()); !dir.mkpath(".")) {
				QMessageBox::information(this, "failed", "Could not access to output directory.");
				OnCloseBtnClicked();
				return false;
			}

			if (d->controller->HasNextApp())
				d->controller->NextApp();
		}
		return true;
	}

	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
		return nullptr;
	}

	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {}

	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
		return std::make_tuple(false, false);
	}

	auto MainWindow::TryActivate() -> bool {
		d->ui = new Ui::MainWindow;
		d->ui->setupUi(this);
		d->ui->finishBtn->setObjectName("bt-square-primary");
		d->ui->nextBtn->setObjectName("bt-square-primary");
		d->ui->finishBtn->setVisible(false);
		d->ui->nextBtn->setVisible(false);

		d->plugin = new Plugin::CilsExecutionFinisher;
		d->controller = std::make_unique<Interactor::ExecutionController>();
		d->controller->SetViewer(this);

		connect(d->ui->finishBtn, &QPushButton::clicked, this, &MainWindow::OnFinishBtnClicked);
		connect(d->ui->nextBtn, &QPushButton::clicked, this, &MainWindow::OnNextBtnClicked);
		connect(d->ui->closeBtn, &QPushButton::clicked, this, &MainWindow::OnCloseBtnClicked);
		connect(d->ui->discardBtn, &QPushButton::clicked, this, &MainWindow::OnDiscardBtnClicked);
		connect(d->ui->openOutputBtn, &QPushButton::clicked, this, &MainWindow::OnOpenOutputBtnClicked);
		connect(d->ui->openTcfBtn, &QPushButton::clicked, this, &MainWindow::OnOpenTcfBtnClicked);

		return true;
	}

	auto MainWindow::TryDeactivate() -> bool {
		const auto manager = AppManagerTA::GetInstance();
		auto* app = manager->GetAppInterface(d->parentSymbol);
		auto* widget = dynamic_cast<IMainWindowTA*>(app->GetInterfaceWidget());
		widget->SetParameter("ExecutionClosed", nullptr);

		return true;
	}

	auto MainWindow::IsActivate() -> bool {
		return (nullptr != d->ui);
	}

	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
	}

	auto MainWindow::GetFeatureName() const -> QString {
		return "org.tomocube.cilsexecutionPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}

	auto MainWindow::OnFinishBtnClicked() -> void {
		const auto result = QMessageBox::warning(this, "Finish", "Are you sure to finish?", QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			d->controller->Finish({});
			d->ui->finishBtn->setDisabled(true);
		}
	}

	auto MainWindow::OnNextBtnClicked() -> void {
		if (d->controller->HasNextApp())
			d->controller->NextApp();
	}

	auto MainWindow::OnCloseBtnClicked() -> void {
		AppEvent event(AppTypeEnum::APP_CLOSE);
		event.setFullName(d->appProperties["Symbol"].toString());
		event.setAppName(GetDisplayTitle());
		publishSignal(event, d->appProperties["AppKey"].toString());
	}

	auto MainWindow::OnDiscardBtnClicked() -> void {
		const auto result = QMessageBox::warning(this, "Discard", "Your works will be deleted and cannot be restored. Are you sure?", QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			QDirIterator iter(d->controller->GetOutputPath(), QDir::Files | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

			while (iter.hasNext()) {
				QFile::remove(iter.next());
			}

			OnCloseBtnClicked();
		}
	}

	auto MainWindow::OnOpenTcfBtnClicked() -> void {
		QProcess::startDetached("explorer.exe", { d->controller->GetTcfPath() });
	}

	auto MainWindow::OnOpenOutputBtnClicked() -> void {
		QProcess::startDetached("explorer.exe", { d->controller->GetOutputPath() });
	}

	auto MainWindow::Finished(int id) -> void {
		d->ui->finishBtn->setEnabled(true);
		OnCloseBtnClicked();
	}

	auto MainWindow::FinishFailed(int id, QString reason) -> void {
		d->ui->finishBtn->setEnabled(true);
		QMessageBox::information(this, "failed", reason);
	}

	auto MainWindow::CurrentAppChanged(QString appName) -> void {
		d->ui->finishBtn->setVisible(!d->controller->HasNextApp());
		d->ui->nextBtn->setVisible(d->controller->HasNextApp());

		const auto manager = AppManagerTA::GetInstance();
		auto* app = dynamic_cast<AppInterfaceTA*>(manager->GetAppInterface(appName));
		auto* widget = dynamic_cast<IMainWindowTA*>(app->GetInterfaceWidget());

		manager->Activate(appName);
		const auto filePath = d->controller->GetTcfFilePath();

		QString param;
		QVariantMap params;
		params["TCFPath"] = filePath;
		if (appName == "org.tomocube.intersegPlugin") {			
			params["ExecutionType"] = "OpenTCF";
		} else {
			QFile file(d->controller->GetOutputPath() + ".tcpg");
			file.open(QIODevice::WriteOnly);
			file.close();

			const auto procPath = d->controller->GetProcessorPath(widget->GetMetaInfo());
			params["Processor path"] = procPath;
			params["Playground"] = QFileInfo(file).absoluteFilePath();			
		}		
		//app->OpenTcf(param);
		app->Execute(params);

		if (d->ui->appFrame->layout()->count() > 0)
			d->ui->appFrame->layout()->removeItem(d->ui->appFrame->layout()->itemAt(0));
		d->ui->appFrame->layout()->addWidget(widget);

		d->InitAppLabels();
	}

	auto MainWindow::AppChangeFailed(QString reason) -> void {
		QMessageBox::information(this, "Error", reason);
	}
}
