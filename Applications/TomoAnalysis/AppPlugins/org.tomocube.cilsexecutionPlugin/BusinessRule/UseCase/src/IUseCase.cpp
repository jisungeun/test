#include "IUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	IUseCase::IUseCase() {
		finisher = ICilsExecutionFinisher::GetInstance();
		app = Entity::AppPackage::GetInstance();
	    exec = Entity::Execution::GetInstance();
	}

	IUseCase::~IUseCase() = default;
}
