#include "GetProcessorPathUseCase.h"

#include <QDir>
#include <QApplication>

namespace TomoAnalysis::CilsExecution::UseCase {
    struct GetProcessorPathUseCase::Impl {
        QVariantMap meta;
    };

    GetProcessorPathUseCase::GetProcessorPathUseCase(const QVariantMap& meta) : IUseCase(), d(new Impl) {
        d->meta = meta;
    }

    GetProcessorPathUseCase::~GetProcessorPathUseCase() = default;

    auto GetProcessorPathUseCase::Do() -> void {

    }

    auto GetProcessorPathUseCase::GetProcessorPath() const -> QString {
        if (d->meta.contains("Processors")) {
            const QDir dir(QCoreApplication::applicationDirPath() + d->meta["Processors"].toStringList().first());
            auto files = dir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);

            QString path = dir.absolutePath();
            if (!files.empty())
                return dir.absoluteFilePath(files.first());
        }

        return {};
    }
}
