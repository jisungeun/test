#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QFile>

#include "FinishUseCase.h"
#include "Execution.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct FinishUseCase::Impl {
		QMap<QString, QString> map;
		IFinishOutput* output;

		auto RoleToString(Entity::RoleType type) -> QString {
			switch (type) {
			case Entity::RoleType::Any:
				return "any";
			case Entity::RoleType::Assignee:
				return "assignee";
			case Entity::RoleType::Reviewer:
				return "reviewer";
			}

			return {};
		}
	};

	FinishUseCase::FinishUseCase(const QMap<QString, QString>& map, IFinishOutput* output) : IUseCase(), d(new Impl) {
		d->map = map;
		d->output = output;
	}

	FinishUseCase::~FinishUseCase() = default;

	auto FinishUseCase::Do() -> void {
		const QDir dir(exec->GetOutputPath());
		QFile report(dir.absoluteFilePath(exec->GetExecutionTitle() + ".cils"));

		QJsonObject json;
		foreach(const auto & key, d->map)
			json[key] = d->map[key];

		if (report.open(QIODevice::WriteOnly)) {
			report.write(QJsonDocument(json).toJson());
			report.close();

			finisher->FinishExecution(exec->GetExecutionId(), d->RoleToString(exec->GetType()), exec->GetOutputPath());
		} else {
			d->output->FinishFailed(exec->GetExecutionId(), "Could not create a report file.");
		}
	}
}
