#include "SetOutputPathUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetOutputPathUseCase::Impl {
		QString path;
	};

	SetOutputPathUseCase::SetOutputPathUseCase(const QString& path) : IUseCase(), d(new Impl) {
		d->path = path;
	}

	SetOutputPathUseCase::~SetOutputPathUseCase() = default;

    auto SetOutputPathUseCase::Do() -> void {
		exec->SetOutputPath(d->path);
    }
}
