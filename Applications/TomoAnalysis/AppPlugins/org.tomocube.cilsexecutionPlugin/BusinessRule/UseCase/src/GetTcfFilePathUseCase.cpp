#include "GetTcfFilePathUseCase.h"

#include <QDir>

namespace TomoAnalysis::CilsExecution::UseCase {
    struct GetTcfFilePathUseCase::Impl {
        QString path;
    };

    GetTcfFilePathUseCase::GetTcfFilePathUseCase(const QString& tcfDirPath) : IUseCase(), d(new Impl) {
        d->path = tcfDirPath;
    }

    GetTcfFilePathUseCase::~GetTcfFilePathUseCase() = default;

    auto GetTcfFilePathUseCase::Do() -> void {
    }

    auto GetTcfFilePathUseCase::GetFilePath() const -> QString {
        const QDir dir(d->path);
        auto files = dir.entryList({ "*.tcf" }, QDir::Filter::Files, QDir::SortFlag::Name);

        if (!files.empty())
            return dir.absoluteFilePath(files.first());
        return {};
    }
}
