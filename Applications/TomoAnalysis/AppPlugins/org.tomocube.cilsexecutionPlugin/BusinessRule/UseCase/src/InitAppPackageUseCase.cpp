#include "InitAppPackageUseCase.h"
#include "AppPackage.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct InitAppPackageUseCase::Impl {
		QString name;
		QStringList names;
	};

	InitAppPackageUseCase::InitAppPackageUseCase(const QString& packageName, const QStringList& appNames) : IUseCase(), d(new Impl) {
		d->name = packageName;
		d->names = appNames;
	}

	InitAppPackageUseCase::~InitAppPackageUseCase() = default;

    auto InitAppPackageUseCase::Do() -> void {
		app->InitPackage(d->name, d->names);
    }
}
