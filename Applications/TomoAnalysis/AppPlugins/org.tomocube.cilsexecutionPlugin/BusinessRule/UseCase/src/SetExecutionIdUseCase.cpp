#include "SetExecutionIdUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetExecutionIdUseCase::Impl {
		int id;
	};

	SetExecutionIdUseCase::SetExecutionIdUseCase(int id) : IUseCase(), d(new Impl) {
		d->id = id;
	}

	SetExecutionIdUseCase::~SetExecutionIdUseCase() = default;

    auto SetExecutionIdUseCase::Do() -> void {
		exec->SetExecutionId(d->id);
    }
}
