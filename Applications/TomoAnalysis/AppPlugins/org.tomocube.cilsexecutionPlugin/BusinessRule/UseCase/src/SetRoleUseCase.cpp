#include "SetRoleUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetRoleUseCase::Impl {
		Entity::RoleType type;
	};

	SetRoleUseCase::SetRoleUseCase(Entity::RoleType type) : IUseCase(), d(new Impl) {
		d->type = type;
	}

	SetRoleUseCase::~SetRoleUseCase() = default;

    auto SetRoleUseCase::Do() -> void {
		exec->SetType(d->type);
    }
}
