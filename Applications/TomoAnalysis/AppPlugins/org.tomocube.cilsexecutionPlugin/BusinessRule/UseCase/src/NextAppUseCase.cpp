#include "NextAppUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
    struct NextAppUseCase::Impl {
        INextAppOutput* output = nullptr;
    };

    NextAppUseCase::~NextAppUseCase() = default;

    NextAppUseCase::NextAppUseCase(INextAppOutput* output) : IUseCase(), d(new Impl) {
        d->output = output;
    }

    auto NextAppUseCase::Do() -> void {
        if (app->HasNext()) {
            const auto& name = app->Next();

            if (d->output)
                d->output->CurrentAppChanged(name);
        } else {
            if (d->output)
                d->output->AppChangeFailed("No more application found.");
        }
    }
}
