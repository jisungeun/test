#include "SetExecutionTitleUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetExecutionTitleUseCase::Impl {
		QString name;
	};

	SetExecutionTitleUseCase::SetExecutionTitleUseCase(const QString& name) : IUseCase(), d(new Impl) {
		d->name = name;
	}

	SetExecutionTitleUseCase::~SetExecutionTitleUseCase() = default;

    auto SetExecutionTitleUseCase::Do() -> void {
		exec->SetExecutionTitle(d->name);
    }
}
