#include "SetTcfPathUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetTcfPathUseCase::Impl {
		QString path;
	};

	SetTcfPathUseCase::SetTcfPathUseCase(const QString& path) : IUseCase(), d(new Impl) {
		d->path = path;
	}

	SetTcfPathUseCase::~SetTcfPathUseCase() = default;

    auto SetTcfPathUseCase::Do() -> void {
		exec->SetTcfPath(d->path);
    }
}
