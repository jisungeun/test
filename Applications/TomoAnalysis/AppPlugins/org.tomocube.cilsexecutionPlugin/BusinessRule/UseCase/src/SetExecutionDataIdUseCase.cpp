#include "SetExecutionDataIdUseCase.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	struct SetExecutionDataIdUseCase::Impl {
		QString id;
	};

	SetExecutionDataIdUseCase::SetExecutionDataIdUseCase(const QString& id) : IUseCase(), d(new Impl) {
		d->id = id;
	}

	SetExecutionDataIdUseCase::~SetExecutionDataIdUseCase() = default;

    auto SetExecutionDataIdUseCase::Do() -> void {
		exec->SetExecutionDataId(d->id);
    }
}
