#pragma once

#include <QMap>

#include "IUseCase.h"
#include "IFinishOutput.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API FinishUseCase : public IUseCase {
	public:
		FinishUseCase(const QMap<QString, QString>& map, IFinishOutput* output = nullptr);
        ~FinishUseCase() override;
		
        auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}
