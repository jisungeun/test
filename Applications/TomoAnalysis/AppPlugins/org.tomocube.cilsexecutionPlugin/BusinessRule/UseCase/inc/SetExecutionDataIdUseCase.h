#pragma once

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API SetExecutionDataIdUseCase : public IUseCase {
	public:
		SetExecutionDataIdUseCase(const QString& id);
        ~SetExecutionDataIdUseCase() override;
		
        auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}
