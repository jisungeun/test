#pragma once

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API SetExecutionTitleUseCase : public IUseCase {
	public:
		SetExecutionTitleUseCase(const QString& name);
        ~SetExecutionTitleUseCase() override;
		
        auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}
