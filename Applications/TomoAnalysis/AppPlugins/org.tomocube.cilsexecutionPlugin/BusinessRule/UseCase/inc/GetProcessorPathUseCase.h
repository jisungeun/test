#pragma once

#include <QVariantMap>

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API GetProcessorPathUseCase : public IUseCase {
	public:
		GetProcessorPathUseCase(const QVariantMap& meta);
		~GetProcessorPathUseCase() override;

		auto Do() -> void override;
		auto GetProcessorPath() const -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
