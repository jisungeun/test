#pragma once

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API SetOutputPathUseCase : public IUseCase {
	public:
		SetOutputPathUseCase(const QString& path);
        ~SetOutputPathUseCase() override;
		
        auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}
