#pragma once

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API InitAppPackageUseCase : public IUseCase {
	public:
		InitAppPackageUseCase(const QString& packageName, const QStringList& appNames);
        ~InitAppPackageUseCase() override;
		
        auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}
