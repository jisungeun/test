#pragma once

#include "IUseCase.h"
#include "INextAppOutput.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API NextAppUseCase : public IUseCase {
	public:
		NextAppUseCase(INextAppOutput* output = nullptr);
		~NextAppUseCase() override;

		auto Do() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
