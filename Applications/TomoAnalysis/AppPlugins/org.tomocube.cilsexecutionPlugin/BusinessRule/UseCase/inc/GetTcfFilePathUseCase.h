#pragma once

#include "IUseCase.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API GetTcfFilePathUseCase : public IUseCase {
	public:
		GetTcfFilePathUseCase(const QString& tcfDirPath);
		~GetTcfFilePathUseCase() override;

		auto Do() -> void override;
		[[nodiscard]] auto GetFilePath() const -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
