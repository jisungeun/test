#pragma once

#include "AppPackage.h"
#include "Execution.h"
#include "ICilsExecutionFinisher.h"

#include "CilsExecutionUseCaseExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionUseCase_API IUseCase {
	public:
		IUseCase();
		virtual ~IUseCase();

		virtual auto Do()->void = 0;

	protected:
		ICilsExecutionFinisher* finisher = nullptr;
        Entity::AppPackage* app = nullptr;
        Entity::Execution* exec = nullptr;
	};
}
