#pragma once

#include <QString>

#include "IOutputPort.h"

#include "CilsExecutionPortExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionPort_API INextAppOutput : public IOutputPort {
	public:
		~INextAppOutput() override = default;

		virtual auto CurrentAppChanged(QString appName) -> void = 0;
		virtual auto AppChangeFailed(QString reason) -> void = 0;
	};
}
