#pragma once

#include <QString>

#include "IOutputPort.h"

#include "CilsExecutionPortExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionPort_API IFinishOutput : public IOutputPort {
	public:
        ~IFinishOutput() override = default;

		virtual auto Finished(int id) -> void = 0;
		virtual auto FinishFailed(int id, QString reason) -> void = 0;
	};
}
