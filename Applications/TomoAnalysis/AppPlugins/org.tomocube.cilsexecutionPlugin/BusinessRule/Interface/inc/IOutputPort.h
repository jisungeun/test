#pragma once

#include "CilsExecutionPortExport.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionPort_API IOutputPort {
	public:
		virtual ~IOutputPort() = default;
	};
}