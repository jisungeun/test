#pragma once

#include <memory>

#include <QString>

#include "IFinishOutput.h"
#include "INextAppOutput.h"

#include "CilsExecutionPortExport.h"
#include "OutputPortHandler.h"

namespace TomoAnalysis::CilsExecution::UseCase {
	class CilsExecutionPort_API ICilsExecutionFinisher {
	public:
		[[nodiscard]] static auto GetInstance()->ICilsExecutionFinisher*;

		[[nodiscard]] auto GetFinishOutputHandler() const -> OutputPortHandler<IFinishOutput>&;

		virtual auto FinishExecution(int id, const QString& type, const QString& path) -> void = 0;

	protected:
		ICilsExecutionFinisher();
		virtual ~ICilsExecutionFinisher();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
