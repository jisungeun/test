#pragma once

#include <QVector>

#include <functional>

namespace TomoAnalysis::CilsExecution::UseCase {
	template<typename T>
	class OutputPortHandler {
	public:
		auto AddObserver(T* output) -> void;
		auto RemoveObserver(T* output) -> void;

		auto Notify(std::function<void(T*)>&& predicate) -> void;
		auto Notify(const std::function<void(T*)>& predicate) -> void;

	private:
		QVector<T*> outputs;
	};

	template <typename T>
	auto OutputPortHandler<T>::AddObserver(T* output) -> void {
		if (output && !outputs.contains(output))
			outputs.push_back(output);
	}

	template <typename T>
	auto OutputPortHandler<T>::RemoveObserver(T* output) -> void {
		if (output)
			outputs.removeOne(output);
	}

	template <typename T>
	auto OutputPortHandler<T>::Notify(std::function<void(T*)>&& predicate) -> void {
		for (auto* o : outputs)
			predicate(o);
	}

	template <typename T>
	auto OutputPortHandler<T>::Notify(const std::function<void(T*)>& predicate) -> void {
		for (auto* o : outputs)
			predicate(o);
	}
}
