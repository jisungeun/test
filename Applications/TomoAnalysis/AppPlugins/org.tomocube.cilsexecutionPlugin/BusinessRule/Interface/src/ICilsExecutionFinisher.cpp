#include "ICilsExecutionFinisher.h"

namespace TomoAnalysis::CilsExecution::UseCase {
    static ICilsExecutionFinisher* instance = nullptr;

    struct ICilsExecutionFinisher::Impl {
        OutputPortHandler<IFinishOutput> finishOutput;
    };

    auto ICilsExecutionFinisher::GetInstance() -> ICilsExecutionFinisher* {
        return instance;
    }

    auto ICilsExecutionFinisher::GetFinishOutputHandler() const -> OutputPortHandler<IFinishOutput>& {
        return d->finishOutput;
    }

    ICilsExecutionFinisher::ICilsExecutionFinisher() : d(new Impl) {
        instance = this;
    }

    ICilsExecutionFinisher::~ICilsExecutionFinisher() = default;
}
