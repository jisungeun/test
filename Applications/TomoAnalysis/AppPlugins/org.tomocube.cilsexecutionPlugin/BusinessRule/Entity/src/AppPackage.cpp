#include "AppPackage.h"

namespace TomoAnalysis::CilsExecution::Entity {
	struct AppPackage::Impl {
		QString packageName;
		QStringList appNames;
		int index = 0;
	};

	static AppPackage* instance = nullptr;

    auto AppPackage::GetInstance() -> AppPackage* {
		if (!instance)
			instance = new AppPackage;

		return instance;
    }

	auto AppPackage::InitPackage(const QString& packageName, const QStringList& appNames) -> void {
		d->packageName = packageName;
		d->appNames = appNames;
		d->index = 0;
	}

    auto AppPackage::HasNext() const -> bool {
		return d->index < d->appNames.count();
    }

    auto AppPackage::Next() const -> const QString& {
		return d->appNames[d->index++];
    }

    auto AppPackage::GetCount() const -> int {
		return d->appNames.count();
    }

    auto AppPackage::GetIndex() const -> int {
		return d->index - 1;
    }

	auto AppPackage::GetCurrentAppName() const -> const QString& {
		return d->appNames[d->index - 1];
    }

	auto AppPackage::GetPackageName() const -> const QString& {
		return d->packageName;
    }

    auto AppPackage::GetAppNames() const -> const QStringList& {
		return d->appNames;
    }

    AppPackage::AppPackage() : d(new Impl) {}

	AppPackage::~AppPackage() = default;
}
