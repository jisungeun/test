#include "Execution.h"

namespace TomoAnalysis::CilsExecution::Entity {
	struct Execution::Impl {
		QString dataId;
		QString title;
		QString packageName;
		QStringList appNames;
		int id = -1;
		RoleType type = RoleType::Any;
		QString outputPath;
		QString tcfPath;
	};

	static Execution* instance = nullptr;

	auto Execution::GetInstance() -> Execution* {
		if (!instance)
			instance = new Execution;

		return instance;
	}

    auto Execution::GetExecutionDataId() const -> const QString& {
		return d->dataId;
    }

    auto Execution::GetExecutionTitle() const -> const QString& {
		return d->title;
    }

    Execution::Execution() : d(new Impl) {}

	Execution::~Execution() = default;

	auto Execution::GetExecutionId() const -> int {
		return d->id;
	}

	auto Execution::GetType() const -> RoleType {
		return d->type;
	}

	auto Execution::GetOutputPath() const -> const QString& {
		return d->outputPath;
	}

    auto Execution::GetTcfPath() const -> const QString& {
		return d->tcfPath;
    }

	auto Execution::SetExecutionDataId(const QString& name) -> void {
		d->dataId = name;
	}

	auto Execution::SetExecutionTitle(const QString& name) -> void {
		d->title = name;
	}

    auto Execution::SetExecutionId(int id) -> void {
		d->id = id;
	}

	auto Execution::SetType(RoleType type) -> void {
		d->type = type;
	}
	
	auto Execution::SetOutputPath(const QString& path) -> void {
		d->outputPath = path;
	}

    auto Execution::SetTcfPath(const QString& path) -> void {
		d->tcfPath = path;
    }
}
