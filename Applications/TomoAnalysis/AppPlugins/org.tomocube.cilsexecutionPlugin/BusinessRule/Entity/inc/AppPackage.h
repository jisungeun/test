#pragma once

#include <QStringList>

#include "CilsExecutionEntityExport.h"

namespace TomoAnalysis::CilsExecution::Entity {
	class CilsExecutionEntity_API AppPackage {
	public:
		static auto GetInstance()->AppPackage*;

		auto InitPackage(const QString& packageName, const QStringList& appNames) -> void;

		[[nodiscard]] auto HasNext() const -> bool;
		[[nodiscard]] auto Next() const -> const QString&;

		[[nodiscard]] auto GetCount() const -> int;
		[[nodiscard]] auto GetIndex() const -> int;
		[[nodiscard]] auto GetCurrentAppName() const -> const QString&;
		[[nodiscard]] auto GetPackageName() const -> const QString&;
		[[nodiscard]] auto GetAppNames() const -> const QStringList&;

	private:
		AppPackage();
		~AppPackage();

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}