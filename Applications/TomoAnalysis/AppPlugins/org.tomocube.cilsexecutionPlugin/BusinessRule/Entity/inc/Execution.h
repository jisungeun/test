#pragma once

#include <QStringList>

#include <memory>

#include "CilsExecutionEntityExport.h"

namespace TomoAnalysis::CilsExecution::Entity {
    enum class CilsExecutionEntity_API RoleType {
        Any,
        Assignee,
        Reviewer,
    };

    class CilsExecutionEntity_API Execution {
    public:
        static auto GetInstance()->Execution*;

        [[nodiscard]] auto GetExecutionDataId() const->const QString&;
        [[nodiscard]] auto GetExecutionTitle() const->const QString&;
        [[nodiscard]] auto GetExecutionId() const -> int;
        [[nodiscard]] auto GetType() const->RoleType;
        [[nodiscard]] auto GetOutputPath() const -> const QString&;
        [[nodiscard]] auto GetTcfPath() const -> const QString&;

        auto SetExecutionDataId(const QString& name) -> void;
        auto SetExecutionTitle(const QString& name) -> void;
        auto SetExecutionId(int id) -> void;
        auto SetType(RoleType type) -> void;
        auto SetOutputPath(const QString& path) -> void;
        auto SetTcfPath(const QString& path) -> void;

    private:
        Execution();
        ~Execution();
        
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
