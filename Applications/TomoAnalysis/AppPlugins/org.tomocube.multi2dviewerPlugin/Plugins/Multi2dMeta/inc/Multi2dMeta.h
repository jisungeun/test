#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "Multi2dMetaExport.h"

namespace TomoAnalysis::Multi2DViewer::Plugins {
	class Multi2dMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.multi2dviewerMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "2D Image Gallery"; }
		auto GetFullName()const->QString override { return "org.tomocube.multi2dviewerPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}