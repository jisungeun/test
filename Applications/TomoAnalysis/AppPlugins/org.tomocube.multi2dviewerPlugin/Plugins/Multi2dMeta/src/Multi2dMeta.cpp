#include "Multi2dMeta.h"

namespace TomoAnalysis::Multi2DViewer::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "Project Manager";
		d->appProperties["AppKey"] = "2D Image Gallery";
		d->appProperties["hasSingleRun"] = false;
		d->appProperties["hasBatchRun"] = true;
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}