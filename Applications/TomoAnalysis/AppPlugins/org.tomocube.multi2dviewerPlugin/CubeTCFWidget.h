#pragma once

#include <memory>
#include <QWidget>

#include <Cube.h>
#include <SimpleTCF2DViewer.h>

namespace TomoAnalysis::Multi2DViewer::Plugins {
    class CubeTCFWidget : public QWidget  {
        Q_OBJECT

    public:
        CubeTCFWidget(QWidget* parent=nullptr);
        ~CubeTCFWidget();

        void SetCube(const TomoAnalysis::Cube::Pointer& cube) const;
        void FitZoomAll() const;
        void SetKeepSquare(bool keep) const;

        void SetImageType(const TC::TCF2DWidget::ImageType& type) const;

        auto GetCurrentPage() const ->int;
        auto GetCount() const ->int;
        auto GetIndex(TC::SimpleTCF2DViewer* widget) const ->int;

        auto GetPath(const int& index) const ->QString;
        void Swap(const int& from, const int& to) const;

        void SetVisibleSubUI(bool visible);

    private slots:
        void on_pageComboBox_currentIndexChanged(int index) const;

        void on_prevPageButton_clicked(bool) const;
        void on_nextPageButton_clicked(bool) const;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}