#include "TCFInfoWidgetControl.h"

namespace TomoAnalysis::Multi2DViewer::AppUI {
    struct TCFInfoWidgetControl::Impl {
    };

    TCFInfoWidgetControl::TCFInfoWidgetControl() : d{ new Impl } {
    }

    TCFInfoWidgetControl::~TCFInfoWidgetControl() {
    }

    auto TCFInfoWidgetControl::ReadTCFCommonMeta(const QString& path, TC::IO::TCFMetaReader::Meta::Pointer& meta)->bool {
        if(true == path.isEmpty()) {
            return false;
        }

        auto reader = TC::IO::TCFMetaReader();
        meta = reader.Read(path);

        if(nullptr == meta) {
            return false;
        }

        return true;
    }

    auto TCFInfoWidgetControl::GenerateFileInfo(const TC::IO::TCFMetaReader::Meta::Pointer& meta,
                                                  QList<QPair<QString, QString>>& infoList)->bool {
        if(nullptr == meta) {
            return false;
        }

        auto MakeInfo = [=](const QString& key, const QString& value) {
            return QPair<QString, QString>(key, value);
        };

        infoList.push_back(MakeInfo("Title / Device / Host",
                                    QString("%1 / %2 / %3").arg(meta->common.title)
                                                           .arg(meta->common.deviceSerial)
                                                           .arg("N/A")));

        infoList.push_back(MakeInfo("Recorded / Created",
                                    QString("%1 / %2").arg(meta->common.recordingTime)
                                                      .arg(meta->common.createDate)));

        infoList.push_back(MakeInfo("Magnification / NA",
                                    QString("%1 / %2").arg(QString::number(meta->info.device.magnification, 'f', 1))
                                                      .arg(QString::number(meta->info.device.na, 'f', 2))));
        infoList.push_back(MakeInfo("Medium RI",
                                    QString::number(meta->info.device.ri, 'f', 3)));

        infoList.push_back(MakeInfo("Camera Shutter (ms)",
                                    QString::number(meta->info.imaging.cameraShutter, 'f', 3)));

        auto riMIPResolution = 0.f;
        if (true == meta->data.data2DMIP.exist) {
            riMIPResolution = meta->data.data2DMIP.resolutionX;
        }

        infoList.push_back(MakeInfo("RI(MIP) Resolution (um)",
                                    QString::number(riMIPResolution, 'f', 3)));

        return true;
    }
}
