#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::Multi2DViewer::AppUI {
    class TCFInfoWidget : public QWidget {
        Q_OBJECT
        
    public:
        TCFInfoWidget(QWidget* parent=nullptr);
        ~TCFInfoWidget();

        void SetTCF(const QString& path, const int& pageIndex, const int& widgetIndex);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}