#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::Multi2DViewer::AppUI {
    class ReplaceDialog : public QDialog {
        Q_OBJECT
        
    public:
        ReplaceDialog(int count, QWidget* parent=nullptr);
        ~ReplaceDialog();

        static auto ReplaceIndex(const int& count, const QString& currentIndex)->QString;

        void SetCurrentIndex(const QString& index) const;
        auto GetTargetIndex() const ->QString;

    protected slots:
        void on_okButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}