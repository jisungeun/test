#include <QFileInfo>

#include "TCFInfoWidgetControl.h"

#include "ui_TCFInfoWidget.h"
#include "TCFInfoWidget.h"


namespace TomoAnalysis::Multi2DViewer::AppUI {
    struct TCFInfoWidget::Impl {
        Ui::TCFInfoWidget* ui{ nullptr };

        std::shared_ptr<TCFInfoWidgetControl> control{ nullptr };
    };

    TCFInfoWidget::TCFInfoWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->ui = new Ui::TCFInfoWidget();
        d->ui->setupUi(this);

        this->setWindowTitle("View Information");

        d->control.reset(new TCFInfoWidgetControl());
    }

    TCFInfoWidget::~TCFInfoWidget() {
        delete d->ui;
    }

    void TCFInfoWidget::SetTCF(const QString& path, const int& pageIndex, const int& widgetIndex) {
        if(true == path.isEmpty()) {
            return;
        }

        // read meta info
        std::shared_ptr<TC::IO::TCFMetaReader::Meta> meta{ nullptr };
        if(false == d->control->ReadTCFCommonMeta(path, meta)) {
            return;
        }

        QList<QPair<QString, QString>> infoList;
        if (false == d->control->GenerateFileInfo(meta, infoList)) {
            return;
        }

        // init ui
        d->ui->infoTableWidget->clear();

        d->ui->infoTableWidget->setRowCount(infoList.size() + 2);

        const QStringList headers{ "Name", "Value" };
        d->ui->infoTableWidget->setColumnCount(headers.size());
        d->ui->infoTableWidget->setHorizontalHeaderLabels(headers);

        // file name
        const QFileInfo fileInfo(path);

        auto fileNameKeyItem = new QTableWidgetItem("File Name");
        fileNameKeyItem->setFlags(fileNameKeyItem->flags() ^ Qt::ItemIsEditable);

        auto fileNameValueItem = new QTableWidgetItem(fileInfo.fileName());
        fileNameValueItem->setFlags(fileNameValueItem->flags() ^ Qt::ItemIsEditable);

        d->ui->infoTableWidget->setItem(0, 0, fileNameKeyItem);
        d->ui->infoTableWidget->setItem(0, 1, fileNameValueItem);

        // image position
        auto imagePosition = QString("%1-%2").arg(pageIndex).arg(widgetIndex);

        auto imagePositionKeyItem = new QTableWidgetItem("Image Position");
        imagePositionKeyItem->setFlags(imagePositionKeyItem->flags() ^ Qt::ItemIsEditable);

        auto imagePositionValueItem = new QTableWidgetItem(imagePosition);
        imagePositionValueItem->setFlags(imagePositionValueItem->flags() ^ Qt::ItemIsEditable);

        d->ui->infoTableWidget->setItem(1, 0, imagePositionKeyItem);
        d->ui->infoTableWidget->setItem(1, 1, imagePositionValueItem);

        // meta info
        for(auto i = 0; i < infoList.size(); ++i) {
            auto keyItem = new QTableWidgetItem(infoList.at(i).first);
            keyItem->setFlags(keyItem->flags() ^ Qt::ItemIsEditable);

            auto valueItem = new QTableWidgetItem(infoList.at(i).second);
            valueItem->setFlags(valueItem->flags() ^ Qt::ItemIsEditable);

            d->ui->infoTableWidget->setItem(i + 2, 0, keyItem);
            d->ui->infoTableWidget->setItem(i + 2, 1, valueItem);
        }

        d->ui->infoTableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
        d->ui->infoTableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);                
    }
}
