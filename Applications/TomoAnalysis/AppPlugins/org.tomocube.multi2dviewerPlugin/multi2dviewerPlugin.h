#pragma once

#include <IAppActivator.h>
#include <MenuEvent.h>

const QString plugin_symbolic_name = "org.tomocube.multi2dviewerPlugin";

namespace TomoAnalysis::Multi2DViewer::AppUI {
    class multi2dviewerPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.multi2dviewerPlugin")
            
    public:
        static multi2dviewerPlugin* getInstance();

        multi2dviewerPlugin();
        ~multi2dviewerPlugin();

        auto start(ctkPluginContext* context)->void override;
        //auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                            

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static multi2dviewerPlugin* instance;
    };
}