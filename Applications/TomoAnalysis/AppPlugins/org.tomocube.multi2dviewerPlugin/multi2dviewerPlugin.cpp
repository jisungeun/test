#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#include <MainWindow.h>

#include "multi2dviewerPlugin.h"

namespace TomoAnalysis::Multi2DViewer::AppUI {
    struct multi2dviewerPlugin::Impl {
        Impl() = default;
        ~Impl() = default;
        ctkPluginContext* context{ nullptr };        
        MainWindow* mainWindow{ nullptr };
        AppInterfaceTA* appInterface{ nullptr };
    };

    multi2dviewerPlugin* multi2dviewerPlugin::instance = 0;

    multi2dviewerPlugin* multi2dviewerPlugin::getInstance(){        
        return instance;
    }

    multi2dviewerPlugin::multi2dviewerPlugin() : d(new Impl) {        
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);
    }

    multi2dviewerPlugin::~multi2dviewerPlugin() {
    }

    auto multi2dviewerPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);
    }
    
    /*auto multi2dviewerPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);        
    }*/

    auto multi2dviewerPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }   
}
