#pragma once

#include <memory>
#include <QList>
#include <QPair>
#include <TCFMetaReader.h>

namespace TomoAnalysis::Multi2DViewer::AppUI {
    class TCFInfoWidgetControl {
    public:
        TCFInfoWidgetControl();
        ~TCFInfoWidgetControl();

        auto ReadTCFCommonMeta(const QString& path, TC::IO::TCFMetaReader::Meta::Pointer& meta)->bool;
        auto GenerateFileInfo(const TC::IO::TCFMetaReader::Meta::Pointer& meta, QList<QPair<QString, QString>>& infoList)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
