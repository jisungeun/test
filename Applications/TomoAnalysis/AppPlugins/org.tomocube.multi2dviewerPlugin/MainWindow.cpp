#include <iostream>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QResizeEvent>
#include <QSettings>
#include <QStandardPaths>
#include <QScrollBar>

#include <AppEvent.h>
#include <MenuEvent.h>

#include <ToastMessageBox.h>
#include <ProjectStorage.h>

#include "CubeTCFWidget.h"
#include "TCFInfoWidget.h"
#include "ReplaceDialog.h"

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::Multi2DViewer::AppUI {
	using namespace TC::Framework;
	struct MainWindow::Impl {
		Ui::MainWindow* ui{ nullptr };

		QList<Plugins::CubeTCFWidget*> cubeTcfWidgets;

		TC::SimpleTCF2DViewer* currentWidget{ nullptr };

		TCFInfoWidget* infoWidget{ nullptr };

		QMap<QString, IParameter::Pointer> share_param;

		QVariantMap appProperties;
	};

	MainWindow::MainWindow(QWidget* parent)
		: IMainWindowTA("2D Image Gallery", "2D Image Gallery", parent)
		, d{ new Impl() }
	{
		d->appProperties["Parent-App"] = "Project Manager";
		d->appProperties["AppKey"] = "2D Image Gallery";
		d->appProperties["hasSingleRun"] = false;
		d->appProperties["hasBatchRun"] = true;
	}

	MainWindow::~MainWindow() {
		
	}
	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        bool isBatchRun = true;
		bool isSingleRun = false;
		return std::make_tuple(isSingleRun,isBatchRun);
    }

	auto MainWindow::Execute(const QVariantMap& params)->bool {
		if (params.isEmpty()) {
			return true;
		}
		if (false == params.contains("ExecutionType")) {
			return false;
		}
		if (params["ExecutionType"] == "BatchRun") {
			return this->ExecuteBatchRun(params);
		}
		return true;
	}

	auto MainWindow::ExecuteBatchRun(const QVariantMap& params)->bool {
		auto playgroundPath = params["Playground path"].toString();
		auto hyperName = params["Hypercube name"].toString();

		auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
		playgroundDir.cdUp();

		const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());

		const auto parentProject = ProjectManager::Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
		if (nullptr == parentProject) {
			return false;
		}
		const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
		if (nullptr == playground) {
			return false;
		}

		auto hyperList = playground->GetHyperCubeList();
		HyperCube::Pointer found{ nullptr };
		for (const auto& hcube : hyperList) {
			if (hcube->GetName().compare(hyperName) == 0) {
				found = hcube;
				break;
			}
		}
		if (nullptr == found) {
			return false;
		}
		this->Show2DMultiViewer(found);
	}		
	auto MainWindow::Show2DMultiViewer(const HyperCube::Pointer& hyperCube) ->void {
	    if(nullptr == hyperCube) {
			return;
		}

		d->cubeTcfWidgets.clear();
		d->ui->title->setText(hyperCube->GetName());

		auto mainWidget = new QWidget;
		mainWidget->setObjectName("panel-contents");

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(10, 0, 10, 0);
		layout->setSpacing(0);

		auto cubeList = hyperCube->GetCubeList();
		for(const auto& cube : cubeList) {
			auto widget = new Plugins::CubeTCFWidget(this);
			widget->SetCube(cube);

			if (layout->count() > 0) {
				auto line = new QFrame(mainWidget);
				line->setObjectName("line-separator-section");
				line->setFixedHeight(1);
				line->setFrameShape(QFrame::HLine);
				line->setFrameShadow(QFrame::Sunken);

			    layout->addWidget(line);
			}

			layout->addWidget(widget);			

			d->cubeTcfWidgets.append(widget);
		}

		layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

		mainWidget->setLayout(layout);

		d->ui->scrollArea->setWidget(mainWidget);
		d->ui->scrollArea->setContentsMargins(0, 0, 0, 0);

		d->ui->htButton->setChecked(true);

		this->resizeEvent(new QResizeEvent(this->size(), this->size()));
	}

	void MainWindow::resizeEvent(QResizeEvent* event) {
		Q_UNUSED(event)
		//const auto height = d->ui->scrollArea->height() / 2 - 9; // margin
		const auto height = d->ui->scrollArea->height() / 2; // margin

		for(auto widget : d->cubeTcfWidgets) {
			if (nullptr == widget) {
				continue;
			}

			widget->setMinimumHeight(height);
		}
	}

	bool MainWindow::eventFilter(QObject* obj, QEvent* event) {
		if(event->type() == QEvent::MouseButtonPress) {
			auto selectedWidget = dynamic_cast<TC::SimpleTCF2DViewer*>(obj);
			if(selectedWidget == d->currentWidget) {
				selectedWidget->SetSelected(false);
				d->currentWidget = nullptr;
				return true;
			}

		    if(nullptr != d->currentWidget) {
				d->currentWidget->SetSelected(false);
			}

			d->currentWidget = dynamic_cast<TC::SimpleTCF2DViewer*>(obj);
			d->currentWidget->SetSelected(true);
		}
		else if ( event->type() == QEvent::KeyPress) {
            const auto keyEvent = dynamic_cast<QKeyEvent*>(event);
			if (keyEvent->key() == Qt::Key_Escape) {
				if (nullptr != d->currentWidget) {
					d->currentWidget->SetSelected(false);
					d->currentWidget = nullptr;
					return true;
				}
			}
		}
		else if(event->type() == QEvent::Wheel) {
			const auto wheelEvent = dynamic_cast<QWheelEvent*>(event);
			auto wheelDirection = wheelEvent->angleDelta().y();
			if (d->cubeTcfWidgets.count() < 3) {//less than 3 cubes
				return true;//do nothing
			}
			auto areaHeight = d->ui->scrollArea->height();
			auto curPosition = d->ui->scrollArea->verticalScrollBar()->value();
			auto singleStep = areaHeight / 2 - 59;//icon size
			if(wheelDirection > 0) {//upper direction				
				auto new_pos = curPosition - singleStep;
				if(new_pos < 0 ) {
					new_pos = 0;
				}
				d->ui->scrollArea->verticalScrollBar()->setValue(new_pos);
			}else {//lower direction				
				auto new_pos = curPosition + singleStep;
				if(new_pos > areaHeight) {
					new_pos = areaHeight;
				}
				d->ui->scrollArea->verticalScrollBar()->setValue(new_pos);
			}			
			return true;
		}

		return false;
	}

	void MainWindow::on_htButton_clicked(bool) const {
		for (auto widget : d->cubeTcfWidgets) {
			if(nullptr == widget) {
			    continue;
			}

			widget->SetImageType(TC::TCF2DWidget::ImageType::HT);
		}
	}

	void MainWindow::on_flButton_clicked(bool) const {
		for (auto widget : d->cubeTcfWidgets) {
			if (nullptr == widget) {
				continue;
			}

			widget->SetImageType(TC::TCF2DWidget::ImageType::FL);
		}
	}

	void MainWindow::on_bfButton_clicked(bool) const {
		for (auto widget : d->cubeTcfWidgets) {
			if (nullptr == widget) {
				continue;
			}

			widget->SetImageType(TC::TCF2DWidget::ImageType::BF);
		}
	}

	void MainWindow::on_inverseGrayButton_clicked(bool) const {
	    
	}

	void MainWindow::on_scaleBarButton_clicked(bool) const {
	    
	}

	void MainWindow::on_infoButton_clicked(bool) const {
		if(nullptr == d->currentWidget) {
		    return;
		}

		const auto path = d->currentWidget->GetTCFPath();
		if(true == path.isEmpty()) {
			auto toastMessageBox = new TC::ToastMessageBox;
			toastMessageBox->Show(tr("Selected item is invalid."));
			return;
		}

		auto pageIndex = -1;
		auto widgetIndex = -1;
		for(auto i = 0; i < d->cubeTcfWidgets.size(); ++i) {
            const auto cubeTcfWidget = d->cubeTcfWidgets.at(i);
			if(nullptr == cubeTcfWidget) {
			    continue;
			}

			widgetIndex = cubeTcfWidget->GetIndex(d->currentWidget);
			if(0 <= widgetIndex) {
				pageIndex = cubeTcfWidget->GetCurrentPage();
			    break;
			}
		}

		if(-1 == pageIndex || -1 == widgetIndex) {
			return;
		}

		if(nullptr == d->infoWidget) {
			d->infoWidget = new TCFInfoWidget(d->ui->centralWidget);
			d->infoWidget->setWindowFlag(Qt::Window);
		}

		d->infoWidget->SetTCF(path, pageIndex + 1, widgetIndex + 1);
	    d->infoWidget->show();

		if (false == d->infoWidget->isActiveWindow()){
		    d->infoWidget->raise();
			d->infoWidget->activateWindow();
		}
	}

	void MainWindow::on_replaceButton_clicked(bool) const {
		if(nullptr == d->currentWidget) {
			auto toastMessageBox = new TC::ToastMessageBox;
			toastMessageBox->Show(tr("There is no selected item."));
		    return;
		}

		auto pageIndex = -1;
		auto widgetIndex = -1;

		Plugins::CubeTCFWidget* currentCubeWidget = nullptr;
		for (auto i = 0; i < d->cubeTcfWidgets.size(); ++i) {
			const auto cubeTcfWidget = d->cubeTcfWidgets.at(i);
			if (nullptr == cubeTcfWidget) {
				continue;
			}

			widgetIndex = cubeTcfWidget->GetIndex(d->currentWidget);
			if (0 <= widgetIndex) {
				pageIndex = cubeTcfWidget->GetCurrentPage();
				currentCubeWidget = cubeTcfWidget;
				break;
			}
		}
		
	    if (-1 == pageIndex || -1 == widgetIndex || nullptr == currentCubeWidget) {
			return;
		}

		const auto fromIndex = pageIndex * 4 + widgetIndex;
		if(fromIndex >= currentCubeWidget->GetCount()) {
			auto toastMessageBox = new TC::ToastMessageBox;
			toastMessageBox->Show(tr("Selected item is invalid."));
			return;
		}

		const auto imagePosition = QString("%1-%2").arg(pageIndex + 1).arg(widgetIndex + 1);
		const auto targetPosition = ReplaceDialog::ReplaceIndex(currentCubeWidget->GetCount(), imagePosition);

		if(true == targetPosition.isEmpty()) {
			return;
		}

		auto split = targetPosition.split("-");
		if(2 > split.size()) {
			return;
		}

		
	    const auto toIndex = (split[0].toInt() - 1) * 4 + (split[1].toInt() - 1);

		currentCubeWidget->Swap(fromIndex, toIndex);
	}

	void MainWindow::on_captureButton_clicked(bool) const {
		const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentCapture", desktopPath).toString();

		const QString name = tr("2D Image Gallery ") + QDateTime::currentDateTime().toString("yyyy-MM-dd hhmmss") + ".png";

		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save screen shot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
		if (true == path.isEmpty()) {
			return;
		}

		QSettings("Tomocube", "TomoAnalysis").setValue("recentCapture", QFileInfo(path).dir().path());

		d->ui->toolWidget->setVisible(false);
		d->ui->scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		d->ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		for(auto child : d->cubeTcfWidgets) {
		    child->SetVisibleSubUI(false);
		}

		if(nullptr != d->currentWidget) {
			d->currentWidget->SetSelected(false);
		}

		auto px = d->ui->centralWidget->grab();

		d->ui->toolWidget->setVisible(true);
		d->ui->scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		d->ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		for (auto child : d->cubeTcfWidgets) {
		    child->SetVisibleSubUI(true);
		}

		if (nullptr != d->currentWidget) {
			d->currentWidget->SetSelected(true);
		}

		QString text;
		if(true == px.save(path)) {
			text = tr("The screen shot has been saved successfully.\n%1").arg(path);
		}
		else {
			text = tr("Failed to save screen shot.");
		}

		auto toastMessageBox = new TC::ToastMessageBox;
		toastMessageBox->Show(text);
	}

	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        if(d->share_param.contains(name)) {
			return d->share_param[name];
        }
		return nullptr;
    }

	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
		if(nullptr == param) {
			return;
		}

		d->share_param[name] = std::make_shared<IParameter>(param->Clone());
    }

	auto MainWindow::TryDeactivate() -> bool {
		//TODO: clean part
		return true;
    }
	auto MainWindow::TryActivate() -> bool {
		d->ui = new Ui::MainWindow();
		d->ui->setupUi(this);

		d->ui->scrollArea->setWidgetResizable(true);
		d->ui->scrollArea->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

		d->ui->titleIconLabel->setScaledContents(true);
		d->ui->titleIconLabel->setPixmap(QPixmap(":/img/playground-hypercube-link.svg"));

		// hide tool buttons
		d->ui->inverseGrayButton->hide();
		d->ui->scaleBarButton->hide();
		d->ui->line->hide();

		// set object names
		d->ui->scrollArea->setObjectName("scrollarea-bottom-round");
		d->ui->scrollAreaWidgetContents->setObjectName("panel-contents");
		d->ui->baseWidget->setObjectName("panel-base");
		d->ui->title->setObjectName("h4");
		d->ui->htButton->setObjectName("bt-round-tool");
		d->ui->flButton->setObjectName("bt-round-tool");
		d->ui->bfButton->setObjectName("bt-round-tool");
		d->ui->inverseGrayButton->setObjectName("bt-round-tool");
		d->ui->scaleBarButton->setObjectName("bt-round-tool");
		d->ui->infoButton->setObjectName("bt-round-tool");
		d->ui->replaceButton->setObjectName("bt-round-tool");
		d->ui->captureButton->setObjectName("bt-round-tool");

		d->ui->line->setObjectName("line-separator-bt");
		d->ui->line_2->setObjectName("line-separator-bt");
		d->ui->line_3->setObjectName("line-separator-bt");

		//start global communication
		subscribeEvent("TabChange");
		subscribeEvent(TC::Framework::MenuEvent::Topic());
		connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnCtkEvent(ctkEvent)));
		return true;
    }
	auto MainWindow::IsActivate() -> bool {
		return (nullptr != d->ui);
    }
	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
    }

	auto MainWindow::GetFeatureName() const -> QString {
		return  "org.tomocube.multi2dviewerPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}

	void MainWindow::OnCtkEvent(ctkEvent ctkEvent) {
		using MenuType = TC::Framework::MenuTypeEnum;
		MenuType menuAction = MenuType::Action;

		if (ctkEvent.getProperty("TabName").isValid()) {
			OnTabFocused(ctkEvent);
		}
		else if (ctkEvent.getProperty(menuAction._to_string()).isValid()) {
			OnMenuAction(ctkEvent);
		}
    }
	auto MainWindow::OnTabFocused(const ctkEvent& ctkEvent) -> void {
		auto TabName = ctkEvent.getProperty("TabName").toString();
		if (TabName.compare("2D Image Gallery") == 0) {
			MenuEvent menuEvt(MenuTypeEnum::MenuScript);
			menuEvt.scriptAddMenu("Help");
			menuEvt.scriptAddAction("About", "Help");			
			publishSignal(menuEvt, "2D Image Gallery");

			MenuEvent titleBarEvt(MenuTypeEnum::TitleBar);
			publishSignal(titleBarEvt, "2D Image Gallery");
		}
    }
	auto MainWindow::OnMenuAction(const ctkEvent& ctkEvent) -> void {
		using MenuType = TC::Framework::MenuTypeEnum;

		MenuType menuAction = MenuType::Action;
		auto actionName = ctkEvent.getProperty(menuAction._to_string()).toString();

		if (actionName.contains("About")) {
			QMessageBox msgBox;
			msgBox.setText("TomoAnalysis v1.0.0b");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.exec();
		}
    }
}