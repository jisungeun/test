#include <QValidator>
#include <QMessageBox>
#include <ToastMessageBox.h>

#include "ui_ReplaceDialog.h"
#include "ReplaceDialog.h"

namespace TomoAnalysis::Multi2DViewer::AppUI {
    struct ReplaceDialog::Impl {
        Ui::ReplaceDialog* ui{ nullptr };

        int pageCount = -1;
    };

    ReplaceDialog::ReplaceDialog(int count, QWidget* parent)
    : QDialog(parent)
    , d{ new Impl } {
        d->ui = new Ui::ReplaceDialog();
        d->ui->setupUi(this);

        d->pageCount = count / 4 +1;

        QRegExp re("^(([1-9]\\d*-[1-4]$\\d*)|([1-9]\\d*))");
        d->ui->currentLineEdit->setValidator(new QRegExpValidator(re));
        d->ui->targetLineEdit->setValidator(new QRegExpValidator(re));

        d->ui->currentLineEdit->setReadOnly(true);

        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    }

    ReplaceDialog::~ReplaceDialog() {
        delete d->ui;
    }

    auto ReplaceDialog::ReplaceIndex(const int& count, const QString& currentIndex)->QString {
        ReplaceDialog dialog(count);
        dialog.SetCurrentIndex(currentIndex);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetTargetIndex();
    }

    void ReplaceDialog::SetCurrentIndex(const QString& index) const {
        d->ui->currentLineEdit->setText(index);
    }

    auto ReplaceDialog::GetTargetIndex() const ->QString {
        return d->ui->targetLineEdit->text();
    }

    void ReplaceDialog::on_okButton_clicked() {
        const auto split = d->ui->targetLineEdit->text().split("-");
        if(2 > split.size()) {
            QMessageBox::warning(nullptr, "Warning", "Invalid index format.");
            return;
        }

        if(d->pageCount < split.at(0).toInt()) {
            QMessageBox::warning(nullptr, "Warning", "Invalid page index");
            return;
        }

        if (4 < split.at(1).toInt() || split.at(1).isEmpty()) {
            QMessageBox::warning(nullptr, "Warning", "Invalid item index");
            return;
        }

        accept();
    }

    void ReplaceDialog::on_cancelButton_clicked() {
        reject();
    }
}
