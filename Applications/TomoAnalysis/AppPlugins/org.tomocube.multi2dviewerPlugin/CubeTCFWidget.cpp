#include <iostream>

#include <QLabel>
#include <QDirIterator>
#include <QMouseEvent>

#include <SimpleTCF2DViewer.h>

#include "ui_CubeTCFWidget.h"
#include "CubeTCFWidget.h"

namespace  TomoAnalysis::Multi2DViewer::Plugins {
   struct CubeTCFWidget::Impl {
       Ui::CubeTCFWidget* ui{ nullptr };
       QList<TC::SimpleTCF2DViewer*> widgets;

       QList<QString> filePathList;

       int currentPage = 0;
       TC::TCF2DWidget::ImageType currentType = TC::TCF2DWidget::ImageType::HT;
   };

   CubeTCFWidget::CubeTCFWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
       d->ui = new Ui::CubeTCFWidget();
       d->ui->setupUi(this);
       
       d->widgets.append(d->ui->widget1);
       d->widgets.append(d->ui->widget2);
       d->widgets.append(d->ui->widget3);
       d->widgets.append(d->ui->widget4);

       for(auto widget : d->widgets) {
           //widget->KeepSquare(true);
           widget->SetSelectable(true);
           widget->SetVisibleImageTypeControl(false);
           widget->SetVisibleTimelapseControl(false);
           widget->installEventFilter(parent);
       }

       d->ui->contentsWidget->setObjectName("panel-contents");
       d->ui->title->setObjectName("h7");
       d->ui->prevPageButton->setObjectName("bt-arrow-left");
       d->ui->nextPageButton->setObjectName("bt-arrow-right");

       d->ui->titleIconLabel->setScaledContents(true);
       d->ui->titleIconLabel->setPixmap(QPixmap(":/img/playground-cube.svg"));
    }

   CubeTCFWidget::~CubeTCFWidget() {
       delete d->ui;
   }

   void CubeTCFWidget::SetCube(const TomoAnalysis::Cube::Pointer& cube) const {
       if(nullptr == cube) {
           return;
       }

       // set title
       d->ui->title->setText("");
       d->ui->title->setText(cube->GetName());

       // sorting file list
       d->filePathList.clear();

       for(const auto& dir : cube->GetTCFDirList()) {
           auto path = dir->GetPath();
           d->filePathList.append(path);
       }

       if(true == d->filePathList.isEmpty()) {
           return;
       }

       // init page combobox
       d->ui->pageComboBox->clear();

       const auto pageCount = ceil(d->filePathList.size() / 4.0);
       if(0 == pageCount) {
           return;
       }

       for(int i = 1; i < pageCount + 1; ++i) {
           auto text = QString("page %1").arg(i);
           d->ui->pageComboBox->addItem(text);
       }

       d->ui->pageComboBox->setCurrentIndex(0);
   }

   void CubeTCFWidget::FitZoomAll() const {
       for(auto widget : d->widgets) {
           widget->ZoomFit();
       }
   }

   void CubeTCFWidget::SetKeepSquare(bool keep) const {
       for (auto widget : d->widgets) {
           widget->KeepSquare(keep);
       }
   }

   void CubeTCFWidget::SetImageType(const TC::TCF2DWidget::ImageType& type) const {
       d->currentType = type;

       for(auto widget : d->widgets) {
           if(nullptr == widget) {
               continue;
           }

           if(true == widget->GetTCFPath().isEmpty()) {
               continue;
           }

           widget->ShowImage(d->currentType);
       }
   }

   auto CubeTCFWidget::GetCurrentPage() const ->int {
       return d->currentPage;
   }

   auto CubeTCFWidget::GetCount() const ->int {
       return d->filePathList.size();
   }

   auto CubeTCFWidget::GetIndex(TC::SimpleTCF2DViewer* widget) const ->int {
       int index = -1;

       for(auto i = 0; i < d->widgets.size(); ++i) {
           if(widget == d->widgets.at(i)) {
               index = i;
               break;
           }
       }

       return index;
   }

   auto CubeTCFWidget::GetPath(const int& index) const ->QString {
       if(index > d->filePathList.size()) {
           return QString();
       }

       return d->filePathList.at(index);
   }

   void CubeTCFWidget::Swap(const int& from, const int& to) const {
       d->filePathList.swapItemsAt(from, to);
       on_pageComboBox_currentIndexChanged(d->currentPage);
   }

   void CubeTCFWidget::SetVisibleSubUI(bool visible) {
       d->ui->pageComboBox->setVisible(visible);
       d->ui->prevPageButton->setVisible(visible);
       d->ui->nextPageButton->setVisible(visible);

       const auto size = this->size();

       adjustSize();
       FitZoomAll();
       this->resize(size);
   }

   void CubeTCFWidget::on_pageComboBox_currentIndexChanged(int index) const {
       const auto startIndex = index * 4;

       for (auto i = 0; i < 4; ++i) {
           const auto currentIndex = startIndex + i;
           const auto widget = d->widgets.at(i);
           if (nullptr == widget) {
               continue;
           }

           if (currentIndex < d->filePathList.size()) {
               if(false == widget->SetTCFPath(d->filePathList.at(currentIndex))) {
                   return;
               }

               widget->ShowImage(d->currentType, 0);
               if(d->currentType == TC::TCF2DWidget::HT) {
                   widget->forceVisible();
               }               
           }
           else {
               widget->Clear();
               widget->ShowTextImage("No Image");               
           }
       }

       d->currentPage = index;
   }

   void CubeTCFWidget::on_prevPageButton_clicked(bool) const {
       const auto index = d->ui->pageComboBox->currentIndex() - 1;
       if(0 > index) {
           return;
       }

       d->ui->pageComboBox->setCurrentIndex(index);
   }

   void CubeTCFWidget::on_nextPageButton_clicked(bool) const {
       const auto index = d->ui->pageComboBox->currentIndex() + 1;
       if (d->ui->pageComboBox->count() <= index) {
           return;
       }

       d->ui->pageComboBox->setCurrentIndex(index);
   }
}
