#pragma once

#include <QMainWindow>
#include <IMainWindowTA.h>

#include <HyperCube.h>

#include "ILicensed.h"

namespace TomoAnalysis::Multi2DViewer::AppUI {
	class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

		auto SetParameter(QString name, IParameter::Pointer param) -> void override;
		auto GetParameter(QString name) -> IParameter::Pointer override;
		
		auto Execute(const QVariantMap& params)->bool override;
		auto GetRunType() -> std::tuple<bool, bool> override;
		auto Show2DMultiViewer(const HyperCube::Pointer& hyperCube) ->void;
		auto TryActivate() -> bool final;
		auto TryDeactivate() -> bool final;
		auto IsActivate() -> bool final;
		auto GetMetaInfo()->QVariantMap final;

		auto GetFeatureName() const->QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	protected:
		void resizeEvent(QResizeEvent* event) override;
		bool eventFilter(QObject* obj, QEvent* event) override;

	private slots:
		void on_htButton_clicked(bool) const;
		void on_flButton_clicked(bool) const;
		void on_bfButton_clicked(bool) const;

		void on_inverseGrayButton_clicked(bool) const;
		void on_scaleBarButton_clicked(bool) const;

		void on_infoButton_clicked(bool) const;
		void on_replaceButton_clicked(bool) const;

		void on_captureButton_clicked(bool) const;

		void OnCtkEvent(ctkEvent);

	private:
		auto OnTabFocused(const ctkEvent& ctkEvent)->void;
		auto OnMenuAction(const ctkEvent& ctkEvent)->void;

		auto ExecuteBatchRun(const QVariantMap& params)->bool;
	
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
