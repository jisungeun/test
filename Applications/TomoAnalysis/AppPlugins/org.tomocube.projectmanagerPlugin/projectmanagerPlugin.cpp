#include "projectmanagerPlugin.h"

#include <ctkPluginFrameworkLauncher.h>
#include <QtPlugin>

#include <service/event/ctkEvent.h>

#include <AppInterfaceTA.h>

#include <MenuEvent.h>
#include <AppEvent.h>

#include "MainWindow.h"

namespace TomoAnalysis::ProjectManager::AppUI {

    struct projectmanagerPlugin::Impl {
        Impl() = default;
        ~Impl() = default;
        ctkPluginContext* context{ nullptr };        
        MainWindow* mainWindow{ nullptr };
        AppInterfaceTA* appInterface{ nullptr };
    };

    projectmanagerPlugin* projectmanagerPlugin::instance = 0;

    projectmanagerPlugin* projectmanagerPlugin::getInstance(){        
        return instance;
    }
    projectmanagerPlugin::projectmanagerPlugin() : d(new Impl) {        
        d->mainWindow = new MainWindow;        
        d->appInterface = new AppInterfaceTA(d->mainWindow);
        d->appInterface->SetProjectManager(true);
    }
    projectmanagerPlugin::~projectmanagerPlugin() {
    }

    auto projectmanagerPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);                
    }            
    auto projectmanagerPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);
        //Never destroy this plugin
    }
    auto projectmanagerPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }    
}
