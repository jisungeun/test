#define LOGGER_TAG "[Project Manager]"
#include <TCLogger.h>

#include <iostream>

#include <QString>
#include <QMessageBox>
#include <QFileDialog>
#include <QJsonDocument>
#include <QTimer>
#include <QDirIterator>

#include <ProjectPresenter.h>
#include <ProjectController.h>
#include <PlaygroundPresenter.h>
#include <PlaygroundController.h>

#include <TCParameterReader.h>
#include <TCParameterWriter.h>

#include <ApplicationPanel.h>
#include <PlaygroundPanel.h>

#include <TAProjectDataWriter.h>
#include <TAProjectDataReader.h>

#include <TCFMetaReader.h>

#include <AppEvent.h>
#include <MenuEvent.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::ProjectManager::AppUI {
	using namespace TC::Framework;
	struct MainWindow::Impl {
		Ui::MainWindow* ui{ nullptr };
		QString current_project;
		QString current_hypercube;
		QString current_app;
		QMap<QString, IParameter::Pointer> share_param;

		QVariantMap appProperties;
	};

	MainWindow::MainWindow(QWidget* parent)
		: IMainWindowTA("Project Manager", "Project Manager", parent)
		, d{ new Impl() }
	{
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "Project Manager";
	}

	MainWindow::~MainWindow() {
		//delete d->ui;
	}

	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
		auto singleRun = false;
		auto batchRun = false;
		return std::make_tuple(singleRun, batchRun);
	}


	//protected
	void MainWindow::resizeEvent(QResizeEvent* event) {
		Q_UNUSED(event);
	}
	auto MainWindow::ForceClosePopUp() -> void {
		if (nullptr != d->ui) {
			d->ui->previewPanel->ClosePopup();			
		}
	}

	// slot    
	void MainWindow::onCreateProject(const QString& path) const {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project name or path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{ new Interactor::ProjectPresenter(d->ui->explorerPanel) };

		Interactor::ProjectController projectController(presenter.get(), writer.get(), nullptr);
		if (false == projectController.Create(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to create project.");
			return;
		}
	}

	void MainWindow::onOpenProject(const QString& path) const {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project name or path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataReader> reader{ new Plugins::ProjectDataIO::TAProjectDataReader };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->pglistPanel) };

		Interactor::ProjectController controller(presenter.get(), nullptr, reader.get());
		if (false == controller.Open(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to open project.");
		}
	}

	void MainWindow::onChangeCurrentPlayground(const QString& path) const {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground name or path.");
			return;
		}

		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,
			d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), nullptr);
		if (false == controller.SetCurrent(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to change playground.");
			return;
		}
	}

	void MainWindow::onAddSubProject(const QString& path) const {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel) };

		Interactor::ProjectController controller(presenter.get(), writer.get(), nullptr);
		if (false == controller.CreateSub(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to create project.");
			return;
		}
	}

	void MainWindow::onAddPlayground(const QString& path) const {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,nullptr, d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.Create(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add playground.");
			return;
		}
	}

	void MainWindow::onAddCube(const QString& parentPlaygroundPath, const QString& name) const {
		if (true == parentPlaygroundPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		if (true == name.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no cube name.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,
			d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.AddCube(parentPlaygroundPath, name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}
	}

	void MainWindow::onAddHypercube(const QString& parentPlaygroundPath, const QString& name) const {
		if (true == parentPlaygroundPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		if (true == name.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no hypercube name.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.AddHypercube(parentPlaygroundPath, name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add hypercube.");
			return;
		}
	}

	void MainWindow::onLinkFolderToProject(const QString& parentProjectPath, const QString& path) const {
		if (true == parentProjectPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project path.");
			return;
		}

		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no folder path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->pglistPanel) };
		Interactor::ProjectController controller(presenter.get(), writer.get(), nullptr);
		if (false == controller.LinkTCFFolder(parentProjectPath, path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to link folder to project.");
			return;
		}
	}

	void MainWindow::onLinkFolderToPlayground(const QString& playgroundPath, const QString& path) {
		if (true == playgroundPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no folder path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		QString newPath = path;

		if (path.contains(".tcf") || path.contains(".TCF")) {
			newPath = QFileInfo(path).dir().absolutePath();
		}

		if (false == controller.LinkTCFDirectory(playgroundPath, newPath)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}
	}

	void MainWindow::onDuplicateTCFToPlayground(const QString& playgroundPath, const QString& folderPath) {
		if (true == playgroundPath.isEmpty()) {
			return;
		}

		if (true == folderPath.isEmpty()) {
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,
			d->ui->playgroundPanel,
			nullptr) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.DuplicateTCFDirectory(playgroundPath, folderPath)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add hypercube.");
			return;
		}

	}

	void MainWindow::onLinkAppToPlayground(const QString& appPath, const QStringList& procList) {
		Q_UNUSED(appPath)
			Q_UNUSED(procList)
	}

	void MainWindow::onLoadPlugin(const QString& pluginPath) {
		if (true == pluginPath.isEmpty()) {
			return;
		}
	}

	void MainWindow::onLoadProcessor(const QString& processorPath) {
		if (true == processorPath.isEmpty()) {
			return;
		}
	}

	void MainWindow::pgCubeDelete(QString cubeInfo, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.DeleteCube(parent, cubeInfo)) {
			QMessageBox::warning(nullptr, "Error", "Failed to delete cube.");
			return;
		}
	}
	void MainWindow::pgCubeCopy(QString cubeName, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.CopyCube(parent, cubeName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to copy cube.");
			return;
		}
	}
	void MainWindow::pgHyperDelete(QString hyperInfo, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.DeleteHyperCube(parent, hyperInfo)) {
			QMessageBox::warning(nullptr, "Error", "Failed to delete hypercube.");
			return;
		}
	}
	void MainWindow::pgHyperCopy(QString hyperName, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.CopyHyperCube(parent, hyperName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to copy hypercube.");
			return;
		}
	}
	void MainWindow::pgCubeRename(QString cubeInfo, QString newName, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.RenameCube(parent, cubeInfo, newName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to rename cube.");
			return;
		}
	}
	void MainWindow::pgHyperRename(QString hyperInfo, QString newName, QString parent) {
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.RenameHyperCube(parent, hyperInfo, newName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to rename hypercube.");
			return;
		}
	}
	void MainWindow::pgNoSelection() {
		/*const std::shared_ptr<Interactor::ItemPresenter> presenter{ new Interactor::ItemPresenter(d->ui->itemInfoPanel) };
		Interactor::ItemController controller(presenter.get());
		if(false == controller.ClearItemInfo()) {
			QMessageBox::warning(nullptr, "Error", "Failed to clear item info");
			return;
		}*/
	}
	void MainWindow::pgItemSelected(QString itemName, QString parent) {
		if (itemName.contains("Application!")) {
			//applicatoin handling은 따로 끝난 상태라 skip
			return;
		}
		//NoSelection과 동일하게 동작
		//
		//clear current informations
		d->current_hypercube = QString();
		d->current_app = QString();

		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(nullptr,
			nullptr,
			nullptr,
			nullptr,
			nullptr,
			d->ui->parameterPanel) };

		Interactor::PlaygroundController controller(presenter.get(), nullptr);
		if (false == controller.ClearApplication(parent)) {
			QMessageBox::warning(nullptr, "Error", "Failed to clear application.");
			return;
		}
	}
	void MainWindow::pgAppSelected(QString appName, QString parent) {
		//parse app information
		auto split = appName.split("!");
		auto app = split[1];
		auto hyper = split[2];

		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(nullptr,
			nullptr,
			nullptr,
			nullptr,
			nullptr,
			d->ui->parameterPanel) };

		Interactor::PlaygroundController controller(presenter.get(), nullptr);
		if (false == controller.ShowApplication(parent, app, hyper)) {
			QMessageBox::warning(nullptr, "Error", "Failed to show application.");
			return;
		}


		//try to load temporal parameter information
		auto pReader = std::make_shared<TC::IO::TCParameterReader>(parent);
		auto tup = pReader->Read(app, hyper);
		auto param = std::get<1>(tup);
		auto procName = std::get<0>(tup);

		if (nullptr != param && !procName.isEmpty()) {
			if (d->ui->parameterPanel->SetCurrentProcessor(procName)) {
				d->ui->parameterPanel->SetParameter(param);
			}
		}
		//store current app + hypercube pair
		d->current_project = parent;
		d->current_app = app;
		d->current_hypercube = hyper;
	}

	auto MainWindow::openProjectfromMain()->void {
		//d->ui->buttonPanel->on_openButton_clicked();
	}

	auto MainWindow::createProjectfromMain()->void {
		//d->ui->buttonPanel->on_newButton_clicked();
	}
	auto MainWindow::Execute(const QVariantMap& params)->bool {
		if (params.isEmpty()) {
			return true;
		}
		if (false == params.contains("ExecutionType")) {
			return false;
		}
		if (params["ExecutionType"].toString() == "BatchFinished") {
			d->ui->previewPanel->UpdateWithCurrentInfo();
		}
		return true;
	}
		
	void MainWindow::onUnlinkAppFromCube(const QString& appPath, const QString& cubeName) {
		if (true == appPath.isEmpty()) {
			return;
		}
		if (true == cubeName.isEmpty()) {
			return;
		}
	}
	void MainWindow::onUnlinkCubeFromHyperCube(const QString& cubeName, const QString& hyperName) {
		if (true == cubeName.isEmpty()) {
			return;
		}
		if (true == hyperName.isEmpty()) {
			return;
		}
	}

	void MainWindow::onUnlinkTcfFromCube(const QString& tcfPath, const QString& cubeName) {
		if (true == tcfPath.isEmpty()) {
			return;
		}
		if (true == cubeName.isEmpty()) {
			return;
		}
	}

	void MainWindow::onCurrentAppChanged(const QString& appPath, const QString& parentName) {
		Q_UNUSED(appPath)
			Q_UNUSED(parentName)
	}

	void MainWindow::onExecuteApp(const QString& appPath, const QString& tcfPath) {
		Q_UNUSED(appPath)
			Q_UNUSED(tcfPath)
	}

	void MainWindow::onUnlinkAppFromHyperCube(const QString& appPath, const QString& hyperName) {
		if (true == appPath.isEmpty()) {
			return;
		}
		if (true == hyperName.isEmpty()) {
			return;
		}
	}


	void MainWindow::onPlaygroundNameChanged(QString projectPath, QString name, QString newName) const {
		if (true == projectPath.isEmpty()) {
			return;
		}

		if (true == name.isEmpty()) {
			return;
		}

		if (true == newName.isEmpty()) {
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			d->ui->pglistPanel) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.Rename(projectPath, name, newName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to rename playground");
			return;
		}
	}

	void MainWindow::onDeletePlaygroundRequested(QString projectPath, QString name) {
		if (true == projectPath.isEmpty()) {
			return;
		}

		if (true == name.isEmpty()) {
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			d->ui->pglistPanel,
			d->ui->previewPanel) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.Delete(projectPath, name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to delete playground");
			return;
		}
	}

	void MainWindow::onCurrentPlaygroundChanged(QString projectPath, QString name) {
		if (true == projectPath.isEmpty()) {
			return;
		}
		if (true == name.isEmpty()) {
			return;
		}
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			d->ui->pglistPanel,
			d->ui->previewPanel) };

		Interactor::PlaygroundController controller(presenter.get(), nullptr);

		QFileInfo fileInfo(projectPath);
		const auto dir = fileInfo.absoluteDir();
		QDir playgroundDir(dir.filePath("playground/"));

		const auto path = QString("%1.tcpg").arg(playgroundDir.filePath(name));

		if (false == controller.SetCurrent(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to change playground.");
			return;
		}
	}

	void MainWindow::onRemoveTCF(const QString& projectPath, const QString& dirPath) {
		if (true == projectPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project path.");
			return;
		}
		if (true == dirPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no folder path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{ new Interactor::ProjectPresenter(d->ui->explorerPanel,
																	   d->ui->operationPanel,
																	   d->ui->playgroundPanel,
																	   d->ui->previewPanel,
																	   nullptr) };

		Interactor::ProjectController controller(presenter.get(), writer.get(), nullptr);
		if (false == controller.UnlinkTCFFolder(projectPath, dirPath)) {
			QMessageBox::warning(nullptr, "Error", "Failed to remove folder to project.");
			return;
		}
	}

	void MainWindow::sqAddTcf(QString proj_path, QString folder_path) {
		if (true == proj_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project path.");
			return;
		}
		if (true == folder_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no folder path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };
		Interactor::ProjectController controller(presenter.get(), writer.get(), nullptr);
		if (false == controller.LinkTCFFolder(proj_path, folder_path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to link folder to project.");
			return;
		}
	}

	void MainWindow::sqAddCilsTcf(const QMap<int, QStringList>& items, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>& images) {
		d->ui->previewPanel->UpdateCils(items, worksets, images);
	}

	void MainWindow::sqLinkTcfToWorkset(QString proj_path, QString path, QString worksetName, int worksetId) {
		if(path.isEmpty() || worksetName.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			nullptr,
			d->ui->previewPanel,
			nullptr) };
		Interactor::ProjectController controller(presenter.get(), writer.get(), nullptr);
		if (false == controller.LinkWorkset(proj_path, worksetId, worksetName, path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to link folder to workset.");
			return;
		}
	}

	void MainWindow::sqCreateCube(QString parentPlaygroundPath, QString cube, QString hyper) {
		if (true == parentPlaygroundPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		if (true == cube.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no cube name.");
			return;
		}

		if (true == hyper.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no hypercube name.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (true == controller.CheckDuplicateCube(parentPlaygroundPath, hyper, cube)) {
			QMessageBox::warning(nullptr, "Error", QString("%1 already exists in playground.").arg(cube));
			return;
		}

		if (false == controller.AddCube(parentPlaygroundPath, cube, hyper)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}
	}

	void MainWindow::sqCreateHypercube(QString parentPlaygroundPath, QString name) {
		if (true == parentPlaygroundPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		if (true == name.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no hypercube name.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (true == controller.CheckDuplicateHypercube(parentPlaygroundPath, name)) {
			QMessageBox::warning(nullptr, "Error", QString("%1 already exists.").arg(name));
			return;
		}
		d->ui->operationPanel->ClearHyperHolder();
		if (false == controller.AddHypercube(parentPlaygroundPath, name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add hypercube.");
			return;
		}
	}

	void MainWindow::sqCreatePlayground(QString name) {
		if (name.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no playground path.");
			return;
		}

		QFileInfo dir(name);
		if (dir.exists()) {
			QMessageBox::warning(nullptr, "Error", "Playground name already exists.");
			return;
		}
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			d->ui->pglistPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.Create(name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add playground.");
			return;
		}
	}

	void MainWindow::sqConnectApplication(QString app, QString appfull, QStringList procList, QString hyper, QString playgroundPath, std::tuple<bool, bool> appType) {
		if (true == app.isEmpty()) {
			return;
		}

		if (d->current_app.compare(appfull) != 0) {
			auto hyperPath = playgroundPath.chopped(5);
			QDir hyperDir(hyperPath);
			if (hyperDir.exists(hyper)) {
				hyperDir.cd(hyper);
				for (auto entry : hyperDir.entryList()) {
					hyperDir.remove(entry);
				}
			}
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			nullptr,
			d->ui->playgroundPanel,
			nullptr,
			nullptr,
			d->ui->parameterPanel) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.LinkApplication(playgroundPath, app, appfull, procList, hyper, appType)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add Application");
			return;
		}
		auto appText = QString("Application!%1!%2").arg(appfull).arg(hyper);
		d->ui->playgroundPanel->SetAppSelected(appText);
		//d->ui->parameterPanel->GetParameter();
	}

	void MainWindow::sqOpenProject(QString path) {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project name or path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataReader> reader{ new Plugins::ProjectDataIO::TAProjectDataReader };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
											 d->ui->operationPanel,
											 d->ui->playgroundPanel,
											 d->ui->previewPanel,
											 d->ui->pglistPanel,
											 d->ui->parameterPanel) };

		Interactor::ProjectController controller(presenter.get(), nullptr, reader.get());
		if (false == controller.Open(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to open project.");
		}
	}
	void MainWindow::pjRenameProject(QString path, QString newName) {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project name or path.");
			return;
		}
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
											 d->ui->operationPanel,
											 d->ui->playgroundPanel,
											 d->ui->previewPanel,
											 d->ui->pglistPanel,
											 d->ui->parameterPanel) };
		Interactor::ProjectController projectController(presenter.get(), writer.get(), nullptr);
		if (false == projectController.Rename(path, newName)) {
			QMessageBox::warning(nullptr, "Error", "Failed to rename project.");
		}
	}


	void MainWindow::sqCreateProject(QString path) {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "There is no project name or path.");
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::ProjectPresenter> presenter{
			new Interactor::ProjectPresenter(d->ui->explorerPanel,
											 d->ui->operationPanel,
											 d->ui->playgroundPanel,
											 d->ui->previewPanel,
											 d->ui->pglistPanel,
											 d->ui->parameterPanel) };


		Interactor::ProjectController projectController(presenter.get(), writer.get(), nullptr);
		if (false == projectController.Create(path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to create project.");
			return;
		}
	}

	void MainWindow::pvViewer(QStringList tcf_path, QString prj_path) {
		if (true == tcf_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to view");
			return;
		}
		if (true == prj_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Wrong project path");
			return;
		}

		BroadcastViewerApp(tcf_path, prj_path);
	}

	void MainWindow::pvSegmentation(QString tcf_path) {
		if (true == tcf_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to perform segmentation");
			return;
		}

		BroadcastSegmentationApp(tcf_path);
	}
	void MainWindow::pvSegmentation2D(QString tcf_path) {
		if (true == tcf_path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to perform segmentation");
			return;
		}
		AppEvent appEvent(AppTypeEnum::APP_WITH_ARGS);
		appEvent.setFullName("org.tomocube.maskeditor2dPlugin");
		appEvent.setAppName("2D Mask Editor");
		appEvent.addParameter("TCFPath", tcf_path);

		TC::Framework::AppTypeEnum type = TC::Framework::AppTypeEnum::APP_WITH_ARGS;

		publishSignal(appEvent, "Project Manager");
	}

	void MainWindow::pvLink(QStringList tcf_list, QString cube_name, QString playground) {
		if (tcf_list.size() < 1) {
			return;
		}
		if (cube_name.isEmpty())
			return;

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel) };
		//,d->ui->parameterPanel)};

		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.LinkTCFsToCube(playground, tcf_list, cube_name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}
	}

	void MainWindow::pvUnlink(QStringList tcf_list, QString cube_name, QString playground) {
		if (true == tcf_list.isEmpty()) {
			return;
		}

		if (cube_name.isEmpty()) {
			return;
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			nullptr,
			d->ui->previewPanel) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get());

		if (false == controller.UnLinkTCFsToCube(playground, tcf_list, cube_name)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}
	}

	void MainWindow::pvSimple(QString tcf_path) {
		//Make Time Stamp
		//Identical Playground name - HypercubeName - CubeName
		QString timeStamp = QDateTime::currentDateTime().toString("yyMMdd_hhmmss");


		auto projPath = d->ui->operationPanel->GetCurrentProject();

		if (projPath.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Project is empty");
			return;
		}
		auto parentDir = QFileInfo(projPath).absoluteDir();
		auto full_path = parentDir.path() + "/playground/" + timeStamp + ".tcpg";
		//Create Playground				

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(d->ui->explorerPanel,
			d->ui->operationPanel,
			d->ui->playgroundPanel,
			d->ui->pglistPanel,
			d->ui->previewPanel,
			d->ui->parameterPanel) };

		Interactor::PlaygroundController controller(presenter.get(), writer.get());
		if (false == controller.Create(full_path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add playground.");
			return;
		}

		//Create Hypercube
		d->ui->operationPanel->ClearHyperHolder();
		if (false == controller.AddHypercube(full_path, timeStamp)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add hypercube.");
			return;
		}

		//CreateCube
		if (false == controller.AddCube(full_path, timeStamp, timeStamp)) {
			QMessageBox::warning(nullptr, "Error", "Failed to add cube.");
			return;
		}

		//Link selected TCF to cube
		QStringList tcf_list;
		tcf_list.push_back(tcf_path);

		if (false == controller.LinkTCFsToCube(full_path, tcf_list, timeStamp)) {
			QMessageBox::warning(nullptr, "Error", "Failed to link tcf to cube.");
			return;
		}

		d->ui->operationPanel->ForceStep(6);
	}

	void MainWindow::pvReport(QString pg_report_folder) {
		auto split = pg_report_folder.split("/");
		auto cnt = split.count();
		auto timeStamp = split[cnt - 1];
		auto hyperName = split[cnt - 2];

		QDir hDir(pg_report_folder);// .../timestamp
		hDir.cdUp(); // .../hypercube
		hDir.cdUp(); // .../history

		auto param_reader = std::make_shared<TC::IO::TCParameterReader>(hDir.path(), true);
		auto entry = param_reader->ReadHistory(hyperName, timeStamp);
		auto reportName = std::get<2>(entry);

		BroadcastReport(pg_report_folder, reportName);
	}

	auto MainWindow::copyAndReplaceFolderContents(const QString& fromDir, const QString& toDir) ->void {
		QDirIterator it(fromDir, QDirIterator::Subdirectories);
		QDir dir(fromDir);
		const int absSourcePathLength = dir.absoluteFilePath(fromDir).length();

		while (it.hasNext()) {
			it.next();
			const auto fileInfo = it.fileInfo();
			if (!fileInfo.isHidden()) { //filters dot and dotdot
				const QString subPathStructure = fileInfo.absoluteFilePath().mid(absSourcePathLength);
				const QString constructedAbsolutePath = toDir + subPathStructure;

				if (fileInfo.isDir()) {
					//Create directory in target folder
					dir.mkpath(constructedAbsolutePath);
				}
				else if (fileInfo.isFile()) {
					//Copy File to target directory

					//Remove file at target location, if it exists, or QFile::copy will fail
					QFile::remove(constructedAbsolutePath);
					QFile::copy(fileInfo.absoluteFilePath(), constructedAbsolutePath);
				}
			}
		}
	}

	void MainWindow::pvDeleteHistory(QString pg_history_path) {
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(nullptr,
			nullptr,
			nullptr,
			//nullptr,nullptr,
			//d->ui->parameterPanel) };
			nullptr,nullptr,
			nullptr) };
		Interactor::PlaygroundController controller(presenter.get(), nullptr);

		QDir pDir(pg_history_path);
		auto timeStamp = pDir.dirName();
		pDir.cdUp();
		auto hypercube = pDir.dirName();
		pDir.cdUp();

		auto paramFilePath = pDir.path() + "/" + hypercube + "_" + timeStamp + ".param";

		QString reportName;
		QFile loadFile(paramFilePath);
		if (loadFile.open(QIODevice::ReadOnly)) {
			const QByteArray data = loadFile.readAll();
			const  QJsonDocument doc(QJsonDocument::fromJson(data));
			auto object = doc.object();
			reportName = object["ReportName"].toString();
		}

		loadFile.close();

		if (false == controller.DeleteHistory(pg_history_path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to delete playground history");
			return;
		}

		d->ui->previewPanel->UpdateAfterDelete();
		BroadcastCloseApp(reportName);
	}
	void MainWindow::pvHistory(QString pg_history_path) {
		QFile loadFile(pg_history_path);
		if (false == loadFile.open(QIODevice::ReadOnly)) {
			QLOG_ERROR() << "Cannot load" << pg_history_path << " history";
		}

		const QByteArray bdata = loadFile.readAll();
		const  QJsonDocument doc(QJsonDocument::fromJson(bdata));
		auto object = doc.object();

		auto pg_name = object["name"].toString();

		//parsing target playground		

		QDir hPath(pg_history_path);
		hPath.makeAbsolute();
		hPath.cdUp();//history
		hPath.cdUp();//playground

		auto target_path = hPath.path() + "/" + pg_name + ".tcpg";
		QFileInfo target(target_path);
		if (!target.exists()) {
			//force create playground
			sqCreatePlayground(target_path);
			//force create hypercube path						
			QFileInfo hisFile(pg_history_path);
			auto spliter = hisFile.fileName().chopped(5).split("_");

			auto time_stamp = spliter[spliter.size() - 1];

			auto hypercube_name = QString();

			for (auto i = 0; i < spliter.count() - 1; i++) {
				hypercube_name += spliter[i];
			}
			auto rootPath = hisFile.absoluteDir().path();
			auto hyperFolderPath = rootPath + "/" + hypercube_name + "/" + time_stamp;

			hPath.mkdir(pg_name);
			hPath.cd(pg_name);
			hPath.mkdir(hypercube_name);

			//copy containing files
			hPath.cdUp();
			auto contentsTarget = hPath.path() + "/" + pg_name + "/" + hypercube_name;
			QDir sourceFolder(hyperFolderPath);
			auto cubeList = sourceFolder.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
			for (auto c : cubeList) {
				auto contentsSource = hyperFolderPath + "/" + c;
				copyAndReplaceFolderContents(contentsSource, contentsTarget);
			}

			//force copy hypercube parameter			
			auto hyperParamTarget = hPath.path() + "/" + pg_name + "/" + hypercube_name + ".param";
			auto hyperParamSource = rootPath + "/" + hypercube_name + "_" + time_stamp + ".param";

			QFile::copy(hyperParamSource, hyperParamTarget);
		}

		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataWriter> writer{ new Plugins::ProjectDataIO::TAProjectDataWriter };
		std::shared_ptr<Plugins::ProjectDataIO::TAProjectDataReader> reader{ new Plugins::ProjectDataIO::TAProjectDataReader };
		const std::shared_ptr<Interactor::PlaygroundPresenter> presenter{
			new Interactor::PlaygroundPresenter(nullptr,
			nullptr,
			d->ui->playgroundPanel,
			//nullptr,nullptr,
			//d->ui->parameterPanel) };
			d->ui->pglistPanel,d->ui->previewPanel,
			d->ui->parameterPanel) };
		Interactor::PlaygroundController controller(presenter.get(), writer.get(), reader.get());

		if (false == controller.Copy(pg_history_path, target_path)) {
			QMessageBox::warning(nullptr, "Error", "Failed to load playground history");
			return;
		}

		//parse playground path into target parameter
		QFileInfo file(pg_history_path);
		auto paramName = file.fileName().chopped(5);
		auto split = paramName.split("_");
		auto timeStamp = split[split.size() - 1];
		QString hyperName;
		for (auto i = 0; i < split.size() - 1; i++) {
			hyperName += split[i];
			if (i < split.size() - 2) {
				hyperName += "_";
			}
		}

		auto preader = std::make_shared<TC::IO::TCParameterReader>(hPath.path() + "/history", true);
		auto tup = preader->ReadHistory(hyperName, timeStamp);
		auto param = std::get<3>(tup);
		auto procName = std::get<1>(tup);
		//apply copied parameter into current status
		d->ui->parameterPanel->SetCurrentProcessor(procName);
		d->ui->parameterPanel->SetParameter(param);
	}	
	void MainWindow::apInitParam(IParameter::Pointer param, QString procName) {
		d->share_param[procName] = param;
	}

	void MainWindow::apParamWhole(IParameter::Pointer param, QString sender) {
		auto procName = sender.split("*")[1];
		d->share_param[procName] = param;

		BroadcastCopyParam(param, sender);
	}

	void MainWindow::apBatchRun(QString appName, QString symbolicName, QString procName, QString playgroundPath, QString hyperName, QList<QStringList> cubeContents) {
		if (d->share_param.contains(procName)) {
			if (nullptr != d->share_param[procName]) {
				if (false == d->share_param[procName]->IsValueEqual(d->ui->parameterPanel->GetParameter())) {
					auto hyperPath = d->current_project.chopped(5);
					QDir hyperDir(hyperPath);
					if (hyperDir.exists(d->current_hypercube)) {
						hyperDir.cd(d->current_hypercube);
						QFileInfo hyperParam = hyperDir.path() + ".param";
						if (hyperParam.exists()) {
							for (auto entry : hyperDir.entryList()) {
								if (entry == "." || entry == "..") {
									continue;
								}
								if (hyperDir.exists(entry)) {
									QDir removeDir(hyperDir.path());
									removeDir.cd(entry);
									removeDir.removeRecursively();
								}
								else {
									hyperDir.remove(entry);
								}
							}
						}
					}

				}
			}
		}		
		d->share_param[procName] = d->ui->parameterPanel->GetParameter();

		d->ui->previewPanel->FlushSelection();

		auto sender = symbolicName + "*" + procName;
		if (nullptr != d->share_param[procName]) {
			BroadcastCopyParam(d->ui->parameterPanel->GetParameter(), sender);
		}

		BroadcastBatchRun(appName, symbolicName, procName, playgroundPath, hyperName, cubeContents);
	}

	void MainWindow::apRun(QString appName, QString symbolicName, QString procName, QString tcfFile, QString playgroundPath, QString hyperName) {
		d->share_param[procName] = d->ui->parameterPanel->GetParameter();
		d->ui->previewPanel->FlushSelection();

		BroadcastSingleRun(appName, symbolicName, procName, tcfFile, playgroundPath, hyperName);
		auto sender = symbolicName + "*" + procName;
		if (nullptr != d->share_param[procName]) {

			BroadcastCopyParam(d->ui->parameterPanel->GetParameter(), sender);
		}
	}


	//private
	auto MainWindow::InitUI(void)->bool {
		//set initial stretch factors

		d->ui->mainSplitter->setStretchFactor(0, 0);
		d->ui->mainSplitter->setStretchFactor(1, 1);
		d->ui->mainSplitter->setStretchFactor(2, 0);


		QList<int> expSize;
		expSize << 300 << 100;
		d->ui->explorerSplitter->setSizes(expSize);

		d->ui->operationSplitter->setStretchFactor(0, 1);
		d->ui->operationSplitter->setStretchFactor(1, 9);

		QList<int> pgSize;
		pgSize << 200 << 200;
		d->ui->playgroundSplitter->setSizes(pgSize);

		//project event
		connect(d->ui->explorerPanel, SIGNAL(renameProject(QString, QString)), this, SLOT(pjRenameProject(QString, QString)));

		//operation sequence event
		connect(d->ui->operationPanel, SIGNAL(tcfAdd(QString, QString)), this, SLOT(sqAddTcf(QString, QString)));
		connect(d->ui->operationPanel, SIGNAL(cilsTcfAdd(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&)), this, SLOT(sqAddCilsTcf(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&)));
		connect(d->ui->operationPanel, SIGNAL(OnLinkTab(bool)), d->ui->previewPanel, SLOT(sqOnLinkTab(bool)));
		connect(d->ui->operationPanel, SIGNAL(createCube(QString, QString, QString)), this, SLOT(sqCreateCube(QString, QString, QString)));
		connect(d->ui->operationPanel, SIGNAL(createHyper(QString, QString)), this, SLOT(sqCreateHypercube(QString, QString)));
		connect(d->ui->operationPanel, SIGNAL(connectApp(QString, QString, QStringList, QString, QString, std::tuple<bool, bool>)), this, SLOT(sqConnectApplication(QString, QString, QStringList, QString, QString, std::tuple<bool, bool>)));
		connect(d->ui->operationPanel, SIGNAL(createPG(QString)), this, SLOT(sqCreatePlayground(QString)));
		connect(d->ui->operationPanel, SIGNAL(openProj(QString)), this, SLOT(sqOpenProject(QString)));
		connect(d->ui->operationPanel, SIGNAL(createProj(QString)), this, SLOT(sqCreateProject(QString)));

		connect(d->ui->previewPanel, SIGNAL(sigViewer(QStringList, QString)), this, SLOT(pvViewer(QStringList, QString)));
		connect(d->ui->previewPanel, SIGNAL(sigSegmentation(QString)), this, SLOT(pvSegmentation(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigSegmentation2D(QString)), this, SLOT(pvSegmentation2D(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigLinkRequest(QStringList, QString, QString)), this, SLOT(pvLink(QStringList, QString, QString)));
		connect(d->ui->previewPanel, SIGNAL(sigUnlinkRequest(QStringList, QString, QString)), this, SLOT(pvUnlink(QStringList, QString, QString)));
		connect(d->ui->previewPanel, SIGNAL(sigLoadHistory(QString)), this, SLOT(pvHistory(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigDeleteHistory(QString)), this, SLOT(pvDeleteHistory(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigLoadReport(QString)), this, SLOT(pvReport(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigCreateSimple(QString)), this, SLOT(pvSimple(QString)));
		connect(d->ui->previewPanel, SIGNAL(sigAddTcf(QString, QString)), this, SLOT(sqAddTcf(QString, QString)));
		connect(d->ui->previewPanel, SIGNAL(sigLinkTcfToWorkset(QString, QString, QString, int)), this, SLOT(sqLinkTcfToWorkset(QString, QString, QString, int)));

		//playground event
		connect(d->ui->playgroundPanel, SIGNAL(noItemSelected()), this, SLOT(pgNoSelection()));
		connect(d->ui->playgroundPanel, SIGNAL(appSelected(QString, QString)), this, SLOT(pgAppSelected(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(cubeDelete(QString, QString)), this, SLOT(pgCubeDelete(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(cubeCopy(QString, QString)), this, SLOT(pgCubeCopy(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(cubeRename(QString, QString, QString)), this, SLOT(pgCubeRename(QString, QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(hyperDelete(QString, QString)), this, SLOT(pgHyperDelete(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(hyperRename(QString, QString, QString)), this, SLOT(pgHyperRename(QString, QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(hyperCopy(QString, QString)), this, SLOT(pgHyperCopy(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(itemClicked(QString, QString)), this, SLOT(pgItemSelected(QString, QString)));
		connect(d->ui->playgroundPanel, SIGNAL(itemClicked(QString, QString)), d->ui->previewPanel, SLOT(onPlaygroundPanelItemChanged(QString, QString)));

		connect(d->ui->pglistPanel, SIGNAL(playgroundNameChanged(QString, QString, QString)), this, SLOT(onPlaygroundNameChanged(QString, QString, QString)));
		connect(d->ui->pglistPanel, SIGNAL(deletePlaygroundRequested(QString, QString)), this, SLOT(onDeletePlaygroundRequested(QString, QString)));
		connect(d->ui->pglistPanel, SIGNAL(pgSelectionChanged(QString, QString)), this, SLOT(onCurrentPlaygroundChanged(QString, QString)));

		connect(d->ui->parameterPanel, SIGNAL(singleRun(QString, QString, QString, QString, QString, QString)), this, SLOT(apRun(QString, QString, QString, QString, QString, QString)));
		connect(d->ui->parameterPanel, SIGNAL(batchRun(QString, QString, QString, QString, QString, QList<QStringList>)), this, SLOT(apBatchRun(QString, QString, QString, QString, QString, QList<QStringList>)));		
		connect(d->ui->parameterPanel, SIGNAL(sigParameter(IParameter::Pointer, QString)), this, SLOT(apParamWhole(IParameter::Pointer, QString)));
		connect(d->ui->parameterPanel, SIGNAL(sigInitParam(IParameter::Pointer, QString)), this, SLOT(apInitParam(IParameter::Pointer, QString)));

		connect(d->ui->explorerPanel, SIGNAL(removeTCF(const QString&, const QString&)), this, SLOT(onRemoveTCF(const QString&, const QString&)));

		return true;
	}

	auto MainWindow::InitApp() -> bool {
		return true;
	}
	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
		if (d->share_param.contains(name)) {
			if (nullptr == d->share_param[name]) {
				return nullptr;
			}
			return std::shared_ptr<IParameter>(d->share_param[name]->Clone());
		}
		return nullptr;
	}

	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
		auto new_param = std::shared_ptr<IParameter>(param->Clone());
		auto isChanged = false;
		if (d->share_param.contains(name)) {
			if (!d->share_param[name]->IsValueEqual(param)) {//if there is changes                
				d->share_param[name] = new_param;
				isChanged = true;
			}
		}
		else {//new one
			d->share_param[name] = new_param;
			isChanged = true;
		}

		d->ui->parameterPanel->SetCurrentProcessor(name);

		d->ui->parameterPanel->SetParameter(d->share_param[name]);

		if (isChanged) {
			//remove related hypercube Items            
			auto hyperPath = d->current_project.chopped(5);
			QDir hyperDir(hyperPath);
			if (hyperDir.exists(d->current_hypercube)) {
				hyperDir.cd(d->current_hypercube);
				for (auto entry : hyperDir.entryList()) {
					hyperDir.remove(entry);
				}
			}
			//write current parameter to temporal
			auto writer = std::make_shared<TC::IO::TCParameterWriter>(d->current_project);
			writer->Write(d->share_param[name], d->current_app, QString(), d->current_hypercube, name);
		}
	}
	auto MainWindow::TryDeactivate() -> bool {
		//TODO clean part
		return true;
	}

	auto MainWindow::TryActivate() -> bool {
		d->ui = new Ui::MainWindow;
		d->ui->setupUi(this);
		this->InitUI();
		this->InitApp();

		//start global communication
		subscribeEvent("TabChange");
		subscribeEvent(TC::Framework::MenuEvent::Topic());
		connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnCtkEvent(ctkEvent)));

		return true;
	}
	auto MainWindow::IsActivate() -> bool {
		return (nullptr != d->ui);
	}
	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
	}
	void MainWindow::OnCtkEvent(const ctkEvent& ctkEvent) {
		using MenuType = TC::Framework::MenuTypeEnum;
		MenuType menuAction = MenuType::Action;
		MenuType sudden = MenuType::NONE;
		if (ctkEvent.getProperty("TabName").isValid()) {
			OnTabFocused(ctkEvent);
		}
		else if (ctkEvent.getProperty(menuAction._to_string()).isValid()) {
			OnMenuAction(ctkEvent);
		}
		else if (ctkEvent.getProperty(sudden._to_string()).isValid()) {
			ForceClosePopUp();
		}
	}

	auto MainWindow::GetFeatureName() const -> QString {
		return "org.tomocube.projectmanagerPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}

	auto MainWindow::OnTabFocused(const ctkEvent& ctkEvent) -> void {
		auto TabName = ctkEvent.getProperty("TabName").toString();
		if (TabName.compare("Project Manager") == 0) {
			auto tcf_path = QString();
			MenuEvent menuEvt(MenuTypeEnum::TitleBar);
			menuEvt.scriptSet(tcf_path);
			publishSignal(menuEvt, "Project Manager");
		}
	}
	auto MainWindow::OnMenuAction(const ctkEvent& ctkEvent) -> void {
		using MenuType = TC::Framework::MenuTypeEnum;
		MenuType menuAction = MenuType::Action;
		auto actionName = ctkEvent.getProperty(menuAction._to_string()).toString();
		if (actionName.contains("Open P")) {
			openProjectfromMain();
		}
		else if (actionName.contains("Create P")) {
			createProjectfromMain();
		}
		else if (actionName.contains("About")) {
			QMessageBox msgBox;
			msgBox.setText("TomoAnalysis v1.5.1a");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);

			msgBox.exec();
		}
	}
	auto MainWindow::BroadcastViewerApp(QStringList tcfPath, QString prjPath) -> void {
		AppEvent appEvent(AppTypeEnum::APP_WITH_ARGS);
		appEvent.setFullName("org.tomocube.viewer2dPlugin");
		appEvent.setAppName("2D View");		
		appEvent.addParameter("TCFPath", tcfPath);
		appEvent.addParameter("Playground", prjPath);
		appEvent.addParameter("ExecutionType", "OpenTCF");

		publishSignal(appEvent, "Project Manager");
	}
	auto MainWindow::BroadcastSegmentationApp(QString tcfPath) -> void {

		AppEvent appEvent(AppTypeEnum::APP_WITH_ARGS);
		appEvent.setFullName("org.tomocube.intersegPlugin");
		appEvent.setAppName("Mask Editor");
		appEvent.addParameter("TCFPath", tcfPath);
		appEvent.addParameter("ExecutionType", "OpenTCF");

		TC::Framework::AppTypeEnum type = TC::Framework::AppTypeEnum::APP_WITH_ARGS;

		publishSignal(appEvent, "Project Manager");
	}
	auto MainWindow::BroadcastSingleRun(QString appName, QString symbolicName, QString procPath, QString tcfPath, QString playPath, QString hyperName) -> void {
		AppEvent appEvent(AppTypeEnum::APP_WITH_ARGS);
		appEvent.setFullName(symbolicName);

		appEvent.setAppName(appName);
		appEvent.addParameter("TCFPath", tcfPath);
		appEvent.addParameter("Processor path", procPath);
		appEvent.addParameter("Playground", playPath);
		appEvent.addParameter("Hypercube", hyperName);
		appEvent.addParameter("ExecutionType", "OpenTCF");

		publishSignal(appEvent, "Project Manager");
	}

	auto MainWindow::BroadcastBatchRun(QString appName, QString symbolicName, QString procPath, QString playPath, QString hyperName, QList<QStringList> cubes) -> void {
		AppEvent appEvent(AppTypeEnum::APP_WITH_SHARE);
		appEvent.setFullName(symbolicName);

		appEvent.setAppName(appName);
		appEvent.addParameter("ExecutionType", "BatchRun");
		if (false == procPath.isEmpty()) {
			appEvent.addParameter("Processor path", procPath);
		}

		appEvent.addParameter("Playground path", playPath);
		appEvent.addParameter("Hypercube name", hyperName);

		for (auto i = 0; i < cubes.size(); i++) {
			auto cube = cubes[i];
			auto cubeNum = QString("Cube") + QString::number(i + 1);

			/*QString cubeInfo;
			for (auto j = 0; j < cube.size(); j++) {
				cubeInfo += cube[j];
				if (j < cube.size() - 1) {
					cubeInfo += "#c#";
				}
			}
			appEvent.addParameter(cubeNum, cubeInfo);*/
			appEvent.addParameter(cubeNum, cube);
		}

		publishSignal(appEvent, "Project Manager");
	}
	auto MainWindow::BroadcastSetParam(QString val, QString appName, QString symbolicName, QString procName, QString algo_name, QString param_name) -> void {
		AppEvent appEvent(AppTypeEnum::ARGS_ONLY);
		appEvent.setFullName(symbolicName);

		appEvent.setAppName(appName);
		appEvent.addParameter("processor name", procName);
		appEvent.addParameter("algorithm name", algo_name);
		appEvent.addParameter("parameter name", param_name);
		appEvent.addParameter("parameter value", val);

		publishSignal(appEvent, "Project Manager");
	}
	auto MainWindow::BroadcastCopyParam(IParameter::Pointer param, QString target) -> void {
		MenuEvent menuEvt(MenuTypeEnum::ParameterSet);
		auto setter = QString("org.tomocube.projectmanagerPlugin");

		auto script = setter + "!" + target;
		menuEvt.scriptSet(script);
		publishSignal(menuEvt, "Project Manager");
	}
	auto MainWindow::BroadcastCloseApp(QString reportAppName) -> void {
		AppEvent appEvent(AppTypeEnum::APP_CLOSE);
		auto rr = reportAppName;
		rr.replace("[T]", "Time");
		auto lower = rr.toLower();
		auto fullname = "org.tomocube." + lower + "Plugin";

		appEvent.setFullName(fullname);
		appEvent.setAppName(reportAppName);		
		publishSignal(appEvent, "Project Manager");
	}
	auto MainWindow::BroadcastReport(QString resultpath, QString reportAppName) -> void {
		
		AppEvent appEvent(AppTypeEnum::APP_WITH_ARGS);
		//auto rr = reportAppName;//
		//rr.replace("[T]", "Time");
		//auto lower = rr.toLower();// reportAppName.toLower();
		//auto fullname = "org.tomocube." + lower + "Plugin";
		QString fullname = "org.tomocube.reportflPlugin";		
		appEvent.setFullName(fullname);
		//appEvent.setAppName(reportAppName);
		appEvent.setAppName("Report");
		appEvent.addParameter("ExecutionType", "OpenTCF");
		appEvent.addParameter("resultPath", resultpath);		
		publishSignal(appEvent, "Project Manager");
	}

}
