#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.projectmanagerPlugin";

namespace TomoAnalysis::ProjectManager::AppUI {
    class projectmanagerPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.projectmanagerPlugin")            
    public:
        static projectmanagerPlugin* getInstance();

        projectmanagerPlugin();
        ~projectmanagerPlugin() override;

        auto start(ctkPluginContext* context)->void override;
        auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                
            
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static projectmanagerPlugin* instance;
    };
}