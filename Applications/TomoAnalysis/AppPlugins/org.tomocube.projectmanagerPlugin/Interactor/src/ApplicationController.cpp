//UseCases
#include <ModifyApplication.h>
#include <ModifyApplicationParameter.h>

#include "ApplicationController.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ApplicationController::Impl {
        UseCase::IUpdateApplicationPort* outPort{ nullptr };
        UseCase::IProjectDataWritePort* writePort{ nullptr };
    };

    ApplicationController::ApplicationController(UseCase::IUpdateApplicationPort* outputPort, UseCase::IProjectDataWritePort* writer)
        : d{ new Impl } {
        d->outPort = outputPort;
        d->writePort = writer;
    }
    ApplicationController::~ApplicationController() {
        
    }

    auto ApplicationController::LoadApps(const QString& path)const-> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyApplication();        

        return useCase.LoadDlls(path, d->outPort);
    }    
    
    auto ApplicationController::LoadPlugin(const QString& path)const-> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyApplication();

        return useCase.ActivatePlugin(path,d->outPort);
    }

    auto ApplicationController::LoadProcessor(const QString& path)const-> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyApplication();
        
        return useCase.ActivateProcessor(path,d->outPort);
    }

    auto ApplicationController::LoadAppParameter(const QString& playgroundPath,const QString& path,const QString& parent )const-> bool {
        if(true == path.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyApplication();
        auto appPath = path.split("!")[1];

        return useCase.LoadPluginParameter(playgroundPath, appPath, d->outPort,parent);
    }

}