#include <iostream>
#include <QDirIterator>

#include "ProjectPresenter.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ProjectPresenter::Impl {
        IProjectExplorerPanel* explorerPanel{ nullptr };
        IOpSequencePanel* sequencePanel{ nullptr };
        IPlaygroundPanel* playgroundPanel{ nullptr };
        IPGNameListPanel* pgNameListPanel{ nullptr };
        IPreviewPanel* previewPanel{ nullptr };
        IApplicationParameterPanel* appParamPanel{nullptr};
    };

    ProjectPresenter::ProjectPresenter(IProjectExplorerPanel* explorerPanel, 
                                       IOpSequencePanel* sequencePanel,
                                       IPlaygroundPanel* playgroundPanel,
                                       IPreviewPanel* previewPanel,
                                       IPGNameListPanel* pgNameListPanel,
                                       IApplicationParameterPanel* appParamPanel)  
    : d{ new Impl } {
        d->explorerPanel = explorerPanel;
        d->sequencePanel = sequencePanel;
        d->playgroundPanel = playgroundPanel;
        d->previewPanel = previewPanel;
        d->pgNameListPanel = pgNameListPanel;
        d->previewPanel = previewPanel;
        d->appParamPanel = appParamPanel;
    }

    ProjectPresenter::~ProjectPresenter() {

    }

    auto ProjectPresenter::Load(const ProjectInfo::Pointer& info) -> void {
        if (nullptr == info.get()) {
            return;
        }

        if (nullptr != d->explorerPanel) {
            d->explorerPanel->Init(info);
        }

        if (nullptr != d->pgNameListPanel) {
            d->pgNameListPanel->Update(info);
        }

        if (nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if (nullptr != d->previewPanel) {
            d->previewPanel->Update(info);
        }
        if(nullptr != d->appParamPanel) {
            d->appParamPanel->Update(info);
        }
                
        if (nullptr != d->playgroundPanel) {
            d->playgroundPanel->Update(info->GetCurrentPlayground());
        }
    }

    auto ProjectPresenter::Update(const ProjectInfo::Pointer& info) -> void {
        if(nullptr == info.get()) {            
            return;
        }

        if(nullptr != d->explorerPanel) {
            d->explorerPanel->Update(info);
        }

        if(nullptr != d->pgNameListPanel) {            
            d->pgNameListPanel->Update(info);
        }

        if (nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if (nullptr != d->playgroundPanel) {
            d->playgroundPanel->Update(info->GetCurrentPlayground());
        }

        if(nullptr != d->previewPanel) {
            d->previewPanel->Update(info);
        }
    }

    auto ProjectPresenter::AddTCF(const ProjectInfo::Pointer& info, 
                                  const TCFDir::Pointer& dir) -> void {
        if (nullptr == info.get()) {
            return;
        }

        if (nullptr != d->explorerPanel) {
            d->explorerPanel->Update(info);
        }

        if (nullptr != d->pgNameListPanel) {
            d->pgNameListPanel->Update(info);
        }

        if (nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if (nullptr != d->previewPanel) {
            d->previewPanel->AddItem(dir);
        }
    }

    auto ProjectPresenter::AddItem(const Cube::Pointer& cube,
                                   const HyperCube::Pointer& hypercube,
                                   const TCFDir::List& tcfDirList) const ->void {
        if (nullptr == d->sequencePanel) {
            return;
        }

        auto ds = PlaygroundInfoDS();
        if (nullptr != cube) {
            PlaygroundInfoDS::CubeDS cubeDs;
            cubeDs.id = cube->GetID();
            cubeDs.name = cube->GetName();
            
            for (const auto& dir : cube->GetTCFDirList()) {
                if (nullptr == dir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfds;

                tcfds.path = dir->GetPath();
                tcfds.contents = dir->GetFileList();

                cubeDs.tcfDirList.append(tcfds);
            }

            ds.cubeList.append(cubeDs);
        }


        if (nullptr != hypercube) {
            PlaygroundInfoDS::HypercubeDS hypercubeDs;
            hypercubeDs.name = hypercube->GetName();

            for (const auto& linkedCube : hypercube->GetCubeList()) {
                if (nullptr == linkedCube) {
                    continue;
                }

                hypercubeDs.cubeList.append(linkedCube->GetID());
            }

            ds.hypercubeList.append(hypercubeDs);
        }

        if (false == tcfDirList.isEmpty()) {
            for (const auto& tcfDir : tcfDirList) {
                if (nullptr == tcfDir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfDirDs;
                tcfDirDs.path = tcfDir->GetPath();
                tcfDirDs.contents = tcfDir->GetFileList();

                ds.tcfDirList.append(tcfDirDs);
            }
        }

        d->playgroundPanel->AppendItem(ds);
        //d->sequencePanel->AppendItem(ds);
    }
}

