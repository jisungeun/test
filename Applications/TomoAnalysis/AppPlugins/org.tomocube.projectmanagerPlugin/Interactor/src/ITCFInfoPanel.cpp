#include "ITCFInfoPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ITCFInfoPanel::Impl {
        TCFInfoPanelDS::Pointer ds = std::make_shared<TCFInfoPanelDS>();
    };

    ITCFInfoPanel::ITCFInfoPanel() : d{ new Impl } {
    }

    ITCFInfoPanel::~ITCFInfoPanel() {
    }

    auto ITCFInfoPanel::GetDS() const -> TCFInfoPanelDS::Pointer {
        return d->ds;
    }
}