#include "IProjectExplorerPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IProjectExplorerPanel::Impl {
    };

    IProjectExplorerPanel::IProjectExplorerPanel() : d{ new Impl } {
    }

    IProjectExplorerPanel::~IProjectExplorerPanel() {
    }
}
