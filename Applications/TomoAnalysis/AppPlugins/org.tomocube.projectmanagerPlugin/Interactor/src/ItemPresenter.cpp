#include "IItemInfoPanel.h"
#include "ItemPresenter.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ItemPresenter::Impl {
        IItemInfoPanel* widget{ nullptr };
    };

    ItemPresenter::ItemPresenter(IItemInfoPanel* widget) : d{ new Impl } {
        d->widget = widget;
    }

    ItemPresenter::~ItemPresenter() {
        
    }

    auto ItemPresenter::Update(const Cube::Pointer& cubeItem) -> void {
        if(nullptr == cubeItem) {
            return;
        }
        if(nullptr == d->widget) {
            return;
        }
        d->widget->Update(cubeItem);
    }

    auto ItemPresenter::Update(const HyperCube::Pointer& hypercubeItem) -> void {
        if(nullptr == hypercubeItem) {
            return;
        }
        if(nullptr == d->widget) {
            return;
        }
        d->widget->Update(hypercubeItem);
    }

    auto ItemPresenter::Update(const PluginAppInfo::Pointer& appItem) -> void {
        if(nullptr == appItem) {
            return;
        }
        if(nullptr == d->widget) {
            return;
        }
        d->widget->Update(appItem);
    }

    auto ItemPresenter::Update(const TCFDir::Pointer tcfItem) -> void {
        if(nullptr == tcfItem) {
            return;
        }
        if(nullptr == d->widget) {
            return;
        }
        d->widget->Update(tcfItem);
    }
    auto ItemPresenter::Clear() -> void {
        if(nullptr==d->widget) {
            return;
        }
        d->widget->Clear();
    }

}