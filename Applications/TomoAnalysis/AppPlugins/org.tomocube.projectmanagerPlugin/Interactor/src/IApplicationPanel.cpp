#include "IApplicationPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IApplicationPanel::Impl {
        ApplicationDS::Pointer ds = std::make_shared<ApplicationDS>();
    };

    IApplicationPanel::IApplicationPanel() : d{ new Impl } {
    }

    IApplicationPanel::~IApplicationPanel() {
    }

    auto IApplicationPanel::GetDS() const -> ApplicationDS::Pointer {
        return d->ds;
    }
}