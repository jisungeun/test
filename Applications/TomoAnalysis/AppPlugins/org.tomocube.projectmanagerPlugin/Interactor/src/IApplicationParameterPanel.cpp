#include "IApplicationParameterPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IApplicationParameterPanel::Impl {
        ApplicationParameterDS::Pointer ds = std::make_shared<ApplicationParameterDS>();
    };

    IApplicationParameterPanel::IApplicationParameterPanel() : d{ new Impl } {
    }

    IApplicationParameterPanel::~IApplicationParameterPanel() {
    }

    auto IApplicationParameterPanel::GetDS() const -> ApplicationParameterDS::Pointer {
        return d->ds;
    }
}