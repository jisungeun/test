#include <QDirIterator>
#include <QMessageBox>
#include <QFileInfo>

#include <CreatePlayground.h>
#include <OpenPlayground.h>
#include <CreateCube.h>
#include <CreateHypercube.h>
#include <iostream>
#include <ModifyPlayground.h>

#include "PlaygroundController.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct PlaygroundController::Impl {
        UseCase::IUpdatePlaygroundPort* outPort{ nullptr };
        UseCase::IProjectDataReadPort* readPort{ nullptr };
        UseCase::IProjectDataWritePort* writePort{ nullptr };
    };

    PlaygroundController::PlaygroundController(UseCase::IUpdatePlaygroundPort* outPort,
                                               UseCase::IProjectDataWritePort* writer,
        UseCase::IProjectDataReadPort* reader)
    : d{ new Impl } {
        d->outPort = outPort;
        d->writePort = writer;
        d->readPort = reader;
    }

    PlaygroundController::~PlaygroundController() = default;

    auto PlaygroundController::Create(const QString& path) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreatePlayground();
        return useCase.Request(path, d->outPort, d->writePort);
    }

    auto PlaygroundController::Copy(const QString& path, const QString& target) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.Copy(path, target, d->outPort, d->readPort,d->writePort);
    }

    auto PlaygroundController::SetCurrent(const QString& path) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::OpenPlayground();
        return useCase.Request(path, d->outPort);
    }

    auto PlaygroundController::Rename(const QString& projectPath, const QString& name, const QString& newName) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        if (true == newName.isEmpty()) {
            return false;
        }

        QFileInfo fileInfo(projectPath);
        const auto dir = fileInfo.absoluteDir();
        QDir playgroundDir(dir.filePath("playground/"));

        const auto path = QString("%1.tcpg").arg(playgroundDir.filePath(name));
        const auto newPath = QString("%1.tcpg").arg(playgroundDir.filePath(newName));

        QFileInfo qdir(newPath);
        if (qdir.exists()) {
            QMessageBox::warning(nullptr, "Error", "Playground name already exists.");
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.Rename(path, newPath, d->outPort, d->writePort);
    }        

    auto PlaygroundController::RenameCube(const QString& projectPath, const QString& name, const QString& newName) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        if (true == newName.isEmpty()) {
            return false;
        }


        auto useCase = UseCase::ModifyPlayground();
        return useCase.RenameCube(projectPath, name,newName, d->outPort, d->writePort);
    }

    auto PlaygroundController::RenameHyperCube(const QString& projectPath, const QString& name, const QString& newName) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        if (true == newName.isEmpty()) {
            return false;
        }        

        auto useCase = UseCase::ModifyPlayground();
        return useCase.RenameHyperCube(projectPath, name, newName, d->outPort, d->writePort);
    }


    auto PlaygroundController::Delete(const QString& projectPath, const QString& name)->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        QFileInfo fileInfo(projectPath);
        const auto dir = fileInfo.absoluteDir();
        QDir playgroundDir(dir.filePath("playground/"));

        const auto path = QString("%1.tcpg").arg(playgroundDir.filePath(name));

        auto useCase = UseCase::ModifyPlayground();
        return useCase.Delete(path, d->outPort, d->writePort);
    }

    auto PlaygroundController::DeleteHyperCube(const QString& projectPath, const QString& name) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }
        if(projectPath.isEmpty()) {
            return false;
        }
        if(name.isEmpty()) {
            return false;
        }        

        auto useCase = UseCase::ModifyPlayground();
        return useCase.DeleteHyperCube(projectPath,name, d->outPort, d->writePort);
    }
    auto PlaygroundController::CopyHyperCube(const QString& projectPath, const QString& name) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        if(projectPath.isEmpty()) {
            return false;
        }
        if(name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.CopyHyperCube(projectPath,name,d->outPort,d->writePort);
    }
    auto PlaygroundController::CopyCube(const QString& projectPath, const QString& name) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        if(projectPath.isEmpty()) {
            return false;
        }
        if(name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.CopyCube(projectPath,name,d->outPort,d->writePort);
    }
    auto PlaygroundController::DeleteCube(const QString& projectPath, const QString& name) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }
        if (projectPath.isEmpty()) {
            return false;
        }
        if (name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.DeleteCube(projectPath,name, d->outPort, d->writePort);
    }


    auto PlaygroundController::AddCube(const QString& playgroundPath, const QString& name) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }                

        auto useCase = UseCase::CreateCube();
        return useCase.Request(playgroundPath, name, d->outPort, d->writePort);
    }

    auto PlaygroundController::AddCube(const QString& playgroundPath, const QString& name,const QString& hyper) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        if(true == hyper.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateCube();
        return useCase.Request(playgroundPath, name,hyper, d->outPort, d->writePort);
    }
    auto PlaygroundController::AddHypercube(const QString& playgroundPath, const QString& name) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateHypercube();
        return useCase.Request(playgroundPath, name, d->outPort, d->writePort);
    }

    auto PlaygroundController::CheckDuplicateHypercube(const QString& playgroundPath, const QString& name) const ->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateHypercube();
        return useCase.IsDuplicate(playgroundPath, name);
    }

    auto PlaygroundController::CheckDuplicateCube(const QString& playgroundPath, const QString& hypercube, const QString& cube) const ->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == hypercube.isEmpty()) {
            return false;
        }

        if (true == cube.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateCube();
        return useCase.IsDuplicate(playgroundPath, hypercube, cube);
    }

    auto PlaygroundController::LinkTCFDirectory(const QString& playgroundPath, const QString& folderPath) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.LinkFolder(playgroundPath, folderPath, d->outPort, d->writePort);
    }
    auto PlaygroundController::DeleteHistory(const QString& path) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.DeleteHistory(path, d->outPort);
    }
    auto PlaygroundController::DuplicateTCFDirectory(const QString& playgroundPath, const QString& folderPath) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.DuplicateTCFFolderItem(playgroundPath, folderPath, d->outPort, nullptr);
    }

    auto PlaygroundController::LinkApplication(const QString& playgroundPath, const QString& appPath, const QString& appName, const QStringList&procList,const QString& hyperName,std::tuple<bool,bool> appType) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.LinkApplication(playgroundPath, appPath,appName, procList,hyperName, appType,d->outPort, d->writePort);
    }

    auto PlaygroundController::ClearApplication(const QString& playgroundPath) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.ClearApplication(playgroundPath,d->outPort);
    }


    auto PlaygroundController::ShowApplication(const QString& playgroundPath, const QString& appName, const QString& hyperName) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == appName.isEmpty()) {
            return false;
        }
        if(true == hyperName.isEmpty()) {
            return false;
        }        
        auto useCase = UseCase::ModifyPlayground();        
        return useCase.ShowLinkedApplication(playgroundPath,appName,hyperName,d->outPort);
    }


    auto PlaygroundController::LinkApplication(const QString& playgroundPath, const QString& appPath,const QStringList& procList) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }
        
        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }                

        auto useCase = UseCase::ModifyPlayground();
        return useCase.LinkApplication(playgroundPath, appPath,procList, d->outPort, d->writePort);
    }

    auto PlaygroundController::DuplicateApplication(const QString& playgroundPath, const QString& appPath) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        /*
        if (nullptr == d->writePort) {
            return false;
        }*/

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.DuplicateApplicationtItem(playgroundPath, appPath, d->outPort, nullptr);
    }
    auto PlaygroundController::LinkTCFsToCube(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == tcfList.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == d->outPort) {
            return false;
        }
        if (nullptr == d->writePort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.LinkTCFs(playgroundPath, tcfList, cubeName, d->outPort, d->writePort);
        //return useCase.LinkFolderToCube(playgroundPath, folderPath, cubeName, d->outPort, d->writePort);
    }
    auto PlaygroundController::UnLinkTCFsToCube(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == tcfList.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == d->outPort) {
            return false;
        }
        if (nullptr == d->writePort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        //return useCase.LinkFolderToCube(playgroundPath, folderPath, cubeName, d->outPort, d->writePort);
        return useCase.UnlinkTCFs(playgroundPath, tcfList, cubeName, d->outPort, d->writePort);
    }

    auto PlaygroundController::LinkTCFToCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName) -> bool {                        
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == folderPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();        
        return useCase.LinkFolderToCube(playgroundPath, folderPath, cubeName, d->outPort, d->writePort);
    }
    auto PlaygroundController::UnlinkTcfFromCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == folderPath.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == d->outPort) {
            return false;
        }
        if (nullptr == d->writePort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.UnLinkFolderFromCube(playgroundPath, folderPath, cubeName, d->outPort, d->writePort);
    }
    auto PlaygroundController::LinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName) -> bool {
        if(true==playgroundPath.isEmpty()) {
            return false;
        }
        if (true == hyperCubeName.isEmpty()) {
            return false;
        }
        if(true ==cubeName.isEmpty()) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.LinkCubeToHyperCube(playgroundPath,cubeName,hyperCubeName,d->outPort,d->writePort);
    }
    auto PlaygroundController::UnlinkCubeFromHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperName) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == hyperName.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == d->writePort) {
            return false;
        }
        if (nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.UnLinkCubeToHyperCube(playgroundPath, cubeName, hyperName, d->outPort, d->writePort);
    }
    auto PlaygroundController::LinkAppToCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName) -> bool {
        if(nullptr == d->writePort) {
            return false;
        }
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();        
        return useCase.LinkAppToCube(playgroundPath,appPath,cubeName,d->outPort,d->writePort);
    }
    auto PlaygroundController::LinkAppToHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName) -> bool {
        if(nullptr == d->writePort) {
            return false;
        }
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appPath.isEmpty()) {
            return false;
        }
        if(true == hypercubeName.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();        
        return useCase.LinkAppToHyperCube(playgroundPath, appPath, hypercubeName, d->outPort, d->writePort);
    }
    auto PlaygroundController::UnlinkAppFromCube(const QString& playgroundPath, const QString& appPath, const QString cubeName) -> bool {
        if(nullptr == d->writePort) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.UnLinkAppFromCube(playgroundPath,appPath,cubeName,d->outPort,d->writePort);
    }
    auto PlaygroundController::UnlinkAppFromHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName) -> bool {
        if(nullptr == d->writePort) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appPath.isEmpty()) {
            return false;
        }
        if(true ==hypercubeName.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyPlayground();
        return useCase.UnLinkAppFromHyperCube(playgroundPath, appPath, hypercubeName, d->outPort, d->writePort);
    }
    auto PlaygroundController::MoveAppItem(const QString& playgroundPath, const QString& appPath, const QPoint& pos) -> bool {
        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.MoveApplication(playgroundPath, appPath, pos, nullptr, d->writePort);
    }
    auto PlaygroundController::MoveCubeItem(const QString& playgroundPath, const QString& cubeName, const QPoint& pos) -> bool {
        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == cubeName.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.MoveCube(playgroundPath, cubeName, pos, nullptr, d->writePort);
    }
    auto PlaygroundController::MoveHyperItem(const QString& playgroundPath, const QString& hyperName, const QPoint& pos) -> bool {
        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == hyperName.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.MoveHyperCube(playgroundPath, hyperName, pos, nullptr, d->writePort);
    }
    auto PlaygroundController::MoveTCFItem(const QString& playgroundPath, const QString& folderPath, const QPoint& pos) -> bool {
        if (nullptr == d->writePort) {
            return false;
        }

        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyPlayground();
        return useCase.MoveTCF(playgroundPath, folderPath, pos, nullptr, d->writePort);        
    }

}
