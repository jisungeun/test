#include <iostream>

#include <CreateProject.h>
#include <OpenProject.h>
#include <ModifyProject.h>

#include "ProjectController.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ProjectController::Impl {
        UseCase::IUpdateProjectPort* outPort{ nullptr };
        UseCase::IProjectDataWritePort* writePort{ nullptr };
        UseCase::IProjectDataReadPort* readPort{ nullptr };
    };

    ProjectController::ProjectController() : d{ new Impl } {

    }

    ProjectController::ProjectController(UseCase::IUpdateProjectPort* outPort,
                                         UseCase::IProjectDataWritePort* writer,
                                         UseCase::IProjectDataReadPort* reader)
        : d(new Impl()) {
        d->outPort = outPort;
        d->writePort = writer;
        d->readPort = reader;
    }

    ProjectController::~ProjectController() = default;

    auto ProjectController::Create(const QString& path) const ->bool {

        if(nullptr == d->outPort) {
            return false;
        }

        if(nullptr == d->writePort) {
            return false;
        }

        if(true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateProject();
        return useCase.Request(path, d->outPort, d->writePort);
    }
    auto ProjectController::Rename(const QString& path, const QString& newName) const -> bool {
        if(nullptr == d->outPort) {
            return false;
        }
        if(nullptr == d->writePort) {
            return false;
        }
        if(true == newName.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyProject();
        return useCase.RenameProject(path,newName,d->outPort,d->writePort);
    }

    auto ProjectController::CreateSub(const QString& path) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::CreateProject();
        return useCase.RequestSubProject(path, d->outPort, d->writePort);
    }

    auto ProjectController::Open(const QString& path) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->readPort) {
            return false;
        }

        if (true == path.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::OpenProject();
        return useCase.Request(path, d->outPort, d->readPort);
    }

    auto ProjectController::LinkTCFFolder(const QString& projectPath,const QString& folderPath) const ->bool {        
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }
        
        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyProject();
        return useCase.LinkFolder(projectPath, folderPath, d->outPort, d->writePort);
    }

    auto ProjectController::UnlinkTCFFolder(const QString& projectPath, const QString& folderPath) const ->bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyProject();
        return useCase.UnlinkFolder(projectPath, folderPath, d->outPort, d->writePort);
    }

    auto ProjectController::LinkWorkset(const QString& projectPath, int worksetId, const QString& worksetName, const QString& folderPath) const -> bool {
        if (nullptr == d->outPort) {
            return false;
        }

        if (nullptr == d->writePort) {
            return false;
        }

        if (true == projectPath.isEmpty()) {
            return false;
        }

        if (true == worksetName.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        auto useCase = UseCase::ModifyProject();
        return useCase.LinkWorkset(projectPath, worksetId, worksetName, folderPath, d->outPort, d->writePort);
    }
}
