#include "IProjectPathPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IProjectPathPanel::Impl {
        IProjectPathDS::Pointer ds = std::make_shared<IProjectPathDS>();
    };

    IProjectPathPanel::IProjectPathPanel() : d{ new Impl } {
    }

    IProjectPathPanel::~IProjectPathPanel() {
    }

    auto IProjectPathPanel::GetDS() const -> IProjectPathDS::Pointer {
        return d->ds;
    }
}