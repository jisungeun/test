#include <iostream>

#include "PlaygroundPresenter.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct PlaygroundPresenter::Impl {
        IProjectExplorerPanel* explorerPanel{ nullptr };
        IOpSequencePanel* sequencePanel{ nullptr };
        IPlaygroundPanel* playgroundPanel{ nullptr };
        IPGNameListPanel* pgNameListPanel{ nullptr };
        IPreviewPanel* previewPanel{ nullptr };
        IApplicationParameterPanel* parameterPanel{ nullptr };
    };

    PlaygroundPresenter::PlaygroundPresenter(IProjectExplorerPanel* explorerPanel,
                                             IOpSequencePanel* sequencePanel,
                                             IPlaygroundPanel* playgroundPanel,
                                             IPGNameListPanel* pgNameListPanel,
        IPreviewPanel* previewPanel,
        IApplicationParameterPanel* parameterPanel)
    : d{ new Impl } {
        d->explorerPanel = explorerPanel;
        d->sequencePanel = sequencePanel;
        d->playgroundPanel = playgroundPanel;
        d->pgNameListPanel = pgNameListPanel;
        d->previewPanel = previewPanel;
        d->parameterPanel = parameterPanel;
    }

    PlaygroundPresenter::~PlaygroundPresenter() {

    }

    auto PlaygroundPresenter::Refresh(const PlaygroundInfo::Pointer& info) -> void {
        if(nullptr == info.get()) {
            return;
        }

        if (nullptr == d->playgroundPanel) {
            return;
        }

        auto ds = PlaygroundInfoDS();

        ds.name = info->GetName();
        ds.path = info->GetPath();

        // hypercube
        for (const auto& hypercube : info->GetHyperCubeList()) {            
            if (nullptr == hypercube) {
                continue;
            }

            PlaygroundInfoDS::HypercubeDS hypercubeDs;
            hypercubeDs.name = hypercube->GetName();
            hypercubeDs.pos = hypercube->GetPosition();
            for(const auto& cube : hypercube->GetCubeList()) {
                if(nullptr == cube) {
                    continue;
                }

                hypercubeDs.cubeList.append(cube->GetID());
            }
            for(const auto& app : hypercube->GetMeasurementAppInfoList()) {
                if(nullptr == app) {
                    continue;
                }
                hypercubeDs.appList.append(app->GetPath());
            }

            ds.hypercubeList.append(hypercubeDs);
        }

        // cube
        for (const auto& cube : info->GetCubeList()) {
            if (nullptr == cube) {
                continue;
            }

            PlaygroundInfoDS::CubeDS cubeDs;
            cubeDs.id = cube->GetID();
            cubeDs.name = cube->GetName();
            cubeDs.pos = cube->GetPosition();
            for (const auto& dir : cube->GetTCFDirList()) {
                if(nullptr == dir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfDirDs;

                tcfDirDs.path = dir->GetPath();
                tcfDirDs.contents = dir->GetFileList();                
                tcfDirDs.pos = dir->GetPosition();
                cubeDs.tcfDirList.append(tcfDirDs);
            }
            for(const auto& app : cube->GetMeasurementAppInfoList()) {
                if(nullptr == app) {
                    continue;
                }
                cubeDs.appList.append(app->GetPath());
            }
            ds.cubeList.append(cubeDs);
        }

        // TCF Dir in project
        for(const auto& dir : info->GetTCFDirList()) {
            if(nullptr ==  dir) {
                continue;
            }

            PlaygroundInfoDS::TCFDirDS dirDs;

            dirDs.path = dir->GetPath();
            dirDs.contents = dir->GetFileList();
            dirDs.pos = dir->GetPosition();
            ds.tcfDirList.append(dirDs);
        }

        for(const auto& app : info->GetAppList()) {
            if(nullptr == app) {
                continue;
            }

            PlaygroundInfoDS::AppDS AppDs;

            AppDs.name = app->GetName();
            AppDs.path = app->GetPath();
            AppDs.pos = app->GetPosition();
            ds.appList.append(AppDs);
        }

        d->playgroundPanel->Refresh(ds);
    }

    auto PlaygroundPresenter::Update(const ProjectInfo::Pointer& info)->void {
        if(nullptr ==  info) {
            return;
        }

        if (nullptr != d->explorerPanel) {
            d->explorerPanel->Update(info);
        }

        if (nullptr != d->pgNameListPanel) {
            d->pgNameListPanel->Update(info);
        }

        if(nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if(nullptr != d->playgroundPanel) {            
            d->playgroundPanel->Update(info->GetCurrentPlayground());            
        }

        if(nullptr != d->previewPanel) {
            d->previewPanel->Update(info);
        }

        if(nullptr != d->parameterPanel) {
            d->parameterPanel->Update(info);
        }
    }

    auto PlaygroundPresenter::UpdateLink(const ProjectInfo::Pointer& info)->void {
        if (nullptr == info) {
            return;
        }

        if (nullptr != d->explorerPanel) {
            d->explorerPanel->Update(info);
        }

        if (nullptr != d->pgNameListPanel) {
            d->pgNameListPanel->Update(info);
        }

        if (nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if (nullptr != d->playgroundPanel) {
            d->playgroundPanel->Update(info->GetCurrentPlayground());
        }

        if (nullptr != d->previewPanel) {
            d->previewPanel->UpdateLink(info);
        }

        if (nullptr != d->parameterPanel) {
            d->parameterPanel->Update(info);
        }
    }

    auto PlaygroundPresenter::ChangeCurrent(const ProjectInfo::Pointer& info)->void {
        if (nullptr == info) {
            return;
        }

        if (nullptr != d->sequencePanel) {
            d->sequencePanel->Update(info);
        }

        if (nullptr != d->playgroundPanel) {
            d->playgroundPanel->Update(info->GetCurrentPlayground());
        }

        if (nullptr != d->parameterPanel) {
            d->parameterPanel->Update(info);
        }

        if (nullptr != d->previewPanel) {
            d->previewPanel->UpdateLink(info);
        }
    }

    auto PlaygroundPresenter::ModifyItem(const Cube::Pointer& cube, const HyperCube::Pointer& hypercube, const TCFDir::List& tcfDirList, const PluginAppInfo::List& appList) -> void {
        if(nullptr == d->playgroundPanel) {
            return;
        }
        auto ds = PlaygroundInfoDS();

        if (nullptr != cube) {
            PlaygroundInfoDS::CubeDS cubeDs;
            cubeDs.id = cube->GetID();
            cubeDs.name = cube->GetName();
            cubeDs.pos = cube->GetPosition();
            for (const auto& dir : cube->GetTCFDirList()) {
                if (nullptr == dir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfds;

                tcfds.path = dir->GetPath();
                tcfds.contents = dir->GetFileList();

                cubeDs.tcfDirList.append(tcfds);
            }
            for( const auto& app : cube->GetMeasurementAppInfoList()) {
                if(nullptr == app) {
                    continue;
                }
                cubeDs.appList.append(app->GetPath());
            }

            ds.cubeList.append(cubeDs);
        }

        if (nullptr != hypercube) {
            PlaygroundInfoDS::HypercubeDS hypercubeDs;
            hypercubeDs.name = hypercube->GetName();
            hypercubeDs.pos = hypercube->GetPosition();
            for (const auto& linkedCube : hypercube->GetCubeList()) {
                if (nullptr == linkedCube) {
                    continue;
                }                
                hypercubeDs.cubeList.append(linkedCube->GetID());
            }

            for(const auto& app : hypercube->GetMeasurementAppInfoList()) {
                if(nullptr == app) {
                    continue;
                }
                hypercubeDs.appList.append(app->GetPath());
            }

            ds.hypercubeList.append(hypercubeDs);
        }

        if (false == tcfDirList.isEmpty()) {
            for (const auto& tcfDir : tcfDirList) {
                if (nullptr == tcfDir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfDirDs;
                tcfDirDs.pos = tcfDir->GetPosition();
                tcfDirDs.path = tcfDir->GetPath();
                tcfDirDs.contents = tcfDir->GetFileList();

                ds.tcfDirList.append(tcfDirDs);
            }
        }

        if (false == appList.isEmpty()) {
            for (const auto& app : appList) {
                if (nullptr == app) {
                    continue;
                }
                PlaygroundInfoDS::AppDS appDs;
                appDs.pos = app->GetPosition();
                appDs.path = app->GetPath();
                appDs.name = app->GetName();

                ds.appList.append(appDs);
            }
        }
        d->playgroundPanel->ModifyItem(ds);        
    }
    auto PlaygroundPresenter::AddItem(const Cube::Pointer& cube, const HyperCube::Pointer& hypercube) -> void {
        if (nullptr == d->playgroundPanel) {
            return;
        }

        auto ds = PlaygroundInfoDS();

        if (nullptr != cube) {
            PlaygroundInfoDS::CubeDS cubeDs;
            cubeDs.id = cube->GetID();
            cubeDs.name = cube->GetName();
            cubeDs.pos = cube->GetPosition();
            for (const auto& dir : cube->GetTCFDirList()) {
                if (nullptr == dir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfds;

                tcfds.path = dir->GetPath();
                tcfds.contents = dir->GetFileList();

                cubeDs.tcfDirList.append(tcfds);
            }

            ds.cubeList.append(cubeDs);
        }

        if (nullptr != hypercube) {
            PlaygroundInfoDS::HypercubeDS hypercubeDs;
            hypercubeDs.name = hypercube->GetName();
            hypercubeDs.pos = hypercube->GetPosition();
            for (const auto& linkedCube : hypercube->GetCubeList()) {
                if (nullptr == linkedCube) {
                    continue;
                }

                hypercubeDs.cubeList.append(linkedCube->GetID());
            }

            ds.hypercubeList.append(hypercubeDs);
        }
        //d->playgroundPanel->AddLinkCube(hypercube->GetName(), cube->GetName());
    }
    auto PlaygroundPresenter::AddItem(const Cube::Pointer& cube,
                                      const HyperCube::Pointer& hypercube,
                                      const TCFDir::List& tcfDirList,
                                      const PluginAppInfo::List& appList)->void {
        if (nullptr == d->playgroundPanel) {
            return;
        }

        auto ds = PlaygroundInfoDS();

        if(nullptr != cube) {
            PlaygroundInfoDS::CubeDS cubeDs;
            cubeDs.id = cube->GetID();
            cubeDs.name = cube->GetName();
            cubeDs.pos = cube->GetPosition();
            for (const auto& dir : cube->GetTCFDirList()) {
                if (nullptr == dir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfds;

                tcfds.path = dir->GetPath();
                tcfds.contents = dir->GetFileList();                                

                cubeDs.tcfDirList.append(tcfds);
            }

            ds.cubeList.append(cubeDs);
        }

        if (nullptr != hypercube) {
            PlaygroundInfoDS::HypercubeDS hypercubeDs;
            hypercubeDs.name = hypercube->GetName();
            hypercubeDs.pos = hypercube->GetPosition();
            for (const auto& linkedCube : hypercube->GetCubeList()) {
                if (nullptr == linkedCube) {
                    continue;
                }

                hypercubeDs.cubeList.append(linkedCube->GetID());
            }

            ds.hypercubeList.append(hypercubeDs);
        }

        if(false == tcfDirList.isEmpty()) {
            for (const auto& tcfDir : tcfDirList) {
                if (nullptr == tcfDir) {
                    continue;
                }

                PlaygroundInfoDS::TCFDirDS tcfDirDs;
                tcfDirDs.pos = tcfDir->GetPosition();
                tcfDirDs.path = tcfDir->GetPath();
                tcfDirDs.contents = tcfDir->GetFileList();

                ds.tcfDirList.append(tcfDirDs);
            }
        }

        if(false == appList.isEmpty()) {
            for(const auto& app : appList) {
                if(nullptr == app) {
                    continue;
                }
                PlaygroundInfoDS::AppDS appDs;
                appDs.pos = app->GetPosition();
                appDs.path = app->GetPath();
                appDs.name = app->GetName();                                

                ds.appList.append(appDs);
            }
        }
        d->playgroundPanel->AppendItem(ds);
    }

    auto PlaygroundPresenter::LinkTCFDirToProject(const ProjectInfo::Pointer& project,
                                                  const TCFDir::Pointer& tcfDir)->void {
        if(nullptr == d->explorerPanel) {
            return;
        }

        if(nullptr == project) {
            return;
        }

        if(nullptr ==  tcfDir) {
            return;
        }

        /*ProjectExplorerDS::TCFDS tcfDS;
        tcfDS.path = tcfDir->GetPath();
        tcfDS.contents = tcfDir->GetFileList();

        d->explorerPanel->LinkTCF(project->GetPath(), tcfDS);*/
    }
}
