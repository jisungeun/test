
#include <iostream>
#include <ShowItemInfo.h>

#include "ItemController.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ItemController::Impl {
        UseCase::IUpdateItemPort* outPort{ nullptr };
    };

    ItemController::ItemController(UseCase::IUpdateItemPort* outPort) : d{ new Impl } {
        d->outPort = outPort;
    }
    ItemController::~ItemController() {
        
    }

    auto ItemController::ShowAppItemInfo(const QString& playgroundPath, const QString& appItemInfo) -> bool {
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appItemInfo.isEmpty()) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }

        auto useCase = UseCase::ShowItemInfo();
        return useCase.UpdateAppInfo(playgroundPath, appItemInfo, d->outPort);                
    }

    auto ItemController::ShowCubeItemInfo(const QString& playgroundPath, const QString& cubeItemInfo) -> bool {
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == cubeItemInfo.isEmpty()) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }        

        auto useCase = UseCase::ShowItemInfo();        
        return useCase.UpdateCubeInfo(playgroundPath, cubeItemInfo, d->outPort);
    }
    auto ItemController::ClearItemInfo() -> bool {
        if (nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ShowItemInfo();
        return useCase.ClearItemInfo(d->outPort);
    }

    auto ItemController::ShowTcfItemInfo(const QString& playgroundPath, const QString& tcfItemInfo) -> bool {
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == tcfItemInfo.isEmpty()) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ShowItemInfo();
        return useCase.UpdateTcfInfo(playgroundPath, tcfItemInfo, d->outPort);        
    }

    auto ItemController::ShowHypercubeItemInfo(const QString& playgroundPath, const QString& hyperItemInfo) -> bool {
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == hyperItemInfo.isEmpty()) {
            return false;
        }
        if(nullptr == d->outPort) {
            return false;
        }
        auto useCase = UseCase::ShowItemInfo();
        return useCase.UpdateHypercubeInfo(playgroundPath, hyperItemInfo, d->outPort);
    }

}