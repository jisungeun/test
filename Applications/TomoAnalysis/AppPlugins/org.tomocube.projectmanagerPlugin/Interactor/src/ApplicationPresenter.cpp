#include "ApplicationPresenter.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct ApplicationPresenter::Impl {
        IApplicationPanel* applicationPanel{ nullptr };
        IApplicationParameterPanel* parameterPanel{ nullptr };
    };

    ApplicationPresenter::ApplicationPresenter(IApplicationPanel* applicationPanel, IApplicationParameterPanel* parameterPanel)
        :d{ new Impl } {
        d->applicationPanel = applicationPanel;
        d->parameterPanel = parameterPanel;
    }
    ApplicationPresenter::~ApplicationPresenter() {
        
    }
    auto ApplicationPresenter::Update(const ProcessorInfo::List& procInfo,const PluginAppInfo::List& pluginList) -> void {        
        if (nullptr == d->parameterPanel) {
            return;
        }
        auto ds = ApplicationParameterDS();

        for(const auto& plugin : pluginList) {
            ds.parentName = plugin->GetName();
        }

        for(const auto & processor : procInfo) {
            ApplicationParameterDS::ProcessorDS proc;
            proc.name = processor->GetName();
            proc.path = processor->GetPath();
            proc.loaded = processor->GetLoaded();
            proc.parent_plugin = processor->GetParent();
            ds.processorList.push_back(proc);
        }
        d->parameterPanel->Update(ds);
    }

    auto ApplicationPresenter::Update(const PluginAppInfo::List& pluginInfo, const ProcessorInfo::List& procInfo) -> void {
        if (0 == pluginInfo.size() && 0 == procInfo.size()) {
            return;
        }
        if (nullptr == d->applicationPanel) {
            return;
        }        

        auto ds = ApplicationDS();
        for(const auto & plugins : pluginInfo) {
            ApplicationDS::PluginDS app;
            app.name = plugins->GetName();
            app.path = plugins->GetPath();
            app.loaded = plugins->GetLoaded();
            ds.pluginList.push_back(app);
        }
        for(const auto & processor : procInfo) {
            ApplicationDS::ProcessorDS proc;
            proc.name = processor->GetName();
            proc.path = processor->GetPath();
            proc.loaded = processor->GetLoaded();            
            proc.parent_plugin = processor->GetParent();
            ds.processorList.push_back(proc);
        }

        d->applicationPanel->Update(ds);        
    }
}