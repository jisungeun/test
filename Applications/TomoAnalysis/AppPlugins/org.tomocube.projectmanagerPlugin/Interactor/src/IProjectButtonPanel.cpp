#include "IProjectButtonPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IProjectButtonPanel::Impl {
        ProjectButtonDS::Pointer ds = std::make_shared<ProjectButtonDS>();
    };

    IProjectButtonPanel::IProjectButtonPanel() : d{ new Impl } {
    }

    IProjectButtonPanel::~IProjectButtonPanel() {
    }

    auto IProjectButtonPanel::GetDS() const -> ProjectButtonDS::Pointer {
        return d->ds;
    }
}