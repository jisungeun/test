#include "IPlaygroundPanel.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    struct IPlaygroundPanel::Impl {
    };

    IPlaygroundPanel::IPlaygroundPanel() : d{ new Impl } {
    }

    IPlaygroundPanel::~IPlaygroundPanel() {
    }
}