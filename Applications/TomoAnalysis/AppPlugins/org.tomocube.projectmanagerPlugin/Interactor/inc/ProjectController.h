#pragma once

#include <memory>

#include <IUpdateProjectPort.h>
#include <IProjectDataWritePort.h>
#include <IProjectDataReadPort.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API ProjectController final {
    public:
        ProjectController();
        ProjectController(UseCase::IUpdateProjectPort* outPort,
                          UseCase::IProjectDataWritePort* writer,
                          UseCase::IProjectDataReadPort* reader);
        virtual ~ProjectController();

        auto Create(const QString& path) const ->bool;
        auto Rename(const QString& path,const QString& newName)const->bool;
        auto CreateSub(const QString& path) const ->bool;

        auto Open(const QString& path) const ->bool;

        auto LinkTCFFolder(const QString& projectPath, const QString& folderPath) const ->bool;
        auto UnlinkTCFFolder(const QString& projectPath, const QString& folderPath) const ->bool;

        auto LinkWorkset(const QString& projectPath, int worksetId, const QString& worksetName, const QString& folderPath) const -> bool;

        private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}