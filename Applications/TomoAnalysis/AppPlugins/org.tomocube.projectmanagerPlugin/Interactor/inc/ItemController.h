#pragma once

#include <memory>

#include <IUpdateItemPort.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API ItemController {
    public:
        ItemController(UseCase::IUpdateItemPort* outPort);
        virtual ~ItemController();

        auto ShowTcfItemInfo(const QString& playgroundPath, const QString& tcfItemInfo)->bool;
        auto ShowCubeItemInfo(const QString& playgroundPath, const QString& cubeItemInfo)->bool;
        auto ShowHypercubeItemInfo(const QString& playgroundPath, const QString& hyperItemInfo)->bool;
        auto ShowAppItemInfo(const QString& playgroundPath, const QString& appItemInfo)->bool;
        auto ClearItemInfo()->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}