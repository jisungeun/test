#pragma once

#include <memory>

#include <IUpdateApplicationPort.h>
#include <IProjectDataWritePort.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor{
	class ProjectManagerInteractor_API ApplicationController {
	public:
		ApplicationController(UseCase::IUpdateApplicationPort* outputPort, UseCase::IProjectDataWritePort* writer);
		virtual ~ApplicationController();;

		auto LoadPlugin(const QString& path)const-> bool;
		auto LoadProcessor(const QString& path)const-> bool;
		auto LoadApps(const QString& path)const-> bool;		
		auto LoadAppParameter(const QString& playgroundPath,const QString& path,const QString& parent=QString())const-> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}