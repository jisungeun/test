#pragma once

#include <memory>
#include <IUpdateItemPort.h>


#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class IItemInfoPanel;
    class ProjectManagerInteractor_API ItemPresenter : public UseCase::IUpdateItemPort {
    public:
        ItemPresenter(IItemInfoPanel* widget =nullptr);
        virtual ~ItemPresenter();

        auto Update(const TCFDir::Pointer tcfItem)->void override;
        auto Update(const Cube::Pointer& cubeItem)->void override;
        auto Update(const HyperCube::Pointer& hypercubeItem)->void override;
        auto Update(const PluginAppInfo::Pointer& appItem)->void override;

        auto Clear()->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}