#pragma once

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API IProjectManagerWidget {
    public:
        IProjectManagerWidget();
        virtual ~IProjectManagerWidget();

        virtual auto Update()->void = 0;
    };
}
