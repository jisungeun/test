#pragma once

#include <memory>
#include <QString>

#include <PlaygroundInfo.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API PlaygroundInfoDS {
		struct TCFDirDS {
			QString path;
			QPoint pos;
			QStringList contents;
			QList<TCFDirDS> subDir;
		};
		struct AppDS {
			QPoint pos;
			QString path;
			QString name;			
		};
		struct CubeDS {
			int id;
		    QString name;
			QPoint pos;
			QList<QString> appList;
			QList<TCFDirDS> tcfDirList;
		};

        struct HypercubeDS {
			QString name;
			QPoint pos;
			QList<QString> appList;
			QList<int> cubeList;
        };		

		QString name;
		QString path;
		QList<CubeDS> cubeList;
		QList<HypercubeDS> hypercubeList;
		QList<TCFDirDS> tcfDirList;
		QList<AppDS> appList;

		typedef std::shared_ptr<PlaygroundInfoDS> Pointer;
	};

	class ProjectManagerInteractor_API IPlaygroundPanel {
	public:
		IPlaygroundPanel();
		virtual ~IPlaygroundPanel();

		virtual auto Refresh(const PlaygroundInfoDS& ds)->bool = 0;
		virtual auto AppendItem(const PlaygroundInfoDS& ds)->bool = 0;
		virtual auto ModifyItem(const PlaygroundInfoDS& ds)->bool = 0;
		virtual auto Update(const PlaygroundInfo::Pointer& pg)->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}