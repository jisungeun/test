#pragma once

#include <memory>

#include <ProjectInfo.h>
#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API IPreviewPanel {
    public:
        IPreviewPanel();
        virtual ~IPreviewPanel();

        virtual auto Update(const ProjectInfo::Pointer& info)->bool = 0;
        virtual auto UpdateLink(const ProjectInfo::Pointer& info)->bool = 0;
        virtual auto AddItem(const TCFDir::Pointer& dir)->bool = 0;
    };
}