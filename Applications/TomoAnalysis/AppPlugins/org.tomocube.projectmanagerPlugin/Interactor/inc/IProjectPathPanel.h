#pragma once

#include <memory>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API IProjectPathDS {
		typedef std::shared_ptr<IProjectPathDS> Pointer;
	};

	class ProjectManagerInteractor_API IProjectPathPanel {
	public:
		IProjectPathPanel();
		virtual ~IProjectPathPanel();

		auto GetDS() const->IProjectPathDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}