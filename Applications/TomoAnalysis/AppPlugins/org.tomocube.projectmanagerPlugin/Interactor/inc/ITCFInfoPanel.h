#pragma once

#include <memory>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API TCFInfoPanelDS {
		typedef std::shared_ptr<TCFInfoPanelDS> Pointer;
	};

	class ProjectManagerInteractor_API ITCFInfoPanel {
	public:
		ITCFInfoPanel();
		virtual ~ITCFInfoPanel();

		auto GetDS() const->TCFInfoPanelDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}