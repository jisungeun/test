#pragma once

#include <memory>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API ProjectButtonDS {
		typedef std::shared_ptr<ProjectButtonDS> Pointer;
	};

	class ProjectManagerInteractor_API IProjectButtonPanel {
	public:
		IProjectButtonPanel();
		virtual ~IProjectButtonPanel();

		auto GetDS() const->ProjectButtonDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}