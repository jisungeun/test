#pragma once

#include <memory>
#include <ProjectInfo.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	class ProjectManagerInteractor_API IProjectExplorerPanel {
	public:
		IProjectExplorerPanel();
		virtual ~IProjectExplorerPanel();

		virtual auto Init(const ProjectInfo::Pointer& project)->bool = 0;
	    virtual auto Update(const ProjectInfo::Pointer& project)->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}