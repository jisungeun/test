#pragma once

#include <memory>
#include <IUpdateApplicationPort.h>
#include <IApplicationPanel.h>
#include <IApplicationParameterPanel.h>
#include <ProjectManagerInteractorExport.h>
#include <PluginAppInfo.h>
#include <ProcessorInfo.h>

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API ApplicationPresenter : public UseCase::IUpdateApplicationPort {
    public:
        ApplicationPresenter(IApplicationPanel* applicationPanel = nullptr, IApplicationParameterPanel* parameterPanel = nullptr);
        virtual ~ApplicationPresenter();

        auto Update(const PluginAppInfo::List& pluginInfo, const ProcessorInfo::List& procInfo) -> void override;
        auto Update(const ProcessorInfo::List& procList,const PluginAppInfo::List& parentPlugin)->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}