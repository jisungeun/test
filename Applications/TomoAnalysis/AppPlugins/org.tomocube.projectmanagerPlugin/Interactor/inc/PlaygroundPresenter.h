#pragma once

#include <memory>
#include <IUpdatePlaygroundPort.h>

#include "IProjectExplorerPanel.h"
#include "IOpSequencePanel.h"
#include "IPGNameListPanel.h"
#include "IApplicationParameterPanel.h"

#include "IPreviewPanel.h"
#include "IPlaygroundPanel.h"

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API PlaygroundPresenter : public UseCase::IUpdatePlaygroundPort {
    public:
        PlaygroundPresenter(IProjectExplorerPanel* explorerPanel = nullptr,
                            IOpSequencePanel* sequencePanel = nullptr,
                            IPlaygroundPanel* playgroundPanel = nullptr,
                            IPGNameListPanel* pgNameListPanel = nullptr,
            IPreviewPanel* previewPanel = nullptr,
            IApplicationParameterPanel* parameterPanel = nullptr);
        virtual ~PlaygroundPresenter();

        auto Refresh(const PlaygroundInfo::Pointer& info) -> void override;
        auto Update(const ProjectInfo::Pointer& info)->void override;
        auto UpdateLink(const ProjectInfo::Pointer& info)->void override;
        auto ChangeCurrent(const ProjectInfo::Pointer& info)->void override;
        auto AddItem(const Cube::Pointer& cube, const HyperCube::Pointer& hypercube) -> void override;
        auto AddItem(const Cube::Pointer& cube,
                     const HyperCube::Pointer& hypercube,
                     const TCFDir::List& tcfDirList,
                     const PluginAppInfo::List& appList)->void override;
        auto ModifyItem(const Cube::Pointer& cube, 
            const HyperCube::Pointer& hypercube, 
            const TCFDir::List& tcfDirList, 
            const PluginAppInfo::List& appList) -> void override;
        auto LinkTCFDirToProject(const ProjectInfo::Pointer& project,
                                 const TCFDir::Pointer& tcfDir)->void override;

    private:
        auto ScanSubdir(const TCFDir::List& subDirs)->PlaygroundInfoDS::TCFDirDS;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}