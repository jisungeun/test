#pragma once

#include <memory>
#include <QString>
#include <QList>
#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API ApplicationDS {
		struct PluginDS {
			QString path;
			QString name;
			bool loaded = false;
		};
		struct ProcessorDS {
			QString path;
			QString name;
			QString parent_plugin;
			bool loaded = false;
		};

		QList<PluginDS> pluginList;
		QList<ProcessorDS> processorList;

		typedef std::shared_ptr<ApplicationDS> Pointer;
	};

	class ProjectManagerInteractor_API IApplicationPanel {
	public:
		IApplicationPanel();
		virtual ~IApplicationPanel();

		auto GetDS() const->ApplicationDS::Pointer;

		virtual auto Update(ApplicationDS ds)->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}