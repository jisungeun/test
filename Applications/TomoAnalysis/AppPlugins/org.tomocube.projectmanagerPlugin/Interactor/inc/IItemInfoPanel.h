#pragma once

#include <TCFDir.h>
#include <PluginAppInfo.h>
#include <Cube.h>
#include <HyperCube.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API IItemInfoPanel {
    public:
        IItemInfoPanel();
        virtual ~IItemInfoPanel();

        virtual auto Update(const TCFDir::Pointer&)->void = 0;
        virtual auto Update(const Cube::Pointer&)->void = 0;
        virtual auto Update(const HyperCube::Pointer&)->void = 0;
        virtual auto Update(const PluginAppInfo::Pointer&)->void = 0;
        virtual auto Clear()->void = 0;
    };
}
