#pragma once

#include <memory>

#include <ProjectInfo.h>
#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
	struct ProjectManagerInteractor_API ApplicationParameterDS {
		struct ProcessorDS {
			QString path;
			QString name;
			QString parent_plugin;
			bool loaded = false;
		};

		QString parentName;
		QList<ProcessorDS> processorList;

		typedef std::shared_ptr<ApplicationParameterDS> Pointer;
	};

	class ProjectManagerInteractor_API IApplicationParameterPanel {
	public:
		IApplicationParameterPanel();
		virtual ~IApplicationParameterPanel();

		auto GetDS() const->ApplicationParameterDS::Pointer;

		virtual auto Update(ApplicationParameterDS ds)->bool = 0;
		virtual auto Update(ProjectInfo::Pointer proj)->bool = 0;
		virtual auto Reset()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}