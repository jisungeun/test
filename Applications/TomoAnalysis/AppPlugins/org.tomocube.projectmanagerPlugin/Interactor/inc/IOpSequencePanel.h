#pragma once

#include <memory>
#include <enum.h>

#include <ProjectInfo.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    BETTER_ENUM(SubPanelEnum, int,
        PROJECT_PANEL = 100,
        TCF_ADD_PANEL = 101,
        PLAYGROUND_PANEL = 102,
        HYPERCUBE_PANEL = 103,
        CUBE_PANEL = 104,
        TCF_LINK_PANEL = 105,
        APPLICATION_PANEL = 106
    )

    class ProjectManagerInteractor_API IOpSequencePanel {
    public:
        IOpSequencePanel();
        virtual ~IOpSequencePanel();

        virtual auto Update(const ProjectInfo::Pointer project = nullptr)->bool = 0;
    };
}