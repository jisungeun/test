#pragma once

#include <memory>
#include <IUpdateProjectPort.h>

#include "IProjectExplorerPanel.h"
#include "IOpSequencePanel.h"
#include "IPreviewPanel.h"
#include "IPlaygroundPanel.h"
#include "IPGNameListPanel.h"
#include "IApplicationParameterPanel.h"

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API ProjectPresenter : public UseCase::IUpdateProjectPort {
    public:
        ProjectPresenter(IProjectExplorerPanel* explorerPanel = nullptr, 
                         IOpSequencePanel* sequencePanel = nullptr,
                         IPlaygroundPanel* playgroundPanel = nullptr,
                         IPreviewPanel* previewPanel = nullptr,
                         IPGNameListPanel* pgNameListPanel = nullptr,
                         IApplicationParameterPanel* appParameterPanel = nullptr
            );

        virtual ~ProjectPresenter();

        auto Load(const ProjectInfo::Pointer& info)->void;
        auto Update(const ProjectInfo::Pointer& info)->void;
        auto AddTCF(const ProjectInfo::Pointer& info,
                    const TCFDir::Pointer& dir) -> void;
        auto AddItem(const Cube::Pointer& cube,
                     const HyperCube::Pointer& hypercube,
                     const TCFDir::List& tcfDirList) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}