#pragma once

#include <memory>
#include <ProjectInfo.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    //No DS
    class ProjectManagerInteractor_API IPGNameListPanel {
    public:
        IPGNameListPanel();
        ~IPGNameListPanel();

        virtual auto Update(const ProjectInfo::Pointer& info)->bool = 0;
    };
}