#pragma once

#include <memory>
#include <QString>

#include <IUpdatePlaygroundPort.h>
#include <IProjectDataWritePort.h>
#include <IProjectDataReadPort.h>

#include "ProjectManagerInteractorExport.h"

namespace TomoAnalysis::ProjectManager::Interactor {
    class ProjectManagerInteractor_API PlaygroundController {
    public:
        PlaygroundController(UseCase::IUpdatePlaygroundPort* outPort, UseCase::IProjectDataWritePort* writer,UseCase::IProjectDataReadPort* reader=nullptr);
        virtual ~PlaygroundController();

        auto Create(const QString& name) const ->bool;
        auto SetCurrent(const QString& path) const ->bool;
        auto Copy(const QString& path, const QString& target)const ->bool;
        auto DeleteHistory(const QString& path)const->bool;

        auto Rename(const QString& projectPath, const QString& name, const QString& newName) const ->bool;
        auto Delete(const QString& projectPath, const QString& name)->bool;

        auto RenameCube(const QString& projectPath, const QString& name, const QString& newName)const->bool;
        auto DeleteCube(const QString& projectPath, const QString& name)const->bool;
        auto CopyCube(const QString& projectPath,const QString& name)const->bool;

        auto RenameHyperCube(const QString& projectPath, const QString& name, const QString& newName)const->bool;
        auto DeleteHyperCube(const QString& projectPath, const QString& name)const->bool;
        auto CopyHyperCube(const QString& projectPath,const QString& name)const ->bool;               

        //add Items to playground
        auto AddCube(const QString& playgroundPath, const QString& name,const QString&hyper) const ->bool;
        auto AddCube(const QString& playgroundPath, const QString& name) const ->bool;
        auto AddHypercube(const QString& playgroundPath, const QString& name) const ->bool;

        auto CheckDuplicateHypercube(const QString& playgroundPath, const QString& name) const ->bool;
        auto CheckDuplicateCube(const QString& playgroundPath, const QString& hypercube, const QString& cube) const ->bool;

        auto LinkTCFDirectory(const QString& playgroundPath, const QString& folderPath) const ->bool;
        auto DuplicateTCFDirectory(const QString& playgroundPath, const QString& folderPath) const ->bool;

        auto LinkApplication(const QString& playgroundPath, const QString& appPath,const QString& appName, const QStringList& procList,const QString& hyperName,std::tuple<bool,bool> appType) const -> bool;
        auto LinkApplication(const QString& playgroundPath, const QString& appPath,const QStringList& procList) const -> bool;
        auto DuplicateApplication(const QString& playgroundPath, const QString& appPath) const -> bool;
        auto ShowApplication(const QString& playgroundPath, const QString& appName, const QString& hyperName)const ->bool;
        auto ClearApplication(const QString& playgroundPath)const->bool;

        //update position information
        auto MoveTCFItem(const QString& playgroundPath, const QString& folderPath, const QPoint& pos)->bool;
        auto MoveCubeItem(const QString& playgroundPath, const QString& cubeName, const QPoint& pos)->bool;
        auto MoveHyperItem(const QString& playgroundPath, const QString& hyperName, const QPoint& pos)->bool;
        auto MoveAppItem(const QString& playgroundPath, const QString& appPath, const QPoint& pos)->bool;

        //make relationship between Items
        auto LinkTCFToCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName)->bool;
        auto LinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName)->bool;
        auto LinkAppToCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName)->bool;
        auto LinkAppToHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName)->bool;
        auto LinkTCFsToCube(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName)->bool;
                
        //disconnect relationship among items
        auto UnlinkAppFromHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName)->bool;
        auto UnlinkAppFromCube(const QString& playgroundPath, const QString& appPath, const QString cubeName)->bool;
        auto UnlinkCubeFromHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperName)->bool;
        auto UnlinkTcfFromCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName)->bool;
        auto UnLinkTCFsToCube(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}