#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::ProjectManager::AppUI {
	class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

		auto createProjectfromMain()->void;
		auto openProjectfromMain()->void;		

		auto GetParameter(QString name) -> IParameter::Pointer override;
		auto SetParameter(QString name, IParameter::Pointer param) -> void override;
		
		auto Execute(const QVariantMap& params)->bool override;
		auto GetRunType() -> std::tuple<bool, bool> override;
		auto ForceClosePopUp()->void;
		auto TryActivate() -> bool final;
		auto TryDeactivate() -> bool final;
		auto IsActivate() -> bool final;
		auto GetMetaInfo() -> QVariantMap final;

		auto GetFeatureName() const->QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	protected:
		virtual void resizeEvent(QResizeEvent* event);

	protected slots:
		void onCreateProject(const QString& path) const;
		void onOpenProject(const QString& path) const;		

		void onChangeCurrentPlayground(const QString& path) const;

		void onAddSubProject(const QString& path) const;
		void onAddPlayground(const QString& path) const;
		void onAddCube(const QString& parentPlaygroundPath, const QString& name) const;
		void onAddHypercube(const QString& parentPlaygroundPath, const QString& name) const;
		void onLinkFolderToProject(const QString& parentProjectPath, const QString& path) const;
		void onLinkFolderToPlayground(const QString& playgroundName, const QString& path);
		void onDuplicateTCFToPlayground(const QString& playgroundPath, const QString& folderPath);

		void pjRenameProject(QString path,QString newName);

		//from operation sequence panel
		void sqAddTcf(QString,QString);
		void sqAddCilsTcf(const QMap<int, QStringList>&, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>&);
		void sqLinkTcfToWorkset(QString, QString, QString, int);
		void sqCreateCube(QString,QString,QString);
		void sqCreateHypercube(QString,QString);
		void sqCreatePlayground(QString);
		void sqConnectApplication(QString,QString,QStringList,QString,QString,std::tuple<bool,bool>);
		void sqOpenProject(QString);
		void sqCreateProject(QString);

		//from playground panel
		void pgCubeRename(QString cubeName, QString newName, QString parent);
		void pgCubeDelete(QString cubeName, QString parent);
		void pgCubeCopy(QString cubeName,QString parent);
		void pgHyperRename(QString hyperName,QString newName, QString parent);
		void pgHyperDelete(QString hyperName, QString parent);
		void pgHyperCopy(QString hyperName,QString parent);
		void pgNoSelection();
		void pgAppSelected(QString appName, QString parent);
		void pgItemSelected(QString itemName, QString parent);

		//form previewPanel
		void pvViewer(QStringList tcf_path,QString prj_path);		
		void pvSegmentation(QString tcf_path);
		void pvSegmentation2D(QString tcf_path);
		void pvLink(QStringList tcf_list,QString cube_name,QString playground);
		void pvUnlink(QStringList tcf_list,QString cube_name,QString playground);
		void pvHistory(QString pg_history_path);
		void pvReport(QString pg_report_folder);
		void pvDeleteHistory(QString pg_history_path);
		void pvSimple(QString tcf_path);

		//from applicationPanel
		void apRun(QString appName,QString symbolicName,QString procName,QString tcfFile,QString playgroundPath,QString hyperName);
		void apBatchRun(QString appName, QString symbolicName,QString procName,QString playgroundPath,QString hyperName,QList<QStringList> cubeContents);		
		void apParamWhole(IParameter::Pointer param,QString sender);
		void apInitParam(IParameter::Pointer,QString);
				
	    void onUnlinkAppFromHyperCube(const QString& appPath, const QString& hyperName);
		void onUnlinkAppFromCube(const QString& appPath, const QString& cubeName);
		void onUnlinkCubeFromHyperCube(const QString& cubeName, const QString& hyperName);
		void onUnlinkTcfFromCube(const QString& tcfPath, const QString& cubeName);

		void onLinkAppToPlayground(const QString& appPath,const QStringList& procList);
		void onLoadPlugin(const QString& pluginPath);
		void onLoadProcessor(const QString& processorPath);
		void onCurrentAppChanged(const QString& appPath,const QString& parentName);
		void onExecuteApp(const QString&, const QString&);

		void onPlaygroundNameChanged(QString projectPath, QString name, QString newName) const;
		void onDeletePlaygroundRequested(QString projectPath, QString name);
		void onCurrentPlaygroundChanged(QString projectPath, QString name);

		void onRemoveTCF(const QString& projectPath, const QString& dirPath);

		void OnCtkEvent(const ctkEvent& ctkEvent);

	private:
		auto InitUI(void)->bool;
		auto InitApp(void)->bool;
		auto OnTabFocused(const ctkEvent& ctkEvent)->void;
		auto OnMenuAction(const ctkEvent& ctkEvent)->void;

		auto copyAndReplaceFolderContents(const QString& fromDir, const QString& toDir)->void;

		auto BroadcastViewerApp(QStringList, QString)->void;
		auto BroadcastSegmentationApp(QString)->void;
		auto BroadcastSingleRun(QString, QString, QString, QString, QString, QString)->void;
		auto BroadcastBatchRun(QString, QString, QString, QString, QString, QList< QStringList>)->void;
		auto BroadcastSetParam(QString, QString, QString, QString, QString, QString)->void;
		auto BroadcastCopyParam(IParameter::Pointer, QString)->void;
		auto BroadcastReport(QString, QString)->void;
		auto BroadcastCloseApp(QString)->void;
	
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
