#pragma once

#include <memory>
#include <QWidget>
#include <QListWidgetItem>

#include <IPGNameListPanel.h>

#include "PGNameListPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class PGNameListPanel_API PGNameListPanel : public QWidget, public Interactor::IPGNameListPanel {
        Q_OBJECT
    public:
        typedef PGNameListPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        PGNameListPanel(QWidget* parent = nullptr);
        ~PGNameListPanel();

        auto Update(const ProjectInfo::Pointer& info)->bool override;

    signals:
        void pgSelectionChanged(QString projectPath, QString name);
        void playgroundNameChanged(QString projectPath, QString name, QString newName);
        void deletePlaygroundRequested(QString projectPath, QString name);

    public slots:
        void on_renameButton_clicked();
        void on_deleteButton_clicked();
        void OnCurrentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);

    private:
        auto Init() const ->void;
        auto LoadPlaygroundList(const PlaygroundInfo::List& list) const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    };
}