#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class DeleteDialog : public QDialog {
        Q_OBJECT

    public:
        explicit DeleteDialog(QWidget* parent, const QString& name);
        virtual ~DeleteDialog();

    public:
        static auto Delete(QWidget* parent, const QString& name)->bool;

    protected slots:
        void on_deleteButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
