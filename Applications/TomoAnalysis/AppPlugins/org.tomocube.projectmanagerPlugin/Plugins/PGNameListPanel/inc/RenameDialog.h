#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class RenameDialog : public QDialog {
        Q_OBJECT

    public:
        explicit RenameDialog(QWidget* parent, const QString& name);
        virtual ~RenameDialog();

    public:
        static auto Rename(QWidget* parent, const QString& name)->QString;

        auto GetNewName(void) const ->QString;

    protected slots:
        void on_okButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
