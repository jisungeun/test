#include <QMessageBox>
#include <QRegExpValidator>

#include "ui_RenameDialog.h"
#include "RenameDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct RenameDialog::Impl {
        Ui::RenameDialog* ui{ nullptr };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
    };

    RenameDialog::RenameDialog(QWidget* parent, const QString& name)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::RenameDialog();
        d->ui->setupUi(this);

        d->ui->oriLineEdit->setText(name);

        // set object names
        d->ui->titleLabel->setObjectName("label-title-dialog");
        d->ui->oriLineEdit->setObjectName("input-high");
        d->ui->label->setObjectName("h2");
        d->ui->renameLineEdit->setObjectName("input-high");
        d->ui->okButton->setObjectName("bt-square-primary");
        d->ui->cancelButton->setObjectName("bt-square-line");

        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->renameLineEdit->setValidator(new QRegExpValidator(re));
    }

    RenameDialog::~RenameDialog() = default;

    auto RenameDialog::Rename(QWidget* parent, const QString& name)->QString {
        RenameDialog dialog(parent, name);

        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        const auto newName = dialog.GetNewName();

        return newName;
    }

    auto RenameDialog::GetNewName(void) const ->QString {
        auto result = d->ui->renameLineEdit->text();
        result = d->rstrip(result);
        return d->ui->renameLineEdit->text();
    }

    void RenameDialog::on_okButton_clicked() {
        if (true == d->ui->renameLineEdit->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a new playground name", "Empty or invalid playground name");
            return;
        }

        this->accept();
    }

    void RenameDialog::on_cancelButton_clicked() {
        this->reject();
    }
}
