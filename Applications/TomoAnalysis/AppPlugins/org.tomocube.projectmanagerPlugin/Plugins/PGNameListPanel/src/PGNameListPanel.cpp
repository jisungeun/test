#include <iostream>

#include "RenameDialog.h"
#include "DeleteDialog.h"

#include "ui_PGNameListPanel.h"
#include "PGNameListPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct PGNameListPanel::Impl {
        Ui::PGNameListPanel* ui{ nullptr };
        QString projectPath;
    };

    PGNameListPanel::PGNameListPanel(QWidget* parent) : QWidget(parent), d{ new Impl }{
        d->ui = new Ui::PGNameListPanel;
        d->ui->setupUi(this);

        this->Init();
    }

    PGNameListPanel::~PGNameListPanel() {
        delete d->ui;
    }

    auto PGNameListPanel::Update(const ProjectInfo::Pointer& info)->bool {
        d->projectPath = info->GetPath();        

        d->ui->playgroundListWidget->clear();

        if(false == this->LoadPlaygroundList(info->GetPlaygroundInfoList())) {
            return false;
        }

        auto pgInfo = info->GetCurrentPlayground();

        auto isSelection = false;
        if (nullptr != pgInfo) {
            for (auto i = 0; i < d->ui->playgroundListWidget->count(); i++) {
                auto item = d->ui->playgroundListWidget->item(i);
                if (item->text() == info->GetCurrentPlayground()->GetName()) {
                    item->setSelected(true);
                    d->ui->playgroundListWidget->setCurrentItem(item);
                    isSelection = true;
                    break;
                }
            }
        }

        if (false == isSelection) {
            const auto lastItem = d->ui->playgroundListWidget->item(d->ui->playgroundListWidget->count() - 1);
            if (nullptr != lastItem) {
                lastItem->setSelected(true);
                d->ui->playgroundListWidget->setCurrentItem(lastItem);
            }
        }

        return true;
    }

    void PGNameListPanel::on_renameButton_clicked() {
        const auto currentItem = d->ui->playgroundListWidget->currentItem();
        if (nullptr == currentItem) {
            return;
        }

        const auto name = currentItem->text();
        if (true == name.isEmpty()) {
            return;
        }

        const auto newName = RenameDialog::Rename(this, name);
        if (true == newName.isEmpty()) {
            return;
        }

        emit playgroundNameChanged(d->projectPath, name, newName);
    }        

    void PGNameListPanel::on_deleteButton_clicked() {
        const auto currentItem = d->ui->playgroundListWidget->currentItem();
        if (nullptr == currentItem) {
            return;
        }

        const auto name = currentItem->text();
        if (true == name.isEmpty()) {
            return;
        }

        if (true == DeleteDialog::Delete(this, name)) {
            emit deletePlaygroundRequested(d->projectPath, name);
        }
    }

    void PGNameListPanel::OnCurrentItemChanged(QListWidgetItem* current, QListWidgetItem* previous) {
        Q_UNUSED(previous);
        if(nullptr == current) {
            return;
        }

        const auto name = current->text();
        emit pgSelectionChanged(d->projectPath, name);
    }


    auto PGNameListPanel::Init() const ->void {
        d->ui->playgroundListWidget->clear();

        connect(d->ui->playgroundListWidget, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(OnCurrentItemChanged(QListWidgetItem*, QListWidgetItem*)));

        // set object names
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->settingButton->setObjectName("bt-tool-setting");
        d->ui->playgroundListWidget->setObjectName("panel-contents");
        d->ui->renameButton->setObjectName("bt-square-gray");
        d->ui->deleteButton->setObjectName("bt-square-line");

        d->ui->renameButton->setIconSize(QSize(16, 16));
        d->ui->renameButton->AddIcon(":/img/ic-edit.svg");
        d->ui->renameButton->AddIcon(":/img/ic-edit-s.svg", QIcon::Selected);
        d->ui->renameButton->AddIcon(":/img/ic-edit-s.svg", QIcon::Active);
        d->ui->renameButton->AddIcon(":/img/ic-edit-d.svg", QIcon::Disabled);

        d->ui->deleteButton->setIconSize(QSize(16, 16));
        d->ui->deleteButton->AddIcon(":/img/ic-del.svg");
        d->ui->deleteButton->AddIcon(":/img/ic-del-s.svg", QIcon::Selected);
        d->ui->deleteButton->AddIcon(":/img/ic-del-s.svg", QIcon::Active);
        d->ui->deleteButton->AddIcon(":/img/ic-del-d.svg", QIcon::Disabled);
    }

    auto PGNameListPanel::LoadPlaygroundList(const PlaygroundInfo::List& list) const ->bool {
        for (const auto& info : list) {
            if (nullptr == info) {
                continue;
            }

            auto item = new QListWidgetItem;
            item->setText(info->GetName());
            item->setToolTip(info->GetName());
            item->setIcon(QIcon(":/img/ic-explorer-playground.svg"));

            d->ui->playgroundListWidget->addItem(item);
        }

        return true;
    }
}