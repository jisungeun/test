#include "ui_DeleteDialog.h"
#include "DeleteDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct DeleteDialog::Impl {
        Ui::DeleteDialog* ui{ nullptr };
    };

    DeleteDialog::DeleteDialog(QWidget* parent, const QString& name)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::DeleteDialog();
        d->ui->setupUi(this);

        d->ui->nameLineEdit->setText(name);

        // set object names
        d->ui->titleLabel->setObjectName("label-title-dialog");
        d->ui->nameLineEdit->setObjectName("input-high");
        d->ui->deleteButton->setObjectName("bt-square-primary");
        d->ui->cancelButton->setObjectName("bt-square-line");
    }

    DeleteDialog::~DeleteDialog() = default;

    auto DeleteDialog::Delete(QWidget* parent, const QString& name)->bool {
        DeleteDialog dialog(parent, name);

        if(dialog.exec() != QDialog::Accepted) {
            return false ;
        }

        return true;
    }

    void DeleteDialog::on_deleteButton_clicked() {
        this->accept();
    }

    void DeleteDialog::on_cancelButton_clicked() {
        this->reject();
    }
}