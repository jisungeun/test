#include <iostream>

#include <QStyle>

#include <TCFMetaReader.h>
#include <TCFRawDataReader.h>

#include "ui_NamedImageWidget.h"
#include "NamedImageWidget.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct NamedImageWidget::Impl {
        QWidget* parent = nullptr;

        Ui::NamedImageWidget* ui = nullptr;

        TC::IO::TCFMetaReader* metaReader =  nullptr;
        TC::IO::TCFRawDataReader* dataReader = nullptr;

        TC::IO::TCFMetaReader::Meta::Pointer meta;

        bool selected = false;
    };

    NamedImageWidget::NamedImageWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->parent = parent;
        
        d->ui = new Ui::NamedImageWidget;
        d->ui->setupUi(this);

        d->ui->widget->KeepSquare(false);

        d->ui->textEdit->setAttribute(Qt::WA_TransparentForMouseEvents);
        d->ui->textEdit->setFocusPolicy(Qt::NoFocus);

        // set object names
        d->ui->innerWidget->setObjectName("panel-contents-image");

        // initialize default properties
        d->ui->innerWidget->setProperty("selected", false);
        d->ui->innerWidget->style()->unpolish(d->ui->innerWidget);
        d->ui->innerWidget->style()->polish(d->ui->innerWidget);
    }

    NamedImageWidget::~NamedImageWidget() = default;

    bool NamedImageWidget::SetFile(const QString& path) const {
        // set image
        if(false == d->ui->widget->SetTCFPath(path)) {
            return false;
        }

        if(false == d->ui->widget->ShowImage(TC::TCF2DWidget::ImageType::HT)) {
            return false;
        }

        // set simple name
        auto GetSimpleName = [=](const QString& name) {
            auto split = name.split("/");
            auto simple = split[split.size() - 1];

            if (20 > simple.size()) {
                return simple;
            }

            const auto list = simple.split(".");
            if (3 > list.size()) {
                return simple;
            }

            simple = simple.remove(0, 20);
            return simple.chopped(4);
        };

        d->ui->textEdit->setText(GetSimpleName(path));

        d->ui->widget->SetVisible(true);

        return true;
    }

    auto NamedImageWidget::GetTCFPath() const ->QString {
        return d->ui->widget->GetTCFPath();
    }

    bool NamedImageWidget::Selected() const {
        return d->selected;
    }

    void NamedImageWidget::SetSelected(bool selected) {
        d->selected = selected;

        if (true == selected) {
            d->ui->innerWidget->setProperty("selected", true);
        }
        else {
            d->ui->innerWidget->setProperty("selected", false);
        }

        d->ui->innerWidget->style()->unpolish(d->ui->innerWidget);
        d->ui->innerWidget->style()->polish(d->ui->innerWidget);
    }

    void NamedImageWidget::SetHeight(const int& h) {
        if(0 >= h) {
            return;
        }

        auto length = h - 12;
        if(0 >= length) {
            length = 1;
        }

        d->ui->widget->setFixedHeight(length);
        this->setFixedHeight(h);
    }
}
