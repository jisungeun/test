#include <ToastMessageBox.h>

#include "NamedImageWidgetGroupBox.h"
#include "NamedImageWidget.h"

#include "ui_SingleRunPanel.h"
#include "SingleRunPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct SingleRunPanel::Impl {
        Ui::SingleRunPanel* ui{ nullptr };

        QList<NamedImageWidgetGroupBox*> children;
        NamedImageWidget* selectedWidget = nullptr;
        bool isTime{false};

        uint64_t arrBound{ INT_MAX };
    };

    SingleRunPanel::SingleRunPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
        d->ui = new Ui::SingleRunPanel;
        d->ui->setupUi(this);

        this->setWindowTitle(tr("Single run"));
        d->ui->hypercubeIconLabel->setScaledContents(true);

        // set object names
        setObjectName("widget-popup");
        d->ui->hypercubeNameLabel->setObjectName("h7");
        d->ui->singleRunButton->setObjectName("bt-square-primary");
        d->ui->CancelButton->setObjectName("bt-square-gray");

        d->ui->singleRunButton->setIconSize(QSize(16, 16));
        d->ui->singleRunButton->AddIcon(":/img/ic-run-single.svg");
        d->ui->singleRunButton->AddIcon(":/img/ic-run-single-d.svg", QIcon::Disabled);
    }

    SingleRunPanel::~SingleRunPanel() {
        
    }
    auto SingleRunPanel::SetForceTime(const bool& isTime) -> void {
        d->isTime = isTime;
    }
    auto SingleRunPanel::SetBound(const uint64_t& bound) -> void {
        d->arrBound = bound;
    }
    auto SingleRunPanel::SetHypercube(const HyperCube::Pointer& hypercube)->bool {
        if(nullptr == hypercube) {
            return false;
        }

        d->ui->hypercubeIconLabel->setPixmap(QPixmap(QString::fromUtf8(":/img/playground-hypercube-link.svg")));
        d->ui->hypercubeNameLabel->setText(hypercube->GetName());

        auto widget = new QWidget;
        widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        auto layout = new QVBoxLayout;
        widget->setLayout(layout);
        d->ui->scrollArea->setWidget(widget);

        for(const auto& cube : hypercube->GetCubeList()) {
            const auto group = new NamedImageWidgetGroupBox(this);
            group->SetForceTime(d->isTime);
            group->SetBound(d->arrBound);
            if(false == group->SetCube(cube)) {
                continue;
            }            
            layout->addWidget(group);
            d->children.append(group);
        }

        layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

        return true;
    }
    void SingleRunPanel::on_CancelButton_clicked() {
        this->close();
    }
    void SingleRunPanel::on_singleRunButton_clicked() {
        if(nullptr == d->selectedWidget) {
            auto toastMessageBox = new TC::ToastMessageBox;
            toastMessageBox->Show(tr("There is no selected tcf."));
            return;
        }

        emit singleRun(d->selectedWidget->GetTCFPath());
    }

    void SingleRunPanel::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
        for(auto child : d->children) {
            child->UpdateScreen();
        }
    }

    bool SingleRunPanel::eventFilter(QObject* obj, QEvent* event) {
        if (event->type() == QEvent::MouseButtonDblClick) {
            const auto selectedWidget = dynamic_cast<NamedImageWidget*>(obj);
            if (nullptr == selectedWidget) {
                return true;
            }

            emit singleRun(selectedWidget->GetTCFPath());
            return true;
        }
        else if (event->type() == QEvent::MouseButtonPress) {
            auto selectedWidget = dynamic_cast<NamedImageWidget*>(obj);
            if (selectedWidget == d->selectedWidget) {
                selectedWidget->SetSelected(false);
                d->selectedWidget = nullptr;
                return true;
            }

            if (nullptr != d->selectedWidget) {
                d->selectedWidget->SetSelected(false);
            }

            d->selectedWidget = dynamic_cast<NamedImageWidget*>(obj);
            d->selectedWidget->SetSelected(true);
        }

        return false;
    }
}
    