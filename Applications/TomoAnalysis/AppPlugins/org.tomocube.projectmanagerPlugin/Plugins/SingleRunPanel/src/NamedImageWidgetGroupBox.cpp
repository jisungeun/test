#include <TCFMetaReader.h>
#include "NamedImageWidget.h"

#include "ui_NamedImageWidgetGroupBox.h"
#include "NamedImageWidgetGroupBox.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct NamedImageWidgetGroupBox::Impl {
        Ui::NamedImageWidgetGroupBox* ui = nullptr;

        QWidget* parent = nullptr;
        QList<NamedImageWidget*> children;
        bool isTime{false};                

        const int space = 6;
        const int count = 6;

        uint64_t bound{ INT_MAX };
    };

    NamedImageWidgetGroupBox::NamedImageWidgetGroupBox(QWidget* parent)
        : QWidget(parent)
        , d(new Impl()) {
        d->parent = parent;

        d->ui = new Ui::NamedImageWidgetGroupBox;
        d->ui->setupUi(this);

        d->ui->cubeIconLabel->setScaledContents(true);

        // set object names
        d->ui->cubeNameLabel->setObjectName("h7");
    }

    NamedImageWidgetGroupBox::~NamedImageWidgetGroupBox() {

    }
    auto NamedImageWidgetGroupBox::SetBound(const uint64_t& bound) -> void {
        d->bound = bound;
    }
    auto NamedImageWidgetGroupBox::SetForceTime(const bool& isTime) -> void {
        d->isTime = isTime;
    }
    auto NamedImageWidgetGroupBox::SetCube(const Cube::Pointer& cube) ->bool {
        if(nullptr == cube) {
            return false;
        }

        d->ui->cubeIconLabel->setPixmap(QPixmap(QString::fromUtf8(":/img/playground-cube.svg")));
        d->ui->cubeNameLabel->setText(cube->GetName());

        auto layout = new QGridLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(d->space);
        d->ui->frame->setLayout(layout);

        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();

        TCFDir::List real_list;

        for(auto tcf: cube->GetTCFDirList()) {
            const auto meta = metaReader->Read(tcf->GetPath());
            if(d->isTime) {
                auto count = meta->data.data3D.dataCount;
                if(count > 1) {
                    auto arrSize = static_cast<uint64_t>(meta->data.data3D.sizeX) * static_cast<uint64_t>(meta->data.data3D.sizeY) * static_cast<uint64_t>(meta->data.data3D.sizeZ);
                    //if(arrSize <= d->bound) {
                    if(true){
                        real_list.append(tcf);
                    }
                }
            }else {
                auto arrSize = static_cast<uint64_t>(meta->data.data3D.sizeX) * static_cast<uint64_t>(meta->data.data3D.sizeY) * static_cast<uint64_t>(meta->data.data3D.sizeZ);
                //if (arrSize <= d->bound) {
                if(true){
                    real_list.append(tcf);
                }
            }
        }
        /*
        if(!d->isTime) {
            real_list = cube->GetTCFDirList();
        }else {
            for(auto tcf : cube->GetTCFDirList()) {
                auto count = metaReader->Read(tcf->GetPath())->data.data3D.dataCount;
                if(count >1){
                    real_list.append(tcf);
                }
            }
        }*/

        //for(auto i = 0;  i < cube->GetTCFDirList().size(); ++i) {
        for(auto i=0;i<real_list.size();++i){
            const auto& dir = real_list.at(i);//cube->GetTCFDirList().at(i);
            if (nullptr == dir) {
                continue;
            }
                        
            auto filePath = dir->GetPath();            

            /*
            if(d->isTime) {
                auto count = metaReader->Read(filePath)->data.data3D.dataCount;
                if(count < 2) {
                    continue;
                }
            }*/

            auto widget = new NamedImageWidget(this);            

            if (false == widget->SetFile(dir->GetPath())) {
                continue;
            }

            widget->installEventFilter(d->parent);

            layout->addWidget(widget, i / d->count, i % d->count);
            layout->setRowStretch(i / d->count, 1);
            layout->setColumnStretch(i % d->count, 1);

            d->children.append(widget);        
        }

        //if (0 != cube->GetTCFDirList().size() % d->count) {
        if(0!= real_list.size() % d->count){
            //for (auto i = cube->GetTCFDirList().size() % d->count; i < d->count; ++i) {
            for (auto i = real_list.size() % d->count; i < d->count; ++i) {
                const auto widget = new NamedImageWidget;
                //layout->addWidget(widget, cube->GetTCFDirList().size() / d->count, i % d->count);
                layout->addWidget(widget, real_list.size() / d->count, i % d->count);
                //layout->setRowStretch(cube->GetTCFDirList().size() / d->count, 1);
                layout->setRowStretch(real_list.size() / d->count, 1);
                layout->setColumnStretch(i % d->count, 1);
            }
        }

        return true;
    }

    void NamedImageWidgetGroupBox::UpdateScreen() {
        const auto spacing = (d->count - 1) * d->space; 
        const auto length = (d->ui->frame->width() - spacing) / d->count / 2;

        for(auto child : d->children) {
            if(nullptr == child) {
                continue;
            }

            child->SetHeight(length);
        }

        auto rowCount = 1;
        if(d->children.size() > d->count) {
            rowCount = ceil((double)d->children.size() / (double)d->count);
        }
        
        const auto height = (length * rowCount) + ((rowCount + 1)* d->count) + d->ui->cubeIconLabel->height();
        this->setFixedHeight(height);
    }

}
