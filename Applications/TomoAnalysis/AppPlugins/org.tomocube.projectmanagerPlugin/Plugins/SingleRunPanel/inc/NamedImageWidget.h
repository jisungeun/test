#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class NamedImageWidget : public QWidget{
        Q_OBJECT

    public:
        typedef NamedImageWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        NamedImageWidget(QWidget* parent=nullptr);
        ~NamedImageWidget();

        bool SetFile(const QString& path) const;
        auto GetTCFPath() const ->QString;

        bool Selected() const;
        void SetSelected(bool selected);

        void SetHeight(const int& h);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}