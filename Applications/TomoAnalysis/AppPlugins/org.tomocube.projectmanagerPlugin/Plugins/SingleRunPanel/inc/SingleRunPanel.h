#pragma once

#include <memory>
#include <QWidget>

#include "HyperCube.h"

#include "SingleRunPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class SingleRunPanel_API SingleRunPanel : public QWidget {
        Q_OBJECT

    public:
        typedef SingleRunPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        SingleRunPanel(QWidget* parent = nullptr);
        ~SingleRunPanel();

        auto SetHypercube(const HyperCube::Pointer& hypercube)->bool;
        auto SetForceTime(const bool& isTime)->void;
        auto SetBound(const uint64_t& bound)->void;

    signals:
        void singleRun(QString);

    protected slots:
        void on_singleRunButton_clicked();
        void on_CancelButton_clicked();

    protected:
        void resizeEvent(QResizeEvent* event) override;
        bool eventFilter(QObject* obj, QEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}