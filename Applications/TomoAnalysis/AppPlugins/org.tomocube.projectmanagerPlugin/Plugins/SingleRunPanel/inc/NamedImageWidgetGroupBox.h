#pragma once

#include <memory>
#include <QWidget>

#include <Cube.h>

namespace TomoAnalysis::ProjectManager::Plugins {
    class NamedImageWidgetGroupBox : public QWidget {
        Q_OBJECT

    public:
        typedef NamedImageWidgetGroupBox Self;
        typedef std::shared_ptr<Self> Pointer;

        NamedImageWidgetGroupBox(QWidget* parent = nullptr);
        ~NamedImageWidgetGroupBox();

        auto SetBound(const uint64_t& bound)->void;
        auto SetCube(const Cube::Pointer& cube) ->bool;
        auto SetForceTime(const bool& isTime)->void;        
        void UpdateScreen();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}