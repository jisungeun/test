#pragma once

#include <memory>
#include <QWidget>
#include <IApplicationParameterPanel.h>
#include <IParameter.h>

#include "ApplicationParameterPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class ApplicationParameterPanel_API ApplicationParameterPanel : public QWidget, public Interactor::IApplicationParameterPanel {
        Q_OBJECT
        
    public:
        typedef ApplicationParameterPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ApplicationParameterPanel(QWidget* parent=nullptr);
        ~ApplicationParameterPanel();

        auto Update(Interactor::ApplicationParameterDS ds) -> bool override;
        auto Update(ProjectInfo::Pointer proj) -> bool override;

        auto Reset() -> bool override;

        //non-clean architecture
        auto GetParameter(QString algorithm_name, QString subName = QString()) ->IParameter::Pointer;
        auto GetParameter()->IParameter::Pointer;
        auto SetParameter(IParameter::Pointer param)->void;
        auto InitParameter(IParameter::Pointer param,QString proc_name)->void;
        auto SetApplicationName(QString name)->void;
        auto GetApplicationName()->QString;                
        auto SetCurrentProcessor(QString procName,bool force = false)->bool;
        auto SetHideButtons(bool hide)->void;

    signals:
        void singleRun(QString,QString,QString,QString,QString,QString);        
        void batchRun(QString,QString,QString,QString,QString,QList<QStringList>);        
        void sigParameter(IParameter::Pointer,QString);
        void sigInitParam(IParameter::Pointer,QString);

    protected slots:
        void OnCurrentProcChanged(int idx);
        void OnSingleRunButtonClicked();
        void OnSingleRun(QString);
        void OnBatchRun();
        void OnParamValChanged(double, QString);
        void OnParamPathChanged(QString, QString);
        
    private:
        auto Init()->void;
        auto ConvertSymbolicName(const QString& dllName)const->QString;
        auto ParseParamNSet(bool sendSignal = true)->void;
        auto SetProcessorParamVisible(bool visible)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}