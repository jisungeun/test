#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class SingleRun: public QDialog {
        Q_OBJECT
    public:
        explicit SingleRun(QStringList tcfList, QWidget* parent = nullptr);
        virtual ~SingleRun();

        auto GetCurrentTCF()const->QString;

    public:
        static auto GetTCFName(QStringList tcfList, QWidget* parent = nullptr)->QString;

    protected slots:
        void OnRunBtn();
        void OnCancelBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}