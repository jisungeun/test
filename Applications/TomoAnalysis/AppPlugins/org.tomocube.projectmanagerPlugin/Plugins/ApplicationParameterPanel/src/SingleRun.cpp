#include "SingleRun.h"

#include "ui_SingleRun.h"

#include <QMessageBox>
#include <QListView>

namespace TomoAnalysis::ProjectManager::Plugins {
    struct SingleRun::Impl {
        Ui::SingleRun* ui{ nullptr };
    };
    SingleRun::SingleRun(QStringList tcfList,QWidget* parent)
        : QDialog(parent), d{ new Impl }{
        d->ui = new Ui::SingleRun;
        d->ui->setupUi(this);

        for(const auto &tcf : tcfList) {
            d->ui->tcfList->addItem(tcf);
        }
        Init();
    }

    SingleRun::~SingleRun() = default;

    auto SingleRun::GetCurrentTCF() const -> QString {
        return d->ui->tcfList->currentText();
    }
    void SingleRun::OnCancelBtn() {
        reject();
    }
    void SingleRun::OnRunBtn() {
        if(d->ui->tcfList->count()<1) {
            QMessageBox::warning(nullptr, "No TCF", "Add TCF first");
            return;
        }
        accept();
    }
    auto SingleRun::GetTCFName(QStringList tcfList, QWidget* parent) -> QString {
        SingleRun dialog(tcfList, parent);
        if(dialog.exec()!= QDialog::Accepted) {
            return QString();
        }
        auto tcfName = dialog.GetCurrentTCF();

        return tcfName;
    }
    auto SingleRun::Init() -> void {
        d->ui->tcfList->setView(new QListView);

        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
        connect(d->ui->runBtn, SIGNAL(clicked()), this, SLOT(OnRunBtn()));

        // set object names
        d->ui->label->setObjectName("label-title-dialog");
        d->ui->label_2->setObjectName("h2");
        d->ui->tcfList->setObjectName("dropdown-high");
        d->ui->runBtn->setObjectName("bt-square-primary");
        d->ui->cancelBtn->setObjectName("bt-square-line");
    }

}