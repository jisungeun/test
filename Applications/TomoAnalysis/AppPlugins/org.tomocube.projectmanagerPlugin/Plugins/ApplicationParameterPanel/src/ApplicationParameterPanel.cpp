#define LOGGER_TAG "[Project Manager]"
#include <TCLogger.h>

#include <iostream>
#include <QFileInfo>
#include <QDirIterator>
#include <QListView>

#include <IProcessor.h>
#include <PluginRegistry.h>
#include <ProcControl.h>
#include <UIUtility.h>
#include <TCFMetaReader.h>

#include "SingleRunPanel.h"

#include "ui_ApplicationParameterPanel.h"
#include "ApplicationParameterPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct ApplicationParameterPanel::Impl {
        Ui::ApplicationParameterPanel* ui{ nullptr };
        TC::ProcControl* paramControl;
        QStringList procPath;
        QStringList fullName;
        ProjectInfo::Pointer cur_proj = nullptr;
                
        QString appName;
        QString appSymbolicName;
        QString pg_path;
        QString hyperName;
        QStringList cur_tcfList;
        QList<QSpacerItem*> spacer;

        int cur_proc_idx{0};
    };

    ApplicationParameterPanel::ApplicationParameterPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IApplicationParameterPanel()
    , d{ new Impl } {
        d->ui = new Ui::ApplicationParameterPanel();
        d->ui->setupUi(this);
        this->Init();
    }

    ApplicationParameterPanel::~ApplicationParameterPanel() {
        delete d->paramControl;        
        delete d->ui;
    }
    auto ApplicationParameterPanel::SetHideButtons(bool hide) -> void {
        if(hide) {
            d->ui->batchBtn->hide();
            d->ui->singleBtn->hide();
            d->paramControl->SetDisableControl(true);
        }else {
            d->ui->batchBtn->show();
            d->ui->singleBtn->show();
            d->paramControl->SetDisableControl(false);
        }        
    }

    auto ApplicationParameterPanel::SetApplicationName(QString name) -> void {
        d->appName = name;
    }
    auto ApplicationParameterPanel::GetApplicationName() -> QString {
        return d->appName;
    }
    auto ApplicationParameterPanel::InitParameter(IParameter::Pointer param,QString proc_name) -> void {
        auto cparam = std::shared_ptr<IParameter>(param->Clone());
        auto processor = std::dynamic_pointer_cast<IProcessor>(PluginRegistry::GetPlugin(proc_name));
        if (nullptr == processor) {
            return;
        }
        auto metaParam = processor->MetaParameter();

        QStringList realname_list{ "membrane","nucleus","nuleoli","lipid droplet" };
        QStringList displayname_list{ "Whole cell","Nucleus","Nucleolus","Lipid droplet" };
        //d->paramControl->setConverter(displayname_list,realname_list);        
        d->paramControl->SetConverter(displayname_list, realname_list);
        //d->paramControl->setProcessingParameter(cparam);
        d->paramControl->SetProcessingParameter(cparam);
        d->paramControl->SetMetaParameter(metaParam);
        d->paramControl->BuildInterface();

        /*QStringList realname_list{"membrane","nucleus","nuleoli","lipid droplet"};
		QStringList displayname_list{"Whole cell","Nucleus","Nucleolus","Lipid droplet"};		
		
		d->paramControl->setConverter(displayname_list,realname_list);
        d->paramControl->setProcessingParameter(cparam);*/

        SetProcessorParamVisible(true);
        d->paramControl->show();
    }

    auto ApplicationParameterPanel::Reset() -> bool {
        d->ui->procCombo->blockSignals(true);
        d->ui->procCombo->clear();
        d->procPath.clear();
        d->fullName.clear();
        d->paramControl->ClearInterface();
        d->ui->appName->clear();
        d->cur_tcfList.clear();
        d->ui->singleBtn->hide();
        d->ui->batchBtn->hide();

        return true;
    }


    auto ApplicationParameterPanel::Update(ProjectInfo::Pointer proj) -> bool {        
        d->ui->procCombo->blockSignals(true);
        d->ui->procCombo->clear();
        d->procPath.clear();
        d->fullName.clear();
        d->paramControl->ClearInterface();
        d->ui->appName->clear();                
        d->cur_tcfList.clear();
        d->ui->singleBtn->hide();
        d->ui->batchBtn->hide();

        if (proj->GetPath().isEmpty()) {
            //clear interface only
            return false;
        }                

        d->cur_proj = proj;
        if(nullptr == proj->GetCurrentPlayground()) {
            return false;
        }
        d->pg_path = proj->GetCurrentPlayground()->GetPath();

        auto curApp = proj->GetCurrentAppInfo();

        if(nullptr == curApp) {
            return true;
        }
        auto isTime = curApp->GetName().contains("Time");

        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();

        auto cur_hyper = proj->GetCurrentHyperCube();
        d->hyperName = cur_hyper->GetName();        
        auto cubeList = cur_hyper->GetCubeList();
        for(const auto& cube:cubeList) {
            auto tcfList = cube->GetTCFDirList();
            for(const auto& tcf:tcfList) {
                if(isTime) {
                    auto metaInfo = metaReader->Read(tcf->GetPath());
                    auto HT_cnt = metaInfo->data.data3D.dataCount;
                    if(HT_cnt < 2) {
                        continue;//ignore single tcf from list
                    }
                }
                d->cur_tcfList.push_back(tcf->GetPath());
            }
        }                
        
        
        QFileInfo info(curApp->GetPath());                        

        QString parsed_Name = info.fileName().chopped(4);
        parsed_Name = parsed_Name.remove(0, 3);
        parsed_Name = parsed_Name.replace("_", ".");        
        d->appSymbolicName = parsed_Name;        
        d->appName = curApp->GetName();        

        d->ui->appName->setText(curApp->GetName());
        
        auto procList = curApp->GetProcessors();
        
        for(const auto& proc : procList) {            
            auto sname = this->ConvertSymbolicName(proc->GetPath());
            //d->ui->procCombo->addItem(proc->GetName().chopped(4));
            d->ui->procCombo->addItem(proc->GetName());
            d->fullName.push_back(sname);
            d->procPath.push_back(proc->GetPath());            
        }

        if (d->ui->procCombo->count() > 0) {
            d->ui->procCombo->setCurrentIndex(0);            
            SetProcessorParamVisible(true);

            this->OnCurrentProcChanged(0);
        }
        else {            
            SetProcessorParamVisible(false);
        }

        d->ui->procCombo->blockSignals(false);

        //show/hide run buttons
        auto appTypes = curApp->GetAppType();
        auto isSingleRun = std::get<0>(appTypes);
        auto isBatchRun = std::get<1>(appTypes);

        if(isSingleRun) {
            d->ui->singleBtn->show();
        }else {
            d->ui->singleBtn->hide();
        }
        if(isBatchRun) {
            d->ui->batchBtn->show();
        }else {
            d->ui->batchBtn->hide();
        }

        //HERE

        
        auto param = d->paramControl->GetParameter();
        if (nullptr == param) {
            QLOG_ERROR() << "No Parameter initialized";
            return false;
        }
        auto procIdx = d->ui->procCombo->currentIndex();
        if (procIdx > -1) {
            emit sigInitParam(param, d->fullName[procIdx]);
        }

        return true;
    }

    auto ApplicationParameterPanel::Update(Interactor::ApplicationParameterDS ds) -> bool {        
        d->ui->procCombo->blockSignals(true);
        d->ui->procCombo->clear();
        d->procPath.clear();
        d->fullName.clear();
        d->paramControl->ClearInterface();
        d->ui->appName->clear();        

        d->ui->appName->setText(ds.parentName);

        auto procList = ds.processorList;
        
        for(const auto& proc: procList ) {            
            auto sname = this->ConvertSymbolicName(proc.path);
            d->ui->procCombo->addItem(proc.name);
            d->fullName.push_back(sname);
            d->procPath.push_back(proc.path);            
        }
        if (d->ui->procCombo->count() > 0) {
            d->ui->procCombo->setCurrentIndex(0);
            SetProcessorParamVisible(true);
            this->OnCurrentProcChanged(0);
        }else {
            SetProcessorParamVisible(false);
        }

        d->ui->procCombo->blockSignals(false);

        
        return true;
    }
    auto ApplicationParameterPanel::SetCurrentProcessor(QString procName,bool force) -> bool {
        if(force) {
            d->ui->procCombo->blockSignals(true);
            d->fullName.clear();
            d->ui->procCombo->clear();
            if(!d->fullName.contains(procName)) {
                d->fullName.push_back(procName);
                d->cur_proc_idx = d->fullName.count() - 1;

                IPluginModule::Pointer processor = PluginRegistry::GetPlugin(procName);
                if (nullptr == processor) {
                    std::cout << "No plugin loaded" << std::endl;
                    return false;
                }                                
                
                d->ui->procCombo->addItem(processor->GetName());                
            }
            d->ui->procCombo->blockSignals(false);
        }
        for(auto i=0;i<d->ui->procCombo->count();i++) {
            if(d->fullName[i].compare(procName)==0) {                
                d->cur_proc_idx = i;
                d->ui->procCombo->setCurrentIndex(i);                
                return true;
            }
        }
        return false;
    }

    void ApplicationParameterPanel::OnCurrentProcChanged(int idx) {        
        auto selectedProcessor = d->fullName[idx];        
        IPluginModule::Pointer processor = PluginRegistry::GetPlugin(selectedProcessor);
        if(nullptr == processor) {            
            if(PluginRegistry::LoadPlugin(d->procPath[idx])==-1) {
                auto appWrite = d->procPath[idx].split("/processor/")[0];
                auto appRemain = d->procPath[idx].split("/processor/")[1];                

                auto realPath = qApp->applicationDirPath() + "/processor/" + appRemain;
                if(PluginRegistry::LoadPlugin(realPath) == -1) {
                    QLOG_DEBUG() << "Failed to load processor dll";
                    return;
                }                                
            }
            processor = PluginRegistry::GetPlugin(selectedProcessor);
        }

        if(nullptr == processor) {
            return;
        }                        
                
        auto param = processor->Parameter();                
        for (auto nnt : param->GetNameAndTypes()) {
            auto name = std::get<0>(nnt);
            auto desp = param->Description(name);
            auto algoName = param->GetValue(name).toString();
            auto algoPath = qApp->applicationDirPath() + "/" + desp;

            auto algorithm = PluginRegistry::GetPlugin(algoName);
            if (nullptr == algorithm) {
                PluginRegistry::LoadPlugin(algoPath);
            }
        }

        auto typed_processor = std::dynamic_pointer_cast<IProcessor>(processor);
        auto metaParam = typed_processor->MetaParameter();


        QStringList realname_list{"membrane","nucleus","nucleoli","lipid droplet"};
		QStringList displayname_list{"Whole cell","Nucleus","Nucleolus","Lipid droplet"};		
		
		d->paramControl->SetConverter(displayname_list,realname_list);
        d->paramControl->SetProcessingParameter(param);
        d->paramControl->SetMetaParameter(metaParam);
        d->paramControl->BuildInterface();
        d->paramControl->show();
    }

    auto ApplicationParameterPanel::Init() -> void {
        //d->spacer = new QSpacerItem(1, 1, QSizePolicy::Preferred, QSizePolicy::Expanding);
        connect(d->ui->procCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnCurrentProcChanged(int)));
        d->ui->procCombo->setView(new QListView);

        d->paramControl = new TC::ProcControl(nullptr);
        d->paramControl->SetHideExecute(true);
        d->paramControl->SetHideParamExecute(true);
               
        d->ui->scrollArea->setWidget(d->paramControl);
        //d->ui->scrollArea->setWidgetResizable(true);
        SetProcessorParamVisible(false);

        //d->ui->appName->setReadOnly(true);

        //connect(d->ui->singleBtn, SIGNAL(clicked()), this, SLOT(OnSingleRun()));
        connect(d->ui->singleBtn, SIGNAL(clicked()), this, SLOT(OnSingleRunButtonClicked()));
        connect(d->ui->batchBtn, SIGNAL(clicked()), this, SLOT(OnBatchRun()));

        // set object names
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->frame->setObjectName("panel-base");
        d->ui->currentAppTitleLabel->setObjectName("h8");
        d->ui->line->setObjectName("line-separator-section");
        d->ui->selectProcessorTitleLabel->setObjectName("h8");
        d->ui->singleBtn->setObjectName("bt-square-gray");
        d->ui->batchBtn->setObjectName("bt-square-primary");

        d->ui->singleBtn->setIconSize(QSize(16, 16));
        d->ui->singleBtn->AddIcon(":/img/ic-run-single.svg");
        d->ui->singleBtn->AddIcon(":/img/ic-run-single-s.svg", QIcon::Selected);
        d->ui->singleBtn->AddIcon(":/img/ic-run-single-s.svg", QIcon::Active);
        d->ui->singleBtn->AddIcon(":/img/ic-run-single-d.svg", QIcon::Disabled);

        d->ui->batchBtn->setIconSize(QSize(16, 16));
        d->ui->batchBtn->AddIcon(":/img/ic-run-batch.svg");
        d->ui->batchBtn->AddIcon(":/img/ic-run-batch-d.svg", QIcon::Disabled);

        {
            QStringList pathList;
            QStringList fileList;
            auto appPath = QString(qApp->applicationDirPath());
            QDir dir(appPath);
            dir.cd("algorithms");
            dir.cd("ai");
            appPath = dir.absolutePath();
            fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
            for (int i = 0; i < fileList.size(); i++) {
                auto fullPath = appPath + "/" + fileList[i];                
                if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다                    
                    QLOG_ERROR() << "Filed to load plugin in " << fullPath;
                }
            }
        }
        {
            QStringList pathList;
            QStringList fileList;
            auto appPath = QString(qApp->applicationDirPath());
            QDir dir(appPath);
            dir.cd("algorithms");
            dir.cd("labeling");
            appPath = dir.absolutePath();
            fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
            for (int i = 0; i < fileList.size(); i++) {
                auto fullPath = appPath + "/" + fileList[i];                
                if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
                    QLOG_ERROR() << "Filed to load plugin in " << fullPath;                    
                }
            }
        }
        {
            QStringList pathList;
            QStringList fileList;
            auto appPath = QString(qApp->applicationDirPath());
            QDir dir(appPath);
            dir.cd("algorithms");
            dir.cd("masking");
            appPath = dir.absolutePath();
            fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
            for (int i = 0; i < fileList.size(); i++) {
                auto fullPath = appPath + "/" + fileList[i];                
                if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
                    QLOG_ERROR() << "Failed to load plugins in " << fullPath;                    
                }
            }
        }
        {
            QStringList pathList;
            QStringList fileList;
            auto appPath = QString(qApp->applicationDirPath());
            QDir dir(appPath);
            dir.cd("algorithms");
            dir.cd("measurement");
            appPath = dir.absolutePath();
            fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
            for (int i = 0; i < fileList.size(); i++) {
                auto fullPath = appPath + "/" + fileList[i];                 
                if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
                    QLOG_ERROR() << "Failed to load plugins in " << fullPath;                    
                }
            }
        }
        {
            QStringList pathList;
            QStringList fileList;
            auto appPath = QString(qApp->applicationDirPath());
            QDir dir(appPath);
            dir.cd("algorithms");
            dir.cd("segmentation");
            appPath = dir.absolutePath();
            fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
            for (int i = 0; i < fileList.size(); i++) {
                auto fullPath = appPath + "/" + fileList[i];                
                if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
                    QLOG_ERROR() << "Failed to load plugins in " << fullPath;                    
                }
            }
        }
        d->ui->singleBtn->hide();
        d->ui->batchBtn->hide();
    }

    void ApplicationParameterPanel::OnParamPathChanged(QString path, QString name) {        
        Q_UNUSED(path)
        Q_UNUSED(name)
    }

    void ApplicationParameterPanel::OnParamValChanged(double val, QString name) {
        Q_UNUSED(val)
        Q_UNUSED(name)
    }

    void ApplicationParameterPanel::OnSingleRunButtonClicked() {
        const auto playground = d->cur_proj->GetCurrentPlayground();
        if (nullptr == playground) {
            return;
        }

        const auto hypercube = playground->FindHypercube(d->hyperName);
        if (nullptr == hypercube) {
            return;
        }

        auto curApp = d->cur_proj->GetCurrentAppInfo();
        auto isTime = false;        
        if(curApp->GetName().contains("[T]")) {
            isTime = true;            
        }

        auto widget = new SingleRunPanel;                

        widget->SetBound(INT_MAX / 3 * 2);
        widget->SetForceTime(isTime);
        widget->SetHypercube(hypercube);
        widget->showMaximized();

        connect(widget, SIGNAL(singleRun(QString)), this, SLOT(OnSingleRun(QString)));
    }

    void ApplicationParameterPanel::OnSingleRun(QString path) {
        auto sender = static_cast<QWidget*>(QObject::sender());
        if(nullptr != sender && sender->isWindow()) {
            sender->close();
            delete sender;
            sender = nullptr;
        }

        const auto procIdx = d->ui->procCombo->currentIndex();
        if(0 > procIdx) {
            return;
        }

        emit singleRun(d->appName, d->appSymbolicName, d->fullName[procIdx], path, d->pg_path, d->hyperName);
    }

    auto ApplicationParameterPanel::ParseParamNSet(bool sendSignal) -> void {
        auto param = d->paramControl->GetParameter();        
        if (nullptr == param) {
            return;
        }
        auto procIdx = d->ui->procCombo->currentIndex();
        if (procIdx > -1 && sendSignal) {
            emit sigParameter(param, d->appSymbolicName + "*" + d->fullName[procIdx]);
        }
    }

    auto ApplicationParameterPanel::SetProcessorParamVisible(bool visible)->void {        
        auto layout = qobject_cast<QVBoxLayout*>(d->ui->contentsWidget->layout()); 
        if (layout == nullptr) {
            return;
        }

        if (visible) {
            /*for (auto i = 0; i < layout->count(); i++) {
                auto spacer = layout->itemAt(i)->spacerItem();
                if (spacer) {                    
                    layout->removeItem(spacer);                    
                    delete spacer;
                    break;
                }
            }*/
            /*if (nullptr != d->spacer) {
                layout->removeItem(d->spacer);
                delete d->spacer;
                d->spacer = nullptr;
            }*/
            for (auto i=0;i<d->spacer.count();i++) {
                auto spacer = d->spacer[i];
                layout->removeItem(spacer);
                delete spacer;
            }
            d->spacer.clear();
            d->ui->processorParamFrame->show();
        } else {            
            auto index = layout->indexOf(d->ui->processorParamFrame);
            auto spacer = new QSpacerItem(1, 1, QSizePolicy::Preferred, QSizePolicy::Expanding);
            //layout->insertSpacerItem(index, new QSpacerItem(1, 1, QSizePolicy::Preferred, QSizePolicy::Expanding));
            layout->insertSpacerItem(index, spacer);
            d->spacer.push_back(spacer);
            d->ui->processorParamFrame->hide();
        }
    }

    void ApplicationParameterPanel::OnBatchRun() {
        if (nullptr == d->cur_proj)
            return;
        auto procIdx = -1;
        QString procName = QString();                
        if (d->ui->procCombo->count() > 0) {
            procIdx = d->ui->procCombo->currentIndex();
            procName = d->fullName[procIdx];
        }                
        ParseParamNSet(false);
        auto playground = d->cur_proj->GetCurrentPlayground();
        if (nullptr == playground) {
            return;
        }        
        auto hypercube = playground->FindHypercube(d->hyperName);
        if(nullptr == hypercube) {
            return;
        }
        auto cubes = hypercube->GetCubeList();
        if(cubes.size()<1) {
            return;
        }

        auto curApp = d->cur_proj->GetCurrentAppInfo();
        auto isTime = curApp->GetName().contains("Time");

        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();

        QList<QStringList> cubeContents;
        for(const auto& cube: cubes) {
            QStringList contents;
            auto cubeName = cube->GetName();
            contents.append(cubeName);
            auto tcfList = cube->GetTCFDirList();
            for(const auto& tcf : tcfList) {
                if(isTime) {
                    auto metaInfo = metaReader->Read(tcf->GetPath());
                    auto HT_cnt = metaInfo->data.data3D.dataCount;
                    if(HT_cnt<2)
                        continue;
                }
                contents.append(tcf->GetPath());
            }
            cubeContents.append(contents);
        }                
        emit batchRun(d->appName,d->appSymbolicName, procName, d->pg_path, d->hyperName,cubeContents);                
    }

    auto ApplicationParameterPanel::GetParameter(QString algorithm_name,QString subName) -> IParameter::Pointer {
        IParameter::Pointer param;
        if (subName.isEmpty()) {
            param = d->paramControl->GetParameter(algorithm_name);
            return param;
        }
        return nullptr;
    }
    auto ApplicationParameterPanel::GetParameter() -> IParameter::Pointer {        
        return d->paramControl->GetParameter();        
    }

    auto ApplicationParameterPanel::SetParameter(IParameter::Pointer param)->void {
        d->paramControl->SetParameterValue(param);        
    }
    auto ApplicationParameterPanel::ConvertSymbolicName(const QString& dllName) const -> QString {                
        auto fileName = QFileInfo(dllName).fileName();
        auto exceptExt = fileName.split(".dll")[0];
        auto replace = exceptExt.replace(QString("TC"), QString("org.tomocube"));
        replace = replace.replace(QString("_"), QString("."));
        replace = replace.toLower();        
        return replace;
    }

}

