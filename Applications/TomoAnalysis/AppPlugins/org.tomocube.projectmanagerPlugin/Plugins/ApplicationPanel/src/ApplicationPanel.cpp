#include <iostream>
#include <QMenu>
#include <QDirIterator>

#include <UIUtility.h>

#include "ui_ApplicationPanel.h"
#include "ApplicationPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct ApplicationPanel::Impl {
        Ui::ApplicationPanel* ui{ nullptr };

        QStringList PluginList;
        QStringList ProcessorList;
    };

    ApplicationPanel::ApplicationPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IApplicationPanel()
    , d{ new Impl } {
        d->ui = new Ui::ApplicationPanel();
        d->ui->setupUi(this);
        this->Init();
        //this->Update();
    }

    ApplicationPanel::~ApplicationPanel() {
        delete d->ui;
    }

    auto ApplicationPanel::Update(Interactor::ApplicationDS ds)->bool {
        auto pluginList = ds.pluginList;
        auto procList = ds.processorList;
        if(pluginList.size()<1) {
            QTreeWidgetItem* procItem;            
            if (procList[0].loaded) {
                procItem = CreateItem(procList[0].name, procList[0].path, ItemFlag::LoadedProcessor);
            }else {
                procItem = CreateItem(procList[0].name, procList[0].path, ItemFlag::Processor);
            }
            UpdateProcessor(procItem);
            return true;
        }
        if(procList.size()<1) {
            QTreeWidgetItem* plugItem;
            if(pluginList[0].loaded) {
                plugItem = CreateItem(pluginList[0].name, pluginList[0].path, ItemFlag::LoadedPlugin);
            }else {
                plugItem = CreateItem(pluginList[0].name, pluginList[0].path, ItemFlag::Plugin);
            }
            UpdatePlugin(plugItem);
            return true;
        }

        d->ui->AppTree->clear();
        d->ui->AppTree->setRootIsDecorated(false);        

        for(const auto& plugin : pluginList) {
            QTreeWidgetItem* pluginItem;
            if (plugin.loaded) {
                pluginItem = CreateItem(plugin.name, plugin.path,ItemFlag::LoadedPlugin);
            }else {
                pluginItem = CreateItem(plugin.name, plugin.path,ItemFlag::Plugin);
            }
            d->ui->AppTree->addTopLevelItem(pluginItem);
        }        

        for(const auto& processor : procList ) {
            QTreeWidgetItem* processorItem;///
            if (processor.loaded) {
                processorItem = CreateItem(processor.name,processor.path, ItemFlag::LoadedProcessor);
            }else {
                processorItem = CreateItem(processor.name,processor.path, ItemFlag::Processor);
            }            
            auto segApp = d->ui->AppTree->findItems(processor.parent_plugin, Qt::MatchContains);
            if (segApp.size() > 0) {
                segApp[0]->addChild(processorItem);
            }
        }

        this->ShowRootIndicator();

        d->ui->AppTree->resizeColumnToContents(0);

        return true;
    }
    auto ApplicationPanel::UpdatePlugin(QTreeWidgetItem* item) -> void {        
        auto plugin = d->ui->AppTree->findItems(item->text(0), Qt::MatchContains);
        if (plugin.size() > 0) {
            auto idx = d->ui->AppTree->indexOfTopLevelItem(plugin[0]);            
            if(plugin[0]->childCount()>0) {
                for(int i=0;i<plugin[0]->childCount();i++) {
                    auto orphan = plugin[0]->child(i);
                    auto adopted = CreateItem(orphan->text(0), orphan->toolTip(0), orphan->type());
                    item->addChild(adopted);
                }
            }
            delete plugin[0];
            d->ui->AppTree->insertTopLevelItem(idx, item);            
        }
    }
    auto ApplicationPanel::UpdateProcessor(QTreeWidgetItem* item) -> void {        
        auto proc = d->ui->AppTree->findItems(item->text(0), Qt::MatchContains | Qt::MatchRecursive);
        if (proc.size() > 0) {
            auto parent = proc[0]->parent();
            auto idx = parent->indexOfChild(proc[0]);
            parent->removeChild(proc[0]);
            parent->insertChild(idx,item);
        }
    }
    auto ApplicationPanel::ShowRootIndicator() const -> void {
        if (nullptr == d->ui->AppTree) {
            return;
        }

        for(auto i =0 ;i< d->ui->AppTree->topLevelItemCount();i++) {
            const auto root = d->ui->AppTree->topLevelItem(i);
            if (nullptr == root) {
                continue;
            }
            if (0 == root->childCount()) {
                continue;
            }
            if (true == d->ui->AppTree->rootIsDecorated()) {
                continue;
            }
            d->ui->AppTree->setRootIsDecorated(true);
        }
        d->ui->AppTree->expandAll();
    }

    void ApplicationPanel::OnShowContextMenu(const QPoint& pos) {
        auto* item = d->ui->AppTree->itemAt(pos);
        if(nullptr==item) {
            return;
        }

        const auto globalPos = d->ui->AppTree->viewport()->mapToGlobal(pos);

        QMenu menu;
        QAction* selectedItem{ nullptr };

        const auto flag = (ItemFlag)item->type();
        if(flag & ItemFlag::Plugin) {
            if (!(flag & ItemFlag::Loaded)) {
                menu.addAction("Load Plugin");
                
            }
            else {//if plugin already loaded
                if (!(flag & ItemFlag::InPlayground)) {
                    menu.addAction("Link to playground");
                }
            }
            selectedItem = menu.exec(globalPos);
        }else if(flag & ItemFlag::Processor) {
            if (!(flag & ItemFlag::Loaded)) {
                menu.addAction("Load Processor");
            }
            selectedItem = menu.exec(globalPos);
        }
        if(nullptr!= selectedItem) {
            const auto text = selectedItem->text();
            if(true == text.contains("Load Processor")) {                
                this->LoadProcessor(item);
            }else if(true == text.contains("Load Plugin")) {                
                this->LoadPlugin (item);
            }else if(true == text.contains("Link to playground")) {
                this->AddPluginToPlayground(item);
            }
        }
    }

    auto ApplicationPanel::Init() -> bool {
        d->ui->AppTree->clear();
        d->ui->AppTree->setColumnCount(2);
        //d->ui->AppTree->hideColumn()
        QStringList emptyLabel = {"",""};        
        d->ui->AppTree->setHeaderLabels(emptyLabel);        
        d->ui->AppTree->setSortingEnabled(true);
        d->ui->AppTree->sortByColumn(1, Qt::SortOrder::AscendingOrder);
        d->ui->AppTree->setContextMenuPolicy(Qt::CustomContextMenu);        

        connect(d->ui->AppTree, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(OnShowContextMenu(const QPoint&)));

        return true;
    }

    auto ApplicationPanel::CreateItem(const QString& name,const QString& path, const int& flag, const QString& userData) -> QTreeWidgetItem* {
        auto GetItemType = [=](const ItemFlag& flag) {
            if (flag == ItemFlag::LoadedPlugin) {
                return ItemFlag::Plugin;
            }
            if(flag == ItemFlag::LoadedProcessor) {
                return ItemFlag::Processor;
            }            
            return flag;
        };
        auto GetItemStatus = [=](const ItemFlag& flag) {
            if (flag & ItemFlag::Loaded) {
                return ItemFlag::Loaded;
            }
            else {
                return (ItemFlag)0;
            }
        };
        auto GetIcon = [=](const ItemFlag& flag) {
            if (flag & ItemFlag::Plugin) {
                return QIcon(":/image/images/plugin.png");
            }
            else if (flag & ItemFlag::Processor) {
                return QIcon(":/image/images/processor.png");
            }
            return QIcon();
        };
        auto GetStatus = [=](const ItemFlag& flag) {
            if (flag & ItemFlag::Loaded) {                
                return QIcon(":/image/images/loaded.png");
            }
            else{
                return QIcon(":image/images/unloaded.png");
            }            
        };        

        auto item = new QTreeWidgetItem(flag);
        item->setText(0, name);        
        item->setToolTip(0, path);//save full path at tooltip
        item->setIcon(0, GetIcon(GetItemType((ItemFlag)flag)));        
        item->setIcon(1, GetStatus((ItemFlag)flag));
                
        if(false == userData.isEmpty()) {
            this->SetUserData(item, userData);
        }
        return item;
    }

    auto ApplicationPanel::SetUserData(QTreeWidgetItem* item, const QString& udata) const -> void {

        if(nullptr == item) {
            return;
        }
        item->setData(0, Qt::UserRole, udata);
    }

    auto ApplicationPanel::GetUserData(QTreeWidgetItem* item) -> QString {
        if(nullptr == item) {
            return QString();
        }
        return item->data(0, Qt::UserRole).toString();
    }

    auto ApplicationPanel::GetFileName(QString dllPath) -> QString {
        QFile f(dllPath);
        QFileInfo fileInfo(f.fileName());
        return fileInfo.fileName();
    }

    auto ApplicationPanel::AddApplicationPlugin(QTreeWidgetItem* parent) -> void {
        Q_UNUSED(parent)
    }

    auto ApplicationPanel::AddApplicationProc(QTreeWidgetItem* parent) -> void {
        Q_UNUSED(parent)
    }

    auto ApplicationPanel::AddPluginToPlayground(QTreeWidgetItem* plugin_item) -> void {
        auto path = plugin_item->toolTip(0);
        QStringList processorList;
        for(auto i=0;i<plugin_item->childCount();i++) {
            auto proc = plugin_item->child(i);
            auto proc_path = proc->toolTip(0);
            processorList.push_back(proc_path);
        }
        emit linkAppToPlaygroundRequested(path,processorList);
    }
        

    auto ApplicationPanel::LoadProcessor(QTreeWidgetItem* processor_item) -> void {
        auto path = processor_item->toolTip(0);
        emit loadProcessorRequested(path);
    }

    auto ApplicationPanel::LoadPlugin(QTreeWidgetItem* plugin_item) -> void {
        auto path = plugin_item->toolTip(0);
        emit loadPluginRequested(path);
    }
}