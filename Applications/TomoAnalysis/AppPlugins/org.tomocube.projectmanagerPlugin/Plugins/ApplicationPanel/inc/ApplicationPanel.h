#pragma once

#include <memory>
#include <QWidget>
#include <QTreeWidgetItem>

#include <IApplicationPanel.h>

#include "ApplicationPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class ApplicationPanel_API ApplicationPanel : public QWidget, public Interactor::IApplicationPanel {
        Q_OBJECT
        enum ItemFlag{
            None = 0,
            Plugin = None | 1,
            Processor = None | 2,

            Loaded = 4,
            InPlayground = 8,

            LoadedPlugin = Plugin | Loaded,
            LoadedProcessor = Processor | Loaded
        };
    public:
        typedef ApplicationPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ApplicationPanel(QWidget* parent=nullptr);
        ~ApplicationPanel();

        auto Update(Interactor::ApplicationDS ds)->bool override;

    signals:
        void linkAppToPlaygroundRequested(const QString&,const QStringList&);
        void loadProcessorRequested(const QString&);
        void loadPluginRequested(const QString&);

    private slots:
        void OnShowContextMenu(const QPoint& pos);
        

    private:
        auto Init()->bool;

        auto CreateItem(const QString& name,const QString& path, const int& flag, const QString& userData = "")->QTreeWidgetItem*;

        auto AddApplicationPlugin(QTreeWidgetItem* parent)->void;
        auto AddApplicationProc(QTreeWidgetItem* parent)->void;

        auto GetUserData(QTreeWidgetItem* item)->QString;
        auto SetUserData(QTreeWidgetItem* item, const QString& data) const ->void;

        auto ShowRootIndicator() const->void;

        auto LoadPlugin(QTreeWidgetItem* plugin_item)->void;
        auto LoadProcessor(QTreeWidgetItem* processor_item)->void;
        auto AddPluginToPlayground(QTreeWidgetItem* plugin_item)->void;

        auto GetFileName(QString dllPath)->QString;

        auto UpdatePlugin(QTreeWidgetItem* item)->void;
        auto UpdateProcessor(QTreeWidgetItem* item)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
