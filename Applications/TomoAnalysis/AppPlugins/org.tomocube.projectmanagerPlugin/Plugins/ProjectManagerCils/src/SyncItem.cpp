#include "SyncItem.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct SyncItem::Impl {
		QString infoId;
		QString title;
		QString dataId;
		int percentage = 0;
		QString path;
		QString message;
		bool downloaded = false;
		bool paused = false;
		bool pending = false;
		bool downloading = false;
	};

	SyncItem::SyncItem() : d(new Impl) {}

	SyncItem::SyncItem(SyncItem&& obj) noexcept : d(std::move(obj.d)) {}

	SyncItem::SyncItem(const SyncItem& obj) : d(new Impl{ *obj.d }) {}

	SyncItem& SyncItem::operator=(SyncItem&& obj) noexcept {
		if (d == obj.d) return *this;
		
		if (d)
			d.reset();
		d = std::move(obj.d);
		return *this;
	}

	SyncItem& SyncItem::operator=(const SyncItem& obj) {
		if (d == obj.d) return *this;
		
		if (d)
			d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	SyncItem::~SyncItem() = default;

	auto SyncItem::GetInfoId() const -> const QString& {
		return d->infoId;
	}

	auto SyncItem::GetDataId() const -> const QString& {
		return d->dataId;
	}

	auto SyncItem::GetTitle() const -> const QString& {
		return d->title;
	}

	auto SyncItem::GetPercentage() const -> int {
		return d->percentage;
	}

	auto SyncItem::IsDownloaded() const -> bool {
		return d->downloaded;
	}

	auto SyncItem::IsPaused() const -> bool {
		return d->paused;
	}

	auto SyncItem::IsPending() const -> bool {
		return d->pending;
	}

	auto SyncItem::IsDownloading() const -> bool {
		return d->downloading;
	}

	auto SyncItem::GetPath() const -> const QString& {
		return d->path;
	}

	auto SyncItem::GetErrorMessage() const -> const QString& {
		return d->message;
	}

	auto SyncItem::SetInfoId(const QString& infoId) -> void {
		d->infoId = infoId;
	}

	auto SyncItem::SetDataId(const QString& dataId) -> void {
		d->dataId = dataId;
	}

	auto SyncItem::SetTitle(const QString& title) -> void {
		d->title = title;
	}

	auto SyncItem::SetPercentage(int percentage) -> void {
		d->percentage = percentage;
	}

	auto SyncItem::SetPath(const QString& path) -> void {
		d->path = path;
	}

	auto SyncItem::SetErrorMessage(const QString& message) -> void {
		d->message = message;
	}

	auto SyncItem::SetDownloaded(bool downloaded) -> void {
		d->downloaded = downloaded;
	}

	auto SyncItem::SetPaused(bool paused) -> void {
		d->paused = paused;
	}

	auto SyncItem::SetPending(bool pending) -> void {
		d->pending = pending;
	}

	auto SyncItem::SetDownloading(bool downloading) -> void {
		d->downloading = downloading;
	}
}
