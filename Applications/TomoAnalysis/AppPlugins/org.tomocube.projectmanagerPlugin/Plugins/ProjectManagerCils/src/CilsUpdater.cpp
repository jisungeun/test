#include "CilsUpdater.h"

#include <QtConcurrent/qtconcurrentfilter.h>

#include "CilsClient.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsUpdater::Impl {
		TC::Cils::Client::CilsClient client;

		bool isLoadingWorkset = false;
		bool isLoadingPreview = false;
		bool isLoadingDownload = false;
		int nextWorksetId = -1;

		QStringList downloadQueue;

		auto ConvertToSyncItem(const TC::Cils::JsonEntity::ItemProgress& p) -> SyncItem {
			SyncItem item;
			item.SetTitle(p.GetTitle());
			item.SetDataId(p.GetDataId());
			item.SetPercentage(p.GetPercentage());
			item.SetPath(p.GetItemPath());
			item.SetInfoId(p.GetInfoId());

			switch (p.GetProgressStatus()) {
			case TC::Cils::JsonEntity::ProgressStatus::Pending:
				item.SetPending(true);
			case TC::Cils::JsonEntity::ProgressStatus::InProgress:
				[[fallthrough]];
			case TC::Cils::JsonEntity::ProgressStatus::Merging:
				item.SetDownloading(true);
				break;
			case TC::Cils::JsonEntity::ProgressStatus::Paused:
				item.SetPaused(true);
				break;
			case TC::Cils::JsonEntity::ProgressStatus::Finished:
				item.SetDownloaded(true);
				break;
			case TC::Cils::JsonEntity::ProgressStatus::Deleted:
				break;
			}

			return item;
		}
	};

	CilsUpdater::CilsUpdater(QObject* parent) : QObject(parent), d(new Impl) {}

	CilsUpdater::~CilsUpdater() = default;

	auto CilsUpdater::LoadWorkset() -> void {
		LoadWorksetRecursive({});
	}

	auto CilsUpdater::LoadWorksetItems(int worksetId) -> void {
		d->nextWorksetId = worksetId;

		LoadItemsQueue();
	}

	auto CilsUpdater::LoadDownloadItems(const QStringList& dataIds) -> void {
		auto* response = d->client.GetDownloadingItems();

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[this, response, dataIds = dataIds]() mutable -> void {
				if (!response->IsError()) {
					const auto& list = response->GetList();
					QVector<SyncItem> executions;

					for (const auto& s : list) {
						for (const auto& p : s.GetProgress()) {
							if (dataIds.contains(p.GetDataId())) {
								dataIds.removeAll(p.GetDataId());
								executions.push_back(d->ConvertToSyncItem(p));
								break;
							}
						}
					}

					emit DownloadItemsLoaded(executions, dataIds);
				} else {
					emit Error(response->GetError()->GetMessage());
				}

				delete response;
			}
		);
	}

	auto CilsUpdater::LoadPreview(const QString& dataId) -> void {
		if (d->isLoadingPreview)
			return;

		d->isLoadingPreview = true;
		auto* response = d->client.GetItemPreview(dataId);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[this, response, dataId]() -> void {
				d->isLoadingPreview = false;

				if (!response->IsError())
					emit PreviewLoaded(dataId, response->GetImage());
				else
					emit Error(response->GetError()->GetMessage());

				delete response;
			}
		);
	}

	auto CilsUpdater::DownloadItems(const QStringList& dataIds) -> void {
		auto* response = d->client.GetDownloadingItems();

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[this, response, dataIds = dataIds]() mutable -> void {
				if (!response->IsError()) {
					const auto& list = response->GetList();

					for (const auto& s : list) {
						for (const auto& p : s.GetProgress()) {
							if (dataIds.contains(p.GetDataId())) {
								dataIds.removeAll(p.GetDataId());
							}
						}
					}

					for (const auto& di : dataIds)
						if (!d->downloadQueue.contains(di))
							d->downloadQueue.push_back(di);

					RequestDownload();
				} else {
					emit Error(response->GetError()->GetMessage());
				}

				delete response;
			}
		);
	}

	auto CilsUpdater::CancelDownload(const QString& infoId) -> void {
		auto* response = d->client.CancelDownloadItem(infoId);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[response] {
				delete response;
			}
		);
	}

	auto CilsUpdater::PauseDownload(const QString& infoId) -> void {
		auto* response = d->client.PauseDownloadItem(infoId);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[response] {
				delete response;
			}
		);
	}

	auto CilsUpdater::ResumeDownload(const QString& infoId) -> void {
		auto* response = d->client.ResumeDownloadItem(infoId);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[response] {
				delete response;
			}
		);
	}

	auto CilsUpdater::LoadWorksetRecursive(const QVector<TC::Cils::JsonEntity::Workset>& worksets, int page) -> void {
		auto* response = d->client.GetWorksets(page);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[this, worksets = worksets, page, response]() mutable -> void {
				if (!response->IsError()) {
					const auto& list = response->GetList();
					const auto isFinished = page == response->GetPageLength();

					for (const auto& l : list)
						worksets.push_back(l);

					emit WorksetLoaded(worksets);

					if (!isFinished)
						LoadWorksetRecursive(worksets, page + 1);
				} else
					emit Error(response->GetError()->GetMessage());

				delete response;
			}
		);
	}

	auto CilsUpdater::LoadItemsQueue() -> void {
		if (d->nextWorksetId < 0 || d->isLoadingWorkset)
			return;

		auto id = d->nextWorksetId;
		d->nextWorksetId = -1;
		LoadItemRecursive(id, {});
	}

	auto CilsUpdater::LoadItemRecursive(int id, const QVector<TC::Cils::JsonEntity::Item>& items, int page) -> void {
		d->isLoadingWorkset = true;
		auto* response = d->client.GetWorksetItems(id, page);

		connect(response, &TC::Cils::Client::IResponse::Initialized,
			[this, items = items, id, page, response]() mutable -> void {
				if (!response->IsError()) {
					const auto& list = response->GetList();
					const auto isFinished = page == response->GetPageLength();

					for (const auto& l : list)
						items.push_back(l);

					emit ItemLoaded(id, items, isFinished);

					if (!isFinished)
						LoadItemRecursive(id, items, page + 1);
					else {
						d->isLoadingWorkset = false;
						LoadItemsQueue();
					}
				} else
					emit Error(response->GetError()->GetMessage());

				delete response;
			}
		);
	}

	auto CilsUpdater::RequestDownload() -> void {
		if (d->downloadQueue.isEmpty())
			return;

		d->isLoadingDownload = true;
		auto dataId = d->downloadQueue.takeFirst();
		auto* response = d->client.DownloadItem(dataId);

		connect(response, &TC::Cils::Client::IResponse::Initialized, this,
			[this, response, dataId] {
				if (!response->IsError() && response->IsDone()) {
					emit DownloadStarted(dataId);
				} else {
					if (const auto error = response->GetError())
						emit DownloadFailed(dataId, error->GetMessage());
					else
						emit DownloadFailed(dataId, "Unknown");
				}

				d->isLoadingDownload = false;
				RequestDownload();

				delete response;
			}
		);
	}
}
