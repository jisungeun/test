#pragma once

#include <memory>

#include <QString>

#include "ProjectManagerCilsExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class ProjectManagerCils_API SyncItem {
	public:
		SyncItem();
		SyncItem(SyncItem&&) noexcept;
		SyncItem(const SyncItem&);
		SyncItem& operator=(SyncItem&&) noexcept;
		SyncItem& operator=(const SyncItem&);
		~SyncItem();

		auto GetInfoId() const -> const QString&;
		auto GetDataId() const -> const QString&;
		auto GetTitle() const -> const QString&;
		auto GetPercentage() const -> int;
		auto GetPath() const->const QString&;
		auto GetErrorMessage() const -> const QString&;
		auto IsDownloaded() const -> bool;
		auto IsPaused() const -> bool;
		auto IsPending() const -> bool;
		auto IsDownloading() const -> bool;

		auto SetInfoId(const QString& infoId) -> void;
		auto SetDataId(const QString& dataId) -> void;
		auto SetTitle(const QString& title) -> void;
		auto SetPercentage(int percentage) -> void;
		auto SetPath(const QString& path) -> void;
		auto SetErrorMessage(const QString& message) -> void;
		auto SetDownloaded(bool downloaded) -> void;
		auto SetPaused(bool paused) -> void;
		auto SetPending(bool pending) -> void;
		auto SetDownloading(bool downloading) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}