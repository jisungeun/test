#pragma once

#include <memory>

#include <QObject>

#include "Item.h"
#include "Workset.h"
#include "ProjectManagerCilsExport.h"
#include "SyncItem.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class ProjectManagerCils_API CilsUpdater : public QObject {
		Q_OBJECT

	public:
		CilsUpdater(QObject* parent = nullptr);
		~CilsUpdater() override;

		auto LoadWorkset() -> void;
		auto LoadWorksetItems(int worksetId) -> void;
		auto LoadDownloadItems(const QStringList& dataIds) -> void;
		auto LoadPreview(const QString& dataId) -> void;
		auto DownloadItems(const QStringList& dataIds) -> void;

		auto CancelDownload(const QString& infoId) -> void;
		auto PauseDownload(const QString& infoId) -> void;
		auto ResumeDownload(const QString& infoId) -> void;

	signals:
		auto Error(const QString& message) -> void;
		auto WorksetLoaded(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void;
		auto ItemLoaded(int id, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void;
		auto DownloadItemsLoaded(const QVector<SyncItem>& items, const QStringList& missing) -> void;
		auto PreviewLoaded(const QString& dataId, const QByteArray& image) -> void;
		auto DownloadStarted(const QString& dataId) -> void;
		auto DownloadFailed(const QString& dataId, const QString& error) -> void;

	private:
		auto LoadWorksetRecursive(const QVector<TC::Cils::JsonEntity::Workset>& worksets, int page = 1) -> void;
		auto LoadItemsQueue() -> void;
		auto LoadItemRecursive(int worksetId, const QVector<TC::Cils::JsonEntity::Item>& items, int page = 1) -> void;
		auto RequestDownload() -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
