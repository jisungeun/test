#include <QTimer>
#include <QPainter>

#include "CilsPanel.h"

#include <ui_CilsItemWidget.h>

#include "CilsItemWidget.h"
#include "CilsUpdater.h"

#include "ui_CilsPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsPanel::Impl {
		Ui::CilsPanel ui{};

		QMap<QString, CilsItemWidget*> items;
		CilsUpdater client;
		QTimer timer;

		auto LoadPreview() -> void {
			for (const auto& key : items.keys()) {
				if (!items[key]->IsPainted()) {
					client.LoadPreview(key);
					return;
				}
			}
		}

		auto ToLast(const QString& dataId) -> void {
			
		}
	};

	CilsPanel::CilsPanel(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->timer.setInterval(500);

		connect(&d->timer, &QTimer::timeout, this, &CilsPanel::OnTimeout);
		connect(&d->client, &CilsUpdater::DownloadItemsLoaded, this, &CilsPanel::OnDownloadItemLoaded);
		connect(&d->client, &CilsUpdater::DownloadStarted, this, &CilsPanel::OnDownloadStarted);
		connect(&d->client, &CilsUpdater::DownloadFailed, this, &CilsPanel::OnDownloadFailed);
		connect(&d->client, &CilsUpdater::PreviewLoaded, this, &CilsPanel::OnPreviewLoaded);
		connect(&d->client, &CilsUpdater::Error, this, &CilsPanel::OnError);
		connect(d->ui.clearBtn, &QPushButton::clicked, this, &CilsPanel::OnClearBtnClicked);
	}

	CilsPanel::~CilsPanel() = default;

	auto CilsPanel::UpdateCils(const QMap<int, QStringList>& items, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>& images) -> void {
		QStringList ids;

		for (const auto worksetId : items.keys()) {
			for (const auto& dataId : items[worksetId]) {
				if (!ids.contains(dataId)) {
					ids.push_back(dataId);

					auto* item = new CilsItemWidget(&d->client);
					item->SetDataId(dataId);
					item->SetPreview(images[dataId]);
					item->AddWorkset(worksetId, worksets[worksetId]);

					connect(item, &CilsItemWidget::Downloaded, this, &CilsPanel::OnDownloaded);

					d->items[dataId] = item;
					auto* layout = dynamic_cast<QVBoxLayout*>(d->ui.downloadFrame->layout());
					layout->insertWidget(0, item);
				} else {
					d->items[dataId]->AddWorkset(worksetId, worksets[worksetId]);
				}
			}
		}

		d->client.DownloadItems(ids);
		d->timer.start();
		d->LoadPreview();
	}

	auto CilsPanel::OnDownloadStarted(const QString& dataId) -> void {
		if (d->items.contains(dataId)) {
			d->items[dataId]->StartDownload();

			auto* layout = dynamic_cast<QVBoxLayout*>(d->ui.downloadFrame->layout());
			layout->removeWidget(d->items[dataId]);
			layout->insertWidget(0, d->items[dataId]);
		}
	}

	auto CilsPanel::OnDownloadFailed(const QString& dataId, const QString& message) -> void {
		if (d->items.contains(dataId)) {
			d->items[dataId]->FailDownload(message);
			d->ToLast(dataId);
		}
	}

	auto CilsPanel::OnPreviewLoaded(const QString& dataId, const QByteArray& image) -> void {
		if (d->items.contains(dataId)) {
			QPixmap pixmap;
			pixmap.loadFromData(image);
			d->items[dataId]->SetPreview(pixmap);
		}
		d->LoadPreview();
	}

	auto CilsPanel::OnDownloaded(const QString& dataId, const QMap<int, QString>& worksets, const QString& path) -> void {
		emit addTcf(path);

		for (const auto& id : worksets.keys()) {
			emit linkTcfToWorkset(path, worksets[id], id);
		}

		d->ToLast(dataId);
	}

	auto CilsPanel::OnClearBtnClicked() -> void {
		for (const auto& id : d->items.keys()) {
			if (d->items[id]->IsFinished()) {
				d->ui.downloadFrame->layout()->removeWidget(d->items[id]);
				delete d->items[id];
				d->items.remove(id);
			}
		}

		for(int i = 0; i < d->ui.downloadFrame->layout()->count(); i++) {
			auto* item = d->ui.downloadFrame->layout()->itemAt(i);
			auto* widget = item->widget();
			const auto *cils = dynamic_cast<CilsItemWidget*>(widget);

			if(cils->IsFinished()) {
				d->ui.downloadFrame->layout()->removeWidget(widget);
				delete item;
				delete widget;
			}
		}
	}

	auto CilsPanel::OnError(const QString& message) -> void {}

	auto CilsPanel::OnTimeout() -> void {
		if (d->items.isEmpty()) {
			d->timer.stop();
			return;
		}

		QStringList ids;
		for (const auto& i : d->items.keys())
			if (!d->items[i]->IsFinished())
				ids << i;

		d->client.LoadDownloadItems(ids);
	}

	auto CilsPanel::OnDownloadItemLoaded(const QVector<SyncItem>& syncs, const QStringList& missing) -> void {
		for (const auto& s : syncs) {
			if (d->items.contains(s.GetDataId())) {
				d->items[s.GetDataId()]->Update(s);
			}
		}
	}
}
