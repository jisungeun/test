#include "ui_ThumbnailPanel.h"
#include "ThumbnailPanel.h"

#include <iostream>
#include <QCheckBox>
#include <QScrollBar>
#include <QMouseEvent>
#include <QMenu>

#include <TCFMetaReader.h>

#include "ThumbnailGroupBox.h"
#include "UIUtility.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct ThumbnailPanel::Impl {
        Ui::ThumbnailPanel* ui{nullptr};

        QList<LinkableThumbnailWidget*> children;
        QList<ThumbnailGroupBox*> groups;
        QList<LinkableThumbnailWidget*> selectedList;
        LinkableThumbnailWidget* lastClickedWidget = nullptr;

        TC::IO::TCFMetaReader* metaReader{ nullptr };

        QList<int> groupsize;        
        int loaded_height{-1};
    };

    ThumbnailPanel::ThumbnailPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        //Q_UNUSED(parent)
        d->ui = new Ui::ThumbnailPanel;
        d->ui->setupUi(this);

        d->ui->selectAllButton->hide();
        d->ui->deselectAllButton->hide();

        d->ui->selectAllButton->setObjectName("bt-round-gray500");
        d->ui->deselectAllButton->setObjectName("bt-round-gray500");
        d->metaReader = new TC::IO::TCFMetaReader;

    }

    ThumbnailPanel::~ThumbnailPanel() {
        delete d->metaReader;
    }
    void ThumbnailPanel::OnScrollAreaChanged(int sc) {        
        //check wheather file is visible of not
        auto start = sc;
        auto end = sc + d->ui->scrollArea->viewport()->size().height();

        auto child_cnt =0;
        auto group_cnt = 0;
        auto line_cnt = 0;
        auto final_height = 0;        
        for(auto child : d->children) {            
            if(child_cnt > d->groupsize[group_cnt]-1) {
                line_cnt = 0;
                child_cnt = 0;
                group_cnt++;
                d->loaded_height = final_height;                
            }
            auto group_height = d->groups[group_cnt]->pos().y();
            auto child_st = group_height + child->pos().y();
            auto child_end = group_height + child->pos().y() + child->size().height();
            child_cnt++;
            if(child_st < d->loaded_height) {
                continue;
            }
            if(child_st > end) {
                break;
            }            
            if((child_st < end && child_st > start) || (child_end > start && child_end < end)) {                
                child->ShowImage(true);                
                line_cnt ++;
                final_height = child_st;
                if(line_cnt > 5){
                    d->loaded_height = child_st;
                    line_cnt = 0;
                }
            }            
        }
    }
    void ThumbnailPanel::InitialLoad() {
        auto cnt = 0;
        auto temp_cnt =0;
        auto cur_grp_idx = 0;        
        for(auto child : d->children) {       
            if(temp_cnt > d->groupsize[cur_grp_idx]-1) {
                temp_cnt = 0;
                if(cnt < 5) {
                    cnt = 6;
                }else if(cnt < 10) {
                    cnt = 12;
                }else if(cnt < 17) {
                    cnt = 18;
                }else if(cnt<25) {
                    cnt = 25;
                }
                cur_grp_idx++;
                if(d->groupsize.count() == cur_grp_idx) {
                    break;
                }
            }
            /*if(cnt > 17 ) {
                break;
            } */
            if(cnt>25) {
                break;
            }
            child->ShowImage(true);
            cnt++;
            temp_cnt++;            
        }
    }
    void ThumbnailPanel::Clear() {
        d->groups.clear();
        d->groupsize.clear();
        d->children.clear();
        d->selectedList.clear();
        d->lastClickedWidget = nullptr;

        d->loaded_height = -1;
        d->ui->selectAllButton->hide();
        d->ui->deselectAllButton->hide();

        if(nullptr != d->ui->scrollArea->widget()) {
            for(auto child : d->ui->scrollArea->widget()->children()) {                
                delete child;
                child = nullptr;
            }
        }

        delete d->ui->scrollArea->widget();        
    }

    void ThumbnailPanel::ShowProject(const ProjectInfo::Pointer& project) {
        if(nullptr == project) {
            return;
        }

        this->Clear();

        if(false == project->GetTCFDirList().isEmpty()) {
            d->ui->selectAllButton->show();
            d->ui->deselectAllButton->show();
        }

        // set main layout
        auto previewWidget = new QWidget;
        previewWidget->setObjectName("widget-panel");

        auto layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(14);
        previewWidget->setLayout(layout);
        d->ui->scrollArea->setWidget(previewWidget);

        QMap<int, ThumbnailGroupBox*> worksets;
        // set folder
        for (const auto& dir : project->GetTCFDirList()) {
            if (nullptr == dir) {
                continue;
            }

            QStringList fileList;
            ScanDir(dir, fileList);
            
            const auto worksetIds = project->GetWorksetIdOfTcfDir(dir);
            QVector<ThumbnailGroupBox*> thumbnailGroupBoxs;
            if (worksetIds.isEmpty())
                thumbnailGroupBoxs.push_back(new ThumbnailGroupBox(ThumbnailGroupBox::Project, dir->GetName(), this));
            else {
                for (const auto id : worksetIds) {
                    if (!worksets.contains(id))
                        worksets[id] = new ThumbnailGroupBox(ThumbnailGroupBox::Project, project->GetWorksetName(id), this);

                    thumbnailGroupBoxs.push_back(worksets[id]);
                }
            }
            
            for (auto* thumbnailGroupBox : thumbnailGroupBoxs) {
                d->groups.push_back(thumbnailGroupBox);
                auto children = thumbnailGroupBox->GetChildren();

                for (auto* c : children)
                    d->children.removeAll(c);
                d->groupsize.removeOne(children.size());

                for (const auto& path : fileList) {
                    auto imageWidget = new LinkableThumbnailWidget;
                    if (false == imageWidget->SetFile(path, true)) {
                        imageWidget = nullptr;
                        continue;
                    }
                    UpdateValidAnalaysis(imageWidget);
                    imageWidget->installEventFilter(this);
                    children.append(imageWidget);
                }
                thumbnailGroupBox->SetChildren(children);
                layout->addWidget(thumbnailGroupBox);
                d->groupsize.push_back(children.size());
                d->children.append(children);
            }
        }

        if (false == d->children.isEmpty()) {
            layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }
        connect(d->ui->scrollArea->verticalScrollBar(),SIGNAL(valueChanged(int)),this,SLOT(OnScrollAreaChanged(int)));
        // update link info
                
        UpdateLink(project);        
        //OnScrollAreaChanged(0);
        
        InitialLoad();        
    }

    void ThumbnailPanel::ShowHypercube(const HyperCube::Pointer& hypercube, const ProjectInfo::Pointer& project) {
        if (nullptr == hypercube) {
            return;
        }

        if (nullptr == project) {
            return;
        }

        this->Clear();

        if (false == hypercube->GetCubeList().isEmpty()) {
            d->ui->selectAllButton->show();
            d->ui->deselectAllButton->show();
        }

        // set main layout
        auto previewWidget = new QWidget;
        previewWidget->setObjectName("widget-panel");

        auto layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(14);
        previewWidget->setLayout(layout);
        d->ui->scrollArea->setWidget(previewWidget);

        // set cube
        for (const auto& cube : hypercube->GetCubeList()) {
            if (nullptr == cube) {
                continue;
            }

            const auto thumbnailGroupBox = new ThumbnailGroupBox(ThumbnailGroupBox::Cube, cube->GetName(), this);
            d->groups.push_back(thumbnailGroupBox);
            QList<LinkableThumbnailWidget*> children;
            for (const auto& dir : cube->GetTCFDirList()) {
                if(nullptr == dir) {
                    continue;
                }

                auto imageWidget = new LinkableThumbnailWidget;
                if (false == imageWidget->SetFile(dir->GetPath(),true)) {
                    continue;
                }
                UpdateValidAnalaysis(imageWidget);
                imageWidget->installEventFilter(this);
                children.append(imageWidget);
            }

            thumbnailGroupBox->SetChildren(children);
            layout->addWidget(thumbnailGroupBox);
            d->groupsize.push_back(children.size());
            d->children.append(children);
        }

        if(false == d->children.isEmpty()) {
            layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }

        // update link info
        UpdateLink(project);        

        InitialLoad();        
    }

    void ThumbnailPanel::ShowCube(const Cube::Pointer& cube, const ProjectInfo::Pointer& project) {
        if (nullptr == cube) {
            return;
        }

        if (nullptr == project) {
            return;
        }        
        
        this->Clear();

        if (false == cube->GetTCFDirList().isEmpty()) {
            d->ui->selectAllButton->show();
            d->ui->deselectAllButton->show();
        }

        // set main layout
        auto previewWidget = new QWidget;
        previewWidget->setObjectName("widget-panel");

        auto layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(14);
        previewWidget->setLayout(layout);
        d->ui->scrollArea->setWidget(previewWidget);

        // set cube
        const auto thumbnailGroupBox = new ThumbnailGroupBox(ThumbnailGroupBox::Cube, cube->GetName(), this);
        d->groups.push_back(thumbnailGroupBox);
        QList<LinkableThumbnailWidget*> children;
        for (const auto& dir : cube->GetTCFDirList()) {
            if (nullptr == dir) {
                continue;
            }

            auto imageWidget = new LinkableThumbnailWidget;
            if (false == imageWidget->SetFile(dir->GetPath(),true)) {
                continue;
            }
            UpdateValidAnalaysis(imageWidget);
            imageWidget->installEventFilter(this);
            children.append(imageWidget);
        }

        thumbnailGroupBox->SetChildren(children);
        layout->addWidget(thumbnailGroupBox);
        d->groupsize.push_back(children.size());
        d->children.append(children);

        if (false == d->children.isEmpty()) {
            layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }

        // update link info
        UpdateLink(project);        

        InitialLoad();        
    }

    void ThumbnailPanel::AddItem(const TCFDir::Pointer& dir) {
        if (nullptr == dir) {
            return;
        }

        QStringList fileList;
        ScanDir(dir, fileList);

        
        const auto thumbnailGroupBox = new ThumbnailGroupBox(ThumbnailGroupBox::Project, dir->GetName(), this);
        d->groups.push_back(thumbnailGroupBox);
        QList<LinkableThumbnailWidget*> children;
        for (const auto& path : fileList) {
            auto imageWidget = new LinkableThumbnailWidget;            
            if (false == imageWidget->SetFile(path,true)) {
                continue;
            }
            UpdateValidAnalaysis(imageWidget);
            imageWidget->installEventFilter(this);
            children.append(imageWidget);            
        }

        thumbnailGroupBox->SetChildren(children);

        auto layout = static_cast<QVBoxLayout*>(d->ui->scrollArea->widget()->layout());
        layout->addWidget(thumbnailGroupBox);

        if (true == d->children.isEmpty()) {
            layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

            d->ui->selectAllButton->show();
            d->ui->deselectAllButton->show();
        }
        d->groupsize.push_back(children.size());
        d->children.append(children);
        
        InitialLoad();        
    }

    void ThumbnailPanel::UpdateValidAnalaysis(LinkableThumbnailWidget* child) const {
        auto path = child->GetTCFPath();
        const uint64_t arrSize = d->metaReader->ReadArrSize(path);
        child->SetAbleAnalysis(2);
        return;
        //if(arrSize < 2500*2500){
        if (arrSize <= 2500*2500*200) {//TODO: AI enable
            child->SetAbleAnalysis(2);
        }
        else if (arrSize < INT_MAX) {//Ri enable
            child->SetAbleAnalysis(1);
        }
        else {//not available
            child->SetAbleAnalysis(0);
        }      
    }

    void ThumbnailPanel::UpdateLink(const ProjectInfo::Pointer& project) const {
        if(nullptr == project) {
            return;
        }

        for (auto child : d->children) {
            QStringList cubeNameList;
            const auto playground = project->GetCurrentPlayground();
            if(nullptr == playground) {
                continue;
            }

            for (const auto& hypercube : playground->GetHyperCubeList()) {
                if (nullptr == hypercube) {
                    continue;
                }

                for (const auto& cube : hypercube->GetCubeList()) {
                    if (nullptr == cube) {
                        continue;
                    }

                    auto cubeName = cube->GetName();
                    for (const auto& tcfFolder : cube->GetTCFDirList()) {
                        if (nullptr == tcfFolder) {
                            continue;
                        }

                        if (child->GetTCFPath() == tcfFolder->GetPath()) {
                            cubeNameList << cubeName;
                        }
                    }
                }
            }

            child->SetLinkedCubeNameList(cubeNameList);
        }
    }

    void ThumbnailPanel::SetPathList(const QStringList& pathList, const ProjectInfo::Pointer& project) {
        auto previewWidget = new QWidget;
        auto layout = new QVBoxLayout;
        previewWidget->setLayout(layout);

        d->ui->scrollArea->setWidget(previewWidget);

        auto gridLayout = new QGridLayout;
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setSpacing(6);
        layout->addLayout(gridLayout);
        for (auto i = 0; i < pathList.size(); ++i) {
            auto imageWidget = new LinkableThumbnailWidget;
            if (false == imageWidget->SetFile(pathList.at(i),true)) {
                continue;
            }
            UpdateValidAnalaysis(imageWidget);
            imageWidget->installEventFilter(this);            
            d->children.append(imageWidget);

            gridLayout->addWidget(imageWidget, i / 6, i % 6);
            gridLayout->setRowStretch(i / 6, 1);
            gridLayout->setColumnStretch(i % 6, 1);
        }

        if (0 != pathList.size() % 6) {
            for (auto i = pathList.size() % 6; i < 6; ++i) {
                const auto imageWidget = new LinkableThumbnailWidget;
                UpdateValidAnalaysis(imageWidget);
                gridLayout->addWidget(imageWidget, i / 6, i % 6);
                gridLayout->setRowStretch(i / 6, 1);
                gridLayout->setColumnStretch(i % 6, 1);
            }
        }
        layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

        UpdateLink(project);        

        //InitialLoad();
    }

    auto ThumbnailPanel::GetLastSelectedTCF() const ->QString {
        if(true == d->selectedList.isEmpty()) {
            return QString();
        }

        auto last = d->selectedList.last();
        if(last->Selected()) {
            return d->selectedList.last()->GetTCFPath();
        }

        return QString();
    }

    auto ThumbnailPanel::GetSelectedTCFList() const ->QStringList {
        QStringList stringList;

        for(auto child : d->children) {
            if(nullptr == child) {
                continue;
            }

            if(true == child->Selected()) {
                stringList << child->GetTCFPath();
            }
        }

        return stringList;
    }

    auto ThumbnailPanel::GetLinkedCubeNameList() const ->QStringList {
        QStringList stringList;

        for (auto child : d->children) {
            if (nullptr == child) {
                continue;
            }

            auto cubeNameList = child->GetLinkedCubeNameList();
            for(auto name : cubeNameList) {
                if(false == stringList.contains(name)) {
                    stringList << name;
                }
            }
        }

        return stringList;
    }

    bool ThumbnailPanel::eventFilter(QObject* obj, QEvent* event) {
        if (event->type() == QEvent::MouseButtonDblClick) {
            auto selectedWidget = dynamic_cast<LinkableThumbnailWidget*>(obj);
            if (nullptr == selectedWidget) {
                return false;
            }
            d->lastClickedWidget = selectedWidget;
            const auto selected = !selectedWidget->Selected();
            selectedWidget->SetSelected(selected);
            if (true == selected) {
                d->selectedList.append(selectedWidget);
            }
            else {
                d->selectedList.removeOne(selectedWidget);
            }            
            emit infoPanel(selectedWidget->GetTCFPath());
            
            return true;
        }
        else if (event->type() == QEvent::MouseButtonPress) {
            auto selectedWidget = dynamic_cast<LinkableThumbnailWidget*>(obj);
            if (nullptr == selectedWidget) {
                return false;
            }            
            if (Qt::ShiftModifier == QApplication::keyboardModifiers()) {
                auto first = 0;
                if (nullptr != d->lastClickedWidget) {
                    first = d->children.indexOf(d->lastClickedWidget);
                }

                const auto second = d->children.indexOf(selectedWidget);

                for (auto i = first; i <= second; ++i) {
                    if (nullptr == d->children.at(i)) {
                        continue;
                    }

                    if(false == d->children.at(i)->Selected()) {
                        d->children.at(i)->SetSelected(true);
                        d->selectedList.append(d->children.at(i));
                    }
                }
            }
            else {                
                const auto selected = !selectedWidget->Selected();
                selectedWidget->SetSelected(selected);

                if(true == selected) {
                    d->selectedList.append(selectedWidget);                    
                }
                else {                    
                    d->selectedList.removeOne(selectedWidget);
                }
                d->lastClickedWidget = selectedWidget;

                QMouseEvent *me = static_cast<QMouseEvent *>(event);
                if(me->button() == Qt::RightButton) {
                    QMenu myMenu;
                    myMenu.addAction("Create Simple Playground");
                    myMenu.addAction("Open Folder");
                    myMenu.addAction("Link to Cube");
                    myMenu.addAction("Unlink from Cube");
                    myMenu.addAction("Viewer");
                    QAction* selectedItem = myMenu.exec(me->globalPos());
                    if (selectedItem) {
                        auto text = selectedItem->text();
                        if (text == "Create Simple Playground") {
                            emit createSimple(selectedWidget->GetTCFPath());
                        }
                        else if (text == "Open Folder") {
                            emit openFolder(selectedWidget->GetTCFPath());
                        }
                        else if (text == "Link to Cube") {
                            emit linkCube(selectedWidget->GetTCFPath());
                        }
                        else if (text == "Unlink from Cube") {
                            emit unlinkCube(selectedWidget->GetTCFPath());
                        }
                        else if (text == "Viewer") {
                            QStringList tcfList{ selectedWidget->GetTCFPath() };
                            emit openViewer(tcfList);
                        }
                    }
                }
            }
        }        
        return true;
    }

    auto ThumbnailPanel::ClearSelection() const -> void {
        for(auto child:d->children) {
            if(nullptr == child) {
                continue;;
            }
            child->SetSelected(false);
        }
        d->selectedList.clear();
        d->lastClickedWidget = nullptr;
    }

    void ThumbnailPanel::on_selectAllButton_clicked() const {
        for (auto child : d->children) {
            if (nullptr == child) {
                continue;
            }
            child->SetSelected(true);
        }
    }

    void ThumbnailPanel::on_deselectAllButton_clicked() const {
        for (auto child : d->children) {
            if (nullptr == child) {
                continue;
            }

            child->SetSelected(false);
        }
        d->selectedList.clear();
        d->lastClickedWidget = nullptr;
    }

    void ThumbnailPanel::ScanDir(const TCFDir::Pointer& dir, QStringList& fileList) {
        if(nullptr == dir) {
            return;
        }

        for(const auto& file : dir->GetFileList()) {
            auto filePath = QString("%1/%2").arg(dir->GetPath()).arg(file);
            fileList.append(filePath);
        }

        for(const auto& sub : dir->GetDirList()) {
            if (nullptr == sub) {
                continue;
            }

            ScanDir(sub, fileList);
        }
    }
}