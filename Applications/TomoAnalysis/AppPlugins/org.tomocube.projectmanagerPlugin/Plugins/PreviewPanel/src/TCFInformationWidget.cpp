#include <iostream>
#include <QFileInfo>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_TCFInformationWidget.h"
#include "TCFInformationWidget.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct TCFInformationWidget::Impl {
        Ui::TCFInformationWidget* ui{ nullptr };
        QString path;
    };

    TCFInformationWidget::TCFInformationWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->ui = new Ui::TCFInformationWidget();
        d->ui->setupUi(this);

        d->ui->thumbnailWidget->SetVisibleImageTypeControl(true);
        d->ui->thumbnailWidget->SetVisibleTimelapseControl(true);
        d->ui->thumbnailWidget->KeepSquare(true);

        d->ui->metaTableWidget->setProperty("styleVariant",1);        

        this->setWindowTitle("Information");
        this->resize(1280, 800);

        setObjectName("widget-popup");
        //setAttribute(Qt::WA_DeleteOnClose);
    }

    TCFInformationWidget::~TCFInformationWidget() {
        delete d->ui;
    }

    void TCFInformationWidget::SetFile(const QString& path) {
        if(true == path.isEmpty()) {
            QMessageBox::warning(nullptr, "Error", "TCF file path is empty.");
            return;
        }
        d->path = path;
        // read meta info
        std::shared_ptr<TC::IO::TCFMetaReader::Meta> meta{ nullptr };
        if(false == this->ReadTCFCommonMeta(path, meta)) {
            QMessageBox::warning(nullptr, "Error", "Fail to read TCF file.");
            return;
        }

        QList<QPair<QString, QString>> infoList;
        if (false == this->GenerateFileInfo(path, meta, infoList)) {
            QMessageBox::warning(nullptr, "Error", "Fail to generate TCF meta info.");
            return;
        }

        // thumbnail ui
        if(false == d->ui->thumbnailWidget->SetTCFPath(path)) {
            QMessageBox::warning(nullptr, "Error", "Fail to open TCF file.");
            return;
        }

        d->ui->thumbnailWidget->forceVisible();

        // meta table
        // clear ui
        d->ui->metaTableWidget->clear();

        const auto row = infoList.size();
        d->ui->metaTableWidget->setRowCount(row);

        const QStringList headers{ "Name", "Value" };
        d->ui->metaTableWidget->setColumnCount(headers.size());
        d->ui->metaTableWidget->setHorizontalHeaderLabels(headers);

        for(auto i = 0; i < row; ++i) {
            auto keyItem = new QTableWidgetItem(infoList.at(i).first);
            keyItem->setFlags(keyItem->flags() ^ Qt::ItemIsEditable);

            auto valueItem = new QTableWidgetItem(infoList.at(i).second);
            valueItem->setFlags(valueItem->flags() ^ Qt::ItemIsEditable);

            d->ui->metaTableWidget->setItem(i, 0, keyItem);
            d->ui->metaTableWidget->setItem(i, 1, valueItem);
        }

        d->ui->metaTableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
        d->ui->metaTableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);    
    }

    void TCFInformationWidget::SetWindowSize(const int& w, const int& h) {
        this->resize(w, h);
    }

    void TCFInformationWidget::closeEvent(QCloseEvent* ev) {
        if (ev->spontaneous()) {
            emit infoClosed(d->path);
        }
        else {
            QWidget::closeEvent(ev);
        }
    }

    void TCFInformationWidget::resizeEvent(QResizeEvent*) {
        d->ui->thumbnailWidget->setFixedWidth(d->ui->thumbnailWidget->height());
    }

    auto TCFInformationWidget::ReadTCFCommonMeta(const QString& path, TC::IO::TCFMetaReader::Meta::Pointer& meta)->bool {
        if(true == path.isEmpty()) {
            return false;
        }

        auto reader = TC::IO::TCFMetaReader();
        meta = reader.Read(path);

        if(nullptr == meta) {
            return false;
        }

        return true;
    }

    auto TCFInformationWidget::GenerateFileInfo(const QString& path,
                                               const TC::IO::TCFMetaReader::Meta::Pointer& meta,
                                               QList<QPair<QString, QString>>& infoList)->bool {
        if(nullptr == meta) {
            return false;
        }

        auto MakeInfo = [=](const QString& key, const QString& value) {
            return QPair<QString, QString>(key, value);
        };

        auto GetSimpleName = [=](const QString& path) {
            auto split = path.split("/");
            auto name = split[split.size() - 1];

            if(20 > name.size()) {
                return name;
            }

            const auto list = name.split(".");
            if(3 > list.size()) {
                return name;
            }

            name = name.remove(0, 20);
            return name.chopped(4);
        };

        infoList.push_back(MakeInfo("Title",
                                    QString("%1").arg(GetSimpleName(path))));

        infoList.push_back(MakeInfo("Device / Host",
                                    QString("%1 / %2").arg(meta->common.deviceSerial)
                                                      .arg("N/A")));

        infoList.push_back(MakeInfo("Data Path", path));

        infoList.push_back(MakeInfo("Recorded / Created",
                                    QString("%1 / %2").arg(meta->common.recordingTime)
                                                      .arg(meta->common.createDate)));

        infoList.push_back(MakeInfo("Magnification / NA",
                                    QString("%1 / %2").arg(QString::number(meta->info.device.magnification, 'f', 1))
                                                      .arg(QString::number(meta->info.device.na, 'f', 2))));

        infoList.push_back(MakeInfo("Medium RI",
                                    QString::number(meta->info.device.ri, 'f', 3)));

        infoList.push_back(MakeInfo("Laser (um)",
                                    QString::number(meta->info.device.waveLength, 'f', 3)));

        infoList.push_back(MakeInfo("Camera Gain (dB)",
                                    QString::number(meta->info.imaging.cameraGain, 'f', 3)));

        infoList.push_back(MakeInfo("Camera Shutter (ms)",
                                    QString::number(meta->info.imaging.cameraShutter, 'f', 3)));


        // position
        double positionX = 0.0;
        double positionY = 0.0;
        double positionZ = 0.0;

        if(0 < meta->data.data3D.dataCount) {
            positionX = meta->data.data3D.positionX;
            positionY = meta->data.data3D.positionY;
            positionZ = meta->data.data3D.positionZ;
        }
        else if (0 < meta->data.data2DMIP.dataCount) {
            positionX = meta->data.data2DMIP.positionX;
            positionY = meta->data.data2DMIP.positionY;
            positionZ = meta->data.data2DMIP.positionZ;
        }
        else if (0 < meta->data.data3DFL.dataCount) {
            positionX = meta->data.data3DFL.positionX;
            positionY = meta->data.data3DFL.positionY;
            positionZ = meta->data.data3DFL.positionZ;
        }
        else if (0 < meta->data.data2D.dataCount) {
            positionX = meta->data.data2D.positionX;
            positionY = meta->data.data2D.positionY;
            positionZ = meta->data.data2D.positionZ;
        }

        infoList.push_back(MakeInfo("Position (mm)", 
                                    QString("X:%1 Y:%2 Z:%3").arg(positionX, 0, 'f', 3)
		                                                     .arg(positionY, 0, 'f', 3)
		                                                     .arg(positionZ, 0, 'f', 3)));

        // tile
        if(true == meta->info.tile.exist) {
            // TCF 1.3.1 이전 버전 호환
            if(meta->info.tile.tilesH == 0 || meta->info.tile.tilesV == 0) {
                infoList.push_back(MakeInfo("Tile (Col, Row, Count)",
                                            QString("%1, %2, %3").arg(meta->info.tile.columnIndex)
                                                                 .arg(meta->info.tile.rowIndex)
                                                                 .arg(meta->info.tile.tiles)));
            }
            else {
                infoList.push_back(MakeInfo("Tile (Col, Row)",
                                            QString("%1 of %2, %3 of %4").arg(meta->info.tile.columnIndex)
                                                                         .arg(meta->info.tile.tilesH)
                                                                         .arg(meta->info.tile.rowIndex)
                                                                         .arg(meta->info.tile.tilesV)));
            }

            infoList.push_back(MakeInfo("Tile Overlap (um)",
                                        QString("%1 x %2").arg(meta->info.tile.overlapH)
                                                          .arg(meta->info.tile.overlapV)));

            infoList.push_back(MakeInfo("Tile Position (mm)",
                                        QString("%1, %2, %3").arg(meta->info.tile.x)
                                                             .arg(meta->info.tile.y)
                                                             .arg(meta->info.tile.z)));
        }
        
        // Tomogram
        if(true == meta->data.data3D.exist) {
            infoList.push_back(MakeInfo("RI Dimension (um) / Pixels",
                                        QString("%1 x %2 x %3 / %4 x %5 x %6")
                                                .arg(meta->data.data3D.resolutionX * meta->data.data3D.sizeX, 3, 'f', 3)
                                                .arg(meta->data.data3D.resolutionY * meta->data.data3D.sizeY, 3, 'f', 3)
                                                .arg(meta->data.data3D.resolutionZ * meta->data.data3D.sizeZ, 3, 'f', 3)
                                                .arg(meta->data.data3D.sizeX)
                                                .arg(meta->data.data3D.sizeY)
                                                .arg(meta->data.data3D.sizeZ)));

            infoList.push_back(MakeInfo("RI Resolution (um)",
                                        QString("%1 x %2 x %3").arg(meta->data.data3D.resolutionX, 3, 'f', 3)
                                                               .arg(meta->data.data3D.resolutionX, 3, 'f', 3)
                                                               .arg(meta->data.data3D.resolutionZ, 3, 'f', 3)));

            infoList.push_back(MakeInfo("RI Timeframes", QString::number(meta->data.data3D.dataCount)));

            if(1 < meta->data.data3D.dataCount) {
                infoList.push_back(MakeInfo("RI Interval (sec)",
                                            QString::number(meta->data.data3D.timeInterval, 'f', 3)));
            }
        }

        // FL Tomogram
        if(true == meta->data.data3DFL.exist) {
            infoList.push_back(MakeInfo("FL Dimension (um) / Pixels",
                                        QString("%1 x %2 x %3 / %4 x %5 x %6")
                                                .arg(meta->data.data3DFL.resolutionX * meta->data.data3DFL.sizeX, 3, 'f', 3)
                                                .arg(meta->data.data3DFL.resolutionY * meta->data.data3DFL.sizeY, 3, 'f', 3)
                                                .arg(meta->data.data3DFL.resolutionZ * meta->data.data3DFL.sizeZ, 3, 'f', 3)
                                                .arg(meta->data.data3DFL.sizeX)
                                                .arg(meta->data.data3DFL.sizeY)
                                                .arg(meta->data.data3DFL.sizeZ)));

            infoList.push_back(MakeInfo("FL Resolution (um)",
                                        QString("%1 x %2 x %3").arg(meta->data.data3DFL.resolutionX, 3, 'f', 3)
                                                               .arg(meta->data.data3DFL.resolutionX, 3, 'f', 3)
                                                               .arg(meta->data.data3DFL.resolutionZ, 3, 'f', 3)));

            infoList.push_back(MakeInfo("FL Timeframes", QString::number(meta->data.data3DFL.dataCount)));

            if(1 < meta->data.data3DFL.dataCount) {
                infoList.push_back(MakeInfo("FL Interval (sec)",
                                            QString::number(meta->data.data3DFL.timeInterval, 'f', 3)));
            }
        }

        // Phasemap
        if (true == meta->data.data2D.exist) {
            infoList.push_back(MakeInfo("Phase Dimension (um)",
                                        QString("%1 X %2")
                                                .arg(QString::number(meta->data.data2D.resolutionX * meta->data.data2D.sizeX, 'f', 3))
                                                .arg(QString::number(meta->data.data2D.resolutionY * meta->data.data2D.sizeY, 'f', 3))));


            infoList.push_back(MakeInfo("Phase Resolution (um)",
                                        QString::number(meta->data.data2D.resolutionX, 'f', 3)));

            infoList.push_back(MakeInfo("Phase Frames", QString::number(meta->data.data2D.dataCount)));

            if (1 < meta->data.data2D.dataCount) {
                infoList.push_back(MakeInfo("Phase Interval (sec)", QString::number(meta->data.data2D.timeInterval, 'f', 3)));
            }
        }

        // MIP
        if (true == meta->data.data2DMIP.exist) {
            infoList.push_back(MakeInfo("RI(MIP) Dimension (um)",
                                        QString("%1 X %2")
                                                .arg(QString::number(meta->data.data2DMIP.resolutionX * meta->data.data2DMIP.sizeX, 'f', 3))
                                                .arg(QString::number(meta->data.data2DMIP.resolutionY * meta->data.data2DMIP.sizeY, 'f', 3))));


            infoList.push_back(MakeInfo("RI(MIP) Resolution (um)",
                                        QString::number(meta->data.data2DMIP.resolutionX, 'f', 3)));

            infoList.push_back(MakeInfo("RI(MIP) Frames", QString::number(meta->data.data2DMIP.dataCount)));

            if (1 < meta->data.data2DMIP.dataCount) {
                infoList.push_back(MakeInfo("RI(MIP) Interval (sec)", QString::number(meta->data.data2DMIP.timeInterval, 'f', 3)));
            }
        }

        // Annotation
        infoList.push_back(MakeInfo("Cell Type", QString("%1").arg(meta->info.annotation.cellType)));
        infoList.push_back(MakeInfo("TF Preset", QString("%1").arg(meta->info.annotation.tfPreset)));

        return true;
    }
}
