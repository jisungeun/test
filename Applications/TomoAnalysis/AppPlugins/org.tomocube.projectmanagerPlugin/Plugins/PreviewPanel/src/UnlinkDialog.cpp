#include <QMessageBox>
#include <QListView>
#include <QFileInfo>

#include "ui_UnlinkDialog.h"
#include "UnlinkDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct UnlinkDialog::Impl {
        Ui::UnlinkDialog* ui{ nullptr };
    };
    UnlinkDialog::UnlinkDialog(int tcfCnt,QStringList cubeList,QWidget* parent)
        :QDialog(parent), d{ new Impl }{
        d->ui = new Ui::UnlinkDialog;
        d->ui->setupUi(this);
        d->ui->tcfNum->setReadOnly(true);
        d->ui->tcfNum->setText(QString::number(tcfCnt));

        for (const auto& cube : cubeList) {
            d->ui->cubeList->addItem(cube);
        }

        Init();
    }
    UnlinkDialog::~UnlinkDialog() = default;
    auto UnlinkDialog::SetText(QString path)->void {
        QFileInfo info(path);
        QString newText = QString("Are you sure to unlink\n %1").arg(info.fileName());
        d->ui->tcfNum->hide();
        d->ui->label_2->setText("from");
        d->ui->label->setText(newText);
    }        
    void UnlinkDialog::OnUnlinkBtn() {
        if(d->ui->cubeList->count()<1) {
            QMessageBox::warning(nullptr, "Add Cube First", "No cube is added to project");
            return;
        }
        accept();
    }

    void UnlinkDialog::OnCancelBtn() {
        reject();
    }

    auto UnlinkDialog::Init()->void {
        d->ui->cubeList->setView(new QListView);

        connect(d->ui->unlinkBtn, SIGNAL(clicked()), this, SLOT(OnUnlinkBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));

        // set object names
        d->ui->label->setObjectName("label-title-dialog");
        d->ui->tcfNum->setObjectName("input-high");
        d->ui->label_2->setObjectName("h2");
        d->ui->cubeList->setObjectName("dropdown-high");
        d->ui->unlinkBtn->setObjectName("bt-square-primary");
        d->ui->cancelBtn->setObjectName("bt-square-line");
    }


    auto UnlinkDialog::GetCurrentCube() const -> QString {
        QString result;
        if(d->ui->cubeList->count() >0) {
            result = d->ui->cubeList->currentText();
        }
        return result;
    }

    auto UnlinkDialog::GetCubeName(int tcfCount, QStringList cubeList, QWidget* parent) -> QString {
        UnlinkDialog dialog(tcfCount, cubeList, parent);
        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }
        
        auto cubeName = dialog.GetCurrentCube();

        return cubeName;
    }


}