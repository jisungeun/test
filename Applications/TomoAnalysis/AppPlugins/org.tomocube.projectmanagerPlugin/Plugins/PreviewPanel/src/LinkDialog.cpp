#include <iostream>

#include <QFileInfo>
#include <QMessageBox>
#include <QListView>

#include "ui_LinkDialog.h"
#include "LinkDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct LinkDialog::Impl {
        Ui::LinkDialog* ui{ nullptr };
    };
    LinkDialog::LinkDialog(QStringList cubeList,QWidget* parent)
        :QDialog(parent), d{ new Impl }{
        d->ui = new Ui::LinkDialog;
        d->ui->setupUi(this);

        for (const auto& cube : cubeList) {
            d->ui->cubeCombo->addItem(cube);
        }

        Init();
    }
    LinkDialog::~LinkDialog() = default;

    void LinkDialog::OnLinkBtn() {
        if (d->ui->cubeCombo->count() < 1) {
            QMessageBox::warning(nullptr, "Add Cube First", "No cube is added to project");
            return;
        }
        accept();
    }
    auto LinkDialog::SetText(QString path)->void {
        QFileInfo info(path);        
        QString newText = QString("Link %1 to").arg(info.fileName());
        d->ui->label->setText(newText);
    }
    auto LinkDialog::Init() -> void {
        d->ui->cubeCombo->setView(new QListView);

        connect(d->ui->linkBtn, SIGNAL(clicked()), this, SLOT(OnLinkBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));

        // set object names
        d->ui->label->setObjectName("label-title-dialog");
        d->ui->label_2->setObjectName("h2");
        d->ui->cubeCombo->setObjectName("dropdown-high");
        d->ui->linkBtn->setObjectName("bt-square-primary");
        d->ui->cancelBtn->setObjectName("bt-square-line");
    }

    void LinkDialog::OnCancelBtn() {
        reject();
    }

    auto LinkDialog::GetCurrentCube() const -> QString {
        QString result;
        if(d->ui->cubeCombo->count()>0){
            result =  d->ui->cubeCombo->currentText();
        }
        return result;
    }

    auto LinkDialog::GetCubeName(QStringList cubeList, QWidget* parent) -> QString {
        LinkDialog dialog(cubeList,parent);
        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }
        auto cubeName = dialog.GetCurrentCube();                

        return cubeName;
    }
}