#include <iostream>
#include <QMessageBox>
#include <QFileInfo>
#include <QDesktopServices>
#include <QDir>

#include "ThumbnailPanel.h"
#include "DetailPanel.h"

#include "LinkDialog.h"
#include "UnlinkDialog.h"
#include "TCFInformationWidget.h"
#include "ResultListWidget.h"
#include "CilsPanel.h"

#include "ui_PreviewPanel.h"
#include "PreviewPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct PreviewPanel::Impl {
		Ui::PreviewPanel* ui{ nullptr };

		const int previewIndex = 0;
		const int detailIndex = 1;
		const int cilsIndex = 2;

		ThumbnailPanel* thumbnailPanel{ nullptr };
		DetailPanel* detailPanel{ nullptr };
		ResultListWidget* resultWidget{ nullptr };
		CilsPanel* cilsWidget{ nullptr };

		ProjectInfo::Pointer project;
		HyperCube::Pointer hypercube;
		Cube::Pointer cube;

		QMap<QString, TCFInformationWidget*> infos;

		enum Type {
			None = -1,
			Project = 0,
			Hypercube = 1,
			Cube = 2,
		};

		Type type = None;
	};

	PreviewPanel::PreviewPanel(QWidget* parent) :QWidget(parent), d{ new Impl } {
		d->ui = new Ui::PreviewPanel;
		d->ui->setupUi(this);

		d->thumbnailPanel = new ThumbnailPanel;
		d->detailPanel = new DetailPanel;
		d->resultWidget = new ResultListWidget;
		d->cilsWidget = new CilsPanel;

		d->ui->tabWidget->widget(d->previewIndex)->layout()->addWidget(d->thumbnailPanel);
		d->ui->tabWidget->widget(d->detailIndex)->layout()->addWidget(d->detailPanel);
		d->ui->tabWidget->widget(d->cilsIndex)->layout()->addWidget(d->cilsWidget);

		connect(d->detailPanel, SIGNAL(linkRequest(QStringList, QString, QString)),
			this, SLOT(onLinkRequested(QStringList, QString, QString)));
		connect(d->detailPanel, SIGNAL(unlinkRequest(QStringList, QString, QString)),
			this, SLOT(onUnlinkRequested(QStringList, QString, QString)));
		connect(d->detailPanel, SIGNAL(infoPanel(QString)), this, SLOT(OnDbClickInfo(QString)));
		connect(d->detailPanel, SIGNAL(openFolder(QString)), this, SLOT(OnOpenFolder(QString)));
		connect(d->detailPanel, SIGNAL(linkCube(QString)), this, SLOT(OnLinkCube(QString)));
		connect(d->detailPanel, SIGNAL(unlinkCube(QString)), this, SLOT(OnUnlinkCube(QString)));
		connect(d->detailPanel, SIGNAL(openViewer(QStringList)), this, SLOT(OnOpenViewer(QStringList)));

		connect(d->ui->resultButton, SIGNAL(clicked()), this, SLOT(OnResultClicked()));

		connect(d->resultWidget, SIGNAL(sigHistory(QString)), this, SLOT(OnHistory(QString)));
		connect(d->resultWidget, SIGNAL(sigReport(QString)), this, SLOT(OnReport(QString)));
		connect(d->resultWidget, SIGNAL(sigDelete(QString)), this, SLOT(OnDeleteHistory(QString)));

		connect(d->thumbnailPanel, SIGNAL(createSimple(QString)), this, SLOT(OnCreateSimple(QString)));
		connect(d->thumbnailPanel, SIGNAL(infoPanel(QString)), this, SLOT(OnDbClickInfo(QString)));
		connect(d->thumbnailPanel, SIGNAL(openFolder(QString)), this, SLOT(OnOpenFolder(QString)));
		connect(d->thumbnailPanel, SIGNAL(linkCube(QString)), this, SLOT(OnLinkCube(QString)));
		connect(d->thumbnailPanel, SIGNAL(unlinkCube(QString)), this, SLOT(OnUnlinkCube(QString)));
		connect(d->thumbnailPanel, SIGNAL(openViewer(QStringList)), this, SLOT(OnOpenViewer(QStringList)));

		connect(d->cilsWidget, SIGNAL(addTcf(QString)), this, SLOT(OnAddTcf(QString)));
		connect(d->cilsWidget, SIGNAL(linkTcfToWorkset(QString, QString, int)), this, SLOT(OnLinkTcfToWorkset(QString, QString, int)));

		connect(d->ui->seg2dButton, SIGNAL(clicked()), this, SLOT(OnSeg2dClicked()));

		// set object names
		d->ui->frame->setObjectName("panel-base");
		d->ui->preview->setObjectName("h6");
		d->ui->titleSeparatorLine->setObjectName("line-separator-title");
		d->ui->titleLabel->setObjectName("h4");
		d->ui->informationToolButton->setObjectName("bt-round-tool");
		d->ui->viewerToolButton->setObjectName("bt-round-tool");
		d->ui->resultSeparatorLine->setObjectName("line-separator-bt");
		d->ui->resultButton->setObjectName("bt-round-tool");
		d->ui->segToolButton->setObjectName("bt-round-tool");
		d->ui->seg2dButton->setObjectName("bt-round-tool");
		d->ui->cubeSeparatorLine->setObjectName("line-separator-bt");
		d->ui->linkToolButton->setObjectName("bt-round-tool");
		d->ui->unlinkToolButton->setObjectName("bt-round-tool");

		// set icons
		d->ui->informationToolButton->setIconSize(QSize(16, 16));
		d->ui->informationToolButton->AddIcon(":/img/ic-info.svg");
		d->ui->informationToolButton->AddIcon(":/img/ic-info-s.svg", QIcon::Selected);
		d->ui->informationToolButton->AddIcon(":/img/ic-info-s.svg", QIcon::Active);
		d->ui->informationToolButton->AddIcon(":/img/ic-info-d.svg", QIcon::Disabled);

		d->ui->viewerToolButton->setIconSize(QSize(16, 16));
		d->ui->viewerToolButton->AddIcon(":/img/ic-viewer.svg");
		d->ui->viewerToolButton->AddIcon(":/img/ic-viewer-s.svg", QIcon::Selected);
		d->ui->viewerToolButton->AddIcon(":/img/ic-viewer-s.svg", QIcon::Active);
		d->ui->viewerToolButton->AddIcon(":/img/ic-viewer-d.svg", QIcon::Disabled);

		d->ui->resultButton->setIconSize(QSize(16, 16));
		d->ui->resultButton->AddIcon(":/img/ic-result.svg");
		d->ui->resultButton->AddIcon(":/img/ic-result-s.svg", QIcon::Selected);
		d->ui->resultButton->AddIcon(":/img/ic-result-s.svg", QIcon::Active);
		d->ui->resultButton->AddIcon(":/img/ic-result-d.svg", QIcon::Disabled);

		d->ui->segToolButton->setIconSize(QSize(16, 16));
		d->ui->segToolButton->AddIcon(":/img/ic-mask.svg");
		d->ui->segToolButton->AddIcon(":/img/ic-mask-s.svg", QIcon::Selected);
		d->ui->segToolButton->AddIcon(":/img/ic-mask-s.svg", QIcon::Active);
		d->ui->segToolButton->AddIcon(":/img/ic-mask-d.svg", QIcon::Disabled);

		d->ui->seg2dButton->setIconSize(QSize(16, 16));
		d->ui->seg2dButton->AddIcon(":/img/ic-mask.svg");
		d->ui->seg2dButton->AddIcon(":/img/ic-mask-s.svg", QIcon::Selected);
		d->ui->seg2dButton->AddIcon(":/img/ic-mask-s.svg", QIcon::Active);
		d->ui->seg2dButton->AddIcon(":/img/ic-mask-d.svg", QIcon::Disabled);

		d->ui->linkToolButton->setIconSize(QSize(16, 16));
		d->ui->linkToolButton->AddIcon(":/img/ic-link.svg");
		d->ui->linkToolButton->AddIcon(":/img/ic-link-s.svg", QIcon::Selected);
		d->ui->linkToolButton->AddIcon(":/img/ic-link-s.svg", QIcon::Active);
		d->ui->linkToolButton->AddIcon(":/img/ic-link-d.svg", QIcon::Disabled);

		d->ui->unlinkToolButton->setIconSize(QSize(16, 16));
		d->ui->unlinkToolButton->AddIcon(":/img/ic-unlink.svg");
		d->ui->unlinkToolButton->AddIcon(":/img/ic-unlink-s.svg", QIcon::Selected);
		d->ui->unlinkToolButton->AddIcon(":/img/ic-unlink-s.svg", QIcon::Active);
		d->ui->unlinkToolButton->AddIcon(":/img/ic-unlink-d.svg", QIcon::Disabled);

		//d->ui->segToolButton->setDisabled(true);

		//For ETH
		d->ui->seg2dButton->hide();
		d->ui->viewerToolButton->setText("Image Viewer");

		//hide CILS Tab
		d->ui->tabWidget->removeTab(2);
	}

	PreviewPanel::~PreviewPanel() {

	}
	void PreviewPanel::OnInfoClosed(QString path) {
		auto send = dynamic_cast<TCFInformationWidget*>(sender());
		delete send;
		d->infos[path] = nullptr;
	}
	void PreviewPanel::OnOpenFolder(QString path) {
		QFileInfo info(path);
		auto dirPath = info.dir().path();
		QDesktopServices::openUrl(QUrl::fromLocalFile(dirPath));
	}
	void PreviewPanel::OnLinkCube(QString path) {
		QStringList paths;
		paths.push_back(path);
		QStringList cubes;
		const auto currentPlayground = d->project->GetCurrentPlayground();
		if (nullptr == currentPlayground) {
			return;
		}

		for (const auto& hypercube : currentPlayground->GetHyperCubeList()) {
			if (nullptr == hypercube) {
				continue;
			}

			for (const auto& cube : hypercube->GetCubeList()) {
				if (nullptr == cube) {
					continue;
				}
				cubes.push_back(cube->GetName());
			}
		}
		auto linkDialog = new LinkDialog(cubes);
		linkDialog->SetText(path);
		auto result = linkDialog->exec();
		if (result != QDialog::Accepted) {
			return;
		}
		const auto targetCube = linkDialog->GetCurrentCube();// LinkDialog::GetCubeName(cubes);
		if (true == targetCube.isEmpty()) {
			return;
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigLinkRequest(paths, targetCube, currentPlayground->GetPath());

		delete linkDialog;
	}
	void PreviewPanel::OnUnlinkCube(QString path) {
		QStringList paths;
		paths.push_back(path);
		QStringList cubes;
		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			cubes = d->thumbnailPanel->GetLinkedCubeNameList();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			cubes = d->detailPanel->GetLinkedCubeNameList();
		}

		if (true == paths.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to unlink");
			return;
		}

		if (true == cubes.isEmpty()) {
			return;
		}

		// get cube list;
		QStringList cubeList;
		const auto currentPlayground = d->project->GetCurrentPlayground();
		if (nullptr == currentPlayground) {
			return;
		}

		for (const auto& hypercube : currentPlayground->GetHyperCubeList()) {
			if (nullptr == hypercube) {
				continue;
			}

			for (const auto& cube : hypercube->GetCubeList()) {
				if (nullptr == cube) {
					continue;
				}
				auto isCube = false;
				for (auto p : paths) {
					if (nullptr != cube->FindTCFDir(p)) {
						isCube = true;
						break;
					}
				}
				if (isCube) {
					cubeList.push_back(cube->GetName());
				}
			}
		}
		auto unlinkDialog = new UnlinkDialog(1, cubeList);
		unlinkDialog->SetText(path);
		auto result = unlinkDialog->exec();
		if (result != QDialog::Accepted) {
			return;
		}
		auto targetCube = unlinkDialog->GetCurrentCube();
		if (true == targetCube.isEmpty()) {
			return;
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigUnlinkRequest(paths, targetCube, currentPlayground->GetPath());
	}
	void PreviewPanel::OnOpenViewer(QStringList path) {
		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		TC::IO::TCFMetaReader* reader = new TC::IO::TCFMetaReader;
		auto arrSize = reader->ReadArrSize(path[0]);

		if (arrSize >= INT_MAX) {
			auto isLdm = reader->ReadIsLDM(path[0]);
			if (false == isLdm) {
				QMessageBox::warning(nullptr, "Error", "Convert tcf file into LDM format using TomoStitcher!");
				return;
			}
		}

		auto proj_path = QString();
		if (d->project) {
			proj_path = d->project->GetPath();
		}

		emit sigViewer(path, proj_path);
	}
	void PreviewPanel::OnDbClickInfo(QString path) {
		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to show information");
			return;
		}

		/*if (d->infos.contains(path)) {
			d->infos[path]->hide();
			d->infos[path]->show();
		}
		else {*/
		auto infoWidget = new TCFInformationWidget;
		infoWidget->SetFile(path);
		infoWidget->show();
		connect(infoWidget, SIGNAL(infoClosed(QString)), this, SLOT(OnInfoClosed(QString)));

		d->infos[path] = infoWidget;
		//}
		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();
	}
	auto PreviewPanel::ClosePopup() -> void {
		d->resultWidget->close();
		for (auto info : d->infos) {
			if (info) {
				info->close();
			}
		}
	}
	void PreviewPanel::OnCreateSimple(QString tcfPath) {
		emit sigCreateSimple(tcfPath);
	}

	void PreviewPanel::OnAddTcf(QString path) {
		emit sigAddTcf(d->project->GetPath(), path);
	}

	void PreviewPanel::OnLinkTcfToWorkset(QString path, QString worksetName, int worksetId) {
		emit sigLinkTcfToWorkset(d->project->GetPath(), path, worksetName, worksetId);
	}

	void PreviewPanel::OnDeleteHistory(QString history) {
		emit sigDeleteHistory(history);
	}
	void PreviewPanel::OnHistory(QString history) {
		emit sigLoadHistory(history);
	}
	void PreviewPanel::OnReport(QString report) {
		emit sigLoadReport(report);
	}
	auto PreviewPanel::FlushSelection() -> void {
		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();
	}

	auto PreviewPanel::Update(const ProjectInfo::Pointer& info) -> bool {
		// clear
		d->thumbnailPanel->Clear();
		d->detailPanel->Clear();

		if (nullptr == info) {
			return false;
		}

		auto pg = info->GetCurrentPlayground();
		if (nullptr == pg) {
			d->ui->titleLabel->setText(info->GetName());
		}
		else {
			d->ui->titleLabel->setText(pg->GetName());
		}

		d->thumbnailPanel->ShowProject(info);
		d->detailPanel->Init(info);

		d->project = info;

		d->type = Impl::Project;
		d->hypercube = nullptr;
		d->cube = nullptr;

		d->resultWidget->Update(info);

		return true;
	}

	auto PreviewPanel::UpdateLink(const ProjectInfo::Pointer& info)->bool {
		if (nullptr == info) {
			return false;
		}

		if (d->type == Impl::Project) {
			d->thumbnailPanel->UpdateLink(info);
			d->detailPanel->UpdateLink(info);
		}
		else if (d->type == Impl::Hypercube) {
			d->thumbnailPanel->ShowHypercube(d->hypercube, info);
			d->detailPanel->Update(info);
		}
		else if (d->type == Impl::Cube) {
			d->thumbnailPanel->ShowCube(d->cube, info);
			d->detailPanel->Update(info);
		}

		auto pg = info->GetCurrentPlayground();
		d->ui->titleLabel->setText(pg->GetName());

		d->resultWidget->Update(info);

		d->project = info;

		return true;
	}

	auto PreviewPanel::UpdateWithCurrentInfo() -> void {
		if (nullptr == d->project) {
			return;
		}
		d->resultWidget->Update(d->project, true);
	}
	auto PreviewPanel::UpdateAfterDelete() -> void {
		if (nullptr == d->project) {
			return;
		}
		d->resultWidget->Update(d->project, false, true);
	}

	auto PreviewPanel::UpdateCils(const QMap<int, QStringList>& items, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>& images) -> void {
		d->cilsWidget->UpdateCils(items, worksets, images);
	}

	void PreviewPanel::OnResultClicked() {
		d->resultWidget->hide();
		d->resultWidget->show();
		d->resultWidget->raise();
	}
	auto PreviewPanel::AddItem(const TCFDir::Pointer& dir)->bool {
		if (nullptr == dir) {
			return false;
		}

		d->thumbnailPanel->AddItem(dir);
		d->detailPanel->AddItem(dir);

		return true;
	}

	void PreviewPanel::on_informationToolButton_clicked() const {
		QString path;
		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			path = d->thumbnailPanel->GetLastSelectedTCF();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			path = d->detailPanel->GetLastSelectedTCF();
		}

		if (true == path.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to show information");
			return;
		}

		/*if (d->infos.contains(path)) {
			d->infos[path]->hide();
			d->infos[path]->show();
		} else {*/
		auto infoWidget = new TCFInformationWidget;
		infoWidget->SetFile(path);
		infoWidget->show();
		connect(infoWidget, SIGNAL(infoClosed(QString)), this, SLOT(OnInfoClosed(QString)));
		d->infos[path] = infoWidget;
		//}
		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();
	}
	void PreviewPanel::sqOnLinkTab(bool isLink) {
		if (isLink) {
			d->ui->linkToolButton->setStyleSheet("background-color: rgb(91, 139, 151);");
			d->ui->unlinkToolButton->setStyleSheet("background-color: rgb(91, 139, 151);");
		}
		else {
			d->ui->linkToolButton->setStyleSheet("");
			d->ui->unlinkToolButton->setStyleSheet("");
		}
	}

	void PreviewPanel::on_viewerToolButton_clicked() {
		QStringList path;
		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			path = d->thumbnailPanel->GetSelectedTCFList();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			path = d->detailPanel->GetSelectedIncludedTCFList();
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

        TC::IO::TCFMetaReader reader;
		for (auto p : path) {
			auto arrSize = reader.ReadArrSize(p);
			if (arrSize >= INT_MAX) {
				auto isLdm = reader.ReadIsLDM(p);
				if (false == isLdm) {
					QMessageBox::warning(nullptr, "Error", "Convert tcf file into LDM format using TomoStitcher!");
					return;
				}
			}
		}

		auto proj_path = QString();
		if (d->project) {
			proj_path = d->project->GetPath();
		}

		emit sigViewer(path, proj_path);
	}

	void PreviewPanel::OnSeg2dClicked() {
		QString tcfPath;

		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			tcfPath = d->thumbnailPanel->GetLastSelectedTCF();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			tcfPath = d->detailPanel->GetLastSelectedTCF();
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigSegmentation2D(tcfPath);
	}

	void PreviewPanel::on_segToolButton_clicked() {
		QString tcfPath;

		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			tcfPath = d->thumbnailPanel->GetLastSelectedTCF();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			tcfPath = d->detailPanel->GetLastSelectedTCF();
		}

		/*
		if (tcfPath.isEmpty()) {
			return;
		}*/

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigSegmentation(tcfPath);
	}

	void PreviewPanel::on_linkToolButton_clicked() {
		QStringList paths;
		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			paths = d->thumbnailPanel->GetSelectedTCFList();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			paths = d->detailPanel->GetSelectedContextsTCFList();
		}


		if (true == paths.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to link");
			return;
		}

		// get cube list;
		QStringList cubes;
		const auto currentPlayground = d->project->GetCurrentPlayground();
		if (nullptr == currentPlayground) {
			return;
		}

		for (const auto& hypercube : currentPlayground->GetHyperCubeList()) {
			if (nullptr == hypercube) {
				continue;
			}

			for (const auto& cube : hypercube->GetCubeList()) {
				if (nullptr == cube) {
					continue;
				}
				cubes.push_back(cube->GetName());
			}
		}

		const auto targetCube = LinkDialog::GetCubeName(cubes);
		if (true == targetCube.isEmpty()) {
			return;
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigLinkRequest(paths, targetCube, currentPlayground->GetPath());
	}

	void PreviewPanel::on_unlinkToolButton_clicked() {
		QStringList paths;
		QStringList cubes;
		if (d->ui->tabWidget->currentIndex() == d->previewIndex) {
			paths = d->thumbnailPanel->GetSelectedTCFList();
			cubes = d->thumbnailPanel->GetLinkedCubeNameList();
		}
		else if (d->ui->tabWidget->currentIndex() == d->detailIndex) {
			paths = d->detailPanel->GetSelectedIncludedTCFList();
			cubes = d->detailPanel->GetLinkedCubeNameList();
		}

		if (true == paths.isEmpty()) {
			QMessageBox::warning(nullptr, "Error", "Select tcf files to unlink");
			return;
		}

		if (true == cubes.isEmpty()) {
			return;
		}

		// get cube list;
		QStringList cubeList;
		const auto currentPlayground = d->project->GetCurrentPlayground();
		if (nullptr == currentPlayground) {
			return;
		}

		for (const auto& hypercube : currentPlayground->GetHyperCubeList()) {
			if (nullptr == hypercube) {
				continue;
			}

			for (const auto& cube : hypercube->GetCubeList()) {
				if (nullptr == cube) {
					continue;
				}
				auto isCube = false;
				for (auto p : paths) {
					if (nullptr != cube->FindTCFDir(p)) {
						isCube = true;
						break;
					}
				}
				if (isCube) {
					cubeList.push_back(cube->GetName());
				}
			}
		}

		auto targetCube = UnlinkDialog::GetCubeName(paths.count(), cubeList);
		if (true == targetCube.isEmpty()) {
			return;
		}

		d->thumbnailPanel->ClearSelection();
		d->detailPanel->ClearSelection();

		emit sigUnlinkRequest(paths, targetCube, currentPlayground->GetPath());
	}

	void PreviewPanel::onPlaygroundPanelItemChanged(QString info, QString playgroundPath) {
		if (nullptr == d->project) {
			return;
		}

		const auto playground = d->project->FindPlaygroundInfo(playgroundPath);
		if (nullptr == playground) {
			return;
		}

		if (info.contains("HyperCube!")) {
			const auto name = info.split("!")[1];
			const auto hypercube = playground->FindHypercube(name);
			if (nullptr == hypercube) {
				return;
			}

			d->ui->titleLabel->setText(hypercube->GetName());
			d->thumbnailPanel->ShowHypercube(hypercube, d->project);

			d->type = Impl::Hypercube;
			d->hypercube = hypercube;
			d->cube = nullptr;
		}
		else if (info.contains("Cube!")) {
			const auto name = info.split("!")[1];
			const auto cube = playground->FindCube(name);
			if (nullptr == cube) {
				return;
			}

			d->ui->titleLabel->setText(cube->GetName());
			d->thumbnailPanel->ShowCube(cube, d->project);

			d->type = Impl::Cube;

			d->hypercube = nullptr;
			d->cube = cube;
		}
		else {
			if (d->type != Impl::Project) {
				d->ui->titleLabel->setText(d->project->GetName());
				Update(d->project);
				d->type = Impl::Project;

				d->hypercube = nullptr;
				d->cube = nullptr;
			}
		}
	}

	void PreviewPanel::onLinkRequested(QStringList tcfList, QString cube, QString playground) {
		if (true == tcfList.isEmpty()) {
			return;
		}

		if (true == cube.isEmpty()) {
			return;
		}

		if (true == playground.isEmpty()) {
			return;
		}

		emit sigLinkRequest(tcfList, cube, playground);
	}

	void PreviewPanel::onUnlinkRequested(QStringList tcfList, QString cube, QString playground) {
		if (true == tcfList.isEmpty()) {
			return;
		}

		if (true == cube.isEmpty()) {
			return;
		}

		if (true == playground.isEmpty()) {
			return;
		}

		emit sigUnlinkRequest(tcfList, cube, playground);
	}
}
