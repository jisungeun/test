#define LOGGER_TAG "[Project Manager]"
#include <TCLogger.h>

#include <iostream>
#include <QDir>
#include <QJsonDocument>
#include <QJsonArray>

#include <PlaygroundInfo.h>
#include <IParameter.h>
#include <TCParameterReader.h>
#include <PluginRegistry.h>

#include "DeleteDialog.h"
#include "ui_ResultListWidget.h"
#include "ResultListWidget.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    typedef struct historyNode{
        IParameter::Pointer param;
        QString appName;
        QString procName;        
    }hNode;    
    struct ResultListWidget::Impl {        
        auto MakeTCFDirList(const QJsonArray& array) const ->TCFDir::List {
            TCFDir::List list;            
            for (const auto& item : array) {
                auto path = item["path"];
                QDir dir(path.toString());
                QFileInfo fileInfo(path.toString());
                if (false == dir.exists() && false == fileInfo.exists()) {
                    continue;
                }

                auto tcfDir = std::make_shared<TCFDir>();
                tcfDir->SetPath(path.toString());                                

                auto posX = item["posX"].toInt();
                auto posY = item["posY"].toInt();
                tcfDir->SetPosition(QPoint(posX, posY));

                list.append(tcfDir);
            }

            return list;
        }
        auto MakeProcessorInfoList(const QJsonArray& array) const -> ProcessorInfo::List {
            ProcessorInfo::List list;
            for (const auto& item : array) {
                auto proc = std::make_shared<ProcessorInfo>();
                proc->SetName(item["name"].toString());
                proc->SetPath(item["path"].toString());

                QMap<QString, QString> paramterList;
                auto parameters = item["paramters"].toObject();

                for (const auto& key : parameters.keys()) {
                    paramterList[key] = parameters[key].toString();
                }
                proc->SetParameterList(paramterList);
                list.append(proc);
            }
            return list;
        }
        auto MakeMeasurementAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List {
            PluginAppInfo::List list;

            for (const auto& item : array) {
                auto app = std::make_shared<PluginAppInfo>();
                app->SetName(item["name"].toString());
                app->SetPath(item["path"].toString());
                auto posX = item["posX"].toInt();
                auto posY = item["posY"].toInt();
                app->SetPosition(QPoint(posX, posY));

                //processor
                auto procList = this->MakeProcessorInfoList(item["processors"].toArray());
                app->SetProcessors(procList);

                list.append(app);
            }

            return list;
        }
        auto MakeReportAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List {
            PluginAppInfo::List list;

            for (auto item : array) {
                auto app = std::make_shared<PluginAppInfo>();
                app->SetName(item["name"].toString());

                // parameter
                QMap<QString, QString> parameterList;
                auto parameters = item["parameters"].toObject();

                for (auto key : parameters.keys()) {
                    parameterList[key] = parameters[key].toString();
                }

                //app->GetProcessors().at(0)->SetParameterList(parameterList);
                list.append(app);
            }

            return list;
        }
        auto MakeMaskInfoList(const QJsonArray& array) const ->MaskInfo::List {
            MaskInfo::List list;

            for (const auto& item : array) {
                auto mask = std::make_shared<MaskInfo>();
                mask->SetReferenceTCFPath(item["referencePath"].toString());
                mask->SetMaskTCFPath(item["maskPath"].toString());
                mask->SetInterpretation(item["interpretation"].toString());

                list.append(mask);
            }

            return list;
        }
        auto MakeResultInfoList(const QJsonArray& array) const ->ResultInfo::List {
            ResultInfo::List list;

            for (const auto& item : array) {
                auto result = std::make_shared<ResultInfo>();
                result->SetPath(item["rawDataPath"].toString());
                result->SetMaskInfoList(this->MakeMaskInfoList(item["masks"].toArray()));
                result->SetReportAppInfoList(this->MakeReportAppInfoList(item["reportApps"].toArray()));

                list.append(result);
            }

            return list;
        }

        auto MakeCubeInfoList(const QJsonArray& array) ->Cube::List {
            Cube::List list;
            for (const auto& item : array) {                
                auto cube = std::make_shared<Cube>();
                cube->SetID(item["id"].toInt(-1));
                cube->SetName(item["name"].toString());
                auto posX = item["posX"].toInt();
                auto posY = item["posY"].toInt();
                cube->SetPosition(QPoint(posX, posY));
                cube->SetTCFDirList(MakeTCFDirList(item["tcfDirectorys"].toArray()));
                cube->SetMeasurementAppInfoList(MakeMeasurementAppInfoList(item["apps"].toArray()));
                cube->SetResultInfoList(this->MakeResultInfoList(item["results"].toArray()));

                list.append(cube);
            }
            return list;
        }
        auto MakeHypercubeInfoList(const QJsonArray& array, const Cube::List& cubeList)->HyperCube::List {
            Q_UNUSED(cubeList)
            auto FindCube = [=](const Cube::ID& id, const Cube::List& cubes) {
                for (auto cube : cubes) {
                    if (id == cube->GetID()) {
                        return cube;
                    }
                }

                return std::make_shared<Cube>();
            };

            auto MakeCubeIdList = [=](const QJsonArray& array, const Cube::List& cubes) {
                Cube::List list;

                for (const auto& item : array) {
                    auto cube = FindCube(item.toInt(-1), cubes);
                    if (nullptr == cube) {
                        continue;
                    }

                    list.append(cube);
                }
                return list;
            };


            HyperCube::List list;

            for (const auto& item : array) {
                auto hypercube = std::make_shared<HyperCube>();
                hypercube->SetName(item["name"].toString());
                auto posX = item["posX"].toInt();
                auto posY = item["posY"].toInt();
                hypercube->SetPosition(QPoint(posX, posY));
                //hypercube->SetCubeList(MakeCubeIdList(item["cubes"].toArray(), cubeList));
                hypercube->SetCubeList(this->MakeCubeInfoList(item["cubes"].toArray()));
                hypercube->SetMeasurementAppInfoList(this->MakeMeasurementAppInfoList(item["apps"].toArray()));
                hypercube->SetResultInfoList(this->MakeResultInfoList(item["results"].toArray()));

                list.append(hypercube);
            }

            return list;
        }
        Ui::ResultListWidget* ui{nullptr};

        QStringList TableHeader;        
        QString cur_pg_path;
        QMap<QString, QMap<QString, hNode>> params;
        int curRow{-1};
    };
    ResultListWidget::ResultListWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ResultListWidget;
        d->ui->setupUi(this);

        this->setWindowTitle("Result History");
        this->setMinimumSize(1200, 800);

        //Init UIs
        d->ui->appParam->SetHideButtons(true);        

        d->TableHeader <<"Application" << "Processing mode" << "Date" << "Hypercube";
        d->ui->resultTable->setColumnCount(4);
        d->ui->resultTable->setHorizontalHeaderLabels(d->TableHeader);        
        d->ui->resultTable->verticalHeader()->setVisible(true);
        d->ui->resultTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->ui->resultTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        d->ui->resultTable->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui->resultTable->setShowGrid(true);
        d->ui->resultTable->setProperty("styleVariant",1);        


        connect(d->ui->resultTable, SIGNAL(itemSelectionChanged()), this, SLOT(OnSelectionChanged()));

        connect(d->ui->applyPGButton, SIGNAL(clicked()), this, SLOT(OnApplyButton()));
        connect(d->ui->reportButton, SIGNAL(clicked()), this, SLOT(OnReportButton()));
        connect(d->ui->deleteButton, SIGNAL(clicked()), this, SLOT(OnDeleteButton()));

        // set object names
        setObjectName("widget-popup");
        d->ui->scrollAreaWidgetContents->setObjectName("panel-contents");
        d->ui->applyPGButton->setObjectName("bt-square-gray");
        d->ui->reportButton->setObjectName("bt-square-gray");
        d->ui->deleteButton->setObjectName("bt-square-gray");
    }
    ResultListWidget::~ResultListWidget() {
        delete d->ui;
    }
    void ResultListWidget::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }
    auto ResultListWidget::CallLastReport() -> void {
        auto hyperNames = d->params.keys();
        if(hyperNames.size()<1) {
            std::cout << "hyperName smaller than 1" << std::endl;
            return;
        }
        QString lastHyper = hyperNames[0];
        QString lastTimeStamp;
        double max = -1;
        for(auto h : hyperNames) {
            auto timestamps = d->params[h].keys();
            for(auto t: timestamps) {
                if(max<0 || max < t.toDouble()) {
                    max = t.toDouble();
                    lastTimeStamp = t;
                    lastHyper = h;
                }
            }            
        }
        auto reportFolder = d->cur_pg_path.chopped(5);

        QDir hDir(reportFolder);
        hDir.cdUp();//playground
        hDir.cd("history");

        reportFolder = hDir.path() + "/";
                
        reportFolder += lastHyper;
        reportFolder += "/";                
        reportFolder += lastTimeStamp;

        emit sigReport(reportFolder);
    }
    void ResultListWidget::OnApplyButton() {        
        auto row = d->ui->resultTable->currentRow();
        if (row < 0)
            return;
        auto historyPath = d->cur_pg_path.chopped(5);
        QDir hDir(historyPath);
        hDir.cdUp();
        hDir.cd("history");
        historyPath = hDir.path();
        historyPath += "/";

        auto hyper_name = d->ui->resultTable->item(row, 3)->text();
        historyPath += hyper_name;
        historyPath += "_";

        auto timeStamp = d->ui->resultTable->item(row, 2)->text();
        historyPath += timeStamp;
        historyPath += ".tcpg";

        emit sigHistory(historyPath);       
    }

    void ResultListWidget::OnDeleteButton() {
        auto row = d->ui->resultTable->currentRow();
        if(row<0) {
            return;
        }
        auto reportFolder = d->cur_pg_path.chopped(5);
        QDir hDir(reportFolder);
        auto pgName = hDir.dirName();
        hDir.cdUp();//playground
        hDir.cdUp();
        auto projectName = hDir.dirName();
        
        hDir.cd("playground");
        hDir.cd("history");

        reportFolder = hDir.path() + "/";

        auto hypercube = d->ui->resultTable->item(row, 3)->text();
        reportFolder += hypercube;
        reportFolder += "/";

        auto timeStamp = d->ui->resultTable->item(row, 2)->text();
        reportFolder += timeStamp;
                
        if(true == DeleteDialog::Delete(nullptr, projectName,pgName,hypercube,timeStamp)) {
            emit sigDelete(reportFolder);
        }        
    }

    void ResultListWidget::OnReportButton() {
        auto row = d->ui->resultTable->currentRow();
        if (row < 0)
            return;
        auto reportFolder = d->cur_pg_path.chopped(5);

        QDir hDir(reportFolder);
        hDir.cdUp();//playground
        hDir.cd("history");

        reportFolder = hDir.path() + "/";

        auto hypercube = d->ui->resultTable->item(row, 3)->text();
        reportFolder += hypercube;
        reportFolder += "/";

        auto timeStamp = d->ui->resultTable->item(row, 2)->text();
        reportFolder += timeStamp;

        emit sigReport(reportFolder);
    }


    void ResultListWidget::OnSelectionChanged() {
        auto rowIdx = d->ui->resultTable->currentRow();
        if (rowIdx < 0)
            return;
        if (d->curRow < 0 || d->curRow != rowIdx) {
            auto appName = d->ui->resultTable->item(rowIdx, 0)->text();
            auto procName = d->ui->resultTable->item(rowIdx, 1)->text();
            auto timeStamp = d->ui->resultTable->item(rowIdx, 2)->text();
            auto hyperName = d->ui->resultTable->item(rowIdx, 3)->text();

            //load playground and apply
            auto parentFolder = d->cur_pg_path.chopped(5);
            QDir pdir(parentFolder);
            pdir.cdUp();
            pdir.cd("history");            
            auto targetPGpath = pdir.path() + "/" + hyperName + "_" + timeStamp + ".tcpg";

            QFile loadFile(targetPGpath);
            if(false == loadFile.open(QIODevice::ReadOnly)) {
                QLOG_ERROR() << "Cannot load " << targetPGpath;                
                return;
            }
            const QByteArray bdata = loadFile.readAll();
            const  QJsonDocument doc(QJsonDocument::fromJson(bdata));
            auto object = doc.object();

            auto targetPG = std::make_shared<PlaygroundInfo>();
            targetPG->SetPath(targetPGpath);
            targetPG->SetName(object["name"].toString());
            targetPG->SetCubeList(d->MakeCubeInfoList(object["cubes"].toArray()));
            targetPG->SetHyperCubeList(d->MakeHypercubeInfoList(object["hypercubes"].toArray(),
                targetPG->GetCubeList()));

            const auto tcfDirectorys = object["tcfDirectorys"].toArray();
            targetPG->SetTCFDirList(d->MakeTCFDirList(tcfDirectorys));
            const auto appPaths = object["applicationPaths"].toArray();
            targetPG->SetAppList(d->MakeMeasurementAppInfoList(appPaths));                        

            d->ui->playground->Update(targetPG);

            auto node = d->params[hyperName][timeStamp];
            auto param = node.param;                        
                        
            d->ui->appParam->InitParameter(param,node.procName);
            d->ui->appParam->SetApplicationName(node.appName);
            d->ui->appParam->SetCurrentProcessor(node.procName,true);
            d->ui->appParam->SetParameter(param);            
            d->curRow = rowIdx;
        }
    }
    auto ResultListWidget::Update(ProjectInfo::Pointer proj,bool force,bool afterDelete) -> void {
        auto pg = proj->GetCurrentPlayground();
        if(nullptr == pg) {
            return;
        }        
        auto pg_path = pg->GetPath();
        if (d->cur_pg_path.compare(pg_path) == 0 && !force && !afterDelete) {
            return;
        }
        d->params.clear();
        d->ui->playground->Reset();
        d->ui->appParam->Reset();
        d->curRow = -1;

        d->cur_pg_path = pg_path;
        d->ui->playground->UpdateName(pg);
        //force load related processors
        for(auto hyper : pg->GetHyperCubeList()) {
            for(auto appInfo : hyper->GetMeasurementAppInfoList()) {
                for(auto pro : appInfo->GetProcessors()) {
                    PluginRegistry::LoadPlugin(pro->GetPath());
                }
            }
        }

        ParseResultList(pg);
        RefreshTable();
        if(force){
            CallLastReport();
        }
    }
    auto ResultListWidget::RefreshTable() -> void {
        d->ui->resultTable->blockSignals(true);
        d->ui->resultTable->clearContents();

        auto rowCnt = 0;
        auto hypers = d->params.keys();
        for(auto h: hypers) {
            auto stamps = d->params[h].keys();
            rowCnt += stamps.size();
        }
        d->ui->resultTable->setRowCount(rowCnt);

        auto rowIdx = 0;
        for(auto h: hypers) {
            auto paramSet = d->params[h];
            auto stamps = paramSet.keys();
            for(auto s:stamps) {
                auto param = paramSet[s];                
                //Applicatoin                
                d->ui->resultTable->setItem(rowIdx, 0, new QTableWidgetItem(param.appName));
                //Processor
                auto real_proc = param.procName;                
                IPluginModule::Pointer processor = PluginRegistry::GetPlugin(real_proc);
                if (nullptr != processor) {                    
                    real_proc = processor->GetName();                    
                }
                d->ui->resultTable->setItem(rowIdx, 1, new QTableWidgetItem(real_proc));
                //Time stamp
                d->ui->resultTable->setItem(rowIdx, 2, new QTableWidgetItem(s));
                //hypercube
                d->ui->resultTable->setItem(rowIdx, 3, new QTableWidgetItem(h));
                rowIdx++;
            }
        }
        d->ui->resultTable->blockSignals(false);        
        if(rowIdx>0) {            
            d->ui->resultTable->selectRow(0);
        }

        d->ui->resultTable->resizeColumnsToContents();
    }
    auto ResultListWidget::ParseResultList(PlaygroundInfo::Pointer pg) -> void {
        auto playground_path = pg->GetPath();
        auto parentPath = playground_path.chopped(5);
        QDir pdir(parentPath);
        pdir.cdUp();
        pdir.cd("history");
        if(false == pdir.exists()) {
            return;
        }
        /*auto hyperList = pg->GetHyperCubeList();
        QStringList hyperNames;
        for(auto hyper :hyperList) {
            hyperNames.push_back(hyper->GetName());
        }
        if(hyperNames.size()<1) {
            return;
        }*/
        QStringList paramList = pdir.entryList(QStringList() << "*.param" << "*.PARAM", QDir::Files);        

        for(auto p : paramList) {
            auto fileName = p.chopped(6);
            //if (CheckHyper(fileName, hyperNames))
                //continue;
            auto split = fileName.split("_");
            auto timeStamp = split[split.size() - 1];
            QString hyperName;
            for(auto s =0;s<split.size()-1;s++) {                
                hyperName += split[s];
                if(s <split.size()-2) {
                    hyperName +="_";
                }
            }
            auto reader = std::make_shared<TC::IO::TCParameterReader>(pdir.path(),true);
            auto tuple = reader->ReadHistory(hyperName, timeStamp);

            hNode node;
            node.appName = std::get<0>(tuple);
            node.procName = std::get<1>(tuple);
            node.param = std::get<3>(tuple);
                        
            if(d->params.contains(hyperName)) {
                d->params[hyperName][timeStamp] = node;
            }else {
                QMap<QString, hNode> paramPerHyper;
                paramPerHyper[timeStamp] = node;
                d->params[hyperName] = paramPerHyper;
            }
        }
    }
    auto ResultListWidget::CheckHyper(QString candidate, QStringList list) -> bool {
        for (auto l : list) {
            if(l.compare(candidate)==0) {
                return true;
            }
        }
        return false;
    }

}
