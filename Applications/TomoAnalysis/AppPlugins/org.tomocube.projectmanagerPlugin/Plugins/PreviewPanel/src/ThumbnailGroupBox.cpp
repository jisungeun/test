#include "ui_ThumbnailGroupBox.h"
#include "ThumbnailGroupBox.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct ThumbnailGroupBox::Impl {
        Ui::ThumbnailGroupBox* ui = nullptr;

        QWidget* parent = nullptr;
        ThumbnailGroupBoxType type = None;

        QList<LinkableThumbnailWidget*> children;
    };

    ThumbnailGroupBox::ThumbnailGroupBox(const ThumbnailGroupBoxType& type,
                                         const QString& text, QWidget* parent)
        : QWidget(parent)
        , d(new Impl()) {
        d->ui = new Ui::ThumbnailGroupBox;
        d->ui->setupUi(this);

        d->parent = parent;
        d->type = type;

        d->ui->titleButton->setProperty("type", d->type);
        d->ui->titleButton->setText(text);

        auto gridLayout = new QGridLayout;
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setSpacing(10);

        d->ui->frame->setLayout(gridLayout);

        // set object names
        d->ui->titleButton->setObjectName("bt-title-thumbnail");
        d->ui->line->setObjectName("line-separator-section");

        // set icons
        d->ui->titleButton->setIconSize(QSize(16, 16));
        switch(d->type) {
        case ThumbnailGroupBoxType::Project:
            d->ui->titleButton->AddIcon(":/img/ic-workset-folder-2.svg");
            break;
        case ThumbnailGroupBoxType::Cube:
            d->ui->titleButton->AddIcon(":/img/ic-explorer-cube.svg");
            break;
        case ThumbnailGroupBoxType::None:
        default:
            break;
        }
    }

    ThumbnailGroupBox::~ThumbnailGroupBox() {

    }

    auto ThumbnailGroupBox::Text() const->QString {
        return d->ui->titleButton->text();
    }

    void ThumbnailGroupBox::SetText(const QString& text) const {
        d->ui->titleButton->setText(text);
    }

    void ThumbnailGroupBox::SetList(const QStringList& pathList) const {
        if (true == pathList.isEmpty()) {
            return;
        }

        auto layout = static_cast<QGridLayout*>(d->ui->frame->layout());
        if (nullptr == layout) {
            auto gridLayout = new QGridLayout;
            gridLayout->setContentsMargins(0, 0, 0, 0);
            gridLayout->setSpacing(10);

            d->ui->frame->setLayout(gridLayout);
        }

        for(auto i = 0; i < pathList.size(); ++i) {
            auto imageWidget = new LinkableThumbnailWidget;
            if(false == imageWidget->SetFile(pathList.at(i))) {
                continue;
            }

            imageWidget->installEventFilter(d->parent);

            d->children.append(imageWidget);

            if (layout != nullptr) {
                layout->addWidget(imageWidget, i / 6, i % 6);
                layout->setRowStretch(i / 6, 1);
                layout->setColumnStretch(i % 6, 1);
            }
        }

        if (0 != pathList.size() % 6) {
            for (auto i = pathList.size() % 6; i < 6; ++i) {
                const auto imageWidget = new LinkableThumbnailWidget;

                if (layout != nullptr) {
                    layout->addWidget(imageWidget, pathList.size() / 6, i % 6);
                    layout->setRowStretch(pathList.size() / 6, 1);
                    layout->setColumnStretch(i % 6, 1);
                }
            }
        }
    }

    void ThumbnailGroupBox::SetChildren(QList<LinkableThumbnailWidget*> children) const {
        d->children = children;

        auto layout = static_cast<QGridLayout*>(d->ui->frame->layout());
        if (nullptr == layout) {
            auto gridLayout = new QGridLayout;
            gridLayout->setContentsMargins(0, 0, 0, 0);
            gridLayout->setSpacing(10);

            d->ui->frame->setLayout(gridLayout);
        }

        for (auto i = 0; i < children.size(); ++i) {
            if (layout != nullptr) {
                layout->addWidget(children.at(i), i / 6, i % 6);
                layout->setRowStretch(i / 6, 1);
                layout->setColumnStretch(i % 6, 1);
            }
        }

        if (0 != children.size() % 6) {
            for (auto i = children.size() % 6; i < 6; ++i) {
                const auto imageWidget = new LinkableThumbnailWidget;
                if (layout != nullptr) {
                    layout->addWidget(imageWidget, children.size() / 6, i);
                    layout->setRowStretch(children.size() / 6, 1);
                    layout->setColumnStretch(i % 6, 1);
                }
            }
        }

        //if(d->type == Cube) {
        //    if(false == d->children.isEmpty()) {
        //        QIcon icon;
        //        icon.addPixmap(QPixmap(QString::fromUtf8(":/image/images/CubeFill.png")), QIcon::Normal, QIcon::Off);
        //        icon.addPixmap(QPixmap(QString::fromUtf8(":/image/images/CubeFill.png")), QIcon::Active, QIcon::Off);

        //        d->ui->titleButton->setIcon(icon);
        //        d->ui->titleButton->setIconSize(QSize(20, 20));
        //    }
        //}
    }

    auto ThumbnailGroupBox::GetChildren() const ->QList<LinkableThumbnailWidget*> {
        return d->children;
    }

    void ThumbnailGroupBox::SetSelectAll(bool select) const {
        for (auto child : d->children) {
            if (nullptr == child) {
                continue;
            }

            child->SetSelected(select);
        }
    }

    void ThumbnailGroupBox::on_titleButton_clicked() const {
        auto selectedAll = true;
        for (auto child : d->children) {
            if (nullptr == child) {
                continue;
            }

            if (false == child->Selected()) {
                selectedAll = false;
                break;
            }
        }

        SetSelectAll(!selectedAll);
    }
}
