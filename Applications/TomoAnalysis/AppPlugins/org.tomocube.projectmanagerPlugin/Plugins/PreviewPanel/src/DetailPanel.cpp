#include <iostream>

#include <QListView>
#include <QScrollBar>
#include <QMenu>
#include <QMouseEvent>

#include <TCFMetaReader.h>

#include "ui_DetailPanel.h"
#include "DetailPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct DetailPanel::Impl {
        Ui::DetailPanel* ui{ nullptr };

        ProjectInfo::Pointer currentProject = nullptr;
        PlaygroundInfo::Pointer currentPlayground = nullptr;
        QString currentHypercubeName;
        QString currentCubeName;

        QString lastTCFPath;

        bool exclusiveMode = false;
    };

    DetailPanel::DetailPanel(QWidget* parent) : d{ new Impl }, QWidget(parent) {
        //Q_UNUSED(parent)
        d->ui = new Ui::DetailPanel;
        d->ui->setupUi(this);

        d->ui->cubeComboBox->setView(new QListView);
        d->ui->hypercubeComboBox->setView(new QListView);

        const QStringList headers{ "Path", "Date", "Name","Valid", "HT", "FL", "BF", "Cube", "Hypercube" };
        d->ui->contentsTableWidget->setColumnCount(headers.size());
        d->ui->contentsTableWidget->setHorizontalHeaderLabels(headers);
        d->ui->contentsTableWidget->hideColumn(0);

        d->ui->includedTableWidget->setColumnCount(headers.size());
        d->ui->includedTableWidget->setHorizontalHeaderLabels(headers);
        d->ui->includedTableWidget->hideColumn(0);

        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(7, QHeaderView::ResizeToContents);
        d->ui->contentsTableWidget->horizontalHeader()->setSectionResizeMode(8, QHeaderView::Stretch);

        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(7, QHeaderView::ResizeToContents);
        d->ui->includedTableWidget->horizontalHeader()->setSectionResizeMode(8, QHeaderView::Stretch);

        d->ui->contentsTableWidget->setProperty("styleVariant", 1);
        d->ui->includedTableWidget->setProperty("styleVariant", 1);

        // set object names
        d->ui->label->setObjectName("h9");
        d->ui->label_2->setObjectName("h9");
        d->ui->contentsSelectAllButton->setObjectName("bt-round-gray500");
        d->ui->contentsDeselectAllButton->setObjectName("bt-round-gray500");
        d->ui->includedSelectAllButton->setObjectName("bt-round-gray500");
        d->ui->includedDeselectAllButton->setObjectName("bt-round-gray500");
        d->ui->includeButton->setObjectName("bt-arrow-right");
        d->ui->excludeButton->setObjectName("bt-arrow-left");

        connect(d->ui->includedTableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(OnIncTableDbClick(int, int)));
        connect(d->ui->contentsTableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(OnContTableDbClick(int, int)));       
        
        d->ui->contentsTableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(d->ui->contentsTableWidget, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(OnContRightClick(const QPoint&)));
        d->ui->includedTableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(d->ui->includedTableWidget, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(OnIncRightClick(const QPoint&)));        
    }

    DetailPanel::~DetailPanel() {

    }
    void DetailPanel::OnIncRightClick(const QPoint& pos) {
        auto tableItem = d->ui->includedTableWidget->itemAt(pos);
        if (tableItem) {
            auto path = d->ui->includedTableWidget->item(tableItem->row(), 0)->text();
            QMenu myMenu;
            myMenu.addAction("Create Simple Playground");
            myMenu.addAction("Open Folder");
            myMenu.addAction("Unlink from Cube");
            myMenu.addAction("Viewer");
            QAction* selectedItem = myMenu.exec(d->ui->includedTableWidget->mapToGlobal(pos));
            if (selectedItem) {
                auto text = selectedItem->text();
                if (text == "Create Simple Playground") {
                    emit createSimple(path);
                }
                else if (text == "Open Folder") {
                    emit openFolder(path);
                }
                else if (text == "Unlink from Cube") {
                    emit unlinkCube(path);
                }
                else if (text == "Viewer") {
                    QStringList pathList{ path };
                    emit openViewer(pathList);
                }
            }
        }
    }
    void DetailPanel::OnContRightClick(const QPoint& pos) {
        auto tableItem = d->ui->contentsTableWidget->itemAt(pos);
        if (tableItem) {            
            auto path = d->ui->contentsTableWidget->item(tableItem->row(), 0)->text();
            QMenu myMenu;
            myMenu.addAction("Create Simple Playground");
            myMenu.addAction("Open Folder");
            myMenu.addAction("Link to Cube");
            myMenu.addAction("Viewer");
            QAction* selectedItem = myMenu.exec(d->ui->contentsTableWidget->mapToGlobal(pos));
            if (selectedItem) {
                auto text = selectedItem->text();
                if (text == "Create Simple Playground") {
                    emit createSimple(path);
                }
                else if (text == "Open Folder") {
                    emit openFolder(path);
                }
                else if (text == "Link to Cube") {
                    emit linkCube(path);
                }
                else if (text == "Viewer") {
                    QStringList pathList{ path };
                    emit openViewer(pathList);
                }
            }
        }
    }
    bool DetailPanel::eventFilter(QObject* obj, QEvent* event) {
        if (event->type() == QEvent::MouseButtonPress) {
            auto selectedTable = dynamic_cast<QWidget*>(obj);
            if (nullptr == selectedTable) {
                return false;
            }
            QMouseEvent* me = static_cast<QMouseEvent*>(event);
            if (me->button() == Qt::RightButton) {
                QMenu myMenu;
                myMenu.addAction("Create Simple Playground");
                myMenu.addAction("Open Folder");
                myMenu.addAction("Link to Cube");
                myMenu.addAction("Unlink from Cube");
                myMenu.addAction("Viewer");
                QAction* selectedItem = myMenu.exec(me->globalPos());
                if (selectedItem) {
                    auto text = selectedItem->text();
                    if (text == "Create Simple Playground") {
                        //emit createSimple(selectedWidget->GetTCFPath());
                    }
                    else if (text == "Open Folder") {
                        //emit openFolder(selectedWidget->GetTCFPath());
                    }
                    else if (text == "Link to Cube") {
                        //emit linkCube(selectedWidget->GetTCFPath());
                    }
                    else if (text == "Unlink from Cube") {
                        //emit unlinkCube(selectedWidget->GetTCFPath());
                    }
                    else if (text == "Viewer") {
                        //emit openViewer(selectedWidget->GetTCFPath());
                    }
                }
            }
        }
        return true;
    }

    void DetailPanel::OnIncTableDbClick(int row, int col) {
        //std::cout << "include table " << row << "," << col << " double click" << std::endl;
        auto path = d->ui->includedTableWidget->item(row, 0)->text();
        emit infoPanel(path);
    }

    void DetailPanel::OnContTableDbClick(int row, int col){
        //std::cout << "contents table " << row << "," << col << " double click" << std::endl;
        auto path = d->ui->contentsTableWidget->item(row, 0)->text();
        emit infoPanel(path);
    }

    void DetailPanel::Clear() const {
        d->ui->cubeComboBox->clear();
        d->ui->hypercubeComboBox->clear();

        while (d->ui->contentsTableWidget->rowCount() > 0) {
            d->ui->contentsTableWidget->removeRow(0);
        }

        while (d->ui->includedTableWidget->rowCount() > 0) {
            d->ui->includedTableWidget->removeRow(0);
        }

        d->currentHypercubeName = "";
        d->currentCubeName = "";

        d->currentPlayground = nullptr;
    }

    auto DetailPanel::ClearSelection() const -> void {
        d->lastTCFPath = QString();
        d->ui->contentsTableWidget->clearSelection();
        d->ui->includedTableWidget->clearSelection();
    }


    void DetailPanel::ScanDir(const TCFDir::Pointer& dir, QStringList& fileList) {
        if(nullptr == dir) {
            return;
        }

        for(const auto& file : dir->GetFileList()) {
            auto filePath = QString("%1/%2").arg(dir->GetPath()).arg(file);
            fileList.append(filePath);
        }

        for(const auto& sub : dir->GetDirList()) {
            if (nullptr == sub) {
                continue;
            }

            ScanDir(sub, fileList);
        }
    }
    void DetailPanel::AddItem(const TCFDir::Pointer& dir) {
        if(nullptr == dir) {
            return;
        }

        QStringList fileList;
        ScanDir(dir,fileList);

        //for (const auto& tcf : dir->GetFileList()) {
        for(const auto& tcf : fileList){
            //auto path = QString("%1/%2").arg(dir->GetPath()).arg(tcf);
            auto path = tcf;

            DetailInfo info;
            auto info_exist = GenerateDetailInfo(path, info);
            if(info_exist){
                AddDetailItem(d->ui->contentsTableWidget, info);
            }
        }
    }

    void DetailPanel::Init(const ProjectInfo::Pointer& project) {
        if (nullptr == project) {
            return;
        }

        this->Clear();

        d->currentProject = project;

        // contentsTableWidget
        UpdateContentsTableWidget();

        const auto playground = project->GetCurrentPlayground();
        if (nullptr == playground) {
            return;
        }

        d->currentPlayground = playground;

        for (const auto& hypercube : d->currentPlayground->GetHyperCubeList()) {
            if (nullptr == hypercube) {
                continue;
            }

            d->ui->hypercubeComboBox->addItem(hypercube->GetName());
            //auto dropdownList = qobject_cast<QListView*>(d->ui->hypercubeComboBox->view());
            d->ui->hypercubeComboBox->setItemData(
                d->ui->hypercubeComboBox->count() - 1,
                hypercube->GetName(),
                Qt::ToolTipRole
            );
        }

        d->ui->hypercubeComboBox->setCurrentIndex(0);
    }

    void DetailPanel::Update(const ProjectInfo::Pointer& project) {
        if (nullptr == project) {
            return;
        }

        d->currentProject = project;

        const auto playground = project->GetCurrentPlayground();
        if (d->currentPlayground != playground) {
            this->Clear();
            d->currentPlayground = playground;
        }

        // contentsTableWidget
        UpdateContentsTableWidget();

        if (nullptr == d->currentPlayground) {
            return;
        }

        // update ComboBox
        for (const auto& hypercube : d->currentPlayground->GetHyperCubeList()) {
            if (nullptr == hypercube) {
                continue;
            }

            if(0 > d->ui->hypercubeComboBox->findText(hypercube->GetName())) {
                d->ui->hypercubeComboBox->addItem(hypercube->GetName());
                d->ui->hypercubeComboBox->setItemData(
                    d->ui->hypercubeComboBox->count() - 1,
                    hypercube->GetName(),
                    Qt::ToolTipRole
                );
            }
        }

        if (true == d->currentHypercubeName.isEmpty()) {
            d->ui->hypercubeComboBox->setCurrentIndex(0);
        }
        else {
            const auto findHypercube = d->currentPlayground->FindHypercube(d->currentHypercubeName);
            if(nullptr != findHypercube) {
                for (const auto& cube : findHypercube->GetCubeList()) {
                    if (nullptr == cube) {
                        continue;
                    }

                    if (0 > d->ui->cubeComboBox->findText(cube->GetName())) {
                        d->ui->cubeComboBox->addItem(cube->GetName());
                        d->ui->cubeComboBox->setItemData(
                            d->ui->cubeComboBox->count() - 1,
                            cube->GetName(),
                            Qt::ToolTipRole
                        );
                    }
                }
            }

            if (true == d->currentCubeName.isEmpty()) {
                d->ui->cubeComboBox->setCurrentIndex(0);
            }
            else {
                const auto index = d->ui->cubeComboBox->findText(d->currentCubeName);
                if (0 <= index) {
                    on_cubeComboBox_currentIndexChanged(index);
                }
            }
        }
    }

    void DetailPanel::UpdateLink(const ProjectInfo::Pointer& project) {
        if (nullptr == project) {
            return;
        }

        const auto contentsTableWidgetVerticalScrollPosition = d->ui->contentsTableWidget->verticalScrollBar()->value();
        const auto contentsTableWidgetHorizontalScrollPosition = d->ui->contentsTableWidget->horizontalScrollBar()->value();
        const auto includedTableWidgetVerticalScrollPosition = d->ui->includedTableWidget->verticalScrollBar()->value();
        const auto includedTableWidgetHorizontalScrollPosition = d->ui->includedTableWidget->horizontalScrollBar()->value();

        Update(project);

        d->ui->contentsTableWidget->verticalScrollBar()->setValue(contentsTableWidgetVerticalScrollPosition);
        d->ui->contentsTableWidget->horizontalScrollBar()->setValue(contentsTableWidgetHorizontalScrollPosition);
        d->ui->includedTableWidget->verticalScrollBar()->setValue(includedTableWidgetVerticalScrollPosition);
        d->ui->includedTableWidget->horizontalScrollBar()->setValue(includedTableWidgetHorizontalScrollPosition);
    }

    auto DetailPanel::GetLastSelectedTCF() const->QString {
        return d->lastTCFPath;
    }

    auto DetailPanel::GetSelectedContextsTCFList() const->QStringList {
        QStringList selectedTCFList;

        for(auto modelItem : d->ui->contentsTableWidget->selectionModel()->selectedRows()) {
            selectedTCFList << d->ui->contentsTableWidget->item(modelItem.row(), 0)->text();
        }

        return selectedTCFList;
    }

    auto DetailPanel::GetSelectedIncludedTCFList() const->QStringList {
        QStringList selectedTCFList;

        for (auto modelItem : d->ui->includedTableWidget->selectionModel()->selectedRows()) {
            selectedTCFList << d->ui->includedTableWidget->item(modelItem.row(), 0)->text();
        }

        return selectedTCFList;
    }

    auto DetailPanel::GetLinkedCubeNameList() const->QStringList {
        QStringList linkedCubeList;

        for (auto modelItem : d->ui->includedTableWidget->selectionModel()->selectedRows()) {
            const auto cubeNameList = d->ui->includedTableWidget->item(modelItem.row(), 6)->text();
            QStringList list = cubeNameList.split(" ,");

            for (auto name : list) {
                if (false == linkedCubeList.contains(name)) {
                    linkedCubeList << name;
                }
            }
        }

        return linkedCubeList;
    }

    void DetailPanel::on_hypercubeComboBox_currentIndexChanged(int index) {
        Q_UNUSED(index)
        d->ui->cubeComboBox->clear();

        const auto hypercubeName = d->ui->hypercubeComboBox->currentText();
        if(true == hypercubeName.isEmpty()) {
            return;
        }

        const auto hypercube = d->currentPlayground->FindHypercube(hypercubeName);
        if(nullptr == hypercube) {
            return;
        }

        for(const auto& cube : hypercube->GetCubeList()) {
            if(nullptr == cube) {
                continue;
            }

            d->ui->cubeComboBox->addItem(cube->GetName());
            d->ui->cubeComboBox->setItemData(
                d->ui->cubeComboBox->count() - 1,
                cube->GetName(),
                Qt::ToolTipRole
            );
        }

        d->currentHypercubeName = hypercubeName;

        on_cubeComboBox_currentIndexChanged(0);
    }

    void DetailPanel::on_cubeComboBox_currentIndexChanged(int index) {
        Q_UNUSED(index)
        while (d->ui->includedTableWidget->rowCount() > 0) {
            d->ui->includedTableWidget->removeRow(0);
        }

        const auto cubeName = d->ui->cubeComboBox->currentText();
        if (true == cubeName.isEmpty()) {
            return;
        }

        const auto hypercube = d->currentPlayground->FindHypercube(d->currentHypercubeName);
        if (nullptr == hypercube) {
            return;
        }

        const auto cube = hypercube->FindCube(cubeName);
        if(nullptr == cube) {
            return;
        }

        for(const auto& dir : cube->GetTCFDirList()) {
            if (nullptr == dir) {
                continue;
            }

            DetailInfo info;
            if (false == info.cubeList.contains(cubeName)) {
                info.cubeList << cubeName;
            }

            if (false == info.hypercubeList.contains(hypercube->GetName())) {
                info.hypercubeList << hypercube->GetName();
            }

            auto info_exist=  GenerateDetailInfo(dir->GetPath(), info);

            if(info_exist) {
                AddDetailItem(d->ui->includedTableWidget, info);
            }
        }

        if(true == d->exclusiveMode) {
            UpdateContentsTableWidget();
        }

        d->currentCubeName = cubeName;
    }

    void DetailPanel::on_contentsSelectAllButton_clicked() {
        d->ui->contentsTableWidget->selectAll();

        auto lastItem = d->ui->contentsTableWidget->item(d->ui->contentsTableWidget->rowCount() - 1, 0);
        if (lastItem) {
            d->lastTCFPath = lastItem->text();
        }
    }

    void DetailPanel::on_contentsDeselectAllButton_clicked() {
        d->ui->contentsTableWidget->clearSelection();
        d->lastTCFPath.clear();
    }

    void DetailPanel::on_contentsTableWidget_itemSelectionChanged() const {
        const auto row = d->ui->contentsTableWidget->currentRow();
        if (0 > row) {
            return;
        }

        d->lastTCFPath = d->ui->contentsTableWidget->item(row, 0)->text();
    }

    void DetailPanel::on_includedSelectAllButton_clicked() {
        d->ui->includedTableWidget->selectAll();

        auto lastItem = d->ui->includedTableWidget->item(d->ui->includedTableWidget->rowCount() - 1, 0);
        if (lastItem) {
            d->lastTCFPath = lastItem->text();
        }
    }

    void DetailPanel::on_includedDeselectAllButton_clicked() {
        d->ui->includedTableWidget->clearSelection();
        d->lastTCFPath.clear();
    }

    void DetailPanel::on_includedTableWidget_itemSelectionChanged() const {
        const auto row = d->ui->includedTableWidget->currentRow();
        if(0 > row) {
            return;
        }

        d->lastTCFPath = d->ui->includedTableWidget->item(row, 0)->text();
    }

    void DetailPanel::on_includeButton_clicked() {
        if(nullptr == d->currentPlayground) {
            return;
        }

        const auto selectedList = GetSelectedContextsTCFList();
        emit linkRequest(selectedList, 
                         d->ui->cubeComboBox->currentText(), 
                         d->currentPlayground->GetPath());

    }

    void DetailPanel::on_excludeButton_clicked() {
        if(nullptr == d->currentPlayground) {
            return;
        }

        const auto selectedList = GetSelectedIncludedTCFList();
        emit unlinkRequest(selectedList, 
                           d->ui->cubeComboBox->currentText(), 
                           d->currentPlayground->GetPath());
    }

    void DetailPanel::on_exclusiveCheckBox_stateChanged(int state) {
        if (Qt::Unchecked == state) {
            d->exclusiveMode = false;
        }
        else {
            d->exclusiveMode = true;
        }

        UpdateContentsTableWidget();
    }

    void DetailPanel::UpdateContentsTableWidget() {
        if(nullptr == d->currentProject) {
            return;
        }

        while (d->ui->contentsTableWidget->rowCount() > 0) {
            d->ui->contentsTableWidget->removeRow(0);
        }

        for (const auto& dir : d->currentProject->GetTCFDirList()) {
            if (nullptr == dir) {
                continue;
            }

            QStringList fileList;
            ScanDir(dir,fileList);

            //for (const auto& tcf : dir->GetFileList()) {
            for(const auto& tcf : fileList){
                //auto path = QString("%1/%2").arg(dir->GetPath()).arg(tcf);
                auto path  =tcf;

                if (true == d->exclusiveMode) {
                    bool included = false;
                    
                    for(auto i = 0; i < d->ui->includedTableWidget->rowCount(); ++i) {
                        if(path == d->ui->includedTableWidget->item(i, 0)->text()) {
                            included = true;
                            break;
                        }
                    }

                    if(true == included) {
                        continue;
                    }
                }

                DetailInfo info;
                auto info_exist = GenerateDetailInfo(path, info);

                // find include hypercube, cube
                const auto currentPlayground = d->currentProject->GetCurrentPlayground();
                if(nullptr != currentPlayground) {
                    for (const auto& hypercube : currentPlayground->GetHyperCubeList()) {
                        if (nullptr == hypercube) {
                            continue;
                        }

                        for (const auto& cube : hypercube->GetCubeList()) {
                            if (nullptr == cube) {
                                continue;
                            }

                            const auto cubeName = cube->GetName();
                            for (const auto& tcfFolder : cube->GetTCFDirList()) {
                                if (nullptr == tcfFolder) {
                                    continue;
                                }

                                if (path == tcfFolder->GetPath()) {
                                    if(false == info.cubeList.contains(cubeName)) {
                                        info.cubeList << cubeName;
                                    }

                                    if(false == info.hypercubeList.contains(hypercube->GetName())) {
                                        info.hypercubeList << hypercube->GetName();
                                    }
                                }
                            }
                        }
                    }
                }
                if(info_exist){
                    AddDetailItem(d->ui->contentsTableWidget, info);
                }
            }
        }
    }

    bool DetailPanel::GenerateDetailInfo(const QString& path, DetailInfo& info) {
        if (true == path.isEmpty()) {
            return false;
        }
        auto GetSimpleName = [=](const QString& name) {
            auto split = name.split("/");
            auto simple = split[split.size() - 1];

            if (20 > simple.size()) {
                return simple;
            }

            const auto list = simple.split(".");
            if (3 > list.size()) {
                return simple;
            }

            simple = simple.remove(0, 20);
            return simple.chopped(4);
        };

        auto reader = TC::IO::TCFMetaReader();
        const auto meta = reader.Read(path);

        if (nullptr == meta) {            
            return false;
        }

        if(false == meta->data.data2DMIP.exist) {
            return false;
        }

        info.path = path;
        info.date = QDateTime::fromString(meta->common.createDate, "yyyy-MM-dd hh:mm:ss").toString("yyyy.MM.dd");
        info.name = GetSimpleName(path);
        if(true == meta->data.data3D.exist) {
            uint64_t arrSize = static_cast<uint64_t>(meta->data.data3D.sizeX) * static_cast<uint64_t>(meta->data.data3D.sizeY) * static_cast<uint64_t>(meta->data.data3D.sizeZ);
            if(arrSize < INT_MAX/4) {
                info.valid = "AI";
            }else if(arrSize < INT_MAX / 3 * 2) {
                info.valid = "RI";
            }else {
                info.valid = "N/A";
            }
        }else {
            info.valid = "N/A";
        }
        if(true == meta->data.data2DMIP.exist) {
            info.countHT = meta->data.data2DMIP.dataCount;
        }

        if (true == meta->data.data2DFLMIP.exist) {
            info.countFL = meta->data.data2DFLMIP.dataCount;
        }

        if (true == meta->data.dataBF.exist) {
            info.countBF = meta->data.dataBF.dataCount;
        }

        return true;
    }

    void DetailPanel::AddDetailItem(QTableWidget* tableWidget, const DetailInfo& info) const {
        const auto row = tableWidget->rowCount();        
        tableWidget->insertRow(row);

        auto pathItem = new QTableWidgetItem(info.path);
        pathItem->setFlags(pathItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 0, pathItem);

        auto dateItem = new QTableWidgetItem(info.date);
        dateItem->setTextAlignment(Qt::AlignCenter);
        dateItem->setFlags(dateItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 1, dateItem);

        auto nameItem = new QTableWidgetItem(info.name);
        nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 2, nameItem);

        auto validItem = new QTableWidgetItem(info.valid);
        validItem->setTextAlignment(Qt::AlignCenter);
        validItem->setFlags(validItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 3, validItem);

        //auto htItem = new QTableWidgetItem(QString::number(info.countHT));
        auto htItem = new IntTableItem(QString::number(info.countHT));
        htItem->setTextAlignment(Qt::AlignCenter);
        htItem->setFlags(htItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 4, htItem);        
        //auto flItem = new QTableWidgetItem(QString::number(info.countFL));
        auto flItem = new IntTableItem(QString::number(info.countFL));
        flItem->setTextAlignment(Qt::AlignCenter);
        flItem->setFlags(flItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 5, flItem);

        //auto bfItem = new QTableWidgetItem(QString::number(info.countBF));
        auto bfItem = new IntTableItem(QString::number(info.countBF));
        bfItem->setTextAlignment(Qt::AlignCenter);
        bfItem->setFlags(bfItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 6, bfItem);

        const auto cubeText = info.cubeList.join(", ");  
        auto cubeItem = new QTableWidgetItem(cubeText);
        cubeItem->setTextAlignment(Qt::AlignCenter);
        cubeItem->setFlags(cubeItem->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, 7, cubeItem);

        const auto hypercubeText = info.hypercubeList.join(", ");
        auto hypercubeItem = new QTableWidgetItem(hypercubeText);
        hypercubeItem->setTextAlignment(Qt::AlignCenter);
        hypercubeItem->setFlags(hypercubeItem->flags() ^ Qt::ItemIsEditable);
        hypercubeItem->setToolTip(hypercubeText);
        tableWidget->setItem(row, 8, hypercubeItem);
    }
}