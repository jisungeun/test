#include <iostream>
#include <QHBoxLayout>
#include <QPushButton>
#include <qgraphicseffect.h>
#include <QLabel>

#include <SimpleTCF2DViewer.h>
#include <TCFMetaReader.h>
#include <TCFRawDataReader.h>

#include "LinkableThumbnailWidget.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    /////////////////////////////////////////////////////////////////////////////
    //////////////////////////// TimelapseInfoWidget ////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct TimelapseInfoWidget::Impl {
    };

    TimelapseInfoWidget::TimelapseInfoWidget(QWidget* parent)
        : QWidget(parent)
        , d{ new Impl } {
        auto oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.8);
        this->setGraphicsEffect(oe);
        this->setAutoFillBackground(true);

        // Set UI
        auto layout = new QHBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);

        auto button = new QPushButton;
        button->setText("T");
        button->setFixedSize(22, 22);

        // time-lapse annotation 표시용 버튼의 스타일은 예외적으로 소스코드에서 정의함.
        // 같은 stylesheet를 qss에서 작성하여 적용하려는 경우, border가 표시되지 않는 문제가 발생함.
        // Todo: 위 문제 원인 확인 후 qss에서 스타일 정의
        button->setStyleSheet(
            "background-color: rgba(255,255,255,0);"
            "border: 1px solid #F0F0F0; border-radius: 2px;"
            "color: #F0F0F0; text-align: center;"
            "font-size: 14px;"
            "padding: 0;"
        );

        layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
        layout->addWidget(button);

        this->setLayout(layout);
    }

    TimelapseInfoWidget::~TimelapseInfoWidget() = default;


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////// FileInfoWidget //////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    struct FileInfoWidget::Impl {
        int availability{0};
        //0: not able 1: RI able 2: AI able
    };

    FileInfoWidget::FileInfoWidget(QWidget* parent) :QWidget(parent), d{ new Impl } {
        auto oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(1.0);
        this->setGraphicsEffect(oe);
        this->setAutoFillBackground(true);
    }

    auto FileInfoWidget::SetAble(int able) -> void {
        d->availability = able;
        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                delete item->widget();
                delete item;
            }
            delete this->layout();
        }
        auto layout = new QHBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->setMargin(0);

        layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));

        auto button = new QPushButton;
        button->setObjectName("bt-annotation-link");
        button->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
        if (able < 1) {
            button->setIcon(QIcon(":/image/images/RiDisable.png"));
        }else {
            button->setIcon(QIcon(":/image/images/RiAble.png"));
        }
        button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        button->setFixedWidth(28);
        button->setIconSize(QSize(16, 16));

        if (able > 1) {
            auto button2 = new QPushButton;
            button2->setObjectName("bt-annotation-link");
            button2->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
            button2->setIcon(QIcon(":/image/images/AiAble.png"));
            button2->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
            button2->setFixedWidth(28);
            button2->setIconSize(QSize(16, 16));
            layout->addWidget(button2);
        }

        layout->addWidget(button);

        this->setLayout(layout);

    }
    auto FileInfoWidget::GetAble() -> int {
        return d->availability;
    }


    FileInfoWidget::~FileInfoWidget() = default;


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////// LinkInfoWidget //////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct LinkInfoWidget::Impl {
        QStringList linkedNameList;
    };

    LinkInfoWidget::LinkInfoWidget(QWidget* parent)
        : QWidget(parent)
        , d{ new Impl } {
        auto oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(1.0);
        this->setGraphicsEffect(oe);
        this->setAutoFillBackground(true);
    }

    LinkInfoWidget::~LinkInfoWidget() = default;

    void LinkInfoWidget::SetLinkInfo(const QStringList& list) {
        if (d->linkedNameList == list) {
            return;
        }

        if (this->layout() != nullptr) {
            QLayoutItem* item;
            while ((item = this->layout()->takeAt(0)) != nullptr) {
                delete item->widget();
                delete item;
            }
            delete this->layout();
        }

        auto layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);

        for (auto name : list) {
            auto button = new QPushButton;
            button->setObjectName("bt-annotation-link");
            button->setIcon(QIcon(":/img/ic-explorer-cube.svg"));
            button->setIconSize(QSize(16, 16));
            button->setText(name);

            layout->addWidget(button);
        }

        this->setLayout(layout);

        d->linkedNameList = list;
    }

    void LinkInfoWidget::AddLinkInfo(const QString& name) const {
        if (true == d->linkedNameList.contains(name)) {
            return;
        }

        auto button = new QPushButton;
        button->setObjectName("bt-annotation-link");
        button->setIcon(QIcon(":/img/ic-explorer-cube.svg"));
        button->setIconSize(QSize(16, 16));
        button->setText(name);

        this->layout()->addWidget(button);
        d->linkedNameList.append(name);
       
    }

    auto LinkInfoWidget::GetLinkList() const ->QStringList {
        return d->linkedNameList;
    }


    /////////////////////////////////////////////////////////////////////////////
    ////////////////////////// LinkableThumbnailWidget //////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct LinkableThumbnailWidget::Impl {
        TC::IO::TCFMetaReader* metaReader =  nullptr;
        TC::IO::TCFRawDataReader* dataReader = nullptr;

        TC::IO::TCFMetaReader::Meta::Pointer meta;

        TC::TCF2DWidget* thumbnailWidget = nullptr;
        LinkInfoWidget* linkInfoWidget = nullptr;
        FileInfoWidget* fileInfoWidget = nullptr;
        TimelapseInfoWidget* timelapseInfoWidget = nullptr;
    };

    LinkableThumbnailWidget::LinkableThumbnailWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->thumbnailWidget = new TC::TCF2DWidget(parent);
        d->thumbnailWidget->KeepSquare(true);
        d->thumbnailWidget->SetSelectable(true);

        d->linkInfoWidget = new LinkInfoWidget;
        d->fileInfoWidget = new FileInfoWidget;

        d->timelapseInfoWidget = new TimelapseInfoWidget;
        d->timelapseInfoWidget->hide();

        auto mergeLayout = new QHBoxLayout;        
        mergeLayout->setContentsMargins(0, 0, 0, 0);
        mergeLayout->setSpacing(0);

        auto linkLayout = new QVBoxLayout;
        linkLayout->addWidget(d->linkInfoWidget);
        linkLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        linkLayout->setContentsMargins(0, 0, 0, 0);
        linkLayout->setSpacing(0);

        mergeLayout->addLayout(linkLayout);

        auto fileLayout = new QVBoxLayout;
        //fileLayout->addLayout(linkLayout);
        fileLayout->addWidget(d->fileInfoWidget);
        fileLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        fileLayout->setContentsMargins(0, 0, 0, 0);
        fileLayout->setSpacing(0);

        mergeLayout->addLayout(fileLayout);

        auto timelapseLayout = new QVBoxLayout;
        //timelapseLayout->addLayout(linkLayout);
        //timelapseLayout->addLayout(fileLayout);
        timelapseLayout->addLayout(mergeLayout);
        timelapseLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        timelapseLayout->addWidget(d->timelapseInfoWidget);
        timelapseLayout->setContentsMargins(0, 0, 0, 0);
        timelapseLayout->setSpacing(0);

        auto thumbnailLayout = new QVBoxLayout();
        thumbnailLayout->addLayout(timelapseLayout);
        thumbnailLayout->setContentsMargins(0, 0, 0, 0);
        d->thumbnailWidget->SetLayout(thumbnailLayout);

        auto mainLayout = new QVBoxLayout();
        mainLayout->addWidget(d->thumbnailWidget);
        mainLayout->setContentsMargins(0, 0, 0, 0);
        this->setLayout(mainLayout);
    }

    LinkableThumbnailWidget::~LinkableThumbnailWidget() = default;

    auto LinkableThumbnailWidget::ShowImage(const bool& show) -> void {
        d->thumbnailWidget->SetVisible(show);        
    }

    bool LinkableThumbnailWidget::SetFile(const QString& path,bool late_load) const {
        if(nullptr == d->thumbnailWidget) {
            return false;
        }

        if(false == d->thumbnailWidget->SetTCFPath(path)) {            
            return false;
        }
        
        if(false == d->thumbnailWidget->ShowImage(TC::TCF2DWidget::ImageType::HT,0,late_load)) {            
            return false;
        }
        /*
        if(false == d->thumbnailWidget->ShowImage(TC::TCF2DWidget::ImageType::No)) {
            return false;
        }*/

        // timelapse
        auto frameCount = d->thumbnailWidget->GetTimeFrameCount(TC::TCF2DWidget::ImageType::HT);
        if(2 > frameCount) {
            frameCount = d->thumbnailWidget->GetTimeFrameCount(TC::TCF2DWidget::ImageType::FL);
        }

        if(1 < frameCount) {
            d->timelapseInfoWidget->show();
        }
        else {
            d->timelapseInfoWidget->hide();
        }

        return true;
    }
        
    auto LinkableThumbnailWidget::GetLinkedCubeNameList() const ->QStringList {
        if (nullptr == d->linkInfoWidget) {
            return QStringList();
        }

        return d->linkInfoWidget->GetLinkList();
    }

    void LinkableThumbnailWidget::AddLinkedCubeName(const QString& cubeName) const {
        if (nullptr == d->linkInfoWidget) {
            return;
        }

        d->linkInfoWidget->AddLinkInfo(cubeName);
    }

    void LinkableThumbnailWidget::SetLinkedCubeNameList(const QStringList& cubeNameList) const {
        if(nullptr == d->linkInfoWidget) {
            return;
        }

        d->linkInfoWidget->SetLinkInfo(cubeNameList);
    }

    auto LinkableThumbnailWidget::SetAbleAnalysis(const int& able) -> void {
        if(nullptr == d->fileInfoWidget) {
            return;
        }
        d->fileInfoWidget->SetAble(able);
    }

    auto LinkableThumbnailWidget::GetAbleAnalysis() const -> int {
        if(nullptr == d->fileInfoWidget) {
            return -1;
        }
        return d->fileInfoWidget->GetAble();
    }

    auto LinkableThumbnailWidget::GetTCFPath() const ->QString {
        return d->thumbnailWidget->GetTCFPath();
    }

    bool LinkableThumbnailWidget::Selected() const {
        return d->thumbnailWidget->Selected();
    }

    void LinkableThumbnailWidget::SetSelected(bool selected) const {
        d->thumbnailWidget->SetSelected(selected);
    }
}
