#include "CilsItemWidget.h"

#include "CilsUpdater.h"
#include "GalleryContent.h"
#include "ui_CilsItemWidget.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsItemWidget::Impl {
		Ui::CilsItemWidget ui{};

		CilsUpdater* client = nullptr;

		QString dataId;
		QString infoId;
		QMap<int, QString> worksets;

		bool painted = false;
		bool titled = false;
		bool paused = false;
		bool finished = false;
	};

	CilsItemWidget::CilsItemWidget(CilsUpdater* client, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->client = client;
		d->ui.gallery->SetColumnCount(1);
		d->ui.gallery->AddItem();

		connect(d->ui.cancelBtn, &QPushButton::clicked, this, &CilsItemWidget::OnCancelBtnClicked);
		connect(d->ui.pauseBtn, &QPushButton::clicked, this, &CilsItemWidget::OnPauseBtnClicked);
		connect(d->ui.resumeBtn, &QPushButton::clicked, this, &CilsItemWidget::OnResumeBtnClicked);

		d->ui.cancelBtn->setVisible(false);
		d->ui.pauseBtn->setVisible(false);
		d->ui.resumeBtn->setVisible(false);
	}

	CilsItemWidget::~CilsItemWidget() = default;

	auto CilsItemWidget::Update(const SyncItem& item) -> void {
		if (d->dataId == item.GetDataId()) {
			if (!d->titled)
				d->ui.titleLabel->setText(item.GetTitle());

			if (d->infoId.isEmpty())
				d->infoId = item.GetInfoId();

			bool pauseChanged = d->paused;
			d->paused = item.IsPaused();
			pauseChanged = d->paused != pauseChanged;
			auto finishChanged = d->finished;
			d->finished = !(item.IsPaused() || item.IsPending() || item.IsDownloading());
			finishChanged = d->finished != finishChanged;

			d->ui.progressBar->setValue(item.GetPercentage());

			if (d->finished) {
				const auto status = QString("Downloaded (100%)");
				d->ui.statusLabel->setText(status);
				d->ui.progressBar->setVisible(false);
				d->ui.cancelBtn->setVisible(false);
				d->ui.pauseBtn->setVisible(false);
				d->ui.resumeBtn->setVisible(false);
			} else {
				const auto status = QString("%1 (%2%)").arg((d->paused) ? "Paused" : "Downloading").arg(item.GetPercentage());
				d->ui.statusLabel->setText(status);

				if (d->paused) {
					d->ui.resumeBtn->setVisible(true);
					d->ui.cancelBtn->setVisible(true);
					d->ui.pauseBtn->setVisible(false);

					if (pauseChanged)
						d->ui.progressBar->setStyleSheet("QProgressBar::chunk { background-color: #FF8F27 }");
				} else {
					d->ui.pauseBtn->setVisible(true);
					d->ui.cancelBtn->setVisible(true);
					d->ui.resumeBtn->setVisible(false);

					if (pauseChanged)
						d->ui.progressBar->setStyleSheet("QProgressBar::chunk { background-color: #00C2D3 }");
				}
			}

			if(finishChanged && d->finished && item.IsDownloaded()) {
				emit Downloaded(d->dataId, d->worksets, item.GetPath());
			}
		}
	}

	auto CilsItemWidget::StartDownload() -> void {
		const auto status = QString("Download started (0%)");
		d->ui.statusLabel->setText(status);

		d->ui.pauseBtn->setVisible(true);
		d->ui.cancelBtn->setVisible(true);
	}

	auto CilsItemWidget::FailDownload(const QString& message) -> void {
		d->finished = true;

		QString msg = message;
		if (msg.contains("Upload"))
			msg = "Upload error";
		else if (msg.contains('\n'))
			msg = "Unknown";

		const auto status = QString("Failed (%1)").arg(msg);
		d->ui.statusLabel->setText(status);
		d->ui.progressBar->setVisible(false);
	}

	auto CilsItemWidget::SetDataId(const QString& dataId) -> void {
		d->dataId = dataId;
		d->ui.titleLabel->setText(dataId);
	}

	auto CilsItemWidget::SetPreview(const QPixmap& image) -> void {
		if (!d->painted && !image.isNull()) {
			auto* item = d->ui.gallery->GetItemAt(0);
			item->SetPixmap(image);
			d->painted = true;
		}
	}

	auto CilsItemWidget::AddWorkset(int id, const QString& name) -> void {
		d->worksets[id] = name;

		const auto workset = (d->ui.worksetLabel->text().isEmpty()) ? QString("%2").arg(name) : QString("%1, %2").arg(d->ui.worksetLabel->text()).arg(name);
		d->ui.worksetLabel->setText(workset);
	}

	auto CilsItemWidget::IsPainted() const -> bool {
		return d->painted;
	}

	auto CilsItemWidget::IsFinished() const -> bool {
		return d->finished;
	}

	auto CilsItemWidget::OnCancelBtnClicked() -> void {
		if (!d->infoId.isEmpty()) {
			d->client->CancelDownload(d->infoId);

			d->finished = true;
			d->ui.progressBar->setVisible(false);
			d->ui.cancelBtn->setVisible(false);
			d->ui.pauseBtn->setVisible(false);
			d->ui.resumeBtn->setVisible(false);
			d->ui.statusLabel->setText("Cancelled");
		}
	}

	auto CilsItemWidget::OnPauseBtnClicked() -> void {
		if (!d->infoId.isEmpty())
			d->client->PauseDownload(d->infoId);
	}

	auto CilsItemWidget::OnResumeBtnClicked() -> void {
		if (!d->infoId.isEmpty())
			d->client->ResumeDownload(d->infoId);
	}
}
