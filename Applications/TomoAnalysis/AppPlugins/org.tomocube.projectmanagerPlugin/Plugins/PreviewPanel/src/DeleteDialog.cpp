#include "ui_DeleteDialog.h"
#include "DeleteDialog.h"

#include <QDateTime>

namespace TomoAnalysis::ProjectManager::Plugins {
    struct DeleteDialog::Impl {
        Ui::DeleteDialog* ui{ nullptr };
    };

    DeleteDialog::DeleteDialog(QWidget* parent, const QString& project, const QString&playground,const QString& hypercube, const QString& timestamp)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::DeleteDialog();
        d->ui->setupUi(this);

        // set object names
        d->ui->titleLabel->setObjectName("label-title-dialog");
        d->ui->label->setObjectName("h5");
        d->ui->label_2->setObjectName("h5");
        d->ui->label_3->setObjectName("h5");
        d->ui->label_4->setObjectName("h5");
        d->ui->playgroundEdit->setObjectName("input-high");
        d->ui->projectEdit->setObjectName("input-high");
        d->ui->hyperEdit->setObjectName("input-high");
        d->ui->timeEdit->setObjectName("input-high");
        d->ui->deleteButton->setObjectName("bt-square-primary");
        d->ui->cancelButton->setObjectName("bt-square-line");

        connect(d->ui->hyperEdit, SIGNAL(textChanged(QString)), this, SLOT(OnHyperText(QString)));
        connect(d->ui->projectEdit, SIGNAL(textChanged(QString)), this, SLOT(OnProjectText(QString)));
        connect(d->ui->playgroundEdit, SIGNAL(textChanged(QString)), this, SLOT(OnPGText(QString)));
        connect(d->ui->timeEdit, SIGNAL(textChanged(QString)), this, SLOT(OnTimeText(QString)));

        //d->ui->nameLineEdit->setText(project);
        d->ui->projectEdit->setText(project);        
        d->ui->hyperEdit->setText(hypercube);        
        auto timeText = QDateTime::fromString(timestamp, "yyMMdd.hhmmss").toString("yy-MM-dd hh:mm:ss");
        d->ui->timeEdit->setText(timeText);        
        d->ui->playgroundEdit->setText(playground);
        
        adjustSize();

        
    }
    
    DeleteDialog::~DeleteDialog() = default;

    void DeleteDialog::OnTimeText(QString txt) {
        QFont font("Noto Sans KR", 14);
        QFontMetrics fm(font);
        int pixelsWide = fm.width(txt) + 20;

        d->ui->timeEdit->setFixedWidth(pixelsWide);
    }

    void DeleteDialog::OnHyperText(QString txt) {
        QFont font("Noto Sans KR", 14);
        QFontMetrics fm(font);
        int pixelsWide = fm.width(txt) + 20;

        d->ui->hyperEdit->setFixedWidth(pixelsWide);
    }

    void DeleteDialog::OnPGText(QString txt) {
        QFont font("Noto Sans KR", 14);
        QFontMetrics fm(font);
        int pixelsWide = fm.width(txt) + 20;

        d->ui->playgroundEdit->setFixedWidth(pixelsWide);
    }

    void DeleteDialog::OnProjectText(QString txt) {
        QFont font("Noto Sans KR", 14);
        QFontMetrics fm(font);
        int pixelsWide = fm.width(txt) + 20;

        d->ui->projectEdit->setFixedWidth(pixelsWide);
    }


    auto DeleteDialog::Delete(QWidget* parent, const QString& project,const QString& playground, const QString& hypercube, const QString& timestamp)->bool {
        DeleteDialog dialog(parent, project,playground,hypercube,timestamp);

        if(dialog.exec() != QDialog::Accepted) {
            return false ;
        }

        return true;
    }

    void DeleteDialog::on_deleteButton_clicked() {
        this->accept();
    }

    void DeleteDialog::on_cancelButton_clicked() {
        this->reject();
    }
}
