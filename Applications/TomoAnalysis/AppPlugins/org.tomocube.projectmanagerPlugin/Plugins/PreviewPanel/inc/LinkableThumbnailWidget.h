#pragma once

#include <memory>
#include <QWidget>
#include <QStringList>

namespace TomoAnalysis::ProjectManager::Plugins {
    class TimelapseInfoWidget :public QWidget {
        Q_OBJECT

    public:
        typedef TimelapseInfoWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        TimelapseInfoWidget(QWidget* parent = nullptr);
        ~TimelapseInfoWidget();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class FileInfoWidget : public QWidget {
        Q_OBJECT
    public:
        typedef FileInfoWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        FileInfoWidget(QWidget* parent = nullptr);
        ~FileInfoWidget();

        auto SetAble(int able)->void;
        auto GetAble()->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class LinkInfoWidget :public QWidget {
        Q_OBJECT

    public:
        typedef LinkInfoWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        LinkInfoWidget(QWidget* parent = nullptr);
        ~LinkInfoWidget();

        void SetLinkInfo(const QStringList& list);
        void AddLinkInfo(const QString& name) const;

        auto GetLinkList() const ->QStringList;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class LinkableThumbnailWidget : public QWidget{
        Q_OBJECT

    public:
        typedef LinkableThumbnailWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        LinkableThumbnailWidget(QWidget* parent=nullptr);
        ~LinkableThumbnailWidget();

        bool SetFile(const QString& path,bool late_load = false) const;
        auto ShowImage(const bool& show)->void;

        auto GetLinkedCubeNameList() const ->QStringList;
        void AddLinkedCubeName(const QString& cubeName) const;
        void SetLinkedCubeNameList(const QStringList& cubeNameList) const;
        auto SetAbleAnalysis(const int& able)->void;
        auto GetAbleAnalysis()const->int;

        auto GetTCFPath() const ->QString;

        bool Selected() const;
        void SetSelected(bool selected) const;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}