#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class UnlinkDialog : public QDialog {
        Q_OBJECT
    public:
        explicit UnlinkDialog(int tcfCount,QStringList cubeList,QWidget* parent = nullptr);
        virtual ~UnlinkDialog();

        auto GetCurrentCube()const->QString;
        auto SetText(QString path)->void;

    public:
        static auto GetCubeName(int tcfCount, QStringList cubeList, QWidget* parent = nullptr)->QString;

    protected slots:
        void OnUnlinkBtn();
        void OnCancelBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}