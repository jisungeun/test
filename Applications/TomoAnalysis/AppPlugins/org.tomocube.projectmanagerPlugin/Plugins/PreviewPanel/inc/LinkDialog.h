#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class LinkDialog : public QDialog {
        Q_OBJECT
    public:
        explicit LinkDialog(QStringList cubeList,QWidget* parent = nullptr);
        virtual ~LinkDialog();

        auto GetCurrentCube()const->QString;
        auto SetText(QString path)->void;

    public:
        static auto GetCubeName(QStringList cubeList, QWidget* parent = nullptr)->QString;

    protected slots:
        void OnLinkBtn();
        void OnCancelBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}