#pragma once

#include <memory>
#include <QWidget>

#include <ProjectInfo.h>

namespace TomoAnalysis::ProjectManager::Plugins {
    class LinkableThumbnailWidget;

    class ThumbnailPanel : public QWidget {
        Q_OBJECT
    public:
        ThumbnailPanel(QWidget* parent = nullptr);
        ~ThumbnailPanel();

        void Clear();
        void ShowProject(const ProjectInfo::Pointer& project);
        void ShowHypercube(const HyperCube::Pointer& hypercube, const ProjectInfo::Pointer& project);
        void ShowCube(const Cube::Pointer& cube, const ProjectInfo::Pointer& project);
        void AddItem(const TCFDir::Pointer& dir);
        void UpdateLink(const ProjectInfo::Pointer& project) const;
        void UpdateValidAnalaysis(LinkableThumbnailWidget* widget)const;
        void SetPathList(const QStringList& pathList, const ProjectInfo::Pointer& project);

        auto ClearSelection()const->void;

        auto GetLastSelectedTCF() const ->QString;
        auto GetSelectedTCFList() const ->QStringList;

        auto GetLinkedCubeNameList() const ->QStringList;

    signals:
        void createSimple(QString);
        void openFolder(QString);
        void linkCube(QString);
        void unlinkCube(QString);
        void openViewer(QStringList);
        void infoPanel(QString);

    protected:
        bool eventFilter(QObject* obj, QEvent* event) override;

    private slots:
        void OnScrollAreaChanged(int);
        void on_selectAllButton_clicked() const;
        void on_deselectAllButton_clicked() const;

    private:
        void ScanDir(const TCFDir::Pointer& dir, QStringList& fileList);
        void InitialLoad();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}