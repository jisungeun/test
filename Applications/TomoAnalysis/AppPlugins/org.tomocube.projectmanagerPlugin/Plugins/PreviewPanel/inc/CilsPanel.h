#pragma once

#include <memory>

#include <QWidget>

#include "SyncExecution.h"
#include "SyncItem.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class CilsPanel : public QWidget {
		Q_OBJECT

	public:
		CilsPanel(QWidget* parent = nullptr);
		~CilsPanel() override;

		auto UpdateCils(const QMap<int, QStringList>&, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>&) -> void;

	protected slots:
		auto OnTimeout() -> void;
		auto OnDownloadItemLoaded(const QVector<SyncItem>& syncs, const QStringList& missing) -> void;
		auto OnDownloadStarted(const QString& dataId) -> void;
		auto OnDownloadFailed(const QString& dataId, const QString& message) -> void;
		auto OnPreviewLoaded(const QString& dataId, const QByteArray& image) -> void;
		auto OnDownloaded(const QString& dataId, const QMap<int, QString>& worksets, const QString& path) -> void;
		auto OnClearBtnClicked() -> void;
		auto OnError(const QString& message) -> void;

	signals:
		auto addTcf(QString path) -> void;
		auto linkTcfToWorkset(QString path, QString worksetName, int worksetId) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
