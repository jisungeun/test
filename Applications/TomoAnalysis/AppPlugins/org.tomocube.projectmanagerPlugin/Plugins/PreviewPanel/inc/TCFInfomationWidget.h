#pragma once

#include <memory>
#include <QWidget>

#include <TCFMetaReader.h>
#include <TCFRawDataReader.h>

namespace TomoAnalysis::ProjectManager::Plugins {
    class TCFInformationWidget : public QWidget  {
        Q_OBJECT
        
    public:
        typedef TCFInformationWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        TCFInformationWidget(QWidget* parent=nullptr);
        ~TCFInformationWidget();

        void SetFile(const QString& path);
        void SetWindowSize(const int& w, const int& h);

    protected:
        void resizeEvent(QResizeEvent*);

    private:
        auto ReadTCFCommonMeta(const QString& path, TC::IO::TCFMetaReader::Meta::Pointer& meta)->bool;
        auto GenerateFileInfo(const QString& path,
                              const TC::IO::TCFMetaReader::Meta::Pointer& meta,
                              QList<QPair<QString, QString>>& infoList)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}