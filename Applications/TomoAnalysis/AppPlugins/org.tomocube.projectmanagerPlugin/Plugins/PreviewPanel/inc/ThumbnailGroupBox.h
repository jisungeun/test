#pragma once

#include <memory>
#include <QWidget>
#include "LinkableThumbnailWidget.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class ThumbnailGroupBox : public QWidget {
        Q_OBJECT

    public:
        typedef ThumbnailGroupBox Self;
        typedef std::shared_ptr<Self> Pointer;

        enum ThumbnailGroupBoxType {
            None = -1,
            Project = 0,
            Cube = 1,
        };

        ThumbnailGroupBox(const ThumbnailGroupBoxType& type,
                          const QString& text = "", QWidget* parent = nullptr);
        ~ThumbnailGroupBox();

        auto Text() const->QString;
        void SetText(const QString& text) const;

        void SetList(const QStringList& pathList) const;
        void SetChildren(QList<LinkableThumbnailWidget*> children) const;

        auto GetChildren() const ->QList<LinkableThumbnailWidget*>;

        void SetSelectAll(bool) const;

    protected slots:
        void on_titleButton_clicked() const;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}