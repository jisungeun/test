#pragma once

#include <memory>
#include <QTableWidget>

#include <ProjectInfo.h>

namespace TomoAnalysis::ProjectManager::Plugins {
    class IntTableItem : public QTableWidgetItem {
    public:
        IntTableItem(const QString& text, int type = 0) : QTableWidgetItem(text, type) {
            
        }
        ~IntTableItem() = default;
        bool operator< (const QTableWidgetItem& other)const {
            return (this->text().toInt() < other.text().toInt());
        }
    };
    class DetailPanel : public QWidget {
        Q_OBJECT

    public:
        DetailPanel(QWidget* parent = nullptr);
        ~DetailPanel();

        void Clear() const;
        void AddItem(const TCFDir::Pointer& dir);
        void Init(const ProjectInfo::Pointer& project);
        void Update(const ProjectInfo::Pointer& project);
        void UpdateLink(const ProjectInfo::Pointer& project);

        auto GetLastSelectedTCF() const->QString;
        auto GetSelectedContextsTCFList() const->QStringList;
        auto GetSelectedIncludedTCFList() const->QStringList;
        auto GetLinkedCubeNameList() const->QStringList;

        auto ClearSelection()const->void;

    signals:
        void linkRequest(QStringList tcfList, QString cube, QString playground);
        void unlinkRequest(QStringList tcfList, QString cube, QString playground);
        void infoPanel(QString);
        void createSimple(QString);
        void openFolder(QString);
        void linkCube(QString);
        void unlinkCube(QString);
        void openViewer(QStringList);

    protected slots:
        void on_hypercubeComboBox_currentIndexChanged(int index);
        void on_cubeComboBox_currentIndexChanged(int index);

        void on_contentsSelectAllButton_clicked();
        void on_contentsDeselectAllButton_clicked();
        void on_contentsTableWidget_itemSelectionChanged() const;

        void on_includedSelectAllButton_clicked();
        void on_includedDeselectAllButton_clicked();
        void on_includedTableWidget_itemSelectionChanged() const;

        void on_includeButton_clicked();
        void on_excludeButton_clicked();

        void on_exclusiveCheckBox_stateChanged(int state);

        void OnIncTableDbClick(int, int);
        void OnContTableDbClick(int, int);
        void OnIncRightClick(const QPoint&);
        void OnContRightClick(const QPoint&);

    protected:
        bool eventFilter(QObject* obj, QEvent* event) override;

    private:
        struct DetailInfo {
            QString path;
            QString date;
            QString name;
            QString valid;
            QStringList hypercubeList;
            QStringList cubeList;
            int countHT = 0;
            int countFL = 0;
            int countBF = 0;
        };

        void ScanDir(const TCFDir::Pointer& dir, QStringList& fileList);
        void UpdateContentsTableWidget();
        bool GenerateDetailInfo(const QString& path, DetailInfo& info);
        void AddDetailItem(QTableWidget* tableWidget, const DetailInfo& info) const;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}