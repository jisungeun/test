#pragma once

#include <memory>
#include <QWidget>

#include <ProjectInfo.h>

namespace TomoAnalysis::ProjectManager::Plugins {
    class ResultListWidget : public QWidget {
        Q_OBJECT
    public:
        ResultListWidget(QWidget* parent = nullptr);
        ~ResultListWidget();

        auto Update(ProjectInfo::Pointer proj,bool force=false,bool afterDelete=false)->void;                

    signals:
        void sigHistory(QString);
        void sigReport(QString);
        void sigDelete(QString);

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void OnSelectionChanged();
        void OnApplyButton();
        void OnReportButton();
        void OnDeleteButton();

    private:
        auto ParseResultList(PlaygroundInfo::Pointer pg)->void;
        auto RefreshTable()->void;
        auto CheckHyper(QString, QStringList)->bool;
        auto CallLastReport()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}