#pragma once

#include <memory>
#include <QWidget>

#include <IPreviewPanel.h>

#include "PreviewPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class PreviewPanel_API PreviewPanel : public QWidget, public Interactor::IPreviewPanel {
        Q_OBJECT
    public:
        typedef PreviewPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        PreviewPanel(QWidget* parent = nullptr);
        ~PreviewPanel();

        auto Update(const ProjectInfo::Pointer& info) -> bool override;
        auto UpdateLink(const ProjectInfo::Pointer& info)->bool override;
        auto AddItem(const TCFDir::Pointer& dir)->bool override;
        auto ClosePopup()->void;
        auto UpdateWithCurrentInfo()->void;
        auto UpdateAfterDelete()->void;
        auto UpdateCils(const QMap<int, QStringList>&, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>&) -> void;

        auto FlushSelection()->void;

    signals:
        void sigViewer(QStringList,QString);
        void sigSegmentation(QString);
        void sigSegmentation2D(QString);
        void sigLinkRequest(QStringList, QString, QString);        
        void sigUnlinkRequest(QStringList, QString, QString);
        void sigLoadHistory(QString);
        void sigDeleteHistory(QString);
        void sigLoadReport(QString);
        void sigCreateSimple(QString);
        void sigAddTcf(QString, QString);
        void sigLinkTcfToWorkset(QString, QString, QString, int);
    
    protected slots:
        void OnDbClickInfo(QString);
        void OnInfoClosed(QString);
        void OnOpenFolder(QString);
        void OnLinkCube(QString);
        void OnUnlinkCube(QString);
        void OnOpenViewer(QStringList);

        void on_informationToolButton_clicked() const;
        void on_viewerToolButton_clicked();
        void on_segToolButton_clicked();
        void OnSeg2dClicked();
        void on_linkToolButton_clicked();
        void on_unlinkToolButton_clicked();
        void OnResultClicked();
        void OnHistory(QString history);
        void OnDeleteHistory(QString history);
        void OnReport(QString report);
        void sqOnLinkTab(bool);
        void OnCreateSimple(QString tcfPath);
        void OnAddTcf(QString path);
        void OnLinkTcfToWorkset(QString path, QString worksetName, int worksetId);

        void onPlaygroundPanelItemChanged(QString info, QString playgroundPath);

        void onLinkRequested(QStringList tcfList, QString cube, QString playground);
        void onUnlinkRequested(QStringList tcfList, QString cube, QString playground);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}