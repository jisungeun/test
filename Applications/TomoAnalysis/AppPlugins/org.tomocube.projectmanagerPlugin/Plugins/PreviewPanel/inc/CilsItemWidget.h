#pragma once

#include <memory>

#include <QWidget>

#include "CilsUpdater.h"
#include "SyncItem.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class CilsItemWidget : public QWidget {
		Q_OBJECT

	public:
		CilsItemWidget(CilsUpdater* client, QWidget* parent = nullptr);
		~CilsItemWidget() override;

		auto Update(const SyncItem& item) -> void;
		auto StartDownload() -> void;
		auto FailDownload(const QString& message) -> void;

		auto SetDataId(const QString& dataId) -> void;
		auto SetPreview(const QPixmap& image) -> void;
		auto AddWorkset(int id, const QString& name) -> void;
		auto IsPainted() const -> bool;
		auto IsFinished() const -> bool;

	protected slots:
		auto OnCancelBtnClicked() -> void;
		auto OnPauseBtnClicked() -> void;
		auto OnResumeBtnClicked() -> void;

	signals:
		auto Downloaded(const QString& dataId, const QMap<int, QString>& worksets, const QString& path) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
