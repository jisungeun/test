#pragma once

#include <memory>
#include <QWidget>

#include <IProjectPathPanel.h>

#include "ProjectPathPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class ProjectPathPanel_API ProjectPathPanel : public QWidget, public Interactor::IProjectPathPanel {
        Q_OBJECT
        
    public:
        typedef ProjectPathPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ProjectPathPanel(QWidget* parent=nullptr);
        ~ProjectPathPanel();

        auto Update()->bool override;

    protected slots:
        void onChangeCurrentProject(const QString& path);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}