#include <UIUtility.h>

#include "ui_ProjectPathPanel.h"
#include "ProjectPathPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct ProjectPathPanel::Impl {
        Ui::ProjectPathPanel* ui{ nullptr };
    };

    ProjectPathPanel::ProjectPathPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IProjectPathPanel()
    , d{ new Impl } {
        d->ui = new Ui::ProjectPathPanel();
        d->ui->setupUi(this);
    }

    ProjectPathPanel::~ProjectPathPanel() {
        delete d->ui;
    }

    auto ProjectPathPanel::Update()->bool {
        return true;
    }

    void ProjectPathPanel::onChangeCurrentProject(const QString& path) {
        d->ui->pathLineEdit->setText(path);
    }
}
