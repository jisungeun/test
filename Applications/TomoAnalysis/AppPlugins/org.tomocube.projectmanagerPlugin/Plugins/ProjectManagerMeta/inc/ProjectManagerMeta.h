#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "ProjectManagerMetaExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class ProjectManagerMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.projectmanagerMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "Project Manager"; }
		auto GetFullName()const->QString override { return "org.tomocube.projectmanagerPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}