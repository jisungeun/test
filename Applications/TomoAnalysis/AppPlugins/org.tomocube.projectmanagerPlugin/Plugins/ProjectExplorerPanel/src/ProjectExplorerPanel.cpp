#include <iostream>
#include <QFileDialog>
#include <QFileInfo>
#include <QMenuBar>
#include <QMessageBox>
#include <QMouseEvent>
#include <QAbstractItemModel>

#include "RenameProjectDialog.h"
#include "ui_ProjectExplorerPanel.h"
#include "ProjectExplorerPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct ProjectExplorerPanel::Impl {
        Ui::ProjectExplorerPanel* ui{ nullptr };

        QString projectPath;
        QString projectName;

        const int nameColumnIndex = 0;
        const int typeColumnIndex = 1;
        const QString folderName = "TCF";
        const QString worksetName = "CILS Workset";
    };

    ProjectExplorerPanel::ProjectExplorerPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IProjectExplorerPanel()
    , d{ new Impl } {
        d->ui = new Ui::ProjectExplorerPanel();
        d->ui->setupUi(this);
        
        this->InitUI();
    }

    ProjectExplorerPanel::~ProjectExplorerPanel() {
        delete d->ui;
    }

    auto ProjectExplorerPanel::Init(const ProjectInfo::Pointer& project)->bool {
        if (nullptr == project) {
            return false;
        }

        d->ui->projectTreeWidget->clear();
        d->ui->projectTreeWidget->setRootIsDecorated(false);

        const auto projectItem = this->LoadProjectInfo(project);
        d->ui->projectTreeWidget->addTopLevelItem(projectItem);

        this->ShowRootIndicator();

        on_collapseButton_clicked();

        return true;
    }

    auto ProjectExplorerPanel::Update(const ProjectInfo::Pointer& project)->bool {
        if(nullptr == project) {
            return false;
        }

        QStringList state;
        SaveState(d->ui->projectTreeWidget->topLevelItem(0), state);

        this->Init(project);
        d->ui->projectTreeWidget->expandAll();

        RestoreState(d->ui->projectTreeWidget->topLevelItem(0), state);

        return true;
    }

    void ProjectExplorerPanel::on_expandButton_clicked() const {
        d->ui->projectTreeWidget->expandAll();
    }

    void ProjectExplorerPanel::on_collapseButton_clicked() {
        QTreeWidgetItem* item = d->ui->projectTreeWidget->topLevelItem(0);
        CollapseChild(item);
    }

    void ProjectExplorerPanel::OnShowContextMenu(const QPoint& pos) {
        auto* item = d->ui->projectTreeWidget->itemAt(pos);
        if (nullptr == item) {
            return;
        }

        const auto globalPos = d->ui->projectTreeWidget->viewport()->mapToGlobal(pos);

        QMenu menu;
        QAction* selectedItem{ nullptr };

        const auto type = item->type();
        switch (type) {
        case ProjectItemFlag::Root_Folder: {
            menu.addAction("Remove TCF");
            selectedItem = menu.exec(globalPos);
            break;
        }
        case ProjectItemFlag::Project: {
            menu.addAction("Rename Project");
            selectedItem = menu.exec(globalPos);
            break;
        }
        default:;
        }

        if (nullptr != selectedItem) {
            const auto text = selectedItem->text();
            if (true == text.contains("Remove TCF")) {
                this->RemoveTCF(item);
            }else if(true== text.contains("Rename Project")) {
                auto newName = RenameProjectDialog::Rename(nullptr,d->projectName);
                if(false == newName.isEmpty()) {
                    emit renameProject(d->projectPath,newName);
                }
            }
        }

        ShowRootIndicator();
    }

    // private
    auto ProjectExplorerPanel::InitUI() const ->void {
        d->ui->projectTreeWidget->clear();
        d->ui->projectTreeWidget->setStyleSheet("QTreeView:disabled { color: #F0F0F0; }");
        d->ui->projectTreeWidget->setColumnCount(2);
        d->ui->projectTreeWidget->hideColumn(d->typeColumnIndex);
        d->ui->projectTreeWidget->setHeaderLabels(QStringList(""));

        d->ui->projectTreeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
        d->ui->projectTreeWidget->header()->setStretchLastSection(false);

        d->ui->projectTreeWidget->setRootIsDecorated(false);

        d->ui->projectTreeWidget->setSortingEnabled(true);
        d->ui->projectTreeWidget->sortByColumn(d->typeColumnIndex, Qt::SortOrder::AscendingOrder);

        d->ui->projectTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(d->ui->projectTreeWidget, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(OnShowContextMenu(const QPoint&)));

        // set object names
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->projectTreeWidget->setObjectName("panel-contents");
        d->ui->expandButton->setObjectName("bt-expand-all");
        d->ui->collapseButton->setObjectName("bt-collapse-all");
    }

    auto ProjectExplorerPanel::ShowRootIndicator() const ->void {
        if(nullptr == d->ui->projectTreeWidget) {
            return;
        }

        const auto root = d->ui->projectTreeWidget->topLevelItem(0);
        if(nullptr == root) {
            return;
        }

        if (0 == root->childCount()) {
            return;
        }

        if (true == d->ui->projectTreeWidget->rootIsDecorated()) {
            return;
        }

        d->ui->projectTreeWidget->setRootIsDecorated(true);
    }

    auto ProjectExplorerPanel::LoadProjectInfo(const ProjectInfo::Pointer& project)->QTreeWidgetItem* {
        if(nullptr == project) {
           return nullptr;
        }

        d->projectPath = project->GetPath();
        d->projectName = project->GetName();

        auto projectItem = MakeItem(project->GetName(), ProjectItemFlag::Project);

        // playground
        for(const auto& playground : project->GetPlaygroundInfoList()) {
            auto playgroundItem = MakeItem(playground->GetName(), ProjectItemFlag::Playground);
            projectItem->addChild(playgroundItem);

            // hypercube
            for (const auto& hypercube : playground->GetHyperCubeList()) {
                const auto hypercubeItem = MakeItem(hypercube->GetName(), ProjectItemFlag::Hypercube);
                playgroundItem->addChild(hypercubeItem);

                // cube
                for (const auto& cube : hypercube->GetCubeList()) {
                    const auto cubeItem = MakeItem(cube->GetName(), ProjectItemFlag::Cube);
                    hypercubeItem->addChild(cubeItem);

                    // tcf
                    for(const auto& file : cube->GetTCFDirList()) {
                        QFileInfo fileInfo(file->GetPath());
                        const auto fileItem = MakeItem(fileInfo.fileName(), ProjectItemFlag::File);
                        cubeItem->addChild(fileItem);
                    }
                }
            }
        }

        // tcf
        if(false == project->GetTCFDirList().isEmpty()) {
            const auto folderItem = MakeItem(d->folderName, ProjectItemFlag::Folder_Group);
            projectItem->addChild(folderItem);

            QTreeWidgetItem* allWorksetsItem = nullptr;
            QMap<int, QTreeWidgetItem*> worksetTreeItems;

            if (!project->GetWorksetDirs().isEmpty()) {
                allWorksetsItem = MakeItem(d->worksetName, Folder_Group);
                projectItem->addChild(allWorksetsItem);
            }

            for (const auto& tcfDir : project->GetTCFDirList()) {
                if(const auto ids = project->GetWorksetIdOfTcfDir(tcfDir); !ids.isEmpty() && allWorksetsItem) {
                    for (const auto id : ids) {
                        auto worksetName = project->GetWorksetName(id);
                        auto* worksetItem = worksetTreeItems.contains(id) ? worksetTreeItems[id] : MakeItem(worksetName, ProjectItemFlag::Folder);
                        worksetTreeItems[id] = worksetItem;
                        allWorksetsItem->addChild(worksetItem);

                        ScanWorksetItem(worksetItem, tcfDir);
                    }
                } else {
                    const auto name = tcfDir->GetName();
                    const auto rootFolderItem = MakeItem(name, ProjectItemFlag::Root_Folder, tcfDir->GetPath());
                    folderItem->addChild(rootFolderItem);
                    ScanFolderItem(rootFolderItem, tcfDir);
                }
            }
        }

        return projectItem;
    }
    
    auto ProjectExplorerPanel::MakeItem(const QString& text, 
                                        const ProjectItemFlag& flag, 
                                        const QString& userData)->QTreeWidgetItem* {
        auto GetIcon = [=](const ProjectItemFlag& flag) {
            if (flag == ProjectItemFlag::Project) {
                return QIcon(":/image/images/project.png");
            }
            else if (flag == ProjectItemFlag::Playground) {
                return QIcon(":/img/ic-explorer-playground.svg");
            }
            else if (flag == ProjectItemFlag::Root_Folder) {
                return QIcon(":/img/ic-explorer-tcffiles.svg");
            }
            else if (flag == ProjectItemFlag::Folder) {
                return QIcon(":/img/ic-explorer-tcffolder.svg");
            }
            else if (flag == ProjectItemFlag::Folder_Group) {
                return QIcon(":/img/ic-explorer-tcffolder.svg");
            }
            else if (flag == ProjectItemFlag::Hypercube) {
                return QIcon(":/img/ic-explorer-hypercube-n.svg");
            }
            else if (flag == ProjectItemFlag::Cube) {
                return QIcon(":/img/ic-explorer-cube.svg");
            }
            else if (flag == ProjectItemFlag::File) {
                return QIcon();
            }

            return QIcon();
        };

        auto item = new QTreeWidgetItem(flag);
        //item->setFlags(item->flags() & Qt::NoItemFlags);
        item->setText(d->nameColumnIndex, text);
        item->setToolTip(d->nameColumnIndex, text);
        item->setIcon(d->nameColumnIndex, GetIcon(flag));
        item->setText(d->typeColumnIndex, QString::number(flag));
        item->setData(0, Qt::UserRole, userData);

        return item;
    }

    auto ProjectExplorerPanel::ScanFolderItem(QTreeWidgetItem* parentItem, const TCFDir::Pointer& dir)->void {
        for (const auto& file : dir->GetFileList()) {
            const auto fileItem = MakeItem(file, ProjectItemFlag::File);
            parentItem->addChild(fileItem);
        }

        for (const auto& sub : dir->GetDirList()) {
            const auto name = sub->GetName();
            const auto folderItem = MakeItem(name, ProjectItemFlag::Folder);
            parentItem->addChild(folderItem);

            ScanFolderItem(folderItem, sub);
        }
    }

    auto ProjectExplorerPanel::ScanWorksetItem(QTreeWidgetItem* parentItem, const TCFDir::Pointer& dir)->void {
        for (const auto& file : dir->GetFileList()) {
            const auto fileItem = MakeItem(file, ProjectItemFlag::Root_Folder, dir->GetPath());
            parentItem->addChild(fileItem);
        }
    }

    auto ProjectExplorerPanel::RemoveTCF(QTreeWidgetItem* item)->void {
        if (ProjectItemFlag::Root_Folder != item->type()) {
            return;
        }

        const auto answer = QMessageBox::question(nullptr, "Remove TCF",
                                                  "Are you sure you want to remove TCF Folder?",
                                                  QMessageBox::Yes | QMessageBox::No);

        if (QMessageBox::No == answer) {
            return;
        }

        emit removeTCF(d->projectPath, item->data(0, Qt::UserRole).toString());
    }

    void ProjectExplorerPanel::CollapseChild(QTreeWidgetItem* item) {
        if(nullptr == item) {
            return;
        }

        for (auto i = 0; i < item->childCount(); ++i) {
            const auto type = item->type();
            if (type== Cube || type == Root_Folder || type == Folder) {
                item->setExpanded(false);
            }
            else {
                item->setExpanded(true);
            }

            CollapseChild(item->child(i));
        }
    }
 
    void ProjectExplorerPanel::SaveState(QTreeWidgetItem* item, QStringList& state) const {
        if(nullptr == item) {
            return;
        }

        if(false == item->isExpanded()) {
            state.append(MakePath(item));
        }

        for (auto i = 0; i < item->childCount(); ++i) {
            if(item->type() == File) {
                continue;
            }

            SaveState(item->child(i), state);
        }
    }

    void ProjectExplorerPanel::RestoreState(QTreeWidgetItem* item, const QStringList& state) const {
        if (nullptr == item) {
            return;
        }

        if (true == state.contains(MakePath(item))) {
            item->setExpanded(false);
        }

        for (auto i = 0; i < item->childCount(); ++i) {
            if (item->type() == File) {
                continue;
            }

            RestoreState(item->child(i), state);
        }
    }

    auto ProjectExplorerPanel::MakePath(QTreeWidgetItem* item) const ->QString {
        QString path = item->text(d->nameColumnIndex);

        auto root = item;

        while (root->parent() != nullptr) {
            path = root->parent()->text(d->nameColumnIndex) + "/" + path;
            root = root->parent();
        }

        return path;
    }
}

