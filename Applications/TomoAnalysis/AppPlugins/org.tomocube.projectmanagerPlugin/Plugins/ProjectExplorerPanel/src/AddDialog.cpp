#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include "ui_AddDialog.h"
#include "AddDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct AddDialog::Impl {
        Ui::AddDialog* ui{ nullptr };

        QString path;
    };

    AddDialog::AddDialog(const QString& title, const QString& text, const QString& path, QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::AddDialog();
        d->ui->setupUi(this);

        d->path = path;

        this->setWindowTitle(title);
        d->ui->titleLabel->setText(text);
    }

    AddDialog::~AddDialog() = default;

    auto AddDialog::GetProjectName(const QString& path, QWidget* parent)->QString {
        const QString title = "New Project";

        AddDialog dialog(title, title, path, parent);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetName();
    }

    auto AddDialog::GetPlaygroundName(const QString& path, QWidget* parent)->QString {
        const QString title = "New Playground";

        AddDialog dialog(title, title, path, parent);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetName();
    }

    auto AddDialog::GetHypercubeName(QWidget* parent)->QString {
        const QString title = "New Hypercube";

        AddDialog dialog(title, title, QString(), parent);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetName();
    }

    auto AddDialog::GetCubeName(QWidget* parent)->QString {
        const QString title = "New Cube";

        AddDialog dialog(title, title, QString(), parent);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetName();
    }

    auto AddDialog::GetName() const ->QString {
        return d->ui->lineEdit->text();
    }

    void AddDialog::on_addButton_clicked() {
        if (true == d->ui->lineEdit->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a name", "Empty or invalid name");
            return;
        }

        if(true == this->windowTitle().contains("New Project") ||
           true == this->windowTitle().contains("New Playground")) {
            const QDir dir(d->path);
            const auto newFile = dir.filePath(d->ui->lineEdit->text());
            if (true == QFile::exists(newFile)) {
                QMessageBox::warning(nullptr, "Warning", "Cannot be created since a file already exists with the same path.");
                return;
            }
        }

        this->accept();
    }

    void AddDialog::on_cancelButton_clicked() {
        this->reject();
    }
}