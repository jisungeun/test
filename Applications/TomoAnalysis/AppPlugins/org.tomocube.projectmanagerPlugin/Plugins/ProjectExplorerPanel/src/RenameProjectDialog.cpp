#include <QMessageBox>
#include <QRegExpValidator>
#include <QStyle>

#include "ui_RenameProjectDialog.h"
#include "RenameProjectDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct RenameProjectDialog::Impl {
	    Ui::RenameProjectDialog* ui{nullptr};
		QString rstrip(const QString& str) {
			int n = str.size() - 1;
			for (; n >= 0; --n) {
				if (!str.at(n).isSpace()) {
					return str.left(n + 1);
				}
			}
			return "";
		}
	};
	RenameProjectDialog::RenameProjectDialog(QWidget* parent,const QString& name) :QDialog(parent),d{new Impl} {
	    d->ui = new Ui::RenameProjectDialog;
		d->ui->setupUi(this);
		d->ui->oriName->setText(name);
		d->ui->oriName->setReadOnly(true);

		QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
		d->ui->newName->setValidator(new QRegExpValidator(re));
		d->ui->newName->setPlaceholderText("Enter new name");

		d->ui->newName->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
		connect(d->ui->newName, &QLineEdit::textChanged, [=] { d->ui->newName->style()->polish(d->ui->newName); });

		connect(d->ui->okBtn,SIGNAL(clicked()),this,SLOT(OnOkBtn()));
		connect(d->ui->cancelBtn,SIGNAL(clicked()),this,SLOT(OnCancelBtn()));
	}
	RenameProjectDialog::~RenameProjectDialog() = default;
	auto RenameProjectDialog::Rename(QWidget* parent, const QString& name) -> QString {
		RenameProjectDialog dialog(parent,name);
		if(dialog.exec()!=QDialog::Accepted) {
		    return QString();
		}
		const auto newName = dialog.GetNewName();
        return newName;
    }

	auto RenameProjectDialog::GetNewName() const -> QString {
		auto result = d->ui->newName->text();
		result = d->rstrip(result);
        return d->ui->newName->text();
    }

	void RenameProjectDialog::OnCancelBtn() {
        reject();
    }
	void RenameProjectDialog::OnOkBtn() {
		if(true == d->ui->newName->text().isEmpty()) {
		    QMessageBox::warning(nullptr,"Enter a new name","Empty or invalid name");
			return;
		}
		auto resultName = d->ui->newName->text();
		resultName = d->rstrip(resultName);
		if(d->ui->oriName->text().compare(resultName)==0) {
            QMessageBox::warning(nullptr, "Enter a new name", "Empty or invalid name");
            return;
        }
        accept();
    }

}
