#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include "ui_LinkToPlaygroundDialog.h"
#include "LinkToPlaygroundDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct LinkToPlaygroundDialog::Impl {
        Ui::LinkToPlaygroundDialog* ui{ nullptr };
    };

    LinkToPlaygroundDialog::LinkToPlaygroundDialog(const QStringList& items, QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::LinkToPlaygroundDialog();
        d->ui->setupUi(this);

        d->ui->playgroundComboBox->addItems(items);
    }

    LinkToPlaygroundDialog::~LinkToPlaygroundDialog() = default;

    auto LinkToPlaygroundDialog::Exec(const QStringList& items, QWidget* parent)->QString {
        LinkToPlaygroundDialog dialog(items, parent);

        if (dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        return dialog.GetItemText();
    }

    auto LinkToPlaygroundDialog::GetItemText(void) const->QString {
        return d->ui->playgroundComboBox->currentText();
    }

    void LinkToPlaygroundDialog::on_linkButton_clicked() {
        this->accept();
    }

    void LinkToPlaygroundDialog::on_cancelButton_clicked() {
        this->reject();
    }
}