#pragma once

#include <memory>
#include <QWidget>
#include <QTreeWidgetItem>
#include <IProjectExplorerPanel.h>

#include "ProjectExplorerPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class ProjectExplorerPanel_API ProjectExplorerPanel : public QWidget, public Interactor::IProjectExplorerPanel {
        Q_OBJECT

    public:
        typedef ProjectExplorerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ProjectExplorerPanel(QWidget* parent=nullptr);
        ~ProjectExplorerPanel();

        auto Init(const ProjectInfo::Pointer& project)->bool override;
        auto Update(const ProjectInfo::Pointer& project)->bool override;

    protected:
        enum ProjectItemFlag {
            Project,
            Playground,
            Folder_Group,
            Root_Folder,
            Folder,
            Hypercube,
            Cube,
            File,
        };
    signals:
        void removeTCF(const QString& projectPath, const QString& path);
        void renameProject(QString projectPath,QString newName);

    private slots:
        void on_expandButton_clicked() const;
        void on_collapseButton_clicked();

        void OnShowContextMenu(const QPoint&);

    private:
        auto InitUI() const ->void;
        auto ShowRootIndicator() const ->void;

        auto LoadProjectInfo(const ProjectInfo::Pointer& project)->QTreeWidgetItem*;
        auto MakeItem(const QString& text, const ProjectItemFlag& flag, const QString& userData = "")->QTreeWidgetItem*;
        auto ScanFolderItem(QTreeWidgetItem* parentItem, const TCFDir::Pointer& dir)->void;
        auto ScanWorksetItem(QTreeWidgetItem* parentItem, const TCFDir::Pointer& dir)->void;

        auto RemoveTCF(QTreeWidgetItem* item)->void;

        void CollapseChild(QTreeWidgetItem* item);

        void SaveState(QTreeWidgetItem* item, QStringList& state) const;
        void RestoreState(QTreeWidgetItem* item, const QStringList& state) const;
        auto MakePath(QTreeWidgetItem* item) const ->QString;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}