#pragma once

#include <memory>
#include <QDialog>
#include <QTreeWidgetItem>

namespace TomoAnalysis::ProjectManager::Plugins {
    class LinkToPlaygroundDialog : public QDialog {
        Q_OBJECT

    public:
        explicit LinkToPlaygroundDialog(const QStringList& items, QWidget* parent = nullptr);
        virtual ~LinkToPlaygroundDialog();

    public:
        static auto Exec(const QStringList& items, QWidget* parent = nullptr)->QString;

        auto GetItemText(void) const->QString;

    protected slots:
        void on_linkButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
