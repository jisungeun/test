#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class AddDialog : public QDialog {
        Q_OBJECT

    public:
        explicit AddDialog(const QString& title, const QString& text, const QString& path, QWidget* parent = nullptr);
        virtual ~AddDialog();

    public:
        static auto GetProjectName(const QString& path, QWidget* parent = nullptr)->QString;
        static auto GetPlaygroundName(const QString& path, QWidget* parent = nullptr)->QString;
        static auto GetHypercubeName(QWidget* parent = nullptr)->QString;
        static auto GetCubeName(QWidget* parent = nullptr)->QString;

        auto GetName() const ->QString;

    protected slots:
        void on_addButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
