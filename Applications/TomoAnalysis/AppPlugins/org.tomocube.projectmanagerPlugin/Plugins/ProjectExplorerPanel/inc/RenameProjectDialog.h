#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class RenameProjectDialog : public QDialog {
        Q_OBJECT
    public:
        explicit RenameProjectDialog(QWidget* parent,const QString& name);
        virtual ~RenameProjectDialog();

        static auto Rename(QWidget* parent,const QString& name)->QString;
        auto GetNewName(void)const->QString;

    protected slots:
        void OnOkBtn();
        void OnCancelBtn();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}