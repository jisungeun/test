#pragma once

#include <IProjectDataReadPort.h>

#include "ProjectDataIOExport.h"

namespace TomoAnalysis::ProjectManager::Plugins::ProjectDataIO {
	class ProjectDataIO_API TAProjectDataReader : public UseCase::IProjectDataReadPort {
	public:
		TAProjectDataReader();
		~TAProjectDataReader();

		auto ReadProjectData(const QString& path, ProjectInfo::Pointer& projectInfo)->bool override;
		auto ReadPlaygroundData(const QString& path, PlaygroundInfo::Pointer& playgroundInfo)->bool override;			
	};
}