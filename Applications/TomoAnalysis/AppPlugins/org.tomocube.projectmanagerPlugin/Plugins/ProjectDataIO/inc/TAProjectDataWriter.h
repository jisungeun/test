#pragma once

#include <IProjectDataWritePort.h>

#include "ProjectDataIOExport.h"

namespace TomoAnalysis::ProjectManager::Plugins::ProjectDataIO {
	class ProjectDataIO_API TAProjectDataWriter : public UseCase::IProjectDataWritePort {
	public:
		TAProjectDataWriter();
		~TAProjectDataWriter();

		auto WriteProjectData(const QString& path, const ProjectInfo::Pointer& projectInfo)->bool override;
		auto WritePlaygroundData(const QString& path, const PlaygroundInfo::Pointer& playgroundInfo)->bool override;
	};
}
