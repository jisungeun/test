#include <iostream>

#include <QDirIterator>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include <ProjectDataReader.h>

#include "TAProjectDataReader.h"

namespace TomoAnalysis::ProjectManager::Plugins::ProjectDataIO {
	TAProjectDataReader::TAProjectDataReader() {

	}

	TAProjectDataReader::~TAProjectDataReader() = default;

	auto TAProjectDataReader::ReadProjectData(const QString& path, ProjectInfo::Pointer& projectInfo)->bool {
		auto reader = std::make_shared<ProjectDataReader>();
		return reader->ReadProjectData(path, projectInfo);		
	}
    
	auto TAProjectDataReader::ReadPlaygroundData(const QString& path, PlaygroundInfo::Pointer& playgroundInfo)->bool {
		auto reader = std::make_shared<ProjectDataReader>();
		return reader->ReadPlaygroundData(path, playgroundInfo);
	}
}
