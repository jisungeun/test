#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

#include <ProjectDataWriter.h>

#include "TAProjectDataWriter.h"

namespace TomoAnalysis::ProjectManager::Plugins::ProjectDataIO {
	TAProjectDataWriter::TAProjectDataWriter() {

	}

	TAProjectDataWriter::~TAProjectDataWriter() = default;

	auto TAProjectDataWriter::WriteProjectData(const QString& path, const ProjectInfo::Pointer& projectInfo)->bool {
		auto writer = new ProjectDataWriter;
	    auto result = writer->WriteProjectData(path,projectInfo);
		return result;
	}
	auto TAProjectDataWriter::WritePlaygroundData(const QString& path, const PlaygroundInfo::Pointer& playgroundInfo) -> bool {
		auto writer = new ProjectDataWriter;
		auto result = writer->WritePlaygroundData(path, playgroundInfo);
		return result;
    }

}

