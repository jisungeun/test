#include <iostream>
#include <QMenu>
#include <qtextedit.h>

#include "ui_ItemInfoPanel.h"
#include "ItemInfoPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct ItemInfoPanel::Impl {
        Ui::ItemInfoPanel* ui{nullptr};

        QTreeWidgetItem* currentItem{ nullptr };

        QString currentApp;
        QString currentTcf;

        const int nameColumnIndex = 0;
        const int typeColumnIndex = 1;
        const QString removeTCF = "Unlink TCF item";
        const QString removeCube = "Unlink Cube item";
        const QString removeApp = "Unlink Application";
        const QString executeApp = "Execute App Examiner";
    };
    ItemInfoPanel::ItemInfoPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ItemInfoPanel;
        d->ui->setupUi(this);

        this->Init();
    }
    ItemInfoPanel::~ItemInfoPanel() {
        
    }
    auto ItemInfoPanel::Update(const TCFDir::Pointer& tcfItem) -> void {
        d->ui->itemTree->clear();
        d->ui->itemTree->setRootIsDecorated(false);
        //load tcf list
        auto name = tcfItem->GetPath();        
        auto RootItem = CreateItem(name, ItemType::RootTCF);                                

        for (const auto& file : tcfItem->GetFileList()) {            
            auto fullPath = name + "/"+file;
            auto childItem = CreateItem(file, ItemType::TCF,fullPath);
            RootItem->addChild(childItem);            
        }
        d->ui->itemTree->addTopLevelItem(RootItem);
        RootItem->setExpanded(true);

        this->ShowRootIndicator();        
    }
    auto ItemInfoPanel::Update(const Cube::Pointer& cubeItem) -> void {        
        d->ui->itemTree->clear();
        d->ui->itemTree->setRootIsDecorated(false);

        auto name = cubeItem->GetName();
        auto RootItem = CreateItem(name, ItemType::RootCube,name);
        QTreeWidgetItem* tcfSeparator = nullptr;
        QTreeWidgetItem* appSeparator = nullptr;
        if (cubeItem->GetTCFDirList().size() > 0) {
            tcfSeparator = CreateItem("TCFDirs", ItemType::None);
            RootItem->addChild(tcfSeparator);
            for (const auto& tcf : cubeItem->GetTCFDirList()) {
                auto tcfName = tcf->GetPath();
                auto childItem = CreateItem(tcfName, ItemType::ChildTCF,tcfName);
                for (const auto tcfItem : tcf->GetFileList()) {
                    auto fullPath = tcfName + "/" +tcfItem;
                    auto grandItem = CreateItem(tcfItem, ItemType::TCF,fullPath);
                    childItem->addChild(grandItem);
                }
                tcfSeparator->addChild(childItem);                
            }            
        }
        if (cubeItem->GetMeasurementAppInfoList().size() > 0) {
            appSeparator = CreateItem("Application", ItemType::None);
            RootItem->addChild(appSeparator);
            for (const auto& app : cubeItem->GetMeasurementAppInfoList()) {
                auto appName = app->GetName();
                auto appPath = app->GetPath();
                auto childItem = CreateItem(appName, ItemType::ChildApplication,appPath);
                appSeparator->addChild(childItem);                
            }            
        }

        d->ui->itemTree->addTopLevelItem(RootItem);        
        RootItem->setExpanded(true);
        if(nullptr != tcfSeparator) {
            tcfSeparator->setExpanded(true);
        }
        if(nullptr != appSeparator) {
            appSeparator->setExpanded(true);
        }

        this->ShowRootIndicator();
    }
    auto ItemInfoPanel::Update(const HyperCube::Pointer& hyperItem) -> void {
        d->ui->itemTree->clear();
        d->ui->itemTree->setRootIsDecorated(false);

        auto name = hyperItem->GetName();        
        auto RootItem = CreateItem(name,ItemType::RootHypercube,name);
        QTreeWidgetItem* cubeSeparator = nullptr;
        QTreeWidgetItem* appSeparator = nullptr;
        if(hyperItem->GetCubeList().size()>0) {
            cubeSeparator = CreateItem("Cubes", ItemType::None);
            RootItem->addChild(cubeSeparator);
            for(const auto &cube:hyperItem->GetCubeList()) {
                auto cubeName = cube->GetName();                
                auto childItem = CreateItem(cubeName, ItemType::ChildCube,cubeName);
                for(const auto &tcfDir : cube->GetTCFDirList()) {
                    auto dirName = tcfDir->GetPath();
                    auto dirItem = CreateItem(dirName, ItemType::Folder);
                    for(const auto &file : tcfDir->GetFileList()) {
                        auto fullPath = dirName +"/"+ file;
                        auto fileItem = CreateItem(file, ItemType::TCF,fullPath);
                        dirItem->addChild(fileItem);
                    }
                    childItem->addChild(dirItem);
                }
                cubeSeparator->addChild(childItem);
            }
        }
        if(hyperItem->GetMeasurementAppInfoList().size()>0) {
            appSeparator = CreateItem("Application", ItemType::None);
            RootItem->addChild(appSeparator);
            for (const auto& app : hyperItem->GetMeasurementAppInfoList()) {
                auto appName = app->GetName();
                auto appPath = app->GetPath();
                auto childItem = CreateItem(appName, ItemType::ChildApplication,appPath);
                appSeparator->addChild(childItem);
            }            
        }

        d->ui->itemTree->addTopLevelItem(RootItem);
        RootItem->setExpanded(true);
        if (nullptr != cubeSeparator) {
            cubeSeparator->setExpanded(true);
        }
        if (nullptr != appSeparator) {
            appSeparator->setExpanded(true);
        }

        this->ShowRootIndicator();
    }
    auto ItemInfoPanel::Update(const PluginAppInfo::Pointer& appItem) -> void {
        Q_UNUSED(appItem)
        d->ui->itemTree->clear();
        d->ui->itemTree->setRootIsDecorated(false);                

        auto description = new QTreeWidgetItem;
        //TODO application instruction 저장 방법 선택 필요
        description->setText(0, "Some instruction\nSample application\nparameter meanings");
        d->ui->itemTree->addTopLevelItem(description);

        this->ShowRootIndicator();
    }
    auto ItemInfoPanel::Clear() -> void {
        d->ui->itemTree->clear();
        d->ui->itemTree->setRootIsDecorated(false);

        this->ShowRootIndicator();
    }
    auto ItemInfoPanel::Init() -> void {
        d->ui->itemTree->clear();
        d->ui->itemTree->setColumnCount(2);
        d->ui->itemTree->hideColumn(d->typeColumnIndex);
        d->ui->itemTree->setHeaderLabels(QStringList(""));

        d->ui->itemTree->setRootIsDecorated(false);

        d->ui->itemTree->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
        d->ui->itemTree->setSortingEnabled(true);
        d->ui->itemTree->sortByColumn(d->typeColumnIndex, Qt::SortOrder::AscendingOrder);
        d->ui->itemTree->setContextMenuPolicy(Qt::CustomContextMenu);

        connect(d->ui->itemTree, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(OnShowContextMenu(const QPoint&)));
        connect(d->ui->itemTree, SIGNAL(itemClicked(QTreeWidgetItem * , int )), this,SLOT(OnItemClicked(QTreeWidgetItem*,int)));
    }
    void ItemInfoPanel::OnItemClicked(QTreeWidgetItem* item,int column) {        
        auto GetItemType = [=](const ItemType& flag) {
            if (flag == ItemType::RootApplication || flag == ItemType::ChildApplication) {
                return ItemType::Application;
            }
            else if (flag == ItemType::RootCube || flag == ItemType::ChildCube) {
                return ItemType::Cube;
            }
            else if (flag == ItemType::RootHypercube) {
                return ItemType::Hypercube;
            }
            else if (flag == ItemType::RootTCF || flag == ItemType::ChildTCF) {
                return ItemType::Folder;
            }
            return flag;
        };

        auto type = item->type();
        if (ItemType::Application == GetItemType((ItemType)type)) {
            auto appPath = QString("Application!");
            appPath += item->toolTip(column);            
            d->currentApp = appPath;
            auto parent = item->parent()->parent();
            auto parentType = parent->type();
            QString parentName;
            if(ItemType::Hypercube == GetItemType((ItemType)parentType)) {
                parentName = QString("HyperCube!");                
            }else if (ItemType::Cube == GetItemType((ItemType)parentType)) {
                parentName = QString("Cube!");                
            }
            parentName += parent->toolTip(column);

            emit applicationSelected(appPath,parentName);
        }else if(ItemType::TCF==GetItemType((ItemType)type)) {
            auto tcfPath = item->toolTip(column);
            d->currentTcf = tcfPath;

            emit tcfSelected(tcfPath);
        }
    }

    void ItemInfoPanel::OnShowContextMenu(const QPoint& pos) {
        auto item = d->ui->itemTree->itemAt(pos);
        if(nullptr == item) {
            return;
        }

        const auto globalPos = d->ui->itemTree->viewport()->mapToGlobal(pos);

        QMenu menu;
        QAction* selectedItem{ nullptr };

        const auto flag = (ItemType)item->type();
        if(flag == ItemType::ChildTCF) {
            menu.addAction(d->removeTCF);
            selectedItem = menu.exec(globalPos);
        }else if(flag == ItemType::ChildCube) {
            menu.addAction(d->removeCube);
            selectedItem = menu.exec(globalPos);
        }else if(flag == ItemType::ChildApplication) {            
            menu.addAction(d->removeApp);
            selectedItem = menu.exec(globalPos);
        }else if(flag == ItemType::TCF) {
            menu.addAction(d->executeApp);
            selectedItem = menu.exec(globalPos);
        }

        if(nullptr != selectedItem) {
            const auto text = selectedItem->text();
            if(true == text.contains(d->removeTCF)) {
                this->RemoveTCFItem(item,item->parent()->parent());
            }else if(true == text.contains(d->removeCube)) {
                this->RemoveCubeItem(item,item->parent()->parent());
            }else if(true==text.contains(d->removeApp)) {
                this->RemoveAppItem(item,item->parent()->parent());
            }else if(true==text.contains(d->executeApp)) {
                this->ExecuteApp(item);
            }
        }
    }    
    auto ItemInfoPanel::RemoveAppItem(QTreeWidgetItem* item, QTreeWidgetItem* parent) -> void {        
        auto GetItemType = [=](const ItemType& flag) {
            if (flag == ItemType::RootApplication || flag == ItemType::ChildApplication) {
                return ItemType::Application;
            }
            else if (flag == ItemType::RootCube || flag == ItemType::ChildCube) {
                return ItemType::Cube;
            }
            else if (flag == ItemType::RootHypercube) {
                return ItemType::Hypercube;
            }
            else if (flag == ItemType::RootTCF || flag == ItemType::ChildTCF) {
                return ItemType::Folder;
            }
            return flag;
        };
        auto targetApp = item->toolTip(0);
        auto fromItem = parent->toolTip(0);

        auto type = parent->type();
        if(GetItemType((ItemType)type) == ItemType::Cube) {
            emit removeAppFromCubeRequested(targetApp, fromItem);
        }else if(GetItemType((ItemType)type)==ItemType::Hypercube) {
            emit removeAppFromHyperRequested(targetApp, fromItem);
        }        
    }
    auto ItemInfoPanel::RemoveCubeItem(QTreeWidgetItem* item, QTreeWidgetItem* parent) -> void {                
        auto cubeName = item->toolTip(0);
        auto hyperName = parent->toolTip(0);
        emit removeCubeRequested(cubeName, hyperName);
    }
    auto ItemInfoPanel::RemoveTCFItem(QTreeWidgetItem* item, QTreeWidgetItem* parent) -> void {
        auto tcfPath = item->toolTip(0);
        auto cubeName = parent->toolTip(0);
        emit removeTcfRequested(tcfPath, cubeName);
    }
    auto ItemInfoPanel::ExecuteApp(QTreeWidgetItem* item) -> void {
        if (false == d->currentApp.isEmpty()) {
            auto tcfPath = item->toolTip(0);
            emit executeAppRequested(d->currentApp, tcfPath);
        }
    }    

    auto ItemInfoPanel::ShowRootIndicator() const -> void {
        if (nullptr == d->ui->itemTree) {
            return;
        }

        const auto root = d->ui->itemTree->topLevelItem(0);
        if (nullptr == root) {
            return;
        }

        if (0 == root->childCount()) {
            return;
        }

        if (true == d->ui->itemTree->rootIsDecorated()) {
            return;
        }

        d->ui->itemTree->setRootIsDecorated(true);        
    }
    auto ItemInfoPanel::CreateItem(const QString& name, const int& flag, const QString& path, const QString& userData) -> QTreeWidgetItem* {
        auto GetItemType = [=](const ItemType& flag) {
            if(flag == ItemType::RootApplication || flag == ItemType::ChildApplication) {
                return ItemType::Application;
            }else if(flag == ItemType::RootCube || flag == ItemType::ChildCube) {
                return ItemType::Cube;
            }else if(flag == ItemType::RootHypercube) {
                return ItemType::Hypercube;
            }else if(flag == ItemType::RootTCF || flag == ItemType::ChildTCF) {
                return ItemType::Folder;
            }
            return flag;
        };

        auto GetIcon = [=](const ItemType& flag) {
            if (flag == ItemType::Application) {
                return QIcon(":/image/images/App.png");
            }
            else if (flag == ItemType::Hypercube) {
                return QIcon(":/image/images/Hypercube.png");
            }
            else if (flag == ItemType::Cube) {
                return QIcon(":/image/images/Cube.png");
            }
            else if (flag == ItemType::TCF) {
                return QIcon(":/image/images/Item.png");
            }
            else if (flag == ItemType::Folder) {
                return QIcon(":/image/images/folder.png");
            }
            return QIcon();
        };

        auto item = new QTreeWidgetItem(flag);
        item->setText(d->nameColumnIndex, name);
        item->setToolTip(d->nameColumnIndex, path);
        item->setIcon(d->nameColumnIndex, GetIcon(GetItemType((ItemType)flag)));
        item->setText(d->typeColumnIndex, QString::number(flag));

        if(false == userData.isEmpty()) {
            //set user data
        }

        return item;
    }
}
