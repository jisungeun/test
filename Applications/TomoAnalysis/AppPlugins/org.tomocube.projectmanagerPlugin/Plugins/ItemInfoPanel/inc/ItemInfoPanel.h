#pragma once

#include <memory>
#include <QWidget>
#include <QTreeWidgetItem>
#include <IItemInfoPanel.h>

#include "ItemInfoPanelExport.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    class ItemInfoPanel_API ItemInfoPanel : public QWidget, public Interactor::IItemInfoPanel {
        Q_OBJECT
        enum ItemType {
            RootTCF = 1000,
            RootCube = 1001,
            RootHypercube = 1002,
            RootApplication = 1003,

            ChildTCF = 2000,
            ChildFolder = 2001,
            ChildCube = 2002,
            ChildApplication = 2003,

            None = 3000,
            TCF = 3001,
            Cube = 3002,
            Hypercube = 3003,
            Application = 3004,
            Folder =3005
        };
    public:
        typedef ItemInfoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ItemInfoPanel(QWidget* parent = nullptr);
        ~ItemInfoPanel();

        auto Update(const Cube::Pointer& cubeItem) -> void override;
        auto Update(const HyperCube::Pointer& hypercubeItem) -> void override;
        auto Update(const PluginAppInfo::Pointer& infoItem) -> void override;
        auto Update(const TCFDir::Pointer& tcfItem) -> void override;
        auto Clear() -> void override;

    signals:
        void removeAppFromCubeRequested(const QString& app, const QString& cube);
        void removeAppFromHyperRequested(const QString& app, const QString& hyper);
        void removeCubeRequested(const QString& cube, const QString& hyper);
        void removeTcfRequested(const QString& tcf, const QString& cube);
        void tcfSelected(const QString& tcfPath);
        void applicationSelected(const QString& app,const QString& parent);
        void executeAppRequested(const QString& app, const QString& tcf);

    protected slots:
        void OnShowContextMenu(const QPoint& pos);
        void OnItemClicked(QTreeWidgetItem* item,int column);

    private:
        auto Init()->void;

        auto RemoveTCFItem(QTreeWidgetItem* item, QTreeWidgetItem* parent)->void;
        auto RemoveCubeItem(QTreeWidgetItem* item, QTreeWidgetItem* parent)->void;
        auto RemoveAppItem(QTreeWidgetItem* item, QTreeWidgetItem* parent)->void;
        auto ExecuteApp(QTreeWidgetItem* item)->void;

        auto ShowRootIndicator()const->void;
        auto CreateItem(const QString& name, const int& flag, const QString& path = QString(),const QString& userData = "")->QTreeWidgetItem*;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}