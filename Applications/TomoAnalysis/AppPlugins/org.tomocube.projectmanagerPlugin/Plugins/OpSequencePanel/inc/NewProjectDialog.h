#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class NewProjectDialog : public QDialog {
        Q_OBJECT

    public:
        explicit NewProjectDialog(QWidget* parent = nullptr);
        virtual ~NewProjectDialog();

    public:
        static auto GetCreateProjectPath(QWidget* parent = nullptr)->QString;

        auto GetName(void) const ->QString;
        auto GetLocation(void) const ->QString;

        auto GenerateProjectFilePath() const->QString;

    protected slots:
        void on_createButton_clicked();
        void on_cancelButton_clicked();
        void on_pathButton_clicked();


    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
