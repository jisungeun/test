#pragma once

#include <QDialog>

#include "CilsContainer.h"
#include "Item.h"
#include "Workset.h"

#include "OpSequencePanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class OpSequencePanel_API CilsWorksetDialog final : public QDialog{
		Q_OBJECT

	public:
		explicit CilsWorksetDialog(CilsContainer* container, QWidget * parent = nullptr);
		~CilsWorksetDialog() override;

		auto GetSelectedTcfImage() const-> QMap<QString, QPixmap>;
		auto GetSelectedTcfsWithWorkset() const -> const QMap<int, QStringList>&;

	protected slots:
		auto OnClearBtnClicked() -> void;
		auto OnSelAllBtnClicked() -> void;
		auto OnReloadBtnClicked() -> void;
		auto OnInitializeBtnClicked() -> void;
		auto OnItemSelectionChanged() -> void;
		auto OnStateChanged(int checked) -> void;
		auto OnSearchChanged(const QString& text) -> void;

		auto OnError() -> void;
		auto OnWorksetLoaded(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void;
		auto OnItemLoaded(int id, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void;
		auto OnPreviewLoaded(const QString& dataId, const QByteArray& image) -> void;
		
		auto OnPreviewSelected(const QString& dataId) -> void;
		auto OnPreviewHovered(const QString& dataId, bool hovered) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
