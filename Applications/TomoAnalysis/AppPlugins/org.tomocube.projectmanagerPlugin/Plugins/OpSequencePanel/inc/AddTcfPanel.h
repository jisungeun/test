#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class AddTcfPanel : public QWidget {
        Q_OBJECT
    public:
        AddTcfPanel(QWidget* parent = nullptr);
        ~AddTcfPanel();

    signals:
        void sigAdd(QString);
        void sigAddInCils(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&);

    protected slots:
        void OnAddBtn();
        void OnCilsBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}