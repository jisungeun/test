#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class SelectAppPanel : public QWidget {
        Q_OBJECT
    public:
        SelectAppPanel(QWidget* parent = nullptr);
        ~SelectAppPanel();

        auto ClearAppList()->void;
        auto ClearHyperList()->void;
        auto AppendApp(const QString& app)->void;
        auto AppendHyper(const QString& hyper)->void;
        auto AppendAppFull(const QString& full_name)->void;
        auto AppendProc(const QStringList& proc_path)->void;
        auto AppendRunType(const std::tuple<bool,bool>& runType)->void;

    signals:
        void sigApp(QString,QString,QStringList, QString,std::tuple<bool,bool>);

    protected slots:
        void OnOkBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}