#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class TcfLinkPanel : public QWidget {
        Q_OBJECT
    public:
        TcfLinkPanel(QWidget* parent = nullptr);
        ~TcfLinkPanel();

        auto AppendCube(const QString& name)->void;
        auto SetCount(const QString& name,int count)->void;
        auto ClearTable()->void;

    signals:

    protected slots:

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}