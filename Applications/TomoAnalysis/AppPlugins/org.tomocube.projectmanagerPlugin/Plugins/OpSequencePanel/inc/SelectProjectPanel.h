#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class SelectProjectPanel : public QWidget {
        Q_OBJECT
    public:
        SelectProjectPanel(QWidget* parent = nullptr);
        ~SelectProjectPanel();

        auto showCurrentProjectPath(const QString& path)->void;
        auto hideCurrentProjectPath()->void;
        auto getCurrentProjectPath()const->QString;

    signals:
        void sigOpen(QString);
        void sigCreate(QString);

    protected slots:
        void OnNewButton();
        void OnOpenButton();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}