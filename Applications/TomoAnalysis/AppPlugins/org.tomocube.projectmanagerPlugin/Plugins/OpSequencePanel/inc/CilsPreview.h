#pragma once

#include <optional>

#include "GalleryView.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class CilsPreview : public TC::GalleryView {
		Q_OBJECT

	public:
		CilsPreview(QWidget* parent = nullptr);
		~CilsPreview() override;

		auto SetCurrentWorkset(int worksetId, const QStringList& items) -> void;
		auto SetPixmap(const QString& dataId, const QPixmap& pixmap) -> void;

		auto GetPixmap(const QString& dataId) -> std::optional<QPixmap>;

		auto SelectAllItems() -> void;
		auto ClearSelectedItems() -> void;
		auto GetSelectedItems() const -> const QMap<int, QStringList>&;
		auto GetNonpreviewDataId() const -> QString;

		auto ShowSelectedItems() -> void;
		auto ShowWorksetItems(bool show) -> void;

		auto Refresh() -> void;

	protected:
		auto eventFilter(QObject*, QEvent*) -> bool override;
		auto keyReleaseEvent(QKeyEvent* event) -> void override;
		auto keyPressEvent(QKeyEvent*) -> void override;

		auto OnItemClicked(int index, QMouseEvent* event) -> void;
		auto OnItemHovered(int index, bool hovered) -> void;

	signals:
		auto Selected(const QString& dataId) -> void;
		auto Hovered(const QString& dataId, bool hovered) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
