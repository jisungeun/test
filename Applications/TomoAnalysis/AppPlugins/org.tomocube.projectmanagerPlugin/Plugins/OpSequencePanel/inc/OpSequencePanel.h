#pragma once

#include <memory>
#include <QWidget>

#include <IOpSequencePanel.h>

#include "OpSequencePanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {   
    class OpSequencePanel_API OpSequencePanel : public QWidget, public Interactor::IOpSequencePanel {
        Q_OBJECT
    public:
        typedef OpSequencePanel Self;
        typedef std::shared_ptr<Self> Pointer;

        OpSequencePanel(QWidget* parent = nullptr);
        ~OpSequencePanel();

        auto Update(ProjectInfo::Pointer project) -> bool override;

        auto ClearHyperHolder()->void;
        auto GetCurrentProject()->QString;
        auto ForceStep(int step)->void;

    signals:
        void tcfAdd(QString,QString);
        void createCube(QString, QString,QString);
        void createHyper(QString,QString);
        void createPG(QString);
        void connectApp(QString, QString,QStringList, QString,QString,std::tuple<bool,bool>);
        void createProj(QString);
        void openProj(QString);
        void cilsTcfAdd(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&);

        void OnLinkTab(bool);

    protected slots:
        //slot for tab change
        void OnAddTcf(bool);
        void OnAddTcfInCils(const QMap<int, QStringList>&, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>&);
        void OnCreateCube(bool);
        void OnCreateHypercube(bool);
        void OnLinkTcf(bool);
        void OnPlayground(bool);
        void OnSelectApp(bool);
        void OnSelectProj(bool);
        void OnPrevBtn();
        void OnNextBtn();

        //slot for project create/open
        void OnProjectOpen(QString path);
        void OnProjectCreate(QString path);
        //slot for tcf management
        void OnTcfAdd(QString path);
        //slot for playground management
        void OnMakePlayground(QString name);

        //slot for hypercube management
        void OnMakeHypercube(QString name);

        //slot for cube management
        void OnMakeCube(QString cube, QString hyper);

        //slot for tcf linker
        //slot for app selection
        void OnAppOpen(QString app,QString appfull, QStringList procList, QString hyper,std::tuple<bool,bool>);

    private:
        auto Init()->void;
        auto InitButtons()->void;
        auto InitArrows()->void;
        auto InitConnections()->void;
        auto removePanels()->void;

        auto UpdateButtons(ProjectInfo::Pointer project)->void;
        auto ClearBtnColor()->void;

        struct Impl;
        std::unique_ptr < Impl > d;
    };
}