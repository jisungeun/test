#pragma once
#include <memory>

#include <QPushButton>

class ImageButton : public QPushButton {
	Q_OBJECT
public:
	explicit ImageButton(QWidget* parent = nullptr);
	auto setPad(int pad)->void;
	auto setNoBackground()->void;
	auto setFocusable()->void;
	auto setDefault()->void;
protected:
	//virtual void resizeEvent(QResizeEvent* event) override;
	virtual void paintEvent(QPaintEvent* event) override;
private:
	struct Impl;
	std::unique_ptr<Impl> d;
};