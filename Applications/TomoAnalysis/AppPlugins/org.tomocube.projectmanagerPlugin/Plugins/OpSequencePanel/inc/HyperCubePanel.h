#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class HyperCubePanel : public QWidget {
        Q_OBJECT
    public:
        HyperCubePanel(QWidget* parent = nullptr);
        ~HyperCubePanel();

    signals:
        void sigHyper(QString);

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void OnMakeBtn();        

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}