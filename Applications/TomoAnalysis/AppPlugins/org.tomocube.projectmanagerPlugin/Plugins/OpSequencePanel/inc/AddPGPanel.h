#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class AddPGPanel : public QWidget {
        Q_OBJECT
    public:
        AddPGPanel(QWidget* parent = nullptr);
        ~AddPGPanel();

        auto setDefaultName(const QString& name)->void;
        auto setCurName(const QString& name)->void;
        auto getCurName()const->QString;

        auto Clear()->void;

    signals:
        void sigPG(QString);

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void OnOkBtn();
        void OnNewBtn();

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}