#pragma once

#include <memory>

#include <QByteArray>
#include <QVector>

#include "Item.h"
#include "Workset.h"

#include "OpSequencePanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class OpSequencePanel_API CilsContainer {
	public:
		CilsContainer();
		~CilsContainer();

		auto Clear() -> void;

		auto SetWorksets(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void;
		auto SetItems(int worksetId, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void;

		auto GetAnyEmptyWorkset() const -> int;
		auto IsEmptyWorkset(int id) const -> bool;
		auto IsWorksetsEmpty() const -> bool;
		auto IsItemsAllLoaded(int id) -> bool;

		auto GetWorksets() const -> const QVector<TC::Cils::JsonEntity::Workset>&;
		auto GetItems(int worksetId) const ->QStringList;
		auto GetItemDetails(int worksetId) const->const QVector<TC::Cils::JsonEntity::Item>&;
		auto GetItemSize(const QString& dataId) const ->unsigned long long;
		auto GetItemTitle(const QString& dataId) const ->QString;

		auto Search(const QString& text) const ->QVector<TC::Cils::JsonEntity::Workset>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
