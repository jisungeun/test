#pragma once

#include <memory>
#include <QWidget>

namespace TomoAnalysis::ProjectManager::Plugins {
    class CubePanel : public QWidget {
        Q_OBJECT
    public:
        CubePanel(QWidget* parent = nullptr);
        ~CubePanel();

        auto AppendHyperCube(const QString& name)->void;
        auto ClearHyperList()->void;
        auto ClearHyperSel()->void;

    signals:
        void sigCube(QString,QString);

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void OnMakeBtn();
        void OnHyperSelected(int);

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}