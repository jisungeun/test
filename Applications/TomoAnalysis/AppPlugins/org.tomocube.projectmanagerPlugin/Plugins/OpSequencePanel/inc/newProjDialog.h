#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class newProjDialog : public QDialog {
        Q_OBJECT
    public:
        explicit newProjDialog(QWidget* parent = nullptr);
        virtual ~newProjDialog();

        auto GetName()const->QString;
        auto GetPath()const->QString;

        auto Init()->void;

    public:
        static auto GetProjectName(QWidget* parent = nullptr)->QString;

    protected slots:
        void OnCreateBtn();
        void OnCancelBtn();
        void OnFolderBtn();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        
    };
}