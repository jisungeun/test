#include <QFileDialog>

#include "ui_AddTcfPanel.h"
#include "AddTcfPanel.h"

#include "CilsContainer.h"
#include "CilsWorksetDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct AddTcfPanel::Impl {
        Ui::AddTcfPanel* ui{ nullptr };
        CilsContainer container;
    };
    AddTcfPanel::AddTcfPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::AddTcfPanel;
        d->ui->setupUi(this);

        Init();

        //for ETH
        d->ui->cilsBtn->hide();
    }
    AddTcfPanel::~AddTcfPanel() {

    }
    auto AddTcfPanel::Init() -> void {
        connect(d->ui->addBtn, SIGNAL(clicked()), this, SLOT(OnAddBtn()));
        connect(d->ui->cilsBtn, SIGNAL(clicked()), this, SLOT(OnCilsBtn()));

        // set object names
        d->ui->label->setObjectName("h1");
        d->ui->addBtn->setObjectName("bt-round-operation");
        d->ui->cilsBtn->setObjectName("bt-round-operation");

        d->ui->addBtn->setIconSize(QSize(24, 24));
        d->ui->addBtn->AddIcon(":/img/ic-newfile-n.svg");
        d->ui->addBtn->AddIcon(":/img/ic-newfile-s.svg", QIcon::Selected);
        d->ui->addBtn->AddIcon(":/img/ic-newfile-s.svg", QIcon::Active);
        d->ui->cilsBtn->setIconSize(QSize(24, 24));
        d->ui->cilsBtn->AddIcon(":/app/icons/cloud_white.svg");
        d->ui->cilsBtn->AddIcon(":/app/icons/cloud_white.svg", QIcon::Selected);
        d->ui->cilsBtn->AddIcon(":/app/icons/cloud_white.svg", QIcon::Active);
    }
    void AddTcfPanel::OnAddBtn() {
        QString tcfFolder = QFileDialog::getExistingDirectory(
            this,
            "Open tcf folder"
        );
        if (!tcfFolder.isEmpty()) {
            emit sigAdd(tcfFolder);
        }
    }

    void AddTcfPanel::OnCilsBtn() {
        if (CilsWorksetDialog dialog(&d->container); dialog.exec() == QDialog::Accepted) {
            const auto ids = dialog.GetSelectedTcfsWithWorkset();
            const auto images = dialog.GetSelectedTcfImage();
            const auto worksets = d->container.GetWorksets();

            QMap<int, QString> workset;
            for (const auto& w : worksets)
                workset[w.GetId()] = w.GetName();
            
            emit sigAddInCils(ids, workset, images);
        }
    }
}
