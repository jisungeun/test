#include <iostream>
#include <QKeyEvent>
#include <QStyle>
#include <QRegExpValidator>

#include "ui_HyperCubePanel.h"
#include "HyperCubePanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct HyperCubePanel::Impl {
        Ui::HyperCubePanel* ui{ nullptr };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
    };
    HyperCubePanel::HyperCubePanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::HyperCubePanel;
        d->ui->setupUi(this);

        Init();
    }
    HyperCubePanel::~HyperCubePanel() {

    }

    //slots
    void HyperCubePanel::OnMakeBtn() {
        QString result;
        if(d->ui->hypercubeName->text().isEmpty()) {
            result = d->ui->hypercubeName->placeholderText();
        }else {
            result = d->ui->hypercubeName->text();
            result = d->rstrip(result);
        }
        d->ui->hypercubeName->clear();
        emit sigHyper(result);
    }
    bool HyperCubePanel::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {
                OnMakeBtn();
            }            
        }
        return QObject::eventFilter(watched, event);
    }
    auto HyperCubePanel::Init() -> void {
        installEventFilter(this);
        // set object names
        d->ui->label->setObjectName("h1");
        d->ui->hypercubeName->setObjectName("input-high");
        d->ui->makeBtn->setObjectName("bt-round-operation");
        d->ui->hypercubeName->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->hypercubeName, &QLineEdit::textChanged, [=] { d->ui->hypercubeName->style()->polish(d->ui->hypercubeName); });

        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->hypercubeName->setValidator(new QRegExpValidator(re));        
        d->ui->hypercubeName->setPlaceholderText("Hypercube");
        connect(d->ui->makeBtn, SIGNAL(clicked()), this, SLOT(OnMakeBtn()));
    }
}