#include <iostream>
#include <windows.h>
#include <QStyle>
#include <QDateTime>
#include <QKeyEvent>
#include <QRegExpValidator>

#include "ui_AddPGPanel.h"
#include "AddPGPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct AddPGPanel::Impl {
        Ui::AddPGPanel* ui{ nullptr };
        QString pg_name;
        QString default_name;
        QString default_time;
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
    };
    AddPGPanel::AddPGPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::AddPGPanel;
        d->ui->setupUi(this);

        //store computer name first
        char  ComputerName[MAX_COMPUTERNAME_LENGTH + 1];
        DWORD cbComputerName = sizeof(ComputerName);
        if (GetComputerName(ComputerName, &cbComputerName)) {
            d->default_name = QString(ComputerName);
        }

        Init();
    }
    AddPGPanel::~AddPGPanel() {
        
    }
    bool AddPGPanel::eventFilter(QObject* watched, QEvent* event) {
         if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {
                OnOkBtn();
            }            
        }
        return QObject::eventFilter(watched, event);
    }
    auto AddPGPanel::Init() -> void {
        installEventFilter(this);
        // set object names
        d->ui->newGroup->setObjectName("widget-panel");
        d->ui->label->setObjectName("h1");        
        d->ui->okBtn->setObjectName("bt-round-operation");

        d->ui->curGroup->setObjectName("widget-panel");
        d->ui->label_2->setObjectName("h3");
        d->ui->cur_status->setObjectName("h1-hl");
        //d->ui->label_3->setObjectName("h3");
        d->ui->newPlayground->setObjectName("bt-round-operation");

        d->ui->newPlayground->setIconSize(QSize(24, 24));
        d->ui->newPlayground->AddIcon(":/img/ic-newplayground-n.svg");
        d->ui->newPlayground->AddIcon(":/img/ic-newplayground-s.svg", QIcon::Selected);
        d->ui->newPlayground->AddIcon(":/img/ic-newplayground-s.svg", QIcon::Active);

        connect(d->ui->okBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
        connect(d->ui->newPlayground, SIGNAL(clicked()), this, SLOT(OnNewBtn()));

        QDateTime dateTime = dateTime.currentDateTime();
        auto time_string = dateTime.toString("yyyyMMddHHmmss");
        time_string.remove(0, 2);
        d->default_time = time_string;

        d->ui->playgroundName->setObjectName("input-high");
        d->ui->playgroundName->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->playgroundName, &QLineEdit::textChanged, [=] { d->ui->playgroundName->style()->polish(d->ui->playgroundName); });

        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->playgroundName->setValidator(new QRegExpValidator(re));

        d->ui->playgroundName->setPlaceholderText(time_string+"-"+d->default_name);
        d->ui->newGroup->show();
        d->ui->curGroup->hide();
    }
    auto AddPGPanel::setDefaultName(const QString& name) -> void {        
        d->ui->playgroundName->setPlaceholderText(name);
    }
    auto AddPGPanel::setCurName(const QString& name) -> void {
        if (false == name.isEmpty()) {
            d->pg_name = name;
            d->ui->cur_status->setText(name);
            d->ui->curGroup->show();
            d->ui->newGroup->hide();
        }
    }
    auto AddPGPanel::getCurName() const -> QString {
        return d->pg_name;
    }

    auto AddPGPanel::Clear()->void {
        QDateTime dateTime = dateTime.currentDateTime();
        auto time_string = dateTime.toString("yyyyMMddHHmmss"); 
        time_string.remove(0, 2);
        d->default_time = time_string;

        d->ui->playgroundName->setPlaceholderText(time_string + "-" + d->default_name);
        d->ui->newGroup->show();
        d->ui->curGroup->hide();
    }

    void AddPGPanel::OnOkBtn() {
        QString result;
        if (d->ui->playgroundName->text().isEmpty()) {
            result = d->ui->playgroundName->placeholderText();
            QDateTime dateTime = dateTime.currentDateTime();
            auto time_string = dateTime.toString("yyyyMMddHHmmss");
            time_string.remove(0, 2);
            d->default_time = time_string;
            d->ui->playgroundName->setPlaceholderText(time_string + "-" + d->default_name);
        }else {
            result = d->ui->playgroundName->text();
            result = d->rstrip(result);
        }
        d->ui->playgroundName->clear();
        emit sigPG(result);
    }
    void AddPGPanel::OnNewBtn() {
        d->ui->curGroup->hide();
        d->ui->newGroup->show();
        d->ui->playgroundName->setText(QString());
    }

}