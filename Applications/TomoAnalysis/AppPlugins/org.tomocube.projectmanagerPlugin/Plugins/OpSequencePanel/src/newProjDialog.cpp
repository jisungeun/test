#include "newProjDialog.h"
#include "ui_newProjDialog.h";

#include <QFileDialog>
#include <QMessageBox>

namespace TomoAnalysis::ProjectManager::Plugins {
    struct newProjDialog::Impl {
        Ui::newProjDialog* ui{ nullptr };
    };

    newProjDialog::newProjDialog(QWidget* parent)
        : QDialog(parent), d{ new Impl}{
        d->ui = new Ui::newProjDialog;
        d->ui->setupUi(this);                

        setWindowTitle("New Project");
        //setWindowFlags(Qt::FramelessWindowHint);
        Init();        
    }

    newProjDialog::~newProjDialog() = default;

    auto newProjDialog::GetProjectName(QWidget* parent) -> QString {
        newProjDialog dialog(parent);

        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        auto path = dialog.GetPath();
        if(path.isEmpty()) {
            return QString();
        }

        auto full_path = path + "/" + dialog.GetName() + "/" + dialog.GetName() + ".tcpro";

        return full_path;
    }

    void newProjDialog::OnCancelBtn() {
        reject();
    }

    void newProjDialog::OnCreateBtn() {
        if (true == d->ui->projName->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a name", "Empty or invalid name");            
            return;
        }
        if (true == d->ui->projPath->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a path", "Empty or invalid path");            
            return;
        }
        accept();
    }

    void newProjDialog::OnFolderBtn() {
        QFileDialog dialog(this);
        dialog.setFileMode(QFileDialog::Directory);
        if (QDialog::Accepted == dialog.exec()) {             
            d->ui->projPath->setText(dialog.directory().absolutePath());
        }
    }
    auto newProjDialog::Init() -> void {
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
        connect(d->ui->createBtn, SIGNAL(clicked()), this, SLOT(OnCreateBtn()));
        connect(d->ui->pathBtn, SIGNAL(clicked()), this, SLOT(OnFolderBtn()));

        d->ui->projName->setPlaceholderText("Enter a project name");
        d->ui->projPath->setReadOnly(true);
    }

    auto newProjDialog::GetName() const -> QString {
        return d->ui->projName->text();
    }
    auto newProjDialog::GetPath() const -> QString {
        return d->ui->projPath->text();
    }

}