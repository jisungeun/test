#include <iostream>
#include <QKeyEvent>
#include <QListView>

#include "ui_CubePanel.h"
#include "CubePanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct CubePanel::Impl {
        Ui::CubePanel* ui{ nullptr };
        int hyper_idx{ 0 };
        int hyper_on_add{ -1 };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
    };
    CubePanel::CubePanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::CubePanel;
        d->ui->setupUi(this);

        Init();
    }
    CubePanel::~CubePanel() {


    }
    auto CubePanel::ClearHyperSel() -> void {
        d->hyper_on_add =-1;
    }

    auto CubePanel::ClearHyperList() -> void {
        d->ui->hyperList->blockSignals(true);
        d->hyper_idx = 0;
        d->ui->hyperList->clear();
        d->ui->hyperList->blockSignals(false);
    }

    auto CubePanel::AppendHyperCube(const QString& name) -> void {
        d->ui->hyperList->blockSignals(true);
        d->hyper_idx = d->ui->hyperList->count();
        d->ui->hyperList->addItem(name);
        if(d->hyper_on_add > -1){
            if(d->ui->hyperList->count()>d->hyper_on_add) {
                d->hyper_idx = d->hyper_on_add;
            }
        }
        d->ui->hyperList->setCurrentIndex(d->hyper_idx);
        d->ui->hyperList->blockSignals(false);
    }
    void CubePanel::OnMakeBtn() {
        QString result;
        if (d->ui->hyperList->count() < 1)
            return;

        if(d->ui->cubeName->text().isEmpty()) {
            result = d->ui->cubeName->placeholderText();
        }else {
            result = d->ui->cubeName->text();
            result = d->rstrip(result);
        }
        d->hyper_on_add = d->ui->hyperList->currentIndex();

        d->ui->cubeName->clear();
        emit sigCube(result, d->ui->hyperList->currentText());                        
    }
    void CubePanel::OnHyperSelected(int idx) {
        d->hyper_idx = idx;
    }
    bool CubePanel::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {
                OnMakeBtn();
            }            
        }
        return QObject::eventFilter(watched, event);
    }

    auto CubePanel::Init() -> void {
        installEventFilter(this);
        // set object names
        d->ui->label->setObjectName("h1");
        d->ui->addLabel->setObjectName("h2");
        d->ui->cubeName->setObjectName("input-high");
        d->ui->label_3->setObjectName("h2");
        d->ui->hyperList->setObjectName("dropdown-high");
        d->ui->makeBtn->setObjectName("bt-round-operation");
        d->ui->cubeName->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->cubeName, &QLineEdit::textChanged, [=] { d->ui->cubeName->style()->polish(d->ui->cubeName); });
        

        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->cubeName->setValidator(new QRegExpValidator(re));        

        d->ui->cubeName->setPlaceholderText("CubeName");

        d->ui->hyperList->setView(new QListView);
        d->ui->hyperList->addItem("Sample");//deprecated

        connect(d->ui->makeBtn, SIGNAL(clicked()), this, SLOT(OnMakeBtn()));
        connect(d->ui->hyperList, SIGNAL(currentIndexChanged(int)), this, SLOT(OnHyperSelected(int)));
    }
}