#include <QTableWidget>
#include <QPixmap>
#include <QScrollEvent>
#include <QScrollBar>
#include <QPainter>
#include <QCheckBox>
#include <QMessageBox>

#include "CilsWorksetDialog.h"

#include "CilsClient.h"
#include "CilsContainer.h"
#include "CilsUpdater.h"

#include "ui_CilsWorksetDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsWorksetDialog::Impl {
		Ui::CilsWorksetDialog ui{};

		CilsUpdater client;
		CilsContainer* container;

		int selectedWorksetId = -1;

		auto SetWorksets(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void {
			ui.tableWidget->setRowCount(0);
			ui.tableWidget->setRowCount(worksets.count());

			for (int i = 0; i < worksets.count(); i++) {
				ui.tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(worksets[i].GetId())));
				ui.tableWidget->setItem(i, 1, new QTableWidgetItem(worksets[i].GetName()));
				ui.tableWidget->setItem(i, 2, new QTableWidgetItem(QString::number(container->GetItems(worksets[i].GetId()).count())));

				if (worksets[i].GetId() == selectedWorksetId)
					ui.tableWidget->selectRow(i);
			}
		}

		auto SetItems(int id, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void {
			for (int i = 0; i < ui.tableWidget->rowCount(); i++) {
				if (const auto* item = ui.tableWidget->item(i, 0); item && item->text() == QString::number(id)) {
					if (auto* count = ui.tableWidget->item(i, 2))
						count->setText(QString::number(items.count()));

					break;
				}
			}

			if (id == selectedWorksetId) {
				ui.gallery->SetCurrentWorkset(id, container->GetItems(id));
				ui.loadingBar->setVisible(!finished);
				LoadPreview();
			}
		}

		auto LoadItems() -> void {
			if (container->IsEmptyWorkset(selectedWorksetId) && selectedWorksetId > -1) {
				client.LoadWorksetItems(selectedWorksetId);
			} else {
				if (const auto id = container->GetAnyEmptyWorkset(); id > -1)
					client.LoadWorksetItems(id);
			}
		}

		auto LoadPreview() -> void {
			const auto preview = ui.gallery->GetNonpreviewDataId();

			if (!preview.isEmpty())
				client.LoadPreview(preview);
		}

		auto UpdateState() -> void {
			unsigned int count = 0;
			const auto& items = ui.gallery->GetSelectedItems();

			for (const auto i : items.keys()) {
				count += items[i].count();
			}

			ui.countLabel->setText(QString::number(count));

			unsigned long long size = 0;

			QStringList alreadyAdded;
			for (const auto i : items.keys()) {
				for (const auto& j : items[i]) {
					if (!alreadyAdded.contains(j)) {
						alreadyAdded.push_back(j);
						size += container->GetItemSize(j);
					}
				}
			}

			size = size / (1024ull * 1024ull);

			if (size < 1024) {
				ui.sizeLabel->setText(QString("%1MB").arg(size));
			} else {
				double psize = size / 1024.0;

				if (psize < 1024.0) {
					ui.sizeLabel->setText(QString("%1GB").arg(psize, 0, 'g', 3));
				} else {
					psize /= 1024.0;
					ui.sizeLabel->setText(QString("%1TB").arg(psize, 0, 'g', 3));
				}
			}

			ui.initializeBtn->setEnabled(count > 0);
			ui.clearBtn->setEnabled(count > 0);
		}
	};

	CilsWorksetDialog::CilsWorksetDialog(CilsContainer* container, QWidget* parent) : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.loadingBar->setVisible(false);
		d->container = container;

		d->ui.searchLine->addAction(QIcon(":/app/icons/search.png"), QLineEdit::LeadingPosition);
		d->ui.initializeBtn->setObjectName("bt-square-primary");

		d->ui.tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		d->ui.tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
		d->ui.tableWidget->horizontalHeader()->hideSection(0);

		connect(&d->client, &CilsUpdater::WorksetLoaded, this, &CilsWorksetDialog::OnWorksetLoaded);
		connect(&d->client, &CilsUpdater::ItemLoaded, this, &CilsWorksetDialog::OnItemLoaded);
		connect(&d->client, &CilsUpdater::PreviewLoaded, this, &CilsWorksetDialog::OnPreviewLoaded);
		connect(&d->client, &CilsUpdater::Error, this, &CilsWorksetDialog::OnError);

		connect(d->ui.tableWidget, &QTableWidget::itemSelectionChanged, this, &CilsWorksetDialog::OnItemSelectionChanged);
		connect(d->ui.viewCheckBox, &QCheckBox::stateChanged, this, &CilsWorksetDialog::OnStateChanged);
		connect(d->ui.reloadBtn, &QPushButton::clicked, this, &CilsWorksetDialog::OnReloadBtnClicked);
		connect(d->ui.clearBtn, &QPushButton::clicked, this, &CilsWorksetDialog::OnClearBtnClicked);
		connect(d->ui.selAllBtn, &QPushButton::clicked, this, &CilsWorksetDialog::OnSelAllBtnClicked);
		connect(d->ui.initializeBtn, &QPushButton::clicked, this, &CilsWorksetDialog::OnInitializeBtnClicked);
		connect(d->ui.searchLine, &QLineEdit::textChanged, this, &CilsWorksetDialog::OnSearchChanged);
		connect(d->ui.gallery, &CilsPreview::Hovered, this, &CilsWorksetDialog::OnPreviewHovered);
		connect(d->ui.gallery, &CilsPreview::Selected, this, &CilsWorksetDialog::OnPreviewSelected);

		if (d->container->IsWorksetsEmpty())
			d->client.LoadWorkset();
		else {
			d->SetWorksets(d->container->GetWorksets());
		}
	}

	CilsWorksetDialog::~CilsWorksetDialog() = default;

	auto CilsWorksetDialog::GetSelectedTcfImage() const -> QMap<QString, QPixmap> {
		QMap<QString, QPixmap> items;

		const auto& selected = d->ui.gallery->GetSelectedItems();

		for (const auto& i : selected[d->selectedWorksetId]) {
			auto pixmap = d->ui.gallery->GetPixmap(i);

			if (pixmap.has_value())
				items[i] = pixmap.value();
		}

		return items;
	}

	auto CilsWorksetDialog::GetSelectedTcfsWithWorkset() const -> const QMap<int, QStringList>& {
		return d->ui.gallery->GetSelectedItems();
	}

	auto CilsWorksetDialog::OnReloadBtnClicked() -> void {
		d->ui.tableWidget->setRowCount(0);

		d->client.LoadWorkset();
	}

	auto CilsWorksetDialog::OnClearBtnClicked() -> void {
		d->ui.gallery->ClearSelectedItems();
		d->UpdateState();
	}

	auto CilsWorksetDialog::OnSelAllBtnClicked() -> void {
		d->ui.gallery->SelectAllItems();
		d->UpdateState();
	}

	auto CilsWorksetDialog::OnInitializeBtnClicked() -> void {
		d->ui.orderFrame->setEnabled(false);
		accept();
	}

	auto CilsWorksetDialog::OnItemSelectionChanged() -> void {
		const auto items = d->ui.tableWidget->selectedItems();

		if (items.count() == 0)
			return;

		const auto id = d->ui.tableWidget->item(items.first()->row(), 0)->text().toInt();
		d->selectedWorksetId = id;

		d->ui.loadingBar->setVisible(!d->container->IsItemsAllLoaded(id));
		d->ui.gallery->SetCurrentWorkset(id, d->container->GetItems(id));
		d->LoadPreview();
		d->LoadItems();
	}

	auto CilsWorksetDialog::OnStateChanged(int checked) -> void {
		d->ui.gallery->ShowWorksetItems(!checked);
		d->LoadPreview();
	}

	auto CilsWorksetDialog::OnSearchChanged(const QString& text) -> void {
		d->SetWorksets(d->container->Search(text));
	}

	auto CilsWorksetDialog::OnError() -> void {
		d->ui.messageLabel->setText(QString("Failed in updating at %1").arg(QTime::currentTime().toString()));
	}

	auto CilsWorksetDialog::OnWorksetLoaded(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void {
		d->container->SetWorksets(worksets);
		d->SetWorksets(worksets);
		d->LoadItems();

		OnSearchChanged(d->ui.searchLine->text());
		d->ui.messageLabel->setText(QString("Updated at %1").arg(QTime::currentTime().toString()));
	}

	auto CilsWorksetDialog::OnItemLoaded(int id, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void {
		d->container->SetItems(id, items, finished);
		d->SetItems(id, items, finished);

		OnSearchChanged(d->ui.searchLine->text());
		d->LoadItems();
	}

	auto CilsWorksetDialog::OnPreviewLoaded(const QString& dataId, const QByteArray& image) -> void {
		QPixmap pixmap;
		pixmap.loadFromData(image);

		d->ui.gallery->SetPixmap(dataId, pixmap);
		d->LoadPreview();
	}

	auto CilsWorksetDialog::OnPreviewSelected(const QString& dataId) -> void {
		d->UpdateState();
	}

	auto CilsWorksetDialog::OnPreviewHovered(const QString& dataId, bool hovered) -> void {
		if (hovered) {
			const auto& title = d->container->GetItemTitle(dataId);

			d->ui.gallery->setToolTip(title);
			d->ui.titleLabel->setText(title);
			d->ui.dataIdLabel->setText(dataId);
		} else {
			d->ui.gallery->setToolTip({});
			d->ui.titleLabel->setText({});
			d->ui.dataIdLabel->setText({});
		}
	}
}
