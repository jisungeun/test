#include <QStylePainter>
#include <QStyleOptionButton>

#include "ImageButton.h"

struct ImageButton::Impl {
    int pad = 4;
    int minSize = 8;
};

ImageButton::ImageButton(QWidget* parent) : QPushButton(parent), d{ new Impl } {
    //this->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding));
    //this->setStyleSheet("background-color: rgba(255, 255, 255, 0); border : none;");    
    
}
auto ImageButton::setNoBackground() -> void {
    this->setStyleSheet("background-color: rgba(255, 255, 255, 0); border : none;");
}

auto ImageButton::setFocusable() -> void {
    this->setStyleSheet("background-color: rgba(255,0,0,255);");    
}

auto ImageButton::setDefault() -> void {
    this->setStyleSheet("");
}



auto ImageButton::setPad(int pad) -> void {
    d->pad = pad;
}


/*
void ImageButton::resizeEvent(QResizeEvent* event) {
    setIconSize(size());
}*/

void ImageButton::paintEvent(QPaintEvent* event) {
    Q_UNUSED(event)
    QStylePainter painter(this);

    QStyleOptionButton opt;
    this->initStyleOption(&opt);

    QRect r = opt.rect;

    int h = r.height();
    int w = r.width();
    //int iconSize = qMax(qMin(h, w) - 2 * d->pad, d->minSize);

    //opt.iconSize = QSize(iconSize, iconSize);
    opt.iconSize = QSize(w, h);

    painter.drawControl(QStyle::CE_PushButton, opt);
}
