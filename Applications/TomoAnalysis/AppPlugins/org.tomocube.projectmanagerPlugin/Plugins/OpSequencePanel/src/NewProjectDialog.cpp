#include <QStyle>
#include <QFileDialog>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QSettings>

#include "ui_NewProjectDialog.h"
#include "NewProjectDialog.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct NewProjectDialog::Impl {
        Ui::NewProjectDialog* ui{ nullptr };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
        //registry keys
        struct {
            const char* recentCreateProjectFolder = "Recent/CreateProjectFolder";
        } entry;
    };

    NewProjectDialog::NewProjectDialog(QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::NewProjectDialog();
        d->ui->setupUi(this);

                
        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->nameLineEdit->setValidator(new QRegExpValidator(re));        
        setWindowTitle("New Project");
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        d->ui->nameLineEdit->setPlaceholderText("Enter a project name");
        d->ui->nameLineEdit->setObjectName("input-high");
        d->ui->nameLineEdit->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->nameLineEdit, &QLineEdit::textChanged, [=] { d->ui->nameLineEdit->style()->polish(d->ui->nameLineEdit); });

        const auto prevDir = QSettings("Tomocube", "TomoAnalysis").value(d->entry.recentCreateProjectFolder).toString();
        d->ui->locationLineEdit->setText(prevDir);
    }

    NewProjectDialog::~NewProjectDialog() = default;

    auto NewProjectDialog::GetCreateProjectPath(QWidget* parent)->QString {
        NewProjectDialog dialog(parent);

        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        const auto path = dialog.GenerateProjectFilePath();

        return path;
    }

    auto NewProjectDialog::GetName(void) const ->QString {
        auto result = d->ui->nameLineEdit->text();
        result = d->rstrip(result);
        return result;
    }

    auto NewProjectDialog::GetLocation(void) const ->QString {
        return d->ui->locationLineEdit->text();
    }

    auto NewProjectDialog::GenerateProjectFilePath() const ->QString {
        auto result = d->ui->nameLineEdit->text();
        result = d->rstrip(result);
        auto path = QString("%1/%2/%3.tcpro").arg(d->ui->locationLineEdit->text())
                                             .arg(result)
                                             .arg(result);

        return path;
    }

    void NewProjectDialog::on_createButton_clicked() {
        if (true == d->ui->nameLineEdit->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a project name", "Empty or invalid project name");
            return;
        }

        if (true == d->ui->locationLineEdit->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a project location", "Empty or invalid project location");
            return;
        }

        const auto path = this->GenerateProjectFilePath();
        if (true == QFile::exists(path)) {
            QMessageBox::warning(nullptr, "Warning", "The folder cannot be created since a file already exists with the same path.");
            return;
        }

        this->accept();
    }

    void NewProjectDialog::on_cancelButton_clicked() {
        this->reject();
    }

    void NewProjectDialog::on_pathButton_clicked() {
        const auto prevDir = QSettings("Tomocube", "TomoAnalysis").value(d->entry.recentCreateProjectFolder).toString();
        const auto directory = QFileDialog::getExistingDirectory(this, "Select a directory to save project", prevDir);
        if(true == directory.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue(d->entry.recentCreateProjectFolder, directory);

        d->ui->locationLineEdit->setText(directory);
    }
}