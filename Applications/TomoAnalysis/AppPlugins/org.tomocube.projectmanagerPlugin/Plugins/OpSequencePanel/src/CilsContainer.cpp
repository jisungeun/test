#include <QPixmap>

#include "CilsContainer.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsContainer::Impl {
		QMap<int, QStringList> items;
		QMap<QString, TC::Cils::JsonEntity::Item> itemDetail;
		QMap<int, bool> finished;

		QVector<TC::Cils::JsonEntity::Workset> worksets;
	};

	CilsContainer::CilsContainer() : d(new Impl) {}

	CilsContainer::~CilsContainer() = default;

	auto CilsContainer::Clear() -> void {
		d->items.clear();
		d->itemDetail.clear();
		d->worksets.clear();
	}

	auto CilsContainer::SetWorksets(const QVector<TC::Cils::JsonEntity::Workset>& worksets) -> void {
		d->worksets = worksets;
	}

	auto CilsContainer::SetItems(int worksetId, const QVector<TC::Cils::JsonEntity::Item>& items, bool finished) -> void {
		QStringList list;

		for (const auto& i : items) {
			d->itemDetail[i.GetDataId()] = i;
			list << i.GetDataId();
		}

		d->items[worksetId] = list;
		d->finished[worksetId] = finished;
	}

	auto CilsContainer::GetAnyEmptyWorkset() const -> int {
		for (const auto& w : d->worksets) {
			if (!d->finished.contains(w.GetId()) || !d->finished[w.GetId()])
				return w.GetId();

			if (!d->items.contains(w.GetId()))
				return w.GetId();
		}

		return -1;
	}

	auto CilsContainer::IsEmptyWorkset(int id) const -> bool {
		return !d->items.contains(id);
	}

	auto CilsContainer::IsWorksetsEmpty() const -> bool {
		return d->worksets.isEmpty();
	}

	auto CilsContainer::IsItemsAllLoaded(int id) -> bool {
		if (d->finished.contains(id))
			return d->finished[id];
		return false;
	}

	auto CilsContainer::GetWorksets() const -> const QVector<TC::Cils::JsonEntity::Workset>& {
		return d->worksets;
	}

	auto CilsContainer::GetItems(int worksetId) const -> QStringList {
		if (d->items.contains(worksetId))
			return d->items[worksetId];
		else
			return {};
	}

	auto CilsContainer::GetItemSize(const QString& dataId) const -> unsigned long long {
		if (d->itemDetail.contains(dataId))
			return d->itemDetail[dataId].GetSize();
		return
			-1;
	}

	auto CilsContainer::GetItemTitle(const QString& dataId) const -> QString {
		if (d->itemDetail.contains(dataId))
			return d->itemDetail[dataId].GetTitle().value();
		else
			return {};
	}

	auto CilsContainer::Search(const QString& text) const -> QVector<TC::Cils::JsonEntity::Workset> {
		QVector<TC::Cils::JsonEntity::Workset> result;
		QStringList found;

		if (text.isEmpty()) {
			return d->worksets;
		}

		for (const auto& w : d->worksets) {
			if (w.GetName().toLower().contains(text)) {
				result.push_back(w);
			}
		}

		for (const auto& k : d->itemDetail.keys()) {
			if (d->itemDetail[k].GetTitle()->toLower().contains(text) || k.toLower().contains(text))
				found << k;
		}

		for (const auto k : d->items.keys()) {
			for (const auto& v : d->items[k]) {
				if (found.contains(v)) {
					for (const auto& w : d->worksets) {
						if (w.GetId() == k) {
							if (!result.contains(w))
								result << w;
							break;
						}
					}
				}
			}
		}

		return result;
	}
}
