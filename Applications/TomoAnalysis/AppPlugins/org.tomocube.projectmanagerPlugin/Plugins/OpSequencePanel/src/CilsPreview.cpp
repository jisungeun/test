#include <QScrollBar>
#include <QWheelEvent>
#include <QPainter>

#include "CilsPreview.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	struct CilsPreview::Impl {
		bool ctrlPressed = false;
		bool showWorkset = true;

		int currentWorksetId = -1;
		QStringList allTcfs;

		QMap<QString, QPixmap> previews;
		QMap<int, QStringList> worksetTcfs;

		static auto CreatePixmap(const QColor& color) -> QPixmap {
			QPixmap pixmap(1, 1);
			pixmap.fill(Qt::transparent);
			QPainter painter(&pixmap);
			painter.setPen(Qt::NoPen);
			painter.setBrush(color);
			painter.drawRect(0, 0, 1, 1);
			painter.end();
			return pixmap;
		}
	};

	CilsPreview::CilsPreview(QWidget* parent) : GalleryView(parent), d(new Impl) {
		setToolTipDuration(-1);
		SetColumnCount(6);
		InstallEventFilterOnScrollViewer(this);
		AddDefaultLayer("hovered", d->CreatePixmap(QColor(255, 255, 255, 30)), false);
		AddDefaultLayer("selected", d->CreatePixmap(QColor(255, 255, 255, 80)), false);

		connect(this, &GalleryView::ItemClicked, this, &CilsPreview::OnItemClicked);
		connect(this, &GalleryView::ItemHovered, this, &CilsPreview::OnItemHovered);
	}

	CilsPreview::~CilsPreview() = default;

	auto CilsPreview::SetCurrentWorkset(int worksetId, const QStringList& items) -> void {
		d->currentWorksetId = worksetId;
		d->allTcfs = items;

		Refresh();
	}

	auto CilsPreview::SetPixmap(const QString& dataId, const QPixmap& pixmap) -> void {
		if (!d->previews.contains(dataId)) {
			d->previews[dataId] = pixmap;

			const auto& items = (d->showWorkset) ? d->allTcfs : d->worksetTcfs[d->currentWorksetId];
			if (auto* item = GetItemAt(items.indexOf(dataId)))
				item->SetPixmap(pixmap);
		}
	}

	auto CilsPreview::GetPixmap(const QString& dataId) -> std::optional<QPixmap> {
		if (d->previews.contains(dataId))
			return d->previews[dataId];
		return {};
	}

	auto CilsPreview::SelectAllItems() -> void {
		for (const auto& i : d->allTcfs)
			if (!d->worksetTcfs[d->currentWorksetId].contains(i))
				d->worksetTcfs[d->currentWorksetId] << i;

		Refresh();
	}

	auto CilsPreview::ClearSelectedItems() -> void {
		d->worksetTcfs.clear();

		Refresh();
	}

	auto CilsPreview::GetSelectedItems() const -> const QMap<int, QStringList>& {
		return d->worksetTcfs;
	}

	auto CilsPreview::GetNonpreviewDataId() const -> QString {
		auto [from, to] = GetVisibleIndex();
		QStringList items;

		if (d->showWorkset)
			items = d->allTcfs;
		else {
			for (const auto id : d->worksetTcfs.keys())
				for (const auto& dataId : d->worksetTcfs[id])
					items.push_back(dataId);
		}

		for (int i = from; i < to; i++) {
			if (!d->previews.contains(items[i]))
				return items[i];
		}

		for (int i = 0; i < items.count(); i++) {
			if (!d->previews.contains(items[i]))
				return items[i];
		}

		return {};
	}

	auto CilsPreview::ShowSelectedItems() -> void {
		d->showWorkset = false;

		Refresh();
	}

	auto CilsPreview::ShowWorksetItems(bool show) -> void {
		d->showWorkset = show;

		Refresh();
	}

	auto CilsPreview::Refresh() -> void {
		auto items = (d->showWorkset) ? d->allTcfs : d->worksetTcfs[d->currentWorksetId];

		Clear();
		AddItems(items.count());

		for (int i = 0; i < items.count(); i++) {
			if (auto* item = GetItemAt(i)) {
				if (d->previews.contains(items[i]))
					item->SetPixmap(d->previews[items[i]]);

				if (d->showWorkset && d->worksetTcfs[d->currentWorksetId].contains(items[i])) {
					if (auto* layer = item->GetLayer("selected")) {
						layer->SetVisibility(true);
					}
				}
			}
		}
	}

	bool CilsPreview::eventFilter(QObject* object, QEvent* event) {
		if (d->ctrlPressed && event->type() == QEvent::Wheel) {
			if (dynamic_cast<QScrollBar*>(object)) {
				const auto* wheel = dynamic_cast<QWheelEvent*>(event);

				if (d->ctrlPressed) {
					if (wheel->angleDelta().y() > 0 && GetColumnCount() > 1)
						SetColumnCount(GetColumnCount() - 1);
					else if (wheel->angleDelta().y() < 0)
						SetColumnCount(GetColumnCount() + 1);
				}

				return true;
			}
		}

		return QWidget::eventFilter(object, event);
	}

	void CilsPreview::keyReleaseEvent(QKeyEvent* event) {
		GalleryView::keyReleaseEvent(event);

		if (event->key() == Qt::Key_Control)
			d->ctrlPressed = false;
	}

	void CilsPreview::keyPressEvent(QKeyEvent* event) {
		GalleryView::keyPressEvent(event);

		if (event->key() == Qt::Key_Control)
			d->ctrlPressed = true;
	}

	auto CilsPreview::OnItemClicked(int index, QMouseEvent* event) -> void {
		const auto& dataId = (d->showWorkset) ? d->allTcfs[index] : d->worksetTcfs[d->currentWorksetId][index];

		if (const auto* item = GetItemAt(index); item) {
			if (auto* layer = item->GetLayer("selected"); layer) {
					if (d->worksetTcfs[d->currentWorksetId].contains(dataId)) {
						d->worksetTcfs[d->currentWorksetId].removeOne(dataId);
						layer->SetVisibility(false);
					} else {
						d->worksetTcfs[d->currentWorksetId].push_back(dataId);
						layer->SetVisibility(true);
					}
				}
			}

		if (!d->showWorkset)
			Refresh();

		emit Selected(dataId);
	}

	auto CilsPreview::OnItemHovered(int index, bool hovered) -> void {
		if (const auto* item = GetItemAt(index); item) {
			if (auto* layer = item->GetLayer("hovered"); layer)
				layer->SetVisibility(hovered);
		}

		const auto& dataId = (d->showWorkset) ? d->allTcfs[index] : d->worksetTcfs[d->currentWorksetId][index];
			emit Hovered(dataId, hovered);
	}
}
