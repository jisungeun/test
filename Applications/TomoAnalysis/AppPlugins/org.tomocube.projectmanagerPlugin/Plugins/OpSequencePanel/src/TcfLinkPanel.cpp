#include "ui_TcfLinkPanel.h"
#include "TcfLinkPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct TcfLinkPanel::Impl {
        Ui::TcfLinkPanel* ui{ nullptr };
        QMap<QString, int> counts;
    };
    TcfLinkPanel::TcfLinkPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::TcfLinkPanel;
        d->ui->setupUi(this);
        
        Init();                
    }
    TcfLinkPanel::~TcfLinkPanel() {

    }

    auto TcfLinkPanel::ClearTable() -> void {
        d->counts.clear();
        d->ui->cubeTable->setRowCount(0);
    }

    auto TcfLinkPanel::AppendCube(const QString& name) -> void {
        d->counts[name] = 0;
        auto row_cnt = d->ui->cubeTable->rowCount();
        d->ui->cubeTable->insertRow(row_cnt);

        auto cubeItem = new QTableWidgetItem(name);
        cubeItem->setTextAlignment(Qt::AlignCenter);

        auto countItem = new QTableWidgetItem(QString::number(0));
        countItem->setTextAlignment(Qt::AlignCenter);

        d->ui->cubeTable->setItem(row_cnt, 0, cubeItem);
        d->ui->cubeTable->setItem(row_cnt, 1, countItem);

        d->ui->cubeTable->resizeColumnsToContents();
    }

    auto TcfLinkPanel::SetCount(const QString& name,int count) -> void {
        if(d->counts.contains(name)) {
            d->counts[name] = count;
            auto row_idx = -1;
            for (auto i = 0; i < d->ui->cubeTable->rowCount();i++) {
                auto text = d->ui->cubeTable->item(i, 0)->text();
                if(text.compare(name)==0) {
                    row_idx = i;
                    break;
                }
            }
            if(row_idx > -1) {
                auto countItem = new QTableWidgetItem(QString::number(count));
                countItem->setTextAlignment(Qt::AlignCenter);

                d->ui->cubeTable->setItem(row_idx, 1, countItem);
            }
            d->ui->cubeTable->resizeColumnsToContents();
        }        
    }

    auto TcfLinkPanel::Init() -> void {
        // set object names
        d->ui->label->setObjectName("h1");

        d->ui->cubeTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->ui->cubeTable->setProperty("styleVariant",1);
    }
}