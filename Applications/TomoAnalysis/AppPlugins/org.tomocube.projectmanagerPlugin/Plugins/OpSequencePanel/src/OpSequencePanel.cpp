#include <iostream>
#include <QMap>
#include <QDir>
#include <QButtonGroup>
#include <utility>

//subpanels
#include "SelectProjectPanel.h"
#include "AddTcfPanel.h"
#include "AddPGPanel.h"
#include "HyperCubePanel.h"
#include "CubePanel.h"
#include "TcfLinkPanel.h"
#include "SelectAppPanel.h"

#include <AppManagerTA.h>
#include <AppInterfaceTA.h>

#include "ui_OpSequencePanel.h"
#include "OpSequencePanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct OpSequencePanel::Impl {
        Ui::OpSequencePanel* ui{ nullptr };
        QStringList iconList;
        QStringList iconOnList;
        QStringList iconDList;
        QVector<QIcon> icons;                

        QButtonGroup sequenceButtons;

        //sub-panels
        QVBoxLayout* layout;

        SelectProjectPanel* projectPanel;
        AddTcfPanel* addTcfPanel;
        AddPGPanel* addPgPanel;
        HyperCubePanel* hyperCubePanel;
        CubePanel* cubePanel;
        TcfLinkPanel* linkPanel;
        SelectAppPanel* appPanel;

        int currentStep = -1;
    };
    OpSequencePanel::OpSequencePanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::OpSequencePanel;
        d->ui->setupUi(this);

        Init();
    }    
    OpSequencePanel::~OpSequencePanel() {
        if (d->projectPanel)
            delete d->projectPanel;
        if (d->addTcfPanel)
            delete d->addTcfPanel;
        if (d->addPgPanel)
            delete d->addPgPanel;
        if (d->hyperCubePanel)
            delete d->hyperCubePanel;
        if (d->cubePanel)
            delete d->cubePanel;
        if (d->linkPanel)
            delete d->linkPanel;
        if (d->appPanel)
            delete d->appPanel;
    }
    auto OpSequencePanel::ClearHyperHolder() -> void {
        if(nullptr!=d->cubePanel) {
            d->cubePanel->ClearHyperSel();
        }
    }

    auto OpSequencePanel::Update(ProjectInfo::Pointer project) -> bool {
        try {
            if (nullptr == project) {
                throw std::exception();
            }

            // clear
            d->addPgPanel->Clear();            
            d->cubePanel->ClearHyperList();
            d->linkPanel->ClearTable();
            d->appPanel->ClearAppList();
            d->appPanel->ClearHyperList();

            //update for project sub-panel
            const auto path = project->GetPath();
            if (path.isEmpty()) {
                d->projectPanel->hideCurrentProjectPath();
                throw std::exception();
            }

            d->projectPanel->showCurrentProjectPath(path);

            //update for playground sub-panel
            const auto cur_pg = project->GetCurrentPlayground();
            if (nullptr == cur_pg) {
                d->addPgPanel->setCurName(QString());
                throw std::exception();
            }

            d->addPgPanel->setCurName(cur_pg->GetName());

            //update for cube sub-panel
            auto hyperList = cur_pg->GetHyperCubeList();
            for (const auto& hyper : hyperList) {
                d->cubePanel->AppendHyperCube(hyper->GetName());
            }

            //update for link sub-panel            
            for (const auto& hyper : hyperList) {
                auto cubeList = hyper->GetCubeList();
                for (const auto& cube : cubeList) {
                    d->linkPanel->AppendCube(cube->GetName());
                    d->linkPanel->SetCount(cube->GetName(), cube->GetTCFDirList().size());
                }
            }

            auto appManager = std::dynamic_pointer_cast<AppManagerTA>(AppManagerTA::GetInstance());
                        
            for (auto appName : appManager->GetAppList()) {
                auto metaPath = qApp->applicationDirPath() + "/PluginApp/Meta/" + appName.chopped(6) + ".meta";
                auto appMeta = IAppMetaRegistry::GetMeta(metaPath, true);
                if (!appMeta) {
                    continue;
                }
                auto appProp = appMeta->GetMetaInfo();
                auto parentApp = appProp["Parent-App"].toString();
                if (parentApp != "Project Manager") {
                    continue;
                }
                auto appTitle = appMeta->GetName();
                d->appPanel->AppendApp(appTitle);
                auto appPath = appManager->GetPluginPath(appName);
                d->appPanel->AppendAppFull(appPath);
                QStringList procs;
                if (appProp.contains("Processors")) {
                    auto processors = appProp["Processors"].toStringList();
                    for (auto p : processors) {
                        QDir procDir(qApp->applicationDirPath() + p);
                        auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
                        for (auto f : files) {
                            procs.push_back(qApp->applicationDirPath() + p + "/" + f);
                        }
                    }
                }
                d->appPanel->AppendProc(procs);
                auto isSingle = appProp["hasSingleRun"].toBool();
                auto isBatch = appProp["hasBatchRun"].toBool();
                d->appPanel->AppendRunType(std::make_tuple(isSingle, isBatch));
            }
            /*for (auto appName : appManager->GetAppList()) {
                auto appInterface = appManager->GetAppInterface(appName);
                if (appInterface) {
                    auto appInterfaceTA = static_cast<AppInterfaceTA*>(appManager->GetAppInterface(appName));
                    if(appInterfaceTA) {
                        auto appProp = appManager->GetAppProperty(appName);
                        auto parentApp = appProp["Parent-App"].toString();
                        if (parentApp == "Project Manager") {
                            auto appTitle = appInterfaceTA->GetDisplayTitle();
                            d->appPanel->AppendApp(appTitle);
                            auto appPath = appManager->GetPluginPath(appName);
                            d->appPanel->AppendAppFull(appPath);
                            QStringList procs;
                            if(appProp.contains("Processors")) {
                                auto processors = appProp["Processors"].toStringList();
                                for (auto p: processors) {                                    
                                    QDir procDir(qApp->applicationDirPath()+ p);
                                    auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
                                    for (auto f : files) {
                                        procs.push_back(qApp->applicationDirPath()+ p + "/" + f);
                                    }                      
                                }                                
                            }
                            d->appPanel->AppendProc(procs);
                            auto isSingle = appProp["hasSingleRun"].toBool();
                            auto isBatch = appProp["hasBatchRun"].toBool();
                            d->appPanel->AppendRunType(std::make_tuple(isSingle, isBatch));
                        }
                    }
                }                
            }*/
           
            for (const auto& hyper : hyperList) {
                d->appPanel->AppendHyper(hyper->GetName());
            }
        }
        catch (std::exception& ) {
            // do nothing, not error
        }

        UpdateButtons(project);

        return true;
    }

    auto OpSequencePanel::UpdateButtons(ProjectInfo::Pointer project) -> void {
        auto enableAddTCF = false;
        auto enablePlayground = false;
        auto enableHypercube = false;
        auto enableCube = false;
        auto enableLink = false;
        auto enableApp = false;

        try {
            if (nullptr == project) {
                throw std::exception();
            }

            enableAddTCF = true;

            if (false == project->GetTCFDirList().isEmpty()) {
                enablePlayground = true;
            }

            const auto cur_pg = project->GetCurrentPlayground();
            if (nullptr == cur_pg) {
                throw std::exception();
            }
            else {
                enableHypercube = true;
            }

            const auto hyperList = cur_pg->GetHyperCubeList();
            if (false == hyperList.isEmpty()) {
                enableCube = true;
            }

            for (const auto& hyper : hyperList) {
                if (nullptr == hyper) {
                    continue;
                }

                auto cubeList = hyper->GetCubeList();
                if (false == cubeList.isEmpty()) {
                    enableLink = true;
                }

                for (const auto& cube : cubeList) {
                    if (nullptr == cube) {
                        continue;
                    }

                    if (false == cube->GetTCFDirList().isEmpty()) {
                        enableApp = true;
                        break;
                    }
                }

                if (true == enableApp) {
                    break;
                }
            }
        }
        catch (std::exception& ) {
            // do nothing, not error
        }

        d->ui->selectProjTab->setEnabled(true);
        d->ui->addTcfTab->setEnabled(enableAddTCF);
        d->ui->playgroundTab->setEnabled(enablePlayground);
        d->ui->hypercubeTab->setEnabled(enableHypercube);
        d->ui->cubeTab->setEnabled(enableCube);
        d->ui->linkTcfTab->setEnabled(enableLink);
        d->ui->appTab->setEnabled(enableApp);

        // activate button
        if (true == enableAddTCF && d->currentStep == 1) {
            d->ui->addTcfTab->setChecked(true);
            return;
        }

        if (true == enableCube && d->currentStep == 4) {
            d->ui->cubeTab->setChecked(true);
            return;
        }

        if(true == enableApp) {
            d->ui->appTab->setChecked(true);
        }
        else if (true == enableLink && d->currentStep != 4) {
            d->ui->linkTcfTab->setChecked(true);
        }
        else if (true == enableCube) {
            d->ui->cubeTab->setChecked(true);
        }
        else if (true == enableHypercube) {
            d->ui->hypercubeTab->setChecked(true);
        }
        else if (true == enablePlayground && d->currentStep != 1) {
            d->ui->playgroundTab->setChecked(true);
        }
        else if (true == enableAddTCF) {
            d->ui->addTcfTab->setChecked(true);
        }
        else {
            d->ui->selectProjTab->setChecked(true);
        }
    }

    auto OpSequencePanel::ForceStep(int step) -> void {
        if(d->currentStep == step) {
            return;
        }
        switch(step) {
        case 0:
            d->ui->selectProjTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        case 1:
            d->ui->addTcfTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        case 2:
            d->ui->playgroundTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        case 3:
            d->ui->hypercubeTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        case 4:
            d->ui->cubeTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        case 5:
            d->ui->linkTcfTab->setChecked(true);
            emit OnLinkTab(true);
            break;
        case 6:            
            d->ui->appTab->setChecked(true);
            emit OnLinkTab(false);
            break;
        }        
    }


    //slots
    void OpSequencePanel::OnAddTcf(bool checked) {
        if (!checked) {
            return;
        }                
        ClearBtnColor();
        //d->ui->addTcfTab->setFocusable();
        removePanels();        
        d->layout->addWidget(d->addTcfPanel);
        d->addTcfPanel->show();
        d->currentStep = 1;

        emit OnLinkTab(false);
    }

    void OpSequencePanel::OnAddTcfInCils(const QMap<int, QStringList>& items, const QMap<int, QString>& worksets, const QMap<QString, QPixmap>& images) {
        emit cilsTcfAdd(items, worksets, images);
    }

    void OpSequencePanel::OnCreateCube(bool checked) {
        if (!checked) {
            return;
        }
        ClearBtnColor();
        //d->ui->cubeTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->cubePanel);
        d->cubePanel->show();
        d->currentStep = 4;

        emit OnLinkTab(false);
    }
    void OpSequencePanel::OnCreateHypercube(bool checked) {
        if (!checked) {
            return;
        }
        ClearBtnColor();
        //d->ui->hypercubeTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->hyperCubePanel);
        d->hyperCubePanel->show();
        d->currentStep = 3;

        emit OnLinkTab(false);
    }
    void OpSequencePanel::OnLinkTcf(bool checked) {
        if (!checked) {
            return;
        }
        ClearBtnColor();
        //d->ui->linkTcfTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->linkPanel);
        d->linkPanel->show();
        d->currentStep = 5;

        emit OnLinkTab(true);
    }    
    void OpSequencePanel::OnPlayground(bool checked) {
        if (!checked) {
            return;
        }                
        ClearBtnColor();
        //d->ui->playgroundTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->addPgPanel);
        d->addPgPanel->show();
        d->currentStep = 2;

        emit OnLinkTab(false);
    }    
    void OpSequencePanel::OnSelectProj(bool checked) {
        if (!checked) {
            return;
        }                
        ClearBtnColor();
        //d->ui->selectProjTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->projectPanel);
        d->projectPanel->show();
        d->currentStep = 0;

        emit OnLinkTab(false);
    }

    void OpSequencePanel::OnSelectApp(bool checked) {
        if (!checked) {
            return;
        }
        ClearBtnColor();
        //d->ui->appTab->setFocusable();
        removePanels();
        d->layout->addWidget(d->appPanel);
        d->appPanel->show();
        d->currentStep = 6;
        
        emit OnLinkTab(false);
    }
    void OpSequencePanel::OnNextBtn() {        
        switch(d->currentStep) {
        case 0:
            if(d->ui->addTcfTab->isEnabled()) {
                d->ui->addTcfTab->setChecked(true);
            }
            break;
        case 1:
            if(d->ui->playgroundTab->isEnabled()) {
                d->ui->playgroundTab->setChecked(true);
            }
            break;
        case 2:
            if(d->ui->hypercubeTab->isEnabled()) {
                d->ui->hypercubeTab->setChecked(true);
            }
            break;
        case 3:
            if(d->ui->cubeTab->isEnabled()) {
                d->ui->cubeTab->setChecked(true);
            }
            break;
        case 4:
            if(d->ui->linkTcfTab->isEnabled()) {
                d->ui->linkTcfTab->setChecked(true);
            }
            break;
        case 5:
            if(d->ui->appTab->isEnabled()) {
                d->ui->appTab->setChecked(true);
            }
            break;
        }
    }
    void OpSequencePanel::OnPrevBtn() {
        switch(d->currentStep) {
        case 1:
            d->ui->selectProjTab->setChecked(true);
            break;
        case 2:
            d->ui->addTcfTab->setChecked(true);
            break;
        case 3:
            d->ui->playgroundTab->setChecked(true);
            break;
        case 4:
            d->ui->hypercubeTab->setChecked(true);
            break;
        case 5:
            d->ui->cubeTab->setChecked(true);
            break;
        case 6:
            d->ui->linkTcfTab->setChecked(true);
            break;
        }
    }
    auto OpSequencePanel::ClearBtnColor() -> void {
        //d->ui->selectProjTab->setDefault();
        //d->ui->addTcfTab->setDefault();
        //d->ui->linkTcfTab->setDefault();
        //d->ui->playgroundTab->setDefault();
        //d->ui->cubeTab->setDefault();
        //d->ui->hypercubeTab->setDefault();
        //d->ui->appTab->setDefault();
    }


    void OpSequencePanel::OnProjectOpen(QString path) {        
        emit openProj(path);
    }

    void OpSequencePanel::OnProjectCreate(QString path) {        
        emit createProj(path);
    }

    void OpSequencePanel::OnTcfAdd(QString path) {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        emit tcfAdd(projPath,path);
    }

    void OpSequencePanel::OnMakePlayground(QString name) {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        auto parentDir = QFileInfo(projPath).absoluteDir();

        auto full_path = parentDir.path() + "/playground/" + name + ".tcpg";
        emit createPG(full_path);
    }

    auto OpSequencePanel::GetCurrentProject() -> QString {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        return projPath;
    }


    void OpSequencePanel::OnMakeHypercube(QString name) {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        auto curPlayground = d->addPgPanel->getCurName();
        auto parentDir = QFileInfo(projPath).absoluteDir();
        auto pgName = parentDir.path() + "/playground/" + curPlayground + ".tcpg";

        emit createHyper(pgName, name);
    }

    void OpSequencePanel::OnMakeCube(QString cube, QString hyper) {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        auto curPlayground = d->addPgPanel->getCurName();
        auto parentDir = QFileInfo(projPath).absoluteDir();
        auto pgName = parentDir.path() + "/playground/" + curPlayground + ".tcpg";
        emit createCube(pgName,cube, hyper);
    }    
    
    void OpSequencePanel::OnAppOpen(QString app,QString appfull,QStringList procList, QString hyper,std::tuple<bool,bool> appType) {
        auto projPath = d->projectPanel->getCurrentProjectPath();
        auto curPlayground = d->addPgPanel->getCurName();
        auto parentDir = QFileInfo(projPath).absoluteDir();
        auto pgName = parentDir.path() + "/playground/" + curPlayground + ".tcpg";
        emit connectApp(app,appfull,procList,hyper,pgName,appType);
    }
    //privates
    auto OpSequencePanel::removePanels() -> void {        
        if (d->layout->count() > 0) {
            //remove existing sub-panel
            QLayoutItem* wItem;
            while ((wItem = d->layout->takeAt(0)) != 0)
            {
                if (wItem->widget()) {
                    wItem->widget()->setParent(nullptr);
                    wItem->widget()->hide();
                }
            }
        }
    }

    auto OpSequencePanel::Init() -> void {
        d->layout = new QVBoxLayout;
        d->ui->instWidget->setLayout(d->layout);
        d->projectPanel = new SelectProjectPanel;        
        d->addTcfPanel = new AddTcfPanel;
        d->addPgPanel = new AddPGPanel;
        d->hyperCubePanel = new HyperCubePanel;
        d->cubePanel = new CubePanel;
        d->linkPanel = new TcfLinkPanel;
        d->appPanel = new SelectAppPanel;

        //InitButtons();
        //InitArrows();
        InitConnections();

        d->sequenceButtons.addButton(d->ui->selectProjTab);
        d->sequenceButtons.addButton(d->ui->addTcfTab);
        d->sequenceButtons.addButton(d->ui->playgroundTab);
        d->sequenceButtons.addButton(d->ui->hypercubeTab);
        d->sequenceButtons.addButton(d->ui->cubeTab);
        d->sequenceButtons.addButton(d->ui->linkTcfTab);
        d->sequenceButtons.addButton(d->ui->appTab);

        d->sequenceButtons.setExclusive(true);

        d->ui->selectProjTab->setChecked(true);

        // set object names
        d->ui->mainFrame->setObjectName("panel-base");

        d->ui->selectProjTab->setObjectName("bt-round-tab");
        d->ui->addTcfTab->setObjectName("bt-round-tab");
        d->ui->playgroundTab->setObjectName("bt-round-tab");
        d->ui->hypercubeTab->setObjectName("bt-round-tab");
        d->ui->cubeTab->setObjectName("bt-round-tab");
        d->ui->linkTcfTab->setObjectName("bt-round-tab");
        d->ui->appTab->setObjectName("bt-round-tab");

        d->ui->prevBtn->setObjectName("bt-arrow-left");
        d->ui->nextBtn->setObjectName("bt-arrow-right");

        d->ui->instWidget->setObjectName("widget-panel");
        d->ui->sequenceButtonFrame->setObjectName("frame-tab");
    }
    auto OpSequencePanel::InitArrows() -> void {
        d->iconList.push_back(":/image/images/PrevArrow.png");
        d->iconList.push_back(":/image/images/NextArrow.png");
        d->ui->prevBtn->setIcon(QIcon(d->iconList[0]));
        //d->ui->prevBtn->setNoBackground();
        //d->ui->prevBtn->setPad(20);

        d->ui->nextBtn->setIcon(QIcon(d->iconList[1]));
        //d->ui->nextBtn->setNoBackground();
        //d->ui->nextBtn->setPad(20);
    }
    auto OpSequencePanel::InitButtons() -> void {
        d->iconList.push_back(":/image/images/AddTcf.png");
        d->iconList.push_back(":/image/images/CreateCube.png");
        d->iconList.push_back(":/image/images/CreateHC.png");
        d->iconList.push_back(":/image/images/LinkToCube.png");
        d->iconList.push_back(":/image/images/Playground.png");
        d->iconList.push_back(":/image/images/SelectApp.png");
        d->iconList.push_back(":/image/images/SelectProj.png");
        d->iconList.push_back(":/image/images/PrevArrow.png");
        d->iconList.push_back(":/image/images/NextArrow.png");

        d->iconOnList.push_back(":/image/images/AddTcfOn.png");
        d->iconOnList.push_back(":/image/images/CreateCubeOn.png");
        d->iconOnList.push_back(":/image/images/CreateHCOn.png");
        d->iconOnList.push_back(":/image/images/LinkToCubeOn.png");
        d->iconOnList.push_back(":/image/images/PlaygroundOn.png");
        d->iconOnList.push_back(":/image/images/SelectAppOn.png");
        d->iconOnList.push_back(":/image/images/SelectProjOn.png");        

        d->iconDList.push_back(":/image/images/AddTcfD.png");
        d->iconDList.push_back(":/image/images/CreateCubeD.png");
        d->iconDList.push_back(":/image/images/CreateHCD.png");
        d->iconDList.push_back(":/image/images/LinkToCubeD.png");
        d->iconDList.push_back(":/image/images/PlaygroundD.png");
        d->iconDList.push_back(":/image/images/SelectAppD.png");
        d->iconDList.push_back(":/image/images/SelectProjD.png");

        for(auto i=0;i<7;i++) {
            d->icons.push_back(QIcon());
        }

        for(auto i=0;i<7;i++) {
            d->icons[i].addPixmap(QPixmap(d->iconList[i]), QIcon::Normal);
            d->icons[i].addPixmap(QPixmap(d->iconDList[i]), QIcon::Disabled);
        }
        
        d->ui->addTcfTab->setIcon(d->icons[0]);
        d->ui->cubeTab->setIcon(d->icons[1]);
        d->ui->hypercubeTab->setIcon(d->icons[2]);
        d->ui->linkTcfTab->setIcon(d->icons[3]);
        d->ui->playgroundTab->setIcon(d->icons[4]);
        d->ui->appTab->setIcon(d->icons[5]);
        d->ui->selectProjTab->setIcon(d->icons[6]);
        
        d->ui->prevBtn->setIcon(QIcon(d->iconList[7]));
        //d->ui->prevBtn->setPad(20);
        
        d->ui->nextBtn->setIcon(QIcon(d->iconList[8]));
        //d->ui->nextBtn->setPad(20);
    }
    auto OpSequencePanel::InitConnections() -> void {
        connect(d->ui->addTcfTab, SIGNAL(toggled(bool)), this, SLOT(OnAddTcf(bool)));
        connect(d->ui->appTab, SIGNAL(toggled(bool)), this, SLOT(OnSelectApp(bool)));
        connect(d->ui->cubeTab, SIGNAL(toggled(bool)), this, SLOT(OnCreateCube(bool)));
        connect(d->ui->hypercubeTab, SIGNAL(toggled(bool)), this, SLOT(OnCreateHypercube(bool)));
        connect(d->ui->linkTcfTab, SIGNAL(toggled(bool)), this, SLOT(OnLinkTcf(bool)));
        connect(d->ui->playgroundTab, SIGNAL(toggled(bool)), this, SLOT(OnPlayground(bool)));
        connect(d->ui->selectProjTab, SIGNAL(toggled(bool)), this, SLOT(OnSelectProj(bool)));

        connect(d->ui->nextBtn, SIGNAL(clicked()), this, SLOT(OnNextBtn()));
        connect(d->ui->prevBtn, SIGNAL(clicked()), this, SLOT(OnPrevBtn()));

        //sub panel connections
        connect(d->projectPanel, SIGNAL(sigOpen(QString)), this, SLOT(OnProjectOpen(QString)));
        connect(d->projectPanel, SIGNAL(sigCreate(QString)), this, SLOT(OnProjectCreate(QString)));
                
        connect(d->addTcfPanel, SIGNAL(sigAdd(QString)), this, SLOT(OnTcfAdd(QString)));
        connect(d->addTcfPanel, SIGNAL(sigAddInCils(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&)), this, SLOT(OnAddTcfInCils(const QMap<int, QStringList>&, const QMap<int, QString>&, const QMap<QString, QPixmap>&)));

        connect(d->addPgPanel, SIGNAL(sigPG(QString)), this, SLOT(OnMakePlayground(QString)));

        connect(d->hyperCubePanel, SIGNAL(sigHyper(QString)), this, SLOT(OnMakeHypercube(QString)));

        connect(d->cubePanel, SIGNAL(sigCube(QString, QString)), this, SLOT(OnMakeCube(QString, QString)));

        connect(d->appPanel, SIGNAL(sigApp(QString,QString,QStringList, QString,std::tuple<bool,bool>)),this, SLOT(OnAppOpen(QString,QString,QStringList,QString,std::tuple<bool,bool>)));

        d->ui->addTcfTab->setEnabled(false);
        d->ui->playgroundTab->setEnabled(false);
        d->ui->hypercubeTab->setEnabled(false);
        d->ui->cubeTab->setEnabled(false);
        d->ui->linkTcfTab->setEnabled(false);
        d->ui->appTab->setEnabled(false);
    }
}
