#include <QFileDialog>
#include <QSettings>

#include "NewProjectDialog.h"

#include "ui_SelectProjectPanel.h"
#include "SelectProjectPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct SelectProjectPanel::Impl {
        Ui::SelectProjectPanel* ui{ nullptr };
        QString cur_path;

        //registry keys
        struct {
            const char* recentOpenProjectFolder = "Recent/OpenProjectFolder";
        } entry;
    };

    SelectProjectPanel::SelectProjectPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::SelectProjectPanel;
        d->ui->setupUi(this);

        Init();
    }

    SelectProjectPanel::~SelectProjectPanel() {
    }

    auto SelectProjectPanel::Init() -> void {        
        connect(d->ui->newBtn, SIGNAL(clicked()), this, SLOT(OnNewButton()));
        connect(d->ui->openBtn, SIGNAL(clicked()), this, SLOT(OnOpenButton()));

        // set object names
        d->ui->projHead->setObjectName("h3");
        d->ui->pathLabel->setObjectName("h5");
        d->ui->label->setObjectName("h1");

        d->ui->newBtn->setObjectName("bt-round-operation");
        d->ui->openBtn->setObjectName("bt-round-operation");

        d->ui->newBtn->setIconSize(QSize(24, 24));
        d->ui->newBtn->AddIcon(":/img/ic-newproject-n.svg");
        d->ui->newBtn->AddIcon(":/img/ic-newproject-s.svg", QIcon::Selected);
        d->ui->newBtn->AddIcon(":/img/ic-newproject-s.svg", QIcon::Active);

        d->ui->openBtn->setIconSize(QSize(24, 24));
        d->ui->openBtn->AddIcon(":/img/ic-openproject-n.svg");
        d->ui->openBtn->AddIcon(":/img/ic-openproject-s.svg", QIcon::Selected);
        d->ui->openBtn->AddIcon(":/img/ic-openproject-s.svg", QIcon::Active);
    }

    auto SelectProjectPanel::showCurrentProjectPath(const QString& path) -> void {
        d->cur_path = path;
        d->ui->pathLabel->setText(path);

        d->ui->projHead->show();
        d->ui->pathLabel->show();
    }

    auto SelectProjectPanel::hideCurrentProjectPath() -> void {
        d->ui->projHead->hide();
        d->ui->pathLabel->hide();
    }

    auto SelectProjectPanel::getCurrentProjectPath() const -> QString {
        return d->cur_path;
    }

    //slots
    void SelectProjectPanel::OnNewButton() {
        auto new_path = NewProjectDialog::GetCreateProjectPath();
        if (!new_path.isEmpty()) {
            emit sigCreate(new_path);
        }
    }

    void SelectProjectPanel::OnOpenButton() {
        const auto prevDir = QSettings("Tomocube", "TomoAnalysis")
                            .value(d->entry.recentOpenProjectFolder).toString();

        QString file_path = QFileDialog::getOpenFileName(this, "Open project",
                                                         prevDir, 
                                                         tr("Tomocube project (*.tcpro)"));

        if (true == file_path.isEmpty()) {
            return;
        }        

        const QFileInfo fileInfo(file_path);
        QSettings("Tomocube", "TomoAnalysis").setValue(d->entry.recentOpenProjectFolder, 
                                                       fileInfo.absoluteDir().absolutePath());

        emit sigOpen(file_path);
    }
}