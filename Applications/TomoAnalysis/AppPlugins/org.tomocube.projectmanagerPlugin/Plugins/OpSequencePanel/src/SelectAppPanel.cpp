#include <QListView>

#include "ui_SelectAppPanel.h"
#include "SelectAppPanel.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct SelectAppPanel::Impl {
        Ui::SelectAppPanel* ui{ nullptr };
        QStringList AppFullName;
        QList<QStringList> AppProc;
        QList<std::tuple<bool,bool>> AppType;
    };
    SelectAppPanel::SelectAppPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::SelectAppPanel;
        d->ui->setupUi(this);

        Init();
    }
    SelectAppPanel::~SelectAppPanel() {

    }

    auto SelectAppPanel::ClearAppList() -> void {
        d->AppFullName.clear();
        d->AppProc.clear();
        d->AppType.clear();
        d->ui->appList->clear();
    }

    auto SelectAppPanel::ClearHyperList() -> void {
        d->ui->hyperList->clear();
    }

    auto SelectAppPanel::AppendAppFull(const QString& full_name) -> void {
        d->AppFullName.push_back(full_name);        
    }
    auto SelectAppPanel::AppendRunType(const std::tuple<bool, bool>& runType) -> void {
        d->AppType.push_back(runType);
    }

    auto SelectAppPanel::AppendApp(const QString& app) -> void {
        d->ui->appList->addItem(app);
        d->ui->appList->setCurrentIndex(0);
    }

    auto SelectAppPanel::AppendHyper(const QString& hyper) -> void {
        d->ui->hyperList->addItem(hyper);
    }

    void SelectAppPanel::OnOkBtn() {
        if (d->ui->appList->count() < 1)
            return;
        if (d->ui->hyperList->count() < 1)
            return;

        auto idx = d->ui->appList->currentIndex();
        emit sigApp(d->AppFullName[idx], d->ui->appList->currentText(),
            d->AppProc[idx], d->ui->hyperList->currentText(),d->AppType[idx]);
    }

    auto SelectAppPanel::AppendProc(const QStringList& proc_path) -> void {
        d->AppProc.push_back(proc_path);
    }

    auto SelectAppPanel::Init() -> void {
        // set object names
        d->ui->label->setObjectName("h1");
        d->ui->label_2->setObjectName("h3");
        d->ui->hyperList->setObjectName("dropdown-high");
        d->ui->label_3->setObjectName("h3");
        d->ui->appList->setObjectName("dropdown-high");
        d->ui->okBtn->setObjectName("bt-round-operation");

        d->ui->hyperList->setView(new QListView);
        d->ui->appList->setView(new QListView);

        connect(d->ui->okBtn, SIGNAL(clicked()), this, SLOT(OnOkBtn()));
    }
}