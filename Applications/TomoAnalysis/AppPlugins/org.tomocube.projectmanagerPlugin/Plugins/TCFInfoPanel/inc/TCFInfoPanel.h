#pragma once

#include <memory>
#include <QWidget>
#include <ITCFInfoPanel.h>

#include "TCFInfoPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class TCFInfoPanel_API TCFInfoPanel : public QWidget, public Interactor::ITCFInfoPanel {
        Q_OBJECT
        
    public:
        typedef TCFInfoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        TCFInfoPanel(QWidget* parent=nullptr);
        ~TCFInfoPanel();

        auto Update()->bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}