#include <UIUtility.h>

#include "ui_TCFInfoPanel.h"
#include "TCFInfoPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct TCFInfoPanel::Impl {
        Ui::TCFInfoPanel* ui{ nullptr };
    };

    TCFInfoPanel::TCFInfoPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::ITCFInfoPanel()
    , d{ new Impl } {
        d->ui = new Ui::TCFInfoPanel();
        d->ui->setupUi(this);
    }

    TCFInfoPanel::~TCFInfoPanel() {
        delete d->ui;
    }

    auto TCFInfoPanel::Update()->bool {
        return true;
    }
}
