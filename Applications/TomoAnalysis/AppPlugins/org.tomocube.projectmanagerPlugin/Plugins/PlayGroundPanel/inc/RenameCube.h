#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class RenameCube : public QDialog {
        Q_OBJECT
    public:
        explicit RenameCube(QWidget* parent, const QString& name, const QString& type);
        virtual ~RenameCube();

    public:
        static auto Rename(QWidget* parent, const QString& name, const QString& type)->QString;

        auto GetNewName(void)const->QString;

    protected slots:
        void OnOkBtn();
        void OnCancelBtn();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}