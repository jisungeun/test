#pragma once

#include <memory>
#include <QWidget>
#include <IPlaygroundPanel.h>

#include "PlaygroundPanelExport.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    class PlaygroundPanel_API PlaygroundPanel : public QWidget, public Interactor::IPlaygroundPanel {
        Q_OBJECT
    public:
        typedef PlaygroundPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        PlaygroundPanel(QWidget* parent = nullptr);
        ~PlaygroundPanel();;        

        auto Refresh(const Interactor::PlaygroundInfoDS& ds)->bool override;
        auto AppendItem(const Interactor::PlaygroundInfoDS& ds)->bool override;
        auto ModifyItem(const Interactor::PlaygroundInfoDS& ds)->bool override;
        auto Update(const PlaygroundInfo::Pointer& pg) -> bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->void;

        //for result List
        auto UpdateName(const PlaygroundInfo::Pointer& pg)->bool;
        auto SetAppSelected(const QString& appText)->void;

    signals:        
        void LoadAppTest();

        void cubeSelected(QString,QString);
        void hyperSelected(QString,QString);
        void appSelected(QString,QString);
        void tcfSelected(QString,QString);        
        void noItemSelected();
        void itemClicked(QString, QString);

        void cubeRename(QString, QString,QString);
        void hyperRename(QString, QString,QString);
        void cubeDelete(QString, QString);
        void hyperDelete(QString, QString);

        void hyperCopy(QString,QString);
        void cubeCopy(QString,QString);

        ////////deprecated////////
        void cubeDoubleClicked(QString, QString);
        void hyperDoubleClicked(QString, QString);
        void itemOverlapped(QString,QString,QString);
        void itemMoved(QString, QPoint,QString);
        /////////////////////////

    protected slots:        
        void OnItemChanged(QString);
        void OnItemDoubleClicked(QString);
        void OnItemOverlapped(QString, QString);
        void OnItemMoved(QString, QPoint);

        void OnRenameHyper(QString);
        void OnDeleteHyper(QString);
        void OnCopyHyper(QString);
        void OnRenameCube(QString);
        void OnDeleteCube(QString);
        void OnCopyCube(QString);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}