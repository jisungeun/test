#pragma once

#include <enum.h>

#include <memory>
#include <QWidget>
#include <QLabel>

namespace TomoAnalysis::ProjectManager::Plugins {
    BETTER_ENUM(PG_ITEM_TYPES, int,
        ITEM = 101,                 // deprecated
        ITEM_SINGLE = 102,          // deprecated
        ITEM_MULTIPLE = 103,        // deprecated
        CUBE = 201,
        CUBE_FILL = 202,
        CUBE_FILL_APP = 203,        // deprecated
        HYPERCUBE = 301,
        HYPERCUBE_FILL = 302,
        HYPERCUBE_FILL_APP = 303,
        APPLICATION = 401
    )
    class DraggableIcon : public QLabel {
        Q_OBJECT
    public:
        typedef DraggableIcon Self;
        typedef std::shared_ptr<Self> Pointer;

        DraggableIcon(PG_ITEM_TYPES type, QString info = QString(), QWidget* parent = nullptr);
        ~DraggableIcon();

        auto setType(PG_ITEM_TYPES type)->void;
        auto setSelected(bool selected)->void;        
        auto getType()->PG_ITEM_TYPES;
        auto setInfo(QString info)->void;
        auto getInfo()->QString;

    private:
        auto setPixmapList()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}