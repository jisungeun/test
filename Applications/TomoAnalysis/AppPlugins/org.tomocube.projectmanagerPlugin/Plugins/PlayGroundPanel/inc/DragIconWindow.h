#pragma once

#include <memory>
#include <QFrame>
#include <QWidget>

#include "DraggableIcon.h"

namespace TomoAnalysis::ProjectManager::Plugins {
	class DraggableIcon;
	class DragIconWindow : public QFrame {
		Q_OBJECT
	public:
		explicit DragIconWindow(QWidget* parent = nullptr);

		auto addItem(int type,QString info,QPoint pos=QPoint(100,100))->void;
		auto moveItem(QString info,QPoint pos)->void;
		auto updateItem(QString info,int type)->void;
		auto removeItem(QString info)->void;
		auto findItem(QString info)->DraggableIcon*;		

		//new rule
		auto addItem(int type, QString info,int layer = 0)->void;

		auto GetLayerNum()->int;
		auto GetLayerNum(const QString& hyper)->int;

	    auto Clear() const ->void;
		auto setItemSelected(QString info)->void;

	protected:
		bool event(QEvent* event) override;
		void dragEnterEvent(QDragEnterEvent* event) override;
		void dragMoveEvent(QDragMoveEvent* event) override;
		void dropEvent(QDropEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseReleaseEvent(QMouseEvent* event) override;
		void mouseDoubleClickEvent(QMouseEvent* event) override;
		void paintEvent(QPaintEvent* event) override;

	signals:
		void overlapIcons(QString, QString);
		void clickIcon(QString);
		void doubleClickIcon(QString);
		void moveIcon(QString, QPoint);

		void copyHyper(QString);
		void copyCube(QString);
		//modified signals
		void deleteHyper(QString);
		void renameHyper(QString);
		void deleteCube(QString);
		void renameCube(QString);		

	private:
		auto checkOverlap(QPoint pos)->DraggableIcon*;
		auto setNoItemSelected()->void;

		//layered placing
		auto addHyperCube(int type, QString info, int layer)->void;
		auto addCube(int type, QString info, int layer)->void;
		auto addApplication(int type,QString info, int layer)->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
