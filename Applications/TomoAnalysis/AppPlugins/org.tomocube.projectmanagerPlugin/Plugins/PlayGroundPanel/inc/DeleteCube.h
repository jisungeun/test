#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::ProjectManager::Plugins {
    class DeleteCube : public QDialog {
        Q_OBJECT
    public:
        explicit DeleteCube(QWidget* parent, const QString& name, const QString& type);
        virtual ~DeleteCube();

    public:
        static auto Delete(QWidget* parent, const QString& name,const QString& type)->bool;

    protected slots:
        void OnDeleteBtn();
        void OnCancelBtn();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}