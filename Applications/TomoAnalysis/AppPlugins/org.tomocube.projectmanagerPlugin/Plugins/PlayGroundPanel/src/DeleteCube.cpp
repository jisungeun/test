#include "ui_DeleteCube.h"
#include "DeleteCube.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct DeleteCube::Impl {
        Ui::DeleteCube* ui{ nullptr };
    };
    DeleteCube::DeleteCube(QWidget* parent,const QString& name,const QString& type)
        :QDialog(parent)
        , d{ new Impl } {
        d->ui = new Ui::DeleteCube;
        d->ui->setupUi(this);

        d->ui->label->setText("Are you sure to delete " + type + "?");
        d->ui->cubeName->setReadOnly(true);
        d->ui->cubeName->setText(name);
        //connect

        connect(d->ui->deleteBtn, SIGNAL(clicked()), this, SLOT(OnDeleteBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
    }
    DeleteCube::~DeleteCube() = default;

    auto DeleteCube::Delete(QWidget* parent, const QString& name, const QString& type) -> bool {
        DeleteCube dialog(parent, name, type);

        if(dialog.exec()!=QDialog::Accepted) {
            return false;
        }
        return true;
    }
    void DeleteCube::OnCancelBtn() {
        reject();
    }
    void DeleteCube::OnDeleteBtn() {
        accept();
    }

}