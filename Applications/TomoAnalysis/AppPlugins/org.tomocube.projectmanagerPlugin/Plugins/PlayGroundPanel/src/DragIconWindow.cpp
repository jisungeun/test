#include <iostream>

#include <QMouseEvent>
#include <QMimeData>
#include <QDrag>
#include <QPainter>
#include <qtooltip.h>
#include <QHelpEvent>
#include <QMenuBar>

#include "DragIconWindow.h"

namespace TomoAnalysis::ProjectManager::Plugins {

    struct DragIconWindow::Impl {        
        QString dragging_info;
        int dragging_type;

        //temporal variables
        int test_idx{ 0 };
        QMap<int, int> num_cube;
        QMap<int, QStringList> cube_infos;
        QMap<int, QString> app_infos;
        QMap<int, QString> hyper_infos;

        QColor lineCol {57,76, 83};
        QColor textCol{ 91,139,151};
        QColor textBG{ Qt::darkRed };
        QFont font;
    };
    DragIconWindow::DragIconWindow(QWidget* parent) : QFrame(parent), d{ new Impl } {
        setMinimumSize(300, 400);
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        setFrameStyle(QFrame::NoFrame);
        setAcceptDrops(true);
                
        d->font.setPixelSize(12);
    }

    auto DragIconWindow::GetLayerNum() -> int {
        return d->hyper_infos.size();
    }
    auto DragIconWindow::GetLayerNum(const QString& hyper) -> int {
        int key = 0;
        for(auto i=0;i<d->hyper_infos.size();i++) {
            if(d->hyper_infos[i].compare(hyper)==0) {
                key = i;
                break;
            }
        }
        return key;
    }

    auto DragIconWindow::findItem(QString info) -> DraggableIcon* {
        auto idx = -1;
        auto children = findChildren<DraggableIcon*>();
        for (int i = 0; i < children.size(); i++) {            
            if (children[i]->getInfo().compare(info) == 0) {
                idx = i;
                break;
            }
        }
        if (idx > -1) {
            return children[idx];
        }
        return nullptr;
    }
    auto DragIconWindow::addItem(int type,QString info,QPoint pos) -> void {
        auto newIcon = new DraggableIcon(PG_ITEM_TYPES::_from_integral(type), info,this);
        //newIcon->move(100, 100);
        newIcon->move(pos);
        newIcon->show();        
    }

    auto DragIconWindow::addItem(int type, QString info, int layer) -> void {
        if(!d->num_cube.contains(layer)) {
            d->num_cube[layer] = 0;
            d->cube_infos[layer] = QStringList();
            setMinimumSize(300, 400 + layer * 240);
        }
        if(type/100 == 3) {//hypercube
            addHyperCube(type, info, layer);
        }else if(type/100 == 2) {//cube
            addCube(type, info, layer);
        }else if(type/100 == 4) {//application
            addApplication(type,info, layer);
        }
    }

    auto DragIconWindow::addHyperCube(int type, QString info, int layer) -> void {
        QPoint pos;
        auto width = minimumWidth();
        pos.setX(width / 2 - 30);
        pos.setY(20 + 240 * layer);
        auto newIcon = new DraggableIcon(PG_ITEM_TYPES::_from_integral(type), info, this);
        newIcon->move(pos);
        newIcon->show();
        d->hyper_infos[layer] = info;
    }

    auto DragIconWindow::addCube(int type, QString info, int layer) -> void {
        QPoint pos;
        auto width = minimumWidth();
        pos.setY(120 + 240 * layer);
        d->num_cube[layer]++;
        for(auto i=0;i<d->cube_infos[layer].size();i++) {//move existing cube
            pos.setX(width / (d->num_cube[layer] + 1) * (i + 1) - 30);
            moveItem(d->cube_infos[layer][i], pos);
        }        
        d->cube_infos[layer].push_back(info);
        pos.setX(width / (d->num_cube[layer] + 1) * d->num_cube[layer] - 30);
        auto newIcon = new DraggableIcon(PG_ITEM_TYPES::_from_integral(type), info, this);
        newIcon->move(pos);
        newIcon->show();
    }

    auto DragIconWindow::addApplication(int type,QString info, int layer) -> void {
        QPoint pos;
        auto width = minimumWidth();
        pos.setY(20 + 240 * layer);
        pos.setX(width / 2 - 100 -30);
        auto newIcon = new DraggableIcon(PG_ITEM_TYPES::_from_integral(type), info, this);
        newIcon->move(pos);
        newIcon->show();
        d->app_infos[layer] = info;
    }    

    auto DragIconWindow::moveItem(QString info, QPoint pos) -> void {
        auto item = findItem(info);
        if(nullptr!=item) {
            item->move(pos);            
        }
    }

    auto DragIconWindow::removeItem(QString info) -> void {
        auto item = findItem(info);
        if (nullptr != item) {
            if(item->getType()._to_integral() / 100 == 2) {//cube
                int layer = (item->pos().y() - 120) / 240;
                d->num_cube[layer]--;
                for(auto i =0; i< d->cube_infos[layer].size();i++) {
                    if(d->cube_infos[layer][i].compare(info)==0) {
                        d->cube_infos[layer].removeAt(i);
                        break;
                    }
                }
                QPoint pos;
                auto width = minimumWidth();
                pos.setY(120 + 200 * layer);
                for (auto i = 0; i < d->cube_infos[layer].size(); i++) {//move remaining cubes
                    pos.setX(width / (d->num_cube[layer] + 1) * (i + 1)-30);
                    moveItem(d->cube_infos[layer][i], pos);
                }
            }else if(item->getType()._to_integral() / 100 == 4) {//application
                int layer = (item->pos().y() - 20) / 240;
                d->app_infos.remove(layer);
            }else if(item->getType()._to_integral() / 100 == 3) {//hypercube
                int layer = (item->pos().y() - 20) / 240;
                d->hyper_infos.remove(layer);
            }

            item->close();
        }        
    }

    auto DragIconWindow::updateItem(QString info, int type) -> void {
        auto item = findItem(info);
        if(nullptr!=item) {
            item->setType(PG_ITEM_TYPES::_from_integral(type));
            emit clickIcon(info);
        }
    }

    auto DragIconWindow::checkOverlap(QPoint pos) -> DraggableIcon* {        
        DraggableIcon* overlap = static_cast<DraggableIcon*>(childAt(pos));
        if(nullptr!=overlap) {
            if(overlap->getInfo().compare(d->dragging_info)!=0) {
                return overlap;
            }
        }
        return nullptr;
    } 
    auto DragIconWindow::setItemSelected(QString info) -> void {
        setNoItemSelected();
        auto item = findItem(info);
        item->setSelected(true);
    }
    auto DragIconWindow::setNoItemSelected() -> void {
        auto children = findChildren<DraggableIcon*>();
        for (int i = 0; i < children.size(); i++) {
            children[i]->setSelected(false);
        }
    }
    void DragIconWindow::mouseDoubleClickEvent(QMouseEvent* event) {
        DraggableIcon* child = static_cast<DraggableIcon*>(childAt(event->pos()));
        if(child == nullptr) {                       
            setNoItemSelected();
            emit clickIcon("NoItemSelected");
            d->dragging_type = 0;
            d->dragging_info = QString();
            return;
        }
        //double click을 통해 cube & hypercube 내부의 item을 뺄 수 있는 process 호출        
        emit doubleClickIcon(child->getInfo());
    }
    void DragIconWindow::mouseReleaseEvent(QMouseEvent* event) {
        DraggableIcon* child = static_cast<DraggableIcon*>(childAt(event->pos()));
        if (child == nullptr)
            return;
        if (event->button() == Qt::RightButton) {
            QMenu menu;
            QAction* selectedItem{ nullptr };

            auto type = child->getType()._to_integral() / 100;
            if (type == 3) {//hypercube
                menu.addAction("Rename Hypercube");
                menu.addAction("Delete Hypercube");
                menu.addAction("Copy Hypercube");
            }
            else if (type == 2) {//cube
                menu.addAction("Rename Cube");
                menu.addAction("Delete Cube");
                menu.addAction("Copy Cube");
            }
            else if (type == 4) {//application
               //Do nothing
            }
            selectedItem = menu.exec(mapToGlobal(child->pos()));

            if (nullptr != selectedItem) {
                const auto text = selectedItem->text();
                if (true == text.contains("Rename Hypercube")) {
                    emit renameHyper(child->getInfo());
                }
                else if (true == text.contains("Delete Hypercube")) {
                    emit deleteHyper(child->getInfo());
                }
                else if(true == text.contains("Copy Hypercube")) {                    
                    emit copyHyper(child->getInfo());
                }
                else if (true == text.contains("Rename Cube")) {
                    emit renameCube(child->getInfo());
                }
                else if (true == text.contains("Delete Cube")) {
                    emit deleteCube(child->getInfo());
                }                
                else if(true == text.contains("Copy Cube")) {
                    emit copyCube(child->getInfo());
                }
            }
        }
    }


    void DragIconWindow::mousePressEvent(QMouseEvent* event) {        
        DraggableIcon* child = static_cast<DraggableIcon*>(childAt(event->pos()));
        if (child == nullptr) {            
            setNoItemSelected();
            emit clickIcon("NoItemSelected");
            d->dragging_type = 0;
            d->dragging_info = QString();
            return;
        }
        
        //const QPixmap pixmap = *child->pixmap();

        auto prev_info = d->dragging_info;

        d->dragging_info = child->getInfo();
        d->dragging_type = child->getType();

        if (prev_info.compare(d->dragging_info) != 0) {
            //When current Clicked icon changed emit signal
            emit clickIcon(child->getInfo());
            setItemSelected(d->dragging_info);
        }                                
    }
    
    void DragIconWindow::dragEnterEvent(QDragEnterEvent* event) {        
        if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
            if (event->source() == this) {
                event->setDropAction(Qt::MoveAction);
                event->accept();
            }
            else {
                event->acceptProposedAction();
            }
        }
        else {
            event->ignore();
        }
    }
    void DragIconWindow::dragMoveEvent(QDragMoveEvent* event) {        
        if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
            if (event->source() == this) {
                event->setDropAction(Qt::MoveAction);
                event->accept();
            }
            else {
                event->acceptProposedAction();
            }
        }else {
            event->ignore();
        }
    }    
    bool DragIconWindow::event(QEvent* event) {
        if (event->type() == QEvent::ToolTip) {
            QHelpEvent* helpEvent = static_cast<QHelpEvent*>(event);
            DraggableIcon* child = static_cast<DraggableIcon*>(childAt(helpEvent->pos()));
            if (child) {
                QToolTip::showText(helpEvent->globalPos(), child->toolTip());
            }
            else {
                QToolTip::hideText();
                event->ignore();
            }
            return true;
        }
        return QWidget::event(event);
    }
    void DragIconWindow::paintEvent(QPaintEvent* event) {
        QPainter paint(this);        
        paint.setPen(d->lineCol);
        paint.setBrush(d->lineCol);

        auto line_width =4;

        //draw horizontal line for application
        auto app_keys = d->app_infos.keys();
        auto width = minimumWidth();
        for(const auto &key : app_keys) {                        
            auto x = width / 2 - 90;
            auto y = 20 + 240 * key + 35;            
            paint.drawRect(x, y, 75, line_width);
        }
        //draw lines for cube
        auto cube_keys = d->cube_infos.keys();
        for (const auto& key : cube_keys) {            
            auto cubes = d->cube_infos[key];
            auto cube_cnt = cubes.size();
            if (cube_cnt > 0) {
                //draw line from hypercube            
                auto hyper_x = width / 2 + 5;
                auto hyper_y = 20 + 240 * key + 60;
                paint.drawRect(hyper_x, hyper_y, line_width, 25);

                //draw center line
                auto center_x = width / (cube_cnt + 1) + 5;
                auto center_y = 20 + 240 * key + 60 + 25;
                auto center_width = width / (cube_cnt + 1) * (cube_cnt - 1);
                paint.drawRect(center_x, center_y, center_width, line_width);

                //draw lines to cubes
                for (auto i = 0; i < cubes.size(); i++) {
                    auto cube_x = width / (cube_cnt + 1) * (i + 1) + 5;
                    //if (cubes.size() == 1) cube_x -= 3;
                    paint.drawRect(cube_x, center_y, line_width, 25);
                }
            }
        }
        
        paint.end();

        //draw item names        
                
        QPainter app_text(this);

        app_text.setBackgroundMode(Qt::BGMode::TransparentMode);
        app_text.setBrush(Qt::white);
        app_text.setPen(Qt::white);
        app_text.setFont(d->font);
        //draw app text
        for(const auto& key : app_keys) {
            auto app_x = width / 2 - 100 - 40 + 4;
            auto app_y = 20 + 240 * key  + 74 + 10;
            auto name = d->app_infos[key].split("!")[1];
            app_text.drawText(app_x, app_y, name);
        }
        app_text.end();

        QPainter text(this);
        //text.setBackground(d->textBG);
        text.setBackgroundMode(Qt::BGMode::TransparentMode);
        text.setBrush(d->textCol);
        text.setPen(d->textCol);
        text.setFont(d->font);
        //draw hyper text
        auto hyper_keys = d->hyper_infos.keys();
        for (const auto& key : hyper_keys) {
            auto hyper_x = width / 2 + 12 + 8;
            auto hyper_y = 20 + 240 * key + 64 + 10;
            auto name = d->hyper_infos[key].split("!")[1];
            text.drawText(hyper_x, hyper_y, name);
        }        
        //draw cube text
        for(const auto& key : cube_keys) {
            auto cube_y = 20 + 240 * key + 60 + 120 + 6;
            for(auto i=0; i<d->cube_infos[key].size();i++) {
                auto cube_x = width / (d->num_cube[key] + 1) * (i + 1) + 6;// -12;
                auto name = d->cube_infos[key][i].split("!")[1];
                auto name_split = name.split(" ");
                int max_len = name_split[0].length();
                int cur_len =0;
                QString new_name = "";
                QString newLine('\n');
                //double x_ratio = 1.0;                

                QStringList newNames;                

                for (const auto& n : name_split) {                    
                    cur_len += n.length();
                    if (cur_len > 12) {
                        newNames.push_back(new_name);
                        new_name = "";
                        cur_len = n.length();
                    }
                    if (cur_len != n.length()) {
                        new_name += QString(" ");
                    }
                    new_name += n;                    
                    if (max_len < cur_len) {
                        max_len = cur_len;
                    }
                }
                newNames.push_back(new_name);
                for(auto ii=0;ii<newNames.size();ii++) {
                    auto nn = newNames[ii];
                    if (false == nn.isEmpty()) {
                        text.drawText(cube_x - max_len * 4, cube_y + 18 * ii, nn);
                    }
                }
                //text.drawText(cube_x, cube_y,name);                
            }
        }

        text.end();

        QFrame::paintEvent(event);
    }

    void DragIconWindow::dropEvent(QDropEvent* event) {        
        if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
            QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
            QDataStream dataStream(&itemData, QIODevice::ReadOnly);

            QPixmap pixmap;
            QPoint offset;
            dataStream >> pixmap >> offset;

            auto overlap = checkOverlap(event->pos());
            auto just_move = true;
            if (nullptr != overlap) {                
                if (d->dragging_type / 100 == 1 && 
                    (overlap->getType()._to_integral() == PG_ITEM_TYPES::CUBE || overlap->getType()._to_integral() == PG_ITEM_TYPES::CUBE_FILL)) {                    
                    emit overlapIcons(d->dragging_info, overlap->getInfo());
                    emit clickIcon(overlap->getInfo());
                    setItemSelected(overlap->getInfo());                    
                    just_move = false;
                }                
                else if ((d->dragging_type == PG_ITEM_TYPES::CUBE_FILL || d->dragging_type == PG_ITEM_TYPES::CUBE_FILL_APP) 
                    && (overlap->getType()._to_integral() == PG_ITEM_TYPES::HYPERCUBE || overlap->getType()._to_integral() == PG_ITEM_TYPES::HYPERCUBE_FILL)) {
                    emit overlapIcons(d->dragging_info, overlap->getInfo());
                    emit clickIcon(overlap->getInfo());
                    setItemSelected(overlap->getInfo());                    
                    just_move = false;
                }               
                else if(d->dragging_type == PG_ITEM_TYPES::APPLICATION && overlap->getType()._to_integral() == PG_ITEM_TYPES::CUBE_FILL) {
                    emit overlapIcons(d->dragging_info, overlap->getInfo());
                    emit clickIcon(overlap->getInfo());
                    setItemSelected(overlap->getInfo());
                    just_move = false;
                }
                else if(d->dragging_type == PG_ITEM_TYPES::APPLICATION && overlap->getType()._to_integral() == PG_ITEM_TYPES::HYPERCUBE_FILL) {
                    emit overlapIcons(d->dragging_info, overlap->getInfo());
                    emit clickIcon(overlap->getInfo());
                    setItemSelected(overlap->getInfo());                    
                    just_move = false;
                }
            }
            if (just_move) {
                setNoItemSelected();
                DraggableIcon* newIcon = new DraggableIcon(PG_ITEM_TYPES::_from_integral(d->dragging_type), d->dragging_info, this);
                //newIcon->setPixmap(pixmap);
                newIcon->move(event->pos() - offset);
                newIcon->show();
                newIcon->setAttribute(Qt::WA_DeleteOnClose);
                newIcon->setSelected(true);//drag & drop = selected icon
                
                emit moveIcon(d->dragging_info, newIcon->pos());
            }

            if (event->source() == this) {
                event->setDropAction(Qt::MoveAction);
                event->accept();
            }
            else {
                event->acceptProposedAction();
            }
        }
        else {
            event->ignore();
        }
    }

    auto DragIconWindow::Clear() const ->void {        
        d->test_idx = 0;
        d->num_cube.clear();
        d->cube_infos.clear();
        d->app_infos.clear();
        d->hyper_infos.clear();

        auto children = findChildren<DraggableIcon*>();

        for (auto child : children) {
            child->close();
            //delete child;
            //child->deleteLater();
        }
    }
}
