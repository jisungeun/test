#include <iostream>

#include "DraggableIcon.h"

namespace TomoAnalysis::ProjectManager::Plugins {
    struct DraggableIcon::Impl {
        QString Info = QString();
        int Type;
        double selected{ false };
        QStringList PixmapList;
        QStringList SelectionList;
    };
    DraggableIcon::DraggableIcon(PG_ITEM_TYPES type,QString info,QWidget* parent) : QLabel(parent),d{ new Impl } {
        setPixmapList();
        setAttribute(Qt::WA_TranslucentBackground);
        setAttribute(Qt::WA_DeleteOnClose);        
        setType(type);
        setInfo(info);        
    }
    DraggableIcon::~DraggableIcon() {
        
    }
    auto DraggableIcon::setPixmapList() -> void {
        d->PixmapList.push_back("");
        d->PixmapList.push_back("");
        d->PixmapList.push_back("");
        d->PixmapList.push_back(":/img/playground-cube-empty.svg");
        d->PixmapList.push_back(":/img/playground-cube.svg");
        d->PixmapList.push_back("");
        d->PixmapList.push_back(":/img/playground-hypercube-empty.svg");
        d->PixmapList.push_back(":/img/playground-hypercube.svg");
        d->PixmapList.push_back(":/img/playground-hypercube-link.svg");
        d->PixmapList.push_back(":/img/playground-application.svg");

        d->SelectionList.push_back("");
        d->SelectionList.push_back("");
        d->SelectionList.push_back("");
        d->SelectionList.push_back(":/img/playground-cube-empty-s.svg");
        d->SelectionList.push_back(":/img/playground-cube-s.svg");
        d->SelectionList.push_back("");
        d->SelectionList.push_back(":/img/playground-hypercube-empty-s.svg");
        d->SelectionList.push_back(":/img/playground-hypercube-s.svg");
        d->SelectionList.push_back(":/img/playground-hypercube-link-s.svg");
        d->SelectionList.push_back(":/img/playground-application-s.svg");
    }
    auto DraggableIcon::setInfo(QString info) -> void {
        d->Info = info;
        setToolTip(d->Info);        
    }
    auto DraggableIcon::getInfo() -> QString {
        return d->Info;
    }
    auto DraggableIcon::setType(PG_ITEM_TYPES type) -> void {
        d->Type = type._to_integral();
        if(d->selected) {
            auto p = QPixmap(d->SelectionList[static_cast<int>(type._to_index())]).scaled(76, 76, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            setPixmap(p);
        }
        else {            
            auto p = QPixmap(d->PixmapList[static_cast<int>(type._to_index())]).scaled(76, 76, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            setPixmap(p);
        }
    }
    auto DraggableIcon::setSelected(bool selected) -> void {
        auto type = PG_ITEM_TYPES::_from_integral(d->Type);
        d->selected = selected;
        if(selected) {         
            auto p = QPixmap(d->SelectionList[static_cast<int>(type._to_index())]).scaled(76, 76, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            setPixmap(p);            
        }else {
            auto p = QPixmap(d->PixmapList[static_cast<int>(type._to_index())]).scaled(76, 76, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            setPixmap(p);
        }
    }

    auto DraggableIcon::getType() ->PG_ITEM_TYPES {
        return PG_ITEM_TYPES::_from_integral(d->Type);
    }
}
