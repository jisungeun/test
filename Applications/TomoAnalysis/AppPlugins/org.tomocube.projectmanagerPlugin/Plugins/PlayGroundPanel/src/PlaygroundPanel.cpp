#include <iostream>

#include <QDir>
#include <QtConcurrent/QtConcurrent>

#include "DragIconWindow.h"
#include "RenameCube.h"
#include "DeleteCube.h"

#include "ui_PlaygroundPanel.h"
#include "PlaygroundPanel.h"

namespace  TomoAnalysis::ProjectManager::Plugins {
    struct PlaygroundPanel::Impl {
        Ui::PlaygroundPanel* ui{nullptr};
        DragIconWindow* window;
        QString path;
    };

    PlaygroundPanel::PlaygroundPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::PlaygroundPanel;
        d->ui->setupUi(this);
        Init();        
    }

    PlaygroundPanel::~PlaygroundPanel() {
        delete d->ui;
    }

    auto PlaygroundPanel::Init() const -> bool {        
        d->window = new DragIconWindow();

        auto containerLayout = new QVBoxLayout;
        containerLayout->setContentsMargins(0, 0, 0, 0);
        d->ui->container->setLayout(containerLayout);
        d->ui->container->layout()->addWidget(d->window);

        //connect qt signals        
        connect(d->window, SIGNAL(clickIcon(QString)), this, SLOT(OnItemChanged(QString)));
        connect(d->window, SIGNAL(renameHyper(QString)), this, SLOT(OnRenameHyper(QString)));
        connect(d->window, SIGNAL(deleteHyper(QString)), this, SLOT(OnDeleteHyper(QString)));
        connect(d->window,SIGNAL(copyHyper(QString)),this,SLOT(OnCopyHyper(QString)));
        connect(d->window, SIGNAL(renameCube(QString)), this, SLOT(OnRenameCube(QString)));
        connect(d->window, SIGNAL(deleteCube(QString)), this, SLOT(OnDeleteCube(QString)));
        connect(d->window,SIGNAL(copyCube(QString)),this,SLOT(OnCopyCube(QString)));

        d->path = QString();

        // set object names
        d->ui->frame->setObjectName("panel-base");
        d->ui->playgroundName->setObjectName("h6");

        d->ui->container->setObjectName("panel-contents");
        d->ui->scrollArea->setObjectName("scrollarea-bottom-round");

        return true;
    }

    auto PlaygroundPanel::Reset() const -> void {
        d->window->Clear();
        d->ui->playgroundName->setText("");
        d->path = "";
    }

    auto PlaygroundPanel::UpdateName(const PlaygroundInfo::Pointer& pg) -> bool {
        if(nullptr == d->window) {
            return false;
        }

        if(nullptr == pg) {
            return false;
        }        

        d->ui->playgroundName->setText(pg->GetName());
        return true;
    }

    auto PlaygroundPanel::Update(const PlaygroundInfo::Pointer& pg) -> bool {
        if (nullptr == d->window) {
            return false;
        }

        this->Reset();

        if(nullptr == pg) {
            return true;
        }

        d->ui->container->layout()->removeWidget(d->window);

        d->window = new DragIconWindow;

        d->ui->container->layout()->addWidget(d->window);

        connect(d->window, SIGNAL(clickIcon(QString)), this, SLOT(OnItemChanged(QString)));
        connect(d->window, SIGNAL(renameHyper(QString)), this, SLOT(OnRenameHyper(QString)));
        connect(d->window, SIGNAL(deleteHyper(QString)), this, SLOT(OnDeleteHyper(QString)));
        connect(d->window,SIGNAL(copyHyper(QString)),this,SLOT(OnCopyHyper(QString)));
        connect(d->window, SIGNAL(renameCube(QString)), this, SLOT(OnRenameCube(QString)));
        connect(d->window, SIGNAL(deleteCube(QString)), this, SLOT(OnDeleteCube(QString)));
        connect(d->window,SIGNAL(copyCube(QString)),this,SLOT(OnCopyCube(QString)));

        auto noApp = true;

        d->path = pg->GetPath();
        d->ui->playgroundName->setText(pg->GetName());
        auto hyperList = pg->GetHyperCubeList();
        for(const auto hyper : hyperList) {
            const auto &text = QString("HyperCube!%1").arg(hyper->GetName());
            auto cubeList = hyper->GetCubeList();
            auto app = hyper->GetMeasurementAppInfoList();
            auto layer = d->window->GetLayerNum();
            if(cubeList.isEmpty()) {
                d->window->addItem(PG_ITEM_TYPES::HYPERCUBE,text,layer);
            }else {
                for(const auto &cube : cubeList) {
                    const auto &cube_text  = QString("Cube!%1").arg(cube->GetName());
                    if(cube->GetTCFDirList().size()>0) {
                        d->window->addItem(PG_ITEM_TYPES::CUBE_FILL, cube_text, layer);
                    }
                    else {
                        d->window->addItem(PG_ITEM_TYPES::CUBE, cube_text, layer);
                    }
                    
                }
                if(true == app.isEmpty()) {
                    d->window->addItem(PG_ITEM_TYPES::HYPERCUBE_FILL, text, layer);
                }else {
                    d->window->addItem(PG_ITEM_TYPES::HYPERCUBE_FILL_APP, text, layer);
                    auto appName = app[0]->GetName();
                    auto appText = QString("Application!%1!%2").arg(appName).arg(hyper->GetName());
                    d->window->addItem(PG_ITEM_TYPES::APPLICATION, appText, layer);
                    noApp = false;
                    emit appSelected(appText, d->path);
                    d->window->setItemSelected(appText);
                }
            }
        }
        d->window->update();

        if(noApp) {
            emit itemClicked("NoItemSelected", d->path);
        }

        return true;
    }
    auto PlaygroundPanel::SetAppSelected(const QString& appText) -> void {
        emit appSelected(appText, d->path);
        d->window->setItemSelected(appText);
    }
    auto PlaygroundPanel::Refresh(const Interactor::PlaygroundInfoDS& ds)->bool {
        if(nullptr == d->window) {
            return false;
        }

        d->window->Clear();
        d->path = ds.path;
        return this->AppendItem(ds);
    }    
    auto PlaygroundPanel::ModifyItem(const Interactor::PlaygroundInfoDS& ds) -> bool {
        for (const auto& item : ds.hypercubeList) {
            const auto text = QString("HyperCube!%1").arg(item.name);
            if (true == item.cubeList.isEmpty()) {
                d->window->updateItem(text,PG_ITEM_TYPES::HYPERCUBE);
            }
            else {
                if(true == item.appList.isEmpty()) {
                    d->window->updateItem(text, PG_ITEM_TYPES::HYPERCUBE_FILL);
                }else {
                    d->window->updateItem(text, PG_ITEM_TYPES::HYPERCUBE_FILL_APP);
                }                
            }
        }

        for (const auto& item : ds.cubeList) {
            const auto text = QString("Cube!%1").arg(item.name);
            if (true == item.tcfDirList.isEmpty()) {
                d->window->updateItem(text, PG_ITEM_TYPES::CUBE);
            }
            else {
                if (true == item.appList.isEmpty()) {
                    d->window->updateItem(text, PG_ITEM_TYPES::CUBE_FILL);
                }else {
                    d->window->updateItem(text, PG_ITEM_TYPES::CUBE_FILL_APP);
                }
            }
        }
       
        return true;
    }

    auto PlaygroundPanel::AppendItem(const Interactor::PlaygroundInfoDS& ds)->bool {        
        for (const auto& item : ds.hypercubeList) {            
            const auto text = QString("HyperCube!%1").arg(item.name);
            if(true == item.cubeList.isEmpty()) {
                auto cur_layer = d->window->GetLayerNum();
                d->window->addItem(PG_ITEM_TYPES::HYPERCUBE, text, cur_layer);
            }
            else {
                if (true == item.appList.isEmpty()) {
                    auto cur_layer = d->window->GetLayerNum();
                    d->window->addItem(PG_ITEM_TYPES::HYPERCUBE_FILL, text, cur_layer);
                }else {
                    auto cur_layer = d->window->GetLayerNum();
                    d->window->addItem(PG_ITEM_TYPES::HYPERCUBE_FILL_APP, text, cur_layer);
                }
            }
        }

        for (const auto& item : ds.cubeList) {
            const auto text = QString("Cube!%1").arg(item.name);
            if(true == item.tcfDirList.isEmpty()) {                
                auto cur_layer = d->window->GetLayerNum();
                d->window->addItem(PG_ITEM_TYPES::CUBE, text,cur_layer);
            }
            else {
                if (true == item.appList.isEmpty()) {
                    auto cur_layer = d->window->GetLayerNum();
                    d->window->addItem(PG_ITEM_TYPES::CUBE_FILL, text, cur_layer);
                }else {
                    auto cur_layer = d->window->GetLayerNum();
                    d->window->addItem(PG_ITEM_TYPES::CUBE_FILL_APP, text, cur_layer);
                }
            }
        }

        for (const auto& tcfDir : ds.tcfDirList) {//deprecated
            if (1 == tcfDir.contents.size()) {
                const auto text = QString("Item!Single!%1").arg(tcfDir.path);
                if (nullptr != d->window->findItem(text)) {
                    continue;
                }
                d->window->addItem(PG_ITEM_TYPES::ITEM_SINGLE, text,tcfDir.pos);
            }
            else {
                const auto text = QString("Item!Folder!%1").arg(tcfDir.path);
                if (nullptr != d->window->findItem(text)) {
                    continue;
                }

                d->window->addItem(PG_ITEM_TYPES::ITEM_MULTIPLE, text,tcfDir.pos);
            }
        }

        for (const auto& app : ds.appList) {
            auto cur_layer = d->window->GetLayerNum();
            const auto text = QString("Application!%1").arg(app.path);
            d->window->addItem(PG_ITEM_TYPES::APPLICATION, text,cur_layer);
        }
        
        return true;
    }

    void PlaygroundPanel::OnItemChanged(QString info) {        
        if (info.compare("NoItemSelected") == 0) {            
            emit noItemSelected();
        }        
        else {
            if (info.contains("HyperCube!")) {                
                emit hyperSelected(info,d->path);
            }
            else if (info.contains("Cube!")) {                
                emit cubeSelected(info,d->path);
            }
            else if (info.contains("Application!")) {                
                emit appSelected(info,d->path);
            }
            else if (info.contains("Item!")) {
                emit tcfSelected(info, d->path);                
            }
        }

        emit itemClicked(info, d->path);
    }
    void PlaygroundPanel::OnItemDoubleClicked(QString info) {
        if(info.contains("HyperCube!")) {
            emit hyperDoubleClicked(info, d->path);
        }else if(info.contains("Cube!")) {
            emit cubeDoubleClicked(info, d->path);
        }
    }
    void PlaygroundPanel::OnItemOverlapped(QString fromInfo, QString toInfo) {
        emit itemOverlapped(fromInfo, toInfo,d->path);
    }
    void PlaygroundPanel::OnItemMoved(QString info, QPoint pos) {
        emit itemMoved(info, pos,d->path);
    }

    void PlaygroundPanel::OnDeleteCube(QString info) {
        auto cubeName = info.split("!")[1];
        auto del = DeleteCube::Delete(nullptr, cubeName, "cube");
        if (del) {
            emit cubeDelete(cubeName, d->path);
        }
    }
    void PlaygroundPanel::OnDeleteHyper(QString info) {
        auto hyperName = info.split("!")[1];
        auto del = DeleteCube::Delete(nullptr, hyperName, "hypercube");
        if (del) {
            emit hyperDelete(hyperName, d->path);
        }
    }
    void PlaygroundPanel::OnRenameCube(QString info) {
        auto cubeName = info.split("!")[1];
        auto newName = RenameCube::Rename(nullptr, cubeName, "cube");
        if (false == newName.isEmpty()) {
            emit cubeRename(cubeName, newName,d->path);
        }
    }
    void PlaygroundPanel::OnRenameHyper(QString info) {
        auto hyperName = info.split("!")[1];
        auto newName = RenameCube::Rename(nullptr, hyperName, "hypercube");
        if (false == newName.isEmpty()) {
            emit hyperRename(hyperName,newName, d->path);
        }
    }
    void PlaygroundPanel::OnCopyHyper(QString info) {
        auto hyperName = info.split("!")[1];        
        emit hyperCopy(hyperName,d->path);
    }
    void PlaygroundPanel::OnCopyCube(QString info) {
        auto cubeName = info.split("!")[1];
        emit cubeCopy(cubeName,d->path);
    }


}
