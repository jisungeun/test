#pragma once

#include <memory>
#include <ProjectInfo.h>

#include "ProjectManagerEntityExport.h"

namespace TomoAnalysis::ProjectManager::Entity {
    class ProjectManagerEntity_API ProjectStorage final {
	public:
		typedef ProjectStorage Self;
		typedef std::shared_ptr<Self> Pointer;

	public:
		static auto GetInstance()->Pointer;
		explicit ProjectStorage(const ProjectStorage& other) = delete;
		~ProjectStorage();
	    
		auto Publish(const QString& path) const ->ProjectInfo::Pointer;
		auto Append(const ProjectInfo::Pointer& project) const ->void;
        auto Remove(const QString& path) const ->void;

        auto FindProject(const QString& path) const->ProjectInfo::Pointer;
		auto FindParentProject(const QString& path) const->ProjectInfo::Pointer;

		auto Clear(void) const ->void;

	private:
		ProjectStorage();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}