#include <QMap>
#include <mutex>

#include <QDirIterator>
#include <QFileInfo>

#include "ProjectStorage.h"
#pragma warning( push )
#pragma warning(disable:4700)

namespace TomoAnalysis::ProjectManager::Entity {
	struct ProjectStorage::Impl {
		std::mutex lock;
		QMap<QString, ProjectInfo::Pointer> projects;  // id, instance
	};

	ProjectStorage::ProjectStorage()
		: d(new Impl()) {
	}

	ProjectStorage::~ProjectStorage() {
	}

	auto ProjectStorage::GetInstance() -> Pointer {
		static Pointer theInstance(new ProjectStorage());
		return theInstance;
	}

	auto ProjectStorage::Publish(const QString& path) const ->ProjectInfo::Pointer {
        auto project = std::make_shared<ProjectInfo>();
		project->SetPath(path);

		try {
			d->projects[path] = project;
		}
		catch (...) {
		}

		return project;
	}

	auto ProjectStorage::Append(const ProjectInfo::Pointer& project) const ->void {
		try {
			d->projects[project->GetPath()] = project;
		}
		catch (...) {
		}
	}

	auto ProjectStorage::Remove(const QString& path) const ->void {
		for (const auto& project : d->projects) {
			if (project->GetPath() == path) {
				d->projects.remove(path);
			}
		}
	}

	auto ProjectStorage::FindProject(const QString& path) const ->ProjectInfo::Pointer {
		for(auto project : d->projects) {
		    if(project->GetPath() == path) {
				return project;
		    }
		}
		
		return nullptr;
	}

	auto ProjectStorage::FindParentProject(const QString& path) const->ProjectInfo::Pointer {
		auto absoluteDir = QFileInfo(path).absoluteDir();
		absoluteDir.cdUp();

	    const auto parentPath = QString("%1/%2.tcpro").arg(absoluteDir.path()).arg(absoluteDir.dirName());
		return ProjectStorage::GetInstance()->FindProject(parentPath);
	}

	auto ProjectStorage::Clear(void) const ->void {
		d->projects.clear();
	}
}

#pragma warning( pop )