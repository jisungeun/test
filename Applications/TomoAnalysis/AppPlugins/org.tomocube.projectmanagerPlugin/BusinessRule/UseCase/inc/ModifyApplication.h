#pragma once

#include <memory>

#include "IUpdateApplicationPort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API ModifyApplication {
    public:
        ModifyApplication();
        virtual ~ModifyApplication();

        auto LoadDlls(const QString& rootFolder, IUpdateApplicationPort* port)const->bool;
        auto ActivatePlugin(const QString& dllPath, IUpdateApplicationPort* port)const->bool;
        auto ActivateProcessor(const QString& dllPath, IUpdateApplicationPort* port)const->bool;        
        auto LoadPluginParameter(const QString&playgroundPath, const QString& dllPath, IUpdateApplicationPort* port, const QString& parentName = QString())const->bool;

    private:
        auto activateAlgorithms(const QString& rootFolder)const ->bool;
        auto parsePluginName(const QString& fileName)const->QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}