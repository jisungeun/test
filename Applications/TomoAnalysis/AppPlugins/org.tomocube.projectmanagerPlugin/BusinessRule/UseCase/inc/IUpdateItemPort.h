#pragma once

#include <TCFDir.h>
#include <Cube.h>
#include <HyperCube.h>
#include <PluginAppInfo.h>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IUpdateItemPort {
    public:
        IUpdateItemPort();
        virtual ~IUpdateItemPort();

        virtual auto Update(const TCFDir::Pointer tcfItem)->void = 0;
        virtual auto Update(const Cube::Pointer& cubeItem)->void = 0;
        virtual auto Update(const HyperCube::Pointer& hypercubeItem)->void = 0;
        virtual auto Update(const PluginAppInfo::Pointer& appItem)->void = 0;
        virtual auto Clear()->void = 0;
    };
}