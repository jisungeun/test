#pragma once

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IUpdateTCFPort {
    public:
        IUpdateTCFPort();
        virtual ~IUpdateTCFPort();

        virtual auto Update() -> void = 0;
    };
}