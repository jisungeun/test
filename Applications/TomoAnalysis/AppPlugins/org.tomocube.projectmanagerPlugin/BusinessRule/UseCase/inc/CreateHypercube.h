#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API CreateHypercube {
	public:
		CreateHypercube();
		virtual ~CreateHypercube();

		auto Request(const QString& playgroundPath, const QString& name, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;

		auto IsDuplicate(const QString& playgroundPath, const QString& name)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}