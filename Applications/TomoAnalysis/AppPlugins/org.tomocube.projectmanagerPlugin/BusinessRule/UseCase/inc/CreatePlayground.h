#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API CreatePlayground {
	public:
		CreatePlayground();
		virtual ~CreatePlayground();

		auto Request(const QString& path, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
	};
}