#pragma once

#include <memory>

#include "IUpdateProjectPort.h"
#include "IProjectDataReadPort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API OpenProject {
	public:
		OpenProject();
		virtual ~OpenProject();

		auto Request(const QString& path, IUpdateProjectPort* port, IProjectDataReadPort* reader)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}