#pragma once

#include <memory>

#include "IUpdateProjectPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ModifyProject {
	public:
		ModifyProject();
		virtual ~ModifyProject();

		auto LinkFolder(const QString& projectPath, const QString& folderPath, IUpdateProjectPort* port, IProjectDataWritePort* writer) const ->bool;
		auto UnlinkFolder(const QString& projectPath, const QString& folderPath, IUpdateProjectPort* port, IProjectDataWritePort* writer) const ->bool;
		auto LinkWorkset(const QString& projectPath, int worksetId, const QString& worksetName, const QString& folderPath, IUpdateProjectPort* port, IProjectDataWritePort* writer) const -> bool;
		auto RenameProject(const QString& projectPath,const QString& newName,IUpdateProjectPort* port,IProjectDataWritePort* writer)const->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}