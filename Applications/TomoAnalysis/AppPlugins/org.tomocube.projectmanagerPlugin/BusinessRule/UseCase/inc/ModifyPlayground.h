#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"
#include "IProjectDataReadPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ModifyPlayground {
	public:
		ModifyPlayground();
		virtual ~ModifyPlayground();

		auto Rename(const QString& path, const QString& newPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto Delete(const QString& path,IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto Copy(const QString& path, const QString& targetPath,IUpdatePlaygroundPort* port,IProjectDataReadPort* reader,IProjectDataWritePort* writer)->bool;
		auto DeleteHistory(const QString& path, IUpdatePlaygroundPort* port)->bool;

		auto RenameCube(const QString& playgroundPath,const QString& cubeName, const QString& newName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto RenameHyperCube(const QString& playgroundPath,const QString& hyperName,const QString& newName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;

		auto DeleteCube(const QString& playgroundPath,const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto DeleteHyperCube(const QString& playgroundPath,const QString& hyperName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;

		auto CopyCube(const QString& playgroundPath,const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto CopyHyperCube(const QString& playgroundPath,const QString& hyperName,IUpdatePlaygroundPort* port,IProjectDataWritePort* writer)->bool;

		auto LinkTCFs(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName,IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const ->bool;

		auto UnlinkTCFs(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const ->bool;

		auto LinkFolder(const QString& playgroundPath, const QString& folderPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool;

		auto DuplicateTCFFolderItem(const QString& playgroundPath, const QString& folderPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool;

		auto LinkApplication(const QString& playgroundPath, const QString& appPath, const QStringList& procList,  IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)  const -> bool;

	    auto LinkApplication(const QString& playgroundPath, const QString& appPath,const QString& appName,const QStringList& procList, const QString& hyperName,std::tuple<bool,bool> appType, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)  const -> bool;

		auto DuplicateApplicationtItem(const QString& playgroundPath, const QString& appPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)  const -> bool;

		auto LinkFolderToCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName,IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool;

		auto UnLinkFolderFromCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool;

		auto LinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool;

		auto UnLinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool;

		auto LinkAppToCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;

		auto UnLinkAppFromCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;

		auto LinkAppToHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;

	    auto UnLinkAppFromHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;

		auto ShowLinkedApplication(const QString& playgroundPath, const QString& appName, const QString& hypercubeName, IUpdatePlaygroundPort* port)const->bool;
		auto ClearApplication(const QString& playgroundPath,IUpdatePlaygroundPort* port)const->bool;

		auto MoveCube(const QString& playgroundPath, const QString& cubeName, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;
		auto MoveHyperCube(const QString& playgroundPath, const QString& hypercubeName, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;
		auto MoveTCF(const QString& playgroundPath, const QString& folderPath, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;
		auto MoveApplication(const QString& playgroundPath, const QString& appPath, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)const->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}