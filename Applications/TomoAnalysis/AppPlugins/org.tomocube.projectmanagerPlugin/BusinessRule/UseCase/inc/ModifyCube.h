#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ModifyCube {
	public:
		ModifyCube();
		virtual ~ModifyCube();

		auto LinkFolder(const QString& projectPath, const QString& cubeName, const QString& folderPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}