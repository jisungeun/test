#pragma once

#include <memory>
#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API PreviewProject {
	public:
		PreviewProject();
		virtual ~PreviewProject();

		auto Request()->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}