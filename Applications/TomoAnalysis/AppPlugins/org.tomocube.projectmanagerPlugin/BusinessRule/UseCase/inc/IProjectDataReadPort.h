#pragma once

#include <ProjectInfo.h>
#include <PlaygroundInfo.h>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API IProjectDataReadPort {
	public:
		IProjectDataReadPort();
		virtual ~IProjectDataReadPort();

		virtual auto ReadProjectData(const QString& path, ProjectInfo::Pointer& projectInfo)->bool = 0;
		virtual auto ReadPlaygroundData(const QString& path, PlaygroundInfo::Pointer& playgroundInfo)->bool = 0;
	};
}