#pragma once

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IItemLoader {
    public:
        IItemLoader();
        virtual ~IItemLoader();

        virtual auto Update() -> void = 0;
    };
}