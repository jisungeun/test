#pragma once

#include <memory>

#include "IUpdateItemPort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ShowItemInfo {
	public:
		ShowItemInfo();
		virtual ~ShowItemInfo();

		auto UpdateTcfInfo(const QString& playgroundPath, const QString& tcfItemInfo, IUpdateItemPort* port)->bool;
		auto UpdateCubeInfo(const QString& playgroundPath, const QString& cubeItemInfo, IUpdateItemPort* port)->bool;
		auto UpdateHypercubeInfo(const QString& playgroundPath, const QString& hyperItemInfo, IUpdateItemPort* port)->bool;
		auto UpdateAppInfo(const QString& playgroundPath, const QString& appItemInfo, IUpdateItemPort* port)->bool;
		auto ClearItemInfo(IUpdateItemPort* port)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
