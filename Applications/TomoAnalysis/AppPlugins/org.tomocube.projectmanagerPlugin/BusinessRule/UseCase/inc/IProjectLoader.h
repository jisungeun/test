#pragma once

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IProjectLoader {
    public:
        IProjectLoader();
        virtual ~IProjectLoader();

        virtual auto Update() -> void = 0;
    };
}