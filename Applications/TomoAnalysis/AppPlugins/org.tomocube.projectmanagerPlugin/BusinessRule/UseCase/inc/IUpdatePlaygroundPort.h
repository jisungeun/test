#pragma once

#include <ProjectInfo.h>
#include <PlaygroundInfo.h>
#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IUpdatePlaygroundPort {
    public:
        IUpdatePlaygroundPort();
        virtual ~IUpdatePlaygroundPort();

        virtual auto Refresh(const PlaygroundInfo::Pointer& info) -> void = 0;
        virtual auto Update(const ProjectInfo::Pointer& info)->void = 0;
        virtual auto UpdateLink(const ProjectInfo::Pointer& info)->void = 0;
        virtual auto ChangeCurrent(const ProjectInfo::Pointer& info)->void = 0;
        virtual auto AddItem(const Cube::Pointer& cube,
            const HyperCube::Pointer& hypercube)->void = 0;
        virtual auto AddItem(const Cube::Pointer& cube, 
                             const HyperCube::Pointer& hyperCube, 
                             const TCFDir::List& tcfDirList,
                             const PluginAppInfo::List& appList)->void = 0;
        virtual auto ModifyItem(const Cube::Pointer& cube,
            const HyperCube::Pointer& hyperCube,
            const TCFDir::List& tcfDirList,
            const PluginAppInfo::List& appList)->void = 0;
        virtual auto LinkTCFDirToProject(const ProjectInfo::Pointer& project,
                                         const TCFDir::Pointer& tcfDir)->void = 0;        
    };
}