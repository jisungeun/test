#pragma once

#include <ProjectInfo.h>
#include <PlaygroundInfo.h>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API IProjectDataWritePort {
	public:
		IProjectDataWritePort();
		virtual ~IProjectDataWritePort();

		virtual auto WriteProjectData(const QString& path, const ProjectInfo::Pointer& projectInfo)->bool = 0;
		virtual auto WritePlaygroundData(const QString& path, const PlaygroundInfo::Pointer& playgroundInfo)->bool = 0;
	};
}