#pragma once

#include "IUpdateProjectPort.h"
#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API CreateProject {
	public:
		CreateProject();
		virtual ~CreateProject();

		auto Request(const QString& path, IUpdateProjectPort* port, IProjectDataWritePort* writer) const ->bool;
		auto RequestSubProject(const QString& path, IUpdateProjectPort* port, IProjectDataWritePort* writer)->bool;
	};
}