#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"

#include "IProjectDataWritePort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API CreateCube {
	public:
		CreateCube();
		virtual ~CreateCube();

		auto Request(const QString& playgroundPath, const QString& name, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;
		auto Request(const QString& playgroundPath, const QString& name, const QString& hyperName,IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool;

		auto IsDuplicate(const QString& playgroundPath, const QString& hypercube, const QString& cube)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
