#pragma once

#include <memory>

#include "IUpdatePlaygroundPort.h"

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API OpenPlayground {
	public:
		OpenPlayground();
		virtual ~OpenPlayground();

		auto Request(const QString& path, IUpdatePlaygroundPort* port)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}