#pragma once

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API ITCFLoader {
    public:
        ITCFLoader();
        virtual ~ITCFLoader();

        virtual auto Update() -> void = 0;
    };
}