#pragma once

#include <PluginAppInfo.h>
#include <ProcessorInfo.h>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IUpdateApplicationPort {
    public:
        IUpdateApplicationPort();
        virtual ~IUpdateApplicationPort();
                
        virtual auto Update(const PluginAppInfo::List& pluginInfo,const ProcessorInfo::List& procInfo)->void = 0;
        virtual auto Update(const ProcessorInfo::List&,const PluginAppInfo::List&)->void = 0;
    };
}