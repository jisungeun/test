#pragma once

#include <memory>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ShowTCFInfo {
	public:
		ShowTCFInfo();
		virtual ~ShowTCFInfo();

		auto Request()->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}