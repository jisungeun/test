#pragma once

#include <ProjectInfo.h>

#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    class ProjectManagerUseCase_API IUpdateProjectPort {
    public:
        IUpdateProjectPort();
        virtual ~IUpdateProjectPort();

        virtual auto Load(const ProjectInfo::Pointer& info) -> void = 0;
        virtual auto Update(const ProjectInfo::Pointer& info) -> void = 0;
        virtual auto AddTCF(const ProjectInfo::Pointer& info, const TCFDir::Pointer& dir) -> void = 0;
        virtual auto AddItem(const Cube::Pointer& cube,
                             const HyperCube::Pointer& hypercube,
                             const TCFDir::List& tcfDirList) const ->void = 0;
    };
}