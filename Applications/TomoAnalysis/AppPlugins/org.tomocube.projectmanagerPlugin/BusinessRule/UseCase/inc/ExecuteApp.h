#pragma once

#include <memory>
#include "ProjectManagerUseCaseExport.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	class ProjectManagerUseCase_API ExecuteApp {
	public:
		ExecuteApp();
		virtual ~ExecuteApp();

		auto Request()->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}