#pragma once
#include <QFileInfo>
#include <QDirIterator>

#include <ProjectStorage.h>

#include "ShowItemInfo.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ShowItemInfo::Impl {

    };
    ShowItemInfo::ShowItemInfo() : d{ new Impl } {

    }
    ShowItemInfo::~ShowItemInfo() {

    }        

    auto ShowItemInfo::UpdateAppInfo(const QString& playgroundPath, const QString& appItemInfo, IUpdateItemPort* port) -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto appName = appItemInfo.split("!")[1];
        auto app = playground->FindApp(appName);

        port->Update(app);

        return true;
    }

    auto ShowItemInfo::UpdateCubeInfo(const QString& playgroundPath, const QString& cubeItemInfo, IUpdateItemPort* port) -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto cubeName = cubeItemInfo.split("!")[1];
        auto cube = playground->FindCube(cubeName);

        port->Update(cube);

        return true;
    }

    auto ShowItemInfo::UpdateHypercubeInfo(const QString& playgroundPath, const QString& hyperItemInfo, IUpdateItemPort* port) -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto hyperName = hyperItemInfo.split("!")[1];
        auto hyper = playground->FindHypercube(hyperName);

        port->Update(hyper);

        return true;
    }

    auto ShowItemInfo::UpdateTcfInfo(const QString& playgroundPath, const QString& tcfItemInfo, IUpdateItemPort* port) -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto dirPath = tcfItemInfo.split("!")[2];
        auto tcfDir = playground->FindTCFDir(dirPath);

        port->Update(tcfDir);

        return true;
    }

    auto ShowItemInfo::ClearItemInfo(IUpdateItemPort* port) -> bool {
        port->Clear();
        return true;
    }

}