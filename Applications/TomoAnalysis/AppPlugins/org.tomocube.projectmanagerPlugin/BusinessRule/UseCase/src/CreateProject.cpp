#include <CreateProject.h>
#include <QDirIterator>
#include <QFileInfo>
#include <FileUtility.h>

#include "ProjectStorage.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    CreateProject::CreateProject() = default;
    CreateProject::~CreateProject() = default;

    auto CreateProject::Request(const QString& path, IUpdateProjectPort* port, IProjectDataWritePort* writer) const -> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        if(nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (false == TC::MakeDir(QFileInfo(path).absoluteDir().path())) {
            return false;
        }

        const auto projectInfo = std::make_shared<ProjectInfo>();
        projectInfo->SetPath(path);
        projectInfo->SetRoot(true);
        projectInfo->SetName(TC::GetBaseName(path));

        const QDateTime currentTime = QDateTime::currentDateTime();
        projectInfo->SetCreatedTime(currentTime);
        projectInfo->SetModifiedTime(currentTime);

        if(false == writer->WriteProjectData(path, projectInfo)) {
            return false;
        }

        Entity::ProjectStorage::GetInstance()->Append(projectInfo);
        
        port->Load(projectInfo);

        return true;
    }

    auto CreateProject::RequestSubProject(const QString& path, IUpdateProjectPort* port, IProjectDataWritePort* writer)->bool {
        if (true == path.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (false == TC::MakeDir(QFileInfo(path).absoluteDir().path())) {
            return false;
        }

        // get parent project
        auto parentDir = QFileInfo(path).absoluteDir();
        parentDir.cdUp();
        parentDir.cdUp();
          
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if(nullptr == parentProject) {
            return false;
        }

        const QDateTime currentTime = QDateTime::currentDateTime();

        const auto projectInfo = std::make_shared<ProjectInfo>();
        projectInfo->SetRoot(false);
        projectInfo->SetName(TC::GetBaseName(path));
        projectInfo->SetCreatedTime(currentTime);
        projectInfo->SetModifiedTime(currentTime);

        if (false == writer->WriteProjectData(path, projectInfo)) {
            return false;
        }

        parentProject->AddSubProject(projectInfo);
        Entity::ProjectStorage::GetInstance()->Append(projectInfo);

        // Don't update panel

        return true;
    }
}
