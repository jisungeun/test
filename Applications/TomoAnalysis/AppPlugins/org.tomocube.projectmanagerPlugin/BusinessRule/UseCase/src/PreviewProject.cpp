#pragma once

#include "PreviewProject.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct PreviewProject::Impl {

    };
    PreviewProject::PreviewProject() : d{ new Impl } {

    }
    PreviewProject::~PreviewProject() {

    }
    auto PreviewProject::Request() -> bool {
        return EXIT_SUCCESS;
    }
}