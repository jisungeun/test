#define LOGGER_TAG "[Project Manager]"
#include <TCLogger.h>

#include <iostream>

#include <QDirIterator>
#include <QFileInfo>
#include <QMessageBox>

#include <FileUtility.h>
#include <PluginRegistry.h>
#include <ProjectStorage.h>

#include "ModifyPlayground.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ModifyPlayground::Impl {

    };

    ModifyPlayground::ModifyPlayground() : d{ new Impl } {

    }

    ModifyPlayground::~ModifyPlayground() = default;

    auto ModifyPlayground::Copy(const QString& path, const QString& targetPath, IUpdatePlaygroundPort* port, IProjectDataReadPort* reader, IProjectDataWritePort* writer) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }
        if(true == targetPath.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        if(nullptr == writer) {
            return false;
        }
        if(nullptr == reader) {
            return false;
        }

        const auto project = Entity::ProjectStorage::GetInstance()->FindParentProject(targetPath);
        if (nullptr == project) {
            return false;
        }
        
        const auto playground = project->FindPlaygroundInfo(targetPath);        
        project->SetCurrentPlayground(targetPath);

        auto pg = std::make_shared<PlaygroundInfo>();
        reader->ReadPlaygroundData(path, pg);

        //copy contents
        playground->SetCubeList(pg->GetCubeList());
        playground->SetHyperCubeList(pg->GetHyperCubeList());
        playground->SetTCFDirList(pg->GetTCFDirList());
        playground->SetAppList(pg->GetAppList());

        //if(false == writer->WritePlaygroundData(path, playground)) {
        if (false == writer->WritePlaygroundData(targetPath, playground)) {
            return false;
        }

        port->Update(project);

        return true;
    }


    auto ModifyPlayground::Rename(const QString& path, const QString& newPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool {
        if (true == path.isEmpty()) {
            return false;
        }

        if (true == newPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        const auto project = Entity::ProjectStorage::GetInstance()->FindParentProject(path);
        if(nullptr == project) {
            return false;
        }

        const auto playground = project->FindPlaygroundInfo(path);
        if (nullptr == playground) {
            return false;
        }

        playground->SetPath(newPath);
        playground->SetName(TC::GetBaseName(newPath));
        
        if (false == writer->WritePlaygroundData(newPath, playground)) {
            return false;
        }

        auto old_path = path.chopped(5);
        auto new_path = newPath.chopped(5);
        QDir().rename(old_path, new_path);

        QFile file(path);        
        file.remove();
        

        project->SetModifiedTime(QDateTime::currentDateTime());

        port->Update(project);

        return true;
    }

    auto ModifyPlayground::RenameHyperCube(const QString& playgroundPath, const QString& hyperName, const QString& newName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (true == hyperName.isEmpty()) {
            return false;
        }
        if(true == newName.isEmpty()) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto hyperList = playground->GetHyperCubeList();

        for(const auto& hyper : hyperList) {
            if(hyper->GetName() == newName) {
                QMessageBox::warning(nullptr, "Error", QString("%1 already exists.").arg(newName));
                return false;
            }
        }


        for(const auto& hyper:hyperList) {
            if (hyper->GetName() == hyperName) {
                hyper->SetName(newName);
                break;
            }
        }

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        QString pgPath = playgroundPath.chopped(5);
        QDir pdir(pgPath);
        if(pdir.exists(hyperName)) {
            auto old_path = pgPath + "/" + hyperName;
            auto new_path = pgPath + "/" + newName;
            QDir().rename(old_path, new_path);
        }
        auto paramPath = pgPath + "/" + hyperName + ".param";
        if(QFile().exists(paramPath)) {
            auto newParamPath = pgPath + "/" + newName + ".param";
            QFile().rename(paramPath, newParamPath);
        }

        port->Update(parentProject);

        return true;
    }

    auto ModifyPlayground::RenameCube(const QString& playgroundPath, const QString& cubeName, const QString& newName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (true == newName.isEmpty()) {
            return false;
        }        
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto hyperList = playground->GetHyperCubeList();

        for (const auto& hyper : hyperList) {
            auto cubeList = hyper->GetCubeList();
            for (const auto& cube : cubeList) {
                if (cube->GetName() == newName) {
                    QMessageBox::warning(nullptr, "Error", QString("%1 already exists in playground.").arg(newName));
                    return false;
                }
            }            
        }


        auto cubeFound = false;        
        for (const auto& hyper : hyperList) {
            auto cubeList = hyper->GetCubeList();
            for(const auto& cube:cubeList) {                
                if(cube->GetName()==cubeName) {
                    cube->SetName(newName);
                    cubeFound = true;
                    break;
                }
            }
            if (cubeFound)
                break;
        }

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        port->Update(parentProject);

        return true;
    }

    auto ModifyPlayground::CopyCube(const QString& playgroundPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if(true == cubeName.isEmpty()) {
            return false;
        }
         auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //get hypercube        
        auto hyperList = playground->GetHyperCubeList();
        std::shared_ptr<HyperCube> sourceHyper{nullptr};
        std::shared_ptr<Cube> sourceCube{nullptr};
        auto cubeFound = false;
        for(const auto& hyper:hyperList) {
            for(const auto& cube : hyper->GetCubeList()) {
                if(cube->GetName().compare(cubeName)==0) {
                    sourceCube = cube;
                    cubeFound = true;
                    break;
                }
            }
            if(cubeFound) {
                sourceHyper = hyper;
                break;
            }
        }

        if(nullptr == sourceHyper || nullptr == sourceCube) {
            return false;
        }

        auto copiedHyper = std::make_shared<HyperCube>();
        copiedHyper->SetName(sourceHyper->GetName()+"_copy");
        auto copiedCube = std::make_shared<Cube>();
        copiedCube->SetName(sourceCube->GetName()+"_copy");
        copiedCube->SetTCFDirList(sourceCube->GetTCFDirList());
        copiedCube->SetID(copiedHyper->GetCubeList().count());
        copiedHyper->AddCube(copiedCube);

        playground->AddHyperCube(copiedHyper);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        port->Update(parentProject);

        return true;
    }

    auto ModifyPlayground::DeleteCube(const QString& playgroundPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if(true == cubeName.isEmpty()) {
            return false;
        }

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto hyperList = playground->GetHyperCubeList();

        for(const auto&hyper:hyperList) {
            auto cube = hyper->FindCube(cubeName);
            if (nullptr != cube) {
                auto prevID = cube->GetID();
                hyper->RemoveCube(cubeName);
                for (const auto& prevCube : hyper->GetCubeList()) {
                    auto id = prevCube->GetID();
                    if (id > prevID) {
                        prevCube->SetID(id - 1);
                    }
                }
                break;
            }            
        }        

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        port->Update(parentProject);        

        return true;
    }

    auto ModifyPlayground::CopyHyperCube(const QString& playgroundPath, const QString& hyperName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (true == hyperName.isEmpty()) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //get hypercube        
        auto hyperList = playground->GetHyperCubeList();
        std::shared_ptr<HyperCube> sourceHyper{nullptr};
        for(const auto& hyper:hyperList) {
            if (hyper->GetName() == hyperName) {
                sourceHyper = hyper;
                break;
            }
        }

        if(nullptr == sourceHyper) {
            return false;
        }

        //copy hyper cube information
        auto copiedHyper = std::make_shared<HyperCube>();
        copiedHyper->SetName(sourceHyper->GetName()+"_copy");
        for(const auto & cube : sourceHyper->GetCubeList()) {
            auto copiedCube = std::make_shared<Cube>();
            copiedCube->SetName(cube->GetName()+"_copy");
            copiedCube->SetTCFDirList(cube->GetTCFDirList());
            copiedCube->SetID(copiedHyper->GetCubeList().count());
            copiedHyper->AddCube(copiedCube);
        }
        //append copied hypercube to playground
        playground->AddHyperCube(copiedHyper);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        port->Update(parentProject);

        return true;
    }
    auto ModifyPlayground::DeleteHyperCube(const QString& playgroundPath, const QString& hyperName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        if (true == hyperName.isEmpty()) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        playground->RemoveHyperCube(hyperName);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        port->Update(parentProject);

        return true;
    }
    auto ModifyPlayground::DeleteHistory(const QString& path, IUpdatePlaygroundPort* port) -> bool {
        if(path.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }                

        QDir dir(path);
        auto timeStamp = dir.dirName();
        dir.cdUp();
        auto hyperName = dir.dirName();        
        dir.cdUp();//history
        auto pgFilePath = dir.path() + "/" + hyperName + "_" + timeStamp + ".tcpg";
        auto paramFilePath = dir.path() + "/" + hyperName + "_" + timeStamp + ".param";

        dir.cdUp();//playground
        dir.cdUp();//project
        auto projName = dir.dirName();

        auto projPath = QString("%1/%2.tcpro").arg(dir.path()).arg(projName);        

        const auto project = Entity::ProjectStorage::GetInstance()->FindProject(projPath);        
        if (nullptr == project) {
            return false;
        }
        

        //delete files
        QFile::remove(pgFilePath);
        QFile::remove(paramFilePath);

        //delete folder
        QDir deldir(path);
        deldir.removeRecursively();
                
        //port->Update(project);

        return true;
    }

    auto ModifyPlayground::Delete(const QString& path, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer)->bool {
        if (true == path.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        const auto project = Entity::ProjectStorage::GetInstance()->FindParentProject(path);
        if (nullptr == project) {
            return false;
        }

        if(false == project->RemovePlaygroundInfo(path)) {
            return false;
        }

        project->SetCurrentPlayground("");

        QFile file(path);
        file.remove();

        QDir filePath(path.chopped(5));
        filePath.removeRecursively();

        project->SetModifiedTime(QDateTime::currentDateTime());

        port->Update(project);

        return true;
    }

    auto ModifyPlayground::LinkFolder(const QString& playgroundPath, const QString& folderPath,
                                      IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }                

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if(nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if(nullptr == playground) {
            return false;
        }

        auto tcfDir = std::make_shared<TCFDir>();
        tcfDir->SetPath(folderPath);
        tcfDir->ScanDir();

        playground->AddTCFDir(tcfDir);        

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if(currentPlayground == playground) {
            auto tcfDirList = TCFDir::List();
            tcfDirList.append(tcfDir);
            port->AddItem(nullptr, nullptr, tcfDirList,PluginAppInfo::List());
        }

        return true;
    }

    auto ModifyPlayground::DuplicateTCFFolderItem(const QString& playgroundPath, const QString& folderPath,
                                                   IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool {
        Q_UNUSED(writer)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            auto tcfDir = std::make_shared<TCFDir>();
            tcfDir->SetPath(folderPath);
            tcfDir->ScanDir();

            auto tcfDirList = TCFDir::List();
            tcfDirList.append(tcfDir);
            port->AddItem(nullptr, nullptr, tcfDirList,PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::ClearApplication(const QString&playgroundPath,IUpdatePlaygroundPort* port) const -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->Update(std::make_shared<ProjectInfo>());
        }
        return true;
    }

    auto ModifyPlayground::ShowLinkedApplication(const QString& playgroundPath, const QString& appName, const QString& hypercubeName, IUpdatePlaygroundPort* port) const -> bool {
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto appInfo = playground->FindApp(appName);

        parentProject->SetCurrentAppInfo(appInfo);

        auto hyper = playground->FindHypercube(hypercubeName);

        parentProject->SetCurrentHyperCube(hyper);

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            //auto appList = PluginAppInfo::List();
            //appList.append(appInfo);
            //port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
            port->Update(parentProject);
        }
        return true;        
    }


    auto ModifyPlayground::LinkApplication(const QString& playgroundPath, const QString& appPath,const QString& appName,const QStringList& procList, const QString& hyperName,std::tuple<bool,bool> appType, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }
        auto hyper = playground->FindHypercube(hyperName);
        if(0==hyper->GetCubeList().size()) {
            return false;
        }

        auto appInfo = std::make_shared<PluginAppInfo>();
        appInfo->SetPath(appPath);        
        appInfo->SetName(appName);
        appInfo->SetAppType(appType);

        auto processors = ProcessorInfo::List();
                
        for (const auto& procPath : procList) {
            auto procInfo = std::make_shared<ProcessorInfo>();
            procInfo->SetPath(procPath);            
            auto plugin = PluginRegistry::GetPlugin(procPath,true);            
            procInfo->SetName(plugin->GetName());
            procInfo->SetParent(appInfo->GetName());
            processors.append(procInfo);
        }
        appInfo->SetProcessors(processors);

        parentProject->SetCurrentAppInfo(appInfo);                
                
        hyper->AddApp(appInfo);

        parentProject->SetCurrentHyperCube(hyper);

        if (nullptr != writer) {
            if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
                return false;
            }
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            auto appList = PluginAppInfo::List();
            appList.append(appInfo);
            //port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
            port->Update(parentProject);
        }
        return true;        
    }

    auto ModifyPlayground::LinkApplication(const QString& playgroundPath, const QString& appPath,const QStringList& procList, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }
        
        if (nullptr == writer) {
            return false;
        }        

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto appInfo = std::make_shared<PluginAppInfo>();
        appInfo->SetPath(appPath);
        appInfo->ScanName();

        auto processors = ProcessorInfo::List();

        for(const auto& procPath :procList) {
            auto procInfo = std::make_shared<ProcessorInfo>();
            procInfo->SetPath(procPath);
            procInfo->ScanName();
            procInfo->SetParent(appInfo->GetName());
            processors.append(procInfo);            
        }

        appInfo->SetProcessors(processors);


        playground->AddApp(appInfo);
        if (nullptr != writer) {
            if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
                return false;
            }
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            auto appList = PluginAppInfo::List();
            appList.append(appInfo);
            port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
        }
        return true;
    }

    auto ModifyPlayground::DuplicateApplicationtItem(const QString& playgroundPath, const QString& appPath, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        Q_UNUSED(writer)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == appPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            auto appInfo = std::make_shared<PluginAppInfo>();
            appInfo->SetPath(appPath);
            appInfo->ScanName();

            auto appList = PluginAppInfo::List();
            appList.append(appInfo);
            port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
        }

        return true;
                
    }
    auto ModifyPlayground::LinkTCFs(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName,IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == tcfList.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //link folder to existing cube
        auto cube = playground->FindCube(cubeName);
        
        for (const auto& tcf : tcfList) {
            if(nullptr != cube->FindTCFDir(tcf)) {
                continue;
            }

            auto tcfDir = std::make_shared<TCFDir>();
            tcfDir->SetPath(tcf);            
            cube->AddTCFDir(tcfDir);
        }

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->UpdateLink(parentProject);
            //port->Update(parentProject);
            //port->ModifyItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
        }

        return true;        
    }
    auto ModifyPlayground::LinkFolderToCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if(true == playgroundPath.isEmpty()){            
            return false;
        }
        if(true == folderPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        if(nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }


        //remove folder link from playground
        auto tcf = playground->FindTCFDir(folderPath);                

        //link folder to existing cube
        auto tcfDir = std::make_shared<TCFDir>();
        tcfDir->SetPath(folderPath);
        tcfDir->SetPosition(tcf->GetPosition());
        tcfDir->ScanDir();

        auto cube = playground->FindCube(cubeName);
        cube->AddTCFDir(tcfDir);

        playground->RemoveTCFDir(folderPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {            
            port->ModifyItem(cube,nullptr,TCFDir::List(),PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::UnlinkTCFs(const QString& playgroundPath, const QStringList& tcfList, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == tcfList.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //unlink files from existing cube
        auto cube = playground->FindCube(cubeName);

        for (const auto& tcf : tcfList) {
            cube->RemoveTCFDir(tcf);            
        }        

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->UpdateLink(parentProject);
            //port->Update(parentProject);
            //port->ModifyItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::UnLinkFolderFromCube(const QString& playgroundPath, const QString& folderPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == folderPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //unlink folder from existing cube
        auto cube = playground->FindCube(cubeName);
        //cube->RemoveTCFDir(folderPath);                       
        auto tcf = cube->FindTCFDir(folderPath);
        //check if same folder exist in outside or not
        auto tcfList = TCFDir::List();
        if(nullptr == playground->FindTCFDir(folderPath)) {
            auto tcfDir = std::make_shared<TCFDir>();
            tcfDir->SetPath(folderPath);
            tcfDir->SetPosition(tcf->GetPosition());
            tcfDir->ScanDir();            

            playground->AddTCFDir(tcfDir);
            tcfList.append(tcfDir);
        }

        cube->RemoveTCFDir(folderPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }                
        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(nullptr, nullptr, tcfList, PluginAppInfo::List());
            port->ModifyItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
        }
        //apply item changes to playground panel
        return true;
    }
    auto ModifyPlayground::LinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if(true == hyperCubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }        
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //copy cube info from playground
        auto cube = std::make_shared<Cube>(playground->FindCube(cubeName));

        //link cube to existing hypercube
        auto hypercube = playground->FindHypercube(hyperCubeName);
        if(hypercube->GetCubeList().size()>1) {
            QMessageBox::warning(nullptr, "Error", "Maximum cube count is 2!");
            return false;
        }
        hypercube->AddCube(cube);

        //remove cube from playground
        playground->RemoveCube(cubeName);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->ModifyItem(nullptr, hypercube, TCFDir::List(), PluginAppInfo::List());
            port->Update(parentProject);
        }

        return true;
    }
    auto ModifyPlayground::UnLinkCubeToHyperCube(const QString& playgroundPath, const QString& cubeName, const QString& hyperCubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (true == hyperCubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }
        
        auto hypercube = playground->FindHypercube(hyperCubeName);        
        
        //check if cube exist at outside or not
        Cube::Pointer cube = nullptr;
        if(nullptr == playground->FindCube(cubeName)) {
            //copy cube from hypercube
            cube = std::make_shared<Cube>(hypercube->FindCube(cubeName));
            //add cube to playground
            playground->AddCube(cube);
        }
        hypercube->RemoveCube(cubeName);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(cube,nullptr,TCFDir::List(),PluginAppInfo::List());
            port->ModifyItem(nullptr, hypercube, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::LinkAppToCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true==appPath.isEmpty()) {
            return false;
        }
        if(true == cubeName.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        if(nullptr == writer) {
            return false;
        }

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //copy application info
        auto app = std::make_shared<PluginAppInfo>(*playground->FindApp(appPath));        

        //link app to cube
        auto cube = playground->FindCube(cubeName);        
        cube->AddApp(app);

        //remove app from playground outside
        playground->RemoveApp(appPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }
                
        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->ModifyItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::UnLinkAppFromCube(const QString& playgroundPath, const QString& appPath, const QString& cubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == appPath.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }

        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }


        auto cube = playground->FindCube(cubeName);                
        //check app already exist in outside of not
        auto appList = PluginAppInfo::List();
        if(nullptr == playground->FindApp(appPath)) {
            //copy app info from cube and add to playground
            auto app = std::make_shared<PluginAppInfo>(*cube->FindApp(appPath));
            playground->AddApp(app);
            
            appList.append(app);            
        }
        //remove app from cube
        cube->RemoveApp(appPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
            port->ModifyItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::LinkAppToHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if(true == playgroundPath.isEmpty()) {
            return false;
        }
        if(true == appPath.isEmpty()) {
            return false;
        }
        if(true==hypercubeName.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }
        if(nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        //copy app info
        auto app = std::make_shared<PluginAppInfo>(*playground->FindApp(appPath));

        //link app to hypercube
        auto hypercube = playground->FindHypercube(hypercubeName);
        hypercube->AddApp(app);

        //remove app from playground
        playground->RemoveApp(appPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->ModifyItem(nullptr, hypercube, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::UnLinkAppFromHyperCube(const QString& playgroundPath, const QString& appPath, const QString& hypercubeName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == appPath.isEmpty()) {
            return false;
        }
        if (true == hypercubeName.isEmpty()) {
            return false;
        }
        if (nullptr == port) {
            return false;
        }
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }
                
        auto hypercube = playground->FindHypercube(hypercubeName);
        auto appList = PluginAppInfo::List();
        //check if app is already exist in playground or not
        if(playground->FindApp(appPath) == nullptr) {
            //copy app from hypercube            
            auto app = std::make_shared<PluginAppInfo>(*hypercube->FindApp(appPath));
            //link app to playground
            playground->AddApp(app);
            appList.append(app);
        }
        //remove app from hypercube
        hypercube->RemoveApp(appPath);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        // apply item changes to playground panel
        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(nullptr, nullptr, TCFDir::List(), appList);
            port->ModifyItem(nullptr, hypercube, TCFDir::List(), PluginAppInfo::List());
        }

        return true;
    }
    auto ModifyPlayground::MoveApplication(const QString& playgroundPath, const QString& appPath, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        Q_UNUSED(port)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == appPath.isEmpty()) {
            return false;
        }        
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto app = playground->FindApp(appPath);
        if(nullptr == app) {
            QLOG_ERROR() << "No app for " << appPath;
            return false;
        }

        app->SetPosition(pos);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        //moved position already applied to panel by itself
        return true;
    }
    auto ModifyPlayground::MoveCube(const QString& playgroundPath, const QString& cubeName, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        Q_UNUSED(port)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == cubeName.isEmpty()) {
            return false;
        }                
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto cube = playground->FindCube(cubeName);
        cube->SetPosition(pos);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        return true;
    }
    auto ModifyPlayground::MoveHyperCube(const QString& playgroundPath, const QString& hypercubeName, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        Q_UNUSED(port)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == hypercubeName.isEmpty()) {
            return false;
        }        
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto hyper = playground->FindHypercube(hypercubeName);
        hyper->SetPosition(pos);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        return true;
    }
    auto ModifyPlayground::MoveTCF(const QString& playgroundPath, const QString& folderPath, const QPoint& pos, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const -> bool {
        Q_UNUSED(port)
        if (true == playgroundPath.isEmpty()) {
            return false;
        }
        if (true == folderPath.isEmpty()) {
            return false;
        }        
        if (nullptr == writer) {
            return false;
        }
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        //get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto tcf = playground->FindTCFDir(folderPath);
        tcf->SetPosition(pos);


        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }
        
        return true;
    }

}