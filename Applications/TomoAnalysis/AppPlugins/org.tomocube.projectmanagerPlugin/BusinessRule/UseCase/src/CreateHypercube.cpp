#include <QDirIterator>
#include <QFileInfo>

#include <FileUtility.h>
#include <ProjectStorage.h>

#include "CreateHypercube.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct CreateHypercube::Impl {
        
    };

    CreateHypercube::CreateHypercube() : d{ new Impl } {
        
    }

    CreateHypercube::~CreateHypercube() {
        
    }

    auto CreateHypercube::Request(const QString& playgroundPath, const QString& name, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if(nullptr == playground) {
            return false;
        }

        if(nullptr != playground->FindHypercube(name)) {
            return false;
        }

        const auto hypercube = std::make_shared<HyperCube>();
        hypercube->SetName(name);
        playground->AddHyperCube(hypercube);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(nullptr, hypercube, TCFDir::List(),PluginAppInfo::List());
            port->Update(parentProject);
        }

        return true;
    }

    auto CreateHypercube::IsDuplicate(const QString& playgroundPath, const QString& name)->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == name.isEmpty()) {
            return false;
        }

        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        if (nullptr != playground->FindHypercube(name)) {
            return true;
        }

        return false;
    }
}