#include <iostream>
#include <QDirIterator>
#include <QFileInfo>

#include <FileUtility.h>
#include <ProjectStorage.h>

#include "CreatePlayground.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    CreatePlayground::CreatePlayground() {
    }

    CreatePlayground::~CreatePlayground() {
    }

    auto CreatePlayground::Request(const QString& path, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        if(nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }                        
        if (false == TC::MakeDir(QFileInfo(path).absoluteDir().path())) {            
            return false;
        }

        // get parent project path
        auto parentDir = QFileInfo(path).absoluteDir();
        parentDir.cdUp();
    
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());        
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {            
            return false;
        }

        // generate playground info
        const auto playgroundInfo = std::make_shared<PlaygroundInfo>();
        playgroundInfo->SetPath(path);
        playgroundInfo->SetName(TC::GetBaseName(path));

        if(false == writer->WritePlaygroundData(path, playgroundInfo)) {            
            return false;
        }

        parentProject->AddPlaygroundInfo(playgroundInfo);
        parentProject->SetCurrentPlayground(path);

        port->Update(parentProject);

        return true;
    }

}
