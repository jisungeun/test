#include <QDirIterator>
#include <QFileInfo>

#include <ProjectStorage.h>

#include "OpenPlayground.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct OpenPlayground::Impl {

    };

    OpenPlayground::OpenPlayground() : d{ new Impl } {

    }

    OpenPlayground::~OpenPlayground() {

    }

    auto OpenPlayground::Request(const QString& path, IUpdatePlaygroundPort* port) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        // get parent project
        auto parentDir = QFileInfo(path).absoluteDir();
        parentDir.cdUp();

        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(path);
        if(nullptr == playground) {
            return false;
        }

        if(false == parentProject->SetCurrentPlayground(playground->GetPath())) {
            return false;
        }

        //port->Update(parentProject);
        //port->Refresh(playground);
        port->ChangeCurrent(parentProject);

        return true;
    }
}
