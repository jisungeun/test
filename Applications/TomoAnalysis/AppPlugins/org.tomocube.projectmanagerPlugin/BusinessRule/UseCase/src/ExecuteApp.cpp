#include "ExecuteApp.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ExecuteApp::Impl {

    };
    ExecuteApp::ExecuteApp() : d{ new Impl } {

    }
    ExecuteApp::~ExecuteApp() {

    }
    auto ExecuteApp::Request() -> bool {
        return EXIT_SUCCESS;
    }
}