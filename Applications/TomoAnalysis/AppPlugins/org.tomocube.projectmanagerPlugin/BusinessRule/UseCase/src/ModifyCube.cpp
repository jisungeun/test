#pragma once

#include <QDirIterator>
#include <QFileInfo>

#include <ProjectStorage.h>

#include "ModifyCube.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ModifyCube::Impl {

    };

    ModifyCube::ModifyCube() : d{ new Impl } {

    }

    ModifyCube::~ModifyCube() = default;

    auto ModifyCube::LinkFolder(const QString& playgroundPath, const QString& cubeName, const QString& folderPath, 
                                IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) const ->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == cubeName.isEmpty()) {
            return false;
        }

        if (true == folderPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        const auto cube = playground->FindCube(cubeName);
        if(nullptr == cube) {
            return false;
        }

        auto tcfDir = std::make_shared<TCFDir>();
        tcfDir->SetPath(folderPath);
        tcfDir->ScanDir();

        cube->AddTCFDir(tcfDir);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        if(false == parentProject->ExistTCFDir(folderPath)) {
            parentProject->AddTCFDir(tcfDir);
            writer->WriteProjectData(parentProject->GetPath(), parentProject);
            port->LinkTCFDirToProject(parentProject, tcfDir);
        }

        return true;
    }
}
