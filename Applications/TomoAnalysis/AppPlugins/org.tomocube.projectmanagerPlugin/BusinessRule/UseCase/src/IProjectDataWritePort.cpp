#include "IProjectDataWritePort.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	IProjectDataWritePort::IProjectDataWritePort() = default;
	IProjectDataWritePort::~IProjectDataWritePort() = default;
}