#include "ShowTCFInfo.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ShowTCFInfo::Impl {

    };
    ShowTCFInfo::ShowTCFInfo() : d{ new Impl } {

    }
    ShowTCFInfo::~ShowTCFInfo() {

    }
    auto ShowTCFInfo::Request() -> bool {
        return EXIT_SUCCESS;
    }
}