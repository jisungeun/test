#include <QDirIterator>

#include <ProjectStorage.h>

#include "OpenProject.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct OpenProject::Impl {

    };

    OpenProject::OpenProject() : d{ new Impl } {

    }

    OpenProject::~OpenProject() {

    }

    auto OpenProject::Request(const QString& path, IUpdateProjectPort* port, IProjectDataReadPort* reader) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == reader) {
            return false;
        }

        auto LoadPlaygroundInfoList = [=](const QString& path, IProjectDataReadPort* reader) {
            PlaygroundInfo::List list;

            const QDir playgroundDir(path);
            if (false == playgroundDir.exists()) {
                return list;
            }

            auto playgroundFiles = playgroundDir.entryList(QStringList() << "*.tcpg" << "*.TCPG", QDir::Files);
            for (const auto& file : playgroundFiles) {
                auto playgroundPath = QString("%1/%2").arg(path).arg(file);
                auto playground = std::make_shared<PlaygroundInfo>();
                if (false == reader->ReadPlaygroundData(playgroundPath, playground)) {
                    continue;
                }

                list.append(playground);
            }

            return list;
        };

        auto projectInfo = Entity::ProjectStorage::GetInstance()->Publish(path);
        if(false == reader->ReadProjectData(path, projectInfo)) {
            return false;
        }

        const auto dir = QFileInfo(path).absoluteDir();

        // get playground info
        const auto playgroundFolderPath = dir.filePath("playground");
        projectInfo->SetPlaygroundInfoList(LoadPlaygroundInfoList(playgroundFolderPath, reader));

        projectInfo->ScanTCFDir();

        port->Load(projectInfo);

        return true;
    }
}
