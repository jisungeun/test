#include "IProjectDataReadPort.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	IProjectDataReadPort::IProjectDataReadPort() = default;
	IProjectDataReadPort::~IProjectDataReadPort() = default;
}