#define LOGGER_TAG "[Project Manager]"
#include <TCLogger.h>

#include <iostream>

#include <ctkPluginException.h>
#include <ctkPluginFrameworkLauncher.h>

#include <QDirIterator>
#include <QFileInfo>
#include <QMessageBox>

#include <PluginRegistry.h>

#include <EventPublisher.h>

#include <AppEvent.h>
#include <ProjectStorage.h>

#include "ModifyApplication.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ModifyApplication::Impl {
        
    };

    ModifyApplication::ModifyApplication() : d{ new Impl } {
        
    }

    ModifyApplication::~ModifyApplication() = default;

    auto ModifyApplication::LoadDlls(const QString& rootFolder, IUpdateApplicationPort* port) const -> bool {
        if(true == rootFolder.isEmpty()) {
            return false;
        }
        if(nullptr == port) {
            return false;
        }

        if (false == activateAlgorithms(rootFolder)) {
            QMessageBox::warning(nullptr, "Error", "Failed to load algorithms");
            return false;
        }

        PluginAppInfo::List pluginList;
        ProcessorInfo::List processorList;

        
        auto pluginPath = rootFolder + "/plugins";

        if(true == pluginPath.isEmpty()) {
            return false;
        }
        QDir dir(pluginPath);                
        if (false == dir.exists() || false == dir.isReadable()) {
            return false;
        }

        dir.setNameFilters(QStringList() << "*.dll" << "*.DLL");
        dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        auto fileList = dir.entryList();
        for (const auto& file : fileList) {
            auto abs = dir.absoluteFilePath(file);            
            if (false == file.contains("project")) {
                auto info = std::make_shared<PluginAppInfo>();
                //info->SetName(file);
                info->SetName(parsePluginName(file));
                info->SetPath(abs);

                pluginList.append(info);
            }
        }

        auto processorPath = rootFolder + "/processor";
        
        if (true == processorPath.isEmpty()) {
            return false;
        }
        QDir pdir(processorPath);
        if (false == pdir.exists() || false == pdir.isReadable()) {
            return false;
        }

        pdir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        auto procList = pdir.entryList();
        for (const auto& proc : procList) {            
            QDir dd = pdir;
            dd.cd(proc);

            dd.setNameFilters(QStringList() << "*.dll" << "*.DLL");
            dd.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

            auto procName = dd.entryList();
            for (const auto& name : procName) {
                auto abs = dd.absoluteFilePath(name);                
                auto info = std::make_shared<ProcessorInfo>();
                info->SetName(name);
                info->SetPath(abs);
                //�ӽ�
                auto procc = QString();                
                if(proc.contains("basicanalysis")) {
                    procc = "segmentation";
                }
                info->SetParent(procc);

                processorList.append(info);
            }
        }
        port->Update(pluginList, processorList);

        

        return true;
    }

    auto ModifyApplication::ActivatePlugin(const QString& dllPath, IUpdateApplicationPort* port) const -> bool {        
        bool succeeded = false;
        QFileInfo file(dllPath);        
        auto fname = parsePluginName(file.fileName());
        try {            
            succeeded = ctkPluginFrameworkLauncher::start(fname);
        }catch(ctkPluginException& e) {
            QLOG_ERROR() << "Error in " << dllPath << " " << e.message();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();

            return false;
        }
        catch (ctkRuntimeException& e)
        {
            QLOG_ERROR() << "Error in " << dllPath << " " << e.what();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();

            return false;
        }

        PluginAppInfo::List pluginList;
        ProcessorInfo::List procList;
        auto info = std::make_shared<PluginAppInfo>();
        //info->SetName(file);
        info->SetName(fname);
        info->SetPath(dllPath);
        info->SetLoaded(true);

        pluginList.append(info);

        port->Update(pluginList, procList);

        return succeeded;
    }

    auto ModifyApplication::ActivateProcessor(const QString& dllPath, IUpdateApplicationPort* port) const -> bool {       
        if(PluginRegistry::LoadPlugin(dllPath) ==0) {
            QLOG_ERROR() << "failed to load processor " << dllPath;
            return false;
        }
        QFileInfo file(dllPath);
        auto fname = file.fileName();
                
        PluginAppInfo::List pluginList;
        ProcessorInfo::List procList;
        auto info = std::make_shared<ProcessorInfo>();
        //info->SetName(file);
        info->SetName(fname);
        info->SetPath(dllPath);
        info->SetLoaded(true);

        procList.append(info);

        port->Update(pluginList, procList);

        return true;
    }

    auto ModifyApplication::LoadPluginParameter(const QString& playgroundPath,const QString& dllPath, IUpdateApplicationPort* port, const QString& parentName) const -> bool {
        //load processor for given app
        auto parentDir = QFileInfo(playgroundPath).absoluteDir();
        parentDir.cdUp();
        const auto parentPath = QString("%1/%2.tcpro").arg(parentDir.path()).arg(parentDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(parentPath);
        if (nullptr == parentProject) {
            return false;
        }
        // get playground
        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }
        PluginAppInfo::Pointer app;
        if(parentName.isEmpty()){
            app = playground->FindApp(dllPath);
        }else {
            auto type = parentName.split("!")[0];
            if(type.compare("HyperCube")==0) {
                auto hyper = playground->FindHypercube(parentName.split("!")[1]);
                app = hyper->FindApp(dllPath);
            }else if(type.compare("Cube")==0){
                auto cube = playground->FindCube(parentName.split("!")[1]);
                app = cube->FindApp(dllPath);
            }
        }

        ProcessorInfo::List procList;
        for(const auto proc:app->GetProcessors()) {
            procList.append(proc);
        }        

        PluginAppInfo::List appList;
        appList.append(app);

        port->Update(procList,appList);

        return true;
    }

    auto ModifyApplication::parsePluginName(const QString& fileName) const -> QString {
        auto split = fileName.split(".");
        auto name = QString();
        for(int i=0;i<split.size()-1;i++) {
            name += split[i];
        }
        //name without .dll
        name = name.replace("liborg", "org");

        name = name.replace("_", ".");

        return name;
    }
    auto ModifyApplication::activateAlgorithms(const QString& rootFolder) const -> bool {
        auto algorithmPath = rootFolder + "/algorithms";        

        QDir dir(algorithmPath);
        if (false == dir.exists() || false == dir.isReadable()) {
            return false;
        }

        dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        auto algoList = dir.entryList();
        for (const auto& algo : algoList) {
            QDir dd = dir;
            dd.cd(algo);

            dd.setNameFilters(QStringList() << "*.dll" << "*.DLL");
            dd.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

            auto algoName = dd.entryList();
            for (const auto& name : algoName) {
                auto abs = dd.absoluteFilePath(name);                
                if (PluginRegistry::LoadPlugin(abs) == 0) {
                    QLOG_ERROR() << "failed to load plugin " << abs;
                    return false;
                }
            }
        }
        return true;
    }

}
