#include <iostream>
#include <QDirIterator>
#include <QFileInfo>

#include <FileUtility.h>
#include <ProjectStorage.h>

#include "CreateCube.h"


namespace TomoAnalysis::ProjectManager::UseCase {
    struct CreateCube::Impl {


    };
    CreateCube::CreateCube() : d{ new Impl } {
        
    }

    CreateCube::~CreateCube() {
        
    }

    auto CreateCube::Request(const QString& playgroundPath, const QString& name, const QString& hyperName, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }
        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        auto cube = std::make_shared<Cube>();
        cube->SetName(name);
        //cube->SetID(playground->GetCubeList().count());        

        auto hyperList = playground->GetHyperCubeList();
        HyperCube::Pointer found{nullptr};
        for(const auto& hcube : hyperList) {
            if(hcube->GetName().compare(hyperName)==0) {
                found = hcube;
                break;
            }
        }
        if(nullptr == found) {
            return false;
        }
        cube->SetID(found->GetCubeList().count());        
        found->AddCube(cube);
        //playground->AddCube(cube);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            //port->AddItem(cube, nullptr, TCFDir::List(), PluginAppInfo::List());
            //port->AddItem(cube, found);
            port->ModifyItem(nullptr, found, TCFDir::List(), PluginAppInfo::List());
            port->Update(parentProject);
        }

        return true;
    }


    auto CreateCube::Request(const QString& playgroundPath, const QString& name, IUpdatePlaygroundPort* port, IProjectDataWritePort* writer) -> bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (nullptr == port) {
            return false;
        }

        if (nullptr == writer) {
            return false;
        }

        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if(nullptr == playground) {
            return false;
        }

        auto cube = std::make_shared<Cube>();
        cube->SetName(name);
        cube->SetID(playground->GetCubeList().size());
        playground->AddCube(cube);

        if (false == writer->WritePlaygroundData(playgroundPath, playground)) {
            return false;
        }

        const auto currentPlayground = parentProject->GetCurrentPlayground();
        if (currentPlayground == playground) {
            port->AddItem(cube, nullptr, TCFDir::List(),PluginAppInfo::List());
            port->Update(parentProject);
        }

        return true;
    }

    auto CreateCube::IsDuplicate(const QString& playgroundPath, const QString& hypercube, const QString& cube)->bool {
        if (true == playgroundPath.isEmpty()) {
            return false;
        }

        if (true == hypercube.isEmpty()) {
            return false;
        }

        if (true == cube.isEmpty()) {
            return false;
        }

        auto playgroundDir = QFileInfo(playgroundPath).absoluteDir();
        playgroundDir.cdUp();

        const auto projectPath = QString("%1/%2.tcpro").arg(playgroundDir.path()).arg(playgroundDir.dirName());
        const auto parentProject = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
        if (nullptr == parentProject) {
            return false;
        }

        const auto playground = parentProject->FindPlaygroundInfo(playgroundPath);
        if (nullptr == playground) {
            return false;
        }

        for(auto hyper : playground->GetHyperCubeList()) {
            if (nullptr == hyper)
                continue;
            if (nullptr != hyper->FindCube(cube)) {
                return true;
            }
        }

        /*const auto parentHypercube = playground->FindHypercube(hypercube);
        if (nullptr == parentHypercube) {
            return false;
        }

        if(nullptr != parentHypercube->FindCube(cube)) {
            return true;
        }*/

        return false;
    }
}
