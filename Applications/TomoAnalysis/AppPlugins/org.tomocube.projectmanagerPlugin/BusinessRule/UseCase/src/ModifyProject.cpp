#include <iostream>

#include <QDir>
#include <ProjectStorage.h>

#include "ModifyProject.h"

namespace TomoAnalysis::ProjectManager::UseCase {
	struct ModifyProject::Impl {

	};

	ModifyProject::ModifyProject() : d{ new Impl } {

	}

	ModifyProject::~ModifyProject() = default;

	auto ModifyProject::RenameProject(const QString& projectPath, const QString& newName, IUpdateProjectPort* port, IProjectDataWritePort* writer) const -> bool {
		if (true == projectPath.isEmpty()) {
			return false;
		}
		if (true == newName.isEmpty()) {
			return false;
		}
		if (nullptr == port) {
			return false;
		}
		if (nullptr == writer) {
			return false;
		}

		const auto project = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
		if (nullptr == project) {
			return false;
		}

		auto oriname = project->GetName();

		QDir dir(projectPath);
		dir.cdUp();
		auto folderPath = dir.absolutePath();

		dir.cdUp();
		auto parentPath = dir.absolutePath();

		auto newFolderPath = parentPath + "/" + newName;
		auto tepmPath = folderPath + "/" + newName + ".tcpro";

		//update pg info first
		project->UpdatePGInfo(oriname, newName);

		QFile fileInfo(projectPath);
		fileInfo.rename(tepmPath);

		QFile folderInfo(folderPath);
		folderInfo.rename(newFolderPath);

		auto newPath = parentPath + "/" + newName + "/" + newName + ".tcpro";

		project->SetName(newName);
		project->SetPath(newPath);

		if (false == writer->WriteProjectData(newPath, project)) {
			return false;
		}

		port->Update(project);

		return true;
	}


	auto ModifyProject::LinkFolder(const QString& projectPath, const QString& folderPath,
		IUpdateProjectPort* port, IProjectDataWritePort* writer) const ->bool {
		if (true == projectPath.isEmpty()) {
			return false;
		}

		if (true == folderPath.isEmpty()) {
			return false;
		}

		if (nullptr == port) {
			return false;
		}

		if (nullptr == writer) {
			return false;
		}

		const auto project = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
		if (nullptr == project) {
			return false;
		}

		auto tcfDir = std::make_shared<TCFDir>();
		tcfDir->SetPath(folderPath);
		tcfDir->ScanDir();

		project->AddTCFDir(tcfDir);

		port->AddTCF(project, tcfDir);
		//port->Update(project);
		//port->AddItem(nullptr, nullptr, tcfDir->GetDirList());

		if (false == writer->WriteProjectData(projectPath, project)) {
			return false;
		}

		return true;
	}

	auto ModifyProject::UnlinkFolder(const QString& projectPath, const QString& folderPath,
		IUpdateProjectPort* port, IProjectDataWritePort* writer) const ->bool {
		if (true == projectPath.isEmpty()) {
			return false;
		}

		if (true == folderPath.isEmpty()) {
			return false;
		}

		if (nullptr == port) {
			return false;
		}

		if (nullptr == writer) {
			return false;
		}

		const auto project = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
		if (nullptr == project) {
			return false;
		}

		const auto dir = project->GetTCFDir(folderPath);
		if (nullptr == dir) {
			return false;
		}

		QStringList fileList;
		dir->GetAllFileFullPathList(dir, fileList);

		for (const auto& playground : project->GetPlaygroundInfoList()) {
			if (nullptr == playground) {
				continue;
			}

			for (const auto& hypercube : playground->GetHyperCubeList()) {
				if (nullptr == hypercube) {
					continue;
				}

				for (const auto& cube : hypercube->GetCubeList()) {
					if (nullptr == cube) {
						continue;
					}

					for (const auto& file : fileList) {
						cube->RemoveTCFDir(file);
					}


				}
			}
		}

		const auto worksetIds = project->GetWorksetIdOfTcfDir(dir);

		if (!worksetIds.isEmpty()) {
			for (const auto id : worksetIds) {
				project->UnlinkTcfDirToWorkset(id, dir);

				if (project->GetCountOfWorkset(id) == 0) {
					project->RemoveWorkset(id);
				}
			}
		}

		if (false == project->RemoveTCFDir(folderPath)) {
			return false;
		}

		if (false == writer->WriteProjectData(projectPath, project)) {
			return false;
		}

		port->Update(project);

		return true;
	}

	auto ModifyProject::LinkWorkset(const QString& projectPath, int worksetId, const QString& worksetName, const QString& folderPath, IUpdateProjectPort* port, IProjectDataWritePort* writer) const -> bool {
		if (true == projectPath.isEmpty()) {
			return false;
		}

		if (true == worksetName.isEmpty()) {
			return false;
		}

		if (true == folderPath.isEmpty()) {
			return false;
		}

		if (nullptr == port) {
			return false;
		}

		if (nullptr == writer) {
			return false;
		}

		const auto project = Entity::ProjectStorage::GetInstance()->FindProject(projectPath);
		if (nullptr == project) {
			return false;
		}

		if (!project->ExistsWorkset(worksetId)) {
			project->AddWorkset(worksetName, worksetId);
		}

		const auto dir = project->GetTCFDir(folderPath);

		if (!dir)
			return false;

		project->LinkTcfDirToWorkset(worksetId, dir);

		if (false == writer->WriteProjectData(projectPath, project)) {
			return false;
		}

		port->Update(project);

		return true;
	}
}
