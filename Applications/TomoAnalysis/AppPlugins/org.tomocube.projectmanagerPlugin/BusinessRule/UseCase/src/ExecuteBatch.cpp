#include "ExecuteBatch.h"

namespace TomoAnalysis::ProjectManager::UseCase {
    struct ExecuteBatch::Impl {

    };
    ExecuteBatch::ExecuteBatch() : d{ new Impl } {

    }
    ExecuteBatch::~ExecuteBatch() {

    }
    auto ExecuteBatch::Request() -> bool {
        return EXIT_SUCCESS;
    }
}