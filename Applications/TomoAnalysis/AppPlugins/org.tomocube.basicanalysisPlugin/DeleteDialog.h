#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::BasicAnalysis::AppUI {
    class DeleteDialog : public QDialog {
        Q_OBJECT

    public:
        explicit DeleteDialog(QWidget* parent, const QString& project,const QString&playground,const QString& hypercube,const QString& timestamp);
        virtual ~DeleteDialog();

    public:
        static auto Delete(QWidget* parent, const QString& project,const QString&playground, const QString& hypercube, const QString& timestamp)->bool;

    protected slots:
        void on_deleteButton_clicked();
        void on_cancelButton_clicked();

        void OnProjectText(QString);
        void OnPGText(QString);
        void OnHyperText(QString);
        void OnTimeText(QString);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
