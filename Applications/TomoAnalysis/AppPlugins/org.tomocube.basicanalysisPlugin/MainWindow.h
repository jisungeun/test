#pragma once
#include <QMutexLocker>

#include <IMainWindowTA.h>

#include <BasicAnalysisProcessor.h>
#include <WorkingSet.h>

#include "ILicensed.h"

namespace TomoAnalysis::BasicAnalysis::AppUI {  
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto GetCurTCF()const->QString;

        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto ForceClosePopup()->void;
        auto GetRunType() -> std::tuple<bool, bool> override;
        auto Execute(const QVariantMap& params)->bool override;

        auto TryDeactivate() -> bool final;
        auto TryActivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo() -> QVariantMap final;

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

        static void* ThreadFunc(void* userData);

    signals:
        void testSignal();
        void OstepChanged(int);
        void Ofinished(int);
        void OstepStarted(int);

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void HandleProcValChange(double, QString);
        void HandleProcChange(QString);
        void HandleTimeChange(int);
        void HandleAlgoExecution(QString,QString);
        void HandleMaskSelection(QString);
        void HandleSingleCSV();
        void HandleCloseResult();
        void HandleRefreshHighlight();

        void HandleMaskCorrection();

        void HandleToggleResult(bool);
        void HandleApplyParameter(QString);

        void HandleMaskViz(QString, bool);
        void HandleMultiMaskViz(QStringList);
        void HandleMaskOpacity(double);
        void HandleMaskCellFilter(bool);

        void OnTabFocused(const ctkEvent& ctkEvent);
        void OnAcceptBatchRun();
        void OnRejectBatchRun();
        void OnAcceptValidity();
        void OnRejectValidity();

        void OivFinished(int);
        void OivStarted(int);
        void OivChanged(int);
        void OnBatchCanceled();        

    private:
        auto InitUI(void)->bool;
        auto HandleSingleExecution(QString,QString)->void;
        auto HandleInternalExecution(QString,QString)->void;
        auto HandleTimeLapseExecution(QString)->void;        

        auto ExecuteBatchRun(const QVariantMap& params)->bool;
        auto ExecuteOpenTCF(const QVariantMap& params)->bool;
        auto ExecuteUpdate(const QVariantMap& params)->bool;        

        auto UpdateApplication()->void;
        auto CopyPath(QString src, QString dst)->void;
        
        auto BroadcastApplyParam(QString)->void;
        auto BroadcastFinishBatch()->void;
        auto BroadcastAbortBatch()->void;
        auto BroadcastCorrection(QString,QString)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
