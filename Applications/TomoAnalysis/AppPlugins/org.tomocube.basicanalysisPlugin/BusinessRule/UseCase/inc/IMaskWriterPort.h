#pragma once

#include <QString>
#include <IBaseMask.h>
#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API IMaskWriterPort {
    public:
        IMaskWriterPort();
        virtual ~IMaskWriterPort();

        virtual auto SetLayerNames(QStringList name)->void=0;
        virtual auto Write(IBaseMask::Pointer data, const QString& path,int time_step =0)->bool = 0;
    };
}