#pragma once

#include <QString>
#include <TCMeasure.h>
#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API IMeasureWriterPort {
    public:
        IMeasureWriterPort();
        virtual ~IMeasureWriterPort();

        virtual auto Write(TCMeasure::Pointer data, const QString& path)->bool = 0;
        virtual auto Modify(TCMeasure::Pointer data, const QString& path)->bool = 0;
    };
}