#pragma once

#include <memory>

#include <WorkingSet.h>

#include "IMeasureReaderPort.h"
#include "IMaskWriterPort.h"

#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API SaveResult {
    public:
        SaveResult();
        virtual ~SaveResult();

        auto SaveCurrentMeasure(const QString& path,Entity::WorkingSet::Pointer workingset,IMeasureReaderPort* port)->bool;
        auto SaveBatchMeasure(const QString& path, const QString& hyperName, Entity::WorkingSet::Pointer workingset, IMeasureReaderPort* port)->bool;
        auto SaveMasks(Entity::WorkingSet::Pointer workingset, IMaskWriterPort* port)->bool;

    private:
        auto addMetric(QString measure)->QString;
    };
}