#pragma once

#include <QString>
#include <TCMeasure.h>
#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API IMeasureReaderPort {
    public:
        IMeasureReaderPort();
        virtual ~IMeasureReaderPort();

        virtual auto Read(const QString& path,int time_step)->TCMeasure::Pointer = 0;
    };
}