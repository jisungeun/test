#pragma once

#include <memory>

#include <WorkingSet.h>

#include "IImageReaderPort.h"
#include "IMaskReaderPort.h"
#include "IMeasureReaderPort.h"

#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API OpenDataSet {
    public:
        OpenDataSet();
        virtual ~OpenDataSet();

        auto OpenTcfImage(const QString& path,const QString& playPath,IImageReaderPort* reader, Entity::WorkingSet::Pointer workingset,int time_step =0 )const ->bool;
        auto OpenTimedTcf(Entity::WorkingSet::Pointer workingset, IImageReaderPort* reader)const ->bool;
        auto OpenTimedMask(Entity::WorkingSet::Pointer workingset, IMaskReaderPort* reader)const ->bool;
        auto OpenTimedResult(Entity::WorkingSet::Pointer workingset, IMeasureReaderPort* reader)const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}