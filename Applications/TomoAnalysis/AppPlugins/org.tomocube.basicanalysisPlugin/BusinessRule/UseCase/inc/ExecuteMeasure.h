#pragma once

#include <memory>
#include <WorkingSet.h>

#include "IProcessingEngine.h"
#include "IImageReaderPort.h"
#include "IMaskReaderPort.h"
#include "IMeasureWriterPort.h"

#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
	class BasicAnalysisUseCase_API ExecuteMeasure {
	public:
		ExecuteMeasure();
		virtual ~ExecuteMeasure();

		auto Request(Entity::WorkingSet::Pointer workingset, const QString& algo_name, IParameter::Pointer param,
			IProcessingEngine* engine, IMeasureWriterPort* writer,bool saveResult = true)->bool;
		auto Refresh(const QString& pg_path, const QString& tcf_path, const QString& hypercube_name,const QString& user_name, IProcessingEngine* engine,IImageReaderPort* ireader,IMaskReaderPort* mreader, IMeasureWriterPort* writer)->bool;
	private:
		auto CopyPath(QString src, QString dst)->void;
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}