#pragma once

#include <memory>

#include <WorkingSet.h>

#include "ISceneManagerPort.h"

#include "BasicAnalysisUseCaseExport.h"

namespace TomoAnalysis::BasicAnalysis::UseCase {
    class BasicAnalysisUseCase_API ModifyScene {
    public:
        ModifyScene();
        virtual ~ModifyScene();

        auto SetMaskList(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port)->bool;
        auto SetImage(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port)->bool;
        auto SetMask(Entity::WorkingSet::Pointer workingset, const QString& mask_name, ISceneManagerPort* port)->bool;
        auto SetMultiMask(Entity::WorkingSet::Pointer workingset, const QStringList& multi_mask, ISceneManagerPort* port)->bool;
        auto SetReport(Entity::WorkingSet::Pointer workingset, ISceneManagerPort* port)->bool;
    };
}