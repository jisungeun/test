#define LOGGER_TAG "[BasicAnalyzer]"
#include <TCLogger.h>

#include <iostream>
#include <QDir>

#include <IProcessor.h>
#include <TCMask.h>

#include <PluginRegistry.h>

#include "ExecuteProcessor.h"

#include <QElapsedTimer>

namespace TomoAnalysis::BasicAnalysis::UseCase {    
    ExecuteProcessor::ExecuteProcessor() {
        
    }
    ExecuteProcessor::~ExecuteProcessor() {
        
    }
    auto ExecuteProcessor::Request(Entity::WorkingSet::Pointer workingset, const QString& proc_name, IParameter::Pointer param, IProcessingEngine* engine, IMaskWriterPort* maskWriter, IMeasureWriterPort* measureWriter, bool saveResult) -> bool {
        Q_UNUSED(engine)
        workingset->SetParameter(param);
        auto moduleInstance = PluginRegistry::GetPlugin(proc_name);
        if(nullptr == moduleInstance) {
            QLOG_ERROR() << "module: " << proc_name<<" not exist";
            return false;
        }

        auto processorModule = std::dynamic_pointer_cast<IProcessor>(moduleInstance);
        if(!processorModule) {
            QLOG_ERROR()<< "Plugin instance was note a processor";
            return false;
        }

        //parse result format
        auto of = processorModule->GetOutputFormat();
        auto output_formats = of.split("*");
        if(output_formats.count()<1) {
            QLOG_ERROR()<< "No output processed";
            return false;
        }
        workingset->SetCurrentOutputFormat(of);

        DataSet::Pointer dset{DataSet::New()};
        dset->AppendData(workingset->GetImage(),"RefImage");        
                
        if(!processorModule->Parameter(param)) {
            QLOG_ERROR()<<"Parameter Setting Failed";
        }
        processorModule->SetData(dset);        
        processorModule->Execute();

        auto output = processorModule->GetResult();
        QString parent_path = QString();
        if(saveResult) {
            parent_path = workingset->GetWorkingDirectory();            
            auto hyperName = workingset->GetCurrentHyperCubeName();            
            auto parent_folder = parent_path.chopped(5);
            QDir pdir(parent_folder);
            if(false == pdir.exists()) {
                QDir().mkdir(parent_folder);
            }
            if(false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);
            parent_path = pdir.path();            
        }

        for(auto i=0;i< output->Count() ; i++) {
            auto entry = output->GetData(i);
            auto format = output_formats[i];                        
            auto type = format.split("!")[0];
            auto name = format.split("!")[1];
            if(type.contains("Mask")) {
                handleMask(workingset,entry,name,parent_path,processorModule->GetLayerName(name),maskWriter);
            }else if(type.contains("Measure")) {
                handleMeasure(workingset,entry,parent_path,measureWriter);
            }
        }

        //processorModule->FreeMemory();

        return true;
    }
    //private        
    auto ExecuteProcessor::handleMask(Entity::WorkingSet::Pointer workingset, IBaseData::Pointer data, QString name, QString savePath,QStringList layer_list,IMaskWriterPort* writer) -> void {
        QElapsedTimer etimer;
        etimer.start();
        auto mask = std::dynamic_pointer_cast<TCMask>(data);
        workingset->RemoveMask(name);
        workingset->AddMask(mask,name);
        workingset->SetMaskValid(true);
        auto path = savePath + "/" + workingset->GetCurrentImageName() + ".msk";
        workingset->SetMaskPath(path);
        if(false == savePath.isEmpty()) {//save result                        
            mask->SetTimeStep(workingset->GetImageTimeStep());            
            writer->SetLayerNames(layer_list);
            writer->Write(mask,path);            
        }
        QLOG_INFO() << "elapsed time for Mask I/O: ";
        QLOG_INFO() << etimer.elapsed();
    }

    auto ExecuteProcessor::handleMeasure(Entity::WorkingSet::Pointer workingset, IBaseData::Pointer data, QString savePath,IMeasureWriterPort* writer) -> void {
        QElapsedTimer etimer;
        etimer.start();
        auto measure = std::dynamic_pointer_cast<TCMeasure>(data);
        workingset->SetMeasure(measure);
        workingset->SetMeasureValid(true);
        if(false == savePath.isEmpty()) {
            auto path = savePath + "/"+workingset->GetCurrentImageName() + ".rep";            
            auto time_idx  = workingset->GetImageTimeStep();            

            measure->SetTimeIndex(time_idx);
            measure->SetTimePoint(workingset->GetImageTimePoints()[time_idx]);
            writer->Write(measure,path);
        }
        QLOG_INFO() << "elapsed time for measure I/O: ";
        QLOG_INFO() << etimer.elapsed();
    }
}
