#include <iostream>
#include <QFileInfo>

#include "WorkingSet.h"

namespace TomoAnalysis::BasicAnalysis::Entity {
    struct WorkingSet::Impl {
        QString workingDir;
        QString hypercubeName;
        QString imageName;
        QString outputFormat;

        //QStringList maskList;        
        QString maskDir;
        QString imageDir;
        TCImage::Pointer image;
        int time_steps{0};
        int cur_step{0};
        QMap<QString, TCMask::Pointer> mask;
        QList<double> tps;

        bool maskValid{ false };
        bool measureValid{ false };
        bool paramChanged{ false };

        Algorithm::Pointer processing_algorithm;//contains 1 or more algorithms
        IParameter::Pointer parameter;
        TCMeasure::Pointer measure;
    };
    WorkingSet::WorkingSet():d{new Impl} {
        
    }
    WorkingSet::~WorkingSet() {
        
    }    

    auto WorkingSet::GetInstance() -> Pointer {
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }

    auto WorkingSet::GetParamChanged() -> bool {
        return d->paramChanged;
    }

    auto WorkingSet::SetParamChanged(bool changed) -> void {
        d->paramChanged = changed;
    }

    auto WorkingSet::GetMaskValid() -> bool {
        return d->maskValid;
    }

    auto WorkingSet::SetMaskValid(bool valid) -> void {
        d->maskValid = valid;
    }

    auto WorkingSet::GetMeausreValid() -> bool {
        return d->measureValid;
    }

    auto WorkingSet::SetMeasureValid(bool valid) -> void {
        d->measureValid = valid;
    }

    auto WorkingSet::SetMaskPath(const QString& path)->void {
        d->maskDir = path;
    }
    
    auto WorkingSet::ClearMaskPath() -> void {
        d->maskDir = QString();
    }

    auto WorkingSet::ClearMeasure() -> void {
        d->measure = nullptr;
    }
    auto WorkingSet::ClearMask() -> void {
        d->mask.clear();
    }    
    auto WorkingSet::GetMeasurePath() const -> QString {
        auto measure_name = d->workingDir.chopped(5);
        measure_name += "/";
        measure_name += d->hypercubeName;
        measure_name += "/";
        measure_name += d->imageName;
        measure_name += ".rep";

        return measure_name;        
    }

    auto WorkingSet::GetMaskPath() const -> QString {

        auto mask_header = d->workingDir.chopped(5);
        mask_header += "/";
        mask_header += d->hypercubeName;
        mask_header += "/";
        mask_header += d->imageName;
        mask_header += ".msk";

        d->maskDir = mask_header;
        return d->maskDir;
    }

    auto WorkingSet::GetImagePath() const -> QString {
        return d->imageDir;
    }
    auto WorkingSet::SetImagePath(const QString& path) -> void {
        d->imageDir = path;
    }
    auto WorkingSet::SetImageTimePoints(QList<double> tps)->void {
        d->tps = tps;
    }
    auto WorkingSet::GetImageTimePoints()->QList<double> {
        return d->tps;
    }
    auto WorkingSet::GetCurrentImageName() const -> QString {
        return d->imageName;
    }

    auto WorkingSet::SetCurrentImageName(const QString& name) -> void {
        d->imageName = name;
    }
    auto WorkingSet::GetCurrentOutputFormat() const -> QString {
        return d->outputFormat;
    }
    auto WorkingSet::SetCurrentOutputFormat(const QString& format) -> void {
        d->outputFormat = format;
    }

    auto WorkingSet::GetWorkingDirectory() const -> QString {
        return d->workingDir;
    }
    auto WorkingSet::SetWorkingDirectory(const QString& path) -> void {
        d->workingDir = path;
    }
    auto WorkingSet::GetCurrentHyperCubeName() const -> QString {
        return d->hypercubeName;
    }
    auto WorkingSet::SetCurrentHyperCubeName(const QString& name) -> void {
        d->hypercubeName = name;
    }

    auto WorkingSet::SetImage(TCImage::Pointer image) -> void {
        //d->image = nullptr;
        d->image = image;
    }
    auto WorkingSet::GetImageTimeStep() const -> int {
        return d->cur_step;
    }
    auto WorkingSet::SetImageTimeStep(const int& ts) -> void {
        d->cur_step = ts;
    }
    auto WorkingSet::GetImageTimeSteps() const -> int {
        return d->time_steps;
    }

    auto WorkingSet::SetImageTimeSteps(const int& steps) -> void {
        d->time_steps = steps;
    }


    auto WorkingSet::GetImage() -> TCImage::Pointer {
        return d->image;
    }
    auto WorkingSet::SetProcessingAlgorithm(Algorithm::Pointer procAlgo) -> void {
        d->processing_algorithm = procAlgo;
    }
    auto WorkingSet::GetProcessingAlgorithm() -> Algorithm::Pointer {
        return d->processing_algorithm;
    }
    auto WorkingSet::SetParameter(IParameter::Pointer param) -> void {
        d->parameter = param;
    }
    auto WorkingSet::GetParameter() -> IParameter::Pointer {
        return d->parameter;
    }
    auto WorkingSet::SetMeasure(TCMeasure::Pointer measure) -> void {
        d->measure = measure;
    }
    auto WorkingSet::GetMeasure() -> TCMeasure::Pointer {
        return d->measure;
    }
    auto WorkingSet::AddMask(TCMask::Pointer mask, const QString& key) -> void {
        d->mask[key] = mask;
    }
    auto WorkingSet::RemoveMask(const QString& key) -> void {
        d->mask[key] = nullptr;
        d->mask.remove(key);        
    }
    auto WorkingSet::GetMask(const QString& key) -> TCMask::Pointer {
        return d->mask[key];
    }
    auto WorkingSet::GetMasks() -> QMap<QString, TCMask::Pointer> {
        return d->mask;
    }
}
