#pragma once

#include <memory>
#include <QMap>
#include <QString>

#include <TCImage.h>
#include <TCMask.h>
#include <TCMeasure.h>
#include <IParameter.h>

#include "Algorithm.h"

#include "BasicAnalysisEntityExport.h"

namespace TomoAnalysis::BasicAnalysis::Entity {
    class BasicAnalysisEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;
        WorkingSet();
    
        virtual ~WorkingSet();

        static auto GetInstance(void)->Pointer;

        auto SetWorkingDirectory(const QString& path)->void;
        auto GetWorkingDirectory()const->QString;

        auto SetCurrentImageName(const QString& name)->void;
        auto GetCurrentImageName()const->QString;

        auto SetCurrentOutputFormat(const QString& format)->void;
        auto GetCurrentOutputFormat()const->QString;

        auto SetCurrentHyperCubeName(const QString& name)->void;
        auto GetCurrentHyperCubeName()const->QString;

        auto SetImage(TCImage::Pointer image)->void;
        auto GetImage()->TCImage::Pointer;

        auto SetImagePath(const QString& path)->void;
        auto GetImagePath()const-> QString;

        auto SetImageTimeStep(const int &ts)->void;
        auto GetImageTimeStep()const->int;

        auto SetImageTimePoints(QList<double> tps)->void;
        auto GetImageTimePoints()->QList<double>;

        auto SetImageTimeSteps(const int& steps)->void;
        auto GetImageTimeSteps()const->int;

        auto SetProcessingAlgorithm(Algorithm::Pointer procAlgo)->void;
        auto GetProcessingAlgorithm()->Algorithm::Pointer;

        auto SetParameter(IParameter::Pointer param)->void;
        auto GetParameter()->IParameter::Pointer;

        auto SetMeasure(TCMeasure::Pointer measure)->void;
        auto GetMeasure()->TCMeasure::Pointer;
        auto GetMeasurePath()const->QString;
        auto ClearMeasure()->void;
        auto SetMeasureValid(bool valid)->void;
        auto GetMeausreValid()->bool;
        auto SetParamChanged(bool changed)->void;
        auto GetParamChanged()->bool;

        auto AddMask(TCMask::Pointer mask, const QString& key)->void;
        auto RemoveMask(const QString& key)->void;
        auto ClearMask()->void;
        auto GetMask(const QString& key)->TCMask::Pointer;
        auto GetMasks()->QMap<QString, TCMask::Pointer>;

        auto SetMaskPath(const QString& path)->void;
        auto ClearMaskPath()->void;
        auto GetMaskPath()const->QString;
        auto SetMaskValid(bool valid)->void;
        auto GetMaskValid()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
