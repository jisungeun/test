#pragma once

#include <memory>

#include "BasicAnalysisEntityExport.h"

namespace TomoAnalysis::BasicAnalysis::Entity {
	class BasicAnalysisEntity_API Algorithm {
	public :
		typedef Algorithm Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		Algorithm();
		Algorithm(const Algorithm& other);
		virtual ~Algorithm();

		auto Init()->void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}