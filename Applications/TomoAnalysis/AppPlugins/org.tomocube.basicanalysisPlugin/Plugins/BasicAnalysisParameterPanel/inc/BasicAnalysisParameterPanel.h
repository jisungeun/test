#pragma once

#include <memory>
#include <QWidget>

#include <IParameterPanel.h>
#include <IParameter.h>

#include <DataList.h>
#include <MaskRaw.h>

#include "BasicAnalysisParameterPanelExport.h"

class SoVolumeData;

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisParameterPanel_API ParameterPanel : public QWidget, public Interactor::IParameterPanel {
        Q_OBJECT
    public:
        typedef ParameterPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ParameterPanel(QWidget* parent = nullptr);
        ~ParameterPanel();
        auto Init(void)->bool;        
        auto Update(Entity::WorkingSet::Pointer workingset) -> bool override;
        auto GetParameter()->IParameter::Pointer;
        auto GetParameter(QString algo_name)->IParameter::Pointer;
        auto GetConnection()->QMap<QString, std::tuple<QString, QStringList>>&;

        auto SetParameter(IParameter::Pointer param,bool blockSig = true)->void;

        //Non-clean architecture
        auto SetResultToggle(bool)->void;
        auto SetNextHighlight(QString algo_name)->void;
        auto SetNoHighlight()->void;
        auto SetDefaultHighlight()->void;
        auto SetMeasureHighlight()->void;
        auto SetApplyHighlight()->void;        
        auto SetCurrentProcessor(QString procName)->bool;
        auto GetCurrentProcessor()->QString;
        auto SetDefaultProcessors(QStringList procPaths)->void;
        auto SetDefaultProcessors(QStringList procPaths, QStringList invalidKey)->void;
        auto GetResultToggle()->bool;

    signals:
        void sigProc(QString);
        void sigAlgoValue(double, QString);
        void sigAlgoExe(QString,QString);
        void sigMaskSelected(QString);
        void sigRefHigh();
        void sigMaskCorrection();

        void sigToggleResult(bool);
        void sigApplyParam(QString);        

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void OnCurrentProcChanged(int);
        void OnProcessorValueChanged(double, QString);
        void OnAlgorithmExecuted(QString,QString);
        void OnHighlightApply(bool);
        void OnRefreshHighlight();

        void OnApplyParameter(void);
        void OnMaskCorrection(void);
        void OnToggleResult(void);

    private:        
        auto DefaultSetting()->void;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}