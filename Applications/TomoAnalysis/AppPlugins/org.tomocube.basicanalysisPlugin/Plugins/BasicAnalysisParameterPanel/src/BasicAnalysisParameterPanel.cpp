#define LOGGER_TAG "[BasicAnalyzer]"
#include <TCLogger.h>

#include <QDirIterator>
#include <QProgressDialog>
#include <QElapsedTimer>
#include <QApplication>
#include <QLayout>
#include <QScrollArea>

#include <VolumeViz/nodes/SoVolumeData.h>

#include <IProcessor.h>
#include <IParameter.h>
#include <ISegmentationAlgorithm.h>
#include <IProcessingAlgorithm.h>
#include <ICustomAlgorithm.h>
#include <PluginRegistry.h>

#include <ParamControl.h>
#include <ProcControl.h>
#include <ParameterRegistry.h>

#include "ui_BasicAnalysisParameterPanel.h"
#include "BasicAnalysisParameterPanel.h"


namespace TomoAnalysis::BasicAnalysis::Plugins {
	struct ParameterPanel::Impl {
		Ui::BasicAnalysisParameterPanel* ui{ nullptr };
		TC::ProcControl* procControl;
		QList<QString> procPath;
		QList<QString> fullName;

		IParameter::Pointer cur_param{ nullptr };

		QString cur_imagePath;
		bool showResult{ false };
	};
	ParameterPanel::ParameterPanel(QWidget* parent) :QWidget(parent),
		d{ new Impl }{
		d->ui = new Ui::BasicAnalysisParameterPanel;
		d->ui->setupUi(this);
		Init();
	}
	ParameterPanel::~ParameterPanel() {
		delete d->ui;
	}

	bool ParameterPanel::eventFilter(QObject* watched, QEvent* event) {
		if (event->type() == QEvent::Resize) {
			auto height = this->height() - 80 - d->ui->procCombo->height() - d->ui->resultBtn->height() - d->ui->paramBtn->height();
			d->procControl->SetCompetableMinSize(height);
		}
		return QObject::eventFilter(watched, event);
	}

	auto ParameterPanel::Init()-> bool {
		installEventFilter(this);
		connect(d->ui->procCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnCurrentProcChanged(int)));

		d->procControl = new TC::ProcControl;
		d->ui->verticalLayout->setAlignment(Qt::AlignTop);

		d->ui->procLayout->setAlignment(Qt::AlignTop);
		d->ui->procLayout->addWidget(d->procControl);
		//d->ui->procLayout->setStretch(0, 0);
		//d->ui->procLayout->setStretch(1, 0);
		//d->ui->procLayout->setStretch(2, 1);				

		connect(d->procControl, SIGNAL(valueCall(double, QString)), this, SLOT(OnProcessorValueChanged(double, QString)));
		connect(d->procControl, SIGNAL(executeCall(QString, QString)), this, SLOT(OnAlgorithmExecuted(QString, QString)));
		connect(d->procControl, SIGNAL(highlightApply(bool)), this, SLOT(OnHighlightApply(bool)));
		connect(d->procControl, SIGNAL(refreshHighlight()), this, SLOT(OnRefreshHighlight()));

		connect(d->ui->resultBtn, SIGNAL(clicked()), this, SLOT(OnToggleResult()));
		connect(d->ui->correctionBtn, SIGNAL(clicked()), this, SLOT(OnMaskCorrection()));
		connect(d->ui->paramBtn, SIGNAL(clicked()), this, SLOT(OnApplyParameter()));


		d->procControl->SetHideExecute(true);
		d->procControl->hide();

		DefaultSetting();
		return true;
	}
	auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
		ParameterNode node;
		node.name = name;
		node.displayName = param->DisplayName(name);
		node.description = param->Description(name);
		node.unit = param->Unit(name);
		node.value = param->GetValue(name);
		node.defaultValue = param->GetDefault(name);
		auto range = param->GetRange(name);
		node.minValue = std::get<0>(range);
		node.maxValue = std::get<1>(range);

		return node;
	};
	auto ParameterPanel::SetCurrentProcessor(QString procName) -> bool {
		for (auto i = 0; i < d->ui->procCombo->count(); i++) {
			if (d->fullName[i].compare(procName) == 0) {
				d->ui->procCombo->setCurrentIndex(i);
				return true;
			}
		}
		return false;
	}
	auto ParameterPanel::GetCurrentProcessor() -> QString {
		return d->ui->procCombo->currentText();
	}

	auto ParameterPanel::GetConnection() -> QMap<QString, std::tuple<QString, QStringList>>& {
		return d->procControl->GetConnections();
	}

	auto ParameterPanel::SetDefaultProcessors(QStringList procPaths, QStringList invalidKey) -> void {
		blockSignals(true);
		d->procControl->ClearInterface();
		d->ui->procCombo->blockSignals(true);
		d->ui->procCombo->clear();
		d->procPath.clear();
		d->fullName.clear();
		for (auto p : procPaths) {
			//parse processor path
			QDir procDir(qApp->applicationDirPath() + p);
			auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
			for (auto f : files) {
				auto fullPath = qApp->applicationDirPath() + p + "/" + f;
				auto plugin = PluginRegistry::GetPlugin(fullPath, true);
				auto isValid = true;;
				for (auto ikey : invalidKey) {
					if (plugin->GetFullName().contains(ikey)) {
						isValid = false;
						break;
					}
				}
				if (isValid) {
					d->ui->procCombo->addItem(plugin->GetName());
					d->fullName.push_back(plugin->GetFullName());
					d->procPath.push_back(fullPath);
				}
			}
		}

		d->ui->procCombo->setCurrentIndex(0);
		this->OnCurrentProcChanged(0);

		d->ui->procCombo->blockSignals(false);
		blockSignals(false);
	}

	auto ParameterPanel::SetDefaultProcessors(QStringList procPaths) -> void {
		blockSignals(true);
		d->procControl->ClearInterface();
		d->ui->procCombo->blockSignals(true);
		d->ui->procCombo->clear();
		d->procPath.clear();
		d->fullName.clear();
		for (auto p : procPaths) {
			//parse processor path
			QDir procDir(qApp->applicationDirPath() + p);
			auto files = procDir.entryList({ "*.dll" }, QDir::Filter::Files, QDir::SortFlag::Name);
			for (auto f : files) {
				auto fullPath = qApp->applicationDirPath() + p + "/" + f;
				auto plugin = PluginRegistry::GetPlugin(fullPath, true);
				d->ui->procCombo->addItem(plugin->GetName());
				d->fullName.push_back(plugin->GetFullName());
				d->procPath.push_back(fullPath);
			}
		}

		d->ui->procCombo->setCurrentIndex(0);
		this->OnCurrentProcChanged(0);

		d->ui->procCombo->blockSignals(false);
		blockSignals(false);
    }

	auto ParameterPanel::SetParameter(IParameter::Pointer param,bool blockSig) -> void {
	    d->procControl->SetParameterValue(param,QString(),blockSig);
		d->cur_param = param;
	}

	auto ParameterPanel::SetNoHighlight() -> void {
		d->ui->paramBtn->setStyleSheet("");
		d->procControl->ClearHighlights();
		
	}

	void ParameterPanel::OnHighlightApply(bool hi) {
		if (hi) {
			d->ui->paramBtn->setStyleSheet("background-color: rgb(91,139,151);");
		}else {
			d->ui->paramBtn->setStyleSheet("");
		}
    }

	void ParameterPanel::OnRefreshHighlight() {
		emit sigRefHigh();
	}

	auto ParameterPanel::SetDefaultHighlight() -> void {
		d->procControl->SetDefaultHighlight();
	}

	auto ParameterPanel::SetMeasureHighlight() -> void {		
		auto param = d->procControl->GetParameter();		
		if (nullptr != param) {
			for (auto name : param->GetNames()) {
				if (!name.contains("Mask Selector")) {
					auto fullname = param->GetValue(name).toString();					
					if (fullname.contains("measurement")) {
						d->procControl->SetProcHighlight(true, fullname);
					}
				}
			}
		}
	}

	auto ParameterPanel::SetApplyHighlight() -> void {
		d->ui->paramBtn->setStyleSheet("background-color: rgb(91,139,151);");
	}


	auto ParameterPanel::Update(Entity::WorkingSet::Pointer workingset) -> bool {
		auto path = workingset->GetImagePath();
		if(path.compare(d->cur_imagePath) !=0 && !d->cur_imagePath.isEmpty()) {
			QLOG_INFO() << "Image Changed Force Default Setting";
		    DefaultSetting();						
		}		
		d->cur_imagePath = path;
		return true;
	}


	auto ParameterPanel::DefaultSetting() -> void {
		blockSignals(true);
		d->procControl->ClearInterface();		
	    if (d->ui->procCombo->count() > 0) {
			d->ui->procCombo->setCurrentIndex(0);
			d->procControl->show();
			this->OnCurrentProcChanged(0);
		}
		else {
			d->procControl->hide();
		}
		d->ui->procCombo->blockSignals(false);
		blockSignals(false);
	}
	//slots

	void ParameterPanel::OnApplyParameter() {		
		auto cur_idx = d->ui->procCombo->currentIndex();
		emit sigApplyParam(d->fullName[cur_idx]);
	}

	void ParameterPanel::OnMaskCorrection() {
		emit sigMaskCorrection();
	}

	void ParameterPanel::OnToggleResult() {		
		d->showResult = !d->showResult;
		emit sigToggleResult(d->showResult);

		if (d->showResult) {
			d->ui->resultBtn->setText("Hide Result");
		}
		else {
			d->ui->resultBtn->setText("Show Result");
		}
	}

	void ParameterPanel::OnCurrentProcChanged(int idx) {
		//change processor
		auto proc_name = d->ui->procCombo->currentText();
		auto selectedProcessor = d->fullName[idx];
		QLOG_INFO() << "Selected Processor: " << selectedProcessor;
		IPluginModule::Pointer processor = PluginRegistry::GetPlugin(selectedProcessor);
		if (nullptr == processor) {
			if (PluginRegistry::LoadPlugin(d->procPath[idx]) ==-1) {
				auto appWrite = d->procPath[idx].split("/processor/")[0];
				auto appRemain = d->procPath[idx].split("/processor/")[1];

				auto realPath = qApp->applicationDirPath() + "/processor/" + appRemain;
				if (PluginRegistry::LoadPlugin(realPath) == -1) {
					QLOG_DEBUG() << "Failed to load processor dll";
					return;
				}				
			}
			processor = PluginRegistry::GetPlugin(selectedProcessor);
		}
		auto param = processor->Parameter();
		for (auto nnt : param->GetNameAndTypes()) {
			auto name = std::get<0>(nnt);
			auto desp = param->Description(name);
			auto algoName = param->GetValue(name).toString();
			auto algoPath = qApp->applicationDirPath() + "/" + desp;

			auto algorithm = PluginRegistry::GetPlugin(algoName);
			if (nullptr == algorithm) {
				PluginRegistry::LoadPlugin(algoPath);
			}
		}

		auto procModule = std::dynamic_pointer_cast<IProcessor>(processor);
		auto metaParam = procModule->MetaParameter();

		QStringList realname_list{"membrane","nucleus","nucleoli","lipid droplet"};
		QStringList displayname_list{"Whole cell","Nucleus","Nucleolus","Lipid droplet"};		
		
		d->procControl->SetConverter(displayname_list,realname_list);
		d->procControl->SetProcessingParameter(param);
		d->procControl->SetMetaParameter(metaParam);
		d->procControl->BuildInterface();
		d->procControl->show();
		
		emit sigProc(proc_name);
	}
	void ParameterPanel::OnProcessorValueChanged(double val, QString sender) {
		emit sigAlgoValue(val, sender);
	}
	void ParameterPanel::OnAlgorithmExecuted(QString param_name,QString algo_name) {
		emit sigAlgoExe(param_name,algo_name);		
	}
	auto ParameterPanel::SetNextHighlight(QString algo_name) -> void {
		d->procControl->SetNextHighlight(algo_name);
    }	
	auto ParameterPanel::GetParameter() -> IParameter::Pointer {
		return d->procControl->GetParameter();
	}
	auto ParameterPanel::GetParameter(QString algo_name) -> IParameter::Pointer {
		auto param = d->procControl->GetParameter(algo_name);

		return param;
	}
	auto ParameterPanel::GetResultToggle() -> bool {
		return d->showResult;
    }
	auto ParameterPanel::SetResultToggle(bool show) -> void {
		d->showResult = show;
		if (show) {
			d->ui->resultBtn->setText("Hide Result");
		}
		else {
			d->ui->resultBtn->setText("Show Result");
		}
	}

}