#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "BasicAnalysisMetaExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
	class BasicAnalysisMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.basicanalysisMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "Basic Analyzer"; }
		auto GetFullName()const->QString override { return "org.tomocube.basicanalysisPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}