#include "BasicAnalysisMeta.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "Project Manager";
		d->appProperties["AppKey"] = "Basic Analyzer";
		QStringList processor_path;
		processor_path.push_back("/processor/basicanalysis");
		d->appProperties["Processors"] = processor_path;
		d->appProperties["hasSingleRun"] = true;
		d->appProperties["hasBatchRun"] = true;
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}