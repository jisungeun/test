#include <iostream>

#include <QtConcurrent/QtConcurrent>

#include <UIUtility.h>

#include "ui_BasicAnalysisPlayerPanel.h"
#include "BasicAnalysisPlayerPanel.h"

namespace  TomoAnalysis::BasicAnalysis::Plugins {
    struct PlayerPanel::Impl {
        Ui::BasicAnalysisPlayerPanel* ui{ nullptr };        

        bool isProgress = false;
        int range = 0;
        int interval = 0;
        
        QFuture<void> future;
        QFutureWatcher<void> watcher;
    };

    PlayerPanel::PlayerPanel(QWidget* parent)
    : QWidget(parent)    
    , d{ new Impl } {
        d->ui = new Ui::BasicAnalysisPlayerPanel();
        d->ui->setupUi(this);
        

        Init();
    }

    PlayerPanel::~PlayerPanel() {
        delete d->ui;
    }

    auto PlayerPanel::Update(Entity::WorkingSet::Pointer workingset) -> bool {
        //change time steps
        auto steps = workingset->GetImageTimeSteps();
        auto isEnabled = false;
        if (1 < steps) {
            isEnabled = true;
            d->range = steps;
        }
        //no play function for segmentation
        TC::SilentCall(d->ui->playButton)->setEnabled(false);
        //TC::SilentCall(d->ui->progressSlider)->setEnabled(isEnabled);
        TC::SilentCall(d->ui->progressSlider)->setEnabled(false);
        //TC::SilentCall(d->ui->progressSpinBox)->setEnabled(isEnabled);
        TC::SilentCall(d->ui->progressSpinBox)->setEnabled(false);

        //TC::SilentCall(d->ui->progressSlider)->setRange(0, d->range - 1);
        TC::SilentCall(d->ui->progressSlider)->setRange(1, d->range);
        //TC::SilentCall(d->ui->progressSpinBox)->setRange(0, d->range - 1);
        TC::SilentCall(d->ui->progressSpinBox)->setRange(1, d->range);

        return true;
    }


    auto PlayerPanel::isEnabled() -> bool {
        auto enable = d->ui->progressSlider->isEnabled();
        return enable;
    }

    auto PlayerPanel::Init(void) const ->bool {
        const auto isEnabled = false;

        TC::SilentCall(d->ui->playButton)->setEnabled(isEnabled);
        TC::SilentCall(d->ui->progressSlider)->setEnabled(isEnabled);
        TC::SilentCall(d->ui->progressSpinBox)->setEnabled(isEnabled);                

        return true;
    }

    auto PlayerPanel::Reset(void) const ->bool {
        d->isProgress = false;
        d->range = 0;
        d->interval = 0;

        //d->timelapseInfoList = nullptr;

        return true;
    }

    auto PlayerPanel::IsProgress(void) const ->bool {
        return d->isProgress;
    }

    auto PlayerPanel::SetProgress(const bool& isProgress) ->void {
        d->isProgress = isProgress;
        TC::SilentCall(d->ui->playButton)->setText("Play");
    }

    auto PlayerPanel::SetSliceIndex(const int& index) const ->void {
        TC::SilentCall(d->ui->progressSpinBox)->setValue(index);
        TC::SilentCall(d->ui->progressSlider)->setValue(index);
    }

    void PlayerPanel::Progress(void* instance, const int& start, const int& end, const int& interval) {
        if(nullptr == instance) {
            return;
        }

        auto panel = static_cast<PlayerPanel*>(instance);
        for(auto i = start; i < end; ++i) {
            if (false == panel->IsProgress()) {
                return;
            }

            panel->SetSliceIndex(i);
            //emit panel->timelapseIndexChanged(i);
            emit panel->timelapseIndexChanged(i-1);

            QThread::sleep(interval);
        }

        // 완료 후 다시 처음으로
        //panel->SetSliceIndex(0);
        panel->SetSliceIndex(1);
        panel->SetProgress(false);
        emit panel->timelapseIndexChanged(0);
    }

    void PlayerPanel::on_playButton_pressed() {
        if(true == d->isProgress) {
            d->isProgress = false;
            TC::SilentCall(d->ui->playButton)->setText("Play");

            return;
        }

        d->isProgress = true;
        TC::SilentCall(d->ui->playButton)->setText("Stop");

        auto currentIndex = d->ui->progressSpinBox->value();

        d->future = QtConcurrent::run([=] { return Progress(this, currentIndex, d->range, 1); });
        d->watcher.setFuture(d->future);
    }

    void PlayerPanel::on_progressSlider_valueChanged(int value) {
        if (true == d->isProgress) {
            d->isProgress = false;
            TC::SilentCall(d->ui->playButton)->setText("Play");
        }

        TC::SilentCall(d->ui->progressSpinBox)->setValue(value);        
        emit timelapseIndexChanged(value-1);
    }

    void PlayerPanel::on_progressSpinBox_valueChanged(int value) {
        if (true == d->isProgress) {
            d->isProgress = false;
            TC::SilentCall(d->ui->playButton)->setText("Play");
        }

        TC::SilentCall(d->ui->progressSlider)->setValue(value);

        emit timelapseIndexChanged(value-1);
    }    
    
}
