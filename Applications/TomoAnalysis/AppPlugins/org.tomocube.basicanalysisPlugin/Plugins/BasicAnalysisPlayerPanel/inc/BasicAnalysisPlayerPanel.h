#pragma once

#include <memory>
#include <QWidget>

#include <IPlayerPanel.h>

#include "BasicAnalysisPlayerPanelExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisPlayerPanel_API PlayerPanel : public QWidget, public Interactor::IPlayerPanel {
        Q_OBJECT
    public:
        typedef PlayerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        PlayerPanel(QWidget* parent=nullptr);
        ~PlayerPanel();

        auto Update(Entity::WorkingSet::Pointer workingset) -> bool override;        

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;
        
        auto IsProgress(void) const ->bool;
        auto SetProgress(const bool& isProgress) ->void;
        auto SetSliceIndex(const int& index) const ->void;

        auto isEnabled(void)->bool;

        static void Progress(void* instance, const int& start, const int& end, const int& interval);

        

    signals:
        void timelapseIndexChanged(int index);

    protected slots:
        void on_playButton_pressed();

        void on_progressSlider_valueChanged(int value);
        void on_progressSpinBox_valueChanged(int value);

        //void onTimelapseChanged(Entity::Modality modality);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}