#pragma once

#include <QOivRenderWindow.h>

#include "BaRenderWindow3DExport.h"

class SoEventCallback;

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BaRenderWindow3D_API BaRenderWindow3D : public QOivRenderWindow {
        Q_OBJECT
    public:
        BaRenderWindow3D(QWidget* parent);
        ~BaRenderWindow3D();

        auto closeEvent(QCloseEvent* unused) -> void override;;
        auto requestUpdate()->void;
        auto reset3DView()->void;
        auto setViewDirection(int axis)->void;

    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;
        auto ChangeBackGround(int type)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}