#pragma once

#include <memory>

#include <IProcessingEngine.h>

#include "BasicAnalysisProcessorExport.h"

class DataList;

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisProcessor_API Processor : public UseCase::IProcessingEngine {
    public:
        typedef Processor Self;
        typedef std::shared_ptr<Self> Pointer;

        static Pointer GetInstance();

        Processor();
        ~Processor();

        auto SetReferenceImage(IBaseImage::Pointer image) -> void override;        
                
        auto ImageToSingleMask(const QString& module_name, IParameter::Pointer param) -> std::tuple<IBaseMask::Pointer, IBaseMask::Pointer> override;
        auto ImageToBoth(const QString& module_name, IParameter::Pointer param) -> std::tuple<IBaseMask::Pointer, IBaseMask::Pointer> override;
        auto BinaryMaskToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param) -> IBaseMask::Pointer override;
        auto Measurement(const QString& module_name, IBaseMask::Pointer organ_mask, IBaseMask::Pointer label_mask, IParameter::Pointer param) -> IBaseData::Pointer override;
        auto ImageToValue(const QString& module_name, IParameter::Pointer param) -> DataList::Pointer override;
        auto LabelToLabel(const QString& module_name, IBaseMask::Pointer mask, IParameter::Pointer param) -> IBaseMask::Pointer override;

        //Interactivity
        auto SetParameterValue(IParameter::Pointer param)->void;

    private:        
        auto MaskOnlyMeasure(const QString& module_name,IBaseMask::Pointer mask, IParameter::Pointer param)->IBaseData::Pointer;
        auto BothMeasure(const QString& module_name,IBaseMask::Pointer mask, IBaseMask::Pointer label, IParameter::Pointer param)->IBaseData::Pointer;        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
