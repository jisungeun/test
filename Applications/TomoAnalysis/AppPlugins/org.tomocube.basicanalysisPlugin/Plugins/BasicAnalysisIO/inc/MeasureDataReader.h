#pragma once

#include <IMeasureReaderPort.h>

#include "BasicAnalysisIOExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisIO_API MeasureDataReader : public UseCase::IMeasureReaderPort {
    public:
        MeasureDataReader();
        ~MeasureDataReader();

        auto Read(const QString& path,int time_step)->TCMeasure::Pointer override;
    };
}