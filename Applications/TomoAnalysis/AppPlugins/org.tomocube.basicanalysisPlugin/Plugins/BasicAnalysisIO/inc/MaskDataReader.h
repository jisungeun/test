#pragma once

#include <IMaskReaderPort.h>

#include "BasicAnalysisIOExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins{
    class BasicAnalysisIO_API MaskDataReader : public UseCase::IMaskReaderPort {
    public:
        MaskDataReader();
        ~MaskDataReader();

        auto Read(const QString& path,int time_idx, const bool loadMaskVolume,const bool isLabel) const -> IBaseMask::Pointer override;
        auto Create(int x0, int y0, int z0, int x1, int y1, int z1,int code) -> IBaseMask::Pointer override;
        
    private:
        auto ReadOld(const QString& path, int time_idx, const bool loadMaskVolume, const bool isLabel) const->IBaseMask::Pointer;
    };
}