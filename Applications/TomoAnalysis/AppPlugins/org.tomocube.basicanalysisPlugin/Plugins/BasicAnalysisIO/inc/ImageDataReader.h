#pragma once

#include <IImageReaderPort.h>

#include "BasicAnalysisIOExport.h"

class SoVolumeData;

namespace TomoAnalysis::BasicAnalysis::Plugins{
    class BasicAnalysisIO_API ImageDataReader : public UseCase::IImageReaderPort {
    public:
        ImageDataReader();
        ~ImageDataReader();

        auto Read(const QString& path, const bool loadImage = false,int time_step=0) const -> TCImage::Pointer override;
        auto GetTimeSteps(const QString& path) const -> int override;
        auto ReadOiv(const QString& path)const->SoVolumeData*;

    private:

    };
}