#pragma once

#include <IMaskWriterPort.h>

#include "BasicAnalysisIOExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins{
    class BasicAnalysisIO_API MaskDataWriter : public UseCase::IMaskWriterPort {
    public:
        MaskDataWriter();
        ~MaskDataWriter();

        auto SetLayerNames(QStringList name) -> void override;
        auto Write(IBaseMask::Pointer data, const QString& path,int time_step=0) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}