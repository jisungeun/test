#pragma once

#include <IMeasureWriterPort.h>

#include "BasicAnalysisIOExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisIO_API MeasureDataWriter : public UseCase::IMeasureWriterPort {
    public:
        MeasureDataWriter();
        ~MeasureDataWriter();

        auto Write(TCMeasure::Pointer data, const QString& path) -> bool override;
        auto Modify(TCMeasure::Pointer data, const QString& path)->bool override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}