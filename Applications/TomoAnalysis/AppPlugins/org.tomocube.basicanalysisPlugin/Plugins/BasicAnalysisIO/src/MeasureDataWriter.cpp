#include <iostream>
#include <QList>

#include <TCMeasureWriter.h>

#include "MeasureDataWriter.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
    struct MeasureDataWriter::Impl {
        
    };
    MeasureDataWriter::MeasureDataWriter() : d{ new Impl } {
        
    }
    MeasureDataWriter::~MeasureDataWriter() {
        
    }
    auto MeasureDataWriter::Write(TCMeasure::Pointer data, const QString& path) -> bool {        
        auto count = data->GetCellCount();
        auto organ_names = data->GetOrganNames();
        auto keys = data->GetKeys();
        auto time_idx = data->GetTimeIndex();    
        auto time_point = data->GetTimePoint();
        TC::IO::TCMeasureWriter writer(path, count, keys.size());
        auto measureText = data->GetKeys(0);        
        //write key for measure names
        writer.WriteMeasureNames(keys);         
        for(auto i=0;i<organ_names.size();i++) {
            writer.ClearMeausreCount(organ_names[i], time_idx);
        }
        for(auto i=0;i<count;i++) {//cell idx
            for(auto j=0;j<organ_names.size();j++) {
                auto organ = organ_names[j];
                QList<double> vals;
                for(auto k=0;k<keys.size();k++) {                    
                    auto key = keys[k];                    
                    auto final_key = organ + "." + key;                    
                    auto value = data->GetMeasure(final_key, i);
                    vals.push_back(value);
                }
                writer.WriteMeasures(organ, (i + 1), time_idx,time_point, vals);
            }            
        }        
        return true;
    }
    auto MeasureDataWriter::Modify(TCMeasure::Pointer data, const QString& path) -> bool {
        auto count = data->GetCellCount();
        auto organ_names = data->GetOrganNames();
        auto keys = data->GetKeys();
        auto time_idx = data->GetTimeIndex();
        auto time_point = data->GetTimePoint();
        TC::IO::TCMeasureWriter writer(path, count, keys.size());
        auto measureText = data->GetKeys(0);
                
        //writer.WriteMeasureNames(keys);//no need to modify meta info
        for(auto i=0;i<organ_names.size();i++) {
            writer.ClearMeausreCount(organ_names[i],time_idx);
        }
        for(auto i=0;i<count;i++) {            
            for(auto j=0;j<organ_names.size();j++) {
                auto organ = organ_names[j];
                QList<double> vals;
                for(auto k=0;k<keys.size();k++) {
                    auto key = keys[k];
                    auto final_key = organ + "." + key;
                    auto value = data->GetMeasure(final_key, i);
                    vals.push_back(value);
                }
                //writer.WriteMeasures(organ, (i + 1), time_idx, vals);
                writer.UpdateMeasures(organ, (i + 1), time_idx,time_point, vals);
            }
        }
        return true;
    }

}