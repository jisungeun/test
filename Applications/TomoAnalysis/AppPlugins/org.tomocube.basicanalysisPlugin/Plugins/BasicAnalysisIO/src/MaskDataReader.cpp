#include <TCMaskReader.h>
#include <TCHMaskReader.h>
#include <TCMaskReaderPort.h>

#include <TCMask.h>
#include <TCDataConverter.h>

#include "MaskDataReader.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
	MaskDataReader::MaskDataReader() {

	}
	MaskDataReader::~MaskDataReader() {

	}
	auto MaskDataReader::ReadOld(const QString& path, int time_idx, const bool loadMaskVolume, const bool isLabel) const -> IBaseMask::Pointer {
		auto reader = TC::IO::TCMaskReader(path);
		if (!reader.Exist()) return nullptr;

		TCMask::Pointer maskData(new TCMask());
		maskData->SetTimeStep(time_idx);
		if (isLabel) {
			maskData->SetType(MaskTypeEnum::MultiLabel);
		}
		else {
			maskData->SetType(MaskTypeEnum::MultiLayer);
		}
		const auto blobs = reader.GetBlobCount("HT", time_idx);
		if (blobs < 1) {
			return nullptr;
		}
		auto nameList = reader.GetNameList("HT");
		for (int idx = 0; idx < blobs; idx++) {
			auto data = reader.ReadBlob("HT", time_idx, idx);
			auto index = std::get<0>(data);
			auto bbox = std::get<1>(data);
			auto code = std::get<2>(data);
			auto offset = bbox.GetOffset();
			auto size = bbox.GetSize();
			MaskBlob blob;
			blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
			blob.SetCode(code);
			if (isLabel) {
				maskData->AppendBlob(index, blob);
			}
			else {
				maskData->AppendLayer(nameList[index], blob, index);
			}

			if (!loadMaskVolume) continue;

			auto maskVolume = reader.ReadMask("HT", time_idx, index);
			if (isLabel) {
				maskData->AppendMaskVolume(index, maskVolume);
			}
			else {
				maskData->AppendLayerVolume(nameList[index], maskVolume, index);
			}
		}
		const auto size = reader.GetMaskSize();
		const auto res = reader.GetMaskResolution();
		double ress[3] = { std::get<0>(res),std::get<1>(res),std::get<2>(res) };

		maskData->SetSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
		maskData->SetResolution(ress);
		maskData->SetPath(path);

		return maskData;
	}

	auto MaskDataReader::Read(const QString& path, int time_idx, const bool loadMaskVolume, const bool isLabel) const -> IBaseMask::Pointer {
		auto reader = TC::IO::TCMaskReaderPort();
		auto maskList = reader.GetMaskList(path, "HT");
		if (maskList.isEmpty()) {
			return nullptr;
		}
		QString instName;
		QStringList organNames;
		for (auto i = 0; i < maskList.count(); i++) {
			if (maskList[i].type == MaskTypeEnum::MultiLayer) {
				organNames.append(maskList[i].name);
			}
			else {
				instName = maskList[i].name;
			}
		}
		if (isLabel) {
			return reader.Read(path, "HT", instName, time_idx);
		}
		else {
			TCMask::Pointer maskData(new TCMask());
			maskData->SetTimeStep(time_idx);
			maskData->SetType(MaskTypeEnum::MultiLayer);
			double res[3];
			int size[3];
			for (auto i = 0; i < organNames.count(); i++) {
				auto layerMask = reader.Read(path, "HT", organNames[i], time_idx);				
				if (nullptr == layerMask) {
					return nullptr;
				}
				if (i == 0) {
					res[0] = std::get<0>(layerMask->GetResolution());
					res[1] = std::get<1>(layerMask->GetResolution());
					res[2] = std::get<2>(layerMask->GetResolution());
					size[0] = std::get<0>(layerMask->GetSize());
					size[1] = std::get<1>(layerMask->GetSize());
					size[2] = std::get<2>(layerMask->GetSize());
				}
				auto blob = layerMask->GetBlob(0);
				maskData->AppendLayer(organNames[i], blob, i);
				auto bb = blob.GetBoundingBox();
				auto xSize = std::get<3>(bb) - std::get<0>(bb) + 1;
				auto ySize = std::get<4>(bb) - std::get<1>(bb) + 1;
				auto zSize = std::get<5>(bb) - std::get<2>(bb) + 1;
			}			
			maskData->SetResolution(res);
			maskData->SetSize(size[0], size[1], size[2]);
			return maskData;
		}	
	}
	auto MaskDataReader::Create(int x0, int y0, int z0, int x1, int y1, int z1, int code) -> IBaseMask::Pointer {
		TCMask::Pointer maskData(new TCMask());
		maskData->SetTimeStep(0);

		MaskBlob blob;
		blob.SetBoundingBox(x0, y0, z0, x1, y1, z1);
		blob.SetCode(code);

		maskData->AppendBlob(0, blob);
		auto sizeX = x1 - x0 + 1;
		auto sizeY = y1 - y0 + 1;
		auto sizeZ = z1 - z0 + 1;

		//add empty volume
		auto maskVolume = new unsigned char[sizeX * sizeY * sizeZ];
		for (auto i = 0; i < sizeX * sizeY * sizeZ; i++) {
			maskVolume[i] = 0;
		}
		const std::shared_ptr<unsigned char> sp(maskVolume);
		maskData->AppendMaskVolume(0, sp);

		return maskData;
	}

}
