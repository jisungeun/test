#pragma once

#include <memory> 

#include <QWidget>
#include <QLayout>
//#include <Inventor/nodes/SoNode.h>

#include <TCMask.h>

#include <ScreenShotWidget.h>
#include <IRenderWindowPanel.h>

#include "BasicAnalysisViewerPanelExport.h"

class SoVolumeData;

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisViewerPanel_API ViewerPanel : public QWidget, public Interactor::IRenderWindowPanel {
        Q_OBJECT
    public:
        typedef ViewerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ViewerPanel(QWidget* parent = nullptr);
        ~ViewerPanel();

        auto Update(IBaseImage::Pointer image,IBaseMask::Pointer mask,const QString& organ_name) -> bool override;
        auto UpdateMasks(const QStringList& organ_names, IBaseMask::Pointer instMask, IBaseMask::Pointer organMask) -> bool override;
        auto UpdateVisibility(int blob_index, bool visibility) -> bool override;

        auto Update(SoVolumeData* mask)->bool;
        auto ClearMask(bool clearMask = true)->void;
        auto SetCellFilter(bool cellwise)->void;
        auto SetMaskOpacity(double opac)->void;
        auto ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget)->void;

    signals:
        void sliceIndexChanged(int, int);
    protected slots:
        void onMoveSlice(float value);

    private:        
        auto MaskToOivVolume(TCMask::Pointer mask,int time_idx,const QString& organ_name)const->SoVolumeData*;
        auto ImageToOivVolume(IBaseImage::Pointer image,int time_idx)const->SoVolumeData*;        

        auto Switch2DRenderWindow()const->void;

        auto Init()->void;
        auto InitDataGroups()->void;
        auto Init3dScene()->void;
        auto Init2dScene()->void;
        auto InitConnections()->void;
        auto UpdateSingle()->void;
        auto UpdateMulti()->void;
        auto MakeCellWiseOrgan()->bool;
        auto UpdateImage()->void;

        auto Reset()->void;
        auto ResetMask()->void;        
        auto ResetImageAdapters(bool reset_inst,bool reset_organ)->void;        
        auto ResetFilters(bool inst,bool organ)->void;

        auto GetRealMaskName(QString name)->QString;
        auto CreateLabelTextMap()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
