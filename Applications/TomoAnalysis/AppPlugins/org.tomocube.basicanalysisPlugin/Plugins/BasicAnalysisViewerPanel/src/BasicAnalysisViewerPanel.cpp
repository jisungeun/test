//qt
#include <QApplication>
#include <QGridLayout>
#include <QSplitter>

#pragma warning(push)
#pragma warning(disable:4819)
//inventor
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/helpers/Medicalhelper.h>
#include <Medical/nodes/Gnomon.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <Inventor/Axis.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoDrawStyle.h>
#pragma warning(pop)

#include <TCImage.h>
#include <OivScaleBar.h>
#include <ScaleBarSwitch.h>
#include <ScalarBarSwitch.h>

//container - deprecated
//#include "OivAxisMarker.h"

#include <OivMaskGenerator.h>

#include <TCDataConverter.h>

#include <BaRenderWindow2D.h>
#include <BaRenderWindow3D.h>

#include "BasicAnalysisViewerPanel.h"


namespace TomoAnalysis::BasicAnalysis::Plugins {
#define SAFE_DELETE( p ) { if( p ) { delete ( p ); ( p ) = NULL; } }

	MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
	float color_table[20][3]{
	230,25,75, //red
	60,180,75, //green
	255,255,25, //yellow
	0,130,200, //blue
	245,130,48, //Orange
	145,30,180, //Purple
	70,240,240, //Cyan
	240,50,230, //Magenta
	210,245,60, //Lime
	250,190,212, //Pink
	0,128,128, //Teal
	220,190,255, //Lavender
	170,110,40, //Brown
	255,250,200, //Beige
	128,0,0,//Maroon
	170,255,195, //Mint
	128,128,0, //Olive
	255,215,180, //Apricot
	0,0,128, //Navy
	128,128,128 //Gray
	};
	struct ViewerPanel::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		BaRenderWindow2D* qOiv2DRenderWindow[3] = { nullptr, };
		BaRenderWindow3D* qOiv3DRenderWindow = nullptr;

		QWidget* win2dContainer[3]{ nullptr };
		QWidget* win2dSocket{ nullptr };
		int cur2dWin;

		QSplitter* mainSplit;

		//Scene Graph Data
		SoVolumeData* curImage{ nullptr };
		SoVolumeData* curInstMask{ nullptr };
		SoVolumeData* curOrganMask{ nullptr };
		SoVolumeData* curInstOrgan{ nullptr };
		SoVolumeData* curMultiMask{ nullptr };
		OivMaskGenerator* maskGenerator{ nullptr };

		SoMemoryDataAdapter* instAdapter{ nullptr };
		SoMemoryDataAdapter* organAdapter{ nullptr };
		//SoVRImageDataReader* reader{nullptr};

		SoGroup* imageGroup;
		SoSwitch* imageSocket;
		SoDataRange* dataRange;
		SoMaterial* matImage;
		SoTransferFunction* tfImage;

		SoGroup* maskGroup;
		SoSwitch* maskSocket;
		SoDataRange* dataRangeMask;
		SoMaterial* matlMaskVolume{nullptr};
		SoMaterial* matlMaskSlice{ nullptr };
		SoTransferFunction* tfMask;		

		//Scene Graph 2D
		SoSeparator* root2d[3] = { nullptr, };
		SoOrthographicCamera* camera2d[3] = { nullptr, };
		SoSeparator* orthoSep[3] = { nullptr, };
		SoSeparator* orthoSepMask[3] = { nullptr, };
		SoOrthoSlice* orthoSlice[3] = { nullptr, };
		SoOrthoSlice* orthoSliceMask[3] = { nullptr, };

		SoSwitch* scaleBarSwitch[3] = { nullptr, };		
		TC::ScaleBarSwitch* scaleBar[3] = { nullptr, };

		SoSwitch* scalarBarSwitch[3] = { nullptr, };
		TC::ScalarBarSwitch* scalarBar[3] = { nullptr, };

		//SceneGraph 3D
		SoSeparator* root3d{ nullptr };
		SoPerspectiveCamera* camera3d{ nullptr };
		SoSeparator* volRenSep{ nullptr };
		SoSeparator* volRenSepMask{ nullptr };		
		SoVolumeRenderingQuality* volQual{ nullptr };
		SoVolumeRenderingQuality* volQualMask{ nullptr };
		SoVolumeRender* volRender{ nullptr };
		SoVolumeRender* volRenderMask{ nullptr };

		//For label Text
		SoSwitch* labelTextSwitch{ nullptr };

		//data share
		SoSwitch* inheritor{nullptr};

		SoFragmentShader* maskFrag{ nullptr };

		bool maskFirstLoad{ true };

		//scene info
		int curSlice[3] = { 0, };
		bool instFilter{ true };
		bool isInst{ false };
		double opacity{ 1.0 };
	};

	ViewerPanel::ViewerPanel(QWidget* parent) : QWidget(parent), d(new Impl) {
		Init();
		InitDataGroups();
		Init3dScene();
		Init2dScene();
		InitConnections();
	}
	ViewerPanel::~ViewerPanel() {
		delete d->qOiv3DRenderWindow;
		for (auto i = 0; i < 3; i++) {
			delete d->qOiv2DRenderWindow[i];
		}
	}
	auto ViewerPanel::Init()->void {		
		d->qOiv3DRenderWindow = new BaRenderWindow3D(nullptr);
		for (auto i = 0; i < 3; i++) {
			d->qOiv2DRenderWindow[i] = new BaRenderWindow2D(nullptr);
			d->qOiv2DRenderWindow[i]->setRenderWindowID(i);
			d->win2dContainer[i] = new QWidget(nullptr);
			auto lay = new QVBoxLayout;
			lay->addWidget(d->qOiv2DRenderWindow[i]);
			d->win2dContainer[i]->setLayout(lay);
		}
		d->win2dSocket = new QWidget(nullptr);
		auto slay = new QVBoxLayout;
		d->cur2dWin = 0;
		//slay->addWidget(d->win2dContainer[0]);
		slay->addWidget(d->qOiv3DRenderWindow);
		d->win2dSocket->setLayout(slay);

		auto layout = new QHBoxLayout(nullptr);
		d->mainSplit = new QSplitter;
		d->mainSplit->setOrientation(Qt::Orientation::Horizontal);

		//d->mainSplit->addWidget(d->qOiv3DRenderWindow);
		d->mainSplit->addWidget(d->win2dContainer[0]);
		d->mainSplit->addWidget(d->win2dSocket);
		QList<int> mainSizes;
		mainSizes << 400 << 400;
		d->mainSplit->setSizes(mainSizes);

		layout->addWidget(d->mainSplit);

		this->setLayout(layout);

		d->maskGenerator = new OivMaskGenerator;

		d->qOiv2DRenderWindow[0]->setWindowTitle("XY");
		d->qOiv2DRenderWindow[0]->setWindowTitleColor(0.8f, 0.2f, 0.2f);
		d->qOiv2DRenderWindow[1]->setWindowTitle("YZ");
		d->qOiv2DRenderWindow[1]->setWindowTitleColor(0.2f, 0.8f, 0.2f);
		d->qOiv2DRenderWindow[2]->setWindowTitle("XZ");
		d->qOiv2DRenderWindow[2]->setWindowTitleColor(0.2f, 0.2f, 0.8f);
		d->qOiv3DRenderWindow->setWindowTitle("3D");
		d->qOiv3DRenderWindow->setWindowTitleColor(0.8f, 0.8f, 0.2f);
	}
	auto ViewerPanel::ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void {
		widget->SetRenderWindow(d->qOiv3DRenderWindow, "threeD");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[0], "XY");

		widget->SetMultiLayerType(TC::MultiLayoutType::HXY3D);
    }
	auto ViewerPanel::InitDataGroups()->void {
		//shared components for image data visualization
		d->imageGroup = new SoGroup;		
		d->imageSocket = new SoSwitch;
		d->imageSocket->whichChild = -1;
		d->dataRange = new SoDataRange;
		d->dataRange->setName("HTSliceRange");		
		d->tfImage = new SoTransferFunction;
		d->tfImage->setName("colormapHTSlice");
		d->tfImage->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;		

		d->imageGroup->addChild(d->imageSocket);
		d->imageGroup->addChild(d->tfImage);
		d->imageGroup->addChild(d->dataRange);		

		//shared components for mask data visualization
		d->maskGroup = new SoGroup;		
		d->maskSocket = new SoSwitch;
		d->maskSocket->whichChild = -1;
		d->dataRangeMask = new SoDataRange;
		d->dataRangeMask->setName("MaskSliceRange");		
		d->tfMask = new SoTransferFunction;
		d->tfMask->setName("colormapMask");

		d->maskGroup->addChild(d->maskSocket);
		d->maskGroup->addChild(d->tfMask);
		d->maskGroup->addChild(d->dataRangeMask);
	}
	auto ViewerPanel::Init3dScene() -> void {
		d->root3d = new SoSeparator;
		d->matlMaskVolume = new SoMaterial;
		d->matlMaskVolume->ambientColor.setValue(1, 1, 1);
		d->matlMaskVolume->diffuseColor.setValue(1, 1, 1);
		d->matlMaskVolume->transparency.setValue(0.0);
		//d->volRenSep = new SoSeparator;
		d->volRenSepMask = new SoSeparator;

		d->root3d->addChild(d->volRenSepMask);

		//scene graph for image volume render
		//d->volRenSep->addChild(d->imageGroup);
		//d->volQual = new SoVolumeRenderingQuality;
		//d->volQual->interpolateOnMove = TRUE;
		//d->volQual->preIntegrated = FALSE;
		//d->volQual->ambientOcclusion = FALSE;
		//d->volQual->deferredLighting = FALSE;
		//d->volRenSep->addChild(d->volQual);

		//d->volRender = new SoVolumeRender;
		//d->volRenSep->addChild(d->volRender);				

		d->maskFrag = new SoFragmentShader;
		const auto shaderPath = QString("%1/shader/SurfaceRender.glsl").arg(qApp->applicationDirPath());
		d->maskFrag->sourceProgram.setValue(shaderPath.toStdString());
		d->maskFrag->addShaderParameter1i("transfer", 0);
		d->maskFrag->addShaderParameter1i("data1", 1);

		d->volRenSepMask->addChild(d->matlMaskVolume);
		d->volRenSepMask->addChild(d->maskGroup);
		d->volQualMask = new SoVolumeRenderingQuality;
		d->volQualMask->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->maskFrag);
		d->volQualMask->interpolateOnMove = TRUE;
		d->volQualMask->preIntegrated = TRUE;
		d->volQualMask->lighting = TRUE;
		d->volQualMask->ambientOcclusion = FALSE;
		d->volQualMask->deferredLighting = FALSE;
		d->volRenSepMask->addChild(d->volQualMask);

		d->volRenderMask = new SoVolumeRender;
		d->volRenderMask->interpolation = SoVolumeRender::NEAREST;
		d->volRenderMask->samplingAlignment = SoVolumeRender::SamplingAlignment::VIEW_ALIGNED;
		d->volRenSepMask->addChild(d->volRenderMask);

		auto cube = new SoViewingCube;
		cube->position = SoViewingCube::PositionInViewport::BOTTOM_LEFT;
		cube->sceneCamera = d->qOiv3DRenderWindow->getCamera();
		cube->upAxis = openinventor::inventor::Axis::Z;
		cube->facePosX = (qApp->applicationDirPath()+"/img/ViewFront.png").toStdString();
		cube->faceNegX = (qApp->applicationDirPath()+"/img/ViewBack.png").toStdString();
		cube->facePosY = (qApp->applicationDirPath() + "/img/ViewRight.png").toStdString();
		cube->faceNegY = (qApp->applicationDirPath()+"/img/ViewLeft.png").toStdString();
		cube->facePosZ = (qApp->applicationDirPath()+"/img/ViewTop.png").toStdString();
		cube->faceNegZ = (qApp->applicationDirPath()+"/img/ViewBottom.png").toStdString();		
		//d->root3d->addChild(new OivAxisMarker);
		d->root3d->addChild(cube);

		d->labelTextSwitch = new SoSwitch;
		d->labelTextSwitch->addChild(new SoSeparator);
		d->labelTextSwitch->whichChild = -1;
		d->root3d->addChild(d->labelTextSwitch);

		d->qOiv3DRenderWindow->setSceneGraph(d->root3d);
	}
	auto ViewerPanel::Init2dScene() -> void {				

		d->matlMaskSlice = new SoMaterial;		
		d->matlMaskSlice->ambientColor.setValue(1, 1, 1);
		d->matlMaskSlice->diffuseColor.setValue(1, 1, 1);
		
		for (auto i = 0; i < 3; i++) {
			d->root2d[i] = new SoSeparator;

			d->orthoSep[i] = new SoSeparator;
			d->orthoSepMask[i] = new SoSeparator;
			d->orthoSepMask[i]->setName("MaskSeparator");
			auto trans = new SoTransform;
			if (i == 0) {
				trans->translation.setValue(0, 0, -0.1f);
			}else if(i==1) {
				trans->translation.setValue(0, -0.1f, 0);
			}else if(i==2) {
				trans->translation.setValue(-0.1f, 0, 0);
			}
			d->orthoSepMask[i]->addChild(trans);
			d->root2d[i]->addChild(d->orthoSep[i]);
			d->root2d[i]->addChild(d->orthoSepMask[i]);			

			d->orthoSep[i]->addChild(d->imageGroup);			
			d->orthoSepMask[i]->addChild(d->maskGroup);

			d->orthoSlice[i] = new SoOrthoSlice;
			d->orthoSlice[i]->interpolation = SoSlice::NEAREST;
			d->orthoSlice[i]->alphaUse = SoSlice::ALPHA_AS_IS;
			d->orthoSep[i]->addChild(d->orthoSlice[i]);
						
			d->orthoSepMask[i]->addChild(d->matlMaskSlice);

			d->orthoSliceMask[i] = new SoOrthoSlice;
			d->orthoSliceMask[i]->interpolation = SoSlice::NEAREST;
			d->orthoSliceMask[i]->alphaUse = SoSlice::ALPHA_AS_IS;
			d->orthoSepMask[i]->addChild(d->orthoSliceMask[i]);

			if (i == 0) {
				d->orthoSlice[i]->axis = MedicalHelper::AXIAL;
				d->orthoSliceMask[i]->axis = MedicalHelper::AXIAL;
			}
			else if (i == 1) {
				d->orthoSlice[i]->axis = MedicalHelper::CORONAL;
				d->orthoSliceMask[i]->axis = MedicalHelper::CORONAL;
			}
			else {
				d->orthoSlice[i]->axis = MedicalHelper::SAGITTAL;
				d->orthoSliceMask[i]->axis = MedicalHelper::SAGITTAL;
			}

			d->qOiv2DRenderWindow[i]->setSceneGraph(d->root2d[i]);

			if(nullptr ==d->scaleBarSwitch[i]) {
				d->scaleBarSwitch[i] = new SoSwitch;
			}else {
				d->scaleBarSwitch[i]->removeAllChildren();
			}
			d->scaleBarSwitch[i]->whichChild = -3;
						
			d->scaleBar[i] = new TC::ScaleBarSwitch(d->qOiv2DRenderWindow[i]->getCamera());
			d->scaleBar[i]->SetPosition(0.95f, -0.95f);
			d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);

			d->scaleBarSwitch[i]->addChild(d->scaleBar[i]->GetRoot());
			if (d->root2d[i]->findChild(d->scaleBarSwitch[i]) < 0) {
				d->root2d[i]->addChild(d->scaleBarSwitch[i]);
			}

			if (nullptr == d->scalarBarSwitch[i]) {
				d->scalarBarSwitch[i] = new SoSwitch;
				d->scalarBarSwitch[i]->setName("ScalarBarSwitch");
			}
			else {
				d->scalarBarSwitch[i]->removeAllChildren();
			}
			d->scalarBarSwitch[i]->whichChild = -1;			
			
			d->scalarBar[i] = new TC::ScalarBarSwitch;
			auto tcFunc = new SoTransferFunction;
			tcFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			d->scalarBar[i]->SetTransferFunction(tcFunc);
			d->scalarBar[i]->SetMajorLength(4);
			d->scalarBar[i]->SetNumberOfAnnotation(3);
			d->scalarBar[i]->SetRatio(12);
			d->scalarBar[i]->SetHorizontal(true);
			d->scalarBar[i]->SetPosition(-0.95f, -0.95f);
			d->scalarBar[i]->SetSize(0.3f, 0.02f);
			d->scalarBar[i]->SetFontSize(13.0f);
			d->scalarBar[i]->SetLastAnnotationMargin(340);
			//d->scalarBar[i]->SetAnnotationDiv(10000.0);
			d->scalarBar[i]->GetRoot()->whichChild = 0;

			d->scalarBarSwitch[i]->addChild(d->scalarBar[i]->GetRoot());
			if (d->root2d[i]->findChild(d->scalarBarSwitch[i]) < 0) {
				d->root2d[i]->addChild(d->scalarBarSwitch[i]);
			}
		}
	}
	auto ViewerPanel::InitConnections() -> void {
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigWheel(float)), this, SLOT(onMoveSlice(float)));
	}

	auto ViewerPanel::Switch2DRenderWindow()const -> void {
		d->cur2dWin++;
		d->cur2dWin %= 3;
		auto lay = d->win2dSocket->layout();
		lay->removeItem(lay->takeAt(0));
		lay->addWidget(d->win2dContainer[d->cur2dWin]);
	}
	auto ViewerPanel::Update(SoVolumeData* mask) -> bool {
		d->curInstMask = mask;
		ResetMask();
		UpdateSingle();

		return true;
	}	
	auto ViewerPanel::ResetImageAdapters(bool reset_inst, bool reset_organ) -> void {
		Q_UNUSED(reset_inst)
		Q_UNUSED(reset_organ)
		return;
	}
	auto ViewerPanel::ResetFilters(bool inst, bool organ) -> void {
		Q_UNUSED(inst)
		Q_UNUSED(organ)
	}
	auto ViewerPanel::SetMaskOpacity(double opac) -> void {
		d->opacity = opac;
		d->matlMaskSlice->transparency.setValue(1.0 - d->opacity);
		if(d->instFilter) {
			d->matlMaskVolume->transparency.setValue(1.0 - d->opacity);
		}else {
		    if(d->opacity == 0.0) {
				d->matlMaskVolume->transparency.setValue(1);
		    }else {
				d->matlMaskVolume->transparency.setValue(0.9f);
		    }
		}
	}
	auto ViewerPanel::SetCellFilter(bool cellwise)->void {
		d->instFilter = cellwise;
		if (d->instFilter) {
			if (nullptr != d->instAdapter && !d->isInst) {
				if (MakeCellWiseOrgan()) {
					ClearMask(false);
					UpdateSingle();
				}
			}
			d->matlMaskVolume->transparency.setValue(1.0-d->opacity);
		}
		else {
			if (nullptr != d->curOrganMask) {
				ClearMask();				
			}
			if(d->opacity ==0.0) {
				d->matlMaskVolume->transparency.setValue(1);
			}else {
				d->matlMaskVolume->transparency.setValue(0.9f);
			}
			
		}
	}
	auto ViewerPanel::UpdateMasks(const QStringList& organ_names, IBaseMask::Pointer instMask, IBaseMask::Pointer organMask) -> bool {
		if(nullptr == d->curImage) {
			return false;
		}
		ClearMask();
		d->maskGenerator->Clear(true);

		//append masks;
	    for(auto o:organ_names) {			
			auto real_name = GetRealMaskName(o);			
			auto converter = new TCDataConverter;
	        if(real_name.compare("cellInst")==0) {				
				auto tcinst = std::dynamic_pointer_cast<TCMask>(instMask);
				auto m = converter->MaskToSoVolumeData(tcinst, real_name);
				d->maskGenerator->AppendMask(real_name, m);
	        }else {				
				auto tcorgan = std::dynamic_pointer_cast<TCMask>(organMask);
				auto m = converter->MaskToSoVolumeData(tcorgan, real_name);
				d->maskGenerator->AppendMask(real_name, m);
	        }
	    }	    
		d->maskGenerator->SetMaskOrder();
		d->curMultiMask = d->maskGenerator->GetCurrentResult();		
		UpdateMulti();

		//d->qOiv3DRenderWindow->reset3DView();

		return true;
	}
	auto ViewerPanel::Update(IBaseImage::Pointer image, IBaseMask::Pointer mask, const QString& organ_name) -> bool {
		if (nullptr == image) {			
			if (nullptr == mask) {
				return false;
			}			
			ClearMask();
			auto tcoivmask = std::dynamic_pointer_cast<TCMask>(mask);
			auto realName = GetRealMaskName(organ_name);
			auto converter = std::make_shared<TCDataConverter>();
			auto oivVol = converter->MaskToSoVolumeData(tcoivmask, realName);
			if (tcoivmask->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {//instance mask
				d->curInstMask = oivVol;
				d->curInstMask->dataSetId = 1;
				if (nullptr != d->instAdapter) {
					while (d->instAdapter->getRefCount() > 0) {
						d->instAdapter->unref();
					}
				}
				////MedicalHelper::dicomAdjustVolume(d->curInstMask);
				d->instAdapter = MedicalHelper::getImageDataAdapter(d->curInstMask);
				d->instAdapter->ref();
				d->instAdapter->interpretation = SoMemoryDataAdapter::VALUE;
				d->isInst = true;
			}
			else if (tcoivmask->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {//organ mask					
				d->curOrganMask = oivVol;
				d->curOrganMask->dataSetId = 1;
				d->curOrganMask->ref();
				d->isInst = false;
				if (d->instFilter) {
					MakeCellWiseOrgan();
				}
			}
			if (nullptr == d->curOrganMask && nullptr == d->curInstMask) {
				return false;
			}
			UpdateSingle();
			if (d->maskFirstLoad) {
				d->qOiv3DRenderWindow->reset3DView();
				d->maskFirstLoad = false;
			}
		}
		else {
			Reset();
			d->curImage = ImageToOivVolume(image, 0);
			d->curImage->setName("volData");
			d->curImage->dataSetId = 1;
			UpdateImage();
			d->maskFirstLoad = true;
			if (nullptr != mask) {
				auto tcoivmask = std::dynamic_pointer_cast<TCMask>(mask);
				auto realName = GetRealMaskName(organ_name);
				auto converter = std::make_shared<TCDataConverter>();
				auto oivVol = converter->MaskToSoVolumeData(tcoivmask, realName);
				if (tcoivmask->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {//instance mask
					d->curInstMask = oivVol;
					d->curInstMask->dataSetId = 2;
					if (nullptr != d->instAdapter) {
						while (d->instAdapter->getRefCount() > 0) {
							d->instAdapter->unref();
						}
					}
					////MedicalHelper::dicomAdjustVolume(d->curInstMask);
					d->instAdapter = MedicalHelper::getImageDataAdapter(d->curInstMask);
					d->instAdapter->ref();
					d->instAdapter->interpretation = SoMemoryDataAdapter::VALUE;
					d->isInst = true;
				}
				else if (tcoivmask->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {//organ mask					
					d->curOrganMask = oivVol;
					d->curOrganMask->dataSetId = 2;
					d->curOrganMask->ref();
					d->isInst = false;
					if (d->instFilter) {
						MakeCellWiseOrgan();
					}
				}
				if (nullptr == d->curOrganMask && nullptr == d->curInstMask) {
					return false;
				}
				UpdateSingle();
				d->qOiv3DRenderWindow->reset3DView();
			}
			//double min, max;
			//d->curImage->getMinMax(min, max);
			auto tcimage = std::dynamic_pointer_cast<TCImage>(image);
			auto imin = std::get<0>(tcimage->GetMinMax());
			auto imax = std::get<1>(tcimage->GetMinMax());
			d->dataRange->min = imin;
			d->dataRange->max = imax;
			d->qOiv2DRenderWindow[0]->setHTRange(imin, imax);
		}
		return true;
	}
	auto ViewerPanel::UpdateImage() -> void {
		d->imageSocket->addChild(d->curImage);
		d->imageSocket->whichChild = 0;
		//MedicalHelper::dicomAdjustDataRange(d->dataRange, d->curImage);		

		MedicalHelper::dicomCheckMonochrome1(d->tfImage, d->curImage);

		d->orthoSlice[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		d->orthoSlice[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
		d->orthoSlice[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;
		d->orthoSliceMask[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		d->orthoSliceMask[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
		d->orthoSliceMask[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;

		double min, max;
		d->curImage->getMinMax(min, max);
		for (auto i = 0; i < 3; i++) {
			d->scalarBar[i]->SetImageRange(static_cast<float>(min)/10000.0, static_cast<float>(max)/10000.0);
		}

		auto cam1 = d->qOiv2DRenderWindow[0]->getCamera();
		MedicalHelper::orientView(MedicalHelper::AXIAL, cam1, d->curImage);

		auto cam2 = d->qOiv2DRenderWindow[1]->getCamera();
		MedicalHelper::orientView(MedicalHelper::CORONAL, cam2, d->curImage);

		auto cam3 = d->qOiv2DRenderWindow[2]->getCamera();
		MedicalHelper::orientView(MedicalHelper::SAGITTAL, cam3, d->curImage);		
	}
	auto ViewerPanel::ClearMask(bool clearMask) -> void {
		d->maskSocket->removeChild(0);
		d->maskSocket->whichChild = -1;
		d->labelTextSwitch->replaceChild(0, new SoSeparator);
		d->labelTextSwitch->whichChild = -1;
		if (clearMask) {
			if (nullptr != d->curOrganMask) {
				while (d->curOrganMask->getRefCount() > 0) {
					d->curOrganMask->unref();
				}
				d->curOrganMask = nullptr;
			}

			if (nullptr != d->curInstMask) {
				while (d->curInstMask->getRefCount() > 0) {
					d->curInstMask->unref();
				}
				d->curInstMask = nullptr;
			}

			if (nullptr != d->curInstOrgan) {
				while (d->curInstOrgan->getRefCount() > 0) {
					d->curInstOrgan->unref();
				}
				d->curInstOrgan = nullptr;
			}
			if(nullptr != d->curMultiMask) {
			    while(d->curMultiMask->getRefCount()>0) {
					d->curMultiMask->unref();
			    }
				d->curMultiMask = nullptr;
			}
		}
	}
	auto ViewerPanel::MakeCellWiseOrgan() -> bool {
		if (nullptr == d->instAdapter) {
			return false;
		}
		if (nullptr == d->curOrganMask) {
			return false;
		}

		if (nullptr != d->curInstOrgan) {			
			while (d->curInstOrgan->getRefCount() > 0) {
				d->curInstOrgan->unref();
			}
			d->curInstOrgan = nullptr;
		}

		if (nullptr != d->organAdapter) {			
			while (d->organAdapter->getRefCount() > 0) {
				d->organAdapter->unref();
			}
			d->organAdapter = nullptr;
		}
		d->organAdapter = MedicalHelper::getImageDataAdapter(d->curOrganMask);
		d->organAdapter->ref();
		d->organAdapter->interpretation = SoMemoryDataAdapter::BINARY;

		SoRef<SoMaskImageProcessing> maskFilter = new SoMaskImageProcessing;
		maskFilter->inBinaryImage = d->organAdapter;
		maskFilter->inImage = d->instAdapter;

		auto reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&maskFilter->outImage);		
				
		d->curInstOrgan = new SoVolumeData;
		d->curInstOrgan->ref();		
		d->curInstOrgan->setReader(*reader, TRUE);
		d->curInstOrgan->dataSetId = 1;

		return true;
	}
	auto ViewerPanel::UpdateMulti() -> void {		
		d->maskSocket->addChild(d->curMultiMask);
		d->maskSocket->whichChild = 0;

		double min, max;
		d->curMultiMask->getMinMax(min, max);

		MedicalHelper::dicomAdjustDataRange(d->dataRangeMask, d->curMultiMask);
		auto rangemin = d->dataRangeMask->min.getValue();
		d->dataRangeMask->min.setValue(rangemin - 0.1);
		MedicalHelper::orientView(MedicalHelper::AXIAL, d->camera3d, d->curMultiMask);

		d->curMultiMask->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		d->curMultiMask->ldmResourceParameters.getValue()->resolution = 0;

		d->tfMask->colorMap.setNum(256 * 4);

		float* p = d->tfMask->colorMap.startEditing();
		for (int i = 0; i < 256; ++i) {
			int idx = (float)i / 255.0 * max;
			if (idx < 1) {
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
			}
			else {
				float r = color_table[(idx - 1) % 20][0] / 255.0;
				float g = color_table[(idx - 1) % 20][1] / 255.0;
				float b = color_table[(idx - 1) % 20][2] / 255.0;
				*p++ = r;
				*p++ = g;
				*p++ = b;
				*p++ = 0.5;
			}
		}
		d->tfMask->colorMap.finishEditing();		

		onMoveSlice(0);
    }

	auto ViewerPanel::UpdateSingle() -> void {

		SoVolumeData* curmask;
		if (d->isInst) {
			curmask = d->curInstMask;
		}
		else {
			if (d->instFilter) {
				curmask = d->curInstOrgan;
			}
			else {
				curmask = d->curOrganMask;
			}
		}
		d->maskSocket->addChild(curmask);
		d->maskSocket->whichChild = 0;

		double min, max;
		curmask->getMinMax(min, max);

		//Create mask label if it is inst-wise separated mask
		if(d->isInst || d->instFilter) {
			CreateLabelTextMap();
		}

		MedicalHelper::dicomAdjustDataRange(d->dataRangeMask, curmask);
		auto rangemin = d->dataRangeMask->min.getValue();
		d->dataRangeMask->min.setValue(rangemin - 0.1);
		MedicalHelper::orientView(MedicalHelper::AXIAL, d->camera3d, curmask);

		curmask->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		curmask->ldmResourceParameters.getValue()->resolution = 0;

		d->tfMask->colorMap.setNum(256 * 4);

		float* p = d->tfMask->colorMap.startEditing();
		for (int i = 0; i < 256; ++i) {			
			int idx = (float)i / 255.0 * max;
			if (idx < 1) {
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
			}
			else {
				float r = color_table[(idx - 1) % 20][0] / 255.0;
				float g = color_table[(idx - 1) % 20][1] / 255.0;
				float b = color_table[(idx - 1) % 20][2] / 255.0;
				*p++ = r;
				*p++ = g;
				*p++ = b;
				*p++ = 0.5;
			}
		}
		d->tfMask->colorMap.finishEditing();
		
		onMoveSlice(0);
	}
	auto ViewerPanel::CreateLabelTextMap() -> void {
		auto labelRoot = new SoSeparator;
		d->labelTextSwitch->replaceChild(0, labelRoot);
		d->labelTextSwitch->whichChild = 0;

		auto font = new SoFont;
		font->size = 30;
		font->name = "NOTO SANS KR : BOLD";
		labelRoot->addChild(font);

		auto mask = static_cast<SoVolumeData*>(d->maskSocket->getChild(0));
		SoRef<SoLabelAnalysisQuantification> analysis = new SoLabelAnalysisQuantification;
		analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OX));
		analysis->measureList.set1Value(1, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OY));
		analysis->measureList.set1Value(2, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OZ));		
		analysis->measureList.set1Value(3, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_DY));
		analysis->measureList.set1Value(4, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_DZ));
		////MedicalHelper::dicomAdjustVolume(mask);
		SoRef<SoMemoryDataAdapter> labelAdapter = MedicalHelper::getImageDataAdapter(mask);
		labelAdapter->interpretation = SoImageDataAdapter::Interpretation::LABEL;
		analysis->inLabelImage = labelAdapter.ptr();	
		analysis->inIntensityImage = labelAdapter.ptr();

		auto analysisOutput = analysis->outAnalysis.getValue();
		auto numLabel = analysisOutput->getNumLabels();

		for(auto label = 0;label<numLabel;label++) {
			//retrive label position
			auto x = analysisOutput->getValueAsDouble(label, 0, 0);
			auto y = analysisOutput->getValueAsDouble(label, 1, 0);
			auto z = analysisOutput->getValueAsDouble(label, 2, 0);			
			auto ySize = analysisOutput->getValueAsDouble(label, 3, 0);
			auto zSize = analysisOutput->getValueAsDouble(label, 4, 0);

			auto textSep = new SoSeparator;

			auto textMatl = new SoMaterial;
			auto color_idx = label % 20;
			textMatl->diffuseColor.setValue(color_table[color_idx][0] / 255.0, color_table[color_idx][1] / 255.0, color_table[color_idx][2] / 255.0);

			textSep->addChild(textMatl);

			auto trans = new SoTranslation;
			trans->translation.setValue(x, y + ySize, z + zSize);

			textSep->addChild(trans);

			auto text = new SoText2;
			text->string = std::to_string(label + 1);

			textSep->addChild(text);

			labelRoot->addChild(textSep);
		}
    }

	auto ViewerPanel::UpdateVisibility(int blob_index, bool visibility) -> bool {
		Q_UNUSED(blob_index)
		Q_UNUSED(visibility)
		return true;
	}

	auto ViewerPanel::ImageToOivVolume(IBaseImage::Pointer image, int time_idx)const -> SoVolumeData* {
		Q_UNUSED(time_idx)
		auto tcimage = std::dynamic_pointer_cast<TCImage>(image);
		auto conv = std::make_shared<TCDataConverter>();		
	    return conv->ImageToSoVolumeData(tcimage);
	}

	auto ViewerPanel::Reset() -> void {
		d->imageSocket->removeChild(0);
		d->imageSocket->whichChild = -1;
		for (auto i = 0; i < 3; i++) d->curSlice[i] = 0;


		if (nullptr != d->curImage) {			
			while (d->curImage->getRefCount() > 0) {
				d->curImage->unref();
			}
			d->curImage = nullptr;
		}

		ClearMask();
	}
	auto ViewerPanel::ResetMask()->void {
		d->maskSocket->removeAllChildren();
		d->maskSocket->whichChild = -1;
		d->labelTextSwitch->replaceChild(0, new SoSeparator);
		d->labelTextSwitch->whichChild = -1;
	}		

	auto ViewerPanel::GetRealMaskName(QString name) -> QString {
		auto real_key = name;
		if (real_key == "Lipid droplet") {
			real_key = "lipid droplet";
		}
		else if (real_key == "Whole Cell") {
			real_key = "membrane";
		}
		else if (real_key == "Nucleus") {
			real_key = "nucleus";
		}
		else if (real_key == "Nucleolus") {
			real_key = "nucleoli";
		}
		else if (real_key == "Cell Instance") {
			real_key = "cellInst";
		}
		return real_key;
	}

	auto ViewerPanel::MaskToOivVolume(TCMask::Pointer mask, int time_idx, const QString& organ_name)const -> SoVolumeData* {
		Q_UNUSED(time_idx)
		double res[3];
		int dim[3];
		auto size = mask->GetSize();

		dim[0] = std::get<0>(size);
		dim[1] = std::get<1>(size);
		dim[2] = std::get<2>(size);

		auto resolusion = mask->GetResolution();

		res[0] = std::get<0>(resolusion);
		res[1] = std::get<1>(resolusion);
		res[2] = std::get<2>(resolusion);


		auto converter = std::make_shared<TCDataConverter>();
		const auto resultVolume = converter->MaskToSoVolumeData(mask, organ_name);
		resultVolume->ref();

		return resultVolume;
	}
	void ViewerPanel::onMoveSlice(float value) {

		//auto slice0 = MedicalHelper::find<SoOrthoSlice>(d->root2d[0], "HT_slice0_0");		
		auto slice0 = d->orthoSlice[0];
		auto maskSlice0 = d->orthoSliceMask[0];
		int currentSlice = slice0->sliceNumber.getValue();
		auto volData = d->curImage;
		auto slicenum = volData->data.getSize()[ax[0]];
		if (0 < value) {
			currentSlice++;
		}
		else if (0 > value) {
			currentSlice--;
		}
		if (slicenum > currentSlice && currentSlice > -1) {
			if (nullptr != d->curImage) {
				slice0->sliceNumber.setValue(currentSlice);
				d->curSlice[ax[0]] = currentSlice;
				if (nullptr != d->curInstMask || nullptr != d->curOrganMask || nullptr != d->curInstOrgan || nullptr != d->curMultiMask) {
					maskSlice0->sliceNumber.setValue(currentSlice);
				}
			}
			//buffer 4D에 대한 처리는 나중에
		}
		//emit sliceIndexChanged(0, currentSlice);		
	}
}
