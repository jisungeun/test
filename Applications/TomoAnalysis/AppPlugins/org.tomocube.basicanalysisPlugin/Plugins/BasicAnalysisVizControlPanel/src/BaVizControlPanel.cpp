#include <iostream>

#include <TCMask.h>
#include <UIUtility.h>

#include "ui_BaVizControlPanel.h"
#include "BaVizControlPanel.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {    
    struct VizControlPanel::Impl {
        Ui::BaVizControlPanel* ui{ nullptr };
        QHBoxLayout* hbox;
        QMap<QString, QRadioButton*> radios;
        QMap<QString, QCheckBox*> checks;
        QCheckBox* allCheck{ nullptr };
        bool isMultiLayer{ false };
        double cur_opacity{ 1.0 };

        QStringList maskOrder;
    };
    VizControlPanel::VizControlPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::BaVizControlPanel();
        d->ui->setupUi(this);

        Init();
    }

    VizControlPanel::~VizControlPanel() {
        
    }

    auto VizControlPanel::Clear() -> void {
        ClearInterface();
    }

    auto VizControlPanel::ToDisplayName(const QString& name) -> QString {
        if(name.compare("cellInst")==0) {
            return QString("Cell Instance");
        }
        if(name.compare("membrane")==0) {
            return QString("Whole Cell");
        }
        if(name.compare("nucleus")==0){
            return QString("Nucleus");
        }
        if(name.compare("nucleoli")==0) {
            return QString("Nucleolus");
        }
        if(name.compare("lipid droplet")==0) {
            return QString("Lipid droplet");
        }
        return name;
    }

    auto VizControlPanel::ToRealName(const QString& name) -> QString {
        if(name.compare("Cell Instance")==0) {
            return QString("cellInst");
        }
        if(name.compare("Whole Cell")==0) {
            return QString("membrane");
        }
        if(name.compare("Nucleus")==0) {
            return QString("nucleus");
        }
        if(name.compare("Nucleolus")==0) {
            return QString("nucleoli");
        }
        if(name.compare("Lipid droplet")==0) {
            return QString("lipid droplet");
        }
        return name;
    }

    auto VizControlPanel::Update(Entity::WorkingSet::Pointer workingset) -> bool {
        ClearInterface();
        QStringList mask_names;
        QVector<int> order_key;
        for(auto m : workingset->GetMasks()) {
            auto mask = std::dynamic_pointer_cast<TCMask>(m);
            for(auto name : mask->GetLayerNames()) {
                mask_names.push_back(name);
                if(name.compare("cellInst")==0) {
                    order_key.push_back(0);
                }else if(name.compare("membrane")==0) {
                    order_key.push_back(1);
                }else if(name.compare("nucleus")==0) {
                    order_key.push_back(2);
                }else if(name.compare("nucleoli")==0) {
                    order_key.push_back(3);
                }else if(name.compare("lipid droplet")==0) {
                    order_key.push_back(4);
                }
            }
        }
        QStringList ordered_mask;
        for(auto k = 0;k<5;k++) {
            for(auto i=0;i<mask_names.size();i++) {
                if(order_key[i] == k) {
                    ordered_mask.push_back(mask_names[i]);                    
                    break;
                }
            }            
        }
        d->maskOrder = ordered_mask;
        CreateRadio(ordered_mask);
        CreateCB(ordered_mask);
        d->ui->cfLabel->show();
        d->ui->opaLabel->show();
        d->ui->maskOpacity->show();
        d->ui->vizToggle->show();
        d->ui->cellFilter->show();
        d->ui->labelFilter->show();
        return true;
    }

    auto VizControlPanel::Update(IParameter::Pointer param) -> bool {
        ClearInterface();        
        auto names = param->GetNames();
        QStringList dups;
        for(auto n: names) {            
           if(param->Description(n).contains("Duplicate")) {
               dups = param->GetValue(n).toString().split("!");
               break;
           }
        }
        if (dups.size() < 1)
            return false;

        QStringList mask_names;
        for(auto du: dups) {
            if (du.contains("*")) {
                mask_names.push_back(du.split("*")[0]);
            }else {
                mask_names.push_back(du);
            }
        }

        CreateRadio(mask_names);
        CreateCB(mask_names);
        d->ui->cfLabel->show();
        d->ui->opaLabel->show();
        d->ui->maskOpacity->show();
        d->ui->cellFilter->show();
        d->ui->labelFilter->show();
        d->ui->vizToggle->show();
        return true;
    }
    void VizControlPanel::OnLabelFilterChk(bool state) {        
        QStringList checkedMasks;
        if(state) {
            checkedMasks.push_back("Cell Instance");
        }
        for (auto cb : d->checks) {
            if (cb->isChecked()) {
                checkedMasks.push_back(cb->text());
            }
        }
        emit sigMultiMask(checkedMasks);
    }

    void VizControlPanel::OnCellFilterChk(bool state) {        
        d->isMultiLayer = state;
        if(!state) {
            for(auto rad: d->radios) {
                rad->show();
            }
            for(auto chk : d->checks) {
                chk->hide();
            }
            d->allCheck->hide();
            d->ui->labelFilter->setEnabled(false);
        }else {
            for(auto chk : d->checks) {
                chk->show();
            }
            for(auto rad : d->radios) {
                rad->hide();
            }
            d->allCheck->show();
            d->ui->labelFilter->setEnabled(true);
        }
        emit sigCellFilter(state);

        if(state) {
            QStringList checkedMasks;
            if(d->ui->labelFilter->isChecked()) {
                checkedMasks.push_back("Cell Instance");
            }
            for (auto cb : d->checks) {
                if (cb->isChecked()) {
                    checkedMasks.push_back(cb->text());
                }
            }
            emit sigMultiMask(checkedMasks);
        }else {
            QString key;
            for (auto cb : d->radios) {
                if (cb->isChecked()) {
                    key = cb->text();
                    break;
                }
            }
            emit sigMask(key, true);            
        }
    }
    void VizControlPanel::OnMaskViz() {
        if(d->ui->vizToggle->isChecked()) {
            //hide visualization
            emit sigMaskOpa(0.0);
        }else {
            //restore visualization
            emit sigMaskOpa(d->cur_opacity);
        }
    }

    void VizControlPanel::OnMaskOpacity(double opa) {
        d->cur_opacity = opa;
        emit sigMaskOpa(opa);
    }
    auto VizControlPanel::CreateRadio(QStringList names) -> void {        
        for(auto n : names) {
            if(n == "cellInst") {
                continue;
            }
            auto rad = new QRadioButton(ToDisplayName(n));
            if (d->radios.size() < 1)
                rad->setChecked(true);
            connect(rad, SIGNAL(clicked()), this, SLOT(OnRadioChanged()));            
            d->radios[n] = rad;
            d->hbox->addWidget(rad);
            if(d->isMultiLayer) {
                rad->hide();
            }
        }
    }
    auto VizControlPanel::CreateCB(QStringList names) -> void {
        if (nullptr == d->allCheck) {
            d->allCheck = new QCheckBox("ALL");
            connect(d->allCheck, SIGNAL(clicked()), this, SLOT(OnMaskAll()));
        }        
        d->hbox->addWidget(d->allCheck);
        if(!d->isMultiLayer) {
            d->allCheck->hide();
        }
        bool isFirst = true;
        for(auto i=0;i<names.count();i++) {
            auto n = names[i];
            if (n == "cellInst") {
                continue;
            }
            auto cb = new QCheckBox(ToDisplayName(n));
            if (isFirst) {
                cb->setChecked(true);
                isFirst = false;
            }
            connect(cb, SIGNAL(clicked()), this, SLOT(OnCheckChanged()));
            d->checks[n] = cb;
            d->hbox->addWidget(cb);
            if(!d->isMultiLayer) {
                cb->hide();
            }
        }
    }
    void VizControlPanel::OnMaskAll() {
        auto isCheck = d->allCheck->isChecked();
        if (isCheck) {
            for (auto cb : d->checks) {
                TC::SilentCall(cb)->setChecked(true);
            }
        }else {           
            for(auto cb:d->checks) {
                TC::SilentCall(cb)->setChecked(false);
            }            
            //maskOrder 0 is always cell instance
            TC::SilentCall(d->checks[d->maskOrder[1]])->setChecked(true);
        }
        OnCheckChanged();
    }
    void VizControlPanel::OnCheckChanged() {
        QStringList checkedMasks;
        auto pChk = qobject_cast<QCheckBox*>(sender());
        if(pChk) {
            if(d->ui->labelFilter->isChecked()) {
                checkedMasks.push_back("Cell Instance");
            }
            auto count = 0;
            auto hasFalse = false;
            for(auto cb: d->checks) {
                if(cb->isChecked()) {
                    checkedMasks.push_back(cb->text());
                    count++;
                }else {
                    hasFalse = true;
                }
            }
            if(hasFalse) {
                TC::SilentCall(d->allCheck)->setChecked(false);
            }else {
                TC::SilentCall(d->allCheck)->setChecked(true);
            }
            if (count > 0) {
                emit sigMultiMask(checkedMasks);
            }else {
                pChk->blockSignals(true);//force at least on element checked
                pChk->setChecked(true);
                pChk->blockSignals(false);
            }
        }
    }

    void VizControlPanel::OnRadioChanged() {
        auto pChk = qobject_cast<QRadioButton*>(sender());
        if(pChk) {            
            auto chkText = ToRealName(pChk->text());
            auto isChecked = d->radios[chkText]->isChecked();
            for(auto cb : d->radios) {
                if(cb->text().compare(chkText)!=0) {
                    cb->blockSignals(true);
                    cb->setChecked(false);
                    cb->blockSignals(false);
                }
            }
            emit sigMask(pChk->text(), isChecked);
        }
    }
    auto VizControlPanel::Init() -> void {
        d->hbox = new QHBoxLayout;
        d->hbox->setContentsMargins(0, 9, 9, 9);
        d->ui->labelFilter->setObjectName("bt-switch");
        d->ui->cellFilter->setObjectName("bt-switch");
        d->ui->controlSocket->setLayout(d->hbox);
        d->ui->opaLabel->hide();
        d->ui->maskOpacity->hide();
        d->ui->labelFilter->hide();
        d->ui->cellFilter->hide();
        d->ui->cfLabel->hide();
        d->ui->vizToggle->hide();
        d->ui->maskOpacity->setRange(0,1);
        d->ui->maskOpacity->setSingleStep(0.01);
        d->ui->maskOpacity->setValue(1.0);
        connect(d->ui->cellFilter,SIGNAL(clicked(bool)),this,SLOT(OnCellFilterChk(bool)));
        connect(d->ui->labelFilter, SIGNAL(clicked(bool)), this, SLOT(OnLabelFilterChk(bool)));
        connect(d->ui->maskOpacity,SIGNAL(valueChanged(double)),this,SLOT(OnMaskOpacity(double)));
        connect(d->ui->vizToggle, SIGNAL(clicked()), this, SLOT(OnMaskViz()));
    }
    auto VizControlPanel::ClearInterface() -> void {        
        for(auto c : d->radios) {
            d->hbox->removeWidget(c);           
            delete c;
        }
        if (d->allCheck) {
            d->hbox->removeWidget(d->allCheck);
            delete d->allCheck;
            d->allCheck = nullptr;
        }
        for(auto cc : d->checks) {            
            d->hbox->removeWidget(cc);
            delete cc;
        }
        d->ui->cfLabel->hide();
        d->ui->opaLabel->hide();
        d->ui->maskOpacity->hide();
        d->ui->cellFilter->hide();
        d->ui->labelFilter->hide();
        d->ui->vizToggle->hide();
        d->radios.clear();
        d->checks.clear();

        d->ui->cellFilter->blockSignals(true);
        d->ui->cellFilter->setChecked(false);
        d->ui->cellFilter->blockSignals(false);

        d->ui->labelFilter->blockSignals(true);
        d->ui->labelFilter->setChecked(true);
        d->ui->labelFilter->setEnabled(false);
        d->ui->labelFilter->blockSignals(false);
        d->isMultiLayer = false;
    }
}