#pragma once

#include <memory>
#include <QWidget>

#include <IVizControlPanel.h>

#include "BasicAnalysisVizControlPanelExport.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BasicAnalysisVizControlPanel_API VizControlPanel : public QWidget, public Interactor::IVizControlPanel {
        Q_OBJECT
    public:
        typedef VizControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        VizControlPanel(QWidget* parent = nullptr);
        ~VizControlPanel();

        auto Update(IParameter::Pointer param) -> bool override;
        auto Update(Entity::WorkingSet::Pointer workingset) -> bool override;
        auto Clear()->void override;

    signals:
        void sigMask(QString, bool);
        void sigMultiMask(QStringList);
        void sigMaskOpa(double);
        void sigCellFilter(bool);

    protected slots:
        void OnRadioChanged();
        void OnCheckChanged();
        void OnCellFilterChk(bool);
        void OnLabelFilterChk(bool);
        void OnMaskOpacity(double);
        void OnMaskViz();
        void OnMaskAll();

    private:
        auto Init()->void;
        auto ClearInterface()->void;
        auto CreateRadio(QStringList names)->void;
        auto CreateCB(QStringList names)->void;
        auto ToDisplayName(const QString& name)->QString;
        auto ToRealName(const QString& name)->QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}