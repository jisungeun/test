#include <QLineEdit>
#include <QMenu>
#include <QVBoxLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <RangeSlider.h>
#include <SpinBoxAction.h>
#include <OivRangeBar.h>
#include <OivScaleBar.h>

#include "BaRenderWindow2D.h"

namespace TomoAnalysis::BasicAnalysis::Plugins {    
    struct BaRenderWindow2D::Impl {
        //colormap
        int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow                

        //levelWindow
        TC::SpinBoxAction * levelWindow;//
        int level_min{ 0 };
        int level_max{ 100 };

        bool is_reload{ false };

        bool is_delete{ false };
        int ID{ -1 };

        bool right_mouse_pressed{ false };
        bool left_mouse_pressed{ false };
        bool middle_mouse_pressed{ false };
        double right_stamp{ 0.0 };
        double prev_scale{ 1.0 };
        bool is_scalarBar{ false };

        bool scalar_pressed{ false };
        float scalar_pressed_pos[2]{ 0,0 };        
        bool scale_pressed{ false };
        float scale_pressed_pos[2]{ 0,0 };
    };

    BaRenderWindow2D::BaRenderWindow2D(QWidget* parent) :
        QOivRenderWindow(parent,true), d{ new Impl } {
        setDefaultWindowType();

        d->levelWindow = new TC::SpinBoxAction("RI Range");
        connect(d->levelWindow->spinBox(), SIGNAL(lowerValueChanged(int)), this, SLOT(lowerLevelChanged(int)));
        connect(d->levelWindow->spinBox(), SIGNAL(upperValueChanged(int)), this, SLOT(upperLevelChanged(int)));
        connect(d->levelWindow, SIGNAL(sigMinChanged(double)), this, SLOT(lowerSpinChanged(double)));
        connect(d->levelWindow, SIGNAL(sigMaxChanged(double)), this, SLOT(upperSpinChanged(double)));
    }

    BaRenderWindow2D::~BaRenderWindow2D() {

    }

    auto BaRenderWindow2D::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;
        
        QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
        QAction* cmOptions[5];
        cmOptions[0] = cmMenu->addAction("Grayscale");
        cmOptions[1] = cmMenu->addAction("Inverse grayscale");
        cmOptions[2] = cmMenu->addAction("Hot iron");
        cmOptions[3] = cmMenu->addAction("JET");
        cmOptions[4] = cmMenu->addAction("Rainbow");

        for (int i = 0; i < 5; i++) {
            cmOptions[i]->setCheckable(true);
        }
        cmOptions[d->colorMap]->setChecked(true);                

        QMenu* sbMenu = myMenu.addMenu(tr("ScalarBar"));
        auto toggleSB = sbMenu->addAction("Show ScalarBar");
        toggleSB->setCheckable(true);
        toggleSB->setChecked(d->is_scalarBar);

        QMenu* levelMenu = myMenu.addMenu(tr("Level Window"));
        levelMenu->addAction(d->levelWindow);
                
        myMenu.addAction("Reset View");
     
        QAction* selectedItem = myMenu.exec(globalPos);

        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Inverse")) {
                change2DColorMap(1);
            } else if (text.contains("Grayscale")) {
                change2DColorMap(0);
            } else if (text.contains("Hot")) {
                change2DColorMap(2);
            } else if (text.contains("JET")) {
                change2DColorMap(3);
            } else if (text.contains("Rainbow")) {
                change2DColorMap(4);
            } else if (text.contains("Reset View")) {
                reset2DView();
            }
            else if (text.contains("Show ScalarBar")) {
                toggleScalarBar();
            }
        }
    }   
    auto BaRenderWindow2D::toggleScalarBar()->void {
        auto swname = "ScalarBarSwitch";
        auto scalarSwitch = MedicalHelper::find<SoSwitch>(getSceneGraph(), swname);

        if (scalarSwitch) {
            if (d->is_scalarBar) {
                scalarSwitch->whichChild = -1;
                d->is_scalarBar = false;
            }
            else {
                scalarSwitch->whichChild = 0;
                d->is_scalarBar = true;
            }
        }
    }
    auto BaRenderWindow2D::setRenderWindowID(int idx) -> void {
        d->ID = idx;
    }
    auto BaRenderWindow2D::requestUpdate() -> void {
        //getQtRenderArea()->render();
        getQtRenderArea()->update();
    }
       
    auto BaRenderWindow2D::reset2DView() -> void {
        //find which axis
        MedicalHelper::Axis ax;
        auto root = getSceneGraph();

        //reset scalar bar position
        auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
        if (scalarSep) {
            scalarSep->position.setValue(-0.95f, -0.95f);
        }     
        //reset scale position
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (scaleSep) {
            scaleSep->position.setValue(0.95f, -0.95f);
        }

        SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root,"volData");//find any volume Data

        if (volData) {            
            float slack;
            auto dims = volData->getDimension();
            auto spacing = volData->getVoxelSize();

            float x_len = dims[0] * spacing[0];
            float y_len = dims[1] * spacing[1];
            float z_len = dims[2] * spacing[2];
            if (d->ID == 0) {
                ax = MedicalHelper::Axis::AXIAL;
                slack = y_len / x_len;
            }
            else if (d->ID == 1) {
                ax = MedicalHelper::SAGITTAL;
                slack = z_len / y_len;
            }
            else if (d->ID == 2) {
                ax = MedicalHelper::CORONAL;
                slack = z_len / x_len;
            }
            else {
                return;
            }

            auto windowSlack = static_cast<float>(size().height()) / static_cast<float>(size().width());
            if (windowSlack > slack) {
                slack = windowSlack > 1 ? 1.0 : windowSlack;
            }

            MedicalHelper::orientView(ax, getCamera(), volData, slack);

            change2DColorMap(0);
            d->levelWindow->setLower(d->level_min);
            d->levelWindow->setUpper(d->level_max);
            auto scalarBar = MedicalHelper::find<OivRangeBar>(root);
            if (scalarBar) {
                scalarBar->setMinMax(d->level_min, d->level_max, false);
            }
        }
    }

    void BaRenderWindow2D::lowerSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->min = int_val;
            }
            auto scalarBar = MedicalHelper::find<OivRangeBar>(rootScene);
            if (scalarBar) {
                scalarBar->setMinMax(int_val, range->max.getValue(), false);
            }
        }
    }

    void BaRenderWindow2D::lowerLevelChanged(int val) {
        d->levelWindow->setLower(val, false);
        //d->level_min = val;

        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->min = val;// d->level_min;
            }
            auto scalarBar = MedicalHelper::find<OivRangeBar>(rootScene);
            if (scalarBar) {                
                scalarBar->setMinMax(val, range->max.getValue(), false);
            }            
        }
    }

    void BaRenderWindow2D::upperSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->max = int_val;// d->level_max;
            }
            auto scalarBar = MedicalHelper::find<OivRangeBar>(rootScene);
            if (scalarBar) {
                scalarBar->setMinMax(range->min.getValue(), int_val, false);
            }
        }
    }

    void BaRenderWindow2D::upperLevelChanged(int val) {
        d->levelWindow->setUpper(val, false);
        //d->level_max = val;
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->max = val;// d->level_max;
            }
            auto scalarBar = MedicalHelper::find<OivRangeBar>(rootScene);
            if (scalarBar) {
                scalarBar->setMinMax(range->min.getValue(), val, false);
            }            
        }
    }

    auto BaRenderWindow2D::change2DColorMap(int idx) -> void {
        auto rootScene = getSceneGraph();

        if (rootScene) {
            auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colormapHTSlice");
            if (tf) {
                int pred = 0 ;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                d->colorMap = idx;
                tf->predefColorMap = pred;
            }
            auto scalarBar = MedicalHelper::find<OivRangeBar>(rootScene);
            if (scalarBar) {
                int pred = 0;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                scalarBar->getTransferFunction()->predefColorMap = pred;
            }
            auto scaleBar = MedicalHelper::find<SoMaterial>(rootScene,"scaleMatl");
            if (scaleBar) {
                scaleBar->diffuseColor.setValue(1, 1, 1);
                if (idx == 1) {
                    scaleBar->diffuseColor.setValue(0, 0, 0);
                }
            }
        }
    }

    auto BaRenderWindow2D::initRangeSlider() -> void {
        auto newScene = getSceneGraph();
        if (newScene) {                        
            //auto volName = "volData";
            auto volData = MedicalHelper::find<SoVolumeData>(newScene,"volData");
            if (volData) {                
                d->levelWindow->setMinMax(d->level_min, d->level_max);
                d->levelWindow->setLower(d->level_min);
                d->levelWindow->setUpper(d->level_max);
            }
        }
    }
    auto BaRenderWindow2D::refreshRangeSlider() -> void {
        d->is_reload = true;
    }

    auto BaRenderWindow2D::setHTRange(double min, double max) -> void {        
        d->level_min = min;
        d->level_max = max;        
        d->levelWindow->blockSignals(true);
        d->levelWindow->setMinMax(d->level_min, d->level_max);
        d->levelWindow->setLower(d->level_min);
        d->levelWindow->setUpper(d->level_max);
        d->levelWindow->blockSignals(false);
    }

    auto BaRenderWindow2D::MouseMoveEvent(SoEventCallback* node) -> void {
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (d->scalar_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
            if (scalarSep) {
                scalarSep->position.setValue(d->scalar_pressed_pos[0] + move_x, d->scalar_pressed_pos[1] + move_y);               
            }
            return;            
        }
        if (d->scale_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
            if (scaleSep) {
                scaleSep->position.setValue(d->scale_pressed_pos[0] + move_x, -d->scale_pressed_pos[1] + move_y);
            }
            return;
        }
        if (isNavigation()) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->right_mouse_pressed) {
                auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
                auto factor = 1.0 - diff * 1.5;
                if (factor > 0) {
                    getCamera()->scaleHeight(factor / d->prev_scale);
                    d->prev_scale = factor;
                }
            }
            if (d->left_mouse_pressed) {                
                node->setHandled();
            }
            if(d->middle_mouse_pressed) {
                //panCamera(moveEvent->getPositionFloat());
                panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
        }
    }

    auto BaRenderWindow2D::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        emit sigWheel(delta);
    }
    
    auto BaRenderWindow2D::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();
        //Press Event

        //Release Event
        if(!isNavigation()){//selection mode
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                
            }
        } else {//navigation mode
            auto pos = mouseButton->getPositionFloat();
            auto vr = getViewportRegion();
            auto normpos = mouseButton->getNormalizedPosition(vr);
            auto viewport_size = vr.getViewportSizePixels();
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = true;
                d->right_stamp = normpos[1];
                d->prev_scale = 1.0;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = false;
                d->right_stamp = 0.0;
                d->prev_scale = 1.0;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                auto mouse_in_scalar = false;
                auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
                auto vp = getViewportRegion();
                if (scalarSep) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    auto x_min = (vvs[0][0] + 1.0f) / 2.0f;
                    auto x_max = (vvs[2][0] + 1.0f) / 2.0f;
                    auto y_min = (vvs[0][1] + 1.0f) / 2.0f;
                    auto y_max = (vvs[2][1] + 1.0f) / 2.0f;

                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scalar = true;
                    }
                }
                auto mouse_in_scale = false;
                auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
                if (scaleSep) {
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    auto x_min = (lbbox.getMin()[0] + 1.0f) / 2.0f;
                    auto x_max = (lbbox.getMax()[0] + 1.0f) / 2.0f;
                    auto y_min = (lbbox.getMin()[1] + 1.0f) / 2.0f;
                    auto y_max = (tbbox.getMax()[1] + 1.0f) / 2.0f;
                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scale = true;
                    }
                }
                if (mouse_in_scalar) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    d->scalar_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - vvs[0][0]);
                    d->scalar_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - vvs[0][1]);
                    d->scalar_pressed = true;
                }
                else if (mouse_in_scale) {
                    d->scale_pressed = true;
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    d->scale_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - lbbox.getMax()[0]);
                    d->scale_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - tbbox.getMax()[1]);
                }
                else {
                    d->middle_mouse_pressed = true;
                    startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->scalar_pressed = false;
                d->scale_pressed = false;
                d->middle_mouse_pressed = false;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                d->left_mouse_pressed = true;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {               
                if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                    showContextMenu(pos);
                    //node->setHandled();
                }
                d->left_mouse_pressed = false;
            }
        }
    }

    auto BaRenderWindow2D::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback        
            //set handled를 통해 event를 scene의 하위 callback으로 전달하지 않고 terminate
            //instance->setSeekMode(true);
            //seek mode는 우선 지원하지 않는것으로 20.10.16 Jose T. Kim
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode가 selection 일때 alt키를 누르는 동안 임시로 navigation mode로의 전환
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation()) {
                setInteractionMode(false);
            }
            else {
                setInteractionMode(true);
            }
        }
        //Shift + F12는 native viewer event callback이 해결하도록
    }

    auto BaRenderWindow2D::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto BaRenderWindow2D::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.0,0.0,0.0 };
        SbVec3f end_color = { 0.0,0.0,0.0 };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(true);//orthographic

        //set default navigation type
    }
}
