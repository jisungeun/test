#pragma once

#include <QOivRenderWindow.h>

#include "BaRenderWindow2DExport.h"

class SoEventCallback;

namespace TomoAnalysis::BasicAnalysis::Plugins {
    class BaRenderWindow2D_API BaRenderWindow2D : public QOivRenderWindow {
        Q_OBJECT
    public:
        BaRenderWindow2D(QWidget* parent);
        ~BaRenderWindow2D();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset2DView()->void;
        auto requestUpdate()->void;
        auto setHTRange(double min, double max)->void;
        auto setRenderWindowID(int idx)->void;
        auto refreshRangeSlider()->void;

    signals:
        void sigWheel(float);

    protected slots:
        void lowerLevelChanged(int val);
        void upperLevelChanged(int val);
        void lowerSpinChanged(double val);
        void upperSpinChanged(double val);

    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;

        auto change2DColorMap(int idx)->void;
        auto initRangeSlider()->void;

        auto toggleScalarBar()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}