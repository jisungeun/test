#pragma once

#include <memory>

#include <WorkingSet.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    class BasicAnalysisInteractor_API IPlayerPanel {
    public:
        IPlayerPanel();
        virtual ~IPlayerPanel();

        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool= 0;        
    };
}