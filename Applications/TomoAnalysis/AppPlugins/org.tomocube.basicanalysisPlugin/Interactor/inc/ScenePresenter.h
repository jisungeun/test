#pragma once

#include <memory>
#include <ISceneManagerPort.h>

#include "IRenderWindowPanel.h"
#include "IResultPanel.h"
#include "IParameterPanel.h"
#include "IPlayerPanel.h"
#include "IVizControlPanel.h"

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {    
    class BasicAnalysisInteractor_API ScenePresenter : public UseCase::ISceneManagerPort {
    public:
        ScenePresenter(IRenderWindowPanel* panel = nullptr,IResultPanel* result = nullptr,IPlayerPanel* player = nullptr,IParameterPanel* param = nullptr, IVizControlPanel* viz = nullptr);
        virtual ~ScenePresenter();

        auto Update(const Entity::WorkingSet::Pointer& image) -> void override;
        auto UpdateRange(const Entity::WorkingSet::Pointer& workingset) -> void override;
        auto UpdateImage(const IBaseImage::Pointer& image) -> void override;
        auto UpdateMask(const IBaseMask::Pointer& mask,const QString& organ_name = QString()) -> bool override;
        auto UpdateMultiMasks(const IBaseMask::Pointer& instMask, const IBaseMask::Pointer& organMask, const QStringList& organ_names) -> bool override;
        auto UpdateMeasure(const IBaseData::Pointer& measure) -> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}