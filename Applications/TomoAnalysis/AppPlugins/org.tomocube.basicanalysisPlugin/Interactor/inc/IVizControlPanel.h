#pragma once

#include <memory>

#include <WorkingSet.h>
#include <IParameter.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    class BasicAnalysisInteractor_API IVizControlPanel {
    public:
        IVizControlPanel();
        virtual ~IVizControlPanel();

        virtual auto Update(IParameter::Pointer param)->bool = 0;
        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool =0;
        virtual auto Clear()->void=0;
    };
}