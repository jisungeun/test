#pragma once

#include <memory>
#include <QString>

#include <WorkingSet.h>

#include <IProcessingEngine.h>
#include <IImageReaderPort.h>
#include <IMaskReaderPort.h>
#include <IMaskWriterPort.h>
#include <IMeasureReaderPort.h>
#include <IMeasureWriterPort.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {    
    class BasicAnalysisInteractor_API ProcessingController {
    public:
        ProcessingController(UseCase::IProcessingEngine* engine,
            UseCase::IImageReaderPort* ireader = nullptr,
            UseCase::IMaskReaderPort* mreader = nullptr,
            UseCase::IMaskWriterPort* mwriter = nullptr,
            UseCase::IMeasureReaderPort* rreader = nullptr,
            UseCase::IMeasureWriterPort* rwriter = nullptr);
        virtual ~ProcessingController();

        auto OpenReferenceImage(const QString& path,const QString& playPath,Entity::WorkingSet::Pointer workingset)->bool;

        auto Process(const QString& algo_name,IParameter::Pointer param, Entity::WorkingSet::Pointer workingset,bool saveResult = true)->bool;

        auto ChangeTimeStep(Entity::WorkingSet::Pointer workingset, int time_step)->bool;

        auto SaveCSVFile(const QString& path, Entity::WorkingSet::Pointer workingset)->bool;

        auto SaveTemporalMasks(Entity::WorkingSet::Pointer workingset)->bool;

        auto UpdateApplication(Entity::WorkingSet::Pointer workingset)->bool;

        auto RecomputeSelectedImage(const QString& pgpath, const QString& hypername, const QString& imagePath,const QString& userName)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}