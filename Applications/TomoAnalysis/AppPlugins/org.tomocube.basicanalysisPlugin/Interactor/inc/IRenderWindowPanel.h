#pragma once

#include <memory>

#include <QString>

#include <IBaseImage.h>
#include <IBaseMask.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    class BasicAnalysisInteractor_API IRenderWindowPanel {
    public:
        IRenderWindowPanel();
        virtual ~IRenderWindowPanel();

        virtual auto Update(IBaseImage::Pointer image,IBaseMask::Pointer mask = nullptr,const QString& organ_name = QString())->bool = 0;
        virtual auto UpdateMasks(const QStringList& organ_names,IBaseMask::Pointer instMask = nullptr,IBaseMask::Pointer organMask = nullptr)->bool = 0;
        virtual auto UpdateVisibility(int blob_index, bool visibility)->bool = 0;
    };
}