#pragma once

#include <IBaseData.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    class BasicAnalysisInteractor_API IResultPanel {
    public:
        IResultPanel();
        ~IResultPanel();;
        virtual auto Update(const IBaseData::Pointer& measure)->bool = 0;
    };
}