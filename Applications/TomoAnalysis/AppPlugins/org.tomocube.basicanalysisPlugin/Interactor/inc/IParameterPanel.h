#pragma once

#include <memory>

#include <WorkingSet.h>

#include "BasicAnalysisInteractorExport.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    class BasicAnalysisInteractor_API IParameterPanel {
    public:
        IParameterPanel();
        virtual ~IParameterPanel();

        virtual auto Update(Entity::WorkingSet::Pointer workingset)->bool = 0;    
    };
}