#include <iostream>

#include <OpenDataSet.h>
#include <ExecuteSegmentation.h>
#include <ExecuteMeasure.h>
#include <ExecuteProcessor.h>
#include <SaveResult.h>

#include "ProcessingController.h"

namespace TomoAnalysis::BasicAnalysis::Interactor {
    struct ProcessingController::Impl {
        UseCase::IProcessingEngine* engine{ nullptr };
        UseCase::IImageReaderPort* imageReader{ nullptr };
        UseCase::IMaskReaderPort* maskReader{ nullptr };
        UseCase::IMaskWriterPort* maskWriter{ nullptr };
        UseCase::IMeasureReaderPort* measureReader{ nullptr };
        UseCase::IMeasureWriterPort* measureWriter{ nullptr };
    };

    ProcessingController::ProcessingController(UseCase::IProcessingEngine* engine, UseCase::IImageReaderPort* ireader, UseCase::IMaskReaderPort* mreader, UseCase::IMaskWriterPort* mwriter, UseCase::IMeasureReaderPort* rreader, UseCase::IMeasureWriterPort* rwriter)
        : d{ new Impl } {
        d->engine = engine;
        d->imageReader = ireader;
        d->maskReader = mreader;
        d->maskWriter = mwriter;
        d->measureReader = rreader;
        d->measureWriter = rwriter;       
    }

    ProcessingController::~ProcessingController() {

    }
    auto ProcessingController::OpenReferenceImage(const QString& path,const QString& playPath,Entity::WorkingSet::Pointer workingset) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }
        if(true == playPath.isEmpty()) {
            return false;
        }
        if(nullptr == d->imageReader) {
            return false;
        }
        auto useCase = UseCase::OpenDataSet();
        return useCase.OpenTcfImage(path, playPath,d->imageReader, workingset);
    }
    auto ProcessingController::SaveCSVFile(const QString& path, Entity::WorkingSet::Pointer workingset) -> bool {
        if(true == path.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::SaveResult();
        return useCase.SaveCurrentMeasure(path,workingset,d->measureReader);
    }
    auto ProcessingController::SaveTemporalMasks(Entity::WorkingSet::Pointer workingset) -> bool {
        auto useCase = UseCase::SaveResult();
        return useCase.SaveMasks(workingset,d->maskWriter);
    }

    auto ProcessingController::RecomputeSelectedImage(const QString& pgpath, const QString& hypername, const QString& imagePath,const QString& userName) -> bool {        
        auto useCase = UseCase::ExecuteMeasure();
        return useCase.Refresh(pgpath,imagePath,hypername,userName,d->engine,d->imageReader,d->maskReader,d->measureWriter);
    }

    auto ProcessingController::UpdateApplication(Entity::WorkingSet::Pointer workingset) -> bool {
        auto useCase = UseCase::OpenDataSet();
        return useCase.OpenTimedMask(workingset, d->maskReader);
    }

    auto ProcessingController::Process(const QString& algo_name,IParameter::Pointer param, Entity::WorkingSet::Pointer workingset,bool saveResult) -> bool {
        if(true == algo_name.isEmpty()) {
            return false;
        }
        //param이 없는 algorithm이 있을수 있으니 param의 validity check은 하지 않는다
        if(nullptr == d->engine) {
            return false;
        }
        if(nullptr == d->maskWriter) {
            return false;
        }
        if (algo_name.contains("measurement")) {
            auto useCase = UseCase::ExecuteMeasure();
            return useCase.Request(workingset,algo_name,param,d->engine,d->measureWriter,saveResult);
        }
        if(algo_name.contains("masking")) {
            auto useCase = UseCase::ExecuteSegmentation();            
            return useCase.Request(workingset,algo_name,param,d->engine,d->maskWriter,saveResult);
        }
        if(algo_name.contains("labeling")) {
            auto useCase = UseCase::ExecuteSegmentation();
            return useCase.RequestLabeling(workingset,algo_name,param,d->engine,d->maskWriter,false);
        }
        if (algo_name.contains("postprocessing")) {
            auto useCase = UseCase::ExecuteSegmentation();
            return useCase.RequestPostProcessing(workingset, algo_name, param, d->engine, d->maskWriter, false);
        }
        if(algo_name.contains("processor")) {
            auto useCase = UseCase::ExecuteProcessor();
            return useCase.Request(workingset,algo_name,param,d->engine,d->maskWriter,d->measureWriter);
        }        
        return false;
    }

    auto ProcessingController::ChangeTimeStep(Entity::WorkingSet::Pointer workingset, int time_step) -> bool {
        if (nullptr == d->imageReader) {
            return false;
        }        
        workingset->SetImageTimeStep(time_step);
        auto useCase = UseCase::OpenDataSet();

        //open reference image
        bool isRef = useCase.OpenTimedTcf(workingset, d->imageReader);
        //open mask image
        bool isMask = useCase.OpenTimedMask(workingset, d->maskReader);
        isMask = isMask && !workingset->GetParamChanged();
        workingset->SetMaskValid(isMask);
        if(!isMask) {
            workingset->ClearMaskPath();
            workingset->ClearMask();
        }
        //open measure result
        bool isMeasure = useCase.OpenTimedResult(workingset, d->measureReader);
        isMeasure = isMeasure && !workingset->GetParamChanged();
        workingset->SetMeasureValid(isMeasure);
        if(!isMeasure) {
            workingset->ClearMeasure();
        }

        return isRef;
    }


}