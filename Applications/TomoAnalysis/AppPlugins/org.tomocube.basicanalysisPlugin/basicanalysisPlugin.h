#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.basicanalysisPlugin";

namespace TomoAnalysis::BasicAnalysis::AppUI {
    class basicanalysisPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.basicanalysisPlugin")

    public:
        static basicanalysisPlugin* getInstance();

        basicanalysisPlugin();
        ~basicanalysisPlugin();

        auto start(ctkPluginContext* context)->void override;
        //auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;
            
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static basicanalysisPlugin* instance;
    };
}