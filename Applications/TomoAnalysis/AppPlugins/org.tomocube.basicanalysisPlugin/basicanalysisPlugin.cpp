#include "basicanalysisPlugin.h"

#include <AppInterfaceTA.h>

//Pluginss

#include "MainWindow.h"

namespace TomoAnalysis::BasicAnalysis::AppUI {    
    struct basicanalysisPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;        
        MainWindow* mainWindow{nullptr};                
        AppInterfaceTA* appInterface{ nullptr };
        //Scene graph
        std::string cur_tcfname = "";
    };

    basicanalysisPlugin* basicanalysisPlugin::instance = 0;

    basicanalysisPlugin* basicanalysisPlugin::getInstance(){
        return instance;
    }
    basicanalysisPlugin::basicanalysisPlugin() : d(new Impl) {
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);
    }
    basicanalysisPlugin::~basicanalysisPlugin() {
        
    }

    auto basicanalysisPlugin::start(ctkPluginContext* context)->void {
        instance = this;
        d->context = context;
                
        registerService(context, d->appInterface, plugin_symbolic_name);
    }    
    /*auto basicanalysisPlugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context);
    }*/
    auto basicanalysisPlugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }   
}
