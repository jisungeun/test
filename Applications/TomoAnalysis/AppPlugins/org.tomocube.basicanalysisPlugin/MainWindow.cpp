#define LOGGER_TAG "[BasicAnalyzer]"
#include <Python.h>

#include <TCLogger.h>

#include <iostream>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoInfo.h>
#include <ImageViz/SoImageViz.h>
#include <Medical/InventorMedical.h>
#pragma warning(pop)

//Thread
#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QThread>
#include <QJsonValue>
#include <QFileDialog>
#include <QMessageBox>
#include <QTabBar>

//Entity
#include <IParameter.h>
#include <AppEvent.h>
#include <MenuEvent.h>
#include <ParameterList.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <TCParameterReader.h>
#include <TCParameterWriter.h>
#include <TCFMetaReader.h>
#include <ScreenShotWidget.h>
#include <FileChangeInfo.h>
#include <FileValidInfo.h>

//Controller
#include <SceneController.h>
#include <ProcessingController.h>

//Presenter
#include <ScenePresenter.h>

//Plugins
#include <ImageDataReader.h>
#include <MaskDataReader.h>
#include <MaskDataWriter.h>
#include <MeasureDataWriter.h>
#include <MeasureDataReader.h>
#include <BasicAnalysisProcessor.h>
#include <ImageDataReader.h>

//Panel
#include <BasicAnalysisViewerPanel.h>
#include <BasicAnalysisParameterPanel.h>
#include <BasicAnalysisResultPanel.h>
#include <BasicAnalysisPlayerPanel.h>

#include "DeleteDialog.h"

#include <OivTimer.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::BasicAnalysis::AppUI {
    using namespace TC::Framework;

    void OnTopWarning(QString msg) {
        QMessageBox* msgBox = new QMessageBox;
        msgBox->setIcon(QMessageBox::Warning);
        msgBox->setParent(nullptr);
        msgBox->setWindowTitle("ERROR");
        msgBox->setText(msg);
        msgBox->setWindowFlags(Qt::WindowStaysOnTopHint|Qt::Dialog | Qt::WindowTitleHint);
        msgBox->exec();
    }  

    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };        

        //Impl() = default;
        //~Impl() = default;

        QString tcf_path;

        IParameter::Pointer global_param{ nullptr };
        QMap<QString, IParameter::Pointer> share_param;                                

        //Processing enging
        Plugins::Processor::Pointer processor{ nullptr };

        // working set contains image, mask, measurements
        Entity::WorkingSet::Pointer workingset{ nullptr };

        //pop-up report panel
        Plugins::ResultPanel::Pointer resultPanel{ nullptr };        

        //screenshot widget
        TC::ScreenShotWidget::Pointer screenshotWidget{ nullptr };

        QVariantMap appProperties;

        //file changes widget
        FileChangeInfo* fileChanges{ nullptr };

        //file valids widget
        FileValidInfo* fileValids{ nullptr };

        QString cur_playgroundpath;
        QString cur_hyperpath;
        QString cur_procName;
        QString cur_hyperName;
        QStringList cur_tcfs;
        QStringList cur_ignore_tcfs;
        QList<int> cur_ignore_tcf_type;
        QStringList cur_ignore_tcf_cube;
        QString cur_displayName;
        bool cur_param_changed;
        QList<int> cur_cubeCounts;
        QStringList cur_cubes;

        //variable for OIV thread
        QMutex q_mutex;
        QWaitCondition q_wait;
        bool signal_processed{ false };
        SbThreadMutex oiv_mutex;
        int cur_tcf_step;        
        bool cancelEmit{false};        
        IParameter::Pointer oiv_param{ nullptr };
        QProgressDialog* dialog{nullptr};
        QMessageBox* waitMsg{ nullptr };
        QString oTimeStamp;
        std::shared_ptr<TC::IO::TCParameterWriter> oWriter{nullptr};
        MainWindow* thisPointer{ nullptr };
        SbThread* curThread{ nullptr };

        PyThreadState* pyThState{nullptr};
        PyGILState_STATE pyGILState;
    };
    MainWindow::MainWindow(QWidget* parent)
        : IMainWindowTA("Basic Analyzer", "Basic Analyzer", parent)
        , d{ new Impl }
    {
        d->appProperties["Parent-App"] = "Project Manager";
        d->appProperties["AppKey"] = "Basic Analyzer";
        QStringList processor_path;
        processor_path.push_back("/processor/basicanalysis");    
        d->appProperties["Processors"] = processor_path;
        d->appProperties["hasSingleRun"] = true;
        d->appProperties["hasBatchRun"] = true;        
    }
    MainWindow::~MainWindow() {
        if (d->fileChanges) {
            delete d->fileChanges;
        }
        if (d->fileValids) {
            delete d->fileValids;
        }
    }
    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    auto MainWindow::ForceClosePopup() -> void {
        if (nullptr != d->resultPanel) {
            d->resultPanel->close();
        }
    }


    auto MainWindow::GetCurTCF() const -> QString {
        return d->tcf_path;
    }
    void MainWindow::OnAcceptValidity() {
        d->fileValids->hide();

        //read previous parameter if exist
        auto param_reader = std::make_shared<TC::IO::TCParameterReader>(d->cur_playgroundpath);
        auto prev_tup = param_reader->Read("Basic Analyzer", d->cur_hyperName);
        auto prev_proc_name = std::get<0>(prev_tup);
        auto prev_param = std::get<1>(prev_tup);

        d->cur_param_changed = false;
        QString paramMessage = QString();
        if (prev_proc_name.compare(d->cur_procName) != 0) {//processor changed
            QLOG_INFO() << "Processor changed, flush exiting results";
            paramMessage = "Processor changed, flush exiting results";
            d->cur_param_changed = true;
        }
        else if (nullptr == prev_param) {
            QLOG_INFO() << "Empty previous parameter, flush existing results";
            paramMessage = "Empty previous parameter, flush existing results";
            d->cur_param_changed = true;
        }
        else if (!prev_param->IsValueEqual(d->share_param[d->cur_procName])) {
            QLOG_INFO() << "Parameter changed, flush existing results";
            paramMessage = "Parameter changed, flush existing results";
            d->cur_param_changed = true;
        }

        QDir hyperDir(d->cur_hyperpath);
        if (false == hyperDir.exists()) {
            OnAcceptBatchRun();
        }
        else {
            auto total_count = hyperDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot).count() + hyperDir.entryList(QDir::Files).count();
            if (total_count < 1) {
                OnAcceptBatchRun();
            }
            else {
                d->fileChanges->showMaximized();
                d->fileChanges->SetParentPath(d->cur_hyperpath);
                if (d->cur_param_changed) {
                    d->fileChanges->SetMessage(paramMessage);
                }
                else {
                    d->fileChanges->SetTcfPaths(d->cur_tcfs);
                    d->fileChanges->ComposeTable();
                }
            }
        }
    }
    void MainWindow::OnRejectValidity() {
        d->fileValids->hide();
        BroadcastAbortBatch();
    }
    void* MainWindow::ThreadFunc(void* userData) {
        SoDB::init();
        SoImageViz::init();
        SoVolumeRendering::init();
        InventorMedical::init();

        SoDB::setSystemTimer(OivTimer::GetInstance());

        SoPreferences::setValue("LDM_USE_IN_MEM_COMPRESSION", "0");
        SoPreferences::setValue("LDM_USE_BUILDTILE_BY_SLICE", "1");

        /*auto timer = new OivTimer;
        timer->startTimer(100, Qt::TimerType::CoarseTimer);
        SoDB::setSystemTimer(timer);*/
        //SoDB::setSystemTimer(new OivTimer);

        auto dd = static_cast<Impl*>(userData);
        dd->pyGILState = PyGILState_Ensure();
        dd->cur_tcf_step = 0;
        {            
            emit dd->thisPointer->OstepChanged(0);
            //dd->thisPointer->OivChanged(0);
            /*QMutexLocker locker(&dd->q_mutex);
            if (false == dd->signal_processed) {
                dd->q_wait.wait(&dd->q_mutex);
            }
            dd->signal_processed = false;*/
        }
        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader,std::default_delete<Plugins::ImageDataReader>() };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader,std::default_delete<Plugins::MaskDataReader>() };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader ,std::default_delete<Plugins::MeasureDataReader>() };
        std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter ,std::default_delete<Plugins::MaskDataWriter>() };
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter,std::default_delete<Plugins::MeasureDataWriter>() };
        Interactor::ProcessingController procController(dd->processor.get(),
            reader.get(), mreader.get(), mwriter.get(), rreader.get(), rwriter.get());
        for (auto i = 0; i < dd->cur_tcfs.size(); i++) {            
            auto cur_tcf = dd->cur_tcfs[i];
            if (dd->cancelEmit) {                
                emit dd->thisPointer->Ofinished(i);
                //dd->thisPointer->OivFinished(i);
                return nullptr;
            }
            if (false == procController.OpenReferenceImage(cur_tcf, dd->cur_playgroundpath, dd->workingset)) {            
                OnTopWarning("Failed to open tcf.");
                return nullptr;
            }

            if (false == procController.ChangeTimeStep(dd->workingset, 0)) {                
                OnTopWarning("Failed to change timestep.");
                return nullptr;
            }
            {                
                emit dd->thisPointer->OstepStarted(i + 1);
                //dd->thisPointer->OivStarted(i + 1);
                /*QMutexLocker locker(&dd->q_mutex);
                if (false == dd->signal_processed) {
                    dd->q_wait.wait(&dd->q_mutex);
                }
                dd->signal_processed = false;*/
            }
            if (!dd->workingset->GetMeausreValid() || !dd->workingset->GetMaskValid()) {
                if (false == procController.Process(dd->cur_procName, dd->oiv_param, dd->workingset)) {
                    //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                    OnTopWarning("Failed to process algorithm");
                    return nullptr;
                }
            }
            {                
                emit dd->thisPointer->OstepChanged(i + 1);
                //dd->thisPointer->OivChanged(i + 1);
                /*QMutexLocker locker(&dd->q_mutex);
                if (false == dd->signal_processed) {
                    dd->q_wait.wait(&dd->q_mutex);
                }
                dd->signal_processed = false;*/
            }                        
            dd->cur_tcf_step = i;

            SoDB::processEvents();
            SbThread::sleep_ms(1000);
        }
        {            
            emit dd->thisPointer->Ofinished(-1);
            //dd->thisPointer->OivFinished(-1);
        }
        PyGILState_Release(dd->pyGILState);

        InventorMedical::finish();
        SoVolumeRendering::finish();
        SoImageViz::finish();
        SoDB::finish();

        //delete timer;

        return nullptr;
    }

    void MainWindow::OnAcceptBatchRun() {
        d->fileChanges->hide();

        d->oWriter = std::make_shared<TC::IO::TCParameterWriter>(d->cur_playgroundpath);
        //create time stamp
        auto current = QDateTime::currentDateTime();
        auto timeStamp = current.toString("yyMMdd.hhmmss");
        d->oTimeStamp = current.toString("yyMMdd.hhmmss");
        auto tcf_count = d->cur_tcfs.count();                
        d->cancelEmit = false;

        d->waitMsg = new QMessageBox(QMessageBox::Icon::Information, "Information", "Wait for job finished..");
        d->waitMsg->setStandardButtons(nullptr);
        d->waitMsg->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        if(d->dialog) {
            delete d->dialog;
            d->dialog = nullptr;
        }
        d->dialog = new QProgressDialog;
        d->dialog->setLabelText(QString("Progressing %1 tcf files...").arg(tcf_count));        

        d->workingset->SetParamChanged(false);
        
        if (d->cur_param_changed) {
            QDir dir(d->cur_hyperpath);
            auto file_list = dir.entryList(QDir::Filter::Files);
            for (auto f : file_list) {
                auto file_path = d->cur_hyperpath + "/" + f;                
                QFile::remove(file_path);
                QDir dirPath(file_path.chopped(4));
                if (dirPath.exists()) {
                    dirPath.removeRecursively();
                }
            }
            //save parameter as current
            d->oWriter->Write(d->share_param[d->cur_procName], "Basic Analyzer", "Report", d->cur_hyperName, d->cur_procName);
            d->workingset->SetParamChanged(true);
        }
        d->oiv_param = d->share_param[d->cur_procName];        
        d->thisPointer = this;
                
        d->dialog->setRange(0, tcf_count);
        connect(d->dialog, SIGNAL(canceled()), this, SLOT(OnBatchCanceled()));
        d->dialog->setRange(0, tcf_count);        

        d->pyThState = PyEval_SaveThread();               

        d->curThread = SbThread::create(ThreadFunc, (void*)d.get());
        d->dialog->open();        
    }
    void MainWindow::OivStarted(int step) {
        if (d->cancelEmit) {
            //QMutexLocker locker(&d->q_mutex);
            //d->signal_processed = true;
            //d->q_wait.wakeAll();
            return;
        }
        auto tcf_idx = d->cur_tcf_step;// procStep->GetTCFStep(); ;
        auto tcf_name = d->cur_tcfs[tcf_idx];
        QFileInfo file_name(tcf_name);
        auto fn = file_name.fileName();
        d->dialog->setLabelText(QString("Current file: %1 (%2/%3)\nPerforming %4")
            .arg(fn).arg(tcf_idx + 1).arg(d->cur_tcfs.count()).arg(d->cur_displayName));
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }
    void MainWindow::OnBatchCanceled() {
        d->cancelEmit = true;
        d->dialog->finished(0);        
        d->waitMsg->exec();
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }
    void MainWindow::OivChanged(int time) {
        if(!d->cancelEmit) {
            d->dialog->setValue(time);
        }
        /*QMutexLocker locker(&d->q_mutex);
        d->signal_processed = true;
        d->q_wait.wakeAll();*/
    }

    void MainWindow::OivFinished(int result_step) {        
        if (result_step == -1) {//result
            d->dialog->finished(0);
            ////save batch Run parameter
            ///Create required history folder here
            d->oWriter->Write(d->share_param[d->cur_procName], "Basic Analyzer", "Report", d->cur_hyperName, d->cur_procName, d->oTimeStamp);
            //copy existing *.rep files to batchRun folder
            auto repFolderPath = d->cur_playgroundpath.chopped(5);

            QDir pdir(repFolderPath);
            pdir.cdUp();//playground folder
            pdir.cd("history");
            pdir.cd(d->cur_hyperName);
            pdir.cd(d->oTimeStamp);

            repFolderPath += "/";
            repFolderPath += d->cur_hyperName;

            //auto targetFolderPath = repFolderPath + "/" + timeStamp;
            auto targetFolderPath = pdir.path();

            QDir cdir(targetFolderPath);
            for (auto c : d->cur_cubes) {
                if (!cdir.exists(c)) {
                    cdir.mkdir(c);
                }
            }

            auto cubeCntIdx = 0;
            auto curCnt = 0;
            for (auto tcfName : d->cur_tcfs) {
                QFileInfo info(tcfName);
                auto filename = info.fileName().chopped(4);
                auto reportPath = repFolderPath + "/" + filename + ".rep";
                //auto instPath = repFolderPath + "/" + filename + "_MultiLabel.msk";
                //auto organPath = repFolderPath + "/" + filename + "_MultiLayer.msk";
                auto maskPath = repFolderPath + "/" + filename + ".msk";
                if (QFileInfo::exists(reportPath)) {
                    //if report file exist copy it to target folder                
                    QString ccc;
                    if (d->cur_cubeCounts[cubeCntIdx] > curCnt) {
                        curCnt++;
                        ccc = d->cur_cubes[cubeCntIdx];
                    }
                    else {
                        curCnt = 1;
                        ccc = d->cur_cubes[++cubeCntIdx];
                    }
                    auto targetPath = targetFolderPath + "/" + ccc + "/" + filename + ".rep";
                    //auto targetInstPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLabel.msk";
                    //auto targetOrganPath = targetFolderPath + "/" + ccc + "/" + filename + "_MultiLayer.msk";
                    auto targetMaskPath = targetFolderPath + "/" + ccc + "/" + filename + ".msk";
                    QFile::copy(reportPath, targetPath);
                    QFile::copy(maskPath, targetMaskPath);
                    //QFile::copy(instPath, targetInstPath);
                    //QFile::copy(organPath, targetOrganPath);
                    //copy mask paths
                    auto maskDir = maskPath.chopped(4);
                    auto targetMaskDir = targetMaskPath.chopped(4);
                    CopyPath(maskDir, targetMaskDir);
                    //auto instDir = instPath.chopped(4);
                    //auto targetInstDir = targetInstPath.chopped(4);                    
                    //CopyPath(instDir, targetInstDir);
                    //auto organDir = organPath.chopped(4);
                    //auto targetOrganDir = targetOrganPath.chopped(4);
                    //CopyPath(organDir, targetOrganDir);
                }
            }
            //copy current playground
            auto copyPGPath = d->cur_playgroundpath.chopped(5);
            QDir zdir(copyPGPath);
            zdir.cdUp();
            zdir.cd("history");

            copyPGPath = zdir.path();
            copyPGPath += "/";
            copyPGPath += d->cur_hyperName;
            copyPGPath += "_";
            copyPGPath += d->oTimeStamp;
            copyPGPath += ".tcpg";
            QFile::copy(d->cur_playgroundpath, copyPGPath);

            BroadcastFinishBatch();
        }
        else {            
            BroadcastAbortBatch();
        }                

        SbThread::destroy(d->curThread);
        if (d->waitMsg->isVisible()) {
            d->waitMsg->accept();
        }        
        //delete d->dialog;
        //d->dialog = nullptr;
        delete d->waitMsg;
        d->waitMsg = nullptr;

        PyEval_RestoreThread(d->pyThState);
    }

    void MainWindow::OnRejectBatchRun() {
        d->fileChanges->hide();
        BroadcastAbortBatch();
    }
    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }

        if (params["ExecutionType"].toString() == "BatchRun") {
            return this->ExecuteBatchRun(params);
        }

        if (params["ExecutionType"].toString() == "OpenTCF") {
            return this->ExecuteOpenTCF(params);
        }

        if (params["ExecutionType"].toString() == "Update") {
            return this->ExecuteUpdate(params);
        }

        return true;
    }
    auto MainWindow::ExecuteBatchRun(const QVariantMap& params)->bool {
        d->ui->tabWidget->setTabEnabled(0, false);
        d->ui->tabWidget->setTabEnabled(1, true);
        d->ui->tabWidget->setCurrentIndex(1);

        d->cur_playgroundpath = params["Playground path"].toString();
        d->cur_hyperName = params["Hypercube name"].toString();
        d->cur_procName = params["Processor path"].toString();

        if (d->cur_playgroundpath.isEmpty() || d->cur_hyperName.isEmpty() || d->cur_procName.isEmpty()) {
            return false;
        }
        d->cur_tcfs.clear();
        d->cur_ignore_tcfs.clear();
        d->cur_ignore_tcf_type.clear();
        d->cur_ignore_tcf_cube.clear();
        d->cur_cubeCounts.clear();
        d->cur_cubes.clear();

        TC::IO::TCFMetaReader* metaReader = new TC::IO::TCFMetaReader;

        int idx = 1;
        while (1) {
            auto cn = "Cube" + QString::number(idx);
            auto cube = params[cn];
            auto real_cube_cnt = 0;
            if (cube.isNull()) {
                break;
            }
            auto cubeInfo = cube.toStringList();
            auto cubeName = cubeInfo[0];
            d->cur_cubes.push_back(cubeName);
            for (int j = 1; j < cubeInfo.size(); j++) {
                auto tcfPath = cubeInfo[j];
                auto arrSize = metaReader->ReadArrSize(tcfPath);
                if (d->cur_procName.contains(".ai")) {
                    if (false) {
                        //if(arrSize > 2500*2500 * 200){
                        //if(arrSize > INT_MAX/4) {
                        d->cur_ignore_tcfs.append(tcfPath);
                        d->cur_ignore_tcf_cube.append(cubeName);
                        if (arrSize >= INT_MAX) {
                            d->cur_ignore_tcf_type.push_back(0);
                        }
                        else {
                            d->cur_ignore_tcf_type.push_back(1);
                        }
                    }
                    else {
                        d->cur_tcfs.append(tcfPath);
                        real_cube_cnt++;
                    }
                }
                else {
                    if (arrSize >= INT_MAX) {
                        d->cur_ignore_tcfs.append(tcfPath);
                        d->cur_ignore_tcf_cube.append(cubeName);
                        d->cur_ignore_tcf_type.push_back(0);
                    }
                    else {
                        d->cur_tcfs.append(tcfPath);
                        real_cube_cnt++;
                    }
                }
            }
            d->cur_cubeCounts.push_back(real_cube_cnt);
            idx++;
        }
        d->workingset = std::make_shared<Entity::WorkingSet>();
        d->workingset->SetCurrentHyperCubeName(d->cur_hyperName);
        auto moduleInstance = PluginRegistry::GetPlugin(d->cur_procName);
        d->cur_displayName = moduleInstance->GetName();
        d->cur_hyperpath = d->cur_playgroundpath.chopped(5) + "/" + d->cur_hyperName;

        //preform validity test first
        if (d->cur_ignore_tcfs.count() > 0) {
            d->fileValids->SetTcfPaths(d->cur_ignore_tcfs);
            d->fileValids->SetTcfCubes(d->cur_ignore_tcf_cube);
            d->fileValids->SetTcfTypes(d->cur_ignore_tcf_type);
            d->fileValids->ComposeTable();
            d->fileValids->showMaximized();
        }
        else {
            OnAcceptValidity();
        }
        return true;
    }    
    auto MainWindow::CopyPath(QString src, QString dst) -> void {
        QDir dir(src);
        if (!dir.exists())
            return;

        foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString dst_path = dst + QDir::separator() + dd;
            dir.mkpath(dst_path);
            CopyPath(src + QDir::separator() + dd, dst_path);
        }

        foreach(QString f, dir.entryList(QDir::Files)) {
            QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
        }
    }
    auto MainWindow::ExecuteUpdate(const QVariantMap& params)->bool {
        if (params["UpdateType"].toString() == "SingleRun") {
            UpdateApplication();
            return true;
        }
        auto pg_path = params["PGPath"].toString();
        auto hypername = params["HyperName"].toString();
        auto tcf_path = params["TCFPath"].toString();
        auto user_name = params["UserName"].toString();

        auto workingset = std::make_shared<Entity::WorkingSet>();

        std::shared_ptr<Plugins::ImageDataReader> ireader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> reader{ new Plugins::MaskDataReader };
        std::shared_ptr<Plugins::MeasureDataWriter> mwriter{ new Plugins::MeasureDataWriter };

        Interactor::ProcessingController procController(d->processor.get(), ireader.get(), reader.get(), nullptr, nullptr, mwriter.get());

        if (false == procController.RecomputeSelectedImage(pg_path, hypername, tcf_path, user_name)) {
            OnTopWarning("Failed to modify measure");
            return false;
        }

        //Ask remove    

        QFile loadFile(pg_path);
        if (loadFile.open(QIODevice::ReadOnly)) {
            const QByteArray data = loadFile.readAll();
            const  QJsonDocument doc(QJsonDocument::fromJson(data));
            auto object = doc.object();
            auto pgName = object["name"].toString();//pg name
            QFileInfo pgFile(pg_path);
            auto pgBase = pgFile.fileName().chopped(5);
            auto osplit = pgBase.split("_");
            auto originalTime = osplit[osplit.count() - 1];//time
            auto original_pgPath = pg_path;
            QDir fpdir(original_pgPath);
            fpdir.cdUp();//history
            fpdir.cdUp();//playground
            fpdir.cdUp();//projectname
            auto projName = fpdir.dirName();

            if (true == DeleteDialog::Delete(nullptr, projName, pgName, hypername, originalTime)) {
                //delete previous information
                auto paramPath = pg_path.chopped(5) + ".param";
                QFile::remove(paramPath);
                QFile::remove(pg_path);

                QDir deldir(original_pgPath);
                deldir.cdUp();//history
                deldir.cd(hypername);
                deldir.cd(originalTime);

                deldir.removeRecursively();
            }
        }

        BroadcastFinishBatch();
        return true;
    }    
    auto MainWindow::BroadcastApplyParam(QString procName) -> void {
        MenuEvent menuEvt(MenuTypeEnum::ParameterSet);
        QString target = "org.tomocube.projectmanagerPlugin";
        target += "*";
        target += procName;

        QString sender = "org.tomocube.basicanalysisPlugin";
        menuEvt.scriptSet(sender + "!" + target);
        publishSignal(menuEvt, "Basic Analyzer");
    }

    auto MainWindow::BroadcastCorrection(QString tcfPath,QString maskPath) -> void {
        AppEvent appEvent(AppTypeEnum::APP_WITH_SHARE);
        appEvent.setFullName("org.tomocube.intersegPlugin");
        appEvent.setAppName("Mask Editor");
        appEvent.addParameter("ExecutionType", "LinkRun");
        appEvent.addParameter("TcfPath", tcfPath);
        appEvent.addParameter("MaskPath", maskPath);
        appEvent.addParameter("SenderFull", "org.tomocube.basicanalysisPlugin");
        appEvent.addParameter("SenderName", "Basic Analyzer");
        publishSignal(appEvent, "Basic Analyzer");
    }
    auto MainWindow::BroadcastAbortBatch() -> void {
        MenuEvent menuEvt(MenuTypeEnum::AbortTab);
        QString sender = "org.tomocube.basicanalysisPlugin";
        menuEvt.scriptSet(sender);
        publishSignal(menuEvt, "Basic Analyzer");
    }
    auto MainWindow::BroadcastFinishBatch() -> void {
        MenuEvent menuEvt(MenuTypeEnum::BatchFinish);
        QString target = "org.tomocube.projectmanagerPlugin";
        QString sender = "org.tomocube.basicanalysisPlugin";
        menuEvt.scriptSet(sender + "!" + target);
        publishSignal(menuEvt, "Basic Analyzer");
    }

    auto MainWindow::ExecuteOpenTCF(const QVariantMap& params)->bool {
        auto tcf = params["TCFPath"].toString();
        auto proc = params["Processor path"].toString();
        d->ui->tabWidget->setCurrentIndex(0);
        d->ui->tabWidget->setTabEnabled(1, false);
        d->ui->tabWidget->setTabEnabled(0, true);

        d->tcf_path = tcf;

        d->screenshotWidget->SetTcfPath(d->tcf_path);

        //force processor due to file size
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        auto arrsize = metaReader->ReadArrSize(d->tcf_path);
        auto procFolderlist = d->appProperties["Processors"].toStringList();
        if (false) {
            //if(arrsize > 2500 * 2500 * 200){
            //if (arrsize > INT_MAX / 4) {//not for AI
            d->ui->parameterPanel->SetDefaultProcessors(procFolderlist, QStringList{ ".ai" });
        }
        else {
            d->ui->parameterPanel->SetDefaultProcessors(procFolderlist);
        }

        QString playgroundpath;
        if (params.contains("Playground")) {
            playgroundpath = params["Playground"].toString();
        }        

        //read tcf
        d->workingset = std::make_shared<Entity::WorkingSet>();//clear workingset
        if (params.contains("Hypercube")) {
            d->workingset->SetCurrentHyperCubeName(params["Hypercube"].toString());
        }        

        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(nullptr,
            reader.get(), mreader.get(), nullptr, rreader.get(), nullptr);

        if (false == procController.OpenReferenceImage(tcf, playgroundpath, d->workingset)) {
            QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
            return false;
        }//image is set to working set variable        

        if (false == procController.ChangeTimeStep(d->workingset, 0)) {
            QMessageBox::warning(nullptr, "Error", "Failed to change time_step.");
            return false;
        }

        //show tcf on viewer panel
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
            new Interactor::ScenePresenter(d->ui->viewerPanel,d->resultPanel.get(),
            d->ui->playerPanel,d->ui->parameterPanel,d->ui->VizControlPanel) };

        Interactor::BaSceneController sceneController(scenePresenter.get());

        if (false == sceneController.SetReferenceImage(d->workingset)) {
            QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
        }

        if (d->workingset->GetMaskValid()) {
            d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
            if (false == sceneController.SetMaskList(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
            }
            QStringList init_mask;
            init_mask.push_back("cellInst");
            if (false == sceneController.SetMaskImage(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
            }
        }
        d->resultPanel->clearResult();
        d->resultPanel->hide();
        if (d->workingset->GetMeausreValid()) {
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
            if (false == sceneController.SetResultData(d->workingset)) {
                QMessageBox::warning(nullptr, "Error", "Failed to set result data");
                d->resultPanel->hide();
                d->ui->parameterPanel->SetResultToggle(false);
            }
        }
        else {
            d->ui->parameterPanel->SetResultToggle(false);
        }

        //for highlight
        d->ui->parameterPanel->SetNoHighlight();
        if (!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }
        else if (!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }
        else {
            d->ui->parameterPanel->SetApplyHighlight();
        }

        if (!d->ui->parameterPanel->SetCurrentProcessor(proc)) {
            QLOG_ERROR() << "Failed to change proc";
        }
    }
    
    auto MainWindow::InitUI()->bool {
        //processor parameter panel
        connect(d->ui->parameterPanel, SIGNAL(sigAlgoValue(double,QString)), this, SLOT(HandleProcValChange(double,QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigProc(QString)), this, SLOT(HandleProcChange(QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigAlgoExe(QString,QString)), this, SLOT(HandleAlgoExecution(QString,QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigMaskSelected(QString)), this, SLOT(HandleMaskSelection(QString)));

        connect(d->ui->parameterPanel, SIGNAL(sigToggleResult(bool)), this, SLOT(HandleToggleResult(bool)));
        connect(d->ui->parameterPanel, SIGNAL(sigApplyParam(QString)), this, SLOT(HandleApplyParameter(QString)));
        connect(d->ui->parameterPanel, SIGNAL(sigRefHigh()), this, SLOT(HandleRefreshHighlight()));

        connect(d->ui->parameterPanel, SIGNAL(sigMaskCorrection()), this, SLOT(HandleMaskCorrection()));

        //player panel
        connect(d->ui->playerPanel, SIGNAL(timelapseIndexChanged(int)), this, SLOT(HandleTimeChange(int)));

        //report panel
        connect(d->resultPanel.get(), SIGNAL(sigSaveCsv()), this, SLOT(HandleSingleCSV()));
        connect(d->resultPanel.get(), SIGNAL(sigCloseResult()), this, SLOT(HandleCloseResult()));

        //rendering panel
        connect(d->ui->VizControlPanel, SIGNAL(sigMask(QString, bool)), this, SLOT(HandleMaskViz(QString, bool)));
        connect(d->ui->VizControlPanel, SIGNAL(sigMultiMask(QStringList)), this, SLOT(HandleMultiMaskViz(QStringList)));
        connect(d->ui->VizControlPanel,SIGNAL(sigMaskOpa(double)),this,SLOT(HandleMaskOpacity(double)));
        connect(d->ui->VizControlPanel,SIGNAL(sigCellFilter(bool)),this,SLOT(HandleMaskCellFilter(bool)));
        
        return true;
    }
    void MainWindow::HandleMaskCellFilter(bool cellwise) {
        d->ui->viewerPanel->SetCellFilter(!cellwise);
    }
    void MainWindow::HandleMaskOpacity(double opacity) {
        d->ui->viewerPanel->SetMaskOpacity(opacity);
    }
    void MainWindow::HandleApplyParameter(QString procName) {        
        d->share_param[procName] = d->ui->parameterPanel->GetParameter();

        BroadcastApplyParam(procName);
    }

    void MainWindow::HandleToggleResult(bool show) {
        if(show) {            
            d->resultPanel->showNormal();
            d->resultPanel->raise();            
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
        }else {
            d->resultPanel->hide();
        }
    }

    void MainWindow::HandleMaskViz(QString name, bool state) {        
        if (state) {
            const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                    new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr) };

            Interactor::BaSceneController sceneController(scenePresenter.get());

            if (false == sceneController.SetMaskImage(d->workingset, name)) {
                QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                return;
            }
        }else {
            
        }
    }

    void MainWindow::HandleMultiMaskViz(QStringList masks) {
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{ new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr) };
        Interactor::BaSceneController sceneController(scenePresenter.get());
        if(false == sceneController.SetMultiMask(d->workingset,masks)) {
            QMessageBox::warning(nullptr, "Error", "Failed to visuzlize multi-layered mask");
            return;
        }
    }


    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        auto singleRun = true;
        auto batchRun = true;
        return std::make_tuple(singleRun,batchRun);
    }
    void MainWindow::HandleRefreshHighlight() {
        if (!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }
        else if (!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }
        else {
            d->ui->parameterPanel->SetApplyHighlight();
        }
    }
    void MainWindow::HandleCloseResult() {
        d->ui->parameterPanel->SetResultToggle(false);
    }
    void MainWindow::HandleSingleCSV() {
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(d->processor.get(),
            nullptr, nullptr, nullptr, rreader.get());
        auto path = qApp->applicationDirPath();        
        QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save File"),
            path,
            tr("Excel csv file (*.csv)"));
        if(d->workingset->GetMeausreValid()) {
            if(false == procController.SaveCSVFile(fileName,d->workingset)) {                
                //QMessageBox::warning(nullptr, "Error", "Failed to save CSV");
                OnTopWarning("Failed to save CSV");
                return;
            }
        }
    }
    auto MainWindow::HandleInternalExecution(QString param_name,QString algo_name) -> void {
        auto param = d->ui->parameterPanel->GetParameter(param_name);

        auto connect_info = d->ui->parameterPanel->GetConnection();
        
        if (connect_info.contains(param_name)) {
            d->processor->SetReferenceImage(d->workingset->GetImage());
            auto info = connect_info[param_name];;
            auto targetAlgo = std::get<0>(info);            
            auto variables = std::get<1>(info);
            //auto targetName = PluginRegistry::GetPlugin(targetAlgo)->GetName();
            auto value = d->processor->ImageToValue(algo_name, param);
            auto data = std::dynamic_pointer_cast<DataSet>(value->GetData(0));
            auto whole_param = std::shared_ptr<IParameter>(d->ui->parameterPanel->GetParameter()->Clone());
            auto child = whole_param->GetChild(targetAlgo + " Parameter");
            for (auto i = 0; i < variables.size(); i++) {                
                auto variable = std::dynamic_pointer_cast<ScalarData>(data->GetData(variables[i]));
                auto val = variable->ValueAsDouble();                
                child->SetValue(variables[i], val);
            }
            d->ui->parameterPanel->SetParameter(whole_param);

            d->ui->parameterPanel->SetNextHighlight(param_name);
        }
    }

    auto MainWindow::UpdateApplication() -> void {        
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader,std::default_delete<Plugins::MaskDataReader>() };

        Interactor::ProcessingController procController(nullptr, nullptr, mreader.get(), nullptr, nullptr);
        if(false == procController.UpdateApplication(d->workingset)) {            
            //QMessageBox::warning(nullptr, "Error", "Failed to update Application");
            OnTopWarning("Failed to update Application");
            return;
        }

        HandleTimeChange(0);
    }
    auto MainWindow::HandleSingleExecution(QString param_name,QString algo_name) -> void {
        Q_UNUSED(param_name)
        d->pyGILState = PyGILState_Ensure();
        std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter ,std::default_delete<Plugins::MaskDataWriter>()};
        std::shared_ptr<Plugins::MeasureDataWriter> rwriter{ new Plugins::MeasureDataWriter ,std::default_delete<Plugins::MeasureDataWriter>()};

        Interactor::ProcessingController procController(d->processor.get(),
            nullptr,
            nullptr, mwriter.get(), nullptr, rwriter.get());

        auto is_ai = false;
        if(algo_name.contains("ai"))
            is_ai = true;

        if(algo_name.contains("masking")||algo_name.contains("labeling"))
            d->ui->viewerPanel->ClearMask();

        //Non-clean architecture
        //get current parameter from UI panel
        auto param = d->ui->parameterPanel->GetParameter();                

        if(algo_name.contains("measurement")) {
            auto isManual = algo_name.contains("Manual");
            if (false == procController.Process(algo_name, param, d->workingset,!isManual)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                PyGILState_Release(d->pyGILState);
                return;
            }            
        }else if(algo_name.contains("masking")) {
            auto isManual = algo_name.contains("Manual");
            if (false == procController.Process(algo_name, param, d->workingset,!isManual)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                PyGILState_Release(d->pyGILState);
                return;
            }            
        }else if(algo_name.contains("labeling")) {
            if(false == procController.Process(algo_name,param,d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                PyGILState_Release(d->pyGILState);
                return;
            }
        }else if(algo_name.contains("postprocessing")) {
            if(false == procController.Process(algo_name,param,d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to process algorithm");
                OnTopWarning("Failed to process algorithm");
                PyGILState_Release(d->pyGILState);
                return;
            }
        }

        d->ui->parameterPanel->SetNextHighlight(param_name);

        if (algo_name.contains("measurement") && d->workingset->GetMeausreValid()) {
            //show result on report panel
            d->resultPanel->clearResult();
            d->resultPanel->hide();
            d->ui->parameterPanel->SetResultToggle(false);
            const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                new Interactor::ScenePresenter(nullptr,d->resultPanel.get()),
                std::default_delete<Interactor::ScenePresenter>() };

            Interactor::BaSceneController sceneController(scenePresenter.get());

            if (false == sceneController.SetResultData(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to show result");
                OnTopWarning("Failed to show result");
                PyGILState_Release(d->pyGILState);
                return;
            }            
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
        }
        if (!is_ai) {
            if ((algo_name.contains("masking") || algo_name.contains("labeling")||algo_name.contains("postprocessing")) && d->workingset->GetMaskValid()) {
                //show tcf on viewer panel
                const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                    new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr,nullptr,d->ui->parameterPanel,d->ui->VizControlPanel),
                    std::default_delete<Interactor::ScenePresenter>()};

                Interactor::BaSceneController sceneController(scenePresenter.get());
                d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
                if (false == sceneController.SetMaskList(d->workingset)) {
                    //QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
                    OnTopWarning("Failed to update mask list");
                    PyGILState_Release(d->pyGILState);
                    return;
                }

                if (false == sceneController.SetMaskImage(d->workingset)) {
                    //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                    OnTopWarning("Failed to visualize mask image");
                    PyGILState_Release(d->pyGILState);
                    return;
                }
            }
        }
        else {
            HandleTimeChange(0);
        }

        PyGILState_Release(d->pyGILState);        
    }       

    void MainWindow::HandleMaskCorrection() {        
        auto procName = d->ui->parameterPanel->GetCurrentProcessor();
        auto maskPath = d->workingset->GetMaskPath();

        if(d->workingset->GetMasks().isEmpty()) {
            //QMessageBox::warning(nullptr, "Error", "There is no temporal mask");
            OnTopWarning("There is no temporal mask");
            return;
        }                

        //if(!procName.contains("AI")) {
        if (true){
            //save temporal mask first
            std::shared_ptr<Plugins::MaskDataWriter> mwriter{ new Plugins::MaskDataWriter ,std::default_delete<Plugins::MaskDataWriter>() };
            Interactor::ProcessingController procController(nullptr, nullptr, nullptr, mwriter.get(), nullptr, nullptr);
            if(false == procController.SaveTemporalMasks(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to save temporal masks");
                OnTopWarning("Failed to save temporal masks");
                return;
            }
        }
        auto tcfPath = d->workingset->GetImagePath();
        BroadcastCorrection(tcfPath, maskPath);
    }

    void MainWindow::HandleAlgoExecution(QString param_name,QString algo_name) {
        
        if(algo_name.contains("value")) {
            HandleInternalExecution(param_name,algo_name);
        }else {
            HandleSingleExecution(param_name,algo_name);
        }        
    }
    void MainWindow::HandleMaskSelection(QString mask_name) {        
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
                new Interactor::ScenePresenter(d->ui->viewerPanel,nullptr) };

        Interactor::BaSceneController sceneController(scenePresenter.get());

        if (false == sceneController.SetMaskImage(d->workingset,mask_name)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
            OnTopWarning("Failed to visualize mask image");
            return;
        }
    }

    void MainWindow::HandleTimeChange(int time_step) {
        d->resultPanel->clearResult();
        d->resultPanel->hide();
        d->ui->parameterPanel->SetResultToggle(false);        
        std::shared_ptr<Plugins::ImageDataReader> reader{ new Plugins::ImageDataReader };
        std::shared_ptr<Plugins::MaskDataReader> mreader{ new Plugins::MaskDataReader };
        std::shared_ptr<Plugins::MeasureDataReader> rreader{ new Plugins::MeasureDataReader };

        Interactor::ProcessingController procController(nullptr,
            reader.get(),mreader.get(),nullptr,rreader.get(),nullptr);

        if (false == procController.ChangeTimeStep(d->workingset,time_step)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to open tcf.");
            OnTopWarning("Failed to open tcf.");
            return;
        }//change image and mask in workingset

        //present changed information into panels
        //업데이트 된 working set에서 data가 존재하는 경우에만 presentation
        const std::shared_ptr<Interactor::ScenePresenter> scenePresenter{
            new Interactor::ScenePresenter(d->ui->viewerPanel,d->resultPanel.get(),
            d->ui->playerPanel,d->ui->parameterPanel,d->ui->VizControlPanel) };

        Interactor::BaSceneController sceneController(scenePresenter.get());

        if (false == sceneController.SetReferenceImage(d->workingset)) {
            //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
            OnTopWarning("Failed to visualize reference image");
        }

        if (d->workingset->GetMaskValid()) {
            d->workingset->SetParameter(d->ui->parameterPanel->GetParameter());
            if (false == sceneController.SetMaskList(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to update mask list");
                OnTopWarning("Failed to update mask list");
            }

            if (false == sceneController.SetMaskImage(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize mask image");
                OnTopWarning("Failed to visualize mask image");
            }
        }

        if (d->workingset->GetMeausreValid()) {            
            d->resultPanel->showNormal();
            d->resultPanel->raise();
            auto globalPos = mapToGlobal(geometry().topLeft());
            d->resultPanel->move(globalPos);
            d->ui->parameterPanel->SetResultToggle(true);
            if (false == sceneController.SetResultData(d->workingset)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to visualize reference image");
                OnTopWarning("Failed to visualize reference image");
                d->resultPanel->hide();
                d->ui->parameterPanel->SetResultToggle(false);
            }            
        }else {
            d->resultPanel->hide();
            d->ui->parameterPanel->SetResultToggle(false);
        }

        //for highlight                
        d->ui->parameterPanel->SetNoHighlight();
        if(!d->workingset->GetMaskValid()) {
            d->ui->parameterPanel->SetDefaultHighlight();
        }else if(!d->workingset->GetMeausreValid()) {
            d->ui->parameterPanel->SetMeasureHighlight();
        }else {
            d->ui->parameterPanel->SetApplyHighlight();
        }
    }
    void MainWindow::HandleProcValChange(double val, QString sender) {
        Q_UNUSED(val)
            //e.g. UL Threshold        
        if(sender.contains("Manual")){
            auto module_name = sender.split("!")[0];
            auto variable_name = sender.split("!")[1];            
            auto moduleInstance = PluginRegistry::GetPlugin(module_name);
            auto param = d->ui->parameterPanel->GetParameter()->GetChild(moduleInstance->GetName() + " Parameter");
            d->processor->SetParameterValue(param);
        }
    }
    void MainWindow::HandleProcChange(QString proc_name) {        
        QLOG_INFO() << "Current processor changed, flush existing results";

        //flush existing temporal files
        auto hyper_name = d->workingset->GetCurrentHyperCubeName();
        auto pg_path = d->workingset->GetWorkingDirectory();

        if (hyper_name.isEmpty()) {
            return;
        }
        if (pg_path.isEmpty()) {
            return;
        }
        auto file_base_name = d->workingset->GetCurrentImageName();        
        auto hyper_path = pg_path.chopped(5) + "/" + hyper_name;
        QDir dir(hyper_path);
        auto file_list = dir.entryList(QDir::Filter::Files);
        for (auto f : file_list) {
            if (f.contains(file_base_name)) {
                auto file_path = hyper_path + "/" + f;
                QFile::remove(file_path);
            }
        }
        auto folder_list = dir.entryList(QDir::Filter::Dirs);
        for(auto fo : folder_list) {
            if (fo.contains(file_base_name)) {
                QDir dd = dir;
                dd.cd(fo);
                dd.removeRecursively();
            }
        }
        HandleTimeChange(0);
    }
    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        if(d->share_param.contains(name)) {
            return d->share_param[name];
        }
        return nullptr;
    }
    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
        d->share_param[name] = std::shared_ptr<IParameter>(param->Clone());     
        d->ui->parameterPanel->SetParameter(d->share_param[name]);
    }
    auto MainWindow::TryDeactivate() -> bool {
        //TODO: clean function including UI clean
        return true;
    }
    auto MainWindow::TryActivate() -> bool {
        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        d->processor = std::make_shared<Plugins::Processor>();
        d->workingset = std::make_shared<Entity::WorkingSet>();
        d->resultPanel = std::make_shared<Plugins::ResultPanel>();

        d->screenshotWidget = std::make_shared<TC::ScreenShotWidget>();
        auto slayout = new QVBoxLayout;
        slayout->setContentsMargins(0, 0, 0, 0);
        d->ui->screenshotContainer->setLayout(slayout);
        slayout->addWidget(d->screenshotWidget.get());

        //connect screen shot widget
        d->ui->viewerPanel->ConnectScreenShotWidget(d->screenshotWidget);

        d->resultPanel->setWindowTitle("Measurement");        
        this->InitUI();
        d->ui->playerPanel->hide();
        d->ui->tabWidget->tabBar()->setEnabled(false);

        //Set processor information to parameter panel
        auto procFolderlist = d->appProperties["Processors"].toStringList();
        d->ui->parameterPanel->SetDefaultProcessors(procFolderlist);

        //init global communication
        subscribeEvent("TabChange");
        subscribeEvent(MenuEvent::Topic());
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnTabFocused(ctkEvent)));

        d->fileChanges = new FileChangeInfo;
        d->fileChanges->setWindowTitle("File changes");        
        connect(d->fileChanges, SIGNAL(sigAccept()), this, SLOT(OnAcceptBatchRun()));
        connect(d->fileChanges, SIGNAL(sigReject()), this, SLOT(OnRejectBatchRun()));

        d->fileValids = new FileValidInfo;
        d->fileValids->setWindowTitle("File validity");
        connect(d->fileValids, SIGNAL(sigAccept()), this, SLOT(OnAcceptValidity()));
        connect(d->fileValids, SIGNAL(sigReject()), this, SLOT(OnRejectValidity()));

        connect(this, SIGNAL(OstepChanged(int)), this, SLOT(OivChanged(int)));
        connect(this, SIGNAL(Ofinished(int)), this, SLOT(OivFinished(int)));
        connect(this, SIGNAL(OstepStarted(int)), this, SLOT(OivStarted(int)));

        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return (nullptr != d->ui);
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::GetFeatureName() const -> QString {
	    return "org.tomocube.basicanalysisPlugin";
    }

    auto MainWindow::OnAccepted() -> void {
    }

    auto MainWindow::OnRejected() -> void {
    }

    void MainWindow::OnTabFocused(const ctkEvent& ctkEvent) {
        using MenuType = MenuTypeEnum;
        MenuType sudden = MenuType::NONE;

        if (ctkEvent.getProperty("TabName").isValid()) {
            auto TabName = ctkEvent.getProperty("TabName").toString();
            if (TabName.compare("Basic Analyzer") == 0) {
                auto tcf_path = GetCurTCF();

                MenuEvent menuEvt(MenuTypeEnum::TitleBar);
                menuEvt.scriptSet(tcf_path);
                publishSignal(menuEvt, "Basic Analyzer");                
                HandleToggleResult(d->ui->parameterPanel->GetResultToggle());
            }else {
                ForceClosePopup();
            }
        }
        else if (ctkEvent.getProperty(sudden._to_string()).isValid()) {
            ForceClosePopup();
        }
    }
}
