#include <iostream>
#include "TCFThumbnailWriter.h"
#include "Version.h"

int main(int argc, char* argv[]) {
    if (argc == 2) {
        const auto argument = std::string{ argv[1] };
        const auto argumentIsHelp = (argument == "-h") || (argument == "-H") || (argument == "-Help") || (argument =="-help");

        if (argumentIsHelp) {
            std::cout << R"(Version )" PROJECT_REVISION << std::endl;
            std::cout << R"(ThumbnailWriter.exe writes png files from TCF MIP Images.)" << std::endl;
            std::cout << R"(Arguments should be as [ThumbnailWriter.exe "tcfFilePath" "outputFolderPath"])" << std::endl;
            return 0;
        }
    }

    auto argumentError = false;
    if (argc != 3) {
        argumentError = true;
    }

    if (!argumentError) {
        const auto tcfFilePath = argv[1];
        const auto outputFolderPath = argv[2];

        TC::IO::TCFThumbnailWriter tcfThumbnailWriter;
        tcfThumbnailWriter.SetTcfFilePath(tcfFilePath);
        tcfThumbnailWriter.SetOutputFolderPath(outputFolderPath);

        const auto writingResult = tcfThumbnailWriter.Write();

        if (writingResult == true) {
            std::cout << R"(Writing is done.)";
        } else {
            std::cout << R"(Writing fails.)";
        }
    } else {
        std::cout << R"(Wrong argument : Argument should be as [ThumbnailWriter.exe "tcfFilePath" "outputFolderPath"] )" << std::endl;
    }

    return 0;
}