function Component()
{
}

Component.prototype.createOperations = function()
{
	//call default implementation
    component.createOperations();

	//add shortcuts on start menu
    component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/CBCResearch.exe",
						   "@StartMenuDir@/CBCResearch.lnk",
						   "workingDirectory=@TargetDir@/bin");
			
	component.addOperation("CreateShortcut",
						   "@TargetDir@/MaintenanceTool.exe",
						   "@StartMenuDir@/Uninstall.lnk",
						   " -- uninstall");
			
	component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/Tomocube.url",
						   "@StartMenuDir@/Tomocube Inc.lnk");
                          
    //add shortcuts on desktop                          
    component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/CBCResearch.exe",
						   "@DesktopDir@/CBCResearch.lnk",
						   "workingDirectory=@TargetDir@/bin");
}
