
message( "External project - libTorch (${CUSTOM_BUILD_TYPE})" )

#release/debug builds are not ABI compatible...

FetchContent_Declare(libTorch
  #URL https://download.pytorch.org/libtorch/cu111/libtorch-win-shared-with-deps-1.9.0%2Bcu111.zip
  #URL https://download.pytorch.org/libtorch/cu113/libtorch-win-shared-with-deps-1.11.0%2Bcu113.zip
  URL https://download.pytorch.org/libtorch/cu113/libtorch-win-shared-with-deps-1.10.2%2Bcu113.zip
  #URL https://download.pytorch.org/libtorch/cu115/libtorch-win-shared-with-deps-1.11.0%2Bcu115.zip
  #URL_MD5 fab3fdda5ec4f593cae39bd16c7161e3
  #URL_MD5 be7a27312c797a04f2870f9a41cd6589
  URL_MD5 D3711140709C64764DB837040921B19F
  #URL_MD5 FA76C5E14F09E6D0FC1B2DC3442A6B55
)

FetchContent_Declare(libTorchDebug
  #URL https://download.pytorch.org/libtorch/cu111/libtorch-win-shared-with-deps-debug-1.9.0%2Bcu111.zip
  #URL https://download.pytorch.org/libtorch/cu113/libtorch-win-shared-with-deps-debug-1.11.0%2Bcu113.zip
  URL https://download.pytorch.org/libtorch/cu113/libtorch-win-shared-with-deps-debug-1.10.2%2Bcu113.zip
  #URL https://download.pytorch.org/libtorch/cu115/libtorch-win-shared-with-deps-debug-1.11.0%2Bcu115.zip
  #URL_MD5 dce393251b3c99a1488d55680c40841b
  #URL_MD5 994dde0dd98a7e73cce512350167efec
  URL_MD5 1533B48CB593A68C0B8D1C81505CC342
  #URL_MD5 E71E6D55D3E11A2A4EB44476C1A151A4
)

if (CUSTOM_BUILD_TYPE STREQUAL "Release")
  FetchContent_MakeAvailable(libTorch)
  set( libTorch_DIR ${libtorch_SOURCE_DIR} )
else()
  FetchContent_MakeAvailable(libTorchDebug)
  set( libTorch_DIR ${libtorchdebug_SOURCE_DIR} )
endif()

set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${libTorch_DIR}/share/cmake/Torch )
set( Torch_DIR ${libTorch_DIR}/share/cmake/Torch )
set( TORCH_INCLUDE_DIRS ${libTorch_DIR}/include )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${libTorch_DIR}/lib" )




