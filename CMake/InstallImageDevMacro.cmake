#Install ImageDev Related Files

if(DEFINED ImageDev_HOME)
	if(DEFINED ImageDev_ARCH_RELEASE)
		set(IDEV_BIN ${ImageDev_HOME}/${ImageDev_ARCH_RELEASE}/bin)
		file(GLOB EVERY_DLLS "${IDEV_BIN}/*.dll")
		
		message(STATUS EVERY_DLLS)
		
		foreach(IDEV_FILE IN LISTS EVERY_DLLS)
			install(FILES ${IDEV_FILE} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_oiv)
		endforeach(IDEV_FILE)
	endif()
endif()

if(DEFINED ImageDevExamples_HOME)
	if(DEFINED ImageDevExamples_ARCH_RELEASE)
		set(IDEV_EXAMPLE_BIN ${ImageDevExamples_HOME}/${ImageDevExamples_ARCH_RELEASE}/bin)
		file(GLOB EXAMPLE_DLLS "${IDEV_EXAMPLE_BIN}/*.dll")
	
		message(STATUS EXAMPLE_DLLS)
		
		foreach(IDEV_EXAMPLE_FILE IN LISTS EXAMPLE_DLLS)
			install(FILES ${IDEV_EXAMPLE_FILE} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_oiv)
		endforeach(IDEV_EXAMPLE_FILE)
		
		install(DIRECTORY ${ImageDevExamples_HOME}/${ImageDevExamples_ARCH_RELEASE}/bin/ioplugins DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_oiv)
	endif()
endif()