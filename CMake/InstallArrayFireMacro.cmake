#Install ArrayFire Related Files
if (DEFINED ArrayFire_DIR)
	set(ArrayFire_BIN ${ArrayFire_DIR}/../lib)
	file(GLOB_RECURSE ArrayFire_DLLS "${ArrayFire_BIN}/*.dll")
			
	foreach(file ${ArrayFire_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_decon)
	endforeach(file)
endif()