#Install CTK Related Files
if (DEFINED CTK_DIR)
	file(GLOB_RECURSE CTK_DLLS "${CTK_DIR}/*.dll")
			
	foreach(file ${CTK_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
endif()