#Install ArrayFire Related Files
if (DEFINED SevenZip_DIR)
	file(GLOB_RECURSE SevenZip_FILS "${SevenZip_DIR}/bin/*.exe")
			
	foreach(file ${SevenZip_FILS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_util)
	endforeach(file)
endif()