if(USE_SUPERBUILD)
    message( "External project - QtAdvancedDockingSystem" )

    ExternalProject_add(QtAdvancedDockingSystem
      PREFIX ${CMAKE_BINARY_DIR}/Extern/QtAdvancedDockingSystem
      GIT_REPOSITORY https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System.git
      GIT_TAG 4.1.1
      UPDATE_COMMAND ""
      CMAKE_ARGS
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_PREFIX_PATH}
        -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/QtAdvancedDockingSystem
		-DBUILD_EXAMPLES=OFF
      INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}/QtAdvancedDockingSystem
    )
endif()

set( ADS_DIR ${INSTALL_DEPENDENCIES_DIR}/QtAdvancedDockingSystem )
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ADS_DIR} )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${INSTALL_DEPENDENCIES_DIR}/QtAdvancedDockingSystem/bin" )