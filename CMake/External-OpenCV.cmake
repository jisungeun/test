if(USE_SUPERBUILD)
    message( "External project - OpenCV" )
	ExternalProject_add(OpenCV
	  PREFIX ${CMAKE_BINARY_DIR}/Extern/OpenCV
	  GIT_REPOSITORY https://github.com/opencv/opencv.git
	  GIT_TAG 4.3.0
	  UPDATE_COMMAND ""
	  CMAKE_ARGS
		-DBUILD_opencv_python2=OFF
		-DBUILD_opencv_python3=OFF
		-DBUILD_opencv_java=OFF
		-DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/opencv
	  INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}
	)

endif()

#if ("${CUSTOM_BUILD_TYPE}" STREQUAL "Release")
#    set( OpenCV_ZLib_DIR ${CMAKE_BINARY_DIR}/Extern/OpenCV/src/OpenCV-build/bin/Release )
#elseif ("${CUSTOM_BUILD_TYPE}" STREQUAL "Debug")
#    set( OpenCV_ZLib_DIR ${CMAKE_BINARY_DIR}/Extern/OpenCV/src/OpenCV-build/bin/Debug )
#else()
#		message(FATAL_ERROR "CUSTOM_BUILD_TYPE must be 'Release' or 'Debug'")
#endif()

#set( OpenCV_ZLib_FILE ${OpenCV_ZLib_DIR}/zlib1.dll )
set( OpenCV_DIR ${INSTALL_DEPENDENCIES_DIR}/opencv )
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${OpenCV_DIR} )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${OpenCV_DIR}/x64/vc16/bin")
#set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${OpenCV_DIR}/x64/vc16/bin;${OpenCV_ZLib_DIR}" )
