
if(USE_SUPERBUILD)
	message( "External project - CTK (${CUSTOM_BUILD_TYPE})" )

	ExternalProject_add(CTK
	  PREFIX ${CMAKE_BINARY_DIR}/Extern/CTK
	  GIT_REPOSITORY https://github.com/commontk/CTK.git
	  GIT_TAG 2018-10-29
	  UPDATE_COMMAND ""
	  CMAKE_ARGS
		-DBUILD_EXAMPLES:BOOL=OFF
		-DCTK_BUILD_SHARED_LIBS:BOOL=ON
		-DBUILD_TESTING:BOOL=OFF
		-DCTK_QT_VERSION:STRING=5
		-DQt5_DIR=${Qt5_DIR}
		-DCTK_LIB_CommandLineModules/Frontend/QtGui:BOOL=ON
		-DCTK_LIB_CommandLineModules/Backend/LocalProcess:BOOL=ON
		-DCTK_LIB_CommandLineModules/Core:BOOL=ON
		-DCTK_LIB_Core:BOOL=ON
		-DCTK_LIB_PluginFramework:BOOL=ON
		-DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/CTK
		-DCTK_LIB_Widgets:BOOL=ON
		-DCTK_ENABLE_PluginFramework:BOOL=ON
		-DCTK_PLUGIN_org.commontk.configadmin:BOOL=ON
		-DCTK_PLUGIN_org.commontk.eventadmin:BOOL=ON
		-DCTK_APP_ctkPluginGenerator:BOOL=ON
	  INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}
	)
endif()

set( CTK_DIR ${CMAKE_BINARY_DIR}/Extern/CTK/src/CTK-build)
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CTK_DIR} )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${CMAKE_BINARY_DIR}/Extern/CTK/src/CTK-build/CTK-build/bin/${CUSTOM_BUILD_TYPE}" )