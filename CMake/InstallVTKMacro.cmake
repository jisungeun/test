#Install VTK Related Files
if (DEFINED VTK_DIR)
	string(REPLACE "lib/cmake/vtk-9.0" "bin" VTKBIN ${VTK_DIR})
	file(GLOB_RECURSE VTK_DLLS "${VTKBIN}/*.dll")
			
	foreach(file ${VTK_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_ta)
	endforeach(file)
endif()