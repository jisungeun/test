#Install Files related to HT Processing Module using Matlab
if (DEFINED Qt5_DIR)
	#Copy Qt5 binary DLL files.
	string(REPLACE "lib/cmake/Qt5" "bin" Qt5BIN ${Qt5_DIR})
	
	set(QT5_DLLS "${Qt5BIN}/Qt5Core.dll")
	
	foreach(file ${QT5_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
	endforeach(file)
endif()

install(TARGETS TCLogger RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS QsLog RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCProfileIO RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCHTProcessingProfile RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCPSFProfile RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCImagingProfile RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCHTProcessingMatlabAppComponent RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)
install(TARGETS TCBeadEvaluationMatlabAppComponent RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/HTProc COMPONENT processing)