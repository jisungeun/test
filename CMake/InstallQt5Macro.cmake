#Install Qt5 Related Files
if (DEFINED Qt5_DIR)
	#Copy Qt5 binary DLL files.
	string(REPLACE "lib/cmake/Qt5" "bin" Qt5BIN ${Qt5_DIR})
	
	set(QT5_DLLS "${Qt5BIN}/Qt5Concurrent.dll"
                 "${Qt5BIN}/Qt5Core.dll"
				 "${Qt5BIN}/Qt5Gui.dll"
				 "${Qt5BIN}/Qt5Network.dll"
				 "${Qt5BIN}/Qt5Help.dll"
				 "${Qt5BIN}/Qt5OpenGL.dll"
				 "${Qt5BIN}/Qt5Qml.dll"
				 "${Qt5BIN}/Qt5Quick.dll"
				 "${Qt5BIN}/Qt5Widgets.dll"
				 "${Qt5BIN}/Qt5QuickControls2.dll"
				 "${Qt5BIN}/Qt5QuickTemplates2.dll"
				 "${Qt5BIN}/Qt5QuickWidgets.dll"
				 "${Qt5BIN}/Qt5Xml.dll"
				 "${Qt5BIN}/Qt5Sql.dll"
				 "${Qt5BIN}/Qt5Charts.dll"
				 "${Qt5BIN}/Qt5Svg.dll"
                 "${Qt5BIN}/Qt5SerialPort.dll"
	)
	
	foreach(file ${QT5_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
	
	
	#Copy Qt5 plugin DLL files.
	string(REPLACE "lib/cmake/Qt5" "" Qt5_RAW_DIR ${Qt5_DIR})
	file(GLOB_RECURSE Qt5_PLUGIN_FILES "${Qt5_RAW_DIR}/plugins/*.dll")
	
	foreach(file ${Qt5_PLUGIN_FILES})
		# Some of dll files could end with -d though it's not a debug file.
		get_filename_component(Qt5_DLL_FILENAME ${file} NAME)
		string(REPLACE ${Qt5_RAW_DIR} ${CMAKE_INSTALL_BINDIR} Qt5_INSTALL_DIR ${file})
		string(REPLACE ${Qt5_DLL_FILENAME} "" Qt5_INSTALL_DIR ${Qt5_INSTALL_DIR})
		
		if(${file} MATCHES "d.dll$")
			string(REPLACE "d.dll" ".dll" relfile ${file})
			if(NOT EXISTS ${relfile})
				install(FILES ${file} DESTINATION ${Qt5_INSTALL_DIR} COMPONENT thirdparty)
			endif()
		else()
			install(FILES ${file} DESTINATION ${Qt5_INSTALL_DIR} COMPONENT thirdparty)
		endif()
	endforeach(file)
endif()