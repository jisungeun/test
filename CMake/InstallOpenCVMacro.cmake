#Install OpenCV Related Files
if (DEFINED OpenCV_DIR)
    set(OPENCV_BIN ${OpenCV_DIR}//x64/vc16/bin)
	file(GLOB_RECURSE OpenCV_DLLS "${OPENCV_BIN}/*.dll")
			
	foreach(file ${OpenCV_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
endif()

if (DEFINED OpenCV_ZLib_FILE)
    install(FILES ${OpenCV_ZLib_FILE} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
endif()