if(USE_SUPERBUILD)
    message( "External project - QtKeyChain" )


    ExternalProject_add(QtKeyChain
      PREFIX ${CMAKE_BINARY_DIR}/Extern/QtKeyChain
      GIT_REPOSITORY https://github.com/frankosterfeld/qtkeychain.git
      GIT_TAG v0.12.0
      UPDATE_COMMAND ""
      CMAKE_ARGS
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_PREFIX_PATH}
        -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/qtkeychain
        -DUSE_CREDENTIAL_STORE=OFF
      INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}/qtkeychain
    )

endif()

set( QtKeyChain_DIR ${INSTALL_DEPENDENCIES_DIR}/qtkeychain )
set( Qt5Keychain_DIR ${QtKeyChain_DIR}/lib/cmake/Qt5Keychain )
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${QtKeyChain_DIR}/lib/cmake/Qt5Keychain )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${QtKeyChain_DIR}/bin" )