#Install HDF5 Related Files
if (DEFINED HDF5_DIR)
    string(REPLACE "cmake/hdf5" "bin" HDF5_BIN ${HDF5_DIR})
	file(GLOB_RECURSE HDF5_DLLS "${HDF5_BIN}/*.dll")
			
	foreach(file ${HDF5_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
    
    set(ZLIB_DLL ${INSTALL_DEPENDENCIES_DIR}/zlib/bin/zlib.dll)
    install(FILES ${ZLIB_DLL} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
endif()