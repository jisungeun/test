#Install LIBTIFF Related Files
if (DEFINED LIBTIFF_DIR)
	file(GLOB_RECURSE LIBTIFF_DLL "${LIBTIFF_DIR}/bin/*.dll")
	
	foreach(file ${LIBTIFF_DLL})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
endif()
