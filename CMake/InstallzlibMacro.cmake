#Install ZLIB Related Files
if (DEFINED ZLIB_DIR)
    set(ZLIB_BIN ${ZLIB_DIR}/bin)
    file(GLOB_RECURSE ZLIB_DLLS "${ZLIB_BIN}/*.dll")
    
    foreach(file ${ZLIB_DLLS})
        install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
    endforeach(file)
endif()