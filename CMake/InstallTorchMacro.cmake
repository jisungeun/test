#Install Torch Related Files
if (DEFINED Torch_DIR)
	string(REPLACE "share/cmake/Torch" "lib" TorchBin ${Torch_DIR})
	file(GLOB_RECURSE Torch_DLLS "${TorchBin}/*.dll")
			
	foreach(file ${Torch_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_ai)
	endforeach(file)
endif()