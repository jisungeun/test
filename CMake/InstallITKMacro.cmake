#Install ITK Related Files
if (DEFINED ITK_DIR)
	string(REPLACE "lib/cmake/ITK-5.1" "bin" ITKBIN ${ITK_DIR})
	file(GLOB_RECURSE ITK_DLLS "${ITKBIN}/*.dll")
			
	foreach(file ${ITK_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_deprecated)
	endforeach(file)
endif()