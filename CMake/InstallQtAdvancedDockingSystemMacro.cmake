if (DEFINED ADS_DIR)
	set(ADS_BIN "${ADS_DIR}/bin")
	file(GLOB_RECURSE ADS_DLLS "${ADS_BIN}/*.dll")

	foreach(file ${ADS_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty_ta)
	endforeach(file)
endif()