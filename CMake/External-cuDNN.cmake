
message( "External project - cuDNN" )

FetchContent_Declare(cuDNN
  GIT_REPOSITORY https://bitbucket.org/tomocubemitkdev/cudnn.git
  GIT_TAG cudnn-11.3-windows-x64-v8.2.0.53
  #GIT_TAG cudnn-11.5-windows-x64-v8.3.3.40
)

FetchContent_MakeAvailable(cuDNN)


set( cuDNN_DIR ${cudnn_SOURCE_DIR}/cuda )
set( CUDNN_INCLUDE_PATH ${cuDNN_DIR}/include )
set( CUDNN_LIBRARY_PATH ${cuDNN_DIR}/lib/x64/cudnn.lib )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${cuDNN_DIR}/bin" )