set(VC_DIR ${CMAKE_SOURCE_DIR}/Extern/VCRedist)

file(GLOB_RECURSE VC_EXECUTABLE "${VC_DIR}/*.exe")

foreach(file ${VC_EXECUTABLE})
	install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT redist)
endforeach(file)