#Install QtKeyChain Related Files
if (DEFINED QtKeyChain_DIR)
    set(QtKeyChain_BIN ${QtKeyChain_DIR}/bin)
	file(GLOB_RECURSE QtKeyChain_DLLS "${QtKeyChain_BIN}/*.dll")
			
	foreach(file ${QtKeyChain_DLLS})
		install(FILES ${file} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT thirdparty)
	endforeach(file)
endif()
