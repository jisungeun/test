set(MATLAB_RUNTIME_DIR "" CACHE PATH "Selection of location of matlab runtime 2021a")

if(MATLAB_RUNTIME_DIR STREQUAL "")
	message(FATAL_ERROR "Set Matlab runtime 2021a(v910) directory 'C:/Program Files/MATLAB/MATLAB Runtime/v910'")
else()
	set(MATLAB_INCLUDE_DIR "${MATLAB_RUNTIME_DIR}/extern/include")
	set(MATLAB_DATAARRAY_LIB "${MATLAB_RUNTIME_DIR}/extern/lib/win64/microsoft/libMatlabDataArray.lib")
	set(MATLAB_CPP_SHARED_LIB "${MATLAB_RUNTIME_DIR}/extern/lib/win64/microsoft/libMatlabCppSharedLib.lib")
endif()

set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${MATLAB_RUNTIME_DIR}/bin/win64;${MATLAB_RUNTIME_DIR}/runtime/win64")