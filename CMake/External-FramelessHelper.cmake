if(USE_SUPERBUILD)
    message( "External project - FramelessHelper" )

    ExternalProject_add(FramelessHelper
      PREFIX ${CMAKE_BINARY_DIR}/Extern/FramelessHelper
      GIT_REPOSITORY https://github.com/wangwenx190/framelesshelper.git
      GIT_TAG 2.4.2
      UPDATE_COMMAND ""
      CMAKE_ARGS
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_PREFIX_PATH}
        -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/FramelessHelper
		-DFRAMELESSHELPER_BUILD_QUICK=OFF
		-DFRAMELESSHELPER_NO_MICA_MATERIAL=ON
		-DFRAMELESSHELPER_NO_DEBUG_OUTPUT=ON
      INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}/FramelessHelper
    )
endif()

set( FRAMELESSHELPER_DIR ${INSTALL_DEPENDENCIES_DIR}/FramelessHelper )
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${FRAMELESSHELPER_DIR} )
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${FRAMELESSHELPER_DIR}/bin64/debug;${FRAMELESSHELPER_DIR}/bin64/release" )