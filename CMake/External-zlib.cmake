if(USE_SUPERBUILD)
    message( "External project - zlib" )
    ExternalProject_add(zlib
        PREFIX ${CMAKE_BINARY_DIR}/Extern/zlib
        URL https://www.zlib.net/fossils/zlib-1.2.13.tar.gz
        URL_HASH SHA256=b3a24de97a8fdbc835b9833169501030b8977031bcb54b3b3ac13740f846ab30
        UPDATE_COMMAND ""
        CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}/zlib			
        INSTALL_DIR ${INSTALL_DEPENDENCIES_DIR}
    )
endif()

set( ZLIB_DIR ${INSTALL_DEPENDENCIES_DIR}/zlib)
set( TSS_RUNTIME_PATH "${TSS_RUNTIME_PATH};${ZLIB_DIR}/bin" )