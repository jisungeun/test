#pragma once

#include <QObject>
#include <IProcessor.h>

#include "TC_Processor_BasicAnalysis_ManualExport.h"

namespace TC::Processor::BasicAnalysis::Manual {
	class TC_Processor_BasicAnalysis_Manual_API Processor
		: public QObject
		, public IProcessor {
		Q_OBJECT
			Q_PLUGIN_METADATA(IID "org.tomocube.processor.basicanalysis.manual")
			Q_INTERFACES(IProcessor)
	public:
		Processor();
		virtual ~Processor();

		auto GetName() const -> QString override { return "Manaul BasicAnalysis"; }
		auto GetFullName() const -> QString override { return "org.tomocube.processor.basicanalysis.manual"; }
		auto GetDescription() const -> QString override { return "Basic analysis based on interactive manual segmentation"; }
		auto clone() const -> IPluginModule* override { return new Processor(); }

		auto Parameter(const QString& key) -> IParameter::Pointer override;		
		auto SetData(DataSet::Pointer data) -> void override;
		auto Execute() -> bool override;
		auto GetOutputFormat() const->QString override;
		auto GetResult()->DataSet::Pointer override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}