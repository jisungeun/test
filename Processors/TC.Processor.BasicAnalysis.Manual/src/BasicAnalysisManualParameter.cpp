#include <ParameterRegistry.h>
#include "BasicAnalysisManualParameter.h"

namespace TC::Processor::BasicAnalysis::Manual{
    auto Parameter::Register()->void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter()
        : IParameter() {
        RegisterNode("Manual Segmentation", "Manual segmentation algorithm", "Manual segmentation algorithm", "0", "org.tomocube.algorithm.masking.ManualThreshold", "", "");
        RegisterNode("Labeling", "Labeling algorithm", "Automatic labeling algorithm", "1", "org.tomocube.algorithm.labeling.basic", "", "");
        RegisterNode("Basic Measurement", "Basic measurement algorithm", "Basic quantitative measure algorithm", "2", "org.tomocube.algorithm.measurement.basic", "", "");
        RegisterChild("Manual Segmentation Parameter", "Parameter for manual algorithm", nullptr);
        RegisterChild("Labeling Parameter", "Paraemter for labeling algorithm", nullptr);
        RegisterChild("Basic Measurement Parameter", "Paramter for basic meausrement", nullptr);
    }
}