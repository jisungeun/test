#include <iostream>
#include <memory>

#include <IBaseImage.h>

#include "BasicAnalysisManual.h"
#include "BasicAnalysisManualParameter.h"
#include "ICustomAlgorithm.h"
#include "ISegmentationAlgorithm.h"
#include "ParameterRegistry.h"
#include "PluginRegistry.h"

namespace TC::Processor::BasicAnalysis::Manual {
    struct Processor::Impl {
        IParameter::Pointer param{ ParameterRegistry::Create(Parameter::GetName()) };
        DataSet::Pointer data;
        DataSet::Pointer result;
    };
    Processor::Processor() :d{ new Impl } {
        Parameter::Register();
        
    }
    Processor::~Processor() {

    }
    auto Processor::Parameter(const QString& key) -> IParameter::Pointer {
        return d->param;
    }
    auto Processor::SetData(DataSet::Pointer data) -> void {
        d->data = data;
    }
    auto Processor::Execute()->bool {
        const auto manualSegAlgo = d->param->GetValue("Manual Segmentation").toString();
        const auto labelingAlgo = d->param->GetValue("Labeling").toString();
        const auto measureAlgo = d->param->GetValue("Basic Measurement").toString();

        auto ManualSeg = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(manualSegAlgo));
        auto Labeling = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(labelingAlgo));
        auto Measure = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(measureAlgo));

        auto autoParam = d->param->GetChild("Manual Segmentation Parameter");
        auto labelParam = d->param->GetChild("Labeling Parameter");
        auto measureParam = d->param->GetChild("Basic Measurement Parameter");

        if (!ManualSeg->Parameter(autoParam)) {
            //set binary segmentation parameter
            std::cout << "segmentation parameter setting failed" << std::endl;
            return false;
        }

        if (!Labeling->Parameter(labelParam)) {
            //set labeling parameter
            std::cout << "labeling parameter setting failed" << std::endl;
            return false;
        }

        if(!Measure->Parameter(measureParam)) {
            std::cout << "measurement parameter setting failed" << std::endl;
            return false;
        }

        try {
            //perform otsu segmentation
            auto image = std::dynamic_pointer_cast<IBaseImage>(d->data->GetData(0));
            ManualSeg->SetInput(image);
            if (!ManualSeg->Execute()) {
                std::cout << "performing segmentation failed" << std::endl;
            }

            auto binaryMask = ManualSeg->GetOutput();

            //perfrom labeling
            Labeling->SetInput(binaryMask);
            if (!Labeling->Execute()) {
                std::cout << "performing labeling failed" << std::endl;
            }

            auto resultingMask = Labeling->GetOutput();

            DataSet::Pointer result{ DataSet::New() };
            result->AppendData(resultingMask,"Raw Mask Image");//append mask data

            Measure->SetInput(resultingMask);
            if(!Measure->Execute()) {
                std::cout << "performing measurement failed" << std::endl;
            }

            auto measureResult = Measure->GetOutput();
            result->AppendData(measureResult,"Measurement Result");

            d->result = result;
        }
        catch (...) {
            std::cout << "exception occured" << std::endl;
            return false;
        }
        return EXIT_SUCCESS;
    }
    auto Processor::GetResult()->DataSet::Pointer {
        return d->result;
    }
    auto Processor::GetOutputFormat() const -> QString {
        return QString();
    }

}
