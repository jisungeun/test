#define LOGGER_TAG "[BasicAnalysisFL]"
#include <TCLogger.h>

#include <memory>

#include <ScalarData.h>
#include <TCMask.h>
#include <TCMeasure.h>
#include <IBaseImage.h>

#include <ICustomAlgorithm.h>
#include <ISegmentationAlgorithm.h>
#include <ParameterRegistry.h>
#include <PluginRegistry.h>

#include "BasicAnalysisFLMetaParameter.h"
#include "BasicAnalysisFLParameter.h"
#include "BasicAnalysisFL.h"

namespace TC::Processor::BasicAnalysis::FL {
    struct Processor::Impl {
        IParameter::Pointer param{ ParameterRegistry::Create(Parameter::GetName()) };
        IMetaParameter::Pointer metaParam{MetaParameterRegistry::Create(MetaParameter::GetName())};
        DataSet::Pointer data;
        DataSet::Pointer result;
        std::shared_ptr<ISegmentationAlgorithm> flThresh{ nullptr };
        std::shared_ptr<ISegmentationAlgorithm> labeling{ nullptr };
        std::shared_ptr<ISegmentationAlgorithm> border_kill{ nullptr };
        std::shared_ptr<ISegmentationAlgorithm> modifylabel{ nullptr };
        std::shared_ptr<ICustomAlgorithm> measure{ nullptr };

        bool isBorder{ false };

        auto DataListToMeausre(DataList::Pointer dl)->TCMeasure::Pointer {
            auto dmeasure = std::make_shared<TCMeasure>();
            for (const auto& dataSet : dl->GetList()) {
                auto name = std::static_pointer_cast<ScalarData>(dataSet->GetData("Name"));

                auto organ_name = name->ValueAsString();
                auto cellcount = std::static_pointer_cast<ScalarData>(dataSet->GetData("CellCount"));

                for (auto j = 0; j < cellcount->ValueAsInt(); j++) {
                    auto real_idx = std::static_pointer_cast<ScalarData>(dataSet->GetData("real_idx" + QString::number(j)))->ValueAsInt();
                    auto volume = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Volume"));
                    dmeasure->AppendMeasure(organ_name + ".Volume", j, volume->ValueAsDouble());
                    auto sa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".SurfaceArea"));
                    dmeasure->AppendMeasure(organ_name + ".SurfaceArea", j, sa->ValueAsDouble());
                    auto mri = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".MeanRI"));
                    dmeasure->AppendMeasure(organ_name + ".MeanRI", j, mri->ValueAsDouble());
                    auto sph = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Sphericity"));
                    dmeasure->AppendMeasure(organ_name + ".Sphericity", j, sph->ValueAsDouble());
                    auto pa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".ProjectedArea"));
                    dmeasure->AppendMeasure(organ_name + ".ProjectedArea", j, pa->ValueAsDouble());
                    auto dm = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Drymass"));
                    dmeasure->AppendMeasure(organ_name + ".Drymass", j, dm->ValueAsDouble());
                    auto ct = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Concentration"));
                    dmeasure->AppendMeasure(organ_name + ".Concentration", j, ct->ValueAsDouble());
                    auto sc = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Score"));
                    dmeasure->AppendMeasure(organ_name + ".Score", j, sc->ValueAsDouble());
                }
            }
            return dmeasure;
        }       
    };
    Processor::Processor() : d{ new Impl } {
        Parameter::Register();
        MetaParameter::Register();
    }
    Processor::~Processor() {
        
    }
    auto Processor::Parameter(const QString& key) -> IParameter::Pointer {
        Q_UNUSED(key)
        return d->param;
    }
    auto Processor::MetaParameter(const QString& key) -> IMetaParameter::Pointer {
        Q_UNUSED(key)
        return d->metaParam;
    }
    auto Processor::SetData(DataSet::Pointer data) -> void {
        d->data = data;
    }
    auto Processor::GetResult() -> DataSet::Pointer {
        return d->result;
    }
    auto Processor::GetOutputFormat() const -> QString {
        //Use FL channel as cell organ
        return QString("Mask!CellInst*Measure!DataList");
    }
    auto Processor::GetLayerName(QString key) -> QStringList {
        Q_UNUSED(key)
        return QStringList();
    }
    auto Processor::Execute() -> bool {
        //use fl threshold binary mask for the measurement
        const auto ulThreshName = d->param->GetValue("UL Threshold").toString();
        const auto labelingName = d->param->GetValue("Labeling").toString();
        const auto borderkillName = d->param->GetValue("Exclude on edges").toString();
        const auto labelmodifyName = d->param->GetValue("Modify Label").toString();
        const auto measureName = d->param->GetValue("Basic Measurement").toString();

        d->flThresh = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(ulThreshName));
        d->labeling = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(labelingName));
        d->modifylabel = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(labelmodifyName));
        d->measure = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(measureName));

        auto threshParam = d->param->GetChild("UL Threshold Parameter");
        auto labelParam = d->param->GetChild("Labeling Parameter");
        auto borderParam = d->param->GetChild("Exclude on edges Parameter");
        auto labelmodifyParam = d->param->GetChild("Modify Label Parameter");
        
        auto riImage = std::dynamic_pointer_cast<IBaseImage>(d->data->GetData(0));
        IBaseImage::Pointer flImage[3]{ nullptr, };
        for(auto i=1;i<d->data->Count();i++) {
            auto tmpImage = std::dynamic_pointer_cast<IBaseImage>(d->data->GetData(i));
            auto ch = tmpImage->getChannel();
            if(ch<3 && ch>-1) {
                flImage[ch] = tmpImage;
            }
        }
        DataSet::Pointer result{ DataSet::New() };

        for(auto i=0;i<3;i++) {
            auto flImg = flImage[i];
            if(nullptr == flImage) {
                continue;
            }
            auto ChName = QString("CH%1").arg(i);
            IBaseMask::Pointer binaryMask{ nullptr };
            IBaseMask::Pointer resultingOrgan{ nullptr };
            IBaseMask::Pointer labeledMask{ nullptr };
            IBaseMask::Pointer killedMask{ nullptr };
            IBaseMask::Pointer modifiedMask{ nullptr };
            if (!d->flThresh->Parameter(threshParam)) {
                QLOG_ERROR() << "ul threshold setting failed";
                return false;
            }
            try {
                d->flThresh->SetInput(flImg);
                if(!d->flThresh->Execute()) {
                    QLOG_ERROR() << "UL Threshold failed";
                }
                binaryMask = std::dynamic_pointer_cast<TCMask>(d->flThresh->GetOutput(0));
                resultingOrgan = std::dynamic_pointer_cast<TCMask>(d->flThresh->GetOutput(1));
            }catch(...) {
                QLOG_ERROR() << "exception occurred during UL threshold";
                return false;
            }
            if(!d->labeling->Parameter(labelParam)) {
                QLOG_ERROR() << "labeling parameter setting failed";
                return false;
            }
            try {
                d->labeling->SetInput(binaryMask);
                if (!d->labeling->Execute()) {
                    QLOG_ERROR() << "perform labeling failed";
                }
                binaryMask = nullptr;
                labeledMask = std::dynamic_pointer_cast<TCMask>(d->labeling->GetOutput());
            }
            catch (...) {
                QLOG_ERROR() << "exception occured during labeling";
                return false;
            }
            if(d->isBorder) {
                if (nullptr == d->border_kill) {
                    d->border_kill = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(borderkillName));
                }
                if(!d->border_kill->Parameter(borderParam)) {
                    QLOG_ERROR() << "borderkill parameter setting failed";
                    return false;
                }
                try {
                    d->border_kill->SetInput(labeledMask);
                    if(!d->border_kill->Execute()) {
                        QLOG_ERROR() << "perform borderkill failed";
                    }
                    labeledMask = nullptr;
                    killedMask = std::dynamic_pointer_cast<TCMask>(d->border_kill->GetOutput());
                    result->AppendData(killedMask, ChName + "_FLMask");
                }catch(...) {
                    QLOG_ERROR() << "exception occured during borderKill";
                    return false;
                }
            }
            if(!d->modifylabel->Parameter(labelmodifyParam)) {
                QLOG_ERROR() << "label modification parameter setting failed";
                return false;
            }
            try {
                if(d->isBorder) {
                    d->modifylabel->SetInput(killedMask);
                }else {
                    d->modifylabel->SetInput(labeledMask);
                }
                if(!d->modifylabel->Execute()) {
                    QLOG_ERROR() << "perform label modification failed";
                }
                killedMask = nullptr;
                labeledMask = nullptr;
                modifiedMask = std::dynamic_pointer_cast<TCMask>(d->modifylabel->GetOutput());
            }catch(...) {
                QLOG_ERROR() << "exception occured during label modification";
                return false;
            }                        
            auto chParam = d->param->GetChild("Basic Measurement Parameter!" + ChName);
            d->measure->Parameter(chParam);

            try {
                d->measure->SetInput(modifiedMask);
                d->measure->SetInput2(riImage);
                d->measure->SetInput3(resultingOrgan);
                if(!d->measure->Execute()) {
                    QLOG_ERROR() << "performing measurement failed";
                    return false;
                }
                auto measureRaw = std::dynamic_pointer_cast<DataList>(d->measure->GetOutput());
                auto measureResult = d->DataListToMeausre(measureRaw);
                result->AppendData(measureResult, ChName + "_Measure");
            }catch(...) {
                QLOG_ERROR() << "exception occured during basic measurement";
                return false;
            }
        }

        d->result = result;

        return true;
    }
    auto Processor::FreeMemory() -> void {
        d->measure->FreeMemory();
        if(d->isBorder) {
            d->border_kill->FreeMemory();
        }
        d->flThresh->FreeMemory();
        d->labeling->FreeMemory();
        d->modifylabel->FreeMemory();
    }

}