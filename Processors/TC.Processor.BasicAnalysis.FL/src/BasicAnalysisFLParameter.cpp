#include <QJsonArray>
#include <ParameterRegistry.h>
#include "BasicAnalysisFLParameter.h"

namespace TC::Processor::BasicAnalysis::FL {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(GetName(), CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.1.0");
        SetFullName(QString(GetName().c_str()));
        //Processing parameter nodes
        RegisterNode("Ch 1 intensity", "Ch1 intensity", "algorithms/segmentation/TC_Algorithm_segmentation_ManualThreshold.dll", "0", "org.tomocube.algorithm.masking.ManualThreshold", "", "");
        RegisterNode("Ch 2 intensity", "Ch2 intensity", "algorithms/segmentation/TC_Algorithm_segmentation_ManualThreshold.dll", "1", "org.tomocube.algorithm.masking.ManualThreshold", "", "");
        RegisterNode("Ch 3 intensity", "Ch3 intensity", "algorithms/segmentation/TC_Algorithm_segmentation_ManualThreshold.dll", "2", "org.tomocube.algorithm.masking.ManualThreshold", "", "");
        RegisterNode("Labeling", "Labeling algorithm", "algorithms/labeling/TC_Algorithm_segmentation_labeling.dll", "3", "org.tomocube.algorithm.labeling.basic", "", "");
        RegisterNode("Exclude on edges", "Border kill algorithm", "algorithms/masking/TC_Algorithm_segmentation_BorderKill.dll", "4", "org.tomocube.algorithm.postprocessing.borderkill", "", "");
        RegisterNode("Modify Label","Label modification algorithm","algorithms/labeling/TC_Algorithm_segmentation_modifylabel.dll","5","org.tomocube.algorithm.labeling.modification","","");
        RegisterNode("Basic Measurement", "Basic measurement algorithm", "algorithms/measurement/TC_Algorithm_measurement_basic.dll", "6", "org.tomocube.algorithm.measurement.basic", "", "");
        RegisterNode("ProcSelection", "Select target channel", "", "7", QJsonArray{ false,false,false }, "", "");
        RegisterNode("DupNames", "Channel names", "", "8", QJsonArray{ "Ch 1","Ch 2","Ch 3" }, "", "");
        RegisterChild("Ch 1 intensity Parameter", "Parameter for manual Ch1Intensity algorithm", ParameterRegistry::Create("org.tomocube.algorithm.masking.ManualThreshold"));
        RegisterChild("Ch 2 intensity Parameter", "Parameter for manual Ch2Intensity algorithm", ParameterRegistry::Create("org.tomocube.algorithm.masking.ManualThreshold"));
        RegisterChild("Ch 3 intensity Parameter", "Parameter for manual Ch3Intensity algorithm", ParameterRegistry::Create("org.tomocube.algorithm.masking.ManualThreshold"));
        RegisterChild("Labeling Parameter", "Parameter for labeling algorithm", ParameterRegistry::Create("org.tomocube.algorithm.labeling.basic"));
        RegisterChild("Exclude on edges Parameter", "Parameter for border kill", ParameterRegistry::Create("org.tomocube.algorithm.postprocessing.borderkill"));
        RegisterChild("Modify Label Parameter", "Parameter for label modification", ParameterRegistry::Create("org.tomocube.algorithm.labeling.modification"));
        RegisterChild("Basic Measurement Parameter!CH0", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
        RegisterChild("Basic Measurement Parameter!CH1", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
        RegisterChild("Basic Measurement Parameter!CH2", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
    }
}