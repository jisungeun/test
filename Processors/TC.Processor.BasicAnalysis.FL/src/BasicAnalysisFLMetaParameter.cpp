#include <ParameterRegistry.h>
#include "BasicAnalysisFLMetaParameter.h"

namespace TC::Processor::BasicAnalysis::FL {
    auto MetaParameter::Register() -> void {
        static bool s_registered = MetaParameterRegistry::Register(GetName(), CreateMethod);
    }
    MetaParameter::MetaParameter() : IMetaParameter() {
        SetFullName(QString(GetName().c_str()));
        //Meta parameter nodes
        RegisterDuplicator("Duplicator", QStringList{ "Ch 1","Ch 2","Ch 3"}, "Basic Measurement");
        RegisterFixer("Fix1", "", "Labeling", "Method", true);
        RegisterHider("Hider1", QStringList{"Labeling","Modify Label"});
        RegisterSelector("ChSelect","ProcSelection", SEL_CHECK_BOX, 3,QStringList{"Ch 1 intensity","Ch 2 intensity","Ch 3 intensity"},QStringList{"CH1","CH2","CH3"});
        RegisterSorter("Order", QStringList{ "ProcSelection","Ch 1 intensity","Ch 2 intensity","Ch 3 intensity","Labeling","Exclude on edges","Modify Label","Basic Measurement" });
    }
}