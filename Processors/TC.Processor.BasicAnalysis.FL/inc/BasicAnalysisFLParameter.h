#pragma once

#include <IParameter.h>

namespace TC::Processor::BasicAnalysis::FL {
    class Parameter :public IParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static Pointer CreateMethod() {
            return std::make_shared<Parameter>();
        }

        static std::string GetName() { return "org.tomocube.processor.basicanalysis.fl"; }

        Parameter();
    };
}