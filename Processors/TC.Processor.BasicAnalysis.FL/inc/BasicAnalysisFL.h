#pragma once

#include <QObject>
#include <IProcessor.h>

#include "TC_Processor_BasicAnalysis_FLExport.h"

namespace TC::Processor::BasicAnalysis::FL {
	class TC_Processor_BasicAnalysis_FL_API Processor
		: public QObject
		, public IProcessor {
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.processor.basicanalysis.fl")
		Q_INTERFACES(IProcessor)
	public:
		Processor();
		virtual ~Processor();

		auto GetName() const -> QString override { return "FL Thresholding"; }
		auto GetFullName() const -> QString override { return "org.tomocube.processor.basicanalysis.fl"; }
		auto GetDescription() const -> QString override { return "Basic Analysis based on FL threshold segmentation"; }
		auto clone() const -> IPluginModule* override { return new Processor(); }

		auto FreeMemory() -> void override;
		auto Parameter(const QString& key)->IParameter::Pointer override;
		auto MetaParameter(const QString& key)->IMetaParameter::Pointer override;
		auto SetData(DataSet::Pointer data) -> void override;
		auto Execute() -> bool override;
		auto GetOutputFormat() const->QString override;
		auto GetLayerName(QString key)->QStringList override;
		auto GetResult()->DataSet::Pointer override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}