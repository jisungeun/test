#pragma once

#include <IMetaParameter.h>

namespace TC::Processor::BasicAnalysis::FL {
    class MetaParameter : public IMetaParameter {
        Q_OBJECT
    public:
        static auto Register()->void;
        static auto CreateMethod()->Pointer {
            return std::make_shared<MetaParameter>();
        }
        static auto GetName()->std::string { return "org.tomocube.processor.basicanalysis.fl"; }

        MetaParameter();
    };
}