#define LOGGER_TAG "[BasicAnalysisAi]"
#include <TCLogger.h>

#include <iostream>
#include <memory>

#include <IBaseImage.h>
#include <ScalarData.h>
#include <TCImage.h>
#include <TCMeasure.h>
#include <TCMask.h>

#include <ISegmentationAlgorithm.h>
#include <ICustomAlgorithm.h>

#include <ParameterRegistry.h>
#include <PluginRegistry.h>

#include "BasicAnalysisAiMetaParameter.h"
#include "BasicAnalysisAiParameter.h"
#include "BasicAnalysisAi.h"

namespace TC::Processor::BasicAnalysis::Ai {
    struct Processor::Impl {
        IParameter::Pointer param{ ParameterRegistry::Create(Parameter::GetName()) };
        IMetaParameter::Pointer metaParam{ MetaParameterRegistry::Create(MetaParameter::GetName()) };
        DataSet::Pointer data;
        DataSet::Pointer result;
        std::shared_ptr<ISegmentationAlgorithm> ai_inference;
        std::shared_ptr<ISegmentationAlgorithm> border_kill;
        std::shared_ptr<ICustomAlgorithm> measure;
        auto DataListToMeausre(DataList::Pointer dl)->TCMeasure::Pointer {
            auto dmeasure = std::make_shared<TCMeasure>();
            for(const auto &dataSet : dl->GetList()) {
                auto name = std::static_pointer_cast<ScalarData>(dataSet->GetData("Name"));
                
                auto organ_name = name->ValueAsString();
                auto cellcount = std::static_pointer_cast<ScalarData>(dataSet->GetData("CellCount"));                
                
                for(auto j=0;j<cellcount->ValueAsInt();j++) {
                    auto real_idx = std::static_pointer_cast<ScalarData>(dataSet->GetData("real_idx" + QString::number(j)))->ValueAsInt();
                    auto volume = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Volume"));
                    dmeasure->AppendMeasure(organ_name + ".Volume", j, volume->ValueAsDouble());
                    auto sa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".SurfaceArea"));
                    dmeasure->AppendMeasure(organ_name + ".SurfaceArea", j, sa->ValueAsDouble());
                    auto mri = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".MeanRI"));
                    dmeasure->AppendMeasure(organ_name + ".MeanRI", j, mri->ValueAsDouble());
                    auto sph = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Sphericity"));
                    dmeasure->AppendMeasure(organ_name + ".Sphericity", j, sph->ValueAsDouble());
                    auto pa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".ProjectedArea"));
                    dmeasure->AppendMeasure(organ_name + ".ProjectedArea", j, pa->ValueAsDouble());
                    auto dm = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Drymass"));
                    dmeasure->AppendMeasure(organ_name + ".Drymass", j, dm->ValueAsDouble());
                    auto ct = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Concentration"));
                    dmeasure->AppendMeasure(organ_name + ".Concentration", j, ct->ValueAsDouble());
                    auto sc = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Score"));
                    dmeasure->AppendMeasure(organ_name + ".Score", j, sc->ValueAsDouble());
                }                
            }
            return dmeasure;
        }
    };
    Processor::Processor() :d{ new Impl } {
        Parameter::Register();
        MetaParameter::Register();
    }
    Processor::~Processor() {

    }
    auto Processor::Parameter(const QString& key) -> IParameter::Pointer {
        Q_UNUSED(key)
        return d->param;
    }
    auto Processor::MetaParameter(const QString& key) -> IMetaParameter::Pointer {
        Q_UNUSED(key)
        return d->metaParam;
    }
    auto Processor::SetData(DataSet::Pointer data) -> void {
        d->data = data;
    }
    auto Processor::Execute()->bool {
        const auto aiSegAlgo = d->param->GetValue("AI Inference").toString();
        const auto borderKillAlgo = d->param->GetValue("Exclude on edges").toString();
        const auto measureAlgo = d->param->GetValue("Basic Measurement").toString();
       
        d->ai_inference = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(aiSegAlgo));        
        auto aiParam = d->param->GetChild("AI Inference Parameter");        
        auto borderKillParam = d->param->GetChild("Exclude on edges Parameter");

        auto isBorderKill = borderKillParam->GetValue("Method").toBool();

        if(!d->ai_inference->Parameter(aiParam)) {
            QLOG_ERROR() << "ai parameter setting failed";
        }                
        IBaseMask::Pointer resulting_org;
        IBaseMask::Pointer resulting_inst;        
        auto image = std::dynamic_pointer_cast<TCImage>(d->data->GetData(0));
        try {            
            d->ai_inference->SetInput(image);
            if(!d->ai_inference->Execute()) {
                QLOG_ERROR() << "performing ai failed";
                //return false;
            }            
            resulting_org = d->ai_inference->GetOutput(1);
            resulting_inst =d->ai_inference->GetOutput(0);

            /*if (!isBorderKill) {
                result->AppendData(resulting_inst, "CellInst");
            }
            result->AppendData(resulting_org,"CellOrgan");*/
        }catch(...){
            QLOG_ERROR() << "exception in ai segmentation";
            return false;
        }
        IBaseMask::Pointer killed_inst;
        if (isBorderKill) {
            d->border_kill = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(borderKillAlgo));

            if (!d->border_kill->Parameter(borderKillParam)) {
                QLOG_ERROR() << "border kill parameter setting failed";
            }            
            d->border_kill->SetInput(resulting_inst);
            d->border_kill->Execute();

            killed_inst = std::dynamic_pointer_cast<TCMask>(d->border_kill->GetOutput(0));
            //result->AppendData(killed_inst, "CellInst");
        }  

        DataSet::Pointer result{ DataSet::New() };
        if(d->border_kill) {
            result->AppendData(killed_inst, "CellInst");
        }else {
            result->AppendData(resulting_inst, "CellInst");
        }
        result->AppendData(resulting_org, "CellOrgan");

        d->measure = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(measureAlgo));

        if(d->param->ExistNode("Mask Selector")) {
            auto div = d->param->GetValue("Mask Selector").toString().split("!");
            QStringList dupNames;
            for(auto di : div) {
                if(!di.contains("*")) {
                    dupNames.push_back(di);
                }
            }
            d->measure->DuplicateParameter(dupNames);
            for(auto du:dupNames) {
                auto dup_param = d->param->GetChild("Basic Measurement Parameter!"+du);
                d->measure->Parameter(dup_param,du);
            }
        }
        else if(d->metaParam->GetDuplicatorNames().count() > 0){
            auto dupNodeName = d->metaParam->GetDuplicatorNames()[0];
            auto div = std::get<0>(d->metaParam->GetDuplicator(dupNodeName));
            d->measure->DuplicateParameter(div);
            for(auto di:div) {
                auto dup_param = d->param->GetChild("Basic Measurement Parameter!" + di);
                d->measure->Parameter(dup_param, di);
            }
        }
        else {
            auto measureParam = d->param->GetChild("Basic Measurement Parameter");
            d->measure->Parameter(measureParam);
        }
        try{
            if (isBorderKill) {
                d->measure->SetInput(killed_inst);
            }
            else {
                d->measure->SetInput(resulting_inst);
            }
            d->measure->SetInput2(image);
            d->measure->SetInput3(resulting_org);            
            if (!d->measure->Execute()) {
                QLOG_ERROR() << "performing measurement failed";
                return false;
            }
            auto measureRaw =  std::dynamic_pointer_cast<DataList>(d->measure->GetOutput());
            auto measureResult = d->DataListToMeausre(measureRaw);
            result->AppendData(measureResult, "Measure");

            d->result = result;            
        }catch(...) {
            QLOG_ERROR() << "exception in basic measure";
            return false;
        }
        
        return true;
    }
    auto Processor::GetLayerName(QString key) -> QStringList {
        const auto aiSegAlgo = d->param->GetValue("AI Inference").toString();        

        auto AiSeg = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(aiSegAlgo));
        return AiSeg->GetLayerNames(key);
    }
    auto Processor::GetResult()->DataSet::Pointer {
        return d->result;
    }
    auto Processor::GetOutputFormat() const -> QString {
        return QString("Mask!CellInst*Mask!CellOrgan*Measure!DataList");
    }
    auto Processor::FreeMemory() -> void {
        d->ai_inference->FreeMemory();
        if (d->border_kill) {
            d->border_kill->FreeMemory();
        }
        d->measure->FreeMemory();
    }

}
