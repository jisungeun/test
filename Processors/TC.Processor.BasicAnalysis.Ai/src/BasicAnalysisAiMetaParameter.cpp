#include <ParameterRegistry.h>
#include "BasicAnalysisAiMetaParameter.h"

namespace TC::Processor::BasicAnalysis::Ai {
	auto MetaParameter::Register()->void {
		static bool s_registered = MetaParameterRegistry::Register(MetaParameter::GetName(), MetaParameter::CreateMethod);
	}
	MetaParameter::MetaParameter() :IMetaParameter() {		
		SetFullName(QString(GetName().c_str()));
		RegisterDuplicator("Duplicator", QStringList{ "membrane","nucleus","nucleoli","lipid droplet" },"Basic Measurement");
		RegisterHighlighter("Highlight1", "Default", "AI Inference");
		RegisterHighlighter("Highlight2", "AI Inference", "Basic Measurement");
		RegisterHighlighter("HighLight3", "Exclude on edges", "Basic Measurement");
		RegisterHighlighter("Highlight4", "Basic Measurement", "Apply Parameter");
		RegisterSorter("Order", QStringList{"AI Inference","Exclude on edges","Basic Measurement" });
		RegisterDefaultValue("Default1", "lipid droplet", "Basic Measurement", "Preset", 2);
		RegisterDefaultValue("Default2", "lipid droplet", "Basic Measurement", "RII", 0.135);
	}
};