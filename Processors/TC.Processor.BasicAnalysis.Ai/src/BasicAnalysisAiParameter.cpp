#include <ParameterRegistry.h>
#include "BasicAnalysisAiParameter.h"

namespace TC::Processor::BasicAnalysis::Ai{
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter()
        :IParameter(){
        SetVersion("1.1.0");
        SetFullName(QString(GetName().c_str()));
        RegisterNode("AI Inference", "Ai segmentation algorithm", "algorithms/ai/TC_Algorithm_segmentation_Unified.dll", "0", "org.tomocube.algorithm.masking.ai.unified", "", "");
        RegisterNode("Exclude on edges", "Border kill algorithm", "algorithms/masking/TC_Algorithm_segmentation_BorderKill.dll", "1", "org.tomocube.algorithm.postprocessing.borderkill", "", "");
        RegisterNode("Basic Measurement", "Basic measurement algorithm", "algorithms/measurement/TC_Algorithm_measurement_basic.dll", "2", "org.tomocube.algorithm.measurement.basic", "", "");          

        RegisterChild("AI Inference Parameter", "Parameter for ai algorithm", ParameterRegistry::Create("org.tomocube.algorithm.masking.ai.unified"));

        RegisterChild("Exclude on edges Parameter", "Parameter for border kill", ParameterRegistry::Create("org.tomocube.algorithm.postprocessing.borderkill"));

        RegisterChild("Basic Measurement Parameter!membrane", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
        RegisterChild("Basic Measurement Parameter!lipid droplet", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
        RegisterChild("Basic Measurement Parameter!nucleus", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));
        RegisterChild("Basic Measurement Parameter!nucleoli", "Parameter for basic meausrement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));    
    }
}