#define LOGGER_TAG "[BasicAnalysisRi]"
#include <TCLogger.h>
#include <iostream>
#include <memory>

#include <ScalarData.h>
#include <TCMask.h>
#include <TCMeasure.h>
#include <IBaseImage.h>

#include <ICustomAlgorithm.h>
#include <ISegmentationAlgorithm.h>
#include <ParameterRegistry.h>
#include <PluginRegistry.h>

#include "BasicAnalysisRiMetaParameter.h"
#include "BasicAnalysisRiParameter.h"
#include "BasicAnalysisRi.h"

namespace TC::Processor::BasicAnalysis::Ri {
    struct Processor::Impl {
        IParameter::Pointer param{ParameterRegistry::Create(Parameter::GetName())};
        IMetaParameter::Pointer metaParam{ MetaParameterRegistry::Create(MetaParameter::GetName()) };
        DataSet::Pointer data;
        DataSet::Pointer result;
        std::shared_ptr<ICustomAlgorithm> otsu{nullptr};
        std::shared_ptr<ISegmentationAlgorithm> ulthresh{nullptr};
        std::shared_ptr<ISegmentationAlgorithm> labeling{nullptr};
        std::shared_ptr<ISegmentationAlgorithm> border_kill{ nullptr };
        std::shared_ptr<ICustomAlgorithm> measure{nullptr};        

        bool isOtsu{ false };
        bool isLabeling{ false };
        bool isBorder{ false };
                
        auto DataListToMeausre(DataList::Pointer dl)->TCMeasure::Pointer {
            auto dmeasure = std::make_shared<TCMeasure>();
            for(const auto &dataSet : dl->GetList()) {
                auto name = std::static_pointer_cast<ScalarData>(dataSet->GetData("Name"));
                
                auto organ_name = name->ValueAsString();
                auto cellcount = std::static_pointer_cast<ScalarData>(dataSet->GetData("CellCount"));                
                
                for(auto j=0;j<cellcount->ValueAsInt();j++) {
                    auto real_idx = std::static_pointer_cast<ScalarData>(dataSet->GetData("real_idx" + QString::number(j)))->ValueAsInt();
                    auto volume = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Volume"));
                    dmeasure->AppendMeasure(organ_name + ".Volume", j, volume->ValueAsDouble());
                    auto sa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".SurfaceArea"));
                    dmeasure->AppendMeasure(organ_name + ".SurfaceArea", j, sa->ValueAsDouble());
                    auto mri = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".MeanRI"));
                    dmeasure->AppendMeasure(organ_name + ".MeanRI", j, mri->ValueAsDouble());
                    auto sph = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Sphericity"));
                    dmeasure->AppendMeasure(organ_name + ".Sphericity", j, sph->ValueAsDouble());
                    auto pa = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".ProjectedArea"));
                    dmeasure->AppendMeasure(organ_name + ".ProjectedArea", j, pa->ValueAsDouble());
                    auto dm = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Drymass"));
                    dmeasure->AppendMeasure(organ_name + ".Drymass", j, dm->ValueAsDouble());
                    auto ct = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Concentration"));
                    dmeasure->AppendMeasure(organ_name + ".Concentration", j, ct->ValueAsDouble());
                    auto sc = std::static_pointer_cast<ScalarData>(dataSet->GetData("cell" + QString::number(real_idx) + ".Score"));
                    dmeasure->AppendMeasure(organ_name + ".Score", j, sc->ValueAsDouble());
                }                
            }
            return dmeasure;
        }
    };
    Processor::Processor() : d{new Impl} {
        Parameter::Register();
        MetaParameter::Register();
    }
    Processor::~Processor() {
        
    }
    auto Processor::Parameter(const QString& key) -> IParameter::Pointer {
        Q_UNUSED(key)
        return d->param;
    }
    auto Processor::MetaParameter(const QString& key) -> IMetaParameter::Pointer {
        Q_UNUSED(key)
        return d->metaParam;
    }
    auto Processor::SetData(DataSet::Pointer data) -> void {
        d->data = data;
    }
    auto Processor::GetResult() -> DataSet::Pointer {
        return d->result;
    }
    auto Processor::GetOutputFormat() const -> QString {
        return QString("Mask!CellInst*Mask!CellOrgan*Measure!DataList");
    }
    auto Processor::GetLayerName(QString key) -> QStringList {        
       const auto ulThreshName = d->param->GetValue("UL Threshold").toString();
        auto ulThreshAlgo = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(ulThreshName));
        
        return ulThreshAlgo->GetLayerNames(key);
    }

    auto Processor::Execute() -> bool {
        const auto otsuName = d->param->GetValue("Auto Threshold").toString();
        const auto ulThreshName = d->param->GetValue("UL Threshold").toString();
        const auto labelingName = d->param->GetValue("Labeling").toString();
        const auto borderkillName = d->param->GetValue("Exclude on edges").toString();
        const auto measureName = d->param->GetValue("Basic Measurement").toString();

        
        d->ulthresh = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(ulThreshName));        
        d->measure = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(measureName));        

        auto otsuParam = d->param->GetChild("Auto Threshold Parameter");
        auto ulThreshParam = d->param->GetChild("UL Threshold Parameter");
        auto labelParam = d->param->GetChild("Labeling Parameter");
        auto borderParam = d->param->GetChild("Exclude on edges Parameter");
        auto measureParam = d->param->GetChild("Basic Measurement Parameter");

        if(otsuParam->ExistNode("Method!Enabler")) {
            d->isOtsu = otsuParam->GetValue("Method!Enabler").toBool();
        }else {
            d->isOtsu = otsuParam->GetValue("Method").toBool();
        }
        if(labelParam->ExistNode("Method!Enabler")) {
            d->isLabeling = labelParam->GetValue("Method!Enabler").toBool();
        }else {
            d->isLabeling = labelParam->GetValue("Method").toBool();
        }
        if(borderParam->ExistNode("Method!Enabler")) {
            d->isBorder = borderParam->GetValue("Method!Enabler").toBool();
        }else {
            d->isBorder = borderParam->GetValue("Method").toBool();
        }        

        auto image = std::dynamic_pointer_cast<IBaseImage>(d->data->GetData(0));

        IBaseMask::Pointer resultingInst{nullptr};
        IBaseMask::Pointer resultingOrgan{nullptr};
        IBaseMask::Pointer labeledInst{nullptr};
        IBaseMask::Pointer killedInst{ nullptr };
        if(d->isOtsu) {
            d->otsu = std::dynamic_pointer_cast<ICustomAlgorithm>(PluginRegistry::GetPlugin(otsuName));
            if(!d->otsu->Parameter(otsuParam)) {
                QLOG_ERROR() << "otsu parameter setting failed";
                return false;
            }
            try {
                d->otsu->SetInput(image);
                d->otsu->Execute();
                auto otsuResult = std::dynamic_pointer_cast<DataList>(d->otsu->GetOutput());
                auto minotsu = -1.0;
                for(auto o : otsuResult->GetList()) {
                    auto o_val = std::dynamic_pointer_cast<ScalarData>(o->GetData())->ValueAsFloat();
                    if(minotsu < 0.0 || minotsu > o_val) {
                        minotsu = o_val;
                    }                    
                }                
                ulThreshParam->SetValue("LThreshold",minotsu);
                ulThreshParam->SetValue("UThreshold", 2.0);//max value
            }catch(...) {
                QLOG_ERROR() << "exception occur during otsu segmentation";
            }
        }
        if(!d->ulthresh->Parameter(ulThreshParam)) {
            QLOG_ERROR() << "ul threshold setting failed";
            return false;
        }
        try {
            d->ulthresh->SetInput(image);
            if(!d->ulthresh->Execute()) {
                QLOG_ERROR() << "UL Threshold failed";
                //return false; //instead of return false, create empty masks
            }
            resultingInst = std::dynamic_pointer_cast<TCMask>(d->ulthresh->GetOutput(0));
            resultingOrgan = std::dynamic_pointer_cast<TCMask>(d->ulthresh->GetOutput(1));
        }catch(...) {
            QLOG_ERROR() << "exception occurred during UL threshold";
            return false;
        }
        if(d->isLabeling) {
            d->labeling = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(labelingName));
            if(!d->labeling->Parameter(labelParam)) {
                QLOG_ERROR() << "labeling parameter setting failed";
                return false;
            }
            try{
                d->labeling->SetInput(resultingInst);
                if(!d->labeling->Execute()) {
                    QLOG_ERROR() << "perform labeling failed";
                    //return false;//instead of return false, create empty masks
                }                
                resultingInst = nullptr;
                labeledInst = std::dynamic_pointer_cast<TCMask>(d->labeling->GetOutput());
            }catch(...) {
                QLOG_ERROR() << "exception occured during labeling";
                return false;
            }
        }

        if (d->isBorder) {
            d->border_kill = std::dynamic_pointer_cast<ISegmentationAlgorithm>(PluginRegistry::GetPlugin(borderkillName));
            if(!d->border_kill->Parameter(borderParam)) {
                QLOG_ERROR() << "borderkill parameter setting failed";
                return false;
            }
            try {
                if(d->isLabeling) {
                    d->border_kill->SetInput(labeledInst);
                }else {
                    d->border_kill->SetInput(resultingInst);
                }                
                if(!d->border_kill->Execute()) {
                    QLOG_ERROR() << "perform borderkill failed";
                }
                resultingInst = nullptr;
                labeledInst = nullptr;
                killedInst = std::dynamic_pointer_cast<TCMask>(d->border_kill->GetOutput());
            }catch(...) {
                QLOG_ERROR() << "exception occured during borderKill";
                return false;
            }
        }

        DataSet::Pointer result{ DataSet::New() };
        if (d->isBorder) {
            result->AppendData(killedInst, "CellInst");//append mask data
        }
        else if(d->isLabeling){
            result->AppendData(labeledInst,"CellInst");//append mask data
        }else {
            result->AppendData(resultingInst,"CellInst");
        }
        result->AppendData(resultingOrgan,"CellOrgan");
        if(!d->measure->Parameter(measureParam)) {
            QLOG_ERROR() << "measure parameter setting failed";
            return false;
        }
        try {
            if (d->isBorder) {
                d->measure->SetInput(killedInst);
            }
            else if(d->isLabeling){
                d->measure->SetInput(labeledInst);
            }else {
                d->measure->SetInput(resultingInst);
            }
            d->measure->SetInput2(image);
            d->measure->SetInput3(resultingOrgan);
            if(!d->measure->Execute()) {
                QLOG_ERROR() << "performing measurement failed";
                return false;
            }
            auto measureRaw =  std::dynamic_pointer_cast<DataList>(d->measure->GetOutput());
            auto measureResult = d->DataListToMeausre(measureRaw);
            result->AppendData(measureResult,"Measure");

            d->result = result;
        }catch(...) {
            QLOG_ERROR() << "exception occured during basic measurement";
            return false;
        }

        return true;
    }
    auto Processor::FreeMemory() -> void {        
        d->measure->FreeMemory();
        if (d->isLabeling) {
            d->labeling->FreeMemory();
        }
        d->ulthresh->FreeMemory();
        if (d->isOtsu) {
            d->otsu->FreeMemory();
        }
        if (d->isBorder) {
            d->border_kill->FreeMemory();
        }
        d->result->Clear();
    }

}
