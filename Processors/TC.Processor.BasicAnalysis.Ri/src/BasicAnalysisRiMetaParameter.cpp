#include <ParameterRegistry.h>
#include "BasicAnalysisRiMetaParameter.h"

namespace TC::Processor::BasicAnalysis::Ri {
    auto MetaParameter::Register() -> void {
        static bool s_registered = MetaParameterRegistry::Register(MetaParameter::GetName(), MetaParameter::CreateMethod);
    }
    MetaParameter::MetaParameter() : IMetaParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterConnector("Connector1", QStringList{ "LThreshold","UThreshold" }, "Auto Threshold", "UL Threshold");        
        RegisterHighlighter("Highlight1",  "Default", "UL Threshold");
        RegisterHighlighter("Highlight2",  "UL Threshold", "Basic Measurement");
        RegisterHighlighter("Highlight3",  "Auto Threshold", "UL Threshold");
        RegisterHighlighter("Highlight4",  "Labeling", "Basic Measurement");
        RegisterHighlighter("Highlight5",  "Exclude on edges", "Basic Measurement");
        RegisterHighlighter("Highlight6",  "Basic Measurement", "Apply Parameter");
        RegisterSorter("Order", QStringList{ "UL Threshold","Auto Threshold","Labeling","Exclude on edges","Basic Measurement" });
    }
}