#include <ParameterRegistry.h>
#include "BasicAnalysisRiParameter.h"

namespace TC::Processor::BasicAnalysis::Ri {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(),Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.1.0");
        SetFullName(QString(GetName().c_str()));
        RegisterNode("Auto Threshold", "Auto threshold algorithm", "algorithms/segmentation/TC_Algorithm_segmentation_Threshold.dll", "1", "org.tomocube.algorithm.value.threshold", "", "");
        RegisterNode("UL Threshold", "UL Threshold algorithm", "algorithms/segmentation/TC_Algorithm_segmentation_ManualThreshold.dll", "0", "org.tomocube.algorithm.masking.ManualThreshold", "", "");
        RegisterNode("Labeling", "Labeling algorithm", "algorithms/labeling/TC_Algorithm_segmentation_labeling.dll", "2", "org.tomocube.algorithm.labeling.basic", "", "");
        RegisterNode("Exclude on edges", "Border kill algorithm", "algorithms/masking/TC_Algorithm_segmentation_BorderKill.dll", "3", "org.tomocube.algorithm.postprocessing.borderkill", "", "");
        RegisterNode("Basic Measurement", "Basic measurement algorithm", "algorithms/measurement/TC_Algorithm_measurement_basic.dll", "4", "org.tomocube.algorithm.measurement.basic", "", "");
        
        RegisterChild("Auto Threshold Parameter", "Parameter for otsu algorithm", ParameterRegistry::Create("org.tomocube.algorithm.value.threshold"));
        RegisterChild("UL Threshold Parameter", "Parameter for manual threshold algorithm", ParameterRegistry::Create("org.tomocube.algorithm.masking.ManualThreshold"));
        RegisterChild("Labeling Parameter", "Parameter for labeling algorithm", ParameterRegistry::Create("org.tomocube.algorithm.labeling.basic"));
        RegisterChild("Exclude on edges Parameter", "Parameter for border kill", ParameterRegistry::Create("org.tomocube.algorithm.postprocessing.borderkill"));
        RegisterChild("Basic Measurement Parameter", "Parameter for basic measurement", ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic"));       
    }
}