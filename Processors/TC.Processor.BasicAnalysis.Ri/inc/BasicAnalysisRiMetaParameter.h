#pragma once

#include <IMetaParameter.h>

namespace TC::Processor::BasicAnalysis::Ri {
    class MetaParameter : public IMetaParameter {
        Q_OBJECT
    public:
        static auto Register()->void;
        static auto CreateMethod()->IMetaParameter::Pointer {
            return std::make_shared<MetaParameter>();
        }
        static auto GetName()->std::string { return "org.tomocube.processor.basicanalysis.ri"; }

        MetaParameter();
    };
}