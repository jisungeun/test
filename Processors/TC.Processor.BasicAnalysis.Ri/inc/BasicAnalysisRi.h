#pragma once

#include <QObject>
#include <IProcessor.h>

#include "TC_Processor_BasicAnalysis_RiExport.h"

namespace TC::Processor::BasicAnalysis::Ri {
    class TC_Processor_BasicAnalysis_Ri_API Processor
        : public QObject
        , public IProcessor {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.processor.basicanalysis.ri")
        Q_INTERFACES(IProcessor)
    public:
        Processor();
        virtual ~Processor();

        auto GetName() const -> QString override {return "RI Thresholding";}
        auto GetFullName() const -> QString override {return "org.tomocube.processor.basicanalysis.ri";}
        auto GetDescription() const -> QString override {return "Basic Analysis based on RI threshold segmentation";}
        auto clone() const -> IPluginModule* override {return new Processor();}

        auto FreeMemory() -> void override;
        auto Parameter(const QString& key) -> IParameter::Pointer override;
        auto MetaParameter(const QString& key) -> IMetaParameter::Pointer override;
        auto SetData(DataSet::Pointer data) -> void override;
        auto Execute() -> bool override;
        auto GetOutputFormat() const -> QString override;
        auto GetLayerName(QString key) -> QStringList override;
        auto GetResult() -> DataSet::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}