#include <ParameterRegistry.h>
#include "BasicAnalysisAutoParameter.h"

namespace TC::Processor::BasicAnalysis::Auto {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter(){
        //unit에 binary 인지 multi-label인지 flag를 추가 할 수 있도록
        //외부에서 custom algorithm 노드를 설정하지 않는다면 default로 사용할 정보
        RegisterNode("Auto Segmentation", "Auto segmentation algorithm", "Automatic segmentation algorithm", "0", "org.tomocube.algorithm.masking.threshold", "", "");
        RegisterNode("Labeling", "Labeling algorithm", "Automatic labeling algorithm", "1", "org.tomocube.algorithm.labeling.basic", "", "");
        RegisterNode("Basic Measurement", "Basic measurement algorithm", "Basic quantitative measure algorithm", "2", "org.tomocube.algorithm.measurement.basic", "", "");
        RegisterChild("Auto Segmentation Parameter", "Parameter for auto algorithm", nullptr);        
        RegisterChild("Labeling Parameter", "Paraemter for labeling algorithm", nullptr);
        RegisterChild("Basic Measurement Parameter", "Paramter for basic meausrement", nullptr);        
    }
}