#pragma once

#include <QObject>
#include <IProcessor.h>

#include "TC_Processor_BasicAnalysis_AutoExport.h"

namespace TC::Processor::BasicAnalysis::Auto {
    class TC_Processor_BasicAnalysis_Auto_API Processor
        : public QObject
        , public IProcessor {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.processor.basicanalysis.auto")
        Q_INTERFACES(IProcessor)
	public:
		Processor();
		virtual ~Processor();

		auto GetName() const -> QString override { return "Auto BasicAnalysis"; }
		auto GetFullName() const -> QString override { return "org.tomocube.processor.basicanalysis.auto"; }
		auto GetDescription() const -> QString override { return "Basic analysis based on auto segmentation"; }
		auto clone() const -> IPluginModule* override { return new Processor(); }

		auto Parameter(const QString& key) -> IParameter::Pointer override;
		auto SetData(DataSet::Pointer data) -> void override;
		auto Execute() -> bool override;
		auto GetOutputFormat() const->QString override;
		auto GetResult()->DataSet::Pointer override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}