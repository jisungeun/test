#pragma once

#include "IParameter.h"

namespace TC::Processor::BasicAnalysis::Auto {
    class Parameter : public IParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static IParameter::Pointer CreateMethod() {
            return std::make_shared<Parameter>();
        }
        static std::string GetName() { return "org.tomocube.processor.basicanalysis.auto"; }

        Parameter();
    };
}