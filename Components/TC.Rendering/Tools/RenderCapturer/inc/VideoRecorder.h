#pragma once

#include <memory>
#include <QWidget>

#include <opencv2/opencv.hpp>
#include <OivBufferManager.h>
#include "ScreenShot.h"
#include "VideoEditWidget.h"
#include "TC.Rendering.Tools.RenderCapturerExport.h"

namespace TC {
    BETTER_ENUM(RecordType, int,
        None = -1,
        XY_Plane = 0,
        YZ_Plane = 1,
        XZ_Plane = 2,
        _3D = 3,
        Multi_View = 4)

        struct RecordAction {
        ActionType type = ActionType::None;
        ActionAxis axis = ActionAxis::None;
        ActionWindow windowType = ActionWindow::None;

        float startTime = 0.f;
        float endTime = 0.f;

        int begin = -1;
        int finish = -1;

        bool reverse = false;
    };
	class TC_Rendering_Tools_RenderCapturer_API VideoRecorder : public QObject {
        Q_OBJECT
	public:
		typedef VideoRecorder Self;
		typedef std::shared_ptr<Self> Pointer;

		VideoRecorder(QObject* parent = nullptr);
		~VideoRecorder();		

		auto SetRenderWindow(QOivRenderWindow* win, QString name)->void;
        auto SetBufferManager(OivBufferManager* manager)->void;
		auto SetVolumeName(const QString& name)->void;
        auto SetMultiLayerType(MultiLayoutType type)->void;

        auto StartSimulate(QList<RecordAction> process)->void;
        auto StopSimulate()const->void;        

        auto StartRecord(QString path, RecordType type, QList<RecordAction> process)->void;        

        auto SetTimePoints(std::vector<double> tps)->void;

    signals:
        void finishSimulate();
        void timeStepChanged(double step);
        void moveSlice(int viewIndex, int sliceIndex);
        void captureScreenSize(int,QString,double);
        void captureScreen(int);

    protected slots:
        auto OnCaptureScreen(int type)->void;
        auto OnFinishedSimulate()->void;
        auto OnFinishedRecord()->void;
        auto OnCalcFrameSize(int type,QString path,double fps)->void;

	protected:
        auto RecordScreen(QString path, RecordType type, QList<RecordAction> process)->bool;        
        auto CreateCVFile(QString path, double fps, cv::Size frameSize)->bool;

	private:        
        auto CaptureTwoByTwo(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ)->bool;
        auto CaptureHSlicesBy3D(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ)->bool;
        auto CaptureVSlicesBy3D(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ)->bool;
        auto CaptureHXY3D(const QImage& image3D, const QImage& imageXY)->bool;
        auto CaptureVXY3D(const QImage& image3D, const QImage& imageXY)->bool;
        auto CapturePlane(const QImage& image)->bool;
        //for Mask Editor
        auto CaptureBig(const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3)->bool;
        auto SizeBig(const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3)->cv::Size;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}