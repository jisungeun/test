#pragma once

#include <memory>
#include <QWidget>

#include <enum.h>

#include "ScreenShot.h"
#include "VideoRecorder.h"

#include "TC.Rendering.Tools.RenderCapturerExport.h"

class QOivRenderWindow;

namespace TC {    
    class TC_Rendering_Tools_RenderCapturer_API VideoRecorderWidget : public QWidget {
        Q_OBJECT
    public:
        typedef VideoRecorderWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        VideoRecorderWidget(QWidget* parent = nullptr);
        ~VideoRecorderWidget();

        auto Update()->bool;
        auto Init()->bool;
        auto Reset()const->bool;        

        auto Force2D()->void;
        auto ForceMIP()->void;
        auto Restore3D()->void;
        auto RestoreMIP()->void;

        //set outer render components
        auto SetTcfName(QString path)->void;
        auto SetMultiLayerType(MultiLayoutType type)->void;
        auto SetRenderWindow(QOivRenderWindow* win, QString name)->void;
        auto SetOivVolumeName(QString name)->void;
        auto SetTimePoints(std::vector<double> tps)->void;
        auto SetSliceMax(int maxXY,int maxYZ, int maxXZ)->void;
        auto SetBufferManager(OivBufferManager* buffer)->void;

    signals:
        void recMoveSlice(int, int);
        void recTimeStep(double);

    protected slots:
        void OnAddButton();
        void OnDeleteButton();
        void OnPlayButton();
        void OnRecordButton();

        void OnAngleSliderChanged(int value);
        void OnAngleSpinChanged(int value);
        void OnOrbitTextChanged(const QString& text);
        void OnOrbitCheckBox(bool checked);
        void OnXRadioButton(bool checked);
        void OnYRadioButton(bool checked);
        void OnZRadioButton(bool checked);
        void OnTRadioButton(bool checked);

        void OnSliceMinChanged(int value);
        void OnSliceMaxChanged(int value);
        void OnSliceCheckBox(bool checked);
        void OnSliceComboBox(int index);

        void OnTimeMinChanged(int value);
        void OnTimeMaxChanged(int value);
        void OnTimeCheckBox(bool checked);

        void OnAddAction();

        void OnRecord(bool);

        void OnCurrentModalityChanged(Modality modality);
        void OnSelectedActionItem(RecordActionItem* item);

        void OnFinishVideo();
        void OnTimeStepChanged(double);
        void OnSliceChanged(int, int);

        void OnItemMinChanged(int);
        void OnItemMaxChanged(int);

        void OnFpsToggled();
        void OnFpsValueChanged(int);

    protected:
        virtual auto keyPressEvent(QKeyEvent* keyPress)->void;

    private:        
        auto InitConnections()->void;

        auto ForceSelectItem(RecordActionItem* item)const->void;
        auto MakeUpProcess()const->bool;
        auto SetPlayingState(bool isPlaying)->void;
        auto AddTimeSettingAction(const QString& text, QMenu* parentMenu)->void;
        auto ApplyTimeByFps(int fps)->void;


        struct Impl;
        std::unique_ptr<Impl> d;
    };
}