#pragma once

#include <memory>
#include <QDialog>

namespace TC {
    class  MultipleCaptureDialog : public QDialog {
        Q_OBJECT
    public:
        explicit MultipleCaptureDialog(QString labelText, QWidget* parent = nullptr);
        virtual ~MultipleCaptureDialog();

    protected slots:
        void OnReplaceBtn();
        void OnCancelBtn();

    private:        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
