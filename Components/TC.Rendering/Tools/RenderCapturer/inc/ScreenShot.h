#pragma once

#include <enum.h>

#include <memory>
#include <QCoreApplication>
#include <QWidget>

#include "TC.Rendering.Tools.RenderCapturerExport.h"

class QOivRenderWindow;

namespace TC {
	BETTER_ENUM(MultiLayoutType, uint8_t, UNKNOWN = 0, TwoByTwo, HSlicesBy3D, VSlicesBy3D, Only3D, XYPlane, YZPlane, XZPlane, HXY3D, VXY3D, Big3D, BigXY, BigYZ, BigXZ, FLXY, FLYZ, FLXZ);

	//Big series is for switching window type (e.g. Mask Editor)
	BETTER_ENUM(CaptureType, uint8_t, XY = 0, YZ = 1, XZ = 2, threeD =3, ALL = 4);

	BETTER_ENUM(DirectionType, uint8_t, X = 0, Y = 1, Z = 2);

	enum ImageType {
		Unknown = 0,
		HT3D = 1,
		FL3D,
		HT2D,
		FL2D,
		PHASE,
		HT2DMIP,
		FL2DMIP,
		BF,
		Count,
	};

	enum Modality {
		None = 0x00,
		HTVolume = (0x01 << 1),
		FLVolume = (0x01 << 2),
		HTMIP = (0x01 << 3),
		FLMIP = (0x01 << 4),
		BF2D = (0x01 << 5),
	};

	class TC_Rendering_Tools_RenderCapturer_API ScreenShot {
		Q_DECLARE_TR_FUNCTIONS(ScreenShot)
	public:
		typedef ScreenShot Self;
		typedef std::shared_ptr<Self> Pointer;

		ScreenShot();
		~ScreenShot();
		auto Clear() -> void;

		//renderer settings		
		auto SetRenderWindow(QOivRenderWindow* win, QString name) -> void;
		auto SetVolumeName(const QString& name) -> void;

		//capture calls
		auto CaptureSlice(CaptureType type, QString path, int upsampling) -> bool;
		auto CaptureVolume(QString path, int upsampling) -> bool;
		auto CaptureMultiView(QString path, int upsampling, MultiLayoutType type = MultiLayoutType::UNKNOWN) -> bool;
		auto ChangeDirection(DirectionType type) -> bool;

	private:
		//multi-capture functions
		//for 3D Visualizer
		auto CaptureTwoByTwo(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool;
		auto CaptureHSlicesBy3D(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool;
		auto CaptureVSlicesBy3D(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool;
		auto CaptureHXY3D(QString path, const QImage& image3D, const QImage& imageXY) -> bool;
		auto CaptureVXY3D(QString path, const QImage& image3D, const QImage& imageXY) -> bool;
		auto CapturePlane(QString path, const QImage& image) -> bool;
		//for Mask Editor
		auto CaptureBig(QString path, const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3) -> bool;
		//for FL generator
		auto CaptureFL(QString path, const QImage& imageBig, const QImage& imageRight, const QImage& imageBottom) -> bool;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
