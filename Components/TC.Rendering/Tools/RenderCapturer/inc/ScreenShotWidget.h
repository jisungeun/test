#pragma once

#include <memory>
#include <QWidget>

#include <enum.h>

#include "ScreenShot.h"

#include "TC.Rendering.Tools.RenderCapturerExport.h"

class QOivRenderWindow;

namespace TC {	
	class TC_Rendering_Tools_RenderCapturer_API ScreenShotWidget : public QWidget{
		Q_OBJECT
	public:
		typedef ScreenShotWidget Self;
		typedef std::shared_ptr<Self> Pointer;

		ScreenShotWidget(QWidget* parent = nullptr);
		~ScreenShotWidget();

		auto SetTcfPath(const QString& path)->void;
		auto SetVolumeName(const QString& name)->void;
		auto SetEnableUI(bool enable)->void;
		auto EnableUIComponent(const QString& name)->void;		

		auto SetMultiLayerType(MultiLayoutType type)->void;
				
		auto SetRenderWindow(QOivRenderWindow* win, QString name)->void;

		auto force2D()->void;
		auto restore3D()->void;
		auto ToggleTab(int idx,bool show)->void;

	protected slots:
		void OnSliceCapture();
		void OnVolumeCapture();
		void OnVolumeAllCapture();
		void OnMultiCapture();

	private:
		auto Init()->void;
		auto InitConnections()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}