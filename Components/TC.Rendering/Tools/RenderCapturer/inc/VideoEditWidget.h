#pragma once

#include <memory>
#include <QWidget>
#include <QTableWidget>
#include <QMouseEvent>

#include <enum.h>

#include "TC.Rendering.Tools.RenderCapturerExport.h"

namespace TC {
    BETTER_ENUM(ActionType, int, 
                None        = -1, 
                Orbit       = 0,
                Slice       = 1, 
                Timelapse   = 2)

    BETTER_ENUM(ActionAxis, int,
                None = -1,
                X = 0,
                Y = 1,
                Z = 2,
                T = 3)

    BETTER_ENUM(ActionWindow, int,
                None = -1,
                XY = 0,
                YZ = 1,
                XZ = 2)

    class TickMarkWidget : public QWidget {
        Q_OBJECT

    public:
        typedef TickMarkWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        TickMarkWidget(QWidget* parent = nullptr);
        ~TickMarkWidget();

        void SetInterval(const int& interval);

    protected:
        virtual void paintEvent(QPaintEvent* event) override;

    private:
        void DrawTicker(QPainter* painter) const;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class TC_Rendering_Tools_RenderCapturer_API RecordActionItem : public QWidget {
        Q_OBJECT

    public:
        struct OrbitInfo {
            int startAngle = 180;
            int orbit = 360;
            bool reverse = false;
            ActionAxis axis = ActionAxis::None;
        };

        struct SliceInfo {
            ActionWindow windowType = ActionWindow::None;
            int min = 0;
            int max = -1;
            bool reverse = false;
        };

        struct TimeInfo {
            int min = 0;
            int max = 0;
            bool reverse = false;
        };

        typedef RecordActionItem Self;
        typedef std::shared_ptr<Self> Pointer;

        RecordActionItem(QWidget* parent = nullptr);
        ~RecordActionItem();

        auto GetType() const ->ActionType;
        void SetType(const ActionType& type) const;
        void SetPosition(const int& start, const int& end) const;
        void UpdateBox() const;

        auto GetOrbitInfo() const ->OrbitInfo*;
        auto GetSliceInfo() const ->SliceInfo*;
        auto GetTimeInfo() const ->TimeInfo*;
        void SetOrbitInfo(OrbitInfo* info) const;
        void SetSliceInfo(SliceInfo* info) const;
        void SetTimeInfo(TimeInfo* info) const;

        auto GetStartTime() const ->float;
        auto GetEndTime() const ->float;

        auto SetCheckOverlap(const ActionType& type) const ->void;
        auto IsCheckOverlap() const ->bool;
        auto GetCheckOverlapType(void) const ->ActionType;

        void UpdateGeometry(const int& begin, const int& end) const;

        auto IsContains(const int& begin, const int& end) const ->bool;

    signals:
        void changedGeometry(ActionType checkOverlapType, int begin, int end);

    protected:
        virtual bool eventFilter(QObject* watched, QEvent* event) override;

    private:
        auto Magnetic(const int& x) const -> int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class TC_Rendering_Tools_RenderCapturer_API VideoEditWidget : public QWidget {
        Q_OBJECT
        
    public:
        typedef VideoEditWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        VideoEditWidget(QWidget* parent=nullptr);
        ~VideoEditWidget();

        void AddAction(RecordActionItem* action) const;
        void DeleteSelectedAction() const;

        void SetInterval(const float& interval) const;
        auto GetProcess() const ->QList<RecordActionItem*>;

        void ClearSelection(void);        

    signals:
        void selectItem(RecordActionItem* item);
        void updateItemGeometry(int begin, int end);

    protected slots:
        void on_timestampWidget_itemSelectionChanged();
        void on_timestampWidget_dragAndDrop(int, int, int) const;
        void on_action_geometryChanged(ActionType type, int begin, int end);

    private:        
        void RefreshList() const;
        auto IsOverlap(const ActionType& type, const RecordActionItem* item, const int& begin, const int& end) const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}