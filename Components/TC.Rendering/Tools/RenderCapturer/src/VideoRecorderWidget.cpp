#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include <UIUtility.h>

#include "ui_VideoRecorderWidget.h"
#include "VideoRecorderWidget.h"

#include <QWidgetAction>

namespace TC {
    struct VideoRecorderWidget::Impl {
        Ui::VideoRecorderWidgetUI* ui{ nullptr };
        VideoRecorder::Pointer recorderEngine{ nullptr };

        QMenu* addMenu;
        QMenu* recordMenu;
           
        float interval{ 40.f };
                
        Modality prevModality{ Modality::None };

        RecordActionItem* currentItem{ nullptr };
        int currentSliceWidth{ -1 };
        int currentSliceHeight{ -1 };
        int currentSliceDepth{ -1 };

        int currentSliceMin{ 0 };
        int currentSliceMax{ -1 };            

        int currentTimeMax{ -1 };

        QString tcfName;

        QList<RecordAction> process;

        bool playing = false;
        bool force2D = false;

        const QList<ActionType> actionList = { ActionType::Orbit ,
                                               ActionType::Slice,
                                               ActionType::Timelapse };

        const QStringList recordTypeList = { "XY Plane",
                                             "YZ Plane",
                                             "XZ Plane",
                                             "3D",
                                             "Multi-view" };        
    };
    VideoRecorderWidget::VideoRecorderWidget(QWidget* parent) : QWidget(parent),d{new Impl} {
        d->ui = new Ui::VideoRecorderWidgetUI();
        d->ui->setupUi(this);

        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->addButton->setObjectName("bt-round-gray700");
        d->ui->deleteButton->setObjectName("bt-round-gray700");

        d->ui->orbitPageTitleLabel->setObjectName("h8");
        d->ui->angleLabel->setObjectName("h9");
        d->ui->orbitLabel->setObjectName("h9");

        d->ui->slicePageTitleLabel->setObjectName("h8");
        d->ui->timeRangeLabel->setObjectName("h9");
        d->ui->sliceWindowLabel->setObjectName("h9");

        d->ui->timePageTitleLabel->setObjectName("h8");
        d->ui->timeRangeLabel->setObjectName("h9");

        d->ui->recordButton->setObjectName("bt-square-gray700");
        d->ui->playButton->setObjectName("bt-square-gray700");

        d->ui->line->setObjectName("line-separator-section");
        d->ui->line_2->setObjectName("line-separator-section");
        d->ui->line_3->setObjectName("line-separator-section");

        // set icons
        d->ui->recordButton->AddIcon(":/img/ic-record.svg");
        d->ui->recordButton->AddIcon(":/img/ic-record-d.svg", QIcon::Disabled);
        d->ui->recordButton->AddIcon(":/img/ic-record-s.svg", QIcon::Active);
        d->ui->recordButton->AddIcon(":/img/ic-record-s.svg", QIcon::Selected);

        d->ui->fpsSpin->setSingleStep(1);
        d->ui->fpsSpin->setValue(30);
        d->ui->fpsSpin->setRange(1, 60);
        d->ui->fpsSpin->setEnabled(false);

        d->recorderEngine = std::make_shared<VideoRecorder>();
        d->recorderEngine->SetVolumeName("volData");
                
        InitConnections();
        SetPlayingState(false);
    }
    VideoRecorderWidget::~VideoRecorderWidget() {
        delete d->ui;
    }

    void VideoRecorderWidget::OnItemMaxChanged(int val) {        
        auto parent = sender()->parent();
        auto minSpin = parent->findChild<QSpinBox*>("MinSpin");
        if (minSpin) {
            if (minSpin->value() >= val) {
                SilentCall(minSpin)->setValue(val - 1);
            }
        }
    }

    void VideoRecorderWidget::OnItemMinChanged(int val) {
        auto parent = sender()->parent();        
        auto maxSpin = parent->findChild<QSpinBox*>("MaxSpin");
        if(maxSpin) {
            if(maxSpin->value()<=val) {
                SilentCall(maxSpin)->setValue(val + 1);
            }
        }
    }

    auto VideoRecorderWidget::SetBufferManager(OivBufferManager* buffer) -> void {
        d->recorderEngine->SetBufferManager(buffer);
    }

    auto VideoRecorderWidget::SetOivVolumeName(QString name) -> void {
        d->recorderEngine->SetVolumeName(name);
    }
    auto VideoRecorderWidget::SetMultiLayerType(MultiLayoutType type) -> void {        
        d->recorderEngine->SetMultiLayerType(type);
    }
    auto VideoRecorderWidget::SetRenderWindow(QOivRenderWindow* win, QString name) -> void {
        d->recorderEngine->SetRenderWindow(win, name);
    }
    auto VideoRecorderWidget::SetTcfName(QString path) -> void {
        QFileInfo fileInfo(path);
        d->tcfName = fileInfo.completeBaseName();
    }
    auto VideoRecorderWidget::SetSliceMax(int maxXY,int maxYZ,int maxXZ) -> void {
        //changed due to modality
        d->currentSliceDepth = maxXY;
        d->currentSliceWidth = maxYZ;
        d->currentSliceHeight = maxXZ;
        d->currentSliceMax = d->currentSliceDepth;
        SilentCall(d->ui->sliceMinSpinBox)->setRange(0, d->currentSliceMax);
        SilentCall(d->ui->sliceMaxSpinBox)->setRange(0, d->currentSliceMax);
        //SilentCall(d->ui->sliceMaxSpinBox)->setValue(d->currentSliceMax);
        //SilentCall(d->ui->sliceMinSpinBox)->setValue(0);
        d->ui->sliceMaxSpinBox->setValue(d->currentSliceMax);
        d->ui->sliceMinSpinBox->setValue(0);

        //update existing action range
        const auto previousProc = d->ui->editWidget->GetProcess();
        for (const auto item : previousProc) {
            const auto type = item->GetType();
            if (type._to_integral() == ActionType::Slice) {
                auto info = item->GetSliceInfo();
                if(info->windowType._to_integral() == ActionWindow::XY) {
                    info->max = maxXY;
                }else if(info->windowType._to_integral() == ActionWindow::YZ) {
                    info->max = maxYZ;
                }else if(info->windowType._to_integral()==ActionWindow::XZ) {
                    info->max = maxXZ;
                }
            }
        }
    }
    auto VideoRecorderWidget::SetTimePoints(std::vector<double> tps) -> void {
        d->recorderEngine->SetTimePoints(tps);
        d->currentTimeMax = tps.size();        

        SilentCall(d->ui->timeMinSpinBox)->setRange(1, d->currentTimeMax);
        SilentCall(d->ui->timeMaxSpinBox)->setRange(1, d->currentTimeMax);
        //SilentCall(d->ui->timeMaxSpinBox)->setValue(d->currentTimeMax);
        //SilentCall(d->ui->timeMinSpinBox)->setValue(1);
        d->ui->timeMaxSpinBox->setValue(d->currentTimeMax);
        d->ui->timeMinSpinBox->setValue(1);

        //update existing action range
        const auto previousProc = d->ui->editWidget->GetProcess();
        for(const auto item : previousProc){
            const auto type = item->GetType();            
            if(type._to_integral() == ActionType::Timelapse) {
                auto info = item->GetTimeInfo();
                info->min = 0;
                info->max = d->currentTimeMax-1;
            }            
        }
    }

    auto VideoRecorderWidget::Update() -> bool {        
        const bool enable = true;
        d->ui->addButton->setEnabled(enable);
        d->ui->deleteButton->setEnabled(enable);
        d->ui->playButton->setEnabled(enable);
        d->ui->recordButton->setEnabled(enable);

        return true;
    }
    
    auto VideoRecorderWidget::Init()-> bool {
        d->ui->addButton->setContextMenuPolicy(Qt::CustomContextMenu);

        d->addMenu = new QMenu;

        for (auto i = 0; i < 3; ++i) {
            //const auto action = new QAction(d->actionList.at(i)._to_string());
            //connect(action, SIGNAL(triggered(bool)), this, SLOT(OnAddAction(bool)));
            //d->addMenu->addAction(action);
            AddTimeSettingAction(d->actionList.at(i)._to_string(), d->addMenu);
        }

        // add record menu
        d->ui->recordButton->setContextMenuPolicy(Qt::CustomContextMenu);

        d->recordMenu = new QMenu;

        for (auto i = 0; i < 5; ++i) {
            const auto action = new QAction(d->recordTypeList.at(i));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(OnRecord(bool)));
            d->recordMenu->addAction(action);
        }

        connect(d->ui->editWidget, SIGNAL(selectItem(RecordActionItem*)), this, SLOT(OnSelectedActionItem(RecordActionItem*)));

        // time stamp
        d->ui->editWidget->SetInterval(d->interval);
        d->ui->scrollArea->setWidgetResizable(true);

        d->ui->stackedWidget->hide();

        SilentCall(d->ui->angleSlider)->setRange(0, 360);
        SilentCall(d->ui->angleSpinBox)->setRange(0, 360);
        SilentCall(d->ui->angleSlider)->setValue(180);
        SilentCall(d->ui->angleSpinBox)->setValue(180);
        SilentCall(d->ui->orbitLineEdit)->setText("360");

        SilentCall(d->ui->sliceWindowComboBox)->addItem("XY Plane");
        SilentCall(d->ui->sliceWindowComboBox)->addItem("YZ Plane");
        SilentCall(d->ui->sliceWindowComboBox)->addItem("XZ Plane");
        SilentCall(d->ui->sliceWindowComboBox)->setCurrentIndex(0);

        const bool enable = true;
        d->ui->addButton->setEnabled(enable);
        d->ui->deleteButton->setEnabled(enable);
        d->ui->playButton->setEnabled(enable);
        d->ui->recordButton->setEnabled(enable);


        return true;
    }

    auto VideoRecorderWidget::Reset() const -> bool {
        return true;
    }

    auto VideoRecorderWidget::AddTimeSettingAction(const QString& text, QMenu* parentMenu) -> void {
        const auto addMenu = new QMenu(text);
        const auto addWidgetMenu = new QWidgetAction(parentMenu);
        const auto addWidget = new QWidget();
        const auto widgetLayout = new QVBoxLayout;
        addWidget->setLayout(widgetLayout);
        addWidgetMenu->setDefaultWidget(addWidget);

        widgetLayout->setContentsMargins(5, 5, 5, 5);
        widgetLayout->setSpacing(9);

        const auto rangeLabel = new QLabel("Record Time(Sec)");
        rangeLabel->setObjectName("h9");
        widgetLayout->addWidget(rangeLabel);

        const auto rangeLayout = new QHBoxLayout;
        rangeLayout->setMargin(9);
        rangeLayout->setContentsMargins(0, 0, 0, 0);               

        const auto minSpin = new QSpinBox(addMenu);
        minSpin->setRange(0, 89);
        minSpin->setValue(0);
        minSpin->setObjectName("MinSpin");
        const auto maxSpin = new QSpinBox(addMenu);
        maxSpin->setRange(1, 90);
        maxSpin->setValue(1);
        maxSpin->setObjectName("MaxSpin");

        connect(minSpin, SIGNAL(valueChanged(int)), this, SLOT(OnItemMinChanged(int)));
        connect(maxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnItemMaxChanged(int)));

        const auto slashLabel = new QLabel("~");
        rangeLayout->addWidget(minSpin);
        rangeLayout->addWidget(slashLabel);
        rangeLayout->addWidget(maxSpin);

        widgetLayout->addLayout(rangeLayout);

        const auto addButton = new QPushButton("Add",addMenu);
        addButton->setObjectName("bt-square-gray700");
        addButton->setWhatsThis(text);
        widgetLayout->addWidget(addButton);

        addMenu->addAction(addWidgetMenu);        
        connect(addButton, SIGNAL(clicked()), this, SLOT(OnAddAction()));

        parentMenu->addMenu(addMenu);
    }

    auto VideoRecorderWidget::ForceMIP() -> void {
        d->force2D = true;
        d->addMenu->clear();        
        AddTimeSettingAction(d->actionList.at(2)._to_string(), d->addMenu);        
        SilentCall(d->ui->sliceWindowComboBox)->setEnabled(false);

        d->recordMenu->clear();
        const auto recordAction = new QAction(d->recordTypeList.at(0));
        connect(recordAction, SIGNAL(triggered(bool)), this, SLOT(OnRecord(bool)));
        d->recordMenu->addAction(recordAction);
	}

    auto VideoRecorderWidget::RestoreMIP() -> void {
		if(d->force2D) {
            d->force2D = false;
            Force2D();
		}else {
            d->force2D = true;
            Restore3D();
		}
	}

    auto VideoRecorderWidget::Force2D() -> void {
        if (!d->force2D) {
            d->force2D = true;
            d->addMenu->clear();
            AddTimeSettingAction(d->actionList.at(1)._to_string(), d->addMenu);
            AddTimeSettingAction(d->actionList.at(2)._to_string(), d->addMenu);
            //const auto addAction = new QAction("Timelapse");
            //connect(addAction, SIGNAL(triggered(bool)), this, SLOT(OnAddAction(bool)));
            //d->addMenu->addAction(addAction);
            SilentCall(d->ui->sliceWindowComboBox)->setEnabled(false);
            
            d->recordMenu->clear();
            const auto recordAction = new QAction(d->recordTypeList.at(0));
            connect(recordAction, SIGNAL(triggered(bool)), this, SLOT(OnRecord(bool)));
            d->recordMenu->addAction(recordAction);
        }
    }

    auto VideoRecorderWidget::Restore3D() -> void {
        if (d->force2D) {
            d->force2D = false;
            d->addMenu->clear();
            SilentCall(d->ui->sliceWindowComboBox)->setEnabled(true);
            for (auto i = 0; i < 3; ++i) {
                //const auto action = new QAction(d->actionList.at(i)._to_string());
                //connect(action, SIGNAL(triggered(bool)), this, SLOT(OnAddAction(bool)));
                //d->addMenu->addAction(action);
                AddTimeSettingAction(d->actionList.at(i)._to_string(), d->addMenu);
            }
            d->recordMenu->clear();
            for (auto i = 0; i < 5; ++i) {
                const auto action = new QAction(d->recordTypeList.at(i));
                connect(action, SIGNAL(triggered(bool)), this, SLOT(OnRecord(bool)));
                d->recordMenu->addAction(action);
            }
        }
    }
    
    auto VideoRecorderWidget::InitConnections() -> void {
        connect(d->ui->addButton, SIGNAL(clicked()), this, SLOT(OnAddButton()));
        connect(d->ui->deleteButton, SIGNAL(clicked()), this, SLOT(OnDeleteButton()));
        connect(d->ui->playButton, SIGNAL(clicked()), this, SLOT(OnPlayButton()));
        connect(d->ui->recordButton, SIGNAL(clicked()), this, SLOT(OnRecordButton()));
        connect(d->ui->angleSlider, SIGNAL(valueChanged(int)), this, SLOT(OnAngleSliderChanged(int)));
        connect(d->ui->angleSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnAngleSpinChanged(int)));
        connect(d->ui->orbitLineEdit, SIGNAL(textChanged(const QString&)), this, SLOT(OnOrbitTextChanged(const QString&)));
        connect(d->ui->orbitCheckBox, SIGNAL(toggled(bool)), this, SLOT(OnOrbitCheckBox(bool)));
        connect(d->ui->xRadioButton, SIGNAL(toggled(bool)), this, SLOT(OnXRadioButton(bool)));
        connect(d->ui->yRadioButton, SIGNAL(toggled(bool)), this, SLOT(OnYRadioButton(bool)));
        connect(d->ui->zRadioButton, SIGNAL(toggled(bool)), this, SLOT(OnZRadioButton(bool)));
        connect(d->ui->tRadioButton, SIGNAL(toggled(bool)), this, SLOT(OnTRadioButton(bool)));
        connect(d->ui->sliceMinSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnSliceMinChanged(int)));
        connect(d->ui->sliceMaxSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnSliceMaxChanged(int)));
        connect(d->ui->sliceCheckBox, SIGNAL(toggled(bool)), this, SLOT(OnSliceCheckBox(bool)));
        connect(d->ui->sliceWindowComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(OnSliceComboBox(int)));
        connect(d->ui->timeCheckBox, SIGNAL(toggled(bool)), this, SLOT(OnTimeCheckBox(bool)));
        connect(d->ui->timeMinSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnTimeMinChanged(int)));
        connect(d->ui->timeMaxSpinBox, SIGNAL(valueChanged(int)), this, SLOT(OnTimeMaxChanged(int)));
        connect(d->ui->fpsChk, SIGNAL(clicked()), this, SLOT(OnFpsToggled()));
        connect(d->ui->fpsSpin, SIGNAL(valueChanged(int)), this, SLOT(OnFpsValueChanged(int)));

        connect(d->recorderEngine.get(), SIGNAL(finishSimulate()), this, SLOT(OnFinishVideo()));
        connect(d->recorderEngine.get(), SIGNAL(timeStepChanged(double)), this, SLOT(OnTimeStepChanged(double)));
        connect(d->recorderEngine.get(), SIGNAL(moveSlice(int,int)), this, SLOT(OnSliceChanged(int,int)));        
    }

    auto VideoRecorderWidget::ApplyTimeByFps(int fps) -> void {
        if (nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }
        
        auto curMinStep = d->ui->timeMinSpinBox->value();
        auto curMaxStep = d->ui->timeMaxSpinBox->value();
        auto timeSteps = curMaxStep - curMinStep + 1;

        auto sec = 1.f / static_cast<float>(fps) * static_cast<float>(timeSteps);
        if (sec < 0.5f) sec = 0.5f;

        auto startTime = d->currentItem->GetStartTime();

        auto endTime = startTime + sec;

        auto startTimeStep = static_cast<int>(startTime / 0.5f);
        auto endTimeStep = static_cast<int>(ceil(endTime / 0.5f));

        emit d->currentItem->changedGeometry(ActionType::Timelapse, startTimeStep * d->interval, endTimeStep * d->interval);
    }


    //Slots
    void VideoRecorderWidget::OnFpsToggled() {
        if (false == d->ui->fpsChk->isChecked()) {
            d->ui->fpsSpin->setEnabled(false);
            return;
        }
        d->ui->fpsSpin->setEnabled(true);        

        ApplyTimeByFps(d->ui->fpsSpin->value());       
    }

    void VideoRecorderWidget::OnFpsValueChanged(int value) {
        ApplyTimeByFps(value);
    }

    void VideoRecorderWidget::OnTimeStepChanged(double step) {
        //broadcast signal from engine
        emit recTimeStep(step);
    }

    void VideoRecorderWidget::OnSliceChanged(int viewIndex, int sliceIndex) {
        //broadcast signal from engine
        emit recMoveSlice(viewIndex, sliceIndex);
    }


    void VideoRecorderWidget::OnAddButton() {
        //TODO scenario changed according to TAIT-58
        QPoint pos = this->mapToGlobal(d->ui->addButton->pos());
        pos.setY(pos.y() + d->ui->addButton->height());
        d->addMenu->exec(pos);

    }
    void VideoRecorderWidget::OnDeleteButton() {
        d->ui->editWidget->DeleteSelectedAction();
        d->ui->editWidget->ClearSelection();
    }
    void VideoRecorderWidget::OnPlayButton() {
        if (false == d->playing) {
            MakeUpProcess();

            SetPlayingState(true);
            d->recorderEngine->StartSimulate(d->process);            
        }
        else {
            SetPlayingState(false);
            d->recorderEngine->StopSimulate();
        }
    }
    void VideoRecorderWidget::OnRecordButton() {
        QPoint pos = QCursor::pos();
        d->recordMenu->exec(pos);
    }
    void VideoRecorderWidget::OnAngleSliderChanged(int value) {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->startAngle = value;

        SilentCall(d->ui->angleSpinBox)->setValue(value);
    }
    void VideoRecorderWidget::OnAngleSpinChanged(int value) {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->startAngle = value;

        SilentCall(d->ui->angleSlider)->setValue(value);
    }
    void VideoRecorderWidget::OnOrbitTextChanged(const QString& text) {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->orbit = text.toInt();
    }
    void VideoRecorderWidget::OnOrbitCheckBox(bool checked) {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->reverse = checked;
    }
    void VideoRecorderWidget::OnXRadioButton(bool checked) {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::X;
    }
    void VideoRecorderWidget::OnYRadioButton(bool checked) {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::Y;
    }
    void VideoRecorderWidget::OnZRadioButton(bool checked) {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::Z;
    }
    void VideoRecorderWidget::OnTRadioButton(bool checked) {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::T;
    }
    void VideoRecorderWidget::OnSliceMinChanged(int value) {
        if(nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetSliceInfo()->min = value;
    }
    void VideoRecorderWidget::OnSliceMaxChanged(int value) {
        if(nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }
        d->currentItem->GetSliceInfo()->max = value;
    }
    void VideoRecorderWidget::OnSliceCheckBox(bool checked) {
        if(nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetSliceInfo()->reverse = checked;
    }
    void VideoRecorderWidget::OnSliceComboBox(int index) {
        d->currentItem->GetSliceInfo()->windowType = ActionWindow::_from_integral(index);

        d->currentSliceMax = -1;
        switch (index) {
        case 0: // XY
            d->currentSliceMax = d->currentSliceDepth;
            break;
        case 1: // YZ
            d->currentSliceMax = d->currentSliceHeight;
            break;
        case 2: // XZ
            d->currentSliceMax = d->currentSliceWidth;
            break;
        default:;
        }

        d->ui->sliceMinSpinBox->setRange(0, d->currentSliceMax);
        d->ui->sliceMinSpinBox->setValue(0);

        d->ui->sliceMaxSpinBox->setRange(0, d->currentSliceMax);
        d->ui->sliceMaxSpinBox->setValue(d->currentSliceMax);
    }
    void VideoRecorderWidget::OnTimeCheckBox(bool checked) {
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetTimeInfo()->reverse = checked;
    }
    void VideoRecorderWidget::OnTimeMaxChanged(int value) {
        if(nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }
        //d->currentItem->GetTimeInfo()->max = value;
        d->currentItem->GetTimeInfo()->max = value - 1;
    }
    void VideoRecorderWidget::OnTimeMinChanged(int value) {
        if(nullptr == d->currentItem) {
            return;
        }
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }
        //d->currentItem->GetTimeInfo()->min = value;
        d->currentItem->GetTimeInfo()->min = value - 1;
    }
    void VideoRecorderWidget::OnAddAction() {
        //const auto action = qobject_cast<QAction*>(sender());
        const auto action = qobject_cast<QPushButton*>(sender());
        if (nullptr == action) {
            return;
        }

        auto maxSpin = action->parent()->findChild<QSpinBox*>("MaxSpin");
        auto minSpin = action->parent()->findChild<QSpinBox*>("MinSpin");

        auto minVal = 0;
        auto maxVal = 1;

        if(maxSpin) {
            maxVal = maxSpin->value();
        }
        if(minSpin) {
            minVal = minSpin->value();
        }
        
        auto item = new RecordActionItem(nullptr);
        //item->SetType(ActionType::_from_string(action->text().toStdString().c_str()));        
        item->SetType(ActionType::_from_string(action->whatsThis().toStdString().c_str()));
        item->SetPosition(d->interval*static_cast<float>(minVal)*2.f, d->interval * static_cast<float>(maxVal) * 2.f);

        //if (action->text() == d->actionList.at(0)._to_string()) { // orbit
        if (action->whatsThis() == d->actionList.at(0)._to_string()) { // orbit
            item->SetCheckOverlap(ActionType::Orbit);
            item->GetOrbitInfo()->axis = ActionAxis::X;
        }
        //else if (action->text() == d->actionList.at(1)._to_string()) { // slice
        else if (action->whatsThis() == d->actionList.at(1)._to_string()) { // slice
            item->GetSliceInfo()->min = d->currentSliceMin;
            item->GetSliceInfo()->max = d->currentSliceMax;
            item->GetSliceInfo()->windowType = ActionWindow::XY;
        }
        //else if (action->text() == d->actionList.at(2)._to_string()) { // timelapse
        else if (action->whatsThis() == d->actionList.at(2)._to_string()) { // timelapse
            //item->GetTimeInfo()->max = d->currentTimeMax;
            item->GetTimeInfo()->max = d->currentTimeMax - 1;
        }
        else {
            // error
            delete item;
            item = nullptr;
        }

        d->ui->editWidget->AddAction(item);
    }
    void VideoRecorderWidget::OnRecord(bool) {
        const auto action = qobject_cast<QAction*>(sender());
        if (nullptr == action) {
            return;
        }

        auto text = action->text();
        const auto index = d->recordTypeList.indexOf(QRegExp(text, Qt::CaseInsensitive, QRegExp::RegExp2), 0);
        if (-1 == index) {
            return;
        }

        const QString name = d->tcfName + "_" + text.replace(" ", "_") + ".mp4";// ".avi";

        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentRecord").toString();
        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save video file"),
            prev + "/" + name, tr("Movie (*.mp4)"));

        QFileInfo fileinfo(savePath);

        QSettings("Tomocube", "TomoAnalysis").setValue("recentRecord", fileinfo.absoluteDir().path());

        if (true == savePath.isEmpty()) {
            return;
        }

        if (false == MakeUpProcess()) {
            return;
        }
        
        d->recorderEngine->StartRecord(savePath, RecordType::_from_integral(index), d->process);
        //emit startRecord(savePath, RecordType::_from_integral(index), d->process);
    }
    void VideoRecorderWidget::OnCurrentModalityChanged(Modality modality) {
        //deprecated
        //originally send signal from main window
        //instead, set meta info such as time min, max etc.
    }
    void VideoRecorderWidget::OnSelectedActionItem(RecordActionItem* item) {
        if (d->currentItem == item) {
            return;
        }

        d->currentItem = item;

        //d->ui->orbitGroupBox->hide();
        //d->ui->sliceGroupBox->hide();
        //d->ui->timeGroupBox->hide();

        if (nullptr == item) {
            return;
        }

        auto type = item->GetType();
        switch (type) {
        case ActionType::Orbit: {
            //d->ui->orbitGroupBox->show();

            const auto info = d->currentItem->GetOrbitInfo();
            SilentCall(d->ui->angleSlider)->setValue(info->startAngle);
            SilentCall(d->ui->angleSpinBox)->setValue(info->startAngle);
            SilentCall(d->ui->orbitLineEdit)->setText(QString::number(info->orbit));
            SilentCall(d->ui->orbitCheckBox)->setChecked(info->reverse);

            if (+ActionAxis::X == info->axis) {
                SilentCall(d->ui->xRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Y == info->axis) {
                SilentCall(d->ui->yRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Z == info->axis) {
                SilentCall(d->ui->zRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::T == info->axis) {
                SilentCall(d->ui->tRadioButton)->setChecked(true);
            }

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(0);

            break;
        }

        case ActionType::Slice: {
            //d->ui->sliceGroupBox->show();

            const auto info = d->currentItem->GetSliceInfo();
            SilentCall(d->ui->sliceMinSpinBox)->setValue(info->min);
            SilentCall(d->ui->sliceMaxSpinBox)->setValue(info->max);
            SilentCall(d->ui->sliceCheckBox)->setChecked(info->reverse);
            SilentCall(d->ui->sliceWindowComboBox)->setCurrentIndex(info->windowType);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(1);

            break;
        }
        case ActionType::Timelapse: {
            //d->ui->timeGroupBox->show();

            const auto info = d->currentItem->GetTimeInfo();
            //SilentCall(d->ui->timeMinSpinBox)->setValue(info->min);
            SilentCall(d->ui->timeMinSpinBox)->setValue(info->min + 1);
            //SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max);
            SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max + 1);
            SilentCall(d->ui->timeCheckBox)->setChecked(info->reverse);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(2);

            break;
        }
        case ActionType::None:
        default:
            d->ui->stackedWidget->hide();
            break;
        }
    }
    void VideoRecorderWidget::OnFinishVideo() {        
        //originally send signal from main window
        SetPlayingState(false);
    }

    auto VideoRecorderWidget::keyPressEvent(QKeyEvent* keyPress) -> void {
        switch (keyPress->key()) {
        case Qt::Key_Escape: {
            d->ui->editWidget->ClearSelection();
            break;
        }
        default:
            break;
        }
    }
    auto VideoRecorderWidget::ForceSelectItem(RecordActionItem* item) const -> void {
        
    }
    auto VideoRecorderWidget::MakeUpProcess() const -> bool {
        d->process.clear();

        auto temp = d->ui->editWidget->GetProcess();
        std::sort(temp.begin(), temp.end(),
            [](RecordActionItem* first, RecordActionItem* second) -> bool
        {
            return first->GetEndTime() < second->GetEndTime();
        });
        auto ignore = false;
        for (auto item : temp) {
            const auto type = item->GetType();
            RecordAction action;
            switch (type) {
            case ActionType::Orbit: {               
                action.type = ActionType::Orbit;
                action.startTime = item->GetStartTime();
                action.endTime = item->GetEndTime();
                action.begin = item->GetOrbitInfo()->startAngle;
                action.finish = item->GetOrbitInfo()->orbit;
                action.reverse = item->GetOrbitInfo()->reverse;
                action.axis = item->GetOrbitInfo()->axis;
                if (d->force2D)
                    ignore = true;
                break;
            }
            case ActionType::Slice: {
                action.type = ActionType::Slice;
                action.startTime = item->GetStartTime();
                action.endTime = item->GetEndTime();
                action.begin = item->GetSliceInfo()->min;
                action.finish = item->GetSliceInfo()->max;
                action.reverse = item->GetSliceInfo()->reverse;
                action.windowType = item->GetSliceInfo()->windowType;                
                break;
            }
            case ActionType::Timelapse: {
                action.type = ActionType::Timelapse;
                action.startTime = item->GetStartTime();
                action.endTime = item->GetEndTime();
                action.begin = item->GetTimeInfo()->min;
                action.finish = item->GetTimeInfo()->max;
                action.reverse = item->GetTimeInfo()->reverse;
                break;
            }
            default:;
            }
            if (!ignore) {
                d->process.push_back(action);
            }
        }
        return true;
    }
    auto VideoRecorderWidget::SetPlayingState(bool isPlaying) -> void {
        d->playing = isPlaying;

        if (isPlaying) {
            d->ui->playButton->setText("Stop");

            d->ui->playButton->AddIcon(":/img/ic-stop.svg");
            d->ui->playButton->AddIcon(":/img/ic-stop-d.svg", QIcon::Disabled);
            d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Active);
            d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Selected);
        }
        else {
            d->ui->playButton->setText("Play");

            d->ui->playButton->AddIcon(":/img/ic-play.svg");
            d->ui->playButton->AddIcon(":/img/ic-play-d.svg", QIcon::Disabled);
            d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Active);
            d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Selected);
        }
    }
}
