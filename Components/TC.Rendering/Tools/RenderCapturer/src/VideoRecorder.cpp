//#include <qmath.h>

#pragma warning(disable:4005)

#include <QPixmap>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include <QOivRenderWindow.h>
#include <ToastMessageBox.h>

#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeRender.h>


#include "VideoRecorder.h"

namespace TC {
    struct VideoRecorder::Impl {
		bool isRecording = false;
		QMap<QString, QOivRenderWindow*> renderWindows;		

		QMutex simulationMutex;
		QFutureWatcher<bool> simulationWatcher;		
		QString recordSavePath;
		QFutureWatcher<bool> recordWatcher;
		bool simulating{ false };
		QString oivVolName;

		std::vector<double> time_points;
		cv::VideoWriter cvWriter;
		cv::Size curScreenSize;

		OivBufferManager* buffermanager{ nullptr };
		bool curFixedVolume{ false };
		bool curFixedSlice{ false };

		MultiLayoutType curLayoutType{ MultiLayoutType::UNKNOWN };
    };

	VideoRecorder::VideoRecorder(QObject* parent)  :d{ new Impl }, QObject(parent) {
		connect(&d->simulationWatcher, SIGNAL(finished()), this, SLOT(OnFinishedSimulate()));
		connect(this, SIGNAL(captureScreen(int)), this, SLOT(
			OnCaptureScreen(int)), Qt::BlockingQueuedConnection);
		connect(this, SIGNAL(captureScreenSize(int,QString,double)), this, SLOT(OnCalcFrameSize(int,QString,double)), Qt::BlockingQueuedConnection);
		connect(&d->recordWatcher, SIGNAL(finished()), this, SLOT(OnFinishedRecord()));
	}

	VideoRecorder::~VideoRecorder() {

	}

	auto VideoRecorder::SetBufferManager(OivBufferManager* manager) -> void {
		d->buffermanager = manager;
	}
	
	auto VideoRecorder::SetMultiLayerType(MultiLayoutType type) -> void {
		d->curLayoutType = type;
    }
	auto VideoRecorder::SetRenderWindow(QOivRenderWindow* win, QString name) -> void {
		d->renderWindows[name] = win;
    }	

	auto VideoRecorder::StartSimulate(QList<RecordAction> process) -> void {
		d->simulating = true;
		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen("", RecordType::None, process);
		});
		d->simulationWatcher.setFuture(future);
    }
	auto VideoRecorder::StopSimulate() const -> void {
		d->simulating = false;
    }
	auto VideoRecorder::OnFinishedSimulate() -> void {		
		d->simulating = false;
		emit finishSimulate();
    }

	auto VideoRecorder::SetTimePoints(std::vector<double> tps) -> void {
		d->time_points.clear();
		d->time_points = std::move(tps);		
	}

	auto VideoRecorder::SetVolumeName(const QString& name) -> void {
		d->oivVolName = name;
    }

	auto VideoRecorder::StartRecord(QString path, RecordType type, QList<RecordAction> process) -> void {
        if(true == path.isEmpty()) {
			return;
        }
		for(auto renWin:d->renderWindows) {
			if (renWin) {
				renWin->setSettingIconViz(false);
				renWin->setWindowTitleViz(false);
			}
		}

		if(d->buffermanager) {
			d->curFixedVolume = d->buffermanager->isFixedResolutionEnabled(true);
			d->curFixedSlice = d->buffermanager->isFixedResolutionEnabled(false);
			d->buffermanager->enableFixedResolution(true,true);
			d->buffermanager->enableFixedResolution(true, false);
		}

		//turn-off resolution degradation
		if (d->renderWindows["threeD"]) {
			auto root = d->renderWindows["threeD"]->getSceneGraph();
			auto volRender = MedicalHelper::find<SoVolumeRender>(root);
			volRender->lowResMode = SoVolumeRender::DECREASE_NONE;
			volRender->lowScreenResolutionScale = 1;
		}

		QFileInfo fileInfo(path);
		d->recordSavePath = path;
		d->simulating = true;
		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen(path, type, process);
		});
		d->recordWatcher.setFuture(future);
    }
	auto VideoRecorder::OnFinishedRecord() -> void {		
		QString text;
		if (true == d->recordWatcher.result()) {
			text = tr("The recorded video file has been saved successfully.\n%1").arg(d->recordSavePath);
		}
		else {
			text = tr("Failed to save recorded video file.");
		}

		for(auto renWin : d->renderWindows) {
			if (renWin) {
				renWin->setSettingIconViz(true);
				renWin->setWindowTitleViz(true);
			}
		}

		if (d->renderWindows["threeD"]) {
			auto root = d->renderWindows["threeD"]->getSceneGraph();
			auto volRender = MedicalHelper::find<SoVolumeRender>(root);
			volRender->lowResMode = SoVolumeRender::DECREASE_SCREEN_RESOLUTION;
			volRender->lowScreenResolutionScale = 4;
		}

		if (true == d->cvWriter.isOpened()) {			
			d->cvWriter.release();
		}				

		if(d->buffermanager) {
			d->buffermanager->enableFixedResolution(d->curFixedVolume,true);
			d->buffermanager->enableFixedResolution(d->curFixedSlice, false);
		}

		auto toastMessageBox = new TC::ToastMessageBox(nullptr);
		toastMessageBox->Show(text);
    }	

	auto VideoRecorder::CreateCVFile(QString path,double fps,cv::Size frameSize) -> bool {
		bool result = true;
		try {
		    const auto codec = cv::VideoWriter::fourcc('a', 'v', 'c', '1');
			if (false == d->cvWriter.open(path.toStdString(), codec, fps, frameSize, true)) {
				throw std::exception();
			}			
		}catch(cv::Exception& e) {			
			std::cout << e.what() << std::endl;
		}
		catch (std::exception& ex) {			
			std::cout << ex.what() << std::endl;
			result = false;
		}
		catch (...) {			
			result = false;
		}

		return result;
    }

	auto VideoRecorder::OnCalcFrameSize(int type,QString path,double fps) -> void {
		QMutexLocker locker(&d->simulationMutex);

		QImage img3D = QImage();
		QImage imgXY = QImage();
		QImage imgYZ = QImage();
		QImage imgXZ = QImage();
		if(d->renderWindows.contains("XY") && nullptr != d->renderWindows["XY"]) {
			imgXY = d->renderWindows["XY"]->getRenderBuffer().toImage();
		}
		if(d->renderWindows.contains("YZ") && nullptr != d->renderWindows["YZ"]) {
			imgYZ = d->renderWindows["YZ"]->getRenderBuffer().toImage();
		}
		if(d->renderWindows.contains("XZ") && nullptr != d->renderWindows["XZ"]) {
			imgXZ = d->renderWindows["XZ"]->getRenderBuffer().toImage();
		}
		if(d->renderWindows["threeD"] && nullptr != d->renderWindows["threeD"]) {
		    img3D = d->renderWindows["threeD"]->getRenderBuffer().toImage();
		}
	    if(type == 0) {
			if(imgXY.isNull()) {
				d->curScreenSize = cv::Size();
			}
			d->curScreenSize = cv::Size(imgXY.width(), imgXY.height());
	    }
	    if(type == 1 ) {
	        if(imgYZ.isNull()) {
				d->curScreenSize = cv::Size();
	        }			
			d->curScreenSize = cv::Size(imgYZ.width(), imgYZ.height());
	    }
		if(type == 2) {
		    if(imgXZ.isNull()) {
				d->curScreenSize = cv::Size();
		    }			
			d->curScreenSize = cv::Size(imgXZ.width(), imgXZ.height());
		}
		if(type == 3) {
		    if(img3D.isNull()) {
				d->curScreenSize = cv::Size();
		    }			
			d->curScreenSize = cv::Size(img3D.width(), img3D.height());
		}
		if(type ==4) {
			switch (d->curLayoutType) {
			case MultiLayoutType::UNKNOWN:
				d->curScreenSize = cv::Size();
			case MultiLayoutType::XYPlane:
				if (imgXY.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(imgXY.width(), imgXY.height());
			case MultiLayoutType::YZPlane:
				if (imgYZ.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(imgYZ.width(), imgYZ.height());
			case MultiLayoutType::XZPlane:
				if (imgXZ.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(imgXZ.width(), imgXZ.height());
			case MultiLayoutType::Only3D:
				if (img3D.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(img3D.width(), img3D.height());
			case MultiLayoutType::TwoByTwo:
				if (img3D.isNull() || imgXY.isNull() || imgYZ.isNull() || imgXZ.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(imgXY.width() + img3D.width(), imgXZ.height() + img3D.height());
			case MultiLayoutType::HSlicesBy3D:
				if (img3D.isNull() || imgXY.isNull() || imgYZ.isNull() || imgXZ.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(imgXY.width() + img3D.width(), img3D.height());
			case MultiLayoutType::VSlicesBy3D:
				if (img3D.isNull() || imgXY.isNull() || imgYZ.isNull() || imgXZ.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(img3D.width(), img3D.height() + imgXY.height());
			case MultiLayoutType::HXY3D:
				if (img3D.isNull() || imgXY.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(img3D.width() + imgXY.width(), img3D.height());
			case MultiLayoutType::VXY3D:
				if (img3D.isNull() || imgXY.isNull()) {
					d->curScreenSize = cv::Size();
				}
				d->curScreenSize = cv::Size(img3D.width(), img3D.height() + imgXY.height());
			case MultiLayoutType::BigXY:
				d->curScreenSize = SizeBig(imgXY, img3D, imgYZ, imgXZ);
			case MultiLayoutType::BigYZ:
				d->curScreenSize = SizeBig(imgYZ, img3D, imgXY, imgXZ);
			case MultiLayoutType::BigXZ:
				d->curScreenSize = SizeBig(imgXZ, img3D, imgXY, imgYZ);
			case MultiLayoutType::Big3D:
				d->curScreenSize = SizeBig(img3D, imgXY, imgYZ, imgXZ);
			}
		}
		//d->curScreenSize = cv::Size();

		if (false == d->curScreenSize.empty()) {
			CreateCVFile(path, fps, d->curScreenSize);
		}
    }

	auto VideoRecorder::SizeBig(const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3) -> cv::Size {
		if (imageBig.isNull() || imageV1.isNull() || imageV2.isNull() || imageV3.isNull()) {
			return cv::Size();
		}
		return cv::Size(imageBig.width() + imageV1.width(), imageBig.height());
    }


	auto VideoRecorder::OnCaptureScreen(int type) -> void {
		//Capture current screen in buffer & write into CV video file		
		if(type == -1 ){
			return;
		}
		if (false == d->cvWriter.isOpened()) {			
			return;
		}
		QMutexLocker locker(&d->simulationMutex);
		if(type < 4) {//single screen capture			
			QOivRenderWindow* targetWindow{nullptr};
			switch(type) {
			case 0:
				targetWindow = d->renderWindows["XY"];
				break;
			case 1:
				targetWindow = d->renderWindows["YZ"];
				break;
			case 2:
				targetWindow = d->renderWindows["XZ"];
				break;
			case 3:
				targetWindow = d->renderWindows["threeD"];
				break;
			}
			if(nullptr ==targetWindow) {				
				return;
			}
			auto buffer = targetWindow->getRenderBuffer();
		    QImage image = buffer.toImage().convertToFormat(QImage::Format_RGB888);
			cv::Mat mat(image.height(), image.width(),
				CV_8UC3, (void*)image.constBits(),
				image.bytesPerLine());
			cv::Mat matNoAlpha;
			cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

			d->cvWriter.write(matNoAlpha);
		}else {
		    if(d->curLayoutType._to_index() == MultiLayoutType::UNKNOWN) {
				return;
		    }
			QImage img3D = QImage();
			QImage imgXY = QImage();
			QImage imgYZ = QImage();
			QImage imgXZ = QImage();

			if (d->renderWindows["threeD"] && nullptr != d->renderWindows["threeD"]) {
				auto buf3D = d->renderWindows["threeD"]->getRenderBuffer();
				img3D = buf3D.toImage().convertToFormat(QImage::Format_RGB888);
			}
			if (d->renderWindows.contains("XY") && nullptr != d->renderWindows["XY"]) {
				auto bufXY = d->renderWindows["XY"]->getRenderBuffer();
				imgXY = bufXY.toImage().convertToFormat(QImage::Format_RGB888);
			}
			if (d->renderWindows.contains("YZ") && nullptr != d->renderWindows["YZ"]) {
				auto bufYZ = d->renderWindows["YZ"]->getRenderBuffer();
				imgYZ = bufYZ.toImage().convertToFormat(QImage::Format_RGB888);
			}
			if (d->renderWindows.contains("XZ") && nullptr != d->renderWindows["XZ"]) {
				auto bufXZ = d->renderWindows["XZ"]->getRenderBuffer();
				imgXZ = bufXZ.toImage().convertToFormat(QImage::Format_RGB888);
			}
			switch(d->curLayoutType) {
			case  MultiLayoutType::TwoByTwo:
				CaptureTwoByTwo(img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::HSlicesBy3D:
				CaptureHSlicesBy3D(img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::VSlicesBy3D:
				CaptureVSlicesBy3D(img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::HXY3D://for BA single run
				CaptureHXY3D(img3D, imgXY);
				break;
			case MultiLayoutType::VXY3D:
				CaptureVXY3D(img3D, imgXY);
				break;
			case MultiLayoutType::XYPlane:
				CapturePlane(imgXY);
				break;
			case MultiLayoutType::YZPlane:
				CapturePlane(imgYZ);
				break;
			case MultiLayoutType::XZPlane:
				CapturePlane(imgXZ);
				break;
			case MultiLayoutType::Only3D:
				CapturePlane(img3D);
				break;
				//for Mask Editor
			case MultiLayoutType::BigXY:
				CaptureBig(imgXY, img3D, imgYZ, imgXZ);
				break;
			case MultiLayoutType::BigYZ:
				CaptureBig(imgYZ, img3D, imgXY, imgXZ);
				break;
			case MultiLayoutType::BigXZ:
				CaptureBig(imgXZ, img3D, imgXY, imgYZ);
				break;
			case MultiLayoutType::Big3D:
				CaptureBig(img3D, imgXY, imgYZ, imgXZ);
				break;
			}
		}		
	}
	auto VideoRecorder::CaptureTwoByTwo(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height() + imageYZ.height(), imageXY.width() + image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(0, imageXY.height(), imageXZ.width(), imageXZ.height())));

		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);

		return true;
    }

	auto VideoRecorder::CaptureHSlicesBy3D(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height(), imageXY.width() + image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(0, imageXY.height(), imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(0, imageXY.height() + imageYZ.height(), imageXZ.width(), imageXZ.height())));


		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);

		return true;
    }

	auto VideoRecorder::CaptureVSlicesBy3D(const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(imageXY.width() + imageYZ.width(), 0, imageXZ.width(), imageXZ.height())));


		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);

		return true;
    }

	auto VideoRecorder::CaptureHXY3D(const QImage& image3D, const QImage& imageXY) -> bool {
		if (image3D.isNull() || imageXY.isNull()) {
			return false;
		}
		cv::Mat target = cv::Mat::zeros(image3D.height(), image3D.width() + imageXY.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);

	    return true;						
    }

	auto VideoRecorder::CaptureVXY3D(const QImage& image3D, const QImage& imageXY) -> bool {
		if (image3D.isNull() || imageXY.isNull()) {
			return false;
		}
		cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);
		return true;
    }

	auto VideoRecorder::CapturePlane(const QImage& image) -> bool {
		if (image.isNull()) {
			return false;
		}
		cv::Mat mat(image.height(), image.width(),
			CV_8UC3, (void*)image.constBits(),
			image.bytesPerLine());
		cv::Mat matNoAlpha;
		cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);
		d->cvWriter.write(matNoAlpha);
		return true;
    }
	auto VideoRecorder::CaptureBig(const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3) -> bool {
		if (imageBig.isNull() || imageV1.isNull() || imageV2.isNull() || imageV3.isNull()) {			
			return false;
		}
		cv::Mat target = cv::Mat::zeros(imageBig.height(), imageBig.width() + imageV1.width(), CV_8UC3);

		cv::Mat matBig(imageBig.height(), imageBig.width(), CV_8UC3, (void*)imageBig.constBits(), imageBig.bytesPerLine());
		cv::Mat matV1(imageV1.height(), imageV1.width(), CV_8UC3, (void*)imageV1.constBits(), imageV1.bytesPerLine());
		cv::Mat matV2(imageV2.height(), imageV2.width(), CV_8UC3, (void*)imageV2.constBits(), imageV2.bytesPerLine());
		cv::Mat matV3(imageV3.height(), imageV3.width(), CV_8UC3, (void*)imageV3.constBits(), imageV3.bytesPerLine());

		matBig.copyTo(target(cv::Rect(0, 0, imageBig.width(), imageBig.height())));
		matV1.copyTo(target(cv::Rect(imageBig.width(), 0, imageV1.width(), imageV1.height())));
		matV2.copyTo(target(cv::Rect(imageBig.width(), imageV1.height(), imageV2.width(), imageV2.height())));
		matV3.copyTo(target(cv::Rect(imageBig.width(), imageV1.height() + imageV2.height(), imageV3.width(), imageV3.height())));

		cv::Mat matNoAlpha;
		cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		d->cvWriter.write(matNoAlpha);
		return true;
    }


	auto VideoRecorder::RecordScreen(QString path, RecordType type, QList<RecordAction> process) -> bool {
		auto totalEndTime = 0.f;
		const auto fps = 30.f;		
		if (type != +RecordType::None) {			
			emit captureScreenSize(type,path,fps);						
		}

		struct SimulationAction {
			enum ActiveWindow {
				NONE = -1,
				XY = 0,
				YZ = 1,
				XZ = 2,
				VOLUME = 3,
				ALL = 4,
			};

			struct OrbitInfo {
				OrbitInfo(float a1, float a2, float a3, float v)
					: x(a1), y(a2), z(a3), value(v) {
				}

				float x = 0.f;
				float y = 0.f;
				float z = 0.f;
				float value = 0.f;
			};

			ActiveWindow window = ActiveWindow::NONE;

			QList<OrbitInfo> orbit; // axis x, y, z, orbit
			QList<int> slice; // plane, index,
			QList<int> timelapse; // time step
		};
		
		// make simulation per frame
		QList<SimulationAction> simulationActionList;
		for(auto item : process){
			/*if (nullptr == item) {
				continue;
			}*/

			if (totalEndTime < item.endTime) {
				totalEndTime = item.endTime;
			}
		}

		const auto totalFrame = totalEndTime * fps;

		for (auto item : process) {
			/*if (nullptr == item) {
				continue;
			}*/

			const auto actionFrame = (item.endTime - item.startTime) * fps;

			SimulationAction action;
			if (item.type == +ActionType::Orbit) {
				action.window = SimulationAction::VOLUME;

				float axis[3];

				if (item.axis == +ActionAxis::T) {
					const auto direction = 4;

					// when orbit is 360(default), tilt is 2 degree
					const auto tilt = qDegreesToRadians(static_cast<float>(item.finish) / 180.f) * 10.f;
					auto tic = tilt * direction / totalFrame;

					if (true == item.reverse) {
						tic *= -1;
					}

					// start frame
					for (auto i = 1; i < item.startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					const auto repeat = totalFrame / direction / 2;
					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				}
				else {
					if (item.axis == +ActionAxis::X) {
						axis[0] = 0.f;
						axis[1] = 1.f;
						axis[2] = 0.f;
					}
					else if (item.axis == +ActionAxis::Y) {
						axis[0] = 1.f;
						axis[1] = 0.f;
						axis[2] = 0.f;
					}
					else if (item.axis == +ActionAxis::Z) {
						axis[0] = 0.f;
						axis[1] = 0.f;
						axis[2] = 1.f;
					}

					const auto radian = qDegreesToRadians(static_cast<float>(item.finish));
					auto tic = radian / actionFrame;
					if (true == item.reverse) {
						tic *= -1;
					}

					// start frame
					const auto startRadian = qDegreesToRadians(static_cast<float>(item.begin - 180));
					action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], startRadian));

					for (auto i = 1; i < item.startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					for (auto i = 0; i < item.endTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				}
			}
			else if (item.type == +ActionType::Slice) {
				if (item.windowType == +ActionWindow::XY) {
					action.window = SimulationAction::XY;
				}
				else if (item.windowType == +ActionWindow::YZ) {
					action.window = SimulationAction::YZ;
				}
				else if (item.windowType == +ActionWindow::XZ) {
					action.window = SimulationAction::XZ;
				}

				// start frame
				for (auto i = 0; i < item.startTime * fps; ++i) {
					action.slice.append(-1);
				}

				// time frame
				auto startIndex = item.begin;
				auto endIndex = item.finish;
				float tic = (endIndex - startIndex + 1) / (actionFrame - 2);

				if (true == item.reverse) {
					startIndex = item.finish;
					endIndex = item.begin;
				}

				// time frame -> first
				action.slice.append(startIndex);

				for (auto i = 1; i < actionFrame - 1; ++i) {
					auto index = startIndex + ceil(i * tic);

					if (true == item.reverse) {
						index = startIndex - ceil(i * tic);
					}

					if (index < item.begin) {
						index = item.begin;
					}

					if (index > item.finish) {
						index = item.finish;
					}

					action.slice.append(index);
				}

				// time frame -> last
				action.slice.append(endIndex);

				// end frame
				for (auto i = 0; i < totalFrame - actionFrame; ++i) {
					action.slice.append(-1);
				}
			}
			else if (item.type == +ActionType::Timelapse) {
				action.window = SimulationAction::ALL;
				// start frame
				for (auto i = 0; i < item.startTime * fps; ++i) {
					action.timelapse.append(-1);
				}

				auto startIndex = item.begin;
				auto endIndex = item.finish;				
				float tic = (endIndex - startIndex + 1) / (actionFrame);

				if (true == item.reverse) {
					startIndex = item.finish;
					endIndex = item.begin;
				}

				// time frame -> first
				for (auto i = 0; i < actionFrame; i++) {
					auto index = startIndex + i * tic;

					if (true == item.reverse) {
						index = startIndex - i * tic;
					}

					if (index < item.begin) {
						index = item.begin;
					}

					if (index > item.finish) {
						index = item.finish;
					}
					action.timelapse.append((int)index);
				}
				// time frame -> last
				//action.timelapse.append(endIndex);
				// end frame
				for (auto i = 0; i < totalFrame - actionFrame - 1; ++i) {
					action.timelapse.append(-1);
				}
			}

			simulationActionList.append(action);
		}

		// save buffer
		auto interval = 1000.f / fps;
		auto prevTimeStep = -1;				

		if(d->renderWindows["threeD"]) {
			//clear orbit process
		}
		//d->qOiv3DRenderWindow->clearOrbit();

		for (auto i = 0; i < totalFrame; ++i) {
			if (false == d->simulating) {				
				break;
			}
			auto tpStart = std::chrono::system_clock::now();
			{
				for (const auto& action : simulationActionList) {
					if (false == d->simulating) {
						break;
					}
					if (action.window == SimulationAction::VOLUME) {
						if (0 != action.orbit.at(i).value) {
							if(d->renderWindows["threeD"]) {
								d->renderWindows["threeD"]->rotateTick(SbVec3f(action.orbit.at(i).x,
									action.orbit.at(i).y,
									action.orbit.at(i).z),
									action.orbit.at(i).value);
								/*d->qOiv3DRenderWindow->rotate(action.orbit.at(i).x,
									action.orbit.at(i).y,
									action.orbit.at(i).z,
									action.orbit.at(i).value);*/
							}
						}
					}
					else if (action.window == SimulationAction::ALL) {
						if (action.timelapse.count() > i) {
							if (0 <= action.timelapse.at(i)) {
								if (prevTimeStep != action.timelapse.at(i)) {
									prevTimeStep = action.timelapse.at(i);
									emit timeStepChanged(d->time_points[prevTimeStep]);

								}
							}
						}
					}
					else {
						if (action.slice.count() > i) {
							if (0 <= action.slice.at(i)) {
								emit moveSlice(action.window, action.slice.at(i));
							}
						}
					}
				}
				if (type != +RecordType::None) {
					//OnCaptureScreen(type._to_integral());
					emit captureScreen(type._to_integral());
				}				
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		if(d->renderWindows["threeD"]) {			
			d->renderWindows["threeD"]->clearOrbitCenter();
		}
				
		d->simulating = false;

		return true;
    }
}
