#include <QPixmap>
#include <QMap>

#include "ScreenShot.h"

#pragma warning(disable:4267)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoOffscreenRenderArea.h>
#include <Inventor/devices/SoGLContext.h>
#include <Inventor/nodes/SoNode.h>
#include <opencv2/opencv.hpp>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include "OivMinimap.h"
#include "OivRangeBar.h"
#include "OivScaleBar.h"
#pragma warning(pop)

#include <QOivRenderWindow.h>

#include <ToastMessageBox.h>


namespace TC {
	struct ScreenShot::Impl {
		QMap<QString, QOivRenderWindow*> renderWindows;
		QString volName { QString() };

		auto SetUpsampleText(SoNode* root, int upsample, bool reverse = false) -> void;
		auto SetUpsampleWidgets(SoNode* root, int upsample = 1) -> void;
	};

	auto ScreenShot::Impl::SetUpsampleText(SoNode* root, int upsample, bool reverse) -> void {
		auto fonts = MedicalHelper::findNodes<SoFont>(root);
		QMap<QString, bool> named_font;
		for (auto i = 0; i < fonts.size(); i++) {
			if (fonts[i]->getName().getLength() > 0) {
				auto font_key = QString(fonts[i]->getName().getString());
				if (named_font.contains(font_key)) {
					continue;
				}
				named_font[font_key] = true;
			}
			if (reverse) {
				fonts[i]->size = fonts[i]->size.getValue() / static_cast<float>(upsample);
			} else {
				fonts[i]->size = fonts[i]->size.getValue() * static_cast<float>(upsample);
			}
		}
	}

	auto ScreenShot::Impl::SetUpsampleWidgets(SoNode* root, int upsample) -> void {
		auto sceneRoot = MedicalHelper::find<OivScaleBar>(root);
		if (sceneRoot) {
			sceneRoot->SetUpsample(upsample);
		}
		auto scalarRoot = MedicalHelper::find<OivRangeBar>(root);
		if (scalarRoot) {
			scalarRoot->SetUpsample(upsample);
		}
		auto minimap = MedicalHelper::find<OivMinimap>(root);
		if (minimap) {
			minimap->SetUpsample(upsample);
		}
	}

	ScreenShot::ScreenShot() : d { new Impl } { }

	ScreenShot::~ScreenShot() { }

	auto ScreenShot::SetRenderWindow(QOivRenderWindow* win, QString name) -> void {
		d->renderWindows[name] = win;
	}

	auto ScreenShot::SetVolumeName(const QString& name) -> void {
		d->volName = name;
	}

	auto ScreenShot::Clear() -> void {
		d->renderWindows.clear();
	}

	auto ScreenShot::CaptureSlice(CaptureType type, QString path, int upsampling) -> bool {
		if (true == path.isEmpty()) {
			return false;
		}
		const int sliceIndex = type._to_integral();
		if (false == d->renderWindows.contains(type._to_string())) {
			return false;
		}
		auto renderWindow = d->renderWindows[type._to_string()];
		if (nullptr == renderWindow)
			return false;
		renderWindow->setWindowTitleViz(false);
		renderWindow->setSettingIconViz(false);
		const auto root = renderWindow->getSceneGraph();

		QString text;
		d->SetUpsampleText(root, upsampling);
		d->SetUpsampleWidgets(root, upsampling);
		if (true == renderWindow->saveSnapshot(path, upsampling)) {
			text = tr("The slice screenshot has been saved successfully.\n%1").arg(path);
		} else {
			text = tr("Failed to save slice screenshot.");
		}
		d->SetUpsampleText(root, upsampling, true);
		d->SetUpsampleWidgets(root);

		auto toastBox = new TC::ToastMessageBox;
		toastBox->Show(text);

		renderWindow->setWindowTitleViz(true);
		renderWindow->setSettingIconViz(true);

		return true;
	}

	auto ScreenShot::CaptureVolume(QString path, int upsampling) -> bool {
		if (true == path.isEmpty()) {
			return false;
		}
		if (false == d->renderWindows.contains("threeD")) {
			return false;
		}
		auto renderWindow = d->renderWindows["threeD"];
		if (nullptr == renderWindow)
			return false;
		renderWindow->setWindowTitleViz(false);
		renderWindow->setSettingIconViz(false);
		const auto root = renderWindow->getSceneGraph();

		QString text;
		d->SetUpsampleText(root, upsampling);
		d->SetUpsampleWidgets(root, upsampling);
		if (true == renderWindow->saveSnapshot(path, upsampling)) {
			text = tr("The 3D screenshot has been saved successfully.\n%1").arg(path);
		} else {
			text = tr("Failed to save 3D screenshot.");
		}
		d->SetUpsampleWidgets(root);
		d->SetUpsampleText(root, upsampling, true);
		auto toastMessageBox = new TC::ToastMessageBox;
		toastMessageBox->Show(text);

		renderWindow->setWindowTitleViz(true);
		renderWindow->setSettingIconViz(true);

		return true;
	}

	auto ScreenShot::ChangeDirection(DirectionType type) -> bool {
		if (false == d->renderWindows.contains("threeD")) {
			return false;
		}
		auto renderWindow = d->renderWindows["threeD"];
		if (nullptr == renderWindow)
			return false;

		renderWindow->SetViewDirection(type, d->volName);

		return true;
	}

	auto ScreenShot::CaptureMultiView(QString path, int upsampling, MultiLayoutType type) -> bool {
		Q_UNUSED(upsampling)
		if (true == path.isEmpty()) {
			return false;
		}
		for (auto window : d->renderWindows) {
			if (nullptr == window)
				continue;
			window->setSettingIconViz(false);
			window->setWindowTitleViz(false);
		}

		QImage img3D = QImage();
		QImage imgXY = QImage();
		QImage imgYZ = QImage();
		QImage imgXZ = QImage();

		if (d->renderWindows.contains("threeD") && nullptr != d->renderWindows["threeD"]) {
			auto buf3D = d->renderWindows["threeD"]->getRenderBuffer();
			img3D = buf3D.toImage().convertToFormat(QImage::Format_RGB888);
		}
		if (d->renderWindows.contains("XY") && nullptr != d->renderWindows["XY"]) {
			auto bufXY = d->renderWindows["XY"]->getRenderBuffer();
			imgXY = bufXY.toImage().convertToFormat(QImage::Format_RGB888);
		}
		if (d->renderWindows.contains("YZ") && nullptr != d->renderWindows["YZ"]) {
			auto bufYZ = d->renderWindows["YZ"]->getRenderBuffer();
			imgYZ = bufYZ.toImage().convertToFormat(QImage::Format_RGB888);
		}
		if (d->renderWindows.contains("XZ") && nullptr != d->renderWindows["XZ"]) {
			auto bufXZ = d->renderWindows["XZ"]->getRenderBuffer();
			imgXZ = bufXZ.toImage().convertToFormat(QImage::Format_RGB888);
		}

		auto success = false;
		switch (type) {
			//for 3D Visualizer
			case MultiLayoutType::UNKNOWN:
				success = false;
				break;
			case MultiLayoutType::TwoByTwo:
				success = CaptureTwoByTwo(path, img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::HSlicesBy3D:
				success = CaptureHSlicesBy3D(path, img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::VSlicesBy3D:
				success = CaptureVSlicesBy3D(path, img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::HXY3D://for BA single run
				success = CaptureHXY3D(path, img3D, imgXY);
				break;
			case MultiLayoutType::VXY3D:
				success = CaptureVXY3D(path, img3D, imgXY);
				break;
			case MultiLayoutType::XYPlane:
				success = CapturePlane(path, imgXY);
				break;
			case MultiLayoutType::YZPlane:
				success = CapturePlane(path, imgYZ);
				break;
			case MultiLayoutType::XZPlane:
				success = CapturePlane(path, imgXZ);
				break;
			case MultiLayoutType::Only3D:
				success = CapturePlane(path, img3D);
				break;
			//for Mask Editor
			case MultiLayoutType::BigXY:
				success = CaptureBig(path, imgXY, img3D, imgYZ, imgXZ);
				break;
			case MultiLayoutType::BigYZ:
				success = CaptureBig(path, imgYZ, img3D, imgXY, imgXZ);
				break;
			case MultiLayoutType::BigXZ:
				success = CaptureBig(path, imgXZ, img3D, imgXY, imgYZ);
				break;
			case MultiLayoutType::Big3D:
				success = CaptureBig(path, img3D, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::FLXY:
				success = CaptureFL(path, imgXY, imgYZ, imgXZ);
				break;
			case MultiLayoutType::FLYZ:
				success = CaptureFL(path, imgYZ, imgXY, imgXZ);
				break;
			case MultiLayoutType::FLXZ:
				success = CaptureFL(path, imgXZ, imgYZ, imgXY);
				break;
		}
		QString text;
		if (true == success) {
			text = tr("The multi-view screenshot has been saved successfully.\n%1").arg(path);
		} else {
			std::cout << "Failed on OpenCV imwrite" << std::endl;
			text = tr("Failed to save multi-view screenshot.");
		}

		auto toastMessageBox = new TC::ToastMessageBox(nullptr);
		toastMessageBox->Show(text);

		for (auto window : d->renderWindows) {
			if (nullptr == window)
				continue;
			window->setSettingIconViz(true);
			window->setWindowTitleViz(true);
		}
		return true;
	}

	auto ScreenShot::CaptureTwoByTwo(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height() + imageYZ.height(), imageXY.width() + image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(0, imageXY.height(), imageXZ.width(), imageXZ.height())));

		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);

		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureHSlicesBy3D(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height(), imageXY.width() + image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(0, imageXY.height(), imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(0, imageXY.height() + imageYZ.height(), imageXZ.width(), imageXZ.height())));


		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);

		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureVSlicesBy3D(QString path, const QImage& image3D, const QImage& imageXY, const QImage& imageYZ, const QImage& imageXZ) -> bool {
		if (image3D.isNull() || imageXY.isNull() || imageYZ.isNull() || imageXZ.isNull()) {
			return false;
		}

		cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
		cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
		cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
		matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
		matXZ.copyTo(target(cv::Rect(imageXY.width() + imageYZ.width(), 0, imageXZ.width(), imageXZ.height())));


		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);

		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);

		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureHXY3D(QString path, const QImage& image3D, const QImage& imageXY) -> bool {
		if (image3D.isNull() || imageXY.isNull()) {
			return false;
		}
		cv::Mat target = cv::Mat::zeros(image3D.height(), image3D.width() + imageXY.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);

		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureVXY3D(QString path, const QImage& image3D, const QImage& imageXY) -> bool {
		if (image3D.isNull() || imageXY.isNull()) {
			return false;
		}
		cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

		cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
		cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

		mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
		matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);
		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CapturePlane(QString path, const QImage& image) -> bool {
		if (image.isNull()) {
			return false;
		}
		cv::Mat mat(image.height(), image.width(),
					CV_8UC3, (void*)image.constBits(),
					image.bytesPerLine());
		//cv::Mat matNoAlpha;
		//cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);
		QImage imgIn = QImage((uchar*)mat.data, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);

		//return cv::imwrite(path.toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureBig(QString path, const QImage& imageBig, const QImage& imageV1, const QImage& imageV2, const QImage& imageV3) -> bool {
		if (imageBig.isNull() || imageV1.isNull() || imageV2.isNull() || imageV3.isNull()) {
			std::cout << "some image is null" << std::endl;
			return false;
		}
		cv::Mat target = cv::Mat::zeros(imageBig.height(), imageBig.width() + imageV1.width(), CV_8UC3);

		cv::Mat matBig(imageBig.height(), imageBig.width(), CV_8UC3, (void*)imageBig.constBits(), imageBig.bytesPerLine());
		cv::Mat matV1(imageV1.height(), imageV1.width(), CV_8UC3, (void*)imageV1.constBits(), imageV1.bytesPerLine());
		cv::Mat matV2(imageV2.height(), imageV2.width(), CV_8UC3, (void*)imageV2.constBits(), imageV2.bytesPerLine());
		cv::Mat matV3(imageV3.height(), imageV3.width(), CV_8UC3, (void*)imageV3.constBits(), imageV3.bytesPerLine());

		matBig.copyTo(target(cv::Rect(0, 0, imageBig.width(), imageBig.height())));
		matV1.copyTo(target(cv::Rect(imageBig.width(), 0, imageV1.width(), imageV1.height())));
		matV2.copyTo(target(cv::Rect(imageBig.width(), imageV1.height(), imageV2.width(), imageV2.height())));
		matV3.copyTo(target(cv::Rect(imageBig.width(), imageV1.height() + imageV2.height(), imageV3.width(), imageV3.height())));


		//cv::Mat matNoAlpha;
		//cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);
		//return cv::imwrite(path.toLocal8Bit().toStdString(), matNoAlpha);
		return imgIn.save(path);
	}

	auto ScreenShot::CaptureFL(QString path, const QImage& imageBig, const QImage& imageRight, const QImage& imageBottom) -> bool {
		if (imageBig.isNull() || imageRight.isNull() || imageBottom.isNull()) {
			return false;
		}
		cv::Mat target = cv::Mat::zeros(imageBig.height() + imageBottom.height(), imageBig.width() + imageRight.width(),CV_8UC3);

		cv::Mat matBig(imageBig.height(), imageBig.width(), CV_8UC3, (void*)imageBig.constBits(), imageBig.bytesPerLine());
		cv::Mat matRight(imageRight.height(), imageRight.width(), CV_8UC3, (void*)imageRight.constBits(), imageRight.bytesPerLine());
		cv::Mat matBottom(imageBottom.height(), imageBottom.width(), CV_8UC3, (void*)imageBottom.constBits(), imageBottom.bytesPerLine());

		matBig.copyTo(target(cv::Rect(0, 0, imageBig.width(), imageBig.height())));
		matRight.copyTo(target(cv::Rect(imageBig.width(), 0, imageRight.width(), imageRight.height())));
		matBottom.copyTo(target(cv::Rect(0, imageBig.height(), imageBottom.width(), imageBottom.height())));

		QImage imgIn = QImage((uchar*)target.data, target.cols, target.rows, target.step, QImage::Format_RGB888);

		return imgIn.save(path);
	}
}
