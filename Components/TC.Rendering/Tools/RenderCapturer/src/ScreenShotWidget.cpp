#include "ui_ScreenShotWidget.h"

#include <QFileDialog>
#include <QSettings>
#include <QStandardPaths>

#include "MultipleCaptureDialog.h"

#include "ScreenShotWidget.h"

namespace TC {
    struct ScreenShotWidget::Impl {
        Ui::ScreenShotWidgetUI* ui{ nullptr };
        ScreenShot::Pointer screenshotEngine{ nullptr };

        QString tcfName{ QString() };        
        MultiLayoutType curLayoutType{ MultiLayoutType::UNKNOWN };
    };
    ScreenShotWidget::ScreenShotWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ScreenShotWidgetUI;
        d->ui->setupUi(this);

        d->screenshotEngine = std::make_shared<ScreenShot>();

        Init();
        InitConnections();
        SetEnableUI(false);
    }
    ScreenShotWidget::~ScreenShotWidget() {

    }
    auto ScreenShotWidget::Init() -> void {
        d->ui->sliceUpsampleSpin->setRange(1, 5);
        d->ui->volumeUpsampleSpin->setRange(1, 5);
        d->ui->multiUpsampleSpin->setRange(1, 5);

        d->ui->sliceUpsampleSpin->setValue(3);

        d->ui->multiUpsampleLabel->hide();
        d->ui->multiUpsampleSpin->hide();

        //set object names
        d->ui->mainWidget->setObjectName("panel-contents");
        d->ui->sliceUpsampleLabel->setObjectName("h9");
        d->ui->sliceCaptureBtn->setObjectName("bt-round-gray700");

        d->ui->volumeUpsampleLabel->setObjectName("h9");
        d->ui->volumeCaptureBtn->setObjectName("bt-round-gray700");
        d->ui->volumeAllCaptureBtn->setObjectName("bt-round-gray700");

        d->ui->multiUpsampleLabel->setObjectName("h9");
        d->ui->multiCaptureBtn->setObjectName("bt-round-gray700");

        d->ui->allRadioBtn->setChecked(true);

    }
    auto ScreenShotWidget::InitConnections() -> void {
        connect(d->ui->sliceCaptureBtn, SIGNAL(clicked()), this, SLOT(OnSliceCapture()));
        connect(d->ui->volumeCaptureBtn, SIGNAL(clicked()), this, SLOT(OnVolumeCapture()));
        connect(d->ui->volumeAllCaptureBtn, SIGNAL(clicked()), this, SLOT(OnVolumeAllCapture()));
        connect(d->ui->multiCaptureBtn, SIGNAL(clicked()), this, SLOT(OnMultiCapture()));
    }
    auto ScreenShotWidget::SetTcfPath(const QString& path) -> void {
        QFileInfo fileInfo(path);
        d->tcfName = fileInfo.completeBaseName();        
    }
    auto ScreenShotWidget::SetVolumeName(const QString& name) -> void {
        d->screenshotEngine->SetVolumeName(name);
    }
    auto ScreenShotWidget::EnableUIComponent(const QString& name) -> void {
        if(name == "threeD") {
            d->ui->volumeCaptureBtn->setEnabled(true);
            d->ui->volumeUpsampleSpin->setEnabled(true);
            d->ui->volumeAllCaptureBtn->setEnabled(true);
        }else if (name == "XY") {
            d->ui->xyRadioBtn->setEnabled(true);
            d->ui->sliceUpsampleSpin->setEnabled(true);
            d->ui->sliceCaptureBtn->setEnabled(true);
            d->ui->xyRadioBtn->setChecked(true);
        }else if(name == "YZ") {
            d->ui->yzRadioBtn->setEnabled(true);
            d->ui->sliceUpsampleSpin->setEnabled(true);
            d->ui->sliceCaptureBtn->setEnabled(true);
        }else if(name == "XZ") {
            d->ui->xzRadioBtn->setEnabled(true);
            d->ui->sliceUpsampleSpin->setEnabled(true);
            d->ui->sliceCaptureBtn->setEnabled(true);
        }
        d->ui->multiUpsampleSpin->setEnabled(true);
        d->ui->multiCaptureBtn->setEnabled(true);

        d->ui->allRadioBtn->setEnabled(true);
    }
    auto ScreenShotWidget::SetEnableUI(bool enable) -> void {
        //2D
        d->ui->xyRadioBtn->setEnabled(enable);
        d->ui->yzRadioBtn->setEnabled(enable);
        d->ui->xzRadioBtn->setEnabled(enable);
        d->ui->allRadioBtn->setEnabled(enable);
        d->ui->sliceCaptureBtn->setEnabled(enable);
        d->ui->sliceUpsampleSpin->setEnabled(enable);

        //3D
        d->ui->volumeCaptureBtn->setEnabled(enable);
        d->ui->volumeAllCaptureBtn->setEnabled(enable);
        d->ui->volumeUpsampleSpin->setEnabled(enable);

        //multi view
        d->ui->multiUpsampleSpin->setEnabled(enable);
        d->ui->multiCaptureBtn->setEnabled(enable);
    }
    void ScreenShotWidget::OnSliceCapture() {
        if(true == d->tcfName.isEmpty()) {
            return;
        }
        TC::CaptureType type = TC::CaptureType::XY;
        if(true == d->ui->xyRadioBtn->isChecked()) {
            type = TC::CaptureType::XY;
        }else if(true == d->ui->yzRadioBtn->isChecked()) {
            type = TC::CaptureType::YZ;
        }else if(true == d->ui->xzRadioBtn->isChecked()) {
            type = TC::CaptureType::XZ;
        }else if(true == d->ui->allRadioBtn->isChecked()) {
            type = TC::CaptureType::ALL;
        }

        const int upSampling = d->ui->sliceUpsampleSpin->value();

        if(+TC::CaptureType::ALL == type) {
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent2DCapture").toString();
            const auto directory = QFileDialog::getExistingDirectory(this, "Select a directory to save screenshot files", prev);
            if (true == directory.isEmpty()) {
                return;
            }

            QDir dir(directory);
            QString file_name[3];
            QStringList dupFiles;

            for (auto i = 0; i < 3; i++) {
                auto index = TC::CaptureType::_from_integral(i);
                file_name[i] = d->tcfName + "_" + index._to_string() + "_Plane.png";
                if (dir.exists(file_name[i])) {
                    dupFiles.push_back(file_name[i]);
                }
            }

            if (dupFiles.size() > 0) {
                QString script = QString();
                for (auto i = 0; i < dupFiles.size(); i++) {
                    auto dup = dupFiles[i];
                    script += dup;
                    if (i < dupFiles.size() - 1)
                        script += "\n";
                }
                MultipleCaptureDialog capture(script);
                capture.setWindowTitle("Warning");
                auto result = capture.exec();
                if (result < 1) {
                    return;
                }
            }

            //check directory already exist                                                
            QSettings("Tomocube", "TomoAnalysis").setValue("recent2DCapture", directory);

            for (int i = 0; i < 3; ++i) {
                auto ind = TC::CaptureType::_from_integral(i);
                const auto savePath = directory + "/" + file_name[i];
                d->screenshotEngine->CaptureSlice(ind, savePath, upSampling);                
            }
        }
        else {
            const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent2DCapture", desktopPath).toString();

            const QString name = d->tcfName + "_" + type._to_string() + "_Plane.png";

            const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
            if (true == savePath.isEmpty()) {
                return;
            }

            QSettings("Tomocube", "TomoAnalysis").setValue("recent2DCapture", QFileInfo(savePath).dir().path());

            d->screenshotEngine->CaptureSlice(type, savePath, upSampling);
        }
    }
    void ScreenShotWidget::OnVolumeCapture() {
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent3DSingleCapture", desktopPath).toString();

        const QString name = d->tcfName + "_3D_screenshot.png";

        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
        if (true == savePath.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recent3DSingleCapture", QFileInfo(savePath).dir().path());

        const int upsampling = d->ui->volumeUpsampleSpin->value();

        d->screenshotEngine->CaptureVolume(savePath, upsampling);        
    }
    void ScreenShotWidget::OnVolumeAllCapture() {
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const int upsampling = d->ui->volumeUpsampleSpin->value();

        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent3DAllCapture").toString();
        const auto directory = QFileDialog::getExistingDirectory(this, "Select a directory to save screenshot files", prev);
        if (true == directory.isEmpty()) {
            return;
        }
        QSettings("Tomocube", "TomoAnalysis").setValue("recent3DAllCapture", directory);

        for (int i = 0; i < 3; ++i) {
            const auto name = d->tcfName + QString("_3D_View%1.png").arg(i + 1);
            const auto filePath = directory + "/" + name;

            d->screenshotEngine->ChangeDirection(TC::DirectionType::_from_index(i));
            d->screenshotEngine->CaptureVolume(filePath, upsampling);            
        }
    }
    void ScreenShotWidget::OnMultiCapture() {
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentMultiCapture", desktopPath).toString();
        const QString name = d->tcfName + "_multi-view_screenshot.png";

        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
        if (true == savePath.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentMultiCapture", QFileInfo(savePath).dir().path());

        d->screenshotEngine->CaptureMultiView(savePath, 1,d->curLayoutType);
    }
    auto ScreenShotWidget::SetRenderWindow(QOivRenderWindow* win, QString name) -> void {
        d->screenshotEngine->SetRenderWindow(win, name);
        EnableUIComponent(name);
    }
    auto ScreenShotWidget::SetMultiLayerType(MultiLayoutType type) -> void {
        d->curLayoutType = type;
    }
    auto ScreenShotWidget::force2D() -> void {
        d->ui->xyRadioBtn->setChecked(true);
        d->ui->yzRadioBtn->setEnabled(false);
        d->ui->xzRadioBtn->setEnabled(false);
        d->ui->allRadioBtn->setEnabled(false);
        d->ui->yzRadioBtn->hide();
        d->ui->xzRadioBtn->hide();
        d->ui->allRadioBtn->hide();
        d->ui->tabWidget->setCurrentIndex(0);
        d->ui->tabWidget->tabBar()->setEnabled(false);
        d->ui->tabWidget->tabBar()->removeTab(2);
        d->ui->tabWidget->tabBar()->removeTab(1);
    }
    auto ScreenShotWidget::restore3D() -> void {
        d->ui->yzRadioBtn->setEnabled(true);
        d->ui->xzRadioBtn->setEnabled(true);
        d->ui->allRadioBtn->setEnabled(true);
        d->ui->yzRadioBtn->show();
        d->ui->xzRadioBtn->show();
        d->ui->allRadioBtn->show();
        d->ui->tabWidget->tabBar()->setEnabled(true);
    }
    auto ScreenShotWidget::ToggleTab(int idx, bool show) -> void {
        d->ui->tabWidget->setTabEnabled(idx, show);
    }
}