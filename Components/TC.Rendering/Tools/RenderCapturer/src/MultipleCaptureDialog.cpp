#include "MultipleCaptureDialog.h"

#include "ui_MultipleCaptureDialog.h"

#include <QMessageBox>

namespace TC {
	struct MultipleCaptureDialog::Impl {
        Ui::Dialog* ui{ nullptr };
	};
	MultipleCaptureDialog::MultipleCaptureDialog(QString labelText, QWidget* parent) : QDialog(parent), d{ new Impl }{
        d->ui = new Ui::Dialog;
        d->ui->setupUi(this);

        d->ui->label->setText(labelText);
        setObjectName("widget-popup");
        d->ui->label->setObjectName("h8");
        d->ui->replaceBtn->setObjectName("bt-square-primary");
        d->ui->cancelBtn->setObjectName("bt-square-gray");
        connect(d->ui->replaceBtn, SIGNAL(clicked()), this, SLOT(OnReplaceBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
    }
	MultipleCaptureDialog::~MultipleCaptureDialog() {
        
    }
	void MultipleCaptureDialog::OnCancelBtn() {
        reject();
    }
	void MultipleCaptureDialog::OnReplaceBtn() {
        accept();
    }
}