#include <iostream>
#include <QFileDialog>
#include <QHeaderView>
#include <QPainter>
#include <QTime>
#include <QMouseEvent>
#include <QStyle>

#include <UIUtility.h>
#include "ui_VideoEditWidget.h"
#include "VideoEditWidget.h"

namespace TC {
	struct TickMarkWidget::Impl {
		int interval = 40;
		int total_time = 90;//seconds;
	};

	TickMarkWidget::TickMarkWidget(QWidget* parent)
		: QWidget(parent), d{ new Impl } {

		this->setFixedHeight(30);
		this->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding));
		setObjectName("panel-contents");				
	}

	TickMarkWidget::~TickMarkWidget() = default;

    void TickMarkWidget::SetInterval(const int& interval) {
		d->interval = interval;
		setMinimumWidth(d->interval * d->total_time * 2);
	}

	void TickMarkWidget::paintEvent(QPaintEvent* event) {
		Q_UNUSED(event)
		QPainter painter(this);
		QFont font = painter.font();
		font.setPointSize(10);
		painter.setFont(font);
		painter.setRenderHints(QPainter::TextAntialiasing | QPainter::Antialiasing);

		QRectF rect = this->rect();

		DrawTicker(&painter);
	}

	void TickMarkWidget::DrawTicker(QPainter* painter) const {		
		auto rect = this->rect();

		for (int pos = rect.left(), count = 0; pos < rect.left() + d->interval * d->total_time *2; pos += d->interval, ++count) {
			int x1 = pos;
			int y1 = this->rect().top() + 10;
			int x2 = pos;
			int y2 = this->rect().bottom();

			if (0 == count % 2) {
				QPen tickerPen(QColor("#5f6f7a"), 1);
				painter->setPen(tickerPen);
				painter->drawLine(QLineF(x1, y1 + 10, x2, y2));

			    // draw text
				if(pos != rect.left() && pos != rect.right()) {
					QPen textPen(QColor("#5b8b97"), 1);
					//QTime time(0, count/2, 0);
					QTime time(0, 0, 0);
					time = time.addSecs(count / 2);
					painter->drawText(x1 - 15, y1 + 1, time.toString("mm:ss"));					
				}
			}
			else {
				QPen tickerPen(QColor("#5f6f7a"), 1);
				painter->setPen(tickerPen);
				painter->drawLine(QLineF(x1, y1 + 12, x2, y2));
			}
		}

		QPen tickerPen(QColor("#5f6f7a"), 1);
		painter->setPen(tickerPen);

	    const int x1 = rect.left();
        const int y1 = rect.bottom();
        //const int x2 = rect.right();
		const int x2 = rect.left() + d->interval * d->total_time*2;// rect.right();
        const int y2 = rect.bottom();

		painter->drawLine(QLineF(x1, y1, x2, y2));		
    }

	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////

	struct RecordActionItem::Impl {
		ActionType type = ActionType::None;

		int interval = 40;

		QLabel* beginMarker = nullptr;
		QLabel* endMarker = nullptr;
		QLabel* box = nullptr;

		const int markerWidth = 10;
		const int markerHeight = 30;
		const int bodyHeight = 30;

		OrbitInfo* orbitInfo = nullptr;
		SliceInfo* sliceInfo = nullptr;
		TimeInfo* timeInfo = nullptr;

		ActionType checkOverlapType = ActionType::None;
	};

	RecordActionItem::RecordActionItem(QWidget* parent)
		: QWidget(parent), d{ new Impl } {

		this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
		setAttribute(Qt::WA_OpaquePaintEvent);

		d->beginMarker = new QLabel(this);
		d->beginMarker->setObjectName("handle-track-cut-left");
		d->beginMarker->setFixedSize(d->markerWidth, d->markerHeight);
		d->beginMarker->setScaledContents(true);
		d->beginMarker->setCursor(Qt::SizeHorCursor);
		d->beginMarker->move(0, 0);
		d->beginMarker->installEventFilter(this);

		d->endMarker = new QLabel(this);
		d->endMarker->setObjectName("handle-track-cut-right");
		d->endMarker->setFixedSize(d->markerWidth, d->markerHeight);
		d->endMarker->setScaledContents(true);
		d->endMarker->move(d->markerWidth + 20, 0);
		d->endMarker->setCursor(Qt::SizeHorCursor);
		d->endMarker->installEventFilter(this);

		d->box = new QLabel(this);
		d->box->setObjectName("box-track");
		d->box->setCursor(Qt::SplitHCursor);
		d->box->installEventFilter(this);
		this->UpdateBox();
	}

	RecordActionItem::~RecordActionItem() = default;

    auto RecordActionItem::GetType() const ->ActionType {
		return d->type;
	}

	void RecordActionItem::SetType(const ActionType& type) const {
		d->type = type;
        switch (type) {
		case ActionType::Orbit: {
			d->orbitInfo = new OrbitInfo;
			break;
		}
		case ActionType::Slice: {
			d->sliceInfo = new SliceInfo;
			break;
		}
		case ActionType::Timelapse: {
			d->timeInfo = new TimeInfo;
			break;
		}
        }
	}

	void RecordActionItem::SetPosition(const int& start, const int& end) const {
		d->beginMarker->move(start, 0);
		d->endMarker->move(end - d->markerWidth, 0);

		this->UpdateBox();
	}

	void RecordActionItem::UpdateBox() const {
		d->box->setGeometry(d->beginMarker->x() + d->markerWidth, 0,
			                d->endMarker->x() - (d->beginMarker->x() + d->markerWidth), d->bodyHeight);
	}

	auto RecordActionItem::GetOrbitInfo() const ->OrbitInfo* {
		return d->orbitInfo;
	}

	auto RecordActionItem::GetSliceInfo() const ->SliceInfo* {
		return d->sliceInfo;
	}

	auto RecordActionItem::GetTimeInfo() const ->TimeInfo* {
		return d->timeInfo;
	}

	void RecordActionItem::SetOrbitInfo(OrbitInfo* info) const {
		d->orbitInfo = info;
	}

	void RecordActionItem::SetSliceInfo(SliceInfo* info) const {
		d->sliceInfo = info;

	}
	void RecordActionItem::SetTimeInfo(TimeInfo* info) const {
		d->timeInfo = info;
	}

	auto RecordActionItem::GetStartTime() const ->float {
		return d->beginMarker->x() / d->interval / 2.0f;
	}

	auto RecordActionItem::GetEndTime() const ->float {
		return (d->endMarker->x() + d->markerWidth) / d->interval / 2.0f;
	}

	auto RecordActionItem::SetCheckOverlap(const ActionType& type) const ->void {
		d->checkOverlapType = type;
    }

	auto RecordActionItem::IsCheckOverlap() const ->bool {
        if(d->checkOverlapType == +ActionType::None) {
			return false;
        }

		return true;
    }

	auto RecordActionItem::GetCheckOverlapType(void) const ->ActionType {
		return d->checkOverlapType;
    }

	void RecordActionItem::UpdateGeometry(const int& begin, const int& end) const {
		d->beginMarker->move(begin, 0);
		d->endMarker->move(end, 0);
		UpdateBox();
    }

	auto RecordActionItem::IsContains(const int& begin, const int& end) const ->bool {
		auto contain = false;
		if ((d->beginMarker->x() <= begin && begin < d->endMarker->x() ||
			d->beginMarker->x() <= end && end < d->endMarker->x())|| 
			(begin <= d->beginMarker->x() && d->beginMarker->x() < end || 
			begin <= d->endMarker->x() && d->endMarker->x() < end)) {
			contain = true;
		}

		return contain;
    }

	bool RecordActionItem::eventFilter(QObject* watched, QEvent* event) {
		if (watched == d->box || watched == d->beginMarker || watched == d->endMarker) {
			static QPoint lastPnt;
			static bool isClicked = false;
			if (event->type() == QEvent::MouseButtonPress) {
                const auto control = dynamic_cast<QLabel*>(watched);
                auto* e = dynamic_cast<QMouseEvent*>(event);

			    if (control->rect().contains(e->pos()) &&
					(e->button() == Qt::LeftButton)) {
					lastPnt = e->pos();
					isClicked = true;
				}
			}
			else if (event->type() == QEvent::MouseMove && isClicked) {
                auto e = dynamic_cast<QMouseEvent*>(event);
				const int dx = e->pos().x() - lastPnt.x();

				// calculate begin, end point
				int begin = d->beginMarker->x();
				int end = d->endMarker->x();

				if (watched == d->box) {
					const int length = d->endMarker->x() + d->markerWidth - d->beginMarker->x();
				    begin = Magnetic(d->beginMarker->x() + dx);
					end = Magnetic(begin + length) - d->markerWidth;				
				}
				else if (watched == d->beginMarker) {
					begin = Magnetic(d->beginMarker->x() + dx);
				}
				else if (watched == d->endMarker) {
					end = Magnetic(d->endMarker->x() + dx + d->markerWidth) - d->markerWidth;
				}

				// apply points
				if (d->checkOverlapType == +ActionType::None) {
					this->UpdateGeometry(begin, end);
				}
				else {
					emit changedGeometry(d->checkOverlapType, begin, end);
				}
			}
			else if (event->type() == QEvent::MouseButtonRelease && isClicked) {
				isClicked = false;
			}
		}

		return false;
	}

	auto RecordActionItem::Magnetic(const int& x) const -> int {
		const int half = d->interval / 2;
		return int((x + half) / d->interval) * d->interval;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////

    struct VideoEditWidget::Impl {
		TickMarkWidget* tickMarkWidget = nullptr;
		QTableWidget* timestampWidget = nullptr;

		QList<RecordActionItem*> actionList;

		int interval = 40;
    };

    VideoEditWidget::VideoEditWidget(QWidget* parent)
    : QWidget(parent), d{ new Impl } {
		
		const int titleWidth = 80;

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(14, 14, 14, 14);
		layout->setSpacing(0);
		this->setLayout(layout);

		// tick mark
		auto tickmarkWidget = new QWidget();

        d->tickMarkWidget = new TickMarkWidget(tickmarkWidget);
		d->tickMarkWidget->SetInterval(d->interval);

        auto tickmarkLayout = new QVBoxLayout;
		tickmarkLayout->setContentsMargins(titleWidth, 0, 0, 0);
		tickmarkLayout->addWidget(d->tickMarkWidget);

		tickmarkWidget->setLayout(tickmarkLayout);
		layout->addWidget(tickmarkWidget);

		// timeline
		d->timestampWidget = new QTableWidget(this);
		d->timestampWidget->setColumnCount(1);
		d->timestampWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
		d->timestampWidget->setSelectionMode(QAbstractItemView::SingleSelection);

		d->timestampWidget->horizontalHeader()->setStretchLastSection(true);
		d->timestampWidget->horizontalHeader()->hide();

		d->timestampWidget->verticalHeader()->setFixedWidth(titleWidth - 3);
		d->timestampWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
		d->timestampWidget->verticalHeader()->setDefaultSectionSize(30);
		d->timestampWidget->verticalHeader()->setDragEnabled(true);
		d->timestampWidget->verticalHeader()->setDragDropMode(QAbstractItemView::DragDrop);
		d->timestampWidget->verticalHeader()->setSectionsMovable(true);

		layout->addWidget(d->timestampWidget, 1);

		connect(d->timestampWidget, SIGNAL(itemSelectionChanged()), this, SLOT(on_timestampWidget_itemSelectionChanged()));
		connect(d->timestampWidget->verticalHeader(), SIGNAL(sectionMoved(int, int, int)), this, SLOT(on_timestampWidget_dragAndDrop(int, int, int)));

		this->installEventFilter(this);

		// set object names
		d->timestampWidget->setObjectName("table-tracks");
    }

    VideoEditWidget::~VideoEditWidget() = default;

    void VideoEditWidget::AddAction(RecordActionItem* action) const {
		d->actionList.push_back(action);

		int index = d->actionList.size() - 1;
		d->timestampWidget->insertRow(index);

		d->timestampWidget->setVerticalHeaderItem(index, new QTableWidgetItem(d->actionList.at(index)->GetType()._to_string()));
		d->timestampWidget->setCellWidget(index, 0, d->actionList.at(index));

		d->timestampWidget->selectRow(index);

		connect(action, SIGNAL(changedGeometry(ActionType, int, int)), this, SLOT(on_action_geometryChanged(ActionType, int, int)));
    }

	void VideoEditWidget::DeleteSelectedAction() const {
		QItemSelectionModel* select = d->timestampWidget->selectionModel();
		if(false == select->hasSelection()) {
			return;
		}

        const auto rows = select->selectedRows();
        const auto row = rows.at(0).row();
		
		d->timestampWidget->removeRow(row);
		d->actionList.removeAt(row);
    }

	void VideoEditWidget::SetInterval(const float& interval) const {
		d->interval = interval;
		d->tickMarkWidget->SetInterval(d->interval);
    }

	auto VideoEditWidget::GetProcess() const ->QList<RecordActionItem*> {
		return d->actionList;
    }

	void VideoEditWidget::ClearSelection(void) {
		d->timestampWidget->clearSelection();
    }

	void VideoEditWidget::on_timestampWidget_itemSelectionChanged() {
		auto SetTrackItemSelection = [=](bool selected, int row = -1) {
			for (int i = 0; i < d->timestampWidget->rowCount(); i++) {
				auto trackWidget = d->timestampWidget->cellWidget(i, 0);
				auto trackControllerList = trackWidget->findChildren<QLabel*>();
				for (auto controller : trackControllerList) {
					if (row == i) {
						controller->setProperty("selected", selected);
					} else {
						controller->setProperty("selected", !selected);
					}

					controller->style()->unpolish(controller);
					controller->style()->polish(controller);
					controller->update();
				}
			}
		};

        QItemSelectionModel* select = d->timestampWidget->selectionModel();
		if (nullptr == select) {
			SetTrackItemSelection(false);
			emit selectItem(nullptr);
			return;
		}

		if (false == select->hasSelection()) {
			SetTrackItemSelection(false);
			emit selectItem(nullptr);
			return;
		}

		auto rows = select->selectedRows();
		auto index = rows.at(0).row();

		SetTrackItemSelection(true, index);

		emit selectItem(d->actionList.at(index));
    }

	void VideoEditWidget::on_timestampWidget_dragAndDrop(int, int, int) const {
		RefreshList();
    }

	void VideoEditWidget::on_action_geometryChanged(ActionType type, int begin, int end) {
		auto item = static_cast<RecordActionItem*>(this->sender());

        if(true == this->IsOverlap(type, item, begin, end)) {
			return;
        }

		item->UpdateGeometry(begin, end);
    }

	void VideoEditWidget::RefreshList() const {
		d->actionList.clear();

		for(int row =0; row < d->timestampWidget->rowCount(); ++row) {
			d->actionList.push_back(dynamic_cast<RecordActionItem*>(d->timestampWidget->cellWidget(row, 0)));
		}
    }

	auto VideoEditWidget::IsOverlap(const ActionType& type, const RecordActionItem* item, const int& begin, const int& end) const ->bool {
		if (nullptr == item) {
			return false;
		}

		bool isOverlap = false;

		for (auto action : d->actionList) {
			if (item == action) {
				continue;
			}

			if (action->GetType() != +type) {
				continue;
			}

			if (true == action->IsContains(begin, end)) {
				isOverlap = true;
				break;
			}
		}

		return isOverlap;
	}
}
