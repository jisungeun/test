#include "IMaskGeneral.h"

namespace Tomocube::Rendering::Mask {    
    IMaskGeneral::IMaskGeneral() : general_d{ new Impl } {
        general_d->BuildColorTable();
    }
    IMaskGeneral::~IMaskGeneral() {
        
    }
    auto IMaskGeneral::GetRootSwitch() -> SoSwitch* {
        return general_d->rootSwitch.ptr();
    }

    auto IMaskGeneral::GetName() const -> QString {
        return general_d->name;
    }

    auto IMaskGeneral::Impl::BuildColorTable() -> void {
        color_table[0][0] = 230; color_table[0][1] = 0; color_table[0][2] = 0; //red
        color_table[1][0] = 60; color_table[1][1] = 180; color_table[1][2] = 75; //green
        color_table[2][0] = 255; color_table[2][1] = 255; color_table[2][2] = 25; //yellow
        color_table[3][0] = 0; color_table[3][1] = 130; color_table[3][2] = 200; //blue
        color_table[4][0] = 245; color_table[4][1] = 130; color_table[4][2] = 48; //Orange
        color_table[5][0] = 145; color_table[5][1] = 30; color_table[5][2] = 180; //Purple
        color_table[6][0] = 70; color_table[6][1] = 240; color_table[6][2] = 240; //Cyan
        color_table[7][0] = 240; color_table[7][1] = 50; color_table[7][2] = 230; //Magenta
        color_table[8][0] = 210; color_table[8][1] = 245; color_table[8][2] = 60; //Lime
        color_table[9][0] = 250; color_table[9][1] = 190; color_table[9][2] = 212; //Pink
        color_table[10][0] = 0; color_table[10][1] = 128; color_table[10][2] = 128; //Teal
        color_table[11][0] = 220; color_table[11][1] = 190; color_table[11][2] = 255; //Lavender
        color_table[12][0] = 170; color_table[12][1] = 110; color_table[12][2] = 40; //Brown
        color_table[13][0] = 255; color_table[13][1] = 250; color_table[13][2] = 200; //Beige
        color_table[14][0] = 128; color_table[14][1] = 0; color_table[14][2] = 0; //Maroon
        color_table[15][0] = 170; color_table[15][1] = 255; color_table[15][2] = 195; //Mint
        color_table[16][0] = 128; color_table[16][1] = 128; color_table[16][2] = 0; //Olive
        color_table[17][0] = 255; color_table[17][1] = 215; color_table[17][2] = 180; //Apricot
        color_table[18][0] = 0; color_table[18][1] = 0; color_table[18][2] = 128; //Navy
        color_table[19][0] = 128; color_table[19][1] = 128; color_table[19][2] = 128; //Navy
    }
}
