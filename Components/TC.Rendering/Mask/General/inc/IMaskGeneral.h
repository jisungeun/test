#pragma once

#include <memory>

#include <QString>

#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include "TC.Rendering.Mask.GeneralExport.h"

namespace Tomocube::Rendering::Mask {
    class TC_Rendering_Mask_General_API IMaskGeneral {
    public:
        IMaskGeneral();
        virtual ~IMaskGeneral();
                
        virtual auto SetVolume(SoVolumeData* vol)->void = 0;
        virtual auto SetDataRange(double min, double max)->void = 0;
        virtual auto SetHighlight(bool isHighlight, int index)->void = 0;        

        [[nodiscard]] auto GetRootSwitch()->SoSwitch*;
        [[nodiscard]] auto GetName()const->QString;

    protected:
        struct Impl {
            QString name;
            double min{ -1 };
            double max{ -1 };
            SoRef<SoSwitch> rootSwitch{ nullptr };
            float color_table[20][3];
            bool isHighlight{ false };
            int highlightIdx{ 1 };

            auto BuildColorTable()->void;
        };
        std::unique_ptr<Impl> general_d;
    };
}