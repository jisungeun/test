#pragma once

#include <memory>

#include <QString>

namespace Tomocube::Rendering::Mask {
    class IMaskSlice {
    public:
        IMaskSlice();
        virtual ~IMaskSlice();

        virtual auto SetSliceNumber(int sliceNumber, bool apply)->void = 0;
        virtual auto SetSliceTransparency(float transp)->void = 0;
        virtual auto ChangeName(const QString& name)->void = 0;
        [[nodiscard]] auto GetSliceNumber()const->int;
        [[nodiscard]] auto GetSliceTransparency()const->float;

    protected:
        struct Impl {
            float transp{ 0.0 };
            int sliceNumber{ 0 };
        };
        std::unique_ptr<Impl> slice_d;
    };
}