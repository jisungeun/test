#pragma once

#include <memory>

#include "IMaskGeneral.h"
#include "IMaskSlice.h"

#include "TC.Rendering.Mask.SliceExport.h"

namespace Tomocube::Rendering::Mask {
    class TC_Rendering_Mask_Slice_API MaskSlice : public IMaskSlice, public IMaskGeneral {
    public:
        MaskSlice(const QString& name);
		~MaskSlice() override;

        auto SetVolume(SoVolumeData* vol) -> void override;
        auto SetDataRange(double min, double max) -> void override;
        auto SetSliceNumber(int sliceNumber, bool apply) -> void override;
        auto SetHighlight(bool isHighlight, int index) -> void override;
        auto SetSliceTransparency(float transp) -> void override;
        auto ChangeName(const QString& name) -> void override;

        auto Clear()->void;

    private:
        auto BuildSceneGraph()->void;
        auto BuildMaskTF()->void;
        auto ApplyHighlgiht(int idx)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}