#include "MaskSlice.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>

#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/helpers/Medicalhelper.h>
#pragma warning(pop)

namespace Tomocube::Rendering::Mask {
    struct MaskSlice::Impl {
        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoVolumeData> maskVolume{ nullptr };
        SoRef<SoSwitch> maskSocket{ nullptr };
        SoRef<SoMaterial> maskMatl{ nullptr };
        SoRef<SoDataRange> maskRange{ nullptr };
        SoRef<SoTransferFunction> maskTF{ nullptr };
        SoRef<SoVolumeShader> maskShader{ nullptr };
        SoRef<SoOrthoSlice> maskSlice{ nullptr };
        SoRef<SoTranslation> sliceTrans{ nullptr };
     
        auto ClearMaskTF()->void;        
    };
    MaskSlice::MaskSlice(const QString& name) : IMaskGeneral(),IMaskSlice(),d{ new Impl } {
        general_d->name = name;
        BuildSceneGraph();
    }
    MaskSlice::~MaskSlice() {
        
    }
    auto MaskSlice::ChangeName(const QString& name) -> void {
        general_d->name = name;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        d->maskMatl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->maskRange->setName((general_d->name + "_DataRange").toStdString().c_str());
        d->maskSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->maskTF->setName((general_d->name + "_TF").toStdString().c_str());
        d->maskShader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->maskSlice->setName((general_d->name + "_Slice").toStdString().c_str());
	}
    auto MaskSlice::BuildSceneGraph() -> void {
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->sliceTrans = new SoTranslation;
        d->root->addChild(d->sliceTrans.ptr());

        d->maskMatl = new SoMaterial;
        d->maskMatl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->maskMatl->ambientColor.setValue(1, 1, 1);
        d->maskMatl->diffuseColor.setValue(1, 1, 1);

        d->maskRange = new SoDataRange;
        d->maskRange->setName((general_d->name + "_DataRange").toStdString().c_str());

        d->maskSocket = new SoSwitch;
        d->maskSocket->whichChild = 0;
        d->maskSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->maskSocket->addChild(new SoSeparator);

        d->maskTF = new SoTransferFunction;
        d->maskTF->setName((general_d->name + "_TF").toStdString().c_str());

        d->maskShader = new SoVolumeShader;
        d->maskShader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->maskShader->forVolumeOnly = FALSE;

        d->maskSlice = new SoOrthoSlice;
        d->maskSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
        d->maskSlice->interpolation = SoOrthoSlice::NEAREST;
        d->maskSlice->setName((general_d->name + "_Slice").toStdString().c_str());
        d->maskSlice->axis = MedicalHelper::AXIAL;

        d->root->addChild(d->maskMatl.ptr());
        d->root->addChild(d->maskRange.ptr());
        d->root->addChild(d->maskSocket.ptr());
        d->root->addChild(d->maskTF.ptr());
        d->root->addChild(d->maskShader.ptr());
        d->root->addChild(d->maskSlice.ptr());
    }
    auto MaskSlice::ApplyHighlgiht(int idx) -> void {
        d->maskTF->colorMap.setNum((static_cast<int>(d->maskRange->max.getValue()) + 1) * 4);
        auto p = d->maskTF->colorMap.startEditing();
        *p++ = 0.0; *p++ = 0.0; *p++ = 0.0; *p++ = 0.0;
        for (auto i = 1; i < d->maskRange->max.getValue() + 1; ++i) {
            auto r = general_d->color_table[(i - 1) % 20][0] / 255.0f;
            auto g = general_d->color_table[(i - 1) % 20][1] / 255.0f;
            auto b = general_d->color_table[(i - 1) % 20][2] / 255.0f;
            *p++ = r;
            *p++ = g;
            *p++ = b;
            if (i == idx) {
                *p++ = 1.0f;
            }else {
                *p++ = 0.4f;
            }
        }
        d->maskTF->colorMap.finishEditing();
    }    
    auto MaskSlice::BuildMaskTF() -> void {
        d->maskTF->colorMap.setNum((static_cast<int>(d->maskRange->max.getValue()) + 1) * 4);
        auto p = d->maskTF->colorMap.startEditing();
        *p++ = 0.0; *p++ = 0.0; *p++ = 0.0; *p++ = 0.0;
        for (auto i = 1; i < d->maskRange->max.getValue() + 1; ++i) {
            auto r = general_d->color_table[(i - 1) % 20][0] / 255.0f;
            auto g = general_d->color_table[(i - 1) % 20][1] / 255.0f;
            auto b = general_d->color_table[(i - 1) % 20][2] / 255.0f;
            *p++ = r;
            *p++ = g;
            *p++ = b;
            *p++ = 0.6f;
        }
        d->maskTF->colorMap.finishEditing();
    }
    auto MaskSlice::Impl::ClearMaskTF() -> void {
        maskTF->colorMap.setNum(4);
        auto p = maskTF->colorMap.startEditing();
        for (auto i = 0; i < 4; i++) {
            *p++ = 0.0;
        }
        maskTF->colorMap.finishEditing();
    }
    auto MaskSlice::SetVolume(SoVolumeData* vol) -> void {
        if (general_d->min < 0 && general_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            general_d->min = data_min;
            general_d->max = data_max;
            d->maskRange->min = data_min;
            d->maskRange->max = data_max;
        }
        d->maskSocket->replaceChild(0, vol);
        BuildMaskTF();
    }
    auto MaskSlice::SetDataRange(double min, double max) -> void {
        d->maskRange->min = min;
        d->maskRange->max = max;
        general_d->min = min;
        general_d->max = max;
        BuildMaskTF();
    }
    auto MaskSlice::SetSliceTransparency(float transp) -> void {
        slice_d->transp = transp;
        d->maskMatl->transparency = transp;
	}
    auto MaskSlice::SetHighlight(bool isHighlight, int index) -> void {
        general_d->isHighlight = isHighlight;
        general_d->highlightIdx = index;
        if(isHighlight) {
            ApplyHighlgiht(index);
        }else {
            BuildMaskTF();
        }
	}
    auto MaskSlice::SetSliceNumber(int sliceNumber, bool apply) -> void {
        slice_d->sliceNumber = sliceNumber;
        if(apply) {
            d->maskSlice->sliceNumber = sliceNumber;
        }
    }
    auto MaskSlice::Clear() -> void {
        slice_d.reset();
        general_d->min = -1;
        general_d->max = -1;
        d->maskRange->min = -1;
        d->maskRange->max = -1;
        d->maskSocket->replaceChild(0, new SoSeparator);
    }
}