#include "IMaskSlice.h"

namespace Tomocube::Rendering::Mask {
    IMaskSlice::IMaskSlice() : slice_d{ new Impl } {
        
    }
    IMaskSlice::~IMaskSlice() {
        
    }
    auto IMaskSlice::GetSliceNumber() const -> int {
        return slice_d->sliceNumber;
    }
    auto IMaskSlice::GetSliceTransparency() const -> float {
        return slice_d->transp;
	}
}