#include <QApplication>
#include <QHBoxLayout>

#include <QFileDialog>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <TCFMetaReader.h>
#include <RenderWindowSlice.h>

#include <MaskSlice.h>

using namespace Tomocube::Rendering::Mask;

int main(int argc,char** argv) {
    QApplication app(argc, argv);
    auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", "F:/CellLibrary/20210614_ TCF 1.4.0/20210607.170006.233.HTFL 3D single-001/20210607.170006.233.HTFL 3D single-001.TCF", "TCF (*.tcf)");
    if (path.isEmpty()) {
        return EXIT_FAILURE;
    }
    QWidget* socket = new QWidget;
    socket->setMinimumSize(500, 500);
    auto layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    socket->setLayout(layout);
    SoQt::init(socket);
    SoVolumeRendering::init();

    auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
    auto metaInfo = metaReader->Read(path);

    auto sizeX = metaInfo->data.data3D.sizeX;
    auto sizeY = metaInfo->data.data3D.sizeY;
    auto resX = metaInfo->data.data3D.resolutionX;
    auto resY = metaInfo->data.data3D.resolutionY;


    auto arr = new uint16_t[sizeX * sizeY];
    for (auto i = 0; i < sizeX; i++) {
        for (auto j = 0; j < sizeY; j++) {
            auto idx = i * sizeY + j;
            *(arr + idx) = 0;
        }
    }
    //create synthetic 2D mask
    for (auto i = sizeX / 6; i < sizeX / 3; i++) {
        for (auto j = sizeY / 6; j < sizeY / 3; j++) {
            auto idx = i * sizeY + j;
            *(arr + idx) = 1;
        }
    }

    for (auto i = sizeX / 6 * 4; i < sizeX / 6 * 5; i++) {
        for (auto j = sizeY / 6; j < sizeY / 3; j++) {
            auto idx = i * sizeY + j;
            *(arr + idx) = 2;
        }
    }

    for (auto i = sizeX / 6; i < sizeX / 3; i++) {
        for (auto j = sizeY / 6 * 4; j < sizeY / 6 * 5; j++) {
            auto idx = i * sizeY + j;
            *(arr + idx) = 3;
        }
    }

    for (auto i = sizeX / 6 * 4; i < sizeX / 6 * 5; i++) {
        for (auto j = sizeY / 6 * 4; j < sizeY / 6 * 5; j++) {
            auto idx = i * sizeY + j;
            *(arr + idx) = 4;
        }
    }

    SoRef<SoVolumeData> volData = new SoVolumeData;
    volData->data.setValue(SbVec3i32(sizeX, sizeY, 1), SbDataType::UNSIGNED_SHORT, 16, arr, SoSFArray::NO_COPY_AND_DELETE);
    volData->extent.setValue(-resX * sizeX / 2.0, -resY * sizeY / 2.0, -0.5, resX * sizeX / 2.0, resY * sizeY / 2.0, 0.5);

    auto maskSlice = new MaskSlice("Test");
    maskSlice->SetVolume(volData.ptr());
        
    auto renderWindow = new RenderWindow2D(nullptr);
    layout->addWidget(renderWindow);

    renderWindow->setSceneGraph(maskSlice->GetRootSwitch());
    renderWindow->ResetView2D(Direction2D::XY);

    SoQt::show(socket);
    SoQt::mainLoop();

    delete socket;

    SoVolumeRendering::finish();
    SoQt::finish();

    return EXIT_SUCCESS;
}