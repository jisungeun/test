#pragma once

#include <memory>

namespace Tomocube::Rendering::Mask {
    class IMaskVolume {
    public:
        IMaskVolume();
        virtual ~IMaskVolume();

    protected:
        struct Impl {
            
        };
        std::unique_ptr<Impl> volume_d;
    };
}