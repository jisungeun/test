#pragma once

#include <memory>

#include "IMaskGeneral.h"
#include "IMaskVolume.h"

#include "TC.Rendering.Mask.VolumeExport.h"

namespace Tomocube::Rendering::Mask {
    class TC_Rendering_Mask_Volume_API MaskVolume : public IMaskGeneral,public IMaskVolume {
    public:
        MaskVolume(const QString& name);
        virtual ~MaskVolume();

        auto SetVolume(SoVolumeData* vol) -> void override;
        auto SetDataRange(double min, double max) -> void override;
        auto SetHighlight(bool isHighlight,int index) -> void override;

    	auto SetHighlightFactor(double factor) -> void;
        auto SetMaskColor(int labelIdx, QColor col) -> void;

        auto SetZRange(double min, double max)->void;
        auto SetXRange(double min, double max)->void;
        auto SetYRange(double min, double max)->void;

        auto ToggleJittering(bool use)->void;
        auto ToggleDeferredLighing(bool use)->void;

        auto Clear()->void;

    private:
        auto BuildSceneGraph()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}