#include <qcoreapplication.h>
#include <QColor>

#include "MaskVolume.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>

#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/helpers/Medicalhelper.h>
#pragma warning(pop)

namespace Tomocube::Rendering::Mask {
    struct MaskVolume::Impl {
        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoVolumeData> maskVolume{ nullptr };
        SoRef<SoSwitch> maskSocket{ nullptr };
        SoRef<SoMaterial> maskMatl{ nullptr };
        SoRef<SoDataRange> maskRange{ nullptr };
        SoRef<SoTransferFunction> maskTF{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoVolumeRenderingQuality> maskShader{ nullptr };
        SoRef<SoVolumeRender> maskVolRender{ nullptr };

        QVector<QColor> maskColormap;

        QString shaderPath;

        int maxLabel{ 0 };
        double highlightFactor{ 0.6 };

        auto BuildMaskColorMap()->void;
        auto BuildHighlightMaskColorMap(int idx)->void;
    };
    MaskVolume::MaskVolume(const QString& name) : d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/VolumeMask.glsl").arg(qApp->applicationDirPath());
        BuildSceneGraph();        
    }
    MaskVolume::~MaskVolume() {
        
    }
    auto MaskVolume::BuildSceneGraph() -> void {
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->maskMatl = new SoMaterial;
        d->maskMatl->setName((general_d->name + "_Matl").toStdString().c_str());

        d->maskRange = new SoDataRange;
        d->maskRange->setName((general_d->name + "_DataRange").toStdString().c_str());

        d->maskSocket = new SoSwitch;
        d->maskSocket->whichChild = 0;
        d->maskSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->maskSocket->addChild(new SoSeparator);

        d->maskTF = new SoTransferFunction;
        d->maskTF->setName((general_d->name + "_TF").toStdString().c_str());

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1f("lowerBound", 0);
        d->fragmentShader->addShaderParameter1f("upperBound", 1);
        d->fragmentShader->addShaderParameter1f("xMinBound", 0);
        d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
        d->fragmentShader->addShaderParameter1f("yMinBound", 0);
        d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

        d->maskShader = new SoVolumeRenderingQuality;
        d->maskShader->lighting = FALSE;
        d->maskShader->preIntegrated = FALSE;
        d->maskShader->deferredLighting = TRUE;
        d->maskShader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->maskShader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->maskVolRender = new SoVolumeRender;
        d->maskVolRender->interpolation = SoVolumeRender::Interpolation::NEAREST;
        d->maskVolRender->setName((general_d->name + "_VolRender").toStdString().c_str());        

        d->root->addChild(d->maskMatl.ptr());
        d->root->addChild(d->maskRange.ptr());
        d->root->addChild(d->maskSocket.ptr());
        d->root->addChild(d->maskTF.ptr());
        d->root->addChild(d->maskShader.ptr());
        d->root->addChild(d->maskVolRender.ptr());
    }
    auto MaskVolume::SetXRange(double min, double max) -> void {
        d->fragmentShader->setShaderParameter1f("xMinBound", min);
        d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}
    auto MaskVolume::SetYRange(double min, double max) -> void {
        d->fragmentShader->setShaderParameter1f("yMinBound", min);
        d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}
    auto MaskVolume::SetZRange(double min, double max) -> void {
        d->fragmentShader->setShaderParameter1f("lowerBound", min);
        d->fragmentShader->setShaderParameter1f("upperBound", max);
	}

    auto MaskVolume::SetHighlight(bool isHighlight, int index) -> void {
        general_d->isHighlight = isHighlight;
        general_d->highlightIdx = index;        
        if(index > 0 && isHighlight) {
            d->BuildHighlightMaskColorMap(index);
        }else {
            d->BuildMaskColorMap();
        }        
	}    
    auto MaskVolume::Impl::BuildMaskColorMap() -> void {
        const auto count = maskColormap.count() + 1;
        maskTF->colorMap.setNum(count * 4);
        auto p = maskTF->colorMap.startEditing();
        *p++ = 0;
        *p++ = 0;
        *p++ = 0;
        *p++ = 0;
        for (auto i = 0; i < count - 1; i++) {
            *p++ = static_cast<float>(maskColormap[i].redF());
            *p++ = static_cast<float>(maskColormap[i].greenF());
            *p++ = static_cast<float>(maskColormap[i].blueF());            
            *p++ = static_cast<float>(maskColormap[i].alphaF());
        }
        maskTF->colorMap.finishEditing();
	}
    auto MaskVolume::Impl::BuildHighlightMaskColorMap(int idx) -> void {
        const auto count = maskColormap.count() + 1;
        maskTF->colorMap.setNum(count * 4);
        auto p = maskTF->colorMap.startEditing();
        *p++ = 0;
        *p++ = 0;
        *p++ = 0;
        *p++ = 0;
        for (auto i = 0; i < count - 1; i++) {
            *p++ = static_cast<float>(maskColormap[i].redF());
            *p++ = static_cast<float>(maskColormap[i].greenF());
            *p++ = static_cast<float>(maskColormap[i].blueF());
            if (i + 1 == idx) {
                *p++ = static_cast<float>(maskColormap[i].alphaF());
            }
            else {
                *p++ = static_cast<float>(maskColormap[i].alphaF()) * static_cast<float>(highlightFactor);
            }
        }
        maskTF->colorMap.finishEditing();
	}

    auto MaskVolume::ToggleDeferredLighing(bool use) -> void {
        d->maskShader->deferredLighting = use;
	}

    auto MaskVolume::ToggleJittering(bool use) -> void {
        d->maskShader->jittering = use;		
	}

    auto MaskVolume::SetHighlightFactor(double factor) -> void {
        d->highlightFactor = factor;
        if (general_d->isHighlight && general_d->highlightIdx > 0) {
            d->BuildHighlightMaskColorMap(general_d->highlightIdx);
        }
        else {
            d->BuildMaskColorMap();
        }
	}
    auto MaskVolume::SetMaskColor(int labelIdx, QColor col) -> void {
		if(d->maskColormap.count() > labelIdx - 1) {
            d->maskColormap[labelIdx - 1] = col;
		}
        if (general_d->isHighlight && general_d->highlightIdx > 0) {
            d->BuildHighlightMaskColorMap(general_d->highlightIdx);
        }
        else {
            d->BuildMaskColorMap();
        }
	}
    auto MaskVolume::SetVolume(SoVolumeData* vol) -> void {
        if (general_d->min < 0 && general_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            general_d->min = data_min;
            general_d->max = data_max;
            d->maskRange->min = data_min;
            d->maskRange->max = data_max;            
        }
        d->maxLabel = static_cast<int>(general_d->max);
        d->maskColormap.clear();
        for(auto i=0;i<d->maxLabel;i++) {
            d->maskColormap.push_back(QColor(255, 255, 255));
        }
        d->maskSocket->replaceChild(0, vol);        
    }
    auto MaskVolume::SetDataRange(double min, double max) -> void {
        d->maskRange->min = min;
        d->maskRange->max = max;
        general_d->min = min;
        general_d->max = max;        
    }
    auto MaskVolume::Clear() -> void {
        volume_d.reset();
        general_d->min = -1;
        general_d->max = -1;
        d->maskRange->min = -1;
        d->maskRange->max = -1;
        d->maskSocket->replaceChild(0, new SoSeparator);
    }
}