//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

////////////////////////////////////////////////////////////////////////
// Simple multi-channel fragment shader for volume rendering
//
// Notes
// 1) This version is for volume rendering.
//    (Must make a separate version for slice/skin rendering.)
// 2) This version is for 3 channels of data.
//    (Must make a separate version for 2 channels.)
// 3) This shader assumes that 3D textures are used.
// 4) It seems reasonable to sum the color (RGB) components.  But we
//    don't necessarily want the volume to be more opaque where
//    values exist in multiple channels, so we'll use the highest
//    opacity value in any of the channels.  Does that make sense?

uniform VVizDataSetId data1;

uniform float upperBound;
uniform float lowerBound;
uniform float xMinBound;
uniform float xMaxBound;
uniform float yMinBound;
uniform float yMaxBound;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;
	vec4 clrMask = vec4(0.0, 0.0, 0.0, 0.0);
	
	vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);		
	VVIZ_DATATYPE index1 = VVizGetData(data1, dataCoord1);			
	clrMask = VVizTransferFunction(index1, 1);				
	clrMask = mix(clrMask, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));

	float x = dataCoord1[0];
	float y = dataCoord1[1];
	float depth = dataCoord1[2];				
	
	if (depth < lowerBound || depth > upperBound) {
		clrMask = vec4(0);
		return clrMask;
	}

	if (x < xMinBound || x > xMaxBound) {
		clrMask = vec4(0);
		return clrMask;
	}

	if (y < yMinBound || y > yMaxBound) {
		clrMask = vec4(0);
		return clrMask;
	}

	return clrMask;	
}