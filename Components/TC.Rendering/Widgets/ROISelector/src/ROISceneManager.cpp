#include <VolumeViz/nodes/SoVolumeData.h>

#include <OivLdmReaderXY.h>
#include <OivXYReader.h>

//For HT 2D
#include <OivLdmReader.h>
//For FL 2D
#include <OivHdf5Reader.h>
#include <OivLdmReaderFL.h>

#include <Slice3ChannelTF1D.h>
#include <SliceTF1D.h>
#include "ROISceneManager.h"


using namespace Tomocube::Rendering::Image;
namespace TC::Rendering::Widgets::ROISelector {	
	struct ROISceneManager::Impl {
		std::shared_ptr<Slice3ChannelTF1D> scene{ nullptr };

		QString tcfPath;
		IO::TCFMetaReader::Meta::Pointer meta{ nullptr };
		bool isNaive{ false };
		bool ht8Bit{ false };
		bool htExist{ false };
		bool flExist[3]{ false, };
		bool flInRange[3]{ false, };
		bool is2D{ false };
		bool showHT{ false };
		bool showFL[3]{ false,false,false };
		//VolumeData
		SoRef<SoVolumeData> ht{ nullptr };
		SoRef<SoVolumeData> fl[3]{ nullptr, };

		SoRef<OivLdmReaderXY> htLdmReader{ nullptr };
		SoRef<OivXYReader> htReader{ nullptr };
		SoRef<OivLdmReaderXY> flLdmReader[3]{ nullptr, };
		SoRef<OivXYReader> flReader[3]{ nullptr, };

		double curRIMin, curRIMax;
		
		auto Init()->void;
		auto calcFLIndexFromHT(int idx)->int;
	};
	auto ROISceneManager::Impl::Init() -> void {
		scene = std::make_shared<Slice3ChannelTF1D>("ROI");
		ht = new SoVolumeData;
		ht->ldmResourceParameters.getValue()->minTilesToLoad = 128;
		htReader = new OivXYReader;
		htReader->setDataGroupPath("/Data/3D");
		htLdmReader = new OivLdmReaderXY;
		htLdmReader->setDataGroupPath("/Data/3D");
		for(auto i=0;i<3;i++) {
			fl[i] = new SoVolumeData;
			fl[i]->ldmResourceParameters.getValue()->minTilesToLoad = 128;
			flReader[i] = new OivXYReader(true);
			flReader[i]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(i).toStdString());
			flLdmReader[i] = new OivLdmReaderXY(true);
			flLdmReader[i]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(i).toStdString());
		}
	}
	auto ROISceneManager::Impl::calcFLIndexFromHT(int idx) -> int {
		const auto flRes = meta->data.data3DFL.resolutionZ;
		const auto offsetZ = meta->data.data3DFL.offsetZ - (meta->data.data3DFL.sizeZ * meta->data.data3DFL.resolutionZ / 2);

		auto phyZ = meta->data.data3D.resolutionZ * idx - offsetZ;
		auto idxFLz = static_cast<int>(phyZ / flRes);
		
		return idxFLz;
	}
	ROISceneManager::ROISceneManager() : d{ new Impl } {
		d->Init();
	}
	ROISceneManager::~ROISceneManager() {
		
	}
	auto ROISceneManager::ToggleHT(bool show) -> void {
		d->showHT = show;
		d->scene->ToggleViz(show);
	}
	auto ROISceneManager::ToggleFL(int ch, bool show) -> void {
		d->showFL[ch] = show;
		d->scene->ToggleViz(ch, show && d->flInRange[ch]);
	}
	auto ROISceneManager::Clear() -> void {		
		d->tcfPath = QString();
		d->meta = nullptr;
		d->scene->Clear();
		d->htExist = false;
		d->ht8Bit = false;
		d->showHT = false;
		for(auto i=0;i<3;i++) {
			d->flExist[i] = false;
			d->showFL[i] = false;
			d->flInRange[i] = false;
		}
	}
	auto ROISceneManager::ClearScene() -> void {
		d->scene->Clear();
		d->htExist = false;
		d->ht8Bit = false;
		d->showHT = false;
		for (auto i = 0; i < 3; i++) {
			d->flExist[i] = false;
			d->showFL[i] = false;
			d->flExist[i] = false;
		}
	}
	auto ROISceneManager::SetHTRange(double min, double max) -> void {
		if(d->ht8Bit) {
			const auto realMin = (min - d->curRIMin) * 1000.0;
			const auto realMax = (max - d->curRIMin) * 1000.0;
			d->scene->SetDataRange(realMin, realMax); 
		}else {
			d->scene->SetDataRange(min * 10000.0, max * 10000.0);
		}
	}
	auto ROISceneManager::SetFLRange(int ch, int min, int max) -> void {
		d->scene->SetDataRange(ch, static_cast<double>(min), static_cast<double>(max));
	}
	auto ROISceneManager::ReadHT2D(int timestep) -> void {
		if (d->tcfPath.isEmpty()) {
			return;
		}
		if (nullptr == d->meta) {
			return;
		}
		d->is2D = true;
		QString tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));
		const auto is8Bit = d->meta->data.data2DMIP.scalarType[timestep] == 1;
		d->ht8Bit = is8Bit;
		if(d->isNaive) {
			SoRef<OivHdf5Reader> reader = new OivHdf5Reader(false,is8Bit);
			reader->setDataGroupPath("/Data/2DMIP");
			reader->setTileName(tileName.toStdString());
			reader->setFilename(d->tcfPath.toStdString());
			d->ht->setReader(*reader);
		}else {
			SoRef<OivLdmReader> reader = new OivLdmReader(true,is8Bit);
			reader->setTileDimension(d->meta->data.data2DMIP.tileSizeX, d->meta->data.data2DMIP.tileSizeY, 1);
			reader->setDataGroupPath("/Data/2DMIP");
			reader->setTileName(tileName.toStdString());
			reader->setFilename(d->tcfPath.toStdString());
			d->ht->setReader(*reader);
		}
		const auto min = d->meta->data.data2DMIP.riMinList[timestep];
		const auto max = d->meta->data.data2DMIP.riMaxList[timestep];
		d->curRIMin = min;
		d->curRIMax = max;
		if (is8Bit) {
			const auto range = (max - min) * 1000;
			d->scene->SetDataMinMax(0, range);
		}
		else {
			d->scene->SetDataMinMax(min * 10000.0, max * 10000.0);
		}
		d->htExist = true;
		d->scene->SetVolume(d->ht.ptr());
	}
	auto ROISceneManager::ReadHT3D(int timestep) -> void {
		if (d->tcfPath.isEmpty()) {
			return;
		}
		if (nullptr == d->meta) {
			return;
		}
		d->is2D = false;
		QString tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));
		const auto is8Bit = d->meta->data.data3D.scalarType[timestep] == 1;
		d->ht8Bit = is8Bit;
		if (d->isNaive) {
			d->htReader->set8Bit(is8Bit);
			d->htReader->setTileName(tileName.toStdString());
			d->htReader->setFilename(d->tcfPath.toStdString());
			d->htReader->setIndex(d->meta->data.data3D.sizeZ / 2);
			d->ht->setReader(*d->htReader);
			d->htReader->touch();
		}
		else {
			d->htLdmReader->set8Bit(is8Bit);
			d->htLdmReader->setTileName(tileName.toStdString());
			d->htLdmReader->setTileDimension(d->meta->data.data3D.tileSizeX, d->meta->data.data3D.tileSizeY, d->meta->data.data3D.tileSizeZ);
			d->htLdmReader->setFilename(d->tcfPath.toStdString());
			d->htLdmReader->setIndex(d->meta->data.data3D.sizeZ / 2);
			d->ht->setReader(*d->htLdmReader);
			d->htLdmReader->touch();
		}

		const auto min = d->meta->data.data3D.riMinList[timestep];
		const auto max = d->meta->data.data3D.riMaxList[timestep];
		d->curRIMin = min;
		d->curRIMax = max;
		if(is8Bit) {
			const auto range = (max - min) * 1000;
			d->scene->SetDataMinMax(0, range);
		}else {
			d->scene->SetDataMinMax(min* 10000.0, max * 10000.0);
		}
		d->htExist = true;
		d->scene->SetVolume(d->ht.ptr());
		//d->scene->SetSliceNumber(d->meta->data.data3D.sizeZ / 2, d->isNaive);
	}		
	auto ROISceneManager::ReadFL2D(int ch, int timestep) -> void {
		if (d->tcfPath.isEmpty()) {
			return;
		}
		if (nullptr == d->meta) {
			return;
		}
		d->is2D = true;
		QString tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));
		const auto is8Bit = d->meta->data.data2DFLMIP.scalarType[ch][timestep] == 1;		
		if (d->isNaive) {			
			SoRef<OivHdf5Reader> reader = new OivHdf5Reader(true, is8Bit);
			reader->setDataGroupPath(QString("/Data/2DFLMIP/CH%1").arg(ch).toStdString());
			reader->setTileName(tileName.toStdString());
			reader->setFilename(d->tcfPath.toStdString());
			d->fl[ch]->setReader(*reader);
		}
		else {
			SoRef<OivLdmReaderFL> reader = new OivLdmReaderFL(ch, true, is8Bit);			
			reader->setTileDimension(d->meta->data.data2DFLMIP.tileSizeX, d->meta->data.data2DFLMIP.tileSizeY, 1);
			reader->setDataGroupPath(QString("/Data/2DFLMIP/CH%1").arg(ch).toStdString());
			reader->setTileName(tileName.toStdString());
			reader->setFilename(d->tcfPath.toStdString());
			d->fl[ch]->setReader(*reader);
		}
		const auto min = d->meta->data.data2DFLMIP.minIntensityList[ch][timestep];
		const auto max = d->meta->data.data2DFLMIP.maxIntensityList[ch][timestep];
		d->scene->SetDataMinMax(ch, min, max);
		d->flExist[ch] = true;
		d->scene->SetVolume(ch,d->fl[ch].ptr(),false);
	}
	auto ROISceneManager::ReadFL3D(int ch, int timestep) -> void {
		if (d->tcfPath.isEmpty()) {
			return;
		}
		if (nullptr == d->meta) {
			return;
		}
		d->is2D = false;
		QString tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));
		const auto is8Bit = d->meta->data.data3DFL.scalarType[ch][timestep] == 1;
		int flIdx = d->meta->data.data3DFL.sizeZ / 2;
		if (d->htExist) {
			flIdx = d->calcFLIndexFromHT(d->meta->data.data3D.sizeZ / 2);
		}
		if (d->isNaive) {
			d->flReader[ch]->set8Bit(is8Bit);
			d->flReader[ch]->setTileName(tileName.toStdString());
			d->flReader[ch]->setFilename(d->tcfPath.toStdString());			
			d->flReader[ch]->setIndex(flIdx);
			d->fl[ch]->setReader(*d->flReader[ch]);
		}
		else {
			d->flLdmReader[ch]->set8Bit(is8Bit);
			d->flLdmReader[ch]->setTileName(tileName.toStdString());
			d->flLdmReader[ch]->setTileDimension(d->meta->data.data3DFL.tileSizeX, d->meta->data.data3DFL.tileSizeY, d->meta->data.data3DFL.tileSizeZ);
			d->flLdmReader[ch]->setIndex(flIdx);
			d->flLdmReader[ch]->setFilename(d->tcfPath.toStdString());
			d->fl[ch]->setReader(*d->flLdmReader[ch]);
		}
		const auto min = d->meta->data.data3DFL.minIntensityList[ch][timestep];
		const auto max = d->meta->data.data3DFL.maxIntensityList[ch][timestep];
		d->scene->SetDataMinMax(ch, min, max);
		d->flExist[ch] = true;
		d->showFL[ch] = true;
		d->flInRange[ch] = !(flIdx < 0 || flIdx + 1 > d->meta->data.data3DFL.sizeZ);
		d->scene->SetVolume(ch, d->fl[ch].ptr(), false);		
		d->scene->ToggleViz(ch, d->flInRange[ch]);
	}
	auto ROISceneManager::SetTCFPath(const QString& path) -> void {
		d->tcfPath = path;
	}
	auto ROISceneManager::SetMeta(IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
		d->isNaive = !d->meta->data.isLDM;		
	}
	auto ROISceneManager::SetSliceNumber(int sliceNum,bool isHT) -> void {
		if (d->tcfPath.isEmpty()) {
			return;
		}
		if (nullptr == d->meta) {
			return;
		}
		if(d->is2D) {
			return;
		}
		//d->scene->SetSliceNumber(sliceNum-1,d->isNaive);
		if(false == d->isNaive) {
			if(d->htExist) {
				d->htLdmReader->setIndex(sliceNum - 1);
				d->htLdmReader->touch();
			}
			for(auto i=0;i<3;i++) {
				if (false == d->flExist[i])
					continue;
				if (isHT) {
					const auto flFromHT = d->calcFLIndexFromHT(sliceNum - 1);
					d->flInRange[i] = !(flFromHT < 0 || flFromHT + 1 > d->meta->data.data3DFL.sizeZ);
					if(d->flInRange[i]) {
						d->flLdmReader[i]->setIndex(flFromHT);						
					}
					d->scene->ToggleViz(i, d->showFL[i]&&d->flInRange[i]);
				}else {
					d->flLdmReader[i]->setIndex(sliceNum-1);
				}
				d->flLdmReader[i]->touch();
			}
		}else {
			if(d->htExist) {
				d->htReader->setIndex(sliceNum - 1);
				d->htReader->touch();
			}
			for (auto i = 0; i < 3; i++) {
				if (false == d->flExist[i])
					continue;
				if (isHT) {
					const auto flFromHT = d->calcFLIndexFromHT(sliceNum - 1);
					d->flInRange[i] = !(flFromHT < 0 || flFromHT + 1 > d->meta->data.data3DFL.sizeZ);
					if (d->flInRange[i]) {
						d->flReader[i]->setIndex(flFromHT);
					}
					d->scene->ToggleViz(i, d->showFL[i] && d->flInRange[i]);					
				}
				else {
					d->flReader[i]->setIndex(sliceNum - 1);
				}
				d->flReader[i]->touch();
			}
		}
	}
	auto ROISceneManager::GetSceneGraph() -> SoSwitch* {
		return d->scene->GetRootSwitch();
	}
}