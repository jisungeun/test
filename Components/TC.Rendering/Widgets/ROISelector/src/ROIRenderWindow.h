#pragma once
#include <memory>
#include <enum.h>

#include <QOivRenderWindow.h>

class SoEventCallback;
namespace TC::Rendering::Widgets::ROISelector {
	class ROIRenderWindow : public QOivRenderWindow {
		Q_OBJECT
	public:
		ROIRenderWindow(QWidget* parent);
		~ROIRenderWindow();

		auto ResetView2D()->void;//XY only

	signals:
		void sigMouseWheel(int);

	protected:
		auto MouseButtonEvent(SoEventCallback* node) -> void override;
		auto MouseMoveEvent(SoEventCallback* node) -> void override;
		auto KeyboardEvent(SoEventCallback* node) -> void override;
		auto MouseWheelEvent(SoEventCallback* node) -> void override;

		auto setDefaultWindowType() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}