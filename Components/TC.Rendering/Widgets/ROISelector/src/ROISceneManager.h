#pragma once

#include <memory>

#include <TCFMetaReader.h>

class SoSwitch;

namespace TC::Rendering::Widgets::ROISelector {
	class ROISceneManager {
	public:
		ROISceneManager();
		~ROISceneManager();

		auto Clear()->void;
		auto ClearScene()->void;

		auto SetTCFPath(const QString& path)->void;
		auto SetMeta(IO::TCFMetaReader::Meta::Pointer meta)->void;
		auto ReadHT2D(int timestep)->void;
		auto ReadHT3D(int timestep)->void;
		auto ReadFL2D(int ch, int timestep)->void;
		auto ReadFL3D(int ch, int timestep)->void;

		auto ToggleHT(bool show)->void;
		auto ToggleFL(int ch, bool show)->void;

		auto SetHTRange(double min, double max)->void;
		auto SetFLRange(int ch, int min, int max)->void;

		auto SetSliceNumber(int sliceNum,bool isHT)->void;

		auto GetSceneGraph()->SoSwitch*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}