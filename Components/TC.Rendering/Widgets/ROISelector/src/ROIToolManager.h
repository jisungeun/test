#pragma once

#include <memory>
#include <ROI.h>
#include <IROISep.h>

class SoSwitch;

namespace TC::Rendering::Widgets::ROISelector {
	enum {
		SQUARE,
		CIRCLE,
		ELLIPSE,
		POLYGON,
		LASSO,
		Idle = -1
	};

	class ItemZRange {
	public:
		ItemZRange() {}

		~ItemZRange() = default;
		int min { 1 };
		int max { 1 };
	};

	class ROIToolManager : public QObject {
		Q_OBJECT
	public:
		ROIToolManager(QObject* parent = nullptr);
		~ROIToolManager();

		auto GetRootSwitch() -> SoSwitch*;

		auto ActivateTool(int idx) -> void;
		auto DeactivateTool(int idx) -> void;
		auto DeactivateAll() -> void;
		auto SetGlobalZMinMax(int min, int max) -> void;
		auto SetCurrentZ(int zIdx) -> void;
		auto SetItemZRange(int toolIdx, int itemIdx, int zMin, int zMax) -> void;

		auto HighlightItem(int itemIdx) -> bool;
		auto SelectItem(int itemIdx) -> bool;
		auto DeleteItem(int itemIdx) -> bool;
		auto DeleteAll() -> void;
		auto GetROIInfo(int toolIdx) -> QList<IO::ROI>;
		auto AddROI(int toolIdx, IO::ROI roi) -> void;
		auto SetHandleSize(double size) -> void;

	signals:
		void sigRoiAdded(int toolIdx, int itemIdx);
		void sigRoiSelected(int toolIdx, int itemIdx);

	protected slots:
		void OnSquareAdded(int itemIdx);
		void OnCircleAdded(int itemIdx);
		void OnEllipseAdded(int itemIdx);
		void OnPolygonAdded(int itemIdx);
		void OnLassoAdded(int itemIdx);

		void OnSquareSelected(int itemIdx);
		void OnCircleSelected(int itemIdx);
		void OnEllipseSelected(int itemIdx);
		void OnPolygonSelected(int itemIdx);
		void OnLassoSelected(int itemIdx);

	private:
		auto InitConnections() -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
