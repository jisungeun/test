#include <QApplication>

#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <TCFMetaReader.h>
#include <TimePointSettingDialog.h>
//#include <ROISelectionWidget.h>

using namespace TC::TimePointSelector;
int main(int argc,char** argv) {
    QApplication app(argc, argv);
    app.setApplicationName("ROITestApp");
    //const auto parent = SoQt::init("Test");
    SoDB::init();
    SoVolumeRendering::init();
    TC::TimePointSelector::Config config;
        config.sources = {
        { Modality::Ht3d, {"HT 3d"}},
        { Modality::Fl3d, {"FL 3d CH1"}}
    };

    TimePointSettingDialog tps(config);
    QStringList tcfs = {
        //"F:/CellLibrary/230711_FLTimeLapse/230614.181755.20230614 Sytox Dapi.001.PMA.A1.T001P03.TCF",
        "F:/CellLibrary/2307013_HeteroFL/230712.151045.HeteroTimelapse.001.Hep3B_2CH_FIX.A1.T001P01.TCF",
        "F:/CellLibrary/210329_FL_gallery/HeLa-Rab/20201215.111055.269.HeLa Rab5a-GFP-002/20201215.111055.269.HeLa Rab5a-GFP-002.TCF"
    };
    tps.SetTCFs(tcfs);                    

    tps.exec();

    const auto result = tps.GetResult();
    /*   QList<TC::Rendering::Widgets::ROISelector::Config> roiConfig;
    TC::Rendering::Widgets::ROISelector::Config configs[3];
    configs[0].tcfPath = tcfs[0];
    configs[0].timeSelection = result[tcfs[0]];
    configs[1].tcfPath = tcfs[1];
    configs[1].timeSelection = result[tcfs[1]];
    //configs[2].tcfPath = tcfs[2];
    //configs[2].timeSelection = result[tcfs[2]];
    
    roiConfig.append(configs[0]);
    roiConfig.append(configs[1]);
    //roiConfig.append(configs[2]);

    auto widget = new ROISelectionWidget();
    widget->SetInput(roiConfig);
    widget->show();
    //SoQt::show(widget);
    //SoQt::mainLoop();                
    */
    app.exec();

    //delete widget;

    SoVolumeRendering::finish();
    //SoQt::finish();
    SoDB::finish();

    return EXIT_SUCCESS;   
}