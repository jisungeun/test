#pragma once

#include <QList>

#include "TimePointSelectorDef.h"

namespace TC::Rendering::Widgets::ROISelector {	
	struct Config {
		QString tcfPath;
		TimePointSelector::TimeSelectionOutput timeSelection;
		QList<double> realTime;
	};
	using inputStream = QList<Config>;
}
