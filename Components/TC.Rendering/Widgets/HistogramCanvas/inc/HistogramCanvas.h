#pragma once

#include <enum.h>
#include <QGraphicsView>
#include <TransparencyMap.h>

#include "TC.Rendering.Widgets.HistogramCanvasExport.h"

BETTER_ENUM(MouseType, int,
			Idle = 0,
			Handle = 1
			)

class TC_Rendering_Widgets_HistogramCanvas_API HistogramCanvas : public QGraphicsView {
	Q_OBJECT
public:
	HistogramCanvas(QWidget* parent = nullptr);
	~HistogramCanvas();
	auto SetTransparencyMap(TransparencyMapPtr tm) -> void;

	auto SetDataXRange(double xmin, double xmax, int div) -> void;
	auto SetWindowXRange(double min, double max)->void;	
	auto SetDataHistogram(QColor color, QList<int> value) -> void;	
	auto RequestDraw() -> void;
	auto Clear() -> void;
signals:	
	void resetRequested();
	void sigTransp();
protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
