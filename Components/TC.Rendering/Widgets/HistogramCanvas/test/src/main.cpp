#include <iostream>

#include <QApplication>

#include <Inventor/Qt/SoQt.h>

#include <HistogramCanvas.h>
#include <TransparencyMap.h>

void main(int argc, char** argv) {
	QApplication app(argc, argv);

	HistogramCanvas canvas;

	auto tf = std::make_shared<TransparencyMap>();
	std::srand(std::time(0));

	QList<int> histo;
	for (int i = 0; i < 100; ++i) {
		histo.append(std::rand() % 101); // % 101 to get numbers between 0 and 100
	}
	canvas.setMinimumSize(800, 200);
	canvas.SetTransparencyMap(tf);
	canvas.SetDataHistogram(Qt::gray, histo);
	canvas.show();

	app.exec();
}