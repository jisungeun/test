#include "HistogramCanvas.h"

#include <iostream>
#include <QGraphicsRectItem>
#include <QResizeEvent>

struct HistogramCanvas::Impl {
	int32_t cur_scene_height { 0 };
	int32_t cur_scene_width { 0 };

	HistogramCanvas* thisPointer { nullptr };
	QGraphicsTextItem* rangeText[2] { nullptr, };

	MouseType mouse_type { MouseType::Idle };

	QList<int> histogram_value;
	QColor histogram_color;
	//original data's information
	double xmin;//
	double xmax;
	double ymax;
	double data_ymax;
	int div { 1 };

	//range item's information	
	double range_min { -1 };
	double prev_min { -1 };
	double range_max { -1 };
	double prev_max { -1 };

	TransparencyMapPtr tm { nullptr };
	int GrabbedHandle { -1 };
	double GrabHandleX { 0 };
	int HoverHandle { -1 };
	QList<QGraphicsEllipseItem*> handles;
	float handleRadius { 6 };
	int finalHandleIdx { -1 };
	double epsilon { 0.1 };

	bool movingHandle { false };

	auto DrawHistogram() -> bool;
	auto CheckMousePos(QPoint pt) -> void;
	auto UpdateYMax() -> void;
	auto CalcCeil(double val, int precision) -> double;
	auto CalcFloor(double val, int precision) -> double;
	auto AddPoint(QPoint pt) -> std::tuple<int, double>;
	auto toTranspMapX(double x) -> double;
};

auto HistogramCanvas::Impl::toTranspMapX(double x) -> double {
	return x / static_cast<double>(cur_scene_width) * 255.0;
}


auto HistogramCanvas::Impl::CalcCeil(double val, int precision) -> double {
	double base = pow(10.0, precision);
	double result = val * base;
	result = ceil(result);
	result /= base;
	return result;
}

auto HistogramCanvas::Impl::CalcFloor(double val, int precision) -> double {
	double base = pow(10.0, precision);
	double result = val * base;
	result = floor(result);
	result /= base;
	return result;
}

auto HistogramCanvas::Impl::UpdateYMax() -> void {
	auto tmpymax = -1.0;
	auto startingIdx = static_cast<int>(range_min);
	auto steps = histogram_value.count();
	steps -= startingIdx;
	steps -= static_cast<int>(-range_max);
	for (auto i = startingIdx; i < startingIdx + steps; i++) {
		if (tmpymax < histogram_value[i]) {
			tmpymax = histogram_value[i];
		}
	}
	ymax = tmpymax;
}

auto HistogramCanvas::Impl::CheckMousePos(QPoint pt) -> void {
	const auto px = pt.x();
	const auto py = pt.y();
	mouse_type = MouseType::Idle;
	HoverHandle = -1;
	for (auto i = 0; i < handles.count(); i++) {
		const auto handle = handles[i];
		if (handle->boundingRect().contains(pt)) {
			mouse_type = MouseType::Handle;
			HoverHandle = i;
			break;
		}
	}
}

auto HistogramCanvas::Impl::AddPoint(QPoint pt) -> std::tuple<int, double> {
	const auto px = pt.x();
	const auto py = pt.y();

	auto tx = toTranspMapX(px);
	if (tx < 0)
		tx = 0;
	if (tx > 255)
		tx = 255;
	const auto dataPointer = tm->GetDataPointer();
	const auto dataSize = tm->GetSize();
	for (auto i = 0; i < dataSize; i++) {
		const auto prevX = dataPointer[i * 2];
		if (fabs(prevX - tx) < epsilon) {
			return {};
		}
	}

	auto ty = 1 - static_cast<double>(py) / cur_scene_height;
	if (ty < 0)
		ty = 0;
	if (ty > 1)
		ty = 1;
	auto idx = tm->AddPoint(tx, ty);
	finalHandleIdx = tm->GetSize() - 1;
	return std::make_tuple(idx, tx);
}

auto HistogramCanvas::Impl::DrawHistogram() -> bool {
	if (histogram_value.isEmpty()) {
		return false;
	}

	auto steps = histogram_value.count();
	auto startIdx = 0;
	if (range_min > xmin || range_max < xmax) {
		steps -= static_cast<int>(range_min - xmin);
		steps -= static_cast<int>(xmax - range_max);
		startIdx = static_cast<int>(range_min - xmin);
	}
	auto unit_width = static_cast<double>(cur_scene_width) / steps;
	for (auto i = startIdx; i < startIdx + steps; i++) {
		auto val = histogram_value[i];
		auto startPoint = (i - startIdx) * unit_width;
		auto height = cur_scene_height * val / ymax;
		auto tempRect = thisPointer->scene()->addRect(startPoint, cur_scene_height - height, unit_width, height, QPen(QColor(0, 0, 0, 0)), QBrush(histogram_color));
		tempRect->setFlag(QGraphicsItem::ItemIsFocusable, false);
		tempRect->setFlag(QGraphicsItem::ItemIsMovable, false);
		tempRect->setFlag(QGraphicsItem::ItemIsSelectable, false);
	}
	return true;
}

HistogramCanvas::HistogramCanvas(QWidget* parent) : QGraphicsView(parent), d { new Impl } {
	this->setViewportUpdateMode(ViewportUpdateMode::SmartViewportUpdate);
	setDragMode(DragMode::NoDrag);
	setMouseTracking(true);
	setCacheMode(QGraphicsView::CacheBackground);
	setBackgroundBrush(QBrush(Qt::black));
	//setRenderHint(QPainter::Antialiasing);
	setMaximumHeight(300);
	QFont font;
	font.setBold(true);
	font.setPointSize(12);
	for (auto i = 0; i < 2; i++) {
		d->rangeText[i] = new QGraphicsTextItem;
		d->rangeText[i]->setDefaultTextColor(QColor(255, 255, 255, 255));
		d->rangeText[i]->setFont(font);
	}
	d->thisPointer = this;	
}

HistogramCanvas::~HistogramCanvas() {}

auto HistogramCanvas::SetTransparencyMap(TransparencyMapPtr tm) -> void {
	d->tm = tm;
	d->finalHandleIdx = tm->GetSize() - 1;
	RequestDraw();
}

auto HistogramCanvas::SetDataHistogram(QColor color, QList<int> value) -> void {
	d->histogram_value = value;
	d->histogram_color = color;
	auto ymax = -1.0;
	for (auto val : value) {
		if (ymax < val) {
			ymax = val;
		}
	}
	d->data_ymax = d->ymax = ymax;
	this->RequestDraw();
}

auto HistogramCanvas::SetWindowXRange(double min, double max) -> void {
	d->range_min = min;
	d->range_max = max;
	if (d->range_min > d->xmin || d->range_max < d->xmax) {
		d->UpdateYMax();
	}
	this->RequestDraw();
}

auto HistogramCanvas::SetDataXRange(double xmin, double xmax, int div) -> void {
	d->xmin = xmin;
	d->div = div;
	d->xmax = xmax;
	if (div == 1) {
		d->rangeText[0]->setPlainText(QString::number(static_cast<int>(xmin)));
		d->rangeText[1]->setPlainText(QString::number(static_cast<int>(xmax)));
	} else {
		auto finalMin = d->CalcCeil(xmin / static_cast<double>(div), 3);
		auto finalMax = d->CalcFloor(xmax / static_cast<double>(div), 3);
		d->rangeText[0]->setPlainText(QString::number(finalMin, 'f', 3));
		d->rangeText[1]->setPlainText(QString::number(finalMax, 'f', 3));
	}
	d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[1]->boundingRect().height());
	d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
}

auto HistogramCanvas::Clear() -> void {
	d->histogram_value.clear();
	d->histogram_color = QColor();
	d->xmin = -1;
	d->xmax = -1;
	d->ymax = -1;
}

auto HistogramCanvas::RequestDraw() -> void {
	auto scene = new QGraphicsScene;
	scene->setSceneRect(QRectF(0, 0, d->cur_scene_width, d->cur_scene_height));
	setScene(scene);
	if (d->DrawHistogram()) {
		d->handles.clear();
		scene->addItem(d->rangeText[0]);
		scene->addItem(d->rangeText[1]);
		d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[0]->boundingRect().height());
		d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
		if (d->tm) {
			const auto dataPointer = d->tm->GetDataPointer();
			const auto pointNum = d->tm->GetSize();
			//Draw Line
			for (auto i = 0; i < pointNum - 1; i++) {
				const auto x1 = dataPointer[i * 2];// 0 ~ 255
				const auto y1 = dataPointer[i * 2 + 1];// 0 ~ 1
				const auto scenePosX1 = static_cast<float>(d->cur_scene_width) * x1 / 255;
				const auto scenePosY1 = static_cast<float>(d->cur_scene_height) * (1 - y1);

				const auto x2 = dataPointer[(i + 1) * 2];// 0 ~ 255
				const auto y2 = dataPointer[(i + 1) * 2 + 1];// 0 ~ 1
				const auto scenePosX2 = static_cast<float>(d->cur_scene_width) * x2 / 255;
				const auto scenePosY2 = static_cast<float>(d->cur_scene_height) * (1 - y2);
				scene->addLine(scenePosX1, scenePosY1, scenePosX2, scenePosY2, QPen(Qt::red, 1.5));
			}
			//Draw Handle			
			for (auto i = 0; i < pointNum; i++) {
				auto radius = d->handleRadius;
				QBrush handleBrush = QBrush(Qt::green, Qt::BrushStyle::SolidPattern);
				if (d->GrabbedHandle == i) {
					radius *= 1.5;
					handleBrush = QBrush(Qt::yellow, Qt::BrushStyle::SolidPattern);
				}
				const auto x = dataPointer[i * 2];// 0 ~ 255
				const auto y = dataPointer[i * 2 + 1];// 0 ~ 1
				const auto scenePosX = static_cast<float>(d->cur_scene_width) * x / 255;
				const auto scenePosY = static_cast<float>(d->cur_scene_height) * (1 - y);
				const auto tempEllipse = scene->addEllipse(scenePosX - radius / 2, scenePosY - radius / 2, radius, radius, QPen(Qt::red, 1), handleBrush);
				d->handles.push_back(tempEllipse);
			}
		}
		emit sigTransp();
	}
}

void HistogramCanvas::mouseDoubleClickEvent(QMouseEvent* event) {
	if (event->button() == Qt::MouseButton::RightButton) {
		emit resetRequested();
	}
	QGraphicsView::mouseDoubleClickEvent(event);
}

void HistogramCanvas::mousePressEvent(QMouseEvent* event) {
	d->prev_min = d->range_min;
	d->prev_max = d->range_max;
	if (event->button() & Qt::LeftButton) {
		if (d->mouse_type._to_index() == MouseType::Handle) {
			d->GrabbedHandle = d->HoverHandle;
			d->GrabHandleX = d->tm->GetDataPointer()[d->GrabbedHandle * 2];
			d->HoverHandle = -1;
			d->movingHandle = true;
			RequestDraw();
		} else if (d->mouse_type._to_index() == MouseType::Idle) {
			if (const auto [newIdx,newX] = d->AddPoint(event->pos()); newIdx > -1) {
				d->GrabbedHandle = newIdx;
				d->HoverHandle = -1;
				d->GrabHandleX = newX;
				d->movingHandle = true;
				RequestDraw();
			}
		}
	} else if (event->button() & Qt::RightButton) {
		if (d->mouse_type._to_index() == MouseType::Handle){
			auto targetIdx = -1;
			for (auto i = 1; i < d->handles.count() - 1; i++) {
				const auto handle = d->handles[i];
				if (handle->boundingRect().contains(event->pos())) {					
					targetIdx = i;
					break;
				}
			}
			if (targetIdx > -1) {
				const auto dataPointer = d->tm->GetDataPointer();
				const auto targetX = dataPointer[targetIdx * 2];
				d->tm->RemovePoint(targetX);			
				RequestDraw();
			}
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void HistogramCanvas::mouseReleaseEvent(QMouseEvent* event) {
	if (d->movingHandle) {
		d->movingHandle = false;
		d->GrabbedHandle = -1;
		d->GrabHandleX = 0;
		RequestDraw();
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void HistogramCanvas::mouseMoveEvent(QMouseEvent* event) {
	if (d->movingHandle) {
		//Mouse move action
		auto handle = d->handles[d->GrabbedHandle];
		double movedX = event->pos().x();
		double movedY = event->pos().y();
		auto tx = d->toTranspMapX(movedX);
		auto ty = 1 - movedY / d->cur_scene_height;
		if (d->GrabbedHandle == 0) {
			tx = 0;
		}
		if (d->GrabbedHandle == d->finalHandleIdx) {
			tx = 255;
		}
		if (tx < 0)
			tx = 0;
		if (tx > 255)
			tx = 255;
		if (ty < 0)
			ty = 0;
		if (ty > 1)
			ty = 1;

		const auto dataPointer = d->tm->GetDataPointer();
		const auto dataSize = d->tm->GetSize();
		for (auto i = 0; i < dataSize; i++) {
			if (i == d->GrabbedHandle) {
				continue;
			}
			const auto prevX = dataPointer[i * 2];
			if (fabs(prevX - tx) < d->epsilon) {
				d->GrabbedHandle = -1;
				d->GrabHandleX = 0;
				d->movingHandle = false;
				RequestDraw();
				QGraphicsView::mouseMoveEvent(event);
				return;
			}
		}

		d->tm->RemovePoint(d->GrabHandleX);
		d->tm->AddPoint(tx, ty);
		d->GrabHandleX = tx;
		d->finalHandleIdx = d->tm->GetSize() - 1;
		RequestDraw();

	} else {
		d->CheckMousePos(event->pos());
	}

	QGraphicsView::mouseMoveEvent(event);
}

void HistogramCanvas::resizeEvent(QResizeEvent* event) {
	auto new_height = event->size().height();
	auto new_width = event->size().width();
		
	d->cur_scene_height = new_height;
	d->cur_scene_width = new_width;

	this->RequestDraw();

	QGraphicsView::resizeEvent(event);
}
