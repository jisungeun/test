#pragma once

#include <enum.h>
#include <QWidget>

#include "TC.Rendering.Widgets.ColormapCanvasExport.h"

class ColorTransferFunction;
using ColorTFPtr = std::shared_ptr<ColorTransferFunction>;
class TC_Rendering_Widgets_ColormapCanvas_API ColorTransferFunction {
public:
	ColorTransferFunction();
	~ColorTransferFunction();

	auto SetColormap(QList<float> colormapArr)->void;
	auto AddRGBAPoint(double xpos,int x, float r, float g, float b, float a)->void;
	auto GetFunctionPos(int idx)->double;
	auto ToggleLinearInterpolation(bool isLinear)->void;

	auto RemovePoint(int x)->void;

	auto GetRedValue(int x)->float;
	auto GetGreenValue(int x)->float;
	auto GetBlueValue(int x)->float;
	auto GetAlphaValue(int x)->float;

	auto GetColor(int x, float rgba[4])->void;

	auto GetDataPointer()->float*;

	auto GetSize()->int;

	auto Clear()->void;

private:

	struct Impl;
	std::unique_ptr<Impl> d;
};