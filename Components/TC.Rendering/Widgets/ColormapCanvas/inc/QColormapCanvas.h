#pragma once

#include <enum.h>
#include <QWidget>
#include <QLineEdit>

#include "ColorMap.h"
#include "TC.Rendering.Widgets.ColormapCanvasExport.h"

class TC_Rendering_Widgets_ColormapCanvas_API QColormapCanvas : public QWidget {
	Q_OBJECT
public:
	QColormapCanvas(QWidget* parent = nullptr, Qt::WindowFlags f = nullptr);
	~QColormapCanvas();
	auto SetColorTransferFunction(ColorMapPtr tf)->void;

	auto GetMin() -> double;
	auto SetMin(double min) -> void;
	auto GetMax() -> double;
	auto SetMax(double max) -> void;		

	auto AddFunctionPoint(double x)->int;
	auto RemoveFunctionPoint(double x)->void;
	auto RemoveAllFunctionPoints()->void;
	auto GetFunctionX(int index)->double;
	auto GetFunctionY(int index)->double;
	auto GetFunctionMin()->double;
	auto GetFunctionMax()->double;
	auto GetFunctionSize()->int;
	auto SetQLineEdits(QLineEdit* xEdit, QLineEdit* yEdit)->void;
	auto DoubleClickOnHandle(int handle)->void;

	void mousePressEvent(QMouseEvent* mouseEvent) override;
	void mouseMoveEvent(QMouseEvent* mouseEvent) override;
	void mouseReleaseEvent(QMouseEvent* mouseEvent) override;
	void mouseDoubleClickEvent(QMouseEvent* mouseEvent) override;
	void keyPressEvent(QKeyEvent* e) override;
	void paintEvent(QPaintEvent* e) override;
	
	auto GetNearHandle(int x, int y, unsigned int maxSquaredDistance = 32)->int;
	auto MoveFunctionPoint(int index, std::pair<double, double> pos)->void;

	auto FunctionToCanvas(std::pair<double, double> functionPoint)->std::pair<int, int>;
	auto CanvasToFunction(std::pair<int, int> canvasPoint)->std::pair<double, double>;

	auto UpdateColormap()->void;

signals:
	void sigRender();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};