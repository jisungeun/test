#include <QMap>
#include <stdlib.h>
#include "TcColorTF.h"

#include <iostream>

typedef struct RGBAPoint {
	double xpos;
	float r;
	float g;
	float b;
	float a;
} TFNode;

struct ColorTransferFunction::Impl {
	float colormapArr[1024];
	QMap<int, TFNode> colormap;

	bool isLinear{ true };
	auto RebuildColorMap() -> void;
};

auto ColorTransferFunction::Impl::RebuildColorMap() -> void {
	if (colormap.count() == 0) {
		return;
	}
	auto keys = colormap.keys();
	std::sort(keys.begin(), keys.end());

	const auto firstKey = keys.first();
	const auto lastKey = keys.last();

	//fill left color with first key
	for (auto i = 0; i < firstKey; i++) {
		colormapArr[i * 4] = colormap[firstKey].r;
		colormapArr[i * 4 + 1] = colormap[firstKey].g;
		colormapArr[i * 4 + 2] = colormap[firstKey].b;
		colormapArr[i * 4 + 3] = colormap[firstKey].a;
	}

	//fill right color with last key
	for (auto i = lastKey; i < 256; i++) {
		colormapArr[i * 4] = colormap[lastKey].r;
		colormapArr[i * 4 + 1] = colormap[lastKey].g;
		colormapArr[i * 4 + 2] = colormap[lastKey].b;
		colormapArr[i * 4 + 3] = colormap[lastKey].a;
	}

	//fill intermediate color with interpolated value
	if (isLinear) {
		for (auto i = 0; i < keys.count() - 1; i++) {
			for (auto j = keys[i]; j < keys[i + 1]; j++) {
				const auto ratio = static_cast<float>(j - keys[i]) / static_cast<float>(keys[i + 1] - keys[i]);
				const auto rRange = colormap[keys[i + 1]].r - colormap[keys[i]].r;
				const auto gRange = colormap[keys[i + 1]].g - colormap[keys[i]].g;
				const auto bRange = colormap[keys[i + 1]].b - colormap[keys[i]].b;
				const auto aRange = colormap[keys[i + 1]].a - colormap[keys[i]].a;

				colormapArr[j * 4] = colormap[keys[i]].r + rRange * ratio;
				colormapArr[j * 4 + 1] = colormap[keys[i]].g + gRange * ratio;
				colormapArr[j * 4 + 2] = colormap[keys[i]].b + bRange * ratio;
				colormapArr[j * 4 + 3] = colormap[keys[i]].a + aRange * ratio;
			}
		}
	}else {
		for(auto i=0;i<keys.count()-1;i++) {
			for(auto j=keys[i];j<keys[i+1];j++) {
				colormapArr[j * 4] = colormap[keys[i]].r;
				colormapArr[j * 4 + 1] = colormap[keys[i]].g;
				colormapArr[j * 4 + 2] = colormap[keys[i]].b;
				colormapArr[j * 4 + 3] = colormap[keys[i]].a;
			}
		}
	}
}

ColorTransferFunction::ColorTransferFunction() : d { new Impl } {
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4] = 1.0f;
		d->colormapArr[i * 4 + 1] = 1.0f;
		d->colormapArr[i * 4 + 2] = 1.0f;
		d->colormapArr[i * 4 + 3] = 1.0f;
	}
}

ColorTransferFunction::~ColorTransferFunction() {}

auto ColorTransferFunction::ToggleLinearInterpolation(bool isLinear) -> void {
	d->isLinear = isLinear;
	d->RebuildColorMap();
}

auto ColorTransferFunction::AddRGBAPoint(double xpos, int x, float r, float g, float b, float a) -> void {
	TFNode node;
	node.xpos = xpos;
	node.r = r;
	node.g = g;
	node.b = b;
	node.a = a;
	d->colormap[x] = node;

	d->RebuildColorMap();
}

auto ColorTransferFunction::GetFunctionPos(int idx) -> double {
	auto keys = d->colormap.keys();
	if (keys.count() > idx) {
		std::sort(keys.begin(), keys.end());
		return d->colormap[keys[idx]].xpos;
	}
	return -1.0;
}

auto ColorTransferFunction::RemovePoint(int x) -> void {
	if (d->colormap.contains(x)) {
		d->colormap.remove(x);
	}
	d->RebuildColorMap();
}

auto ColorTransferFunction::GetRedValue(int x) -> float {
	return d->colormapArr[x * 4];
}

auto ColorTransferFunction::GetGreenValue(int x) -> float {
	return d->colormapArr[x * 4 + 1];
}

auto ColorTransferFunction::SetColormap(QList<float> colormapArr) -> void {	
	Clear();
	for (auto i = 0; i < 1024; i++) {
		d->colormapArr[i] = colormapArr[i];
	}
}

auto ColorTransferFunction::GetBlueValue(int x) -> float {
	return d->colormapArr[x * 4 + 2];
}

auto ColorTransferFunction::GetAlphaValue(int x) -> float {
	return d->colormapArr[x * 4 + 3];
}

auto ColorTransferFunction::GetColor(int x, float rgba[4]) -> void {
	for (auto i = 0; i < 4; i++) {
		rgba[i] = d->colormapArr[x * 4 + i];
	}
}

auto ColorTransferFunction::GetDataPointer() -> float* {
	return d->colormapArr;
}

auto ColorTransferFunction::GetSize() -> int {
	return d->colormap.count();
}

auto ColorTransferFunction::Clear() -> void {
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4] = 1.0f;
		d->colormapArr[i * 4 + 1] = 1.0f;
		d->colormapArr[i * 4 + 2] = 1.0f;
		d->colormapArr[i * 4 + 3] = 1.0f;
	}
	d->colormap.clear();
}
