#include <QPainter>
#include <QMouseEvent>
#include <QColorDialog>

#include "QColormapCanvas.h"

#include <iostream>

struct QColormapCanvas::Impl {
	ColorMapPtr tf{ nullptr };
	int GrabbedHandle{ -1 };	
	double Min{ 0 };
	double Max{ 1 };
	bool LineEditAvailable{ false };
	QLineEdit* XEdit{ nullptr };
	QLineEdit* YEdit{ nullptr };

	auto ValidateCoord(std::pair<double, double> x)->std::pair<double, double>;
};

auto QColormapCanvas::Impl::ValidateCoord(std::pair<double, double> x) -> std::pair<double, double> {
	double max = 0;
	double min = 255;
	if (x.first < min) x.first = min;
	if (x.first > max) x.first = max;
	if (x.second < 0) x.second = 0;
	if (x.second > 1) x.second = 1;
	return x;
}

QColormapCanvas::QColormapCanvas(QWidget* parent, Qt::WindowFlags f) : QWidget(parent), d{ new Impl } {
	setFocusPolicy(Qt::ClickFocus);
	//setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);	
}

QColormapCanvas::~QColormapCanvas() {
	
}

auto QColormapCanvas::SetQLineEdits(QLineEdit* xEdit, QLineEdit* yEdit) -> void {
	d->XEdit = xEdit;
	d->YEdit = yEdit;
	d->LineEditAvailable = true;
}

auto QColormapCanvas::SetColorTransferFunction(ColorMapPtr tf) -> void {
	d->tf = tf;
	update();	
}

auto QColormapCanvas::GetMin() -> double {
	return d->Min;
}

auto QColormapCanvas::SetMin(double min) -> void {
	d->Min = min;
}

auto QColormapCanvas::GetMax() -> double {
	return d->Max;
}

auto QColormapCanvas::SetMax(double max) -> void {
	d->Max = max;
}

auto QColormapCanvas::AddFunctionPoint(double x) -> int {
	const auto idx = static_cast<int>(x * 255.0);
	d->tf->AddRGBAPoint(x,idx, d->tf->GetRedValue(idx), d->tf->GetGreenValue(idx), d->tf->GetBlueValue(idx), d->tf->GetAlphaValue(idx));
	return 0;
}

auto QColormapCanvas::RemoveFunctionPoint(double x) -> void {	
	d->tf->RemovePoint(static_cast<int>(x*255));		
}

auto QColormapCanvas::RemoveAllFunctionPoints() -> void {
	d->tf->Clear();
}

auto QColormapCanvas::GetFunctionMin() -> double {
	return 0;
}

auto QColormapCanvas::GetFunctionMax() -> double {
	return 1;
}


auto QColormapCanvas::GetFunctionY(int index) -> double {
	return 0.0;
}

auto QColormapCanvas::GetFunctionX(int index) -> double {	
	return d->tf->GetFunctionPos(index);
}

auto QColormapCanvas::GetFunctionSize() -> int {
	return d->tf->GetSize();
}

auto QColormapCanvas::UpdateColormap() -> void {
	d->tf->UpdateTransparency();
}

void QColormapCanvas::paintEvent(QPaintEvent* e) {
	QPainter painter(this);	
	QRect contentsRect = this->contentsRect();
	//painter.setPen(Qt::gray);	
	painter.setPen(Qt::transparent);
	painter.drawRect(0, 0, contentsRect.width() + 1, contentsRect.height() + 1);	
	if(d->tf) {
		for (int x = contentsRect.x(); x < contentsRect.x() + contentsRect.width(); x++) {
			double xVal = d->Min + static_cast<float>(x) / contentsRect.width() * (d->Max
				- d->Min);
			const auto idx = static_cast<int>(xVal * 255);			
			QColor col(d->tf->GetRedValue(idx) * 255, d->tf->GetGreenValue(idx) * 255,
				d->tf->GetBlueValue(idx) * 255, d->tf->GetAlphaValue(idx) * 255);						
			painter.setPen(col);
			painter.drawLine(x, 0, x, contentsRect.height());
		}
	}	
	if (!this->isEnabled())
		return;
	if (d->tf)
	{
		// now paint the handles
		painter.setBrush(Qt::black);
		painter.setPen(Qt::black);
		for (int i = 0; i < this->GetFunctionSize(); i++)
		{
			int handleHeight = (i == d->GrabbedHandle) ? (int)(contentsRect.height() / 1.5)
				: contentsRect.height() / 2;
			int handleWidth = (i == d->GrabbedHandle) ? 6 : 4;
			std::pair<int, int> point = this->FunctionToCanvas(std::make_pair(
				GetFunctionX(i), 0.0f));
			int y = height() / 2;
			painter.drawRoundedRect(point.first - handleWidth / 2,
				y - handleHeight / 2, handleWidth, handleHeight, 50, 50);

			if (i == d->GrabbedHandle && d->LineEditAvailable)
			{
				int xCursor = d->XEdit->cursorPosition();
				d->XEdit->setText(QString::number(GetFunctionX(d->GrabbedHandle) / 10000, 'g', 4));
				d->XEdit->setCursorPosition(xCursor);
			}
		}
	}
}

void QColormapCanvas::mousePressEvent(QMouseEvent* mouseEvent)
{
	if (d->LineEditAvailable)
	{
		d->XEdit->clear();
		if (d->YEdit)
			d->YEdit->clear();
	}

	d->GrabbedHandle = GetNearHandle(mouseEvent->pos().x(), mouseEvent->pos().y());

	if ((mouseEvent->button() & Qt::LeftButton) && d->GrabbedHandle == -1)
	{
		this->AddFunctionPoint(
			this->CanvasToFunction(std::make_pair(mouseEvent->pos().x(),
				mouseEvent->pos().y())).first);
		d->GrabbedHandle = GetNearHandle(mouseEvent->pos().x(),
			mouseEvent->pos().y());
		emit sigRender();
	}
	else if ((mouseEvent->button() & Qt::RightButton) && d->GrabbedHandle != -1 && this->GetFunctionSize() > 1)
	{
		this->RemoveFunctionPoint(this->GetFunctionX(d->GrabbedHandle));
		d->GrabbedHandle = -1;
		emit sigRender();
	}
	update();
}

void QColormapCanvas::mouseMoveEvent(QMouseEvent* mouseEvent) {
	if (d->GrabbedHandle != -1)
	{
		std::pair<double, double>
			newPos = this->CanvasToFunction(std::make_pair(mouseEvent->x(),
				mouseEvent->y()));

		// X Clamping
		{
			// Check with predecessor
			if (d->GrabbedHandle > 0)
				if (newPos.first <= this->GetFunctionX(d->GrabbedHandle - 1))
					newPos.first = this->GetFunctionX(d->GrabbedHandle);

			// Check with sucessor
			if (d->GrabbedHandle < this->GetFunctionSize() - 1)
				if (newPos.first >= this->GetFunctionX(d->GrabbedHandle + 1))
					newPos.first = this->GetFunctionX(d->GrabbedHandle);

			// Clamping to histogramm
			if (newPos.first < d->Min) newPos.first = d->Min;
			else if (newPos.first > d->Max) newPos.first = d->Max;
		}

		// Y Clamping
		{
			if (newPos.second < 0.0) newPos.second = 0.0;
			else if (newPos.second > 1.0) newPos.second = 1.0;
		}

		// Move selected point
		this->MoveFunctionPoint(d->GrabbedHandle, newPos);

		update();

		emit sigRender();
	}
}

void QColormapCanvas::mouseReleaseEvent(QMouseEvent* mouseEvent) {
	update();
	emit sigRender();
}

void QColormapCanvas::mouseDoubleClickEvent(QMouseEvent* mouseEvent) {
	int nearHandle = GetNearHandle(mouseEvent->pos().x(), mouseEvent->pos().y());
	if (nearHandle != -1)
	{
		this->DoubleClickOnHandle(nearHandle);
	}
}

auto QColormapCanvas::DoubleClickOnHandle(int handle) -> void {
	double xVal = GetFunctionX(handle);
	const auto idx = static_cast<int>(xVal * 255);
	QColor col(d->tf->GetRedValue(idx) * 255,
		d->tf->GetGreenValue(idx) * 255,
		d->tf->GetBlueValue(idx) * 255,
		d->tf->GetAlphaValue(idx) * 255);
	QColorDialog diag;
	diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
	diag.setCurrentColor(col);
	diag.exec();
	QColor result = diag.selectedColor();
	if (result.isValid())
	{
		d->tf->AddRGBAPoint(xVal, idx, result.red() / 255.0,
			result.green() / 255.0, result.blue() / 255.0, result.alpha() / 255.0);
		this->update();
		emit sigRender();
	}
}

auto QColormapCanvas::GetNearHandle(int x, int y, unsigned maxSquaredDistance) -> int {
	Q_UNUSED(y)
	for (int i = 0; i < this->GetFunctionSize(); i++)
	{
		std::pair<int, int> point = this->FunctionToCanvas(std::make_pair(
			GetFunctionX(i), (double)0.0));
		if (static_cast<unsigned int>((point.first - x) * (point.first - x))
			< maxSquaredDistance)
		{
			return i;
		}
	}
	return -1;
}

void QColormapCanvas::keyPressEvent(QKeyEvent* e) {
	if (d->GrabbedHandle == -1)
		return;

	switch (e->key())
	{
	case Qt::Key_Delete:
		if (this->GetFunctionSize() > 1)
		{
			this->RemoveFunctionPoint(GetFunctionX(d->GrabbedHandle));
			d->GrabbedHandle = -1;
		}
		break;

	case Qt::Key_Left:
		this->MoveFunctionPoint(d->GrabbedHandle, d->ValidateCoord(std::make_pair(GetFunctionX(d->GrabbedHandle) - 1, GetFunctionY(d->GrabbedHandle))));
		break;

	case Qt::Key_Right:
		this->MoveFunctionPoint(d->GrabbedHandle, d->ValidateCoord(std::make_pair(GetFunctionX(d->GrabbedHandle) + 1, GetFunctionY(d->GrabbedHandle))));
		break;

	case Qt::Key_Up:
		this->MoveFunctionPoint(d->GrabbedHandle, d->ValidateCoord(std::make_pair(GetFunctionX(d->GrabbedHandle), GetFunctionY(d->GrabbedHandle) + 0.001)));
		break;

	case Qt::Key_Down:
		this->MoveFunctionPoint(d->GrabbedHandle, d->ValidateCoord(std::make_pair(GetFunctionX(d->GrabbedHandle), GetFunctionY(d->GrabbedHandle) - 0.001)));
		break;
	}

	update();
	emit sigRender();
}

auto QColormapCanvas::MoveFunctionPoint(int index, std::pair<double, double> pos) -> void {
	float color[4];
	d->tf->GetColor(static_cast<int>(GetFunctionX(index)*255), color);
	RemoveFunctionPoint(GetFunctionX(index));
	d->tf->AddRGBAPoint(pos.first,static_cast<int>(pos.first*255.0), color[0], color[1], color[2],color[3]);
}

auto QColormapCanvas::FunctionToCanvas(std::pair<double, double> functionPoint) -> std::pair<int, int> {
	return std::make_pair((int)((functionPoint.first - d->Min) / (d->Max
		- d->Min) * contentsRect().width()) + contentsRect().x(), (int)(contentsRect().height() * (1 - functionPoint.second)) + contentsRect().y());
}

auto QColormapCanvas::CanvasToFunction(std::pair<int, int> canvasPoint) -> std::pair<double, double> {
	return std::make_pair((canvasPoint.first - contentsRect().x()) * (d->Max - d->Min) / contentsRect().width()
		+ d->Min, 1.0 - (double)(canvasPoint.second - contentsRect().y()) / contentsRect().height());
}