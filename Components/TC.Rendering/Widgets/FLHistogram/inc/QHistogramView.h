#pragma once

#include <enum.h>
#include <QGraphicsView>

#include "TC.Rendering.Widgets.Histogram1dExport.h"

BETTER_ENUM(MouseType,int,
    Idle = 0,
    LeftHandle = 1,
    RightHandle =2,
    Body = 3)

class TC_Rendering_Widgets_Histogram1d_API QHistogramView : public QGraphicsView {
    Q_OBJECT
public:
    QHistogramView(QWidget* parent = nullptr);
    ~QHistogramView();

    auto SetDataXRange(double xmin, double xmax,int div)->void;
    auto SetWindowXRange(double min, double max)->void;
    auto SetDataHistogram(QColor color,QList<int> value)->void;
    auto ToggleCutoff(bool isCutoff)->void;
    auto RequestDraw()->void;
    auto Clear()->void;
signals:
    void windowRangeChanged(double, double);
    void resetRequested();
protected:
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};