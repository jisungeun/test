#pragma once

#include <QGraphicsView>
#include <QGraphicsItem>

#include "TC.Rendering.Widgets.Histogram1dExport.h"

class TC_Rendering_Widgets_Histogram1d_API QColorMapView : public QGraphicsView {
	Q_OBJECT
public:
	QColorMapView(QWidget* parent = nullptr);
	~QColorMapView();

	auto SetColorLUT(QList<QColor> LUT)->void;	
	auto SetDataXRange(double xmin, double xmax)->void;
	auto SetDataXRange(double xmin, double xmax, int div)->void;
	auto RequestDraw()->void;

protected:
	void resizeEvent(QResizeEvent* event) override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};