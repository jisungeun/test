#pragma once

#include <memory>
#include <QWidget>

#include "TC.Rendering.Widgets.Histogram1dExport.h"

class TC_Rendering_Widgets_Histogram1d_API QFLHistogram : public QWidget
{
	Q_OBJECT
public:
	QFLHistogram(QWidget* parent = nullptr);
	~QFLHistogram();

	auto OpenTCF(const QString& path)->bool;
	auto SetControlVisible(const bool& visible)->void;

signals:

protected:
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;	
		
private:
	struct Impl;
	std::unique_ptr<Impl> d;
};