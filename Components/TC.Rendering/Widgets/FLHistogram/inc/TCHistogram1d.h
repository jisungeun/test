#pragma once

#include <memory>
#include <QWidget>

#include "TC.Rendering.Widgets.Histogram1dExport.h"

namespace TC {
	class TC_Rendering_Widgets_Histogram1d_API Histogram1d : public QWidget {
		Q_OBJECT
	public:
		Histogram1d(QWidget* parent = nullptr);
		~Histogram1d();

		//global parameters
		auto Clear()->void;
		auto SetDataRange(double min, double max, int div)->void;
		auto RequestDraw()->void;
		auto SetOpacity(double opacity)->void;
		auto SetFixedOpacity(bool isFixed)->void;
		auto SetFixedHistogram(bool show)->void;
		auto SetReadOnlyHistogram(bool readOnly)->void;
		auto SetFixedColorMap(bool show)->void;

		//histogram view functions
		auto SetWindowRange(double min, double max)->void;
		auto SetDecimals(int dec)->void;
		auto SetSingleStep(double step)->void;
		auto SetHistogram(QColor color, QList<int> histogram)->void;

		//colormap view functions
		auto SetColorMap(QList<QColor> lut)->void;
		auto SetColorMap(const float* lut_ptr, int lut_count)->void;
		auto SetGammaValue(double gamma)->void;
		auto GetGammaValue()->double;
		auto SetGammaEnabled(bool isGamma)->void;
		auto GetGammaEnabled()->bool;

	signals:
		void sigApplyGamma(bool);
		void sigGammaValue(double);
		void sigRangeChanged(double, double);
		void sigOpacityChanged(double);
		void sigResetRange();

	protected slots:
		void OnHistoChkClicked();
		void OnColorChkClicked();
		void OnCutoffClicked();
		void OnGammaClicked();
		void OnGammaValueChanged(double);
		void OnWindowChanged(double, double);
		void OnOpacityChanged(double);
		void OnRangeMinChanged(double);
		void OnRangeMaxChanged(double);
		void OnResetRequested();

	private:
		auto InitUI()->void;
		auto InitConnections()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}