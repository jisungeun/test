#include "QColorMapView.h"

#include <QGraphicsRectItem>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextCharFormat>
#include <QResizeEvent>

struct QColorMapView::Impl {
    int32_t cur_scene_height{ 0 };
    int32_t cur_scene_width{ 0 };

    QList<QColor> lut;

    QColorMapView* thisPointer{ nullptr };

    QGraphicsTextItem* rangeText[2]{ nullptr, };

    double xmin{ 0 };
    double xmax{ 0 };
    int div{1};    

    auto DrawColorMapLUT()->bool;
    auto CalcCeil(double val, int precision)->double;
    auto CalcFloor(double val, int precision)->double;
};

auto QColorMapView::Impl::CalcCeil(double val, int precision) -> double {
    double base = pow(10.0, precision);
    double result = val * base;
    result = ceil(result);
    result /= base;
    return result;
}

auto QColorMapView::Impl::CalcFloor(double val, int precision) -> double {
    double base = pow(10.0, precision);
    double result = val * base;
    result = floor(result);
    result /= base;
    return result;
}

auto QColorMapView::Impl::DrawColorMapLUT() -> bool {
    if(lut.isEmpty()) {
        return false;
    }
    
    auto steps = lut.count();
    auto unit_width = static_cast<double>(cur_scene_width) / steps;
    for(auto i=0;i<steps;i++) {
        auto color = lut[i];
        auto startPoint = i * unit_width;
        
        auto tempRect = thisPointer->scene()->addRect(startPoint, 0, unit_width, cur_scene_height, QPen(), QBrush(color));        
        tempRect->setFlag(QGraphicsItem::ItemIsFocusable, false);
        tempRect->setFlag(QGraphicsItem::ItemIsMovable, false);
        tempRect->setFlag(QGraphicsItem::ItemIsSelectable, false);
        tempRect->setPen(Qt::NoPen);
    }
    return true;
}

QColorMapView::QColorMapView(QWidget* parent) : QGraphicsView(parent), d{ new Impl } {
    this->setViewportUpdateMode(SmartViewportUpdate);
    setDragMode(DragMode::NoDrag);
    setMouseTracking(false);
    setCacheMode(CacheBackground);
    setBackgroundBrush(QBrush(Qt::white));

    d->thisPointer = this;
    
    for (auto i = 0; i < 2; i++) {               

        d->rangeText[i] = new QGraphicsTextItem;
        d->rangeText[i]->setDefaultTextColor(QColor(255, 255,255));
    }    
}

QColorMapView::~QColorMapView() {
    
}
auto QColorMapView::SetDataXRange(double xmin, double xmax) -> void {
    d->xmin = xmin;
    d->xmax = xmax;

    auto document = new QTextDocument;
    QTextCharFormat charFormat;
    QFont f;
    f.setPointSize(12);
    f.setBold(true);
    charFormat.setFont(f);
    QPen outlinePen = QPen(QColor(0, 0, 0), 0.8, Qt::SolidLine);
    charFormat.setTextOutline(outlinePen);
    QTextCursor cursor = QTextCursor(document);    
    auto document2 = new QTextDocument;
    QTextCursor cursor2 = QTextCursor(document2);

    if (d->div == 1) {
        cursor.insertText(QString::number(static_cast<int>(xmin)), charFormat);
        d->rangeText[0]->setDocument(document);
        cursor2.insertText(QString::number(static_cast<int>(xmax)), charFormat);
        d->rangeText[1]->setDocument(document2);
    }
    else {
        const auto finalMin = d->CalcCeil(xmin / static_cast<double>(d->div), 3);
        cursor.insertText(QString::number(finalMin, 'f', 3), charFormat);
        d->rangeText[0]->setDocument(document);
        const auto finalMax = d->CalcFloor(xmax / static_cast<double>(d->div), 3);
        cursor2.insertText(QString::number(finalMax, 'f', 3), charFormat);
        d->rangeText[1]->setDocument(document2);
    }
    d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[1]->boundingRect().height());
    d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());

    d->rangeText[0]->update();
    d->rangeText[1]->update();
}

auto QColorMapView::SetDataXRange(double xmin, double xmax, int div) -> void {
    d->xmin = xmin;
    d->xmax = xmax;
    d->div = div;
    auto document = new QTextDocument;
    QTextCharFormat charFormat;
    QFont f;
    f.setPointSize(12);
    f.setBold(true);
    charFormat.setFont(f);
    QPen outlinePen = QPen(QColor(0, 0, 0), 0.8, Qt::SolidLine);
    charFormat.setTextOutline(outlinePen);
    QTextCursor cursor = QTextCursor(document);
    auto document2 = new QTextDocument;
    QTextCursor cursor2 = QTextCursor(document2);
    if(div == 1) {
        cursor.insertText(QString::number(static_cast<int>(xmin)), charFormat);
        d->rangeText[0]->setDocument(document);
        cursor2.insertText(QString::number(static_cast<int>(xmax)), charFormat);
        d->rangeText[1]->setDocument(document2);
    }else {
        const auto finalMin = d->CalcCeil(xmin / static_cast<double>(d->div), 3);
        cursor.insertText(QString::number(finalMin, 'f', 3), charFormat);
        d->rangeText[0]->setDocument(document);
        const auto finalMax = d->CalcFloor(xmax / static_cast<double>(d->div), 3);
        cursor2.insertText(QString::number(finalMax, 'f', 3), charFormat);
        d->rangeText[1]->setDocument(document2);
    }
    d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[0]->boundingRect().height());
    d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
}

auto QColorMapView::SetColorLUT(QList<QColor> LUT) -> void {
    d->lut = LUT;
    this->RequestDraw();    
}

auto QColorMapView::RequestDraw() -> void {
    auto scene = new QGraphicsScene;
    scene->setSceneRect(QRectF(0, 0, d->cur_scene_width, d->cur_scene_height));
    setScene(scene);

    if(d->DrawColorMapLUT()) {
        scene->addItem(d->rangeText[0]);
        d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[0]->boundingRect().height());
        scene->addItem(d->rangeText[1]);
        d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
    }
}

void QColorMapView::resizeEvent(QResizeEvent* event) {
    auto new_height = event->size().height();
    auto new_width = event->size().width();
        
    d->cur_scene_height = new_height;
    d->cur_scene_width = new_width;

    this->RequestDraw();
    
    QGraphicsView::resizeEvent(event);
}