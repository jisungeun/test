#include "TCHistogram1d.h"

#include "ui_TCHistogram1d.h"
namespace TC {
    struct Histogram1d::Impl {
        Ui::Histogram1d ui;

        double dataMin{ 0 };
        double dataMax{ 0 };

        double windowMin{ 0 };
        double windowMax{ 0 };

        int div;

        bool isFixedOpacity{ false };    
    };

    Histogram1d::Histogram1d(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        this->InitUI();
        this->InitConnections();
    }

    Histogram1d::~Histogram1d() {

    }

    auto Histogram1d::InitUI() -> void {
        d->ui.histoView->hide();
        d->ui.colorView->hide();

        d->ui.gammaSpin->setDecimals(2);
        d->ui.gammaSpin->setRange(0.01, 3.5);
        d->ui.gammaSpin->setSingleStep(0.01);
        d->ui.gammaSpin->setValue(1.0);

        d->ui.opacitySpin->setDecimals(2);
        d->ui.opacitySpin->setRange(0, 1);
        d->ui.opacitySpin->setSingleStep(0.01);
        d->ui.opacitySpin->setValue(0.7);

        d->ui.cutoffChk->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.gammaChk->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.colormapChk->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.histoChk->setStyleSheet("font-size: 11px; font-weight: bold;");
    }

    auto Histogram1d::InitConnections() -> void {
        connect(d->ui.histoChk, SIGNAL(clicked()), this, SLOT(OnHistoChkClicked()));
        connect(d->ui.colormapChk, SIGNAL(clicked()), this, SLOT(OnColorChkClicked()));
        connect(d->ui.cutoffChk, SIGNAL(clicked()), this, SLOT(OnCutoffClicked()));

        connect(d->ui.minSpin, SIGNAL(valueChanged(double)), this, SLOT(OnRangeMinChanged(double)));
        connect(d->ui.maxSpin, SIGNAL(valueChanged(double)), this, SLOT(OnRangeMaxChanged(double)));
        connect(d->ui.opacitySpin, SIGNAL(valueChanged(double)), this, SLOT(OnOpacityChanged(double)));

        connect(d->ui.gammaChk, SIGNAL(clicked()), this, SLOT(OnGammaClicked()));
        connect(d->ui.gammaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGammaValueChanged(double)));

        connect(d->ui.histoView, SIGNAL(windowRangeChanged(double, double)), this, SLOT(OnWindowChanged(double, double)));
        connect(d->ui.histoView, SIGNAL(resetRequested()), this, SLOT(OnResetRequested()));
    }

    //global parameters
    auto Histogram1d::SetFixedOpacity(bool isFixed) -> void {
        d->isFixedOpacity = isFixed;        
        d->ui.opacitySpin->setReadOnly(isFixed);        
    }

    auto Histogram1d::SetReadOnlyHistogram(bool readOnly) -> void {
        d->ui.minSpin->setReadOnly(readOnly);
        d->ui.maxSpin->setReadOnly(readOnly);
        d->ui.histoView->setDisabled(true);
        d->ui.cutoffChk->setVisible(!readOnly);
        d->ui.label_3->setVisible(!readOnly);
        d->ui.opacitySpin->setVisible(!readOnly);
    }
    auto Histogram1d::SetFixedHistogram(bool show) -> void {
        if(show) {
            d->ui.histoChk->setChecked(true);
            this->OnHistoChkClicked();
            d->ui.histoChk->hide();            
        }else {
            d->ui.histoChk->show();
        }
    }
    auto Histogram1d::SetFixedColorMap(bool show) -> void {
        if(show) {
            d->ui.colormapChk->setChecked(true);
            this->OnColorChkClicked();
            d->ui.colormapChk->hide();            
        }else {
            d->ui.colormapChk->show();
        }
    }
    auto Histogram1d::Clear() -> void {
        this->blockSignals(true);
        d->ui.gammaChk->setChecked(false);
        d->ui.gammaSpin->setValue(1.0);                        
        d->ui.opacitySpin->setValue(0.7);
        this->blockSignals(false);
    }
    auto Histogram1d::SetOpacity(double opacity) -> void {
        d->ui.opacitySpin->blockSignals(true);
        if (d->isFixedOpacity) {
            d->ui.opacitySpin->setValue(1);
            
        }else {
            d->ui.opacitySpin->setValue(opacity);
        }
        d->ui.opacitySpin->blockSignals(false);
    }
    auto Histogram1d::SetDataRange(double min, double max, int div) -> void {
        d->dataMax = max;
        d->dataMin = min;
        d->div = div;
        d->ui.colorView->SetDataXRange(min, max, div);
        d->ui.histoView->SetDataXRange(min, max, div);
        d->ui.minSpin->blockSignals(true);
        d->ui.minSpin->setRange(min/static_cast<double>(div), max/static_cast<double>(div) - d->ui.minSpin->singleStep());
        d->ui.minSpin->setValue(min/static_cast<double>(div));
        d->ui.minSpin->blockSignals(false);
        d->ui.maxSpin->blockSignals(true);
        d->ui.maxSpin->setRange(min/static_cast<double>(div) + d->ui.maxSpin->singleStep(), max/static_cast<double>(div));
        d->ui.maxSpin->setValue(max/static_cast<double>(div));
        d->ui.maxSpin->blockSignals(false);
        this->SetWindowRange(min, max);
    }

    auto Histogram1d::RequestDraw() -> void {
        d->ui.histoView->RequestDraw();
        d->ui.colorView->RequestDraw();
    }

    //colormap functions
    auto Histogram1d::SetColorMap(QList<QColor> lut) -> void {
        d->ui.colorView->SetColorLUT(lut);
    }

    auto Histogram1d::SetColorMap(const float* lut_ptr, int lut_count) -> void {
        QList<QColor> result;
        for (auto i = 0; i < lut_count; i++) {
            QColor color;
            color.setRedF(*lut_ptr++);
            color.setGreenF(*lut_ptr++);
            color.setBlueF(*lut_ptr++);
            color.setAlphaF(*lut_ptr++);
            result.append(color);
        }
        d->ui.colorView->SetColorLUT(result);
    }
    auto Histogram1d::GetGammaEnabled() -> bool {
        return d->ui.gammaChk->isChecked();
    }
    auto Histogram1d::SetGammaEnabled(bool isGamma) -> void {
        d->ui.gammaChk->blockSignals(true);
        d->ui.gammaChk->setChecked(isGamma);
        d->ui.gammaChk->blockSignals(false);
    }

    auto Histogram1d::GetGammaValue() -> double {
        return d->ui.gammaSpin->value();
    }

    auto Histogram1d::SetGammaValue(double gamma) -> void {
        d->ui.gammaSpin->blockSignals(true);
        d->ui.gammaSpin->setValue(gamma);
        d->ui.gammaSpin->blockSignals(false);
    }


    //hitogram functions
    auto Histogram1d::SetHistogram(QColor color, QList<int> histogram) -> void {
        d->ui.histoView->SetDataHistogram(color, histogram);
    }
    auto Histogram1d::SetWindowRange(double min, double max) -> void {
        d->windowMax = max;
        d->windowMin = min;        
        d->ui.histoView->SetWindowXRange(min, max);
        d->ui.colorView->SetDataXRange(min, max);
    }
    auto Histogram1d::SetDecimals(int dec) -> void {
        d->ui.minSpin->blockSignals(true);
        d->ui.minSpin->setDecimals(dec);
        d->ui.minSpin->blockSignals(false);
        d->ui.maxSpin->blockSignals(true);
        d->ui.maxSpin->setDecimals(dec);
        d->ui.maxSpin->blockSignals(false);
    }

    auto Histogram1d::SetSingleStep(double step) -> void {
        d->ui.minSpin->blockSignals(true);
        d->ui.minSpin->setSingleStep(step);
        d->ui.minSpin->blockSignals(false);
        d->ui.maxSpin->blockSignals(true);
        d->ui.maxSpin->setSingleStep(step);
        d->ui.maxSpin->blockSignals(false);
    }

    //slots
    void Histogram1d::OnResetRequested() {
        emit sigResetRange();
    }

    void Histogram1d::OnOpacityChanged(double value) {
        emit sigOpacityChanged(value);        
    }

    void Histogram1d::OnRangeMaxChanged(double val) {
        auto min = d->ui.minSpin->value();
        if (val < min + d->ui.minSpin->singleStep()) {
            d->ui.maxSpin->blockSignals(true);
            d->ui.maxSpin->setValue(val + d->ui.minSpin->singleStep());
            d->ui.maxSpin->blockSignals(false);
        }
        d->ui.histoView->SetWindowXRange(min * d->div, val * d->div);
        d->ui.colorView->SetDataXRange(min * d->div, val * d->div);

        d->ui.minSpin->blockSignals(true);
        d->ui.minSpin->setRange(d->ui.minSpin->minimum(), val - d->ui.minSpin->singleStep());
        d->ui.minSpin->blockSignals(false);

        emit sigRangeChanged(min * d->div, val * d->div);
    }

    void Histogram1d::OnRangeMinChanged(double val) {
        auto max = d->ui.maxSpin->value();
        if (val > max - d->ui.maxSpin->singleStep()) {
            d->ui.minSpin->blockSignals(true);
            d->ui.minSpin->setValue(val - d->ui.minSpin->singleStep());
            d->ui.minSpin->blockSignals(false);
        }
        d->ui.histoView->SetWindowXRange(val *d->div, max * d->div);
        d->ui.colorView->SetDataXRange(val * d->div, max * d->div);

        d->ui.maxSpin->blockSignals(true);
        d->ui.maxSpin->setRange(val + d->ui.maxSpin->singleStep(), d->ui.maxSpin->maximum());
        d->ui.maxSpin->blockSignals(false);

        emit sigRangeChanged(val * d->div, max * d->div);
    }

    void Histogram1d::OnWindowChanged(double min, double max) {
        d->ui.colorView->SetDataXRange(min, max);

        d->windowMin = min;
        d->windowMax = max;
        d->ui.minSpin->blockSignals(true);
        d->ui.minSpin->setRange(d->ui.minSpin->minimum(), max / static_cast<double>(d->div) - d->ui.minSpin->singleStep());
        d->ui.minSpin->setValue(min / static_cast<double>(d->div));
        d->ui.minSpin->blockSignals(false);
        d->ui.maxSpin->blockSignals(true);
        d->ui.maxSpin->setRange(min / static_cast<double>(d->div) + d->ui.maxSpin->singleStep(), d->ui.maxSpin->maximum());
        d->ui.maxSpin->setValue(max / static_cast<double>(d->div));
        d->ui.maxSpin->blockSignals(false);
        emit sigRangeChanged(min, max);
    }

    void Histogram1d::OnHistoChkClicked() {
        d->ui.histoView->setVisible(d->ui.histoChk->isChecked());
    }

    void Histogram1d::OnColorChkClicked() {
        d->ui.colorView->setVisible(d->ui.colormapChk->isChecked());
    }
    void Histogram1d::OnCutoffClicked() {
        //apply cutoff    
        d->ui.histoView->ToggleCutoff(d->ui.cutoffChk->isChecked());
    }
    void Histogram1d::OnGammaClicked() {
        //apply gamma correction for color map LUT
        emit sigApplyGamma(d->ui.gammaChk->isChecked());
        emit sigGammaValue(d->ui.gammaSpin->value());
    }
    void Histogram1d::OnGammaValueChanged(double val) {
        emit sigGammaValue(val);
    }
}