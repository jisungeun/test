#include <iostream>

#include "QFLHistogram.h"

#include <H5Cpp.h>

#include <QtCharts>

struct QFLHistogram::Impl {
	QColor chColor[3] = { QColor(Qt::blue), QColor(Qt::green),QColor(Qt::red) };
	QList<int> original_data[3];
	QList<int> histogram[3];
	double log_base = 1.5;
	bool isLog = true;
	int cutoff = 100;
	int histogram_step = 0;
	int spacing = 1;
	bool chExist[3] = { false,false,false };
	int64_t minSignal = 0;
	int64_t maxSignal = 0;
	bool is3d = false;

	QChart* chart = nullptr;
	QWidget* ctrlWidget = nullptr;

	auto ReadTCF(const QString& path)->bool;
	auto ReadOriginalData(int ch, H5::Group flGroup)->QList<int>;

	auto DrawChart(QChart* rootChart)->void;
	auto CalculateChHistogram(int ch, int steps)->QList<int>;
};

QFLHistogram::QFLHistogram(QWidget* parent) : QWidget(parent), d(new Impl()) {	
	d->chart = new QChart;
	d->chart->setBackgroundBrush(QBrush(QColor(Qt::black)));
	d->chart->layout()->setContentsMargins(0, 0, 0, 0);
	d->chart->setBackgroundRoundness(0);
	d->chart->legend()->hide();
	auto chartTitleFont = d->chart->titleFont();
	chartTitleFont.setBold(true);
	d->chart->setTitleBrush(QBrush(Qt::white));
	d->chart->setTitleFont(chartTitleFont);
	d->chart->setTitle("FL Histogram");
	auto chartView = new QChartView(d->chart);

	auto layout = new QVBoxLayout();
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	this->setLayout(layout);
	layout->addWidget(chartView);

	d->ctrlWidget = new QWidget;
	auto ctrlLayout = new QVBoxLayout();
	d->ctrlWidget->setLayout(ctrlLayout);
	layout->addWidget(d->ctrlWidget);

	auto logChk = new QCheckBox;
	logChk->setText("Toggle Log");
	logChk->setChecked(true);
	ctrlLayout->addWidget(logChk);

	auto logBaseSpin = new QDoubleSpinBox;
	logBaseSpin->setRange(1.1, 10.0);
	logBaseSpin->setSingleStep(0.1);
	logBaseSpin->setDecimals(1);
	logBaseSpin->setValue(1.5);

	auto logLayout = new QHBoxLayout;
	logLayout->setContentsMargins(0, 0, 0, 0);
	logLayout->setSpacing(0);

	auto logLabel = new QLabel;
	logLabel->setText("Log base");
	logLayout->addWidget(logLabel);
	logLayout->addWidget(logBaseSpin);
	ctrlLayout->addLayout(logLayout);

	auto cutOffSpin = new QSpinBox;
	cutOffSpin->setRange(1, 500);
	cutOffSpin->setSingleStep(1);
	cutOffSpin->setValue(100);

	auto cutoffLayout = new QHBoxLayout;
	cutoffLayout->setContentsMargins(0, 0, 0, 0);
	cutoffLayout->setSpacing(0);

	auto cutoffLabel = new QLabel;
	cutoffLabel->setText("Cut-off");
	cutoffLayout->addWidget(cutoffLabel);
	cutoffLayout->addWidget(cutOffSpin);
	ctrlLayout->addLayout(cutoffLayout);

	auto spacingSpin = new QSpinBox;
	spacingSpin->setRange(1, 50);
	spacingSpin->setSingleStep(1);
	spacingSpin->setValue(1);

	auto spaingLayout = new QHBoxLayout;
	spaingLayout->setContentsMargins(0, 0, 0, 0);
	spaingLayout->setSpacing(0);

	auto spacingLabel = new QLabel;
	spacingLabel->setText("Spacing");
	spaingLayout->addWidget(spacingLabel);
	spaingLayout->addWidget(spacingSpin);
	ctrlLayout->addLayout(spaingLayout);

	//connections
	QObject::connect(logBaseSpin, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=](double value) {
		d->log_base = logBaseSpin->value();
		if (d->isLog) {
			d->DrawChart(d->chart);
		}
		});
	QObject::connect(cutOffSpin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
		d->cutoff = cutOffSpin->value();
		d->DrawChart(d->chart);
		});
	QObject::connect(spacingSpin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
		if (false == d->isLog) {
			d->spacing = spacingSpin->value();
			d->DrawChart(d->chart);
		}
		else {
			spacingSpin->blockSignals(true);
			spacingSpin->setValue(1);
			spacingSpin->blockSignals(false);
		}
		});
	QObject::connect(logChk, &QCheckBox::clicked, [=]() {
		d->isLog = logChk->isChecked();
		if (d->isLog) {
			d->spacing = 1;
			spacingSpin->blockSignals(true);
			spacingSpin->setValue(1);
			spacingSpin->blockSignals(false);
		}
		else {
			d->spacing = spacingSpin->value();
		}
		d->DrawChart(d->chart);
		});
}

QFLHistogram::~QFLHistogram() {

}

auto QFLHistogram::OpenTCF(const QString& path)->bool {
	if (path.isEmpty()) return false;
	if (false == QFileInfo::exists(path)) return false;

	return d->ReadTCF(path);
}

auto QFLHistogram::SetControlVisible(const bool& visible)->void {
	d->ctrlWidget->setVisible(visible);
}

auto QFLHistogram::Impl::ReadTCF(const QString& path)->bool {
	if (path.isEmpty()) return false;
	if (false == QFileInfo::exists(path)) return false;

	using namespace H5;

	H5File file(path.toStdString(), H5F_ACC_RDONLY);
	auto dataGroup = file.openGroup("Data");
	Group flGroup;
	if (is3d) {
		if (false == dataGroup.exists("3DFL")) {
			dataGroup.close();
			file.close();
			return 0;
		}
		flGroup = dataGroup.openGroup("3DFL");
	}
	else {
		if (false == dataGroup.exists("2DFLMIP")) {
			dataGroup.close();
			file.close();
			return 0;
		}
		flGroup = dataGroup.openGroup("2DFLMIP");
	}
	auto flExist = false;
	for (auto i = 0; i < 3; i++) {
		auto chName = QString("CH%1").arg(i).toStdString();
		chExist[i] = flGroup.exists(chName);
		flExist |= chExist[i];
	}
	if (false == flExist) {
		flGroup.close();
		dataGroup.close();
		file.close();
		return 0;
	}
	auto maxAttrb = flGroup.openAttribute("MaxIntensity");
	maxAttrb.read(PredType::NATIVE_INT64, &maxSignal);
	maxAttrb.close();
	auto minAttrb = flGroup.openAttribute("MinIntensity");
	minAttrb.read(PredType::NATIVE_INT64, &minSignal);
	minAttrb.close();

	for (auto i = 0; i < 3; i++) {
		if (chExist[i]) {
			original_data[i] = ReadOriginalData(i, flGroup);
		}
	}
	flGroup.close();
	dataGroup.close();
	file.close();

	DrawChart(chart);

	return true;
}

auto QFLHistogram::Impl::ReadOriginalData(int ch, H5::Group flGroup)->QList<int> {
	auto result = QList<int>();
	auto chName = QString("CH%1").arg(ch).toStdString();
	auto chGroup = flGroup.openGroup(chName);
	auto chDSet = chGroup.openDataSet("000000");
	auto datspace = chDSet.getSpace();
	hsize_t dims[3];
	datspace.getSimpleExtentDims(dims);
	unsigned long long numberOfElements;
	if (is3d) {
		const auto dataSizeX = dims[2];
		const auto dataSizeY = dims[1];
		const auto dataSizeZ = dims[0];
		numberOfElements = dataSizeX * dataSizeY * dataSizeZ;
	}
	else {
		const auto dataSizeX = dims[1];
		const auto dataSizeY = dims[0];
		numberOfElements = dataSizeX * dataSizeY;
	}
	const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
	chDSet.read(rawData.get(), chDSet.getDataType());
	datspace.close();
	chDSet.close();
	chGroup.close();

	for (auto i = 0; i < numberOfElements; i++) {
		result.append(rawData.get()[i]);
	}
	return result;
}

auto QFLHistogram::Impl::DrawChart(QChart* rootChart)->void {
	rootChart->removeAllSeries();
	for (auto ax : rootChart->axes()) {
		rootChart->removeAxis(ax);
	}

	histogram_step = static_cast<int>(ceil(static_cast<float>(maxSignal - minSignal) / static_cast<float>(spacing)));
	if (isLog) {
		histogram_step = log(histogram_step) / log(log_base);
	}

	for (auto i = 0; i < 3; i++) {
		if (chExist[i]) {
			histogram[i] = CalculateChHistogram(i, histogram_step);
		}
	}

	auto yMax = -1;
	QLineSeries* series[3];
	for (auto i = 0; i < 3; i++) {
		series[i] = new QLineSeries;
		series[i]->setBrush(chColor[i]);
		rootChart->addSeries(series[i]);
		if (false == chExist[i])
			continue;		
		//auto barset = new QBarSet(QString("CH%1").arg(i));
		//barset->setBrush(chColor[i]);
		for (auto j = 0; j < histogram_step; j++) {
			series[i]->append(QPointF(j, histogram[i][j]));
			if (histogram[i][j] > yMax) {
				yMax = histogram[i][j];
			}
		}		
	}	

	auto axisX = new QBarCategoryAxis;
	QStringList categoryX;
	for (auto i = 0; i < histogram_step; i++) {
		if (isLog) {
			categoryX.append(QString::number(static_cast<int>(pow(log_base, (i + 1) * spacing))));
		}
		else {
			categoryX.append(QString::number((i + 1) * spacing));
		}

	}
	auto axisFont = axisX->labelsFont();
	axisFont.setBold(true);
	axisX->setCategories(categoryX);
	axisX->setLabelsFont(axisFont);
	axisX->setLabelsAngle(90);
	axisX->setLabelsBrush(QBrush(Qt::white));

	rootChart->addAxis(axisX, Qt::AlignBottom);

	auto axisY = new QValueAxis;
	if (isLog) {
		axisY->setRange(0, yMax + 50000);
	}
	else {
		axisY->setRange(0, yMax + 50);
	}
	axisY->setLabelsVisible(false);
	rootChart->addAxis(axisY, Qt::AlignLeft);

	for (auto i = 0; i < 3; i++) {
		series[i]->attachAxis(axisX);
		series[i]->attachAxis(axisY);
	}
}

auto QFLHistogram::Impl::CalculateChHistogram(int ch, int steps)->QList<int> {
	auto result = QList<int>();
	for (auto i = 0; i < steps; i++) {
		result.append(0);
	}

	for (auto i = 0; i < original_data[ch].count(); i++) {
		auto value = original_data[ch][i];
		if (value < cutoff)
			continue;
		int step = 0;
		if (isLog) {
			step = static_cast<int>(log(value / spacing) / log(log_base));
		}
		else {
			step = static_cast<int>(value / spacing);
		}
		result[step]++;
	}

	return result;
}

void QFLHistogram::mouseMoveEvent(QMouseEvent* event) {
    
}

void QFLHistogram::mousePressEvent(QMouseEvent* event) {
	std::cout << "Clicked Pos: " << event->x() << " " << event->y() << std::endl;
	std::cout << "Chart Rect : " << d->chart->rect().bottomLeft().x() << "!" << d->chart->rect().bottomLeft().y() << " " << d->chart->rect().size().width() << "," << d->chart->rect().size().height() << std::endl;	
}

void QFLHistogram::mouseReleaseEvent(QMouseEvent* event) {
    
}