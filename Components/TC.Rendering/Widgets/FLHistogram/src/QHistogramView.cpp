#include "QHistogramView.h"

#include <iostream>
#include <QGraphicsRectItem>
#include <QResizeEvent>

struct QHistogramView::Impl {
	int32_t cur_scene_height { 0 };
	int32_t cur_scene_width { 0 };

	int32_t startX { 0 };
	int32_t endX { 0 };

	QHistogramView* thisPointer { nullptr };
	QGraphicsRectItem* dragItem { nullptr };
	QGraphicsTextItem* rangeText[2] { nullptr, };

	bool moveRange { false };
	bool modifyMin { false };
	bool modifyMax { false };
	bool addRange { false };
	MouseType mouse_type { MouseType::Idle };
	QRectF prev_range_rect;

	QList<int> histogram_value;
	QColor histogram_color;
	//original data's information
	double xmin;
	double xmax;
	double ymax;
	double data_ymax;
	int div { 1 };
	bool cutoff { false };

	//range item's information
	QGraphicsRectItem* rangeItem { nullptr };
	double range_min { -1 };
	double prev_min { -1 };
	double range_max { -1 };
	double prev_max { -1 };

	auto DrawHistogram() -> bool;
	auto RedrawRangeItem() -> void;
	auto CheckMousePos(QPoint pt) -> void;
	auto EmitRangeChange() -> void;
	auto UpdateYMax() -> void;
	auto CalcCeil(double val, int precision) -> double;
	auto CalcFloor(double val, int precision) -> double;
};

auto QHistogramView::Impl::CalcCeil(double val, int precision) -> double {
	double base = pow(10.0, precision);
	double result = val * base;
	result = ceil(result);
	result /= base;
	return result;
}

auto QHistogramView::Impl::CalcFloor(double val, int precision) -> double {
	double base = pow(10.0, precision);
	double result = val * base;
	result = floor(result);
	result /= base;
	return result;
}

auto QHistogramView::Impl::UpdateYMax() -> void {
	auto tmpymax = -1.0;
	auto startingIdx = static_cast<int>(range_min - xmin);
	auto steps = histogram_value.count();
	steps -= startingIdx;
	steps -= static_cast<int>(xmax - range_max);
	for (auto i = startingIdx; i < startingIdx + steps; i++) {
		if (tmpymax < histogram_value[i]) {
			tmpymax = histogram_value[i];
		}
	}
	ymax = tmpymax;
}


auto QHistogramView::Impl::EmitRangeChange() -> void {
	auto startingX = rangeItem->rect().x();
	auto widthX = rangeItem->rect().width();
	auto minRatio = static_cast<double>(startingX) / static_cast<double>(cur_scene_width);
	auto maxRatio = static_cast<double>(startingX + widthX) / static_cast<double>(cur_scene_width);
	if (cutoff && (range_min > xmin || range_max < xmax)) {
		range_min = prev_min + (prev_max - prev_min) * minRatio;
		range_max = prev_min + (prev_max - prev_min) * maxRatio;
	} else {
		range_min = xmin + (xmax - xmin) * minRatio;
		range_max = xmin + (xmax - xmin) * maxRatio;
	}
	emit thisPointer->windowRangeChanged(range_min, range_max);
}

auto QHistogramView::Impl::CheckMousePos(QPoint pt) -> void {
	if (pt.x() > rangeItem->rect().x() - 3 && pt.x() < rangeItem->rect().x() + 3) {
		mouse_type = MouseType::LeftHandle;
		thisPointer->setCursor(QCursor(Qt::CursorShape::SizeHorCursor));
	} else if (pt.x() > rangeItem->rect().x() + rangeItem->rect().width() - 3 && pt.x() < rangeItem->rect().x() + rangeItem->rect().width() + 3) {
		mouse_type = MouseType::RightHandle;
		thisPointer->setCursor(QCursor(Qt::CursorShape::SizeHorCursor));
	} else if (pt.x() > rangeItem->rect().x() + 3 && pt.x() < rangeItem->rect().x() + rangeItem->rect().width() - 3) {
		mouse_type = MouseType::Body;
		thisPointer->setCursor(QCursor(Qt::CursorShape::OpenHandCursor));
	} else {
		mouse_type = MouseType::Idle;
		thisPointer->setCursor(QCursor(Qt::CursorShape::ArrowCursor));
	}
}

auto QHistogramView::Impl::RedrawRangeItem() -> void {
	if (cutoff && (range_min > xmin || range_max < xmax)) {
		if (div == 1) {
			rangeText[0]->setPlainText(QString::number(static_cast<int>(range_min)));
			rangeText[1]->setPlainText(QString::number(static_cast<int>(range_max)));
		} else {
			auto finalMin = CalcCeil(range_min / static_cast<double>(div), 3);
			auto finalMax = CalcFloor(range_max / static_cast<double>(div), 3);
			rangeText[0]->setPlainText(QString::number(finalMin, 'f', 3));
			rangeText[1]->setPlainText(QString::number(finalMax, 'f', 3));
		}
		rangeItem->setRect(0, 0, cur_scene_width, cur_scene_height);
	} else {
		if (div == 1) {
			rangeText[0]->setPlainText(QString::number(static_cast<int>(xmin)));
			rangeText[1]->setPlainText(QString::number(static_cast<int>(xmax)));
		} else {
			auto finalMin = CalcCeil(xmin / static_cast<double>(div), 3);
			auto finalMax = CalcFloor(xmax / static_cast<double>(div), 3);
			rangeText[0]->setPlainText(QString::number(finalMin, 'f', 3));
			rangeText[1]->setPlainText(QString::number(finalMax, 'f', 3));
		}
		auto minRatio = (range_min - xmin) / (xmax - xmin);
		auto maxRatio = (range_max - xmin) / (xmax - xmin);
		auto startingX = static_cast<double>(cur_scene_width) * minRatio;
		auto widthX = static_cast<double>(cur_scene_width) * (maxRatio - minRatio);
		rangeItem->setRect(startingX, 0, widthX, cur_scene_height);
	}
	rangeItem->update();
}

auto QHistogramView::Impl::DrawHistogram() -> bool {
	if (histogram_value.isEmpty()) {
		return false;
	}

	auto steps = histogram_value.count();
	auto startIdx = 0;
	if (cutoff && (range_min > xmin || range_max < xmax)) {
		steps -= static_cast<int>(range_min - xmin);
		steps -= static_cast<int>(xmax - range_max);
		startIdx = static_cast<int>(range_min - xmin);
	}
	auto unit_width = static_cast<double>(cur_scene_width) / steps;
	for (auto i = startIdx; i < startIdx + steps; i++) {
		auto val = histogram_value[i];
		auto startPoint = (i - startIdx) * unit_width;
		auto height = cur_scene_height * val / ymax;
		auto tempRect = thisPointer->scene()->addRect(startPoint, cur_scene_height - height, unit_width, height, QPen(QColor(0, 0, 0, 0)), QBrush(histogram_color));
		tempRect->setFlag(QGraphicsItem::ItemIsFocusable, false);
		tempRect->setFlag(QGraphicsItem::ItemIsMovable, false);
		tempRect->setFlag(QGraphicsItem::ItemIsSelectable, false);
	}
	return true;
}

QHistogramView::QHistogramView(QWidget* parent) : QGraphicsView(parent), d { new Impl } {
	this->setViewportUpdateMode(ViewportUpdateMode::SmartViewportUpdate);
	setDragMode(DragMode::NoDrag);
	setMouseTracking(true);
	setCacheMode(QGraphicsView::CacheBackground);
	setBackgroundBrush(QBrush(Qt::black));

	d->thisPointer = this;
}

QHistogramView::~QHistogramView() {}

auto QHistogramView::ToggleCutoff(bool isCutoff) -> void {
	d->cutoff = isCutoff;

	if (isCutoff && (d->range_min > d->xmin || d->range_max < d->xmax)) {
		d->UpdateYMax();
	} else {
		d->ymax = d->data_ymax;
	}
	this->RequestDraw();
}

auto QHistogramView::SetDataHistogram(QColor color, QList<int> value) -> void {
	d->histogram_value = value;
	d->histogram_color = color;
	auto ymax = -1.0;
	for (auto val : value) {
		if (ymax < val) {
			ymax = val;
		}
	}
	d->data_ymax = d->ymax = ymax;
	this->RequestDraw();
}

auto QHistogramView::SetWindowXRange(double min, double max) -> void {
	d->range_min = min;
	d->range_max = max;

	if (d->cutoff && (d->range_min > d->xmin || d->range_max < d->xmax)) {
		d->UpdateYMax();
	}
	this->RequestDraw();
}

auto QHistogramView::SetDataXRange(double xmin, double xmax, int div) -> void {
	d->xmin = xmin;
	d->div = div;
	d->xmax = xmax;
	if (!d->rangeText[0]) {
		return;
	}
	if (div == 1) {
		d->rangeText[0]->setPlainText(QString::number(static_cast<int>(xmin)));
		d->rangeText[1]->setPlainText(QString::number(static_cast<int>(xmax)));
	} else {
		auto finalMin = d->CalcCeil(xmin / static_cast<double>(div), 3);
		auto finalMax = d->CalcFloor(xmax / static_cast<double>(div), 3);
		d->rangeText[0]->setPlainText(QString::number(finalMin, 'f', 3));
		d->rangeText[1]->setPlainText(QString::number(finalMax, 'f', 3));
	}
	d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[1]->boundingRect().height());
	d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
	d->RedrawRangeItem();
}

auto QHistogramView::Clear() -> void {
	d->histogram_value.clear();
	d->histogram_color = QColor();
	d->xmin = -1;
	d->xmax = -1;
	d->ymax = -1;
}

auto QHistogramView::RequestDraw() -> void {
	auto* oldScene = scene();

	auto newScene = new QGraphicsScene;
	newScene->setSceneRect(QRectF(0, 0, d->cur_scene_width, d->cur_scene_height));
	setScene(newScene);

	delete oldScene;

	if (d->DrawHistogram()) {
		d->dragItem = new QGraphicsRectItem(0, 0, 0, 0);
		d->dragItem->setPen(QPen(QColor(255, 0, 0, 255)));
		d->dragItem->setBrush(QBrush(QColor(255, 0, 0, 125)));

		d->rangeItem = new QGraphicsRectItem(0, 0, 0, 0);
		d->rangeItem->setPen(QPen(QColor(Qt::darkGray)));
		d->rangeItem->setBrush(QBrush(QColor(Qt::darkGray), Qt::BrushStyle::BDiagPattern));

		QFont font;
		font.setBold(true);
		font.setPointSize(12);
		for (auto i = 0; i < 2; i++) {
			d->rangeText[i] = new QGraphicsTextItem;
			d->rangeText[i]->setDefaultTextColor(QColor(255, 255, 255, 255));
			d->rangeText[i]->setFont(font);
		}

		newScene->addItem(d->dragItem);
		newScene->addItem(d->rangeItem);

		newScene->addItem(d->rangeText[0]);
		newScene->addItem(d->rangeText[1]);
		d->RedrawRangeItem();
		d->rangeText[0]->setPos(0, d->cur_scene_height - d->rangeText[0]->boundingRect().height());
		d->rangeText[1]->setPos(d->cur_scene_width - d->rangeText[1]->boundingRect().width(), d->cur_scene_height - d->rangeText[1]->boundingRect().height());
	}
}

void QHistogramView::mouseDoubleClickEvent(QMouseEvent* event) {
	if (event->button() == Qt::MouseButton::RightButton) {
		emit resetRequested();
	}
	QGraphicsView::mouseDoubleClickEvent(event);
}

void QHistogramView::mousePressEvent(QMouseEvent* event) {
	d->startX = d->endX = event->pos().x();
	d->prev_range_rect = d->rangeItem->rect();
	d->prev_min = d->range_min;
	d->prev_max = d->range_max;
	if (event->button() & Qt::LeftButton) {
		if (d->mouse_type._to_index() == MouseType::Idle) {
			d->addRange = true;
			d->dragItem->setRect(0, 0, 0, 0);
			d->dragItem->update();
			d->dragItem->show();
		} else if (d->mouse_type._to_index() == MouseType::LeftHandle) {
			d->modifyMin = true;
		} else if (d->mouse_type._to_index() == MouseType::RightHandle) {
			d->modifyMax = true;
		} else {
			d->moveRange = true;
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void QHistogramView::mouseReleaseEvent(QMouseEvent* event) {
	if (d->addRange) {
		d->dragItem->hide();
		d->endX = event->pos().x();
		auto startingX = (d->startX > d->endX) ? d->endX : d->startX;
		auto widthX = abs(d->endX - d->startX);
		d->rangeItem->setRect(startingX, 0, widthX, d->cur_scene_height);
		d->rangeItem->update();

		d->EmitRangeChange();
		this->RequestDraw();
	} else if (d->modifyMax || d->modifyMin || d->moveRange) {
		if (d->cutoff && (d->range_min > d->xmin || d->range_max < d->xmax)) {
			d->UpdateYMax();
		}
		this->RequestDraw();
	}
	d->addRange = d->moveRange = d->modifyMax = d->modifyMin = false;
	QGraphicsView::mouseReleaseEvent(event);
}

void QHistogramView::mouseMoveEvent(QMouseEvent* event) {
	d->endX = event->pos().x();
	auto diff = d->endX - d->startX;
	if (d->addRange) {
		auto startingX = (d->startX > d->endX) ? d->endX : d->startX;
		d->dragItem->setRect(startingX, 0, abs(d->endX - d->startX), d->cur_scene_height);
		d->dragItem->update();
	} else if (d->moveRange) {
		if (d->prev_range_rect.x() + diff >= 0 &&
			d->prev_range_rect.x() + d->prev_range_rect.width() + diff <= d->cur_scene_width) {
			d->rangeItem->setRect(d->prev_range_rect.x() + diff, 0, d->prev_range_rect.width(), d->cur_scene_height);
			d->rangeItem->update();
			d->EmitRangeChange();
		}
	} else if (d->modifyMax) {
		if (d->prev_range_rect.width() + diff > 0 &&
			d->prev_range_rect.x() + d->prev_range_rect.width() + diff <= d->cur_scene_width) {
			d->rangeItem->setRect(d->prev_range_rect.x(), 0, d->prev_range_rect.width() + diff, d->cur_scene_height);
			d->rangeItem->update();
			d->EmitRangeChange();
		}
	} else if (d->modifyMin) {
		if (d->prev_range_rect.width() - diff > 0 &&
			d->prev_range_rect.x() + diff >= 0) {
			d->rangeItem->setRect(d->prev_range_rect.x() + diff, 0, d->prev_range_rect.width() - diff, d->cur_scene_height);
			d->rangeItem->update();
			d->EmitRangeChange();
		}
	} else {
		d->CheckMousePos(event->pos());
	}

	QGraphicsView::mouseMoveEvent(event);
}

void QHistogramView::resizeEvent(QResizeEvent* event) {
	auto new_height = event->size().height();
	auto new_width = event->size().width();

	d->cur_scene_height = new_height;
	d->cur_scene_width = new_width;

	this->RequestDraw();

	QGraphicsView::resizeEvent(event);
}
