#include <QApplication>
#include <QSettings>
#include <QFileDialog>

#include <QFLHistogram.h>
#include <QHistogramView.h>
#include <TCHistogram1d.h>

#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>

#include <H5Cpp.h>
#include <iostream>

double log_base = 2;
int cutoff = 0;
int spacing = 1;
auto isLog = false;

auto ReadHTData(H5::Group htGroup)->QList<int> {
    auto result = QList<int>();
    auto dset = htGroup.openDataSet("000000");
    auto dataspace = dset.getSpace();
    hsize_t dims[2];
    dataspace.getSimpleExtentDims(dims);
    unsigned long long numberOfElements;

    const auto dataSizeX = dims[1];
    const auto dataSizeY = dims[0];
    numberOfElements = dataSizeX * dataSizeY;

    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    dset.read(rawData.get(), dset.getDataType());
    dataspace.close();
    dset.close();

    for(auto i=0;i<numberOfElements;i++) {
        result.append(rawData.get()[i]);
    }
    return result;
}

auto ReadOriginalData(int ch,H5::Group flGroup)->QList<int> {
    auto result = QList<int>();
    auto chName = QString("CH%1").arg(ch).toStdString();
    auto chGroup = flGroup.openGroup(chName);
    auto chDSet = chGroup.openDataSet("000000");
    auto datspace = chDSet.getSpace();
    hsize_t dims[3];
    datspace.getSimpleExtentDims(dims);
    unsigned long long numberOfElements;

    const auto dataSizeX = dims[1];
    const auto dataSizeY = dims[0];
    numberOfElements = dataSizeX * dataSizeY;
    
    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    chDSet.read(rawData.get(), chDSet.getDataType());
    datspace.close();
    chDSet.close();
    chGroup.close();

    for (auto i = 0; i < numberOfElements; i++) {
        result.append(rawData.get()[i]);
    }
    return result;
}

auto CalculateChHistogram(int steps,QList<int> original_data,int min = 0)->QList<int> {
    auto result = QList<int>();
    for (auto i = 0; i < steps; i++) {
        if (steps * spacing < cutoff)
            continue;
        result.append(0);
    }    
    
    for (auto i = 0; i < original_data.count(); i++) {
        auto value = original_data[i] - min;
        if (value < cutoff)
            continue;
        int step = 0;
        if (isLog) {
            step = static_cast<int>(log((value -cutoff) / spacing) / log(log_base));
        }
        else {
            step = static_cast<int>((value-cutoff) / spacing);
        }
        result[step]++;
    }

    return result;
}

int main(int argc,char** argv) {
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QApplication app(argc, argv);

    QSettings qs("Test/TCFLhistogramTest");

    auto prevPath = qs.value("prevPath").toString();
    if(prevPath.isEmpty()) {
        prevPath = qApp->applicationDirPath();
    }

    QString file_name = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

    if(file_name.isEmpty()) {
        return -1;
    }

    qs.setValue("prevPath", file_name);

    SoDB::init();
    SoVolumeRendering::init();
        
    using namespace H5;
    auto path = file_name;
    H5File file(path.toStdString(), H5F_ACC_RDONLY);
    auto dataGroup = file.openGroup("Data");

#if 1    
	Group flGroup;
    if (false == dataGroup.exists("2DFLMIP")) {
        dataGroup.close();
        file.close();
        return 0;
    }
    flGroup = dataGroup.openGroup("2DFLMIP");
	auto flExist = false;
    bool chExist[3] = { false, };
    int chIdx = 0;
    QString chName;
	for (auto i = 0; i < 3; i++) {
		auto tempName = QString("CH%1").arg(i);
		chExist[i] = flGroup.exists(tempName.toStdString());
        if(chExist[i]) {
            flExist = true;
            chIdx = i;
            chName = QString("CH%1").arg(i);
            break;
        }
	}
	if (false == flExist) {
		flGroup.close();
		dataGroup.close();
		file.close();
		return 0;
	}

    auto chGroup = flGroup.openGroup(chName.toStdString());
    auto dataSet = chGroup.openDataSet("000000");

    int64_t maxSignal;
    int64_t minSignal;
	auto maxAttrb = dataSet.openAttribute("MaxIntensity");
	maxAttrb.read(PredType::NATIVE_INT64, &maxSignal);
	maxAttrb.close();
	auto minAttrb = dataSet.openAttribute("MinIntensity");
	minAttrb.read(PredType::NATIVE_INT64, &minSignal);
	minAttrb.close();

    dataSet.close();
    chGroup.close();

    auto data = ReadOriginalData(chIdx, flGroup);
    auto histogram_step = static_cast<int>(ceil(static_cast<float>(maxSignal - minSignal) / static_cast<float>(spacing)));
    if (isLog) {
        histogram_step = log(histogram_step) / log(log_base);
    }
    auto histogram = CalculateChHistogram(histogram_step,data);
    
	flGroup.close();	
#else
    Group htGroup;
    if (false == dataGroup.exists("2DMIP")) {
        dataGroup.close();
        file.close();
        return 0;
    }
    htGroup = dataGroup.openGroup("2DMIP");
    auto dataSet = htGroup.openDataSet("000000");
    double maxTmp;
    double minTmp;
    auto maxAttrb = dataSet.openAttribute("RIMax");
    maxAttrb.read(PredType::NATIVE_DOUBLE, &maxTmp);
    maxAttrb.close();
    auto minAttrb = dataSet.openAttribute("RIMin");
    minAttrb.read(PredType::NATIVE_DOUBLE, &minTmp);
    minAttrb.close();

    dataSet.close();

    auto maxSignal = static_cast<int>(maxTmp * 10000.0);
    auto minSignal = static_cast<int>(minTmp * 10000.0);

    auto data = ReadHTData(htGroup);
    auto histogram_step = static_cast<int>(ceil(static_cast<float>(maxSignal - minSignal)/ static_cast<float>(spacing)));
    auto histogram = CalculateChHistogram(histogram_step, data,static_cast<int>(minSignal));

    htGroup.close();
    
#endif
    dataGroup.close();
    file.close();
    auto hist1d = new TC::Histogram1d(nullptr);    
    auto tf = new SoTransferFunction;
    tf->ref();
    tf->predefColorMap.setValue(SoTransferFunction::PredefColorMap::INTENSITY);
    tf->touch();
    auto p = tf->actualColorMap.getValues(0);
    hist1d->SetColorMap(p, tf->actualColorMap.getNum()/4);    
    tf->unref();
    hist1d->SetDecimals(3);
    hist1d->SetSingleStep(0.001);
    //hist1d->SetDataRange(minSignal, maxSignal, 10000);
    hist1d->SetDataRange(minSignal, maxSignal, 1);
    hist1d->SetHistogram(QColor(0, 255, 0, 200), histogram);    
    hist1d->show();    
    
    app.exec();

    delete hist1d;

    SoVolumeRendering::finish();
    SoDB::finish();

    return 0;
}