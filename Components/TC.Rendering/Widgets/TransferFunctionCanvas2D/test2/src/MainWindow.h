#pragma once
#include <memory>
#include <QWidget>

class MainWindow : public QWidget {
	Q_OBJECT
public:
	MainWindow(QWidget* parent = nullptr);
	~MainWindow() override;
		
protected slots:
	void OnOpenTCF();
	void OnUpdatePolygon();
	void OnCanvasChanged();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};