#include <QVBoxLayout>

#include <QCanvasViewLasso.h>
#include <QCanvasViewRect.h>
#include <QCanvasViewPolygon.h>
#include <HiddenSceneGeneral.h>
#include <HiddenScene.h>

#include <QOiv3DRenderWindow.h>
#include <OivHdf5Reader.h>
#include <TCFMetaReader.h>
#include <VolumeTF2D.h>
#include <VolumeTF1D.h>

#include "MainWindow.h"

#include <QFileDialog>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include "ui_MainWindow.h"

struct MainWindow::Impl {
	Ui::Form ui;
	MainWindow* thisPointer{nullptr};
	std::shared_ptr<QCanvasViewRect> canvas_rect{ nullptr };
	std::shared_ptr<QCanvasViewLasso> canvas_lasso{ nullptr };
	std::shared_ptr<QCanvasViewPolygon> canvas_polygon{ nullptr };
	std::shared_ptr<HiddenSceneGeneral> hiddenScene{ nullptr };	
	std::shared_ptr<Tomocube::Rendering::Image::VolumeTF2D> renderer{ nullptr };
	std::shared_ptr<QOiv3DRenderWindow> window{ nullptr };
	TC::IO::TCFMetaReader::Meta::Pointer meta{ nullptr };

	auto InitUI()->void;
	auto InitConnection()->void;	
};

auto MainWindow::Impl::InitUI() -> void {
	//add canvas
	canvas_rect = std::make_shared<QCanvasViewRect>(thisPointer);
	canvas_lasso = std::make_shared<QCanvasViewLasso>(thisPointer);
	canvas_polygon = std::make_shared<QCanvasViewPolygon>(thisPointer);
	auto* layout1 = new QVBoxLayout;
	layout1->setContentsMargins(0, 0, 0, 0);
	layout1->setSpacing(0);

	ui.rectSocket->setLayout(layout1);	
	layout1->addWidget(canvas_rect.get());

	auto* layout2 = new QVBoxLayout;
	layout2->setContentsMargins(0, 0, 0, 0);
	layout2->setSpacing(0);

	ui.polySocket->setLayout(layout2);
	layout2->addWidget(canvas_polygon.get());

	auto* layout3 = new QVBoxLayout;
	layout3->setContentsMargins(0, 0, 0, 0);
	layout3->setSpacing(0);

	ui.lassoSocket->setLayout(layout3);
	layout3->addWidget(canvas_lasso.get());

	hiddenScene = std::make_shared<HiddenSceneGeneral>();	
	hiddenScene->SetImageParentDir(qApp->applicationDirPath());

	renderer = std::make_shared<Tomocube::Rendering::Image::VolumeTF2D>("Test");	
	renderer->SetHiddenSep(hiddenScene->GetSceneGraph());
	renderer->BuildSceneGraph();
	
	window = std::make_shared<QOiv3DRenderWindow>(nullptr);
	window->ChangeBackGround(1);
	auto* wlayout = new QVBoxLayout;
	wlayout->setContentsMargins(0, 0, 0, 0);
	wlayout->setSpacing(0);
	
	ui.windowSocket->setLayout(wlayout);
	wlayout->addWidget(window.get());

	window->setSceneGraph(renderer->GetRootSwitch());
}

auto MainWindow::Impl::InitConnection() -> void {
	connect(ui.pushButton, SIGNAL(clicked()), thisPointer, SLOT(OnOpenTCF()));
	connect(canvas_rect.get(), SIGNAL(sigUpdatePolygons()), thisPointer, SLOT(OnUpdatePolygon()));
	connect(canvas_lasso.get(), SIGNAL(sigUpdatePolygons()), thisPointer, SLOT(OnUpdatePolygon()));
	connect(canvas_polygon.get(), SIGNAL(sigUpdatePolygons()), thisPointer, SLOT(OnUpdatePolygon()));

	connect(ui.rectRadio, SIGNAL(clicked()), thisPointer, SLOT(OnCanvasChanged()));
	connect(ui.lassoRadio, SIGNAL(clicked()), thisPointer, SLOT(OnCanvasChanged()));
	connect(ui.polygonRadio, SIGNAL(clicked()), thisPointer, SLOT(OnCanvasChanged()));
	
	ui.rectRadio->setChecked(true);
}

MainWindow::MainWindow(QWidget* parent) : QWidget(parent), d{ new Impl } {
	d->thisPointer = this;
	d->ui.setupUi(this);
	d->InitUI();
	d->InitConnection();
}

MainWindow::~MainWindow() {
	
}

void MainWindow::OnOpenTCF() {
	QString file_name = QFileDialog::getOpenFileName(nullptr, "Select TCF", "", "TCF (*.tcf)");

	if(file_name.isEmpty()) {
		return;
	}

	TC::IO::TCFMetaReader metaReader;
	d->meta = metaReader.Read(file_name);

	if(d->meta->data.isLDM) {
		return;
	}

	if(false == d->meta->data.data3D.exist) {
		return;
	}

	d->renderer->SetDataMinMax(d->meta->data.data3D.riMin * 10000.0, d->meta->data.data3D.riMax * 10000.0);

	SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
	reader->setDataGroupPath("/Data/3D");
	reader->setTileName("000000");
	reader->setFilename(file_name.toStdString());

	SoRef<SoVolumeData> volume = new SoVolumeData;
	volume->setReader(*reader);
	d->hiddenScene->SetDataMinMax(d->meta->data.data3D.riMin * 10000.0, d->meta->data.data3D.riMax * 10000.0);
	d->hiddenScene->SetFilePath(file_name);
	d->hiddenScene->Calc2DHistogram(volume.ptr(), false);
	QDir dir(qApp->applicationDirPath());
	dir.cdUp();	
	d->canvas_rect->setStyleSheet(QString("border-image:url(\"%1/Background.tif\") 0 0 0 0 stretch stretch;").arg(dir.path()));
	d->canvas_lasso->setStyleSheet(QString("border-image:url(\"%1/Background.tif\") 0 0 0 0 stretch stretch;").arg(dir.path()));
	d->canvas_polygon->setStyleSheet(QString("border-image:url(\"%1/Background.tif\") 0 0 0 0 stretch stretch;").arg(dir.path()));
	d->renderer->SetVolume(volume.ptr());

	d->window->viewAll();
}

void MainWindow::OnCanvasChanged() {		
	if (d->ui.rectRadio->isChecked()) {
		d->ui.stackedWidget->setCurrentIndex(0);
	}else if(d->ui.lassoRadio->isChecked()) {
		d->ui.stackedWidget->setCurrentIndex(2);		
	}else {
		d->ui.stackedWidget->setCurrentIndex(1);
	}
	OnUpdatePolygon();
}

void MainWindow::OnUpdatePolygon() {
	QList<QPolygonF> polygons;
	QList<QColor> colors;
	QList<bool> isHidden;	
	if (d->ui.rectRadio->isChecked()) {
		polygons = d->canvas_rect->GetPolygons();
		colors = d->canvas_rect->GetColors();
		isHidden = d->canvas_rect->GetHidden();
	}else if(d->ui.lassoRadio->isChecked()) {
		polygons = d->canvas_lasso->GetPolygons();
		colors = d->canvas_lasso->GetColors();
		isHidden = d->canvas_lasso->GetHidden();
	}else if(d->ui.polygonRadio->isChecked()) {
		polygons = d->canvas_polygon->GetPolygons();
		colors = d->canvas_polygon->GetColors();
		isHidden = d->canvas_polygon->GetHidden();
	}
	
	d->hiddenScene->UpdateTF(polygons,colors,isHidden);
}