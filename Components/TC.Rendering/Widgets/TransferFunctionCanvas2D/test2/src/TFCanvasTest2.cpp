#include <ImageDev/ImageDev.h>

#include <QApplication>
#include <QVBoxLayout>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <OivHdf5Reader.h>

#include <VolumeTF2D.h>
#include <Inventor/Qt/SoQt.h>

#include "MainWindow.h"
#include "SoTF2D.h"

int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QApplication app(argc, argv);
    QWidget* container = new QWidget;
    SoQt::init(container);    
    SoVolumeRendering::init();
    OivHdf5Reader::initClass();
    SoTF2D::initClass();
    imagedev::init();

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    container->setLayout(layout);
    

    MainWindow* w = new MainWindow;
    layout->addWidget(w);

    container->show();
    app.exec();

    delete container;

    imagedev::finish();
    SoTF2D::exitClass();
    OivHdf5Reader::exitClass();
    SoVolumeRendering::finish();
    SoQt::finish();
    
    return 0;
}
