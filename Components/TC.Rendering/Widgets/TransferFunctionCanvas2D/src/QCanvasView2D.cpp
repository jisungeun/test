#include <vector>
#include <iostream>

#include "QCanvasView2D.h"
#include "QCanvasScene2D.h"

#include <QResizeEvent>
#include <QColorDialog>
#include <QGraphicsWidget>

struct QCanvasView2D::Impl {
    std::vector<QRectItem2D*> rectList;
    std::vector<double> dataX_minVal;//actual current data values
    std::vector<double> dataX_maxVal;
    std::vector<std::string> rectNameList;
    std::vector<double> rectOpacity;

    std::vector<double> dataY_minVal;//actual current gradient values
    std::vector<double> dataY_maxVal;

    std::vector<bool> box_hidden;

    int cur_scene_height;
    int cur_scene_width;
    int cur_RectIdx;
    int rectCnt;

    QRectItem2D* curRectItem;

    double dataX_Min;
    double dataX_Max;

    double dataY_Min;
    double dataY_Max;

    double singleStep_X = 0.0;
    double singleStep_Y = 0.0;
};

QCanvasView2D::QCanvasView2D(QWidget* parent):
d(new Impl()){
    Q_UNUSED(parent)
    setDragMode(DragMode::RubberBandDrag);    

    d->dataX_Min = 13000;
    d->dataX_Max = 14000;
    
    setMouseTracking(true);
}

QCanvasView2D::~QCanvasView2D() {
    
}

auto QCanvasView2D::updateCurItemVal(double x_minVal, double x_maxVal,double y_minVal,double y_maxVal, double opacity)->void {
    if(d->cur_RectIdx < 0) {
        return;
    }    
    d->dataX_maxVal[d->cur_RectIdx] = x_maxVal;
    d->dataX_minVal[d->cur_RectIdx] = x_minVal;
    d->dataY_maxVal[d->cur_RectIdx] = y_maxVal;
    d->dataY_minVal[d->cur_RectIdx] = y_minVal;
    d->rectOpacity[d->cur_RectIdx] = opacity;
    redrawBoxItem(d->rectList[d->cur_RectIdx]);
    scene()->update();
    emit sigUpdateBoxs();
}

auto QCanvasView2D::accrueCurItemVal(double x_minVal, double x_maxVal, double y_minVal, double y_maxVal)->void {
    if(d->cur_RectIdx<0) {
        return;
    }
    d->dataX_maxVal[d->cur_RectIdx] += x_maxVal;
    d->dataX_minVal[d->cur_RectIdx] += x_minVal;
    d->dataY_maxVal[d->cur_RectIdx] += y_maxVal;
    d->dataY_minVal[d->cur_RectIdx] += y_minVal;

    redrawBoxItem(d->rectList[d->cur_RectIdx]);
    scene()->update();
    emit sigUpdateBoxs();
    emit sigUpdatePannel();
}

auto QCanvasView2D::setXSingleStep(double step)->void {
    d->singleStep_X = step;
}

auto QCanvasView2D::setYSingleStep(double step)->void {
    d->singleStep_Y = step;
}

auto QCanvasView2D::getColors() -> std::vector<QColor> {
    std::vector<QColor> cur_clr;

    for(int i=0;i<d->rectList.size();i++) {
        QColor clr = d->rectList[i]->getColor();
        cur_clr.push_back(clr);
    }
    return cur_clr;
}

auto QCanvasView2D::getCurItemIdx() -> int {
    for(int i=0;i<d->rectList.size();i++) {
        if (d->rectList[i] == d->curRectItem)
            return i;
    }
    return -1;
}

auto QCanvasView2D::setCurItemIdx(int index)->void {
    d->cur_RectIdx = index;

    foreach(QGraphicsItem * item, scene()->items())
    {
        item->setSelected(false);
    }//Deactivate all

    if(-1 !=  d->cur_RectIdx) {
        d->curRectItem = d->rectList[index];

        d->curRectItem->setSelected(true);
        scene()->setFocusItem(d->curRectItem);
    }
    else {
        d->curRectItem = nullptr;
    }

    emit sigUpdateBoxs();
    emit sigUpdatePannel();
}

auto QCanvasView2D::getYMaxs() -> std::vector<double> {
    return d->dataY_maxVal;
}

auto QCanvasView2D::getYMins() -> std::vector<double> {
    return d->dataY_minVal;
}


auto QCanvasView2D::getMaxs() -> std::vector<double> {
    return d->dataX_maxVal;
}

auto QCanvasView2D::getMins() -> std::vector<double> {
    return d->dataX_minVal;
}

auto QCanvasView2D::getTrans() -> std::vector<double> {
    return d->rectOpacity;
}

auto QCanvasView2D::getHidden() -> std::vector<bool> {
    return d->box_hidden;
}


auto QCanvasView2D::deleteBoxItem(int idx)->void {
    for(int i=idx+1;i<d->rectList.size();i++) {
        d->rectList[i - 1] = d->rectList[i];
        d->dataX_maxVal[i - 1] = d->dataX_maxVal[i];
        d->dataX_minVal[i - 1] = d->dataX_minVal[i];
        d->rectOpacity[i - 1] = d->rectOpacity[i];
        d->box_hidden[i - 1] = d->box_hidden[i];
    }
    d->rectList.pop_back();
    d->dataX_maxVal.pop_back();
    d->dataX_minVal.pop_back();
    d->rectOpacity.pop_back();
    d->box_hidden.pop_back();

    setCurrentItem(0);//no box is selected after delete

    //redraw scene
    updateBoxItems();

    //QCanvasScene2D* prev_scene = (QCanvasScene2D*)scene();
    //disconnect(prev_scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));

    QCanvasScene2D* scene = new QCanvasScene2D;
    scene->setSceneRect(QRectF(0, -d->cur_scene_height, d->cur_scene_width, d->cur_scene_height));
    setScene(scene);
    //connect(scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));
    //scene 내부에서 signal 주고 받은 내용이 있다면 resize시마다 disconnect, connect 해준다                
    redrawBoxItems(d->cur_scene_height, d->cur_scene_width);
}

void QCanvasView2D::drawForeground(QPainter* painter, const QRectF& rect) {
    QGraphicsView::drawForeground(painter, rect);
}

void QCanvasView2D::keyPressEvent(QKeyEvent* event) {
    QKeyEvent* key = static_cast<QKeyEvent*>(event);
    Qt::KeyboardModifiers modifiers = key->modifiers();

    if (d->cur_RectIdx == -1) {
        return;
    }

    if (key->key() == Qt::Key_Delete) {
        deleteBoxItem(d->cur_RectIdx);
    }

    if (key->key() == Qt::Key_Up) {
        if ((modifiers & Qt::ShiftModifier) == Qt::ShiftModifier)
        {
            // grad(y) min +
            accrueCurItemVal(0, 0, d->singleStep_Y, 0);
        }

        if ((modifiers & Qt::ControlModifier) == Qt::ControlModifier)
        {
            // grad(y) max +
            accrueCurItemVal(0, 0, 0, d->singleStep_Y);
        }
    }

    if (key->key() == Qt::Key_Down) {
        if ((modifiers & Qt::ShiftModifier) == Qt::ShiftModifier)
        {
            // grad(y) min -
            accrueCurItemVal(0, 0, -d->singleStep_Y, 0);
        }

        if ((modifiers & Qt::ControlModifier) == Qt::ControlModifier)
        {
            // grad(y) max -
            accrueCurItemVal(0, 0, 0, -d->singleStep_Y);
        }
    }

    const double unit = 10000.0;

    if (key->key() == Qt::Key_Left) {
        if ((modifiers & Qt::ShiftModifier) == Qt::ShiftModifier)
        {
            // ri(x) min -
            accrueCurItemVal(-d->singleStep_X * unit, 0, 0, 0);
        }

        if ((modifiers & Qt::ControlModifier) == Qt::ControlModifier)
        {
            // ri(x) max -
            accrueCurItemVal(0, -d->singleStep_X * unit, 0, 0);
        }
    }

    if (key->key() == Qt::Key_Right) {
        if ((modifiers & Qt::ShiftModifier) == Qt::ShiftModifier)
        {
            // ri(x) min +
            accrueCurItemVal(d->singleStep_X * unit, 0, 0, 0);
        }

        if ((modifiers & Qt::ControlModifier) == Qt::ControlModifier)
        {
            // ri(x) max +
            accrueCurItemVal(0, d->singleStep_X * unit, 0, 0);
        }
    }


    QGraphicsView::keyPressEvent(event);
}

void QCanvasView2D::keyReleaseEvent(QKeyEvent* event) {
    QGraphicsView::keyReleaseEvent(event);
}

void QCanvasView2D::mouseDoubleClickEvent(QMouseEvent* event) {    
    if(d->curRectItem) {
        QColor new_col = QColorDialog::getColor(d->curRectItem->getColor(), nullptr, "Select Color");
        if(false == new_col.isValid()) {
            return;
        }

        new_col.setAlphaF(d->rectOpacity[d->cur_RectIdx]);
        d->curRectItem->setBrush(new_col);
        d->curRectItem->setPen(new_col);
        updateBoxItems();
    }
    QGraphicsView::mouseDoubleClickEvent(event);
}

void QCanvasView2D::mouseMoveEvent(QMouseEvent* event) {
    QPointF pos = mapToScene(event->pos());    
    ;
    setMouseCursor(pos);

    const double curX = toDataX(pos.x()) / 10000.; 
    const double curY = pos.y();              
    if (event->buttons() & Qt::LeftButton)
    {
        QGraphicsItem* item = scene()->focusItem();        
        if (item)
        {            
            //showInfo();
            updateBoxItems();
            //TODO: For real-time changing add Render Window Update Call HERE
        }
    }

    scene()->invalidate(scene()->sceneRect());
        
    QGraphicsView::mouseMoveEvent(event);
}
auto QCanvasView2D::getCurrentItem() -> QRectItem2D* {
    return d->curRectItem;
}

auto QCanvasView2D::setDataXRange(double x_min, double x_max) -> void {
    d->dataX_Max = x_max;
    d->dataX_Min = x_min;
}

auto QCanvasView2D::setDataYRange(double y_min, double y_max) -> void {
    d->dataY_Min = y_min;
    d->dataY_Max = y_max;
}

auto QCanvasView2D::toggleCurrentItem(QRectItem2D* item) -> void {    
    if(nullptr != item) {
        for(int i=0;i<d->rectList.size();i++) {
            if(d->rectList[i] == item) {
                item->toggleHidden();
                d->box_hidden[i] = item->getHidden();
            }
        }
    }    
    emit sigUpdateBoxs();
}

auto QCanvasView2D::setCurrentItem(QRectItem2D* item) -> void {
    d->curRectItem = item;
    d->cur_RectIdx = -1;

    if(nullptr != item) {
        for (int i = 0; i < d->rectList.size(); i++) {
            if (d->rectList[i] == d->curRectItem)
                d->cur_RectIdx = i;
        }
    }
}

auto QCanvasView2D::updateBoxItems() -> void {//update box items due to changing    
    for (int i = 0; i < d->rectList.size(); i++) {
        QRectItem2D* pRectItem = d->rectList[i];        
        QRectF rect = pRectItem->boundingRect();

        if(rect.width() == 0 || rect.height() == 0) {
            continue;
        }

        d->dataX_maxVal[i] = toDataX(rect.right());
        d->dataX_minVal[i] = toDataX(rect.left());

        //rect.setTop(-d->cur_scene_height);
        //rect.setBottom(0);//
        d->dataY_maxVal[i] = toDataY(rect.top());
        d->dataY_minVal[i] = toDataY(rect.bottom());        

        d->rectList[i]->setRect(rect);
        d->rectList[i]->update();        
    }

    //if(false == d->rectList.empty()) {
        emit sigUpdateBoxs();    
        emit sigUpdatePannel();
    //}
}


void QCanvasView2D::mousePressEvent(QMouseEvent* event) {
    emit sigMousePress();
    setCurrentItem(0);    
    foreach(QGraphicsItem * item, scene()->items())
    {
        item->setSelected(false);
    }//Deactivate all
    
    QGraphicsView::mousePressEvent(event);    

    if (event->button() == Qt::LeftButton) {
        foreach(QGraphicsItem * item, scene()->items())
        {
            if (item->isSelected()) {
                setCurrentItem(dynamic_cast<QRectItem2D*>(item));
                break;
            }
        }//Find new selection
    }else if(event->button() == Qt::RightButton) {
        QPointF pos = mapToScene(event->pos());
        foreach(QGraphicsItem* item, scene()->items()) {
            if(item->contains(pos)){
                toggleCurrentItem(dynamic_cast<QRectItem2D*>(item));
                break;
            }
        }
    }
}

void QCanvasView2D::mouseReleaseEvent(QMouseEvent* event) {
    //if (event->button() == Qt::LeftButton || event->button() == Qt::RightButton)
    emit sigMouseRelease();
    if (event->button() == Qt::LeftButton )
    {//after drawing finished        
        if (rubberBandRect().isValid() && !scene()->selectedItems().count())
        {

            QRectF band = mapToScene(rubberBandRect()).boundingRect();            
            QRectF rect = scene()->sceneRect().intersected(band);

            makeRectItem(rect);            
            //saveCurrentInfo();
        }
        else {
            QGraphicsItem* item = getCurrentItem();
            if (item)
            {                
                for (int i = 0; i < d->rectList.size(); i++) {
                    if (d->rectList[i]->boundingRect() == item->boundingRect()) {
                        d->cur_RectIdx = i;
                    }
                }
                QRectItem2D* cur_item = dynamic_cast<QRectItem2D*>(item);
                QRectF intersect_rect = scene()->sceneRect().intersected(cur_item->boundingRect());
                cur_item->setRect(intersect_rect);

                //emit signalOpacityChange();
                //emit signalSoftChange();               

                updateBoxItems();

                //QCanvasScene2D* prev_scene = (QCanvasScene2D*)scene();
                //disconnect(prev_scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));

                QCanvasScene2D* scene = new QCanvasScene2D;
                scene->setSceneRect(QRectF(0, -d->cur_scene_height, d->cur_scene_width, d->cur_scene_height));
                setScene(scene);
                //connect(scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));
                //scene 내부에서 signal 주고 받은 내용이 있다면 resize시마다 disconnect, connect 해준다                
                redrawBoxItems(d->cur_scene_height, d->cur_scene_width);                

                //showInfo();                
                //saveCurrentInfo();
            }
            else
            {
                setCurrentItem(nullptr);
                emit sigUpdatePannel();
                //saveCurrentInfo();
            }
        }
    }
    QGraphicsView::mouseReleaseEvent(event);
}

void QCanvasView2D::wheelEvent(QWheelEvent* event) {
    QGraphicsView::wheelEvent(event);
}
 
void QCanvasView2D::resizeEvent(QResizeEvent* event) {    
    //int new_height = (event->size().height() - 248) / 2 + 200;
    //int new_width = event->size().width() - 264 + 250;
    //유격을 둘것인가
    int new_height = event->size().height();
    int new_width = event->size().width();

    //QCanvasScene2D* prev_scene = (QCanvasScene2D*)scene();
    //disconnect(prev_scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));

    QCanvasScene2D* scene = new QCanvasScene2D;
    scene->setSceneRect(QRectF(0, -new_height, new_width, new_height));    
    setScene(scene);
    //connect(scene, SIGNAL(sigUpdateVisibility()), this, SLOT(onUpdateVisibility()));
    //scene 내부에서 signal 주고 받은 내용이 있다면 resize시마다 disconnect, connect 해준다
    
    redrawBoxItems(new_height,new_width);

    d->cur_scene_height = new_height;
    d->cur_scene_width = new_width;

    //updateBoxItems();

    QGraphicsView::resizeEvent(event);
}

auto QCanvasView2D::redrawBoxItem(QRectItem2D* item) -> void {        
    QRectF prevRect = item->boundingRect();
    QRectF newRect;
    int idx = -1;
    for(int i=0;i<d->rectList.size();i++) {
        if (d->rectList[i] == item)
            idx = i;
    }
    newRect.setLeft(toSceneX(d->dataX_minVal[idx]));
    newRect.setRight(toSceneX(d->dataX_maxVal[idx]));

    //newRect.setTop(-d->cur_scene_height);//1D에서는 무조건 max다
    //newRect.setBottom(0);
    newRect.setTop(toSceneY(d->dataY_maxVal[idx]));
    newRect.setBottom(toSceneY(d->dataY_minVal[idx]));

    QColor new_col = item->getColor();
    new_col.setAlphaF(d->rectOpacity[idx]);

    item->setPen(new_col);
    item->setBrush(new_col);
    item->setRect(newRect);
    item->update();   
}


auto QCanvasView2D::redrawBoxItems(float new_h, float new_w) -> void {
    Q_UNUSED(new_w)
    for (int i = 0; i < d->rectList.size(); i++) {
        QRectF prevRect = d->rectList[i]->boundingRect();
        
        QRectF newRect;
        newRect.setLeft(toSceneX(d->dataX_minVal[i]));
        newRect.setRight(toSceneX(d->dataX_maxVal[i]));        

        newRect.setY(-new_h / 2);
        //newRect.setTop(-new_h);
        //newRect.setBottom(0);
        //2D에서는 실제 데이터 값을 따라간다

        newRect.setBottom(toSceneY(d->dataY_minVal[i]));
        newRect.setTop(toSceneY(d->dataY_maxVal[i]));

        d->rectList[i]->setRect(newRect);
        d->rectList[i]->update();

        scene()->addItem(d->rectList[i]);        
    }
}

auto QCanvasView2D::toDataY(double SceneY) -> double {    
    double gradRange = d->dataY_Max - d->dataY_Min;
    double sceneHeight = scene()->sceneRect().height();
    double scale = gradRange / sceneHeight;
    double result = d->dataY_Min + (-SceneY * scale);

    if (result > d->dataY_Max) result = d->dataY_Max;
    if (result < d->dataY_Min) result = d->dataY_Min;

    return result;
}

auto QCanvasView2D::toSceneY(double dataY) -> double {
    double gradRange = d->dataY_Max - d->dataY_Min;
    double sceneHeight = scene()->sceneRect().height();
    double scale = sceneHeight / gradRange;
    return -(dataY - d->dataY_Min) * scale;
}

auto QCanvasView2D::toDataX(double sceneX)->double
{
    if (sceneX < sceneRect().left()) sceneX = sceneRect().left();
    if (sceneX > sceneRect().right()) sceneX = sceneRect().right();
        
    double dataRange = d->dataX_Max - d->dataX_Min;
    double sceneWidth = scene()->sceneRect().width();
    double scale = dataRange / sceneWidth;    

    return d->dataX_Min + (sceneX * scale);
}
auto QCanvasView2D::toSceneX(double dataX)->double
{
    double dataRange = d->dataX_Max - d->dataX_Min;
    double sceneWidth = scene()->sceneRect().width();
    double scale = sceneWidth / dataRange;
    return (dataX - d->dataX_Min) * scale;
}

auto QCanvasView2D::makeRectItem(QRectF rect,bool bData) -> void {
    double x1 = rect.left();
    double x2 = rect.right();
    double y1 = rect.bottom();
    double y2 = rect.top();

    //y1 = -d->cur_scene_height;
    //y2 = 0;

    if (!bData)
    {
        x1 = toDataX(x1);
        x2 = toDataX(x2);
        y1 = toDataY(y1);
        y2 = toDataY(y2);
    }

    QColor color = QColorDialog::getColor(Qt::red, nullptr, "Select Color");
    if(false == color.isValid()) {
        return;
    }

    double cr = color.redF();
    double cg = color.greenF();
    double cb = color.blueF();
    double opacity = 0.5;    
    int v = 1;
    std::string name = "default";
    
    addRectItem(x1, x2, y1, y2, cr, cg, cb, opacity, name, v);
}

auto QCanvasView2D::checkRectValidity(double x1, double x2) -> bool {
    if (x1 < d->dataX_Min || x2 > d->dataX_Max)
        return false;
    else
        return true;
}

auto QCanvasView2D::checkRectValidityY(double y1, double y2) -> bool {
    if (y1 < d->dataY_Min || y2 > d->dataY_Max)
        return false;
    else
        return true;
}


auto QCanvasView2D::addRectItem(double x1, double x2, double y1, double y2, double r, double g, double b, double opacity, std::string name, int v) -> void {
    std::string nname;
    if (name.length() == 0)
        nname = "default";
    else
        nname = name;

    QRectF rect;
    rect.setLeft(toSceneX(x1));
    rect.setRight(toSceneX(x2));    
    rect.setBottom(toSceneY(y1));
    rect.setTop(toSceneY(y2));

    d->dataX_minVal.push_back(x1);
    d->dataX_maxVal.push_back(x2);
    d->dataY_minVal.push_back(y1);
    d->dataY_maxVal.push_back(y2);

    d->rectNameList.push_back(nname);

    d->box_hidden.push_back(false);

    QRectItem2D* item = new QRectItem2D();

    item->setToolTip(QString(name.data()).toUtf8());

    double cr = r * 255.0 + 0.5;
    double cg = g * 255.0 + 0.5;
    double cb = b * 255.0 + 0.5;

    item->setRect(rect);

    QColor color(cr, cg, cb, opacity * 255.0);

    QPen pen(color, 0);
    pen.setStyle(v ? Qt::SolidLine : Qt::DotLine);
    item->setPen(pen);
    
    item->setBrush(color);    
       
    d->rectOpacity.push_back(opacity);
    d->rectList.push_back(item);

    d->rectCnt++;
    d->cur_RectIdx = d->rectCnt - 1;

    setCurrentItem(item);

    // 전체 선택 해제
    foreach(QGraphicsItem * items, scene()->selectedItems())
    {
        items->setSelected(false);
    }
    // 아이템 선택
    scene()->addItem(item);
    item->setSelected(true);
    scene()->setFocusItem(item);        

    updateBoxItems();
}

auto QCanvasView2D::setMouseCursor(QPointF pos)->void {
    QRectItem2D::InnerPosition innerPosition = QRectItem2D::InnerPosition::NONE;

    for (auto item : d->rectList) {
        innerPosition = item->getInnerPosition(pos);
        if (QRectItem2D::InnerPosition::NONE != innerPosition) {
            break;
        }
    }
    switch (innerPosition) {
    case QRectItem2D::InnerPosition::TOP_LEFT:
        setCursor(QCursor(Qt::SizeFDiagCursor));
        break;
    case QRectItem2D::InnerPosition::TOP:
        setCursor(QCursor(Qt::SizeVerCursor));
        break;
    case QRectItem2D::InnerPosition::TOP_RIGHT:
        setCursor(QCursor(Qt::SizeBDiagCursor));
        break;
    case QRectItem2D::InnerPosition::RIGHT:
        setCursor(QCursor(Qt::SizeHorCursor));
        break;
    case QRectItem2D::InnerPosition::BOTTOM_RIGHT:
        setCursor(QCursor(Qt::SizeFDiagCursor));
        break;
    case QRectItem2D::InnerPosition::BOTTOM:
        setCursor(QCursor(Qt::SizeVerCursor));
        break;
    case QRectItem2D::InnerPosition::BOTTOM_LEFT:
        setCursor(QCursor(Qt::SizeBDiagCursor));
        break;
    case QRectItem2D::InnerPosition::LEFT:
        setCursor(QCursor(Qt::SizeHorCursor));
        break;
    case QRectItem2D::InnerPosition::CENTER:
        setCursor(QCursor(Qt::SizeAllCursor));
        break;
    case QRectItem2D::InnerPosition::NONE:
        setCursor(QCursor(Qt::ArrowCursor));
        break;
    }
}

auto QCanvasView2D::addItem(double x1, double x2, double y1, double y2, double r, double g, double b, double opacity, std::string name, int v)->void {
    addRectItem(x1, x2, y1, y2, r, g, b, opacity, name, v);

    //QCanvasScene2D* prev_scene = (QCanvasScene2D*)scene();
    QCanvasScene2D* scene = new QCanvasScene2D;
    scene->setSceneRect(QRectF(0, -d->cur_scene_height, d->cur_scene_width, d->cur_scene_height));
    setScene(scene);
    redrawBoxItems(d->cur_scene_height, d->cur_scene_width);
}

auto QCanvasView2D::deleteAll()->void {
    d->rectList.clear();
    d->dataX_maxVal.clear();
    d->dataX_minVal.clear();
    d->dataY_maxVal.clear();
    d->dataY_minVal.clear();
    d->rectOpacity.clear();
    d->box_hidden.clear();

    //redraw scene
    updateBoxItems();

    QCanvasScene2D* scene = new QCanvasScene2D;
    scene->setSceneRect(QRectF(0, -d->cur_scene_height, d->cur_scene_width, d->cur_scene_height));
    setScene(scene);
    redrawBoxItems(d->cur_scene_height, d->cur_scene_width);

    //emit sigUpdateBoxs();
}