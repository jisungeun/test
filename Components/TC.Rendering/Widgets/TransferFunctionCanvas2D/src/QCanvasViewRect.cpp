#include <QResizeEvent>
#include <QGraphicsPolygonItem>
#include <QColorDialog>

#include <QCanvasViewRect.h>

#include <iostream>

#include "QCanvasScene2D.h"

struct QCanvasViewRect::Impl {
	QCanvasViewRect* thisPointer { nullptr };

	int cur_scene_height { 200 };
	int cur_scene_width { 200 };

	QList<QPolygonF> polygons;
	QList<QGraphicsPolygonItem*> polygonItems;
	QGraphicsPolygonItem* activePolygonItem { nullptr };
	QList<bool> isHidden;
	QList<QColor> colors;
	QPolygonF activePolygon;
	QGraphicsPolygonItem* movingPolygonItem { nullptr };
	QGraphicsPolygonItem* handlingPolygonItem { nullptr };
	QPointF dragOffset;
	int handleIdx { -1 };

	QPointF startPoint;

	QMap<QGraphicsPolygonItem*, QList<QPointF>> handles;
	float radius { 5 };

	bool isDrawing { false };
	bool isHandling { false };

	auto toScenePos(const QPointF& dataPos) -> QPointF;
	auto toDataPos(const QPointF& scenePos) -> QPointF;

	auto SetHighlightIndex(int idx)->void;
	auto RedrawHandles() -> void;
	auto RedrawPolygonItems(float new_h, float new_w) -> void;
	auto IsInHandle(QPointF pos) -> std::tuple<bool, QGraphicsPolygonItem*, int>;
	auto distance(const QPointF& point1, const QPointF& point2) -> double;
};

auto QCanvasViewRect::Impl::SetHighlightIndex(int idx) -> void {
	for (auto i = 0; i < polygonItems.count(); i++) {
		const auto color = colors[i];
		auto penColor = color;
		penColor.setAlphaF(1);

		polygonItems[i]->setPen(QPen(penColor, 2));
	}

	if (idx < 0) {
		return;
	}

	if (idx >= polygonItems.count()) {
		return;
	}

	const auto targetColor = colors[idx];
	auto penColor = targetColor;
	penColor.setAlphaF(1);
	polygonItems[idx]->setPen(QPen(penColor, 3, Qt::PenStyle::DotLine));

	emit thisPointer->sigCurPolygon(idx);
}

auto QCanvasViewRect::Impl::toDataPos(const QPointF& scenePos) -> QPointF {
	return QPointF(scenePos.x() / cur_scene_width, 1 - scenePos.y() / cur_scene_height);
}

auto QCanvasViewRect::Impl::toScenePos(const QPointF& dataPos) -> QPointF {
	return QPointF(dataPos.x() * cur_scene_width, (1 - dataPos.y()) * cur_scene_height);
}


auto QCanvasViewRect::Impl::RedrawHandles() -> void {
	//remove every handles
	for (const auto& item : thisPointer->scene()->items()) {
		if (const auto ellipse = qgraphicsitem_cast<QGraphicsEllipseItem*>(item)) {
			thisPointer->scene()->removeItem(ellipse);
		}
	}
	//redraw every handles		
	for (const auto& key : handles.keys()) {
		const auto handleList = handles[key];
		for (const auto& p : handleList) {
			const auto fp = p + key->pos();
			QGraphicsEllipseItem* circleItem = new QGraphicsEllipseItem(fp.x() - radius, fp.y() - radius, 2 * radius, 2 * radius);
			circleItem->setBrush(QBrush(Qt::green));
			circleItem->setPen(QPen(Qt::darkGreen));

			// Add the circle item to the scene 
			thisPointer->scene()->addItem(circleItem);
		}
	}
}

auto QCanvasViewRect::Impl::IsInHandle(QPointF pos) -> std::tuple<bool, QGraphicsPolygonItem*, int> {
	for (auto& body : handles.keys()) {
		if (false == handles.contains(body)) {
			return std::make_tuple(false, nullptr, -1);
		}
		const auto handleList = handles[body];
		for (auto i = 0; i < handleList.count(); i++) {
			const auto fp = handleList[i] + body->pos();
			if (distance(fp, pos) < radius) {
				return std::make_tuple(true, body, i);
			}
		}
	}
	return std::make_tuple(false, nullptr, -1);
}


auto QCanvasViewRect::Impl::distance(const QPointF& point1, const QPointF& point2) -> double {
	qreal dx = point1.x() - point2.x();
	qreal dy = point1.y() - point2.y();
	return std::sqrt(dx * dx + dy * dy);
}


auto QCanvasViewRect::Impl::RedrawPolygonItems(float new_h, float new_w) -> void {
	for (auto i = 0; i < polygons.count(); i++) {
		auto metaPoly = polygons[i];
		const auto& shape = polygonItems[i];
		if (nullptr == shape) {
			continue;
		}

		QPolygonF newPoly;
		for (auto p : metaPoly) {
			QPointF revisedP = QPointF(p.x() / cur_scene_width * static_cast<double>(new_w), p.y() / cur_scene_height * static_cast<double>(new_h));
			newPoly << revisedP;
		}
		polygons[i] = newPoly;
		if (handles.contains(shape)) {
			handles[shape] = newPoly.toList();
		}
		shape->setPolygon(newPoly);
		shape->update();

		thisPointer->scene()->addItem(shape);
	}
	RedrawHandles();
}

QCanvasViewRect::QCanvasViewRect(QWidget* parent) : d { new Impl } {
	Q_UNUSED(parent)
	d->thisPointer = this;
	setMouseTracking(true);
}

QCanvasViewRect::~QCanvasViewRect() { }

auto QCanvasViewRect::GetColors() -> QList<QColor> {
	QList<QColor> result;
	if (d->isDrawing) {
		result.push_back(QColor(150, 150, 150, 120));
	}
	result.append(d->colors);
	return result;
}

auto QCanvasViewRect::GetPolygons() -> QList<QPolygonF> {
	QList<QPolygonF> result;

	if (d->isDrawing) {
		auto poly = d->activePolygon;
		auto shape = d->activePolygonItem;
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);			
		}
		result.append(entry);
	}

	for (auto i = 0; i < d->polygons.count(); i++) {
		auto poly = d->polygons[i];
		auto shape = d->polygonItems[i];
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);
		}
		result.append(entry);
	}
	return result;
}

auto QCanvasViewRect::GetHidden() -> QList<bool> {
	QList<bool> result;
	if (d->isDrawing) {
		result.push_back(false);
	}
	result.append(d->isHidden);
	return result;
}

auto QCanvasViewRect::Clear() -> void {
	d->movingPolygonItem = nullptr;
	d->activePolygonItem = nullptr;
	d->activePolygon.clear();
	d->colors.clear();
	d->isHidden.clear();
	d->polygons.clear();
	d->polygonItems.clear();
}

void QCanvasViewRect::mouseMoveEvent(QMouseEvent* event) {
	if (event->buttons() & Qt::LeftButton) {
		QPointF scenePos = mapToScene(event->pos());
		// Continue drawing the polygon
		if (d->isHandling) {
			const auto newPos = scenePos - d->dragOffset;
			d->handles[d->handlingPolygonItem][d->handleIdx] = newPos;
			switch (d->handleIdx) {
				case 0:
					d->handles[d->handlingPolygonItem][1].setX(newPos.x());
					d->handles[d->handlingPolygonItem][3].setY(newPos.y());
					break;
				case 1:
					d->handles[d->handlingPolygonItem][0].setX(newPos.x());
					d->handles[d->handlingPolygonItem][2].setY(newPos.y());
					break;
				case 2:
					d->handles[d->handlingPolygonItem][1].setY(newPos.y());
					d->handles[d->handlingPolygonItem][3].setX(newPos.x());
					break;
				case 3:
					d->handles[d->handlingPolygonItem][0].setY(newPos.y());
					d->handles[d->handlingPolygonItem][2].setX(newPos.x());
					break;
			}
			const auto idx = d->polygonItems.indexOf(d->handlingPolygonItem);
			auto result = QPolygonF();
			for (auto& p : d->handles[d->handlingPolygonItem]) {
				result << p;
			}

			d->polygons[idx] = result;
			d->handlingPolygonItem->setPolygon(d->polygons[idx]);

			d->RedrawHandles();

			emit sigUpdatePolygons();
		} else if (d->movingPolygonItem) {
			d->movingPolygonItem->setPos(scenePos - d->dragOffset);
			d->RedrawHandles();
			emit sigUpdatePolygons();
		} else if (d->activePolygonItem) {
			d->activePolygon.clear();
			d->activePolygon << QPointF(d->startPoint.x(), d->startPoint.y());
			d->activePolygon << QPointF(d->startPoint.x(), scenePos.y());
			d->activePolygon << QPointF(scenePos.x(), scenePos.y());
			d->activePolygon << QPointF(scenePos.x(), d->startPoint.y());
			//d->activePolygon << QPointF(d->startPoint.x(), d->startPoint.y());
			d->activePolygonItem->setPolygon(d->activePolygon);

			emit sigUpdatePolygons();
		}
	}
}

void QCanvasViewRect::mouseDoubleClickEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		QPointF scenePos = mapToScene(event->pos());
		QGraphicsItem* item = scene()->itemAt(scenePos, transform());
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(selectedPolygon->brush().color());
			diag.setWindowTitle("Change Color");
			if (diag.exec() == 0) {
				return;
			}
			QColor result = diag.selectedColor();
			selectedPolygon->setBrush(QBrush(result));
			auto penColor = result;
			penColor.setAlphaF(1);
			selectedPolygon->setPen(QPen(penColor, 2));
			d->colors[idx] = result;

			d->SetHighlightIndex(idx);

			emit sigUpdatePolygons();
		}
	}
}

void QCanvasViewRect::mousePressEvent(QMouseEvent* event) {
	QPointF scenePos = mapToScene(event->pos());
	QGraphicsItem* item = scene()->itemAt(scenePos, transform());

	if (event->button() == Qt::LeftButton) {
		auto result_tuple = d->IsInHandle(scenePos);
		const auto isHandle = std::get<0>(result_tuple);
		if (isHandle) {
			const auto handleBody = std::get<1>(result_tuple);
			const auto handleID = std::get<2>(result_tuple);
			d->handlingPolygonItem = handleBody;
			d->handleIdx = handleID;
			d->isHandling = isHandle;
			// Store the offset from the clicked point to the handle's position
			d->dragOffset = scenePos - d->handles[d->handlingPolygonItem][d->handleIdx];
		} else if (item && item->type() == QGraphicsPolygonItem::Type) {
			d->movingPolygonItem = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			// Store the offset from the clicked point to the polygon's position
			d->dragOffset = scenePos - d->movingPolygonItem->pos();
		} else {
			// Start drawing a new point
			d->movingPolygonItem = nullptr; // No active polygon during drawing
			d->activePolygon.clear();
			d->startPoint = scenePos;
			d->activePolygonItem = scene()->addPolygon(QPolygonF(), QPen(Qt::gray), QBrush(Qt::lightGray));
			d->activePolygonItem->setPolygon(d->activePolygon);
			d->isDrawing = true;
		}
	} else if (event->button() == Qt::RightButton) {
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->polygonItems.removeAt(idx);
			d->polygons.removeAt(idx);
			d->isHidden.removeAt(idx);
			d->colors.removeAt(idx);
			d->handles.remove(selectedPolygon);

			scene()->removeItem(selectedPolygon);

			d->RedrawHandles();

			emit sigUpdatePolygons();
		}
	} else if (event->button() == Qt::MiddleButton) {
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->isHidden[idx] = !d->isHidden[idx];
			if (d->isHidden[idx]) {
				selectedPolygon->setBrush(Qt::transparent);
			} else {
				selectedPolygon->setBrush(d->colors[idx]);
			}
			emit sigUpdatePolygons();
		}
	}
	QGraphicsView::mousePressEvent(event);
}

auto QCanvasViewRect::SetTransp(int idx, float transp) -> void {
	if (idx < 0) {
		return;
	}
	if (d->colors.count() <= idx) {
		return;
	}
	QColor newColor;
	newColor = d->colors[idx];
	newColor.setAlphaF(transp);

	d->colors[idx] = newColor;

	auto penColor = newColor;
	penColor.setAlphaF(1);
	d->polygonItems[idx]->setPen(QPen(penColor, 3, Qt::DotLine));
	d->polygonItems[idx]->setBrush(QBrush(newColor));

	emit sigUpdatePolygons();
}


auto QCanvasViewRect::SetHandleRadius(float radius) -> void {
	d->radius = radius;
	d->RedrawHandles();
}

void QCanvasViewRect::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {		
		if (d->isHandling) {
			auto highlight_idx = d->polygonItems.indexOf(d->handlingPolygonItem);
			d->SetHighlightIndex(highlight_idx);
			d->handlingPolygonItem = nullptr;
			d->handleIdx = -1;
			d->isHandling = false;
		} else if (!d->movingPolygonItem) {
			// Finish drawing the polygon
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(QColor(150, 150, 150, 120));
			diag.setWindowTitle("Select Color");
			if (diag.exec() == 0) {
				d->activePolygon.clear();
				d->activePolygonItem->setPolygon(QPolygonF());
				emit sigUpdatePolygons();
				return;
			}
			QColor result = diag.selectedColor();
			auto penColor = result;
			penColor.setAlphaF(1);
			const auto new_polygon = scene()->addPolygon(d->activePolygon, QPen(penColor, 2), QBrush(result));
			d->polygonItems.append(new_polygon);
			d->polygons.append(d->activePolygon);
			d->isHidden.append(false);
			d->colors.append(result);

			for (auto& p : d->activePolygon) {
				d->handles[new_polygon] << p;
			}
			d->activePolygon.clear();
			d->activePolygonItem->setPolygon(QPolygonF());

			d->RedrawHandles();

			d->SetHighlightIndex(d->polygonItems.count() - 1);

			emit sigUpdatePolygons();
		} else {
			auto highlight_idx = d->polygonItems.indexOf(d->movingPolygonItem);
			d->SetHighlightIndex(highlight_idx);
			d->movingPolygonItem = nullptr;
		}
		d->isDrawing = false;
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void QCanvasViewRect::keyPressEvent(QKeyEvent* event) {
	QGraphicsView::keyPressEvent(event);
}

void QCanvasViewRect::resizeEvent(QResizeEvent* event) {	
	const auto new_height = event->size().height();
	const auto new_width = event->size().width();
	
	QCanvasScene2D* scene = new QCanvasScene2D;
	scene->setSceneRect(QRectF(0, 0, new_width, new_height));
	setScene(scene);

	d->RedrawPolygonItems(static_cast<float>(new_height), static_cast<float>(new_width));

	d->cur_scene_height = new_height;
	d->cur_scene_width = new_width;

	QGraphicsView::resizeEvent(event);
}
