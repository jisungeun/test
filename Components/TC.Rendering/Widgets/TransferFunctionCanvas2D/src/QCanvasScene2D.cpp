#include "QCanvasScene2D.h"

#include <QGraphicsSceneMouseEvent>

struct QCanvasScene2D::Impl {

};

QCanvasScene2D::QCanvasScene2D(QWidget* parent):
d(new Impl()){
	Q_UNUSED(parent)
    
}

QCanvasScene2D::~QCanvasScene2D() {
    
}

void QCanvasScene2D::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	if (event->button() == Qt::RightButton)
	{
		
	}

	QGraphicsScene::mousePressEvent(event);
}

void QCanvasScene2D::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	QGraphicsScene::mouseMoveEvent(event);
}

void QCanvasScene2D::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	QGraphicsScene::mouseReleaseEvent(event);
}
