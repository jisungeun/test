#include <QResizeEvent>
#include <QGraphicsPolygonItem>
#include <QColorDialog>

#include <QCanvasViewPolygon.h>

#include <iostream>

#include "QCanvasScene2D.h"

struct QCanvasViewPolygon::Impl {
	QCanvasViewPolygon* thisPointer { nullptr };

	int cur_scene_height { 200 };
	int cur_scene_width { 200 };

	QList<QPolygonF> polygons;
	QList<QGraphicsPolygonItem*> polygonItems;
	QGraphicsPolygonItem* activePolygonItem { nullptr };
	QList<bool> isHidden;
	QList<QColor> colors;
	QPolygonF activePolygon;
	QList<QPointF> activeHandle;
	QGraphicsPolygonItem* movingPolygonItem { nullptr };
	QGraphicsPolygonItem* handlingPolygonItem { nullptr };
	QPointF dragOffset;
	int handleIdx { -1 };

	QMap<QGraphicsPolygonItem*, QList<QPointF>> handles;
	float radius { 5 };

	bool isDrawing { false };
	bool isHandling { false };

	auto toScenePos(const QPointF& dataPos) -> QPointF;
	auto toDataPos(const QPointF& scenePos) -> QPointF;

	auto SetHighlightIndex(int idx)->void;
	auto RedrawHandles() -> void;
	auto RedrawPolygonItems(float new_h, float new_w) -> void;
	auto IsInHandle(QPointF pos) -> std::tuple<bool, QGraphicsPolygonItem*, int>;
	auto distance(const QPointF& point1, const QPointF& point2) -> double;
};

auto QCanvasViewPolygon::Impl::SetHighlightIndex(int idx) -> void {
	for(auto i=0;i<polygonItems.count();i++) {
		const auto color = colors[i];
		auto penColor = color;
		penColor.setAlphaF(1);

		polygonItems[i]->setPen(QPen(penColor, 2));
	}

	if(idx < 0) {
		return;
	}

	if(idx >= polygonItems.count()) {
		return;
	}

	const auto targetColor = colors[idx];
	auto penColor = targetColor;
	penColor.setAlphaF(1);
	polygonItems[idx]->setPen(QPen(penColor, 3, Qt::PenStyle::DotLine));

	emit thisPointer->sigCurPolygon(idx);
}

auto QCanvasViewPolygon::Impl::toDataPos(const QPointF& scenePos) -> QPointF {
	return QPointF(scenePos.x() / cur_scene_width, 1 - scenePos.y() / cur_scene_height);
}

auto QCanvasViewPolygon::Impl::toScenePos(const QPointF& dataPos) -> QPointF {
	return QPointF(dataPos.x() * cur_scene_width, (1 - dataPos.y()) * cur_scene_height);
}


auto QCanvasViewPolygon::Impl::distance(const QPointF& point1, const QPointF& point2) -> double {
	qreal dx = point1.x() - point2.x();
	qreal dy = point1.y() - point2.y();
	return std::sqrt(dx * dx + dy * dy);
}

auto QCanvasViewPolygon::SetHandleRadius(float radius) -> void {
	d->radius = radius;
	d->RedrawHandles();
}

auto QCanvasViewPolygon::Impl::IsInHandle(QPointF pos) -> std::tuple<bool, QGraphicsPolygonItem*, int> {
	for (auto& body : handles.keys()) {
		if (false == handles.contains(body)) {
			return std::make_tuple(false, nullptr, -1);
		}
		const auto handleList = handles[body];
		for (auto i = 0; i < handleList.count(); i++) {
			const auto fp = handleList[i] + body->pos();
			if (distance(fp, pos) < radius) {
				return std::make_tuple(true, body, i);
			}
		}
	}
	return std::make_tuple(false, nullptr, -1);
}

auto QCanvasViewPolygon::Impl::RedrawHandles() -> void {
	//remove every handles
	for (const auto& item : thisPointer->scene()->items()) {
		if (const auto ellipse = qgraphicsitem_cast<QGraphicsEllipseItem*>(item)) {
			thisPointer->scene()->removeItem(ellipse);
		}
	}
	//redraw every handles		
	for (const auto& key : handles.keys()) {
		const auto handleList = handles[key];
		for (const auto& p : handleList) {
			const auto fp = p + key->pos();
			QGraphicsEllipseItem* circleItem = new QGraphicsEllipseItem(fp.x() - radius, fp.y() - radius, 2 * radius, 2 * radius);
			circleItem->setBrush(QBrush(Qt::green));
			circleItem->setPen(QPen(Qt::darkGreen));

			// Add the circle item to the scene 
			thisPointer->scene()->addItem(circleItem);
		}
	}

}

auto QCanvasViewPolygon::Impl::RedrawPolygonItems(float new_h, float new_w) -> void {
	for (auto i = 0; i < polygons.count(); i++) {
		auto metaPoly = polygons[i];
		const auto& shape = polygonItems[i];
		if (nullptr == shape) {
			continue;
		}

		QPolygonF newPoly;
		for (auto p : metaPoly) {
			QPointF revisedP = QPointF(p.x() / cur_scene_width * static_cast<double>(new_w), p.y() / cur_scene_height * static_cast<double>(new_h));
			newPoly << revisedP;
		}
		polygons[i] = newPoly;
		if (handles.contains(shape)) {
			handles[shape] = newPoly.toList();
		}
		shape->setPolygon(newPoly);
		shape->update();

		thisPointer->scene()->addItem(shape);
	}
	RedrawHandles();
}

QCanvasViewPolygon::QCanvasViewPolygon(QWidget* parent) : d { new Impl } {
	Q_UNUSED(parent)
	d->thisPointer = this;
	setMouseTracking(true);
}

QCanvasViewPolygon::~QCanvasViewPolygon() {}

auto QCanvasViewPolygon::GetColors() -> QList<QColor> {
	QList<QColor> result;
	if (d->isDrawing) {
		result.push_back(QColor(150, 150, 150, 120));
	}
	result.append(d->colors);
	return result;
}

auto QCanvasViewPolygon::SetTransp(int idx, float transp) -> void {
	if(idx < 0) {
		return;
	}
	if(d->colors.count() <= idx) {
		return;
	}
	QColor newColor;
	newColor = d->colors[idx];
	newColor.setAlphaF(transp);

	d->colors[idx] = newColor;

	auto penColor = newColor;
	penColor.setAlphaF(1);
	d->polygonItems[idx]->setPen(QPen(penColor,3,Qt::DotLine));
	d->polygonItems[idx]->setBrush(QBrush(newColor));

	emit sigUpdatePolygons();
}

auto QCanvasViewPolygon::GetPolygons() -> QList<QPolygonF> {
	QList<QPolygonF> result;

	if (d->isDrawing) {
		auto poly = d->activePolygon;
		auto shape = d->activePolygonItem;
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);
		}
		result.append(entry);
	}

	for (auto i = 0; i < d->polygons.count(); i++) {
		auto poly = d->polygons[i];
		auto shape = d->polygonItems[i];
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);
		}
		result.append(entry);
	}
	return result;
}

auto QCanvasViewPolygon::GetHidden() -> QList<bool> {
	QList<bool> result;
	if (d->isDrawing) {
		result.push_back(false);
	}
	result.append(d->isHidden);
	return result;
}

auto QCanvasViewPolygon::Clear() -> void {
	d->movingPolygonItem = nullptr;
	d->activePolygonItem = nullptr;
	d->activePolygon.clear();
	d->colors.clear();
	d->isHidden.clear();
	d->polygons.clear();
	d->polygonItems.clear();
}

void QCanvasViewPolygon::mouseMoveEvent(QMouseEvent* event) {
	QPointF scenePos = mapToScene(event->pos());
	if (event->buttons() & Qt::LeftButton) {
		// Continue drawing the polygon
		if (d->isHandling) {
			d->handles[d->handlingPolygonItem][d->handleIdx] = scenePos - d->dragOffset;

			const auto idx = d->polygonItems.indexOf(d->handlingPolygonItem);

			auto result = QPolygonF();
			for (auto& p : d->handles[d->handlingPolygonItem]) {
				result << p;
			}

			d->polygons[idx] = result;
			d->handlingPolygonItem->setPolygon(d->polygons[idx]);

			d->RedrawHandles();

			emit sigUpdatePolygons();
		} else if (d->movingPolygonItem) {
			d->movingPolygonItem->setPos(scenePos - d->dragOffset);
			d->RedrawHandles();
			emit sigUpdatePolygons();
		}
	}
	if (d->activePolygonItem && d->isDrawing) {
		d->activePolygon.clear();
		for (const auto& p : d->activeHandle) {
			d->activePolygon << p;
		}
		d->activePolygon << scenePos;
		d->activePolygonItem->setPolygon(d->activePolygon);

		emit sigUpdatePolygons();
	}
}

void QCanvasViewPolygon::mouseDoubleClickEvent(QMouseEvent* event) {
	if (d->isDrawing) {
		return;
	}
	if (event->button() == Qt::LeftButton) {
		QPointF scenePos = mapToScene(event->pos());
		QGraphicsItem* item = scene()->itemAt(scenePos, transform());
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(selectedPolygon->brush().color());
			diag.setWindowTitle("Change Color");
			if (diag.exec() == 0) {
				return;
			}
			QColor result = diag.selectedColor();
			selectedPolygon->setBrush(QBrush(result));
			auto penColor = result;
			penColor.setAlphaF(1);
			selectedPolygon->setPen(QPen(penColor, 2));
			d->colors[idx] = result;

			d->SetHighlightIndex(idx);

			emit sigUpdatePolygons();
		}
	}
}

void QCanvasViewPolygon::mousePressEvent(QMouseEvent* event) {
	QPointF scenePos = mapToScene(event->pos());
	QGraphicsItem* item = scene()->itemAt(scenePos, transform());

	if (event->button() == Qt::LeftButton) {
		if (d->isDrawing) {
			d->activePolygon << scenePos;
			d->activeHandle << scenePos;
			d->activePolygonItem->setPolygon(d->activePolygon);
		} else {
			auto result_tuple = d->IsInHandle(scenePos);
			const auto isHandle = std::get<0>(result_tuple);
			if (isHandle) {
				const auto handleBody = std::get<1>(result_tuple);
				const auto handleID = std::get<2>(result_tuple);
				d->handlingPolygonItem = handleBody;
				d->handleIdx = handleID;
				d->isHandling = isHandle;
				// Store the offset from the clicked point to the handle's position
				d->dragOffset = scenePos - d->handles[d->handlingPolygonItem][d->handleIdx];
			} else if (item && item->type() == QGraphicsPolygonItem::Type) {
				d->movingPolygonItem = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
				// Store the offset from the clicked point to the polygon's position
				d->dragOffset = scenePos - d->movingPolygonItem->pos();
			} else {
				// Start drawing a new point
				d->movingPolygonItem = nullptr; // No active polygon during drawing
				d->activePolygon.clear();
				d->activePolygon << scenePos;
				d->activeHandle.clear();
				d->activeHandle << scenePos;
				d->activePolygonItem = scene()->addPolygon(QPolygonF(), QPen(Qt::gray), QBrush(Qt::lightGray));
				d->activePolygonItem->setPolygon(d->activePolygon);
				d->isDrawing = true;
			}
		}
	} else if (event->button() == Qt::RightButton) {
		if (false == d->isDrawing && item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->polygonItems.removeAt(idx);
			d->polygons.removeAt(idx);
			d->isHidden.removeAt(idx);
			d->colors.removeAt(idx);
			d->handles.remove(selectedPolygon);

			scene()->removeItem(selectedPolygon);

			d->RedrawHandles();

			emit sigUpdatePolygons();
		}
	} else if (event->button() == Qt::MiddleButton) {
		if (false == d->isDrawing && item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->isHidden[idx] = !d->isHidden[idx];
			if (d->isHidden[idx]) {
				selectedPolygon->setBrush(Qt::transparent);
			} else {
				selectedPolygon->setBrush(d->colors[idx]);
			}
			emit sigUpdatePolygons();
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void QCanvasViewPolygon::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		auto highlight_idx = -1;
		if (d->isHandling) {
			highlight_idx = d->polygonItems.indexOf(d->handlingPolygonItem);			

			d->handlingPolygonItem = nullptr;
			d->handleIdx = -1;
			d->isHandling = false;
		} else if (d->movingPolygonItem) {
			highlight_idx = d->polygonItems.indexOf(d->movingPolygonItem);			

			d->movingPolygonItem = nullptr;
		}
		d->SetHighlightIndex(highlight_idx);

	} else if (event->button() == Qt::RightButton) {
		if (d->isDrawing) {
			//finish drawing
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(QColor(150, 150, 150, 120));
			diag.setWindowTitle("Select Color");
			if (diag.exec() == 0) {
				d->activePolygon.clear();
				d->activePolygonItem->setPolygon(QPolygonF());
				d->activeHandle.clear();
				d->isDrawing = false;
				emit sigUpdatePolygons();
				return;
			}
			QColor result = diag.selectedColor();
			auto penColor = result;
			penColor.setAlphaF(1);
			const auto new_polygon = scene()->addPolygon(d->activePolygon, QPen(penColor, 2), QBrush(result));
			d->polygonItems.append(new_polygon);
			d->polygons.append(d->activePolygon);
			d->isHidden.append(false);
			d->colors.append(result);

			for (auto& p : d->activePolygon) {
				d->handles[new_polygon] << p;
			}

			d->activePolygon.clear();
			d->activePolygonItem->setPolygon(QPolygonF());
			d->activeHandle.clear();
			d->isDrawing = false;

			d->RedrawHandles();

			d->SetHighlightIndex(d->polygonItems.count()-1);

			emit sigUpdatePolygons();
		}
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void QCanvasViewPolygon::keyPressEvent(QKeyEvent* event) {
	if (event->key() == Qt::Key_Escape) {
		if (d->isDrawing) {
			d->activePolygon.clear();
			d->activePolygonItem->setPolygon(QPolygonF());
			d->activeHandle.clear();
			d->isDrawing = false;
			emit sigUpdatePolygons();
			return;
		}
	}
	QGraphicsView::keyPressEvent(event);
}

void QCanvasViewPolygon::resizeEvent(QResizeEvent* event) {
	const auto new_height = event->size().height();
	const auto new_width = event->size().width();

	QCanvasScene2D* scene = new QCanvasScene2D;
	scene->setSceneRect(QRectF(0, 0, new_width, new_height));
	setScene(scene);

	d->RedrawPolygonItems(static_cast<float>(new_height), static_cast<float>(new_width));

	d->cur_scene_height = new_height;
	d->cur_scene_width = new_width;

	QGraphicsView::resizeEvent(event);
}
