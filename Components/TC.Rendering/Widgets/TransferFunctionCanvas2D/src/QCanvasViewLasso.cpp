#include <QResizeEvent>
#include <QGraphicsPolygonItem>
#include <QColorDialog>

#include "QCanvasViewLasso.h"

#include <iostream>

#include "QCanvasScene2D.h"

struct QCanvasViewLasso::Impl {
	QCanvasViewLasso* thisPointer { nullptr };

	int cur_scene_height { -1 };
	int cur_scene_width { -1 };

	QList<QPolygonF> polygons;
	QList<QGraphicsPolygonItem*> polygonItems;
	QGraphicsPolygonItem* activePolygonItem { nullptr };
	QList<bool> isHidden;
	QList<QColor> colors;
	QPolygonF activePolygon;
	QGraphicsPolygonItem* movingPolygonItem { nullptr };
	QPointF dragOffset;

	bool isDrawing { false };

	auto toScenePos(const QPointF& dataPos) -> QPointF;
	auto toDataPos(const QPointF& scenePos) -> QPointF;

	auto SetHighlightIndex(int idx)->void;

	auto RedrawPolygonItems(float new_h, float new_w) -> void;
};

auto QCanvasViewLasso::Impl::SetHighlightIndex(int idx) -> void {
	for (auto i = 0; i < polygonItems.count(); i++) {
		const auto color = colors[i];
		auto penColor = color;
		penColor.setAlphaF(1);

		polygonItems[i]->setPen(QPen(penColor, 2));
	}

	if (idx < 0) {
		return;
	}

	if (idx >= polygonItems.count()) {
		return;
	}

	const auto targetColor = colors[idx];
	auto penColor = targetColor;
	penColor.setAlphaF(1);
	polygonItems[idx]->setPen(QPen(penColor, 3, Qt::PenStyle::DotLine));

	emit thisPointer->sigCurPolygon(idx);
}

auto QCanvasViewLasso::SetTransp(int idx, float transp) -> void {
	if (idx < 0) {
		return;
	}
	if (d->colors.count() <= idx) {
		return;
	}
	QColor newColor;
	newColor = d->colors[idx];
	newColor.setAlphaF(transp);

	d->colors[idx] = newColor;

	auto penColor = newColor;
	penColor.setAlphaF(1);
	d->polygonItems[idx]->setPen(QPen(penColor, 3, Qt::DotLine));
	d->polygonItems[idx]->setBrush(QBrush(newColor));

	emit sigUpdatePolygons();
}

auto QCanvasViewLasso::Impl::toDataPos(const QPointF& scenePos) -> QPointF {
	return QPointF(scenePos.x() / cur_scene_width, 1 - scenePos.y() / cur_scene_height);
}

auto QCanvasViewLasso::Impl::toScenePos(const QPointF& dataPos) -> QPointF {
	return QPointF(dataPos.x() * cur_scene_width, (1 - dataPos.y()) * cur_scene_height);
}


auto QCanvasViewLasso::Impl::RedrawPolygonItems(float new_h, float new_w) -> void {
	for (auto i = 0; i < polygons.count(); i++) {
		auto metaPoly = polygons[i];
		const auto& shape = polygonItems[i];
		if (nullptr == shape) {
			continue;
		}

		QPolygonF newPoly;
		for (auto p : metaPoly) {
			QPointF revisedP = QPointF(p.x() / cur_scene_width * static_cast<double>(new_w), p.y() / cur_scene_height * static_cast<double>(new_h));
			newPoly << revisedP;
		}
		polygons[i] = newPoly;
		shape->setPolygon(newPoly);
		shape->update();

		thisPointer->scene()->addItem(shape);
	}
}

QCanvasViewLasso::QCanvasViewLasso(QWidget* parent) : d { new Impl } {
	Q_UNUSED(parent)
	d->thisPointer = this;
	setMouseTracking(true);
}

QCanvasViewLasso::~QCanvasViewLasso() { }

auto QCanvasViewLasso::GetColors() -> QList<QColor> {
	QList<QColor> result;
	if (d->isDrawing) {
		result.push_back(QColor(150, 150, 150, 120));
	}
	result.append(d->colors);
	return result;
}

auto QCanvasViewLasso::GetPolygons() -> QList<QPolygonF> {
	QList<QPolygonF> result;

	if (d->isDrawing) {
		auto poly = d->activePolygon;
		auto shape = d->activePolygonItem;
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);
		}
		result.append(entry);
	}

	for (auto i = 0; i < d->polygons.count(); i++) {
		auto poly = d->polygons[i];
		auto shape = d->polygonItems[i];
		poly.translate(shape->x(), shape->y());
		QPolygonF entry;
		for (const auto p : poly) {
			entry << d->toDataPos(p);
		}
		result.append(entry);
	}
	return result;
}

auto QCanvasViewLasso::GetHidden() -> QList<bool> {
	QList<bool> result;
	if (d->isDrawing) {
		result.push_back(false);
	}
	result.append(d->isHidden);
	return result;
}

auto QCanvasViewLasso::Clear() -> void {
	d->movingPolygonItem = nullptr;
	d->activePolygonItem = nullptr;
	d->activePolygon.clear();
	d->colors.clear();
	d->isHidden.clear();
	d->polygons.clear();
	d->polygonItems.clear();
}

void QCanvasViewLasso::mouseMoveEvent(QMouseEvent* event) {
	if (event->buttons() & Qt::LeftButton) {
		QPointF scenePos = mapToScene(event->pos());
		// Continue drawing the polygon
		if (d->movingPolygonItem) {
			d->movingPolygonItem->setPos(scenePos - d->dragOffset);

			emit sigUpdatePolygons();
		} else if (d->activePolygonItem) {
			d->activePolygon << scenePos;
			d->activePolygonItem->setPolygon(d->activePolygon);

			emit sigUpdatePolygons();
		}
	}
}

void QCanvasViewLasso::mouseDoubleClickEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		QPointF scenePos = mapToScene(event->pos());
		QGraphicsItem* item = scene()->itemAt(scenePos, transform());
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(selectedPolygon->brush().color());
			diag.setWindowTitle("Change Color");
			if (diag.exec() == 0) {
				return;
			}
			QColor result = diag.selectedColor();			
			selectedPolygon->setBrush(QBrush(result));
			auto penColor = result;
			penColor.setAlphaF(1);
			selectedPolygon->setPen(QPen(penColor, 2));
			d->colors[idx] = result;

			d->SetHighlightIndex(idx);

			emit sigUpdatePolygons();
		}
	}
}

void QCanvasViewLasso::mousePressEvent(QMouseEvent* event) {
	QPointF scenePos = mapToScene(event->pos());
	QGraphicsItem* item = scene()->itemAt(scenePos, transform());
	if (event->button() == Qt::LeftButton) {
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			d->movingPolygonItem = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			// Store the offset from the clicked point to the polygon's position
			d->dragOffset = scenePos - d->movingPolygonItem->pos();
		} else {
			// Start drawing a new point
			d->movingPolygonItem = nullptr; // No active polygon during drawing
			d->activePolygon.clear();
			d->activePolygon << scenePos;
			d->activePolygonItem = scene()->addPolygon(QPolygonF(), QPen(Qt::gray), QBrush(Qt::lightGray));
			d->activePolygonItem->setPolygon(d->activePolygon);
			d->isDrawing = true;
		}
	} else if (event->button() == Qt::RightButton) {
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->polygonItems.removeAt(idx);
			d->polygons.removeAt(idx);
			d->isHidden.removeAt(idx);
			d->colors.removeAt(idx);

			scene()->removeItem(selectedPolygon);

			emit sigUpdatePolygons();
		}
	} else if (event->button() == Qt::MiddleButton) {
		if (item && item->type() == QGraphicsPolygonItem::Type) {
			const auto selectedPolygon = qgraphicsitem_cast<QGraphicsPolygonItem*>(item);
			const auto idx = d->polygonItems.indexOf(selectedPolygon);
			d->isHidden[idx] = !d->isHidden[idx];
			if (d->isHidden[idx]) {
				selectedPolygon->setBrush(Qt::transparent);
			} else {
				selectedPolygon->setBrush(d->colors[idx]);
			}
			emit sigUpdatePolygons();
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void QCanvasViewLasso::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		// Finish drawing the polygon
		if (!d->movingPolygonItem) {
			QColorDialog diag;
			diag.setOption(QColorDialog::ColorDialogOption::ShowAlphaChannel);
			diag.setCurrentColor(QColor(150, 150, 150, 120));
			diag.setWindowTitle("Select Color");
			if (diag.exec() == 0) {
				d->activePolygon.clear();
				d->activePolygonItem->setPolygon(QPolygonF());
				emit sigUpdatePolygons();
				return;
			}
			QColor result = diag.selectedColor();
			auto penColor = result;
			penColor.setAlphaF(1);
			const auto new_polygon = scene()->addPolygon(d->activePolygon, QPen(penColor, 2), QBrush(result));
			d->polygonItems.append(new_polygon);
			d->polygons.append(d->activePolygon);
			d->isHidden.append(false);
			d->colors.append(result);

			d->activePolygon.clear();
			d->activePolygonItem->setPolygon(QPolygonF());

			d->SetHighlightIndex(d->polygonItems.count() - 1);

			emit sigUpdatePolygons();
		} else {
			const auto idx = d->polygonItems.indexOf(d->movingPolygonItem);
			d->movingPolygonItem = nullptr;
			d->SetHighlightIndex(idx);
		}
		d->isDrawing = false;
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void QCanvasViewLasso::keyPressEvent(QKeyEvent* event) {
	QGraphicsView::keyPressEvent(event);
}

void QCanvasViewLasso::resizeEvent(QResizeEvent* event) {
	const auto new_height = event->size().height();
	const auto new_width = event->size().width();

	QCanvasScene2D* scene = new QCanvasScene2D;
	scene->setSceneRect(QRectF(0, 0, new_width, new_height));
	setScene(scene);

	d->RedrawPolygonItems(static_cast<float>(new_height), static_cast<float>(new_width));

	d->cur_scene_height = new_height;
	d->cur_scene_width = new_width;

	QGraphicsView::resizeEvent(event);
}
