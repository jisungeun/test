#include <iostream>

#include "QTransferFunctionCanvas2D.h"
#include "QCanvasScene2D.h"
#include "ui_QCanvas2D.h"

#include "HiddenScene.h"

#include <QWheelEvent>
#include <QPixmap>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QScrollBar>
#include <QDir>

#define WHEEL_DELTA 120

struct QTransferFunctionCanvas2D::Impl {
	///////////////////////////
	HiddenScene* hiddenScene{ nullptr };
	/////////////////////////

	double dividerY;
	double dividerX;
	int decimalX;
	int decimalY;
	double single_stepX;
	double single_stepY;
	QString nameX;
	QString nameY;

	double value = 1;

	QString cur_proj_path;
};

QTransferFunctionCanvas2D::QTransferFunctionCanvas2D(QWidget* parent) :
	QWidget(parent),
	d(new Impl()),
	ui(new Ui::QCanvas2D)
{
	ui->setupUi(this);	
	QCanvasScene2D* scene = new QCanvasScene2D(this);	
	scene->setSceneRect(0, 0, 800, 200);	
	 
	ui->Canvas->setScene(scene);
	ui->Canvas->resize(800, 200);

	ui->opacitySpin->setRange(0.0, 1.0);
	ui->opacitySpin->setSingleStep(0.01);
	ui->opacitySpin->setDecimals(2);

	ui->grayscaleRadio->setChecked(true);

	d->dividerY = 1.0;
	d->dividerX = 1.0;

	connect(ui->opacitySpin, SIGNAL(valueChanged(double)), this, SLOT(opacityChanged(double)));
	connect(ui->xMaxSpin, SIGNAL(valueChanged(double)), this, SLOT(maxXChanged(double)));
	connect(ui->xMinSpin, SIGNAL(valueChanged(double)), this, SLOT(minXChanged(double)));

	connect(ui->yMaxSpin, SIGNAL(valueChanged(double)), this, SLOT(maxYChanged(double)));
	connect(ui->yMinSpin, SIGNAL(valueChanged(double)), this, SLOT(minYChanged(double)));

	connect(ui->Canvas, SIGNAL(sigMousePress()), this, SLOT(canvasMousePressed()));
	connect(ui->Canvas, SIGNAL(sigMouseRelease()), this, SLOT(canvasMouseReleased()));
	connect(ui->Canvas, SIGNAL(sigUpdateBoxs()), this, SLOT(sendRenderCall()));
	connect(ui->Canvas, SIGNAL(sigUpdatePannel()), this, SLOT(updatePannel()));
	
	connect(ui->grayscaleRadio, SIGNAL(clicked()), this, SLOT(updateHistogram()));
	connect(ui->heatmapRadio, SIGNAL(clicked()), this, SLOT(updateHistogram()));		

    // hide control ui
	//ui->controlWidget->hide();

	//ui->CanvasWidget->setStyleSheet("border-image:url(\"Background.png\") 0 0 0 0 stretch stretch;");
	ui->CanvasWidget->setStyleSheet("border-image:url(\"Background.tif\") 0 0 0 0 stretch stretch;");
	//can refresh image with this line

	// hide control ui
	ui->controlWidget->hide();
	ui->controlYWidget->hide();
	ui->histogramWidget->hide();

	ui->scrollArea->setWidgetResizable(true);
	ui->scrollArea->verticalScrollBar()->installEventFilter(this);
	ui->scrollArea->horizontalScrollBar()->installEventFilter(this);
}

QTransferFunctionCanvas2D::~QTransferFunctionCanvas2D() {
	delete ui;
}

auto QTransferFunctionCanvas2D::setCurProjPath(const QString& path) -> void {
    d->cur_proj_path = path;
	QDir dir(path);
	dir.cdUp();

	auto fullpath = "border-image:url(" + dir.path() + "/Background.tif) 0 0 0 0 stretch stretch";
		
	ui->CanvasWidget->setStyleSheet(fullpath);
}

////////////임시 구현 내용////////////////////////
//////모두 외부로 빼서 Dependency를 없에시오//////
auto QTransferFunctionCanvas2D::setHidden(HiddenScene* hiddenScene) -> void {
	d->hiddenScene = hiddenScene;
}


auto QTransferFunctionCanvas2D::ChangeHistogram(const int& type)->void {
	QDir dir(d->cur_proj_path);
	dir.cdUp();

    if (0 == type) {
		auto fullpath = "border-image:url(" +  dir.path() + "/Background.tif) 0 0 0 0 stretch stretch";
		ui->CanvasWidget->setStyleSheet(fullpath);
	}
	else {
		auto colfullpath = "border-image:url(" + dir.path() + "/ColorBackground.tif) 0 0 0 0 stretch stretch";
		ui->CanvasWidget->setStyleSheet(colfullpath);
	}
}

auto QTransferFunctionCanvas2D::AddItem(const double& minX, const double& maxX,
										const double& minY, const double& maxY,
										const int& r, const int& g, const int& b,
										const double& transparency)->void {
	ui->Canvas->addItem(minX, maxX, minY, maxY, r / 255.0, g / 255.0, b / 255.0, transparency, "default", true);
}

auto QTransferFunctionCanvas2D::ChangeItem(const int& index, const double& minX, const double& maxX,
	                                       const double& minY, const double& maxY, const double& transparency)->void {
	Q_UNUSED(index)
	ui->Canvas->updateCurItemVal(minX, maxX, minY, maxY, transparency);
}

auto QTransferFunctionCanvas2D::DeleteAllItem()->void {
	ui->Canvas->deleteAll();
}

///위 임시함수들은 확인 후 영구히 삭제 될것임////////
///////////////////////////////////////////////////

void QTransferFunctionCanvas2D::updateHistogram() {	
	QDir dir(d->cur_proj_path);
	dir.cdUp();
		
    if(ui->grayscaleRadio->isChecked()) {
		auto fullpath = "border-image:url(:" +  dir.path() + "/Background.tif) 0 0 0 0 stretch stretch";
		ui->CanvasWidget->setStyleSheet(fullpath);
    }else {
		auto colfullpath = "border-image:url(:" + dir.path() + "/ColorBackground.tif) 0 0 0 0 stretch stretch";
		ui->CanvasWidget->setStyleSheet(colfullpath);
    }
}

void QTransferFunctionCanvas2D::canvasMousePressed() {
	emit sigCanvasMouse(true);
}

void QTransferFunctionCanvas2D::canvasMouseReleased() {
	emit sigCanvasMouse(false);
}

void QTransferFunctionCanvas2D::minYChanged(double val) {
	int idx = ui->Canvas->getCurItemIdx();	
	if (ui->Canvas->getCurItemIdx() != -1) {
		double prev_xMin = ui->Canvas->getMins()[idx];
		double prev_xMax = ui->Canvas->getMaxs()[idx];
		double prev_trans = ui->Canvas->getTrans()[idx];
	    double prev_yMax = ui->Canvas->getYMaxs()[idx];
		double new_yMin = toYorigin(val);
		ui->Canvas->updateCurItemVal(prev_xMin, prev_xMax, new_yMin, prev_yMax, prev_trans);
	}
}

void QTransferFunctionCanvas2D::maxYChanged(double val) {	
	int idx = ui->Canvas->getCurItemIdx();	
	if (ui->Canvas->getCurItemIdx() != -1) {
		double prev_xMin = ui->Canvas->getMins()[idx];
		double prev_xMax = ui->Canvas->getMaxs()[idx];
		double prev_trans = ui->Canvas->getTrans()[idx];
		double new_yMax = toYorigin(val);
		double prev_yMin = ui->Canvas->getYMins()[idx];
		ui->Canvas->updateCurItemVal(prev_xMin, prev_xMax, prev_yMin, new_yMax, prev_trans);
	}
}

void QTransferFunctionCanvas2D::maxXChanged(double val) {
	int idx = ui->Canvas->getCurItemIdx();
	if (ui->Canvas->getCurItemIdx() != -1) {
		double new_xMax = toXorigin(val);
		double prev_xMin = ui->Canvas->getMins()[idx];
		double prev_trans = ui->Canvas->getTrans()[idx];
		double prev_yMin = ui->Canvas->getYMins()[idx];
		double prev_yMax = ui->Canvas->getYMaxs()[idx];
		ui->Canvas->updateCurItemVal(prev_xMin, new_xMax, prev_yMin,prev_yMax ,prev_trans);
	}
}

void QTransferFunctionCanvas2D::minXChanged(double val) {
	int idx = ui->Canvas->getCurItemIdx();
	if (ui->Canvas->getCurItemIdx() != -1) {
		double prev_xMax = ui->Canvas->getMaxs()[idx];
		double new_xMin = toXorigin(val);
		double prev_trans = ui->Canvas->getTrans()[idx];
		double prev_yMin = ui->Canvas->getYMins()[idx];
		double prev_yMax = ui->Canvas->getYMaxs()[idx];
		ui->Canvas->updateCurItemVal(new_xMin, prev_xMax,prev_yMin,prev_yMax, prev_trans);
	}
}

void QTransferFunctionCanvas2D::opacityChanged(double val) {
	int idx = ui->Canvas->getCurItemIdx();
	if (ui->Canvas->getCurItemIdx() != -1) {
		double prev_xMax = ui->Canvas->getMaxs()[idx];
		double prev_xMin = ui->Canvas->getMins()[idx];
		double new_trans = val; //(1.0-val?)
		double prev_yMax = ui->Canvas->getYMaxs()[idx];
		double prev_yMin = ui->Canvas->getYMins()[idx];
		ui->Canvas->updateCurItemVal(prev_xMin, prev_xMax,prev_yMin,prev_yMax, new_trans);
	}
}

auto QTransferFunctionCanvas2D::setDataYSingleStep(double step) -> void {
	d->single_stepY = step;
	ui->yMinSpin->setSingleStep(step);
	ui->yMaxSpin->setSingleStep(step);

	ui->Canvas->setYSingleStep(step);
}

auto QTransferFunctionCanvas2D::setDataXSingleStep(double step) -> void {
	d->single_stepX = step;
    ui->xMinSpin->setSingleStep(step);
	ui->xMaxSpin->setSingleStep(step);

	ui->Canvas->setXSingleStep(step);
}
void QTransferFunctionCanvas2D::updatePannel() {
	int idx = ui->Canvas->getCurItemIdx();

	double cur_min = 0.0;
	double cur_max = 0.0;
	double cur_trans = 0.0;
	double cur_ymax = 0.0;
	double cur_ymin = 0.0;

	if (idx != -1) {
		cur_min = ui->Canvas->getMins()[idx];
		cur_max = ui->Canvas->getMaxs()[idx];
		cur_trans = ui->Canvas->getTrans()[idx];
		cur_ymax = ui->Canvas->getYMaxs()[idx];
		cur_ymin = ui->Canvas->getYMins()[idx];

		ui->opacitySpin->blockSignals(true);
		ui->opacitySpin->setValue(cur_trans);
		ui->opacitySpin->blockSignals(false);

		ui->xMaxSpin->blockSignals(true);
		ui->xMaxSpin->setValue(toXpresent(cur_max));
		ui->xMaxSpin->blockSignals(false);

		ui->xMinSpin->blockSignals(true);
		ui->xMinSpin->setValue(toXpresent(cur_min));
		ui->xMinSpin->blockSignals(false);

		ui->yMaxSpin->blockSignals(true);
		ui->yMaxSpin->setValue(toYpresent(cur_ymax));
		ui->yMaxSpin->blockSignals(false);

		ui->yMinSpin->blockSignals(true);
		ui->yMinSpin->setValue(toYpresent(cur_ymin));
		ui->yMinSpin->blockSignals(false);
	}

	emit sigSelectItem(idx, cur_min, cur_max, cur_ymin, cur_ymax, cur_trans);
}

auto QTransferFunctionCanvas2D::setDataYname(QString name) -> void {
	d->nameY = name;
	ui->nameYLabel->setText(name);
}


auto QTransferFunctionCanvas2D::setDataXname(QString name) -> void {
	d->nameX = name;
	ui->nameLabel->setText(name);
}

auto QTransferFunctionCanvas2D::setDataYdecimation(int dec) -> void {
	d->decimalY = dec;
	ui->yMaxSpin->setDecimals(dec);
	ui->yMinSpin->setDecimals(dec);
}


auto QTransferFunctionCanvas2D::setDataXdecimation(int dec) -> void {
	d->decimalX = dec;
	ui->xMaxSpin->setDecimals(dec);
	ui->xMaxSpin->setDecimals(dec);
}

auto QTransferFunctionCanvas2D::setDataYdivider(double div) -> void {
	d->dividerY = div;
}


auto QTransferFunctionCanvas2D::setDataXdivider(double div) -> void {
	d->dividerX = div;
}

auto QTransferFunctionCanvas2D::toYorigin(double yP) -> double {
	return yP * d->dividerY;
}

auto QTransferFunctionCanvas2D::toYpresent(double yO) -> double {
	return yO / d->dividerY;
}

auto QTransferFunctionCanvas2D::toXorigin(double xP)->double {
	return xP * d->dividerX;
}

auto QTransferFunctionCanvas2D::toXpresent(double xO)->double {
	return xO / d->dividerX;
}

auto QTransferFunctionCanvas2D::setDataYRange(double y_min, double y_max) -> void {
	ui->Canvas->setDataYRange(y_min, y_max);
	ui->yMinSpin->setRange(y_min/d->dividerY, y_max/d->dividerY);
	ui->yMaxSpin->setRange(y_min/d->dividerY, y_max/d->dividerY);
}

auto QTransferFunctionCanvas2D::setDataXRange(double x_min, double x_max)->void {
	ui->Canvas->setDataXRange(x_min, x_max);//real value
	ui->xMinSpin->setRange(x_min/d->dividerX, x_max/d->dividerX);//presentation value
	ui->xMaxSpin->setRange(x_min/d->dividerX, x_max/d->dividerX);
}

void QTransferFunctionCanvas2D::sendRenderCall(){	
	emit sigRender();//rendering update request signal	
	
	//building 2D TF list for Oiv Volume Render & volume Container
	std::vector<TFBox> m_TFBox;
	for(int i=0;i<getColorList().size();i++) {
		TFBox new_item;
		new_item.min_val = getMinList()[i];		
		new_item.max_val = getMaxList()[i];
		new_item.grad_min = getGradMinList()[i];
		new_item.grad_max = getGradMaxList()[i];
		new_item.color[0] = getColorList()[i].redF();
		new_item.color[1] = getColorList()[i].greenF();
		new_item.color[2] = getColorList()[i].blueF();
		new_item.opacity = getTransparencyList()[i];
		new_item.hidden = getHiddenList()[i];
		m_TFBox.push_back(new_item);
	}
	//Volume Container (Volume Rendering) Scene Graph에 갱신된 TF 정보 전달	
	if(d->hiddenScene) {
		d->hiddenScene->Update2DTF(m_TFBox);
	}	
}

auto QTransferFunctionCanvas2D::getColorList() -> std::vector<QColor> {
	return ui->Canvas->getColors();
}

auto QTransferFunctionCanvas2D::getGradMaxList() -> std::vector<double> {
	return ui->Canvas->getYMaxs();
}

auto QTransferFunctionCanvas2D::getGradMinList() -> std::vector<double> {
	return ui->Canvas->getYMins();
}


auto QTransferFunctionCanvas2D::getMaxList() -> std::vector<double> {
	return ui->Canvas->getMaxs();
}

auto QTransferFunctionCanvas2D::getMinList()->std::vector<double> {
	return ui->Canvas->getMins();
}

auto QTransferFunctionCanvas2D::getTransparencyList() -> std::vector<double> {
	return ui->Canvas->getTrans();
}
auto QTransferFunctionCanvas2D::getHiddenList() -> std::vector<bool> {
	return ui->Canvas->getHidden();
}
auto QTransferFunctionCanvas2D::Zoom(double factor)->void {
	ui->CanvasWidget->setFixedSize(ui->CanvasWidget->size().width() * factor,
		                           ui->CanvasWidget->size().height() * factor);

	adjustScrollBar(ui->scrollArea->horizontalScrollBar(), factor);
	adjustScrollBar(ui->scrollArea->verticalScrollBar(), factor);
}

auto QTransferFunctionCanvas2D::eventFilter(QObject* object, QEvent* event) -> bool {
	if (event->type() == QEvent::Wheel) {
		auto wheelEvent = static_cast<QWheelEvent*>(event);
		if (QApplication::keyboardModifiers() == Qt::ControlModifier) {
			double factor = 1.0;
			if (0 < wheelEvent->angleDelta().y() / WHEEL_DELTA) {
				factor = 1.1;
			}
			else {
				factor = 0.9;
			}

			Zoom(factor);

			return true;
		}
	}

	Q_UNUSED(object)

	return false;
}

auto QTransferFunctionCanvas2D::adjustScrollBar(QScrollBar* scrollBar, double factor)->void {
	scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep() / 2)));
}

auto QTransferFunctionCanvas2D::setCurrentItem(int index)->void {
	ui->Canvas->setCurItemIdx(index);
}