#include <iostream>

#include "QRectItem2D.h"
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

#define PIE 3.1415926535897932384626433832795

struct QRectItem2D::Impl {
	QRectF m_BoundingRect;

	QRectF m_ActualRect;

	//! Resizable handles around the shape
	QVector<QRectF> m_ResizeHandles;

	bool m_IsResizing;

	bool m_MousePressed;

	InnerPosition m_ResizeCorner;

	QPen pens;
	QBrush brushes;
	QPointF oldPos;
	QRectF oldRect;

	QCursor* norCursor = nullptr;
	QCursor* moveCursor = nullptr;
	QCursor* sizeVerCursor = nullptr;
	QCursor* sizeHorCursor = nullptr;
	QCursor* sizeBDCursor = nullptr;
	QCursor* sizeFDCursor = nullptr;

	bool hide{ false };
};

auto QRectItem2D::getHidden() -> bool {
	return d->hide;
}

auto QRectItem2D::toggleHidden() -> void {
	d->hide = !d->hide;
	update();
}


auto QRectItem2D::setRect(QRectF rect) -> void {
	d->m_BoundingRect = rect;

}
auto QRectItem2D::setPen(QPen pen)->void {
	d->pens = pen;
}
auto QRectItem2D::setBrush(QBrush brush)->void {
	d->brushes = brush;
}

auto QRectItem2D::rect()->QRectF {
	return mapRectToParent(d->m_BoundingRect);
}
auto QRectItem2D::getColor() -> QColor {
	return d->brushes.color();
}

QRectItem2D::QRectItem2D() :
	d(new Impl())
{
	d->m_BoundingRect = QRectF(0, 0, 200, 200);
	d->m_ResizeHandles.fill(QRect(0, 0, 0, 0), 8); //initially empty handles
	d->m_MousePressed = false;
	d->m_IsResizing = false;
	setFlags(QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges);
	setFiltersChildEvents(true);
	setPos(0, 0);

	d->norCursor = new QCursor(Qt::CursorShape::ArrowCursor);
	d->moveCursor = new QCursor(Qt::CursorShape::SizeAllCursor);
	d->sizeVerCursor = new QCursor(Qt::CursorShape::SizeVerCursor);
	d->sizeHorCursor = new QCursor(Qt::CursorShape::SizeHorCursor);
	d->sizeBDCursor = new QCursor(Qt::CursorShape::SizeBDiagCursor);
	d->sizeFDCursor = new QCursor(Qt::CursorShape::SizeFDiagCursor);
}

QRectF QRectItem2D::boundingRect() const
{
	return d->m_BoundingRect;
}
QVariant QRectItem2D::itemChange(GraphicsItemChange change, const QVariant& value) {

	return QGraphicsItem::itemChange(change, value);
}
void QRectItem2D::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	Q_UNUSED(option)
		Q_UNUSED(widget)


		QPen pen = painter->pen();
	if(d->hide) {
		pen.setStyle(Qt::DashLine);
	}else {
		pen.setStyle(Qt::SolidLine);
	}
	painter->setPen(pen);
    //painter->fillRect(boundingRect(), d->brushes);
	painter->drawRect(boundingRect());
	painter->fillRect(boundingRect(), d->brushes);

	//resize handles
	double scale = this->scene()->views().at(0)->transform().m11(); //get current sace factor
	float rectSize = 6 / scale; //this is to maintain same size for resize handle rects
	QRectF handles(0, 0, rectSize, rectSize);
	QRectF m_CornerRect = d->m_BoundingRect.adjusted(1, 1, -1, -1);

	handles.moveCenter(m_CornerRect.topLeft());  //TopLeft
	d->m_ResizeHandles.replace(0, handles);

	handles.moveCenter(m_CornerRect.topRight()); //TopRight
	d->m_ResizeHandles.replace(2, handles);

	handles.moveCenter(m_CornerRect.bottomRight());  //BottomRight
	d->m_ResizeHandles.replace(4, handles);

	handles.moveCenter(m_CornerRect.bottomLeft()); //BottomLeft
	d->m_ResizeHandles.replace(6, handles);

	QPointF center(m_CornerRect.center().x(), m_CornerRect.top()); //Top
	handles.moveCenter(center);
	d->m_ResizeHandles.replace(1, handles);

	center = QPointF(m_CornerRect.right(), m_CornerRect.center().y()); //Right
	handles.moveCenter(center);
	d->m_ResizeHandles.replace(3, handles);

	center = QPointF(m_CornerRect.center().x(), m_CornerRect.bottom());  //Bottom
	handles.moveCenter(center);
	d->m_ResizeHandles.replace(5, handles);

	center = QPointF(m_CornerRect.left(), m_CornerRect.center().y());  //Left
	handles.moveCenter(center);
	d->m_ResizeHandles.replace(7, handles);


	//pens.setCosmetic(true); //to maintain same width of pen across zoom levels
	//draw rect
	//painter->setPen(pens);
	//painter->drawRect(m_CornerRect);

	//draw arrow handle
	//QPen arrowPen;
	//arrowPen.setCosmetic(true);
	//arrowPen.setColor(Qt::yellow);
	//painter->setBrush(Qt::black);
	//painter->setPen(arrowPen);   

	//draw resize handles
	if (this->isSelected()) {
		QPen handlePen;
		handlePen.setColor(Qt::black);
		painter->setBrush(Qt::green);
		painter->setPen(handlePen);
		for (int i = 0; i < d->m_ResizeHandles.size(); i++) {
			painter->drawEllipse(d->m_ResizeHandles[i]);
		}
	}
}

void QRectItem2D::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	d->oldPos = event->pos();
	d->oldRect = boundingRect();
	d->m_MousePressed = true;
	d->m_IsResizing = mousePosOnHandles(event->scenePos()); //to check event on corners or not
	//this->setSelected(true);
	if (d->m_IsResizing)
	{
		d->m_ActualRect = d->m_BoundingRect;
	}
	QGraphicsItem::mousePressEvent(event);
}

void QRectItem2D::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	if (d->m_IsResizing)
	{
		QPointF ptMouseMoveInItemsCoord = mapFromScene(event->scenePos());
		switch (d->m_ResizeCorner)
		{
		case InnerPosition::TOP_LEFT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setTopLeft(ptMouseMoveInItemsCoord);
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::TOP:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setTop(ptMouseMoveInItemsCoord.y());
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::TOP_RIGHT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setTopRight(ptMouseMoveInItemsCoord);
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::RIGHT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setRight(ptMouseMoveInItemsCoord.x());
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::BOTTOM_RIGHT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setBottomRight(ptMouseMoveInItemsCoord);
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::BOTTOM:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setBottom(ptMouseMoveInItemsCoord.y());
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::BOTTOM_LEFT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setBottomLeft(ptMouseMoveInItemsCoord);
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		case InnerPosition::LEFT:
			if (this->scene()->sceneRect().contains(event->scenePos()))
			{
				d->m_BoundingRect.setLeft(ptMouseMoveInItemsCoord.x());
				d->m_BoundingRect = d->m_BoundingRect.normalized();
			}
			break;
		}

		prepareGeometryChange();
		update();
	}
	else
	{
		QPointF ptMouseMoveInItemsCoord = mapFromScene(event->scenePos());

		qreal x_move = event->pos().x() - d->oldPos.x();
		qreal y_move = event->pos().y() - d->oldPos.y();

		d->m_BoundingRect.setLeft(d->oldRect.left() + x_move);
		d->m_BoundingRect.setRight(d->oldRect.right() + x_move);
		//d->m_BoundingRect.setBottom(0);
		//d->m_BoundingRect.setTop(-scene()->height());
		d->m_BoundingRect.setBottom(d->oldRect.bottom() + y_move);
		d->m_BoundingRect.setTop(d->oldRect.top() + y_move);

		prepareGeometryChange();
		update();
	}
}

void QRectItem2D::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	d->m_MousePressed = false;
	d->m_IsResizing = false;
	d->m_IsResizing = false;
	if (d->m_ActualRect != d->m_BoundingRect)
	{ // Rotating won't trigger this, only resizing.        
		auto oldScenePos = scenePos();
		setTransformOriginPoint(d->m_BoundingRect.center());
		auto newScenePos = scenePos();
		auto oldPos = pos();
		setPos(oldPos.x() + (oldScenePos.x() - newScenePos.x()), oldPos.y() + (oldScenePos.y() - newScenePos.y()));
	}

	setPos(x(), y());

	prepareGeometryChange();
	update();
	QGraphicsItem::mouseReleaseEvent(event);
}

void QRectItem2D::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
	Q_UNUSED(event)
}

void QRectItem2D::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
	Q_UNUSED(event)
}

void QRectItem2D::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
	Q_UNUSED(event)
}

void QRectItem2D::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{
	Q_UNUSED(event)
}

auto QRectItem2D::mousePosOnRect(QPointF pos) -> bool {
	return mapToScene(d->m_BoundingRect).containsPoint(pos, Qt::WindingFill);
}


bool QRectItem2D::mousePosOnHandles(QPointF pos)
{
	bool resizable = false;
	int rem4Index = 8;// +(qRound(this->rotation()) / 45);
	if (mapToScene(d->m_ResizeHandles[(0 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::TOP_LEFT;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(1 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::TOP;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(2 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::TOP_RIGHT;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(3 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::RIGHT;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(4 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::BOTTOM_RIGHT;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(5 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::BOTTOM;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(6 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::BOTTOM_LEFT;
		resizable = true;
	}
	else if (mapToScene(d->m_ResizeHandles[(7 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		d->m_ResizeCorner = InnerPosition::LEFT;
		resizable = true;
	}

	return resizable;
}

auto QRectItem2D::getInnerPosition(QPointF pos) const ->InnerPosition {
	InnerPosition position = InnerPosition::NONE;

	QRectF centerRect(d->m_BoundingRect.left() + 2,
		d->m_BoundingRect.top() + 2,
		d->m_BoundingRect.width() - 4,
		d->m_BoundingRect.height() - 4);

	const int rem4Index = 8;

	if (mapToScene(d->m_ResizeHandles[(0 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::TOP_LEFT;
	}
	else if (mapToScene(d->m_ResizeHandles[(1 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::TOP;
	}
	else if (mapToScene(d->m_ResizeHandles[(2 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::TOP_RIGHT;
	}
	else if (mapToScene(d->m_ResizeHandles[(3 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::RIGHT;
	}
	else if (mapToScene(d->m_ResizeHandles[(4 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::BOTTOM_RIGHT;
	}
	else if (mapToScene(d->m_ResizeHandles[(5 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::BOTTOM;
	}
	else if (mapToScene(d->m_ResizeHandles[(6 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::BOTTOM_LEFT;
	}
	else if (mapToScene(d->m_ResizeHandles[(7 + rem4Index) % 8]).containsPoint(pos, Qt::WindingFill))
	{
		position = InnerPosition::LEFT;
	}
	else if (mapToScene(centerRect).containsPoint(pos, Qt::WindingFill)) {
		position = InnerPosition::CENTER;
	}

	return position;
}
