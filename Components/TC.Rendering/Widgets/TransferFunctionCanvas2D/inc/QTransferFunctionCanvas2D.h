#pragma once

#include <QWidget>
#include <QColor>
#include <vector>
#include <QScrollBar>

#include "QRectItem2D.h"

#include "TC.Rendering.Widgets.TransferFunctionCanvas2DExport.h"

namespace Ui {
	class QCanvas2D;
}


class HiddenScene;

class TC_Rendering_Widgets_TransferFunctionCanvas2D_API QTransferFunctionCanvas2D : public QWidget
{
	Q_OBJECT
public:
	QTransferFunctionCanvas2D(QWidget* parent = nullptr);
	~QTransferFunctionCanvas2D();
		
	auto getColorList(void)->std::vector<QColor>;
	auto getMinList(void)->std::vector<double>;
	auto getMaxList(void)->std::vector<double>;
	auto getGradMinList(void)->std::vector<double>;
	auto getGradMaxList(void)->std::vector<double>;
	auto getTransparencyList(void)->std::vector<double>;
	auto getHiddenList(void)->std::vector<bool>;

	auto setDataYRange(double y_min, double y_max)->void;
	auto setDataYname(QString name)->void;
	auto setDataYdecimation(int dec)->void;
	auto setDataYSingleStep(double step)->void;
	auto setDataYdivider(double div)->void;

	auto setDataXRange(double x_min, double x_max)->void;
	auto setDataXname(QString name)->void;
	auto setDataXdecimation(int dec)->void;
	auto setDataXSingleStep(double step)->void;
	auto setDataXdivider(double div)->void;

	auto setCurrentItem(int index)->void;

	//////////////////////////////
	auto setHidden(HiddenScene* hiddenScene)->void;
	//////////////////////////////

	auto ChangeHistogram(const int& type)->void;

	auto AddItem(const double& minX, const double& maxX,
		         const double& minY, const double& maxY, 
		         const int& r, const int& g, const int& b,
		         const double& transparency)->void;
	auto ChangeItem(const int& index, const double& minX, const double& maxX, 
		            const double& minY, const double& maxY, const double& transparency)->void;

	auto DeleteAllItem()->void;

	auto Zoom(double factor)->void;
	auto eventFilter(QObject* obj, QEvent* event) -> bool override;

	auto setCurProjPath(const QString& path)->void;

public slots:
	void opacityChanged(double val);
	void minXChanged(double val);
	void maxXChanged(double val);

	void minYChanged(double val);
	void maxYChanged(double val);

	void sendRenderCall();
	void updatePannel();
	void canvasMousePressed();
	void canvasMouseReleased();

	void updateHistogram();
		
signals:
	void sigCanvasMouse(bool);
	void sigRender();	
	void sigSelectItem(int index, double min, double max, double grad_min, double grad_max, double opacity);

private:
	auto toXorigin(double xP)->double;
	auto toXpresent(double xO)->double;

	auto toYorigin(double yP)->double;
	auto toYpresent(double yO)->double;

	auto adjustScrollBar(QScrollBar* scrollBar, double factor)->void;

	struct Impl;
	std::unique_ptr<Impl> d;
	Ui::QCanvas2D* ui;
};