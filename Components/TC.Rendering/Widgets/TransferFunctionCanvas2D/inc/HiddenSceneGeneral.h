#pragma once

#include <memory>

#include <QString>
#include <QPolygonF>

#include "TC.Rendering.Widgets.TransferFunctionCanvas2DExport.h"

class SoSeparator;
class SoVolumeData;

class TC_Rendering_Widgets_TransferFunctionCanvas2D_API HiddenSceneGeneral {
public:
	HiddenSceneGeneral();
	virtual ~HiddenSceneGeneral();
	auto UpdateTF(QList<QPolygonF> tfShape, QList<QColor> tfColor, QList<bool> isHidden) -> void;
	auto SetImageParentDir(const QString& path) -> void;
	auto Calc2DHistogram(SoVolumeData* volume, bool isLDM) -> void;
	auto Calc2DHistogramFloat(SoVolumeData* volume, bool isLDM) -> void;
	auto GetSceneGraph() -> SoSeparator*;
	auto GetGradMinMax(double& min, double& max) -> void;
	auto SetDataMinMax(double min, double max) -> void;
	auto SetFilePath(const QString& path) -> void;

protected:
	auto BuildSceneGraph() -> void;

private:
	struct Impl;
	std::shared_ptr<Impl> d;
};
