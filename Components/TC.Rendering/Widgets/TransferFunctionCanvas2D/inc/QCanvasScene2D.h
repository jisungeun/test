#ifndef Q_CANVASSCENE2D_H
#define Q_CANVASSCENE2D_H

#include <memory>
#include <QGraphicsScene>

class QCanvasScene2D : public QGraphicsScene {
    Q_OBJECT
public:
    QCanvasScene2D(QWidget* parent = nullptr);
    ~QCanvasScene2D();

protected:
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};

#endif