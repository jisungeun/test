#pragma once

#include <QGraphicsView>
#include "TC.Rendering.Widgets.TransferFunctionCanvas2DExport.h"

class TC_Rendering_Widgets_TransferFunctionCanvas2D_API QCanvasViewPolygon : public QGraphicsView {
	Q_OBJECT
public:
	QCanvasViewPolygon(QWidget* parent = nullptr);
	~QCanvasViewPolygon();

	auto GetColors() -> QList<QColor>;
	auto GetPolygons() -> QList<QPolygonF>;
	auto GetHidden() -> QList<bool>;

	auto SetTransp(int idx, float transp)->void;

	auto SetHandleRadius(float radius) -> void;

	auto Clear() -> void;

signals:
	void sigUpdatePolygons();
	void sigCurPolygon(int);
	void sigMousePress();
	void sigMouseRelease();

private:
	struct Impl;
	std::unique_ptr<Impl> d;

protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void keyPressEvent(QKeyEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;
};
