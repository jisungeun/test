#pragma once

#include <QGraphicsView>
#include "TC.Rendering.Widgets.TransferFunctionCanvas2DExport.h"

class TC_Rendering_Widgets_TransferFunctionCanvas2D_API QCanvasViewRect : public QGraphicsView {
	Q_OBJECT
public:
	QCanvasViewRect(QWidget* parent = nullptr);
	~QCanvasViewRect();

	auto GetColors()->QList<QColor>;
	auto GetPolygons()->QList<QPolygonF>;
	auto GetHidden()->QList<bool>;

	auto SetTransp(int idx, float transp)->void;
	
	auto Clear() -> void;

	auto SetHandleRadius(float radius)->void;

signals:
	void sigUpdatePolygons();
	void sigMousePress();
	void sigMouseRelease();
	void sigCurPolygon(int);

private:
	struct Impl;
	std::unique_ptr<Impl> d;

protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void keyPressEvent(QKeyEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;
};