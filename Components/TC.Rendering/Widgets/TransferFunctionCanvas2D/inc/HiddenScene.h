#pragma once

#include <memory>
#include <vector>

#include <QString>

#include "TC.Rendering.Widgets.TransferFunctionCanvas2DExport.h"

class SoSeparator;
class SoVolumeData;

typedef struct TF_nodes {
	int min_val;
	int max_val;
	int grad_min;
	int grad_max;
	float opacity;
	float color[3];//r,g, b
	bool hidden;
} TFBox;

class TC_Rendering_Widgets_TransferFunctionCanvas2D_API HiddenScene {
public:
	HiddenScene();
	auto Update2DTF(std::vector<TFBox> tfToChange) -> void;
	auto SetImageParentDir(const QString& path) -> void;
	auto SetIs8Bit(bool is8bit) -> void;
	auto Calc2DHistogram(SoVolumeData* volume, bool isLDM) -> void;
	auto GetSceneGraph() -> SoSeparator*;
	auto GetGradMinMax(double& min, double& max) -> void;
	auto SetHTMinMax(double min, double max) -> void;
	auto SetFilePath(const QString& path) -> void;

protected:
	virtual ~HiddenScene();
	auto BuildSceneGraph() -> void;
	auto toScreenX(double val) -> double;
	auto toScreenY(double val) -> double;
	auto map(unsigned short value, unsigned short originalRangeMin, unsigned short originalRangeMax, unsigned short targetedRangeMin, unsigned short targetedRangeMax) -> unsigned short;

private:
	struct Impl;
	std::shared_ptr<Impl> d;
};
