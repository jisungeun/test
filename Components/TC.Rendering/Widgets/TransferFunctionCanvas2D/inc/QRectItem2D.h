#ifndef Q_RECTITEM2D_H
#define Q_RECTITEM2D_H

#include <QGraphicsItem>

class QRectItem2D : public QGraphicsItem
{
public:
    enum class InnerPosition
    {
        NONE = -1,
        TOP_LEFT = 1,
        TOP,
        TOP_RIGHT,
        RIGHT,
        BOTTOM_RIGHT,
        BOTTOM,
        BOTTOM_LEFT,
        LEFT,
        CENTER
    };

    QRectItem2D();

    QRectF boundingRect() const;

    auto setRect(QRectF rect)->void;
    auto setPen(QPen pen)->void;
    auto setBrush(QBrush brush)->void;
    auto rect()->QRectF;
    auto getColor()->QColor;

    auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0)->void;

    auto mousePressEvent(QGraphicsSceneMouseEvent* event)->void;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    auto hoverEnterEvent(QGraphicsSceneHoverEvent* event)->void;
    auto hoverLeaveEvent(QGraphicsSceneHoverEvent* event)->void;
    auto hoverMoveEvent(QGraphicsSceneHoverEvent* event)->void;
    auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)->void;
    auto mousePosOnHandles(QPointF pos)->bool;
    auto mousePosOnRect(QPointF pos)->bool;
    QVariant itemChange(GraphicsItemChange change, const QVariant& value);

    auto getInnerPosition(QPointF pos) const ->InnerPosition;

    auto getHidden()->bool;
    auto toggleHidden()->void;

private:
    struct Impl;
    std::unique_ptr<Impl> d;

};

#endif // Q_RECTITEM_H