#ifndef Q_CANVASVIEW2D_H
#define Q_CANVASVIEW2D_H

#include <QGraphicsView>
#include "QRectItem2D.h"

class QCanvasView2D : public QGraphicsView
{
	Q_OBJECT
public:
	QCanvasView2D(QWidget* parent = nullptr);
	~QCanvasView2D(void);

	auto setDataXRange(double x_min, double x_max)->void;
	auto setDataYRange(double y_min, double y_max)->void;
	auto getColors()->std::vector<QColor>;
	auto getMins()->std::vector<double>;
	auto getMaxs()->std::vector<double>;

	auto getYMins()->std::vector<double>;
	auto getYMaxs()->std::vector<double>;

	auto getTrans()->std::vector<double>;
	auto getHidden()->std::vector<bool>;
	auto getCurItemIdx()->int;
	auto setCurItemIdx(int index)->void;
	auto updateCurItemVal(double x_minVal, double x_maxVal,double y_minVal,double y_maxVal, double opacity)->void;
	auto accrueCurItemVal(double x_minVal, double x_maxVal, double y_minVal, double y_maxVal)->void;

	auto setXSingleStep(double step)->void;
	auto setYSingleStep(double step)->void;

	auto addItem(double x1, double x2, double y1, double y2, double r, double g, double b, double opacity, std::string name, int v)->void;
	auto deleteAll()->void;

signals:
	void sigUpdateBoxs();
	void sigUpdatePannel();
	void sigMousePress();
	void sigMouseRelease();

private:
	auto redrawBoxItems(float new_h, float new_w)->void;
	auto redrawBoxItem(QRectItem2D* item)->void;
	auto updateBoxItems()->void;
	auto toSceneX(double dataX)->double;
	auto toDataX(double SceneX)->double;

	auto toSceneY(double dataY)->double;
	auto toDataY(double SceneY)->double;

	auto getCurrentItem()->QRectItem2D*;
	auto setCurrentItem(QRectItem2D* item)->void;
	auto toggleCurrentItem(QRectItem2D* item)->void;
	auto makeRectItem(QRectF rect,bool bData = false)->void;
	auto addRectItem(double x1, double x2, double y1, double y2, double r, double g, double b, double opacity, std::string name, int v)->void;
	auto checkRectValidity(double x1, double x2)->bool;
	auto checkRectValidityY(double y1, double y2)->bool;
	auto deleteBoxItem(int idx)->void;

	auto setMouseCursor(QPointF pos)->void;

	struct Impl;
	std::unique_ptr<Impl> d;

protected:
	virtual void mouseMoveEvent(QMouseEvent* event) override;
	//virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * event) override;
	virtual void mousePressEvent(QMouseEvent* event) override;
	virtual void mouseReleaseEvent(QMouseEvent* event) override;
	virtual void keyPressEvent(QKeyEvent* event) override; 
	virtual void keyReleaseEvent(QKeyEvent* event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent* event) override;
	virtual void wheelEvent(QWheelEvent* event) override;
	virtual void drawForeground(QPainter* painter, const QRectF& rect) override;
	virtual void resizeEvent(QResizeEvent* event) override;

};
#endif