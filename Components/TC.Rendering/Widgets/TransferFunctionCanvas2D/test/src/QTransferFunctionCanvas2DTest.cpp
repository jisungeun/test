#include <iostream>
#include <QApplication>
#include <QFileDialog>
#include <QOpenGLWidget>
#include <QVBoxLayout>
//#include <QOivMultiWindow.h>

#include "QOiv3DRenderWindow.h"
#include "OivTcfReader.h"
#include "QTransferFunctionCanvas2D.h"

#include <VolumeViz/nodes/SoVolumeData.h>

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>

#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Inventor/nodes/SoSwitch.h>
#include <LDM/nodes/SoMultiDataSeparator.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoOrthographicCamera.h>



void main(int argc,char** argv) {
	/*
    QApplication app(argc,argv);	
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();
	QString file_name = QFileDialog::getOpenFileName(nullptr, "Select TCF", "", "TCF (*.tcf)");

	OivTcfReader* info_reader = new OivTcfReader;
	info_reader->SetImageFilePath(file_name.toStdString());
	info_reader->ReadInfo();
	
	int cnt = info_reader->GetTimeSteps();	

	bool flExist[3] = { false, };
	if (info_reader->IsFL3dExist()) {
		for (int i = 0; i < 3; i++) {
			if (info_reader->IsFL3dChExist(i)) {
				flExist[i] = true;			
			}
		}
	}

	OivVolumeContainer* volCon = new OivVolumeContainer;

	volCon->setFLExist(flExist[0], flExist[1], flExist[2]);

	//volCon->buildHTVolume(2, cnt);
	volCon->buildHTVolumeB(2, cnt);

	//volCon->buildFLVolume(1, cnt);
	volCon->buildFLVolumeB(1, cnt);


	float offset = 0.0;

	for (int i = 0; i < cnt; i++) {
		OivTcfReader* single_reader = new OivTcfReader;
		single_reader->SetImageFilePath(file_name.toStdString());		
		single_reader->ReadSingleTcfData(i);
		//volCon->setSingleHTVolume(single_reader->GetSingle3DHTData(), i);
		bool fl_exist = false;
		for (int ch = 0; ch < 3; ch++) {
			if (single_reader->IsFL3dChExist(ch)) {
				//volCon->setSingleFLVolume(single_reader->GetSingle3DFLData(ch), ch, i);
				fl_exist = true;
			}
		}
		if (i == 0 && fl_exist) {
			offset = single_reader->getOffsetZ();
			//volCon->setFLOffset(single_reader->getOffsetZ());//IMPORTANT after reader read some channel data
		}
	}
	
	SoSeparator* mRenRoot = new SoSeparator;
	mRenRoot->addChild(new SoPerspectiveCamera);
	mRenRoot->addChild(volCon->getHT3DRenderRoot());

	QOiv3DRenderWindow* m_RenderWindow = new QOiv3DRenderWindow(nullptr);
	m_RenderWindow->setMinimumSize(400, 400);

	m_RenderWindow->setSceneGraph(mRenRoot);

	m_RenderWindow->viewAll();

	m_RenderWindow->show();
	
	QTransferFunctionCanvas2D* tfCanvas = new QTransferFunctionCanvas2D(nullptr);
	double min, max;
	volCon->getHTMinMax(min, max);
	tfCanvas->setDataXdecimation(4);
	tfCanvas->setDataXSingleStep(0.0001);
	tfCanvas->setDataXdivider(10000.0);
	tfCanvas->setDataXname("RI");
	tfCanvas->setDataXRange(min, max);

	tfCanvas->setDataYdecimation(2);
	tfCanvas->setDataYSingleStep(0.05);
	tfCanvas->setDataYdivider(1.0);
	tfCanvas->setDataYname("Gradient");

	double gmin, gmax;
	volCon->getGradMinMax(gmin, gmax);
	tfCanvas->setDataYRange(gmin, gmax);

	tfCanvas->setVolCon(volCon);
	tfCanvas->setRenWin3D(m_RenderWindow);

	tfCanvas->show();

	OivSliceContainer* sliceCon = new OivSliceContainer;

	SoSeparator* m_sliceRoot[3];
	
	for (int i = 0; i < 3; i++) {
		m_sliceRoot[i] = new SoSeparator;
		m_sliceRoot[i]->addChild(new SoOrthographicCamera);			
	}

	sliceCon->setFLExist(flExist[0], flExist[1], flExist[2]);

	for (int i = 0; i < cnt; i++) {
		sliceCon->setSharedInherit(volCon->getSharedInheritHT(i), i);
		//sliceCon->setSharedSeparator(volCon->getSharedSeparatorHT(i), i);
		sliceCon->setSharedSeparator(volCon->getSharedSeparator2D(i), i,1);//changed
		//getSharedMds --> getSharedSeparator2D
		//addtional variable for the transfer function type
		
		sliceCon->setSharedInheritFL(volCon->getSharedInheritFL(i), i);
		sliceCon->setSharedSepartorFL(volCon->getSharedSeparatorFL(i), i);
	}//Set Volume 대신 공유 할 수 있는 노드를 공유해준다

    sliceCon->set2DHiddenSep(volCon->getHTTFHideRoot());
	//도형을 그리기 위한 hidden separator를 SliceContainer를
	//!!!구축하기전!!! 에 공유해준다

	sliceCon->buildHTSingleSlice(cnt);
	sliceCon->buildFLSingleSlice(cnt);
	sliceCon->setFLOffset(offset);
	sliceCon->initFLfs();			

	auto volData = MedicalHelper::find<SoVolumeData>(volCon->getSharedSeparator2D(0), "volData0");

	std::string win_title[3] = { "Axial Slice","Sagittal Slice","Coronal Slice" };
	
	MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
	QOiv2DRenderWindow* m_SliceWindow[3];
	for (int i = 0; i < 3; i++) {
		m_SliceWindow[i] = new QOiv2DRenderWindow(nullptr);
		m_SliceWindow[i]->setMinimumSize(400, 400);		
		m_sliceRoot[i]->addChild(sliceCon->getHT2DRenderRoot(i));
		m_SliceWindow[i]->setSceneGraph(m_sliceRoot[i]);
		MedicalHelper::orientView(ax[i], m_SliceWindow[i]->getCamera(), volData);
		//if (i > 0) m_SliceWindow[i]->viewAll();
		m_SliceWindow[i]->setWindowTitle(win_title[i].c_str());
		m_SliceWindow[i]->show();
	}
	tfCanvas->setSliceCon(sliceCon);	
	
	app.exec();
	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	*/
	//Deprecated test 21.07.29 Jose T. Kim
}