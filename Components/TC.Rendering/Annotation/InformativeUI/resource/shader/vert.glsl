//!oiv_include <Inventor/oivShapeAttribute.h>
//!oiv_include <Inventor/oivShaderState.h>
//!oiv_include <Inventor/oivShaderVariables.h>

// Bump-mapping
varying vec3 eNormal;       // Normal in eye space
varying vec3 view;
varying vec3 mvLightv;       // Vertex to light vector in eye space

void main()
{
  // Output vertex position
  gl_Position = OivModelViewProjectionMatrix() * OivVertexPosition();

  vec4 ePosition      = OivModelViewMatrix() * OivVertexPosition();   // Position in eye space
  vec4 eLightPosition = OivLightSourcePosition(0);       // Light position in eye space
       eNormal        = OivNormalMatrix() * OivVertexNormal();      // Normal in eye space

 
  mat3 mvi = mat3(OivModelViewMatrixInverse()[0].xyz, 
                 OivModelViewMatrixInverse()[1].xyz,
                  OivModelViewMatrixInverse()[2].xyz);
  mvLightv = mvi*OivLightSourcePosition(0).xyz;
  mvLightv = normalize(mvLightv);
        
  view = -ePosition.xyz;

  //OivSetTexCoord(0, OivVertexTextureCoordinate(0));
  vec2 coord = vec2(dot(OivVertexPosition(), OivObjectPlaneS(0)),
	                  dot(OivVertexPosition(), OivObjectPlaneT(0)));
  OivSetTexCoord(0, vec4(coord, 0.0, 0.0));
}
