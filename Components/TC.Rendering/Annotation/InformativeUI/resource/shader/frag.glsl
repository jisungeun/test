//!oiv_include <Inventor/oivShaderState.h>
//!oiv_include <Inventor/oivShaderVariables.h>

//!oiv_include <Inventor/oivDepthPeeling_frag.h>
uniform sampler2D colormap;
uniform sampler2D indexedTex; 

varying vec3 eNormal;       // Normal in eye space
varying vec3 view; 
varying vec3 mvLightv;       // Vertex to light vector in eye space

uniform float depth;
uniform float dimTexture;

void main()
{
  vec3 feNormal       = eNormal;

    // Compute reflected ray
    mat3 normalMatrixInverse = 
    mat3(OivModelViewMatrix()[0][0], OivModelViewMatrix()[1][0], OivModelViewMatrix()[2][0], 
         OivModelViewMatrix()[0][1], OivModelViewMatrix()[1][1], OivModelViewMatrix()[2][1],
         OivModelViewMatrix()[0][2], OivModelViewMatrix()[1][2], OivModelViewMatrix()[2][2]);

    
    vec2 newTexCoord = OivFragmentTexCoord(0).st;

    feNormal = normalize(feNormal);
    
    float diffuseCoeff;
    float specularCoeff;

      float Hg, Ha, Hr;
      float decalOnePixel = 1.0/dimTexture;
      Hg = texture2D(indexedTex, newTexCoord).r;                            // height of the given texel
      vec4 indColor = texture2D(colormap, vec2(Hg, 0));
      Ha = texture2D(indexedTex, newTexCoord + vec2(0.0, decalOnePixel)).r; // height of the texel directly above the given texel
      Hr = texture2D(indexedTex, newTexCoord + vec2(decalOnePixel, 0.0)).r; // height of the texel directly to the right of the given texel

      vec3 tNormal; // normal in texture space
      tNormal = vec3(depth*(Hg-Hr), depth*(Hg-Ha), 1.0);
      tNormal = normalize(tNormal);

      // the following code are far-fetched computations to get an estimation of the direction of 
      // the s direction (evolution of the s texture coordinate) in eye space, to get a consistent tangent system
      vec2 deTangent    = vec2(dFdx(newTexCoord.s),  dFdy(newTexCoord.s));
      vec2 deZ          = vec2(dFdx(gl_FragCoord.z), dFdy(gl_FragCoord.z));
      vec3 tempeTangent = vec3(deTangent, deTangent.x * -deZ.x + deTangent.y * -deZ.y);
      vec3 temp         = cross(tempeTangent, feNormal);
      vec3 eTangent     = cross(feNormal, temp);

      vec3 otNormal  = normalize(OivNormalMatrix() * tNormal);

      eTangent  = normalize(eTangent);
      vec3 eBinormal = cross(feNormal, eTangent);

      vec3 tLightv2; // vertex to light vector in texture space
      tLightv2.x = dot(mvLightv, eTangent);
      tLightv2.y = dot(mvLightv, eBinormal);
      tLightv2.z = dot(mvLightv, feNormal);
      tLightv2   = normalize(tLightv2);

     
      vec3 tView;
      tView.x = dot(view, eTangent);
      tView.y = dot(view, eBinormal);
      tView.z = dot(view, feNormal);
      tView   = normalize(tView);
      
      //double face lighting
      tNormal = -faceforward(tNormal, tView, tNormal);
      diffuseCoeff  = max(dot(tNormal, tLightv2), 0.0);

      
      vec3 H = -reflect(normalize(tView), tNormal);
      specularCoeff = max(dot(tLightv2, normalize(H)), 0.);
      
       // Computation of final colors
    vec4 diffuseColor, reflectedColor, specularColor;
    diffuseColor  = indColor
                  * OivFrontMaterialDiffuse()
                  * OivLightSourceDiffuse(0)
                  * vec4(diffuseCoeff);

    specularColor = vec4(pow(specularCoeff, OivFrontMaterialShininess()))*OivFrontMaterialSpecular();

    //vec4 color = diffuseColor + specularColor;
    vec4 color = indColor;
    color.w = indColor.w*OivFrontMaterialDiffuse().w;
    OivDepthPeelingOutputColor(color);
}
