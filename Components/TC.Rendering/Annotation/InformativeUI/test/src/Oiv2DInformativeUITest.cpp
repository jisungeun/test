#include <iostream>

#include <QApplication>
#include <QBoxLayout>
#include <QOiv2DRenderWindow.h>
#include <QPushButton>

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoAnnoText3.h>
#include <Inventor/nodes/SoAnnoText3Property.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>
#include <Inventor/draggers/SoScale2Dragger.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#include <Inventor/nodes/SoSwitch.h>

#include "OivScalarBar.h"
#include "Oiv2DScaler.h"
#include "Oiv2DTranslator.h"


//Informative UI with translator & scaler

QOiv2DRenderWindow* renderWindow;

Oiv2DTranslator* translator;
Oiv2DScaler* scaler;
OivScalarBar* scalar;
SoScale2Dragger* scale2D;
SoTranslate2Dragger* trans2D;
SoSwitch* manipulatorSwitch;
SoSeparator* tfSep;
SoSeparator* scalarRoot;


SoTranslation* trans;
SoScale* scale;

int dragger_id = -1;

static void toggleDragger() {
	dragger_id++;
	dragger_id %= 2;

	//create Dragger and attach to current scalar bar
	if(dragger_id == 0) {//translator
		auto new_trans = new SoTranslate2Dragger;
		new_trans->translation.setValue(trans->translation.getValue());

		auto vp = renderWindow->getViewportRegion();
		SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
		action->apply(scalarRoot->getChild(2));
		auto bbox = action->getBoundingBox();

		auto fs = new SoFaceSet;
		SoVertexProperty* vertices = new SoVertexProperty;
		fs->vertexProperty = vertices;
		vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
		vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
		vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
		vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));

		auto handleSep = new SoSeparator;

		auto handleMatl = new SoMaterial;

		auto transScale = new SoScale;
		transScale->scaleFactor.setValue(scale->scaleFactor.getValue());

		handleMatl->transparency.setValue(1.0);

		handleSep->addChild(handleMatl);
		handleSep->addChild(transScale);
		handleSep->addChild(fs);

		new_trans->setPart("translatorActive", handleSep);
		new_trans->setPart("translator", handleSep);

		trans->translation.connectFrom(&new_trans->translation);		
		manipulatorSwitch->replaceChild(0, new_trans);
		manipulatorSwitch->addChild(handleSep);

	}else if(dragger_id == 1) {//scaler
		auto new_scale = new SoScale2Dragger;
		new_scale->scaleFactor.setValue(scale->scaleFactor.getValue());		

		auto vp = renderWindow->getViewportRegion();
		SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
		action->apply(scalarRoot->getChild(2));
		auto bbox = action->getBoundingBox();

		auto fs = new SoFaceSet;
		SoVertexProperty* vertices = new SoVertexProperty;
		fs->vertexProperty = vertices;
		vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
		vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
		vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
		vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));

		auto handleSep = new SoSeparator;
		
		auto handleMatl = new SoMaterial;

		handleMatl->transparency.setValue(1.0);

		auto transScale = new SoTranslation;
		transScale->translation.setValue(trans->translation.getValue());

		handleSep->addChild(handleMatl);						
		handleSep->addChild(fs);		

		new_scale->setPart("scalerActive", handleSep);
		new_scale->setPart("scaler", handleSep);
		new_scale->setPart("feedback", new SoSeparator);
		new_scale->setPart("feedbackActive", new SoSeparator);

		auto hanSep = new SoSeparator;
		hanSep->addChild(transScale);
		hanSep->addChild(new_scale);

		scale->scaleFactor.connectFrom(&new_scale->scaleFactor);
		//manipulatorSwitch->replaceChild(1, new_scale);
		manipulatorSwitch->replaceChild(1, hanSep);
	}
	
	manipulatorSwitch->whichChild = dragger_id;	
}

void main(int argc,char** argv) {
	QApplication app(argc, argv);

	renderWindow = new QOiv2DRenderWindow(nullptr);
	renderWindow->setMinimumSize(600, 600);

	SoVolumeRendering::init();

	SoSeparator* root = new SoSeparator;
	root->ref();
		

	auto volSep = new SoSeparator;

	auto volData = new SoVolumeData;

	volData->fileName = "$OIVHOME/examples/data/Medical/files/tooth.ldm";
	volData->setName("volData0");
	volSep->addChild(volData);

	auto dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange, volData);
	volSep->addChild(dataRange);

	SoMaterial* volMatl = new SoMaterial;
	volMatl->diffuseColor.setValue(1, 1, 1);
	volSep->addChild(volMatl);

	SoTransferFunction* transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
	transFunc->setName("colormap2D_TF");
	MedicalHelper::dicomCheckMonochrome1(transFunc, volData);
	volSep->addChild(transFunc);

	int numSlices = volData->data.getSize()[MedicalHelper::AXIAL];
	SoOrthoSlice* orthoSlice = new SoOrthoSlice;
	orthoSlice->axis = MedicalHelper::AXIAL;
	orthoSlice->sliceNumber = (numSlices > 1) ? (numSlices / 2) : 0;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volSep->addChild(orthoSlice);

	

	//Scalar Bar test

	manipulatorSwitch = new SoSwitch;
	
    //translator = new Oiv2DTranslator;
	//scaler = new Oiv2DScaler;

	/*manipulatorSwitch->addChild(translator->getManipulatorRoot());
	manipulatorSwitch->addChild(scaler->getManipulatorRoot());*/
	//trans2D = new SoTranslate2Dragger;	
	//scale2D = new SoScale2Dragger;

	//auto dragTrans = new SoTranslation;
	//auto dragScale = new SoScale;


	//auto manipSep = new SoSeparator;
	//manipSep->addChild(dragTrans);
	//manipSep->addChild(dragScale);

	manipulatorSwitch->addChild(new SoSeparator);
	manipulatorSwitch->addChild(new SoSeparator);
	manipulatorSwitch->whichChild = dragger_id;

	//manipSep->addChild(manipulatorSwitch);
	

	scalar = new OivScalarBar;
	scalar->initScalarBar();
	scalar->setScale(8.0, 50.0);
	scalar->setTransferFunction(transFunc);

	double smin, smax;
	volData->getMinMax(smin, smax);
	scalar->setMinMax(smin, smax);
	/*
	auto troot = new SoSeparator;

	SoBaseColor* col = new SoBaseColor;	
	SoAnnoText3Property* prop = new SoAnnoText3Property;
	SoAnnoText3* text = new SoAnnoText3;
	SoFont* font = new SoFont;
	font->name = "Arial";
	font->size = 10;
	font->renderStyle = SoFont::RenderStyle::TEXTURE;
	col->rgb.setValue(1.0, 0.0, 0.0);
    prop->renderPrintType = SoAnnoText3Property::RenderPrintType::RENDER2D_PRINT_RASTER;
	prop->fontSizeHint = SoAnnoText3Property::FontSizeHint::ANNOTATION;

	text->string = "Test";
	//text->parts = (SoText3::FRONT | SoText3::SIDES);
	text->justification = SoAnnoText3::CENTER;

	troot->addChild(font);
	troot->addChild(col);	
	troot->addChild(prop);
	troot->addChild(text);*/

	//translator->setContentsRoot(nullptr);
	//scaler->setContentsRoot(nullptr);
	
    root->addChild(new SoOrthographicCamera);		
	root->addChild(volSep);	
	
	renderWindow->setSceneGraph(root);
	MedicalHelper::orientView(MedicalHelper::Axis::AXIAL, renderWindow->getCamera(), volData);
		
	scalarRoot = new SoSeparator;
	trans = new SoTranslation;

	auto wsize = renderWindow->getViewportRegion().getWindowSize();
	auto min = wsize[0];
	if (min > wsize[1]) min = wsize[1];

	trans->translation.setValue(min/4, min/4,0.0);
	
	scale = new SoScale;
	scale->scaleFactor.setValue(1.0, 1.0, 1.0);

	root->addChild(manipulatorSwitch);
	//root->addChild(manipSep);

	scalarRoot->addChild(trans);
	scalarRoot->addChild(scale);
	scalarRoot->addChild(scalar->getRootSceneGraph());

	//tfSep = new SoSeparator;
	//tfSep->addChild(group);
	//trans->translation.connectFrom()
	//group->addChild(translator->getManipulatorRoot());
	//group->addChild(scaler->getManipulatorRoot());
	//group->addChild(scalar->getRootSceneGraph());
	//trans->translation.connectFrom(&trans2D->translation);
	//scale->scaleFactor.connectFrom(&scale2D->scaleFactor);

	//dragTrans->translation.connectFrom(&trans2D->translation);
	//dragScale->scaleFactor.connectFrom(&scale2D->scaleFactor);

	//trans2D->setPart("translatorActive", scalar->getRootSceneGraph());
	//trans2D->setPart("translator", scalar->getRootSceneGraph());

	//scale2D->setPart("scaler", tfSep);
	//scale2D->setPart("scalerActive", tfSep);


	//root->addChild(scalar->getRootSceneGraph());
	root->addChild(scalarRoot);

	QWidget* volControl = new QWidget(nullptr);

	QVBoxLayout* layout = new QVBoxLayout;
	QPushButton* button1 = new QPushButton("&Toggle dragger", volControl);

	layout->addWidget(button1);

	QObject::connect(button1, &QPushButton::clicked, toggleDragger);
	volControl->setLayout(layout);

    renderWindow->show();
	volControl->show();

	app.exec();

	SoVolumeRendering::finish();
}