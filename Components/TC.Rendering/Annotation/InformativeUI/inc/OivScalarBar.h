#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoShaderProgram.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class TC_Rendering_Annotation_InformativeUI_API OivScalarBar {
public:
	OivScalarBar();
	~OivScalarBar();

	auto initScalarBar(int dir = 0) -> void;

	auto getRootSceneGraph(void) -> SoSeparator*;
	auto changeColorMap(int idx) -> void;
	auto setScale(float sizeX, float sizeY) -> void;
	auto setTranslation(float posX, float posY) -> void;
	auto setTransferFunction(SoTransferFunction* tf) -> void;
	auto setMinMax(float min, float max, bool original = true) -> void;
	auto setSteps(int step) -> void;
	auto setInverse(bool isInverse) -> void;

private:
	auto createFlatVolumeBuffer(void) -> SoCpuBufferObject*;
	auto createFlatVolumeBufferChar(void) -> unsigned char*;
	auto createShader(void) -> SoShaderProgram*;
	auto createPlane(void) -> SoFaceSet*;
	auto createBorder(void) -> SoFaceSet*;
	auto createBorder(float scaleX, float scaleY) -> SoFaceSet*;
	auto createAnnotation(void) -> void;

	struct Impl;
	std::unique_ptr<Impl> d;
};
