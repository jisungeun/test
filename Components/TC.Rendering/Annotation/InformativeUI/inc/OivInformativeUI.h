#pragma once

#include <QObject>
#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#pragma warning(pop)

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class TC_Rendering_Annotation_InformativeUI_API OivInformativeUI : public QObject
{
	Q_OBJECT
public:
	OivInformativeUI();
	~OivInformativeUI();

	auto GetBoundaryBoxSwitch(SbBox3f box, 
		                      SoSwitch* boundaryBoxNode, SoSwitch* axisGridNode,
		                      SbColor color = SbColor(1, 0, 0), float width = 2,
		                      int gridX = 10, int gridY = 10, int gridZ = 10)->bool;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
