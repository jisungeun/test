#pragma once

#include <QStringList>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/fields/SoSFFloat.h>
#include <Inventor/fields/SoSFInt32.h>
#include <Inventor/fields/SoSFNode.h>
#include <Inventor/fields/SoSFString.h>
#include <Inventor/fields/SoSFVec2f.h>
#pragma warning(pop)

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class SoTransferFunction;
class SoShaderProgram;
class SoFaceSet;
class SoSensor;
class SoAction;

#define COLORMAP_COUNT 5

const QStringList colorMapLabels { "Grayscale","Inverse Grayscale","Hot iron","JET","Rainbow" };

class TC_Rendering_Annotation_InformativeUI_API OivRangeBar : public SoAnnotation {
	SO_NODE_HEADER(OivRangeBar);
public:
	SoSFFloat majorlength;
	SoSFFloat ratio;	
	SoSFInt32 numOfAnnotation;
	SoSFVec2f position;
	SoSFNode trackedCamera;
	SoSFEnum orientation;
	SoSFEnum alignment;

	SoSFFloat fontSize;
	SoSFVec2f barSize;
	SoSFInt32 lastAnnotationMargin;
	SoSFBool reverseTextColor;

	enum Orientation {
		HORIZONTAL = 0,
		VERTICAL = 1
	};

	enum Alignment {
	    LEFT = 0,
		BOTTOM = 0,
		CENTER = 1,
		RIGHT = 2,
		TOP = 2
	};

	OivRangeBar();
	auto setMinMax(float min, float max, bool original)->void;
	auto setTransferFunction(SoTransferFunction* tf)->void;
	auto getTransferFunction()->SoTransferFunction*;
	auto getBorderGeometry()->SoFaceSet*;
	static void initClass();
	static void exitClass();

	auto rebuildAnnotation() const ->void;
	auto SetUpsample(int upsample = 1)->void;
	auto SetPrecision(int prec)->void;

protected:
	virtual ~OivRangeBar();

	auto buildSceneGraph()->void;
	auto initScalarBar()->void;
	
	auto createAnnotation(SoAction* action)->void;
	auto createFlatVolumeBufferChar(void)->unsigned char*;
	auto modifyFlatVolumeBuffer()->void;
	auto createShader(void)->SoShaderProgram*;
	auto createPlane(void)->SoFaceSet*;
	auto modifyPlane(void)->void;
	auto createBorder(void)->SoFaceSet*;
	auto modifyBorder(void)->void;

	static void sensorCB(void* data, SoSensor* sensor);
	static void staticCB(void* data, SoAction* action);
	void renderCB(SoAction* action);		

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};