#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/fields/SoSFFloat.h>
#include <Inventor/fields/SoSFInt32.h>
#include <Inventor/fields/SoSFNode.h>
#include <Inventor/fields/SoSFString.h>
#include <Inventor/fields/SoSFVec2f.h>
#pragma warning(pop)

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class SoSensor;
class SoAction;

class TC_Rendering_Annotation_InformativeUI_API OivMinimap : public SoAnnotation {
	SO_NODE_HEADER(OivMinimap);
public:
	SoSFFloat zoomFactor;
	SoSFFloat zoomBase;
	SoSFFloat sizeX;
	SoSFFloat sizeY;
	SoSFFloat movingX;
	SoSFFloat movingY;	

	OivMinimap();

	static void initClass();
	static void exitClass();

	auto SetWindowFullSize(int width, int height)->void;
	auto SetUpsample(int upsample = 1)->void;

protected:
	virtual ~OivMinimap();

	static void sensorCB(void* data, SoSensor* sensor);
	static void staticCB(void* data, SoAction* action);
	void renderCB(SoAction* action);

private:
	auto BuildSceneGraph()->void;

	struct Impl;
	std::unique_ptr<Impl> d;
};