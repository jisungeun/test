#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/fields/SoSFFloat.h>
#include <Inventor/fields/SoSFInt32.h>
#include <Inventor/fields/SoSFNode.h>
#include <Inventor/fields/SoSFString.h>
#include <Inventor/fields/SoSFVec2f.h>
#pragma warning(pop)

class SoAction;
class SoDrawStyle;
class SoFont;
class SoLineSet;
class SoMaterial;
class SoSeparator;
class SoText2;
class SoTranslation;
class SoVertexProperty;
class SoNodeSensor;

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class TC_Rendering_Annotation_InformativeUI_API OivScaleBar : public SoAnnotation {

    SO_NODE_HEADER(OivScaleBar);

public:
    SoSFVec2f position;
    SoSFFloat length;
    SoSFInt32 numTickIntervals;
    SoSFNode trackedCamera;
    SoSFEnum orientation;
    SoSFEnum alignment;
    SoSFString label;
    SoSFInt32 tick;

    enum Orientation {
        HORIZONTAL = 0,
        VERTICAL = 1
    };

    enum Alignment {
        LEFT = 0,
        BOTTOM = 0,
        CENTER = 1,
        RIGHT = 2,
        TOP = 2
    };

    OivScaleBar();

    static void   initClass();
    static void   exitClass();

    auto SetUpsample(int upsample = 1)->void;
    auto GetLineSep()->SoSeparator* {
        return m_lineSep.ptr();
    }
    auto GetTextSep()->SoSeparator* {
        return m_textSep.ptr();
    }

    auto GetRealLength()->float;
    auto GetLength()->int;
    void SetTextVisible(bool visible);
    bool GetTextVisible();
    void SetRealLength(const float& value);

protected:
    virtual ~OivScaleBar();

    void buildSceneGraph();
    void computeEndPoints(SbVec3f& p0, SbVec3f& p1, float len);

    SoRef<SoSeparator>      m_lineSep;
    SoRef<SoLineSet>        m_axisLineSet;
    SoRef<SoVertexProperty> m_vertProp;
    SoRef<SoDrawStyle>      m_lineStyle;

    SoRef<SoSeparator>      m_textSep;
    SoRef<SoTranslation>    m_labelPos;
    SoRef<SoFont>           m_labelFont;
    SoRef<SoText2>          m_labelText;

    bool  m_fieldsChanged; 
    float m_ndcLength;     
    SbVec3f m_p0;          
    SbVec3f m_p1;          

    int       m_tickLenPix;  
    float     m_tickLenNdc;  
    SbVec2i32 m_winSizePix;  
    SbVec2f   m_pixelPerNdc; 
    int m_upsample;
    bool isUpsample;
    double m_baseViewvol;
    
    SoNodeSensor* m_sensor; 
    static void sensorCB(void* data, SoSensor* sensor);

    static void staticCB(void* data, SoAction* action); 
    void renderCB(SoAction* action);

    void resetLines();                        
    void addLine(SbVec3f& p0, SbVec3f& p1); 

    void updateAxis();

    float m_ndcStandardLength;

    float m_realLength;
    int m_length;

    bool m_textVisible;
    float m_lengthOffset;
};
