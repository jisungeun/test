#pragma once

#include <QObject>
#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoCamera.h>
#pragma warning(pop)

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class TC_Rendering_Annotation_InformativeUI_API OivAxisGrid : public QObject {
	Q_OBJECT
public:
	OivAxisGrid();
	~OivAxisGrid();

	auto SetAxisVisibility(bool visible) -> void;
	auto GetRootSceneGraph() -> SoSeparator*;
	auto SetPhysicalSize(double x, double y, double z) -> void;
	auto GenerateBoundaryBox(SbBox3f box, SoCamera* camera, float width = 2, int gridX = 10, int gridY = 10, int gridZ = 10) -> bool;
	auto SetColor(double r, double g, double b) -> void;
	auto SetTextColor(int axis, int r, int g, int b) -> void;
	auto SetFontSize(int size) -> void;

protected:
	static void staticCB(void* data, SoSensor*);
	void renderCB(SoSensor*);

private:
	auto Init() -> void;
	auto CleanBox() -> void;
	auto ManageViewProperty(SbVec3f camPos) -> void;

	struct Impl;
	std::unique_ptr<Impl> d;
};
