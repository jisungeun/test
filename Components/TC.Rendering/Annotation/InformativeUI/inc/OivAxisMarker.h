#pragma once

#include <memory>

#include <Medical/nodes/Gnomon.h>

#include "TC.Rendering.Annotation.InformativeUIExport.h"

class TC_Rendering_Annotation_InformativeUI_API OivAxisMarker : public Gnomon {
public:
	OivAxisMarker();	

protected:
	virtual ~OivAxisMarker();	
	auto buildSceneGraph()->void;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};