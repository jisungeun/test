#include <stdio.h>
#include <tchar.h>

#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoBBox.h>
#include <Inventor/nodes/SoCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTextProperty.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoVertexProperty.h>

#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>

#include <Inventor/elements/SoViewportRegionElement.h>
#include <Inventor/elements/SoViewVolumeElement.h>

#include <Inventor/sensors/SoNodeSensor.h>

#include <Medical/InventorMedical.h>
#pragma warning(pop)

#include "OivScaleBar.h"

#include <QDebug>

SO_NODE_SOURCE(OivScaleBar);

void OivScaleBar::initClass()
{
    getClassRenderEngineMode().setRenderMode(SbRenderEngineMode::OIV_OPENINVENTOR_RENDERING);
    SO_NODE_INIT_CLASS(OivScaleBar, SoAnnotation, "Annotation");
}

void OivScaleBar::exitClass()
{
    SO__NODE_EXIT_CLASS(OivScaleBar);
}

OivScaleBar::OivScaleBar()
{
    SO_NODE_CONSTRUCTOR(OivScaleBar);
    SO_NODE_ADD_FIELD(position, (0, 0));
    SO_NODE_ADD_FIELD(length, (1));
    SO_NODE_ADD_FIELD(numTickIntervals, (10));
    SO_NODE_ADD_FIELD(trackedCamera, (NULL));
    SO_NODE_ADD_FIELD(orientation, (OivScaleBar::HORIZONTAL));
    SO_NODE_ADD_FIELD(alignment, (OivScaleBar::CENTER));
    SO_NODE_ADD_FIELD(label, (""));
    SO_NODE_ADD_FIELD(tick, (1));

    // Set up static info for enumerated type fields
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, LEFT);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, BOTTOM);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, CENTER);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, RIGHT);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, TOP);
    SO_NODE_DEFINE_ENUM_VALUE(Orientation, HORIZONTAL);
    SO_NODE_DEFINE_ENUM_VALUE(Orientation, VERTICAL);

    // Associate info for enumerated type fields
    SO_NODE_SET_SF_ENUM_TYPE(alignment, Alignment);
    SO_NODE_SET_SF_ENUM_TYPE(orientation, Orientation);

    // Hide inherited fields from IvTune.
    // It's not necessary to do this, but the SoSeparator fields are not
    // relevant to our "shape" node, so this makes it a little bit easier
    // to observe and modify in IvTune.
#if SO_INVENTOR_VERSION >= 9100
    boundingBoxCaching.setFieldType(SoField::PRIVATE_FIELD);
    renderCulling.setFieldType(SoField::PRIVATE_FIELD);
    pickCulling.setFieldType(SoField::PRIVATE_FIELD);
    fastEditing.setFieldType(SoField::PRIVATE_FIELD);
    renderUnitId.setFieldType(SoField::PRIVATE_FIELD);
#endif

    m_upsample = 1;
    isUpsample = false;
    // Set by node sensor if a field is changed, checked in renderCB
    m_fieldsChanged = false;

    // Note: m_ndcLength must be set before calling buildSceneGraph().
    //m_ndcLength = length.getValue();
    m_ndcStandardLength = 0.24f;
    m_ndcLength = m_ndcStandardLength;

    // Window size in pixels (approximate until first traversal)
    m_winSizePix.setValue(500, 500);
    // Conversion from 1 ndc unit to pixels
    m_pixelPerNdc.setValue(0.5f * (float)m_winSizePix[0], 0.5f * (float)m_winSizePix[1]);

    // Tick line length in pixels
    // Tick line length in NDC (depends on viewport so updated later)
    m_tickLenPix = 5;
    m_tickLenNdc = (float)m_tickLenPix / m_pixelPerNdc[0];// * m_upsample;

    // Create the internal scene graph
    buildSceneGraph();

    m_realLength = 0;
    m_length = 0;

    // Monitor changes to our fields
    m_sensor = new SoNodeSensor(sensorCB, (void*)this);
    m_sensor->attach(this);
    m_sensor->setPriority(0);

    m_textVisible = true;
    m_lengthOffset = 0.f;
}

OivScaleBar::~OivScaleBar()
{
    delete m_sensor;
    if (m_labelText.ptr() != NULL)
        m_labelText->string.disconnect(&(this->label));
}

///////////////////////////////////////////////////////////////////////
// Create axis scene graph
//
// Separator "AxisBoxRoot"
// +- EventCallback
// +- LightModel
// +- PickStyle
// +- DrawStyle
// +- Font
// +- Material
// +- Separator "AxisBoxLines"
// |  +- LineSet
// +- Separator "AxisBoxText"
//    +- SoSeparator (repeated for each string)
//       +- Translation
//       +- Text2
//
// Note: Do not put property nodes under the text separator.
//       Text is cleared by removing all children of this node.

auto OivScaleBar::SetUpsample(int upsample) -> void {    
    m_upsample = upsample;
    m_labelFont->size = 13.0 * upsample;
    m_lineStyle->lineWidth = 2.0 * upsample;
    isUpsample = true;
}

void OivScaleBar::SetRealLength(const float& value) {
    m_lengthOffset = value - (m_realLength - m_lengthOffset) -1.f;
}

auto OivScaleBar::GetRealLength()->float {
    return m_realLength;
}

auto OivScaleBar::GetLength()->int {
    return m_length;
}

void OivScaleBar::SetTextVisible(bool visible) {
    m_textVisible = visible;
}

bool OivScaleBar::GetTextVisible() {
    return m_textVisible;
}

void OivScaleBar::buildSceneGraph()
{
    // Event callback
    SoCallback* callbackNode = new SoCallback;
    callbackNode->setCallback(OivScaleBar::staticCB, (void*)this);
    this->addChild(callbackNode);

    SoBBox* bbox = new SoBBox();
    bbox->mode = SoBBox::NO_BOUNDING_BOX;
    this->addChild(bbox);

    SoLightModel* lmodel = new SoLightModel();
    lmodel->model = SoLightModel::BASE_COLOR;
    this->addChild(lmodel);

    SoPickStyle* pstyle = new SoPickStyle();
    pstyle->style = SoPickStyle::UNPICKABLE;
    this->addChild(pstyle);

    SoOrthographicCamera* camera = new SoOrthographicCamera();
    camera->viewportMapping = SoCamera::LEAVE_ALONE;
    this->addChild(camera);

    // Lines -------------------------------------------------------------
    m_lineSep = new SoSeparator;
    this->addChild(m_lineSep.ptr());

    // Initial geometry
    m_vertProp = new SoVertexProperty();
    computeEndPoints(m_p0, m_p1, m_ndcLength);
    m_vertProp->vertex.set1Value(0, m_p0);
    m_vertProp->vertex.set1Value(1, m_p1);

    m_lineStyle = new SoDrawStyle();
    m_lineStyle->lineWidth = 2.0 * m_upsample;
    m_lineSep->addChild(m_lineStyle.ptr());

    m_axisLineSet = new SoLineSet();
    m_axisLineSet->vertexProperty = m_vertProp.ptr();
    m_lineSep->addChild(m_axisLineSet.ptr());

    // Text -------------------------------------------------------------
    m_textSep = new SoSeparator;
    this->addChild(m_textSep.ptr());

    m_labelPos = new SoTranslation();
    m_textSep->addChild(m_labelPos.ptr());

    m_labelFont = new SoFont();
    m_labelFont->size = 13.0;
    m_labelFont->name = "Arial:Bold";
    m_labelFont->renderStyle = SoFont::TEXTURE;
    m_textSep->addChild(m_labelFont.ptr());

    SoTextProperty* textProp = new SoTextProperty();
    if (orientation.getValue() == OivScaleBar::HORIZONTAL) {
        textProp->alignmentH = SoTextProperty::RIGHT;
        textProp->alignmentV = SoTextProperty::BOTTOM;
    }
    else {
        textProp->alignmentH = SoTextProperty::LEFT;
        textProp->alignmentV = SoTextProperty::TOP;
    }
    m_textSep->addChild(textProp);

    m_labelText = new SoText2();
    m_labelText->justification = SoText2::INHERITED;
    m_textSep->addChild(m_labelText.ptr());
}

void OivScaleBar::sensorCB(void* data, SoSensor* sensor)
{
    OivScaleBar* self = (OivScaleBar*)data;
    const SoField* trigger = ((SoNodeSensor*)sensor)->getTriggerField();

    if (trigger != NULL) {
        self->m_fieldsChanged = true;
    }
}

////////////////////////////////////////////////////////////////////////
// Static method called from SoCallback node.
//
// Note: It might not be necessary to handle boundingBoxAction since
//       our bounding box is canceled out by SoBBox. But... it's usually
//       the first action we see that has valid values for the viewport,
//       which we need to compute pixel size, so keep it for now.
void OivScaleBar::staticCB(void* data, SoAction* action)
{
    OivScaleBar* self = (OivScaleBar*)data;
    if (action->isOfType(SoGLRenderAction::getClassTypeId()) ||
        action->isOfType(SoGetBoundingBoxAction::getClassTypeId())) {

        self->renderCB(action);
    }
}

void OivScaleBar::renderCB(SoAction* action)
{
    bool mustUpdateAxis = false;

    // Get values from state we need
    // (creates a dependency on these elements in the state)    
    SoState* state = action->getState();    
    const SbViewportRegion& vpregion = SoViewportRegionElement::get(state);
    const SbViewVolume& viewVol = SoViewVolumeElement::get(state);
    const SbVec2i32& winSize = vpregion.getViewportSizePixels_i32();    

    if (1 > winSize[0] || 1 > winSize[1]) {
        return;
    }
    if(m_upsample == 1) {
        m_baseViewvol = viewVol.getWidth();
    }

    auto winRate = 1.f;
    if (winSize != m_winSizePix) {
        mustUpdateAxis = true;

        winRate = (float)winSize[0] / (float)m_winSizePix[0];

        m_winSizePix = winSize;
        m_pixelPerNdc.setValue(0.5f * (float)m_winSizePix[0] , 0.5f * (float)m_winSizePix[1] );

        int horizOrVert = 1 - this->orientation.getValue();
        m_tickLenNdc = (float)m_tickLenPix / m_pixelPerNdc[horizOrVert];

        m_ndcLength /= winRate;
    }

    m_realLength = (viewVol.getWidth()* m_ndcLength / 2.f) + m_lengthOffset;
    int tempLength = std::floor(m_realLength *(float)m_upsample + 0.5f);
    m_length = (int(tempLength / tick.getValue()) + 1) * tick.getValue();
    
    QString label_text = QString::number(m_length) + " um";
    label_text.replace("u", QString::fromWCharArray(L"\x00B5"));
    if (m_textVisible) m_labelText->string = label_text.toStdWString();
    else m_labelText->string = " ";
    
    float ndcLength = (float)m_length / viewVol.getWidth() / m_baseViewvol * viewVol.getWidth() * 2.0f;
    
    computeEndPoints(m_p0, m_p1, ndcLength);

    updateAxis();    
}

///////////////////////////////////////////////////////////////////////
// Compute new end-points of scale bar based on orientation and
// alignment.  Must use the member variable m_ndcLength, not the
// length field, in case actual length is tracking a 3D distance.
void OivScaleBar:: computeEndPoints(SbVec3f& p0, SbVec3f& p1, float len)
{
    float xoffset = 0;
    float yoffset = 0;
    float xlength = 0;
    float ylength = 0;

    if (orientation.getValue() == OivScaleBar::HORIZONTAL) {
        xlength = len;
        if (alignment.getValue() == OivScaleBar::CENTER) {
            xoffset -= len / 2;
        }
        else if (alignment.getValue() == OivScaleBar::RIGHT) {
            xoffset -= len;
        }
    }
    else { // VERTICAL
        ylength = len;
        if (alignment.getValue() == OivScaleBar::CENTER) {
            yoffset -= len / 2;
        }
        else if (alignment.getValue() == OivScaleBar::TOP) {
            yoffset += 0;
        }
    }

    float x, y;
    position.getValue().getValue(x, y);
        
    x += xoffset;//-xlength
    y += yoffset;    
    p0.setValue(x, y, 0);
    p1.setValue(x + xlength , y + ylength, 0);    
}

void OivScaleBar::resetLines()
{
    m_axisLineSet->numVertices.setNum(0);
    m_vertProp->vertex.setNum(0);
}

void OivScaleBar::addLine(SbVec3f& p0, SbVec3f& p1)
{
    int numVerts = m_vertProp->vertex.getNum();
    m_vertProp->vertex.setNum(numVerts + 2);
    m_vertProp->vertex.set1Value(numVerts, p0);
    m_vertProp->vertex.set1Value(numVerts + 1, p1);

    int numLines = m_axisLineSet->numVertices.getNum();
    m_axisLineSet->numVertices.set1Value(numLines, 2);    
}

void OivScaleBar::updateAxis()
{
    m_lineSep->enableNotify(FALSE);
    resetLines();

    // Main line
    addLine(m_p0, m_p1);

    // End lines ------------------------------------------------------
    // Slightly exaggerated length looks nicer?
    SbVec3f offset(0, m_tickLenNdc, 0);
    if (this->orientation.getValue() != OivScaleBar::HORIZONTAL) {
        offset.setValue(m_tickLenNdc, 0, 0);
    }
    SbVec3f p2, p3;
    p2 = m_p0 + (1.25f * offset);
    //addLine(m_p0, p2);
    p2 = m_p1 + (1.25f * offset);
    //addLine(m_p1, p2);

    // Tick marks -----------------------------------------------------
    int numTickInt = numTickIntervals.getValue();
    if (numTickInt > 0) {
        float dist = m_ndcLength / (float)numTickInt;
        SbVec3f tickInterval(dist, 0, 0);
        if (this->orientation.getValue() != OivScaleBar::HORIZONTAL) {
            tickInterval.setValue(0, dist, 0);
        }
        for (int i = 1; i < numTickInt; ++i) {
            p2 = m_p0 + ((float)i * tickInterval);
            p3 = p2 + offset;
            addLine(p2, p3);
        }
    }
    m_lineSep->enableNotify(TRUE);

    // Label ----------------------------------------------------------
    SbVec3f labelPos;
    if (this->orientation.getValue() == OivScaleBar::HORIZONTAL) {
        labelPos = m_p1;
        //labelPos[0] += m_tickLenNdc;
    }
    else { // VERTICAL
        //float textHeight = m_labelFont->size.getValue() / m_pixelPerNdc[1]; // Cvt pixel size to ndc
        labelPos = m_p0;
        //labelPos[1] -= (textHeight + m_tickLenNdc);
    }
    m_labelPos->enableNotify(FALSE);
    m_labelPos->translation = labelPos;
    m_labelPos->enableNotify(TRUE);
}