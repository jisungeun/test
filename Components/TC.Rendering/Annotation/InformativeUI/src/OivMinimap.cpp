#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoBBox.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#include <Inventor/elements/SoViewportRegionElement.h>
#include <Inventor/elements/SoViewVolumeElement.h>

#include <Inventor/sensors/SoNodeSensor.h>

#include <Medical/InventorMedical.h>
#pragma warning(pop)

#include "OivMinimap.h"


SO_NODE_SOURCE(OivMinimap);


struct OivMinimap::Impl {
    SoSwitch* rootSwitch{ nullptr };
    SoSeparator* root{ nullptr };

    SoSeparator* borderRoot{ nullptr };
    SoMaterial* borderMatl{ nullptr };
    SoDrawStyle* borderStyle{ nullptr };
    SoFaceSet* border{ nullptr };

    SoSeparator* borderPtRoot{ nullptr };
    SoDrawStyle* borderPtStyle{ nullptr };

    SoSeparator* roiRoot{ nullptr };
    SoMaterial* roiMatl{ nullptr };
    SoDrawStyle* roiStyle{ nullptr };
    SoTranslation* roiBackTrans{ nullptr };
    SoTranslation* roiForwardTrans{ nullptr };
    SoTranslation* roiTrans{ nullptr };
    SoScale* roiScale{ nullptr };
    SoFaceSet* roi{ nullptr };
};

void OivMinimap::initClass() {
    getClassRenderEngineMode().setRenderMode(SbRenderEngineMode::OIV_OPENINVENTOR_RENDERING);
    SO_NODE_INIT_CLASS(OivMinimap, SoAnnotation, "Annotation");
}

void OivMinimap::exitClass() {
    SO__NODE_EXIT_CLASS(OivMinimap);
}

OivMinimap::OivMinimap() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivMinimap);
    SO_NODE_ADD_FIELD(zoomBase, (1))
    SO_NODE_ADD_FIELD(zoomFactor, (1))
    SO_NODE_ADD_FIELD(sizeX, (1))
    SO_NODE_ADD_FIELD(sizeY, (1))
    SO_NODE_ADD_FIELD(movingX, (0))
    SO_NODE_ADD_FIELD(movingY, (0))

#if SO_INVENTOR_VERSION >= 9100
    boundingBoxCaching.setFieldType(SoField::PRIVATE_FIELD);
    renderCulling.setFieldType(SoField::PRIVATE_FIELD);
    pickCulling.setFieldType(SoField::PRIVATE_FIELD);
    fastEditing.setFieldType(SoField::PRIVATE_FIELD);
    renderUnitId.setFieldType(SoField::PRIVATE_FIELD);
#endif

    this->BuildSceneGraph();
}

OivMinimap::~OivMinimap() {
    
}

auto OivMinimap::BuildSceneGraph() -> void {
    SoCallback* callbackNode = new SoCallback;
    callbackNode->setCallback(OivMinimap::staticCB, (void*)this);
    this->addChild(callbackNode);

    SoBBox* bbox = new SoBBox();
    bbox->mode = SoBBox::NO_BOUNDING_BOX;
    this->addChild(bbox);

    SoLightModel* lmodel = new SoLightModel();
    lmodel->model = SoLightModel::BASE_COLOR;
    this->addChild(lmodel);

    SoPickStyle* pstyle = new SoPickStyle();
    pstyle->style = SoPickStyle::UNPICKABLE;
    this->addChild(pstyle);

    SoOrthographicCamera* camera = new SoOrthographicCamera();
    camera->viewportMapping = SoCamera::ViewportMapping::ADJUST_CAMERA;
    this->addChild(camera);

    d->rootSwitch = new SoSwitch;
    this->addChild(d->rootSwitch);
    d->rootSwitch->whichChild = 0;

    d->root = new SoSeparator;
    d->rootSwitch->addChild(d->root);

    d->borderRoot = new SoSeparator;
    d->root->addChild(d->borderRoot);

    d->borderMatl = new SoMaterial;
    d->borderMatl->ambientColor.setValue(0, 194.0f / 255.0f, 211.0f / 255.0f);
    d->borderMatl->diffuseColor.setValue(0, 194.0f / 255.0f, 211.0f / 255.0f);
    d->borderMatl->transparency.setValue(0);
    d->borderRoot->addChild(d->borderMatl);

    d->borderStyle = new SoDrawStyle;    
    d->borderStyle->lineWidth = 2;    
    d->borderStyle->style = SoDrawStyle::Style::LINES;
    d->borderRoot->addChild(d->borderStyle);
        
    d->border = new SoFaceSet;    
    d->borderRoot->addChild(d->border);

    d->borderPtRoot = new SoSeparator;
    d->root->addChild(d->borderPtRoot);

    d->borderPtRoot->addChild(d->borderMatl);

    d->borderPtStyle = new SoDrawStyle;
    d->borderPtStyle->pointSize = 2;
    d->borderPtStyle->style = SoDrawStyle::Style::POINTS;
    d->borderPtRoot->addChild(d->borderPtStyle);

    d->borderPtRoot->addChild(d->border);

    d->roiRoot = new SoSeparator;
    d->root->addChild(d->roiRoot);

    d->roiMatl = new SoMaterial;
    d->roiMatl->ambientColor.setValue(0, 194.0f / 255.0f, 211.0f / 255.0f);
    d->roiMatl->diffuseColor.setValue(0, 194.0f / 255.0f, 211.0f / 255.0f);
    d->roiMatl->transparency.setValue(0.3);
    d->roiRoot->addChild(d->roiMatl);

    d->roiStyle = new SoDrawStyle;
    d->roiStyle->lineWidth = 3;    
    d->roiRoot->addChild(d->roiStyle);
    
    d->roiScale = new SoScale;
    d->roiBackTrans = new SoTranslation;
    d->roiRoot->addChild(d->roiBackTrans);
    d->roiRoot->addChild(d->roiScale);
    d->roiForwardTrans = new SoTranslation;
    d->roiRoot->addChild(d->roiForwardTrans);

    d->roiTrans = new SoTranslation;
    d->roiRoot->addChild(d->roiTrans);

    d->roi = new SoFaceSet;
    d->roiRoot->addChild(d->roi);
}

auto OivMinimap::SetUpsample(int upsample) -> void {
    d->borderStyle->lineWidth = 2 * upsample;
    d->borderPtStyle->lineWidth = 2 * upsample;
}

void OivMinimap::sensorCB(void* data, SoSensor* sensor) {
    
}

void OivMinimap::staticCB(void* data, SoAction* action) {
    auto self = (OivMinimap*)data;
    if (action->isOfType(SoGLRenderAction::getClassTypeId()) ||
        action->isOfType(SoGetBoundingBoxAction::getClassTypeId())) {

        self->renderCB(action);
    }
}

void OivMinimap::renderCB(SoAction* action) {
    //redraw minimap boxes
    if(false == zoomFactor.getValue() < zoomBase.getValue()) {
        d->rootSwitch->whichChild = -1;
        return;
    }
    d->rootSwitch->whichChild = 0;

    const auto xLen = sizeX.getValue();
    const auto yLen = sizeY.getValue();

    float xUnitLen;
    float yUnitLen;
    
    if(xLen > yLen) {
        xUnitLen = 0.25f;
        yUnitLen = 0.25f / xLen * yLen;
    }else {
        yUnitLen = 0.25f;
        xUnitLen = 0.25f / yLen * xLen;
    }

    auto vp = new SoVertexProperty;
    vp->vertex.set1Value(0, 0.95f, 0.95f, 0.0f);
    vp->vertex.set1Value(1, 0.95f - xUnitLen, 0.95f, 0.0f);
    vp->vertex.set1Value(2, 0.95f - xUnitLen, 0.95f - yUnitLen, 0.0f);
    vp->vertex.set1Value(3, 0.95f, 0.95f - yUnitLen, 0.0f);
    vp->vertex.set1Value(4, 0.95f, 0.95f, 0.0f);

    d->roiBackTrans->translation.setValue(0.95f - xUnitLen / 2.f, 0.95f - yUnitLen / 2.f, 0.0f);
    d->roiForwardTrans->translation.setValue(-0.95f + xUnitLen / 2.f, -0.95f + yUnitLen / 2.f, 0.0f);
    d->roi->vertexProperty.setValue(vp);
    d->border->vertexProperty.setValue(vp);

    const auto transX = movingX.getValue() / xLen * xUnitLen;
    const auto transY = -movingY.getValue() / yLen * yUnitLen;

    auto scale = zoomFactor.getValue() / zoomBase.getValue();
    d->roiTrans->translation.setValue(transX /scale, transY/scale, 0);    
        
    d->roiScale->scaleFactor.setValue(scale, scale, 1);
}