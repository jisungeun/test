#include <qcoreapplication.h>
#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoColorMap.h>
#include <Inventor/nodes/SoIndexedTexture2.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoAnnoText3.h>
#include <Inventor/nodes/SoAnnoText3Property.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoFont.h>
#pragma warning(pop)

#include "OivScalarBar.h"

struct OivScalarBar::Impl {
	SoSeparator* root;

	SoSeparator* scalarRoot;
	SoMaterial* matl;
	SoShaderProgram* shader;
	SoTransferFunction* tf;//from outer class
	SoColorMap* colormap;
	SoIndexedTexture2* texture;
	SoFaceSet* facet;

	//facet for borderline
	SoDirectionalLight* borderLight;
	SoSeparator* borderRoot;
	SoMaterial* borderMatl;
	SoFaceSet* border;
	SoTranslation* borderTrans;
	SoScale* borderScale;

	//annotation texts
	SoSeparator* annotations;

	SoCpuBufferObject* dummyBuffer;

	//position of scalarBar in render window coordinates
	float position_x { 0.0 };
	float position_y { 0.0 };
	SoTranslation* trans;

	//size of scalarBar in ratio term
	//will be applied to transformation of unit square
	float size_x { 1.0 };
	float size_y { 1.0 };
	SoScale* scale;

	float ori_min { 0.0 };
	float ori_max { 10000.0 };

	float scalar_min { 0.0 };
	float scalar_max { 10000.0 };

	int steps { 5 };

	bool isInverse { false };

	int direction { 0 }; //0: XY, 1: YZ, 2:XZ
};

OivScalarBar::OivScalarBar() : d { new Impl } {}

OivScalarBar::~OivScalarBar() {}

auto OivScalarBar::setTransferFunction(SoTransferFunction* tf) -> void {
	d->tf = tf;
	d->colormap->predefinedColorMap.connectFrom(&d->tf->predefColorMap);
}

auto OivScalarBar::setSteps(int step) -> void {
	d->steps = step;
	createAnnotation();
}

auto OivScalarBar::setMinMax(float min, float max, bool original) -> void {
	if (original) {
		d->ori_min = min;
		d->ori_max = max;
	}
	d->scalar_min = min;
	d->scalar_max = max;
	createAnnotation();
}

auto OivScalarBar::setInverse(bool isInverse) -> void {
	d->isInverse = isInverse;
}

auto OivScalarBar::createAnnotation() -> void {
	d->annotations->removeAllChildren();
	SoBaseColor* col = new SoBaseColor;
	SoAnnoText3Property* prop = new SoAnnoText3Property;
	SoFont* font = new SoFont;
	font->setName("ScalarFont");
	font->name = "Arial";
	font->size = d->size_x;
	font->renderStyle = SoFont::RenderStyle::TEXTURE;

	if (d->isInverse) {
		col->rgb.setValue(0.0, 0.0, 0.0);
	} else {
		col->rgb.setValue(1.0, 1.0, 1.0);
	}

	prop->renderPrintType = SoAnnoText3Property::RenderPrintType::RENDER2D_PRINT_RASTER;
	prop->fontSizeHint = SoAnnoText3Property::FontSizeHint::ANNOTATION;

	d->annotations->addChild(font);
	d->annotations->addChild(col);
	d->annotations->addChild(prop);

	for (int i = 0; i < d->steps; i++) {
		auto range = d->scalar_max - d->scalar_min;
		auto ori_ratio = (float)i / (float)d->steps;;
		float ratio;
		if (d->direction == 0)
			ratio = (float)(d->steps - i - 1) / (float)d->steps;
		else
			ratio = (float)i / (float)d->steps;
		auto val = d->scalar_min + (ori_ratio * range);
		val = val / 10000.0;
		auto text = new SoAnnoText3;
		auto qtext = QString::number(val, 'f', 4);
		text->string = qtext.toStdString();
		text->justification = SoAnnoText3::Justification::LEFT;

		auto group = new SoSeparator;
		auto trans = new SoTranslation;
		switch (d->direction) {
			case 0:
				trans->translation.setValue(d->size_x * 1.9, -d->size_y * 0.8 + ratio * d->size_y * 2.15, 0.0);
				break;
			case 1:
				trans->translation.setValue(-d->size_x * 1.9, -d->size_y * 0.9 + ratio * d->size_y * 2.15, 0.0);
				break;
			case 2:
				trans->translation.setValue(d->size_x * 1.9, -d->size_y * 0.8 + ratio * d->size_y * 2.15, 0.0);
				break;
		}
		group->addChild(trans);
		group->addChild(text);

		d->annotations->addChild(group);
	}
}

auto OivScalarBar::initScalarBar(int dir) -> void {
	//build scalar bar Scene graph
	d->root = new SoSeparator;
	d->direction = dir;

	//create Border plane
	d->borderRoot = new SoSeparator;
	d->root->addChild(d->borderRoot);
	d->borderLight = new SoDirectionalLight;


	d->borderRoot->addChild(d->borderLight);
	d->borderMatl = new SoMaterial;
	d->borderMatl->diffuseColor.setValue(1.0, 1.0, 1.0);
	d->border = createBorder();
	d->borderRoot->addChild(d->borderMatl);

	d->borderScale = new SoScale;
	d->borderRoot->addChild(d->borderScale);
	d->borderRoot->addChild(d->border);

	//create scalar bar plane
	d->scalarRoot = new SoSeparator;
	d->root->addChild(d->scalarRoot);

	d->matl = new SoMaterial;
	d->shader = createShader();

	auto group = new SoGroup;
	group->setName("shaderName");
	group->addChild(d->matl);
	group->addChild(d->shader);

	d->scalarRoot->addChild(group);

	d->colormap = new SoColorMap;
	d->colormap->predefinedColorMap = SoColorMap::TEMPERATURE;
	d->colormap->max = 255;

	d->texture = new SoIndexedTexture2;
	d->texture->wrapS = SoTexture2::CLAMP_TO_EDGE;
	d->texture->wrapT = SoTexture2::CLAMP_TO_EDGE;
	d->texture->minFilter = SoTexture2::LINEAR;
	d->texture->magFilter = SoTexture2::LINEAR;
	d->texture->override = TRUE;
	auto dummy = createFlatVolumeBufferChar();
	d->texture->imageIndex.setValue(SbVec2s(256, 256), SoSFArray2D::UNSIGNED_BYTE, dummy, SoSFArray::NO_COPY_AND_DELETE);

	d->scalarRoot->addChild(d->colormap);
	d->scalarRoot->addChild(d->texture);
	SoShapeHints* shapeHints = new SoShapeHints;
	shapeHints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
	shapeHints->shapeType = SoShapeHints::UNKNOWN_SHAPE_TYPE;
	shapeHints->creaseAngle = 40;
	d->scalarRoot->addChild(shapeHints);

	d->facet = createPlane();

	//applay translation
	d->trans = new SoTranslation;
	d->scale = new SoScale;
	d->trans->translation.setValue(d->position_x, d->position_y, 0.0);
	d->scale->scaleFactor.setValue(d->size_x, d->size_y, 1.0);

	d->scalarRoot->addChild(d->trans);
	d->scalarRoot->addChild(d->scale);

	d->scalarRoot->addChild(d->facet);

	d->annotations = new SoSeparator;
	d->root->addChild(d->annotations);
}


auto OivScalarBar::setScale(float sizeX, float sizeY) -> void {
	d->size_x = sizeX;
	d->size_y = sizeY;
	d->scale->scaleFactor.setValue(sizeX, sizeY, 1.0);
	d->borderScale->scaleFactor.setValue(sizeX + 0.1, sizeY + 0.1, 1.0);
}

auto OivScalarBar::setTranslation(float posX, float posY) -> void {
	d->position_x = posX;
	d->position_y = posY;
	d->trans->translation.setValue(posX, posY, 0.0);
}

auto OivScalarBar::createBorder(float sizeX, float sizeY) -> SoFaceSet* {
	SoFaceSet* plan = new SoFaceSet;
	SoVertexProperty* vertices = new SoVertexProperty;
	plan->vertexProperty = vertices;

	vertices->vertex.set1Value(0, SbVec3f(-sizeX - 0.05, sizeY + 0.05, 0));
	vertices->vertex.set1Value(1, SbVec3f(-sizeX - 0.05, -sizeY - 0.05, 0));
	vertices->vertex.set1Value(2, SbVec3f(sizeX + 0.05, -sizeY - 0.05, 0));
	vertices->vertex.set1Value(3, SbVec3f(sizeX + 0.05, sizeY + 0.05, 0));
	return plan;
}

auto OivScalarBar::createBorder() -> SoFaceSet* {
	SoFaceSet* plan = new SoFaceSet;
	SoVertexProperty* vertices = new SoVertexProperty;
	plan->vertexProperty = vertices;

	vertices->vertex.set1Value(0, SbVec3f(-1.05f, 1.01f, 0.f));
	vertices->vertex.set1Value(1, SbVec3f(-1.05f, -1.01f, 0.f));
	vertices->vertex.set1Value(2, SbVec3f(1.05f, -1.01f, 0.f));
	vertices->vertex.set1Value(3, SbVec3f(1.05f, 1.01f, 0.f));

	return plan;
}


auto OivScalarBar::createPlane(void) -> SoFaceSet* {
	SoFaceSet* plan = new SoFaceSet;
	SoVertexProperty* vertices = new SoVertexProperty;
	plan->vertexProperty = vertices;

	vertices->vertex.set1Value(0, SbVec3f(-1.f, 1.f, 0.f));
	vertices->vertex.set1Value(1, SbVec3f(-1.f, -1.f, 0.f));
	vertices->vertex.set1Value(2, SbVec3f(1.f, -1.f, 0.f));
	vertices->vertex.set1Value(3, SbVec3f(1.f, 1.f, 0.f));

	return plan;
}

auto OivScalarBar::createShader(void) -> SoShaderProgram* {
	//Shader
	SoShaderProgram* program = new SoShaderProgram;

	SoVertexShader* vp = new SoVertexShader;
	const auto verShaderPath = QString("%1/shader/vert.glsl").arg(qApp->applicationDirPath());
	vp->sourceProgram.setValue(verShaderPath.toStdString());

	SoFragmentShader* fp = new SoFragmentShader;
	const auto fragShaderPath = QString("%1/shader/frag.glsl").arg(qApp->applicationDirPath());
	fp->sourceProgram.setValue(fragShaderPath.toStdString());

	auto texSizeParam = new SoShaderParameter1f;
	texSizeParam->name = "dimTexture";
	texSizeParam->value = 64.0f;

	auto depthParam = new SoShaderParameter1f;
	SoShaderParameter1i* param = new SoShaderParameter1i;
	depthParam->name = "depth";
	depthParam->value = 1;
	param->value.setValue(0);
	param->name = "indexedTex";
	SoShaderParameter1i* param2 = new SoShaderParameter1i;
	param2->name = "colormap";
	param2->value.setValue(1);
	fp->parameter.set1Value(0, param);
	fp->parameter.set1Value(1, param2);
	fp->parameter.set1Value(2, depthParam);
	fp->parameter.set1Value(3, texSizeParam);

	program->shaderObject.set1Value(0, fp);
	program->shaderObject.set1Value(1, vp);

	return program;
}

auto OivScalarBar::getRootSceneGraph(void) -> SoSeparator* {
	return d->root;
}

auto OivScalarBar::createFlatVolumeBufferChar() -> unsigned char* {
	unsigned char* imageData = new unsigned char[256 * 256];
	for (int i = 0; i < 256; i++) {
		for (int j = 0; j < 256; j++) {
			if (d->direction == 0) {
				auto val = 255 - j;
				imageData[i * 256 + j] = val;
			} else {
				auto val = j;
				imageData[i * 256 + j] = val;
			}
		}
	}
	return imageData;
}

auto OivScalarBar::createFlatVolumeBuffer(void) -> SoCpuBufferObject* {
	SbVec4i32 imageSize(256, 256, 1, 1);
	SoCpuBufferObject* imageBuffer = new SoCpuBufferObject();
	imageBuffer->setSize(imageSize[0] * imageSize[1] * imageSize[2] * sizeof(unsigned char));
	unsigned char* imageData = static_cast<unsigned char*>(imageBuffer->map(SoCpuBufferObject::SET));

	for (int k = 0; k < imageSize[2]; ++k) {
		// Loop on image rows
		for (int j = 0; j < imageSize[1]; ++j) {
			// Loop on image columns
			for (int i = 0; i < imageSize[0]; ++i) {
				auto val = i;
				//0~255 value 
				imageData[k * imageSize[1] * imageSize[0] + j * imageSize[0] + i] = val;
			}
		}
	}
	imageBuffer->unmap();

	return imageBuffer;
}

auto OivScalarBar::changeColorMap(int idx) -> void {
	if (d->colormap) {
		//0: gray scale
		//1: Inverse grayscale
		//2: Hot iron
		//3: JET
		//4: Rainbow
		switch (idx) {
			case 0:
				d->colormap->predefinedColorMap = SoColorMap::INTENSITY;
				break;
			case 1:
				d->colormap->predefinedColorMap = SoColorMap::INTENSITY_REVERSED;
				break;
			case 2:
				d->colormap->predefinedColorMap = SoColorMap::GLOW;
				break;
			case 3:
				d->colormap->predefinedColorMap = SoColorMap::PHYSICS;
				break;
			case 4:
				d->colormap->predefinedColorMap = SoColorMap::STANDARD;
				break;
		}
	}
}
