#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Medical/nodes/TextBox.h>
#pragma warning(pop)

#include "OivInformativeUI.h"

struct OivInformativeUI::Impl {
};

OivInformativeUI::OivInformativeUI() : d(new Impl()) {
}

OivInformativeUI::~OivInformativeUI() {
}

auto OivInformativeUI::GetBoundaryBoxSwitch(SbBox3f box,
                                            SoSwitch* boundaryBoxNode, SoSwitch* axisGridNode,
                                            SbColor color, float width, int gridX, int gridY, int gridZ)->bool {
    // The box will be easier to see without lighting
    SoLightModel* pLModel = new SoLightModel;
    pLModel->model = SoLightModel::BASE_COLOR;

    // And with wide lines
    SoDrawStyle* pStyle = new SoDrawStyle;
    pStyle->lineWidth = width;

    // The box should be unpickable
    // (so geometry inside can be picked, draggers work inside, etc)
    SoPickStyle* pPickable = new SoPickStyle;
    pPickable->style = SoPickStyle::UNPICKABLE;

    // Create a cube outlining the specified box
    float xmin, xmax, ymin, ymax, zmin, zmax;
    box.getBounds(xmin, ymin, zmin, xmax, ymax, zmax);
    SoVertexProperty* pProp = new SoVertexProperty;
    pProp->vertex.set1Value(0, SbVec3f(xmin, ymin, zmin));
    pProp->vertex.set1Value(1, SbVec3f(xmax, ymin, zmin));
    pProp->vertex.set1Value(2, SbVec3f(xmax, ymax, zmin));
    pProp->vertex.set1Value(3, SbVec3f(xmin, ymax, zmin));
    pProp->vertex.set1Value(4, SbVec3f(xmin, ymin, zmax));
    pProp->vertex.set1Value(5, SbVec3f(xmax, ymin, zmax));
    pProp->vertex.set1Value(6, SbVec3f(xmax, ymax, zmax));
    pProp->vertex.set1Value(7, SbVec3f(xmin, ymax, zmax));
    pProp->orderedRGBA.set1Value(0, color.getPackedValue());

    // Draw it with a line set
    int coordIndices[] = { 0,1,2,3,0,-1,4,5,6,7,4,-1,
                          0,4,-1, 1,5,-1, 2,6,-1, 3,7 };
    int numCoordIndices = sizeof(coordIndices) / sizeof(int);
    SoIndexedLineSet* pLines = new SoIndexedLineSet;
    pLines->vertexProperty = pProp;
    pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

    // Assemble scene graph
    SoSeparator* pBoxSep = new SoSeparator;
    pBoxSep->addChild(pLModel);
    pBoxSep->addChild(pPickable);
    pBoxSep->addChild(pStyle);
    pBoxSep->addChild(pLines);

    //additional lines for grid representation
    //axisGridNode = new SoSwitch;
    axisGridNode->setName("GridBox");
    axisGridNode->whichChild = -1;

    pBoxSep->addChild(axisGridNode);


    SoVertexProperty* gridProp = new SoVertexProperty;
    auto idx = 0;
    std::vector<int> grid_coordIndices;
    for (int i = 1; i < gridX - 1; i++) {
        float newX = xmin + ((float)(xmax - xmin) / (float)gridX * (float)i);
        auto st = idx;
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
        grid_coordIndices.push_back(st);
        grid_coordIndices.push_back(-1);
    }
    for (int j = 1; j < gridY - 1; j++) {
        float newY = ymin + ((float)(ymax - ymin) / (float)gridY * (float)j);
        auto st = idx;
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
        grid_coordIndices.push_back(st);
        grid_coordIndices.push_back(-1);
    }
    for (int k = 1; k < gridZ - 1; k++) {
        float newZ = zmin + ((float)(zmax - zmin) / (float)gridZ * (float)k);
        auto st = idx;
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
        grid_coordIndices.push_back(idx);
        gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
        grid_coordIndices.push_back(st);
        grid_coordIndices.push_back(-1);
    }
    SbColorRGBA color2(0.4f, 0.4f, 0.4f, 0.5f);
    gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

    int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());//sizeof(grid_coordIndices) / sizeof(int);
    SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
    grid_pLines->vertexProperty = gridProp;
    grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());

    axisGridNode->addChild(grid_pLines);

    //boundaryBoxNode = new SoSwitch;
    boundaryBoxNode->setName("BoundaryBox");
    boundaryBoxNode->whichChild = -1;
    boundaryBoxNode->addChild(pBoxSep);
    
    return true;
}