#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTransform.h>
#pragma warning(pop)

#include "OivAxisMarker.h"

struct OivAxisMarker::Impl {
	SoSeparator* root;
};

OivAxisMarker::OivAxisMarker() : d{ new Impl } {		
    buildSceneGraph();

	setGeometry(d->root);
}

OivAxisMarker::~OivAxisMarker() {
	
}

auto OivAxisMarker::buildSceneGraph() -> void {
	d->root = new SoSeparator;	
	
	auto cone1 = new SoCone;
	auto cyl1 = new SoCylinder;
	auto mat1 = new SoMaterial;
	mat1->diffuseColor.setValue(1.0f, 0.0f, 0.0f);
	cone1->height = 0.2f;
	cone1->bottomRadius = 0.1f;
	auto trans1 = new SoTransform;
	trans1->translation.setValue(0.0f, 0.2f, 0.0f);
	cyl1->height = 0.3f;
	cyl1->radius = 0.04f;
	auto sep1 = new SoSeparator;
	sep1->addChild(mat1);
	sep1->addChild(cyl1);
	sep1->addChild(trans1);
	sep1->addChild(cone1);

	auto sep11 = new SoSeparator;
	auto tt1 = new SoTransform;
	tt1->translation.setValue(0.0f, 0.15f, 0.0f);
	sep11->addChild(tt1);
	sep11->addChild(sep1);

	d->root->addChild(sep11);

	auto cone2 = new SoCone;
	auto cyl2 = new SoCylinder;
	auto mat2 = new SoMaterial;
	cone2->height = 0.2f;
	cone2->bottomRadius = 0.1f;
	auto trans2 = new SoTransform;
	auto rot2 = new SoTransform;
	cyl2->height = 0.3f;
	cyl2->radius = 0.04f;
	mat2->diffuseColor.setValue(0.0f, 1.0f, 0.0f);

	trans2->translation.setValue(0.0f, 0.2f, 0.0f);
	rot2->rotation.setValue(SbVec3f(1.f, 0.f, 0.f), static_cast<float>(0.5 * M_PI));


	auto sep2 = new SoSeparator;
	sep2->addChild(mat2);
	sep2->addChild(cyl2);
	sep2->addChild(trans2);
	sep2->addChild(cone2);

	auto sep22 = new SoSeparator;
	auto tt2 = new SoTransform;
	tt2->translation.setValue(0.0f, 0.15f, 0.0f);
	sep22->addChild(rot2);
	sep22->addChild(tt2);
	sep22->addChild(sep2);
	d->root->addChild(sep22);

	auto cone3 = new SoCone;
	auto cyl3 = new SoCylinder;
	auto mat3 = new SoMaterial;
	cone3->height = 0.2f;
	cone3->bottomRadius = 0.1f;
	auto trans3 = new SoTransform;
	auto rot3 = new SoTransform;
	cyl3->height = 0.3f;
	cyl3->radius = 0.04f;
	mat3->diffuseColor.setValue(0.0f, 0.0f, 1.0f);

	trans3->translation.setValue(0.0f, 0.2f, 0.0f);
	rot3->rotation.setValue(SbVec3f(0.f, 0.f, 1.f), static_cast<float>(-0.5 * M_PI));

	auto sep3 = new SoSeparator;
	sep3->addChild(mat3);
	sep3->addChild(cyl3);
	sep3->addChild(trans3);
	sep3->addChild(cone3);

	auto sep33 = new SoSeparator;
	auto tt3 = new SoTransform;
	tt3->translation.setValue(0.0f, 0.15f, 0.0f);
	sep33->addChild(rot3);
	sep33->addChild(tt3);
	sep33->addChild(sep3);
	d->root->addChild(sep33);		
}
