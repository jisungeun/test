#include <QApplication>
#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoBBox.h>
#include <Inventor/nodes/SoCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoFontStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTextProperty.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoVertexProperty.h>

#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>

#include <Inventor/elements/SoViewportRegionElement.h>
#include <Inventor/elements/SoViewVolumeElement.h>

#include <Inventor/sensors/SoNodeSensor.h>

#include <Medical/InventorMedical.h>

#include <VolumeViz/nodes/SoTransferFunction.h>
#include <Inventor/nodes/SoColorMap.h>
#include <Inventor/nodes/SoIndexedTexture2.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoShaderProgram.h>
#include <Inventor/nodes/SoAnnoText3Property.h>
#include <Inventor/nodes/SoTextProperty.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoAnnoText3.h>

#include <VolumeViz/nodes/SoTransferFunction.h>
#pragma warning(pop)

#include "OivRangeBar.h"

SO_NODE_SOURCE(OivRangeBar);

struct OivRangeBar::Impl {
    //basic nodes
    SoCallback* callbackNode{ nullptr };
    SoBBox* bbox{ nullptr };
    SoLightModel* lmodel{ nullptr };
    SoPickStyle* pstyle{ nullptr };
    SoOrthographicCamera* camera{ nullptr };
    SoNodeSensor* sensor{ nullptr };

    //scene nodes
    SoSeparator* root;

    SoSeparator* scalarRoot;
    SoMaterial* matl;
    SoShaderProgram* shader;
    SoTransferFunction* tf;
    SoColorMap* colormap;
    SoIndexedTexture2* texture;
    SoFaceSet* facet;

    //facet for borderline
    SoDirectionalLight* borderLight;
    SoSeparator* borderRoot;
    SoMaterial* borderMatl;
    SoFaceSet* border;
    SoTranslation* borderTrans;
    SoScale* borderScale;

    //annotation texts
    SoSeparator* annotations;
    SoSeparator* annotationBorders;
    SoRef<SoFont> font{ nullptr };
    int upsample{ 1 };

    SoCpuBufferObject* dummyBuffer;

    //position of scalarBar in render window coordinates    
    SoTranslation* trans;
    SoTranslation* annoTrans{ nullptr };
    //size of scalarBar in ratio term
    //will be applied to transformation of unit square    
    SoScale* scale;

    float ori_min{ 0.0 };
    float ori_max{ 10000.0 };

    float scalar_min{ 0.0 };
    float scalar_max{ 10000.0 };        

    bool isRangeChanged{ false };

    //int direction{ 0 }; //0: XY, 1: YZ, 2:XZ
    bool rebuildAnnotation{ false };

    int precision{ 3 };
};

void OivRangeBar::initClass() {
    getClassRenderEngineMode().setRenderMode(SbRenderEngineMode::OIV_OPENINVENTOR_RENDERING);
    SO_NODE_INIT_CLASS(OivRangeBar, SoAnnotation, "Annotation");
}

void OivRangeBar::exitClass() {
    SO__NODE_EXIT_CLASS(OivRangeBar);
}
OivRangeBar::OivRangeBar() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivRangeBar);
    SO_NODE_ADD_FIELD(position, (-0.95, -0.95));
    SO_NODE_ADD_FIELD(trackedCamera, (NULL));
    SO_NODE_ADD_FIELD(orientation, (OivRangeBar::HORIZONTAL));
    SO_NODE_ADD_FIELD(alignment, (OivRangeBar::CENTER));
    SO_NODE_ADD_FIELD(majorlength, (9));
    SO_NODE_ADD_FIELD(ratio, (10));
    SO_NODE_ADD_FIELD(numOfAnnotation, (2));
    SO_NODE_ADD_FIELD(fontSize, (0));
    SO_NODE_ADD_FIELD(barSize, (0, 0));
    SO_NODE_ADD_FIELD(lastAnnotationMargin, (0));
    SO_NODE_ADD_FIELD(reverseTextColor, (true));
        
    // Set up static info for enumerated type fields
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, LEFT);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, BOTTOM);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, CENTER);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, RIGHT);
    SO_NODE_DEFINE_ENUM_VALUE(Alignment, TOP);
    SO_NODE_DEFINE_ENUM_VALUE(Orientation, HORIZONTAL);
    SO_NODE_DEFINE_ENUM_VALUE(Orientation, VERTICAL);

    // Associate info for enumerated type fields
    SO_NODE_SET_SF_ENUM_TYPE(alignment, Alignment);
    SO_NODE_SET_SF_ENUM_TYPE(orientation, Orientation);

#if SO_INVENTOR_VERSION >= 9100
    boundingBoxCaching.setFieldType(SoField::PRIVATE_FIELD);
    renderCulling.setFieldType(SoField::PRIVATE_FIELD);
    pickCulling.setFieldType(SoField::PRIVATE_FIELD);
    fastEditing.setFieldType(SoField::PRIVATE_FIELD);
    renderUnitId.setFieldType(SoField::PRIVATE_FIELD);
#endif

    buildSceneGraph();    

    d->sensor = new SoNodeSensor(sensorCB, (void*)this);
    d->sensor->attach(this);
    d->sensor->setPriority(0);
}

OivRangeBar::~OivRangeBar() {
    delete d->sensor;
}

void OivRangeBar::sensorCB(void* data, SoSensor* sensor) {
    OivRangeBar* self = static_cast<OivRangeBar*>(data);
    const SoField* trigger = dynamic_cast<SoNodeSensor*>(sensor)->getTriggerField();

    if(trigger != nullptr) {
        //self->d->sensor //Do nothing
    }
}

void OivRangeBar::staticCB(void* data, SoAction* action) {
    OivRangeBar* self = (OivRangeBar*)data;
    if (action->isOfType(SoGLRenderAction::getClassTypeId()) ||
        action->isOfType(SoGetBoundingBoxAction::getClassTypeId())) {

        self->renderCB(action);
    }
}

void OivRangeBar::renderCB(SoAction* action) {
    modifyBorder();
    modifyPlane();
    createAnnotation(action);
    
    if (d->isRangeChanged) {
        modifyFlatVolumeBuffer();
    }
}

auto OivRangeBar::buildSceneGraph()->void {
    // Event callback
    d->callbackNode = new SoCallback;
    d->callbackNode->setCallback(OivRangeBar::staticCB, (void*)this);
    this->addChild(d->callbackNode);

    d->bbox = new SoBBox();
    d->bbox->mode = SoBBox::NO_BOUNDING_BOX;
    this->addChild(d->bbox);

    d->lmodel = new SoLightModel();
    d->lmodel->model = SoLightModel::BASE_COLOR;
    this->addChild(d->lmodel);

    d->pstyle = new SoPickStyle();
    d->pstyle->style = SoPickStyle::UNPICKABLE;
    this->addChild(d->pstyle);

    d->camera = new SoOrthographicCamera();
    d->camera->viewportMapping = SoCamera::LEAVE_ALONE;
    this->addChild(d->camera);

    initScalarBar();
}

auto OivRangeBar::rebuildAnnotation() const ->void {
    d->rebuildAnnotation = true;
}

auto OivRangeBar::initScalarBar() -> void {
    //build scalar bar Scene graph
    d->root = new SoSeparator;
    d->root->setName("RangeBarRoot");

    //d->direction = dir;
    this->addChild(d->root);

    d->annotations = new SoSeparator;
    auto annoSep = new SoSeparator;
    d->annoTrans = new SoTranslation;
    annoSep->addChild(d->annoTrans);
    annoSep->addChild(d->annotations);
    //d->root->addChild(d->annotations);
    d->root->addChild(annoSep);

    //create Border plane
    d->borderRoot = new SoSeparator;
    d->borderRoot->setName("RangeBorderRoot");
    d->root->addChild(d->borderRoot);
    d->borderLight = new SoDirectionalLight;


    d->borderRoot->addChild(d->borderLight);
    d->borderMatl = new SoMaterial;
    d->borderMatl->diffuseColor.setValue(1.0, 1.0, 1.0);
    d->border = createBorder();
    d->borderRoot->addChild(d->borderMatl);

    d->borderScale = new SoScale;
    d->borderRoot->addChild(d->borderScale);
    d->borderRoot->addChild(d->border);

    //create scalar bar plane
    d->scalarRoot = new SoSeparator;
    d->root->addChild(d->scalarRoot);

    d->matl = new SoMaterial;
    d->shader = createShader();

    auto group = new SoGroup;
    group->setName("shaderName");
    group->addChild(d->matl);
    group->addChild(d->shader);

    d->scalarRoot->addChild(group);

    d->colormap = new SoColorMap;
    d->colormap->predefinedColorMap = SoColorMap::TEMPERATURE;
    d->colormap->max = 255;

    d->texture = new SoIndexedTexture2;
    d->texture->wrapS = SoTexture2::CLAMP_TO_EDGE;
    d->texture->wrapT = SoTexture2::CLAMP_TO_EDGE;
    d->texture->minFilter = SoTexture2::LINEAR;
    d->texture->magFilter = SoTexture2::LINEAR;

    //d->dummyBuffer = createFlatVolumeBuffer();
    auto dummy = createFlatVolumeBufferChar();
    d->texture->imageIndex.setValue(SbVec2s(256, 256), SoSFArray2D::UNSIGNED_BYTE, dummy,SoSFArray::NO_COPY_AND_DELETE);    

    d->scalarRoot->addChild(d->colormap);
    d->scalarRoot->addChild(d->texture);
    SoShapeHints* shapeHints = new SoShapeHints;
    shapeHints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
    shapeHints->shapeType = SoShapeHints::UNKNOWN_SHAPE_TYPE;
    shapeHints->creaseAngle = 40;
    d->scalarRoot->addChild(shapeHints);

    d->facet = createPlane();

    //applay translation
    d->trans = new SoTranslation;
    d->scale = new SoScale;    

    d->trans->translation.setValue(0, 0, 0.0);
    d->scale->scaleFactor.setValue(1, 1, 1.0);
    
    d->scalarRoot->addChild(d->trans);
    d->scalarRoot->addChild(d->scale);

    d->scalarRoot->addChild(d->facet);

    d->annotationBorders = new SoSeparator;
    d->root->addChild(d->annotationBorders);        
}

auto OivRangeBar::setTransferFunction(SoTransferFunction* tf) -> void {
    d->tf = tf;
    d->colormap->predefinedColorMap.connectFrom(&d->tf->predefColorMap);    
}

auto OivRangeBar::getTransferFunction() -> SoTransferFunction* {
    return d->tf;
}

auto OivRangeBar::setMinMax(float min, float max, bool original) -> void {
    if(original) {
        d->ori_min = min;
        d->ori_max = max;
    }
    d->scalar_min = min;
    d->scalar_max = max;
    d->isRangeChanged = false;
}

auto OivRangeBar::SetUpsample(int upsample) -> void {
    if (nullptr != d->font.ptr()) {
        d->font->size = fontSize.getValue() * upsample;
    }
    d->upsample = upsample;
}

auto OivRangeBar::createAnnotation(SoAction* action) -> void {    
    /*if (false == d->rebuildAnnotation) {
        if (d->annotations->getNumChildren() > 0) {
            return;
        }
    }*/
    
    d->annotations->removeAllChildren();
    
    auto len_div = majorlength.getValue();
    float size_x = barSize.getValue()[0];
    float size_y = barSize.getValue()[1];

    if(0 == size_x && 0 == size_y) {
        if (orientation.getValue() == VERTICAL) {
            size_y = (len_div < 1) ? 1.8f : (1.8f / len_div);
            size_x = size_y / ratio.getValue();
        }
        else {
            size_x = (len_div < 1) ? 1.8f : (1.8f / len_div);
            size_y = size_x / ratio.getValue();
        }
    }

    SoBaseColor* col = new SoBaseColor;
    //SoAnnoText3Property* prop = new SoAnnoText3Property;    
    SoTextProperty* prop = new SoTextProperty;
    if (nullptr == d->font.ptr()) {
        d->font = new SoFont;
        d->font->setName("ScalarFont");
        d->font->name = "Arial:Bold";
        d->font->size.setValue(fontSize.getValue());
        if (0.f == d->font->size.getValue()) {
            if (orientation.getValue() == VERTICAL) {
                d->font->size = size_x * 0.7;
            }
            else {
                d->font->size = size_y * 0.7;
            }
        }
        d->font->renderStyle = SoFont::RenderStyle::TEXTURE;
    }

    //if (d->isInverse) {
    if(d->colormap->predefinedColorMap.getValue() == SoTransferFunction::PredefColorMap::INTENSITY_REVERSED){
        if(reverseTextColor.getValue()) col->rgb.setValue(0.0, 0.0, 0.0);
        else col->rgb.setValue(1.0, 1.0, 1.0);;
    }
    else {
        col->rgb.setValue(1.0, 1.0, 1.0);
    }    
    if (orientation.getValue() == HORIZONTAL) {
        prop->alignmentH = SoTextProperty::RIGHT;
        prop->alignmentV = SoTextProperty::BOTTOM;
    }
    else {
        prop->alignmentH = SoTextProperty::LEFT;
        prop->alignmentV = SoTextProperty::TOP;
    }
    //prop->renderPrintType = SoAnnoText3Property::RenderPrintType::RENDER2D_PRINT_RASTER;
    //prop->fontSizeHint = SoAnnoText3Property::FontSizeHint::ANNOTATION;

    d->annotations->addChild(d->font.ptr());
    d->annotations->addChild(col);
    d->annotations->addChild(prop);

    auto range = d->scalar_max - d->scalar_min;
    const float div = pow(10, d->precision);

    if (orientation.getValue() == VERTICAL) {        
        for (int i = 0; i < numOfAnnotation.getValue() - 1; i++) {            
            float ratio = (float)(i) / (float)(numOfAnnotation.getValue()-1);
            
            auto val = d->scalar_min + (ratio * range);

            auto group = new SoSeparator;
            auto trans = new SoTranslation;
            
            auto slack = d->font->size.getValue() / 812.5 / d->upsample;            
            if (i == 0) {
                trans->translation.setValue(position.getValue()[0] + size_x * 1.2,
                    position.getValue()[1] + size_y * ratio + slack , 0.0);                
                val *= div;
                val = ceil(val);
                val /= div;
            }
            else {                
                trans->translation.setValue(position.getValue()[0] + size_x * 1.2,
                    position.getValue()[1] + size_y * ratio + slack/2 , 0.0);
            }
            auto text = new SoText2;
            auto qtext = QString::number(val, 'f', d->precision);
            text->string = qtext.toStdString();
            text->justification = SoAnnoText3::Justification::LEFT;
            group->addChild(trans);
            group->addChild(text);                        

            d->annotations->addChild(group);
        }
        //draw final bullet
        auto val = d->scalar_max;
        val *= div;
        val = floor(val);
        val /= div;
        //auto text = new SoAnnoText3;
        auto text = new SoText2;
        auto qtext = QString::number(val, 'f', d->precision);
        text->string = qtext.toStdString();
        text->justification = SoAnnoText3::Justification::LEFT;
        auto group = new SoSeparator;
        auto trans = new SoTranslation;

        trans->translation.setValue(position.getValue()[0] + size_x * 1.2, 
                                    position.getValue()[1] + size_y , 0.0);
        group->addChild(trans);
        group->addChild(text);

        d->annotations->addChild(group);
    }else {
        for (int i = 0; i < numOfAnnotation.getValue() - 1; i++) {
            float ratio = (float)(i) / (float)(numOfAnnotation.getValue()-1);

            auto val = d->scalar_min + (ratio * range);

            auto text = new SoText2;            
            if (i == 0) {                
                text->justification = SoAnnoText3::Justification::LEFT;
                val *= div;
                val = ceil(val);
                val /= div;
            }
            else {
                text->justification = SoAnnoText3::Justification::CENTER;
            }
            auto group = new SoSeparator;
            auto trans = new SoTranslation;
            if (i == 0) {
                trans->translation.setValue(position.getValue()[0] + size_x * ratio,
                                            position.getValue()[1] + size_y * 1.2, 0.0);                
            }else{
                SbBox3f box;
                SbVec3f center;
                text->computeBBox(action, box, center);
                auto slack = (box.getMax()[0] - box.getMin()[0]) / 560;
                trans->translation.setValue(position.getValue()[0] + size_x * ratio - slack,
                                            position.getValue()[1] + size_y * 1.2, 0.0);                
            }            

            auto qtext = QString::number(val, 'f', d->precision);
            text->string = qtext.toStdString();

            group->addChild(trans);
            group->addChild(text);

            d->annotations->addChild(group);
        }
        //draw final bullet
        auto val = d->scalar_max;
        val *= div;
        val = floor(val);
        val /= div;
        auto text = new SoText2;
        auto qtext = QString::number(val, 'f', d->precision);
        text->string = qtext.toStdString();        
        text->justification = SoAnnoText3::Justification::RIGHT;
                
        auto group = new SoSeparator;
        auto trans = new SoTranslation;
        
        group->addChild(trans);
        group->addChild(text);

        d->annotations->addChild(group);

        int margin = lastAnnotationMargin.getValue();
        if (0 == margin) margin = 280;

        SbBox3f box;
        SbVec3f center;
        text->computeBBox(action, box, center);
        trans->translation.setValue(position.getValue()[0] + size_x,
                                    position.getValue()[1] + size_y * 1.2, 0.0);
    }

    d->rebuildAnnotation = false;
}

auto OivRangeBar::createFlatVolumeBufferChar() -> unsigned char* {
    unsigned char* imageData = new unsigned char[256 * 256];
    for (int i = 0; i < 256; i++) {
        for (int j = 0; j < 256; j++) {
            auto val = j;//255 - j;
            imageData[i * 256 + j] = val;            
        }
    }
    return imageData;
}

auto OivRangeBar::modifyFlatVolumeBuffer() -> void {    
    auto size = SbVec2s(256, 256);
    auto type = SoSFArray::DataType::UNSIGNED_BYTE;
    auto val = (unsigned char*)d->texture->imageIndex.startEditing(size,type);
    auto ori_range = d->ori_max - d->ori_min;
    auto changed_range = d->scalar_max - d->scalar_min;
    auto st = 256 * (d->scalar_min-d->ori_min) / ori_range;
    auto step = changed_range / ori_range;
    for(auto i=0;i<256;i++) {
        for(int j=0;j<256;j++) {
            *(val + (i * 256 + j)) = static_cast<unsigned short>(st + step*j);
        }
    }
    d->texture->imageIndex.finishEditing();
    d->isRangeChanged = false;
}

auto OivRangeBar::createBorder() -> SoFaceSet* {
    SoFaceSet* plan = new SoFaceSet;
    SoVertexProperty* vertices = new SoVertexProperty;
    plan->vertexProperty = vertices;        

    vertices->vertex.set1Value(0, SbVec3f(-.21f, .21f, 0.f));
    vertices->vertex.set1Value(1, SbVec3f(-.21f, -.21f, 0.f));
    vertices->vertex.set1Value(2, SbVec3f(.21f, -.21f, 0.f));
    vertices->vertex.set1Value(3, SbVec3f(.21f, .21f, 0.f));
    //1.03
    return plan;
}
auto OivRangeBar::getBorderGeometry()->SoFaceSet* {
    return d->border;
}
auto OivRangeBar::modifyBorder() -> void {
    auto vertices = dynamic_cast<SoVertexProperty*>(d->border->vertexProperty.getValue());
    if (orientation.getValue() == VERTICAL) {
        auto x_st = position.getValue()[0];
        auto y_st = position.getValue()[1];
        auto len_div = majorlength.getValue();
        float x_len = barSize.getValue()[0];
        float y_len = barSize.getValue()[1];

        if(0 == x_len && 0 == y_len) {
            y_len = (len_div < 1) ? 1.8f : (1.8f / len_div);
            x_len = y_len / ratio.getValue();
        }
        
        auto y_diff = (len_div < 1) ? 0.2f : (0.2f / len_div);
        auto x_diff = y_diff / ratio.getValue() /2;
        
        vertices->vertex.set1Value(0, SbVec3f(x_st- x_diff, y_st - x_diff, 0.f));
        vertices->vertex.set1Value(1, SbVec3f(x_st- x_diff, y_st + y_len + x_diff, 0.f));
        vertices->vertex.set1Value(2, SbVec3f(x_st + x_len + x_diff, y_st + y_len + x_diff, 0.f));
        vertices->vertex.set1Value(3, SbVec3f(x_st + x_len + x_diff, y_st - x_diff, 0.f));
    }
    else {        
        auto x_st = position.getValue()[0];
        auto y_st = position.getValue()[1];
        auto len_div = majorlength.getValue();

        float x_len = barSize.getValue()[0];
        float y_len = barSize.getValue()[1];

        if (0 == x_len && 0 == y_len) {
            x_len = (len_div < 1) ? 1.8f : (1.8f / len_div);
            y_len = x_len / ratio.getValue();
        }
        
        auto x_diff = (len_div < 1) ? 0.2f : (0.2f / len_div );
        auto y_diff = x_diff / ratio.getValue() /2;
        vertices->vertex.set1Value(0, SbVec3f(x_st - y_diff, y_st - y_diff, 0.f));
        vertices->vertex.set1Value(1, SbVec3f(x_st - y_diff, y_st + y_len + y_diff, 0.f));
        vertices->vertex.set1Value(2, SbVec3f(x_st + x_len + y_diff, y_st + y_len + y_diff, 0.f));
        vertices->vertex.set1Value(3, SbVec3f(x_st + x_len + y_diff, y_st - y_diff, 0.f));
    }
}

auto OivRangeBar::createPlane() -> SoFaceSet* {    
    SoFaceSet* plan = new SoFaceSet;    
    SoVertexProperty* vertices = new SoVertexProperty;
    plan->vertexProperty = vertices;
    vertices->vertex.set1Value(0, SbVec3f(-.99f, -.99f, 0.f));
    vertices->vertex.set1Value(1, SbVec3f(-.99f, 0.f, 0.f));
    vertices->vertex.set1Value(2, SbVec3f(0.f, 0.f, 0.f));
    vertices->vertex.set1Value(3, SbVec3f(0.f, -.99f, 0.f));

    return plan;
}

auto OivRangeBar::modifyPlane() -> void {
    auto vertices = dynamic_cast<SoVertexProperty*>(d->facet->vertexProperty.getValue());
    if (orientation.getValue() == VERTICAL) {
        auto x_st = position.getValue()[0];
        auto y_st = position.getValue()[1];
        auto len_div = majorlength.getValue();

        float x_len = barSize.getValue()[0];
        float y_len = barSize.getValue()[1];

        if (0 == x_len && 0 == y_len) {
            y_len = (len_div < 1) ? 1.8f : (1.8f / len_div);
            x_len = y_len / ratio.getValue();
        }
        
        vertices->vertex.set1Value(0, SbVec3f(x_st, y_st, 0.f));
        vertices->vertex.set1Value(1, SbVec3f(x_st, y_st + y_len, 0.f));
        vertices->vertex.set1Value(2, SbVec3f(x_st + x_len, y_st + y_len, 0.f));
        vertices->vertex.set1Value(3, SbVec3f(x_st + x_len, y_st, 0.f));
    }
    else {
        auto x_st = position.getValue()[0];
        auto y_st = position.getValue()[1];
        auto len_div = majorlength.getValue();

        float x_len = barSize.getValue()[0];
        float y_len = barSize.getValue()[1];

        if (0 == x_len && 0 == y_len) {
            x_len = (len_div < 1) ? 1.8f : (1.8f / len_div);
            y_len = x_len / ratio.getValue();
        }
        
        vertices->vertex.set1Value(0, SbVec3f(x_st, y_st, 0.f));
        vertices->vertex.set1Value(1, SbVec3f(x_st, y_st + y_len, 0.f));
        vertices->vertex.set1Value(2, SbVec3f(x_st + x_len, y_st + y_len, 0.f));
        vertices->vertex.set1Value(3, SbVec3f(x_st + x_len, y_st, 0.f));
    }
}

auto OivRangeBar::SetPrecision(int prec) -> void {
    d->precision = prec;
}

auto OivRangeBar::createShader() -> SoShaderProgram* {
    //Shader
    SoShaderProgram* program = new SoShaderProgram;

    SoVertexShader* vp = new SoVertexShader;
    const auto verShaderPath = QString("%1/shader/vert.glsl").arg(qApp->applicationDirPath());
    vp->sourceProgram.setValue(verShaderPath.toStdString());

    SoFragmentShader* fp = new SoFragmentShader;
    const auto fragShaderPath = QString("%1/shader/frag.glsl").arg(qApp->applicationDirPath());
    fp->sourceProgram.setValue(fragShaderPath.toStdString());

    auto texSizeParam = new SoShaderParameter1f;
    texSizeParam->name = "dimTexture";
    texSizeParam->value = 64.0f;

    auto depthParam = new SoShaderParameter1f;
    SoShaderParameter1i* param = new SoShaderParameter1i;
    depthParam->name = "depth";
    depthParam->value = 1;
    param->value.setValue(0);
    param->name = "indexedTex";
    SoShaderParameter1i* param2 = new SoShaderParameter1i;
    param2->name = "colormap";
    param2->value.setValue(1);
    fp->parameter.set1Value(0, param);
    fp->parameter.set1Value(1, param2);
    fp->parameter.set1Value(2, depthParam);
    fp->parameter.set1Value(3, texSizeParam);

    program->shaderObject.set1Value(0, fp);
    program->shaderObject.set1Value(1, vp);

    return program;
}
