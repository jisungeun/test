#pragma warning(disable:4305)

#include <QCoreApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/SbColorRGBA.h>
#include <Inventor/sensors/SoFieldSensor.h>
#include <Inventor/actions/SoAction.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#pragma warning(pop)

#include "OivAxisGrid.h"


struct OivAxisGrid::Impl {
	SoRef<SoSeparator> BBoxSeparator { nullptr };

	SoRef<SoSwitch> XYSwitch[2] { nullptr, };//lower, upper
	SoRef<SoSeparator> BBoxXYSeparator[2] { nullptr, };
	SoRef<SoSwitch> AxisXYSwitch[2] { nullptr, };
	SoRef<SoSeparator> AxisXYSeparator[2] { nullptr, };

	SoRef<SoSwitch> NoteXSwitch[4] { nullptr, };
	SoRef<SoTranslation> NoteXTranslation[4] { nullptr, };
	SoRef<SoTranslation> NoteXInvTranslation[4] { nullptr, };
	SoRef<SoRotationXYZ> NoteXRotation[4] { nullptr, };
	SoRef<SoSeparator> NoteXRoot[4] { nullptr, };
	SoRef<SoSeparator> NoteXSeparator[4] { nullptr, };

	SoRef<SoSwitch> YZSwitch[2] { nullptr, };
	SoRef<SoSeparator> BBoxYZSeparator[2] { nullptr, };
	SoRef<SoSwitch> AxisYZSwitch[2] { nullptr, };
	SoRef<SoSeparator> AxisYZSeparator[2] { nullptr, };

	SoRef<SoSwitch> NoteYSwitch[4] { nullptr, };
	SoRef<SoTranslation> NoteYTranslation[4] { nullptr, };
	SoRef<SoTranslation> NoteYInvTranslation[4] { nullptr, };
	SoRef<SoRotationXYZ> NoteYRotation[4] { nullptr, };
	SoRef<SoSeparator> NoteYRoot[4] { nullptr, };
	SoRef<SoSeparator> NoteYSeparator[4] { nullptr, };

	SoRef<SoSwitch> XZSwitch[2] { nullptr, };
	SoRef<SoSeparator> BBoxXZSeparator[2] { nullptr, };
	SoRef<SoSwitch> AxisXZSwitch[2] { nullptr, };
	SoRef<SoSeparator> AxisXZSeparator[2] { nullptr, };

	SoRef<SoSwitch> NoteZSwitch[4] { nullptr, };
	SoRef<SoTranslation> NoteZTranslation[4] { nullptr, };
	SoRef<SoTranslation> NoteZInvTranslation[4] { nullptr, };
	SoRef<SoRotationXYZ> NoteZRotation[4] { nullptr, };
	SoRef<SoSeparator> NoteZRoot[4] { nullptr, };
	SoRef<SoSeparator> NoteZSeparator[4] { nullptr, };

	//SoCallback* callbackNode{ nullptr };

	///////////////////////////////////////
	bool isBoundingBox { true };
	SoRef<SoDrawStyle> defaultStyle { nullptr };
	SoRef<SoCamera> camera3d { nullptr };
	double x_size { 0 };
	double y_size { 0 };
	double z_size { 0 };
	SoRef<SoMaterial> text_matl[3];
	SoRef<SoMaterial> bounding_matl;
	SoFieldSensor* mySensor;

	SoRef<SoFont> textFont { nullptr };
};

OivAxisGrid::OivAxisGrid() : d { new Impl } {
	Init();
}

OivAxisGrid::~OivAxisGrid() {
	if (nullptr != d->mySensor) {
		delete d->mySensor;
	}
}

auto OivAxisGrid::SetTextColor(int axis, int r, int g, int b) -> void {
	d->text_matl[axis]->diffuseColor.setValue(static_cast<float>(r) / 255.0f, static_cast<float>(g) / 255.0f, static_cast<float>(b) / 255.0f);
}

auto OivAxisGrid::SetColor(double r, double g, double b) -> void {
	d->bounding_matl->diffuseColor.setValue(r, g, b);
}

auto OivAxisGrid::Init() -> void {
	for (auto i = 0; i < 3; i++) {
		d->text_matl[i] = new SoMaterial;
		d->text_matl[i]->transparency.setValue(0);
		d->text_matl[i]->diffuseColor.setValue(0.8, 0.8, 0.8);
	}

	d->bounding_matl = new SoMaterial;
	d->bounding_matl->transparency.setValue(0);
	d->bounding_matl->diffuseColor.setValue(0.8, 0.8, 0.8);
	//Init emtpy scene graph
	d->BBoxSeparator = new SoSeparator;
	d->BBoxSeparator->ref();//hold reference cound;    
	d->BBoxSeparator->fastEditing.setValue(SoSeparator::FastEditing::KEEP_ZBUFFER);
	SoLightModel* pLModel = new SoLightModel;
	pLModel->model = SoLightModel::BASE_COLOR;

	d->defaultStyle = new SoDrawStyle;

	SoPickStyle* pPickable = new SoPickStyle;
	pPickable->style = SoPickStyle::UNPICKABLE;

	d->BBoxSeparator->addChild(pLModel);
	d->BBoxSeparator->addChild(d->defaultStyle.ptr());
	d->BBoxSeparator->addChild(pPickable);

	//For Boundary Box & Axis
	for (auto i = 0; i < 2; i++) {
		d->XYSwitch[i] = new SoSwitch;
		QString name = "XYSwitch" + QString::number(i);
		d->XYSwitch[i]->setName(name.toStdString().c_str());
		d->XYSwitch[i]->whichChild = SO_SWITCH_ALL;
		d->BBoxXYSeparator[i] = new SoSeparator;
		d->XYSwitch[i]->addChild(d->BBoxXYSeparator[i].ptr());
		d->AxisXYSwitch[i] = new SoSwitch;
		d->AxisXYSwitch[i]->whichChild = SO_SWITCH_NONE;
		d->XYSwitch[i]->addChild(d->AxisXYSwitch[i].ptr());
		d->AxisXYSeparator[i] = new SoSeparator;
		d->AxisXYSwitch[i]->addChild(d->AxisXYSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->XYSwitch[i].ptr());

		d->YZSwitch[i] = new SoSwitch;
		QString nameyz = "YZSwitch" + QString::number(i);
		d->YZSwitch[i]->setName(nameyz.toStdString().c_str());
		d->YZSwitch[i]->whichChild = SO_SWITCH_ALL;
		d->BBoxYZSeparator[i] = new SoSeparator;
		d->YZSwitch[i]->addChild(d->BBoxYZSeparator[i].ptr());
		d->AxisYZSwitch[i] = new SoSwitch;
		d->AxisYZSwitch[i]->whichChild = SO_SWITCH_NONE;
		d->YZSwitch[i]->addChild(d->AxisYZSwitch[i].ptr());
		d->AxisYZSeparator[i] = new SoSeparator;
		d->AxisYZSwitch[i]->addChild(d->AxisYZSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->YZSwitch[i].ptr());

		d->XZSwitch[i] = new SoSwitch;
		QString namexz = "XZSwitch" + QString::number(i);
		d->XZSwitch[i]->setName(namexz.toStdString().c_str());
		d->XZSwitch[i]->whichChild = SO_SWITCH_ALL;
		d->BBoxXZSeparator[i] = new SoSeparator;
		d->XZSwitch[i]->addChild(d->BBoxXZSeparator[i].ptr());
		d->AxisXZSwitch[i] = new SoSwitch;
		d->AxisXZSwitch[i]->whichChild = SO_SWITCH_NONE;
		d->XZSwitch[i]->addChild(d->AxisXZSwitch[i].ptr());
		d->AxisXZSeparator[i] = new SoSeparator;
		d->AxisXZSwitch[i]->addChild(d->AxisXZSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->XZSwitch[i].ptr());
	}

	//For Notes
	for (auto i = 0; i < 4; i++) {
		d->NoteXSwitch[i] = new SoSwitch;
		QString namex = "NoteXSwitch" + QString::number(i);
		d->NoteXSwitch[i]->setName(namex.toStdString().c_str());
		d->NoteXSwitch[i]->whichChild = -1;
		d->NoteXRotation[i] = new SoRotationXYZ;
		d->NoteXSeparator[i] = new SoSeparator;
		d->NoteXRoot[i] = new SoSeparator;
		d->NoteXSwitch[i]->addChild(d->NoteXRoot[i].ptr());
		d->NoteXTranslation[i] = new SoTranslation;
		d->NoteXInvTranslation[i] = new SoTranslation;
		d->NoteXRoot[i]->addChild(d->NoteXTranslation[i].ptr());
		d->NoteXRoot[i]->addChild(d->NoteXRotation[i].ptr());
		d->NoteXRoot[i]->addChild(d->NoteXInvTranslation[i].ptr());
		d->NoteXRoot[i]->addChild(d->NoteXSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->NoteXSwitch[i].ptr());

		d->NoteYSwitch[i] = new SoSwitch;
		QString namey = "NoteYSwitch" + QString::number(i);
		d->NoteYSwitch[i]->setName(namey.toStdString().c_str());
		d->NoteYSwitch[i]->whichChild = -1;
		d->NoteYRotation[i] = new SoRotationXYZ;
		d->NoteYSeparator[i] = new SoSeparator;
		d->NoteYRoot[i] = new SoSeparator;
		d->NoteYTranslation[i] = new SoTranslation;
		d->NoteYInvTranslation[i] = new SoTranslation;
		d->NoteYRoot[i]->addChild(d->NoteYTranslation[i].ptr());
		d->NoteYRoot[i]->addChild(d->NoteYRotation[i].ptr());
		d->NoteYRoot[i]->addChild(d->NoteYInvTranslation[i].ptr());
		d->NoteYSwitch[i]->addChild(d->NoteYRoot[i].ptr());
		d->NoteYRoot[i]->addChild(d->NoteYSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->NoteYSwitch[i].ptr());

		d->NoteZSwitch[i] = new SoSwitch;
		QString namez = "NoteZSwitch" + QString::number(i);
		d->NoteZSwitch[i]->setName(namez.toStdString().c_str());
		d->NoteZSwitch[i]->whichChild = -1;
		d->NoteZRotation[i] = new SoRotationXYZ;
		d->NoteZRoot[i] = new SoSeparator;
		d->NoteZTranslation[i] = new SoTranslation;
		d->NoteZInvTranslation[i] = new SoTranslation;
		d->NoteZRoot[i]->addChild(d->NoteZTranslation[i].ptr());
		d->NoteZRoot[i]->addChild(d->NoteZRotation[i].ptr());
		d->NoteZRoot[i]->addChild(d->NoteZInvTranslation[i].ptr());
		d->NoteZSeparator[i] = new SoSeparator;
		d->NoteZSwitch[i]->addChild(d->NoteZRoot[i].ptr());
		d->NoteZRoot[i]->addChild(d->NoteZSeparator[i].ptr());

		d->BBoxSeparator->addChild(d->NoteZSwitch[i].ptr());
	}
}

auto OivAxisGrid::CleanBox() -> void {
	for (auto i = 0; i < 2; i++) {
		//Bounding lines
		d->BBoxXYSeparator[i]->removeAllChildren();
		d->BBoxYZSeparator[i]->removeAllChildren();
		d->BBoxXZSeparator[i]->removeAllChildren();

		d->AxisXYSeparator[i]->removeAllChildren();
		d->AxisYZSeparator[i]->removeAllChildren();
		d->AxisXZSeparator[i]->removeAllChildren();
	}
	for (auto i = 0; i < 4; i++) {
		//scale + text annotation
		d->NoteXSeparator[i]->removeAllChildren();
		d->NoteYSeparator[i]->removeAllChildren();
		d->NoteZSeparator[i]->removeAllChildren();
	}
}


auto OivAxisGrid::GenerateBoundaryBox(SbBox3f box, SoCamera* camera, float, int, int, int) -> bool {
	CleanBox();
	//attach Camera sensor
	d->camera3d = camera;
	d->mySensor =
		new SoFieldSensor(staticCB, this);
	d->mySensor->attach(&camera->position);

	//Bounding Box Construction
	float xmin, xmax, ymin, ymax, zmin, zmax;
	box.getBounds(xmin, ymin, zmin, xmax, ymax, zmax);
	auto pProp = new SoVertexProperty;
	pProp->vertex.set1Value(0, SbVec3f(xmin, ymin, zmin));
	pProp->vertex.set1Value(1, SbVec3f(xmax, ymin, zmin));
	pProp->vertex.set1Value(2, SbVec3f(xmax, ymax, zmin));
	pProp->vertex.set1Value(3, SbVec3f(xmin, ymax, zmin));
	pProp->vertex.set1Value(4, SbVec3f(xmin, ymin, zmax));
	pProp->vertex.set1Value(5, SbVec3f(xmax, ymin, zmax));
	pProp->vertex.set1Value(6, SbVec3f(xmax, ymax, zmax));
	pProp->vertex.set1Value(7, SbVec3f(xmin, ymax, zmax));

	auto bboxStyle = new SoDrawStyle;
	bboxStyle->lineWidth = 2.5;

	//XY Front Plane
	{
		int coordIndices[] = { 0, 1, 2, 3, 0 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxXYSeparator[0]->addChild(bboxStyle);
		d->BBoxXYSeparator[0]->addChild(d->bounding_matl.ptr());
		d->BBoxXYSeparator[0]->addChild(pLines);
	}
	//XY Back Plane
	{
		int coordIndices[] = { 4, 5, 6, 7, 4 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxXYSeparator[1]->addChild(bboxStyle);
		d->BBoxXYSeparator[1]->addChild(d->bounding_matl.ptr());
		d->BBoxXYSeparator[1]->addChild(pLines);
	}
	//YZ Left Plane
	{
		int coordIndices[] = { 0, 3, 7, 4, 0 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxYZSeparator[0]->addChild(bboxStyle);
		d->BBoxYZSeparator[0]->addChild(d->bounding_matl.ptr());
		d->BBoxYZSeparator[0]->addChild(pLines);
	}

	//YZ Right Plane
	{
		int coordIndices[] = { 1, 2, 6, 5, 1 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxYZSeparator[1]->addChild(bboxStyle);
		d->BBoxYZSeparator[1]->addChild(d->bounding_matl.ptr());
		d->BBoxYZSeparator[1]->addChild(pLines);
	}

	//XZ Upper Plane
	{
		int coordIndices[] = { 0, 1, 5, 4, 0 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxXZSeparator[0]->addChild(bboxStyle);
		d->BBoxXZSeparator[0]->addChild(d->bounding_matl.ptr());
		d->BBoxXZSeparator[0]->addChild(pLines);
	}

	//XZ Lower Plane
	{
		int coordIndices[] = { 2, 3, 7, 6, 2 };
		int numCoordIndices = sizeof(coordIndices) / sizeof(int);
		auto pLines = new SoIndexedLineSet;
		pLines->vertexProperty = pProp;
		pLines->coordIndex.setValues(0, numCoordIndices, coordIndices);

		d->BBoxYZSeparator[1]->addChild(bboxStyle);
		d->BBoxXZSeparator[1]->addChild(d->bounding_matl.ptr());
		d->BBoxXZSeparator[1]->addChild(pLines);
	}

	//Axis Grid Construction
	//Calculate 10 um - wise scales
	std::vector<float> x_points;
	std::vector<float> y_points;
	std::vector<float> z_points;
	std::vector<QString> x_anno;
	std::vector<QString> y_anno;
	std::vector<QString> z_anno;

	auto x_mid = xmin + (xmax - xmin) / 2.0;
	x_points.push_back(x_mid);
	x_anno.push_back("0");
	auto x_temp = -10;
	while (x_mid + x_temp >= xmin) {
		x_points.push_back(x_mid + x_temp);
		x_anno.push_back(QString::number(x_temp));
		x_temp -= 10;
	}
	x_temp = 10;
	while (x_mid + x_temp <= xmax) {
		x_points.push_back(x_mid + x_temp);
		x_anno.push_back(QString::number(x_temp));
		x_temp += 10;
	}

	auto y_mid = ymin + (ymax - ymin) / 2.0;
	y_points.push_back(y_mid);
	y_anno.push_back("0");
	auto y_temp = -10;
	while (y_mid + y_temp >= ymin) {
		y_points.push_back(y_mid + y_temp);
		y_anno.push_back(QString::number(y_temp));
		y_temp -= 10;
	}
	y_temp = 10;
	while (y_mid + y_temp <= ymax) {
		y_points.push_back(y_mid + y_temp);
		y_anno.push_back(QString::number(y_temp));
		y_temp += 10;
	}

	auto z_mid = zmin + (zmax - zmin) / 2.0;
	z_points.push_back(z_mid);
	z_anno.push_back("0");
	auto z_temp = -10;
	while (z_mid + z_temp >= zmin) {
		z_points.push_back(z_mid + z_temp);
		z_anno.push_back(QString::number(z_temp));
		z_temp -= 10;
	}
	z_temp = 10;
	while (z_mid + z_temp <= zmax) {
		z_points.push_back(z_mid + z_temp);
		z_anno.push_back(QString::number(z_temp));
		z_temp += 10;
	}

	//XY Front Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		for (auto i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
			grid_coordIndices.push_back(-1);
		}
		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisXYSeparator[0]->addChild(grid_pLines);
	}
	//XY Back Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		for (auto i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
			grid_coordIndices.push_back(-1);
		}

		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisXYSeparator[1]->addChild(grid_pLines);
	}
	//YZ Left Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;
		for (int i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
			grid_coordIndices.push_back(-1);
		}
		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisYZSeparator[0]->addChild(grid_pLines);
	}
	//YZ Right Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;
		for (int i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
			grid_coordIndices.push_back(-1);
		}
		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisYZSeparator[1]->addChild(grid_pLines);
	}
	//XZ Upper Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;
		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
			grid_coordIndices.push_back(-1);
		}
		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisXZSeparator[0]->addChild(grid_pLines);
	}
	//XZ Lower Plane
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;
		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
			grid_coordIndices.push_back(-1);
		}
		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
			grid_coordIndices.push_back(-1);
		}
		SbColorRGBA color2(0.4, 0.4, 0.4, 0.5);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		d->AxisXZSeparator[1]->addChild(grid_pLines);
	}

	//Note Construction
	//Shared Annotation node
	//auto d->textFont = new SoFont;
	d->textFont = new SoFont;
	d->textFont->setName("AxisGridFont");
	d->textFont->setFontPaths("font");
	d->textFont->size = 18;
	d->textFont->name = "NOTO SANS KR : REGULAR";
	d->textFont->renderStyle = SoFont::RenderStyle::TEXTURE;

	//X Annotation Text
	SoText2* annoLabelX = new SoText2;
	QString label_text("X [um]");
	label_text.replace("[u", QString("[") + QString::fromWCharArray(L"\x00B5"));
	annoLabelX->string = label_text.toStdWString();
	std::vector<SoText2*> annoX;
	for (auto i = 0; i < x_anno.size(); i++) {
		auto text = new SoText2;
		text->string = x_anno[i].toStdString();
		annoX.push_back(text);
	}
	//Y Annotation Text
	SoText2* annoLabelY = new SoText2;
	QString label_textY("Y [um]");
	label_textY.replace("[u", QString("[") + QString::fromWCharArray(L"\x00B5"));
	annoLabelY->string = label_textY.toStdWString();
	std::vector<SoText2*> annoY;
	for (auto i = 0; i < y_anno.size(); i++) {
		auto text = new SoText2;
		text->string = y_anno[i].toStdString();
		annoY.push_back(text);
	}
	//Z Annotation Text
	SoText2* annoLabelZ = new SoText2;
	QString label_textZ("Z [um]");
	label_textZ.replace("[u", QString("[") + QString::fromWCharArray(L"\x00B5"));
	annoLabelZ->string = label_textZ.toStdWString();
	std::vector<SoText2*> annoZ;
	for (auto i = 0; i < z_anno.size(); i++) {
		auto text = new SoText2;
		text->string = z_anno[i].toStdString();
		annoZ.push_back(text);
	}
	//X Upper Front    
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[0].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue((xmax - xmin) / 2, ymin - 7, zmin);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelX);
		text_root->addChild(label_sep);

		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 2, zmin));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(newX - 1, ymin - 4, zmin);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoX[i]);
			text_root->addChild(text_sep);
		}
		x_temp = -1;
		while (x_mid + x_temp >= xmin) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 1, zmin));
			grid_coordIndices.push_back(-1);
			x_temp--;
		}
		x_temp = 1;
		while (x_mid + x_temp <= xmax) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 1, zmin));
			grid_coordIndices.push_back(-1);
			x_temp++;
		}
		d->NoteXSeparator[0]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteXRotation[0]->axis.setValue(SoRotationXYZ::Axis::X);
		d->NoteXRotation[0]->angle.setValue(1.57);
		d->NoteXSeparator[0]->addChild(drawStyle);
		d->NoteXSeparator[0]->addChild(grid_pLines);
	}
	//X Lower Front
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[0].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue((xmax - xmin) / 2, ymax + 7, zmin);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelX);
		text_root->addChild(label_sep);

		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 2, zmin));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(newX - 1, ymax + 4, zmin);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoX[i]);
			text_root->addChild(text_sep);
		}
		x_temp = -1;
		while (x_mid + x_temp >= xmin) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 1, zmin));
			grid_coordIndices.push_back(-1);
			x_temp--;
		}
		x_temp = 1;
		while (x_mid + x_temp <= xmax) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 1, zmin));
			grid_coordIndices.push_back(-1);
			x_temp++;
		}
		d->NoteXSeparator[1]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteXTranslation[1]->translation.setValue(0, ymax, -zmax);
		d->NoteXRotation[1]->axis.setValue(SoRotationXYZ::Axis::X);
		d->NoteXRotation[1]->angle.setValue(-1.57);
		d->NoteXInvTranslation[1]->translation.setValue(0, -ymax, zmax);
		d->NoteXSeparator[1]->addChild(drawStyle);
		d->NoteXSeparator[1]->addChild(grid_pLines);
	}
	//X Upper Back
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[0].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue((xmax - xmin) / 2, ymin - 7, zmax);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelX);
		text_root->addChild(label_sep);

		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 2, zmax));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(newX - 1, ymin - 4, zmax);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoX[i]);
			text_root->addChild(text_sep);
		}
		x_temp = -1;
		while (x_mid + x_temp >= xmin) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 1, zmax));
			grid_coordIndices.push_back(-1);
			x_temp--;
		}
		x_temp = 1;
		while (x_mid + x_temp <= xmax) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymin - 1, zmax));
			grid_coordIndices.push_back(-1);
			x_temp++;
		}
		d->NoteXSeparator[2]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteXTranslation[2]->translation.setValue(0, 0, zmax);
		d->NoteXRotation[2]->axis.setValue(SoRotationXYZ::Axis::X);
		d->NoteXRotation[2]->angle.setValue(-1.57);
		d->NoteXInvTranslation[2]->translation.setValue(0, 0, -zmax);
		d->NoteXSeparator[2]->addChild(drawStyle);
		d->NoteXSeparator[2]->addChild(grid_pLines);
	}
	//X Lower Back
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;
		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[0].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue((xmax - xmin) / 2, ymax + 7, zmax);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelX);
		text_root->addChild(label_sep);

		for (int i = 0; i < x_points.size(); i++) {
			float newX = x_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 2, zmax));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(newX - 1, ymax + 4, zmax);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoX[i]);
			text_root->addChild(text_sep);
		}
		x_temp = -1;
		while (x_mid + x_temp >= xmin) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 1, zmax));
			grid_coordIndices.push_back(-1);
			x_temp--;
		}
		x_temp = 1;
		while (x_mid + x_temp <= xmax) {
			float newX = x_mid + x_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(newX, ymax + 1, zmax));
			grid_coordIndices.push_back(-1);
			x_temp++;
		}
		d->NoteXSeparator[3]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteXTranslation[3]->translation.setValue(0, ymax, zmax);
		d->NoteXRotation[3]->axis.setValue(SoRotationXYZ::Axis::X);
		d->NoteXRotation[3]->angle.setValue(1.57);
		d->NoteXInvTranslation[3]->translation.setValue(0, -ymax, -zmax);
		d->NoteXSeparator[3]->addChild(drawStyle);
		d->NoteXSeparator[3]->addChild(grid_pLines);
	}
	//Y Left Front
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[1].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmin - 7, (ymax - ymin) / 2, zmin);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelY);
		text_root->addChild(label_sep);

		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 2, newY, zmin));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmin - 4, newY + 1, zmin);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoY[i]);
			text_root->addChild(text_sep);
		}
		y_temp = -1;
		while (y_mid + y_temp >= ymin) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, newY, zmin));
			grid_coordIndices.push_back(-1);
			y_temp--;
		}
		y_temp = 1;
		while (y_mid + y_temp <= ymax) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, newY, zmin));
			grid_coordIndices.push_back(-1);
			y_temp++;
		}
		d->NoteYSeparator[0]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteYTranslation[0]->translation.setValue(-xmax, 0, -zmax);
		d->NoteYRotation[0]->axis.setValue(SoRotationXYZ::Axis::Y);
		d->NoteYRotation[0]->angle.setValue(-1.57);
		d->NoteYInvTranslation[0]->translation.setValue(xmax, 0, zmax);
		d->NoteYSeparator[0]->addChild(drawStyle);
		d->NoteYSeparator[0]->addChild(grid_pLines);
	}
	//Y Right Front
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[1].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmax + 7, (ymax - ymin) / 2, zmin);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelY);
		text_root->addChild(label_sep);

		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 2, newY, zmin));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmax + 4, newY + 1, zmin);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoY[i]);
			text_root->addChild(text_sep);
		}
		y_temp = -1;
		while (y_mid + y_temp >= ymin) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, newY, zmin));
			grid_coordIndices.push_back(-1);
			y_temp--;
		}
		y_temp = 1;
		while (y_mid + y_temp <= ymax) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmin));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, newY, zmin));
			grid_coordIndices.push_back(-1);
			y_temp++;
		}
		d->NoteYSeparator[1]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteYTranslation[1]->translation.setValue(xmax, 0, -zmax);
		d->NoteYRotation[1]->axis.setValue(SoRotationXYZ::Axis::Y);
		d->NoteYRotation[1]->angle.setValue(1.57);
		d->NoteYInvTranslation[1]->translation.setValue(-xmax, 0, zmax);
		d->NoteYSeparator[1]->addChild(drawStyle);
		d->NoteYSeparator[1]->addChild(grid_pLines);
	}
	//Y Left Back
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[1].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmin - 7, (ymax - ymin) / 2, zmax);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelY);
		text_root->addChild(label_sep);

		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 2, newY, zmax));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmin - 4, newY + 1, zmax);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoY[i]);
			text_root->addChild(text_sep);
		}
		y_temp = -1;
		while (y_mid + y_temp >= ymin) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, newY, zmax));
			grid_coordIndices.push_back(-1);
			y_temp--;
		}
		y_temp = 1;
		while (y_mid + y_temp <= ymax) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, newY, zmax));
			grid_coordIndices.push_back(-1);
			y_temp++;
		}
		d->NoteYSeparator[2]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteYTranslation[2]->translation.setValue(0, 0, zmax);
		d->NoteYRotation[2]->axis.setValue(SoRotationXYZ::Axis::Y);
		d->NoteYRotation[2]->angle.setValue(1.57);
		d->NoteYInvTranslation[2]->translation.setValue(0, 0, -zmax);
		d->NoteYSeparator[2]->addChild(drawStyle);
		d->NoteYSeparator[2]->addChild(grid_pLines);
	}
	//Y Right Back
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[1].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmax + 7, (ymax - ymin) / 2, zmax);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelY);
		text_root->addChild(label_sep);

		for (auto i = 0; i < y_points.size(); i++) {
			float newY = y_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 2, newY, zmax));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmax + 4, newY + 1, zmax);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoY[i]);
			text_root->addChild(text_sep);
		}
		y_temp = -1;
		while (y_mid + y_temp >= ymin) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, newY, zmax));
			grid_coordIndices.push_back(-1);
			y_temp--;
		}
		y_temp = 1;
		while (y_mid + y_temp <= ymax) {
			float newY = y_mid + y_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, newY, zmax));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, newY, zmax));
			grid_coordIndices.push_back(-1);
			y_temp++;
		}
		d->NoteYSeparator[3]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteYTranslation[3]->translation.setValue(xmax, 0, zmax);
		d->NoteYRotation[3]->axis.setValue(SoRotationXYZ::Axis::Y);
		d->NoteYRotation[3]->angle.setValue(-1.57);
		d->NoteYInvTranslation[3]->translation.setValue(-xmax, 0, -zmax);
		d->NoteYSeparator[3]->addChild(drawStyle);
		d->NoteYSeparator[3]->addChild(grid_pLines);
	}
	//Z Left Upper
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[2].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmin - 7, ymin, (zmax - zmin) / 2);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelZ);
		text_root->addChild(label_sep);

		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 2, ymin, newZ));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmin - 4, ymin, newZ - 1);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoZ[i]);
			text_root->addChild(text_sep);
		}

		z_temp = -1;
		while (z_mid + z_temp >= zmin) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, ymin, newZ));
			grid_coordIndices.push_back(-1);
			z_temp--;
		}
		z_temp = 1;
		while (z_mid + z_temp <= zmax) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, ymin, newZ));
			grid_coordIndices.push_back(-1);
			z_temp++;
		}

		d->NoteZSeparator[0]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteZTranslation[0]->translation.setValue(-xmax, -ymax, 0);
		d->NoteZRotation[0]->axis.setValue(SoRotationXYZ::Axis::Z);
		d->NoteZRotation[0]->angle.setValue(1.57);
		d->NoteZInvTranslation[0]->translation.setValue(xmax, ymax, 0);
		d->NoteZSeparator[0]->addChild(drawStyle);
		d->NoteZSeparator[0]->addChild(grid_pLines);
	}
	//Z Right Upper
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[2].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmax + 7, ymin, (zmax - zmin) / 2);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelZ);
		text_root->addChild(label_sep);

		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			//auto st = idx;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 2, ymin, newZ));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmax + 4, ymin, newZ - 1);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoZ[i]);
			text_root->addChild(text_sep);
		}
		z_temp = -1;
		while (z_mid + z_temp >= zmin) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, ymin, newZ));
			grid_coordIndices.push_back(-1);
			z_temp--;
		}
		z_temp = 1;
		while (z_mid + z_temp <= zmax) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymin, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, ymin, newZ));
			grid_coordIndices.push_back(-1);
			z_temp++;
		}
		d->NoteZSeparator[1]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteZTranslation[1]->translation.setValue(xmax, -ymax, 0);
		d->NoteZRotation[1]->axis.setValue(SoRotationXYZ::Axis::Z);
		d->NoteZRotation[1]->angle.setValue(-1.57);
		d->NoteZInvTranslation[1]->translation.setValue(-xmax, ymax, 0);
		d->NoteZSeparator[1]->addChild(drawStyle);
		d->NoteZSeparator[1]->addChild(grid_pLines);
	}
	//Z Left Lower
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[2].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmin - 7, ymax, (zmax - zmin) / 2);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelZ);
		text_root->addChild(label_sep);

		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 2, ymax, newZ));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmin - 4, ymax, newZ - 1);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoZ[i]);
			text_root->addChild(text_sep);
		}
		z_temp = -1;
		while (z_mid + z_temp >= zmin) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, ymax, newZ));
			grid_coordIndices.push_back(-1);
			z_temp--;
		}
		z_temp = 1;
		while (z_mid + z_temp <= zmax) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmin - 1, ymax, newZ));
			grid_coordIndices.push_back(-1);
			z_temp++;
		}
		d->NoteZSeparator[2]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteZTranslation[2]->translation.setValue(-xmax, ymax, 0);
		d->NoteZRotation[2]->axis.setValue(SoRotationXYZ::Axis::Z);
		d->NoteZRotation[2]->angle.setValue(-1.57);
		d->NoteZInvTranslation[2]->translation.setValue(xmax, -ymax, 0);
		d->NoteZSeparator[2]->addChild(drawStyle);
		d->NoteZSeparator[2]->addChild(grid_pLines);
	}
	//Z Right Lower
	{
		SoVertexProperty* gridProp = new SoVertexProperty;
		auto idx = 0;
		std::vector<int> grid_coordIndices;

		auto text_root = new SoSeparator;
		text_root->addChild(d->textFont.ptr());
		text_root->addChild(d->text_matl[2].ptr());
		auto label_sep = new SoSeparator;
		auto label_trans = new SoTranslation;
		label_trans->translation.setValue(xmax + 7, ymax, (zmax - zmin) / 2);
		label_sep->addChild(label_trans);
		label_sep->addChild(annoLabelZ);
		text_root->addChild(label_sep);

		for (auto i = 0; i < z_points.size(); i++) {
			float newZ = z_points[i];
			//auto st = idx;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 2, ymax, newZ));
			grid_coordIndices.push_back(-1);

			auto text_sep = new SoSeparator;
			auto text_trans = new SoTranslation;
			text_trans->translation.setValue(xmax + 4, ymax, newZ - 1);
			text_sep->addChild(text_trans);
			text_sep->addChild(annoZ[i]);
			text_root->addChild(text_sep);
		}
		z_temp = -1;
		while (z_mid + z_temp >= zmin) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, ymax, newZ));
			grid_coordIndices.push_back(-1);
			z_temp--;
		}
		z_temp = 1;
		while (z_mid + z_temp <= zmax) {
			float newZ = z_mid + z_temp;
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax, ymax, newZ));
			grid_coordIndices.push_back(idx);
			gridProp->vertex.set1Value(idx++, SbVec3f(xmax + 1, ymax, newZ));
			grid_coordIndices.push_back(-1);
			z_temp++;
		}
		d->NoteZSeparator[3]->addChild(text_root);
		SbColorRGBA color2(0.6, 0.6, 0.6, 1.0);
		gridProp->orderedRGBA.set1Value(0, color2.getPackedValue());

		int grid_numCoordIndices = static_cast<int>(grid_coordIndices.size());
		SoIndexedLineSet* grid_pLines = new SoIndexedLineSet;
		grid_pLines->vertexProperty = gridProp;
		grid_pLines->coordIndex.setValues(0, grid_numCoordIndices, grid_coordIndices.data());
		auto drawStyle = new SoDrawStyle;
		drawStyle->lineWidth = 2;
		d->NoteZTranslation[3]->translation.setValue(xmax, ymax, 0);
		d->NoteZRotation[3]->axis.setValue(SoRotationXYZ::Axis::Z);
		d->NoteZRotation[3]->angle.setValue(1.57);
		d->NoteZInvTranslation[3]->translation.setValue(-xmax, -ymax, 0);
		d->NoteZSeparator[3]->addChild(drawStyle);
		d->NoteZSeparator[3]->addChild(grid_pLines);
	}
	return true;
}

auto OivAxisGrid::SetPhysicalSize(double x, double y, double z) -> void {
	d->x_size = x;
	d->y_size = y;
	d->z_size = z;
}

auto OivAxisGrid::GetRootSceneGraph() -> SoSeparator* {
	return d->BBoxSeparator.ptr();
}

auto OivAxisGrid::SetAxisVisibility(bool visible) -> void {
	d->isBoundingBox = !visible;
	for (auto i = 0; i < 2; i++) {
		if (visible) {
			d->AxisXYSwitch[i]->whichChild = SO_SWITCH_ALL;
			d->AxisXZSwitch[i]->whichChild = SO_SWITCH_ALL;
			d->AxisYZSwitch[i]->whichChild = SO_SWITCH_ALL;
		} else {
			d->AxisXYSwitch[i]->whichChild = SO_SWITCH_NONE;
			d->AxisXZSwitch[i]->whichChild = SO_SWITCH_NONE;
			d->AxisYZSwitch[i]->whichChild = SO_SWITCH_NONE;
			//bounding box will show every plane
			d->XYSwitch[i]->whichChild = SO_SWITCH_ALL;
			d->YZSwitch[i]->whichChild = SO_SWITCH_ALL;
			d->XZSwitch[i]->whichChild = SO_SWITCH_ALL;
		}
	}
	for (auto i = 0; i < 4; i++) {
		if (!visible) {
			d->NoteXSwitch[i]->whichChild = -1;
			d->NoteYSwitch[i]->whichChild = -1;
			d->NoteZSwitch[i]->whichChild = -1;
		}
	}
	if (visible) {
		renderCB(nullptr);
	}
}

//CallBack Function Due to Camera View Point changed
void OivAxisGrid::staticCB(void* data, SoSensor*) {
	auto self = (OivAxisGrid*)data;
	self->renderCB(nullptr);
}

void OivAxisGrid::renderCB(SoSensor*) {
	//judge viewer direction here & hide or show planes    
	ManageViewProperty(d->camera3d->position.getValue());
}

auto OivAxisGrid::ManageViewProperty(SbVec3f camPos) -> void {
	if (d->isBoundingBox) {
		return;
	}
	float x, y, z;
	camPos.getValue(x, y, z);
	if (x > 0) {
		d->YZSwitch[0]->whichChild = -3;
		d->YZSwitch[1]->whichChild = -1;
	} else {
		d->YZSwitch[0]->whichChild = -1;
		d->YZSwitch[1]->whichChild = -3;
	}
	if (y > 0) {
		d->XZSwitch[0]->whichChild = -3;
		d->XZSwitch[1]->whichChild = -1;
	} else {
		d->XZSwitch[0]->whichChild = -1;
		d->XZSwitch[1]->whichChild = -3;
	}
	if (z > 0) {
		d->XYSwitch[0]->whichChild = -3;
		d->XYSwitch[1]->whichChild = -1;
	} else {
		d->XYSwitch[0]->whichChild = -1;
		d->XYSwitch[1]->whichChild = -3;
	}

	for (auto i = 0; i < 4; i++) {
		d->NoteXSwitch[i]->whichChild = -1;
		d->NoteYSwitch[i]->whichChild = -1;
		d->NoteZSwitch[i]->whichChild = -1;
	}

	if (x > 0) {//right
		if (y > 0) {//lower
			if (z > 0) {//back
				d->NoteXSwitch[1]->whichChild = 0;
				d->NoteXRotation[1]->angle.setValue(0);
				d->NoteYSwitch[1]->whichChild = 0;
				d->NoteYRotation[1]->angle.setValue(0);
				d->NoteZSwitch[1]->whichChild = 0;
				d->NoteZRotation[1]->angle.setValue(0);
			} else {//front
				d->NoteXSwitch[3]->whichChild = 0;
				d->NoteXRotation[3]->angle.setValue(0);
				d->NoteYSwitch[0]->whichChild = 0;
				d->NoteYRotation[0]->angle.setValue(-1.57);
				d->NoteZSwitch[2]->whichChild = 0;
				d->NoteZRotation[2]->angle.setValue(-1.57);
			}
		} else {//upper
			if (z > 0) {//back
				d->NoteXSwitch[3]->whichChild = 0;
				d->NoteXRotation[3]->angle.setValue(1.57);
				d->NoteYSwitch[1]->whichChild = 0;
				d->NoteYRotation[1]->angle.setValue(0);
				d->NoteZSwitch[3]->whichChild = 0;
				d->NoteZRotation[3]->angle.setValue(0);
			} else {//front
				d->NoteXSwitch[2]->whichChild = 0;
				d->NoteXRotation[2]->angle.setValue(0);
				d->NoteYSwitch[0]->whichChild = 0;
				d->NoteYRotation[0]->angle.setValue(-1.57);
				d->NoteZSwitch[0]->whichChild = 0;
				d->NoteZRotation[0]->angle.setValue(1.57);
			}
		}
	} else {//left
		if (y > 0) {//loewr
			if (z > 0) {//back
				d->NoteXSwitch[1]->whichChild = 0;
				d->NoteXRotation[1]->angle.setValue(0);
				d->NoteYSwitch[0]->whichChild = 0;
				d->NoteYRotation[0]->angle.setValue(0);
				d->NoteZSwitch[0]->whichChild = 0;
				d->NoteZRotation[0]->angle.setValue(0);
			} else {//front           
				d->NoteXSwitch[3]->whichChild = 0;
				d->NoteXRotation[3]->angle.setValue(0);
				d->NoteYSwitch[1]->whichChild = 0;
				d->NoteYRotation[1]->angle.setValue(1.57);
				d->NoteZSwitch[3]->whichChild = 0;
				d->NoteZRotation[3]->angle.setValue(1.57);
			}
		} else {//upper
			if (z > 0) {//back
				d->NoteXSwitch[0]->whichChild = 0;
				d->NoteXRotation[0]->angle.setValue(0);
				d->NoteYSwitch[3]->whichChild = 0;
				d->NoteYRotation[3]->angle.setValue(-1.57);
				d->NoteZSwitch[1]->whichChild = 0;
				d->NoteZRotation[1]->angle.setValue(-1.57);
			} else {//front
				d->NoteXSwitch[1]->whichChild = 0;
				d->NoteXRotation[1]->angle.setValue(-1.57);
				d->NoteYSwitch[1]->whichChild = 0;
				d->NoteYRotation[1]->angle.setValue(1.57);
				d->NoteZSwitch[1]->whichChild = 0;
				d->NoteZRotation[1]->angle.setValue(-1.57);
			}
		}
	}
}


auto OivAxisGrid::SetFontSize(int size) -> void {
	d->textFont->size = size;
}
