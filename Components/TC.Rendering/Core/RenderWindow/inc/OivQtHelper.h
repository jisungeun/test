/*=======================================================================
 *** THE CONTENT OF THIS WORK IS PROPRIETARY TO FEI S.A.S, (FEI S.A.S.),            ***
 ***              AND IS DISTRIBUTED UNDER A LICENSE AGREEMENT.                     ***
 ***                                                                                ***
 ***  REPRODUCTION, DISCLOSURE,  OR USE,  IN WHOLE OR IN PART,  OTHER THAN AS       ***
 ***  SPECIFIED  IN THE LICENSE ARE  NOT TO BE  UNDERTAKEN  EXCEPT WITH PRIOR       ***
 ***  WRITTEN AUTHORIZATION OF FEI S.A.S.                                           ***
 ***                                                                                ***
 ***                        RESTRICTED RIGHTS LEGEND                                ***
 ***  USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT OF THE CONTENT OF THIS      ***
 ***  WORK OR RELATED DOCUMENTATION IS SUBJECT TO RESTRICTIONS AS SET FORTH IN      ***
 ***  SUBPARAGRAPH (C)(1) OF THE COMMERCIAL COMPUTER SOFTWARE RESTRICTED RIGHT      ***
 ***  CLAUSE  AT FAR 52.227-19  OR SUBPARAGRAPH  (C)(1)(II)  OF  THE RIGHTS IN      ***
 ***  TECHNICAL DATA AND COMPUTER SOFTWARE CLAUSE AT DFARS 52.227-7013.             ***
 ***                                                                                ***
 ***                   COPYRIGHT (C) 1996-2021 BY FEI S.A.S,                        ***
 ***                        BORDEAUX, FRANCE                                        ***
 ***                      ALL RIGHTS RESERVED                                       ***
 **=======================================================================*/
 /*=======================================================================
 ** Author      : VSG (MMM YYYY)
 **=======================================================================*/

#pragma once

#include <QCoreApplication>
#include <QFile>
#include <QString>

#include <Inventor/SoPreferences.h>
#include <Inventor/helpers/SbFileHelper.h>
#include <Inventor/sys/SoDynamicLibManager.h>

 /**
  * @VSGEXT Utilities class for Qt management.
  *
  * @ingroup ViewerComponentsQt
  *
  * [OIV-WRAPPER-NO-WRAP]
  */
class OivQtHelper
{
public:
	/**
	 * When a Qt5 application is run, Qt will first treat the application's executable
	 * directory as the base directory for searching for platform plugins. The purpose
	 * of this function is to load Qt platform plugins by guessing the Qt platforms path
	 * from the Open Inventor binaries path.
	 *
	 * The Qt environment variable QT_QPA_PLATFORM_PLUGIN_PATH also enables to specify
	 * the platform plugins path. Nevertheless, the use of both this environment variable
	 * and the Qt method QCoreApplication::addLibraryPath causes significant load delay.
	 *
	 * Qt platform plugins are stored in $OIVHOME/$OIVHARCH/bin/platforms
	 *
	 * \param path : force the directory for searching for platforms plugins.
	 */
	static void
		addPlatformPluginsPath(QString path = "")
	{
		// if the QApplication alraady exists, we can safely assume everything is in order
		// if there is a qt.conf file, we do not want to apply our library path modifications
		// as we should trust the qt.conf to do everything that is required.
		const bool qtconfExists = QFile::exists("qt.conf");
		if (qApp || qtconfExists)
			return;

		if (SoPreferences::getString("QT_QPA_PLATFORM_PLUGIN_PATH", "").isEmpty())
		{
			if (path.isEmpty())
			{
				// Try to guess QT platforms path from Open Inventor binaries path
				const QString pluginPath = qtPluginsPath();
				if (SbFileHelper::isAccessible(pluginPath.toStdString()))
				{
					QCoreApplication::addLibraryPath(pluginPath);
				}
			}
			else
			{
				QCoreApplication::addLibraryPath(path);
			}
		}
	}

	/**
	 * Returns the path of the Qt storage plugin.
	 * Used in case of QML to specify the QML's import path
	 */
	static QString
		qtPluginsPath()
	{
		const SbString binariesPath = SoDynamicLibManager::getLibraryFromSymbol((void*)SoInventorBase::init);
		const QString qtPlatformsPath = SbFileHelper::toUnixPath(SbFileHelper::getDirName(binariesPath) + "qtplugins/").toStdString().c_str();
		return qtPlatformsPath;
	}
};
