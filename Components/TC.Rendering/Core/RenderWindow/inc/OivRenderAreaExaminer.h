#pragma once

#include <Inventor/ViewerComponents/nodes/SceneExaminer.h>
#include <Inventor/ViewerComponents/nodes/SiInteractionModeListener.h>
#include <qcursor.h>

#include "OivRenderAreaInteractive.h"
#include "TC.Rendering.Core.RenderWindowExport.h"

/**
 * Class to render an OpenInventor scene graph in a Qt OpenGL window.
 * This class extends RenderAreaInteractive to add examiner viewer behaviors.
 *
 * Note: When using this component, the automatic interactive mode is disabled,
 * see SoSceneManager::setAutoInteractiveMode for more details.
 * Interactive mode is managed by the SceneExaminer node.
 *
 * @ingroup ViewerComponentsQt
 */
class TC_Rendering_Core_RenderWindow_API OivRenderAreaExaminer : public OivRenderAreaInteractive, public SiInteractionModeListener
{
	Q_OBJECT
public:
	static QCursor* s_seekCursor;
	static QCursor* s_viewingCursor;
	/** Constructor */
	OivRenderAreaExaminer(QWidget* parent);

	/**
	 * Returns the scene examiner.
	 * @return The scene examiner
	 */
	SoDEPRECATED_METHOD(10500, "Use getSceneInteractor() method instead.")
		SceneExaminer* getSceneExaminer();

	/**
	 * Returns the scene examiner.
	 * @return The scene examiner
	 */
	virtual SceneExaminer* getSceneInteractor() const override;

	/**
	* Sets navigation mode.
	*/
	virtual void setNavigationMode(SceneExaminer::NavigationMode mode);

	/**
	* Returns the current navigation mode.
	*/
	virtual SceneExaminer::NavigationMode getNavigationMode();

	/**
	 * This method is called when the seek mode is activated and deactivated.
	 */
	void seekModeChanged(const bool onOrOff);

	/**
	 * This method is called when the interaction has changed.
	 */
	void interactionModeChanged(SceneExaminer::InteractionMode mode);
signals:
	void fpsValue(float);
private:
	auto sendFPS(float fps) -> void override;
	
	SceneExaminer* m_examinerRootSceneGraph;
	void updateInteractionCursor();

};