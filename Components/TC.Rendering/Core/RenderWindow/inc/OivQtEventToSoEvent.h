#pragma once

#include <QKeyEvent>
#include <QMouseEvent>
#include <QTouchEvent>
#include <QWheelEvent>

#include <Inventor/ViewerComponents/SoEventBuilder.h>

#include "TC.Rendering.Core.RenderWindowExport.h"

/**
 * @ingroup ViewerComponentsQt
 */
class TC_Rendering_Core_RenderWindow_API OivQtEventToSoEvent
{
public:
	/**
	 * Transform QMousePressEvent to SoMouseButtonEvent
	 * @param qevent Qt event to transform
	 * @param eventPosition event position transformed to be OpenInventor complient
	 */
	static SoMouseButtonEvent*
		getMousePressEvent(QMouseEvent* qevent, QPoint eventPosition)
	{
		return m_ivEvent.getMousePressEvent(eventPosition.x(),
			eventPosition.y(),
			getButtonId(qevent),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QMouseReleaseEvent to SoMouseButtonEvent
	 * @param qevent Qt event to transform
	 * @param eventPosition event position transformed to be OpenInventor complient
	 */
	static SoMouseButtonEvent*
		getMouseReleaseEvent(QMouseEvent* qevent, QPoint eventPosition)
	{
		return m_ivEvent.getMouseReleaseEvent(eventPosition.x(),
			eventPosition.y(),
			getButtonId(qevent),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QMouseDoubleClickEvent to SoMouseButtonEvent
	 * @param qevent Qt event to transform
	 * @param eventPosition event position transformed to be OpenInventor complient
	 */
	static SoMouseButtonEvent*
		getMouseDoubleClickEvent(QMouseEvent* qevent, QPoint eventPosition)
	{
		return m_ivEvent.getMouseDoubleClickEvent(eventPosition.x(),
			eventPosition.y(),
			getButtonId(qevent),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QWheelEvent to SoMouseWheelEvent
	 * @param qevent Qt event to transform
	 */
	static SoMouseWheelEvent*
		getMouseWheelEvent(QWheelEvent* qevent)
	{
		return m_ivEvent.getMouseWheelEvent(qevent->angleDelta().y(),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QMouseEvent to SoLocation2Event
	 * @param qevent Qt event to transform
	 * @param eventPosition event position transformed to be OpenInventor complient
	 */
	static SoLocation2Event*
		getMouseMoveEvent(QMouseEvent* qevent, QPoint eventPosition)
	{
		return m_ivEvent.getMouseMoveEvent(eventPosition.x(),
			eventPosition.y(),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform mouse enter event to SoLocation2Event
	 */
	static SoLocation2Event*
		getMouseEnterEvent()
	{
		return m_ivEvent.getMouseEnterEvent(0, 0, false, false, false);
	}

	/**
	 * Transform mouse leave event to SoLocation2Event
	 */
	static SoLocation2Event*
		getMouseLeaveEvent()
	{
		return m_ivEvent.getMouseLeaveEvent(0, 0, false, false, false);
	}

	/**
	 * Transform QHoverEvent to SoLocation2Event
	 * @param qevent Qt event to transform
	 * @param eventPosition event position transformed to be OpenInventor complient
	 */
	static SoLocation2Event*
		getHoverMouseEvent(QHoverEvent* qevent, QPoint eventPosition)
	{
		return m_ivEvent.getMouseMoveEvent(eventPosition.x(),
			eventPosition.y(),
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QKeyEvent press to SoKeyboardEvent
	 * @param qevent Qt event to transform
	 */
	static SoKeyboardEvent*
		getKeyPressEvent(QKeyEvent* qevent)
	{
		SoKeyboardEvent::Key ivKey = getIvKey(qevent);
		return m_ivEvent.getKeyPressEvent(ivKey,
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QKeyEvent release to SoKeyboardEvent
	 * @param qevent Qt event to transform
	 */
	static SoKeyboardEvent*
		getKeyReleaseEvent(QKeyEvent* qevent)
	{
		SoKeyboardEvent::Key ivKey = getIvKey(qevent);
		return m_ivEvent.getKeyReleaseEvent(ivKey,
			qevent->modifiers() & Qt::AltModifier,
			qevent->modifiers() & Qt::ControlModifier,
			qevent->modifiers() & Qt::ShiftModifier);
	}

	/**
	 * Transform QTouchEvent to a list of SoEvent representing the touch event
	 * @param qevent Qt event to transform
	 * @param height Height of the widget where the event is firered. it's used to transform event position to OpenInventor complient position
	 */
	const std::vector<const SoEvent*>& getTouchEvents(QTouchEvent* qevent, int height);

protected:

	/**
	 * Utilitary function to get the OpenInventor button Id for a given QMouseEvent
	 * @param qevent QMouseButton event used to get button id
	 */
	static SoMouseButtonEvent::Button getButtonId(QMouseEvent* qevent);

	/**
	 * Events builder used to transform Qt events to SoEvents
	 */
	static SoEventBuilder m_ivEvent;

private:
	static bool initClass();
	static bool s_init;

	static SoKeyboardEvent::Key getIvKey(QKeyEvent* qevent);

	std::vector<const SoEvent*> m_soeventlist;

	// Mapping from QT virtual keys to SoKeyboardEvent::Key enum
	static SoKeyboardEvent::Key keyMap[256];
	static SoKeyboardEvent::Key keyMap2[97];
	static int keyMapInitFlag;
};
