#pragma once

#include <Inventor/sys/port.h>
/*
#if defined(_WIN32) && !defined(OIV_DISABLE_AUTOLINK) && !defined(QtViewerComponents_EXPORTS)
#  if !defined(__QT_VIEWER_COMPONENTS_LIB)
#    if defined(_DEBUG)
#      define __QT_VIEWER_COMPONENTS_LIB "fei_inventor_QtViewerComponentsD.lib"
#    else
#      define __QT_VIEWER_COMPONENTS_LIB "fei_inventor_QtViewerComponents.lib"
#    endif
#  endif
#  pragma comment(lib, __QT_VIEWER_COMPONENTS_LIB)
#endif
*/
#include <QWindow>

#include <Inventor/misc/SoRef.h>
#include <Inventor/sys/SoGL.h>
#include <Inventor/ViewerComponents/SoRenderAreaCore.h>
#include <Inventor/actions/SoAction.h>
#include <Inventor/SbColorRGBA.h>

#include <Inventor/sys/port.h>

#include "TC.Rendering.Core.RenderWindowExport.h"

class SoNode;
class QOpenGLPaintDevice;

/**
 * Class to render an OpenInventor scene in a Qt OpenGL window
 *
 * @ingroup ViewerComponentsQt
 *
 * @DESCRIPTION
 *
 * OivRenderArea creates a 3D rendering window as a child of another widget.
 * Use the setSceneGraph() method to specify the scene graph to be rendered.
 *
 * A SceneExaminer node is typically used to allow the user to manipulate the scene camera.
 *
 * @SEE_ALSO
 *    RenderAreaInteractive, RenderAreaExaminer, SceneInteractor, SceneExaminer
 */
class TC_Rendering_Core_RenderWindow_API OivRenderArea : public QWindow, public SiRenderArea, public SiRenderAreaStereo, public SiRenderAreaAntialiasing, public SiRenderAreaStillSuperSampling
{
public:
	/**
	 * This constructor initializes the QOpenGLWidget using the default QSurfaceFormat.
	 */
	OivRenderArea(QWidget* parent);

	/** Destructor */
	virtual ~OivRenderArea();

	/**
	 * Sets the scene graph to render.
	 */
	virtual void setSceneGraph(SoNode* sceneGraph);

	/**
	 * Gets the scene graph to render.
	 */
	virtual SoNode* getSceneGraph() const;

	/**
	 * Defines the color buffer and depth buffer clear policy.
	 * @useenum{ClearPolicy} Default is COLORBUFFER_AND_DEPTHBUFFER.
	 */
	virtual void setClearPolicy(ClearPolicy policy);

	/**
	 * Gets the color buffer and depth buffer clear policy.
	 */
	virtual ClearPolicy getClearPolicy() const;

	/**
	 * Defines the RGBA value used when the color buffer is cleared.
	 * Default is transparent black (0,0,0,0).
	 */
	virtual void setClearColor(const SbColorRGBA& color);

	/**
	 * Gets the RGBA value used when the color buffer is cleared.
	 */
	virtual SbColorRGBA getClearColor() const;

	/**
	 * Defines the depth value used when the depth buffer is cleared.
	 * Default is 1. Range is 0..1.
	 */
	virtual void setClearDepth(float depth);

	/**
	 * Gets the depth value used when the depth buffer is cleared.
	 */
	virtual float getClearDepth() const;

	/**
	 * Defines the size used for rendering.
	 */
	virtual void setSize(const SbVec2i32& size);
	
	/**
	 * Gets the size used for rendering.
	 */
	virtual SbVec2i32 getSize() const;

	/**
	 * Defines the render action used for rendering.
	 */
	virtual void setGLRenderAction(SoGLRenderAction* glAction);

	/**
	 * Returns the render action used for rendering.
	 * This method returns nullptr if the show() of the main window is not done.
	 */
	virtual SoGLRenderAction* getGLRenderAction() const;

	/**
	 * Sets the global transparency algorithm to use when rendering.
	 * @param type The global transparency algorithm
	 */
	virtual void setTransparencyType(SoGLRenderAction::TransparencyType type);

	/**
	 * Gets the global transparency algorithm to use when rendering.
	 */
	virtual SoGLRenderAction::TransparencyType getTransparencyType() const;

	/**
	* Define the antialiasing mode.
	* @param mode The antialiasing algorithm. Default is NO_ANTIALIASING which turns off antialiasing.
	*/
	virtual void setAntialiasingMode(SoSceneManager::AntialiasingMode mode) override;

	/**
	* @see setAntialiasingMode().
	*/
	virtual SoSceneManager::AntialiasingMode getAntialiasingMode() const override;

	/**
	* Define the antialiasing quality value.
	* @param quality The quality is a factor in the range [0.0,1.0]. @BR
	*        Default is 0.0. The value 0.0 turns off antialiasing.
	*/
	virtual void setAntialiasingQuality(float quality) override;

	/**
	* @see setAntialiasingQuality().
	*/
	virtual float getAntialiasingQuality() const override;

	/**
	* Set quality for supersampling when "still" (not interacting).
	* When quality is greater than 0, still images will be automatically supersampled.
	* @param quality The quality is a factor in the range [0.0,1.0]. @BR
	*        Default value is 0.0. Use the value 0.0 to turn off still supersampling. 0.5 is a typical value.
	* See also setStillSuperSamplingDelay.
	*/
	virtual void setStillSuperSamplingQuality(float quality) override;

	/**
	* @see setStillSuperSamplingQuality().
	*/
	virtual float getStillSuperSamplingQuality() const override;

	/**
	* Set delay for supersampling when "still" (not interacting).
	* If greater than 0, images will be supersampled after the specified delay.
	* @param delay The delay is in milliseconds. @BR
	*        If greater than 0, images will be supersampled after the specified delay
	* See also setStillSuperSamplingQuality.
	*/
	virtual void setStillSuperSamplingDelay(unsigned int delay) override;

	/**
	* @see setStillSuperSamplingDelay().
	*/
	virtual unsigned int getStillSuperSamplingDelay() const override;

	/**
	 * Returns the event handler that raises when a new render starts.
	 */
	virtual SbEventHandler<RenderEventArg&>& onStartRender();

	/**
	 * Activate/Deactivate stereo.
	 */
	virtual void activateStereo(bool activated);

	/**
	 * Returns true if stereo rendering is currently activated.
	 *
	 * @NOTES
	 *   Stereo status can only be retrieved once rendering area is initialized.
	 */
	virtual bool isStereoActivated() const;

	/**
	 * Defines the stereo parameters.
	 *
	 * @NOTES
	 *   Stereo parameters can only be set once rendering area is initialized.
	 */
	virtual void setStereoParameters(SoStereoParameters* parameters);

	/**
	 * @see setStereoParameters().
	 */
	virtual SoStereoParameters* getStereoParameters() const;

	/**
	 * Returns true if stereo buffering is enabled.
	 */
	bool isRawStereoAvailable();

	QWidget*
		getContainerWidget()
	{
		return m_containerWidget;
	}

protected:
	/**
	 * This method is called when Qt wants to render the OpenGL surface.
	 * It is called also when OpenInventor needs an update through the oivRenderCB function.
	 * OpenInventor must be able to inform us of the required update when the scenegraph is
	 * modified.
	 */
	virtual RenderStatus render();

	/**
	 * This method is called by Qt in order to treat events
	 */
	virtual bool event(QEvent* e);

	/**
	 * This method is called by Qt in order to allow custom parameters
	 * for OpenGL.
	 * This function is used in this demo to enable ZBuffer test.
	 */
	virtual void initializeGL();

	/**
	 * This method is called by Qt when the size of the widget is modified.
	 * We need to know when the size is modified in order to update the viewport for
	 * the internal computation of the clipping planes, aspect ratio, mouse coordinates...
	 *
	 * It is called when the widget is displayed for the first time and when the size changes.
	 */
	virtual void resizeEvent(QResizeEvent* e);

	/**
	 * This method is called by Qt when widget need to be rendered.
	 */
	virtual void exposeEvent(QExposeEvent* e);

protected:
	/**
	 * Instance of the class which provides the algorithms for viewing and managing
	 * OpenInventor scene graphs.
	 */
	SoRef<SoRenderAreaCore> m_renderAreaCore;

	/** Member for each setting which can be set after initializeGL call */
	SoNode* m_sceneGraph;
	ClearPolicy m_clearPolicy;
	SbColorRGBA m_color;
	float m_depth;
	SbVec2i32 m_size;
	bool m_stereo;
	double prev_time;

	/** GLRenderAction used by this render area */
	SoGLRenderAction* m_glRenderAction;

	/**
	 * Transparency type used by this render area
	 */
	SoGLRenderAction::TransparencyType m_transparencyType;

	/**
	 * Antialiasing mode used by this render area
	 */
	SoSceneManager::AntialiasingMode m_antialiasingMode;

	/**
	 * Antialiasing quality used by this render area
	 */
	float m_antialiasingQuality;

	/**
	 * Delay for still supersampling
	 */
	unsigned int m_stillAntialiasingDelay;

	/**
	 * Quality of still supersampling
	 */
	float m_stillAntialiasingQuality;

	/** Qt container widget */
	QWidget* m_containerWidget;

	/** Store FBO information to provide save/restore mechanism. */
	struct RenderAreaSaveRestore
	{
		RenderAreaSaveRestore();
		virtual ~RenderAreaSaveRestore();
		/** Viewport size of the previous frame */
		SbVec2i32 m_previousRenderingSize;
		/** Id of the backup framebuffer */
		GLuint m_backupFBOId;
		/** Id of the color texture in the backup FBO */
		GLuint m_colorTextureId;
		/** Id of the depth-stencil texture in the backup FBO */
		GLuint m_depthStencilTextureId;
		/** Store context used to create FBO */
		SoRef<SoGLContext> m_context;
	};

#if SoDEPRECATED_BEGIN( 9900 )
	/** Stores the content of the current FBO in a backup FBO. Called when rendering is successful. */
	SoDEPRECATED_METHOD_NOWARN(9900, "This function is useless with new implementation of Qt OivRenderArea")
		void saveRenderArea() {}

	/** Writes the content of the backup FBO in the current FBO. Called when traversal is aborted. */
	SoDEPRECATED_METHOD_NOWARN(9900, "This function is useless with new implementation of Qt OivRenderArea")
		void restoreRenderArea() {}
#endif /** @DEPRECATED_END */

private:
	void init();

	bool m_needInitGL;
	SoRef<SoGLContext> m_context;
	QOpenGLContext* m_qtContext;
	QOpenGLPaintDevice* m_device;
};