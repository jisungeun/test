#pragma once

#include <Inventor/ViewerComponents/nodes/SceneOrbiter.h>

#include "OivRenderAreaInteractive.h"
#include "TC.Rendering.Core.RenderWindowExport.h"

/**
 * @PREVIEWTAG
 * Class to render an OpenInventor scene graph in a Qt OpenGL window.
 * This class extends RenderAreaInteractive to add scene orbiter viewer behaviors.
 * Unlike the RenderAreaExaminer, the scene orbiter is a "mode-less" viewer.
 *
 * Note: When using this component, the automatic interactive mode is disabled,
 * see SoSceneManager::setAutoInteractiveMode for more details.
 * Interactive mode is managed by the SceneOrbiter node.
 *
 * @SEE_ALSO
 *    SceneOrbiter
 *
 * @ingroup ViewerComponentsQt
 *
 * @PREVIEWFEATURES
 */
class TC_Rendering_Core_RenderWindow_API OivRenderAreaOrbiter : public OivRenderAreaInteractive
{
public:
	/** Constructor */
	OivRenderAreaOrbiter(QWidget* parent);

	/**
	 * Returns the scene orbiter.
	 * @return the scene orbiter
	 */
	virtual SceneOrbiter* getSceneInteractor() const;

private:
	SceneOrbiter* m_sceneOrbiter;

};
