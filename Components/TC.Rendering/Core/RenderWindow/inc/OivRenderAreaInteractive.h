#pragma once

#include "OivRenderArea.h"
#include "OivQtEventToSoEvent.h"

#include "TC.Rendering.Core.RenderWindowExport.h"

class QRenderAreaCore;
class QMouseEvent;
class QWheelEvent;
class QKeyEvent;

class SceneInteractor;
class SoGroup;
class SoText2;

/**
 * Class to render an OpenInventor scene in a Qt OpenGL window.
 * This class extends RenderArea to add mouse and keyboard interactions.
 *
 * @ingroup ViewerComponentsQt
 */
class TC_Rendering_Core_RenderWindow_API OivRenderAreaInteractive : public OivRenderArea, public SiRenderAreaInteractive
{
public:

	/**
	* Camera clipping planes adjustment mode.
	* When adjustment mode is set to AUTO, the camera near and far planes
	* are dynamically adjusted to be as tight as possible (least amount of stuff
	* is clipped) before each render traversal.
	* When adjustment mode is set to MANUAL, the user is expected to
	* manually set the camera near and far planes.
	*/
	enum ClippingPlanesAdjustMode
	{
		AUTO,
		MANUAL
	};

	/**
	 * This constructor initializes the QOpenGLWidget using the default QSurfaceFormat.
	 */
	OivRenderAreaInteractive(QWidget* parent);

	/**
	 * Destructor.
	 */
	virtual ~OivRenderAreaInteractive();

	/*
	 * Sets the scene graph to render.
	 */
	virtual void setSceneGraph(SoNode* sceneGraph);

	/**
	 * Set the camera clipping planes adjustment mode.
	 * When adjustment mode is set to AUTO, the camera near and far planes
	 * are dynamically adjusted to be as tight as possible (least amount of stuff
	 * is clipped) before each render traversal.
	 * When adjustment mode is set to MANUAL, the user is expected to
	 * manually set those planes. Updating clipping planes after a camera move is
	 * not enough, if a dragger or a rendered shape is moved, they can disappear
	 * or become partially clipped.
	 * Default is AUTO.
	 */
	void setClippingPlanesAdjustMode(ClippingPlanesAdjustMode mode);

	/**
	 * Get the camera clipping planes adjustment mode.
	 */
	ClippingPlanesAdjustMode getClippingPlanesAdjustMode();

	void ToggleFPS(bool show);

	/**
	 * Return the root of the scene graph.
	 */
	SoDEPRECATED_METHOD(10500, "Use getSceneInteractor() method instead.")
		SceneInteractor* getRootSceneGraph() const;

	/**
	 * Return the root of the scene graph.
	 */
	virtual SceneInteractor* getSceneInteractor() const;

	/**
	 * Move the camera to view the scene defined by the given path.
	 * Equivalent to calling the SoCamera method viewAll(). Camera position is changed, but not orientation.
	 */
	virtual void viewAll(const SbViewportRegion& viewport);

	/**
	 * Moves the camera to be aligned with the given direction vector while
	 * keeping the "up" direction of the camera parallel to the specified up
	 * vector.
	 */
	virtual void viewAxis(const SbVec3f& direction, const SbVec3f& up);

	/**
	 * Activate/Deactivate stereo.
	 */
	virtual void activateStereo(bool activated);

	/**
	 * Returns true if stereo can be activated.
	 */
	virtual bool isStereoSupported() const;

	/**
	 * Set the stereo offset.
	 */
	void setStereoCameraOffset(float offset);

	/**
	 * Set the stereo balance.
	 */
	void setStereoCameraBalance(float balance);

#if SoDEPRECATED_BEGIN(101000)
	/**
	 * Sets the current interactive mode.
	 */
	SoDEPRECATED_METHOD(101000, "Use SoInteractiveComplexity node instead.")
		void setInteractiveMode(SoInteractiveComplexity::InteractiveMode mode);

	/**
	 * Gets the current interactive mode.
	 */
	SoDEPRECATED_METHOD(101000, "Use SoInteractiveComplexity node instead.")
		virtual SoInteractiveComplexity::InteractiveMode getInteractiveMode() const;
#endif /** @DEPRECATED_END */

	/**
	 * Processes the passed event to the scene graph managed here.
	 * Returns TRUE if the event was handled by a node.
	 *
	 * @NOTES
	 *   Events can only be processed once rendering area is initialized.
	 */
	virtual SbBool processEvent(const SoEvent* event);

	/**
	 * Processes the passed event to the scene graph managed here.
	 * Returns TRUE if at least one event was handled by a node.
	 *
	 * @NOTES
	 *   Events can only be processed once rendering area is initialized.
	 */
	virtual SbBool processEvents(const std::vector<const SoEvent*>& eventList);

protected:

	OivRenderAreaInteractive(QWidget* parent, bool buildRootSceneGraph);

	/**
	 * Render the scene graph.
	 */
	virtual SoRenderAreaCore::RenderStatus render();

	/**
	 * This method is called by Qt in order to allow custom parameters for OpenGL.
	 * This function is used in this demo to enable ZBuffer test.
	 */
	virtual void initializeGL();

	/**
	 * This method is called by Qt when a mouse button is pressed.
	 * It starts the trackball rotation.
	 *
	 * This function also manage the interactive mode.
	 * We switch on interactive (FORCE_INTERACTION) mode when we press a mouse button,
	 * and switch back to AUTO mode when we release the button.
	 */
	virtual void mousePressEvent(QMouseEvent* event);

	/**
	 * This method is called by Qt when a mouse button is released.
	 * It ends the trackball rotation.
	 */
	virtual void mouseReleaseEvent(QMouseEvent* event);

	/**
	 * This method is called by Qt when the mouse moves.
	 * It updates the camera position if a mouse button is pressed.
	 */
	virtual void mouseMoveEvent(QMouseEvent* event);

	/**
	 * This method is called by Qt when the mouse wheel is used.
	 * It is used to change the zoom level.
	 */
	virtual void wheelEvent(QWheelEvent* event);

	/**
	 * This method is called by Qt when a key is pressed.
	 */
	virtual void keyPressEvent(QKeyEvent* event);

	/**
	 * This method is called by Qt when a key is released.
	 */
	virtual void keyReleaseEvent(QKeyEvent* event);

	/**
	 * This method is called by Qt when the cursor enters the window.
	 */
	virtual void enterEvent(QEvent* event);

	/**
	 * This method is called by Qt when the cursor leaves the window.
	 */
	virtual void leaveEvent(QEvent* event);

	/**
	 * This method is called by Qt when a double-click occurs.
	 */
	virtual void mouseDoubleClickEvent(QMouseEvent* event);

	/**
	 * This method is called by Qt for all events.
	 * It is overridden to handle touch events.
	 */
	virtual bool event(QEvent* qevent);

	/**
	 * Build internal scene graph.
	 */
	void buildSceneGraph();

	SceneInteractor* m_rootSceneGraph;
	// Auto interactive mode
	bool m_isAutoInteractive;
private:

	void init(bool buildRootSceneGraph);

	void startRender(SiRenderArea::RenderEventArg& arg);

	/**
	* Check if there are event pending on system event queue and ask for
	* aborting event to keep interactivity.
	*/
	static SbBool s_abortRenderCallback(SoAction* action, void*);
	static void s_renderCallback(void* , SoSceneManager*);
	virtual auto sendFPS(float fps)->void= 0;

	SoGroup* m_appSceneGraph;
	SoSwitch* m_fpsSwitch;
	SoSeparator* m_fpsSceneGraph;
	SoText2* m_fpsText;
	ClippingPlanesAdjustMode m_clippingMode;
	SoInteractiveComplexity::InteractiveMode m_interactiveMode;

	OivQtEventToSoEvent m_eventbuilder;

	bool m_isDragging;
	unsigned long lastTime{ 0 };
};