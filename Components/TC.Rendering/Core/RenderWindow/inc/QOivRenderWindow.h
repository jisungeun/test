#pragma once

#include <QWidget>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/ViewerComponents/SoCameraInteractor.h>
#include <Inventor/ViewerComponents/nodes/SceneInteractor.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/devices/SoGLContext.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/Gui/viewers/SoGuiAlgoViewers.h>
#include <Inventor/ViewerComponents/SiRenderArea.h>
#pragma warning(pop)

#include "TC.Rendering.Core.RenderWindowExport.h"

struct TCVoxelInfo {
	bool valid { false };
	double x { 0 };
	double y { 0 };
	double z { 0 };
	double htIntensity { -1 };
	double flIntensity[3] { -1, -1, -1 };
};

class TC_Rendering_Core_RenderWindow_API QOivRenderWindow : public QWidget {
	Q_OBJECT
public:
	QOivRenderWindow(QWidget* parent, bool is2d = false);
	~QOivRenderWindow();

	auto setSceneGraph(SoNode* newScene) -> void;
	auto immediateRender() -> void;
	auto getCamera() -> SoCamera*;
	auto getLight() -> SoDirectionalLight*;
	auto viewX() -> void;
	auto viewY() -> void;
	auto viewZ() -> void;
	auto viewall() -> void;
	auto setCameraZoom(float zoom) -> void;
	auto resetCameraZoom() -> void;
	auto getCameraZoom() -> float;
	auto setGradientBackground(SbVec3f start, SbVec3f end) -> void;
	auto getSceneGraph() -> SoNode*;
	auto getViewportRegion() -> SbViewportRegion;
	auto getContext() -> SoGLContext*;
	auto getRenderBuffer(int sampling = 1) -> QPixmap;

	auto setName(QString name) -> void;
	auto getName() -> QString;
	auto forceArrowCursor(bool force) -> void;

	bool saveSnapshot(const QString& filename, int sampling, bool overwrite = true);

	auto setWindowTitle(QString title) -> void;
	auto setWindowTitleColor(float r, float g, float b) -> void;
	auto setWindowTitleViz(bool show) -> void;
	auto setSettingIconViz(bool show) -> void;

	auto SetViewDirection(int axis, QString name) -> void;
	auto StartCalculateFPS(float threshold) -> void;
	auto FinishCalculateFPS() -> float;

	virtual auto closeEvent(QCloseEvent* unused) -> void;

	auto rotateTick(SbVec3f axis, float val) -> void;
	auto clearOrbitCenter() -> void;
	auto InteractiveCount(bool inc) -> void;
	auto setTransparencyType(SoGLRenderAction::TransparencyType type) -> void;
	auto setAntiAliasingMode(SoSceneManager::AntialiasingMode mode) -> void;
	auto setClearPolicy(SiRenderArea::ClearPolicy policy) -> void;

	auto toggleFPS(bool showFPS) -> void;

protected:
	static auto OnMouseButtonEvent(void* data, SoEventCallback* node) -> void;
	static auto OnMouseMoveEvent(void* data, SoEventCallback* node) -> void;
	static auto OnMouseWheelEvent(void* data, SoEventCallback* node) -> void;
	static auto OnKeyboardEvent(void* data, SoEventCallback* node) -> void;

	auto getQtRenderArea() -> QWidget*;
	auto setOrbitConstraint(bool x, bool y, bool z) -> void;
	auto spinCamera(SbVec2f newLoc) -> void;
	auto startSpin(SbVec2f startLoc) -> void;
	auto panCamera(SbVec2f newLoc) -> void;
	auto startPan(SbVec2f startLoc) -> void;

	//Event handling functions	
	auto isAltPressed() -> bool;
	auto setAltPressed(bool alt) -> void;
	auto isNavigation() -> bool;
	auto setInteractionMode(bool isNavi) -> void;
	auto isOrbit() -> bool;
	auto setSeekMode(bool seek) -> void;
	auto setCameraType(bool isOrthographic) -> void;
	auto viewAxis(const SbVec3f& direction, const SbVec3f& up) -> void;
protected slots:
	void OnFPS(float);
private:
	//set default renderWindow interface	
	virtual auto setDefaultWindowType() -> void = 0;

	virtual auto MouseButtonEvent(SoEventCallback* node) -> void = 0;
	virtual auto MouseMoveEvent(SoEventCallback* node) -> void = 0;
	virtual auto MouseWheelEvent(SoEventCallback* node) -> void = 0;
	virtual auto KeyboardEvent(SoEventCallback* node) -> void = 0;

	auto initSettingIcon(SoSeparator* sceneRoot) -> void;
	auto initTitleText(SoSeparator* sceneRoot) -> void;

	struct Impl;
	std::unique_ptr<Impl> d;
};

Q_DECLARE_METATYPE(TCVoxelInfo);
