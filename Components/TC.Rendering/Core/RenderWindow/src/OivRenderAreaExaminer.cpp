#include <qpixmap.h>

#include "OivRenderAreaExaminer.h"


QCursor* OivRenderAreaExaminer::s_seekCursor = NULL;
QCursor* OivRenderAreaExaminer::s_viewingCursor = NULL;

//------------------------------------------------------------------------------
OivRenderAreaExaminer::OivRenderAreaExaminer( QWidget* parent )
  : OivRenderAreaInteractive( parent, false )
{
  // QPixmap need QApplication
  if ( OivRenderAreaExaminer::s_seekCursor == NULL )
  {
    //OivRenderAreaExaminer::s_seekCursor = new QCursor(QPixmap(QLatin1String(":icons/CursorSeek.png"), "png", Qt::MonoOnly));
      OivRenderAreaExaminer::s_seekCursor = new QCursor();
  }

  if ( OivRenderAreaExaminer::s_viewingCursor == NULL )
  {
    //OivRenderAreaExaminer::s_viewingCursor = new QCursor(QPixmap(QLatin1String(":icons/CursorCurvedHand.png"), "png", Qt::MonoOnly));
      OivRenderAreaExaminer::s_viewingCursor = new QCursor();
  }

  // Disable SceneExaminer automatic adjustment of clipping planes,
  // adjustment is managed by the render area.
  SoPreferences::setBool("OIV_SCENE_EXAMINER_AUTO_CLIPPING_PLANES", false);
  // Deactivate auto interactive mode,
  // interactive mode is managed by the SceneExaminer.
  m_isAutoInteractive = false;

  m_rootSceneGraph = m_examinerRootSceneGraph = new SceneExaminer();
  // Listening interaction from the scene examiner
  m_examinerRootSceneGraph->addInteractionModeListener(this);
  // Init cursor shape
  updateInteractionCursor();

  buildSceneGraph();
}

auto OivRenderAreaExaminer::sendFPS(float fps) -> void {
    emit fpsValue(fps);
}

//------------------------------------------------------------------------------
SceneExaminer*
OivRenderAreaExaminer::getSceneExaminer()
{
  return m_examinerRootSceneGraph;
}

//------------------------------------------------------------------------------
SceneExaminer*
OivRenderAreaExaminer::getSceneInteractor() const
{
  return m_examinerRootSceneGraph;
}

//------------------------------------------------------------------------------
void
OivRenderAreaExaminer::setNavigationMode( SceneExaminer::NavigationMode mode )
{
  m_examinerRootSceneGraph->setNavigationMode( mode );
  updateInteractionCursor();
}

//------------------------------------------------------------------------------
SceneExaminer::NavigationMode
OivRenderAreaExaminer::getNavigationMode()
{
  return m_examinerRootSceneGraph->getNavigationMode();
}

//------------------------------------------------------------------------------
void
OivRenderAreaExaminer::seekModeChanged( const bool seek )
{
  if ( seek )
  {
    setCursor( *OivRenderAreaExaminer::s_seekCursor );
  }
  else
  {
    updateInteractionCursor();
  }
}

//------------------------------------------------------------------------------
void
OivRenderAreaExaminer::interactionModeChanged( SceneExaminer::InteractionMode /*mode*/ )
{
  updateInteractionCursor();
}

//------------------------------------------------------------------------------
void
OivRenderAreaExaminer::updateInteractionCursor()
{
  if ( ( ( SceneExaminer* )getSceneInteractor() )->getInteractionMode() == SceneExaminer::NAVIGATION )
  {
    setCursor( *OivRenderAreaExaminer::s_viewingCursor );
  }
  else
  {
    setCursor( Qt::ArrowCursor );
  }
}