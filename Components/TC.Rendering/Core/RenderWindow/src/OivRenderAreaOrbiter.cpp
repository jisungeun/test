#include "OivRenderAreaOrbiter.h"

//------------------------------------------------------------------------------
OivRenderAreaOrbiter::OivRenderAreaOrbiter(QWidget* parent)
	: OivRenderAreaInteractive(parent, false)
{
	m_rootSceneGraph = m_sceneOrbiter = new SceneOrbiter();

	// Deactivate auto interactive mode,
	// interactive mode is managed by the SceneOrbiter.
	m_isAutoInteractive = false;

	buildSceneGraph();
}

//------------------------------------------------------------------------------
SceneOrbiter*
OivRenderAreaOrbiter::getSceneInteractor() const
{
	return m_sceneOrbiter;
}
