#include <Inventor/ViewerComponents/nodes/SceneInteractor.h>

#include <Inventor/elements/SoInteractionElement.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoStereoCamera.h>
#include <Inventor/nodes/SoBBox.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoText2.h>

#include <Inventor/SoDB.h>
#include <Inventor/SoSceneManager.h>

#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QWidget>

#include <OivTimer.h>
#include "OivRenderAreaInteractive.h"

//------------------------------------------------------------------------------
OivRenderAreaInteractive::OivRenderAreaInteractive(QWidget* parent)
	: OivRenderArea(parent)
{
	init(true);
}

//------------------------------------------------------------------------------
OivRenderAreaInteractive::OivRenderAreaInteractive(QWidget* parent, bool buildRootSceneGraph)
	: OivRenderArea(parent)
{
	init(buildRootSceneGraph);
}

//------------------------------------------------------------------------------
OivRenderAreaInteractive::~OivRenderAreaInteractive()
{
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::startRender(SiRenderArea::RenderEventArg&)
{
	requestUpdate();
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::init(bool buildRootSceneGraph)
{
	m_clippingMode = AUTO;
	m_isDragging = false;
	m_interactiveMode = SoInteractiveComplexity::AUTO;
	m_isAutoInteractive = true;

	if (buildRootSceneGraph)
	{
		m_rootSceneGraph = new SceneInteractor();
		buildSceneGraph();
	}

	m_containerWidget->setMouseTracking(true);
	m_containerWidget->setAttribute(Qt::WA_AcceptTouchEvents, true);
	//SoDB::setSystemTimer(OivTimer::GetInstance().get());
	SoDB::setSystemTimer(new OivTimer());
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::buildSceneGraph()
{
	m_appSceneGraph = new SoGroup;
	{
		//FPS Display
		m_fpsSwitch = new SoSwitch;		
		m_fpsSceneGraph = new SoSeparator;
		m_fpsSwitch->addChild(m_fpsSceneGraph);
		m_fpsSceneGraph->setName("FPS_root");
		SoRef<SoBBox> box = new SoBBox;
		box->mode = SoBBox::NO_BOUNDING_BOX;
		m_fpsSceneGraph->addChild(box.ptr());
		SoRef<SoLightModel> lightModel = new SoLightModel;
		lightModel->model = SoLightModel::Model::BASE_COLOR;
		m_fpsSceneGraph->addChild(lightModel.ptr());
		SoRef<SoPickStyle> pickStyle = new SoPickStyle;
		pickStyle->style = SoPickStyle::UNPICKABLE;
		m_fpsSceneGraph->addChild(pickStyle.ptr());
		SoRef<SoOrthographicCamera> orthoCam = new SoOrthographicCamera;
		orthoCam->viewportMapping = SoOrthographicCamera::LEAVE_ALONE;
		m_fpsSceneGraph->addChild(orthoCam.ptr());
		SoRef<SoFont> font = new SoFont;
		font->size = 18;		
		m_fpsSceneGraph->addChild(font.ptr());
		SoRef<SoBaseColor> color = new SoBaseColor;
		color->rgb.setValue(1, 1, 1);
		m_fpsSceneGraph->addChild(color.ptr());
		SoRef<SoTranslation> trans = new SoTranslation;
		trans->translation.setValue(-0.95f, 0.92f, 0);
		m_fpsSceneGraph->addChild(trans.ptr());

		m_fpsText = new SoText2;
		m_fpsText->string = "FPS: ";
		m_fpsSceneGraph->addChild(m_fpsText);

		m_rootSceneGraph->addChild(m_fpsSwitch);
	}
	m_rootSceneGraph->addChild(m_appSceneGraph);
	OivRenderArea::setSceneGraph(m_rootSceneGraph);
}

void OivRenderAreaInteractive::ToggleFPS(bool show) {
    if(show) {
		m_fpsSwitch->whichChild = 0;
    }else {
		m_fpsSwitch->whichChild = -1;
    }
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::setSceneGraph(SoNode* sceneGraph)
{
	m_appSceneGraph->removeAllChildren();	
	m_appSceneGraph->addChild(sceneGraph);
}

//------------------------------------------------------------------------------
SoRenderAreaCore::RenderStatus
OivRenderAreaInteractive::render()
{
	if (m_clippingMode == AUTO && m_renderAreaCore.ptr() != NULL)
	{
		// Update camera clipping planes before each rendering.
		m_rootSceneGraph->adjustClippingPlanes(m_renderAreaCore->getSceneManager()->getViewportRegion());
	}

	// render scene
	return OivRenderArea::render();
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::setClippingPlanesAdjustMode(ClippingPlanesAdjustMode mode)
{
	if (m_clippingMode != mode)
	{
		m_clippingMode = mode;
		requestUpdate();
	}
}

//------------------------------------------------------------------------------
OivRenderAreaInteractive::ClippingPlanesAdjustMode
OivRenderAreaInteractive::getClippingPlanesAdjustMode()
{
	return m_clippingMode;
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::initializeGL()
{
	OivRenderArea::initializeGL();
	m_renderAreaCore->onStartRender().add(*this, &OivRenderAreaInteractive::startRender);
	// check if there are incoming events when rendering, and if so abort the rendering
	m_renderAreaCore->getSceneManager()->setAbortRenderCallback(OivRenderAreaInteractive::s_abortRenderCallback, this);
	m_renderAreaCore->getSceneManager()->setRenderCallback(OivRenderAreaInteractive::s_renderCallback, this);	
	
	m_renderAreaCore->getSceneManager()->setAutoInteractiveMode(m_isAutoInteractive);
	m_renderAreaCore->getSceneManager()->setInteractive(m_interactiveMode == SoInteractiveComplexity::FORCE_INTERACTION);
}

//------------------------------------------------------------------------------
SceneInteractor*
OivRenderAreaInteractive::getRootSceneGraph() const
{
	return m_rootSceneGraph;
}

//------------------------------------------------------------------------------
SceneInteractor*
OivRenderAreaInteractive::getSceneInteractor() const
{
	return m_rootSceneGraph;
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::viewAll(const SbViewportRegion& viewport)
{
	m_rootSceneGraph->viewAll(viewport);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::viewAxis(const SbVec3f& direction, const SbVec3f& up)
{
	m_rootSceneGraph->viewAxis(direction, up);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::activateStereo(bool activated)
{
	if (activated)
	{
		// check camera
		if (!isStereoSupported())
		{
			qWarning("Cannot activate stereo: current camera is not a SoStereoCamera!");
			return;
		}
	}
	OivRenderArea::activateStereo(activated);
}

//------------------------------------------------------------------------------
bool
OivRenderAreaInteractive::isStereoSupported() const
{
	// check camera
	SoCamera* camera = m_rootSceneGraph->getCamera();
	return (dynamic_cast<SoStereoCamera*>(camera) != NULL);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::setStereoCameraOffset(float offset)
{
	SoCamera* camera = m_rootSceneGraph->getCamera();
	SoStereoCamera* stereoCamera = NULL;

	if ((stereoCamera = dynamic_cast<SoStereoCamera*>(camera)) == NULL)
		qWarning("Current camera is not a SoStereoCamera!");
	else
		stereoCamera->offset = offset;
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::setStereoCameraBalance(float balance)
{
	SoCamera* camera = m_rootSceneGraph->getCamera();
	SoStereoCamera* stereoCamera = NULL;

	if ((stereoCamera = dynamic_cast<SoStereoCamera*>(camera)) == NULL)
		qWarning("Current camera is not a SoStereoCamera!");
	else
		stereoCamera->balance = balance;
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::setInteractiveMode(SoInteractiveComplexity::InteractiveMode mode)
{
	m_interactiveMode = mode;
	m_isAutoInteractive = (mode == SoInteractiveComplexity::AUTO);

	if (m_renderAreaCore.ptr() != NULL)
	{
		m_renderAreaCore->getSceneManager()->setAutoInteractiveMode(m_isAutoInteractive);
		m_renderAreaCore->getSceneManager()->setInteractive(m_interactiveMode == SoInteractiveComplexity::FORCE_INTERACTION);
	}
}

//------------------------------------------------------------------------------
SoInteractiveComplexity::InteractiveMode
OivRenderAreaInteractive::getInteractiveMode() const
{
	return m_interactiveMode;
}

//------------------------------------------------------------------------------
SbBool
OivRenderAreaInteractive::processEvent(const SoEvent* event)
{
	if (m_renderAreaCore.ptr() != NULL)
		return m_renderAreaCore->processEvent(event);

	SoDebugError::postWarning("OivRenderAreaInteractive::processEvents",
		"Rendering area is not initialized, events cannot be processed.");
	return FALSE;
}

//------------------------------------------------------------------------------
SbBool
OivRenderAreaInteractive::processEvents(const std::vector<const SoEvent*>& eventList)
{
	if (m_renderAreaCore.ptr() != NULL)
		return m_renderAreaCore->processEvents(eventList);

	SoDebugError::postWarning("OivRenderAreaInteractive::processEvents",
		"Rendering area is not initialized, events cannot be processed.");
	return FALSE;
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::mousePressEvent(QMouseEvent* qevent)
{
	m_isDragging = true;
	SoEvent* event = OivQtEventToSoEvent::getMousePressEvent(qevent, QPoint(qevent->x() * m_containerWidget->devicePixelRatio(), ((m_containerWidget->height() - 1) - qevent->y()) * m_containerWidget->devicePixelRatio()));
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::mouseReleaseEvent(QMouseEvent* qevent)
{
	m_isDragging = false;
	SoEvent* event = OivQtEventToSoEvent::getMouseReleaseEvent(qevent, QPoint(qevent->x() * m_containerWidget->devicePixelRatio(), ((m_containerWidget->height() - 1) - qevent->y()) * m_containerWidget->devicePixelRatio()));
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::mouseMoveEvent(QMouseEvent* qevent)
{
	SoEvent* event = OivQtEventToSoEvent::getMouseMoveEvent(qevent, QPoint(qevent->x() * m_containerWidget->devicePixelRatio(), ((m_containerWidget->height() - 1) - qevent->y()) * m_containerWidget->devicePixelRatio()));
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::wheelEvent(QWheelEvent* qevent)
{
	SoEvent* event = OivQtEventToSoEvent::getMouseWheelEvent(qevent);
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::keyPressEvent(QKeyEvent* qevent)
{
	SoEvent* event = OivQtEventToSoEvent::getKeyPressEvent(qevent);
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::keyReleaseEvent(QKeyEvent* qevent)
{
	SoEvent* event = OivQtEventToSoEvent::getKeyReleaseEvent(qevent);
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::enterEvent(QEvent* /*qevent*/)
{
	SoEvent* event = OivQtEventToSoEvent::getMouseEnterEvent();
	processEvent(event);
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::leaveEvent(QEvent* /*qevent*/)
{
	SoEvent* event = OivQtEventToSoEvent::getMouseLeaveEvent();
	processEvent(event);	
}

//------------------------------------------------------------------------------
void
OivRenderAreaInteractive::mouseDoubleClickEvent(QMouseEvent* qevent)
{
	SoEvent* event = OivQtEventToSoEvent::getMouseDoubleClickEvent(qevent, QPoint(qevent->x() * m_containerWidget->devicePixelRatio(), ((m_containerWidget->height() - 1) - qevent->y()) * m_containerWidget->devicePixelRatio()));
	processEvent(event);
}

//------------------------------------------------------------------------------
bool
OivRenderAreaInteractive::event(QEvent* qevent)
{	
	if (qevent->type() == QEvent::Leave) {		
		leaveEvent(qevent);
		return true;
	}
	QTouchEvent* qtouchEvent = dynamic_cast<QTouchEvent*>(qevent);
	if (qtouchEvent != NULL)
	{
		const std::vector<const SoEvent*>& soeventlist =
			m_eventbuilder.getTouchEvents(qtouchEvent, m_containerWidget->height());
		processEvents(soeventlist);
		return true;
	}
	else
		return OivRenderArea::event(qevent);
}

void OivRenderAreaInteractive::s_renderCallback(void* data, SoSceneManager*) {
	auto curTime = SoDB::getCurrentTime().getMsecValue();
	OivRenderAreaInteractive* renderArea = static_cast<OivRenderAreaInteractive*>(data);
	auto fps = 1000.0 / static_cast<double>(curTime - renderArea->lastTime);
	renderArea->lastTime = curTime;
	renderArea->m_fpsText->string = QString("FPS: %1").arg(QString::number(fps, 'f', 2)).toStdString();
	renderArea->render();
	renderArea->sendFPS(static_cast<float>(fps));
}

//------------------------------------------------------------------------------
SbBool
OivRenderAreaInteractive::s_abortRenderCallback(SoAction* action, void* data)
{
	OivRenderAreaInteractive* renderArea = static_cast<OivRenderAreaInteractive*>(data);

	SoState* state = action->getState();

	// check if we are rendering a frame in INTERATIVE mode. In that case, don't abort rendering,
	// else we might abort all frame and we won't have any rendering.
	// if frame is in STILL mode (non interacting), then we will check for event in event queue
	// to get back interactive as soon as possible.
	if (!SoInteractionElement::isInteracting(state))
	{
#if defined (_WIN32)
		SoSceneManager* sceneMgr = action->getSceneManager();
		if (sceneMgr != NULL && sceneMgr->isAutoInteractiveMode() && renderArea->m_isDragging)
		{
			if (GetQueueStatus(QS_MOUSEMOVE | QS_MOUSEBUTTON | QS_KEY))
				return TRUE;
		}
		else
		{
			if (GetQueueStatus(QS_MOUSEBUTTON | QS_KEY))
				return TRUE;
		}
#elif defined(__APPLE__)
		//TODO: NOT IMPLEMENTED
#else // UNIX
		//TODO: NOT IMPLEMENTED
#endif
	}

	return FALSE;
}
