#include "OivRenderArea.h"

#include <Inventor/SbViewportRegion.h>
#include <Inventor/SoPreferences.h>
#include <Inventor/SoSceneManager.h>
#include <Inventor/SoPreferences.h>
#include <Inventor/ViewerComponents/SoRawStereoParameters.h>
#include <Inventor/devices/SoGLContext.h>
#include <Inventor/nodes/SoNode.h>

#include <QApplication>
#include <QOpenGLPaintDevice>
#include <QResizeEvent>
#include <QTimer>
#include <QWidget>

//------------------------------------------------------------------------------
OivRenderArea::OivRenderArea(QWidget* /*parent*/)
	: QWindow(),
	m_renderAreaCore(nullptr),
	m_sceneGraph(nullptr),
	m_clearPolicy(COLORBUFFER_AND_DEPTHBUFFER),
	m_color(0, 0, 0, 0),
	m_depth(1.0f),
	m_size(0, 0),
	m_stereo(false),
	m_glRenderAction(nullptr),
	m_antialiasingMode(SoSceneManager::NO_ANTIALIASING),
	m_transparencyType(SoGLRenderAction::TransparencyType::NO_SORT),
	m_antialiasingQuality(0),
	m_stillAntialiasingDelay(0),
	m_stillAntialiasingQuality(0),
	m_needInitGL(true),
	m_qtContext(nullptr),
    prev_time(0)
{
	init();
}

//------------------------------------------------------------------------------
OivRenderArea::~OivRenderArea()
{
	// remove the ref added in setSceneGraph
	if (m_sceneGraph != nullptr)
		m_sceneGraph->unref();

	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore = nullptr;

	delete m_qtContext;
}

//------------------------------------------------------------------------------
void
OivRenderArea::init()
{
	setSurfaceType(QSurface::OpenGLSurface);
	m_containerWidget = QWidget::createWindowContainer(this);
	// Allow the widget to receive keyboard events
	m_containerWidget->setFocusPolicy(Qt::StrongFocus);

	// Create a SoGLContext with Qt's GL context
	m_qtContext = new QOpenGLContext;
	QSurfaceFormat format = requestedFormat();

	static SbBool isCoreProfile = SoPreferences::getBool("OIV_RENDERENGINE_USE_GLCORE", FALSE);
	if (isCoreProfile)
	{
		format.setVersion(4, 5);
		format.setProfile(QSurfaceFormat::CoreProfile); // Requires >=Qt-4.8.0
	}

	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	m_qtContext->setFormat(format);
	m_qtContext->create();

	// This code is used for test process only and can be safely removed.
	// This environment variable should not be used for application deployment
	if (SoPreferences::getBool("OIV_DISABLE_MAINLOOP", FALSE))
		QTimer::singleShot(3000, qApp, &QCoreApplication::quit);
}

//------------------------------------------------------------------------------
void
OivRenderArea::setSceneGraph(SoNode* sceneGraph)
{
	if (sceneGraph == m_sceneGraph)
		return;

	// unref previous scene graph
	if (m_sceneGraph != nullptr)
		m_sceneGraph->unref();

	m_sceneGraph = sceneGraph;
	// keep an additional ref to the scene graph, because the scene renderer
	// may be created later.
	if (m_sceneGraph != nullptr)
		m_sceneGraph->ref();

	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setSceneGraph(m_sceneGraph);
}

//------------------------------------------------------------------------------
SoNode*
OivRenderArea::getSceneGraph() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getSceneGraph();
	else
		return m_sceneGraph;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setClearPolicy(SiRenderArea::ClearPolicy policy)
{
	m_clearPolicy = policy;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setClearPolicy(m_clearPolicy);
}

//------------------------------------------------------------------------------
SiRenderArea::ClearPolicy
OivRenderArea::getClearPolicy() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getClearPolicy();
	else
		return m_clearPolicy;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setClearColor(const SbColorRGBA& color)
{
	m_color = color;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setClearColor(m_color);
}

//------------------------------------------------------------------------------
SbColorRGBA
OivRenderArea::getClearColor() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getClearColor();
	else
		return m_color;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setClearDepth(float depth)
{
	m_depth = depth;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setClearDepth(m_depth);
}

//------------------------------------------------------------------------------
float
OivRenderArea::getClearDepth() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getClearDepth();
	else
		return m_depth;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setSize(const SbVec2i32& size)
{
	m_size = size;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setSize(m_size);
}

//------------------------------------------------------------------------------
SbVec2i32
OivRenderArea::getSize() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getSize();
	else
		return m_size;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setGLRenderAction(SoGLRenderAction* glAction)
{
	m_glRenderAction = glAction;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setGLRenderAction(glAction);
}

//------------------------------------------------------------------------------
SoGLRenderAction*
OivRenderArea::getGLRenderAction() const
{
	if (m_renderAreaCore.ptr() != nullptr)
		return m_renderAreaCore->getGLRenderAction();
	else
		return m_glRenderAction;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setTransparencyType(SoGLRenderAction::TransparencyType type)
{
	m_transparencyType = type;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setTransparencyType(type);
}

//------------------------------------------------------------------------------
SoGLRenderAction::TransparencyType
OivRenderArea::getTransparencyType() const
{
	if (m_renderAreaCore.ptr() == nullptr)
		return m_transparencyType;
	return m_renderAreaCore.ptr()->getTransparencyType();
}

//------------------------------------------------------------------------------
void
OivRenderArea::setAntialiasingMode(SoSceneManager::AntialiasingMode mode)
{
	m_antialiasingMode = mode;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setAntialiasingMode(mode);
}

//------------------------------------------------------------------------------
SoSceneManager::AntialiasingMode
OivRenderArea::getAntialiasingMode() const
{
	if (m_renderAreaCore.ptr() == nullptr)
		return m_antialiasingMode;
	return m_renderAreaCore.ptr()->getAntialiasingMode();
}

//------------------------------------------------------------------------------
void
OivRenderArea::setAntialiasingQuality(float quality)
{
	m_antialiasingQuality = quality;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setAntialiasingQuality(quality);
}

//------------------------------------------------------------------------------
float
OivRenderArea::getAntialiasingQuality() const
{
	if (m_renderAreaCore.ptr() == nullptr)
		return m_antialiasingQuality;
	return m_renderAreaCore.ptr()->getAntialiasingQuality();
}

//------------------------------------------------------------------------------
void
OivRenderArea::setStillSuperSamplingQuality(float quality)
{
	m_stillAntialiasingQuality = quality;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setStillSuperSamplingQuality(quality);
}

//------------------------------------------------------------------------------
float
OivRenderArea::getStillSuperSamplingQuality() const
{
	if (m_renderAreaCore.ptr() == nullptr)
		return m_stillAntialiasingQuality;
	return m_renderAreaCore.ptr()->getStillSuperSamplingQuality();
}

//------------------------------------------------------------------------------
void
OivRenderArea::setStillSuperSamplingDelay(unsigned int delay)
{
	m_stillAntialiasingDelay = delay;
	if (m_renderAreaCore.ptr() != nullptr)
		m_renderAreaCore->setStillSuperSamplingDelay(delay);
}

//------------------------------------------------------------------------------
unsigned int
OivRenderArea::getStillSuperSamplingDelay() const
{
	if (m_renderAreaCore.ptr() == nullptr)
		return m_stillAntialiasingDelay;
	return m_renderAreaCore.ptr()->getStillSuperSamplingDelay();
}

//------------------------------------------------------------------------------
SbEventHandler<SiRenderArea::RenderEventArg&>&
OivRenderArea::onStartRender()
{
	return m_renderAreaCore->onStartRender();
}

//------------------------------------------------------------------------------
void
OivRenderArea::activateStereo(bool activated)
{
	if (m_stereo == activated)
		return;

	if (activated && !isRawStereoAvailable())
		qWarning("Stereo buffers are not enabled");

	m_stereo = activated;

	if (m_renderAreaCore.ptr() == nullptr)
		return;

	// Create the new QtContext
	QSurfaceFormat format = requestedFormat();
	format.setStereo(m_stereo);
	m_context->invalidate();
	m_qtContext->setFormat(format);
	m_qtContext->create();
	m_needInitGL = true;

	destroy();
	setFormat(format);
	create();
	setVisible(true);
	render();

	if (m_stereo)
	{
		// activate raw stereo on RenderAreaCore
		SoRawStereoParameters* params = new SoRawStereoParameters();
		setStereoParameters(params);
		m_renderAreaCore->activateStereo(true);
		delete params;
	}
	else
		m_renderAreaCore->activateStereo(false);
}

//------------------------------------------------------------------------------
bool
OivRenderArea::isStereoActivated() const
{
	if (m_renderAreaCore.ptr() != NULL)
		return m_renderAreaCore->isStereoActivated();

	SoDebugError::postWarning("OivRenderArea::isStereoActivated",
		"Rendering area is not initialized, stereo status cannot be retrieved.");
	return false;
}

//------------------------------------------------------------------------------
void
OivRenderArea::setStereoParameters(SoStereoParameters* parameters)
{
	if (m_renderAreaCore.ptr() != NULL)
		m_renderAreaCore->setStereoParameters(parameters);
	else
		SoDebugError::postWarning("OivRenderArea::setStereoParameters",
			"Rendering area is not initialized, stereo parameters cannot be set.");
}

//------------------------------------------------------------------------------
SoStereoParameters*
OivRenderArea::getStereoParameters() const
{
	if (m_renderAreaCore.ptr() != NULL)
		return m_renderAreaCore->getStereoParameters();

	SoDebugError::postWarning("OivRenderArea::getStereoParameters",
		"Rendering area is not initialized, stereo parameters cannot be retrieved.");
	return nullptr;
}

//------------------------------------------------------------------------------
bool
OivRenderArea::isRawStereoAvailable()
{
	QSurfaceFormat format = requestedFormat();
	format.setStereo(true);

	QOpenGLContext context;
	context.setFormat(format);
	context.create();
	QSurfaceFormat returnedFormat = context.format();

	return returnedFormat.stereo();
}

//------------------------------------------------------------------------------
SoRenderAreaCore::RenderStatus
OivRenderArea::render()
{
	if (m_needInitGL)
	{
		initializeGL();
	}

	if (!isExposed())
		return SoRenderAreaCore::ABORTED;

	m_qtContext->makeCurrent(this);
	//TODO: Rendering speed measurement code 
	/*SbString valueString;
	SoDB::getGlobalField("realTime")->get(valueString);
	auto curTime = valueString.toDouble();
	std::cout << "FPS:" << curTime - prev_time << std::endl;
	prev_time = curTime;*/ 

	// Update defaultPixelPerInch, must be call after Creation of SoGlContext
	const float ratio = m_containerWidget->devicePixelRatio();
	SbViewportRegion::setDefaultPixelsPerInch(SbViewportRegion::s_historicalPixelPerInch * ratio);

	// render the scene graph.
	SoRenderAreaCore::RenderStatus renderStatus = m_renderAreaCore->render();

	// Check if the rendering is made well
	// If not : don't swap the buffer because we can have actifacts in the buffer
	if (renderStatus != SoRenderAreaCore::ABORTED)
	{
		m_context->swapBuffers();
	}
	else
	{
		// Ask for a new render if the previous frame was aborted (for example, a frame
		// may have been aborted because some GUI events were waiting to be processed)
		m_renderAreaCore->getSceneManager()->scheduleRedraw();
	}

	return renderStatus;
}

bool
OivRenderArea::event(QEvent* e)
{
	if (e->type() == QEvent::UpdateRequest || e->type() == QEvent::Show)
	{
		render();
		return true;
	}
	return QWindow::event(e);
}

//------------------------------------------------------------------------------
void
OivRenderArea::initializeGL()
{
	bool result = m_qtContext->makeCurrent(this);
	if (!result)
		return;

	m_needInitGL = false;

	if (m_renderAreaCore.ptr() != nullptr)
	{
		m_renderAreaCore = nullptr;
	}

	m_context = SoGLContext::getCurrent(true);
	m_renderAreaCore = new SoRenderAreaCore(m_context.ptr());

	// Set all properties kept while m_renderAreaCore was not initialized
	m_renderAreaCore->setSceneGraph(m_sceneGraph);
	setClearDepth(m_depth);
	setSize(m_size);
	setClearColor(m_color);
	if (m_glRenderAction != nullptr)
		m_renderAreaCore->setGLRenderAction(m_glRenderAction);
	m_renderAreaCore->setTransparencyType(m_transparencyType);
	m_renderAreaCore->setAntialiasingMode(m_antialiasingMode);
	m_renderAreaCore->setAntialiasingQuality(m_antialiasingQuality);
	m_renderAreaCore->setStillSuperSamplingDelay(m_stillAntialiasingDelay);
	m_renderAreaCore->setStillSuperSamplingQuality(m_stillAntialiasingQuality);
}

//------------------------------------------------------------------------------
void
OivRenderArea::resizeEvent(QResizeEvent* event)
{
	// Apply Device pixel ratio to width and height return by Qt
	const float ratio = m_containerWidget->devicePixelRatio();
	setSize(SbVec2i32(event->size().width() * ratio, event->size().height() * ratio));

	requestUpdate();
}

//------------------------------------------------------------------------------
void
OivRenderArea::exposeEvent(QExposeEvent* event)
{
	QWindow::exposeEvent(event);

	if (isExposed())
		render();
}
