#include <QGridLayout>
#include <QMenu>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "OivRenderAreaExaminer.h"

#include <Inventor/nodes/SoGradientBackground.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>

#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoBBox.h>
#include <Inventor/nodes/SoFontStyle.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoCamera.h>

#include <Medical/helpers/MedicalHelper.h>

#include "QOivRenderWindow.h"

#include <IvTune/SoIvTune.h>

#include <Inventor/SoOffscreenRenderArea.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoCallback.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoPointLight.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <ScaleViz/SoScaleViz.h>
#include <QStandardPaths>


class pixelCameraInfo {
public:
	pixelCameraInfo() :
		m_pCamera(NULL),
		m_invertX(0),
		m_invertY(0),
		m_lastViewport(0, 0) {}

	SoOrthographicCamera* m_pCamera;      // Ortho camera we will modify
	SbBool m_invertX;      // 0..N-1 if FALSE, else -(N-1)..0
	SbBool m_invertY;      // ditto
	SbVec2i32 m_lastViewport; // 
};

void pixelCameraCB(void* userData, SoAction* pAction) {
	// Get the current traversal state
	SoState* pState = pAction->getState();
	if (!pState)
		return;

	// Get the current viewport from the traversal state
	const SbViewportRegion& vpRegion = SoViewportRegionElement::get(pState);
	const SbVec2i32 viewport = vpRegion.getViewportSizePixels_i32();

	// Get pixel space info object
	pixelCameraInfo* pInfo = (pixelCameraInfo*)userData;

	// Return if viewport same as last time (no need to change camera)
	if (viewport == pInfo->m_lastViewport)
		return;

	// Save viewport and compute aspect ratio
	pInfo->m_lastViewport = viewport;
	int vpWidth, vpHeight;
	viewport.getValue(vpWidth, vpHeight);
	float vpAspect = (float)vpWidth / vpHeight;

	// First disable notification on the camera node
	// (we're already in the middle of a traversal)
	// 
	// Set the view volume height to number of pixels in Y.
	// Set the aspect ratio the same as the viewport
	// (effectively setting view volume width to pixels in X).
	SoOrthographicCamera* pCamera = pInfo->m_pCamera;
	pCamera->enableNotify(FALSE);
	pCamera->height = (float)vpHeight;
	pCamera->aspectRatio = vpAspect;

	// Compute an offset that will make the view volume 0..N-1
	// (or the reverse if inverted).
	//
	// Remember that Inventor first computes a view volume centered around
	// 0,0.  I.e. -N/2 to N/2, where N is the width or the height.
	// Then it translates the view volume by pCamera->position.  So...
	//  - If the viewport width is even, for example 400, we need the left
	//    edge at zero, the center at 199.5 and the right edge at 399.
	//  - If the viewport width is odd, for example 399, we need the left
	//    edge at zero, the center at 199 and the right edge at 398.
	//
	// If the invert flag is set, that axis will be -(N-1)..0 instead.
	// This allows positioning geometry relative to the right/top edge.
	float xRadius = 0.5f * (vpWidth - 1);
	float yRadius = 0.5f * (vpHeight - 1);
	if (pInfo->m_invertX)
		xRadius = -xRadius;
	if (pInfo->m_invertY)
		yRadius = -yRadius;
	pCamera->position = SbVec3f(xRadius, yRadius, 2);
	pCamera->enableNotify(TRUE);
}

void addPixelSpaceCamera(SoGroup* pParent,
						SbBool invertX = 0, SbBool invertY = 0) {
	// Create an orthographic camera
	SoOrthographicCamera* p2dCam = new SoOrthographicCamera();

	// Set the viewportMapping to LEAVE_ALONE because we're going to
	// make the view volume aspect match the viewport ourselves.
	p2dCam->viewportMapping = SoCamera::LEAVE_ALONE;

	// We're going to put the camera position at +2 in Z, so these
	// near and far distances make the view volume 2 units deep in
	// Z centered around zero.  Obviously we're assuming all the
	// 2D annotation geometry really is flat!
	p2dCam->nearDistance = 1;
	p2dCam->farDistance = 10;

	// Setup info for pixel space camera callback
	pixelCameraInfo* pInfo = new pixelCameraInfo();
	pInfo->m_pCamera = p2dCam;
	pInfo->m_invertX = invertX;
	pInfo->m_invertY = invertY;

	// Create callback node to modify the camera.
	// Set it to call pixelCameraCB with pInfo
	SoCallback* p2dCamCB = new SoCallback();
	p2dCamCB->setCallback(pixelCameraCB, (void*)pInfo);

	// Add callback and camera to scene graph (callback before camera)
	pParent->addChild(p2dCamCB);
	pParent->addChild(p2dCam);
}

struct QOivRenderWindow::Impl {
	// Qt cursors
	QCursor normalCursor;
	QCursor dollyCursor;
	QCursor panCursor;
	QCursor rollCursor;
	QCursor seekCursor;
	QCursor spinCursor;

	OivRenderAreaExaminer* renderArea;
	SoDirectionalLight* dirLight;
	QString name;

	bool m_altSwitchBack;

	SoSeparator* rootScene;
	SoGradientBackground* gradient;

	SoSwitch* settingSwitch { nullptr };

	SoFont* titleFont { nullptr };
	SoSwitch* titleSwitch { nullptr };
	SoBaseColor* titleColor { nullptr };
	SoTranslation* titleTrans { nullptr };
	SoText2* titleText { nullptr };
	QString titleString { QString() };
	float titleRGB[3] { 1.0f, };

	bool forceArrow { false };
	bool is2D { false };
	bool fpsRecord { false };
	float fpsThreshold { 30 };
	float fpsSum { 0 };
	int fpsStep { 0 };
};

QOivRenderWindow::QOivRenderWindow(QWidget* parent, bool is2d) : QWidget(parent), d { new Impl } {
	setAttribute(Qt::WA_DeleteOnClose);
	QGridLayout* gridLayout = new QGridLayout;
	gridLayout->setSpacing(0);
	gridLayout->setMargin(0);
	{
		d->renderArea = new OivRenderAreaExaminer(nullptr);
		gridLayout->addWidget(d->renderArea->getContainerWidget(), 0, 0);
		connect(d->renderArea, SIGNAL(fpsValue(float)), this, SLOT(OnFPS(float)));

		//set cameara default mode to crop		
		//d->renderArea->getSceneInteractor()->getCamera()->viewportMapping.setValue(SoCamera::CROP_VIEWPORT_NO_FRAME);
	}
	setLayout(gridLayout);
	d->m_altSwitchBack = false;

	d->rootScene = new SoSeparator;
	d->rootScene->setName("SceneRoot");
	d->gradient = new SoGradientBackground;
	d->is2D = is2d;
	if (d->is2D) {
		d->renderArea->getSceneInteractor()->setNavigationMode(SceneExaminer::NavigationMode::PLANE);
		d->rootScene->addChild(new SoSeparator);
	} else {
		d->renderArea->getSceneInteractor()->setNavigationMode(SceneExaminer::NavigationMode::ORBIT);
		d->rootScene->addChild(d->gradient);
	}
	d->rootScene->addChild(new SoSeparator);
	d->renderArea->setSceneGraph(d->rootScene);

	auto callback = new SoEventCallback;
	callback->addEventCallback(SoLocation2Event::getClassTypeId(), OnMouseMoveEvent, this);
	callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), OnMouseButtonEvent, this);
	callback->addEventCallback(SoMouseWheelEvent::getClassTypeId(), OnMouseWheelEvent, this);
	callback->addEventCallback(SoKeyboardEvent::getClassTypeId(), OnKeyboardEvent, this);
	callback->setName("CustomCallback");

	d->renderArea->getSceneInteractor()->removeChild(0);//remove OIV default event callback
	d->renderArea->getSceneInteractor()->addChild(callback);

	initSettingIcon(d->rootScene);
	initTitleText(d->rootScene);
}

QOivRenderWindow::~QOivRenderWindow() {
	delete d->renderArea;
}

void QOivRenderWindow::OnFPS(float value) {
	if (d->fpsRecord) {
		if (value < d->fpsThreshold) {
			std::cout << "FPS: " << value << std::endl;
			d->fpsSum += value;
			d->fpsStep++;
		}
	}
}

auto QOivRenderWindow::StartCalculateFPS(float threshold) -> void {
	d->fpsSum = 0;
	d->fpsThreshold = threshold;
	d->fpsRecord = true;
	d->fpsStep = 0;
}

auto QOivRenderWindow::FinishCalculateFPS() -> float {
	d->fpsRecord = false;
	return d->fpsSum / static_cast<float>(d->fpsStep);
}

auto QOivRenderWindow::setAntiAliasingMode(SoSceneManager::AntialiasingMode mode) -> void {
	d->renderArea->setAntialiasingMode(mode);
}

auto QOivRenderWindow::setClearPolicy(SiRenderArea::ClearPolicy policy) -> void {
	d->renderArea->setClearPolicy(policy);
}

auto QOivRenderWindow::setTransparencyType(SoGLRenderAction::TransparencyType type) -> void {
	d->renderArea->setTransparencyType(type);
}

auto QOivRenderWindow::setName(QString name) -> void {
	d->name = name;
}

auto QOivRenderWindow::getName() -> QString {
	return d->name;
}

auto QOivRenderWindow::getCamera() -> SoCamera* {
	return d->renderArea->getSceneInteractor()->getCamera();
}

auto QOivRenderWindow::getLight() -> SoDirectionalLight* {
	return d->dirLight;
}

auto QOivRenderWindow::getCameraZoom() -> float {
	auto camera = d->renderArea->getSceneInteractor()->getCamera();
	if (camera == NULL)
		return 0;

	if (camera->isOfType(SoPerspectiveCamera::getClassTypeId()))
		return (float)(((SoPerspectiveCamera*)camera)->heightAngle.getValue() * 180.0f / M_PI);
	else if (camera->isOfType(SoOrthographicCamera::getClassTypeId()))
		return ((SoOrthographicCamera*)camera)->height.getValue();
	else
		return 0;
}

auto QOivRenderWindow::immediateRender() -> void { }

auto QOivRenderWindow::resetCameraZoom() -> void {
	auto camera = d->renderArea->getSceneInteractor()->getCamera();
	if (camera == NULL) {
		return;
	}
	if (camera->isOfType(SoPerspectiveCamera::getClassTypeId())) {
		((SoPerspectiveCamera*)camera)->heightAngle = 22.05 * M_PI / 180.0;
	}
}

auto QOivRenderWindow::setCameraZoom(float zoom) -> void {
	auto camera = d->renderArea->getSceneInteractor()->getCamera();
	if (camera == NULL)
		return;
	if (zoom < 0.0)//prevent inversion
		return;

	if (camera->isOfType(SoPerspectiveCamera::getClassTypeId()))
		((SoPerspectiveCamera*)camera)->heightAngle = zoom * M_PI / 180.0;
	else if (camera->isOfType(SoOrthographicCamera::getClassTypeId()))
		((SoOrthographicCamera*)camera)->height = zoom;
}

auto QOivRenderWindow::viewall() -> void {
	resetCameraZoom();
	d->renderArea->viewAll(getViewportRegion());
}

auto QOivRenderWindow::viewAxis(const SbVec3f& direction, const SbVec3f& up) -> void {
	d->renderArea->viewAxis(direction, up);
}

auto QOivRenderWindow::viewX() -> void {
	viewAxis(SbVec3f(-1.f, 0.f, 0.f), SbVec3f(0, 0, 1));
	viewall();
}

auto QOivRenderWindow::viewY() -> void {
	viewAxis(SbVec3f(0.f, -1.f, 0.f), SbVec3f(1, 0, 0));
	viewall();
}

auto QOivRenderWindow::viewZ() -> void {
	viewAxis(SbVec3f(0.f, 0.f, -1.f), SbVec3f(0, 1, 0));
	viewall();	
}

auto QOivRenderWindow::setSettingIconViz(bool show) -> void {
	if (nullptr != d->settingSwitch) {
		if (show) {
			d->settingSwitch->whichChild = 0;
		} else {
			d->settingSwitch->whichChild = -1;
		}
	}
}


auto QOivRenderWindow::setSceneGraph(SoNode* newScene) -> void {
	SoNode* root = d->renderArea->getSceneGraph();

	auto sceneRoot = MedicalHelper::find<SoSeparator>(root, "SceneRoot");
	if (sceneRoot) {
		sceneRoot->replaceChild(1, newScene);
	}
}

auto QOivRenderWindow::toggleFPS(bool showFPS) -> void {
	d->renderArea->ToggleFPS(showFPS);
}

auto QOivRenderWindow::setWindowTitleViz(bool show) -> void {
	if (nullptr != d->titleSwitch) {
		if (show) {
			d->titleSwitch->whichChild = 0;
		} else {
			d->titleSwitch->whichChild = -1;
		}
	}
}

auto QOivRenderWindow::setWindowTitle(QString title) -> void {
	d->titleString = title;
	if (nullptr != d->titleText) {
		d->titleText->string = title.toStdString();
	}
}

auto QOivRenderWindow::setWindowTitleColor(float r, float g, float b) -> void {
	d->titleRGB[0] = r;
	d->titleRGB[1] = g;
	d->titleRGB[2] = b;

	if (nullptr != d->titleColor) {
		d->titleColor->rgb.setValue(d->titleRGB);
	}
}

auto QOivRenderWindow::initTitleText(SoSeparator* sceneRoot) -> void {
	if (nullptr == d->titleSwitch) {
		d->titleSwitch = new SoSwitch;
	} else {
		d->titleSwitch->removeAllChildren();
	}

	auto titleSep = new SoSeparator;

	SoBBox* bbox = new SoBBox();
	bbox->mode = SoBBox::NO_BOUNDING_BOX;
	titleSep->addChild(bbox);

	SoLightModel* lmodel = new SoLightModel();
	lmodel->model = SoLightModel::BASE_COLOR;
	titleSep->addChild(lmodel);

	SoPickStyle* pstyle = new SoPickStyle();
	pstyle->style = SoPickStyle::UNPICKABLE;
	titleSep->addChild(pstyle);

	SoOrthographicCamera* camera = new SoOrthographicCamera();
	camera->viewportMapping = SoCamera::LEAVE_ALONE;
	titleSep->addChild(camera);

	d->titleFont = new SoFont;
	d->titleFont->name = "NOTO SANS KR : BOLD";
	d->titleFont->size = 18;
	d->titleFont->setFontPaths("font");
	d->titleFont->renderStyle = SoFont::TEXTURE;
	titleSep->addChild(d->titleFont);

	d->titleColor = new SoBaseColor;
	d->titleColor->rgb.setValue(d->titleRGB);
	titleSep->addChild(d->titleColor);

	d->titleTrans = new SoTranslation;
	d->titleTrans->translation.setValue(-0.95f, -0.95f, 0.);
	titleSep->addChild(d->titleTrans);

	d->titleText = new SoText2;
	d->titleText->string = d->titleString.toStdString();
	titleSep->addChild(d->titleText);

	d->titleSwitch->addChild(titleSep);
	d->titleSwitch->whichChild = 0;
	sceneRoot->addChild(d->titleSwitch);
}

auto QOivRenderWindow::initSettingIcon(SoSeparator* sceneRoot) -> void {
	if (nullptr == d->settingSwitch) {
		d->settingSwitch = new SoSwitch;
	} else {
		d->settingSwitch->removeAllChildren();
	}
	auto p2dSep = new SoSeparator;
	d->settingSwitch->addChild(p2dSep);
	d->settingSwitch->whichChild = 0;

	auto con = new SoImage;
	auto image = new QImage(":/img/ic-setting.svg");
	image->convertTo(QImage::Format_RGBA8888);
	con->image.setValue(SbVec2s(16, 16), 4, image->bits());
	delete image;
	con->setName("icon_file");
	con->width = 26;
	con->height = 26;

	auto trans = new SoTranslation();
	trans->translation.setValue(-26, -26, 0.0);

	//sceneRoot->addChild(p2dSep);
	sceneRoot->addChild(d->settingSwitch);
	auto bb = new SoBBox;
	bb->mode = SoBBox::NO_BOUNDING_BOX;
	p2dSep->addChild(bb);
	addPixelSpaceCamera(p2dSep, TRUE, TRUE);
	p2dSep->addChild(trans);
	p2dSep->addChild(con);
}

auto QOivRenderWindow::forceArrowCursor(bool force) -> void {
	if (force) {
		d->renderArea->setCursor(QCursor());
	}
	d->forceArrow = force;
}


auto QOivRenderWindow::isOrbit() -> bool {
	return true;
}

auto QOivRenderWindow::isNavigation() -> bool {
	return d->renderArea->getSceneInteractor()->getInteractionMode() == SceneExaminer::InteractionMode::NAVIGATION;
}

auto QOivRenderWindow::setInteractionMode(bool isNavi) -> void {
	if (isNavi) {
		d->renderArea->getSceneInteractor()->setInteractionMode(SceneExaminer::NAVIGATION);
		if (!d->forceArrow)
			d->renderArea->setCursor(d->spinCursor);
	} else {
		d->renderArea->getSceneInteractor()->setInteractionMode(SceneExaminer::SELECTION);
		d->renderArea->setCursor(QCursor());
	}
}

auto QOivRenderWindow::closeEvent(QCloseEvent* unused) -> void {
	Q_UNUSED(unused)
}

auto QOivRenderWindow::setSeekMode(bool seek) -> void {
	d->renderArea->getSceneInteractor()->setSeekMode(seek);
}

auto QOivRenderWindow::getQtRenderArea() -> QWidget* {
	return d->renderArea->getContainerWidget();
}

auto QOivRenderWindow::OnKeyboardEvent(void* data, SoEventCallback* node) -> void {
	auto instance = static_cast<QOivRenderWindow*>(data);
	instance->KeyboardEvent(node);
}

auto QOivRenderWindow::OnMouseButtonEvent(void* data, SoEventCallback* node) -> void {
	auto instance = static_cast<QOivRenderWindow*>(data);
	instance->MouseButtonEvent(node);
}

auto QOivRenderWindow::OnMouseMoveEvent(void* data, SoEventCallback* node) -> void {
	auto instance = static_cast<QOivRenderWindow*>(data);
	const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
	if (SoLocation2Event::MOUSE_LEAVE == moveEvent->getEventSource()) {
		if (instance->isNavigation()) {
			auto root = instance->getSceneGraph();
			if (root) {
				auto color = MedicalHelper::find<SoImage>(root, "icon_file");
				if (color) {
					auto image = new QImage(":/img/ic-setting.svg");
					image->convertTo(QImage::Format_RGBA8888);
					color->image.setValue(SbVec2s(16, 16), 4, image->bits());
					delete image;
				}
			}
		}
		return;
	}
	instance->MouseMoveEvent(node);
}

auto QOivRenderWindow::OnMouseWheelEvent(void* data, SoEventCallback* node) -> void {
	auto instance = static_cast<QOivRenderWindow*>(data);
	instance->MouseWheelEvent(node);
}

auto QOivRenderWindow::InteractiveCount(bool inc) -> void {
	if (inc) {
		d->renderArea->setInteractiveMode(SoInteractiveComplexity::InteractiveMode::FORCE_INTERACTION);
	} else {
		d->renderArea->setInteractiveMode(SoInteractiveComplexity::InteractiveMode::FORCE_STILL);
	}
}

auto QOivRenderWindow::isAltPressed() -> bool {
	return d->m_altSwitchBack;
}

auto QOivRenderWindow::setAltPressed(bool alt) -> void {
	d->m_altSwitchBack = true;
}

auto QOivRenderWindow::setGradientBackground(SbVec3f start, SbVec3f end) -> void {
	d->gradient->color0.setValue(start);
	d->gradient->color1.setValue(end);
}

auto QOivRenderWindow::clearOrbitCenter() -> void {
	//d->guiAlgo->resetRotationTargetPosition();
	d->renderArea->getSceneInteractor()->getCameraInteractor()->setRotationCenter(SbVec3f(0, 0, 0));
}

auto QOivRenderWindow::setOrbitConstraint(bool x, bool y, bool z) -> void {
	//d->guiAlgo->constrainCameraRotation(x, y, z);
	d->renderArea->getSceneInteractor()->getCameraInteractor()->setRotationAxis(SbVec3f(static_cast<int>(x), static_cast<int>(y), static_cast<int>(z)));
}

auto QOivRenderWindow::rotateTick(SbVec3f axis, float val) -> void {
	//SoDB::writelock();
	d->renderArea->getSceneInteractor()->getCameraInteractor()->setRotationAxis(axis);
	d->renderArea->getSceneInteractor()->getCameraInteractor()->rotate(val);
	//SoDB::writeunlock();
}

auto QOivRenderWindow::startPan(SbVec2f startLoc) -> void {
	d->renderArea->getSceneInteractor()->getCameraInteractor()->activatePanning(startLoc, getViewportRegion());
}

auto QOivRenderWindow::panCamera(SbVec2f newLoc) -> void {
	d->renderArea->getSceneInteractor()->getCameraInteractor()->pan(newLoc, getViewportRegion());
}


auto QOivRenderWindow::startSpin(SbVec2f startLoc) -> void {
	d->renderArea->getSceneInteractor()->getCameraInteractor()->activateOrbiting(startLoc);
}

auto QOivRenderWindow::spinCamera(SbVec2f newLoc) -> void {
	d->renderArea->getSceneInteractor()->getCameraInteractor()->orbit(newLoc);
}


auto QOivRenderWindow::setCameraType(bool isOrthographic) -> void {
	if (isOrthographic) {
		d->renderArea->getSceneInteractor()->setCameraMode(SceneInteractor::ORTHOGRAPHIC);
	} else {
		d->renderArea->getSceneInteractor()->setCameraMode(SceneInteractor::PERSPECTIVE);
	}
}

auto QOivRenderWindow::getSceneGraph() -> SoNode* {
	return d->renderArea->getSceneGraph();
}

auto QOivRenderWindow::getViewportRegion() -> SbViewportRegion {
	return d->renderArea->getSceneInteractor()->getCamera()->getLastViewportRegion();
}

auto QOivRenderWindow::getContext() -> SoGLContext* {
	const auto id = d->renderArea->getGLRenderAction()->getCacheContext();
	return SoGLContext::getContextFromId(id);
}

auto QOivRenderWindow::getRenderBuffer(int sampling) -> QPixmap {
	const auto width = d->renderArea->getGLRenderAction()->getViewportRegion().getWindowSize_i32()[0] * sampling;
	const auto height = d->renderArea->getGLRenderAction()->getViewportRegion().getWindowSize_i32()[1] * sampling;

	SoOffscreenRenderArea offscreen(getContext());
	offscreen.setSize(SbVec2i32(width, height));
	//offscreen.setClearPolicy(SiRenderArea::ClearPolicy::DEPTHBUFFER);
	offscreen.setSceneGraph(getSceneGraph());

	SoRef<SoCpuBufferObject> bufferObject = new SoCpuBufferObject;
	offscreen.renderToBuffer(bufferObject.ptr(), SoOffscreenRenderArea::OutputFormat::RGBA);
	const auto buffer = static_cast<unsigned char*>(bufferObject->map(SoBufferObject::READ_ONLY));

	const QImage image(buffer, width, height, QImage::Format::Format_RGBX8888);
	const auto invertImg = image.mirrored(); //image is inverted and rotated by 180 degree

	auto pixmap = QPixmap::fromImage(invertImg);

	bufferObject->unmap();

	return pixmap;
}

bool QOivRenderWindow::saveSnapshot(const QString& filename, int sampling, bool overwrite) {
	const auto width = d->renderArea->getGLRenderAction()->getViewportRegion().getWindowSize_i32()[0] * sampling;
	const auto height = d->renderArea->getGLRenderAction()->getViewportRegion().getWindowSize_i32()[1] * sampling;

	auto root = getSceneGraph();

	SoOffscreenRenderArea offscreen(getContext());
	offscreen.setSize(SbVec2i32(width, height));
	offscreen.setSceneGraph(root);

	SoRef<SoCpuBufferObject> bufferObject = new SoCpuBufferObject;
	offscreen.renderToBuffer(bufferObject.ptr(), SoOffscreenRenderArea::OutputFormat::RGBA);
	const auto buffer = static_cast<unsigned char*>(bufferObject->map(SoBufferObject::READ_ONLY));

	const QImage image(buffer, width, height, QImage::Format::Format_RGBX8888);
	auto result = image.mirrored().save(filename);

	bufferObject->unmap();
	
	return result;
}

auto QOivRenderWindow::SetViewDirection(int axis, QString name) -> void {
	if (d->is2D)
		return;

	const auto root = getSceneGraph();
	SoVolumeData* volData;
	if (name.isEmpty()) {
		volData = MedicalHelper::find<SoVolumeData>(root);
	} else {
		QString nodeName = name;
		QString invalidLetterInOiv = " +{}[]'""\".";
		for (const QChar character : invalidLetterInOiv) {
			nodeName.replace(character, '_');
		}
		volData = MedicalHelper::find<SoVolumeData>(root, nodeName.toStdString());
	}
	if (nullptr == volData) {
		return;
	}

	switch (axis) {
		case 0:
			MedicalHelper::orientView(MedicalHelper::SAGITTAL, getCamera(), volData);
			break;
		case 1:
			MedicalHelper::orientView(MedicalHelper::CORONAL, getCamera(), volData);
			break;
		case 2:
			MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
			break;
		default: ;
	}
	setCameraZoom(getCameraZoom() * 0.7f);
}
