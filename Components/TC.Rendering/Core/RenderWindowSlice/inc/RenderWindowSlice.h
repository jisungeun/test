#pragma once
#include <memory>
#include <enum.h>

#include <QOivRenderWindow.h>

#include "TC.Rendering.Core.RenderWindow2DExport.h"

BETTER_ENUM(Direction2D,int,
	XY = 0,
	YZ = 1,
	XZ = 2)

class TC_Rendering_Core_RenderWindow2D_API RenderWindow2D : public QOivRenderWindow {
	Q_OBJECT
public:
	RenderWindow2D(QWidget* parent);
	~RenderWindow2D();

	auto ResetView2D(Direction2D dir)->void;

signals:	
	void sigMouseWheel(int);
	void sigMousePos(float, float);

protected:
	auto MouseButtonEvent(SoEventCallback* node) -> void override;
	auto MouseMoveEvent(SoEventCallback* node) -> void override;
	auto KeyboardEvent(SoEventCallback* node) -> void override;
	auto MouseWheelEvent(SoEventCallback* node) -> void override;

	auto setDefaultWindowType() -> void override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};