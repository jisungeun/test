#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/events/SoKeyboardEvent.h>

#include "RenderWindowVolume.h"

struct RenderWindow3D::Impl {
    bool mouseLeftPressed{ false };

    bool mouseMiddlePressed{ false };
};

RenderWindow3D::RenderWindow3D(QWidget* parent) : QOivRenderWindow(parent, false), d{ new Impl } {
    setDefaultWindowType();
}

RenderWindow3D::~RenderWindow3D() {
    
}

auto RenderWindow3D::setDefaultWindowType() -> void {
    //set default gradient background color
    SbVec3f start_color = { 0.7f,0.7f,0.8f };
    SbVec3f end_color = { 0.0,0.1f,0.3f };
    setGradientBackground(start_color, end_color);

    //set default camera type
    setCameraType(false);//perpective
}

auto RenderWindow3D::MouseButtonEvent(SoEventCallback* node) -> void {    
    if(false == isNavigation()) {
        return;
    }
    const auto mouseButton = reinterpret_cast<const SoMouseButtonEvent*>(node->getEvent());
    const auto vr = getViewportRegion();
    const auto norm_pos = mouseButton->getNormalizedPosition(vr);
    if(SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton,SoMouseButtonEvent::BUTTON3)) {        
        viewZ();
    }
    if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {        
        InteractiveCount(true);
        startSpin(norm_pos);
        d->mouseLeftPressed = true;
        emit sigMouseClick();
    }
    if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
        InteractiveCount(false);
        d->mouseLeftPressed = false;
    }
    if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
        InteractiveCount(true);        
        startPan(norm_pos);
        d->mouseMiddlePressed = true;
    }
    if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
        InteractiveCount(false);
        d->mouseMiddlePressed = false;
    }
}

auto RenderWindow3D::MouseMoveEvent(SoEventCallback* node) -> void {    
    if(false == isNavigation()) {
        return;
    }
    const auto moveEvent = reinterpret_cast<const SoLocation2Event*>(node->getEvent());
    const auto vr = getViewportRegion();
    const auto norm_pos = moveEvent->getNormalizedPosition(vr);
    if(d->mouseLeftPressed) {
        spinCamera(norm_pos);
    }
    if(d->mouseMiddlePressed) {
        panCamera(norm_pos);
    }
}

auto RenderWindow3D::MouseWheelEvent(SoEventCallback* node) -> void {
    const auto mouseWheel = reinterpret_cast<const SoMouseWheelEvent*>(node->getEvent());
    auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
    auto zoom = getCameraZoom();
    setCameraZoom(zoom + static_cast<float>(delta));
}

auto RenderWindow3D::KeyboardEvent(SoEventCallback* node) -> void {
    const auto keyEvent = node->getEvent();
    if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {        
        if (!isNavigation()) {
            setInteractionMode(true);
            setAltPressed(true);
        }
        node->setHandled();
    }
    if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
        if (isAltPressed()) {
            setInteractionMode(false);
            setAltPressed(false);
        }
        node->setHandled();
    }
}