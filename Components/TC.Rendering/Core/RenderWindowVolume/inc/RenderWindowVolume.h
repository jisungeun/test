#pragma once
#include <memory>

#include <QOivRenderWindow.h>

#include "TC.Rendering.Core.RenderWindow3DExport.h"

class TC_Rendering_Core_RenderWindow3D_API RenderWindow3D : public QOivRenderWindow {
	Q_OBJECT
public:
	RenderWindow3D(QWidget* parent);
	~RenderWindow3D();

signals:
	void sigWheel(int);
	void sigMousePos(float, float);
	void sigMouseClick();

protected:
	auto MouseButtonEvent(SoEventCallback* node) -> void override;
	auto MouseMoveEvent(SoEventCallback* node) -> void override;
	auto KeyboardEvent(SoEventCallback* node) -> void override;
	auto MouseWheelEvent(SoEventCallback* node) -> void override;

	auto setDefaultWindowType() -> void override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};