#pragma once

#include <enum.h>
#include <QWidget>

#include "TC.Rendering.LookupTable.TransparencyMapExport.h"

class TransparencyMap;
using TransparencyMapPtr = std::shared_ptr<TransparencyMap>;
class TC_Rendering_LookupTable_TransparencyMap_API TransparencyMap {
public:
	TransparencyMap();
	~TransparencyMap();

	enum SearchMethod {
		BINARY_SEARCH = 0,
		INTERPOLATION_SEARCH = 1,
		MAX_ENUM = 2
	};

	auto BuildDefaultMap()->void;

	auto GetSize() -> int;
		
	auto AddPoint(double x, double y) ->int;
	auto AddPoint(double x, double y, double midpoint, double sharpness) ->int;
		
	auto RemovePointByIndex(size_t id) ->bool;	
	auto RemovePoint(double x) -> int;	
	auto RemovePoint(double x, double y) ->int;
	auto RemoveAllPoints()->void;

	auto AddSegment(double x1, double y1, double x2, double y2) -> void;
	
	auto GetValue(double x) ->double;
	
	auto GetNodeValue(int index, double val[4]) ->int;
	auto SetNodeValue(int index, double val[4]) ->int;
		
	auto GetDataPointer()->double*;
	auto FillFromDataPointer(int, double*)->void;

	auto AdjustRange(double range[2]) ->int;

	auto GetRange() -> double*;

	auto GetTable(double x1, double x2, int size, float* table, int stride = 1, int logIncrements = 0,
		double epsilon = 1e-5) ->void;
	auto GetTable(double x1, double x2, int size, double* table, int stride = 1,
		int logIncrements = 0, double epsilon = 1e-5)->void;
	
	auto BuildFunctionFromTable(double x1, double x2, int size, double* table, int stride = 1)->void;
		
	auto GetType()->const char*;
		
	auto GetFirstNonZeroValue()->double;
		
	auto Initialize()->void;
	
	auto EnableDuplicateScalar(bool enable) -> void;

	auto EstimateMinNumberOfSamples(double const& x1, double const& x2)->int;

	auto UpdateSearchMethod(double epsilon = 1e-12, double thresh = 1e-4)->void;

	auto GetAutomaticSearchMethod()->int;
	auto SetUseCustomSearchMethod(bool use)->void;
	auto SetCustomSearchMethod(int type)->void;
	auto GetCustomSearchMethod()->int;

	auto PrintSelf(std::ostream& os) -> void;

protected:	
	auto SortAndUpdateRange(bool updateSearchMethod = true)->void;

	auto UpdateRange()->bool;

	auto FindMinimumXDistance()->double;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
