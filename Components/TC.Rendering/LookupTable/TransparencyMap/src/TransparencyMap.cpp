#include "TransparencyMap.h"


#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iterator>
#include <limits>
#include <set>
#include <vector>

class TransparencyMapNode {
public:
	double X;
	double Y;
	double Sharpness;
	double Midpoint;
};

class TransparencyMapCompareNodes {
public:
	bool operator()(const TransparencyMapNode* node1, const TransparencyMapNode* node2) {
		return node1->X < node2->X;
	}
};

class TransparencyMapFindNodeInRange {
public:
	double X1;
	double X2;

	bool operator()(const TransparencyMapNode* node) {
		return (node->X >= X1 && node->X <= X2);
	}
};

class TransparencyMapFindNodeOutOfRange {
public:
	double X1;
	double X2;

	bool operator()(const TransparencyMapNode* node) {
		return (node->X < X1 || node->X > X2);
	}
};

class TransparencyMapInternals {
public:
	std::vector<TransparencyMapNode*> Nodes;
	TransparencyMapCompareNodes CompareNodes;
	TransparencyMapFindNodeInRange FindNodeInRange;
	TransparencyMapFindNodeOutOfRange FindNodeOutOfRange;
	TransparencyMap::SearchMethod AutomaticSearchMethod = TransparencyMap::BINARY_SEARCH;
	TransparencyMap::SearchMethod CustomSearchMethod = TransparencyMap::BINARY_SEARCH;
	bool UseCustomSearchMethod = false;
	std::vector<TransparencyMapNode*>::iterator UpperBound(TransparencyMapNode* node);
	std::vector<TransparencyMapNode*>::iterator InterpolationSearch(
		TransparencyMapNode* node);
};

struct TransparencyMap::Impl {
	TransparencyMapInternals* Internal;

	double* Function;
	double Range[2];

	bool Clamping;
	bool AllowDuplicateScalars;
	bool UseLogScale;
};

TransparencyMap::TransparencyMap() : d { new Impl } {
	d->Clamping = 1;
	d->Range[0] = 0;
	d->Range[1] = 0;

	d->Function = nullptr;

	d->AllowDuplicateScalars = false;
	d->UseLogScale = false;

	d->Internal = new TransparencyMapInternals;

	BuildDefaultMap();
}

TransparencyMap::~TransparencyMap() {
	delete[] d->Function;

	for (unsigned int i = 0; i < d->Internal->Nodes.size(); i++) {
		delete d->Internal->Nodes[i];
	}
	d->Internal->Nodes.clear();
	delete d->Internal;
}

auto TransparencyMap::BuildDefaultMap() -> void {
	RemoveAllPoints();

	AddPoint(0, 0);
	AddPoint(255, 1);
}

auto TransparencyMap::Initialize() -> void {
	RemoveAllPoints();
}

auto TransparencyMap::GetSize() -> int {
	return static_cast<int>(d->Internal->Nodes.size());
}

auto TransparencyMap::GetType() -> const char* {
	unsigned int i;
	double value;
	double prev_value = 0.0;
	int function_type;

	function_type = 0;

	if (!d->Internal->Nodes.empty()) {
		prev_value = d->Internal->Nodes[0]->Y;
	}

	for (i = 1; i < d->Internal->Nodes.size(); i++) {
		value = d->Internal->Nodes[i]->Y;

		if (value != prev_value) {
			if (value > prev_value) {
				switch (function_type) {
					case 0:
					case 1:
						function_type = 1;
						break;
					case 2:
						function_type = 3;
						break;
				}
			} else {
				switch (function_type) {
					case 0:
					case 2:
						function_type = 2;
						break;
					case 1:
						function_type = 3;
						break;
				}
			}
		}

		prev_value = value;

		if (function_type == 3) {
			break;
		}
	}

	switch (function_type) {
		case 0:
			return "Constant";
		case 1:
			return "NonDecreasing";
		case 2:
			return "NonIncreasing";
		case 3:
			return "Varied";
	}

	return "Unknown";
}

auto TransparencyMap::GetDataPointer() -> double* {
	int size = static_cast<int>(d->Internal->Nodes.size());

	delete[] d->Function;
	d->Function = nullptr;

	if (size > 0) {
		d->Function = new double[size * 2];
		for (int i = 0; i < size; i++) {
			d->Function[2 * i] = d->Internal->Nodes[i]->X;
			d->Function[2 * i + 1] = d->Internal->Nodes[i]->Y;
		}
	}

	return d->Function;
}

auto TransparencyMap::GetFirstNonZeroValue() -> double {
	if (d->Internal->Nodes.empty()) {
		return 0;
	}

	unsigned int i;
	int all_zero = 1;
	double x = 0.0;
	for (i = 0; i < d->Internal->Nodes.size(); i++) {
		if (d->Internal->Nodes[i]->Y != 0.0) {
			all_zero = 0;
			break;
		}
	}

	if (all_zero) {
		x = INT_MAX;
	} else {
		if (i > 0) {
			x = d->Internal->Nodes[i - 1]->X;
		} else {
			if (d->Clamping) {
				x = INT_MIN;
			} else {
				x = d->Internal->Nodes[0]->X;
			}
		}
	}

	return x;
}

auto TransparencyMap::GetNodeValue(int index, double val[4]) -> int {
	int size = static_cast<int>(d->Internal->Nodes.size());

	if (index < 0 || index >= size) {
		std::cout << "Index out of range!" << std::endl;
		return -1;
	}

	val[0] = d->Internal->Nodes[index]->X;
	val[1] = d->Internal->Nodes[index]->Y;
	val[2] = d->Internal->Nodes[index]->Midpoint;
	val[3] = d->Internal->Nodes[index]->Sharpness;

	return 1;
}

auto TransparencyMap::SetNodeValue(int index, double val[4]) -> int {
	int size = static_cast<int>(d->Internal->Nodes.size());

	if (index < 0 || index >= size) {
		std::cout << "Index out of range!" << std::endl;
		return -1;
	}

	double oldX = d->Internal->Nodes[index]->X;
	d->Internal->Nodes[index]->X = val[0];
	d->Internal->Nodes[index]->Y = val[1];
	d->Internal->Nodes[index]->Midpoint = val[2];
	d->Internal->Nodes[index]->Sharpness = val[3];

	if (oldX != val[0]) {
		SortAndUpdateRange();
	} else {
		
	}

	return 1;
}

auto TransparencyMap::AddPoint(double x, double y) -> int {	
	return AddPoint(x, y, 0.5, 0.0);
}

auto TransparencyMap::AddPoint(double x, double y, double midpoint, double sharpness) -> int {
	if (midpoint < 0.0 || midpoint > 1.0) {
		std::cout << "Midpoint outside range [0.0, 1.0]" << std::endl;
		return -1;
	}

	if (sharpness < 0.0 || sharpness > 1.0) {
		std::cout << "Sharpness outside range [0.0, 1.0]" << std::endl;
		return -1;
	}

	if(x < 0 || x > 255) {
		return - 1;
	}

	if(y < 0 || y > 1) {
		return -1;
	}

	if (!d->AllowDuplicateScalars) {
		RemovePoint(x);
	}

	TransparencyMapNode* node = new TransparencyMapNode;
	node->X = x;
	node->Y = y;
	node->Sharpness = sharpness;
	node->Midpoint = midpoint;

	d->Internal->Nodes.push_back(node);
	SortAndUpdateRange();

	unsigned int i;
	for (i = 0; i < d->Internal->Nodes.size(); i++) {
		if (d->Internal->Nodes[i]->X == x && d->Internal->Nodes[i]->Y == y) {
			break;
		}
	}

	int retVal;

	if (i < d->Internal->Nodes.size()) {
		retVal = i;
	} else {
		retVal = -1;
	}

	return retVal;
}

auto TransparencyMap::SortAndUpdateRange(bool updateSearchMethod) -> void {
	std::stable_sort(
					d->Internal->Nodes.begin(), d->Internal->Nodes.end(), d->Internal->CompareNodes);
	bool modifiedInvoked = UpdateRange();
	if (!modifiedInvoked) {
		
	}

	if (updateSearchMethod) {
		UpdateSearchMethod();
	}
}

auto TransparencyMap::UpdateRange() -> bool {
	double oldRange[2];
	oldRange[0] = d->Range[0];
	oldRange[1] = d->Range[1];

	int size = static_cast<int>(d->Internal->Nodes.size());
	if (size) {
		d->Range[0] = d->Internal->Nodes[0]->X;
		d->Range[1] = d->Internal->Nodes[size - 1]->X;
	} else {
		d->Range[0] = 0;
		d->Range[1] = 0;
	}
	if (oldRange[0] == d->Range[0] && oldRange[1] == d->Range[1]) {
		return false;
	}

	
	return true;
}

auto TransparencyMap::RemovePoint(double x) -> int {
	size_t i;
	for (i = 0; i < d->Internal->Nodes.size(); i++) {
		if (fabs(d->Internal->Nodes[i]->X - x) < 0.1) {
			break;
		}
	}

	if (i == d->Internal->Nodes.size()) {
		return -1;
	}

	RemovePointByIndex(i);
	return static_cast<int>(i);
}

auto TransparencyMap::RemovePoint(double x, double y) -> int {
	size_t i;
	for (i = 0; i < d->Internal->Nodes.size(); i++) {
		if (fabs(d->Internal->Nodes[i]->X - x) < 0.1 && fabs(d->Internal->Nodes[i]->Y - y) < 0.1) {
			break;
		}
	}

	if (i == d->Internal->Nodes.size()) {
		return -1;
	}

	RemovePointByIndex(i);
	return static_cast<int>(i);
}

auto TransparencyMap::RemovePointByIndex(size_t id) -> bool {
	if (id > d->Internal->Nodes.size()) {
		return false;
	}

	delete d->Internal->Nodes[id];
	d->Internal->Nodes.erase(d->Internal->Nodes.begin() + id);

	bool modifiedInvoked = false;
	if (id == 0 || id == d->Internal->Nodes.size()) {
		modifiedInvoked = UpdateRange();
	}
	if (!modifiedInvoked) {
		
	}
	return true;
}

auto TransparencyMap::RemoveAllPoints() -> void {
	for (unsigned int i = 0; i < d->Internal->Nodes.size(); i++) {
		delete d->Internal->Nodes[i];
	}
	d->Internal->Nodes.clear();

	SortAndUpdateRange(false);
}

auto TransparencyMap::AddSegment(double x1, double y1, double x2, double y2) -> void {
	int done;

	done = 0;
	while (!done) {
		done = 1;

		d->Internal->FindNodeInRange.X1 = x1;
		d->Internal->FindNodeInRange.X2 = x2;

		std::vector<TransparencyMapNode*>::iterator iter = std::find_if(
																		d->Internal->Nodes.begin(), d->Internal->Nodes.end(), d->Internal->FindNodeInRange);

		if (iter != d->Internal->Nodes.end()) {
			delete*iter;
			d->Internal->Nodes.erase(iter);
			
			done = 0;
		}
	}

	AddPoint(x1, y1, 0.5, 0.0);
	AddPoint(x2, y2, 0.5, 0.0);
}

auto TransparencyMap::GetValue(double x) -> double {
	double table[1];
	GetTable(x, x, 1, table);
	return table[0];
}

auto TransparencyMap::GetRange() -> double* {
	return d->Range;
}


auto TransparencyMap::AdjustRange(double range[2]) -> int {
	if (!range) {
		return 0;
	}

	double* function_range = GetRange();

	if (function_range[0] < range[0]) {
		AddPoint(range[0], GetValue(range[0]));
	} else {
		AddPoint(range[0], GetValue(function_range[0]));
	}

	if (function_range[1] > range[1]) {
		AddPoint(range[1], GetValue(range[1]));
	} else {
		AddPoint(range[1], GetValue(function_range[1]));
	}

	int done;

	done = 0;
	while (!done) {
		done = 1;

		d->Internal->FindNodeOutOfRange.X1 = range[0];
		d->Internal->FindNodeOutOfRange.X2 = range[1];

		std::vector<TransparencyMapNode*>::iterator iter =
			std::find_if(d->Internal->Nodes.begin(), d->Internal->Nodes.end(),
						d->Internal->FindNodeOutOfRange);

		if (iter != d->Internal->Nodes.end()) {
			delete*iter;
			d->Internal->Nodes.erase(iter);
			
			done = 0;
		}
	}

	SortAndUpdateRange();
	return 1;
}

auto TransparencyMap::EstimateMinNumberOfSamples(double const& x1, double const& x2) -> int {
	double const d = FindMinimumXDistance();
	int idealWidth = static_cast<int>(ceil((x2 - x1) / d));

	return idealWidth;
}

auto TransparencyMap::FindMinimumXDistance() -> double {
	std::vector<TransparencyMapNode*> const& nodes = d->Internal->Nodes;
	size_t const size = nodes.size();
	if (size < 2)
		return -1.0;

	double distance = std::numeric_limits<double>::max();
	for (size_t i = 0; i < size - 1; i++) {
		double const currentDist = nodes[i + 1]->X - nodes[i]->X;
		if (currentDist < distance) {
			distance = currentDist;
		}
	}

	return distance;
}

std::vector<TransparencyMapNode*>::iterator TransparencyMapInternals::InterpolationSearch(
	TransparencyMapNode* node) {
	if (Nodes.empty()) {
		return Nodes.end();
	}

	std::vector<TransparencyMapNode*>::iterator begin = Nodes.begin();
	std::vector<TransparencyMapNode*>::iterator end = Nodes.end();
	std::vector<TransparencyMapNode*>::iterator mid = Nodes.begin();
	std::vector<TransparencyMapNode*>::iterator lastNode = end - 1;

	if (node->X > (*lastNode)->X) {
		return Nodes.end();
	}

	bool side = true;

	while ((*begin)->X <= node->X && node->X <= (*lastNode)->X && begin != end) {
		double fraction = (node->X - (*begin)->X) / ((*lastNode)->X - (*begin)->X);
		mid = begin +
			std::iterator_traits<std::vector<TransparencyMapNode*>::iterator>::difference_type(
																								fraction * (std::distance(begin, end) - 1));

		if ((*mid)->X < node->X) {
			begin = mid + 1;
			side = false;
		} else if (node->X > (*lastNode)->X) {
			end = mid;
			side = true;
		} else {
			return mid;
		}
	}

	return (side == false) ? mid + 1 : mid;
}

std::vector<TransparencyMapNode*>::iterator TransparencyMapInternals::UpperBound(
	TransparencyMapNode* node) {
	TransparencyMapCompareNodes comparator;
	TransparencyMap::SearchMethod searchMethod = AutomaticSearchMethod;

	if (UseCustomSearchMethod) {
		searchMethod = CustomSearchMethod;
	}

	if (searchMethod == TransparencyMap::BINARY_SEARCH) {
		return std::upper_bound(Nodes.begin(), Nodes.end(), node, comparator);
	} else if (searchMethod == TransparencyMap::INTERPOLATION_SEARCH) {
		return InterpolationSearch(node);
	} else {
		std::cout << "The search method should only be binary search or interpolation search." << std::endl;
		return Nodes.begin();
	}
}

auto TransparencyMap::GetTable(
	double start, double end, int size, double* table, int stride, int logIncrements, double epsilon) -> void {
	int numNodes = static_cast<int>(d->Internal->Nodes.size());
	double* tptr = nullptr;
	double xLoc = 0.0;
	double xStart = start;
	double xEnd = end;

	double lastValue = 0.0;
	if (numNodes != 0) {
		lastValue = d->Internal->Nodes[numNodes - 1]->Y;
	}

	if (logIncrements) {
		xStart = std::log10(xStart);
		xEnd = std::log10(xEnd);
	}

	for (int i = 0; i < size; i++) {
		tptr = table + stride * i;

		if (size > 1) {
			xLoc = xStart + (static_cast<double>(i) / static_cast<double>(size - 1)) * (xEnd - xStart);
		} else {
			xLoc = 0.5 * (xStart + xEnd);
		}
		if (logIncrements) {
			xLoc = std::pow(10., xLoc);
		}

		TransparencyMapNode node;
		node.X = xLoc;
		std::vector<TransparencyMapNode*>::iterator lowBound;
		std::vector<TransparencyMapNode*>::iterator upBound;
		upBound = d->Internal->UpperBound(&node);

		if (upBound == d->Internal->Nodes.end()) {
			*tptr = d->Clamping ? lastValue : 0.0;
		} else if (upBound == d->Internal->Nodes.begin()) {
			*tptr = d->Clamping ? d->Internal->Nodes[0]->Y : 0.0;
		} else {
			double x1, x2, y1, y2, midpoint, sharpness;

			lowBound = upBound - 1;
			x1 = (*lowBound)->X;
			x2 = (*upBound)->X;
			y1 = (*lowBound)->Y;
			y2 = (*upBound)->Y;

			midpoint = (*lowBound)->Midpoint;
			sharpness = (*lowBound)->Sharpness;

			if (midpoint < epsilon) {
				midpoint = epsilon;
			}

			if (midpoint > 1 - epsilon) {
				midpoint = 1 - epsilon;
			}
			double scale;
			if (d->UseLogScale) {
				double xLog = std::log10(xLoc);
				double x1Log = std::log10(x1);
				double x2Log = std::log10(x2);
				scale = (xLog - x1Log) / (x2Log - x1Log);
			} else {
				scale = (xLoc - x1) / (x2 - x1);
			}

			if (scale < midpoint) {
				scale = 0.5 * scale / midpoint;
			} else {
				scale = 0.5 + 0.5 * (scale - midpoint) / (1.0 - midpoint);
			}

			if (sharpness > 0.99) {
				*tptr = scale < 0.5 ? y1 : y2;
			}

			if (sharpness < 0.01) {
				*tptr = (1 - scale) * y1 + scale * y2;
				continue;
			}

			if (scale < 0.5) {
				scale = 0.5 * std::pow(scale * 2, 1.0 + 10 * sharpness);
			} else if (scale > 0.5) {
				scale = 1.0 - 0.5 * std::pow((1.0 - scale) * 2, 1 + 10 * sharpness);
			}

			double ss = scale * scale;
			double sss = ss * scale;

			double h1 = 2 * sss - 3 * ss + 1;
			double h2 = -2 * sss + 3 * ss;
			double h3 = sss - 2 * ss + scale;
			double h4 = sss - ss;

			double slope;
			double t;

			slope = y2 - y1;
			t = (1.0 - sharpness) * slope;

			*tptr = h1 * y1 + h2 * y2 + h3 * t + h4 * t;

			double min = (y1 < y2) ? (y1) : (y2);
			double max = (y1 > y2) ? (y1) : (y2);

			*tptr = (*tptr < min) ? (min) : (*tptr);
			*tptr = (*tptr > max) ? (max) : (*tptr);
		}
	}
}

auto TransparencyMap::GetTable(
	double xStart, double xEnd, int size, float* table, int stride, int logIncrements, double epsilon) -> void {
	double* tmpTable = new double[size];

	GetTable(xStart, xEnd, size, tmpTable, 1, logIncrements, epsilon);

	double* tmpPtr = tmpTable;
	float* tPtr = table;

	for (int i = 0; i < size; i++) {
		*tPtr = static_cast<float>(*tmpPtr);
		tPtr += stride;
		tmpPtr++;
	}

	delete[] tmpTable;
}

auto TransparencyMap::BuildFunctionFromTable(
	double xStart, double xEnd, int size, double* table, int stride) -> void {
	double inc = 0.0;
	double* tptr = table;

	RemoveAllPoints();

	if (size > 1) {
		inc = (xEnd - xStart) / static_cast<double>(size - 1);
	}

	int i;
	for (i = 0; i < size; i++) {
		TransparencyMapNode* node = new TransparencyMapNode;
		node->X = xStart + inc * i;
		node->Y = *tptr;
		node->Sharpness = 0.0;
		node->Midpoint = 0.5;

		d->Internal->Nodes.push_back(node);
		tptr += stride;
	}

	SortAndUpdateRange();
}

auto TransparencyMap::FillFromDataPointer(int nb, double* ptr) -> void {
	if (nb <= 0 || !ptr) {
		return;
	}

	RemoveAllPoints();

	double* inPtr = ptr;

	int i;
	for (i = 0; i < nb; i++) {
		TransparencyMapNode* node = new TransparencyMapNode;
		node->X = inPtr[0];
		node->Y = inPtr[1];
		node->Sharpness = 0.0;
		node->Midpoint = 0.5;

		d->Internal->Nodes.push_back(node);
		inPtr += 2;
	}

	SortAndUpdateRange();
}

auto TransparencyMap::UpdateSearchMethod(double epsilon, double thresh) -> void {
	double averageDiff = 0;
	double stdDiff = 0;
	double currDiff = 0;
	const size_t nodeCount = d->Internal->Nodes.size();

	if (nodeCount < 3) {
		d->Internal->AutomaticSearchMethod = BINARY_SEARCH;
		return;
	}

	averageDiff = (d->Internal->Nodes[nodeCount - 1]->X - d->Internal->Nodes[0]->X) /
		static_cast<double>(nodeCount);

	if (std::abs(averageDiff) < epsilon) {
		d->Internal->AutomaticSearchMethod = BINARY_SEARCH;
		return;
	}

	for (size_t k = 0; k < d->Internal->Nodes.size() - 1; ++k) {
		currDiff = d->Internal->Nodes[k + 1]->X - d->Internal->Nodes[k]->X;
		stdDiff += std::pow(currDiff - averageDiff, 2);
	}

	stdDiff /= std::max(static_cast<double>(d->Internal->Nodes.size() - 1), 1.0);
	stdDiff = std::sqrt(stdDiff);

	double C = std::abs(stdDiff / averageDiff);

	if (C < thresh) {
		d->Internal->AutomaticSearchMethod = INTERPOLATION_SEARCH;
	} else {
		d->Internal->AutomaticSearchMethod = BINARY_SEARCH;
	}
}

auto TransparencyMap::GetAutomaticSearchMethod() -> int {
	return static_cast<int>(d->Internal->AutomaticSearchMethod);
}

auto TransparencyMap::SetUseCustomSearchMethod(bool useCustomSearchMethod) -> void {
	d->Internal->UseCustomSearchMethod = useCustomSearchMethod;
}

auto TransparencyMap::SetCustomSearchMethod(int type) -> void {
	if (type < 0 || type >= static_cast<int>(MAX_ENUM)) {
		std::cout << "enum out of scope, binary search will be applied" << std::endl;

		d->Internal->CustomSearchMethod = BINARY_SEARCH;
	}

	d->Internal->CustomSearchMethod = static_cast<SearchMethod>(type);
}

auto TransparencyMap::GetCustomSearchMethod() -> int {
	return static_cast<int>(d->Internal->CustomSearchMethod);
}

auto TransparencyMap::PrintSelf(std::ostream& os) -> void {
	os << "Clamping: " << d->Clamping << std::endl;
	os << "Range: [" << d->Range[0] << "," << d->Range[1] << "]" << std::endl;
	os << "Function Points: " << d->Internal->Nodes.size() << std::endl;
	for (auto i = 0; i < d->Internal->Nodes.size(); i++) {
		os << "  " << i << " X: " << d->Internal->Nodes[i]->X
			<< " Y: " << d->Internal->Nodes[i]->Y
			<< " Sharpness: " << d->Internal->Nodes[i]->Sharpness
			<< " Midpoint: " << d->Internal->Nodes[i]->Midpoint << std::endl;
	}
	os << "AllowDuplicateScalars: " << d->AllowDuplicateScalars << std::endl;
	os << "UseLogScale: " << d->UseLogScale << std::endl;
}

auto TransparencyMap::EnableDuplicateScalar(bool enable) -> void {
	d->AllowDuplicateScalars = enable;
}
