#include <QMap>
#include <stdlib.h>
#include "ColorMap.h"

#include <iostream>
#include <TransparencyMap.h>

typedef struct RGBAPoint {
	double xpos;
	float r;
	float g;
	float b;
	float a;
} TFNode;

struct ColorMap::Impl {
	float colormapArr[1024];
	float transpArr[256];
	QMap<int, TFNode> colormap;

	bool isLinear { true };

	TransparencyMapPtr transparency { nullptr };
	bool useTransp { false };
	auto RebuildColorMap() -> void;
};

auto ColorMap::Impl::RebuildColorMap() -> void {
	if (colormap.count() == 0) {
		return;
	}
	auto keys = colormap.keys();
	std::sort(keys.begin(), keys.end());

	const auto firstKey = keys.first();
	const auto lastKey = keys.last();

	//fill left color with first key	
	for (auto i = 0; i < firstKey; i++) {
		colormapArr[i * 4] = colormap[firstKey].r;
		colormapArr[i * 4 + 1] = colormap[firstKey].g;
		colormapArr[i * 4 + 2] = colormap[firstKey].b;
		if(false == useTransp){
			colormapArr[i * 4 + 3] = colormap[firstKey].a;
			transpArr[i] = colormap[firstKey].a;
		}else {
			colormapArr[i * 4 + 3] = static_cast<float>(transparency->GetValue(i));
		}
	}

	//fill right color with last key
	for (auto i = lastKey; i < 256; i++) {
		colormapArr[i * 4] = colormap[lastKey].r;
		colormapArr[i * 4 + 1] = colormap[lastKey].g;
		colormapArr[i * 4 + 2] = colormap[lastKey].b;
		if (false == useTransp) {
			colormapArr[i * 4 + 3] = colormap[lastKey].a;
			transpArr[i] = colormap[lastKey].a;
		}else {
			colormapArr[i * 4 + 3] = static_cast<float>(transparency->GetValue(i));
		}
	}

	//fill intermediate color with interpolated value
	if (isLinear) {
		for (auto i = 0; i < keys.count() - 1; i++) {
			for (auto j = keys[i]; j < keys[i + 1]; j++) {
				const auto ratio = static_cast<float>(j - keys[i]) / static_cast<float>(keys[i + 1] - keys[i]);
				const auto rRange = colormap[keys[i + 1]].r - colormap[keys[i]].r;
				const auto gRange = colormap[keys[i + 1]].g - colormap[keys[i]].g;
				const auto bRange = colormap[keys[i + 1]].b - colormap[keys[i]].b;
				const auto aRange = colormap[keys[i + 1]].a - colormap[keys[i]].a;

				colormapArr[j * 4] = colormap[keys[i]].r + rRange * ratio;
				colormapArr[j * 4 + 1] = colormap[keys[i]].g + gRange * ratio;
				colormapArr[j * 4 + 2] = colormap[keys[i]].b + bRange * ratio;
				if (false == useTransp) {
					colormapArr[j * 4 + 3] = colormap[keys[i]].a + aRange * ratio;
					transpArr[j] = colormap[keys[i]].a + aRange * ratio;
				}else {
					colormapArr[j * 4 + 3] = static_cast<float>(transparency->GetValue(j));
				}
			}
		}
	} else {
		for (auto i = 0; i < keys.count() - 1; i++) {
			for (auto j = keys[i]; j < keys[i + 1]; j++) {
				colormapArr[j * 4] = colormap[keys[i]].r;
				colormapArr[j * 4 + 1] = colormap[keys[i]].g;
				colormapArr[j * 4 + 2] = colormap[keys[i]].b;
				if (false == useTransp) {
					colormapArr[j * 4 + 3] = colormap[keys[i]].a;
					transpArr[j] = colormap[keys[i]].a;
				}else {
					colormapArr[j * 4 + 3] = static_cast<float>(transparency->GetValue(j));
				}
			}
		}
	}
}

ColorMap::ColorMap() : d { new Impl } {
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4] = 1.0f;
		d->colormapArr[i * 4 + 1] = 1.0f;
		d->colormapArr[i * 4 + 2] = 1.0f;
		d->colormapArr[i * 4 + 3] = 1.0f;
		d->transpArr[i] = 1.0f;
	}
}

ColorMap::~ColorMap() {}

auto ColorMap::ToggleLinearInterpolation(bool isLinear) -> void {
	d->isLinear = isLinear;
	d->RebuildColorMap();
}

auto ColorMap::AddRGBAPoint(double xpos, int x, float r, float g, float b, float a) -> void {
	TFNode node;
	node.xpos = xpos;
	node.r = r;
	node.g = g;
	node.b = b;
	node.a = a;
	d->colormap[x] = node;

	d->RebuildColorMap();
}

auto ColorMap::GetFunctionPos(int idx) -> double {
	auto keys = d->colormap.keys();
	if (keys.count() > idx) {
		std::sort(keys.begin(), keys.end());
		return d->colormap[keys[idx]].xpos;
	}
	return -1.0;
}

auto ColorMap::RemovePoint(int x) -> void {
	if (d->colormap.contains(x)) {
		d->colormap.remove(x);
	}
	d->RebuildColorMap();
}

auto ColorMap::GetRedValue(int x) -> float {
	return d->colormapArr[x * 4];
}

auto ColorMap::GetGreenValue(int x) -> float {
	return d->colormapArr[x * 4 + 1];
}

auto ColorMap::SetColormap(QList<float> colormapArr) -> void {
	Clear();
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4] = colormapArr[i * 4];
		d->colormapArr[i * 4 + 1] = colormapArr[i * 4 + 1];
		d->colormapArr[i * 4 + 2] = colormapArr[i * 4 + 2];
		if (d->useTransp) {
			d->colormapArr[i * 4 + 3] = static_cast<float>(d->transparency->GetValue(i));
		}
		else {
			d->colormapArr[i * 4 + 3] = colormapArr[i * 4 + 3];
		}
		d->transpArr[i] = colormapArr[i * 4 + 3];
	}
}

auto ColorMap::GetBlueValue(int x) -> float {
	return d->colormapArr[x * 4 + 2];
}

auto ColorMap::GetAlphaValue(int x) -> float {
	return d->colormapArr[x * 4 + 3];
}

auto ColorMap::GetColor(int x, float rgba[4]) -> void {
	for (auto i = 0; i < 4; i++) {
		rgba[i] = d->colormapArr[x * 4 + i];
	}
}

auto ColorMap::GetDataPointer() -> float* {
	return d->colormapArr;
}

auto ColorMap::GetSize() -> int {
	return d->colormap.count();
}

auto ColorMap::Clear() -> void {
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4] = 1.0f;
		d->colormapArr[i * 4 + 1] = 1.0f;
		d->colormapArr[i * 4 + 2] = 1.0f;
		d->colormapArr[i * 4 + 3] = 1.0f;
		d->transpArr[i] = 1.0f;
	}
	d->colormap.clear();	
}

auto ColorMap::UpdateTransparency() -> void {
	if(false == d->useTransp) {
		return;
	}
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4 + 3] = static_cast<float>(d->transparency->GetValue(i));
	}
}


auto ColorMap::SetTransparencyMap(const TransparencyMapPtr& transp) -> void {
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4 + 3] = static_cast<float>(transp->GetValue(i));
	}
	
	d->transparency = transp;
	d->useTransp = true;
}

auto ColorMap::RestoreTransparency() -> void {
	d->useTransp = false;
	d->transparency = nullptr;
	for (auto i = 0; i < 256; i++) {
		d->colormapArr[i * 4 + 3] = d->transpArr[i];
	}
}
