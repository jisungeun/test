#include <QFileDialog>
#include <QVBoxLayout>

#include <ImageDev/ImageDev.h>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>


//Volume Scene
#include <VolumeMipMask.h>
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>


#include <OivActivator.h>
#include <TCFMetaReader.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>
#include "TestWindow.h"
#include <RenderWindowVolume.h>

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	QStringList OivKeys;
	OivKeys.append("License OpenInventor 10.12 1-Jan-0 0 HY:PKW=I>@C` \"APP-TOMOANALYSIS\"");
	OivKeys.append("License VolumeVizLDM 10.12 1-Jan-0 0 JDJ]=[<:?LB[ \"APP-TOMOANALYSIS\"");
	QString ImageDevKeys;
	ImageDevKeys.append("INCREMENT ImageDev asglmd 2022.2 permanent uncounted ");
	ImageDevKeys.append("VENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=14-feb-2023 ");
	ImageDevKeys.append("NOTICE=TomoAnalysis SN=23022968-IMDEV START=13-feb-2023 ");
	QStringList ImageDevPW{ "03H6","3B4:","AGH6","EG8I","E6;<","4778","3G67","01GI","D559","4273","6923","BC95","4B58","A15H","865D","6DH;","DDCF","A2C;","EB86","648G","2C59" };
	ImageDevKeys.append("ONE_TS_OK SIGN=\"");
	ImageDevKeys.append(OivActivator::Convert(ImageDevPW));
	ImageDevKeys.append("\"");
	OivActivator::Activate(OivKeys);

	TestWindow* testwindow = new TestWindow;	
	testwindow->setMinimumSize(600, 800);
	testwindow->setWindowTitle("Default Volume Rendering Test");

	SoQt::init(testwindow);
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();
	OivLdmReader::initClass();
	SoTF2D::initClass();
	imagedev::init(ImageDevKeys.toStdString().c_str());

	testwindow->BuildSceneGraph();
		
	SoQt::show(testwindow);
	SoQt::mainLoop();

	delete testwindow;

	imagedev::finish();
	SoTF2D::exitClass();
	OivLdmReader::exitClass();
	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	SoQt::finish();

	return EXIT_SUCCESS;
}