#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

//Volume Scene
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>


#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

//with colormap
#include <ColorMap.h>
#include <QColormapCanvas.h>
#include <VolumeMipMask.h>

//with 2D TF
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>

#include <TCFMetaReader.h>
#include <OivHdf5Reader.h>

#include <RenderWindowVolume.h>

#include "TestWindow.h"
#include "ui_TestWindow.h"

#include "OivLdmReader.h"

using namespace Tomocube::Rendering::Blending;
using namespace imagedev;
using namespace iolink;

struct TestWindow::Impl {
	Ui::Form ui;

	QString path;
	std::shared_ptr<RenderWindow3D> renderwindow { nullptr };

	float offset { 0 };
	float divider { 1 };
	ColorMapPtr tf { nullptr };
	std::shared_ptr<VolumeMipMask> volumeLayer{nullptr};
	QColormapCanvas* colormapWidget { nullptr };
	float color_table[12][3];
	auto BuildColorTable()->void;
};

auto TestWindow::Impl::BuildColorTable() -> void {
	color_table[0][0] = 255; color_table[0][1] = 59; color_table[0][2] = 48; //red
	color_table[1][0] = 255; color_table[1][1] = 149; color_table[1][2] = 0; //Orange
	color_table[2][0] = 255; color_table[2][1] = 204; color_table[2][2] = 0; //yellow
	color_table[3][0] = 52; color_table[3][1] = 199; color_table[3][2] = 89; //Green
	color_table[4][0] = 0; color_table[4][1] = 199; color_table[4][2] = 190; //Mint
	color_table[5][0] = 48; color_table[5][1] = 176; color_table[5][2] = 199; //Teal
	color_table[6][0] = 50; color_table[6][1] = 173; color_table[6][2] = 230; //Cyan
	color_table[7][0] = 0; color_table[7][1] = 122; color_table[7][2] = 255; //Blue
	color_table[8][0] = 88; color_table[8][1] = 86; color_table[8][2] = 214; //Indigo
	color_table[9][0] = 175; color_table[9][1] = 82; color_table[9][2] = 222; //Purple
	color_table[10][0] = 255; color_table[10][1] = 45; color_table[10][2] = 85; //Pink
	color_table[11][0] = 162; color_table[11][1] = 132; color_table[11][2] = 94; //Brown
}


TestWindow::TestWindow(QWidget* parent) : QWidget(parent), d { new Impl } {
	d->ui.setupUi(this);
	d->tf = std::make_shared<ColorMap>();
	InitUI();
	d->BuildColorTable();
}

TestWindow::~TestWindow() { }

auto TestWindow::BuildSceneGraph() -> void {
	d->renderwindow = std::make_shared<RenderWindow3D>(nullptr);
	d->renderwindow->setGradientBackground(SbVec3f(0, 0, 0), SbVec3f(0, 0, 0));

	const auto layout = new QVBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	d->ui.RenderSocket->setLayout(layout);

	layout->addWidget(d->renderwindow.get());

	d->volumeLayer = std::make_shared<VolumeMipMask>("Test");
	d->renderwindow->setSceneGraph(d->volumeLayer->GetRootSwitch());
	this->OnColorMap(0);
}

auto TestWindow::InitUI() -> void {
	d->ui.depthMin->setRange(0, 0.99);
	d->ui.depthMin->setDecimals(2);
	d->ui.depthMin->setSingleStep(0.01);
	d->ui.depthMin->setValue(0);
	d->ui.depthMax->setRange(0.01, 1);
	d->ui.depthMax->setDecimals(2);
	d->ui.depthMax->setSingleStep(0.01);
	d->ui.depthMax->setValue(1);

	d->ui.xcropMin->setRange(0, 0.99);
	d->ui.xcropMin->setDecimals(2);
	d->ui.xcropMin->setSingleStep(0.01);
	d->ui.xcropMin->setValue(0);
	d->ui.xcropMax->setRange(0.01, 1);
	d->ui.xcropMax->setDecimals(2);
	d->ui.xcropMax->setSingleStep(0.01);
	d->ui.xcropMax->setValue(1);

	d->ui.ycropMin->setRange(0, 0.99);
	d->ui.ycropMin->setDecimals(2);
	d->ui.ycropMin->setSingleStep(0.01);
	d->ui.ycropMin->setValue(0);
	d->ui.ycropMax->setRange(0.01, 1);
	d->ui.ycropMax->setDecimals(2);
	d->ui.ycropMax->setSingleStep(0.01);
	d->ui.ycropMax->setValue(1);

	connect(d->ui.depthMin, SIGNAL(valueChanged(double)), this, SLOT(OnDepthMin(double)));
	connect(d->ui.depthMax, SIGNAL(valueChanged(double)), this, SLOT(OnDepthMax(double)));
	connect(d->ui.xcropMin, SIGNAL(valueChanged(double)), this, SLOT(OnXMin(double)));
	connect(d->ui.xcropMax, SIGNAL(valueChanged(double)), this, SLOT(OnXMax(double)));
	connect(d->ui.ycropMin, SIGNAL(valueChanged(double)), this, SLOT(OnYMin(double)));
	connect(d->ui.ycropMax, SIGNAL(valueChanged(double)), this, SLOT(OnYMax(double)));

	d->ui.renderCombo->addItem("Max Intensity Projection");
	//d->ui.comboBox->addItem("Max Intensity Difference Accumulation");
	//d->ui.comboBox->addItem("Direct Volume Rendering");
	d->ui.renderCombo->setCurrentIndex(0);

	connect(d->ui.renderCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRenderMode(int)));

	d->ui.colorCombo->addItem("Standard");
	d->ui.colorCombo->addItem("Temperature");
	d->ui.colorCombo->addItem("Physics");
	d->ui.colorCombo->addItem("Glow");
	d->ui.colorCombo->addItem("Blue_Red");
	d->ui.colorCombo->addItem("Seismic");
	d->ui.colorCombo->addItem("Blue_White_Red");
	d->ui.colorCombo->addItem("RedTone");
	d->ui.colorCombo->addItem("GreenTone");
	d->ui.colorCombo->addItem("Custom");

	connect(d->ui.colorCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColorMap(int)));

	d->ui.minRI->setDecimals(4);
	d->ui.minRI->setSingleStep(0.0001);
	d->ui.maxRI->setDecimals(4);
	d->ui.maxRI->setSingleStep(0.0001);

	d->ui.minRI->setEnabled(false);
	d->ui.maxRI->setEnabled(false);

	connect(d->ui.minRI, SIGNAL(valueChanged(double)), this, SLOT(OnDataRangeMin(double)));
	connect(d->ui.maxRI, SIGNAL(valueChanged(double)), this, SLOT(OnDataRangeMax(double)));

	d->ui.depthFactor->setRange(0, 1);
	d->ui.depthFactor->setDecimals(2);
	d->ui.depthFactor->setValue(0.5);
	d->ui.depthFactor->setSingleStep(0.01);
	d->ui.depthFactor->setEnabled(false);

	connect(d->ui.depthFactor, SIGNAL(valueChanged(double)), this, SLOT(OnDepthFactor(double)));

	d->ui.gradFactor->setRange(0, 1);
	d->ui.gradFactor->setDecimals(2);
	d->ui.gradFactor->setValue(0.5);
	d->ui.gradFactor->setSingleStep(0.01);
	d->ui.gradFactor->setEnabled(false);

	connect(d->ui.gradFactor, SIGNAL(valueChanged(double)), this, SLOT(OnGradFactor(double)));

	connect(d->ui.gradientChk, SIGNAL(clicked()), this, SLOT(OnToggleGradient()));
	connect(d->ui.depthChk, SIGNAL(clicked()), this, SLOT(OnToggleDepth()));
	connect(d->ui.depColChk, SIGNAL(clicked()), this, SLOT(OnToggleDepthColor()));
	connect(d->ui.viewChk, SIGNAL(clicked()), this, SLOT(OnToggleViewDir()));

	connect(d->ui.openBtn, SIGNAL(clicked()), this, SLOT(OnOpenTCF()));
	connect(d->ui.resetBtn, SIGNAL(clicked()), this, SLOT(OnResetView()));

	const auto clayout = new QVBoxLayout;
	clayout->setContentsMargins(0, 0, 0, 0);
	clayout->setSpacing(0);
	d->ui.colormapSocket->setLayout(clayout);

	d->colormapWidget = new QColormapCanvas;
	clayout->addWidget(d->colormapWidget);
	d->colormapWidget->SetColorTransferFunction(d->tf);

	connect(d->colormapWidget, SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged()));
	d->colormapWidget->setEnabled(false);

	connect(d->ui.maskChk, SIGNAL(clicked()), this, SLOT(OnToggleMask()));;
	connect(d->ui.maskColChk, SIGNAL(clicked()), this, SLOT(OnToggleMaskColor()));
	connect(d->ui.maskSpin, SIGNAL(valueChanged(int)), this, SLOT(OnHighlightMask(int)));
}

void TestWindow::OnToggleMask() {
	d->volumeLayer->ToggleUseMask(d->ui.maskChk->isChecked());
}

void TestWindow::OnToggleMaskColor() {
	d->volumeLayer->ToggleMaskColor(d->ui.maskColChk->isChecked());
}

void TestWindow::OnHighlightMask(int idx) {
	d->volumeLayer->SetHighlight(idx);
}

void TestWindow::OnCustomColormapChanged() {
	d->volumeLayer->SetColorMapArr(d->tf->GetDataPointer());
}

void TestWindow::OnResetView() {
	d->renderwindow->viewZ();
}

void TestWindow::OnOpenTCF() {
	const auto path = QString(qApp->applicationDirPath()+"/220718.103243.K562.016.Group1.A1.S016.TCF");

	auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
	auto metaInfo = metaReader->Read(path);

	if (false == metaInfo->data.data3D.exist) {
		QMessageBox::warning(nullptr, "Warning", QString("%1\nhas no HT 3D modality!").arg(path));
		return;
	}

	SoRef<SoVolumeData> volData = new SoVolumeData;
	d->offset = 0;
	d->divider = 1;
	if (metaInfo->data.isLDM) {
		SoRef<OivLdmReader> reader = new OivLdmReader;
		reader->setDataGroupPath("/Data/3D");
		reader->setTileName("000000");
		reader->setTileDimension(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
		reader->setFilename(path.toStdString());
		volData->setReader(*reader, TRUE);

		if (metaInfo->data.data3D.scalarType[0] == 1) {
			d->offset = metaInfo->data.data3D.riMinList[0] * 10000.0;
			d->divider = 10;
		}

		d->volumeLayer->SetDataMinMax((metaInfo->data.data3D.riMinList[0] * 10000.0 - d->offset) / d->divider,
									(metaInfo->data.data3D.riMaxList[0] * 10000.0 - d->offset) / d->divider);
	} else {
		SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
		reader->setDataGroupPath("/Data/3D");
		reader->setTileName("000000");
		reader->setFilename(path.toStdString());
		volData->setReader(*reader, TRUE);
	}

	d->ui.minRI->blockSignals(true);
	d->ui.minRI->setEnabled(true);
	d->ui.minRI->setValue(metaInfo->data.data3D.riMin);
	d->ui.minRI->blockSignals(false);

	d->ui.maxRI->setEnabled(true);
	d->ui.maxRI->setEnabled(true);
	d->ui.maxRI->setValue(metaInfo->data.data3D.riMax);
	d->ui.maxRI->blockSignals(false);

	d->volumeLayer->SetVolume(volData.ptr());
	try{
		//Create Dummy Mask
		const auto dim = volData->getDimension();
		auto resolution = volData->getVoxelSize();
		auto x = dim[0];
		auto y = dim[1];
		auto z = dim[2];

		const auto bBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(x-1, y-1, z-1));
		SoLDMDataAccess::DataInfoBox dataInfoBox = volData->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
		dataBufferObj->setSize(static_cast<size_t>(dataInfoBox.bufferSize));
		volData->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
		const auto buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

		auto maskBuffer = new uint16_t[x * y * z];
		memcpy(maskBuffer, buffer, sizeof(uint16_t) * x * y * z);
		dataBufferObj->unmap();

		auto spatialCalibrationProperty = SpatialCalibrationProperty(
			{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
			{ resolution[0], resolution[1], resolution[2] }	// spacing
		);
		std::shared_ptr<iolink::ImageView> imageView{ nullptr };
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape{ static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		const RegionXu64 imageRegion{ {0, 0, 0}, imageShape };
		imageView->writeRegion(imageRegion, maskBuffer);
				
		const auto floatImage = convertImage(imageView, ConvertImage::FLOAT_32_BIT);

		const auto filteredImage = gaussianFilter3d(floatImage, GaussianFilter3d::SEPARABLE, { 3, 3, 3 }, 2.0, GaussianFilter3d::OutputType::SAME_AS_INPUT, false);

		const auto ushortImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

		auto thresholded = thresholding(ushortImage, { 13405,INT_MAX });

		auto eroded = erosion3d(thresholded, 9, Erosion3d::CONNECTIVITY_26);

		auto labeled = labeling3d(eroded, Labeling3d::LABEL_16_BIT, Labeling3d::CONNECTIVITY_18);

		const auto objectSize = 10 / (resolution[0] * resolution[1] * resolution[2]);

		auto sizeFiltered = removeSmallSpots(labeled, objectSize);

		auto filteredBinary = removeSmallSpots(thresholded, objectSize);

		auto distanceMap = distanceMap3d(filteredBinary, DistanceMap3d::MappingMode::INSIDE, DistanceMap3d::BorderCondition::ZERO, 1, 1.4142135623730951, 1.7320508075688772, DistanceMap3d::OutputType::FLOAT_32_BIT);

		distanceMap = arithmeticOperationWithValue(distanceMap, -1, ArithmeticOperationWithValue::MULTIPLY);

		const auto watersheded = markerBasedWatershed3d(distanceMap, sizeFiltered, MarkerBasedWatershed3d::FAST, MarkerBasedWatershed3d::OutputType::CONTIGUOUS_REGIONS, MarkerBasedWatershed3d::CONNECTIVITY_26);

		const auto binaryInput = convertImage(filteredBinary, ConvertImage::BINARY);
		const auto result = maskImage(watersheded, binaryInput);

		SoRef<SoVolumeData> maskVol = new SoVolumeData;
		maskVol->data.setValue(dim, SbDataType::UNSIGNED_SHORT, 16, result->bufferReadOnly(), SoSFArray::COPY);
		maskVol->extent.setValue(volData->extent.getValue());

		double mmin, mmax;
		maskVol->getMinMax(mmin, mmax);

		d->ui.maskSpin->blockSignals(true);
		d->ui.maskSpin->setRange(0, static_cast<int>(mmax));
		d->ui.maskSpin->blockSignals(false);

		d->volumeLayer->SetMaskVolume(maskVol.ptr());
		for(auto i=0;i<static_cast<int>(mmax);i++) {
			d->volumeLayer->SetMaskColor(i + 1, QColor(d->color_table[i % 12][0], d->color_table[i % 12][1], d->color_table[i % 12][2]));
		}		
	}catch(Exception& e) {
		std::cout << e.what() << std::endl;
	}

	d->renderwindow->viewZ();
}

void TestWindow::OnToggleDepth() {
	const auto isDepth = d->ui.depthChk->isChecked();
	d->volumeLayer->ToggleDepthEnhanced(isDepth);
	d->ui.depthFactor->setEnabled(isDepth);
}

void TestWindow::OnToggleDepthColor() {
	const auto isDepthColor = d->ui.depColChk->isChecked();
	d->volumeLayer->ToggleColorDepth(isDepthColor);
}

void TestWindow::OnToggleViewDir() {
	const auto isViewDir = d->ui.viewChk->isChecked();
	d->volumeLayer->ToggleViewDir(isViewDir);
}

void TestWindow::OnToggleGradient() {
	const auto isGrad = d->ui.gradientChk->isChecked();
	d->volumeLayer->ToggleGradientEnhanced(isGrad);
	d->ui.gradFactor->setEnabled(isGrad);
}

void TestWindow::OnColorMap(int idx) {
	if (idx == 9) {
		d->volumeLayer->SetColorMapArr(d->tf->GetDataPointer());
		d->colormapWidget->SetColorTransferFunction(d->tf);
		d->colormapWidget->setEnabled(true);
	} else {
		d->colormapWidget->setEnabled(false);
		if (idx == 0) {//Standard
			d->volumeLayer->SetColorMap(DepthColorMap::STANDARD);
		} else if (idx == 1) {
			d->volumeLayer->SetColorMap(DepthColorMap::TEMPERATURE);
		} else if (idx == 2) {
			d->volumeLayer->SetColorMap(DepthColorMap::PHYSICS);
		} else if (idx == 3) {
			d->volumeLayer->SetColorMap(DepthColorMap::GLOW);
		} else if (idx == 4) {
			d->volumeLayer->SetColorMap(DepthColorMap::BLUE_RED);
		} else if (idx == 5) {
			d->volumeLayer->SetColorMap(DepthColorMap::SEISMIC);
		} else if (idx == 6) {
			d->volumeLayer->SetColorMap(DepthColorMap::BLUE_WHITE_RED);
		} else if (idx == 7) {
			d->volumeLayer->SetColorMap(DepthColorMap::REDTONE);
		} else if (idx == 8) {
			d->volumeLayer->SetColorMap(DepthColorMap::GREENTONE);
		}
		auto tempTF = std::make_shared<ColorMap>();
		const auto predColorArr = d->volumeLayer->GetColorMapArr();
		for (auto i = 0; i < 256; i++) {
			tempTF->AddRGBAPoint(static_cast<double>(i / 255.0), i, predColorArr[i * 4], predColorArr[i * 4 + 1], predColorArr[i * 4 + 2], predColorArr[i * 4 + 3]);
		}
		d->colormapWidget->SetColorTransferFunction(tempTF);
	}
}


void TestWindow::OnRenderMode(int idx) {
	
}

void TestWindow::OnGradFactor(double value) {
	d->volumeLayer->SetGradientEnhancementFactor(static_cast<float>(value));
}

void TestWindow::OnDepthFactor(double value) {
	d->volumeLayer->SetDepthEnhancementFactor(static_cast<float>(value));
}

void TestWindow::OnDataRangeMax(double value) {
	Q_UNUSED(value)
	const auto min = d->ui.minRI->value() * 10000.0;
	const auto max = d->ui.maxRI->value() * 10000.0;

	d->volumeLayer->SetDataRange((min - d->offset) / d->divider, (max - d->offset) / d->divider);
}

void TestWindow::OnDataRangeMin(double value) {
	Q_UNUSED(value)
	const auto min = d->ui.minRI->value() * 10000.0;
	const auto max = d->ui.maxRI->value() * 10000.0;

	d->volumeLayer->SetDataRange((min - d->offset) / d->divider, (max - d->offset) / d->divider);
}

void TestWindow::OnDepthMin(double value) {
	const auto min = d->ui.depthMin->value();
	const auto max = d->ui.depthMax->value();

	d->volumeLayer->SetZRange(min, max);
}

void TestWindow::OnDepthMax(double value) {
	const auto min = d->ui.depthMin->value();
	const auto max = d->ui.depthMax->value();

	d->volumeLayer->SetZRange(min, max);
}

void TestWindow::OnXMin(double value) {
	const auto min = d->ui.xcropMin->value();
	const auto max = d->ui.xcropMax->value();

	d->volumeLayer->SetXRange(min, max);
}

void TestWindow::OnXMax(double value) {
	const auto min = d->ui.xcropMin->value();
	const auto max = d->ui.xcropMax->value();

	d->volumeLayer->SetXRange(min, max);
}

void TestWindow::OnYMin(double value) {
	const auto min = d->ui.ycropMin->value();
	const auto max = d->ui.ycropMax->value();

	d->volumeLayer->SetYRange(min, max);
}

void TestWindow::OnYMax(double value) {
	const auto min = d->ui.ycropMin->value();
	const auto max = d->ui.ycropMax->value();

	d->volumeLayer->SetYRange(min, max);
}
