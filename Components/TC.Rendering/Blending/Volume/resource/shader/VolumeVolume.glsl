//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

////////////////////////////////////////////////////////////////////////
// Simple multi-channel fragment shader for volume rendering
//
// Notes
// 1) This version is for volume rendering.
//    (Must make a separate version for slice/skin rendering.)
// 2) This version is for 3 channels of data.
//    (Must make a separate version for 2 channels.)
// 3) This shader assumes that 3D textures are used.
// 4) It seems reasonable to sum the color (RGB) components.  But we
//    don't necessarily want the volume to be more opaque where
//    values exist in multiple channels, so we'll use the highest
//    opacity value in any of the channels.  Does that make sense?

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;

uniform int isData1Exist;
uniform int isData2Exist;
uniform int isDepthEnhanced;
uniform int isGradientEnhanced;
uniform float depthEnhanceFactor;
uniform float gradientEnhanceFactor;
uniform float upperBound;
uniform float lowerBound;
uniform float xMinBound;
uniform float xMaxBound;
uniform float yMinBound;
uniform float yMaxBound;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);			
	vec4 clrHT2 = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 result = vec4(0.0);

	vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);


	float x = dataCoord1[0];
	float y = dataCoord1[1];
	float depth = dataCoord1[2];
	float dep = depth * depthEnhanceFactor;

	if (depth < lowerBound || depth > upperBound) {
		clrHT = vec4(0);
		return clrHT;
	}

	if (x < xMinBound || x > xMaxBound) {
		clrHT = vec4(0);
		return clrHT;
	}

	if (y < yMinBound || y > yMaxBound) {
		clrHT = vec4(0);
		return clrHT;
	}

	if (isData1Exist > 0) {
		VVIZ_DATATYPE index1 = VVizGetData(data1, dataCoord1);		
		clrHT = VVizTransferFunction(index1, 1);

		if (isDepthEnhanced > 0) {			
			float r = clrHT.r * (1 + dep);
			float g = clrHT.g * (1 + dep);
			float b = clrHT.b * (1 + dep);
			float a = clrHT.a * (1 + dep);

			clrHT.r = min(r, 1);
			clrHT.g = min(g, 1);
			clrHT.b = min(b, 1);
			clrHT.a = min(a, 1);
		}
		if (isGradientEnhanced > 0) {
			vec3 normal, gradient;
			VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);
			float gradientValue = length(gradient);
			clrHT.r = mix(clrHT.r, gradientValue, gradientEnhanceFactor);
			clrHT.g = mix(clrHT.g, gradientValue, gradientEnhanceFactor);
			clrHT.b = mix(clrHT.b, gradientValue, gradientEnhanceFactor);
			clrHT.a = mix(clrHT.a, gradientValue, gradientEnhanceFactor);
		}
	}
	if (isData2Exist > 0) {
		VVIZ_DATATYPE index2 = VVizGetData(data2, dataCoord1);
		clrHT2 = VVizTransferFunction(index2, 2);
		if (isDepthEnhanced > 0) {
			float r2 = clrHT2.r * (1 + dep);
			float g2 = clrHT2.g * (1 + dep);
			float b2 = clrHT2.b * (1 + dep);
			float a2 = clrHT2.a * (1 + dep);

			clrHT2.r = min(r2, 1);
			clrHT2.g = min(g2, 1);
			clrHT2.b = min(b2, 1);
			clrHT2.a = min(a2, 1);
		}
		if (isGradientEnhanced > 0) {			
			vec3 normal2, gradient2;
			VVizComputeGradientCommon(data2, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient2, normal2);
			float gradientValue2 = length(gradient2);
			clrHT2.r = mix(clrHT2.r, gradientValue2, gradientEnhanceFactor);
			clrHT2.g = mix(clrHT2.g, gradientValue2, gradientEnhanceFactor);
			clrHT2.b = mix(clrHT2.b, gradientValue2, gradientEnhanceFactor);
			clrHT2.a = mix(clrHT2.a, gradientValue2, gradientEnhanceFactor);
		}
	}
	result.r = max(clrHT.r, clrHT2.r);
	result.g = max(clrHT.g, clrHT2.g);
	result.b = max(clrHT.b, clrHT2.b);
	result.a = max(clrHT.a, clrHT2.a);

	return result;	
}
