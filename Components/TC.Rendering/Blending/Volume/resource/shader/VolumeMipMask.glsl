//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

////////////////////////////////////////////////////////////////////////
// Simple multi-channel fragment shader for volume rendering
//
// Notes
// 1) This version is for volume rendering.
//    (Must make a separate version for slice/skin rendering.)
// 2) This version is for 3 channels of data.
//    (Must make a separate version for 2 channels.)
// 3) This shader assumes that 3D textures are used.
// 4) It seems reasonable to sum the color (RGB) components.  But we
//    don't necessarily want the volume to be more opaque where
//    values exist in multiple channels, so we'll use the highest
//    opacity value in any of the channels.  Does that make sense?

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;

uniform int isHTExist;
uniform int isDepthEnhanced;
uniform int isGradientEnhanced;
uniform int isColorDepth;
uniform int isViewDir;
uniform int isMaskBlend;
uniform int useMask;
uniform float depthEnhanceFactor;
uniform float gradientEnhanceFactor;
uniform float upperBound;
uniform float lowerBound;
uniform float xMinBound;
uniform float xMaxBound;
uniform float yMinBound;
uniform float yMaxBound;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);			

	if (isHTExist > 0) {
		vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);		
		VVIZ_DATATYPE index1 = VVizGetData(data1, dataCoord1);		
		
		clrHT = VVizTransferFunction(index1, 1);				
		//clrHT = mix(clrHT, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));
				
		float x = dataCoord1[0];
		float y = dataCoord1[1];
		float depth = dataCoord1[2];				

		if (depth < lowerBound || depth > upperBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (x < xMinBound || x > xMaxBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (y < yMinBound || y > yMaxBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (isViewDir > 0) {
			depth = VVizGetProjectedDepth(dataCoord1);
		}

		if (isDepthEnhanced > 0) {
			float dep = depth * depthEnhanceFactor;
			float r = clrHT.r * (1 + dep);
			float g = clrHT.g * (1 + dep);
			float b = clrHT.b * (1 + dep);
			float a = clrHT.a * (1 + dep);

			clrHT.r = min(r, 1);
			clrHT.g = min(g, 1);
			clrHT.b = min(b, 1);
			clrHT.a = min(a, 1);			
		}
		if (isGradientEnhanced > 0) {
			vec3 normal, gradient;
			VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);
			float gradientValue = length(gradient);
			clrHT.r = mix(clrHT.r, gradientValue, gradientEnhanceFactor);
			clrHT.g = mix(clrHT.g, gradientValue, gradientEnhanceFactor);
			clrHT.b = mix(clrHT.b, gradientValue, gradientEnhanceFactor);
			clrHT.a = mix(clrHT.a, gradientValue, gradientEnhanceFactor);
		}
		vec4 maskClr = vec4(0.0);
		if (useMask > 0) {
			vec3 maskCoord = VVizTextureToTextureVec(data, data2, texCoord);
			VVIZ_DATATYPE maskIdx = VVizGetData(data2, maskCoord);
			maskClr = VVizTransferFunction(maskIdx, 2);
			clrHT.a = clrHT.a * maskClr.a;
		}

		if (isMaskBlend + useMask > 1) {
			float remapDepth = (depth - lowerBound) / (upperBound - lowerBound);
			clrHT.r = clrHT.r * maskClr.r;
			clrHT.g = clrHT.g * maskClr.g;
			clrHT.b = clrHT.b * maskClr.b;
		}
		else if (isColorDepth > 0) {
			float remapDepth = (depth - lowerBound) / (upperBound - lowerBound);			
			vec4 dcol = VVizTransferFunction(remapDepth, 3);

			clrHT.r = clrHT.r * dcol.r;
			clrHT.g = clrHT.g * dcol.g;
			clrHT.b = clrHT.b * dcol.b;
		}		
	}

	return clrHT;	
}
