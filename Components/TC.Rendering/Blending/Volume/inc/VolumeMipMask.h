#pragma once

#include <memory>
#include <enum.h>
#include <QColor>

#include <IImageGeneral.h>
#include <IImageSingle.h>

#include "TC.Rendering.Blending.VolumeExport.h"

namespace Tomocube::Rendering::Blending {
	using namespace Image;

	class TC_Rendering_Blending_Volume_API VolumeMipMask : public IImageGeneral, public IImageSingle {
	public:
		VolumeMipMask(const QString& name);
		~VolumeMipMask();

		auto SetDataRange(double lower, double upper) -> void override;
		auto SetDataMinMax(double min, double max) -> void override;
		auto SetVolume(SoVolumeData* vol) -> void override;
		auto ToggleGamma(bool isGamma) -> void override;
		auto SetGamma(float gamma) -> void override;
		auto ToggleViz(bool show) -> void override;

		auto SetMaskVolume(SoVolumeData* vol) -> void;
		auto GetLabelCount() const -> int;

		////////////////MASK//////////////
		auto SetMaskColor(int labelIdx, QColor col) -> void;
		auto GetMaskColor(int labelIdx) const -> QColor;
		auto ToggleUseMask(bool isUseMask) -> void;
		auto ToggleMaskColor(bool isUseMaskColor) -> void;
		auto SetHighlight(int labelIdx) -> void;

		////////////////MIP//////////////
		auto SetZRange(double min, double max) -> void;
		auto SetXRange(double min, double max) -> void;
		auto SetYRange(double min, double max) -> void;
		auto Clear() -> void;

		auto SetColorMap(DepthColorMap idx) -> void;
		auto SetColorMapArr(float* colormapArr) -> void;
		auto SetColorMapArr(QList<float> colormapArr) -> void;
		auto GetColorMapArr() -> float*;

		auto ToggleDepthEnhanced(bool isDepthEnhanced) -> void;
		auto SetDepthEnhancementFactor(float factor) -> void;
		auto ToggleGradientEnhanced(bool isGradientEnhanced) -> void;
		auto SetGradientEnhancementFactor(float factor) -> void;
		auto ToggleColorDepth(bool isColor) -> void;
		auto ToggleViewDir(bool isViewDir) -> void;

		auto ToggleJittering(bool use)->void;
		auto ToggleDeferredLighting(bool use)->void;

	private:
		auto BuildSceneGraph() -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
