#pragma once

#include <memory>
#include <enum.h>
#include <QColor>

#include <IImageGeneral.h>
#include <IImageSingle.h>

#include "TC.Rendering.Blending.VolumeExport.h"

namespace Tomocube::Rendering::Blending {
	using namespace Image;

	class TC_Rendering_Blending_Volume_API VolumeVolume : public IImageGeneral, public IImageSingle {
	public:
		VolumeVolume(const QString& name);
		~VolumeVolume();

		auto SetDataRange(double lower, double upper) -> void override;
		auto SetDataMinMax(double min, double max) -> void override;
		auto SetVolume(SoVolumeData* vol) -> void override;
		auto SetColormap(QList<float> colormapArr)->void;
		
		auto SetDataRange2(double lower, double upper) -> void;
		auto SetDataMinMax2(double min, double max) -> void;
		auto SetVolume2(SoVolumeData* vol) -> void;
		auto SetColormap2(QList<float> colormapArr)->void;
		
		auto ToggleViz(bool show) -> void override;
		auto ToggleViz2(bool show) -> void;
		auto ToggleGamma(bool isGamma) -> void override;
		auto SetGamma(float gamma) -> void override;

		////////////////MIP//////////////
		auto SetZRange(double min, double max) -> void;
		auto SetXRange(double min, double max) -> void;
		auto SetYRange(double min, double max) -> void;
		auto Clear() -> void;
				
		auto ToggleDepthEnhanced(bool isDepthEnhanced) -> void;
		auto SetDepthEnhancementFactor(float factor) -> void;
		auto ToggleGradientEnhanced(bool isGradientEnhanced) -> void;
		auto SetGradientEnhancementFactor(float factor) -> void;

		auto ToggleMIP(bool use)->void;
		auto ToggleJittering(bool use)->void;
		auto ToggleDeferredLighting(bool use)->void;

	private:
		auto BuildSceneGraph() -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
