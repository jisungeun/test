#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <Inventor/nodes/SoTransformProjection.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "VolumeVolume.h"

namespace Tomocube::Rendering::Blending {
	struct VolumeVolume::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		SoRef<SoSeparator> root { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoMultiDataSeparator> mds { nullptr };
		SoRef<SoDataRange> dataRange { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };
		SoRef<SoSwitch> volumeSocket { nullptr };
		SoRef<SoFragmentShader> fragmentShader { nullptr };
		SoRef<SoVolumeRenderingQuality> shader { nullptr };
		SoRef<SoVolumeRender> volumeRender { nullptr };

		SoRef<SoDataRange> dataRange2 { nullptr };
		double data2Min { -1 };
		double data2Max { -1 };
		double data2Lower { -1 };
		double data2Upper { -1 };
		SoRef<SoTransferFunction> transFunc2 { nullptr };
		SoRef<SoSwitch> volumeSocket2 { nullptr };

		double depthMin { 0 };
		double depthMax { 1 };
		double xMinBound { 0 };
		double xMaxBound { 1 };
		double yMinBound { 0 };
		double yMaxBound { 1 };

		QString shaderPath;
	};

	VolumeVolume::VolumeVolume(const QString& name) : IImageGeneral(), IImageSingle(), d { new Impl } {
		general_d->name = name;
		d->shaderPath = QString("%1/shader/VolumeVolume.glsl").arg(qApp->applicationDirPath());
		BuildSceneGraph();
	}

	VolumeVolume::~VolumeVolume() { }

	auto VolumeVolume::SetGamma(float gamma) -> void {
		Q_UNUSED(gamma)
	}

	auto VolumeVolume::ToggleGamma(bool isGamma) -> void {
		Q_UNUSED(isGamma)
	}

	auto VolumeVolume::SetZRange(double min, double max) -> void {
		d->depthMin = min;
		d->depthMax = max;
		d->fragmentShader->setShaderParameter1f("lowerBound", min);
		d->fragmentShader->setShaderParameter1f("upperBound", max);
	}

	auto VolumeVolume::SetXRange(double min, double max) -> void {
		d->xMinBound = min;
		d->xMaxBound = max;
		d->fragmentShader->setShaderParameter1f("xMinBound", min);
		d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}

	auto VolumeVolume::SetYRange(double min, double max) -> void {
		d->yMaxBound = min;
		d->yMaxBound = max;
		d->fragmentShader->setShaderParameter1f("yMinBound", min);
		d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}

	auto VolumeVolume::Clear() -> void {
		single_d.reset();
		d->dataRange->min = -1;
		d->dataRange->max = -1;
		d->volumeSocket->replaceChild(0, new SoSeparator);
		d->depthMin = 0;
		d->depthMax = 1;
	}

	auto VolumeVolume::SetDataMinMax(double min, double max) -> void {
		single_d->min = min;
		single_d->max = max;
		single_d->lower = min;
		single_d->lower = max;
		d->dataRange->min = min;
		d->dataRange->max = max;
	}

	auto VolumeVolume::SetDataMinMax2(double min, double max) -> void {
		d->data2Min = min;
		d->data2Max = max;
		d->data2Lower = min;
		d->data2Upper = max;
		d->dataRange2->min = min;
		d->dataRange2->max = max;
	}


	auto VolumeVolume::SetDataRange(double lower, double upper) -> void {
		single_d->lower = lower;
		single_d->upper = upper;
		d->dataRange->min = lower;
		d->dataRange->max = upper;
	}

	auto VolumeVolume::SetDataRange2(double lower, double upper) -> void {
		d->dataRange2->min = lower;
		d->dataRange2->max = upper;
		d->data2Lower = lower;
		d->data2Upper = lower;
	}


	auto VolumeVolume::ToggleViz(bool show) -> void {
		if (show && single_d->hasData) {
			d->fragmentShader->setShaderParameter1i("isData1Exist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isData1Exist", 0);
		}
	}

	auto VolumeVolume::ToggleViz2(bool show) -> void {
		if (show && single_d->hasData) {
			d->fragmentShader->setShaderParameter1i("isData2Exist", 1);
		}
		else {
			d->fragmentShader->setShaderParameter1i("isData2Exist", 0);
		}
	}

	auto VolumeVolume::ToggleDeferredLighting(bool use) -> void {
		d->shader->deferredLighting = use;
	}

	auto VolumeVolume::ToggleJittering(bool use) -> void {
		d->shader->jittering = use;
	}

	auto VolumeVolume::SetVolume(SoVolumeData* vol) -> void {
		if (single_d->min < 0 && single_d->max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			single_d->min = data_min;
			single_d->max = data_max;
			single_d->lower = data_min;
			single_d->upper = data_max;

			d->dataRange->min = data_min + (data_max - data_min) / 5;
			d->dataRange->max = data_max - (data_max - data_min) / 5;
		}
		vol->dataSetId = 1;
		d->volumeSocket->replaceChild(0, vol);
		single_d->hasData = true;
		d->fragmentShader->setShaderParameter1i("isData1Exist", 1);
	}

	auto VolumeVolume::SetVolume2(SoVolumeData* vol) -> void {
		if (d->data2Min < 0 && d->data2Max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			d->data2Min = data_min;
			d->data2Max = data_max;
			d->data2Lower = data_min;
			d->data2Upper = data_max;

			d->dataRange2->min = data_min + (data_max - data_min) / 5;
			d->dataRange2->max = data_max - (data_max - data_min) / 5;
		}
		vol->dataSetId = 2;
		d->volumeSocket2->replaceChild(0, vol);
		d->fragmentShader->setShaderParameter1i("isData2Exist", 1);
	}


	auto VolumeVolume::BuildSceneGraph() -> void {
		general_d->rootSwitch = new SoSwitch;
		general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
		general_d->rootSwitch->whichChild = 0;

		d->root = new SoSeparator;
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		general_d->rootSwitch->addChild(d->root.ptr());

		d->matl = new SoMaterial;
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->matl->ambientColor.setValue(1, 1, 1);
		d->matl->diffuseColor.setValue(1, 1, 1);

		d->mds = new SoMultiDataSeparator;
		d->mds->setName((general_d->name = "_Mds").toStdString().c_str());

		d->dataRange = new SoDataRange;
		d->dataRange->dataRangeId = 1;
		d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

		d->dataRange2 = new SoDataRange;
		d->dataRange2->dataRangeId = 2;
		d->dataRange2->setName((general_d->name + "_DataRange2").toStdString().c_str());

		d->volumeSocket = new SoSwitch;
		d->volumeSocket->whichChild = 0;
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->volumeSocket->addChild(new SoSeparator);

		d->volumeSocket2 = new SoSwitch;
		d->volumeSocket2->whichChild = 0;
		d->volumeSocket2->setName((general_d->name + "_Socket2").toStdString().c_str());
		d->volumeSocket2->addChild(new SoSeparator);

		d->transFunc = new SoTransferFunction;
		d->transFunc->transferFunctionId = 1;
		d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
		d->transFunc->colorMap.setNum(256 * 4);
		auto p = d->transFunc->colorMap.startEditing();
		const auto steps = d->transFunc->colorMap.getNum() / 4;
		for (auto i = 0; i < steps; i++) {
			const auto val = static_cast<float>(i) / static_cast<float>(steps);
			*p++ = val;
			*p++ = val;
			*p++ = val;
			*p++ = val;
		}
		d->transFunc->colorMap.finishEditing();

		d->transFunc2 = new SoTransferFunction;
		d->transFunc2->transferFunctionId = 2;
		d->transFunc2->setName((general_d->name + "_TF2").toStdString().c_str());
		d->transFunc2->colorMap.setNum(256 * 4);
		auto p2 = d->transFunc2->colorMap.startEditing();
		const auto steps2 = d->transFunc2->colorMap.getNum() / 4;
		for (auto i = 0; i < steps2; i++) {
			const auto val = static_cast<float>(i) / static_cast<float>(steps);
			*p2++ = val;
			*p2++ = val;
			*p2++ = val;
			*p2++ = val;
		}
		d->transFunc2->colorMap.finishEditing();

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("data2", 2);
		d->fragmentShader->addShaderParameter1i("isData1Exist", 0);
		d->fragmentShader->addShaderParameter1i("isData2Exist", 0);
		d->fragmentShader->addShaderParameter1i("isDepthEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("depthEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1i("isGradientEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("gradientEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1f("lowerBound", 0);
		d->fragmentShader->addShaderParameter1f("upperBound", 1);
		d->fragmentShader->addShaderParameter1f("xMinBound", 0);
		d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
		d->fragmentShader->addShaderParameter1f("yMinBound", 0);
		d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

		d->shader = new SoVolumeRenderingQuality;
		d->shader->lighting = TRUE;
		d->shader->jittering = TRUE;
		d->shader->deferredLighting = FALSE;
		d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
		d->shader->forVolumeOnly = TRUE;
		d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

		d->volumeRender = new SoVolumeRender;
		d->volumeRender->setName((general_d->name + "_Render").toStdString().c_str());
		d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		d->volumeRender->interpolation = SoVolumeRender::Interpolation::LINEAR;
		d->root->addChild(d->matl.ptr());
		d->root->addChild(d->mds.ptr());
		d->mds->addChild(d->dataRange.ptr());
		d->mds->addChild(d->volumeSocket.ptr());
		d->mds->addChild(d->transFunc.ptr());
		d->mds->addChild(d->dataRange2.ptr());
		d->mds->addChild(d->volumeSocket2.ptr());
		d->mds->addChild(d->transFunc2.ptr());
		d->mds->addChild(d->shader.ptr());

		d->mds->addChild(d->volumeRender.ptr());
	}

	auto VolumeVolume::ToggleDepthEnhanced(bool isDepthEnhanced) -> void {
		if (isDepthEnhanced) {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 0);
		}
	}

	auto VolumeVolume::SetDepthEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("depthEnhanceFactor", factor);
	}

	auto VolumeVolume::ToggleGradientEnhanced(bool isGradientEnhanced) -> void {
		if (isGradientEnhanced) {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 0);
		}
	}

	auto VolumeVolume::SetGradientEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("gradientEnhanceFactor", factor);
	}

	auto VolumeVolume::ToggleMIP(bool use) -> void {
		if (use) {
			d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		}else {
			d->volumeRender->renderMode = SoVolumeRender::RenderMode::VOLUME_RENDERING;
		}
	}

	auto VolumeVolume::SetColormap(QList<float> colormapArr) -> void {
		auto actualGamma = single_d->gamma;
		if (false == single_d->isGamma) {
			actualGamma = 1;
		}
		d->transFunc->actualColorMap.setNum(256 * 4);
		auto p = d->transFunc->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			auto r = colormapArr[i * 4];
			auto g = colormapArr[i * 4 + 1];
			auto b = colormapArr[i * 4 + 2];
			auto a = colormapArr[i * 4 + 3];
			float mod_r = pow(r, 1.0 / actualGamma);
			float mod_g = pow(g, 1.0 / actualGamma);
			float mod_b = pow(b, 1.0 / actualGamma);
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = a;
		}
		d->transFunc->actualColorMap.finishEditing();
		d->transFunc->touch();
	}

	auto VolumeVolume::SetColormap2(QList<float> colormapArr) -> void {
		auto actualGamma = single_d->gamma;
		if (false == single_d->isGamma) {
			actualGamma = 1;
		}
		d->transFunc->actualColorMap.setNum(256 * 4);
		auto p = d->transFunc2->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			auto r = colormapArr[i * 4];
			auto g = colormapArr[i * 4 + 1];
			auto b = colormapArr[i * 4 + 2];
			auto a = colormapArr[i * 4 + 3];
			float mod_r = pow(r, 1.0 / actualGamma);
			float mod_g = pow(g, 1.0 / actualGamma);
			float mod_b = pow(b, 1.0 / actualGamma);
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = a;
		}
		d->transFunc2->actualColorMap.finishEditing();
		d->transFunc2->touch();
	}
}
