#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <Inventor/nodes/SoTransformProjection.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "VolumeMipMask.h"

namespace Tomocube::Rendering::Blending {
	struct VolumeMipMask::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		SoRef<SoSeparator> root { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoMultiDataSeparator> mds { nullptr };
		SoRef<SoDataRange> dataRange { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };
		SoRef<SoTransferFunction> depthTF { nullptr };
		SoRef<SoSwitch> volumeSocket { nullptr };
		SoRef<SoFragmentShader> fragmentShader { nullptr };
		SoRef<SoVolumeRenderingQuality> shader { nullptr };
		SoRef<SoVolumeRender> volumeRender { nullptr };

		SoRef<SoSwitch> maskSocket { nullptr };
		SoRef<SoDataRange> maskRange { nullptr };
		SoRef<SoTransferFunction> maskTF { nullptr };
		int maxLabel { 0 };
		QVector<QColor> maskColormap;

		double depthMin { 0 };
		double depthMax { 1 };
		double xMinBound { 0 };
		double xMaxBound { 1 };
		double yMinBound { 0 };
		double yMaxBound { 1 };

		QString shaderPath;
		float currentColormapArr[256 * 4];
		int highlightIdx { 0 };

		auto BuildMaskColorMap() -> void;
		auto BuildHighlightMaskColorMap() -> void;
	};
	auto VolumeMipMask::ToggleJittering(bool use) -> void {
		d->shader->jittering = use;
	}
	auto VolumeMipMask::ToggleDeferredLighting(bool use) -> void {
		d->shader->deferredLighting = use;
	}
	auto VolumeMipMask::Impl::BuildHighlightMaskColorMap() -> void {
		const auto count = maskColormap.count() + 1;
		maskTF->colorMap.setNum(count * 4);
		auto p = maskTF->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		for (auto i = 0; i < count - 1; i++) {
			*p++ = static_cast<float>(maskColormap[i].redF());
			*p++ = static_cast<float>(maskColormap[i].greenF());
			*p++ = static_cast<float>(maskColormap[i].blueF());
			if (i + 1 == highlightIdx) {
				*p++ = static_cast<float>(maskColormap[i].alphaF());
			} else {
				*p++ = 0;
			}
		}
		maskTF->colorMap.finishEditing();
	}

	auto VolumeMipMask::Impl::BuildMaskColorMap() -> void {
		const auto count = maskColormap.count() + 1;
		maskTF->colorMap.setNum(count * 4);
		auto p = maskTF->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		for (auto i = 0; i < count - 1; i++) {
			*p++ = static_cast<float>(maskColormap[i].redF());
			*p++ = static_cast<float>(maskColormap[i].greenF());
			*p++ = static_cast<float>(maskColormap[i].blueF());
			*p++ = static_cast<float>(maskColormap[i].alphaF());
		}
		maskTF->colorMap.finishEditing();
	}

	VolumeMipMask::VolumeMipMask(const QString& name) : IImageGeneral(), IImageSingle(), d { new Impl } {
		general_d->name = name;
		d->shaderPath = QString("%1/shader/VolumeMipMask.glsl").arg(qApp->applicationDirPath());
		BuildSceneGraph();
	}

	VolumeMipMask::~VolumeMipMask() { }

	auto VolumeMipMask::SetHighlight(int labelIdx) -> void {
		d->highlightIdx = labelIdx;
		if (labelIdx > 0) {
			d->BuildHighlightMaskColorMap();
		} else {
			d->BuildMaskColorMap();
		}
	}

	auto VolumeMipMask::SetGamma(float gamma) -> void {
		Q_UNUSED(gamma)
	}

	auto VolumeMipMask::ToggleGamma(bool isGamma) -> void {
		Q_UNUSED(isGamma)
	}

	auto VolumeMipMask::SetZRange(double min, double max) -> void {
		d->depthMin = min;
		d->depthMax = max;
		d->fragmentShader->setShaderParameter1f("lowerBound", min);
		d->fragmentShader->setShaderParameter1f("upperBound", max);
	}

	auto VolumeMipMask::SetXRange(double min, double max) -> void {
		d->xMinBound = min;
		d->xMaxBound = max;
		d->fragmentShader->setShaderParameter1f("xMinBound", min);
		d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}

	auto VolumeMipMask::SetYRange(double min, double max) -> void {
		d->yMaxBound = min;
		d->yMaxBound = max;
		d->fragmentShader->setShaderParameter1f("yMinBound", min);
		d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}

	auto VolumeMipMask::Clear() -> void {
		single_d.reset();
		d->dataRange->min = -1;
		d->dataRange->max = -1;
		d->volumeSocket->replaceChild(0, new SoSeparator);
		d->depthMin = 0;
		d->depthMax = 1;
	}

	auto VolumeMipMask::SetDataMinMax(double min, double max) -> void {
		single_d->min = min;
		single_d->max = max;
		single_d->lower = min;
		single_d->lower = max;
		d->dataRange->min = min;
		d->dataRange->max = max;
	}

	auto VolumeMipMask::SetDataRange(double lower, double upper) -> void {
		single_d->lower = lower;
		single_d->upper = upper;
		d->dataRange->min = lower;
		d->dataRange->max = upper;
	}

	auto VolumeMipMask::ToggleViz(bool show) -> void {
		if (show && single_d->hasData) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
	}

	auto VolumeMipMask::SetVolume(SoVolumeData* vol) -> void {
		if (single_d->min < 0 && single_d->max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			single_d->min = data_min;
			single_d->max = data_max;
			single_d->lower = data_min;
			single_d->upper = data_max;

			d->dataRange->min = data_min + (data_max - data_min) / 5;
			d->dataRange->max = data_max - (data_max - data_min) / 5;
		}
		vol->dataSetId = 1;
		d->volumeSocket->replaceChild(0, vol);
		single_d->hasData = true;
		d->fragmentShader->setShaderParameter1i("isHTExist", 1);
	}

	auto VolumeMipMask::BuildSceneGraph() -> void {
		general_d->rootSwitch = new SoSwitch;
		general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
		general_d->rootSwitch->whichChild = 0;

		d->root = new SoSeparator;
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		general_d->rootSwitch->addChild(d->root.ptr());

		d->matl = new SoMaterial;
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->matl->ambientColor.setValue(1, 1, 1);
		d->matl->diffuseColor.setValue(1, 1, 1);

		d->mds = new SoMultiDataSeparator;
		d->mds->setName((general_d->name = "_Mds").toStdString().c_str());

		d->maskRange = new SoDataRange;
		d->maskRange->dataRangeId = 2;
		d->maskRange->setName((general_d->name + "_MaskRange").toStdString().c_str());

		d->maskSocket = new SoSwitch;
		d->maskSocket->whichChild = 0;
		d->maskSocket->setName((general_d->name + "_MaskSocket").toStdString().c_str());
		d->maskSocket->addChild(new SoSeparator);

		d->dataRange = new SoDataRange;
		d->dataRange->dataRangeId = 1;
		d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

		d->volumeSocket = new SoSwitch;
		d->volumeSocket->whichChild = 0;
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->volumeSocket->addChild(new SoSeparator);

		d->transFunc = new SoTransferFunction;
		d->transFunc->transferFunctionId = 1;
		d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
		d->transFunc->colorMap.setNum(256 * 4);
		auto p = d->transFunc->colorMap.startEditing();
		const auto steps = d->transFunc->colorMap.getNum() / 4;
		for (auto i = 0; i < steps; i++) {
			const auto val = static_cast<float>(i) / static_cast<float>(steps);
			*p++ = val;
			*p++ = val;
			*p++ = val;
			*p++ = val;
		}
		d->transFunc->colorMap.finishEditing();

		d->maskTF = new SoTransferFunction;
		d->maskTF->transferFunctionId = 2;
		d->maskTF->setName((general_d->name + "_MaskTF").toStdString().c_str());

		d->depthTF = new SoTransferFunction;
		d->depthTF->transferFunctionId = 3;
		d->depthTF->setName((general_d->name + "_DepthTF").toStdString().c_str());
		d->depthTF->colorMap.setNum(512 * 4);
		d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("data2", 2);
		d->fragmentShader->addShaderParameter1i("isHTExist", 0);
		d->fragmentShader->addShaderParameter1i("isViewDir", 0);
		d->fragmentShader->addShaderParameter1i("isMaskBlend", 0);
		d->fragmentShader->addShaderParameter1i("useMask", 0);
		d->fragmentShader->addShaderParameter1i("isDepthEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("depthEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1i("isGradientEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("gradientEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1i("isColorDepth", 0);
		d->fragmentShader->addShaderParameter1f("lowerBound", 0);
		d->fragmentShader->addShaderParameter1f("upperBound", 1);
		d->fragmentShader->addShaderParameter1f("xMinBound", 0);
		d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
		d->fragmentShader->addShaderParameter1f("yMinBound", 0);
		d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

		d->shader = new SoVolumeRenderingQuality;
		d->shader->lighting = TRUE;
		d->shader->jittering = TRUE;
		d->shader->deferredLighting = FALSE;
		d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
		d->shader->forVolumeOnly = TRUE;
		d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

		d->volumeRender = new SoVolumeRender;
		d->volumeRender->setName((general_d->name + "_Render").toStdString().c_str());
		d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		d->volumeRender->interpolation = SoVolumeRender::Interpolation::LINEAR;
		d->root->addChild(d->matl.ptr());
		d->root->addChild(d->mds.ptr());
		d->mds->addChild(d->dataRange.ptr());
		d->mds->addChild(d->volumeSocket.ptr());
		d->mds->addChild(d->transFunc.ptr());
		d->mds->addChild(d->maskRange.ptr());
		d->mds->addChild(d->maskSocket.ptr());
		d->mds->addChild(d->maskTF.ptr());
		d->mds->addChild(d->depthTF.ptr());
		d->mds->addChild(d->shader.ptr());

		d->mds->addChild(d->volumeRender.ptr());
	}

	auto VolumeMipMask::ToggleMaskColor(bool isUseMaskColor) -> void {
		if (isUseMaskColor) {
			d->fragmentShader->setShaderParameter1i("isMaskBlend", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isMaskBlend", 0);
		}
	}

	auto VolumeMipMask::ToggleUseMask(bool isUseMask) -> void {
		if (isUseMask) {
			d->fragmentShader->setShaderParameter1i("useMask", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("useMask", 0);
		}
	}

	auto VolumeMipMask::SetColorMapArr(QList<float> colormapArr) -> void {
		d->depthTF->actualColorMap.setNum(256 * 4);
		auto p = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			*p++ = colormapArr[i * 4];
			*p++ = colormapArr[i * 4 + 1];
			*p++ = colormapArr[i * 4 + 2];
			*p++ = colormapArr[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMipMask::SetColorMapArr(float* colormapArr) -> void {
		d->depthTF->actualColorMap.setNum(256 * 4);
		auto p = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			*p++ = colormapArr[i * 4];
			*p++ = colormapArr[i * 4 + 1];
			*p++ = colormapArr[i * 4 + 2];
			*p++ = colormapArr[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMipMask::GetColorMapArr() -> float* {
		return d->currentColormapArr;
	}

	auto VolumeMipMask::SetColorMap(DepthColorMap idx) -> void {
		switch (idx) {
			case DepthColorMap::STANDARD:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
				break;
			case DepthColorMap::TEMPERATURE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::TEMPERATURE;
				break;
			case DepthColorMap::PHYSICS:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
				break;
			case DepthColorMap::GLOW:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
				break;
			case DepthColorMap::BLUE_RED:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_RED;
				break;
			case DepthColorMap::SEISMIC:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::SEISMIC;
				break;
			case DepthColorMap::BLUE_WHITE_RED:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_WHITE_RED;
				break;
			case DepthColorMap::REDTONE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::VOLREN_RED;
				break;
			case DepthColorMap::GREENTONE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::VOLREN_GREEN;
				break;
		}
		SoRef<SoTransferFunction> tempTF = new SoTransferFunction;
		tempTF->predefColorMap = d->depthTF->predefColorMap;
		const auto p = tempTF->actualColorMap.getValues(0);
		auto pp = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			d->currentColormapArr[i * 4] = p[i * 4];
			d->currentColormapArr[i * 4 + 1] = p[i * 4 + 1];
			d->currentColormapArr[i * 4 + 2] = p[i * 4 + 2];
			d->currentColormapArr[i * 4 + 3] = p[i * 4 + 3];
			*pp++ = p[i * 4];
			*pp++ = p[i * 4 + 1];
			*pp++ = p[i * 4 + 2];
			*pp++ = p[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMipMask::ToggleDepthEnhanced(bool isDepthEnhanced) -> void {
		if (isDepthEnhanced) {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 0);
		}
	}

	auto VolumeMipMask::SetDepthEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("depthEnhanceFactor", factor);
	}

	auto VolumeMipMask::ToggleGradientEnhanced(bool isGradientEnhanced) -> void {
		if (isGradientEnhanced) {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 0);
		}
	}

	auto VolumeMipMask::SetGradientEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("gradientEnhanceFactor", factor);
	}

	auto VolumeMipMask::ToggleColorDepth(bool isColor) -> void {
		if (isColor) {
			d->fragmentShader->setShaderParameter1i("isColorDepth", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isColorDepth", 0);
		}
	}

	auto VolumeMipMask::ToggleViewDir(bool isViewDir) -> void {
		if (isViewDir) {
			d->fragmentShader->setShaderParameter1i("isViewDir", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isViewDir", 0);
		}
	}

	auto VolumeMipMask::SetMaskVolume(SoVolumeData* vol) -> void {
		double data_min, data_max;
		vol->getMinMax(data_min, data_max);
		vol->dataSetId = 2;
		d->maskRange->min = data_min;
		d->maskRange->max = data_max;

		d->maxLabel = static_cast<int>(data_max);
		d->maskColormap.clear();
		for (auto i = 0; i < d->maxLabel; i++) {
			d->maskColormap.push_back(QColor(255, 255, 255));
		}
		d->maskSocket->replaceChild(0, vol);
	}

	auto VolumeMipMask::GetLabelCount() const -> int {
		return d->maxLabel;
	}

	auto VolumeMipMask::SetMaskColor(int labelIdx, QColor col) -> void {
		if (d->maskColormap.count() > labelIdx - 1) {
			d->maskColormap[labelIdx - 1] = col;
		}
		d->BuildMaskColorMap();
	}

	auto VolumeMipMask::GetMaskColor(int labelIdx) const -> QColor {
		if (d->maskColormap.count() > labelIdx - 1) {
			return d->maskColormap[labelIdx - 1];
		}
		return QColor();
	}
}
