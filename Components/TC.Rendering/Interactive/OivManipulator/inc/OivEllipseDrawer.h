#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoEllipseScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API OivEllipseDrawer : public SoEllipseScreenDrawer {
	SO_NODE_HEADER(OivEllipseDrawer);
public:
	OivEllipseDrawer();
	virtual void reset();

	auto GetCenterPt()->SbVec2f;
	auto GetMajorAxisPt()->SbVec2f;
	auto GetMinorAxisPt()->SbVec2f;
	auto GetIsConverted()->bool;

	void Activate();
	void Deactivate();

SoINTERNAL public:
	static void initClass();
	static void exitClass();

SoEXTENDER_Documented protected:
	virtual void onMouseDown(SoHandleEventAction* action);
	virtual void onMouseUp(SoHandleEventAction* action);

protected:
	~OivEllipseDrawer();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};