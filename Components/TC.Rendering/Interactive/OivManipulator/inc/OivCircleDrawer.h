#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoEllipseScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API OivCircleDrawer : public SoPolyLineScreenDrawer {
	SO_NODE_HEADER(OivCircleDrawer);

public:
	SoSFInt32 numberOfPoints;

	OivCircleDrawer();

	virtual void reset();

	auto GetStartPt()->SbVec2f;
	auto GetEndPt()->SbVec2f;

	auto setHandleRoot(SoSeparator* root)->void;
	auto getHandleRoot()->SoSeparator*;

	void Activate();
	void Deactivate();

SoINTERNAL public:
	static void initClass();

	static void exitClass();

SoEXTENDER_Documented protected:
	virtual void onMouseDragging(SoHandleEventAction*);

	virtual void onMouseDown(SoHandleEventAction* action);

	virtual void onMouseUp(SoHandleEventAction* action);

protected:
	~OivCircleDrawer();

private:
	void finalizeLine(SoHandleEventAction*);

	struct Impl;
	std::unique_ptr<Impl> d;
};