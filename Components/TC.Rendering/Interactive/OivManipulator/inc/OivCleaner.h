#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

class Oiv2DDrawer;
class SoEventCallback;

class OivCleaner : public SoGroup {
public:
	OivCleaner(Oiv2DDrawer* parent);

	auto SetAxis(const MedicalHelper::Axis& axis)->void;
	auto SetMode(const int &mode)->void;	

private:
	static void HandleMouseButtonEvent(void* data,SoEventCallback* node);

protected:
	virtual ~OivCleaner();

private:

	struct Impl;
	std::unique_ptr<Impl> d;
};