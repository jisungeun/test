#pragma once

#include <memory>

struct PointType {
	int position[3];
};

class TCSkeltonization3d {
public:
	typedef TCSkeltonization3d Self;
	typedef std::shared_ptr<Self> Pointer;

	TCSkeltonization3d();
	~TCSkeltonization3d();

	auto setDimension(int x, int y, int z)->void;		
	auto setInput(unsigned short* img)->void;
	auto Process()->void;
	auto getOutput()->unsigned short*;

private:
	auto fillEulerLUT(int LUT[256])->void;	
	auto isEulerInvariant(int neighbors[26], int LUT[256])->bool;
	auto isSimplePoint(int neighbors[26])->bool;
	auto Octree_labeling(int octant, int label, int cube[26])->void;

	auto getNeighborhood(unsigned short* img, int center[3],int neighbor[26])->void;
	auto getPixel(unsigned short* img, int idx[3])->int;
	struct Impl;
	std::shared_ptr<Impl> d;
};