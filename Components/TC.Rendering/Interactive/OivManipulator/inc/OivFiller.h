#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

class Oiv2DDrawer;
class SoEventCallback;

class OivFiller : public SoGroup {
public:
	OivFiller(Oiv2DDrawer* parent);

	auto SetFillMode(bool isFill)->void;
	auto SetAxis(const MedicalHelper::Axis& axis)->void;
	auto SetLabel(const uint32_t& lable)->void;

private:
	static void HandleMouseButtonEvent(void* data, SoEventCallback* node);

protected:
	virtual ~OivFiller();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};