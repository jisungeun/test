#pragma once

#include <memory>
#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/drawers/SoLassoScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class SoSeparator;
class SoVolumeData;

class TC_Rendering_Interactive_OivManipulator_API OivPickHandler : public QObject {
	Q_OBJECT
public:
	OivPickHandler();
	~OivPickHandler();

	auto SetTargetVolume(SoVolumeData* targetVolume)->void;
	auto SetAxis(const MedicalHelper::Axis& axis)->void;
	auto GetAxis()->MedicalHelper::Axis;
	auto GetSceneGraph()->SoSeparator*;
	auto TogglePick(bool isPick)->void;
	auto SetRotation(bool isRot)->void;

signals:
	void pickValue(int);

private:
	bool eventFilter(QObject* watched, QEvent* event) override;
	auto BuildSceneGraph()->void;
	

	struct Impl;
	std::unique_ptr<Impl> d;
};