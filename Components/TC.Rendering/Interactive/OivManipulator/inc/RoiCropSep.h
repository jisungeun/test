#pragma once
#include <memory>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolyLineScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class SoEventCallback;
class SoMFVec2f;
class SoVertexProperty;


namespace TC {
    class TC_Rendering_Interactive_OivManipulator_API RoiCropSep : public QObject {
        Q_OBJECT
    public:
        RoiCropSep(QObject* parent = nullptr);
        ~RoiCropSep();

        auto GetRoot()->SoSeparator*;
        
        auto ClearROIs()->void;
        auto RemoveROI(int idx)->void;
        auto SetFocus(int idx)->void;

        auto ToggleAdd(bool activate)->void;
        auto ToggleModify(bool activate)->void;
        auto GetROIs()->QList<SoVertexProperty*>;

    signals:
        void roiAdded();
        void roiModified(int);

    protected:
        static void rectCallback(SoPolyLineScreenDrawer::EventArg& arg);
        static void rectMoveCB(void* pImpl, SoEventCallback* eventCB);
        static void rectHandleCB(void* pImpl, SoEventCallback* eventCB);

    private:
        static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}