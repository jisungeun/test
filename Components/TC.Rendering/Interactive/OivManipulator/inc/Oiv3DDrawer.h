#pragma once

#include <memory>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolyLineScreenDrawer.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"
#include "Oiv2DDrawer.h"

class SoSeparator;
class SoVolumeData;
class SoEventCallback;
class SoPerspectiveCamera;

class TC_Rendering_Interactive_OivManipulator_API Oiv3DDrawer : public QObject {
	Q_OBJECT
public:
	Oiv3DDrawer(bool force2D = false);
	~Oiv3DDrawer();

	auto SetTargetVolume(SoVolumeData* targetVolume)->void;
	auto SetSceneRoot(SoSeparator* sceneRoot)->void;
	auto SetCamera(SoPerspectiveCamera* cam)->void;

	auto GetSceneGraph()->SoSeparator*;
	
	auto SetTool(DrawerToolType tool)->void;
	auto SetSizeVolume(SoVolumeData* vol)->void;
	auto PerformSizeFilter(int idx)->void;
	auto SaveEditings()->void;

signals:
	void sigStartDrawing();
	void sigFinishDrawing();

	void sigStartBranch();
	void sigFinishBranch();

	void sigStartSize(int);

private:
	//static void HandleMouseButtonEvent(void* data, SoEventCallback* node);	
	static void HandleSelectionEvent(void* data, SoPath* node);
	static void HandleDeselectionEvent(void* data, SoPath* node);
	static void HandleKeyboardEvent(void* data, SoEventCallback* node);	

	auto BuildSceneGraph()->void;

	//bool eventFilter(QObject* watched, QEvent* event) override;

	auto DrawFreeLineCallback(SoPolyLineScreenDrawer::EventArg& eventArg)->void;
	auto PerformSeparation()->void;
	auto CreateBranch()->void;
	auto PerformBranch()->void;
	auto InitSizeFilter()->void;
	auto FinishSizeFilter()->void;

	struct Impl;
	std::unique_ptr<Impl> d;
};