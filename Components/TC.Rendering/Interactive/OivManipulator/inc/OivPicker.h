#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

class SoEventCallback;
class SoVolumeData;

class OivPicker : public SoGroup {
public:
	OivPicker();

	auto SetAxis(const MedicalHelper::Axis& axis)->void;
	auto GetLabelValue()->int;	
	auto GetTransactionId()->int;
	auto SetMode(int mode)->void;
	auto isRot(bool rot)->void;
	auto SetTargetVolume(SoVolumeData* vol)->void;
private:
	static void HandleMouseButtonEvent(void* data,SoEventCallback* node);

protected:
	virtual ~OivPicker();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};