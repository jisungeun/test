#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolyLineScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API OivRectangleDrawer : public SoPolyLineScreenDrawer {
	SO_NODE_HEADER(OivRectangleDrawer);

public:
	OivRectangleDrawer();

	virtual void reset();

SoINTERNAL public:
	static void initClass();
	static void exitClass();

SoEXTENDER_Documented protected:
	virtual void onKeyDown(SoHandleEventAction* action);
	virtual void onMouseDown(SoHandleEventAction* action);
	virtual void onMouseUp(SoHandleEventAction* action);
	virtual void onMouseDragging(SoHandleEventAction* action);

protected:
	~OivRectangleDrawer();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};