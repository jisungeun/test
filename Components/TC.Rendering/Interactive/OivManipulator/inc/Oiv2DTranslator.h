#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API Oiv2DTranslator {
public:
	Oiv2DTranslator();
	~Oiv2DTranslator();

	auto setContentsRoot(SoSeparator* root)->void;
	auto getContentsRoot()->SoSeparator*;
	auto getActiveParts()->SoNode*;

	auto showGuideline(bool show)->void;

	auto getManipulatorRoot()->SoNode*;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};