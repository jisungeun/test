#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/nodes/SoSeparator.h>

#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API Oiv2DScaler {
public:
	Oiv2DScaler();
	~Oiv2DScaler();

	auto setContentsRoot(SoSeparator* root)->void;

	auto getContentsRoot()->SoSeparator*;	

	auto showGuideline(bool show)->void;

	auto getManipulatorRoot()->SoNode*;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};