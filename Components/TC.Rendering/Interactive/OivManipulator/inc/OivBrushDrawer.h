#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)


class Oiv2DDrawer;
class SoSeparator;
class SoEventCallback;
class SoVolumeData;

class OivBrushDrawer :public SoGroup
{	
public:
	OivBrushDrawer(Oiv2DDrawer* parent);
	~OivBrushDrawer();

	auto SetAxis(const MedicalHelper::Axis& axis)->void;

	auto SetBrushSize(const uint32_t& size)->void;
	auto SetMaskValue(const double& value)->void;	
	auto SetLabel(const uint32_t& label)->void;

private:
	static void HandleMouseMoveEvent(void* data, SoEventCallback* node);
	static void HandleMouseButtonEvent(void* data, SoEventCallback* node);	

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};