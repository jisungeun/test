#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API OivAngleDrawer : public SoPolyLineScreenDrawer {
	SO_NODE_HEADER(OivAngleDrawer);

public:
	OivAngleDrawer();

	auto setHandleRoot(SoSeparator* root)->void;
	auto getHandleRoot()->SoSeparator*;

	virtual void reset();
	void Activate();
	void Deactivate();

SoINTERNAL public:
	static void initClass();

	static void exitClass();

SoEXTENDER_Documented protected:
	virtual void onKeyDown(SoHandleEventAction* action);

	virtual void onMouseMove(SoHandleEventAction* action);

	virtual void onMouseDown(SoHandleEventAction* action);

	virtual void onMouseUp(SoHandleEventAction* action);


protected:
	~OivAngleDrawer();

private:
	void finalizeLine(SoHandleEventAction*);

	struct Impl;
	std::unique_ptr<Impl> d;
};