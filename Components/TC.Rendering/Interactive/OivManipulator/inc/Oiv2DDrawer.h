#pragma once

#include <memory>
#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/drawers/SoLassoScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class SoGuiRenderArea;
class SoSeparator;
class SoVolumeData;
class SoEventCallback;

enum class DrawerToolType {
	None =-1,
	Paint = 1,
	Wipe = 2,
	Fill =3,
	Erase =4,
	LassoAdd= 5,
	LassoSubtract =6,
	Pick =7,
	Clean = 8,
	CleanSelection = 9,
	Flush = 10,
	FlushSelection = 11,
	Divide = 112,
	Branch = 13,
	MergeLabel = 114,
	Erode = 15,
	Dilate = 16,
	WaterShed = 17,
	PickDelete = 18,
	LassoDelete = 19,
	SizeFilter = 20,
	MergeSupport = 115
};


class TC_Rendering_Interactive_OivManipulator_API Oiv2DDrawer : public QObject {
	Q_OBJECT
public:
	Oiv2DDrawer();
	~Oiv2DDrawer();

	auto SetTargetVolume(SoVolumeData* targetVolume)->void;
	auto SetAxis(const MedicalHelper::Axis& axis) ->void;

	auto GetSceneGraph()->SoSeparator*;

	auto SetTool(DrawerToolType tool)->void;
	auto GetTool()->DrawerToolType;	

	auto SetCurLabel(uint32_t label)->void;
	auto GetCurLabel()->uint32_t;
	auto SetPaintBrushSize(uint32_t size)->void;
	auto SetWipeBrushSize(uint32_t size)->void;

	auto SetMergerVolume(SoVolumeData* vol)->void;	

	auto SaveEditinigs()->void;
	auto HistoryFromTool(int)->void;

	auto ActivateMergeSupport()->void;

signals:
	void pickValue(int);
	void pickRemove();
	void mergeVol(int);
	void sigHistory(int);	

private:
	auto BuildSceneGraph()->void;

	bool eventFilter(QObject* watched, QEvent* event) override;
	
	auto DrawLassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg)->void;
	auto MergeSupportCallback(SoPolyLineScreenDrawer::EventArg& eventArg)->void;
    auto EraseLassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg)->void;
	auto FrustumLassoCallBack(SoPolyLineScreenDrawer::EventArg& eventArg)->void;
	auto DrawPolyCallback(SoPolyLineScreenDrawer::EventArg& eventArg)->void;

    auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;

	struct Impl;
	std::unique_ptr<Impl> d;
};