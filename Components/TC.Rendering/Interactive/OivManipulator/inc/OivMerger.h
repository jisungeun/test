#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

class SoEventCallback;

class OivMerger : public SoGroup {
public:
	OivMerger();
	auto SetAxis(const MedicalHelper::Axis& axis)->void;
	auto GetChunkValue()->int;	
	auto SetChunkVolume(SoVolumeData* vol)->void;
	auto ManageSelectedRegion(std::vector<SbVec3f> lasso_points)->void;	

	auto isUpdate()->bool;
	auto resetUpdate()->void;

private:
	static void HandleMouseButtonEvent(void* data, SoEventCallback* node);

protected:
	virtual ~OivMerger();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};