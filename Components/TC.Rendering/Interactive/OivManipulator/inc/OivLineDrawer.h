#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class TC_Rendering_Interactive_OivManipulator_API OivLineDrawer : public SoPolyLineScreenDrawer {
	SO_NODE_HEADER(OivLineDrawer);

public:
	OivLineDrawer();

	virtual void reset();

	void Activate();
	void Deactivate();

SoINTERNAL public:
	static void initClass();

	static void exitClass();

SoEXTENDER_Documented protected:
	virtual void onKeyDown(SoHandleEventAction* action);

	virtual void onMouseMove(SoHandleEventAction* action);

	virtual void onMouseDown(SoHandleEventAction* action);

	virtual void onMouseUp(SoHandleEventAction* action);

protected:
	~OivLineDrawer();

private:
	void finalizeLine(SoHandleEventAction*);

	struct Impl;
	std::unique_ptr<Impl> d;
};