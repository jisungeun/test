#pragma once

#include <QObject>

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolyLineScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.OivManipulatorExport.h"

class SoSeparator;
class SoSwitch;

class TC_Rendering_Interactive_OivManipulator_API Oiv2DSpuit : public QObject {
	Q_OBJECT
public:
	Oiv2DSpuit(QObject* parent = nullptr);
	~Oiv2DSpuit();

	auto setSliceSceneGraphRoot(SoSeparator* root) -> void;
	auto getSpuitRoot() -> SoSwitch*;
	auto activateSpuit(bool activate) -> void;
	auto getCurrentTFBoxInfo() -> double*;
	auto setFinalizeAction(SoHandleEventAction* action) -> void;

signals:
	void sigFinishSpuit();

protected:
	static void lineFinalizedCallback(SoPolyLineScreenDrawer::EventArg& eventArg);

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
