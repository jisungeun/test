#include <QApplication>
#include <QMainWindow>
#include <QShortcut>
#include <QFileDialog>
#include <QSettings>

#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/details/SoFaceDetail.h>
#include <Inventor/details/SoLineDetail.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>


#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <LDM/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>

#include <OivHdf5Reader.h>
#include <OivRectangleDrawer.h>

#include <RoiCropSep.h>

static MedicalHelper::Axis viewAxis{ MedicalHelper::AXIAL };

int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	QSettings qs("Test/TC2dMeasureTest");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}

	QString file_name = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (file_name.isEmpty()) {
		return 1;
	}

	qs.setValue("prevPath", file_name);

	QMainWindow* window = new QMainWindow;
	SoQt::init(window);
	SoVolumeRendering::init();
	OivRectangleDrawer::initClass();
	OivHdf5Reader::initClass();

	//create scene graph
	SoRef<SoSeparator> rootSceneGraph = new SoSeparator;
	rootSceneGraph->setName("SceneRoot");

	SoRef<SoOrthographicCamera> camera = new SoOrthographicCamera;
	rootSceneGraph->addChild(camera.ptr());

	SoRef< SoSeparator> volSep = new SoSeparator;

	SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
	reader->setDataGroupPath("/Data/3D");
	reader->setTileDimension(256);
	reader->setTileName("000000");
	reader->setFilename(file_name.toStdString());

	SoRef<SoVolumeData> volData = new SoVolumeData;
	volData->setReader(*reader, TRUE);
	volSep->addChild(volData.ptr());

	SoRef<SoDataRange> dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange.ptr(), volData.ptr());
	volSep->addChild(dataRange.ptr());

	SoRef<SoMaterial> volMatl = new SoMaterial;
	volMatl->diffuseColor.setValue(1, 1, 1);
	volMatl->transparency.setValue(0.5);
	volSep->addChild(volMatl.ptr());

	SoRef<SoTransferFunction> transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
	MedicalHelper::dicomCheckMonochrome1(transFunc.ptr(), volData.ptr());
	volSep->addChild(transFunc.ptr());

	SoRef<SoOrthoSlice> orthoSlice = new SoOrthoSlice;
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volData->data.getSize()[viewAxis] / 2;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volSep->addChild(orthoSlice.ptr());

	rootSceneGraph->addChild(volSep.ptr());

	SoRef<SoSeparator> drawerSep = new SoSeparator;
	drawerSep->setName("DrawerSeparator");
	drawerSep->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	SoRef<SoSwitch> measureSwitch = new SoSwitch;
	measureSwitch->whichChild = -1;

	TC::RoiCropSep* roiSep = new TC::RoiCropSep;
	measureSwitch->addChild(roiSep->GetRoot());
			
    drawerSep->addChild(measureSwitch.ptr());		

	rootSceneGraph->addChild(drawerSep.ptr());

	auto viewer = new SoQtPlaneViewer(window);

	viewer->setSceneGraph(rootSceneGraph.ptr());
	viewer->setTransparencyType(SoGLRenderAction::OPAQUE_FIRST);
	viewer->setViewing(FALSE);
	viewer->viewAll();
	viewer->show();	

	window->resize(800, 600);

	MedicalHelper::orientView(viewAxis, camera.ptr(), volData.ptr());
		
	// setup shortcuts
	QShortcut* turnOnROI = new QShortcut(QKeySequence("Ctrl+1"), window);
	QObject::connect(turnOnROI, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 0;
	});

	QShortcut* turnOffROI = new QShortcut(QKeySequence("Ctrl+2"), window);
	QObject::connect(turnOffROI, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = -1;
		});

	QShortcut* clearROI = new QShortcut(QKeySequence("Ctrl+3"), window);
	QObject::connect(clearROI, &QShortcut::activated, [=]() {
		roiSep->ClearROIs();
	});		

	// run app
	window->show();
    app.exec();

	rootSceneGraph = NULL;
	delete window;

	OivHdf5Reader::exitClass();
	OivRectangleDrawer::exitClass();
	SoVolumeRendering::finish();
	SoQt::finish();

	return 0;
}