#include <QApplication>
#include <QMainWindow>
#include <QShortcut>
#include <QFileDialog>
#include <QSettings>
#include <QTextStream>

#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/details/SoFaceDetail.h>
#include <Inventor/details/SoLineDetail.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>


#include <ImageViz/SoImageViz.h>

#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <LDM/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>

#include <OivHdf5Reader.h>
#include <OivLineDrawer.h>
#include <OivMultiLineDrawer.h>
#include <OivCircleDrawer.h>
#include <OivEllipseDrawer.h>

#include <LineMeasureSep.h>
#include <CircleMeasureSep.h>
#include <MultiLineMeasureSep.h>
#include <PolygonMeasureSep.h>
#include <EllipsoidMeasureSep.h>
#include <AngleMeasureSep.h>

#include <Measure2dControl.h>

static MedicalHelper::Axis viewAxis{ MedicalHelper::AXIAL };

int main(int argc, char** argv)
{	
	QApplication app(argc, argv);

	QSettings qs("Test/TC2dMeasureTest");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}

	QString file_name = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (file_name.isEmpty()) {
		return 1;
	}

	qs.setValue("prevPath", file_name);
		
	QFile f("G:/Source/TSS/Resources/style/tcdarkstyle/tcdark.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	}
	else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

	QMainWindow* window = new QMainWindow;
	SoQt::init(window);
	SoImageViz::init();
	SoVolumeRendering::init();
	OivLineDrawer::initClass();
	OivCircleDrawer::initClass();
	OivEllipseDrawer::initClass();
	OivMultiLineDrawer::initClass();
	OivHdf5Reader::initClass();

	//create scene graph
	SoRef<SoSeparator> rootSceneGraph = new SoSeparator;
	rootSceneGraph->setName("SceneRoot");

	SoRef<SoOrthographicCamera> camera = new SoOrthographicCamera;
	rootSceneGraph->addChild(camera.ptr());

	SoRef< SoSeparator> volSep = new SoSeparator;

	SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
	reader->setDataGroupPath("/Data/3D");
	reader->setTileDimension(256);
	reader->setTileName("000000");
	reader->setFilename(file_name.toStdString());

	SoRef<SoVolumeData> volData = new SoVolumeData;
	volData->setReader(*reader, TRUE);
	volSep->addChild(volData.ptr());

	SoRef<SoDataRange> dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange.ptr(), volData.ptr());
	volSep->addChild(dataRange.ptr());

	SoRef<SoMaterial> volMatl = new SoMaterial;
	volMatl->diffuseColor.setValue(1, 1, 1);
	volMatl->transparency.setValue(0.5);
	volSep->addChild(volMatl.ptr());

	SoRef<SoTransferFunction> transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
	MedicalHelper::dicomCheckMonochrome1(transFunc.ptr(), volData.ptr());
	volSep->addChild(transFunc.ptr());

	SoRef<SoOrthoSlice> orthoSlice = new SoOrthoSlice;
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volData->data.getSize()[viewAxis] / 2;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volSep->addChild(orthoSlice.ptr());

	rootSceneGraph->addChild(volSep.ptr());

	SoRef<SoSeparator> drawerSep = new SoSeparator;
	drawerSep->setName("DrawerSeparator");
	drawerSep->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	SoRef<SoSwitch> measureSwitch = new SoSwitch;
	measureSwitch->whichChild = -1;
	
	TC::LineMeasureSep* lineSep = new TC::LineMeasureSep;
	measureSwitch->addChild(lineSep->GetRoot());

	TC::MultiLineMeasureSep* multiLineSep = new TC::MultiLineMeasureSep;
	measureSwitch->addChild(multiLineSep->GetRoot());

	TC::PolygonMeasureSep* polygonSep = new TC::PolygonMeasureSep;
	measureSwitch->addChild(polygonSep->GetRoot());

	TC::CircleMeasureSep* circleSep = new TC::CircleMeasureSep;
	measureSwitch->addChild(circleSep->GetRoot());

	TC::EllipsoidMeasureSep* ellipSep = new TC::EllipsoidMeasureSep;
	measureSwitch->addChild(ellipSep->GetRoot());

	TC::AngleMeasureSep* angleSep = new TC::AngleMeasureSep;
	measureSwitch->addChild(angleSep->GetRoot());

    //drawerSep->addChild(measureSwitch.ptr());
	auto control = new TC::Measure2dControl;
	drawerSep->addChild(control->GetToolRoot());
	rootSceneGraph->addChild(drawerSep.ptr());

	auto viewer = new SoQtPlaneViewer(window);

	viewer->setSceneGraph(rootSceneGraph.ptr());
	viewer->setTransparencyType(SoGLRenderAction::OPAQUE_FIRST);
	viewer->setViewing(FALSE);
	viewer->viewAll();
	viewer->show();	
		
	control->show();

	window->resize(800, 600);

	MedicalHelper::orientView(viewAxis, camera.ptr(), volData.ptr());
		
	// setup shortcuts
	QShortcut* lineTool = new QShortcut(QKeySequence("Ctrl+1"), window);
	QObject::connect(lineTool, &QShortcut::activated, [=]() {					
		measureSwitch->whichChild = 0;
		lineSep->Activate();
	});
	QShortcut* lineDel = new QShortcut(QKeySequence("Alt+1"), window);
	QObject::connect(lineDel, &QShortcut::activated, [=]() {
		auto num = lineSep->GetMeasure().count();
		lineSep->DeleteItem(num / 2);
		});

	QShortcut* multiLineTool = new QShortcut(QKeySequence("Ctrl+2"), window);
	QObject::connect(multiLineTool, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 1;
		multiLineSep->Activate();
	});

	QShortcut* multiLineDel = new QShortcut(QKeySequence("Alt+2"), window);
	QObject::connect(multiLineDel, &QShortcut::activated, [=]() {
		auto num = multiLineSep->GetMeasure().count();
		multiLineSep->DeleteItem(num / 2);
		});

	QShortcut* polygonTool = new QShortcut(QKeySequence("Ctrl+3"), window);
	QObject::connect(polygonTool, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 2;
		polygonSep->Activate();
		});
	QShortcut* polygonDel = new QShortcut(QKeySequence("Alt+3"), window);
	QObject::connect(polygonDel, &QShortcut::activated, [=]() {
		auto num = polygonSep->GetMeasure().count();
		polygonSep->DeleteItem(num / 2);
		});

	QShortcut* circleTool = new QShortcut(QKeySequence("Ctrl+4"), window);
	QObject::connect(circleTool, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 3;
		circleSep->Activate();
		});

	QShortcut* circleDel = new QShortcut(QKeySequence("Alt+4"), window);
	QObject::connect(circleDel, &QShortcut::activated, [=]() {
		auto num = circleSep->GetMeasure().count();
		circleSep->DeleteItem(num / 2);
	    });

	QShortcut* eTool = new QShortcut(QKeySequence("Ctrl+5"), window);
	QObject::connect(eTool, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 4;
		ellipSep->Activate();
		});

	QShortcut* eDel = new QShortcut(QKeySequence("Alt+5"), window);
	QObject::connect(eDel, &QShortcut::activated, [=]() {
		auto num = ellipSep->GetMeasure().count();
		ellipSep->DeleteItem(num / 2);
		});

	QShortcut* aTool = new QShortcut(QKeySequence("Ctrl+6"), window);
	QObject::connect(aTool, &QShortcut::activated, [=]() {
		measureSwitch->whichChild = 5;
		angleSep->Activate();
		});

	QShortcut* aDel = new QShortcut(QKeySequence("Alt+6"), window);
	QObject::connect(aDel, &QShortcut::activated, [=]() {
		auto num = angleSep->GetMeasure().count();
		angleSep->DeleteItem(num / 2);
		});

	// run app
	window->show();
    app.exec();

	rootSceneGraph = NULL;
	delete window;

	OivHdf5Reader::exitClass();
	OivMultiLineDrawer::exitClass();
	OivEllipseDrawer::exitClass();
	OivCircleDrawer::exitClass();
	OivLineDrawer::exitClass();	
	SoImageViz::finish();
	SoVolumeRendering::finish();
	SoQt::finish();

	return 0;
}