project(TC2dMeasureTest)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES	
	src/OivMeasureTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		${VIEWER_COMPONENT_LINK_LIBRARY}
		Qt5::Widgets
		Qt5::Gui
		Qt5::Core		
		#TC::Components::OIVTCFIO
		TC::Rendering::RenderWindow2D
		TC::Rendering::Interactive::OivManipulator
		TC::Rendering::Interactive::Measure2D
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases") 	

