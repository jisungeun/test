#include <qcoreapplication.h>
#include <QMouseEvent>
#include <QMutex>

#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/misc/SbExtrusionGenerator.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoMaterial.h>

#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/actions/SoSearchAction.h>
#include <Inventor/elements/SoViewVolumeElement.h>

#include <ImageViz/SbImageDataAdapterHelper.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionCubeProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/SeparatingAndFilling/SoExpandLabelsProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Engines/SoImageVizEngineHeaders.h>
#include <ImageViz/Engines/MathematicalMorphology/HitOrMissAndSkeleton/SoPruningProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/HitOrMissAndSkeleton/SoIsolatedPointsProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/HitOrMissAndSkeleton/SoEndPointsProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/OpeningAndClosing/SoClosingBallProcessing3d.h>
#include <ImageViz/Engines/ImageSegmentation/SeparatingAndFilling/SoFillHolesProcessing3d.h>

#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>

#include <Medical/helpers/MedicalHelper.h>

#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSelection.h>

#pragma warning(pop)

#include "OivFreeLine.h"
#include "TCSkeletonization3d.h"

#include "Oiv3DDrawer.h"


struct Oiv3DDrawer::Impl {
	SoVolumeData* targetVolume{ nullptr };
	SoVolumeData* branchVolume{ nullptr };
	SoVolumeData* bodyVolume{ nullptr };

	SoVolumeData* sizeVolume{ nullptr };

	SoSeparator* sceneRoot{ nullptr };

	//SoSeparator* root{ nullptr };
	SoAnnotation* root{ nullptr };
	SoAnnotation* drawerSeparator{ nullptr };

	SoMaterial* shapeMatl{ nullptr };
	SoSeparator* shape3DSeparator{ nullptr };

	//SoPerspectiveCamera* camera;

	SoSwitch* drawToolSwitch{ nullptr };

	SoShape* curGuideShape{ nullptr };

	SoSeparator* handleSeparator{ nullptr };
	SoSelection* selectionNode{ nullptr };
	///SoSeparator* selectionNode{ nullptr };
	int selectionIndex{ -1 };
	//int traj_idx{ -1 };    
	QList<QList<SbVec3f>> trajSet;
	QList<int> traj_idx;

	SoMaterial* curMatl{ nullptr };
	SoTranslation* curTrans{ nullptr };

	OivFreeLine* lineTool{ nullptr };

	DrawerToolType curTool{ DrawerToolType::None };

	std::map<DrawerToolType, std::string> drawTools;

	QList<double> cur_size_list;

	bool force2D;
};

Oiv3DDrawer::Oiv3DDrawer(bool force2D) :d{ new Impl } {
	d->force2D = force2D;
	using ToolInfoPair = std::pair<DrawerToolType, std::string>;
	d->drawTools.insert(ToolInfoPair(DrawerToolType::None, "DrawToolCursor"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Divide, "DrawDivideLine"));

	BuildSceneGraph();

	SetTool(DrawerToolType::None);
	//qApp->installEventFilter(this);

	//d->camera = new SoPerspectiveCamera;
	//d->root->addChild(d->camera);
	d->root->addChild(new SoSeparator);
	auto light = new SoDirectionalLight;
	light->direction.setValue(0, 0, 1);
	d->root->addChild(light);

	d->selectionNode = new SoSelection;
	//d->selectionNode = new SoSeparator;
	d->selectionNode->policy.setValue(SoSelection::Policy::SINGLE);
	d->root->addChild(d->selectionNode);

	d->handleSeparator = new SoSeparator;
	d->selectionNode->addChild(d->handleSeparator);

	SoEventCallback* buttonCallback = new SoEventCallback;
	//buttonCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
	buttonCallback->addEventCallback(SoKeyboardEvent::getClassTypeId(), HandleKeyboardEvent, d.get());
	d->root->addChild(buttonCallback);
	d->selectionNode->addSelectionCallback(HandleSelectionEvent, d.get());
	d->selectionNode->addDeselectionCallback(HandleDeselectionEvent, d.get());
}

Oiv3DDrawer::~Oiv3DDrawer() {

}

auto Oiv3DDrawer::SetCamera(SoPerspectiveCamera* cam) -> void {
	//d->root->replaceChild(0, cam);
	/*d->camera->heightAngle.connectFrom(&cam->heightAngle);
	d->camera->aspectRatio.connectFrom(&cam->aspectRatio);
	d->camera->farDistance.connectFrom(&cam->farDistance);
	d->camera->focalDistance.connectFrom(&cam->focalDistance);
	d->camera->nearDistance.connectFrom(&cam->nearDistance);
	d->camera->orientation.connectFrom(&cam->orientation);
	d->camera->position.connectFrom(&cam->position);*/
}

void Oiv3DDrawer::HandleDeselectionEvent(void* data, SoPath* node) {
	Q_UNUSED(node)
	auto d = static_cast<Impl*>(data);
	if (nullptr == d) {
		return;
	}
	if (d->selectionIndex > -1) {
		if (d->curMatl) {
			d->curMatl->diffuseColor.setValue(0.4f, 0.4f, 0.8f);
		}
		d->curMatl = nullptr;
		d->curTrans = nullptr;
		d->selectionIndex = -1;
		//d->traj_idx = -1;        
	}
}

void Oiv3DDrawer::HandleSelectionEvent(void* data, SoPath* selectionPath) {
	auto d = static_cast<Impl*>(data);
	if (nullptr == d) {
		return;
	}
	int i_unused;
	auto node = selectionPath->getNode(SoSphere::getClassTypeId(), i_unused);
	if (node) {
		d->selectionIndex = -1;
		//d->traj_idx = -1;
		QString sphName = QString(node->getName().getString());
		auto index = sphName.remove(0, 3).toInt() - 1;
		d->selectionIndex = index;
		auto MatlName = QString("SphMatl%1").arg(index + 1);
		d->curMatl = MedicalHelper::find<SoMaterial>(d->handleSeparator, MatlName.toStdString());
		d->curMatl->diffuseColor.setValue(0, 1, 0);

		auto TransName = QString("SphTrans%1").arg(index + 1);
		d->curTrans = MedicalHelper::find<SoTranslation>(d->handleSeparator, TransName.toStdString());
		auto trans_val = d->curTrans->translation.getValue();
		auto traj_set = d->trajSet[index];
		for (auto i = 0; i < traj_set.size(); i++) {
			if (traj_set[i] == trans_val) {
				d->traj_idx[index] = i;
				break;
			}
		}
	}
}

auto Oiv3DDrawer::SetSizeVolume(SoVolumeData* vol) -> void {
	d->sizeVolume = vol;
}

void Oiv3DDrawer::HandleKeyboardEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();
	auto d = static_cast<Impl*>(data);
	if (nullptr == d) {
		action->setHandled();
		return;
	}
	auto keyEvent = (SoKeyboardEvent*)node->getEvent();
	if (d->selectionIndex > -1) {
		auto traj_set = d->trajSet[d->selectionIndex];
		if (keyEvent->getKey() == SoKeyboardEvent::Key::UP_ARROW) {
			if (d->traj_idx[d->selectionIndex] + 1 < traj_set.size()) {
				d->traj_idx[d->selectionIndex]++;
			}
		}
		else if (keyEvent->getKey() == SoKeyboardEvent::Key::DOWN_ARROW) {
			if (d->traj_idx[d->selectionIndex] - 1 > -1) {
				d->traj_idx[d->selectionIndex]--;
			}
		}
		d->curTrans->translation.setValue(traj_set[d->traj_idx[d->selectionIndex]]);
	}
}
/*
void Oiv3DDrawer::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (nullptr == d) {
		action->setHandled();
		return;
	}
	auto buttonEvent = (SoMouseButtonEvent*)node->getEvent();
	auto vr = node->getAction()->getViewportRegion();
	if (buttonEvent->getState() == SoButtonEvent::State::DOWN) {
		if(d->curMatl) {
			d->curMatl->diffuseColor.setValue(0.8, 0.2, 0.2);
		}
		SoRayPickAction rayPick = SoRayPickAction(vr);
		rayPick.setPoint(buttonEvent->getPosition());
		rayPick.apply(d->handleSeparator);
		rayPick.getPickedPoint()

		d->curMatl = nullptr;
		d->curTrans = nullptr;
		d->selectionIndex = -1;
	}
}*/
/*
bool Oiv3DDrawer::eventFilter(QObject* watched, QEvent* event) {
	if (event->type() == QEvent::MouseButtonRelease)
	{
		if(d->curTool == DrawerToolType::Divide) {

		}
	}
	else if(event->type() == QEvent::MouseButtonPress) {
		if (d->curTool == DrawerToolType::Divide) {

		}
	}
	return false;
}*/

auto Oiv3DDrawer::BuildSceneGraph() -> void {
	//d->root = new SoSeparator;
	d->root = new SoAnnotation;
	d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	d->drawerSeparator = new SoAnnotation;
	d->root->addChild(d->drawerSeparator);

	d->drawToolSwitch = new SoSwitch;

	auto lineTool = new OivFreeLine;
	lineTool->setName(d->drawTools.find(DrawerToolType::Divide)->second.c_str());
	lineTool->onFinish.add(*this, &Oiv3DDrawer::DrawFreeLineCallback);

	d->drawToolSwitch->addChild(lineTool);
	d->lineTool = lineTool;

	d->drawerSeparator->addChild(d->drawToolSwitch);

	d->shape3DSeparator = new SoSeparator;
	d->shape3DSeparator->setName("FrustumSep");
	d->shapeMatl = new SoMaterial;
	d->shapeMatl->ambientColor.setValue(1.0, 1.0, 0.0);
	d->shapeMatl->transparency.setValue(0.5);

	d->shape3DSeparator->addChild(d->shapeMatl);
	d->shape3DSeparator->addChild(new SoSeparator);

	d->root->addChild(d->shape3DSeparator);
}

auto Oiv3DDrawer::SetTargetVolume(SoVolumeData* targetVolume) -> void {
	d->targetVolume = targetVolume;
}

auto Oiv3DDrawer::SetSceneRoot(SoSeparator* sceneRoot) -> void {
	d->sceneRoot = sceneRoot;
}

auto Oiv3DDrawer::GetSceneGraph() -> SoSeparator* {
	return d->root;
}

auto Oiv3DDrawer::SetTool(DrawerToolType tool) -> void {
	auto childIndex = -1;
	switch (tool) {
	case DrawerToolType::Divide:
		childIndex = d->drawToolSwitch->findChild(d->lineTool);
		emit sigStartDrawing();
		break;
	case DrawerToolType::Branch:
		CreateBranch();
		emit sigStartBranch();
		break;
	case DrawerToolType::SizeFilter:
		InitSizeFilter();
		break;
	case DrawerToolType::None:
	default:
		tool = DrawerToolType::None;
		if (d->curTool == DrawerToolType::Divide) {			
			emit sigFinishDrawing();
			//perform separation
			//PerformSeparation();
		}
		else if (d->curTool == DrawerToolType::Branch) {
			PerformBranch();
			emit sigFinishBranch();
		}
		else if(d->curTool == DrawerToolType::SizeFilter) {
			FinishSizeFilter();
		}
		break;
	}

	d->drawToolSwitch->whichChild = childIndex;
	d->curTool = tool;
}

auto Oiv3DDrawer::InitSizeFilter() -> void {	
	if(nullptr == d->targetVolume) {
		return;
	}
	//MedicalHelper::dicomAdjustVolume(d->targetVolume);
	auto imageAdapter = MedicalHelper::getImageDataAdapter(d->targetVolume);
	imageAdapter->ref();
	
	//make into binary image
    SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;
	thresh->ref();
    thresh->inImage = imageAdapter;
	thresh->thresholdLevel.setValue(1, static_cast<float>(INT_MAX));
	SoRef<SoLabelingProcessing> labeling = new SoLabelingProcessing;
	labeling->ref();
	labeling->inObjectImage.connectFrom(&thresh->outBinaryImage);

	SoRef<SoLabelAnalysisQuantification> analysis = new SoLabelAnalysisQuantification;
	analysis->ref();

	if (d->force2D) {
		analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::VOXEL_COUNT));
	}
	else {
		analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
	}

	analysis->inIntensityImage = imageAdapter;
	analysis->inLabelImage.connectFrom(&labeling->outLabelImage);

	auto result = analysis->outAnalysis.getValue();

	d->cur_size_list.clear();

	for (auto i = 0;i< result->getNumLabels();i++) {
		auto volume = result->getValueAsDouble(i, 0, 0);
		d->cur_size_list.push_back(volume);	    
	}
	std::stable_sort(d->cur_size_list.begin(), d->cur_size_list.end());	

	while(analysis->getRefCount()>0) {
		analysis->unref();
	}
	analysis = nullptr;
	while(labeling->getRefCount()>0) {
		labeling->unref();
	}
	labeling = nullptr;
	while(thresh->getRefCount()>0) {
		thresh->unref();
	}
	thresh = nullptr;
	while(imageAdapter->getRefCount()>0) {
		imageAdapter->unref();
	}
	imageAdapter = nullptr;

	emit sigStartSize(d->cur_size_list.count());

	PerformSizeFilter(1);
}

auto Oiv3DDrawer::PerformSizeFilter(int idx) -> void {
	if (nullptr == d->targetVolume) {
		return;
	}
	//MedicalHelper::dicomAdjustVolume(d->targetVolume);
	auto imageAdapter = MedicalHelper::getImageDataAdapter(d->targetVolume);
	imageAdapter->ref();

	//make into binary image
	SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;	
	thresh->inImage = imageAdapter;
	thresh->thresholdLevel.setValue(1, static_cast<float>(INT_MAX));

	SoRef<SoLabelingProcessing> labeling = new SoLabelingProcessing;	
	labeling->inObjectImage.connectFrom(&thresh->outBinaryImage);

	SoRef<SoMeasureImageProcessing> toMeasure = new SoMeasureImageProcessing;	
	toMeasure->inLabelImage.connectFrom(&labeling->outLabelImage);
	toMeasure->inIntensityImage = imageAdapter;
	if(d->force2D) {
		toMeasure->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::VOXEL_COUNT));
	}else {
		toMeasure->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
	}	

	auto min_val = d->cur_size_list[idx-1];	

	SoRef<SoThresholdingProcessing> mToThresh = new SoThresholdingProcessing;	
	mToThresh->inImage.connectFrom(&toMeasure->outMeasureImage);
	mToThresh->thresholdLevel.setValue(d->cur_size_list[0], min_val);

	//init sizeVolume

	SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;	
	reader->imageData.connectFrom(&mToThresh->outBinaryImage);
    //d->sizeVolume

	d->sizeVolume->setReader(*reader, TRUE);
	d->sizeVolume->data.touch();	
}

auto Oiv3DDrawer::FinishSizeFilter() -> void {
    
}

auto Oiv3DDrawer::CreateBranch() -> void {
	if (nullptr == d->targetVolume) {
		return;
	}

	//Custom Method based on ITK function
	/*auto size = d->targetVolume->getDimension();

	auto buffer = static_cast<unsigned short*>(d->targetVolume->data.getValue());

	auto skeleton = new TCSkeltonization3d;
	skeleton->setDimension(size[0], size[1], size[2]);
	skeleton->setInput(buffer);

	skeleton->Process();

	d->targetVolume->data.setValue(size, SbDataType::UNSIGNED_SHORT, skeleton->getOutput(), SoSFArray::COPY);
	d->targetVolume->data.touch();    */

	//Open Inventor CenterLine
	//MedicalHelper::dicomAdjustVolume(d->targetVolume);
	auto imageReader = MedicalHelper::getImageDataAdapter(d->targetVolume);	
	imageReader->interpretation = SoMemoryDataAdapter::BINARY;
	//distancemap test
	auto fill = new SoFillHolesProcessing3d;
	fill->inObjectImage = imageReader;

	auto distance = new SoEuclideanDistanceMapProcessing3d;
	distance->inBinaryImage.connectFrom(&fill->outObjectImage);

	auto th = new SoThresholdingProcessing;
	th->inImage.connectFrom(&distance->outMapImage);
	th->thresholdLevel.setValue(1, static_cast<float>(INT_MAX));

	auto dilate = new SoDilationBallProcessing3d;
	dilate->inImage.connectFrom(&th->outBinaryImage);
	dilate->elementSize.setValue(12);

	std::cout << "Estimate center body" << std::endl;	
	
	auto minus = new SoLogicalImageProcessing;
	minus->inImage1.connectFrom(&fill->outObjectImage);
	minus->inImage2.connectFrom(&dilate->outImage);
	minus->logicalOperator = SoLogicalImageProcessing::LogicalOperator::SUB;

	std::cout << "Remove center body from original mask" << std::endl;
	
	auto labeling2 = new SoLabelingProcessing;
	labeling2->inObjectImage.connectFrom(&minus->outImage);

	auto imageToSize = new SoMeasureImageProcessing;
	imageToSize->ref();
	imageToSize->inIntensityImage = imageReader;
	imageToSize->inLabelImage.connectFrom(&labeling2->outLabelImage);
	imageToSize->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));				
	auto th2 = new SoThresholdingProcessing;
	th2->inImage.connectFrom(&imageToSize->outMeasureImage);
	th2->thresholdLevel.setValue(2, static_cast<float>(INT_MAX));

    std::cout << "Remove small particles" << std::endl;
	
	auto lab = new SoLabelingProcessing;
	lab->inObjectImage.connectFrom(&th2->outBinaryImage);

	auto imageToMorph = new SoMeasureImageProcessing;
	imageToMorph->ref();
	imageToMorph->inIntensityImage = imageReader;
	imageToMorph->inLabelImage.connectFrom(&lab->outLabelImage);
	imageToMorph->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::FERET_DIAMETER_RATIO_3D));

	auto th3 = new SoThresholdingProcessing;
	th3->inImage.connectFrom(&imageToMorph->outMeasureImage);
	th3->thresholdLevel.setValue(2, static_cast<float>(INT_MAX));
    std::cout << "Remove non-narrow structure" << std::endl;				

	//save branch volume
	d->branchVolume = nullptr;
	d->branchVolume = new SoVolumeData;

	auto bconv = new SoConvertImageProcessing;
	bconv->inImage.connectFrom(&th3->outBinaryImage);
	bconv->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

	auto breader = new SoVRImageDataReader;
	breader->imageData.connectFrom(&bconv->outImage);

	d->branchVolume->setReader(*breader, TRUE);

	std::cout << "save branch volume" << std::endl;

	auto inputConv = new SoConvertImageProcessing;
	inputConv->inImage = imageReader;
	inputConv->dataType = SoConvertImageProcessing::BINARY;

	auto bodyMinus = new SoLogicalImageProcessing;
	bodyMinus->inImage2.connectFrom(&th3->outBinaryImage);
	bodyMinus->inImage1.connectFrom(&inputConv->outImage);
	bodyMinus->logicalOperator = SoLogicalImageProcessing::LogicalOperator::SUB;

	auto bodyConv = new SoConvertImageProcessing;
	bodyConv->inImage.connectFrom(&bodyMinus->outImage);
	bodyConv->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

	auto bodyReader = new SoVRImageDataReader;
	bodyReader->imageData.connectFrom(&bodyConv->outImage);

	d->bodyVolume = nullptr;
	d->bodyVolume = new SoVolumeData;
	d->bodyVolume->setReader(*bodyReader, TRUE);

	std::cout << "Save static body volumes" << std::endl;
		
    auto labeling = new SoLabelingProcessing;
	labeling->inObjectImage.connectFrom(&th3->outBinaryImage);

	std::cout << "Label separted branches" << std::endl;

	SoRef<SoCenterLineApproximation3d> centerlineExtractor = new SoCenterLineApproximation3d();
	//centerlineExtractor->inLabelImage = imageReader;
	centerlineExtractor->inLabelImage.connectFrom(&labeling->outLabelImage);
	centerlineExtractor->autoMode = true;
	centerlineExtractor->newBranchSensibility = 4;
	centerlineExtractor->distanceMapSmoothing = 0.01;
	centerlineExtractor->smoothingIteration = 2;

	SoRef<SoIndexedLineSet> clLineSet = centerlineExtractor->outLineSet.getValue();

	std::cout << "Center Line estimation finished" << std::endl;	
	
	SoRef<SoDrawStyle> drawStyle = new SoDrawStyle();
	drawStyle->lineWidth = 2;
	drawStyle->pointSize = 2;
	drawStyle->style = SoDrawStyle::Style::LINES;

	SoRef<SoLightModel> lightModel = new SoLightModel();
	lightModel->model = SoLightModel::BASE_COLOR;

	SoRef<SoMaterial> matl = new SoMaterial();
	matl->diffuseColor.setValue(0, 1, 0);
	matl->transparency = 0.5;

	auto lineSetSeparator = new SoSeparator;
	lineSetSeparator->addChild(drawStyle.ptr());
	lineSetSeparator->addChild(lightModel.ptr());
	lineSetSeparator->addChild(matl.ptr());
	lineSetSeparator->addChild(clLineSet.ptr());

	//visualize skeleton
	d->root->addChild(lineSetSeparator);

	//line-based end point
	auto vp = static_cast<SoVertexProperty*>(clLineSet->vertexProperty.getValue());
	auto num_vert = vp->vertex.getNum();
	auto num_line = clLineSet->coordIndex.getNum();

	QList<int> callCount;
	QList<bool> notAdded;
	for (auto i = 0; i < num_vert; i++) {
		callCount.append(0);
		notAdded.append(true);
	}	
	for (auto i = 0; i < num_line; i++) {
		auto lidx = clLineSet->coordIndex.getValues(0)[i];
		if (lidx != -1) {
			callCount[lidx]++;
		}
	}

	QList<SbVec3f> epSet;
	QList<int> epIdxSet;
	QList<int> epLineSet;
	auto firstidx = clLineSet->coordIndex.getValues(0)[0];
	if (callCount[firstidx] == 1) {
		auto vt = vp->vertex.getValues(0)[firstidx];
		epSet.push_back(vt);
		epIdxSet.push_back(firstidx);
		epLineSet.push_back(0);
		notAdded[firstidx] = false;
	}
	for (auto l = 1; l < num_line; l++) {
		auto lidx = clLineSet->coordIndex.getValues(0)[l];
		if (lidx == -1) {
			auto prevL = clLineSet->coordIndex.getValues(0)[l - 1];
			auto nextL = clLineSet->coordIndex.getValues(0)[l + 1];
			if (prevL < num_vert) {
				if (callCount[prevL] == 1 && notAdded[prevL]) {
					auto prev_vt = vp->vertex.getValues(0)[prevL];
					epSet.push_back(prev_vt);
					epIdxSet.push_back(prevL);
					epLineSet.push_back(l - 1);
					notAdded[prevL] = false;
				}
			}
			if (nextL < num_vert) {
				if (callCount[nextL] == 1 && notAdded[nextL]) {
					auto next_vt = vp->vertex.getValues(0)[nextL];
					epSet.push_back(next_vt);
					epIdxSet.push_back(nextL);
					epLineSet.push_back(l + 1);
					notAdded[nextL] = false;
				}
			}
		}
	}		

	for (auto e = 0; e < epSet.count(); e++) {
		auto ep = epSet[e];
		auto sph = new SoSphere;
		sph->setName(QString("Sph%1").arg(e+1).toStdString().c_str());
		sph->radius.setValue(0.3f);
		auto trans = new SoTranslation;
		trans->setName(QString("SphTrans%1").arg(e+1).toStdString().c_str());
		trans->translation.setValue(ep);
		auto sep = new SoSeparator;
		sep->addChild(trans);
		auto sphMatl = new SoMaterial;
		sphMatl->setName(QString("SphMatl%1").arg(e+1).toStdString().c_str());
		sphMatl->diffuseColor.setValue(0.4f, 0.4f, 0.8f);
		sep->addChild(sphMatl);
		sep->addChild(sph);
		d->handleSeparator->addChild(sep);
	}

	std::cout << "find end points" << std::endl;

	//make array of skeleton points (using image dimension)    
	//for each end point create trajectory
	//from end point make trajectories until branching point appears
	//(at least two adjacent filled voxel exist        

	//find branch point
	QList<int32_t> branch_idx;
	for (auto i = 0; i < num_line - 1; i++) {
		if (clLineSet->coordIndex.getValues(0)[i + 1] == -1) {
			branch_idx.push_back(clLineSet->coordIndex.getValues(0)[i]);
		}
	}

	d->trajSet.clear();
	d->traj_idx.clear();

	for (auto i = 0; i < epSet.size(); i++) {		
		auto l_idx = epLineSet[i];
		auto traj_size = 0;
		auto cur_l_idx = l_idx;
		QList<SbVec3f> traj;

		auto test_idx = clLineSet->coordIndex.getValues(0)[cur_l_idx + 1];
		if (test_idx == -1) {
			while (1) {//traval skeleton inverse order
				auto vp_idx = clLineSet->coordIndex.getValues(0)[cur_l_idx];
				/*if (vp_idx == -1) {
					break;
				}*/
				auto vvp = vp->vertex.getValues(0)[vp_idx];
				traj.push_back(vvp);
				traj_size++;
				cur_l_idx--;
				if (cur_l_idx < 0) {
					break;
				}
				auto is_break = false;
				for (auto br : branch_idx) {
					if (clLineSet->coordIndex.getValues(0)[cur_l_idx] == br) {
						is_break = true;
						break;
					}
				}
				if (is_break)
					break;
			}
		}
		else {
			while (1) {//traval skeleton in order
				auto vp_idx = clLineSet->coordIndex.getValues(0)[cur_l_idx];
				auto is_break = false;
				for (auto br : branch_idx) {
					if (vp_idx == br) {
						is_break = true;
						break;
					}
				}
				if (is_break)
					break;
				/*if (vp_idx == -1) {
					break;
				}*/
				auto vvp = vp->vertex.getValues(0)[vp_idx];
				traj.push_back(vvp);
				traj_size++;
				cur_l_idx++;
				if (cur_l_idx == num_line) {
					break;
				}
			}
		}
		d->trajSet.push_back(traj);
		d->traj_idx.push_back(-1);
	}
	std::cout << "Find trajectory of each branch" << std::endl;
}

auto Oiv3DDrawer::PerformBranch() -> void {
	std::cout << "Perform branch separation " << std::endl;
	//Create distance map using current binary mask
	QList<SbVec3f> maskVoxels;
	QList<SbVec3f> maskIdxs;
	//auto dim = d->targetVolume->getDimension();
	auto dim = d->branchVolume->getDimension();
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		d->branchVolume->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

	d->branchVolume->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	unsigned short* ptr = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_WRITE);

	for (auto k = 0; k < dim[2]; k++) {
		for (auto i = 0; i < dim[0]; i++) {
			for (auto j = 0; j < dim[1]; j++) {
				auto key = k * dim[0] * dim[1] + i * dim[1] + j;

				unsigned short val = *(ptr + key);
				SbVec3f indexf;
				indexf[0] = j;
				indexf[1] = i;
				indexf[2] = k;
				if (val > 0) {
					auto ep = d->branchVolume->voxelToXYZ(indexf);
					maskVoxels.push_back(ep);
					maskIdxs.push_back(indexf);
				}
			}
		}
	}
	std::cout << "Total " << maskVoxels.count() << " voxels in current mask" << std::endl;

	QList<std::tuple<int, int>> parentIdx;
	QList<bool> removing;
	for (auto i = 0; i < maskVoxels.count(); i++) {
		auto ep = maskVoxels[i];
		double min_dist = -1;
		int traj_idx = -1;
		int branch_idx = -1;
		for (auto j = 0; j < d->trajSet.size(); j++) {
			auto traj = d->trajSet[j];
			for (auto k = 0; k < traj.size(); k++) {
				auto vt = traj[k];
				double dist = sqrt((vt[0] - ep[0]) * (vt[0] - ep[0]) +
					(vt[1] - ep[1]) * (vt[1] - ep[1]) +
					(vt[2] - ep[2]) * (vt[2] - ep[2]));
				if (min_dist > dist || min_dist < 0.0) {
					min_dist = dist;
					traj_idx = j;
					branch_idx = k;
				}
			}
		}
		parentIdx.push_back(std::make_tuple(traj_idx, branch_idx));
		removing.push_back(false);
	}
	std::cout << "Parenting process finished " << std::endl;

	for (auto i = 0; i < maskVoxels.count(); i++) {
		auto trajID = std::get<0>(parentIdx[i]);
		auto branchID = std::get<1>(parentIdx[i]);
		if (d->traj_idx[trajID] > branchID) {
			removing[i] = true;
		}
	}
	std::cout << "Remove shrinked voxels" << std::endl;

	auto remove_count = 0;
	for (auto i = 0; i < maskVoxels.count(); i++) {
		if (removing[i]) {
			auto mask_idx = maskIdxs[i];

			int x = mask_idx[0];
			int y = mask_idx[1];
			int z = mask_idx[2];
			auto key = z * dim[0] * dim[1] + y * dim[0] + x;
			*(ptr + key) = 0;
			remove_count++;
		}
	}

	//fill body
	SoLDMDataAccess::DataInfoBox dataInfoBox2 =
		d->bodyVolume->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj2 = new SoCpuBufferObject;
	dataBufferObj2->setSize((size_t)dataInfoBox2.bufferSize);
	d->bodyVolume->getLdmDataAccess().getData(0, bBox, dataBufferObj2.ptr());
	unsigned short* ptr2 = (unsigned short*)dataBufferObj2->map(SoBufferObject::READ_WRITE);
	for(auto i=0;i<dim[0]*dim[1]*dim[2];i++) {
		auto bodyVal = *(ptr2 + i);
	    if(bodyVal>0) {
			*(ptr + i) = bodyVal;
	    }
	}
	dataBufferObj2->unmap();
	dataBufferObj->unmap();

	//modify branch
	int transactionId;
	d->targetVolume->startEditing(transactionId);          // Initiate edition
	d->targetVolume->editSubVolume(bBox, dataBufferObj.ptr());  // write the whole edited subvolume
	d->targetVolume->finishEditing(transactionId);
	d->targetVolume->saveEditing();
	
	d->branchVolume = nullptr;
	d->bodyVolume = nullptr;
	std::cout << remove_count << " voxels removed" << std::endl;
	
}


auto Oiv3DDrawer::PerformSeparation() -> void {
	//get min max from existing label;
	if (nullptr == d->targetVolume) {
		return;
	}
	if (nullptr == d->curGuideShape) {
		return;
	}
	/*auto size = d->targetVolume->getDimension();

	SoRef<SoVolumeData> copyVol = new SoVolumeData;
	copyVol->data.setValue(size, SbDataType::UNSIGNED_SHORT, d->targetVolume->data.getValue(), SoSFArray::COPY);
	copyVol->extent.setValue(d->targetVolume->extent.getValue());
	copyVol->touch();

	double mmin, mmax;
	copyVol->getMinMax(mmin, mmax);
	int new_label = mmax + 1;

	int editionId;
	copyVol->startEditing(editionId);
	copyVol->editSolidShape(d->curGuideShape, new_label);
	copyVol->finishEditing(editionId);
	copyVol->saveEditing();

	copyVol->data.touch();

	//copy original image and get threshold first
	//MedicalHelper::dicomAdjustVolume(d->targetVolume);
	SoRef<SoMemoryDataAdapter> oriAdap = MedicalHelper::getImageDataAdapter(d->targetVolume);
	SoRef<SoThresholdingProcessing> oriThresh = new SoThresholdingProcessing;
	oriThresh->inImage = oriAdap.ptr();
	oriThresh->thresholdLevel.setValue(1, static_cast<float>(INT_MAX));

	//perform erosion for threshold imaged
	SoRef<SoErosionCubeProcessing> ero = new SoErosionCubeProcessing;
	ero->inImage.connectFrom(&oriThresh->outBinaryImage);
	ero->elementSize = 10;

	//get modified adapter
	//MedicalHelper::dicomAdjustVolume(copyVol);
	SoRef<SoMemoryDataAdapter> modiAdap = MedicalHelper::getImageDataAdapter(copyVol.ptr());
	SoRef<SoMaskImageProcessing> masking = new SoMaskImageProcessing;
	masking->inImage = modiAdap.ptr();
	masking->inBinaryImage.connectFrom(&ero->outImage);

	SoRef<SoConvertImageProcessing> conv = new SoConvertImageProcessing;
	conv->inImage.connectFrom(&masking->outImage);
	conv->dataType = SoConvertImageProcessing::DataType::LABEL;

	SoRef<SoExpandLabelsProcessing> expand = new SoExpandLabelsProcessing;
	expand->inLabelImage.connectFrom(&conv->outImage);
	expand->expandMode = SoExpandLabelsProcessing::ExpandMode::REGIONS;

	SoRef< SoConvertImageProcessing> iconv = new SoConvertImageProcessing;
	iconv->inImage.connectFrom(&expand->outLabelImage);
	iconv->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

	SoRef<SoMaskImageProcessing> finalMask = new SoMaskImageProcessing;
	finalMask->inImage.connectFrom(&iconv->outImage);
	finalMask->inBinaryImage.connectFrom(&oriThresh->outBinaryImage);

	SoRef<SoConvertImageProcessing> labelConv = new SoConvertImageProcessing;
	labelConv->dataType = SoConvertImageProcessing::DataType::LABEL;
	labelConv->inImage.connectFrom(&finalMask->outImage);

	SoRef<SoConvertImageProcessing> thConv = new SoConvertImageProcessing;
	thConv->inImage.connectFrom(&oriThresh->outBinaryImage);
	thConv->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

	SoRef<SoMarkerBasedWatershedProcessing> water = new SoMarkerBasedWatershedProcessing;
	water->outputMode = SoMarkerBasedWatershedProcessing::OutputMode::SEPARATED_BASINS;
	water->precisionMode = SoMarkerBasedWatershedProcessing::PrecisionMode::FAST;
	water->inGrayImage.connectFrom(&thConv->outImage);
	water->inMarkerImage.connectFrom(&labelConv->outImage);

	SoRef<SoMaskImageProcessing> fmasking = new SoMaskImageProcessing;
	fmasking->inImage.connectFrom(&water->outObjectImage);
	fmasking->inBinaryImage.connectFrom(&oriThresh->outBinaryImage);

	SoRef<SoVRImageDataReader> outSingleReader = new SoVRImageDataReader;
	outSingleReader->imageData.connectFrom(&fmasking->outImage);

	d->targetVolume->setReader(*outSingleReader, TRUE);
	d->targetVolume->data.touch();*/
	//remove guide frustum
	d->shape3DSeparator->replaceChild(1, new SoSeparator);	
	d->curGuideShape = nullptr;
}

auto Oiv3DDrawer::DrawFreeLineCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();
		
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}
	//add extrude shape
	std::vector<SbVec2f> lineInCam(lineDrawer->point.getNum());
	for (unsigned int i = 0; i < lineInCam.size(); ++i) {
		lineInCam[i].setValue(lineDrawer->point[i][0], lineDrawer->point[i][1]);
	}

	SoSearchAction searchAction;
	searchAction.setNode(d->targetVolume);
	searchAction.apply(d->sceneRoot);
	SoPath* pathToExtrudedShapeRoot = searchAction.getPath();

	SbBox3f bbox = d->targetVolume->extent.getValue();

	SoShape* extrudedShape = SbExtrusionGenerator::createFrom2DPoints(lineInCam,
		pathToExtrudedShapeRoot,
		SoViewVolumeElement::get(action->getState()),
		bbox);
	d->curGuideShape = extrudedShape;
	d->curGuideShape->setName("GuideFrustum");
	if (extrudedShape == NULL)
	{
		std::cout << "failed to extrude shape" << std::endl;
		lineDrawer->clear();
		return;
	}

	d->shape3DSeparator->replaceChild(1, extrudedShape);

	lineDrawer->clear();

	action->setHandled();
}

auto Oiv3DDrawer::SaveEditings() -> void {
	d->targetVolume->saveEditing();
}
