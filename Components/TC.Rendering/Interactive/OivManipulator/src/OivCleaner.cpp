#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>

#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/SoPickedPoint.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#pragma warning(pop)

#include "Oiv2DDrawer.h"
#include "OivCleaner.h"

constexpr char* maskVolumeDataNodeName{ "MaskVolume" };
constexpr char* maskSliceNodeName{ "MaskSlice" };

struct OivCleaner::Impl {
    MedicalHelper::Axis axis{MedicalHelper::AXIAL};
    int mode;//0: flush, 1: flush selection, 2: clean, 3 clean selection
    SoRef<SoVolumeData> tempSlice{nullptr};

    int cur_idx{-1};    
    SbVec3f cur_seed{0,0,0};

    Oiv2DDrawer* pPointer;

    auto HandleClean()->void;
    auto HandleCleanSelection()->void;
    auto HandleFlush()->void;
    auto HandleFlushSelection()->void;

    auto CopyCurrentSlice()->SoVolumeData*;
    auto GetLabelValue()->int;
    auto GetMaskVolume()->SoVolumeData*;
    auto GetSliceNumber()->uint32_t;
};

auto OivCleaner::Impl::CopyCurrentSlice() -> SoVolumeData* {
     //Copy current slice 
    auto vol = GetMaskVolume();
    if(nullptr == vol) {
        return nullptr;
    }    

    cur_idx = GetSliceNumber();
    tempSlice = new SoVolumeData;
    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
    SbBox3i32 bBox;    
    auto dim = vol->getDimension();
    auto ext = vol->extent.getValue();
    auto voxelSize = vol->getVoxelSize();
    
    if (axis == MedicalHelper::AXIAL) {
        bBox = SbBox3i32(SbVec3i32(0, 0, cur_idx), SbVec3i32(dim[0] - 1, dim[1] - 1, cur_idx));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox,(SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(dim[0], dim[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2] * cur_idx,
            ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2] * (cur_idx + 1));
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }else if(axis == MedicalHelper::CORONAL) {
        bBox = SbBox3i32(SbVec3i32(0, cur_idx, 0), SbVec3i32(dim[0] - 1, cur_idx, dim[2]-1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(dim[0], 1, dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1]*cur_idx, ext.getMin()[2],
            ext.getMax()[0], ext.getMin()[1] + voxelSize[1]*(cur_idx+1), ext.getMax()[2]);
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }else if(axis == MedicalHelper::SAGITTAL) {
        bBox = SbBox3i32(SbVec3i32(cur_idx, 0, 0), SbVec3i32(cur_idx, dim[1] - 1, dim[2]-1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(1, dim[1], dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0] + voxelSize[0]*cur_idx, ext.getMin()[1], ext.getMin()[2],
            ext.getMin()[0] + voxelSize[0]*(cur_idx+1), ext.getMax()[1], ext.getMax()[2]);
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }
    dataBufferObj->unmap();

    return tempSlice.ptr();
}

auto OivCleaner::Impl::GetMaskVolume() -> SoVolumeData* {
    return dynamic_cast<SoVolumeData*>(SoNode::getByName(maskVolumeDataNodeName));
}

auto OivCleaner::Impl::GetLabelValue() -> int {
    auto volData = GetMaskVolume();
    auto idx = volData->XYZToVoxel(cur_seed);
    auto pos = SbVec3i32(static_cast<int32_t>(idx[0]), static_cast<int32_t>(idx[1]), static_cast<int32_t>(idx[2]));
    auto cur_label = static_cast<int>(volData->getValue(pos));
    return cur_label;
}

auto OivCleaner::Impl::HandleClean() -> void {
    auto volData = GetMaskVolume();
    if(nullptr == volData) {
        return;
    }    
    auto slice_num = GetSliceNumber();
    SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
    SbBox3i32 tBox;
    auto size = volData->getDimension();
    if(axis == MedicalHelper::AXIAL) {        
        tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
        tBufferObj->setSize((size_t)tInfoBox.bufferSize);
        volData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
        auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));
        for(auto i=0;i<size[0]*size[1];i++) {
            targetdata[i] = 0;
        }
    }else if(axis == MedicalHelper::CORONAL) {
        size[1] = 1;
        tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2]-1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
        tBufferObj->setSize((size_t)tInfoBox.bufferSize);
        volData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
        auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));
        for(auto i=0;i<size[0]*size[2];i++) {
            targetdata[i] = 0;
        }
    }else if(axis == MedicalHelper::SAGITTAL) {
        size[2] =1;
        tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2]-1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
        tBufferObj->setSize((size_t)tInfoBox.bufferSize);
        volData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
        auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));
        for(auto i=0;i<size[1]*size[2];i++) {
            targetdata[i] = 0;
        }
    }
    int editionId;
    volData->startEditing(editionId);
    volData->editSubVolume(tBox,tBufferObj.ptr());
    volData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);    
    tBufferObj->unmap();
}

auto OivCleaner::Impl::HandleCleanSelection() -> void {
    auto cur_label = GetLabelValue();
    if(cur_label <= 0) {
        return;
    }
    auto tSlice = CopyCurrentSlice();
    //MedicalHelper::dicomAdjustVolume(tSlice);
    SoRef<SoMemoryDataAdapter> tempAdapter = MedicalHelper::getImageDataAdapter(tSlice);
    tempAdapter->ref();
    tempAdapter->interpretation = SoMemoryDataAdapter::Interpretation::VALUE;

    SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;
    thresh->inImage = tempAdapter.ptr();
    thresh->thresholdLevel.setValue(cur_label,cur_label+0.5);

    SoRef<SoLogicalNotProcessing> not = new SoLogicalNotProcessing;
    not->inImage.connectFrom(&thresh->outBinaryImage);

    SoRef<SoMaskImageProcessing> mask = new SoMaskImageProcessing;
    mask->inImage = tempAdapter.ptr();
    mask->inBinaryImage.connectFrom(&not->outImage);

    SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
    reader->imageData.connectFrom(&mask->outImage);

    SoRef<SoVolumeData> cleanedSlice = new SoVolumeData;
    cleanedSlice->setReader(*reader);

    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
    SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
    SbBox3i32 tBox;
    auto size = tempSlice->getDimension();
    auto slice_num = GetSliceNumber();
    SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		cleanedSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	cleanedSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

    auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
    //apply changed slice to current slice
    auto volumeData = GetMaskVolume();    
    if(axis == MedicalHelper::AXIAL) {
        tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox = volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::CORONAL) {
        tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::SAGITTAL) {
        tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

        memcpy(targetdata,sourcedata,dataBufferObj->getSize());
    }

    int editionId;
    volumeData->startEditing(editionId);
    volumeData->editSubVolume(tBox, tBufferObj.ptr());
    volumeData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);
    tBufferObj->unmap();
    dataBufferObj->unmap();    
}

auto OivCleaner::Impl::HandleFlush() -> void {
    auto volData = GetMaskVolume();
    if(nullptr == volData) {
        return;
    }        
    SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
    SbBox3i32 tBox;
    auto size = volData->getDimension();
    tBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2]-1));
    SoLDMDataAccess::DataInfoBox tInfoBox =
			volData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
    tBufferObj->setSize((size_t)tInfoBox.bufferSize);
    volData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
    auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

    //create empty array filled with 0
    std::shared_ptr<unsigned short> zeros(new unsigned short[size[0]*size[1]*size[2]](),std::default_delete<unsigned short[]>());
    memcpy(targetdata,zeros.get(),tBufferObj->getSize());

    int editionId;
    volData->startEditing(editionId);
    volData->editSubVolume(tBox,tBufferObj.ptr());
    volData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);
    tBufferObj->unmap();
    zeros = nullptr;
}

auto OivCleaner::Impl::HandleFlushSelection() -> void {
    auto cur_label = GetLabelValue();
    if(cur_label <= 0) {
        return;
    }


    auto volData = GetMaskVolume();
    //MedicalHelper::dicomAdjustVolume(volData);
    SoRef<SoMemoryDataAdapter> tempAdapter = MedicalHelper::getImageDataAdapter(volData);
    tempAdapter->ref();
    tempAdapter->interpretation = SoMemoryDataAdapter::Interpretation::VALUE;

    SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
    threshold->inImage = tempAdapter.ptr();
    threshold->thresholdLevel.setValue(cur_label,cur_label+0.5);

    SoRef<SoLogicalNotProcessing> not = new SoLogicalNotProcessing;
    not->inImage.connectFrom(&threshold->outBinaryImage);

    SoRef<SoMaskImageProcessing> mask = new SoMaskImageProcessing;
    mask->inImage = tempAdapter.ptr();
    mask->inBinaryImage.connectFrom(&not->outImage);

    SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
    reader->imageData.connectFrom(&mask->outImage);

    SoRef<SoVolumeData> cleanedVolume = new SoVolumeData;
    cleanedVolume->setReader(*reader);
        
    SoRef<SoCpuBufferObject> sBufferObj = new SoCpuBufferObject;
    SbBox3i32 tBox;
    auto size = volData->getDimension();

    tBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2]-1));
    SoLDMDataAccess::DataInfoBox tInfoBox =
        volData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);    

    sBufferObj->setSize((size_t)tInfoBox.bufferSize);
    cleanedVolume->getLdmDataAccess().getData(0,tBox,sBufferObj.ptr());
    //auto sourceData = static_cast<unsigned short*>(sBufferObj->map(SoBufferObject::READ_ONLY));        

    int editionId;
    volData->startEditing(editionId);
    volData->editSubVolume(tBox, sBufferObj.ptr());
    volData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);
    //sBufferObj->unmap();
    sBufferObj = nullptr;
}

OivCleaner::OivCleaner(Oiv2DDrawer* parent) : d{new Impl} {
    d->pPointer = parent;
    SoEventCallback* eventCallback = new SoEventCallback;
    eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(),HandleMouseButtonEvent,d.get());
    this->addChild(eventCallback);
}

OivCleaner::~OivCleaner() {
    
}


auto OivCleaner::Impl::GetSliceNumber() -> uint32_t {
    uint32_t sliceNumber = 0;

    // get slice number
    SoNodeList foundNodes;
    auto foundNodeCount = getByName(maskSliceNodeName, foundNodes);
    if(foundNodeCount>-1){
        for (auto i = 0; i < foundNodeCount; i++) {
            auto orthoSlice = dynamic_cast<SoOrthoSlice*>(foundNodes[i]);
            if (orthoSlice && (axis == orthoSlice->axis.getValue())) {
                sliceNumber = orthoSlice->sliceNumber.getValue();
                break;
            }
        }
    }
    return sliceNumber;
}

void OivCleaner::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
    auto action = node->getAction();

    auto d = static_cast<Impl*>(data);
    if(nullptr == d) {
        action->setHandled();
        return;
    }
    const SoEvent* event = node->getEvent();
     auto viewportRegion = action->getViewportRegion();
    auto mousePos =  event->getPosition(viewportRegion);
    SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
    rayPick.setPoint(mousePos);
	rayPick.apply(action->getPickRoot());

    SoPickedPoint* pickedPt = rayPick.getPickedPoint();
    if(pickedPt){
        if(SO_MOUSE_PRESS_EVENT(event,BUTTON1)) {
            d->cur_seed = pickedPt->getPoint();
            d->GetLabelValue();
            switch(d->mode) {
            case 0:
                d->HandleFlush();
                break;
            case 1:
                d->HandleFlushSelection();
                break;
            case 2:
                d->HandleClean();
                break;
            case 3:
                d->HandleCleanSelection();
                break;
            }
        }else if(SO_MOUSE_RELEASE_EVENT(event,BUTTON1)) {
            d->cur_seed.setValue(0.0,0.0,0.0);
        }
    }
    action->setHandled();
}

auto OivCleaner::SetAxis(const MedicalHelper::Axis& axis) -> void {
    d->axis = axis;
}

auto OivCleaner::SetMode(const int& mode) -> void {
    d->mode = mode;
}
