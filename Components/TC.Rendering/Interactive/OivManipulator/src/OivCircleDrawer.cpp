#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#pragma warning(pop)

#include "OivCircleDrawer.h"

SO_NODE_SOURCE(OivCircleDrawer);

struct OivCircleDrawer::Impl {
    SbVec2f firstAddedPoint;
    SbVec2f firstPoint;
    SbVec2f lastAddedPoint;
    bool mouse_is_pressed{ false };
    SoSeparator* handleRoot{ nullptr };
    bool activated = false;
    bool in_process = false;
};

OivCircleDrawer::OivCircleDrawer() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivCircleDrawer);
    SO_NODE_ADD_FIELD(numberOfPoints, (100));

    simplificationThreshold.enableNotify(FALSE);
    this->color.setValue(0, 1, 1);
    this->lineWidth = 3;
    this->simplificationThreshold.setValue(0);
    this->isClosed = TRUE;
    simplificationThreshold.enableNotify(TRUE);
}

OivCircleDrawer::~OivCircleDrawer() {

}

auto OivCircleDrawer::GetStartPt()->SbVec2f {
    return d->firstAddedPoint;
}

auto OivCircleDrawer::GetEndPt()->SbVec2f {
    return d->lastAddedPoint;
}

auto OivCircleDrawer::setHandleRoot(SoSeparator* root)->void {
    d->handleRoot = root;
}

auto OivCircleDrawer::getHandleRoot()->SoSeparator* {
    return d->handleRoot;
}

void OivCircleDrawer::Activate() {
    d->activated = true;
}

void OivCircleDrawer::Deactivate() {
    d->activated = false;
}

void OivCircleDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivCircleDrawer, "CircleDrawer", SoPolyLineScreenDrawer);
}

void OivCircleDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivCircleDrawer);
}

void OivCircleDrawer::reset() {

}

void OivCircleDrawer::onMouseDown(SoHandleEventAction* action) {
    if (!d->activated) return;
    d->mouse_is_pressed = true;
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    d->firstPoint = action->getEvent()->getPositionFloat();
    d->firstAddedPoint = pos;
}

void OivCircleDrawer::onMouseDragging(SoHandleEventAction* action) {
    if (!d->activated) return;
    this->clear();

    auto eventPosition = action->getEvent()->getPositionFloat();
    auto centerPoint = (d->firstPoint + eventPosition) / 2;

    auto xdiff = centerPoint[0] - d->firstPoint[0];
    auto ydiff = centerPoint[1] - d->firstPoint[1];
    auto radius = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
    float div = static_cast<float>(numberOfPoints.getValue());
    for (auto i = 0; i < numberOfPoints.getValue(); i++) {
        auto x_pos = radius * cos(2 * M_PI / div * i);
        auto y_pos = radius * sin(2 * M_PI / div * i);
        SbVec2s radiusPt;
        radiusPt[0] = centerPoint[0] + x_pos;
        radiusPt[1] = centerPoint[1] + y_pos;

        auto normPos = action->getViewportRegion().normalize(radiusPt);
        auto screenPos = SbVec2f(normPos[0] * 2.0 - 1.0, normPos[1] * 2.0 - 1.0);
        this->addPoint(screenPos);
    }

    d->in_process = true;
}

void OivCircleDrawer::onMouseUp(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->in_process) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        d->mouse_is_pressed = false;
        d->lastAddedPoint = pos;
        d->in_process = false;

        finalizeLine(action);
        this->clear();
    }
}

void OivCircleDrawer::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}