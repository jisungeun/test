#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#pragma warning(pop)

#include "OivHoveringLasso.h"

SO_NODE_SOURCE(OivHoveringLasso);

struct OivHoveringLasso::Impl {
    SbVec2s lastAddedPoint;
    bool indrawing{ false };
    QList<SbVec3f> pt_list;
};

OivHoveringLasso::OivHoveringLasso() : d{ new Impl } {    
    SO_NODE_CONSTRUCTOR(OivHoveringLasso);
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold.setValue(1);
    simplificationThreshold.enableNotify(TRUE);
    isClosed.enableNotify(FALSE);
    this->isClosed.setValue(TRUE);
    isClosed.enableNotify(TRUE);
}

OivHoveringLasso::~OivHoveringLasso() {

}

void OivHoveringLasso::initClass() {
    SO__NODE_INIT_CLASS(OivHoveringLasso, "HoveringLasso", SoPolyLineScreenDrawer);
}

void OivHoveringLasso::exitClass() {
    SO__NODE_EXIT_CLASS(OivHoveringLasso);
}

void OivHoveringLasso::reset() {

}

void OivHoveringLasso::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivHoveringLasso::onMouseDown(SoHandleEventAction* action) {
    if (false == d->indrawing) {        
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        this->addPoint(pos);
        d->indrawing = true;
    }else {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        d->lastAddedPoint = SbVec2s(pos[0], pos[1]);
        this->addPoint(pos);                
        finalizeLine(action);
        d->indrawing = false;
        this->clear();
    }
}

void OivHoveringLasso::onMouseMove(SoHandleEventAction* action) {
    if (d->indrawing) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        this->addPoint(pos);
    }
}

void OivHoveringLasso::onMouseUp(SoHandleEventAction* action) {
    
}

void OivHoveringLasso::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}