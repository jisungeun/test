#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#pragma warning(pop)

#include "OivAngleDrawer.h"

SO_NODE_SOURCE(OivAngleDrawer);
struct OivAngleDrawer::Impl {
    int index = 0;
    bool mouse_is_pressed{ false };
    SoSeparator* handleRoot{ nullptr };
    bool activated = false;
};

OivAngleDrawer::OivAngleDrawer() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivAngleDrawer);
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold.setValue(0);
    this->lineWidth = 3;
    this->color.setValue(1, 1, 0);
    this->isClosed = FALSE;
    simplificationThreshold.enableNotify(TRUE);
}

OivAngleDrawer::~OivAngleDrawer() {

}
void OivAngleDrawer::Activate() {
    d->activated = true;
}

void OivAngleDrawer::Deactivate() {
    d->activated = false;
}

auto OivAngleDrawer::setHandleRoot(SoSeparator* root)->void {
    d->handleRoot = root;
}

auto OivAngleDrawer::getHandleRoot()->SoSeparator* {
    return d->handleRoot;
}

void OivAngleDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivAngleDrawer, "AngleDrawer", SoPolyLineScreenDrawer);
}

void OivAngleDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivAngleDrawer);
}

void OivAngleDrawer::reset() {
    this->clear();
    d->index = 0;
    d->mouse_is_pressed = false;    
}

void OivAngleDrawer::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivAngleDrawer::onMouseDown(SoHandleEventAction* action) {
    if (!d->activated) return;
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    this->removePoint(d->index);
    if(this->point.getNum()>0) {
        auto prev_point = this->point.getValues(0)[this->point.getNum() - 1];
        auto pX = prev_point[0];
        auto pY = prev_point[1];
        if (sqrt(pow(pX - pos[0], 2) + pow(pY - pos[1], 2)) <0.001 ) {
            return;
        }
    }
    this->addPoint(pos);
    d->index++;

    d->mouse_is_pressed = true;
}

void OivAngleDrawer::onMouseMove(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->mouse_is_pressed && d->index < 3) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);

        this->removePoint(d->index);
        this->addPoint(pos);
    }
}

void OivAngleDrawer::onMouseUp(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->index == 3) {
        finalizeLine(action);
        d->index = 0;
        d->mouse_is_pressed = false;
        this->clear();
    }
}

void OivAngleDrawer::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}