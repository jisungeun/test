#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#pragma warning(pop)

#include "OivLineDrawer.h"

SO_NODE_SOURCE(OivLineDrawer);
struct OivLineDrawer::Impl {
    SbVec2f firstAddedPoint;
    SbVec2f lastAddedPoint;
    bool mouse_is_pressed{ false };
    bool activated = false;
    bool in_process = false;
};

OivLineDrawer::OivLineDrawer() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivLineDrawer);
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold.setValue(0);
    this->lineWidth = 3;
    this->color.setValue(1, 1, 0);
    this->isClosed = FALSE;
    simplificationThreshold.enableNotify(TRUE);
}

OivLineDrawer::~OivLineDrawer() {

}

void OivLineDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivLineDrawer, "LineDrawer", SoPolyLineScreenDrawer);
}

void OivLineDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivLineDrawer);
}

void OivLineDrawer::reset() {
    this->clear();
    d->mouse_is_pressed = false;
    d->in_process = false;
}

void OivLineDrawer::Activate() {
    d->activated = true;
}

void OivLineDrawer::Deactivate() {
    d->activated = false;
}

void OivLineDrawer::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivLineDrawer::onMouseDown(SoHandleEventAction* action) {
    if (!d->activated) return;

    if (this->point.getNum() < 1) {
        d->mouse_is_pressed = true;
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        d->firstAddedPoint = pos;
        this->addPoint(pos);        
    }
}

void OivLineDrawer::onMouseMove(SoHandleEventAction* action) {
    if (!d->activated) return;

    if (d->mouse_is_pressed) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        this->removePoint(1);
        this->addPoint(pos);
        d->in_process = true;
    }
}

void OivLineDrawer::onMouseUp(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->in_process) {
        finalizeLine(action);
        d->mouse_is_pressed = false;
        d->in_process = false;
        this->clear();        
    }
}

void OivLineDrawer::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}