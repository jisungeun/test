#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#pragma warning(pop)

#include "OivRectangleDrawer.h"

#include "RoiCropSep.h"

namespace TC {
    struct RoiCropSep::Impl {
        SoSeparator* root;
        SoSeparator* handleRoot{ nullptr };
        SoSwitch* drawerSwitch{ nullptr };
        OivRectangleDrawer* rect{ nullptr };
        SoSwitch* callbackSwitch{ nullptr };
        SoEventCallback* callback{ nullptr };

        double handleRadius{ 0.5 };
        bool isInHandle{ false };
        int rectIdx{ -1 };
        int handleIdx{ -1 };

        QList<SbVec3f> rectPoints[4];
        QList<SoMaterial*> handleMatl[4];
        QList<SoTranslation*> handleTrans[4];
        QList<SoVertexProperty*> rectProp;
        QList<SoDrawStyle*> rectStyle;
        QList<SoMaterial*> rectColor;

        RoiCropSep* thisPointer;
    };
    RoiCropSep::RoiCropSep(QObject* parent) : QObject(parent), d{ new Impl }{
        Init();
        d->thisPointer = this;
    }
    RoiCropSep::~RoiCropSep() {
        
    }    
    auto RoiCropSep::Init() -> void {
        d->rect = new OivRectangleDrawer;
        d->root = new SoSeparator;
        d->handleRoot = new SoSeparator;
        SoRef<SoLightModel> lightModel = new SoLightModel;
        lightModel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightModel.ptr());

        d->drawerSwitch = new SoSwitch;
        d->drawerSwitch->addChild(d->rect);
        d->drawerSwitch->whichChild = -1;

        d->root->addChild(d->drawerSwitch);
        d->root->addChild(d->handleRoot);

        d->rect->setUserData(d.get());
        d->rect->onFinish.add(rectCallback);

        d->callback = new SoEventCallback;
        d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), rectHandleCB, d.get());
        d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), rectMoveCB, d.get());

        d->callbackSwitch = new SoSwitch;
        d->callbackSwitch->addChild(d->callback);
        d->callbackSwitch->whichChild = -1;

        d->root->insertChild(d->callbackSwitch, 0);
    }
    auto RoiCropSep::RemoveROI(int idx) -> void {
        if(d->rectProp.count() <=idx) {
            return;
        }
        //remove scene graph first

        d->handleRoot->removeChild(idx + 1);//first child is SoLightModel

        d->rectProp.removeAt(idx);
        d->rectStyle.removeAt(idx);
        d->rectColor.removeAt(idx);
        for(auto i=0;i<4;i++) {
            d->rectPoints[i].removeAt(idx);
            d->handleMatl[i].removeAt(idx);
            d->handleTrans[i].removeAt(idx);
        }
    }
    auto RoiCropSep::GetRoot() -> SoSeparator* {
        return d->root;
    }
    auto RoiCropSep::GetROIs() -> QList<SoVertexProperty*> {
        return d->rectProp;
    }
    auto RoiCropSep::SetFocus(int idx) -> void {
        //clear focus first
        for(auto i=0;i<d->rectStyle.count();i++) {
            d->rectStyle[i]->lineWidth = 2;
            d->rectColor[i]->ambientColor.setValue(1, 1, 0);
            d->rectColor[i]->diffuseColor.setValue(1, 1, 0);            
        }
        if(d->rectStyle.count() <=idx || idx == -1) {
            return;
        }
        d->rectStyle[idx]->lineWidth = 3;
        d->rectColor[idx]->ambientColor.setValue(0, 1, 0);
        d->rectColor[idx]->diffuseColor.setValue(0, 1, 0);
    }
    auto RoiCropSep::ToggleAdd(bool activate) -> void {
        if (activate) {
            d->drawerSwitch->whichChild = 0;
        }
        else {
            d->drawerSwitch->whichChild = 1;
        }
    }
    auto RoiCropSep::ToggleModify(bool activate) -> void {
        if(activate) {
            d->callbackSwitch->whichChild = 0;
        }else {
            d->callbackSwitch->whichChild = 1;
        }
    }

    auto RoiCropSep::ClearROIs() -> void {
        d->handleRoot->removeAllChildren();
        SoRef<SoLightModel> lightModel = new SoLightModel;
        lightModel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightModel.ptr());
        for(auto i=0;i<4;i++) {
            d->rectPoints[i].clear();
            d->handleMatl[i].clear();
            d->handleTrans[i].clear();            
        }
        d->rectProp.clear();
        d->rectColor.clear();
        d->rectStyle.clear();
    }
    auto RoiCropSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion) -> std::vector<SbVec3f> {
        std::vector<SbVec3f> result;

        SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
        for (int i = 0; i < source->getNum(); i++) {

            SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
            normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
            normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

            rayPick.setNormalizedPoint(normalizedPoint);
            rayPick.apply(targetNode);

            SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
            if (pickedPoint) {
                result.push_back(pickedPoint->getPoint());
            }
        }
        return result;
    }    
    void RoiCropSep::rectCallback(SoPolyLineScreenDrawer::EventArg& arg) {
        OivRectangleDrawer* source = (OivRectangleDrawer*)arg.getSource();
        SoHandleEventAction* action = arg.getAction();
        auto dd = static_cast<Impl*>(source->getUserData());        
        if(source->point.getNum()<1) {
            source->clear();
            return;
        }        
        if(source->point.getNum() != 4) {
            source->clear();
            return;
        }
        auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());        
        if(resultRayCast.size() != 4) {
            source->clear();
            return;
        }

        //Add Rectangle
        SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
        drawStyle->lineWidth = 2;
        dd->rectStyle.push_back(drawStyle.ptr());
        SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
        for(auto i=0;i<4;i++) {
            vertexProp->vertex.set1Value(i, resultRayCast[i]);
            dd->rectPoints[i].push_back(resultRayCast[i]);
        }        
        vertexProp->vertex.set1Value(4, resultRayCast[0]);

        dd->rectProp.push_back(vertexProp.ptr());

        SoRef<SoLineSet> rect = new SoLineSet;
        rect->vertexProperty = vertexProp.ptr();

        SoRef<SoMaterial> lineMatl = new SoMaterial;
        lineMatl->ambientColor.setValue(1, 1, 0);
        lineMatl->diffuseColor.setValue(1, 1, 0);
        dd->rectColor.push_back(lineMatl.ptr());
        SoRef<SoSeparator> rectSep = new SoSeparator;
        rectSep->addChild(drawStyle.ptr());
        rectSep->addChild(lineMatl.ptr());
        rectSep->addChild(rect.ptr());

        SoRef<SoVertexProperty> circleProp = new SoVertexProperty;
        auto radius = dd->handleRadius;
        auto div = 100.0;
        for (auto i = 0; i < 100; i++) {
            auto x_pos = radius * cos(2 * M_PI / div * i);
            auto y_pos = radius * sin(2 * M_PI / div * i);
            circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
        }
        SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
        circleStyle->setName("circleStyle");
        circleStyle->style = SoDrawStyle::FILLED;

        SoRef<SoFaceSet> faceSet = new SoFaceSet;
        faceSet->vertexProperty = circleProp.ptr();

        SoRef<SoSeparator> circleSep = new SoSeparator;
        circleSep->addChild(circleStyle.ptr());

        for(auto i=0;i<4;i++) {
            SoRef<SoSeparator> circle = new SoSeparator;
            circleSep->addChild(circle.ptr());
            SoRef<SoMaterial> circleMatl = new SoMaterial;
            circleMatl->ambientColor.setValue(1, 0, 0);
            circleMatl->diffuseColor.setValue(1, 0, 0);
            SoRef<SoTranslation> circleTrans = new SoTranslation;
            circleTrans->translation.setValue(resultRayCast[i]);
            circle->addChild(circleMatl.ptr());
            circle->addChild(circleTrans.ptr());
            circle->addChild(faceSet.ptr());

            dd->handleTrans[i].push_back(circleTrans.ptr());
            dd->handleMatl[i].push_back(circleMatl.ptr());                        
        }

        SoRef<SoSeparator> roiSep = new SoSeparator;
        roiSep->addChild(circleSep.ptr());
        roiSep->addChild(rectSep.ptr());

        dd->handleRoot->addChild(roiSep.ptr());

        emit dd->thisPointer->roiAdded();
    }    
    void RoiCropSep::rectHandleCB(void* pImpl, SoEventCallback* eventCB) {
        auto doubleComp = [](double left, double right)->bool {
            return std::fabs(left - right) < 0.00001;
        };
        auto dd = static_cast<Impl*>(pImpl);
        const SoEvent* event = eventCB->getEvent();
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
        if(SoMouseButtonEvent::isButtonPressEvent(mouseButton,SoMouseButtonEvent::BUTTON1)) {
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

            SoRayPickAction pickAction = SoRayPickAction(myRegion);
            pickAction.setPoint(event->getPosition());
            pickAction.setSceneManager(action->getSceneManager());
            pickAction.apply(action->getPickRoot());
            auto p = pickAction.getPickedPoint();
            if(p==NULL) {
                eventCB->releaseEvents();
                return;
            }
            auto point = p->getPoint();
            dd->isInHandle = false;
            dd->rectIdx = -1;
            dd->handleIdx = -1;
            for(auto i=0;i<dd->rectProp.count();i++) {
                for(auto j=0;j<4;j++) {
                    if(false == doubleComp(dd->rectPoints[j][i][2],point[2])) {
                        continue;
                    }
                    auto dist = sqrt(pow(dd->rectPoints[j][i][0] - point[0], 2) + pow(dd->rectPoints[j][i][1] - point[1], 2));
                    if(dist > dd->handleRadius) {
                        continue;
                    }
                    dd->isInHandle = true;
                    dd->rectIdx = i;
                    dd->handleIdx = j;
                    break;
                }
                if(dd->isInHandle) {
                    dd->handleMatl[dd->handleIdx][dd->rectIdx]->ambientColor.setValue(1, 1, 0);
                    dd->handleMatl[dd->handleIdx][dd->rectIdx]->diffuseColor.setValue(1, 1, 0);
                    break;
                }
            }
        }
        else if(SoMouseButtonEvent::isButtonReleaseEvent(mouseButton,SoMouseButtonEvent::BUTTON1)) {
            if(dd->isInHandle) {
                SoHandleEventAction* action = eventCB->getAction();
                const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

                SoRayPickAction pickaction = SoRayPickAction(myRegion);
                pickaction.setPoint(event->getPosition());
                pickaction.setSceneManager(action->getSceneManager());
                pickaction.apply(action->getPickRoot());
                auto p = pickaction.getPickedPoint();

                dd->rectPoints[dd->handleIdx][dd->rectIdx] = p->getPoint();
                auto fixedIdx = (dd->handleIdx + 2) % 4;
                int xMoveIdx;
                int yMoveIdx;
                if (0 == dd->handleIdx % 2) {
                    xMoveIdx = (dd->handleIdx + 4 - 1) % 4;
                    yMoveIdx = (dd->handleIdx + 1) % 4;
                }
                else {
                    yMoveIdx = (dd->handleIdx + 4 - 1) % 4;
                    xMoveIdx = (dd->handleIdx + 1) % 4;
                }
                auto fixedPoint = dd->rectProp[dd->rectIdx]->vertex.getValues(0)[fixedIdx];
                dd->rectPoints[xMoveIdx][dd->rectIdx] = SbVec3f(p->getPoint()[0], fixedPoint[1], fixedPoint[2]);
                dd->rectPoints[yMoveIdx][dd->rectIdx] = SbVec3f(fixedPoint[0], p->getPoint()[1], fixedPoint[2]);

                dd->handleMatl[dd->handleIdx][dd->rectIdx]->ambientColor.setValue(1, 0, 0);
                dd->handleMatl[dd->handleIdx][dd->rectIdx]->diffuseColor.setValue(1, 0, 0);

                dd->isInHandle = false;
                dd->rectIdx = -1;
                dd->handleIdx = -1;
            }
        }
        if(dd->isInHandle) {
            eventCB->setHandled();
            return;
        }
        eventCB->releaseEvents();
    }
    void RoiCropSep::rectMoveCB(void* pImpl, SoEventCallback* eventCB) {
        auto dd = static_cast<Impl*>(pImpl);
        if(dd->isInHandle) {
            const SoEvent* event = eventCB->getEvent();
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
            SoRayPickAction pickaction = SoRayPickAction(myRegion);
            pickaction.setPoint(event->getPosition());
            pickaction.setSceneManager(action->getSceneManager());
            pickaction.apply(action->getPickRoot());
            auto p = pickaction.getPickedPoint();
            if (p == NULL) {
                //broadcast mouse event to further callback				
                eventCB->releaseEvents();
                return;
            }
            dd->rectProp[dd->rectIdx]->vertex.set1Value(dd->handleIdx, p->getPoint());            
            auto fixedIdx = (dd->handleIdx + 2) % 4;            
            int xMoveIdx;
            int yMoveIdx;
            if(0 == dd->handleIdx % 2) {
                xMoveIdx = (dd->handleIdx + 4 - 1) % 4;
                yMoveIdx = (dd->handleIdx + 1) % 4;
            }else {
                yMoveIdx = (dd->handleIdx + 4 - 1) % 4;
                xMoveIdx = (dd->handleIdx + 1) % 4;
            }            
            auto fixedPoint = dd->rectProp[dd->rectIdx]->vertex.getValues(0)[fixedIdx];
            auto xMovePoint = SbVec3f(p->getPoint()[0], fixedPoint[1], fixedPoint[2]);
            auto yMovePoint = SbVec3f(fixedPoint[0], p->getPoint()[1], fixedPoint[2]);
            dd->rectProp[dd->rectIdx]->vertex.set1Value(xMoveIdx, xMovePoint);
            dd->rectProp[dd->rectIdx]->vertex.set1Value(yMoveIdx, yMovePoint);
            auto zeroPoint = dd->rectProp[dd->rectIdx]->vertex.getValues(0)[0];
            dd->rectProp[dd->rectIdx]->vertex.set1Value(4, zeroPoint);

            //apply handle translation
            dd->handleTrans[dd->handleIdx][dd->rectIdx]->translation.setValue(p->getPoint());            
            dd->handleTrans[xMoveIdx][dd->rectIdx]->translation.setValue(xMovePoint);
            dd->handleTrans[yMoveIdx][dd->rectIdx]->translation.setValue(yMovePoint);

            eventCB->setHandled();
            emit dd->thisPointer->roiModified(dd->rectIdx);

            return;
        }
        eventCB->releaseEvents();
    }
}
