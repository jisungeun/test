#include <QMap>

#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/SoPickedPoint.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Engines/ImageSegmentation/RegionGrowing/SoFloodFillThresholdProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalImageProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>

#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#pragma warning(pop)

#include "OivMerger.h"

#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoCropImageProcessing.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoVertexProperty.h>

constexpr char* maskVolumeDataNodeName{ "MaskVolume" };

struct OivMerger::Impl {
	MedicalHelper::Axis axis{ MedicalHelper::AXIAL };
	int cur_label{ 1 };
	bool underPicking{ false };
	SbVec3f cur_seed;

	SoVolumeData* result_chunk;
	bool isUpdate{ false };

	auto GetMaskVolume()->SoVolumeData* {
		return dynamic_cast<SoVolumeData*>(SoNode::getByName(maskVolumeDataNodeName));
	}
	auto GetLabelValue()->int {
		auto volData = GetMaskVolume();
		if (nullptr == volData) {
			return 0;
		}
		auto idx = volData->XYZToVoxel(cur_seed);

		auto pos = SbVec3i32(static_cast<int32_t>(idx[0]), static_cast<int32_t>(idx[1]), static_cast<int32_t>(idx[2]));
		cur_label = static_cast<int>(volData->getValue(pos));
		return cur_label;
	}
	auto ManageSelectedPosition()->void {
		auto volData = GetMaskVolume();
		if (nullptr == volData) {
			return;
		}
		if (cur_label < 1) {
			return;
		}
		double min, max;
		volData->getMinMax(min, max);
		//MedicalHelper::dicomAdjustVolume(volData);
		auto oriAdapter = MedicalHelper::getImageDataAdapter(volData);
		oriAdapter->interpretation = SoMemoryDataAdapter::VALUE;

		auto flood = new SoFloodFillThresholdProcessing;
		flood->inImage = oriAdapter;
		auto idx = volData->XYZToVoxel(cur_seed);
		flood->seedPoint.setValue(idx);
		/*if (volData->getDimension()[2] == 1) {
			flood->computeMode = SoFloodFillThresholdProcessing::ComputeMode::MODE_2D;
		}else {
			flood->computeMode = SoFloodFillThresholdProcessing::ComputeMode::MODE_3D;
		}       */
		flood->thresholdLevel.setValue(static_cast<float>(cur_label), static_cast<float>(cur_label) + 0.5f);

		if (result_chunk->getName() == "Start") {
			auto convert = new SoConvertImageProcessing;
			convert->inImage.connectFrom(&flood->outBinaryImage);
			convert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;
			auto outChunkReader = new SoVRImageDataReader;
			outChunkReader->imageData.connectFrom(&convert->outImage);

			result_chunk->setReader(*outChunkReader, TRUE);			

			double min, max;
			result_chunk->getMinMax(min, max);

			result_chunk->setName("ChunkMask");
		}
		else {
			//MedicalHelper::dicomAdjustVolume(result_chunk);
			auto prevAdapter = MedicalHelper::getImageDataAdapter(result_chunk);
			auto convBin = new SoConvertImageProcessing;
			convBin->inImage = prevAdapter;
			convBin->dataType = SoConvertImageProcessing::BINARY;

			auto logical = new SoLogicalImageProcessing;
			logical->inImage1.connectFrom(&flood->outBinaryImage);
			logical->inImage2.connectFrom(&convBin->outImage);
			logical->logicalOperator = SoLogicalImageProcessing::LogicalOperator::XOR;

			auto convert = new SoConvertImageProcessing;
			convert->inImage.connectFrom(&logical->outImage);
			convert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;
			auto outChunkReader = new SoVRImageDataReader;
			outChunkReader->imageData.connectFrom(&convert->outImage);
			result_chunk->setReader(*outChunkReader, TRUE);			
		}
		isUpdate = true;
	}
};

OivMerger::OivMerger() : d{ new Impl } {
	SoEventCallback* eventCallback = new SoEventCallback;
	eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
	this->addChild(eventCallback);

	//d->result_chunk = new SoVolumeData;
	//d->result_chunk->ref();
}

OivMerger::~OivMerger() {

}

auto OivMerger::isUpdate() -> bool {
	return d->isUpdate;
}

auto OivMerger::resetUpdate() -> void {
	d->isUpdate = false;
}

auto OivMerger::GetChunkValue()->int {
	if (d->underPicking) {
		d->underPicking = false;
		return d->cur_label;
	}
	return -1;
}

auto OivMerger::SetChunkVolume(SoVolumeData* vol) -> void {
	d->result_chunk = vol;
}


auto OivMerger::SetAxis(const MedicalHelper::Axis& axis) -> void {
	d->axis = axis;
}

void OivMerger::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto dd = static_cast<Impl*>(data);
	if (nullptr == dd) {
		action->setHandled();
		return;
	}
	const SoEvent* event = node->getEvent();
	auto viewportRegion = action->getViewportRegion();
	auto mousePos = event->getPosition(viewportRegion);
	SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
	rayPick.setPoint(mousePos);
	rayPick.apply(action->getPickRoot());

	SoPickedPoint* pickedPt = rayPick.getPickedPoint();
	if (pickedPt) {
		if (SO_MOUSE_PRESS_EVENT(event, BUTTON1)) {
			dd->cur_seed = pickedPt->getPoint();
			dd->GetLabelValue();
			dd->ManageSelectedPosition();
			dd->underPicking = true;
		}
	}
	action->setHandled();
}

//auto OivMerger::ConfirmSelectedRegion() -> void {    
//    auto origin_vol = d->GetMaskVolume();
//    double orimin, orimax;
//    origin_vol->getMinMax(orimin, orimax);
//
//    SoRef<SoMemoryDataAdapter> oriAdap = MedicalHelper::getImageDataAdapter(origin_vol);
//
//    SoRef<SoMemoryDataAdapter> binAdap = MedicalHelper::getImageDataAdapter(d->result_chunk);
//    binAdap->interpretation = SoMemoryDataAdapter::BINARY;
//
//    SoRef<SoResetImageProcessing> reset = new SoResetImageProcessing;
//    reset->intensityValue = orimax + 1;
//    reset->inImage = oriAdap.ptr();
//
//    SoRef<SoCombineByMaskProcessing> combine = new SoCombineByMaskProcessing;
//    combine->inBinaryImage = binAdap.ptr();
//    combine->inImage1.connectFrom(&reset->outImage);
//    combine->inImage2 = oriAdap.ptr();
//
//    SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
//    reader->imageData.connectFrom(&combine->outImage);
//
//    origin_vol->setReader(*reader, TRUE);
//}

auto OivMerger::ManageSelectedRegion(std::vector<SbVec3f> lasso_points) -> void {
	SoVertexProperty* vertexProp = new SoVertexProperty;
	vertexProp->vertex.setValues(0, static_cast<int>(lasso_points.size()), &lasso_points[0]);

	SoFaceSet* faceSet = new SoFaceSet;
	faceSet->numVertices.set1Value(0, static_cast<int>(lasso_points.size()));
	faceSet->vertexProperty.setValue(vertexProp);

	//make copy of current volume
	auto origin_vol = d->GetMaskVolume();
	double orimin, orimax;
	origin_vol->getMinMax(orimin, orimax);
	auto odim = origin_vol->getDimension();

	auto zeros = new uint16_t[odim[0] * odim[1]]();

	SoRef<SoVolumeData> tmpVolume = new SoVolumeData;
	tmpVolume->data.setValue(SbVec3i32(odim[0], odim[1], 1), SbDataType::DataType::UNSIGNED_SHORT, 16, zeros, SoSFArray::NO_COPY_AND_DELETE);
	tmpVolume->extent.setValue(origin_vol->extent.getValue());

	//apply modification to current mask volume
	int editionId;
	tmpVolume->startEditing(editionId);
	tmpVolume->editSurfaceShape(faceSet, 1., d->cur_label);
	tmpVolume->finishEditing(editionId);
	tmpVolume->saveEditing();

	double tmin, tmax;
	tmpVolume->getMinMax(tmin, tmax);

	//MedicalHelper::dicomAdjustVolume(tmpVolume.ptr());
	SoRef<SoMemoryDataAdapter> binAdap = MedicalHelper::getImageDataAdapter(tmpVolume.ptr());
	binAdap->interpretation = SoMemoryDataAdapter::Interpretation::BINARY;

	//MedicalHelper::dicomAdjustVolume(origin_vol);
	SoRef<SoMemoryDataAdapter> oriAdap = MedicalHelper::getImageDataAdapter(origin_vol);

	SoRef<SoMaskImageProcessing> mask = new SoMaskImageProcessing;
	mask->inImage = oriAdap.ptr();
	mask->inBinaryImage = binAdap.ptr();

	SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;
	thresh->inImage.connectFrom(&mask->outImage);
	thresh->thresholdLevel.setValue(1, INT_MAX);

	if (d->result_chunk->getName() == "Start") {
		SoRef<SoConvertImageProcessing> convert = new SoConvertImageProcessing;
		convert->inImage.connectFrom(&thresh->outBinaryImage);
		convert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

		SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&convert->outImage);
		d->result_chunk->setReader(*reader, TRUE);		
		d->result_chunk->setName("ChunkMask");
	}
	else {
		//MedicalHelper::dicomAdjustVolume(d->result_chunk);
		SoRef<SoMemoryDataAdapter> chAdap = MedicalHelper::getImageDataAdapter(d->result_chunk);

		SoRef<SoConvertImageProcessing> iconvert = new SoConvertImageProcessing;
		iconvert->inImage.connectFrom(&thresh->outBinaryImage);
		iconvert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

		SoRef<SoLogicalImageProcessing> logical = new SoLogicalImageProcessing;
		logical->inImage1.connectFrom(&iconvert->outImage);
		logical->inImage2 = chAdap.ptr();
		logical->logicalOperator = SoLogicalImageProcessing::LogicalOperator::XOR;

		SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&logical->outImage);

		d->result_chunk->setReader(*reader, TRUE);		
	}
}