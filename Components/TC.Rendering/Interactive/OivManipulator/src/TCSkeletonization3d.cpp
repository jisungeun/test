#include "TCSkeletonization3d.h"

#include <iostream>
#include <vector>

struct TCSkeltonization3d::Impl {
	int dim[3];
	int eulerLUT[256];
	//unsigned short* inputImage{ nullptr };
	std::shared_ptr<unsigned short> outputImage;
};

TCSkeltonization3d::TCSkeltonization3d() :d{ new Impl } {
	fillEulerLUT(d->eulerLUT);
}

TCSkeltonization3d::~TCSkeltonization3d() {

}

auto TCSkeltonization3d::setInput(unsigned short* img) -> void {
	//d->inputImage = img;
	if (nullptr == d->outputImage) {
		return;
	}
	auto size = d->dim[0] * d->dim[1] * d->dim[2];
	memcpy(d->outputImage.get(), img, sizeof(unsigned short) * size);
}

auto TCSkeltonization3d::getOutput() -> unsigned short* {
	return d->outputImage.get();
}


auto TCSkeltonization3d::setDimension(int x, int y, int z) -> void {
	d->dim[0] = x;
	d->dim[1] = y;
	d->dim[2] = z;

	auto size = d->dim[0] * d->dim[1] * d->dim[2];

	std::shared_ptr<unsigned short> outputBuf(new unsigned short[size](), std::default_delete<unsigned short[]>());
	d->outputImage = outputBuf;
}

auto TCSkeltonization3d::Process() -> void {
	if (nullptr == d->outputImage) {
		return;
	}
	//Boundary condition -> if index exceed boundary set as 0
	std::vector<PointType> simpleBorderPoints;
	typename std::vector<PointType>::iterator simpleBorderPointsIt;

	int unchangedBorders = 0;
	while (unchangedBorders < 6) {
		unchangedBorders = 0;
		for (int currentBorder = 1; currentBorder <= 6; currentBorder++) {
			for (auto i = 1; i < d->dim[0] - 1; i++) {
				for (auto j = 1; j < d->dim[1] - 1; j++) {
					for (auto k = 1; k < d->dim[2] - 1; k++) {
						int idx[3] = { i,j,k };
						if (getPixel(d->outputImage.get(), idx) != 1) {
							continue;
						}
						int neighbor[26] = { 0, };
						getNeighborhood(d->outputImage.get(), idx, neighbor);

						bool isBorderPoint = false;
						if (currentBorder == 1) {//North
							if (neighbor[10] + neighbor[9] + neighbor[11] + neighbor[0] + neighbor[1] + neighbor[2] + neighbor[17] + neighbor[18] + neighbor[19] == 0) {
								isBorderPoint = true;
							}
						}
						if (currentBorder == 2) {//South
							if (neighbor[15] + neighbor[14] + neighbor[16] + neighbor[6] + neighbor[7] + neighbor[8] + neighbor[23] + neighbor[24] + neighbor[25] == 0) {
								isBorderPoint = true;
							}
						}
						if (currentBorder == 3) {//East
							if (neighbor[13] + neighbor[2] + neighbor[5] + neighbor[8] + neighbor[11] + neighbor[16] + neighbor[19] + neighbor[22] + neighbor[25] == 0) {
								isBorderPoint = true;
							}
						}
						if (currentBorder == 4) {//West
							if (neighbor[12] + neighbor[0] + neighbor[3] + neighbor[6] + neighbor[9] + neighbor[14] + neighbor[17] + neighbor[20] + neighbor[23] == 0) {
								isBorderPoint = true;
							}
						}
						if (currentBorder == 5) {//Back
							if (neighbor[21] + neighbor[17] + neighbor[20] + neighbor[23] + neighbor[18] + neighbor[24] + neighbor[19] + neighbor[22] + neighbor[25] == 0) {
								isBorderPoint = true;
							}
						}
						if (currentBorder == 6) {//Front
							if (neighbor[4] + neighbor[0] + neighbor[3] + neighbor[6] + neighbor[1] + neighbor[7] + neighbor[2] + neighbor[5] + neighbor[8] == 0) {
								isBorderPoint = true;
							}
						}
						if (!isBorderPoint) {
							continue;
						}
						int numberOfNeighbors = 0;//0,0,0 already included
						for (auto n = 0; n < 26; n++) {
							if (neighbor[n] == 1) {
								numberOfNeighbors++;
							}
						}

						if (numberOfNeighbors == 1) {
							continue;
						}

						if (!isEulerInvariant(neighbor, d->eulerLUT)) {
							continue;
						}

						if (!isSimplePoint(neighbor)) {
							continue;
						}
						PointType p;
						p.position[0] = idx[0];
						p.position[1] = idx[1];
						p.position[2] = idx[2];
						simpleBorderPoints.push_back(p);
					}
				}
			}
			bool noChange = true;
			for (simpleBorderPointsIt = simpleBorderPoints.begin();
				simpleBorderPointsIt != simpleBorderPoints.end();
				simpleBorderPointsIt++) {
				auto p = *simpleBorderPointsIt;
				int sidx[3] = { p.position[0],p.position[1],p.position[2] };
				d->outputImage.get()[sidx[2] * d->dim[0] * d->dim[1] + sidx[0] * d->dim[1] + sidx[1]] = 0;
				//d->outputImage.get()[sidx[0] * d->dim[1] * d->dim[2] + sidx[1] * d->dim[2] + sidx[2]] = 0;
				int tempNeighbor[26]{ 0, };
				getNeighborhood(d->outputImage.get(), sidx, tempNeighbor);
				if (!isSimplePoint(tempNeighbor)) {
					d->outputImage.get()[sidx[2] * d->dim[0] * d->dim[1] + sidx[0] * d->dim[1] + sidx[1]] = 1;
					//d->outputImage.get()[sidx[0] * d->dim[1] * d->dim[2] + sidx[1] * d->dim[2] + sidx[2]] = 1;
				}
				else {
					noChange = false;
				}
			}
			if (noChange) {
				unchangedBorders++;
				std::cout << unchangedBorders << std::endl;
			}
			simpleBorderPoints.clear();
		}
	}
}

auto TCSkeltonization3d::getNeighborhood(unsigned short* img, int center[3], int neighbor[26]) -> void {//get neiborohod of given center point
	for (auto i = 0; i < 3; i++) {
		if (center[i] - 1 < 0) {
			return;
		}
		if (center[i] + 1 == d->dim[i]) {
			return;
		}
	}//ignore border

	int cur_idx[3];
	//0    
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[0] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[0] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//1
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[1] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[1] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//2
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[2] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[2] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//3
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] - 1;
	{
		neighbor[3] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[3] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//4
	cur_idx[0] = center[0];
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] - 1;
	{
		neighbor[4] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[4] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//5
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] - 1;
	{
		neighbor[5] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[5] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//6
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[6] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[6] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//7
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[7] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[7] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//8
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] - 1;
	{
		neighbor[8] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[8] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}

	/////////

	//9
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2];
	{
		neighbor[9] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[9] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//10
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2];
	{
		neighbor[10] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[10] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//11
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2];
	{
		neighbor[11] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[11] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//12
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2];
	{
		neighbor[12] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[12] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//13
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2];
	{
		neighbor[13] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[13] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//14
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2];
	{
		neighbor[14] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[14] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//15
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2];
	{
		neighbor[15] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[15] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//16
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2];
	{
		neighbor[16] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[16] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}

	////////////////////////
	//17
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[17] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[17] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//18
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[18] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[18] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//19
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] - 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[19] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[19] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//20
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] + 1;
	{
		neighbor[20] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[20] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//21
	cur_idx[0] = center[0];
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] + 1;
	{
		neighbor[21] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[21] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//22
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1];
	cur_idx[2] = center[2] + 1;
	{
		neighbor[22] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[22] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//23
	cur_idx[0] = center[0] - 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[23] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[23] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//24
	cur_idx[0] = center[0];
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[24] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[24] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
	//25
	cur_idx[0] = center[0] + 1;
	cur_idx[1] = center[1] + 1;
	cur_idx[2] = center[2] + 1;
	{
		neighbor[25] = img[cur_idx[2] * d->dim[0] * d->dim[1] + cur_idx[0] * d->dim[1] + cur_idx[1]];
		//neighbor[25] = img[cur_idx[0] * d->dim[1] * d->dim[2] + cur_idx[1] * d->dim[2] + cur_idx[2]];
	}
}

auto TCSkeltonization3d::getPixel(unsigned short* img, int idx[3]) -> int {
	for (auto i = 0; i < 3; i++) {
		if (idx[i] < 0 || idx[i] > d->dim[i] - 1) {
			return 0;
		}
	}
	//return img[idx[0] * d->dim[1] * d->dim[2] + idx[1] * d->dim[2] + idx[2]];
	return img[idx[2] * d->dim[0] * d->dim[1] + idx[0] * d->dim[1] + idx[1]];
}


auto TCSkeltonization3d::isEulerInvariant(int neighbors[26], int LUT[256]) -> bool {
	// calculate Euler characteristic for each octant and sum up
	int EulerChar = 0;
	unsigned char n;
	// Octant SWU
	n = 1;
	//if (neighbors[24] == 1)
	if (neighbors[23] == 1)
		n |= 128;
	//if (neighbors[25] == 1)
	if (neighbors[24] == 1)
		n |= 64;
	//if (neighbors[15] == 1)
	if (neighbors[14] == 1)
		n |= 32;
	//if (neighbors[16] == 1)
	if (neighbors[15] == 1)
		n |= 16;
	//if (neighbors[21] == 1)
	if (neighbors[20] == 1)
		n |= 8;
	//if (neighbors[22] == 1)
	if (neighbors[21] == 1)
		n |= 4;
	if (neighbors[12] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant SEU
	n = 1;
	//if (neighbors[26] == 1)
	if (neighbors[25] == 1)
		n |= 128;
	//if (neighbors[23] == 1)
	if (neighbors[22] == 1)
		n |= 64;
	//if (neighbors[17] == 1)
	if (neighbors[16] == 1)
		n |= 32;
	//if (neighbors[14] == 1)
	if (neighbors[13] == 1)
		n |= 16;
	//if (neighbors[25] == 1)
	if (neighbors[24] == 1)
		n |= 8;
	//if (neighbors[22] == 1)
	if (neighbors[21] == 1)
		n |= 4;
	//if (neighbors[16] == 1)
	if (neighbors[15] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant NWU
	n = 1;
	//if (neighbors[18] == 1)
	if (neighbors[17] == 1)
		n |= 128;
	//if (neighbors[21] == 1)
	if (neighbors[20] == 1)
		n |= 64;
	if (neighbors[9] == 1)
		n |= 32;
	if (neighbors[12] == 1)
		n |= 16;
	//if (neighbors[19] == 1)
	if (neighbors[18] == 1)
		n |= 8;
	//if (neighbors[22] == 1)
	if (neighbors[21] == 1)
		n |= 4;
	if (neighbors[10] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant NEU
	n = 1;
	//if (neighbors[20] == 1)
	if (neighbors[19] == 1)
		n |= 128;
	//if (neighbors[23] == 1)
	if (neighbors[22] == 1)
		n |= 64;
	//if (neighbors[19] == 1)
	if (neighbors[18] == 1)
		n |= 32;
	//if (neighbors[22] == 1)
	if (neighbors[21] == 1)
		n |= 16;
	if (neighbors[11] == 1)
		n |= 8;
	//if (neighbors[14] == 1)
	if (neighbors[13] == 1)
		n |= 4;
	if (neighbors[10] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant SWB
	n = 1;
	if (neighbors[6] == 1)
		n |= 128;
	//if (neighbors[15] == 1)
	if (neighbors[14] == 1)
		n |= 64;
	if (neighbors[7] == 1)
		n |= 32;
	//if (neighbors[16] == 1)
	if (neighbors[15] == 1)
		n |= 16;
	if (neighbors[3] == 1)
		n |= 8;
	if (neighbors[12] == 1)
		n |= 4;
	if (neighbors[4] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant SEB
	n = 1;
	if (neighbors[8] == 1)
		n |= 128;
	if (neighbors[7] == 1)
		n |= 64;
	//if (neighbors[17] == 1)
	if (neighbors[16] == 1)
		n |= 32;
	//if (neighbors[16] == 1)
	if (neighbors[15] == 1)
		n |= 16;
	if (neighbors[5] == 1)
		n |= 8;
	if (neighbors[4] == 1)
		n |= 4;
	//if (neighbors[14] == 1)
	if (neighbors[13] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant NWB
	n = 1;
	if (neighbors[0] == 1)
		n |= 128;
	if (neighbors[9] == 1)
		n |= 64;
	if (neighbors[3] == 1)
		n |= 32;
	if (neighbors[12] == 1)
		n |= 16;
	if (neighbors[1] == 1)
		n |= 8;
	if (neighbors[10] == 1)
		n |= 4;
	if (neighbors[4] == 1)
		n |= 2;
	EulerChar += LUT[n];
	// Octant NEB
	n = 1;
	if (neighbors[2] == 1)
		n |= 128;
	if (neighbors[1] == 1)
		n |= 64;
	if (neighbors[11] == 1)
		n |= 32;
	if (neighbors[10] == 1)
		n |= 16;
	if (neighbors[5] == 1)
		n |= 8;
	if (neighbors[4] == 1)
		n |= 4;
	//if (neighbors[14] == 1)
	if (neighbors[13] == 1)
		n |= 2;
	EulerChar += LUT[n];
	if (EulerChar == 0)
		return true;
	return false;
}

auto TCSkeltonization3d::isSimplePoint(int neighbors[26]) -> bool {
	// copy neighbors for labeling
	int cube[26];	
	for (auto i = 0; i < 26; i++) // i =  0..12 -> cube[0..12]
		cube[i] = neighbors[i];
	// i != 13 : ignore center pixel when counting (see [Lee94])
	/*for (i = 14; i < 27; i++) // i = 14..26 -> cube[13..25]
		cube[i - 1] = neighbors[i];*/
	// set initial label
	int label = 2;
	// for all points in the neighborhood
	for (int i = 0; i < 26; i++)
	{
		if (cube[i] == 1) // voxel has not been labelled yet
		{
			// start recursion with any octant that contains the point i
			switch (i)
			{
			case 0:
			case 1:
			case 3:
			case 4:
			case 9:
			case 10:
			case 12:
				Octree_labeling(1, label, cube);
				break;
			case 2:
			case 5:
			case 11:
			case 13:
				Octree_labeling(2, label, cube);
				break;
			case 6:
			case 7:
			case 14:
			case 15:
				Octree_labeling(3, label, cube);
				break;
			case 8:
			case 16:
				Octree_labeling(4, label, cube);
				break;
			case 17:
			case 18:
			case 20:
			case 21:
				Octree_labeling(5, label, cube);
				break;
			case 19:
			case 22:
				Octree_labeling(6, label, cube);
				break;
			case 23:
			case 24:
				Octree_labeling(7, label, cube);
				break;
			case 25:
				Octree_labeling(8, label, cube);
				break;
			}
			label++;
			if (label - 2 >= 2)
			{
				return false;
			}
		}
	}
	//return label-2; in [Lee94] if the number of connected compontents would be needed
	return true;
}

auto TCSkeltonization3d::Octree_labeling(int octant, int label, int cube[26]) -> void {
	// check if there are points in the octant with value 1
	if (octant == 1)
	{
		// set points in this octant to current label
		// and recurseive labeling of adjacent octants
		if (cube[0] == 1)
			cube[0] = label;
		if (cube[1] == 1)
		{
			cube[1] = label;
			Octree_labeling(2, label, cube);
		}
		if (cube[3] == 1)
		{
			cube[3] = label;
			Octree_labeling(3, label, cube);
		}
		if (cube[4] == 1)
		{
			cube[4] = label;
			Octree_labeling(2, label, cube);
			Octree_labeling(3, label, cube);
			Octree_labeling(4, label, cube);
		}
		if (cube[9] == 1)
		{
			cube[9] = label;
			Octree_labeling(5, label, cube);
		}
		if (cube[10] == 1)
		{
			cube[10] = label;
			Octree_labeling(2, label, cube);
			Octree_labeling(5, label, cube);
			Octree_labeling(6, label, cube);
		}
		if (cube[12] == 1)
		{
			cube[12] = label;
			Octree_labeling(3, label, cube);
			Octree_labeling(5, label, cube);
			Octree_labeling(7, label, cube);
		}
	}
	if (octant == 2)
	{
		if (cube[1] == 1)
		{
			cube[1] = label;
			Octree_labeling(1, label, cube);
		}
		if (cube[4] == 1)
		{
			cube[4] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(3, label, cube);
			Octree_labeling(4, label, cube);
		}
		if (cube[10] == 1)
		{
			cube[10] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(5, label, cube);
			Octree_labeling(6, label, cube);
		}
		if (cube[2] == 1)
			cube[2] = label;
		if (cube[5] == 1)
		{
			cube[5] = label;
			Octree_labeling(4, label, cube);
		}
		if (cube[11] == 1)
		{
			cube[11] = label;
			Octree_labeling(6, label, cube);
		}
		if (cube[13] == 1)
		{
			cube[13] = label;
			Octree_labeling(4, label, cube);
			Octree_labeling(6, label, cube);
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 3)
	{
		if (cube[3] == 1)
		{
			cube[3] = label;
			Octree_labeling(1, label, cube);
		}
		if (cube[4] == 1)
		{
			cube[4] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(2, label, cube);
			Octree_labeling(4, label, cube);
		}
		if (cube[12] == 1)
		{
			cube[12] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(5, label, cube);
			Octree_labeling(7, label, cube);
		}
		if (cube[6] == 1)
			cube[6] = label;
		if (cube[7] == 1)
		{
			cube[7] = label;
			Octree_labeling(4, label, cube);
		}
		if (cube[14] == 1)
		{
			cube[14] = label;
			Octree_labeling(7, label, cube);
		}
		if (cube[15] == 1)
		{
			cube[15] = label;
			Octree_labeling(4, label, cube);
			Octree_labeling(7, label, cube);
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 4)
	{
		if (cube[4] == 1)
		{
			cube[4] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(2, label, cube);
			Octree_labeling(3, label, cube);
		}
		if (cube[5] == 1)
		{
			cube[5] = label;
			Octree_labeling(2, label, cube);
		}
		if (cube[13] == 1)
		{
			cube[13] = label;
			Octree_labeling(2, label, cube);
			Octree_labeling(6, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[7] == 1)
		{
			cube[7] = label;
			Octree_labeling(3, label, cube);
		}
		if (cube[15] == 1)
		{
			cube[15] = label;
			Octree_labeling(3, label, cube);
			Octree_labeling(7, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[8] == 1)
			cube[8] = label;
		if (cube[16] == 1)
		{
			cube[16] = label;
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 5)
	{
		if (cube[9] == 1)
		{
			cube[9] = label;
			Octree_labeling(1, label, cube);
		}
		if (cube[10] == 1)
		{
			cube[10] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(2, label, cube);
			Octree_labeling(6, label, cube);
		}
		if (cube[12] == 1)
		{
			cube[12] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(3, label, cube);
			Octree_labeling(7, label, cube);
		}
		if (cube[17] == 1)
			cube[17] = label;
		if (cube[18] == 1)
		{
			cube[18] = label;
			Octree_labeling(6, label, cube);
		}
		if (cube[20] == 1)
		{
			cube[20] = label;
			Octree_labeling(7, label, cube);
		}
		if (cube[21] == 1)
		{
			cube[21] = label;
			Octree_labeling(6, label, cube);
			Octree_labeling(7, label, cube);
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 6)
	{
		if (cube[10] == 1)
		{
			cube[10] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(2, label, cube);
			Octree_labeling(5, label, cube);
		}
		if (cube[11] == 1)
		{
			cube[11] = label;
			Octree_labeling(2, label, cube);
		}
		if (cube[13] == 1)
		{
			cube[13] = label;
			Octree_labeling(2, label, cube);
			Octree_labeling(4, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[18] == 1)
		{
			cube[18] = label;
			Octree_labeling(5, label, cube);
		}
		if (cube[21] == 1)
		{
			cube[21] = label;
			Octree_labeling(5, label, cube);
			Octree_labeling(7, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[19] == 1)
			cube[19] = label;
		if (cube[22] == 1)
		{
			cube[22] = label;
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 7)
	{
		if (cube[12] == 1)
		{
			cube[12] = label;
			Octree_labeling(1, label, cube);
			Octree_labeling(3, label, cube);
			Octree_labeling(5, label, cube);
		}
		if (cube[14] == 1)
		{
			cube[14] = label;
			Octree_labeling(3, label, cube);
		}
		if (cube[15] == 1)
		{
			cube[15] = label;
			Octree_labeling(3, label, cube);
			Octree_labeling(4, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[20] == 1)
		{
			cube[20] = label;
			Octree_labeling(5, label, cube);
		}
		if (cube[21] == 1)
		{
			cube[21] = label;
			Octree_labeling(5, label, cube);
			Octree_labeling(6, label, cube);
			Octree_labeling(8, label, cube);
		}
		if (cube[23] == 1)
			cube[23] = label;
		if (cube[24] == 1)
		{
			cube[24] = label;
			Octree_labeling(8, label, cube);
		}
	}
	if (octant == 8)
	{
		if (cube[13] == 1)
		{
			cube[13] = label;
			Octree_labeling(2, label, cube);
			Octree_labeling(4, label, cube);
			Octree_labeling(6, label, cube);
		}
		if (cube[15] == 1)
		{
			cube[15] = label;
			Octree_labeling(3, label, cube);
			Octree_labeling(4, label, cube);
			Octree_labeling(7, label, cube);
		}
		if (cube[16] == 1)
		{
			cube[16] = label;
			Octree_labeling(4, label, cube);
		}
		if (cube[21] == 1)
		{
			cube[21] = label;
			Octree_labeling(5, label, cube);
			Octree_labeling(6, label, cube);
			Octree_labeling(7, label, cube);
		}
		if (cube[22] == 1)
		{
			cube[22] = label;
			Octree_labeling(6, label, cube);
		}
		if (cube[24] == 1)
		{
			cube[24] = label;
			Octree_labeling(7, label, cube);
		}
		if (cube[25] == 1)
			cube[25] = label;
	}
}


auto TCSkeltonization3d::fillEulerLUT(int LUT[256]) -> void {
	LUT[1] = 1;
	LUT[3] = -1;
	LUT[5] = -1;
	LUT[7] = 1;
	LUT[9] = -3;
	LUT[11] = -1;
	LUT[13] = -1;
	LUT[15] = 1;
	LUT[17] = -1;
	LUT[19] = 1;
	LUT[21] = 1;
	LUT[23] = -1;
	LUT[25] = 3;
	LUT[27] = 1;
	LUT[29] = 1;
	LUT[31] = -1;
	LUT[33] = -3;
	LUT[35] = -1;
	LUT[37] = 3;
	LUT[39] = 1;
	LUT[41] = 1;
	LUT[43] = -1;
	LUT[45] = 3;
	LUT[47] = 1;
	LUT[49] = -1;
	LUT[51] = 1;

	LUT[53] = 1;
	LUT[55] = -1;
	LUT[57] = 3;
	LUT[59] = 1;
	LUT[61] = 1;
	LUT[63] = -1;
	LUT[65] = -3;
	LUT[67] = 3;
	LUT[69] = -1;
	LUT[71] = 1;
	LUT[73] = 1;
	LUT[75] = 3;
	LUT[77] = -1;
	LUT[79] = 1;
	LUT[81] = -1;
	LUT[83] = 1;
	LUT[85] = 1;
	LUT[87] = -1;
	LUT[89] = 3;
	LUT[91] = 1;
	LUT[93] = 1;
	LUT[95] = -1;
	LUT[97] = 1;
	LUT[99] = 3;
	LUT[101] = 3;
	LUT[103] = 1;

	LUT[105] = 5;
	LUT[107] = 3;
	LUT[109] = 3;
	LUT[111] = 1;
	LUT[113] = -1;
	LUT[115] = 1;
	LUT[117] = 1;
	LUT[119] = -1;
	LUT[121] = 3;
	LUT[123] = 1;
	LUT[125] = 1;
	LUT[127] = -1;
	LUT[129] = -7;
	LUT[131] = -1;
	LUT[133] = -1;
	LUT[135] = 1;
	LUT[137] = -3;
	LUT[139] = -1;
	LUT[141] = -1;
	LUT[143] = 1;
	LUT[145] = -1;
	LUT[147] = 1;
	LUT[149] = 1;
	LUT[151] = -1;
	LUT[153] = 3;
	LUT[155] = 1;

	LUT[157] = 1;
	LUT[159] = -1;
	LUT[161] = -3;
	LUT[163] = -1;
	LUT[165] = 3;
	LUT[167] = 1;
	LUT[169] = 1;
	LUT[171] = -1;
	LUT[173] = 3;
	LUT[175] = 1;
	LUT[177] = -1;
	LUT[179] = 1;
	LUT[181] = 1;
	LUT[183] = -1;
	LUT[185] = 3;
	LUT[187] = 1;
	LUT[189] = 1;
	LUT[191] = -1;
	LUT[193] = -3;
	LUT[195] = 3;
	LUT[197] = -1;
	LUT[199] = 1;
	LUT[201] = 1;
	LUT[203] = 3;
	LUT[205] = -1;
	LUT[207] = 1;

	LUT[209] = -1;
	LUT[211] = 1;
	LUT[213] = 1;
	LUT[215] = -1;
	LUT[217] = 3;
	LUT[219] = 1;
	LUT[221] = 1;
	LUT[223] = -1;
	LUT[225] = 1;
	LUT[227] = 3;
	LUT[229] = 3;
	LUT[231] = 1;
	LUT[233] = 5;
	LUT[235] = 3;
	LUT[237] = 3;
	LUT[239] = 1;
	LUT[241] = -1;
	LUT[243] = 1;
	LUT[245] = 1;
	LUT[247] = -1;
	LUT[249] = 3;
	LUT[251] = 1;
	LUT[253] = 1;
	LUT[255] = -1;
}
