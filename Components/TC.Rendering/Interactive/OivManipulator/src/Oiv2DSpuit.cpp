#include <ioformat/IOFormat.h>
#include <iolink/view/ImageViewProvider.h>
#include <iolink/view/ImageViewFactory.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>

#include "Oiv2DSpuit.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/drawers/SoLassoScreenDrawer.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoInfo.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

using namespace imagedev;
using namespace ioformat;
using namespace iolink;

struct Oiv2DSpuit::Impl {
	SoSeparator* targetSliceScene { nullptr };
	SoVolumeData* originalVolume { nullptr };
	SoVolumeData* gradientVolume { nullptr };
	SoVolumeData* selectedRegion { nullptr };
	SoVolumeData* selectedRegionG { nullptr };
	float boxInfo[4];//0: RIMin 1: RIMax 2: GradMin 3: GradMax

	SoSwitch* spuitSwitch { nullptr };
	SoSeparator* spuitRoot { nullptr };
	SoLassoScreenDrawer* spuit { nullptr };

	double global_result[4] = { 0.0, 0.0, 0.0, 0.0 };
	bool valid_lasso { false };

	Oiv2DSpuit* thisPointer { nullptr };

	auto setTargetVolume() -> void;
	auto buildSpuitScene() -> void;
	auto getTargetVolumeData(SoSeparator* root) -> SoVolumeData*;
	auto getImageViewFromSoVolumeData(SoVolumeData* input) -> std::shared_ptr<ImageView>;
	auto calculateRegionGradient(SoVolumeData* input, std::shared_ptr<ImageView> grad, SoVolumeData* mask) -> void;
	auto createGradientVolume(SoVolumeData* input) -> std::shared_ptr<ImageView>;
	auto extractSubSlice(int slice_idx, SoVolumeData* input) -> SoVolumeData*;
	auto createEmptyImage(SoVolumeData* input) -> SoVolumeData*;
};

auto Oiv2DSpuit::Impl::createEmptyImage(SoVolumeData* input) -> SoVolumeData* {
	auto volDim = input->getDimension();

	auto numBytes = volDim[0] * volDim[1] * volDim[2];

	auto data = new unsigned short[numBytes];

	for (auto i = 0; i < numBytes; i++) {
		data[i] = 0;
	}

	auto emptyVolume = new SoVolumeData;
	emptyVolume->ref();
	emptyVolume->data.setValue(volDim, SbDataType::UNSIGNED_SHORT, 16, data, SoSFArray::NO_COPY_AND_DELETE);
	emptyVolume->extent.setValue(input->extent.getValue());
	emptyVolume->unrefNoDelete();

	return emptyVolume;
}

auto Oiv2DSpuit::Impl::extractSubSlice(int slice_idx, SoVolumeData* input) -> SoVolumeData* {
	auto dim = input->data.getSize();
	auto extent = input->extent.getValue();
	SbBox3i32 bBox(SbVec3i32(0, 0, slice_idx), SbVec3i32(dim[0] - 1, dim[1] - 1, slice_idx));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		input->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	input->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	unsigned short* data = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

	auto slicedVolume = new SoVolumeData;
	slicedVolume->ref();
	auto ldmParam = input->ldmResourceParameters.getValue()->tileDimension.getValue();
	SbVec3i32 volDim;
	volDim[0] = dim[0];
	volDim[1] = dim[1];
	volDim[2] = 1;
	slicedVolume->ldmResourceParameters.getValue()->tileDimension.setValue(ldmParam[0], ldmParam[1], 1);
	slicedVolume->data.setValue(volDim, SbDataType::UNSIGNED_SHORT, data, SoSFArray::COPY);
	slicedVolume->extent.setValue(input->extent.getValue());
	dataBufferObj->unmap();
	dataBufferObj->unref();

	slicedVolume->unrefNoDelete();
	return slicedVolume;
}

auto Oiv2DSpuit::Impl::createGradientVolume(SoVolumeData* input) -> std::shared_ptr<ImageView> {
	const auto idim = input->getDimension();
	const auto ires = input->getVoxelSize();

	VectorXu64 imageShape { static_cast<unsigned long long>(idim[0]), static_cast<unsigned long long>(idim[1]), static_cast<unsigned long long>(idim[2]) };
	Vector3d spacing { ires[0], ires[1], ires[2] };
	Vector3d origin { -idim[0] * ires[0] / 2, -idim[1] * ires[1] / 2, -idim[2] * ires[2] };
	auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
	auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
	setDimensionalInterpretation(image, ImageTypeId::IMAGE);

	VectorXu64 imageOrig { 0, 0, 0 };
	RegionXu64 imageRegion { imageOrig, imageShape };

	image->writeRegion(imageRegion, input->data.getValue());

	const auto grad = gradientOperator2d(image, GradientOperator2d::SHEN_CASTAN, GradientOperator2d::AMPLITUDE_MAXIMUM, 60);

	const auto stat = intensityStatistics(grad.outputAmplitudeImage, IntensityStatistics::MIN_MAX, { 0, 1 });

	return grad.outputAmplitudeImage;
}

auto Oiv2DSpuit::Impl::calculateRegionGradient(SoVolumeData* input, std::shared_ptr<ImageView> grad, SoVolumeData* mask) -> void {
	try {
		const auto image = getImageViewFromSoVolumeData(input);

		const auto maskImage = getImageViewFromSoVolumeData(mask);

		auto maskLabel = convertImage(maskImage, ConvertImage::LABEL_16_BIT);

		const auto analysis = std::make_shared<AnalysisMsr>();
		const auto meanMsr = analysis->select(NativeMeasurements::intensityMean);
		const auto sdMsr = analysis->select(NativeMeasurements::intensityStandardDeviation);

		LabelAnalysis labelAnalysisAlgo;
		labelAnalysisAlgo.setInputIntensityImage(image);
		labelAnalysisAlgo.setOutputAnalysis(analysis);
		labelAnalysisAlgo.setInputLabelImage(maskLabel);
		labelAnalysisAlgo.execute();

		const auto mean = meanMsr->value();
		const auto sigma = sdMsr->value();

		const auto ganalysis = std::make_shared<AnalysisMsr>();
		const auto gmeanMsr = ganalysis->select(NativeMeasurements::intensityMean);
		const auto gsdMsr = ganalysis->select(NativeMeasurements::intensityStandardDeviation);
		LabelAnalysis glabelAnalysisAlgo;
		glabelAnalysisAlgo.setInputIntensityImage(grad);
		glabelAnalysisAlgo.setOutputAnalysis(ganalysis);
		glabelAnalysisAlgo.setInputLabelImage(maskLabel);
		glabelAnalysisAlgo.execute();

		const auto gmean = gmeanMsr->value();
		const auto gsigma = gsdMsr->value();

		global_result[0] = (mean - sigma) / 10000.0; // RI mean - RI Standard Deviation
		global_result[1] = (mean + sigma) / 10000.0; // RI mean + RI Standard Deviation
		global_result[2] = gmean - gsigma; // Grad mean - Grad Standard Deviation
		global_result[3] = gmean + gsigma; // Grad mean + Grad Standard Deviation						
	} catch (Exception& e) {
		std::cout << e.what();
	}
}

auto Oiv2DSpuit::Impl::getImageViewFromSoVolumeData(SoVolumeData* input) -> std::shared_ptr<ImageView> {
	const auto dim = input->getDimension();
	const auto res = input->getVoxelSize();
	VectorXu64 imageShape { static_cast<unsigned long long>(dim[0]), static_cast<unsigned long long>(dim[1]), static_cast<unsigned long long>(dim[2]) };
	Vector3d spacing { res[0], res[1], res[2] };
	Vector3d origin { -dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2 };
	auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
	auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
	setDimensionalInterpretation(image, ImageTypeId::IMAGE);
	VectorXu64 imageOrig { 0, 0, 0 };
	RegionXu64 imageRegion { imageOrig, imageShape };

	image->writeRegion(imageRegion, input->data.getValue());
	return image;
}


auto Oiv2DSpuit::Impl::getTargetVolumeData(SoSeparator* root) -> SoVolumeData* {
	auto timeInfo = MedicalHelper::find<SoInfo>(root, "HT_CUR_TIME");
	if (!timeInfo)
		return nullptr;
	auto timestep = timeInfo->string.getValue().toInt();
	timestep %= 2;//number of buffer	
	std::string fileName = "volData";
	fileName += std::to_string(timestep);

	auto volData = MedicalHelper::find<SoVolumeData>(root, fileName);
	if (!volData) {
		return nullptr;
	}

	return volData;
}

auto Oiv2DSpuit::Impl::setTargetVolume() -> void {
	if (targetSliceScene) {
		auto volData = MedicalHelper::find<SoVolumeData>(targetSliceScene, "volData0");
		if (volData) {
			originalVolume = volData;
		} else {
			auto volData2 = MedicalHelper::find<SoVolumeData>(targetSliceScene, "volData1");
			originalVolume = volData2;
		}
	} else {
		originalVolume = nullptr;
	}
}

auto Oiv2DSpuit::Impl::buildSpuitScene() -> void {
	spuitSwitch = new SoSwitch;
	spuitSwitch->setName("SpuitSwitch");
	spuitSwitch->whichChild = -1;

	spuitRoot = new SoSeparator;
	spuitRoot->setName("SpuitRoot");
	spuitRoot->fastEditing = SoSeparator::CLEAR_ZBUFFER;
	spuitRoot->boundingBoxIgnoring = FALSE;

	spuit = new SoLassoScreenDrawer;
	spuit->color.setValue(1.0, 0.0, 0.0);
	spuit->setName("Lasso_spuit");
	spuit->onFinish.add(lineFinalizedCallback);
	spuit->simplificationThreshold.setValue(2);
	spuit->isClosed = TRUE;

	spuitSwitch->addChild(spuitRoot);
	spuitRoot->addChild(spuit);
}

Oiv2DSpuit::Oiv2DSpuit(QObject* parent) : QObject(parent), d { new Impl } {
	d->boxInfo[0] = 1.33f;
	d->boxInfo[1] = 1.34f;
	d->boxInfo[2] = 0.0f;
	d->boxInfo[3] = 255.0f;

	d->thisPointer = this;
	d->buildSpuitScene();
	d->spuit->setUserData(d.get());
}

Oiv2DSpuit::~Oiv2DSpuit() { }

void Oiv2DSpuit::lineFinalizedCallback(SoPolyLineScreenDrawer::EventArg& eventArg) {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	auto dd = static_cast<Impl*>(lineDrawer->getUserData());

	// If less than 1 point, shape cannot be generated.
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}
	//get root scenegraph
	auto root = eventArg.getAction()->getPickRoot();
	SoRayPickAction act = SoRayPickAction(eventArg.getAction()->getViewportRegion());
	// retrieve points of line in cam space
	std::vector<SbVec3f> lineInCam(lineDrawer->point.getNum());
	for (unsigned int i = 0; i < lineInCam.size(); ++i) {
		SbVec2f myVec2f(lineDrawer->point[i][0], lineDrawer->point[i][1]);
		myVec2f[0] = (myVec2f[0] + 1.0f) / 2.0f;
		myVec2f[1] = (myVec2f[1] + 1.0f) / 2.0f;
		act.setNormalizedPoint(myVec2f);
		act.apply(root);
		SoPickedPoint* picked_point = act.getPickedPoint(0);
		if (picked_point) {
			lineInCam[i].setValue(picked_point->getPoint()[0], picked_point->getPoint()[1], picked_point->getPoint()[2]);
		}
	}

	//create closed polygon shape using shaped interaction
	SoVertexProperty* vertexProp = new SoVertexProperty;
	vertexProp->vertex.setValues(0, static_cast<int>(lineInCam.size()), &lineInCam[0]);

	SoFaceSet* faceSet = new SoFaceSet;
	faceSet->numVertices.set1Value(0, static_cast<int32_t>(lineInCam.size()));
	faceSet->vertexProperty.setValue(vertexProp);

	//get current original volumeData
	if (root) {
		auto curVol = dd->getTargetVolumeData(static_cast<SoSeparator*>(root));
		//calculate current gradient image
		if (curVol) {
			auto slice = MedicalHelper::find<SoOrthoSlice>(root, "Slice_HT");
			auto slicedVolume = dd->extractSubSlice(slice->sliceNumber.getValue(), curVol);

			if (slicedVolume) {
				auto curGrad = dd->createGradientVolume(slicedVolume);
				if (curGrad) {
					//create empty image for generating temporal region
					auto curMask = dd->createEmptyImage(slicedVolume);
					if (curMask) {
						int editionId;

						curMask->startEditing(editionId);
						curMask->editSurfaceShape(faceSet, 1., 1.);
						curMask->finishEditing(editionId);
						curMask->saveEditing();
						curMask->data.touch();

						double min, max;
						curMask->getMinMax(min, max);
						if (max > 0) {
							dd->calculateRegionGradient(slicedVolume, curGrad, curMask);
							dd->valid_lasso = true;
						} else {
							dd->valid_lasso = false;
						}
					}
				}
			}
		}
	}
	// don't forget to clear line
	lineDrawer->clear();

	auto lassoSwitch = MedicalHelper::find<SoSwitch>(root, "SpuitSwitch");
	if (lassoSwitch)
		lassoSwitch->whichChild = -1;
	emit dd->thisPointer->sigFinishSpuit();
	action->setHandled();
}

auto Oiv2DSpuit::setFinalizeAction(SoHandleEventAction* action) -> void {
	d->spuit->finalize(action);
}


auto Oiv2DSpuit::getSpuitRoot() -> SoSwitch* {
	return d->spuitSwitch;
}

auto Oiv2DSpuit::setSliceSceneGraphRoot(SoSeparator* root) -> void {
	d->targetSliceScene = root;
	d->setTargetVolume();
}

auto Oiv2DSpuit::activateSpuit(bool activate) -> void {
	if (activate) {
		d->spuitSwitch->whichChild = 0;
	} else {
		d->spuitSwitch->whichChild = -1;
	}
}

auto Oiv2DSpuit::getCurrentTFBoxInfo() -> double* {
	if (d->valid_lasso) {
		return d->global_result;
	}
	return nullptr;
}
