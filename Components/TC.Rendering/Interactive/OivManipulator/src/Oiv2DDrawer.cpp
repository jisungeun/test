#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/Gui/SoGuiRenderArea.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <qcoreapplication.h>
#include <QMouseEvent>
#include <Inventor/elements/SoViewVolumeElement.h>
#include <Inventor/misc/SbExtrusionGenerator.h>
#pragma warning(pop)

#include "OivBrushDrawer.h"
#include "OivFiller.h"
#include "OivCleaner.h"
#include "OivPicker.h"
#include "OivMerger.h"
#include "OivFreeLine.h"

#include "Oiv2DDrawer.h"

struct Oiv2DDrawer::Impl {
	SoVolumeData* targetVolume{ nullptr };
	int cur_label{ 1 };
	bool isPick{ false };
	bool isPickErase{ false };
	bool isMerge{ false };

	SoSeparator* root{ nullptr };
	SoAnnotation* drawerSeparator{ nullptr };
	SoSwitch* drawToolSwitch{ nullptr };

	SoLassoScreenDrawer* lassoAddTool{ nullptr };
	SoLassoScreenDrawer* lassoSubTool{ nullptr };
	SoLassoScreenDrawer* lassoFrustumTool{ nullptr };
	SoLassoScreenDrawer* mergeSupport{ nullptr };
	OivBrushDrawer* paintTool{ nullptr };
	OivBrushDrawer* wipeTool{ nullptr };
	OivFiller* fillTool{nullptr};
	OivFiller* eraseTool{nullptr};
	OivCleaner* cleanTool{nullptr};	
	OivPicker* pickTool{nullptr};
	OivMerger* mergeTool{ nullptr };
	OivFreeLine* polyTool{ nullptr };
	
	DrawerToolType currentTool{ DrawerToolType::None };

	std::map<DrawerToolType, std::string> drawTools;
};

Oiv2DDrawer::Oiv2DDrawer() : d{ new Impl } {
	using ToolInfoPair = std::pair<DrawerToolType, std::string>;
	d->drawTools.insert(ToolInfoPair(DrawerToolType::None, "DrawToolCursor"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Paint, "DrawToolPaint"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Wipe, "DrawToolWipe"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Fill, "DrawToolFill"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Erase, "DrawToolErase"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::LassoAdd, "DrawToolLassoAdd"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::LassoSubtract, "DrawToolLassoSubtract"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Pick,"DrawToolPick"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Clean,"DrawToolClean"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::CleanSelection,"DrawToolCleanSelection"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::Flush,"DrawToolFlush"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::FlushSelection,"DrawToolFlushSelection"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::MergeLabel, "DrawToolMergelabel"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::LassoDelete, "DrawerToolLassoDelete"));
	d->drawTools.insert(ToolInfoPair(DrawerToolType::MergeSupport, "DrawerToolMergeSupport"));
	//d->drawTools.insert(ToolInfoPair(DrawerToolType::Divide, "DrawDivideLine"));

	BuildSceneGraph();
	//SetTool(DrawerToolType::LassoAdd);
	SetTool(DrawerToolType::None);
	qApp->installEventFilter(this);
}

Oiv2DDrawer::~Oiv2DDrawer() {		
}

bool Oiv2DDrawer::eventFilter(QObject* watched, QEvent* event) {
	Q_UNUSED(watched)
    if (event->type() == QEvent::MouseButtonRelease)
	{
       QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
	   if(mouseEvent->button() == Qt::MouseButton::LeftButton) {
	       if(d->isPick) {
	           auto val = d->pickTool->GetLabelValue();
			   //if(val>0) {				   
			       emit pickValue(val);
			   //}
		   }
		   else if (d->isMerge) {
			   if (d->mergeTool->isUpdate()) {
				   auto val = d->mergeTool->GetChunkValue();
				   emit mergeVol(val);
				   d->mergeTool->resetUpdate();
			   }
		   }else if(d->isPickErase) {
		       if(d->pickTool) {
				   auto id = d->pickTool->GetTransactionId();
				   if(id != -1) {
					   emit pickRemove();
				   }
		       }
		   }
	   }
	}
    return false;
}

auto Oiv2DDrawer::ActivateMergeSupport() -> void {
	const auto childIndex = d->drawToolSwitch->findChild(d->mergeSupport);

	d->drawToolSwitch->whichChild = childIndex;
}

auto Oiv2DDrawer::HistoryFromTool(int id)->void {
	emit sigHistory(id);
}

auto Oiv2DDrawer::BuildSceneGraph() -> void {
	d->root = new SoSeparator;
	d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;
		
	d->drawerSeparator = new SoAnnotation;
	d->root->addChild(d->drawerSeparator);

	d->drawToolSwitch = new SoSwitch;

	SoSFColor eraserLineColor;
	eraserLineColor.setValue(1., 1., 1.);

	auto paintTool = new OivBrushDrawer(this);
	paintTool->setName(d->drawTools.find(DrawerToolType::Paint)->second.c_str());
	paintTool->SetMaskValue(1.);		

	d->drawToolSwitch->addChild(paintTool);
	d->paintTool = paintTool;
		
	auto eraserTool = new OivBrushDrawer(this);
	eraserTool->setName(d->drawTools.find(DrawerToolType::Wipe)->second.c_str());
	eraserTool->SetMaskValue(0.);

	d->drawToolSwitch->addChild(eraserTool);
	d->wipeTool = eraserTool;	

	auto fillTool = new OivFiller(this);
	fillTool->setName(d->drawTools.find(DrawerToolType::Fill)->second.c_str());
	fillTool->SetFillMode(true);

	d->drawToolSwitch->addChild(fillTool);
	d->fillTool = fillTool;

	auto eraseTool = new OivFiller(this);
	eraseTool->setName(d->drawTools.find(DrawerToolType::Erase)->second.c_str());
	eraseTool->SetFillMode(false);

	d->drawToolSwitch->addChild(eraseTool);
	d->eraseTool = eraseTool;

	auto pickTool = new OivPicker;
	pickTool->setName(d->drawTools.find(DrawerToolType::Pick)->second.c_str());
	d->drawToolSwitch->addChild(pickTool);
	d->pickTool = pickTool;

	auto cleanTool = new OivCleaner(this);
	cleanTool->setName(d->drawTools.find(DrawerToolType::Clean)->second.c_str());
	d->drawToolSwitch->addChild(cleanTool);
	d->cleanTool = cleanTool;	
		
	auto lassoAddTool = new SoLassoScreenDrawer;
	lassoAddTool->simplificationThreshold.setValue(0);
	lassoAddTool->setName(d->drawTools.find(DrawerToolType::LassoAdd)->second.c_str());
	lassoAddTool->onFinish.add(*this, &Oiv2DDrawer::DrawLassoCallback);
	d->drawToolSwitch->addChild(lassoAddTool);
	d->lassoAddTool = lassoAddTool;

	auto lassoSubtractTool = new SoLassoScreenDrawer;	
	lassoSubtractTool->simplificationThreshold.setValue(0);
	lassoSubtractTool->color = eraserLineColor;
	lassoSubtractTool->setName(d->drawTools.find(DrawerToolType::LassoSubtract)->second.c_str());
	lassoSubtractTool->onFinish.add(*this, &Oiv2DDrawer::EraseLassoCallback);
	d->drawToolSwitch->addChild(lassoSubtractTool);
	d->lassoSubTool = lassoSubtractTool;

	auto mergeSupport = new SoLassoScreenDrawer;
	mergeSupport->simplificationThreshold.setValue(0);
	mergeSupport->setName(d->drawTools.find(DrawerToolType::MergeSupport)->second.c_str());
	mergeSupport->onFinish.add(*this, &Oiv2DDrawer::MergeSupportCallback);
	d->drawToolSwitch->addChild(mergeSupport);
	d->mergeSupport = mergeSupport;

	auto mergeTool = new OivMerger;
	mergeTool->setName(d->drawTools.find(DrawerToolType::MergeLabel)->second.c_str());
	d->drawToolSwitch->addChild(mergeTool);
	d->mergeTool = mergeTool;

	auto lassoDeleteTool = new SoLassoScreenDrawer;
	lassoDeleteTool->simplificationThreshold.setValue(0);
	lassoDeleteTool->color = eraserLineColor;
	lassoDeleteTool->setName(d->drawTools.find(DrawerToolType::LassoDelete)->second.c_str());
	lassoDeleteTool->onFinish.add(*this, &Oiv2DDrawer::FrustumLassoCallBack);
	d->drawToolSwitch->addChild(lassoDeleteTool);
	d->lassoFrustumTool = lassoDeleteTool;

	/*auto polyTool = new OivFreeLine;
	polyTool->setName(d->drawTools.find(DrawerToolType::Divide)->second.c_str());
	polyTool->onFinish.add(*this, &Oiv2DDrawer::DrawPolyCallback);

	d->drawToolSwitch->addChild(polyTool);
	d->polyTool = polyTool;*/

	d->drawerSeparator->addChild(d->drawToolSwitch);
}

auto Oiv2DDrawer::SetTargetVolume(SoVolumeData* targetVolume) -> void {
	d->targetVolume = targetVolume;	
	d->pickTool->SetTargetVolume(targetVolume);
}

auto Oiv2DDrawer::SetAxis(const MedicalHelper::Axis& axis) -> void {
	d->paintTool->SetAxis(axis);
	d->wipeTool->SetAxis(axis);
	d->fillTool->SetAxis(axis);
	d->eraseTool->SetAxis(axis);
	d->pickTool->SetAxis(axis);
	d->mergeTool->SetAxis(axis);
	d->cleanTool->SetAxis(axis);	
}

auto Oiv2DDrawer::GetSceneGraph() -> SoSeparator* {
	//return d->drawerSeparator;
	return d->root;
}

auto Oiv2DDrawer::SetMergerVolume(SoVolumeData* vol) -> void {
	d->mergeTool->SetChunkVolume(vol);
}

auto Oiv2DDrawer::SetCurLabel(uint32_t label) -> void {
	//std::cout << "Set current label as " << label << std::endl;

    d->cur_label = label;
	d->paintTool->SetLabel(label);
	//d->wipeTool->SetLabel(label); -
	d->fillTool->SetLabel(label);
	d->eraseTool->SetLabel(label);
	//d->cleanTool->SetLabel(label);//for clean selection
}

auto Oiv2DDrawer::GetCurLabel() -> uint32_t {
	return d->cur_label;
}


auto Oiv2DDrawer::GetTool() -> DrawerToolType {
	return d->currentTool;
}

auto Oiv2DDrawer::SetTool(DrawerToolType tool) -> void {
	auto childIndex = -1;
	d->isPick = false;
	d->isMerge = false;
	d->isPickErase = false;
	switch (tool) {
	case DrawerToolType::Paint:
		childIndex = d->drawToolSwitch->findChild(d->paintTool);		
		break;
	case DrawerToolType::Wipe:
		childIndex = d->drawToolSwitch->findChild(d->wipeTool);		
		break;
	case DrawerToolType::LassoAdd:
		childIndex = d->drawToolSwitch->findChild(d->lassoAddTool);
		break;
	case DrawerToolType::LassoSubtract:
		childIndex = d->drawToolSwitch->findChild(d->lassoSubTool);
		break;
	case DrawerToolType::Fill:		
		childIndex = d->drawToolSwitch->findChild(d->fillTool);
		break;
	case DrawerToolType::Erase:		
		childIndex = d->drawToolSwitch->findChild(d->eraseTool);
		break;
	case DrawerToolType::Pick:
		d->isPick = true;
		childIndex = d->drawToolSwitch->findChild(d->pickTool);
		d->pickTool->SetMode(0);
		break;
	case DrawerToolType::Clean:
		childIndex = d->drawToolSwitch->findChild(d->cleanTool);
		d->cleanTool->SetMode(2);
		break;
	case DrawerToolType::CleanSelection:
		childIndex = d->drawToolSwitch->findChild(d->cleanTool);
		d->cleanTool->SetMode(3);
		break;
	case DrawerToolType::Flush:
		childIndex = d->drawToolSwitch->findChild(d->cleanTool);
		d->cleanTool->SetMode(0);
		break;
	case DrawerToolType::FlushSelection:
		childIndex = d->drawToolSwitch->findChild(d->cleanTool);
		d->cleanTool->SetMode(1);
		break;
	case DrawerToolType::MergeLabel:
		childIndex = d->drawToolSwitch->findChild(d->mergeTool);
		d->isMerge = true;
		break;
	case DrawerToolType::PickDelete:
		childIndex = d->drawToolSwitch->findChild(d->pickTool);
		d->isPickErase = true;
		d->pickTool->SetMode(1);
		break;
	case DrawerToolType::LassoDelete:
		childIndex = d->drawToolSwitch->findChild(d->lassoFrustumTool);		
		break;
	/*case DrawerToolType::Divide:
		std::cout << "Poly Tool Activated" << std::endl;
		childIndex = d->drawToolSwitch->findChild(d->polyTool);
		break;*/	
	case DrawerToolType::None:
	default:
		tool = DrawerToolType::None;
		break;
	}

	d->drawToolSwitch->whichChild = childIndex;
	d->currentTool = tool;
}

auto Oiv2DDrawer::SetPaintBrushSize(uint32_t size) -> void {
	d->paintTool->SetBrushSize(size);
}

auto Oiv2DDrawer::SetWipeBrushSize(uint32_t size) -> void {
	d->wipeTool->SetBrushSize(size);
}

auto Oiv2DDrawer::FrustumLassoCallBack(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}

	std::vector<SbVec2f> lineInCam(lineDrawer->point.getNum());
	for (unsigned int i = 0; i < lineInCam.size(); ++i) {
		lineInCam[i].setValue(lineDrawer->point[i][0], lineDrawer->point[i][1]);
	}
	SoSearchAction searchAction;
	searchAction.setNode(d->targetVolume);
	searchAction.apply(d->root);
	SoPath* pathToExtrudedShapeRoot = searchAction.getPath();

	SbBox3f bbox = d->targetVolume->extent.getValue();

	SoShape* extrudedShape = SbExtrusionGenerator::createFrom2DPoints(lineInCam,
		pathToExtrudedShapeRoot,
		SoViewVolumeElement::get(action->getState()),
		bbox);	
	if(nullptr == extrudedShape) {
		std::cout << "failed to extrude shape" << std::endl;
		lineDrawer->clear();
		return;
	}
	
	//apply deletion according to shape
	int editionId;
	d->targetVolume->startEditing(editionId);
	d->targetVolume->editSolidShape(extrudedShape, 0);	
	d->targetVolume->finishEditing(editionId);
	d->targetVolume->saveEditing();

	lineDrawer->clear();

	action->setHandled();
}

auto Oiv2DDrawer::MergeSupportCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	// If less than 1 point, shape cannot be generated.
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}
	auto resultRayCast = CalcRayCasting(&lineDrawer->point, action->getPickRoot(), action->getViewportRegion());

	d->mergeTool->ManageSelectedRegion(resultRayCast);

	const auto childIndex = d->drawToolSwitch->findChild(d->mergeTool);

	d->drawToolSwitch->whichChild = childIndex;

	emit mergeVol(-1);

	lineDrawer->clear();	
	action->setHandled();
}

auto Oiv2DDrawer::DrawLassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg) ->void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	// If less than 1 point, shape cannot be generated.
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}	
	
	auto resultRayCast = CalcRayCasting(&lineDrawer->point, action->getPickRoot(), action->getViewportRegion());
		
	//create closed polygon shape using shaped interaction
	SoVertexProperty* vertexProp = new SoVertexProperty;
	vertexProp->vertex.setValues(0, static_cast<int>(resultRayCast.size()), &resultRayCast[0]);

	SoFaceSet* faceSet = new SoFaceSet;
	faceSet->numVertices.set1Value(0, static_cast<int>(resultRayCast.size()));
	faceSet->vertexProperty.setValue(vertexProp);
	faceSet->ref();

	//apply modification to current mask volume
	int editionId;
	d->targetVolume->startEditing(editionId);
	d->targetVolume->editSurfaceShape(faceSet, 1., d->cur_label);
	d->targetVolume->finishEditing(editionId);
		
	emit sigHistory(editionId);
	lineDrawer->clear();
	faceSet->unref();
	action->setHandled();
}

auto Oiv2DDrawer::SaveEditinigs() -> void {
	d->targetVolume->saveEditing();
}


auto Oiv2DDrawer::DrawPolyCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	//SoHandleEventAction* action = eventArg.getAction();		

	if(lineDrawer->point.getNum()<1) {
		lineDrawer->clear();
		return;
	}		

	lineDrawer->clear();
}


auto Oiv2DDrawer::EraseLassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	// If less than 1 point, shape cannot be generated.
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}

	auto resultRayCast = CalcRayCasting(&lineDrawer->point, action->getPickRoot(), action->getViewportRegion());

	//create closed polygon shape using shaped interaction
	SoVertexProperty* vertexProp = new SoVertexProperty;
	vertexProp->vertex.setValues(0, static_cast<int>(resultRayCast.size()), &resultRayCast[0]);

	SoFaceSet* faceSet = new SoFaceSet;
	faceSet->numVertices.set1Value(0, static_cast<int>(resultRayCast.size()));
	faceSet->vertexProperty.setValue(vertexProp);

	//apply modification to current mask volume
	int editionId;
	d->targetVolume->startEditing(editionId);
	d->targetVolume->editSurfaceShape(faceSet, 1., 0);
	d->targetVolume->finishEditing(editionId);
		
	//d->targetVolume->saveEditing();
	emit sigHistory(editionId);	
	lineDrawer->clear();

	action->setHandled();
}

auto Oiv2DDrawer::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
	std::vector<SbVec3f> result;

	SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
	for (int i = 0; i < source->getNum(); i++) {
		
		SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
		normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
		normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

		rayPick.setNormalizedPoint(normalizedPoint);
		rayPick.apply(targetNode);

		SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
		if (pickedPoint) {
			result.push_back(pickedPoint->getPoint());
		}
	}

	return result;
}