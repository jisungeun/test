#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#pragma warning(pop)

#include "OivEllipseDrawer.h"

SO_NODE_SOURCE(OivEllipseDrawer);

struct OivEllipseDrawer::Impl {
    SbVec2f firstAddedPoint;
    SbVec2f lastAddedPoint;
    SbVec2f centerPoint;
    SbVec2f majorAxisPoint;
    SbVec2f minorAxisPoint;

    bool isConverted;
    bool activated = false;
    bool mouse_is_pressed{ false };
};

OivEllipseDrawer::OivEllipseDrawer() :d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivEllipseDrawer);
    this->method = CORNER_CORNER;
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold = 0;
    simplificationThreshold.enableNotify(TRUE);
    this->nbPoint = 100;
    this->color.setValue(1, 1, 0);
    this->lineWidth = 2;
}

OivEllipseDrawer::~OivEllipseDrawer() {

}

void OivEllipseDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivEllipseDrawer, "EllipseDrawer", SoEllipseScreenDrawer);
}

void OivEllipseDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivEllipseDrawer);
}

void OivEllipseDrawer::reset() {
    this->clear();
    d->mouse_is_pressed = false;
}

auto OivEllipseDrawer::GetCenterPt() -> SbVec2f {
    return d->centerPoint;
}

auto OivEllipseDrawer::GetMajorAxisPt() -> SbVec2f {
    return d->majorAxisPoint;
}

auto OivEllipseDrawer::GetMinorAxisPt() -> SbVec2f {
    return d->minorAxisPoint;
}

void OivEllipseDrawer::Activate() {
    d->activated = true;
}

void OivEllipseDrawer::Deactivate() {
    d->activated = false;
}


void OivEllipseDrawer::onMouseDown(SoHandleEventAction* action) {
    if (!d->activated) return;
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    d->firstAddedPoint = pos;
    d->mouse_is_pressed = true;
    SoEllipseScreenDrawer::onMouseDown(action);
}

void OivEllipseDrawer::onMouseUp(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->mouse_is_pressed) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        d->lastAddedPoint = pos;
        d->centerPoint = (d->firstAddedPoint + d->lastAddedPoint) / 2.0;
        auto xlen = abs(d->centerPoint[0] - d->lastAddedPoint[0]);
        auto ylen = abs(d->centerPoint[1] - d->lastAddedPoint[1]);
        d->isConverted = false;
        if (xlen > ylen) {
            d->majorAxisPoint = SbVec2f(d->lastAddedPoint[0], d->centerPoint[1]);
            d->minorAxisPoint = SbVec2f(d->centerPoint[0], d->firstAddedPoint[1]);
        }
        else {
            d->majorAxisPoint = SbVec2f(d->centerPoint[0], d->firstAddedPoint[1]);
            d->minorAxisPoint = SbVec2f(d->lastAddedPoint[0], d->centerPoint[1]);
            d->isConverted = true;
        }
        SoEllipseScreenDrawer::onMouseUp(action);
        this->clear();
    }
}

auto OivEllipseDrawer::GetIsConverted() -> bool {
    return d->isConverted;
}