#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/draggers/SoScale2Dragger.h>

#pragma warning(pop)

#include "Oiv2DScaler.h"

struct Oiv2DScaler::Impl {
    SoScale2Dragger* scale2D{ nullptr };
    SoSeparator* contents{nullptr};
    SoNode* feedback{ nullptr };
    SoNode* feedbackActive{ nullptr };

    bool showGuide{ false };
};

Oiv2DScaler::Oiv2DScaler() :d{ new Impl } {
    d->scale2D = new SoScale2Dragger;    
    d->feedback = d->scale2D->getPart("feedback", true);
    d->feedbackActive = d->scale2D->getPart("feedbackActive", true);

    d->scale2D->setPart("feedback", new SoSeparator);
    d->scale2D->setPart("feedbackActive", new SoSeparator);
    
}

Oiv2DScaler::~Oiv2DScaler() {
    
}

auto Oiv2DScaler::setContentsRoot(SoSeparator* root) -> void {
    d->contents = root;
    d->scale2D->setPart("scaler", d->contents);
    d->scale2D->setPart("scalerActive", d->contents);    
}



auto Oiv2DScaler::getContentsRoot() -> SoSeparator* {
    return d->contents;
}

auto Oiv2DScaler::showGuideline(bool show) -> void {
    d->showGuide = show;
    if(show) {
        d->scale2D->setPart("feedback", d->feedback);
        d->scale2D->setPart("feedbackActive", d->feedbackActive);
    }else {
        d->scale2D->setPart("feedback", new SoSeparator);
        d->scale2D->setPart("feedbackActive", new SoSeparator);
    }
}

auto Oiv2DScaler::getManipulatorRoot() -> SoNode* {
    return d->scale2D;
}