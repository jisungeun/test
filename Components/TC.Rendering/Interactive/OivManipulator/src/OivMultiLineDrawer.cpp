#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#pragma warning(pop)

#include "OivMultiLineDrawer.h"

SO_NODE_SOURCE(OivMultiLineDrawer);
struct OivMultiLineDrawer::Impl {
    //QList<SbVec2f> points;
    int index = 0;
    bool mouse_is_pressed{ false };
    bool in_process{ false };
    SoSeparator* handleRoot{ nullptr };
    bool activated = false;
};

OivMultiLineDrawer::OivMultiLineDrawer() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivMultiLineDrawer);
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold.setValue(0);
    this->lineWidth = 3;
    this->color.setValue(1, 1, 0);
    this->isClosed = FALSE;
    simplificationThreshold.enableNotify(TRUE);
}

OivMultiLineDrawer::~OivMultiLineDrawer() {

}

void OivMultiLineDrawer::Activate() {
    d->activated = true;
}

void OivMultiLineDrawer::Deactivate() {
    d->activated = false;
}

auto OivMultiLineDrawer::setHandleRoot(SoSeparator* root)->void {
    d->handleRoot = root;
}

auto OivMultiLineDrawer::getHandleRoot()->SoSeparator* {
    return d->handleRoot;
}

void OivMultiLineDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivMultiLineDrawer, "MultiLineDrawer", SoPolyLineScreenDrawer);
}

void OivMultiLineDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivMultiLineDrawer);
}

void OivMultiLineDrawer::reset() {

}

void OivMultiLineDrawer::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivMultiLineDrawer::onMouseDown(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (false == d->in_process) {
        this->clear();
    }

    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    this->removePoint(d->index);
    this->addPoint(pos);
    d->index++;

    d->mouse_is_pressed = true;
    d->in_process = true;
}

void OivMultiLineDrawer::onMouseMove(SoHandleEventAction* action) {
    if (!d->activated) return;
    if (d->mouse_is_pressed) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);

        this->removePoint(d->index);
        this->addPoint(pos);
    }
}

void OivMultiLineDrawer::onMouseUp(SoHandleEventAction* action) {
    if (!d->activated) return;
}

auto OivMultiLineDrawer::Reset() -> void {
    d->mouse_is_pressed = false;
    d->in_process = false;
    this->clear();
    d->index = 0;    
}

void OivMultiLineDrawer::onMouseDblClick(SoHandleEventAction* action) {
    if (!d->activated) return;
    d->mouse_is_pressed = false;
    d->in_process = false;

    this->removePoint(d->index - 1);

    finalizeLine(action);
    d->index = 0;
    this->clear();
}

void OivMultiLineDrawer::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}