#include <future>
#include <vector>
#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/SoPickedPoint.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoVertexProperty.h>

#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "Oiv2DDrawer.h"
#include "OivBrushDrawer.h"

constexpr char* maskVolumeDataNodeName{ "MaskVolume" };
constexpr char* maskSliceNodeName{ "MaskSlice" };
constexpr char* cursorNodeName{ "CursorFaceSet" };
constexpr char* editSliceNodeName{ "TempSlice" };
constexpr char* editVizNodeNAme{ "TempViz" };

using namespace std;

struct OivBrushDrawer::Impl {
	bool mousePressed{ false };	

	MedicalHelper::Axis axis{ MedicalHelper::AXIAL };
	uint32_t brushSize{ 5 };	
	vector<SbVec3f> contourVertices;	

	SbVec3f currentTrace {0,0,0};
    SoTranslation* cursorTranslation{ nullptr };
	SoFaceSet* cursorFaceSet{ nullptr };
	SoSeparator* cursorHoverSep{ nullptr };
	SoGroup* cursorFaceSetTrace{ nullptr };
	SoSwitch* cursorSwitch{ nullptr };

	SoRef<SoVolumeData> tempSlice{nullptr};	

	double faceSetValue{ 0 };
	int editID{0};	
	int cur_idx{ -1 };

	bool isStartDrawing{ false };
	int tID_while_drawing;

	auto UpperLeft(SbVec2f point);
	auto UpdateContour();

	auto DisplayCursor(const SbVec3f& pos);	
	auto StartBrush();
	auto DrawBrush();
	auto EndBrush();
		
	SoVolumeData* CreateEmptySlice();		
	SoVolumeData* GetMaskVolume();

	uint32_t GetSliceNumber();

	Oiv2DDrawer* pPointer;
};

auto OivBrushDrawer::SetLabel(const uint32_t& label) -> void {
    //d->cur_label = label;
	d->faceSetValue = label;
}



auto OivBrushDrawer::Impl::UpperLeft(SbVec2f point) {
	point[0] -= 0.5;
	point[1] += 0.5;

    return point;
}

auto OivBrushDrawer::Impl::UpdateContour() {
	auto radius = static_cast<float>(brushSize) / 2.f;

	SbVec2f centerCorrection({0.f, 0.f});

	bool evenSize = ((brushSize % 2) == 0);
	if (evenSize) {
		centerCorrection[0] += 0.5f;
		centerCorrection[1] += 0.5f;
	}

	// store points for each quarter part of a circle contour
	vector<SbVec2f> upperRightCycle;
	vector<SbVec2f> upperLeftCycle;
	vector<SbVec2f> lowerRightCycle;
	vector<SbVec2f> lowerLeftCycle;

	SbVec2f point(0, static_cast<float>(brushSize / 2));
	bool isInside = true;

	upperRightCycle.push_back(UpperLeft(point));

	while (point[1] > 0) {
		// move right until pixel is outside circle
		auto squaredX = 0.f;
		auto squaredY = pow(point[1] - centerCorrection[1], 2);

		while (isInside) {
			point[0]++;
		    squaredX = pow(point[0] - centerCorrection[0], 2);

			if (sqrt(squaredX + squaredY) > radius) {
				isInside = false;
			}
		}

		upperRightCycle.push_back(UpperLeft(point));

		// move down until pixel is inside circle
		while (!isInside) {
			point[1]--;
			squaredY = pow(point[1] - centerCorrection[1], 2);
			
			if (sqrt(squaredX + squaredY) <= radius) {
				isInside = true;
				upperRightCycle.push_back(UpperLeft(point));
			}

			if (point[1] <= 0) {
				break;
			}
		}
	}

	// compute points of other quarters
	if (!evenSize) {
	    for (auto elem : upperRightCycle) {
			auto transferPoint = elem;

			transferPoint[1] *= -1;
			lowerRightCycle.push_back(transferPoint);

			transferPoint[0] *= -1;
			lowerLeftCycle.push_back(transferPoint);

			transferPoint[1] *= -1;
			upperLeftCycle.push_back(transferPoint);
	    }
	} else {
	    for (auto elem : upperRightCycle) {
			auto transferPoint = elem;

			transferPoint[1] = -(transferPoint[1]) + 1;
			lowerRightCycle.push_back(transferPoint);

			transferPoint[0] = -(transferPoint[0]) + 1;
			lowerLeftCycle.push_back(transferPoint);

			transferPoint = elem;
			transferPoint[0] = -(transferPoint[0]) + 1;
			upperLeftCycle.push_back(transferPoint);
	    }
	}

	vector<SbVec3f> contour;

	for (auto it = upperRightCycle.begin(); it != upperRightCycle.end(); ++it) {
		contour.push_back(
			SbVec3f((*it)[0], (*it)[1], 0.0)
		);
	}

	for (auto it = lowerRightCycle.rbegin(); it != lowerRightCycle.rend(); ++it) {
		contour.push_back(
			SbVec3f((*it)[0], (*it)[1], 0.0)
		);
	}

	for (auto it = lowerLeftCycle.begin(); it != lowerLeftCycle.end(); ++it) {
		contour.push_back(
			SbVec3f((*it)[0], (*it)[1], 0.0)
		);
	}

	for (auto it = upperLeftCycle.rbegin(); it != upperLeftCycle.rend(); ++it) {
		contour.push_back(
			SbVec3f((*it)[0], (*it)[1], 0.0)
		);
	}

    contourVertices = contour;
}

auto OivBrushDrawer::Impl::DisplayCursor(const SbVec3f& pos) {
	auto volumeData = GetMaskVolume();
	if (volumeData == nullptr) {
		return;
	}

	// local coordinates 로 계산된 contour 좌표를 voxel coordinates로 변경	
	auto voxelSize = volumeData->getVoxelSize();

	vector<SbVec3f> voxelContourVertices;
	for (auto elem : contourVertices) {
		SbVec3f vertex;
		switch (axis) {
		case MedicalHelper::AXIAL:
			vertex[0] = elem[0] * voxelSize[0];
			vertex[1] = elem[1] * voxelSize[1];
			vertex[2] = 0.f;	// ray picking 된 위치로 translation 동작을 통해 이동하므로 0으로 설정
			break;
		case MedicalHelper::CORONAL:
			vertex[0] = elem[1] * voxelSize[0];
			vertex[1] = 0.f;
			vertex[2] = elem[0] * voxelSize[2];
			break;
		case MedicalHelper::SAGITTAL:
			vertex[0] = 0.f;
			vertex[1] = elem[1] * voxelSize[1];
			vertex[2] = elem[0] * voxelSize[2];
			break;
		default:
			break;
		}
		voxelContourVertices.push_back(vertex);
	}	
	if (!voxelContourVertices.empty()) {
		// apply modification to current mask volume
		SoVertexProperty* contourVertexProp = new SoVertexProperty;
		contourVertexProp->vertex.setValues(0, static_cast<int>(voxelContourVertices.size()), &voxelContourVertices[0]);
		cursorFaceSet->numVertices.setNum(0);
		cursorFaceSet->numVertices.set1Value(0, static_cast<int>(voxelContourVertices.size()));
		cursorFaceSet->vertexProperty.setValue(contourVertexProp);
	}
    cursorTranslation->translation.setValue(pos);	
}

auto OivBrushDrawer::Impl::StartBrush() {
	
}

auto OivBrushDrawer::Impl::DrawBrush() {			
	vector<SbVec3f> faceSetVertices;
	auto mousePos = cursorTranslation->translation.getValue();

	auto volExt = tempSlice->extent.getValue();
	auto voxelSize = tempSlice->getVoxelSize();

    //auto sliceNumber = GetSliceNumber();		

	for (auto elem : contourVertices) {
		SbVec3f vertex;		
		switch(axis) {
		case MedicalHelper::AXIAL:
			vertex[0] = elem[0] * voxelSize[0] + mousePos[0];
			vertex[1] = elem[1] * voxelSize[1] + mousePos[1];			
			vertex[2] = mousePos[2];
			break;
		case MedicalHelper::CORONAL:
			vertex[0] = elem[1] * voxelSize[0] + mousePos[0];
			vertex[1] = mousePos[1];
			vertex[2] = elem[0] * voxelSize[2] + mousePos[2];
			break;
		case MedicalHelper::SAGITTAL:
			vertex[0] = mousePos[0];
			vertex[1] = elem[1] * voxelSize[1] + mousePos[1];
			vertex[2] = elem[0] * voxelSize[2] + mousePos[2];			
			break;
		default:
			break;
		}

		faceSetVertices.push_back(vertex);
	}

	if (!faceSetVertices.empty()) {
		// apply modification to current mask volume		
		SoRef<SoVertexProperty> contourVertexProp = new SoVertexProperty;
		contourVertexProp->vertex.setValues(0, static_cast<int>(faceSetVertices.size()), &faceSetVertices[0]);

		SoRef<SoFaceSet> contourFaceSet = new SoFaceSet;
		contourFaceSet->ref();
		contourFaceSet->numVertices.setNum(0);
		contourFaceSet->numVertices.set1Value(0, static_cast<int>(faceSetVertices.size()));
		contourFaceSet->vertexProperty.setValue(contourVertexProp.ptr());

		//int editionId;
		if (isStartDrawing) {
			tempSlice->startEditing(tID_while_drawing);
			isStartDrawing = false;
		}
		tempSlice->editSurfaceShape(contourFaceSet.ptr(), 1., 1.0);		
		//tempSlice->finishEditing(editionId);
		//tempSlice->saveEditing();
		while(contourFaceSet->getRefCount()>0) {
			contourFaceSet->unref();
		}

	}
}

auto OivBrushDrawer::Impl::EndBrush() {
	auto volumeData = GetMaskVolume();		
	if(nullptr == volumeData) {
		return;
	}
	tempSlice->finishEditing(tID_while_drawing);	
	tempSlice->saveEditing();
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
	auto size = tempSlice->getDimension();
	auto slice_num = GetSliceNumber();
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		tempSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	tempSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
    //apply temporal slice to original volume
	auto name_idx = 0;
	if (axis == MedicalHelper::AXIAL) {//XY plane		
	    tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);		
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[0] * size[1]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = faceSetValue;
			}
		}
		name_idx = 0;
	}else if(axis == MedicalHelper::CORONAL) {//YZ plane		
		tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[0] * size[2]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = faceSetValue;
			}
		}
		name_idx = 1;
	}else if(axis == MedicalHelper::SAGITTAL) {//XZ plane
		tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[1] * size[2]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = faceSetValue;
			}
		}
		name_idx = 2;		
	}	
    int editionId;
    volumeData->startEditing(editionId);
    volumeData->editSubVolume(tBox, tBufferObj.ptr());
    volumeData->finishEditing(editionId);
	editID = editionId;	
	
    tBufferObj->unmap();
    dataBufferObj->unmap();

	auto socketName = std::string("TempSlice") + std::to_string(name_idx);
	auto sw = dynamic_cast<SoSwitch*>(SoNode::getByName(socketName.c_str()));	
	sw->removeAllChildren();
	sw->whichChild = -1;
	auto vizName = std::string("TempViz") + std::to_string(name_idx);
	auto viz = dynamic_cast<SoSwitch*>(SoNode::getByName(vizName.c_str()));
	viz->whichChild = -1;

	tempSlice->unref();
	tempSlice = nullptr;
}

SoVolumeData* OivBrushDrawer::Impl::CreateEmptySlice() {
	auto vol = GetMaskVolume();
	if(nullptr == vol) {
		return nullptr;
	}	
	auto name_idx = 0;
	if(axis == MedicalHelper::AXIAL) {		
		name_idx = 0;
	}else if(axis == MedicalHelper::CORONAL) {		
		name_idx = 1;
	}else if(axis == MedicalHelper::SAGITTAL) {		
		name_idx = 2;
	}
	
	auto socketName = std::string("TempSlice") + std::to_string(name_idx);
	auto sw = dynamic_cast<SoSwitch*>(SoNode::getByName(socketName.c_str()));
	auto slice_num = GetSliceNumber();			

	auto vizName = std::string("TempViz") + std::to_string(name_idx);
	auto viz = dynamic_cast<SoSwitch*>(getByName(vizName.c_str()));

	if(sw->whichChild.getValue() == -1 || static_cast<int>(slice_num) != cur_idx) {		
	    //generate empty slice from existing volume		
		sw->removeAllChildren();		
	    auto ext = vol->extent.getValue();
		auto voxelSize = vol->getVoxelSize();

	    cur_idx = slice_num;
		auto size = vol->getDimension();		
		if (axis == MedicalHelper::AXIAL) {//for XY slice
			//copy xy slice from current mask volume			
			std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[1]](), std::default_delete<unsigned short[]>());												

			tempSlice = new SoVolumeData;							    
			
			tempSlice->data.setValue(SbVec3i32(size[0], size[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
			tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2]*(slice_num),
				ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2]*(slice_num+1));
			tempSlice->setName("tempData");
			tempSlice->ldmResourceParameters.getValue()->resolution = 0;			
			tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

			sw->addChild(tempSlice.ptr());
			sw->whichChild = 0;

			emptySlice = nullptr;

			viz->whichChild = 0;			
		}else if(axis  ==  MedicalHelper::CORONAL) {//for YZ slice
			//copy xz slice from current mask volume						
			std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[2]](), std::default_delete<unsigned short[]>());			
			tempSlice = new SoVolumeData;

			tempSlice->data.setValue(SbVec3i32(size[0], 1, size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
			tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1] * slice_num, ext.getMin()[2],
				ext.getMax()[0], ext.getMin()[1] + voxelSize[1] * (slice_num + 1), ext.getMax()[2]);

			tempSlice->setName("tempData");
			tempSlice->ldmResourceParameters.getValue()->resolution = 0;
			tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

			sw->addChild(tempSlice.ptr());
			sw->whichChild = 0;

			emptySlice = nullptr;

			viz->whichChild = 0;
		}else if(axis == MedicalHelper::SAGITTAL) {//for XZ slice
			//copy yz slice from current mask volume
			std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[1] * size[2]](), std::default_delete<unsigned short[]>());
			
			tempSlice = new SoVolumeData;

			tempSlice->data.setValue(SbVec3i32(1, size[1], size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
			tempSlice->extent.setValue(ext.getMin()[0] + voxelSize[0] * slice_num, ext.getMin()[1], ext.getMin()[2],
				ext.getMin()[0] + voxelSize[0] * (slice_num + 1), ext.getMax()[1], ext.getMax()[2]);
			tempSlice->setName("tempData");
			tempSlice->ldmResourceParameters.getValue()->resolution = 0;
			tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

			sw->addChild(tempSlice.ptr());
			sw->whichChild = 0;

			emptySlice = nullptr;

			viz->whichChild = 0;			
		}

	}
	return nullptr;
}

SoVolumeData* OivBrushDrawer::Impl::GetMaskVolume() {	
    return dynamic_cast<SoVolumeData*>(SoNode::getByName(maskVolumeDataNodeName));
}

uint32_t OivBrushDrawer::Impl::GetSliceNumber() {
	uint32_t sliceNumber = 0;

	// get slice number
	SoNodeList foundNodes;
	auto foundNodeCount = getByName(maskSliceNodeName, foundNodes);
	if(foundNodeCount>-1) {
	    for (auto i = 0; i < foundNodeCount; i++) {
			auto orthoSlice = dynamic_cast<SoOrthoSlice*>(foundNodes[i]);
			if (orthoSlice && (axis == orthoSlice->axis.getValue())) {
			    sliceNumber = orthoSlice->sliceNumber.getValue();
				break;
			}
	    }
	}

	return sliceNumber;
}

OivBrushDrawer::OivBrushDrawer(Oiv2DDrawer* p) : d{ new Impl } {
	d->pPointer = p;
	SoEventCallback* eventCallback = new SoEventCallback;	
	eventCallback->addEventCallback(SoLocation2Event::getClassTypeId(), HandleMouseMoveEvent, d.get());
	eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
	this->addChild(eventCallback);

	SoMaterial* baseColor = new SoMaterial;
	baseColor->diffuseColor.setValue(0, 1, 0);	
	this->addChild(baseColor);

	SoDrawStyle* drawStyle = new SoDrawStyle;	
	drawStyle->style = SoDrawStyle::Style::LINES;
	this->addChild(drawStyle);

	d->cursorHoverSep = new SoSeparator;
	d->cursorHoverSep->setName("HoverSep");
	d->cursorTranslation = new SoTranslation;
	d->cursorTranslation->translation.setValue(0.f, 0.f, 0.f);	// Todo: z값	
	d->cursorHoverSep->addChild(d->cursorTranslation);

	d->cursorFaceSet = new SoFaceSet;
	d->cursorFaceSet->setName(cursorNodeName);
	d->cursorHoverSep->addChild(d->cursorFaceSet);

	d->cursorSwitch = new SoSwitch;
	d->cursorSwitch->addChild(d->cursorHoverSep);
	d->cursorFaceSetTrace = new SoGroup;
	d->cursorSwitch->addChild(d->cursorFaceSetTrace);
	d->cursorSwitch->whichChild = 0;    
	this->addChild(d->cursorSwitch);	
}

OivBrushDrawer::~OivBrushDrawer() {
}

auto OivBrushDrawer::SetAxis(const MedicalHelper::Axis& axis) -> void {
	d->axis = axis;	
}

auto OivBrushDrawer::SetBrushSize(const uint32_t& size) -> void {
	if (size <= 0) {
		return;
	}

	d->brushSize = size;
	d->UpdateContour();
	d->DisplayCursor(d->cursorTranslation->translation.getValue());
}

auto OivBrushDrawer::SetMaskValue(const double& value) -> void {
	d->faceSetValue = value;
}

void OivBrushDrawer::HandleMouseMoveEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (d == nullptr) {
		action->setHandled();
		return;
	}

	auto event = action->getEvent();
	auto viewportRegion = action->getViewportRegion();

	if (d->contourVertices.empty()) {
		node->setHandled();
		return;
	}

	auto mousePosition = event->getPosition(viewportRegion);

	SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
	rayPick.setPoint(mousePosition);
	rayPick.apply(action->getPickRoot());

	SoPickedPoint* pickedPt = rayPick.getPickedPoint();
	if (pickedPt) {
		auto currentPt = d->cursorTranslation->translation.getValue();
		auto nextPt = pickedPt->getPoint();
		if (currentPt != nextPt) {
		    d->DisplayCursor(nextPt);
		}
		if (d->mousePressed) {
			auto dd = pickedPt->getPoint();
			if (d->currentTrace != dd) {
				d->DrawBrush();
				d->currentTrace = dd;
			}
		}
	}	

	node->setHandled();
}

void OivBrushDrawer::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (d == nullptr) {
		action->setHandled();
		return;
	}

	const SoEvent* event = node->getEvent();

	if (SO_MOUSE_PRESS_EVENT(event, BUTTON1)) {
		d->mousePressed = true;
		d->isStartDrawing = true;
		d->CreateEmptySlice();		
		d->DrawBrush();
	}
	else if (SO_MOUSE_RELEASE_EVENT(event, BUTTON1)) {		
		d->mousePressed = false;
		d->EndBrush();
		d->pPointer->HistoryFromTool(d->editID);
	}
	action->setHandled();
}