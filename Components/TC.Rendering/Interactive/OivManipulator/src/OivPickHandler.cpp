#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/Gui/SoGuiRenderArea.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <qcoreapplication.h>
#include <QMouseEvent>
#include <Inventor/elements/SoViewVolumeElement.h>
#include <Inventor/misc/SbExtrusionGenerator.h>
#pragma warning(pop)

#include "OivPicker.h"

#include "OivPickHandler.h"

struct OivPickHandler::Impl {
    SoVolumeData* targetVolume{ nullptr };
    bool isPick{ false };
    SoSeparator* root{ nullptr };
    SoAnnotation* drawerSeparator{ nullptr };    
    SoSwitch* pickSwitch{ nullptr };
    OivPicker* pickTool{ nullptr };
    MedicalHelper::Axis axis;
};

OivPickHandler::OivPickHandler() :d{ new Impl } {
    BuildSceneGraph();
    qApp->installEventFilter(this);
}

OivPickHandler::~OivPickHandler() {
    
}

bool OivPickHandler::eventFilter(QObject* watched, QEvent* event) {
    Q_UNUSED(watched)    
    if (event->type() == QEvent::MouseButtonRelease)
    {
        if (nullptr == d->targetVolume) {
            return false;
        }
        if (d->pickSwitch->whichChild.getValue() != 0) {
            return false;
        }
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->button() == Qt::MouseButton::LeftButton) {
            auto val = d->pickTool->GetLabelValue();
            if (val > -1) {
                emit pickValue(val);
            }
        }
    }
    return false;
}

auto OivPickHandler::BuildSceneGraph() -> void {
    d->root = new SoSeparator;
    d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;

    d->drawerSeparator = new SoAnnotation;
    d->root->addChild(d->drawerSeparator);

    d->pickSwitch = new SoSwitch;
    d->drawerSeparator->addChild(d->pickSwitch);

    auto pickTool = new OivPicker;
    pickTool->setName("DrawToolPick");    
    d->pickTool = pickTool;
    d->pickTool->SetMode(0);
    d->pickSwitch->addChild(d->pickTool);
    d->pickSwitch->whichChild = 0;
}

auto OivPickHandler::SetRotation(bool isRot) -> void {
    d->pickTool->isRot(isRot);
}


auto OivPickHandler::TogglePick(bool isPick) -> void {
    d->isPick = isPick;
    if(d->isPick) {
        d->pickSwitch->whichChild = 0;
    }else {
        d->pickSwitch->whichChild = -1;
    }
}

auto OivPickHandler::GetSceneGraph() -> SoSeparator* {
    return d->root;
}

auto OivPickHandler::SetAxis(const MedicalHelper::Axis& axis) -> void {
    d->pickTool->SetAxis(axis);
    d->axis = axis;
}

auto OivPickHandler::GetAxis() -> MedicalHelper::Axis {
    return d->axis;
}


auto OivPickHandler::SetTargetVolume(SoVolumeData* targetVolume) -> void {
    d->targetVolume = targetVolume;
    d->pickTool->SetTargetVolume(targetVolume);
}