#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#pragma warning(pop)

#include "OivFreeLine.h"

SO_NODE_SOURCE(OivFreeLine);
//SO_FIELDCONTAINER_SOURCE(OivFreeLine)

struct OivFreeLine::Impl {
    SbVec2s lastAddedPoint;
    bool mouse_is_pressed{ false };
    QList<SbVec3f> pt_list;
};

OivFreeLine::OivFreeLine() : d{ new Impl } {
    //SO_FIELDCONTAINER_CONSTRUCTOR(OivFreeLine);
    SO_NODE_CONSTRUCTOR(OivFreeLine);
    simplificationThreshold.enableNotify(FALSE);
    this->simplificationThreshold.setValue(2);
    simplificationThreshold.enableNotify(TRUE);
}

OivFreeLine::~OivFreeLine() {

}

void OivFreeLine::initClass() {
    //SO_FIELDCONTAINER_INIT_CLASS(OivFreeLine, "OivFreeLine", SoPolyLineScreenDrawer);
    SO__NODE_INIT_CLASS(OivFreeLine, "CustomFreeLine", SoPolyLineScreenDrawer);
}

void OivFreeLine::exitClass() {
    //SO__FIELDCONTAINER_EXIT_CLASS(OivFreeLine, "OivFreeLine", SoPolyLineScreenDrawer);
    SO__NODE_EXIT_CLASS(OivFreeLine);
}

void OivFreeLine::reset() {
    
}

void OivFreeLine::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivFreeLine::onMouseDown(SoHandleEventAction* action) {    
    d->mouse_is_pressed = true;        
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    this->addPoint(SbVec2f(-1,1));    
}

void OivFreeLine::onMouseMove(SoHandleEventAction* action) {
    if (d->mouse_is_pressed) {
        auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
        auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
        this->addPoint(pos);
    }
}

void OivFreeLine::onMouseUp(SoHandleEventAction* action) {
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    d->lastAddedPoint = SbVec2s(pos[0], pos[1]);    
    d->mouse_is_pressed = false;    
    this->addPoint(SbVec2f(-1, -1));
    this->addPoint(SbVec2f(-1, 1));    
    finalizeLine(action);
}

void OivFreeLine::finalizeLine(SoHandleEventAction* action) {
    this->finalize(action);
}