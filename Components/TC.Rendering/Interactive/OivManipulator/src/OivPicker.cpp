#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Engines/ImageSegmentation/RegionGrowing/SoFloodFillThresholdProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoReorderLabelsProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/SoPickedPoint.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/SbImageDataAdapterHelper.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "OivPicker.h"

constexpr char* maskVolumeDataNodeName{ "MaskVolume" };

struct OivPicker::Impl {
    MedicalHelper::Axis axis{MedicalHelper::AXIAL};
    int cur_label{1};
    int cur_editID{ 0 };
    bool deleteSuccess{ false };
    bool underPicking{false};
    SbVec3f cur_seed;
    bool isRot{false};

    int mode{ 0 };//0 : picking by index, 1: picking by connected component & delete

    //auto GetMaskVolume()->SoVolumeData*;
    SoVolumeData* volData{ nullptr };
    auto InternalGetLabelValue()->int;
    auto InternalDeleteConnectedComponent()->void;
};

auto OivPicker::SetMode(int mode) -> void {
    d->mode = mode;
}

auto OivPicker::SetTargetVolume(SoVolumeData* vol)->void {
    d->volData = vol;
}

auto OivPicker::GetTransactionId() -> int {
    auto id = -1;
    if(d->deleteSuccess) {
        id = d->cur_editID;
    }
    d->deleteSuccess = false;
    return id;
}

/*auto OivPicker::Impl::GetMaskVolume() -> SoVolumeData* {
    return dynamic_cast<SoVolumeData*>(SoNode::getByName(maskVolumeDataNodeName));
}*/
auto OivPicker::isRot(bool rot) -> void {
    d->isRot = rot;
}

auto OivPicker::Impl::InternalDeleteConnectedComponent() -> void {
    //SoRef<SoVolumeData> volData = GetMaskVolume();
    if(nullptr == volData){
        return;
    }
    auto idx = volData->XYZToVoxel(cur_seed);
    auto pos = SbVec3i32(static_cast<int32_t>(idx[0]), static_cast<int32_t>(idx[1]), static_cast<int32_t>(idx[2]));
    const auto label_value = volData->getValue(pos);
    if(label_value > 0) {        
        //MedicalHelper::dicomAdjustVolume(volData);
        SoRef<SoMemoryDataAdapter> imageAdapter = MedicalHelper::getImageDataAdapter(volData);
        SoRef<SoFloodFillThresholdProcessing> floodFill = new SoFloodFillThresholdProcessing;
        //floodFill->ref();
        floodFill->inImage = imageAdapter.ptr();
        floodFill->seedPoint.setValue(idx);
        floodFill->thresholdLevel.setValue(static_cast<float>(label_value) - 0.5f, static_cast<float>(label_value) + 0.5f);

        SoRef<SoLogicalNotProcessing> logicNot = new SoLogicalNotProcessing;
        //logicNot->ref();
        logicNot->inImage.connectFrom(&floodFill->outBinaryImage);

        SoRef<SoMaskImageProcessing> masking = new SoMaskImageProcessing;
        //masking->ref();
        masking->inImage = imageAdapter.ptr();
        masking->inBinaryImage.connectFrom(&logicNot->outImage);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        //reader->ref();
        reader->imageData.connectFrom(&masking->outImage);

        SoRef<SoVolumeData> changeVol = new SoVolumeData;
        //changeVol->ref();
        changeVol->setReader(*reader, TRUE);

        auto dim = volData->getDimension();

        SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            changeVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        changeVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

        int transactionId;        
        volData->startEditing(transactionId);
        volData->editSubVolume(bBox, dataBufferObj.ptr());
        volData->finishEditing(transactionId);
        cur_editID = transactionId;
        //volData->saveEditing();//possible undo/redo but too heavy

        /*SoRef<SoMemoryDataAdapter> oriAdapter = MedicalHelper::getImageDataAdapter(volData.ptr());
        oriAdapter->interpretation = SoMemoryDataAdapter::Interpretation::LABEL;

        //auto labeling = new SoLabelingProcessing;
        //labeling->inObjectImage = oriAdapter;
        SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;
        reorder->inLabelImage = oriAdapter.ptr();

        SoRef<SoConvertImageProcessing> converter = new SoConvertImageProcessing;
        converter->inImage.connectFrom(&reorder->outLabelImage);
        converter->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

        //SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;        
        auto rreader = new SoVRImageDataReader;
        rreader->imageData.connectFrom(&converter->outImage);

        volData->setReader(*rreader, TRUE);*/

        //release memory
        /*
        dataBufferObj = nullptr;
        while(changeVol->getRefCount()>0) {
            changeVol->unref();
        }
        changeVol = nullptr;
        while(reader->getRefCount()>0) {
            reader->unref();
        }
        reader = nullptr;
        while(masking->getRefCount()>0) {
            masking->unref();
        }
        masking = nullptr;
        while(logicNot->getRefCount()>0) {
            logicNot->unref();
        }
        logicNot = nullptr;
        while(floodFill->getRefCount()>0) {
            floodFill->unref();
        }
        floodFill = nullptr;
        while(imageAdapter->getRefCount()>0) {
            imageAdapter->unref();
        }
        imageAdapter = nullptr;
        */
        deleteSuccess = true;
    }
}


auto OivPicker::Impl::InternalGetLabelValue() -> int {
    //auto volData = GetMaskVolume();
    if(nullptr == volData) {
        return 0;
    }
    auto idx = volData->XYZToVoxel(cur_seed);
    const auto dx = static_cast<int32_t>(idx[0]);
    const auto dy = static_cast<int32_t>(idx[1]);
    const auto dz = static_cast<int32_t>(idx[2]);
    auto pos = SbVec3i32(dx, dy, dz);
    cur_label = static_cast<int>(volData->getValue(pos));
    return cur_label;    
}

OivPicker::OivPicker() :d{new Impl}{
    SoEventCallback* eventCallback = new SoEventCallback;
    eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(),HandleMouseButtonEvent,d.get());
    this->addChild(eventCallback);
}

OivPicker::~OivPicker() {
    
}

auto OivPicker::GetLabelValue()->int {
    if(d->underPicking){
        d->underPicking = false;
        return d->cur_label;
    }
    return -1;    
}
void OivPicker::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
    auto action = node->getAction();

    auto d = static_cast<Impl*>(data);
    if(nullptr == d) {
        action->setHandled();
        return;
    }
    const SoEvent* event = node->getEvent();
    auto viewportRegion = action->getViewportRegion();    
    auto mousePos =  event->getPosition(viewportRegion);
    SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
    rayPick.setPoint(mousePos);
    rayPick.apply(action->getPickRoot());
    
    SoPickedPoint* pickedPt = rayPick.getPickedPoint();    
    if(pickedPt){
        if(SO_MOUSE_PRESS_EVENT(event,BUTTON1)) {
            d->cur_seed = pickedPt->getPoint();
            if (d->isRot) {
                auto temp = -d->cur_seed[2];
                d->cur_seed[2] = d->cur_seed[1];
                d->cur_seed[1] = temp;
            }
            if (d->mode == 0) {
                d->InternalGetLabelValue();
                d->underPicking = true;
            }else {
                d->InternalDeleteConnectedComponent();
            }            
        }
    }
    action->setHandled();
}

auto OivPicker::SetAxis(const MedicalHelper::Axis& axis) -> void {
    d->axis = axis;
}

