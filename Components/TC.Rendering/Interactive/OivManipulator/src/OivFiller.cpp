#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/SoPickedPoint.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>

#include <ImageViz/Engines/ImageSegmentation/RegionGrowing/SoFloodFillThresholdProcessing.h>//connected component from seedpoint
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoArithmeticImageProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#pragma warning(pop)

#include "Oiv2DDrawer.h"
#include "OivFiller.h"

constexpr char* maskVolumeDataNodeName{ "MaskVolume" };
constexpr char* maskSliceNodeName{ "MaskSlice" };


struct OivFiller::Impl {
    bool isFill;
    MedicalHelper::Axis axis{ MedicalHelper::AXIAL };
    SoRef<SoVolumeData> tempSlice{ nullptr };
    SoRef<SoVolumeData> tempFilled{nullptr};
    uint32_t cur_label{1};
    int cur_pick{-1};

    int cur_idx{ -1 };    
    SbVec3f cur_seed{0,0,0};

    Oiv2DDrawer* pPointer;

    auto CopyCurrentSlice()->SoVolumeData*;    
    auto FillProcess()->void;
    auto EraseProcess()->void;
    auto GetMaskVolume()->SoVolumeData*;
    auto GetSliceNumber()->uint32_t;
    auto GetLabelValue()->uint32_t;
    auto CreateFilledSlice()->void;
};

auto OivFiller::Impl::GetLabelValue() -> uint32_t {
    auto volData = GetMaskVolume();
    auto idx = volData->XYZToVoxel(cur_seed);
    auto pos = SbVec3i32(static_cast<int32_t>(idx[0]), static_cast<int32_t>(idx[1]), static_cast<int32_t>(idx[2]));
    auto clabel = static_cast<uint32_t>(volData->getValue(pos));
    return clabel;
}


auto OivFiller::SetLabel(const uint32_t& lable) -> void {
    d->cur_label = lable;    
}


auto OivFiller::Impl::CreateFilledSlice() -> void {    
    auto vol = GetMaskVolume();
    if(nullptr == vol) {
        return;
    }    
    auto slice_num = GetSliceNumber();			
    auto size = vol->getDimension();
    auto ext = vol->extent.getValue();
    auto voxelSize = vol->getVoxelSize();
    if(axis == MedicalHelper::AXIAL) {        
        std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[1]](), std::default_delete<unsigned short[]>());
        std::fill_n(emptySlice.get(),size[0]*size[1],cur_label);

        tempFilled = new SoVolumeData;
        tempFilled->data.setValue(SbVec3i32(size[0],size[1],1),SbDataType::UNSIGNED_SHORT,16,(void*)emptySlice.get(),SoSFArray::COPY);
        tempFilled->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2]*(slice_num),
				ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2]*(slice_num+1));
        tempFilled->setName("tempFilled");
        tempFilled->ldmResourceParameters.getValue()->resolution = 0;
        tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

        emptySlice = nullptr;
    }else if(axis == MedicalHelper::CORONAL) {
        std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[2]](), std::default_delete<unsigned short[]>());
        std::fill_n(emptySlice.get(),size[0]*size[2],cur_label);

        tempFilled = new SoVolumeData;
        tempFilled->data.setValue(SbVec3i32(size[0],1,size[2]),SbDataType::UNSIGNED_SHORT,16,(void*)emptySlice.get(),SoSFArray::COPY);
        tempFilled->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1]*slice_num, ext.getMin()[2],
				ext.getMax()[0], ext.getMin()[1] + voxelSize[1]*(slice_num+1), ext.getMax()[2]);
        tempFilled->setName("tempFilled");
        tempFilled->ldmResourceParameters.getValue()->resolution = 0;
        tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

        emptySlice = nullptr;
    }else if(axis == MedicalHelper::SAGITTAL) {
        std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[1] * size[2]](), std::default_delete<unsigned short[]>());
        std::fill_n(emptySlice.get(),size[1]*size[2],cur_label);

        tempFilled = new SoVolumeData;
        tempFilled->data.setValue(SbVec3i32(1,size[1],size[2]),SbDataType::UNSIGNED_SHORT,16,(void*)emptySlice.get(),SoSFArray::COPY);
        tempFilled->extent.setValue(ext.getMin()[0]+voxelSize[0]*slice_num, ext.getMin()[1], ext.getMin()[2],
				ext.getMin()[0] + voxelSize[0]*(slice_num+1), ext.getMax()[1], ext.getMax()[2]);
        tempFilled->setName("tempFilled");
        tempFilled->ldmResourceParameters.getValue()->resolution = 0;
        tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

        emptySlice = nullptr;
    }
    //MedicalHelper::dicomAdjustVolume(tempFilled.ptr());
}

auto OivFiller::Impl::GetSliceNumber() -> uint32_t {
    uint32_t sliceNumber = 0;

    // get slice number
    SoNodeList foundNodes;
    auto foundNodeCount = getByName(maskSliceNodeName, foundNodes);
    if(foundNodeCount > -1){
        for (auto i = 0; i < foundNodeCount; i++) {
            auto orthoSlice = dynamic_cast<SoOrthoSlice*>(foundNodes[i]);
            if (orthoSlice && (axis == orthoSlice->axis.getValue())) {
                sliceNumber = orthoSlice->sliceNumber.getValue();
                break;
            }
        }
    }

    return sliceNumber;
}

auto OivFiller::Impl::CopyCurrentSlice()->SoVolumeData* {
    //Copy current slice 
    auto vol = GetMaskVolume();
    if(nullptr == vol) {
        return nullptr;
    }
    CreateFilledSlice();
    auto name_idx = 0;
    if (axis == MedicalHelper::AXIAL) {
        name_idx = 0;
    }
    else if (axis == MedicalHelper::CORONAL) {
        name_idx = 1;
    }
    else if (axis == MedicalHelper::SAGITTAL) {
        name_idx = 2;
    }

    cur_idx = GetSliceNumber();
    tempSlice = new SoVolumeData;
    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
    SbBox3i32 bBox;
    auto dim = vol->getDimension();
    auto ext = vol->extent.getValue();
    auto voxelSize = vol->getVoxelSize();
    
    if (axis == MedicalHelper::AXIAL) {
        bBox = SbBox3i32(SbVec3i32(0, 0, cur_idx), SbVec3i32(dim[0] - 1, dim[1] - 1, cur_idx));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox,(SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(dim[0], dim[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2] * cur_idx,
            ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2] * (cur_idx + 1));
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }else if(axis == MedicalHelper::CORONAL) {
        bBox = SbBox3i32(SbVec3i32(0, cur_idx, 0), SbVec3i32(dim[0] - 1, cur_idx, dim[2]-1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(dim[0], 1, dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1]*cur_idx, ext.getMin()[2],
            ext.getMax()[0], ext.getMin()[1] + voxelSize[1]*(cur_idx+1), ext.getMax()[2]);
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }else if(axis == MedicalHelper::SAGITTAL) {
        bBox = SbBox3i32(SbVec3i32(cur_idx, 0, 0), SbVec3i32(cur_idx, dim[1] - 1, dim[2]-1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

        tempSlice->data.setValue(SbVec3i32(1, dim[1], dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
        tempSlice->extent.setValue(ext.getMin()[0] + voxelSize[0]*cur_idx, ext.getMin()[1], ext.getMin()[2],
            ext.getMin()[0] + voxelSize[0]*(cur_idx+1), ext.getMax()[1], ext.getMax()[2]);
        tempSlice->setName("tempData");
        tempSlice->ldmResourceParameters.getValue()->resolution = 0;
        tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
    }
    dataBufferObj->unmap();
    dataBufferObj = nullptr;

    //MedicalHelper::dicomAdjustVolume(tempSlice.ptr());

    return nullptr;
}

auto OivFiller::Impl::EraseProcess() -> void { 
    SoRef<SoMemoryDataAdapter> sliceAdapter = MedicalHelper::getImageDataAdapter(tempSlice.ptr());
    sliceAdapter->ref();    
    sliceAdapter->interpretation = SoImageDataAdapter::Interpretation::VALUE;
        
    SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;    
    thresh->inImage = sliceAdapter.ptr();
    thresh->thresholdLevel.setValue(cur_label,cur_label+0.5);

    SoRef<SoFloodFillThresholdProcessing> floodfill = new SoFloodFillThresholdProcessing;    
    floodfill->thresholdLevel.setValue(0.5,100.0);
    auto pp = tempSlice->XYZToVoxel(cur_seed);
    floodfill->seedPoint.setValue(pp);    
    SoRef<SoLogicalImageProcessing> logic = new SoLogicalImageProcessing;    
    logic->inImage1.connectFrom(&thresh->outBinaryImage);


    floodfill->inImage.connectFrom(&thresh->outBinaryImage);        

    SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;    

    SoRef<SoMemoryDataAdapter> filledAdapter = MedicalHelper::getImageDataAdapter(tempFilled.ptr());
    filledAdapter->ref();
    filledAdapter->interpretation = SoImageDataAdapter::Interpretation::VALUE;


    SoRef<SoLogicalNotProcessing> inv = new SoLogicalNotProcessing;
    inv->inImage.connectFrom(&floodfill->outBinaryImage);

    SoRef<SoMaskImageProcessing> mask = new SoMaskImageProcessing;
    mask->inImage = sliceAdapter.ptr();
    mask->inBinaryImage.connectFrom(&inv->outImage);

    reader->imageData.connectFrom(&mask->outImage);    

    SoRef<SoVolumeData> filledSlice = new SoVolumeData;
    filledSlice->setReader(*reader);

    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
    auto size = tempSlice->getDimension();
    auto slice_num = GetSliceNumber();
    SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		filledSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	filledSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

    auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
    //apply changed slice to current slice
    auto volumeData = GetMaskVolume();    
    if(axis == MedicalHelper::AXIAL) {
        tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox = volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::CORONAL) {
        tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::SAGITTAL) {
        tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

        memcpy(targetdata,sourcedata,dataBufferObj->getSize());
    }

    int editionId;
    volumeData->startEditing(editionId);
    volumeData->editSubVolume(tBox, tBufferObj.ptr());
    volumeData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);

    tBufferObj->unmap();
    dataBufferObj->unmap();

    tBufferObj = nullptr;
    dataBufferObj = nullptr;
}


auto OivFiller::Impl::FillProcess()->void {
    //find connected component object using clicked seed        
    SoRef<SoMemoryDataAdapter> sliceAdapter = MedicalHelper::getImageDataAdapter(tempSlice.ptr());
    sliceAdapter->ref();    
    sliceAdapter->interpretation = SoImageDataAdapter::Interpretation::VALUE;
        
    SoRef<SoThresholdingProcessing> thresh = new SoThresholdingProcessing;    
    thresh->inImage = sliceAdapter.ptr();
    thresh->thresholdLevel.setValue(cur_label,cur_label+0.5);    


    SoRef<SoThresholdingProcessing> thresh2 = new SoThresholdingProcessing;    
    thresh2->inImage = sliceAdapter.ptr();
    thresh2->thresholdLevel.setValue(1.0,255.0);

    SoRef<SoFloodFillThresholdProcessing> floodfill = new SoFloodFillThresholdProcessing;    
    floodfill->thresholdLevel.setValue(0.5,100.0);
    auto pp = tempSlice->XYZToVoxel(cur_seed);
    floodfill->seedPoint.setValue(pp);    

    SoRef<SoLogicalImageProcessing> logic = new SoLogicalImageProcessing;    
    logic->inImage1.connectFrom(&thresh->outBinaryImage);

    //sliceAdapter->interpretation = SoImageDataAdapter::Interpretation::BINARY;
    SoRef<SoLogicalNotProcessing> invert = new SoLogicalNotProcessing;
    invert->inImage.connectFrom(&thresh2->outBinaryImage);

    floodfill->inImage.connectFrom(&invert->outImage);

    logic->inImage2.connectFrom(&floodfill->outBinaryImage);
    logic->logicalOperator = SoLogicalImageProcessing::LogicalOperator::OR;
        
    SoRef<SoMemoryDataAdapter> filledAdapter = MedicalHelper::getImageDataAdapter(tempFilled.ptr());
    filledAdapter->ref();
    filledAdapter->interpretation = SoImageDataAdapter::Interpretation::VALUE;

    //get mask of generated mask
    SoRef<SoMaskImageProcessing> mask1 = new SoMaskImageProcessing;
    mask1->inImage = filledAdapter.ptr();
    mask1->inBinaryImage.connectFrom(&logic->outImage);    

    //get inverse mask of original slice    
    SoRef<SoLogicalNotProcessing> inv = new SoLogicalNotProcessing;
    inv->inImage.connectFrom(&logic->outImage);    

    SoRef<SoMaskImageProcessing> mask2 = new SoMaskImageProcessing;
    mask2->inImage = sliceAdapter.ptr();
    mask2->inBinaryImage.connectFrom(&inv->outImage);        
    
    SoRef<SoArithmeticImageProcessing> ari = new SoArithmeticImageProcessing;
    ari->inImage1.connectFrom(&mask1->outImage);
    ari->inImage2.connectFrom(&mask2->outImage);
    ari->arithmeticOperator = SoArithmeticImageProcessing::ArithmeticOperator::ADD;

    SoRef<SoConvertImageProcessing> convert = new SoConvertImageProcessing;
    convert->inImage.connectFrom(&ari->outImage);
    convert->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

    SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;    
    reader->imageData.connectFrom(&convert->outImage);    

    SoRef<SoVolumeData> filledSlice = new SoVolumeData;
    filledSlice->setReader(*reader);

    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
    auto size = tempSlice->getDimension();
    auto slice_num = GetSliceNumber();
    SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		filledSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	filledSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

    auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
    //apply changed slice to current slice
    auto volumeData = GetMaskVolume();    
    if(axis == MedicalHelper::AXIAL) {
        tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox = volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::CORONAL) {
        tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata,sourcedata,dataBufferObj->getSize());		
    }else if(axis == MedicalHelper::SAGITTAL) {
        tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

        memcpy(targetdata,sourcedata,dataBufferObj->getSize());
    }
    int editionId;
    volumeData->startEditing(editionId);
    volumeData->editSubVolume(tBox, tBufferObj.ptr());
    volumeData->finishEditing(editionId);
    pPointer->HistoryFromTool(editionId);

    tBufferObj->unmap();
    dataBufferObj->unmap();

    tBufferObj = nullptr;
    dataBufferObj = nullptr;
}

auto OivFiller::Impl::GetMaskVolume()->SoVolumeData* {
    return dynamic_cast<SoVolumeData*>(SoNode::getByName(maskVolumeDataNodeName));
}

OivFiller::OivFiller(Oiv2DDrawer* parent) : d{ new Impl } {
    d->pPointer = parent;
    SoEventCallback* eventCallback = new SoEventCallback;
    eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
    this->addChild(eventCallback);


}

OivFiller::~OivFiller() {
    
}

auto OivFiller::SetFillMode(bool isFill) -> void {
    d->isFill = isFill;
}
auto OivFiller::SetAxis(const MedicalHelper::Axis& axis) -> void {
    d->axis = axis;
}
void OivFiller::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
    auto action = node->getAction();

    auto d = static_cast<Impl*>(data);
    if(nullptr == d) {
        action->setHandled();
        return;
    }
    const SoEvent* event = node->getEvent();
    auto viewportRegion = action->getViewportRegion();
    auto mousePos =  event->getPosition(viewportRegion);
    SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
    rayPick.setPoint(mousePos);
	rayPick.apply(action->getPickRoot());

    SoPickedPoint* pickedPt = rayPick.getPickedPoint();

    if(pickedPt){
        if(SO_MOUSE_PRESS_EVENT(event,BUTTON1)) {
            d->cur_seed = pickedPt->getPoint();
            //d->cur_label = d->GetLabelValue();
            d->cur_pick = d->GetLabelValue();
            if(d->isFill && d->cur_pick ==0){
                d->CopyCurrentSlice();
                d->FillProcess();
            }else if(!d->isFill&&static_cast<int>(d->cur_label) == d->cur_pick) {
                d->CopyCurrentSlice();
                d->EraseProcess();
            }
        }else if(SO_MOUSE_RELEASE_EVENT(event,BUTTON1)) {
            d->cur_seed.setValue(0.0,0.0,0.0);
        }
    }
    action->setHandled();
}
