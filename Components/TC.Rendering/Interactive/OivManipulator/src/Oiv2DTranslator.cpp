#pragma warning(push)
#pragma warning(disable:4819)

#include <Inventor/draggers/SoTranslate2Dragger.h>
#pragma warning(pop)

#include "Oiv2DTranslator.h"

struct Oiv2DTranslator::Impl {
    SoTranslate2Dragger* translate2D{ nullptr };
    SoSeparator* contents{ nullptr };
    SoNode* xAxisFeedback {nullptr};
    SoNode* yAxisFeedback{ nullptr };

    bool showGuide{false};
};

Oiv2DTranslator::Oiv2DTranslator() :d{ new Impl } {
    d->translate2D = new SoTranslate2Dragger;
    d->xAxisFeedback = d->translate2D->getPart("xAxisFeedback", true);    
    d->yAxisFeedback = d->translate2D->getPart("yAxisFeedback", true);    
    
    d->translate2D->setPart("xAxisFeedback", new SoSeparator);
    d->translate2D->setPart("yAxisFeedback", new SoSeparator);
}

Oiv2DTranslator::~Oiv2DTranslator() {
    
}

auto Oiv2DTranslator::getActiveParts() -> SoNode* {
    return d->translate2D->getPart("translatorActive", false);
}


auto Oiv2DTranslator::setContentsRoot(SoSeparator* root) -> void {
    d->contents = root;
    d->translate2D->setPart("translatorActive", d->contents);
    d->translate2D->setPart("translator", d->contents);
}

auto Oiv2DTranslator::getContentsRoot() -> SoSeparator* {
    return d->contents;    
}

auto Oiv2DTranslator::showGuideline(bool show) -> void {
    d->showGuide = show;
    if(show) {
        //현재는 default guide로 돌리고 추후 디자인 추가 가능
        d->translate2D->setPart("xAxisFeedback", d->xAxisFeedback);
        d->translate2D->setPart("yAxisFeedback", d->yAxisFeedback);
    }
    else {
        d->translate2D->setPart("xAxisFeedback", new SoSeparator);
        d->translate2D->setPart("yAxisFeedback", new SoSeparator);
    }
}

auto Oiv2DTranslator::getManipulatorRoot() -> SoNode* {
    return d->translate2D;
}