#include <QList>

#pragma warning(disable:4002)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#pragma warning(pop)

#include "OivRectangleDrawer.h"

#include <Inventor/events/SoEvent.h>

SO_NODE_SOURCE(OivRectangleDrawer);
struct OivRectangleDrawer::Impl {
    SbVec2f firstAddedPoint;
    SbVec2f lastAddedPoint;    
};

OivRectangleDrawer::OivRectangleDrawer() : d{ new Impl } {
    SO_NODE_CONSTRUCTOR(OivRectangleDrawer);
    simplificationThreshold.enableNotify(FALSE);
    simplificationThreshold.setValue(0);
    lineWidth = 2;    
    color.setValue(1, 1, 0);
    isClosed = TRUE;
    simplificationThreshold.enableNotify(TRUE);
}

OivRectangleDrawer::~OivRectangleDrawer() {
    
}

void OivRectangleDrawer::initClass() {
    SO__NODE_INIT_CLASS(OivRectangleDrawer, "RectangleDrawer", SoPolyLineScreenDrawer);
}

void OivRectangleDrawer::exitClass() {
    SO__NODE_EXIT_CLASS(OivRectangleDrawer);
}

void OivRectangleDrawer::reset() {
    
}

void OivRectangleDrawer::onKeyDown(SoHandleEventAction* action) {
    Q_UNUSED(action)
}

void OivRectangleDrawer::onMouseDown(SoHandleEventAction* action) {
    this->clear();
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    d->firstAddedPoint = pos;
    this->addPoint(pos);
}

void OivRectangleDrawer::onMouseDragging(SoHandleEventAction* action) {
    auto pt = action->getEvent()->getNormalizedPosition(action->getViewportRegion());
    auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
    d->lastAddedPoint = pos;
        
    this->clear();
    this->addPoint(d->firstAddedPoint);

    auto posX = SbVec2f(d->lastAddedPoint[0],d->firstAddedPoint[1]);
    auto posY = SbVec2f(d->firstAddedPoint[0], d->lastAddedPoint[1]);
    this->addPoint(posX);
    this->addPoint(d->lastAddedPoint);
    this->addPoint(posY);        
}

void OivRectangleDrawer::onMouseUp(SoHandleEventAction* action) {
    this->finalize(action);
    this->clear();
}