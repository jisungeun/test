#include <QApplication>
#include <QMainWindow>
#include <QShortcut>

#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>

#include <ImageViz/SoImageViz.h>

#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <LDM/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>

#include "Oiv2DDrawer.h"

static MedicalHelper::Axis viewAxis{ MedicalHelper::AXIAL };

int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	SoDB::init();

    QMainWindow* window = new QMainWindow;
	SoQt::init(window);

	SoImageViz::init();
	SoVolumeRendering::init();

	//create scene graph
	SoRef<SoSeparator> rootSceneGraph = new SoSeparator;

	auto camera = new SoOrthographicCamera;
	rootSceneGraph->addChild(camera);

	auto volSep = new SoSeparator;

	auto volData = new SoVolumeData;
	volData->fileName = "$OIVHOME/examples/data/VolumeViz/3DHEAD.ldm";
	volSep->addChild(volData);

	SoDataRange* dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange, volData);
	volSep->addChild(dataRange);

	SoMaterial* volMatl = new SoMaterial;
	volMatl->diffuseColor.setValue(1, 1, 1);
	volMatl->transparency.setValue(0.5);
	volSep->addChild(volMatl);

	SoTransferFunction* transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
	MedicalHelper::dicomCheckMonochrome1(transFunc, volData);
	volSep->addChild(transFunc);

	SoOrthoSlice* orthoSlice = new SoOrthoSlice;
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volData->data.getSize()[viewAxis] / 3;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volSep->addChild(orthoSlice);

	// create empty mask image
	auto maskSep = new SoSeparator;
	maskSep->setName("MaskSeparator");

	auto volDim = volData->getDimension();
	auto numBytes = volDim[0] * volDim[1] * volDim[2];
	auto data = new unsigned char[numBytes];

	for (auto i = 0; i < numBytes; i++) {
		data[i] = 0;
	}

	auto memObj = new SoMemoryObject(data, numBytes);

	auto volMask = new SoVolumeData;	// seg mask의 볼륨 전체 데이터
	volMask->setName("MaskVolume");
	volMask->ldmResourceParameters.getValue()->tileDimension.setValue(
		volData->ldmResourceParameters.getValue()->tileDimension.getValue());
	volMask->data.setValue(volDim, 8, memObj, SoSFArray::NO_COPY);
	volMask->extent.setValue(volData->extent.getValue());

	maskSep->addChild(volMask);

	auto maskMatl = new SoMaterial;
	maskMatl->diffuseColor.setValue(1., 1., 1.);
	//maskMatl->transparency.setValue(0.4);
	maskSep->addChild(maskMatl);

	auto maskRange = new SoDataRange;
	maskRange->min = 0;
	maskRange->max = 1;
	maskSep->addChild(maskRange);

	auto maskTF = new SoTransferFunction;
	maskTF->predefColorMap = SoTransferFunction::PredefColorMap::NONE;
	maskTF->colorMap.setNum(256 * 4);
	float* p = maskTF->colorMap.startEditing();
	for (int i = 0; i < 256; ++i) {
		if (i > 128) {
			*p++ = 1.0;
			*p++ = 0.0;
			*p++ = 0.0;
			*p++ = 0.5;
		}
		else {
			*p++ = 0.0;
			*p++ = 0.0;
			*p++ = 0.0;
			*p++ = 1.0;
		}
	}
	maskTF->colorMap.finishEditing();
	maskSep->addChild(maskTF);

	SoOrthoSlice* maskSlice = new SoOrthoSlice;
	maskSlice->setName("MaskSlice");
	maskSlice->axis = viewAxis;
	maskSlice->sliceNumber = volData->data.getSize()[viewAxis] / 3;
	maskSlice->interpolation = SoSlice::NEAREST;
	maskSep->addChild(maskSlice);

	rootSceneGraph->addChild(maskSep);
	rootSceneGraph->addChild(volSep);

	SoSeparator* drawerSep = new SoSeparator;
	drawerSep->setName("DrawerSeparator");
	drawerSep->fastEditing = SoSeparator::CLEAR_ZBUFFER;
	drawerSep->boundingBoxIgnoring = TRUE;

	static uint32_t brushSize = 5;

	Oiv2DDrawer* drawer = new Oiv2DDrawer;	// OivDrawingToolManager
	drawer->SetTargetVolume(volMask);
	//drawer->SetBrushSize(brushSize);
    drawerSep->addChild(drawer->GetSceneGraph());

	rootSceneGraph->addChild(drawerSep);

	auto viewer = new SoQtPlaneViewer(window);

	viewer->setSceneGraph(rootSceneGraph.ptr());
	viewer->setTransparencyType(SoGLRenderAction::OPAQUE_FIRST);
	viewer->setViewing(FALSE);
	viewer->viewAll();
	viewer->show();

	window->resize(800, 600);

    MedicalHelper::orientView(viewAxis, camera, volData);

	// setup shortcuts
	QShortcut* paintBrush = new QShortcut(QKeySequence("Ctrl+1"), window);
	QObject::connect(paintBrush, &QShortcut::activated, [=]() {
		//drawer->SetDrawer(DrawerToolType::Paint);
	});

	QShortcut* eraser = new QShortcut(QKeySequence("Ctrl+2"), window);
	QObject::connect(eraser, &QShortcut::activated, [=]() {
		//drawer->SetDrawer(DrawerToolType::Wipe);
	});

	QShortcut* increaseSize = new QShortcut(QKeySequence(']'), window);
	QObject::connect(increaseSize, &QShortcut::activated, [=]() {
		if (brushSize < 100) {
			brushSize++;
			//drawer->SetBrushSize(brushSize);
		}
	});

	QShortcut* decreaseSize = new QShortcut(QKeySequence('['), window);
	QObject::connect(decreaseSize, &QShortcut::activated, [=]() {
		if (brushSize > 1) {
			brushSize--;
			//drawer->SetBrushSize(brushSize);
		}
	});

	QShortcut* lassoDrawing = new QShortcut(QKeySequence("Ctrl+3"), window);
	QObject::connect(lassoDrawing, &QShortcut::activated, [=]() {
		//drawer->SetDrawer(DrawerToolType::LassoAdd);
	});

	QShortcut* lassoClearing = new QShortcut(QKeySequence("Ctrl+4"), window);
	QObject::connect(lassoClearing, &QShortcut::activated, [=]() {
		//drawer->SetDrawer(DrawerToolType::LassoSubtract);
	});

	QShortcut* changeAxisAxial = new QShortcut(Qt::Key_A, window);
	QObject::connect(changeAxisAxial, &QShortcut::activated, [=]() {
		viewAxis = MedicalHelper::AXIAL;

	    orthoSlice->axis = maskSlice->axis = viewAxis;
		orthoSlice->sliceNumber = maskSlice->sliceNumber = volData->data.getSize()[viewAxis] / 3;
		
		MedicalHelper::orientView(viewAxis, camera, volData);
	});

	QShortcut* changeAxisCoronal = new QShortcut(Qt::Key_C, window);
	QObject::connect(changeAxisCoronal, &QShortcut::activated, [=]() {
		viewAxis = MedicalHelper::CORONAL;

		orthoSlice->axis = maskSlice->axis = viewAxis;
		orthoSlice->sliceNumber = maskSlice->sliceNumber = volData->data.getSize()[viewAxis] / 3;
		MedicalHelper::orientView(viewAxis, camera, volData);				
	});

	QShortcut* changeAxisSagittal = new QShortcut(Qt::Key_S, window);
	QObject::connect(changeAxisSagittal, &QShortcut::activated, [=]() {
		viewAxis = MedicalHelper::SAGITTAL;

		orthoSlice->axis = maskSlice->axis = viewAxis;
		orthoSlice->sliceNumber = maskSlice->sliceNumber = volData->data.getSize()[viewAxis] / 3;

		MedicalHelper::orientView(viewAxis, camera, volData);			    
	});		

	// run app
    window->show();
    app.exec();

	rootSceneGraph = NULL;
	delete window;

	SoImageViz::finish();
	SoVolumeRendering::finish();

	SoDB::finish();

	return 0;
}