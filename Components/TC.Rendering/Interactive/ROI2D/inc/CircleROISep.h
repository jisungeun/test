#pragma once

#include <memory>
#include <tuple>

#include <QObject>
#include "IROISep.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.ROI2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;
class SoVertexProperty;

namespace TC {
	class TC_Rendering_Interactive_ROI2D_API CircleROISep final: public QObject,public IROISep {
		Q_OBJECT
	public:
		CircleROISep(QObject* parent = nullptr);
		~CircleROISep() override;

		auto GetRoot() -> SoSeparator* override;
		auto Clear() -> void override;
		auto ClearHighlight() -> void override;
		auto HighlightItem(int idx) -> bool override;
		auto DeleteItem(int idx) -> bool override;
		auto Activate() -> void override;
		auto Deactivate() -> void override;
		auto SetHandleSize(double radius) -> void override;
		auto ToggleVisibility(int idx, bool show) -> bool override;

		auto GetROIVertices() -> QList<QList<pointInfo>> override;
		auto AddROI(QList<pointInfo> vertices) -> void override;
		auto GetROIs()->QList<SoVertexProperty*>;		
		
		void Finish(int index);
		void Update(int index);

	signals:
		void sigFinish(int index);
		void sigUpdate(int index);
		void sigSelected(int index);

	protected:
		static void RenderCB(SoPolyLineScreenDrawer::EventArg& arg);
		static void MouseMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void MouseButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}