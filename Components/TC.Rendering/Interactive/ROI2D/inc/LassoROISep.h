#pragma once

#include <memory>

#include <QObject>
#include "IROISep.h"
#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoLassoScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.ROI2DExport.h"

class SoEventCallback;
class SoMFVec2f;
class SoVertexProperty;

namespace TC {
	class TC_Rendering_Interactive_ROI2D_API LassoROISep final: public QObject, public IROISep {
		Q_OBJECT
	public:
		LassoROISep(QObject* parent = nullptr);
		~LassoROISep() override;

		auto GetRoot() -> SoSeparator* override;
		auto Clear() -> void override;
		auto ClearHighlight() -> void override;
		auto HighlightItem(int idx) -> bool override;
		auto DeleteItem(int idx) -> bool override;
		auto SetHandleSize(double radius) -> void override;
		auto Activate() -> void override;
		auto Deactivate() -> void override;
		auto ToggleVisibility(int idx, bool show) -> bool override;
		auto AddROI(QList<pointInfo> vertices) -> void override;
		auto GetROIVertices() -> QList<QList<pointInfo>> override;
		auto GetROIs()->QList<SoVertexProperty*>;

		void Finish(int index);
		void Update(int index);

		auto SetSamplingStep(int step)->void;

	signals:
		void sigFinish(int);
		void sigUpdate(int);
		void sigSelected(int index);

	protected:
		static void lassoCallback(SoLassoScreenDrawer::EventArg& arg);
		static void lassoMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void lassoHandleCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;

	};
}