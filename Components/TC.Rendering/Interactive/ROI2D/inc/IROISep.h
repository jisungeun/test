#pragma once

#include <QList>

class SoSeparator;

namespace TC {
	using pointInfo = std::tuple<float, float, float>;	
	class IROISep {
	public:
		IROISep() = default;
		virtual ~IROISep() = default;

		virtual auto GetRoot()->SoSeparator* = 0;
		virtual auto Clear()->void = 0;
		virtual auto ClearHighlight()->void = 0;
		virtual auto HighlightItem(int idx)->bool = 0;
		virtual auto DeleteItem(int idx)->bool = 0;
		virtual auto ToggleVisibility(int idx,bool show)->bool = 0;

		virtual auto SetHandleSize(double radius)->void = 0;

		virtual auto GetROIVertices()->QList<QList<pointInfo>> = 0;

		virtual auto AddROI(QList<pointInfo> vertices)->void = 0;

		virtual auto Activate()->void = 0;
		virtual auto Deactivate()->void = 0;		
	};
}