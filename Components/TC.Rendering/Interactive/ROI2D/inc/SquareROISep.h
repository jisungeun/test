#pragma once
#include <memory>

#include <QObject>

#include "IROISep.h"
#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolyLineScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.ROI2DExport.h"

class SoEventCallback;
class SoMFVec2f;
class SoVertexProperty;


namespace TC {
    class TC_Rendering_Interactive_ROI2D_API SquareROISep final : public QObject, public IROISep {
        Q_OBJECT
    public:
        SquareROISep(QObject* parent = nullptr);
        ~SquareROISep() override;

        auto GetRoot() -> SoSeparator* override;
        auto Clear() -> void override;
        auto ClearHighlight() -> void override;
        auto HighlightItem(int idx) -> bool override;
        auto DeleteItem(int idx) -> bool override;
        auto SetHandleSize(double radius) -> void override;
        auto Activate() -> void override;
        auto Deactivate() -> void override;
        auto ToggleVisibility(int idx, bool show) -> bool override;

        auto GetROIVertices() -> QList<QList<pointInfo>> override;
        auto AddROI(QList<pointInfo> vertices) -> void override;
        auto GetROIs()->QList<SoVertexProperty*>;

        void Finish(int index);
        void Update(int index);        

    signals:
        void sigFinish(int);
        void sigUpdate(int);
        void sigSelected(int index);

    protected:
        static void rectCallback(SoPolyLineScreenDrawer::EventArg& arg);
        static void rectMoveCB(void* pImpl, SoEventCallback* eventCB);
        static void rectHandleCB(void* pImpl, SoEventCallback* eventCB);

    private:
        static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}