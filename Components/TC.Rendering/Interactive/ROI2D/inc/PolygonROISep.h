#pragma once
#include <memory>
#include <tuple>

#include "IROISep.h"
#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.ROI2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;
class SoVertexProperty;

namespace TC {
	class TC_Rendering_Interactive_ROI2D_API PolygonROISep final : public QObject, public IROISep  {
		Q_OBJECT
	public:
		PolygonROISep(QObject* parent = nullptr);
		~PolygonROISep() override;

		auto GetRoot() -> SoSeparator* override;
		auto Clear() -> void override;
		auto ClearHighlight() -> void override;
		auto HighlightItem(int idx) -> bool override;
		auto DeleteItem(int idx) -> bool override;
		auto SetHandleSize(double radius) -> void override;
		auto Activate() -> void override;
		auto Deactivate() -> void override;
		auto ToggleVisibility(int idx, bool show) -> bool override;

		auto AddROI(QList<pointInfo> vertices) -> void override;
		auto GetROIs()->QList<QList<SoVertexProperty*>>;
		auto GetROIVertices() -> QList<QList<pointInfo>> override;

		void Finish(int index);
		void Update(int index);

	signals:
		void sigFinish(int index);
		void sigUpdate(int index);
		void sigSelected(int index);

	protected:
		static void lineCallback(SoPolyLineScreenDrawer::EventArg& arg);
		static void LineMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void LineButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}