#pragma once

#include <memory>

class SoSeparator;
class SoOrthographicCamera;
class SoVolumeData;

class SceneGraph : public QObject {
    Q_OBJECT
public:
    enum class ROITool{
	    CIRCLE,
        ELLIPSOID,
        POLYGON,
        SQAURE,
        LASSO
    };

    explicit SceneGraph();
    ~SceneGraph();

    auto SetVolumeData(SoVolumeData* volumeData) -> void;
	auto GetVolumeData() -> SoVolumeData*;

    auto GetRoot() -> SoSeparator*;

    auto SwitchROI(ROITool id) -> void;

    auto Clear() -> void;

signals:
	void sigFinished();

private slots:
	void OnCircleFinished(int index);
	void OnEllipsoidFinished(int index);
	void OnPolygonFinished(int index);
    void OnSqaureFinished(int index);
    void OnLassoFinished(int index);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};