#pragma once

#include <memory>

#include <QMainWindow>

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

	auto View() -> QWidget*;

private slots:
    void OnOpen();

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};