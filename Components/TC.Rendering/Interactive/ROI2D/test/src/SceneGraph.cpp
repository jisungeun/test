#include <QDebug>

#include <CircleROISep.h>
#include <EllipsoidROISep.h>
#include <PolygonROISep.h>
#include <SquareROISep.h>
#include <LassoROISep.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>

#include "SceneGraph.h"


constexpr std::string_view refSepName = "RefImageSeparator";
constexpr std::string_view refVolumeDataName = "RefVolumeData";
constexpr std::string_view refMaterialName = "RefMaterial";
constexpr std::string_view refOrthoSliceName = "RefOrthoSlice";

constexpr std::string_view roiTranslationName = "ROITranslation";
constexpr std::string_view roiSwitchName = "ROISwitch";
constexpr std::string_view roiCircleName = "ROICircleSeparator";
constexpr std::string_view roiSquareName = "ROISqaureSeparator";

constexpr MedicalHelper::Axis viewAxis{ MedicalHelper::AXIAL };


struct SceneGraph::Impl {
    SoRef<SoSeparator> root{ nullptr };
	std::unique_ptr<TC::CircleROISep> roiCircle{ nullptr };
	std::unique_ptr<TC::EllipsoidROISep> roiEllipsoid{ nullptr };
	std::unique_ptr<TC::PolygonROISep> roiPolygon{ nullptr };
	std::unique_ptr<TC::SquareROISep> roiSquare{ nullptr };
	std::unique_ptr<TC::LassoROISep> roiLasso{ nullptr };
    auto initSceneGraph() -> void;
};

auto SceneGraph::Impl::initSceneGraph() -> void {
    root = new SoSeparator;

	// add a volume separator
	SoRef<SoSeparator> volumeSep = new SoSeparator;
    volumeSep->setName(refSepName.data());

    root->addChild(volumeSep.ptr());

	// add a roi tools
	SoRef<SoSeparator> roiGroup = new SoSeparator;

	SoRef<SoTranslation> roiTrans = new SoTranslation;
	roiTrans->setName(roiTranslationName.data());
	roiGroup->addChild(roiTrans.ptr());

	SoRef<SoSwitch> roiToolSwitch = new SoSwitch;
	roiToolSwitch->setName(roiSwitchName.data());

	roiCircle = std::make_unique<TC::CircleROISep>();
	roiToolSwitch->addChild(roiCircle->GetRoot());

	roiEllipsoid = std::make_unique<TC::EllipsoidROISep>();
	roiToolSwitch->addChild(roiEllipsoid->GetRoot());

	roiPolygon = std::make_unique<TC::PolygonROISep>();
	roiToolSwitch->addChild(roiPolygon->GetRoot());

	roiSquare = std::make_unique<TC::SquareROISep>();
	roiToolSwitch->addChild(roiSquare->GetRoot());

	roiLasso = std::make_unique<TC::LassoROISep>();
	roiToolSwitch->addChild(roiLasso->GetRoot());

	roiToolSwitch->whichChild = SO_SWITCH_ALL;

	roiGroup->addChild(roiToolSwitch.ptr());

	root->addChild(roiGroup.ptr());
}

SceneGraph::SceneGraph() : d{ std::make_unique<Impl>() } {
	d->initSceneGraph();

	connect(d->roiCircle.get(), SIGNAL(sigFinish(int)), this, SLOT(OnCircleFinished(int)));
	connect(d->roiEllipsoid.get(), SIGNAL(sigFinish(int)), this, SLOT(OnEllipsoidFinished(int)));
	connect(d->roiPolygon.get(), SIGNAL(sigFinish(int)), this, SLOT(OnPolygonFinished(int)));
	connect(d->roiSquare.get(), SIGNAL(sigFinish(int)), this, SLOT(OnSqaureFinished(int)));
	connect(d->roiLasso.get(), SIGNAL(sigFinish(int)), this, SLOT(OnLassoFinished(int)));
}

SceneGraph::~SceneGraph() {
	d->root = NULL;
}

auto SceneGraph::SetVolumeData(SoVolumeData* volumeData) -> void {
	const auto volumeSep = dynamic_cast<SoSeparator*>(d->root->getByName(refSepName.data()));
	volumeSep->removeAllChildren();

	auto dims = volumeData->data.getSize();
	auto volext = volumeData->extent.getValue().getSize();
	double zSpacing = volext[2] / (double)dims[2];


    volumeData->setName(refVolumeDataName.data());
	volumeSep->addChild(volumeData);
	
    SoRef<SoDataRange> dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange.ptr(), volumeData);
	volumeSep->addChild(dataRange.ptr());

    SoRef<SoMaterial> material = new SoMaterial;
	material->setName(refMaterialName.data());
	material->diffuseColor.setValue(1, 1, 1);
	volumeSep->addChild(material.ptr());

    SoRef<SoTransferFunction> transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
    MedicalHelper::dicomCheckMonochrome1(transFunc.ptr(), volumeData);
	volumeSep->addChild(transFunc.ptr());

    SoRef<SoOrthoSlice> orthoSlice = new SoOrthoSlice;
	orthoSlice->setName(refOrthoSliceName.data());
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volumeData->data.getSize()[viewAxis] / 2;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volumeSep->addChild(orthoSlice.ptr());

	auto roiTrans = dynamic_cast<SoTranslation*>(d->root->getByName(roiTranslationName.data()));
	roiTrans->translation.setValue(0, 0, dims[2] * zSpacing * -1);
}

auto SceneGraph::GetVolumeData() -> SoVolumeData* {
	return dynamic_cast<SoVolumeData*>(d->root->getByName(refVolumeDataName.data()));
}

auto SceneGraph::GetRoot() -> SoSeparator* {
    return d->root.ptr();
}

auto SceneGraph::SwitchROI(ROITool id) -> void {
	const auto roiSwitch = dynamic_cast<SoSwitch*>(d->root->getByName(roiSwitchName.data()));
	if (roiSwitch == nullptr) return;

	d->roiCircle->Deactivate();
	d->roiEllipsoid->Deactivate();
	d->roiPolygon->Deactivate();
	d->roiSquare->Deactivate();
	d->roiLasso->Deactivate();

	switch(id) {
	case ROITool::CIRCLE: d->roiCircle->Activate(); break;
	case ROITool::ELLIPSOID: d->roiEllipsoid->Activate(); break;
	case ROITool::POLYGON: d->roiPolygon->Activate(); break;
	case ROITool::SQAURE: d->roiSquare->Activate(); break;
	case ROITool::LASSO: d->roiLasso->Activate(); break;
	}
}

auto SceneGraph::Clear() -> void {
    const auto volumeSep = dynamic_cast<SoSeparator*>(d->root->getByName(refSepName.data()));
	volumeSep->removeAllChildren();

	d->roiCircle->Clear();
}

void SceneGraph::OnCircleFinished(int index) {
	d->roiCircle->ClearHighlight();
	d->roiEllipsoid->ClearHighlight();
	d->roiPolygon->ClearHighlight();
	d->roiSquare->ClearHighlight();
	d->roiLasso->ClearHighlight();

	d->roiCircle->HighlightItem(index);
}

void SceneGraph::OnEllipsoidFinished(int index) {
	d->roiCircle->ClearHighlight();
	d->roiEllipsoid->ClearHighlight();
	d->roiPolygon->ClearHighlight();
	d->roiSquare->ClearHighlight();
	d->roiLasso->ClearHighlight();

	d->roiEllipsoid->HighlightItem(index);
}

void SceneGraph::OnPolygonFinished(int index) {
	d->roiCircle->ClearHighlight();
	d->roiEllipsoid->ClearHighlight();
	d->roiPolygon->ClearHighlight();	
	d->roiSquare->ClearHighlight();
	d->roiLasso->ClearHighlight();

	d->roiPolygon->HighlightItem(index);
}

void SceneGraph::OnSqaureFinished(int index) {
	d->roiCircle->ClearHighlight();
	d->roiEllipsoid->ClearHighlight();
	d->roiPolygon->ClearHighlight();
	d->roiSquare->ClearHighlight();
	d->roiLasso->ClearHighlight();

	d->roiSquare->HighlightItem(index);
}

void SceneGraph::OnLassoFinished(int index) {
	d->roiCircle->ClearHighlight();
	d->roiEllipsoid->ClearHighlight();
	d->roiPolygon->ClearHighlight();
	d->roiSquare->ClearHighlight();
	d->roiLasso->ClearHighlight();

	d->roiLasso->HighlightItem(index);
}
