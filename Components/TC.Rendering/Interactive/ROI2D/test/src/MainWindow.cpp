#include <QFileDialog>
#include <QShortcut>

#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <RenderWindowSlice.h>
#include <TCImage.h>
#include <TCDataConverter.h>

#include "SceneGraph.h"
#include "ImageDataReader.h"

#include "MainWindow.h"
#include "ui_MainWindow.h"

struct MainWindow::Impl {
    Ui::MainWindow ui;

    std::unique_ptr<SceneGraph> sceneGraph{nullptr};
    std::unique_ptr<RenderWindow2D> renderWindow{ nullptr };
};

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ std::make_unique<Impl>() } {
    d->ui.setupUi(this);

	SoVolumeRendering::init();

    const auto circle = new QShortcut(QKeySequence("Ctrl+1"), this);
    connect(circle, &QShortcut::activated, this, [&]() {
	    d->sceneGraph->SwitchROI(SceneGraph::ROITool::CIRCLE);
    });

    const auto ellipsoid = new QShortcut(QKeySequence("Ctrl+2"), this);
    connect(ellipsoid, &QShortcut::activated, this, [&]() {
	    d->sceneGraph->SwitchROI(SceneGraph::ROITool::ELLIPSOID);
    });

	const auto polygon = new QShortcut(QKeySequence("Ctrl+3"), this);
    connect(polygon, &QShortcut::activated, this, [&]() {
	    d->sceneGraph->SwitchROI(SceneGraph::ROITool::POLYGON);
    });

    const auto square = new QShortcut(QKeySequence("Ctrl+4"), this);
    connect(square, &QShortcut::activated, this, [&]() {
        d->sceneGraph->SwitchROI(SceneGraph::ROITool::SQAURE);
        });
    const auto lasso = new QShortcut(QKeySequence("Ctrl+5"), this);
    connect(lasso, &QShortcut::activated, this, [&]() {
        d->sceneGraph->SwitchROI(SceneGraph::ROITool::LASSO);
        });

    connect(d->ui.openButton, &QPushButton::clicked, this, &MainWindow::OnOpen);

    d->sceneGraph = std::make_unique<SceneGraph>();

    d->renderWindow = std::make_unique<RenderWindow2D>(d->ui.widget);
    d->renderWindow->setSceneGraph(d->sceneGraph->GetRoot());

    const auto viewLayout = new QHBoxLayout;
    viewLayout->addWidget(d->renderWindow.get());
    d->ui.widget->setLayout(viewLayout);

    //////////////////////////////////////////////////////////////////
    const ImageDataReader reader;
	auto image = reader.Read("F:/CellLibrary/210329_FL_gallery/HeLa-Rab/20201229.104551.194.HeLa-Rab5a-Rab7a-001/20201229.104551.194.HeLa-Rab5a-Rab7a-001.TCF", true, 0);

    d->sceneGraph->Clear();

    TCDataConverter converter;
    const auto imageVolume = SoRef<SoVolumeData>(converter.ImageToSoVolumeData(image, true));

	d->sceneGraph->SetVolumeData(imageVolume.ptr());
    d->renderWindow->ResetView2D(Direction2D::XY);
}

MainWindow::~MainWindow() {
    SoVolumeRendering::finish();
}

auto MainWindow::View() -> QWidget* {
	return d->ui.widget;
}


void MainWindow::OnOpen() {
    const auto path = QFileDialog::getOpenFileName(this, "Open TCF", "/home", "TCF (*.tcf)");
    if (path.isEmpty()) return;

    const ImageDataReader reader;
	auto image = reader.Read(path, true, 0);

    d->sceneGraph->Clear();

    TCDataConverter converter;
    const auto imageVolume = SoRef<SoVolumeData>(converter.ImageToSoVolumeData(image, true));

	d->sceneGraph->SetVolumeData(imageVolume.ptr());
    d->renderWindow->ResetView2D(Direction2D::XY);
}