#include <iostream>

#include <QApplication>

#include <Inventor/Qt/SoQt.h>

#include "MainWindow.h"

void main(int argc,char** argv) {
	QApplication app(argc, argv);

	MainWindow w;
	SoQt::init(w.View());

	SoQt::show(&w);
    SoQt::mainLoop();

    SoQt::finish();
}