#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#pragma warning(pop)

#include <OivHoveringLasso.h>
#include "LassoROISep.h"

#include <Medical/helpers/MedicalHelper.h>

namespace TC {
	struct LassoROISep::Impl {
        SoSeparator* root;
        SoSeparator* handleRoot{ nullptr };
        SoSwitch* drawerSwitch{ nullptr };
        SoRef<SoVertexProperty> circleProp{ nullptr };
        //SoLassoScreenDrawer* lasso{ nullptr };
        OivHoveringLasso* lasso{ nullptr };
        SoSwitch* callbackSwitch{ nullptr };
        SoEventCallback* callback{ nullptr };

        bool isActivated{ false };
        double handleRadius{ 0.5 };
        bool isInHandle{ false };
        int lassoIdx{ -1 };
        QList<SbVec3f> lassoCenterPt;
        SbVec3f startPoint;
        SoRef<SoVertexProperty> startProp{ nullptr };
                
        QList<SoMaterial*> handleMatl;
        QList<SoTranslation*> handleTrans;
        QList<SoVertexProperty*> lassoProp;
        QList<SoDrawStyle*> lassoStyle;
        QList<SoMaterial*> lassoColor;

        LassoROISep* thisPointer;
	};
    LassoROISep::LassoROISep(QObject* parent) : QObject(parent), IROISep(), d{ new Impl } {
        Init();
        d->thisPointer = this;        
	}
    LassoROISep::~LassoROISep() {
		
	}
    auto LassoROISep::SetSamplingStep(int step) -> void {
        d->lasso->simplificationThreshold = step;
	}
    auto LassoROISep::Init() -> void {
        d->startProp = new SoVertexProperty;
        //d->lasso = new SoLassoScreenDrawer;
        d->lasso = new OivHoveringLasso;
        d->lasso->simplificationThreshold = 1;        
        d->root = new SoSeparator;
        d->handleRoot = new SoSeparator;
        SoRef<SoLightModel> lightModel = new SoLightModel;
        lightModel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightModel.ptr());

        d->drawerSwitch = new SoSwitch;
        d->drawerSwitch->addChild(d->lasso);
        d->drawerSwitch->whichChild = -1;

        d->root->addChild(d->drawerSwitch);
        d->root->addChild(d->handleRoot);

        d->lasso->setUserData(d.get());
        d->lasso->onFinish.add(lassoCallback);

        d->circleProp = new SoVertexProperty;
        auto radius = d->handleRadius;
        auto div = 100.0;
        for (auto i = 0; i < 100; i++) {
            auto x_pos = radius * cos(2 * M_PI / div * i);
            auto y_pos = radius * sin(2 * M_PI / div * i);
            d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
        }

        d->callback = new SoEventCallback;
        d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), lassoHandleCB, d.get());
        d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), lassoMoveCB, d.get());

        d->callbackSwitch = new SoSwitch;
        d->callbackSwitch->addChild(d->callback);
        d->callbackSwitch->whichChild = -1;

        d->root->insertChild(d->callbackSwitch, 0);
	}
    auto LassoROISep::ToggleVisibility(int idx, bool show) -> bool {
		if(d->lassoColor.count() <= idx) {
            return false;
		}
        if(show) {
            d->lassoColor[idx]->transparency = 0;
            d->handleMatl[idx]->transparency = 0;
        }else {
            d->lassoColor[idx]->transparency = 1;
            d->handleMatl[idx]->transparency = 1;
        }
        return true;
	}
    auto LassoROISep::DeleteItem(int idx) -> bool {
        if (d->lassoProp.count() <= idx) {
            return false;
        }
        //remove scene graph first
        d->handleRoot->removeChild(idx + 1);//first child is SoLightModel

        d->lassoProp.removeAt(idx);
        d->lassoStyle.removeAt(idx);
        d->lassoColor.removeAt(idx);
        d->handleMatl.removeAt(idx);
        d->handleTrans.removeAt(idx);
        
        return true;
	}
    auto LassoROISep::SetHandleSize(double rad) -> void {
        d->handleRadius = rad;
        auto div = 100.0;
        for (auto i = 0; i < 100; i++) {
            auto x_pos = rad * cos(2 * M_PI / div * i);
            auto y_pos = rad * sin(2 * M_PI / div * i);
            d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
        }
        d->circleProp->touch();
	}
    auto LassoROISep::GetRoot() -> SoSeparator* {
        return d->root;
	}    
    auto LassoROISep::GetROIVertices() -> QList<QList<pointInfo>> {
        QList<QList<pointInfo>> result;
        for (auto i = 0; i < d->lassoProp.count(); i++) {
            QList<pointInfo> vertexList;
            const auto vp = d->lassoProp[i];
            const auto vn = vp->vertex.getNum();
            for (auto j = 0; j < vn; j++) {
                const auto v = vp->vertex.getValues(0)[j];
                pointInfo info = std::make_tuple(v[0], v[1], v[2]);
                vertexList.append(info);
            }
            result.append(vertexList);
        }
        return result;
	}

    auto LassoROISep::GetROIs() -> QList<SoVertexProperty*> {
        return d->lassoProp;
	}
    auto LassoROISep::ClearHighlight() -> void {
        for (auto i = 0; i < d->lassoStyle.count(); i++) {
            d->lassoStyle[i]->lineWidth = 2;
            d->lassoColor[i]->ambientColor.setValue(1, 1, 0);
            d->lassoColor[i]->diffuseColor.setValue(1, 1, 0);
        }
	}
    auto LassoROISep::HighlightItem(int idx) -> bool {        
        if (d->lassoStyle.count() <= idx || idx == -1) {
            return false;
        }
        ClearHighlight();
        d->lassoStyle[idx]->lineWidth = 3;
        d->lassoColor[idx]->ambientColor.setValue(0, 1, 0);
        d->lassoColor[idx]->diffuseColor.setValue(0, 1, 0);
        return true;
	}
    void LassoROISep::Activate() {
        d->isActivated = true;
        d->callbackSwitch->whichChild = 0;
	}
    void LassoROISep::Deactivate() {
        ClearHighlight();
        d->isActivated = false;
        d->callbackSwitch->whichChild = -1;
	}
    auto LassoROISep::Clear() -> void {
        d->handleRoot->removeAllChildren();
        SoRef<SoLightModel> lightModel = new SoLightModel;
        lightModel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightModel.ptr());
    	d->handleMatl.clear();
    	d->handleTrans.clear();
        d->lassoProp.clear();
        d->lassoColor.clear();
        d->lassoStyle.clear();
        d->lassoCenterPt.clear();
	}
    auto LassoROISep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion) -> std::vector<SbVec3f> {
        std::vector<SbVec3f> result;

        SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
        for (int i = 0; i < source->getNum(); i++) {

            SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
            normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
            normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

            rayPick.setNormalizedPoint(normalizedPoint);
            rayPick.apply(targetNode);

            SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
            if (pickedPoint) {
                result.push_back(pickedPoint->getPoint());
            }
        }
        return result;
    }
    auto LassoROISep::AddROI(QList<pointInfo> vertices) -> void {
        std::vector<SbVec3f> resultRayCast;
        for(auto i=0;i<vertices.count()-1;i++) {
            const auto x = std::get<0>(vertices[i]);
            const auto y = std::get<1>(vertices[i]);
            const auto z = std::get<2>(vertices[i]);
            resultRayCast.push_back(SbVec3f(x, y, z));
        }
        //Add Lasso
        SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
        drawStyle->lineWidth = 2;
        d->lassoStyle.push_back(drawStyle.ptr());

        SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
        for (auto i = 0; i < resultRayCast.size(); i++) {
            vertexProp->vertex.set1Value(i, resultRayCast[i]);
        }
        vertexProp->vertex.set1Value(resultRayCast.size(), resultRayCast[0]);

        d->lassoProp.push_back(vertexProp.ptr());

        SoRef<SoLineSet> lasso = new SoLineSet;
        lasso->vertexProperty = vertexProp.ptr();

        SoRef<SoMaterial> lineMatl = new SoMaterial;
        lineMatl->ambientColor.setValue(1, 1, 0);
        lineMatl->diffuseColor.setValue(1, 1, 0);
        d->lassoColor.push_back(lineMatl.ptr());

        SoRef<SoSeparator> lassoSep = new SoSeparator;
        lassoSep->addChild(drawStyle.ptr());
        lassoSep->addChild(lineMatl.ptr());
        lassoSep->addChild(lasso.ptr());

        SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
        circleStyle->setName("circleStyle");
        circleStyle->style = SoDrawStyle::FILLED;

        SoRef<SoFaceSet> faceSet = new SoFaceSet;
        faceSet->vertexProperty = d->circleProp.ptr();

        SoRef<SoSeparator> circleSep = new SoSeparator;
        circleSep->addChild(circleStyle.ptr());

        //Calculate Center of Lasso
        SbVec3f avg(0, 0, 0);
        for (auto i = 0; i < resultRayCast.size(); i++) {
            avg += resultRayCast[i];
        }
        avg /= resultRayCast.size();

        d->lassoCenterPt.append(avg);

        SoRef<SoSeparator> circle = new SoSeparator;
        circleSep->addChild(circle.ptr());
        SoRef<SoMaterial> circleMatl = new SoMaterial;
        circleMatl->ambientColor.setValue(1, 0, 0);
        circleMatl->diffuseColor.setValue(1, 0, 0);
        SoRef<SoTranslation> circleTrans = new SoTranslation;
        circleTrans->translation.setValue(avg);
        circle->addChild(circleMatl.ptr());
        circle->addChild(circleTrans.ptr());
        circle->addChild(faceSet.ptr());

        d->handleTrans.push_back(circleTrans.ptr());
        d->handleMatl.push_back(circleMatl.ptr());

        SoRef<SoSeparator> roiSep = new SoSeparator;
        roiSep->addChild(circleSep.ptr());
        roiSep->addChild(lassoSep.ptr());

        d->handleRoot->addChild(roiSep.ptr());
        d->thisPointer->Finish(d->lassoProp.count() - 1);

        d->isInHandle = false;
        d->lassoIdx = -1;
    }
    void LassoROISep::lassoCallback(SoLassoScreenDrawer::EventArg& arg) {
        SoLassoScreenDrawer* source = static_cast<SoLassoScreenDrawer*>(arg.getSource());
        SoHandleEventAction* action = arg.getAction();
        auto dd = static_cast<Impl*>(source->getUserData());
        if(source->point.getNum() < 3) {
            source->clear();
            return;
        }
        
        auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());
        if (resultRayCast.size() < 3) {
            source->clear();
            return;
        }
        //Add Lasso
        SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
        drawStyle->lineWidth = 2;
        dd->lassoStyle.push_back(drawStyle.ptr());

        SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
        for (auto i = 0; i < resultRayCast.size(); i++) {
            vertexProp->vertex.set1Value(i, resultRayCast[i]);            
        }
        vertexProp->vertex.set1Value(resultRayCast.size(), resultRayCast[0]);

        dd->lassoProp.push_back(vertexProp.ptr());

        SoRef<SoLineSet> lasso = new SoLineSet;
        lasso->vertexProperty = vertexProp.ptr();

        SoRef<SoMaterial> lineMatl = new SoMaterial;
        lineMatl->ambientColor.setValue(1, 1, 0);
        lineMatl->diffuseColor.setValue(1, 1, 0);
        dd->lassoColor.push_back(lineMatl.ptr());

        SoRef<SoSeparator> lassoSep = new SoSeparator;
        lassoSep->addChild(drawStyle.ptr());
        lassoSep->addChild(lineMatl.ptr());
        lassoSep->addChild(lasso.ptr());

        SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
        circleStyle->setName("circleStyle");
        circleStyle->style = SoDrawStyle::FILLED;

        SoRef<SoFaceSet> faceSet = new SoFaceSet;
        faceSet->vertexProperty = dd->circleProp.ptr();

        SoRef<SoSeparator> circleSep = new SoSeparator;
        circleSep->addChild(circleStyle.ptr());

        //Calculate Center of Lasso
        SbVec3f avg(0, 0, 0);
        for(auto i=0;i<resultRayCast.size();i++) {
            avg += resultRayCast[i];
        }
        avg /= resultRayCast.size();

        dd->lassoCenterPt.append(avg);
        SoRef<SoSeparator> circle = new SoSeparator;
        circleSep->addChild(circle.ptr());
        SoRef<SoMaterial> circleMatl = new SoMaterial;
        circleMatl->ambientColor.setValue(1, 0, 0);
        circleMatl->diffuseColor.setValue(1, 0, 0);
        SoRef<SoTranslation> circleTrans = new SoTranslation;
        circleTrans->translation.setValue(avg);
        circle->addChild(circleMatl.ptr());
        circle->addChild(circleTrans.ptr());
        circle->addChild(faceSet.ptr());

        dd->handleTrans.push_back(circleTrans.ptr());
        dd->handleMatl.push_back(circleMatl.ptr());

        SoRef<SoSeparator> roiSep = new SoSeparator;
        roiSep->addChild(circleSep.ptr());
        roiSep->addChild(lassoSep.ptr());

        dd->handleRoot->addChild(roiSep.ptr());
        dd->thisPointer->Finish(dd->lassoProp.count() - 1);

        dd->drawerSwitch->whichChild = -1;                
	}
    void LassoROISep::lassoHandleCB(void* pImpl, SoEventCallback* eventCB) {
        auto dd = static_cast<Impl*>(pImpl);
        if (false == dd->isActivated) {
            eventCB->releaseEvents();
            return;
        }
        const SoEvent* event = eventCB->getEvent();
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
        if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

            SoRayPickAction pickAction = SoRayPickAction(myRegion);
            pickAction.setPoint(event->getPosition());
            pickAction.setSceneManager(action->getSceneManager());
            pickAction.apply(action->getPickRoot());
            auto p = pickAction.getPickedPoint();
            if (p == NULL) {
                eventCB->releaseEvents();
                return;
            }
            auto point = p->getPoint();
            dd->isInHandle = false;
            dd->lassoIdx = -1;
            for (auto i = 0; i < dd->lassoProp.count(); i++) {
                auto dist = sqrt(pow(dd->lassoCenterPt[i][0] - point[0], 2) + pow(dd->lassoCenterPt[i][1] - point[1], 2));
                if (dist > dd->handleRadius) {
                    continue;
                }
                dd->isInHandle = true;
                dd->lassoIdx = i;
                break;                
            }
            if(false == dd->isInHandle) {
                dd->drawerSwitch->whichChild = 0;
            }else {
                dd->handleMatl[dd->lassoIdx]->ambientColor.setValue(1, 1, 0);
                dd->handleMatl[dd->lassoIdx]->diffuseColor.setValue(1, 1, 0);
                dd->startPoint = point;
                dd->startProp->vertex.copyFrom(dd->lassoProp[dd->lassoIdx]->vertex);

                dd->thisPointer->HighlightItem(dd->lassoIdx);                                
            }
        }
        else if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            if (dd->isInHandle) {
                SoHandleEventAction* action = eventCB->getAction();
                const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
                SoRayPickAction pickaction = SoRayPickAction(myRegion);
                pickaction.setPoint(event->getPosition());
                pickaction.setSceneManager(action->getSceneManager());
                pickaction.apply(action->getPickRoot());
                auto p = pickaction.getPickedPoint();

                dd->lassoCenterPt[dd->lassoIdx] = p->getPoint();
                
                dd->handleMatl[dd->lassoIdx]->ambientColor.setValue(1, 0, 0);
                dd->handleMatl[dd->lassoIdx]->diffuseColor.setValue(1, 0, 0);

                dd->isInHandle = false;
                dd->lassoIdx = -1;
            }
        }
        if (dd->isInHandle) {
            eventCB->setHandled();
            return;
        }
        eventCB->releaseEvents();
    }
    void LassoROISep::lassoMoveCB(void* pImpl, SoEventCallback* eventCB) {
        auto dd = static_cast<Impl*>(pImpl);
        if (false == dd->isActivated) {
            eventCB->releaseEvents();
            return;
        }
        if (dd->isInHandle) {
            const SoEvent* event = eventCB->getEvent();
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
            SoRayPickAction pickaction = SoRayPickAction(myRegion);
            pickaction.setPoint(event->getPosition());
            pickaction.setSceneManager(action->getSceneManager());
            pickaction.apply(action->getPickRoot());
            auto p = pickaction.getPickedPoint();
            if (p == NULL) {
                //broadcast mouse event to further callback				
                eventCB->releaseEvents();
                return;
            }
            const auto curPos = p->getPoint();
            const auto zPos = dd->lassoProp[dd->lassoIdx]->vertex.getValues(0)[0][2];
            for(auto i=0;i<dd->lassoProp[dd->lassoIdx]->vertex.getNum();i++) {
                auto newPosition = dd->startProp->vertex.getValues(0)[i] - dd->startPoint + curPos;
                newPosition[2] = zPos;
                dd->lassoProp[dd->lassoIdx]->vertex.set1Value(i, newPosition);
            }

            dd->handleTrans[dd->lassoIdx]->translation.setValue(p->getPoint());

            eventCB->setHandled();
            dd->thisPointer->Update(dd->lassoIdx);
            return;
        }
        eventCB->releaseEvents();
    }
    void LassoROISep::Finish(int index) {
        emit sigFinish(index);
	}
    void LassoROISep::Update(int index) {
        emit sigUpdate(index);
	}
}
