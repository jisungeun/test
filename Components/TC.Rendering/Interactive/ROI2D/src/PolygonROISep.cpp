#pragma warning(push)
#pragma warning(disable:4819)

#include "OivMultiLineDrawer.h"
#include "PolygonROISep.h"

#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#include <Inventor/sys/glew.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

namespace TC {
	struct PolygonROISep::Impl {
		SoSeparator* root{ nullptr };
		SoSeparator* handleRoot{ nullptr };
		OivMultiLineDrawer* line{ nullptr };
		SoEventCallback* callback{ nullptr };
		SoRef<SoVertexProperty> circleProp{ nullptr };
		SoSwitch* drawerSwitch{ nullptr };
		SoSwitch* callbackSwitch{ nullptr };

		PolygonROISep* instance = nullptr;

		//container
		double handleRadius{ 0.5 };
		bool isInHandle{ false };
		bool inProcess{ false };
		int lineIdx = -1;
		int startIdx = -1;
		int countIdx = -1;
		QList<QList<SbVec3f>> linePoints[2];
		QList<QList<SoMaterial*>> handleMatl[2];
		QList<QList<SoTranslation*>> lineTrans[2];
		QList<QList<SoVertexProperty*>> lineProp;
		QList<QList<SoMaterial*>> lineMatl;

		QList<std::vector<SbVec3f>> linePtStream;

		bool isActivated{ false };

		auto findLength(std::vector<SbVec3f> inputVertices)->double;
		auto findArea(std::vector<SbVec3f> inputVertices)->double;
		auto isScrewed(std::vector<SbVec3f> inputVertices)->bool;
		auto onSegment(SbVec3f p, SbVec3f q, SbVec3f r)->bool;
		auto orientation(SbVec3f p, SbVec3f q, SbVec3f r)->int;
		auto isIntersect(SbVec3f p1, SbVec3f q1, SbVec3f p2, SbVec3f q2)->bool;
	};
	auto PolygonROISep::Impl::isIntersect(SbVec3f p1, SbVec3f q1, SbVec3f p2, SbVec3f q2) -> bool {
		int o1 = orientation(p1, q1, p2);
		int o2 = orientation(p1, q1, q2);
		int o3 = orientation(p2, q2, p1);
		int o4 = orientation(p2, q2, q1);

		// General case
		if (o1 != o2 && o3 != o4) {
			return true;
		}

		// Special Cases
		// p1, q1 and p2 are collinear and p2 lies on segment p1q1
		if (o1 == 0 && onSegment(p1, p2, q1)) return true;

		// p1, q1 and q2 are collinear and q2 lies on segment p1q1
		if (o2 == 0 && onSegment(p1, q2, q1)) return true;

		// p2, q2 and p1 are collinear and p1 lies on segment p2q2
		if (o3 == 0 && onSegment(p2, p1, q2)) return true;

		// p2, q2 and q1 are collinear and q1 lies on segment p2q2
		if (o4 == 0 && onSegment(p2, q1, q2)) return true;

		return false; // Doesn't fall in any of the above cases
	}
	auto PolygonROISep::Impl::orientation(SbVec3f p, SbVec3f q, SbVec3f r) -> int {
		int val = (q[1] - p[1]) * (r[0] - q[0]) -
			(q[0] - p[0]) * (r[1] - q[1]);

		if (val == 0) {
			return 0;  // collinear
		}

		return (val > 0) ? 1 : 2; // clock or counterclock wise
	}
	auto PolygonROISep::Impl::onSegment(SbVec3f p, SbVec3f q, SbVec3f r) -> bool {
		if (q[0] <= std::max(p[0], r[0]) && q[0] >= std::min(p[0], r[0]) &&
			q[1] <= std::max(p[1], r[1]) && q[1] >= std::min(p[1], r[1])) {
			return true;
		}

		return false;
	}
	auto PolygonROISep::Impl::isScrewed(std::vector<SbVec3f> inputVertices) -> bool {
		if (inputVertices.size() < 3) {
			return false;
		}
		for (auto i = 0; i < inputVertices.size() - 2; i++) {
			auto i0 = i;
			auto i1 = (i + 1) % inputVertices.size();
			for (auto j = i1 + 1; j < inputVertices.size(); j++) {
				auto j1 = (j + 1) % inputVertices.size();
				if (j1 == i0) {
					continue;
				}
				if (isIntersect(inputVertices[i0], inputVertices[i1], inputVertices[j], inputVertices[j1])) {
					return true;
				}
			}
		}
		return false;
	}
	auto PolygonROISep::Impl::findArea(std::vector<SbVec3f> inputVertices) -> double {
		double area = 0.0;
		int j = inputVertices.size() - 1;
		for (auto i = 0; i < inputVertices.size(); i++) {
			area += (inputVertices[j][0] + inputVertices[i][0]) * (inputVertices[j][1] - inputVertices[i][1]);
			j = i;
		}
		return abs(area);
	}
	auto PolygonROISep::Impl::findLength(std::vector<SbVec3f> inputVertices) -> double {
		double length = 0.0;
		for (auto i = 0; i < inputVertices.size(); i++) {
			length += sqrt(pow(inputVertices[i][0] - inputVertices[(i + 1) % 2][0], 2) + pow(inputVertices[i][1] - inputVertices[(i + 1) % 2][1], 2));
		}
		return length;
	}

	PolygonROISep::PolygonROISep(QObject* parent) : QObject(parent), IROISep(), d{ new Impl } {
		Init();
		d->instance = this;
	}
	PolygonROISep::~PolygonROISep() {

	}
	auto PolygonROISep::ToggleVisibility(int idx, bool show) -> bool {
		if (d->lineMatl.count() <= idx) {
			return false;
		}
		if (show) {
			for(auto matl : d->lineMatl[idx]) {
				matl->transparency = 0;
			}
			for(auto handleMatl : d->handleMatl[0][idx]) {
				handleMatl->transparency = 0;
			}
			for (auto handleMatl : d->handleMatl[1][idx]) {
				handleMatl->transparency = 0;
			}
		}
		else {
			for (auto matl : d->lineMatl[idx]) {
				matl->transparency = 1;
			}
			for (auto handleMatl : d->handleMatl[0][idx]) {
				handleMatl->transparency = 1;
			}
			for (auto handleMatl : d->handleMatl[1][idx]) {
				handleMatl->transparency = 1;
			}
		}
		return true;
	}

	auto PolygonROISep::SetHandleSize(double radius) -> void {
		d->handleRadius = radius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}
		d->circleProp->touch();
    }
	void PolygonROISep::Activate() {
		d->callbackSwitch->whichChild = 0;
		d->line->Activate();
		d->isActivated = true;
	}

	void PolygonROISep::Deactivate() {
		ClearHighlight();
		d->callbackSwitch->whichChild = -1;
		d->line->Deactivate();
		d->isActivated = false;
	}
	auto PolygonROISep::Init()->void {
		d->callbackSwitch = new SoSwitch;
		d->callbackSwitch->whichChild = -1;
		d->drawerSwitch = new SoSwitch;
		d->drawerSwitch->whichChild = -1;
		d->line = new OivMultiLineDrawer;
		d->root = new SoSeparator;
		d->handleRoot = new SoSeparator;
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->drawerSwitch->addChild(d->line);
		d->root->addChild(d->drawerSwitch);
		d->root->addChild(d->handleRoot);

		d->line->setUserData(d.get());
		d->line->onFinish.add(lineCallback);

		d->circleProp = new SoVertexProperty;
		auto radius = d->handleRadius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}

		d->callback = new SoEventCallback;
		d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), LineButtonCB, d.get());
		d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), LineMoveCB, d.get());
		d->callbackSwitch->addChild(d->callback);
		d->root->insertChild(d->callbackSwitch, 0);
	}

	auto PolygonROISep::ClearHighlight() -> void {
        for(auto matlList : d->lineMatl) {
            for(auto matl: matlList) {
				matl->ambientColor.setValue(1, 1, 0);
				matl->diffuseColor.setValue(1, 1, 0);
            }
        }
    }

	auto PolygonROISep::HighlightItem(int idx) -> bool {
		if (d->lineProp.count() <= idx) {
			return false;
		}
		if (d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		ClearHighlight();
		for(auto matl: d->lineMatl[idx]) {
			matl->ambientColor.setValue(0, 1, 0);
			matl->diffuseColor.setValue(0, 1, 0);
		}
		return true;
    }

	auto PolygonROISep::DeleteItem(int idx) -> bool {
        if(d->lineProp.count() <= idx) {
			return false;
        }
		if(d->handleRoot->getNumChildren()<idx) {
			return false;
		}
		d->handleRoot->removeChild(idx+1);
		for(auto i=0;i<2;i++) {
			d->linePoints[i].removeAt(idx);
			d->handleMatl[i].removeAt(idx);
			d->lineTrans[i].removeAt(idx);
		}
		d->lineProp.removeAt(idx);
		d->linePtStream.removeAt(idx);
		d->lineMatl.removeAt(idx);

		return true;
    }

	auto PolygonROISep::Clear()->void {
		d->handleRoot->removeAllChildren();
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->linePoints[0].clear();
		d->linePoints[1].clear();
		d->handleMatl[0].clear();
		d->handleMatl[1].clear();
		d->lineTrans[0].clear();
		d->lineTrans[1].clear();
		d->lineProp.clear();
		d->linePtStream.clear();
		d->lineMatl.clear();
	}
	void PolygonROISep::Finish(int index) {
		emit sigFinish(index);
	}
	void PolygonROISep::Update(int index) {
		emit sigUpdate(index);
	}

	auto PolygonROISep::GetROIVertices() -> QList<QList<pointInfo>> {
		QList<QList<pointInfo>> result;		
		for(auto i=0;i<d->lineProp.count();i++) {
			const auto lineSet = d->lineProp[i];
			QList<pointInfo> vertexList;
			//for 0, add to vertices			
			if(lineSet.count()<1) {				
				return {};
			}
			const auto firstPt = lineSet[0];
			const auto fp0 = firstPt->vertex.getValues(0)[0];
			const auto fp1 = firstPt->vertex.getValues(0)[1];
			pointInfo firstSt = std::make_tuple(fp0[0], fp0[1], fp0[2]);
			pointInfo firstEd = std::make_tuple(fp1[0], fp1[1], fp1[2]);
			vertexList.append(firstSt);
			vertexList.append(firstEd);
			for(auto j=1;j<lineSet.count();j++) {
				const auto pt = lineSet[j]->vertex.getValues(0)[1];
				pointInfo ed = std::make_tuple(pt[0], pt[1], pt[2]);
				vertexList.append(ed);
			}
			result.append(vertexList);
		}
		return result;
	}	
	auto PolygonROISep::GetROIs() -> QList<QList<SoVertexProperty*>> {
		return d->lineProp;
	}
	auto PolygonROISep::GetRoot()->SoSeparator* {
		return d->root;
	}
	auto PolygonROISep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
		std::vector<SbVec3f> result;

		SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
		for (int i = 0; i < source->getNum(); i++) {

			SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
			normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
			normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

			rayPick.setNormalizedPoint(normalizedPoint);
			rayPick.apply(targetNode);

			SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
			if (pickedPoint) {
				result.push_back(pickedPoint->getPoint());
			}
		}

		return result;
	}
	auto PolygonROISep::AddROI(QList<pointInfo> vertices) -> void {
		std::vector<SbVec3f> resultRayCast;
		for(auto i=0;i<vertices.count() - 1;i++) {
			const auto x = std::get<0>(vertices[i]);
			const auto y = std::get<1>(vertices[i]);
			const auto z = std::get<2>(vertices[i]);
			resultRayCast.push_back(SbVec3f(x, y, z));
		}
		d->linePtStream.push_back(resultRayCast);

		d->lineProp.append(QList<SoVertexProperty*>());
		d->linePoints[0].append(QList<SbVec3f>());
		d->linePoints[1].append(QList<SbVec3f>());
		d->lineTrans[0].append(QList<SoTranslation*>());
		d->lineTrans[1].append(QList<SoTranslation*>());
		d->handleMatl[0].append(QList<SoMaterial*>());
		d->handleMatl[1].append(QList<SoMaterial*>());
		d->lineMatl.append(QList<SoMaterial*>());
		QString label_text;

		SoRef<SoSeparator> handleGroup = new SoSeparator;

		for (auto i = 0; i < resultRayCast.size(); ++i) {
			auto index1 = i;
			auto index2 = i + 1;

			if (i == resultRayCast.size() - 1) {
				index1 = i;
				index2 = 0;
			}

			auto xDiff = resultRayCast[index2][0] - resultRayCast[index1][0];
			auto yDiff = resultRayCast[index2][1] - resultRayCast[index1][1];

			//Add line
			SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
			drawStyle->setName("drawStyle");
			drawStyle->lineWidth = 2;
			SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
			vertexProp->vertex.set1Value(0, resultRayCast[index1]);
			vertexProp->vertex.set1Value(1, resultRayCast[index2]);

			d->lineProp[d->lineProp.count() - 1].append(vertexProp.ptr());

			SoRef<SoLineSet> lineSet = new SoLineSet;
			lineSet->setName("myline");
			lineSet->vertexProperty = vertexProp.ptr();

			d->linePoints[0][d->linePoints[0].count() - 1].append(resultRayCast[index1]);
			d->linePoints[1][d->linePoints[0].count() - 1].append(resultRayCast[index2]);

			SoRef<SoMaterial> lineMatl = new SoMaterial;
			lineMatl->ambientColor.setValue(1, 1, 0);
			lineMatl->diffuseColor.setValue(1, 1, 0);
			d->lineMatl[d->lineMatl.count() - 1].append(lineMatl.ptr());

			SoRef<SoSeparator> lineSep = new SoSeparator;
			lineSep->addChild(drawStyle.ptr());
			lineSep->addChild(lineMatl.ptr());
			lineSep->addChild(lineSet.ptr());

			//Add handles			
			SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
			circleStyle->setName("circleStyle");
			circleStyle->style = SoDrawStyle::FILLED;

			SoRef<SoFaceSet> faceSet = new SoFaceSet;
			faceSet->vertexProperty = d->circleProp.ptr();

			SoRef<SoSeparator> circleSep = new SoSeparator;
			circleSep->addChild(circleStyle.ptr());

			SoRef<SoSeparator> circle1 = new SoSeparator;
			circleSep->addChild(circle1.ptr());
			SoRef<SoMaterial> circleMatl1 = new SoMaterial;
			circleMatl1->ambientColor.setValue(1, 0, 0);
			circleMatl1->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans1 = new SoTranslation;
			circleTrans1->translation.setValue(resultRayCast[index1]);
			circle1->addChild(circleMatl1.ptr());
			circle1->addChild(circleTrans1.ptr());
			circle1->addChild(faceSet.ptr());

			SoRef<SoSeparator> circle2 = new SoSeparator;
			circleSep->addChild(circle2.ptr());
			SoRef<SoMaterial> circleMatl2 = new SoMaterial;
			circleMatl2->ambientColor.setValue(1, 0, 0);
			circleMatl2->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans2 = new SoTranslation;
			circleTrans2->translation.setValue(resultRayCast[index2]);
			circle2->addChild(circleMatl2.ptr());
			circle2->addChild(circleTrans2.ptr());
			circle2->addChild(faceSet.ptr());

			d->lineTrans[0][d->lineTrans[0].count() - 1].append(circleTrans1.ptr());
			d->lineTrans[1][d->lineTrans[1].count() - 1].append(circleTrans2.ptr());

			d->handleMatl[0][d->handleMatl[0].count() - 1].append(circleMatl1.ptr());
			d->handleMatl[1][d->handleMatl[1].count() - 1].append(circleMatl2.ptr());

			handleGroup->addChild(circleSep.ptr());
			handleGroup->addChild(lineSep.ptr());
		}

		d->handleRoot->addChild(handleGroup.ptr());
		d->inProcess = false;
		d->instance->Finish(d->lineProp.count() - 1);
	}
	void PolygonROISep::lineCallback(SoPolyLineScreenDrawer::EventArg& arg) {
		OivMultiLineDrawer* source = (OivMultiLineDrawer*)arg.getSource();
		SoHandleEventAction* action = arg.getAction();
		auto dd = static_cast<Impl*>(source->getUserData());
		if (source->point.getNum() < 3) {
			source->clear();
			dd->drawerSwitch->whichChild = -1;
			dd->inProcess = false;
			return;
		}

		auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());
		if (resultRayCast.size() < 3) {
			source->clear();
			dd->drawerSwitch->whichChild = -1;
			dd->inProcess = false;
			return;
		}
				
		dd->linePtStream.push_back(resultRayCast);

		dd->lineProp.append(QList<SoVertexProperty*>());
		dd->linePoints[0].append(QList<SbVec3f>());
		dd->linePoints[1].append(QList<SbVec3f>());
		dd->lineTrans[0].append(QList<SoTranslation*>());
		dd->lineTrans[1].append(QList<SoTranslation*>());
		dd->handleMatl[0].append(QList<SoMaterial*>());
		dd->handleMatl[1].append(QList<SoMaterial*>());
		dd->lineMatl.append(QList<SoMaterial*>());
		QString label_text;

		SoRef<SoSeparator> handleGroup = new SoSeparator;

		for (auto i = 0; i < resultRayCast.size(); ++i) {
			auto index1 = i;
			auto index2 = i + 1;

			if (i == resultRayCast.size() - 1) {
				index1 = i;
				index2 = 0;
			}

			auto xDiff = resultRayCast[index2][0] - resultRayCast[index1][0];
			auto yDiff = resultRayCast[index2][1] - resultRayCast[index1][1];

			//Add line
			SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
			drawStyle->setName("drawStyle");
			drawStyle->lineWidth = 2;
			SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
			vertexProp->vertex.set1Value(0, resultRayCast[index1]);
			vertexProp->vertex.set1Value(1, resultRayCast[index2]);

			dd->lineProp[dd->lineProp.count() - 1].append(vertexProp.ptr());

			SoRef<SoLineSet> lineSet = new SoLineSet;
			lineSet->setName("myline");
			lineSet->vertexProperty = vertexProp.ptr();

			dd->linePoints[0][dd->linePoints[0].count() - 1].append(resultRayCast[index1]);
			dd->linePoints[1][dd->linePoints[0].count() - 1].append(resultRayCast[index2]);

			SoRef<SoMaterial> lineMatl = new SoMaterial;
			lineMatl->ambientColor.setValue(1, 1, 0);
			lineMatl->diffuseColor.setValue(1, 1, 0);
			dd->lineMatl[dd->lineMatl.count() - 1].append(lineMatl.ptr());

			SoRef<SoSeparator> lineSep = new SoSeparator;
			lineSep->addChild(drawStyle.ptr());
			lineSep->addChild(lineMatl.ptr());
			lineSep->addChild(lineSet.ptr());
			
			//Add handles			
			SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
			circleStyle->setName("circleStyle");
			circleStyle->style = SoDrawStyle::FILLED;

			SoRef<SoFaceSet> faceSet = new SoFaceSet;
			faceSet->vertexProperty = dd->circleProp.ptr();

			SoRef<SoSeparator> circleSep = new SoSeparator;
			circleSep->addChild(circleStyle.ptr());

			SoRef<SoSeparator> circle1 = new SoSeparator;
			circleSep->addChild(circle1.ptr());
			SoRef<SoMaterial> circleMatl1 = new SoMaterial;
			circleMatl1->ambientColor.setValue(1, 0, 0);
			circleMatl1->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans1 = new SoTranslation;
			circleTrans1->translation.setValue(resultRayCast[index1]);
			circle1->addChild(circleMatl1.ptr());
			circle1->addChild(circleTrans1.ptr());
			circle1->addChild(faceSet.ptr());

			SoRef<SoSeparator> circle2 = new SoSeparator;
			circleSep->addChild(circle2.ptr());
			SoRef<SoMaterial> circleMatl2 = new SoMaterial;
			circleMatl2->ambientColor.setValue(1, 0, 0);
			circleMatl2->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans2 = new SoTranslation;
			circleTrans2->translation.setValue(resultRayCast[index2]);
			circle2->addChild(circleMatl2.ptr());
			circle2->addChild(circleTrans2.ptr());
			circle2->addChild(faceSet.ptr());

			dd->lineTrans[0][dd->lineTrans[0].count() - 1].append(circleTrans1.ptr());
			dd->lineTrans[1][dd->lineTrans[1].count() - 1].append(circleTrans2.ptr());

			dd->handleMatl[0][dd->handleMatl[0].count() - 1].append(circleMatl1.ptr());
			dd->handleMatl[1][dd->handleMatl[1].count() - 1].append(circleMatl2.ptr());

			handleGroup->addChild(circleSep.ptr());
			handleGroup->addChild(lineSep.ptr());
		}

		dd->handleRoot->addChild(handleGroup.ptr());
		dd->inProcess = false;
		dd->instance->Finish(dd->lineProp.count() - 1);

		dd->drawerSwitch->whichChild = -1;
	}
	void PolygonROISep::LineMoveCB(void* pImpl, SoEventCallback* eventCB) {
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		if (dd->isInHandle) {
			//consume event
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback				
				eventCB->releaseEvents();
				return;
			}
			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->translation.setValue(p->getPoint());
			dd->lineProp[dd->lineIdx][dd->countIdx]->vertex.set1Value(dd->startIdx, p->getPoint());

			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->touch();

			if (dd->countIdx > 0 && dd->startIdx == 0) {
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx - 1]->vertex.set1Value(1, p->getPoint());
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.touch();
			}
			if (dd->countIdx + 1 < dd->lineProp.count() && dd->startIdx == 1) {
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx + 1]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.touch();
			}

			if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
				dd->lineTrans[0][dd->lineIdx][0]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][0]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[0][dd->lineIdx][0]->translation.touch();
			}
			else if (dd->startIdx == 0 && dd->countIdx == 0) {
				auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;

				dd->lineTrans[1][dd->lineIdx][count]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][count]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[1][dd->lineIdx][count]->translation.touch();
			}

			eventCB->setHandled();
			return;
		}
		eventCB->releaseEvents();
	}
	void PolygonROISep::LineButtonCB(void* pImpl, SoEventCallback* eventCB) {
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		const SoEvent* event = eventCB->getEvent();
		const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			//check whether handle is under click or not
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback	
				eventCB->releaseEvents();
				return;
			}
			auto point = p->getPoint();
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
			if (false == dd->inProcess) {
				for (auto i = 0; i < dd->lineProp.count(); i++) {//travel paths
					for (auto k = 0; k < dd->lineProp[i].count(); k++) {//travel lines in path
						for (auto j = 0; j < 2; j++) {//travel start or end point
							auto dist = sqrt(pow(dd->linePoints[j][i][k][0] - point[0], 2) + pow(dd->linePoints[j][i][k][1] - point[1], 2));
							if (dist > dd->handleRadius) {
								continue;
							}
							dd->isInHandle = true;
							dd->lineIdx = i;
							dd->startIdx = j;
							dd->countIdx = k;
							break;
						}
					}
					if (dd->isInHandle) {
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 1, 0);
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 1, 0);

						if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
							dd->handleMatl[0][dd->lineIdx][0]->ambientColor.setValue(1, 1, 0);
							dd->handleMatl[0][dd->lineIdx][0]->diffuseColor.setValue(1, 1, 0);
						}
						else if (dd->startIdx == 0 && dd->countIdx == 0) {
							auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;
							dd->handleMatl[1][dd->lineIdx][count]->ambientColor.setValue(1, 1, 0);
							dd->handleMatl[1][dd->lineIdx][count]->diffuseColor.setValue(1, 1, 0);
						}

						dd->instance->HighlightItem(dd->lineIdx);
						emit dd->instance->sigSelected(dd->lineIdx);

						break;
					}
				}
			}
			if(false == dd->isInHandle) {
				dd->drawerSwitch->whichChild = 0;
			}
			dd->inProcess = !dd->isInHandle;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (dd->isInHandle) {
				SoHandleEventAction* action = eventCB->getAction();
				const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

				SoRayPickAction pickaction = SoRayPickAction(myRegion);
				pickaction.setPoint(event->getPosition());
				pickaction.setSceneManager(action->getSceneManager());
				pickaction.apply(action->getPickRoot());
				auto p = pickaction.getPickedPoint();
				dd->linePoints[dd->startIdx][dd->lineIdx][dd->countIdx] = p->getPoint();

				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 0, 0);
				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 0, 0);

				if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
					dd->handleMatl[0][dd->lineIdx][0]->ambientColor.setValue(1, 0, 0);
					dd->handleMatl[0][dd->lineIdx][0]->diffuseColor.setValue(1, 0, 0);
				}
				else if (dd->startIdx == 0 && dd->countIdx == 0) {
					auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;
					dd->handleMatl[1][dd->lineIdx][count]->ambientColor.setValue(1, 0, 0);
					dd->handleMatl[1][dd->lineIdx][count]->diffuseColor.setValue(1, 0, 0);
				}

				//recompute line length
				if (dd->countIdx > 0 && dd->startIdx == 0) {
					dd->linePoints[1][dd->lineIdx][dd->countIdx - 1] = p->getPoint();
				}
				if (dd->countIdx + 1 < dd->lineProp.count() && dd->startIdx == 1) {
					dd->linePoints[0][dd->lineIdx][dd->countIdx + 1] = p->getPoint();
				}
				auto streamSize = dd->linePtStream[dd->lineIdx].size();
				dd->linePtStream[dd->lineIdx][(dd->countIdx + dd->startIdx) % streamSize] = p->getPoint();
				
				dd->instance->Update(dd->lineIdx);
			}
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
		}

		if (dd->isInHandle) {
			//consume event
			eventCB->setHandled();
			return;
		}

		SoHandleEventAction* action2 = eventCB->getAction();
		const SbViewportRegion& myRegion2 = eventCB->getAction()->getViewportRegion();

		SoRayPickAction pickaction2 = SoRayPickAction(myRegion2);
		pickaction2.setPoint(event->getPosition());
		pickaction2.setSceneManager(action2->getSceneManager());
		pickaction2.apply(action2->getPickRoot());
		auto p = pickaction2.getPickedPoint();
		if (p == NULL) {
			dd->line->Reset();
		}

		auto pos = mouseButton->getPositionFloat();
		auto viewport_size = myRegion2.getViewportSizePixels();
		auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
		if (mouse_in_setting) {
			dd->line->Reset();
		}

		eventCB->releaseEvents();
	}
}