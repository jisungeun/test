#pragma once
#include <memory>
#include <tuple>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;

namespace TC {
	class TC_Rendering_Interactive_Measure2D_API MultiLineMeasureSep : public QObject {
		Q_OBJECT
	public:
		MultiLineMeasureSep(QObject* parent = nullptr);
		~MultiLineMeasureSep();

		auto GetRoot()->SoSeparator*;

		auto Clear()->void;
		auto DeleteItem(int idx)->bool;
		auto ClearHighlight()->void;
		auto HighlightItem(int idx)->bool;
		auto GetMeasure()->QList<float>;

		auto SetHandleSize(double radius)->void;

		void Activate();
		void Deactivate();

		void Finish(QString text);
		void Update(int index, QString text);

	signals:
		void sigFinish(QString text);
		void sigUpdate(int index, QString text);

	protected:
		static void lineCallback(SoPolyLineScreenDrawer::EventArg& arg);
		static void LineMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void LineButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}