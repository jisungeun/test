#pragma once
#include <memory>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoEllipseScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;

namespace TC {
    typedef struct ellipsoidMeasureInformation {
        float area;
        float circumference;
        float majorAxis;
        float minorAxis;
    }ellipseMeasure;
    class TC_Rendering_Interactive_Measure2D_API EllipsoidMeasureSep :public QObject {
        Q_OBJECT
    public:
        EllipsoidMeasureSep(QObject* parent = nullptr);
        ~EllipsoidMeasureSep();

        auto GetRoot()->SoSeparator*;

        auto Clear()->void;
        auto ClearHighlight()->void;
        auto HighlightItem(int idx)->bool;
        auto DeleteItem(int idx)->bool;
        auto GetMeasure()->QList<ellipseMeasure>;
        void Activate();
        void Deactivate();

        auto SetHandleSize(double radius)->void;

        void Finish(QString text);
        void Update(int index, QString text);

    signals:
        void sigFinish(QString text);
        void sigUpdate(int index, QString text);

    protected:
        static void RenderCB(SoEllipseScreenDrawer::EventArg& arg);
        static void MouseMoveCB(void* pImpl, SoEventCallback* eventCB);
        static void MouseButtonCB(void* pImpl, SoEventCallback* evnetCB);

    private:
        static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}