#pragma once
#include <memory>
#include <tuple>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;

namespace TC {
	typedef struct circleMeasureInformation{
		float radius;
		float circumference;
		float area;
	}circleMeasure;

	class TC_Rendering_Interactive_Measure2D_API CircleMeasureSep : public QObject {
		Q_OBJECT
	public:
		CircleMeasureSep(QObject* parent = nullptr);
		~CircleMeasureSep();

		auto GetRoot()->SoSeparator*;
		auto Clear()->void;
		auto ClearHighlight()->void;
		auto HighlightItem(int idx)->bool;
		auto DeleteItem(int idx)->bool;
		auto GetMeasure()->QList<circleMeasure>;

		auto SetHandleSize(double rad)->void;

		void Activate();
		void Deactivate();

		void Finish(QString text);
		void Update(int index, QString text);

	signals:
		void sigFinish(QString text);
		void sigUpdate(int index, QString text);

	protected:
		static void RenderCB(SoPolyLineScreenDrawer::EventArg& arg);
		static void MouseMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void MouseButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}