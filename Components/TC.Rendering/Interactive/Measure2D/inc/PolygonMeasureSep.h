#pragma once
#include <memory>
#include <tuple>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;

namespace TC {
	typedef struct polygonMeasureInformation {
		float area;
		float circumference;
	}polygonMeasure;
	class TC_Rendering_Interactive_Measure2D_API PolygonMeasureSep : public QObject {
		Q_OBJECT
	public:
		PolygonMeasureSep(QObject* parent = nullptr);
		~PolygonMeasureSep();

		auto GetRoot()->SoSeparator*;

		auto GetMeasure()->QList<polygonMeasure>;
		auto Clear()->void;
		auto ClearHighlight()->void;
		auto HighlightItem(int idx)->bool;
		auto DeleteItem(int idx)->bool;

		auto SetHandleSize(double radius)->void;

		void Activate();
		void Deactivate();

		void Finish(QString text);
		void Update(int index, QString text);

	signals:
		void sigFinish(QString text);
		void sigUpdate(int index, QString text);

	protected:
		static void lineCallback(SoPolyLineScreenDrawer::EventArg& arg);
		static void LineMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void LineButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}