#pragma once
#include <memory>
#include <tuple>

#include <QObject>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)
#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoNode;
class SoSeparator;
class SoEventCallback;
class SoMFVec2f;

namespace TC {
	class TC_Rendering_Interactive_Measure2D_API AngleMeasureSep : public QObject {
		Q_OBJECT
	public:
		AngleMeasureSep(QObject* parent = nullptr);
		~AngleMeasureSep();

		auto GetRoot()->SoSeparator*;

		auto Clear()->void;
		auto ClearHighlight()->void;
		auto HighlightItem(int idx)->bool;
		auto DeleteItem(int idx)->bool;
		auto GetMeasure()->QList<float>;
		void Activate();
		void Deactivate();

		auto SetHandleSize(double radius)->void;

		void Finish(QString text);
		void Update(int index, QString text);

	signals:
		void sigFinish(QString text);
		void sigUpdate(int index, QString text);

	protected:
		static void lineCallback(SoPolyLineScreenDrawer::EventArg& arg);
		static void LineMoveCB(void* pImpl, SoEventCallback* eventCB);
		static void LineButtonCB(void* pImpl, SoEventCallback* eventCB);

	private:
		static auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f>;
		auto Init()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}