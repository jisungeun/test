#pragma once

#include <enum.h>

#include <memory>

#include <QWidget>

#include "TC.Rendering.Interactive.Measure2DExport.h"

class SoSeparator;

namespace TC {
	BETTER_ENUM(MeasureType, int,
		Line = 0,
		Path = 1,
		Angle = 2,
		Circle = 3,
		Ellipsoid = 4,
		Polygon = 5,
		None = -1);
	class TC_Rendering_Interactive_Measure2D_API Measure2dControl :public QWidget {
		Q_OBJECT
	public:
		Measure2dControl(QWidget* parent = nullptr);
		~Measure2dControl();

		auto GetToolRoot()->SoSeparator*;
		auto ClearMeasures()->void;
		auto DeactivateAll()->void;

		auto SetHandleRadius(float rad)->void;
		auto SetHandleBaseRadius(double base_rad)->void;

	signals:	
		void sigActivateMeasure();

	protected slots:
		void OnButtonGroupToggled(int,bool);		
		void OnVizToggleClicked();

		void OnLineFinish(QString);
		void OnLineModified(int,QString);
		void OnPathFinish(QString);
		void OnPathModified(int, QString);
		void OnAngleFinish(QString);
		void OnAngleModified(int, QString);
		void OnCircleFinish(QString);
		void OnCircleModified(int, QString);
		void OnEllipseFinish(QString);
		void OnEllipseModified(int, QString);
		void OnPolygonFinish(QString);
		void OnPolygonModified(int, QString);
				
		void OnExport();
		void OnSelectAll();
		void OnDeselectAll();
		void OnDeleteItems();

		void OnTableSelectionChanged();

	private:
		auto Init()->void;
		auto InitTable()->void;
		auto InitSceneGraph()->void;
	    auto InitConnections()->void;

		auto OnLineToggled(bool)->void;
		auto OnPathToggled(bool)->void;
		auto OnAngleToggled(bool)->void;
		auto OnCircleToggled(bool)->void;
		auto OnEllipseToggled(bool)->void;
		auto OnPolygonToggled(bool)->void;

		auto AppendTableItem(QString text,MeasureType type)->void;
		auto ModifyTableItem(int idx, MeasureType type,QString text)->void;
		auto DeleteMeasureItem(int idx, MeasureType type)->void;
		auto HighlightMeasureItem(int idx, MeasureType type)->void;

		auto FlushToggle()->void;


		struct Impl;
		std::unique_ptr<Impl> d;
	};
}