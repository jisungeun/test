#include <QDebug>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#pragma warning(pop)

#include "OivEllipseDrawer.h"
#include "EllipsoidMeasureSep.h"

namespace TC {
    struct EllipsoidMeasureSep::Impl {
        EllipsoidMeasureSep* instance = false;
        SoSeparator* root{ nullptr };
        SoSeparator* handleRoot{ nullptr };
        OivEllipseDrawer* ellipse{ nullptr };
        SoEventCallback* callback{ nullptr };
        SoRef<SoVertexProperty> circleProp{ nullptr };

        //handle parameter
        QList<SoTranslation*> handleTrans[3];
        QList<SoMaterial*> handleMatl[3];
        QList<SoVertexProperty*> guideProp[2];
        QList<float> curAngle[2];
        float movingAngle = 0;
        //body parameter
        QList<SoMaterial*> lineMatl;
        QList<SoVertexProperty*> lineProp;
        QList<SoRotationXYZ*> lineRot;
        QList<SoTranslation*> lineTrans;
        QList<SoText2*> labelText;
        QList<SoTranslation*> labelPos;

        QList<ellipseMeasure> measure;

        SbVec3f pickedHandlePos[3];
        double handleRadius{ 0.5 };
        bool isInHandle{ false };
        int ellipseIdx = -1;
        int handleIdx = -1;

        bool isActivated{ false };

        double constant1 = pow(M_E, 2) / 8.0 + pow(M_E, 4) / 16.0 + 71 * pow(M_E, 6) / 2048.0;
        double constant2 = 5 * pow(M_E, 4) / 256.0 + 5 * pow(M_E, 6) / 256.0;
        double constant3 = 29.0 * pow(M_E, 6) / 6144.0;
        auto thetaEquation(double r)->double;// {//r for radians            
        auto drawEllipsoid(SbVec3f axis1, SbVec3f axis2, SbVec3f center, SoVertexProperty* prop)->bool;
        auto calcArea(double a, double b)->double;
        auto calcLength(double a, double b)->double;
        static bool xLessThan(const SbVec3f& v1, const SbVec3f& v2)
        {
            return v1[0] < v2[0];
        }
        static bool xMoreThan(const SbVec3f& v1, const SbVec3f& v2)
        {
            return v1[0] > v2[0];
        }
    };
    auto EllipsoidMeasureSep::Impl::calcArea(double a, double b) -> double {
        return M_PI * a * b;
    }
    auto EllipsoidMeasureSep::Impl::calcLength(double a, double b) -> double {
        return 2.0 * M_PI * sqrt((a * a + b * b) / 2.0);
    }
    auto EllipsoidMeasureSep::Impl::thetaEquation(double r) -> double {
        double result = r + constant1 * sin(2 * r) + constant2 * sin(4 * r) + constant3 * sin(6 * r);
        //+ O(pow(M_E,8)); Taylor's remainder theorem
        return result;
    }    
    auto EllipsoidMeasureSep::Impl::drawEllipsoid(SbVec3f axis1, SbVec3f axis2, SbVec3f center, SoVertexProperty* vertexProp) -> bool {
        bool isConverted = false;
        auto majorLength = sqrt(pow(axis1[0] - center[0], 2) + pow(axis1[1] - center[1], 2));
        auto minorLength = sqrt(pow(axis2[0] - center[0], 2) + pow(axis2[1] - center[1], 2));
        if (majorLength < minorLength) {
            auto temp = majorLength;
            majorLength = minorLength;
            minorLength = temp;
            isConverted = true;
        }
        QList<SbVec3f> mpList;
        QList<SbVec3f> ppList;
        QList<SbVec3f> pmList;
        QList<SbVec3f> mmList;
        for (auto i = 0; i < 100; ++i) {
            auto rad = 2 * M_PI / 100 * i;
            auto theta = thetaEquation(rad);
            auto x = majorLength * sin(theta);
            auto y = minorLength * cos(theta);
            auto pt = SbVec3f(x, y, center[2]);
            if (x < 0) {
                if (y > 0) {
                    mpList.append(pt);
                }
                else {
                    mmList.append(pt);
                }
            }
            else {
                if (y > 0) {
                    ppList.append(pt);
                }
                else {
                    pmList.append(pt);
                }
            }
        }
        std::sort(mpList.begin(), mpList.end(), xLessThan);
        std::sort(ppList.begin(), ppList.end(), xLessThan);
        std::sort(pmList.begin(), pmList.end(), xMoreThan);
        std::sort(mmList.begin(), mmList.end(), xMoreThan);

        auto constant = 0;
        for (auto i = 0; i < mpList.count(); i++) {
            vertexProp->vertex.set1Value(i, mpList[i]);
        }
        constant += mpList.count();
        for (auto i = 0; i < ppList.count(); i++) {
            vertexProp->vertex.set1Value(constant + i, ppList[i]);
        }
        constant += ppList.count();
        for (auto i = 0; i < pmList.count(); i++) {
            vertexProp->vertex.set1Value(constant + i, pmList[i]);
        }
        constant += pmList.count();
        for (auto i = 0; i < mmList.count(); i++) {
            vertexProp->vertex.set1Value(constant + i, mmList[i]);
        }
        vertexProp->vertex.set1Value(100, SbVec3f(mpList[0]));
        return isConverted;
    }
    EllipsoidMeasureSep::EllipsoidMeasureSep(QObject* parent) : QObject(parent), d{ new Impl }{
        this->Init();
        d->instance = this;
    }
    EllipsoidMeasureSep::~EllipsoidMeasureSep() {

    }
    auto EllipsoidMeasureSep::SetHandleSize(double radius) -> void {
        d->handleRadius = radius;
        auto div = 100.0;
        for (auto i = 0; i < 100; i++) {
            auto x_pos = radius * cos(2 * M_PI / div * i);
            auto y_pos = radius * sin(2 * M_PI / div * i);
            d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
        }
        d->circleProp->touch();
    }

    void EllipsoidMeasureSep::Finish(QString text) {
        emit sigFinish(text);
    }
    void EllipsoidMeasureSep::Update(int index, QString text) {
        emit sigUpdate(index, text);
    }
    auto EllipsoidMeasureSep::GetMeasure() -> QList<ellipseMeasure> {
        return d->measure;
    }
    auto EllipsoidMeasureSep::Init() -> void {
        d->ellipse = new OivEllipseDrawer;
        d->root = new SoSeparator;
        d->handleRoot = new SoSeparator;
        SoRef<SoLightModel> lightmodel = new SoLightModel;
        lightmodel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightmodel.ptr());

        d->root->addChild(d->ellipse);
        d->root->addChild(d->handleRoot);

        d->ellipse->setUserData(d.get());
        d->ellipse->onFinish.add(RenderCB);

        d->circleProp = new SoVertexProperty;
        auto radius = d->handleRadius;
        auto div = 100.0;
        for (auto i = 0; i < 100; i++) {
            auto x_pos = radius * cos(2 * M_PI / div * i);
            auto y_pos = radius * sin(2 * M_PI / div * i);
            d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
        }

        d->callback = new SoEventCallback;
        d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), MouseButtonCB, d.get());
        d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), MouseMoveCB, d.get());
        d->root->insertChild(d->callback, 0);
    }
    auto EllipsoidMeasureSep::ClearHighlight() -> void {
        for(auto matl:d->lineMatl) {
            matl->ambientColor.setValue(1, 1, 0);
            matl->diffuseColor.setValue(1, 1, 0);
        }
    }
    auto EllipsoidMeasureSep::HighlightItem(int idx) -> bool {
        if (d->lineProp.count() <= idx) {
            return false;
        }
        if (d->handleRoot->getNumChildren() < idx) {
            return false;
        }
        d->lineMatl[idx]->ambientColor.setValue(0, 1, 0);
        d->lineMatl[idx]->diffuseColor.setValue(0, 1, 0);
        return true;
    }

    auto EllipsoidMeasureSep::DeleteItem(int idx) -> bool {
        if(d->lineProp.count()<=idx) {
            return false;
        }
        if(d->handleRoot->getNumChildren() < idx) {
            return false;
        }
        d->handleRoot->removeChild(idx + 1);
        d->lineProp.removeAt(idx);
        d->lineMatl.removeAt(idx);
        d->lineTrans.removeAt(idx);
        d->lineRot.removeAt(idx);
        d->measure.removeAt(idx);
        d->labelPos.removeAt(idx);
        d->labelText.removeAt(idx);
        for(auto i=0;i<3;i++) {
            d->handleMatl[i].removeAt(idx);
            d->handleTrans[i].removeAt(idx);
        }
        for(auto i=0;i<2;i++) {
            d->guideProp[i].removeAt(idx);
            d->curAngle[i].removeAt(idx);
        }        

        return true;
    }
    auto EllipsoidMeasureSep::Clear() -> void {
        d->handleRoot->removeAllChildren();
        SoRef<SoLightModel> lightmodel = new SoLightModel;
        lightmodel->model = SoLightModel::BASE_COLOR;
        d->handleRoot->addChild(lightmodel.ptr());

        d->lineProp.clear();
        d->lineMatl.clear();
        d->lineTrans.clear();
        d->lineRot.clear();
        d->measure.clear();
        d->labelPos.clear();
        d->labelText.clear();
        for (auto i = 0; i < 3; i++) {
            d->handleMatl[i].clear();
            d->handleTrans[i].clear();
        }

        for (auto i = 0; i < 2; i++) {
            d->guideProp[i].clear();
            d->curAngle[i].clear();
        }
    }
    void EllipsoidMeasureSep::Activate() {
        d->ellipse->Activate();
        d->isActivated = true;
    }

    void EllipsoidMeasureSep::Deactivate() {
        d->ellipse->Deactivate();
        d->isActivated = false;
    }
    auto EllipsoidMeasureSep::GetRoot() -> SoSeparator* {
        return d->root;
    }
    auto EllipsoidMeasureSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion) -> std::vector<SbVec3f> {
        std::vector<SbVec3f> result;

        SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
        for (int i = 0; i < source->getNum(); i++) {

            SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
            normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
            normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

            rayPick.setNormalizedPoint(normalizedPoint);
            rayPick.apply(targetNode);

            SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
            if (pickedPoint) {
                result.push_back(pickedPoint->getPoint());
            }
        }

        return result;
    }
    void EllipsoidMeasureSep::RenderCB(SoEllipseScreenDrawer::EventArg& arg) {
        auto source = (OivEllipseDrawer*)arg.getSource();
        auto action = arg.getAction();
        auto dd = static_cast<Impl*>(source->getUserData());

        auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());
        if (resultRayCast.size() == 0) {
            source->clear();
            return;
        }
        SoMFVec2f additionalPts;
        additionalPts.set1Value(0, source->GetCenterPt());
        additionalPts.set1Value(1, source->GetMajorAxisPt());
        additionalPts.set1Value(2, source->GetMinorAxisPt());
        auto resultHandles = CalcRayCasting(&additionalPts, action->getPickRoot(), action->getViewportRegion());
        if (resultHandles.size() < 3) {
            source->clear();
            return;
        }

        auto max_x = resultRayCast[0][0];
        auto min_y = resultRayCast[0][1];
        for (auto i = 0; i < resultRayCast.size(); ++i) {
            if (max_x < resultRayCast[i][0]) max_x = resultRayCast[i][0];
            if (min_y > resultRayCast[i][1]) min_y = resultRayCast[i][1];
        }

        auto a = sqrt(pow(resultHandles[1][0] - resultHandles[0][0], 2) + pow(resultHandles[1][1] - resultHandles[0][1], 2));
        auto b = sqrt(pow(resultHandles[2][0] - resultHandles[0][0], 2) + pow(resultHandles[2][1] - resultHandles[0][1], 2));

        ellipseMeasure newMeasure;

        newMeasure.circumference = dd->calcLength(a, b);
        newMeasure.area = dd->calcArea(a, b);
        auto correctMajorPt = false;
        if (a > b) {            
            newMeasure.majorAxis = a;
            newMeasure.minorAxis = b;
            correctMajorPt = true;
        }else {
            newMeasure.majorAxis = b;
            newMeasure.minorAxis = a;
        }

        dd->measure.append(newMeasure);

        SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
        drawStyle->setName("drawStyle");
        drawStyle->lineWidth = 2;
        SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
        dd->drawEllipsoid(resultHandles[1], resultHandles[2], resultHandles[0], vertexProp.ptr());

        dd->lineProp.append(vertexProp.ptr());

        SoRef<SoLineSet> lineSet = new SoLineSet;
        lineSet->setName("ellipsoid");
        lineSet->vertexProperty = vertexProp.ptr();

        SoRef<SoMaterial> lineMatl = new SoMaterial;
        lineMatl->ambientColor.setValue(1, 1, 0);
        lineMatl->diffuseColor.setValue(1, 1, 0);
        dd->lineMatl.append(lineMatl.ptr());

        SoRef<SoSeparator> lineSep = new SoSeparator;
        lineSep->addChild(drawStyle.ptr());
        lineSep->addChild(lineMatl.ptr());

        auto angle = 0.0;
        auto isInvertAxis = source->GetIsConverted() && correctMajorPt;
        if (isInvertAxis) {
            angle = 1.57;
        }

        if (isInvertAxis) {
            dd->curAngle[0].append(-M_PI / 2.0);
            dd->curAngle[1].append(0);
        }
        else {
            dd->curAngle[0].append(angle);
            dd->curAngle[1].append(-M_PI / 2.0);
        }

        SoRef<SoRotationXYZ> rot = new SoRotationXYZ;
        rot->axis = SoRotationXYZ::Axis::Z;
        rot->angle = angle;
        dd->lineRot.append(rot.ptr());
        SoRef<SoTranslation> backTrans = new SoTranslation;
        backTrans->translation.setValue(resultHandles[0]);
        dd->lineTrans.append(backTrans.ptr());
        lineSep->addChild(backTrans.ptr());
        lineSep->addChild(rot.ptr());
        lineSep->addChild(lineSet.ptr());

        //Add text
        SoRef<SoSeparator> textSep = new SoSeparator;
        textSep->setName("textSep");

        SoRef<SoTranslation> labelPos = new SoTranslation();
        labelPos->translation.setValue(max_x, min_y, 0);
        
        textSep->addChild(labelPos.ptr());
        dd->labelPos.append(labelPos.ptr());

        SoRef<SoFont> labelFont = new SoFont();
        labelFont->size = 13.0;
        labelFont->name = "Arial:Bold";
        labelFont->renderStyle = SoFont::TEXTURE;
        textSep->addChild(labelFont.ptr());

        SoTextProperty* textProp = new SoTextProperty();
        textProp->alignmentH = SoTextProperty::LEFT;
        textProp->alignmentV = SoTextProperty::TOP;
        textSep->addChild(textProp);

        SoRef<SoText2> labelText = new SoText2();
        labelText->justification = SoText2::INHERITED;
        textSep->addChild(labelText.ptr());
        dd->labelText.append(labelText.ptr());

        SoRef<SoMaterial> fontMat = new SoMaterial;
        fontMat->ambientColor.setValue(1, 1, 0);
        fontMat->diffuseColor.setValue(1, 1, 0);
        textSep->addChild(fontMat.ptr());



        QString volumeText = QString::number(dd->measure.last().area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");

        QString label_text = QString("Area %1 \n").arg(volumeText);
        label_text.append(QString("Major axis: %1 \n").arg(QString::number(dd->measure.last().majorAxis, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
        label_text.append(QString("Minor axis: %1 \n").arg(QString::number(dd->measure.last().minorAxis, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
        label_text.append(QString("Circumference: %1").arg(QString::number(dd->measure.last().circumference, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
        labelText->string = volumeText.toStdWString();


        //handle shape        
        SoRef<SoDrawStyle> handleStyle = new SoDrawStyle;
        handleStyle->setName("handleStyle");
        handleStyle->style = SoDrawStyle::FILLED;
        SoRef<SoFaceSet> faceSet = new SoFaceSet;
        faceSet->vertexProperty = dd->circleProp.ptr();
        SoRef<SoSeparator> handleSep = new SoSeparator;
        handleSep->addChild(handleStyle.ptr());
        SoRef<SoDrawStyle> guideStyle = new SoDrawStyle;
        guideStyle->lineWidth = 1.8;
        guideStyle->linePattern = 52428;//16 bit toggle pattern
        //1100110011001100 = 52428

        //add center handle
        SoRef<SoSeparator> centerHandle = new SoSeparator;
        handleSep->addChild(centerHandle.ptr());
        SoRef<SoMaterial> centerMatl = new SoMaterial;
        centerMatl->ambientColor.setValue(1, 0, 0);
        centerMatl->diffuseColor.setValue(1, 0, 0);
        dd->handleMatl[0].append(centerMatl.ptr());

        SoRef<SoTranslation> centerTrans = new SoTranslation;
        centerTrans->translation.setValue(resultHandles[0]);
        centerHandle->addChild(centerMatl.ptr());
        centerHandle->addChild(centerTrans.ptr());
        centerHandle->addChild(faceSet.ptr());
        dd->handleTrans[0].append(centerTrans.ptr());

        //add axis 1 handle
        SoRef<SoSeparator> axis1Handle = new SoSeparator;
        handleSep->addChild(axis1Handle.ptr());
        SoRef<SoMaterial> axis1Matl = new SoMaterial;
        axis1Matl->ambientColor.setValue(1, 0, 0);
        axis1Matl->diffuseColor.setValue(1, 0, 0);
        dd->handleMatl[1].append(axis1Matl.ptr());

        SoRef<SoTranslation> axis1Trans = new SoTranslation;
        axis1Trans->translation.setValue(resultHandles[1]);
        axis1Handle->addChild(axis1Matl.ptr());
        axis1Handle->addChild(axis1Trans.ptr());
        axis1Handle->addChild(faceSet.ptr());
        dd->handleTrans[1].append(axis1Trans.ptr());

        //add axis 1 guideline
        SoRef<SoSeparator> axis1Line = new SoSeparator;
        axis1Line->addChild(guideStyle.ptr());
        SoRef<SoMaterial> axis1LineMatl = new SoMaterial;
        axis1LineMatl->ambientColor.setValue(1, 0, 0);
        axis1LineMatl->diffuseColor.setValue(1, 0, 0);
        axis1Line->addChild(axis1LineMatl.ptr());
        SoRef<SoLineSet> axis1LineSet = new SoLineSet;
        SoRef<SoVertexProperty> axis1Prop = new SoVertexProperty;
        axis1Prop->vertex.set1Value(0, resultHandles[0]);
        axis1Prop->vertex.set1Value(1, resultHandles[1]);
        axis1LineSet->vertexProperty = axis1Prop.ptr();
        axis1Line->addChild(axis1LineSet.ptr());
        handleSep->addChild(axis1Line.ptr());
        dd->guideProp[0].append(axis1Prop.ptr());

        //add axis 2 handle
        SoRef<SoSeparator> axis2Handle = new SoSeparator;
        handleSep->addChild(axis2Handle.ptr());
        SoRef<SoMaterial> axis2Matl = new SoMaterial;
        axis2Matl->ambientColor.setValue(1, 0, 0);
        axis2Matl->diffuseColor.setValue(1, 0, 0);
        dd->handleMatl[2].append(axis2Matl.ptr());

        SoRef<SoTranslation> axis2Trans = new SoTranslation;
        axis2Trans->translation.setValue(resultHandles[2]);
        axis2Handle->addChild(axis2Matl.ptr());
        axis2Handle->addChild(axis2Trans.ptr());
        axis2Handle->addChild(faceSet.ptr());
        dd->handleTrans[2].append(axis2Trans.ptr());

        //add axis 2 guideline
        SoRef<SoSeparator> axis2Line = new SoSeparator;
        axis2Line->addChild(guideStyle.ptr());
        SoRef<SoMaterial> axis2LineMatl = new SoMaterial;
        axis2LineMatl->ambientColor.setValue(1, 0, 0);
        axis2LineMatl->diffuseColor.setValue(1, 0, 0);
        axis2Line->addChild(axis2LineMatl.ptr());
        SoRef<SoLineSet> axis2LineSet = new SoLineSet;
        SoRef<SoVertexProperty> axis2Prop = new SoVertexProperty;
        axis2Prop->vertex.set1Value(0, resultHandles[0]);
        axis2Prop->vertex.set1Value(1, resultHandles[2]);
        axis2LineSet->vertexProperty = axis2Prop.ptr();
        axis2Line->addChild(axis2LineSet.ptr());
        handleSep->addChild(axis2Line.ptr());
        dd->guideProp[1].append(axis2Prop.ptr());

        SoRef<SoSeparator> handleGroup = new SoSeparator;
        handleGroup->addChild(handleSep.ptr());
        handleGroup->addChild(lineSep.ptr());
        handleGroup->addChild(textSep.ptr());

        dd->handleRoot->addChild(handleGroup.ptr());
        
        dd->instance->Finish(label_text);
    }
    void EllipsoidMeasureSep::MouseButtonCB(void* pImpl, SoEventCallback* eventCB) {
        auto doubleComp = [](double left, double right)->bool {
            return std::fabs(left - right) < 0.00001;
        };
        auto dd = static_cast<Impl*>(pImpl);
        if(false == dd->isActivated) {
            eventCB->releaseEvents();
            return;
        }

        const SoEvent* event = eventCB->getEvent();
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
        if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

            SoRayPickAction pickaction = SoRayPickAction(myRegion);
            pickaction.setPoint(event->getPosition());
            pickaction.setSceneManager(action->getSceneManager());
            pickaction.apply(action->getPickRoot());
            auto p = pickaction.getPickedPoint();
            if (p == NULL) {
                //broadcast mouse event to further callback	
                eventCB->releaseEvents();
                return;
            }
            auto point = p->getPoint();
            dd->isInHandle = false;
            dd->ellipseIdx = -1;
            dd->handleIdx = -1;
            for (auto i = 0; i < dd->lineProp.count(); i++) {//for ellipsoid
                for (auto j = 0; j < 3; j++) {//for handle
                    auto handlePos = dd->handleTrans[j][i]->translation.getValue();
                    if (false == doubleComp(handlePos[2], point[2])) {
                        continue;
                    }
                    auto dist = sqrt(pow(handlePos[0] - point[0], 2) + pow(handlePos[1] - point[1], 2));
                    if (dist <= dd->handleRadius) {
                        dd->isInHandle = true;
                        dd->ellipseIdx = i;
                        dd->handleIdx = j;
                        dd->pickedHandlePos[0] = dd->handleTrans[0][dd->ellipseIdx]->translation.getValue();
                        dd->pickedHandlePos[1] = dd->handleTrans[1][dd->ellipseIdx]->translation.getValue();
                        dd->pickedHandlePos[2] = dd->handleTrans[2][dd->ellipseIdx]->translation.getValue();
                        break;
                    }
                }
                if (dd->isInHandle) {
                    break;
                }
            }
            if (dd->isInHandle) {
                dd->handleMatl[dd->handleIdx][dd->ellipseIdx]->ambientColor.setValue(1, 1, 0);
                dd->handleMatl[dd->handleIdx][dd->ellipseIdx]->diffuseColor.setValue(1, 1, 0);
            }
        }
        bool releaseEvent = false;
        if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            if (dd->isInHandle) {
                SoHandleEventAction* action = eventCB->getAction();
                const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

                SoRayPickAction pickaction = SoRayPickAction(myRegion);
                pickaction.setPoint(event->getPosition());
                pickaction.setSceneManager(action->getSceneManager());
                pickaction.apply(action->getPickRoot());
                auto p = pickaction.getPickedPoint();

                dd->handleMatl[dd->handleIdx][dd->ellipseIdx]->ambientColor.setValue(1, 0, 0);
                dd->handleMatl[dd->handleIdx][dd->ellipseIdx]->diffuseColor.setValue(1, 0, 0);
                for (auto i = 0; i < 2; i++) {
                    float val = dd->curAngle[i][dd->ellipseIdx] + dd->movingAngle;
                    if (val > M_PI * 2) {
                        val -= (M_PI * 2);
                    }
                    else if (val < -M_PI * 2) {
                        val += (M_PI * 2);
                    }
                    dd->curAngle[i][dd->ellipseIdx] = val;
                }

                auto temp_p = dd->handleTrans[0][dd->ellipseIdx]->translation.getValue();
                auto max_x = temp_p[0];
                auto min_y = temp_p[1];

                for (auto i = 0; i < 3; ++i) {
                    temp_p = dd->handleTrans[i][dd->ellipseIdx]->translation.getValue();
                    if (max_x < temp_p[0]) max_x = temp_p[0];
                    if (min_y > temp_p[1]) min_y = temp_p[1];
                }                

                QString volumeText = QString::number(dd->measure[dd->ellipseIdx].area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");

                dd->labelPos[dd->ellipseIdx]->translation.setValue(max_x, min_y,0);

                QString label_text = QString("Area %1 \n").arg(volumeText);
                label_text.append(QString("Major axis: %1 \n").arg(QString::number(dd->measure[dd->ellipseIdx].majorAxis, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
                label_text.append(QString("Minor axis: %1 \n").arg(QString::number(dd->measure[dd->ellipseIdx].minorAxis, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
                label_text.append(QString("Circumference: %1").arg(QString::number(dd->measure[dd->ellipseIdx].circumference, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
                dd->labelText[dd->ellipseIdx]->string = volumeText.toStdWString();

                dd->instance->Update(dd->ellipseIdx, label_text);

                dd->isInHandle = false;
                dd->handleIdx = -1;
                dd->ellipseIdx = -1;
                dd->movingAngle = 0.0;

                releaseEvent = true;
            }
        }
        if (dd->isInHandle || releaseEvent) {
            eventCB->setHandled();
            return;
        }

        SoHandleEventAction* action2 = eventCB->getAction();
        const SbViewportRegion& myRegion2 = eventCB->getAction()->getViewportRegion();
                
        auto pos = mouseButton->getPositionFloat();
        auto viewport_size = myRegion2.getViewportSizePixels();
        auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
        if (mouse_in_setting) {
            dd->ellipse->reset();
        }
        eventCB->releaseEvents();
    }
    void EllipsoidMeasureSep::MouseMoveCB(void* pImpl, SoEventCallback* eventCB) {
        auto doubleComp = [](double left, double right)->bool {
            return std::fabs(left - right) < 0.00001;
        };

        auto dd = static_cast<Impl*>(pImpl);
        if(false ==dd->isActivated) {
            eventCB->releaseEvents();
            return;
        }
        if (dd->isInHandle) {
            const SoEvent* event = eventCB->getEvent();
            SoHandleEventAction* action = eventCB->getAction();
            const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
            SoRayPickAction pickaction = SoRayPickAction(myRegion);
            pickaction.setPoint(event->getPosition());
            pickaction.setSceneManager(action->getSceneManager());
            pickaction.apply(action->getPickRoot());
            auto p = pickaction.getPickedPoint();
            if (p == NULL) {
                //broadcast mouse event to further callback				
                eventCB->releaseEvents();
                return;
            }
            //translate handle
            dd->handleTrans[dd->handleIdx][dd->ellipseIdx]->translation.setValue(p->getPoint());

            if (dd->handleIdx == 0) {//center move
                auto diff = p->getPoint() - dd->pickedHandlePos[0];
                auto handle1NewTrans = dd->pickedHandlePos[1] + diff;
                dd->guideProp[0][dd->ellipseIdx]->vertex.set1Value(0, p->getPoint());
                dd->guideProp[0][dd->ellipseIdx]->vertex.set1Value(1, handle1NewTrans);
                dd->handleTrans[1][dd->ellipseIdx]->translation.setValue(handle1NewTrans);

                auto handle2NewTrans = dd->pickedHandlePos[2] + diff;
                dd->guideProp[1][dd->ellipseIdx]->vertex.set1Value(0, p->getPoint());
                dd->guideProp[1][dd->ellipseIdx]->vertex.set1Value(1, handle2NewTrans);
                dd->handleTrans[2][dd->ellipseIdx]->translation.setValue(handle2NewTrans);

                dd->lineTrans[dd->ellipseIdx]->translation.setValue(p->getPoint());
            }
            else {
                auto guideIdx = dd->handleIdx - 1;
                dd->guideProp[guideIdx][dd->ellipseIdx]->vertex.set1Value(1, p->getPoint());

                auto perpenIdx = 1;
                if (dd->handleIdx == 1) {
                    perpenIdx = 2;
                }
                //recompute other handle's position, keep perpendicular
                SbVec3f baseVector;
                baseVector = p->getPoint() - dd->pickedHandlePos[0];
                baseVector.normalize();
                SbVec3f majorVector = dd->pickedHandlePos[dd->handleIdx] - dd->pickedHandlePos[0];
                majorVector.normalize();

                auto perpenDiff = dd->pickedHandlePos[perpenIdx] - dd->pickedHandlePos[0];
                auto perpenDist = sqrt(perpenDiff[0] * perpenDiff[0] + perpenDiff[1] * perpenDiff[1]);
                auto dot = majorVector[0] * baseVector[0] + majorVector[1] * baseVector[1];
                auto det = majorVector[0] * baseVector[1] - majorVector[1] * baseVector[0];
                auto angle = atan2(det, dot);
                dd->movingAngle = angle;
                auto perpenAngle = dd->curAngle[perpenIdx - 1][dd->ellipseIdx] + angle;
                auto perpenX = cos(perpenAngle) * perpenDist;
                auto perpenY = sin(perpenAngle) * perpenDist;

                auto perpenPos = SbVec3f(perpenX + dd->handleTrans[0][dd->ellipseIdx]->translation.getValue()[0], perpenY + dd->handleTrans[0][dd->ellipseIdx]->translation.getValue()[1], dd->pickedHandlePos[dd->handleIdx][2]);
                dd->guideProp[perpenIdx - 1][dd->ellipseIdx]->vertex.set1Value(1, perpenPos);

                dd->handleTrans[perpenIdx][dd->ellipseIdx]->translation.setValue(perpenPos);
                //redraw ellipsoidal points' based on modified two axis

                auto a = sqrt(pow(p->getPoint()[0] - dd->pickedHandlePos[0][0], 2) + pow(p->getPoint()[1] - dd->pickedHandlePos[0][1], 2));
                auto b = sqrt(pow(perpenPos[0] - dd->pickedHandlePos[0][0], 2) + pow(perpenPos[1] - dd->pickedHandlePos[0][1], 2));
                dd->measure[dd->ellipseIdx].circumference = dd->calcLength(a, b);
                dd->measure[dd->ellipseIdx].area = dd->calcArea(a, b);

                if (a > b) {
                    dd->measure[dd->ellipseIdx].majorAxis = a;
                    dd->measure[dd->ellipseIdx].minorAxis = b;
                }
                else {
                    dd->measure[dd->ellipseIdx].majorAxis = b;
                    dd->measure[dd->ellipseIdx].minorAxis = a;
                }

                auto isConverted = dd->drawEllipsoid(p->getPoint(), perpenPos, dd->pickedHandlePos[0], dd->lineProp[dd->ellipseIdx]);

                //recompute rotation angle based on axis vector
                if (isConverted) {
                    dd->lineRot[dd->ellipseIdx]->angle.setValue(dd->curAngle[perpenIdx - 1][dd->ellipseIdx] + angle);
                }
                else {
                    dd->lineRot[dd->ellipseIdx]->angle.setValue(dd->curAngle[dd->handleIdx - 1][dd->ellipseIdx] + angle);
                }

            }
        }

        if (dd->isInHandle) {
            eventCB->setHandled();
            return;
        }
        eventCB->releaseEvents();
    }
}