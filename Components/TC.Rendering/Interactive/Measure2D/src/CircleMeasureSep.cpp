#pragma warning(push)
#pragma warning(disable:4819)

#include "OivCircleDrawer.h"
#include "CircleMeasureSep.h"

#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#pragma warning(pop)

#include <QDebug>

namespace TC {
	struct CircleMeasureSep::Impl {
		SoSeparator* root{ nullptr };
		SoSeparator* handleRoot{ nullptr };
		OivCircleDrawer* circle{ nullptr };
		SoEventCallback* callback{ nullptr };
		SoRef<SoVertexProperty> circleProp{ nullptr };

		//handleParameter
		QList<SoTranslation*> handleTrans[3];
		QList<SoMaterial*> handleMatl[3];
		QList<SoVertexProperty*> guideProp[2];
		QList<float> curAngle[2];
		float movingAngle = 0;

		//body parameter		
		QList<SbVec3f> points;
		QList<SoVertexProperty*> prop;
		QList<SoText2*> labelText;
		QList<SoTranslation*> labelPos;
		QList<SoMaterial*> lineMatl;

		QList<circleMeasure> measure;

		SbVec3f pickedHandlePos[3];
		double handleRadius{ 0.5 };
		bool isInHandle{ false };
		int circleIdx = -1;
		int handleIdx = -1;

		CircleMeasureSep* instance = nullptr;
		bool isActivated{false};

		auto drawCircle(SbVec3f axis1, SbVec3f center, SoVertexProperty* prop)->std::tuple<float, float>;
	};
	CircleMeasureSep::CircleMeasureSep(QObject* parent) : QObject(parent), d{ new Impl } {
		Init();
		d->instance = this;
	}
	CircleMeasureSep::~CircleMeasureSep() {
	}
	auto CircleMeasureSep::Impl::drawCircle(SbVec3f axis1, SbVec3f center, SoVertexProperty* vertexProp) -> std::tuple<float, float> {
		auto radius = sqrt(pow(axis1[0] - center[0], 2) + pow(axis1[1] - center[1], 2));
		for (auto i = 0; i < 100; ++i) {
			auto rad = 2 * M_PI / 100.0 * i;
			auto x = center[0] + radius * sin(rad);
			auto y = center[1] + radius * cos(rad);
			auto pt = SbVec3f(x, y, center[2]);
			vertexProp->vertex.set1Value(i, pt);
		}
		vertexProp->vertex.set1Value(100, vertexProp->vertex.getValues(0)[0]);

		auto max_x = center[0] + radius;
		auto min_y = center[1] - radius;
		return std::make_tuple(max_x, min_y);
	}	
	auto CircleMeasureSep::SetHandleSize(double rad) -> void {
		d->handleRadius = rad;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = rad * cos(2 * M_PI / div * i);
			auto y_pos = rad * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}
		d->circleProp->touch();
    }

	void CircleMeasureSep::Activate() {
		d->isActivated = true;
		d->circle->Activate();
	}

	void CircleMeasureSep::Deactivate() {
		d->isActivated = false;
		d->circle->Deactivate();
	}
	void CircleMeasureSep::Finish(QString text) {
		emit sigFinish(text);
	}
	void CircleMeasureSep::Update(int index, QString text) {
		emit sigUpdate(index, text);
	}
	auto CircleMeasureSep::Init()->void {
		d->circle = new OivCircleDrawer;
		d->root = new SoSeparator;
		d->handleRoot = new SoSeparator;
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->root->addChild(d->circle);
		d->root->addChild(d->handleRoot);

		d->circle->setUserData(d.get());
		d->circle->onFinish.add(RenderCB);

		d->circleProp = new SoVertexProperty;
		auto radius = d->handleRadius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}

		d->callback = new SoEventCallback;
		d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), MouseButtonCB, d.get());
		d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), MouseMoveCB, d.get());
		d->root->insertChild(d->callback, 0);
	}
	auto CircleMeasureSep::GetMeasure() -> QList<circleMeasure> {
		return d->measure;
    }
	auto CircleMeasureSep::Clear()->void {
		d->handleRoot->removeAllChildren();
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());
		for (auto i = 0; i < 3; i++) {
			d->handleMatl[i].clear();
			d->handleTrans[i].clear();
		}
		for (auto i = 0; i < 2; i++) {
			d->guideProp[i].clear();
			d->curAngle[i].clear();
		}
		d->points.clear();
		d->prop.clear();
		d->labelText.clear();
		d->labelPos.clear();
		d->measure.clear();
		d->lineMatl.clear();
	}
	auto CircleMeasureSep::ClearHighlight() -> void {
        for(auto matl: d->lineMatl) {
			matl->ambientColor.setValue(1, 1, 0);
			matl->diffuseColor.setValue(1, 1, 0);
        }
    }
	auto CircleMeasureSep::HighlightItem(int idx) -> bool {
		if (d->prop.count() <= idx) {
			return false;
		}
		if (d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		d->lineMatl[idx]->ambientColor.setValue(0, 1, 0);
		d->lineMatl[idx]->diffuseColor.setValue(0, 1, 0);
		return true;
    }

	auto CircleMeasureSep::DeleteItem(int idx) -> bool {
        if(d->prop.count() <= idx) {
			return false;
        }
		if(d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		d->handleRoot->removeChild(idx + 1);
		for(auto i=0;i<3;i++) {
			d->handleMatl[i].removeAt(idx);
			d->handleTrans[i].removeAt(idx);
		}
		for(auto i=0;i<2;i++) {
			d->guideProp[i].removeAt(idx);
			d->curAngle[i].removeAt(idx);
		}
		d->points.removeAt(idx);
		d->prop.removeAt(idx);
		d->labelText.removeAt(idx);
		d->labelPos.removeAt(idx);
		d->measure.removeAt(idx);
		d->lineMatl.removeAt(idx);
		return true;
    }
	auto CircleMeasureSep::GetRoot()->SoSeparator* {
		return d->root;
	}
	auto CircleMeasureSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
		std::vector<SbVec3f> result;

		SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
		for (int i = 0; i < source->getNum(); i++) {

			SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
			normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
			normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

			rayPick.setNormalizedPoint(normalizedPoint);
			rayPick.apply(targetNode);

			SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
			if (pickedPoint) {
				result.push_back(pickedPoint->getPoint());
			}
		}

		return result;
	}
	void CircleMeasureSep::RenderCB(SoPolyLineScreenDrawer::EventArg& arg) {
		OivCircleDrawer* source = (OivCircleDrawer*)arg.getSource();
		SoHandleEventAction* action = arg.getAction();
		auto dd = static_cast<Impl*>(source->getUserData());

		auto sourceCount = source->point.getNum();

		auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());

		if (sourceCount != resultRayCast.size()) {
			return;
		}

		SoMFVec2f additionalPts;
		additionalPts.set1Value(0, source->GetStartPt());
		additionalPts.set1Value(1, source->GetEndPt());

		auto resultRadiusPts = CalcRayCasting(&additionalPts, action->getPickRoot(), action->getViewportRegion());
		if (resultRadiusPts.size() < 2) {
			source->clear();
			return;
		}

		dd->curAngle[0].append(-M_PI / 2.0);
		dd->curAngle[1].append(0);

		auto centerPt = (resultRadiusPts[0] + resultRadiusPts[1]) / 2;

		auto xDiff = resultRadiusPts[0][0] - centerPt[0];
		auto yDiff = resultRadiusPts[0][1] - centerPt[1];
		auto r = sqrt(xDiff * xDiff + yDiff * yDiff);
		auto area = r * r * M_PI;

		auto handlePos1 = SbVec3f(centerPt[0], centerPt[1] - r, centerPt[2]);
		auto handlePos2 = SbVec3f(centerPt[0] + r, centerPt[1], centerPt[2]);

		//Add circle
		SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
		drawStyle->setName("drawStyle");
		drawStyle->lineWidth = 2;
		SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;

		auto minmax = dd->drawCircle(handlePos1, centerPt, vertexProp.ptr());
		auto max_x = std::get<0>(minmax);
		auto min_y = std::get<1>(minmax);

		dd->prop.append(vertexProp.ptr());

		SoRef<SoLineSet> lineSet = new SoLineSet;
		lineSet->setName("circle");
		lineSet->vertexProperty = vertexProp.ptr();

		SoRef<SoMaterial> lineMatl = new SoMaterial;
		lineMatl->ambientColor.setValue(1, 1, 0);
		lineMatl->diffuseColor.setValue(1, 1, 0);

		dd->lineMatl.append(lineMatl.ptr());

		SoRef<SoSeparator> lineSep = new SoSeparator;
		lineSep->addChild(drawStyle.ptr());
		lineSep->addChild(lineMatl.ptr());
		lineSep->addChild(lineSet.ptr());
		
		//Add text
		SoRef<SoSeparator> textSep = new SoSeparator;
		textSep->setName("textSep");

		SoRef<SoTranslation> labelPos = new SoTranslation();
		labelPos->translation.setValue(max_x, min_y, 0);

		textSep->addChild(labelPos.ptr());
		dd->labelPos.append(labelPos.ptr());

		SoRef<SoFont> labelFont = new SoFont();
		labelFont->size = 13.0;
		labelFont->name = "Arial:Bold";
		labelFont->renderStyle = SoFont::TEXTURE;
		textSep->addChild(labelFont.ptr());

		SoTextProperty* textProp = new SoTextProperty();
		textProp->alignmentH = SoTextProperty::LEFT;
		textProp->alignmentV = SoTextProperty::TOP;
		textSep->addChild(textProp);

		SoRef<SoText2> labelText = new SoText2();
		labelText->justification = SoText2::INHERITED;
		textSep->addChild(labelText.ptr());
		dd->labelText.append(labelText.ptr());

		SoRef<SoMaterial> fontMat = new SoMaterial;
		fontMat->ambientColor.setValue(1, 1, 0);
		fontMat->diffuseColor.setValue(1, 1, 0);
		textSep->addChild(fontMat.ptr());

		circleMeasure newMeasure;
		newMeasure.area = area;
		newMeasure.radius = r;
		newMeasure.circumference = 2 * M_PI * r;

		dd->measure.append(newMeasure);

	    QString volumeText = QString::number(area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");

		QString label_text = QString("Area: %1 \n").arg(volumeText);
		label_text.append(QString("Radius: %1 \n").arg(QString::number(r, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
		label_text.append(QString("Circumference: %1").arg(QString::number(2 * M_PI * r, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
		labelText->string = volumeText.toStdWString();

		//handle shape
		SoRef<SoDrawStyle> handleStyle = new SoDrawStyle;
		handleStyle->setName("handleStyle");
		handleStyle->style = SoDrawStyle::FILLED;
		SoRef<SoFaceSet> faceSet = new SoFaceSet;
		faceSet->vertexProperty = dd->circleProp.ptr();
		SoRef<SoSeparator> handleSep = new SoSeparator;
		handleSep->addChild(handleStyle.ptr());
		SoRef<SoDrawStyle> guideStyle = new SoDrawStyle;
		guideStyle->lineWidth = 1.8;
		guideStyle->linePattern = 52428;//16 bit toggle pattern
		//1100110011001100 = 52428

		//Add center handle
		SoRef<SoSeparator> handle1 = new SoSeparator;
		handleSep->addChild(handle1.ptr());
		SoRef<SoMaterial> handleMatl1 = new SoMaterial;
		handleMatl1->ambientColor.setValue(1, 0, 0);
		handleMatl1->diffuseColor.setValue(1, 0, 0);
		dd->handleMatl[0].append(handleMatl1.ptr());

		SoRef<SoTranslation> handleTrans1 = new SoTranslation;
		handleTrans1->translation.setValue(centerPt);
		handle1->addChild(handleMatl1.ptr());
		handle1->addChild(handleTrans1.ptr());
		handle1->addChild(faceSet.ptr());
		dd->handleTrans[0].append(handleTrans1.ptr());

		//add axis1 handle
		SoRef<SoSeparator> axis1Handle = new SoSeparator;
		handleSep->addChild(axis1Handle.ptr());
		SoRef<SoMaterial> axis1Matl = new SoMaterial;
		axis1Matl->ambientColor.setValue(1, 0, 0);
		axis1Matl->diffuseColor.setValue(1, 0, 0);
		dd->handleMatl[1].append(axis1Matl.ptr());

		SoRef<SoTranslation> axis1Trans = new SoTranslation;
		axis1Trans->translation.setValue(handlePos1);
		axis1Handle->addChild(axis1Matl.ptr());
		axis1Handle->addChild(axis1Trans.ptr());
		axis1Handle->addChild(faceSet.ptr());
		dd->handleTrans[1].append(axis1Trans.ptr());

		//add axis 1 guideline
		SoRef<SoSeparator> axis1Line = new SoSeparator;
		axis1Line->addChild(guideStyle.ptr());
		SoRef<SoMaterial> axis1LineMatl = new SoMaterial;
		axis1LineMatl->ambientColor.setValue(1, 0, 0);
		axis1LineMatl->diffuseColor.setValue(1, 0, 0);
		axis1Line->addChild(axis1LineMatl.ptr());
		SoRef<SoLineSet> axis1LineSet = new SoLineSet;
		SoRef<SoVertexProperty> axis1Prop = new SoVertexProperty;
		axis1Prop->vertex.set1Value(0, centerPt);
		axis1Prop->vertex.set1Value(1, handlePos1);
		axis1LineSet->vertexProperty = axis1Prop.ptr();
		axis1Line->addChild(axis1LineSet.ptr());
		handleSep->addChild(axis1Line.ptr());
		dd->guideProp[0].append(axis1Prop.ptr());

		//add axis 2 handle
		SoRef<SoSeparator> axis2Handle = new SoSeparator;
		handleSep->addChild(axis2Handle.ptr());
		SoRef<SoMaterial> axis2Matl = new SoMaterial;
		axis2Matl->ambientColor.setValue(1, 0, 0);
		axis2Matl->diffuseColor.setValue(1, 0, 0);
		dd->handleMatl[2].append(axis2Matl.ptr());

		SoRef<SoTranslation> axis2Trans = new SoTranslation;
		axis2Trans->translation.setValue(handlePos2);
		axis2Handle->addChild(axis2Matl.ptr());
		axis2Handle->addChild(axis2Trans.ptr());
		axis2Handle->addChild(faceSet.ptr());
		dd->handleTrans[2].append(axis2Trans.ptr());

		//add axis 2 guideline
		SoRef<SoSeparator> axis2Line = new SoSeparator;
		axis2Line->addChild(guideStyle.ptr());
		SoRef<SoMaterial> axis2LineMatl = new SoMaterial;
		axis2LineMatl->ambientColor.setValue(1, 0, 0);
		axis2LineMatl->diffuseColor.setValue(1, 0, 0);
		axis2Line->addChild(axis2LineMatl.ptr());
		SoRef<SoLineSet> axis2LineSet = new SoLineSet;
		SoRef<SoVertexProperty> axis2Prop = new SoVertexProperty;
		axis2Prop->vertex.set1Value(0, centerPt);
		axis2Prop->vertex.set1Value(1, handlePos2);
		axis2LineSet->vertexProperty = axis2Prop.ptr();
		axis2Line->addChild(axis2LineSet.ptr());
		handleSep->addChild(axis2Line.ptr());
		dd->guideProp[1].append(axis2Prop.ptr());

		SoRef<SoSeparator> handleGroup = new SoSeparator;
		handleGroup->addChild(handleSep.ptr());
		handleGroup->addChild(lineSep.ptr());
		handleGroup->addChild(textSep.ptr());

		dd->handleRoot->addChild(handleGroup.ptr());
		
		dd->instance->Finish(label_text);
	}

	void CircleMeasureSep::MouseMoveCB(void* pImpl, SoEventCallback* eventCB) {		
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};

		auto dd = static_cast<Impl*>(pImpl);
		if (false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		if (dd->isInHandle) {
			//consume event
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback				
				eventCB->releaseEvents();
				return;
			}


			dd->handleTrans[dd->handleIdx][dd->circleIdx]->translation.setValue(p->getPoint());

			if (dd->handleIdx == 0) {//center move
				auto diff = p->getPoint() - dd->pickedHandlePos[0];
				auto handle1NewTrans = dd->pickedHandlePos[1] + diff;
				dd->guideProp[0][dd->circleIdx]->vertex.set1Value(0, p->getPoint());
				dd->guideProp[0][dd->circleIdx]->vertex.set1Value(1, handle1NewTrans);
				dd->handleTrans[1][dd->circleIdx]->translation.setValue(handle1NewTrans);

				auto handle2NewTrans = dd->pickedHandlePos[2] + diff;
				dd->guideProp[1][dd->circleIdx]->vertex.set1Value(0, p->getPoint());
				dd->guideProp[1][dd->circleIdx]->vertex.set1Value(1, handle2NewTrans);
				dd->handleTrans[2][dd->circleIdx]->translation.setValue(handle2NewTrans);

				auto minmax = dd->drawCircle(handle1NewTrans, p->getPoint(), dd->prop[dd->circleIdx]);

				auto max_x = std::get<0>(minmax);
				auto min_y = std::get<1>(minmax);

				dd->labelPos[dd->circleIdx]->translation.setValue(max_x, min_y, 0);
			}
			else {
				auto guideIdx = dd->handleIdx - 1;
				dd->guideProp[guideIdx][dd->circleIdx]->vertex.set1Value(1, p->getPoint());
				auto perpenIdx = 1;
				if (dd->handleIdx == 1) {
					perpenIdx = 2;
				}

				SbVec3f baseVector;
				baseVector = p->getPoint() - dd->pickedHandlePos[0];
				baseVector.normalize();
				SbVec3f majorVector = dd->pickedHandlePos[dd->handleIdx] - dd->pickedHandlePos[0];
				majorVector.normalize();

				auto dot = majorVector[0] * baseVector[0] + majorVector[1] * baseVector[1];
				auto det = majorVector[0] * baseVector[1] - majorVector[1] * baseVector[0];
				auto angle = atan2(det, dot);
				dd->movingAngle = angle;
				auto perpenAngle = dd->curAngle[perpenIdx - 1][dd->circleIdx] + angle;

				auto radiusDiff = p->getPoint() - dd->pickedHandlePos[0];
				auto perpenDist = sqrt(radiusDiff[0] * radiusDiff[0] + radiusDiff[1] * radiusDiff[1]);
				auto perpenX = cos(perpenAngle) * perpenDist;
				auto perpenY = sin(perpenAngle) * perpenDist;

				auto perpenPos = SbVec3f(perpenX + dd->handleTrans[0][dd->circleIdx]->translation.getValue()[0], perpenY + dd->handleTrans[0][dd->circleIdx]->translation.getValue()[1], dd->pickedHandlePos[dd->handleIdx][2]);
				dd->guideProp[perpenIdx - 1][dd->circleIdx]->vertex.set1Value(1, perpenPos);

				dd->handleTrans[perpenIdx][dd->circleIdx]->translation.setValue(perpenPos);

				auto minmax = dd->drawCircle(p->getPoint(), dd->pickedHandlePos[0], dd->prop[dd->circleIdx]);

				auto max_x = std::get<0>(minmax);
				auto min_y = std::get<1>(minmax);

				dd->labelPos[dd->circleIdx]->translation.setValue(max_x, min_y, 0);

				dd->measure[dd->circleIdx].area = perpenDist * perpenDist * M_PI;
				dd->measure[dd->circleIdx].radius = perpenDist;
				dd->measure[dd->circleIdx].circumference = 2 * M_PI * perpenDist;

				QString volumeText = QString::number(perpenDist * perpenDist * M_PI, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");

				QString label_text = QString("Area: %1 \n").arg(volumeText);
				label_text.append(QString("Radius: %1 \n").arg(QString::number(perpenDist, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
				label_text.append(QString("Circumference: %1").arg(QString::number(2 * M_PI * perpenDist, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
				dd->labelText[dd->circleIdx]->string = volumeText.toStdWString();

				dd->instance->Update(dd->circleIdx, label_text);
			}
			eventCB->setHandled();
			return;
		}
		eventCB->releaseEvents();
	}

	void CircleMeasureSep::MouseButtonCB(void* pImpl, SoEventCallback* eventCB) {
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};

		auto dd = static_cast<Impl*>(pImpl);
		if(false ==dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		const SoEvent* event = eventCB->getEvent();
		const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback	
				eventCB->releaseEvents();
				return;
			}
			auto point = p->getPoint();
			dd->isInHandle = false;
			dd->circleIdx = -1;
			dd->handleIdx = -1;

			for (auto i = 0; i < dd->prop.count(); i++) {//for ellipsoid
				for (auto j = 0; j < 3; j++) {//for handle
					auto handlePos = dd->handleTrans[j][i]->translation.getValue();
					if (false == doubleComp(handlePos[2], point[2])) {
						continue;
					}
					auto dist = sqrt(pow(handlePos[0] - point[0], 2) + pow(handlePos[1] - point[1], 2));
					if (dist <= dd->handleRadius) {
						dd->isInHandle = true;
						dd->circleIdx = i;
						dd->handleIdx = j;
						dd->pickedHandlePos[0] = dd->handleTrans[0][dd->circleIdx]->translation.getValue();
						dd->pickedHandlePos[1] = dd->handleTrans[1][dd->circleIdx]->translation.getValue();
						dd->pickedHandlePos[2] = dd->handleTrans[2][dd->circleIdx]->translation.getValue();
						break;
					}
				}
				if (dd->isInHandle) {
					break;
				}
			}
			if (dd->isInHandle) {
				dd->handleMatl[dd->handleIdx][dd->circleIdx]->ambientColor.setValue(1, 1, 0);
				dd->handleMatl[dd->handleIdx][dd->circleIdx]->diffuseColor.setValue(1, 1, 0);
			}
		}
		bool releaseEvent = false;
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (dd->isInHandle) {
				SoHandleEventAction* action = eventCB->getAction();
				const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

				SoRayPickAction pickaction = SoRayPickAction(myRegion);
				pickaction.setPoint(event->getPosition());
				pickaction.setSceneManager(action->getSceneManager());
				pickaction.apply(action->getPickRoot());
				auto p = pickaction.getPickedPoint();

				dd->handleMatl[dd->handleIdx][dd->circleIdx]->ambientColor.setValue(1, 0, 0);
				dd->handleMatl[dd->handleIdx][dd->circleIdx]->diffuseColor.setValue(1, 0, 0);
				for (auto i = 0; i < 2; i++) {
					float val = dd->curAngle[i][dd->circleIdx] + dd->movingAngle;
					if (val > M_PI * 2) {
						val -= (M_PI * 2);
					}
					else if (val < -M_PI * 2) {
						val += (M_PI * 2);
					}
					dd->curAngle[i][dd->circleIdx] = val;
				}

				dd->isInHandle = false;
				dd->handleIdx = -1;
				dd->circleIdx = -1;
				dd->movingAngle = 0.0;

				releaseEvent = true;
			}
		}
		if (dd->isInHandle || releaseEvent) {
			eventCB->setHandled();
			return;
		}
		eventCB->releaseEvents();
	}
}