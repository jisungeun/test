#pragma warning(push)
#pragma warning(disable:4819)

#include "OivMultiLineDrawer.h"
#include "MultiLineMeasureSep.h"

#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#pragma warning(pop)

namespace TC {
	struct MultiLineMeasureSep::Impl {
		SoSeparator* root{ nullptr };
		SoSeparator* handleRoot{ nullptr };
		OivMultiLineDrawer* line{ nullptr };
		SoEventCallback* callback{ nullptr };
		SoRef<SoVertexProperty> circleProp{ nullptr };

		MultiLineMeasureSep* instance = nullptr;

		//container
		double handleRadius{ 0.5 };
		bool isInHandle{ false };
		bool inProcess{ false };
		int lineIdx = -1;
		int startIdx = -1;
		int countIdx = -1;
		QList<QList<SbVec3f>> linePoints[2];
		QList<QList<SoMaterial*>> handleMatl[2];
		QList<QList<SoTranslation*>> lineTrans[2];
		QList<QList<SoVertexProperty*>> lineProp;
		QList<QList<SoText2*>> labelText;
		QList<QList<SoTranslation*>> labelPos;
		QList<QList<SoMaterial*>> lineMatl;
		QList<float> pathLength;

		bool isActivated{ false };
	};
	MultiLineMeasureSep::MultiLineMeasureSep(QObject* parent) : QObject(parent), d{ new Impl } {
		Init();
		d->instance = this;
	}
	MultiLineMeasureSep::~MultiLineMeasureSep() {

	}
	auto MultiLineMeasureSep::SetHandleSize(double radius) -> void {
		d->handleRadius = radius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}
		d->circleProp->touch();
    }
	void MultiLineMeasureSep::Activate() {
		d->line->Activate();
		d->isActivated = true;
	}

	void MultiLineMeasureSep::Deactivate() {
		d->line->Deactivate();
		d->isActivated = false;
	}
	void MultiLineMeasureSep::Finish(QString text) {
		emit sigFinish(text);
	}

	void MultiLineMeasureSep::Update(int index, QString text) {
		emit sigUpdate(index, text);
	}
	auto MultiLineMeasureSep::Init()->void {
		d->line = new OivMultiLineDrawer;
		d->root = new SoSeparator;
		d->handleRoot = new SoSeparator;
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->root->addChild(d->line);
		d->root->addChild(d->handleRoot);

		d->line->setUserData(d.get());
		d->line->onFinish.add(lineCallback);

		d->circleProp = new SoVertexProperty;
		auto radius = d->handleRadius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}

		d->callback = new SoEventCallback;
		d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), LineButtonCB, d.get());
		d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), LineMoveCB, d.get());
		d->root->insertChild(d->callback, 0);
	}

	auto MultiLineMeasureSep::GetMeasure() -> QList<float> {
		return d->pathLength;
    }

	auto MultiLineMeasureSep::Clear()->void {
		d->handleRoot->removeAllChildren();
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());
		d->linePoints[0].clear();
		d->linePoints[1].clear();
		d->handleMatl[0].clear();
		d->handleMatl[1].clear();
		d->lineTrans[0].clear();
		d->lineTrans[1].clear();
		d->lineProp.clear();
		d->labelText.clear();
		d->labelPos.clear();
		d->pathLength.clear();
		d->lineMatl.clear();
	}
	auto MultiLineMeasureSep::ClearHighlight() -> void {
        for(auto matlList : d->lineMatl) {
            for(auto matl : matlList) {
				matl->ambientColor.setValue(1, 1, 0);
				matl->diffuseColor.setValue(1, 1, 0);
            }
        }
    }
	auto MultiLineMeasureSep::HighlightItem(int idx) -> bool {
		if (d->lineProp.count() <= idx) {
			return false;
		}
		if (d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		for(auto matl: d->lineMatl[idx]) {
			matl->diffuseColor.setValue(0, 1, 0);
			matl->ambientColor.setValue(0, 1, 0);
		}
		return true;
    }

	auto MultiLineMeasureSep::DeleteItem(int idx) -> bool {
        if(d->lineProp.count() <= idx) {
			return false;
        }
		if(d->handleRoot->getNumChildren() < idx) {
			return false;
		}

		d->handleRoot->removeChild(idx + 1);
		for(auto i=0;i<2;i++) {
			d->linePoints[i].removeAt(idx);
			d->handleMatl[i].removeAt(idx);
			d->lineTrans[i].removeAt(idx);
		}
		d->lineProp.removeAt(idx);
		d->labelText.removeAt(idx);
		d->labelPos.removeAt(idx);
		d->pathLength.removeAt(idx);
		d->lineMatl.removeAt(idx);

		return true;
    }

	auto MultiLineMeasureSep::GetRoot()->SoSeparator* {
		return d->root;
	}
	auto MultiLineMeasureSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
		std::vector<SbVec3f> result;

		SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
		for (int i = 0; i < source->getNum(); i++) {

			SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
			normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
			normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

			rayPick.setNormalizedPoint(normalizedPoint);
			rayPick.apply(targetNode);

			SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
			if (pickedPoint) {
				result.push_back(pickedPoint->getPoint());
			}
		}

		return result;
	}
	void MultiLineMeasureSep::lineCallback(SoPolyLineScreenDrawer::EventArg& arg) {
		OivMultiLineDrawer* source = (OivMultiLineDrawer*)arg.getSource();
		SoHandleEventAction* action = arg.getAction();
		auto dd = static_cast<Impl*>(source->getUserData());
		if (source->point.getNum() < 1) {
			source->clear();
			return;
		}

		auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());
		if (resultRayCast.size() < 1) {
			source->clear();
			return;
		}

		dd->lineProp.append(QList<SoVertexProperty*>());
		dd->labelPos.append(QList<SoTranslation*>());
		dd->labelText.append(QList<SoText2*>());
		dd->linePoints[0].append(QList<SbVec3f>());
		dd->linePoints[1].append(QList<SbVec3f>());
		dd->lineTrans[0].append(QList<SoTranslation*>());
		dd->lineTrans[1].append(QList<SoTranslation*>());
		dd->handleMatl[0].append(QList<SoMaterial*>());
		dd->handleMatl[1].append(QList<SoMaterial*>());
		dd->lineMatl.append(QList<SoMaterial*>());
		auto total = 0.0;

		SoRef<SoSeparator> handleGroup = new SoSeparator;
		
		for (auto i = 0; i < resultRayCast.size() - 1; ++i) {
			auto xDiff = resultRayCast[i + 1][0] - resultRayCast[i][0];
			auto yDiff = resultRayCast[i + 1][1] - resultRayCast[i][1];

			//Add line
			SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
			drawStyle->setName("drawStyle");
			drawStyle->lineWidth = 2;
			SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
			vertexProp->vertex.set1Value(0, resultRayCast[i]);
			vertexProp->vertex.set1Value(1, resultRayCast[i + 1]);

			dd->lineProp[dd->lineProp.count() - 1].append(vertexProp.ptr());

			SoRef<SoLineSet> lineSet = new SoLineSet;
			lineSet->setName("myline");
			lineSet->vertexProperty = vertexProp.ptr();

			dd->linePoints[0][dd->linePoints[0].count() - 1].append(resultRayCast[i]);
			dd->linePoints[1][dd->linePoints[0].count() - 1].append(resultRayCast[i + 1]);

			SoRef<SoMaterial> lineMatl = new SoMaterial;
			lineMatl->ambientColor.setValue(1, 1, 0);
			lineMatl->diffuseColor.setValue(1, 1, 0);

			dd->lineMatl[dd->lineMatl.count() - 1].append(lineMatl.ptr());

			SoRef<SoSeparator> lineSep = new SoSeparator;
			lineSep->addChild(drawStyle.ptr());
			lineSep->addChild(lineMatl.ptr());
			lineSep->addChild(lineSet.ptr());
			
			//Add text
			SoRef<SoSeparator> textSep = new SoSeparator;
			textSep->setName("textSep");

			SoRef<SoTranslation> labelPos = new SoTranslation();
			labelPos->translation.setValue(((resultRayCast[i][0] + resultRayCast[i + 1][0]) / 2) - 2.0,
				((resultRayCast[i][1] + resultRayCast[i + 1][1]) / 2) + 0.5,
				(resultRayCast[i][2] + resultRayCast[i + 1][2]) / 2);

			textSep->addChild(labelPos.ptr());
			dd->labelPos[dd->labelPos.count() - 1].append(labelPos.ptr());

			SoRef<SoFont> labelFont = new SoFont();
			labelFont->size = 13.0;
			labelFont->name = "Arial:Bold";
			labelFont->renderStyle = SoFont::TEXTURE;
			textSep->addChild(labelFont.ptr());

			SoTextProperty* textProp = new SoTextProperty();
			textProp->alignmentH = SoTextProperty::LEFT;
			textProp->alignmentV = SoTextProperty::TOP;
			textSep->addChild(textProp);

			SoRef<SoText2> labelText = new SoText2();
			labelText->justification = SoText2::INHERITED;
			textSep->addChild(labelText.ptr());
			//dd->labelText.append(QList<SoText2*>());
			dd->labelText[dd->labelText.count() - 1].append(labelText.ptr());

			SoRef<SoMaterial> fontMat = new SoMaterial;
			fontMat->ambientColor.setValue(1, 1, 0);
			fontMat->diffuseColor.setValue(1, 1, 0);
			textSep->addChild(fontMat.ptr());
			
			QString label_text = QString::number(sqrt(xDiff * xDiff + yDiff * yDiff), 'f', 2) + " um";
			label_text.replace("u", QString::fromWCharArray(L"\x00B5"));
			labelText->string = label_text.toStdWString();
			total += sqrt(xDiff * xDiff + yDiff * yDiff);

			//Add handles			
			SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
			circleStyle->setName("circleStyle");
			circleStyle->style = SoDrawStyle::FILLED;

			SoRef<SoFaceSet> faceSet = new SoFaceSet;
			faceSet->vertexProperty = dd->circleProp.ptr();

			SoRef<SoSeparator> circleSep = new SoSeparator;
			circleSep->addChild(circleStyle.ptr());

			SoRef<SoSeparator> circle1 = new SoSeparator;
			circleSep->addChild(circle1.ptr());
			SoRef<SoMaterial> circleMatl1 = new SoMaterial;
			circleMatl1->ambientColor.setValue(1, 0, 0);
			circleMatl1->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans1 = new SoTranslation;
			circleTrans1->translation.setValue(resultRayCast[i]);
			circle1->addChild(circleMatl1.ptr());
			circle1->addChild(circleTrans1.ptr());
			circle1->addChild(faceSet.ptr());

			SoRef<SoSeparator> circle2 = new SoSeparator;
			circleSep->addChild(circle2.ptr());
			SoRef<SoMaterial> circleMatl2 = new SoMaterial;
			circleMatl2->ambientColor.setValue(1, 0, 0);
			circleMatl2->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans2 = new SoTranslation;
			circleTrans2->translation.setValue(resultRayCast[i + 1]);
			circle2->addChild(circleMatl2.ptr());
			circle2->addChild(circleTrans2.ptr());
			circle2->addChild(faceSet.ptr());

			dd->lineTrans[0][dd->lineTrans[0].count() - 1].append(circleTrans1.ptr());
			dd->lineTrans[1][dd->lineTrans[1].count() - 1].append(circleTrans2.ptr());

			dd->handleMatl[0][dd->handleMatl[0].count() - 1].append(circleMatl1.ptr());
			dd->handleMatl[1][dd->handleMatl[1].count() - 1].append(circleMatl2.ptr());

			handleGroup->addChild(circleSep.ptr());
			handleGroup->addChild(lineSep.ptr());
			handleGroup->addChild(textSep.ptr());
		}
		dd->handleRoot->addChild(handleGroup.ptr());

		dd->pathLength.append(total);		

		QString label_text = QString("Total Length: ") + QString::number(total, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m";
		dd->instance->Finish(label_text);
		dd->inProcess = false;
	}
	void MultiLineMeasureSep::LineMoveCB(void* pImpl, SoEventCallback* eventCB) {
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		if (dd->isInHandle) {
			//consume event
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();			
			if (p == NULL) {
				//broadcast mouse event to further callback				
				eventCB->releaseEvents();
				return;
			}
			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->translation.setValue(p->getPoint());
			dd->lineProp[dd->lineIdx][dd->countIdx]->vertex.set1Value(dd->startIdx, p->getPoint());

			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->touch();

			if (dd->countIdx > 0 && dd->startIdx == 0) {
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx - 1]->vertex.set1Value(1, p->getPoint());
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.touch();
			}
			if (dd->countIdx + 1 < dd->lineProp[dd->lineIdx].count() && dd->startIdx == 1) {
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx + 1]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.touch();
			}

			eventCB->setHandled();
			return;
		}
		eventCB->releaseEvents();
	}
	void MultiLineMeasureSep::LineButtonCB(void* pImpl, SoEventCallback* eventCB) {
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		const SoEvent* event = eventCB->getEvent();
		const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			//check whether handle is under click or not
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();			
			if (NULL == p) {				
				eventCB->setHandled();
				return;
			}
			
			auto point = p->getPoint();
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
			if (false == dd->inProcess) {
				for (auto i = 0; i < dd->lineProp.count(); i++) {//travel paths
					for (auto k = 0; k < dd->lineProp[i].count(); k++) {//travel lines in path
						for (auto j = 0; j < 2; j++) {//travel start or end point
							if (false == doubleComp(dd->linePoints[j][i][k][2], point[2])) {
								continue;
							}
							auto dist = sqrt(pow(dd->linePoints[j][i][k][0] - point[0], 2) + pow(dd->linePoints[j][i][k][1] - point[1], 2));
							if (dist > dd->handleRadius) {
								continue;
							}
							dd->isInHandle = true;
							dd->lineIdx = i;
							dd->startIdx = j;
							dd->countIdx = k;
							break;
						}
					}
					if (dd->isInHandle) {
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 1, 0);
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 1, 0);
						break;
					}
				}
			}
			dd->inProcess = !dd->isInHandle;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (dd->isInHandle) {
				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 0, 0);
				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 0, 0);

				SoHandleEventAction* action = eventCB->getAction();
				const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
				
				SoRayPickAction pickaction = SoRayPickAction(myRegion);
				pickaction.setPoint(event->getPosition());
				pickaction.setSceneManager(action->getSceneManager());
				pickaction.apply(action->getPickRoot());
				auto p = pickaction.getPickedPoint();
				
				SbVec3f point;
				if (p == NULL) {
					point = dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->translation.getValue();
				}
				else {
					point = p->getPoint();
				}				
				dd->linePoints[dd->startIdx][dd->lineIdx][dd->countIdx] = point;

				//recompute line length
				auto p1 = dd->linePoints[(dd->startIdx + 1) % 2][dd->lineIdx][dd->countIdx];
				auto p2 = dd->linePoints[dd->startIdx][dd->lineIdx][dd->countIdx];
				auto xDiff = p1[0] - p2[0];
				auto yDiff = p1[1] - p2[1];

				dd->labelPos[dd->lineIdx][dd->countIdx]->translation.setValue((p1[0] + p2[0]) / 2 - 2.0,
					(p1[1] + p2[1]) / 2 + 0.5,
					(p1[2] + p2[2]) / 2);
				QString label_text = QString::number(sqrt(xDiff * xDiff + yDiff * yDiff), 'f', 2) + " um";
				label_text.replace("u", QString::fromWCharArray(L"\x00B5"));
				dd->labelText[dd->lineIdx][dd->countIdx]->string = label_text.toStdWString();

				if (dd->countIdx > 0 && dd->startIdx == 0) {
					dd->linePoints[1][dd->lineIdx][dd->countIdx - 1] = point;

					auto pp1 = dd->linePoints[0][dd->lineIdx][dd->countIdx - 1];
					auto pp2 = dd->linePoints[1][dd->lineIdx][dd->countIdx - 1];

					auto xxDiff = pp1[0] - pp2[0];
					auto yyDiff = pp1[1] - pp2[1];

					dd->labelPos[dd->lineIdx][dd->countIdx - 1]->translation.setValue((pp1[0] + pp2[0]) / 2 - 2.0,
						(pp1[1] + pp2[1]) / 2 + 0.5,
						(pp1[2] + pp2[2]) / 2);

					QString next_label_text = QString::number(sqrt(xxDiff * xxDiff + yyDiff * yyDiff), 'f', 2) + " um";
					next_label_text.replace("u", QString::fromWCharArray(L"\x00B5"));
					dd->labelText[dd->lineIdx][dd->countIdx - 1]->string = next_label_text.toStdWString();
				}
				if (dd->countIdx + 1 < dd->lineProp[dd->lineIdx].count() && dd->startIdx == 1) {
					dd->linePoints[0][dd->lineIdx][dd->countIdx + 1] = point;

					auto pp1 = dd->linePoints[0][dd->lineIdx][dd->countIdx + 1];
					auto pp2 = dd->linePoints[1][dd->lineIdx][dd->countIdx + 1];

					auto xxDiff = pp1[0] - pp2[0];
					auto yyDiff = pp1[1] - pp2[1];

					dd->labelPos[dd->lineIdx][dd->countIdx + 1]->translation.setValue((pp1[0] + pp2[0]) / 2 - 2.0,
						(pp1[1] + pp2[1]) / 2 + 0.5,
						(pp1[2] + pp2[2]) / 2);

					QString prev_label_text = QString::number(sqrt(xxDiff * xxDiff + yyDiff * yyDiff), 'f', 2) + " um";
					prev_label_text.replace("u", QString::fromWCharArray(L"\x00B5"));
					dd->labelText[dd->lineIdx][dd->countIdx + 1]->string = prev_label_text.toStdWString();
				}

				auto total = 0.0;
				for (auto a = 0; a < dd->lineProp[dd->lineIdx].count() - 1; a++) {//travel lines in path
					auto pp1 = dd->linePoints[0][dd->lineIdx][a + 1] - dd->linePoints[0][dd->lineIdx][a];
					auto pp2 = dd->linePoints[1][dd->lineIdx][a + 1] - dd->linePoints[1][dd->lineIdx][a];

					auto xxDiff = pp1[0] - pp2[0];
					auto yyDiff = pp1[1] - pp2[1];
					total += sqrt(xxDiff * xxDiff + yyDiff * yyDiff);
				}

				dd->pathLength[dd->lineIdx] = total;

				QString tt = QString("Total Length: ") + QString::number(total, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m";
				dd->instance->Update(dd->lineIdx, tt);
			}
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
		}

		if (dd->isInHandle) {
			//consume event
			eventCB->setHandled();
			return;
		}

		SoHandleEventAction* action2 = eventCB->getAction();
		const SbViewportRegion& myRegion2 = eventCB->getAction()->getViewportRegion();

		SoRayPickAction pickaction2 = SoRayPickAction(myRegion2);
		pickaction2.setPoint(event->getPosition());
		pickaction2.setSceneManager(action2->getSceneManager());
		pickaction2.apply(action2->getPickRoot());
		auto p = pickaction2.getPickedPoint();
		if (p == NULL) {
			dd->line->Reset();
		}

		auto pos = mouseButton->getPositionFloat();
		auto viewport_size = myRegion2.getViewportSizePixels();
		auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
		if (mouse_in_setting) {			
			dd->line->Reset();			
		}

		eventCB->releaseEvents();
	}
}