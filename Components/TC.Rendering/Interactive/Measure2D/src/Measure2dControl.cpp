#include <iostream>

#include <QIcon>
#include <QButtonGroup>
#include <QFileDialog>
#include <QSettings>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#pragma warning(pop)

#include "LineMeasureSep.h"
#include "MultiLineMeasureSep.h"
#include "AngleMeasureSep.h"
#include "CircleMeasureSep.h"
#include "EllipsoidMeasureSep.h"
#include "PolygonMeasureSep.h"

#include "ui_Measure2dControl.h"
#include "Measure2dControl.h"

namespace TC {
    struct Measure2dControl::Impl {
        Ui::Measure2dControl ui;

        QButtonGroup* buttonGroup{ nullptr };

        //root switch
        SoSeparator* toolRoot{ nullptr };
        SoSwitch* toolSwitch{ nullptr, };

        //measurement tools
        LineMeasureSep* lineTool{ nullptr };
        MultiLineMeasureSep* multiLineTool{ nullptr };
        AngleMeasureSep* angleTool{ nullptr };
        CircleMeasureSep* circleTool{ nullptr };
        EllipsoidMeasureSep* ellipseTool{ nullptr };
        PolygonMeasureSep* polygonTool{ nullptr };

        QList<int> tableMap[6];
        float handleBaseSize{ 0.5 };
    };
    Measure2dControl::Measure2dControl(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        this->Init();
        this->InitTable();
        this->InitSceneGraph();
        this->InitConnections();        
    }
    Measure2dControl::~Measure2dControl() {
        
    }
    auto Measure2dControl::Init() -> void {
        this->setObjectName("panel");

        d->ui.lineBtn->setObjectName("bt-toggle");
        d->ui.pathBtn->setObjectName("bt-toggle");
        d->ui.angleBtn->setObjectName("bt-toggle");
        d->ui.circleBtn->setObjectName("bt-toggle");
        d->ui.ellipseBtn->setObjectName("bt-toggle");
        d->ui.polygonBtn->setObjectName("bt-toggle");

        d->ui.deleteBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.exportBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.selectAllBtn->setStyleSheet("font-size: 11px; font-weight: bold;");
        d->ui.deselectAllBtn->setStyleSheet("font-size: 11px; font-weight: bold;");

        QIcon lineIcon;
        lineIcon.addFile(":/img/measurement-line", QSize(24, 24), QIcon::Normal, QIcon::Off);
        lineIcon.addFile(":/img/measurement-line-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.lineBtn->setIcon(lineIcon);
        d->ui.lineBtn->setIconSize(QSize(26, 26));

        QIcon shapeIcon;
        shapeIcon.addFile(":/img/measurement-shape", QSize(24, 24), QIcon::Normal, QIcon::Off);
        shapeIcon.addFile(":/img/measurement-shape-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.pathBtn->setIcon(shapeIcon);
        d->ui.pathBtn->setIconSize(QSize(26, 26));

        QIcon angleIcon;
        angleIcon.addFile(":/img/measurement-angle", QSize(24, 24), QIcon::Normal, QIcon::Off);
        angleIcon.addFile(":/img/measurement-angle-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.angleBtn->setIcon(angleIcon);
        d->ui.angleBtn->setIconSize(QSize(26, 26));

        QIcon circleIcon;
        circleIcon.addFile(":/img/measurement-circle", QSize(24, 24), QIcon::Normal, QIcon::Off);
        circleIcon.addFile(":/img/measurement-circle-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.circleBtn->setIcon(circleIcon);
        d->ui.circleBtn->setIconSize(QSize(26, 26));

        QIcon ellipseIcon;
        ellipseIcon.addFile(":/img/measurement-ellipse", QSize(24, 24), QIcon::Normal, QIcon::Off);
        ellipseIcon.addFile(":/img/measurement-ellipse-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.ellipseBtn->setIcon(ellipseIcon);
        d->ui.ellipseBtn->setIconSize(QSize(26, 26));

        QIcon poligonIcon;
        poligonIcon.addFile(":/img/measurement-poligon", QSize(24, 24), QIcon::Normal, QIcon::Off);
        poligonIcon.addFile(":/img/measurement-poligon-s", QSize(24, 24), QIcon::Normal, QIcon::On);
        d->ui.polygonBtn->setIcon(poligonIcon);
        d->ui.polygonBtn->setIconSize(QSize(26, 26));

        d->ui.lineBtn->setStyleSheet("QPushButton{ border-top-left-radius: 6px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 6px; }");
        d->ui.pathBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.angleBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.circleBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.ellipseBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.polygonBtn->setStyleSheet("QPushButton{ border-top-left-radius: 0px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 0px; }");

        d->buttonGroup = new QButtonGroup;
        d->buttonGroup->addButton(d->ui.lineBtn, 0);
        d->buttonGroup->addButton(d->ui.pathBtn, 1);
        d->buttonGroup->addButton(d->ui.angleBtn, 2);
        d->buttonGroup->addButton(d->ui.circleBtn, 3);
        d->buttonGroup->addButton(d->ui.ellipseBtn, 4);
        d->buttonGroup->addButton(d->ui.polygonBtn, 5);
        d->buttonGroup->setExclusive(false);

        d->ui.vizToggle->setChecked(true);                
    }
    auto Measure2dControl::InitTable() -> void {        
        d->ui.measureTable->setColumnCount(2);
        d->ui.measureTable->setHorizontalHeaderLabels({ "ID","Measures" });
        auto header = d->ui.measureTable->horizontalHeader();
        header->setSectionResizeMode(QHeaderView::Stretch);
        d->ui.measureTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    }
    auto Measure2dControl::InitConnections() -> void {
        connect(d->buttonGroup, &QButtonGroup::idToggled, this, &Measure2dControl::OnButtonGroupToggled);
        connect(d->ui.vizToggle, SIGNAL(clicked()), this, SLOT(OnVizToggleClicked()));

        connect(d->lineTool, SIGNAL(sigFinish(QString)), this, SLOT(OnLineFinish(QString)));
        connect(d->lineTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnLineModified(int, QString)));
        connect(d->multiLineTool, SIGNAL(sigFinish(QString)), this, SLOT(OnPathFinish(QString)));
        connect(d->multiLineTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnPathModified(int, QString)));
        connect(d->angleTool, SIGNAL(sigFinish(QString)), this, SLOT(OnAngleFinish(QString)));
        connect(d->angleTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnAngleModified(int, QString)));
        connect(d->circleTool, SIGNAL(sigFinish(QString)), this, SLOT(OnCircleFinish(QString)));
        connect(d->circleTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnCircleModified(int, QString)));
        connect(d->ellipseTool, SIGNAL(sigFinish(QString)), this, SLOT(OnEllipseFinish(QString)));
        connect(d->ellipseTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnEllipseModified(int, QString)));
        connect(d->polygonTool, SIGNAL(sigFinish(QString)), this, SLOT(OnPolygonFinish(QString)));
        connect(d->polygonTool, SIGNAL(sigUpdate(int, QString)), this, SLOT(OnPolygonModified(int, QString)));

        connect(d->ui.deleteBtn, SIGNAL(clicked()), this, SLOT(OnDeleteItems()));
        connect(d->ui.selectAllBtn, SIGNAL(clicked()), this, SLOT(OnSelectAll()));
        connect(d->ui.exportBtn, SIGNAL(clicked()), this, SLOT(OnExport()));
        connect(d->ui.deselectAllBtn, SIGNAL(clicked()), this, SLOT(OnDeselectAll()));

        connect(d->ui.measureTable, SIGNAL(itemSelectionChanged()), this, SLOT(OnTableSelectionChanged()));
    }
    auto Measure2dControl::InitSceneGraph()->void {
        d->toolRoot = new SoSeparator;
        d->toolSwitch = new SoSwitch;
        d->toolRoot->addChild(d->toolSwitch);

        d->lineTool = new LineMeasureSep;
        d->toolSwitch->addChild(d->lineTool->GetRoot());

        d->multiLineTool = new MultiLineMeasureSep;        
        d->toolSwitch->addChild(d->multiLineTool->GetRoot());

        d->angleTool = new AngleMeasureSep;
        d->toolSwitch->addChild(d->angleTool->GetRoot());

        d->circleTool = new CircleMeasureSep;
        d->toolSwitch->addChild(d->circleTool->GetRoot());

        d->ellipseTool = new EllipsoidMeasureSep;
        d->toolSwitch->addChild(d->ellipseTool->GetRoot());

        d->polygonTool = new PolygonMeasureSep;
        d->toolSwitch->addChild(d->polygonTool->GetRoot());

        d->toolSwitch->whichChild = -3;
    }
    auto Measure2dControl::GetToolRoot()->SoSeparator* {
        return d->toolRoot;
    }        

    auto Measure2dControl::FlushToggle() -> void {
        d->ui.lineBtn->setStyleSheet("QPushButton{ border-top-left-radius: 6px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 6px; }");
        d->ui.pathBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.angleBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.circleBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.ellipseBtn->setStyleSheet("QPushButton{ border-radius: 0px; }");
        d->ui.polygonBtn->setStyleSheet("QPushButton{ border-top-left-radius: 0px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 0px; }");
    }

    auto Measure2dControl::OnLineToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->lineTool->Activate();
            d->ui.lineBtn->setStyleSheet("QPushButton{ border-top-left-radius: 6px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 6px; background-color: rgb(39, 162, 191); }");
        }
        else {
            d->lineTool->Deactivate();
        }
    }
    auto Measure2dControl::SetHandleBaseRadius(double base_rad) -> void {
        d->handleBaseSize = base_rad;
    }
    auto Measure2dControl::SetHandleRadius(float rad) -> void {
        if(d->lineTool) {
            d->lineTool->SetHandleSize(d->handleBaseSize*rad);
        }
        if(d->angleTool) {
            d->angleTool->SetHandleSize(d->handleBaseSize * rad);
        }
        if(d->multiLineTool) {
            d->multiLineTool->SetHandleSize(d->handleBaseSize * rad);
        }
        if(d->circleTool) {
            d->circleTool->SetHandleSize(d->handleBaseSize * rad);
        }
        if(d->ellipseTool) {
            d->ellipseTool->SetHandleSize(d->handleBaseSize * rad);
        }
        if(d->polygonTool) {
            d->polygonTool->SetHandleSize(d->handleBaseSize * rad);
        }
    }
    auto Measure2dControl::OnPathToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->multiLineTool->Activate();
            d->ui.pathBtn->setStyleSheet("QPushButton{ border-radius: 0px; background-color: rgb(39, 162, 191);}");
        }
        else {
            d->multiLineTool->Deactivate();
        }
    }
    auto Measure2dControl::OnAngleToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->angleTool->Activate();            
            d->ui.angleBtn->setStyleSheet("QPushButton{ border-radius: 0px; background-color: rgb(39, 162, 191);}");            
        }
        else {
            d->angleTool->Deactivate();
        }
    }
    auto Measure2dControl::OnCircleToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->circleTool->Activate();
            d->ui.circleBtn->setStyleSheet("QPushButton{ border-radius: 0px; background-color: rgb(39, 162, 191);}");            
        }
        else {
            d->circleTool->Deactivate();
        }
    }
    auto Measure2dControl::OnEllipseToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->ellipseTool->Activate();
            d->ui.ellipseBtn->setStyleSheet("QPushButton{ border-radius: 0px; background-color: rgb(39, 162, 191);}");            
        }
        else {
            d->ellipseTool->Deactivate();
        }
    }
    auto Measure2dControl::OnPolygonToggled(bool toggled) -> void {
        FlushToggle();
        if (toggled) {
            d->polygonTool->Activate();
            d->ui.polygonBtn->setStyleSheet("QPushButton{ border-top-left-radius: 0px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 0px; background-color: rgb(39, 162, 191);}");
        }
        else {
            d->polygonTool->Deactivate();
        }
    }

    auto Measure2dControl::AppendTableItem(QString text, MeasureType type) -> void {
        auto curRowCnt = d->ui.measureTable->rowCount();
        auto itemID = new QTableWidgetItem(QString(type._to_string()));
        itemID->setTextAlignment(Qt::AlignLeft);
        d->ui.measureTable->setRowCount(curRowCnt + 1);
        d->ui.measureTable->setItem(curRowCnt, 0, itemID);
        auto contents = new QTableWidgetItem(text);
        contents->setFlags(contents->flags() ^ Qt::ItemIsEditable);
        contents->setTextAlignment(Qt::AlignLeft);
        d->ui.measureTable->setItem(curRowCnt, 1, contents);

        d->ui.measureTable->resizeRowsToContents();

        d->tableMap[type].append(curRowCnt);                
    }
    auto Measure2dControl::DeactivateAll() -> void {
        d->lineTool->Deactivate();
        d->multiLineTool->Deactivate();
        d->angleTool->Deactivate();
        d->ellipseTool->Deactivate();
        d->circleTool->Deactivate();
        d->polygonTool->Deactivate();

        for (auto i = 0; i < 6; i++) {
            d->buttonGroup->buttons()[i]->setChecked(false);
        }
    }

    auto Measure2dControl::ModifyTableItem(int idx, MeasureType type, QString text) -> void {
        if(d->tableMap[type].count() <=idx) {
            return;
        }
        auto tableIdx = d->tableMap[type][idx];
        d->ui.measureTable->item(tableIdx, 1)->setText(text);
    }

    auto Measure2dControl::HighlightMeasureItem(int idx, MeasureType type) -> void {
        switch(type) {
            case MeasureType::Line:
                d->lineTool->HighlightItem(idx);
                break;
            case MeasureType::Path:
                d->multiLineTool->HighlightItem(idx);
                break;
            case MeasureType::Angle:
                d->angleTool->HighlightItem(idx);
                break;
            case MeasureType::Circle:
                d->circleTool->HighlightItem(idx);
                break;
            case MeasureType::Ellipsoid:
                d->ellipseTool->HighlightItem(idx);
                break;
            case MeasureType::Polygon:
                d->polygonTool->HighlightItem(idx);
                break;
            default:
                break;
        }
    }

    auto Measure2dControl::DeleteMeasureItem(int idx, MeasureType type) -> void {
        switch(type) {
            case MeasureType::Line:
                d->lineTool->DeleteItem(idx);
                break;
            case MeasureType::Path:
                d->multiLineTool->DeleteItem(idx);
                break;
            case MeasureType::Angle:
                d->angleTool->DeleteItem(idx);
                break;
            case MeasureType::Circle:
                d->circleTool->DeleteItem(idx);
                break;
            case MeasureType::Ellipsoid:
                d->ellipseTool->DeleteItem(idx);
                break;
            case MeasureType::Polygon:
                d->polygonTool->DeleteItem(idx);
                break;
            default:
                break;
        }
    }

    auto Measure2dControl::ClearMeasures() -> void {
        d->lineTool->Clear();
        d->multiLineTool->Clear();
        d->angleTool->Clear();
        d->circleTool->Clear();
        d->ellipseTool->Clear();
        d->polygonTool->Clear();
        d->ui.measureTable->clearContents();
        d->ui.measureTable->setRowCount(0);
        for(auto i=0;i<6;i++) {
            d->tableMap[i].clear();
        }
    }

    //slots
    void Measure2dControl::OnTableSelectionChanged() {
        auto numColumns = d->ui.measureTable->columnCount();
        d->lineTool->ClearHighlight();
        d->multiLineTool->ClearHighlight();
        d->angleTool->ClearHighlight();
        d->circleTool->ClearHighlight();
        d->ellipseTool->ClearHighlight();
        d->polygonTool->ClearHighlight();

        for(auto i=0;i<d->ui.measureTable->selectedItems().count();i+= numColumns) {
            auto row = d->ui.measureTable->selectedItems()[i]->row();
            MeasureType measureType = MeasureType::Line;
            for (auto j = 0; j < 6; j++) {
                if (d->tableMap[j].contains(row)) {
                    measureType = MeasureType::_from_index(j);
                    break;
                }
            }
            HighlightMeasureItem(d->tableMap[measureType].indexOf(row), measureType);
        }
    }
    void Measure2dControl::OnDeleteItems() {
        auto numColumns = d->ui.measureTable->columnCount();
        for (auto i = d->ui.measureTable->selectedItems().count() - 1; i >= 0; i -= numColumns) {
            auto row = d->ui.measureTable->selectedItems()[i]->row();
            d->ui.measureTable->removeRow(row);
            MeasureType measureType = MeasureType::Line;
            for(auto j=0;j<6;j++) {                
                if(d->tableMap[j].contains(row)) {                    
                    measureType = MeasureType::_from_index(j);                                        
                    break;
                }
            }
            DeleteMeasureItem(d->tableMap[measureType].indexOf(row), measureType);
                        
            for(auto j=0;j<6;j++) {
                auto tempType = MeasureType::_from_index(j);
                for(auto k=0;k<d->tableMap[j].count();k++) {                    
                    auto tableIdx = d->tableMap[tempType][k];
                    if(tableIdx > row) {
                        d->tableMap[tempType][k]--;
                    }
                }
            }

            auto tableIdxToRemove = -1;
            for (auto j = 0; j < d->tableMap[measureType].count(); j++) {
                auto tableIdx = d->tableMap[measureType][j];
                if(tableIdx == row) {
                    tableIdxToRemove = j;
                    break;
                }
            }
            if(tableIdxToRemove > -1) {
                d->tableMap[measureType].removeAt(tableIdxToRemove);
            }
        }
    }
    void Measure2dControl::OnSelectAll() {
        d->ui.measureTable->selectAll();
    }
    void Measure2dControl::OnDeselectAll() {
        d->ui.measureTable->clearSelection();
    }
    void Measure2dControl::OnExport() {
        auto path = qApp->applicationDirPath();
        const auto prevPath = QSettings("Tomocube", "TomoAnalysis").value("recentSave2dMeasure",path).toString();        
        QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save File"),
            prevPath,
            tr("Excel csv file (*.csv)"));

        if(fileName.isEmpty()) {
            return;
        }
        QSettings("Tomocube", "TomoAnalysis").setValue("recentSave2dMeasure", fileName);

        std::ofstream new_csv;
        new_csv.open(fileName.toStdString());
        new_csv << "Number,ID,Category,Measure";
        new_csv << "\n";

        for(auto i=0;i<d->ui.measureTable->rowCount();i++) {
            auto typeIdx = -1;
            for(auto j=0;j<6;j++) {
                if(d->tableMap[j].contains(i)) {
                    typeIdx = j;
                    break;
                }
            }
            if(typeIdx < 0) {
                continue;
            }

            auto measureIdx = d->tableMap[typeIdx].indexOf(i);

            if(typeIdx < 2) { //line, path
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "length (um),";
                new_csv << d->lineTool->GetMeasure()[measureIdx];
                new_csv << "\n";
            }else if(typeIdx == MeasureType::Path) {
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "length (um),";
                new_csv << d->multiLineTool->GetMeasure()[measureIdx];
                new_csv << "\n";
            }else if(typeIdx == MeasureType::Angle) {
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "angle (degree),";
                new_csv << d->angleTool->GetMeasure()[measureIdx];
                new_csv << "\n";
            }else if(typeIdx == MeasureType::Circle) {
                const auto measure = d->circleTool->GetMeasure()[measureIdx];
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "radius (um),";
                new_csv << measure.radius;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "circumference (um),";
                new_csv << measure.circumference;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "area (um^2),";
                new_csv << measure.area;
                new_csv << "\n";
            }else if(typeIdx == MeasureType::Ellipsoid) {
                const auto measure = d->ellipseTool->GetMeasure()[measureIdx];
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "major axis (um),";
                new_csv << measure.majorAxis;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "minor axis (um),";
                new_csv << measure.minorAxis;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "circumference (um),";
                new_csv << measure.circumference;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "area (um^2),";
                new_csv << measure.area;
                new_csv << "\n";                
            }else if(typeIdx == MeasureType::Polygon) {
                const auto measure = d->polygonTool->GetMeasure()[measureIdx];
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "circumference (um),";
                new_csv << measure.circumference;
                new_csv << "\n";
                new_csv << i + 1 << ",";
                new_csv << d->ui.measureTable->item(i, 0)->text().toStdString() << ",";
                new_csv << "area (um^2),";
                new_csv << measure.area;
                new_csv << "\n";
            }
        }
        new_csv.close();
    }

    void Measure2dControl::OnVizToggleClicked() {
        if(d->ui.vizToggle->isChecked()) {
            d->toolSwitch->whichChild = -3;            
            for (auto button : d->buttonGroup->buttons()) {                
                button->setEnabled(true);
            }
        }else {
            d->toolSwitch->whichChild = -1;
            for (auto button : d->buttonGroup->buttons()) {
                button->setChecked(false);
                button->setEnabled(false);
            }
        }
    }
    void Measure2dControl::OnButtonGroupToggled(int id, bool toggled) {
        if(toggled) {
            for (auto i = 0; i < 6; i++) {
                if(id != i) {
                    d->buttonGroup->buttons()[i]->setChecked(false);
                }
            }
            emit sigActivateMeasure();
        }
        switch(id) {
            case 0:
                OnLineToggled(toggled);
                break;
            case 1:
                OnPathToggled(toggled);
                break;
            case 2:
                OnAngleToggled(toggled);
                break;
            case 3:
                OnCircleToggled(toggled);
                break;
            case 4:
                OnEllipseToggled(toggled);
                break;
            case 5:
                OnPolygonToggled(toggled);
                break;
            default:
                break;
        }
    }    
    void Measure2dControl::OnLineFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Line);        
    }
    void Measure2dControl::OnLineModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Line, measureTxt);
    }
    void Measure2dControl::OnPathFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Path);
    }
    void Measure2dControl::OnPathModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Path, measureTxt);
    }
    void Measure2dControl::OnAngleFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Angle);
    }
    void Measure2dControl::OnAngleModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Angle, measureTxt);
    }
    void Measure2dControl::OnCircleFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Circle);
    }
    void Measure2dControl::OnCircleModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Circle, measureTxt);
    }
    void Measure2dControl::OnEllipseFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Ellipsoid);
    }
    void Measure2dControl::OnEllipseModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Ellipsoid, measureTxt);
    }
    void Measure2dControl::OnPolygonFinish(QString measureTxt) {
        AppendTableItem(measureTxt, MeasureType::Polygon);
    }
    void Measure2dControl::OnPolygonModified(int idx, QString measureTxt) {
        ModifyTableItem(idx, MeasureType::Polygon, measureTxt);
    }

}
