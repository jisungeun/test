#pragma warning(push)
#pragma warning(disable:4819)

#include "OivMultiLineDrawer.h"
#include "PolygonMeasureSep.h"

#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

namespace TC {
	struct PolygonMeasureSep::Impl {
		SoSeparator* root{ nullptr };
		SoSeparator* handleRoot{ nullptr };
		OivMultiLineDrawer* line{ nullptr };
		SoEventCallback* callback{ nullptr };
		SoRef<SoVertexProperty> circleProp{ nullptr };

		PolygonMeasureSep* instance = nullptr;

		//container
		double handleRadius{ 0.5 };
		bool isInHandle{ false };
		bool inProcess{ false };
		int lineIdx = -1;
		int startIdx = -1;
		int countIdx = -1;
		QList<QList<SbVec3f>> linePoints[2];
		QList<QList<SoMaterial*>> handleMatl[2];
		QList<QList<SoTranslation*>> lineTrans[2];
		QList<QList<SoVertexProperty*>> lineProp;
		QList<QList<SoMaterial*>> lineMatl;
		QList<SoText2*> labelText;
		QList<SoTranslation*> labelPos;

		QList<polygonMeasure> measure;
		QList<std::vector<SbVec3f>> linePtStream;

		bool isActivated{ false };

		auto findLength(std::vector<SbVec3f> inputVertices)->double;
		auto findArea(std::vector<SbVec3f> inputVertices)->double;
		auto isScrewed(std::vector<SbVec3f> inputVertices)->bool;
		auto onSegment(SbVec3f p, SbVec3f q, SbVec3f r)->bool;
		auto orientation(SbVec3f p, SbVec3f q, SbVec3f r)->int;
		auto isIntersect(SbVec3f p1, SbVec3f q1, SbVec3f p2, SbVec3f q2)->bool;
	};
	auto PolygonMeasureSep::Impl::isIntersect(SbVec3f p1, SbVec3f q1, SbVec3f p2, SbVec3f q2) -> bool {
		int o1 = orientation(p1, q1, p2);
		int o2 = orientation(p1, q1, q2);
		int o3 = orientation(p2, q2, p1);
		int o4 = orientation(p2, q2, q1);

		// General case
		if (o1 != o2 && o3 != o4) {
			return true;
		}

		// Special Cases
		// p1, q1 and p2 are collinear and p2 lies on segment p1q1
		if (o1 == 0 && onSegment(p1, p2, q1)) return true;

		// p1, q1 and q2 are collinear and q2 lies on segment p1q1
		if (o2 == 0 && onSegment(p1, q2, q1)) return true;

		// p2, q2 and p1 are collinear and p1 lies on segment p2q2
		if (o3 == 0 && onSegment(p2, p1, q2)) return true;

		// p2, q2 and q1 are collinear and q1 lies on segment p2q2
		if (o4 == 0 && onSegment(p2, q1, q2)) return true;

		return false; // Doesn't fall in any of the above cases
	}
	auto PolygonMeasureSep::Impl::orientation(SbVec3f p, SbVec3f q, SbVec3f r) -> int {
		int val = (q[1] - p[1]) * (r[0] - q[0]) -
			(q[0] - p[0]) * (r[1] - q[1]);

		if (val == 0) {
			return 0;  // collinear
		}

		return (val > 0) ? 1 : 2; // clock or counterclock wise
	}
	auto PolygonMeasureSep::Impl::onSegment(SbVec3f p, SbVec3f q, SbVec3f r) -> bool {
		if (q[0] <= std::max(p[0], r[0]) && q[0] >= std::min(p[0], r[0]) &&
			q[1] <= std::max(p[1], r[1]) && q[1] >= std::min(p[1], r[1])) {
			return true;
		}

		return false;
	}
	auto PolygonMeasureSep::Impl::isScrewed(std::vector<SbVec3f> inputVertices) -> bool {
		if (inputVertices.size() < 3) {
			return false;
		}
		for (auto i = 0; i < inputVertices.size() - 2; i++) {
			auto i0 = i;
			auto i1 = (i + 1) % inputVertices.size();
			for (auto j = i1 + 1; j < inputVertices.size(); j++) {
				auto j1 = (j + 1) % inputVertices.size();
				if (j1 == i0) {
					continue;
				}
				if (isIntersect(inputVertices[i0], inputVertices[i1], inputVertices[j], inputVertices[j1])) {
					return true;
				}
			}
		}
		return false;
	}
	auto PolygonMeasureSep::Impl::findArea(std::vector<SbVec3f> inputVertices) -> double {
		double area = 0.0;
		int j = inputVertices.size() - 1;
		for (auto i = 0; i < inputVertices.size(); i++) {
			area += (inputVertices[j][0] + inputVertices[i][0]) * (inputVertices[j][1] - inputVertices[i][1]);
			j = i;
		}
		return abs(area);
	}
	auto PolygonMeasureSep::Impl::findLength(std::vector<SbVec3f> inputVertices) -> double {
		double length = 0.0;
		for (auto i = 0; i < inputVertices.size(); i++) {
			length += sqrt(pow(inputVertices[i][0] - inputVertices[(i + 1) % 2][0], 2) + pow(inputVertices[i][1] - inputVertices[(i + 1) % 2][1], 2));
		}
		return length;
	}

	PolygonMeasureSep::PolygonMeasureSep(QObject* parent) : QObject(parent), d{ new Impl } {
		Init();
		d->instance = this;
	}
	PolygonMeasureSep::~PolygonMeasureSep() {

	}
	auto PolygonMeasureSep::SetHandleSize(double radius) -> void {
		d->handleRadius = radius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}
		d->circleProp->touch();
    }
	void PolygonMeasureSep::Activate() {
		d->line->Activate();
		d->isActivated = true;
	}

	void PolygonMeasureSep::Deactivate() {
		d->line->Deactivate();
		d->isActivated = false;
	}
	auto PolygonMeasureSep::Init()->void {
		d->line = new OivMultiLineDrawer;
		d->root = new SoSeparator;
		d->handleRoot = new SoSeparator;
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->root->addChild(d->line);
		d->root->addChild(d->handleRoot);

		d->line->setUserData(d.get());
		d->line->onFinish.add(lineCallback);

		d->circleProp = new SoVertexProperty;
		auto radius = d->handleRadius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}

		d->callback = new SoEventCallback;
		d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), LineButtonCB, d.get());
		d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), LineMoveCB, d.get());
		d->root->insertChild(d->callback, 0);
	}

	auto PolygonMeasureSep::ClearHighlight() -> void {
        for(auto matlList : d->lineMatl) {
            for(auto matl: matlList) {
				matl->ambientColor.setValue(1, 1, 0);
				matl->diffuseColor.setValue(1, 1, 0);
            }
        }
    }

	auto PolygonMeasureSep::HighlightItem(int idx) -> bool {
		if (d->lineProp.count() <= idx) {
			return false;
		}
		if (d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		for(auto matl: d->lineMatl[idx]) {
			matl->ambientColor.setValue(0, 1, 0);
			matl->diffuseColor.setValue(0, 1, 0);
		}
		return true;
    }

	auto PolygonMeasureSep::DeleteItem(int idx) -> bool {
        if(d->lineProp.count() <= idx) {
			return false;
        }
		if(d->handleRoot->getNumChildren()<idx) {
			return false;
		}
		d->handleRoot->removeChild(idx+1);
		for(auto i=0;i<2;i++) {
			d->linePoints[i].removeAt(idx);
			d->handleMatl[i].removeAt(idx);
			d->lineTrans[i].removeAt(idx);
		}
		d->lineProp.removeAt(idx);
		d->labelText.removeAt(idx);
		d->labelPos.removeAt(idx);
		d->measure.removeAt(idx);
		d->linePtStream.removeAt(idx);
		d->lineMatl.removeAt(idx);

		return true;
    }

	auto PolygonMeasureSep::Clear()->void {
		d->handleRoot->removeAllChildren();
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->linePoints[0].clear();
		d->linePoints[1].clear();
		d->handleMatl[0].clear();
		d->handleMatl[1].clear();
		d->lineTrans[0].clear();
		d->lineTrans[1].clear();
		d->lineProp.clear();
		d->labelText.clear();
		d->labelPos.clear();
		d->measure.clear();
		d->linePtStream.clear();
		d->lineMatl.clear();
	}
	void PolygonMeasureSep::Finish(QString text) {
		emit sigFinish(text);
	}
	void PolygonMeasureSep::Update(int index, QString text) {
		emit sigUpdate(index, text);
	}
	auto PolygonMeasureSep::GetMeasure() -> QList<polygonMeasure> {
		return d->measure;
    }
	auto PolygonMeasureSep::GetRoot()->SoSeparator* {
		return d->root;
	}
	auto PolygonMeasureSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
		std::vector<SbVec3f> result;

		SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
		for (int i = 0; i < source->getNum(); i++) {

			SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
			normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
			normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

			rayPick.setNormalizedPoint(normalizedPoint);
			rayPick.apply(targetNode);

			SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
			if (pickedPoint) {
				result.push_back(pickedPoint->getPoint());
			}
		}

		return result;
	}
	void PolygonMeasureSep::lineCallback(SoPolyLineScreenDrawer::EventArg& arg) {
		OivMultiLineDrawer* source = (OivMultiLineDrawer*)arg.getSource();
		SoHandleEventAction* action = arg.getAction();
		auto dd = static_cast<Impl*>(source->getUserData());
		if (source->point.getNum() < 3) {
			source->clear();
			return;
		}

		auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());
		if (resultRayCast.size() < 3) {
			source->clear();
			return;
		}

		polygonMeasure newMeasure;

		if (dd->isScrewed(resultRayCast)) {
			newMeasure.area = -1;
		}
		else {
			auto area = dd->findArea(resultRayCast);
			newMeasure.area = area;
		}
		auto length = dd->findLength(resultRayCast);
		newMeasure.circumference = length;

		dd->measure.append(newMeasure);

		dd->linePtStream.push_back(resultRayCast);

		dd->lineProp.append(QList<SoVertexProperty*>());
		dd->labelPos.append(QList<SoTranslation*>());
		dd->labelText.append(QList<SoText2*>());
		dd->linePoints[0].append(QList<SbVec3f>());
		dd->linePoints[1].append(QList<SbVec3f>());
		dd->lineTrans[0].append(QList<SoTranslation*>());
		dd->lineTrans[1].append(QList<SoTranslation*>());
		dd->handleMatl[0].append(QList<SoMaterial*>());
		dd->handleMatl[1].append(QList<SoMaterial*>());
		dd->lineMatl.append(QList<SoMaterial*>());
		QString label_text;

		SoRef<SoSeparator> handleGroup = new SoSeparator;

		for (auto i = 0; i < resultRayCast.size(); ++i) {
			auto index1 = i;
			auto index2 = i + 1;

			if (i == resultRayCast.size() - 1) {
				index1 = i;
				index2 = 0;
			}

			auto xDiff = resultRayCast[index2][0] - resultRayCast[index1][0];
			auto yDiff = resultRayCast[index2][1] - resultRayCast[index1][1];

			//Add line
			SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
			drawStyle->setName("drawStyle");
			drawStyle->lineWidth = 2;
			SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
			vertexProp->vertex.set1Value(0, resultRayCast[index1]);
			vertexProp->vertex.set1Value(1, resultRayCast[index2]);

			dd->lineProp[dd->lineProp.count() - 1].append(vertexProp.ptr());

			SoRef<SoLineSet> lineSet = new SoLineSet;
			lineSet->setName("myline");
			lineSet->vertexProperty = vertexProp.ptr();

			dd->linePoints[0][dd->linePoints[0].count() - 1].append(resultRayCast[index1]);
			dd->linePoints[1][dd->linePoints[0].count() - 1].append(resultRayCast[index2]);

			SoRef<SoMaterial> lineMatl = new SoMaterial;
			lineMatl->ambientColor.setValue(1, 1, 0);
			lineMatl->diffuseColor.setValue(1, 1, 0);
			dd->lineMatl[dd->lineMatl.count() - 1].append(lineMatl.ptr());

			SoRef<SoSeparator> lineSep = new SoSeparator;
			lineSep->addChild(drawStyle.ptr());
			lineSep->addChild(lineMatl.ptr());
			lineSep->addChild(lineSet.ptr());
						
			//Add text
			if (i == 0) {
				SoRef<SoSeparator> textSep = new SoSeparator;
				textSep->setName("textSep");

				SoRef<SoTranslation> labelPos = new SoTranslation();
				labelPos->translation.setValue(((resultRayCast[index1][0] + resultRayCast[index2][0]) / 2) - 2.0,
					((resultRayCast[index1][1] + resultRayCast[index2][1]) / 2) + 0.5,
					(resultRayCast[index1][2] + resultRayCast[index2][2]) / 2);

				textSep->addChild(labelPos.ptr());
				dd->labelPos.append(labelPos.ptr());

				SoRef<SoFont> labelFont = new SoFont();
				labelFont->size = 13.0;
				labelFont->name = "Arial:Bold";
				labelFont->renderStyle = SoFont::TEXTURE;
				textSep->addChild(labelFont.ptr());

				SoTextProperty* textProp = new SoTextProperty();
				textProp->alignmentH = SoTextProperty::LEFT;
				textProp->alignmentV = SoTextProperty::TOP;
				textSep->addChild(textProp);

				SoRef<SoText2> labelText = new SoText2();
				labelText->justification = SoText2::INHERITED;
				textSep->addChild(labelText.ptr());
				dd->labelText.append(labelText.ptr());

				SoRef<SoMaterial> fontMat = new SoMaterial;
				fontMat->ambientColor.setValue(1, 1, 0);
				fontMat->diffuseColor.setValue(1, 1, 0);
				textSep->addChild(fontMat.ptr());

				QString volumeText;
				if (dd->measure.last().area < 0) {
					volumeText = "N/A";
				}
				else {
					volumeText = QString::number(dd->measure.last().area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");
				}
				label_text = QString("Area: %1 \n").arg(volumeText);
				label_text.append(QString("Circumference: %1").arg(QString::number(dd->measure.last().circumference, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m"));
				labelText->string = volumeText.toStdWString();

				handleGroup->addChild(textSep.ptr());
			}

			//Add handles			
			SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
			circleStyle->setName("circleStyle");
			circleStyle->style = SoDrawStyle::FILLED;

			SoRef<SoFaceSet> faceSet = new SoFaceSet;
			faceSet->vertexProperty = dd->circleProp.ptr();

			SoRef<SoSeparator> circleSep = new SoSeparator;
			circleSep->addChild(circleStyle.ptr());

			SoRef<SoSeparator> circle1 = new SoSeparator;
			circleSep->addChild(circle1.ptr());
			SoRef<SoMaterial> circleMatl1 = new SoMaterial;
			circleMatl1->ambientColor.setValue(1, 0, 0);
			circleMatl1->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans1 = new SoTranslation;
			circleTrans1->translation.setValue(resultRayCast[index1]);
			circle1->addChild(circleMatl1.ptr());
			circle1->addChild(circleTrans1.ptr());
			circle1->addChild(faceSet.ptr());

			SoRef<SoSeparator> circle2 = new SoSeparator;
			circleSep->addChild(circle2.ptr());
			SoRef<SoMaterial> circleMatl2 = new SoMaterial;
			circleMatl2->ambientColor.setValue(1, 0, 0);
			circleMatl2->diffuseColor.setValue(1, 0, 0);
			SoRef<SoTranslation> circleTrans2 = new SoTranslation;
			circleTrans2->translation.setValue(resultRayCast[index2]);
			circle2->addChild(circleMatl2.ptr());
			circle2->addChild(circleTrans2.ptr());
			circle2->addChild(faceSet.ptr());

			dd->lineTrans[0][dd->lineTrans[0].count() - 1].append(circleTrans1.ptr());
			dd->lineTrans[1][dd->lineTrans[1].count() - 1].append(circleTrans2.ptr());

			dd->handleMatl[0][dd->handleMatl[0].count() - 1].append(circleMatl1.ptr());
			dd->handleMatl[1][dd->handleMatl[1].count() - 1].append(circleMatl2.ptr());

			handleGroup->addChild(circleSep.ptr());
			handleGroup->addChild(lineSep.ptr());
		}

		dd->handleRoot->addChild(handleGroup.ptr());
		dd->inProcess = false;
		dd->instance->Finish(label_text);
	}
	void PolygonMeasureSep::LineMoveCB(void* pImpl, SoEventCallback* eventCB) {
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		if (dd->isInHandle) {
			//consume event
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback				
				eventCB->releaseEvents();
				return;
			}
			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->translation.setValue(p->getPoint());
			dd->lineProp[dd->lineIdx][dd->countIdx]->vertex.set1Value(dd->startIdx, p->getPoint());

			dd->lineTrans[dd->startIdx][dd->lineIdx][dd->countIdx]->touch();

			if (dd->countIdx > 0 && dd->startIdx == 0) {
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx - 1]->vertex.set1Value(1, p->getPoint());
				dd->lineTrans[1][dd->lineIdx][dd->countIdx - 1]->translation.touch();
			}
			if (dd->countIdx + 1 < dd->lineProp.count() && dd->startIdx == 1) {
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][dd->countIdx + 1]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[0][dd->lineIdx][dd->countIdx + 1]->translation.touch();
			}

			if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
				dd->lineTrans[0][dd->lineIdx][0]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][0]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[0][dd->lineIdx][0]->translation.touch();
			}
			else if (dd->startIdx == 0 && dd->countIdx == 0) {
				auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;

				dd->lineTrans[1][dd->lineIdx][count]->translation.setValue(p->getPoint());
				dd->lineProp[dd->lineIdx][count]->vertex.set1Value(0, p->getPoint());
				dd->lineTrans[1][dd->lineIdx][count]->translation.touch();
			}

			eventCB->setHandled();
			return;
		}
		eventCB->releaseEvents();
	}
	void PolygonMeasureSep::LineButtonCB(void* pImpl, SoEventCallback* eventCB) {
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		const SoEvent* event = eventCB->getEvent();
		const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			//check whether handle is under click or not
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback	
				eventCB->releaseEvents();
				return;
			}
			auto point = p->getPoint();
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
			if (false == dd->inProcess) {
				for (auto i = 0; i < dd->lineProp.count(); i++) {//travel paths
					for (auto k = 0; k < dd->lineProp[i].count(); k++) {//travel lines in path
						for (auto j = 0; j < 2; j++) {//travel start or end point
							if (false == doubleComp(dd->linePoints[j][i][k][2], point[2])) {
								continue;
							}
							auto dist = sqrt(pow(dd->linePoints[j][i][k][0] - point[0], 2) + pow(dd->linePoints[j][i][k][1] - point[1], 2));
							if (dist > dd->handleRadius) {
								continue;
							}
							dd->isInHandle = true;
							dd->lineIdx = i;
							dd->startIdx = j;
							dd->countIdx = k;
							break;
						}
					}
					if (dd->isInHandle) {
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 1, 0);
						dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 1, 0);

						if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
							dd->handleMatl[0][dd->lineIdx][0]->ambientColor.setValue(1, 1, 0);
							dd->handleMatl[0][dd->lineIdx][0]->diffuseColor.setValue(1, 1, 0);
						}
						else if (dd->startIdx == 0 && dd->countIdx == 0) {
							auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;
							dd->handleMatl[1][dd->lineIdx][count]->ambientColor.setValue(1, 1, 0);
							dd->handleMatl[1][dd->lineIdx][count]->diffuseColor.setValue(1, 1, 0);
						}

						break;
					}
				}
			}
			dd->inProcess = !dd->isInHandle;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (dd->isInHandle) {
				SoHandleEventAction* action = eventCB->getAction();
				const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

				SoRayPickAction pickaction = SoRayPickAction(myRegion);
				pickaction.setPoint(event->getPosition());
				pickaction.setSceneManager(action->getSceneManager());
				pickaction.apply(action->getPickRoot());
				auto p = pickaction.getPickedPoint();
				dd->linePoints[dd->startIdx][dd->lineIdx][dd->countIdx] = p->getPoint();

				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->ambientColor.setValue(1, 0, 0);
				dd->handleMatl[dd->startIdx][dd->lineIdx][dd->countIdx]->diffuseColor.setValue(1, 0, 0);

				if (dd->startIdx == 1 && dd->countIdx == dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1) {
					dd->handleMatl[0][dd->lineIdx][0]->ambientColor.setValue(1, 0, 0);
					dd->handleMatl[0][dd->lineIdx][0]->diffuseColor.setValue(1, 0, 0);
				}
				else if (dd->startIdx == 0 && dd->countIdx == 0) {
					auto count = dd->handleMatl[dd->startIdx][dd->lineIdx].size() - 1;
					dd->handleMatl[1][dd->lineIdx][count]->ambientColor.setValue(1, 0, 0);
					dd->handleMatl[1][dd->lineIdx][count]->diffuseColor.setValue(1, 0, 0);
				}

				//recompute line length
				auto p1 = dd->linePoints[(dd->startIdx + 1) % 2][dd->lineIdx][dd->countIdx];
				auto p2 = dd->linePoints[dd->startIdx][dd->lineIdx][dd->countIdx];
				auto xDiff = p1[0] - p2[0];
				auto yDiff = p1[1] - p2[1];

				dd->labelPos[dd->lineIdx]->translation.setValue((p1[0] + p2[0]) / 2 - 2.0,
					(p1[1] + p2[1]) / 2 + 0.5,
					(p1[2] + p2[2]) / 2);

				if (dd->countIdx > 0 && dd->startIdx == 0) {
					dd->linePoints[1][dd->lineIdx][dd->countIdx - 1] = p->getPoint();

					auto pp1 = dd->linePoints[0][dd->lineIdx][dd->countIdx - 1];
					auto pp2 = dd->linePoints[1][dd->lineIdx][dd->countIdx - 1];

					auto xxDiff = pp1[0] - pp2[0];
					auto yyDiff = pp1[1] - pp2[1];

					dd->labelPos[dd->lineIdx]->translation.setValue((pp1[0] + pp2[0]) / 2 - 2.0,
						(pp1[1] + pp2[1]) / 2 + 0.5,
						(pp1[2] + pp2[2]) / 2);
				}
				if (dd->countIdx + 1 < dd->lineProp.count() && dd->startIdx == 1) {
					dd->linePoints[0][dd->lineIdx][dd->countIdx + 1] = p->getPoint();

					auto pp1 = dd->linePoints[0][dd->lineIdx][dd->countIdx + 1];
					auto pp2 = dd->linePoints[1][dd->lineIdx][dd->countIdx + 1];

					auto xxDiff = pp1[0] - pp2[0];
					auto yyDiff = pp1[1] - pp2[1];

					dd->labelPos[dd->lineIdx]->translation.setValue((pp1[0] + pp2[0]) / 2 - 2.0,
						(pp1[1] + pp2[1]) / 2 + 0.5,
						(pp1[2] + pp2[2]) / 2);

				}
				auto streamSize = dd->linePtStream[dd->lineIdx].size();
				dd->linePtStream[dd->lineIdx][(dd->countIdx + dd->startIdx) % streamSize] = p->getPoint();
				if (dd->isScrewed(dd->linePtStream[dd->lineIdx])) {
					dd->measure[dd->lineIdx].area = -1;
				}
				else {
					auto volume = dd->findArea(dd->linePtStream[dd->lineIdx]);
					dd->measure[dd->lineIdx].area = volume;
				}
				auto length = dd->findLength(dd->linePtStream[dd->lineIdx]);
				dd->measure[dd->lineIdx].circumference = length;

				QString volumeText;
				if (dd->measure[dd->lineIdx].area < 0) {
					volumeText = "N/A";
				}
				else {
					volumeText = QString::number(dd->measure[dd->lineIdx].area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m" + QString::fromWCharArray(L"\x00B2");
				}
				QString label_text = QString("Area : %1 \n").arg(volumeText);
				label_text += QString("Circumference: %1").arg(QString::number(dd->measure[dd->lineIdx].area, 'f', 2) + " " + QString::fromWCharArray(L"\x00B5") + "m");

				dd->labelText[dd->lineIdx]->string = volumeText.toStdWString();

				dd->instance->Update(dd->lineIdx, label_text);
			}
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
		}

		if (dd->isInHandle) {
			//consume event
			eventCB->setHandled();
			return;
		}

		SoHandleEventAction* action2 = eventCB->getAction();
		const SbViewportRegion& myRegion2 = eventCB->getAction()->getViewportRegion();

		SoRayPickAction pickaction2 = SoRayPickAction(myRegion2);
		pickaction2.setPoint(event->getPosition());
		pickaction2.setSceneManager(action2->getSceneManager());
		pickaction2.apply(action2->getPickRoot());
		auto p = pickaction2.getPickedPoint();
		if (p == NULL) {
			dd->line->Reset();
		}

		auto pos = mouseButton->getPositionFloat();
		auto viewport_size = myRegion2.getViewportSizePixels();
		auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
		if (mouse_in_setting) {
			dd->line->Reset();
		}

		eventCB->releaseEvents();
	}
}