#pragma warning(push)
#pragma warning(disable:4819)

#include "OivAngleDrawer.h"
#include "AngleMeasureSep.h"

#include <Inventor/SoDB.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoTextProperty.h>
#pragma warning(pop)

namespace TC {
	struct AngleMeasureSep::Impl {
		SoSeparator* root{ nullptr };
		SoSeparator* handleRoot{ nullptr };
		OivAngleDrawer* line{ nullptr };
		SoEventCallback* callback{ nullptr };
		SoRef<SoVertexProperty> circleProp{ nullptr };

		//container
		double handleRadius{ 0.5 };
		bool isInHandle{ false };
		bool inProgress{ false };
		bool isMouseDown{ false };
		int lineIdx = -1;
		int startIdx = -1;
		QList<SbVec3f> linePoints[3];
		QList<SoMaterial*> handleMatl[3];
		QList<SoTranslation*> lineTrans[3];
		QList<SoVertexProperty*> lineProp;
		QList<SoText2*> labelText;
		QList<SoTranslation*> labelPos;
		QList<SoMaterial*> lineMatl;
		QList<float> angles;

		bool isActivated{ false };

		AngleMeasureSep* instance = nullptr;				
	};
	AngleMeasureSep::AngleMeasureSep(QObject* parent) : QObject(parent), d{ new Impl } {
		Init();
		d->instance = this;
	}
	AngleMeasureSep::~AngleMeasureSep() {

	}
	void AngleMeasureSep::Activate() {
		d->line->Activate();
		d->isActivated = true;
	}

	void AngleMeasureSep::Deactivate() {
		d->line->Deactivate();
		d->isActivated = false;
	}
	void AngleMeasureSep::Finish(QString text) {
		emit sigFinish(text);
	}
	void AngleMeasureSep::Update(int index, QString text) {
		emit sigUpdate(index, text);
	}
	auto AngleMeasureSep::Init()->void {
		d->line = new OivAngleDrawer;
		d->root = new SoSeparator;
		d->handleRoot = new SoSeparator;
		SoRef<SoLightModel> lightmodel = new SoLightModel;
		lightmodel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightmodel.ptr());

		d->root->addChild(d->line);
		d->root->addChild(d->handleRoot);

		d->line->setUserData(d.get());
		d->line->onFinish.add(lineCallback);

		d->circleProp = new SoVertexProperty;
		auto radius = d->handleRadius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}

		d->callback = new SoEventCallback;
		d->callback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), LineButtonCB, d.get());
		d->callback->addEventCallback(SoLocation2Event::getClassTypeId(), LineMoveCB, d.get());
		d->root->insertChild(d->callback, 0);
	}
	auto AngleMeasureSep::GetMeasure()->QList<float> {
		return d->angles;
	}
	auto AngleMeasureSep::Clear()->void {
		d->handleRoot->removeAllChildren();
		SoRef<SoLightModel> lightModel = new SoLightModel;
		lightModel->model = SoLightModel::BASE_COLOR;
		d->handleRoot->addChild(lightModel.ptr());
		d->linePoints[0].clear();
		d->linePoints[1].clear();
		d->linePoints[2].clear();
		d->handleMatl[0].clear();
		d->handleMatl[1].clear();
		d->handleMatl[2].clear();
		d->lineTrans[0].clear();
		d->lineTrans[1].clear();
		d->lineTrans[2].clear();
		d->lineProp.clear();
		d->labelText.clear();
		d->labelPos.clear();
		d->angles.clear();
		d->lineMatl.clear();
	}
	auto AngleMeasureSep::ClearHighlight() -> void {
        for(auto matl:d->lineMatl) {
			matl->ambientColor.setValue(1, 1, 0);
			matl->diffuseColor.setValue(1, 1, 0);
        }
    }
	auto AngleMeasureSep::HighlightItem(int idx) -> bool {
		if (d->lineProp.count() <= idx) {
			return false;
		}
		if (d->handleRoot->getNumChildren() < idx) {
			return false;
		}
		d->lineMatl[idx]->ambientColor.setValue(0, 1, 0);
		d->lineMatl[idx]->diffuseColor.setValue(0, 1, 0);
		return true;
    }
	auto AngleMeasureSep::DeleteItem(int idx) -> bool {
        if(d->lineProp.count() <= idx) {
			return false;
        }
		if(d->handleRoot->getNumChildren() < idx) {
			return false;
		}

		d->handleRoot->removeChild(idx + 1);
		for(auto i=0;i<3;i++) {
			d->linePoints[i].removeAt(idx);
			d->handleMatl[i].removeAt(idx);
			d->lineTrans[i].removeAt(idx);
		}
		d->lineProp.removeAt(idx);
		d->labelText.removeAt(idx);
		d->labelPos.removeAt(idx);
		d->angles.removeAt(idx);
		d->lineMatl.removeAt(idx);

		return true;
    }
	auto AngleMeasureSep::GetRoot()->SoSeparator* {
		return d->root;
	}
	auto AngleMeasureSep::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion)->std::vector<SbVec3f> {
		std::vector<SbVec3f> result;

		SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
		for (int i = 0; i < source->getNum(); i++) {

			SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
			normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
			normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

			rayPick.setNormalizedPoint(normalizedPoint);
			rayPick.apply(targetNode);

			SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
			if (pickedPoint) {
				result.push_back(pickedPoint->getPoint());
			}
		}

		return result;
	}
	auto AngleMeasureSep::SetHandleSize(double radius) -> void {
		d->handleRadius = radius;
		auto div = 100.0;
		for (auto i = 0; i < 100; i++) {
			auto x_pos = radius * cos(2 * M_PI / div * i);
			auto y_pos = radius * sin(2 * M_PI / div * i);
			d->circleProp->vertex.set1Value(i, x_pos, y_pos, 0);
		}
		d->circleProp->touch();
    }
	void AngleMeasureSep::lineCallback(SoPolyLineScreenDrawer::EventArg& arg) {
		OivAngleDrawer* source = (OivAngleDrawer*)arg.getSource();
		SoHandleEventAction* action = arg.getAction();
		auto dd = static_cast<Impl*>(source->getUserData());

		auto resultRayCast = CalcRayCasting(&source->point, action->getPickRoot(), action->getViewportRegion());

		auto aa = sqrt(pow(resultRayCast[0][0] - resultRayCast[2][0], 2) + pow(resultRayCast[0][1] - resultRayCast[2][1], 2));
		auto bb = sqrt(pow(resultRayCast[0][0] - resultRayCast[1][0], 2) + pow(resultRayCast[0][1] - resultRayCast[1][1], 2));
		auto cc = sqrt(pow(resultRayCast[1][0] - resultRayCast[2][0], 2) + pow(resultRayCast[1][1] - resultRayCast[2][1], 2));

		auto temp = (pow(bb, 2) + pow(cc, 2) - pow(aa, 2)) / (2 * bb * cc);
		auto ang = acos(temp);
		ang = ang * (180 / M_PI);

		//Add line
		SoRef<SoDrawStyle> drawStyle = new SoDrawStyle;
		drawStyle->setName("drawStyle");
		drawStyle->lineWidth = 2;
		SoRef<SoVertexProperty> vertexProp = new SoVertexProperty;
		vertexProp->vertex.set1Value(0, resultRayCast[0]);
		vertexProp->vertex.set1Value(1, resultRayCast[1]);
		vertexProp->vertex.set1Value(2, resultRayCast[2]);

		dd->lineProp.append(vertexProp.ptr());

		SoRef<SoLineSet> lineSet = new SoLineSet;
		lineSet->setName("myline");
		lineSet->vertexProperty = vertexProp.ptr();

		dd->linePoints[0].append(resultRayCast[0]);
		dd->linePoints[1].append(resultRayCast[1]);
		dd->linePoints[2].append(resultRayCast[2]);

		SoRef<SoMaterial> lineMatl = new SoMaterial;
		lineMatl->ambientColor.setValue(1, 1, 0);
		lineMatl->diffuseColor.setValue(1, 1, 0);

		dd->lineMatl.append(lineMatl.ptr());

		SoRef<SoSeparator> lineSep = new SoSeparator;
		lineSep->addChild(drawStyle.ptr());
		lineSep->addChild(lineMatl.ptr());
		lineSep->addChild(lineSet.ptr());

		//Add text
		SoRef<SoSeparator> textSep = new SoSeparator;
		textSep->setName("textSep");

		SoRef<SoTranslation> labelPos = new SoTranslation();
		labelPos->translation.setValue(((resultRayCast[0][0] + resultRayCast[2][0]) / 2),
			((resultRayCast[0][1] + resultRayCast[2][1]) / 2),
			(resultRayCast[0][2] + resultRayCast[2][2]) / 2);

		textSep->addChild(labelPos.ptr());
		dd->labelPos.append(labelPos.ptr());

		SoRef<SoFont> labelFont = new SoFont();
		labelFont->size = 13.0;
		labelFont->name = "Arial:Bold";
		labelFont->renderStyle = SoFont::TEXTURE;
		textSep->addChild(labelFont.ptr());

		SoTextProperty* textProp = new SoTextProperty();
		textProp->alignmentH = SoTextProperty::LEFT;
		textProp->alignmentV = SoTextProperty::TOP;
		textSep->addChild(textProp);

		SoRef<SoText2> labelText = new SoText2();
		labelText->justification = SoText2::INHERITED;
		textSep->addChild(labelText.ptr());
		dd->labelText.append(labelText.ptr());

		SoRef<SoMaterial> fontMat = new SoMaterial;
		fontMat->ambientColor.setValue(1, 1, 0);
		fontMat->diffuseColor.setValue(1, 1, 0);
		textSep->addChild(fontMat.ptr());

		dd->angles.append(ang);
		QString label_text = QString::number(ang, 'f', 2) + " " + QString::fromWCharArray(L"\x00B0");
		labelText->string = label_text.toStdWString();

		//Add handles		
		SoRef<SoDrawStyle> circleStyle = new SoDrawStyle;
		circleStyle->setName("circleStyle");
		circleStyle->style = SoDrawStyle::FILLED;

		SoRef<SoFaceSet> faceSet = new SoFaceSet;
		faceSet->vertexProperty = dd->circleProp.ptr();

		SoRef<SoSeparator> circleSep = new SoSeparator;
		circleSep->addChild(circleStyle.ptr());

		SoRef<SoSeparator> circle1 = new SoSeparator;
		circleSep->addChild(circle1.ptr());
		SoRef<SoMaterial> circleMatl1 = new SoMaterial;
		circleMatl1->ambientColor.setValue(1, 0, 0);
		circleMatl1->diffuseColor.setValue(1, 0, 0);
		SoRef<SoTranslation> circleTrans1 = new SoTranslation;
		circleTrans1->translation.setValue(resultRayCast[0]);
		circle1->addChild(circleMatl1.ptr());
		circle1->addChild(circleTrans1.ptr());
		circle1->addChild(faceSet.ptr());

		SoRef<SoSeparator> circle2 = new SoSeparator;
		circleSep->addChild(circle2.ptr());
		SoRef<SoMaterial> circleMatl2 = new SoMaterial;
		circleMatl2->ambientColor.setValue(1, 0, 0);
		circleMatl2->diffuseColor.setValue(1, 0, 0);
		SoRef<SoTranslation> circleTrans2 = new SoTranslation;
		circleTrans2->translation.setValue(resultRayCast[1]);
		circle2->addChild(circleMatl2.ptr());
		circle2->addChild(circleTrans2.ptr());
		circle2->addChild(faceSet.ptr());

		SoRef<SoSeparator> circle3 = new SoSeparator;
		circleSep->addChild(circle3.ptr());
		SoRef<SoMaterial> circleMatl3 = new SoMaterial;
		circleMatl3->ambientColor.setValue(1, 0, 0);
		circleMatl3->diffuseColor.setValue(1, 0, 0);
		SoRef<SoTranslation> circleTrans3 = new SoTranslation;
		circleTrans3->translation.setValue(resultRayCast[2]);
		circle3->addChild(circleMatl3.ptr());
		circle3->addChild(circleTrans3.ptr());
		circle3->addChild(faceSet.ptr());

		dd->lineTrans[0].append(circleTrans1.ptr());
		dd->lineTrans[1].append(circleTrans2.ptr());
		dd->lineTrans[2].append(circleTrans3.ptr());

		dd->handleMatl[0].append(circleMatl1.ptr());
		dd->handleMatl[1].append(circleMatl2.ptr());
		dd->handleMatl[2].append(circleMatl3.ptr());

		SoRef<SoSeparator> handleGroup = new SoSeparator;
		handleGroup->addChild(circleSep.ptr());
		handleGroup->addChild(lineSep.ptr());
		handleGroup->addChild(textSep.ptr());

		dd->handleRoot->addChild(handleGroup.ptr());

		dd->inProgress = false;

		dd->instance->Finish(QString("Angle: ") + label_text);
	}
	void AngleMeasureSep::LineMoveCB(void* pImpl, SoEventCallback* eventCB) {
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		if (dd->isInHandle) {
			//consume event
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback				
				eventCB->releaseEvents();
				return;
			}
			dd->lineTrans[dd->startIdx][dd->lineIdx]->translation.setValue(p->getPoint());
			dd->lineProp[dd->lineIdx]->vertex.set1Value(dd->startIdx, p->getPoint());

			dd->lineTrans[dd->startIdx][dd->lineIdx]->touch();

			eventCB->setHandled();
			return;
		}
		if(dd->isMouseDown) {
			const SoEvent* event = eventCB->getEvent();
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();
			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				dd->inProgress = false;
				dd->line->reset();
			}
		}
		eventCB->releaseEvents();
	}
	void AngleMeasureSep::LineButtonCB(void* pImpl, SoEventCallback* eventCB) {
		auto doubleComp = [](double left, double right)->bool {
			return std::fabs(left - right) < 0.00001;
		};
		auto dd = static_cast<Impl*>(pImpl);
		if(false == dd->isActivated) {
			eventCB->releaseEvents();
			return;
		}
		const SoEvent* event = eventCB->getEvent();
		const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)event;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			dd->isMouseDown = true;
			//check whether handle is under click or not
			SoHandleEventAction* action = eventCB->getAction();
			const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

			SoRayPickAction pickaction = SoRayPickAction(myRegion);
			pickaction.setPoint(event->getPosition());
			pickaction.setSceneManager(action->getSceneManager());
			pickaction.apply(action->getPickRoot());
			auto p = pickaction.getPickedPoint();
			if (p == NULL) {
				//broadcast mouse event to further callback
				dd->line->reset();
				eventCB->releaseEvents();
				return;
			}
			auto point = p->getPoint();
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
			if (false == dd->inProgress) {
				for (auto i = 0; i < dd->lineProp.count(); i++) {
					for (auto j = 0; j < 3; j++) {
						if (false == doubleComp(dd->linePoints[j][i][2], point[2])) {
							continue;
						}
						auto dist = sqrt(pow(dd->linePoints[j][i][0] - point[0], 2) + pow(dd->linePoints[j][i][1] - point[1], 2));
						if (dist > dd->handleRadius) {
							continue;
						}
						dd->isInHandle = true;
						dd->lineIdx = i;
						dd->startIdx = j;
						break;
					}
					if (dd->isInHandle) {
						dd->handleMatl[dd->startIdx][dd->lineIdx]->ambientColor.setValue(1, 1, 0);
						dd->handleMatl[dd->startIdx][dd->lineIdx]->diffuseColor.setValue(1, 1, 0);

						break;
					}
				}
			}
			dd->inProgress = !dd->isInHandle;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			dd->isMouseDown = false;
			if (dd->isInHandle) {
				SoHandleEventAction* action = eventCB->getAction();
				const SbViewportRegion& myRegion = eventCB->getAction()->getViewportRegion();

				SoRayPickAction pickaction = SoRayPickAction(myRegion);
				pickaction.setPoint(event->getPosition());
				pickaction.setSceneManager(action->getSceneManager());
				pickaction.apply(action->getPickRoot());
				auto p = pickaction.getPickedPoint();
				dd->linePoints[dd->startIdx][dd->lineIdx] = p->getPoint();

				dd->handleMatl[dd->startIdx][dd->lineIdx]->ambientColor.setValue(1, 0, 0);
				dd->handleMatl[dd->startIdx][dd->lineIdx]->diffuseColor.setValue(1, 0, 0);

				//recompute degree
				auto p1 = dd->linePoints[dd->startIdx][dd->lineIdx];
				auto p2 = dd->linePoints[dd->startIdx + 1][dd->lineIdx];
				auto p3 = dd->linePoints[dd->startIdx + 2][dd->lineIdx];

				if (dd->startIdx % 3 == 1) {
					p1 = dd->linePoints[dd->startIdx - 1][dd->lineIdx];
					p2 = dd->linePoints[dd->startIdx][dd->lineIdx];
					p3 = dd->linePoints[dd->startIdx + 1][dd->lineIdx];
				}
				else if (dd->startIdx % 3 == 2) {
					p1 = dd->linePoints[dd->startIdx - 2][dd->lineIdx];
					p2 = dd->linePoints[dd->startIdx - 1][dd->lineIdx];
					p3 = dd->linePoints[dd->startIdx][dd->lineIdx];
				}

				auto aa = sqrt(pow(p1[0] - p3[0], 2) + pow(p1[1] - p3[1], 2));
				auto bb = sqrt(pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2));
				auto cc = sqrt(pow(p2[0] - p3[0], 2) + pow(p2[1] - p3[1], 2));

				auto temp = (pow(bb, 2) + pow(cc, 2) - pow(aa, 2)) / (2 * bb * cc);
				auto ang = acos(temp);
				ang = ang * (180 / M_PI);

				dd->angles[dd->lineIdx] = ang;
				QString label_text = QString::number(ang, 'f', 2) + " " + QString::fromWCharArray(L"\x00B0");
				dd->labelText[dd->lineIdx]->string = label_text.toStdWString();

				dd->labelPos[dd->lineIdx]->translation.setValue(((p1[0] + p3[0]) / 2),
					((p1[1] + p3[1]) / 2),
					(p1[2] + p3[2]) / 2);
				dd->instance->Update(dd->lineIdx, QString("Angle: ") + label_text);
			}
			dd->isInHandle = false;
			dd->lineIdx = -1;
			dd->startIdx = -1;
		}
		if (dd->isInHandle) {
			//consume event
			eventCB->setHandled();
			return;
		}

		SoHandleEventAction* action2 = eventCB->getAction();
		const SbViewportRegion& myRegion2 = eventCB->getAction()->getViewportRegion();

		SoRayPickAction pickaction2 = SoRayPickAction(myRegion2);
		pickaction2.setPoint(event->getPosition());
		pickaction2.setSceneManager(action2->getSceneManager());
		pickaction2.apply(action2->getPickRoot());
		auto p = pickaction2.getPickedPoint();
		if (p == NULL) {
			dd->line->reset();
		}

		auto pos = mouseButton->getPositionFloat();
		auto viewport_size = myRegion2.getViewportSizePixels();
		auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
		if (mouse_in_setting) {
			dd->line->reset();
		}

		eventCB->releaseEvents();
	}
}