#pragma once

#include <memory>
#include <QObject>

#include "TC.Rendering.Interactive.MaskEditingTool.BrushExport.h"

class SoVolumeData;
class SoSeparator;
class SoEventCallback;

class TC_Rendering_Interactive_MaskEditingTool_Brush_API OivBrush : public QObject {
	enum Axis {
		XY,
		YZ,
		XZ
	};

	Q_OBJECT
public:
	OivBrush();
	~OivBrush();

	auto SetTargetVolume(SoVolumeData* targetVolume) -> void;
	auto GetSceneGraph() -> SoSeparator*;

	auto SetBrushSize(uint32_t size) -> void;
	auto GetBrushSize() const -> uint32_t;
	auto SetBrushValue(uint32_t value) -> void;
	auto GetBrushValue() const -> uint32_t;

	auto SetSliceNumber(int value) -> void;
	auto GetSliceNumber() const -> int;

	auto SetAxis(Axis ax) -> void;

signals:
	void sigHistory(int);

protected:
	auto BuildSceneGraph() -> void;
	static void HandleMouseMoveEvent(void* data, SoEventCallback* node);
	static void HandleMouseButtonEvent(void* data, SoEventCallback* node);

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
