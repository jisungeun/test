#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include "OivBrush.h"

struct OivBrush::Impl {
	bool mousePressed { false };
	bool isStartDrawing { true };

	int tID_while_drawing;
	int editID;
	int slice_number { -1 };
	SbVec3f currentTrace { 0, 0, 0 };

	int brushSize;
	int brushValue { 1 };
	std::vector<SbVec3f> contourVertices;

	Axis ax { XY };

	SoRef<SoVolumeData> targetVolume { nullptr };
	SoRef<SoVolumeData> tempSlice { nullptr };

	SoRef<SoEventCallback> eventCallback { nullptr };
	SoRef<SoMaterial> baseColor { nullptr };
	SoRef<SoDrawStyle> drawStyle { nullptr };
	SoRef<SoSeparator> root { nullptr };
	SoRef<SoAnnotation> drawerSeparator { nullptr };

	SoRef<SoSeparator> cursorHoverSep { nullptr };
	SoRef<SoTranslation> cursorTranslation { nullptr };
	SoRef<SoFaceSet> cursorFaceSet { nullptr };
	SoRef<SoSwitch> cursorSwitch { nullptr };

	SoRef<SoSeparator> tempSep { nullptr };
	SoRef<SoSwitch> tempSocket { nullptr };
	SoRef<SoMaterial> tempMatl { nullptr };
	SoRef<SoDataRange> tempRange { nullptr };
	SoRef<SoTransferFunction> tempTF { nullptr };
	SoRef<SoOrthoSlice> tempRenderer { nullptr };

	auto UpperLeft(SbVec2f point);
	auto UpdateContour();

	auto DisplayCursor(const SbVec3f& pos);
	auto DrawBrush();
	auto EndBrush();

	SoVolumeData* CreateEmptySlice();
	OivBrush* thisPointer { nullptr };
};

auto OivBrush::Impl::UpperLeft(SbVec2f point) {
	SbVec2f result;
	result[0] = point[0] - 0.5;
	result[1] = point[1] + 0.5;

	return result;
}

auto OivBrush::Impl::UpdateContour() {
	auto radius = static_cast<float>(brushSize) / 2.f;

	SbVec2f centerCorrection({ 0.f, 0.f });

	bool evenSize = ((brushSize % 2) == 0);
	if (evenSize) {
		centerCorrection[0] += 0.5f;
		centerCorrection[1] += 0.5f;
	}

	// store points for each quarter part of a circle contour
	std::vector<SbVec2f> upperRightCycle;
	std::vector<SbVec2f> upperLeftCycle;
	std::vector<SbVec2f> lowerRightCycle;
	std::vector<SbVec2f> lowerLeftCycle;

	SbVec2f point(0, static_cast<float>(brushSize / 2));
	bool isInside = true;

	upperRightCycle.push_back(UpperLeft(point));

	while (point[1] > 0) {
		// move right until pixel is outside circle
		auto squaredX = 0.f;
		auto squaredY = pow(point[1] - centerCorrection[1], 2);

		while (isInside) {
			point[0]++;
			squaredX = pow(point[0] - centerCorrection[0], 2);

			if (sqrt(squaredX + squaredY) > radius) {
				isInside = false;
			}
		}

		upperRightCycle.push_back(UpperLeft(point));

		// move down until pixel is inside circle
		while (!isInside) {
			point[1]--;
			squaredY = pow(point[1] - centerCorrection[1], 2);

			if (sqrt(squaredX + squaredY) <= radius) {
				isInside = true;
				upperRightCycle.push_back(UpperLeft(point));
			}

			if (point[1] <= 0) {
				break;
			}
		}
	}

	// compute points of other quarters
	if (!evenSize) {
		for (auto elem : upperRightCycle) {
			auto transferPoint = elem;

			transferPoint[1] *= -1;
			lowerRightCycle.push_back(transferPoint);

			transferPoint[0] *= -1;
			lowerLeftCycle.push_back(transferPoint);

			transferPoint[1] *= -1;
			upperLeftCycle.push_back(transferPoint);
		}
	} else {
		for (auto elem : upperRightCycle) {
			auto transferPoint = elem;

			transferPoint[1] = -(transferPoint[1]) + 1;
			lowerRightCycle.push_back(transferPoint);

			transferPoint[0] = -(transferPoint[0]) + 1;
			lowerLeftCycle.push_back(transferPoint);

			transferPoint = elem;
			transferPoint[0] = -(transferPoint[0]) + 1;
			upperLeftCycle.push_back(transferPoint);
		}
	}

	std::vector<SbVec3f> contour;

	for (auto it = upperRightCycle.begin(); it != upperRightCycle.end(); ++it) {
		contour.push_back(
						SbVec3f((*it)[0], (*it)[1], 0.0)
						);
	}

	for (auto it = lowerRightCycle.rbegin(); it != lowerRightCycle.rend(); ++it) {
		contour.push_back(
						SbVec3f((*it)[0], (*it)[1], 0.0)
						);
	}

	for (auto it = lowerLeftCycle.begin(); it != lowerLeftCycle.end(); ++it) {
		contour.push_back(
						SbVec3f((*it)[0], (*it)[1], 0.0)
						);
	}

	for (auto it = upperLeftCycle.rbegin(); it != upperLeftCycle.rend(); ++it) {
		contour.push_back(
						SbVec3f((*it)[0], (*it)[1], 0.0)
						);
	}

	contourVertices = contour;
}

auto OivBrush::Impl::DisplayCursor(const SbVec3f& pos) {
	auto volumeData = targetVolume;
	if (volumeData == nullptr) {
		return;
	}

	auto voxelSize = volumeData->getVoxelSize();

	std::vector<SbVec3f> voxelContourVertices;
	for (auto elem : contourVertices) {
		SbVec3f vertex;
		switch (ax) {
			case XY:
				vertex[0] = elem[0] * voxelSize[0];
				vertex[1] = elem[1] * voxelSize[1];
				vertex[2] = 0.f;
				break;
			case XZ:
				vertex[0] = elem[1] * voxelSize[0];
				vertex[1] = 0.f;
				vertex[2] = elem[0] * voxelSize[2];
				break;
			case YZ:
				vertex[0] = 0.f;
				vertex[1] = elem[1] * voxelSize[1];
				vertex[2] = elem[0] * voxelSize[2];
				break;
			default:
				break;
		}
		voxelContourVertices.push_back(vertex);
	}
	if (!voxelContourVertices.empty()) {
		// apply modification to current mask volume
		SoVertexProperty* contourVertexProp = new SoVertexProperty;
		contourVertexProp->vertex.setValues(0, static_cast<int>(voxelContourVertices.size()), &voxelContourVertices[0]);
		cursorFaceSet->numVertices.setNum(0);
		cursorFaceSet->numVertices.set1Value(0, static_cast<int>(voxelContourVertices.size()));
		cursorFaceSet->vertexProperty.setValue(contourVertexProp);
	}
	cursorTranslation->translation.setValue(pos);
}

auto OivBrush::Impl::DrawBrush() {
	if (!targetVolume) {
		return;
	}
	std::vector<SbVec3f> faceSetVertices;
	auto mousePos = cursorTranslation->translation.getValue();

	auto volExt = tempSlice->extent.getValue();
	auto voxelSize = tempSlice->getVoxelSize();

	const auto zRes = targetVolume->getVoxelSize()[2];
	const auto zPhysMin = targetVolume->extent.getValue().getMin()[2];
	const auto zPos = slice_number * zRes + zPhysMin;

	for (auto elem : contourVertices) {
		SbVec3f vertex;
		switch (ax) {
			case XY:
				vertex[0] = elem[0] * voxelSize[0] + mousePos[0];
				vertex[1] = elem[1] * voxelSize[1] + mousePos[1];
				vertex[2] = zPos;
				break;
			case XZ:
				vertex[0] = elem[1] * voxelSize[0] + mousePos[0];
				vertex[1] = mousePos[1];
				vertex[2] = elem[0] * voxelSize[2] + mousePos[2];
				break;
			case YZ:
				vertex[0] = mousePos[0];
				vertex[1] = elem[1] * voxelSize[1] + mousePos[1];
				vertex[2] = elem[0] * voxelSize[2] + mousePos[2];
				break;
			default:
				break;
		}

		faceSetVertices.push_back(vertex);
	}

	if (!faceSetVertices.empty()) {
		// apply modification to current mask volume		
		SoRef<SoVertexProperty> contourVertexProp = new SoVertexProperty;
		contourVertexProp->vertex.setValues(0, static_cast<int>(faceSetVertices.size()), &faceSetVertices[0]);

		SoRef<SoFaceSet> contourFaceSet = new SoFaceSet;
		contourFaceSet->ref();
		contourFaceSet->numVertices.setNum(0);
		contourFaceSet->numVertices.set1Value(0, static_cast<int>(faceSetVertices.size()));
		contourFaceSet->vertexProperty.setValue(contourVertexProp.ptr());

		//int editionId;
		if (isStartDrawing) {
			tempSlice->startEditing(tID_while_drawing);
			isStartDrawing = false;
		}
		tempSlice->editSurfaceShape(contourFaceSet.ptr(), 1., 1.0);
		while (contourFaceSet->getRefCount() > 0) {
			contourFaceSet->unref();
		}

	}
}

auto OivBrush::Impl::EndBrush() {
	auto volumeData = targetVolume;
	if (nullptr == volumeData) {
		return;
	}
	tempSlice->finishEditing(tID_while_drawing);
	tempSlice->saveEditing();
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
	auto size = tempSlice->getDimension();
	auto slice_num = slice_number;
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		tempSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	tempSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
	//apply temporal slice to original volume
	auto name_idx = 0;
	if (ax == XY) {
		tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[0] * size[1]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = brushValue;
			}
		}
		name_idx = 0;
	} else if (ax == YZ) {
		tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[0] * size[2]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = brushValue;
			}
		}
		name_idx = 1;
	} else if (ax == XZ) {//XZ plane
		tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		for (auto i = 0; i < size[1] * size[2]; i++) {
			if (sourcedata[i] > 0) {
				targetdata[i] = brushValue;
			}
		}
		name_idx = 2;
	}
	int editionId;
	volumeData->startEditing(editionId);
	volumeData->editSubVolume(tBox, tBufferObj.ptr());
	volumeData->finishEditing(editionId);
	//volumeData->saveEditing();// no history
	editID = editionId;

	emit thisPointer->sigHistory(editID);

	tBufferObj->unmap();
	dataBufferObj->unmap();

	auto sw = tempSocket;
	sw->removeAllChildren();
	sw->whichChild = -1;

	tempSlice->unref();
	tempSlice = nullptr;
}

SoVolumeData* OivBrush::Impl::CreateEmptySlice() {
	auto vol = targetVolume;
	if (nullptr == vol) {
		return nullptr;
	}

	auto slice_num = slice_number;

	auto ext = vol->extent.getValue();
	auto voxelSize = vol->getVoxelSize();

	auto size = vol->getDimension();
	if (ax == XY) {//for XY slice
		//copy xy slice from current mask volume			
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[1]](), std::default_delete<unsigned short[]>());

		tempSlice = new SoVolumeData;

		tempSlice->data.setValue(SbVec3i32(size[0], size[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2] * (slice_num),
									ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2] * (slice_num + 1));
		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		tempSocket->addChild(tempSlice.ptr());
		tempSocket->whichChild = 0;

		emptySlice = nullptr;
	} else if (ax == YZ) {//for YZ slice
		//copy xz slice from current mask volume						
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[2]](), std::default_delete<unsigned short[]>());
		tempSlice = new SoVolumeData;

		tempSlice->data.setValue(SbVec3i32(size[0], 1, size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1] * slice_num, ext.getMin()[2],
									ext.getMax()[0], ext.getMin()[1] + voxelSize[1] * (slice_num + 1), ext.getMax()[2]);

		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		tempSocket->addChild(tempSlice.ptr());
		tempSocket->whichChild = 0;

		emptySlice = nullptr;
	} else if (ax == XZ) {//for XZ slice
		//copy yz slice from current mask volume
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[1] * size[2]](), std::default_delete<unsigned short[]>());

		tempSlice = new SoVolumeData;

		tempSlice->data.setValue(SbVec3i32(1, size[1], size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0] + voxelSize[0] * slice_num, ext.getMin()[1], ext.getMin()[2],
									ext.getMin()[0] + voxelSize[0] * (slice_num + 1), ext.getMax()[1], ext.getMax()[2]);
		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		tempSocket->addChild(tempSlice.ptr());
		tempSocket->whichChild = 0;

		emptySlice = nullptr;
	}
	return nullptr;
}

OivBrush::OivBrush() : d { new Impl } {
	d->thisPointer = this;
	BuildSceneGraph();
	SetBrushSize(10);
}

OivBrush::~OivBrush() { }

auto OivBrush::SetTargetVolume(SoVolumeData* targetVolume) -> void {
	d->targetVolume = targetVolume;
}

auto OivBrush::GetSceneGraph() -> SoSeparator* {
	return d->root.ptr();
}

auto OivBrush::BuildSceneGraph() -> void {
	d->root = new SoSeparator;
	d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	d->drawerSeparator = new SoAnnotation;
	d->root->addChild(d->drawerSeparator.ptr());

	d->eventCallback = new SoEventCallback;
	d->eventCallback->addEventCallback(SoLocation2Event::getClassTypeId(), HandleMouseMoveEvent, d.get());
	d->eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
	d->drawerSeparator->addChild(d->eventCallback.ptr());

	d->baseColor = new SoMaterial;
	d->baseColor->diffuseColor.setValue(1, 0, 0);
	d->drawerSeparator->addChild(d->baseColor.ptr());

	d->drawStyle = new SoDrawStyle;
	d->drawStyle->style = SoDrawStyle::Style::LINES;
	d->drawerSeparator->addChild(d->drawStyle.ptr());

	d->cursorHoverSep = new SoSeparator;
	d->cursorHoverSep->setName("HoverSep");
	d->cursorTranslation = new SoTranslation;
	d->cursorTranslation->translation.setValue(0.f, 0.f, 0.f);
	d->cursorHoverSep->addChild(d->cursorTranslation.ptr());

	d->cursorFaceSet = new SoFaceSet;
	d->cursorHoverSep->addChild(d->cursorFaceSet.ptr());

	d->cursorSwitch = new SoSwitch;
	d->cursorSwitch->addChild(d->cursorHoverSep.ptr());
	d->cursorSwitch->whichChild = 0;
	d->drawerSeparator->addChild(d->cursorSwitch.ptr());

	d->tempSep = new SoSeparator;
	d->root->addChild(d->tempSep.ptr());

	d->tempSocket = new SoSwitch;
	d->tempSep->addChild(d->tempSocket.ptr());

	d->tempMatl = new SoMaterial;
	d->tempMatl->diffuseColor.setValue(1, 1, 1);
	d->tempSep->addChild(d->tempMatl.ptr());

	d->tempRange = new SoDataRange;
	d->tempSep->addChild(d->tempRange.ptr());
	d->tempRange->min = 0;
	d->tempRange->max = 1;
	d->tempTF = new SoTransferFunction;
	d->tempSep->addChild(d->tempTF.ptr());
	d->tempTF->colorMap.setNum(8);
	auto* p = d->tempTF->colorMap.startEditing();
	*p++ = 0;
	*p++ = 0;
	*p++ = 0;
	*p++ = 0;
	*p++ = 1;
	*p++ = 0;
	*p++ = 0;
	*p = 0.5;
	d->tempTF->colorMap.finishEditing();


	d->tempRenderer = new SoOrthoSlice;
	d->tempRenderer->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
	d->tempSep->addChild(d->tempRenderer.ptr());

}

auto OivBrush::SetAxis(Axis ax) -> void {
	d->ax = ax;
	if (ax == XY) {
		d->tempRenderer->axis = SoOrthoSlice::Axis::Z;
	} else if (ax == YZ) {
		d->tempRenderer->axis = SoOrthoSlice::Axis::X;
	} else {
		d->tempRenderer->axis = SoOrthoSlice::Axis::Y;
	}
}

auto OivBrush::SetSliceNumber(int value) -> void {
	if (!d->targetVolume) {
		return;
	}
	if (value >= d->targetVolume->getDimension()[2]) {
		return;
	}
	if (d->slice_number == value) {
		return;
	}
	d->slice_number = value;

	if (d->tempSlice) {
		auto sw = d->tempSocket;
		sw->removeAllChildren();
		sw->whichChild = -1;

		d->tempSlice->unref();
		d->tempSlice = nullptr;
	}
}

auto OivBrush::GetSliceNumber() const -> int {
	return d->slice_number;
}

auto OivBrush::SetBrushSize(uint32_t size) -> void {
	d->brushSize = size;
	d->UpdateContour();
	d->DisplayCursor(d->cursorTranslation->translation.getValue());
}

auto OivBrush::GetBrushSize() const -> uint32_t {
	return d->brushSize;
}

auto OivBrush::SetBrushValue(uint32_t value) -> void {
	d->brushValue = value;
	if (value < 1) {
		auto* p = d->tempTF->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 1;
		*p++ = 0;
		*p++ = 0;
		*p = 0.5;
		d->tempTF->colorMap.finishEditing();
		d->baseColor->diffuseColor.setValue(1, 0, 0);
	} else {
		auto* p = d->tempTF->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 1;
		*p++ = 0;
		*p = 0.5;
		d->tempTF->colorMap.finishEditing();
		d->baseColor->diffuseColor.setValue(0, 1, 0);
	}
}

auto OivBrush::GetBrushValue() const -> uint32_t {
	return d->brushValue;
}

void OivBrush::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (d == nullptr) {
		action->setHandled();
		return;
	}

	const SoEvent* event = node->getEvent();

	if (SO_MOUSE_PRESS_EVENT(event, BUTTON1)) {
		d->mousePressed = true;
		d->isStartDrawing = true;
		if (!d->tempSlice) {
			d->CreateEmptySlice();
		}
		d->DrawBrush();
	} else if (SO_MOUSE_RELEASE_EVENT(event, BUTTON1)) {
		d->mousePressed = false;
		d->EndBrush();
	}
	action->setHandled();
}

void OivBrush::HandleMouseMoveEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (d == nullptr) {
		action->setHandled();
		return;
	}
	auto event = action->getEvent();
	auto viewportRegion = action->getViewportRegion();

	if (d->contourVertices.empty()) {
		node->setHandled();
		return;
	}

	auto mousePosition = event->getPosition(viewportRegion);

	SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
	rayPick.setPoint(mousePosition);
	rayPick.apply(action->getPickRoot());

	SoPickedPoint* pickedPt = rayPick.getPickedPoint();
	if (pickedPt) {
		auto currentPt = d->cursorTranslation->translation.getValue();
		auto nextPt = pickedPt->getPoint();
		if (currentPt != nextPt) {
			d->DisplayCursor(nextPt);
		}
		if (d->mousePressed) {
			auto dd = pickedPt->getPoint();
			if (d->currentTrace != dd) {
				d->DrawBrush();
				d->currentTrace = dd;
			}
		}
	}

	node->setHandled();
}
