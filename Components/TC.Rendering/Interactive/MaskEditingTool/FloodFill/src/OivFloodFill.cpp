#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/SoPickedPoint.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include "OivFloodFill.h"

using namespace imagedev;
using namespace iolink;
using namespace ioformat;

struct OivFloodFill::Impl {
	SoVolumeData* targetVolume { nullptr };
	int fillValue { 1 };
	bool isFill { true };

	SoRef<SoSeparator> root { nullptr };
	SoRef<SoAnnotation> drawerSeparator { nullptr };
	SoRef<SoEventCallback> eventCallback { nullptr };

	SoRef<SoVolumeData> tempSlice { nullptr };
	SoRef<SoVolumeData> tempFilled { nullptr };

	SbVec3f cur_seed { 0, 0, 0 };

	Axis ax { XY };

	int slice_number { 0 };
	int cur_pick { -1 };

	auto CopyCurrentSlice() -> void;
	auto FillProcess() -> void;
	auto EraseProcess() -> void;
	auto GetLabelValue() -> uint32_t;
	auto CreateFilledSlice() -> void;

	auto SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView>;
	OivFloodFill* thisPointer { nullptr };
};

auto OivFloodFill::Impl::SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };
	try {
		const auto dim = vol->getDimension();
		const auto x = dim[0];
		const auto y = dim[1];
		const auto resolution = vol->getVoxelSize();

		auto spatialCalibrationProperty = SpatialCalibrationProperty(
																	{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																	{ resolution[0], resolution[1], 1 }	// spacing
																	);
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

		auto buffer = vol->data.getValue();
		const RegionXu64 imageRegion { { 0, 0 }, imageShape };
		imageView->writeRegion(imageRegion, buffer);
	} catch (Exception& e) {
		std::cout << e.what() << std::endl;
	}
	return imageView;
}

auto OivFloodFill::Impl::CopyCurrentSlice() -> void {
	//Copy current slice 
	auto vol = targetVolume;
	if (nullptr == vol) {
		return;
	}
	CreateFilledSlice();

	auto cur_idx = slice_number;
	tempSlice = new SoVolumeData;
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SbBox3i32 bBox;
	auto dim = vol->getDimension();
	auto ext = vol->extent.getValue();
	auto voxelSize = vol->getVoxelSize();

	if (ax == XY) {
		bBox = SbBox3i32(SbVec3i32(0, 0, cur_idx), SbVec3i32(dim[0] - 1, dim[1] - 1, cur_idx));
		SoLDMDataAccess::DataInfoBox dataInfoBox =
			vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
		vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
		auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

		tempSlice->data.setValue(SbVec3i32(dim[0], dim[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2] * cur_idx,
									ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2] * (cur_idx + 1));
		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
	} else if (ax == XZ) {
		bBox = SbBox3i32(SbVec3i32(0, cur_idx, 0), SbVec3i32(dim[0] - 1, cur_idx, dim[2] - 1));
		SoLDMDataAccess::DataInfoBox dataInfoBox =
			vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
		vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
		auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

		tempSlice->data.setValue(SbVec3i32(dim[0], 1, dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1] * cur_idx, ext.getMin()[2],
									ext.getMax()[0], ext.getMin()[1] + voxelSize[1] * (cur_idx + 1), ext.getMax()[2]);
		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
	} else if (ax == YZ) {
		bBox = SbBox3i32(SbVec3i32(cur_idx, 0, 0), SbVec3i32(cur_idx, dim[1] - 1, dim[2] - 1));
		SoLDMDataAccess::DataInfoBox dataInfoBox =
			vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
		vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
		auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

		tempSlice->data.setValue(SbVec3i32(1, dim[1], dim[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)sourcedata, SoSFArray::COPY);
		tempSlice->extent.setValue(ext.getMin()[0] + voxelSize[0] * cur_idx, ext.getMin()[1], ext.getMin()[2],
									ext.getMin()[0] + voxelSize[0] * (cur_idx + 1), ext.getMax()[1], ext.getMax()[2]);
		tempSlice->setName("tempData");
		tempSlice->ldmResourceParameters.getValue()->resolution = 0;
		tempSlice->ldmResourceParameters.getValue()->fixedResolution = TRUE;
	}
	dataBufferObj->unmap();
	dataBufferObj = nullptr;
}

auto OivFloodFill::Impl::FillProcess() -> void {
	//find connected component object using clicked seed           
	const auto sliceView = SoVolumeToImageView(tempSlice.ptr());

	const auto thresholded = thresholding(sliceView, { static_cast<double>(fillValue), fillValue + 0.5 });

	const auto thresh2 = thresholding(sliceView, { 1.0,INT_MAX });

	const auto noted = logicalNot(thresh2);

	const auto ppp = tempSlice->XYZToVoxel(cur_seed);
	const auto floodfilled = floodFillThreshold2d(noted, { static_cast<int>(ppp[0]), static_cast<int>(ppp[1]) }, { 0.5, 100.0 });

	const auto logiced = logicalOperationWithImage(thresholded, floodfilled, LogicalOperationWithImage::OR);

	const auto filledView = SoVolumeToImageView(tempFilled.ptr());

	const auto masked1 = maskImage(filledView, logiced);

	const auto inv = logicalNot(logiced);

	const auto masked2 = maskImage(sliceView, inv);

	const auto ari = arithmeticOperationWithImage(masked1, masked2, ArithmeticOperationWithImage::ADD);

	const auto converted = convertImage(ari, ConvertImage::UNSIGNED_INTEGER_16_BIT);

	SoRef<SoVolumeData> filledSlice = new SoVolumeData;
	filledSlice->data.setValue(tempSlice->getDimension(), SbDataType::UNSIGNED_SHORT, 16, converted->buffer(), SoSFArray::COPY);
	filledSlice->extent.setValue(tempSlice->extent.getValue());

	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
	auto size = tempSlice->getDimension();
	auto slice_num = slice_number;
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		filledSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	filledSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

	auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
	//apply changed slice to current slice
	auto volumeData = targetVolume;
	if (ax == XY) {
		tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox = volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	} else if (ax == XZ) {
		tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	} else if (ax == YZ) {
		tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	}
	int editionId;
	volumeData->startEditing(editionId);
	volumeData->editSubVolume(tBox, tBufferObj.ptr());
	volumeData->finishEditing(editionId);
	//volumeData->saveEditing(); //No history

	emit thisPointer->sigHistory(editionId);

	tBufferObj->unmap();
	dataBufferObj->unmap();

	tBufferObj = nullptr;
	dataBufferObj = nullptr;
}

auto OivFloodFill::Impl::EraseProcess() -> void {
	const auto sliceView = SoVolumeToImageView(tempSlice.ptr());

	const auto thresholded = thresholding(sliceView, { static_cast<double>(fillValue), fillValue + 0.5 });

	const auto ppp = tempSlice->XYZToVoxel(cur_seed);
	const auto floodfilled = floodFillThreshold2d(thresholded, { static_cast<int>(ppp[0]), static_cast<int>(ppp[1]) }, { 0.5, 100.0 });

	const auto filledView = SoVolumeToImageView(tempFilled.ptr());

	const auto inv = logicalNot(floodfilled);

	const auto masked = maskImage(sliceView, inv);

	const auto converted = convertImage(masked, ConvertImage::UNSIGNED_INTEGER_16_BIT);

	SoRef<SoVolumeData> filledSlice = new SoVolumeData;
	filledSlice->data.setValue(tempSlice->getDimension(), SbDataType::UNSIGNED_SHORT, 16, converted->buffer(), SoSFArray::COPY);
	filledSlice->extent.setValue(tempSlice->extent.getValue());

	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	SoRef<SoCpuBufferObject> tBufferObj = new SoCpuBufferObject;
	SbBox3i32 tBox;
	auto size = tempSlice->getDimension();
	auto slice_num = slice_number;
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		filledSlice->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	filledSlice->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

	auto sourcedata = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));
	//apply changed slice to current slice
	auto volumeData = targetVolume;
	if (ax == XY) {
		tBox = SbBox3i32(SbVec3i32(0, 0, slice_num), SbVec3i32(size[0] - 1, size[1] - 1, slice_num));
		SoLDMDataAccess::DataInfoBox tInfoBox = volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	} else if (ax == XZ) {
		tBox = SbBox3i32(SbVec3i32(0, slice_num, 0), SbVec3i32(size[0] - 1, slice_num, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	} else if (ax = YZ) {
		tBox = SbBox3i32(SbVec3i32(slice_num, 0, 0), SbVec3i32(slice_num, size[1] - 1, size[2] - 1));
		SoLDMDataAccess::DataInfoBox tInfoBox =
			volumeData->getLdmDataAccess().getData(0, tBox, (SoBufferObject*)NULL);
		tBufferObj->setSize((size_t)tInfoBox.bufferSize);
		volumeData->getLdmDataAccess().getData(0, tBox, tBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(tBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, sourcedata, dataBufferObj->getSize());
	}

	int editionId;
	volumeData->startEditing(editionId);
	volumeData->editSubVolume(tBox, tBufferObj.ptr());
	volumeData->finishEditing(editionId);

	//TODO :Temporal - no history
	volumeData->saveEditing();


	tBufferObj->unmap();
	dataBufferObj->unmap();

	tBufferObj = nullptr;
	dataBufferObj = nullptr;
}

auto OivFloodFill::Impl::GetLabelValue() -> uint32_t {
	auto volData = targetVolume;
	auto idx = volData->XYZToVoxel(cur_seed);
	auto pos = SbVec3i32(static_cast<int32_t>(idx[0]), static_cast<int32_t>(idx[1]), static_cast<int32_t>(idx[2]));
	auto clabel = static_cast<uint32_t>(volData->getValue(pos));
	return clabel;
}

auto OivFloodFill::Impl::CreateFilledSlice() -> void {
	auto vol = targetVolume;
	if (nullptr == vol) {
		return;
	}
	auto slice_num = slice_number;
	auto size = vol->getDimension();
	auto ext = vol->extent.getValue();
	auto voxelSize = vol->getVoxelSize();
	if (ax == XY) {
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[1]](), std::default_delete<unsigned short[]>());
		std::fill_n(emptySlice.get(), size[0] * size[1], fillValue);

		tempFilled = new SoVolumeData;
		tempFilled->data.setValue(SbVec3i32(size[0], size[1], 1), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempFilled->extent.setValue(ext.getMin()[0], ext.getMin()[1], ext.getMin()[2] + voxelSize[2] * (slice_num),
									ext.getMax()[0], ext.getMax()[1], ext.getMin()[2] + voxelSize[2] * (slice_num + 1));
		tempFilled->setName("tempFilled");
		tempFilled->ldmResourceParameters.getValue()->resolution = 0;
		tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		emptySlice = nullptr;
	} else if (ax == XZ) {
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[0] * size[2]](), std::default_delete<unsigned short[]>());
		std::fill_n(emptySlice.get(), size[0] * size[2], fillValue);

		tempFilled = new SoVolumeData;
		tempFilled->data.setValue(SbVec3i32(size[0], 1, size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempFilled->extent.setValue(ext.getMin()[0], ext.getMin()[1] + voxelSize[1] * slice_num, ext.getMin()[2],
									ext.getMax()[0], ext.getMin()[1] + voxelSize[1] * (slice_num + 1), ext.getMax()[2]);
		tempFilled->setName("tempFilled");
		tempFilled->ldmResourceParameters.getValue()->resolution = 0;
		tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		emptySlice = nullptr;
	} else if (ax == YZ) {
		std::shared_ptr<unsigned short> emptySlice(new unsigned short[size[1] * size[2]](), std::default_delete<unsigned short[]>());
		std::fill_n(emptySlice.get(), size[1] * size[2], fillValue);

		tempFilled = new SoVolumeData;
		tempFilled->data.setValue(SbVec3i32(1, size[1], size[2]), SbDataType::UNSIGNED_SHORT, 16, (void*)emptySlice.get(), SoSFArray::COPY);
		tempFilled->extent.setValue(ext.getMin()[0] + voxelSize[0] * slice_num, ext.getMin()[1], ext.getMin()[2],
									ext.getMin()[0] + voxelSize[0] * (slice_num + 1), ext.getMax()[1], ext.getMax()[2]);
		tempFilled->setName("tempFilled");
		tempFilled->ldmResourceParameters.getValue()->resolution = 0;
		tempFilled->ldmResourceParameters.getValue()->fixedResolution.setValue(TRUE);

		emptySlice = nullptr;
	}
}


OivFloodFill::OivFloodFill() : d { new Impl } {
	d->thisPointer = this;
	BuildSceneGraph();
}

OivFloodFill::~OivFloodFill() { }

auto OivFloodFill::SetSliceNumber(int num) -> void {
	d->slice_number = num;
}

auto OivFloodFill::SetAxis(Axis ax) -> void {
	d->ax = ax;
}

auto OivFloodFill::ToggleFillErase(bool isFill) -> void {
	d->isFill = isFill;
}


auto OivFloodFill::SetTargetVolume(SoVolumeData* targetVolume) -> void {
	d->targetVolume = targetVolume;
}

auto OivFloodFill::GetSceneGraph() -> SoSeparator* {
	return d->root.ptr();
}

auto OivFloodFill::SetCurLabel(uint32_t label) -> void {
	d->fillValue = label;
}

auto OivFloodFill::GetCurLabel() -> uint32_t {
	return d->fillValue;
}

auto OivFloodFill::BuildSceneGraph() -> void {
	d->root = new SoSeparator;
	d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	d->drawerSeparator = new SoAnnotation;
	d->root->addChild(d->drawerSeparator.ptr());

	d->eventCallback = new SoEventCallback;
	d->eventCallback->addEventCallback(SoMouseButtonEvent::getClassTypeId(), HandleMouseButtonEvent, d.get());
	d->drawerSeparator->addChild(d->eventCallback.ptr());
}

void OivFloodFill::HandleMouseButtonEvent(void* data, SoEventCallback* node) {
	auto action = node->getAction();

	auto d = static_cast<Impl*>(data);
	if (nullptr == d) {
		action->setHandled();
		return;
	}
	const SoEvent* event = node->getEvent();
	auto viewportRegion = action->getViewportRegion();
	auto mousePos = event->getPosition(viewportRegion);
	SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
	rayPick.setPoint(mousePos);
	rayPick.apply(action->getPickRoot());

	SoPickedPoint* pickedPt = rayPick.getPickedPoint();

	if (pickedPt) {
		if (SO_MOUSE_PRESS_EVENT(event, BUTTON1)) {
			d->cur_seed = pickedPt->getPoint();
			//d->cur_label = d->GetLabelValue();
			d->cur_pick = d->GetLabelValue();
			if (d->isFill && d->cur_pick == 0) {
				d->CopyCurrentSlice();
				d->FillProcess();
			} else if (!d->isFill && d->fillValue == d->cur_pick) {
				d->CopyCurrentSlice();
				d->EraseProcess();
			}
		} else if (SO_MOUSE_RELEASE_EVENT(event, BUTTON1)) {
			d->cur_seed.setValue(0.0, 0.0, 0.0);
		}
	}
	action->setHandled();
}
