#pragma once

#include <memory>
#include <QObject>

#include "TC.Rendering.Interactive.MaskEditingTool.FloodFillExport.h"

class SoVolumeData;
class SoSeparator;
class SoEventCallback;

class TC_Rendering_Interactive_MaskEditingTool_FloodFill_API OivFloodFill : public QObject {
	enum Axis {
		XY,
		YZ,
		XZ
	};

	Q_OBJECT
public:
	OivFloodFill();
	~OivFloodFill();

	auto SetTargetVolume(SoVolumeData* targetVolume) -> void;
	auto GetSceneGraph() -> SoSeparator*;

	auto SetCurLabel(uint32_t label) -> void;
	auto GetCurLabel() -> uint32_t;

	auto SetSliceNumber(int num) -> void;

	auto ToggleFillErase(bool isFill) -> void;

	auto SetAxis(Axis ax) -> void;

signals:
	void sigHistory(int);

protected:
	auto BuildSceneGraph() -> void;;
	static void HandleMouseButtonEvent(void* data, SoEventCallback* node);

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
