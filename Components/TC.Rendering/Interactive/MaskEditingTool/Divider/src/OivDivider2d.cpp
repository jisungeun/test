#pragma warning(disable:4002)

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>
#include <ImageDev/Data/NativeMeasurements.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/events/SoEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <QList>

#include "OivDivider2d.h"

using namespace imagedev;
using namespace iolink;
using namespace ioformat;


SO_NODE_SOURCE(OivDivider);

struct OivDivider::Impl {
	SbVec2s lastAddedPoint;
	bool mouse_is_pressed { false };

	QList<SbVec2f> points;

	SoVolumeData* volData { nullptr };

	auto GetPointsInLine(SbVec2d p1, SbVec2d p2) -> QList<SbVec2d>; // in pixel(voxel) space
	auto SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView>;
};

auto OivDivider::Impl::SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };
	try {
		const auto dim = vol->getDimension();
		const auto resolution = vol->getVoxelSize();

		const auto x = dim[0];
		const auto y = dim[1];
		const auto z = dim[2];

		if (z < 2) {	// 2D			
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																		{ resolution[0], resolution[1], 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, vol->data.getValue());

		} else {	// 3D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																		{ resolution[0], resolution[1], resolution[2] }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, vol->data.getValue());
		}
	} catch (Exception& e) {
		std::cout << e.what() << std::endl;
	}
	return imageView;
}


auto OivDivider::Impl::GetPointsInLine(SbVec2d p1, SbVec2d p2) -> QList<SbVec2d> {
	QList<SbVec2d> result;

	auto slopeX = p2[0] - p1[0];
	auto slopeY = p2[1] - p1[1];
	auto length = sqrt(slopeX * slopeX + slopeY * slopeY);
	double deltaX = slopeX / length;
	double deltaY = slopeY / length;

	for (auto i = 0; i <= length && length > 0; i++) {
		result << SbVec2d { p1[0] + deltaX * i, p1[1] + deltaY * i };
	}

	return result;
}

OivDivider::OivDivider() : d { std::make_unique<Impl>() } {
	SO_NODE_CONSTRUCTOR(OivDivider);
	simplificationThreshold.enableNotify(FALSE);
	this->simplificationThreshold.setValue(0);
	simplificationThreshold.enableNotify(TRUE);
}

OivDivider::~OivDivider() {}

void OivDivider::initClass() {
	SO__NODE_INIT_CLASS(OivDivider, "DividerFreeLine", SoPolyLineScreenDrawer);
}

void OivDivider::exitClass() {
	SO__NODE_EXIT_CLASS(OivDivider);
}

void OivDivider::reset() {}

auto OivDivider::SetTargetVolume(SoVolumeData* vol) -> void {
	d->volData = vol;
}

auto OivDivider::DivideLabel(SoPolyLineScreenDrawer::EventArg& eventArg, int label) -> std::tuple<int, int> {
	const auto action = eventArg.getAction();
	const auto drawer = eventArg.getSource();

	if (d->volData == nullptr) {
		drawer->clear();
		return std::make_tuple(-1, -1);
	}

	try {
		const auto dim = d->volData->getDimension();

		// step 1
		auto rayPick = SoRayPickAction(action->getViewportRegion());

		QList<SbVec3f> divideLineVoxelIndexes;
		for (auto i = 0; i < d->points.count(); i++) {
			const auto point = d->points.at(i);

			rayPick.setNormalizedPoint({ (point[0] + 1.f) / 2.f, (point[1] + 1.f) / 2.f });
			rayPick.apply(action->getPickRoot());

			const auto pickedPoint = rayPick.getPickedPoint();
			if (pickedPoint == nullptr)
				continue;

			const auto voxelIndex = d->volData->XYZToVoxel(pickedPoint->getPoint());
			divideLineVoxelIndexes << voxelIndex;
		}

		if (divideLineVoxelIndexes.isEmpty())
			throw std::runtime_error("Failed to get voxel indexes.");

		// step 2
		const auto zIndex = static_cast<int>(divideLineVoxelIndexes[0][2]);
		auto originVolumeIV = d->SoVolumeToImageView(d->volData);
		originVolumeIV = convertImage(originVolumeIV, ConvertImage::LABEL_16_BIT);

		const auto originSliceIV = getSliceFromVolume3d(originVolumeIV, GetSliceFromVolume3d::Z_AXIS, zIndex);

		auto currentLabelSliceIV = thresholding(originSliceIV, { static_cast<double>(label), label + 0.5 });
		currentLabelSliceIV = convertImage(currentLabelSliceIV, ConvertImage::LABEL_16_BIT);

		int hasCount = 0;
		for (auto i = 0; i < divideLineVoxelIndexes.count() - 1; i++) {
			const SbVec2d p1 = SbVec2d { static_cast<double>(divideLineVoxelIndexes[i][0]), static_cast<double>(divideLineVoxelIndexes[i][1]) };
			const SbVec2d p2 = SbVec2d { static_cast<double>(divideLineVoxelIndexes[i + 1][0]), static_cast<double>(divideLineVoxelIndexes[i + 1][1]) };

			const auto allPts = d->GetPointsInLine(p1, p2);
			auto buffer = static_cast<unsigned short*>(currentLabelSliceIV->buffer());

			for (auto& pt : allPts) {
				const auto bufferIndex = static_cast<int>(pt[0]) + dim[0] * static_cast<int>(pt[1]);
				if (*(buffer + bufferIndex) > 0)
					hasCount++;

				memset(buffer + bufferIndex, 0, sizeof(unsigned short));
			}
		}

		if (hasCount < 1)
			throw std::runtime_error("Out of current label.");

		const auto analysis = std::make_shared<AnalysisMsr>();
		const auto bboxOXMsr = analysis->select(NativeMeasurements::boundingBoxOX);
		const auto bboxOYMsr = analysis->select(NativeMeasurements::boundingBoxOY);
		const auto bboxDXMsr = analysis->select(NativeMeasurements::boundingBoxDX);
		const auto bboxDYMsr = analysis->select(NativeMeasurements::boundingBoxDY);

		labelAnalysis(currentLabelSliceIV, nullptr, analysis);

		const auto res = d->volData->getVoxelSize();

		auto cropOX = bboxOXMsr->value(0);
		auto cropOY = bboxOYMsr->value(0);
		auto cropDX = bboxDXMsr->value(0);
		auto cropDY = bboxDYMsr->value(0);

		const auto ox = static_cast<int>(round((cropOX + res[0] * dim[0] / 2) / res[0]));
		const auto oy = static_cast<int>(round((cropOY + res[1] * dim[1] / 2) / res[1]));
		auto dx = static_cast<int>(cropDX / res[0]) + 1;
		auto dy = static_cast<int>(cropDY / res[1]) + 1;
		if (dx < 1)
			dx = 1;
		if (dy < 1)
			dy = 1;

		auto cropIV = cropImage2d(currentLabelSliceIV, { ox, oy }, { dx, dy });

		// step 3
		auto dividedLabelIV = labeling2d(cropIV, Labeling2d::LABEL_16_BIT, Labeling2d::CONNECTIVITY_4);

		// step 4
		const auto dividedLabelHistogramMsr = intensityHistogram(dividedLabelIV, IntensityHistogram::MIN_MAX, {});
		QList<int64_t> dividedLabels;
		for (auto i = 0; i < dividedLabelHistogramMsr->size(); i++) {
			if (const auto value = dividedLabelHistogramMsr->intensityValue(i); value > 0) {
				dividedLabels << value;
			}
		}

		std::sort(dividedLabels.begin(), dividedLabels.end());

		const auto dividedLabelMin = dividedLabels.first();
		const auto dividedLabelMax = dividedLabels.last();

		const auto originLabelStat = intensityStatistics(originVolumeIV, IntensityStatistics::MIN_MAX, {});
		auto labelMax = dividedLabels.count() + originLabelStat->maximum() - 1;
		const auto revisedLabelNum = labelMax;

		for (auto labelIndex = dividedLabelMax; labelIndex >= dividedLabelMin && labelIndex > 0; labelIndex--) {
			const auto newLabelValue = (labelIndex == dividedLabelMin) ? label : labelMax;
			const auto formula = QString("if(I1 == %1, %2, I1)").arg(labelIndex).arg(newLabelValue);
			dividedLabelIV = imageFormula(dividedLabelIV, nullptr, nullptr, formula.toStdString(), ImageFormula::LABEL_16_BIT);

			labelMax--;
		}

		// step 5
		auto cropOriginSliceIV = cropImage2d(originSliceIV, { ox, oy }, { dx, dy });
		auto resultSliceIV = imageFormula(dividedLabelIV, cropOriginSliceIV, nullptr, "if(I1 == 0, I2, I1)", ImageFormula::LABEL_16_BIT);

		SoRef<SoCpuBufferObject> bufferObj = new SoCpuBufferObject;
		auto bBox = SbBox3i32(SbVec3i32(ox, oy, zIndex), SbVec3i32(ox + dx - 1, oy + dy - 1, zIndex));
		auto dataInfoBox = d->volData->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		bufferObj->setSize(dataInfoBox.bufferSize);
		d->volData->getLdmDataAccess().getData(0, bBox, bufferObj.ptr());

		auto buffer = static_cast<unsigned short*>(bufferObj->map(SoBufferObject::READ_WRITE));
		memcpy(buffer, resultSliceIV->bufferReadOnly(), resultSliceIV->bufferSize());

		int editionId;
		d->volData->startEditing(editionId);
		d->volData->editSubVolume(bBox, bufferObj.ptr());
		d->volData->finishEditing(editionId);
		//d->volData->saveEditing();//No history
		drawer->clear();
		d->points.clear();
		return std::make_tuple(revisedLabelNum, editionId);
	} catch (std::exception& e) {
		std::cout << e.what() << std::endl;
	}
	catch (Exception& e) {
		std::cout << e.what() << std::endl;
	}
	catch (...) {
		std::cout << "UNKNOWN ERROR" << std::endl;
	}
	drawer->clear();
	d->points.clear();
	return std::make_tuple(-1, -1);
}

void OivDivider::onKeyDown(SoHandleEventAction* action) {
	Q_UNUSED(action)
}

void OivDivider::onMouseDown(SoHandleEventAction* action) {
	const auto event = action->getEvent();

	d->mouse_is_pressed = true;
	auto pt = event->getNormalizedPosition(action->getViewportRegion());
	auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
	this->clear();
	this->addPoint(pos);

	d->points << pos;
}

void OivDivider::onMouseMove(SoHandleEventAction* action) {
	const auto event = action->getEvent();
	if (d->mouse_is_pressed) {
		auto pt = event->getNormalizedPosition(action->getViewportRegion());
		auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);

		d->points << pos;
		this->addPoint(pos);
	}
}

void OivDivider::onMouseUp(SoHandleEventAction* action) {
	const auto event = action->getEvent();

	auto pt = event->getNormalizedPosition(action->getViewportRegion());
	auto pos = SbVec2f(pt[0] * 2.0 - 1.0, pt[1] * 2.0 - 1.0);
	d->lastAddedPoint = SbVec2s(pos[0], pos[1]);
	d->mouse_is_pressed = false;
	this->addPoint(pos);
	d->points << pos;

	finalizeLine(action);

	this->clear();
}

void OivDivider::finalizeLine(SoHandleEventAction* action) {
	this->finalize(action);
}
