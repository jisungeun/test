#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/drawers/SoPolygonScreenDrawer.h>
#pragma warning(pop)

#include "TC.Rendering.Interactive.MaskEditingTool.Divider2dExport.h"

class SoVolumeData;


class TC_Rendering_Interactive_MaskEditingTool_Divider2d_API OivDivider : public SoPolyLineScreenDrawer {
	SO_NODE_HEADER(OivDivider);

public:
	OivDivider();
	~OivDivider();

	virtual void reset();

	auto SetTargetVolume(SoVolumeData* vol) -> void;

	auto DivideLabel(SoPolyLineScreenDrawer::EventArg& eventArg, int label) -> std::tuple<int, int>;

	SoINTERNAL public:
	static void initClass();

	static void exitClass();

	SoEXTENDER_Documented protected:
	virtual void onKeyDown(SoHandleEventAction* action);

	virtual void onMouseMove(SoHandleEventAction* action);

	virtual void onMouseDown(SoHandleEventAction* action);

	virtual void onMouseUp(SoHandleEventAction* action);

private:
	void finalizeLine(SoHandleEventAction*);

	struct Impl;
	std::unique_ptr<Impl> d;
};
