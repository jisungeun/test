#pragma once

#include <memory>
#include <QObject>

#include <Inventor/drawers/SoLassoScreenDrawer.h>

#include "TC.Rendering.Interactive.MaskEditingTool.LassoExport.h"

class SoVolumeData;

class TC_Rendering_Interactive_MaskEditingTool_Lasso_API OivLasso : public QObject {
	Q_OBJECT
public:
	OivLasso();
	~OivLasso();

	auto SetTargetVolume(SoVolumeData* targetVolume) -> void;
	auto GetSceneGraph() -> SoSeparator*;

	auto SetLassoValue(uint32_t value) -> void;
	auto GetLassoValue() const -> uint32_t;

signals:
	void sigHistory(int);

protected:
	auto BuildSceneGraph() -> void;
	auto LassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
