#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include "OivLasso.h"

struct OivLasso::Impl {
	SoVolumeData* targetVolume { nullptr };
	int lassoValue { 1 };

	SoRef<SoSeparator> root { nullptr };
	SoRef<SoAnnotation> drawerSeparator { nullptr };
	SoRef<SoLassoScreenDrawer> lassoTool { nullptr };

	auto CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion) -> std::vector<SbVec3f>;
	OivLasso* thisPointer { nullptr };
};

auto OivLasso::Impl::CalcRayCasting(SoMFVec2f* source, SoNode* targetNode, const SbViewportRegion& pickingRegion) -> std::vector<SbVec3f> {
	std::vector<SbVec3f> result;

	SoRayPickAction rayPick = SoRayPickAction(pickingRegion);
	for (int i = 0; i < source->getNum(); i++) {

		SbVec2f normalizedPoint((*source)[i][0], (*source)[i][1]);
		normalizedPoint[0] = (normalizedPoint[0] + 1.0f) / 2.0f;
		normalizedPoint[1] = (normalizedPoint[1] + 1.0f) / 2.0f;

		rayPick.setNormalizedPoint(normalizedPoint);
		rayPick.apply(targetNode);

		SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
		if (pickedPoint) {
			result.push_back(pickedPoint->getPoint());
		}
	}

	return result;
}

OivLasso::OivLasso() : d { new Impl } {
	d->thisPointer = this;
	BuildSceneGraph();
}

OivLasso::~OivLasso() {}

auto OivLasso::SetTargetVolume(SoVolumeData* targetVolume) -> void {
	d->targetVolume = targetVolume;
}

auto OivLasso::GetSceneGraph() -> SoSeparator* {
	return d->root.ptr();
}

auto OivLasso::SetLassoValue(uint32_t value) -> void {
	if (value < 1) {
		d->lassoTool->color.setValue(1, 0, 0);
	} else {
		d->lassoTool->color.setValue(0, 1, 0);
	}
	d->lassoValue = value;
}

auto OivLasso::GetLassoValue() const -> uint32_t {
	return d->lassoValue;
}

auto OivLasso::BuildSceneGraph() -> void {
	d->root = new SoSeparator;
	d->root->fastEditing = SoSeparator::CLEAR_ZBUFFER;

	d->drawerSeparator = new SoAnnotation;
	d->root->addChild(d->drawerSeparator.ptr());

	d->lassoTool = new SoLassoScreenDrawer;
	d->lassoTool->simplificationThreshold.setValue(0);
	d->lassoTool->isClosed = TRUE;
	d->lassoTool->doCCW = FALSE;
	d->lassoTool->onFinish.add(*this, &OivLasso::LassoCallback);

	d->drawerSeparator->addChild(d->lassoTool.ptr());
}

auto OivLasso::LassoCallback(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
	SoPolyLineScreenDrawer* lineDrawer = eventArg.getSource();
	SoHandleEventAction* action = eventArg.getAction();

	// If less than 1 point, shape cannot be generated.
	if (lineDrawer->point.getNum() < 1) {
		lineDrawer->clear();
		return;
	}

	auto resultRayCast = d->CalcRayCasting(&lineDrawer->point, action->getPickRoot(), action->getViewportRegion());

	//create closed polygon shape using shaped interaction
	SoVertexProperty* vertexProp = new SoVertexProperty;
	vertexProp->vertex.setValues(0, static_cast<int>(resultRayCast.size()), &resultRayCast[0]);

	SoFaceSet* faceSet = new SoFaceSet;
	faceSet->numVertices.set1Value(0, static_cast<int>(resultRayCast.size()));
	faceSet->vertexProperty.setValue(vertexProp);
	faceSet->ref();

	//apply modification to current mask volume
	int editionId;
	d->targetVolume->startEditing(editionId);
	d->targetVolume->editSurfaceShape(faceSet, 1., d->lassoValue);
	d->targetVolume->finishEditing(editionId);
	//d->targetVolume->saveEditing(); // no history

	emit sigHistory(editionId);
	lineDrawer->clear();
	faceSet->unref();
	action->setHandled();
}
