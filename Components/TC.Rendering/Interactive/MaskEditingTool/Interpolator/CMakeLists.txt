project(TC.Rendering.Interactive.MaskEditingTool.Interpolator)

find_package( OpenCV REQUIRED )

#Header files for external use
set(INTERFACE_HEADERS
	inc/CvInterpolate.h
	inc/OivInterpolator.h
)
	
#Header files for internal use
set(PRIVATE_HEADERS

)

#Sources
set(SOURCES
	src/CvInterpolate.cpp
	src/OivInterpolator.cpp
)

#Resources
set(RESOURCES
   
)


add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${RESOURCES}
)

add_library(TC::Rendering::Interactive::MaskEditingTool::Interpolator ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>				
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/		
		${OpenCV_INCLUDE_DIRS}
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/Rendering/Interactive/MaskEditingTool")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

target_link_libraries( ${PROJECT_NAME} 
	PUBLIC
		Extern::BetterEnums		
	PRIVATE				
		Qt5::Widgets
		Qt5::Gui
		Qt5::Core		
		
		${VIEWER_COMPONENT_LINK_LIBRARY}		
		ImageDev
		ImageDevExamples		
		${OpenCV_LIBS}
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT rendering)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)