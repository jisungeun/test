#pragma once

#include <memory>

#include "TC.Rendering.Interactive.MaskEditingTool.InterpolatorExport.h"

class SoVolumeData;

namespace TC {
	class TC_Rendering_Interactive_MaskEditingTool_Interpolator_API OivInterpolator {
	public:
		explicit OivInterpolator();
		~OivInterpolator();

		auto SetInputVolume(SoVolumeData* volData) -> void;
		auto SetLabelValue(int label) -> void;
		auto IsOverwrite(bool overwrite) -> void;
		auto Perform() -> std::tuple<bool, int>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
