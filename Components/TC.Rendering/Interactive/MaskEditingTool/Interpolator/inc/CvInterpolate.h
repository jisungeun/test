#pragma once

#include <memory>

namespace cv {
    class Mat;
}

namespace TC {
    class CvInterpolate {
    public:
        CvInterpolate();
        ~CvInterpolate();

        static auto Interpolate(cv::Mat& top, cv::Mat& bottom, float precision)->cv::Mat;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}