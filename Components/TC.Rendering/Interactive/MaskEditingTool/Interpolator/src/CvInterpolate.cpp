#include <iostream>
#include <format>

#include <opencv2/opencv.hpp>

#include "CVInterpolate.h"

using namespace cv;
using namespace std;

namespace TC {
	struct CvInterpolate::Impl {
		static auto create_centered_trapezoid(Mat& image, int top_width, int bottom_width, int height) -> void;
		static auto ndgrid(const Range& xgv, const Range& ygv, Mat& X, Mat& Y) -> void;
		static auto bwperim2(Mat& im)->Mat;
		static auto signed_bwdist(Mat& im)->Mat;
		static auto bwdist(Mat& im)->Mat;
	};

	auto CvInterpolate::Impl::create_centered_trapezoid(Mat& image, int top_width, int bottom_width, int height) -> void {
		image = Scalar(0);  // Set the entire image to black

		int center_row = image.rows / 2;
		int start_row = center_row - height / 2;

		for (int i = 0; i < height; ++i) {
			int current_width = top_width + (bottom_width - top_width) * i / (height - 1);
			int start_col = (image.cols - current_width) / 2;
			int end_col = start_col + current_width;

			// Draw a white rectangle to represent the trapezoid
			rectangle(image, Point(start_col, start_row + i), Point(end_col, start_row + i + 1), Scalar(255), FILLED);
		}
	}

	auto CvInterpolate::Impl::ndgrid(const Range& xgv, const Range& ygv, Mat& X, Mat& Y) -> void {
		vector<float> x, y;
		for (int i = xgv.start; i <= xgv.end; ++i) {
			x.push_back(static_cast<float>(i));
		}
		for (int j = ygv.start; j <= ygv.end; ++j) {
			y.push_back(static_cast<float>(j));
		}

		repeat(Mat(x).reshape(1, 1), y.size(), 1, X);
		repeat(Mat(y).reshape(1, 1).t(), 1, x.size(), Y);
	}

	auto CvInterpolate::Impl::bwperim2(Mat& im) -> Mat {
		Mat dilated;
		dilate(im, dilated, Mat());
		return 0 - (im - dilated);
	}

	auto CvInterpolate::Impl::signed_bwdist(Mat& im) -> Mat {
		Mat perim = bwperim2(im);
		Mat dist_im;

		distanceTransform(1 - perim, dist_im, DIST_L2, DIST_MASK_PRECISE);

		Mat result = Mat::zeros(im.size(), CV_64F);
		for (int i = 0; i < im.rows; ++i) {
			for (int j = 0; j < im.cols; ++j) {
				if (im.at<uchar>(i, j) == 0) {
					result.at<double>(i, j) = -dist_im.at<float>(i, j);
				}
				else {
					result.at<double>(i, j) = dist_im.at<float>(i, j);
				}
			}
		}

		return result;
	}


	auto CvInterpolate::Impl::bwdist(Mat& im) -> Mat {
		Mat dist_im;
		distanceTransform(1 - im, dist_im, DIST_L2, DIST_MASK_PRECISE);
		return dist_im;
	}

	CvInterpolate::CvInterpolate() : d(std::make_unique<Impl>()) {

	}

	CvInterpolate::~CvInterpolate() {

	}

	auto CvInterpolate::Interpolate(Mat& top, Mat& bottom, float precision) -> Mat {
		if (precision > 2) {
			cout << "Error: Precision must be between 0 and 1 (float)" << endl;
		}

		Mat top_dist = Impl::signed_bwdist(top);
		Mat bottom_dist = Impl::signed_bwdist(bottom);

		Mat result = Mat::zeros(top.size(), CV_8UC1);

		for (int i = 0; i < top.rows; ++i) {
			for (int j = 0; j < top.cols; ++j) {
				double weight_top = 1.0 - precision;
				double weight_bottom = precision;

				double interpolated_value = weight_top * top_dist.at<double>(i, j) + weight_bottom * bottom_dist.at<double>(i, j);

				result.at<uchar>(i, j) = (interpolated_value > 0) ? 255 : 0;
			}
		}

		return result;
	}
}

