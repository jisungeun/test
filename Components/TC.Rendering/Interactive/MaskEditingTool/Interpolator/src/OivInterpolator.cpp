#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QMap>

#include "CVInterpolate.h"
#include <opencv2/opencv.hpp>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "OivInterpolator.h"

using namespace imagedev;
using namespace iolink;
using namespace ioformat;

namespace TC {
	using ImageViewPtr = std::shared_ptr<ImageView>;

	struct OivInterpolator::Impl {
		std::shared_ptr<CvInterpolate> interpolate { nullptr };
		SoVolumeData* volData { nullptr };
		int label { -1 };
		bool isOverwrite { true };

		auto SoVolumeToImageView(SoVolumeData* vol) -> ImageViewPtr;
		auto ImageViewToCvMat(ImageViewPtr iv) -> cv::Mat;
		auto CvMatToImageView(cv::Mat& mat) -> ImageViewPtr;

		auto ApplyModifiedVolumeWithHistory(ImageViewPtr modifiedVolume) -> int;
		OivInterpolator* thisPointer { nullptr };
	};

	auto OivInterpolator::Impl::ApplyModifiedVolumeWithHistory(ImageViewPtr modifiedVolume) -> int {
		if (!volData) {
			return -1;
		}
		SoRef<SoCpuBufferObject> targetBufferObj = new SoCpuBufferObject;

		auto size = volData->getDimension();
		SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));

		SoLDMDataAccess::DataInfoBox targetInfoBox = volData->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		targetBufferObj->setSize((size_t)targetInfoBox.bufferSize);
		volData->getLdmDataAccess().getData(0, bBox, targetBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(targetBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, modifiedVolume->buffer(), targetBufferObj->getSize());

		int editionId;
		volData->startEditing(editionId);
		volData->editSubVolume(bBox, targetBufferObj.ptr());
		volData->finishEditing(editionId);

		targetBufferObj->unmap();

		targetBufferObj = nullptr;

		return editionId;
	}

	auto OivInterpolator::Impl::CvMatToImageView(cv::Mat& mat) -> ImageViewPtr {
		ImageViewPtr iv;

		VectorXu64 imageShape { static_cast<uint64_t>(mat.size[0]), static_cast<uint64_t>(mat.size[1]) };

		iv = ImageViewFactory::allocate(imageShape, DataTypeId::UINT8);
		setDimensionalInterpretation(iv, ImageTypeId::IMAGE);
		setImageInterpretation(iv, ImageInterpretation::BINARY);

		VectorXu64 imageOrig { 0, 0 };
		RegionXu64 imageRegion { imageOrig, imageShape };

		iv->writeRegion(imageRegion, mat.data);

		return iv;
	}

	auto OivInterpolator::Impl::ImageViewToCvMat(ImageViewPtr iv) -> cv::Mat {
		const auto shape = iv->shape();

		cv::Mat mat(shape[0], shape[1], CV_8UC1);
		memcpy(mat.data, iv->bufferReadOnly(), iv->bufferSize());

		return mat;
	}

	auto OivInterpolator::Impl::SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView> {
		std::shared_ptr<iolink::ImageView> imageView { nullptr };
		try {
			const auto dim = vol->getDimension();
			const auto resolution = vol->getVoxelSize();

			const auto x = dim[0];
			const auto y = dim[1];
			const auto z = dim[2];

			if (z < 2) {	// 2D			
				auto spatialCalibrationProperty = SpatialCalibrationProperty(
																			{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																			{ resolution[0], resolution[1], 1 }	// spacing
																			);

				const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
				const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
				imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
				setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

				const RegionXu64 imageRegion { { 0, 0 }, imageShape };
				imageView->writeRegion(imageRegion, vol->data.getValue());

			} else {	// 3D
				auto spatialCalibrationProperty = SpatialCalibrationProperty(
																			{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																			{ resolution[0], resolution[1], resolution[2] }	// spacing
																			);

				const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
				const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

				imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
				setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

				const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
				imageView->writeRegion(imageRegion, vol->data.getValue());
			}
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return imageView;
	}

	OivInterpolator::OivInterpolator() : d { new Impl } {
		d->interpolate = std::make_shared<CvInterpolate>();
	}

	OivInterpolator::~OivInterpolator() { }

	auto OivInterpolator::IsOverwrite(bool overwrite) -> void {
		d->isOverwrite = overwrite;
	}


	auto OivInterpolator::SetLabelValue(int label) -> void {
		d->label = label;
	}


	auto OivInterpolator::SetInputVolume(SoVolumeData* volData) -> void {
		d->volData = volData;
	}

	auto OivInterpolator::Perform() -> std::tuple<bool, int> {
		if (!d->volData) {
			return std::make_tuple(false, -1);
		}
		if (d->label < 1) {
			return std::make_tuple(false, -1);
		}
		auto inputView = d->SoVolumeToImageView(d->volData);
		if (!inputView) {
			return std::make_tuple(false, -1);
		}

		try {
			const auto dim = inputView->shape();
			const auto spacing = getCalibrationSpacing(inputView);

			const auto binaryIV = thresholdingByCriterion(inputView, ThresholdingByCriterion::EQUAL_TO, d->label);

			//find z-slice that exist slice information
			QMap<int, ImageViewPtr> basisBinaryIVs;
			for (auto i = 0; i < dim[2]; i++) {
				const auto binarySlice = getSliceFromVolume3d(binaryIV, GetSliceFromVolume3d::Z_AXIS, i);

				if (intensityIntegral2d(binarySlice)->intensityIntegral() > 0) {
					basisBinaryIVs.insert(i, binarySlice);
				}
			}

			if (basisBinaryIVs.count() < 2) {
				throw std::runtime_error("Failed to get base distance images");
			}

			auto basisIndexes = basisBinaryIVs.keys();
			std::sort(basisIndexes.begin(), basisIndexes.end());

			for (auto i = 0; i < basisIndexes.count() - 1; i++) {
				const auto lowerIndex = basisIndexes.at(i);
				const auto upperIndex = basisIndexes.at(i + 1);

				const auto lowerIV = basisBinaryIVs.value(lowerIndex);
				const auto upperIV = basisBinaryIVs.value(upperIndex);

				auto lowerMat = d->ImageViewToCvMat(lowerIV);
				auto upperMat = d->ImageViewToCvMat(upperIV);

				auto newSliceCount = upperIndex - lowerIndex - 1;
				for (auto newIndex = lowerIndex + 1; newIndex < upperIndex; newIndex++) {
					const double precision = (newIndex - lowerIndex) / static_cast<double>(newSliceCount + 1);
					auto newSliceMat = CvInterpolate::Interpolate(lowerMat, upperMat, precision);

					auto newSliceIV = d->CvMatToImageView(newSliceMat);

					// change label value to input value
					newSliceIV = convertImage(newSliceIV, ConvertImage::UNSIGNED_INTEGER_16_BIT);

					newSliceIV = imageFormula(newSliceIV, nullptr, nullptr,
											QString("if(I1 > 0, %1, 0)").arg(d->label).toStdString(),
											ImageFormula::OutputType::UNSIGNED_INTEGER_16_BIT
											);

					// paste a new slice into original slice
					if (false == d->isOverwrite) {
						const auto originSliceIV = getSliceFromVolume3d(inputView, GetSliceFromVolume3d::Z_AXIS, newIndex);
						newSliceIV = imageFormula(originSliceIV, newSliceIV, nullptr,
												"if(I2 > 0, I2, I1)",
												ImageFormula::OutputType::UNSIGNED_INTEGER_16_BIT
												);
					}
					inputView = setSliceToVolume3d(inputView, newSliceIV, SetSliceToVolume3d::Z_AXIS, newIndex);
				}
			}

			//Copy Modified data to original volumeData			
			//d->volData->data.setValue(d->volData->getDimension(), SbDataType::UNSIGNED_SHORT, 16, inputView->buffer(), SoSFArray::COPY);
			const auto editionID = d->ApplyModifiedVolumeWithHistory(inputView);
			return std::make_tuple(true, editionID);
		} catch (std::exception& e) {
			std::cout << e.what() << std::endl;
		}
		catch (Exception& e) {
			std::cout << e.what();
		}catch (...) {
			std::cout << "Unknown Error" << std::endl;
		}
		return std::make_tuple(false, -1);
	}

}
