#pragma once

#include <QString>

#include <Inventor/nodes/SoSwitch.h>

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API IVolumeGeneral {
    public:
        IVolumeGeneral();
        virtual ~IVolumeGeneral();

    protected:
        struct Impl {
            
        };
        std::unique_ptr<Impl> volume_d;
    };
}