#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include <IImage3Channel.h>
#include "IVolumeGeneral.h"

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API Volume3ChannelTF1D : public IImageGeneral, public IImageSingle, public IImage3Channel, public IVolumeGeneral {
    public:
        Volume3ChannelTF1D(const QString& name);
        ~Volume3ChannelTF1D() override;

        //For single scalar data
        auto SetDataMinMax(double min, double max) -> void override;
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;
        auto ToggleViz(bool show) -> void override;

        auto SetPredefinedColormap(int colormap_idx)->void;
        [[nodiscard]] auto GetPredefinedColormap()const->int;

        //For Multi channel data
        auto SetDataMinMax(int ch, double min, double max) -> void override;
        auto SetDataRange(int ch, double lower, double upper) -> void override;
        auto SetVolume(int ch, SoVolumeData* vol, bool singleVolume) -> void override;
        auto ToggleGamma(int ch, bool isGamma) -> void override;
        auto SetGamma(int ch, float gamma) -> void override;
        auto SetColor(int ch, QColor color) -> void override;
        auto SetTransparency(int ch, float transp) -> void override;
        auto ToggleViz(int ch, bool show) -> void override;

        //General
        auto Clear()->void;

    private:
        auto BuildSceneGraph()->void;
        auto CreateGammaCorrection(bool isGamma)->void;
        auto RestorePredefinedColormap()->void;
        auto CreateColormap(int ch)->void;
        [[nodiscard]] auto CheckUniformity(SoVolumeData* vol1, SoVolumeData* vol2)->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}