#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImage3Channel.h>
#include "IVolumeGeneral.h"
#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API Volume3Channel : public IImageGeneral, public IImage3Channel, public IVolumeGeneral {
    public:
        Volume3Channel(const QString& name);
        ~Volume3Channel();

        auto SetDataMinMax(int ch, double min, double max) -> void override;
        auto SetDataRange(int ch, double lower, double upper) -> void override;
        auto SetVolume(int ch, SoVolumeData* vol, bool singleVolume) -> void override;
        auto ToggleGamma(int ch, bool isGamma) -> void override;
        auto SetGamma(int ch, float gamma) -> void override;
        auto SetColor(int ch, QColor color) -> void override;
        auto SetTransparency(int ch, float transp) -> void override;
        auto ToggleViz(int ch, bool show) -> void override;

        auto Clear()->void;

    private:
        auto BuildSceneGraph()->void;
        auto CreateColormap(int ch)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}