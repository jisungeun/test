#pragma once

#include <memory>
#include <enum.h>
#include <QColor>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include "IVolumeGeneral.h"

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
	BETTER_ENUM(VRMode, int,
		DVR = 100,
		MIDA = 200,
		MIP = 300)			
	class TC_Rendering_Image_Volume_API VolumeMIP : public IImageGeneral, public IImageSingle, public IVolumeGeneral {
	public:
		VolumeMIP(const QString& name);
		~VolumeMIP();
		auto SetDataRange(double lower, double upper) -> void override;
		auto SetDataMinMax(double min, double max) -> void override;
		auto SetVolume(SoVolumeData* vol) -> void override;
		auto ToggleGamma(bool isGamma) -> void override;
		auto SetGamma(float gamma) -> void override;
		auto ToggleViz(bool show) -> void override;
				
		auto ToggleDepthEnhanced(bool isDepthEnhanced)->void;
		auto SetDepthEnhancementFactor(float factor)->void;
		auto ToggleGradientEnhanced(bool isGradientEnhanced)->void;
		auto SetGradientEnhancementFactor(float factor)->void;
		auto ToggleColorDepth(bool isColor)->void;
		auto ToggleViewDir(bool isViewDir)->void;
		auto SetVRMode(VRMode mode)->void;
		auto SetZRange(double min, double max)->void;
		auto SetXRange(double min, double max)->void;
		auto SetYRange(double min, double max)->void;
		auto SetColorMap(DepthColorMap idx)->void;
		auto SetColorMapArr(QList<float> colormapArr)->void;
		auto SetColorMapArr(float* colormapArr)->void;
		auto GetColorMapArr()->float*;
		auto Clear()->void;

		auto ToggleJittering(bool use)->void;
		auto ToggleDeferredLighting(bool use)->void;

	private:
		auto BuildSceneGraph()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}