#pragma once

#include <memory>

#include <QColor>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include "IVolumeGeneral.h"

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API VolumeTF1D : public IImageGeneral, public IImageSingle, public IVolumeGeneral {
    public:
        VolumeTF1D(const QString& name);
        ~VolumeTF1D();
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetDataMinMax(double min, double max) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;
        auto ToggleViz(bool show) -> void override;

        auto SetColormap(QList<QColor> colormap)->void;
        auto Clear()->void;

        auto ToggleJittering(bool use)->void;
        auto ToggleDeferredLighting(bool use)->void;

        auto SetXRange(double min, double max)->void;
        auto SetYRange(double min, double max)->void;
        auto SetZRange(double min, double max)->void;

    private:
        auto BuildSceneGraph()->void;
        auto SetDefaultColormap()->void;
        auto CreateColormap()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}