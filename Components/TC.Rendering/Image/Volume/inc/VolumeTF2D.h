#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include "IVolumeGeneral.h"
#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API VolumeTF2D :public IImageGeneral, public IImageSingle, public IVolumeGeneral {
    public:
        VolumeTF2D(const QString& name);
        ~VolumeTF2D();

        auto SetDataMinMax(double min, double max) -> void override;
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;        
        auto ToggleViz(bool show) -> void override;

        auto Clear()->void;

        auto ToggleJittering(bool use)->void;
        auto ToggleDeferredLighting(bool use)->void;

        auto SetXRange(double min, double max)->void;
        auto SetYRange(double min, double max)->void;
        auto SetZRange(double min, double max)->void;

        auto SetHiddenSep(SoSeparator* hidden)->void;
        auto BuildSceneGraph()->void;

    private:

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}