#pragma once

#include <QString>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image{
	class TC_Rendering_Image_Volume_API IVolumeSingle {
	public:
		IVolumeSingle();
		virtual ~IVolumeSingle();

		virtual auto SetDataRange(double lower, double upper)->void = 0;
	};
}