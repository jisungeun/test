#pragma once

#include <memory>
#include <QColor>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include <IVolumeGeneral.h>

#include "TC.Rendering.Image.VolumeExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Volume_API VolumeMIPTF2D : public IImageGeneral, public IImageSingle, public IVolumeGeneral {
    public:
        VolumeMIPTF2D(const QString& name);
        ~VolumeMIPTF2D();
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetDataMinMax(double min, double max) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;
        auto ToggleViz(bool show) -> void override;

        auto ToggleJittering(bool use)->void;
        auto ToggleDeferredLighting(bool use)->void;

        auto SetXRange(double min, double max)->void;
        auto SetYRange(double min, double max)->void;
        auto SetZRange(double min, double max)->void;

        auto ToggleDepthEnhanced(bool isDepthEnhanced)->void;
        auto SetDepthEnhancementFactor(float factor)->void;
        auto ToggleGradientEnhanced(bool isGradientEnhanced)->void;
        auto SetGradientEnhancementFactor(float factor)->void;
        auto ToggleColorTF(bool isColor)->void;
        auto ToggleOpacityTF(bool isOpacity)->void;
        auto ToggleViewDir(bool isViewDir)->void;

        auto SetHiddenSep(SoSeparator* hidden)->void;
        auto BuildSceneGraph()->void;

        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}