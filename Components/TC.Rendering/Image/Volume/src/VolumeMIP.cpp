#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoROI.h>
#include <Inventor/nodes/SoTransformProjection.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "VolumeMIP.h"

namespace Tomocube::Rendering::Image {
	struct VolumeMIP::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		SoRef<SoSeparator> root { nullptr };
		SoRef<SoDataRange> dataRange { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };
		SoRef<SoTransferFunction> depthTF { nullptr };
		SoRef<SoSwitch> volumeSocket { nullptr };
		SoRef<SoSwitch> projectSocket { nullptr };
		SoRef<SoFragmentShader> fragmentShader { nullptr };
		SoRef<SoVolumeRenderingQuality> shader { nullptr };
		SoRef<SoVolumeRender> volumeRender { nullptr };

		double depthMin { 0 };
		double depthMax { 1 };
		double xMinBound { 0 };
		double xMaxBound { 1 };
		double yMinBound { 0 };
		double yMaxBound { 1 };

		QString shaderPath;
		float currentColormapArr[256 * 4];
	};

	VolumeMIP::VolumeMIP(const QString& name) : IImageGeneral(), IImageSingle(), IVolumeGeneral(), d { new Impl } {
		general_d->name = name;
		d->shaderPath = QString("%1/shader/VolumeMIP.glsl").arg(qApp->applicationDirPath());
		BuildSceneGraph();
	}

	VolumeMIP::~VolumeMIP() { }

	auto VolumeMIP::SetGamma(float gamma) -> void {
		Q_UNUSED(gamma)
	}

	auto VolumeMIP::ToggleJittering(bool use) -> void {		
		d->shader->jittering = use;
	}

	auto VolumeMIP::ToggleDeferredLighting(bool use) -> void {		
		d->shader->deferredLighting = use;
	}

	auto VolumeMIP::ToggleGamma(bool isGamma) -> void {
		Q_UNUSED(isGamma)
	}

	auto VolumeMIP::SetZRange(double min, double max) -> void {
		d->depthMin = min;
		d->depthMax = max;
		d->fragmentShader->setShaderParameter1f("lowerBound", min);
		d->fragmentShader->setShaderParameter1f("upperBound", max);
	}

	auto VolumeMIP::SetXRange(double min, double max) -> void {
		d->xMinBound = min;
		d->xMaxBound = max;
		d->fragmentShader->setShaderParameter1f("xMinBound", min);
		d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}

	auto VolumeMIP::SetYRange(double min, double max) -> void {
		d->yMaxBound = min;
		d->yMaxBound = max;
		d->fragmentShader->setShaderParameter1f("yMinBound", min);
		d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}

	auto VolumeMIP::Clear() -> void {
		single_d.reset();
		volume_d.reset();
		d->dataRange->min = -1;
		d->dataRange->max = -1;
		d->volumeSocket->replaceChild(0, new SoSeparator);
		d->depthMin = 0;
		d->depthMax = 1;
	}

	auto VolumeMIP::SetDataMinMax(double min, double max) -> void {
		single_d->min = min;
		single_d->max = max;
		single_d->lower = min;
		single_d->lower = max;
		d->dataRange->min = min;
		d->dataRange->max = max;
	}

	auto VolumeMIP::SetDataRange(double lower, double upper) -> void {
		single_d->lower = lower;
		single_d->upper = upper;
		d->dataRange->min = lower;
		d->dataRange->max = upper;
	}

	auto VolumeMIP::ToggleViz(bool show) -> void {
		if (show && single_d->hasData) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
	}

	auto VolumeMIP::SetVolume(SoVolumeData* vol) -> void {
		if (single_d->min < 0 && single_d->max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			single_d->min = data_min;
			single_d->max = data_max;
			single_d->lower = data_min;
			single_d->upper = data_max;

			d->dataRange->min = data_min;
			d->dataRange->max = data_max;
		}
		d->volumeSocket->replaceChild(0, vol);
		single_d->hasData = true;
		d->fragmentShader->setShaderParameter1i("isHTExist", 1);
	}

	auto VolumeMIP::BuildSceneGraph() -> void {
		general_d->rootSwitch = new SoSwitch;
		general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
		general_d->rootSwitch->whichChild = 0;

		d->root = new SoSeparator;
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		general_d->rootSwitch->addChild(d->root.ptr());

		d->matl = new SoMaterial;
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->matl->ambientColor.setValue(1, 1, 1);
		d->matl->diffuseColor.setValue(1, 1, 1);

		d->dataRange = new SoDataRange;
		d->dataRange->dataRangeId = 1;
		d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

		d->volumeSocket = new SoSwitch;
		d->volumeSocket->whichChild = 0;
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->volumeSocket->addChild(new SoSeparator);

		d->transFunc = new SoTransferFunction;
		d->transFunc->transferFunctionId = 1;
		d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
		d->transFunc->colorMap.setNum(256 * 4);
		auto p = d->transFunc->colorMap.startEditing();
		const auto steps = d->transFunc->colorMap.getNum() / 4;
		for (auto i = 0; i < steps; i++) {
			const auto val = static_cast<float>(i) / static_cast<float>(steps);
			*p++ = val;
			*p++ = val;
			*p++ = val;
			*p++ = val;
		}
		d->transFunc->colorMap.finishEditing();
		d->depthTF = new SoTransferFunction;
		d->depthTF->transferFunctionId = 2;
		d->depthTF->setName((general_d->name + "_DepthTF").toStdString().c_str());
		d->depthTF->colorMap.setNum(512 * 4);
		d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("isHTExist", 0);
		d->fragmentShader->addShaderParameter1i("isViewDir", 0);
		d->fragmentShader->addShaderParameter1i("isDepthEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("depthEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1i("isGradientEnhanced", 0);
		d->fragmentShader->addShaderParameter1f("gradientEnhanceFactor", 0.5);
		d->fragmentShader->addShaderParameter1i("isColorDepth", 0);
		d->fragmentShader->addShaderParameter1f("lowerBound", 0);
		d->fragmentShader->addShaderParameter1f("upperBound", 1);
		d->fragmentShader->addShaderParameter1f("xMinBound", 0);
		d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
		d->fragmentShader->addShaderParameter1f("yMinBound", 0);
		d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

		d->shader = new SoVolumeRenderingQuality;
		d->shader->lighting = TRUE;
		d->shader->jittering = TRUE;
		d->shader->deferredLighting = FALSE;
		d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
		d->shader->forVolumeOnly = TRUE;
		d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

		d->volumeRender = new SoVolumeRender;
		d->volumeRender->setName((general_d->name + "_Render").toStdString().c_str());
		d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		d->root->addChild(d->matl.ptr());
		d->root->addChild(d->dataRange.ptr());
		d->root->addChild(d->volumeSocket.ptr());
		d->root->addChild(d->transFunc.ptr());
		d->root->addChild(d->depthTF.ptr());
		d->root->addChild(d->shader.ptr());
		d->projectSocket = new SoSwitch;
		SoRef<SoTransformProjection> proj = new SoTransformProjection;
		d->projectSocket->addChild(proj.ptr());
		d->projectSocket->whichChild = 0;
		d->root->addChild(d->projectSocket.ptr());
		d->root->addChild(d->volumeRender.ptr());
	}

	auto VolumeMIP::SetColorMapArr(QList<float> colormapArr) -> void {
		d->depthTF->actualColorMap.setNum(256 * 4);
		auto p = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			*p++ = colormapArr[i * 4];
			*p++ = colormapArr[i * 4 + 1];
			*p++ = colormapArr[i * 4 + 2];
			*p++ = colormapArr[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMIP::SetColorMapArr(float* colormapArr) -> void {
		d->depthTF->actualColorMap.setNum(256 * 4);
		auto p = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			*p++ = colormapArr[i * 4];
			*p++ = colormapArr[i * 4 + 1];
			*p++ = colormapArr[i * 4 + 2];
			*p++ = colormapArr[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMIP::GetColorMapArr() -> float* {
		return d->currentColormapArr;
	}

	auto VolumeMIP::SetColorMap(DepthColorMap idx) -> void {
		switch (idx) {
			case DepthColorMap::STANDARD:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
				break;
			case DepthColorMap::TEMPERATURE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::TEMPERATURE;
				break;
			case DepthColorMap::PHYSICS:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
				break;
			case DepthColorMap::GLOW:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
				break;
			case DepthColorMap::BLUE_RED:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_RED;
				break;
			case DepthColorMap::SEISMIC:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::SEISMIC;
				break;
			case DepthColorMap::BLUE_WHITE_RED:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_WHITE_RED;
				break;
			case DepthColorMap::REDTONE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::VOLREN_RED;
				break;
			case DepthColorMap::GREENTONE:
				d->depthTF->predefColorMap = SoTransferFunction::PredefColorMap::VOLREN_GREEN;
				break;
		}
		SoRef<SoTransferFunction> tempTF = new SoTransferFunction;
		tempTF->predefColorMap = d->depthTF->predefColorMap;
		const auto p = tempTF->actualColorMap.getValues(0);
		auto pp = d->depthTF->actualColorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			d->currentColormapArr[i * 4] = p[i * 4];
			d->currentColormapArr[i * 4 + 1] = p[i * 4 + 1];
			d->currentColormapArr[i * 4 + 2] = p[i * 4 + 2];
			d->currentColormapArr[i * 4 + 3] = p[i * 4 + 3];
			*pp++ = p[i * 4];
			*pp++ = p[i * 4 + 1];
			*pp++ = p[i * 4 + 2];
			*pp++ = p[i * 4 + 3];
		}
		d->depthTF->actualColorMap.finishEditing();
	}

	auto VolumeMIP::SetVRMode(VRMode mode) -> void {
		if (mode._to_integral() == VRMode::DVR) {
			d->projectSocket->whichChild = -1;
			d->volumeRender->renderMode = SoVolumeRender::RenderMode::VOLUME_RENDERING;
		} else if (mode._to_integral() == VRMode::MIDA) {
			d->projectSocket->whichChild = -1;
			//d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_DIFFERENCE_ACCUMULATION;
			d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		} else if (mode._to_integral() == VRMode::MIP) {
			d->projectSocket->whichChild = 0;
			d->volumeRender->renderMode = SoVolumeRender::RenderMode::MAX_INTENSITY_PROJECTION;
		}
	}

	auto VolumeMIP::ToggleDepthEnhanced(bool isDepthEnhanced) -> void {
		if (isDepthEnhanced) {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isDepthEnhanced", 0);
		}
	}

	auto VolumeMIP::SetDepthEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("depthEnhanceFactor", factor);
	}

	auto VolumeMIP::ToggleGradientEnhanced(bool isGradientEnhanced) -> void {
		if (isGradientEnhanced) {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isGradientEnhanced", 0);
		}
	}

	auto VolumeMIP::SetGradientEnhancementFactor(float factor) -> void {
		d->fragmentShader->setShaderParameter1f("gradientEnhanceFactor", factor);
	}

	auto VolumeMIP::ToggleColorDepth(bool isColor) -> void {
		if (isColor) {
			d->fragmentShader->setShaderParameter1i("isColorDepth", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isColorDepth", 0);
		}
	}

	auto VolumeMIP::ToggleViewDir(bool isViewDir) -> void {
		if (isViewDir) {
			d->fragmentShader->setShaderParameter1i("isViewDir", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isViewDir", 0);
		}
	}
}
