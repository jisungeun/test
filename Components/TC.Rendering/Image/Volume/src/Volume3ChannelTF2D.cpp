#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Medical/helpers/Medicalhelper.h>

#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeRender.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <SoTF2D.h>
#include "Volume3ChannelTF2D.h"

namespace Tomocube::Rendering::Image {
    struct Volume3ChannelTF2D::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;

        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoMaterial> matl{ nullptr };
        SoRef<SoMultiDataSeparator> mds{ nullptr };
        //Intensity
        SoRef<SoSwitch> volumeSocket{ nullptr };
        SoRef<SoSeparator> hiddenSep{ nullptr };
        SoRef<SoDataRange> dataRange{ nullptr };
        SoRef<SoRenderToTextureProperty> renderToTex{ nullptr };
        SoRef<SoTF2D> tf2D{ nullptr };
        //Channel
        SoRef<SoDataRange> chRange[3]{ nullptr,nullptr,nullptr };
        SoRef<SoTransferFunction> chTF[3]{ nullptr,nullptr,nullptr };
        SoRef<SoSwitch> chSocket[3]{ nullptr,nullptr,nullptr };

        //General
        SoRef<SoVolumeRenderingQuality> shader{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoVolumeRender> volumeRender{ nullptr };

        //
        QString shaderPath;
    };
    Volume3ChannelTF2D::Volume3ChannelTF2D(const QString& name) : IImageGeneral(), IImageSingle(), IImage3Channel(), IVolumeGeneral(), d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/Volume3ChannelTF2D.glsl").arg(qApp->applicationDirPath());
    }
    Volume3ChannelTF2D::~Volume3ChannelTF2D() {

    }
    auto Volume3ChannelTF2D::SetDataMinMax(double min, double max) -> void {
        single_d->min = min;
        single_d->max = max;
        single_d->lower = min;
        single_d->upper = max;

        d->dataRange->min = min;
        d->dataRange->max = max;
    }
    auto Volume3ChannelTF2D::SetDataRange(double lower, double upper) -> void {
        single_d->lower = lower;
        single_d->upper = upper;
        d->dataRange->min = lower;
        d->dataRange->max = upper;
    }
    auto Volume3ChannelTF2D::SetVolume(SoVolumeData* vol) -> void {
        auto isUniform = true;
        for (auto i = 0; i < 3; i++) {
            if (channel_d->hasData[i]) {
                const auto chVolume = reinterpret_cast<SoVolumeData*>(d->chSocket[i]->getChild(0));
                isUniform = CheckUniformity(vol, chVolume);
                break;
            }
        }
        if (isUniform) {
            d->shaderPath = QString("%1/shader/Volume3ChannelTF2D.glsl").arg(qApp->applicationDirPath());
        }
        else {
            d->shaderPath = QString("%1/shader/Volume3ChannelTF2DNonUniform.glsl").arg(qApp->applicationDirPath());
        }
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->setShaderParameter1i("isHTExist", 1);
        d->fragmentShader->touch();

        if (single_d->min < 0 && single_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            single_d->min = data_min;
            single_d->max = data_max;
            single_d->lower = data_min;
            single_d->upper = data_max;

            d->dataRange->min = data_min;
            d->dataRange->max = data_max;
        }
        vol->dataSetId = 1;
        d->volumeSocket->replaceChild(0, vol);
        single_d->hasData = true;
    }
    auto Volume3ChannelTF2D::ToggleGamma(bool isGamma) -> void {
        single_d->isGamma = isGamma;
    }
    auto Volume3ChannelTF2D::SetGamma(float gamma) -> void {
        single_d->gamma = gamma;
    }
    auto Volume3ChannelTF2D::SetHiddenSep(SoSeparator* hidden) -> void {
        d->hiddenSep = hidden;
    }
    auto Volume3ChannelTF2D::ToggleViz(bool show) -> void {
        if (show && single_d->hasData) {
            d->fragmentShader->setShaderParameter1i("isHTExist", 1);
        }
        else {
            d->fragmentShader->setShaderParameter1i("isHTExist", 0);
        }
    }
    ///////////////////////////FL////////////////////////////////////
    auto Volume3ChannelTF2D::ToggleViz(int ch, bool show) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        if (show && channel_d->hasData[ch]) {
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 1);
        }
        else {
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 0);
        }
    }

    auto Volume3ChannelTF2D::SetDataMinMax(int ch, double min, double max) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->min[ch] = min;
        channel_d->max[ch] = max;
        channel_d->lower[ch] = min;
        channel_d->upper[ch] = max;
        d->chRange[ch]->min = min;
        d->chRange[ch]->max = max;
    }
    auto Volume3ChannelTF2D::SetDataRange(int ch, double lower, double upper) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->lower[ch] = lower;
        channel_d->upper[ch] = upper;
        d->chRange[ch]->min = lower;
        d->chRange[ch]->max = upper;
    }
    auto Volume3ChannelTF2D::SetVolume(int ch, SoVolumeData* vol, bool singleVolume) -> void {
        if (ch < 0 || ch>2) {
            return;
        }
        auto isUniform = true;
        if (single_d->hasData) {
            const auto intensityVolume = reinterpret_cast<SoVolumeData*>(d->volumeSocket->getChild(0));
            isUniform = CheckUniformity(vol, intensityVolume);
        }
        if (isUniform) {
            d->shaderPath = QString("%1/shader/Volume3ChannelTF2D.glsl").arg(qApp->applicationDirPath());
        }
        else {
            d->shaderPath = QString("%1/shader/Volume3ChannelTF2DNonUniform.glsl").arg(qApp->applicationDirPath());
        }
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 1);
        d->fragmentShader->touch();
        if (channel_d->min[ch] < 0 && channel_d->max[ch] < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            channel_d->min[ch] = data_min;
            channel_d->max[ch] = data_max;
            channel_d->lower[ch] = data_min;
            channel_d->upper[ch] = data_max;

            d->chRange[ch]->min = data_min;
            d->chRange[ch]->max = data_max;
        }
        vol->dataSetId = ch + 2;
        d->chSocket[ch]->replaceChild(0, vol);
        channel_d->hasData[ch] = true;
    }
    auto Volume3ChannelTF2D::ToggleGamma(int ch, bool isGamma) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        const auto isChanged = channel_d->isGamma[ch] != isGamma;
        channel_d->isGamma[ch] = isGamma;
        if (isChanged) {
            CreateColormap(ch);
        }
    }
    auto Volume3ChannelTF2D::SetGamma(int ch, float gamma) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->gamma[ch] = gamma;
        if (channel_d->isGamma[ch]) {
            CreateColormap(ch);
        }
    }
    auto Volume3ChannelTF2D::CreateColormap(int ch) -> void {
        const auto tf = d->chTF[ch];
        auto gammaValue = channel_d->gamma[ch];
        if (false == channel_d->isGamma[ch]) {
            gammaValue = 1;
        }
        if (nullptr == tf.ptr()) {
            return;
        }
        const float R = channel_d->color[ch].redF();
        const float G = channel_d->color[ch].greenF();
        const float B = channel_d->color[ch].blueF();
        tf->colorMap.setNum(256 * 4);
        float* p = tf->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {
            const float factor = static_cast<float>(i) / 255.0f * (1.0f - channel_d->transp[ch]);
            const float gammaFactor = pow(static_cast<float>(i) / 255.0f, 1.f / gammaValue) * (1.0f - channel_d->transp[ch]);
            const auto mod_r = R * gammaFactor;
            const auto mod_g = G * gammaFactor;
            const auto mod_b = B * gammaFactor;
            mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
            mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
            mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
            *p++ = factor;
        }
        tf->colorMap.finishEditing();
    }
    auto Volume3ChannelTF2D::SetColor(int ch, QColor color) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->color[ch] = color;
        CreateColormap(ch);
    }
    auto Volume3ChannelTF2D::SetTransparency(int ch, float transp) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->transp[ch] = transp;
        CreateColormap(ch);
    }
    ///////////////////////////Genral////////////////////////////////////    
    auto Volume3ChannelTF2D::Clear() -> void {
        channel_d.reset();
        single_d.reset();        
        for (auto i = 0; i < 3; i++) {
            d->chRange[i]->min = -1;
            d->chRange[i]->max = -1;
            d->chSocket[i]->replaceChild(0, new SoSeparator);
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(i).toStdString(), 0);
        }
        d->volumeSocket->replaceChild(0, new SoSeparator);
        d->fragmentShader->setShaderParameter1i("isHTExist", 0);
    }
    auto Volume3ChannelTF2D::CheckUniformity(SoVolumeData* vol1, SoVolumeData* vol2) -> bool {
        auto AreSame = [&](float a, float b) {
            return fabs(a - b) < 0.0001;
        };
        auto dim1 = vol1->getDimension();
        auto dim2 = vol2->getDimension();
        SbVec3f ext1_min, ext1_max;
        SbVec3f ext2_min, ext2_max;
        vol1->extent.getValue().getBounds(ext1_min, ext1_max);
        vol2->extent.getValue().getBounds(ext2_min, ext2_max);

        for (auto i = 0; i < 3; i++) {
            if (dim1[i] != dim2[i]) {
                return false;
            }
        }
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(ext1_min[i], ext2_min[i])) {
                return false;
            }
            if (false == AreSame(ext1_max[i], ext2_max[i])) {
                return false;
            }
        }
        return true;
    }

    auto Volume3ChannelTF2D::BuildSceneGraph() -> void {
        if (nullptr == d->hiddenSep.ptr()) {
            return;
        }
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->matl = new SoMaterial;
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->matl->ambientColor.setValue(1, 1, 1);
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->mds = new SoMultiDataSeparator;
        d->mds->setName((general_d->name + "_Mds").toStdString().c_str());

        d->root->addChild(d->matl.ptr());
        d->root->addChild(d->mds.ptr());

        //for intensity data
        d->volumeSocket = new SoSwitch;
        d->volumeSocket->whichChild = 0;
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->volumeSocket->addChild(new SoSeparator);

        d->dataRange = new SoDataRange;
        d->dataRange->setName((general_d->name + "_dataRange").toStdString().c_str());
        d->dataRange->dataRangeId = 1;

        d->renderToTex = new SoRenderToTextureProperty;
        d->renderToTex->setName((general_d->name + "_renderTexture").toStdString().c_str());
        d->renderToTex->component = SoRenderToTextureProperty::RGB_ALPHA;
        d->renderToTex->size.setValue(SbVec2s(512, 512));
        d->renderToTex->updatePolicy = SoRenderToTextureProperty::WHEN_NEEDED;
        d->renderToTex->node.set1Value(0, (SoNode*)d->hiddenSep.ptr());

        d->tf2D = new SoTF2D(15);
        d->tf2D->setName((general_d->name + "_TF2D").toStdString().c_str());
        d->tf2D->model.setValue(SoTexture2::Model::REPLACE);
        d->tf2D->internalFormat.setValue(SoTF2D::RGBA_FORMAT);
        d->tf2D->minFilter = SoTF2D::NEAREST;
        d->tf2D->magFilter = SoTF2D::NEAREST;
        d->tf2D->renderToTextureProperty.setValue(d->renderToTex.ptr());

        d->mds->addChild(d->volumeSocket.ptr());
        d->mds->addChild(d->tf2D.ptr());
        d->mds->addChild(d->dataRange.ptr());

        //for channel data
        for (auto i = 0; i < 3; i++) {
            d->chRange[i] = new SoDataRange;
            d->chRange[i]->setName((general_d->name + "_Ch%1Range").arg(i).toStdString().c_str());
            d->chRange[i]->dataRangeId = i + 2;

            d->chSocket[i] = new SoSwitch;
            d->chSocket[i]->whichChild = 0;
            d->chSocket[i]->setName((general_d->name + "_Ch%1Socket").arg(i).toStdString().c_str());
            d->chSocket[i]->addChild(new SoSeparator);

            d->chTF[i] = new SoTransferFunction;
            d->chTF[i]->transferFunctionId = i + 2;
            d->chTF[i]->setName((general_d->name + "_Ch%1TF").arg(i).toStdString().c_str());
            CreateColormap(i);

            d->mds->addChild(d->chSocket[i].ptr());
            d->mds->addChild(d->chRange[i].ptr());
            d->mds->addChild(d->chTF[i].ptr());
        }

        d->shader = new SoVolumeRenderingQuality;
        d->shader->lighting = TRUE;
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());        

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", 15));
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1i("data2", 2);
        d->fragmentShader->addShaderParameter1i("data3", 3);
        d->fragmentShader->addShaderParameter1i("data4", 4);
        d->fragmentShader->addShaderParameter1i("isHTExist", 0);
        d->fragmentShader->addShaderParameter1i("isCh0Exist", 0);
        d->fragmentShader->addShaderParameter1i("isCh1Exist", 0);
        d->fragmentShader->addShaderParameter1i("isCh2Exist", 0);

        d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->volumeRender = new SoVolumeRender;
        d->volumeRender->setName((general_d->name + "_Volume").toStdString().c_str());

        d->mds->addChild(d->shader.ptr());
        d->mds->addChild(d->volumeRender.ptr());
    }
}