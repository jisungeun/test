#include <QApplication>
#include <QColor>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Medical/helpers/Medicalhelper.h>

#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeRender.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "Volume3Channel.h"

namespace Tomocube::Rendering::Image {
    struct Volume3Channel::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;

        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoMaterial> matl{ nullptr };
        SoRef<SoMultiDataSeparator> mds{ nullptr };
        SoRef<SoDataRange> chRange[3]{ nullptr,nullptr,nullptr };
        SoRef<SoTransferFunction> chTF[3]{ nullptr,nullptr,nullptr };
        SoRef<SoSwitch> chSocket[3]{ nullptr ,nullptr,nullptr };
        SoRef<SoVolumeRenderingQuality> shader{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoVolumeRender> volumeRender{ nullptr };

        QString shaderPath;
    };
    Volume3Channel::Volume3Channel(const QString& name) :IImageGeneral(), IImage3Channel(), IVolumeGeneral(), d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/Volume3Channel.glsl").arg(qApp->applicationDirPath());
        BuildSceneGraph();
    }
    Volume3Channel::~Volume3Channel() {

    }

    auto Volume3Channel::SetDataRange(int ch, double lower, double upper) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->lower[ch] = lower;
        channel_d->upper[ch] = upper;
        d->chRange[ch]->min = lower;
        d->chRange[ch]->max = upper;
    }

    auto Volume3Channel::Clear() -> void {
        channel_d.reset();
        volume_d.reset();
        for (auto i = 0; i < 3; i++) {
            d->chRange[i]->min = -1;
            d->chRange[i]->max = -1;
            d->chSocket[i]->replaceChild(0, new SoSeparator);
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(i).toStdString(), 0);
        }        
    }

    auto Volume3Channel::SetTransparency(int ch, float transp) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->transp[ch] = transp;
        CreateColormap(ch);
    }


    auto Volume3Channel::SetDataMinMax(int ch, double min, double max) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->min[ch] = min;
        channel_d->max[ch] = max;
        channel_d->lower[ch] = min;
        channel_d->upper[ch] = max;
        d->chRange[ch]->min = min;
        d->chRange[ch]->max = max;
    }

    auto Volume3Channel::ToggleGamma(int ch, bool isGamma) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        const auto isChanged = channel_d->isGamma[ch] != isGamma;
        channel_d->isGamma[ch] = isGamma;
        if (isChanged) {
            CreateColormap(ch);
        }
    }
    auto Volume3Channel::ToggleViz(int ch, bool show) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        if (show && channel_d->hasData[ch]) {
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 1);
        }
        else {
            d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 0);
        }
    }


    auto Volume3Channel::SetColor(int ch, QColor color) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->color[ch] = color;
        CreateColormap(ch);
    }

    auto Volume3Channel::SetGamma(int ch, float gamma) -> void {
        if (ch < 0 || ch > 2) {
            return;
        }
        channel_d->gamma[ch] = gamma;
        if (channel_d->isGamma[ch]) {
            CreateColormap(ch);
        }
    }

    auto Volume3Channel::SetVolume(int ch, SoVolumeData* vol, bool singleVolume) -> void {	
        if (ch < 0 || ch > 2) {
            return;
        }
        if (channel_d->min[ch] < 0 && channel_d->max[ch] < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            channel_d->min[ch] = data_min;
            channel_d->max[ch] = data_max;
            channel_d->lower[ch] = data_min;
            channel_d->upper[ch] = data_max;

            d->chRange[ch]->min = data_min;
            d->chRange[ch]->max = data_max;
        }
        vol->dataSetId = ch + 1;
        channel_d->hasData[ch] = true;
        d->chSocket[ch]->replaceChild(0, vol);
        d->fragmentShader->setShaderParameter1i(QString("isCh%1Exist").arg(ch).toStdString(), 1);
    }
    auto Volume3Channel::CreateColormap(int ch) -> void {
        const auto tf = d->chTF[ch];
        auto gammaValue = channel_d->gamma[ch];
        if (false == channel_d->isGamma[ch]) {
            gammaValue = 1;
        }
        if (nullptr == tf.ptr()) {
            return;
        }
        const float R = channel_d->color[ch].redF();
        const float G = channel_d->color[ch].greenF();
        const float B = channel_d->color[ch].blueF();
        tf->colorMap.setNum(256 * 4);
        float* p = tf->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {
            const float factor = static_cast<float>(i) / 255.0f * (1.0f - channel_d->transp[ch]);
            const float gammaFactor = pow(static_cast<float>(i) / 255.0f, 1.f / gammaValue) * (1.0f - channel_d->transp[ch]);
            const auto mod_r = R * gammaFactor;
            const auto mod_g = G * gammaFactor;
            const auto mod_b = B * gammaFactor;
            mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
            mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
            mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
            *p++ = factor;
        }
        tf->colorMap.finishEditing();
    }
    auto Volume3Channel::BuildSceneGraph()->void {
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->matl = new SoMaterial;
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->matl->ambientColor.setValue(1, 1, 1);
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->mds = new SoMultiDataSeparator;
        d->mds->setName((general_d->name + "_Mds").toStdString().c_str());

        d->root->addChild(d->matl.ptr());
        d->root->addChild(d->mds.ptr());

        for (auto i = 0; i < 3; i++) {
            d->chRange[i] = new SoDataRange;
            d->chRange[i]->setName((general_d->name + "_Ch%1Range").arg(i).toStdString().c_str());
            d->chRange[i]->dataRangeId = i + 1;

            d->chSocket[i] = new SoSwitch;
            d->chSocket[i]->whichChild = 0;
            d->chSocket[i]->setName((general_d->name + "_Ch%1Socket").arg(i).toStdString().c_str());
            d->chSocket[i]->addChild(new SoSeparator);

            d->chTF[i] = new SoTransferFunction;
            d->chTF[i]->setName((general_d->name + "_Ch%1TF").arg(i).toStdString().c_str());
            d->chTF[i]->transferFunctionId = i + 1;
            CreateColormap(i);

            d->mds->addChild(d->chSocket[i].ptr());
            d->mds->addChild(d->chRange[i].ptr());
            d->mds->addChild(d->chTF[i].ptr());
        }

        d->shader = new SoVolumeRenderingQuality;
        d->shader->lighting = TRUE;
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());        

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1i("data2", 2);
        d->fragmentShader->addShaderParameter1i("data3", 3);
        d->fragmentShader->addShaderParameter1i("isCh0Exist", 0);
        d->fragmentShader->addShaderParameter1i("isCh1Exist", 0);
        d->fragmentShader->addShaderParameter1i("isCh2Exist", 0);

        d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->volumeRender = new SoVolumeRender;
        d->volumeRender->setName((general_d->name + "_Volume").toStdString().c_str());

        d->mds->addChild(d->shader.ptr());
        d->mds->addChild(d->volumeRender.ptr());
    }        
}