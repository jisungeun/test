#include <QApplication>

#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include <SoTF2D.h>
#include "VolumeTF2D.h"

namespace Tomocube::Rendering::Image {
	struct VolumeTF2D::Impl {
		Impl() = default;
		Impl(const Impl & other) = default;
		SoRef<SoSeparator> root{ nullptr };
		SoRef<SoDataRange> dataRange{ nullptr };
		SoRef<SoMultiDataSeparator> mds{ nullptr };
		SoRef<SoMaterial> matl{ nullptr };
		SoRef<SoSwitch> volumeSocket{ nullptr };
		SoRef<SoSeparator> hiddenSep{ nullptr };
		SoRef<SoRenderToTextureProperty> renderToTex{ nullptr };
		SoRef<SoVolumeRenderingQuality> shader{ nullptr };
		SoRef<SoVolumeRender> volumeRender{ nullptr };
		SoRef<SoFragmentShader> fragmentShader{ nullptr };
		SoRef<SoTF2D> tf2D{ nullptr };

		QString shaderPath;
	};
	VolumeTF2D::VolumeTF2D(const QString& name) : IImageGeneral(), IImageSingle(), IVolumeGeneral(), d{ new Impl } {
		general_d->name = name;
		d->shaderPath = QString("%1/shader/VolumeTF2D.glsl").arg(qApp->applicationDirPath());
    }
	VolumeTF2D::~VolumeTF2D() {
        
    }
	auto VolumeTF2D::ToggleDeferredLighting(bool use) -> void {
		d->shader->deferredLighting = use;
	}
	auto VolumeTF2D::ToggleJittering(bool use) -> void {
		d->shader->jittering = use;
	}
	auto VolumeTF2D::SetXRange(double min, double max) -> void {
		d->fragmentShader->setShaderParameter1f("xMinBound", min);
		d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}
	auto VolumeTF2D::SetYRange(double min, double max) -> void {
		d->fragmentShader->setShaderParameter1f("yMinBound", min);
		d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}
	auto VolumeTF2D::SetZRange(double min, double max) -> void {
		d->fragmentShader->setShaderParameter1f("lowerBound", min);
		d->fragmentShader->setShaderParameter1f("upperBound", max);
	}
	auto VolumeTF2D::BuildSceneGraph() -> void {
        if(nullptr == d->hiddenSep.ptr()) {
			return;
        }
		general_d->rootSwitch = new SoSwitch;
		general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
		general_d->rootSwitch->whichChild = 0;

		d->root = new SoSeparator;
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		general_d->rootSwitch->addChild(d->root.ptr());

		d->mds = new SoMultiDataSeparator;

		d->matl = new SoMaterial;
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->matl->ambientColor.setValue(1, 1, 1);
		d->matl->diffuseColor.setValue(1, 1, 1);

		d->root->addChild(d->matl.ptr());
		d->root->addChild(d->mds.ptr());

		d->volumeSocket = new SoSwitch;
		d->volumeSocket->whichChild = 0;
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->volumeSocket->addChild(new SoSeparator);

		d->dataRange = new SoDataRange;
		d->dataRange->setName((general_d->name + "_Range").toStdString().c_str());
		d->dataRange->dataRangeId = 1;

		d->renderToTex = new SoRenderToTextureProperty;
		d->renderToTex->setName((general_d->name + "_renderTexture").toStdString().c_str());
		d->renderToTex->component = SoRenderToTextureProperty::RGB_ALPHA;
		d->renderToTex->size.setValue(SbVec2s(512, 512));
		d->renderToTex->updatePolicy = SoRenderToTextureProperty::WHEN_NEEDED;
		d->renderToTex->node.set1Value(0, (SoNode*)d->hiddenSep.ptr());

		d->tf2D = new SoTF2D(15);
		d->tf2D->setName((general_d->name + "_TF2D").toStdString().c_str());
		d->tf2D->model.setValue(SoTexture2::Model::REPLACE);
		d->tf2D->internalFormat.setValue(SoTF2D::RGBA_FORMAT);
		d->tf2D->minFilter = SoTF2D::NEAREST;
		d->tf2D->magFilter = SoTF2D::NEAREST;
		d->tf2D->renderToTextureProperty.setValue(d->renderToTex.ptr());

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
		d->fragmentShader->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", 15));
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("isHTExist", 0);
		d->fragmentShader->addShaderParameter1f("lowerBound", 0);
		d->fragmentShader->addShaderParameter1f("upperBound", 1);
		d->fragmentShader->addShaderParameter1f("xMinBound", 0);
		d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
		d->fragmentShader->addShaderParameter1f("yMinBound", 0);
		d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

		d->shader = new SoVolumeRenderingQuality;
		d->shader->lighting = TRUE;
		d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

		d->volumeRender = new SoVolumeRender;
		d->volumeRender->setName((general_d->name + "_Render").toStdString().c_str());

		d->mds->addChild(d->tf2D.ptr());
		d->mds->addChild(d->volumeSocket.ptr());
		d->mds->addChild(d->dataRange.ptr());
		d->mds->addChild(d->shader.ptr());
		d->mds->addChild(d->volumeRender.ptr());
    }
	auto VolumeTF2D::ToggleViz(bool show) -> void {
		if (show && single_d->hasData) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		}
		else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
    }
	auto VolumeTF2D::SetHiddenSep(SoSeparator* hidden) -> void {
		d->hiddenSep = hidden;
    }
	auto VolumeTF2D::SetDataRange(double lower, double upper) -> void {
		single_d->lower = lower;
		single_d->upper = upper;
    }
	auto VolumeTF2D::SetDataMinMax(double min, double max) -> void {
		single_d->min = min;
		single_d->max = max;
		single_d->lower = min;
		single_d->upper = max;
		d->dataRange->min = min;
		d->dataRange->max = max;
    }
	auto VolumeTF2D::SetGamma(float gamma) -> void {
		single_d->gamma = gamma;
    }
	auto VolumeTF2D::ToggleGamma(bool isGamma) -> void {
		single_d->isGamma = isGamma;
    }
	auto VolumeTF2D::SetVolume(SoVolumeData* vol) -> void {
		if (single_d->min < 0 && single_d->max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			single_d->min = data_min;
			single_d->max = data_max;
			single_d->lower = data_min;
			single_d->upper = data_max;

			d->dataRange->min = data_min;
			d->dataRange->max = data_max;
		}
		d->volumeSocket->replaceChild(0, vol);
		vol->dataSetId = 1;
		single_d->hasData = true;
		d->fragmentShader->setShaderParameter1i("isHTExist", 1);
    }
	auto VolumeTF2D::Clear() -> void {
		single_d.reset();
		volume_d.reset();
		d->volumeSocket->replaceChild(0, new SoSeparator);
		d->fragmentShader->setShaderParameter1i("isHTExist", 0);
    }
}