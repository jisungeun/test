#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "VolumeTF1D.h"

namespace Tomocube::Rendering::Image {
    struct VolumeTF1D::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;

        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoDataRange> dataRange{ nullptr };
        SoRef<SoMaterial> matl{ nullptr };
        SoRef<SoTransferFunction> transFunc{ nullptr };
        SoRef<SoSwitch> volumeSocket{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoVolumeRenderingQuality> shader{ nullptr };
        SoRef<SoVolumeRender> volumeRender{ nullptr };

        QList<QColor> colormap;
        QString shaderPath;
    };
    VolumeTF1D::VolumeTF1D(const QString& name) : IImageGeneral(), IImageSingle(), IVolumeGeneral(), d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/VolumeTF1D.glsl").arg(qApp->applicationDirPath());
        BuildSceneGraph();
        SetDefaultColormap();
    }
    VolumeTF1D::~VolumeTF1D() {
        
    }
    auto VolumeTF1D::ToggleDeferredLighting(bool use) -> void {
        d->shader->deferredLighting = use;
	}
    auto VolumeTF1D::ToggleJittering(bool use) -> void {
        d->shader->jittering = use;
	}
    auto VolumeTF1D::SetXRange(double min, double max) -> void {        
        d->fragmentShader->setShaderParameter1f("xMinBound", min);
        d->fragmentShader->setShaderParameter1f("xMaxBound", max);
	}
    auto VolumeTF1D::SetYRange(double min, double max) -> void {        
        d->fragmentShader->setShaderParameter1f("yMinBound", min);
        d->fragmentShader->setShaderParameter1f("yMaxBound", max);
	}
    auto VolumeTF1D::SetZRange(double min, double max) -> void {        
        d->fragmentShader->setShaderParameter1f("lowerBound", min);
        d->fragmentShader->setShaderParameter1f("upperBound", max);
	}
    auto VolumeTF1D::SetGamma(float gamma) -> void {
        single_d->gamma = gamma;
        if(single_d->isGamma) {
            CreateColormap();
        }
    }
    auto VolumeTF1D::ToggleGamma(bool isGamma) -> void {
        if(single_d->isGamma != isGamma) {
            single_d->isGamma = isGamma;
            CreateColormap();
            return;
        }
        single_d->isGamma = isGamma;        
    }
    auto VolumeTF1D::SetDefaultColormap() -> void {
        d->colormap.clear();
        for(auto i=0;i<256;i++) {
            QColor color(0, 0, 0,0);
            d->colormap.append(color);
        }
    }
    auto VolumeTF1D::SetColormap(QList<QColor> colormap) -> void {           
        if(256 != colormap.count()) {
            return;
        }
        d->colormap = colormap;
        CreateColormap();
    }
    auto VolumeTF1D::CreateColormap() -> void {
        auto actualGamma = single_d->gamma;
        if(false == single_d->isGamma) {
            actualGamma = 1;
        }
        d->transFunc->actualColorMap.setNum(256 * 4);
        auto p = d->transFunc->actualColorMap.startEditing();
        for(auto i=0;i<256;i++) {
            auto r = d->colormap[i].redF();
            auto g = d->colormap[i].greenF();
            auto b = d->colormap[i].blueF();
            auto a = d->colormap[i].alphaF();
            float mod_r = pow(r, 1.0 / actualGamma);
            float mod_g = pow(g, 1.0 / actualGamma);
            float mod_b = pow(b, 1.0 / actualGamma);
            mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
            mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
            mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
            *p++ = a;
        }
        d->transFunc->actualColorMap.finishEditing();
        d->transFunc->touch();
    }
    auto VolumeTF1D::Clear() -> void {
        single_d.reset();
        volume_d.reset();
        d->dataRange->min = -1;
        d->dataRange->max = -1;
        d->volumeSocket->replaceChild(0, new SoSeparator);
        SetDefaultColormap();
        CreateColormap();
    }
    auto VolumeTF1D::ToggleViz(bool show) -> void {
        if (show && single_d->hasData) {
            d->fragmentShader->setShaderParameter1i("isHTExist", 1);
        }
        else {
            d->fragmentShader->setShaderParameter1i("isHTExist", 0);
        }
    }
    auto VolumeTF1D::SetDataRange(double lower, double upper) -> void {
        single_d->lower = lower;
        single_d->upper = upper;
        d->dataRange->min = lower;
        d->dataRange->max = upper;
    }
    auto VolumeTF1D::SetDataMinMax(double min, double max) -> void {
        single_d->min = min;
        single_d->max = max;
        single_d->lower = min;
        single_d->lower = max;
        d->dataRange->min = min;
        d->dataRange->max = max;
    }
    auto VolumeTF1D::SetVolume(SoVolumeData* vol) -> void {
        if (single_d->min < 0 && single_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            single_d->min = data_min;
            single_d->max = data_max;
            single_d->lower = data_min;
            single_d->upper = data_max;

            d->dataRange->min = data_min;
            d->dataRange->max = data_max;
        }
        d->volumeSocket->replaceChild(0, vol);
        single_d->hasData = true;
        d->fragmentShader->setShaderParameter1i("isHTExist", 1);
    }
    auto VolumeTF1D::BuildSceneGraph() -> void {
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_VolumeSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->matl = new SoMaterial;
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->matl->ambientColor.setValue(1, 1, 1);
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->dataRange = new SoDataRange;
        d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

        d->volumeSocket = new SoSwitch;
        d->volumeSocket->whichChild = 0;
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->volumeSocket->addChild(new SoSeparator);

        d->transFunc = new SoTransferFunction;
        d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
        d->transFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1i("isHTExist", 0);
        d->fragmentShader->addShaderParameter1f("lowerBound", 0);
        d->fragmentShader->addShaderParameter1f("upperBound", 1);
        d->fragmentShader->addShaderParameter1f("xMinBound", 0);
        d->fragmentShader->addShaderParameter1f("xMaxBound", 1);
        d->fragmentShader->addShaderParameter1f("yMinBound", 0);
        d->fragmentShader->addShaderParameter1f("yMaxBound", 1);

        d->shader = new SoVolumeRenderingQuality;
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->shader->lighting = TRUE;
        d->shader->deferredLighting = FALSE;
        d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->volumeRender = new SoVolumeRender;
        d->volumeRender->setName((general_d->name + "_Render").toStdString().c_str());

        d->root->addChild(d->matl.ptr());
        d->root->addChild(d->dataRange.ptr());
        d->root->addChild(d->volumeSocket.ptr());
        d->root->addChild(d->transFunc.ptr());
        d->root->addChild(d->shader.ptr());
        d->root->addChild(d->volumeRender.ptr());
    }
}