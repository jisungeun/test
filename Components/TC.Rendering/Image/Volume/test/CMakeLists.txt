project(ImageVolumeTest)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES	
	src/ImageVolumeTest.cpp
)

add_executable(${PROJECT_NAME}	
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		${VIEWER_COMPONENT_LINK_LIBRARY}
		Qt5::Widgets
		Qt5::Gui
		Qt5::Core
		TC::Components::TCFIO
		TC::IO::OIV::Slice::Naive
		TC::IO::OIV::Volume::Naive
		TC::Rendering::Core::RenderWindow3D
		TC::Rendering::Image::Volume
		TC::Rendering::Widgets::TransferFunctionCanvas2D
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/Rendering/Image/TestCases") 	

