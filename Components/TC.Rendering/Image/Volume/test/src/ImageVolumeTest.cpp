#include <QFileDialog>
#include <QVBoxLayout>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

//Volume Scene
#include <VolumeTF1D.h>
#include <Volume3Channel.h>
#include <Volume3ChannelTF1D.h>

//with 2D TF
#include <VolumeTF2D.h>
#include <Volume3ChannelTF2D.h>
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>

#include <TCFMetaReader.h>
#include <OivHdf5Reader.h>
#include <OivXYReader.h>
#include <RenderWindowVolume.h>

using namespace Tomocube::Rendering::Image;

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", "F:/CellLibrary/20210614_ TCF 1.4.0/20210607.170006.233.HTFL 3D single-001/20210607.170006.233.HTFL 3D single-001.TCF", "TCF (*.tcf)");

	if(path.isEmpty()) {
		return EXIT_FAILURE;
	}

	QWidget* socket = new QWidget;
	socket->setMinimumSize(1000, 500);
	auto layout = new QHBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	socket->setLayout(layout);

	SoQt::init(socket);
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();
	OivXYReader::initClass();
	SoTF2D::initClass();
	
	auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
	auto metaInfo = metaReader->Read(path);

	auto renderWindow = new RenderWindow3D(nullptr);
	layout->addWidget(renderWindow);

	auto hiddenScene = new HiddenScene;
	auto canvas = new QTransferFunctionCanvas2D;
	layout->addWidget(canvas);
	canvas->setCurProjPath(qApp->applicationDirPath());
	hiddenScene->SetFilePath(path);
	hiddenScene->SetImageParentDir(qApp->applicationDirPath());
	canvas->setHidden(hiddenScene);

	SoRef<SoVolumeData> volData = new SoVolumeData;
	SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
	reader->setDataGroupPath("/Data/3D");
	reader->setTileName("000000");
	reader->setFilename(path.toStdString());
	volData->setReader(*reader, TRUE);

	SoRef<SoVolumeData> sliceData = new SoVolumeData;
	SoRef<OivXYReader> sliceReader = new OivXYReader;
	sliceReader->setDataGroupPath("/Data/3D");
	sliceReader->setTileName("000000");
	sliceReader->setIndex(metaInfo->data.data3D.sizeZ / 2);
	sliceReader->setFilename(path.toStdString());
	sliceData->setReader(*sliceReader, TRUE);

	SoRef<SoVolumeData> chData[3]{ nullptr,nullptr,nullptr };
	SoRef<OivHdf5Reader> chReader[3]{ nullptr,nullptr,nullptr };
	for (auto i = 0; i < 3; i++) {
		chData[i] = new SoVolumeData;
		chReader[i] = new OivHdf5Reader(true);
		chReader[i]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(i).toStdString());
		chReader[i]->setTileDimension(256);
		chReader[i]->setTileName("000000");		
		chReader[i]->setFilename(path.toStdString());
		chData[i]->setReader(*chReader[i], TRUE);				
	}

	{
		hiddenScene->SetHTMinMax(metaInfo->data.data3D.riMin * 10000.0, metaInfo->data.data3D.riMax * 10000.0);
		hiddenScene->Calc2DHistogram(sliceData.ptr(), false);
		canvas->setDataXdecimation(3);
		canvas->setDataXSingleStep(0.001);
		canvas->setDataXdivider(10000.0);
		canvas->setDataXname("RI");
		canvas->setDataXRange(metaInfo->data.data3D.riMin * 10000.0, metaInfo->data.data3D.riMax * 10000.0);

		canvas->setDataYdecimation(2);
		canvas->setDataYSingleStep(0.05);
		canvas->setDataYdivider(1.0);
		canvas->setDataYname("Gradient");

		double grad_min, grad_max;
		hiddenScene->GetGradMinMax(grad_min, grad_max);
		canvas->setDataYRange(grad_min, grad_max);
	}

	layout->setStretch(0, 1);
	layout->setStretch(1, 1);
#if 0
	auto volumeTF1D = new VolumeTF1D("Test");
	volumeTF1D->SetVolume(volData.ptr());
	renderWindow->setSceneGraph(volumeTF1D->GetRootSwitch());
#endif
#if 0
	auto volumeTF2D = new VolumeTF2D("Test");
	volumeTF2D->SetHiddenSep(hiddenScene->GetSceneGraph());
	volumeTF2D->BuildSceneGraph();
	volumeTF2D->SetVolume(volData.ptr());
	renderWindow->setSceneGraph(volumeTF2D->GetRootSwitch());
#endif
#if 0
	auto volume3Channel = new Volume3Channel("Test");
	for (auto i = 0; i < 3; i++) {
		volume3Channel->SetVolume(i, chData[i].ptr());
	}
	renderWindow->setSceneGraph(volume3Channel->GetRootSwitch());
#endif
#if 0
	auto volume3ChannelTF1D = new Volume3ChannelTF1D("Test");
	volume3ChannelTF1D->SetVolume(volData.ptr());
	for (auto i = 0; i < 3; i++) {
		volume3ChannelTF1D->SetVolume(i, chData[i].ptr());
	}
	renderWindow->setSceneGraph(volume3ChannelTF1D->GetRootSwitch());
#endif
	auto volume3ChannelTF2D = new Volume3ChannelTF2D("Test");
	volume3ChannelTF2D->SetHiddenSep(hiddenScene->GetSceneGraph());
	volume3ChannelTF2D->BuildSceneGraph();
	volume3ChannelTF2D->SetVolume(volData.ptr());
	for (auto i = 0; i < 3; i++) {
		volume3ChannelTF2D->SetVolume(i, chData[i].ptr(),false);
	}
	renderWindow->setSceneGraph(volume3ChannelTF2D->GetRootSwitch());

	renderWindow->viewall();

	SoQt::show(socket);
	SoQt::mainLoop();

	delete socket;

	SoTF2D::exitClass();
	OivXYReader::exitClass();
	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	SoQt::finish();

	return EXIT_SUCCESS;
}