//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;
uniform VVizDataSetId data4;

uniform int isHTExist;
uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);
	VVIZ_DATATYPE val1 = 0;
	VVIZ_DATATYPE val2 = 0;
	VVIZ_DATATYPE val3 = 0;
	vec4 clr1 = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 clr2 = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 clr3 = vec4(0.0, 0.0, 0.0, 0.0);

	if (isHTExist > 0) {
		vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);
		VVIZ_DATATYPE index1 = VVizGetData(data1, dataCoord1);		
		clrHT = VVizComputeVolumeRendering(index1, 1);		
		clrHT = mix(clrHT, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));
	}
	if (isCh0Exist > 0) {
		vec3 dataCoord2 = VVizTextureToTextureVec(data, data2, texCoord);
		val1 = VVizGetData(data2, dataCoord2);
		clr1 = VVizComputeVolumeRendering(val1, 2);
		clr1 = mix(clr1, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord2)));
	}
	if (isCh1Exist > 0)
	{
		vec3 dataCoord3 = VVizTextureToTextureVec(data, data3, texCoord);
		val2 = VVizGetData(data3, dataCoord3);
		clr2 = VVizComputeVolumeRendering(val2, 3);
		clr2 = mix(clr2, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord3)));
	}
	if (isCh2Exist > 0)
	{
		vec3 dataCoord4 = VVizTextureToTextureVec(data, data4, texCoord);
		val3 = VVizGetData(data4, dataCoord4);
		clr3 = VVizComputeVolumeRendering(val3, 4);
		clr3 = mix(clr3, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord4)));
	}
	// Combine color values but use highest alpha value  
	vec4 res;
	res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;
		
	res.r = max(res.r, clrHT.r);
	res.g = max(res.g, clrHT.g);
	res.b = max(res.b, clrHT.b);	

	res.a = max(clrHT.a, max(clr1.a, max(clr2.a, clr3.a)));

	return res;	
}
