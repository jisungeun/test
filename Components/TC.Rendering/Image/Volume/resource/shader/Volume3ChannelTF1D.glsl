//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;
uniform VVizDataSetId data4;

uniform int isHTExist;
uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{   
    vec3 texCoord = voxelInfoFront.texCoord;
    vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);
    VVIZ_DATATYPE val1, val2, val3;
    vec4 clr1, clr2, clr3;

    if (isHTExist > 0) {
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);        
        clrHT = VVizComputeVolumeRendering(index1, 1);        
    }    
    
    if(isCh0Exist > 0){
        val1 = VVizGetData(data2, voxelInfoFront.texCoord);
        clr1 = VVizComputeVolumeRendering(val1, 2);
    }
    else
    {
        val1 = 0.0;
        clr1 = vec4(0,0,0,0);     
    }
    if(isCh1Exist > 0)
    {
        val2 = VVizGetData(data3, voxelInfoFront.texCoord);
        clr2 = VVizComputeVolumeRendering(val2, 3);         
    }
    else
    {
        val2 = 0.0;
        clr2 = vec4(0, 0, 0, 0);
    }
    if(isCh2Exist > 0)
    {
        val3 = VVizGetData(data4, voxelInfoFront.texCoord);
        clr3 = VVizComputeVolumeRendering(val3, 4);         
    }
    else
    {
        val3 = 0.0;
        clr3 = vec4(0,0,0,0);
    }  
    
    // Combine color values but use highest alpha value  
    vec4 res;
    res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;  
    
    res.r = max(res.r,clrHT.r);
    res.g = max(res.g,clrHT.g);
    res.b = max(res.b,clrHT.b);    

    res.a = max(clrHT.a, max(clr1.a, max(clr2.a, clr3.a)));        

    return res;    
}