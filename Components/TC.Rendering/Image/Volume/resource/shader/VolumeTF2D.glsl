//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform sampler2D tex2D;
uniform int isHTExist;
uniform float upperBound;
uniform float lowerBound;
uniform float xMinBound;
uniform float xMaxBound;
uniform float yMinBound;
uniform float yMaxBound;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
    vec4 color = vec4(0.0,0.0,0.0,0.0);

    if(isHTExist > 0){
        vec3 texCoord = voxelInfoFront.texCoord;

        float x = texCoord[0];
        float y = texCoord[1];
        float depth = texCoord[2];

        if (depth < lowerBound || depth > upperBound) {
            color = vec4(0);
            return color;
        }

        if (x < xMinBound || x > xMaxBound) {
            color = vec4(0);
            return color;
        }

        if (y < yMinBound || y > yMaxBound) {
            color = vec4(0);
            return color;
        }

        //texture unit 1 -> data 1
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);

        vec3 normal, gradient;
        VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);

        float gradientValue = length(gradient);

        color = texture2D(tex2D, vec2(index1, gradientValue));
        color = VVizComputeVolumeRenderingLighting(color, normal, gradientValue);
    }

    return color;
}
