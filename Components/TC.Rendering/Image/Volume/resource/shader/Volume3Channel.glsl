//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{   	
	vec4 clr1 = vec4(0.0);
	vec4 clr2 = vec4(0.0);
	vec4 clr3 = vec4(0.0);
	if(isCh0Exist > 0){
		VVIZ_DATATYPE val1 = VVizGetData(data1, voxelInfoFront.texCoord);
		clr1 = VVizComputeVolumeRendering(val1, 1);
	}  
	if(isCh1Exist > 0)
	{
		VVIZ_DATATYPE val2 = VVizGetData(data2, voxelInfoFront.texCoord);
		clr2 = VVizComputeVolumeRendering(val2, 2);         
	}
  
	if(isCh2Exist > 0)
	{
		VVIZ_DATATYPE val3 = VVizGetData(data3, voxelInfoFront.texCoord);
		clr3 = VVizComputeVolumeRendering(val3, 3);         
	}	 
	// Combine color values but use highest alpha value  
	vec4 res;
	res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;  
	res.a = max(clr1.a,max(clr2.a,clr3.a));  

	return res;
}