//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform VVizDataSetId data1;
uniform sampler2D tex2D;

uniform int isHTExist;
uniform int isDepthEnhanced;
uniform int isGradientEnhanced;
uniform int isColorTF;
uniform int isOpacityTF;
uniform int isViewDir;
uniform float depthEnhanceFactor;
uniform float gradientEnhanceFactor;
uniform float upperBound;
uniform float lowerBound;
uniform float xMinBound;
uniform float xMaxBound;
uniform float yMinBound;
uniform float yMaxBound;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);

	if (isHTExist > 0) {
		vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);		
		VVIZ_DATATYPE index1 = VVizGetData(data1, dataCoord1);				
		clrHT = VVizTransferFunction(index1, 1);				
		clrHT = mix(clrHT, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));
				
		float x = dataCoord1[0];
		float y = dataCoord1[1];
		float depth = dataCoord1[2];				

		if (depth < lowerBound || depth > upperBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (x < xMinBound || x > xMaxBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (y < yMinBound || y > yMaxBound) {
			clrHT = vec4(0);
			return clrHT;
		}

		if (isViewDir > 0) {
			depth = VVizGetProjectedDepth(dataCoord1);
		}

		if (isDepthEnhanced > 0) {
			float dep = depth * depthEnhanceFactor;
			float r = clrHT.r * (1 + dep);
			float g = clrHT.g * (1 + dep);
			float b = clrHT.b * (1 + dep);
			float a = clrHT.a * (1 + dep);

			clrHT.r = min(r, 1);
			clrHT.g = min(g, 1);
			clrHT.b = min(b, 1);
			clrHT.a = min(a, 1);			
		}
		if (isGradientEnhanced > 0) {
			vec3 normal, gradient;
			VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);
			float gradientValue = length(gradient);
			clrHT.r = mix(clrHT.r, gradientValue, gradientEnhanceFactor);
			clrHT.g = mix(clrHT.g, gradientValue, gradientEnhanceFactor);
			clrHT.b = mix(clrHT.b, gradientValue, gradientEnhanceFactor);
			clrHT.a = mix(clrHT.a, gradientValue, gradientEnhanceFactor);
		}

		vec3 normal, gradient;
		VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);

		float gradientValue = length(gradient);

		vec4 dcol = texture2D(tex2D, vec2(index1, gradientValue));
		//dcol = VVizComputeVolumeRenderingLighting(dcol, normal, gradientValue);
		if (isColorTF > 0) {
			if (dcol.r + dcol.g + dcol.b > 0.2) {
				clrHT.r = clrHT.r * dcol.r;
				clrHT.g = clrHT.g * dcol.g;
				clrHT.b = clrHT.b * dcol.b;
			}
		}
		if (isOpacityTF > 0) {
			clrHT.a = dcol.a;
		}
	}

	return clrHT;	
}
