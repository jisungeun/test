#include <QFileDialog>
#include <QVBoxLayout>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

//Volume Scene
#include <VolumeTF1D.h>
#include <Volume3Channel.h>
#include <Volume3ChannelTF1D.h>

//with 2D TF
#include <VolumeTF2D.h>
#include <Volume3ChannelTF2D.h>
#include <VolumeMIP.h>
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>


#include <OivActivator.h>
#include <TCFMetaReader.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>
#include "TestWindow.h"
#include <RenderWindowVolume.h>

using namespace Tomocube::Rendering::Image;

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	QStringList OivKeys;
	OivKeys.append("License OpenInventor 10.12 1-Jan-0 0 HY:PKW=I>@C` \"APP-TOMOANALYSIS\"");
	OivKeys.append("License VolumeVizLDM 10.12 1-Jan-0 0 JDJ]=[<:?LB[ \"APP-TOMOANALYSIS\"");

	OivActivator::Activate(OivKeys);

	TestWindow* testwindow = new TestWindow;	
	testwindow->setMinimumSize(600, 800);
	testwindow->setWindowTitle("Default Volume Rendering Test");

	SoQt::init(testwindow);
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();
	OivLdmReader::initClass();
	SoTF2D::initClass();

	testwindow->BuildSceneGraph();
		
	SoQt::show(testwindow);
	SoQt::mainLoop();

	delete testwindow;

	SoTF2D::exitClass();
	OivLdmReader::exitClass();
	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	SoQt::finish();

	return EXIT_SUCCESS;
}