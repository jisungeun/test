#pragma once

#include <memory>
#include <QWidget>

#include <RenderWindowVolume.h>

class SoSeparator;

class TestWindow : public QWidget {
	Q_OBJECT
public:
	TestWindow(QWidget* parent = nullptr);
	~TestWindow();

	auto BuildSceneGraph()->void;

protected slots:
	void OnOpenTCF();
	void OnResetView();
	void OnToggleViewDir();
	void OnToggleDepth();
	void OnToggleDepthColor();
	void OnToggleGradient();
	void OnRenderMode(int idx);
	void OnColorMap(int idx);
	void OnDepthFactor(double value);
	void OnGradFactor(double value);
	void OnDataRangeMin(double value);
	void OnDataRangeMax(double value);
	void OnDepthMin(double value);
	void OnDepthMax(double value);
	void OnXMin(double value);
	void OnXMax(double value);
	void OnYMin(double value);
	void OnYMax(double value);
	void OnCustomColormapChanged();
	
private:	
	auto InitUI()->void;
	
	struct Impl;
	std::unique_ptr<Impl> d;
};