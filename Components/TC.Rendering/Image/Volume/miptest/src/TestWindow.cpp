#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

//Volume Scene
#include <VolumeTF1D.h>
#include <Volume3Channel.h>
#include <Volume3ChannelTF1D.h>

//with colormap
#include <ColorMap.h>
#include <QColormapCanvas.h>

//with 2D TF
#include <VolumeTF2D.h>
#include <Volume3ChannelTF2D.h>
#include <VolumeMIP.h>
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>

#include <TCFMetaReader.h>
#include <OivHdf5Reader.h>

#include <RenderWindowVolume.h>

#include "ui_TestWindow.h"
#include "TestWindow.h"

#include "OivLdmReader.h"

using namespace Tomocube::Rendering::Image;

struct TestWindow::Impl {
	Ui::Form ui;

	QString path;
	std::shared_ptr<RenderWindow3D> renderwindow { nullptr };
	std::shared_ptr<VolumeMIP> volumeLayer { nullptr };

	float offset { 0 };
	float divider { 1 };
	ColorMapPtr tf { nullptr };
	QColormapCanvas* colormapWidget { nullptr };
};

TestWindow::TestWindow(QWidget* parent) : QWidget(parent), d { new Impl } {
	d->ui.setupUi(this);
	d->tf = std::make_shared<ColorMap>();
	InitUI();
}

TestWindow::~TestWindow() { }

auto TestWindow::BuildSceneGraph() -> void {
	d->renderwindow = std::make_shared<RenderWindow3D>(nullptr);
	d->renderwindow->setGradientBackground(SbVec3f(0, 0, 0), SbVec3f(0, 0, 0));

	const auto layout = new QVBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	d->ui.RenderSocket->setLayout(layout);

	layout->addWidget(d->renderwindow.get());

	d->volumeLayer = std::make_shared<VolumeMIP>("Test");
	d->renderwindow->setSceneGraph(d->volumeLayer->GetRootSwitch());
	this->OnColorMap(0);
}

auto TestWindow::InitUI() -> void {
	d->ui.depthMin->setRange(0, 0.99);
	d->ui.depthMin->setDecimals(2);
	d->ui.depthMin->setSingleStep(0.01);
	d->ui.depthMin->setValue(0);
	d->ui.depthMax->setRange(0.01, 1);
	d->ui.depthMax->setDecimals(2);
	d->ui.depthMax->setSingleStep(0.01);
	d->ui.depthMax->setValue(1);

	d->ui.xcropMin->setRange(0, 0.99);
	d->ui.xcropMin->setDecimals(2);
	d->ui.xcropMin->setSingleStep(0.01);
	d->ui.xcropMin->setValue(0);
	d->ui.xcropMax->setRange(0.01, 1);
	d->ui.xcropMax->setDecimals(2);
	d->ui.xcropMax->setSingleStep(0.01);
	d->ui.xcropMax->setValue(1);

	d->ui.ycropMin->setRange(0, 0.99);
	d->ui.ycropMin->setDecimals(2);
	d->ui.ycropMin->setSingleStep(0.01);
	d->ui.ycropMin->setValue(0);
	d->ui.ycropMax->setRange(0.01, 1);
	d->ui.ycropMax->setDecimals(2);
	d->ui.ycropMax->setSingleStep(0.01);
	d->ui.ycropMax->setValue(1);

	connect(d->ui.depthMin, SIGNAL(valueChanged(double)), this, SLOT(OnDepthMin(double)));
	connect(d->ui.depthMax, SIGNAL(valueChanged(double)), this, SLOT(OnDepthMax(double)));
	connect(d->ui.xcropMin, SIGNAL(valueChanged(double)), this, SLOT(OnXMin(double)));
	connect(d->ui.xcropMax, SIGNAL(valueChanged(double)), this, SLOT(OnXMax(double)));
	connect(d->ui.ycropMin, SIGNAL(valueChanged(double)), this, SLOT(OnYMin(double)));
	connect(d->ui.ycropMax, SIGNAL(valueChanged(double)), this, SLOT(OnYMax(double)));

	d->ui.renderCombo->addItem("Max Intensity Projection");
	//d->ui.comboBox->addItem("Max Intensity Difference Accumulation");
	//d->ui.comboBox->addItem("Direct Volume Rendering");
	d->ui.renderCombo->setCurrentIndex(0);

	connect(d->ui.renderCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRenderMode(int)));

	d->ui.colorCombo->addItem("Standard");
	d->ui.colorCombo->addItem("Temperature");
	d->ui.colorCombo->addItem("Physics");
	d->ui.colorCombo->addItem("Glow");
	d->ui.colorCombo->addItem("Blue_Red");
	d->ui.colorCombo->addItem("Seismic");
	d->ui.colorCombo->addItem("Blue_White_Red");
	d->ui.colorCombo->addItem("RedTone");
	d->ui.colorCombo->addItem("GreenTone");
	d->ui.colorCombo->addItem("Custom");

	connect(d->ui.colorCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColorMap(int)));

	d->ui.minRI->setDecimals(4);
	d->ui.minRI->setSingleStep(0.0001);
	d->ui.maxRI->setDecimals(4);
	d->ui.maxRI->setSingleStep(0.0001);

	d->ui.minRI->setEnabled(false);
	d->ui.maxRI->setEnabled(false);

	connect(d->ui.minRI, SIGNAL(valueChanged(double)), this, SLOT(OnDataRangeMin(double)));
	connect(d->ui.maxRI, SIGNAL(valueChanged(double)), this, SLOT(OnDataRangeMax(double)));

	d->ui.depthFactor->setRange(0, 1);
	d->ui.depthFactor->setDecimals(2);
	d->ui.depthFactor->setValue(0.5);
	d->ui.depthFactor->setSingleStep(0.01);
	d->ui.depthFactor->setEnabled(false);

	connect(d->ui.depthFactor, SIGNAL(valueChanged(double)), this, SLOT(OnDepthFactor(double)));

	d->ui.gradFactor->setRange(0, 1);
	d->ui.gradFactor->setDecimals(2);
	d->ui.gradFactor->setValue(0.5);
	d->ui.gradFactor->setSingleStep(0.01);
	d->ui.gradFactor->setEnabled(false);

	connect(d->ui.gradFactor, SIGNAL(valueChanged(double)), this, SLOT(OnGradFactor(double)));

	connect(d->ui.gradientChk, SIGNAL(clicked()), this, SLOT(OnToggleGradient()));
	connect(d->ui.depthChk, SIGNAL(clicked()), this, SLOT(OnToggleDepth()));
	connect(d->ui.depColChk, SIGNAL(clicked()), this, SLOT(OnToggleDepthColor()));
	connect(d->ui.viewChk, SIGNAL(clicked()), this, SLOT(OnToggleViewDir()));

	connect(d->ui.openBtn, SIGNAL(clicked()), this, SLOT(OnOpenTCF()));
	connect(d->ui.resetBtn, SIGNAL(clicked()), this, SLOT(OnResetView()));

	const auto clayout = new QVBoxLayout;
	clayout->setContentsMargins(0, 0, 0, 0);
	clayout->setSpacing(0);
	d->ui.colormapSocket->setLayout(clayout);

	d->colormapWidget = new QColormapCanvas;
	clayout->addWidget(d->colormapWidget);
	d->colormapWidget->SetColorTransferFunction(d->tf);

	connect(d->colormapWidget, SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged()));
	d->colormapWidget->setEnabled(false);
}

void TestWindow::OnCustomColormapChanged() {
	d->volumeLayer->SetColorMapArr(d->tf->GetDataPointer());
}

void TestWindow::OnResetView() {
	d->renderwindow->viewZ();
}

void TestWindow::OnOpenTCF() {
	const auto prev = QSettings("Tomocube", "TomoAnalysis").value("test/MIPVolumeRender", QString()).toString();
	auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", prev, "TCF (*.tcf)");
	if (path.isEmpty()) {
		return;
	}
	QSettings("Tomocube", "TomoAnalysis").setValue("test/MIPVolumeRender", path);

	auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
	auto metaInfo = metaReader->Read(path);

	if (false == metaInfo->data.data3D.exist) {
		QMessageBox::warning(nullptr, "Warning", QString("%1\nhas no HT 3D modality!").arg(path));
		return;
	}

	SoRef<SoVolumeData> volData = new SoVolumeData;
	d->offset = 0;
	d->divider = 1;
	if (metaInfo->data.isLDM) {
		SoRef<OivLdmReader> reader = new OivLdmReader;
		reader->setDataGroupPath("/Data/3D");
		reader->setTileName("000000");
		reader->setTileDimension(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
		reader->setFilename(path.toStdString());
		volData->setReader(*reader, TRUE);

		if (metaInfo->data.data3D.scalarType[0] == 1) {
			d->offset = metaInfo->data.data3D.riMinList[0] * 10000.0;
			d->divider = 10;
		}

		d->volumeLayer->SetDataMinMax((metaInfo->data.data3D.riMinList[0] * 10000.0 - d->offset) / d->divider,
									(metaInfo->data.data3D.riMaxList[0] * 10000.0 - d->offset) / d->divider);
	} else {
		SoRef<OivHdf5Reader> reader = new OivHdf5Reader;
		reader->setDataGroupPath("/Data/3D");
		reader->setTileName("000000");
		reader->setFilename(path.toStdString());
		volData->setReader(*reader, TRUE);
	}

	d->ui.minRI->blockSignals(true);
	d->ui.minRI->setEnabled(true);
	d->ui.minRI->setValue(metaInfo->data.data3D.riMin);
	d->ui.minRI->blockSignals(false);

	d->ui.maxRI->setEnabled(true);
	d->ui.maxRI->setEnabled(true);
	d->ui.maxRI->setValue(metaInfo->data.data3D.riMax);
	d->ui.maxRI->blockSignals(false);

	d->volumeLayer->SetVolume(volData.ptr());
	d->renderwindow->viewZ();
}

void TestWindow::OnToggleDepth() {
	const auto isDepth = d->ui.depthChk->isChecked();
	d->volumeLayer->ToggleDepthEnhanced(isDepth);
	d->ui.depthFactor->setEnabled(isDepth);
}

void TestWindow::OnToggleDepthColor() {
	const auto isDepthColor = d->ui.depColChk->isChecked();
	d->volumeLayer->ToggleColorDepth(isDepthColor);
}

void TestWindow::OnToggleViewDir() {
	const auto isViewDir = d->ui.viewChk->isChecked();
	d->volumeLayer->ToggleViewDir(isViewDir);
}

void TestWindow::OnToggleGradient() {
	const auto isGrad = d->ui.gradientChk->isChecked();
	d->volumeLayer->ToggleGradientEnhanced(isGrad);
	d->ui.gradFactor->setEnabled(isGrad);
}

void TestWindow::OnColorMap(int idx) {
	if (idx == 9) {
		d->volumeLayer->SetColorMapArr(d->tf->GetDataPointer());
		d->colormapWidget->SetColorTransferFunction(d->tf);
		d->colormapWidget->setEnabled(true);
	} else {
		d->colormapWidget->setEnabled(false);
		if (idx == 0) {//Standard
			d->volumeLayer->SetColorMap(DepthColorMap::STANDARD);
		} else if (idx == 1) {
			d->volumeLayer->SetColorMap(DepthColorMap::TEMPERATURE);
		} else if (idx == 2) {
			d->volumeLayer->SetColorMap(DepthColorMap::PHYSICS);
		} else if (idx == 3) {
			d->volumeLayer->SetColorMap(DepthColorMap::GLOW);
		} else if (idx == 4) {
			d->volumeLayer->SetColorMap(DepthColorMap::BLUE_RED);
		} else if (idx == 5) {
			d->volumeLayer->SetColorMap(DepthColorMap::SEISMIC);
		} else if (idx == 6) {
			d->volumeLayer->SetColorMap(DepthColorMap::BLUE_WHITE_RED);
		} else if (idx == 7) {
			d->volumeLayer->SetColorMap(DepthColorMap::REDTONE);
		} else if (idx == 8) {
			d->volumeLayer->SetColorMap(DepthColorMap::GREENTONE);
		}
		auto tempTF = std::make_shared<ColorMap>();
		const auto predColorArr = d->volumeLayer->GetColorMapArr();
		for (auto i = 0; i < 256; i++) {
			tempTF->AddRGBAPoint(static_cast<double>(i / 255.0), i, predColorArr[i * 4], predColorArr[i * 4 + 1], predColorArr[i * 4 + 2], predColorArr[i * 4 + 3]);
		}
		d->colormapWidget->SetColorTransferFunction(tempTF);
	}
}


void TestWindow::OnRenderMode(int idx) {
	if (idx == 0) {// MIP
		d->volumeLayer->SetVRMode(VRMode::MIP);
	} else if (idx == 1) {//MIDA
		d->volumeLayer->SetVRMode(VRMode::MIDA);
	} else {//DVR
		d->volumeLayer->SetVRMode(VRMode::DVR);
	}
}

void TestWindow::OnGradFactor(double value) {
	d->volumeLayer->SetGradientEnhancementFactor(static_cast<float>(value));
}

void TestWindow::OnDepthFactor(double value) {
	d->volumeLayer->SetDepthEnhancementFactor(static_cast<float>(value));
}

void TestWindow::OnDataRangeMax(double value) {
	Q_UNUSED(value)
	const auto min = d->ui.minRI->value() * 10000.0;
	const auto max = d->ui.maxRI->value() * 10000.0;

	d->volumeLayer->SetDataRange((min - d->offset) / d->divider, (max - d->offset) / d->divider);
}

void TestWindow::OnDataRangeMin(double value) {
	Q_UNUSED(value)
	const auto min = d->ui.minRI->value() * 10000.0;
	const auto max = d->ui.maxRI->value() * 10000.0;

	d->volumeLayer->SetDataRange((min - d->offset) / d->divider, (max - d->offset) / d->divider);
}

void TestWindow::OnDepthMin(double value) {
	const auto min = d->ui.depthMin->value();
	const auto max = d->ui.depthMax->value();

	d->volumeLayer->SetZRange(min, max);
}

void TestWindow::OnDepthMax(double value) {
	const auto min = d->ui.depthMin->value();
	const auto max = d->ui.depthMax->value();

	d->volumeLayer->SetZRange(min, max);
}

void TestWindow::OnXMin(double value) {
	const auto min = d->ui.xcropMin->value();
	const auto max = d->ui.xcropMax->value();

	d->volumeLayer->SetXRange(min, max);
}

void TestWindow::OnXMax(double value) {
	const auto min = d->ui.xcropMin->value();
	const auto max = d->ui.xcropMax->value();

	d->volumeLayer->SetXRange(min, max);
}

void TestWindow::OnYMin(double value) {
	const auto min = d->ui.ycropMin->value();
	const auto max = d->ui.ycropMax->value();

	d->volumeLayer->SetYRange(min, max);
}

void TestWindow::OnYMax(double value) {
	const auto min = d->ui.ycropMin->value();
	const auto max = d->ui.ycropMax->value();

	d->volumeLayer->SetYRange(min, max);
}
