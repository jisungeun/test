#include "IImageGeneral.h"

namespace Tomocube::Rendering::Image {
    IImageGeneral::IImageGeneral() : general_d{ new Impl } {
        
    }
    IImageGeneral::~IImageGeneral() {
        
    }
    auto IImageGeneral::ToggleWhole(bool show) -> void {
        if (nullptr == general_d->rootSwitch.ptr()) {
            return;
        }
        if (show) {
            general_d->rootSwitch->whichChild = 0;
        }
        else {
            general_d->rootSwitch->whichChild = -1;
        }
    }
    auto IImageGeneral::GetRootSwitch() -> SoSwitch* {
        return general_d->rootSwitch.ptr();
    }
    auto IImageGeneral::GetName() const -> QString {
        return general_d->name;
    }
    auto IImageGeneral::SetMetaInfo(TC::IO::TCFMetaReader::Meta::Pointer meta) -> void {
        general_d->metaInfo = meta;
    }

}
