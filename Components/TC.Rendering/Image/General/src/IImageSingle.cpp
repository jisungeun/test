#include "IImageSingle.h"

namespace Tomocube::Rendering::Image {
    IImageSingle::IImageSingle() : single_d{ new Impl } {

    }
    IImageSingle::~IImageSingle() {

    }
    auto IImageSingle::Impl::Reset() -> void {
        min = -1;
        max = -1;
        lower = -1;
        upper = -1;
        isGamma = false;
        gamma = 1.0;
        hasData = false;
	}
    auto IImageSingle::ResetSingleImpl() const -> void {
        single_d->Reset();
	}
    auto IImageSingle::GetDataRange() const -> std::tuple<double, double> {
        return std::make_tuple(single_d->lower, single_d->upper);
    }
    auto IImageSingle::GetDataMinMax() const -> std::tuple<double, double> {
        return std::make_tuple(single_d->min, single_d->max);
    }
    auto IImageSingle::GetIsGamma() const -> bool {
        return single_d->isGamma;
    }
    auto IImageSingle::GetGamma() const -> float {
        return single_d->gamma;
    }
}