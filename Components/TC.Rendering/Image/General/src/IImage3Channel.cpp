#include "IImage3Channel.h"

namespace Tomocube::Rendering::Image {
    IImage3Channel::IImage3Channel() : channel_d{ new Impl } {

    }
    IImage3Channel::~IImage3Channel() {
		
	}
    auto IImage3Channel::Impl::Reset() -> void {
		for(auto i=0;i<3;i++) {
            min[i] = -1;
            max[i] = -1;
            lower[i] = -1;
            upper[i] = -1;
            transp[i] = 0;
            isGamma[i] = false;
            gamma[i] = 1;
            hasData[i] = false;
		}
        color[0] = QColor(0, 0, 255);
        color[1] = QColor(0, 255, 0);
        color[2] = QColor(255, 0, 0);
	}
    auto IImage3Channel::ResetChImpl() const -> void {
        channel_d->Reset();
	}
    auto IImage3Channel::GetDataMinMax(int ch) const -> std::tuple<double, double> {
        if (ch < 0 || ch >2) {
            return std::make_tuple(-1, -1);
        }
        return std::make_tuple(channel_d->min[ch], channel_d->max[ch]);
    }
    auto IImage3Channel::GetDataRange(int ch) const -> std::tuple<double, double> {
        if (ch < 0 || ch >2) {
            return std::make_tuple(-1, -1);
        }
        return std::make_tuple(channel_d->lower[ch], channel_d->upper[ch]);
    }
    auto IImage3Channel::GetIsGamma(int ch) const -> bool {
        if (ch < 0 || ch >2) {
            return false;
        }
        return channel_d->isGamma[ch];
    }
    auto IImage3Channel::GetGamma(int ch) const -> float {
        if (ch < 0 || ch >2) {
            return -1;
        }
        return channel_d->gamma[ch];
    }
    auto IImage3Channel::GetColor(int ch) const -> QColor {
        if (ch < 0 || ch >2) {
            return QColor();
        }
        return channel_d->color[ch];
    }
    auto IImage3Channel::GetTransparency(int ch) const -> float {
        if (ch < 0 || ch > 2) {
            return -1;
        }
        return channel_d->transp[ch];
    }
}