#pragma once

#include <QString>
#include <enum.h>
#include <Inventor/nodes/SoSwitch.h>
#include <TCFMetaReader.h>

#include "TC.Rendering.Image.GeneralExport.h"

namespace Tomocube::Rendering::Image {
    BETTER_ENUM(DepthColorMap, int,
        STANDARD = 100,
        TEMPERATURE = 200,
        PHYSICS = 300,
        GLOW = 400,
        BLUE_RED = 500,
        SEISMIC = 600,
        BLUE_WHITE_RED = 700,
        REDTONE = 800,
        GREENTONE = 900
    )
    class TC_Rendering_Image_General_API IImageGeneral {        
    public:
        IImageGeneral();
        virtual ~IImageGeneral();
        auto ToggleWhole(bool show)->void;
        auto SetMetaInfo(TC::IO::TCFMetaReader::Meta::Pointer meta)->void;

        [[nodiscard]] auto GetRootSwitch()->SoSwitch*;
        [[nodiscard]] auto GetName()const->QString;
    protected:
        struct Impl {
            QString name;
            SoRef<SoSwitch> rootSwitch{ nullptr };
            TC::IO::TCFMetaReader::Meta::Pointer metaInfo{ nullptr };
        };
        std::unique_ptr<Impl> general_d;
    };
}