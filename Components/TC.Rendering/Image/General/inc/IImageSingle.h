#pragma once

#include <QString>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "TC.Rendering.Image.GeneralExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_General_API IImageSingle {
    public:
        IImageSingle();
        virtual ~IImageSingle();

        virtual auto SetDataRange(double lower, double upper)->void = 0;
        virtual auto SetDataMinMax(double min, double max)->void = 0;
        virtual auto ToggleGamma(bool isGamma)->void = 0;
        virtual auto SetGamma(float gamma)->void = 0;
        virtual auto SetVolume(SoVolumeData* vol)->void = 0;
        virtual auto ToggleViz(bool show)->void = 0;

        [[nodiscard]] auto GetDataRange()const->std::tuple<double, double>;
        [[nodiscard]] auto GetDataMinMax()const->std::tuple<double, double>;
        [[nodiscard]] auto GetIsGamma()const->bool;
        [[nodiscard]] auto GetGamma()const->float;
        [[nodiscard]] auto ResetSingleImpl()const->void;
    protected:
        struct Impl {
            double min{ -1 };
            double max{ -1 };
            double lower{ -1 };
            double upper{ -1 };
            bool isGamma{ false };
            float gamma{ 1.0 };
            bool hasData{ false };
            auto Reset()->void;
        };
        std::unique_ptr<Impl> single_d;
    };
}