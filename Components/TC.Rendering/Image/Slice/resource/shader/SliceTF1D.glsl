//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VVizPipeline/FRAGMENT_GRADIENT_FUNCTION/vvizGradient_frag.h>

uniform int isHTExist;

uniform VVizDataSetId data1;

vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{	
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);
	if(isHTExist > 0){
		VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);            
        clrHT = VVizTransferFunction(index1, 1);
	}	

	return clrHT;
}