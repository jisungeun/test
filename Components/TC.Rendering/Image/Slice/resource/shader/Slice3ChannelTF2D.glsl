//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VVizPipeline/FRAGMENT_GRADIENT_FUNCTION/vvizGradient_frag.h>

uniform sampler2D tex2D;

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;
uniform VVizDataSetId data4;

uniform int isHTExist;
uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

// Implement VVizComputeFragmentColor for slice nodes
vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{	
	vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);
    VVIZ_DATATYPE val1 = 0.0;
    VVIZ_DATATYPE val2 = 0.0;
    VVIZ_DATATYPE val3 = 0.0;
    vec4 clr1 = vec4(0.0);
    vec4 clr2 = vec4(0.0);
    vec4 clr3 = vec4(0.0);
        
    if (isHTExist > 0) {        
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);
        vec3 gradient = VVizComputeGradient(data1, texCoord);
        vec3 normal = VVizComputeNormal(texCoord);
        float gradientValue = length(gradient);
        clrHT = texture2D(tex2D, vec2(index1, gradientValue));        
	}

    if (isCh0Exist > 0) {        
        val1 = VVizGetData(data2, texCoord);
        clr1 = VVizTransferFunction(val1, 2);        
    }    
    if (isCh1Exist > 0)
    {        
        val2 = VVizGetData(data3, texCoord);
        clr2 = VVizTransferFunction(val2, 3);        
    }    
    if (isCh2Exist > 0)
    {        
        val3 = VVizGetData(data4, texCoord);
        clr3 = VVizTransferFunction(val3, 4);        
    }
    
    vec4 res = vec4(0,0,0,0);
    res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;

    res.r = max(res.r, clrHT.r);
    res.g = max(res.g, clrHT.g);
    res.b = max(res.b, clrHT.b);

    res.a = max(clrHT.a, max(clr1.a, max(clr2.a, clr3.a)));
	
    return res;
}