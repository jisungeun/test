#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include "ISliceGeneral.h"
#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
	class TC_Rendering_Image_Slice_API SliceGeneral : public IImageGeneral, public IImageSingle, public ISliceGeneral {
	public:
		SliceGeneral(const QString& name);
		~SliceGeneral() override;
		auto SetDataRange(double lower, double upper) -> void override;
		auto SetDataMinMax(double min, double max) -> void override;
		auto SetVolume(SoVolumeData* vol) -> void override;
		auto ToggleGamma(bool isGamma) -> void override;
		auto SetGamma(float gamma) -> void override;
		auto SetSliceNumber(int sliceNumber, bool apply) -> void override;
		auto ChangeName(const QString& name) -> void override;
		auto ToggleViz(bool show) -> void override;

		auto SetSliceTransparency(float transp) -> void override;

		auto SetPhyZPos(double zPos, bool apply) -> void;
		auto SetGeometryZ(int dimZ, double resZ, double offset = 0) -> void;

		auto SetIsMask(bool isMask) -> void;
		auto SetShader(const QString& shaderPath) -> void;
		auto SetColormap(float* cm, bool isLabel, int maxNum = 256, int highlight = -1) -> void;
		auto SetLabelHighlight(QList<int> highlight) -> void;
		auto Clear() -> void;

	private:
		auto UpdateViz() -> void;
		auto BuildSceneGraph() -> void;
		auto CreateGammaCorrection(bool isGamma) -> void;
		auto RestorePredefinedColormap() -> void;
		auto SetLabelColorMap(float* cm, int maxNum, int highlight) -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
