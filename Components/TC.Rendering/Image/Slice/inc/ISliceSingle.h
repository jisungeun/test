#pragma once

#include <QString>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Slice_API ISliceSingle {
    public:
        ISliceSingle();
        virtual ~ISliceSingle();

        virtual auto SetDataRange(double lower, double upper)->void = 0;        
        virtual auto SetDataMinMax(double min, double max)->void = 0;        
        virtual auto ToggleGamma(bool isGamma)->void = 0;
        virtual auto SetGamma(float gamma)->void = 0;
        virtual auto SetSliceVolume(SoVolumeData* vol)->void = 0;
        virtual auto ToggleViz(bool show)->void = 0;

        [[nodiscard]] auto GetDataRange()const->std::tuple<double, double>;
        [[nodiscard]] auto GetDataMinMax()const->std::tuple<double, double>;
        [[nodiscard]] auto GetIsGamma()const->bool;
        [[nodiscard]] auto GetGamma()const->float;
    protected:
        struct Impl{
            double min{ -1 };
            double max{ -1 };
            double lower{ -1 };
            double upper{ -1 };
            bool isGamma{ false };
            float gamma{ 1.0 };
            bool hasData{ false };
        };
        std::unique_ptr<Impl> single_d;
    };
}