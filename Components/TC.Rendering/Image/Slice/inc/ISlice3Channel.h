#pragma once

#include <tuple>

#include <QColor>

#include <VolumeViz/nodes/SoVolumeData.h>

#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
	class TC_Rendering_Image_Slice_API ISlice3Channel {
	public:
		ISlice3Channel();
		virtual ~ISlice3Channel() = default;

		virtual auto SetDataMinMax(int ch, double min, double max)->void = 0;		
		virtual auto SetDataRange(int ch, double lower, double upper)->void = 0;
		virtual auto ToggleGamma(int ch, bool isGamma)->void = 0;		
		virtual auto SetGamma(int ch, float gamma)->void = 0;
		virtual auto SetColor(int ch, QColor color)->void = 0;
		virtual auto SetTransparency(int ch, float transp)->void = 0;
		virtual auto SetSliceVolume(int ch, SoVolumeData* vol)->void = 0;
		virtual auto ToggleViz(int ch, bool show)->void = 0;

	    [[nodiscard]] auto GetDataMinMax(int ch)const->std::tuple<double, double>;
		[[nodiscard]] auto GetDataRange(int ch)const->std::tuple<double, double>;
		[[nodiscard]] auto GetIsGamma(int ch)const->bool;
		[[nodiscard]] auto GetGamma(int ch)const->float;
		[[nodiscard]] auto GetColor(int ch)const->QColor;
		[[nodiscard]] auto GetTransparency(int ch)const->float;

	protected:
		struct Impl {
			double min[3]{ -1,-1,-1 };
			double max[3]{ -1,-1,-1 };
			double lower[3]{ -1,-1,-1 };
			double upper[3]{ -1,-1,-1 };
			QColor color[3]{ QColor(0,0,255), QColor(0,255,0),QColor(255,0,0) };
			float transp[3]{ 0,0,0 };
			bool isGamma[3]{ false ,false,false };
			float gamma[3]{ 1,1,1 };
			bool hasData[3]{ false,false,false };
		};
		std::unique_ptr<Impl> channel_d;
	};
}