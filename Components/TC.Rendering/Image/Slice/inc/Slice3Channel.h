#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImage3Channel.h>
#include "ISliceGeneral.h"
#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Slice_API Slice3Channel : public IImageGeneral,public IImage3Channel,public ISliceGeneral {
    public:
        Slice3Channel(const QString& name);
        ~Slice3Channel();
        
        auto SetDataMinMax(int ch, double min, double max) -> void override;
        auto SetDataRange(int ch, double lower, double upper) -> void override;        
        auto SetVolume(int ch, SoVolumeData* vol, bool singleVolume = true) -> void override;
        auto ToggleGamma(int ch, bool isGamma) -> void override;
        auto SetGamma(int ch, float gamma) -> void override;
        auto SetSliceNumber(int sliceNumber, bool apply) -> void override;
        auto SetColor(int ch, QColor color) -> void override;
        auto SetTransparency(int ch, float transp) -> void override;
        auto SetSliceTransparency(float transp) -> void override;
        auto ToggleViz(int ch, bool show) -> void override;
        auto ChangeName(const QString& name) -> void override;

        auto Clear()->void;
        auto Reset()->void;

    private:
        auto BuildSceneGraph()->void;
        auto CreateColormap(int ch)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}