#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include "ISliceGeneral.h"
#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Slice_API SliceTF2D :public IImageGeneral,public IImageSingle, public ISliceGeneral{
    public:
        SliceTF2D(const QString& name);
        ~SliceTF2D();

        auto SetDataMinMax(double min, double max) -> void override;
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;
        auto SetSliceNumber(int sliceNumber, bool apply) -> void override;
        auto SetSliceTransparency(float transp) -> void override;
        auto ToggleViz(bool show) -> void override;
        auto ChangeName(const QString& name) -> void override;
        auto Clear()->void;

        auto SetHiddenSep(SoSeparator* hidden)->void;
        auto BuildSceneGraph()->void;

    private:

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}