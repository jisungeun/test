#pragma once

#include <memory>

#include <IImageGeneral.h>
#include <IImageSingle.h>
#include <IImage3Channel.h>
#include "ISliceGeneral.h"

#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
    class TC_Rendering_Image_Slice_API Slice3ChannelTF2D : public IImageGeneral, public IImageSingle, public IImage3Channel, public ISliceGeneral {
    public:
        Slice3ChannelTF2D(const QString& name);
        ~Slice3ChannelTF2D() override;

        //For single scalar data
        auto SetDataMinMax(double min, double max) -> void override;
        auto SetDataRange(double lower, double upper) -> void override;
        auto SetVolume(SoVolumeData* vol) -> void override;
        auto ToggleGamma(bool isGamma) -> void override;
        auto SetGamma(float gamma) -> void override;
        auto ToggleViz(bool show) -> void override;
        
        //For Multi channel data
        auto SetDataMinMax(int ch, double min, double max) -> void override;
        auto SetDataRange(int ch, double lower, double upper) -> void override;
        auto SetVolume(int ch, SoVolumeData* vol, bool singleVolume) -> void override;
        auto ToggleGamma(int ch, bool isGamma) -> void override;
        auto SetGamma(int ch, float gamma) -> void override;
        auto SetColor(int ch, QColor color) -> void override;
        auto SetTransparency(int ch, float transp) -> void override;
        auto ToggleViz(int ch, bool show) -> void override;

        //General
        auto SetSliceTransparency(float transp) -> void override;
        auto SetSliceNumber(int sliceNumber, bool apply) -> void override;
        auto Clear()->void;
        auto SetHiddenSep(SoSeparator* hidden)->void;
        auto BuildSceneGraph()->void;
        auto ChangeName(const QString& name) -> void override;

    private:
        [[nodiscard]] auto CheckUniformity(SoVolumeData* vol1, SoVolumeData* vol2)->bool;
        auto CreateColormap(int ch)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}