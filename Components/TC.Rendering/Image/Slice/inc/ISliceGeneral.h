#pragma once

#include <QString>

#include <Inventor/nodes/SoSwitch.h>

#include "TC.Rendering.Image.SliceExport.h"

namespace Tomocube::Rendering::Image {
	class TC_Rendering_Image_Slice_API ISliceGeneral {
	public:
		ISliceGeneral();
		virtual ~ISliceGeneral();
		virtual auto SetSliceNumber(int sliceNumber, bool apply)->void = 0;
		virtual auto SetSliceTransparency(float transp)->void = 0;
		virtual auto ChangeName(const QString& name)->void = 0;
		[[nodiscard]] auto GetSliceNumber()const->int;
		[[nodiscard]] auto GetSliceTransparency()const->float;

	protected:
		struct Impl {
			QString name;
			int sliceNumber{ 0 };
			float transp{ 0.0 };
			SoRef<SoSwitch> rootSwitch{ nullptr };			
		};
		std::unique_ptr<Impl> slice_d;
	};
}