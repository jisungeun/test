#include "ISliceSingle.h"

namespace Tomocube::Rendering::Image {
    ISliceSingle::ISliceSingle() : single_d{ new Impl } {
        
    }
    ISliceSingle::~ISliceSingle() {
        
    }
    auto ISliceSingle::GetDataRange() const -> std::tuple<double, double> {
        return std::make_tuple(single_d->lower, single_d->upper);
    }
    auto ISliceSingle::GetDataMinMax() const -> std::tuple<double, double> {
        return std::make_tuple(single_d->min, single_d->max);
    }
    auto ISliceSingle::GetIsGamma() const -> bool {
        return single_d->isGamma;
    }
    auto ISliceSingle::GetGamma() const -> float {
        return single_d->gamma;
    }
}