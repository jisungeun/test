#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoTranslation.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "SliceTF1D.h"

namespace Tomocube::Rendering::Image {
    struct SliceTF1D::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;

        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoDataRange> dataRange{ nullptr };
        SoRef<SoMaterial> matl{ nullptr };
        SoRef<SoTransferFunction> transFunc{ nullptr };
        SoRef<SoSwitch> volumeSocket{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoVolumeShader> shader{ nullptr };
        SoRef<SoOrthoSlice> slice{ nullptr };
        SoRef<SoTranslation> sliceTrans{ nullptr };

        QString shaderPath;

        int colormapIdx{ 0 };
        SoTransferFunction::PredefColorMap color_map[5] = {
            SoTransferFunction::INTENSITY,
            SoTransferFunction::INTENSITY_REVERSED,
            SoTransferFunction::GLOW,
            SoTransferFunction::PHYSICS,
            SoTransferFunction::STANDARD
        };        
    };

    SliceTF1D::SliceTF1D(const QString& name) : IImageGeneral(), IImageSingle(),ISliceGeneral(), d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/SliceTF1D.glsl").arg(qApp->applicationDirPath());
        BuildSceneGraph();        
    }

    SliceTF1D::~SliceTF1D() {

    }

    auto SliceTF1D::SetGamma(float gamma) -> void {        
        single_d->gamma = gamma;
        if (single_d->isGamma) {
            CreateGammaCorrection(true);
        }
    }    
    auto SliceTF1D::ToggleGamma(bool isGamma) -> void {
        if (single_d->isGamma != isGamma) {
            CreateGammaCorrection(isGamma);
        }
        single_d->isGamma = isGamma;
    }    
    auto SliceTF1D::CreateGammaCorrection(bool isGamma) -> void {
        auto actualGamma = single_d->gamma;
        if (false == isGamma) {
            actualGamma = 1;
        }
        auto dummyTF = new SoTransferFunction;
        dummyTF->ref();
        dummyTF->predefColorMap = d->transFunc->predefColorMap.getValue();
        auto steps = d->transFunc->actualColorMap.getNum() / 4;
        d->transFunc->actualColorMap.setNum(256 * 4);
        auto p = d->transFunc->actualColorMap.startEditing();
        auto base = dummyTF->actualColorMap.getValues(0);
        for (auto i = 0; i < steps; i++) {
            auto r = *base++;
            auto g = *base++;
            auto b = *base++;
            *base++;
            float mod_r = pow(r, 1.0 / actualGamma);
            float mod_g = pow(g, 1.0 / actualGamma);
            float mod_b = pow(b, 1.0 / actualGamma);
            mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
            mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
            mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
            *p++ = 1;
        }
        dummyTF->unref();
        dummyTF = nullptr;
        d->transFunc->actualColorMap.finishEditing();
    }

    auto SliceTF1D::RestorePredefinedColormap() -> void {
        auto dummyTF = new SoTransferFunction;
        dummyTF->ref();
        dummyTF->predefColorMap = d->transFunc->predefColorMap.getValue();
        auto steps = d->transFunc->actualColorMap.getNum() / 4;
        d->transFunc->actualColorMap.setNum(256 * 4);
        auto p = d->transFunc->actualColorMap.startEditing();
        auto base = dummyTF->actualColorMap.getValues(0);        
        for (auto i = 0; i < steps; i++) {
            auto r = *base++;
            auto g = *base++;
            auto b = *base++;
            *base++;
            *p++ = r;
            *p++ = g;
            *p++ = b;
            *p++ = 1;
        }
        dummyTF->unref();
        dummyTF = nullptr;
        d->transFunc->actualColorMap.finishEditing();
    }

    auto SliceTF1D::Clear() -> void {
        single_d.reset();
        slice_d.reset();
        d->dataRange->min = -1;
        d->dataRange->max = -1;
        d->volumeSocket->replaceChild(0, new SoSeparator);
        d->transFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
        d->slice->sliceNumber = 0;
        d->colormapIdx = 0;
        d->fragmentShader->setShaderParameter1i("isHTExist", 0);
    }
    auto SliceTF1D::ToggleViz(bool show) -> void {
        if(show && single_d->hasData) {
            d->fragmentShader->setShaderParameter1i("isHTExist", 1);
        }else {
            d->fragmentShader->setShaderParameter1i("isHTExist", 0);
        }
    }
    auto SliceTF1D::SetSliceTransparency(float transp) -> void {
        slice_d->transp = transp;
        d->matl->transparency.setValue(transp);
	}
    auto SliceTF1D::SetPredefinedColormap(int colormap_idx) -> void {
        if (colormap_idx < 0 || colormap_idx > 4) {
            return;
        }
        d->transFunc->predefColorMap = d->color_map[colormap_idx];
        d->colormapIdx = colormap_idx;
        CreateGammaCorrection(single_d->isGamma);
    }
    auto SliceTF1D::GetPredefinedColormap() const -> int {
        return d->colormapIdx;
    }
    auto SliceTF1D::SetDataRange(double lower, double upper) -> void {
        single_d->lower = lower;
        single_d->upper = upper;
        d->dataRange->min = lower;
        d->dataRange->max = upper;
    }    
    auto SliceTF1D::SetDataMinMax(double min, double max) -> void {
        single_d->min = min;
        single_d->max = max;
        single_d->lower = min;
        single_d->upper = max;
        d->dataRange->min = min;
        d->dataRange->max = max;
    }
    auto SliceTF1D::SetVolume(SoVolumeData* vol) -> void {
        if (single_d->min < 0 && single_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            single_d->min = data_min;
            single_d->max = data_max;
            single_d->lower = data_min;
            single_d->upper = data_max;

            d->dataRange->min = data_min;
            d->dataRange->max = data_max;
        }
        d->volumeSocket->replaceChild(0, vol);
        single_d->hasData = true;
        d->fragmentShader->setShaderParameter1i("isHTExist", 1);
    }
    auto SliceTF1D::ChangeName(const QString& name) -> void {
        general_d->name = name;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
    }
    auto SliceTF1D::BuildSceneGraph() -> void {
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->sliceTrans = new SoTranslation;
        d->root->addChild(d->sliceTrans.ptr());

        d->matl = new SoMaterial;
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->matl->ambientColor.setValue(1, 1, 1);
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->dataRange = new SoDataRange;
        d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

        d->volumeSocket = new SoSwitch;
        d->volumeSocket->whichChild = 0;
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->volumeSocket->addChild(new SoSeparator);

        d->transFunc = new SoTransferFunction;
        d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
        d->transFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());        
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1i("isHTExist", 0);

        d->shader = new SoVolumeShader;
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->shader->forVolumeOnly = FALSE;
        d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->slice = new SoOrthoSlice;
        d->slice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
        d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
        d->slice->axis = MedicalHelper::AXIAL;
        
        d->root->addChild(d->matl.ptr());
        d->root->addChild(d->dataRange.ptr());
        d->root->addChild(d->volumeSocket.ptr());
        d->root->addChild(d->transFunc.ptr());
        d->root->addChild(d->shader.ptr());
        d->root->addChild(d->slice.ptr());
    }
    auto SliceTF1D::SetSliceNumber(int sliceNum, bool apply)->void {
        slice_d->sliceNumber = sliceNum;
        if (apply) {
            d->slice->sliceNumber = sliceNum;
        }
    }    
}