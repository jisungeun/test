#include <QApplication>

#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include <SoTF2D.h>
#include "SliceTF2D.h"

namespace Tomocube::Rendering::Image {
    struct SliceTF2D::Impl{
        Impl() = default;
        Impl(const Impl& other) = default;        
        SoRef<SoSeparator> root{ nullptr };
        SoRef<SoDataRange> dataRange{ nullptr };
        SoRef<SoMultiDataSeparator> mds{ nullptr };
        SoRef<SoMaterial> matl{ nullptr };
        SoRef<SoSwitch> volumeSocket{ nullptr };
        SoRef<SoSeparator> hiddenSep{ nullptr };
        SoRef<SoRenderToTextureProperty> renderToTex{ nullptr };
        SoRef<SoVolumeShader> shader{ nullptr };
        SoRef<SoOrthoSlice> slice{ nullptr };
        SoRef<SoFragmentShader> fragmentShader{ nullptr };
        SoRef<SoTF2D> tf2D{ nullptr };

        QString shaderPath;
    };
    SliceTF2D::SliceTF2D(const QString& name) : IImageGeneral(),IImageSingle(),ISliceGeneral(), d{ new Impl } {
        general_d->name = name;
        d->shaderPath = QString("%1/shader/SliceTF2D.glsl").arg(qApp->applicationDirPath());        
    }
    SliceTF2D::~SliceTF2D() {
        
    }
    auto SliceTF2D::ChangeName(const QString& name) -> void {
        general_d->name = name;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->dataRange->setName((general_d->name + "_Range").toStdString().c_str());
        d->renderToTex->setName((general_d->name + "_renderTexture").toStdString().c_str());
        d->tf2D->setName((general_d->name + "_TF2D").toStdString().c_str());
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
	}
    auto SliceTF2D::BuildSceneGraph() -> void {
        if(nullptr == d->hiddenSep.ptr()) {
            return;
        }
        general_d->rootSwitch = new SoSwitch;
        general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
        general_d->rootSwitch->whichChild = 0;

        d->root = new SoSeparator;
        d->root->setName((general_d->name + "_Root").toStdString().c_str());
        general_d->rootSwitch->addChild(d->root.ptr());

        d->mds = new SoMultiDataSeparator;

        d->matl = new SoMaterial;
        d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
        d->matl->ambientColor.setValue(1, 1, 1);
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->root->addChild(d->matl.ptr());
        d->root->addChild(d->mds.ptr());
                
        d->volumeSocket = new SoSwitch;
        d->volumeSocket->whichChild = 0;
        d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
        d->volumeSocket->addChild(new SoSeparator);

        d->dataRange = new SoDataRange;
        d->dataRange->setName((general_d->name + "_Range").toStdString().c_str());
        d->dataRange->dataRangeId = 1;

        d->renderToTex = new SoRenderToTextureProperty;
        d->renderToTex->setName((general_d->name + "_renderTexture").toStdString().c_str());
        d->renderToTex->component = SoRenderToTextureProperty::RGB_ALPHA;
        d->renderToTex->size.setValue(SbVec2s(512, 512));
        d->renderToTex->updatePolicy = SoRenderToTextureProperty::WHEN_NEEDED;
        d->renderToTex->node.set1Value(0, (SoNode*)d->hiddenSep.ptr());

        d->tf2D = new SoTF2D(15);
        d->tf2D->setName((general_d->name + "_TF2D").toStdString().c_str());
        d->tf2D->model.setValue(SoTexture2::Model::REPLACE);
        d->tf2D->internalFormat.setValue(SoTF2D::RGBA_FORMAT);
        d->tf2D->minFilter = SoTF2D::NEAREST;
        d->tf2D->magFilter = SoTF2D::NEAREST;
        d->tf2D->renderToTextureProperty.setValue(d->renderToTex.ptr());

        d->fragmentShader = new SoFragmentShader;
        d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
        d->fragmentShader->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", 15));
        d->fragmentShader->addShaderParameter1i("data1", 1);
        d->fragmentShader->addShaderParameter1i("isHTExist", 0);        

        d->shader = new SoVolumeShader;
        d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
        d->shader->forVolumeOnly = FALSE;
        d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

        d->slice = new SoOrthoSlice;
        d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
        d->slice->axis = SoOrthoSlice::Axis::Z;
        d->slice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;

        d->mds->addChild(d->tf2D.ptr());
        d->mds->addChild(d->volumeSocket.ptr());
        d->mds->addChild(d->dataRange.ptr());
        d->mds->addChild(d->shader.ptr());
        d->mds->addChild(d->slice.ptr());

    }
    auto SliceTF2D::ToggleViz(bool show) -> void {
        if(show && single_d->hasData) {
            d->fragmentShader->setShaderParameter1i("isHTExist", 1);
        }else {
            d->fragmentShader->setShaderParameter1i("isHTExist", 0);
        }
    }
    auto SliceTF2D::SetSliceTransparency(float transp) -> void {
        slice_d->transp = transp;
        d->matl->transparency = transp;
	}
    auto SliceTF2D::SetHiddenSep(SoSeparator* hidden) -> void {
        d->hiddenSep = hidden;
    }
    auto SliceTF2D::SetDataRange(double lower, double upper) -> void {
        single_d->lower = lower;
        single_d->upper = upper;
    }
    auto SliceTF2D::SetDataMinMax(double min, double max) -> void {
        single_d->min = min;
        single_d->max = max;
        single_d->lower = min;
        single_d->upper = max;
        d->dataRange->min = min;
        d->dataRange->max = max;
    }
    auto SliceTF2D::SetGamma(float gamma) -> void {
        single_d->gamma = gamma;
    }
    auto SliceTF2D::ToggleGamma(bool isGamma) -> void {
        single_d->isGamma = isGamma;
    }
    auto SliceTF2D::SetVolume(SoVolumeData* vol) -> void {
        if (single_d->min < 0 && single_d->max < 0) {
            double data_min, data_max;
            vol->getMinMax(data_min, data_max);
            single_d->min = data_min;
            single_d->max = data_max;
            single_d->lower = data_min;
            single_d->upper = data_max;

            d->dataRange->min = data_min;
            d->dataRange->max = data_max;
        }
        d->volumeSocket->replaceChild(0, vol);
        vol->dataSetId = 1;
        single_d->hasData = true;
        d->fragmentShader->setShaderParameter1i("isHTExist", 1);
    }
    auto SliceTF2D::SetSliceNumber(int sliceNumber, bool apply) -> void {
        slice_d->sliceNumber = sliceNumber;
        if(apply) {
            d->slice->sliceNumber = sliceNumber;
        }
    }
    auto SliceTF2D::Clear() -> void {
        single_d.reset();
        slice_d.reset();
        d->volumeSocket->replaceChild(0, new SoSeparator);        

        d->slice->sliceNumber = 0;
        d->fragmentShader->setShaderParameter1i("isHTExist", 0);
    }
}