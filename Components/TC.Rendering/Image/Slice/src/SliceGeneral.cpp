#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoTranslation.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "SliceGeneral.h"

namespace Tomocube::Rendering::Image {
	struct SliceGeneral::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		SoRef<SoSeparator> root { nullptr };
		SoRef<SoDataRange> dataRange { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };
		SoRef<SoSwitch> volumeSocket { nullptr };
		SoRef<SoFragmentShader> fragmentShader { nullptr };
		SoRef<SoVolumeShader> shader { nullptr };
		SoRef<SoOrthoSlice> slice { nullptr };
		SoRef<SoTranslation> sliceTrans { nullptr };

		float originalColormap[1024];
		bool isLabel{ false };

		int dimZ { 1 };
		double resZ { 1 };
		double offset { 0 };
		bool inRange { false };
		bool toggleViz { false };

		QString shaderPath;
		int maxNum { 256 };

		auto GetIndexZ(double pz, double spz, double originZ, double offset) -> int;
	};

	auto SliceGeneral::Impl::GetIndexZ(double pz, double spz, double originZ, double offset) -> int {
		//const auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));
		const auto iz = static_cast<int>(round((pz - originZ - offset) / spz));

		return iz;
	}

	SliceGeneral::SliceGeneral(const QString& name) : IImageGeneral(), IImageSingle(), ISliceGeneral(), d { new Impl } {
		general_d->name = name;
		d->shaderPath = QString("%1/shader/SliceTF1D.glsl").arg(qApp->applicationDirPath());
		for (auto i = 0; i < 1024; i++) {
			d->originalColormap[i] = 1;
		}
		BuildSceneGraph();
	}

	SliceGeneral::~SliceGeneral() { }

	auto SliceGeneral::SetGamma(float gamma) -> void {
		single_d->gamma = gamma;
		if (single_d->isGamma) {
			CreateGammaCorrection(true);
		}
	}

	auto SliceGeneral::ToggleGamma(bool isGamma) -> void {
		if (single_d->isGamma != isGamma) {
			CreateGammaCorrection(isGamma);
		}
		single_d->isGamma = isGamma;
	}

	auto SliceGeneral::SetIsMask(bool isMask) -> void {
		if (isMask) {
			d->slice->interpolation = SoOrthoSlice::NEAREST;
		} else {
			d->slice->interpolation = SoOrthoSlice::LINEAR;
		}
	}

	auto SliceGeneral::SetLabelColorMap(float* cm, int maxNum, int highlight) -> void {
		d->maxNum = maxNum;
		d->transFunc->colorMap.setNum((maxNum + 2) * 4);

		auto p = d->transFunc->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		if (highlight > 0 && highlight <= maxNum) {
			//build highlight colormap
			for (auto i = 0; i < maxNum; i++) {
				*p++ = cm[i % 12 * 4];
				*p++ = cm[i % 12 * 4 + 1];
				*p++ = cm[i % 12 * 4 + 2];
				if (i == highlight - 1)
					*p++ = 1;
				else
					*p++ = 0.3;
			}
		} else {
			//build naive colormap
			for (auto i = 0; i < maxNum; i++) {
				*p++ = cm[i % 12 * 4];
				*p++ = cm[i % 12 * 4 + 1];
				*p++ = cm[i % 12 * 4 + 2];
				*p++ = 1;
			}
		}
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		d->transFunc->colorMap.finishEditing();
	}

	auto SliceGeneral::SetShader(const QString& shaderPath) -> void {
		if(d->fragmentShader) {
			d->shaderPath = shaderPath;
			d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
			d->fragmentShader->touch();
		}
	}

	auto SliceGeneral::SetLabelHighlight(QList<int> highlight) -> void {
		const auto* cm = d->originalColormap;
		const auto maxNum = d->maxNum;
		auto p = d->transFunc->colorMap.startEditing();
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		for (auto i = 0; i < maxNum; i++) {
			*p++ = cm[i % 12 * 4];
			*p++ = cm[i % 12 * 4 + 1];
			*p++ = cm[i % 12 * 4 + 2];
			if (highlight.contains(i + 1))
				*p++ = 1;
			else
				*p++ = 0.25;
		}
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		*p++ = 0;
		d->transFunc->colorMap.finishEditing();
	}

	
	auto SliceGeneral::SetColormap(float* cm, bool isLabel, int maxNum, int highlight) -> void {
		for (auto i = 0; i < 1024; i++) {
			d->originalColormap[i] = cm[i];
		}
		d->isLabel = isLabel;
		if (isLabel) {
			SetLabelColorMap(cm, maxNum, highlight);
			return;
		}
		d->transFunc->colorMap.setNum(256 * 4);
		auto p = d->transFunc->colorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			*p++ = cm[i * 4];
			*p++ = cm[i * 4 + 1];
			*p++ = cm[i * 4 + 2];
			*p++ = cm[i * 4 + 3];
		}
		d->transFunc->colorMap.finishEditing();
	}

	auto SliceGeneral::CreateGammaCorrection(bool isGamma) -> void {
		auto actualGamma = single_d->gamma;
		if (false == isGamma) {
			actualGamma = 1;
		}
		d->transFunc->colorMap.setNum(256 * 4);

		auto p = d->transFunc->colorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			const auto r = d->originalColormap[i * 4];
			const auto g = d->originalColormap[i * 4 + 1];
			const auto b = d->originalColormap[i * 4 + 2];
			const auto a = d->originalColormap[i * 4 + 3];
			const float mod_r = pow(r, actualGamma);
			const float mod_g = pow(g, actualGamma);
			const float mod_b = pow(b, actualGamma);
			const float mod_a = pow(a, actualGamma);
			*p++ = mod_r;
			*p++ = mod_g;
			*p++ = mod_b;
			*p++ = mod_a;
		}
		d->transFunc->colorMap.finishEditing();
	}

	auto SliceGeneral::RestorePredefinedColormap() -> void {
		d->transFunc->colorMap.setNum(256 * 4);
		auto p = d->transFunc->colorMap.startEditing();
		for (auto i = 0; i < 256; i++) {
			const auto r = d->originalColormap[i * 4];
			const auto g = d->originalColormap[i * 4 + 1];
			const auto b = d->originalColormap[i * 4 + 2];
			const auto a = d->originalColormap[i * 4 + 3];
			*p++ = r;
			*p++ = g;
			*p++ = b;
			*p++ = a;
		}
		d->transFunc->colorMap.finishEditing();
	}

	auto SliceGeneral::Clear() -> void {
		single_d.reset();
		slice_d.reset();
		d->dataRange->min = -1;
		d->dataRange->max = -1;
		d->volumeSocket->replaceChild(0, new SoSeparator);
		d->transFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
		d->slice->sliceNumber = 0;
		d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		d->dimZ = 1;
		d->resZ = 1;
		d->offset = 0;
		d->inRange = false;
	}

	auto SliceGeneral::ToggleViz(bool show) -> void {
		d->toggleViz = show;
		if (show && single_d->hasData && d->inRange) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
	}

	auto SliceGeneral::UpdateViz() -> void {
		if (d->toggleViz && single_d->hasData && d->inRange) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
	}

	auto SliceGeneral::SetSliceTransparency(float transp) -> void {
		slice_d->transp = transp;
		d->matl->transparency.setValue(transp);
	}

	auto SliceGeneral::SetDataRange(double lower, double upper) -> void {
		single_d->lower = lower;
		single_d->upper = upper;
		d->dataRange->min = lower;
		d->dataRange->max = upper;
	}

	auto SliceGeneral::SetDataMinMax(double min, double max) -> void {
		single_d->min = min;
		single_d->max = max;
		single_d->lower = min;
		single_d->upper = max;
		d->dataRange->min = min;
		d->dataRange->max = max;
	}

	auto SliceGeneral::SetVolume(SoVolumeData* vol) -> void {
		if (single_d->min < 0 && single_d->max < 0) {
			double data_min, data_max;
			vol->getMinMax(data_min, data_max);
			single_d->min = data_min;
			single_d->max = data_max;
			single_d->lower = data_min;
			single_d->upper = data_max;

			d->dataRange->min = data_min;
			d->dataRange->max = data_max;
		}
		d->volumeSocket->replaceChild(0, vol);
		single_d->hasData = true;
		d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		d->inRange = true;
		d->toggleViz = true;
	}

	auto SliceGeneral::ChangeName(const QString& name) -> void {
		general_d->name = name;
		general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
		d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
		d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
	}

	auto SliceGeneral::BuildSceneGraph() -> void {
		general_d->rootSwitch = new SoSwitch;
		general_d->rootSwitch->setName((general_d->name + "_SliceSW").toStdString().c_str());
		general_d->rootSwitch->whichChild = 0;

		d->root = new SoSeparator;
		d->root->setName((general_d->name + "_Root").toStdString().c_str());
		general_d->rootSwitch->addChild(d->root.ptr());

		d->sliceTrans = new SoTranslation;
		d->root->addChild(d->sliceTrans.ptr());

		d->matl = new SoMaterial;
		d->matl->setName((general_d->name + "_Matl").toStdString().c_str());
		d->matl->ambientColor.setValue(1, 1, 1);
		d->matl->diffuseColor.setValue(1, 1, 1);

		d->dataRange = new SoDataRange;
		d->dataRange->setName((general_d->name + "_DataRange").toStdString().c_str());

		d->volumeSocket = new SoSwitch;
		d->volumeSocket->whichChild = 0;
		d->volumeSocket->setName((general_d->name + "_Socket").toStdString().c_str());
		d->volumeSocket->addChild(new SoSeparator);

		d->transFunc = new SoTransferFunction;
		d->transFunc->setName((general_d->name + "_TF").toStdString().c_str());
		d->transFunc->predefColorMap = SoTransferFunction::PredefColorMap::NONE;

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->shaderPath.toStdString());
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("isHTExist", 0);

		d->shader = new SoVolumeShader;
		d->shader->setName((general_d->name + "_Shader").toStdString().c_str());
		d->shader->forVolumeOnly = FALSE;
		d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());

		d->slice = new SoOrthoSlice;
		d->slice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->slice->setName((general_d->name + "_Slice").toStdString().c_str());
		d->slice->axis = MedicalHelper::AXIAL;

		d->root->addChild(d->matl.ptr());
		d->root->addChild(d->dataRange.ptr());
		d->root->addChild(d->volumeSocket.ptr());
		d->root->addChild(d->transFunc.ptr());
		d->root->addChild(d->shader.ptr());
		d->root->addChild(d->slice.ptr());
	}

	auto SliceGeneral::SetSliceNumber(int sliceNum, bool apply) -> void {
		if (d->dimZ) {
			return;
		}
		if (sliceNum < 0 || sliceNum > d->dimZ - 1) {
			d->inRange = false;
			UpdateViz();
			return;
		}
		d->inRange = true;
		UpdateViz();
		slice_d->sliceNumber = sliceNum;
		if (apply) {
			d->slice->sliceNumber = sliceNum;
		}
	}

	auto SliceGeneral::SetPhyZPos(double zPos, bool apply) -> void {
		if (d->dimZ == 1) {
			return;
		}

		const auto z = d->GetIndexZ(zPos, d->resZ, -d->dimZ * d->resZ / 2, d->offset);
		if (z < 0 || z > d->dimZ - 1) {
			d->inRange = false;
			UpdateViz();
			return;
		}
		d->inRange = true;
		UpdateViz();
		slice_d->sliceNumber = z;
		if (apply) {
			d->slice->sliceNumber = z;
		}
	}

	auto SliceGeneral::SetGeometryZ(int dimZ, double resZ, double offset) -> void {
		d->dimZ = dimZ;
		d->resZ = resZ;
		d->offset = offset;
	}
}
