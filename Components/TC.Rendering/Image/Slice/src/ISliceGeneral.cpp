#include "ISliceGeneral.h"

namespace Tomocube::Rendering::Image {
    ISliceGeneral::ISliceGeneral() : slice_d{ new Impl } {

    }
    ISliceGeneral::~ISliceGeneral() {
        
    }    
    auto ISliceGeneral::GetSliceNumber() const -> int {
        return slice_d->sliceNumber;
    }
    auto ISliceGeneral::GetSliceTransparency() const -> float {
        return slice_d->transp;
	}
}