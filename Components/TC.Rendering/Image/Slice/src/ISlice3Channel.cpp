#include "ISlice3Channel.h"

namespace Tomocube::Rendering::Image {
    ISlice3Channel::ISlice3Channel() : channel_d{ new Impl } {
        
    }
    auto ISlice3Channel::GetDataMinMax(int ch) const -> std::tuple<double, double> {
        if(ch < 0 || ch >2) {
            return std::make_tuple(-1, -1);
        }
        return std::make_tuple(channel_d->min[ch], channel_d->max[ch]);
    }
    auto ISlice3Channel::GetDataRange(int ch) const -> std::tuple<double, double> {
        if (ch < 0 || ch >2) {
            return std::make_tuple(-1, -1);
        }
        return std::make_tuple(channel_d->lower[ch], channel_d->upper[ch]);
    }
    auto ISlice3Channel::GetIsGamma(int ch) const -> bool {
        if (ch < 0 || ch >2) {
            return false;
        }
        return channel_d->isGamma[ch];
    }
    auto ISlice3Channel::GetGamma(int ch) const -> float {
        if (ch < 0 || ch >2) {
            return -1;
        }
        return channel_d->gamma[ch];
    }
    auto ISlice3Channel::GetColor(int ch) const -> QColor {
        if (ch < 0 || ch >2) {
            return QColor();
        }
        return channel_d->color[ch];
    }
    auto ISlice3Channel::GetTransparency(int ch) const -> float {
        if(ch < 0 || ch > 2) {
            return -1;
        }
        return channel_d->transp[ch];
    }
}