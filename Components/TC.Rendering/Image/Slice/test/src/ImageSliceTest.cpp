#include <QFileDialog>
#include <QVBoxLayout>


#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

//Slice Scenes
#include <Slice3Channel.h>
#include <Slice3ChannelTF1D.h>

//With 2D TF
#include <SliceTF2D.h>
#include <Slice3ChannelTF2D.h>
#include <QTransferFunctionCanvas2D.h>
#include <HiddenScene.h>
#include <SoTF2D.h>

#include <TCFMetaReader.h>
#include <OivXYReader.h>
#include <RenderWindowSlice.h>

using namespace Tomocube::Rendering::Image;

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", "F:/CellLibrary/20210614_ TCF 1.4.0/20210607.170006.233.HTFL 3D single-001/20210607.170006.233.HTFL 3D single-001.TCF", "TCF (*.tcf)");

    if (path.isEmpty()) {
        return EXIT_FAILURE;
    }

    QWidget* socket = new QWidget;
    socket->setMinimumSize(1000, 500);
    auto layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    socket->setLayout(layout);
    SoQt::init(socket);
    SoVolumeRendering::init();
    OivXYReader::initClass();
    SoTF2D::initClass();

    auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
    auto metaInfo = metaReader->Read(path);

    auto renderWindow = new RenderWindow2D(nullptr);
    layout->addWidget(renderWindow);

    auto hiddenScene = new HiddenScene;
    auto canvas = new QTransferFunctionCanvas2D;
    layout->addWidget(canvas);
    canvas->setCurProjPath(qApp->applicationDirPath());
    hiddenScene->SetFilePath(path);
    hiddenScene->SetImageParentDir(qApp->applicationDirPath());
    canvas->setHidden(hiddenScene);

    SoRef<SoVolumeData> volData = new SoVolumeData;
    SoRef<OivXYReader> reader = new OivXYReader;
    reader->setDataGroupPath("/Data/3D");
    reader->setTileName("000000");
    reader->setIndex(metaInfo->data.data3D.sizeZ / 2);
    reader->setFilename(path.toStdString());
    volData->setReader(*reader, TRUE);

    SoRef<SoVolumeData> chData[3]{ nullptr,nullptr,nullptr };
    SoRef<OivXYReader> chReader[3]{ nullptr,nullptr,nullptr };
    for (auto i = 0; i < 3; i++) {
        chData[i] = new SoVolumeData;
        chReader[i] = new OivXYReader(true);
        chReader[i]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(i).toStdString());
        chReader[i]->setTileName("000000");
        chReader[i]->setIndex(metaInfo->data.data3DFL.sizeZ / 2);
        chReader[i]->setFilename(path.toStdString());
        chData[i]->setReader(*chReader[i], TRUE);
    }
    {
        hiddenScene->SetHTMinMax(metaInfo->data.data3D.riMin * 10000.0, metaInfo->data.data3D.riMax * 10000.0);
        hiddenScene->Calc2DHistogram(volData.ptr(), false);
        canvas->setDataXdecimation(3);
        canvas->setDataXSingleStep(0.001);
        canvas->setDataXdivider(10000.0);
        canvas->setDataXname("RI");
        canvas->setDataXRange(metaInfo->data.data3D.riMin * 10000.0, metaInfo->data.data3D.riMax * 10000.0);

        canvas->setDataYdecimation(2);
        canvas->setDataYSingleStep(0.05);
        canvas->setDataYdivider(1.0);
        canvas->setDataYname("Gradient");

        double grad_min, grad_max;
        hiddenScene->GetGradMinMax(grad_min, grad_max);
        canvas->setDataYRange(grad_min, grad_max);
    }

    layout->setStretch(0, 1);
    layout->setStretch(1, 1);

    //SceneGraph
#if 0
    //One scalar image with 1D TF
    auto sliceTF1D = new SliceTF1D("Test");
    sliceTF1D->SetVolume(volData.ptr());
    sliceTF1D->SetSliceNumber(metaInfo->data.data3D.sizeZ / 2, false);

    renderWindow->setSceneGraph(sliceTF1D->GetRootSwitch());
#endif
#if 0
    //One scalar image with 2D TF
    auto sliceTF2D = new SliceTF2D("Test");
    sliceTF2D->SetHiddenSep(hiddenScene->GetSceneGraph());
    sliceTF2D->BuildSceneGraph();

    sliceTF2D->SetVolume(volData.ptr());
    sliceTF2D->SetSliceNumber(metaInfo->data.data3D.sizeZ / 2, false);

    renderWindow->setSceneGraph(sliceTF2D->GetRootSwitch());
#endif
#if 0
    //Three channel images 
    auto slice3Channel = new Slice3Channel("Test");
    for (auto i = 0; i < 3; i++) {
        slice3Channel->SetVolume(i, chData[i].ptr());
    }
    slice3Channel->SetSliceNumber(metaInfo->data.data3DFL.sizeZ / 2, false);
    renderWindow->setSceneGraph(slice3Channel->GetRootSwitch());
#endif
#if 0
    //Thress channel images + One scalar image with 1D TF
    auto slice3ChannelTF1D = new Slice3ChannelTF1D("Test");
    slice3ChannelTF1D->SetVolume(volData.ptr());
    for(auto i =0;i<3;i++) {
        slice3ChannelTF1D->SetVolume(i, chData[i].ptr());
    }
    slice3ChannelTF1D->SetSliceNumber(metaInfo->data.data3D.sizeZ / 2, false);
    renderWindow->setSceneGraph(slice3ChannelTF1D->GetRootSwitch());
#endif
#if 1
    //Three channel images + One scalar image with 2D TF
    auto slice3ChannelTF2D = new Slice3ChannelTF2D("Test");
    slice3ChannelTF2D->SetHiddenSep(hiddenScene->GetSceneGraph());
    slice3ChannelTF2D->BuildSceneGraph();
    slice3ChannelTF2D->SetVolume(volData.ptr());
    for (auto i = 0; i < 3; i++) {
        slice3ChannelTF2D->SetVolume(i, chData[i].ptr(),false);
    }
    slice3ChannelTF2D->SetSliceNumber(metaInfo->data.data3D.sizeZ / 2, false);
    renderWindow->setSceneGraph(slice3ChannelTF2D->GetRootSwitch());
#endif

    renderWindow->ResetView2D(Direction2D::XY);
    
    SoQt::show(socket);
    SoQt::mainLoop();    

    delete socket;

    SoTF2D::exitClass();
    OivXYReader::exitClass();
    SoVolumeRendering::finish();
    SoQt::finish();

    return EXIT_SUCCESS;
}