#include <QApplication>
#include <QFileDialog>

#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>
#pragma warning(pop)

#include <H5Cpp.h>

using namespace imagedev;
using namespace iolink;
using namespace ioformat;

int main(int argc,char** argv) {
    QApplication app(argc, argv);

    try {
        imagedev::init();
    }catch(imagedev::Exception& e) {
        std::cout << e.what() << std::endl;
    }

    auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", "F:/CellLibrary/Test data_Basic Analyzer/RAW 264.7 lps treatment 24 h/20200214.164035.756.RAW LPS-24h-017", "TCF (*.tcf)");

    if(path.isEmpty()) {
        return EXIT_FAILURE;
    }

    
    H5::H5File file(path.toStdString(), H5F_ACC_RDONLY);
    auto group = file.openGroup("/Data/3D");
    auto dataSet = group.openDataSet("000000");

    int64_t sizeX, sizeY, sizeZ;
    auto attrtX = group.openAttribute("SizeX");
    attrtX.read(H5::PredType::NATIVE_INT64, &sizeX);
    attrtX.close();
    auto attrY = group.openAttribute("SizeY");
    attrY.read(H5::PredType::NATIVE_INT64, &sizeY);
    attrY.close();
    auto attrZ = group.openAttribute("SizeZ");
    attrZ.read(H5::PredType::NATIVE_INT64, &sizeZ);
    attrZ.close();        

    std::cout << QString("Original size: %1,%2,%3").arg(sizeX).arg(sizeY).arg(sizeZ).toStdString() << std::endl;
    const auto numberOfElements = sizeX * sizeY * sizeZ;
    std::shared_ptr<uint16_t[]> originalArray{ new uint16_t[numberOfElements]() };
    dataSet.read(originalArray.get(), H5::PredType::NATIVE_UINT16);
    dataSet.close();
    group.close();
    file.close();

    auto spatialCalibrationProperty = SpatialCalibrationProperty(
        { 0,0,0},	// origin
        { 1,1,1 }	// spacing
    );

    const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

    const VectorXu64 imageShape{ static_cast<uint64_t>(sizeX), static_cast<uint64_t>(sizeY),static_cast<uint64_t>(sizeZ) };
    const auto inputImage = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16,imageProperties,nullptr);
    setDimensionalInterpretation(inputImage, ImageTypeId::VOLUME);
    setImageInterpretation(inputImage, ImageInterpretation::GRAYSCALE);

    const RegionXu64 imageRegion{ {0, 0, 0}, imageShape };
    inputImage->writeRegion(imageRegion, originalArray.get()); 
    
    IntensityStatistics intensityStatisticsAlgo;
    intensityStatisticsAlgo.setInputImage(inputImage);
    intensityStatisticsAlgo.execute();

    auto copied = copyImage(inputImage);

    for (auto i = 0; i < 540; i++) {
        auto slice = getSliceFromVolume3d(inputImage, GetSliceFromVolume3d::Axis::X_AXIS, i);
        auto rotSlice = rotateImage2d(slice, { 269,269 }, 30, RotateImage2d::InterpolationType::LINEAR);

        copied = setSliceToVolume3d(copied, rotSlice, SetSliceToVolume3d::X_AXIS, i);
    }

    auto finalSlice = getSliceFromVolume3d(copied, GetSliceFromVolume3d::Z_AXIS, 105);

    auto grad = gradientOperator2d(finalSlice, GradientOperator2d::SOBEL, GradientOperator2d::X_AND_Y_GRADIENTS, 1);

    auto converted = convertImage(grad.outputImageX, ConvertImage::UNSIGNED_INTEGER_16_BIT);

    ioformat::writeView(converted, "G:/test.png");


    imagedev::finish();
    return EXIT_SUCCESS;
}