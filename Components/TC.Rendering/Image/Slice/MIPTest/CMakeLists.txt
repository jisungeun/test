project(MIPTest)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)


#Sources
set(SOURCES	
	src/MIPTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
		$<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/Dependency/hdf5/include>
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
)

set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		${VIEWER_COMPONENT_LINK_LIBRARY}		
		ImageDev
		ImageDevExamples
		Qt5::Core
		Qt5::Gui
		Qt5::Widgets
		optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5_D.lib		
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases") 	

