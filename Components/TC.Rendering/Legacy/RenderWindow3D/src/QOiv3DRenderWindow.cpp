#include <QSizePolicy>
#include <QGridLayout>
#include <QMenu>

#pragma warning(push)
#pragma warning(disable : 4819)

#include <Inventor/SoInput.h>
#include <Inventor/helpers/SbFileHelper.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtCursors.h>
#include <Inventor/Qt/viewers/SoQtIcons.h>
#include <Inventor/Qt/SoQtRenderArea.h>
#include <Inventor/sensors/SoFieldSensor.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoGradientBackground.h>

#include <Inventor/MPEG/SoMPEGNavRenderer.h>

// VolumeViz Header
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>

//IvTune
#include <IvTune/SoIvTune.h>

#pragma warning(pop)

#include "QOiv3DRenderWindow.h"

#include <QTimer>
#define HEIGHT 450
#define WIDTH  450
#define WHEEL_DELTA 120

static SbBool s_firstDrag = TRUE;
static int s_lwdValue = 0;
static int s_rwdValue = 0;
static int s_bwdValue = 0;

struct QOiv3DRenderWindow::Impl {
	QCursor normalCursor;
	QCursor dollyCursor;
	QCursor panCursor;
	QCursor rollCursor;
	QCursor seekCursor;
	QCursor spinCursor;    	

	SoSeparator* rootScene;
	SoGradientBackground* gradient;
	SoPerspectiveCamera* mainCam;	

	SoGuiAlgoViewers* m_guiAlgo;
	SoQtRenderArea* m_renderArea;
	ViewerModes m_currentViewerMode;
	SbTime m_lastMotionTime;
	bool m_toggledCameraType, m_altSwitchBack, m_boxZoom, m_boxSelection;

	float orbit_x = 0.f;
	float orbit_y = 0.f;

	SoSFColor bg_color1;
	SoSFColor bg_color2;

	bool is_SpinAnimation{ false };
	bool is_Black{ false };
	bool is_Gradient{ true };
	bool is_White{ false };
};

//---------------------------------------------------------------
QOiv3DRenderWindow::QOiv3DRenderWindow(QWidget* parent)
	: QWidget(parent), d(new Impl())
{
	setAttribute(Qt::WA_DeleteOnClose);
	resize(WIDTH, HEIGHT);

	//SoQt::init(this);

	QGridLayout* gridLayout = new QGridLayout();	
	{		
		d->m_guiAlgo = new SoGuiAlgoViewers();		
		d->m_guiAlgo->setViewerType(SoGuiAlgoViewers::EXAMINER);		
		d->m_renderArea = new SoQtRenderArea(this, "CVRenderArea", true, true, true, d->m_guiAlgo);
		d->m_renderArea->setEventCallback(QOiv3DRenderWindow::renderAreaEventCB, this); // Register this callback to recieve events from the render area
		d->m_renderArea->getWidget()->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));			
		gridLayout->addWidget(d->m_renderArea->getWidget(), 0, 0);		
	}
	setLayout(gridLayout);

	if (this->layout()) {
		this->layout()->setSpacing(0);
		this->layout()->setMargin(0);
		this->layout()->setContentsMargins(0, 0, 0, 0);
	}

	d->m_currentViewerMode = IDLE_MODE;
	d->m_lastMotionTime = SbTime::zero();
	d->m_toggledCameraType = false;
	d->m_altSwitchBack = false;
	d->m_boxZoom = false;
	d->m_boxSelection = false;	

	//gradient background setting
	d->rootScene = new SoSeparator;
	d->gradient = new SoGradientBackground;
	d->bg_color1.setValue(0.7f, 0.7f, 0.8f);
	d->bg_color2.setValue(0.0f, 0.1f, 0.3f);
	d->rootScene->addChild(d->gradient);

	d->mainCam = new SoPerspectiveCamera;
	d->rootScene->addChild(d->mainCam);
	d->rootScene->addChild(new SoDirectionalLight);

	d->m_guiAlgo->setSceneGraph(d->rootScene);			
}

//---------------------------------------------------------------

auto QOiv3DRenderWindow::ShowContextMenu(const QPoint& pos) ->void {
	QPoint globalPos = this->mapToGlobal(pos);

	QMenu myMenu;	

	QMenu* bgMenu = myMenu.addMenu(tr("Background Options"));
	auto gradient = bgMenu->addAction("Gradient");
	auto black = bgMenu->addAction("Black");
	auto white = bgMenu->addAction("White");

	gradient->setCheckable(true);
	black->setCheckable(true);
	white->setCheckable(true);

	if (d->is_Gradient)
		gradient->setChecked(true);
	else if (d->is_Black)
		black->setChecked(true);
	else if (d->is_White)
		white->setChecked(true);

	QMenu* spinMenu = myMenu.addMenu(tr("Spin Animation Options"));
	auto spin = spinMenu->addAction("Activate Spin");

	spin->setCheckable(true);

	if (d->is_SpinAnimation)
		spin->setChecked(true);

	QMenu* viewMenu = myMenu.addMenu(tr("View"));
	auto view_x = viewMenu->addAction("View X");
	auto view_y = viewMenu->addAction("View Y");
	auto view_z = viewMenu->addAction("View Z");
	auto view_reset = viewMenu->addAction("Reset View");

	view_x->setCheckable(false);
	view_y->setCheckable(false);
	view_z->setCheckable(false);
	view_reset->setCheckable(false);

	QAction* selectedItem = myMenu.exec(globalPos);
	if(selectedItem) {
		auto text = selectedItem->text();
		if(text.contains("Gradient")) {
			d->is_Gradient = true;
			d->is_Black = false;
			d->is_White = false;
			ChangeBackGround(0);
		}else if(text.contains("Black")) {
			d->is_Gradient = false;
			d->is_Black = true;
			d->is_White = false;
			ChangeBackGround(1);
		}else if(text.contains("White")) {
			d->is_Gradient = false;
			d->is_Black = false;
			d->is_White = false;
			ChangeBackGround(2);
		}else if(text.contains("Spin")) {
			d->is_SpinAnimation = !d->is_SpinAnimation;
			if (!d->is_SpinAnimation)
				d->m_guiAlgo->stopSpinAnimation();
		}else if(text.contains("Reset View")) {
			reset3DView();
		}
		else if (text.contains("View X")) {
			auto zoom = getCameraZoom();
			d->m_guiAlgo->viewX();
			setCameraZoom(zoom * 0.7f);
		}
		else if (text.contains("View Y")) {
			auto zoom = getCameraZoom();
			d->m_guiAlgo->viewY();
			setCameraZoom(zoom * 0.7f);
		}
		else if (text.contains("View Z")) {
			auto zoom = getCameraZoom();
			d->m_guiAlgo->viewZ();
			setCameraZoom(zoom * 0.7f);
		}
	}
}

auto QOiv3DRenderWindow::reset3DView() -> void {	
	auto root = d->m_guiAlgo->getSceneRoot();

	SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data	

	MedicalHelper::orientView(MedicalHelper::AXIAL, d->m_guiAlgo->getCamera(), volData);

	auto zoom = getCameraZoom();
	setCameraZoom(zoom * 0.7f);
}


auto QOiv3DRenderWindow::ChangeBackGround(int type) -> void {
    switch(type) {
	case 0://gradient
		d->bg_color1.setValue(0.7f, 0.7f, 0.8f);
		d->bg_color2.setValue(0.0f, 0.1f, 0.3f);		
		break;
	case 1://black
		d->bg_color1.setValue(0.0f, 0.0f, 0.0f);
		d->bg_color2.setValue(0.0f, 0.0f, 0.0f);		
		break;
	case 2://white
		d->bg_color1.setValue(1.0f, 1.0f, 1.0f);
		d->bg_color2.setValue(1.0f, 1.0f, 1.0f);		
		break;
    }
	d->gradient->color0 = d->bg_color1;
	d->gradient->color1 = d->bg_color2;
}

QOiv3DRenderWindow::~QOiv3DRenderWindow()
{
	//delete d->m_renderArea;	
	//SoQt::finish();	
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::closeEvent(QCloseEvent* /*unused*/) -> void
{
	//delete d->m_renderArea;

	//SoQt::finish();
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::keyPressEvent(QKeyEvent* keyPress) -> void
{
	// Add processPickEvent in case key must be processed by the scene graph
	Qt::KeyboardModifiers modifiers = keyPress->modifiers();

	switch (keyPress->key())
	{
	case Qt::Key_Escape:
	{
		if (d->m_guiAlgo->isViewing())
		{
			pickMode();
		}
		else
		{
			viewingMode();
		}
	}
	break;
	case Qt::Key_Alt:
	{
		if (!d->m_guiAlgo->isViewing())
		{
			d->m_altSwitchBack = TRUE;
			viewingMode();
		}
	}
	break;
	case Qt::Key_S:
		seekMode();
		break;
	case Qt::Key_F12:
		if ((modifiers & Qt::ShiftModifier) == Qt::ShiftModifier)
		{
			SoNode* root = d->m_renderArea->getSceneManager()->getSceneGraph();
			if (root != NULL) {
				SoIvTune::start(root);
			}
		}
		break;
	default:
		break;
	}
}

auto QOiv3DRenderWindow::getContext() -> SoGLContext* {
	return d->m_renderArea->getNormalSoContext();	
}

auto QOiv3DRenderWindow::getSharedContext(void)-> SbGLShareContext {
	return d->m_renderArea->getShareContext();
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::keyReleaseEvent(QKeyEvent* keyRelease) -> void
{
	// Add processPickEvent in case key must be processed by the scene graph
	if (d->m_altSwitchBack && (keyRelease->key() == Qt::Key_Alt))
	{
		// If Alt-key, then return to PICK (if we had switched)
		pickMode();
		d->m_altSwitchBack = FALSE;  // Clear the flag
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::mousePressEvent(QMouseEvent* mousePress) -> void
{
	if (processPickEvents(mousePress)) // Consumes the event if the viewer is in picking mode.
		return;
	else if ((mousePress->button() == Qt::LeftButton) || (mousePress->button() == Qt::MiddleButton))
	{
		SbVec2s raSize = d->m_guiAlgo->getGlxSize();
		SbVec2s locator;

		locator[0] = mousePress->x();
		locator[1] = raSize[1] - mousePress->y();
		d->m_guiAlgo->setCurrentMousePositionLocator(locator);

		if (d->m_currentViewerMode == SEEK_MODE)
		{
			if (!d->m_guiAlgo->isSeekMode())
				switchViewerMode(mousePress->buttons() | mousePress->modifiers()); // Prevents from being stuck in seek mode
			else
			{
				d->m_guiAlgo->seekToPoint(locator);
				return;
			}
		}
		else
			switchViewerMode(mousePress->buttons() | mousePress->modifiers());

		if(d->is_SpinAnimation)
			d->m_guiAlgo->stopSpinAnimation();
		d->m_guiAlgo->interactiveCountInc();
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::mouseReleaseEvent(QMouseEvent* mouseRelease) -> void
{
	if ((mouseRelease->button() == Qt::RightButton))
	{
		//can add mouse right button popup

		auto pos = mouseRelease->pos();
		ShowContextMenu(pos);
		return;
	}

	if (processPickEvents(mouseRelease)) return;

	SbVec2s raSize = d->m_guiAlgo->getGlxSize();
	SbVec2s locator;

	if ((mouseRelease->button() == Qt::LeftButton) || (mouseRelease->button() == Qt::MiddleButton))
	{
		locator[0] = mouseRelease->x();
		locator[1] = raSize[1] - mouseRelease->y();
		d->m_guiAlgo->setCurrentMousePositionLocator(locator);

		if (d->m_currentViewerMode == VIEW_MODE)
		{
			if ((SoDB::getCurrentTime() - d->m_lastMotionTime).getMsecValue() < 100)
			{
				if (d->is_SpinAnimation)
					d->m_guiAlgo->startSpinAnimation();
				d->m_guiAlgo->interactiveCountInc();
			}
		}
		if (d->m_currentViewerMode != SEEK_MODE)
			d->m_guiAlgo->interactiveCountDec();

		switchViewerMode(mouseRelease->buttons() | mouseRelease->modifiers());
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::mouseMoveEvent(QMouseEvent* mouseMove) -> void
{
	if (processPickEvents(mouseMove)) return;

	SbVec2s raSize = d->m_guiAlgo->getGlxSize();

	SbVec2f newLocator;
	float mx = mouseMove->x() / float(raSize[0]);
	float my = (raSize[1] - mouseMove->y()) / float(raSize[1]);
	newLocator.setValue(mx, my);

	switch (d->m_currentViewerMode)
	{
	case VIEW_MODE:
	{
		d->m_lastMotionTime = SoDB::getCurrentTime();
		d->m_guiAlgo->spinCamera(newLocator);		
	}
	break;
	case PAN_MODE:
		d->m_guiAlgo->panCamera(newLocator);
		break;
	case DOLLY_MODE:
		d->m_guiAlgo->dollyCamera(SbVec2s(mouseMove->x(), raSize[1] - mouseMove->y()));
		break;
	default:
		// not managed
		break;
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::wheelEvent(QWheelEvent* mouseWheel) -> void
{
	mouseWheel->accept();

	float newValue = (mouseWheel->angleDelta().y()) / WHEEL_DELTA;
	d->m_guiAlgo->mouseWheelMotion(newValue);
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::eventFilter(QObject* obj, QEvent* event) -> bool
{
	QWidget* widget = (QWidget*)obj;
	switch (event->type())
	{
	case QEvent::Close:
		if (widget->isVisible())
		{
			widget->hide();
		}
		else
		{
			widget->show();
		}
		return true;
	default:
		return false;
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::renderAreaEventCB(void* userData, QEvent* anyevent)-> SbBool
{
	QOiv3DRenderWindow* customViewer = (QOiv3DRenderWindow*)userData;

	switch (anyevent->type())
	{
	case QEvent::KeyPress: // 6
		customViewer->keyPressEvent((QKeyEvent*)anyevent);
		return TRUE;
	case QEvent::KeyRelease: // 7
		customViewer->keyReleaseEvent((QKeyEvent*)anyevent);
		return TRUE;
	case QEvent::MouseButtonPress: // 2
		customViewer->mousePressEvent((QMouseEvent*)anyevent);
		return TRUE;
	case QEvent::MouseButtonRelease: // 3
		customViewer->mouseReleaseEvent((QMouseEvent*)anyevent);
		return TRUE;
	case QEvent::MouseMove: // 5
		customViewer->mouseMoveEvent((QMouseEvent*)anyevent);
		return TRUE;
	case QEvent::Wheel: // 31
		customViewer->wheelEvent((QWheelEvent*)anyevent);
		return TRUE;
	default:
		return FALSE; // In case other events than those above should be processed by the render area
	}
}

auto QOiv3DRenderWindow::rotateCallback(void* userData, SoSensor* sensor)->void {
	Q_UNUSED(sensor)
    SoCamera* myCamera = (SoCamera*)userData;
	SbRotation rot;
	SbMatrix mtx;
	SbVec3f pos;

	// Adjust the position
	pos = myCamera->position.getValue();
	rot = SbRotation(SbVec3f(0, 2, 0), (float)(M_PI / 60.0f));

	mtx.setRotate(rot);
	mtx.multVecMatrix(pos, pos);
	myCamera->position.setValue(pos);

	// Adjust the orientation
	myCamera->orientation.setValue(myCamera->orientation.getValue() * rot);
}

auto QOiv3DRenderWindow::getCamera() -> SoCamera* {	
	return d->m_guiAlgo->getCamera();	
}

auto QOiv3DRenderWindow::getSceneGraph(void)->SoNode* {
	return d->m_renderArea->getSceneManager()->getSceneGraph();
}

auto QOiv3DRenderWindow::getViewportRegion()-> const SbViewportRegion& {
	return d->m_renderArea->getViewportRegion();
}

auto QOiv3DRenderWindow::setViewDirection(int axis)->void {
    switch (axis) {
	case 0:
		d->m_guiAlgo->viewX();
		break;
	case 1:
		d->m_guiAlgo->viewY();
		break;
	case 2:
		d->m_guiAlgo->viewZ();
		break;
	}
}

auto QOiv3DRenderWindow::getRenderBuffer()->QPixmap {
	return d->m_renderArea->getWidget()->grab();
}

auto QOiv3DRenderWindow::clearOrbit()->void {
	d->orbit_x = 0.f;
	d->orbit_y = 0.f;
}

auto QOiv3DRenderWindow::rotate(int axis, float val)->void {
	d->orbit_x += val;
	d->orbit_y += val;

	SbVec2f newLocator(d->orbit_x, 0);
	d->m_guiAlgo->spinConstrainedCamera(newLocator, axis);
}

auto QOiv3DRenderWindow::tilting(int axis, int orbit, float tic, float inter)->void {
	Q_UNUSED(axis)
    clearOrbit();

	auto val = tic / 5.f;

	for (int i = 0; i < orbit / 8; ++i) {
		d->orbit_x += val;
		d->orbit_y += val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8; i < orbit / 8 * 2; ++i) {
		d->orbit_x -= val;
		d->orbit_y -= val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 2; i < orbit / 8 * 3; ++i) {
		d->orbit_x -= val;
		d->orbit_y += val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 3; i < orbit / 8 * 4; ++i) {
		d->orbit_x += val;
		d->orbit_y -= val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 4; i < orbit / 8 * 5; ++i) {
		d->orbit_x -= val;
		d->orbit_y -= val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 5; i < orbit / 8 * 6; ++i) {
		d->orbit_x += val;
		d->orbit_y += val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 6; i < orbit / 8 * 7; ++i) {
		d->orbit_x += val;
		d->orbit_y -= val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
	for (int i = orbit / 8 * 7; i < orbit / 8 * 8; ++i) {
		d->orbit_x -= val;
		d->orbit_y += val;
		SbVec2f newLocator(d->orbit_x, d->orbit_y);
		d->m_guiAlgo->spinCamera(newLocator);

		Sleep(inter);
	}
}


//---------------------------------------------------------------
auto QOiv3DRenderWindow::processPickEvents(QEvent* anEvent) -> SbBool
{
	if (!d->m_guiAlgo->isViewing())
	{
		// Send the event to the scene graph if the viewing is off		
		d->m_renderArea->setEventCallback(NULL, NULL); // This will prevent the event to be sent again to QtCustomViewer
		d->m_renderArea->sendEvent(anEvent); // Make the render area process the event
		d->m_renderArea->setEventCallback(QOiv3DRenderWindow::renderAreaEventCB, this);

		static int xPos1, yPos1, xPos2, yPos2;
		QMouseEvent* me = (QMouseEvent*)anEvent;
		static bool mouseDown = false;

		switch (anEvent->type()) // Process the event for the picking
		{
		case QEvent::MouseButtonPress:
			if (me->button() == Qt::LeftButton)
			{
				mouseDown = true;
				xPos1 = me->x();
				yPos1 = me->y();
				xPos2 = xPos1 + 1;
				yPos2 = yPos1 + 1;
			}
			break;
		case QEvent::MouseButtonRelease:
			if (me->button() == Qt::LeftButton)
			{
				if (d->m_boxZoom)
					d->m_guiAlgo->doBoxZoom(xPos1, yPos1, xPos2, yPos2); // Otherwise box selection will be made
				mouseDown = false;
			}
			break;
		case QEvent::MouseMove:
			if (mouseDown)
			{
				xPos2 = me->x();
				yPos2 = me->y();
			}
			break;
		default:
			break;
		}

		return TRUE;
	}

	return FALSE;
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setupCursors() -> void
{ 
	d->dollyCursor = QCursor(QPixmap((const char**)Classic_Dolly));
	d->panCursor = QCursor(QPixmap((const char**)Classic_Pan));
	d->rollCursor = QCursor(QPixmap((const char**)Classic_Roll));
	d->seekCursor = QCursor(QPixmap((const char**)Classic_Seek));
	d->spinCursor = QCursor(QPixmap((const char**)Classic_Spin));

	d->m_renderArea->setCursor(d->spinCursor);
}

//---------------------------------------------------------------
// Actual Renderer setting part
//---------------------------------------------------------------
auto QOiv3DRenderWindow::setSceneGraph(SoNode* newScene) -> void
{
	if (d->rootScene->getChild(3))
		d->rootScene->removeChild(3);
	d->rootScene->addChild(newScene);
	//d->m_guiAlgo->setSceneGraph(newScene);
	//d->root->addChild(newScene);
	//d->m_renderArea->setSceneGraph(newScene);
	d->m_renderArea->show();
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setDecoration(bool isDeco) -> void
{
	d->m_guiAlgo->setDecoration(isDeco);
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setViewing(bool isView) -> void
{
	d->m_guiAlgo->setViewing(isView);
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setTransparencyType(SoGLRenderAction::TransparencyType type) -> void
{
	d->m_guiAlgo->setTransparencyType(type);
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::getBackgroundColor() -> SbColor
{
	return d->m_guiAlgo->getBackgroundColor();
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setDrawStyle(SoGuiAlgoViewers::DrawType type, SoGuiAlgoViewers::DrawStyle style) -> void
{
	d->m_guiAlgo->setDrawStyle(type, style);
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setBufferingType(SoGuiAlgoViewers::BufferType type) -> void
{
	d->m_guiAlgo->setBufferingType(type);

	// Update the popup menu entries
	switch (type)
	{
	case SoGuiAlgoViewers::BUFFER_SINGLE:
		d->m_renderArea->setDoubleBuffer(FALSE);
		break;
	case SoGuiAlgoViewers::BUFFER_DOUBLE:
		d->m_renderArea->setDoubleBuffer(TRUE);
		break;
	case SoGuiAlgoViewers::BUFFER_INTERACTIVE:
		d->m_renderArea->setDoubleBuffer(FALSE);
		break;
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::setCameraZoom(float zoom) -> void
{
	SoCamera* camera = d->m_guiAlgo->getCamera();
	if (camera == NULL)
		return;

	if (camera->isOfType(SoPerspectiveCamera::getClassTypeId()))
		((SoPerspectiveCamera *)camera)->heightAngle = zoom * M_PI / 180.0;
	else if (camera->isOfType(SoOrthographicCamera::getClassTypeId()))
		((SoOrthographicCamera *)camera)->height = zoom;
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::getCameraZoom() -> float
{
	SoCamera* camera = d->m_guiAlgo->getCamera();
	if (camera == NULL)
		return 0;

	if (camera->isOfType(SoPerspectiveCamera::getClassTypeId()))
		return (float)(((SoPerspectiveCamera *)camera)->heightAngle.getValue() * 180.0f / M_PI);
	else if (camera->isOfType(SoOrthographicCamera::getClassTypeId()))
		return ((SoOrthographicCamera *)camera)->height.getValue();
	else
		return 0;
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::viewAll() -> void
{
	if (d->is_SpinAnimation)
		d->m_guiAlgo->stopSpinAnimation();
	d->m_guiAlgo->viewAll();	
}


//---------------------------------------------------------------
auto QOiv3DRenderWindow::switchViewerMode(unsigned int state) -> void
{
	if ((state & Qt::LeftButton) && (state & Qt::MiddleButton))
	{
		d->m_currentViewerMode = DOLLY_MODE;
		d->m_renderArea->setCursor(d->dollyCursor);
	}
	else if (state & Qt::LeftButton)
	{
		if (state & Qt::ControlModifier)
		{
			if (state & Qt::ShiftModifier)
			{
				d->m_currentViewerMode = DOLLY_MODE;
				d->m_renderArea->setCursor(d->dollyCursor);
			}
			else
			{
				d->m_currentViewerMode = PAN_MODE;
				d->m_renderArea->setCursor(d->panCursor);
				d->m_guiAlgo->activatePanning();
			}
		}
		else
		{
			if (state & Qt::ShiftModifier)
			{
				d->m_currentViewerMode = PAN_MODE;
				d->m_renderArea->setCursor(d->panCursor);
				d->m_guiAlgo->activatePanning();
			}
			else
			{
				d->m_renderArea->setCursor(d->spinCursor);
				d->m_currentViewerMode = VIEW_MODE;
				d->m_guiAlgo->activateSpinning();
			}
		}
	}
	else if (state & Qt::MiddleButton)
	{
		if (state & Qt::ControlModifier)
		{
			d->m_renderArea->setCursor(d->dollyCursor);
			d->m_currentViewerMode = DOLLY_MODE;
		}
		else
		{
			d->m_renderArea->setCursor(d->panCursor);
			d->m_currentViewerMode = PAN_MODE;
			d->m_guiAlgo->activatePanning();
		}
	}
	else
	{
		if ((d->m_currentViewerMode == SEEK_MODE) && d->m_guiAlgo->isSeekMode())
		{
			d->m_renderArea->setCursor(d->seekCursor);
			d->m_currentViewerMode = SEEK_MODE;
		}
		else
		{
			d->m_renderArea->setCursor(d->spinCursor);
			d->m_currentViewerMode = IDLE_MODE;
		}
	}
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::pickMode() -> void
{
	if (d->is_SpinAnimation)
		d->m_guiAlgo->stopSpinAnimation();
	if (d->m_guiAlgo->isViewing())
	{
		d->m_guiAlgo->setViewing(false);
		d->m_boxZoom = false;
		d->m_boxSelection = false;
		d->m_guiAlgo->setBoxDrawingMode(FALSE);
		d->m_guiAlgo->setBoxSelectionMode(FALSE);
	}
	d->m_renderArea->setCursor(d->normalCursor);
	d->m_currentViewerMode = PICK_MODE;
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::viewingMode() -> void
{
	if (!d->m_guiAlgo->isViewing())
	{
		d->m_guiAlgo->setViewing(true);
		d->m_boxZoom = false;
		d->m_boxSelection = false;
	}

	d->m_renderArea->setCursor(d->spinCursor);
	d->m_currentViewerMode = IDLE_MODE;
}

//---------------------------------------------------------------
auto QOiv3DRenderWindow::seekMode()-> void
{	
	if (d->is_SpinAnimation)
		d->m_guiAlgo->stopSpinAnimation();
	d->m_guiAlgo->setSeekMode(!d->m_guiAlgo->isSeekMode());
	if (d->m_guiAlgo->isSeekMode())
	{
		d->m_renderArea->setCursor(d->seekCursor);
		d->m_currentViewerMode = SEEK_MODE;
	}
}


auto QOiv3DRenderWindow::immediateRender() -> void {
	d->m_renderArea->render();	
}