#include <iostream>
#include <QApplication>
#include <QOiv3DRenderWindow.h>

#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>

//Create simple Qt+OIV 3D view with 3D Cone object
void main(int argc,char** argv) {
	QApplication app(argc,argv);	

	QOiv3DRenderWindow* m_RenderWindow = new QOiv3DRenderWindow(nullptr);
	m_RenderWindow->setMinimumSize(400, 400);	
    
    //create scene graph for dummy cone
	SoSeparator *root = new SoSeparator;
	SoSeparator* conSep = new SoSeparator;
	SoMaterial *myMaterial = new SoMaterial;		
	myMaterial->diffuseColor.setValue(1.0, 0.0, 0.0);   // Red

	conSep->addChild(myMaterial);
	conSep->addChild(new SoCone);
	root->addChild(conSep);

	SoSeparator* cylSep = new SoSeparator;
	SoMaterial *myMaterial2 = new SoMaterial;
	myMaterial2->diffuseColor.setValue(0.0, 1.0, 0.0);   // Green
	cylSep->addChild(myMaterial2);
	cylSep->addChild(new SoCylinder);
	root->addChild(cylSep);

	m_RenderWindow->setSceneGraph(root);
	m_RenderWindow->setWindowTitle("3D RenderWindow Test");
	m_RenderWindow->viewAll();

	m_RenderWindow->show();

	app.exec();		
}