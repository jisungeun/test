#ifndef QT_OIV_3DRENDERWINDOW_H
#define QT_OIV_3DRENDERWINDOW_H

#include <QMouseEvent>
#include <QWidget>

#include <Inventor/Gui/viewers/SoGuiAlgoViewers.h>

#include <Medical/helpers/MedicalHelper.h>

#include "TCRenderWindow3DExport.h"

class TCRenderWindow3D_API QOiv3DRenderWindow : public QWidget
{
	Q_OBJECT
public:
	struct RotateInfo {
		SoCamera* camera;
		int axis = 0;
		int startAngle = 0.f;
		float ticAngle = 0.f;
		SoGuiAlgoViewers* viewers;
	};

	enum ViewerModes
	{
		PICK_MODE,
		VIEW_MODE,
		PAN_MODE,
		DOLLY_MODE,
		SEEK_MODE,
		IDLE_MODE = -1
	};
	QOiv3DRenderWindow(QWidget* parent);
	~QOiv3DRenderWindow();

	auto setSceneGraph(SoNode* newScene) -> void;
	auto setDecoration(bool isDeco) -> void;
	auto setViewing(bool isView) -> void;
	auto setTransparencyType(SoGLRenderAction::TransparencyType type) -> void;
	auto setDrawStyle(SoGuiAlgoViewers::DrawType type, SoGuiAlgoViewers::DrawStyle style) -> void;
	auto getBackgroundColor() -> SbColor;
	auto viewAll() -> void;
	auto getCamera()->SoCamera*;
	auto immediateRender()->void;

	auto getSceneGraph(void)->SoNode*;
    auto getViewportRegion()-> const SbViewportRegion&;
	auto getContext(void)->SoGLContext*;
	auto getSharedContext(void)-> SbGLShareContext;

	auto setViewDirection(int axis)->void; // 0 : x, 1 : y, 2: z

	auto getRenderBuffer()->QPixmap;

	auto clearOrbit()->void;
	auto rotate(int axis, float val)->void;
	auto tilting(int axis, int orbit, float tic, float inter)->void;

    //Events
	virtual auto closeEvent(QCloseEvent* unused) -> void;
	virtual auto keyPressEvent(QKeyEvent* keyPress) -> void;
	virtual auto keyReleaseEvent(QKeyEvent* keyRelease) -> void;
	virtual auto mousePressEvent(QMouseEvent* mousePress) -> void;
	virtual auto mouseReleaseEvent(QMouseEvent* mouseRelease) -> void;
	virtual auto mouseMoveEvent(QMouseEvent* mouseMove) -> void;
	virtual auto wheelEvent(QWheelEvent* mouseWheel) -> void;
	auto setBufferingType(SoGuiAlgoViewers::BufferType type) -> void;
	auto setCameraZoom(float zoom) -> void;
	auto getCameraZoom() -> float;

	auto reset3DView()->void;
	auto ChangeBackGround(int type)->void;

protected:
	auto eventFilter(QObject* obj, QEvent* event) -> bool;

private:
	static auto renderAreaEventCB(void* userData, QEvent* anyevent) ->  SbBool; // Redirects keyboard and mouse events from the render area to QtCustomViewer
	static auto rotateCallback(void* userData, SoSensor* sensor)->void; 

    auto processPickEvents(QEvent* anEvent) -> SbBool ;
	auto createPopupMenu(void)->void;

    auto setupCursors() -> void;
	auto switchViewerMode(unsigned int state) -> void;
	auto viewingMode() -> void;
	auto pickMode() -> void;
	auto seekMode() -> void;

	auto ShowContextMenu(const QPoint& pos)->void;	

	struct Impl;
	std::unique_ptr<Impl> d;
};

#endif