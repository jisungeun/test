#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaExaminer.h>

#include <iostream>
#include <QApplication>
#include <QPushButton>
#include <QSettings>
#include <QFileDialog>
#include <QDoubleSpinBox>
#include <QVBoxLayout>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <ImageViz/SoImageViz.h>

//#include <QTransferFunctionCanvas2D.h>
//#include <OivSliceContainer.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
//#include <QOiv3DRenderWindow.h>
//#include <QOivRenderWindow.h>
#include <TCFMetaReader.h>

#include <SceneManagerXY.h>
#include <ScaleBarSwitch.h>
#include <TimeStampSwitch.h>
#include <ScalarBarSwitch.h>
//#include <TFHiddenScene.h>

//custom class init
#include <OivScaleBar.h>
#include <OivRangeBar.h>
#include <Medical/nodes/TextBox.h>
#include <OivHdf5Reader.h>
#include <OivXYReader.h>
#include <OivColorReader.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivLdmReaderBF.h>
#include <SoTransferFunction2D.h>

//Create simple Qt+OIV 2D view with tooth image

int cur_time_step = 0;
bool toggledHTRange = false;
bool toggledFLRange = false;

auto makeColorMap(SbColor color, float gamma, int id) -> SoTransferFunction* {
	SoTransferFunction* pTF = new SoTransferFunction();
	pTF->transferFunctionId = id + 1;

	// Color map will contain 256 RGBA values -> 1024 float values.
	// setNum pre-allocates memory so we can edit the field directly.
	pTF->colorMap.setNum(256 * 4);
	float R = color[0];
	float G = color[1];
	float B = color[2];

	// Get an edit pointer, assign values, then finish editing.
	float* p = pTF->colorMap.startEditing();
	for (int i = 0; i < 256; ++i) {
		float factor = (float)i / 255;
		float mod_r = R * factor;
		float mod_g = G * factor;
		float mod_b = B * factor;
		mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
		mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
		mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
		*p++ = pow(factor, gamma);
	}
	pTF->colorMap.finishEditing();
	return pTF;
}


bool isMIP = false;
bool isBF = false;
bool isHT = true;
bool isFL = false;

void main(int argc, char** argv) {
	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();

	OivScaleBar::initClass();
	OivRangeBar::initClass();
	TextBox::initClass();
	OivHdf5Reader::initClass();
	OivXYReader::initClass();
	OivColorReader::initClass();
	OivLdmReader::initClass();
	OivLdmReaderFL::initClass();
	OivLdmReaderBF::initClass();
	SoTransferFunction2D::initClass();

    auto renderWindow2d = new QOiv2DRenderWindow{ nullptr };
	renderWindow2d->setRenderWindowID(0);

	TC::IO::TCFMetaReader reader;

	auto meta = reader.Read(fileName);
	
	TC::SceneManagerXY* manager = new TC::SceneManagerXY();	
	manager->SetRenderWindow(renderWindow2d);	
	manager->SetTF2DScene(new SoSeparator);
	manager->BuildSceneGraphXY();
	manager->SetFilePath(fileName,meta);	

	//Scale Bar
	TC::ScaleBarSwitch* scale = new TC::ScaleBarSwitch(renderWindow2d->getCamera());
	scale->SetPosition(0.95f, -0.95);
	manager->AddAnnotation("Scale",scale->GetRoot());	

	//Time stamp
	TC::TimeStampSwitch* timeStamp = new TC::TimeStampSwitch;
	timeStamp->SetImageMetaInfo(meta);
	timeStamp->SetTimeStep(0);
	manager->AddAnnotation("TimeStamp", timeStamp->GetRoot());

	//Scalar bar	
	TC::ScalarBarSwitch* scalarBar = new TC::ScalarBarSwitch();
	auto tfNew = new SoTransferFunction;
	tfNew->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
	scalarBar->SetTransferFunction(tfNew);
	scalarBar->SetMajorLength(4);
	scalarBar->SetRatio(8);
	scalarBar->SetImageRange(meta->data.data3D.riMin, meta->data.data3D.riMax);
	scalarBar->SetNumberOfAnnotation(4);
	scalarBar->SetHorizontal(false);
	manager->AddAnnotation("Scalar", scalarBar->GetRoot());

    //Create Render Window UI
	QMainWindow* window = new QMainWindow;
	window->setWindowTitle("SceneManagerXY");
	window->resize(800, 800);
	window->show();		

	QWidget* centralWidget = new QWidget(window);
	window->setCentralWidget(centralWidget);
	QWidget* containerWidget = new QWidget(window);
	auto renderLayout = new QHBoxLayout;
	renderLayout->setContentsMargins(0, 0, 0, 0);
	containerWidget->setLayout(renderLayout);

	centralWidget->setLayout(new QVBoxLayout);
	centralWidget->layout()->setContentsMargins(0, 0, 0, 0);
	centralWidget->layout()->setSpacing(0);

	centralWidget->layout()->addWidget(containerWidget);		

	renderLayout->addWidget(renderWindow2d);
	//renderLayout->addWidget(canvas);
	renderLayout->setStretch(0, 1);
	renderLayout->setStretch(1, 1);

	QPushButton* toggleHTRange = new QPushButton(nullptr);
	toggleHTRange->setText("Toggle HT range");
	centralWidget->layout()->addWidget(toggleHTRange);

	QPushButton* toggleFLRange = new QPushButton(nullptr);
	toggleFLRange->setText("Toggle FL range");
	centralWidget->layout()->addWidget(toggleFLRange);

	QPushButton* openTcfBtn = new QPushButton(nullptr);
	openTcfBtn->setText("Open Tcf");
	centralWidget->layout()->addWidget(openTcfBtn);

	QPushButton* toggleMIPBtn = new QPushButton(nullptr);
	toggleMIPBtn->setText("Toggle MIP");
	centralWidget->layout()->addWidget(toggleMIPBtn);

	QPushButton* toggleHTBtn = new QPushButton(nullptr);
	toggleHTBtn->setText("Toggle HT");
	centralWidget->layout()->addWidget(toggleHTBtn);

	QPushButton* toggleFLBtn = new QPushButton(nullptr);
	toggleFLBtn->setText("Toggle FL");
	centralWidget->layout()->addWidget(toggleFLBtn);

	QPushButton* toggleBFBtn = new QPushButton(nullptr);
	toggleBFBtn->setText("Toggle BF");
	centralWidget->layout()->addWidget(toggleBFBtn);

	QDoubleSpinBox* doubleSpin = new QDoubleSpinBox(nullptr);
	doubleSpin->setRange(0, 1);
	doubleSpin->setSingleStep(0.01);
	doubleSpin->setValue(0.0);
	centralWidget->layout()->addWidget(doubleSpin);

	QSpinBox* timeSpin = new QSpinBox(nullptr);
	timeSpin->setRange(1, meta->data.total_time.count());
	timeSpin->setSingleStep(1);
	timeSpin->setValue(0);
	centralWidget->layout()->addWidget(timeSpin);	
	
	QPushButton* timeStampBtn = new QPushButton(nullptr);
	timeStampBtn->setText("Toggle Time Stamp");
	centralWidget->layout()->addWidget(timeStampBtn);

	QPushButton* scaleBtn = new QPushButton(nullptr);
	scaleBtn->setText("Toggle Scale bar");
	centralWidget->layout()->addWidget(scaleBtn);

	QPushButton* sliceUpBtn = new QPushButton;
	sliceUpBtn->setText("Move Slice Upward");
	centralWidget->layout()->addWidget(sliceUpBtn);

	QPushButton* sliceDownBtn = new QPushButton;
	sliceDownBtn->setText("Move Slice Downward");
	centralWidget->layout()->addWidget(sliceDownBtn);

	QPushButton* resetBtn = new QPushButton;
	resetBtn->setText("resetView");
	centralWidget->layout()->addWidget(resetBtn);

	auto sclayout = new QHBoxLayout;
	sclayout->setMargin(0);
	sclayout->setContentsMargins(0, 0, 0, 0);
	auto scWidget = new QWidget;
	scWidget->setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);
	scWidget->setLayout(sclayout);
	centralWidget->layout()->addWidget(scWidget);

	QPushButton* scalBtn = new QPushButton;
	scalBtn->setText("scalarBar");
	sclayout->addWidget(scalBtn);

	QPushButton* scolBtn = new QPushButton;
	scolBtn->setText("colorMap");
	sclayout->addWidget(scolBtn);

	QPushButton* sposBtn = new QPushButton;
	sposBtn->setText("posotion");
	sclayout->addWidget(sposBtn);

	QPushButton* soriBtn = new QPushButton;
	soriBtn->setText("direction");
	sclayout->addWidget(soriBtn);

    renderWindow2d->reset2DView();	

	QObject::connect(toggleHTRange, &QPushButton::clicked, [=]() {
		auto htMin = meta->data.data3D.riMin * 10000.0;
		auto htMax = meta->data.data3D.riMax * 10000.0;
		auto htStep = (meta->data.data3D.riMax - meta->data.data3D.riMin)/4 * 10000.0;
		
		if(toggledHTRange) {
			manager->SetHTRange(htMin, htMax);			
		}else {			
			manager->SetHTRange(htMin + htStep, htMax - htStep);			
		}
		toggledHTRange = !toggledHTRange;
	});
	QObject::connect(toggleFLRange, &QPushButton::clicked, [=]() {
		auto flMin = meta->data.data3DFL.min[1];
		auto flMax = meta->data.data3DFL.max[1];
		auto flStep = (flMax - flMin) / 2;
		auto flStep2 = (flMax - flMin) / 5*4;
		if(toggledFLRange) {
			manager->SetFLRange(1, 0, flMax - flStep);
		}else {
			manager->SetFLRange(1, 0, flMax-flStep2);			
		}
		toggledFLRange = !toggledFLRange;
	});

	QObject::connect(doubleSpin, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=](double value) {
		manager->SetTransparency(value);
	});

	QObject::connect(timeSpin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),[=](int value) {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < std::numeric_limits<double>::epsilon();
		};
		auto time_point = meta->data.total_time[value - 1];
		if (meta->data.data3D.exist) {
			auto timePoint3d = meta->data.data3D.timePoints;
			auto idx = -1;
			for(auto i=0;i<timePoint3d.count();i++) {
			    if(AreSame(time_point,timePoint3d[i])) {
					idx = i;
					break;
			    }
			}
			if(idx > -1) {
				manager->SetHTTimeStep(idx);
			}
		}
		if(meta->data.data3DFL.exist) {
			auto timePointfl = meta->data.data3D.timePoints;
			auto idx = -1;
			for (auto i = 0; i < timePointfl.count(); i++) {
				if (AreSame(time_point, timePointfl[i])) {
					idx = i;
					break;
				}
			}
			if (idx > -1) {
				manager->SetFLTimeStep(idx);
			}
		}
		timeStamp->SetTimeStep(value - 1);		
	});

	QObject::connect(openTcfBtn, &QPushButton::clicked, [=]() {
		auto new_path = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");
		TC::IO::TCFMetaReader new_reader;
		auto new_meta = new_reader.Read(new_path);
		manager->SetFilePath(new_path,new_meta);		
	});

	QObject::connect(toggleMIPBtn, &QPushButton::clicked, [=]() {
		isMIP = !isMIP;
		manager->ToggleMIP(isMIP);
		if(isBF) {
			renderWindow2d->reset2DView();
		}
		if(isMIP) {
			manager->ToggleHTMIP(isHT);
			isBF = false;
		}else {
			manager->ToggleHTMIP(false);
		}		
	});

	QObject::connect(toggleHTBtn, &QPushButton::clicked, [=]() {
		isHT = !isHT;
		manager->ToggleHTIntensity(isHT);
		manager->ToggleHTMIP(isHT);
	});

	QObject::connect(toggleFLBtn, &QPushButton::clicked, [=]() {
		isFL = !isFL;
		for(auto i=0;i<3;i++) {
			manager->ToggleFLChannel(i, isFL);
		}		
	});

	QObject::connect(toggleBFBtn, &QPushButton::clicked, [=]() {
		isBF = !isBF;		
		manager->ToggleBF(isBF);
		renderWindow2d->reset2DView();
	});


	QObject::connect(timeStampBtn, &QPushButton::clicked, [=]() {
		if (manager->GetAnnotationStatus("TimeStamp")._to_index() == AnnoStatus::Visible) {
			manager->ToggleAnnotation("TimeStamp", false);
		}
		else {
			manager->ToggleAnnotation("TimeStamp", true);
		}
	});
	QObject::connect(scaleBtn, &QPushButton::clicked, [=]() {
		if(manager->GetAnnotationStatus("Scale")._to_index() == AnnoStatus::Visible) {			
			manager->ToggleAnnotation("Scale", false);
		}else {
			manager->ToggleAnnotation("Scale", true);			
		}
	});

	QObject::connect(sliceUpBtn, &QPushButton::clicked, [=]() {		
		auto idx = manager->GetHTZIndex();
		manager->SetHTZIndex(idx + 1);
	});
	QObject::connect(sliceDownBtn, &QPushButton::clicked, [=]() {
		auto idx = manager->GetHTZIndex();
		manager->SetHTZIndex(idx -1);
	});
	QObject::connect(resetBtn, &QPushButton::clicked, [=]() {
		renderWindow2d->reset2DView();
	});
	QObject::connect(scalBtn, &QPushButton::clicked, [=]() {
		if (manager->GetAnnotationStatus("Scalar")._to_index() == AnnoStatus::Visible) {
			manager->ToggleAnnotation("Scalar", false);
		}
		else {
			manager->ToggleAnnotation("Scalar", true);
		}
	});
	QObject::connect(scolBtn, &QPushButton::clicked, [=]() {
		auto curTF = scalarBar->GetTransferFunction();
		if(nullptr != curTF) {
			auto tfNew = new SoTransferFunction;
			if(curTF->predefColorMap.getValue() == SoTransferFunction::PredefColorMap::INTENSITY) {
				tfNew->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
			}else {
				tfNew->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			}
			scalarBar->SetTransferFunction(tfNew);
		}
	});
	QObject::connect(sposBtn, &QPushButton::clicked, [=]() {
		
	});
	QObject::connect(soriBtn, &QPushButton::clicked, [=]() {
		if(scalarBar->IsHorizontal()) {
			scalarBar->SetHorizontal(false);
			scalarBar->SetNumberOfAnnotation(4);
			scalarBar->SetImageWindow(meta->data.data3D.riMin, meta->data.data3D.riMax);
		}else {
			scalarBar->SetHorizontal(true);
			scalarBar->SetNumberOfAnnotation(3);
			scalarBar->SetImageWindow(1.3580, 1.3857);
		}
	});

	app.exec();		

	SoTransferFunction2D::exitClass();
	OivLdmReaderBF::exitClass();
	OivLdmReaderFL::exitClass();
	OivLdmReader::exitClass();
	OivColorReader::exitClass();
	OivXYReader::exitClass();
	OivHdf5Reader::exitClass();
	TextBox::exitClass();
	OivRangeBar::exitClass();
	OivScaleBar::exitClass();		
	
	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();		
}