#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#pragma warning(pop)

#include <OivRangeBar.h>

#include "ScalarBarSwitch.h"

namespace TC {
	struct ScalarBarSwitch::Impl {
		SoRef<SoSwitch> rootSwitch { nullptr };
		SoRef<OivRangeBar> rangeBar { nullptr };
	};

	ScalarBarSwitch::ScalarBarSwitch() : d { new Impl } {
		Init();
	}

	ScalarBarSwitch::~ScalarBarSwitch() { }

	auto ScalarBarSwitch::Init() -> void {
		d->rootSwitch = new SoSwitch;
		d->rootSwitch->whichChild = -1;

		d->rangeBar = new OivRangeBar();
		d->rangeBar->setName("ScalarBar");

		d->rootSwitch->addChild(d->rangeBar.ptr());
	}

	auto ScalarBarSwitch::SetPrecision(int prec) -> void {
		d->rangeBar->SetPrecision(prec);
	}

	auto ScalarBarSwitch::GetRoot() -> SoSwitch* {
		return d->rootSwitch.ptr();
	}

	auto ScalarBarSwitch::SetTransferFunction(SoTransferFunction* tf) -> void {
		d->rangeBar->setTransferFunction(tf);
	}

	auto ScalarBarSwitch::GetTransferFunction() -> SoTransferFunction* {
		return d->rangeBar->getTransferFunction();
	}

	auto ScalarBarSwitch::GetPosition() -> std::tuple<float, float> {
		auto pos = d->rangeBar->position.getValue();
		return std::make_tuple(pos[0], pos[1]);
	}

	auto ScalarBarSwitch::SetPosition(float x, float y) -> void {
		d->rangeBar->position.setValue(x, y);
	}

	auto ScalarBarSwitch::SetRatio(float ratio) -> void {
		d->rangeBar->ratio = ratio;
	}

	auto ScalarBarSwitch::GetRatio() -> float {
		return d->rangeBar->ratio.getValue();
	}

	auto ScalarBarSwitch::SetMajorLength(float len) -> void {
		d->rangeBar->majorlength = len;
	}

	auto ScalarBarSwitch::GetMajorLength() -> float {
		return d->rangeBar->majorlength.getValue();
	}

	auto ScalarBarSwitch::SetNumberOfAnnotation(int num) -> void {
		d->rangeBar->numOfAnnotation = num;
	}

	auto ScalarBarSwitch::GetNumberOfAnnotation() -> int {
		return d->rangeBar->numOfAnnotation.getValue();
	}

	auto ScalarBarSwitch::SetHorizontal(bool isHori) -> void {
		auto prevOrientation = d->rangeBar->orientation.getValue();
		auto isToggled = false;
		if (isHori) {
			d->rangeBar->orientation.setValue(OivRangeBar::Orientation::HORIZONTAL);
			if (prevOrientation == OivRangeBar::Orientation::VERTICAL) {
				isToggled = true;
			}
		} else {
			d->rangeBar->orientation.setValue(OivRangeBar::Orientation::VERTICAL);
			if (prevOrientation == OivRangeBar::Orientation::HORIZONTAL) {
				isToggled = true;
			}
		}
		if (isToggled) {
			auto x = 0.f, y = 0.f;
			d->rangeBar->barSize.getValue().getValue(x, y);
			d->rangeBar->barSize.setValue(y, x);
		}
	}

	auto ScalarBarSwitch::IsHorizontal() -> bool {
		return 0 == d->rangeBar->orientation.getValue();
	}

	auto ScalarBarSwitch::SetImageRange(float min, float max) -> void {
		d->rangeBar->setMinMax(min, max, true);
	}

	auto ScalarBarSwitch::SetImageWindow(float min, float max) -> void {
		d->rangeBar->setMinMax(min, max, false);
	}

	auto ScalarBarSwitch::SetFontSize(float size) -> void {
		d->rangeBar->fontSize.setValue(size);
	}

	auto ScalarBarSwitch::GetFontSize() -> float {
		return d->rangeBar->fontSize.getValue();
	}

	auto ScalarBarSwitch::SetSize(float w, float h) -> void {
		d->rangeBar->barSize.setValue(SbVec2f(w, h));
	}

	auto ScalarBarSwitch::GetSize(float& w, float& h) -> void {
		w = d->rangeBar->barSize.getValue()[0];
		h = d->rangeBar->barSize.getValue()[1];
	}

	auto ScalarBarSwitch::SetLastAnnotationMargin(int margin) -> void {
		d->rangeBar->lastAnnotationMargin.setValue(margin);
	}

	auto ScalarBarSwitch::GetLastAnnotationMargin() -> int {
		return d->rangeBar->lastAnnotationMargin.getValue();
	}

	auto ScalarBarSwitch::SetReverseTextColor(bool reverse) -> void {
		d->rangeBar->reverseTextColor.setValue(reverse);
	}
}
