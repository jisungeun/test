#include <QMap>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#pragma warning(pop)

#include <QOivRenderWindow.h>
#include <OivSceneXY.h>
#include <Oiv2dBFSlice.h>

#include <OivHdf5Reader.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivLdmReaderBF.h>
#include <OivColorReader.h>

#include <OivXYReader.h>

#include "SceneManagerXY.h"

namespace TC {
    struct SceneManagerXY::Impl {
        QOivRenderWindow* renWin{ nullptr };        
        OivSceneXY* sceneContainer{ nullptr };
        Oiv2dBFSlice* bfContainer{ nullptr };

        QString filePath{ QString() };        
        IO::TCFMetaReader::Meta::Pointer metaInfo = nullptr;
                
        //IO
        OivXYReader* htReader{ nullptr };
        OivHdf5Reader* htMIPReader{ nullptr };                
        OivXYReader* flReader[3]{ nullptr,nullptr,nullptr };        
        OivHdf5Reader* flMIPReader[3]{ nullptr,nullptr,nullptr };        
        OivColorReader* bfReader[3]{ nullptr ,nullptr,nullptr };        

        //TODO - LDM slice-wise I/O        
        //OivLdmReader* htLDMReader{ nullptr };
        //OivLdmReaderFL* flLDMReader{ nullptr };
        //OivLdmReaderBF* bfLdmReader{ nullptr };

        //volumes
        SoVolumeData* htVolume{ nullptr };
        SoVolumeData* htMIP{ nullptr };
        SoVolumeData* flVolume[3]{ nullptr,nullptr,nullptr };
        SoVolumeData* flMIP[3]{ nullptr,nullptr,nullptr };
        SoVolumeData* bfData[3]{ nullptr,nullptr,nullptr };

        //LDM parameters
        int tileDim[3]{ 256,256,1 };
        int resolution{ 0 };
        bool isFixedResolution{ true };
        int lowRes{0};
        int upRes{2};
        int maxMainMemory{ -1 };
        int max2DTexMemory{ -1 };
        int max3DTexMemory{ -1 };

        //annotation
        QMap<QString, SoSwitch*> annotations;

        SoRef<SoSwitch> measureSwitch;
       
    };
    SceneManagerXY::SceneManagerXY() : d{ new Impl } {
        d->sceneContainer = new OivSceneXY;
        d->bfContainer = new Oiv2dBFSlice;
    }
    SceneManagerXY::~SceneManagerXY() {
        if(nullptr != d->htVolume) {
            d->htVolume->unref();
        }
        if(nullptr != d->htMIP) {
            d->htMIP->unref();
        }
        for(auto i =0 ; i<3;i++) {
            if (nullptr != d->flVolume[i]) {
                d->flVolume[i]->unref();
            }
            if(nullptr !=d->flMIP[i]) {
                d->flVolume[i]->unref();
            }
            if(nullptr !=d->bfData[i]) {
                d->bfData[i]->unref();
            }
        }        
        delete d->bfContainer;
        delete d->sceneContainer;        
    }

    auto SceneManagerXY::AddMeasurement(SoSeparator* sep)->void {
        d->measureSwitch->addChild(sep);
    }

    auto SceneManagerXY::AddAnnotation(const QString& key, SoSwitch* root) -> void {
        d->annotations[key] = root;
        SoSeparator* sep = new SoSeparator;
        SoMaterial* matl = new SoMaterial;
        SoSeparator* shapeSep = new SoSeparator;
        shapeSep->setName("ShapeSep");
        sep->addChild(shapeSep);
        matl->diffuseColor.setValue(1, 0, 0);
        SoDirectionalLight* light = new SoDirectionalLight;
        SoFaceSet* plan = new SoFaceSet;
        SoVertexProperty* vertices = new SoVertexProperty;
        plan->vertexProperty = vertices;
        vertices->vertex.set1Value(0, SbVec3f(-110.f, -80.f, 0.f));
        vertices->vertex.set1Value(1, SbVec3f(-110.f, 0.f, 0.f));
        vertices->vertex.set1Value(2, SbVec3f(0.f, 0.f, 0.f));
        vertices->vertex.set1Value(3, SbVec3f(0.f, -80.f, 0.f));
        
        shapeSep->addChild(light);
        shapeSep->addChild(matl);
        shapeSep->addChild(plan);
        
        //sep->addChild(root);
        d->sceneContainer->GetRenderRoot()->addChild(root);
        //d->sceneContainer->GetRenderRoot()->addChild(sep);
    }

    auto SceneManagerXY::RemoveAnnotation(const QString& key, SoSwitch* root)->void {
        d->annotations.remove(key);
        d->sceneContainer->GetRenderRoot()->removeChild(root);
    }

    auto SceneManagerXY::ToggleAnnotation(const QString& key,bool show) -> void {        
        if(d->annotations.contains(key)) {            
            if(show){                
                d->annotations[key]->whichChild = SO_SWITCH_ALL;
            }else {                
                d->annotations[key]->whichChild = SO_SWITCH_NONE;
            }
        }
    }
    auto SceneManagerXY::GetAnnotationStatus(const QString& key) -> AnnoStatus {
        if(d->annotations.contains(key)) {
            if(d->annotations[key]->whichChild.getValue() == SO_SWITCH_ALL) {
                return AnnoStatus::Visible;
            }
            return AnnoStatus::Hidden;
        }
        return AnnoStatus::NotExist;
    }
    auto SceneManagerXY::GetHTVolume() -> SoVolumeData* {
        return d->htVolume;
    }
    auto SceneManagerXY::SetRenderWindow(QOivRenderWindow* window) -> void {
        d->renWin = window;
    }
    auto SceneManagerXY::SetTF2DScene(SoSeparator* scene) -> void {
        d->sceneContainer->SetHiddenSep(scene);
    }    
    auto SceneManagerXY::SetFilePath(const QString& path, IO::TCFMetaReader::Meta::Pointer metaInfo) -> void {
        d->filePath = path;
        d->metaInfo = std::move(metaInfo);
        if(nullptr != d->sceneContainer) {
            clearScene();
            d->sceneContainer->SetUniform(this->isUniform());
            d->sceneContainer->SetLDM(d->metaInfo->data.isLDM);
            d->sceneContainer->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);
            buildIO();
        }        
    }

    auto SceneManagerXY::SetBFTimeStep(int step) -> void {
        if(nullptr == d->metaInfo) {
            return;
        }
        if(false == d->metaInfo->data.dataBF.exist) {
            return;
        }
        auto prev = d->bfContainer->getTransparency();
        if(step<0) {
            d->bfContainer->setTransparency(1.0);
            return;
        }
        d->bfContainer->setTransparency(prev);
        QString tileName = QString("%1").arg(step, 6, 10, QLatin1Char('0'));
        for (auto i = 0; i < 3; i++) {
            d->bfReader[i]->setTileName(tileName.toStdString());
            d->bfReader[i]->touch();
        }
    }

    auto SceneManagerXY::SetFLTimeStep(int step) -> void {        
        if (nullptr == d->metaInfo) {
            return;
        }
        d->sceneContainer->SetFLInRange(1);
        if(step < 0) {
            d->sceneContainer->SetFLInRange(0);
            return;
        }
        QString tileName = QString("%1").arg(step, 6, 10, QLatin1Char('0'));
        if(d->metaInfo->data.data3DFL.exist) {
            for(auto i=0;i<3;i++) {
                if(d->metaInfo->data.data3DFL.valid[i]) {                    
                    d->flReader[i]->setTileName(tileName.toStdString());
                    d->flReader[i]->touch();
                }
            }                
        }
        if(d->metaInfo->data.data2DFLMIP.exist) {
            for(auto i=0;i<3;i++) {
                if(d->metaInfo->data.data2DFLMIP.valid[i]) {
                    d->flMIPReader[i]->setTileName(tileName.toStdString());
                    d->flMIPReader[i]->touch();
                }
            }
        }
    }

    auto SceneManagerXY::SetHTTimeStep(int step) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        d->sceneContainer->SetHTExist(true);
        if(step<0) {
            d->sceneContainer->SetHTExist(false);
            return;
        }
        QString tileName = QString("%1").arg(step, 6, 10, QLatin1Char('0'));
        if(d->metaInfo->data.data3D.exist) {
            d->htReader->setTileName(tileName.toStdString());
            d->htReader->touch();
        }
        if(d->metaInfo->data.data2DMIP.exist) {
            d->htMIPReader->setTileName(tileName.toStdString());
            d->htMIPReader->touch();
        }
    }

    auto SceneManagerXY::Reset() -> void {
        clearScene();
        d->filePath.clear();
        d->metaInfo = nullptr;
    }

    auto SceneManagerXY::clearScene() -> void {
        if(nullptr != d->sceneContainer) {
            d->sceneContainer->ClearVolume();
            d->sceneContainer->ClearFLVolume();
            d->sceneContainer->ClearMIPVolume();
            d->sceneContainer->ClearMIPFLVolume();
        }
        if(nullptr != d->bfContainer) {
            d->bfContainer->ClearData();
        }
    }

    auto SceneManagerXY::buildIO() -> void {
        d->sceneContainer->SetHTExist(d->metaInfo->data.data3D.exist);
        d->sceneContainer->setFLExist(d->metaInfo->data.data3DFL.valid[0], d->metaInfo->data.data3DFL.valid[1], d->metaInfo->data.data3DFL.valid[2]);        
        if (d->metaInfo->data.data3D.exist){            
            if(nullptr == d->htVolume) {                
                d->htVolume = new SoVolumeData;
                d->htVolume->ref();
                d->htVolume->ldmResourceParameters.getValue()->resolution = 0;
                d->htVolume->ldmResourceParameters.getValue()->fixedResolution = TRUE;
            }
            if(nullptr == d->htReader) {
                d->htReader = new OivXYReader;
                d->htReader->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
                d->htReader->setDataGroupPath("/Data/3D");
            }
            QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
            d->htReader->setTileName(tileName.toStdString());            
            d->htReader->setIndex(d->metaInfo->data.data3D.sizeZ / 2);
            d->htReader->setFilename(d->filePath.toStdString());
            d->htVolume->setReader(*d->htReader, FALSE);
            d->sceneContainer->SetVolume(d->htVolume,true);            
        }
        if(d->metaInfo->data.data3DFL.exist) {            
            for(auto i=0;i<3;i++) {
                if (d->metaInfo->data.data3DFL.valid[i]) {
                    if (nullptr == d->flVolume[i]) {
                        d->flVolume[i] = new SoVolumeData;
                        d->flVolume[i]->ref();
                        d->flVolume[i]->ldmResourceParameters.getValue()->resolution = 0;
                        d->flVolume[i]->ldmResourceParameters.getValue()->fixedResolution = TRUE;
                    }
                    if (nullptr == d->flReader[i]) {
                        d->flReader[i] = new OivXYReader(true);
                        d->flReader[i]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
                        d->flReader[i]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(i).toStdString());
                    }
                    QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
                    d->flReader[i]->setTileName(tileName.toStdString());
                    d->flReader[i]->setIndex(d->metaInfo->data.data3DFL.sizeZ / 2);
                    d->flReader[i]->setFilename(d->filePath.toStdString());
                    d->flVolume[i]->setReader(*d->flReader[i], FALSE);
                    d->sceneContainer->SetFLVolume(i, d->flVolume[i], true);                    
                }
            }
        }
        if(d->metaInfo->data.data2DMIP.exist) {
            if(nullptr == d->htMIP) {
                d->htMIP = new SoVolumeData;
                d->htMIP->ref();
                d->htMIP->ldmResourceParameters.getValue()->resolution = 0;
                d->htMIP->ldmResourceParameters.getValue()->fixedResolution = TRUE;
            }
            if(nullptr == d->htMIPReader) {
                d->htMIPReader = new OivHdf5Reader;
                d->htMIPReader->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
                d->htMIPReader->setDataGroupPath("/Data/2DMIP");
            }
            QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
            d->htMIPReader->setTileName(tileName.toStdString());            
            d->htMIPReader->setFilename(d->filePath.toStdString());
            d->htMIP->setReader(*d->htMIPReader, FALSE);
            d->sceneContainer->SetMIPVolume(d->htMIP, true);
        }
        if(d->metaInfo->data.data2DFLMIP.exist) {
            for(auto i=0;i<3;i++) {
                if(d->metaInfo->data.data2DFLMIP.valid[i]) {
                    if(nullptr ==d->flMIP[i]) {
                        d->flMIP[i] = new SoVolumeData;
                        d->flMIP[i]->ref();
                        d->flMIP[i]->ldmResourceParameters.getValue()->resolution = 0;
                        d->flMIP[i]->ldmResourceParameters.getValue()->fixedResolution = TRUE;
                    }
                    if(nullptr == d->flMIPReader[i]) {
                        d->flMIPReader[i] = new OivHdf5Reader(true);
                        d->flMIPReader[i]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
                        d->flMIPReader[i]->setDataGroupPath(QString("/Data/2DFLMIP/CH%1").arg(i).toStdString());
                    }
                    QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
                    d->flMIPReader[i]->setTileName(tileName.toStdString());
                    d->flMIPReader[i]->setFilename(d->filePath.toStdString());
                    d->flMIP[i]->setReader(*d->flReader[i], FALSE);
                    d->sceneContainer->SetMIPFLVolume(i, d->flMIP[i], true);
                }
            }
        }
        if(d->metaInfo->data.dataBF.exist) {
            for(auto i=0;i<3;i++) {
                if(nullptr == d->bfData[i]) {
                    d->bfData[i] = new SoVolumeData;
                    d->bfData[i]->ref();
                    d->bfData[i]->ldmResourceParameters.getValue()->resolution = 0;
                    d->bfData[i]->ldmResourceParameters.getValue()->fixedResolution = TRUE;
                }
                if(nullptr == d->bfReader[i]) {
                    d->bfReader[i] = new OivColorReader;
                    d->bfReader[i]->setColorChannel(i);
                    d->bfReader[i]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
                    d->bfReader[i]->setDataGroupPath("/Data/BF");
                }
                QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
                d->bfReader[i]->setTileName(tileName.toStdString());
                d->bfReader[i]->setFilename(d->filePath.toStdString());
                d->bfData[i]->setReader(*d->bfReader[i], FALSE);                
            }
            d->bfContainer->setSingleBF(d->bfData[0], d->bfData[1], d->bfData[2], 0);
        }
    }
    auto SceneManagerXY::SetHTZIndex(int index) -> void {
        if(nullptr == d->metaInfo) {
            return;
        }
        if(false == d->metaInfo->data.data3D.exist) {
            return;            
        }
        if(d->metaInfo->data.data3D.sizeZ-1 < index) {
            return;
        }
        d->htReader->setIndex(index);
        d->htReader->touch();
    }
    auto SceneManagerXY::GetHTZIndex() -> int {
        if(nullptr == d->metaInfo) {
            return -1;
        }
        if (false == d->metaInfo->data.data3D.exist) {
            return -1;
        }
        return d->htReader->getIndex();
    }
    auto SceneManagerXY::SetFLZIndex(int index) -> void {
        if(nullptr == d->metaInfo) {
            return;
        }
        if (false ==  d->metaInfo->data.data3DFL.exist) {
            return;
        }
        if(d->metaInfo->data.data3DFL.sizeZ-1 < index) {
            d->sceneContainer->SetFLInRange(0);
            return;
        }
        if(index < 0) {
            d->sceneContainer->SetFLInRange(0);
            return;
        }
        d->sceneContainer->SetFLInRange(1);
        for(auto i=0;i<3;i++) {
            if(d->metaInfo->data.data3DFL.valid[i]) {
                d->flReader[i]->setIndex(index);
                d->flReader[i]->touch();
            }
        }
    }
    auto SceneManagerXY::GetFLZIndex() -> int {
        if (nullptr == d->metaInfo) {
            return -1;
        }
        if (false == d->metaInfo->data.data3DFL.exist) {
            return -1;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                return d->flReader[i]->getIndex();
            }
        }
        return -1;        
    }

    auto SceneManagerXY::BuildSceneGraphXY() -> bool {
        if(nullptr == d->renWin) {
            return false;
        }
        
        d->sceneContainer->buildSceneGraph();
        d->bfContainer->buildBFSlice(1);
        d->bfContainer->setTransparency(0);
        d->bfContainer->setTimeStep(0);

        d->sceneContainer->setBFRoot(d->bfContainer->getRenderRootNode());

        d->renWin->setSceneGraph(d->sceneContainer->GetRenderRoot());

        SoRef<SoSeparator> drawerSep = new SoSeparator;
        drawerSep->setName("DrawerSeparator");
        drawerSep->fastEditing = SoSeparator::CLEAR_ZBUFFER;

        d->measureSwitch = new SoSwitch;
        d->measureSwitch->whichChild = SO_SWITCH_ALL;

        drawerSep->addChild(d->measureSwitch.ptr());
        d->sceneContainer->GetRenderRoot()->addChild(drawerSep.ptr());

        return true;
    }    
    auto SceneManagerXY::isUniform() -> bool {        
        if(nullptr == d->metaInfo) {
            return true;
        }
        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < 0.000001;
        };
        if(d->metaInfo->data.data3D.exist && d->metaInfo->data.data3DFL.exist) {
            if (false == AreSame(d->metaInfo->data.data3D.resolutionX , d->metaInfo->data.data3DFL.resolutionX)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data3D.sizeX, d->metaInfo->data.data3DFL.sizeX)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data3D.resolutionY, d->metaInfo->data.data3DFL.resolutionY)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data3D.sizeY, d->metaInfo->data.data3DFL.sizeY)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data3D.resolutionZ, d->metaInfo->data.data3DFL.resolutionZ)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data3D.sizeZ, d->metaInfo->data.data3DFL.sizeZ)) {
                return false;
            }
        }
        if(d->metaInfo->data.data2DMIP.exist && d->metaInfo->data.data2DFLMIP.exist) {
            if (false == AreSame(d->metaInfo->data.data2DMIP.resolutionX, d->metaInfo->data.data2DFLMIP.resolutionX)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data2DMIP.sizeX, d->metaInfo->data.data2DFLMIP.sizeX)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data2DMIP.resolutionY, d->metaInfo->data.data2DFLMIP.resolutionY)) {
                return false;
            }
            if (false == AreSame(d->metaInfo->data.data2DMIP.sizeY, d->metaInfo->data.data2DFLMIP.sizeY)) {
                return false;
            }
        }
        return true;
    }
    auto SceneManagerXY::SetTileDimension(int dimX, int dimY, int dimZ) -> void {
        d->tileDim[0] = dimX;
        d->tileDim[1] = dimY;
        d->tileDim[2] = dimZ;
        if(nullptr == d->metaInfo) {
            return;
        }
        if(d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->tileDimension.setValue(dimX, dimY, dimZ);
        }
        if(d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->tileDimension.setValue(dimX, dimY, dimZ);
        }
        for(auto i=0;i<3;i++) {
            if(d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->tileDimension.setValue(dimX, dimY, dimZ);
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->tileDimension.setValue(dimX, dimY, 1);
            }
        }
        if(d->metaInfo->data.dataBF.exist) {
            for(auto i=0;i<3;i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->tileDimension.setValue(dimX, dimY, 1);
            }
        }
    }
    auto SceneManagerXY::GetTileDimension(int dim[3]) -> void {
        for (auto i = 0; i < 3; i++) {
            dim[i] = d->tileDim[i];
        }
    }
    auto SceneManagerXY::SetFixedResolution(int res) -> void {
        d->resolution = res;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->resolution = res;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->resolution = res;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->resolution = res;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->resolution = res;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->resolution = res;
            }
        }
    }
    auto SceneManagerXY::GetFixedResolution() -> int {
        return d->resolution;
    }
    auto SceneManagerXY::EnableFixedResolution(bool isFixedRes) -> void {
        d->isFixedResolution = isFixedRes;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->fixedResolution = isFixedRes;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->fixedResolution = isFixedRes;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->fixedResolution = isFixedRes;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->fixedResolution = isFixedRes;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->fixedResolution = isFixedRes;
            }
        }
    }
    auto SceneManagerXY::IsFixedResolutionEnabled() -> bool {
        return d->isFixedResolution;
    }
    auto SceneManagerXY::SetLowerResThreshold(int res) -> void {
        d->lowRes = res;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->minResolutionThreshold = res;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->minResolutionThreshold = res;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->minResolutionThreshold = res;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->minResolutionThreshold = res;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->minResolutionThreshold = res;
            }
        }
    }
    auto SceneManagerXY::GetLowerResThreshold() -> int {
        return d->lowRes;
    }
    auto SceneManagerXY::SetUpperResThreshold(int res) -> void {
        d->upRes = res;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->maxResolutionThreshold = res;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->maxResolutionThreshold = res;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->maxResolutionThreshold = res;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->maxResolutionThreshold = res;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->maxResolutionThreshold = res;
            }
        }
    }
    auto SceneManagerXY::GetUpperResThreshold() -> int {
        return d->upRes;
    }
    auto SceneManagerXY::SetMainMemory(int mem) -> void {
        d->maxMainMemory = mem;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->maxMainMemory = mem;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->maxMainMemory = mem;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->maxMainMemory = mem;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->maxMainMemory = mem;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->maxMainMemory = mem;
            }
        }
    }
    auto SceneManagerXY::GetMainMemory() -> int {
        return d->maxMainMemory;
    }
    auto SceneManagerXY::SetTex2DMemory(int mem) -> void {
        d->max2DTexMemory = mem;
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->max2DTexMemory = mem;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->max2DTexMemory = mem;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->max2DTexMemory = mem;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->max2DTexMemory = mem;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->max2DTexMemory = mem;
            }
        }
    }
    auto SceneManagerXY::GetTex2DMemory() -> int {
        return d->max2DTexMemory;
    }
    auto SceneManagerXY::SetTex3DMemory(int mem) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (d->metaInfo->data.data3D.exist) {
            d->htVolume->ldmResourceParameters.getValue()->maxTexMemory = mem;
        }
        if (d->metaInfo->data.data2DMIP.exist) {
            d->htMIP->ldmResourceParameters.getValue()->maxTexMemory = mem;
        }
        for (auto i = 0; i < 3; i++) {
            if (d->metaInfo->data.data3DFL.valid[i]) {
                d->flVolume[i]->ldmResourceParameters.getValue()->maxTexMemory = mem;
            }
            if (d->metaInfo->data.data2DFLMIP.valid[i]) {
                d->flMIP[i]->ldmResourceParameters.getValue()->maxTexMemory = mem;
            }
        }
        if (d->metaInfo->data.dataBF.exist) {
            for (auto i = 0; i < 3; i++) {
                d->bfData[i]->ldmResourceParameters.getValue()->maxTexMemory = mem;
            }
        }
    }
    auto SceneManagerXY::GetTex3DMemory() -> int {
        return d->max3DTexMemory;
    }
    auto SceneManagerXY::ToggleHTIntensity(bool show) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.data3D.exist) {
            return;
        }
        d->sceneContainer->ToggleHTIntensity(show);        
    }

    auto SceneManagerXY::ToggleMIP(bool show) -> void {
        if(nullptr == d->metaInfo) {
            return;
        }        
        d->sceneContainer->ShowMIP(show);        
        d->sceneContainer->ShowFLMIP(show);
    }

    auto SceneManagerXY::ToggleHTMIP(bool show) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.data2DMIP.exist) {
            return;
        }
        //d->sceneContainer->ShowMIP(show);
        d->sceneContainer->ShowHTMIP(show);
    }
    auto SceneManagerXY::ToggleFLChannel(int ch,bool show) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.data3DFL.exist) {
            return;
        }
        d->sceneContainer->ToggleFLChannel(ch, show);
    }
    /*auto SceneManagerXY::ToggleFLMIP(bool show) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.data2DFLMIP.exist) {
            return;
        }
        d->sceneContainer->ShowFLMIP(show);
    }*/
    auto SceneManagerXY::ToggleBF(bool show) -> void {
        if(nullptr == d->metaInfo) {
            return;
        }
        if(false == d->metaInfo->data.dataBF.exist) {
            return;
        }
        d->sceneContainer->toggleBF(show);        
    }
    auto SceneManagerXY::SetBFTransparency(float transparency) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.dataBF.exist) {
            return;
        }
        d->bfContainer->setTransparency(transparency);        
    }
    auto SceneManagerXY::ToggleTFOverlay(bool show) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == d->metaInfo->data.data3D.exist) {
            return;
        }
        d->sceneContainer->SetHTOverlay(show);        
    }
    auto SceneManagerXY::SetHTColorMap(int idx) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == (d->metaInfo->data.data3D.exist || d->metaInfo->data.data2DMIP.exist)){
            return;
        }
        d->sceneContainer->SetHTColorMap(idx);
    }
    auto SceneManagerXY::SetFLTransparency(int ch, float transparency) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == (d->metaInfo->data.data2DFLMIP.exist|| d->metaInfo->data.data3DFL.exist)) {
            return;
        }
        d->sceneContainer->SetFLTrans(transparency, ch);        
    }
    auto SceneManagerXY::SetHTRange(double min, double max) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == (d->metaInfo->data.data3D.exist || d->metaInfo->data.data2DMIP.exist)) {
            return;
        }
        d->sceneContainer->SetHTLevelWindow(min, max);        
    }
    auto SceneManagerXY::SetFLRange(int ch, int min, int max) -> void {
        if (nullptr == d->metaInfo) {
            return;
        }
        if (false == (d->metaInfo->data.data2DFLMIP.exist || d->metaInfo->data.data3DFL.exist)) {
            return;
        }
        d->sceneContainer->SetFLRange(ch, min, max);        
    }
    auto SceneManagerXY::SetTransparency(float transparency) -> void {
        d->sceneContainer->SetTransparency(transparency);         
    }
}