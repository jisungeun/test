#include <ioformat/IOFormat.h>
#include <iolink/view/ImageViewProvider.h>
#include <iolink/view/ImageViewFactory.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/ImageDev.h>

#include "TFHiddenScene.h"
#include <QDir>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <QOiv2DRenderWindow.h>

#define HIST_RES 100

using namespace imagedev;
using namespace ioformat;
using namespace iolink;

namespace TC {
	struct TFHiddenScene::Impl {
		SoSeparator* hideSep2D { nullptr };
		SoSeparator* shapeRoot2D { nullptr };
		SoShapeHints* shapeHint2D { nullptr };
		SoLightModel* lightModel2D { nullptr };
		SoSeparator* matSep2D { nullptr };

		QOpenGLWidget* offscreen2D { nullptr };
		QOiv2DRenderWindow* hiddenViewer2D { nullptr };

		int64_t grad_min { 0 };
		int64_t grad_max { 1 };
		int64_t min { 0 };
		int64_t max { 1 };
		int64_t range { 1 };
		int64_t grad_range { 1 };

		QString cur_proj_path = QString();
		QString file_path = QString();
		bool fileChanged { false };

		double interpolate(double val, double y0, double x0, double y1, double x1) {
			return (val - x0) * (y1 - y0) / (x1 - x0) + y0;
		}

		double base(double val) {
			if (val <= -0.75)
				return 0;
			if (val <= -0.25)
				return interpolate(val, 0.0, -0.75, 1.0, -0.25);
			if (val <= 0.25)
				return 1.0;
			if (val <= 0.75)
				return interpolate(val, 1.0, 0.25, 0.0, 0.75);
			return 0.0;
		}

		double red(double gray) {
			return base(gray - 0.75);
		}

		double green(double gray) {
			return base(gray - 0.25);
		}

		double blue(double gray) {
			return base(gray + 0.25);
		}

		auto CreateFrom2DPoints(const std::vector<SbVec2f>& pointsInCam) -> SoFaceSet* {
			std::vector<SbVec3f> polygon(pointsInCam.size());
			for (unsigned int i = 0; i < pointsInCam.size(); ++i) {
				polygon[i].setValue(pointsInCam[i][0], pointsInCam[i][1], 0.5f);
			}

			auto polygonVertices = new SoVertexProperty();
			polygonVertices->vertex.setValues(0, static_cast<int>(polygon.size()), &polygon[0]);

			auto polygonFaceSet = new SoFaceSet();
			polygonFaceSet->vertexProperty = polygonVertices;

			return polygonFaceSet;
		}

		auto getData(SoVolumeData* volData, SoCpuBufferObject* cpuBufferObject) -> SoLDMDataAccess::DataInfoBox;
		auto getLDMData(SoVolumeData* volData, SoCpuBufferObject* cpuBufferObject) -> SoLDMDataAccess::DataInfoBox;
	};

	TFHiddenScene::TFHiddenScene() : d { new Impl } {
		BuildSceneGraph();
	}

	TFHiddenScene::~TFHiddenScene() { }

	auto TFHiddenScene::SetFilePath(const QString& path) -> void {
		if (d->file_path != path) {
			d->fileChanged = true;
		}
		d->file_path = path;
	}

	auto TFHiddenScene::Update2DTF(std::vector<TFBox> tfToChange) -> void {
		int prev_num = d->shapeRoot2D->getNumChildren() - 1;
		for (auto i = prev_num; i > 0; i--) {
			d->shapeRoot2D->removeChild(i);
		}

		for (auto i = 0; i < tfToChange.size(); i++) {
			if (tfToChange[i].hidden) {
				continue;
			}
			std::vector<SbVec2f> lineInCam(4);
			double minx = toScreenX(tfToChange[i].min_val) * 2.0 - 1.0;
			double maxx = toScreenX(tfToChange[i].max_val) * 2.0 - 1.0;
			double miny = toScreenY(tfToChange[i].grad_min) * 2.0 - 1.0;
			double maxy = toScreenY(tfToChange[i].grad_max) * 2.0 - 1.0;

			lineInCam[0][0] = minx;
			lineInCam[0][1] = miny;

			lineInCam[1][0] = maxx;
			lineInCam[1][1] = miny;

			lineInCam[2][0] = maxx;
			lineInCam[2][1] = maxy;

			lineInCam[3][0] = minx;
			lineInCam[3][1] = maxy;

			auto switchNode = new SoSwitch();
			switchNode->whichChild = SO_SWITCH_ALL;     //SO_SWITCH_ALL: Show    SO_SWITCH_NONE: hide
			switchNode->setName((std::string("Box") + std::to_string(i + 1)).c_str());

			auto material = new SoMaterial;
			material->diffuseColor = SbColor(tfToChange[i].color);
			material->transparency = 1.0 - tfToChange[i].opacity;

			switchNode->addChild(material);
			auto shape = d->CreateFrom2DPoints(lineInCam);
			switchNode->addChild(shape);

			d->shapeRoot2D->addChild(switchNode);
		}
	}

	auto TFHiddenScene::SetHTMinMax(double min, double max) -> void {
		d->min = min;
		d->max = max;
	}

	auto TFHiddenScene::Calc2DHistogram(SoVolumeData* volume, bool isLDM) -> void {
		if (d->cur_proj_path.isEmpty()) {
			return;
		}
		if (false == d->fileChanged) {
			return;
		}
		//double dmin, dmax;
		//volume->getMinMax(dmin, dmax);
		//d->min = dmin;
		//d->max = dmax;
		d->range = d->max - d->min;
		int64_t min, max;
		min = d->min;
		max = d->max;
		unsigned short dataRange = (unsigned short)(max - min);
		double gradientMax = (float)dataRange / sqrt(3.f);
		unsigned short gradientMaxValue = (unsigned short)(sqrt(double(3 * (gradientMax * gradientMax))));

		SoRef<SoCpuBufferObject> cpuBufferObject1 = new SoCpuBufferObject();
		auto data = volume;
		SoVolumeData::LDMDataAccess::DataInfoBox info1;
		if (isLDM) {
			info1 = d->getLDMData(data, cpuBufferObject1.ptr());
		} else {
			info1 = d->getData(data, cpuBufferObject1.ptr());
		}
		void* values1 = cpuBufferObject1->map(SoCpuBufferObject::READ_ONLY);
		const unsigned short* slicePtr1 = static_cast<unsigned short*>(values1);
		d->grad_min = 100000;
		d->grad_max = -100000;

		if (info1.errorFlag == SoVolumeData::LDMDataAccess::CORRECT) {
			unsigned char histo[HIST_RES][HIST_RES];
			unsigned short hisval[HIST_RES][HIST_RES];
			memset(*histo, 0, sizeof(histo));
			memset(*hisval, 0, sizeof(hisval));
			unsigned short value1, value2;
			unsigned short value1Remaped, value2Remaped;

			for (int k = 0; k < info1.bufferDimension[2]; k++) {
				for (int j = 0; j < info1.bufferDimension[1]; j++) {
					for (int i = 0; i < info1.bufferDimension[0]; i++) {
						int index = i + j * info1.bufferDimension[0] + k * info1.bufferDimension[0] * info1.bufferDimension[1];
						value1 = slicePtr1[index];
						value1Remaped = map(value1, (unsigned short)min, (unsigned short)max, 0, HIST_RES - 1);


						// Compute gradient into CPU with centralDiff
						int neighborsIndex[6];
						neighborsIndex[0] = (i == info1.bufferDimension[0] - 1) ? index : index + 1;
						neighborsIndex[1] = (i == 0) ? index : index - 1;

						neighborsIndex[2] = (j == info1.bufferDimension[1] - 1) ?
												i + (j) * info1.bufferDimension[0] + k * info1.bufferDimension[0] * info1.bufferDimension[1] :
												i + (j + 1) * info1.bufferDimension[0] + k * info1.bufferDimension[0] * info1.bufferDimension[1];

						neighborsIndex[3] = (j == 0) ?
												i + (j) * info1.bufferDimension[0] + k * info1.bufferDimension[0] * info1.bufferDimension[1] :
												i + (j - 1) * info1.bufferDimension[0] + k * info1.bufferDimension[0] * info1.bufferDimension[1];

						neighborsIndex[4] = (k == info1.bufferDimension[2] - 1) ?
												i + j * info1.bufferDimension[0] + (k) * info1.bufferDimension[0] * info1.bufferDimension[1] :
												i + j * info1.bufferDimension[0] + (k + 1) * info1.bufferDimension[0] * info1.bufferDimension[1];

						neighborsIndex[5] = (k == 0) ?
												i + j * info1.bufferDimension[0] + (k) * info1.bufferDimension[0] * info1.bufferDimension[1] :
												i + j * info1.bufferDimension[0] + (k - 1) * info1.bufferDimension[0] * info1.bufferDimension[1];

						SbVec3f gradient;
						gradient[0] = float(slicePtr1[neighborsIndex[0]] - slicePtr1[neighborsIndex[1]]);
						gradient[1] = float(slicePtr1[neighborsIndex[2]] - slicePtr1[neighborsIndex[3]]);
						gradient[2] = float(slicePtr1[neighborsIndex[4]] - slicePtr1[neighborsIndex[5]]);
						gradient /= sqrt(3.f);
						// Lenght of the gradient vector
						value2 = (unsigned short)(sqrt(double((gradient[0] * gradient[0]) + (gradient[1] * gradient[1]) + (gradient[2] * gradient[2]))));
						if (d->grad_min > value2)
							d->grad_min = value2;
						if (d->grad_max < value2)
							d->grad_max = value2;
						value2Remaped = map(value2, 0, gradientMaxValue, 0, HIST_RES - 1);
						if (value2Remaped > HIST_RES - 1) {
							value2Remaped = HIST_RES - 1;
						}
						if (value1Remaped > HIST_RES - 1) {
							value1Remaped = HIST_RES - 1;
						}
						hisval[value2Remaped][value1Remaped]++;

					}
				}
			}
			cpuBufferObject1->unmap();

			int hmax = -1;
			int hmin = 1000000;
			for (int i = 0; i < HIST_RES; i++) {
				for (int j = 0; j < HIST_RES; j++) {
					if (hmax < hisval[i][j])
						hmax = hisval[i][j];
					if (hmin > hisval[i][j])
						hmin = hisval[i][j];
				}
			}

			d->grad_range = d->grad_max - d->grad_min;

			VectorXu64 imageShape { HIST_RES, HIST_RES };
			Vector3d spacing { 1, 1, 1 };
			Vector3d origin { 0, 0, 0 };

			auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
			auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
			setDimensionalInterpretation(image, ImageTypeId::IMAGE);

			VectorXu64 imageOrig { 0, 0 };
			RegionXu64 imageRegion { imageOrig, imageShape };

			image->writeRegion(imageRegion, hisval);

			const auto equaled = histogramEqualization(image, HistogramEqualization::OTHER, { 1, hmax }, { 0, 255 });
			const auto flipped = flipImage2d(equaled, FlipImage2d::CENTER);
			const auto converted = convertImage(flipped, ConvertImage::UNSIGNED_INTEGER_8_BIT);

			//save grayscale
			QDir dir(d->cur_proj_path);
			dir.cdUp();
			auto bg_path = dir.path() + "/Background.tif";
			writeView(converted, bg_path.toStdString());

			//populate to color image
			std::shared_ptr<unsigned char> redBuf(new unsigned char[HIST_RES * HIST_RES](), std::default_delete<unsigned char[]>());
			std::shared_ptr<unsigned char> greenBuf(new unsigned char[HIST_RES * HIST_RES](), std::default_delete<unsigned char[]>());
			std::shared_ptr<unsigned char> blueBuf(new unsigned char[HIST_RES * HIST_RES](), std::default_delete<unsigned char[]>());
			auto buffer = static_cast<unsigned char*>(converted->buffer());
			for (auto i = 0; i < HIST_RES; i++) {
				for (auto j = 0; j < HIST_RES; j++) {
					auto idx = i * HIST_RES + j;
					const auto val = *(buffer + idx);
					if (val != 0) {
						double vv = (double)val / 255.0;
						redBuf.get()[idx] = d->red(vv) * 255.0;
						greenBuf.get()[idx] = d->green(vv) * 255.0;
						blueBuf.get()[idx] = d->blue(vv) * 255.0;
					}
				}
			}
			QImage cImage(HIST_RES, HIST_RES, QImage::Format_RGB888);
			for (int y = 0; y < HIST_RES; y++) {
				for (int x = 0; x < HIST_RES; x++) {
					int index = y * HIST_RES + x;
					QRgb pixelValue = qRgb(redBuf.get()[index], greenBuf.get()[index], blueBuf.get()[index]);
					cImage.setPixel(x, y, pixelValue);
				}
			}

			//save color
			auto color_bg_path = dir.path() + "/ColorBackground.tif";
			cImage.save(color_bg_path);

			delete[] hisval;
		} else {
			cpuBufferObject1->unmap();
		}
		d->fileChanged = false;
	}

	auto TFHiddenScene::SetImageParentDir(const QString& path) -> void {
		d->cur_proj_path = path;
	}

	auto TFHiddenScene::BuildSceneGraph() -> void {
		d->hideSep2D = new SoSeparator;
		d->hideSep2D->setName("HT_hideSep");

		d->hideSep2D->addChild(new SoDirectionalLight);

		d->shapeRoot2D = new SoSeparator;
		d->shapeRoot2D->setName("shapeRoot_RenderToTex");

		d->shapeHint2D = new SoShapeHints;
		d->shapeHint2D->creaseAngle = (float)(0.3 * M_PI);
		d->shapeHint2D->windingType = SoShapeHints::NO_WINDING_TYPE;
		d->shapeHint2D->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
		d->shapeHint2D->shapeType = SoShapeHints::SOLID;
		d->shapeHint2D->faceType = SoShapeHints::CONVEX;

		d->shapeRoot2D->addChild(d->shapeHint2D);

		d->lightModel2D = new SoLightModel;
		d->lightModel2D->model = SoLightModel::BASE_COLOR;

		d->matSep2D = new SoSeparator;
		SoMaterial* pColor = new SoMaterial;
		d->matSep2D->addChild(pColor);

		d->hideSep2D->addChild(d->shapeRoot2D);
		d->hideSep2D->addChild(d->lightModel2D);
		d->hideSep2D->addChild(d->matSep2D);

		d->offscreen2D = new QOpenGLWidget;
		d->hiddenViewer2D = new QOiv2DRenderWindow(d->offscreen2D);
		d->hiddenViewer2D->setSceneGraph(d->hideSep2D);
		d->hiddenViewer2D->viewall();
		d->hiddenViewer2D->setTransparencyType(SoGLRenderAction::OPAQUE_FIRST);
		d->hiddenViewer2D->setMinimumSize(QSize(256, 256));
		d->hiddenViewer2D->setAutoFillBackground(false);
		d->hiddenViewer2D->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		d->hiddenViewer2D->show();
	}

	auto TFHiddenScene::GetSceneGraph() -> SoSeparator* {
		return d->hideSep2D;
	}

	auto TFHiddenScene::toScreenX(double val) -> double {
		return (val - d->min) / d->range;
	}

	auto TFHiddenScene::toScreenY(double val) -> double {
		return (val - d->grad_min) / d->grad_range;
	}

	auto TFHiddenScene::map(unsigned short value, unsigned short originalRangeMin, unsigned short originalRangeMax, unsigned short targetedRangeMin, unsigned short targetedRangeMax) -> unsigned short {
		return targetedRangeMin + (value - originalRangeMin) * (targetedRangeMax - targetedRangeMin) / (originalRangeMax - originalRangeMin);
	}

	auto TFHiddenScene::GetGradMinMax(double& min, double& max) -> void {
		min = static_cast<double>(d->grad_min);
		max = static_cast<double>(d->grad_max);
	}

	auto TFHiddenScene::Impl::getLDMData(SoVolumeData* volData, SoCpuBufferObject* cpuBufferObject) -> SoLDMDataAccess::DataInfoBox {
		SbVec3i32 oridim = volData->data.getSize();
		auto max_dim = -1;
		if (max_dim < oridim[0]) {
			max_dim = oridim[0];
		}
		if (max_dim < oridim[1]) {
			max_dim = oridim[1];
		}
		auto power = 0;
		while (pow(2, power) * 128 < max_dim) {
			power++;
		}
		if (power == 0) {
			power = 1;
		}
		auto maxRes = power - 1;

		if (maxRes > 0) {
			maxRes--;
		}

		SbVec3i32 dim = SbVec3i32(512, 512, 256);
		SbBox3i32 box(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));

		SoVolumeData::LDMDataAccess::DataInfoBox info;
		SoRef<SoBufferObject> dummyPtr = nullptr;
		info = volData->getLdmDataAccess().getData(maxRes, box, dummyPtr.ptr());

		if (info.bufferSize) {
			cpuBufferObject->setSize(info.bufferSize);
			info = volData->getLdmDataAccess().getData(maxRes, box, cpuBufferObject);
			SoVolumeData::DataType dataType = volData->getDataType();
			new SoMemoryObject(cpuBufferObject, (SbDataType)dataType);
			return info;
		}
		return info;
	}

	auto TFHiddenScene::Impl::getData(SoVolumeData* volData, SoCpuBufferObject* cpuBufferObject) -> SoLDMDataAccess::DataInfoBox {
		SbVec3i32 dim = volData->data.getSize();
		SbBox3f volSize = volData->extent.getValue();
		SbBox3i32 box(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));

		// Call with NULL buffer (default 3rd parameter) to get number of bytes required to hold data
		SoVolumeData::LDMDataAccess::DataInfoBox info;
		SoRef<SoBufferObject> dummyPtr = nullptr;
		info = volData->getLdmDataAccess().getData(0, box, dummyPtr.ptr());
		// Fetch the data contained in the ROI in the original volume

		if (info.bufferSize) {
			cpuBufferObject->setSize(info.bufferSize);
			info = volData->getLdmDataAccess().getData(0, box, cpuBufferObject);
			SoVolumeData::DataType dataType = volData->getDataType();
			new SoMemoryObject(cpuBufferObject, (SbDataType)dataType);
			return info;
		}
		return info;
	}

}
