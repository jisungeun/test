#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCamera.h>
#pragma warning(pop)

#include <OivScaleBar.h>

#include "ScaleBarSwitch.h"

namespace TC {
	struct ScaleBarSwitch::Impl {
		SoRef<SoSwitch> rootSwitch { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoDrawStyle> style { nullptr };
		SoRef<OivScaleBar> scalebar { nullptr };
		SoRef<SoCamera> trackedCamera { nullptr };
	};

	ScaleBarSwitch::ScaleBarSwitch(SoCamera* cam) : d { new Impl } {
		d->trackedCamera = cam;
		Init();
	}

	ScaleBarSwitch::~ScaleBarSwitch() { }

	auto ScaleBarSwitch::GetRoot() -> SoSwitch* {
		return d->rootSwitch.ptr();
	}

	auto ScaleBarSwitch::Init() -> void {
		d->rootSwitch = new SoSwitch;

		d->matl = new SoMaterial;
		d->matl->setName("ScaleBarMat");
		d->matl->diffuseColor.setValue(1, 0.25f, 0.25f);
		d->rootSwitch->addChild(d->matl.ptr());

		d->style = new SoDrawStyle;
		d->style->lineWidth = 2;
		d->rootSwitch->addChild(d->style.ptr());

		d->scalebar = new OivScaleBar();
		d->scalebar->setName("ScaleBar");
		d->scalebar->numTickIntervals = 0;
		d->scalebar->position = SbVec2f(0.95f, -0.99f);
		d->scalebar->alignment = OivScaleBar::RIGHT;
		d->scalebar->trackedCamera = d->trackedCamera.ptr();

		d->rootSwitch->addChild(d->scalebar.ptr());

		d->rootSwitch->whichChild = -3;
	}

	auto ScaleBarSwitch::SetColor(float r, float g, float b) -> void {
		d->matl->diffuseColor.setValue(r, g, b);
	}

	auto ScaleBarSwitch::GetColor() -> std::tuple<float, float, float> {
		auto col = d->matl->diffuseColor.getValues(0)[0];
		return std::make_tuple(col[0], col[1], col[2]);
	}

	auto ScaleBarSwitch::SetLineWidth(float width) -> void {
		d->style->lineWidth = width;
	}

	auto ScaleBarSwitch::GetLineWidth() -> float {
		return d->style->lineWidth.getValue();
	}

	auto ScaleBarSwitch::SetPosition(float x, float y) -> void {
		d->scalebar->position = SbVec2f(x, y);
	}

	auto ScaleBarSwitch::GetPosition() -> std::tuple<float, float> {
		auto pos = d->scalebar->position.getValue();
		return std::make_tuple(pos[0], pos[1]);
	}

	auto ScaleBarSwitch::SetTextVisible(bool visible) const -> void {
		d->scalebar->SetTextVisible(visible);
	}

	auto ScaleBarSwitch::GetTextVisible() const -> bool {
		return d->scalebar->GetTextVisible();
	}

	auto ScaleBarSwitch::SetTick(const int& tick) const -> void {
		d->scalebar->tick.setValue(tick);
	}

	auto ScaleBarSwitch::GetTick() const -> int {
		return d->scalebar->tick.getValue();
	}

	auto ScaleBarSwitch::GetLength() const -> float {
		return d->scalebar->GetRealLength();
	}

	auto ScaleBarSwitch::SetLength(const float& length) const -> void {
		d->scalebar->SetRealLength(length);
	}

	auto ScaleBarSwitch::SetHorizontal(bool isHorizontal) const -> void {
		if (isHorizontal) {
			d->scalebar->orientation = OivScaleBar::Orientation::HORIZONTAL;
		} else {
			d->scalebar->orientation = OivScaleBar::Orientation::VERTICAL;
		}
	}

	auto ScaleBarSwitch::IsHorizontal() const -> bool {
		return d->scalebar->orientation.getValue() == OivScaleBar::Orientation::HORIZONTAL;
	}
}
