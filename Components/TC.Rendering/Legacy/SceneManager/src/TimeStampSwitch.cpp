#include <QDateTime>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Medical/nodes/TextBox.h>
#pragma warning(pop)

#include "TimeStampSwitch.h"

namespace TC {
    struct TimeStampSwitch::Impl {
        SoSwitch* rootSwitch{ nullptr };
        SoMaterial* matl{ nullptr };
        TextBox* timestamp{ nullptr };
        IO::TCFMetaReader::Meta::Pointer metaInfo{ nullptr };

        QList<double> timeTable;
        int curTimeStep{ 0 };

        auto sec2str(const int& seconds)->QString;
    };
    TimeStampSwitch::TimeStampSwitch() : d{ new Impl } {        
        Init();
    }
    TimeStampSwitch::~TimeStampSwitch() {
        
    }
    auto TimeStampSwitch::GetRoot() -> SoSwitch* {
        return d->rootSwitch;
    }
    auto TimeStampSwitch::SetImageMetaInfo(IO::TCFMetaReader::Meta::Pointer metaInfo) -> void {
        d->metaInfo = std::move(metaInfo);        
        d->timeTable = d->metaInfo->data.total_time;        
        d->timestamp->deleteAll();
    }
    auto TimeStampSwitch::Init() -> void {
        d->rootSwitch = new SoSwitch;
        d->rootSwitch->whichChild = -1;

        d->matl = new SoMaterial;
        d->matl->setName("TimeStampMat");
        d->matl->diffuseColor.setValue(1, 1, 1);

        d->rootSwitch->addChild(d->matl);

        d->timestamp = new TextBox;        
        d->timestamp->setName("TimeStamp");
        d->timestamp->position.setValue(0.1f, -0.95f, 0);
        d->timestamp->alignmentH = TextBox::CENTER;
        d->timestamp->alignmentV = TextBox::BOTTOM;
        d->timestamp->fontSize.setValue(15);
        d->timestamp->border = FALSE;
        d->timestamp->boundingBoxIgnoring = FALSE;     
        d->timestamp->boundingBoxCaching = TRUE;        

        d->rootSwitch->addChild(d->timestamp);
    }    
    auto TimeStampSwitch::SetColor(float r, float g, float b) -> void {
        d->matl->diffuseColor.setValue(r, g, b);
    }
    auto TimeStampSwitch::GetColor() -> std::tuple<float, float, float> {
        auto col = d->matl->diffuseColor.getValues(0)[0];
        return std::make_tuple(col[0], col[1], col[2]);
    }
    auto TimeStampSwitch::SetPosition(float x, float y, float z) -> void {
        d->timestamp->position.setValue(x, y, z);
    }
    auto TimeStampSwitch::GetPosition() -> std::tuple<float, float, float> {
        auto pos = d->timestamp->position.getValue();
        return std::make_tuple(pos[0], pos[1], pos[2]);
    }
    auto TimeStampSwitch::SetFontSize(float size) -> void {
        d->timestamp->fontSize.setValue(size);
    }
    auto TimeStampSwitch::GetFontSize() -> float {
        return d->timestamp->fontSize.getValue();
    }
    auto TimeStampSwitch::SetTimeStep(int step) -> void {
        if(d->timeTable.isEmpty()) {
            return;
        }
        if(step < d->timeTable.count()) {
            d->curTimeStep = step;
            BuildTimeText();
        }        
    }
    auto TimeStampSwitch::BuildTimeText() -> void {
        //auto tick = QDateTime::fromTime_t(d->timeTable[d->curTimeStep]).toUTC().toString("hh:mm:ss");
        const auto tick = d->sec2str(d->timeTable[d->curTimeStep]);

        QString time_txt = QString("#(%1/%2) %3").arg(d->curTimeStep + 1).arg(d->timeTable.size()).arg(tick);        
        d->timestamp->deleteAll();
        d->timestamp->addLine(time_txt.toStdString());        
    }

    auto TimeStampSwitch::Impl::sec2str(const int& seconds)->QString {
        auto min = seconds / 60;
        const auto sec = seconds % 60;
        const auto hr = min / 60;
        min = min % 60;

        return QString("%1:%2:%3").arg(QString::number(hr).rightJustified(2, '0'))
                                  .arg(QString::number(min).rightJustified(2, '0'))
                                  .arg(QString::number(sec).rightJustified(2, '0'));
    }
}
