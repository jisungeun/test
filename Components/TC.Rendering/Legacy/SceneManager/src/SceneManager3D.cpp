#include <TCLogger.h>

#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

//IO - nonLDM
#include <OivHdf5Reader.h>
//IO - LDM
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
//Scene container
#include <OivScene3D.h>
//RenderWindow
#include <QOivRenderWindow.h>

#include "SceneManager3D.h"

namespace TC {
    struct SceneManager3D::Impl {
        QString tcfPath = QString();

        OivHdf5Reader* reader{ nullptr };
        OivLdmReader* readerLDM{ nullptr };
        OivLdmReaderFL* readerLDMFL{ nullptr };

        OivScene3D* sceneContainer{ nullptr };
        QOivRenderWindow* renWin{ nullptr };
    };
    SceneManager3D::SceneManager3D() : d{ new Impl } {
        d->sceneContainer = new OivScene3D;
    }
    SceneManager3D::~SceneManager3D() {

    }
    auto SceneManager3D::SetTF2DScene(SoSeparator* scene) -> void {
        d->sceneContainer->SetHiddenSep(scene);
    }
    auto SceneManager3D::SetRenderWindow(QOivRenderWindow* window) -> void {
        d->renWin = window;
    }
    auto SceneManager3D::BuildSceneGraph3D() -> bool {
        if(nullptr == d->renWin) {
            QLOG_DEBUG() << "renderwindow was nullptr";
            return false;
        }
        if(false == d->sceneContainer->BuildSceneGraph()) {
            QLOG_DEBUG() << "failed to build graph for 3d scene";
            return false;
        }
        d->renWin->setSceneGraph(d->sceneContainer->GetRenderRoot());
    }

}