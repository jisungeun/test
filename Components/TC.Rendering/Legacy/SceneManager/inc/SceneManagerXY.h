#pragma once

#include <memory>
#include <enum.h>

#include <TCFMetaReader.h>

#include "TCSceneManagerExport.h"

BETTER_ENUM(AnnoStatus, int,
    Hidden = 0,
    Visible = 1,
    NotExist = -1
);

class QOivRenderWindow;
class SoSeparator;
class SoSwitch;

namespace TC {
    class TCSceneManager_API SceneManagerXY {
    public:
        explicit SceneManagerXY();
        virtual ~SceneManagerXY();

        //prerequisite            
        auto SetRenderWindow(QOivRenderWindow* window)->void;
        auto SetTF2DScene(SoSeparator* scene)->void;
        auto BuildSceneGraphXY()->bool;

        //Common        
        auto SetFilePath(const QString& path,IO::TCFMetaReader::Meta::Pointer metaInfo)->void;
        auto Reset()->void;

        //LDM properties
        auto SetTileDimension(int dimX, int dimY, int dimZ = 1)->void;
        auto GetTileDimension(int dim[3])->void;
        auto SetFixedResolution(int res)->void;
        auto GetFixedResolution(void)->int;
        auto EnableFixedResolution(bool isFixedRes)->void;
        auto IsFixedResolutionEnabled(void)->bool;
        auto SetLowerResThreshold(int res)->void;
        auto GetLowerResThreshold(void)->int;
        auto SetUpperResThreshold(int res)->void;
        auto GetUpperResThreshold(void)->int;
        auto SetMainMemory(int mem)->void;
        auto GetMainMemory(void)->int;
        auto SetTex3DMemory(int mem)->void;
        auto GetTex3DMemory(void)->int;
        auto SetTex2DMemory(int mem)->void;
        auto GetTex2DMemory(void)->int;

        //Common
        auto SetTransparency(float transparency)->void;
        auto GetHTVolume()->SoVolumeData*;//for create TF2D

        //mode control
        auto ToggleMIP(bool show)->void;

        //HT control
        auto SetHTTimeStep(int step)->void;
        auto SetHTZIndex(int index)->void;
        auto GetHTZIndex()->int;
        auto ToggleHTIntensity(bool show)->void;
        auto ToggleHTMIP(bool show)->void;
        auto ToggleTFOverlay(bool show)->void;
        auto SetHTRange(double min, double max)->void;
        auto SetHTColorMap(int idx)->void;

        //FL control
        auto SetFLTimeStep(int step)->void;
        auto SetFLZIndex(int index)->void;
        auto GetFLZIndex()->int;
        auto ToggleFLChannel(int ch,bool show)->void;
        //auto ToggleFLMIP(bool show)->void;
        auto SetFLTransparency(int ch, float transparency)->void;
        auto SetFLRange(int ch, int min, int max)->void;        

        //BF control
        auto SetBFTimeStep(int step)->void;
        auto ToggleBF(bool show)->void;
        auto SetBFTransparency(float transparency)->void;        

        //Annotation Control;
        auto AddAnnotation(const QString& key,SoSwitch* root)->void;
        auto RemoveAnnotation(const QString& key, SoSwitch* root)->void;
        auto ToggleAnnotation(const QString& key,bool show)->void;
        auto GetAnnotationStatus(const QString& key)->AnnoStatus;

        auto AddMeasurement(SoSeparator* sep)->void;

    protected:
        auto isUniform()->bool;
        auto clearScene()->void;
        auto buildIO()->void;

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}