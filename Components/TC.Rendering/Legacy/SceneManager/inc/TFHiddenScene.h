#pragma once

#include <memory>
#include <QOpenGLWidget>
#include "TCSceneManagerExport.h"

class SoSeparator;
class SoVolumeData;

namespace TC {
    typedef struct TF_nodes {
        int min_val;
        int max_val;
        int grad_min;
        int grad_max;
        float opacity;
        float color[3];//r,g, b
        bool hidden;
    }TFBox;

    class TCSceneManager_API TFHiddenScene {
    public:
        TFHiddenScene();
        auto Update2DTF(std::vector<TFBox> tfToChange)->void;        
        auto SetImageParentDir(const QString& path)->void;
        auto Calc2DHistogram(SoVolumeData* volume,bool isLDM)->void;
        auto GetSceneGraph()->SoSeparator*;        
        auto GetGradMinMax(double& min, double& max)->void;
        auto SetHTMinMax(double min, double max)->void;
        auto SetFilePath(const QString& path)->void;        

    protected:
        virtual ~TFHiddenScene();
        auto BuildSceneGraph()->void;
        auto toScreenX(double val)->double;
        auto toScreenY(double val)->double;
        auto map(unsigned short value, unsigned short originalRangeMin, unsigned short originalRangeMax, unsigned short targetedRangeMin, unsigned short targetedRangeMax) -> unsigned short;

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}