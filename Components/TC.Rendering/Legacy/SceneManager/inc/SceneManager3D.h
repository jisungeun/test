#pragma once

#include <memory>

#include "TCSceneManagerExport.h"

class QOivRenderWindow;
class SoSeparator;

namespace TC {
    class TCSceneManager_API SceneManager3D {
    public:
        explicit SceneManager3D();
        virtual ~SceneManager3D();
        //prerequisite
        auto SetTF2DScene(SoSeparator* scene)->void;
        auto SetRenderWindow(QOivRenderWindow* window)->void;

        //common
        auto BuildSceneGraph3D()->bool;

        //HT control

        //FL control

    protected:        

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}