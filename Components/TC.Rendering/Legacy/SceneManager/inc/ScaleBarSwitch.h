#pragma once

#include <memory>
#include <tuple>

#include "TCSceneManagerExport.h"

class SoSwitch;
class SoCamera;

namespace TC {
	class TCSceneManager_API ScaleBarSwitch {
	public:
		ScaleBarSwitch(SoCamera* cam);
		~ScaleBarSwitch();

		auto GetRoot() -> SoSwitch*;
		auto SetColor(float r, float g, float b) -> void;
		auto GetColor() -> std::tuple<float, float, float>;
		auto SetLineWidth(float width) -> void;
		auto GetLineWidth() -> float;
		auto SetPosition(float x, float y) -> void;
		auto GetPosition() -> std::tuple<float, float>;

		auto SetTextVisible(bool visible) const -> void;;
		auto GetTextVisible() const -> bool;;
		auto SetTick(const int& tick) const -> void;
		auto GetTick() const -> int;
		auto SetLength(const float& length) const -> void;
		auto GetLength() const -> float;
		auto SetHorizontal(bool isHorizontal) const -> void;
		auto IsHorizontal() const -> bool;

	protected:
		auto Init() -> void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
