#pragma once

#include <memory>

#include <TCFMetaReader.h>

#include "TCSceneManagerExport.h"

class SoSwitch;

namespace TC {
    class TCSceneManager_API TimeStampSwitch {
    public:
        TimeStampSwitch();
        ~TimeStampSwitch();

        auto GetRoot()->SoSwitch*;

        auto SetImageMetaInfo(IO::TCFMetaReader::Meta::Pointer metaInfo)->void;
        auto SetColor(float r, float g, float b)->void;
        auto GetColor()->std::tuple<float, float, float>;
        auto SetPosition(float x, float y, float z=0)->void;
        auto GetPosition()->std::tuple<float, float, float>;
        auto SetFontSize(float size)->void;
        auto GetFontSize()->float;

        auto SetTimeStep(int step)->void;        

    protected:
        auto Init()->void;
        auto BuildTimeText()->void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}