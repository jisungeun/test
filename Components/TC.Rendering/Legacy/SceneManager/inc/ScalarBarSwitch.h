#pragma once

#include <memory>
#include <tuple>

#include "TCSceneManagerExport.h"

class SoSwitch;
class SoTransferFunction;

namespace TC {
	class TCSceneManager_API ScalarBarSwitch {
	public:
		ScalarBarSwitch();
		~ScalarBarSwitch();

		auto GetRoot() -> SoSwitch*;
		auto SetTransferFunction(SoTransferFunction* tf) -> void;
		auto GetTransferFunction() -> SoTransferFunction*;
		auto SetPosition(float x, float y) -> void;
		auto GetPosition() -> std::tuple<float, float>;
		auto SetRatio(float ratio) -> void;
		auto GetRatio() -> float;
		auto SetMajorLength(float len) -> void;
		auto GetMajorLength() -> float;
		auto SetNumberOfAnnotation(int num) -> void;
		auto GetNumberOfAnnotation() -> int;
		auto SetHorizontal(bool isHori) -> void;
		auto IsHorizontal() -> bool;
		auto SetImageRange(float min, float max) -> void;
		auto SetImageWindow(float min, float max) -> void;

		auto SetFontSize(float size) -> void;
		auto GetFontSize() -> float;
		auto SetSize(float w, float h) -> void;
		auto GetSize(float& w, float& h) -> void;
		auto SetLastAnnotationMargin(int margin) -> void;
		auto GetLastAnnotationMargin() -> int;
		auto SetReverseTextColor(bool reverse) -> void;
		auto SetPrecision(int prec) -> void;

	protected:
		auto Init() -> void;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
