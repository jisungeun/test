#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

#include "TCSceneManagerExport.h"

namespace TC {
    class TCSceneManager_API SceneManagerYZ {
    public:
        explicit SceneManagerYZ();        

    protected:
        virtual ~SceneManagerYZ();

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}