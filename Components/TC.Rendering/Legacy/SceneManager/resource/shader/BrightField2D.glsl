// RGB fragment shader

//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizGetData_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
    VVIZ_DATATYPE data_value1 = VVizGetData(data1, texCoord);
    VVIZ_DATATYPE data_value2 = VVizGetData(data2, texCoord);
    VVIZ_DATATYPE data_value3 = VVizGetData(data3, texCoord);

    float redf = VVizGetLuminance(data_value1);
    float greenf = VVizGetLuminance(data_value2);
    float bluef = VVizGetLuminance(data_value3);

    // Lookup to get color/alpha from float value
    vec4 color = vec4(redf, greenf, bluef, 1.0);
    //vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

    return color;
}
