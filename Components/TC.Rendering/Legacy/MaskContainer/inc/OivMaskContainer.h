#pragma once

#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "TCMaskContainerExport.h"

class TCMaskContainer_API OivMaskContainer {
public:
	OivMaskContainer();
	~OivMaskContainer();


	//general for single layer
	auto buildRootSlice(int idx)->void;
	auto buildMaskVolume(int step)->void;

	//2D for single layer
	auto setMaskSliceIndex(int index, int axis)->void;
	auto getMaskSliceIndex(int axis)->int;
	auto setMaskSlicePhyx(float pos, int axis)->void;
	auto getMaskSlicePhyx(int axis)->float;
	auto setMaskTransparency(float trans)->void;
	auto getMask2DRenderRoot(int)->SoSeparator*;

	//3D for single layer
	auto getMaskRenderRoot(void)->SoSeparator*;	
	auto setSingleMaskVolume(SoVolumeData* vol, int step)->void;
	auto getSharedMaskGroup(void)->SoGroup*;

	//2D for multi layer

	//3D for multi layer
	auto getSharedMaskGroup(int idx)->SoGroup*;
	auto addLayerMask(QString name,int steps)->void;
	auto getLayerNum(void)->int;
	auto setLayerMaskVolume(SoVolumeData* vol, int step, QString name)->void;	
	auto setLayerMaskVisibility(QString name, bool isVisible)->void;
	auto setLayerMaskOpacity(QString name, float opacity)->void;
	auto clearMaskLayer()->void;
	auto clearMaskLayer(QStringList names)->void;

private:
	auto buildMaskSingleSlice(int steps)->void;
	auto setSharedMaskData(SoGroup* grp)->void;

	struct Impl;
	std::unique_ptr < Impl> d;
};