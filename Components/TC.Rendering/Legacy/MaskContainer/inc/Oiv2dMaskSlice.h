#pragma once

#include <memory>

#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

class Oiv2dMaskSlice {
public:
    Oiv2dMaskSlice();
    ~Oiv2dMaskSlice();

    auto getRenderRootNode(int idx)->SoSeparator*;
    auto setSlice(int sliceNum, int m_ax)->void;
    auto getSlice(int m_ax)->int;

    auto setSlicePhyx(float position, int m_ax)->void;
    auto getSlicePhyx(int m_ax)->float;

    auto setDataGroup(SoGroup* dataGroup)->void;//set shared data first
    auto buildSingleSlice(int steps)->void;

    auto setOpacity(float opa)->void;
    auto setSliceInfo(void)->void;

    //layer version
    auto setLayerOpacity(float opa, QString name)->void;
    auto buildLayerSlice(int steps)->void;
    auto addDataGroup(SoGroup* dataGroup,QString anme)->void;
    auto addSingleSlice(int steps)->void;
    auto setDataGroup(SoGroup* dataGroup, QString name)->void;
    auto setSliceInfo(QString name)->void;

    auto clearMaskLayer(void)->void;
    auto clearMaskLayer(QStringList names)->void;

    auto removeMaskLayer(QString name)->void;

private:
    auto findSlice(int m_ax)->int;
    auto findLayerIndex(QString name)->int;

    struct Impl;
    std::unique_ptr<Impl> d;
};