#pragma once

#include <memory>

#include "TCMaskContainerExport.h"

class SoVolumeData;

class TCMaskContainer_API OivMaskGenerator {
	typedef OivMaskGenerator Self;
	typedef std::shared_ptr<Self> Pointer;
public:
	OivMaskGenerator();
	virtual ~OivMaskGenerator();

	auto Clear(bool force = false)->void;
	auto GetCurrentResult()->SoVolumeData*;
	auto AppendMask(QString maskName, SoVolumeData* maskData)->void;
	auto DeleteMask(QString maskName)->void;
	auto SetMaskOrder(QStringList maskOrder = QStringList())->void;	

private:
	auto DefaultOrder()->void;	

	struct Impl;
	std::unique_ptr<Impl> d;
};