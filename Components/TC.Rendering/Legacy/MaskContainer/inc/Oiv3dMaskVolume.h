#pragma once


#include <memory>
#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

class Oiv3dMaskVolume {
public:
	Oiv3dMaskVolume();
	~Oiv3dMaskVolume();

	auto buildSceneGraphMask(int step)->void;
	auto getSceneGraphRoot(void)->SoSeparator*;
	auto getNumberOfLabel(void)->int;

	//set loaded mask
	auto setMaskVolume(SoVolumeData* maskVol,int step)->void;

	//mask visualization components
	auto setVisablity(bool visibility)->void;
	auto setVisablity(int labelIdx, bool visibility)->void;
	auto setTransparency(double trans)->void;

	auto setColor(int labelIdx, SbColor col)->void;
	auto setLabelName(int labelIdx, QString name)->void;

	auto getMaskVolumeDataGroup(void)->SoGroup*;
	auto getNumberOfMaskLayers(void)->int;

	auto clearVolume(void)->void;

	//multi layer
	auto setMaskVolume(SoVolumeData* maskVol, int step, QString name)->void;
	auto addMaskLayer(QString name)->void;
	auto removeMaskLayer(QString name)->void;
	auto setLayerVisibility(QString name, bool isVisible)->void;
	auto setLayerTransparency(QString name, float trans)->void;

	auto clearMaskLayer(void)->void;
	auto clearMaskLayer(QStringList names)->void;

	auto getMaskVolumeDataGroup(QString name)->SoGroup*;
	auto setColor(int layer_idx, int labelIdx, SbColor col,float trans = -1.0)->void;

private:
	auto calcNumberOfLabel(void)->void;
	auto buildLabelTF(void)->void;

	//multi layer
	auto findLayerIndex(QString name)->int;
	auto calcNumberOfLabel(int idx)->void;
	auto buildLabelTF(int idx,int organ = 0)->void;

	struct Impl;
	std::unique_ptr<Impl> d;
};