#include <QStringList>

#pragma warning(push)
#pragma warning(disable : 4819)

#include <Inventor/SoInput.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <LDM/nodes/SoMultiDataSeparator.h>

// VolumeViz Header
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#pragma warning(pop)

#include "Oiv2dMaskSlice.h"
#include "Oiv3dMaskVolume.h"

#include "OivMaskContainer.h"

struct OivMaskContainer::Impl {

    SoSeparator* renderRoot2D[3];
    SoSwitch* sliceRoot[3];
    SoOrthographicCamera* orthoCam[3];

    Oiv2dMaskSlice* mask2D;
    Oiv3dMaskVolume* mask3D;
};

OivMaskContainer::OivMaskContainer() : d{ new Impl } {
    for(int i=0;i<3;i++) {
        d->renderRoot2D[i] = new SoSeparator;
        d->sliceRoot[i] = new SoSwitch;
        d->orthoCam[i] = new SoOrthographicCamera;
    }
    d->mask2D = new Oiv2dMaskSlice;
    d->mask3D = new Oiv3dMaskVolume;
}

OivMaskContainer::~OivMaskContainer() {
    
}

auto OivMaskContainer::buildRootSlice(int idx) -> void {
    if (d->mask2D->getRenderRootNode(idx)) {
        d->sliceRoot[idx]->addChild(d->mask2D->getRenderRootNode(idx));
    }
}

auto OivMaskContainer::buildMaskVolume(int step) -> void {
    d->mask3D->buildSceneGraphMask(step);//empty scene graph 3D

    //setSharedMaskData(d->mask3D->getMaskVolumeDataGroup());
    //buildMaskSingleSlice(step);
    d->mask2D->buildLayerSlice(step);//empty scene graph 2D
}

auto OivMaskContainer::getMaskRenderRoot() -> SoSeparator* {
    return d->mask3D->getSceneGraphRoot();
}

auto OivMaskContainer::addLayerMask(QString name,int steps) -> void {
    d->mask3D->addMaskLayer(name);
    d->mask2D->addDataGroup(d->mask3D->getMaskVolumeDataGroup(name),name);    
    d->mask2D->addSingleSlice(steps);
}

auto OivMaskContainer::getLayerNum() -> int {
    return d->mask3D->getNumberOfMaskLayers();
}


auto OivMaskContainer::clearMaskLayer(QStringList names) -> void {
    d->mask3D->clearMaskLayer(names);
    d->mask2D->clearMaskLayer(names);
}

auto OivMaskContainer::clearMaskLayer() -> void {
    d->mask3D->clearMaskLayer();
    d->mask2D->clearMaskLayer();
}

auto OivMaskContainer::getSharedMaskGroup() -> SoGroup* {
    return d->mask3D->getMaskVolumeDataGroup();    
}

auto OivMaskContainer::setLayerMaskVolume(SoVolumeData* vol, int step, QString name) -> void {
    d->mask3D->setMaskVolume(vol, step, name);
    //sharing group node for 2D slice node required
    d->mask2D->setDataGroup(d->mask3D->getMaskVolumeDataGroup(name), name);
    d->mask2D->setSliceInfo(name);
}

auto OivMaskContainer::setLayerMaskVisibility(QString name, bool isVisible) -> void {
    d->mask3D->setLayerVisibility(name, isVisible);
    if (isVisible) {        
        d->mask2D->setLayerOpacity(0.7f, name);
    }else {        
        d->mask2D->setLayerOpacity(0.0f, name);
    }
}

auto OivMaskContainer::setLayerMaskOpacity(QString name, float opacity) -> void {
    d->mask3D->setLayerTransparency(name, 1.0 - opacity);
    d->mask2D->setLayerOpacity(opacity, name);
}

auto OivMaskContainer::setSingleMaskVolume(SoVolumeData* vol, int step) -> void {
    d->mask3D->setMaskVolume(vol, step);
    d->mask2D->setDataGroup(d->mask3D->getMaskVolumeDataGroup());
    d->mask2D->setSliceInfo();
}


auto OivMaskContainer::buildMaskSingleSlice(int steps) -> void {
    d->mask2D->buildSingleSlice(steps);
}

auto OivMaskContainer::getMask2DRenderRoot(int index) -> SoSeparator* {
    	return d->mask2D->getRenderRootNode(index);
}

auto OivMaskContainer::getMaskSliceIndex(int axis) -> int {
    return d->mask2D->getSlice(axis);
}

auto OivMaskContainer::getMaskSlicePhyx(int axis) -> float {
    return d->mask2D->getSlicePhyx(axis);
}

auto OivMaskContainer::setMaskSliceIndex(int index, int axis) -> void {
    d->mask2D->setSlice(index, axis);
}

auto OivMaskContainer::setMaskSlicePhyx(float pos, int axis) -> void {
    d->mask2D->setSlicePhyx(pos, axis);
}

auto OivMaskContainer::setMaskTransparency(float trans) -> void {
    d->mask2D->setOpacity(1.0 - trans);
    d->mask3D->setTransparency(trans);
}

auto OivMaskContainer::setSharedMaskData(SoGroup* grp) -> void {
    d->mask2D->setDataGroup(grp);
}


auto OivMaskContainer::getSharedMaskGroup(int idx) -> SoGroup* {
    Q_UNUSED(idx)
    return nullptr;
}
