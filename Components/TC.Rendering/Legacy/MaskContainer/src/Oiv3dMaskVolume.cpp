#include <QVector>
#include <QStringList>

//Open Inventor headers
#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <Inventor/nodes/SoMaterial.h>
#pragma warning(pop)

#include "Oiv3dMaskVolume.h"

#define BUFFER_COUNT 2

float color_table[20][3]{
    230,25,75, //red
    60,180,75, //green
    255,255,25, //yellow
    0,130,200, //blue
    245,130,48, //Orange
    145,30,180, //Purple
    70,240,240, //Cyan
    240,50,230, //Magenta
    210,245,60, //Lime
    250,190,212, //Pink
    0,128,128, //Teal
    220,190,255, //Lavender
    170,110,40, //Brown
    255,250,200, //Beige
    128,0,0,//Maroon
    170,255,195, //Mint
    128,128,0, //Olive
    255,215,180, //Apricot
    0,0,128, //Navy
    128,128,128 //Gray
};
//https://sashamaps.net/docs/tools/20-colors/

typedef struct single_mask{
    SoSeparator* layerRoot;
    SoGroup* dataGroup;
    SoSwitch* time_stepper;
    SoVolumeData* maskVolume[BUFFER_COUNT];

    SoMaterial* maskMatl;
    SoTransferFunction* tf;
    SoDataRange* range;
    SoVolumeRenderingQuality* qual;
    SoVolumeRender* render;
    int labelN;
    QString name;
    bool maskExist;
    float opacity;

    QStringList label_names;
}MaskType;

struct Oiv3dMaskVolume::Impl {
    //SceneGraph Structure for mask volume 
    SoSeparator* maskRoot;
    SoMaterial* maskMatl;

    //reader를 여러개를 두어 교체하는 방식으로 관리하면
    //동일한 기준으로 작성된 single mask volume으로 해결이 가능하다
    //Multi segmentation data 같은 경우 image append를 통해 하나의
    //Label volume으로 합쳐준다. - deprecated
    //multi layer의 경우 scenegraph를 여러개 생성해서 같이 보여준다


    //single layer
    ////////////////////////////////////////////////

    //Shared variables
    SoGroup* dataGroup;
    SoSwitch* time_stepper;
    SoVolumeData* maskVolume[BUFFER_COUNT];

    //Group for proper volume visualization
    SoTransferFunction* tf;
    SoDataRange* range;
    SoVolumeRenderingQuality* qual;
    SoVolumeRender* render;

    //label Control
    int labelN{ 0 };
    QStringList label_name;
    bool maskExist{ false };

    //visualization    
    float opacity = { 0.7f };

    //multi layer
    /////////////////////////////////////////////////

    //shared variablees
    QList<MaskType> mask_layers;
    /////////////////////////////////////////////////
    
    //time control
    int time_step{ 0 };
    int time_steps{ 0 };  
};

Oiv3dMaskVolume::Oiv3dMaskVolume() : d{ new Impl } {
    
}

Oiv3dMaskVolume::~Oiv3dMaskVolume() {
    
}

auto Oiv3dMaskVolume::getNumberOfMaskLayers(void)->int
{
    return d->mask_layers.size();
}


auto Oiv3dMaskVolume::setMaskVolume(SoVolumeData* maskVol, int step, QString name) -> void {
    auto idx = findLayerIndex(name);
    if (idx >= 0) {
        d->mask_layers[idx].time_stepper->replaceChild(step % BUFFER_COUNT, maskVol);        
        d->mask_layers[idx].time_stepper->whichChild = step;

        d->mask_layers[idx].maskVolume[step % BUFFER_COUNT] = maskVol;
        if(step==0) {
            calcNumberOfLabel(idx);
            if (name.contains("membrane"))
                buildLabelTF(idx, 0);
            else if (name.contains("nucleus"))
                buildLabelTF(idx, 1);
            else if (name.contains("nucleoli"))
                buildLabelTF(idx, 2);
            else if (name.contains("lipid droplet"))
                buildLabelTF(idx, 3);
            else
                buildLabelTF(idx);
        }
        d->mask_layers[idx].maskExist = true;
    }
}

auto Oiv3dMaskVolume::setMaskVolume(SoVolumeData* maskVol, int step) -> void {
    d->time_stepper->replaceChild(step % BUFFER_COUNT, maskVol);
    d->time_stepper->whichChild = step;//TODO: Temporal
    d->maskVolume[step % BUFFER_COUNT] = maskVol;
    if(step==0) {        
        calcNumberOfLabel();
        buildLabelTF();
    }
    d->maskExist = true;
}

auto Oiv3dMaskVolume::setVisablity(bool visibility) -> void {
    //whole visability
    if(d->maskMatl){
        if (visibility) {
            d->maskMatl->transparency.setValue(d->opacity);
        }else {
            d->maskMatl->transparency.setValue(0.0);
        }
    }
}

auto Oiv3dMaskVolume::getMaskVolumeDataGroup() -> SoGroup* {
    //share mask volume data via sharing data group
    return d->dataGroup;
}

auto Oiv3dMaskVolume::getMaskVolumeDataGroup(QString name) -> SoGroup* {
    auto idx = findLayerIndex(name);
    if(idx>=0) {
        return d->mask_layers[idx].dataGroup;
    }
    return nullptr;
}


auto Oiv3dMaskVolume::setLayerVisibility(QString name, bool isVisible) -> void {
    auto idx = findLayerIndex(name);
    if(idx>=0) {
        if(isVisible) {
            d->mask_layers[idx].maskMatl->transparency = 1.0 - d->mask_layers[idx].opacity;;
        }else {
            d->mask_layers[idx].maskMatl->transparency = 1.0;
                
        }
    }
}

auto Oiv3dMaskVolume::setLayerTransparency(QString name, float trans) -> void {
    auto idx = findLayerIndex(name);
    if (idx >= 0) {
        d->mask_layers[idx].opacity = 1.0 - trans;
        d->mask_layers[idx].maskMatl->transparency = trans;
    }
}

auto Oiv3dMaskVolume::clearMaskLayer() -> void {
    for(int i=0;i<d->mask_layers.size();i++){
        d->maskRoot->removeChild(d->mask_layers[i].layerRoot);        
    }
    d->mask_layers.clear();
}

auto Oiv3dMaskVolume::clearMaskLayer(QStringList names)->void {
    for(int i=0;i<names.size();i++) {
        auto idx = findLayerIndex(names[i]);
        if(idx>=0) {
            d->maskRoot->removeChild(d->mask_layers[idx].layerRoot);
            for(int j = idx;j<d->mask_layers.size()-1;j++) {
                d->mask_layers[j] = d->mask_layers[j + 1];
            }
            d->mask_layers.pop_back();
        }
    }
}

auto Oiv3dMaskVolume::removeMaskLayer(QString name) -> void {
    auto idx = findLayerIndex(name);
    if(idx>=0) {
        d->maskRoot->removeChild(d->mask_layers[idx].layerRoot);
        for (int i = idx; i < d->mask_layers.size() - 1; i++) {
            d->mask_layers[i] = d->mask_layers[i + 1];
        }
        d->mask_layers.pop_back();
    }
}



auto Oiv3dMaskVolume::addMaskLayer(QString name) -> void {    
    MaskType newLayer;
    
    newLayer.opacity = 0.1f;
    newLayer.name = name;
    newLayer.labelN = 0;
    newLayer.maskExist = false;

    newLayer.layerRoot = new SoSeparator;

    newLayer.maskMatl = new SoMaterial;

    newLayer.layerRoot->addChild(newLayer.maskMatl);
    newLayer.maskMatl->transparency = 0.9f;

    newLayer.dataGroup = new SoGroup;
    newLayer.layerRoot->addChild(newLayer.dataGroup);
    //d->maskRoot->addChild(newLayer.layerRoot);    

    newLayer.time_stepper = new SoSwitch;        
    newLayer.time_stepper->setName(SbName(std::string("mask_timeStepper.") + name.toStdString()));

    for (int i = 0; i < BUFFER_COUNT; i++) {
        newLayer.maskVolume[i] = new SoVolumeData;
        newLayer.time_stepper->addChild(newLayer.maskVolume[i]);        
    }        
    newLayer.dataGroup->addChild(newLayer.time_stepper);

    newLayer.range = new SoDataRange;    
    newLayer.dataGroup->addChild(newLayer.range);
    newLayer.tf = new SoTransferFunction;    

    newLayer.dataGroup->addChild(newLayer.tf);

    //volume rendering properties
    newLayer.layerRoot->addChild(d->qual);

    newLayer.render = new SoVolumeRender;    
    newLayer.layerRoot->addChild(newLayer.render);

    d->mask_layers.push_back(newLayer);

    d->maskRoot->addChild(newLayer.layerRoot);
}

auto Oiv3dMaskVolume::buildSceneGraphMask(int step) -> void {
    Q_UNUSED(step)
    d->maskRoot = new SoSeparator;
    d->maskRoot->setName("Mask_renderRootSep");
    //d->maskMatl = new SoMaterial;
    //d->maskMatl->transparency.setValue(d->opacity);

    d->qual = new SoVolumeRenderingQuality;
    d->qual->interpolateOnMove = TRUE;
    d->qual->preIntegrated = FALSE;
    d->qual->lighting = FALSE;
    d->qual->colorInterpolation = FALSE;
    d->qual->ambientOcclusion = TRUE;
    d->qual->deferredLighting = FALSE;
}

auto Oiv3dMaskVolume::setTransparency(double trans) -> void {
    d->opacity = trans;    
}

auto Oiv3dMaskVolume::setLabelName(int labelIdx, QString name) -> void {
    //nodify label name is changed - later 20.07.08 Jose T. Kim
    if(labelIdx < d->label_name.size() && labelIdx > -1) {
        d->label_name[labelIdx] = name;
    }
}

auto Oiv3dMaskVolume::findLayerIndex(QString name) -> int {
    int idx = -1;

    for(int i=0;i<d->mask_layers.size();i++) {
        if(d->mask_layers[i].name.compare(name) == 0) {
            idx = i;
        }
    }
    return idx;
}

auto Oiv3dMaskVolume::setColor(int layer_idx, int labelIdx, SbColor col,float trans) -> void {
    if(d->mask_layers.size()>layer_idx) {
        if(d->mask_layers[layer_idx].labelN > labelIdx) {
            float* p = d->mask_layers[layer_idx].tf->colorMap.startEditing();
            for(int i=0;i<256;++i) {
                int ii = (float)i / 255.0 * (float)(d->mask_layers[layer_idx].labelN + 0.1);
                if (ii ==  labelIdx) {
                    float r = col[0];
                    float g = col[1];
                    float b = col[2];
                    *p++ = r;
                    *p++ = g;
                    *p++ = b;
                    if (trans >= 0.0)
                        *p++ = trans;
                    else
                        *p++ = d->mask_layers[layer_idx].opacity;
                } else {
                    *p++;// = 0;
                    *p++;// = 0;
                    *p++;// = 0;
                    *p++;// = 0;
                }
            }
        }
    }
}


auto Oiv3dMaskVolume::setColor(int labelIdx, SbColor col) -> void {
    //특정 Label을 특정 color로 바꾸고 싶을때
    if (d->tf) {
        if (labelIdx > 0 && labelIdx < d->labelN + 1) {
            float* p = d->tf->colorMap.startEditing();
            for (int i = 0; i < 256; ++i) {
                int idx = i * d->labelN / 256;
                if (idx == labelIdx) {
                    *p++ = col[0];
                    *p++ = col[1];
                    *p++ = col[2];
                    *p++ = d->opacity;
                } else {//passing unwanted color points
                    *p++;
                    *p++;
                    *p++;
                    *p++;
                }
            }
            d->tf->colorMap.finishEditing();
        }
    }
}

auto Oiv3dMaskVolume::setVisablity(int labelIdx, bool visibility) -> void {
    //특정 Label만 보이지 않게 하고 싶을 때
    if (d->tf) {
        if (labelIdx > 0 && labelIdx < d->labelN + 1) {
            float* p = d->tf->colorMap.startEditing();
            for (int i = 0; i < 256; ++i) {
                int idx = i * d->labelN / 256;
                if (idx == labelIdx) {
                    *p++;//sustain previous colors
                    *p++;
                    *p++;
                    if (visibility)
                        *p++ = d->opacity;
                    else
                        *p++ = 0.0;//Open Inventor의 Transparency 와 반대되는 value이다
                } else {//passing unwanted color points
                    *p++;
                    *p++;
                    *p++;
                    *p++;
                }
            }
            d->tf->colorMap.finishEditing();
        }
    }
}

auto Oiv3dMaskVolume::buildLabelTF(int idx,int organ) -> void {
    if (d->mask_layers[idx].tf) {
        d->mask_layers[idx].tf->colorMap.setNum(256 * 4);
        float* p = d->mask_layers[idx].tf->colorMap.startEditing();
        for (int i = 0; i < 255; ++i) {            
            int ii = (float)i / 255.0 * (float)(d->mask_layers[idx].labelN + 0.1);
            if (ii > 0) {
                float r = color_table[(ii - 1 + organ) % 20][0] / 255.0;
                float g = color_table[(ii - 1 + organ) % 20][1] / 255.0;
                float b = color_table[(ii - 1 + organ) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 0.9f;
            } else {
                *p++ = 0.f;
                *p++ = 0.f;
                *p++ = 0.f;
                *p++ = 0.f;
            }
        }
        d->mask_layers[idx].tf->colorMap.finishEditing();
    }
}

auto Oiv3dMaskVolume::buildLabelTF() -> void {
    //Mask의 특징에 따라 최초 transfer function을 제작
    //항상 volume이 assign되고 label number가 계산한 뒤 호출
    if (d->tf) {
        d->tf->colorMap.setNum(256 *4);

        float* p = d->tf->colorMap.startEditing();
        for (int i = 0; i < 256; ++i) {
            //int idx = (float)i * (d->labelN+0.5) / 255.0;
            int idx = (float)i / 255.0 * (float)(d->labelN);
            if (true) {
                float r = color_table[(idx) % 20][0] / 255.0;
                float g = color_table[(idx) % 20][1] / 255.0;
                float b = color_table[(idx) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = d->opacity;
            }
        }
        d->tf->colorMap.finishEditing();
    }
}

auto Oiv3dMaskVolume::clearVolume() -> void {
    for(int i=0;i<BUFFER_COUNT;i++) {
        d->maskVolume[i] = nullptr;
    }
    d->labelN = 0;
    d->time_step = 0;
    d->time_steps = 0;
    d->tf = nullptr;
    d->label_name.clear();
}

auto Oiv3dMaskVolume::calcNumberOfLabel(int idx) -> void {
    if(d->mask_layers[idx].maskVolume[0]) {
        double min, max;
        d->mask_layers[idx].maskVolume[0]->getMinMax(min, max);
        d->mask_layers[idx].labelN = max;
        for(int i=0;i<max;i++) {
            QString temp("label");
            auto default_name = temp + QString(std::to_string(i + 1).c_str());
            d->mask_layers[idx].label_names.push_back(default_name);
        }
        //TODO 이상동작 원인 규명 필요
        d->mask_layers[idx].range->min.setValue(min-0.6);
        d->mask_layers[idx].range->max.setValue(max+0.1);
    }
}


auto Oiv3dMaskVolume::calcNumberOfLabel() -> void {
    //항상 volume이 assign 되고 난 뒤 호출
    if (d->maskVolume[0]) {
        double min, max;
        d->maskVolume[0]->getMinMax(min, max);
        //무조건 1, 2, 3, 순서의 오름차순으로 label value를 구성
        d->labelN = max;
        for(int i=0;i<max;i++) {
            QString temp("label");
            auto default_name = temp + QString(std::to_string(i + 1).c_str());
            d->label_name.push_back(default_name);
        }
        d->range->min.setValue(min);
        d->range->max.setValue(max);
    }
}

auto Oiv3dMaskVolume::getNumberOfLabel() -> int {
    return d->labelN;
}

auto Oiv3dMaskVolume::getSceneGraphRoot() -> SoSeparator* {
    return d->maskRoot;
}