//Qt
#include <QMap>
#pragma warning(push)
#pragma warning(disable:4819)
//Inventor
#include <Inventor/nodes/SoSeparator.h>

//ImageViz
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoArithmeticValueProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoCombineByMaskProcessing.h>

//VolumeViz
#include <VolumeViz/nodes/SoVolumeData.h>

//Medical
#include <Medical/helpers/MedicalHelper.h>

#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#pragma warning(pop)

#include "OivMaskGenerator.h"

struct OivMaskGenerator::Impl {
    //SoVolumeData* result{nullptr};
    QMap<QString,SoVolumeData*> maskVolume;
    QStringList maskOrder;

    bool hasMembrane{ false };
};

OivMaskGenerator::OivMaskGenerator() :d {new Impl} {
    
}

OivMaskGenerator::~OivMaskGenerator() = default;

auto OivMaskGenerator::AppendMask(QString maskName, SoVolumeData* maskData) -> void {
    d->maskOrder.push_back(maskName);
    d->maskVolume[maskName] = maskData;
    d->maskVolume[maskName]->ref();
    /*if (nullptr == result) {
        result = new SoVolumeData;
        result->ref();
        auto dim = d->maskVolume[maskName]->getDimension();
        std::shared_ptr<unsigned short> memBuf(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
        result->data.setValue(dim, SbDataType::UNSIGNED_SHORT, (void*)memBuf.get(), SoSFArray::COPY);
        result->extent.setValue(d->maskVolume[maskName]->extent.getValue());
        memBuf = nullptr;
    }*/
}

auto OivMaskGenerator::Clear(bool force) -> void {
    /*if (force) {
        if (nullptr != result) {
            while (result->getRefCount() > 0) {
                result->unref();
            }            
        }
        result = nullptr;
    }
    if(nullptr != result) {
        auto dim = result->getDimension();
        std::shared_ptr<unsigned short> memBuf(new unsigned short[dim[0]*dim[1]*dim[2]](), std::default_delete<unsigned short[]>());
        result->data.setValue(dim, SbDataType::UNSIGNED_SHORT, (void*)memBuf.get(), SoSFArray::COPY);
        memBuf = nullptr;
    }*/    
    if (force) {
        d->maskOrder.clear();
        for (auto maskVol : d->maskVolume) {
            while (maskVol->getRefCount() > 0) {
                maskVol->unref();
            }
            maskVol = nullptr;
        }
        d->maskVolume.clear();
    }
}

auto OivMaskGenerator::SetMaskOrder(QStringList maskOrder) -> void {
    if(maskOrder.count()<1) {
        DefaultOrder();
    }
    else {
        d->maskOrder = maskOrder;
    }
}

auto OivMaskGenerator::DeleteMask(QString maskName) -> void {
    if (d->maskOrder.contains(maskName)) {
        auto idx = d->maskOrder.indexOf(maskName);                
        d->maskOrder.removeAt(idx);
    }
    auto maskVol = d->maskVolume[maskName];
    while(maskVol->getRefCount()>0) {
        maskVol->unref();
    }
    maskVol = nullptr;
    d->maskVolume.remove(maskName);
}

auto OivMaskGenerator::DefaultOrder() -> void {
    QStringList result;
    auto hasInst = false;
    d->hasMembrane = false;
    //Cell Instance
    if(d->maskOrder.contains("cellInst")) {
        result.push_back("cellInst");
        hasInst = true;
    }
    //Membrane
    if (d->maskOrder.contains("membrane")) {
        d->hasMembrane = true;
        if (!hasInst) {
            result.push_back("membrane");
        }
    }
    //Nucleus
    if(d->maskOrder.contains("nucleus")) {
        result.push_back("nucleus");
    }
    //Nucleoli
    if(d->maskOrder.contains("nucleoli")) {
        result.push_back("nucleoli");
    }
    //Lipid
    if(d->maskOrder.contains("lipid droplet")) {
        result.push_back("lipid droplet");
    }
    d->maskOrder = result;
}

auto OivMaskGenerator::GetCurrentResult() -> SoVolumeData* {
    //perform merge mask
    if (d->maskOrder.count() < 1) {
        return nullptr;
    }        

    auto baseMask = d->maskVolume[d->maskOrder[0]];

    auto dim = baseMask->getDimension();
    auto dim_size = dim[0] * dim[1] * dim[2];
    
    std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
    SbDataType type = SbDataType::UNSIGNED_SHORT;
    
    auto result = new SoVolumeData;
    result->ref();

    result->data.setValue(SbVec3i32(dim), type, type.getNumBits(), volume.get(), SoSFArray::CopyPolicy::COPY);
    result->extent.setValue(baseMask->extent.getValue());

    double basemin, basemax;
    baseMask->getMinMax(basemin, basemax);

    volume = nullptr;

    SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
    SoLDMDataAccess::DataInfoBox dataInfoBox =
        result->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
    SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
    dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

    result->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
    unsigned short* base_pointer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_WRITE);


    SoRef<SoCpuBufferObject> refBufferObj = new SoCpuBufferObject;
    refBufferObj->setSize((size_t)dataInfoBox.bufferSize);

    baseMask->getLdmDataAccess().getData(0, bBox, refBufferObj.ptr());
    unsigned short* ref_pointer = (unsigned short*)refBufferObj->map(SoBufferObject::READ_ONLY);

    auto isInstWise = d->maskOrder[0] == "cellInst";

    double min, max;
    auto maxVal = 0;
    auto firstMask = true;
    for(auto i=0;i<d->maskOrder.count();i++) {
        //std::cout << d->maskOrder[i].toStdString() << std::endl;
        auto additiveVolume = d->maskVolume[d->maskOrder[i]];
        additiveVolume->getMinMax(min, max);
        additiveVolume->data.touch();            
        SoRef<SoCpuBufferObject> addBufferObj = new SoCpuBufferObject;
        addBufferObj->setSize((size_t)dataInfoBox.bufferSize);
        additiveVolume->getLdmDataAccess().getData(0, bBox, addBufferObj.ptr());
        unsigned short* add_pointer = (unsigned short*)addBufferObj->map(SoBufferObject::READ_ONLY);        
        if (max > 1) {//instance mask
            if (d->hasMembrane) {
                firstMask = false;
                for (auto p = 0; p < dim_size; p++) {
                    auto val = *(add_pointer + p);
                    *(base_pointer + p) += val;
                }
                maxVal += static_cast<int>(max);
            }
        }else {
            if(isInstWise) {
                if (firstMask) {
                    for (auto p = 0; p < dim_size; p++) {
                        auto val = *(add_pointer + p);
                        if (val > 0) {
                            *(base_pointer + p) = *(ref_pointer + p);
                        }
                    }
                    maxVal += static_cast<int>(basemax);
                    firstMask = false;
                }else {
                    maxVal += static_cast<int>(max);
                    for (auto p = 0; p < dim_size; p++) {
                        auto val = *(add_pointer + p);
                        if (val > 0 && *(ref_pointer + p) > 0) {
                            *(base_pointer + p) = maxVal;
                        }
                    }                    
                }
            }
            else {
                maxVal += static_cast<int>(max);
                for (auto p = 0; p < dim_size; p++) {
                    auto val = *(add_pointer + p);
                    if (val > 0) {
                        *(base_pointer + p) = maxVal;
                    }
                }                
            }
        }        
        addBufferObj->unmap();        
        addBufferObj = nullptr;
    }
    dataBufferObj->unmap();    
    refBufferObj->unmap();

    int trans;
    result->startEditing(trans);    
    result->editSubVolume(bBox, dataBufferObj.ptr());
    result->finishEditing(trans);
    result->saveEditing();

    result->getReader()->touch();        
        
    result->unrefNoDelete();

    dataBufferObj = nullptr;
    refBufferObj = nullptr;
        
    return result;
}