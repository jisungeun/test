#include <QList>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include "Oiv2dMaskSlice.h"

typedef struct slice_layer{
    SoSeparator* slice_root[3];

    SoMaterial* sliceMatl;
    SoGroup* sharedMaskData;
    SoOrthoSlice* sliceRender[3];

    float opacity;
    int numSlice[3];

    QString name;
}SliceType;

struct Oiv2dMaskSlice::Impl {    
    SoSeparator* renderRoot[3];
    SoMaterial* sliceMatl;

    SoGroup* sharedMaskData {nullptr};
    SoOrthoSlice* sliceRender[3];

    float opacity{0};
    int time_steps;    
    int numSlice[3];

    int cur_sliceN[3] = { -1,-1,-1 };

    //layer version
    QList<SliceType> slice_layers;
};

Oiv2dMaskSlice::Oiv2dMaskSlice() : d{ new Impl } {
    
}

Oiv2dMaskSlice::~Oiv2dMaskSlice() {
    
}

auto Oiv2dMaskSlice::setOpacity(float opa) -> void {
    d->opacity = opa;
}

auto Oiv2dMaskSlice::setLayerOpacity(float opa, QString name) -> void {
    auto idx = findLayerIndex(name);
    if(idx>-1) {
        d->slice_layers[idx].sliceMatl->transparency = 1.0 - opa;
    }
}


auto Oiv2dMaskSlice::addDataGroup(SoGroup* dataGroup,QString name) -> void {
    SliceType slice;
    slice.sharedMaskData = dataGroup;
    slice.opacity = 0.5;
    slice.name = name;
    for (int i = 0; i < 3; i++) slice.numSlice[0] = 0;
    d->slice_layers.push_back(slice);
}

auto Oiv2dMaskSlice::setDataGroup(SoGroup* dataGroup) -> void {
    d->sharedMaskData = dataGroup;
}

auto Oiv2dMaskSlice::setDataGroup(SoGroup* dataGroup, QString name) -> void {
    auto idx = findLayerIndex(name);
    if(idx>-1) {
        d->slice_layers[idx].sharedMaskData = dataGroup;
    }
}

auto Oiv2dMaskSlice::findLayerIndex(QString name) -> int {
    int idx = -1;
    for(int i=0;i<d->slice_layers.size();i++) {
        if(d->slice_layers[i].name.compare(name) == 0) {
            idx = i;
        }
    }
    return idx;
}

auto Oiv2dMaskSlice::clearMaskLayer() -> void {
    for(int i=0;i<d->slice_layers.size();i++) {
        for(int j=0;j<3;j++) {
            d->renderRoot[j]->removeChild(d->slice_layers[i].slice_root[j]);
        }
    }
    d->slice_layers.clear();
}

auto Oiv2dMaskSlice::clearMaskLayer(QStringList names) -> void {
    for(int i=0;i<names.size();i++) {
        auto idx = findLayerIndex(names[i]);
        if(idx>=0) {
            for(int j=0;j<3;j++) {
                d->renderRoot[j]->removeChild(d->slice_layers[idx].slice_root[j]);
            }
            for (int j = idx; j < d->slice_layers.size() - 1; j++) {
                d->slice_layers[j] = d->slice_layers[j + 1];
            }
            d->slice_layers.pop_back();
        }
    }
}


auto Oiv2dMaskSlice::removeMaskLayer(QString name) -> void {
    auto idx = findLayerIndex(name);
    if(idx>=0) {
        for(int i=0;i<3;i++) {
            d->renderRoot[i]->removeChild(d->slice_layers[idx].slice_root[i]);            
        }
        for (int j = idx; j < d->slice_layers.size() - 1; j++) {
            d->slice_layers[j] = d->slice_layers[j + 1];
        }
        d->slice_layers.pop_back();
    }
}


auto Oiv2dMaskSlice::setSliceInfo(QString name) -> void {
    auto idx = findLayerIndex(name);
    if(idx>-1) {
        MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
        if(d->cur_sliceN[0] == -1) {//한번도 정보 갱신이 안됬었다면            
            SoVolumeData* m_vol;
            m_vol = MedicalHelper::find<SoVolumeData>(d->slice_layers[idx].sharedMaskData, name.toStdString());
            for(int i=0;i<3;i++) {
                d->numSlice[i] = m_vol->data.getSize()[ax[i]];
                auto val = (d->numSlice[i] > 1) ? (d->numSlice[i] / 2) : 0;;
                d->slice_layers[idx].sliceRender[i]->sliceNumber = val;
                d->cur_sliceN[i] = val;
            }
        }else {//이외의 경우는 기존 slice 정보를 활용
            for (int i = 0; i < 3; i++) {
                d->slice_layers[idx].sliceRender[i]->sliceNumber = d->cur_sliceN[i];
            }        
        }
    }
}


auto Oiv2dMaskSlice::setSliceInfo() -> void {
    if (d->sharedMaskData != nullptr) {
        MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
        SoVolumeData* m_vol;

        m_vol = MedicalHelper::find<SoVolumeData>(d->sharedMaskData, "maskData");
        for (int i = 0; i < 3; i++) {
            d->numSlice[i] = m_vol->data.getSize()[ax[i]];
            d->sliceRender[i]->sliceNumber = (d->numSlice[i] > 1) ? (d->numSlice[i] / 2) : 0;
        }
    }
}

auto Oiv2dMaskSlice::buildLayerSlice(int steps) -> void {
    d->time_steps = steps;
    for (int i = 0; i < 3; i++) {
        d->renderRoot[i] = new SoSeparator;
        std::string nn("Mask2D_rootSep_");
        nn += std::to_string(i);
        d->renderRoot[i]->setName(nn.c_str());
    }
}

auto Oiv2dMaskSlice::addSingleSlice(int steps) -> void {
    Q_UNUSED(steps)
    MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
    auto cur_idx = d->slice_layers.size() - 1;
    if (cur_idx > -1) {
        if (d->slice_layers[cur_idx].sharedMaskData) {            
            d->slice_layers[cur_idx].sliceMatl = new SoMaterial;
            d->slice_layers[cur_idx].sliceMatl->transparency = 0.4f;
            for (int i = 0; i < 3; i++) {
                d->slice_layers[cur_idx].slice_root[i] = new SoSeparator;
                d->slice_layers[cur_idx].slice_root[i]->addChild(d->slice_layers[cur_idx].sliceMatl);
                d->slice_layers[cur_idx].slice_root[i]->addChild(d->slice_layers[cur_idx].sharedMaskData);

                d->slice_layers[cur_idx].sliceRender[i] = new SoOrthoSlice;
                d->slice_layers[cur_idx].sliceRender[i]->alphaUse = SoSlice::ALPHA_BLENDING;
                d->slice_layers[cur_idx].sliceRender[i]->useRGBA = FALSE;
                d->slice_layers[cur_idx].sliceRender[i]->axis = ax[i];
                d->slice_layers[cur_idx].sliceRender[i]->sliceNumber = d->numSlice[i] > 1 ? (d->numSlice[i] / 2) : 0;
                d->slice_layers[cur_idx].sliceRender[i]->interpolation = SoSlice::Interpolation::NEAREST;

                d->slice_layers[cur_idx].slice_root[i]->addChild(d->slice_layers[cur_idx].sliceRender[i]);

                d->renderRoot[i]->addChild(d->slice_layers[cur_idx].slice_root[i]);
            }
        }
    }
}

auto Oiv2dMaskSlice::buildSingleSlice(int steps) -> void {
    MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };

    //if(d->sharedMaskData != nullptr) {
    if(true){        
        if(true){
            d->time_steps = steps;
            d->sliceMatl = new SoMaterial;
            d->sliceMatl->transparency = 0.4f;

            for (int i = 0; i < 3; i++) {
                //d->numSlice[i] = m_vol->data.getSize()[ax[i]];

                d->renderRoot[i] = new SoSeparator;
                
                d->renderRoot[i]->addChild(d->sliceMatl);
                d->renderRoot[i]->addChild(d->sharedMaskData);

                d->sliceRender[i] = new SoOrthoSlice;
                d->sliceRender[i]->alphaUse = SoSlice::ALPHA_OPAQUE;
                d->sliceRender[i]->useRGBA = FALSE;
                d->sliceRender[i]->axis = ax[i];
                d->sliceRender[i]->sliceNumber = (d->numSlice[i] > 1) ? (d->numSlice[i] / 2) : 0;
                d->sliceRender[i]->interpolation = SoSlice::Interpolation::NEAREST;

                d->renderRoot[i]->addChild(d->sliceRender[i]);
                std::string nn("Mask2D_rootSep_");
                nn += std::to_string(i);
                d->renderRoot[i]->setName(nn.c_str());
            }
        }
    }
}

auto Oiv2dMaskSlice::getRenderRootNode(int idx) -> SoSeparator* {
    return d->renderRoot[idx];
}

auto Oiv2dMaskSlice::setSlice(int sliceNum, int m_ax) -> void {
    auto slice_idx = findSlice(m_ax);
    if (d->slice_layers[0].sliceRender[slice_idx]) {
        for (int i = 0; i < d->slice_layers.size(); i++) {
            if (d->numSlice[slice_idx] > sliceNum && sliceNum > -1) {
                d->slice_layers[i].sliceRender[slice_idx]->sliceNumber = sliceNum;
            }
        }
    }
}

auto Oiv2dMaskSlice::getSlice(int m_ax) -> int {
    auto slice_idx = findSlice(m_ax);
    //return d->sliceRender[slice_idx]->sliceNumber.getValue();
    if (d->slice_layers[0].sliceRender[slice_idx]) {
        return d->slice_layers[0].sliceRender[slice_idx]->sliceNumber.getValue();
    } else {
        return -1;
    }
}

auto Oiv2dMaskSlice::setSlicePhyx(float position, int m_ax) -> void {
    int slice_idx = findSlice(m_ax);
    SbVec3f xyzPos;
    xyzPos[slice_idx] = position;
    if (d->slice_layers[0].sharedMaskData) {
        SoVolumeData* m_vol;
        m_vol = MedicalHelper::find<SoVolumeData>(d->slice_layers[0].sharedMaskData, "maskData");

        SbVec3f voxelPos = m_vol->XYZToVoxel(xyzPos);

        if (d->numSlice[slice_idx] > voxelPos[slice_idx] && voxelPos[slice_idx] > -1) {
            //d->sliceRender[slice_idx]->sliceNumber = voxelPos[slice_idx];
            for(int i=0;i<d->slice_layers.size();i++) {
                d->slice_layers[i].sliceRender[slice_idx]->sliceNumber = voxelPos[slice_idx];
            }
        }
    }
}

auto Oiv2dMaskSlice::getSlicePhyx(int m_ax) -> float {
    int slice_idx = findSlice(m_ax);
    if (d->slice_layers[0].sliceRender[slice_idx]) {
        SbVec3f voxelPos;
        int sliceNum = d->slice_layers[0].sliceRender[slice_idx]->sliceNumber.getValue();
        voxelPos[slice_idx] = (float)sliceNum;
        SoVolumeData* m_vol;
        m_vol = MedicalHelper::find<SoVolumeData>(d->slice_layers[0].sharedMaskData, "maskData");

        SbVec3f xyzPos = m_vol->voxelToXYZ(voxelPos);

        return xyzPos[slice_idx];
    }else {
        return -1;
    }
    /*
    SbVec3f voxelPos;
    int sliceNum = d->sliceRender[slice_idx]->sliceNumber.getValue();
    voxelPos[slice_idx] = (float)sliceNum;
    SoVolumeData* m_vol;
    m_vol = MedicalHelper::find<SoVolumeData>(d->sharedMaskData, "maskData");

    SbVec3f xyzPos = m_vol->voxelToXYZ(voxelPos);

    return xyzPos[slice_idx];*/
}

auto Oiv2dMaskSlice::findSlice(int m_ax) -> int {
    /*for (int i = 0; i < 3; i++) {
        if (d->sliceRender[i]->axis.getValue() == m_ax)            
            return i;
    }*/
    if(d->slice_layers.size()>0) {
        for(int i=0;i<3;i++) {
            if (d->slice_layers[0].sliceRender[i]->axis.getValue() == m_ax)
                return i;
        }
    }
    return -1;
}