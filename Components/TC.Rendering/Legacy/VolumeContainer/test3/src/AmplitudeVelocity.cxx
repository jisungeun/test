#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>
#include <Inventor/draggers/SoTabBoxDragger.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <LDM/nodes/SoMultiDataSeparator.h>
#include <QApplication>
#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>
#include <QMainWindow>
#include <QRadioButton>
#include <QFileDialog>
#include <QSlider>
#include <QVBoxLayout>
#include <QWidget>
#include <SpinBoxSliderWidget.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoVolumeSkin.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <QOiv3DRenderWindow.h>
#include <OivLdmReader.h>
#include <OivHdf5Reader.h>
#include <OivLdmReaderFL.h>
#include <QSettings>
#include <TCFMetaReader.h>

// Forward declarations
SoVolumeData* makeVelocityVolume(const SbVec3i32& volDim);
SoSeparator* createBBox(const SbBox3f& bbox);


static const int32_t AMPLITUDE_ID = 1;
static const int32_t VELOCITY_ID = 2;


int
main(int argc, char** argv)
{
	//QtHelper::addPlatformPluginsPath();

	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return 1;
	}

	auto metaReader = new TC::IO::TCFMetaReader;
	auto metaInfo = metaReader->Read(fileName);


	SoVolumeRendering::init();
	OivLdmReader::initClass();	

	////////////////////////////////////////////////////////
	/// GUI
	QMainWindow* window = new QMainWindow;
	window->setWindowTitle("Amplitude + Velocity");
	window->resize(949, 614);
	window->show();

	QWidget* centralWidget = new QWidget(window);
	window->setCentralWidget(centralWidget);
	centralWidget->setLayout(new QVBoxLayout);
	centralWidget->layout()->setContentsMargins(0, 0, 0, 0);
	centralWidget->layout()->setSpacing(0);

	QWidget* topWidget = new QWidget(centralWidget);
	centralWidget->layout()->addWidget(topWidget);
	topWidget->setLayout(new QHBoxLayout);
	topWidget->layout()->setContentsMargins(10, 10, 10, 10);
	topWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);


	QGroupBox* displayGroup = new QGroupBox(topWidget);
	topWidget->layout()->addWidget(displayGroup);
	displayGroup->setTitle("Display");
	displayGroup->setLayout(new QVBoxLayout);

	QCheckBox* sliceButton = new QCheckBox(displayGroup);
	displayGroup->layout()->addWidget(sliceButton);
	sliceButton->setText("Volume Amplitude - slice");
	sliceButton->setChecked(true);

	QCheckBox* skinButton = new QCheckBox(displayGroup);
	displayGroup->layout()->addWidget(skinButton);
	skinButton->setText("Volume Velocity - skin");


	QGroupBox* sliceGroup = new QGroupBox(topWidget);
	topWidget->layout()->addWidget(sliceGroup);
	sliceGroup->setTitle("Slice");
	sliceGroup->setLayout(new QVBoxLayout);

	SpinBoxSliderWidget* sliceSlider = new SpinBoxSliderWidget(sliceGroup);
	sliceGroup->layout()->addWidget(sliceSlider);
	sliceSlider->setLabel("index");
	sliceSlider->setRange(0, 209);
	sliceSlider->setValue(20);


	QGroupBox* conbineGroup = new QGroupBox(topWidget);
	topWidget->layout()->addWidget(conbineGroup);
	conbineGroup->setTitle("Volume blending");
	conbineGroup->setLayout(new QVBoxLayout);

	SpinBoxSliderWidget* combindeSlider = new SpinBoxSliderWidget(conbineGroup);
	combindeSlider->setRange(0, 100);
	combindeSlider->setValue(50);
	combindeSlider->setLabel("level %");
	conbineGroup->layout()->addWidget(combindeSlider);


	QGroupBox* infoGroup = new QGroupBox(topWidget);
	topWidget->layout()->addWidget(infoGroup);
	infoGroup->setTitle("Info");
	infoGroup->setLayout(new QVBoxLayout);
	QLabel* vol1Label = new QLabel(infoGroup);
	infoGroup->layout()->addWidget(vol1Label);
	QLabel* vol2Label = new QLabel(infoGroup);
	infoGroup->layout()->addWidget(vol2Label);


	qobject_cast<QBoxLayout*>(topWidget->layout())->addStretch();


	////////////////////////////////////////////////////////
	/// Scene Graph
	SoRef<SoSeparator> root = new SoSeparator;

	SoMultiDataSeparator* mds = new SoMultiDataSeparator;
	root->addChild(mds);

	auto range = new SoDataRange;
	range->dataRangeId = AMPLITUDE_ID;

	SoVolumeData* volData1 = new SoVolumeData;
	mds->addChild(volData1);
	mds->addChild(range);
	//volData1->fileName = "$OIVHOME/examples/data/VolumeViz/colt-float.ldm";
	auto htReader = new OivLdmReader;
	htReader->setTileName("000000");
	htReader->setTileDimension(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
	//htReader->setTileDimension(metaInfo->data.data3D.tileSizeX);
	htReader->setDataGroupPath("/Data/3D");
	htReader->setFilename(fileName.toStdString());
		
	//volData1->setReader(*htReader, TRUE);
	volData1->fileName = "$OIVHOME/examples/data/VolumeViz/colt-float.ldm";

	volData1->dataSetId = AMPLITUDE_ID;

	std::cout << "HT extent: " << volData1->extent.getValue().getMax()[0] << " " << volData1->extent.getValue().getMax()[1] << " " << volData1->extent.getValue().getMax()[2] << std::endl;

	volData1->extent.setValue(SbVec3f(-1.0f, -1.0f, -1.0f), SbVec3f(1.0f, 1.0f, 1.0f));
		

	SoTransferFunction* pTransFunc1 = new SoTransferFunction;
	mds->addChild(pTransFunc1);
	pTransFunc1->predefColorMap = SoTransferFunction::INTENSITY;
	pTransFunc1->transferFunctionId.connectFrom(&volData1->dataSetId);		

	MedicalHelper::dicomCheckMonochrome1(pTransFunc1, volData1);
	MedicalHelper::dicomAdjustDataRange(range, volData1);

	SoTabBoxDragger* tbbDragger = new SoTabBoxDragger;
	SoBaseColor* baseColor = new SoBaseColor;
	baseColor->rgb = SbVec3f(0.0f, 0.5f, 0.0f);
	baseColor->setOverride(true);
	SoDrawStyle* drawStyle = new SoDrawStyle;
	drawStyle->lineWidth = 2.0f;
	drawStyle->setOverride(true);
	// Dragger custom
	static_cast<SoSeparator*>(tbbDragger->getPart("boxGeom", true))->insertChild(baseColor, 0);
	static_cast<SoSeparator*>(tbbDragger->getPart("boxGeom", true))->insertChild(drawStyle, 0);

	mds->addChild(tbbDragger);

	SoFragmentShader* fragmentShader = new SoFragmentShader;
	fragmentShader->sourceProgram.setValue("$OIVHOME/examples/source/VolumeViz/multiData/AmplitudeVelocity/AmplitudeVelocity_sliceFrag.glsl");

	fragmentShader->addShaderParameter1i("data1", AMPLITUDE_ID);
	fragmentShader->addShaderParameter1i("data2", VELOCITY_ID);
	SoShaderParameter1f* combinedPercentageShaderParam = fragmentShader->addShaderParameter1f("combinedPercentage", 0.5f);

	SoVolumeShader* volShader = new SoVolumeShader;
	volShader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, fragmentShader);
	mds->addChild(volShader);

	SoTranslation* trans = new SoTranslation;
	mds->addChild(trans);

	SoScale* scale = new SoScale;
	mds->addChild(scale);

	//SoVolumeData* volData2 = makeVelocityVolume( volData1->data.getSize() );
	SoVolumeData* volData2 = new SoVolumeData;	
	mds->addChild(volData2);

	auto flReader = new OivLdmReaderFL(1);
	flReader->setTileName("000000");
	flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX, metaInfo->data.data3DFL.tileSizeY, metaInfo->data.data3DFL.tileSizeZ);
	std::cout << "fl tile : " << metaInfo->data.data3DFL.tileSizeX << " " << metaInfo->data.data3DFL.tileSizeY << " " << metaInfo->data.data3DFL.tileSizeZ << std::endl;
	//flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flReader->setDataGroupPath("/Data/3DFL/CH1");
	flReader->setFilename(fileName.toStdString());

	//volData2->setReader(*flReader, TRUE);

	volData2->fileName = "$OIVHOME/examples/data/VolumeViz/3DHEAD.ldm";
	//volData2->extent.setValue( SbVec3f( -1.0f, -1.0f, -1.0f ), SbVec3f( 1.0f, 1.0f, 1.0f ) );
	volData2->dataSetId = VELOCITY_ID;
	std::cout << "Vol2 extent : " << volData2->extent.getValue().getMax()[0] << " " << volData2->extent.getValue().getMax()[1] << " " << volData2->extent.getValue().getMax()[2] << std::endl;

    SoTransferFunction* pTransFunc2 = new SoTransferFunction;
	mds->addChild(pTransFunc2);
	pTransFunc2->predefColorMap = SoTransferFunction::BLUE_WHITE_RED;
	pTransFunc2->transferFunctionId.connectFrom(&volData2->dataSetId);

	SoSwitch* volumeSkinSwitch = new SoSwitch;
	mds->addChild(volumeSkinSwitch);
	volumeSkinSwitch->whichChild = SO_SWITCH_NONE;
	SoVolumeSkin* volumeSkin = new SoVolumeSkin;
	volumeSkin->dataSetId = VELOCITY_ID;
	volumeSkinSwitch->addChild(volumeSkin);


	SoSwitch* orthoSwitch = new SoSwitch;
	mds->addChild(orthoSwitch);
	orthoSwitch->whichChild = SO_SWITCH_ALL;
	SoOrthoSlice* orthoslice = new SoOrthoSlice;
	orthoSwitch->addChild(orthoslice);
	orthoslice->sliceNumber = 32;
	orthoslice->dataSetId = AMPLITUDE_ID;

	root->addChild(createBBox(volData1->extent.getValue()));

	////////////////////////////////////////////////////////
	/// Dragger Connection
	trans->translation.connectFrom(&tbbDragger->translation);
	scale->scaleFactor.connectFrom(&tbbDragger->scaleFactor);
	tbbDragger->translation = SbVec3f(-0.5f, -0.5f, 0.0f);
	tbbDragger->scaleFactor = SbVec3f(0.5f, 0.5f, 0.5f);

	////////////////////////////////////////////////////////
	/// GUI Connection
	SbVec3i32 vol1Size = volData1->data.getSize();
	SbVec3i32 vol2Size = volData2->data.getSize();
	vol1Label->setText(QString("Volume Amplitude (red box): %1 x %2 x %3").arg(vol1Size[0]).arg(vol1Size[1]).arg(vol1Size[2]));
	vol2Label->setText(QString("Volume Velocity (green box): %1 x %2 x %3").arg(vol2Size[0]).arg(vol2Size[1]).arg(vol2Size[2]));

	QObject::connect(sliceButton, &QCheckBox::clicked, [orthoSwitch](bool checked) {
		checked ? orthoSwitch->whichChild = SO_SWITCH_ALL : orthoSwitch->whichChild = SO_SWITCH_NONE;
	});
	QObject::connect(skinButton, &QCheckBox::clicked, [volumeSkinSwitch](bool checked) {
		checked ? volumeSkinSwitch->whichChild = SO_SWITCH_ALL : volumeSkinSwitch->whichChild = SO_SWITCH_NONE;
	});

	QObject::connect(combindeSlider, &SpinBoxSliderWidget::valueChanged, [combinedPercentageShaderParam](int value) {
		combinedPercentageShaderParam->value = (float)value / 100;
	});

	QObject::connect(sliceSlider, &SpinBoxSliderWidget::valueChanged, [orthoslice](int value) { orthoslice->sliceNumber = value; });


	////////////////////////////////////////////////////////
	/// Viewer
	
	RenderAreaOrbiter* renderArea = new RenderAreaOrbiter(window);
	const float grayVal = 0.99f;
	renderArea->setClearColor(SbColorRGBA(grayVal, grayVal, grayVal, 1.0f));
	centralWidget->layout()->addWidget(renderArea->getContainerWidget());
	renderArea->setSceneGraph(root.ptr());
	renderArea->viewAll(SbViewportRegion());
	renderArea->setAntialiasingMode(SoSceneManager::AntialiasingMode::AUTO);
	renderArea->setAntialiasingQuality(0);

	////////////////////////////////////////////////////////
	/// App Exec
	app.exec();


	////////////////////////////////////////////////////////
	/// Exit
	root = nullptr;
	delete window;

	OivLdmReader::exitClass();
	SoVolumeRendering::finish();
	return 0;
}

////////////////////////////////////////////////////////////////////////
// Helper for Bbox creation
SoSeparator*
createBBox(const SbBox3f& bbox)
{
	SoSeparator* bboxSep = new SoSeparator;

	SoPickStyle* pickStyle = new SoPickStyle;
	bboxSep->addChild(pickStyle);
	pickStyle->style = SoPickStyle::Style::UNPICKABLE;

	SoDrawStyle* drawStyle = new SoDrawStyle;
	bboxSep->addChild(drawStyle);
	drawStyle->style = SoDrawStyle::Style::LINES;
	drawStyle->lineWidth = 2.0f;

	SoBaseColor* bboxMat = new SoBaseColor;
	bboxMat->rgb = SbVec3f(0.75f, 0.0f, 0.0f);
	bboxSep->addChild(bboxMat);

	SoLightModel* lightModel = new SoLightModel;
	bboxSep->addChild(lightModel);
	lightModel->model = SoLightModel::Model::BASE_COLOR;

	SoTranslation* translation = new SoTranslation;
	bboxSep->addChild(translation);
	translation->translation = bbox.getCenter();

	SoCube* cube = new SoCube;
	const SbVec3f bboxSize = bbox.getSize();
	cube->width = bboxSize[0];
	cube->height = bboxSize[1];
	cube->depth = bboxSize[2];
	bboxSep->addChild(cube);

	return bboxSep;
}

////////////////////////////////////////////////////////////////////////
// Make synthetic velocity volume (not realistic)
SoVolumeData*
makeVelocityVolume(const SbVec3i32& volDim)
{
	// Allocate memory for volume2 and assign values
	int testXdim = volDim[0];
	int testYdim = volDim[1];
	int testZdim = volDim[2];
	int numTestBytes = testXdim * testYdim * testZdim;
	unsigned char* testData = new unsigned char[numTestBytes];
	int counter = 0;
	int ymax = testYdim - 1;
	for (int i = 0; i < testZdim; i++)
	{
		for (int j = 0; j < testYdim; j++)
		{
			unsigned char value = (float)j / ymax * 255;
			for (int k = 0; k < testXdim; k++)
			{
				float rad = (float)k / (testXdim - 1);
				if (rad > 0.75)
					rad = 0.75;
				rad *= 2 * (float)M_PI;
				float offset = (float)-sin(rad);
				offset *= 30 * (1 - ((float)j / ymax));
				int val = value + offset - i / 2;
				if (val < 1)
					val = 1;
				else if (val > 255)
					val = 255;
				testData[counter++] = 255 - val;
			}
		}
	}

	// Create node and associate data with it.
	SoVolumeData* pVolData2 = new SoVolumeData;
	SoMemoryObject* memObj = new SoMemoryObject(testData, numTestBytes * sizeof(unsigned char));
	pVolData2->data.setValue(volDim, 8, memObj, SoSFArray::NO_COPY_AND_DELETE);
	return pVolData2;
}
