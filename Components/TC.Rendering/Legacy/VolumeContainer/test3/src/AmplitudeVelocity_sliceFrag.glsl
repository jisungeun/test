// Fragment shader for AmplitudeVelocity example
// Take intensity value from volume1 (seismic amplitude)
// and color value from volume2 (velocity model)

//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform float combinedPercentage;

vec4 blend(VVizDataSetId dataset, in vec3 texCoord)
{
  // Convert coordinates
  vec3 dataCoord1 = VVizTextureToTextureVec(dataset, data1, texCoord);
  vec3 dataCoord2 = VVizTextureToTextureVec(dataset, data2, texCoord);

  // Get data and apply TransferFunction
  // Make sure we return vec4(0.0) outside volume
  vec4 data1Color = mix(VVizTransferFunction(VVizGetData(data1, dataCoord1), data1), vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));
  vec4 data2Color = mix(VVizTransferFunction(VVizGetData(data2, dataCoord2), data2), vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord2)));

  // Do not mix outside volumes intersection
  float finalCombinedPercentage = mix(mix(combinedPercentage, 1.0, VVizIsOutsideVolume(dataCoord1)), 0.0, VVizIsOutsideVolume(dataCoord2));

  return mix(data1Color, data2Color, finalCombinedPercentage);
}

// Implement VVizComputeFragmentColor for slice nodes
vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
  return blend(VVizGetDefaultDataSet(), texCoord);
}

// Implement VVizComputeFragmentColor for SoVolumeRender
vec4 VVizComputeFragmentColor(in VVizDataSetId dataset, in vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, in int maskId)
{
  return blend(dataset, voxelInfoFront.texCoord);
}
