#pragma once
#include <QAbstractSpinBox>

class QLabel;
class QSlider;
class QAbstractSpinBox;


class AbstractSpinBoxSliderWidget : public QWidget
{
  Q_OBJECT
public:
  explicit AbstractSpinBoxSliderWidget( QWidget* parent = nullptr );
  void setLabel( const QString& label );

protected:
  void buildWidget( QAbstractSpinBox* spinBox );
  QLabel* mLabel;
  QSlider* mSlider;
  QAbstractSpinBox* mAbsSpinBox;
};
