#include "AbstractSpinBoxSliderWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QWidget>

AbstractSpinBoxSliderWidget::AbstractSpinBoxSliderWidget( QWidget* parent )
  : QWidget( parent )
{
  mLabel = new QLabel( "Label", this );
}

void
AbstractSpinBoxSliderWidget::setLabel( const QString& label )
{
  mLabel->setText( label );
}

void
AbstractSpinBoxSliderWidget::buildWidget( QAbstractSpinBox* spinBox )
{
  mSlider = new QSlider( Qt::Orientation::Horizontal, this );
  mAbsSpinBox = spinBox;
  this->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

  setLayout( new QHBoxLayout );

  layout()->addWidget( mLabel );
  layout()->addWidget( mSlider );
  layout()->addWidget( mAbsSpinBox );
}
