#include "SpinBoxSliderWidget.h"

#include <QDebug>
#include <QSlider>
#include <QSpinBox>
#include <QtMath>


SpinBoxSliderWidget::SpinBoxSliderWidget( QWidget* parent )
  : AbstractSpinBoxSliderWidget( parent )
{
  mSpinBox = new QSpinBox( this );
  buildWidget( mSpinBox );

  connect( mSlider, &QSlider::valueChanged, [this]() {
    QSignalBlocker sb( mSpinBox );
    mSpinBox->setValue( mSlider->value() * mSpinBox->singleStep() );
  } );

  connect( mSlider, &QSlider::valueChanged, mSpinBox, [this]() {
    int value = mSlider->value() * mSpinBox->singleStep();
    mSpinBox->setValue( value );
    emit valueChanged( value );
  } );

  connect( mSpinBox, QOverload<int>::of( &QSpinBox::valueChanged ), mSlider, [this]( int value ) {
    QSignalBlocker sb( mSlider );
    mSlider->setValue( qRound( static_cast<double>( value ) / mSpinBox->singleStep() ) );
  } );

  connect( mSpinBox, QOverload<int>::of( &QSpinBox::valueChanged ), this, &SpinBoxSliderWidget::valueChanged );
}

void
SpinBoxSliderWidget::setRange( int minimum, int maximum )
{
  mSpinBox->setRange( minimum, maximum );
  configureSlider();
}


void
SpinBoxSliderWidget::setSingleStep( int val )
{
  mSpinBox->setSingleStep( val );
  mSlider->setPageStep( 1 );
  configureSlider();
}

void
SpinBoxSliderWidget::setValue( int value )
{
  mSpinBox->setValue( value );
}

void
SpinBoxSliderWidget::configureSlider()
{
  mSlider->setRange( mSpinBox->minimum(), qCeil( static_cast<double>( mSpinBox->maximum() ) / mSpinBox->singleStep() ) );
}
