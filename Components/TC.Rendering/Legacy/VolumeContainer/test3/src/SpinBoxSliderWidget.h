#pragma once
#include <AbstractSpinBoxSliderWidget.h>

class QSpinBox;

class SpinBoxSliderWidget : public AbstractSpinBoxSliderWidget
{
  Q_OBJECT

public:
  SpinBoxSliderWidget( QWidget* parent = nullptr );
  void setRange( int minimum, int maximum );
  void setSingleStep( int val );

public slots:
  void setValue( int value );

signals:
  void valueChanged( int value );

private:
  void configureSlider();
  QSpinBox* mSpinBox;
};
