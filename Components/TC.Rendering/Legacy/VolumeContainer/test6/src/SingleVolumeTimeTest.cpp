#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaExaminer.h>

#include <iostream>
#include <QSettings>
#include <QFileDialog>
#include <QVBoxLayout>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <QTransferFunctionCanvas2D.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivHdf5Reader.h>
#include <OivXYReader.h>
#include <OivYZReader.h>
#include <OivXZReader.h>
#include <OivSliceContainer.h>
#include <OivVolumeContainer.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
#include <QOiv3DRenderWindow.h>
#include <TCFMetaReader.h>
#include <ImageViz/SoImageViz.h>

//Create simple Qt+OIV 2D view with tooth image

int cur_time_step = 0;

auto makeColorMap(SbColor color, float gamma, int id) -> SoTransferFunction* {
	SoTransferFunction* pTF = new SoTransferFunction();
	pTF->transferFunctionId = id + 1;

	// Color map will contain 256 RGBA values -> 1024 float values.
	// setNum pre-allocates memory so we can edit the field directly.
	pTF->colorMap.setNum(256 * 4);
	float R = color[0];
	float G = color[1];
	float B = color[2];

	// Get an edit pointer, assign values, then finish editing.
	float* p = pTF->colorMap.startEditing();
	for (int i = 0; i < 256; ++i) {
		float factor = (float)i / 255;
		float mod_r = R * factor;
		float mod_g = G * factor;
		float mod_b = B * factor;
		mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
		mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
		mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
		*p++ = pow(factor, gamma);
	}
	pTF->colorMap.finishEditing();
	return pTF;
}

void main(int argc, char** argv) {
	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();
	OivXYReader::initClass();
	OivYZReader::initClass();
	OivXZReader::initClass();

	//Read meta data
	auto metaReader = new TC::IO::TCFMetaReader;
	auto metaInfo = metaReader->Read(fileName);

	if (false == metaInfo->data.data3DFL.exist) {
		std::cout << "fl 3d data is not exist" << std::endl;
		return;
	}

	if (false == metaInfo->data.isLDM) {
		std::cout << "file is not an ldm" << std::endl;
		//return;
	}

	if (metaInfo->data.data3D.dataCount < 2) {
		std::cout << "file is not time-lapse" << std::endl;
		return;
	}
	//read data
	//HT Volume initialization
	auto htReader = new OivHdf5Reader;
	htReader->setTileName("000000");
	htReader->setTileDimension(metaInfo->data.data3D.tileSizeX);
	htReader->setDataGroupPath("/Data/3D");
	htReader->setFilename(fileName.toStdString());

	auto htSliceReader = new OivXYReader;
	htSliceReader->setTileName("000000");
	htSliceReader->setDataGroupPath("/Data/3D");
	htSliceReader->setFilename(fileName.toStdString());
	htSliceReader->setIndex(metaInfo->data.data3D.sizeZ / 2);

	auto htYZReader = new OivYZReader;
	htYZReader->setTileName("000000");
	htYZReader->setDataGroupPath("/Data/3D");
	htYZReader->setFilename(fileName.toStdString());
	htYZReader->setIndex(metaInfo->data.data3D.sizeX / 2);

	auto htXZReader = new OivXZReader;
	htXZReader->setTileName("000000");
	htXZReader->setDataGroupPath("/Data/3D");
	htXZReader->setFilename(fileName.toStdString());
	htXZReader->setIndex(metaInfo->data.data3D.sizeY / 2);
    
	auto htVolume = new SoVolumeData;
	htVolume->texturePrecision = 16;
	htVolume->ldmResourceParameters.getValue()->tileDimension.setValue(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
	htVolume->setName("volData0");
	htVolume->setReader(*htReader, TRUE);
	htVolume->dataSetId = 1;
	htVolume->texturePrecision = 16;

	auto htVolumeProj = new SoVolumeData;
	htVolumeProj->texturePrecision = 16;
	htVolumeProj->ldmResourceParameters.getValue()->tileDimension.setValue(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
	htVolumeProj->setName("volData0");
	htVolumeProj->setReader(*htReader, TRUE);
	htVolumeProj->dataSetId = 1;
	htVolumeProj->texturePrecision = 16;

	auto htVolumeslice = new SoVolumeData;
	htVolumeslice->texturePrecision = 16;
	htVolumeslice->ldmResourceParameters.getValue()->tileDimension.setValue(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, 1);
	htVolumeslice->setName("volData0");
	//htVolumeslice->setReader(*htSliceReader, TRUE);
	//htVolumeslice->setReader(*htYZReader, TRUE);
	htVolumeslice->setReader(*htXZReader, TRUE);
	htVolumeslice->dataSetId = 1;
	htVolumeslice->texturePrecision = 16;

	std::cout << "HT extent: " << htVolume->extent.getValue().getMax()[0] << " " << htVolume->extent.getValue().getMax()[1] << " " << htVolume->extent.getValue().getMax()[2] << std::endl;
	std::cout << "HT dimension: " << htVolume->getDimension() << std::endl;
	std::cout << "HT slice dimension: " << htVolumeslice->getDimension() << std::endl;
	std::cout << "HT tile: " << metaInfo->data.data3D.tileSizeX << " " << metaInfo->data.data3D.tileSizeY << " " << metaInfo->data.data3D.tileSizeZ << std::endl;
	
    //FL Volume initialization
	auto offsetZ = metaInfo->data.data3DFL.offsetZ - (metaInfo->data.data3DFL.sizeZ * metaInfo->data.data3DFL.resolutionZ / 2);

	auto flReader = new OivHdf5Reader(true);
	flReader->setTileName("000000");
	flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flReader->setDataGroupPath("/Data/3DFL/CH1");
	flReader->setFilename(fileName.toStdString());

	auto flSliceReader = new OivXYReader(true);
	flSliceReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flSliceReader->setTileName("000000");
	flSliceReader->setDataGroupPath("/Data/3DFL/CH1");
	flSliceReader->setFilename(fileName.toStdString());
	flSliceReader->setIndex(metaInfo->data.data3DFL.sizeZ / 2);

	auto flYZReader = new OivYZReader(true);
	flYZReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flYZReader->setTileName("000000");
	flYZReader->setDataGroupPath("/Data/3DFL/CH1");
	flYZReader->setFilename(fileName.toStdString());
	flYZReader->setIndex(metaInfo->data.data3DFL.sizeX / 2);

	auto flXZReader = new OivXZReader(true);
	flXZReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flXZReader->setTileName("000000");
	flXZReader->setDataGroupPath("/Data/3DFL/CH1");
	flXZReader->setFilename(fileName.toStdString());
	flXZReader->setIndex(metaInfo->data.data3DFL.sizeY / 2);

	auto flVolume = new SoVolumeData;
	flVolume->setName("flVolume");
	flVolume->setReader(*flReader, TRUE);
	flVolume->dataSetId = 2;

	auto flVolumeProj = new SoVolumeData;
	flVolumeProj->setName("flVolumeProj");
	flVolumeProj->setReader(*flReader, TRUE);
	flVolumeProj->dataSetId = 2;

	auto oriextent = flVolume->extent.getValue();

	flVolume->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);
	flVolumeProj->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);

	auto flVolumeSlice = new SoVolumeData;
	flVolumeSlice->setName("flVolumeSlice");
	//flVolumeSlice->setReader(*flSliceReader, TRUE);
	//flVolumeSlice->setReader(*flYZReader, TRUE);
	flVolumeSlice->setReader(*flXZReader, TRUE);
	flVolumeSlice->dataSetId = 2;

	//Create scene graph with shared multi-data separator	
	SoSeparator* root3d = new SoSeparator;
	SoSeparator* rootPlane = new SoSeparator;
	SoSeparator* rootslice = new SoSeparator;

	SoMultiDataSeparator* mds = new SoMultiDataSeparator;
	SoMultiDataSeparator* mdsPlane = new SoMultiDataSeparator;
	SoMultiDataSeparator* mdsSlice = new SoMultiDataSeparator;

	root3d->addChild(mds);
	rootPlane->addChild(mdsPlane);
	rootslice->addChild(mdsSlice);

	MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };

	SbColor red(1, 0, 0);
	SbColor green(0, 1, 0);
	SbColor blue(0, 0, 1);

	const auto shaderPath = QString("%1/shader/2DTransferFunctionCustomShader.glsl").arg(qApp->applicationDirPath());

	auto multiModalFrag = new SoFragmentShader;
	multiModalFrag->sourceProgram.setValue(shaderPath.toStdString());
	multiModalFrag->addShaderParameter1i("data1", 1);
	multiModalFrag->addShaderParameter1i("data2", 2);

	//create scene graph for root3d

	auto rangeHT = new SoDataRange;
	rangeHT->dataRangeId = 1;

	MedicalHelper::dicomAdjustDataRange(rangeHT, htVolume);
	//auto colorHT = makeColorMap(red, 3.37f, 0);

	SoTransferFunction* colorHT = new SoTransferFunction;
	colorHT->transferFunctionId = 1;
	colorHT->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;

	MedicalHelper::dicomCheckMonochrome1(colorHT, htVolume);

    mds->addChild(htVolume);
	mds->addChild(rangeHT);
	mds->addChild(colorHT);

	auto rangeFL = new SoDataRange;
	rangeFL->dataRangeId = 2;

	MedicalHelper::dicomAdjustDataRange(rangeFL, flVolume);
	auto oriMax = rangeFL->max.getValue();
	rangeFL->max.setValue(oriMax / 4);

	auto colorFL = makeColorMap(green, 3.37f, 1);	
	mds->addChild(flVolume);
	mds->addChild(rangeFL);
	mds->addChild(colorFL);

	auto volQual = new SoVolumeRenderingQuality;
	volQual->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);
	mds->addChild(volQual);

	auto render3d = new SoVolumeRender;
	mds->addChild(render3d);


	//create scene graph for Plane	

	mdsPlane->addChild(htVolumeProj);
	mdsPlane->addChild(rangeHT);
	mdsPlane->addChild(colorHT);
	mdsPlane->addChild(flVolumeProj);
	mdsPlane->addChild(rangeFL);
	mdsPlane->addChild(colorFL);

	auto volQualProj = new SoVolumeShader;
	volQualProj->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);
	volQualProj->forVolumeOnly = FALSE;
	mdsPlane->addChild(volQualProj);

	auto sliceXY = new SoOrthoSlice;
	sliceXY->dataSetId = 1;
	sliceXY->axis = ax[2];
	sliceXY->sliceNumber.setValue(metaInfo->data.data3D.sizeY / 2);

	mdsPlane->addChild(sliceXY);

	//create scene graph for slice
	auto rangeHTSlice = new SoDataRange;
	rangeHTSlice->dataRangeId = 1;

	MedicalHelper::dicomAdjustDataRange(rangeHTSlice, htVolume);
	//auto colorHTSlice = makeColorMap(red, 3.37f, 0);
	SoTransferFunction* colorHTSlice = new SoTransferFunction;
	colorHTSlice->transferFunctionId = 1;
	colorHTSlice->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;

	MedicalHelper::dicomCheckMonochrome1(colorHTSlice, htVolume);

	mdsSlice->addChild(htVolumeslice);
	mdsSlice->addChild(rangeHTSlice);
	mdsSlice->addChild(colorHTSlice);
	mdsSlice->addChild(flVolumeSlice);
	mdsSlice->addChild(rangeFL);
	mdsSlice->addChild(colorFL);

	auto volQualSlice = new SoVolumeShader;
	volQualSlice->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);
	volQualSlice->forVolumeOnly = FALSE;
	mdsSlice->addChild(volQualSlice);

	auto sliceSlice = new SoOrthoSlice;
	sliceSlice->dataSetId = 1;
	sliceSlice->axis = ax[0];
	//sliceSlice->sliceNumber.setValue(metaInfo->data.data3D.sizeZ / 2);

	mdsSlice->addChild(sliceSlice);

	//Create Render Window UI
	QMainWindow* window = new QMainWindow;
	window->setWindowTitle("Shared MultiDataSeparator");
	window->resize(2100, 700);
	window->show();

	QWidget* centralWidget = new QWidget(window);
	window->setCentralWidget(centralWidget);
	QWidget* containerWidget = new QWidget(window);
	auto renderLayout = new QHBoxLayout;
	renderLayout->setContentsMargins(0, 0, 0, 0);
	containerWidget->setLayout(renderLayout);

	centralWidget->setLayout(new QVBoxLayout);
	centralWidget->layout()->setContentsMargins(0, 0, 0, 0);
	centralWidget->layout()->setSpacing(0);

	centralWidget->layout()->addWidget(containerWidget);

	RenderAreaOrbiter* renderArea3D = new RenderAreaOrbiter(window);
	const float grayVal = 0.99f;
	renderArea3D->setClearColor(SbColorRGBA(grayVal, grayVal, grayVal, 1.0f));		
	renderArea3D->setSceneGraph(root3d);
	renderArea3D->viewAll(SbViewportRegion());

	RenderAreaOrbiter* renderAreaPlane = new RenderAreaOrbiter(window);
	renderAreaPlane->setSceneGraph(rootPlane);
	MedicalHelper::orientView(ax[2], renderAreaPlane->getSceneInteractor()->getCamera(), htVolume);

	RenderAreaOrbiter* renderAreaSlice = new RenderAreaOrbiter(window);
	renderAreaSlice->setSceneGraph(rootslice);
	MedicalHelper::orientView(ax[0], renderAreaSlice->getSceneInteractor()->getCamera(), htVolumeslice);

	renderLayout->addWidget(renderArea3D->getContainerWidget());
	renderLayout->addWidget(renderAreaPlane->getContainerWidget());
	renderLayout->addWidget(renderAreaSlice->getContainerWidget());
	
	QPushButton* timeStepBtn = new QPushButton(nullptr);
	timeStepBtn->setText("Toggle Time Step");
	centralWidget->layout()->addWidget(timeStepBtn);

	QPushButton* sliceUpBtn = new QPushButton;
	sliceUpBtn->setText("Move Slice Upward");
	centralWidget->layout()->addWidget(sliceUpBtn);

	QPushButton* sliceDownBtn = new QPushButton;
	sliceDownBtn->setText("Move Slice Downward");
	centralWidget->layout()->addWidget(sliceDownBtn);

	QObject::connect(timeStepBtn, &QPushButton::clicked, [=]() {
		cur_time_step = !cur_time_step;
		QString tileName = QString("%1").arg(cur_time_step, 6, 10, QLatin1Char('0'));
		htReader->setTileName(tileName.toStdString());
		htReader->touch();		
	});

	QObject::connect(sliceUpBtn, &QPushButton::clicked, [=]() {
		auto curSlice = sliceXY->sliceNumber.getValue();
		curSlice++;
		if(curSlice < metaInfo->data.data3D.sizeZ) {
			sliceXY->sliceNumber = curSlice;
			htSliceReader->setIndex(curSlice);
			htSliceReader->touch();

			auto resFL = metaInfo->data.data3DFL.resolutionZ;
			auto phyZ = metaInfo->data.data3D.resolutionZ * curSlice;
			auto idxFL = static_cast<int>((phyZ - static_cast<double>(offsetZ)) / resFL);
			flSliceReader->setIndex(idxFL);
			flSliceReader->touch();
		}

	});
	QObject::connect(sliceDownBtn, &QPushButton::clicked, [=]() {
		auto curSlice = sliceXY->sliceNumber.getValue();
		curSlice--;
		if(curSlice>0) {
			sliceXY->sliceNumber = curSlice;
			//htSliceReader->setIndex(curSlice);
			//htSliceReader->touch();
			//htYZReader->setIndex(curSlice);
			//htYZReader->touch();
			htXZReader->setIndex(curSlice);
			htXZReader->touch();

			/*auto resFL = metaInfo->data.data3DFL.resolutionZ;
			auto phyZ = metaInfo->data.data3D.resolutionZ * curSlice;
			auto idxFL = static_cast<int>((phyZ - static_cast<double>(offsetZ)) / resFL);
			flSliceReader->setIndex(idxFL);
			flSliceReader->touch();*/
			/*auto resFL = metaInfo->data.data3DFL.resolutionX;
			auto phyX = metaInfo->data.data3D.resolutionX * curSlice;
			auto idxFL = static_cast<int>(phyX / resFL);
			flYZReader ->setIndex(idxFL);
			flYZReader->touch();*/

			auto resFL = metaInfo->data.data3DFL.resolutionY;
			auto phyY = metaInfo->data.data3D.resolutionY * curSlice;
			auto idxFL = static_cast<int>(phyY / resFL);
			flXZReader->setIndex(idxFL);
			flXZReader->touch();

			std::cout << "curSlice: " << curSlice << " flIdx: " << idxFL << std::endl;
		}

	});

	app.exec();

	delete renderArea3D;
	delete renderAreaPlane;
	//delete renderAreaYZ;
	//delete renderAreaXZ;
	delete renderAreaSlice;


	OivXZReader::exitClass();
	OivYZReader::exitClass();
	OivXYReader::exitClass();
	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();		
}