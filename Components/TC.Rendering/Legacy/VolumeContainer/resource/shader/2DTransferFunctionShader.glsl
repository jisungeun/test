//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
#ifdef USE_DATA2
uniform VVizDataSetId data2;
#endif
uniform sampler2D tex2D;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
    vec3 texCoord = voxelInfoFront.texCoord;

    //texture unit 1 -> data 1
    VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);

    vec3 normal, gradient;
    VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);


#ifdef USE_DATA2
    // texture unit 2 -> data 2
    float gradientValue = VVizGetLuminance(VVizGetData(data2, texCoord));
#else
    float gradientValue = length(gradient);
#endif

    vec4 color = texture2D(tex2D, vec2(index1, gradientValue));
    color = VVizComputeVolumeRenderingLighting(color, normal, gradientValue);

    return color;
}
