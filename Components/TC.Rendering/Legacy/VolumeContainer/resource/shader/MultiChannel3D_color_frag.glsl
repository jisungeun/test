///////////////////////////////////////////////////////////////////////////////
//
// This program is part of the Open Inventor Medical Edition example set.
//
// Open Inventor customers may use this source code to create or enhance
// Open Inventor-based applications.
//
// The medical utility classes are provided as a prebuilt library named
// "fei.inventor.Medical", that can be used directly in an Open Inventor
// application. The classes in the prebuilt library are documented and
// supported by FEI. These classes are also provided as source code.
//
// Please see $OIVHOME/include/Medical/InventorMedical.h for the full text.
//
///////////////////////////////////////////////////////////////////////////////


//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>


////////////////////////////////////////////////////////////////////////
// Simple multi-channel fragment shader for volume rendering
//
// Notes
// 1) This version is for volume rendering.
//    (Must make a separate version for slice/skin rendering.)
// 2) This version is for 3 channels of data.
//    (Must make a separate version for 2 channels.)
// 3) This shader assumes that 3D textures are used.
// 4) It seems reasonable to sum the color (RGB) components.  But we
//    don't necessarily want the volume to be more opaque where
//    values exist in multiple channels, so we'll use the highest
//    opacity value in any of the channels.  Does that make sense?
uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{  
 
  VVIZ_DATATYPE val1,val2,val3;
  vec4 clr1, clr2, clr3;  
  if(isCh0Exist > 0){
     val1 = VVizGetData(data1, voxelInfoFront.texCoord);
	 clr1 = VVizComputeVolumeRendering(val1, 1);
  }
  else
  {
     val1 = 0.0;
	 clr1 = vec4(0,0,0,0);     
  }
  if(isCh1Exist > 0)
  {
     val2 = VVizGetData(data2, voxelInfoFront.texCoord);
	 clr2 = VVizComputeVolumeRendering(val2, 2);         
  }
  else
  {
     val2 = 0.0;
	 clr2 = vec4(0,0,0,0);
  }
  
  if(isCh2Exist > 0)
  {
     val3 = VVizGetData(data3, voxelInfoFront.texCoord);
	 clr3 = VVizComputeVolumeRendering(val3, 3);         
  }
  else
  {
     val3 = 0.0;
	 clr3 = vec4(0,0,0,0);
  }  
  // Combine color values but use highest alpha value  
  vec4 res;
  res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;  
  res.a = max(clr1.a,max(clr2.a,clr3.a));  

  return res;
}
