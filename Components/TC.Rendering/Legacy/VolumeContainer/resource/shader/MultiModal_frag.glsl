//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

////////////////////////////////////////////////////////////////////////
// Simple multi-channel fragment shader for volume rendering
//
// Notes
// 1) This version is for volume rendering.
//    (Must make a separate version for slice/skin rendering.)
// 2) This version is for 3 channels of data.
//    (Must make a separate version for 2 channels.)
// 3) This shader assumes that 3D textures are used.
// 4) It seems reasonable to sum the color (RGB) components.  But we
//    don't necessarily want the volume to be more opaque where
//    values exist in multiple channels, so we'll use the highest
//    opacity value in any of the channels.  Does that make sense?

uniform VVizDataSetId data1;
uniform sampler2D tex2D;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;
uniform VVizDataSetId data4;

uniform int isHTExist;
uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

uniform float resX;
uniform float resY;
uniform float resZ;

void GetCurrentDuDvDw(in VVizDataSetId dataset, in vec4 tileInfo, in vec3 tcoordVirt, out vec3 du, out vec3 dv, out vec3 dw)
{
    // Get resolution of current tile (not necessarily full resolution)

    float res = tileInfo.w;
    vec3 duData = vec3(res, 0, 0);
    vec3 dvData = vec3(0, res, 0);
    vec3 dwData = vec3(0, 0, res);

    // Get normalized voxel size and adjust for tile resolution
    vec3 voxelDims = VVizGetVoxelDimensions(dataset);
    du = voxelDims * duData;
    dv = voxelDims * dvData;
    dw = voxelDims * dwData;
}
vec3 ComputeCentralDiffGradient(VVizDataSetId tex, vec3 tcoord)
{
    // Get uvw offsets (essentially voxel size in normalized coordinates)
    vec3 scaledDu;
    vec3 scaledDv;
    vec3 scaledDw;
    vec4 tileInfo = VVizGetTileInfo(tex, tcoord);
    GetCurrentDuDvDw(tex, tileInfo, tcoord, scaledDu, scaledDv, scaledDw);

    // Compute central difference		

    vec3 G;
    G.x = VVizGetData(tex, tcoord + scaledDu) - VVizGetData(tex, tcoord - scaledDu);
    G.y = VVizGetData(tex, tcoord + scaledDv) - VVizGetData(tex, tcoord - scaledDv);
    G.z = VVizGetData(tex, tcoord + scaledDw) - VVizGetData(tex, tcoord - scaledDw);

    float avg_spacing = (resX + resY + resZ) / 3.0;

    G.x /= (resX * 2 / avg_spacing);
    G.y /= (resY * 2 / avg_spacing);
    G.z /= (resZ * 2 / avg_spacing);

    G *= 1. / sqrt(3.);

    return G;
}

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{   
    vec3 texCoord = voxelInfoFront.texCoord;
    vec4 clrHT = vec4(0.0, 0.0, 0.0, 0.0);
    VVIZ_DATATYPE val1, val2, val3;
    vec4 clr1, clr2, clr3;

    if (isHTExist > 0) {
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);
        vec3 gradient = ComputeCentralDiffGradient(data1, texCoord);
        float gradientValue = length(gradient);
        clrHT = texture2D(tex2D, vec2(index1, gradientValue));        
    }    
    
    if(isCh0Exist > 0){
        val1 = VVizGetData(data2, voxelInfoFront.texCoord);
        clr1 = VVizComputeVolumeRendering(val1, 2);
    }
    else
    {
        val1 = 0.0;
        clr1 = vec4(0,0,0,0);     
    }
    if(isCh1Exist > 0)
    {
        val2 = VVizGetData(data3, voxelInfoFront.texCoord);
        clr2 = VVizComputeVolumeRendering(val2, 3);         
    }
    else
    {
        val2 = 0.0;
        clr2 = vec4(0, 0, 0, 0);
    }
    if(isCh2Exist > 0)
    {
        val3 = VVizGetData(data4, voxelInfoFront.texCoord);
        clr3 = VVizComputeVolumeRendering(val3, 4);         
    }
    else
    {
        val3 = 0.0;
        clr3 = vec4(0,0,0,0);
    }  
    
    // Combine color values but use highest alpha value  

    vec4 res;
    res.rgb = clr1.rgb + clr2.rgb + clr3.rgb;  

    //TODO - blending function implemntation
    res.r = max(res.r,clrHT.r);
    res.g = max(res.g,clrHT.g);
    res.b = max(res.b,clrHT.b);
    //res.rgb = (res.rgb + clrHT.rgb) / 2;

    res.a = max(clrHT.a, max(clr1.a, max(clr2.a, clr3.a)));        

    return res;
    //return clrHT;
}
