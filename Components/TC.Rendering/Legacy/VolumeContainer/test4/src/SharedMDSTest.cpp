#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaExaminer.h>

#include <iostream>
#include <QSettings>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QApplication>
#include <QPushButton>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <QTransferFunctionCanvas2D.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivHdf5Reader.h>
//#include <OivSliceContainer.h>
//#include <OivVolumeContainer.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
#include <QOiv3DRenderWindow.h>
#include <TCFMetaReader.h>
#include <ImageViz/SoImageViz.h>

//Create simple Qt+OIV 2D view with tooth image

int cur_time_step = 0;

auto makeColorMap(SbColor color, float gamma, int id) -> SoTransferFunction* {
	SoTransferFunction* pTF = new SoTransferFunction();
	pTF->transferFunctionId = id + 1;

	// Color map will contain 256 RGBA values -> 1024 float values.
	// setNum pre-allocates memory so we can edit the field directly.
	pTF->colorMap.setNum(256 * 4);	
	float R = color[0];
	float G = color[1];
	float B = color[2];

	// Get an edit pointer, assign values, then finish editing.
	float* p = pTF->colorMap.startEditing();
	for (int i = 0; i < 256; ++i) {
		float factor = (float)i / 255;
		float mod_r = R * factor;
		float mod_g = G * factor;
		float mod_b = B * factor;
		mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
		mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
		mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
		*p++ = pow(factor, gamma);
	}
	pTF->colorMap.finishEditing();
	return pTF;
}

void main(int argc, char** argv) {
	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if(prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if(fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();
	
	//Read meta data
	auto metaReader = new TC::IO::TCFMetaReader;
	auto metaInfo = metaReader->Read(fileName);	

	if (false == metaInfo->data.data3DFL.exist) {
		std::cout << "fl 3d data is not exist" << std::endl;
		return;
	}

	if(false == metaInfo->data.isLDM) {
		std::cout << "file is not an ldm" << std::endl;
		//return;
	}

	if(metaInfo->data.data3D.dataCount < 2) {
		std::cout << "file is not time-lapse" << std::endl;
		return;
	}
	//read data
	//Use HT + green channel
	auto htReader = new OivHdf5Reader;	
	htReader->setTileName("000000");		
	htReader->setTileDimension(metaInfo->data.data3D.tileSizeX);
	htReader->setDataGroupPath("/Data/3D");
	htReader->setFilename(fileName.toStdString());

	auto htVolume = new SoVolumeData;
	htVolume->texturePrecision = 16;
	htVolume->ldmResourceParameters.getValue()->tileDimension.setValue(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
	htVolume->setName("volData0");
	htVolume->setReader(*htReader, TRUE);
	//htVolume->touch();
	htVolume->dataSetId = 1;
	htVolume->texturePrecision = 16;

	std::cout << "HT extent: " << htVolume->extent.getValue().getMax()[0] << " " << htVolume->extent.getValue().getMax()[1] << " " << htVolume->extent.getValue().getMax()[2] << std::endl;
	std::cout << "HT tile: " << metaInfo->data.data3D.tileSizeX << " " << metaInfo->data.data3D.tileSizeY << " " << metaInfo->data.data3D.tileSizeZ << std::endl;
	double min, max;
	min = metaInfo->data.data3D.riMin * 10000.0;
	max = metaInfo->data.data3D.riMax * 10000.0;	

	auto flReader = new OivHdf5Reader(true);	
	flReader->setTileName("000000");	
	flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flReader->setDataGroupPath("/Data/3DFL/CH1");
	flReader->setFilename(fileName.toStdString());
		
	auto flVolume = new SoVolumeData;
	flVolume->setName("flVolume");
	flVolume->setReader(*flReader, TRUE);
	//flVolume->touch();
	flVolume->dataSetId = 2;
	flVolume->texturePrecision = 16;

	std::cout << "FL extent: " << flVolume->extent.getValue().getMax()[0] << " " << flVolume->extent.getValue().getMax()[1] << " " << flVolume->extent.getValue().getMax()[2] << std::endl;
	auto offsetZ = metaInfo->data.data3DFL.offsetZ - (metaInfo->data.data3DFL.sizeZ * metaInfo->data.data3DFL.resolutionZ / 2);
	std::cout << "offset z: " << offsetZ << std::endl;
	auto oriextent = flVolume->extent.getValue();
	flVolume->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);

	//Create scene graph with shared multi-data separator
	SoMultiDataSeparator* mds = new SoMultiDataSeparator;

	SoSeparator* root3d = new SoSeparator;	
	SoSeparator* rootXY = new SoSeparator;	
	SoSeparator* rootYZ = new SoSeparator;	
	SoSeparator* rootXZ = new SoSeparator;	

	//Create indicator SoSwitch nodes
	{
		SoSwitch* indicator3d = new SoSwitch;
		root3d->addChild(indicator3d);
		indicator3d->addChild(mds);
		indicator3d->whichChild = 0;

		SoSwitch* indicatorXY = new SoSwitch;
		rootXY->addChild(indicatorXY);
		indicatorXY->addChild(new SoGroup);
		indicatorXY->addChild(mds);
		indicatorXY->whichChild = 1;

		SoSwitch* indicatorYZ = new SoSwitch;
		rootYZ->addChild(indicatorYZ);
		indicatorYZ->addChild(new SoGroup);
		indicatorYZ->addChild(new SoGroup);
		indicatorYZ->addChild(mds);
		indicatorYZ->whichChild = 2;

		SoSwitch* indicatorXZ = new SoSwitch;
		rootXZ->addChild(indicatorXZ);
		indicatorXZ->addChild(new SoGroup);
		indicatorXZ->addChild(new SoGroup);
		indicatorXZ->addChild(new SoGroup);
		indicatorXZ->addChild(mds);
		indicatorXZ->whichChild = 3;
	}

	//create shared multi data separator	
	{
		SbColor red(1, 0, 0);
		SbColor green(0, 1, 1);

		mds->addChild(htVolume);
		SoDataRange* rangeHT = new SoDataRange;
		rangeHT->dataRangeId = 1;
		mds->addChild(rangeHT);
		auto colorHT = makeColorMap(red, 3.37f,0);
		mds->addChild(colorHT);

		MedicalHelper::dicomAdjustDataRange(rangeHT, htVolume);

		mds->addChild(flVolume);
		SoDataRange* rangeCH1 = new SoDataRange;
		rangeCH1->dataRangeId = 2;
		mds->addChild(rangeCH1);
		auto colorCH1 = makeColorMap(green, 3.37f, 1);
		mds->addChild(colorCH1);

		MedicalHelper::dicomAdjustDataRange(rangeCH1, flVolume);
		auto oriMax = rangeCH1->max.getValue();
		rangeCH1->max.setValue(oriMax / 4);
	}	

	//create inheriting scene graphs
	MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
	{		
		//multi-modal shader
		const auto shaderPath = QString("%1/shader/2DTransferFunctionCustomShader.glsl").arg(qApp->applicationDirPath());
		auto multiModalFrag = new SoFragmentShader;
		multiModalFrag->sourceProgram.setValue(shaderPath.toStdString());
		multiModalFrag->addShaderParameter1i("data1", 1);
		multiModalFrag->addShaderParameter1i("data2", 2);

		SoSwitch* inheritor = new SoSwitch;
		inheritor->whichChild = -2;//inherit from parent switch		
		mds->addChild(inheritor);				

	    //Renderer part for 3d render window
		SoGroup* inheGroup3d = new SoGroup;
		inheritor->addChild(inheGroup3d);

		SoVolumeRenderingQuality* volQual = new SoVolumeRenderingQuality;		
		volQual->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);		
		inheGroup3d->addChild(volQual);

		SoVolumeRender* renderer3d = new SoVolumeRender;		
		inheGroup3d->addChild(renderer3d);

		//Renderer part for XY slice
		SoGroup* inheGroupXY = new SoGroup;
		inheritor->addChild(inheGroupXY);

		SoVolumeShader* shaderXY = new SoVolumeShader;
		shaderXY->forVolumeOnly = FALSE;
		shaderXY->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);
		inheGroupXY->addChild(shaderXY);

		SoOrthoSlice* sliceXY = new SoOrthoSlice;
		sliceXY->dataSetId = 1;
		sliceXY->axis = ax[0];
		sliceXY->sliceNumber.setValue(metaInfo->data.data3D.sizeZ / 2);
		inheGroupXY->addChild(sliceXY);
		
		//Renderer part for YZ slice
		SoGroup* inheGroupYZ = new SoGroup;
		inheritor->addChild(inheGroupYZ);

		SoVolumeShader* shaderYZ = new SoVolumeShader;
		shaderYZ->forVolumeOnly = FALSE;
		shaderYZ->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);
		inheGroupYZ->addChild(shaderYZ);

		SoOrthoSlice* sliceYZ = new SoOrthoSlice;
		sliceYZ->dataSetId = 1;
		sliceYZ->axis = ax[1];
		sliceYZ->sliceNumber.setValue(metaInfo->data.data3D.sizeX / 2);
		inheGroupYZ->addChild(sliceYZ);

		//Renderer part for XZ slice
		SoGroup* inheGroupXZ = new SoGroup;
		inheritor->addChild(inheGroupXZ);

		SoVolumeShader* shaderXZ = new SoVolumeShader;
		shaderXZ->forVolumeOnly = FALSE;
		shaderXZ->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag);		
		inheGroupXZ->addChild(shaderXZ);

		SoOrthoSlice* sliceXZ = new SoOrthoSlice;
		sliceXZ->dataSetId = 1;
		sliceXZ->axis = ax[2];
		sliceXZ->sliceNumber.setValue(metaInfo->data.data3D.sizeY / 2);
		inheGroupXZ->addChild(sliceXZ);
	}

	//Create Render Window UI
	QMainWindow* window = new QMainWindow;
	window->setWindowTitle("Shared MultiDataSeparator");
	window->resize(2000, 500);
	window->show();

	QWidget* centralWidget = new QWidget(window);
	window->setCentralWidget(centralWidget);
	QWidget* containerWidget = new QWidget(window);	
	auto renderLayout = new QHBoxLayout;
	renderLayout->setContentsMargins(0, 0, 0, 0);
	containerWidget->setLayout(renderLayout);

	centralWidget->setLayout(new QVBoxLayout);
	centralWidget->layout()->setContentsMargins(0, 0, 0, 0);
	centralWidget->layout()->setSpacing(0);

	centralWidget->layout()->addWidget(containerWidget);

	RenderAreaOrbiter* renderArea3D = new RenderAreaOrbiter(window);
	const float grayVal = 0.99f;
	renderArea3D->setClearColor(SbColorRGBA(grayVal, grayVal, grayVal, 1.0f));
	//centralWidget->layout()->addWidget(renderArea->getContainerWidget());
	renderLayout->addWidget(renderArea3D->getContainerWidget());
	renderArea3D->setSceneGraph(root3d);
	renderArea3D->viewAll(SbViewportRegion());	
	
	RenderAreaOrbiter* renderAreaXY = new RenderAreaOrbiter(window);
	renderAreaXY->setSceneGraph(rootXY);
	MedicalHelper::orientView(ax[0], renderAreaXY->getSceneInteractor()->getCamera(), htVolume);

	RenderAreaOrbiter* renderAreaYZ = new RenderAreaOrbiter(window);
	renderAreaYZ->setSceneGraph(rootYZ);
	MedicalHelper::orientView(ax[1], renderAreaYZ->getSceneInteractor()->getCamera(), htVolume);

	RenderAreaOrbiter* renderAreaXZ = new RenderAreaOrbiter(window);
	renderAreaXZ->setSceneGraph(rootXZ);
	MedicalHelper::orientView(ax[2], renderAreaXZ->getSceneInteractor()->getCamera(), htVolume);

	renderLayout->addWidget(renderAreaXY->getContainerWidget());
	renderLayout->addWidget(renderAreaYZ->getContainerWidget());
	renderLayout->addWidget(renderAreaXZ->getContainerWidget());
	

	QPushButton* timeStepBtn = new QPushButton(nullptr);
	timeStepBtn->setText("Toggle Time Step");
	centralWidget->layout()->addWidget(timeStepBtn);
		

	QObject::connect(timeStepBtn, &QPushButton::clicked, [=]() {
		cur_time_step = !cur_time_step;
		QString tileName = QString("%1").arg(cur_time_step, 6, 10, QLatin1Char('0'));
		htReader->setTileName(tileName.toStdString());
		htReader->touch();
		flReader->setTileName(tileName.toStdString());
		flReader->touch();

		flVolume->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);
	});
	    	
	app.exec();

	delete renderArea3D;
	delete renderAreaXY;
	delete renderAreaYZ;
	delete renderAreaXZ;

	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();
}