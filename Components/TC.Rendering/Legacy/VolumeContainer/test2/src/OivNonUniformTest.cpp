#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>

#include <iostream>
#include <QSettings>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QApplication>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include <QTransferFunctionCanvas2D.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivHdf5Reader.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
#include <QOiv3DRenderWindow.h>
#include <TCFMetaReader.h>
#include <ImageViz/SoImageViz.h>

//Create simple Qt+OIV 2D view with tooth image
void main(int argc, char** argv) {
	
}