#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaExaminer.h>

#include <iostream>
#include <QSettings>
#include <QFileDialog>
#include <QGridLayout>
#include <QVBoxLayout>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>

#include <QTransferFunctionCanvas2D.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivHdf5Reader.h>
#include <OivSliceContainer.h>
#include <OivVolumeContainer.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
#include <QOiv3DRenderWindow.h>
#include <TCFMetaReader.h>
#include <ImageViz/SoImageViz.h>

//Create simple Qt+OIV 2D view with tooth image

int cur_time_step = 0;

auto makeColorMap(SbColor color, float gamma, int id) -> SoTransferFunction* {
	SoTransferFunction* pTF = new SoTransferFunction();
	pTF->transferFunctionId = id + 1;

	// Color map will contain 256 RGBA values -> 1024 float values.
	// setNum pre-allocates memory so we can edit the field directly.
	pTF->colorMap.setNum(256 * 4);	
	float R = color[0];
	float G = color[1];
	float B = color[2];

	// Get an edit pointer, assign values, then finish editing.
	float* p = pTF->colorMap.startEditing();
	for (int i = 0; i < 256; ++i) {
		float factor = (float)i / 255;
		float mod_r = R * factor;
		float mod_g = G * factor;
		float mod_b = B * factor;
		mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
		mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
		mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
		*p++ = pow(factor, gamma);
	}
	pTF->colorMap.finishEditing();
	return pTF;
}

void main(int argc, char** argv) {
	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if(prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if(fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();
	OivHdf5Reader::initClass();

	//Read meta data
	auto metaReader = new TC::IO::TCFMetaReader;
	auto metaInfo = metaReader->Read(fileName);	

	if (false == metaInfo->data.data3DFL.exist) {
		std::cout << "fl 3d data is not exist" << std::endl;
		return;
	}

	if(false == metaInfo->data.isLDM) {
		std::cout << "file is not an ldm" << std::endl;
		//return;
	}

	if(metaInfo->data.data3D.dataCount < 2) {
		std::cout << "file is not time-lapse" << std::endl;
		return;
	}
	//read data
	//Use HT + green channel
	SoRef<OivHdf5Reader> htReader = new OivHdf5Reader;
	//for (auto i = 0; i < 4; i++) {		
		htReader->setTileName("000000");
		htReader->setTileDimension(metaInfo->data.data3D.tileSizeX);
		htReader->setDataGroupPath("/Data/3D");
		htReader->setFilename(fileName.toStdString());	
	
	//}

	SoRef<SoVolumeData> htVolume[4];
	for (auto i = 0; i < 4; i++) {
		//auto htVolume = new SoVolumeData;
		htVolume[i] = new SoVolumeData;				
		htVolume[i]->setReader(*htReader,TRUE);
		//htVolume[i]->touch();
		//htVolume[i]->fileName = "$OIVHOME/examples/source/VolumeViz/multiChannel/syn_g_chan.vol";
		//htVolume[i]->fileName = "$OIVHOME/examples/data/Medical/files/medicalFoot.ldm";
		//htVolume[i]->fileName = "$OIVHOME/examples/data/Medical/files/spine.lda";
		//htVolume[i]->ldmResourceParameters.getValue()->tileDimension.setValue(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
		htVolume[i]->setName("volData0");		
		htVolume[i]->dataSetId = 1;
		//htVolume[i]->texturePrecision = 16;
		//htVolume[i]->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		//htVolume[i]->ldmResourceParameters.getValue()->resolution = 0;
	}

	std::cout << "HT extent: " << htVolume[0]->extent.getValue().getMax()[0] << " " << htVolume[0]->extent.getValue().getMax()[1] << " " << htVolume[0]->extent.getValue().getMax()[2] << std::endl;
	std::cout << "HT tile: " << metaInfo->data.data3D.tileSizeX << " " << metaInfo->data.data3D.tileSizeY << " " << metaInfo->data.data3D.tileSizeZ << std::endl;
	std::cout << "HT size: " << htVolume[0]->getDimension() << std::endl;
	double min, max;	
	min = metaInfo->data.data3D.riMin * 10000.0;
	max = metaInfo->data.data3D.riMax * 10000.0;	
		
	SoRef<OivHdf5Reader> flReader = new OivHdf5Reader(true);
	//for (auto i = 0; i < 4; i++) {	
	flReader->setTileName("000000");
	flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
	flReader->setDataGroupPath("/Data/3DFL/CH1");
	flReader->setFilename(fileName.toStdString());
	
	//}

	SoRef<SoVolumeData> flVolume[4];
	for (auto i = 0; i < 4; i++) {
		//auto flVolume = new SoVolumeData;
		flVolume[i] = new SoVolumeData;
		flVolume[i]->setName("flVolume");
		flVolume[i]->setReader(*flReader,TRUE);
		//flVolume[i]->fileName = "$OIVHOME/examples/source/VolumeViz/multiChannel/syn_r_chan.vol";
		//flVolume[i]->fileName = "$OIVHOME/examples/data/Medical/files/medicalFoot.ldm";
		//flVolume[i]->fileName = "$OIVHOME/examples/data/Medical/files/spine.lda";
		//flVolume[i]->touch();
		flVolume[i]->dataSetId = 2;
		//flVolume[i]->texturePrecision = 16;
		//flVolume[i]->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		//flVolume[i]->ldmResourceParameters.getValue()->resolution = 0;
	}
	

	std::cout << "FL extent: " << flVolume[0]->extent.getValue().getMax()[0] << " " << flVolume[0]->extent.getValue().getMax()[1] << " " << flVolume[0]->extent.getValue().getMax()[2] << std::endl;
	auto offsetZ = metaInfo->data.data3DFL.offsetZ - (metaInfo->data.data3DFL.sizeZ * metaInfo->data.data3DFL.resolutionZ / 2);
	std::cout << "offset z: " << offsetZ << std::endl;
	auto oriextent = flVolume[0]->extent.getValue();
	for (auto i = 0; i < 4; i++) {
		flVolume[i]->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);
	}

	//Create scene graph with shared multi-data separator - NO each scene graph;
	SoRef<SoMultiDataSeparator> mds[4];// = new SoMultiDataSeparator;
	for(auto i=0;i<4;i++) {
		mds[i] = new SoMultiDataSeparator;
	}

	SoRef<SoSeparator> root3d = new SoSeparator;
	root3d->addChild(mds[0].ptr());
	SoRef<SoSeparator> rootXY = new SoSeparator;
	rootXY->addChild(mds[1].ptr());
	SoRef<SoSeparator> rootYZ = new SoSeparator;
	rootYZ->addChild(mds[2].ptr());
	SoRef<SoSeparator> rootXZ = new SoSeparator;
	rootXZ->addChild(mds[3].ptr());
	
	//shared nodes
	SbColor red(1, 0, 0);
	SbColor green(0, 1, 1);
	SoRef<SoDataRange> rangeHT[4];// = new SoDataRange;
	//rangeHT->dataRangeId = 1;
	SoRef<SoTransferFunction> colorHT[4];// = makeColorMap(red, 3.37f, 0);
	SoRef<SoDataRange> rangeCH1[4];// = new SoDataRange;
	//rangeCH1->dataRangeId = 2;
	SoRef<SoTransferFunction> colorCH1[4];// = makeColorMap(green, 3.37f, 1);
	//MedicalHelper::dicomAdjustDataRange(rangeHT.ptr(), htVolume[0].ptr());
	//MedicalHelper::dicomAdjustDataRange(rangeCH1.ptr(), flVolume[0].ptr());
	//auto oriMax = rangeCH1->max.getValue();
	//rangeCH1->max.setValue(oriMax / 4);
	//create shared multi data separator for each scene graph
	for(auto i=0;i<4;i++)
	{
		rangeHT[i] = new SoDataRange;
		rangeHT[i]->dataRangeId = 1;

		rangeCH1[i] = new SoDataRange;
		rangeCH1[i]->dataRangeId = 2;

		MedicalHelper::dicomAdjustDataRange(rangeHT[i].ptr(), htVolume[i].ptr());
	    MedicalHelper::dicomAdjustDataRange(rangeCH1[i].ptr(), flVolume[i].ptr());

		colorHT[i] = makeColorMap(red, 3.37f, 0);

		colorCH1[i] = makeColorMap(green, 3.37f, 1);

		auto oriMax = rangeCH1[i]->max.getValue();
		rangeCH1[i]->max.setValue(oriMax / 4);

		mds[i]->addChild(htVolume[i].ptr());
		mds[i]->addChild(rangeHT[i].ptr());
		mds[i]->addChild(colorHT[i].ptr());		

		mds[i]->addChild(flVolume[i].ptr());
		mds[i]->addChild(rangeCH1[i].ptr());		
		mds[i]->addChild(colorCH1[i].ptr());				
	}

	MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,MedicalHelper::SAGITTAL,MedicalHelper::CORONAL };
	//multi-modal shader
	const auto shaderPath = QString("%1/shader/2DTransferFunctionCustomShader.glsl").arg(qApp->applicationDirPath());
	SoRef<SoFragmentShader> multiModalFrag[4];// = new SoFragmentShader;
	//multiModalFrag->sourceProgram.setValue(shaderPath.toStdString());
	//multiModalFrag->addShaderParameter1i("data1", 1);
	//multiModalFrag->addShaderParameter1i("data2", 2);

	for(auto i=0;i<4;i++) {
		multiModalFrag[i] = new SoFragmentShader;
		multiModalFrag[i]->sourceProgram.setValue(shaderPath.toStdString());
		multiModalFrag[i]->addShaderParameter1i("data1", 1);
		multiModalFrag[i]->addShaderParameter1i("data2", 2);
	}

	//create scene graph for 3D
	{
		SoRef<SoVolumeRenderingQuality> volQual = new SoVolumeRenderingQuality;
		volQual->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag[0].ptr());
		mds[0]->addChild(volQual.ptr());

		SoRef <SoVolumeRender> renderer3d = new SoVolumeRender;
		mds[0]->addChild(renderer3d.ptr());
	}
	//create scenegraph for XY
	{
		SoRef<SoVolumeShader> shaderXY = new SoVolumeShader;
		shaderXY->forVolumeOnly = FALSE;
		shaderXY->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag[1].ptr());
		mds[1]->addChild(shaderXY.ptr());

		SoRef<SoOrthoSlice> sliceXY = new SoOrthoSlice;
		sliceXY->dataSetId = 1;
		sliceXY->axis = ax[0];
		sliceXY->sliceNumber.setValue(metaInfo->data.data3D.sizeZ / 2);
		mds[1]->addChild(sliceXY.ptr());
	}
	//create scenegraph for YZ
	{
		SoRef<SoVolumeShader> shaderYZ = new SoVolumeShader;
		shaderYZ->forVolumeOnly = FALSE;
		shaderYZ->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag[2].ptr());
		mds[2]->addChild(shaderYZ.ptr());

		SoRef<SoOrthoSlice> sliceYZ = new SoOrthoSlice;
		sliceYZ->dataSetId = 1;
		sliceYZ->axis = ax[1];
		sliceYZ->sliceNumber.setValue(metaInfo->data.data3D.sizeX / 2);
		mds[2]->addChild(sliceYZ.ptr());
	}
	//create scenegraph for XZ
	{
		SoRef<SoVolumeShader> shaderXZ = new SoVolumeShader;
		shaderXZ->forVolumeOnly = FALSE;
		shaderXZ->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, multiModalFrag[3].ptr());
		mds[3]->addChild(shaderXZ.ptr());

		SoRef<SoOrthoSlice> sliceXZ = new SoOrthoSlice;
		sliceXZ->dataSetId = 1;
		sliceXZ->axis = ax[2];
		sliceXZ->sliceNumber.setValue(metaInfo->data.data3D.sizeY / 2);
		mds[3]->addChild(sliceXZ.ptr());
	}
	
	//Create Render Window UI
	QMainWindow* window = new QMainWindow;
	window->setWindowTitle("Shared ReaderTest");
	window->resize(1000, 1000);
	window->show();

	QWidget* centralWidget = new QWidget(window);
	window->setCentralWidget(centralWidget);
	QWidget* containerWidget = new QWidget(window);	
	//auto renderLayout = new QHBoxLayout;
	//renderLayout->setContentsMargins(0, 0, 0, 0);
	auto renderLayout = new QGridLayout;
	renderLayout->setContentsMargins(0, 0, 0, 0);
	containerWidget->setLayout(renderLayout);

	centralWidget->setLayout(new QVBoxLayout);
	centralWidget->layout()->setContentsMargins(0, 0, 0, 0);
	centralWidget->layout()->setSpacing(0);

	centralWidget->layout()->addWidget(containerWidget);

	RenderAreaOrbiter* renderArea3D = new RenderAreaOrbiter(window);
	const float grayVal = 0.99f;
	renderArea3D->setClearColor(SbColorRGBA(grayVal, grayVal, grayVal, 1.0f));
	//centralWidget->layout()->addWidget(renderArea->getContainerWidget());
	renderLayout->addWidget(renderArea3D->getContainerWidget(),0,0);
	renderArea3D->setSceneGraph(root3d.ptr());
	renderArea3D->viewAll(SbViewportRegion());	
	
	RenderAreaOrbiter* renderAreaXY = new RenderAreaOrbiter(window);
	renderAreaXY->setSceneGraph(rootXY.ptr());
	MedicalHelper::orientView(ax[0], renderAreaXY->getSceneInteractor()->getCamera(), htVolume[1].ptr());

	RenderAreaOrbiter* renderAreaYZ = new RenderAreaOrbiter(window);
	renderAreaYZ->setSceneGraph(rootYZ.ptr());
	MedicalHelper::orientView(ax[1], renderAreaYZ->getSceneInteractor()->getCamera(), htVolume[2].ptr());

	RenderAreaOrbiter* renderAreaXZ = new RenderAreaOrbiter(window);
	renderAreaXZ->setSceneGraph(rootXZ.ptr());
	MedicalHelper::orientView(ax[2], renderAreaXZ->getSceneInteractor()->getCamera(), htVolume[3].ptr());

	//renderLayout->addWidget(renderAreaXY->getContainerWidget());
	renderLayout->addWidget(renderAreaXY->getContainerWidget(),0,1);
	//renderLayout->addWidget(renderAreaYZ->getContainerWidget());
	renderLayout->addWidget(renderAreaYZ->getContainerWidget(),1,0);
	//renderLayout->addWidget(renderAreaXZ->getContainerWidget());
	renderLayout->addWidget(renderAreaXZ->getContainerWidget(),1,1);
	

	QPushButton* timeStepBtn = new QPushButton(nullptr);
	timeStepBtn->setText("Toggle Time Step");
	centralWidget->layout()->addWidget(timeStepBtn);
		

	QObject::connect(timeStepBtn, &QPushButton::clicked, [=]() {
		cur_time_step = !cur_time_step;
		QString tileName = QString("%1").arg(cur_time_step, 6, 10, QLatin1Char('0'));
	    /*htReader->setTileName(tileName.toStdString());
	    htReader->touch();		
	    flReader->setTileName(tileName.toStdString());
	    flReader->touch();*/
				
		for (auto i = 0; i < 4; i++) {			
			//flVolume[i]->extent.setValue(oriextent.getMin()[0], oriextent.getMin()[1], oriextent.getMin()[2] + offsetZ, oriextent.getMax()[0], oriextent.getMax()[1], oriextent.getMax()[2] + offsetZ);
			const SbBox3i32 volDataBox(SbVec3i32(0, 0, 0), htVolume[i]->data.getSize() - SbVec3i32(1, 1, 1));
			htVolume[i]->updateRegions(&volDataBox, 1);

			const SbBox3i32 volDataBoxFL(SbVec3i32(0, 0, 0), flVolume[i]->data.getSize() - SbVec3i32(1, 1, 1));
			flVolume[i]->updateRegions(&volDataBoxFL, 1);
		}
	});
	    	
	app.exec();

	delete renderArea3D;
	delete renderAreaXY;
	delete renderAreaYZ;
	delete renderAreaXZ;

	OivHdf5Reader::exitClass();
	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();

	
	return;
}