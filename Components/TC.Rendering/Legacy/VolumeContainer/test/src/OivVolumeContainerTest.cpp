#include <iostream>
#include <QOpenGLWidget>
#include <QFileDialog>
#include <QApplication>

#include <VolumeViz/nodes/SoVolumeData.h>

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>

#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Medical/helpers/MedicalHelper.h>

#include "OivTcfReader.h"
#include "QOiv3DRenderWindow.h"

//Create simple Qt+OIV 2D view with tooth image
void main(int argc, char** argv) {
	QApplication app(argc, argv);

	SoVolumeRendering::init();

	// can found in multi window test

	app.exec();

	SoVolumeRendering::finish();
}