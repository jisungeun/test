/*=======================================================================
 *** THE CONTENT OF THIS WORK IS PROPRIETARY TO FEI S.A.S, (FEI S.A.S.),            ***
 ***              AND IS DISTRIBUTED UNDER A LICENSE AGREEMENT.                     ***
 ***                                                                                ***
 ***  REPRODUCTION, DISCLOSURE,  OR USE,  IN WHOLE OR IN PART,  OTHER THAN AS       ***
 ***  SPECIFIED  IN THE LICENSE ARE  NOT TO BE  UNDERTAKEN  EXCEPT WITH PRIOR       ***
 ***  WRITTEN AUTHORIZATION OF FEI S.A.S.                                           ***
 ***                                                                                ***
 ***                        RESTRICTED RIGHTS LEGEND                                ***
 ***  USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT OF THE CONTENT OF THIS      ***
 ***  WORK OR RELATED DOCUMENTATION IS SUBJECT TO RESTRICTIONS AS SET FORTH IN      ***
 ***  SUBPARAGRAPH (C)(1) OF THE COMMERCIAL COMPUTER SOFTWARE RESTRICTED RIGHT      ***
 ***  CLAUSE  AT FAR 52.227-19  OR SUBPARAGRAPH  (C)(1)(II)  OF  THE RIGHTS IN      ***
 ***  TECHNICAL DATA AND COMPUTER SOFTWARE CLAUSE AT DFARS 52.227-7013.             ***
 ***                                                                                ***
 ***                   COPYRIGHT (C) 1996-2017 BY FEI S.A.S,                        ***
 ***                        MERIGNAC, FRANCE                                        ***
 ***                      ALL RIGHTS RESERVED                                       ***
**=======================================================================*/
/*=======================================================================
** Author      : Julien SALLANNE (July 2011)
**=======================================================================*/
#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/sys/SoGL.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/nodes/SoShaderProgram.h>
#include <Inventor/elements/SoTextureUnitElement.h>
#include <Inventor/SoPreferences.h>
#pragma warning(pop)

#include "SoTransferFunction2D.h"
  
using std::vector;

SO_NODE_SOURCE(SoTransferFunction2D)                              // Static Members

//-----------------------------------------------------------------------------
SoTransferFunction2D::SoTransferFunction2D() {
	SO_NODE_CONSTRUCTOR(SoTransferFunction2D);

	m_tfTexUnit = SoPreferences::getInt("IVVR_TF2D_TEX_UNIT15", 15);
}

SoTransferFunction2D::SoTransferFunction2D(int tu) {
    SO_NODE_CONSTRUCTOR(SoTransferFunction2D);

    std::string name = "IVVR_TF2D_TEX_UNIT" + std::to_string(tu);

    //m_tfTexUnit = SoPreferences::getInt("IVVR_TF2D_TEX_UNIT", tu);
    m_tfTexUnit = SoPreferences::getInt(name.c_str(), tu);
}

//-----------------------------------------------------------------------------
SoTransferFunction2D::~SoTransferFunction2D()
{
}

void
SoTransferFunction2D::initClass()
{
	getClassRenderEngineMode().setRenderMode(SbRenderEngineMode::OIV_OPENINVENTOR_RENDERING);    
	SO__NODE_INIT_CLASS(SoTransferFunction2D, "TransferFunction2D", SoTexture2);
}

void
SoTransferFunction2D::exitClass()
{
	SO__NODE_EXIT_CLASS(SoTransferFunction2D);
}

//-----------------------------------------------------------------------------
void
SoTransferFunction2D::GLRender(SoGLRenderAction* action)
{        
	SoTextureUnitElement::set(action->getState(), this, m_tfTexUnit);    
	SoTexture2::GLRender(action);
}


