#include <QApplication>
#include <QOpenGLWidget>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoRenderToTextureProperty.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#pragma warning(pop)

#include <SoTransferFunction2D.h>

#include "OivScene3D.h"

#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoVertexProperty.h>

#include "QOiv2DRenderWindow.h"

namespace TC {
	struct OivScene3D::Impl {
		SoRef<SoSeparator> hideSep2D { nullptr };
		QString nonUniformShader;
		QString uniformShader;

		SoRef<SoRenderToTextureProperty> renderToTexProperty2D { nullptr };
		SoRef<SoSeparator> renderRoot { nullptr };
		SoRef<SoMultiDataSeparator> mds { nullptr };
		SoRef<SoFragmentShader> fragmentShader { nullptr };
		int texture_unit { 15 };
		SoRef<SoVolumeRenderingQuality> qual { nullptr };
		SoRef<SoMaterial> matl { nullptr };
		SoRef<SoVolumeRender> render { nullptr };

		SoRef<SoTransferFunction2D> HT_tf2D { nullptr };
		SoRef<SoDataRange> HT_range { nullptr };
		SoRef<SoSwitch> HT_stepper { nullptr };
		SoRef<SoVolumeData> HT_volume { nullptr };
		int ht_time_step { 0 };

		float fl_color[3][3] { { 0.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0 }, { 1.0, 0.0, 0.0 } };
		SoRef<SoTransferFunction> FL_TF[3] { nullptr, nullptr, nullptr };
		SoRef<SoDataRange> FL_range[3] { nullptr, nullptr, nullptr };
		SoRef<SoSwitch> FL_stepper[3] { nullptr, nullptr, nullptr };
		SoRef<SoVolumeData> FL_volume[3] { nullptr, nullptr, nullptr };
		float fl_transparency[3] { 0.3f, 0.3f, 0.3f };
		bool fl_exist[3] { false, false, false };
		bool fl_show[3] { false, false, false };
		int fl_time_step[3] { 0, 0, 0 };
		double gamma[3] { 1, 1, 1 };
		bool isGamma[3] { false, false, false };

		int64_t min, max;
		int64_t range;

		IO::TCFMetaReader::Meta::Pointer meta { nullptr };
		double htValueOffset { 0 };
		double htValueDiv { 1 };
		double flValueOffset[3] { 0, 0, 0 };
	};

	OivScene3D::OivScene3D() : d { new Impl } {
		//buildTF2D();
		d->nonUniformShader = QString("%1/shader/NonUniform_frag.glsl").arg(qApp->applicationDirPath());
		d->uniformShader = QString("%1/shader/MultiModal_frag.glsl").arg(qApp->applicationDirPath());
	}

	OivScene3D::~OivScene3D() { }

	auto OivScene3D::SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
		if (d->meta->data.data3D.scalarType.count() > 0) {
			if (d->meta->data.data3D.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data3D.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		} else if (d->meta->data.data2DMIP.scalarType.count() > 0) {
			if (d->meta->data.data2DMIP.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data2DMIP.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		}

		for (auto i = 0; i < 3; i++) {
			if (d->meta->data.data3DFL.scalarType[i].count() > 0) {
				if (d->meta->data.data3DFL.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data3DFL.min[i];
				}
			} else if (d->meta->data.data2DFLMIP.scalarType[i].count() > 0) {
				if (d->meta->data.data2DFLMIP.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data2DFLMIP.min[i];
				}
			}
		}
	}

	auto OivScene3D::SetHiddenSep(SoSeparator* sep) -> void {
		d->hideSep2D = sep;
	}

	auto OivScene3D::GetRenderRoot() -> SoSeparator* {
		return d->renderRoot.ptr();
	}

	auto OivScene3D::SetTransparency(float transp) -> void {
		d->matl->transparency = transp;
	}

	auto OivScene3D::GetTransparency() -> float {
		return d->matl->transparency.getValues(0)[0];
	}

	auto OivScene3D::SetUniform(bool isUniform) -> void {
		if (isUniform) {
			d->fragmentShader->sourceProgram.setValue(d->uniformShader.toStdString());
		} else {
			d->fragmentShader->sourceProgram.setValue(d->nonUniformShader.toStdString());
		}
	}

	auto OivScene3D::BuildSceneGraph() -> bool {
		if (false == BuildTF2D()) {
			return false;
		}
		/*if(nullptr == d->HT_tf2D) {
			return;
		}*/
		d->renderRoot = new SoSeparator;
		d->renderRoot->ref();
		d->mds = new SoMultiDataSeparator;

		d->fragmentShader = new SoFragmentShader;
		d->fragmentShader->sourceProgram.setValue(d->nonUniformShader.toStdString());
		d->fragmentShader->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", d->texture_unit)); // for now force to 15
		d->fragmentShader->addShaderParameter1i("data1", 1);
		d->fragmentShader->addShaderParameter1i("isHTExist", 0);
		d->fragmentShader->addShaderParameter1i("data2", 2);
		d->fragmentShader->addShaderParameter1i("isCh0Exist", 0);
		d->fragmentShader->addShaderParameter1i("data3", 3);
		d->fragmentShader->addShaderParameter1i("isCh1Exist", 0);
		d->fragmentShader->addShaderParameter1i("data4", 4);
		d->fragmentShader->addShaderParameter1i("isCh2Exist", 0);
		d->fragmentShader->addShaderParameter1f("resX", 1);
		d->fragmentShader->addShaderParameter1f("resY", 1);
		d->fragmentShader->addShaderParameter1f("resZ", 1);

		d->qual = new SoVolumeRenderingQuality;
		d->qual->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShader.ptr());
		d->qual->preIntegrated = FALSE;
		d->qual->jittering = TRUE;
		d->qual->interpolateOnMove = TRUE;
		d->qual->lighting = FALSE;

		d->matl = new SoMaterial;
		d->matl->diffuseColor.setValue(1.0, 1.0, 1.0);
		d->matl->ambientColor.setValue(0.5, 0.5, 0.5);
		d->matl->specularColor.setValue(0.2f, 0.2f, 0.2f);
		d->matl->transparency = 0.5;

		d->renderRoot->addChild(d->matl.ptr());
		d->renderRoot->addChild(d->mds.ptr());

		d->HT_range = new SoDataRange;
		d->HT_range->setName("HT_volRange2D");
		d->HT_range->dataRangeId = 1;

		d->HT_tf2D = new SoTransferFunction2D(d->texture_unit);
		d->HT_tf2D->model.setValue(SoTexture2::Model::REPLACE);
		d->HT_tf2D->internalFormat.setValue(SoTransferFunction2D::RGBA_FORMAT);
		d->HT_tf2D->minFilter = SoTransferFunction2D::NEAREST;
		d->HT_tf2D->magFilter = SoTransferFunction2D::NEAREST;
		d->HT_tf2D->setName("HT_transferFunction2D");
		d->HT_tf2D->renderToTextureProperty.setValue(d->renderToTexProperty2D.ptr());

		for (auto i = 0; i < 3; i++) {
			SbColor rgb = SbColor(d->fl_color[i]);
			d->FL_TF[i] = makeColorMap(rgb, 3.39f, i);
			d->FL_TF[i]->setName((std::string("FL_TF_Ch") + std::to_string(i)).c_str());
			d->FL_TF[i]->transferFunctionId = i + 2;
			d->FL_range[i] = new SoDataRange;
			d->FL_range[i]->dataRangeId = i + 2;
			d->FL_range[i]->min = 0;
			d->FL_range[i]->max = 200;
			d->FL_range[i]->setName((std::string("FL_RANGE_Ch") + std::to_string(i)).c_str());
		}

		d->HT_stepper = new SoSwitch;
		d->HT_stepper->addChild(new SoSeparator);

		d->mds->addChild(d->HT_stepper.ptr());
		d->mds->addChild(d->HT_tf2D.ptr());
		d->mds->addChild(d->HT_range.ptr());

		for (auto ch = 0; ch < 3; ch++) {
			d->FL_stepper[ch] = new SoSwitch;
			d->FL_stepper[ch]->addChild(new SoSeparator);

			d->mds->addChild(d->FL_stepper[ch].ptr());
			d->mds->addChild(d->FL_TF[ch].ptr());
			d->mds->addChild(d->FL_range[ch].ptr());
		}

		d->render = new SoVolumeRender;
		d->render->subdivideTile = TRUE;
		d->render->numSlicesControl = SoVolumeRender::AUTOMATIC;
		//d->render->numSlices = 0;
		d->render->lowScreenResolutionScale = 4;
		d->render->lowResMode = SoVolumeRender::LowResMode::DECREASE_SCREEN_RESOLUTION;
		d->render->samplingAlignment = SoVolumeRender::SamplingAlignment::DATA_ALIGNED;

		d->mds->addChild(d->qual.ptr());
		d->mds->addChild(d->render.ptr());
		return true;
	}

	auto OivScene3D::BuildTF2D() -> bool {
		if (nullptr == d->hideSep2D) {
			return false;
		}
		d->renderToTexProperty2D = new SoRenderToTextureProperty;

		d->renderToTexProperty2D->component = SoRenderToTextureProperty::RGB_ALPHA;
		d->renderToTexProperty2D->size.setValue(SbVec2s(512, 512));
		d->renderToTexProperty2D->updatePolicy = SoRenderToTextureProperty::UpdatePolicy::WHEN_NEEDED;
		d->renderToTexProperty2D->setName("HT_renderToTexProperty");
		d->renderToTexProperty2D->node.set1Value(0, (SoNode*)d->hideSep2D.ptr());

		return true;
	}

	auto OivScene3D::SetHTVolume(SoVolumeData* vol, int step) -> void {
		d->HT_stepper->replaceChild(0, vol);
		d->HT_stepper->whichChild = 0;
		d->HT_volume = vol;

		d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		if (step == 0) {
			if (d->min == 0 && d->max == 0) {
				double mmin, mmax;
				vol->getMinMax(mmin, mmax);
				d->min = mmin;
				d->max = mmax;
			}
			d->HT_range->min = d->min / d->htValueDiv - d->htValueOffset;
			d->HT_range->max = d->max / d->htValueDiv - d->htValueOffset;
			d->range = d->max - d->min;
			const auto voxelSize = vol->getVoxelSize();
			d->fragmentShader->setShaderParameter1f("resX", static_cast<float>(voxelSize[0]));
			d->fragmentShader->setShaderParameter1f("resY", static_cast<float>(voxelSize[1]));
			d->fragmentShader->setShaderParameter1f("resZ", static_cast<float>(voxelSize[2]));
		}
	}

	auto OivScene3D::ClearHTVolume() -> void {
		d->HT_stepper->replaceChild(0, new SoSeparator);
		d->HT_volume = nullptr;
		d->fragmentShader->setShaderParameter1i("isHTExist", 0);
	}

	auto OivScene3D::ToggleHT(bool show) -> void {
		if (show) {
			d->fragmentShader->setShaderParameter1i("isHTExist", 1);
		} else {
			d->fragmentShader->setShaderParameter1i("isHTExist", 0);
		}
	}

	auto OivScene3D::GetHTRange(double& min, double& max) -> void {
		min = d->min;
		max = d->max;
	}

	auto OivScene3D::SetHTRange(double min, double max) -> void {
		d->min = min;
		d->max = max;
	}

	auto OivScene3D::SetHTTimeStep(int step) -> void {
		d->ht_time_step = step;
		if (d->ht_time_step > -1) {
			d->HT_stepper->whichChild = 0;
		} else {
			d->HT_stepper->whichChild = -1;
		}
	}

	auto OivScene3D::GetHTTimeStep() -> int {
		return d->ht_time_step;
	}

	auto OivScene3D::SetFLVolume(SoVolumeData* vol, int ch, int step) -> void {
		d->FL_stepper[ch]->replaceChild(0, vol);
		d->FL_volume[ch] = vol;
		vol->dataSetId = ch + 2;
		if (step == 0) {
			d->FL_range[ch]->min = 0;
			d->FL_range[ch]->max = 200;
		}
		d->fl_exist[ch] = true;
		ToggleChannel(ch, true);
	}

	auto OivScene3D::ClearFLVolume() -> void {
		for (auto ch = 0; ch < 3; ch++) {
			d->FL_stepper[ch]->replaceChild(0, new SoSeparator);
			d->FL_volume[ch] = nullptr;
			auto ch_text = QString("isCh%1Exist").arg(ch);
			d->fragmentShader->setShaderParameter1i(ch_text.toStdString(), 0);
			d->fl_exist[ch] = false;
		}
	}

	auto OivScene3D::SetFLExist(bool ch0, bool ch1, bool ch2) -> void {
		d->fl_exist[0] = ch0;
		d->fl_exist[1] = ch1;
		d->fl_exist[2] = ch2;
	}

	auto OivScene3D::SetFLRange(int ch, int min, int max) -> void {
		if (d->fl_exist[ch]) {
			d->FL_range[ch]->min = min - d->flValueOffset[ch];
			d->FL_range[ch]->max = max - d->flValueOffset[ch];
		}
	}

	auto OivScene3D::GetFLRange(int ch, int& min, int& max) -> void {
		if (d->fl_exist[ch]) {
			min = d->FL_range[ch]->min.getValue() + d->flValueOffset[ch];
			max = d->FL_range[ch]->max.getValue() + d->flValueOffset[ch];
		} else {
			min = -1;
			max = -1;
		}
	}

	auto OivScene3D::ToggleChannel(int ch, bool show) -> void {
		d->fl_show[ch] = show;
		if (d->fl_exist[ch]) {
			auto ch_text = QString("isCh%1Exist").arg(ch);
			if (show) {
				d->fragmentShader->setShaderParameter1i(ch_text.toStdString(), 1);
			} else {
				d->fragmentShader->setShaderParameter1i(ch_text.toStdString(), 0);
			}
		}
	}

	auto OivScene3D::SetFLTransparency(float transp, int ch) -> void {
		d->fl_transparency[ch] = transp;

		modifyColorMap(SbColor(d->fl_color[ch]), d->gamma[ch], ch);
	}

	auto OivScene3D::GetFLTransparency(int ch) -> float {
		return d->fl_transparency[ch];
	}

	auto OivScene3D::SetFLGamma(int ch, bool enable, double gamma) -> void {
		d->gamma[ch] = gamma;
		d->isGamma[ch] = enable;
		modifyColorMap(SbColor(d->fl_color[ch]), d->gamma[ch], ch);
	}

	auto OivScene3D::SetFLColor(int r, int g, int b, int ch) -> void {
		d->fl_color[ch][0] = static_cast<float>(r) / 255.0f;
		d->fl_color[ch][1] = static_cast<float>(g) / 255.0f;
		d->fl_color[ch][2] = static_cast<float>(b) / 255.0f;

		modifyColorMap(SbColor(d->fl_color[ch]), d->gamma[ch], ch);
	}

	auto OivScene3D::GetFLColor(int ch) -> std::tuple<int, int, int> {
		auto r = static_cast<int>(d->fl_color[ch][0] * 255.0f);
		auto g = static_cast<int>(d->fl_color[ch][1] * 255.0f);
		auto b = static_cast<int>(d->fl_color[ch][2] * 255.0f);

		return std::make_tuple(r, b, g);
	}

	auto OivScene3D::SetFLTimeStep(int step, int ch) -> void {
		d->fl_time_step[ch] = step;
		QString flShaderParam = QString("isCh%1Exist").arg(ch);
		if (d->fl_time_step[ch] > -1) {
			d->FL_stepper[ch]->whichChild = 0;
			if (d->fl_exist[ch] && d->fl_show[ch]) {
				d->fragmentShader->setShaderParameter1i(flShaderParam.toStdString(), 1);
			}
		} else {
			d->FL_stepper[ch]->whichChild = -1;
			d->fragmentShader->setShaderParameter1i(flShaderParam.toStdString(), 0);
		}
	}

	auto OivScene3D::GetFLTimeStep(int ch) -> int {
		return d->fl_time_step[ch];
	}

	auto OivScene3D::makeColorMap(SbColor color, float gamma, int id, float* range) -> SoTransferFunction* {
		SoTransferFunction* pTF = new SoTransferFunction();
		pTF->transferFunctionId = id + 2;

		// Color map will contain 256 RGBA values -> 1024 float values.
		// setNum pre-allocates memory so we can edit the field directly.
		pTF->colorMap.setNum(256 * 4);
		if (range != nullptr) {
			pTF->minValue = range[0];
			pTF->maxValue = range[1];
		}
		float R = color[0];
		float G = color[1];
		float B = color[2];

		// Get an edit pointer, assign values, then finish editing.
		float* p = pTF->colorMap.startEditing();
		for (int i = 0; i < 256; ++i) {
			float factor = (float)i / 255 * (1.0 - d->fl_transparency[id]);
			float mod_r = R * factor;
			float mod_g = G * factor;
			float mod_b = B * factor;
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = pow(factor, gamma);
		}
		pTF->colorMap.finishEditing();
		return pTF;
	}

	auto OivScene3D::modifyColorMap(SbColor color, float gamma, int id) -> void {
		auto tf = d->FL_TF[id];
		auto gammaValue = gamma;
		if (false == d->isGamma[id]) {
			gammaValue = 1;
		}
		if (nullptr != tf) {
			float R = color[0];
			float G = color[1];
			float B = color[2];
			float* p = tf->colorMap.startEditing();
			for (int i = 0; i < 256; ++i) {
				float factor = (float)i / 255 * (1.0 - d->fl_transparency[id]);
				float gammaFactor = pow((float)i / 255, (1.f / gammaValue)) * (1.0 - d->fl_transparency[id]);
				float mod_r = R * gammaFactor;
				float mod_g = G * gammaFactor;
				float mod_b = B * gammaFactor;
				mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
				mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
				mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
				*p++ = pow(factor, gamma + 2);//trick for volume rendering transparency
			}
			tf->colorMap.finishEditing();
		}
	}
}
