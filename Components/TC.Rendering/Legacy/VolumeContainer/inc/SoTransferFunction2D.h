/*=======================================================================
 *** THE CONTENT OF THIS WORK IS PROPRIETARY TO FEI S.A.S, (FEI S.A.S.),            ***
 ***              AND IS DISTRIBUTED UNDER A LICENSE AGREEMENT.                     ***
 ***                                                                                ***
 ***  REPRODUCTION, DISCLOSURE,  OR USE,  IN WHOLE OR IN PART,  OTHER THAN AS       ***
 ***  SPECIFIED  IN THE LICENSE ARE  NOT TO BE  UNDERTAKEN  EXCEPT WITH PRIOR       ***
 ***  WRITTEN AUTHORIZATION OF FEI S.A.S.                                           ***
 ***                                                                                ***
 ***                        RESTRICTED RIGHTS LEGEND                                ***
 ***  USE, DUPLICATION, OR DISCLOSURE BY THE GOVERNMENT OF THE CONTENT OF THIS      ***
 ***  WORK OR RELATED DOCUMENTATION IS SUBJECT TO RESTRICTIONS AS SET FORTH IN      ***
 ***  SUBPARAGRAPH (C)(1) OF THE COMMERCIAL COMPUTER SOFTWARE RESTRICTED RIGHT      ***
 ***  CLAUSE  AT FAR 52.227-19  OR SUBPARAGRAPH  (C)(1)(II)  OF  THE RIGHTS IN      ***
 ***  TECHNICAL DATA AND COMPUTER SOFTWARE CLAUSE AT DFARS 52.227-7013.             ***
 ***                                                                                ***
 ***                   COPYRIGHT (C) 1996-2017 BY FEI S.A.S,                        ***
 ***                        MERIGNAC, FRANCE                                        ***
 ***                      ALL RIGHTS RESERVED                                       ***
**=======================================================================*/
/*=======================================================================
** Author      : Julien SALLANNE (July 2011)
**=======================================================================*/
#ifndef  _SO_TRANSFER_FUNCTION_2D_Volume
#define  _SO_TRANSFER_FUNCTION_2D_Volume

#include <LDM/SoLDM.h>

#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoTexture2.h>

#include "TCVolumeContainerExport.h"

class SoGLRenderAction;
class SoCallbackAction;
class SoState;

#ifdef _WIN32
#pragma warning( push )
#pragma warning(disable:4251)
#pragma warning(disable:4819)
#endif

/**
 * @LDMEXT Describes the association between data set values and colors.
 *
 * @ingroup LDMNodes
 *
 * @DESCRIPTION
 *   This node defines a mapping from a couple of scalar data set values
 *   to color and transparency (alpha) values.
 *
 *
 * @FILE_FORMAT_DEFAULT
 *    TransferFunction2D {
 *    @TABLE_FILE_FORMAT
 *    @TABLE_END
 *    }
 *
 * @ACTION_BEHAVIOR
 * SoCallbackAction,
 * SoGLRenderAction @BR
 * Sets transfer function 2D parameters in the traversal state.
 *
 * @SEE_ALSO
 *    SoVolumeRender,
 *    SoOrthoSlice,
 *    SoObliqueSlice
 */

class TCVolumeContainer_API SoTransferFunction2D : public SoTexture2 {
	SO_NODE_HEADER(SoTransferFunction2D);

public:

	/**
	 * Constructor
	 */
	SoTransferFunction2D();
    SoTransferFunction2D(int tu);

	//------------------------------------------------------------------------------

SoEXTENDER public:
	virtual void GLRender(SoGLRenderAction *action);

	//------------------------------------------------------------------------------
SoINTERNAL public:

	static void initClass();
	static void exitClass();

protected:
	// Destructor
	virtual ~SoTransferFunction2D();

private:

	/**
	* Texture unit for the transfer function
	*/
	unsigned int m_tfTexUnit;

};

#if defined(_WIN32)
#pragma warning( pop )
#pragma warning(disable:4251)
#endif

#endif // _SO_TRANSFER_FUNCTION_2D_


