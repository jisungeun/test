#pragma once

#include <memory>

#include <QString>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSeparator.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <TCFMetaReader.h>

#include "TCVolumeContainerExport.h"

namespace TC {
	class TCVolumeContainer_API OivScene3D {
	public:
		OivScene3D();
		~OivScene3D();
		//prerequisites
		auto SetHiddenSep(SoSeparator* sep) -> void;

		auto SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void;

		//common
		auto BuildSceneGraph() -> bool;
		auto GetRenderRoot() -> SoSeparator*;
		auto SetTransparency(float transp) -> void;
		auto GetTransparency() -> float;
		auto SetUniform(bool isUniform) -> void;

		//HT
		auto SetHTVolume(SoVolumeData* vol, int step) -> void;
		auto ClearHTVolume() -> void;
		auto GetHTRange(double& min, double& max) -> void;
		auto SetHTRange(double min, double max) -> void;
		auto ToggleHT(bool show) -> void;
		auto SetHTTimeStep(int step) -> void;
		auto GetHTTimeStep() -> int;

		//FL
		auto SetFLVolume(SoVolumeData* vol, int ch, int step) -> void;
		auto ClearFLVolume() -> void;
		auto SetFLExist(bool ch0, bool ch1, bool ch2) -> void;
		auto SetFLRange(int ch, int min, int max) -> void;
		auto GetFLRange(int ch, int& min, int& max) -> void;
		auto ToggleChannel(int ch, bool show) -> void;
		auto SetFLTransparency(float transp, int ch) -> void;
		auto GetFLTransparency(int ch) -> float;
		auto SetFLColor(int r, int g, int b, int ch) -> void;
		auto GetFLColor(int ch) -> std::tuple<int, int, int>;
		auto SetFLGamma(int ch, bool enable, double gamma) -> void;
		auto SetFLTimeStep(int step, int ch) -> void;
		auto GetFLTimeStep(int ch) -> int;

	protected:
		auto BuildTF2D() -> bool;
		auto makeColorMap(SbColor color, float gamma, int id, float* range = nullptr) -> SoTransferFunction*;
		auto modifyColorMap(SbColor color, float gamma, int id) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
