#include <iostream>
#include <QApplication>
#include <QOiv2DRenderWindow.h>
#include <QFileDialog>

#include <VolumeViz/nodes/SoVolumeData.h>

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>

#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Medical/helpers/MedicalHelper.h>

//Create simple Qt+OIV 2D view with tooth image
void main(int argc,char** argv) {
	QApplication app(argc,argv);	

	QOiv2DRenderWindow* m_RenderWindow = new QOiv2DRenderWindow(nullptr);
	m_RenderWindow->setMinimumSize(400, 400);

	SoVolumeRendering::init();		

	//create scene graph for dummy cone
	SoSeparator *root = new SoSeparator;
	root->ref();

	root->addChild(new SoOrthographicCamera);

	auto volSep = new SoSeparator;

	auto volData = new SoVolumeData;
	//volData->setReader( *volReader );
	volData->fileName = "$OIVHOME/examples/data/Medical/files/tooth.ldm";
	////MedicalHelper::dicomAdjustVolume(volData);
	volSep->addChild(volData);

	SoDataRange* dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange, volData);
	volSep->addChild(dataRange);

	SoMaterial* volMatl = new SoMaterial;
	volMatl->diffuseColor.setValue(1, 1, 1);
	volSep->addChild(volMatl);

	SoTransferFunction* transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
	MedicalHelper::dicomCheckMonochrome1(transFunc, volData);
	volSep->addChild(transFunc);

	int numSlices = volData->data.getSize()[MedicalHelper::AXIAL];
	SoOrthoSlice* orthoSlice = new SoOrthoSlice;
	orthoSlice->axis = MedicalHelper::AXIAL;
	orthoSlice->sliceNumber = (numSlices > 1) ? (numSlices / 2) : 0;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volSep->addChild(orthoSlice);

	//sm_RenderWindow->viewingMode();

	root->addChild(volSep);

	m_RenderWindow->setSceneGraph(root);
	
    
	MedicalHelper::orientView(MedicalHelper::Axis::AXIAL, m_RenderWindow->getCamera(), volData);
    	

	m_RenderWindow->show();
	
	app.exec();

	SoVolumeRendering::finish();
}