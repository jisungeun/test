#pragma once
#include <memory>

#include <QOivRenderWindow.h>
#include <QDoubleSpinBox>
#include <QWidgetAction>

#include "TCRenderWindow2DExport.h"

class TCRenderWindow2D_API QOiv2DRenderWindow : public QOivRenderWindow {
	Q_OBJECT
public:
	QOiv2DRenderWindow(QWidget* parent);
	~QOiv2DRenderWindow();

	auto closeEvent(QCloseEvent* unused) -> void override;

	auto reset2DView(bool fromFunc = false)->void;
	auto resetViewBF(bool isBF)->bool;
	auto setHTRange(double min, double max)->void;
	auto setRenderWindowID(int idx)->void;
	auto refreshRangeSlider()->void;
	auto setType(bool is2D)->void;
	auto resetOnLoad()->void;
	auto setColorMapIdx(int idx)->void;

signals:
	void sigWheel(float);
	void sig2dCoord(float, float, float);
	void sigMinMax(float, float);
	void sigColorMap(int);

protected slots:
	void lowerLevelChanged(int val);
	void upperLevelChanged(int val);
	void lowerSpinChanged(double val);
	void upperSpinChanged(double val);

private:
	auto MouseButtonEvent(SoEventCallback* node) -> void override;
	auto MouseMoveEvent(SoEventCallback* node) -> void override;
	auto KeyboardEvent(SoEventCallback* node) -> void override;
	auto MouseWheelEvent(SoEventCallback* node) -> void override;

	auto setDefaultWindowType() -> void override;

	auto showContextMenu(SbVec2f pos)->void;

	auto changeScalarBarLabel(bool isInverse)->void;
	auto change2DColorMap(int idx)->void;
	auto initRangeSlider()->void;

	auto toggleScalarBar()->void;
	auto ScalarBarMove(bool activate)->void;
	auto ScalarBarScale(bool activate)->void;

	auto CalcVolumeCoord(SbVec2f norm_point)->SbVec3f;

	struct Impl;
	std::unique_ptr<Impl> d;
};