#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QVBoxLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/draggers/SoScale2Dragger.h>
#include <Inventor/draggers/SoScale2UniformDragger.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>
#include <Inventor/engines/SoCompose.h>
#include <Inventor/engines/SoCalculator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <RangeSlider.h>
#include <SpinBoxAction.h>

#include "QOiv2DRenderWindow.h"

#include <IvTune/SoIvTune.h>


struct QOiv2DRenderWindow::Impl {
	//colormap
	int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow

	//scalarBar
	bool is_scalarBar{ false };
	bool is_scalarTrans{ false };
	bool is_scalarScale{ false };

	//levelWindow
	TC::SpinBoxAction* levelWindow;//
	int level_min{ 0 };
	int level_max{ 100 };

	bool is_reload{ false };

	bool is_delete{ false };
	int ID{ 0 };//

	bool right_mouse_pressed{ false };
	bool middle_mouse_pressed{ false };
	bool left_mouse_pressed{ false };
	double right_stamp{ 0.0 };
	double prev_scale{ 1.0 };
	bool is2D{ false };
	bool isBF{ false };
};

QOiv2DRenderWindow::QOiv2DRenderWindow(QWidget* parent) :
	QOivRenderWindow(parent, true), d{ new Impl } {
	setDefaultWindowType();

	d->levelWindow = new TC::SpinBoxAction("RI Range");
	connect(d->levelWindow->spinBox(), SIGNAL(lowerValueChanged(int)), this, SLOT(lowerLevelChanged(int)));
	connect(d->levelWindow->spinBox(), SIGNAL(upperValueChanged(int)), this, SLOT(upperLevelChanged(int)));
	connect(d->levelWindow, SIGNAL(sigMinChanged(double)), this, SLOT(lowerSpinChanged(double)));
	connect(d->levelWindow, SIGNAL(sigMaxChanged(double)), this, SLOT(upperSpinChanged(double)));
}

QOiv2DRenderWindow::~QOiv2DRenderWindow() {

}

auto QOiv2DRenderWindow::resetOnLoad() -> void {
	d->is_scalarBar = false;
	d->colorMap = 0;
	changeScalarBarLabel(false);
}

auto QOiv2DRenderWindow::setType(bool is2D) -> void {
	d->is2D = is2D;
}

auto QOiv2DRenderWindow::showContextMenu(SbVec2f pos) -> void {
	//y position is inverted
	//auto new_y = getViewerExaminer()->getRenderArea()->getSize()[1] - pos[1];
	auto new_y = getQtRenderArea()->size().height() - pos[1];
	QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

	QMenu myMenu;

	QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
	QAction* cmOptions[5];
	cmOptions[0] = cmMenu->addAction("Grayscale");
	cmOptions[1] = cmMenu->addAction("Inverse grayscale");
	cmOptions[2] = cmMenu->addAction("Hot iron");
	cmOptions[3] = cmMenu->addAction("JET");
	cmOptions[4] = cmMenu->addAction("Rainbow");

	for (int i = 0; i < 5; i++) {
		cmOptions[i]->setCheckable(true);
	}
	cmOptions[d->colorMap]->setChecked(true);

	//Dynamic addition of color scalarbar
	QMenu* sbMenu = myMenu.addMenu(tr("ScalarBar"));
	auto toggleSB = sbMenu->addAction("Show ScalarBar");
	toggleSB->setCheckable(true);
	toggleSB->setChecked(d->is_scalarBar);

	if (d->is_reload) {
		initRangeSlider();
		d->is_reload = false;
	}

	QMenu* levelMenu = myMenu.addMenu(tr("Level Window"));
	levelMenu->addAction(d->levelWindow);

	myMenu.addAction("Reset View");

	QAction* selectedItem = myMenu.exec(globalPos);

	if (selectedItem) {
		auto text = selectedItem->text();
		if (text.contains("Inverse")) {
			change2DColorMap(1);
		}
		else if (text.contains("Grayscale")) {
			change2DColorMap(0);
		}
		else if (text.contains("Hot")) {
			change2DColorMap(2);
		}
		else if (text.contains("JET")) {
			change2DColorMap(3);
		}
		else if (text.contains("Rainbow")) {
			change2DColorMap(4);
		}
		else if (text.contains("Reset View")) {
			reset2DView(true);
		}
		else if (text.contains("Show ScalarBar")) {
			toggleScalarBar();
		}
	}
}

auto QOiv2DRenderWindow::toggleScalarBar() -> void {
	auto swname = "ScalarBarSwitch" + std::to_string(d->ID);
	auto scalarSwitch = MedicalHelper::find<SoSwitch>(getSceneGraph(), swname);

	if (scalarSwitch) {
		if (d->is_scalarBar) {
			scalarSwitch->whichChild = -1;
			d->is_scalarBar = false;
		}
		else {
			scalarSwitch->whichChild = 0;
			d->is_scalarBar = true;
		}
	}
}
auto QOiv2DRenderWindow::ScalarBarScale(bool activate) -> void {
	auto sbname = "ScalarBar" + std::to_string(d->ID);
	auto scalarSep = MedicalHelper::find<SoSeparator>(getSceneGraph(), sbname);
	if (scalarSep) {
		if (d->is_scalarBar) {
			if (d->is_scalarScale || !activate) {
				auto maniname = "Manip2D_" + std::to_string(d->ID);
				auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);
				if (manisw) {
					manisw->replaceChild(0, new SoSeparator);
					d->is_scalarScale = false;
				}
			}
			else {
				//auto new_scale = new SoScale2Dragger;
				auto new_scale = new SoScale2UniformDragger;
				new_scale->setName("tempScaler");
				new_scale->setMinScale(0.5);
				auto trans = static_cast<SoTranslation*>(scalarSep->getChild(0));
				auto scale = static_cast<SoScale*>(scalarSep->getChild(1));

				auto prev_scale = scale->scaleFactor.getValue();
				if (d->ID == 0) {
					new_scale->scaleFactor.setValue(prev_scale[0], prev_scale[1], 1);
				}
				else if (d->ID == 1) {
					new_scale->scaleFactor.setValue(prev_scale[2], prev_scale[1], 1);
				}
				else if (d->ID == 2) {
					new_scale->scaleFactor.setValue(prev_scale[0], prev_scale[2], 1);
				}

				auto vp = getViewportRegion();
				SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
				action->apply(scalarSep->getChild(2));

				auto bbox = action->getBoundingBox();

				auto fs = new SoFaceSet;
				auto vertices = new SoVertexProperty;
				fs->vertexProperty = vertices;
				switch (d->ID) {
				case 0:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));
					break;
				case 1:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMax()[1] + 0.05, -1));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMin()[1] - 0.05, -1));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMin()[1] - 0.05, -1));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMax()[1] + 0.05, -1));
					break;
				case 2:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[2] + 0.05, -1));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[2] - 0.05, -1));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[2] - 0.05, -1));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[2] + 0.05, -1));
					break;
				}

				auto handleSep = new SoSeparator;

				auto handleMatl = new SoMaterial;
				handleMatl->diffuseColor.setValue(0, 1, 0);
				handleMatl->transparency.setValue(0.5);

				handleSep->addChild(handleMatl);
				handleSep->addChild(fs);

				new_scale->setPart("scalerActive", handleSep);
				new_scale->setPart("scaler", handleSep);
				new_scale->setPart("feedback", new SoSeparator);
				new_scale->setPart("feedbackActive", new SoSeparator);

				auto maniname = "Manip2D_" + std::to_string(d->ID);
				auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

				auto transScale = new SoTranslation;
				auto prev_trans = trans->translation.getValue();
				transScale->translation.setValue(trans->translation.getValue());

				auto hanSep = new SoSeparator;
				hanSep->addChild(transScale);
				hanSep->addChild(new_scale);


				if (manisw) {
					auto newSep = new SoSeparator;
					auto itrans = new SoTranslation;
					auto ttrans = new SoTranslation;

					itrans->translation.setValue(-prev_trans[0], -prev_trans[1], -prev_trans[2]);
					ttrans->translation.setValue(prev_trans[0], prev_trans[1], prev_trans[2]);
					auto rot = new SoRotation;

					auto decomposer = new SoDecomposeVec3f;
					decomposer->vector.connectFrom(&new_scale->scaleFactor);
					auto composer = new SoComposeVec3f;

					newSep->addChild(ttrans);
					newSep->addChild(rot);
					newSep->addChild(itrans);
					newSep->addChild(hanSep);
					manisw->replaceChild(0, newSep);

					if (d->ID == 0) {
						composer->x.connectFrom(&decomposer->x);
						composer->y.connectFrom(&decomposer->y);
						composer->z.connectFrom(&decomposer->z);//fixed value
					}
					else if (d->ID == 1) {
						rot->rotation.setValue(SbVec3f(0, 1, 0), 1.57f);
						composer->x.connectFrom(&decomposer->z);//fixed value
						composer->y.connectFrom(&decomposer->y);
						composer->z.connectFrom(&decomposer->x);
					}
					else {
						rot->rotation.setValue(SbVec3f(1, 0, 0), 1.57f);
						composer->x.connectFrom(&decomposer->x);
						composer->y.connectFrom(&decomposer->z);//fixed value
						composer->z.connectFrom(&decomposer->y);
					}

					scale->scaleFactor.connectFrom(&composer->vector);
					//attach scalar manipulator
					d->is_scalarScale = true;
					//setInteractionMode(false);                        
				}
			}
		}
	}
}

auto QOiv2DRenderWindow::ScalarBarMove(bool activate) -> void {
	auto swname = "ScalarBar" + std::to_string(d->ID);
	auto scalarSep = MedicalHelper::find<SoSeparator>(getSceneGraph(), swname);

	if (scalarSep) {
		if (d->is_scalarBar) {
			if (d->is_scalarTrans || !activate) {
				//detach existing manipulator
				auto maniname = "Manip2D_" + std::to_string(d->ID);
				auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

				if (manisw) {
					manisw->replaceChild(0, new SoSeparator);
					d->is_scalarTrans = false;
				}
			}
			else {
				auto new_trans = new SoTranslate2Dragger;
				new_trans->setName("tempDragger");

				auto trans = static_cast<SoTranslation*>(scalarSep->getChild(0));
				auto scale = static_cast<SoScale*>(scalarSep->getChild(1));

				auto prev_trans = trans->translation.getValue();

				new_trans->translation.setValue(prev_trans);

				auto vp = getViewportRegion();
				SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
				action->apply(scalarSep->getChild(2));
				auto bbox = action->getBoundingBox();

				auto fs = new SoFaceSet;
				SoVertexProperty* vertices = new SoVertexProperty;
				fs->vertexProperty = vertices;
				switch (d->ID) {
				case 0:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));
					break;
				case 1:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMax()[1] + 0.05, -1));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMin()[1] - 0.05, -1));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMin()[1] - 0.05, -1));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMax()[1] + 0.05, -1));
					break;
				case 2:
					vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[2] + 0.05, -1));
					vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[2] - 0.05, -1));
					vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[2] - 0.05, -1));
					vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[2] + 0.05, -1));
					break;
				}

				auto handleSep = new SoSeparator;

				auto handleMatl = new SoMaterial;

				auto transScale = new SoScale;

				auto val = scale->scaleFactor.getValue();

				switch (d->ID) {
				case 0:
					transScale->scaleFactor.setValue(scale->scaleFactor.getValue());
					break;
				case 1:
					transScale->scaleFactor.setValue(val[2], val[1], 1.0);
					break;
				case 2:
					transScale->scaleFactor.setValue(val[0], val[2], 1.0);
					break;
				}
				handleMatl->diffuseColor.setValue(1.0, 0.0, 0.0);
				handleMatl->transparency.setValue(0.5);

				handleSep->addChild(handleMatl);
				handleSep->addChild(transScale);
				handleSep->addChild(fs);


				new_trans->setPart("translatorActive", handleSep);
				new_trans->setPart("translator", handleSep);
				new_trans->setPart("xAxisFeedback", new SoSeparator);
				new_trans->setPart("yAxisFeedback", new SoSeparator);

				auto maniname = "Manip2D_" + std::to_string(d->ID);
				auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

				if (manisw) {
					auto newSep = new SoSeparator;
					auto itrans = new SoTranslation;
					auto ttrans = new SoTranslation;

					itrans->translation.setValue(-prev_trans[0], -prev_trans[1], -prev_trans[2]);
					ttrans->translation.setValue(prev_trans[0], prev_trans[1], prev_trans[2]);
					auto rot = new SoRotation;

					newSep->addChild(ttrans);
					newSep->addChild(rot);
					newSep->addChild(itrans);
					newSep->addChild(new_trans);
					manisw->replaceChild(0, newSep);

					auto decomposer = new SoDecomposeVec3f;
					decomposer->vector.connectFrom(&new_trans->translation);
					auto composer = new SoComposeVec3f;
					if (d->ID == 0) {
						composer->x.connectFrom(&decomposer->x);
						composer->y.connectFrom(&decomposer->y);
						composer->z.connectFrom(&decomposer->z);
					}
					else if (d->ID == 1) {
						rot->rotation.setValue(SbVec3f(0, 1, 0), 1.57f);
						auto calc = new SoCalculator;
						calc->expression = "oc = -a + b";
						calc->a.connectFrom(&decomposer->x);
						calc->b.setValue(prev_trans[0] + prev_trans[2]);

						composer->x.setValue(prev_trans[0]);
						composer->y.connectFrom(&decomposer->y);
						composer->z.connectFrom(&calc->oc);
					}
					else if (d->ID == 2) {
						rot->rotation.setValue(SbVec3f(1, 0, 0), 1.57f);
						auto calc = new SoCalculator;
						calc->expression = "oc = a + b";
						calc->a.connectFrom(&decomposer->y);
						calc->b.setValue(prev_trans[1] + prev_trans[2]);

						composer->x.connectFrom(&decomposer->x);
						composer->y.setValue(prev_trans[1]);
						composer->z.connectFrom(&calc->oc);
					}

					trans->translation.connectFrom(&composer->vector);
					//attach scalar manipulator
					d->is_scalarTrans = true;
					//setInteractionMode(false);
				}

			}
		}
	}
}

auto QOiv2DRenderWindow::setRenderWindowID(int idx) -> void {
	d->ID = idx;
}
auto QOiv2DRenderWindow::resetViewBF(bool isBF) -> bool {
	auto changed = false;
	if (d->isBF != isBF) {
		changed = true;
	}
	d->isBF = isBF;
	return changed;
}

auto QOiv2DRenderWindow::reset2DView(bool fromFunc) -> void {
	//find which axis
	MedicalHelper::Axis ax;
	auto root = getSceneGraph();

	SoVolumeData* volData{ nullptr };// = MedicalHelper::find<SoVolumeData>(root);//find any volume Data

	if (d->isBF) {
		volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
	}
	else {
		//find volume data except BF
		auto nodes = MedicalHelper::findNodes<SoVolumeData>(root);
		for (auto n : nodes) {
			QString nname(n->getName().getString());
			if (nname.contains("BF_CH")) {
				continue;
			}
			volData = n;
			break;
		}
	}

	if (volData) {
		float slack;
		auto dims = volData->getDimension();
		auto spacing = volData->getVoxelSize();

		float x_len = dims[0] * spacing[0];
		float y_len = dims[1] * spacing[1];
		float z_len = dims[2] * spacing[2];
		auto wh = static_cast<float>(size().height());
		auto ww = static_cast<float>(size().width());
		//auto windowSlack = wh > ww ? ww / wh : wh / ww;
		auto windowSlack = wh / ww;
		if (d->ID == 0) {
			ax = MedicalHelper::Axis::AXIAL;
			slack = y_len / x_len;
		}
		else if (d->ID == 1) {
			ax = MedicalHelper::SAGITTAL;
			slack = z_len / y_len;
		}
		else if (d->ID == 2) {
			ax = MedicalHelper::CORONAL;
			slack = z_len / x_len;
		}
		else {
			return;
		}
		if (windowSlack > slack && windowSlack > 0) {
			slack = windowSlack > 1 ? 1.0 : windowSlack;
		}
		MedicalHelper::orientView(ax, getCamera(), volData, slack);
		if (fromFunc) {
			//reset range
			auto range_min = MedicalHelper::find<SoInfo>(root, "HTMin");
			if (range_min) {
				auto min_val = range_min->string.getValue().toInt();
				auto range_max = MedicalHelper::find<SoInfo>(root, "HTMax");
				auto max_val = range_max->string.getValue().toInt();

				d->levelWindow->setLower(min_val);
				d->levelWindow->setUpper(max_val);
			}
			//reset colormap
			change2DColorMap(0);
		}
	}
}

void QOiv2DRenderWindow::lowerSpinChanged(double val) {
	//d->level_min = static_cast<int>(val * 10000.0);
	auto int_val = static_cast<int>(val * 10000.0);
	float min_val = val * 10000.0;
	float max_val = static_cast<float>(d->level_max);
	auto rootScene = getSceneGraph();
	if (rootScene) {
		auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
		if (range) {
			range->min = int_val;// d->level_min;
			max_val = range->max.getValue();
		}
		auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
		if (mipRange) {
			mipRange->min = int_val;// d->level_min;
			max_val = mipRange->max.getValue();
		}
		emit sigMinMax(min_val, max_val);
	}
}

void QOiv2DRenderWindow::lowerLevelChanged(int val) {
	d->levelWindow->setLower(val, false);
	//d->level_min = val;

	float min_val = static_cast<float>(val);
	float max_val = static_cast<float>(d->level_max);

	auto rootScene = getSceneGraph();
	if (rootScene) {
		auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
		if (range) {
			range->min = val;// d->level_min;
			max_val = range->max.getValue();
		}
		auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
		if (mipRange) {
			mipRange->min = val;// d->level_min;
			max_val = mipRange->max.getValue();
		}
		emit sigMinMax(min_val, max_val);
	}
}
void QOiv2DRenderWindow::upperSpinChanged(double val) {
	//d->level_max = static_cast<int>(val * 10000.0);
	auto int_val = static_cast<int>(val * 10000.0);
	float max_val = val * 10000.0;
	float min_val = static_cast<float>(d->level_min);
	auto rootScene = getSceneGraph();
	if (rootScene) {
		auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
		if (range) {
			range->max = int_val;// d->level_max;
			min_val = range->min.getValue();
		}
		auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
		if (mipRange) {
			mipRange->max = int_val;// d->level_max;
			min_val = mipRange->min.getValue();
		}
		emit sigMinMax(min_val, max_val);
	}
}
void QOiv2DRenderWindow::upperLevelChanged(int val) {
	d->levelWindow->setUpper(val, false);
	//d->level_max = val;
	float max_val = static_cast<float>(val);
	float min_val = static_cast<float>(d->level_min);
	auto rootScene = getSceneGraph();
	if (rootScene) {
		auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
		if (range) {
			range->max = val;// d->level_max;
			min_val = range->min.getValue();
		}
		auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
		if (mipRange) {
			mipRange->max = val;// d->level_max;
			min_val = mipRange->min.getValue();
		}
		emit sigMinMax(min_val, max_val);
	}
}

auto QOiv2DRenderWindow::change2DColorMap(int idx) -> void {
	auto rootScene = getSceneGraph();
	if (rootScene) {
		auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colormap2D_TF");
		if (tf) {
			auto isInverse = false;
			int pred = 0;
			switch (idx) {
			case 0: // grayscale
				pred = SoTransferFunction::PredefColorMap::INTENSITY;
				break;
			case 1: // inversegrayscale
				pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
				isInverse = true;
				break;
			case 2: // Hot Iron - GLOW in OIV
				pred = SoTransferFunction::PredefColorMap::GLOW;
				break;
			case 3: // JET - PHYSICS in OIV
				pred = SoTransferFunction::PredefColorMap::PHYSICS;
				break;
			case 4: // Rainbow - STANDARD in OIV
				pred = SoTransferFunction::PredefColorMap::STANDARD;
				break;
			}
			d->colorMap = idx;
			tf->predefColorMap = pred;

			//change scalar bar text color at inverse grayscale
			changeScalarBarLabel(isInverse);
			emit sigColorMap(d->colorMap);
		}
	}
}
auto QOiv2DRenderWindow::setColorMapIdx(int idx) -> void {
	d->colorMap = idx;
	changeScalarBarLabel(d->colorMap == 1);
}
auto QOiv2DRenderWindow::changeScalarBarLabel(bool isInverse) -> void {
	auto swname = "ScalarBarSwitch" + std::to_string(d->ID);
	auto scalarSwitch = MedicalHelper::find<SoSwitch>(getSceneGraph(), swname);
	if (scalarSwitch) {
		auto baseColor = MedicalHelper::find<SoBaseColor>(scalarSwitch);
		if (baseColor) {
			if (isInverse) {
				baseColor->rgb.setValue(0, 0, 0);
			}
			else {
				baseColor->rgb.setValue(1, 1, 1);
			}
		}
	}
}


auto QOiv2DRenderWindow::initRangeSlider() -> void {
	auto newScene = getSceneGraph();
	if (newScene) {
		auto cur_time = MedicalHelper::find<SoInfo>(newScene, "HT_CUR_TIME");
		auto int_cur_time = cur_time->string.getValue().toInt();
		auto buf_idx = int_cur_time % 2;//buffer index is 2 for now
		auto volName = "volData" + std::to_string(buf_idx);


		auto volData = MedicalHelper::find<SoVolumeData>(newScene, volName);
		if (volData) {
			d->levelWindow->setMinMax(d->level_min, d->level_max);
			d->levelWindow->setLower(d->level_min);
			d->levelWindow->setUpper(d->level_max);
		}
	}
}
auto QOiv2DRenderWindow::refreshRangeSlider() -> void {
	d->is_reload = true;
}

auto QOiv2DRenderWindow::setHTRange(double min, double max) -> void {
	d->level_min = min;
	d->level_max = max;
}

auto QOiv2DRenderWindow::MouseMoveEvent(SoEventCallback* node) -> void {
	const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
	if (isNavigation()) {
		auto pos = moveEvent->getPositionFloat();
		auto viewport_size = getViewportRegion().getViewportSizePixels();
		auto root = getSceneGraph();
		auto color = MedicalHelper::find<SoImage>(root, "icon_file");
		if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
			auto image = new QImage(":/img/ic-setting-s.svg");
			image->convertTo(QImage::Format_RGBA8888);
			color->image.setValue(SbVec2s(16, 16), 4, image->bits());
			delete image;
		}
		else {
			auto image = new QImage(":/img/ic-setting.svg");
			image->convertTo(QImage::Format_RGBA8888);
			color->image.setValue(SbVec2s(16, 16), 4, image->bits());
			delete image;
		}
		if (d->right_mouse_pressed) {
			auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
			auto factor = 1.0 - diff * 1.5;
			if (factor > 0) {
				getCamera()->scaleHeight(factor / d->prev_scale);
				d->prev_scale = factor;
			}
		}
		if (d->left_mouse_pressed) {
			if (!d->is2D) {
				auto coord = CalcVolumeCoord(moveEvent->getNormalizedPosition(getViewportRegion()));
				emit sig2dCoord(coord[0], coord[1], coord[2]);
			}
			node->setHandled();
		}
		if (d->middle_mouse_pressed) {
			if (!d->is2D) {
				panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
			}
		}
	}
}
auto QOiv2DRenderWindow::MouseWheelEvent(SoEventCallback* node) -> void {
	const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
	auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
	emit sigWheel(delta);
}

auto QOiv2DRenderWindow::MouseButtonEvent(SoEventCallback* node) -> void {
	const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();

	if (!isNavigation()) {//selection mode

	}
	else {//navigation mode
		auto pos = mouseButton->getPositionFloat();
		auto vr = getViewportRegion();
		auto normpos = mouseButton->getNormalizedPosition(vr);
		auto viewport_size = vr.getViewportSizePixels();
		auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
		auto mouse_in_scalar = false;
		if (d->is_scalarBar) {
			auto swname = "ScalarBar" + std::to_string(d->ID);
			auto scalarSep = MedicalHelper::find<SoSeparator>(getSceneGraph(), swname);

			if (scalarSep) {
				SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vr);
				action->apply(scalarSep);

				auto bbox = action->getBoundingBox();

				SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
				SbVec2f normPoint = normpos;

				auto root = getSceneGraph();

				rayPick.setNormalizedPoint(normPoint);
				rayPick.apply(root);
				SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
				if (pickedPoint) {
					auto pp = pickedPoint->getPoint();
					if (d->ID == 0) {
						mouse_in_scalar = (pp[0] > bbox.getMin()[0] && pp[0] < bbox.getMax()[0] && pp[1] > bbox.getMin()[1] && pp[1] < bbox.getMax()[1]);
					}
					else if (d->ID == 1) {
						mouse_in_scalar = (pp[2] > bbox.getMin()[2] && pp[2] < bbox.getMax()[2] && pp[1] > bbox.getMin()[1] && pp[1] < bbox.getMax()[1]);
					}
					else if (d->ID == 2) {
						mouse_in_scalar = (pp[2] > bbox.getMin()[2] && pp[2] < bbox.getMax()[2] && pp[0] > bbox.getMin()[0] && pp[0] < bbox.getMax()[0]);
					}
				}
			}
		}

		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			if (!mouse_in_setting && !mouse_in_scalar) {
				d->right_mouse_pressed = true;
				d->right_stamp = normpos[1];
				d->prev_scale = 1.0;
			}
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			if (mouse_in_scalar) {
				ScalarBarScale(true);
			}
			else {
				d->right_mouse_pressed = false;
				d->right_stamp = 0.0;
				d->prev_scale = 1.0;
			}
		}
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
			if (!mouse_in_scalar) {
				d->middle_mouse_pressed = true;
				startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
			}
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
			if (mouse_in_scalar) {
				ScalarBarMove(true);
			}
			d->middle_mouse_pressed = false;
		}
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (!mouse_in_setting && !mouse_in_scalar) {
				d->left_mouse_pressed = true;
				if (!d->is2D) {
					auto coord = CalcVolumeCoord(normpos);
					emit sig2dCoord(coord[0], coord[1], coord[2]);
				}
			}
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			if (mouse_in_setting) {
				showContextMenu(pos);
			}
			d->left_mouse_pressed = false;
		}
	}
}

auto QOiv2DRenderWindow::CalcVolumeCoord(SbVec2f norm_point) -> SbVec3f {
	SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
	SbVec2f normPoint = norm_point;

	auto root = getSceneGraph();

	rayPick.setNormalizedPoint(normPoint);
	rayPick.apply(root);

	SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
	if (pickedPoint) {
		SoVolumeData* volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
		if (volData) {
			auto pt = pickedPoint->getPoint();
			auto coord = volData->XYZToVoxel(pt);
			return coord;
		}
	}
	return SbVec3f();
}


auto QOiv2DRenderWindow::KeyboardEvent(SoEventCallback* node) -> void {
	const SoEvent* keyEvent = node->getEvent();

	if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
		//seek mode switching is handled by native viewer event callback                    
		node->setHandled();
	}
	if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
		if (!isNavigation()) {
			setInteractionMode(true);
			setAltPressed(true);
		}
		node->setHandled();
	}
	if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
		if (isAltPressed()) {
			setInteractionMode(false);
			setAltPressed(false);
		}
		node->setHandled();
	}
	if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ENTER)) {
		if (d->is_scalarTrans) {
			ScalarBarMove(false);
		}
		if (d->is_scalarScale) {
			ScalarBarScale(false);
		}
	}
	if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {

	}
	if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::F12)) {
		SoNode* root = getSceneGraph();
		if (root != NULL) {
			SoIvTune::start(root);
		}
	}
}

auto QOiv2DRenderWindow::closeEvent(QCloseEvent* unused) -> void {
	Q_UNUSED(unused)
}

auto QOiv2DRenderWindow::setDefaultWindowType() -> void {
	//set default gradient background color
	SbVec3f start_color = { 0.0,0.0,0.0 };
	SbVec3f end_color = { 0.0,0.0,0.0 };
	setGradientBackground(start_color, end_color);

	//set default camera type
	setCameraType(true);//orthographic
}
