// RGB fragment shader

//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
	vec4 clr1,clr2,clr3;
	if(isCh0Exist > 0)
	{
		VVIZ_DATATYPE data_value1 = VVizGetData( data1, texCoord );
		clr1 = VVizTransferFunction(data_value1, 2);
	}
	else
	{
		clr1 = vec4(0,0,0,0);
	}
	if(isCh1Exist > 0)
	{
		VVIZ_DATATYPE data_value2 = VVizGetData( data2, texCoord );
		clr2 = VVizTransferFunction(data_value2, 3);
	}
	else
	{
		clr2 = vec4(0,0,0,0);
	}
	if(isCh2Exist > 0)
	{
		VVIZ_DATATYPE data_value3 = VVizGetData( data3, texCoord );
		clr3 = VVizTransferFunction(data_value3, 4);
	}
	else
	{
		clr3 = vec4(0,0,0,0);
	}
	
	float w = max(clr1.w,max(clr2.w,clr3.w));
	float r = max(clr1.r,max(clr2.r,clr3.r));
	float g = max(clr1.g,max(clr2.g,clr3.g));
	float b = max(clr1.b,max(clr2.b,clr3.b));

	vec4 color = vec4(r,g,b,w);

	return color;	
}
