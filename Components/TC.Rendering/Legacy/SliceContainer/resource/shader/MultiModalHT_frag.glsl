//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;
uniform VVizDataSetId data4;

uniform int isHTExist;

// Implement VVizComputeFragmentColor for slice nodes
vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{    
    vec4 intensityHT = vec4(0, 0, 0, 0);
    if (isHTExist > 0) {
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);
        intensityHT = VVizTransferFunction(index1, 1);
    }
        
    return intensityHT;
}