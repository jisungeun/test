//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VVizPipeline/FRAGMENT_GRADIENT_FUNCTION/vvizGradient_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;

// Implement VVizComputeFragmentColor for slice nodes
vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{    
    VVIZ_DATATYPE val1 = 0.0;
    VVIZ_DATATYPE val2 = 0.0;
    vec4 clr1 = vec4(0.0);
    vec4 clr2 = vec4(0.0);    

    VVizDataSetId dataset = VVizGetDefaultDataSet();
    
    
    vec3 dataCoord1 = VVizTextureToTextureVec(dataset, data1, texCoord);
    if (false == VVizIsOutsideVolume(dataCoord1)) {
        val1 = VVizGetData(data1, dataCoord1);
        clr1 = VVizTransferFunction(val1, 1);
    }

    vec3 dataCoord2 = VVizTextureToTextureVec(dataset, data2, texCoord);
    if (false == VVizIsOutsideVolume(dataCoord2)) {
        val2 = VVizGetData(data2, dataCoord2);
        clr2 = VVizTransferFunction(val2, 2);
    }    
    
    vec4 res = vec4(0.0);    

    res.r = mix(clr.r, res.r, res.r);
    res.g = mix(clr.g, res.g, res.g);
    res.b = mix(clr.b, res.b, res.b);    

    res.a = max(clr1.a, clr2.a);

    return res;
}

//Implement VVizComputeFragmentColor for volume node

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
	vec3 texCoord = voxelInfoFront.texCoord;	
	VVIZ_DATATYPE val1 = 0;
	VVIZ_DATATYPE val2 = 0;	
	vec4 clr1 = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 clr2 = vec4(0.0, 0.0, 0.0, 0.0);		
	
	vec3 dataCoord1 = VVizTextureToTextureVec(data, data1, texCoord);
	val1 = VVizGetData(data1, dataCoord1);
	clr1 = VVizComputeVolumeRendering(val1, 1);
	clr1 = mix(clr1, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord1)));
	
	
	vec3 dataCoord2 = VVizTextureToTextureVec(data, data2, texCoord);
	val2 = VVizGetData(data2, dataCoord2);
	clr2 = VVizComputeVolumeRendering(val2, 3);
	clr2 = mix(clr2, vec4(0.0), bvec4(VVizIsOutsideVolume(dataCoord2)));
	
	// Combine color values but use highest alpha value  
    vec4 res = vec4(0.0);

    res.rgb = clr1.rgb + clr2.rgb;

    res.a = max(clr1.a, clr2.a);

    return res;
}