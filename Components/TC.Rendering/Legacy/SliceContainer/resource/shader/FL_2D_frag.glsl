// RGB fragment shader

//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform VVizDataSetId data2;
uniform VVizDataSetId data3;

uniform int isCh0Exist;
uniform int isCh1Exist;
uniform int isCh2Exist;

vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
	vec4 clr1,clr2,clr3;
	if(isCh0Exist > 0)
	{
		VVIZ_DATATYPE data_value1 = VVizGetData( data1, texCoord );
		clr1 = VVizTransferFunction(data_value1, 1);
	}
	else
	{
		clr1 = vec4(0,0,0,0);
	}
	if(isCh1Exist > 0)
	{
		VVIZ_DATATYPE data_value2 = VVizGetData( data2, texCoord );
		clr2 = VVizTransferFunction(data_value2, 2);
	}
	else
	{
		clr2 = vec4(0,0,0,0);
	}
	if(isCh2Exist > 0)
	{
		VVIZ_DATATYPE data_value3 = VVizGetData( data3, texCoord );
		clr3 = VVizTransferFunction(data_value3, 3);
	}
	else
	{
		clr3 = vec4(0,0,0,0);
	}
	
	float w = max(clr1.w,max(clr2.w,clr3.w));
	
	vec4 color = vec4(clr3.r ,clr2.g ,clr1.b , w);

	return color;	
}
