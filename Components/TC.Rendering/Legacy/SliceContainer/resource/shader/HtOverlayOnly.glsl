//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>
//!oiv_include <VVizPipeline/FRAGMENT_GRADIENT_FUNCTION/vvizGradient_frag.h>
//!oiv_include <VolumeViz/vvizNpr_frag.h>

uniform sampler2D tex2D;

uniform VVizDataSetId data1;

uniform int isOverlay;
uniform float htOpacity;

uniform float resX;
uniform float resY;
uniform float resZ;

void GetCurrentDuDvDw(in VVizDataSetId dataset, in vec4 tileInfo, in vec3 tcoordVirt, out vec3 du, out vec3 dv, out vec3 dw)
{
    // Get resolution of current tile (not necessarily full resolution)

    float res = tileInfo.w;
    vec3 duData = vec3(res, 0, 0);
    vec3 dvData = vec3(0, res, 0);
    vec3 dwData = vec3(0, 0, res);

    // Get normalized voxel size and adjust for tile resolution
    vec3 voxelDims = VVizGetVoxelDimensions(dataset);
    du = voxelDims * duData;
    dv = voxelDims * dvData;
    dw = voxelDims * dwData;
}
vec3 ComputeCentralDiffGradient(VVizDataSetId tex, vec3 tcoord)
{
    // Get uvw offsets (essentially voxel size in normalized coordinates)
    vec3 scaledDu;
    vec3 scaledDv;
    vec3 scaledDw;
    vec4 tileInfo = VVizGetTileInfo(tex, tcoord);
    GetCurrentDuDvDw(tex, tileInfo, tcoord, scaledDu, scaledDv, scaledDw);

    // Compute central difference		

    vec3 G;
    G.x = VVizGetData(tex, tcoord + scaledDu) - VVizGetData(tex, tcoord - scaledDu);
    G.y = VVizGetData(tex, tcoord + scaledDv) - VVizGetData(tex, tcoord - scaledDv);
    G.z = VVizGetData(tex, tcoord + scaledDw) - VVizGetData(tex, tcoord - scaledDw);

    float avg_spacing = (resX + resY + resZ) / 3.0;

    G.x /= (resX * 2 / avg_spacing);
    G.y /= (resY * 2 / avg_spacing);
    G.z /= (resZ * 2 / avg_spacing);

    G *= 1. / sqrt(3.);

    return G;
}


// Implement VVizComputeFragmentColor for slice nodes
vec4 VVizComputeFragmentColor(VVIZ_DATATYPE vox, vec3 texCoord)
{
    vec4 clrHT = vec4(0.0);

    if (isOverlay > 0) {                
        VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);
        vec3 gradient = ComputeCentralDiffGradient(data1, texCoord);
        float gradientValue = length(gradient);
        clrHT = texture2D(tex2D, vec2(index1, gradientValue));        
    }

    return clrHT;    
}