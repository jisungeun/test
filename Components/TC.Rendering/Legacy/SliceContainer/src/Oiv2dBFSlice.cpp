#include "Oiv2dBFSlice.h"

#include <QString>
#include <QApplication>

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoSwitch.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>

struct Oiv2dBFSlice::Impl {
	//shared
	SoRef<SoSeparator> renderRoot { nullptr };

	SoRef<SoMaterial> bfMatl { nullptr };
	SoRef<SoSwitch> timeStepper { nullptr };
	SoRef<SoFragmentShader> fragment_shader { nullptr };
	SoRef<SoVolumeShader> shader { nullptr };
	SoRef<SoDataRange> range[3] { nullptr, nullptr, nullptr };

	//Buffered
	SoRef<SoMultiDataSeparator> mds { nullptr };
	SoRef<SoVolumeData> volumeR { nullptr };
	SoRef<SoVolumeData> volumeG { nullptr };
	SoRef<SoVolumeData> volumeB { nullptr };
	SoRef<SoOrthoSlice> render { nullptr };

	SoRef<SoTranslation> bfTrans { nullptr };

	//variables
	int time_step { 0 };
	int time_steps { 0 };
	float transparency { 0.0 };
	bool exist { false };
};

Oiv2dBFSlice::Oiv2dBFSlice() : d { new Impl } {}

Oiv2dBFSlice::~Oiv2dBFSlice() { }

auto Oiv2dBFSlice::setTimeStep(int step) -> void {
	if (step < 0)
		return;
	if (nullptr == d->timeStepper)
		return;

	d->time_step = step;
	d->timeStepper->whichChild = 0;
}

auto Oiv2dBFSlice::getTimeStep() -> int {
	return d->time_step;
}

auto Oiv2dBFSlice::getTimeSteps() -> int {
	return d->time_steps;
}

auto Oiv2dBFSlice::setTransparency(float transp) -> void {
	if (d->bfMatl) {
		d->transparency = transp;
		d->bfMatl->transparency.setValue(transp);
	}
}


auto Oiv2dBFSlice::getTransparency() -> float {
	if (d->bfMatl) {
		return d->transparency;
	}
	return -1.0;
}

auto Oiv2dBFSlice::isVolumeExist() -> bool {
	return d->exist;
}

auto Oiv2dBFSlice::getRenderRootNode() -> SoSeparator* {
	return d->renderRoot.ptr();
}

auto Oiv2dBFSlice::buildColorTF() -> void {
	d->fragment_shader = new SoFragmentShader;
	const auto shaderPath = QString("%1/shader/BrightField2D_color_frag.glsl").arg(qApp->applicationDirPath());
	d->fragment_shader->sourceProgram.setValue(shaderPath.toStdString());

	d->fragment_shader->addShaderParameter<SoShaderParameter1i>("data1", 1);
	d->fragment_shader->addShaderParameter<SoShaderParameter1i>("data2", 2);
	d->fragment_shader->addShaderParameter<SoShaderParameter1i>("data3", 3);

	d->shader = new SoVolumeShader;
	d->shader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragment_shader.ptr());
	d->shader->forVolumeOnly = FALSE;

	for (int i = 0; i < 3; i++) {
		d->range[i] = new SoDataRange;
		d->range[i]->min.setValue(0.0);
		d->range[i]->max.setValue(255.0);
		d->range[i]->dataRangeId = i + 1;
	}
}

auto Oiv2dBFSlice::buildBFSlice(int steps) -> void {
	d->renderRoot = new SoSeparator;
	d->renderRoot->setName("BF_renderRootSep2D");

	d->bfTrans = new SoTranslation;
	d->bfTrans->translation.setValue(0, 0, -0.2f);

	d->renderRoot->addChild(d->bfTrans.ptr());

	d->time_step = 0;
	d->time_steps = steps;
	d->timeStepper = new SoSwitch;
	d->timeStepper->setName("BF_time_stepper");

	d->bfMatl = new SoMaterial;
	d->bfMatl->diffuseColor.setValue(1.0, 1.0, 1.0);

	d->mds = new SoMultiDataSeparator;

	d->render = new SoOrthoSlice;

	buildColorTF();

	d->mds->addChild(d->shader.ptr());

	d->volumeR = new SoVolumeData;
	d->volumeG = new SoVolumeData;
	d->volumeB = new SoVolumeData;

	//organize scenegraph    
	d->mds->addChild(d->volumeR.ptr());
	d->mds->addChild(d->volumeG.ptr());
	d->mds->addChild(d->volumeB.ptr());
	for (auto i = 0; i < 3; i++) {
		d->mds->addChild(d->range[i].ptr());
	}
	d->mds->addChild(d->render.ptr());
	d->timeStepper->addChild(d->mds.ptr());

	d->renderRoot->addChild(d->bfMatl.ptr());
	d->renderRoot->addChild(d->timeStepper.ptr());
}

auto Oiv2dBFSlice::setSingleBF(SoVolumeData* rVol, SoVolumeData* gVol, SoVolumeData* bVol, int step) -> void {
	//if (step < d->time_steps) {
	if (d->renderRoot) {
		d->mds->replaceChild(1, rVol);
		d->mds->replaceChild(2, gVol);
		d->mds->replaceChild(3, bVol);
		d->volumeR = rVol;
		d->volumeG = gVol;
		d->volumeB = bVol;
		if (!d->exist)
			d->exist = true;
		d->time_step = step;
	}
	//}
}

auto Oiv2dBFSlice::ClearData() -> void {
	if (d->renderRoot) {
		d->mds->replaceChild(1, new SoSeparator);
		d->mds->replaceChild(2, new SoSeparator);
		d->mds->replaceChild(3, new SoSeparator);
		d->volumeB = nullptr;
		d->volumeG = nullptr;
		d->volumeR = nullptr;
		d->exist = false;
		d->time_step = 0;
	}
}
