#include <QVector>
#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoTranslation.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <Volumeviz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/Helpers/MedicalHelper.h>
#pragma warning(pop)

#include "SoTransferFunction2D.h"

#include "OivSceneXZ.h"

namespace TC {
	struct OivSceneXZ::Impl {
		//Comomn
		SoRef<SoSeparator> renderRootSep { nullptr };
		SoRef<SoSeparator> sliceGroup { nullptr };

		//volume data		
		SoRef<SoVolumeData> htVol { nullptr };
		SoRef<SoVolumeData> flVol[3] { nullptr, nullptr, nullptr };

		//for HT Grayscale + TF 2D Overlay + FL Overlay
		SoRef<SoSeparator> flSep{ nullptr };
		SoRef<SoTranslation> flTrans{ nullptr };
		SoRef<SoSwitch> sliceFLSwitch { nullptr };
		SoRef<SoMaterial> sliceMatl { nullptr };
		SoRef<SoMultiDataSeparator> mds;
		//HT
		SoRef<SoSwitch> htStepper { nullptr };
		SoRef<SoDataRange> htRange { nullptr };
		SoRef<SoDataRange> htWholeRange { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };

		SoRef<SoSeparator> hiddenSep { nullptr };
		SoRef<SoRenderToTextureProperty> renderToTex { nullptr };
		SoRef<SoTransferFunction2D> tf2D = { nullptr };

		//FL
		SoRef<SoSwitch> flStepper[3] { nullptr, nullptr, nullptr };
		SoRef<SoTransferFunction> flTF[3] { nullptr, nullptr, nullptr };
		SoRef<SoDataRange> flRange[3] { nullptr, nullptr, nullptr };

		SoRef<SoFragmentShader> fragmentShaderFL { nullptr };
		SoRef<SoVolumeShader> shaderFL { nullptr };
		SoRef<SoOrthoSlice> sliceFL { nullptr };

		//for HT overlay
		SoRef<SoSwitch> htOverlaySwitch{ nullptr };
		SoRef<SoSeparator> overlayGroup{ nullptr };
		SoRef<SoTranslation> overlayTrans{ nullptr };
		SoRef<SoSeparator> htOverlaySep{ nullptr };
		SoRef<SoFragmentShader> fragmentShaderOverlay{ nullptr };
		SoRef<SoVolumeShader> htOverlayShader{ nullptr };
		SoRef<SoOrthoSlice> htOverlaySlice{ nullptr };

		//for HT
		SoRef<SoSwitch> htSwitch { nullptr };
		SoRef<SoMaterial> htMatl { nullptr };
		SoRef<SoSeparator> htSep { nullptr };
		SoRef<SoSwitch> ht_stepper { nullptr };
		SoRef<SoOrthoSlice> htSlice { nullptr };
		SoRef<SoVolumeShader> htQual { nullptr };
		//shared from volume container		

		float fl_transparency[3] { 0.3f, };
		bool volExist;
		bool blendColor;
		int ht_time_step;
		int fl_time_step[3];
		int colorMapIdx;
		int min, max;
		double grad_min, grad_max;
		int range;

		bool flExist[3] = { false, };
		bool flShow[3] = { false, };
		bool htShow { true };

		double gamma[3] { 1, 1, 1 };
		bool isGamma[3] { false, false, false };
		bool isOverlay { false };

		IO::TCFMetaReader::Meta::Pointer meta { nullptr };
		double htValueOffset { 0 };
		double htValueDiv { 1 };
		double flValueOffset[3] { 0, 0, 0 };

		SoTransferFunction::PredefColorMap color_map[5] = {
			SoTransferFunction::INTENSITY,
			SoTransferFunction::INTENSITY_REVERSED,
			SoTransferFunction::GLOW,
			SoTransferFunction::PHYSICS,
			SoTransferFunction::STANDARD
		};
		MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL, MedicalHelper::SAGITTAL, MedicalHelper::CORONAL };
		SbColor rgb[3] = { SbColor(0, 0, 1), SbColor(0, 1, 0), SbColor(1, 0, 0) };

		QString nonUniformShader;
		QString uniformShader;

		bool htRangeSet { false };
		bool flRangeSet { false };


		float fl_color[3][3] { { 0.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0 }, { 1.0, 0.0, 0.0 } };
	};

	OivSceneXZ::OivSceneXZ() : d { new Impl } {
		d->nonUniformShader = QString("%1/shader/FlOnly.glsl").arg(qApp->applicationDirPath());
		d->uniformShader = QString("%1/shader/FlOnly.glsl").arg(qApp->applicationDirPath());
	}

	OivSceneXZ::~OivSceneXZ() {
		d->renderRootSep->removeAllChildren();
		d->renderRootSep->unref();
	}

	auto OivSceneXZ::SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
		if (d->meta->data.data3D.scalarType.count() > 0) {
			if (d->meta->data.data3D.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data3D.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		} else if (d->meta->data.data2DMIP.scalarType.count() > 0) {
			if (d->meta->data.data2DMIP.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data2DMIP.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (d->meta->data.data3DFL.scalarType[i].count() > 0) {
				if (d->meta->data.data3DFL.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data3DFL.min[i];
				}
			} else if (d->meta->data.data2DFLMIP.scalarType[i].count() > 0) {
				if (d->meta->data.data2DFLMIP.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data2DFLMIP.min[i];
				}
			}
		}
	}

	auto OivSceneXZ::SetHiddenSep(SoSeparator* hidden) -> void {
		d->hiddenSep = hidden;
	}

	auto OivSceneXZ::SetFLGamma(int ch, bool enable, double gamma) -> void {
		d->gamma[ch] = gamma;
		d->isGamma[ch] = enable;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}

	auto OivSceneXZ::SetFLColor(int r, int g, int b, int ch) -> void {
		d->fl_color[ch][0] = static_cast<float>(r) / 255.0f;
		d->fl_color[ch][1] = static_cast<float>(g) / 255.0f;
		d->fl_color[ch][2] = static_cast<float>(b) / 255.0f;

		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}

	auto OivSceneXZ::SetHToriginal(bool original) -> void {
		if (original && d->htShow) {
			d->htMatl->transparency.setValue(0);
		} else {
			d->htMatl->transparency.setValue(1);
		}
	}

	auto OivSceneXZ::SetHTOverlay(bool overlay) -> void {
		d->isOverlay = overlay;
		for (auto i = 0; i < 3; i++) {
			if (overlay) {
				d->fragmentShaderOverlay->setShaderParameter1i("isOverlay", 1);
				d->htMatl->transparency.setValue(1);
			} else {
				d->fragmentShaderOverlay->setShaderParameter1i("isOverlay", 0);
				if (d->htShow) {
					d->htMatl->transparency.setValue(0);
				} else {
					d->htMatl->transparency.setValue(1);
				}
			}
		}
	}

	auto OivSceneXZ::SetUniform(bool isUniform) -> void {
		if (isUniform) {
			d->fragmentShaderFL->sourceProgram.setValue(d->uniformShader.toStdString());
		}
		else {
			d->fragmentShaderFL->sourceProgram.setValue(d->nonUniformShader.toStdString());
		}	
	}

	auto OivSceneXZ::buildHTSceneGraph() -> void {
		d->htSep = new SoSeparator;
		d->htSep->setName("HtOnlySep");
		d->htMatl = new SoMaterial;
		d->htMatl->ambientColor.setValue(1, 1, 1);
		d->htMatl->diffuseColor.setValue(1, 1, 1);
		d->ht_stepper = new SoSwitch;
		d->ht_stepper->whichChild = 0;
		d->ht_stepper->addChild(new SoSeparator);

		d->htSlice = new SoOrthoSlice;
		d->htSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->htSlice->setName("Slice_HT");
		d->htSlice->axis = d->ax[0];

		d->htQual = new SoVolumeRenderingQuality;
		d->htQual->forVolumeOnly = FALSE;

		d->htSep->addChild(d->htMatl.ptr());
		d->htSep->addChild(d->htRange.ptr());
		d->htSep->addChild(d->ht_stepper.ptr());
		d->htSep->addChild(d->transFunc.ptr());
		d->htSep->addChild(d->htQual.ptr());
		d->htSep->addChild(d->htSlice.ptr());

		d->htSwitch = new SoSwitch;
		d->htSwitch->whichChild = 0;
		d->htSwitch->addChild(d->htSep.ptr());

		d->sliceGroup->addChild(d->htSwitch.ptr());
	}


	//build scene graph
	auto OivSceneXZ::buildSceneGraph() -> void {
		//create root separators
		d->renderRootSep = new SoSeparator;
		d->renderRootSep->ref();
		d->renderRootSep->setName("HT2D_XY_root");

		d->sliceMatl = new SoMaterial;
		d->sliceMatl->ambientColor.setValue(1, 1, 1);
		d->sliceMatl->diffuseColor.setValue(1, 1, 1);
		d->renderRootSep->addChild(d->sliceMatl.ptr());

		d->sliceGroup = new SoSeparator;
		d->renderRootSep->addChild(d->sliceGroup.ptr());
		d->sliceFLSwitch = new SoSwitch;
		d->sliceFLSwitch->whichChild = 0;

		//shared range & TF
		d->htRange = new SoDataRange;
		d->htRange->setName("HTSliceRange");
		d->htRange->dataRangeId = 1;

		d->htWholeRange = new SoDataRange;
		d->htWholeRange->setName("HTWholeRange");
		d->htWholeRange->dataRangeId = 1;

		d->transFunc = new SoTransferFunction;
		d->transFunc->setName("colormap2D_TF");
		d->transFunc->transferFunctionId = 1;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		for (auto i = 0; i < 3; i++) {
			d->flTF[i] = makeColorMap(rgb[i], 3.39f, i);
			d->flRange[i] = new SoDataRange;
			d->flRange[i]->dataRangeId = i + 2;
		}

		buildHTSceneGraph();

		d->colorMapIdx = 0;
		d->transFunc->predefColorMap = d->color_map[0];

		d->fragmentShaderFL = new SoFragmentShader;
		d->fragmentShaderFL->sourceProgram.setValue(QString("%1/shader/FlOnly.glsl").arg(qApp->applicationDirPath()).toStdString());
		d->fragmentShaderFL->addShaderParameter1i("data1", 1);
		d->fragmentShaderFL->addShaderParameter1i("isCh0Exist", 0);
		d->fragmentShaderFL->addShaderParameter1i("data2", 2);
		d->fragmentShaderFL->addShaderParameter1i("isCh1Exist", 0);
		d->fragmentShaderFL->addShaderParameter1i("data3", 3);
		d->fragmentShaderFL->addShaderParameter1i("isCh2Exist", 0);

		d->shaderFL = new SoVolumeShader;
		d->shaderFL->forVolumeOnly = FALSE;
		d->shaderFL->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShaderFL.ptr());

		d->sliceFL = new SoOrthoSlice;
		//d->sliceFL->dataSetId = 1;
		d->sliceFL->setName("Slice_FL");
		d->sliceFL->axis = d->ax[0];
		d->sliceFL->alphaUse = SoOrthoSlice::ALPHA_AS_IS;

		//build scene graph for HT grayscale + 2DTF + FL overlay

		d->renderToTex = new SoRenderToTextureProperty;
		d->renderToTex->component = SoRenderToTextureProperty::RGB_ALPHA;
		d->renderToTex->size.setValue(SbVec2s(512, 512));
		d->renderToTex->updatePolicy = SoRenderToTextureProperty::WHEN_NEEDED;
		d->renderToTex->node.set1Value(0, (SoNode*)d->hiddenSep.ptr());

		d->tf2D = new SoTransferFunction2D(15);
		d->tf2D->model.setValue(SoTexture2::Model::REPLACE);
		d->tf2D->internalFormat.setValue(SoTransferFunction2D::RGBA_FORMAT);
		d->tf2D->minFilter = SoTransferFunction2D::NEAREST;
		d->tf2D->magFilter = SoTransferFunction2D::NEAREST;
		d->tf2D->renderToTextureProperty.setValue(d->renderToTex.ptr());

		d->htStepper = new SoSwitch;
		d->htStepper->addChild(new SoSeparator);
		d->htStepper->whichChild = 0;

		for (auto i = 0; i < 3; i++) {
			d->flStepper[i] = new SoSwitch;
			d->flStepper[i]->addChild(new SoSeparator);
			d->flStepper[i]->whichChild = 0;
		}

		d->mds = new SoMultiDataSeparator;		
		for (auto i = 0; i < 3; i++) {
			d->mds->addChild(d->flStepper[i].ptr());
			d->mds->addChild(d->flRange[i].ptr());
			d->mds->addChild(d->flTF[i].ptr());
		}
		d->mds->addChild(d->shaderFL.ptr());
		d->mds->addChild(d->sliceFL.ptr());

		d->flSep = new SoSeparator;
		d->flTrans = new SoTranslation;

		d->flSep->addChild(d->flTrans.ptr());
		d->flSep->addChild(d->mds.ptr());
		d->sliceFLSwitch->addChild(d->flSep.ptr());
		d->sliceGroup->addChild(d->sliceFLSwitch.ptr());

		d->htOverlaySep = new SoSeparator;
		d->htOverlaySep->addChild(d->tf2D.ptr());
		d->htOverlaySep->addChild(d->htStepper.ptr());
		d->htOverlaySep->addChild(d->htWholeRange.ptr());
		d->htOverlaySep->addChild(d->transFunc.ptr());

		d->fragmentShaderOverlay = new SoFragmentShader;
		d->fragmentShaderOverlay->sourceProgram.setValue(QString("%1/shader/htOverlayOnly.glsl").arg(qApp->applicationDirPath()).toStdString());
		d->fragmentShaderOverlay->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", 15));
		d->fragmentShaderOverlay->addShaderParameter1i("data1", 1);
		d->fragmentShaderOverlay->addShaderParameter1i("isOverlay", 0);
		d->fragmentShaderOverlay->addShaderParameter1f("htOpacity", 1);
		d->fragmentShaderOverlay->addShaderParameter1f("resX", 1);
		d->fragmentShaderOverlay->addShaderParameter1f("resY", 1);
		d->fragmentShaderOverlay->addShaderParameter1f("resZ", 1);

		d->htOverlayShader = new SoVolumeShader;
		d->htOverlayShader->forVolumeOnly = FALSE;
		d->htOverlayShader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShaderOverlay.ptr());

		d->htOverlaySlice = new SoOrthoSlice;
		d->htOverlaySlice->setName("Slice_Overlay");
		d->htOverlaySlice->axis = d->ax[0];
		d->htOverlaySlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;

		d->htOverlaySep->addChild(d->htOverlayShader.ptr());
		d->htOverlaySep->addChild(d->htOverlaySlice.ptr());

		d->htOverlaySwitch = new SoSwitch;
		d->htOverlaySwitch->whichChild = 0;

		d->overlayGroup = new SoSeparator;
		d->overlayTrans = new SoTranslation;
		d->overlayGroup->addChild(d->overlayTrans.ptr());
		d->overlayGroup->addChild(d->htOverlaySep.ptr());

		d->htOverlaySwitch->addChild(d->overlayGroup.ptr());
		d->sliceGroup->addChild(d->htOverlaySwitch.ptr());
	}

	auto OivSceneXZ::SetFLTrans(float transp, int ch) -> void {
		d->fl_transparency[ch] = transp;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}


	auto OivSceneXZ::modifyColorMap(SbColor color, float gamma, int id) -> void {
		auto tf = d->flTF[id];
		auto gammaValue = gamma;
		if (false == d->isGamma[id]) {
			gammaValue = 1;
		}
		if (nullptr != tf) {
			float R = color[0];
			float G = color[1];
			float B = color[2];
			float* p = tf->colorMap.startEditing();
			for (int i = 0; i < 256; ++i) {
				float factor = (float)i / 255 * (1.0 - d->fl_transparency[id]);
				float gammaFactor = pow((float)i / 255, (1.f / gammaValue)) * (1.0 - d->fl_transparency[id]);
				float mod_r = R * gammaFactor;
				float mod_g = G * gammaFactor;
				float mod_b = B * gammaFactor;
				mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
				mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
				mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
				*p++ = factor;
			}
			tf->colorMap.finishEditing();
		}
	}


	auto OivSceneXZ::makeColorMap(SbColor color, float gamma, int id, float* range) -> SoTransferFunction* {
		SoTransferFunction* pTF = new SoTransferFunction();
		pTF->transferFunctionId = id + 2;

		// Color map will contain 256 RGBA values -> 1024 float values.
		// setNum pre-allocates memory so we can edit the field directly.
		pTF->colorMap.setNum(256 * 4);
		if (range != nullptr) {
			pTF->minValue = range[0];
			pTF->maxValue = range[1];
		}
		float R = color[0];
		float G = color[1];
		float B = color[2];

		// Get an edit pointer, assign values, then finish editing.
		float* p = pTF->colorMap.startEditing();
		for (int i = 0; i < 256; ++i) {
			float factor = (float)i / 255 * (1.0 - d->fl_transparency[id]);
			float mod_r = R * factor;
			float mod_g = G * factor;
			float mod_b = B * factor;
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = factor;// pow(factor, gamma);
		}
		pTF->colorMap.finishEditing();
		return pTF;
	}

	//common
	auto OivSceneXZ::GetRenderRoot() -> SoSeparator* {
		return d->renderRootSep.ptr();
	}

	auto OivSceneXZ::SetFLTimeStep(int step, int ch) -> void {
		d->fl_time_step[ch] = step;
		if (step > -1) {
			if (d->flExist[ch] && d->flShow[ch]) {
				QString ch_text = QString("isCh%1Exist").arg(ch);
				d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 1);			
			}
		} else {
			QString ch_text = QString("isCh%1Exist").arg(ch);
			d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 0);
		}
	}

	auto OivSceneXZ::SetHTTimeStep(int step) -> void {
		d->ht_time_step = step;
		if (step > -1 && d->htShow) {
			d->htMatl->transparency.setValue(0);
		} else {
			d->htMatl->transparency.setValue(1);
		}
	}


	auto OivSceneXZ::SetTransparency(float transp) -> void {
		d->sliceMatl->transparency = transp;
		d->fragmentShaderOverlay->setShaderParameter1f("htOpacity", 1.0f - transp);
	}

	auto OivSceneXZ::GetTransparency() -> float {
		return d->sliceMatl->transparency.getValues(0)[0];
	}

	auto OivSceneXZ::SetGradMinMax(double min, double max) -> void {
		d->grad_min = min;
		d->grad_max = max;
	}

	//HT
	auto OivSceneXZ::SetHTRange(double min, double max) -> void {
		d->min = min;
		d->max = max;
		d->htRange->min = d->min / d->htValueDiv - d->htValueOffset;
		d->htRange->max = d->max / d->htValueDiv - d->htValueOffset;
		d->htWholeRange->min = d->min / d->htValueDiv - d->htValueOffset;
		d->htWholeRange->max = d->max / d->htValueDiv - d->htValueOffset;
	}

	auto OivSceneXZ::SetHTColorMap(int idx) -> void {
		d->transFunc->predefColorMap = d->color_map[idx];
		auto m_vol = MedicalHelper::find<SoVolumeData>(d->mds.ptr(), "volData0");
		MedicalHelper::dicomCheckMonochrome1(d->transFunc.ptr(), m_vol);
		d->colorMapIdx = idx;
	}

	auto OivSceneXZ::GetHTColorMapIdx() -> int {
		return d->colorMapIdx;
	}

	auto OivSceneXZ::SetHTExist(bool exist) -> void {
		d->volExist = exist;
	}

	auto OivSceneXZ::getSliceTF() -> SoTransferFunction* {
		return d->transFunc.ptr();
	}

	auto OivSceneXZ::SetVolume(SoVolumeData* ht, bool initRange) -> void {
		d->htStepper->replaceChild(0, ht);
		d->ht_stepper->replaceChild(0, ht);
		ht->dataSetId = 1;
		d->htVol = ht;
		if (initRange) {
			d->htRange->min = d->min / d->htValueDiv - d->htValueOffset;
			d->htRange->max = d->max / d->htValueDiv - d->htValueOffset;
		}
		d->fragmentShaderOverlay->setShaderParameter1f("resX", static_cast<float>(d->meta->data.data3D.resolutionX));
		d->fragmentShaderOverlay->setShaderParameter1f("resY", static_cast<float>(d->meta->data.data3D.resolutionY));
		d->fragmentShaderOverlay->setShaderParameter1f("resZ", static_cast<float>(d->meta->data.data3D.resolutionZ));
	}

	auto OivSceneXZ::SetLDM(bool isLDM) -> void {
		//d->sliceHTFL->largeSliceSupport = isLDM;
		if (isLDM) {
			auto htTransFactor = 100000.0;
			auto flTransFactor = 100000.0;
			if(d->meta) {
				if (d->meta->data.data3D.exist) {
					htTransFactor = (d->meta->data.data3D.sizeY + 1) * d->meta->data.data3D.resolutionY;
				}
				if (d->meta->data.data3DFL.excitation) {
					flTransFactor = (d->meta->data.data3DFL.sizeY + 1) * d->meta->data.data3DFL.resolutionY;
				}
			}
			d->flTrans->translation.setValue(0, -htTransFactor, 0);
			d->overlayTrans->translation.setValue(0, -htTransFactor - flTransFactor, 0);
			d->sliceFL->axis = d->ax[2];
			d->htSlice->axis = d->ax[2];
			d->htOverlaySlice->axis = d->ax[2];
		} else {
			d->overlayTrans->translation.setValue(0, 0, 0);
			d->flTrans->translation.setValue(0, 0, 0);
			d->sliceFL->axis = d->ax[0];
			d->htSlice->axis = d->ax[0];
			d->htOverlaySlice->axis = d->ax[0];
		}
	}

	auto OivSceneXZ::ToggleHTIntensity(bool show) -> void {
		d->htShow = show;
		if (show) {
			d->htMatl->transparency.setValue(0);
		} else {
			d->htMatl->transparency.setValue(1);
		}

	}

	auto OivSceneXZ::ToggleFLChannel(int ch, bool show) -> void {
		//toggle for FL MIP
		d->flShow[ch] = show;
		if (d->flExist[ch]) {
			auto ch_text = QString("isCh%1Exist").arg(ch);
			if (show) {
				d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 1);
			} else {
				d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 0);
			}
		}
	}

	auto OivSceneXZ::setFLExist(bool ch0, bool ch1, bool ch2) -> void {
		d->flExist[0] = ch0;
		d->flExist[1] = ch1;
		d->flExist[2] = ch2;
		ToggleFLChannel(0, ch0);
		ToggleFLChannel(1, ch1);
		ToggleFLChannel(2, ch2);		
	}

	auto OivSceneXZ::SetSliceIndexHT(int idx) -> void {
		d->htSlice->sliceNumber = idx;
		d->htOverlaySlice->sliceNumber = idx;
	}

	auto OivSceneXZ::SetSliceIndexFL(int idx) -> void {
		d->sliceFL->sliceNumber = idx;
	}		

	auto OivSceneXZ::SetFLVolume(int ch, SoVolumeData* fl, bool initRange) -> void {
		d->flStepper[ch]->replaceChild(0, fl);
		d->flVol[ch] = fl;
		d->flVol[ch]->dataSetId = ch + 2;

		if (initRange) {
			d->flRange[ch]->min = 0;
			d->flRange[ch]->max = 20;
		}
	}

	auto OivSceneXZ::SetFLRange(int ch, int min, int max) -> void {
		d->flRange[ch]->min = min;
		d->flRange[ch]->max = max;
	}

	//internal	
	auto OivSceneXZ::toScreenX(double val) -> double {
		return (val - d->min) / d->range;
	}

	auto OivSceneXZ::toScreenY(double val) -> double {
		return (val - d->grad_min) / (d->grad_max - d->grad_min);
	}


}
