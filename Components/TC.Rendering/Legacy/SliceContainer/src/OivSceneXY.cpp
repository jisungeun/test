#include <QVector>
#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoFragmentShader.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <Volumeviz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Medical/Helpers/MedicalHelper.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>

#include <VolumeViz/nodes/SoMultiDataSeparator.h>
#pragma warning(pop)

#include "SoTransferFunction2D.h"

#include "OivSceneXY.h"

#include <Inventor/nodes/SoTranslation.h>

namespace TC {
	struct OivSceneXY::Impl {
		//Comomn
		SoRef<SoSeparator> renderRootSep { nullptr };
		SoRef<SoSeparator> sliceGroup { nullptr };

		//volume data		
		SoRef<SoVolumeData> htVol { nullptr };
		SoRef<SoVolumeData> flVol[3] { nullptr, nullptr, nullptr };
		SoRef<SoVolumeData> mipVol { nullptr };
		SoRef<SoVolumeData> mipFLVolume[3] { nullptr, nullptr, nullptr };

		//for HT Grayscale + TF 2D Overlay + FL Overlay
		SoRef<SoSwitch> sliceHTFLSwitch { nullptr };
		SoRef<SoSeparator> htflGroup{ nullptr };
		SoRef<SoTranslation> htflTrans{ nullptr };
		SoRef<SoMultiDataSeparator> mds;
		//HT
		SoRef<SoSwitch> htStepper { nullptr };
		SoRef<SoDataRange> htRange { nullptr };
		SoRef<SoDataRange> htWholeRange { nullptr };
		SoRef<SoTransferFunction> transFunc { nullptr };

		SoRef<SoSeparator> hiddenSep { nullptr };
		SoRef<SoRenderToTextureProperty> renderToTex { nullptr };
		SoRef<SoTransferFunction2D> tf2D = { nullptr };

		//FL
		SoRef<SoSwitch> flStepper[3] { nullptr, nullptr, nullptr };
		SoRef<SoTransferFunction> flTF[3] { nullptr, nullptr, nullptr };
		SoRef<SoDataRange> flRange[3] { nullptr, nullptr, nullptr };

		SoRef<SoFragmentShader> fragmentShaderHTFL { nullptr };
		SoRef<SoVolumeShader> shaderHTFL { nullptr };
		SoRef<SoOrthoSlice> sliceHTFL { nullptr };

		//for HT
		SoRef<SoSwitch> htSwitch { nullptr };
		SoRef<SoMaterial> htMatl { nullptr };
		SoRef<SoSeparator> htSep { nullptr };
		SoRef<SoSwitch> ht_stepper { nullptr };
		SoRef<SoOrthoSlice> htSlice { nullptr };
		SoRef<SoVolumeShader> htQual { nullptr };

		//for MIP
		//independent scene graph
		SoRef<SoSwitch> bfSwitch { nullptr };

		SoRef<SoSwitch> mipSwitch { nullptr };
		SoRef<SoMaterial> mipMatl { nullptr };
		SoRef<SoSeparator> mipSep { nullptr };
		SoRef<SoSwitch> mip_stepper { nullptr };
		SoRef<SoOrthoSlice> mipSlice { nullptr };
		SoRef<SoVolumeShader> sliceQual { nullptr };

		//for FL MIP
		//independent scene graph
		SoRef<SoSwitch> mipFLSwitch { nullptr };
		SoRef<SoMaterial> mipFLMatl { nullptr };
		SoRef<SoMultiDataSeparator> mipFLSep { nullptr, };
		SoRef<SoSwitch> mipFL_stepper[3] { nullptr, nullptr, nullptr };
		SoRef<SoSwitch> mipFLRangeSwitch[3] { nullptr, nullptr, nullptr };
		SoRef<SoFragmentShader> fragmentShaderFL { nullptr };
		SoRef<SoVolumeShader> mipFLShader { nullptr };
		SoRef<SoOrthoSlice> mipFLSlice { nullptr };

		//shared from volume container		

		int fl_in_range { 1 };
		float fl_transparency[3] { 0.0f, };
		bool volExist;
		bool blendColor;
		int ht_time_step;
		int fl_time_step;
		int colorMapIdx;
		int min, max;
		double grad_min, grad_max;
		int range;

		bool flExist[3] = { false, };
		bool flShow[3] = { false, };

		bool htShow { true };

		bool isMIP { false };

		bool isOverlay { false };

		bool built { false };

		IO::TCFMetaReader::Meta::Pointer meta;
		double htValueOffset { 0 };
		double htValueDiv { 1 };
		double flValueOffset[3] { 0, 0, 0 };

		SoTransferFunction::PredefColorMap color_map[5] = {
			SoTransferFunction::INTENSITY,
			SoTransferFunction::INTENSITY_REVERSED,
			SoTransferFunction::GLOW,
			SoTransferFunction::PHYSICS,
			SoTransferFunction::STANDARD
		};
		MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL, MedicalHelper::SAGITTAL, MedicalHelper::CORONAL };
		SbColor rgb[3] = { SbColor(0, 0, 1), SbColor(0, 1, 0), SbColor(1, 0, 0) };

		QString nonUniformShader;
		QString uniformShader;

		bool htRangeSet { false };
		bool flRangeSet { false };

		float fl_color[3][3] { { 0.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0 }, { 1.0, 0.0, 0.0 } };
		double gamma[3] { 1.0, };
		bool isGamma[3] { false, };
	};

	OivSceneXY::OivSceneXY() : d { new Impl } {
		d->nonUniformShader = QString("%1/shader/HTFL_NonUniform.glsl").arg(qApp->applicationDirPath());
		d->uniformShader = QString("%1/shader/HTFL_2D_frag.glsl").arg(qApp->applicationDirPath());
	}

	OivSceneXY::~OivSceneXY() {
		d->renderRootSep->removeAllChildren();
		d->renderRootSep->unref();
	}

	auto OivSceneXY::SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
		if (d->meta->data.data3D.scalarType.count() > 0) {
			if (d->meta->data.data3D.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data3D.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		} else if (d->meta->data.data2DMIP.scalarType.count() > 0) {
			if (d->meta->data.data2DMIP.scalarType[0] == 1) {
				d->htValueOffset = d->meta->data.data2DMIP.riMin * 1000.0;
				d->htValueDiv = 10;
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (d->meta->data.data3DFL.scalarType[i].count() > 0) {
				if (d->meta->data.data3DFL.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data3DFL.min[i];
				}
			} else if (d->meta->data.data2DFLMIP.scalarType[i].count() > 0) {
				if (d->meta->data.data2DFLMIP.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->meta->data.data2DFLMIP.min[i];
				}
			}
		}
	}

	auto OivSceneXY::SetHToriginal(bool original) -> void {
		if (original && d->htShow) {
			if (d->isMIP) {
				d->mipSwitch->whichChild = 0;
			}
			d->htMatl->transparency.setValue(0);
			d->mipMatl->transparency.setValue(0);
		} else {
			d->htMatl->transparency.setValue(1);
			d->mipMatl->transparency.setValue(1);
		}
	}

	auto OivSceneXY::GetBuilt() -> bool {
		return d->built;
	}

	auto OivSceneXY::SetHTOverlay(bool overlay) -> void {
		d->isOverlay = overlay;
		if (overlay) {
			d->fragmentShaderHTFL->setShaderParameter1i("isOverlay", 1);
			d->htMatl->transparency.setValue(1);
		} else {
			d->fragmentShaderHTFL->setShaderParameter1i("isOverlay", 0);
			if (d->htShow) {
				d->htMatl->transparency.setValue(0);
			} else {
				d->htMatl->transparency.setValue(1);
			}
		}
	}

	auto OivSceneXY::SetUniform(bool isUniform) -> void {
		if (isUniform) {
			d->fragmentShaderHTFL->sourceProgram.setValue(d->uniformShader.toStdString());
		} else {
			d->fragmentShaderHTFL->sourceProgram.setValue(d->nonUniformShader.toStdString());
		}
	}

	//build scene graph
	auto OivSceneXY::buildSceneGraph() -> void {
		//create root separators
		d->renderRootSep = new SoSeparator;
		d->renderRootSep->ref();
		d->renderRootSep->setName("HT2D_XY_root");

		auto matl = new SoMaterial;
		matl->diffuseColor.setValue(1, 1, 1);
		matl->ambientColor.setValue(1, 1, 1);
		d->renderRootSep->addChild(matl);

		d->sliceGroup = new SoSeparator;
		d->renderRootSep->addChild(d->sliceGroup.ptr());
		d->sliceHTFLSwitch = new SoSwitch;
		d->sliceHTFLSwitch->whichChild = 0;

		//socket for BF slice
		if (nullptr == d->bfSwitch) {
			d->bfSwitch = new SoSwitch;
			d->bfSwitch->addChild(new SoSeparator);
			d->sliceGroup->addChild(d->bfSwitch.ptr());
		}
		//build Scene Graph for HT slice

		//shared range & TF
		d->htRange = new SoDataRange;
		d->htRange->setName("HTSliceRange");
		d->htRange->dataRangeId = 1;

		d->htWholeRange = new SoDataRange;
		d->htWholeRange->setName("HTWholeRange");
		d->htWholeRange->dataRangeId = 1;

		d->transFunc = new SoTransferFunction;
		d->transFunc->setName("colormap2D_TF");
		d->transFunc->transferFunctionId = 1;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		for (auto i = 0; i < 3; i++) {
			d->flTF[i] = makeColorMap(rgb[i], d->gamma[i], i);
			d->flRange[i] = new SoDataRange;
			d->flRange[i]->dataRangeId = i + 2;
		}

		d->colorMapIdx = 0;
		d->transFunc->predefColorMap = d->color_map[0];

		//build Scene Graph for HT slice
		buildHTSceneGraph();

		d->fragmentShaderHTFL = new SoFragmentShader;
		d->fragmentShaderHTFL->sourceProgram.setValue(QString("%1/shader/HTFL_2D_frag.glsl").arg(qApp->applicationDirPath()).toStdString());
		d->fragmentShaderHTFL->addShaderParameter1i("tex2D", SoPreferences::getInt("sample", 15));
		d->fragmentShaderHTFL->addShaderParameter1i("data1", 1);
		d->fragmentShaderHTFL->addShaderParameter1i("isOverlay", 0);
		d->fragmentShaderHTFL->addShaderParameter1i("data2", 2);
		d->fragmentShaderHTFL->addShaderParameter1i("isCh0Exist", 0);
		d->fragmentShaderHTFL->addShaderParameter1i("data3", 3);
		d->fragmentShaderHTFL->addShaderParameter1i("isCh1Exist", 0);
		d->fragmentShaderHTFL->addShaderParameter1i("data4", 4);
		d->fragmentShaderHTFL->addShaderParameter1i("isCh2Exist", 0);
		d->fragmentShaderHTFL->addShaderParameter1f("htOpacity", 1);
		d->fragmentShaderHTFL->addShaderParameter1f("resX", 1);
		d->fragmentShaderHTFL->addShaderParameter1f("resY", 1);
		d->fragmentShaderHTFL->addShaderParameter1f("resZ", 1);

		d->shaderHTFL = new SoVolumeShader;
		d->shaderHTFL->forVolumeOnly = FALSE;
		d->shaderHTFL->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShaderHTFL.ptr());

		d->sliceHTFL = new SoOrthoSlice;
		//d->sliceHTFL->dataSetId = 1;
		d->sliceHTFL->setName("Slice_HTFL");
		d->sliceHTFL->axis = d->ax[0];
		d->sliceHTFL->alphaUse = SoOrthoSlice::ALPHA_AS_IS;

		//build scene graph for HT grayscale + 2DTF + FL overlay

		d->renderToTex = new SoRenderToTextureProperty;
		d->renderToTex->component = SoRenderToTextureProperty::RGB_ALPHA;
		d->renderToTex->size.setValue(SbVec2s(512, 512));
		d->renderToTex->updatePolicy = SoRenderToTextureProperty::WHEN_NEEDED;
		d->renderToTex->node.set1Value(0, (SoNode*)d->hiddenSep.ptr());

		d->tf2D = new SoTransferFunction2D(15);
		d->tf2D->model.setValue(SoTexture2::Model::REPLACE);
		d->tf2D->internalFormat.setValue(SoTransferFunction2D::RGBA_FORMAT);
		d->tf2D->minFilter = SoTransferFunction2D::NEAREST;
		d->tf2D->magFilter = SoTransferFunction2D::NEAREST;
		d->tf2D->renderToTextureProperty.setValue(d->renderToTex.ptr());

		d->htStepper = new SoSwitch;
		d->htStepper->addChild(new SoSeparator);
		d->htStepper->whichChild = 0;

		for (auto i = 0; i < 3; i++) {
			d->flStepper[i] = new SoSwitch;
			d->flStepper[i]->addChild(new SoSeparator);
			d->flStepper[i]->whichChild = 0;
		}

		d->mds = new SoMultiDataSeparator;
		d->htflGroup = new SoSeparator;
		d->htflTrans = new SoTranslation;
		d->htflTrans->translation.setValue(0, 0, -100000.0);
		d->htflGroup->addChild(d->htflTrans.ptr());
		d->htflGroup->addChild(d->mds.ptr());
		d->sliceHTFLSwitch->addChild(d->htflGroup.ptr());
		d->sliceGroup->addChild(d->sliceHTFLSwitch.ptr());
		d->mds->addChild(d->tf2D.ptr());
		d->mds->addChild(d->htStepper.ptr());
		//d->mds->addChild(d->htRange);
		d->mds->addChild(d->htWholeRange.ptr());
		d->mds->addChild(d->transFunc.ptr());
		for (auto i = 0; i < 3; i++) {
			d->mds->addChild(d->flStepper[i].ptr());
			d->mds->addChild(d->flRange[i].ptr());
			d->mds->addChild(d->flTF[i].ptr());
		}
		d->mds->addChild(d->shaderHTFL.ptr());
		d->mds->addChild(d->sliceHTFL.ptr());

		//build Scene Graph for HT MIP slice
		buildMIPSceneGraph();
		//build Scene Graph for FL MIP slice
		buildFLMIPSceneGraph();

		d->built = true;
	}

	auto OivSceneXY::buildHTSceneGraph() -> void {
		d->htSep = new SoSeparator;
		d->htSep->setName("HtOnlySep");
		auto trans = new SoTranslation;
		trans->translation.setValue(0, 0, 0.1);
		d->htSep->addChild(trans);
		d->htMatl = new SoMaterial;
		d->htMatl->ambientColor.setValue(1, 1, 1);
		d->htMatl->diffuseColor.setValue(1, 1, 1);
		d->ht_stepper = new SoSwitch;
		d->ht_stepper->whichChild = 0;
		d->ht_stepper->addChild(new SoSeparator);

		d->htSlice = new SoOrthoSlice;
		d->htSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->htSlice->setName("Slice_HT");
		d->htSlice->axis = d->ax[0];

		d->htQual = new SoVolumeRenderingQuality;
		d->htQual->forVolumeOnly = FALSE;

		d->htSep->addChild(d->htMatl.ptr());
		d->htSep->addChild(d->htRange.ptr());
		d->htSep->addChild(d->ht_stepper.ptr());
		d->htSep->addChild(d->transFunc.ptr());
		d->htSep->addChild(d->htQual.ptr());
		d->htSep->addChild(d->htSlice.ptr());

		d->htSwitch = new SoSwitch;
		d->htSwitch->whichChild = 0;
		d->htSwitch->addChild(d->htSep.ptr());

		d->sliceGroup->addChild(d->htSwitch.ptr());
	}

	auto OivSceneXY::buildMIPSceneGraph() -> void {
		d->mipSep = new SoSeparator;
		d->mipMatl = new SoMaterial;
		d->mipMatl->ambientColor.setValue(1, 1, 1);
		d->mipMatl->diffuseColor.setValue(1, 1, 1);		
		d->mip_stepper = new SoSwitch;
		d->mip_stepper->whichChild = 0;
		d->mip_stepper->addChild(new SoSeparator);//fill empty volume

		d->mipSlice = new SoOrthoSlice;
		d->mipSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->mipSlice->setName("Slice_MIP");
		d->mipSlice->axis = d->ax[0];

		d->sliceQual = new SoVolumeRenderingQuality;
		d->sliceQual->forVolumeOnly = FALSE;

		d->mipSep->addChild(d->mipMatl.ptr());
		d->mipSep->addChild(d->htRange.ptr());
		d->mipSep->addChild(d->mip_stepper.ptr());
		d->mipSep->addChild(d->transFunc.ptr());//share with HT slice
		d->mipSep->addChild(d->sliceQual.ptr());//share with HT slice
		d->mipSep->addChild(d->mipSlice.ptr());

		d->mipSwitch = new SoSwitch;
		//d->mipSwitch->whichChild = -1;
		d->mipSwitch->whichChild = 0;
		d->mipSwitch->addChild(d->mipSep.ptr());

		d->sliceGroup->addChild(d->mipSwitch.ptr());//only for axial slice
	}

	auto OivSceneXY::setBFRoot(SoSeparator* bfRoot) -> void {
		d->bfSwitch->replaceChild(0, bfRoot);
	}

	auto OivSceneXY::toggleBF(bool show) -> void {
		if (show) {
			d->bfSwitch->whichChild = 0;
			d->htSwitch->whichChild = -1;
			d->mipSwitch->whichChild = -1;
			d->mipFLSwitch->whichChild = -1;
			d->sliceHTFLSwitch->whichChild = -1;
		} else {
			d->bfSwitch->whichChild = -1;
			if (d->isMIP) {
				d->mipSwitch->whichChild = 0;
				d->mipFLSwitch->whichChild = 0;
			} else {
				d->htSwitch->whichChild = 0;
				d->sliceHTFLSwitch->whichChild = 0;
			}
		}
	}

	auto OivSceneXY::buildFLMIPSceneGraph() -> void {
		d->mipFLSep = new SoMultiDataSeparator;
		d->mipFLMatl = new SoMaterial;
		d->mipFLMatl->ambientColor.setValue(1, 1, 1);
		d->mipFLMatl->diffuseColor.setValue(1, 1, 1);
		for (auto i = 0; i < 3; i++) {
			d->mipFL_stepper[i] = new SoSwitch;
			d->mipFL_stepper[i]->addChild(new SoSeparator);
			d->mipFL_stepper[i]->whichChild = 0;
			d->mipFLRangeSwitch[i] = new SoSwitch;
			d->mipFLRangeSwitch[i]->addChild(d->flRange[i].ptr());
			d->mipFLRangeSwitch[i]->whichChild = -1;
		}
		d->mipFLSlice = new SoOrthoSlice;
		d->mipFLSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->mipFLSlice->setName("Slice_MIPFL");
		d->mipFLSlice->axis = d->ax[0];

		const auto shaderPath = QString("%1/shader/MultiChannel2D_color_frag.glsl").arg(qApp->applicationDirPath());
		d->fragmentShaderFL = new SoFragmentShader;
		d->fragmentShaderFL->sourceProgram.setValue(shaderPath.toStdString());
		d->fragmentShaderFL->addShaderParameter1i("data1", 2);
		d->fragmentShaderFL->addShaderParameter1i("isCh0Exist", 0);
		d->fragmentShaderFL->addShaderParameter1i("data2", 3);
		d->fragmentShaderFL->addShaderParameter1i("isCh1Exist", 0);
		d->fragmentShaderFL->addShaderParameter1i("data3", 4);
		d->fragmentShaderFL->addShaderParameter1i("isCh2Exist", 0);
		d->mipFLShader = new SoVolumeShader;
		d->mipFLShader->forVolumeOnly = FALSE;
		d->mipFLShader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->fragmentShaderFL.ptr());

		d->mipFLSep->addChild(d->mipFLShader.ptr());
		for (auto i = 0; i < 3; i++) {
			d->mipFLSep->addChild(d->mipFL_stepper[i].ptr());
		}
		for (auto i = 0; i < 3; i++) {
			//d->mipFLSep->addChild(d->mipFLRange[i]);
			d->mipFLSep->addChild(d->mipFLRangeSwitch[i].ptr());
		}
		for (auto i = 0; i < 3; i++) {
			d->mipFLSep->addChild(d->flTF[i].ptr());
		}
		d->mipFLSep->addChild(d->mipFLSlice.ptr());

		d->mipFLSwitch = new SoSwitch;
		d->mipFLSwitch->whichChild = -1;
		d->mipFLSwitch->addChild(d->mipFLSep.ptr());

		d->sliceGroup->addChild(d->mipFLSwitch.ptr());
	}

	auto OivSceneXY::SetFLInRange(int inrange) -> void {
		d->fl_in_range = inrange;
		for (auto i = 0; i < 3; i++) {
			SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
			modifyColorMap(rgb[i], d->gamma[i], i);
		}
	}

	auto OivSceneXY::SetFLGamma(int ch, bool enable, double gamma) -> void {
		d->gamma[ch] = gamma;
		d->isGamma[ch] = enable;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}

	auto OivSceneXY::SetFLTrans(float transp, int ch) -> void {
		d->fl_transparency[ch] = transp;
		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}


	auto OivSceneXY::modifyColorMap(SbColor color, float gamma, int id) -> void {
		auto tf = d->flTF[id];
		auto gammaValue = gamma;
		if (false == d->isGamma[id]) {
			gammaValue = 1;
		}
		if (nullptr != tf) {
			float R = color[0];
			float G = color[1];
			float B = color[2];
			float* p = tf->colorMap.startEditing();
			for (int i = 0; i < 256; ++i) {
				float factor = (float)i / 255 * d->fl_in_range * (1.0 - d->fl_transparency[id]);
				float gammaFactor = pow((float)i / 255, (1.f / gammaValue)) * d->fl_in_range * (1.0 - d->fl_transparency[id]);
				float mod_r = R * gammaFactor;
				float mod_g = G * gammaFactor;
				float mod_b = B * gammaFactor;
				mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
				mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
				mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
				*p++ = factor;
			}
			tf->colorMap.finishEditing();
		}
	}

	auto OivSceneXY::makeColorMap(SbColor color, float gamma, int id, float* range) -> SoTransferFunction* {
		SoTransferFunction* pTF = new SoTransferFunction();
		pTF->transferFunctionId = id + 2;

		// Color map will contain 256 RGBA values -> 1024 float values.
		// setNum pre-allocates memory so we can edit the field directly.
		pTF->colorMap.setNum(256 * 4);
		if (range != nullptr) {
			pTF->minValue = range[0];
			pTF->maxValue = range[1];
		}
		float R = color[0];
		float G = color[1];
		float B = color[2];

		// Get an edit pointer, assign values, then finish editing.
		float* p = pTF->colorMap.startEditing();
		for (int i = 0; i < 256; ++i) {
			float factor = (float)i / 255 * (1.0 - d->fl_transparency[id]);
			float mod_r = R * factor;
			float mod_g = G * factor;
			float mod_b = B * factor;
			mod_r > 1.0 ? *p++ = 1.0 : *p++ = mod_r;
			mod_g > 1.0 ? *p++ = 1.0 : *p++ = mod_g;
			mod_b > 1.0 ? *p++ = 1.0 : *p++ = mod_b;
			*p++ = factor;
		}
		pTF->colorMap.finishEditing();
		return pTF;
	}

	//common
	auto OivSceneXY::GetRenderRoot() -> SoSeparator* {
		return d->renderRootSep.ptr();
	}

	auto OivSceneXY::SetFLTimeStep(int step, int ch) -> void {
		d->fl_time_step = step;
		if (step > -1) {
			if (d->flExist[ch] && d->flShow[ch]) {
				QString ch_text = QString("isCh%1Exist").arg(ch);
				d->fragmentShaderHTFL->setShaderParameter1i(ch_text.toStdString(), 1);
			}
			d->mipFL_stepper[ch]->whichChild = 0;
		} else {
			QString ch_text = QString("isCh%1Exist").arg(ch);
			d->fragmentShaderHTFL->setShaderParameter1i(ch_text.toStdString(), 0);
			d->mipFL_stepper[ch]->whichChild = -1;
		}
	}

	auto OivSceneXY::SetHTTimeStep(int step) -> void {
		d->ht_time_step = step;
		if (step > -1) {
			d->mip_stepper->whichChild = 0;
			if (d->htShow) {
				if (d->isMIP) {
					d->mipSwitch->whichChild = 0;
				}
				d->htMatl->transparency.setValue(0);
				d->mipMatl->transparency.setValue(0);
			} else {
				d->htMatl->transparency.setValue(1);
				d->mipMatl->transparency.setValue(1);
			}
		} else {
			d->mip_stepper->whichChild = -1;
			d->htMatl->transparency.setValue(1);
			d->mipMatl->transparency.setValue(1);
		}
	}


	auto OivSceneXY::SetTransparency(float transp) -> void {
		d->mipMatl->transparency.setValue(transp);
		d->fragmentShaderHTFL->setShaderParameter1f("htOpacity", 1.0f - transp);
	}

	auto OivSceneXY::GetTransparency() -> float {
		return d->mipMatl->transparency.getValues(0)[0];
	}

	auto OivSceneXY::SetGradMinMax(double min, double max) -> void {
		d->grad_min = min;
		d->grad_max = max;
	}

	//HT
	auto OivSceneXY::SetHTLevelWindow(double min, double max) -> void {
		d->htRange->min = min / d->htValueDiv - d->htValueOffset;
		d->htRange->max = max / d->htValueDiv - d->htValueOffset;
	}

	auto OivSceneXY::SetHTRange(double min, double max) -> void {
		d->min = min;
		d->max = max;
		d->htRange->min = d->min / d->htValueDiv - d->htValueOffset;
		d->htRange->max = d->max / d->htValueDiv - d->htValueOffset;
		d->htWholeRange->min = d->min / d->htValueDiv - d->htValueOffset;
		d->htWholeRange->max = d->max / d->htValueDiv - d->htValueOffset;
	}

	auto OivSceneXY::SetHTColorMap(int idx) -> void {
		d->transFunc->predefColorMap = d->color_map[idx];
		auto m_vol = MedicalHelper::find<SoVolumeData>(d->mds.ptr(), "volData0");
		MedicalHelper::dicomCheckMonochrome1(d->transFunc.ptr(), m_vol);
		d->colorMapIdx = idx;
	}

	auto OivSceneXY::GetHTColorMapIdx() -> int {
		return d->colorMapIdx;
	}

	auto OivSceneXY::SetHiddenSep(SoSeparator* hidden) -> void {
		d->hiddenSep = hidden;
	}

	auto OivSceneXY::SetHTExist(bool exist) -> void {
		d->volExist = exist;
	}

	auto OivSceneXY::getSliceTF() -> SoTransferFunction* {
		return d->transFunc.ptr();
	}

	//MIP
	auto OivSceneXY::ShowMIP(bool show) -> void {
		d->isMIP = show;
		if (show) {
			d->sliceHTFLSwitch->whichChild = -1;
			d->htSwitch->whichChild = -1;
		} else {
			d->sliceHTFLSwitch->whichChild = 0;
			d->htSwitch->whichChild = 0;
		}
		d->bfSwitch->whichChild = -1;
	}

	auto OivSceneXY::ShowHTMIP(bool show) -> void {
		if (show) {
			d->mipSwitch->whichChild = 0;
		} else {
			d->mipSwitch->whichChild = -1;
		}
	}

	auto OivSceneXY::SetVolume(SoVolumeData* ht, bool initRange) -> void {
		d->ht_stepper->replaceChild(0, ht);
		d->htStepper->replaceChild(0, ht);
		ht->dataSetId = 1;
		d->htVol = ht;
		if (initRange) {
			d->htRange->min = d->min / d->htValueDiv - d->htValueOffset;
			d->htRange->max = d->max / d->htValueDiv - d->htValueOffset;
		}
		d->fragmentShaderHTFL->setShaderParameter1f("resX", static_cast<float>(d->meta->data.data3D.resolutionX));
		d->fragmentShaderHTFL->setShaderParameter1f("resY", static_cast<float>(d->meta->data.data3D.resolutionY));
		d->fragmentShaderHTFL->setShaderParameter1f("resZ", static_cast<float>(d->meta->data.data3D.resolutionZ));
	}

	auto OivSceneXY::SetMIPVolume(SoVolumeData* mip, bool initRange) -> void {
		d->mip_stepper->replaceChild(0, mip);
		mip->dataSetId = 1;
		d->mipVol = mip;
		if (initRange) {
			d->htRange->min = d->min / d->htValueDiv - d->htValueOffset;
			d->htRange->max = d->max / d->htValueDiv - d->htValueOffset;
		}
	}

	auto OivSceneXY::SetSliceIndexHT(int idx) -> void {
		d->htSlice->sliceNumber = idx;		
	}

	auto OivSceneXY::SetSliceIndexHTFL(int idx) -> void {
		d->sliceHTFL->sliceNumber = idx;
	}
	
	auto OivSceneXY::SetLDM(bool isLDM) -> void {
		d->htSlice->largeSliceSupport = isLDM;
		d->sliceHTFL->largeSliceSupport = isLDM;
		d->mipSlice->largeSliceSupport = isLDM;
		d->mipFLSlice->largeSliceSupport = isLDM;
	}

	auto OivSceneXY::ClearMIPVolume() -> void {
		if (nullptr != d->mip_stepper) {
			d->mip_stepper->replaceChild(0, new SoSeparator);
			d->mipVol = nullptr;
		}
	}

	auto OivSceneXY::ClearVolume() -> void {
		d->ht_stepper->replaceChild(0, new SoSeparator);
		d->htStepper->replaceChild(0, new SoSeparator);
		d->htVol = nullptr;
	}

	//FL MIP
	auto OivSceneXY::ShowFLMIP(bool show) -> void {
		if (show) {
			d->mipFLSwitch->whichChild = 0;
		} else {
			d->mipFLSwitch->whichChild = -1;
		}
	}

	auto OivSceneXY::ToggleHTIntensity(bool show) -> void {
		d->htShow = show;
		if (show) {
			if (d->isMIP) {
				d->mipSwitch->whichChild = 0;
			}
			d->htMatl->transparency.setValue(0);
			d->mipMatl->transparency.setValue(0);
		} else {
			d->htMatl->transparency.setValue(1);
			d->mipMatl->transparency.setValue(1);
		}
	}

	auto OivSceneXY::SetFLColor(int r, int g, int b, int ch) -> void {
		d->fl_color[ch][0] = static_cast<float>(r) / 255.0f;
		d->fl_color[ch][1] = static_cast<float>(g) / 255.0f;
		d->fl_color[ch][2] = static_cast<float>(b) / 255.0f;

		SbColor rgb[3] = { SbColor(d->fl_color[0][0], d->fl_color[0][1], d->fl_color[0][2]), SbColor(d->fl_color[1][0], d->fl_color[1][1], d->fl_color[1][2]), SbColor(d->fl_color[2][0], d->fl_color[2][1], d->fl_color[2][2]) };
		modifyColorMap(rgb[ch], d->gamma[ch], ch);
	}

	auto OivSceneXY::ToggleFLChannel(int ch, bool show) -> void {
		//toggle for FL MIP
		d->flShow[ch] = show;
		if (d->flExist[ch]) {
			auto ch_text = QString("isCh%1Exist").arg(ch);
			if (show) {
				d->fragmentShaderHTFL->setShaderParameter1i(ch_text.toStdString(), 1);
			} else {
				d->fragmentShaderHTFL->setShaderParameter1i(ch_text.toStdString(), 0);
			}

			if (show) {
				d->mipFLRangeSwitch[ch]->whichChild = 0;
				d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 1);
			} else {
				d->mipFLRangeSwitch[ch]->whichChild = -1;
				d->fragmentShaderFL->setShaderParameter1i(ch_text.toStdString(), 0);
			}
		}
	}

	auto OivSceneXY::setFLExist(bool ch0, bool ch1, bool ch2) -> void {
		d->flExist[0] = ch0;
		d->flExist[1] = ch1;
		d->flExist[2] = ch2;
		if (d->meta->data.isLDM) {
			//d->sliceHTFL->dataSetId = 1;
			return;
		}
		if (false == d->volExist) {
			auto idx = 2;
			if (d->flExist[0]) {
				idx = 2;
			} else if (d->flExist[1]) {
				idx = 3;
			} else if (d->flExist[2]) {
				idx = 4;
			}
			//d->sliceHTFL->dataSetId = idx;
		}
	}

	auto OivSceneXY::SetFLVolume(int ch, SoVolumeData* fl, bool initRange) -> void {
		d->flStepper[ch]->replaceChild(0, fl);
		d->flVol[ch] = fl;
		d->flVol[ch]->dataSetId = ch + 2;

		if (initRange) {
			d->flRange[ch]->min = 0;
			d->flRange[ch]->max = 200;
		}
	}

	auto OivSceneXY::SetMIPFLVolume(int ch, SoVolumeData* vol, bool initRange) -> void {
		d->mipFL_stepper[ch]->replaceChild(0, vol);
		d->mipFLVolume[ch] = vol;
		d->mipFLVolume[ch]->dataSetId = ch + 2;

		if (initRange) {
			d->flRange[ch]->min = 0;
			d->flRange[ch]->max = 200;
		}
	}

	auto OivSceneXY::SetFLRange(int ch, int min, int max) -> void {
		d->flRange[ch]->min = min - d->flValueOffset[ch];
		d->flRange[ch]->max = max - d->flValueOffset[ch];
	}

	auto OivSceneXY::ClearMIPFLVolume() -> void {
		for (auto ch = 0; ch < 3; ch++) {
			d->mipFL_stepper[ch]->replaceChild(0, new SoSeparator);
		}
	}

	auto OivSceneXY::ClearFLVolume() -> void {
		for (auto ch = 0; ch < 3; ch++) {
			d->flStepper[ch]->replaceChild(0, new SoSeparator);
		}
	}


	//internal	
	auto OivSceneXY::toScreenX(double val) -> double {
		return (val - d->min) / d->range;
	}

	auto OivSceneXY::toScreenY(double val) -> double {
		return (val - d->grad_min) / (d->grad_max - d->grad_min);
	}


}
