#pragma once

#include <memory>

#include "TCSliceContainerExport.h"

class SoVolumeData;
class SoSeparator;

class TCSliceContainer_API Oiv2dBFSlice {
public:
	Oiv2dBFSlice();
	~Oiv2dBFSlice();

	auto getRenderRootNode() -> SoSeparator*;

	auto setSingleBF(SoVolumeData* rVol, SoVolumeData* gVol, SoVolumeData* bVol, int step) -> void;
	auto buildBFSlice(int steps) -> void;

	auto setTransparency(float transp) -> void;
	auto getTransparency() -> float;

	auto isVolumeExist() -> bool;

	auto setTimeStep(int step) -> void;
	auto getTimeStep() -> int;
	auto getTimeSteps() -> int;

	auto ClearData() -> void;

private:
	auto buildColorTF() -> void;

	struct Impl;
	std::unique_ptr<Impl> d;
};
