#pragma once

#include <memory>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoRenderToTextureProperty.h>

#include <TCFMetaReader.h>

#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include "TCSliceContainerExport.h"

class SoTransferFunction2D;

namespace TC {
	class TCSliceContainer_API OivSceneYZ {
	public:
		OivSceneYZ();
		~OivSceneYZ();
		//common
		auto GetRenderRoot() -> SoSeparator*;

		auto SetHTTimeStep(int step) -> void;
		auto SetFLTimeStep(int step, int ch) -> void;

		auto SetGradMinMax(double min, double max) -> void;

		auto SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void;

		//set volumes
		auto SetVolume(SoVolumeData* ht, bool initRange = false) -> void;
		auto SetFLVolume(int ch, SoVolumeData* fl, bool initRange = false) -> void;

		//set sliceindex
		auto SetSliceIndexHT(int idx)->void;
		auto SetSliceIndexFL(int idx)->void;

		//HT 
		auto SetHTRange(double min, double max) -> void;
		auto SetHTColorMap(int idx) -> void;
		auto GetHTColorMapIdx() -> int;
		auto getSliceTF() -> SoTransferFunction*;
		auto SetTransparency(float transp) -> void;
		auto GetTransparency() -> float;
		auto SetHTOverlay(bool overlay) -> void;
		auto SetHToriginal(bool original) -> void;

		//FL        
		auto setFLExist(bool ch0, bool ch1, bool ch2) -> void;
		auto SetFLRange(int ch, int min, int max) -> void;
		auto SetFLColor(int r, int g, int b, int ch) -> void;
		auto SetFLGamma(int ch, bool enable, double gamma) -> void;
		auto SetFLTrans(float transp, int ch) -> void;
		auto ToggleFLChannel(int ch, bool show) -> void;
		auto ToggleHTIntensity(bool show) -> void;

		//Commmon
		auto buildHTSceneGraph() -> void;
		auto buildSceneGraph() -> void;
		auto SetLDM(bool isLDM) -> void;
		auto SetUniform(bool isUniform) -> void;
		auto SetHTExist(bool exist) -> void;
		auto SetHiddenSep(SoSeparator* hidden) -> void;

	private:
		auto toScreenX(double val) -> double;
		auto toScreenY(double val) -> double;
		auto makeColorMap(SbColor color, float gamma, int id, float* range = nullptr) -> SoTransferFunction*;
		auto modifyColorMap(SbColor color, float gamma, int id) -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
