#pragma once

#include <memory>
#include <TCFMetaReader.h>

#include "TCSliceContainerExport.h"

class SoTransferFunction2D;
class SoVolumeData;
class SoSeparator;
class SoTransferFunction;
class SbColor;

namespace TC {
	class TCSliceContainer_API OivSceneXY {
	public:
		OivSceneXY();
		~OivSceneXY();
		//common
		auto GetRenderRoot() -> SoSeparator*;

		auto SetHTTimeStep(int step) -> void;
		auto SetFLTimeStep(int step, int ch) -> void;

		auto SetGradMinMax(double min, double max) -> void;

		auto SetMetaInfo(IO::TCFMetaReader::Meta::Pointer meta) -> void;

		//set volumes
		auto SetVolume(SoVolumeData* ht, bool initRange = false) -> void;
		auto SetFLVolume(int ch, SoVolumeData* fl, bool initRange = false) -> void;
		auto SetMIPVolume(SoVolumeData* mip, bool initRange = false) -> void;
		auto SetMIPFLVolume(int ch, SoVolumeData* vol, bool initRange = false) -> void;

		auto SetSliceIndexHT(int idx)->void;
		auto SetSliceIndexHTFL(int idx)->void;

		//HT
		auto SetHTLevelWindow(double min, double max) -> void;
		auto SetHTRange(double min, double max) -> void;
		auto SetHTColorMap(int idx) -> void;
		auto GetHTColorMapIdx() -> int;
		auto getSliceTF() -> SoTransferFunction*;
		auto ShowMIP(bool show) -> void;
		auto ShowHTMIP(bool show) -> void;
		auto ClearMIPVolume() -> void;
		auto ClearVolume() -> void;
		auto SetTransparency(float transp) -> void;
		auto GetTransparency() -> float;
		auto SetHTOverlay(bool overlay) -> void;
		auto SetHToriginal(bool original) -> void;

		//FL        
		auto setFLExist(bool ch0, bool ch1, bool ch2) -> void;
		auto ShowFLMIP(bool show) -> void;
		auto SetFLRange(int ch, int min, int max) -> void;
		auto SetFLTrans(float transp, int ch) -> void;
		auto SetFLInRange(int inrange) -> void;
		auto SetFLColor(int r, int g, int b, int ch) -> void;
		auto SetFLGamma(int ch, bool enable, double gamma) -> void;
		auto ClearMIPFLVolume() -> void;
		auto ClearFLVolume() -> void;
		auto ToggleFLChannel(int ch, bool show) -> void;
		auto ToggleHTIntensity(bool show) -> void;

		//BF
		auto setBFRoot(SoSeparator* bfRoot) -> void;
		auto toggleBF(bool show) -> void;

		//Commmon
		auto buildSceneGraph() -> void;
		auto SetLDM(bool isLDM) -> void;
		auto SetUniform(bool isUniform) -> void;
		auto SetHTExist(bool exist) -> void;
		auto SetHiddenSep(SoSeparator* hidden) -> void;
		auto GetBuilt() -> bool;

	private:
		auto buildHTSceneGraph() -> void;
		auto buildMIPSceneGraph() -> void;
		auto buildFLMIPSceneGraph() -> void;
		auto toScreenX(double val) -> double;
		auto toScreenY(double val) -> double;
		auto makeColorMap(SbColor color, float gamma, int id, float* range = nullptr) -> SoTransferFunction*;
		auto modifyColorMap(SbColor color, float gamma, int id) -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
