#include <Inventor/ViewerComponents/Qt/QtHelper.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaOrbiter.h>

#include <iostream>
#include <QSettings>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QApplication>

//Open Inventor
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Inventor/nodes/SoFragmentShader.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoSeparator.h>
#include <LDM/nodes/SoDataRange.h>
#include <LDM/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include <QTransferFunctionCanvas2D.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivHdf5Reader.h>
#include <QMainWindow>
#include <QOiv2DRenderWindow.h>
#include <QOiv3DRenderWindow.h>
#include <TCFMetaReader.h>
#include <ImageViz/SoImageViz.h>
#include <VolumeViz/nodes/SoVolumeShader.h>

//Create simple Qt+OIV 2D view with tooth image
void main(int argc, char** argv) {
	QApplication app(argc, argv);

	QSettings qs("Test/OivNonUniformText");

	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prevPath, "TCF (*.tcf)");

	if (fileName.isEmpty()) {
		std::cout << "file name is empty" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();

	//Read meta data
	auto metaReader = new TC::IO::TCFMetaReader;
	auto metaInfo = metaReader->Read(fileName);

	if (false == metaInfo->data.data3DFL.exist) {
		std::cout << "fl 3d data is not exist" << std::endl;
		return;
	}

	if (false == metaInfo->data.isLDM) {
		std::cout << "file is not an ldm" << std::endl;
		//return;
	}

	QOiv2DRenderWindow* renWinYZ = new QOiv2DRenderWindow(nullptr);

	//build Simple SceneGraph	
	SoSeparator* root = new SoSeparator;

	SoMultiDataSeparator* mds = new SoMultiDataSeparator;
	root->addChild(mds);

	SoTransferFunction* tfHT = new SoTransferFunction;
	tfHT->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
	tfHT->transferFunctionId = 1;
	mds->addChild(tfHT);

	SoDataRange* rangeHT = new SoDataRange;
	rangeHT->dataRangeId = 1;
	mds->addChild(rangeHT);

	SoSwitch* switchHT = new SoSwitch;
	SoVolumeData* volumeHT = new SoVolumeData;
	switchHT->addChild(volumeHT);
	switchHT->whichChild = 0;
	mds->addChild(switchHT);

	SoTransferFunction* tfFL = new SoTransferFunction;
	tfFL->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
	tfFL->transferFunctionId = 2;
	mds->addChild(tfFL);

	SoDataRange* rangeFL = new SoDataRange;
	rangeFL->dataRangeId = 2;
	mds->addChild(rangeFL);

	SoSwitch* switchFL = new SoSwitch;
	SoVolumeData* volumeFL = new SoVolumeData;
	volumeFL->dataSetId = 2;
	switchFL->addChild(volumeFL);
	switchFL->whichChild = -1;	
	mds->addChild(switchFL);

	SoFragmentShader* fragShader = new SoFragmentShader;
	fragShader->sourceProgram.setValue("$OIVHOME/examples/source/VolumeViz/multiData/AmplitudeVelocity/test.glsl");
	fragShader->addShaderParameter1i("data1", 1);
	fragShader->addShaderParameter1i("data2", 2);

	SoVolumeShader* volShader = new SoVolumeShader;
	volShader->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, fragShader);
	mds->addChild(volShader);

	SoOrthoSlice* sliceHT = new SoOrthoSlice;
	sliceHT->dataSetId = 1;
	mds->addChild(sliceHT);
	sliceHT->axis = MedicalHelper::Axis::CORONAL;

	//SoOrthoSlice* sliceFL = new SoOrthoSlice;
	//sliceFL->dataSetId = 2;
	//mds->addChild(sliceFL);
	//sliceFL->axis = MedicalHelper::Axis::CORONAL;

    renWinYZ->setSceneGraph(root);


	if (metaInfo->data.isLDM) {
		auto htReader = new OivLdmReader;
		htReader->setTileName("000000");
		htReader->setTileDimension(metaInfo->data.data3D.tileSizeX, metaInfo->data.data3D.tileSizeY, metaInfo->data.data3D.tileSizeZ);
		htReader->setDataGroupPath("/Data/3D");
		htReader->setFilename(fileName.toStdString());
		volumeHT->setReader(*htReader, TRUE);

		auto flReader = new OivLdmReaderFL(1);
		flReader->setTileName("000000");
		flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX, metaInfo->data.data3DFL.tileSizeY, metaInfo->data.data3DFL.tileSizeZ);
		flReader->setDataGroupPath("/Data/3DFL/CH1");
		flReader->setFilename(fileName.toStdString());
		volumeFL->setReader(*flReader, TRUE);
	}
	else {
		auto htReader = new OivHdf5Reader;
		htReader->setTileName("000000");
		htReader->setTileDimension(metaInfo->data.data3D.tileSizeX);
		htReader->setDataGroupPath("/Data/3D");
		htReader->setFilename(fileName.toStdString());
		volumeHT->setReader(*htReader, TRUE);

		auto flReader = new OivHdf5Reader(true);
		flReader->setTileName("000000");
		flReader->setTileDimension(metaInfo->data.data3DFL.tileSizeX);
		flReader->setDataGroupPath("/Data/3DFL/CH1");
		flReader->setFilename(fileName.toStdString());
		volumeFL->setReader(*flReader, TRUE);
	}	

	MedicalHelper::dicomAdjustDataRange(rangeHT, volumeHT);
	MedicalHelper::dicomAdjustDataRange(rangeFL, volumeFL);
	MedicalHelper::orientView(MedicalHelper::CORONAL, renWinYZ->getCamera(), volumeHT);

	double min, max;
	min = metaInfo->data.data3D.riMin * 10000.0;
	max = metaInfo->data.data3D.riMax * 10000.0;

	renWinYZ->show();

	app.exec();

	delete renWinYZ;

	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();
}