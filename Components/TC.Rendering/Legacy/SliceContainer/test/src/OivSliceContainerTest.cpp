#include <iostream>

#include <QFileDialog>
#include <QApplication>

#include <QOiv2DRenderWindow.h>

#include <Inventor/nodes/SoSeparator.h>

#include <VolumeViz/nodes/SoDataRange.h>

#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/ViewerComponents/Qt/RenderAreaExaminer.h>

#include <VolumeViz/nodes/SoMultiDataSeparator.h>

#include <QOiv3DRenderWindow.h>


#include <OivTcfReader.h>


void main(int argc,char** argv) {
	QApplication app(argc, argv);
    	
	SoVolumeRendering::init();

	//can be found in multi window test

	app.exec();

	SoVolumeRendering::finish();
}