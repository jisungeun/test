#include "DMDControlDLPC350.h"
#include "DMDControlFactory.h"

namespace TC::DMDControl {
    auto DMDControlFactory::Create(Controller type) -> DMDControl::Pointer {

        auto create = [&]()->DMDControl* {
            switch(type) {
            case Controller::DLPC350:
                return new DMDControlDLPC350();
            }
            return nullptr;
        };

        static DMDControl::Pointer instance{ create() };
        return instance;
    }

}
