#define LOGGER_TAG "[DMD]"

#include <TCLogger.h>
#include "DMDControl.h"

namespace TC::DMDControl {
    struct DMDControl::Impl {
        DMDErrorCode lastError{ DMDErrorCode::NoError };
        QString version;
        QString firmwareTag;
    };

    DMDControl::DMDControl() : d{new Impl} {
    }

    DMDControl::~DMDControl() {
    }

    auto DMDControl::GetLastError() const -> DMDErrorCode {
        auto code = d->lastError;
        d->lastError = DMDErrorCode::NoError;
        return code;
    }

    auto DMDControl::GetFirmwareVersion() const -> QString {
        return d->version;
    }

    auto DMDControl::GetFirmwareTag() const -> QString {
        return d->firmwareTag;
    }

    auto DMDControl::SetLastError(DMDErrorCode code) -> void {
        d->lastError = code;
    }

    auto DMDControl::SetFirmwareVerision(const QString& version) -> void {
        d->version = version;
    }

    auto DMDControl::SetFirmwareTag(const QString& tag) -> void {
        d->firmwareTag = tag;
    }

    auto DMDControl::Update(const QString& message, DMDErrorCode code) -> void {
        if(code == +DMDErrorCode::NoError) {
            QLOG_INFO() << message;
        } else {
            SetLastError(code);
            QLOG_ERROR() << message << "[Error=" << code << "]";
        }
    }
}
