#pragma once
#include <memory>

#include "DMDControl.h"

namespace TC::DMDControl {
    class DMDControlDLPC350 : public DMDControl {
    public:
        DMDControlDLPC350();
        virtual ~DMDControlDLPC350();

        auto Initialize() -> bool override;
        auto SetBitDepth(uint32_t bitdepth)->bool override;
        auto SetExposure(uint32_t exposure, uint32_t period)->bool override;
        auto SetLEDChannel(uint32_t channel, uint32_t intensity)->bool override;
        auto SetLEDChannel(uint32_t redIntensity, uint32_t greenIntensity, uint32_t blueIntensity)->bool override;
        auto SetSequence(QList<uint32_t> list)->bool override;
        auto SetLEDSequence(QList<uint32_t> list)->bool override;
        auto SetTriggerType(int32_t type)->bool override;
        auto StartSequence(bool bRepeat)->bool override;
        auto StartLEDSequence(bool bRepeat)->bool override;
        auto StopSequence(void)->bool override;

    protected:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
