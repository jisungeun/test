#define LOGGER_TAG "[DMD]"

#include <QThread>

#include <TCLogger.h>

#include "dlpc350_usb.h"
#include "dlpc350_api.h"

#include "DMDControlDLPC350.h"

namespace TC::DMDControl {
    struct DMDControlDLPC350::Impl {
        uint32_t bitDepth{ 6 };
        uint32_t exposure{ 4000 };
        uint32_t period{ 6000 };
        uint32_t ledChannel{ 0 };
        QList<uint32_t> imageList;
        QList<uint32_t> ledList;
        int32_t triggerType{ 0 };
    };

    DMDControlDLPC350::DMDControlDLPC350() : DMDControl(), d{new Impl} {
    }

    DMDControlDLPC350::~DMDControlDLPC350() {
    }

    auto DMDControlDLPC350::Initialize() -> bool {
        if (DLPC350_USB_IsConnected() == 0) {
            if(DLPC350_USB_Open() < 0) {
                Update("Failed to open USB port", DMDErrorCode::FailedToOpenUSBPort);
                return false;
            }
        }

        unsigned char HWStatus, SysStatus, MainStatus;
        if(DLPC350_GetStatus(&HWStatus, &SysStatus, &MainStatus) < 0) {
            Update("Failed to get status", DMDErrorCode::FailedToGetStatus);
            return false;
        }

        unsigned int API_ver, App_ver, SWConfig_ver, SeqConfig_ver;
        if(DLPC350_GetVersion(&App_ver, &API_ver, &SWConfig_ver, &SeqConfig_ver) < 0) {
            Update("Failed to get firmware version", DMDErrorCode::FailedToGetVersion);
            return false;
        }

        const auto version = (QString("APP-%1.%2.%3 API-%4.%5.%6 SW-%7.%8.%9 Sequencer-%10.%11.%12")
                              .arg(App_ver>>24).arg((App_ver << 8) >> 24).arg((App_ver << 16) >> 16)
                              .arg(API_ver>>24).arg((API_ver << 8) >> 24).arg((API_ver << 16) >> 16)
                              .arg(SWConfig_ver>>24).arg((SWConfig_ver << 8) >> 24).arg((SWConfig_ver << 16) >> 16)
                              .arg(SeqConfig_ver>>24).arg((SeqConfig_ver << 8) >> 24).arg((SeqConfig_ver << 16) >> 16));
        SetFirmwareVerision(version);
        Update(version);

        unsigned char firmwareTag[33];
        if(DLPC350_GetFirmwareTagInfo(&firmwareTag[0]) < 0) {
            Update("Failed to get firmware tag", DMDErrorCode::FailedToGetFirmwareTag);
            return false;
        }

        const auto tag = (QString("%1").arg(reinterpret_cast<char*>(firmwareTag)));
        SetFirmwareTag(tag);
        Update(QString("Firmware Tag: %1").arg(tag));

        unsigned int numImgInFlash = 0;
        if(DLPC350_GetNumImagesInFlash(&numImgInFlash) < 0) {
            Update("Failed to get images in flash memory", DMDErrorCode::FailedToGetImageCountInFlash);
            return false;
        }

        Update(QString("Total number of image in flash = %1").arg(numImgInFlash));

        bool standby = true;
        if(DLPC350_GetPowerMode(&standby) < 0) {
            Update("Failed to get power mode", DMDErrorCode::FailedToGetPowerMode);
            return false;
        }

        if (standby) {
            Update("DMD controller is in low power state", DMDErrorCode::StandbyMode);
            return false;
        }

        Update("DMD controller is in normal operation state");

        bool patternMode = true;
        if(DLPC350_GetMode(&patternMode) < 0) {
            Update("Failed to get display mode", DMDErrorCode::FailedToGetDisplayMode);
            return false;
        }

        Update(QString("Display mode : %1").arg(patternMode?"Pattern Display Mode":"Video Display Mode"));
        if(!patternMode) {
            if(DLPC350_SetMode(true) < 0) {
                Update("Failed to set display mode to Pattern Display Mode", DMDErrorCode::FailedToSetDisplayMode);
                return false;
            }

            QThread::msleep(100);

            if(DLPC350_GetMode(&patternMode) < 0) {
                Update("Failed to get display mode", DMDErrorCode::FailedToGetDisplayMode);
                return false;
            }

            Update("Display mode is changed to Pattern Display Mode");
        }

        int trigMode = 0;
        if(DLPC350_GetPatternTriggerMode(&trigMode) < 0) {
            Update("Failed to get trigger mode", DMDErrorCode::FailedToGetTriggerMode);
            return false;
        }

        Update(QString("Trigger mode : %1").arg(trigMode));

        if(trigMode != 1) {
            if (DLPC350_SetPatternTriggerMode(1) < 0) {
                Update("Failed to set trigger mode to Mode 1", DMDErrorCode::FailedToSetTriggerMode);
            }
        }

        bool rgbSource = true;
        if(DLPC350_GetPatternDisplayMode(&rgbSource) < 0) {
            Update("Failed to get Pattern Display Data Input Source", DMDErrorCode::FailedToGetDataInputSource);
            return false;
        }

        Update(QString("Pattern Display Data Input Source : %1").arg(rgbSource?"24bit RGB":"Flash Memory"));
        if(rgbSource) {
            if(DLPC350_SetPatternDisplayMode(false) < 0) {
                Update("Failed to set Pattern Display Data Input Source to Flash Memory", DMDErrorCode::FailedToSetDataInputSource);
                return false;
            }

            Update("Pattern Display Data Input Source is changed to Flash Memory");
        }

        return true;
    }

    auto DMDControlDLPC350::SetBitDepth(uint32_t bitdepth) -> bool {
        d->bitDepth = bitdepth;
        return true;
    }

    auto DMDControlDLPC350::SetExposure(uint32_t exposure, uint32_t period) -> bool {
        d->exposure = exposure;
        d->period = period;
        return true;
    }

    auto DMDControlDLPC350::SetLEDChannel(uint32_t channel, uint32_t intensity) -> bool {
        d->ledChannel = channel;
        const uchar redCurrent = ((channel & 0x01) > 0) ? static_cast<uchar>(intensity&0xFF) : 0;
        const uchar greenCurrent = ((channel & 0x02) > 0) ? static_cast<uchar>(intensity & 0xFF) : 0;
        const uchar blueCurrent = ((channel & 0x04) > 0) ? static_cast<uchar>(intensity & 0xFF) : 0;

        Update(QString("LED Channel=%1 Intensity=%2").arg(channel).arg(intensity));

        if (DLPC350_SetLedCurrents(redCurrent, greenCurrent, blueCurrent) < 0) {
            Update("Failed to set LED intensity", DMDErrorCode::FailedToSetLEDIntensity);
            return false;
        }

        return true;
    }

    auto DMDControlDLPC350::SetLEDChannel(uint32_t redIntensity, uint32_t greenIntensity,
        uint32_t blueIntensity) -> bool {
        const uchar redCurrent = static_cast<uchar>(redIntensity & 0xFF);
        const uchar greenCurrent = static_cast<uchar>(greenIntensity & 0xFF);
        const uchar blueCurrent = static_cast<uchar>(blueIntensity & 0xFF);

        Update(QString("LED Intensity Red=%1 Green=%2 Blue=%3").arg(redCurrent).arg(greenCurrent).arg(blueCurrent));

        if (DLPC350_SetLedCurrents(redCurrent, greenCurrent, blueCurrent) < 0) {
            Update("Failed to set LED intensity", DMDErrorCode::FailedToSetLEDIntensity);
            return false;
        }

        return true;
    }

    auto DMDControlDLPC350::SetSequence(QList<uint32_t> list) -> bool {
        d->imageList = list;
        return true;
    }

    auto DMDControlDLPC350::SetLEDSequence(QList<uint32_t> list) -> bool {
        d->ledList = list;
        return true;
    }

    auto DMDControlDLPC350::SetTriggerType(int32_t type) -> bool {
        d->triggerType = type;
        return true;
    }

    auto DMDControlDLPC350::StartSequence(bool bRepeat) -> bool {

        QList<uint32_t> imagesInFlash;

        DLPC350_ClearPatLut();

        int imageCount = 0;
        foreach(const auto imageIndex, d->imageList) {
            const auto patNum = [=]() {
                const auto patternCount = 24/d->bitDepth;
                return imageIndex%patternCount;
            }();

            const auto triggerType = [=]()->int {
                if (d->triggerType == 0) return d->triggerType;

                if (imageCount == 0) return 1;
                else return 3;
            }();

            const auto imageNum = [=]() {
                const auto patternCount = 24 / d->bitDepth;
                return imageIndex / patternCount;
            }();

            const auto bufSwap = [=]()->bool {
                if (imagesInFlash.size() == 0) return true;
                if (imagesInFlash.last() != imageNum) return true;
                return false;
            }();

            if(DLPC350_AddToPatLut(triggerType,
                                   patNum,
                                   d->bitDepth,
                                   d->ledChannel,
                                   false,
                                   true,
                                   bufSwap,
                                   false
            ) < 0) {
                Update("Failed to add pattern lut", DMDErrorCode::FailedToAddPatternLUT);
                return false;
            }

            if(bufSwap) imagesInFlash.push_back(imageNum);
            imageCount++;
        }

        const uint32_t numLutEntries = d->imageList.size();
        const uint32_t numPatsForTrigOut2 = d->imageList.size();
        const uint32_t numImages = imagesInFlash.size();
        if(DLPC350_SetPatternConfig(numLutEntries, bRepeat, numPatsForTrigOut2, numImages) < 0) {
            Update("Failed to set pattern config", DMDErrorCode::FailedToSetPatternConfig);
            return false;
        }

        Update(QString("LUT Entries=%1 Repeat=%2 Patterns For Trig Out2=%3 Images=%4").arg(numLutEntries).arg(bRepeat).arg(numPatsForTrigOut2).arg(numImages));

        if(DLPC350_SetExposure_FramePeriod(d->exposure, d->period) < 0) {
            Update("Failed to set exposure time", DMDErrorCode::FailedToSetExposureTime);
            return false;
        }
        Update(QString("Exposure=%1us Period=%2us").arg(d->exposure).arg(d->period));

        if(DLPC350_SendPatLut() < 0) {
            Update("Failed to send pattern lut", DMDErrorCode::FailedToSendPatternLUT);
            return false;
        }
        Update("Pattern LUT is sent");

        std::unique_ptr<uchar> splashLut{ new uchar[64] };
        int numSplashLutEntries = 0;
        QString splashString;
        foreach(const auto index, imagesInFlash) {
            splashLut.get()[numSplashLutEntries++] = static_cast<uchar>(index);
            splashString.append(QString("%1 ").arg(index));
        }

        if(DLPC350_SendImageLut(splashLut.get(), numSplashLutEntries) < 0) {
            Update("Failed to send image lut", DMDErrorCode::FailedToSendImageLUT);
            return false;
        }
        Update(QString("Image LUT is sent [%1]").arg(splashString));

        if (DLPC350_StartPatLutValidate()) {
            Update("Failed to start validation", DMDErrorCode::FailedToStartValidation);
            return false;
        }

        uint32_t validationStatus;
        uint32_t tryCount = 0;
        do {
            bool ready;

            if (DLPC350_CheckPatLutValidate(&ready, &validationStatus) < 0) {
                Update("Failed to check validation", DMDErrorCode::FailedToCheckValidation);
                return false;
            }

            if (ready) {
                break;
            } else {
                QThread::msleep(100);
            }

            if (tryCount++ > 5) {
                Update("Validation timeout", DMDErrorCode::ValidationTimeout);
                break;
            }
        } while (true);

        if (validationStatus & BIT0) {
            Update("Validation failure - invalid exposure or period", DMDErrorCode::InvalidExpoureOrPeriod);
            return false;
        }

        if (validationStatus & BIT1) {
            Update("Validation failure - invalid pattern numbers", DMDErrorCode::InvalidPatternNumbers);
        }

        if(DLPC350_PatternDisplay(2) < 0) {
            Update("Start pattern sequence is failed", DMDErrorCode::FailedToStartSequence);
            return false;
        }
        Update("Pattern sequence is started");

        return true;
    }

    auto DMDControlDLPC350::StartLEDSequence(bool bRepeat) -> bool {
        QList<uint32_t> imagesInFlash;

        DLPC350_ClearPatLut();

        const auto imageIndex = d->imageList.at(0);
        int imageCount = 0;
        foreach(const auto ledIndex, d->ledList) {
            const auto patNum = [=]() {
                const auto patternCount = 24 / d->bitDepth;
                return imageIndex % patternCount;
            }();

            const auto triggerType = [=]()->int {
                if (d->triggerType == 0) return d->triggerType;

                if (imageCount == 0) return 1;
                else return 3;
            }();

            const auto imageNum = [=]() {
                const auto patternCount = 24 / d->bitDepth;
                return imageIndex / patternCount;
            }();

            const auto bufSwap = [=]()->bool {
                if (imagesInFlash.size() == 0) return true;
                if (imagesInFlash.last() != imageNum) return true;
                return false;
            }();

            if (DLPC350_AddToPatLut(triggerType,
                                    patNum,
                                    d->bitDepth,
                                    ledIndex,
                                    false,
                                    true,
                                    bufSwap,
                                    false
            ) < 0) {
                Update("Failed to add pattern lut", DMDErrorCode::FailedToAddPatternLUT);
                return false;
            }

            if (bufSwap) imagesInFlash.push_back(imageNum);
            imageCount++;
        }

        const uint32_t numLutEntries = d->ledList.size();
        const uint32_t numPatsForTrigOut2 = d->ledList.size();
        const uint32_t numImages = imagesInFlash.size();
        if (DLPC350_SetPatternConfig(numLutEntries, bRepeat, numPatsForTrigOut2, numImages) < 0) {
            Update("Failed to set pattern config", DMDErrorCode::FailedToSetPatternConfig);
            return false;
        }

        Update(QString("LUT Entries=%1 Repeat=%2 Patterns For Trig Out2=%3 Images=%4").arg(numLutEntries).arg(bRepeat).arg(numPatsForTrigOut2).arg(numImages));

        if (DLPC350_SetExposure_FramePeriod(d->exposure, d->period) < 0) {
            Update("Failed to set exposure time", DMDErrorCode::FailedToSetExposureTime);
            return false;
        }
        Update(QString("Exposure=%1us Period=%2us").arg(d->exposure).arg(d->period));

        if (DLPC350_SendPatLut() < 0) {
            Update("Failed to send pattern lut", DMDErrorCode::FailedToSendPatternLUT);
            return false;
        }
        Update("Pattern LUT is sent");

        std::unique_ptr<uchar> splashLut{ new uchar[64] };
        int numSplashLutEntries = 0;
        QString splashString;
        foreach(const auto index, imagesInFlash) {
            splashLut.get()[numSplashLutEntries++] = static_cast<uchar>(index);
            splashString.append(QString("%1 ").arg(index));
        }

        if (DLPC350_SendImageLut(splashLut.get(), numSplashLutEntries) < 0) {
            Update("Failed to send image lut", DMDErrorCode::FailedToSendImageLUT);
            return false;
        }
        Update(QString("Image LUT is sent [%1]").arg(splashString));

        if (DLPC350_StartPatLutValidate()) {
            Update("Failed to start validation", DMDErrorCode::FailedToStartValidation);
            return false;
        }

        uint32_t validationStatus;
        uint32_t tryCount = 0;
        do {
            bool ready;

            if (DLPC350_CheckPatLutValidate(&ready, &validationStatus) < 0) {
                Update("Failed to check validation", DMDErrorCode::FailedToCheckValidation);
                return false;
            }

            if (ready) {
                break;
            } else {
                QThread::msleep(100);
            }

            if (tryCount++ > 5) {
                Update("Validation timeout", DMDErrorCode::ValidationTimeout);
                break;
            }
        } while (true);

        if (validationStatus & BIT0) {
            Update("Validation failure - invalid exposure or period", DMDErrorCode::InvalidExpoureOrPeriod);
            return false;
        }

        if (validationStatus & BIT1) {
            Update("Validation failure - invalid pattern numbers", DMDErrorCode::InvalidPatternNumbers);
        }

        if (DLPC350_PatternDisplay(2) < 0) {
            Update("Start pattern sequence is failed", DMDErrorCode::FailedToStartSequence);
            return false;
        }
        Update("Pattern sequence is started");

        return true;
    }

    auto DMDControlDLPC350::StopSequence() -> bool {
        if(DLPC350_PatternDisplay(0) < 0) {
            Update("Stop pattern sequence is failed", DMDErrorCode::FailedToStopSequence);
            return false;
        }
        Update("Pattern sequence is stopped");
        return true;
    }
}
