#include <QFile>
#include <QSettings>

#include "LutConfigIO.h"

namespace TC::DMDControl {

    auto LutConfigIO::Load(const QString& path, LutConfig& config) -> bool {
        if(!QFile::exists(path)) return false;

        QSettings qs(path, QSettings::IniFormat);
        config.ImageIndex.start = qs.value("Image Index/Start", 0).toUInt();
        config.ImageIndex.end = qs.value("Image Index/End", 0).toUInt();
        config.Timing.exposure = qs.value("Timing/Exposure", 4000).toUInt();
        config.Timing.period = qs.value("Timing/Period", 7000).toUInt();
        config.TriggerType = qs.value("Trigger Type", 0).toUInt();
        config.LedChannel = qs.value("LED Channel", 0).toUInt();
        config.LedIntensity = qs.value("LED Intensity", 0).toUInt();
        config.RepeatMode = qs.value("Repeat Mode").toUInt();

        return true;
    }

    auto LutConfigIO::Save(const QString& path, const LutConfig& config) -> bool {
        QSettings qs(path, QSettings::IniFormat);
        qs.setValue("Image Index/Start", config.ImageIndex.start);
        qs.setValue("Image Index/End", config.ImageIndex.end);
        qs.setValue("Timing/Exposure", config.Timing.exposure);
        qs.setValue("Timing/Period", config.Timing.period);
        qs.setValue("Trigger Type", config.TriggerType);
        qs.setValue("LED Channel", config.LedChannel);
        qs.setValue("Led Intensity", config.LedIntensity);
        qs.setValue("Repeat Mode", config.RepeatMode);
        return true;
    }
}
