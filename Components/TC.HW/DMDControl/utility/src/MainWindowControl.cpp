#include <QDir>

#include <DMDControlFactory.h>

#include "LutConfigIO.h"
#include "MainWindow.h"
#include "MainWindowControl.h"

namespace TC::DMDControl {
    struct MainWindowControl::Impl {
        QString configPath;
        uint32_t index{ 1 };
        LutConfig config;
        bool running{ false };
        DMDControl::Pointer dmdControl{ nullptr };
    };

    MainWindowControl::MainWindowControl(const QString& configPath) : d{ new Impl } {
        d->configPath = configPath;
        if(!QFile::exists(d->configPath)) {
            QDir().mkpath(d->configPath);
        }

        LoadConfig(d->index, d->config);

        d->dmdControl = DMDControlFactory::Create(DMDControlFactory::Controller::DLPC350);
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::Initialize() -> bool {
        if(!d->dmdControl->Initialize()) return false;
        return d->dmdControl->SetBitDepth(6);
    }

    auto MainWindowControl::ChangeIndex(uint32_t index) -> bool {
        d->index = index;
        return LoadConfig(index, d->config);
    }

    auto MainWindowControl::GetLutConfig() const -> LutConfig {
        return d->config;
    }

    auto MainWindowControl::StartSequence(const LutConfig& config) -> bool {
        d->running = true;
        d->dmdControl->SetExposure(config.Timing.exposure, config.Timing.period);
        d->dmdControl->SetLEDChannel(config.LedChannel, config.LedIntensity);
        d->dmdControl->SetTriggerType(config.TriggerType);

        QList<uint32_t> list;
        for(auto idx=config.ImageIndex.start; idx<=config.ImageIndex.end; idx++) {
            list.push_back(idx);
        }
        d->dmdControl->SetSequence(list);

        if(!d->dmdControl->StartSequence(config.RepeatMode==LutConfig::RepeatModeEnum::Repeat)) {
            return false;
        }

        return SaveConfig(d->index, config);
    }

    auto MainWindowControl::StopSequence() -> bool {
        d->running = false;
        return d->dmdControl->StopSequence();
    }

    auto MainWindowControl::IsRunning() const -> bool {
        return d->running;
    }

    auto MainWindowControl::GetFirmwareTag() const -> QString {
        return d->dmdControl->GetFirmwareTag();
    }

    auto MainWindowControl::LoadConfig(uint32_t index, LutConfig& config) -> bool {
        const auto path = QString("%1/%2.ini").arg(d->configPath).arg(index);
        return LutConfigIO::Load(path, config);
    }

    auto MainWindowControl::SaveConfig(uint32_t index, const LutConfig& config) -> bool {
        const auto path = QString("%1/%2.ini").arg(d->configPath).arg(index);
        return LutConfigIO::Save(path, config);
    }
}
