#pragma once
#include <QString>
#include "LutConfig.h"

namespace TC::DMDControl {
    class LutConfigIO {
    public:
        static auto Load(const QString& path, LutConfig& config)->bool;
        static auto Save(const QString& path, const LutConfig& config)->bool;
    };
}