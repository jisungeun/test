#pragma once
#include <memory>
#include <QString>

#include "LutConfig.h"

namespace TC::DMDControl {
    class MainWindowControl {
    public:
        MainWindowControl(const QString& configPath);
        ~MainWindowControl();

        auto Initialize()->bool;
        auto ChangeIndex(uint32_t index)->bool;
        auto GetLutConfig() const->LutConfig;
        auto StartSequence(const LutConfig& config)->bool;
        auto StopSequence()->bool;
        auto IsRunning() const->bool;
        auto GetFirmwareTag() const->QString;

    protected:
        auto LoadConfig(uint32_t index, LutConfig& config)->bool;
        auto SaveConfig(uint32_t index, const LutConfig& config)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
