#pragma once
#include <memory>

namespace TC::DMDControl {
    struct LutConfig {
    public:
        enum TriggerTypeEnum {
            Free = 0,
            External = 1
        };

        enum RepeatModeEnum {
            Once = 0,
            Repeat = 1
        };

    public:
        struct {
            uint32_t start{ 0 };
            uint32_t end{ 0 };
        } ImageIndex;

        struct {
            uint32_t exposure{ 4000 };
            uint32_t period{ 7000 };
        } Timing;

        uint32_t TriggerType{ TriggerTypeEnum::Free };
        uint32_t LedChannel{ 0 };
        uint32_t LedIntensity{ 0 };
        uint32_t RepeatMode{ RepeatModeEnum::Once };
    };
}
