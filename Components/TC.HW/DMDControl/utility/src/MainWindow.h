#pragma once
#include <memory>

#include "LutConfig.h"
#include "QMainWindow"

namespace TC::DMDControl {
    class MainWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

    protected:
		auto UpdateGui()->void;
		auto UpdateConfig()->LutConfig;

	protected slots:
		void onOpenFolder();
		void onStartClicked();

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}