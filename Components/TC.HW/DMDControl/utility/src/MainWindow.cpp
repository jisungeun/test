#define LOGGER_TAG "[UI]"
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>

#include <TCLogger.h>

#include "ui_mainwindow.h"
#include "Version.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace TC::DMDControl {
    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };
        MainWindowControl* control;
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui = new Ui::MainWindow();
        d->ui->setupUi(this);
        const auto configPath = QString("%1/config").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        d->control = new MainWindowControl(configPath);

        connect(d->ui->actionOpen, SIGNAL(triggered()), this, SLOT(onOpenFolder()));
        connect(d->ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
        connect(d->ui->startButton, SIGNAL(clicked()), this, SLOT(onStartClicked()));

        UpdateGui();

        if(!d->control->Initialize()) {
            QMessageBox::warning(this, "Initialization", "Initialization is failed");
            setDisabled(true);
        }

        const auto firmwareTag = d->control->GetFirmwareTag();

        setWindowTitle(QString("HTX DMD Console %1 (%2)").arg(PROJECT_REVISION).arg(firmwareTag));
    }
    
    MainWindow::~MainWindow() {
        delete d->control;
    }

    auto MainWindow::UpdateGui() -> void {
        auto config = d->control->GetLutConfig();
        d->ui->imageIndexStart->setValue(config.ImageIndex.start);
        d->ui->imageIndexEnd->setValue(config.ImageIndex.end);
        d->ui->timingExposure->setValue(config.Timing.exposure);
        d->ui->timingPeriod->setValue(config.Timing.period);

        if(config.TriggerType == LutConfig::TriggerTypeEnum::Free) {
            d->ui->triggerFree->setChecked(true);
        } else {
            d->ui->triggerExternal->setChecked(true);
        }

        d->ui->ledChannel->setValue(config.LedChannel);
        d->ui->ledIntensity->setValue(config.LedIntensity);

        if(config.RepeatMode == LutConfig::RepeatModeEnum::Once) {
            d->ui->repeatOnce->setChecked(true);
        } else {
            d->ui->repeatEndless->setChecked(true);
        }
    }

    auto MainWindow::UpdateConfig() -> LutConfig {
        LutConfig config;
        config.ImageIndex.start = d->ui->imageIndexStart->value();
        config.ImageIndex.end = d->ui->imageIndexEnd->value();
        config.Timing.exposure = d->ui->timingExposure->value();
        config.Timing.period = d->ui->timingPeriod->value();

        if(d->ui->triggerFree->isChecked()) {
            config.TriggerType = LutConfig::TriggerTypeEnum::Free;
        } else {
            config.TriggerType = LutConfig::TriggerTypeEnum::External;
        }

        config.LedChannel = d->ui->ledChannel->value();
        config.LedIntensity = d->ui->ledIntensity->value();

        if(d->ui->repeatOnce->isChecked()) {
            config.RepeatMode = LutConfig::RepeatModeEnum::Once;
        } else {
            config.RepeatMode = LutConfig::RepeatModeEnum::Repeat;
        }

        return config;
    }

    void MainWindow::onOpenFolder() {
        const auto configPath = QString("%1/config").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        QDesktopServices::openUrl(QUrl(QString("file:///%1").arg(configPath), QUrl::TolerantMode));
    }

    void MainWindow::onStartClicked() {
        if(d->control->IsRunning()) {
            if(!d->control->StopSequence()) {
                QMessageBox::warning(this, "Stop sequence", "Failed to stop sequence");
            } else {
                d->ui->startButton->setText("Start");
            }
        } else {
            auto config = UpdateConfig();
            if(!d->control->StartSequence(config)) {
                QMessageBox::warning(this, "Start sequence", "Failed to start sequence");
            } else {
                d->ui->startButton->setText("Stop");
            }
        }
    }
}
