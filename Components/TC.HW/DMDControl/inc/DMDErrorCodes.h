#pragma once

#include <enum.h>

namespace TC::DMDControl {
    BETTER_ENUM(DMDErrorCode, uint32_t,
                NoError                             = 0,
                FailedToGetStatus                   = 1,
                FailedToGetDisplayMode              = 10,
                FailedToSetDisplayMode              = 11,
                FailedToGetTriggerMode              = 12,
                FailedToSetTriggerMode              = 13,
                FailedToGetDataInputSource          = 14,
                FailedToSetDataInputSource          = 15,
                FailedToSetLEDIntensity             = 16,

                //Initialization related errors
                FailedToOpenUSBPort                 = 100,
                FailedToGetVersion                  = 101,
                FailedToGetFirmwareTag              = 102,
                FailedToGetImageCountInFlash        = 103,
                FailedToGetPowerMode                = 104,
                StandbyMode                         = 105,

                //Sequence
                FailedToSetPatternConfig            = 200,
                FailedToSetExposureTime             = 201,
                FailedToAddPatternLUT               = 202,
                FailedToSendPatternLUT              = 203,
                FailedToSendImageLUT                = 204,
                FailedToStartSequence               = 205,
                FailedToStopSequence                = 206,

                //Validation
                FailedToStartValidation             = 300,
                FailedToCheckValidation             = 301,
                ValidationTimeout                   = 302,

                InvalidExpoureOrPeriod              = 350,
                InvalidPatternNumbers               = 351,

                //Last error code
                UnknownError                        = 10000
    )
}