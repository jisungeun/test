#pragma once

#include "DMDControl.h"
#include "TCDMDControlExport.h"

namespace TC::DMDControl {
    class TCDMDControl_API DMDControlFactory {
    public:
        enum class Controller {
            DLPC350
        };

        static auto Create(Controller type = Controller::DLPC350)->DMDControl::Pointer;
    };
}