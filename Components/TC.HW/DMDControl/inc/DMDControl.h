#pragma once
#include <memory>

#include <QString>
#include <QList>

#include "DMDErrorCodes.h"
#include "TCDMDControlExport.h"

namespace TC::DMDControl {
    class TCDMDControl_API DMDControl {
    public:
        typedef std::shared_ptr<DMDControl> Pointer;

    public:
        DMDControl();
        virtual ~DMDControl();

        auto GetLastError() const->DMDErrorCode;
        auto GetFirmwareVersion() const->QString;
        auto GetFirmwareTag() const->QString;

        virtual auto Initialize()->bool = 0;
        virtual auto SetBitDepth(uint32_t bitdepth)->bool = 0;
        virtual auto SetExposure(uint32_t exposure, uint32_t period)->bool = 0;
        virtual auto SetLEDChannel(uint32_t channel, uint32_t intensity)->bool = 0;
        virtual auto SetLEDChannel(uint32_t redIntensity, uint32_t greenIntensity, uint32_t blueIntensity)->bool = 0;
        virtual auto SetSequence(QList<uint32_t> list)->bool = 0;
        virtual auto SetLEDSequence(QList<uint32_t> list)->bool = 0;
        virtual auto SetTriggerType(int32_t type)->bool = 0;
        virtual auto StartSequence(bool bRepeat)->bool = 0;
        virtual auto StartLEDSequence(bool bRepeat)->bool = 0;
        virtual auto StopSequence(void)->bool = 0;

    protected:
        auto SetLastError(DMDErrorCode code)->void;
        auto SetFirmwareVerision(const QString& version)->void;
        auto SetFirmwareTag(const QString& tag)->void;

        auto Update(const QString& message, DMDErrorCode code = DMDErrorCode::NoError)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}