#pragma once
#include <memory>

#include <QByteArray>

#include "MotionControl.h"

namespace TC::MotionControl::TMCM6214 {
    class MotionControlTMCM6214 : public MotionControl {
    public:
        MotionControlTMCM6214(const MotionConfig& config);
        virtual ~MotionControlTMCM6214();

        auto Initialize() -> bool override;
        auto CleanUp() -> void override;
        auto Homing() -> bool override;
        auto Move(Axis axis, int64_t position) -> bool override;
        auto MoveXY(int64_t xposition, int64_t yposition) -> bool override;
        auto ZScan(const ZScanParameter& param) -> bool override;
        auto ZScanFL(const ZScanParameter& param) -> bool override;
        auto RaiseSingleTrigger() -> bool override;
        auto StartJog(Axis axis, int64_t position) -> bool override;
        auto StopJog(Axis axis) -> bool override;
        auto GetPosition(Axis axis) -> int64_t override;
        auto GetEncoderPosition(Axis axis)->int64_t override;
        auto GetFirmwareTag()->QString override;
        auto SetGlobalParameter(int32_t addr, int32_t value) -> bool override;
        auto GetGlobalParameter(int32_t addr, int32_t& value) -> bool override;
        auto RunProgram(int32_t value) -> bool override;

    private:
        auto PerformZHoming()->bool;
        auto PerformCHoming()->bool;
        auto PerformFHoming()->bool;
        auto PerformXYHoming()->bool;
        auto PerformMove(Axis axis, int32_t relPosition)->bool;
        auto PerformMoveXY(int32_t xRelPosition, int32_t yRelPosition)->bool;
        auto PerformStartJog(Axis axis, int32_t relPosition)->bool;
        auto PerformStopJog(Axis axis)->bool;
        auto PerformZScan(uint32_t slices, int32_t step, int32_t relBefore, int32_t relAfter)->bool;
        auto PerformZScanFL(uint32_t slices, int32_t step, int32_t relBefore, int32_t relAfter, int32_t flExposure)->bool;
        auto PerformSingleTrigger()->bool;
        auto PerformReadFirmwareTag()->QString;

        auto GetActualPosition(Axis axis, int32_t& position)->bool;
        auto ResetActualPosition(Axis axis)->bool;
        auto GetEncoderPosition(Axis axis, int32_t& position)->bool;
        auto ResetEncoderPosition(Axis axis)->bool;
        auto GetTargetPosition(Axis axis, int32_t& position)->bool;
        auto ResetTargetPosition(Axis axis)->bool;
        auto SetUserVariable(uint8_t address, int32_t value)->bool;
        auto Send(uint8_t address, uint8_t command, uint8_t type, uint8_t motor, int32_t value, QByteArray& response, uint32_t waitTimeout = 300)->bool;
        auto CheckStatus(const QByteArray& response) const->uint32_t;
        auto CheckValue(const QByteArray& response) const->int32_t;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
