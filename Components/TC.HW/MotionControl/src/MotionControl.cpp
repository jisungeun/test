#define LOGGER_TAG "[Motion]"

#include <QMap>

#include <TCLogger.h>
#include "MotionControl.h"

namespace TC::MotionControl {
    struct MotionControl::Impl {
        MotionConfig config;
        MotionErrorCode lastError{ MotionErrorCode::NoError };

        QMap<Axis, MotionConfig::AxisIndex> toAxisIndex = {
            {Axis::XAxis, MotionConfig::AxisIndex::XAxisIndex},
            {Axis::YAxis, MotionConfig::AxisIndex::YAxisIndex},
            {Axis::ZAxis, MotionConfig::AxisIndex::ZAxisIndex},
            {Axis::CAxis, MotionConfig::AxisIndex::CAxisIndex},
            {Axis::FAxis, MotionConfig::AxisIndex::FAxisIndex},
        };
    };

    MotionControl::MotionControl(const MotionConfig& config) : d{ new Impl } {
        d->config = config;
    }

    MotionControl::~MotionControl() {
    }

    auto MotionControl::SetConfig(const MotionConfig& config) -> void {
        d->config = config;
    }

    auto MotionControl::GetConfig() const -> MotionConfig {
        return d->config;
    }

    auto MotionControl::Convert(Axis axis, double position) const -> int64_t {
        const auto axisInfo = d->config.axisInfo[d->toAxisIndex[axis]];
        return static_cast<int64_t>(axisInfo.pulses_per_mm * position / 1000.0);
    }

    auto MotionControl::Convert(Axis axis, uint64_t position) const -> double {
        const auto axisInfo = d->config.axisInfo[d->toAxisIndex[axis]];
        return static_cast<double>(axisInfo.um_per_1k_pulse * position) / 1000.0;
    }

    auto MotionControl::GetLastError() const -> MotionErrorCode {
        return d->lastError;
    }

    auto MotionControl::SetLastError(MotionErrorCode code) -> void {
        d->lastError = code;
    }

    auto MotionControl::Update(const QString& message, MotionErrorCode code) -> void {
        if (code == +MotionErrorCode::NoError) {
            QLOG_INFO() << message;
        } else {
            SetLastError(code);
            QLOG_ERROR() << message << "[Error=" << code << "]";
        }
    }
}
