#define LOGGER_TAG "[Motion]"

#include <QByteArray>
#include <QSerialPort>
#include <QElapsedTimer>
#include <QThread>
#include <QSerialPortInfo>

#include <TCLogger.h>

#include "MotionControlTMCM6214.h"

namespace TC::MotionControl::TMCM6214 {
    auto ResponseToString(const QByteArray& response)->QString;
    auto FindTMCMPort()->QString;

    enum TMCLCommand {
        MST = 3,
        MVP = 4,
        SAP = 5,
        GAP = 6,
        SGP = 9,
        GGP = 10,
        RFS = 13,
        RUN = 129
    };

    enum TMCLStatus {
        NoError = 100,
        ProgramLoaded = 101,
        WrongChecksum = 1,
        InvalidCommand = 2,
        WrongType = 3,
        InvalidValue = 4,
        EEPROMLocked = 5,
        WrongCommand = 6
    };

    struct MotionControlTMCM6214::Impl {
        MotionConfig config;
        std::shared_ptr<QSerialPort> port;
        uint8_t id{ 1 };        //module id

        QMap<Axis, MotionConfig::AxisIndex> axis2index = {
            {Axis::XAxis, MotionConfig::AxisIndex::XAxisIndex},
            {Axis::YAxis, MotionConfig::AxisIndex::YAxisIndex},
            {Axis::ZAxis, MotionConfig::AxisIndex::ZAxisIndex},
            {Axis::CAxis, MotionConfig::AxisIndex::CAxisIndex},
            {Axis::FAxis, MotionConfig::AxisIndex::FAxisIndex}
        };

        inline auto axisInfo(Axis axis)->MotionConfig::AxisInfo {
            return config.axisInfo[axis2index[axis]];
        }

        inline auto axis2tmclaxis(Axis axis)->uint8_t {
            auto axisIndex = axis2index[axis];
            return config.axisInfo[axisIndex].axisIndex;
        };

        inline auto hasEncoder(Axis axis)->bool {
            auto axisIndex = axis2index[axis];
            return config.axisInfo[axisIndex].has_encoder;
        }
    };

    MotionControlTMCM6214::MotionControlTMCM6214(const MotionConfig& config) : MotionControl(config), d{ new Impl }{
        d->config = config;
    }

    MotionControlTMCM6214::~MotionControlTMCM6214() {
    }

    auto MotionControlTMCM6214::Initialize() -> bool {
        if (!d->config.valid) {
            Update("Invalid configuration", MotionErrorCode::InvalidConfiguration);
            return false;
        }

        d->port.reset(new QSerialPort());

        QString comport = FindTMCMPort();
        if (comport.isEmpty()) {
            comport = QString("COM%1").arg(d->config.serialPort.port);
            Update(QString("Not found COM port. Try %1").arg(comport));
        } else {
            Update(QString("Found COM Port=%1").arg(comport));
        }
        
        d->port->setPortName(comport);
        d->port->setBaudRate(d->config.serialPort.baudrate);
        d->port->setParity(QSerialPort::Parity::NoParity);
        d->port->setStopBits(QSerialPort::StopBits::OneStop);
        d->port->setDataBits(QSerialPort::DataBits::Data8);
        d->port->setReadBufferSize(100);

        if (!d->port->open(QIODevice::ReadWrite)) {
            Update(QString("Failed to open port COM%1").arg(d->config.serialPort.port), MotionErrorCode::FailedToOpenPort);
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::CleanUp() -> void {
        if (d->port->isOpen()) {
            d->port->close();
        }
        d->port = nullptr;
    }

    auto MotionControlTMCM6214::Homing() -> bool {
        if (!PerformZHoming()) return false;
        if (!PerformCHoming()) return false;
        if (!PerformXYHoming()) return false;
        if (!PerformFHoming()) return false;
        return true;
    }

    auto MotionControlTMCM6214::Move(Axis axis, int64_t position) -> bool {
        return PerformMove(axis, position);
    }

    auto MotionControlTMCM6214::MoveXY(int64_t xposition, int64_t yposition) -> bool {
        return PerformMoveXY(xposition, yposition);
    }

    auto MotionControlTMCM6214::ZScan(const ZScanParameter& param) -> bool {
        int32_t startPos{ 0 };
        if(!GetActualPosition(Axis::ZAxis, startPos)) {
            Update("Failed to get Z axis position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        int32_t relBefore = param.startPos - startPos;
        Update(QString("startPos=%1 currentZ=%2 relBefore=%3").arg(param.startPos).arg(startPos).arg(relBefore));
        return PerformZScan(param.sliceCount, param.sliceSpacing, relBefore, param.relativeLastMove);
    }

    auto MotionControlTMCM6214::ZScanFL(const ZScanParameter& param) -> bool {
        int32_t startPos{ 0 };
        if(!GetActualPosition(Axis::ZAxis, startPos)) {
            Update("Failed to get Z axis position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        int32_t relBefore = param.startPos - startPos;
        Update(QString("startPos=%1 currentZ=%2 relBefore=%3").arg(param.startPos).arg(startPos).arg(relBefore));
        return PerformZScanFL(param.sliceCount, param.sliceSpacing, relBefore, param.relativeLastMove, param.flExposure);
    }

    auto MotionControlTMCM6214::RaiseSingleTrigger() -> bool {
        return PerformSingleTrigger();
    }

    auto MotionControlTMCM6214::StartJog(Axis axis, int64_t position) -> bool {
        return PerformStartJog(axis, position);
    }

    auto MotionControlTMCM6214::StopJog(Axis axis) -> bool {
        return PerformStopJog(axis);
    }

    auto MotionControlTMCM6214::GetPosition(Axis axis) -> int64_t {
        int32_t position;
        if (!GetActualPosition(axis, position)) return 0;
        return position;
    }

    auto MotionControlTMCM6214::GetEncoderPosition(Axis axis) -> int64_t {
        int32_t position;
        if (!GetEncoderPosition(axis, position)) return 0;
        return position;
    }

    auto MotionControlTMCM6214::GetFirmwareTag() -> QString {
        return PerformReadFirmwareTag();
    }

    auto MotionControlTMCM6214::SetGlobalParameter(int32_t addr, int32_t value) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::SGP, addr, 2, value, response)) return false;
        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to set global parameter [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToSetGlobalParameter);
        }

        return true;
    }

    auto MotionControlTMCM6214::GetGlobalParameter(int32_t addr, int32_t& value) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::GGP, addr, 2, 0, response)) {
            Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update("Failed to get global variable", MotionErrorCode::FailedToGetGlobalParameter);
        }

        value = CheckValue(response);

        return true;
    }

    auto MotionControlTMCM6214::RunProgram(int32_t value) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::RUN, 1, 0, value, response)) {
            Update(QString("Failed to start a program (%1)").arg(value), MotionErrorCode::FailedToRunProgram);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start program (%1) [Status=%2]").arg(value).arg(CheckStatus(response)), MotionErrorCode::FailedToRunProgram);
            return false;
        }
    }

    auto MotionControlTMCM6214::PerformZHoming() -> bool {
        QByteArray response;

        Update("Start Z axis homing");

        if (!Send(d->id, TMCLCommand::RFS, 0, d->axis2tmclaxis(Axis::ZAxis), 0, response)) {
            Update("Failed to start Z axis homing", MotionErrorCode::FailedToStartZHoming);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start Z axis homing [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartZHoming);
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto homeTimeout = true;
        const auto timeout = (d->config.axisInfo[MotionConfig::ZAxisIndex].homing_timeout * 1000);
        while (timer.elapsed() < timeout) {
            if (!Send(d->id, TMCLCommand::RFS, 2, d->axis2tmclaxis(Axis::ZAxis), 0, response)) {
                Update("Failed to check Z axis homing status", MotionErrorCode::FailedToCheckZHomingStatus);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 0)) {
                homeTimeout = false;
                break;
            }

            QThread::msleep(200);
        }

        if (homeTimeout) {
            Update(QString("Z Homing timeout [elapsed=%1msec] [timeout=%2msec]").arg(timer.elapsed()).arg(timeout), MotionErrorCode::ZHomingTimeout);
            return false;
        }

        if (!PerformMove(Axis::ZAxis, 1000)) {
            Update("Failed to move Z axis to the origin", MotionErrorCode::FailedToMoveZAxisToOrigin);
            return false;
        }

        if (!ResetActualPosition(Axis::ZAxis)) {
            Update("Failed to reset Z axis origin (actual position)", MotionErrorCode::FailedToResetZAxisOrigin);
            return false;
        }

        if (!ResetTargetPosition(Axis::ZAxis)) {
            Update("Failed to reset Z axis origin (target position)", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if (!ResetEncoderPosition(Axis::ZAxis)) {
            Update("Failed to reset Z axis origin (encoder position)", MotionErrorCode::FailedToResetZAxisEncoderOrigin);
            return false;
        }

        Update("Z axis homing is completed");

        return true;
    }

    auto MotionControlTMCM6214::PerformCHoming() -> bool {
        QByteArray response;

        Update("Start C axis homing");

        if (!Send(d->id, TMCLCommand::RFS, 0, d->axis2tmclaxis(Axis::CAxis), 0, response)) {
            Update("Failed to start C axis homing", MotionErrorCode::FailedToStartCHoming);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start C axis homing [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartCHoming);
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto homeTimeout = true;
        const auto timeout = (d->config.axisInfo[MotionConfig::CAxisIndex].homing_timeout * 1000);
        while (timer.elapsed() < timeout) {
            if (!Send(d->id, TMCLCommand::RFS, 2, d->axis2tmclaxis(Axis::CAxis), 0, response)) {
                Update("Failed to check C axis homing status", MotionErrorCode::FailedToCheckCHomingStatus);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 0)) {
                homeTimeout = false;
                break;
            }

            QThread::msleep(200);
        }

        if (homeTimeout) {
            Update(QString("C Homing timeout [elapsed=%1msec] [timeout=%2msec]").arg(timer.elapsed()).arg(timeout), MotionErrorCode::CHomingTimeout);
            return false;
        }

        if (!PerformMove(Axis::CAxis, 1000)) {
            Update("Failed to move C axis to the origin", MotionErrorCode::FailedToMoveCAxisToOrigin);
            return false;
        }

        if (!ResetActualPosition(Axis::CAxis)) {
            Update("Failed to reset C axis origin (actual position)", MotionErrorCode::FailedToResetCAxisOrigin);
            return false;
        }

        if (!ResetTargetPosition(Axis::CAxis)) {
            Update("Failed to reset C axis origin (target position)", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if (!ResetEncoderPosition(Axis::CAxis)) {
            Update("Failed to reset C axis origin (encoder position)", MotionErrorCode::FailedToResetCAxisEncoderOrigin);
            return false;
        }

        Update("C axis homing is completed");

        return true;
    }

    auto MotionControlTMCM6214::PerformFHoming() -> bool {
        QByteArray response;

        Update("Start F axis homing");

        if (!Send(d->id, TMCLCommand::RFS, 0, d->axis2tmclaxis(Axis::FAxis), 0, response)) {
            Update("Failed to start F axis homing", MotionErrorCode::FailedToStartFHoming);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start F axis homing [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartFHoming);
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto homeTimeout = true;
        const auto timeout = (d->config.axisInfo[MotionConfig::FAxisIndex].homing_timeout * 1000);
        while (timer.elapsed() < timeout) {
            if (!Send(d->id, TMCLCommand::RFS, 2, d->axis2tmclaxis(Axis::FAxis), 0, response)) {
                Update("Failed to check F axis homing status", MotionErrorCode::FailedToCheckFHomingStatus);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 0)) {
                homeTimeout = false;
                break;
            }

            QThread::msleep(200);
        }

        if (homeTimeout) {
            Update(QString("F Homing timeout [elapsed=%1msec] [timeout=%2msec]").arg(timer.elapsed()).arg(timeout), MotionErrorCode::FHomingTimeout);
            return false;
        }

        if (!PerformMove(Axis::FAxis, 1000)) {
            Update("Failed to move F axis to the origin", MotionErrorCode::FailedToMoveFAxisToOrigin);
            return false;
        }

        if (!ResetActualPosition(Axis::FAxis)) {
            Update("Failed to reset F axis origin (actual position)", MotionErrorCode::FailedToResetFAxisOrigin);
            return false;
        }

        if (!ResetTargetPosition(Axis::FAxis)) {
            Update("Failed to reset F axis origin (target position)", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if (!ResetEncoderPosition(Axis::FAxis)) {
            Update("Failed to reset F axis origin (encoder position)", MotionErrorCode::FailedToResetFAxisEncoderOrigin);
            return false;
        }

        Update("F axis homing is completed");

        return true;
    }

    auto MotionControlTMCM6214::PerformXYHoming() -> bool {
        QByteArray response;

        Update("Start XY axis homing");

        if (!Send(d->id, TMCLCommand::RFS, 0, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
            Update("Failed to start X axis homing", MotionErrorCode::FailedToStartXHoming);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start X axis homing [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartXHoming);
            return false;
        }

        if (!Send(d->id, TMCLCommand::RFS, 0, d->axis2tmclaxis(Axis::YAxis), 0, response)) {
            Update("Failed to start X axis homing", MotionErrorCode::FailedToStartYHoming);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start Y axis homing [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartYHoming);
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto homeTimeout = true;
        const auto timeout = std::max<uint32_t>(d->config.axisInfo[MotionConfig::XAxisIndex].homing_timeout,
            d->config.axisInfo[MotionConfig::YAxisIndex].homing_timeout);
        while (timer.elapsed() < (timeout * 1000)) {
            QThread::msleep(200);

            if (!Send(d->id, TMCLCommand::RFS, 2, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
                Update("Failed to check X axis homing status", MotionErrorCode::FailedToCheckXHomingStatus);
                return false;
            }

            if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 0)) {
                continue;
            }

            if (!Send(d->id, TMCLCommand::RFS, 2, d->axis2tmclaxis(Axis::YAxis), 0, response)) {
                Update("Failed to check X axis homing status", MotionErrorCode::FailedToCheckYHomingStatus);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 0)) {
                homeTimeout = false;
                break;
            }
        }

        if (homeTimeout) {
            Update("XY Homing timeout", MotionErrorCode::XYHomingTimeout);
            return false;
        }

        if (!PerformMove(Axis::XAxis, 1000)) {
            Update("Failed to move X axis to the origin", MotionErrorCode::FailedToMoveXAxisToOrigin);
            return false;
        }

        if (!ResetActualPosition(Axis::XAxis)) {
            Update("Failed to reset X axis origin (actual position)", MotionErrorCode::FailedToResetXAxisOrigin);
            return false;
        }

        if (!ResetTargetPosition(Axis::XAxis)) {
            Update("Failed to reset x axis origin (target position)", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if (!ResetEncoderPosition(Axis::XAxis)) {
            Update("Failed to reset X axis origin (encoder position)", MotionErrorCode::FailedToResetXAxisEncoderOrigin);
            return false;
        }

        if (!PerformMove(Axis::YAxis, 1000)) {
            Update("Failed to move Y axis to the origin", MotionErrorCode::FailedToMoveYAxisToOrigin);
            return false;
        }

        if (!ResetActualPosition(Axis::YAxis)) {
            Update("Failed to reset Y axis origin (actual position)", MotionErrorCode::FailedToResetYAxisOrigin);
            return false;
        }

        if (!ResetTargetPosition(Axis::YAxis)) {
            Update("Failed to reset Y axis origin (target position)", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if (!ResetEncoderPosition(Axis::YAxis)) {
            Update("Failed to reset Y axis origin (encoder position)", MotionErrorCode::FailedToResetYAxisEncoderOrigin);
            return false;
        }

        Update("XY axis homing is completed");

        return true;
    }

    auto MotionControlTMCM6214::PerformMove(Axis axis, int32_t relPosition) -> bool {
        QByteArray response;

        Update(QString("Move %1 axis to %2").arg(axis).arg(relPosition));

        const auto tmclAxis = d->axis2tmclaxis(axis);

        if (!Send(d->id, TMCLCommand::MVP, 1, tmclAxis, relPosition, response)) {
            Update("Failed to start the axis moving", MotionErrorCode::FailedToStartMoving);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start the axis moving [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartMoving);
            return false;
        }


        auto moveTimeout = true;
        auto timeout = std::max<uint32_t>(2000, d->axisInfo(axis).moving_timeout_per_10k_pulse * std::abs(relPosition) / 10000.0 * 1000);

        QElapsedTimer timer;
        timer.start();
        while (timer.elapsed() < timeout) {
            QThread::msleep(100);

            if (relPosition > 0) {
                if (!Send(d->id, TMCLCommand::GAP, 10, tmclAxis, 0, response)) {
                    Update("Failed to check right limit", MotionErrorCode::FailedToCheckRightLimit);
                    return false;
                }

                //Update(QString::fromStdString(response.toStdString()));

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Right limit is triggered", MotionErrorCode::RightLimitTriggered);
                    return false;
                }
            }

            if (relPosition < 0) {
                if (!Send(d->id, TMCLCommand::GAP, 11, tmclAxis, 0, response)) {
                    Update("Failed to check left limit", MotionErrorCode::FailedToCheckLeftLimit);
                    return false;
                }

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Left limit is triggered", MotionErrorCode::LeftLimitTriggered);
                    return false;
                }
            }

            if (!Send(d->id, TMCLCommand::GAP, 8, tmclAxis, 0, response)) {
                Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 1)) {
                moveTimeout = false;
                break;
            }
        }

        if (moveTimeout) {
            Update(QString("Motion timeout [elapsed=%1msec] [timeout=%2msec]").arg(timer.elapsed()).arg(timeout), MotionErrorCode::MotionTimeout);
            return false;
        }

        int32_t targetpos;
        GetTargetPosition(axis, targetpos);
        Update(QString("Reached to target=%1").arg(targetpos));

        Update("Motion is completed");
        return true;
    }

    auto MotionControlTMCM6214::PerformMoveXY(int32_t xRelPosition, int32_t yRelPosition) -> bool {
        QByteArray response;

        Update(QString("Move XY to (%1,%2)").arg(xRelPosition).arg(yRelPosition));

        if (!Send(d->id, TMCLCommand::MVP, 1, d->axis2tmclaxis(Axis::XAxis), xRelPosition, response)) {
            Update("Failed to start X axis moving", MotionErrorCode::FailedToStartMoving);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start the axis moving [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartMoving);
            return false;
        }

        if (!Send(d->id, TMCLCommand::MVP, 1, d->axis2tmclaxis(Axis::YAxis), yRelPosition, response)) {
            Update("Failed to start Y axis moving", MotionErrorCode::FailedToStartMoving);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start the axis moving [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartMoving);
            return false;
        }

        const auto axis = (abs(xRelPosition) > abs(yRelPosition)) ? Axis::XAxis : Axis::YAxis;
        const auto relPosition = (abs(xRelPosition) > abs(yRelPosition)) ? xRelPosition : yRelPosition;

        auto moveTimeout = true;
        auto timeout = std::max<uint32_t>(2000, d->axisInfo(axis).moving_timeout_per_10k_pulse * std::abs(relPosition) / 10000.0 * 1000);

        QElapsedTimer timer;
        timer.start();
        while (timer.elapsed() < timeout) {
            QThread::msleep(100);

            if (xRelPosition > 0) {
                if (!Send(d->id, TMCLCommand::GAP, 10, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
                    Update("Failed to check right limit", MotionErrorCode::FailedToCheckRightLimit);
                    return false;
                }

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Right limit is triggered", MotionErrorCode::RightLimitTriggered);
                    return false;
                }
            }

            if (yRelPosition > 0) {
                if (!Send(d->id, TMCLCommand::GAP, 10, d->axis2tmclaxis(Axis::YAxis), 0, response)) {
                    Update("Failed to check right limit", MotionErrorCode::FailedToCheckRightLimit);
                    return false;
                }

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Right limit is triggered", MotionErrorCode::RightLimitTriggered);
                    return false;
                }
            }

            if (xRelPosition < 0) {
                if (!Send(d->id, TMCLCommand::GAP, 11, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
                    Update("Failed to check left limit", MotionErrorCode::FailedToCheckLeftLimit);
                    return false;
                }

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Left limit is triggered", MotionErrorCode::LeftLimitTriggered);
                    return false;
                }
            }

            if (yRelPosition < 0) {
                if (!Send(d->id, TMCLCommand::GAP, 11, d->axis2tmclaxis(Axis::YAxis), 0, response)) {
                    Update("Failed to check left limit", MotionErrorCode::FailedToCheckLeftLimit);
                    return false;
                }

                if ((CheckStatus(response) != TMCLStatus::NoError) || (CheckValue(response) != 1)) {
                    Update("Left limit is triggered", MotionErrorCode::LeftLimitTriggered);
                    return false;
                }
            }

            bool bReached = false;
            if (!Send(d->id, TMCLCommand::GAP, 8, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
                Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 1)) {
                bReached = true;
            }

            if (bReached) {
                if (!Send(d->id, TMCLCommand::GAP, 8, d->axis2tmclaxis(Axis::XAxis), 0, response)) {
                    Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                    return false;
                }

                if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 1)) {
                    moveTimeout = false;
                    break;
                }
            }
        }

        if (moveTimeout) {
            Update(QString("Motion timeout [elapsed=%1msec] [timeout=%2msec]").arg(timer.elapsed()).arg(timeout), MotionErrorCode::MotionTimeout);
            return false;
        }

        int32_t targetposX, targetposY;
        GetTargetPosition(Axis::XAxis, targetposX);
        GetTargetPosition(Axis::YAxis, targetposY);

        Update(QString("Reached at target=(%1,%2)").arg(targetposX).arg(targetposY));

        Update("Motion is completed");
        return true;
    }

    auto MotionControlTMCM6214::PerformStartJog(Axis axis, int32_t relPosition) -> bool {
        QByteArray response;

        Update(QString("Start jog motion for %1 axis to %2").arg(axis).arg(relPosition));

        const auto tmclAxis = d->axis2tmclaxis(axis);

        if (!Send(d->id, TMCLCommand::MVP, 1, tmclAxis, relPosition, response)) {
            Update("Failed to start jog motion", MotionErrorCode::FailedToStartJogMotion);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start jog motion [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartJogMotion);
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::PerformStopJog(Axis axis) -> bool {
        QByteArray response;

        Update(QString("Stop jog motion for %1 axis").arg(axis));

        const auto tmclAxis = d->axis2tmclaxis(axis);

        if (!Send(d->id, TMCLCommand::MST, 0, tmclAxis, 0, response)) {
            Update("Failed to stop jog motion", MotionErrorCode::FailedToStopJogMotion);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to stop jog motion [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStopJogMotion);
            return false;
        }

        QElapsedTimer timer;
        timer.start();
        while (timer.elapsed() < 10000) {
            QThread::msleep(100);

            if (!Send(d->id, TMCLCommand::GAP, 3, tmclAxis, 0, response)) {
                Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                return false;
            }

            if ((CheckStatus(response) == TMCLStatus::NoError) && (CheckValue(response) == 0)) {
                break;
            }
        }

        ResetTargetPosition(axis);
        Update("Jog motion is stopped");

        return true;
    }

    auto MotionControlTMCM6214::PerformZScan(uint32_t slices, int32_t step, int32_t relBefore,
        int32_t relAfter) -> bool {
        Update(QString("Z Scanning (slices=%1 step=%2 before=%3 after=%4)").arg(slices).arg(step).arg(relBefore).arg(relAfter));

        int32_t startingPosition{ 0 };
        if (!GetActualPosition(Axis::ZAxis, startingPosition)) {
            Update("Failed to get starting position", MotionErrorCode::FailedToGetStartingPosition);
            return false;
        }

        const int32_t finalPosition = (startingPosition + relBefore + (slices * step) + relAfter);
        Update(QString("Z scan is started at %1 and will be arrived to %2").arg(startingPosition).arg(finalPosition));

        if (!SetUserVariable(10, slices)) {
            Update("Failed to set slices", MotionErrorCode::FailedToSetSlices);
            return false;
        }

        if (!SetUserVariable(11, step)) {
            Update("Failed to set step amount", MotionErrorCode::FailedToSetSteps);
            return false;
        }

        if (!SetUserVariable(12, relBefore)) {
            Update("Failed to set motion amount for the first motion", MotionErrorCode::FailedToSetZStart);
            return false;
        }

        if (!SetUserVariable(13, relAfter)) {
            Update("Failed to set motion amount for the last motion", MotionErrorCode::FailedToSetZLast);
            return false;
        }

        QByteArray response;
        if (!Send(d->id, TMCLCommand::RUN, 1, 0, 2, response)) {
            Update("Failed to start Z scan", MotionErrorCode::FailedToStartZScan);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start Z scan [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartZScan);
            return false;
        }

        QThread::msleep(200);

        QElapsedTimer timer;
        timer.start();

        auto moveTimeout = true;
        while (timer.elapsed() < (d->config.zScan.timeout * 1000)) {
            if (!Send(d->id, TMCLCommand::GGP, 15, 2, 0, response)) {
                Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                return false;
            }

            if (CheckStatus(response) != TMCLStatus::NoError) {
                break;
            }

            const auto flag = CheckValue(response);
            if (flag == 0) {
                moveTimeout = false;
                break;
            }
        }

        if (moveTimeout) {
            Update("Z Scan timeout", MotionErrorCode::ZScanTimeout);
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::PerformZScanFL(uint32_t slices, int32_t step, int32_t relBefore, int32_t relAfter,
        int32_t flExposure) -> bool {
        Update(QString("FL Z Scanning (slices=%1 step=%2 before=%3 after=%4 flExp=%5)")
               .arg(slices).arg(step).arg(relBefore).arg(relAfter).arg(flExposure));

        int32_t startingPosition{ 0 };
        if (!GetActualPosition(Axis::ZAxis, startingPosition)) {
            Update("Failed to get starting position", MotionErrorCode::FailedToGetStartingPosition);
            return false;
        }

        const int32_t finalPosition = (startingPosition + relBefore + (slices * step) + relAfter);
        Update(QString("FL Z scan is started at %1 and will be arrived to %2").arg(startingPosition).arg(finalPosition));

        if (!SetUserVariable(10, slices)) {
            Update("Failed to set slices", MotionErrorCode::FailedToSetSlices);
            return false;
        }

        if (!SetUserVariable(11, step)) {
            Update("Failed to set step amount", MotionErrorCode::FailedToSetSteps);
            return false;
        }

        if (!SetUserVariable(12, relBefore)) {
            Update("Failed to set motion amount for the first motion", MotionErrorCode::FailedToSetZStart);
            return false;
        }

        if (!SetUserVariable(13, relAfter)) {
            Update("Failed to set motion amount for the last motion", MotionErrorCode::FailedToSetZLast);
            return false;
        }

        if (!SetUserVariable(21, flExposure)) {
            Update("Failed to set FL exposure time", MotionErrorCode::FailedToSetFLExposure);
            return false;
        }

        QByteArray response;
        if (!Send(d->id, TMCLCommand::RUN, 1, 0, 4, response)) {
            Update("Failed to start FL Z scan", MotionErrorCode::FailedToStartZScan);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to start FL Z scan [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartZScan);
            return false;
        }

        QThread::msleep(200);

        QElapsedTimer timer;
        timer.start();

        auto moveTimeout = true;
        while (timer.elapsed() < (d->config.zScan.timeout * 1000)) {
            if (!Send(d->id, TMCLCommand::GGP, 15, 2, 0, response)) {
                Update("Failed to check its arrival", MotionErrorCode::FailedToCheckArrival);
                return false;
            }

            if (CheckStatus(response) != TMCLStatus::NoError) {
                break;
            }

            const auto flag = CheckValue(response);
            if (flag == 0) {
                moveTimeout = false;
                break;
            }
        }

        if (moveTimeout) {
            Update("FL Z Scan timeout", MotionErrorCode::ZScanTimeout);
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::PerformSingleTrigger() -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::RUN, 1, 0, 3, response)) {
            Update("Failed to raise single trigger", MotionErrorCode::FailedToRaiseTrigger);
            return false;
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to raise single trigger [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToStartZScan);
            return false;
        }

        QThread::msleep(100);

        return true;
    }

    auto MotionControlTMCM6214::PerformReadFirmwareTag() -> QString {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::GGP, 20, 2, 0, response)) {
            Update("Failed to check firmware tag", MotionErrorCode::FailedToReadFirmwareTag);
            return "";
        }

        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update("Failed to check firmware tag", MotionErrorCode::FailedToReadFirmwareTag);
        }

        const auto tag = CheckValue(response);
        return QString::number(tag);
    }

    auto MotionControlTMCM6214::GetActualPosition(Axis axis, int32_t& position) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::GAP, 1, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to get actual position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        position = CheckValue(response);
        return true;
    }

    auto MotionControlTMCM6214::ResetActualPosition(Axis axis) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::MST, 0, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to stop motor");
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }
        
        if (!Send(d->id, TMCLCommand::SAP, 1, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to reset actual position", MotionErrorCode::FailedToResetActualPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::GetEncoderPosition(Axis axis, int32_t& position) -> bool {
        if(!d->hasEncoder(axis)) {
            position = 0;
            return true;
        }

        QByteArray response;
        if (!Send(d->id, TMCLCommand::GAP, 209, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to get actual position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        position = CheckValue(response);
        return true;
    }

    auto MotionControlTMCM6214::ResetEncoderPosition(Axis axis) -> bool {
        if(!d->hasEncoder(axis)) return true;

        QByteArray response;
        if (!Send(d->id, TMCLCommand::MST, 0, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to stop motor");
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }
        
        if (!Send(d->id, TMCLCommand::SAP, 209, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to reset actual position", MotionErrorCode::FailedToResetEncoderPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::GetTargetPosition(Axis axis, int32_t& position) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::GAP, 0, d->axis2tmclaxis(axis), 0, response)) {
            Update("Failed to get actual position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        position = CheckValue(response);
        return true;
    }

    auto MotionControlTMCM6214::ResetTargetPosition(Axis axis) -> bool {
        QByteArray response;

        int32_t actualPosition;
        if (!GetActualPosition(axis, actualPosition)) {
            Update("Failed to get actual position", MotionErrorCode::FailedToGetActualPosition);
            return false;
        }

        if (!Send(d->id, TMCLCommand::SAP, 0, d->axis2tmclaxis(axis), actualPosition, response)) {
            Update("Failed to reset target position", MotionErrorCode::FailedToResetTargetPosition);
            return false;
        }

        if ((CheckStatus(response) != TMCLStatus::NoError)) {
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::SetUserVariable(uint8_t address, int32_t value) -> bool {
        QByteArray response;
        if (!Send(d->id, TMCLCommand::SGP, address, 2, value, response)) return false;
        if (CheckStatus(response) != TMCLStatus::NoError) {
            Update(QString("Failed to set user variable [Status=%1]").arg(CheckStatus(response)), MotionErrorCode::FailedToSetUserVariable);
        }

        return true;
    }

    auto MotionControlTMCM6214::Send(uint8_t address, uint8_t command, uint8_t type, uint8_t motor,
        int32_t value, QByteArray& response, uint32_t waitTimeout) -> bool {

        char buffer[9];
        buffer[0] = address;
        buffer[1] = command;
        buffer[2] = type;
        buffer[3] = motor;
        buffer[4] = static_cast<uint8_t>((value >> 24)&0xFF);
        buffer[5] = static_cast<uint8_t>((value >> 16)&0xFF);
        buffer[6] = static_cast<uint8_t>((value >> 8)&0xFF);
        buffer[7] = static_cast<uint8_t>(value & 0xFF);
        buffer[8] = 0;
        for (auto idx = 0; idx < 8; idx++) {
            buffer[8] += buffer[idx];
        }

        //QByteArray sendData(buffer, 9);
        //Update(QString("send: %1").arg(ResponseToString(sendData)));

        if (!d->port->write(buffer, 9)) {
            Update("Failed to send command", MotionErrorCode::FailedToSendCommand);
            return false;
        }

        if (!d->port->waitForBytesWritten(waitTimeout)) {
            Update("Write timeout", MotionErrorCode::SendTimeout);
            return false;
        }

        if (!d->port->waitForReadyRead(waitTimeout)) {
            Update("Write timeout", MotionErrorCode::ResponseTimeout);
            return false;
        }

        response = d->port->readAll();
        while (d->port->waitForReadyRead(20))
            response += d->port->readAll();

        uint8_t checksum = 0;
        for (int index = 0; index < 8; index++) {
            checksum += static_cast<uint8_t>(response.at(index));
        }

        if (checksum != static_cast<uint8_t>(response.at(8))) {
            Update(QString("Invalid response [%1]").arg(ResponseToString(response)), MotionErrorCode::InvalidResponse);
            return false;
        }

        return true;
    }

    auto MotionControlTMCM6214::CheckStatus(const QByteArray& response) const -> uint32_t {
        return response.at(2);
    }

    auto MotionControlTMCM6214::CheckValue(const QByteArray& response) const -> int32_t {
        int32_t value;
        value =   ((static_cast<int32_t>(response.at(4))&0xFF) << 24)
                + ((static_cast<int32_t>(response.at(5))&0xFF) << 16)
                + ((static_cast<int32_t>(response.at(6))&0xFF) << 8)
                + ((static_cast<int32_t>(response.at(7))&0xFF));
        return value;
    }

    auto ResponseToString(const QByteArray& response)->QString {
        QString str;
        const auto len = response.length();
        for (auto idx = 0; idx < len; idx++) {
            str.append(QString("0x%1 ").arg(response.at(idx)&0xFF, 2, 16, QLatin1Char('0')));
        }
        str.remove(str.length()-1, 1);
        return str;
    }

    auto FindTMCMPort() -> QString {
        QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();

        foreach(QSerialPortInfo info, list) {
            const QString desc = info.description();
            quint16 vendorID = info.vendorIdentifier();
            quint16 productID = info.productIdentifier();

            if ((vendorID == 10812) && (productID == 256))
                return info.portName();
        }

        return "";
    }
}
