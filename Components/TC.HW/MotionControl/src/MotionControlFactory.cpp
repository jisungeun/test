#include "MotionControlTMCM6212.h"
#include "MotionControlTMCM6214.h"
#include "MotionControlFactory.h"

namespace TC::MotionControl {
    auto MotionControlFactory::Create(Controller type, const MotionConfig& config) -> MotionControl::Pointer {
        MotionControl::Pointer instance;

        switch (type) {
        case Controller::TMCM6212:
            instance.reset(new TMCM6212::MotionControlTMCM6212(config));
            break;
        case Controller::TMCM6214:
            instance.reset(new TMCM6214::MotionControlTMCM6214(config));
            break;
        }

        return instance;
    }

}