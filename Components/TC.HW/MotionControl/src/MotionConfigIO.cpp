#include <QSettings>
#include <QFile>

#include "MotionConfigIO.h"

#define GROUP_SERIALPORT "SerialPort"
#define     PORT "Port"
#define     BARUDRATE "Barudrate"
#define GROUP_AXISINFO "AxisInfo"
#define     AXIS_INDEX "Axis_Index"
#define     PULSES_PER_MM  "Pulses_Per_MM"
#define     UM_PER_1K_PULSE "UM_Per_1K_Pulses"
#define     LOWER_LIMIT_PULSE "Lower_Limit_In_Pulses"
#define     UPPER_LIMIT_PULSE "Upper_Limit_In_Pulses"
#define     HOMING_TIMEOUT "Homing_Timeout_In_Secs"
#define     MOVING_TIMEOUT_PER_10k_PULSE "Moving_Timeout_Per_10K_Pulses_In_Secs"
#define     HAS_ENCODER "Has_Encoder"
#define GROUP_ZSCAN "ZScan"
#define     TIMEOUT "Z_Scan_Timeout"
#define VALID "Valid"

namespace TC::MotionControl {


    auto MotionConfigIO::Load(const QString& path, MotionConfig& config) -> bool {
        if (!QFile::exists(path)) return false;

        QSettings qs(path, QSettings::IniFormat);

        qs.beginGroup(GROUP_SERIALPORT);
        {
            config.serialPort.port = qs.value(PORT, 3).toUInt();
            config.serialPort.baudrate = qs.value(BARUDRATE, 115200).toUInt();
        }
        qs.endGroup();

        qs.beginGroup(GROUP_AXISINFO);
        {
            qs.beginGroup("X");
            {
                const auto axis = MotionConfig::AxisIndex::XAxisIndex;
                config.axisInfo[axis].axisIndex = qs.value(AXIS_INDEX, 0).toUInt();
                config.axisInfo[axis].pulses_per_mm = qs.value(PULSES_PER_MM, 6400).toUInt();
                config.axisInfo[axis].um_per_1k_pulse = qs.value(UM_PER_1K_PULSE, 156.250).toDouble();
                config.axisInfo[axis].lower_limit_pulse = qs.value(LOWER_LIMIT_PULSE, 0).toUInt();
                config.axisInfo[axis].upper_limit_pulse = qs.value(UPPER_LIMIT_PULSE, 10000).toUInt();
                config.axisInfo[axis].homing_timeout = qs.value(HOMING_TIMEOUT, 20).toUInt();
                config.axisInfo[axis].moving_timeout_per_10k_pulse = qs.value(MOVING_TIMEOUT_PER_10k_PULSE, 10.0).toDouble();
                config.axisInfo[axis].has_encoder = qs.value(HAS_ENCODER, false).toBool();
            }
            qs.endGroup();

            qs.beginGroup("Y");
            {
                const auto axis = MotionConfig::AxisIndex::YAxisIndex;
                config.axisInfo[axis].axisIndex = qs.value(AXIS_INDEX, 1).toUInt();
                config.axisInfo[axis].pulses_per_mm = qs.value(PULSES_PER_MM, 6400).toUInt();
                config.axisInfo[axis].um_per_1k_pulse = qs.value(UM_PER_1K_PULSE, 156.250).toDouble();
                config.axisInfo[axis].lower_limit_pulse = qs.value(LOWER_LIMIT_PULSE, 0).toUInt();
                config.axisInfo[axis].upper_limit_pulse = qs.value(UPPER_LIMIT_PULSE, 10000).toUInt();
                config.axisInfo[axis].homing_timeout = qs.value(HOMING_TIMEOUT, 20).toUInt();
                config.axisInfo[axis].moving_timeout_per_10k_pulse = qs.value(MOVING_TIMEOUT_PER_10k_PULSE, 10.0).toDouble();
                config.axisInfo[axis].has_encoder = qs.value(HAS_ENCODER, false).toBool();
            }
            qs.endGroup();

            qs.beginGroup("Z");
            {
                const auto axis = MotionConfig::AxisIndex::ZAxisIndex;
                config.axisInfo[axis].axisIndex = qs.value(AXIS_INDEX, 2).toUInt();
                config.axisInfo[axis].pulses_per_mm = qs.value(PULSES_PER_MM, 25600).toUInt();
                config.axisInfo[axis].um_per_1k_pulse = qs.value(UM_PER_1K_PULSE, 156.250).toDouble();
                config.axisInfo[axis].lower_limit_pulse = qs.value(LOWER_LIMIT_PULSE, 0).toUInt();
                config.axisInfo[axis].upper_limit_pulse = qs.value(UPPER_LIMIT_PULSE, 10000).toUInt();
                config.axisInfo[axis].homing_timeout = qs.value(HOMING_TIMEOUT, 20).toUInt();
                config.axisInfo[axis].moving_timeout_per_10k_pulse = qs.value(MOVING_TIMEOUT_PER_10k_PULSE, 10.0).toDouble();
                config.axisInfo[axis].has_encoder = qs.value(HAS_ENCODER, false).toBool();
            }
            qs.endGroup();

            qs.beginGroup("C");
            {
                const auto axis = MotionConfig::AxisIndex::CAxisIndex;
                config.axisInfo[axis].axisIndex = qs.value(AXIS_INDEX, 4).toUInt();
                config.axisInfo[axis].pulses_per_mm = qs.value(PULSES_PER_MM, 25600).toUInt();
                config.axisInfo[axis].um_per_1k_pulse = qs.value(UM_PER_1K_PULSE, 156.250).toDouble();
                config.axisInfo[axis].lower_limit_pulse = qs.value(LOWER_LIMIT_PULSE, 0).toUInt();
                config.axisInfo[axis].upper_limit_pulse = qs.value(UPPER_LIMIT_PULSE, 10000).toUInt();
                config.axisInfo[axis].homing_timeout = qs.value(HOMING_TIMEOUT, 20).toUInt();
                config.axisInfo[axis].moving_timeout_per_10k_pulse = qs.value(MOVING_TIMEOUT_PER_10k_PULSE, 10.0).toDouble();
                config.axisInfo[axis].has_encoder = qs.value(HAS_ENCODER, false).toBool();
            }
            qs.endGroup();

            qs.beginGroup("F");
            {
                const auto axis = MotionConfig::AxisIndex::FAxisIndex;
                config.axisInfo[axis].axisIndex = qs.value(AXIS_INDEX, 5).toUInt();
                config.axisInfo[axis].pulses_per_mm = qs.value(PULSES_PER_MM, 25600).toUInt();
                config.axisInfo[axis].um_per_1k_pulse = qs.value(UM_PER_1K_PULSE, 156.250).toDouble();
                config.axisInfo[axis].lower_limit_pulse = qs.value(LOWER_LIMIT_PULSE, 0).toUInt();
                config.axisInfo[axis].upper_limit_pulse = qs.value(UPPER_LIMIT_PULSE, 10000).toUInt();
                config.axisInfo[axis].homing_timeout = qs.value(HOMING_TIMEOUT, 20).toUInt();
                config.axisInfo[axis].moving_timeout_per_10k_pulse = qs.value(MOVING_TIMEOUT_PER_10k_PULSE, 10.0).toDouble();
                config.axisInfo[axis].has_encoder = qs.value(HAS_ENCODER, false).toBool();
            }
            qs.endGroup();
        }
        qs.endGroup();

        qs.beginGroup(GROUP_ZSCAN);
        {
            config.zScan.timeout = qs.value(TIMEOUT, 10).toUInt();
        }
        qs.endGroup();

        config.valid = qs.value(VALID, true).toBool();

        return true;
    }

    auto MotionConfigIO::Save(const QString& path, const MotionConfig& config) -> bool {\
        QSettings qs(path, QSettings::IniFormat);

        qs.beginGroup(GROUP_SERIALPORT);
        {
            qs.setValue(PORT, config.serialPort.port);
            qs.setValue(BARUDRATE, config.serialPort.baudrate);
        }
        qs.endGroup();

        qs.beginGroup(GROUP_AXISINFO);
        {
            qs.beginGroup("X");
            {
                const auto axis = MotionConfig::AxisIndex::XAxisIndex;
                qs.setValue(AXIS_INDEX, config.axisInfo[axis].axisIndex);
                qs.setValue(PULSES_PER_MM, config.axisInfo[axis].pulses_per_mm);
                qs.setValue(UM_PER_1K_PULSE, config.axisInfo[axis].um_per_1k_pulse);
                qs.setValue(LOWER_LIMIT_PULSE, config.axisInfo[axis].lower_limit_pulse);
                qs.setValue(UPPER_LIMIT_PULSE, config.axisInfo[axis].upper_limit_pulse);
                qs.setValue(HOMING_TIMEOUT, config.axisInfo[axis].homing_timeout);
                qs.setValue(MOVING_TIMEOUT_PER_10k_PULSE, config.axisInfo[axis].moving_timeout_per_10k_pulse);
                qs.setValue(HAS_ENCODER, config.axisInfo[axis].has_encoder);
            }
            qs.endGroup();

            qs.beginGroup("Y");
            {
                const auto axis = MotionConfig::AxisIndex::YAxisIndex;
                qs.setValue(AXIS_INDEX, config.axisInfo[axis].axisIndex);
                qs.setValue(PULSES_PER_MM, config.axisInfo[axis].pulses_per_mm);
                qs.setValue(UM_PER_1K_PULSE, config.axisInfo[axis].um_per_1k_pulse);
                qs.setValue(LOWER_LIMIT_PULSE, config.axisInfo[axis].lower_limit_pulse);
                qs.setValue(UPPER_LIMIT_PULSE, config.axisInfo[axis].upper_limit_pulse);
                qs.setValue(HOMING_TIMEOUT, config.axisInfo[axis].homing_timeout);
                qs.setValue(MOVING_TIMEOUT_PER_10k_PULSE, config.axisInfo[axis].moving_timeout_per_10k_pulse);
                qs.setValue(HAS_ENCODER, config.axisInfo[axis].has_encoder);
            }
            qs.endGroup();

            qs.beginGroup("Z");
            {
                const auto axis = MotionConfig::AxisIndex::ZAxisIndex;
                qs.setValue(AXIS_INDEX, config.axisInfo[axis].axisIndex);
                qs.setValue(PULSES_PER_MM, config.axisInfo[axis].pulses_per_mm);
                qs.setValue(UM_PER_1K_PULSE, config.axisInfo[axis].um_per_1k_pulse);
                qs.setValue(LOWER_LIMIT_PULSE, config.axisInfo[axis].lower_limit_pulse);
                qs.setValue(UPPER_LIMIT_PULSE, config.axisInfo[axis].upper_limit_pulse);
                qs.setValue(HOMING_TIMEOUT, config.axisInfo[axis].homing_timeout);
                qs.setValue(MOVING_TIMEOUT_PER_10k_PULSE, config.axisInfo[axis].moving_timeout_per_10k_pulse);
                qs.setValue(HAS_ENCODER, config.axisInfo[axis].has_encoder);
            }
            qs.endGroup();

            qs.beginGroup("C");
            {
                const auto axis = MotionConfig::AxisIndex::CAxisIndex;
                qs.setValue(AXIS_INDEX, config.axisInfo[axis].axisIndex);
                qs.setValue(PULSES_PER_MM, config.axisInfo[axis].pulses_per_mm);
                qs.setValue(UM_PER_1K_PULSE, config.axisInfo[axis].um_per_1k_pulse);
                qs.setValue(LOWER_LIMIT_PULSE, config.axisInfo[axis].lower_limit_pulse);
                qs.setValue(UPPER_LIMIT_PULSE, config.axisInfo[axis].upper_limit_pulse);
                qs.setValue(HOMING_TIMEOUT, config.axisInfo[axis].homing_timeout);
                qs.setValue(MOVING_TIMEOUT_PER_10k_PULSE, config.axisInfo[axis].moving_timeout_per_10k_pulse);
                qs.setValue(HAS_ENCODER, config.axisInfo[axis].has_encoder);
            }
            qs.endGroup();

            qs.beginGroup("F");
            {
                const auto axis = MotionConfig::AxisIndex::FAxisIndex;
                qs.setValue(AXIS_INDEX, config.axisInfo[axis].axisIndex);
                qs.setValue(PULSES_PER_MM, config.axisInfo[axis].pulses_per_mm);
                qs.setValue(UM_PER_1K_PULSE, config.axisInfo[axis].um_per_1k_pulse);
                qs.setValue(LOWER_LIMIT_PULSE, config.axisInfo[axis].lower_limit_pulse);
                qs.setValue(UPPER_LIMIT_PULSE, config.axisInfo[axis].upper_limit_pulse);
                qs.setValue(HOMING_TIMEOUT, config.axisInfo[axis].homing_timeout);
                qs.setValue(MOVING_TIMEOUT_PER_10k_PULSE, config.axisInfo[axis].moving_timeout_per_10k_pulse);
                qs.setValue(HAS_ENCODER, config.axisInfo[axis].has_encoder);
            }
            qs.endGroup();
        }
        qs.endGroup();

        qs.beginGroup(GROUP_ZSCAN);
        {
            qs.setValue(TIMEOUT, config.zScan.timeout);
        }
        qs.endGroup();

        qs.setValue(VALID, config.valid);

        return true;
    }
}