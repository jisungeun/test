#define LOGGER_TAG "[UI]"
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QDir>
#include <QMessageBox>
#include <QSerialPortInfo>
#include <QList>
#include <QTimer>

#include <TCLogger.h>

#include "ui_mainwindow.h"
#include "Version.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace TC::MotionControl {
    typedef MainWindowControl::Axis Axis;

    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };
        MainWindowControl* control;
        QTimer updateTimer;
        QList<QPushButton*> jogButtons;

        struct PositionWidget {
            QSpinBox* pos;
            QSpinBox* actualPos;
            QSpinBox* encoderPos;
        };

        QMap<Axis, PositionWidget> posWidgets;

        auto DisableJogButtons(QPushButton* button = nullptr)->void {
            if (button == nullptr) {
                foreach(auto btn, jogButtons) {
                    btn->setEnabled(true);
                }
            } else {
              foreach (auto btn, jogButtons) {
                  if (btn == button) continue;
                  btn->setDisabled(true);
              }
            }
        }

        auto UpdatePosition(Axis axis)->void {
            const auto position = control->GetPosition(axis);
            posWidgets[axis].pos->setValue(position);
            posWidgets[axis].actualPos->setValue(position);

            const auto encPosition = control->GetEncoderPosition(axis);
            posWidgets[axis].encoderPos->setValue(encPosition);

            QLOG_INFO() << Axis2Str(axis) << " Pos:" << position << " Enc Pos:" << encPosition;
        }

        auto Axis2Str(Axis axis) const->QString {
            switch(axis) {
            case Axis::XAxis:
                return "X";
            case Axis::YAxis:
                return "Y";
            case Axis::ZAxis:
                return "Z";
            case Axis ::CAxis:
                return "C";
            case Axis::FAxis:
                return "F";
            }
            return "X";
        }
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui = new Ui::MainWindow();
        d->ui->setupUi(this);
        d->control = new MainWindowControl();

        d->posWidgets = {
            {Axis::XAxis, {d->ui->xpositionSpin, d->ui->xActualPos, d->ui->xEncoderPos}},
            {Axis::YAxis, {d->ui->ypositionSpin, d->ui->yActualPos, d->ui->yEncoderPos}},
            {Axis::ZAxis, {d->ui->zpositionSpin, d->ui->zActualPos, d->ui->zEncoderPos}},
            {Axis::CAxis, {d->ui->cpositionSpin, d->ui->cActualPos, d->ui->cEncoderPos}},
            {Axis::FAxis, {d->ui->fpositionSpin, d->ui->fActualPos, d->ui->fEncoderPos}},
        };

        setWindowTitle(QString("HTX Motion Console %1").arg(PROJECT_REVISION));

        ScanPorts();

        d->jogButtons.push_back(d->ui->xjogminusBtn);
        d->jogButtons.push_back(d->ui->xjogplusBtn);
        d->jogButtons.push_back(d->ui->yjogminusBtn);
        d->jogButtons.push_back(d->ui->yjogplusBtn);
        d->jogButtons.push_back(d->ui->zjogminusBtn);
        d->jogButtons.push_back(d->ui->cjogplusBtn);
        d->jogButtons.push_back(d->ui->cjogminusBtn);
        d->jogButtons.push_back(d->ui->fjogplusBtn);
        d->jogButtons.push_back(d->ui->fjogminusBtn);

        connect(d->ui->actionOpen, SIGNAL(triggered()), this, SLOT(onOpenFolder()));
        connect(d->ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
        connect(d->ui->initializeBtn, SIGNAL(clicked()), this, SLOT(onInitialization()));
        connect(d->ui->homingBtn, SIGNAL(clicked()), this, SLOT(onHoming()));
        connect(d->ui->xmoveBtn, SIGNAL(clicked()), this, SLOT(onMoveX()));
        connect(d->ui->ymoveBtn, SIGNAL(clicked()), this, SLOT(onMoveY()));
        connect(d->ui->zmoveBtn, SIGNAL(clicked()), this, SLOT(onMoveZ()));
        connect(d->ui->cmoveBtn, SIGNAL(clicked()), this, SLOT(onMoveC()));
        connect(d->ui->fmoveBtn, SIGNAL(clicked()), this, SLOT(onMoveF()));
        connect(d->ui->xrelmoveMinusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveXMinus()));
        connect(d->ui->xrelmovePlusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveXPlus()));
        connect(d->ui->yrelmoveMinusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveYMinus()));
        connect(d->ui->yrelmovePlusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveYPlus()));
        connect(d->ui->zrelmoveMinusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveZMinus()));
        connect(d->ui->zrelmovePlusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveZPlus()));
        connect(d->ui->crelmoveMinusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveCMinus()));
        connect(d->ui->crelmovePlusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveCPlus()));
        connect(d->ui->frelmoveMinusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveFMinus()));
        connect(d->ui->frelmovePlusBtn, SIGNAL(clicked()), this, SLOT(onRelMoveFPlus()));
        connect(d->ui->xjogminusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartXJogMinus(bool)));
        connect(d->ui->xjogplusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartXJogPlus(bool)));
        connect(d->ui->yjogminusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartYJogMinus(bool)));
        connect(d->ui->yjogplusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartYJogPlus(bool)));
        connect(d->ui->zjogminusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartZJogMinus(bool)));
        connect(d->ui->zjogplusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartZJogPlus(bool)));
        connect(d->ui->cjogminusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartCJogMinus(bool)));
        connect(d->ui->cjogplusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartCJogPlus(bool)));
        connect(d->ui->fjogminusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartFJogMinus(bool)));
        connect(d->ui->fjogplusBtn, SIGNAL(clicked(bool)), this, SLOT(onStartFJogPlus(bool)));
        connect(d->ui->zscanslicecountSpin, SIGNAL(valueChanged(int)), this, SLOT(onZScanConditionChanged(int)));
        connect(d->ui->zscanslicespacingSpin, SIGNAL(valueChanged(int)), this, SLOT(onZScanConditionChanged(int)));
        connect(d->ui->zscanBtn, SIGNAL(clicked()), this, SLOT(onZScan()));
        connect(d->ui->zscanFlBtn, SIGNAL(clicked()), this, SLOT(onZScanFL()));
        connect(d->ui->ggpBtn, SIGNAL(clicked()), this, SLOT(onGetGlobalParameter()));
        connect(d->ui->sgpBtn, SIGNAL(clicked()), this, SLOT(onSetGlobalParameter()));
        connect(d->ui->runBtn, SIGNAL(clicked()), this, SLOT(onRunProgram()));
        connect(&d->updateTimer, SIGNAL(timeout()), this, SLOT(onUpdatePosition()));
    }
    
    MainWindow::~MainWindow() {
        delete d->control;
    }

    auto MainWindow::ScanPorts() -> void {
        auto ports = QSerialPortInfo::availablePorts();
        foreach(auto port, ports) {
            auto name = port.portName();
            auto number = name.remove("COM").toUInt();
            d->ui->comports->addItem(name, number);
        }
    }

    void MainWindow::onOpenFolder() {
        const auto configPath = QString("%1/config").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        QDir dir(configPath);
        if(!dir.exists()) {
            QDir().mkpath(configPath);
        }

        QDesktopServices::openUrl(QUrl(QString("file:///%1").arg(configPath), QUrl::TolerantMode));
    }

    void MainWindow::onInitialization() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();

        if (!d->control->LoadConfiguration()) {
            QMessageBox::warning(this, "Initialization", "Valid configuration file does not exist. Prepare a correction configuration file at first");
            setDisabled(false);
            d->control->MakeDummyConfiguration();
            onOpenFolder();
        }

        if (!d->control->Initialize(d->ui->comports->currentData().toUInt())) {
            QMessageBox::warning(this, "Initialization", "Initialization is failed");
            setDisabled(true);
        } else {
            statusBar()->showMessage("Initialized");
            setDisabled(false);

            const auto firmwareTag = d->control->GetFirmwareTag();
            setWindowTitle(QString("HTX Motion Console %1 (Firmware: %2)").arg(PROJECT_REVISION).arg(firmwareTag));
        }
    }

    void MainWindow::onHoming() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (!d->control->Homing()) {
            QMessageBox::warning(this, "Homing", "Homing is failed");
        } else {
            statusBar()->showMessage("Homing is completed");
        }
        setDisabled(false);
    }

    void MainWindow::onMoveX() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto target = d->ui->xpositionSpin->value();
        if (!d->control->MoveX(target)) {
            QMessageBox::warning(this, "Move", "Failed to move X Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::XAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onMoveY() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto target = d->ui->ypositionSpin->value();
        if (!d->control->MoveY(target)) {
            QMessageBox::warning(this, "Move", "Failed to move Y Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::YAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onMoveZ() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto target = d->ui->zpositionSpin->value();
        if (!d->control->MoveZ(target)) {
            QMessageBox::warning(this, "Move", "Failed to move Z Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::ZAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onMoveC() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto target = d->ui->cpositionSpin->value();
        if (!d->control->MoveC(target)) {
            QMessageBox::warning(this, "Move", "Failed to move C Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::CAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onMoveF() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto target = d->ui->fpositionSpin->value();
        if (!d->control->MoveF(target)) {
            QMessageBox::warning(this, "Move", "Failed to move F Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::FAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveXMinus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->xrelpositionSpin->value();
        if (!d->control->MoveRelX((-1)*offset)) {
            QMessageBox::warning(this, "Move", "Failed to move X Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::XAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveXPlus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->xrelpositionSpin->value();
        if (!d->control->MoveRelX(offset)) {
            QMessageBox::warning(this, "Move", "Failed to move X Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::XAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveYMinus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->yrelpositionSpin->value();
        if (!d->control->MoveRelY((-1) * offset)) {
            QMessageBox::warning(this, "Move", "Failed to move Y Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::YAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveYPlus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->yrelpositionSpin->value();
        if (!d->control->MoveRelY(offset)) {
            QMessageBox::warning(this, "Move", "Failed to move Y Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::YAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveZMinus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->zrelpositionSpin->value();
        if (!d->control->MoveRelZ((-1) * offset)) {
            QMessageBox::warning(this, "Move", "Failed to move Z Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::ZAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveZPlus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->zrelpositionSpin->value();
        if (!d->control->MoveRelZ(offset)) {
            QMessageBox::warning(this, "Move", "Failed to move Z Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::ZAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveCMinus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->crelpositionSpin->value();
        if (!d->control->MoveRelC((-1) * offset)) {
            QMessageBox::warning(this, "Move", "Failed to move C Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::CAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveCPlus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->crelpositionSpin->value();
        if (!d->control->MoveRelC(offset)) {
            QMessageBox::warning(this, "Move", "Failed to move C Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::CAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveFMinus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->frelpositionSpin->value();
        if (!d->control->MoveRelF((-1) * offset)) {
            QMessageBox::warning(this, "Move", "Failed to move F Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::FAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onRelMoveFPlus() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        const auto offset = d->ui->frelpositionSpin->value();
        if (!d->control->MoveRelF(offset)) {
            QMessageBox::warning(this, "Move", "Failed to move F Axis");
        } else {
            statusBar()->showMessage("Motion completed");
            d->UpdatePosition(Axis::FAxis);
        }
        setDisabled(false);
    }

    void MainWindow::onStartXJogMinus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogX(false)) {
                QMessageBox::warning(this, "Jog", "Failed to start X Axis Jog motion");
            } else {
                statusBar()->showMessage("X Axis is moving to minus direction");
            }
            d->DisableJogButtons(d->ui->xjogminusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogX()) {
                QMessageBox::warning(this, "Jog", "Failed to stop X Axis Jog motion");
            } else {
                statusBar()->showMessage("X Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartXJogPlus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogX(true)) {
                QMessageBox::warning(this, "Jog", "Failed to start X Axis Jog motion");
            } else {
                statusBar()->showMessage("X Axis is moving to plus direction");
            }
            d->DisableJogButtons(d->ui->xjogplusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogX()) {
                QMessageBox::warning(this, "Jog", "Failed to stop X Axis Jog motion");
            } else {
                statusBar()->showMessage("X Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartYJogMinus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogY(false)) {
                QMessageBox::warning(this, "Jog", "Failed to start Y Axis Jog motion");
            } else {
                statusBar()->showMessage("Y Axis is moving to minus direction");
            }
            d->DisableJogButtons(d->ui->yjogminusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogY()) {
                QMessageBox::warning(this, "Jog", "Failed to stop Y Axis Jog motion");
            } else {
                statusBar()->showMessage("Y Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartYJogPlus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogY(true)) {
                QMessageBox::warning(this, "Jog", "Failed to start Y Axis Jog motion");
            } else {
                statusBar()->showMessage("Y Axis is moving to plus direction");
            }
            d->DisableJogButtons(d->ui->yjogplusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogY()) {
                QMessageBox::warning(this, "Jog", "Failed to stop Y Axis Jog motion");
            } else {
                statusBar()->showMessage("Y Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartZJogMinus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogZ(false)) {
                QMessageBox::warning(this, "Jog", "Failed to start Z Axis Jog motion");
            } else {
                statusBar()->showMessage("Z Axis is moving to minus direction");
            }
            d->DisableJogButtons(d->ui->zjogminusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogZ()) {
                QMessageBox::warning(this, "Jog", "Failed to start Z Axis Jog motion");
            } else {
                statusBar()->showMessage("Z Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartZJogPlus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogZ(true)) {
                QMessageBox::warning(this, "Jog", "Failed to start Z Axis Jog motion");
            } else {
                statusBar()->showMessage("Z Axis is moving to plus direction");
            }
            d->DisableJogButtons(d->ui->zjogplusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogZ()) {
                QMessageBox::warning(this, "Jog", "Failed to start Z Axis Jog motion");
            } else {
                statusBar()->showMessage("Z Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartCJogMinus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogC(false)) {
                QMessageBox::warning(this, "Jog", "Failed to start C Axis Jog motion");
            } else {
                statusBar()->showMessage("C Axis is moving to minus direction");
            }
            d->DisableJogButtons(d->ui->cjogminusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogC()) {
                QMessageBox::warning(this, "Jog", "Failed to start C Axis Jog motion");
            } else {
                statusBar()->showMessage("C Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartCJogPlus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogC(true)) {
                QMessageBox::warning(this, "Jog", "Failed to start C Axis Jog motion");
            } else {
                statusBar()->showMessage("C Axis is moving to plus direction");
            }
            d->DisableJogButtons(d->ui->cjogplusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogC()) {
                QMessageBox::warning(this, "Jog", "Failed to start C Axis Jog motion");
            } else {
                statusBar()->showMessage("C Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartFJogMinus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogF(false)) {
                QMessageBox::warning(this, "Jog", "Failed to start F Axis Jog motion");
            } else {
                statusBar()->showMessage("F Axis is moving to minus direction");
            }
            d->DisableJogButtons(d->ui->fjogminusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogF()) {
                QMessageBox::warning(this, "Jog", "Failed to start F Axis Jog motion");
            } else {
                statusBar()->showMessage("F Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onStartFJogPlus(bool bChecked) {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);

        statusBar()->clearMessage();
        if (bChecked) {
            if (!d->control->StartJogF(true)) {
                QMessageBox::warning(this, "Jog", "Failed to start F Axis Jog motion");
            } else {
                statusBar()->showMessage("F Axis is moving to plus direction");
            }
            d->DisableJogButtons(d->ui->fjogplusBtn);
            d->updateTimer.start(300);
        } else {
            if (!d->control->StopJogF()) {
                QMessageBox::warning(this, "Jog", "Failed to start F Axis Jog motion");
            } else {
                statusBar()->showMessage("F Axis is stopped");
            }
            d->DisableJogButtons();
            d->updateTimer.stop();
            onUpdatePosition();
        }
        setDisabled(false);
    }

    void MainWindow::onZScanConditionChanged(int value) {
        Q_UNUSED(value);
        const auto distance = d->ui->zscanslicecountSpin->value() * d->ui->zscanslicespacingSpin->value();
        d->ui->zscandistanceSpin->setValue(distance);
    }

    void MainWindow::onZScan() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);
        statusBar()->clearMessage();
        if (!d->control->ZScan(d->ui->zscanstartSpin->value(),
            d->ui->zscanslicecountSpin->value(),
            d->ui->zscanslicespacingSpin->value(),
            d->ui->zscanlastmoveSpin->value())) {
            QMessageBox::warning(this, "Z Scan", "Failed to start Z Scanning");
        } else {
            statusBar()->showMessage("Z Scanning is finished");
        }
        onUpdatePosition();
        setDisabled(false);
    }

    void MainWindow::onZScanFL() {
        setDisabled(true);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 10);
        statusBar()->clearMessage();
        if (!d->control->ZScanFL(d->ui->zscanstartSpin->value(),
            d->ui->zscanslicecountSpin->value(),
            d->ui->zscanslicespacingSpin->value(),
            d->ui->zscanlastmoveSpin->value(),
            d->ui->zscanFlExposureSpin->value())) {
            QMessageBox::warning(this, "FL Z Scan", "Failed to start FL Z Scanning");
        } else {
            statusBar()->showMessage("FL Z Scanning is finished");
        }
        onUpdatePosition();
        setDisabled(false);
    }

    void MainWindow::onGetGlobalParameter() {
        d->ui->ggpValue->setValue(d->control->GetGlobalParameter(d->ui->ggpAddress->value()));
    }

    void MainWindow::onSetGlobalParameter() {
        if(!d->control->SetGlobalParameter(d->ui->sgpAddr->value(), d->ui->sgpValue->value())) {
            QMessageBox::warning(this, "Set Global Parameter", "Failed to set global parameter");
        }
    }

    void MainWindow::onRunProgram() {
        if(!d->control->RunProgram(d->ui->runValue->value())) {
            QMessageBox::warning(this, "Run Program", "Failed to run program");
        }
    }

    void MainWindow::onUpdatePosition() {
        d->UpdatePosition(Axis::XAxis);
        d->UpdatePosition(Axis::YAxis);
        d->UpdatePosition(Axis::ZAxis);
        d->UpdatePosition(Axis::CAxis);
        d->UpdatePosition(Axis::FAxis);
    }
}
