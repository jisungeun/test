#pragma once
#include <memory>
#include <QString>
#include <QThread>

namespace TC::MotionControl {
    class MainWindowControl : public QThread {
    public:
        enum Axis {
            XAxis,
            YAxis,
            ZAxis,
            CAxis,
            FAxis
        };

    public:
        MainWindowControl();
        ~MainWindowControl();

        auto LoadConfiguration(void)->bool;
        auto MakeDummyConfiguration(void)->void;
        auto Initialize(uint32_t port)->bool;
        auto Homing()->bool;
        auto GetPosition(Axis axis)->int32_t;
        auto GetEncoderPosition(Axis axis)->int32_t;
        auto MoveX(int32_t target)->bool;
        auto MoveY(int32_t target)->bool;
        auto MoveZ(int32_t target)->bool;
        auto MoveC(int32_t target)->bool;
        auto MoveF(int32_t target)->bool;
        auto MoveRelX(int32_t target)->bool;
        auto MoveRelY(int32_t target)->bool;
        auto MoveRelZ(int32_t target)->bool;
        auto MoveRelC(int32_t target)->bool;
        auto MoveRelF(int32_t target)->bool;
        auto StartJogX(bool plusDirection)->bool;
        auto StartJogY(bool plusDirection)->bool;
        auto StartJogZ(bool plusDirection)->bool;
        auto StartJogC(bool plusDirection)->bool;
        auto StartJogF(bool plusDirection)->bool;
        auto StopJogX()->bool;
        auto StopJogY()->bool;
        auto StopJogZ()->bool;
        auto StopJogC()->bool;
        auto StopJogF()->bool;
        auto ZScan(int32_t start, uint32_t slices, uint32_t spacing, int32_t lastMove)->bool;
        auto ZScanFL(int32_t start, uint32_t slices, uint32_t spacing, int32_t lastMove, int32_t flExposure)->bool;
        auto GetFirmwareTag()->QString;
        auto GetGlobalParameter(int32_t addr)->int32_t;
        auto SetGlobalParameter(int32_t addr, int32_t value)->bool;
        auto RunProgram(int32_t value)->bool;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
