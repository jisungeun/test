#define LOGGER_TAG "[MainWindowControl]"

#include <QString>
#include <QDir>
#include <QStandardPaths>
#include <QMutexLocker>
#include <QWaitCondition>

#include <TCLogger.h>
#include <MotionControlFactory.h>
#include <MotionConfigIO.h>

#include "MainWindow.h"
#include "MainWindowControl.h"

namespace TC::MotionControl {
    struct MainWindowControl::Impl {
        volatile bool running{ true };
        QMutex cmdMutex;
        QWaitCondition cmdWait;
        QMutex respMutex;
        QWaitCondition respWait;

        enum class Command {
            Idle,
            Initialization,
            Homing,
            PositionX,
            PositionY,
            PositionZ,
            PositionC,
            PositionF,
            PositionEncoderX,
            PositionEncoderY,
            PositionEncoderZ,
            PositionEncoderC,
            PositionEncoderF,
            MoveX,
            MoveY,
            MoveZ,
            MoveC,
            MoveF,
            StartJogX,
            StartJogY,
            StartJogZ,
            StartJogC,
            StartJogF,
            StopJogX,
            StopJogY,
            StopJogZ,
            StopJogC,
            StopJogF,
            ZScan,
            ZScanFL,
            GetFirmwareTag,
            GetGlobalParameter,
            SetGlobalParameter,
            RunProgram
        } command{ Command::Idle };
        bool response;
        struct {
            int32_t x{ 0 };
            int32_t y{ 0 };
            int32_t z{ 0 };
            int32_t c{ 0 };
            int32_t f{ 0 };
        } position;
        struct {
            int32_t x{ 0 };
            int32_t y{ 0 };
            int32_t z{ 0 };
            int32_t c{ 0 };
            int32_t f{ 0 };
        } encPosition;
        struct {
            int32_t x{ 0 };
            int32_t y{ 0 };
            int32_t z{ 0 };
            int32_t c{ 0 };
            int32_t f{ 0 };
        } target;

        ZScanParameter zscan;

        MotionConfig config;

        QString firmwareTag;

        struct {
            int32_t addr;
            int32_t value;
        } manulOperation;

        auto RunAndWait(Command cmd, uint32_t seconds = 0)->bool {
            cmdMutex.lock();
            command = cmd;
            cmdWait.wakeOne();

            respMutex.lock();
            cmdMutex.unlock();
            const auto noTimeout = (seconds > 0) ? respWait.wait(&respMutex, seconds*1000) : respWait.wait(&respMutex);
            respMutex.unlock();

            if (!noTimeout) return false;

            return response;
        }
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
        start();
    }

    MainWindowControl::~MainWindowControl() {
        d->cmdMutex.lock();
        d->running = false;
        d->cmdWait.wakeOne();
        d->cmdMutex.unlock();
        wait();
    }

    auto MainWindowControl::LoadConfiguration() -> bool {
        const auto configPath = QString("%1/config/motionconfig.ini").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        return MotionConfigIO::Load(configPath, d->config);
    }

    auto MainWindowControl::MakeDummyConfiguration() -> void {
        const auto configPath = QString("%1/config/motionconfig.ini").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        if (!QFile::exists(configPath)) {
            QFileInfo finfo(configPath);
            QDir().mkpath(finfo.absolutePath());
        }

        d->config.serialPort.port = 3;
        d->config.serialPort.baudrate = 115200;

        d->config.axisInfo[0].pulses_per_mm = 6400;
        d->config.axisInfo[0].um_per_1k_pulse = 156.250;
        d->config.axisInfo[0].lower_limit_pulse = 0;
        d->config.axisInfo[0].upper_limit_pulse = 10000;
        d->config.axisInfo[0].homing_timeout = 20;
        d->config.axisInfo[0].moving_timeout_per_10k_pulse = 10.0;

        d->config.axisInfo[1].pulses_per_mm = 6400;
        d->config.axisInfo[1].um_per_1k_pulse = 156.250;
        d->config.axisInfo[1].lower_limit_pulse = 0;
        d->config.axisInfo[1].upper_limit_pulse = 10000;
        d->config.axisInfo[1].homing_timeout = 20;
        d->config.axisInfo[1].moving_timeout_per_10k_pulse = 10.0;

        d->config.axisInfo[2].pulses_per_mm = 25600;
        d->config.axisInfo[2].um_per_1k_pulse = 39.0625;
        d->config.axisInfo[2].lower_limit_pulse = 0;
        d->config.axisInfo[2].upper_limit_pulse = 10000;
        d->config.axisInfo[2].homing_timeout = 20;
        d->config.axisInfo[2].moving_timeout_per_10k_pulse = 10.0;

        d->config.zScan.timeout = 10;

        d->config.valid = true;

        MotionConfigIO::Save(configPath, d->config);
    }

    auto MainWindowControl::Initialize(uint32_t port) -> bool {
        d->config.serialPort.port = port;
        return d->RunAndWait(Impl::Command::Initialization, 10);
    }

    auto MainWindowControl::Homing() -> bool {
        return d->RunAndWait(Impl::Command::Homing, 60);
    }

    auto MainWindowControl::GetPosition(Axis axis) -> int32_t {
        auto cmd = [=]()->Impl::Command {
            switch(axis) {
            case Axis::XAxis:
                return Impl::Command::PositionX;
            case Axis::YAxis:
                return Impl::Command::PositionY;
            case Axis::ZAxis:
                return Impl::Command::PositionZ;
            case Axis::CAxis:
                return Impl::Command::PositionC;
            case Axis::FAxis:
                return Impl::Command::PositionF;
            }
            return Impl::Command::PositionX;
        }();

        if(!d->RunAndWait(cmd)) return 0;

        return [=]()->int32_t {
            switch(axis) {
            case Axis::XAxis:
                return d->position.x;
            case Axis::YAxis:
                return d->position.y;
            case Axis::ZAxis:
                return d->position.z;
            case Axis::CAxis:
                return d->position.c;
            case Axis::FAxis:
                return d->position.f;
            }
            return 0;
        }();
    }

    auto MainWindowControl::GetEncoderPosition(Axis axis) -> int32_t {
        auto cmd = [=]()->Impl::Command {
            switch(axis) {
            case Axis::XAxis:
                return Impl::Command::PositionEncoderX;
            case Axis::YAxis:
                return Impl::Command::PositionEncoderY;
            case Axis::ZAxis:
                return Impl::Command::PositionEncoderZ;
            case Axis::CAxis:
                return Impl::Command::PositionEncoderC;
            case Axis::FAxis:
                return Impl::Command::PositionEncoderF;
            }
            return Impl::Command::PositionX;
        }();

        if(!d->RunAndWait(cmd)) return 0;

        return [=]()->int32_t {
            switch(axis) {
            case Axis::XAxis:
                return d->encPosition.x;
            case Axis::YAxis:
                return d->encPosition.y;
            case Axis::ZAxis:
                return d->encPosition.z;
            case Axis::CAxis:
                return d->encPosition.c;
            case Axis::FAxis:
                return d->encPosition.f;
            }
            return 0;
        }();
    }

    auto MainWindowControl::MoveX(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionX)) return false;

        const int32_t targetInput = target;

        const auto maxLimit = d->config.axisInfo[MotionConfig::AxisIndex::XAxisIndex].upper_limit_pulse;
        const auto minLimit = d->config.axisInfo[MotionConfig::AxisIndex::XAxisIndex].lower_limit_pulse;
        if (target > maxLimit) target = maxLimit;
        else if (target < minLimit) target = minLimit;

        auto relDistance = target - d->position.x;

        QLOG_INFO() << "X Move - input: " << targetInput << " Adjusted: " << target << " Rel. distance: " << relDistance;

        d->target.x = relDistance;
        return d->RunAndWait(Impl::Command::MoveX);
    }

    auto MainWindowControl::MoveY(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionY)) return false;

        const int32_t targetInput = target;

        const auto maxLimit = d->config.axisInfo[MotionConfig::AxisIndex::YAxisIndex].upper_limit_pulse;
        const auto minLimit = d->config.axisInfo[MotionConfig::AxisIndex::YAxisIndex].lower_limit_pulse;
        if (target > maxLimit) target = maxLimit;
        else if (target < minLimit) target = minLimit;

        auto relDistance = target - d->position.y;

        QLOG_INFO() << "Y Move - input: " << targetInput << " Adjusted: " << target << " Rel. distance: " << relDistance;

        d->target.y = relDistance;
        return d->RunAndWait(Impl::Command::MoveY);
    }

    auto MainWindowControl::MoveZ(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionZ)) return false;

        const int32_t targetInput = target;

        const auto maxLimit = d->config.axisInfo[MotionConfig::AxisIndex::ZAxisIndex].upper_limit_pulse;
        const auto minLimit = d->config.axisInfo[MotionConfig::AxisIndex::ZAxisIndex].lower_limit_pulse;
        if (target > maxLimit) target = maxLimit;
        else if (target < minLimit) target = minLimit;

        auto relDistance = target - d->position.z;

        QLOG_INFO() << "Z Move - input: " << targetInput << " Adjusted: " << target << " Rel. distance: " << relDistance;

        d->target.z = relDistance;
        return d->RunAndWait(Impl::Command::MoveZ);
    }

    auto MainWindowControl::MoveC(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionC)) return false;

        const int32_t targetInput = target;

        const auto maxLimit = d->config.axisInfo[MotionConfig::AxisIndex::CAxisIndex].upper_limit_pulse;
        const auto minLimit = d->config.axisInfo[MotionConfig::AxisIndex::CAxisIndex].lower_limit_pulse;
        if (target > maxLimit) target = maxLimit;
        else if (target < minLimit) target = minLimit;

        auto relDistance = target - d->position.c;

        QLOG_INFO() << "Z Move - input: " << targetInput << " Adjusted: " << target << " Rel. distance: " << relDistance;

        d->target.c = relDistance;
        return d->RunAndWait(Impl::Command::MoveC);
    }

    auto MainWindowControl::MoveF(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionF)) return false;

        const int32_t targetInput = target;

        const auto maxLimit = d->config.axisInfo[MotionConfig::AxisIndex::FAxisIndex].upper_limit_pulse;
        const auto minLimit = d->config.axisInfo[MotionConfig::AxisIndex::FAxisIndex].lower_limit_pulse;
        if (target > maxLimit) target = maxLimit;
        else if (target < minLimit) target = minLimit;

        auto relDistance = target - d->position.f;

        QLOG_INFO() << "F Move - input: " << targetInput << " Adjusted: " << target << " Rel. distance: " << relDistance;

        d->target.f = relDistance;
        return d->RunAndWait(Impl::Command::MoveF);
    }

    auto MainWindowControl::MoveRelX(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionX)) return false;
        QLOG_INFO() << "X RelMove: " << target;
        auto absTarget = d->position.x + target;
        return MoveX(absTarget);
    }

    auto MainWindowControl::MoveRelY(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionY)) return false;
        QLOG_INFO() << "Y RelMove: " << target;
        auto absTarget = d->position.y + target;
        return MoveY(absTarget);
    }

    auto MainWindowControl::MoveRelZ(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionZ)) return false;
        QLOG_INFO() << "Z RelMove: " << target;
        auto absTarget = d->position.z + target;
        return MoveZ(absTarget);
    }

    auto MainWindowControl::MoveRelC(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionC)) return false;
        QLOG_INFO() << "F RelMove: " << target;
        auto absTarget = d->position.c + target;
        return MoveC(absTarget);
    }

    auto MainWindowControl::MoveRelF(int32_t target) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionF)) return false;
        QLOG_INFO() << "C RelMove: " << target;
        auto absTarget = d->position.f + target;
        return MoveF(absTarget);
    }

    auto MainWindowControl::StartJogX(bool plusDirection) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionX)) return false;
        auto relDistance = [=]()->int64_t {
            if (plusDirection) {
                return d->config.axisInfo[MotionConfig::AxisIndex::XAxisIndex].upper_limit_pulse - d->position.x;
            }
            return d->config.axisInfo[MotionConfig::AxisIndex::XAxisIndex].lower_limit_pulse - d->position.x;
        }();
        d->target.x = relDistance;
        return d->RunAndWait(Impl::Command::StartJogX);
    }

    auto MainWindowControl::StartJogY(bool plusDirection) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionY)) return false;
        auto relDistance = [=]()->int64_t {
            if (plusDirection) {
                return d->config.axisInfo[MotionConfig::AxisIndex::YAxisIndex].upper_limit_pulse - d->position.y;
            }
            return d->config.axisInfo[MotionConfig::AxisIndex::YAxisIndex].lower_limit_pulse - d->position.y;
        }();
        d->target.y = relDistance;
        return d->RunAndWait(Impl::Command::StartJogY);
    }

    auto MainWindowControl::StartJogZ(bool plusDirection) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionZ)) return false;
        auto relDistance = [=]()->int64_t {
            if (plusDirection) {
                return d->config.axisInfo[MotionConfig::AxisIndex::ZAxisIndex].upper_limit_pulse - d->position.z;
            }
            return d->config.axisInfo[MotionConfig::AxisIndex::ZAxisIndex].lower_limit_pulse - d->position.z;
        }();
        d->target.z = relDistance;
        return d->RunAndWait(Impl::Command::StartJogZ);
    }

    auto MainWindowControl::StartJogC(bool plusDirection) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionC)) return false;
        auto relDistance = [=]()->int64_t {
            if (plusDirection) {
                return d->config.axisInfo[MotionConfig::AxisIndex::CAxisIndex].upper_limit_pulse - d->position.c;
            }
            return d->config.axisInfo[MotionConfig::AxisIndex::CAxisIndex].lower_limit_pulse - d->position.c;
        }();
        d->target.c = relDistance;
        return d->RunAndWait(Impl::Command::StartJogC);
    }

    auto MainWindowControl::StartJogF(bool plusDirection) -> bool {
        if (!d->RunAndWait(Impl::Command::PositionF)) return false;
        auto relDistance = [=]()->int64_t {
            if (plusDirection) {
                return d->config.axisInfo[MotionConfig::AxisIndex::FAxisIndex].upper_limit_pulse - d->position.f;
            }
            return d->config.axisInfo[MotionConfig::AxisIndex::FAxisIndex].lower_limit_pulse - d->position.f;
        }();
        d->target.f = relDistance;
        return d->RunAndWait(Impl::Command::StartJogF);
    }

    auto MainWindowControl::StopJogX() -> bool {
        return d->RunAndWait(Impl::Command::StopJogX);
    }

    auto MainWindowControl::StopJogY() -> bool {
        return d->RunAndWait(Impl::Command::StopJogY);
    }

    auto MainWindowControl::StopJogZ() -> bool {
        return d->RunAndWait(Impl::Command::StopJogZ);
    }

    auto MainWindowControl::StopJogC() -> bool {
        return d->RunAndWait(Impl::Command::StopJogC);
    }

    auto MainWindowControl::StopJogF() -> bool {
        return d->RunAndWait(Impl::Command::StopJogF);
    }

    auto MainWindowControl::ZScan(int32_t start, uint32_t slices, uint32_t spacing, int32_t lastMove) -> bool {
        d->zscan.startPos = start;
        d->zscan.sliceCount = slices;
        d->zscan.sliceSpacing = spacing;
        d->zscan.relativeLastMove = lastMove;

        QLOG_INFO() << "Z Scan - start: " << start << " slices: " << slices << " spacing: " << spacing << " lastMove: " << lastMove;

        return d->RunAndWait(Impl::Command::ZScan);
    }

    auto MainWindowControl::ZScanFL(int32_t start, uint32_t slices, uint32_t spacing, int32_t lastMove,
        int32_t flExposure) -> bool {
        d->zscan.startPos = start;
        d->zscan.sliceCount = slices;
        d->zscan.sliceSpacing = spacing;
        d->zscan.relativeLastMove = lastMove;
        d->zscan.flExposure = flExposure;

        QLOG_INFO() << "Z Scan - start: " << start << " slices: " << slices << " spacing: " << spacing << " lastMove: " << lastMove << " flExposure: " << flExposure;

        return d->RunAndWait(Impl::Command::ZScanFL);
    }

    auto MainWindowControl::GetFirmwareTag() -> QString {
        d->RunAndWait(Impl::Command::GetFirmwareTag);
        return d->firmwareTag;
    }

    auto MainWindowControl::GetGlobalParameter(int32_t addr) -> int32_t {
        d->manulOperation.addr = addr;
        if(d->RunAndWait(Impl::Command::GetGlobalParameter)) {
            return d->manulOperation.value;
        }
        return 0;
    }

    auto MainWindowControl::SetGlobalParameter(int32_t addr, int32_t value) -> bool {
        d->manulOperation.addr = addr;
        d->manulOperation.value = value;
        return d->RunAndWait(Impl::Command::SetGlobalParameter);
    }

    auto MainWindowControl::RunProgram(int32_t value) -> bool {
        d->manulOperation.value = value;
        return d->RunAndWait(Impl::Command::RunProgram);
    }

    void MainWindowControl::run() {
        MotionControl::Pointer motionControl{ nullptr };

        while (true) {
            QMutexLocker locker(&d->cmdMutex);
            if (!d->running) break;
            const auto command = d->command;
            if (command == Impl::Command::Idle) {
                d->cmdWait.wait(locker.mutex());
                continue;
            }
            d->command = Impl::Command::Idle;   //clear
            locker.unlock();

            QMutexLocker respLocker(&d->respMutex);
            switch(command) {
            case Impl::Command::Initialization:
                motionControl = MotionControlFactory::Create(MotionControlFactory::Controller::TMCM6214, d->config);
                d->response = motionControl->Initialize();
                break;
            case Impl::Command::Homing:
                d->response = motionControl->Homing();
                break;
            case Impl::Command::PositionX:
                d->response = true;
                d->position.x = motionControl->GetPosition(MotionControl::Axis::XAxis);
                break;
            case Impl::Command::PositionY:
                d->response = true;
                d->position.y = motionControl->GetPosition(MotionControl::Axis::YAxis);
                break;
            case Impl::Command::PositionZ:
                d->response = true;
                d->position.z = motionControl->GetPosition(MotionControl::Axis::ZAxis);
                break;
            case Impl::Command::PositionC:
                d->response = true;
                d->position.c = motionControl->GetPosition(MotionControl::Axis::CAxis);
                break;
            case Impl::Command::PositionF:
                d->response = true;
                d->position.f = motionControl->GetPosition(MotionControl::Axis::FAxis);
                break;
            case Impl::Command::PositionEncoderX:
                d->response = true;
                d->encPosition.x = motionControl->GetEncoderPosition(MotionControl::Axis::XAxis);
                break;
            case Impl::Command::PositionEncoderY:
                d->response = true;
                d->encPosition.y = motionControl->GetEncoderPosition(MotionControl::Axis::YAxis);
                break;
            case Impl::Command::PositionEncoderZ:
                d->response = true;
                d->encPosition.z = motionControl->GetEncoderPosition(MotionControl::Axis::ZAxis);
                break;
            case Impl::Command::PositionEncoderC:
                d->response = true;
                d->encPosition.c = motionControl->GetEncoderPosition(MotionControl::Axis::CAxis);
                break;
            case Impl::Command::PositionEncoderF:
                d->response = true;
                d->encPosition.f = motionControl->GetEncoderPosition(MotionControl::Axis::FAxis);
                break;
            case Impl::Command::MoveX:
                d->response = motionControl->Move(MotionControl::Axis::XAxis, d->target.x);
                break;
            case Impl::Command::MoveY:
                d->response = motionControl->Move(MotionControl::Axis::YAxis, d->target.y);
                break;
            case Impl::Command::MoveZ:
                d->response = motionControl->Move(MotionControl::Axis::ZAxis, d->target.z);
                break;
            case Impl::Command::MoveC:
                d->response = motionControl->Move(MotionControl::Axis::CAxis, d->target.c);
                break;
            case Impl::Command::MoveF:
                d->response = motionControl->Move(MotionControl::Axis::FAxis, d->target.f);
                break;
            case Impl::Command::StartJogX:
                d->response = motionControl->StartJog(MotionControl::Axis::XAxis, d->target.x);
                break;
            case Impl::Command::StartJogY:
                d->response = motionControl->StartJog(MotionControl::Axis::YAxis, d->target.y);
                break;
            case Impl::Command::StartJogZ:
                d->response = motionControl->StartJog(MotionControl::Axis::ZAxis, d->target.z);
                break;
            case Impl::Command::StartJogC:
                d->response = motionControl->StartJog(MotionControl::Axis::CAxis, d->target.c);
                break;
            case Impl::Command::StartJogF:
                d->response = motionControl->StartJog(MotionControl::Axis::FAxis, d->target.f);
                break;
            case Impl::Command::StopJogX:
                d->response = motionControl->StopJog(MotionControl::Axis::XAxis);
                break;
            case Impl::Command::StopJogY:
                d->response = motionControl->StopJog(MotionControl::Axis::YAxis);
                break;
            case Impl::Command::StopJogZ:
                d->response = motionControl->StopJog(MotionControl::Axis::ZAxis);
                break;
            case Impl::Command::StopJogC:
                d->response = motionControl->StopJog(MotionControl::Axis::CAxis);
                break;
            case Impl::Command::StopJogF:
                d->response = motionControl->StopJog(MotionControl::Axis::FAxis);
                break;
            case Impl::Command::ZScan:
                d->response = motionControl->ZScan(d->zscan);
                break;
            case Impl::Command::ZScanFL:
                d->response = motionControl->ZScanFL(d->zscan);
                break;
            case Impl::Command::GetFirmwareTag:
                d->response = true;
                d->firmwareTag = motionControl->GetFirmwareTag();
                break;
            case Impl::Command::GetGlobalParameter:
                d->response = motionControl->GetGlobalParameter(d->manulOperation.addr, d->manulOperation.value);
                break;
            case Impl::Command::SetGlobalParameter:
                d->response = motionControl->SetGlobalParameter(d->manulOperation.addr, d->manulOperation.value);
                break;
            case Impl::Command::RunProgram:
                d->response = motionControl->RunProgram(d->manulOperation.value);
                break;
            }
            d->respWait.wakeOne();
        }

        motionControl->CleanUp();
    }
}
