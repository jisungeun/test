#pragma once
#include <memory>

#include "QMainWindow"

namespace TC::MotionControl {
    class MainWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

    protected:
		auto ScanPorts()->void;

	protected slots:
		void onOpenFolder();
		void onInitialization();
		void onHoming();
		void onMoveX();
		void onMoveY();
		void onMoveZ();
		void onMoveC();
		void onMoveF();
		void onRelMoveXMinus();
		void onRelMoveXPlus();
		void onRelMoveYMinus();
		void onRelMoveYPlus();
		void onRelMoveZMinus();
		void onRelMoveZPlus();
		void onRelMoveCMinus();
		void onRelMoveCPlus();
		void onRelMoveFMinus();
		void onRelMoveFPlus();
		void onStartXJogMinus(bool bChecked);
		void onStartXJogPlus(bool bChecked);
		void onStartYJogMinus(bool bChecked);
		void onStartYJogPlus(bool bChecked);
		void onStartZJogMinus(bool bChecked);
		void onStartZJogPlus(bool bChecked);
		void onStartCJogMinus(bool bChecked);
		void onStartCJogPlus(bool bChecked);
		void onStartFJogMinus(bool bChecked);
		void onStartFJogPlus(bool bChecked);
		void onZScanConditionChanged(int value);
		void onZScan();
		void onZScanFL();
		void onGetGlobalParameter();
		void onSetGlobalParameter();
		void onRunProgram();
		void onUpdatePosition();

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}