#pragma once

#include "MotionControl.h"
#include "MotionConfig.h"
#include "TCMotionControlExport.h"

namespace TC::MotionControl {
    class TCMotionControl_API MotionControlFactory {
    public:
        enum class Controller {
            TMCM6212,
            TMCM6214
        };

        static auto Create(Controller type, const MotionConfig& config)->MotionControl::Pointer;
    };
}
