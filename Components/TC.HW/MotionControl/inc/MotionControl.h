#pragma once
#include <memory>
#include <QList>

#include "MotionConfig.h"
#include "MotionErrorCodes.h"
#include "ZScanParameter.h"
#include "TCMotionControlExport.h"

class QString;

namespace TC::MotionControl {
    class TCMotionControl_API MotionControl {
    public:
        typedef std::shared_ptr<MotionControl> Pointer;

        enum Axis {
            XAxis,
            YAxis,
            ZAxis,
            CAxis,
            FAxis,
            UAxis,
            VAxis,
            WAxis
        };

    public:
        MotionControl(const MotionConfig& config);
        virtual ~MotionControl();

        auto SetConfig(const MotionConfig& config)->void;
        auto GetConfig() const->MotionConfig;

        auto Convert(Axis axis, double position) const->int64_t;
        auto Convert(Axis axis, uint64_t position) const->double;

        auto GetLastError() const->MotionErrorCode;

        virtual auto Initialize()->bool = 0;
        virtual auto CleanUp()->void = 0;
        virtual auto Homing()->bool = 0;
        virtual auto Move(Axis axis, int64_t position)->bool = 0;
        virtual auto MoveXY(int64_t xposition, int64_t yposition)->bool = 0;
        virtual auto ZScan(const ZScanParameter& param)->bool = 0;
        virtual auto ZScanFL(const ZScanParameter& param)->bool = 0;
        virtual auto RaiseSingleTrigger()->bool = 0;
        virtual auto StartJog(Axis axis, int64_t position)->bool = 0;
        virtual auto StopJog(Axis axis)->bool = 0;
        virtual auto GetPosition(Axis axis)->int64_t = 0;
        virtual auto GetEncoderPosition(Axis axis)->int64_t = 0;
        virtual auto GetFirmwareTag()->QString = 0;

        virtual auto SetGlobalParameter(int32_t addr, int32_t value)->bool = 0;
        virtual auto GetGlobalParameter(int32_t addr, int32_t& value)->bool = 0;
        virtual auto RunProgram(int32_t value)->bool = 0;

    protected:
        auto SetLastError(MotionErrorCode code)->void;
        auto Update(const QString& message, MotionErrorCode code = MotionErrorCode::NoError)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
