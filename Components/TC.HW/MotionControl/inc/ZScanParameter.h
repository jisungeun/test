#pragma once
#include <memory>

namespace TC::MotionControl {
    struct ZScanParameter {
        int32_t startPos;               // start position in pulses
        int32_t relativeLastMove;       // relative motion amount in pulse after z scan is finished
        uint32_t sliceCount;            // number of Z steps
        int32_t sliceSpacing;           // spacing between slices in pulses
        int32_t flExposure;             // FL exposure
    };
}