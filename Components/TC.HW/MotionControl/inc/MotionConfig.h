#pragma once

#include "TCMotionControlExport.h"

namespace TC::MotionControl {
    struct MotionConfig {
        enum AxisIndex {
            XAxisIndex = 0,
            YAxisIndex = 1,
            ZAxisIndex = 2,
            CAxisIndex = 3,
            FAxisIndex = 4,
            AxisCount
        };

        bool valid{ false };

        struct {
            uint32_t port;
            uint32_t baudrate;
        } serialPort;

        struct AxisInfo {
            uint32_t axisIndex;                         // axis index on motion controller
            uint32_t pulses_per_mm;                     // pulses per 1mm
            double um_per_1k_pulse;                     // um per 1,000 pulses
            int32_t lower_limit_pulse;                  // lower limit in pulses
            int32_t upper_limit_pulse;                  // upper limit in pulses
            uint32_t homing_timeout;                    // homing timeout in seconds
            double moving_timeout_per_10k_pulse;        // moving timeout for 10,000 pulses in seconds
            bool has_encoder;                           // has encoder or not
        };

        AxisInfo axisInfo[AxisIndex::AxisCount];

        struct {
            uint32_t timeout;                           // timeout in seconds
        } zScan;
    };
}
