#pragma once

#include <enum.h>

namespace TC::MotionControl {
    BETTER_ENUM(MotionErrorCode, uint32_t,
                NoError                                 = 0,
                FailedToLoadConfiguration               = 1,
                FailedToOpenPort                        = 2,
                FailedToSendCommand                     = 3,
                SendTimeout                             = 4,
                ResponseTimeout                         = 5,
                InvalidResponse                         = 6,
                RightLimitTriggered                     = 7,
                LeftLimitTriggered                      = 8,
                FailedToGetActualPosition               = 9,
                FailedToSetUserVariable                 = 10,
                InvalidConfiguration                    = 11,
                FailedToResetActualPosition             = 12,
                FailedToResetTargetPosition             = 13,
                FailedToResetEncoderPosition            = 14,
                FailedToReadFirmwareTag                 = 15,

                //Status
                WrongChecksum                           = 100,
                InvalidCommand                          = 101,
                WrongType                               = 102,
                InvalidValue                            = 103,
                EEPROMLocked                            = 104,
                WrongCommand                            = 105,

                //Homing
                FailedToStartXHoming                    = 200,
                FailedToStartYHoming                    = 201,
                FailedToStartZHoming                    = 202,
                FailedToStartCHoming                    = 203,
                FailedToStartFHoming                    = 204,
                XYHomingTimeout                         = 210,
                ZHomingTimeout                          = 211,
                CHomingTimeout                          = 212,
                FHomingTimeout                          = 213,
                FailedToCheckZHomingStatus              = 220,
                FailedToCheckXHomingStatus              = 221,
                FailedToCheckYHomingStatus              = 222,
                FailedToCheckCHomingStatus              = 223,
                FailedToCheckFHomingStatus              = 224,
                FailedToMoveXAxisToOrigin               = 230,
                FailedToMoveYAxisToOrigin               = 231,
                FailedToMoveZAxisToOrigin               = 232,
                FailedToMoveCAxisToOrigin               = 233,
                FailedToMoveFAxisToOrigin               = 234,
                FailedToResetXAxisOrigin                = 240,
                FailedToResetYAxisOrigin                = 241,
                FailedToResetZAxisOrigin                = 242,
                FailedToResetCAxisOrigin                = 243,
                FailedToResetFAxisOrigin                = 244,
                FailedToResetXAxisEncoderOrigin         = 250,
                FailedToResetYAxisEncoderOrigin         = 251,
                FailedToResetZAxisEncoderOrigin         = 252,
                FailedToResetCAxisEncoderOrigin         = 253,
                FailedToResetFAxisEncoderOrigin         = 254,

                //Move
                FailedToStartMoving                     = 300,
                FailedToCheckRightLimit                 = 301,
                FailedToCheckLeftLimit                  = 302,
                FailedToCheckArrival                    = 303,
                MotionTimeout                           = 304,

                //Jog motion
                FailedToStartJogMotion                  = 400,
                FailedToStopJogMotion                   = 401,

                //Z Scan
                FailedToSetSlices                       = 500,
                FailedToSetSteps                        = 501,
                FailedToSetZStart                       = 502,
                FailedToSetZLast                        = 503,
                FailedToGetStartingPosition             = 504,
                FailedToStartZScan                      = 505,
                ZScanTimeout                            = 506,
                FailedToSetFLExposure                   = 507,

                //Single trigger
                FailedToRaiseTrigger                    = 600,

                //Global Parameter
                FailedToSetGlobalParameter              = 620,
                FailedToGetGlobalParameter              = 621,

                //Run Program
                FailedToRunProgram                      = 630,

                //Last error code
                UnknownError                            = 10000
    )
}
