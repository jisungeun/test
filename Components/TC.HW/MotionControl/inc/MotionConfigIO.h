#pragma once
#include <memory>

#include <QString>

#include "MotionConfig.h"
#include "TCMotionControlExport.h"

namespace TC::MotionControl {
    class TCMotionControl_API MotionConfigIO {
    public:
        static auto Load(const QString& path, MotionConfig& config)->bool;
        static auto Save(const QString& path, const MotionConfig& config)->bool;
    };
}
