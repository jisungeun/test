#pragma once
#include <memory>

#include <QVariant>
#include <QString>

namespace TC::MCUControl {
    class Settings {
    public:
        typedef std::shared_ptr<Settings> Pointer;

    public:
        Settings();
        virtual ~Settings();

        static auto GetInstance()->Pointer;

        auto GetValue(const QString& key) const->QVariant;

        static auto GetSimulation()->bool;
        static auto GetConfigPath()->QString;
        static auto GetMotionCommandsPath()->QString;
        static auto GetMacrosPath()->QString;
    };
}