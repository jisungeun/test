#include <QAbstractButton>

#include "AddMotionCommandDialogControl.h"
#include "AddMotionCommandDialog.h"
#include "ui_AddMotionCommandDialog.h"

namespace TC::MCUControl {
    struct AddMotionCommandDialog::Impl {
        Ui::AddMotionCommandDialog ui;
        AddMotionCommandDialogControl control;
    };

    AddMotionCommandDialog::AddMotionCommandDialog(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        setWindowTitle("Motion Command");
        for (auto button : d->ui.buttonBox->buttons()) {
            button->setFixedWidth(70);
        }

        auto types = d->control.GetTypes();
        for (auto type : types) {
            d->ui.typeCombo->addItem(type.title, type.type._to_integral());
        }
    }

    AddMotionCommandDialog::~AddMotionCommandDialog() {
    }

    auto AddMotionCommandDialog::GetType() const -> MotionCommandType {
        auto idx = d->ui.typeCombo->currentIndex();
        auto type = MotionCommandType::_from_integral(d->ui.typeCombo->itemData(idx).toInt());
        return type;
    }

    auto AddMotionCommandDialog::GetTitle() const -> QString {
        return d->ui.titleEdit->text();
    }

    void AddMotionCommandDialog::done(int r) {
        QDialog::done(r);
    }
}