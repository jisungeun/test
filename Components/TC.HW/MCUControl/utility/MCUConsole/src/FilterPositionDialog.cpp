#include <QTableWidgetItem>
#include <QItemDelegate>
#include <QSpinBox>

#include "FilterPositionDialogControl.h"
#include "ui_FilterPositionDialog.h"
#include "FilterPositionDialog.h"

namespace TC::MCUControl {
    class Delegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem & option,
                              const QModelIndex & index) const override {
            auto *spinBox = new QSpinBox(parent);
            spinBox->setButtonSymbols(QAbstractSpinBox::ButtonSymbols::NoButtons);
            spinBox->setMinimum(0);
            spinBox->setMaximum(1000000000);
            return spinBox;
        }
    };

    struct FilterPositionDialog::Impl {
        Ui::FilterPositionDialog ui;
        FilterPositionDialogControl control;
    };

    FilterPositionDialog::FilterPositionDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.tableWidget->setColumnCount(2);
        d->ui.tableWidget->setHorizontalHeaderLabels({"Channel", "Position"});
        d->ui.tableWidget->verticalHeader()->hide();
        d->ui.tableWidget->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.tableWidget->setRowCount(20);

        auto positions = d->control.GetPositions();
        SetPositions(positions);
    }

    FilterPositionDialog::~FilterPositionDialog() {
    }

    auto FilterPositionDialog::GetPositions() const -> QList<int32_t> {
        QList<int32_t> positions;

        const int rowCount = d->ui.tableWidget->rowCount();
        for(int32_t idx=0; idx<rowCount; idx++) {
            const auto *item = d->ui.tableWidget->item(idx, 0);
            if(!item) continue;
            bool ok;
            item->text().toInt(&ok);
            if(!ok) continue;

            positions.push_back(d->ui.tableWidget->item(idx, 1)->text().toInt());
        }

        return positions;
    }

    void FilterPositionDialog::done(int r) {
        auto accepted = (r == QDialog::DialogCode::Accepted);
        if (!accepted) {
            QDialog::done(r);
            return;
        }

        const auto positions = GetPositions();
        d->control.SavePositions(positions);
    }

    auto FilterPositionDialog::SetPositions(const QList<int32_t>& positions) -> void {
        const int rowCount = positions.count();

        d->ui.tableWidget->clearContents();
        if(rowCount == 0) return;

        d->ui.tableWidget->setRowCount(rowCount);

        for(int32_t idx=0; idx<rowCount; idx++) {
            auto* itemCh = new QTableWidgetItem(QString::number(idx));
            itemCh->setFlags(itemCh->flags() & ~Qt::ItemIsEditable);
            d->ui.tableWidget->setItem(idx, 0, itemCh);

            auto* itemPos = new QTableWidgetItem(QString::number(positions.at(idx)));
            d->ui.tableWidget->setItemDelegate(new Delegate());
            d->ui.tableWidget->setItem(idx, 1, itemPos);
        }
    }
}
