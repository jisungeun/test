#pragma once
#include <memory>
#include <QObject>

namespace TC::MCUControl {
    class MCUReplayerObserver : public QObject {
        Q_OBJECT

    public:
        MCUReplayerObserver(QObject* parent = nullptr);
        ~MCUReplayerObserver() override;

        auto UpdateProgress(int32_t count)->void;
        auto UpdateStopped(const QString& message)->void;
        auto UpdateFinished()->void;

    signals:
        void sigUpdate(int32_t count);
        void sigStopped(const QString& message);
        void sigFinished();
    };
}