#pragma once
#include <memory>

#include <QWidget>

namespace TC::MCUControl {
    class ControlPanel : public QWidget {
        Q_OBJECT
    public:
        ControlPanel(QWidget* parent = nullptr);
        virtual ~ControlPanel();

    protected slots:
        void onInitializeAuto();
        void onOpenPort();
        void onSetSafeZPos();
        void onStartInitialization();
        void onFinishCalibration();
        void onCheckDLPCVersion();
        void onSetLEDIntensity();
        void onSetDMDExposure();
        void onReadTriggerCount();
        void onResetTriggerCount();
        void onStartTest();
        void onRunMacro();
        void onRunMacroManual();
        void onRunMatrixMacro();
        void onStopMacro();
        void onCheckMacro();
        void onMoveX();
        void onMoveY();
        void onMoveZ();
        void onMoveU();
        void onMoveV();
        void onMoveW();
        void onSetFilterPositions();
        void onChangeFilter();
        void onSetLEDChannel();
        void onSetCameraType();
        void onGenerateTrigger();
        void onGenerateTrigger1();
        void onFinishTest();
        void onStartManualMode();
        void onStopManualMode();
        void onMoveX1();
        void onMoveY1();
        void onMoveZ1();
        void onMoveU1();
        void onMoveV1();
        void onMoveW1();
        void onMoveX1StepMinus();
        void onMoveX1StepPlus();
        void onMoveY1StepMinus();
        void onMoveY1StepPlus();
        void onMoveZ1StepMinus();
        void onMoveZ1StepPlus();
        void onMoveU1StepMinus();
        void onMoveU1StepPlus();
        void onMoveV1StepMinus();
        void onMoveV1StepPlus();
        void onMoveW1StepMinus();
        void onMoveW1StepPlus();
        void onMoveX2();
        void onMoveY2();
        void onMoveZ2();
        void onMoveX2StepMinus();
        void onMoveX2StepPlus();
        void onMoveY2StepMinus();
        void onMoveY2StepPlus();
        void onMoveZ2StepMinus();
        void onMoveZ2StepPlus();
        void onMoveU2();
        void onMoveV2();
        void onMoveW2();
        void onMoveU2StepMinus();
        void onMoveU2StepPlus();
        void onMoveV2StepMinus();
        void onMoveV2StepPlus();
        void onMoveW2StepMinus();
        void onMoveW2StepPlus();
        void onSetDO();
        void onGetDI();
        void onSetLEDChannel2();
        void onSetCameraType2();
        void onSetRGBLedIntensity();
        void onMoveXY();
        void onStopMotion();
        void onRecover();
        void onCheckMovingStatus();
        void onGetAxisPosition();
        void onReadTemperature();
        void onReadAcceleration();
        void onReadCartridgeSensor();
        void onSetMacroStreaming();
        void onSetMacroAFMode(int mode);
        void onAddCommandsToStreamBuffer();
        void onRunMacroStreaming();
        void onStopMacroStreaming();
        void onCheckInitProgress();
        void onSetSWLimit();
        void onSetInPositionBand();
        void onSetAFParameter();
        void onSetAFTarget();
        void onInitAF();
        void onCheckAFInit();
        void onEnableAF();
        void onCheckAFStatus();
        void onChangeAFLensParameter();
        void onSetAFPeakMode();
        void onSetManualAF();
        void onReadAFLog();
        void onScanZForAFM();
        void onDumpZScanForAFM();
        void onCopyAFSensorValues();
        void onCopyAFLogs();
        void onOpenReplayLog();
        void onReplayMCU();
        void onStopReplay();
        void onUpdateReplayProgress(int32_t count);
        void onReplayStopped(const QString& message);
        void onReplayFinished();
        void onSendCustomPacket();

    public slots:
        void onDisableCheck(bool disable);

    signals:
        void sigUpdateFirmareVersion(const QString& ver);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}