#pragma once
#include <memory>

#include <QString>

#include <MCUDefines.h>
#include <MCUResponse.h>

namespace TC::MCUControl {
    class StatusPanelControl {
    public:
        StatusPanelControl();
        virtual ~StatusPanelControl();

        auto UpdateResponse(const MCUResponse& response)->void;

        auto GetState() const->QString;
        auto GetMacroStep() const->uint32_t;

        auto GetAxisAtStart(Axis axis) const->bool;
        auto GetAxisAtEnd(Axis axis) const->bool;
        auto GetAxisMoving(Axis axis) const->bool;
        auto GetAxisPosition(Axis axis) const->int32_t;
        auto GetAxisSWLimit(Axis axis) const->bool;

        auto GetLEDOn(int32_t led) const->bool;
        auto GetAfOn() const->bool;
        auto GetAfError() const->bool;

        auto GetAngle(int32_t direction) const->float;

        auto GetTemperature(int32_t index) const->float;

        auto GetStreamingIndex() const->int32_t;
        auto GetStreamingBufferAvailable() const->int32_t;
        auto GetStreamingBufferHalfEmpty() const->bool;
        auto GetStreamingError() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}