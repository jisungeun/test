#include <MCUFactory.h>

#include "Settings.h"
#include "MacroPanelControl.h"

namespace TC::MCUControl {
    struct MacroPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    MacroPanelControl::MacroPanelControl() : d{ new Impl } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    MacroPanelControl::~MacroPanelControl() {
    }

    auto MacroPanelControl::GetMotionCommands(const QString& path) -> QList<IMCUMotionCommand::Pointer> {
        QList<IMCUMotionCommand::Pointer> commands;

        auto repo = MCUMotionCommandRepository::GetInstance();
        if (repo->Load(path)) {
            const auto count = repo->GetCounts();
            for (uint32_t idx = 0; idx < count; idx++) {
                commands.push_back(repo->CloneCommandByIndex(idx));
            }
        }

        return commands;
    }

    auto MacroPanelControl::Load(const QString& path) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        return repo->Load(path);
    }

    auto MacroPanelControl::Save(const QString& path) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        return repo->Save(path);
    }

    auto MacroPanelControl::Upload() -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        const auto count = repo->GetCounts();
        for (uint32_t idx = 0; idx < count; idx++) {
            auto macros = repo->GetMacroGroupByIdx(idx);
            auto gid = macros->GetGroupID();
            if(!d->mcuControl->ClearMacro(gid)) return false;

            auto macroCount = macros->GetCount();
            for(uint32_t macroIdx=0; macroIdx<macroCount; macroIdx++) {
                auto macro = macros->GetMacro(macroIdx);
                if(!d->mcuControl->AddMacroPoint(gid, macroIdx, 
                                                 macro->GetMotionCommand(),
                                                 macro->GetPositionX(), 
                                                 macro->GetPositionY(),
                                                 macro->GetPositionZ(),
                                                 macro->GetAFMode())) {
                    return false;
                }
            }
        }
        return true;
    }

    auto MacroPanelControl::GetMacroGroups() -> QList<MCUMacroGroup::Pointer> {
        QList<MCUMacroGroup::Pointer> macros;

        auto repo = MCUMacroRepository::GetInstance();
        const auto count = repo->GetCounts();
        for (uint32_t idx = 0; idx < count; idx++) {
            macros.push_back(repo->GetMacroGroupByIdx(idx));
        }

        return macros;
    }

    auto MacroPanelControl::GetMacroGroup(uint32_t groupID) -> MCUMacroGroup::Pointer {
        auto repo = MCUMacroRepository::GetInstance();
        return repo->GetMacroGroup(groupID);
    }

    auto MacroPanelControl::CreateNewGroup(const QString& title) -> uint32_t {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->CreateMacroGroup(title);
        return group->GetGroupID();
    }

    auto MacroPanelControl::RemoveGroup(uint32_t groupID) -> void {
        auto repo = MCUMacroRepository::GetInstance();
        repo->RemoveMacroGroup(groupID);
    }

    auto MacroPanelControl::CloneGroup(uint32_t groupID, const QString& title) -> void {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->GetMacroGroup(groupID);
        auto newGroup = repo->CloneMacroGroup(group);
        if(!title.isEmpty()) {
            newGroup->SetTitle(title);
        }
    }

    auto MacroPanelControl::AddPoints(uint32_t groupID, QList<Point>& points, uint32_t commandID, int32_t afMode) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->GetMacroGroup(groupID);
        if (group.get() == nullptr) return false;

        for (auto point : points) {
            MCUMacro::Pointer macro{ new MCUMacro };
            macro->SetPosition(point.x, point.y, point.z);
            macro->SetMotionCommand(commandID);
            macro->SetAFMode(afMode);
            group->AddMacro(macro);
        }

        return true;
    }

    auto MacroPanelControl::UpdatePoint(uint32_t groupID, uint32_t pointIdx, const Point& point, uint32_t commandID,
                                        int32_t afMode) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->GetMacroGroup(groupID);

        auto macro = group->GetMacro(pointIdx);
        if(macro == nullptr) return false;

        macro->SetPosition(point.x, point.y, point.z);
        macro->SetMotionCommand(commandID);
        macro->SetAFMode(afMode);

        return true;
    }

    auto MacroPanelControl::RemovePoint(uint32_t groupID, int32_t pointIdx) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->GetMacroGroup(groupID);
        if (group.get() == nullptr) return false;

        return group->RemoveMacro(pointIdx);
    }

    auto MacroPanelControl::ChangePointPos(uint32_t groupID, uint32_t curIdx, uint32_t newIdx) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        auto group = repo->GetMacroGroup(groupID);
        if (group.get() == nullptr) return false;

        group->MoveMacro(curIdx, newIdx);

        return true;
    }
}
