#pragma once
#include <memory>
#include <QList>

#include <MCUMacroRepository.h>
#include <MCUMotionCommandRepository.h>

#include "MCUConsoleDefines.h"

namespace TC::MCUControl {
    class MacroPanelControl {
    public:
        MacroPanelControl();
        virtual ~MacroPanelControl();

        auto GetMotionCommands(const QString& path)->QList<IMCUMotionCommand::Pointer>;

        auto Load(const QString& path)->bool;
        auto Save(const QString& path)->bool;
        auto Upload()->bool;

        auto GetMacroGroups()->QList<MCUMacroGroup::Pointer>;
        auto GetMacroGroup(uint32_t groupID)->MCUMacroGroup::Pointer;

        auto CreateNewGroup(const QString& title)->uint32_t;
        auto RemoveGroup(uint32_t groupID)->void;
        auto CloneGroup(uint32_t groupID, const QString& title)->void;

        auto AddPoints(uint32_t groupID, QList<Point>& points, uint32_t commandID, int32_t afMode)->bool;
        auto UpdatePoint(uint32_t groupID, uint32_t pointIdx, const Point& point, uint32_t commandID, int32_t afMode)->bool;
        auto RemovePoint(uint32_t groupID, int32_t pointIdx)->bool;
        auto ChangePointPos(uint32_t groupID, uint32_t curIdx, uint32_t newIdx)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}