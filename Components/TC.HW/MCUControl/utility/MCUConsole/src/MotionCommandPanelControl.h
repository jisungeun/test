#pragma once
#include <memory>
#include <QList>
#include <QStringList>

#include <MCUMotionCommandRepository.h>

namespace TC::MCUControl {
    class MotionCommandPanelControl {
    public:
        MotionCommandPanelControl();
        virtual ~MotionCommandPanelControl();

        auto Load(const QString& path)->bool;
        auto Save(const QString& path)->bool;
        auto Upload()->bool;

        auto GetCommands()->QList<IMCUMotionCommand::Pointer>;
        auto GetCommand(uint32_t id)->IMCUMotionCommand::Pointer;
        auto UpdateCommands(QList<IMCUMotionCommand::Pointer>& commands)->void;
        auto UpdateCommandTitle(uint32_t id, const QString& title)->void;
        auto InsertCommand(IMCUMotionCommand::Pointer& command)->void;
        auto DeleteCommand(uint32_t id)->void;

        auto GetTypes() const->QList<MotionCommandType>;
        auto GetTypesAsString() const->QStringList;
        auto CreateCommand(MotionCommandType type, const QString& title)->IMCUMotionCommand::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}