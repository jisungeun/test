#pragma once
#include <memory>

#include <QWidget>

namespace TC::MCUControl {
    class LogPanel : public QWidget {
        Q_OBJECT
    public:
        LogPanel(QWidget* parent = nullptr);
        virtual ~LogPanel();

    protected slots:
        void onUpdated();
        void onChangeFollowMode(int state);
        void onSave();
        void onClearMCULog();
        void onReadMCULog();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}