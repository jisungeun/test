#pragma once
#include <memory>

#include <MCUResponse.h>

namespace TC::MCUControl {
    class ResponseObserver;

    class ResponseUpdater {
    public:
        typedef std::shared_ptr<ResponseUpdater> Pointer;

    protected:
        ResponseUpdater();

    public:
        virtual ~ResponseUpdater();

        static auto GetInstance()->Pointer;
        auto Register(ResponseObserver* observer)->void;

        auto UpdateResponse(const MCUResponse& response)->void;
        auto UpdateDI(int32_t channel, Flag flag)->void;
        auto UpdateMovingStatus(Axis axis, Flag flag)->void;
        auto UpdatePosition(Axis axis, int32_t position)->void;
        auto UpdateTemperature(int32_t sensor, float temperature)->void;
        auto UpdateAcceleration(float xAngle, float yAngle, float zAngle)->void;
        auto UpdateCartirdge(Flag inlet, Flag loading)->void;

        auto UpdateAFSensorValue(int32_t value)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}