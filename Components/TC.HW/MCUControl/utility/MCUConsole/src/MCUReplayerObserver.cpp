#include "MCUReplayer.h"
#include "MCUReplayerObserver.h"

namespace TC::MCUControl {
    MCUReplayerObserver::MCUReplayerObserver(QObject* parent) : QObject(parent) {
        MCUReplayer::GetInstance()->Register(this);
    }

    MCUReplayerObserver::~MCUReplayerObserver() {
        MCUReplayer::GetInstance()->Deregister(this);
    }

    auto MCUReplayerObserver::UpdateProgress(int32_t count) -> void {
        emit sigUpdate(count);
    }

    auto MCUReplayerObserver::UpdateStopped(const QString& message) -> void {
        emit sigStopped(message);
    }

    void MCUReplayerObserver::UpdateFinished() {
        emit sigFinished();
    }
}