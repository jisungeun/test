#include <MCUResponse.h>

#include "ResponseUpdater.h"
#include "StatusPanelControl.h"

namespace TC::MCUControl {
    struct StatusPanelControl::Impl {
        MCUResponse resp;
    };

    StatusPanelControl::StatusPanelControl() : d{ new Impl } {
    }

    StatusPanelControl::~StatusPanelControl() {
    }

    auto StatusPanelControl::UpdateResponse(const MCUResponse& response) -> void {
        d->resp = response;
    }

    auto StatusPanelControl::GetState() const -> QString {
        const auto state = d->resp.GetState();
        return QString("S%1 %2").arg(state._to_integral()).arg(state._to_string());
    }

    auto StatusPanelControl::GetMacroStep() const -> uint32_t {
        return d->resp.GetValue(Response::MacroStep);
    }

    auto StatusPanelControl::GetAxisAtStart(Axis axis) const -> bool {
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXStart };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXStart;
                break;
            case Axis::Y:
                resp = Response::AxisYStart;
                break;
            case Axis::Z:
                resp = Response::AxisZStart;
                break;
            case Axis::L:
                resp = Response::AxisLStart;
                break;
            case Axis::U:
                resp = Response::AxisUStart;
                break;
            case Axis::V:
                resp = Response::AxisVStart;
                break;
            case Axis::W:
                resp = Response::AxisWStart;
                break;
            }
            return resp;
        };

        const auto start = d->resp.GetValue(respCode(axis));
        return (start != 0);
    }

    auto StatusPanelControl::GetAxisAtEnd(Axis axis) const -> bool {
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXEnd };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXEnd;
                break;
            case Axis::Y:
                resp = Response::AxisYEnd;
                break;
            case Axis::Z:
                resp = Response::AxisZEnd;
                break;
            case Axis::L:
                resp = Response::AxisLEnd;
                break;
            case Axis::U:
                resp = Response::AxisUEnd;
                break;
            case Axis::V:
                resp = Response::AxisVEnd;
                break;
            case Axis::W:
                resp = Response::AxisWEnd;
                break;
            }
            return resp;
        };

        const auto end = d->resp.GetValue(respCode(axis));
        return (end != 0);
    }

    auto StatusPanelControl::GetAxisMoving(Axis axis) const -> bool {
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXMoving };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXMoving;
                break;
            case Axis::Y:
                resp = Response::AxisYMoving;
                break;
            case Axis::Z:
                resp = Response::AxisZMoving;
                break;
            case Axis::L:
                resp = Response::AxisLMoving;
                break;
            case Axis::U:
                resp = Response::AxisUMoving;
                break;
            case Axis::V:
                resp = Response::AxisVMoving;
                break;
            case Axis::W:
                resp = Response::AxisWMoving;
                break;
            }
            return resp;
        };

        const auto moving = d->resp.GetValue(respCode(axis));
        return (moving != 0);
    }

    auto StatusPanelControl::GetAxisPosition(Axis axis) const -> int32_t {
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXPosition };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXPosition;
                break;
            case Axis::Y:
                resp = Response::AxisYPosition;
                break;
            case Axis::Z:
                resp = Response::AxisZPosition;
                break;
            case Axis::U:
                resp = Response::AxisUPosition;
                break;
            case Axis::V:
                resp = Response::AxisVPosition;
                break;
            case Axis::W:
                resp = Response::AxisWPosition;
                break;
            }
            return resp;
        };

        return d->resp.GetValue(respCode(axis));
    }

    auto StatusPanelControl::GetAxisSWLimit(Axis axis) const -> bool {
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXSWLimit };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXSWLimit;
                break;
            case Axis::Y:
                resp = Response::AxisYSWLimit;
                break;
            case Axis::Z:
                resp = Response::AxisZSWLimit;
                break;
            case Axis::U:
                resp = Response::AxisUSWLimit;
                break;
            case Axis::V:
                resp = Response::AxisVSWLimit;
                break;
            case Axis::W:
                resp = Response::AxisWSWLimit;
                break;
            }
            return resp;
        };

        const auto triggered = d->resp.GetValue(respCode(axis));
        return (triggered != 0);
    }

    auto StatusPanelControl::GetLEDOn(int32_t led) const -> bool {
        auto sensor = [](int32_t led)->Response {
            Response resp{ Response::SensorLED0On };
            switch (led) {
            case 0:
                resp = Response::SensorLED0On;
                break;
            case 1:
                resp = Response::SensorLED1On;
                break;
            case 2:
                resp = Response::SensorLED2On;
                break;
            case 3:
                resp = Response::SensorLED3On;
                break;
            }
            return resp;
        };

        const auto on = d->resp.GetValue(sensor(led));
        return (on != 0);
    }

    auto StatusPanelControl::GetAfOn() const -> bool {
        return 1 == d->resp.GetValue(Response::AfOn);
    }

    auto StatusPanelControl::GetAfError() const -> bool {
        return 1 == d->resp.GetValue(Response::AfError);
    }

    auto StatusPanelControl::GetAngle(int32_t direction) const -> float {
        auto sensor = [](int32_t direction) {
            Response resp{ Response::AngleX };
            switch (direction) {
            case 0:
                resp = Response::AngleX;
                break;
            case 1:
                resp = Response::AngleY;
                break;
            case 2:
                resp = Response::AngleZ;
                break;
            }
            return resp;
        };

        return d->resp.GetValueAsFloat(sensor(direction));
    }

    auto StatusPanelControl::GetTemperature(int32_t index) const -> float {
        auto sensor = [](int32_t index)->Response {
            Response resp{ Response::Temperature0 };
            switch (index) {
            case 0:
                resp = Response::Temperature0;
                break;
            case 1:
                resp = Response::Temperature1;
                break;
            case 2:
                resp = Response::Temperature2;
                break;
            case 3:
                resp = Response::Temperature3;
                break;
            }
            return resp;
        };

        return d->resp.GetValueAsFloat(sensor(index));
    }

    auto StatusPanelControl::GetStreamingIndex() const -> int32_t {
        return d->resp.GetValue(Response::MacroStreamingIndex);
    }

    auto StatusPanelControl::GetStreamingBufferAvailable() const -> int32_t {
        return d->resp.GetValue(Response::MacroStreamingEmptyBufferSize);
    }

    auto StatusPanelControl::GetStreamingBufferHalfEmpty() const -> bool {
        return 1 == d->resp.GetValue(Response::MacroStreamingHalfEmpty);
    }

    auto StatusPanelControl::GetStreamingError() const -> bool {
        return 1 == d->resp.GetValue(Response::MacroStreamingError);
    }
}
