#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class ConfigPanel : public QDialog {
        Q_OBJECT
    public:
        ConfigPanel(QWidget* parent = nullptr);
        virtual ~ConfigPanel();

    protected:
        void done(int r) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}