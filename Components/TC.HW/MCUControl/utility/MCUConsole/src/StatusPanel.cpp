#include "ResponseObserver.h"
#include "StatusPanelControl.h"
#include "StatusPanel.h"
#include "ui_StatusPanel.h"

namespace TC::MCUControl {
    struct StatusPanel::Impl {
        Ui::StatusPanel ui;
        StatusPanelControl control;
        ResponseObserver* observer{ nullptr };

        auto updateGUI()->void;
    };

    auto StatusPanel::Impl::updateGUI() -> void {
        ui.stateEdit->setText(control.GetState());
        ui.macroStep->setValue(control.GetMacroStep());

        ui.xStartChk->setChecked(control.GetAxisAtStart(Axis::X));
        ui.xEndChk->setChecked(control.GetAxisAtEnd(Axis::X));
        ui.xMoveChk->setChecked(control.GetAxisMoving(Axis::X));
        ui.xSWLimitChk->setChecked(control.GetAxisSWLimit(Axis::X));

        ui.yStartChk->setChecked(control.GetAxisAtStart(Axis::Y));
        ui.yEndChk->setChecked(control.GetAxisAtEnd(Axis::Y));
        ui.yMoveChk->setChecked(control.GetAxisMoving(Axis::Y));
        ui.ySWLimitChk->setChecked(control.GetAxisSWLimit(Axis::Y));

        ui.zStartChk->setChecked(control.GetAxisAtStart(Axis::Z));
        ui.zEndChk->setChecked(control.GetAxisAtEnd(Axis::Z));
        ui.zMoveChk->setChecked(control.GetAxisMoving(Axis::Z));
        ui.zSWLimitChk->setChecked(control.GetAxisSWLimit(Axis::Z));

        ui.uStartChk->setChecked(control.GetAxisAtStart(Axis::U));
        ui.uEndChk->setChecked(control.GetAxisAtEnd(Axis::U));
        ui.uMoveChk->setChecked(control.GetAxisMoving(Axis::U));
        ui.uSWLimitChk->setChecked(control.GetAxisSWLimit(Axis::U));

        ui.vStartChk->setChecked(control.GetAxisAtStart(Axis::V));
        ui.vEndChk->setChecked(control.GetAxisAtEnd(Axis::V));
        ui.vMoveChk->setChecked(control.GetAxisMoving(Axis::V));
        ui.vSWLimitChk->setChecked(control.GetAxisSWLimit(Axis::V));

        ui.wStartChk->setChecked(control.GetAxisAtStart(Axis::W));
        ui.wEndChk->setChecked(control.GetAxisAtEnd(Axis::W));
        ui.wMoveChk->setChecked(control.GetAxisMoving(Axis::W));
        ui.wSWLimitChk->setChecked(control.GetAxisSWLimit(Axis::W));

        ui.xPosSpin->setValue(control.GetAxisPosition(Axis::X));
        ui.yPosSpin->setValue(control.GetAxisPosition(Axis::Y));
        ui.zPosSpin->setValue(control.GetAxisPosition(Axis::Z));
        ui.uPosSpin->setValue(control.GetAxisPosition(Axis::U));
        ui.vPosSpin->setValue(control.GetAxisPosition(Axis::V));
        ui.wPosSpin->setValue(control.GetAxisPosition(Axis::W));

        ui.led1Chk->setChecked(control.GetLEDOn(0));
        ui.led2Chk->setChecked(control.GetLEDOn(1));
        ui.led3Chk->setChecked(control.GetLEDOn(2));
        ui.led4Chk->setChecked(control.GetLEDOn(3));

        ui.AfOnChk->setChecked(control.GetAfOn());
        ui.afChkError->setChecked(control.GetAfError());

        auto value = control.GetAngle(0);
        ui.xAngleSpin->setValue(control.GetAngle(0));
        ui.yAngleSpin->setValue(control.GetAngle(1));
        ui.zAngleSpin->setValue(control.GetAngle(2));

        ui.temp1Spin->setValue(control.GetTemperature(0));
        ui.temp2Spin->setValue(control.GetTemperature(1));
        ui.temp3Spin->setValue(control.GetTemperature(2));
        ui.temp4Spin->setValue(control.GetTemperature(3));

        ui.streamingIndexSpin->setValue(control.GetStreamingIndex());
        ui.availableBufferSpin->setValue(control.GetStreamingBufferAvailable());
        ui.halfEmptyChk->setChecked(control.GetStreamingBufferHalfEmpty());
        ui.streamingErrorChk->setChecked(control.GetStreamingError());
    }

    StatusPanel::StatusPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        d->observer = new ResponseObserver(this);

        auto chkBoxes = this->findChildren<QCheckBox*>();
        for (auto chkBox : chkBoxes) {
            chkBox->setAttribute(Qt::WA_TransparentForMouseEvents);
            chkBox->setFocusPolicy(Qt::NoFocus);
        }

        onUpdate();

        connect(d->observer, SIGNAL(sigUpdate()), this, SLOT(onUpdate()));
        connect(d->observer, SIGNAL(sigUpdateAFSensor(int32_t)), this, SLOT(onUpdateAFValue(int32_t)));
    }

    StatusPanel::~StatusPanel() {
    }

    void StatusPanel::onUpdate() {
        d->control.UpdateResponse(d->observer->GetResponse());
        d->updateGUI();
    }

    void StatusPanel::onUpdateAFValue(int32_t value) {
        d->ui.afSensorValue->setValue(value);
    }
}
