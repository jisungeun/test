#include <QKeyEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include "MCUDataDialogControl.h"
#include "MCUDataDialog.h"
#include "ui_MCUDataDialog.h"

namespace TC::MCUControl {
    using Field = MCUDataDialogControl::Field;

    struct MCUDataDialog::Impl {
        Ui::MCUDataDialog ui;
        MCUDataDialogControl control;
        MCUDataDialog* p{ nullptr };

        Impl(MCUDataDialog* p) : p{ p } {}
        auto GetLatest() const->QString {
            return QSettings().value("Latest MCU Data Path").toString();
        }
        auto SetLatest(const QString& path)->void {
            QSettings().setValue("Latest MCU Data Path", path);
        }

        auto InitGUI()->void;
        auto LoadFile()->void;
        auto SaveFile()->void;
        auto Write()->void;
        auto Read()->void;
    };

    auto MCUDataDialog::Impl::InitGUI() -> void {
        ui.dataTable->setColumnCount(3);
        ui.dataTable->setHorizontalHeaderLabels({"Address", "Data", "Description"});
        ui.dataTable->setColumnWidth(0, 20);
        ui.dataTable->horizontalHeader()->setStretchLastSection(true);
        ui.dataTable->verticalHeader()->hide();
        ui.dataTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        ui.dataTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    }

    auto MCUDataDialog::Impl::LoadFile() -> void {
        auto path = QFileDialog::getOpenFileName(p, "Select MCU Data File", GetLatest(), "CSV (*csv)");
        if(path.isEmpty()) {
            QMessageBox::warning(p, "Load MCU Data File", "No file is selected");
            return;
        }

        QList<Field> data;
        if(!control.LoadFile(path, data)) {
            QMessageBox::warning(p, "Load MCU Data File",
                                 QString("It fails to load MCU Data from :\n %1").arg(path));
            return;
        }

        SetLatest(path);

        const auto rowCount = data.length();

        ui.dataTable->setRowCount(rowCount);

        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            auto* item = new QTableWidgetItem(QString::number(rowIdx));
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            ui.dataTable->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(QString::number(data.at(rowIdx).first));
            item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui.dataTable->setItem(rowIdx, 1, item);

            item = new QTableWidgetItem(data.at(rowIdx).second);
            ui.dataTable->setItem(rowIdx, 2, item);
        }
    }

    auto MCUDataDialog::Impl::SaveFile() -> void {
        auto path = QFileDialog::getSaveFileName(p, "Select MCU Data File", GetLatest(), "CSV (*csv)");
        if(path.isEmpty()) {
            QMessageBox::warning(p, "Save MCU Data File", "No file is selected");
            return;
        }

        QList<Field> data;

        const auto rowCount = ui.dataTable->rowCount();
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            data.push_back({ui.dataTable->item(rowIdx, 1)->text().toInt(),
                            ui.dataTable->item(rowIdx, 2)->text()});
        }

        if(!control.SaveFile(path, data)) {
            QMessageBox::warning(p, "Save MCU Data File",
                                 QString("It fails to save MCU Data to :\n %1").arg(path));
            return;
        }

        SetLatest(path);
    }

    auto MCUDataDialog::Impl::Write() -> void {
        QList<int32_t> data;

        const auto rowCount = ui.dataTable->rowCount();
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            data.push_back(ui.dataTable->item(rowIdx, 1)->text().toInt());
        }

        if(!control.Write(data)) {
            QMessageBox::warning(p, "Write MCU Data", "It fails to write MCU Data");
            return;
        }
    }

    auto MCUDataDialog::Impl::Read() -> void {
        QList<int32_t> data;

        if(!control.Read(data)) {
            QMessageBox::warning(p, "Read MCU Data", "It fails to read MCU Data");
            return;
        }

        const auto rowCount = data.length();
        const auto isSame = (rowCount == ui.dataTable->rowCount());

        ui.dataTable->setRowCount(rowCount);
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            auto* item = new QTableWidgetItem(QString::number(rowIdx));
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            ui.dataTable->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(QString::number(data.at(rowIdx)));
            item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui.dataTable->setItem(rowIdx, 1, item);

            if(!isSame) ui.dataTable->setItem(rowIdx, 2, new QTableWidgetItem());
        }
    }

    MCUDataDialog::MCUDataDialog(QWidget* parent) : QDialog(parent), d{ std::make_unique<Impl>(this) } {
        d->ui.setupUi(this);
        d->InitGUI();

        connect(d->ui.loadBtn, &QPushButton::clicked, this, [this]() {
            d->LoadFile();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this](){
            d->SaveFile();
        });

        connect(d->ui.writeBtn, &QPushButton::clicked, this, [this](){
            d->Write();
        });

        connect(d->ui.readBtn, &QPushButton::clicked, this, [this]() {
            d->Read();
        });
    }

    MCUDataDialog::~MCUDataDialog() {
    }

    void MCUDataDialog::keyPressEvent(QKeyEvent* event) {
        switch (event->key()) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
            return;
		}
        QDialog::keyPressEvent(event);
    }
}

