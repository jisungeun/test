#include "AddMacroPointPanelControl.h"
#include "AddMacroPointPanel.h"
#include "ui_AddMacroPointPanel.h"

namespace TC::MCUControl {
    struct AddMacroPointPanel::Impl {
        Ui::AddMacroPointPanel ui;
        AddMacroPointPanelControl control;
    };

    AddMacroPointPanel::AddMacroPointPanel(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        setWindowTitle("Macro Point");

        auto commands = d->control.GetMotionCommands();
        for (auto command : commands) {
            d->ui.singleCommand->addItem(command.title, command.id);
            d->ui.multiCommand->addItem(command.title, command.id);
        }

        d->ui.singleAFMode->addItems(QStringList() << "Disable" << "Static" << "Tracking");
        d->ui.multiAFMode->addItems(QStringList() << "Disable" << "Static" << "Tracking");
        
        d->ui.tabWidget->setCurrentIndex(0);
        d->ui.tabWidget->tabBar()->setAttribute(Qt::WA_TransparentForMouseEvents);
        d->ui.tabWidget->tabBar()->setFocusPolicy(Qt::NoFocus);

        connect(d->ui.optionChk, SIGNAL(stateChanged(int)), this, SLOT(onSelectType(int)));
    }

    AddMacroPointPanel::~AddMacroPointPanel() {
    }

    auto AddMacroPointPanel::GetPoints() -> QList<Point> {
        QList<Point> points;

        const auto single = (0 == d->ui.tabWidget->currentIndex());
        if (single) {
            Point pos{ d->ui.singleXPos->value(), d->ui.singleYPos->value(), d->ui.singleZPos->value() };
            points.push_back(pos);
        } else {
            auto pts = d->control.GetPoints(d->ui.mutilXPos->value(), d->ui.multiYPos->value(),
                                            d->ui.multiCountX->value(), d->ui.multiCountY->value(),
                                            d->ui.multiGapX->value(), d->ui.multiGapY->value(),
                                            d->ui.multiZPos->value());
            for (auto item : pts) {
                Point pos{ item.x, item.y, item.z };
                points.push_back(pos);
            }
        }

        return points;
    }

    auto AddMacroPointPanel::GetCommandID() -> uint32_t {
        const auto single = (0 == d->ui.tabWidget->currentIndex());
        if (single) {
            return d->ui.singleCommand->currentData().toUInt();
        }
        return d->ui.multiCommand->currentData().toUInt();
    }

    auto AddMacroPointPanel::GetAFMode() -> int32_t {
        const auto single = (0 == d->ui.tabWidget->currentIndex());
        if (single) {
            return d->ui.singleAFMode->currentIndex();
        }
        return d->ui.multiAFMode->currentIndex();
    }

    void AddMacroPointPanel::onSelectType(int state) {
        if (state == Qt::CheckState::Unchecked) {
            d->ui.tabWidget->setCurrentIndex(0);
        } else {
            d->ui.tabWidget->setCurrentIndex(1);
        }
    }
}