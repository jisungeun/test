#pragma once
#include <memory>
#include <QThread>

namespace TC::MCUControl {
    class MCUReplayerObserver;

    class MCUReplayer : public QThread {
    public:
        using Pointer = std::shared_ptr<MCUReplayer>;

    protected:
        MCUReplayer();

    public:
        ~MCUReplayer();

        static auto GetInstance()->Pointer;

        auto RunReplay(const QString& path, int32_t count, int32_t delay)->void;
        auto StopReplay()->void;

    protected:
        auto Register(MCUReplayerObserver* observer)->void;
        auto Deregister(MCUReplayerObserver* observer)->void;

        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class MCUReplayerObserver;
    };
}