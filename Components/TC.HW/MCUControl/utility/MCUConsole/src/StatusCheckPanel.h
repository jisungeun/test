#pragma once
#include <memory>

#include <QWidget>

namespace TC::MCUControl {
    class StatusCheckPanel : public QWidget {
        Q_OBJECT

    public:
        StatusCheckPanel(QWidget* parent = nullptr);
        ~StatusCheckPanel() override;

    protected slots:
        void onCheckStatus();
        void onEnableAutoCheck(int state);
        void onUpdateStatus();
        void onReadAFMSensor();

    signals:
        void sigDisableCheck(bool disable);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}