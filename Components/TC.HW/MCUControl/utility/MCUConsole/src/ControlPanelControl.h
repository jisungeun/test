#pragma once
#include <memory>

#include <MCUDefines.h>
#include <MCUAFParam.h>

namespace TC::MCUControl {
    class ControlPanelControl {
    public:
        ControlPanelControl();
        virtual ~ControlPanelControl();

        auto InitializeAuto()->bool;
        auto OpenPort()->bool;
        auto SetSafeZPos(int32_t pos)->bool;
        auto StartInitialization()->bool;
        auto CheckInitialiation(int32_t& progress)->bool;
        auto FinishCalibration()->bool;
        auto CheckDLPCVersion(int32_t& fw_ver, int32_t& sw_ver)->bool;
        auto SetLEDIntensity(int32_t red, int32_t green, int32_t blue)->bool;
        auto SetDMDExposure(int32_t exposure, int32_t interval)->bool;
        auto ReadTriggerCount(int32_t& count)->bool;
        auto ResetTriggerCount()->bool;
        auto StartTest()->bool;
        auto RunMacro(int32_t group)->bool;
        auto RunMacroManual(int32_t group)->bool;
        auto RunMatrixMacro(int32_t countX, int32_t countY, int32_t gapX, int32_t gapY, int32_t group)->bool;
        auto StopMacro()->bool;
        auto CheckMacro()->bool;
        auto MoveX(int32_t pos)->bool;
        auto MoveY(int32_t pos)->bool;
        auto MoveZ(int32_t pos)->bool;
        auto MoveU(int32_t pos)->bool;
        auto MoveV(int32_t pos)->bool;
        auto MoveW(int32_t pos)->bool;
        auto SetFilterPositions(const QList<int32_t>& positions)->bool;
        auto ChangeFilter(int32_t channel)->bool;
        auto SetLEDChannel(int32_t channel)->bool;
        auto SetCameraType(bool internalCamera, bool externalCamera)->bool;
        auto SetRGBLedIntensity(int32_t red, int32_t green, int32_t blue)->bool;
        auto MoveXStep(int32_t pos)->bool;
        auto MoveYStep(int32_t pos)->bool;
        auto MoveZStep(int32_t pos)->bool;
        auto MoveUStep(int32_t pos)->bool;
        auto MoveVStep(int32_t pos)->bool;
        auto MoveWStep(int32_t pos)->bool;
        auto SetXEncScaleFactor(double factor)->void;
        auto SetYEncScaleFactor(double factor)->void;
        auto MoveXStepMM(double pos)->bool;
        auto MoveYStepMM(double pos)->bool;
        auto GenerateTrigger(int32_t width, int32_t interval, int32_t count)->bool;
        auto FinishTest()->bool;
        auto StartManualMode()->bool;
        auto StopManualMode()->bool;
        auto SetDO(int32_t channel, int32_t value)->bool;
        auto GetDI(int32_t channel)->bool;
        auto MoveXY(int32_t xpos, int32_t ypos)->bool;
        auto StopMotion()->bool;
        auto Recover()->bool;
        auto CheckMovingStatus(Axis axis)->bool;
        auto GetAxisPosition(Axis axis)->bool;
        auto ReadTemperature(int32_t sensor)->bool;
        auto ReadAcceleration()->bool;
        auto ReadCartridgeSensor()->bool;
        auto ReadFirmwareVersion()->QString;
        auto SetMacroStreaming(int32_t group)->int32_t;
        auto SetMacroAFMode(int32_t mode)->void;
        auto AddCommandsToStreamingBuffer(int32_t count)->bool;
        auto GetCountSentToStreamingBuffer()->int32_t;
        auto StartMacroStreaming()->bool;
        auto StopMacroStreaming()->bool;
        auto SetSWLimit(const QList<int32_t>& limits)->bool;
        auto SetInPositionBand(const QList<int32_t>& bands)->bool;

        auto SetAFParameter(const TC::MCUControl::MCUAFParam& param)->bool;
        auto SetAFTarget(int32_t mode, int32_t value, int32_t& targetOnAFM)->bool;
        auto InitAF()->bool;
        auto CheckAFInitialization()->AFInitState;
        auto EnableAF(int32_t mode)->bool;
        auto CheckAFStatus()->std::tuple<int32_t, int32_t, int32_t>;
        auto ChangeAFLensParameter(int32_t param)->bool;
        auto SetAFPeakMode(bool peakmode)->bool;
        auto SetManualAF(bool laser)->bool;
        auto ReadAFLog(QList<int32_t>& adcDiff, QList<int32_t>& zComp)->bool;
        auto ScanZForAFM(int32_t startPos, int32_t endPos, int32_t interval, int32_t delay)->bool;
        auto DumpZScanForAFM(QList<int32_t>& values)->bool;

        auto SetReplayLog(const QString& path)->void;
        auto ReplayLog(int32_t runCount, int32_t intervalMSec)->void;
        auto StopReplay()->void;

        auto SendCustomPacket(const QString& cmd, const QString& parameters, QString& resp)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}