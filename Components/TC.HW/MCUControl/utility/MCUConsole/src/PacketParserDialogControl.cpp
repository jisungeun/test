#include "../../src/MCUPacketParser.h"
#include "../../src/MCUResponseParserHTX.h"
#include "PacketParserDialogControl.h"

namespace TC::MCUControl {
    auto Parse(const QString& line)->QString {
        const auto startIdx = line.indexOf("{");
        const auto endIdx = line.indexOf("}", std::max<int32_t>(0, startIdx));
        if((startIdx == -1) && (endIdx == -1)) {
            const auto trimmed = line.trimmed();
            const auto blankIdx = trimmed.indexOf(" ");
            if(blankIdx == -1) return QString("{%1}").arg(trimmed);
            return QString();
        }

        return line.mid(startIdx, endIdx-startIdx+1);
    }

    PacketParserDialogControl::PacketParserDialogControl() {
    }

    PacketParserDialogControl::~PacketParserDialogControl() {
    }

    auto PacketParserDialogControl::ParseCommand(const QString& command) const -> Result::Pointer {
        const auto packet = Parse(command);
        if(packet.isEmpty()) return nullptr;

        auto result = std::make_shared<Result>();

        MCUPacketParser parser;
        Command cmdType{ Command::CheckStatus };
        if(!parser.Parse(packet.toLatin1(), cmdType, result->parameters)) return nullptr;
        result->commdand = cmdType._to_string();

        return result;
    }

    auto PacketParserDialogControl::ParseResponse(const QString& response) const -> Result::Pointer {
        const auto packet = Parse(response);
        if(packet.isEmpty()) return nullptr;

        auto result = std::make_shared<Result>();

        MCUPacketParser parser;
        ResponseCode resp{ ResponseCode::Fail };
        if(!parser.Parse(packet.toLatin1(), resp, result->parameters)) return nullptr;
        result->commdand = resp._to_string();

        return result;
    }

    auto PacketParserDialogControl::ParseStatusResponse(const QString& response) const -> MCUResponse {
        MCUResponse mcuResponse;

        const auto packet = Parse(response);
        if(packet.isEmpty()) return mcuResponse;

        MCUPacketParser parser;
        ResponseCode resp{ ResponseCode::Fail };
        QByteArray params;
        if(!parser.Parse(packet.toLatin1(), resp, params)) return mcuResponse;

        MCUResponseParserHTX::Parse(params, mcuResponse);

        return mcuResponse;
    }
}
