#pragma once
#include <memory>
#include <QMap>
#include <QString>

namespace TC::MCUControl {
    class StatusCheckPanelControl {
    public:
        StatusCheckPanelControl();
        ~StatusCheckPanelControl();

        auto CheckStatus()->bool;
        auto ReadAFMSensor()->bool;
        auto ReadAFParameters()->QMap<QString,int32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}