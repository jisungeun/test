#pragma once
#include <memory>

#include <QList>

namespace TC::MCUControl {
    class FilterPositionDialogControl {
    public:
        FilterPositionDialogControl();
        virtual ~FilterPositionDialogControl();

        auto GetPositions() const->QList<int32_t>;
        auto SavePositions(const QList<int32_t>& positions)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
