#include <QTableWidgetItem>
#include <QMessageBox>

#include <InputDialog.h>

#include "Settings.h"
#include "AddMacroPointPanel.h"
#include "MacroPanelControl.h"
#include "MacroPanel.h"
#include "ui_MacroPanel.h"

namespace TC::MCUControl {
    enum MacroTbl {
        ColID = 0,
        ColTtile = 1
    };

    enum PointTbl {
        ColXPos = 0,
        ColYPos = 1,
        ColZPos = 2,
        ColCmd = 3,
        ColAFMode = 4
    };

    struct MacroPanel::Impl {
        Ui::MacroPanel ui;
        MacroPanelControl control;

        auto UpdateMotionCommands()->void;
        auto Reset()->void;
        auto UpdateGUI()->void;
        auto UpdateMacroPointsGUI(MCUMacroGroup::Pointer& macros)->void;
        auto AddGroup(uint32_t groupID, const QString& title)->void;
        auto GetSelectectGroup()->int32_t;
        auto EnablePointButtons(bool enable)->void;
        auto SelectedGroup()->uint32_t;
        auto SelectedGroupTitle()->QString;
    };

    auto MacroPanel::Impl::UpdateMotionCommands() -> void {
        auto commands = control.GetMotionCommands(Settings::GetMotionCommandsPath());
        const auto count = commands.length();

        ui.commandTable->clearContents();
        ui.commandTable->setRowCount(count);

        for (auto idx = 0; idx < count; idx++) {
            auto& cmd = commands.at(idx);

            auto itemID = new QTableWidgetItem(QString::number(cmd->GetCommandID()));
            itemID->setFlags(itemID->flags() & ~Qt::ItemIsEditable);
            ui.commandTable->setItem(idx, MacroTbl::ColID, itemID);

            auto itemTitle = new QTableWidgetItem(cmd->GetCommandTitle());
            itemTitle->setFlags(itemTitle->flags() & ~Qt::ItemIsEditable);
            ui.commandTable->setItem(idx, MacroTbl::ColTtile, itemTitle);
        };
    }

    auto MacroPanel::Impl::Reset() -> void {
        control.Load(Settings::GetMacrosPath());
        UpdateGUI();

        if(ui.macroTable->rowCount() > 0) {
            ui.macroTable->setCurrentCell(0, 0);
        }
    }

    auto MacroPanel::Impl::UpdateGUI() -> void {
        ui.macroTable->clearContents();

        auto macros = control.GetMacroGroups();
        auto count = macros.length();

        ui.macroTable->blockSignals(true);
        ui.macroTable->setRowCount(count);

        for (auto idx = 0; idx < count; idx++) {
            auto& macro = macros.at(idx);

            auto itemID = new QTableWidgetItem(QString::number(macro->GetGroupID()));
            itemID->setFlags(itemID->flags() & ~Qt::ItemIsEditable);
            ui.macroTable->setItem(idx, MacroTbl::ColID, itemID);

            auto itemTitle = new QTableWidgetItem(macro->GetTitle());
            itemTitle->setFlags(itemTitle->flags() | Qt::ItemIsEditable);
            ui.macroTable->setItem(idx, MacroTbl::ColTtile, itemTitle);
        }

        ui.macroTable->blockSignals(false);
    }

    auto MacroPanel::Impl::UpdateMacroPointsGUI(MCUMacroGroup::Pointer& macros) -> void {
        ui.pointTable->clearContents();

        ui.pointTable->blockSignals(true);

        const auto count = macros->GetCount();
        ui.pointTable->setRowCount(count);

        for (auto idx = 0u; idx < count; idx++) {
            auto& macro = macros->GetMacro(idx);

            auto itemXPos = new QTableWidgetItem(QString::number(macro->GetPositionX()));
            itemXPos->setFlags(itemXPos->flags() | Qt::ItemIsEditable);
            itemXPos->setTextAlignment(Qt::AlignRight);
            ui.pointTable->setItem(idx, PointTbl::ColXPos, itemXPos);

            auto itemYPos = new QTableWidgetItem(QString::number(macro->GetPositionY()));
            itemYPos->setFlags(itemYPos->flags() | Qt::ItemIsEditable);
            itemYPos->setTextAlignment(Qt::AlignRight);
            ui.pointTable->setItem(idx, PointTbl::ColYPos, itemYPos);

            auto itemZPos = new QTableWidgetItem(QString::number(macro->GetPositionZ()));
            itemZPos->setFlags(itemYPos->flags() | Qt::ItemIsEditable);
            itemZPos->setTextAlignment(Qt::AlignRight);
            ui.pointTable->setItem(idx, PointTbl::ColZPos, itemZPos);

            auto itemXCmd = new QTableWidgetItem(QString::number(macro->GetMotionCommand()));
            itemXCmd->setFlags(itemXCmd->flags() | Qt::ItemIsEditable);
            itemXCmd->setTextAlignment(Qt::AlignRight);
            ui.pointTable->setItem(idx, PointTbl::ColCmd, itemXCmd);

            auto itemAFMode = new QTableWidgetItem(QString::number(macro->GetAFMode()));
            itemAFMode->setFlags(itemAFMode->flags() | Qt::ItemIsEditable);
            itemAFMode->setTextAlignment(Qt::AlignRight);
            ui.pointTable->setItem(idx, PointTbl::ColAFMode, itemAFMode);
        }

        ui.pointTable->blockSignals(false);
    }

    auto MacroPanel::Impl::AddGroup(uint32_t groupID, const QString& title) -> void {
        const auto rows = ui.macroTable->rowCount();

        ui.macroTable->blockSignals(true);
        ui.macroTable->setRowCount(rows + 1);

        auto itemID = new QTableWidgetItem(QString::number(groupID));
        itemID->setFlags(itemID->flags() & ~Qt::ItemIsEditable);
        ui.macroTable->setItem(rows, 0, itemID);

        auto itemTitle = new QTableWidgetItem(title);
        itemTitle->setFlags(itemTitle->flags() | Qt::ItemIsEditable);
        ui.macroTable->setItem(rows, 1, itemTitle);

        ui.macroTable->blockSignals(false);
    }

    auto MacroPanel::Impl::GetSelectectGroup() -> int32_t {
        const auto row = ui.macroTable->currentRow();
        if (row < 0) return -1;

        return ui.macroTable->item(row, 0)->text().toInt();
    }

    auto MacroPanel::Impl::EnablePointButtons(bool enable) -> void {
        ui.addPointBtn->setEnabled(true);
        ui.deletePointBtn->setEnabled(true);
    }

    auto MacroPanel::Impl::SelectedGroup() -> uint32_t {
        const auto row = ui.macroTable->currentRow();
        return ui.macroTable->item(row, 0)->text().toInt();
    }

    auto MacroPanel::Impl::SelectedGroupTitle() -> QString {
        const auto row = ui.macroTable->currentRow();
        return ui.macroTable->item(row, 1)->text();
    }

    MacroPanel::MacroPanel(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        setWindowTitle("Macros");
        for (auto button : d->ui.buttonBox->buttons()) {
            button->setFixedWidth(70);
        }

        d->EnablePointButtons(false);

        d->ui.macroTable->setColumnCount(2);
        d->ui.macroTable->setHorizontalHeaderLabels({"ID", "Title"});
        d->ui.macroTable->setColumnWidth(0, 20);
        d->ui.macroTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.macroTable->verticalHeader()->hide();
        d->ui.macroTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.macroTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);

        d->ui.pointTable->setColumnCount(5);
        d->ui.pointTable->setHorizontalHeaderLabels({"X Pos", "Y Pos", "Z Pos", "Command", "AF Mode"});
        d->ui.pointTable->setColumnWidth(0, 100);
        d->ui.pointTable->setColumnWidth(1, 100);
        d->ui.pointTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.pointTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.pointTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.pointTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);

        d->ui.commandTable->setColumnCount(2);
        d->ui.commandTable->setHorizontalHeaderLabels({"ID", "Title"});
        d->ui.commandTable->setColumnWidth(0, 20);
        d->ui.commandTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.commandTable->verticalHeader()->hide();
        d->ui.commandTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.commandTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);

        connect(d->ui.buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(onDialogButtonClicked(QAbstractButton*)));
        connect(d->ui.newGroupBtn, SIGNAL(clicked()), this, SLOT(onNewGroup()));
        connect(d->ui.deleteGroupBtn, SIGNAL(clicked()), this, SLOT(onDeleteGroup()));
        connect(d->ui.copyGroupBtn, SIGNAL(clicked()), this, SLOT(onCopyGroup()));
        connect(d->ui.addPointBtn, SIGNAL(clicked()), this, SLOT(onAddPoint()));
        connect(d->ui.deletePointBtn, SIGNAL(clicked()), this, SLOT(onDeletePoint()));
        connect(d->ui.moveUpPointBtn, SIGNAL(clicked()), this, SLOT(onMoveUpPoint()));
        connect(d->ui.moveDownPointBtn, SIGNAL(clicked()), this, SLOT(onMoveDownPoint()));
        connect(d->ui.macroTable, SIGNAL(itemSelectionChanged()), this, SLOT(onMacroSelected()));
        connect(d->ui.pointTable, SIGNAL(cellChanged(int,int)), this, SLOT(onPointsChanged(int,int)));
        
        d->UpdateMotionCommands();
        d->Reset();
    }

    MacroPanel::~MacroPanel() {
    }

    void MacroPanel::onDialogButtonClicked(QAbstractButton* button) {
        auto type = d->ui.buttonBox->standardButton(button);
        if (type == QDialogButtonBox::StandardButton::Reset) {
            d->Reset();
        }
    }

    void MacroPanel::onNewGroup() {
        auto title = TC::InputDialog::getText(this, "Macro", "Enter a macro group title");
        if (title.isEmpty()) return;

        auto id = d->control.CreateNewGroup(title);
        d->AddGroup(id, title);
    }

    void MacroPanel::onDeleteGroup() {
        auto id = d->GetSelectectGroup();
        if (id < 0) return;

        d->control.RemoveGroup(id);
        d->UpdateGUI();
    }

    void MacroPanel::onCopyGroup() {
        const auto groupID = d->SelectedGroup();
        if (groupID < 0) return;

        const auto groupTitle = d->SelectedGroupTitle();

        auto title = TC::InputDialog::getText(this, "Macro", "Enter a macro group title",
                                           QLineEdit::Normal, 
                                           QString("Clone_%1").arg(groupTitle));
        d->control.CloneGroup(groupID, title);
        d->UpdateGUI();
    }

    void MacroPanel::onAddPoint() {
        const auto row = d->ui.macroTable->currentRow();
        if (row < 0) {
            d->EnablePointButtons(false);
            QMessageBox::warning(this, "Macro", tr("Select a macro group at first"));
            return;
        }

        AddMacroPointPanel panel;
        if (Rejected == panel.exec()) return;

        auto points = panel.GetPoints();
        auto command = panel.GetCommandID();
        auto afMode = panel.GetAFMode();

        const auto groupID = d->ui.macroTable->item(row, 0)->text().toInt();

        if (!d->control.AddPoints(groupID, points, command, afMode)) {
            QMessageBox::warning(this, "Macro", tr("It fails to add points to a macro group"));
            return;
        }

        onMacroSelected();
    }

    void MacroPanel::onDeletePoint() {
        const auto row = d->ui.macroTable->currentRow();
        if (row < 0) {
            d->EnablePointButtons(false);
            QMessageBox::warning(this, "Macro", tr("Select a macro group at first"));
            return;
        }

        const auto pointIdx = d->ui.pointTable->currentRow();
        if (pointIdx < 0) {
            QMessageBox::warning(this, "Macro", tr("Select a point at first"));
            return;
        }

        const auto groupID = d->ui.macroTable->item(row, 0)->text().toInt();
        d->control.RemovePoint(groupID, pointIdx);

        onMacroSelected();
    }

    void MacroPanel::onMoveUpPoint() {
        const auto curPos = d->ui.pointTable->currentRow();
        if(curPos == 0) return;

        const auto newPos = std::max<int32_t>(0, curPos - 1);
        const auto groupID = d->SelectedGroup();
        if(!d->control.ChangePointPos(groupID, curPos, newPos)) return;

        auto macroGoup = d->control.GetMacroGroup(groupID);
        d->UpdateMacroPointsGUI(macroGoup);
        
        d->ui.pointTable->selectRow(newPos);
    }

    void MacroPanel::onMoveDownPoint() {
        const auto rowCount = d->ui.pointTable->rowCount();
        const auto curPos = d->ui.pointTable->currentRow();
        if(curPos == (rowCount-1)) return;

        const auto newPos = std::min<int32_t>(rowCount-1, curPos + 1);
        const auto groupID = d->SelectedGroup();
        if(!d->control.ChangePointPos(groupID, curPos, newPos)) return;

        auto macroGoup = d->control.GetMacroGroup(groupID);
        d->UpdateMacroPointsGUI(macroGoup);

        d->ui.pointTable->selectRow(newPos);
    }

    void MacroPanel::onMacroSelected() {
        const auto groupID = d->SelectedGroup();
        auto macroGoup = d->control.GetMacroGroup(groupID);
        if (macroGoup.get() == nullptr) {
            d->ui.addPointBtn->setDisabled(true);
            d->ui.deletePointBtn->setDisabled(true);
            return;
        }

        d->UpdateMacroPointsGUI(macroGoup);

        d->EnablePointButtons(true);
    }

    void MacroPanel::onPointsChanged(int row, int column) {
        const auto groupID = d->SelectedGroup();
        Point point;
        point.x = d->ui.pointTable->item(row, 0)->text().toInt();
        point.y = d->ui.pointTable->item(row, 1)->text().toInt();
        point.z = d->ui.pointTable->item(row, 2)->text().toInt();
        const auto cmd = d->ui.pointTable->item(row, 3)->text().toInt();
        const auto afMode = d->ui.pointTable->item(row, 4)->text().toInt();

        d->control.UpdatePoint(groupID, row, point, cmd, afMode);
    }

    void MacroPanel::done(int r) {
        auto accepted = (r == QDialog::DialogCode::Accepted);
        if (!accepted) {
            QDialog::done(r);
            return;
        }

        if (!d->control.Save(Settings::GetMacrosPath())) {
            QMessageBox::warning(this, "Macro", tr("It fails to save macros to file"));
        }

        if (!d->control.Upload()) {
            QMessageBox::warning(this, "Macro", tr("It fails to upload macros to MCU"));
        }

        QDialog::done(r);
    }
}
