#pragma once
#include <memory>

#include <QWidget>

namespace TC::MCUControl {
    class StatusPanel : public QWidget {
        Q_OBJECT
    public:
        StatusPanel(QWidget* parent = nullptr);
        virtual ~StatusPanel();

    protected slots:
        void onUpdate();
        void onUpdateAFValue(int32_t value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}