#include <QsLog.h>

#include <MCUFactory.h>
#include <MCUMotionCommandFactory.h>

#include "Settings.h"
#include "MotionCommandPanelControl.h"

namespace TC::MCUControl {
    struct MotionCommandPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    MotionCommandPanelControl::MotionCommandPanelControl() : d{ new Impl } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    MotionCommandPanelControl::~MotionCommandPanelControl() {
    }

    auto MotionCommandPanelControl::Load(const QString& path) -> bool {
        auto repo = MCUMotionCommandRepository::GetInstance();
        if(!repo->Load(path)) {
            const auto& error = repo->GetLastError();
            QLOG_ERROR() << "[" << error.GetText() << "] " << error.GetDescription();
            return false;
        }
        return true;
    }

    auto MotionCommandPanelControl::Save(const QString& path) -> bool {
        auto repo = MCUMotionCommandRepository::GetInstance();
        if(!repo->Save(path)) {
            const auto& error = repo->GetLastError();
            QLOG_ERROR() << "[" << error.GetText() << "] " << error.GetDescription();
            return false;
        }
        return true;
    }

    auto MotionCommandPanelControl::Upload() -> bool {
        auto repo = MCUMotionCommandRepository::GetInstance();
        const auto counts = repo->GetCounts();
        for (uint32_t idx = 0; idx < counts; idx++) {
            auto cmd = repo->CloneCommandByIndex(idx);
            if(!d->mcuControl->SetCommand(cmd->GetCommandID(), cmd->GetCommandType(), cmd->GetParameters())) {
                return false;
            }
        }
        return true;
    }

    auto MotionCommandPanelControl::GetCommands() -> QList<IMCUMotionCommand::Pointer> {
        QList<IMCUMotionCommand::Pointer> commands;

        auto repo = MCUMotionCommandRepository::GetInstance();
        const auto counts = repo->GetCounts();
        for (uint32_t idx = 0; idx < counts; idx++) {
            commands.push_back(repo->CloneCommandByIndex(idx));
        }

        return commands;
    }

    auto MotionCommandPanelControl::GetCommand(uint32_t id) -> IMCUMotionCommand::Pointer {
        auto repo = MCUMotionCommandRepository::GetInstance();
        return repo->GetCommand(id);
    }

    auto MotionCommandPanelControl::UpdateCommands(QList<IMCUMotionCommand::Pointer>& commands) -> void {
        auto repo = MCUMotionCommandRepository::GetInstance();
        repo->Clear();

        for (auto command : commands) {
            repo->InsertCommand(command);
        }
    }

    auto MotionCommandPanelControl::UpdateCommandTitle(uint32_t id, const QString& title) -> void {
        auto repo = MCUMotionCommandRepository::GetInstance();
        auto cmd = GetCommand(id);
        if (cmd.get() == nullptr) return;

        cmd->SetCommandTitle(title);
    }

    auto MotionCommandPanelControl::InsertCommand(IMCUMotionCommand::Pointer& command) -> void {
        auto repo = MCUMotionCommandRepository::GetInstance();
        repo->InsertCommand(command);
    }

    auto MotionCommandPanelControl::DeleteCommand(uint32_t id) -> void {
        auto repo = MCUMotionCommandRepository::GetInstance();
        repo->DeleteCommand(id);
    }

    auto MotionCommandPanelControl::GetTypes() const -> QList<MotionCommandType> {
        auto factory = MCUMotionCommandFactory::GetInstance();
        return factory->GetCommandTypes();
    }

    auto MotionCommandPanelControl::GetTypesAsString() const -> QStringList {
        auto factory = MCUMotionCommandFactory::GetInstance();
        return factory->GetCommandTypesAsString();
    }

    auto MotionCommandPanelControl::CreateCommand(MotionCommandType type, const QString& title) -> IMCUMotionCommand::Pointer {
        auto repo = MCUMotionCommandRepository::GetInstance();
        auto factory = MCUMotionCommandFactory::GetInstance();
        auto command = factory->CreateCommand(type);
        command->SetCommandID(repo->GetNextCommandID());
        command->SetCommandTitle(title);
        return command;
    }
}
