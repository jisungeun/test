#include <QFileDialog>

#include <MCUCommunicationObserver.h>

#include "LogPanelControl.h"
#include "LogPanel.h"
#include "ui_LogPanel.h"

namespace TC::MCUControl {
    struct LogPanel::Impl {
        Ui::LogPanel ui;
        LogPanelControl control;
        MCUCommunicationObserver* observer{ nullptr };
    };

    LogPanel::LogPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->ui.logText->SetFollowTail(false);
        d->ui.logText->SetColor(Qt::white);

        d->ui.mcuLogText->SetFollowTail(false);
        d->ui.mcuLogText->SetColor(Qt::white);

        d->observer = new MCUCommunicationObserver(this);

        connect(d->ui.folllowChk, SIGNAL(stateChanged(int)), this, SLOT(onChangeFollowMode(int)));
        connect(d->observer, SIGNAL(sigUpdated()), this, SLOT(onUpdated()));
        connect(d->ui.saveBtn, SIGNAL(clicked()), this, SLOT(onSave()));
        connect(d->ui.clearMCULogBtn, SIGNAL(clicked()), this, SLOT(onClearMCULog()));
        connect(d->ui.readMCULogBtn, SIGNAL(clicked()), this, SLOT(onReadMCULog()));
    }

    LogPanel::~LogPanel() {
    }

    void LogPanel::onUpdated() {
        auto item = d->observer->GetNext();
        while (item.get() != nullptr) {
            QString line;

            line.append(item->time.toString("[yyyy-MM-dd HH:mm:ss.zzz] "));
            if (item->sendPacket) {
                line.append("> ");
            } else {
                line.append("< ");
            }
            line.append(QString::fromStdString(item->packet.toStdString()));

            d->ui.logText->AppendLine(line);
            item = d->observer->GetNext();
        }
    }

    void LogPanel::onChangeFollowMode(int state) {
        d->ui.logText->SetFollowTail(state == Qt::CheckState::Checked);
    }

    void LogPanel::onSave() {
        auto str = QFileDialog::getSaveFileName(this, "Communication Log", "", "Text (*.txt)");
        if (str.isEmpty()) return;

        d->control.Save(d->ui.logText->Text(), str);
    }

    void LogPanel::onClearMCULog() {
        d->ui.mcuLogText->clear();
    }

    void LogPanel::onReadMCULog() {
        const auto logs = d->control.ReadMCULog();
        for(auto log : logs) {
            d->ui.mcuLogText->AppendLine(log);
        }
    }
}
