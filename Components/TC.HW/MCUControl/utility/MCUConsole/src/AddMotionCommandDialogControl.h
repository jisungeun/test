#pragma once
#include <memory>
#include <QString>
#include <QList>

#include <MCUDefines.h>

namespace TC::MCUControl {
    class AddMotionCommandDialogControl {
    public:
        struct Type {
            MotionCommandType type;
            QString title;
        };

    public:
        AddMotionCommandDialogControl();
        virtual ~AddMotionCommandDialogControl();

        auto GetTypes() const->QList<Type>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
