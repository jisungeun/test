#include <QMessageBox>
#include <QTimer>

#include "ParameterDialog.h"
#include "StatusCheckPanelControl.h"
#include "StatusCheckPanel.h"
#include "ui_StatusCheckPanel.h"

namespace TC::MCUControl {
    struct StatusCheckPanel::Impl {
        Ui::StatusCheckPanel ui;
        StatusCheckPanelControl control;
        QTimer statusTimer;
    };

    StatusCheckPanel::StatusCheckPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        connect(d->ui.checkStatusBtn, SIGNAL(clicked()), this, SLOT(onCheckStatus()));
        connect(d->ui.enableAutoCheck, SIGNAL(stateChanged(int)), this, SLOT(onEnableAutoCheck(int)));
        connect(&d->statusTimer, SIGNAL(timeout()), this, SLOT(onUpdateStatus()));

        connect(d->ui.readAFMSensorBtn, SIGNAL(clicked()), this, SLOT(onReadAFMSensor()));
        connect(d->ui.readAFSensorParameterBtn, &QPushButton::clicked, this, [this]() {
            auto params = d->control.ReadAFParameters();

            ParameterDialog dlg;
            dlg.setWindowTitle("AF Sensor Parameters");
            dlg.SetParameter(params);
            dlg.exec();
        });
    }

    StatusCheckPanel::~StatusCheckPanel() {
    }

    void StatusCheckPanel::onCheckStatus() {
        if (!d->control.CheckStatus()) {
            QMessageBox::warning(this, "Check Status", tr("Failed to check MCU status"));
            return;
        }
    }

    void StatusCheckPanel::onEnableAutoCheck(int state) {
        const bool enable = (state == Qt::CheckState::Checked);
        emit sigDisableCheck(enable);

        const auto interval = d->ui.statusCheckInterval->value();
        if (enable) d->statusTimer.start(interval * 1000);
        else d->statusTimer.stop();
    }

    void StatusCheckPanel::onUpdateStatus() {
        d->control.CheckStatus();
    }

    void StatusCheckPanel::onReadAFMSensor() {
        d->control.ReadAFMSensor();
    }
}
