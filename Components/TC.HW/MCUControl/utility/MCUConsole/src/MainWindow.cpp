#include <QMessageBox>

#include "ConfigPanel.h"
#include "MotionCommandPanel.h"
#include "MacroPanel.h"
#include "PacketParserDialog.h"
#include "MCUDataDialog.h"
#include "MainWindowControl.h"
#include "Version.h"
#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TC::MCUControl {
    struct MainWindow::Impl { 
        Ui::MainWindow ui;
        MainWindowControl control;
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui.setupUi(this);

        setWindowTitle(QString("HTX MCU Console %1").arg(PROJECT_REVISION));

        if (!d->control.LoadConfiguration()) {
            onConfigure();
        }

        connect(d->ui.actionQuit, SIGNAL(triggered()), this, SLOT(onQuit()));
        connect(d->ui.actionConfigure, SIGNAL(triggered()), this, SLOT(onConfigure()));
        connect(d->ui.actionMotion_Commands, SIGNAL(triggered()), this, SLOT(onEditMotionCommands()));
        connect(d->ui.actionMacros, SIGNAL(triggered()), this, SLOT(onEditMacros()));
        connect(d->ui.actionParser, SIGNAL(triggered()), this, SLOT(onShowParser()));
        connect(d->ui.actionMCUData, &QAction::triggered, this, [this]() {
            MCUDataDialog dialog(this);
            dialog.exec();
        });
        connect(d->ui.controlPanel, SIGNAL(sigUpdateFirmareVersion(const QString&)), this, 
                SLOT(onUpdateFirmwareVersion(const QString&)));
        connect(d->ui.statusCheckPanel, SIGNAL(sigDisableCheck(bool)), d->ui.controlPanel,
                SLOT(onDisableCheck(bool)));

        if (!d->control.LoadConfiguration()) {
            QMessageBox::warning(this, "MCUConsole", tr("It is not allowed to use this software without valid configuration"));
            d->ui.centralwidget->setDisabled(true);
        }
    }

    MainWindow::~MainWindow() {
    }

    void MainWindow::onQuit() {
        qApp->quit();
    }

    void MainWindow::onConfigure() {
        ConfigPanel panel;
        panel.exec();
    }

    void MainWindow::onEditMotionCommands() {
        MotionCommandPanel panel;
        panel.exec();
    }

    void MainWindow::onEditMacros() {
        MacroPanel panel;
        panel.exec();
    }

    void MainWindow::onShowParser() {
        PacketParserDialog dialog;
        dialog.exec();
    }

    void MainWindow::onUpdateFirmwareVersion(const QString& ver) {
        setWindowTitle(QString("HTX MCU Console %1 (MCU: %2)").arg(PROJECT_REVISION)
                                                              .arg(ver));
    }
}
