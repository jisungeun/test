#pragma once
#include <memory>

#include <QMap>
#include <QDialog>

namespace TC::MCUControl {
    class ParameterDialog : public QDialog {
        Q_OBJECT
    public:
        ParameterDialog(QWidget* parent = nullptr);
        ~ParameterDialog();

        auto SetParameter(const QMap<QString, int32_t>& params)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}