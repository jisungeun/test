#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QSerialPortInfo>

#include <MCUFactory.h>

#include "Settings.h"
#include "ConfigPanelControl.h"

namespace TC::MCUControl {
    struct ConfigPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    ConfigPanelControl::ConfigPanelControl() : d{ new Impl } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    ConfigPanelControl::~ConfigPanelControl() {
    }

    auto ConfigPanelControl::LoadConfig() -> bool {
        const auto configPath = Settings::GetConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) return false;

        return d->mcuControl->LoadConfig(configPath);
    }

    auto ConfigPanelControl::GetConfig() -> MCUConfig& {
        return d->mcuControl->GetConfig();
    }

    auto ConfigPanelControl::SaveConfig(const MCUConfig& config) -> bool {
        const auto configPath = Settings::GetConfigPath();
        if (configPath.isEmpty()) return false;

        QFileInfo info(configPath);
        if (!QFile::exists(info.absolutePath())) {
            QDir().mkpath(info.absolutePath());
        }

        d->mcuControl->SetConfig(config);
        return d->mcuControl->SaveConfig(configPath);
    }

    auto ConfigPanelControl::FindPorts() -> QList<int32_t> {
        QList<int32_t> ports;

        auto portInfos = QSerialPortInfo::availablePorts();
        for (auto portInfo : portInfos) {
            auto name = portInfo.portName();
            auto number = name.remove("COM").toUInt();
            ports.push_back(number);
        }

        return ports;
    }

    auto ConfigPanelControl::FindBaudRates() -> QList<int32_t> {
        return QSerialPortInfo::standardBaudRates();
    }
}
