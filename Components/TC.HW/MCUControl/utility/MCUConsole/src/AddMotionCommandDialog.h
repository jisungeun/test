#pragma once
#include <memory>
#include <QDialog>

#include <MCUDefines.h>

namespace TC::MCUControl {
    class AddMotionCommandDialog : public QDialog {
        Q_OBJECT
    public:
        AddMotionCommandDialog(QWidget* parent = nullptr);
        virtual ~AddMotionCommandDialog();

        auto GetType() const->MotionCommandType;
        auto GetTitle() const->QString;

    protected:
        void done(int r) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}