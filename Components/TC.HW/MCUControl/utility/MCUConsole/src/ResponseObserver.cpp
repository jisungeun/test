#include "ResponseUpdater.h"
#include "ResponseObserver.h"

namespace TC::MCUControl {
    struct ResponseObserver::Impl {
        MCUResponse response;
    };

    ResponseObserver::ResponseObserver(QObject* parent) : QObject(parent), d{ new Impl } {
        ResponseUpdater::GetInstance()->Register(this);
    }

    ResponseObserver::~ResponseObserver() {
    }

    auto ResponseObserver::Update(const MCUResponse& response) -> void {
        d->response = response;
        emit sigUpdate();
    }

    auto ResponseObserver::GetResponse() const -> MCUResponse {
        return d->response;
    }

    auto ResponseObserver::UpdateAFSensor(int32_t value) -> void {
        emit sigUpdateAFSensor(value);
    }
}
