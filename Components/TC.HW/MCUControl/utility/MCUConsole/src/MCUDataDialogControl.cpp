#include <QFile>
#include <QTextStream>

#include <MCUFactory.h>

#include "Settings.h"
#include "MCUDataDialogControl.h"

namespace TC::MCUControl {
    struct MCUDataDialogControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    MCUDataDialogControl::MCUDataDialogControl() : d{ std::make_unique<Impl>() } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    MCUDataDialogControl::~MCUDataDialogControl() {
    }

    auto MCUDataDialogControl::LoadFile(const QString& path, QList<Field>& data) -> bool {
        QFile file(path);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) return false;

        QTextStream in(&file);

        //readout header
        in.readLine();

        while(!in.atEnd()) {
            auto line = in.readLine();
            auto toks = line.split(",");
            if(toks.length() < 3) {
                data.clear();
                break;
            }

            //address
            toks.pop_front();

            //value
            auto value = toks.first().toInt();
            toks.pop_front();

            //description
            auto description = toks.join(',');

            data.push_back({value, description});
        }

        return !data.isEmpty();
    }

    auto MCUDataDialogControl::SaveFile(const QString& path, const QList<Field>& data) -> bool {
        QFile file(path);
        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) return false;

        QTextStream out(&file);
        out << "address,value,description\n";

        auto rowCount = data.size();
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            out << rowIdx << "," << data.at(rowIdx).first << "," << data.at(rowIdx).second << "\n";
        }

        return true;
    }

    auto MCUDataDialogControl::Read(QList<int32_t>& data) -> bool {
        return d->mcuControl->ReadFlash(data);
    }

    auto MCUDataDialogControl::Write(const QList<int32_t>& data) -> bool {
        return d->mcuControl->WriteFlash(data);
    }
}