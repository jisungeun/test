#include <QMessageBox>
#include <QTimer>
#include <QTableWidget>
#include <QFileDialog>
#include <QClipboard>
#include <QTextStream>

#include <MCUAFParam.h>

#include "FilterPositionDialog.h"
#include "ControlPanelControl.h"
#include "ControlPanel.h"
#include "MCUReplayerObserver.h"
#include "ui_ControlPanel.h"

namespace TC::MCUControl {
    struct ControlPanel::Impl {
        Ui::ControlPanel ui;
        ControlPanelControl control;
        QTimer initTimer;
        MCUReplayerObserver* replayObserver{ nullptr };

        auto InitAFLogTable()->void;
        auto InitAFSensorTable()->void;
        auto CopyTable(QTableWidget* table)->void;
    };

    auto ControlPanel::Impl::InitAFLogTable() -> void {
        ui.afLogTable->setColumnCount(2);
        ui.afLogTable->setHorizontalHeaderLabels({"ADC Diff", "Z Comp"});
        ui.afLogTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    }

    auto ControlPanel::Impl::InitAFSensorTable() -> void {
        ui.afSensorValueTable->setColumnCount(1);
        ui.afSensorValueTable->setHorizontalHeaderLabels({"Value"});
        ui.afSensorValueTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    }

    auto ControlPanel::Impl::CopyTable(QTableWidget* table) -> void {
        const auto columns = table->columnCount();
        const auto rows = table->rowCount();

        QStringList header;
        for (auto col = 0; col < columns; ++col) {
            header << table->horizontalHeaderItem(col)->text();
        }

        QStringList contents;

        QString line;
        for (auto idx = 0; idx < columns; idx++) {
            line.push_back(table->horizontalHeaderItem(idx)->text());
            if (idx < (columns - 1)) {
                line.push_back("\t");
            }
        }
        contents.push_back(line);

        for (auto row = 0; row < rows; row++) {
            line.clear();
            for (auto idx = 0; idx < columns; idx++) {
                const auto item = table->item(row, idx);
                if (nullptr == item) {
                    continue;
                }

                line.push_back(item->text());
                if (idx < (columns - 1)) line.push_back("\t");
            }
            contents.push_back(line);
        }

        QString text;
        QTextStream out(&text);
        for (const auto content : contents) {
            out << content << "\n";
        }

        QClipboard* clipboard = QGuiApplication::clipboard();
        clipboard->setText(text);
    }

    ControlPanel::ControlPanel(QWidget* parent) : d{ new Impl } {
        d->ui.setupUi(this);
        d->ui.mainTabWidget->setCurrentIndex(0);
        d->ui.initProgress->setRange(0, 100);

        d->ui.afMode->addItems(QStringList() << "Disable" << "Static" << "Tracking");

        d->InitAFLogTable();
        d->InitAFSensorTable();
        d->ui.afTuningLogTab->setCurrentIndex(0);

        d->replayObserver = new MCUReplayerObserver(this);
        d->ui.replayMCULogBtn->setDisabled(true);
        d->ui.stopMCUReplayBtn->setDisabled(true);
        
        connect(d->ui.initBtn, SIGNAL(clicked()), this, SLOT(onInitializeAuto()));
        connect(d->ui.openBtn, SIGNAL(clicked()), this, SLOT(onOpenPort()));
        connect(d->ui.setSafeZPosBtn, SIGNAL(clicked()), this, SLOT(onSetSafeZPos()));
        connect(d->ui.setStartInitBtn, SIGNAL(clicked()), this, SLOT(onStartInitialization()));
        connect(d->ui.finishCalBtn, SIGNAL(clicked()), this, SLOT(onFinishCalibration()));
        connect(d->ui.startTestBtn, SIGNAL(clicked()), this, SLOT(onStartTest()));
        connect(d->ui.runMacroBtn, SIGNAL(clicked()), this, SLOT(onRunMacro()));
        connect(d->ui.runMacro1Btn, SIGNAL(clicked()), this, SLOT(onRunMacroManual()));
        connect(d->ui.runMatrixMacro, SIGNAL(clicked()), this, SLOT(onRunMatrixMacro()));
        connect(d->ui.stopMacroBtn, SIGNAL(clicked()), this, SLOT(onStopMacro()));
        connect(d->ui.checkMacroBtn, SIGNAL(clicked()), this, SLOT(onCheckMacro()));
        connect(d->ui.moveXBtn, SIGNAL(clicked()), this, SLOT(onMoveX()));
        connect(d->ui.moveYBtn, SIGNAL(clicked()), this, SLOT(onMoveY()));
        connect(d->ui.moveZBtn, SIGNAL(clicked()), this, SLOT(onMoveZ()));
        connect(d->ui.moveUBtn, SIGNAL(clicked()), this, SLOT(onMoveU()));
        connect(d->ui.moveVBtn, SIGNAL(clicked()), this, SLOT(onMoveV()));
        connect(d->ui.moveWBtn, SIGNAL(clicked()), this, SLOT(onMoveW()));
        connect(d->ui.setFilterPosBtn, SIGNAL(clicked()), this, SLOT(onSetFilterPositions()));
        connect(d->ui.changeFilterBtn, SIGNAL(clicked()), this, SLOT(onChangeFilter()));
        connect(d->ui.ledChannelBtn, SIGNAL(clicked()), this, SLOT(onSetLEDChannel()));
        connect(d->ui.cameraTypeBtn, SIGNAL(clicked()), this, SLOT(onSetCameraType()));
        connect(d->ui.generateTriggerBtn, SIGNAL(clicked()), this, SLOT(onGenerateTrigger()));
        connect(d->ui.finishTestBtn, SIGNAL(clicked()), this, SLOT(onFinishTest()));
        connect(d->ui.startManualMode, SIGNAL(clicked()), this, SLOT(onStartManualMode()));
        connect(d->ui.stopManualMode, SIGNAL(clicked()), this, SLOT(onStopManualMode()));
        connect(d->ui.moveX1Btn, SIGNAL(clicked()), this, SLOT(onMoveX1()));
        connect(d->ui.moveY1Btn, SIGNAL(clicked()), this, SLOT(onMoveY1()));
        connect(d->ui.moveZ1Btn, SIGNAL(clicked()), this, SLOT(onMoveZ1()));
        connect(d->ui.moveU1Btn, SIGNAL(clicked()), this, SLOT(onMoveU1()));
        connect(d->ui.moveV1Btn, SIGNAL(clicked()), this, SLOT(onMoveV1()));
        connect(d->ui.moveW1Btn, SIGNAL(clicked()), this, SLOT(onMoveW1()));
        connect(d->ui.moveX1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveX1StepMinus()));
        connect(d->ui.moveX1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveX1StepPlus()));
        connect(d->ui.moveY1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveY1StepMinus()));
        connect(d->ui.moveY1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveY1StepPlus()));
        connect(d->ui.moveZ1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveZ1StepMinus()));
        connect(d->ui.moveZ1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveZ1StepPlus()));
        connect(d->ui.moveU1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveU1StepMinus()));
        connect(d->ui.moveU1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveU1StepPlus()));
        connect(d->ui.moveV1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveV1StepMinus()));
        connect(d->ui.moveV1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveV1StepPlus()));
        connect(d->ui.moveW1StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveW1StepMinus()));
        connect(d->ui.moveW1StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveW1StepPlus()));
        connect(d->ui.moveX2Btn, SIGNAL(clicked()), this, SLOT(onMoveX2()));
        connect(d->ui.moveY2Btn, SIGNAL(clicked()), this, SLOT(onMoveY2()));
        connect(d->ui.moveZ2Btn, SIGNAL(clicked()), this, SLOT(onMoveZ2()));
        connect(d->ui.moveX2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveX2StepMinus()));
        connect(d->ui.moveX2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveX2StepPlus()));
        connect(d->ui.moveY2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveY2StepMinus()));
        connect(d->ui.moveY2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveY2StepPlus()));
        connect(d->ui.moveZ2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveZ2StepMinus()));
        connect(d->ui.moveZ2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveZ2StepPlus()));
        connect(d->ui.moveU2Btn, SIGNAL(clicked()), this, SLOT(onMoveU2()));
        connect(d->ui.moveV2Btn, SIGNAL(clicked()), this, SLOT(onMoveV2()));
        connect(d->ui.moveW2Btn, SIGNAL(clicked()), this, SLOT(onMoveW2()));
        connect(d->ui.moveU2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveU2StepMinus()));
        connect(d->ui.moveU2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveU2StepPlus()));
        connect(d->ui.moveV2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveV2StepMinus()));
        connect(d->ui.moveV2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveV2StepPlus()));
        connect(d->ui.moveW2StepMinusBtn, SIGNAL(clicked()), this, SLOT(onMoveW2StepMinus()));
        connect(d->ui.moveW2StepPlusBtn, SIGNAL(clicked()), this, SLOT(onMoveW2StepPlus()));
        connect(d->ui.generateTrigger1Btn, SIGNAL(clicked()), this, SLOT(onGenerateTrigger1()));
        connect(d->ui.setDOBtn, SIGNAL(clicked()), this, SLOT(onSetDO()));
        connect(d->ui.getDIBtn, SIGNAL(clicked()), this, SLOT(onGetDI()));
        connect(d->ui.ledChannel2Btn, SIGNAL(clicked()), this, SLOT(onSetLEDChannel2()));
        connect(d->ui.cameraType2Btn, SIGNAL(clicked()), this, SLOT(onSetCameraType2()));
        connect(d->ui.rgbLedItensityBtn, SIGNAL(clicked()), this, SLOT(onSetRGBLedIntensity()));
        connect(d->ui.moveXYBtn, SIGNAL(clicked()), this, SLOT(onMoveXY()));
        connect(d->ui.stopMotionBtn, SIGNAL(clicked()), this, SLOT(onStopMotion()));
        connect(d->ui.checkDLPCVersionBtn, SIGNAL(clicked()), this, SLOT(onCheckDLPCVersion()));
        connect(d->ui.setLEDIntensityBtn, SIGNAL(clicked()), this, SLOT(onSetLEDIntensity()));
        connect(d->ui.setDMDExposureBtn, SIGNAL(clicked()), this, SLOT(onSetDMDExposure()));
        connect(d->ui.readTriggerCountBtn, SIGNAL(clicked()), this, SLOT(onReadTriggerCount()));
        connect(d->ui.resetTriggerCountBtn, SIGNAL(clicked()), this, SLOT(onResetTriggerCount()));
        connect(d->ui.recoverBtn, SIGNAL(clicked()), this, SLOT(onRecover()));
        connect(d->ui.chkMovingStatusBtn, SIGNAL(clicked()), this, SLOT(onCheckMovingStatus()));
        connect(d->ui.getAxisPositionBtn, SIGNAL(clicked()), this, SLOT(onGetAxisPosition()));
        connect(d->ui.readTemperatureBtn, SIGNAL(clicked()), this, SLOT(onReadTemperature()));
        connect(d->ui.readAccelerationBtn, SIGNAL(clicked()), this, SLOT(onReadAcceleration()));
        connect(d->ui.readCartridgeSensorBtn, SIGNAL(clicked()), this, SLOT(onReadCartridgeSensor()));
        connect(d->ui.setMacroStreamingBtn, SIGNAL(clicked()), this, SLOT(onSetMacroStreaming()));
        connect(d->ui.macroAfModeSpinbox, SIGNAL(valueChanged(int)), this, SLOT(onSetMacroAFMode(int)));
        connect(d->ui.addMacroToBufferBtn, SIGNAL(clicked()), this, SLOT(onAddCommandsToStreamBuffer()));
        connect(d->ui.runMacroStreamingBtn, SIGNAL(clicked()), this, SLOT(onRunMacroStreaming()));
        connect(d->ui.stopMacroStreamingBtn, SIGNAL(clicked()), this, SLOT(onStopMacroStreaming()));

        //software limit
        connect(d->ui.swLimitXChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitXValue->setEnabled(checked);
        });
        connect(d->ui.swLimitYChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitYValue->setEnabled(checked);
        });
        connect(d->ui.swLimitZChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitZValue->setEnabled(checked);
        });
        connect(d->ui.swLimitUChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitUValue->setEnabled(checked);
        });
        connect(d->ui.swLimitVChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitVValue->setEnabled(checked);
        });
        connect(d->ui.swLimitWChk, &QCheckBox::stateChanged, this, [this](int state) {
            const auto& checked = (state == Qt::CheckState::Checked);
            d->ui.swLimitWValue->setEnabled(checked);
        });
        connect(d->ui.setSWLimitBtn, SIGNAL(clicked()), this, SLOT(onSetSWLimit()));
        connect(d->ui.setInPositionBandBtn, SIGNAL(clicked()), this, SLOT(onSetInPositionBand()));

        //auto focus
        connect(d->ui.afParamSet, SIGNAL(clicked()), this, SLOT(onSetAFParameter()));
        connect(d->ui.afTargetSet, SIGNAL(clicked()), this, SLOT(onSetAFTarget()));
        connect(d->ui.afInitSet, SIGNAL(clicked()), this, SLOT(onInitAF()));
        connect(d->ui.afInitStatCheck, SIGNAL(clicked()), this, SLOT(onCheckAFInit()));
        connect(d->ui.afEnableSet, SIGNAL(clicked()), this, SLOT(onEnableAF()));
        connect(d->ui.afStatCheck, SIGNAL(clicked()), this, SLOT(onCheckAFStatus()));
        connect(d->ui.afChangeLensParam, SIGNAL(clicked()), this, SLOT(onChangeAFLensParameter()));
        connect(d->ui.afPeakModeSet, SIGNAL(clicked()), this, SLOT(onSetAFPeakMode()));
        connect(d->ui.setManualAFBtn, SIGNAL(clicked()), this, SLOT(onSetManualAF()));
        connect(d->ui.readAfLogBtn, SIGNAL(clicked()), this, SLOT(onReadAFLog()));
        connect(d->ui.afZScanBtn, SIGNAL(clicked()), this, SLOT(onScanZForAFM()));
        connect(d->ui.dumpAfZScanBtn, SIGNAL(clicked()), this, SLOT(onDumpZScanForAFM()));
        connect(d->ui.copyAFSensorValueBtn, SIGNAL(clicked()), this, SLOT(onCopyAFSensorValues()));
        connect(d->ui.copyAFLogBtn, SIGNAL(clicked()), this, SLOT(onCopyAFLogs()));

        //encoder
        connect(d->ui.setEncScaleFactor, &QPushButton::clicked, this, [this]() {
            d->control.SetXEncScaleFactor(d->ui.xEncScaleFactor->value());
            d->control.SetYEncScaleFactor(d->ui.yEncScaleFactor->value());
        });

        connect(d->ui.xScaleMinusMoveBtn, &QPushButton::clicked, this, [this]() {
            d->control.MoveXStepMM(-1 * d->ui.xScaleTargetPosSpin->value());
        });

        connect(d->ui.xScalePlusMoveBtn, &QPushButton::clicked, this, [this]() {
            d->control.MoveXStepMM(d->ui.xScaleTargetPosSpin->value());
        });

        connect(d->ui.yScaleMinusMoveBtn, &QPushButton::clicked, this, [this]() {
            d->control.MoveYStepMM(-1 * d->ui.yScaleTargetPosSpin->value());
        });

        connect(d->ui.yScalePlusMoveBtn, &QPushButton::clicked, this, [this]() {
            d->control.MoveYStepMM(d->ui.yScaleTargetPosSpin->value());
        });

        //misc - replay
        connect(d->ui.openMCULogFileBtn, SIGNAL(clicked()), this, SLOT(onOpenReplayLog()));
        connect(d->ui.replayMCULogBtn, SIGNAL(clicked()), this, SLOT(onReplayMCU()));
        connect(d->ui.stopMCUReplayBtn, SIGNAL(clicked()), this, SLOT(onStopReplay()));
        connect(d->replayObserver, SIGNAL(sigUpdate(int32_t)), this, SLOT(onUpdateReplayProgress(int32_t)));
        connect(d->replayObserver, SIGNAL(sigStopped(const QString&)), this, SLOT(onReplayStopped(const QString&)));
        connect(d->replayObserver, SIGNAL(sigFinished()), this, SLOT(onReplayFinished()));

        //misc - customer packet sender
        connect(d->ui.sendGeneratedPacketBtn, SIGNAL(clicked()), this, SLOT(onSendCustomPacket()));

        connect(&d->initTimer, SIGNAL(timeout()), this, SLOT(onCheckInitProgress()));
    }

    ControlPanel::~ControlPanel() {
    }

    void ControlPanel::onInitializeAuto() {
        if (!d->control.InitializeAuto()) {
            QMessageBox::warning(this, "Auto Initialization", tr("Failed to initialize the instrument"));
            return;
        }

        d->ui.initProgress->setValue(0);
        d->initTimer.start();
    }

    void ControlPanel::onOpenPort() {
        if (!d->control.OpenPort()) {
            QMessageBox::warning(this, "Open Port", tr("Failed to open a communication port"));
            return;
        }
    }

    void ControlPanel::onSetSafeZPos() {
        if (!d->control.SetSafeZPos(d->ui.safeZPosition->value())) {
            QMessageBox::warning(this, "Set SafeZPos", tr("Failed to set safe Z position"));
            return;
        }
    }

    void ControlPanel::onStartInitialization() {
        if (!d->control.StartInitialization()) {
            QMessageBox::warning(this, "Start Initialization", tr("Failed to start initialization"));
            return;
        }

        d->ui.initProgress->setValue(0);
        d->initTimer.start();
    }

    void ControlPanel::onFinishCalibration() {
        if (!d->control.FinishCalibration()) {
            QMessageBox::warning(this, "Finish Calibration", tr("Failed to finish calibration"));
            return;
        }
    }

    void ControlPanel::onCheckDLPCVersion() {
        int32_t fw_ver = 0;
        int32_t sw_ver = 0;

        if (!d->control.CheckDLPCVersion(fw_ver, sw_ver)) {
            QMessageBox::warning(this, "DLPC Version", tr("Failed to check DLCP Version"));
            return;
        }

        d->ui.dlpcFWVer->setValue(fw_ver);
        d->ui.dlpcSWVer->setValue(sw_ver);
    }

    void ControlPanel::onSetLEDIntensity() {
        if (!d->control.SetLEDIntensity(d->ui.ledIntensityRed->value(),
                                        d->ui.ledIntensityGreen->value(),
                                        d->ui.ledIntensityBlue->value())) {
            QMessageBox::warning(this, "LED Intensity", tr("Failed to set LED intensity"));
            return;
        }
    }

    void ControlPanel::onSetDMDExposure() {
        if (!d->control.SetDMDExposure(d->ui.dmdExposure->value(),
                                      d->ui.dmdInterval->value())) {
            QMessageBox::warning(this, "DMD Exposure", tr("Failed to set DMD Exposure"));
            return;
        }
    }

    void ControlPanel::onReadTriggerCount() {
        int32_t count = 0;

        if (!d->control.ReadTriggerCount(count)) {
            QMessageBox::warning(this, "Trigger Count", tr("Failed to read trigger count"));
            return;
        }

        d->ui.generatedTriggerCount->setValue(count);
    }

    void ControlPanel::onResetTriggerCount() {
        if (!d->control.ResetTriggerCount()) {
            QMessageBox::warning(this, "Trigger Count", tr("Failed to reset trigger count"));
            return;
        }
    }

    void ControlPanel::onStartTest() {
        if (!d->control.StartTest()) {
            QMessageBox::warning(this, "Start Test", tr("Failed to start test"));
            return;
        }
    }

    void ControlPanel::onRunMacro() {
        if (!d->control.RunMacro(d->ui.macroGroup->value())) {
            QMessageBox::warning(this, "Run Macro", tr("Failed to run a macro"));
            return;
        }
    }

    void ControlPanel::onRunMacroManual() {
        if (!d->control.RunMacroManual(d->ui.macroGroup1->value())) {
            QMessageBox::warning(this, "Run Macro", tr("Failed to run a macro"));
            return;
        }
    }

    void ControlPanel::onRunMatrixMacro() {
        if (!d->control.RunMatrixMacro(d->ui.matrixCountX->value(), d->ui.matrixCountY->value(),
                                       d->ui.matrixGapX->value(), d->ui.matrixGapY->value(),
                                       d->ui.matrixMacroGroup->value())) {
            QMessageBox::warning(this, "Run Macro", tr("Failed to run a macro"));
            return;
        }
    }

    void ControlPanel::onStopMacro() {
        if (!d->control.StopMacro()) {
            QMessageBox::warning(this ,"Stop Macro", tr("Failed to stop macro"));
            return;
        }
    }

    void ControlPanel::onCheckMacro() {
        if (!d->control.CheckMacro()) {
            QMessageBox::warning(this ,"Stop Macro", tr("Failed to stop macro"));
            return;
        }
    }

    void ControlPanel::onMoveX() {
        if (!d->control.MoveX(d->ui.moveXPos->value())) {
            QMessageBox::warning(this, "Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveY() {
        if (!d->control.MoveY(d->ui.moveYPos->value())) {
            QMessageBox::warning(this, "Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ() {
        if (!d->control.MoveZ(d->ui.moveZPos->value())) {
            QMessageBox::warning(this, "Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveU() {
        if (!d->control.MoveU(d->ui.moveUPos->value())) {
            QMessageBox::warning(this, "Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveV() {
        if (!d->control.MoveV(d->ui.moveVPos->value())) {
            QMessageBox::warning(this, "Move Z", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveW() {
        if (!d->control.MoveW(d->ui.moveWPos->value())) {
            QMessageBox::warning(this, "Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onSetFilterPositions() {
        FilterPositionDialog dlg;
        const auto resp = dlg.exec();
        if(resp != QDialog::DialogCode::Accepted) return;

        auto positions = dlg.GetPositions();
        if(!d->control.SetFilterPositions(positions)) {
            QMessageBox::warning(this, "Filter Positions", tr("Failed to set filter positions"));
            return;
        }
    }

    void ControlPanel::onChangeFilter() {
        if(!d->control.ChangeFilter(d->ui.filterChannel->value())) {
            QMessageBox::warning(this, "Filter Position", tr("Failed to change filter"));
            return;
        }
    }

    void ControlPanel::onSetLEDChannel() {
        if (!d->control.SetLEDChannel(d->ui.ledChannel->value())) {
            QMessageBox::warning(this, "LED Channel", tr("Failed to set LED channel"));
            return;
        }
    }

    void ControlPanel::onSetCameraType() {
        if (!d->control.SetCameraType(d->ui.internalCamCheck->isChecked(), d->ui.externalCamCheck->isChecked())) {
            QMessageBox::warning(this, "Camera Type", tr("Failed to set camera type"));
            return;
        }
    }

    void ControlPanel::onGenerateTrigger() {
        if (!d->control.GenerateTrigger(d->ui.triggerWidth->value(),
                                        d->ui.triggerInterval->value(),
                                        d->ui.triggerCount->value())) {
            QMessageBox::warning(this, "Generate Trigger", tr("Failed to generate trigger signal"));
            return;
        }
    }

    void ControlPanel::onGenerateTrigger1() {
        if (!d->control.GenerateTrigger(d->ui.triggerWidth1->value(),
                                        d->ui.triggerInterval1->value(),
                                        d->ui.triggerCount1->value())) {
            QMessageBox::warning(this, "Generate Trigger", tr("Failed to generate trigger signal"));
            return;
        }
    }

    void ControlPanel::onFinishTest() {
        if (!d->control.FinishTest()) {
            QMessageBox::warning(this, "Finish Test", tr("Failed to finish test"));
            return;
        }
    }

    void ControlPanel::onStartManualMode() {
        if (!d->control.StartManualMode()) {
            QMessageBox::warning(this, "Start Manual Mode", tr("Failed to start manual mode"));
            return;
        }
    }

    void ControlPanel::onStopManualMode() {
        if (!d->control.StopManualMode()) {
            QMessageBox::warning(this, "Stop Manual Mode", tr("Failed to stop manual mode"));
            return;
        }
    }

    void ControlPanel::onMoveX1() {
        if (!d->control.MoveX(d->ui.moveX1Pos->value())) {
            QMessageBox::warning(this, "Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveY1() {
        if (!d->control.MoveY(d->ui.moveY1Pos->value())) {
            QMessageBox::warning(this, "Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ1() {
        if (!d->control.MoveZ(d->ui.moveZ1Pos->value())) {
            QMessageBox::warning(this, "Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveU1() {
        if (!d->control.MoveU(d->ui.moveU1Pos->value())) {
            QMessageBox::warning(this, "Move U", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveV1() {
        if (!d->control.MoveV(d->ui.moveV1Pos->value())) {
            QMessageBox::warning(this, "Move V", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveW1() {
        if (!d->control.MoveW(d->ui.moveW1Pos->value())) {
            QMessageBox::warning(this, "Move W", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveX1StepMinus() {
        const auto pos = 0 - d->ui.moveX1StepPos->value();
        if (!d->control.MoveXStep(pos)) {
            QMessageBox::warning(this, "Step Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveX1StepPlus() {
        const auto pos = d->ui.moveX1StepPos->value();
        if (!d->control.MoveXStep(pos)) {
            QMessageBox::warning(this, "Step Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveY1StepMinus() {
        const auto pos = 0 - d->ui.moveY1StepPos->value();
        if (!d->control.MoveYStep(pos)) {
            QMessageBox::warning(this, "Step Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveY1StepPlus() {
        const auto pos = d->ui.moveY1StepPos->value();
        if (!d->control.MoveYStep(pos)) {
            QMessageBox::warning(this, "Step Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ1StepMinus() {
        const auto pos = 0 - d->ui.moveZ1StepPos->value();
        if (!d->control.MoveZStep(pos)) {
            QMessageBox::warning(this, "Step Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ1StepPlus() {
        const auto pos = d->ui.moveZ1StepPos->value();
        if (!d->control.MoveZStep(pos)) {
            QMessageBox::warning(this, "Step Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveU1StepMinus() {
        const auto pos = 0 - d->ui.moveU1StepPos->value();
        if (!d->control.MoveUStep(pos)) {
            QMessageBox::warning(this, "Step Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveU1StepPlus() {
        const auto pos = d->ui.moveU1StepPos->value();
        if (!d->control.MoveUStep(pos)) {
            QMessageBox::warning(this, "Step Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveV1StepMinus() {
        const auto pos = 0 - d->ui.moveV1StepPos->value();
        if (!d->control.MoveVStep(pos)) {
            QMessageBox::warning(this, "Step Move V", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveV1StepPlus() {
        const auto pos = d->ui.moveV1StepPos->value();
        if (!d->control.MoveVStep(pos)) {
            QMessageBox::warning(this, "Step Move V", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveW1StepMinus() {
        const auto pos = 0 - d->ui.moveW1StepPos->value();
        if (!d->control.MoveWStep(pos)) {
            QMessageBox::warning(this, "Step Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onMoveW1StepPlus() {
        const auto pos = d->ui.moveW1StepPos->value();
        if (!d->control.MoveWStep(pos)) {
            QMessageBox::warning(this, "Step Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onMoveX2() {
        if (!d->control.MoveX(d->ui.moveX2Pos->value())) {
            QMessageBox::warning(this, "Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveY2() {
        if (!d->control.MoveY(d->ui.moveY2Pos->value())) {
            QMessageBox::warning(this, "Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ2() {
        if (!d->control.MoveZ(d->ui.moveZ2Pos->value())) {
            QMessageBox::warning(this, "Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveX2StepMinus() {
        const auto pos = 0 - d->ui.moveX2StepPos->value();
        if (!d->control.MoveXStep(pos)) {
            QMessageBox::warning(this, "Step Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveX2StepPlus() {
        const auto pos = d->ui.moveX2StepPos->value();
        if (!d->control.MoveXStep(pos)) {
            QMessageBox::warning(this, "Step Move X", tr("Failed to move X axis"));
            return;
        }
    }

    void ControlPanel::onMoveY2StepMinus() {
        const auto pos = 0 - d->ui.moveY2StepPos->value();
        if (!d->control.MoveYStep(pos)) {
            QMessageBox::warning(this, "Step Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveY2StepPlus() {
        const auto pos = d->ui.moveY2StepPos->value();
        if (!d->control.MoveYStep(pos)) {
            QMessageBox::warning(this, "Step Move Y", tr("Failed to move Y axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ2StepMinus() {
        const auto pos = 0 - d->ui.moveZ2StepPos->value();
        if (!d->control.MoveZStep(pos)) {
            QMessageBox::warning(this, "Step Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveZ2StepPlus() {
        const auto pos = d->ui.moveZ2StepPos->value();
        if (!d->control.MoveZStep(pos)) {
            QMessageBox::warning(this, "Step Move Z", tr("Failed to move Z axis"));
            return;
        }
    }

    void ControlPanel::onMoveU2() {
        if (!d->control.MoveU(d->ui.moveU2Pos->value())) {
            QMessageBox::warning(this, "Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveV2() {
        if (!d->control.MoveV(d->ui.moveV2Pos->value())) {
            QMessageBox::warning(this, "Move V", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveW2() {
        if (!d->control.MoveW(d->ui.moveW2Pos->value())) {
            QMessageBox::warning(this, "Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onMoveU2StepMinus() {
        const auto pos = 0 - d->ui.moveU2StepPos->value();
        if (!d->control.MoveUStep(pos)) {
            QMessageBox::warning(this, "Step Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveU2StepPlus() {
        const auto pos = d->ui.moveU2StepPos->value();
        if (!d->control.MoveUStep(pos)) {
            QMessageBox::warning(this, "Step Move U", tr("Failed to move U axis"));
            return;
        }
    }

    void ControlPanel::onMoveV2StepMinus() {
        const auto pos = 0 - d->ui.moveV2StepPos->value();
        if (!d->control.MoveVStep(pos)) {
            QMessageBox::warning(this, "Step Move V", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveV2StepPlus() {
        const auto pos = d->ui.moveV2StepPos->value();
        if (!d->control.MoveVStep(pos)) {
            QMessageBox::warning(this, "Step Move V", tr("Failed to move V axis"));
            return;
        }
    }

    void ControlPanel::onMoveW2StepMinus() {
        const auto pos = 0 - d->ui.moveW2StepPos->value();
        if (!d->control.MoveWStep(pos)) {
            QMessageBox::warning(this, "Step Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onMoveW2StepPlus() {
        const auto pos = d->ui.moveW2StepPos->value();
        if (!d->control.MoveWStep(pos)) {
            QMessageBox::warning(this, "Step Move W", tr("Failed to move W axis"));
            return;
        }
    }

    void ControlPanel::onSetDO() {
        const auto channel = d->ui.doChannel->value();
        const auto value = (d->ui.doValue->isChecked()) ? 1 : 0;
        if (!d->control.SetDO(channel, value)) {
            QMessageBox::warning(this, "Set DO", tr("Failed to set digital output port"));
            return;
        }
    }

    void ControlPanel::onGetDI() {
        const auto channel = d->ui.diChannel->value();
        if (!d->control.GetDI(channel)) {
            QMessageBox::warning(this, "Read DI", tr("Failed to get digital input port"));
            return;
        }
    }

    void ControlPanel::onSetLEDChannel2() {
        if (!d->control.SetLEDChannel(d->ui.ledChannel2->value())) {
            QMessageBox::warning(this, "LED Channel", tr("Failed to set LED channel"));
            return;
        }
    }

    void ControlPanel::onSetCameraType2() {
        if (!d->control.SetCameraType(d->ui.internalCam2Check->isChecked(), d->ui.externalCam2Check->isChecked())) {
            QMessageBox::warning(this, "Camera Type", tr("Failed to set camera type"));
            return;
        }
    }

    void ControlPanel::onSetRGBLedIntensity() {
        if(!d->control.SetRGBLedIntensity(d->ui.redLedIntensity->value(),
                                          d->ui.greenLedIntensity->value(),
                                          d->ui.blueLedIntensity->value())) {
            QMessageBox::warning(this, "RGB Led Intensity", tr("Failed to set RGB Led Intensity"));
            return;
        }
    }

    void ControlPanel::onMoveXY() {
        const auto xpos = d->ui.moveXY_XPos->value();
        const auto ypos = d->ui.moveXY_YPos->value();
        if (!d->control.MoveXY(xpos, ypos)) {
            QMessageBox::warning(this, "Move XY", tr("Failed to move XY axis"));
            return;
        }
    }

    void ControlPanel::onStopMotion() {
        if(!d->control.StopMotion()) {
            QMessageBox::warning(this, "Stop", tr("Failed to stop motion"));
            return;
        }
    }

    void ControlPanel::onRecover() {
        if (!d->control.Recover()) {
            QMessageBox::warning(this, "Recover", tr("Failed to recover from error state"));
            return;
        }
    }

    void ControlPanel::onCheckMovingStatus() {
        if (!d->control.CheckMovingStatus(Axis::X)) {
            QMessageBox::warning(this, "Check Axis", tr("Failed to check X axis"));
            return;
        }

        if (!d->control.CheckMovingStatus(Axis::Y)) {
            QMessageBox::warning(this, "Check Axis", tr("Failed to check Y axis"));
            return;
        }

        if (!d->control.CheckMovingStatus(Axis::Z)) {
            QMessageBox::warning(this, "Check Axis", tr("Failed to check Z axis"));
            return;
        }

        if (!d->control.CheckMovingStatus(Axis::L)) {
            QMessageBox::warning(this, "Check Axis", tr("Failed to check Z axis"));
            return;
        }
    }

    void ControlPanel::onGetAxisPosition() {
        if (!d->control.GetAxisPosition(Axis::X)) {
            QMessageBox::warning(this, "Get Position", tr("Failed to get X axis position"));
            return;
        }

        if (!d->control.GetAxisPosition(Axis::Y)) {
            QMessageBox::warning(this, "Get Position", tr("Failed to get Y axis position"));
            return;
        }

        if (!d->control.GetAxisPosition(Axis::Z)) {
            QMessageBox::warning(this, "Get Position", tr("Failed to get Z axis position"));
            return;
        }
    }

    void ControlPanel::onReadTemperature() {
        for (int idx = 0; idx < 4; idx++) {
            if (!d->control.ReadTemperature(idx)) {
                QMessageBox::warning(this, "Read temperature", tr("Failed to read temperature"));
                return;
            }
        }
    }

    void ControlPanel::onReadAcceleration() {
        if (!d->control.ReadAcceleration()) {
            QMessageBox::warning(this, "Read Acceleration", tr("Failed to read acceleration"));
            return;
        }
    }

    void ControlPanel::onReadCartridgeSensor() {
        if (!d->control.ReadCartridgeSensor()) {
            QMessageBox::warning(this, "Read Cartridge Sensor", tr("Failed to read cartridge sensor"));
            return;
        }
    }

    void ControlPanel::onSetMacroStreaming() {
        auto count = d->control.SetMacroStreaming(d->ui.macroStreamGroup->value());
        if(count == 0) {
            d->ui.macroStreamCmdCount->setValue(0);
            QMessageBox::warning(this, "Streaming", "No macro exists");
            return;
        }

        d->ui.macroStreamCmdCount->setValue(count);
    }

    void ControlPanel::onSetMacroAFMode(int mode) {
        d->control.SetMacroAFMode(mode);
    }

    void ControlPanel::onAddCommandsToStreamBuffer() {
        if(!d->control.AddCommandsToStreamingBuffer(d->ui.streamCount->value())) {
            QMessageBox::warning(this, "Streaming", "Failed to send motion commands to macro buffer");
            return;
        }

        d->ui.streamSentCount->setValue(d->control.GetCountSentToStreamingBuffer());
    }

    void ControlPanel::onRunMacroStreaming() {
        if(!d->control.StartMacroStreaming()) {
            QMessageBox::warning(this, "Streaming", "Failed to start macro streaming");
            return;
        }
    }

    void ControlPanel::onStopMacroStreaming() {
        if(!d->control.StopMacroStreaming()) {
            QMessageBox::warning(this, "Streaming", "Failed to stop macro streaming");
            return;
        }
    }

    void ControlPanel::onCheckInitProgress() {
        int32_t progress = 0;
        if (!d->control.CheckInitialiation(progress)) return;

        d->ui.initProgress->setValue(progress);
        if (progress == 100) {
            d->initTimer.stop();

            const auto ver = d->control.ReadFirmwareVersion();
            emit sigUpdateFirmareVersion(ver);
        }
    }

    void ControlPanel::onSetSWLimit() {
        auto xLimit = (d->ui.swLimitXChk->isChecked()) ? d->ui.swLimitXValue->value() : -1;
        auto yLimit = (d->ui.swLimitYChk->isChecked()) ? d->ui.swLimitYValue->value() : -1;
        auto zLimit = (d->ui.swLimitZChk->isChecked()) ? d->ui.swLimitZValue->value() : -1;
        auto uLimit = (d->ui.swLimitUChk->isChecked()) ? d->ui.swLimitUValue->value() : -1;
        auto vLimit = (d->ui.swLimitVChk->isChecked()) ? d->ui.swLimitVValue->value() : -1;
        auto wLimit = (d->ui.swLimitWChk->isChecked()) ? d->ui.swLimitWValue->value() : -1;

        if(!d->control.SetSWLimit({xLimit, yLimit, zLimit, uLimit, vLimit, wLimit})) {
            QMessageBox::warning(this, "Software Limit", "It fails to set software limit");
        }
    }

    void ControlPanel::onSetInPositionBand() {
        const auto xBand = d->ui.inPositionXValue->value();
        const auto yBand = d->ui.inPositionYValue->value();
        const auto zBand = d->ui.inPositionZValue->value();
        const auto uBand = d->ui.inPositionUValue->value();
        const auto vBand = d->ui.inPositionVValue->value();
        const auto wBand = d->ui.inPositionWValue->value();
        if(!d->control.SetInPositionBand({xBand, yBand, zBand, uBand, vBand, wBand})) {
            QMessageBox::warning(this, "In-position Band", "It fails to set in-position band");
        }
    }

    void ControlPanel::onSetAFParameter() {
        TC::MCUControl::MCUAFParam param;

        param.SetLensID(d->ui.afParamLensID->value());
        param.SetInFocusRange(d->ui.afParamInfocus->value());
        param.SetDirection(d->ui.afParamDirection->value());
        param.SetLoopInterval(d->ui.afParamLoopInterval->value());
        param.SetMaxTrialCount(d->ui.afParamMaxTrial->value());
        param.SetSensorValue(d->ui.afParamSensorMin->value(), d->ui.afParamSensorMax->value());
        param.SetResolution(d->ui.afParamResolutionMul->value(), d->ui.afParamResolutionDiv->value());
        param.SetMaxCompensationPulse(d->ui.afParamMaxCompPulse->value());
        param.SetAFLimit(d->ui.afParamAFLimit->value());

        d->control.SetAFParameter(param);
    }

    void ControlPanel::onSetAFTarget() {
        int32_t mode = (d->ui.afUseCurrentValue->isChecked())? 1 : 0;
        int32_t targetOnAFM{ 0 };
        if(!d->control.SetAFTarget(mode, d->ui.afTargetValue->value(), targetOnAFM)) return;
        d->ui.afTargetValueOnAFM->setValue(targetOnAFM);
    }

    void ControlPanel::onInitAF() {
        d->control.InitAF();
    }

    void ControlPanel::onCheckAFInit() {
        auto status = d->control.CheckAFInitialization();
        d->ui.afInitState->setText(status._to_string());
    }

    void ControlPanel::onEnableAF() {
        auto mode = d->ui.afMode->currentIndex();
        d->control.EnableAF(mode);
    }

    void ControlPanel::onCheckAFStatus() {
        auto status = d->control.CheckAFStatus();
        d->ui.afStatusValue->setValue(std::get<0>(status));
        d->ui.afResultValue->setValue(std::get<1>(status));
        d->ui.afTrialCountValue->setValue(std::get<2>(status));
    }

    void ControlPanel::onChangeAFLensParameter() {
        auto param = d->ui.afLensParameter->value();
        d->control.ChangeAFLensParameter(param);
    }

    void ControlPanel::onSetAFPeakMode() {
        auto peakMode = d->ui.afPeakMode->value()==1 ? true : false;
        d->control.SetAFPeakMode(peakMode);
    }

    void ControlPanel::onSetManualAF() {
        const auto laser = d->ui.afLaserChk->isChecked();
        d->control.SetManualAF(laser);
    }

    void ControlPanel::onReadAFLog() {
        QList<int32_t> adcDiff, zComp;
        d->control.ReadAFLog(adcDiff, zComp);

        d->ui.afTuningLogTab->setCurrentIndex(1);
        d->ui.afLogTable->clearContents();
        d->ui.afLogTable->setRowCount(adcDiff.length());

        for(int32_t idx=0; idx<adcDiff.length(); idx++) {
            auto* itemDiff = new QTableWidgetItem(QString::number(adcDiff.at(idx)));
            itemDiff->setFlags(itemDiff->flags() & ~Qt::ItemIsEditable);
            d->ui.afLogTable->setItem(idx, 0, itemDiff);

            auto* itemComp = new QTableWidgetItem(QString::number(zComp.at(idx)));
            itemComp->setFlags(itemComp->flags() & ~Qt::ItemIsEditable);
            d->ui.afLogTable->setItem(idx, 1, itemComp);
        }
    }

    void ControlPanel::onScanZForAFM() {
        d->control.ScanZForAFM(d->ui.afZScanStartPos->value(),
                               d->ui.afZScanEndPos->value(),
                               d->ui.afZScanInterval->value(),
                               d->ui.afZScanDelay->value());
    }

    void ControlPanel::onDumpZScanForAFM() {
        QList<int32_t> data;
        d->control.DumpZScanForAFM(data);

        d->ui.afTuningLogTab->setCurrentIndex(0);
        d->ui.afSensorValueTable->clearContents();
        d->ui.afSensorValueTable->setRowCount(data.length());

        for(int32_t idx=0; idx<data.length(); idx++) {
            auto* item = new QTableWidgetItem(QString::number(data.at(idx)));
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            d->ui.afSensorValueTable->setItem(idx, 0, item);
        }
    }

    void ControlPanel::onCopyAFSensorValues() {
        d->CopyTable(d->ui.afSensorValueTable);
    }

    void ControlPanel::onCopyAFLogs() {
        d->CopyTable(d->ui.afLogTable);
    }

    void ControlPanel::onOpenReplayLog() {
        static QString lastDir;
        const auto path = QFileDialog::getOpenFileName(this, "Open a log file", lastDir, "Text Files (*.txt *.log)");
        if(path.isEmpty()) {
            d->ui.replayMCULogBtn->setDisabled(true);
            d->ui.stopMCUReplayBtn->setDisabled(true);
            return;
        }

        d->ui.replayLogFile->setText(QFileInfo(path).fileName());
        d->ui.replayMCULogBtn->setEnabled(true);

        d->control.SetReplayLog(path);
    }

    void ControlPanel::onReplayMCU() {
        d->ui.replayMCULogBtn->setDisabled(true);
        d->ui.stopMCUReplayBtn->setEnabled(true);
        d->ui.openMCULogFileBtn->setDisabled(true);
        d->ui.replayPeformedCount->setValue(0);

        d->control.ReplayLog(d->ui.replayCount->value(), d->ui.replayDelay->value());
    }

    void ControlPanel::onStopReplay() {
        d->ui.openMCULogFileBtn->setEnabled(true);
        d->ui.replayMCULogBtn->setEnabled(true);
        d->ui.stopMCUReplayBtn->setDisabled(true);

        d->control.StopReplay();
    }

    void ControlPanel::onUpdateReplayProgress(int32_t count) {
        d->ui.replayPeformedCount->setValue(count);
    }

    void ControlPanel::onReplayStopped(const QString& message) {
        QMessageBox::warning(this, "Replay", message);
        d->ui.openMCULogFileBtn->setEnabled(true);
        d->ui.replayMCULogBtn->setEnabled(true);
        d->ui.stopMCUReplayBtn->setDisabled(true);
    }

    void ControlPanel::onReplayFinished() {
        d->ui.openMCULogFileBtn->setEnabled(true);
        d->ui.replayMCULogBtn->setEnabled(true);
        d->ui.stopMCUReplayBtn->setDisabled(true);
    }

    void ControlPanel::onSendCustomPacket() {
        d->ui.packetBuilderResponse->clear();

        const auto cmd = d->ui.packetBuilderCommand->text();
        const auto parameters = d->ui.packetBuilderParameters->text();

        if(cmd.length() != 2) {
            QMessageBox::warning(this, "Custom Packet Sender", "Command packet is invalid");
            return;
        }

        if(parameters.length()%8 != 0) {
            QMessageBox::warning(this, "Custom Packet Sender", "Parameters are invalid");
            return;
        }

        QString resp;
        if(!d->control.SendCustomPacket(cmd, parameters, resp)) {
            QMessageBox::warning(this, "Custom Packet Sender", "It fails to send the custom packet");
        }

        d->ui.packetBuilderResponse->setText(resp);
    }

    void ControlPanel::onDisableCheck(bool disable) {
        d->ui.statusCheckGroupBox->setDisabled(disable);
    }
}
