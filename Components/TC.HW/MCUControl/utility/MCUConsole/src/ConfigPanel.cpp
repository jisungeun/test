#include <QAbstractButton>
#include <QMessageBox>
#include <TCLogger.h>
#include "ConfigPanelControl.h"
#include "ConfigPanel.h"
#include "ui_ConfigPanel.h"

namespace TC::MCUControl {
    struct ConfigPanel::Impl {
        Ui::ConfigPanel ui;
        ConfigPanelControl control;

        auto LoadConfig()->bool;
        auto UpdateGUI(const MCUConfig& config)->void;
        auto UpdateConfig(MCUConfig& config)->bool;
    };

    auto ConfigPanel::Impl::LoadConfig() -> bool {
        for (auto port : control.FindPorts()) {
            ui.port->addItem(QString::number(port), port);
        }

        for (int32_t baudrate : control.FindBaudRates()) {
            ui.baudrate->addItem(QString::number(baudrate), baudrate);
        }

        if (!control.LoadConfig()) return false;

        auto config = control.GetConfig();
        UpdateGUI(config);

        return true;
    }

    auto ConfigPanel::Impl::UpdateGUI(const MCUConfig& config) -> void {
        const auto port = config.GetPort();
        const auto port_idx = ui.port->findData(port);
        if (port_idx > -1) {
            ui.port->setCurrentIndex(port_idx);
        }

        const auto baudrate = config.GetBaudrate();
        const auto barurate_idx = ui.baudrate->findData(baudrate);
        if (barurate_idx > -1) {
            ui.baudrate->setCurrentIndex(barurate_idx);
        }

        const auto safeZpos = config.GetSafeZPosition();
        ui.safeZPosition->setValue(safeZpos);

        const auto tempLower = config.GetTemperatureLowerLimit();
        ui.temperatureLow->setValue(tempLower);

        const auto tempUpper = config.GetTemperatureUpperLimit();
        ui.temperatureHigh->setValue(tempUpper);

        const auto inclination = config.GetInclinationLimit();
        ui.inclination->setValue(inclination);

        const auto resolutionX = config.GetResolution(Axis::X);
        ui.resolutionX->setValue(resolutionX);

        const auto resolutionY = config.GetResolution(Axis::Y);
        ui.resolutionY->setValue(resolutionY);

        const auto resolutionZ = config.GetResolution(Axis::Z);
        ui.resolutionZ->setValue(resolutionZ);

        const auto resolutionU = config.GetResolution(Axis::U);
        ui.resolutionU->setValue(resolutionU);

        const auto resolutionV = config.GetResolution(Axis::V);
        ui.resolutionV->setValue(resolutionV);

        const auto resolutionW = config.GetResolution(Axis::W);
        ui.resolutionW->setValue(resolutionW);
    }

    auto ConfigPanel::Impl::UpdateConfig(MCUConfig& config) -> bool {
        config.SetPort(ui.port->itemData(ui.port->currentIndex()).toInt());
        config.SetBaudrate(ui.baudrate->itemData(ui.baudrate->currentIndex()).toInt());
        config.SetSafeZPosition(ui.safeZPosition->value());
        config.SetTemperatureLimit(ui.temperatureLow->value(), ui.temperatureHigh->value());
        config.SetInclinationLimit(ui.inclination->value());
        config.SetResolution(Axis::X, ui.resolutionX->value());
        config.SetResolution(Axis::Y, ui.resolutionY->value());
        config.SetResolution(Axis::Z, ui.resolutionZ->value());
        config.SetResolution(Axis::U, ui.resolutionU->value());
        config.SetResolution(Axis::V, ui.resolutionV->value());
        config.SetResolution(Axis::W, ui.resolutionW->value());

        if (config.GetResolution(Axis::X) == 0) return false;
        if (config.GetResolution(Axis::Y) == 0) return false;
        if (config.GetResolution(Axis::Z) == 0) return false;
        if (config.GetTemperatureUpperLimit() <= config.GetTemperatureLowerLimit()) return false;

        return true;
    }

    ConfigPanel::ConfigPanel(QWidget* parent) : QDialog(parent), d{ new Impl }{
        d->ui.setupUi(this);
        setWindowTitle("Configuration");
        for (auto button : d->ui.buttonBox->buttons()) {
            button->setFixedWidth(70);
        }

        if (!d->LoadConfig()) {
            QMessageBox::warning(this, "Load Configuration", tr("It fails to load MCU configuration"));
        }
    }

    ConfigPanel::~ConfigPanel() {
    }

    void ConfigPanel::done(int r) {
        auto accepted = (r == QDialog::DialogCode::Accepted);
        if (!accepted) {
            QDialog::done(r);
            return;
        }

        MCUConfig config;
        if (!d->UpdateConfig(config)) {
            auto input = QMessageBox::question(this, "Save Configuration", 
                                               tr("Configuration is not completed. Do you want to check it again?"),
                                               QMessageBox::Yes|QMessageBox::No, QMessageBox::Yes);
            if (input == QMessageBox::Yes) return;

            QDialog::done(QDialog::DialogCode::Rejected);
            return;
        }

        if (!d->control.SaveConfig(config)) {
            QMessageBox::warning(this, "Save Configuration", tr("It fails to save configuration"));
        } else {
            QMessageBox::information(this, "Save Configuration", tr("It saves configuration"));
        }

        QDialog::done(r);
    }
}

