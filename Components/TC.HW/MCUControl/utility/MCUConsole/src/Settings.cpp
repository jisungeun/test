#include <QSettings>
#include <QStandardPaths>
#include <QString>

#include "Settings.h"

#define SIMULATION              "Simulation"
#define CONFIG_PATH             "ConfigPath"
#define MOTIONCOMMANDS_PATH     "MotionCommandsPath"
#define MACROS_PATH             "MacrosPath"

namespace TC::MCUControl {
    Settings::Settings() {
        auto qs = QSettings();
        auto allKeys = qs.allKeys();

        if (!allKeys.contains(SIMULATION)) qs.setValue(SIMULATION, false);
        if (!allKeys.contains(CONFIG_PATH)) {
            const auto defaultPath = QString("%1/config/mcuconfig.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(CONFIG_PATH, defaultPath);
        }
        if (!allKeys.contains(MOTIONCOMMANDS_PATH)) {
            const auto defaultPath = QString("%1/config/motioncommands.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(MOTIONCOMMANDS_PATH, defaultPath);
        }
        if (!allKeys.contains(MACROS_PATH)) {
            const auto defaultPath = QString("%1/config/macros.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(MACROS_PATH, defaultPath);
        }
    }

    Settings::~Settings() {
    }

    auto Settings::GetInstance() -> Pointer {
        static Pointer theInstance{ new Settings() };
        return theInstance;
    }

    auto Settings::GetValue(const QString& key) const -> QVariant {
        return QSettings().value(key);
    }

    auto Settings::GetSimulation() -> bool {
        auto settings = GetInstance();
        return settings->GetValue(SIMULATION).toBool();
    }

    auto Settings::GetConfigPath() -> QString {
        auto settings = GetInstance();
        return settings->GetValue(CONFIG_PATH).toString();
    }

    auto Settings::GetMotionCommandsPath() -> QString {
        auto settings = GetInstance();
        return settings->GetValue(MOTIONCOMMANDS_PATH).toString();
    }

    auto Settings::GetMacrosPath() -> QString {
        auto settings = GetInstance();
        return settings->GetValue(MACROS_PATH).toString();
    }
}
