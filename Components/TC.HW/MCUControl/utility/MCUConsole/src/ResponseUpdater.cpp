#include <QList>
#include <QMutexLocker>

#include "ResponseObserver.h"
#include "ResponseUpdater.h"

namespace TC::MCUControl {
    struct ResponseUpdater::Impl {
        QList<ResponseObserver*> observers;
        QMutex mutex;
        MCUResponse response;
    };

    ResponseUpdater::ResponseUpdater() : d{ new Impl } {
    }

    ResponseUpdater::~ResponseUpdater() {
    }

    auto ResponseUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ResponseUpdater() };
        return theInstance;
    }

    auto ResponseUpdater::Register(ResponseObserver* observer) -> void {
        QMutexLocker locker(&d->mutex);
        d->observers.push_back(observer);
    }

    auto ResponseUpdater::UpdateResponse(const MCUResponse& response) -> void {
        QMutexLocker locker(&d->mutex);
        d->response = response;
        for (auto observer : d->observers) observer->Update(response);
    }

    auto ResponseUpdater::UpdateDI(int32_t channel, Flag flag) -> void {
        QMutexLocker locker(&d->mutex);
        //TBD
        //for (auto observer : d->observers) observer->Update();
    }

    auto ResponseUpdater::UpdateMovingStatus(Axis axis, Flag flag) -> void {
        QMutexLocker locker(&d->mutex);
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXMoving };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXMoving;
                break;
            case Axis::Y:
                resp = Response::AxisYMoving;
                break;
            case Axis::Z:
                resp = Response::AxisZMoving;
                break;
            case Axis::L:
                resp = Response::AxisLMoving;
                break;
            case Axis::U:
                resp = Response::AxisUMoving;
                break;
            case Axis::V:
                resp = Response::AxisVMoving;
                break;
            case Axis::W:
                resp = Response::AxisWMoving;
                break;
            }
            return resp;
        };

        d->response.SetValue(respCode(axis), flag._to_integral());

        for (auto observer : d->observers) observer->Update(d->response);
    }

    auto ResponseUpdater::UpdatePosition(Axis axis, int32_t position) -> void {
        QMutexLocker locker(&d->mutex);
        auto respCode = [](Axis axis)->Response {
            Response resp{ Response::AxisXPosition };
            switch (axis) {
            case Axis::X:
                resp = Response::AxisXPosition;
                break;
            case Axis::Y:
                resp = Response::AxisYPosition;
                break;
            case Axis::Z:
                resp = Response::AxisZPosition;
                break;
            case Axis::U:
                resp = Response::AxisUPosition;
                break;
            case Axis::V:
                resp = Response::AxisVPosition;
                break;
            case Axis::W:
                resp = Response::AxisWPosition;
                break;
            }
            return resp;
        };

        d->response.SetValue(respCode(axis), position);

        for (auto observer : d->observers) observer->Update(d->response);
    }

    auto ResponseUpdater::UpdateTemperature(int32_t sensor, float temperature) -> void {
        QMutexLocker locker(&d->mutex);
        auto respCode = [](int32_t sensor)->Response {
            Response resp{ Response::Temperature0 };
            switch (sensor) {
            case 0:
                resp = Response::Temperature0;
                break;
            case 1:
                resp = Response::Temperature1;
                break;
            case 2:
                resp = Response::Temperature2;
                break;
            case 3:
                resp = Response::Temperature3;
                break;
            }
            return resp;
        };

        d->response.SetValue(respCode(sensor), temperature);

        for (auto observer : d->observers) observer->Update(d->response);
    }

    auto ResponseUpdater::UpdateAcceleration(float xAngle, float yAngle, float zAngle) -> void {
        QMutexLocker locker(&d->mutex);
        d->response.SetValue(Response::AngleX, xAngle);
        d->response.SetValue(Response::AngleY, yAngle);
        d->response.SetValue(Response::AngleZ, zAngle);
        for (auto observer : d->observers) observer->Update(d->response);
    }

    auto ResponseUpdater::UpdateCartirdge(Flag inlet, Flag loading) -> void {
        QMutexLocker locker(&d->mutex);
        d->response.SetValue(Response::AxisLStart, inlet._to_integral());
        d->response.SetValue(Response::AxisLEnd, loading._to_integral());
        for (auto observer : d->observers) observer->Update(d->response);
    }

    auto ResponseUpdater::UpdateAFSensorValue(int32_t value) -> void {
        for (auto observer : d->observers) {
            observer->UpdateAFSensor(value);
        }
    }
}
