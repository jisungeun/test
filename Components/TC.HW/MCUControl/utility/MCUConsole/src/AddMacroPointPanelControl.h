#pragma once
#include <memory>
#include <QList>
#include <QString>

#include "MCUConsoleDefines.h"

namespace TC::MCUControl {
    class AddMacroPointPanelControl {
    public:
        struct Command {
            uint32_t id;
            QString title;
        };

    public:
        AddMacroPointPanelControl();
        virtual ~AddMacroPointPanelControl();

        auto GetMotionCommands() const->QList<Command>;
        auto GetPoints(int32_t xCenter, int32_t yCenter, int32_t xCount, int32_t yCount, 
                       int32_t xGap, int32_t yGap, int32_t zPos) const->QList<Point>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}