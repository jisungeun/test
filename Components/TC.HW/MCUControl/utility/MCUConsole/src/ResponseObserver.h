#pragma once
#include <memory>

#include <QObject>

#include <MCUResponse.h>

namespace TC::MCUControl {
    class ResponseObserver : public QObject {
        Q_OBJECT
    public:
        ResponseObserver(QObject* parent = nullptr);
        virtual ~ResponseObserver();

        auto Update(const MCUResponse& response)->void;
        auto GetResponse() const->MCUResponse;

        auto UpdateAFSensor(int32_t value)->void;

    signals:
        void sigUpdate();
        void sigUpdateAFSensor(int32_t value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}