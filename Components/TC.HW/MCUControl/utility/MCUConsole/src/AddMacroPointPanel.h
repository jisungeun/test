#pragma once
#include <memory>
#include <QDialog>
#include <QList>

#include "MCUConsoleDefines.h"

namespace TC::MCUControl {
    class AddMacroPointPanel : public QDialog {
        Q_OBJECT

    public:
        AddMacroPointPanel(QWidget* parent = nullptr);
        virtual ~AddMacroPointPanel();

        auto GetPoints()->QList<Point>;
        auto GetCommandID()->uint32_t;
        auto GetAFMode()->int32_t;

    protected slots:
        void onSelectType(int state);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}