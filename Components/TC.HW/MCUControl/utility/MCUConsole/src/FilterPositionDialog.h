#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class FilterPositionDialog : public QDialog {
        Q_OBJECT
    public:
        FilterPositionDialog(QWidget* parent = nullptr);
        virtual ~FilterPositionDialog();

        auto GetPositions() const->QList<int32_t>;

    protected:
        void done(int r) override;

    private:
        auto SetPositions(const QList<int32_t>& positions)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
