#pragma once
#include <memory>

#include <QDialog>
#include <QAbstractButton>

namespace TC::MCUControl {
    class MacroPanel : public QDialog {
        Q_OBJECT
    public:
        MacroPanel(QWidget* parent = nullptr);
        virtual ~MacroPanel();

    protected slots:
        void onDialogButtonClicked(QAbstractButton* button);
        void onNewGroup();
        void onDeleteGroup();
        void onCopyGroup();
        void onAddPoint();
        void onDeletePoint();
        void onMoveUpPoint();
        void onMoveDownPoint();
        void onMacroSelected();
        void onPointsChanged(int row,int column);

    protected:
        void done(int r) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}