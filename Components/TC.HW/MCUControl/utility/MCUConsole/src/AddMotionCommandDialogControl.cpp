#include <MCUMotionCommandFactory.h>
#include "AddMotionCommandDialogControl.h"

namespace TC::MCUControl {
    struct AddMotionCommandDialogControl::Impl {
    };

    AddMotionCommandDialogControl::AddMotionCommandDialogControl() : d{ new Impl } {
    }

    AddMotionCommandDialogControl::~AddMotionCommandDialogControl() {
    }

    auto AddMotionCommandDialogControl::GetTypes() const -> QList<Type> {
        auto factory = MCUMotionCommandFactory::GetInstance();
        auto types = factory->GetCommandTypes();
        auto typeStrs = factory->GetCommandTypesAsString();

        QList<Type> list;
        for(int idx=0; idx<types.length(); idx++) {
            Type type{ types.at(idx), typeStrs.at(idx) };
            list.push_back(type);
        }

        return list;
    }
}