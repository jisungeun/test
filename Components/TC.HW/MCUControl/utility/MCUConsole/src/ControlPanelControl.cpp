#define LOGGER_TAG "[ControlPanel]"
#include <TCLogger.h>

#include <MCUFactory.h>

#include "Settings.h"
#include "ResponseUpdater.h"
#include "MCUReplayer.h"
#include "ControlPanelControl.h"

namespace TC::MCUControl {
    struct ControlPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;

        struct {
            int32_t group{ 0 };
            int32_t count{ 0 };
            int32_t index{ 0 };
            int32_t afMode{ 0 };
        } streaming;

        struct {
            QString path;
            int32_t count;
            int32_t delay;
        } replay;

        struct {
            double xRatio{ 1.0 };
            double yRatio{ 1.0 };
        } encScale;

        auto UpdateMacro()->bool;
    };

    auto ControlPanelControl::Impl::UpdateMacro() -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        const auto count = repo->GetCounts();
        for (uint32_t idx = 0; idx < count; idx++) {
            auto macros = repo->GetMacroGroupByIdx(idx);
            auto gid = macros->GetGroupID();
            if(!mcuControl->ClearMacro(gid)) return false;

            auto macroCount = macros->GetCount();
            for(uint32_t macroIdx=0; macroIdx<macroCount; macroIdx++) {
                auto macro = macros->GetMacro(macroIdx);
                if(!mcuControl->AddMacroPoint(gid, macroIdx, 
                                              macro->GetMotionCommand(),
                                              macro->GetPositionX(), 
                                              macro->GetPositionY(),
                                              macro->GetPositionZ(),
                                              macro->GetAFMode())) {
                    return false;
                }
            }
        }
        return true;
    }

    ControlPanelControl::ControlPanelControl() : d { new Impl } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());

        auto repo = MCUMacroRepository::GetInstance();
        repo->Load(Settings::GetMacrosPath());

        auto cmdRepo = MCUMotionCommandRepository::GetInstance();
        cmdRepo->Load(Settings::GetMotionCommandsPath());
    }

    ControlPanelControl::~ControlPanelControl() {
    }

    auto ControlPanelControl::InitializeAuto() -> bool {
        if (!d->mcuControl->OpenPort()) return false;
        if (!d->mcuControl->SetSafeZPosition()) return false;
        if (!d->mcuControl->StartInitialization()) return false;
        return true;
    }

    auto ControlPanelControl::OpenPort() -> bool {
        return d->mcuControl->OpenPort();
    }

    auto ControlPanelControl::SetSafeZPos(int32_t pos) -> bool {
        return d->mcuControl->SetSafeZPosition(pos);
    }

    auto ControlPanelControl::StartInitialization() -> bool {
        return d->mcuControl->StartInitialization();
    }

    auto ControlPanelControl::CheckInitialiation(int32_t& progress) -> bool {
        return d->mcuControl->CheckInitialization(progress);
    }

    auto ControlPanelControl::FinishCalibration() -> bool {
        return d->mcuControl->FinishCalibration();
    }

    auto ControlPanelControl::CheckDLPCVersion(int32_t& fw_ver, int32_t& sw_ver) -> bool {
        return d->mcuControl->GetDLPCVersion(fw_ver, sw_ver);
    }

    auto ControlPanelControl::SetLEDIntensity(int32_t red, int32_t green, int32_t blue) -> bool {
        return d->mcuControl->SetLEDIntensity(red, green, blue);
    }

    auto ControlPanelControl::SetDMDExposure(int32_t exposure, int32_t interval) -> bool {
        return d->mcuControl->SetDMDExposure(exposure, interval);
    }

    auto ControlPanelControl::ReadTriggerCount(int32_t& count) -> bool {
        return d->mcuControl->ReadTriggerCount(count);
    }

    auto ControlPanelControl::ResetTriggerCount() -> bool {
        return d->mcuControl->ResetTriggerCount();
    }

    auto ControlPanelControl::StartTest() -> bool {
        return d->mcuControl->StartTest();
    }

    auto ControlPanelControl::RunMacro(int32_t group) -> bool {
        return d->mcuControl->RunMacro(group);
    }

    auto ControlPanelControl::RunMacroManual(int32_t group) -> bool {
        int32_t xpos, ypos, zpos;
        if(!d->mcuControl->GetPosition(Axis::X, xpos)) return false;
        if(!d->mcuControl->GetPosition(Axis::Y, ypos)) return false;
        if(!d->mcuControl->GetPosition(Axis::Z, zpos)) return false;

        auto repo = MCUMacroRepository::GetInstance();
        if(!repo->Load(Settings::GetMacrosPath())) return false;

        auto& macroGroup = repo->GetMacroGroup(group);

        const auto count = macroGroup->GetCount();
        for(uint32_t idx=0; idx<count; idx++) {
            auto& macro = macroGroup->GetMacro(idx);
            macro->SetPosition(xpos, ypos, zpos);
        }

        if(!d->UpdateMacro()) return false;

        return d->mcuControl->RunMacro(group);
    }

    auto ControlPanelControl::RunMatrixMacro(int32_t countX, int32_t countY, int32_t gapX, int32_t gapY,
        int32_t group) -> bool {
        int32_t xpos, ypos, zpos;
        if(!d->mcuControl->GetPosition(Axis::X, xpos)) return false;
        if(!d->mcuControl->GetPosition(Axis::Y, ypos)) return false;
        if(!d->mcuControl->GetPosition(Axis::Z, zpos)) return false;

        auto repo = MCUMacroRepository::GetInstance();
        if(!repo->Load(Settings::GetMacrosPath())) return false;

        auto& macroGroup = repo->GetMacroGroup(group);
        if(macroGroup == nullptr) return false;

        int32_t iterCount = 0;
        for(int xIdx=0; xIdx<countX; xIdx++) {
            for(int yIdx=0; yIdx<countY; yIdx++) {
                int32_t curX = xpos + (gapX * xIdx);
                int32_t curY = ypos + (gapY * yIdx);

                QLOG_INFO() << "RunMatrixMacro [" << ++iterCount << "/" << (countX * countY) << "] X=" << curX << " Y=" << countY;

                const auto count = macroGroup->GetCount();
                for(uint32_t idx=0; idx<count; idx++) {
                    auto& macro = macroGroup->GetMacro(idx);
                    macro->SetPosition(curX, curY, zpos);
                }

                if(!d->UpdateMacro()) return false;

                if(!d->mcuControl->RunMacro(group)) return false;

                Flag macroStatus{ Flag::Running };
                uint32_t macroStep;

                do {
                    QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
                    d->mcuControl->CheckMacroStatus(macroStatus, macroStep);
                } while(macroStatus == +Flag::Running);
            }
        }

        return true;
    }

    auto ControlPanelControl::StopMacro() -> bool {
        return d->mcuControl->StopMacro();
    }

    auto ControlPanelControl::CheckMacro() -> bool {
        MCUControl::Flag flag{ MCUControl::Flag::Idle };
        uint32_t step;

        return d->mcuControl->CheckMacroStatus(flag, step);
    }

    auto ControlPanelControl::MoveX(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::X, pos);
    }

    auto ControlPanelControl::MoveY(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::Y, pos);
    }

    auto ControlPanelControl::MoveZ(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::Z, pos);
    }

    auto ControlPanelControl::MoveU(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::U, pos);
    }

    auto ControlPanelControl::MoveV(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::V, pos);
    }

    auto ControlPanelControl::MoveW(int32_t pos) -> bool {
        return d->mcuControl->Move(Axis::W, pos);
    }

    auto ControlPanelControl::SetFilterPositions(const QList<int32_t>& positions) -> bool {
        return d->mcuControl->SetFilterPositions(positions);
    }

    auto ControlPanelControl::ChangeFilter(int32_t channel) -> bool {
        return d->mcuControl->ChangeFilter(channel);
    }

    auto ControlPanelControl::SetLEDChannel(int32_t channel) -> bool {
        return d->mcuControl->SetLEDChannel(channel);
    }

    auto ControlPanelControl::SetCameraType(bool internalCamera, bool externalCamera) -> bool {
        return d->mcuControl->SetCameraType(internalCamera, externalCamera);
    }

    auto ControlPanelControl::SetRGBLedIntensity(int32_t red, int32_t green, int32_t blue) -> bool {
        return d->mcuControl->SetRGBLedIntensity(red, green, blue);
    }

    auto ControlPanelControl::MoveXStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::X, position)) return false;
        return d->mcuControl->Move(Axis::X, position+pos);
    }

    auto ControlPanelControl::MoveYStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::Y, position)) return false;
        return d->mcuControl->Move(Axis::Y, position+pos);
    }

    auto ControlPanelControl::MoveZStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::Z, position)) return false;
        return d->mcuControl->Move(Axis::Z, position+pos);
    }

    auto ControlPanelControl::MoveUStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::U, position)) return false;
        return d->mcuControl->Move(Axis::U, position+pos);
    }

    auto ControlPanelControl::MoveVStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::V, position)) return false;
        return d->mcuControl->Move(Axis::V, position+pos);
    }

    auto ControlPanelControl::MoveWStep(int32_t pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::W, position)) return false;
        return d->mcuControl->Move(Axis::W, position+pos);
    }

    auto ControlPanelControl::SetXEncScaleFactor(double factor) -> void {
        d->encScale.xRatio = factor;
    }

    auto ControlPanelControl::SetYEncScaleFactor(double factor) -> void {
        d->encScale.yRatio = factor;
    }

    auto ControlPanelControl::MoveXStepMM(double pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::X, position)) return false;

        auto config = d->mcuControl->GetConfig();
        auto delta = static_cast<int32_t>(pos * d->encScale.xRatio * config.GetResolution(Axis::X));

        return d->mcuControl->Move(Axis::X, position+delta);
    }

    auto ControlPanelControl::MoveYStepMM(double pos) -> bool {
        int32_t position;
        if(!d->mcuControl->GetPosition(Axis::Y, position)) return false;

        auto config = d->mcuControl->GetConfig();
        auto delta = static_cast<int32_t>(pos * d->encScale.yRatio * config.GetResolution(Axis::Y));

        return d->mcuControl->Move(Axis::Y, position+delta);
    }

    auto ControlPanelControl::GenerateTrigger(int32_t width, int32_t interval, int32_t count) -> bool {
        return d->mcuControl->GenerateTrigger(width, interval, count);
    }

    auto ControlPanelControl::FinishTest() -> bool {
        return d->mcuControl->FinishTest();
    }

    auto ControlPanelControl::StartManualMode() -> bool {
        return d->mcuControl->StartManualMode();
    }

    auto ControlPanelControl::StopManualMode() -> bool {
        return d->mcuControl->StopManualMode();
    }

    auto ControlPanelControl::SetDO(int32_t channel, int32_t value) -> bool {
        return d->mcuControl->SetDO(channel, value);
    }

    auto ControlPanelControl::GetDI(int32_t channel) -> bool {
        Flag status{ Flag::Low };
        if (!d->mcuControl->ReadDI(channel, status)) return false;
        ResponseUpdater::GetInstance()->UpdateDI(channel, status);
        return true;
    }

    auto ControlPanelControl::MoveXY(int32_t xpos, int32_t ypos) -> bool {
        return d->mcuControl->MoveXY(xpos, ypos);
    }

    auto ControlPanelControl::StopMotion() -> bool {
        return d->mcuControl->StopMotion();
    }

    auto ControlPanelControl::Recover() -> bool {
        return d->mcuControl->Recover();
    }

    auto ControlPanelControl::CheckMovingStatus(Axis axis) -> bool {
        Flag status{ Flag::NotMoving };
        if (!d->mcuControl->IsMoving(axis, status)) return false;
        ResponseUpdater::GetInstance()->UpdateMovingStatus(axis, status);
        return true;
    }

    auto ControlPanelControl::GetAxisPosition(Axis axis) -> bool {
        int32_t position;
        if (!d->mcuControl->GetPosition(axis, position)) return false;
        ResponseUpdater::GetInstance()->UpdatePosition(axis, position);
        return true;
    }

    auto ControlPanelControl::ReadTemperature(int32_t sensor) -> bool {
        float temperature = 0;
        if (!d->mcuControl->ReadTemperature(sensor, temperature)) return false;
        ResponseUpdater::GetInstance()->UpdateTemperature(sensor, temperature);
        return true;
    }

    auto ControlPanelControl::ReadAcceleration() -> bool {
        float xAngle = 0, yAngle = 0, zAngle = 0;
        if (!d->mcuControl->ReadAcceleration(xAngle, yAngle, zAngle)) return false;
        ResponseUpdater::GetInstance()->UpdateAcceleration(xAngle, yAngle, zAngle);
        return true;
    }

    auto ControlPanelControl::ReadCartridgeSensor() -> bool {
        Flag inlet{ Flag::NotTriggered };
        Flag loading{ Flag::NotTriggered };
        if (!d->mcuControl->ReadCartridgeSensor(inlet, loading)) return false;
        ResponseUpdater::GetInstance()->UpdateCartirdge(inlet, loading);
        return true;
    }

    auto ControlPanelControl::ReadFirmwareVersion() -> QString {
        return d->mcuControl->ReadMCUFirmwareVersion();
    }

    auto ControlPanelControl::SetMacroStreaming(int32_t group) -> int32_t {
        auto repo = MCUMacroRepository::GetInstance();
        if(!repo->Load(Settings::GetMacrosPath())) return 0;

        auto macroGroup = repo->GetMacroGroup(group);
        if(!macroGroup) return 0;

        d->streaming.count = macroGroup->GetCount();
        d->streaming.group = group;
        d->streaming.index = 0;

        return d->streaming.count;
    }

    auto ControlPanelControl::SetMacroAFMode(int32_t mode) -> void {
        d->streaming.afMode = mode;
    }

    auto ControlPanelControl::AddCommandsToStreamingBuffer(int32_t count) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        if(!repo) return false;

        auto macroGroup = repo->GetMacroGroup(d->streaming.group);
        if(!macroGroup) return false;

        auto cmdRepo = MCUMotionCommandRepository::GetInstance();
        if(!cmdRepo) return false;

        QList<QList<int32_t>> commands;

        const auto startIdx = d->streaming.index;
        const auto endIdx = std::min<int32_t>(d->streaming.count, d->streaming.index + count);
        for(auto idx=startIdx; idx<endIdx; idx++) {
            const auto macro = macroGroup->GetMacro(idx);
            if(!macro) return false;

            QList<int32_t> params;
            params.push_back(macro->GetPositionX());
            params.push_back(macro->GetPositionY());
            params.push_back(macro->GetPositionZ());
            params.push_back(d->streaming.afMode);

            const auto cmd = cmdRepo->GetCommand(macro->GetMotionCommand());
            if(!cmd) return false;

            params.append(cmd->GetCommandType());
            params.append(cmd->GetParameterCount());
            params.append(cmd->GetParameters());

            commands.push_back(params);
        }

        if(!d->mcuControl->SendMacrosToStreamBuffer(commands)) return false;
        d->streaming.index = endIdx;

        return true;
    }

    auto ControlPanelControl::GetCountSentToStreamingBuffer() -> int32_t {
        return d->streaming.index;
    }

    auto ControlPanelControl::StartMacroStreaming() -> bool {
        return d->mcuControl->StartMacroStreaming();
    }

    auto ControlPanelControl::StopMacroStreaming() -> bool {
        return d->mcuControl->StopMacroStreaming();
    }

    auto ControlPanelControl::SetSWLimit(const QList<int32_t>& limits) -> bool {
        return d->mcuControl->SetSWLimitPosition(limits);
    }

    auto ControlPanelControl::SetInPositionBand(const QList<int32_t>& bands) -> bool {
        return d->mcuControl->SetInPositionBand(bands);
    }

    auto ControlPanelControl::SetAFParameter(const TC::MCUControl::MCUAFParam& param) -> bool {
        return d->mcuControl->SetAFParameter(param);
    }

    auto ControlPanelControl::SetAFTarget(int32_t mode, int32_t value, int32_t& targetOnAFM) -> bool {
        return d->mcuControl->SetAFTarget(mode, value, targetOnAFM);
    }

    auto ControlPanelControl::InitAF() -> bool {
        return d->mcuControl->InitAF();
    }

    auto ControlPanelControl::CheckAFInitialization() -> AFInitState {
        AFInitState status{ AFInitState::Error };
        d->mcuControl->CheckAFInitialization(status);
        return status;
    }

    auto ControlPanelControl::EnableAF(int32_t mode) -> bool {
        return d->mcuControl->EnableAF(mode);
    }

    auto ControlPanelControl::CheckAFStatus() -> std::tuple<int32_t, int32_t, int32_t> {
        int32_t status{ -1 };
        int32_t result{ 0 };
        int32_t trialCount{ 0 };
        d->mcuControl->CheckAFStatus(status, result, trialCount);
        return std::tuple(status, result, trialCount);
    }

    auto ControlPanelControl::ChangeAFLensParameter(int32_t param) -> bool {
        return d->mcuControl->ChangeLensParameter(param);
    }

    auto ControlPanelControl::SetAFPeakMode(bool peakmode) -> bool {
        return d->mcuControl->SetAFMPeakMode(peakmode);
    }

    auto ControlPanelControl::SetManualAF(bool laser) -> bool {
        return d->mcuControl->SetManualAF(laser);
    }

    auto ControlPanelControl::ReadAFLog(QList<int32_t>& adcDiff, QList<int32_t>& zComp) -> bool {
        return d->mcuControl->ReadAFLog(adcDiff, zComp);
    }

    auto ControlPanelControl::ScanZForAFM(int32_t startPos, int32_t endPos, int32_t interval, int32_t delay) -> bool {
        return d->mcuControl->ScanZForAFM(startPos, endPos, interval, delay);
    }

    auto ControlPanelControl::DumpZScanForAFM(QList<int32_t>& values) -> bool {
        return d->mcuControl->DumpZScanForAFM(values);
    }

    auto ControlPanelControl::SetReplayLog(const QString& path) -> void {
        d->replay.path = path;
    }

    auto ControlPanelControl::ReplayLog(int32_t runCount, int32_t intervalMSec) -> void {
        d->replay.count = runCount;
        d->replay.delay = intervalMSec;
        MCUReplayer::GetInstance()->RunReplay(d->replay.path, d->replay.count, d->replay.delay);
    }

    auto ControlPanelControl::StopReplay() -> void {
        MCUReplayer::GetInstance()->StopReplay();
    }

    auto ControlPanelControl::SendCustomPacket(const QString& cmd, const QString& parameters, QString& resp) -> bool {
        QByteArray respPacket;
        ResponseCode respCode{ ResponseCode::Fail };

        if(!d->mcuControl->SendPacket(cmd, parameters, respPacket, respCode)) {
            QLOG_ERROR() << "It fails to send the custom packet";
            return false;
        }

        resp = QString::fromStdString(respPacket.toStdString());
        return true;
    }
}
