#include <cmath>
#include <MCUMotionCommandRepository.h>

#include "AddMacroPointPanelControl.h"

namespace TC::MCUControl {
    struct AddMacroPointPanelControl::Impl {
        auto generate(const int x0, const int gap, const int count)->std::vector<int>;
    };

    auto AddMacroPointPanelControl::Impl::generate(const int x0, const int gap, const int count) -> std::vector<int> {
        const int first = 0 - std::round(count / 2);
        std::vector<int> series(count);

        std::generate(series.begin(), series.end(), [n = first, x0, gap]() mutable {
            return x0 + (gap * n++);
                      });

        return series;
    }

    AddMacroPointPanelControl::AddMacroPointPanelControl() : d{ new Impl } {
    }

    AddMacroPointPanelControl::~AddMacroPointPanelControl() {
    }

    auto AddMacroPointPanelControl::GetMotionCommands() const -> QList<Command> {
        auto repo = MCUMotionCommandRepository::GetInstance();

        QList<Command> list;
        const auto counts = repo->GetCounts();
        for (auto idx = 0u; idx < counts; idx++) {
            auto cmd = repo->CloneCommandByIndex(idx);

            Command item;
            item.id = cmd->GetCommandID();
            item.title = cmd->GetCommandTitle();
            list.push_back(item);
        }

        return list;
    }

    auto AddMacroPointPanelControl::GetPoints(int32_t xCenter, int32_t yCenter, int32_t xCount, int32_t yCount,
        int32_t xGap, int32_t yGap, int32_t zPos) const -> QList<Point> {
        QList<Point> points;
        auto xpos = d->generate(xCenter, xGap, xCount);
        auto ypos = d->generate(yCenter, yGap, yCount);

        for (auto x : xpos) {
            for (auto y : ypos) {
                Point point{ x, y, zPos };
                points.push_back(point);
            }
        }

        return points;
    }
}
