#pragma once
#include <memory>
#include <QString>
#include <QList>

#include <MCUResponse.h>

namespace TC::MCUControl {
    class PacketParserDialogControl {
    public:

        struct Result {
            using Pointer = std::shared_ptr<Result>;

            QString commdand;
            QList<int32_t> parameters;
        };

    public:
        PacketParserDialogControl();
        ~PacketParserDialogControl();

        auto ParseCommand(const QString& command) const->Result::Pointer;
        auto ParseResponse(const QString& response) const->Result::Pointer;
        auto ParseStatusResponse(const QString& response) const->MCUResponse;
    };
}