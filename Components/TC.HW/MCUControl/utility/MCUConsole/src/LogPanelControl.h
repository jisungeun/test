#pragma once
#include <memory>

#include <QList>
#include <QByteArray>

namespace TC::MCUControl {
    class LogPanelControl {
    public:
        LogPanelControl();
        virtual ~LogPanelControl();

        auto Save(const QString& text, const QString& path)->bool;
        auto ReadMCULog()->QList<QString>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}