#define LOGGER_TAG "[Replay]"
#include <QList>
#include <QMutexLocker>
#include <QFile>
#include <QTextStream>

#include <TCLogger.h>
#include <MCUFactory.h>

#include "MCUReplayerObserver.h"
#include "MCUReplayer.h"

namespace TC::MCUControl {
    struct MCUReplayer::Impl {
        QList<MCUReplayerObserver*> observers;

        QString path;
        int32_t count;
        int32_t delay;

        QList<QByteArray> packets;

        bool running{ false };
        QMutex mutex;

        auto LoadPackets()->void;
        auto UpdateProgress(int32_t count)->void;
        auto UpdateStopped(const QString& message)->void;
        auto UpdateFinished()->void;
    };

    auto MCUReplayer::Impl::LoadPackets() -> void {
        packets.clear();

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly  | QIODevice::Text))
            return;

        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();

            const auto startIdx = line.indexOf("{");
            if(startIdx == -1) continue;

            const auto endIdx = line.indexOf("}", startIdx);
            if(endIdx == -1) continue;

            const auto packet = line.mid(startIdx, endIdx-startIdx+1);
            QLOG_INFO() << "[" << packets.size() + 1 << "] " << packet;

            packets.push_back(packet.toLocal8Bit());
        }
    }

    auto MCUReplayer::Impl::UpdateProgress(int32_t count) -> void {
        for(auto observer : observers) {
            observer->UpdateProgress(count);
        }
    }

    auto MCUReplayer::Impl::UpdateStopped(const QString& message) -> void {
        QLOG_ERROR() << message;
        for(auto observer : observers) {
            observer->UpdateStopped(message);
        }
    }

    auto MCUReplayer::Impl::UpdateFinished() -> void {
        for(auto observer : observers) {
            observer->UpdateFinished();
        }
    }

    MCUReplayer::MCUReplayer() : QThread(), d{new Impl} {
    }

    MCUReplayer::~MCUReplayer() {
    }

    auto MCUReplayer::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCUReplayer() };
        return theInstance;
    }

    auto MCUReplayer::RunReplay(const QString& path, int32_t count, int32_t delay) -> void {
        QMutexLocker locker(&d->mutex);

        d->path = path;
        d->count = count;
        d->delay = delay;

        d->running = true;
        start();
    }

    auto MCUReplayer::StopReplay() -> void {
        QMutexLocker locker(&d->mutex);
        d->running = false;
        quit();
    }

    auto MCUReplayer::Register(MCUReplayerObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto MCUReplayer::Deregister(MCUReplayerObserver* observer) -> void {
        d->observers.removeOne(observer);
    }

    void MCUReplayer::run() {
        int32_t runCount = 0;
        int32_t lineCount = 0;

        auto mcuControl = MCUFactory::CreateControl();

        d->LoadPackets();
        const auto packetCount = d->packets.size();
        QLOG_INFO() << "It loads " << packetCount << " packets";

        while(true) {
            QMutexLocker locker(&d->mutex);
            if(runCount >= d->count) break;
            if(d->running == false) break;

            const auto& packet = d->packets.at(lineCount);
            QList<int32_t> params;
            ResponseCode respCode{ ResponseCode::Fail };

            if(!mcuControl->SendPacket(packet, params, respCode)) {
                const auto message = QString("It fails to send packet [runCount=%1 lineCount=%2]\n%3")
                    .arg(runCount)
                    .arg(lineCount)
                    .arg(QString::fromStdString(packet.toStdString()));
                d->UpdateStopped(message);
                break;
            }

            if (respCode != +ResponseCode::Success) {
                const auto message = QString("Error code: %1").arg(params.at(0));
                d->UpdateStopped(message);
                break;
            }

            lineCount++;
            if(lineCount == packetCount) {
                lineCount = 0;
                runCount++;

                d->UpdateProgress(runCount);
            }

            if(d->delay > 0) QThread::msleep(d->delay);
        }

        QLOG_INFO() << "Finished";
        d->UpdateFinished();
    }
}