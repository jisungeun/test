#include <MCUFactory.h>

#include "Settings.h"
#include "ResponseUpdater.h"
#include "StatusCheckPanelControl.h"

namespace TC::MCUControl {
    struct StatusCheckPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    StatusCheckPanelControl::StatusCheckPanelControl() : d{new Impl} {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    StatusCheckPanelControl::~StatusCheckPanelControl() {
    }

    auto StatusCheckPanelControl::CheckStatus() -> bool {
        MCUResponse response;
        if (!d->mcuControl->CheckStatus(response)) return false;
        ResponseUpdater::GetInstance()->UpdateResponse(response);

        return true;
    }

    auto StatusCheckPanelControl::ReadAFMSensor() -> bool {
        int32_t afValue;
        if (!d->mcuControl->ReadAFMValue(afValue)) return false;
        ResponseUpdater::GetInstance()->UpdateAFSensorValue(afValue);

        return true;
    }

    auto StatusCheckPanelControl::ReadAFParameters() -> QMap<QString, int32_t> {
        return d->mcuControl->ReadSensorParameter();
    }
}
