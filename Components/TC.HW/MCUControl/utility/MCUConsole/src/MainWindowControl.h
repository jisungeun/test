#pragma once
#include <memory>

#include <QMainWindow>

namespace TC::MCUControl {
    class MainWindowControl {
    public:
        MainWindowControl();
        virtual ~MainWindowControl();

        auto LoadConfiguration()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}