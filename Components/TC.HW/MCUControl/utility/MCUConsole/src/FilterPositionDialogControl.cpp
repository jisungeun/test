#include <QFileInfo>
#include <QDir>

#include <MCUFactory.h>
#include <Settings.h>

#include "FilterPositionDialogControl.h"

namespace TC::MCUControl {
    struct FilterPositionDialogControl::Impl {
    };

    FilterPositionDialogControl::FilterPositionDialogControl() : d{new Impl} {
    }

    FilterPositionDialogControl::~FilterPositionDialogControl() {
    }

    auto FilterPositionDialogControl::GetPositions() const -> QList<int32_t> {
        QList<int32_t> positions;

        const auto configPath = Settings::GetConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) return positions;

        auto mcuControl = MCUFactory::CreateControl();
        if(!mcuControl->LoadConfig(configPath)) return positions;

        const auto config = mcuControl->GetConfig();

        return config.GetFilterPositions();
    }

    auto FilterPositionDialogControl::SavePositions(const QList<int32_t>& positions) -> void {
        const auto configPath = Settings::GetConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) return;

        auto mcuControl = MCUFactory::CreateControl();
        if(!mcuControl->LoadConfig(configPath)) return;

        auto config = mcuControl->GetConfig();
        config.SetFilterPositions(positions);

        mcuControl->SaveConfig(configPath);
    }
}
