#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class MCUDataDialog : public QDialog {
        Q_OBJECT
    public:
        MCUDataDialog(QWidget* parent = nullptr);
        ~MCUDataDialog() override;

    protected:
        void keyPressEvent(QKeyEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}