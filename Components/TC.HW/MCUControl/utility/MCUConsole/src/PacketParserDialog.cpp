#include <MCUDefines.h>
#include <MCUMotionCommandFactory.h>

#include "PacketParserDialogControl.h"
#include "PacketParserDialog.h"
#include "ui_PacketParserDialog.h"

namespace TC::MCUControl {
    struct PacketParserDialog::Impl {
        PacketParserDialogControl control;
        Ui::PacketParserDialog ui;
    };

    PacketParserDialog::PacketParserDialog(QWidget* parent)  : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.cmdParameters->setColumnCount(1);
        d->ui.cmdParameters->setHorizontalHeaderLabels({"Parameter"});
        d->ui.cmdParameters->horizontalHeader()->setStretchLastSection(true);

        d->ui.respParameters->setColumnCount(1);
        d->ui.respParameters->setHorizontalHeaderLabels({"Parameter"});
        d->ui.respParameters->horizontalHeader()->setStretchLastSection(true);

        connect(d->ui.cmdPacket, SIGNAL(textChanged(const QString&)), this, SLOT(onParseCommand(const QString&)));
        connect(d->ui.respPacket, SIGNAL(textChanged(const QString&)), this, SLOT(onParseResponse(const QString&)));
        connect(d->ui.clearCommandPacketBtn, SIGNAL(clicked()), this, SLOT(onClearCommand()));
        connect(d->ui.clearResponsePacketBtn, SIGNAL(clicked()), this, SLOT(onClearResponse()));
        connect(d->ui.statusPacket, SIGNAL(stateChanged(int)), this, SLOT(onChangeResponseType(int)));
    }

    PacketParserDialog::~PacketParserDialog() {
    }

    void PacketParserDialog::onParseCommand(const QString& line) {
        auto result = d->control.ParseCommand(line);
        if(!result) {
            d->ui.cmdCommand->clear();
            d->ui.cmdParameters->clearContents();
            d->ui.cmdParameters->setRowCount(0);
            return;
        }

        const auto addMacroCmd = (Command::_from_string(result->commdand.toLatin1()) == +Command::AddMacroToBuffer);

        d->ui.cmdCommand->setText(result->commdand);
        d->ui.cmdParameters->setRowCount(result->parameters.length());
        int32_t rowIdx{ 0 };
        if(!addMacroCmd) {
            d->ui.cmdParameters->setColumnCount(1);
            d->ui.cmdParameters->setHorizontalHeaderLabels({"Parameter"});
            for(auto param : result->parameters) {
                auto* item = new QTableWidgetItem(QString::number(param));
                d->ui.cmdParameters->setItem(rowIdx++, 0, item);
            }
        } else {
            d->ui.cmdParameters->setColumnCount(2);
            d->ui.cmdParameters->setHorizontalHeaderLabels({"Title", "Parameter"});
            const QStringList commons{"X Pos", "Y Pos", "Z Pos", "AF Mode", "Command", "Parameters"};
            const auto motionCmdType = MotionCommandType::_from_integral(result->parameters.at(4));
            const auto motionCmd = MCUMotionCommandFactory::GetInstance()->CreateCommand(motionCmdType);

            for(auto param : result->parameters) {
                if(rowIdx < 6) {
                    auto* item = new QTableWidgetItem(commons.at(rowIdx));
                    d->ui.cmdParameters->setItem(rowIdx, 0, item);
                } else {
                    auto* item = new QTableWidgetItem(motionCmd->GetParameterName(rowIdx-6));
                    d->ui.cmdParameters->setItem(rowIdx, 0, item);
                }

                if(rowIdx == 4) {
                    auto* item = new QTableWidgetItem(motionCmdType._to_string());
                    d->ui.cmdParameters->setItem(rowIdx, 1, item);
                } else {
                    auto* item = new QTableWidgetItem(QString::number(param));
                    d->ui.cmdParameters->setItem(rowIdx, 1, item);
                }
                rowIdx++;
            }
        }
    }

    void PacketParserDialog::onParseResponse(const QString& line) {
        auto result = d->control.ParseResponse(line);
        if(!result) {
            d->ui.respResponse->clear();
            d->ui.respParameters->clearContents();
            d->ui.respParameters->setRowCount(0);
            return;
        }

        const auto statusPacket = d->ui.statusPacket->isChecked();

        if(!statusPacket) {
            d->ui.respParameters->setColumnCount(1);
            d->ui.respParameters->setHorizontalHeaderLabels({"Parameter"});
            d->ui.respResponse->setText(result->commdand);
            d->ui.respParameters->setRowCount(result->parameters.length());
            int32_t rowIdx{ 0 };
            for(auto param : result->parameters) {
                auto* item = new QTableWidgetItem(QString::number(param));
                d->ui.respParameters->setItem(rowIdx++, 0, item);
            }
        } else {
            auto status = d->control.ParseStatusResponse(line);

            d->ui.respParameters->setColumnCount(2);
            d->ui.respParameters->setHorizontalHeaderLabels({"Title", "Parameter"});
            d->ui.respResponse->setText(result->commdand);
            d->ui.respParameters->setRowCount(Response::_size());
            int32_t rowIdx{ 0 };
            for(auto respType : Response::_values()) {
                const auto value = status.GetValue(respType);

                auto* item = new QTableWidgetItem(respType._to_string());
                d->ui.respParameters->setItem(rowIdx, 0, item);

                item = new QTableWidgetItem(QString::number(value));
                d->ui.respParameters->setItem(rowIdx, 1, item);

                rowIdx++;
            }
        }
    }

    void PacketParserDialog::onClearCommand() {
        d->ui.cmdPacket->clear();
    }

    void PacketParserDialog::onClearResponse() {
        d->ui.respPacket->clear();
    }

    void PacketParserDialog::onChangeResponseType(int state) {
        Q_UNUSED(state)
        onParseResponse(d->ui.respPacket->text());
    }
}
