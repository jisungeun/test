#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class PacketParserDialog : public QDialog {
        Q_OBJECT
    public:
        PacketParserDialog(QWidget* parent = nullptr);
        ~PacketParserDialog() override;

    protected slots:
        void onParseCommand(const QString& line);
        void onParseResponse(const QString& line);
        void onClearCommand();
        void onClearResponse();
        void onChangeResponseType(int state);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}