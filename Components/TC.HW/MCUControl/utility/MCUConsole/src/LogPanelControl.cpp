#include <QFile>
#include <QTextStream>
#include <MCUFactory.h>

#include "Settings.h"
#include "LogPanelControl.h"

namespace TC::MCUControl {
    struct LogPanelControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    LogPanelControl::LogPanelControl() : d{new Impl} {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    LogPanelControl::~LogPanelControl() {
    }

    auto LogPanelControl::Save(const QString& text, const QString& path) -> bool {
        QFile file(path);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return false;

        QTextStream out(&file);
        out << text;
        out.flush();

        return true;
    }

    auto LogPanelControl::ReadMCULog() -> QList<QString> {
        QString logAll;

        while (true) {
            auto log = d->mcuControl->ReadMCULog();
            if(log.isEmpty()) break;
            logAll.append(log);
        }

        QList<QString> output;

        auto toks = logAll.split("0A");
        for(auto tok : toks) {
            QString str;
            const auto len = tok.length();
            for(int32_t idx=0; idx<(len-1); idx+=2) {
                const auto sub = tok.mid(idx, 2);
                bool ok;
                auto hex = sub.toInt(&ok, 16);
                str.push_back(hex);
            }
            output.push_back(str);
        }

        return output;
    }
}
