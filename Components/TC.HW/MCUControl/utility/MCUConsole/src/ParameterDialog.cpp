#include "ui_ParameterDialog.h"
#include "ParameterDialog.h"

namespace TC::MCUControl {
    struct ParameterDialog::Impl {
        Ui::ParameterDialog ui;
    };

    ParameterDialog::ParameterDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.table->setColumnCount(2);
        d->ui.table->setHorizontalHeaderLabels({"Parameter", "Value"});
        d->ui.table->horizontalHeader()->setStretchLastSection(true);
    }

    ParameterDialog::~ParameterDialog() {
    }

    auto ParameterDialog::SetParameter(const QMap<QString, int32_t>& params) -> void {
        d->ui.table->setRowCount(params.count());

        int32_t rowIdx = 0;
        for(auto itr=params.begin(); itr!=params.end(); ++itr, rowIdx++) {
            auto* item = new QTableWidgetItem(itr.key());
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            d->ui.table->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(QString::number(itr.value()));
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            d->ui.table->setItem(rowIdx, 1, item);
        }
    }
}