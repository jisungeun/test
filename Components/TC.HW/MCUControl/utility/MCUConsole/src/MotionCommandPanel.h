#pragma once
#include <memory>

#include <QDialog>
#include <QTableWidgetItem>
#include <QAbstractButton>

namespace TC::MCUControl {
    class MotionCommandPanel : public QDialog {
        Q_OBJECT
    public:
        MotionCommandPanel(QWidget* parent = nullptr);
        virtual ~MotionCommandPanel();

    protected slots:
        void onDialogButtonClicked(QAbstractButton* button);
        void onAddCommand();
        void onDeleteCommand();
        void onCommandSelected();
        void onCommandChanged(QTableWidgetItem* item);
        void onParameterChanged(QTableWidgetItem* item);

    protected:
        void done(int r) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}