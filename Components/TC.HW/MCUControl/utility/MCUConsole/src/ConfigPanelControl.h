#pragma once
#include <memory>

#include <MCUConfig.h>

namespace TC::MCUControl {
    class ConfigPanelControl {
    public:
        ConfigPanelControl();
        virtual ~ConfigPanelControl();

        auto LoadConfig()->bool;
        auto GetConfig()->MCUConfig&;
        auto SaveConfig(const MCUConfig& config)->bool;

        auto FindPorts()->QList<int32_t>;
        auto FindBaudRates()->QList<int32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}