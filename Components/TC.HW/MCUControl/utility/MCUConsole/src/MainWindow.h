#pragma once
#include <memory>

#include <QMainWindow>

namespace TC::MCUControl {
    class MainWindow : public QMainWindow {
        Q_OBJECT

    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

    protected slots:
        void onQuit();
        void onConfigure();
        void onEditMotionCommands();
        void onEditMacros();
        void onShowParser();
        void onUpdateFirmwareVersion(const QString& ver);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}