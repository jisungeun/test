#include <QFile>

#include <MCUFactory.h>

#include "Settings.h"
#include "MainWindowControl.h"

namespace TC::MCUControl {
    struct MainWindowControl::Impl {
        std::shared_ptr<IMCUControl> mcuControl;
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
        d->mcuControl = MCUFactory::CreateControl(Model::HTX, Settings::GetSimulation());
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::LoadConfiguration() -> bool {
        const auto configPath = Settings::GetConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) return false;

        return d->mcuControl->LoadConfig(configPath);
    }
}
