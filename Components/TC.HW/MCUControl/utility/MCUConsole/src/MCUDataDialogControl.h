#pragma once
#include <memory>
#include <QString>
#include <QPair>
#include <QList>

namespace TC::MCUControl {
    class MCUDataDialogControl {
    public:
        using Field = QPair<int32_t, QString>;

    public:
        MCUDataDialogControl();
        ~MCUDataDialogControl();

        auto LoadFile(const QString& path, QList<Field>& data)->bool;
        auto SaveFile(const QString& path, const QList<Field>& data)->bool;

        auto Read(QList<int32_t>& data)->bool;
        auto Write(const QList<int32_t>& data)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}