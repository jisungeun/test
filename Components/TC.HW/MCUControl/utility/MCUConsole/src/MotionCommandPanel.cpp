#include <QDialogButtonBox>
#include <QMessageBox>
#include <QItemDelegate>
#include <QLineEdit>

#include "Settings.h"
#include "AddMotionCommandDialog.h"
#include "MotionCommandPanelControl.h"
#include "MotionCommandPanel.h"
#include "ui_MotionCommandPanel.h"

namespace TC::MCUControl {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                              const QModelIndex& index) const override {
            QLineEdit* lineEdit = new QLineEdit(parent);
            QIntValidator* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };

    struct MotionCommandPanel::Impl {
        Ui::MotionCommandPanel ui;
        MotionCommandPanelControl control;

        auto Reset()->void;
        auto UpdateGUI()->void;
        auto InsertCommand(IMCUMotionCommand::Pointer& command)->void;
        auto InsertCommandGUI(IMCUMotionCommand::Pointer& command)->void;
        auto UpdateParameterGUI(IMCUMotionCommand::Pointer& command)->void;
        auto GetParameterValue(uint32_t index)->int32_t;
        auto DeleteCommand(uint32_t index)->void;
    };

    auto MotionCommandPanel::Impl::Reset() -> void {
        control.Load(Settings::GetMotionCommandsPath());
        UpdateGUI();
    }

    auto MotionCommandPanel::Impl::UpdateGUI() -> void {
        ui.commandsTable->clearContents();
        ui.parameterTable->clearContents();

        ui.commandsTable->blockSignals(true);
        ui.commandsTable->setRowCount(0);

        auto commands = control.GetCommands();
        for (auto command : commands) {
            InsertCommandGUI(command);
        }

        ui.commandsTable->blockSignals(false);
        ui.commandsTable->selectRow(0);
    }

    auto MotionCommandPanel::Impl::InsertCommand(IMCUMotionCommand::Pointer& command) -> void {
        control.InsertCommand(command);
        InsertCommandGUI(command);
    }

    auto MotionCommandPanel::Impl::InsertCommandGUI(IMCUMotionCommand::Pointer & command) -> void {
        auto rows = ui.commandsTable->rowCount();
        ui.commandsTable->setRowCount(rows + 1);

        auto itemID = new QTableWidgetItem(QString::number(command->GetCommandID()));
        itemID->setFlags(itemID->flags() & ~Qt::ItemIsEditable);
        ui.commandsTable->setItem(rows, 0, itemID);

        auto itemTitle = new QTableWidgetItem(command->GetCommandTitle());
        itemTitle->setFlags(itemTitle->flags() | Qt::ItemIsEditable);
        ui.commandsTable->setItem(rows, 1, itemTitle);

        ui.commandsTable->selectRow(rows);
    }

    auto MotionCommandPanel::Impl::UpdateParameterGUI(IMCUMotionCommand::Pointer& command) -> void {
        ui.parameterTable->clearContents();

        const auto count = command->GetParameterCount();
        ui.parameterTable->setRowCount(count);

        for (uint32_t idx = 0; idx < count; idx++) {
            const auto name = command->GetParameterName(idx);
            const auto unit = command->GetParameterUnit(idx);
            const auto value = command->GetParameter(idx);

            auto itemID = new QTableWidgetItem(QString::number(idx + 1));
            itemID->setFlags(itemID->flags() & ~Qt::ItemIsEditable);
            ui.parameterTable->setItem(idx, 0, itemID);

            auto itemName = new QTableWidgetItem(name);
            itemName->setFlags(itemName->flags() & ~Qt::ItemIsEditable);
            ui.parameterTable->setItem(idx, 1, itemName);

            auto itemValue = new QTableWidgetItem(QString::number(value));
            itemValue->setFlags(itemValue->flags() | Qt::ItemIsEditable);
            itemValue->setTextAlignment(Qt::AlignRight);
            ui.parameterTable->setItem(idx, 2, itemValue);

            auto itemUnit = new QTableWidgetItem(unit);
            itemUnit->setFlags(itemUnit->flags() & ~Qt::ItemIsEditable);
            ui.parameterTable->setItem(idx, 3, itemUnit);
        }
    }

    auto MotionCommandPanel::Impl::GetParameterValue(uint32_t index) -> int32_t {
        return ui.parameterTable->item(index, 2)->text().toInt();
    }

    auto MotionCommandPanel::Impl::DeleteCommand(uint32_t index) -> void {
        const auto rows = ui.commandsTable->rowCount();

        const auto cmdID = ui.commandsTable->item(index, 0)->text().toInt();
        control.DeleteCommand(cmdID);
        ui.commandsTable->clearSelection();
        UpdateGUI();
    }

    MotionCommandPanel::MotionCommandPanel(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        setWindowTitle("Motion Commands");
        for (auto button : d->ui.buttonBox->buttons()) {
            button->setFixedWidth(70);
        }

        d->ui.commandsTable->setColumnCount(2);
        d->ui.commandsTable->setHorizontalHeaderLabels(QStringList() << "ID" << "Title");
        d->ui.commandsTable->setColumnWidth(0, 20);
        d->ui.commandsTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.commandsTable->verticalHeader()->hide();
        d->ui.commandsTable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.commandsTable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
        
        d->ui.parameterTable->setColumnCount(4);
        d->ui.parameterTable->setHorizontalHeaderLabels(QStringList() << "#" << "Title" << "Value" << "Unit");
        d->ui.parameterTable->setColumnWidth(0, 20);
        d->ui.parameterTable->setColumnWidth(1, 150);
        d->ui.parameterTable->setColumnWidth(2, 100);
        d->ui.parameterTable->verticalHeader()->hide();
        d->ui.parameterTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.parameterTable->setItemDelegateForColumn(2, new IntegerDelegate());
        d->ui.parameterTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        connect(d->ui.buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(onDialogButtonClicked(QAbstractButton*)));
        connect(d->ui.addBtn, SIGNAL(clicked()), this, SLOT(onAddCommand()));
        connect(d->ui.deleteBtn, SIGNAL(clicked()), this, SLOT(onDeleteCommand()));
        connect(d->ui.commandsTable, SIGNAL(itemSelectionChanged()), this, SLOT(onCommandSelected()));
        connect(d->ui.commandsTable, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(onCommandChanged(QTableWidgetItem*)));

        d->Reset();
    }

    MotionCommandPanel::~MotionCommandPanel() {
    }

    void MotionCommandPanel::onDialogButtonClicked(QAbstractButton* button) {
        auto type = d->ui.buttonBox->standardButton(button);
        if (type == QDialogButtonBox::StandardButton::Reset) {
            d->Reset();
        }
    }

    void MotionCommandPanel::onAddCommand() {
        auto dialog = AddMotionCommandDialog(this);
        if (dialog.exec() != QDialog::Accepted) return;

        auto type = dialog.GetType();
        auto title = dialog.GetTitle();

        if (title.isEmpty()) {
            QMessageBox::warning(this, "Motion Command", tr("You have to specify the title of motion command"));
            return;
        }

        disconnect(d->ui.commandsTable, SIGNAL(itemChanged(QTableWidgetItem*)), nullptr, nullptr);
        auto cmd = d->control.CreateCommand(type, title);
        d->InsertCommand(cmd);
        connect(d->ui.commandsTable, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(onCommandChanged(QTableWidgetItem*)));
    }

    void MotionCommandPanel::onDeleteCommand() {
        const auto row = d->ui.commandsTable->currentRow();
        if (row < 0) return;
        d->DeleteCommand(row);
    }

    void MotionCommandPanel::onCommandSelected() {
        const auto row = d->ui.commandsTable->currentRow();
        const auto cmdID = d->ui.commandsTable->item(row, 0)->text().toInt();
        auto cmd = d->control.GetCommand(cmdID);
        if (cmd.get() == nullptr) return;

        disconnect(d->ui.parameterTable, nullptr, nullptr, nullptr);
        d->UpdateParameterGUI(cmd);
        connect(d->ui.parameterTable, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(onParameterChanged(QTableWidgetItem*)));
    }

    void MotionCommandPanel::onCommandChanged(QTableWidgetItem* item) {
        const auto row = d->ui.commandsTable->currentRow();
        const auto cmdID = d->ui.commandsTable->item(row, 0)->text().toInt();
        const auto title = d->ui.commandsTable->item(row, 1)->text();

        d->control.UpdateCommandTitle(cmdID, title);
    }

    void MotionCommandPanel::onParameterChanged(QTableWidgetItem* item) {
        const auto row = d->ui.commandsTable->currentRow();
        const auto cmdID = d->ui.commandsTable->item(row, 0)->text().toInt();
        auto cmd = d->control.GetCommand(cmdID);

        const auto paramCount = cmd->GetParameterCount();
        for (uint32_t idx = 0; idx < paramCount; idx++) {
            cmd->SetParameter(idx, d->GetParameterValue(idx));
        }
    }

    void MotionCommandPanel::done(int r) {
        auto accepted = (r == QDialog::DialogCode::Accepted);
        if (!accepted) {
            QDialog::done(r);
            return;
        }

        if (!d->control.Save(Settings::GetMotionCommandsPath())) {
            QMessageBox::warning(this, "Motion Commands", tr("It fails to save motion commands to file"));
        }

        if (!d->control.Upload()) {
            QMessageBox::warning(this, "Motion Commands", tr("It fails to upload motion commands to MCU"));
        }

        QDialog::done(r);
    }
}
