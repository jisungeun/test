#pragma once
#include <memory>

namespace TC::MCUControl {
    struct Point {
        int32_t x;
        int32_t y;
        int32_t z;
    };
}