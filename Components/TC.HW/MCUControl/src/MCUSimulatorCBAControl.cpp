#define LOGGER_TAG "[MCU Simul]"
#pragma warning(disable:4996)
#include <QMessageBox>

#include <TCLogger.h>

#include "MCUDefines.h"
#include "MCUResponse.h"
#include "MCUPacketParser.h"
#include "MCUPacketGenerator.h"
#include "AsciiConverter.h"

#include "MCUFactory.h"
#include "MCUCommSimul.h"
#include "MCUSimulatorCBAControl.h"

namespace TC::MCUControl {
    struct MCUSimulatorCBAControl::Impl {
        MCUResponse response;
        int32_t initProgress{ 0 };
        int32_t safeZPos{ 0 };
        int32_t focusPos{ 0 };
        int32_t scanReadyPosX{ 0 };
        int32_t scanReadyPosY{ 0 };
        int32_t macroProgress{ -1 };
        int32_t triggerCount{ 0 };
        bool sendLog{ true };

        auto GenerateResponse(ResponseCode respCode, QList<int32_t> params)->QByteArray;
        auto GenerateResponse(ResponseCode respCode, const QByteArray& params)->QByteArray;
        auto BuildBytes(const QList<int32_t>& values)->int32_t;
        auto BuildParam(const QByteArray& text)->QList<int32_t>;
    };

    auto MCUSimulatorCBAControl::Impl::GenerateResponse(ResponseCode respCode, QList<int32_t> params) -> QByteArray {
        return MCUPacketGenerator::Generate(respCode, params);
    }

    auto MCUSimulatorCBAControl::Impl::GenerateResponse(ResponseCode respCode, const QByteArray& params) -> QByteArray {
        return MCUPacketGenerator::Generate(respCode, params);
    }

    auto MCUSimulatorCBAControl::Impl::BuildBytes(const QList<int32_t>& values) -> int32_t {
        int32_t packed = 0;

        for(auto idx=0; idx<values.length(); idx++) {
            auto value = (values.at(idx)>0)?1:0;
            packed += (value << idx);
        }

        return packed;
    }

    auto MCUSimulatorCBAControl::Impl::BuildParam(const QByteArray& text) -> QList<int32_t> {
        QList<int32_t> outInt;

        const auto textLen = text.length();
        for(int idx=0; idx<textLen; idx+=4) {
            const auto extLen = std::min<int32_t>(4, textLen-idx);
            QByteArray sub = text.mid(idx, extLen);
            for(int i=extLen; i<4; i++) {
                sub.push_back(' ');
            }

            int32_t packed = 0;
            for(int i=0; i<4; i++) {
                auto byte = static_cast<int32_t>(sub.at(3-i));
                packed += (byte << (i*8));
            }

            outInt.push_back(packed);
        }

        return outInt;
    }

    MCUSimulatorCBAControl::MCUSimulatorCBAControl() : d{ new Impl } {
    }

    MCUSimulatorCBAControl::~MCUSimulatorCBAControl() {
    }

    auto MCUSimulatorCBAControl::Process(const QByteArray& data) -> QByteArray {
        MCUPacketParser parser;
        Command cmd{ Command::CheckStatus };
        QList<int32_t> params;

        if (!parser.Parse(data, cmd, params)) {
            QMessageBox::warning(0, "Simulator", QString("Invalid packet - %1")
                                 .arg(QString::fromStdString(data.toStdString())));
            return QByteArray();
        }

        ResponseCode respCode = ResponseCode::Success;
        QList<int32_t> respParam;
        QByteArray statusResp;

        switch (cmd) {
        case Command::StartInitialization:
            respParam.push_back(0);
            d->initProgress = 0;
            break;
        case Command::CheckInitializationProgress:
            respParam.push_back(0);
            respParam.push_back(d->initProgress);
            d->initProgress += 10;
            break;
        case Command::SetSafeZPosition:
            d->safeZPos = params.at(0);
            respParam.push_back(0);
            break;
        case Command::SetFocusPosition:
            d->focusPos = params.at(0);
            respParam.push_back(0);
            break;
        case Command::SetScanReadyPosition:
            d->scanReadyPosX = params.at(0);
            d->scanReadyPosY = params.at(1);
            respParam.push_back(0);
            break;
        case Command::SetCommand:
            respParam.push_back(0);
            break;
        case Command::AddMacroPoint:
            respParam.push_back(0);
            break;
        case Command::RunMacro:
            d->macroProgress = 0;
            respParam.push_back(0);
            break;
        case Command::StopMacro:
            d->macroProgress = -1;
            respParam.push_back(0);
            break;
        case Command::CheckMacroStatus:
            respParam.push_back(0);
            if (d->macroProgress == -1) {
                respParam.push_back(0);
                respParam.push_back(0);
            } else {
                respParam.push_back(1);
                d->macroProgress += 10;
                respParam.push_back(d->macroProgress);
                if (d->macroProgress == 0) {
                    d->macroProgress = -1;
                }
            }
            break;
        case Command::Move:
            respParam.push_back(0);
            break;
        case Command::MoveToLoadingReadyPosition:
            respParam.push_back(0);
            break;
        case Command::MoveToReadyPosition:
            respParam.push_back(0);
            break;
        case Command::GenerateTrigger:
            respParam.push_back(0);
            break;
        case Command::SetDigitalOutput:
            respParam.push_back(0);
            break;
        case Command::ReadDigitalInput:
            respParam.push_back(0);
            break;
        case Command::MoveXY:
            respParam.push_back(0);
            break;
        case Command::LoadCartridge:
            respParam.push_back(0);
            break;
        case Command::UnloadCartridge:
            respParam.push_back(0);
            break;
        case Command::FinishCalibration:
            respParam.push_back(0);
            break;
        case Command::StartTest:
            respParam.push_back(0);
            break;
        case Command::FinishTest:
            respParam.push_back(0);
            break;
        case Command::PowerOff:
            respParam.push_back(0);
            break;
        case Command::Recover:
            respParam.push_back(0);
            break;
        case Command::ReadDLPCVersion:
            respParam.push_back(0);
            respParam.push_back(100);
            respParam.push_back(20);
            break;
        case Command::ReadTriggerCount:
            respParam.push_back(0);
            respParam.push_back(d->triggerCount++);
            break;
        case Command::ResetTriggerCount:
            d->triggerCount = 0;
            respParam.push_back(0);
            break;
        case Command::CheckMovingStatus:
            respParam.push_back(0);
            respParam.push_back(GetAxisMoveState(params.at(0)));
            break;
        case Command::CheckStatus:
            statusResp = GetStatus();
            break;
        case Command::GetAxisPosition:
            respParam.push_back(0);
            respParam.push_back(GetAxisPosition(params.at(0)));
            break;
        case Command::ReadTemperature:
            respParam.push_back(0);
            respParam.push_back(GetTemperature(params.at(0)));
            break;
        case Command::ReadAcceleration:
            respParam.push_back(0);
            respParam.push_back(GetAcceleration(0));
            respParam.push_back(GetAcceleration(1));
            respParam.push_back(GetAcceleration(2));
            break;
        case Command::ReadCartridgeSensor:
            respParam.push_back(0);
            respParam.push_back(GetCartridgeSensor(0));
            respParam.push_back(GetCartridgeSensor(1));
            break;
        case Command::ReadFirmwareVersion:
            respParam.push_back(0);
            respParam.append(d->BuildParam(QByteArray("CBC_D20210803")));
            break;
        case Command::ReadLog:
            respParam.push_back(0);
            if(d->sendLog) {
                respParam.append(d->BuildParam([]()->QByteArray {
                    QByteArray log;

                    for(int idx=0; idx<10; idx++) {
                        log.append(QString("%1 | test log").arg(idx));
                        log.append('\n');
                    }

                    return log.trimmed();
                }()));
            }
            d->sendLog = !d->sendLog;
            break;
        default:
            respParam.push_back(0);
            break;
        }

        //For status response....
        if (respParam.isEmpty()) {
            return d->GenerateResponse(respCode, statusResp);
        }

        return d->GenerateResponse(respCode, respParam);
    }

    auto MCUSimulatorCBAControl::GetStateList() const -> QStringList {
        QStringList states;

        for (auto state : MCUState::_values()) {
            states.push_back(state._to_string());
        }

        return states;
    }

    auto MCUSimulatorCBAControl::ChangeState(int index) -> void {
        d->response.SetValue(Response::StateMachineId, index);
    }

    auto MCUSimulatorCBAControl::ChangeAxisStartState(Axis axis, bool bChecked) -> void {
        auto resp = [=]()->Response {
            Response code = Response::AxisXStart;
            switch (axis) {
            case Axis::X:
                code = Response::AxisXStart;
                break;
            case Axis::Y:
                code = Response::AxisYStart;
                break;
            case Axis::Z:
                code = Response::AxisZStart;
                break;
            case Axis::L:
                code = Response::AxisLStart;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), (bChecked) ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeAxisEndState(Axis axis, bool bChecked) -> void {
        auto resp = [=]()->Response {
            Response code = Response::AxisXEnd;
            switch (axis) {
            case Axis::X:
                code = Response::AxisXEnd;
                break;
            case Axis::Y:
                code = Response::AxisYEnd;
                break;
            case Axis::Z:
                code = Response::AxisZEnd;
                break;
            case Axis::L:
                code = Response::AxisLEnd;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), (bChecked) ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeAxisMoveState(Axis axis, bool bChecked) -> void {
        auto resp = [=]()->Response {
            Response code = Response::AxisXMoving;
            switch (axis) {
            case Axis::X:
                code = Response::AxisXMoving;
                break;
            case Axis::Y:
                code = Response::AxisYMoving;
                break;
            case Axis::Z:
                code = Response::AxisZMoving;
                break;
            case Axis::L:
                code = Response::AxisLMoving;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), (bChecked) ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeAxisPosition(Axis axis, int position) -> void {
        auto resp = [=]()->Response {
            Response code = Response::AxisXPosition;
            switch (axis) {
            case Axis::X:
                code = Response::AxisXPosition;
                break;
            case Axis::Y:
                code = Response::AxisYPosition;
                break;
            case Axis::Z:
                code = Response::AxisZPosition;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), position);
    }

    auto MCUSimulatorCBAControl::ChangeShutterState(bool open) -> void {
        d->response.SetValue(Response::SensorShutterOpen, (open) ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeDoorState(bool open) -> void {
        d->response.SetValue(Response::SensorDoorOpen, (open) ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeLEDState(int index, bool on) -> void {
        auto resp = [=]()->Response {
            Response code = Response::SensorLED0On;
            switch (index) {
            case 0:
                code = Response::SensorLED0On;
                break;
            case 1:
                code = Response::SensorLED1On;
                break;
            case 2:
                code = Response::SensorLED2On;
                break;
            case 3:
                code = Response::SensorLED3On;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), on ? 1 : 0);
    }

    auto MCUSimulatorCBAControl::ChangeAngle(int index, double value) -> void {
        auto resp = [=]()->Response {
            Response code = Response::AngleX;
            switch (index) {
            case 0:
                code = Response::AngleX;
                break;
            case 1:
                code = Response::AngleY;
                break;
            case 2:
                code = Response::AngleZ;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), static_cast<int>(value * 10), 0.1f);
    }

    auto MCUSimulatorCBAControl::ChangeTemperature(int index, double value) -> void {
        auto resp = [=]()->Response {
            Response code = Response::Temperature0;
            switch (index) {
            case 0:
                code = Response::Temperature0;
                break;
            case 1:
                code = Response::Temperature1;
                break;
            case 2:
                code = Response::Temperature2;
                break;
            case 3:
                code = Response::Temperature3;
                break;
            }
            return code;
        };

        d->response.SetValue(resp(), static_cast<int>(value * 10), 0.1f);
    }

    auto MCUSimulatorCBAControl::GetMacroStep() -> int32_t {
        return d->macroProgress;
    }

    auto MCUSimulatorCBAControl::GetAxisMoveState(int axisID) -> int32_t {
        auto resp = [=]()->Response {
            Response code = Response::AxisXMoving;
            switch (axisID) {
            case 0:
                code = Response::AxisXMoving;
                break;
            case 1:
                code = Response::AxisYMoving;
                break;
            case 2:
                code = Response::AxisZMoving;
                break;
            case 3:
                code = Response::AxisLMoving;
                break;
            }
            return code;
        };

        return d->response.GetValue(resp());
    }

    auto MCUSimulatorCBAControl::GetAxisPosition(int axisID) -> int32_t {
        auto resp = [=]()->Response {
            Response code = Response::AxisXPosition;
            switch (axisID) {
            case 0:
                code = Response::AxisXPosition;
                break;
            case 1:
                code = Response::AxisYPosition;
                break;
            case 2:
                code = Response::AxisZPosition;
                break;
            }
            return code;
        };

        return d->response.GetAngle(resp());
    }

    auto MCUSimulatorCBAControl::GetTemperature(int sensorID) -> int32_t {
        auto resp = [=]()->Response {
            Response code = Response::Temperature0;
            switch (sensorID) {
            case 0:
                code = Response::Temperature0;
                break;
            case 1:
                code = Response::Temperature1;
                break;
            case 2:
                code = Response::Temperature2;
                break;
            case 3:
                code = Response::Temperature3;
                break;
            }
            return code;
        };

        return d->response.GetValue(resp());
    }

    auto MCUSimulatorCBAControl::GetAcceleration(int sensorID) -> int32_t {
        auto resp = [=]()->Response {
            Response code = Response::AngleX;
            switch (sensorID) {
            case 0:
                code = Response::AngleX;
                break;
            case 1:
                code = Response::AngleY;
                break;
            case 2:
                code = Response::AngleZ;
                break;
            }
            return code;
        };

        return d->response.GetValue(resp());
    }

    auto MCUSimulatorCBAControl::GetCartridgeSensor(int /*sensorID*/) -> int32_t {
        return 0;
    }

    auto MCUSimulatorCBAControl::GetStatus() -> QByteArray {
        QByteArray resp;

        resp.push_back(AsciiConverter::toAscii(0, 8)); //Error Code

        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::StateMachineId), 4)); //State machine
        resp.push_back(AsciiConverter::toAscii(std::max<int32_t>(0, d->macroProgress), 4)); //Reserved

        auto axisSensorVal0 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::AxisXEnd)
                                            << d->response.GetValue(Response::AxisXStart)
                                            << d->response.GetValue(Response::AxisXMoving));
        auto axisSensorVal1 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::AxisYEnd)
                                            << d->response.GetValue(Response::AxisYStart)
                                            << d->response.GetValue(Response::AxisYMoving));
        auto axisSensorVal2 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::AxisZEnd)
                                            << d->response.GetValue(Response::AxisZStart)
                                            << d->response.GetValue(Response::AxisZMoving));
        auto axisSensorVal3 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::AxisLEnd)
                                            << d->response.GetValue(Response::AxisLStart)
                                            << d->response.GetValue(Response::AxisLMoving));
        resp.push_back(AsciiConverter::toAscii(axisSensorVal0, 2)); //Axis Sensor
        resp.push_back(AsciiConverter::toAscii(axisSensorVal1, 2));
        resp.push_back(AsciiConverter::toAscii(axisSensorVal2, 2));
        resp.push_back(AsciiConverter::toAscii(axisSensorVal3, 2));

        auto sensorVal0 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::SensorDoorOpen)
                                        << d->response.GetValue(Response::SensorShutterOpen));
        auto sensorVal2 = d->BuildBytes(QList<int32_t>() << d->response.GetValue(Response::SensorLED0On)
                                        << d->response.GetValue(Response::SensorLED1On)
                                        << d->response.GetValue(Response::SensorLED2On)
                                        << d->response.GetValue(Response::SensorLED3On));
        resp.push_back(AsciiConverter::toAscii(sensorVal0, 2)); //Sensor
        resp.push_back(AsciiConverter::toAscii(0, 2));
        resp.push_back(AsciiConverter::toAscii(sensorVal2, 2));
        resp.push_back(AsciiConverter::toAscii(0, 2));

        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AngleX), 2)); //Angle
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AngleY), 2));
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AngleZ), 2));
        resp.push_back(AsciiConverter::toAscii(0, 2));

        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::Temperature0), 4)); //Temperature
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::Temperature1), 4));
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::Temperature2), 4));
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::Temperature3), 4));

        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AxisXPosition), 8));  //Position
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AxisYPosition), 8));
        resp.push_back(AsciiConverter::toAscii(d->response.GetValue(Response::AxisZPosition), 8));
        resp.push_back(AsciiConverter::toAscii(0, 8));
        resp.push_back(AsciiConverter::toAscii(0, 8));
        resp.push_back(AsciiConverter::toAscii(0, 8));

        return resp;
    }
}
