#include <QMap>
#include "MCMDMetaChangePosition.h"
#include "MCMDChangePosition.h"

namespace TC::MCUControl {
    MCMDChangePosition::MCMDChangePosition() {
    }

    MCMDChangePosition::~MCMDChangePosition() {
    }

    auto MCMDChangePosition::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::ChangePosition;
    }

    auto MCMDChangePosition::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaChangePosition::GetInstance();
    }

    auto MCMDChangePosition::CreateInstance() -> IMCUMotionCommand::Pointer {
        static Pointer theInstance{ new MCMDChangePosition() };
        return theInstance;
    }
}
