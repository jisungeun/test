#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaPatternSequence : public IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<MCMDMetaPatternSequence> Pointer;

    protected:
        MCMDMetaPatternSequence();

    public:
        ~MCMDMetaPatternSequence();

        static auto GetInstance()->Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}