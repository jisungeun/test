#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class MCUSimulatorHTX : public QDialog {
        Q_OBJECT
    public:
        MCUSimulatorHTX(QWidget* parent = nullptr);
        virtual ~MCUSimulatorHTX();

    protected slots:
        void onReceived(const QByteArray& dataArray);
        void onStateChanged(int index);
        void onXStartChanged(int state);
        void onYStartChanged(int state);
        void onZStartChanged(int state);
        void onUStartChanged(int state);
        void onVStartChanged(int state);
        void onWStartChanged(int state);
        void onXEndChanged(int state);
        void onYEndChanged(int state);
        void onZEndChanged(int state);
        void onUEndChanged(int state);
        void onVEndChanged(int state);
        void onWEndChanged(int state);
        void onXMovingChanged(int state);
        void onYMovingChanged(int state);
        void onZMovingChanged(int state);
        void onUMovingChanged(int state);
        void onVMovingChanged(int state);
        void onWMovingChanged(int state);
        void onXPositionChanged(int value);
        void onYPositionChanged(int value);
        void onZPositionChanged(int value);
        void onUPositionChanged(int value);
        void onVPositionChanged(int value);
        void onWPositionChanged(int value);
        void onLED1Changed(int state);
        void onLED2Changed(int state);
        void onLED3Changed(int state);
        void onLED4Changed(int state);
        void onXAngleChanged(double value);
        void onYAngleChanged(double value);
        void onZAngleChanged(double value);
        void onTemp1Changed(double value);
        void onTemp2Changed(double value);
        void onTemp3Changed(double value);
        void onTemp4Changed(double value);
        void onAfOnChanged(int state);
        void onAfErrorChanged(int state);
        void onStreamingBufferSizeChanged(int value);
        void onStreamingIndex(int value);
        void onStreamingErrorChanged(int state);

    signals:
        void sigSend(const QByteArray& data);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}