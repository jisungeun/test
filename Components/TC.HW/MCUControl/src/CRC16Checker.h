#pragma once
#include <memory>
#include <QByteArray>

namespace TC::MCUControl {
    class CRC16Checker {
    public:
        typedef std::shared_ptr<CRC16Checker> Pointer;

    protected:
        CRC16Checker();

    public:
        virtual ~CRC16Checker();

        static auto GetInstance()->Pointer;

        auto Check(const QByteArray& input) const ->uint16_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}