#pragma once
#include <memory>
#include <QCoreApplication>
#include <QString>

#include "MCUError.h"
#include "MCUConfig.h"

namespace TC::MCUControl {
    class MCUConfigIO {
        Q_DECLARE_TR_FUNCTIONS(MCUConfigIO)

    public:
        MCUConfigIO();
        virtual ~MCUConfigIO();

        auto Save(const MCUConfig& config, const QString& path)->bool;
        auto Load(MCUConfig& config, const QString& path)->bool;

        auto GetLastError() const->MCUError;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}