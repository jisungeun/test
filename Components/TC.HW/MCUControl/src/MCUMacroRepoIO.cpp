#include <QFile>
#include <QSettings>

#include "MCUMacroRepository.h"
#include "MCUMacroIO.h"
#include "MCUMacroRepoIO.h"

#define GROUP_MACRO_COMMANDS    "Macro_Commands"

namespace TC::MCUControl {
    struct MCUMacroRepoIO::Impl {
        MCUError error;
    };

    MCUMacroRepoIO::MCUMacroRepoIO() : d{ new Impl } {
    }

    MCUMacroRepoIO::~MCUMacroRepoIO() {
    }

    auto MCUMacroRepoIO::Save(const QString& path) -> bool {
        auto repo = MCUMacroRepository::GetInstance();
        const auto counts = repo->GetCounts();

        QSettings qs(path, QSettings::IniFormat);
        MCUMacroIO io;

        qs.clear();
        qs.beginWriteArray(GROUP_MACRO_COMMANDS);
        for (uint32_t idx = 0; idx < counts; idx++) {
            qs.setArrayIndex(idx);

            auto macroGroup = repo->GetMacroGroupByIdx(idx);
            if (!io.Save(macroGroup, qs)) {
                auto error = io.GetLastError();
                if (error != Error::MCU_NOERROR) d->error = error;
                else {
                    d->error = MCUError(Error::MCU_FAILED_SAVING_MACROS, tr("Saving path: %1").arg(path));
                }
                return false;
            }
        }
        qs.endArray();

        return true;
    }

    auto MCUMacroRepoIO::Load(const QString& path) -> bool {
        if (!QFile::exists(path)) {
            d->error = MCUError(Error::MCU_NO_MACROS_FILE, tr("File path: %1").arg(path));
            return false;
        }

        auto repo = MCUMacroRepository::GetInstance();

        QList<MCUMacroGroup::Pointer> list;

        QSettings qs(path, QSettings::IniFormat);
        MCUMacroIO io;

        const auto counts = qs.beginReadArray(GROUP_MACRO_COMMANDS);
        for (int idx = 0; idx < counts; idx++) {
            qs.setArrayIndex(idx);

            MCUMacroGroup::Pointer macroGroup;
            if (!io.Load(macroGroup, qs)) {
                auto error = io.GetLastError();
                if (error == Error::MCU_NOERROR) d->error = error;
                else {
                    d->error = MCUError(Error::MCU_FAILED_LOADING_MACROS, tr("File path: %1").arg(path));
                }
                return false;
            }

            list.push_back(macroGroup);
        }
        qs.endArray();

        repo->Clear();

        for (auto macroGroup : list) {
            repo->InsertMacroGroup(macroGroup);
        }

        return true;
    }

    auto MCUMacroRepoIO::GetLastError() const -> MCUError {
        return d->error;
    }
}
