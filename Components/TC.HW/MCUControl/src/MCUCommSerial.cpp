#include <QSerialPort>
#include "MCUCommSerial.h"

namespace TC::MCUControl {
    struct MCUCommSerial::Impl {
        std::shared_ptr<QSerialPort> port;
        bool opened{ false };
        MCUCommParam param;
    };

    MCUCommSerial::MCUCommSerial(QObject* parent) : MCUComm(parent), d{ new Impl } {
    }

    MCUCommSerial::~MCUCommSerial() {
    }

    auto MCUCommSerial::SetParameter(MCUCommParam& param) -> void {
        d->param = param;
    }

    auto MCUCommSerial::OpenPort() -> bool {
        d->port.reset(new QSerialPort());
        d->port->setPortName(QString("COM%1").arg(d->param.GetAsInt("Port")));
        d->port->setBaudRate(d->param.GetAsInt("Baudrate"));
        d->port->setParity(QSerialPort::Parity::NoParity);
        d->port->setStopBits(QSerialPort::StopBits::OneStop);
        d->port->setDataBits(QSerialPort::DataBits::Data8);
        d->port->setReadBufferSize(1000);

        if (!d->port->open(QIODevice::ReadWrite)) {
            return false;
        }

        d->opened = true;
        return true;
    }

    auto MCUCommSerial::IsOpened() const -> bool {
        return d->opened;
    }

    auto MCUCommSerial::write(const QByteArray& data) -> qint64 {
        if (!d->opened) return -1;
        return d->port->write(data);
    }

    auto MCUCommSerial::waitForBytesWritten(int timeout) -> bool {
        if (!d->opened) return false;
        return d->port->waitForBytesWritten(timeout);
    }

    auto MCUCommSerial::waitForReadyRead(int timeout) -> bool {
        if (!d->opened) return false;
        return d->port->waitForReadyRead(timeout);
    }

    auto MCUCommSerial::readAll() const -> QByteArray {
        if (!d->opened) return QByteArray();
        return d->port->readAll();
    }
}
