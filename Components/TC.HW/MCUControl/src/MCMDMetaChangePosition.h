#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaChangePosition : public IMCUMotionCommandMeta {
    public:
        using Pointer = std::shared_ptr<MCMDMetaChangePosition>;

    protected:
        MCMDMetaChangePosition();

    public:
        ~MCMDMetaChangePosition();

        static auto GetInstance()->Pointer;
    };
}
