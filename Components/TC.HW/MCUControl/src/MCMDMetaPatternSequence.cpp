#include "MCMDMetaPatternSequence.h"

namespace TC::MCUControl {
    struct MCMDMetaPatternSequence::Impl {
    };

    MCMDMetaPatternSequence::MCMDMetaPatternSequence() {
        RegisterParameter("Enable", "");
        RegisterParameter("SequenceID", "");
        RegisterParameter("RedIntensity", "");
        RegisterParameter("GreenIntensity", "");
        RegisterParameter("BlueIntensity", "");
        RegisterParameter("Exposure", "usec");
        RegisterParameter("Interval", "usec");
        RegisterParameter("CameraType", "");
    };

    MCMDMetaPatternSequence::~MCMDMetaPatternSequence() {
    }

    auto MCMDMetaPatternSequence::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaPatternSequence() };
        return theInstance;
    }

}