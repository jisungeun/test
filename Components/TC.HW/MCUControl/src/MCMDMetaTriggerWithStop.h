#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaTriggerWithStop : public IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<MCMDMetaTriggerWithStop> Pointer;

    protected:
        MCMDMetaTriggerWithStop();

    public:
        ~MCMDMetaTriggerWithStop();

        static auto GetInstance()->Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}