#include "MCUMacro.h"

namespace TC::MCUControl {
    struct MCUMacro::Impl {
        uint32_t cmdID{ 0 };
        int32_t xPos{ 0 };
        int32_t yPos{ 0 };
        int32_t zPos{ 0 };
        int32_t afMode{ 0 };
    };


    MCUMacro::MCUMacro() : d{ new Impl } {
    }

    MCUMacro::MCUMacro(const MCUMacro& other) : d{new Impl} {
        d->cmdID = other.d->cmdID;
        d->xPos = other.d->xPos;
        d->yPos = other.d->yPos;
        d->zPos = other.d->zPos;
        d->afMode = other.d->afMode;
    }

    MCUMacro::~MCUMacro() {
    }

    auto MCUMacro::SetMotionCommand(uint32_t cmdID) -> void {
        d->cmdID = cmdID;
    }

    auto MCUMacro::GetMotionCommand() const -> uint32_t {
        return d->cmdID;
    }

    auto MCUMacro::SetAFMode(int32_t mode) -> void {
        d->afMode = mode;
    }

    auto MCUMacro::GetAFMode() const -> int32_t {
        return d->afMode;
    }

    auto MCUMacro::SetPosition(int32_t xpos, int32_t ypos, int32_t zpos) -> void {
        d->xPos = xpos;
        d->yPos = ypos;
        d->zPos = zpos;
    }

    auto MCUMacro::GetPositionX() -> int32_t {
        return d->xPos;
    }

    auto MCUMacro::GetPositionY() -> int32_t {
        return d->yPos;
    }

    auto MCUMacro::GetPositionZ() -> int32_t {
        return d->zPos;
    }
}
