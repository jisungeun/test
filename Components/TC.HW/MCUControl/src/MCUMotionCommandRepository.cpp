#include <QList>
#include "MCMDRepoIO.h"
#include "MCUMotionCommandRepository.h"

namespace TC::MCUControl {
    struct MCUMotionCommandRepository::Impl {
        QList<std::shared_ptr<IMCUMotionCommand>> repo;
        MCUError error;
    };

    MCUMotionCommandRepository::MCUMotionCommandRepository() : d{ new Impl } {
    }

    MCUMotionCommandRepository::~MCUMotionCommandRepository() {
    }

    auto MCUMotionCommandRepository::GetInstance() -> Pointer {
        static MCUMotionCommandRepository::Pointer theInstance{ new MCUMotionCommandRepository() };
        return theInstance;
    }

    auto MCUMotionCommandRepository::InsertCommand(IMCUMotionCommand::Pointer& command) -> void {
        d->repo.push_back(command);
    }

    auto MCUMotionCommandRepository::DeleteCommand(uint32_t commandID) -> void {
        for (auto cmd : d->repo) {
            if (cmd->GetCommandID() == commandID) {
                d->repo.removeOne(cmd);
                return;
            }
        }
    }

    auto MCUMotionCommandRepository::GetCounts() const -> uint32_t {
        return d->repo.size();
    }

    auto MCUMotionCommandRepository::GetNextCommandID() const -> uint32_t {
        uint32_t commandID = 0;

        for (auto cmd : d->repo) {
            if (cmd->GetCommandID() >= commandID) {
                commandID = cmd->GetCommandID() + 1;
            }
        }

        return commandID;
    }

    auto MCUMotionCommandRepository::GetCommand(uint32_t commandID) -> IMCUMotionCommand::Pointer {
        IMCUMotionCommand::Pointer command{ nullptr };

        for (auto cmd : d->repo) {
            if (cmd->GetCommandID() == commandID) {
                command = cmd;
                break;
            }
        }

        return command;
    }

    auto MCUMotionCommandRepository::CloneCommand(uint32_t commandID) -> IMCUMotionCommand::Pointer {
        IMCUMotionCommand::Pointer command{ nullptr };

        for (auto cmd : d->repo) {
            if (cmd->GetCommandID() == commandID) {
                command = cmd->clone();
                break;
            }
        }

        return command;
    }

    auto MCUMotionCommandRepository::CloneCommandByIndex(uint32_t index) -> IMCUMotionCommand::Pointer {
        if (index >= static_cast<uint32_t>(d->repo.length())) return nullptr;
        return d->repo.at(index)->clone();
    }

    auto MCUMotionCommandRepository::Clear() -> void {
        d->repo.clear();
    }

    auto MCUMotionCommandRepository::Save(const QString& path) -> bool {
        MCMDRepoIO io;
        if (!io.Save(path)) {
            d->error = io.GetLastError();
            return false;
        }
        return true;
    }

    auto MCUMotionCommandRepository::Load(const QString& path) -> bool {
        MCMDRepoIO io;
        if (!io.Load(path)) {
            d->error = io.GetLastError();
            return false;
        }
        return true;
    }

    auto MCUMotionCommandRepository::GetLastError() const -> MCUError {
        return d->error;
    }
}
