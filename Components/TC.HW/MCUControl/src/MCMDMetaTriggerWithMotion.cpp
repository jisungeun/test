#include "MCMDMetaTriggerWithMotion.h"

namespace TC::MCUControl {
    struct MCMDMetaTriggerWithMotion::Impl {
    };

    MCMDMetaTriggerWithMotion::MCMDMetaTriggerWithMotion() {
        RegisterParameter("Axis", "");
        RegisterParameter("MotionStartPos", "Pulse");
        RegisterParameter("MotionEndPos", "Pulse");
        RegisterParameter("TriggerStartPos", "Pulse");
        RegisterParameter("TriggerEndPos", "Pulse");
        RegisterParameter("TriggerInterval", "Pulse");
        RegisterParameter("TriggerPulseWidth", "usec");
        RegisterParameter("MotionSpeed", "PPS");
        RegisterParameter("MotionAcceleration", "PPS^2");
    };

    MCMDMetaTriggerWithMotion::~MCMDMetaTriggerWithMotion() {
    }

    auto MCMDMetaTriggerWithMotion::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaTriggerWithMotion() };
        return theInstance;
    }

}