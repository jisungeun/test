#include "MCUCommSimul.h"

namespace TC::MCUControl {
    struct MCUCommSimul::Impl {
        QByteArray packet;
        bool readyRead{ false };
    };

    MCUCommSimul::MCUCommSimul(QObject* parent) : MCUComm(parent), d{ new Impl } {
    }

    MCUCommSimul::~MCUCommSimul() {
    }

    auto MCUCommSimul::SetParameter(MCUCommParam& /*param*/) -> void {
    }

    auto MCUCommSimul::OpenPort() -> bool {
        return true;
    }

    auto MCUCommSimul::IsOpened() const -> bool {
        return true;
    }

    auto MCUCommSimul::write(const QByteArray& data) -> qint64 {
        d->packet.clear();
        d->readyRead = true;
        emit sigWritten(data);
        return data.length();
    }

    auto MCUCommSimul::waitForBytesWritten(int /*timeout*/) -> bool {
        return true;
    }

    auto MCUCommSimul::waitForReadyRead(int /*timeout*/) -> bool {
        auto ready = d->readyRead;
        if (ready) d->readyRead = false;
        return ready;
    }

    auto MCUCommSimul::readAll() const -> QByteArray {
        return d->packet;
    }

    void MCUCommSimul::onReceived(const QByteArray& data) {
        d->packet = data;
    }
}
