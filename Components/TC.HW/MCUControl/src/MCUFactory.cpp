#include "MCUControlHTX.h"
#include "MCUFactory.h"

namespace TC::MCUControl {
    MCUFactory::MCUFactory() {
    }

    MCUFactory::~MCUFactory() {
    }

    auto MCUFactory::CreateControl(Model model, bool simulation) -> std::shared_ptr<IMCUControl> {
        auto create = [=]()->IMCUControl* {
            if (model == +Model::HTX) return new MCUControlHTX(simulation);
            return nullptr;
        };

        static std::shared_ptr<IMCUControl> theInstance{create()};
        return theInstance;
    }
}