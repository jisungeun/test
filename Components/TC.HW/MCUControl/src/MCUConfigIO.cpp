#include <QSettings>
#include <QFile>

#include "MCUConfigIO.h"

#define GROUP_GENERAL               "General"
#define GROUP_RESOLUTION            "Resolution"
#define GROUP_FILTER                "Filter"

#define FIELD_PORT                  "Port"
#define FIELD_BAUDRATE              "Baudrate"
#define FIELD_SAFEZPOSITION         "SafeZPosition"
#define FIELD_TEMPERATURE_LOWER     "TemperatureLower"
#define FIELD_TEMPERATURE_UPPER     "TemperatureUpper"
#define FIELD_INCLINATION           "Inclination"
#define FIELD_POSITION              "Position"

namespace TC::MCUControl {
    class MCUConfigIOException : public std::exception {
    public:
        MCUConfigIOException(const QString& msg) : message(msg.toStdString()) {
        }

        char const* what() const override {
            return message.c_str();
        }

    private:
        std::string message;
    };

    struct MCUConfigIO::Impl{
        MCUError error;
    };

    MCUConfigIO::MCUConfigIO() : d{ new Impl } {
    }

    MCUConfigIO::~MCUConfigIO() {
    }

    auto MCUConfigIO::Save(const MCUConfig& config, const QString& path) -> bool {
        QSettings qs(path, QSettings::IniFormat);

        qs.beginGroup(GROUP_GENERAL);
        {
            qs.setValue(FIELD_PORT, config.GetPort());
            qs.setValue(FIELD_BAUDRATE, config.GetBaudrate());
            qs.setValue(FIELD_SAFEZPOSITION, config.GetSafeZPosition());
            qs.setValue(FIELD_TEMPERATURE_LOWER, config.GetTemperatureLowerLimit());
            qs.setValue(FIELD_TEMPERATURE_UPPER, config.GetTemperatureUpperLimit());
            qs.setValue(FIELD_INCLINATION, config.GetInclinationLimit());
        }
        qs.endGroup();

        qs.beginGroup(GROUP_RESOLUTION);
        {
            for (Axis axis : Axis::_values()) {
                qs.setValue(axis._to_string(), config.GetResolution(axis));
            }
        }
        qs.endGroup();

        const auto filterPositions = config.GetFilterPositions();
        qs.beginWriteArray(GROUP_FILTER);
        for(int32_t idx=0; idx<filterPositions.length(); idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(FIELD_POSITION, filterPositions.at(idx));
        }
        qs.endArray();

        return true;
    }

    auto MCUConfigIO::Load(MCUConfig& config, const QString& path) -> bool {
        if (!QFile::exists(path)) {
            d->error = MCUError(Error::MCU_NO_CONFIG_FILE);
            return false;
        }

        QSettings qs(path, QSettings::IniFormat);

        auto readField = [&](const QString& field)->QVariant {
            auto value = qs.value(field);
            if (value.isNull()) {
                throw MCUConfigIOException(tr("A field is not exist - %1").arg(field));
            }
            return value;
        };

        try {
            qs.beginGroup(GROUP_GENERAL);
            {
                config.SetPort(readField(FIELD_PORT).toInt());
                config.SetBaudrate(readField(FIELD_BAUDRATE).toUInt());
                config.SetSafeZPosition(readField(FIELD_SAFEZPOSITION).toDouble());
                config.SetTemperatureLimit(readField(FIELD_TEMPERATURE_LOWER).toDouble(),
                                           readField(FIELD_TEMPERATURE_UPPER).toDouble());
                config.SetInclinationLimit(readField(FIELD_INCLINATION).toDouble());
            }
            qs.endGroup();

            qs.beginGroup(GROUP_RESOLUTION);
            {
                auto keys = qs.allKeys();
                for (auto key : keys) {
                    auto value = readField(key).toUInt();
                    auto axis = Axis::_from_string(key.toStdString().c_str());
                    config.SetResolution(axis, value);
                }
            }
            qs.endGroup();

            QList<int32_t> filterPositions;
            const auto filterCount = qs.beginReadArray(GROUP_FILTER);
            for(int32_t idx=0; idx<filterCount; idx++) {
                qs.setArrayIndex(idx);
                filterPositions.push_back(readField(FIELD_POSITION).toInt());
            }
            qs.endArray();
            config.SetFilterPositions(filterPositions);
        } catch (MCUConfigIOException& ex) {
            d->error = MCUError(Error::MCU_NO_FIELD_ON_CONFIG, ex.what());
            return false;
        } catch (std::exception& ex) {
            d->error = MCUError(Error::MCU_FAIL_LOADING_CONFIG, ex.what());
            return false;
        }

        return true;
    }

    auto MCUConfigIO::GetLastError() const -> MCUError {
        return d->error;
    }
}
