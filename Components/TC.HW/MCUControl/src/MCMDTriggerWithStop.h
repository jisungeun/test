#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMCTriggerWithStop : public IMCUMotionCommand {
    public:
        MCMCTriggerWithStop();
        virtual ~MCMCTriggerWithStop();

        auto GetCommandType() const->MotionCommandType override;

    protected:
        auto GetMetadata() const->IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance()->IMCUMotionCommand::Pointer override;
    };
}