#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMDChangeFilter : public IMCUMotionCommand {
    public:
        MCMDChangeFilter();
        virtual ~MCMDChangeFilter();

        auto GetCommandType() const->MotionCommandType override;

    protected:
        auto GetMetadata() const->IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance()->IMCUMotionCommand::Pointer override;
    };
}