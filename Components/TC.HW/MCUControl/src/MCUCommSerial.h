#pragma once
#include <memory>

#include "MCUComm.h"

namespace TC::MCUControl {
    class MCUCommSerial : public MCUComm {
        Q_OBJECT

    public:
        MCUCommSerial(QObject* parent = nullptr);
        virtual ~MCUCommSerial();

        auto SetParameter(MCUCommParam& param) -> void override;
        auto OpenPort() -> bool override;
        auto IsOpened() const -> bool override;

        auto write(const QByteArray& data) -> qint64 override;
        auto waitForBytesWritten(int timeout) -> bool override;
        auto waitForReadyRead(int timeout) -> bool override;
        auto readAll() const -> QByteArray override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}