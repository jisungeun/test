#pragma once
#include <memory>
#include <QCoreApplication>

#include "MCUError.h"

namespace TC::MCUControl {
    class MCUMacroRepoIO {
        Q_DECLARE_TR_FUNCTIONS(MCUMacroRepoIO)

    public:
        MCUMacroRepoIO();
        virtual ~MCUMacroRepoIO();

        auto Save(const QString& path)->bool;
        auto Load(const QString& path)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}