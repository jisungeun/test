#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaTriggerOnly : public IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<MCMDMetaTriggerOnly> Pointer;

    protected:
        MCMDMetaTriggerOnly();

    public:
        ~MCMDMetaTriggerOnly();

        static auto GetInstance()->Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}