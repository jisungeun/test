#include <QMap>
#include "MCMDMetaTriggerWithMotion.h"
#include "MCMDTriggerWithMotion.h"

namespace TC::MCUControl {
    MCMCTriggerWithMotion::MCMCTriggerWithMotion() {
    }

    MCMCTriggerWithMotion::~MCMCTriggerWithMotion() {
    }

    auto MCMCTriggerWithMotion::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::TriggerWithMotion;
    }

    auto MCMCTriggerWithMotion::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaTriggerWithMotion::GetInstance();
    }

    auto MCMCTriggerWithMotion::CreateInstance() -> IMCUMotionCommand::Pointer {
        Pointer instance{ new MCMCTriggerWithMotion() };
        return instance;
    }
}
