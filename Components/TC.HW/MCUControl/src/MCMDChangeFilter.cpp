#include <QMap>
#include "MCMDMetaChangeFilter.h"
#include "MCMDChangeFilter.h"

namespace TC::MCUControl {
    MCMDChangeFilter::MCMDChangeFilter() {
    }

    MCMDChangeFilter::~MCMDChangeFilter() {
    }

    auto MCMDChangeFilter::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::ChangeFilter;
    }

    auto MCMDChangeFilter::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaChangeFilter::GetInstance();
    }

    auto MCMDChangeFilter::CreateInstance() -> IMCUMotionCommand::Pointer {
        Pointer instance{ new MCMDChangeFilter() };
        return instance;
    }
}
