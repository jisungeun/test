#include "MCMDMetaTriggerWithStop.h"

namespace TC::MCUControl {
    struct MCMDMetaTriggerWithStop::Impl {
    };

    MCMDMetaTriggerWithStop::MCMDMetaTriggerWithStop() {
        RegisterParameter("Axis", "");
        RegisterParameter("TriggerStartPos", "Pulse");
        RegisterParameter("TriggerEndPos", "Pulse");
        RegisterParameter("TriggerInterval", "Pulse");
        RegisterParameter("TriggerPulseWidth", "usec");
        RegisterParameter("TriggerDelay", "usec");
    };

    MCMDMetaTriggerWithStop::~MCMDMetaTriggerWithStop() {
    }

    auto MCMDMetaTriggerWithStop::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaTriggerWithStop() };
        return theInstance;
    }

}