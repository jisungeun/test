#pragma once

#include "IMCUControl.h"

namespace TC::MCUControl {
    class MCUControlHTX : public IMCUControl {
    public:
        MCUControlHTX(bool simulation = false);
        virtual ~MCUControlHTX();

        auto OpenPort() -> bool override;
        auto StartInitialization() -> bool override;
        auto CheckInitialization(int32_t& progress) -> bool override;
        auto SetSafeZPosition(int32_t position) -> bool override;
        auto SetCommand(int32_t command, int32_t type, const QList<int32_t>& cmdParams) -> bool override;
        auto SetSWLimitPosition(const QList<int32_t>& limits) -> bool override;
        auto SetInPositionBand(const QList<int32_t>& bands) -> bool override;
        auto AddMacroPoint(int32_t group, int32_t index, int32_t command, 
                           int32_t xPosition, int32_t yPosition, int32_t zPosition, int32_t afMode) -> bool override;
        auto ClearMacro(int32_t group) -> bool override;
        auto RunMacro(int32_t group) -> bool override;
        auto StopMacro() -> bool override;
        auto CheckMacroStatus(Flag& status, uint32_t& step) -> bool override;
        auto SendMacrosToStreamBuffer(const QList<QList<int32_t>>& commands) -> bool override;
        auto StartMacroStreaming() -> bool override;
        auto StopMacroStreaming() -> bool override;
        auto Move(Axis axis, int32_t position) -> bool override;
        auto SetFilterPositions(const QList<int32_t>& positions) -> bool override;
        auto ChangeFilter(int32_t channel) -> bool override;
        auto MoveToLoadingReadyPosition() -> bool override;
        auto MoveToSafeZPos() -> bool override;
        auto GenerateTrigger(int32_t pulseWidth, int32_t interval, int32_t count) -> bool override;
        auto SetDO(int32_t channel, int32_t value) -> bool override;
        auto ReadDI(int32_t channel, Flag& status) -> bool override;
        auto MoveXY(int32_t xPosition, int32_t yPosition) -> bool override;
        auto StopMotion() -> bool override;
        auto LoadCartridge() -> bool override;
        auto UnloadCartridge() -> bool override;
        auto FinishCalibration() -> bool override;
        auto StartTest() -> bool override;
        auto FinishTest() -> bool override;
        auto Recover() -> bool override;
        auto StartManualMode() -> bool override;
        auto StopManualMode() -> bool override;
        auto GetDLPCVersion(int32_t& fw_ver, int32_t& sw_ver)->bool override;
        auto SetLEDIntensity(int32_t red, int32_t green, int32_t blue)->bool override;
        auto SetDMDExposure(int32_t exposure, int32_t interval)->bool override;
        auto ReadTriggerCount(int32_t& count)->bool override;
        auto ResetTriggerCount()->bool override;
        auto SetLEDChannel(int32_t channel) -> bool override;
        auto SetCameraType(bool enableInternal, bool enableExternal) -> bool override;
        auto SetRGBLedIntensity(int32_t red, int32_t green, int32_t blue) -> bool override;
        auto IsMoving(Axis axis, Flag& status) -> bool override;
        auto CheckStatus(MCUResponse& status) -> bool override;
        auto GetPosition(Axis axis, int32_t& position) -> bool override;
        auto GetPositions() -> std::tuple<bool,QMap<Axis,int32_t>> override;
        auto ReadTemperature(int32_t sensor, float& temperature) -> bool override;
        auto ReadAcceleration(float& xAngle, float& yAngle, float& zAngle) -> bool override;
        auto ReadCartridgeSensor(Flag& inlet, Flag& loading) -> bool override;
        auto ReadMCUFirmwareVersion() -> QString override;
        auto ReadMCULog() -> QString override;
        auto GetCommPort() -> MCUComm* override;
        auto WriteFlash(const QList<int32_t>& data) -> bool override;
        auto ReadFlash(QList<int32_t>& data) -> bool override;

        auto SetAFParameter(const MCUAFParam& param) -> bool override;
        auto SetAFTarget(int32_t mode, int32_t value, int32_t& targetOnAFM) -> bool override;
        auto InitAF() -> bool override;
        auto CheckAFInitialization(AFInitState& status) -> bool override;
        auto EnableAF(int32_t mode) -> bool override;
        auto CheckAFStatus(int32_t& status, int32_t& result, int32_t& trialCount) -> bool override;
        auto ReadAFMValue(int32_t& value) -> bool override;
        auto SetManualAF(bool laser) -> bool override;
        auto ReadAFLog(QList<int32_t>& adcDiff, QList<int32_t>& zComp) -> bool override;
        auto ScanZForAFM(int32_t startPos, int32_t endPos, int32_t interval, int32_t delay) -> bool override;
        auto DumpZScanForAFM(QList<int32_t>& values) -> bool override;
        auto ReadSensorParameter() -> QMap<QString, int32_t> override;
        auto ChangeLensParameter(int32_t param) -> bool override;
        auto SetAFMPeakMode(bool peakMode) -> bool override;

        auto SendPacket(const QByteArray& packet, QList<int32_t>& params, ResponseCode& respCode) -> bool override;
        auto SendPacket(const QString& cmd, const QString& params, QByteArray& resp, ResponseCode& respCode) -> bool override;

    protected:
        auto Send(const QByteArray& packet, QList<int32_t>& params, ResponseCode& respCode, uint32_t timeout = 30000)->bool;
        auto Send(const QByteArray& packet, QByteArray& params, ResponseCode& respCode, uint32_t timeout = 30000)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}