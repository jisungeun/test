#include <QMap>

#include "MCUResponse.h"

namespace TC::MCUControl {
    struct MCUResponse::Impl {
        QMap<Response, int32_t> response;
        QMap<Response, float> times;
    };

    MCUResponse::MCUResponse() : d{ new Impl } {
        for (auto title : Response::_values()) {
            d->response[title] = 0;
        }
    }

    MCUResponse::MCUResponse(const MCUResponse& other) : d{ new Impl } {
        d->response = other.d->response;
        d->times = other.d->times;
    }

    MCUResponse::~MCUResponse() {
    }

    auto MCUResponse::operator=(const MCUResponse& other) -> MCUResponse& {
        d->response = other.d->response;
        d->times = other.d->times;
        return (*this);
    }

    auto MCUResponse::GetValue(Response title) const -> int32_t {
        return d->response[title];
    }

    auto MCUResponse::GetValueAsFlag(Response title) const -> Flag {
        auto flag = Flag::True;
        if (d->response[title] == 0) flag = Flag::False;
        return flag;
    }

    auto MCUResponse::GetValueAsFloat(Response title) const -> float {
        return d->response[title] * d->times[title];
    }

    auto MCUResponse::SetValue(Response title, int32_t value, float times) -> void {
        d->response[title] = value;
        d->times[title] = times;
    }

    auto MCUResponse::GetState() const -> MCUState {
        MCUState state{ MCUState::Unknown };

        try {
            state = MCUState::_from_integral(d->response[Response::StateMachineId]);
        } catch (std::exception) {
            state = MCUState::Unknown;
        }

        return state;
    }

    auto MCUResponse::GetAngle(Response title) const -> float {
        float angle = -1;
        switch (title) {
        case Response::AngleX:
        case Response::AngleY:
        case Response::AngleZ:
            angle = GetValueAsFloat(title);
            break;
        }
        return angle;
    }

    auto MCUResponse::GetTemperature(Response title) const -> float {
        float temperature = -1;
        switch (title) {
        case Response::Temperature0:
        case Response::Temperature1:
        case Response::Temperature2:
        case Response::Temperature3:
            temperature = GetValueAsFloat(title);
            break;
        }
        return temperature;
    }
}
