#pragma once
#include <memory>
#include <QCoreApplication>
#include <QByteArray>
#include <QList>

#include "MCUError.h"
#include "MCUDefines.h"

namespace TC::MCUControl {
    class MCUPacketParser {
        Q_DECLARE_TR_FUNCTIONS(MCUPacketParser)

    public:
        MCUPacketParser();
        virtual ~MCUPacketParser();

        auto Parse(const QByteArray& packet, ResponseCode& resp, QList<int32_t>& params)->bool;
        auto Parse(const QByteArray& packet, ResponseCode& resp, QByteArray& params)->bool;
        auto Parse(const QByteArray& packet, Command& cmd, QList<int32_t>& params)->bool;
        auto Parse(const QByteArray& packet, Command& cmd, QByteArray& params)->bool;

        auto GetLastError() const->MCUError;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
