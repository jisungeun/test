#define LOGGER_TAG "[MCU]"

#include <TCLogger.h>
#include "MCUConfigIO.h"
#include "IMCUControl.h"

namespace TC::MCUControl {
    struct IMCUControl::Impl {
        MCUError error;
        MCUConfig config;
    };

    IMCUControl::IMCUControl(bool /*simulation*/) : d{ new Impl } {
    }

    IMCUControl::~IMCUControl() {
    }

    auto IMCUControl::GetMotionCommandRepository() -> MCUMotionCommandRepository::Pointer {
        return MCUMotionCommandRepository::GetInstance();
    }

    auto IMCUControl::GetMacroRepository() -> MCUMacroRepository::Pointer {
        return MCUMacroRepository::GetInstance();
    }

    auto IMCUControl::GetLastError() -> MCUError {
        return d->error;
    }

    auto IMCUControl::LoadConfig(const QString& path) -> bool {
        MCUConfigIO io;
        MCUConfig config;

        QLOG_INFO() << "Loading MCU configuration from " << path;

        if (!io.Load(config, path)) {
            SetLastError(io.GetLastError());
            return false;
        }

        d->config = config;

        QLOG_INFO() << "Configuration is loaded";

        return true;
    }

    auto IMCUControl::SetConfig(const MCUConfig& config) -> void {
        d->config = config;
    }

    auto IMCUControl::SaveConfig(const QString& path) -> bool {
        MCUConfigIO io;

        QLOG_INFO() << "Saving MCU configuration to " << path;

        if (!io.Save(d->config, path)) {
            SetLastError(io.GetLastError());
            return false;
        }

        QLOG_INFO() << "Configuration is saved";

        return true;
    }

    auto IMCUControl::SetLastError(const MCUError& error) -> void {
        d->error = error;
        QLOG_ERROR() << d->error.GetDescription() << " [" << d->error.GetText() << "]";
    }

    auto IMCUControl::GetConfig() -> MCUConfig& {
        return d->config;
    }

    auto IMCUControl::SetSafeZPosition() -> bool {
        return SetSafeZPosition(d->config.GetSafeZPosition());
    }

    auto IMCUControl::SetCommand(IMCUMotionCommand::Pointer& cmd) -> bool {
        return SetCommand(cmd->GetCommandID(), cmd->GetCommandType(), cmd->GetParameters());
    }
}
