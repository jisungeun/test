#include <QString>
#include <QMap>

#include "MCUError.h"

namespace TC::MCUControl {
    static QMap<Error, QString> errorList;

    struct MCUError::Impl {
        Error code{ Error::MCU_NOERROR };
        QString description;

        Impl() {
            InitializeErrorList();
        }

        Impl(const Impl& other) {
            code = other.code;
            description = other.description;
            InitializeErrorList();
        }

        auto InitializeErrorList()->void;
    };

    auto MCUError::Impl::InitializeErrorList() -> void {
        if (!errorList.isEmpty()) return;

        errorList = {
            {Error::MCU_NOERROR, tr("No Error")},
            {Error::MCU_FAIL_LOADING_CONFIG, tr("MCU configuration file is disrupted. Please check the file.")},
            {Error::MCU_NO_FIELD_ON_CONFIG, tr("There is a missing field on the MCU configuration file. Please check the file.")},
            {Error::MCU_NO_CONFIG_FILE, tr("MCU configuration file does not exist. Please check if the file exist.")},
            {Error::MCU_NO_FIELD_ON_MOTIONCOMMAND, tr("There is a missing field on the motion command. Please check the motion command.")},
            {Error::MCU_FAIL_UNDEFINED_MOTIONCOMMAND, tr("Undefined motion command. Please check the type of motion command.")},
            {Error::MCU_INVALID_MOTIONCOMMAND, tr("Motion command format is different from the current. Please check the motion command.")},
            {Error::MCU_FAILED_SAVING_MOTIONCOMMANDS, tr("It fails to save motion commands to file. Please save it again.")},
            {Error::MCU_NO_MOTIONCOMMANDS_FILE, tr("MCU commands file does not exist. Please check if the file exist.")},
            {Error::MCU_FAILED_LOADING_MOTIONCOMMANDS, tr("It fails to load motion commands from a file. Please retry it again.")},
            {Error::MCU_NO_FIELD_ON_MACROCOMMAND, tr("There is a missing field on the macro command. Please check the macro command.")},
            {Error::MCU_FAILED_SAVING_MACROS, tr("It fails to save macros to file. Please save it again.")},
            {Error::MCU_NO_MACROS_FILE, tr("Macro file does not exist. Please check if the file exist.")},
            {Error::MCU_FAILED_LOADING_MACROS, tr("It fails to load macros from a file. Please retry it again.")},
            {Error::MCU_COMM_PACKET_SHORT, tr("MCU response packet is too short. Please restart the system.")},
            {Error::MCU_COMM_INVALID_HEADER, tr("MCU response packet includes invalid header. Please restart the system.")},
            {Error::MCU_COMM_INVALID_TAIL, tr("MCU response packet includes invalid tails. Please restart the system.")},
            {Error::MCU_COMM_INVALID_CHECKSUM, tr("MCU response packet is broken. Please restart the system.")},
            {Error::MCU_COMM_INVALID_RESPONSECODE, tr("MCU response code is invalid. Please restart the system.")},
            {Error::MCU_COMM_INVALID_PACKETLENGTH, tr("The length of MCU response packet is invalid. Please restart the system.")},
            {Error::MCU_COMM_INVALID_COMMAND, tr("MCU command code is invalid. Please restart the system")},
            {Error::MCU_FAILED_OPEN_PORT, tr("It fails to open a port. Please restart the system.")},
            {Error::MCU_PORT_NOT_READY, tr("MCU communication port is not ready. Please restart the system")},
            {Error::MCU_FAILED_SENDING_PACKET, tr("It fails to send packet to MCU. Please restart the system.")},
            {Error::MCU_TIMEOUT_FOR_SEND, tr("It fails to send a packet in time. Please restart the system.")},
            {Error::MCU_TIMEOUT_FOR_RESPONSE, tr("It fails to get a response in time. Please restart the system.")},
            {Error::MCU_FAILED_START_INITIALIZATION, tr("It fails to start initialization. Please restart the system.")},
            {Error::MCU_FAILED_CHECK_INITIALIZATION_PROGRESS, tr("It fails to check initialization progress. Please restart the system.")},
            {Error::MCU_FAILED_SET_SAFEZPOSITION, tr("It fails to set SafeZPosition. Please restart the system.")},
            {Error::MCU_FAILED_SET_FOCUSPOSITION, tr("It fails to set FocusPosition. Please restart the system.")},
            {Error::MCU_FAILED_SET_SCANREADYPOSITION, tr("It fails to set ScanReadyPosition. Please restart the system.")},
            {Error::MCU_FAILED_SET_MOTIONCOMMAND, tr("It fails to set motion command. Please restart the system.")},
            {Error::MCU_FAILED_ADD_MACROPOINT, tr("It fails to add a macro point. Please restart the system.")},
            {Error::MCU_FAILED_CLEAR_MACRO, tr("It fails to clear a macro point. Please restart the system.")},
            {Error::MCU_FAILED_RUN_MACRO, tr("It fails to run a macro point. Please restart the system.")},
            {Error::MCU_FAILED_STOP_MACRO, tr("It fails to run a macro point. Please restart the system.")},
            {Error::MCU_FAILED_CHECK_MACROSTATUS, tr("It fails to check macro status. Please restart the system.")},
            {Error::MCU_FAILED_MOVEAXIS, tr("It fails to move an axis. Please restart the system.")},
            {Error::MCU_FAILED_MOVETO_LOADINGREADYPOSITION, tr("It fails to move to LoadingReadyPosition. Please restart the system.")},
            {Error::MCU_FAILED_MOVETO_READYPOSITION, tr("It fails to move to ReadyPosition. Please restart the system.")},
            {Error::MCU_FAILED_GENERATE_TRIGGER, tr("It fails to generate a trigger. Please restart the system.")},
            {Error::MCU_FAILED_SETDO, tr("It fails to set DO port. Please restart the system.")},
            {Error::MCU_FAILED_GETDI, tr("It fails to read DI port. Please restart the system.")},
            {Error::MCU_FAILED_MOVEXY, tr("It fails to move XY axis. Please restart the system.")},
            {Error::MCU_FAILED_LOAD_CARTRIDGE, tr("It fails to load a cartridge. Please restart the system.")},
            {Error::MCU_FAILED_UNLOAD_CARTRIDGE, tr("It fails to unload a cartridge. Please restart the system.")},
            {Error::MCU_FAILED_FINISH_CALIBRATION, tr("It fails to unload a cartridge. Please restart the system.")},
            {Error::MCU_FAILED_START_TEST, tr("It fails to unload a cartridge. Please restart the system.")},
            {Error::MCU_FAILED_FINISH_TEST, tr("It fails to unload a cartridge. Please restart the system.")},
            {Error::MCU_FAILED_POWEROFF, tr("It fails to request power off. Please restart the system.")},
            {Error::MCU_FAILED_RECOVER, tr("It fails to recover from an error state. Please restart the system.")},
            {Error::MCU_FAILED_START_MANUALMODE, tr("It fails to start manual mode. Please restart the system.")},
            {Error::MCU_FAILED_FINISH_MANUALMODE, tr("It fails to finish manual mode. Please restart the system.")},
            {Error::MCU_FAILED_TO_GET_DLPCVERSION, tr("It fails to get DLPC version. Please restart the system.")},
            {Error::MCU_FAILED_TO_SET_LEDINTENSITY, tr("It fails to set LED intensity. Please restart the system.")},
            {Error::MCU_FAILED_TO_SET_DMDEXPOSURE, tr("It fails to set DMD exposure time. Please restart the system.")},
            {Error::MCU_FAILED_TO_GET_TRIGGERCOUNT, tr("It fails to get trigger count. Please restart the system")},
            {Error::MCU_FAILED_TO_RESET_TRIGGERCOUNT, tr("It fails to reset trigger count. Please restart the system.")},
            {Error::MCU_FAILED_TO_SET_LEDCHANNEL, tr("It fails to set LED channel. Please restart the system.")},
            {Error::MCU_FAILED_TO_SET_CAMERATYE, tr("It fails to set camera type. Please restart the system.")},
            {Error::MCU_FAILED_ISMOVING, tr("It fails to check axis moving status. Please restart the system.")},
            {Error::MCU_FAILED_CHECK_STATUS, tr("It fails to check MCU status. Please restart the system.")},
            {Error::MCU_FAILED_READ_TEMPERATURE, tr("It fails to read temperature sensor. Please restart the system.")},
            {Error::MCU_FAILED_READ_ACCELERATION, tr("It fails to read acceleration sensor. Please restart the system.")},
            {Error::MCU_FAILED_READ_CARTRIDGE_SENSOR, tr("It fails to read cartridge sensor. Please restart the system.")},
            {Error::MCU_FAILED_READ_LOG, tr("It fails to read log")},
            {Error::MCU_FAILED_TO_READ_AFSENSOR_PARAMETERS, tr("It fails to read AF sensor parameters")},
            {Error::MCU_FAILED_TO_CHANGE_AFLENS_PARAMETER, tr("It fails to change AF Lens parameters")},
            {Error::MCU_FAILED_TO_SET_AFM_PEAKMODE, tr("It fails to set AFM peak mode")},
            {Error::MCU_FAILED_TO_STOP_MOTION, tr("It fails to stop motion")},
            {Error::MCU_FAILED_TO_SET_SW_LIMIT, tr("It fails to set software limit")},
            {Error::MCU_FAILED_TO_SET_INPOSITION_BAND, tr("It fails to set in-position band")},
            {Error::MCU_FAILED_TO_READ_FLASHDATA, tr("It fails to read MCU Flash")},
            {Error::MCU_FAILED_TO_WRITE_FLASHDATA, tr("It fails to write MCU Flash")}
        };
    }

    MCUError::MCUError(Error error, const QString& message) : d{ new Impl } {
        d->code = error;
        if (message.isEmpty()) {
            d->description = errorList[error];
        } else {
            d->description = QString("%1\n%2").arg(message).arg(errorList[error]);
        }
    }

    MCUError::MCUError(const MCUError& other) : d{ new Impl(*other.d) } {
    }

    MCUError::~MCUError() {
    }

    auto MCUError::operator=(const MCUError& other) -> MCUError& {
        *d = *other.d;
        return *this;
    }

    auto MCUError::operator==(Error error) -> bool {
        return (d->code == error);
    }

    auto MCUError::operator!=(Error error) -> bool {
        return (d->code != error);
    }

    auto MCUError::GetCode() const -> Error {
        return d->code;
    }

    auto MCUError::GetText() const -> QString {
        return d->code._to_string();
    }

    auto MCUError::GetDescription() const -> QString {
        return d->description;
    }
}
