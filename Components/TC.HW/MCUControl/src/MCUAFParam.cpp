#include "MCUAFParam.h"

namespace TC::MCUControl {
    struct MCUAFParam::Impl {
        int32_t lensID;
        int32_t InFocusRange;
        int32_t direction;
        int32_t loopInterval;
        int32_t maxTrialCount;
        int32_t sensorValueMin;
        int32_t sensorValueMax;
        int32_t resolutionMul;
        int32_t resolutionDiv;
        int32_t maxCompensationPulse;
        int32_t afLimit{ 0 };           //Not used in FW - HTX-2500
    };
    
    MCUAFParam::MCUAFParam() : d{new Impl} {
    }
    
    MCUAFParam::~MCUAFParam() {
    }

    auto MCUAFParam::operator=(const MCUAFParam& other) -> MCUAFParam& {
        d->lensID = other.d->lensID;
        d->InFocusRange = other.d->InFocusRange;
        d->direction = other.d->direction;
        d->loopInterval = other.d->loopInterval;
        d->maxTrialCount = other.d->maxTrialCount;
        d->sensorValueMin = other.d->sensorValueMin;
        d->sensorValueMax = other.d->sensorValueMax;
        d->resolutionMul = other.d->resolutionMul;
        d->resolutionDiv = other.d->resolutionDiv;
        d->maxCompensationPulse = other.d->maxCompensationPulse;
        d->afLimit = other.d->afLimit;
        return *this;
    }

    auto MCUAFParam::SetLensID(int32_t id) -> void {
        d->lensID = id;
    }

    auto MCUAFParam::GetLensID() const -> int32_t {
        return d->lensID;
    }

    auto MCUAFParam::SetInFocusRange(int32_t value) -> void {
        d->InFocusRange = value;
    }

    auto MCUAFParam::GetInFocusRange() const -> int32_t {
        return d->InFocusRange;
    }

    auto MCUAFParam::SetDirection(int32_t direction) -> void {
        d->direction = direction;
    }

    auto MCUAFParam::GetDirection() const -> int32_t {
        return d->direction;
    }

    auto MCUAFParam::SetLoopInterval(int32_t interval) -> void {
        d->loopInterval = interval;
    }

    auto MCUAFParam::GetLoopInterval() const -> int32_t {
        return d->loopInterval;
    }

    auto MCUAFParam::SetMaxTrialCount(int32_t count) -> void {
        d->maxTrialCount = count;
    }

    auto MCUAFParam::GetMaxTrialCount() const -> int32_t {
        return d->maxTrialCount;
    }

    auto MCUAFParam::SetSensorValue(int32_t minVal, int32_t maxVal) -> void {
        d->sensorValueMin = minVal;
        d->sensorValueMax = maxVal;
    }

    auto MCUAFParam::GetSensorValueMin() const -> int32_t {
        return d->sensorValueMin;
    }

    auto MCUAFParam::GetSensorValueMax() const -> int32_t {
        return d->sensorValueMax;
    }

    auto MCUAFParam::SetResolution(int32_t mul, int32_t div) -> void {
        d->resolutionMul = mul;
        d->resolutionDiv = div;
    }

    auto MCUAFParam::GetResolutionMul() const -> int32_t {
        return d->resolutionMul;
    }

    auto MCUAFParam::GetResolutionDiv() const -> int32_t {
        return d->resolutionDiv;
    }

    auto MCUAFParam::SetMaxCompensationPulse(int32_t pulse) -> void {
        d->maxCompensationPulse = pulse;
    }

    auto MCUAFParam::GetMaxCompensationPulse() const -> int32_t {
        return d->maxCompensationPulse;
    }

    auto MCUAFParam::SetAFLimit(int32_t value) -> void {
        d->afLimit = value;
    }

    auto MCUAFParam::GetAFLimit() const -> int32_t {
        return d->afLimit;
    }
}
