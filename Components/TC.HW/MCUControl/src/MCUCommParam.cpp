#include <QMap>
#include "MCUCommParam.h"

namespace TC::MCUControl {
    struct MCUCommParam::Impl {
        QMap<QString, QVariant> params;
    };

    MCUCommParam::MCUCommParam() : d{ new Impl } {
    }

    MCUCommParam::~MCUCommParam() {
    }

    auto MCUCommParam::Set(const QString& name, const QVariant& value) -> void {
        d->params[name] = value;
    }

    auto MCUCommParam::Get(const QString& name) const -> QVariant {
        return d->params[name];
    }

    auto MCUCommParam::GetAsString(const QString& name) const -> QString {
        return Get(name).toString();
    }

    auto MCUCommParam::GetAsInt(const QString& name) const -> int32_t {
        return Get(name).toInt();
    }

    auto MCUCommParam::GetParams() const -> QStringList {
        return d->params.keys();
    }

    auto MCUCommParam::operator=(const MCUCommParam& other) -> MCUCommParam& {
        d->params = other.d->params;
        return (*this);
    }
}
