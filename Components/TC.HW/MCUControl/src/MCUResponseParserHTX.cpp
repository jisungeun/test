#define LOGGER_TAG "[MCU]"
#include <TCLogger.h>

#include "AsciiConverter.h"
#include "MCUResponseParserHTX.h"

namespace TC::MCUControl {
    auto MCUResponseParserHTX::Parse(const QByteArray& packet, MCUResponse& response) -> bool {
        if (packet.length() != 120) return false;

        auto extractNum = [=](uint32_t start, uint32_t offset, uint32_t count)->int32_t {
            return AsciiConverter::toNum(packet.mid(8 * start + offset*2, count*2));
        };

        auto extractNum8Bit = [=](uint32_t start, uint32_t offset, uint32_t count)->int32_t {
            return AsciiConverter::toNum8Bit(packet.mid(8 * start + offset*2, count*2));
        };

        auto extractNum16Bit = [=](uint32_t start, uint32_t offset, uint32_t count)->int32_t {
            return AsciiConverter::toNum16Bit(packet.mid(8 * start + offset*2, count*2));
        };

        auto extractBit = [=](uint32_t start, uint32_t offset, uint32_t bit)->int32_t {
            auto num = extractNum(start, offset, 1);
            return (num >> bit) & 0x1;
        };

        //packet[0][0:3] : Error Code
        //packet[1][0:1] : State machine id
        //         [2:3] : Macro step
        response.SetValue(Response::StateMachineId, extractNum(1, 0, 2));
        response.SetValue(Response::MacroStep, extractNum(1, 2, 2));

        //packet[2][0][0] : X end sensor
        //            [1] : X start sensor
        //            [2] : X moving
        //            [3] : X SW Limit
        //            [4] : U end sensor
        //            [5] : U start sensor
        //            [6] : U moving
        //            [7] : U SW Limit
        response.SetValue(Response::AxisXEnd, extractBit(2, 0, 0));
        response.SetValue(Response::AxisXStart, extractBit(2, 0, 1));
        response.SetValue(Response::AxisXMoving, extractBit(2, 0, 2));
        response.SetValue(Response::AxisXSWLimit, extractBit(2, 0, 3));
        response.SetValue(Response::AxisUEnd, extractBit(2, 0, 4));
        response.SetValue(Response::AxisUStart, extractBit(2, 0, 5));
        response.SetValue(Response::AxisUMoving, extractBit(2, 0, 6));
        response.SetValue(Response::AxisUSWLimit, extractBit(2, 0, 7));

        //packet[2][1][0] : Y end sensor
        //            [1] : Y start sensor
        //            [2] : Y moving
        //            [3] : Y SW Limit
        //            [4] : V end sensor
        //            [5] : V start sensor
        //            [6] : V moving
        //            [7] : V SW Limit
        response.SetValue(Response::AxisYEnd, extractBit(2, 1, 0));
        response.SetValue(Response::AxisYStart, extractBit(2, 1, 1));
        response.SetValue(Response::AxisYMoving, extractBit(2, 1, 2));
        response.SetValue(Response::AxisYSWLimit, extractBit(2, 1, 3));
        response.SetValue(Response::AxisVEnd, extractBit(2, 1, 4));
        response.SetValue(Response::AxisVStart, extractBit(2, 1, 5));
        response.SetValue(Response::AxisVMoving, extractBit(2, 1, 6));
        response.SetValue(Response::AxisVSWLimit, extractBit(2, 1, 7));

        //packet[2][2][0] : Z end sensor
        //            [1] : Z start sensor
        //            [2] : Z moving
        //            [3] : Z SW Limit
        //            [4] : W end sensor
        //            [5] : W start sensor
        //            [6] : W moving
        //            [7] : W SW Limit
        response.SetValue(Response::AxisZEnd, extractBit(2, 2, 0));
        response.SetValue(Response::AxisZStart, extractBit(2, 2, 1));
        response.SetValue(Response::AxisZMoving, extractBit(2, 2, 2));
        response.SetValue(Response::AxisZSWLimit, extractBit(2, 2, 3));
        response.SetValue(Response::AxisWEnd, extractBit(2, 2, 4));
        response.SetValue(Response::AxisWStart, extractBit(2, 2, 5));
        response.SetValue(Response::AxisWMoving, extractBit(2, 2, 6));
        response.SetValue(Response::AxisWSWLimit, extractBit(2, 2, 7));

        //packet[2][3][0] : L end sensor
        //            [1] : L start sensor
        //            [3] : Reserved
        //            [4] : Reserved
        //            [5] : Reserved
        //            [6] : Reserved
        //            [7] : Reserved
        response.SetValue(Response::AxisLEnd, extractBit(2, 3, 0));
        response.SetValue(Response::AxisLStart, extractBit(2, 3, 1));
        response.SetValue(Response::AxisLMoving, extractBit(2, 3, 2));

        //packet[3][0][0] : Door open
        //            [1] : Shutter open
        //            [2] : Cartridge #1
        //            [3] : Cartridge #2
        //            [4] : Reserved
        //            [5] : Reserved
        //            [6] : Reserved
        //            [7] : Reserved
        response.SetValue(Response::SensorDoorOpen, extractBit(3, 0, 0));
        response.SetValue(Response::SensorShutterOpen, extractBit(3, 0, 1));
        response.SetValue(Response::SensorCartridge1, extractBit(3, 0, 2));
        response.SetValue(Response::SensorCartridge2, extractBit(3, 0, 3));

        //packet[3][2][0] : LED 0 on
        //            [1] : LED 1 on
        //            [2] : LED 2 on
        //            [3] : LED 3 on
        //            [4] : Reserved
        //            [5] : Reserved
        //            [6] : Reserved
        //            [7] : Reserved
        response.SetValue(Response::SensorLED0On, extractBit(3, 2, 0));
        response.SetValue(Response::SensorLED1On, extractBit(3, 2, 1));
        response.SetValue(Response::SensorLED2On, extractBit(3, 2, 2));
        response.SetValue(Response::SensorLED3On, extractBit(3, 2, 3));

        //packet[3][3][0] : AF on
        //            [1] : AF error
        //            [2] : Reserved
        //            [3] : Reserved
        //            [4] : Reserved
        //            [5] : Reserved
        //            [6] : Reserved
        //            [7] : Reserved
        response.SetValue(Response::AfOn, extractBit(3, 3, 0));
        response.SetValue(Response::AfError, extractBit(3, 3, 1));

        //packet[4][0] : X angle
        //         [1] : Y angle
        //         [2] : Z angle
        //         [3] : Reserved
        response.SetValue(Response::AngleX, extractNum8Bit(4, 0, 1), 0.1f);
        response.SetValue(Response::AngleY, extractNum8Bit(4, 1, 1), 0.1f);
        response.SetValue(Response::AngleZ, extractNum8Bit(4, 2, 1), 0.1f);

        //packet[5][0:1] : TC0
        //         [1:2] : TC1
        //      [6][0:1] : TC2
        //         [1:2] : TC3
        response.SetValue(Response::Temperature0, extractNum16Bit(5, 0, 2), 0.1f);
        response.SetValue(Response::Temperature1, extractNum16Bit(5, 2, 2), 0.1f);
        response.SetValue(Response::Temperature2, extractNum16Bit(6, 0, 2), 0.1f);
        response.SetValue(Response::Temperature3, extractNum16Bit(6, 2, 2), 0.1f);

        //packet[7][0:3] : X position
        //      [8][0:3] : Y position
        //      [9][0:3] : Z position
        //      [10][0:3] : U position
        //      [11][0:3] : V position
        //      [12][0:3] : W position
        response.SetValue(Response::AxisXPosition, extractNum(7, 0, 4));
        response.SetValue(Response::AxisYPosition, extractNum(8, 0, 4));
        response.SetValue(Response::AxisZPosition, extractNum(9, 0, 4));
        response.SetValue(Response::AxisUPosition, extractNum(10, 0, 4));
        response.SetValue(Response::AxisVPosition, extractNum(11, 0, 4));
        response.SetValue(Response::AxisWPosition, extractNum(12, 0, 4));

        //packet[13][0:3] : Macro Streaming Index
        response.SetValue(Response::MacroStreamingIndex, extractNum(13, 0, 4));

        //packet[14][0] : Macro Streaming Error Flag
        //packet[14][1] : Macro Streaming Half Empty
        //packet[14][2:3] : Macro Streaming Empty Buffer Size
        response.SetValue(Response::MacroStreamingError, extractNum(14, 0, 1));
        response.SetValue(Response::MacroStreamingHalfEmpty, extractNum(14, 1, 1));
        response.SetValue(Response::MacroStreamingEmptyBufferSize, extractNum(14, 2, 2));

        return true;
    }
}
