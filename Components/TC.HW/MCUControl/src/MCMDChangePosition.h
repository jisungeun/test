#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMDChangePosition : public IMCUMotionCommand {
    public:
        MCMDChangePosition();
        virtual ~MCMDChangePosition();

        auto GetCommandType() const -> MotionCommandType override;

    protected:
        auto GetMetadata() const -> IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance() -> IMCUMotionCommand::Pointer override;
    };
}