#pragma once
#include <memory>
#include <QByteArray>

#include "MCUComm.h"

namespace TC::MCUControl {
    class MCUCommSimul : public MCUComm {
        Q_OBJECT
    public:
        MCUCommSimul(QObject* parent = nullptr);
        ~MCUCommSimul();

        auto SetParameter(MCUCommParam& param) -> void override;
        auto OpenPort() -> bool override;
        auto IsOpened() const -> bool override;

        auto write(const QByteArray& data) -> qint64 override;
        auto waitForBytesWritten(int timeout) -> bool override;
        auto waitForReadyRead(int timeout) -> bool override;
        auto readAll() const -> QByteArray override;

    public slots:
        void onReceived(const QByteArray& data);

    signals:
        void sigWritten(const QByteArray& data);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}