#include <QMap>
#include "MCUConfig.h"

namespace TC::MCUControl {
    struct MCUConfig::Impl {
        int32_t port{ -1 };
        uint32_t baudrate{ 115200 };
        int32_t safeZPosition{ 0 };
        struct {
            double lower{ 0 };
            double upper{ 100 };
        } temperature;
        double inclination{ 45 };
        QMap<Axis, uint32_t> resolution;
        QMap<int32_t, int32_t> filterPositions;

        Impl() {
        }

        Impl(const Impl& other) {
            port = other.port;
            baudrate = other.baudrate;
            safeZPosition = other.safeZPosition;
            temperature = other.temperature;
            inclination = other.inclination;
            resolution = other.resolution;
            filterPositions = other.filterPositions;
        }
    };

    MCUConfig::MCUConfig() : d{ new Impl } {
    }

    MCUConfig::MCUConfig(const MCUConfig& other) : d{ new Impl(*other.d) } {
    }

    MCUConfig::~MCUConfig() {
    }

    auto MCUConfig::operator=(const MCUConfig& other) -> MCUConfig& {
        *d = *other.d;
        return *this;
    }

    auto MCUConfig::SetPort(const int32_t port) -> void {
        d->port = port;
    }

    auto MCUConfig::GetPort() const -> int32_t {
        return d->port;
    }

    auto MCUConfig::SetBaudrate(uint32_t baudrate) -> void {
        d->baudrate = baudrate;
    }

    auto MCUConfig::GetBaudrate() const -> int32_t {
        return d->baudrate;
    }

    auto MCUConfig::SetSafeZPosition(int32_t pos) -> void {
        d->safeZPosition = pos;
    }

    auto MCUConfig::GetSafeZPosition() const -> int32_t {
        return d->safeZPosition;
    }

    auto MCUConfig::SetTemperatureLimit(double lowerLimit, double upperLimit) -> void {
        d->temperature.lower = lowerLimit;
        d->temperature.upper = upperLimit;
    }

    auto MCUConfig::GetTemperatureLowerLimit() const -> double {
        return d->temperature.lower;
    }

    auto MCUConfig::GetTemperatureUpperLimit() const -> double {
        return d->temperature.upper;
    }

    auto MCUConfig::SetInclinationLimit(double limit) -> void {
        d->inclination = limit;
    }

    auto MCUConfig::GetInclinationLimit() const -> double {
        return d->inclination;
    }

    auto MCUConfig::SetResolution(Axis axis, uint32_t resolution) -> void {
        d->resolution[axis] = resolution;
    }

    auto MCUConfig::GetResolution(Axis axis) const -> uint32_t {
        return d->resolution[axis];
    }

    auto MCUConfig::SetFilterPosition(int32_t channel, int32_t position) -> void {
        d->filterPositions[channel] = position;
    }

    auto MCUConfig::SetFilterPositions(const QList<int32_t>& positions) -> void {
        d->filterPositions.clear();
        for(int32_t idx=0; idx<positions.length(); idx++) {
            d->filterPositions[idx] = positions.at(idx);
        }
    }

    auto MCUConfig::GetFilterPositions() const -> QList<int32_t> {
        return d->filterPositions.values();
    }

}
