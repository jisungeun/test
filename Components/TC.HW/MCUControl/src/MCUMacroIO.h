#pragma once
#include <memory>
#include <QCoreApplication>
#include <QSettings>

#include "MCUError.h"
#include "MCUMacroGroup.h"
#include "MCUMacro.h"

namespace TC::MCUControl {
    class MCUMacroIO {
        Q_DECLARE_TR_FUNCTIONS(MCUMacroIO)

    public:
        MCUMacroIO();
        virtual ~MCUMacroIO();

        auto Save(MCUMacroGroup::Pointer& macros, QSettings& qs)->bool;
        auto Load(MCUMacroGroup::Pointer& macros, QSettings& qs)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}