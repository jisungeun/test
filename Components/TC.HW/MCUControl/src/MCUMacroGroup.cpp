#include <QList>

#include "MCUMacroGroup.h"

namespace TC::MCUControl {
    struct MCUMacroGroup::Impl {
        uint32_t groupID{ 0 };
        QString title;
        QList<MCUMacro::Pointer> macros;
    };

    MCUMacroGroup::MCUMacroGroup() :d{ new Impl } {
    }

    MCUMacroGroup::MCUMacroGroup(const MCUMacroGroup& other) : d{new Impl} {
        d->groupID = other.d->groupID + 1000;
        d->title = QString("Clone_%1").arg(other.d->title);

        for(auto macro : other.d->macros) {
            MCUMacro::Pointer clone{ new MCUMacro(*macro) };
            d->macros.push_back(clone);
        }
    }

    MCUMacroGroup::~MCUMacroGroup() {
    }

    auto MCUMacroGroup::SetGroupID(uint32_t id) -> void {
        d->groupID = id;
    }

    auto MCUMacroGroup::GetGroupID() const-> uint32_t {
        return d->groupID;
    }

    auto MCUMacroGroup::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto MCUMacroGroup::GetTitle() const -> QString {
        return d->title;
    }

    auto MCUMacroGroup::GetCount() const -> uint32_t {
        return d->macros.length();
    }

    auto MCUMacroGroup::AddMacro(MCUMacro::Pointer& macro) -> void {
        d->macros.push_back(macro);
    }

    auto MCUMacroGroup::GetMacro(uint32_t index) -> MCUMacro::Pointer {
        MCUMacro::Pointer macro{ nullptr };
        if (index < static_cast<uint32_t>(d->macros.length())) macro = d->macros.at(index);
        return macro;
    }

    auto MCUMacroGroup::RemoveMacro(uint32_t index) -> bool {
        if (index >= static_cast<uint32_t>(d->macros.length())) return false;
        d->macros.removeAt(index);
        return true;
    }

    auto MCUMacroGroup::MoveMacro(uint32_t fromIdx, uint32_t toIdx) -> void {
        if(fromIdx >= static_cast<uint32_t>(d->macros.size())) return;
        if(toIdx >= static_cast<uint32_t>(d->macros.size())) return;

        auto item = d->macros.takeAt(fromIdx);
        d->macros.insert(toIdx, item);
    }

    auto MCUMacroGroup::ClearMacros() -> void {
        d->macros.clear();
    }
}
