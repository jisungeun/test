#pragma once
#include <memory>

#include <QByteArray>
#include <QStringList>

namespace TC::MCUControl {
    class MCUSimulatorCBAControl {
    public:
        MCUSimulatorCBAControl();
        virtual ~MCUSimulatorCBAControl();

        auto Process(const QByteArray& data)->QByteArray;

        auto GetStateList() const->QStringList;
        auto ChangeState(int index)->void;
        auto ChangeAxisStartState(Axis axis, bool bChecked)->void;
        auto ChangeAxisEndState(Axis axis, bool bChecked)->void;
        auto ChangeAxisMoveState(Axis axis, bool bChecked)->void;
        auto ChangeAxisPosition(Axis axis, int position)->void;
        auto ChangeShutterState(bool open)->void;
        auto ChangeDoorState(bool open)->void;
        auto ChangeLEDState(int index, bool on)->void;
        auto ChangeAngle(int index, double value)->void;
        auto ChangeTemperature(int index, double value)->void;

        auto GetMacroStep()->int32_t;
        auto GetAxisMoveState(int axisID)->int32_t;
        auto GetAxisPosition(int axisID)->int32_t;
        auto GetTemperature(int sensorID)->int32_t;
        auto GetAcceleration(int sensorID)->int32_t;
        auto GetCartridgeSensor(int sensorID)->int32_t;
        auto GetStatus()->QByteArray;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}