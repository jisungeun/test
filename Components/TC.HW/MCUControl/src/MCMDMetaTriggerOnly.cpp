#include "MCMDMetaTriggerOnly.h"

namespace TC::MCUControl {
    struct MCMDMetaTriggerOnly::Impl {
    };

    MCMDMetaTriggerOnly::MCMDMetaTriggerOnly() {
        RegisterParameter("TriggerCount", "");
        RegisterParameter("TriggerInterval", "usec");
        RegisterParameter("TriggerPulseWidth", "usec");
    };

    MCMDMetaTriggerOnly::~MCMDMetaTriggerOnly() {
    }

    auto MCMDMetaTriggerOnly::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaTriggerOnly() };
        return theInstance;
    }

}