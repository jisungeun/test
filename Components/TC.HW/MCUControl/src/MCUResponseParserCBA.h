#pragma once
#include <QList>
#include "MCUResponse.h"

namespace TC::MCUControl {
    class MCUResponseParserCBA {
    public:
        static auto Parse(const QByteArray& packet, MCUResponse& response)->bool;
    };
}
