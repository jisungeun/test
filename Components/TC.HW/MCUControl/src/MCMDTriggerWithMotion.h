#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMCTriggerWithMotion : public IMCUMotionCommand {
    public:
        MCMCTriggerWithMotion();
        virtual ~MCMCTriggerWithMotion();

        auto GetCommandType() const -> MotionCommandType override;

    protected:
        auto GetMetadata() const -> IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance()->IMCUMotionCommand::Pointer override;
    };
}