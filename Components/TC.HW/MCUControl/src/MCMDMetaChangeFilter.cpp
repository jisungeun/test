#include "MCMDMetaChangeFilter.h"

namespace TC::MCUControl {
    struct MCMDMetaChangeFilter::Impl {
    };

    MCMDMetaChangeFilter::MCMDMetaChangeFilter() {
        RegisterParameter("FilterPosition", "");
    };

    MCMDMetaChangeFilter::~MCMDMetaChangeFilter() {
    }

    auto MCMDMetaChangeFilter::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaChangeFilter() };
        return theInstance;
    }

}