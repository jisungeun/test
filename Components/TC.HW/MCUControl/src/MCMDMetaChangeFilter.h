#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaChangeFilter : public IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<MCMDMetaChangeFilter> Pointer;

    protected:
        MCMDMetaChangeFilter();

    public:
        ~MCMDMetaChangeFilter();

        static auto GetInstance()->Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}