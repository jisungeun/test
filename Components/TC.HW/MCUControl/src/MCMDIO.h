#pragma once
#include <memory>
#include <QCoreApplication>
#include <QSettings>

#include "MCUError.h"
#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMDIO {
        Q_DECLARE_TR_FUNCTIONS(MCMDIO)

    public:
        MCMDIO();
        virtual ~MCMDIO();

        auto Save(IMCUMotionCommand::Pointer& cmd, QSettings& qs)->bool;
        auto Load(IMCUMotionCommand::Pointer& cmd, QSettings& qs)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}