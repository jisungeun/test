#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMCTriggerOnly : public IMCUMotionCommand {
    public:
        MCMCTriggerOnly();
        virtual ~MCMCTriggerOnly();

        auto GetCommandType() const->MotionCommandType override;

    protected:
        auto GetMetadata() const->IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance()->IMCUMotionCommand::Pointer override;
    };
}