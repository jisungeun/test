#include <QList>
#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    struct Parameter {
        Parameter(const QString& _name, const QString& _unit) : name(_name), unit(_unit) {}

        QString name;
        QString unit;
    };

    struct IMCUMotionCommandMeta::Impl {
        QList<Parameter> parameters;
    };

    IMCUMotionCommandMeta::IMCUMotionCommandMeta() : d{ new Impl } {
    }

    IMCUMotionCommandMeta::~IMCUMotionCommandMeta() {
    }


    auto IMCUMotionCommandMeta::GetParameterCount() const -> uint32_t {
        return d->parameters.length();
    }

    auto IMCUMotionCommandMeta::GetParameterName(uint32_t index) const -> QString {
        if (index >= static_cast<uint32_t>(d->parameters.length())) return "";
        return d->parameters[index].name;
    }

    auto IMCUMotionCommandMeta::GetParameterNames() const -> QStringList {
        QStringList names;
        for (auto param : d->parameters) {
            names.push_back(param.name);
        }
        return names;
    }

    auto IMCUMotionCommandMeta::GetParameterUnit(uint32_t index) const -> QString {
        if (index >= static_cast<uint32_t>(d->parameters.length())) return "";
        return d->parameters[index].unit;
    }

    auto IMCUMotionCommandMeta::GetParameterUnits() const -> QStringList {
        QStringList units;
        for (auto param : d->parameters) {
            units.push_back(param.unit);
        }
        return units;
    }

    auto IMCUMotionCommandMeta::RegisterParameter(const QString& name, const QString& unit) -> void {
        d->parameters.push_back(Parameter(name, unit));
    }
}
