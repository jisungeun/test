#include "AsciiConverter.h"

namespace TC::MCUControl {
    AsciiConverter::AsciiConverter() {
    }

    AsciiConverter::~AsciiConverter() {
    }

    auto AsciiConverter::toAscii(int32_t value, int bytes) -> QByteArray {
        const int32_t mask = 0xF;

        QByteArray output;

        for (int idx = 0; idx < bytes; idx++) {
            const auto shifted = (value >> (idx * 4));
            const auto masked_value = shifted & mask;
            if (masked_value >= 0 && masked_value <= 9) output.push_front('0' + masked_value);
            else output.push_front('A' + masked_value - 10);
        }

        return output;
    }

    auto AsciiConverter::toNum(const QByteArray& bytes) -> int32_t {
        const int32_t mask = 0x1;
        int32_t value = 0;

        const int counts = bytes.length();
        for (int idx = 0; idx < counts; idx++) {
            auto byte = bytes.at(idx);
            int32_t num = [=]()->int32_t {
                if (byte >= 'A') return (byte - 'A' + 10);
                return (byte - '0');
            }();

            value = value + num * (mask << ((counts - idx - 1) * 4));
        }
        return value;
    }

    auto AsciiConverter::toNum8Bit(const QByteArray& bytes) -> int32_t {
        const int8_t mask = 0x1;
        int8_t value = 0;

        const int counts = bytes.length();
        for (int idx = 0; idx < counts; idx++) {
            auto byte = bytes.at(idx);
            auto num = [=]()->int8_t {
                if (byte >= 'A') return (byte - 'A' + 10);
                return (byte - '0');
            }();

            value = value + num * (mask << ((counts - idx - 1) * 4));
        }
        return value;
    }

    auto AsciiConverter::toNum16Bit(const QByteArray& bytes) -> int32_t {
        const int16_t mask = 0x1;
        int16_t value = 0;

        const int counts = bytes.length();
        for (int idx = 0; idx < counts; idx++) {
            auto byte = bytes.at(idx);
            auto num = [=]()->int16_t {
                if (byte >= 'A') return (byte - 'A' + 10);
                return (byte - '0');
            }();

            value = value + num * (mask << ((counts - idx - 1) * 4));
        }
        return value;
    }
}
