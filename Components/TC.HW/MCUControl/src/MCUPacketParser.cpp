#include "CRC16Checker.h"
#include "AsciiConverter.h"
#include "MCUPacketParser.h"

namespace TC::MCUControl {
    struct MCUPacketParser::Impl {
        MCUError error;
    };

    MCUPacketParser::MCUPacketParser() :d{ new Impl } {
    }

    MCUPacketParser::~MCUPacketParser() {
    }

    auto MCUPacketParser::Parse(const QByteArray& packet, ResponseCode& resp, QList<int32_t>& params) -> bool {
        QByteArray paramsBytes;

        if (!Parse(packet, resp, paramsBytes)) {
            return false;
        }

        const auto paramCount = paramsBytes.size() / 8;
        for (int idx = 0; idx < paramCount; idx++) {
            auto param = AsciiConverter::toNum(paramsBytes.mid(8 * idx, 8));
            params.push_back(param);
        }

        return true;
    }

    auto MCUPacketParser::Parse(const QByteArray& packet, ResponseCode& resp, QByteArray& params) -> bool {
        const int packetLength = packet.length();
        if (packetLength < 10) {
            d->error = MCUError(Error::MCU_COMM_PACKET_SHORT, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        if (packet.at(0) != '{') {
            d->error = MCUError(Error::MCU_COMM_INVALID_HEADER, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        if (packet.back() != '}') {
            d->error = MCUError(Error::MCU_COMM_INVALID_TAIL, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1, packetLength - 6));
        if (crc != AsciiConverter::toNum(packet.mid(packetLength - 5, 4))) {
            d->error = MCUError(Error::MCU_COMM_INVALID_CHECKSUM, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        try {
            resp = ResponseCode::_from_integral(AsciiConverter::toNum(packet.mid(1, 2)));
        } catch (std::exception& /*ex*/) {
            d->error = MCUError(Error::MCU_COMM_INVALID_RESPONSECODE, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        const auto paramCount = AsciiConverter::toNum(packet.mid(3, 2));
        if (packetLength != (10 + paramCount * 8)) {
            d->error = MCUError(Error::MCU_COMM_INVALID_PACKETLENGTH, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        params = packet.mid(5, 8 * paramCount);

        return true;
    }

    auto MCUPacketParser::Parse(const QByteArray& packet, Command& cmd, QList<int32_t>& params) -> bool {
        QByteArray paramsBytes;

        if (!Parse(packet, cmd, paramsBytes)) {
            return false;
        }

        const auto paramCount = paramsBytes.size() / 8;
        for (int idx = 0; idx < paramCount; idx++) {
            auto param = AsciiConverter::toNum(paramsBytes.mid(8 * idx, 8));
            params.push_back(param);
        }

        return true;
    }

    auto MCUPacketParser::Parse(const QByteArray& packet, Command& cmd, QByteArray& params) -> bool {
        const int packetLength = packet.length();
        if (packetLength < 10) {
            d->error = MCUError(Error::MCU_COMM_PACKET_SHORT, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        if (packet.at(0) != '{') {
            d->error = MCUError(Error::MCU_COMM_INVALID_HEADER, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        if (packet.back() != '}') {
            d->error = MCUError(Error::MCU_COMM_INVALID_TAIL, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1, packetLength - 6));
        if (crc != AsciiConverter::toNum(packet.mid(packetLength - 5, 4))) {
            d->error = MCUError(Error::MCU_COMM_INVALID_CHECKSUM, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        try {
            cmd = Command::_from_integral(AsciiConverter::toNum(packet.mid(1, 2)));
        } catch (std::exception& /*ex*/) {
            d->error = MCUError(Error::MCU_COMM_INVALID_COMMAND, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        const auto paramCount = AsciiConverter::toNum(packet.mid(3, 2));
        if (packetLength != (10 + paramCount * 8)) {
            d->error = MCUError(Error::MCU_COMM_INVALID_PACKETLENGTH, tr("Packet: %1")
                                .arg(QString::fromStdString(packet.toStdString())));
            return false;
        }

        params = packet.mid(5, 8 * paramCount);

        return true;
    }

    auto MCUPacketParser::GetLastError() const -> MCUError {
        return d->error;
    }
}