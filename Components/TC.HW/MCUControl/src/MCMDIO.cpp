#include "MCUMotionCommandFactory.h"
#include "MCMDIO.h"

#define GROUP_PARAMETERS    "Parameters"

#define FIELD_TYPE          "Type"
#define FIELD_ID            "ID"
#define FIELD_TITLE         "Title"
#define FIELD_NAME          "Name"
#define FIELD_UNIT          "Unit"
#define FIELD_VALUE         "Value"

namespace TC::MCUControl {
    class MCMDIOException : public std::exception {
    public:
        MCMDIOException(const QString& msg) : message(msg.toStdString()) {
        }

        char const* what() const override {
            return message.c_str();
        }

    private:
        std::string message;
    };

    struct MCMDIO::Impl {
        MCUError error;
    };

    MCMDIO::MCMDIO() :d{ new Impl } {
    }

    MCMDIO::~MCMDIO() {
    }

    auto MCMDIO::Save(IMCUMotionCommand::Pointer& cmd, QSettings& qs) -> bool {
        qs.setValue(FIELD_ID, cmd->GetCommandID());
        qs.setValue(FIELD_TYPE, cmd->GetCommandTypeAsString());
        qs.setValue(FIELD_TITLE, cmd->GetCommandTitle());

        const auto counts = cmd->GetParameterCount();
        qs.beginWriteArray(GROUP_PARAMETERS);
        for(uint32_t idx=0; idx<counts; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(FIELD_ID, idx);
            qs.setValue(FIELD_NAME, cmd->GetParameterName(idx));
            qs.setValue(FIELD_UNIT, cmd->GetParameterUnit(idx));
            qs.setValue(FIELD_VALUE, cmd->GetParameter(idx));
        }
        qs.endArray();

        return true;
    }

    auto MCMDIO::Load(IMCUMotionCommand::Pointer& cmd, QSettings& qs) -> bool {
        auto factory = MCUMotionCommandFactory::GetInstance();

        auto readField = [&](const QString& field)->QVariant {
            auto value = qs.value(field);
            if (value.isNull()) {
                throw MCMDIOException(tr("A field is not exist - %1").arg(field));
            }
            return value;
        };

        QString typeString;
        try {
            typeString = readField(FIELD_TYPE).toString();
        } catch (MCMDIOException& ex) {
            d->error = MCUError(Error::MCU_NO_FIELD_ON_MOTIONCOMMAND, ex.what());
            return false;
        }

        MotionCommandType cmdType{ MotionCommandType::TriggerWithMotion };
        try {
            cmdType = MotionCommandType::_from_string(typeString.toStdString().c_str());
        } catch (std::exception& /*ex*/) {
            d->error = MCUError(Error::MCU_FAIL_UNDEFINED_MOTIONCOMMAND, QString("%1 is not defined"));
            return false;
        }

        cmd = factory->CreateCommand(cmdType);

        try {
            cmd->SetCommandID(readField(FIELD_ID).toUInt());
            cmd->SetCommandTitle(readField(FIELD_TITLE).toString());

            const auto counts = qs.beginReadArray(GROUP_PARAMETERS);
            for (auto idx = 0; idx < counts; idx++) {
                qs.setArrayIndex(idx);
                const auto paramID = readField(FIELD_ID).toUInt();
                const auto paramName = readField(FIELD_NAME).toString();
                const auto paramUnit = readField(FIELD_UNIT).toString();
                const auto paramValue = readField(FIELD_VALUE).toInt();

                if (!cmd->CheckName(paramID, paramName)) {
                    d->error = MCUError(Error::MCU_INVALID_MOTIONCOMMAND, 
                                        tr("The name of the parameter #%1, %2, is invalid or not in the right order")
                                        .arg(idx+1)
                                        .arg(paramName));
                    return false;
                }

                if (!cmd->CheckUnit(paramID, paramUnit)) {
                    d->error = MCUError(Error::MCU_INVALID_MOTIONCOMMAND, 
                                        tr("The unit of the parameter #%1, %2, is invalid or not in the right order")
                                        .arg(idx+1)
                                        .arg(paramUnit));
                    return false;
                }

                cmd->SetParameter(paramID, paramValue);
            }
            qs.endArray();
        } catch (MCMDIOException& ex) {
            d->error = MCUError(Error::MCU_NO_FIELD_ON_MOTIONCOMMAND, ex.what());
            return false;
        }

        return true;
    }

    auto MCMDIO::GetLastError() const -> MCUError {
        return d->error;
    }
}
