#include "CRC16Checker.h"
#include "AsciiConverter.h"
#include "MCUPacketGenerator.h"

namespace TC::MCUControl {
    MCUPacketGenerator::MCUPacketGenerator() {
    }

    MCUPacketGenerator::~MCUPacketGenerator() {
    }

    auto MCUPacketGenerator::Generate(Command cmd, const QList<int32_t>& params) -> QByteArray {
        QByteArray packet;

        packet.push_back('{');                                              //Header
        packet.push_back(AsciiConverter::toAscii(cmd._to_integral(), 2));      //Command
        packet.push_back(AsciiConverter::toAscii(params.length(), 2));      //# of parameters

        for (auto param : params) {                                         //Parameters
            packet.push_back(AsciiConverter::toAscii(param, 8));
        }

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1));
        packet.push_back(AsciiConverter::toAscii(crc, 4));                  //Checksum

        packet.push_back('}');                                              //Tail

        return packet;
    }

    auto MCUPacketGenerator::Generate(const QString& cmd, const QString& params) -> QByteArray {
        QByteArray packet;

        packet.push_back('{');                                              //Header
        packet.push_back(cmd.toLatin1());                                   //Command
        packet.push_back(AsciiConverter::toAscii(params.length()/8, 2));    //# of parameters
        packet.push_back(params.toLatin1());                                //Parameters

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1));
        packet.push_back(AsciiConverter::toAscii(crc, 4));                  //Checksum

        packet.push_back('}');                                              //Tail

        return packet;
    }

    auto MCUPacketGenerator::Generate(ResponseCode respCode, const QList<int32_t>& params) -> QByteArray {
        QByteArray packet;

        packet.push_back('{');                                              //Header
        packet.push_back(AsciiConverter::toAscii(respCode._to_integral(), 2)); //Response Code
        packet.push_back(AsciiConverter::toAscii(params.length(), 2));      //# of parameters

        for (auto param : params) {                                         //Parameters
            packet.push_back(AsciiConverter::toAscii(param, 8));
        }

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1));
        packet.push_back(AsciiConverter::toAscii(crc, 4));                  //Checksum

        packet.push_back('}');                                              //Tail

        return packet;
    }

    auto MCUPacketGenerator::Generate(ResponseCode respCode, const QByteArray& params) -> QByteArray {
        QByteArray packet;

        const auto paramCount = params.length() / 8;

        packet.push_back('{');                                              //Header
        packet.push_back(AsciiConverter::toAscii(respCode._to_integral(), 2)); //Response Code
        packet.push_back(AsciiConverter::toAscii(paramCount, 2));           //# of parameters
        packet.push_back(params);                                           //Parameters

        auto crcChecker = CRC16Checker::GetInstance();
        auto crc = crcChecker->Check(packet.mid(1));
        packet.push_back(AsciiConverter::toAscii(crc, 4));                  //Checksum

        packet.push_back('}');                                              //Tail

        return packet;
    }
}
