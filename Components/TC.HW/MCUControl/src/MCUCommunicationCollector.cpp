#include <QMutexLocker>
#include <QList>

#include "MCUCommunicationObserver.h"
#include "MCUCommunicationCollector.h"

namespace TC::MCUControl {
    struct MCUCommunicationCollector::Impl {
        QList<MCUCommunicationObserver*> list;
        QMutex mutex;
    };

    MCUCommunicationCollector::MCUCommunicationCollector() : d{ new Impl } {
    }

    MCUCommunicationCollector::~MCUCommunicationCollector() {
    }

    auto MCUCommunicationCollector::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCUCommunicationCollector() };
        return theInstance;
    }

    auto MCUCommunicationCollector::AddSendPacket(const QByteArray& packet) -> void {
        QMutexLocker locker(&d->mutex);
        for (auto observer : d->list) {
            observer->AddSendPacket(packet);
        }
    }

    auto MCUCommunicationCollector::AddRecvPacket(const QByteArray& packet) -> void {
        QMutexLocker locker(&d->mutex);
        for (auto observer : d->list) {
            observer->AddRecvPacket(packet);
        }
    }

    auto MCUCommunicationCollector::Register(MCUCommunicationObserver* observer) -> void {
        QMutexLocker locker(&d->mutex);
        d->list.push_back(observer);
    }

    auto MCUCommunicationCollector::Deregister(MCUCommunicationObserver* observer) -> void {
        QMutexLocker locker(&d->mutex);
        d->list.removeOne(observer);
    }
}
