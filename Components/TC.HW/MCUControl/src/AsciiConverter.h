#pragma once
#include <memory>
#include <QByteArray>

#include "MCUDefines.h"

namespace TC::MCUControl {
    class AsciiConverter {
    public:
        AsciiConverter();
        virtual ~AsciiConverter();

        static auto toAscii(int32_t value, int bytes = 2)->QByteArray;
        static auto toNum(const QByteArray& bytes)->int32_t;
        static auto toNum8Bit(const QByteArray& bytes)->int32_t;
        static auto toNum16Bit(const QByteArray& bytes)->int32_t;
    };
}