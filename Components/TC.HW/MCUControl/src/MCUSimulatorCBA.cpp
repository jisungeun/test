#include "MCUFactory.h"
#include "MCUCommSimul.h"
#include "MCUSimulatorCBAControl.h"
#include "MCUSimulatorCBA.h"
#include "ui_MCUSimulatorCBA.h"

namespace TC::MCUControl {
    struct MCUSimulatorCBA::Impl {
        Ui::MCUSimulatorCBA ui;
        MCUSimulatorCBAControl control;
    };

    MCUSimulatorCBA::MCUSimulatorCBA(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);

        auto states = d->control.GetStateList();
        for (auto idx = 0; idx < states.length(); idx++) {
            auto str = QString("S%1 %2").arg(idx).arg(states.at(idx));
            d->ui.stateCBox->addItem(str, idx);
        }

        auto comm = qobject_cast<MCUCommSimul*>(MCUFactory::CreateControl()->GetCommPort());
        connect(comm, SIGNAL(sigWritten(const QByteArray&)), this, SLOT(onReceived(const QByteArray&)));
        connect(this, SIGNAL(sigSend(const QByteArray&)), comm, SLOT(onReceived(const QByteArray&)));

        connect(d->ui.stateCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onStateChanged(int)));
        connect(d->ui.xStartChk, SIGNAL(stateChanged(int)), this, SLOT(onXStartChanged(int)));
        connect(d->ui.yStartChk, SIGNAL(stateChanged(int)), this, SLOT(onYStartChanged(int)));
        connect(d->ui.zStartChk, SIGNAL(stateChanged(int)), this, SLOT(onZStartChanged(int)));
        connect(d->ui.lStartChk, SIGNAL(stateChanged(int)), this, SLOT(onLStartChanged(int)));
        connect(d->ui.xEndChk, SIGNAL(stateChanged(int)), this, SLOT(onXEndChanged(int)));
        connect(d->ui.yEndChk, SIGNAL(stateChanged(int)), this, SLOT(onYEndChanged(int)));
        connect(d->ui.zEndChk, SIGNAL(stateChanged(int)), this, SLOT(onZEndChanged(int)));
        connect(d->ui.lEndChk, SIGNAL(stateChanged(int)), this, SLOT(onLEndChanged(int)));
        connect(d->ui.xMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onXMovingChanged(int)));
        connect(d->ui.yMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onYMovingChanged(int)));
        connect(d->ui.zMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onZMovingChanged(int)));
        connect(d->ui.lMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onLMovingChanged(int)));
        connect(d->ui.xPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onXPositionChanged(int)));
        connect(d->ui.yPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onYPositionChanged(int)));
        connect(d->ui.zPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onZPositionChanged(int)));
        connect(d->ui.shutterChk, SIGNAL(stateChanged(int)), this, SLOT(onShutterChanged(int)));
        connect(d->ui.doorChk, SIGNAL(stateChanged(int)), this, SLOT(onDoorChanged(int)));
        connect(d->ui.led1Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED1Changed(int)));
        connect(d->ui.led2Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED2Changed(int)));
        connect(d->ui.led3Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED3Changed(int)));
        connect(d->ui.led4Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED4Changed(int)));
        connect(d->ui.xAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onXAngleChanged(double)));
        connect(d->ui.yAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onYAngleChanged(double)));
        connect(d->ui.zAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onZAngleChanged(double)));
        connect(d->ui.temp1Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp1Changed(double)));
        connect(d->ui.temp2Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp2Changed(double)));
        connect(d->ui.temp3Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp3Changed(double)));
        connect(d->ui.temp4Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp4Changed(double)));
    }

    MCUSimulatorCBA::~MCUSimulatorCBA() {
    }

    void MCUSimulatorCBA::onReceived(const QByteArray& dataArray) {
        auto macroStep = d->control.GetMacroStep();
        d->ui.macroStep->setValue(macroStep);

        auto packet = d->control.Process(dataArray);
        emit sigSend(packet);
    }

    void MCUSimulatorCBA::onStateChanged(int index) {
        d->control.ChangeState(d->ui.stateCBox->itemData(index).toInt());
    }

    void MCUSimulatorCBA::onXStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onYStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onZStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::L, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onXEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onYEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onZEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::L, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onXMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onYMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onZMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::L, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onXPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::X, value);
    }

    void MCUSimulatorCBA::onYPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::Y, value);
    }

    void MCUSimulatorCBA::onZPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::Z, value);
    }

    void MCUSimulatorCBA::onShutterChanged(int state) {
        d->control.ChangeShutterState(state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onDoorChanged(int state) {
        d->control.ChangeDoorState(state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLED1Changed(int state) {
        d->control.ChangeLEDState(0, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLED2Changed(int state) {
        d->control.ChangeLEDState(1, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLED3Changed(int state) {
        d->control.ChangeLEDState(2, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onLED4Changed(int state) {
        d->control.ChangeLEDState(3, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorCBA::onXAngleChanged(double value) {
        d->control.ChangeAngle(0, value);
    }

    void MCUSimulatorCBA::onYAngleChanged(double value) {
        d->control.ChangeAngle(1, value);
    }

    void MCUSimulatorCBA::onZAngleChanged(double value) {
        d->control.ChangeAngle(2, value);
    }

    void MCUSimulatorCBA::onTemp1Changed(double value) {
        d->control.ChangeTemperature(0, value);
    }

    void MCUSimulatorCBA::onTemp2Changed(double value) {
        d->control.ChangeTemperature(1, value);
    }

    void MCUSimulatorCBA::onTemp3Changed(double value) {
        d->control.ChangeTemperature(2, value);
    }

    void MCUSimulatorCBA::onTemp4Changed(double value) {
        d->control.ChangeTemperature(3, value);
    }
}
