#pragma once
#include <memory>

#include <QDialog>

namespace TC::MCUControl {
    class MCUSimulatorCBA : public QDialog {
        Q_OBJECT
    public:
        MCUSimulatorCBA(QWidget* parent = nullptr);
        virtual ~MCUSimulatorCBA();

    protected slots:
        void onReceived(const QByteArray& dataArray);
        void onStateChanged(int index);
        void onXStartChanged(int state);
        void onYStartChanged(int state);
        void onZStartChanged(int state);
        void onLStartChanged(int state);
        void onXEndChanged(int state);
        void onYEndChanged(int state);
        void onZEndChanged(int state);
        void onLEndChanged(int state);
        void onXMovingChanged(int state);
        void onYMovingChanged(int state);
        void onZMovingChanged(int state);
        void onLMovingChanged(int state);
        void onXPositionChanged(int value);
        void onYPositionChanged(int value);
        void onZPositionChanged(int value);
        void onShutterChanged(int state);
        void onDoorChanged(int state);
        void onLED1Changed(int state);
        void onLED2Changed(int state);
        void onLED3Changed(int state);
        void onLED4Changed(int state);
        void onXAngleChanged(double value);
        void onYAngleChanged(double value);
        void onZAngleChanged(double value);
        void onTemp1Changed(double value);
        void onTemp2Changed(double value);
        void onTemp3Changed(double value);
        void onTemp4Changed(double value);

    signals:
        void sigSend(const QByteArray& data);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}