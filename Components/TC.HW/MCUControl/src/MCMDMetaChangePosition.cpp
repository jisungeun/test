#include "MCMDMetaChangePosition.h"

namespace TC::MCUControl {
    MCMDMetaChangePosition::MCMDMetaChangePosition() {
        RegisterParameter("ZOffset", "Pulse");
    }

    MCMDMetaChangePosition::~MCMDMetaChangePosition() {
    }

    auto MCMDMetaChangePosition::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCMDMetaChangePosition() };
        return theInstance;
    }
}