#include <QList>
#include <QSettings>
#include <QFile>

#include "MCUMotionCommandRepository.h"
#include "MCMDIO.h"
#include "MCMDRepoIO.h"

#define GROUP_MOTION_COMMANDS   "Motion_Commands"

namespace TC::MCUControl {
    struct MCMDRepoIO::Impl {
        MCUError error;
    };

    MCMDRepoIO::MCMDRepoIO() : d{ new Impl } {
    }

    MCMDRepoIO::~MCMDRepoIO() {
    }

    auto MCMDRepoIO::Save(const QString& path) -> bool {
        auto repo = MCUMotionCommandRepository::GetInstance();
        const auto counts = repo->GetCounts();

        QSettings qs(path, QSettings::IniFormat);
        MCMDIO io;

        qs.clear();
        qs.beginWriteArray(GROUP_MOTION_COMMANDS);
        for (uint32_t idx = 0; idx < counts; idx++) {
            qs.setArrayIndex(idx);

            auto cmd = repo->CloneCommandByIndex(idx);
            if (!io.Save(cmd, qs)) {
                auto error = io.GetLastError();
                if (error != Error::MCU_NOERROR) d->error = error;
                else {
                    d->error = MCUError(Error::MCU_FAILED_SAVING_MOTIONCOMMANDS, tr("Saving path: %1").arg(path));
                }
                return false;
            }
        }
        qs.endArray();

        return true;
    }

    auto MCMDRepoIO::Load(const QString& path) -> bool {
        if (!QFile::exists(path)) {
            d->error = MCUError(Error::MCU_NO_MOTIONCOMMANDS_FILE, tr("File path: %1").arg(path));
            return false;
        }

        auto repo = MCUMotionCommandRepository::GetInstance();

        QList<std::shared_ptr<IMCUMotionCommand>> list;

        QSettings qs(path, QSettings::IniFormat);
        MCMDIO io;

        const auto counts = qs.beginReadArray(GROUP_MOTION_COMMANDS);
        for (int idx = 0; idx < counts; idx++) {
            qs.setArrayIndex(idx);

            IMCUMotionCommand::Pointer cmd;
            if (!io.Load(cmd, qs)) {
                auto error = io.GetLastError();
                if (error != Error::MCU_NOERROR) d->error = error;
                else {
                    d->error = MCUError(Error::MCU_FAILED_LOADING_MOTIONCOMMANDS, tr("File path: %1").arg(path));
                }
                return false;
            }

            list.push_back(cmd);
        }
        qs.endArray();

        repo->Clear();
        for (auto cmd : list) {
            repo->InsertCommand(cmd);
        }

        return true;
    }

    auto MCMDRepoIO::GetLastError() const -> MCUError {
        return d->error;
    }
}
