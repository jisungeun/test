#include <QList>

#include "MCUMacroRepoIO.h"
#include "MCUMacroRepository.h"

namespace TC::MCUControl {
    struct MCUMacroRepository::Impl {
        QList<MCUMacroGroup::Pointer> macros;
        MCUError error;
    };

    MCUMacroRepository::MCUMacroRepository() : d{ new Impl } {
    }

    MCUMacroRepository::~MCUMacroRepository() {
    }

    auto MCUMacroRepository::GetInstance() -> Pointer {
        static MCUMacroRepository::Pointer theInstance{ new MCUMacroRepository() };
        return theInstance;
    }

    auto MCUMacroRepository::InsertMacroGroup(MCUMacroGroup::Pointer& macroGroup) -> void {
        d->macros.push_back(macroGroup);
    }

    auto MCUMacroRepository::CreateMacroGroup(const QString& title) -> MCUMacroGroup::Pointer {
        uint32_t groupID = 0;
        for (auto macro : d->macros) {
            if (macro->GetGroupID() >= groupID) {
                groupID = macro->GetGroupID() + 1;
            }
        }

        MCUMacroGroup::Pointer group{ new MCUMacroGroup() };
        group->SetGroupID(groupID);
        group->SetTitle(title);
        d->macros.push_back(group);

        return group;
    }

    auto MCUMacroRepository::CloneMacroGroup(MCUMacroGroup::Pointer& macroGroup) -> MCUMacroGroup::Pointer {
        uint32_t groupID = 0;
        for (auto macro : d->macros) {
            if (macro->GetGroupID() >= groupID) {
                groupID = macro->GetGroupID() + 1;
            }
        }

        MCUMacroGroup::Pointer group{ new MCUMacroGroup(*macroGroup) };
        group->SetGroupID(groupID);
        d->macros.push_back(group);

        return group;
    }

    auto MCUMacroRepository::GetCounts() const -> uint32_t {
        return d->macros.length();
    }

    auto MCUMacroRepository::GetMacroGroup(uint32_t groupID) -> MCUMacroGroup::Pointer {
        MCUMacroGroup::Pointer macroGroup{ nullptr };
        for (auto macro : d->macros) {
            if (macro->GetGroupID() == groupID) {
                macroGroup = macro;
                break;
            }
        }
        return macroGroup;
    }

    auto MCUMacroRepository::GetMacroGroupByIdx(uint32_t index) -> MCUMacroGroup::Pointer {
        MCUMacroGroup::Pointer macroGroup{ nullptr };
        if (index <= static_cast<uint32_t>(d->macros.length())) {
            macroGroup = d->macros.at(index);
        }
        return macroGroup;
    }

    auto MCUMacroRepository::RemoveMacroGroup(uint32_t groupID) -> void {
        for (auto macro : d->macros) {
            if (macro->GetGroupID() == groupID) {
                d->macros.removeOne(macro);
                return;
            }
        }
    }

    auto MCUMacroRepository::Clear() -> void {
        d->macros.clear();
    }

    auto MCUMacroRepository::Save(const QString& path) -> bool {
        MCUMacroRepoIO io;
        if (!io.Save(path)) {
            d->error = io.GetLastError();
            return false;
        }
        return true;
    }

    auto MCUMacroRepository::Load(const QString& path) -> bool {
        MCUMacroRepoIO io;
        if (!io.Load(path)) {
            d->error = io.GetLastError();
            return false;
        }
        return true;
    }

    auto MCUMacroRepository::GetLastError() const -> MCUError {
        return d->error;
    }
}
