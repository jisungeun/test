#include <QMap>
#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    struct IMCUMotionCommand::Impl {
        uint32_t commandID{ 0 };
        QString title;
        QMap<uint32_t, int32_t> parameters;
    };

    IMCUMotionCommand::IMCUMotionCommand() : d{ new Impl } {
    }

    IMCUMotionCommand::IMCUMotionCommand(uint32_t id) : d{ new Impl } {
        d->commandID = id;
    }

    IMCUMotionCommand::~IMCUMotionCommand() {
    }

    auto IMCUMotionCommand::clone() -> IMCUMotionCommand::Pointer {
        auto instance = CreateInstance();
        instance->d->commandID = d->commandID;
        instance->d->title = d->title;
        instance->d->parameters = d->parameters;
        return instance;
    }

    auto IMCUMotionCommand::GetCommandTypeAsString() const -> QString {
        return GetCommandType()._to_string();
    }

    auto IMCUMotionCommand::SetCommandID(uint32_t id) -> void {
        d->commandID = id;
    }

    auto IMCUMotionCommand::GetCommandID() const -> uint32_t {
        return d->commandID;
    }

    auto IMCUMotionCommand::SetCommandTitle(const QString& title) -> void {
        d->title = title;
    }

    auto IMCUMotionCommand::GetCommandTitle() const -> QString {
        return d->title;
    }

    auto IMCUMotionCommand::GetParameterCount() const -> uint32_t {
        auto metaData = GetMetadata();
        return metaData->GetParameterCount();
    }

    auto IMCUMotionCommand::SetParameter(uint32_t index, int value) -> bool {
        auto metaData = GetMetadata();
        if (index >= metaData->GetParameterCount()) return false;
        d->parameters[index] = value;
        return true;
    }

    auto IMCUMotionCommand::GetParameter(uint32_t index) const -> int32_t {
        if (d->parameters.contains(index)) return d->parameters[index];
        return 0;
    }

    auto IMCUMotionCommand::GetParameters() const -> QList<int32_t> {
        return d->parameters.values();
    }

    auto IMCUMotionCommand::GetParameterName(uint32_t index) const -> QString {
        auto metaData = GetMetadata();
        return metaData->GetParameterName(index);
    }

    auto IMCUMotionCommand::GetParameterNames() const -> QStringList {
        auto metaData = GetMetadata();
        return metaData->GetParameterNames();
    }

    auto IMCUMotionCommand::GetParameterUnit(uint32_t index) const -> QString {
        auto metaData = GetMetadata();
        return metaData->GetParameterUnit(index);
    }

    auto IMCUMotionCommand::GetParameterUnits() const -> QStringList {
        auto metaData = GetMetadata();
        return metaData->GetParameterUnits();
    }

    auto IMCUMotionCommand::CheckName(uint32_t index, const QString& name) -> bool {
        auto metaData = GetMetadata();
        return name.compare(metaData->GetParameterName(index), Qt::CaseInsensitive) == 0;
    }

    auto IMCUMotionCommand::CheckUnit(uint32_t index, const QString& unit) -> bool {
        auto metaData = GetMetadata();
        return unit.compare(metaData->GetParameterUnit(index), Qt::CaseInsensitive) == 0;
    }
}
