#pragma once
#include <memory>

#include "IMCUMotionCommandMeta.h"

namespace TC::MCUControl {
    class MCMDMetaTriggerWithMotion : public IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<MCMDMetaTriggerWithMotion> Pointer;

    protected:
        MCMDMetaTriggerWithMotion();

    public:
        ~MCMDMetaTriggerWithMotion();

        static auto GetInstance()->Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}