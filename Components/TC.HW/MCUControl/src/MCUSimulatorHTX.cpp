#include "MCUFactory.h"
#include "MCUCommSimul.h"
#include "MCUSimulatorHTXControl.h"
#include "MCUSimulatorHTX.h"
#include "ui_MCUSimulatorHTX.h"

namespace TC::MCUControl {
    struct MCUSimulatorHTX::Impl {
        Ui::MCUSimulatorHTX ui;
        MCUSimulatorHTXControl control;
    };

    MCUSimulatorHTX::MCUSimulatorHTX(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);

        auto states = d->control.GetStateList();
        for (auto idx = 0; idx < states.length(); idx++) {
            auto str = QString("S%1 %2").arg(idx).arg(states.at(idx));
            d->ui.stateCBox->addItem(str, idx);
        }

        auto comm = qobject_cast<MCUCommSimul*>(MCUFactory::CreateControl()->GetCommPort());
        connect(comm, SIGNAL(sigWritten(const QByteArray&)), this, SLOT(onReceived(const QByteArray&)));
        connect(this, SIGNAL(sigSend(const QByteArray&)), comm, SLOT(onReceived(const QByteArray&)));

        connect(d->ui.stateCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onStateChanged(int)));
        connect(d->ui.xStartChk, SIGNAL(stateChanged(int)), this, SLOT(onXStartChanged(int)));
        connect(d->ui.yStartChk, SIGNAL(stateChanged(int)), this, SLOT(onYStartChanged(int)));
        connect(d->ui.zStartChk, SIGNAL(stateChanged(int)), this, SLOT(onZStartChanged(int)));
        connect(d->ui.uStartChk, SIGNAL(stateChanged(int)), this, SLOT(onUStartChanged(int)));
        connect(d->ui.vStartChk, SIGNAL(stateChanged(int)), this, SLOT(onVStartChanged(int)));
        connect(d->ui.wStartChk, SIGNAL(stateChanged(int)), this, SLOT(onWStartChanged(int)));
        connect(d->ui.xEndChk, SIGNAL(stateChanged(int)), this, SLOT(onXEndChanged(int)));
        connect(d->ui.yEndChk, SIGNAL(stateChanged(int)), this, SLOT(onYEndChanged(int)));
        connect(d->ui.zEndChk, SIGNAL(stateChanged(int)), this, SLOT(onZEndChanged(int)));
        connect(d->ui.uEndChk, SIGNAL(stateChanged(int)), this, SLOT(onUEndChanged(int)));
        connect(d->ui.vEndChk, SIGNAL(stateChanged(int)), this, SLOT(onVEndChanged(int)));
        connect(d->ui.wEndChk, SIGNAL(stateChanged(int)), this, SLOT(onWEndChanged(int)));
        connect(d->ui.xMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onXMovingChanged(int)));
        connect(d->ui.yMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onYMovingChanged(int)));
        connect(d->ui.zMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onZMovingChanged(int)));
        connect(d->ui.uMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onUMovingChanged(int)));
        connect(d->ui.vMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onVMovingChanged(int)));
        connect(d->ui.wMoveChk, SIGNAL(stateChanged(int)), this, SLOT(onWMovingChanged(int)));
        connect(d->ui.xPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onXPositionChanged(int)));
        connect(d->ui.yPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onYPositionChanged(int)));
        connect(d->ui.zPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onZPositionChanged(int)));
        connect(d->ui.uPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onUPositionChanged(int)));
        connect(d->ui.vPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onVPositionChanged(int)));
        connect(d->ui.wPosSpin, SIGNAL(valueChanged(int)), this, SLOT(onWPositionChanged(int)));
        connect(d->ui.led1Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED1Changed(int)));
        connect(d->ui.led2Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED2Changed(int)));
        connect(d->ui.led3Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED3Changed(int)));
        connect(d->ui.led4Chk, SIGNAL(stateChanged(int)), this, SLOT(onLED4Changed(int)));
        connect(d->ui.xAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onXAngleChanged(double)));
        connect(d->ui.yAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onYAngleChanged(double)));
        connect(d->ui.zAngleSpin, SIGNAL(valueChanged(double)), this, SLOT(onZAngleChanged(double)));
        connect(d->ui.temp1Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp1Changed(double)));
        connect(d->ui.temp2Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp2Changed(double)));
        connect(d->ui.temp3Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp3Changed(double)));
        connect(d->ui.temp4Spin, SIGNAL(valueChanged(double)), this, SLOT(onTemp4Changed(double)));
        connect(d->ui.afOnChk, SIGNAL(stateChanged(int)), this, SLOT(onAfOnChanged(int)));
        connect(d->ui.afErrorChk, SIGNAL(stateChanged(int)), this, SLOT(onAfErrorChanged(int)));
        connect(d->ui.streamingBufferSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(onStreamingBufferSizeChanged(int)));
        connect(d->ui.streamingIndexSpin, SIGNAL(valueChanged(int)), this, SLOT(onStreamingIndex(int)));
        connect(d->ui.streamingErrorChk, SIGNAL(stateChanged(int)), this, SLOT(onStreamingErrorChanged(int)));
    }

    MCUSimulatorHTX::~MCUSimulatorHTX() {
    }

    void MCUSimulatorHTX::onReceived(const QByteArray& dataArray) {
        auto macroStep = d->control.GetMacroStep();
        d->ui.macroStep->setValue(macroStep);

        auto packet = d->control.Process(dataArray);
        emit sigSend(packet);
    }

    void MCUSimulatorHTX::onStateChanged(int index) {
        d->control.ChangeState(d->ui.stateCBox->itemData(index).toInt());
    }

    void MCUSimulatorHTX::onXStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onYStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onZStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onUStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::U, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onVStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::V, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onWStartChanged(int state) {
        d->control.ChangeAxisStartState(Axis::W, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onXEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onYEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onZEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onUEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::U, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onVEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::V, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onWEndChanged(int state) {
        d->control.ChangeAxisEndState(Axis::W, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onXMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::X, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onYMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::Y, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onZMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::Z, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onUMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::U, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onVMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::V, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onWMovingChanged(int state) {
        d->control.ChangeAxisMoveState(Axis::W, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onXPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::X, value);
    }

    void MCUSimulatorHTX::onYPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::Y, value);
    }

    void MCUSimulatorHTX::onZPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::Z, value);
    }

    void MCUSimulatorHTX::onUPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::U, value);
    }

    void MCUSimulatorHTX::onVPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::V, value);
    }

    void MCUSimulatorHTX::onWPositionChanged(int value) {
        d->control.ChangeAxisPosition(Axis::W, value);
    }

    void MCUSimulatorHTX::onLED1Changed(int state) {
        d->control.ChangeLEDState(0, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onLED2Changed(int state) {
        d->control.ChangeLEDState(1, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onLED3Changed(int state) {
        d->control.ChangeLEDState(2, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onLED4Changed(int state) {
        d->control.ChangeLEDState(3, state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onXAngleChanged(double value) {
        d->control.ChangeAngle(0, value);
    }

    void MCUSimulatorHTX::onYAngleChanged(double value) {
        d->control.ChangeAngle(1, value);
    }

    void MCUSimulatorHTX::onZAngleChanged(double value) {
        d->control.ChangeAngle(2, value);
    }

    void MCUSimulatorHTX::onTemp1Changed(double value) {
        d->control.ChangeTemperature(0, value);
    }

    void MCUSimulatorHTX::onTemp2Changed(double value) {
        d->control.ChangeTemperature(1, value);
    }

    void MCUSimulatorHTX::onTemp3Changed(double value) {
        d->control.ChangeTemperature(2, value);
    }

    void MCUSimulatorHTX::onTemp4Changed(double value) {
        d->control.ChangeTemperature(3, value);
    }

    void MCUSimulatorHTX::onAfOnChanged(int state) {
        d->control.ChangeAfState(state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onAfErrorChanged(int state) {
        d->control.ChangeAfErrorState(state == Qt::CheckState::Checked);
    }

    void MCUSimulatorHTX::onStreamingBufferSizeChanged(int value) {
        d->control.SetStreamingBufferSize(value);
    }

    void MCUSimulatorHTX::onStreamingIndex(int value) {
        d->control.SetStreamingIndex(value);
    }

    void MCUSimulatorHTX::onStreamingErrorChanged(int state) {
        d->control.SetStreamingError(state == Qt::CheckState::Checked);
    }
}
