#include <QSettings>

#include "MCUMacroIO.h"

#define GROUP_MACRO_macros      "Macros"

#define FIELD_ID                "ID"
#define FIELD_TITLE             "Title"
#define FIELD_COMMAND           "Command"
#define FIELD_XPOS              "X_Position"
#define FIELD_YPOS              "Y_Position"
#define FIELD_ZPOS              "Z_Position"
#define FIELD_AFMODE            "AF_Mode"

namespace TC::MCUControl {
    class MCUMacroIOException : public std::exception {
    public:
        MCUMacroIOException(const QString& msg) : message(msg.toStdString()) {
        }

        char const* what() const override {
            return message.c_str();
        }

    private:
        std::string message;
    };

    struct MCUMacroIO::Impl {
        MCUError error;
    };

    MCUMacroIO::MCUMacroIO() :d{ new Impl } {
    }

    MCUMacroIO::~MCUMacroIO() {
    }

    auto MCUMacroIO::Save(MCUMacroGroup::Pointer& macros, QSettings& qs) -> bool {
        qs.setValue(FIELD_ID, macros->GetGroupID());
        qs.setValue(FIELD_TITLE, macros->GetTitle());

        const auto counts = macros->GetCount();
        qs.beginWriteArray(GROUP_MACRO_macros);
        for (uint32_t idx = 0; idx < counts; idx++) {
            qs.setArrayIndex(idx);

            auto macro = macros->GetMacro(idx);
            qs.setValue(FIELD_COMMAND, macro->GetMotionCommand());
            qs.setValue(FIELD_XPOS, macro->GetPositionX());
            qs.setValue(FIELD_YPOS, macro->GetPositionY());
            qs.setValue(FIELD_ZPOS, macro->GetPositionZ());
            qs.setValue(FIELD_AFMODE, macro->GetAFMode());
        }
        qs.endArray();

        return true;
    }

    auto MCUMacroIO::Load(MCUMacroGroup::Pointer& macroGroup, QSettings& qs) -> bool {
        auto readField = [&](const QString& field)->QVariant {
            auto value = qs.value(field);
            if (value.isNull()) {
                throw MCUMacroIOException(tr("A field is not exist - %1").arg(field));
            }
            return value;
        };

        try {
            auto groupID = readField(FIELD_ID).toUInt();
            auto title = readField(FIELD_TITLE).toString();

            QList<MCUMacro::Pointer> macros;
            const auto counts = qs.beginReadArray(GROUP_MACRO_macros);
            for (auto idx = 0; idx < counts; idx++) {
                qs.setArrayIndex(idx);

                MCUMacro::Pointer cmd{ new MCUMacro() };
                cmd->SetMotionCommand(readField(FIELD_COMMAND).toUInt());
                cmd->SetPosition(readField(FIELD_XPOS).toInt(), readField(FIELD_YPOS).toInt(), readField(FIELD_ZPOS).toInt());
                cmd->SetAFMode(qs.value(FIELD_AFMODE, 0).toInt());  //To support old versions
                macros.push_back(cmd);
            }
            qs.endArray();

            macroGroup.reset(new MCUMacroGroup());
            macroGroup->SetGroupID(groupID);
            macroGroup->SetTitle(title);

            macroGroup->ClearMacros();
            for (auto cmd : macros) {
                macroGroup->AddMacro(cmd);
            }
        } catch (MCUMacroIOException& ex) {
            d->error = MCUError(Error::MCU_NO_FIELD_ON_MACROCOMMAND, ex.what());
            return false;
        }

        return true;
    }

    auto MCUMacroIO::GetLastError() const -> MCUError {
        return d->error;
    }
}
