#pragma once
#include <memory>
#include <QCoreApplication>
#include <QString>

#include "MCUError.h"

namespace TC::MCUControl {
    class MCMDRepoIO {
        Q_DECLARE_TR_FUNCTIONS(MCMDRepoIO)

    public:
        MCMDRepoIO();
        virtual ~MCMDRepoIO();

        auto Save(const QString& path)->bool;
        auto Load(const QString& path)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}