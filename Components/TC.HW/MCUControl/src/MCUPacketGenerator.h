#pragma once
#include <memory>
#include <QByteArray>
#include <QList>

#include "MCUError.h"
#include "MCUDefines.h"

namespace TC::MCUControl {
    class MCUPacketGenerator {
    public:
        MCUPacketGenerator();
        virtual ~MCUPacketGenerator();

        static auto Generate(Command cmd, const QList<int32_t>& params = QList<int32_t>())->QByteArray;
        static auto Generate(const QString& cmd, const QString& params = QString())->QByteArray;
        static auto Generate(ResponseCode respCode, const QList<int32_t>& params)->QByteArray;
        static auto Generate(ResponseCode respCode, const QByteArray& params)->QByteArray;
    };
}