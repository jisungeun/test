#include "MCUCommunicationCollector.h"
#include "MCUCommunicationObserver.h"

namespace TC::MCUControl {
    struct MCUCommunicationObserver::Impl {
        QList<std::shared_ptr<Item>> list;
    };

    MCUCommunicationObserver::MCUCommunicationObserver(QObject* parent) : QObject(parent), d{ new Impl } {
        MCUCommunicationCollector::GetInstance()->Register(this);
    }

    MCUCommunicationObserver::~MCUCommunicationObserver() {
        MCUCommunicationCollector::GetInstance()->Deregister(this);
    }

    auto MCUCommunicationObserver::AddSendPacket(const QByteArray& packet) -> void {
        std::shared_ptr<Item> item{ new Item };
        item->packet = packet;
        item->sendPacket = true;
        d->list.push_back(item);

        emit sigUpdated();
    }

    auto MCUCommunicationObserver::AddRecvPacket(const QByteArray& packet) -> void {
        std::shared_ptr<Item> item{ new Item };
        item->packet = packet;
        item->sendPacket = false;
        d->list.push_back(item);

        emit sigUpdated();
    }

    auto MCUCommunicationObserver::GetNext() -> std::shared_ptr<Item> {
        if (d->list.isEmpty()) return nullptr;
        auto item = d->list.first();
        d->list.removeFirst();
        return item;
    }
}
