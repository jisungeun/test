#define LOGGER_TAG "[MCU]"
#include <QSerialPort>
#include <QThread>

#include <TCLogger.h>

#include "AsciiConverter.h"
#include "MCUCommSimul.h"
#include "MCUSimulatorHTX.h"
#include "MCUCommSerial.h"
#include "MCUPacketGenerator.h"
#include "MCUPacketParser.h"
#include "MCUResponseParserHTX.h"
#include "MCUCommunicationCollector.h"
#include "MCUControlHTX.h"

namespace TC::MCUControl {
    struct MCUControlHTX::Impl {
        MCUComm* port{ nullptr };
        bool simulation{ false };
        MCUSimulatorHTX* simulator{ nullptr };

        inline auto axis2num(Axis axis)->int32_t {
            int32_t num = 0;

            switch (axis) {
            case Axis::X:
                num = 0;
                break;
            case Axis::Y:
                num = 1;
                break;
            case Axis::Z:
                num = 2;
                break;
            case Axis::L:
                num = 3;
                break;
            case Axis::U:
                num = 4;
                break;
            case Axis::V:
                num = 5;
                break;
            case Axis::W:
                num = 6;
            }

            return num;
        }
    };

    MCUControlHTX::MCUControlHTX(bool simulation) : IMCUControl(simulation), d{ new Impl } {
        d->simulation = simulation;
    }

    MCUControlHTX::~MCUControlHTX() {
    }

    auto MCUControlHTX::OpenPort() -> bool {
        auto& config = GetConfig();

        if (d->simulation) {
            d->port = new MCUCommSimul();
            d->simulator = new MCUSimulatorHTX();
            d->simulator->show();
        } else {
            MCUCommParam param;
            param.Set("Port", config.GetPort());
            param.Set("Baudrate", config.GetBaudrate());

            d->port = new MCUCommSerial();
            d->port->SetParameter(param);

            if (!d->port->OpenPort()) {
                SetLastError(MCUError(Error::MCU_FAILED_OPEN_PORT, tr("Port: COM%1").arg(config.GetPort())));
                return false;
            }
        }

        QLOG_INFO() << "Port is opened";

        return true;
    }

    auto MCUControlHTX::StartInitialization() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StartInitialization);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_START_INITIALIZATION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Initialization is started";

        return true;
    }

    auto MCUControlHTX::CheckInitialization(int32_t& progress) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::CheckInitializationProgress);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_START_INITIALIZATION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        progress = params.at(1);

        //To reduce log messages
        static int32_t lastProgress{ -1 };
        if(progress != lastProgress) {
            QLOG_INFO() << "Initialization progress : " << progress;;
            lastProgress = progress;
        };

        return true;
    }

    auto MCUControlHTX::SetSafeZPosition(int32_t position) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetSafeZPosition, { position });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_SET_SAFEZPOSITION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set SafeZPosition = " << position;

        return true;
    }

    auto MCUControlHTX::SetCommand(int32_t command, int32_t type, const QList<int32_t>& cmdParams) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        QList<int32_t> inParams{ command, type };
        inParams.append(cmdParams);

        auto packet = MCUPacketGenerator::Generate(Command::SetCommand, inParams);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_SET_MOTIONCOMMAND, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        const auto cmdParamsStr = [=]()->QString {
            QString str;
            for (auto param : cmdParams) {
                str.append(QString::number(param));
                str.append(",");
            }
            str.remove(str.length() - 1, 1);
            return str;
        }();

        QLOG_INFO() << "Motion command is updated. (cmd=" << command << " type=" << type << " params=" << cmdParamsStr << ")";

        return true;
    }

    auto MCUControlHTX::SetSWLimitPosition(const QList<int32_t>& limits) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetSoftwareLimitPosition, limits);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_SW_LIMIT, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        const auto strLimits = [=]()->QString {
            QString str;
            for (auto limit : limits) {
                str.append(QString::number(limit));
                str.append(",");
            }
            str.remove(str.length() - 1, 1);
            return str;
        }();

        QLOG_INFO() << "Set software limit. (" << strLimits << ")";

        return true;
    }

    auto MCUControlHTX::SetInPositionBand(const QList<int32_t>& bands) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetEncoderInBand, bands);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_INPOSITION_BAND, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        const auto strBands = [=]()->QString {
            QString str;
            for (auto band : bands) {
                str.append(QString::number(band));
                str.append(",");
            }
            str.remove(str.length() - 1, 1);
            return str;
        }();

        QLOG_INFO() << "Set in-position band. (" << strBands << ")";

        return true;
    }

    auto MCUControlHTX::AddMacroPoint(int32_t group, int32_t index, int32_t command, 
                                      int32_t xPosition, int32_t yPosition, int32_t zPosition, int32_t afMode) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::AddMacroPoint, 
                                                   {group, index, command, xPosition, yPosition, zPosition, afMode});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_ADD_MACROPOINT, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "MacroPoint is added. (group=" << group << " index=" << index << " command=" << 
            command << " xpos=" << xPosition << " ypos=" << yPosition << " zpos=" << zPosition << " afMode=" << afMode << ")";

        return true;
    }

    auto MCUControlHTX::ClearMacro(int32_t group) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ClearMacro, { group });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_CLEAR_MACRO, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Macro is cleared. (group=" << group << ")";

        return true;
    }

    auto MCUControlHTX::RunMacro(int32_t group) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::RunMacro, { group });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_RUN_MACRO, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Macro is started. (group=" << group << ")";

        return true;
    }

    auto MCUControlHTX::StopMacro() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StopMacro);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_STOP_MACRO, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Macro is stopped.";

        return true;
    }

    auto MCUControlHTX::CheckMacroStatus(Flag& status, uint32_t& step) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::CheckMacroStatus);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_CHECK_MACROSTATUS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        status = (params.at(1) == 0) ? Flag::Idle : Flag::Running;
        step = params.at(2);

        //To reduce # of log messages....
        static Flag lastStatus{ Flag::Running };
        static uint32_t lastStep{ 1000000000 };
        if((status != lastStatus) || (step !=lastStep)) {
            QLOG_INFO() << "Macro is running=" << status._to_string() << " and Step=" << step;
            lastStatus = status;
            lastStep = step;
        }

        return true;
    }

    auto MCUControlHTX::SendMacrosToStreamBuffer(const QList<QList<int32_t>>& commands) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        for(const auto& command : commands) {
            auto packet = MCUPacketGenerator::Generate(Command::AddMacroToBuffer, command);
            if (!Send(packet, params, respCode)) {
                return false;
            }

            if (respCode != +ResponseCode::Success) {
                SetLastError(MCUError(Error::MCU_FAILED_TO_START_MACROSTREAMING, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
                return false;
            }
        }

        QLOG_INFO() << commands.size() << " motion commands are added to macro streaming buffer";

        return true;
    }

    auto MCUControlHTX::StartMacroStreaming() -> bool {
        MCUResponse resp;
        if(!CheckStatus(resp)) return false;

        if(resp.GetState() == +MCUState::Testing) return true;

        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StartMacroStreaming);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_START_MACROSTREAMING, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Macro streaming is started";

        return true;
    }

    auto MCUControlHTX::StopMacroStreaming() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StopMacroStreaming);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_STOP_MACROSTREAMING, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Macro streaming is stopped";

        return true;
    }

    auto MCUControlHTX::Move(Axis axis, int32_t position) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::Move, { d->axis2num(axis), position });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_MOVEAXIS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Axis (" << axis._to_string() << ") is moving to " << position << "pulses";

        return true;
    }

    auto MCUControlHTX::SetFilterPositions(const QList<int32_t>& positions) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetFilterPosition, positions);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_FILTERPOSITIONS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set filter positions = " << positions;

        return true;
    }

    auto MCUControlHTX::ChangeFilter(int32_t channel) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ChangeFilter, {channel});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_CHANGE_FILTER, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Change filter to " << channel;

        return true;
    }

    auto MCUControlHTX::MoveToLoadingReadyPosition() -> bool {
        QLOG_ERROR() << "It is not implemented";
        return false;
    }

    auto MCUControlHTX::MoveToSafeZPos() -> bool {
        auto& config = GetConfig();
        if(!Move(Axis::Z, config.GetSafeZPosition())) return false;

        QLOG_INFO() << "It is moving to SafeZPosition";
        return true;
    }

    auto MCUControlHTX::GenerateTrigger(int32_t pulseWidth, int32_t interval, int32_t count) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::GenerateTrigger, { pulseWidth, interval, count });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_GENERATE_TRIGGER, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Trigger is generated with (width= " << pulseWidth << " interval=" << interval << " count=" << count << ")";

        return true;
    }

    auto MCUControlHTX::SetDO(int32_t channel, int32_t value) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetDigitalOutput, { channel, value });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_SETDO, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "DO(" << channel << ") is set to " << value;

        return true;
    }

    auto MCUControlHTX::ReadDI(int32_t channel, Flag& status) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadDigitalInput, { channel });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_GETDI, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        status = (params.at(1) == 0) ? Flag::Low : Flag::High;
        QLOG_INFO() << "DI(" << channel << ") is HIGH=" << status._to_string();

        return true;
    }

    auto MCUControlHTX::MoveXY(int32_t xPosition, int32_t yPosition) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::MoveXY, { xPosition, yPosition });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_MOVEXY, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "It is moving to (" << xPosition << "," << yPosition << ")";

        return true;
    }

    auto MCUControlHTX::StopMotion() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StopMotion);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_STOP_MOTION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Stop motion";

        return true;
    }

    auto MCUControlHTX::LoadCartridge() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::LoadCartridge);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_LOAD_CARTRIDGE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Loading a cartridge";

        return true;
    }

    auto MCUControlHTX::UnloadCartridge() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::UnloadCartridge);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_UNLOAD_CARTRIDGE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Unloading a cartridge";

        return true;
    }

    auto MCUControlHTX::FinishCalibration() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::FinishCalibration);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_FINISH_CALIBRATION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Calibration is finished.";

        return true;
    }

    auto MCUControlHTX::StartTest() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StartTest);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_START_TEST, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Test is started.";

        return true;
    }

    auto MCUControlHTX::FinishTest() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::FinishTest);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_FINISH_TEST, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Test is finished.";

        return true;
    }

    auto MCUControlHTX::Recover() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::Recover);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_RECOVER, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Recovering from error state is requested.";

        return true;
    }

    auto MCUControlHTX::StartManualMode() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::StartManualMode);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_START_MANUALMODE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Manual mode is started.";

        return true;
    }

    auto MCUControlHTX::StopManualMode() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::FinishManualMode);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_FINISH_MANUALMODE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Manual mode is finished.";

        return true;
    }

    auto MCUControlHTX::GetDLPCVersion(int32_t& fw_ver, int32_t& sw_ver) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadDLPCVersion);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_GET_DLPCVERSION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        fw_ver = params.at(1);
        sw_ver = params.at(2);

        QLOG_INFO() << "DLPC Version : Firmware=" << fw_ver << " Software=" << sw_ver;

        return true;

    }

    auto MCUControlHTX::SetLEDIntensity(int32_t red, int32_t green, int32_t blue) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetLEDIntensity, {red, green, blue});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_LEDINTENSITY, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "LED Intensity is changed (R,G,B)=(" << red << "," << green << "," << blue << ")";

        return true;
    }

    auto MCUControlHTX::SetDMDExposure(int32_t exposure, int32_t interval) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetDMDExposure, {exposure, interval});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_DMDEXPOSURE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "DMD Exposure is changed. Exposre=" << exposure << " Interval=" << interval;

        return true;
    }

    auto MCUControlHTX::ReadTriggerCount(int32_t& count) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadTriggerCount);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_GET_TRIGGERCOUNT, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        count = params.at(1);

        QLOG_INFO() << "Trigger count = " << count;

        return true;
    }

    auto MCUControlHTX::ResetTriggerCount() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ResetTriggerCount);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_RESET_TRIGGERCOUNT, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Reset trigger count";

        return true;
    }

    auto MCUControlHTX::SetLEDChannel(int32_t channel) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetLEDChannel, {channel});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_LEDCHANNEL, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set LED Channel: " << channel;

        return true;
    }

    auto MCUControlHTX::SetCameraType(bool enableInternal, bool enableExternal) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetCameraType, {enableInternal, enableExternal});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_CAMERATYE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set Camera Type";

        return true;
    }

    auto MCUControlHTX::SetRGBLedIntensity(int32_t red, int32_t green, int32_t blue) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetRGBControl, {red, green, blue});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_RGBLEDINTENSITY, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set RGB LED Intensity";

        return true;
    }

    auto MCUControlHTX::IsMoving(Axis axis, Flag& status) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::CheckMovingStatus, { d->axis2num(axis) });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_ISMOVING, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        status = (params.at(1) == 0) ? Flag::NotMoving : Flag::Moving;

        QLOG_INFO() << "Axis (" << axis._to_string() << ") is moving=" << status._to_string();

        return true;
    }

    auto MCUControlHTX::CheckStatus(MCUResponse& status) -> bool {
        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::CheckStatus);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
            SetLastError(MCUError(Error::MCU_FAILED_CHECK_STATUS, QString("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
            return false;
        }

        if (!MCUResponseParserHTX::Parse(params, status)) {
            QLOG_ERROR() << "It fails to parse status [Packet=" << params << "]";
            return false;
        }

        //To reduce log messages
        static MCUState lastState{ MCUState::Error };
        if(lastState != status.GetState()) {
            QLOG_INFO() << "MCU State = " << status.GetState()._to_string();
            lastState = status.GetState();
        };

        return true;
    }

    auto MCUControlHTX::GetPosition(Axis axis, int32_t& position) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::GetAxisPosition, { d->axis2num(axis) });
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_CHECK_STATUS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        position = params.at(1);
        QLOG_INFO() << "Axis (" << axis._to_string() << ") is at " << position;

        return true;
    }

    auto MCUControlHTX::GetPositions() -> std::tuple<bool,QMap<Axis,int32_t>> {
        MCUResponse resp;
        if(!CheckStatus(resp)) return std::make_tuple(false, QMap<Axis, int32_t>{});

        QMap<Axis, int32_t> positions;
        positions[Axis::X] = resp.GetValue(Response::AxisXPosition);
        positions[Axis::Y] = resp.GetValue(Response::AxisYPosition);
        positions[Axis::Z] = resp.GetValue(Response::AxisZPosition);
        positions[Axis::U] = resp.GetValue(Response::AxisUPosition);
        positions[Axis::V] = resp.GetValue(Response::AxisVPosition);
        positions[Axis::W] = resp.GetValue(Response::AxisWPosition);
        return std::make_tuple(true, positions);
    }

    auto MCUControlHTX::ReadTemperature(int32_t sensor, float& temperature) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadTemperature, { sensor});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_READ_TEMPERATURE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        temperature = params.at(1) * 0.1;
        QLOG_INFO() << "Temperature (" << sensor << ") is " << temperature << "��C";

        return true;
    }

    auto MCUControlHTX::ReadAcceleration(float& xAngle, float& yAngle, float& zAngle) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadAcceleration);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_READ_ACCELERATION, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        xAngle = params.at(1) * 0.1;
        yAngle = params.at(2) * 0.1;
        zAngle = params.at(3) * 0.1;

        QLOG_INFO() << "Angle is (X=" << xAngle << "��, Y=" << yAngle << "��, Z=" << zAngle << "��)";

        return true;
    }

    auto MCUControlHTX::ReadCartridgeSensor(Flag& inlet, Flag& loading) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadCartridgeSensor);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_READ_CARTRIDGE_SENSOR, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        inlet = (params.at(1) == 0) ? Flag::NotTriggered : Flag::Triggered;
        loading = (params.at(2) == 1) ? Flag::NotTriggered : Flag::Triggered;   //TODO �ӽ� ��ġ

        QLOG_INFO() << "Cartridge inlet senor is triggered= " << inlet._to_string() << ", loading sensor is triggered=" << loading._to_string();

        return true;

    }

    auto MCUControlHTX::ReadMCUFirmwareVersion() -> QString {
        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadFirmwareVersion);
        if (!Send(packet, params, respCode)) {
            return QString();
        }

        if (respCode != +ResponseCode::Success) {
            const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
            SetLastError(MCUError(Error::MCU_FAILED_READ_LOG, tr("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
            return QString();
        }

        const auto ver = params.right(params.length()-8);

        QString str;
        const auto len = ver.length();
        for(int32_t idx=0; idx<(len-1); idx+=2) {
            const auto sub = ver.mid(idx, 2);
            bool ok;
            auto hex = sub.toInt(&ok, 16);
            str.push_back(hex);
        }

        const auto fwVer = str.trimmed();
        QLOG_INFO() << "MCU Firmware: " << fwVer;

        return fwVer;
    }

    auto MCUControlHTX::ReadMCULog() -> QString {
        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadLog);
        if (!Send(packet, params, respCode)) {
            return QString();
        }

        if (respCode != +ResponseCode::Success) {
            const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
            SetLastError(MCUError(Error::MCU_FAILED_READ_LOG, tr("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
            return QString();
        }

        return params.right(params.length()-8);
    }

    auto MCUControlHTX::GetCommPort() -> MCUComm* {
        return d->port;
    }

    auto MCUControlHTX::WriteFlash(const QList<int32_t>& data) -> bool {
        constexpr auto blockSize = 32;

        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        const auto blockCount = data.size() / blockSize;

        for(auto blockIdx=0; blockIdx<blockCount; blockIdx++) {
            const auto start = blockIdx * blockSize;
            const auto count = std::min(blockSize, data.size() - start);
            auto packet = MCUPacketGenerator::Generate(Command::WriteFlashData, QList({blockIdx}) + data.mid(start, count));
            if (!Send(packet, params, respCode)) {
                return false;
            }

            if (respCode != +ResponseCode::Success) {
                const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
                SetLastError(MCUError(Error::MCU_FAILED_TO_WRITE_FLASHDATA, tr("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
                return false;
            }
        }

        QLOG_INFO() << "Write flash data : %d" << data.length();

        return true;
    }

    auto MCUControlHTX::ReadFlash(QList<int32_t>& data) -> bool {
        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        int32_t blockIdx=0;
        while(true) {
            auto packet = MCUPacketGenerator::Generate(Command::ReadFlashData, {blockIdx});
            if (!Send(packet, params, respCode)) {
                return false;
            }

            if (respCode != +ResponseCode::Success) {
                const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
                SetLastError(MCUError(Error::MCU_FAILED_TO_READ_FLASHDATA, tr("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
                return false;
            }

            const auto dataPacket = params.right(params.length()-8);

            const auto len = dataPacket.length();
            if(len == 0) break;

            for(int32_t idx=0; idx<len; idx+=8) {
                data.push_back(AsciiConverter::toNum(dataPacket.mid(idx, 8)));
            }

            blockIdx++;
        }

        QLOG_INFO() << "Read flash data : " << data.length();

        return true;
    }

    auto MCUControlHTX::SetAFParameter(const MCUAFParam& param) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetAFMParameter, 
                                                   {param.GetLensID(),
                                                    param.GetInFocusRange(),
                                                    param.GetDirection(),
                                                    param.GetLoopInterval(),
                                                    param.GetMaxTrialCount(),
                                                    param.GetSensorValueMin(),
                                                    param.GetSensorValueMax(),
                                                    param.GetResolutionMul(),
                                                    param.GetResolutionDiv(),
                                                    param.GetMaxCompensationPulse(),
                                                    param.GetAFLimit()});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_AF_PARAMETER, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set AF Parameter";

        return true;
    }

    auto MCUControlHTX::SetAFTarget(int32_t mode, int32_t value, int32_t& targetOnAFM) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::SetAFTarget, {mode, value});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_AF_TARGET, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        targetOnAFM = params.at(1);

        QLOG_INFO() << "Set AF Target (mode=" << mode << ", value=" << value << ") > Target on AFM (" << targetOnAFM << ")";

        return true;
    }

    auto MCUControlHTX::InitAF() -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::InitAFM);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_T0_INIT_AFM, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Init AFM";

        return true;
    }

    auto MCUControlHTX::CheckAFInitialization(AFInitState& status) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::IsInitAFM);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_CHECK_AFM_INITSTATUS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        switch(params.at(1)) {
        case -1:
            status = AFInitState::Error;
            break;
        case 0:
            status = AFInitState::Finished;
            break;
        case 1:
            status = AFInitState::Initializing;
            break;
        default:
            status = AFInitState::Error;
        };

        QLOG_INFO() << "Init AFM Status = " << status._to_string();

        return true;
    }

    auto MCUControlHTX::EnableAF(int32_t mode) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::EnableAF, {mode});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_ENABLE_AFM, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Enable AFM (mode=" << mode << ")";

        return true;
    }

    auto MCUControlHTX::CheckAFStatus(int32_t& status, int32_t& result, int32_t& trialCount) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::CheckAFStatus);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_CHECK_AF_STATUS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        status = params.at(1);
        result = params.at(2);
        trialCount = params.at(3);

        QLOG_INFO() << "AF Status = " << status << " Result=" << result << " TrialCount=" << trialCount;

        return true;
    }

    auto MCUControlHTX::ReadAFMValue(int32_t& value) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadAFMSensor);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_READ_AFM_VALUE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        value = params.at(1);

        QLOG_INFO() << "AF Value = " << value;

        return true;
    }

    auto MCUControlHTX::SetManualAF(bool laser) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ManualAFControl, {laser?1:0});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_MANUALAF, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set Manual AF [laser = " << laser << "]";

        return true;
    }

    auto MCUControlHTX::ReadAFLog(QList<int32_t>& adcDiff, QList<int32_t>& zComp) -> bool {
        QByteArray params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadAFLog);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            const auto errCode = AsciiConverter::toNum(params.mid(0, 8));
            SetLastError(MCUError(Error::MCU_FAILED_TO_READ_AFLOG, tr("Error code: %1(0x%2)").arg(errCode).arg(errCode,0,16)));
            return false;
        }

        for(int32_t idx=8; idx<params.length(); idx+=8) {
            adcDiff.push_back(AsciiConverter::toNum16Bit(params.mid(idx, 4)));
            zComp.push_back(AsciiConverter::toNum16Bit(params.mid(idx+4, 4)));
        }

        QLOG_INFO() << "Read AF Log";

        return true;
    }

    auto MCUControlHTX::ScanZForAFM(int32_t startPos, int32_t endPos, int32_t interval, int32_t delay) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ScanZForAFM, {startPos, endPos, interval, delay});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SCANZ_FOR_AFM, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Z Scan for AFM [start=" << startPos << ", end=" << endPos << ", interval=" << interval << ", delay=" << delay << "]";

        return true;
    }

    auto MCUControlHTX::DumpZScanForAFM(QList<int32_t>& values) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::DumpScanZForAFM);
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_DUMP_SCANZ_FOR_AFM, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        values = params.mid(1);

        QLOG_INFO() << "Dump ScanZ For AFM";

        return true;
    }

    auto MCUControlHTX::ReadSensorParameter() -> QMap<QString, int32_t> {

        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        auto packet = MCUPacketGenerator::Generate(Command::ReadAFSensorParameter);
        if (!Send(packet, params, respCode)) {
            return {};
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_READ_AFSENSOR_PARAMETERS, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return {};
        }

        if(params.size() != 8) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_READ_AFSENSOR_PARAMETERS, tr("Unexpected number of parameters are returned")));
            return {};
        }

        QMap<QString, int32_t> parameters;
        parameters["cog"] = params.at(1);
        parameters["sig2"] = params.at(2);
        parameters["lpw"] = params.at(3);
        parameters["tret"] = params.at(4);
        parameters["ret"] = params.at(5);
        parameters["tretd"] = params.at(6);
        parameters["tretc"] = params.at(7);

        QLOG_INFO() << "Read AF Sensor Parameters";

        return parameters;
    }

    bool MCUControlHTX::ChangeLensParameter(int32_t param) {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        const auto packet = MCUPacketGenerator::Generate(Command::ChangeAFLensParameter, {param});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_CHANGE_AFLENS_PARAMETER, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Change AF lens parameter = " << param;

        return true;
    }

    auto MCUControlHTX::SetAFMPeakMode(bool peakMode) -> bool {
        QList<int32_t> params;
        ResponseCode respCode{ ResponseCode::Fail };

        const auto packet = MCUPacketGenerator::Generate(Command::SetAFMPeakmode, {peakMode? 1 : 0});
        if (!Send(packet, params, respCode)) {
            return false;
        }

        if (respCode != +ResponseCode::Success) {
            SetLastError(MCUError(Error::MCU_FAILED_TO_SET_AFM_PEAKMODE, tr("Error code: %1(0x%2)").arg(params.at(0)).arg(params.at(0),0,16)));
            return false;
        }

        QLOG_INFO() << "Set AFM Peakmode = " << peakMode;

        return true;
    }

    auto MCUControlHTX::SendPacket(const QByteArray& packet, QList<int32_t>& params, ResponseCode& respCode) -> bool {
        MCUCommunicationCollector::GetInstance()->AddSendPacket(packet);
        QLOG_INFO() << QString::fromStdString(packet.toStdString());

        if(d->port == nullptr) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->IsOpened()) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->write(packet)) {
            SetLastError(MCUError(Error::MCU_FAILED_SENDING_PACKET, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForBytesWritten(200)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_SEND, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        QThread::msleep(200);   //MutliThread Issue

        auto response = d->port->readAll();
        while (d->port->waitForReadyRead(20))
            response += d->port->readAll();

        MCUCommunicationCollector::GetInstance()->AddRecvPacket(response);

        MCUPacketParser parser;
        if (!parser.Parse(response, respCode, params)) {
            SetLastError(parser.GetLastError());
            return false;
        }

        return true;
    }

    auto MCUControlHTX::SendPacket(const QString& cmd, const QString& params, QByteArray& resp, ResponseCode& respCode) -> bool {
        const auto packet = MCUPacketGenerator::Generate(cmd, params);

        MCUCommunicationCollector::GetInstance()->AddSendPacket(packet);
        QLOG_INFO() << QString::fromStdString(packet.toStdString());

        if(d->port == nullptr) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->IsOpened()) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->write(packet)) {
            SetLastError(MCUError(Error::MCU_FAILED_SENDING_PACKET, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForBytesWritten(200)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_SEND, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForReadyRead(200)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_RESPONSE, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        auto response = d->port->readAll();
        while (d->port->waitForReadyRead(20))
            response += d->port->readAll();

        MCUCommunicationCollector::GetInstance()->AddRecvPacket(response);

        resp = response;

        QList<int32_t> respParams;
        MCUPacketParser parser;
        if (!parser.Parse(response, respCode, respParams)) {
            SetLastError(parser.GetLastError());
            return false;
        }

        return true;
    }

    auto MCUControlHTX::Send(const QByteArray& packet, QList<int32_t>& params, ResponseCode& respCode, uint32_t timeout) -> bool {
        MCUCommunicationCollector::GetInstance()->AddSendPacket(packet);
        QLOG_INFO() << QString::fromStdString(packet.toStdString());

        if(d->port == nullptr) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->IsOpened()) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->write(packet)) {
            SetLastError(MCUError(Error::MCU_FAILED_SENDING_PACKET, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForBytesWritten(timeout)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_SEND, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForReadyRead(timeout)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_RESPONSE, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        auto response = d->port->readAll();
        while (d->port->waitForReadyRead(20))
            response += d->port->readAll();

        MCUCommunicationCollector::GetInstance()->AddRecvPacket(response);

        MCUPacketParser parser;
        if (!parser.Parse(response, respCode, params)) {
            SetLastError(parser.GetLastError());
            return false;
        }

        return true;
    }

    auto MCUControlHTX::Send(const QByteArray& packet, QByteArray& params, ResponseCode& respCode,
        uint32_t timeout) -> bool {
        if(d->port == nullptr) return false;

        MCUCommunicationCollector::GetInstance()->AddSendPacket(packet);

        if (!d->port->IsOpened()) {
            SetLastError(MCUError(Error::MCU_PORT_NOT_READY));
            return false;
        }

        if (!d->port->write(packet)) {
            SetLastError(MCUError(Error::MCU_FAILED_SENDING_PACKET, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForBytesWritten(timeout)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_SEND, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        if (!d->port->waitForReadyRead(timeout)) {
            SetLastError(MCUError(Error::MCU_TIMEOUT_FOR_RESPONSE, tr("Packet: %1")
                                  .arg(QString::fromStdString(packet.toStdString()))));
            return false;
        }

        auto response = d->port->readAll();
        while (d->port->waitForReadyRead(20))
            response += d->port->readAll();

        MCUCommunicationCollector::GetInstance()->AddRecvPacket(response);

        MCUPacketParser parser;
        if (!parser.Parse(response, respCode, params)) {
            SetLastError(parser.GetLastError());
            return false;
        }

        return true;
    }
}
