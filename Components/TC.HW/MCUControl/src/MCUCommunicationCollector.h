#pragma once
#include <memory>

#include <QByteArray>

namespace TC::MCUControl {
    class MCUCommunicationObserver;

    class MCUCommunicationCollector {
    public:
        typedef std::shared_ptr<MCUCommunicationCollector> Pointer;

    protected:
        MCUCommunicationCollector();

    public:
        virtual ~MCUCommunicationCollector();

        static auto GetInstance()->Pointer;

        auto AddSendPacket(const QByteArray& packet)->void;
        auto AddRecvPacket(const QByteArray& packet)->void;

    protected:
        auto Register(MCUCommunicationObserver* observer)->void;
        auto Deregister(MCUCommunicationObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class MCUCommunicationObserver;
    };
}