#pragma once
#include <memory>

#include "IMCUMotionCommand.h"

namespace TC::MCUControl {
    class MCMDPatternSequence : public IMCUMotionCommand {
    public:
        MCMDPatternSequence();
        virtual ~MCMDPatternSequence();

        auto GetCommandType() const->MotionCommandType override;

    protected:
        auto GetMetadata() const->IMCUMotionCommandMeta::Pointer override;
        auto CreateInstance()->IMCUMotionCommand::Pointer override;
    };
}