#include <QMap>
#include "MCMDMetaPatternSequence.h"
#include "MCMDPatternSequence.h"

namespace TC::MCUControl {
    MCMDPatternSequence::MCMDPatternSequence() {
    }

    MCMDPatternSequence::~MCMDPatternSequence() {
    }

    auto MCMDPatternSequence::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::PatternSequence;
    }

    auto MCMDPatternSequence::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaPatternSequence::GetInstance();
    }

    auto MCMDPatternSequence::CreateInstance() -> IMCUMotionCommand::Pointer {
        Pointer instance{ new MCMDPatternSequence() };
        return instance;
    }
}
