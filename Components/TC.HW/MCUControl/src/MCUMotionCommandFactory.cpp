#include "MCMDTriggerWithMotion.h"
#include "MCMDTriggerWithStop.h"
#include "MCMDTriggerOnly.h"
#include "MCMDPatternSequence.h"
#include "MCMDChangeFilter.h"
#include "MCMDChangePosition.h"

#include "MCUMotionCommandFactory.h"

namespace TC::MCUControl {
    MCUMotionCommandFactory::MCUMotionCommandFactory() {
    }

    MCUMotionCommandFactory::~MCUMotionCommandFactory() {
    }

    auto MCUMotionCommandFactory::GetInstance() -> Pointer {
        static Pointer theInstance{ new MCUMotionCommandFactory() };
        return theInstance;
    }

    auto MCUMotionCommandFactory::GetCommandTypes() const -> QList<MotionCommandType> {
        QList<MotionCommandType> types;

        for (MotionCommandType value : MotionCommandType::_values()) {
            types.push_back(value);
        }

        return types;
    }

    auto MCUMotionCommandFactory::GetCommandTypesAsString() const -> QStringList {
        QStringList types;

        for (MotionCommandType value : MotionCommandType::_values()) {
            types.push_back(value._to_string());
        }

        return types;
    }

    auto MCUMotionCommandFactory::CreateCommand(MotionCommandType type) -> IMCUMotionCommand::Pointer {
        IMCUMotionCommand::Pointer instance{ nullptr };

        switch (type) {
        case MotionCommandType::TriggerWithMotion:
            instance.reset(new MCMCTriggerWithMotion());
            break;
        case MotionCommandType::TriggerWithStop:
            instance.reset(new MCMCTriggerWithStop());
            break;
        case MotionCommandType::TriggerOnly:
            instance.reset(new MCMCTriggerOnly());
            break;
        case MotionCommandType::PatternSequence:
            instance.reset(new MCMDPatternSequence());
            break;
        case MotionCommandType::ChangeFilter:
            instance.reset(new MCMDChangeFilter());
            break;
        case MotionCommandType::ChangePosition:
            instance.reset(new MCMDChangePosition());
            break;
        }

        return instance;
    }
}
