#include <QMap>
#include "MCMDMetaTriggerOnly.h"
#include "MCMDTriggerOnly.h"

namespace TC::MCUControl {
    MCMCTriggerOnly::MCMCTriggerOnly() {
    }

    MCMCTriggerOnly::~MCMCTriggerOnly() {
    }

    auto MCMCTriggerOnly::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::TriggerOnly;
    }

    auto MCMCTriggerOnly::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaTriggerOnly::GetInstance();
    }

    auto MCMCTriggerOnly::CreateInstance() -> IMCUMotionCommand::Pointer {
        Pointer instance{ new MCMCTriggerOnly() };
        return instance;
    }
}
