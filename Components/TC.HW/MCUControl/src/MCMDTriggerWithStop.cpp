#include <QMap>
#include "MCMDMetaTriggerWithStop.h"
#include "MCMDTriggerWithStop.h"

namespace TC::MCUControl {
    MCMCTriggerWithStop::MCMCTriggerWithStop() {
    }

    MCMCTriggerWithStop::~MCMCTriggerWithStop() {
    }

    auto MCMCTriggerWithStop::GetCommandType() const -> MotionCommandType {
        return MotionCommandType::TriggerWithStop;
    }

    auto MCMCTriggerWithStop::GetMetadata() const -> IMCUMotionCommandMeta::Pointer {
        return MCMDMetaTriggerWithStop::GetInstance();
    }

    auto MCMCTriggerWithStop::CreateInstance() -> IMCUMotionCommand::Pointer {
        Pointer instance{ new MCMCTriggerWithStop() };
        return instance;
    }
}
