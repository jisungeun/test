#include <QByteArray>
#include <catch2/catch.hpp>
#include <iostream>

#include "../src/CRC16Checker.h"

namespace MCUPacketGeneratorTest {
    using namespace TC::MCUControl;

    TEST_CASE("CRC16Checker") {
        SECTION("Check") {
            auto crcChecker = CRC16Checker::GetInstance();

            auto crc1 = crcChecker->Check(QByteArray("2000"));
            //std::cout << std::uppercase << std::hex << crc1 << std::endl;
            CHECK(crc1 == 0x871a);

            auto crc2 = crcChecker->Check(QByteArray("0102"));
            //std::cout << std::uppercase << std::hex << crc2 << std::endl;
            CHECK(crc2 == 0x3ecb);

            auto crc3 = crcChecker->Check(QByteArray("01020000000100000002"));
            //std::cout << std::uppercase << std::hex << crc3 << std::endl;
            CHECK(crc3 == 0xa0f6);
        }
    }
}