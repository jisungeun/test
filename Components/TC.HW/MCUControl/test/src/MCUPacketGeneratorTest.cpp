#include <QByteArray>
#include <catch2/catch.hpp>
#include <iostream>

#include "../src/MCUPacketGenerator.h"

namespace MCUPacketGeneratorTest {
    using namespace TC::MCUControl;

    TEST_CASE("MCUPacketGenerator") {
        SECTION("Genereate") {
            QList<int32_t> params{ 0, 1, 2, 3 };
            auto packet_01 = MCUPacketGenerator::Generate(Command::RunMacro, params);

        }
    }

    TEST_CASE("MCULogGeneartor") {
        SECTION("PackLogMessage") {

            QByteArray sub("ABCD"); //0x41424344

            int32_t packed = 0;
            for(int i=0; i<4; i++) {
                auto byte = static_cast<int32_t>(sub.at(3-i));
                packed += (byte << (i*8));
            }

            CHECK(packed == 0x41424344);
        }
    }
}