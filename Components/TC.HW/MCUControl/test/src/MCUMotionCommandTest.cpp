#include <QFile>
#include <QTextStream>

#include <catch2/catch.hpp>

#include "MCUMotionCommandFactory.h"
#include "MCUMotionCommandRepository.h"
#include "IMCUMotionCommand.h"

namespace MCUMotionCommandTest {
    using namespace TC::MCUControl;

    auto loadTextFile(const QString& path)->QStringList {
        QStringList list;

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return list;

        QTextStream in(&file);
        while (!in.atEnd()) {
            auto line = in.readLine();
            list.push_back(line);
        }
        return list;
    };

    TEST_CASE("MotionCommandFactoryTest") {
        SECTION("GetCommandTypes()") {
            auto factory = MCUMotionCommandFactory::GetInstance();

            auto types = factory->GetCommandTypes();
            CHECK(types.length() == MotionCommandType::_size());
        }

        SECTION("GetCommandTypesAsString()") {
            auto factory = MCUMotionCommandFactory::GetInstance();

            auto types = factory->GetCommandTypesAsString();

            for (auto value : MotionCommandType::_values()) {
                bool contains = types.contains(value._to_string());
                CHECK(contains == true);
            }
        }

        SECTION("CreateCommand()") {
            auto factory = MCUMotionCommandFactory::GetInstance();

            auto types = factory->GetCommandTypes();

            for (auto type : types) {
                auto cmd = factory->CreateCommand(type);
                CHECK(type == cmd->GetCommandType());
            }
        }
    }

    TEST_CASE("MCUMotionCommandRepository") {
        SECTION("InsertCommand()") {
            auto repo = MCUMotionCommandRepository::GetInstance();
            auto factory = MCUMotionCommandFactory::GetInstance();

            auto cmd1 = factory->CreateCommand(MotionCommandType::TriggerWithMotion);
            cmd1->SetCommandID(0);
            cmd1->SetCommandTitle("cmd_0");
            repo->InsertCommand(cmd1);

            CHECK(1 == repo->GetCounts());

            auto clone1 = repo->CloneCommand(0);

            CHECK(0 == clone1->GetCommandID());
            CHECK(+MotionCommandType::TriggerWithMotion == clone1->GetCommandType());
            CHECK("cmd_0" == clone1->GetCommandTitle());

            repo->Clear();
            CHECK(0 == repo->GetCounts());
        }

        SECTION("SaveAndLoad()") {
            auto repo = MCUMotionCommandRepository::GetInstance();
            auto factory = MCUMotionCommandFactory::GetInstance();

            auto cmd1 = factory->CreateCommand(MotionCommandType::TriggerWithMotion);
            cmd1->SetCommandID(0);
            cmd1->SetCommandTitle("cmd_0");
            repo->InsertCommand(cmd1);

            auto cmd2 = factory->CreateCommand(MotionCommandType::TriggerOnly);
            cmd2->SetCommandID(1);
            cmd2->SetCommandTitle("cmd_1");
            repo->InsertCommand(cmd2);

            auto path01 = QString("%1/MCUMotionCommandRepository_SaveAndLoad_01.txt").arg(_TEST_OUTPUT);
            auto saveRes01 = repo->Save(path01);
            if (!saveRes01) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == saveRes01);
            }

            auto loadRes01 = repo->Load(path01);
            if (!loadRes01) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == loadRes01);
            }

            auto path02 = QString("%1/MCUMotionCommandRepository_SaveAndLoad_02.txt").arg(_TEST_OUTPUT);
            auto saveRes02 = repo->Save(path02);
            if (!saveRes02) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == saveRes02);
            }

            auto contents01 = loadTextFile(path01);
            auto contents02 = loadTextFile(path02);
            CHECK(contents01.length() == contents02.length());

            for (int idx = 0; idx < contents01.length(); idx++) {
                CHECK(contents01.at(idx) == contents02.at(idx));
            }
        }

        SECTION("Load_Invalid()") {
            auto repo = MCUMotionCommandRepository::GetInstance();

            auto path01 = QString("%1/TD001_InvalidFormat_01.txt").arg(_TEST_DATA);
            auto loadRes01 = repo->Load(path01);
            CHECK(loadRes01 == false);

            auto path02 = QString("%1/TD001_InvalidFormat_02.txt").arg(_TEST_DATA);
            auto loadRes02 = repo->Load(path02);
            CHECK(loadRes02 == false);

            auto path03 = QString("%1/TD001_InvalidFormat_03.txt").arg(_TEST_DATA);
            auto loadRes03 = repo->Load(path03);
            CHECK(loadRes03 == false);
        }
    }
}