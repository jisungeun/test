#include <QByteArray>
#include <catch2/catch.hpp>
#include <iostream>

#include "../src/AsciiConverter.h"

namespace MCUAsciiConverterTest {
    using namespace TC::MCUControl;

    TEST_CASE("AsciiConverter") {
        SECTION("num2ascii") {
            auto ascii_01 = AsciiConverter::toAscii(0x5A, 2);
            auto match_01 = Catch::Matchers::Equals("5A");
            REQUIRE_THAT(ascii_01.toStdString(), match_01);

            auto ascii_02 = AsciiConverter::toAscii(0x5A, 4);
            auto match_02 = Catch::Matchers::Equals("005A");
            REQUIRE_THAT(ascii_02.toStdString(), match_02);

            auto ascii_03 = AsciiConverter::toAscii(0xF15A, 8);
            auto match_03 = Catch::Matchers::Equals("0000F15A");
            REQUIRE_THAT(ascii_03.toStdString(), match_03);

            auto ascii_04 = AsciiConverter::toAscii(20, 4);
            auto match_04 = Catch::Matchers::Equals("0014");
            REQUIRE_THAT(ascii_04.toStdString(), match_04);
        }

        SECTION("num2ascii_Negative") {
            auto ascii_01 = AsciiConverter::toAscii(0xFFFFFF9B, 8); //-101
            auto match_01 = Catch::Matchers::Equals("FFFFFF9B");
            REQUIRE_THAT(ascii_01.toStdString(), match_01);

            auto ascii_02 = AsciiConverter::toAscii(0xFFFFFFFE, 2); //-2
            auto match_02 = Catch::Matchers::Equals("FE");
            REQUIRE_THAT(ascii_02.toStdString(), match_02);
        }
    }

    TEST_CASE("MCUPacketParser") {
        SECTION("ascii2num") {
            auto num_01 = AsciiConverter::toNum("5A");
            CHECK(0x5A == num_01);

            auto num_02 = AsciiConverter::toNum("5AA5");
            CHECK(0x5AA5 == num_02);

            auto num_03 = AsciiConverter::toNum("0014");
            CHECK(20 == num_03);
        }

        SECTION("ascii2num_Negative") {
            auto num_01 = AsciiConverter::toNum("FFFFFF9B"); //-101
            CHECK(-101 == num_01);

            auto num_02 = AsciiConverter::toNum("FFFFFFFE");  //-2
            CHECK(-2 == num_02);
        }

        SECTION("ascii2num_8bit") {
            auto num_01 = AsciiConverter::toNum8Bit("FE");  //-2
            CHECK(-2 == num_01);

            auto num_02 = AsciiConverter::toNum8Bit("7F");  //127
            CHECK(127 == num_02);

            auto num_03 = AsciiConverter::toNum8Bit("81");  //-127
            CHECK(-127 == num_03);
        }

        SECTION("ascii2num_16bit") {
            auto num_01 = AsciiConverter::toNum16Bit("7FFF");  //32767
            CHECK(32767 == num_01);

            auto num_02 = AsciiConverter::toNum16Bit("8001");  //-32767
            CHECK(-32767 == num_02);

            auto num_03 = AsciiConverter::toNum16Bit("FFF9");  //-7
            CHECK(-7 == num_03);
        }
    }
}
