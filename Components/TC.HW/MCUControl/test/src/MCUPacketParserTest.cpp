#include <QByteArray>
#include <catch2/catch.hpp>
#include <iostream>

#include "../src/AsciiConverter.h"
#include "../src/MCUPacketParser.h"

namespace MCUPacketParserTest {
    using namespace TC::MCUControl;

    struct DataSet {
        QByteArray packet;
        uint32_t paramCount;
        bool res;
        Error error;
    };

    QList<DataSet> dataSet{
        {QByteArray("{0000}"), 0, false, Error::MCU_COMM_PACKET_SHORT},
        {QByteArray("[00000000}"), 0, false, Error::MCU_COMM_INVALID_HEADER},
        {QByteArray("{00000000]"), 0, false, Error::MCU_COMM_INVALID_TAIL},
        {QByteArray("{00000000}"), 0, false, Error::MCU_COMM_INVALID_CHECKSUM},
        {QByteArray("{2000871A}"), 0, false, Error::MCU_COMM_INVALID_RESPONSECODE},
        {QByteArray("{01023ECB}"), 0, false, Error::MCU_COMM_INVALID_PACKETLENGTH},
        {QByteArray("{01020000000100000002A0F6}"), 2, true, Error::MCU_NOERROR},
    };

    TEST_CASE("MCUPackterParser") {
        SECTION("Parse") {

            for(auto data : dataSet) {
                ResponseCode resp{ ResponseCode::Fail };
                QList<int32_t> params;

                //std::cout << data.packet.toStdString().c_str() << std::endl;

                MCUPacketParser parser;
                auto res = parser.Parse(data.packet, resp, params);
                auto err = parser.GetLastError();

                CHECK(res == data.res);
                CHECK(err.GetCode() == data.error);
                CHECK(params.length() == data.paramCount);
            }
        }
    }
}