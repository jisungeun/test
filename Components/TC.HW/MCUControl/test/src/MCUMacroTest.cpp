#include <QFile>
#include <QTextStream>

#include <catch2/catch.hpp>

#include "MCUMacroRepository.h"

namespace MCUMacroTest {
    using namespace TC::MCUControl;

    auto loadTextFile(const QString& path)->QStringList {
        QStringList list;

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return list;

        QTextStream in(&file);
        while (!in.atEnd()) {
            auto line = in.readLine();
            list.push_back(line);
        }
        return list;
    };

    auto genMacroGroup(uint32_t id, const QString& title)->MCUMacroGroup::Pointer {
        MCUMacroGroup::Pointer group{ new MCUMacroGroup() };
        group->SetGroupID(id);
        group->SetTitle(title);
        return group;
    }

    auto genMacro(uint32_t cmd, int32_t xpos, int32_t ypos, int32_t zpos)->MCUMacro::Pointer {
        MCUMacro::Pointer macro{ new MCUMacro() };
        macro->SetMotionCommand(cmd);
        macro->SetPosition(xpos, ypos, zpos);
        return macro;
    }

    TEST_CASE("MCUMacroGroup") {
        SECTION("AddMacro()") {
            auto group1 = genMacroGroup(0, "group_0");
            group1->AddMacro(genMacro(0, 0, 0, 0));
            group1->AddMacro(genMacro(1, 1, 1, 1));
            CHECK(2 == group1->GetCount());
        }
    }

    TEST_CASE("MCUMacroRepository") {
        SECTION("InsertMacroGroup()") {
            auto repo = MCUMacroRepository::GetInstance();

            auto group1 = genMacroGroup(0, "group_0");
            group1->AddMacro(genMacro(0, 0, 0, 0));
            group1->AddMacro(genMacro(1, 1, 1, 1));

            repo->InsertMacroGroup(group1);
            CHECK(1 == repo->GetCounts());

            repo->Clear();
        }

        SECTION("SaveAndLoad()") {
            auto repo = MCUMacroRepository::GetInstance();

            auto group1 = genMacroGroup(0, "group_0");
            group1->AddMacro(genMacro(0, 0, 0, 0));
            group1->AddMacro(genMacro(1, 1, 1, 1));

            repo->InsertMacroGroup(group1);

            auto path01 = QString("%1/MCUMacroRepository_SaveAndLoad_01.txt").arg(_TEST_OUTPUT);
            auto saveRes01 = repo->Save(path01);
            if (!saveRes01) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == saveRes01);
            }

            auto loadRes01 = repo->Load(path01);
            if (!loadRes01) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == loadRes01);
            }

            auto path02 = QString("%1/MCUMacroRepository_SaveAndLoad_02.txt").arg(_TEST_OUTPUT);
            auto saveRes02 = repo->Save(path02);
            if (!saveRes02) {
                FAIL(repo->GetLastError().GetDescription().toStdString());
            } else {
                CHECK(true == saveRes02);
            }

            auto contents01 = loadTextFile(path01);
            auto contents02 = loadTextFile(path02);
            CHECK(contents01.length() == contents02.length());

            for (int idx = 0; idx < contents01.length(); idx++) {
                CHECK(contents01.at(idx) == contents02.at(idx));
            }
        }
    }
}