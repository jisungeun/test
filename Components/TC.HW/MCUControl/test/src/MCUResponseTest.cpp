#include <catch2/catch.hpp>

#include "MCUResponse.h"

namespace MCUResponseTest {
    using namespace TC::MCUControl;

    TEST_CASE("MCUResponse") {
        SECTION("SetAndGet") {
            MCUResponse resp;

            CHECK(0 == resp.GetValue(Response::AngleX));

            resp.SetValue(Response::AngleX, 100);
            CHECK(100 == resp.GetValue(Response::AngleX));
        }

        SECTION("Flag") {
            MCUResponse resp;

            resp.SetValue(Response::AxisLEnd, 1);
            CHECK(Flag::Triggered == resp.GetValue(Response::AxisLEnd));
            CHECK(Flag::NotTriggered == resp.GetValue(Response::AxisWEnd));
        }

        SECTION("GetState") {

            MCUResponse resp;

            resp.SetValue(Response::StateMachineId, MCUState::Ready);
            CHECK(resp.GetState() == +MCUState::Ready);

            resp.SetValue(Response::StateMachineId, MCUState::Unknown - 1);
            CHECK(resp.GetState() == +MCUState::Unknown);
        }
    }
}