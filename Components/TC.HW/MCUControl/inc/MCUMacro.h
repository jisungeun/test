#pragma once
#include <memory>
#include <QList>
#include <QStringList>

#include "MCUDefines.h"
#include "IMCUMotionCommand.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUMacro {
    public:
        typedef std::shared_ptr<MCUMacro> Pointer;

    public:
        MCUMacro();
        MCUMacro(const MCUMacro& other);
        ~MCUMacro();

        auto SetMotionCommand(uint32_t cmdID)->void;
        auto GetMotionCommand() const->uint32_t;

        auto SetAFMode(int32_t mode)->void;
        auto GetAFMode() const->int32_t;

        auto SetPosition(int32_t xpos, int32_t ypos, int32_t zpos)->void;
        auto GetPositionX(void)->int32_t;
        auto GetPositionY(void)->int32_t;
        auto GetPositionZ(void)->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}