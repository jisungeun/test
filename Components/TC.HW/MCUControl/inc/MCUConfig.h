#pragma once
#include <memory>

#include "MCUDefines.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUConfig {
    public:
        MCUConfig();
        MCUConfig(const MCUConfig& other);
        virtual ~MCUConfig();

        auto operator=(const MCUConfig& other)->MCUConfig&;

        auto SetPort(const int32_t port)->void;
        auto GetPort() const->int32_t;

        auto SetBaudrate(uint32_t baudrate)->void;
        auto GetBaudrate() const->int32_t;

        auto SetSafeZPosition(int32_t pos)->void;
        auto GetSafeZPosition() const->int32_t;

        auto SetTemperatureLimit(double lowerLimit, double upperLimit)->void;
        auto GetTemperatureLowerLimit() const->double;
        auto GetTemperatureUpperLimit() const->double;

        auto SetInclinationLimit(double limit)->void;
        auto GetInclinationLimit() const->double;

        auto SetResolution(Axis axis, uint32_t resolution)->void;
        auto GetResolution(Axis axis) const->uint32_t;

        auto SetFilterPosition(int32_t channel, int32_t position)->void;
        auto SetFilterPositions(const QList<int32_t>& positions)->void;
        auto GetFilterPositions() const->QList<int32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}