#pragma once
#include <memory>
#include <QList>
#include <QStringList>

#include "MCUDefines.h"
#include "IMCUMotionCommand.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUMotionCommandFactory {
    public:
        typedef std::shared_ptr<MCUMotionCommandFactory> Pointer;

    protected:
        MCUMotionCommandFactory();

    public:
        virtual ~MCUMotionCommandFactory();

        static auto GetInstance()->Pointer;

        auto GetCommandTypes() const->QList<MotionCommandType>;
        auto GetCommandTypesAsString() const->QStringList;

        auto CreateCommand(MotionCommandType type)->IMCUMotionCommand::Pointer;
    };
}