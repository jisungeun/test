#pragma once
#include <memory>

#include <QObject>
#include <QDateTime>

#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUCommunicationObserver : public QObject {
        Q_OBJECT

    public:
        struct Item {
            bool sendPacket{ false };
            QDateTime time{ QDateTime::currentDateTime() };
            QByteArray packet;
        };

    public:
        MCUCommunicationObserver(QObject* parent = nullptr);
        virtual ~MCUCommunicationObserver();

        auto AddSendPacket(const QByteArray& packet)->void;
        auto AddRecvPacket(const QByteArray& packet)->void;

        auto GetNext()->std::shared_ptr<Item>;

    signals:
        void sigUpdated();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}