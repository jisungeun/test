#pragma once
#include <memory>
#include <QString>
#include <QStringList>

#include <enum.h>

#include "MCUDefines.h"
#include "IMCUMotionCommandMeta.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API IMCUMotionCommand {
    public:
        typedef std::shared_ptr<IMCUMotionCommand> Pointer;

    public:
        IMCUMotionCommand();
        IMCUMotionCommand(uint32_t id);
        virtual ~IMCUMotionCommand();

        auto clone()->IMCUMotionCommand::Pointer;
        virtual auto GetCommandType() const->MotionCommandType = 0;
        auto GetCommandTypeAsString() const->QString;

        auto SetCommandID(uint32_t id)->void;
        auto GetCommandID() const->uint32_t;

        auto SetCommandTitle(const QString& title)->void;
        auto GetCommandTitle(void) const->QString;

        auto GetParameterCount() const->uint32_t;

        auto SetParameter(uint32_t index, int value)->bool;
        auto GetParameter(uint32_t index) const->int32_t;
        auto GetParameters() const->QList<int32_t>;

        auto GetParameterName(uint32_t index) const->QString;
        auto GetParameterNames() const->QStringList;

        auto GetParameterUnit(uint32_t index) const->QString;
        auto GetParameterUnits() const->QStringList;

        auto CheckName(uint32_t index, const QString& name)->bool;
        auto CheckUnit(uint32_t index, const QString& unit)->bool;

    protected:
        virtual auto GetMetadata() const->IMCUMotionCommandMeta::Pointer = 0;
        virtual auto CreateInstance()->IMCUMotionCommand::Pointer = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}