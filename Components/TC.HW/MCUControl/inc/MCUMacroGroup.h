#pragma once
#include <memory>

#include "MCUMacro.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUMacroGroup {
    public:
        typedef std::shared_ptr<MCUMacroGroup> Pointer;

    public:
        MCUMacroGroup();
        MCUMacroGroup(const MCUMacroGroup& other);
        virtual ~MCUMacroGroup();

        auto SetGroupID(uint32_t id)->void;
        auto GetGroupID(void) const->uint32_t;

        auto SetTitle(const QString& title)->void;
        auto GetTitle(void) const->QString;

        auto GetCount(void) const->uint32_t;

        auto AddMacro(MCUMacro::Pointer& macro)->void;
        auto GetMacro(uint32_t index)->MCUMacro::Pointer;
        auto RemoveMacro(uint32_t index)->bool;
        auto MoveMacro(uint32_t fromIdx, uint32_t toIdx)->void;

        auto ClearMacros()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}