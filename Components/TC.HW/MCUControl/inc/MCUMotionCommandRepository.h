#pragma once
#include <memory>
#include <QCoreApplication>
#include <QList>
#include <QStringList>

#include "MCUDefines.h"
#include "MCUError.h"
#include "IMCUMotionCommand.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUMotionCommandRepository {
        Q_DECLARE_TR_FUNCTIONS(MCUMotionCommandRepository)

    public:
        typedef std::shared_ptr<MCUMotionCommandRepository> Pointer;

    protected:
        MCUMotionCommandRepository();

    public:
        virtual ~MCUMotionCommandRepository();

        static auto GetInstance()->Pointer;

        auto InsertCommand(IMCUMotionCommand::Pointer& command)->void;
        auto DeleteCommand(uint32_t commandID)->void;

        auto GetCounts() const->uint32_t;
        auto GetNextCommandID() const->uint32_t;
        auto GetCommand(uint32_t commandID)->IMCUMotionCommand::Pointer;
        auto CloneCommand(uint32_t commandID)->IMCUMotionCommand::Pointer;
        auto CloneCommandByIndex(uint32_t index)->IMCUMotionCommand::Pointer;

        auto Clear()->void;

        auto Save(const QString& path)->bool;
        auto Load(const QString& path)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}