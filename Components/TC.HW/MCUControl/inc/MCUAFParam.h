#pragma once
#include <memory>

#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUAFParam {
    public:
        MCUAFParam();
        ~MCUAFParam();

        auto operator=(const MCUAFParam& other)->MCUAFParam&;
        
        auto SetLensID(int32_t id)->void;
        auto GetLensID() const->int32_t;
        
        auto SetInFocusRange(int32_t value)->void;
        auto GetInFocusRange() const->int32_t;
        
        auto SetDirection(int32_t direction)->void;
        auto GetDirection() const->int32_t;
        
        auto SetLoopInterval(int32_t interval)->void;
        auto GetLoopInterval() const->int32_t;
        
        auto SetMaxTrialCount(int32_t count)->void;
        auto GetMaxTrialCount() const->int32_t;
        
        auto SetSensorValue(int32_t minVal, int32_t maxVal)->void;
        auto GetSensorValueMin() const->int32_t;
        auto GetSensorValueMax() const->int32_t;
        
        auto SetResolution(int32_t mul, int32_t div)->void;
        auto GetResolutionMul() const->int32_t;
        auto GetResolutionDiv() const->int32_t;

        auto SetMaxCompensationPulse(int32_t pulse)->void;
        auto GetMaxCompensationPulse() const->int32_t;

        auto SetAFLimit(int32_t value)->void;
        auto GetAFLimit() const->int32_t;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}