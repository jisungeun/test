#pragma once
#include <memory>

#include <QObject>
#include <QByteArray>

#include "MCUCommParam.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUComm : public QObject{
        Q_OBJECT

    public:
        MCUComm(QObject* parent = nullptr);
        virtual ~MCUComm();

        virtual auto SetParameter(MCUCommParam& param)->void = 0;
        virtual auto OpenPort()->bool = 0;
        virtual auto IsOpened() const->bool = 0;

        virtual auto write(const QByteArray& data)->qint64 = 0;
        virtual auto waitForBytesWritten(int timeout)->bool = 0;
        virtual auto waitForReadyRead(int timeout)->bool = 0;
        virtual auto readAll() const->QByteArray = 0;
    };
}
