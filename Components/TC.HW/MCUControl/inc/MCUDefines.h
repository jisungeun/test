#pragma once
#include <memory>
#include <enum.h>

namespace TC::MCUControl {
    BETTER_ENUM(Model, int32_t,
                HTX
    );

    BETTER_ENUM(Axis, int32_t,
                X,
                Y,
                Z,
                L,
                U,
                V,
                W
    );

    BETTER_ENUM(MotionCommandType, uint32_t,
                TriggerWithMotion,
                TriggerWithStop,
                TriggerOnly,
                PatternSequence,
                ChangeFilter,
                ChangePosition
    );

    BETTER_ENUM(Command, uint16_t,
                StartInitialization             = 0x0001,
                CheckInitializationProgress     = 0x0002,
                SetSafeZPosition                = 0x0011,
                SetFocusPosition                = 0x0012,
                SetScanReadyPosition            = 0x0015,
                SetCommand                      = 0x0016,
                SetSoftwareLimitPosition        = 0x0017,
                SetEncoderInBand                = 0x0018,
                ReadFlashData                   = 0x0019,
                WriteFlashData                  = 0x001A,
                AddMacroPoint                   = 0x0021,
                ClearMacro                      = 0x0022,
                RunMacro                        = 0x0023,
                StopMacro                       = 0x0024,
                CheckMacroStatus                = 0x0025,
                StartMacroStreaming             = 0x0026,
                StopMacroStreaming              = 0x0027,
                AddMacroToBuffer                = 0x0028,
                Move                            = 0x0031,
                MoveToLoadingReadyPosition      = 0x0032,
                MoveToReadyPosition             = 0x0034,
                GenerateTrigger                 = 0x0035,
                SetDigitalOutput                = 0x0036,
                ReadDigitalInput                = 0x0037,
                MoveXY                          = 0x0038,
                SetFilterPosition               = 0x003A,
                ChangeFilter                    = 0x003B,
                StopMotion                      = 0x003D,
                LoadCartridge                   = 0x0040,
                UnloadCartridge                 = 0x0041,
                FinishCalibration               = 0x0042,
                StartTest                       = 0x0043,
                FinishTest                      = 0x0044,
                PowerOff                        = 0x0045,
                Recover                         = 0x0047,
                StartManualMode                 = 0x0048,
                FinishManualMode                = 0x0049,
                SetRGBControl                   = 0x0050,
                ReadDLPCVersion                 = 0x00A0,
                SetLEDIntensity                 = 0x00A1,
                SetDMDExposure                  = 0x00A2,
                ReadTriggerCount                = 0x00A3,
                ResetTriggerCount               = 0x00A4,
                SetLEDChannel                   = 0x00A5,
                SetCameraType                   = 0x00A6,
                SetAFMParameter                 = 0x00C1,
                SetAFTarget                     = 0x00C4,
                InitAFM                         = 0x00C5,
                IsInitAFM                       = 0x00C6,
                EnableAF                        = 0x00C8,
                CheckAFStatus                   = 0x00C9,
                ManualAFControl                 = 0x00CA,
                ReadAFMSensor                   = 0x00CB,
                ReadAFLog                       = 0x00CC,
                ScanZForAFM                     = 0x00CD,
                DumpScanZForAFM                 = 0x00CE,
                ReadAFSensorParameter           = 0x00CF,
                ChangeAFLensParameter           = 0x00D1,
                SetAFMPeakmode                  = 0x00D2,
                CheckMovingStatus               = 0x00E1,
                CheckStatus                     = 0x00E2,
                GetAxisPosition                 = 0x00E3,
                ReadTemperature                 = 0x00E4,
                ReadAcceleration                = 0x00E5,
                ReadCartridgeSensor             = 0x00E7,
                ReadFirmwareVersion             = 0x00E8,
                ReadLog                         = 0x00FA
    );

    BETTER_ENUM(Response, uint32_t,
                StateMachineId,
                MacroStep,
                AxisXStart,
                AxisXEnd,
                AxisXMoving,
                AxisYStart,
                AxisYEnd,
                AxisYMoving,
                AxisZStart,
                AxisZEnd,
                AxisZMoving,
                AxisLStart,
                AxisLEnd,
                AxisLMoving,
                AxisUStart,
                AxisUEnd,
                AxisUMoving,
                AxisVStart,
                AxisVEnd,
                AxisVMoving,
                AxisWStart,
                AxisWEnd,
                AxisWMoving,
                SensorDoorOpen,
                SensorShutterOpen,
                SensorCartridge1,
                SensorCartridge2,
                SensorLED0On,
                SensorLED1On,
                SensorLED2On,
                SensorLED3On,
                AfOn,
                AfError,
                AngleX,
                AngleY,
                AngleZ,
                Temperature0,
                Temperature1,
                Temperature2,
                Temperature3,
                AxisXPosition,
                AxisYPosition,
                AxisZPosition,
                AxisUPosition,
                AxisVPosition,
                AxisWPosition,
                MacroStreamingIndex,
                MacroStreamingError,
                MacroStreamingHalfEmpty,
                MacroStreamingEmptyBufferSize,
                AxisXSWLimit,
                AxisYSWLimit,
                AxisZSWLimit,
                AxisUSWLimit,
                AxisVSWLimit,
                AxisWSWLimit
    );

    BETTER_ENUM(Flag, int32_t,
                False               = 0,
                Closed              = 0,
                Off                 = 0,
                NotTriggered        = 0,
                NotMoving           = 0,
                Low                 = 0,
                Idle                = 0,

                True                = 1,
                Open                = 1,
                On                  = 1,
                Triggered           = 1,
                Moving              = 1,
                High                = 1,
                Running             = 1
    );

    BETTER_ENUM(ResponseCode, uint8_t,
                Fail                = 0x00,
                Success             = 0x01
    );

    BETTER_ENUM(MCUState, uint32_t,
                PowerOff                = 0,          //파워 오프
                InitializationReady     = 1,          //초기화 대기
                Ready                   = 2,          //준비
                Testing                 = 3,          //검사 중

                Error                   = 100,        //에러
                Unknown                 = 0xFFFF     //정의되지 않은 상태
    );

    BETTER_ENUM(AFInitState, int32_t,
                Error                   = -1,
                Finished                = 0,
                Initializing            =1
    );
}