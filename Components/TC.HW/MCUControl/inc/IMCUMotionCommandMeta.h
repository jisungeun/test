#pragma once
#include <memory>
#include <QString>
#include <QStringList>

#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API IMCUMotionCommandMeta {
    public:
        typedef std::shared_ptr<IMCUMotionCommandMeta> Pointer;

    public:
        IMCUMotionCommandMeta();
        virtual ~IMCUMotionCommandMeta();

        auto GetParameterCount() const->uint32_t;

        auto GetParameterName(uint32_t index) const->QString;
        auto GetParameterNames() const->QStringList;

        auto GetParameterUnit(uint32_t index) const->QString;
        auto GetParameterUnits() const->QStringList;

    protected:
        auto RegisterParameter(const QString& name, const QString& unit)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}