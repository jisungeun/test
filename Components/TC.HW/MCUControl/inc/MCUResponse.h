#pragma once
#include <memory>
#include <QCoreApplication>

#include "MCUError.h"
#include "MCUDefines.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUResponse {
        Q_DECLARE_TR_FUNCTIONS(MCUResponse)

    public:
        typedef std::shared_ptr<MCUResponse> Pointer;

    public:
        MCUResponse();
        MCUResponse(const MCUResponse& other);
        virtual ~MCUResponse();

        auto operator=(const MCUResponse& other)->MCUResponse&;

        auto GetValue(Response title) const->int32_t;
        auto GetValueAsFlag(Response title) const->Flag;
        auto GetValueAsFloat(Response title) const->float;
        auto SetValue(Response title, int32_t value, float times=1.0)->void;

        auto GetState() const->MCUState;
        auto GetAngle(Response title) const->float;
        auto GetTemperature(Response title) const->float;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}