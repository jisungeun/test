#pragma once
#include <memory>
#include <QCoreApplication>

#include "MCUError.h"
#include "MCUMacroGroup.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUMacroRepository {
        Q_DECLARE_TR_FUNCTIONS(MCUMacroRepository)

    public:
        typedef std::shared_ptr<MCUMacroRepository> Pointer;

    protected:
        MCUMacroRepository();

    public:
        virtual ~MCUMacroRepository();

        static auto GetInstance()->Pointer;

        auto InsertMacroGroup(MCUMacroGroup::Pointer& macroGroup)->void;
        auto CreateMacroGroup(const QString& title)->MCUMacroGroup::Pointer;
        auto CloneMacroGroup(MCUMacroGroup::Pointer& macroGroup)->MCUMacroGroup::Pointer;

        auto GetCounts() const->uint32_t;
        auto GetMacroGroup(uint32_t groupID)->MCUMacroGroup::Pointer;
        auto GetMacroGroupByIdx(uint32_t index)->MCUMacroGroup::Pointer;

        auto RemoveMacroGroup(uint32_t groupID)->void;
        auto Clear()->void;

        auto Save(const QString& path)->bool;
        auto Load(const QString& path)->bool;

        auto GetLastError() const->MCUError;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}