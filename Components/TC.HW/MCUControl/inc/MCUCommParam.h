#pragma once
#include <memory>

#include <QStringList>
#include <QVariant>

#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUCommParam {
    public:
        MCUCommParam();
        virtual ~MCUCommParam();

        auto Set(const QString& name, const QVariant& value)->void;
        auto Get(const QString& name) const->QVariant;
        auto GetAsString(const QString& name) const->QString;
        auto GetAsInt(const QString& name) const->int32_t;

        auto GetParams() const->QStringList;

        auto operator=(const MCUCommParam& other)->MCUCommParam&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
