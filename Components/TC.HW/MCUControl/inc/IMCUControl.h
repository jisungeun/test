#pragma once
#include <memory>
#include <QCoreApplication>

#include <enum.h>

#include "MCUError.h"
#include "MCUDefines.h"
#include "MCUConfig.h"
#include "MCUComm.h"
#include "MCUMotionCommandRepository.h"
#include "MCUMacroRepository.h"
#include "MCUResponse.h"
#include "MCUAFParam.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API IMCUControl {
        Q_DECLARE_TR_FUNCTIONS(IMCUControl)
    public:
        typedef std::shared_ptr<IMCUControl> Pointer;

    public:
        IMCUControl(bool simulation = false);
        virtual ~IMCUControl();

        auto GetMotionCommandRepository()->MCUMotionCommandRepository::Pointer;
        auto GetMacroRepository()->MCUMacroRepository::Pointer;

        auto GetLastError()->MCUError;

        auto LoadConfig(const QString& path)->bool;
        auto SetConfig(const MCUConfig& config)->void;
        auto SaveConfig(const QString& path)->bool;
        auto GetConfig()->MCUConfig&;

        auto SetSafeZPosition()->bool;

        virtual auto OpenPort()->bool = 0;
        virtual auto StartInitialization()->bool = 0;
        virtual auto CheckInitialization(int32_t& progress)->bool = 0;
        virtual auto SetSafeZPosition(int32_t position)->bool = 0;
        virtual auto SetCommand(int32_t command, int32_t type, const QList<int32_t>& cmdParams)->bool = 0;
        virtual auto SetCommand(IMCUMotionCommand::Pointer& cmd)->bool;
        virtual auto SetSWLimitPosition(const QList<int32_t>& limits)->bool = 0;
        virtual auto SetInPositionBand(const QList<int32_t>& bands)->bool = 0;
        virtual auto AddMacroPoint(int32_t group, int32_t index, int32_t command, 
                                   int32_t xPosition, int32_t yPosition, int32_t zPosition, int32_t afMode)->bool = 0;
        virtual auto ClearMacro(int32_t group)->bool = 0;
        virtual auto RunMacro(int32_t group)->bool = 0;
        virtual auto StopMacro()->bool = 0;
        virtual auto CheckMacroStatus(Flag& status, uint32_t& step)->bool = 0;
        virtual auto SendMacrosToStreamBuffer(const QList<QList<int32_t>>& commands)->bool = 0;
        virtual auto StartMacroStreaming()->bool = 0;
        virtual auto StopMacroStreaming()->bool = 0;
        virtual auto Move(Axis axis, int32_t position)->bool = 0;
        virtual auto SetFilterPositions(const QList<int32_t>& positions)->bool = 0;
        virtual auto ChangeFilter(int32_t channel)->bool = 0;
        virtual auto MoveToLoadingReadyPosition()->bool = 0;
        virtual auto MoveToSafeZPos()->bool = 0;
        virtual auto GenerateTrigger(int32_t pulseWidth, int32_t interval, int32_t count)->bool = 0;
        virtual auto SetDO(int32_t channel, int32_t value)->bool = 0;
        virtual auto ReadDI(int32_t channel, Flag& status)->bool = 0;
        virtual auto MoveXY(int32_t xPosition, int32_t yPosition)->bool = 0;
        virtual auto StopMotion()->bool = 0;
        virtual auto LoadCartridge()->bool = 0;
        virtual auto UnloadCartridge()->bool = 0;
        virtual auto FinishCalibration()->bool = 0;
        virtual auto StartTest()->bool = 0;
        virtual auto FinishTest()->bool = 0;
        virtual auto Recover()->bool = 0;
        virtual auto StartManualMode()->bool = 0;
        virtual auto StopManualMode()->bool = 0;
        virtual auto GetDLPCVersion(int32_t& fw_ver, int32_t& sw_ver)->bool = 0;
        virtual auto SetLEDIntensity(int32_t red, int32_t green, int32_t blue)->bool = 0;
        virtual auto SetDMDExposure(int32_t exposure, int32_t interval)->bool = 0;
        virtual auto ReadTriggerCount(int32_t& count)->bool = 0;
        virtual auto ResetTriggerCount()->bool = 0;
        virtual auto SetLEDChannel(int32_t channel)->bool = 0;
        virtual auto SetCameraType(bool enableInternal, bool enableExternal)->bool = 0;
        virtual auto SetRGBLedIntensity(int32_t red, int32_t green, int32_t blue)->bool = 0;
        virtual auto IsMoving(Axis axis, Flag& status)->bool = 0;
        virtual auto CheckStatus(MCUResponse& status)->bool = 0;
        virtual auto GetPosition(Axis axis, int32_t& position)->bool = 0;
        virtual auto GetPositions()->std::tuple<bool,QMap<Axis,int32_t>> = 0 ;
        virtual auto ReadTemperature(int32_t sensor, float& temperature)->bool = 0;
        virtual auto ReadAcceleration(float& xAngle, float& yAngle, float& zAngle)->bool = 0;
        virtual auto ReadCartridgeSensor(Flag& inlet, Flag& loading)->bool = 0;
        virtual auto ReadMCUFirmwareVersion()->QString = 0;
        virtual auto ReadMCULog()->QString = 0;
        virtual auto GetCommPort()->MCUComm* = 0;
        virtual auto WriteFlash(const QList<int32_t>& data)->bool = 0;
        virtual auto ReadFlash(QList<int32_t>& data)->bool = 0;

        //Autofocus
        virtual auto SetAFParameter(const MCUAFParam& param)->bool = 0;
        virtual auto SetAFTarget(int32_t mode, int32_t value, int32_t& targetOnAFM)->bool = 0;
        virtual auto InitAF()->bool = 0;
        virtual auto CheckAFInitialization(AFInitState& status)->bool = 0;
        virtual auto EnableAF(int32_t mode)->bool = 0;
        virtual auto CheckAFStatus(int32_t& status, int32_t& result, int32_t& trialCount)->bool = 0;
        virtual auto ReadAFMValue(int32_t& value)->bool = 0;
        virtual auto SetManualAF(bool laser)->bool = 0;
        virtual auto ReadAFLog(QList<int32_t>& adcDiff, QList<int32_t>& zComp)->bool = 0;
        virtual auto ScanZForAFM(int32_t startPos, int32_t endPos, int32_t interval, int32_t delay)->bool = 0;
        virtual auto DumpZScanForAFM(QList<int32_t>& values)->bool = 0;
        virtual auto ReadSensorParameter()->QMap<QString,int32_t> = 0;
        virtual auto ChangeLensParameter(int32_t param)->bool = 0;
        virtual auto SetAFMPeakMode(bool peakMode)->bool = 0;

        //Misc
        virtual auto SendPacket(const QByteArray& packet, QList<int32_t>& params, ResponseCode& respCode)->bool = 0;
        virtual auto SendPacket(const QString& cmd, const QString& params, QByteArray& resp, ResponseCode& respCode)->bool = 0;

    protected:
        auto SetLastError(const MCUError& error)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
