#pragma once
#include <memory>
#include <QCoreApplication>
#include <QString>

#include <enum.h>

#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    BETTER_ENUM(Error, uint32_t,
                MCU_NOERROR,
                MCU_FAIL_LOADING_CONFIG,
                MCU_NO_FIELD_ON_CONFIG,
                MCU_NO_CONFIG_FILE,
                MCU_NO_FIELD_ON_MOTIONCOMMAND,
                MCU_FAIL_UNDEFINED_MOTIONCOMMAND,
                MCU_INVALID_MOTIONCOMMAND,
                MCU_FAILED_SAVING_MOTIONCOMMANDS,
                MCU_NO_MOTIONCOMMANDS_FILE,
                MCU_FAILED_LOADING_MOTIONCOMMANDS,
                MCU_NO_FIELD_ON_MACROCOMMAND,
                MCU_FAILED_SAVING_MACROS,
                MCU_NO_MACROS_FILE,
                MCU_FAILED_LOADING_MACROS,
                MCU_FAILED_OPEN_PORT,
                MCU_PORT_NOT_READY,
                MCU_FAILED_SENDING_PACKET,
                MCU_TIMEOUT_FOR_SEND,
                MCU_TIMEOUT_FOR_RESPONSE,
                MCU_COMM_PACKET_SHORT,
                MCU_COMM_INVALID_HEADER,
                MCU_COMM_INVALID_TAIL,
                MCU_COMM_INVALID_CHECKSUM,
                MCU_COMM_INVALID_RESPONSECODE,
                MCU_COMM_INVALID_PACKETLENGTH,
                MCU_COMM_INVALID_COMMAND,                   //only for simulation
                MCU_FAILED_START_INITIALIZATION,
                MCU_FAILED_CHECK_INITIALIZATION_PROGRESS,
                MCU_FAILED_SET_SAFEZPOSITION,
                MCU_FAILED_SET_FOCUSPOSITION,
                MCU_FAILED_SET_SCANREADYPOSITION,
                MCU_FAILED_SET_MOTIONCOMMAND,
                MCU_FAILED_ADD_MACROPOINT,
                MCU_FAILED_CLEAR_MACRO,
                MCU_FAILED_RUN_MACRO,
                MCU_FAILED_STOP_MACRO,
                MCU_FAILED_CHECK_MACROSTATUS,
                MCU_FAILED_MOVEAXIS,
                MCU_FAILED_TO_SET_FILTERPOSITIONS,
                MCU_FAILED_TO_CHANGE_FILTER,
                MCU_FAILED_MOVETO_LOADINGREADYPOSITION,
                MCU_FAILED_MOVETO_READYPOSITION,
                MCU_FAILED_GENERATE_TRIGGER,
                MCU_FAILED_SETDO,
                MCU_FAILED_GETDI,
                MCU_FAILED_MOVEXY,
                MCU_FAILED_SET_FILTERPOSITION,
                MCU_FAILED_CHANGE_FILTER,
                MCU_FAILED_LOAD_CARTRIDGE,
                MCU_FAILED_UNLOAD_CARTRIDGE,
                MCU_FAILED_FINISH_CALIBRATION,
                MCU_FAILED_START_TEST,
                MCU_FAILED_FINISH_TEST,
                MCU_FAILED_POWEROFF,
                MCU_FAILED_RECOVER,
                MCU_FAILED_START_MANUALMODE,
                MCU_FAILED_FINISH_MANUALMODE,
                MCU_FAILED_TO_GET_DLPCVERSION,
                MCU_FAILED_TO_SET_LEDINTENSITY,
                MCU_FAILED_TO_SET_DMDEXPOSURE,
                MCU_FAILED_TO_GET_TRIGGERCOUNT,
                MCU_FAILED_TO_RESET_TRIGGERCOUNT,
                MCU_FAILED_TO_SET_LEDCHANNEL,
                MCU_FAILED_TO_SET_CAMERATYE,
                MCU_FAILED_TO_SET_RGBLEDINTENSITY,
                MCU_FAILED_ISMOVING,
                MCU_FAILED_CHECK_STATUS,
                MCU_FAILED_READ_TEMPERATURE,
                MCU_FAILED_READ_ACCELERATION,
                MCU_FAILED_READ_CARTRIDGE_SENSOR,
                MCU_FAILED_READ_LOG,
                MCU_FAILED_TO_START_MACROSTREAMING,
                MCU_FAILED_TO_STOP_MACROSTREAMING,
                MCU_FAILED_TO_ADD_MACROTOBUFFER,
                MCU_FAILED_TO_SET_AF_PARAMETER,
                MCU_FAILED_TO_SET_AF_TARGET,
                MCU_FAILED_T0_INIT_AFM,
                MCU_FAILED_TO_CHECK_AFM_INITSTATUS,
                MCU_FAILED_TO_ENABLE_AFM,
                MCU_FAILED_TO_CHECK_AF_STATUS,
                MCU_FAILED_TO_READ_AFM_VALUE,
                MCU_FAILED_TO_SET_MANUALAF,
                MCU_FAILED_TO_SCANZ_FOR_AFM,
                MCU_FAILED_TO_READ_AFLOG,
                MCU_FAILED_TO_DUMP_SCANZ_FOR_AFM,
                MCU_FAILED_TO_READ_AFSENSOR_PARAMETERS,
                MCU_FAILED_TO_CHANGE_AFLENS_PARAMETER,
                MCU_FAILED_TO_SET_AFM_PEAKMODE,
                MCU_FAILED_TO_STOP_MOTION,
                MCU_FAILED_TO_SET_SW_LIMIT,
                MCU_FAILED_TO_SET_INPOSITION_BAND,
                MCU_FAILED_TO_READ_FLASHDATA,
                MCU_FAILED_TO_WRITE_FLASHDATA
                );

    class TCMCUControl_API MCUError {
        Q_DECLARE_TR_FUNCTIONS(MCUError)

    public:
        MCUError(Error error = Error::MCU_NOERROR, const QString& message=QString());
        MCUError(const MCUError& other);
        virtual ~MCUError();

        auto operator=(const MCUError& other)->MCUError&;
        auto operator==(Error error)->bool;
        auto operator!=(Error error)->bool;

        auto GetCode() const->Error;
        auto GetText() const->QString;
        auto GetDescription() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}