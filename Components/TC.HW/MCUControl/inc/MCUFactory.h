#pragma once
#include <memory>

#include "MCUDefines.h"
#include "IMCUControl.h"
#include "TCMCUControlExport.h"

namespace TC::MCUControl {
    class TCMCUControl_API MCUFactory {
    public:
        MCUFactory();
        virtual ~MCUFactory();

        static auto CreateControl(Model model = Model::HTX, bool simulation = false)->std::shared_ptr<IMCUControl>;
    };
}