#pragma once

#include <enum.h>

namespace TC::JoystickControl {
    BETTER_ENUM(JoystickErrorCode, uint32_t,
                NoError                                 = 0,
                Initialize_Failure                      = 1,
                FailedToReadValue                       = 2,

                //Last error code
                UnknownError                            = 10000
    )
}
