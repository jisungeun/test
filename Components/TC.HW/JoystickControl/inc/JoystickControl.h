#pragma once
#include <memory>
#include <QList>

#include "JoystickErrorCodes.h"
#include "TCJoystickControlExport.h"

class QString;

namespace TC::JoystickControl {
    class TCJoystickControl_API JoystickControl {
    public:
        typedef std::shared_ptr<JoystickControl> Pointer;

        enum class Status {
            Hold,
            Stop,
            Move
        };

        enum class Axis {
            None,
            XAxis,
            YAxis,
            ZAxis
        };

        enum class Direction {
            Hold,
            Plus,
            Minus
        };

        struct Response {
            Status status{ Status::Hold };
            Axis axis{ Axis::None };
            Direction direction{ Direction::Hold };
        };

    public:
        JoystickControl();
        virtual ~JoystickControl();

        virtual auto Initialize()->bool = 0;
        virtual auto Read()->Response = 0;

        auto GetLastError() const->JoystickErrorCode;

    protected:
        auto SetLastError(JoystickErrorCode code)->void;
        auto Update(const QString& message, JoystickErrorCode code = JoystickErrorCode::NoError)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
