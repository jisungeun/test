#pragma once

#include "JoystickControl.h"
#include "TCJoystickControlExport.h"

namespace TC::JoystickControl {
    class TCJoystickControl_API JoystickControlFactory {
    public:
        enum class Controller {
            SpaceMouse
        };

        static auto Create(Controller type)->JoystickControl::Pointer;
    };
}
