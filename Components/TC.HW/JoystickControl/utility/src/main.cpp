#include <QApplication>
#include <QFile>
#include <QStandardPaths>

#include <TCLogger.h>

#include "mainwindow.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	app.setApplicationName("HTXJoystickConsole");
	app.setOrganizationName("Tomocube, Inc.");
	app.setOrganizationDomain("www.tomocube.com");

	QFile f(":style/tcdark.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

	auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	TC::Logger::Initialize(QString("%1/log/HTXJoystickConsole.log").arg(appDataPath), QString("%1/log/HTXJoystickConsoleErrors.log").arg(appDataPath));
	TC::Logger::SetTraceLevel();

	TC::JoystickControl::MainWindow w;
	w.show();

	return app.exec();
}
