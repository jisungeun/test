#pragma once
#include <memory>

#include <JoystickControl.h>

namespace TC::JoystickControl {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto Initialize()->bool;
        auto Read()->JoystickControl::Response;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
