#define LOGGER_TAG "[MainWindowControl]"
#include <TCLogger.h>

#include "JoystickControlFactory.h"

#include "MainWindow.h"
#include "MainWindowControl.h"

namespace TC::JoystickControl {
    using JoystickPtr = JoystickControl::Pointer;
    using joystickFactory = JoystickControlFactory;

    struct MainWindowControl::Impl {
        JoystickPtr joystick{ joystickFactory::Create(joystickFactory::Controller::SpaceMouse) };
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::Initialize() -> bool {
        return d->joystick->Initialize();
    }

    auto MainWindowControl::Read() -> JoystickControl::Response {
        return d->joystick->Read();
    }
}
