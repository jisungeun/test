#define LOGGER_TAG "[UI]"
#include <QStandardPaths>
#include <QMessageBox>
#include <QTimer>

#include <TCLogger.h>

#include "ui_mainwindow.h"
#include "Version.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace TC::JoystickControl {
    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };
        MainWindowControl control;
        QTimer updateTimer;

        auto UpdateGUI()->void;
    };

    auto MainWindow::Impl::UpdateGUI() -> void {
        using Status = JoystickControl::Status;
        using Axis = JoystickControl::Axis;
        using Direction = JoystickControl::Direction;

        auto resp = control.Read();

        switch(resp.status) {
        case Status::Hold:
            ui->statusHold->setChecked(true);
            break;
        case Status::Stop:
            ui->statusStop->setChecked(true);
            break;
        case Status::Move:
            ui->statusMove->setChecked(true);
            break;
        }

        switch(resp.axis) {
        case Axis::None:
            ui->axisNone->setChecked(true);
            break;
        case Axis::XAxis:
            ui->axisX->setChecked(true);
            break;
        case Axis::YAxis:
            ui->axisY->setChecked(true);
            break;
        case Axis::ZAxis:
            ui->axisZ->setChecked(true);
            break;
        }

        switch(resp.direction) {
        case Direction::Hold:
            ui->directionHold->setChecked(true);
            break;
        case Direction::Plus:
            ui->directionPlus->setChecked(true);
            break;
        case Direction::Minus:
            ui->directionMinus->setChecked(true);
            break;
        }
    }

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui = new Ui::MainWindow();
        d->ui->setupUi(this);

        d->ui->statusHold->setChecked(true);
        d->ui->axisNone->setChecked(true);
        d->ui->directionHold->setChecked(true);

        connect(&d->updateTimer, &QTimer::timeout, this, [this](){
            d->UpdateGUI();
        });

        if(!d->control.Initialize()) {
            QMessageBox::warning(this, "Joystick", "It fails to initialize joystick controller");
            return;
        }

        d->updateTimer.start(500);
    }
    
    MainWindow::~MainWindow() {
        delete d->ui;
    }
}
