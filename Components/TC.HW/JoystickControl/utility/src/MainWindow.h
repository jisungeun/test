#pragma once
#include <memory>

#include "QMainWindow"

namespace TC::JoystickControl {
    class MainWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}