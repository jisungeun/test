#pragma once
#include <memory>

#include <QByteArray>

#include "JoystickControl.h"

namespace TC::JoystickControl {
    class JoystickControlSpaceMouse : public JoystickControl {
    public:
        JoystickControlSpaceMouse();
        virtual ~JoystickControlSpaceMouse();

        auto Initialize() -> bool override;
        auto Read()->Response override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
