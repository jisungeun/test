#define LOGGER_TAG "[Joystick]"

#include <QByteArray>
#include <QElapsedTimer>
#include <QThread>

#include <TCLogger.h>

#include "SDLJoystick.h"
#include "JoystickControlSpaceMouse.h"

namespace TC::JoystickControl {

    struct JoystickControlSpaceMouse::Impl {
        std::shared_ptr<SDLJoystick> joystick{ nullptr };
        bool moving{ false };
        bool initFailed{ false };
    };

    JoystickControlSpaceMouse::JoystickControlSpaceMouse() : JoystickControl(), d{ new Impl }{
    }

    JoystickControlSpaceMouse::~JoystickControlSpaceMouse() {
    }

    auto JoystickControlSpaceMouse::Initialize() -> bool {
        if(d->initFailed) return false;

        d->joystick.reset(new SDLJoystick());

        if (!d->joystick->Initialize(0)) {
            Update("Joystick initialization is failed", JoystickErrorCode::Initialize_Failure);
            d->initFailed = true;
            return false;
        }

        Update("Joystick is initialized");
        return true;
    }

    auto JoystickControlSpaceMouse::Read() -> Response {

        Response response;

        JoystickValue jvalue;
        JoystickAxisMap axisValue;

        d->joystick->CheckUpated(jvalue, axisValue);

        quint8 axisNo;
        const auto axisChanged = jvalue.AxisChanged(axisNo);
        //QLOG_INFO() << "axis_changed=" << axisChanged << " axisNo=" << axisNo << " moving=" << d->moving;

        if (axisChanged && !d->moving) {
            response.status = Status::Move;
            switch (axisNo) {
            case 0:
                response.axis = Axis::XAxis;
                response.direction = (jvalue.GetX() < 0) ? Direction::Plus : Direction::Minus;
                break;
            case 1:
                response.axis = Axis::YAxis;
                response.direction = (jvalue.GetY() < 0) ? Direction::Minus : Direction::Plus;
                break;
            case 2:
                response.axis = Axis::ZAxis;
                response.direction = (jvalue.GetZ() < 0) ? Direction::Plus : Direction::Minus;
                break;
            default:
                Update(QString("Invalid axis number : %1").arg(axisNo));
                break;
            }
            d->moving = true;
        } else if (!axisChanged && d->moving) {
            d->moving = false;
            response.status = Status::Stop;
        } else {
            response.status = Status::Hold;
        }

        return response;
    }
}
