#define LOGGER_TAG "[Joystick]"

#include <QMap>

#include <TCLogger.h>
#include "JoystickControl.h"

namespace TC::JoystickControl {
    struct JoystickControl::Impl {
        JoystickErrorCode lastError{ JoystickErrorCode::NoError };
    };

    JoystickControl::JoystickControl() : d{ new Impl } {
    }

    JoystickControl::~JoystickControl() {
    }


    auto JoystickControl::GetLastError() const -> JoystickErrorCode {
        return d->lastError;
    }

    auto JoystickControl::SetLastError(JoystickErrorCode code) -> void {
        d->lastError = code;
    }

    auto JoystickControl::Update(const QString& message, JoystickErrorCode code) -> void {
        if (code == +JoystickErrorCode::NoError) {
            QLOG_INFO() << message;
        } else {
            SetLastError(code);
            QLOG_ERROR() << message << "[Error=" << code << "]";
        }
    }
}
