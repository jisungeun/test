#define LOGGER_TAG "[Joystick]"

#include <TCLogger.h>
#include "SDLJoystick.h"

namespace TC::JoystickControl {
	SDLJoystick::SDLJoystick(QObject* parent)
		: QObject(parent)
		, m_pHandle(nullptr) {
	}

	SDLJoystick::~SDLJoystick()	{
		if (SDL_WasInit(SDL_INIT_JOYSTICK) != 0) {
			SDL_Quit();
		}
	}

	bool SDLJoystick::Initialize(const int idx)	{
		if (SDL_WasInit(SDL_INIT_JOYSTICK) != 0) {
			SDL_Quit();
		}

		if (SDL_Init(SDL_INIT_JOYSTICK) != 0) {
			QLOG_ERROR() << "SDL_INIT_JOYSTICK is fail";
			return false;
		}

		const int count = SDL_NumJoysticks();
		if (idx >= count) {
			QLOG_ERROR() << "Invalid joystick index (" << idx + 1 << "/" << count << ")";
			SDL_Quit();
			return false;
		}

		m_pHandle = SDL_JoystickOpen(idx);
		if (!m_pHandle)	{
			QLOG_ERROR() << "Failed to open SDL_Joystick";
			SDL_Quit();
			return false;
		}

		const int numAxes = SDL_JoystickNumAxes(m_pHandle);
		for (int k = 0; k < numAxes; k++) {
			m_status.m_axes[k] = 0;
			m_axisThreshold[k] = 500;
		}

		const int numButtons = SDL_JoystickNumButtons(m_pHandle);
		for (int k = 0; k < numButtons; k++) m_status.m_buttons[k] = 0;

		return true;
	}

	bool SDLJoystick::CheckUpated(JoystickValue& status, QMap<quint8, qint16>& value) {
		if (!m_pHandle) return false;

		bool bAxisChanged = false;
		bool bButtonChanged = false;

		try	{
			JoystickValue currentValue = Update();

			for (int k = 0; k < GetNumAxes(); k++) {
				Sint16 delta = currentValue.m_axes[k] - m_status.m_axes[k];
				if (abs(delta) < m_axisThreshold[k]) {
					status.m_axes[k] = 0;
				} else {
					//QLOG_INFO() << "   axis=" << k << " delta=" << delta << "  th=" << m_axisThreshold[k];
					status.m_axes[k] = delta;
					bAxisChanged = true;
				}

				value[k] = delta;
			}

			for (int k = 0; k < GetNumButtons(); k++) {
				status.m_buttons[k] = currentValue.m_buttons[k];
				//QLOG_INFO() << "    button=" << k << " value=" << currentValue.m_buttons[k] << "  prev=" << m_status.m_buttons[k];
				if (currentValue.m_buttons[k] != m_status.m_buttons[k]) {
					bButtonChanged = true;
					status.m_ButtonsChanged[k] = true;
				} else {
					status.m_ButtonsChanged[k] = false;
				}
			}

			m_status = currentValue;
		} catch (std::exception& ex) {
			QLOG_ERROR() << "Failed to update value - " << ex.what();
			return false;
		}

		return (bAxisChanged | bButtonChanged);
	}

	Sint16 JoystickValue::GetX(void) const {
		Sint16 val = m_axes[0];

		if (abs(m_axes[4]) > abs(m_axes[0])) val = (-1) * m_axes[4];

		return val;
	}

	Sint16 JoystickValue::GetY(void) const {
		Sint16 val = m_axes[1];

		if (abs(m_axes[3]) > abs(m_axes[1])) val = m_axes[3];

		return val;
	}

	Sint16 JoystickValue::GetZ(void) const {
		Sint16 val = m_axes[2];

		if (abs(m_axes[5]) > abs(m_axes[2])) val = (-1) * m_axes[5];

		return val;
	}

	JoystickValue SDLJoystick::Update(void)	{
		JoystickValue currentVal;
		if (!m_pHandle) return currentVal;

		SDL_Event event;
		SDL_PollEvent(&event);

		for (int k = 0; k < GetNumAxes(); k++) currentVal.m_axes[k] = SDL_JoystickGetAxis(m_pHandle, k);
		for (int k = 0; k < GetNumButtons(); k++) currentVal.m_buttons[k] = SDL_JoystickGetButton(m_pHandle, k);

		return currentVal;
	}

	bool JoystickValue::AxisChanged(quint8& axis) {
		int tempAxis = -1;
		Sint16 tempValue = 0;

		const int count = m_axes.size();
		for (quint8 idx = 0; idx < count; idx++) {
			if (m_axes[idx] == 0) continue;
			if (abs(m_axes[idx]) > abs(tempValue)) {
				tempAxis = idx2axis(idx);
				tempValue = abs(m_axes[idx]);
			}
		}

		if (tempAxis >= 0) axis = (quint8)tempAxis;

		return (tempAxis >= 0);
	}

	bool JoystickValue::ButtonChanged(quint8& buttons) {
		buttons = 0;
		if (m_ButtonsChanged[0]) buttons |= BUTTON_1;
		if (m_ButtonsChanged[1]) buttons |= BUTTON_2;

		return (buttons > 0);
	}

	quint8 JoystickValue::idx2axis(quint8 idx) {
		static QMap<quint8, quint8> map;

		if (map.isEmpty()) {
			map[0] = 0;
			map[4] = 0;
			map[1] = 1;
			map[3] = 1;
			map[2] = 2;
			map[5] = 2;
		}

		return map[idx];
	}
}