#include "JoystickControlSpaceMouse.h"
#include "JoystickControlFactory.h"

namespace TC::JoystickControl {
    auto JoystickControlFactory::Create(Controller type) -> JoystickControl::Pointer {
        JoystickControl::Pointer instance;

        switch (type) {
        case Controller::SpaceMouse:
            instance.reset(new JoystickControlSpaceMouse());
            break;
        }

        return instance;
    }

}