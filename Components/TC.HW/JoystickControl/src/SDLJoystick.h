#pragma once

#include <memory>
#include <QMap>
#include <QObject>

#include "SDL.h"

namespace TC::JoystickControl {
	class JoystickValue
	{
	public:
		enum
		{
			BUTTON_1 = 0x01,
			BUTTON_2 = 0x02,
			BUTTON_3 = 0x04,
			BUTTON_4 = 0x08
		};
	public:
		JoystickValue() {};
		~JoystickValue() {};

		Sint16 GetX(void) const;
		Sint16 GetY(void) const;
		Sint16 GetZ(void) const;
		inline bool GetButtonPressed(int idx) const { return m_buttons[idx]; }

		bool AxisChanged(quint8& axis);
		bool ButtonChanged(quint8& buttons);

	private:
		quint8 idx2axis(quint8 idx);

	public:
		QMap<quint8, Sint16> m_axes;
		QMap<quint8, Uint8> m_buttons;
		//QMap<quint8,Uint8> m_hats;

		QMap<quint8, bool> m_ButtonsChanged;
	};

	typedef QMap<quint8, qint16> JoystickAxisMap;

	class SDLJoystick : public QObject
	{
		Q_OBJECT
	public:
		typedef SDLJoystick Self;
		typedef std::shared_ptr<Self> Pointer;

	public:
		SDLJoystick(QObject* parent = nullptr);
		virtual ~SDLJoystick();

		bool Initialize(const int idx);
		bool CheckUpated(JoystickValue& status, QMap<quint8, qint16>& value);

		inline int GetNumAxes(void) const { return m_status.m_axes.size(); }
		inline int GetNumButtons(void) const { return m_status.m_buttons.size(); }

		inline void SetThreshold(int idx, Uint16 value) { m_axisThreshold[idx] = value; }

	protected:
		JoystickValue Update(void);

	private:
		SDL_Joystick* m_pHandle;
		JoystickValue m_status;

		QMap<quint8, Uint16> m_axisThreshold;
	};
}