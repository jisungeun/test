#define LOGGER_TAG "[ImageSink]"

#include <QMutexLocker>
#include <QList>

#include <TCLogger.h>

#include "ImageRecorder.h"
#include "ImageSink.h"

namespace TC::CameraControl {
    struct ImageSink::Impl {
        QList<Image::Pointer> images;
        QMutex mutex;
    };

    ImageSink::ImageSink() : IImageSink(), d{ new Impl } {
    }

    ImageSink::~ImageSink() {
    }

    auto ImageSink::Send(const Image::Pointer image) -> bool {
        QMutexLocker locker(&d->mutex);
        d->images.push_back(image);
        ImageRecorder::GetInstance()->AddImage(image);
        return true;
    }

    auto ImageSink::RemainCount() const -> int32_t {
        QMutexLocker locker(&d->mutex);
        return d->images.size();
    }

    auto ImageSink::GetLatestImage(bool bClear) -> Image::Pointer {
        QMutexLocker locker(&d->mutex);
        Image::Pointer image;
        if (d->images.size() == 0) return image;

        image = d->images.last();
        if (bClear) d->images.clear();

        return image;
    }

    auto ImageSink::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->images.clear();
    }
}
