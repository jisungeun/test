#pragma once
#include <memory>

#include <IImageSink.h>

namespace TC::CameraControl {
    class ImageSink : public IImageSink {
    public:
        using Pointer = std::shared_ptr<ImageSink>;

    public:
        ImageSink();
        ~ImageSink();

        auto Send(const Image::Pointer image)->bool override;
        auto RemainCount() const -> int32_t override;

        auto GetLatestImage(bool bClear)->Image::Pointer;
        auto Clear()->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}