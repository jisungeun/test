#pragma once
#include <memory>
#include <QDialog>

#include "CameraSystem.h"

namespace TC::CameraControl {
    class CameraSelectionDialog : public QDialog {
        Q_OBJECT

    public:
        CameraSelectionDialog(QWidget* parent = nullptr);
        ~CameraSelectionDialog() override;

        auto AddCamera(const DeviceInfo& info)->void;
        auto GetSelectedDevice() const->int32_t;

    protected:
        void closeEvent(QCloseEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}