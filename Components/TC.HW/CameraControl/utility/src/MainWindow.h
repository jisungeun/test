#pragma once
#include <memory>

#include <Image.h>

#include "QMainWindow"

namespace TC::CameraControl {
    class MainWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

    protected:
		void closeEvent(QCloseEvent* event) override;

	protected slots:
		void onOpenLog();
		void onQuit();
		void onFindFolder();
		void onStartClicked();
		void onRefresh();
		void onGainChanged(int value);
		void onGainChanged(double value);

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}