#include <QMutexLocker>
#include <QWaitCondition>
#include <QDir>
#include <QList>
#include <QPair>

#pragma warning(push)
#pragma warning(disable: 4819)
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#pragma warning(pop)

#include "ImageConverter.h"
#include "ImageRecorder.h"

namespace TC::CameraControl {
    struct ImageRecorder::Impl {
        QString path;
        QString title;
        uint32_t index{ 1 };
        bool recording{ false };
        QMutex mutex;
        QWaitCondition waitCond;
        bool running{ true };

        QList<QPair<TC::CameraControl::Image::Pointer, uint32_t>> images;
    };

    ImageRecorder::ImageRecorder() : d{ new Impl } {
        start();
    }

    ImageRecorder::~ImageRecorder() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeOne();
        d->mutex.unlock();

        wait();
    }

    auto ImageRecorder::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImageRecorder() };
        return theInstance;
    }

    auto ImageRecorder::StartRecord(const QString& path, const QString& title) -> void {
        QMutexLocker locker(&d->mutex);
        d->path = path;
        d->title = title;
        d->recording = true;

        QDir dir(d->path);
        QStringList filter(QString("%1*.png").arg(title));
        QStringList dirs = dir.entryList(filter, QDir::Files);
        d->index = dirs.size() + 1;
    }

    auto ImageRecorder::StopRecord() -> void {
        QMutexLocker locker(&d->mutex);
        d->recording = false;
    }

    auto ImageRecorder::AddImage(TC::CameraControl::Image::Pointer image)->void {
        QMutexLocker locker(&d->mutex);
        if (!d->recording) return;

        d->images.push_back(QPair(image, d->index));
        d->index += 1;

        d->waitCond.wakeOne();
    }

    void ImageRecorder::run() {
        while (d->running) {
            QMutexLocker locker(&d->mutex);
            if (d->images.size() == 0) {
                d->waitCond.wait(locker.mutex());
                continue;
            }

            auto item = d->images.takeFirst();
            locker.unlock();

            const auto& image = item.first;
            cv::Mat cvImg(image->GetHeight(), image->GetWidth(), CV_8UC1, const_cast<uchar*>(image->GetData().get()), image->GetWidth());

            const auto path = QString("%1/%2-%3.png").arg(d->path).arg(d->title).arg(item.second, 4, 10, QLatin1Char('0'));
            cv::imwrite(path.toLocal8Bit().constData(), cvImg);
        }
    }
}
