#include <QList>
#include <QMessageBox>

#include "ui_CameraSelectionDialog.h"
#include "CameraSelectionDialog.h"

#include <QCloseEvent>

namespace TC::CameraControl {
    struct CameraSelectionDialog::Impl {
        QList<DeviceInfo> list;
        Ui::CameraSelectionDialog ui;
    };

    CameraSelectionDialog::CameraSelectionDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.tableWidget->setColumnCount(4);
        d->ui.tableWidget->setHorizontalHeaderLabels({"Model", "Vendor ID", "Product ID", "Serial"});
        d->ui.tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.tableWidget->setColumnWidth(0, 150);
        d->ui.tableWidget->setSelectionMode(QAbstractItemView::SelectionMode::ExtendedSelection);
    }

    CameraSelectionDialog::~CameraSelectionDialog() {
    }

    auto CameraSelectionDialog::AddCamera(const DeviceInfo& info) -> void {
        d->list.push_back(info);

        const auto idx = d->ui.tableWidget->rowCount();
        d->ui.tableWidget->setRowCount(idx+1);

        auto* item = new QTableWidgetItem(info.modelName);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        d->ui.tableWidget->setItem(idx, 0, item);

        item = new QTableWidgetItem(info.vendorID);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        d->ui.tableWidget->setItem(idx, 1, item);

        item = new QTableWidgetItem(info.productID);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        d->ui.tableWidget->setItem(idx, 2, item);

        item = new QTableWidgetItem(info.serialNumber);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        d->ui.tableWidget->setItem(idx, 3, item);
    }

    auto CameraSelectionDialog::GetSelectedDevice() const -> int32_t {
        const auto& ranges = d->ui.tableWidget->selectedRanges();
        if(ranges.size() != 1) return -1;

        return ranges.at(0).topRow();
    }

    void CameraSelectionDialog::closeEvent(QCloseEvent* event) {
        if(GetSelectedDevice() == -1) {
            QMessageBox::warning(this, "Camera", "Select the camera to proceed to the next step.");
            event->ignore();
            return;
        }

        QDialog::closeEvent(event);
    }
}
