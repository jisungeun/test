#include <CameraControlFactory.h>
#include <ImageSink.h>

#include "ImageRecorder.h"
#include "MainWindowControl.h"

namespace TC::CameraControl {
    struct MainWindowControl::Impl {
        ImageSink::Pointer sink{ nullptr };
        CameraSystem::Pointer cameraSystem{ nullptr };
        CameraControl::Pointer cameraControl{ nullptr };
        QString title;
        QString path;
        bool acquisitionMode{ false };
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
        d->sink.reset(new ImageSink());
        d->cameraSystem = CameraControlFactory::Create(CameraControlFactory::Controller::PointGrey);
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::GetCameras() const -> QList<DeviceInfo> {
        return d->cameraSystem->GetDeviceInfos();
    }

    auto MainWindowControl::Initialize(int32_t cameraIndex) -> bool {
        const auto& infos = d->cameraSystem->GetDeviceInfos();
        if(cameraIndex >= infos.size()) return false;

        const auto& deviceInfo = infos.at(cameraIndex);
        d->cameraControl = d->cameraSystem->GetCamera(deviceInfo.serialNumber, d->sink.get());
        return d->cameraControl->Initialize();
    }

    auto MainWindowControl::GetSerial() const -> QString {
        if(d->cameraControl == nullptr) return QString();
        return d->cameraControl->GetSerial();
    }

    auto MainWindowControl::GetModel() const -> QString {
        if(d->cameraControl == nullptr) return QString();
        return d->cameraControl->GetModel();
    }

    auto MainWindowControl::CleanUp() -> void {
        d->cameraControl->CleanUp();
    }

    auto MainWindowControl::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto MainWindowControl::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto MainWindowControl::StartAcquisition(bool bRecord) -> bool {
        if (bRecord) {
            ImageRecorder::GetInstance()->StartRecord(d->path, d->title);
        }

        auto res = d->cameraControl->StartAcquisition(0);
        if (res) d->acquisitionMode = true;
        return res;
    }

    auto MainWindowControl::StopAcquisition() -> bool {
        ImageRecorder::GetInstance()->StopRecord();

        auto res = d->cameraControl->StopAcquisition();
        if (res) d->acquisitionMode = false;
        return res;
    }

    auto MainWindowControl::IsAcquisitionState() const -> bool {
        return d->acquisitionMode;
    }

    auto MainWindowControl::GetLatestImage() -> Image::Pointer {
        return d->sink->GetLatestImage(true);
    }

    auto MainWindowControl::GetGainRange(double& gainMin, double& gainMax) -> void {
        d->cameraControl->GetGainRange(gainMin, gainMax);
    }

    auto MainWindowControl::GetGain() const -> double {
        return d->cameraControl->GetGain();
    }
    auto MainWindowControl::SetGain(const double gain) -> void {
        d->cameraControl->SetGain(gain);
    }

    auto MainWindowControl::ChangeTriggerSource(TriggerSource source) -> void {
        d->cameraControl->SetTriggerSource(source);
    }
}
