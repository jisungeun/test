#define LOGGER_TAG "[UI]"
#include <QStandardPaths>
#include <QDesktopServices>
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QTimer>
#include <QCloseEvent>

#include <TCLogger.h>

#include "ui_mainwindow.h"
#include "Version.h"
#include "ImageConverter.h"
#include "CameraSelectionDialog.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace TC::CameraControl {
    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };
        MainWindowControl* control;
        QTimer timer;
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui = new Ui::MainWindow();
        d->ui->setupUi(this);
        d->control = new MainWindowControl();

        CameraSelectionDialog selectionDialog;
        const auto& deviceInfos = d->control->GetCameras();
        if(deviceInfos.isEmpty()) {
            QMessageBox::warning(this, "Camera Console", "No camera is found");
            setDisabled(true);
            return;
        }

        for(const auto& info : deviceInfos) {
            selectionDialog.AddCamera(info);
        }
        selectionDialog.exec();

        const auto cameraIdx = selectionDialog.GetSelectedDevice();
        if (!d->control->Initialize(cameraIdx)) {
            QMessageBox::warning(this, "Initialization", "Initialization is failed");
            d->ui->startBtn->setDefault(true);
        }

        const auto& serial = d->control->GetSerial();
        const auto& model = d->control->GetModel();
        setWindowTitle(QString("HTX Camera Console %1 [%2 / %3]").arg(PROJECT_REVISION).arg(model).arg(serial));

        double gainMin, gainMax;
        d->control->GetGainRange(gainMin, gainMax);

        d->ui->gainSlider->setRange(gainMin*1000, gainMax*1000);
        d->ui->gainSpin->setRange(gainMin, gainMax);

        connect(d->ui->actionQuit, SIGNAL(triggered()), this, SLOT(onQuit()));
        connect(d->ui->actionLog, SIGNAL(triggered()), this, SLOT(onOpenLog()));
        connect(d->ui->folderBtn, SIGNAL(clicked()), this, SLOT(onFindFolder()));
        connect(d->ui->startBtn, SIGNAL(clicked()), this, SLOT(onStartClicked()));
        connect(&d->timer, SIGNAL(timeout()), this, SLOT(onRefresh()));
        connect(d->ui->gainSlider, SIGNAL(valueChanged(int)), this, SLOT(onGainChanged(int)));
        connect(d->ui->gainSpin, SIGNAL(valueChanged(double)), this, SLOT(onGainChanged(double)));

        connect(d->ui->actionSoftware, &QAction::triggered, this, 
                [this](){d->control->ChangeTriggerSource(TriggerSource::Software); });
        connect(d->ui->actionLine_0, &QAction::triggered, this, 
                [this](){d->control->ChangeTriggerSource(TriggerSource::Line_0); });
        connect(d->ui->actionLine_1, &QAction::triggered, this, 
                [this](){d->control->ChangeTriggerSource(TriggerSource::Line_1); });
        connect(d->ui->actionLine_2, &QAction::triggered, this, 
                [this](){d->control->ChangeTriggerSource(TriggerSource::Line_2); });

        d->timer.start(50);
    }
    
    MainWindow::~MainWindow() {
        delete d->control;
    }

    void MainWindow::closeEvent(QCloseEvent* event) {
        auto resBtn = QMessageBox::question(this, "HTX Camera Console",
                                            tr("Are you sure?\n"),
                                            QMessageBox::No | QMessageBox::Yes,
                                            QMessageBox::No);
        if (resBtn != QMessageBox::Yes) {
            event->ignore();
        } else {
            d->control->CleanUp();
            event->accept();
        }
    }

    void MainWindow::onOpenLog() {
        const auto path = QString("%1/log").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        QDesktopServices::openUrl(QUrl(QString("file:///%1").arg(path), QUrl::TolerantMode));
    }

    void MainWindow::onQuit() {
        d->control->CleanUp();
        close();
    }

    void MainWindow::onFindFolder() {
        const auto prevPath = QSettings().value("Recording folder", "D:/").toString();
        const auto path = QFileDialog::getExistingDirectory(this, "Select a folder to save images", prevPath);
        if (path.isEmpty()) return;

        QSettings().setValue("Recording folder", path);
        d->ui->folderEdit->setText(path);
    }

    void MainWindow::onStartClicked() {
        if (d->control->IsAcquisitionState()) {
            if (!d->control->StopAcquisition()) {
                QMessageBox::warning(this, "Acquisition", "Failed to stop acquisition");
                return;
            }
            d->ui->startBtn->setText("Start");
        } else {
            if (d->ui->recordCheck->isChecked()) {
                const auto title = d->ui->titleEdit->text();
                const auto path = d->ui->folderEdit->text();
                if (title.isEmpty() || path.isEmpty()) {
                    QMessageBox::information(this, "Acquisition", "Title and Folder are not allowed to be empty");
                    return;
                }

                d->control->SetTitle(title);
                d->control->SetPath(path);
                if (!d->control->StartAcquisition(true)) {
                    QMessageBox::warning(this, "Acquisition", "Failed to start acquisition");
                    return;
                }
            } else {
                if (!d->control->StartAcquisition(false)) {
                    QMessageBox::warning(this, "Acquisition", "Failed to start acquisition");
                    return;
                }
            }
            d->ui->startBtn->setText("Stop");
        }
    }

    void MainWindow::onRefresh() {
        auto image = d->control->GetLatestImage();
        if (image.get()) {
            d->ui->imageView->ShowImage(ImageConverter::Image2QImage(image));
        }
    }

    void MainWindow::onGainChanged(int value) {
        const double gain = value / 1000.0;
        d->control->SetGain(gain);

        d->ui->gainSpin->blockSignals(true);
        d->ui->gainSpin->setValue(gain);
        d->ui->gainSpin->blockSignals(false);
    }

    void MainWindow::onGainChanged(double value) {
        d->control->SetGain(value);

        d->ui->gainSlider->blockSignals(true);
        d->ui->gainSlider->setValue(value*1000.0);
        d->ui->gainSlider->blockSignals(false);
    }
}
