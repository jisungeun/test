#pragma once
#include <memory>

#include <QString>
#include <QThread>

#include <CameraImage.h>

namespace TC::CameraControl {
    class ImageRecorder : public QThread {
        Q_OBJECT

    public:
        typedef std::shared_ptr<ImageRecorder> Pointer;

    protected:
        ImageRecorder();

    public:
        ~ImageRecorder();
        static auto GetInstance()->Pointer;

        auto StartRecord(const QString& path, const QString& title)->void;
        auto StopRecord()->void;

        auto AddImage(Image::Pointer image)->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}