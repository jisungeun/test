#pragma once
#include <memory>

#include <Image.h>

namespace TC::CameraControl {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto GetCameras() const->QList<DeviceInfo>;
        auto Initialize(int32_t cameraIndex)->bool;
        auto GetSerial() const->QString;
        auto GetModel() const->QString;
        auto CleanUp()->void;
        auto SetTitle(const QString& title)->void;
        auto SetPath(const QString& path)->void;
        auto StartAcquisition(bool bRecord)->bool;
        auto StopAcquisition()->bool;
        auto IsAcquisitionState() const->bool;
        auto GetLatestImage()->Image::Pointer;
        auto GetGainRange(double& gainMin, double& gainMax)->void;
        auto GetGain() const->double;
        auto SetGain(const double gain)->void;
        auto ChangeTriggerSource(TriggerSource source)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}