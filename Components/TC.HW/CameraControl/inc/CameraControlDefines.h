#pragma once

#include <enum.h>

namespace TC::CameraControl {
    BETTER_ENUM(TriggerSource, int32_t,
        Software,
        Line_0,
        Line_1,
        Line_2
    );

    BETTER_ENUM(GainConversion, int32_t,
        LCG,
        HCG
    );
}