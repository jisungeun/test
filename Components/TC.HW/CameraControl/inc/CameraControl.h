#pragma once
#include <memory>

#include <QString>

#include "CameraErrorCodes.h"
#include "CameraControlDefines.h"
#include "IImageSink.h"
#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    class TCCameraControl_API CameraControl {
    public:
        typedef std::shared_ptr<CameraControl> Pointer;

    public:
        CameraControl(IImageSink* sink, const QString& alias = QString());
        virtual ~CameraControl();

        auto GetLastError() const->CameraErrorCode;
        auto GetSerial() const->QString;
        auto GetModel() const->QString;
        auto GetAlias() const->QString;

        virtual auto Initialize()->bool = 0;
        virtual auto CleanUp()->bool = 0;
        virtual auto StartAcquisition(uint32_t images)->bool = 0;   //images=0 - infinite
        virtual auto StopAcquisition()->bool = 0;
        virtual auto GetNoAcquiredCount() const->int32_t = 0;
        auto GetNotDeliveredCount() const->int32_t;

        virtual auto GetGainRange(double& gainMin, double& gainMax)->bool = 0;
        virtual auto GetGain()->double = 0;
        virtual auto SetGain(const double gain)->bool = 0;
        virtual auto SetGainConversionMode(GainConversion mode)->bool = 0;

        virtual auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY)->bool = 0;
        virtual auto GetFOV() const->std::tuple<int32_t, int32_t> = 0;

        virtual auto SetTriggerSource(TriggerSource source, bool useModeControl = true)->bool = 0;
        virtual auto SetTriggerMode(bool on)->bool = 0;

    protected:
        auto ClearImages()->void;
        auto SendImage(Image::Pointer& image)->bool;

        auto SetLastError(CameraErrorCode code)->void;
        auto SetSerial(const QString& serial)->void;
        auto SetModel(const QString& model)->void;
        auto Update(const QString& message, CameraErrorCode code = CameraErrorCode::NoError)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}