#pragma once

#include "CameraSystem.h"
#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    class TCCameraControl_API CameraControlFactory {
    public:
        enum class Controller {
            NoCamera,
            PointGrey
        };

        static auto Create(Controller type = Controller::PointGrey)->CameraSystem::Pointer;
    };
}