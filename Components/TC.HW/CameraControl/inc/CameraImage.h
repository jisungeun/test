#pragma once
#include <memory>
#include <QDateTime>

#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    class TCCameraControl_API Image {
    public:
        typedef std::shared_ptr<Image> Pointer;

    public:
        Image();
        Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, void* data);
        virtual ~Image();

        auto IsValid() const->bool;
        auto GetWidth() const->uint32_t;
        auto GetHeight() const->uint32_t;
        auto GetBufferSize() const->uint32_t;
        auto GetBitDepth() const->uint32_t;
        auto GetData() const->std::shared_ptr<uint8_t[]>;

        auto SetIndex(uint64_t index)->void;
        auto GetIndex() const->uint64_t;

        auto GetTimestamp() const->QDateTime;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}