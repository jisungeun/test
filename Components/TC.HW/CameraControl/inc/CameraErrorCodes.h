#pragma once

#include <enum.h>

namespace TC::CameraControl {
    BETTER_ENUM(CameraErrorCode, uint32_t,
                NoError                             = 0,
                AccessFailureToUserSetSelector      = 1,
                AccessFailureToUserSet              = 2,
                FailedToLoadUserSet                 = 3,
                FailedToSetExposureMode             = 4,
                FailedToSetTriggerMode              = 5,
                FailedToSetTriggerSource            = 6,
                FailedToSetTriggerActivation        = 7,
                FailedToSetGain                     = 8,
                FailedToSetChuckMode                = 9,
                FailedToSetExposureTime             = 10,
                FailedToGetGainRange                = 11,
                FailedToGetGain                     = 12,
                FailedToSetGainConversionMode       = 13,

                //Initialization
                NoCameraFound                       = 100,
                NotConnectedToUSB3                  = 101,
                FailedToSetAcquisitionMode          = 102,
                FailedToSetBufferHandlingMode       = 103,
                FailedToSetBufferCountMode          = 104,
                FailedToSetBufferSize               = 105,

                //Acquisition
                FailedToBeginAcquisition            = 200,
                FailedToEndAcquisition              = 201,

                //Last error code
                UnknownError                        = 10000
    )
}