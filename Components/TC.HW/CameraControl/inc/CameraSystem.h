#pragma once
#include <memory>

#include <QString>

#include "CameraControl.h"
#include "IImageSink.h"
#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    struct TCCameraControl_API DeviceInfo {
        int32_t index;
        QString deviceID;
        QString vendorID;
        QString productID;
        QString modelName;
        QString serialNumber;
    };
    
    class TCCameraControl_API CameraSystem {
    public:
        typedef std::shared_ptr<CameraSystem> Pointer;
        
    public:
        CameraSystem();
        virtual ~CameraSystem();

        auto GetLibraryVersion() const->QString;
        auto GetDeviceInfos() const->QList<DeviceInfo>;
        auto CleanUp()->void;

        virtual auto GetCamera(uint32_t index = 0,
                               IImageSink* sink = nullptr,
                               const QString& alias = QString())->CameraControl::Pointer = 0;
        virtual auto GetCamera(const QString& serial,
                               IImageSink* sink = nullptr,
                               const QString& alias = QString())->CameraControl::Pointer = 0;
        
    protected:
        auto SetLibraryVersion(const QString& version)->void;
        auto AddDevice(const DeviceInfo& deviceID)->void;
        auto FindDevice(const QString& serial, DeviceInfo& deviceInfo) const->bool;

        auto RegisterControl(CameraControl::Pointer camera)->void;
        virtual auto PostCleanUp()->void = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}