#pragma once

#include "CameraImage.h"
#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    class TCCameraControl_API IImageSink {
    public:
        IImageSink();
        virtual ~IImageSink();

        virtual auto Send(const Image::Pointer image)->bool = 0;
        virtual auto Clear()->void = 0;

        virtual auto RemainCount() const->int32_t = 0;
    };
}