#define LOGGER_TAG "[Camera"

#include <TCLogger.h>
#include "CameraControl.h"

namespace TC::CameraControl {
    struct CameraControl::Impl {
        CameraErrorCode lastError{ CameraErrorCode::NoError };
        IImageSink* sink{ nullptr };
        QString serial;
        QString model;
        QString alias;
    };

    CameraControl::CameraControl(IImageSink* sink, const QString& alias) : d{ new Impl} {
        d->sink = sink;
        d->alias = alias;
    }

    CameraControl::~CameraControl() {
    }

    auto CameraControl::GetLastError() const -> CameraErrorCode {
        auto code = d->lastError;
        d->lastError = CameraErrorCode::NoError;
        return code;
    }

    auto CameraControl::GetSerial() const -> QString {
        return d->serial;
    }

    auto CameraControl::GetModel() const -> QString {
        return d->model;
    }

    auto CameraControl::GetAlias() const -> QString {
        return d->alias;
    }

    auto CameraControl::GetNotDeliveredCount() const -> int32_t {
        return d->sink->RemainCount();
    }

    auto CameraControl::ClearImages() -> void {
        d->sink->Clear();
    }

    auto CameraControl::SendImage(Image::Pointer& image) -> bool {
        if(!d->sink) return true;
        return d->sink->Send(image);
    }

    auto CameraControl::SetLastError(CameraErrorCode code) -> void {
        d->lastError = code;
    }

    auto CameraControl::SetSerial(const QString& serial) -> void {
        d->serial = serial;
    }

    auto CameraControl::SetModel(const QString& model) -> void {
        d->model = model;
    }

    auto CameraControl::Update(const QString& message, CameraErrorCode code) -> void {
        if(code == +CameraErrorCode::NoError) {
            QLOG_INFO() << d->alias << "]" << message;
        } else {
            SetLastError(code);
            QLOG_ERROR() << d->alias << "]" << message << "[Error=" << code << "]";
        }
    }

}
