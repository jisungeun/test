#include "CameraImage.h"

namespace TC::CameraControl {
    struct Image::Impl {
        uint32_t width{ 0 };
        uint32_t height{ 0 };
        uint32_t size{ 0 };
        uint32_t bitDepth{ 0 };
        std::shared_ptr<uint8_t[]> data;
        uint64_t index{ 0 };
        QDateTime timestamp;
    };

    Image::Image() : d{ new Impl } {
	    d->timestamp = QDateTime::currentDateTime();
    }

    Image::Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, void* data) : d{ new Impl } {
        d->width = width;
        d->height = height;
        d->size = size;
        d->bitDepth = bitDepth;

        d->data.reset(new uint8_t[d->size]);
        memcpy_s(d->data.get(), d->size, data, d->size);

	    d->timestamp = QDateTime::currentDateTime();
    }

    Image::~Image() {
    }

    auto Image::IsValid() const -> bool {
        return (d->data != nullptr);
    }

    auto Image::GetWidth() const -> uint32_t {
        return d->width;
    }

    auto Image::GetHeight() const -> uint32_t {
        return d->height;
    }

    auto Image::GetBufferSize() const -> uint32_t {
        return d->size;
    }

    auto Image::GetBitDepth() const -> uint32_t {
        return d->bitDepth;
    }

    auto Image::GetData() const -> std::shared_ptr<uint8_t[]> {
        return d->data;
    }

    auto Image::SetIndex(uint64_t index) -> void {
        d->index = index;
    }

    auto Image::GetIndex() const -> uint64_t {
        return d->index;
    }

    auto Image::GetTimestamp() const -> QDateTime {
        return d->timestamp;
    }
}
