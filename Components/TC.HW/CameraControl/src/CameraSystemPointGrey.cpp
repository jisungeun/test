#define LOGGER_TAG "[Camera]"

#include <Spinnaker.h>
#include <SpinGenApi/SpinnakerGenApi.h>

#include <TCLogger.h>

#include "CameraControlPointGrey.h"
#include "CameraSystemPointGrey.h"

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;

namespace TC::CameraControl::PointGrey {
    struct CameraSystemPointGrey::Impl {
        SystemPtr cameraSystem;
        CameraList cameraList;
        bool valid{ false };

        auto PrintDeviceInfo(const uint32_t index, const Spinnaker::GenApi::INodeMap & nodeMap)->void;
        auto GetDeviceInfo(const Spinnaker::GenApi::INodeMap & nodeMap, DeviceInfo& deviceInfo)->bool;
    };

    auto CameraSystemPointGrey::Impl::PrintDeviceInfo(const uint32_t index,
                                                      const Spinnaker::GenApi::INodeMap& nodeMap) -> void {
        QLOG_INFO() << "*** CAMERA INFORMATION ***";
        QLOG_INFO() << ">>  Index  -  " << index + 1;

        try {
            FeatureList_t features;
            const CCategoryPtr category = nodeMap.GetNode("DeviceInformation");

            if (IsAvailable(category) && IsReadable(category)) {
                category->GetFeatures(features);
                for (auto& feature : features) {
                    const CNodePtr pfeatureNode = feature;
                    CValuePtr pValue = static_cast<CValuePtr>(pfeatureNode);

                    QLOG_INFO() << ">> " << pfeatureNode->GetName() << " - " << (IsReadable(pValue) ? pValue->ToString() : "Node not readable");
                }
            } else {
                QLOG_ERROR() << "Device control information not available.";
            }
        } catch (Spinnaker::Exception& e) {
            QLOG_ERROR() << "Error: " << e.what();
        }
    }

    auto CameraSystemPointGrey::Impl::GetDeviceInfo(const Spinnaker::GenApi::INodeMap& nodeMap, DeviceInfo& deviceInfo)->bool {
        auto parse = [](const QString& fullID)->std::tuple<QString,QString> {
            QString vid, pid;

            auto toks = fullID.split("&");
            for(auto& tok : toks) {
                if(tok.contains("USB\\VID_")) {
                    vid = tok.remove("USB\\VID_");
                } else if(tok.contains("PID_")) {
                    pid = tok.remove("PID_");
                }
            }

            return std::make_tuple(vid, pid);
        };

        try {
            FeatureList_t features;
            const CCategoryPtr category = nodeMap.GetNode("DeviceInformation");

            if (IsAvailable(category) && IsReadable(category)) {
                category->GetFeatures(features);
                for (auto& feature : features) {
                    const CNodePtr pfeatureNode = feature;
                    CValuePtr pValue = static_cast<CValuePtr>(pfeatureNode);

                    const QString featureName{pfeatureNode->GetName().c_str()};
                    if(featureName == "DeviceID") {
                        if(!IsReadable(pValue)) {
                            QLOG_ERROR() << "It can't get device ID";
                            return false;
                        }

                        deviceInfo.deviceID = pValue->ToString();
                        std::tie(deviceInfo.vendorID, deviceInfo.productID) = parse(deviceInfo.deviceID);
                    } else if(featureName == "DeviceModelName") {
                        if(!IsReadable(pValue)) {
                            QLOG_ERROR() << "It can't get device model name";
                            return false;
                        }

                        deviceInfo.modelName = pValue->ToString();
                    } else if(featureName == "DeviceSerialNumber") {
                        if(!IsReadable(pValue)) {
                            QLOG_ERROR() << "It can't get device serial number";
                            return false;
                        }

                        deviceInfo.serialNumber = pValue->ToString();
                    }
                }
            } else {
                QLOG_ERROR() << "Device control information not available.";
                return false;
            }
        } catch (Spinnaker::Exception& e) {
            QLOG_ERROR() << "Error: " << e.what();
            return false;
        }

        return true;
    }

    CameraSystemPointGrey::CameraSystemPointGrey() : CameraSystem(), d{ new Impl } {
        d->valid = false;

        try {
            d->cameraSystem = System::GetInstance();

            const auto spVersion = d->cameraSystem->GetLibraryVersion();
            const auto version = QString("%1.%2.%3.%4").arg(spVersion.major).arg(spVersion.minor).arg(spVersion.type).arg(spVersion.build);
            SetLibraryVersion(version);
            QLOG_INFO() << QString("Library version: %1").arg(version);
            QLOG_INFO() << QString("Is in use : %1").arg(d->cameraSystem->IsInUse());
        } catch(Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("Failed to prepare camera system - %1").arg(ex.what());
            return;
        } catch(std::exception& ex) {
            QLOG_ERROR() << QString("Failed to prepare camera system - %1").arg(ex.what());
            return;
        }

        d->cameraList = d->cameraSystem->GetCameras();
        const auto numCameras = d->cameraList.GetSize();

        if(numCameras==0) {
            QLOG_ERROR() << "No camera found";
            return;
        }

        QLOG_INFO() << QString("Camera count : %1").arg(numCameras);

        for(auto idx=0u; idx<numCameras; idx++) {
            auto camera = d->cameraList.GetByIndex(idx);
            INodeMap& nodeMapTLDevice = camera->GetTLDeviceNodeMap();
            d->PrintDeviceInfo(idx, nodeMapTLDevice);

            DeviceInfo deviceInfo;
            if(d->GetDeviceInfo(nodeMapTLDevice, deviceInfo)) {
                deviceInfo.index = idx;
                AddDevice(deviceInfo);
            }
        }

        d->valid = true;
    }

    CameraSystemPointGrey::~CameraSystemPointGrey() {
    }

    auto CameraSystemPointGrey::GetCamera(uint32_t index,
                                          IImageSink* sink,
                                          const QString& alias) -> CameraControl::Pointer {
        if(!d->valid) return nullptr;

        auto cameraInstance = d->cameraList.GetByIndex(index);
        if(cameraInstance == nullptr) return nullptr;

        CameraControlPointGrey::Pointer camera{ new PointGrey::CameraControlPointGrey(cameraInstance, sink, alias) };
        RegisterControl(camera);
        return camera;
    }

    auto CameraSystemPointGrey::GetCamera(const QString& serial,
                                          IImageSink* sink,
                                          const QString& alias) -> CameraControl::Pointer {
        if(!d->valid) return nullptr;
        DeviceInfo deviceInfo;
        if(!FindDevice(serial, deviceInfo)) return nullptr;

        auto cameraInstance = d->cameraList.GetByIndex(deviceInfo.index);
        if(cameraInstance == nullptr) return nullptr;

        CameraControlPointGrey::Pointer camera{ new PointGrey::CameraControlPointGrey(cameraInstance, sink, alias) };
        RegisterControl(camera);
        return camera;
    }

    auto CameraSystemPointGrey::PostCleanUp() -> void {
        if(!d->valid) return;

        try {
            const auto count = d->cameraList.GetSize();
            for(auto idx=0u; idx<count; idx++) {
                auto camera = d->cameraList.GetByIndex(idx);
                camera = nullptr;
            }
            d->cameraList.Clear();
            d->cameraSystem->ReleaseInstance();
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("Clean up error - %1").arg(ex.what());
            QLOG_ERROR() << QString("Error code %1 raised in function %2 at line %3").arg(ex.GetError())
                            .arg(ex.GetFunctionName()).arg(ex.GetLineNumber());
        }
    }
}
