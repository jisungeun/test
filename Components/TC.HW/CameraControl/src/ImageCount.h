#pragma once
#include <memory>

#include "TCCameraControlExport.h"

namespace TC::CameraControl {
    class TCCameraControl_API ImageCount {
    public:
        ImageCount();
        ~ImageCount();

        auto Add()->void;
        auto Clear()->void;
        auto Count() const->uint64_t;
        auto IsReached(uint64_t count)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}