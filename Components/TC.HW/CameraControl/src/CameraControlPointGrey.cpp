#define LOGGER_TAG "[Camera]"

#include <numeric>
#include <QMutexLocker>
#include <QWaitCondition>
#include <QElapsedTimer>

#include <Spinnaker.h>
#include <SpinGenApi/SpinnakerGenApi.h>

#include <TCLogger.h>

#include "ImageCount.h"
#include "CameraControlPointGrey.h"

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;

namespace TC::CameraControl::PointGrey {
    auto IsSupported(const char* nodeName, Spinnaker::GenApi::INodeMap& nodeMap)->bool;
    auto SetEnumParameter(const char* nodeName, const char* entryName, Spinnaker::GenApi::INodeMap& nodeMap, bool ignoreLocked = false)->bool;
    auto SetIntegerParameter(const char* nodeName, int64_t value, Spinnaker::GenApi::INodeMap& nodeMap, bool force = true)->bool;
    auto SetFloatParameter(const char* nodeName, float value, Spinnaker::GenApi::INodeMap& nodeMap, bool force = true)->bool;
    auto PrintDeviceInfo(const uint32_t index, const Spinnaker::GenApi::INodeMap & nodeMap)->void;
    auto GetDeviceID(const Spinnaker::GenApi::INodeMap & nodeMap)->QString;

    struct CameraControlPointGrey::Impl {
        Spinnaker::CameraPtr camera;

        QMutex mutex;
        QWaitCondition waitCond;
        uint32_t imagesToGet{ 0 };      //0 - infinite
        ImageCount imageCount;
        bool grabbing{ false };
        bool running{ true };
        bool initialized{ false };

        auto GetROIStep()->int32_t;
        auto GetOffsetStep()->int32_t;
        auto GetTriggerSourceStr(TriggerSource source)->QString;
    };

    auto CameraControlPointGrey::Impl::GetROIStep() -> int32_t {
        if (!camera->IsInitialized()) return 0;
        const auto incX = camera->Width.GetInc();
        const auto incY = camera->Height.GetInc();
        return std::lcm(incX, incY);    //least common multiple of incX and incY
    }

    auto CameraControlPointGrey::Impl::GetOffsetStep() -> int32_t {
        const auto incX = camera->OffsetX.GetInc();
        const auto incY = camera->OffsetY.GetInc();
        return std::lcm(incX, incY);    //least common multiple of incX and incY
    }

    auto CameraControlPointGrey::Impl::GetTriggerSourceStr(TriggerSource source) -> QString {
        QString str = "Software";

        switch(source) {
        case TriggerSource::Software:
            str = "Software";
            break;
        case TriggerSource::Line_0:
            str = "Line0";
            break;
        case TriggerSource::Line_1:
            str = "Line1";
            break;
        case TriggerSource::Line_2:
            str = "Line2";
            break;
        }
        return str;
    }

    CameraControlPointGrey::CameraControlPointGrey(Spinnaker::CameraPtr camera,
                                                   IImageSink* sink,
                                                   const QString& alias) : CameraControl(sink, alias), d{ new Impl } {
        d->camera = camera;
    }

    CameraControlPointGrey::~CameraControlPointGrey() {
        if(d->initialized) {
            Update("Camera is not cleaned up");
        }
        Update("Camera instance is destroyed");
    }

    auto CameraControlPointGrey::Initialize() -> bool {
        try {
            d->camera->Init();
            Update(QString("Camera serial: %1").arg(d->camera->DeviceID.GetValue().c_str()));

            d->camera->AcquisitionStop();
            LoadDefaultSetting();

            //Set Serial & Model
            const auto serial = d->camera->DeviceSerialNumber.GetValue();
            SetSerial(QString::fromStdString(serial.c_str()));

            const auto model = d->camera->DeviceModelName.GetValue();
            SetModel(QString::fromStdString(model.c_str()));

            //GenApi::IEnumerationT<DeviceCurrentSpeedEnum>& DeviceCurrentSpeed
            INodeMap& genTLNodeMap = d->camera->GetTLDeviceNodeMap();

            CEnumerationPtr currentSpeed = static_cast<CEnumerationPtr>(genTLNodeMap.GetNode("DeviceCurrentSpeed"));
            if (currentSpeed->GetCurrentEntry()->GetSymbolic().compare("SuperSpeed")!=0) {
                Update(QString("Camera is not connected to USB 3.0 port or not recognized as USB 3.0. [val=%1]").arg(currentSpeed->GetCurrentEntry()->GetSymbolic().c_str()), CameraErrorCode::NotConnectedToUSB3);
                return false;
            }

            //Set continuous acquisition
            d->camera->AcquisitionMode.SetValue(AcquisitionMode_Continuous);
            if (d->camera->AcquisitionMode.GetValue() != AcquisitionMode_Continuous) {
                Update("Unable to set acquisition mode to continuous", CameraErrorCode::FailedToSetAcquisitionMode);
                return false;
            }

            //Set buffer handling mode and size
            INodeMap& streamNodeMap = d->camera->GetTLStreamNodeMap();
            if(!SetEnumParameter("StreamBufferHandlingMode", "OldestFirst", streamNodeMap)) {
                Update("Unable to set buffer handling mode", CameraErrorCode::FailedToSetBufferHandlingMode);
                return false;
            }

            if(!SetEnumParameter("StreamBufferCountMode", "Manual", streamNodeMap)) {
                Update("Unable to set buffer count mode", CameraErrorCode::FailedToSetBufferCountMode);
                return false;
            }

            if(!SetIntegerParameter("StreamBufferCountManual", 500, streamNodeMap)) {
                Update("Unable to set buffer size", CameraErrorCode::FailedToSetBufferSize);
                return false;
            } else {
                CIntegerPtr ptrBufferCount = streamNodeMap.GetNode("StreamBufferCountManual");
                Update(QString("Default Buffer Count: %1").arg(ptrBufferCount->GetValue()));
                Update(QString("Maximum Buffer Count: %1").arg(ptrBufferCount->GetMax()));
            }
        } catch(Spinnaker::Exception& ex) {
            Update(QString("Failed to initialize camera - %1").arg(ex.what()));
            return false;
        } catch(std::exception& ex) {
            Update(QString("Failed to initialize camera - %1").arg(ex.what()));
            return false;
        }

        d->initialized = true;
        Update("Initialization is completed");

        start();

        return true;
    }

    auto CameraControlPointGrey::CleanUp() -> bool {
        QMutexLocker locker(&d->mutex);
        if(!d->initialized) return false;
        locker.unlock();

        StopAcquisition();

        locker.relock();
        if(isRunning()) {
            Update(QString("Grabbing thread is running now [grabbing=%1 running=%2]").arg(d->grabbing).arg(d->running));
            d->running = false;
            d->waitCond.wakeOne();
        }
        locker.unlock();

        if(!wait()) {
            Update("Grabbing thread is not finished");
        }

        try {
            ClearImages();
            d->camera->DeInit();
            d->camera = nullptr;
        } catch (Spinnaker::Exception& ex) {
            Update(QString("Clean up error - %1").arg(ex.what()));
            Update(QString("Error code %1 raised in function %2 at line %3").arg(ex.GetError())
                   .arg(ex.GetFunctionName()).arg(ex.GetLineNumber()));
        }

        d->initialized = false;

        Update(QString("Camera instance[%1] is cleaned up").arg(GetSerial()));

        return true;
    }

    auto CameraControlPointGrey::StartAcquisition(uint32_t images) -> bool {
        d->mutex.lock();
        if(!d->initialized) return false;;
        d->imagesToGet = images;
        d->imageCount.Clear();
        d->mutex.unlock();

        try {
            if (d->camera->IsStreaming()) {
                Update("It is already streaming");
                return true;
            }

            d->mutex.lock();
            d->grabbing = true;
            d->waitCond.wakeOne();
            d->mutex.unlock();

            ClearImages();

            if(images>0) Update(QString("It will acquire %1 images").arg(images));

            d->camera->BeginAcquisition();
        } catch (Spinnaker::Exception& ex) {
            Update(QString("Failed to start acquisition - %1").arg(ex.what()), CameraErrorCode::FailedToBeginAcquisition);
            return false;
        }

        Update(QString("is streaming[2]=%1").arg(d->camera->IsStreaming()));

        Update("Acquisition is started");

        return true;
    }

    auto CameraControlPointGrey::StopAcquisition() -> bool {
        d->mutex.lock();
        if(!d->initialized) return false;
        const auto isGrabbing = d->grabbing;
        d->mutex.unlock();

        Update(QString("is streaming[0]=%1").arg(d->camera->IsStreaming()));

        if (isGrabbing) {
            if ((d->imagesToGet > 0) && !d->imageCount.IsReached(d->imagesToGet)) {
                Update(QString("Waiting for the end of acquisition. [%1/%2])").arg(d->imageCount.Count()).arg(d->imagesToGet));

                QElapsedTimer timer;
                timer.start();

                auto count = d->imageCount.Count();
                while ((timer.elapsed() < 500) && !d->imageCount.IsReached(d->imagesToGet)) {
                    QThread::msleep(50);
                    if(count != d->imageCount.Count()) {
                        timer.restart();
                        count = d->imageCount.Count();
                    }
                }
            }

            d->mutex.lock();
            d->grabbing = false;
            d->waitCond.wakeOne();
            d->mutex.unlock();

            try {
                d->camera->EndAcquisition();
            } catch (Spinnaker::Exception& ex) {
                Update(QString("Failed to stop grabbing - %1").arg(ex.what()), CameraErrorCode::FailedToEndAcquisition);
                return false;
            }
            ClearImages();
            Update("Acquisition is stopped");
        }

        Update(QString("is streaming[1]=%1").arg(d->camera->IsStreaming()));

        return true;
    }

    auto CameraControlPointGrey::GetNoAcquiredCount() const -> int32_t {
        QMutexLocker locker(&d->mutex);
        return std::max<int32_t>(0, d->imagesToGet - d->imageCount.Count());
    }

    auto CameraControlPointGrey::GetGainRange(double& gainMin, double& gainMax) -> bool {
        try {
            gainMin = d->camera->Gain.GetMin();
            gainMax = d->camera->Gain.GetMax();
        } catch(Spinnaker::Exception& ex) {
            Update(QString("unable to get gain range- %1").arg(ex.what()), CameraErrorCode::FailedToGetGainRange);
            return false;
        }

        Update(QString("Gain range - (%1, %2)").arg(gainMin).arg(gainMax));
        return true;
    }

    auto CameraControlPointGrey::GetGain() -> double {
        double gainValue = 0.0;

        try {
            gainValue = d->camera->Gain.GetValue();
        } catch(Spinnaker::Exception& ex) {
            Update(QString("unable to get gain - %1").arg(ex.what()), CameraErrorCode::FailedToGetGain);
            return false;
        }

        Update(QString("Get gain - %1").arg(gainValue));

	    return gainValue;
    }

    auto CameraControlPointGrey::SetGain(const double gain) -> bool {
        return ChangeGain(gain);
    }

    auto CameraControlPointGrey::SetGainConversionMode(GainConversion mode) -> bool {
        try {
            INodeMap& nodeMap = d->camera->GetNodeMap();

            if(!IsSupported("GainConversion", nodeMap)) {
                Update(QString("Gain conversion mode is not supported"));
                return true;
            }

            const auto value = (mode == +GainConversion::LCG) ? "LCG" : "HCG";
            if (!SetEnumParameter("GainConversion", value, nodeMap)) {
                Update("unable to set gain conversion mode", CameraErrorCode::FailedToSetGainConversionMode);
                return false;
            }
        } catch(Spinnaker::Exception& ex) {
            Update(QString("unable to set gain conversion mode - %1").arg(ex.what()), CameraErrorCode::FailedToSetGainConversionMode);
            return false;
        }

        Update(QString("Set gain conversion mode - %1").arg(mode._to_string()));

        return true;
    }

    auto CameraControlPointGrey::ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool {
        int32_t curROIX, curROIY;
        std::tie(curROIX, curROIY) = GetFOV();
        if ((curROIX == xPixels) && (curROIY == yPixels)) {
            QLOG_INFO() << "ROI(" << xPixels << "," << yPixels << ") is same as the current";
            return true;
        }

        const auto isGrabbing = [=]()->bool {
            QMutexLocker locker(&d->mutex);
            return d->grabbing;
        }();
        if(isGrabbing) StopAcquisition();

        try {
            const auto imgStep = d->GetROIStep();
            const auto maxPixelsX = d->camera->WidthMax.GetValue();
            const auto maxPixelsY = d->camera->HeightMax.GetValue();

            if(imgStep == 0) {
                QLOG_INFO() << "Invalid image step = " << imgStep;
                return false;
            }

            // CHCO-61 nOffsetH/nOffsetV를 계산할 때 2로 나누는 것을 고려한 것임
            //  nImgStep=4 이고 nImgHStep=8이 되도록 하려는 의도임
            const unsigned int nImgHStep = imgStep * 2;
            const unsigned int nImgVStep = imgStep * 2;

            const unsigned int nImgModH = xPixels % nImgHStep;
            const unsigned int nImgModV = yPixels % nImgVStep;

            const unsigned int newROIH = xPixels + ((nImgModH > 0) ? (nImgHStep - nImgModH) : 0);
            const unsigned int newROIV = yPixels + ((nImgModV > 0) ? (nImgVStep - nImgModV) : 0);

            const unsigned int nOffHStep = d->GetOffsetStep();
            const unsigned int nOffVStep = nOffHStep;

            if(nOffHStep == 0) {
                QLOG_INFO() << "Invalid image offset step = " << nOffHStep;
                return false;
            }

            const unsigned int nOffsetH = static_cast<uint>((maxPixelsX - newROIH) / 2);
            const unsigned int nOffsetV = static_cast<uint>((maxPixelsY - newROIV) / 2);

            const unsigned int nOffModH = nOffsetH % nOffHStep;
            const unsigned int nOffModV = nOffsetV % nOffVStep;

            const int32_t newOffsetH = nOffsetH + ((nOffModH > 0) ? (nOffHStep - nOffModH) : 0);
            const int32_t newOffsetV = nOffsetV + ((nOffModV > 0) ? (nOffVStep - nOffModV) : 0);

            offsetX = newOffsetH;
            offsetY = newOffsetV;

            QLOG_INFO() << "ROI=(" << xPixels << "," << yPixels << ") newROI=(" << newROIH << "," << newROIV << ") newOffset=(" << newOffsetH << "," << newOffsetV << ")";

            d->camera->OffsetX.SetValue(0);
            d->camera->OffsetY.SetValue(0);
            d->camera->Width.SetValue(newROIH);
            d->camera->Height.SetValue(newROIV);
            d->camera->OffsetX.SetValue(newOffsetH);
            d->camera->OffsetY.SetValue(newOffsetV);
        } catch(Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("Camera: fails to change ROI - %1").arg(ex.what());
            return false;
        }

	    return true;
    }

    auto CameraControlPointGrey::GetFOV() const -> std::tuple<int32_t, int32_t> {
        int32_t xInPixels{ 0 };
        int32_t yInPixels{ 0 };

        try {
            xInPixels = d->camera->Height.GetValue();
            yInPixels = d->camera->Width.GetValue();
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("failed to get ROI - %1").arg(ex.what());
        }

        return std::make_tuple(xInPixels, yInPixels);
    }

    auto CameraControlPointGrey::SetTriggerSource(TriggerSource source, bool useModeControl) -> bool {
        INodeMap& nodeMap = d->camera->GetNodeMap();

        if(useModeControl) {
            if (!SetEnumParameter("TriggerMode", "Off", nodeMap)) {
                Update("unable to set trigger mode (off)", CameraErrorCode::FailedToSetTriggerMode);
                return false;
            }
        }

        const auto str= d->GetTriggerSourceStr(source);
        if (!SetEnumParameter("TriggerSource", str.toStdString().c_str(), nodeMap)) {
            Update(QString("unable to set trigger source (%1)").arg(str), CameraErrorCode::FailedToSetTriggerSource);
            return false;
        }

        if(useModeControl) {
            if (!SetEnumParameter("TriggerMode", "On", nodeMap)) {
                Update("unable to set trigger mode (on)", CameraErrorCode::FailedToSetTriggerMode);
                return false;
            }
        }

        return true;
    }

    auto CameraControlPointGrey::SetTriggerMode(bool on) -> bool {
        INodeMap& nodeMap = d->camera->GetNodeMap();

        if(on) {
            if (!SetEnumParameter("TriggerMode", "On", nodeMap)) {
                Update("unable to set trigger mode (on)", CameraErrorCode::FailedToSetTriggerMode);
                return false;
            }
        } else {
            if (!SetEnumParameter("TriggerMode", "Off", nodeMap)) {
                Update("unable to set trigger mode (off)", CameraErrorCode::FailedToSetTriggerMode);
                return false;
            }
        }

        return true;
    }

    auto CameraControlPointGrey::LoadDefaultSetting() -> bool {
        INodeMap& nodeMap = d->camera->GetNodeMap();

        CEnumerationPtr ptrUserSetSelector = nodeMap.GetNode("UserSetSelector");
        if (!IsAvailable(ptrUserSetSelector) || !IsWritable(ptrUserSetSelector)) {
            Update("Unable to access User Set Selector (enum retrieval). Aborting...", CameraErrorCode::AccessFailureToUserSetSelector);
            return false;
        }

        CEnumEntryPtr ptrUserSet1 = ptrUserSetSelector->GetEntryByName("UserSet1");
        if (!IsAvailable(ptrUserSet1) || !IsReadable(ptrUserSet1)) {
            Update("Unable to access User Set (entry retrieval). Aborting...", CameraErrorCode::AccessFailureToUserSet);
            return false;
        }

        ptrUserSetSelector->SetIntValue(ptrUserSet1->GetValue());

        // Load custom settings from User Set 1
        CCommandPtr ptrUserSetLoad = d->camera->GetNodeMap().GetNode("UserSetLoad");
        if (!IsAvailable(ptrUserSetLoad) || !IsReadable(ptrUserSetLoad)) {
            Update("Unable to load User Set...", CameraErrorCode::FailedToLoadUserSet);
        } else {
            ptrUserSetLoad->Execute();
        }

        return true;
    }

    auto CameraControlPointGrey::SetTimedTriggerMode(bool on, uint32_t exposureTime) -> bool {
        INodeMap& nodeMap = d->camera->GetNodeMap();

        if (!SetEnumParameter("ExposureMode", "Timed", nodeMap)) {
		    Update("unable to set exposure mode (timed)", CameraErrorCode::FailedToSetExposureMode);
		    return false;
        }

        if (!SetEnumParameter("TriggerMode", "Off", nodeMap)) {
            Update("unable to set trigger mode (off)", CameraErrorCode::FailedToSetTriggerMode);
            return false;
        }

        if (on) {

            if (!SetEnumParameter("TriggerSource", "Line2", nodeMap)) {
                Update("unable to set trigger source (line 2)", CameraErrorCode::FailedToSetTriggerSource);
                return false;
            }

            if (!SetEnumParameter("TriggerActivation", "RisingEdge", nodeMap)) {
                Update("unable to set trigger activation (rising edge)", CameraErrorCode::FailedToSetTriggerActivation);
                return false;
            }

            if (!SetFloatParameter("ExposureTime", exposureTime, nodeMap)) {
                Update(QString("unable to set exposure time (%1us)").arg(exposureTime), CameraErrorCode::FailedToSetExposureTime);
                return false;
            }

            if (!SetEnumParameter("TriggerMode", "On", nodeMap)) {
                Update("unable to set trigger mode (on)", CameraErrorCode::FailedToSetTriggerMode);
                return false;
            }

            if (!SetEnumParameter("ExposureMode", "Timed", nodeMap)) {
                Update("unable to set exposure mode (Timed)", CameraErrorCode::FailedToSetExposureMode);
                return false;
            }
        }

	    return true;
    }

    auto CameraControlPointGrey::ChangeGain(double value) -> bool {
        try {
            d->camera->GainAuto.SetValue(GainAutoEnums::GainAuto_Off);
            d->camera->Gain.SetValue(value);
        } catch(Spinnaker::Exception& ex) {
            Update(QString("unabled to set gain - %1").arg(ex.what()), CameraErrorCode::FailedToSetGain);
            return false;
        }

        Update(QString("Set gain - %1").arg(value));

	    return true;
    }

    auto CameraControlPointGrey::TurnOffEmbeddedInfo() -> bool {
        try {
            CBooleanPtr ptrChunkModeActive = d->camera->GetNodeMap().GetNode("ChunkModeActive");
            if (!IsAvailable(ptrChunkModeActive) || !IsWritable(ptrChunkModeActive)) {
                Update("Camera: unable to change chuck mode", CameraErrorCode::FailedToSetChuckMode);
                return false;
            }

            ptrChunkModeActive->SetValue(false);
        } catch (Spinnaker::Exception& ex) {
            Update(QString("Camera: unable to turn off chunk mode - %1").arg(ex.what()), CameraErrorCode::FailedToSetChuckMode);
            return false;
        }

        return true;
    }

    auto CameraControlPointGrey::GetImage() -> Image::Pointer {
        static uint64_t index{ 0 };
        static uint64_t failCount{ 0 };

        Image::Pointer image;
        try {
            auto rawImage = d->camera->GetNextImage(1000);
            if (rawImage->IsIncomplete()) return image;

            image.reset(new Image(static_cast<uint32_t>(rawImage->GetWidth()), 
                                  static_cast<uint32_t>(rawImage->GetHeight()), 
                                  static_cast<uint32_t>(rawImage->GetImageSize()), 
                                  static_cast<uint32_t>(rawImage->GetBitsPerPixel()), 
                                  rawImage->GetData()));
            image->SetIndex(index++);
            rawImage->Release();
        } catch (Spinnaker::Exception& ex) {
            failCount++;
#ifdef _DEBUG
            if((failCount%60)==0) {
                Update(QString("Failed to get image (fails=%1) - %2").arg(failCount).arg(ex.what()));
            }
#else
            Q_UNUSED(ex)
#endif
            return image;
        }

        failCount = 0;

        return image;
    }

    void CameraControlPointGrey::run() {
        Update("Grabbing thread is started");

        while (true) {
            QMutexLocker locker(&d->mutex);
            if (d->running == false) {
                break;
            }

            if (d->grabbing == false) {
                d->waitCond.wait(locker.mutex());
                continue;
            }
            locker.unlock();

            locker.relock();
            if (d->grabbing == false) {
                continue;
            }

            auto image = GetImage();
            if (image == nullptr) continue;

            SendImage(image);
            d->imageCount.Add();
        }

        Update("Grabbing thread is finished");
    }

    auto IsSupported(const char* nodeName, Spinnaker::GenApi::INodeMap& nodeMap) -> bool {
        try {
            CEnumerationPtr node = nodeMap.GetNode(nodeName);
            if (!IsAvailable(node) || !IsWritable(node)) return false;
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("failed to check node - %1 [%2]").arg(nodeName).arg(ex.what());
            return false;
        }
        return true;
    }

    auto SetEnumParameter(const char* nodeName, const char* entryName, Spinnaker::GenApi::INodeMap& nodeMap, bool ignoreLocked) -> bool {
        try {
            CEnumerationPtr node = nodeMap.GetNode(nodeName);
            if (!IsAvailable(node) || !IsWritable(node)) return ignoreLocked;

            CEnumEntryPtr entry = node->GetEntryByName(entryName);
            if (!IsAvailable(entry) || !IsReadable(entry)) return false;

            node->SetIntValue(entry->GetValue());

            CEnumerationPtr readEntry = static_cast<CEnumerationPtr>(nodeMap.GetNode(nodeName));
            QLOG_TRACE() << "" << nodeName << " = " << readEntry->GetCurrentEntry()->GetSymbolic().c_str();
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("failed to set enum parameter - %1").arg(ex.what());
            return false;
        }

        return true;
    }

    auto SetIntegerParameter(const char* nodeName, int64_t value, Spinnaker::GenApi::INodeMap& nodeMap, bool force) -> bool {
        try {
            CIntegerPtr node = nodeMap.GetNode(nodeName);
            if (!IsAvailable(node)) return false;
            if (force && !IsWritable(node)) return false;

            if (value < node->GetMin()) {
                value = node->GetMin();
            } else if (value > node->GetMax()) {
                value = node->GetMax();
            } else {
            }

            node->SetValue(value);
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("failed to set integer parameter - %1").arg(ex.what());
            return false;
        }

        return true;
    }

    auto SetFloatParameter(const char* nodeName, float value, Spinnaker::GenApi::INodeMap& nodeMap, bool force)->bool {
        try {
            CFloatPtr node = nodeMap.GetNode(nodeName);
            if (!IsAvailable(node)) return false;
            if (force && !IsWritable(node)) return false;

            if (value < node->GetMin()) {
                value = node->GetMin();
            } else if (value > node->GetMax()) {
                value = node->GetMax();
            } else {
            }

            node->SetValue(value);
        } catch (Spinnaker::Exception& ex) {
            QLOG_ERROR() << QString("failed to set float parameter - %1").arg(ex.what());
            return false;
        }

        return true;
    }

    auto PrintDeviceInfo(const uint32_t index, const Spinnaker::GenApi::INodeMap & nodeMap) -> void {
        QLOG_INFO() << "*** CAMERA INFORMATION ***";
        QLOG_INFO() << ">>  Index  -  " << index + 1;

        try {
            FeatureList_t features;
            const CCategoryPtr category = nodeMap.GetNode("DeviceInformation");

            if (IsAvailable(category) && IsReadable(category)) {
                category->GetFeatures(features);
                for (auto& feature : features) {
                    const CNodePtr pfeatureNode = feature;
                    CValuePtr pValue = static_cast<CValuePtr>(pfeatureNode);

                    QLOG_INFO() << ">> " << pfeatureNode->GetName() << " - " << (IsReadable(pValue) ? pValue->ToString() : "Node not readable");
                }
            } else {
                QLOG_ERROR() << "Device control information not available.";
            }
        } catch (Spinnaker::Exception& e) {
            QLOG_ERROR() << "Error: " << e.what();
        }
    }

    auto GetDeviceID(const Spinnaker::GenApi::INodeMap & nodeMap)->QString {
        QString deviceID;

        try {
            FeatureList_t features;
            const CCategoryPtr category = nodeMap.GetNode("DeviceInformation");

            if (IsAvailable(category) && IsReadable(category)) {
                category->GetFeatures(features);
                for (auto& feature : features) {
                    const CNodePtr pfeatureNode = feature;
                    CValuePtr pValue = static_cast<CValuePtr>(pfeatureNode);

                    const QString featureName{pfeatureNode->GetName().c_str()};
                    if(featureName == "DeviceID") {
                        if(!IsReadable(pValue)) {
                            QLOG_ERROR() << "It can't get device ID";
                            break;
                        }

                        deviceID = pValue->ToString();
                        break;
                    }
                }
            } else {
                QLOG_ERROR() << "Device control information not available.";
            }
        } catch (Spinnaker::Exception& e) {
            QLOG_ERROR() << "Error: " << e.what();
        }

        return deviceID;
    }
}
