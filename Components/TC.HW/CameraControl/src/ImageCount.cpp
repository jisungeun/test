#include <QMutexLocker>

#include "ImageCount.h"

namespace TC::CameraControl {
    struct ImageCount::Impl {
        QMutex mutex;
        uint64_t count{ 0 };
    };

    ImageCount::ImageCount() : d{ new Impl } {
    }

    ImageCount::~ImageCount() {
    }

    auto ImageCount::Add() -> void {
        QMutexLocker locker(&d->mutex);
        d->count = d->count + 1;
    }

    auto ImageCount::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->count = 0;
    }

    auto ImageCount::Count() const -> uint64_t {
        QMutexLocker locker(&d->mutex);
        return d->count;
    }

    auto ImageCount::IsReached(uint64_t count) -> bool {
        QMutexLocker locker(&d->mutex);
        return (d->count >= count);
    }
}


