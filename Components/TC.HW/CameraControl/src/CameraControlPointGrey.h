#pragma once
#include <memory>

#include <QThread>
#include <Spinnaker.h>

#include "CameraControl.h"

namespace TC::CameraControl::PointGrey {
    class CameraControlPointGrey : public QThread, public CameraControl {
    public:
        CameraControlPointGrey(Spinnaker::CameraPtr camera, IImageSink* sink, const QString& alias = QString());
        virtual ~CameraControlPointGrey() override;

        auto Initialize() -> bool override;
        auto CleanUp() -> bool override;
        auto StartAcquisition(uint32_t images) -> bool override;
        auto StopAcquisition() -> bool override;
        auto GetNoAcquiredCount() const -> int32_t override;

        auto GetGainRange(double& gainMin, double& gainMax) -> bool override;
        auto GetGain() -> double override;
        auto SetGain(const double gain) -> bool override;
        auto SetGainConversionMode(GainConversion mode) -> bool override;

        auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool override;
        auto GetFOV() const -> std::tuple<int32_t, int32_t> override;

        auto SetTriggerSource(TriggerSource source, bool useModeControl = true) -> bool override;
        auto SetTriggerMode(bool on) -> bool override;

    protected:
        auto LoadDefaultSetting()->bool;
        auto SetTimedTriggerMode(bool on, uint32_t exposureTime)->bool;
        auto ChangeGain(double value)->bool;
        auto TurnOffEmbeddedInfo()->bool;
        auto GetImage()->Image::Pointer;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
