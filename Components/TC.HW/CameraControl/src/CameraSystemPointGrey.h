#pragma once
#include <memory>

#include "CameraSystem.h"

namespace TC::CameraControl::PointGrey {
    class CameraSystemPointGrey : public CameraSystem {
    public:
        CameraSystemPointGrey();
        ~CameraSystemPointGrey() override;

        auto GetCamera(uint32_t index = 0,
                       IImageSink* sink = nullptr,
                       const QString& alias = QString()) -> CameraControl::Pointer override;
        auto GetCamera(const QString& serial,
                       IImageSink* sink = nullptr,
                       const QString& alias = QString()) ->CameraControl::Pointer override;

    protected:
        auto PostCleanUp() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}