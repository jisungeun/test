#include "CameraSystemPointGrey.h"
#include "CameraControlFactory.h"

namespace TC::CameraControl {
    auto CameraControlFactory::Create(Controller type) -> CameraSystem::Pointer {
        CameraSystem::Pointer system{ nullptr };

        switch(type) {
        case Controller::PointGrey:
            system.reset(new PointGrey::CameraSystemPointGrey());
            break;
        }

        return system;
    }
}
