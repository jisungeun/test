#include <QList>

#include "CameraSystem.h"

namespace TC::CameraControl {
    struct CameraSystem::Impl {
        QString libraryVersion;
        QList<DeviceInfo> list;
        QList<CameraControl::Pointer> cameraControls;
    };

    CameraSystem::CameraSystem() : d{new Impl} {
    }

    CameraSystem::~CameraSystem() {
    }

    auto CameraSystem::GetLibraryVersion() const -> QString {
        return d->libraryVersion;
    }

    auto CameraSystem::GetDeviceInfos() const -> QList<DeviceInfo> {
        return d->list;
    }

    auto CameraSystem::CleanUp() -> void {
        for(auto camera : d->cameraControls) {
            camera->CleanUp();
        }

        d->cameraControls.clear();

        PostCleanUp();
    }

    auto CameraSystem::SetLibraryVersion(const QString& version) -> void {
        d->libraryVersion = version;
    }

    auto CameraSystem::AddDevice(const DeviceInfo& deviceID) -> void {
        d->list.push_back(deviceID);
    }

    auto CameraSystem::FindDevice(const QString& serial,
                                  DeviceInfo& deviceInfo) const -> bool {
        for(const auto& info : d->list) {
            if(info.serialNumber == serial) {
                deviceInfo = info;
                return true;
            }
        }
        return false;
    }

    auto CameraSystem::RegisterControl(CameraControl::Pointer camera) -> void {
        d->cameraControls.push_back(camera);
    }
}
