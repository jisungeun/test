#include "ParamConverter.h"

namespace TC {
    struct ParamConverter::Impl {
        QMap<QString, QString> unitLUT;
        QMap<QString, QString> nameLUT;
    };
    ParamConverter::ParamConverter() : d{ new Impl } {
        BuildNameTable();
        BuildUnitTable();
    }
    ParamConverter::~ParamConverter() {
        
    }
    auto ParamConverter::GetInstance() -> Pointer {
        static Pointer theInstance{ new ParamConverter() };
        return theInstance;
    }
    auto ParamConverter::BuildNameTable() -> void {        
        //For Auto Threshold
        d->nameLUT["Method!Enabler"] = "Method";
        d->nameLUT["Method!Setter"] = "AlgoSelection";

        //For Basic Measure
        d->nameLUT["Preset!Enabler"] = "Preset";        
    }
    auto ParamConverter::BuildUnitTable() -> void {
        d->unitLUT["ScalarValue.double"] = "ScalarValue.double";
        d->unitLUT["ScalarValue.int"] = "ScalarValue.int";        
    }
    auto ParamConverter::ConvertName(const QString& name) -> QString {
        if(d->nameLUT.contains(name)) {
            return d->nameLUT[name];
        }
        return name;
    }
    auto ParamConverter::ConvertUnit(const QString& unit) -> QString {
        //remove enabler text
        auto realUnit = unit;
        if(realUnit.contains("ScalarValue.double")) {
            realUnit = "ScalarValue.double";
        }else if(realUnit.contains("ScalarValue.int")) {
            realUnit = "ScalarValue.int";
        }
        if(d->unitLUT.contains(realUnit)) {
            return d->unitLUT[realUnit];
        }
        return unit;
    }
}