#define LOGGER_TAG "[ParameterTable]"
#include <TCLogger.h>

#include "ParameterTable.h"

#include <iostream>
#include <QComboBox>
#include <QHeaderView>
#include <QPushButton>
#include <qstyleditemdelegate.h>
#include <QTableWidgetItem>
#include <QVBoxLayout>
#include <QScrollBar>
#include "StrComboBox.h"
#include <QItemDelegate>
#include <QLineEdit>
#include "AdaptiveHeader.h"

namespace TC {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
            const QModelIndex& index) const override {
            Q_UNUSED(option)
            Q_UNUSED(index)
            QLineEdit* lineEdit = new QLineEdit(parent);
            QIntValidator* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };
    class DoubleDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
            const QModelIndex& index) const override {
            Q_UNUSED(option)
            Q_UNUSED(index)
            QLineEdit* lineEdit = new QLineEdit(parent);
            QDoubleValidator* validator = new QDoubleValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };
    struct ParameterTable::Impl {
        QMap<QString,IParameter::Pointer> param_val;
        QStringList dupList;
        QList<bool> dupReadOnly;
        //shared properties
        IParameter::Pointer param;
        QString param_name;        
        ParameterList list;
        QList<QString> node_name;
        QStringList real_table_header;

        ParameterList NoList;
        QList<QString> no_name;
        TMap type_list;

        //interface
        QVBoxLayout *layout;
        QTableWidget* table;
        QMap<QString,QList<QComboBox*>> combos;
        QPushButton* executeBtn;

        bool disable{false};
        bool hideExecute{false};

        QStringList displayNames;
        QStringList realNames;
    };
    ParameterTable::ParameterTable(QWidget* parent) : QWidget(parent),d{new Impl} {
        d->layout = new QVBoxLayout(this);
        d->layout->setContentsMargins(0, 0, 0, 0);

        setLayout(d->layout);
        d->table = new QTableWidget;
        d->table->setShowGrid(true);
        d->table->setSizeAdjustPolicy(QTableWidget::SizeAdjustPolicy::AdjustToContents);        
        d->table->setProperty("styleVariant",1);
        auto horiheader = new AdaptiveHeader(Qt::Orientation::Horizontal);
        d->table->setHorizontalHeader(horiheader);
        auto verheader = new AdaptiveHeader(Qt::Orientation::Vertical);        
        d->table->setVerticalHeader(verheader);
        d->layout->addWidget(d->table);
        setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);

        connect(d->table,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(OnTableItemChanged(QTableWidgetItem*)));
    }
    auto ParameterTable::toDisplayName(QString name) -> QString {        
        for(auto i=0;i<d->realNames.size();i++) {
            if(name.compare(d->realNames[i])==0) {
                return d->displayNames[i];
            }
        }
        return name;
    }
    auto ParameterTable::toRealName(QString name) -> QString {
        for(auto i=0;i<d->displayNames.size();i++) {
            if(name.compare(d->displayNames[i])==0) {
                return d->realNames[i];
            }
        }
        return name;
    }

    ParameterTable::~ParameterTable() {
        disconnect();
    }
    auto ParameterTable::setName(QString name) -> void {
        d->param_name = name;
    }
    void ParameterTable::OnTableItemChanged(QTableWidgetItem* item) {        
        auto val = item->text().toDouble();
        auto organ_name = d->dupList[item->column()];
        auto param_name = d->real_table_header[item->row()];        
        emit valChanged(val,organ_name,param_name,d->param_name);
    }
    auto ParameterTable::setProcessingAlgorithm(IParameter::Pointer param) -> void {
        d->param = param;
    }
    auto ParameterTable::setDuplicator(QStringList dupList) -> void {
        d->dupList = dupList;
        d->dupReadOnly.clear();
        for(auto dd : dupList) {
            d->dupReadOnly.push_back(true);
            d->param_val[dd] = std::shared_ptr<IParameter>(d->param->Clone());
        }
    }
    auto ParameterTable::highlightBtn(bool isHi) -> void {
        if (d->executeBtn) {
            if (isHi) {
                d->executeBtn->setStyleSheet("background-color: rgb(91,139,151);");
            }
            else {
                d->executeBtn->setStyleSheet("");
            }
        }
    }

    auto ParameterTable::setAlgorithmValue(IParameter::Pointer param, QString key,bool blockSig)->void {
        //auto organ = key.split("!")[1];
        auto organ = key;
        int kIdx = -1;
        for(auto i=0;i<d->dupList.size();i++) {
            if(d->dupList[i].compare(organ)==0) {
                kIdx = i;
                break;
            }
        }
        if(d->param_val.contains(organ)) {
            d->param_val[organ] = param;
            auto names = param->GetNameAndTypes();
            for(const auto nnt:names) {
                auto name = std::get<0>(nnt);
                auto type = std::get<1>(nnt);
                auto desc = param->Description(name);
                if(desc.contains("DoNotMakeInterface")) {
                    continue;
                }                
                if(name.contains("Setter")) {
                    auto val = param->GetValue(name).toInt();
                    if(kIdx>-1){
                        if(blockSig){
                            d->combos[name][kIdx]->blockSignals(true);
                        }
                        d->combos[name][kIdx]->setCurrentIndex(val);
                        if(d->combos[name][kIdx]->currentText().compare("Customize")==0) {
                            d->dupReadOnly[kIdx] = false;
                        }else {
                            d->dupReadOnly[kIdx] = true;
                        }
                        if(blockSig){
                            d->combos[name][kIdx]->blockSignals(false);
                        }
                    }
                }else if(type == QJsonValue::Double) {
                    auto val = param->GetValue(name).toDouble();
                    //set value to table directly
                    auto rIdx = -1;
                    for(auto k = 0; k <d->real_table_header.size();k++) {                        
                        if(d->real_table_header[k].compare(name)==0) {
                            rIdx = k;
                            break;
                        }
                    }
                    if(rIdx>-1 && kIdx>-1) {
                        auto item = new QTableWidgetItem(QString::number(val));                        
                        if(d->dupReadOnly[kIdx]) {                            
                            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                        }
                        if(blockSig){
                            d->table->blockSignals(true);
                        }
                        d->table->setItem(rIdx,kIdx,item);
                        if(blockSig){
                            d->table->blockSignals(false);
                        }
                    }
                }
            }            
        }
    }
    auto ParameterTable::setHideExecute(bool isHide) -> void {
        d->hideExecute = isHide;
    }
    auto ParameterTable::setDisable(bool dis) -> void {
        d->disable = dis;
    }
    auto ParameterTable::GetParameter(QString key) -> IParameter::Pointer {
        if(d->param_val.contains(key)) {
            return d->param_val[key];
        }
        return nullptr;
    }
    auto ParameterTable::getScalarValue(QString name, QString key) ->double{
        if(d->param_val.contains(key)) {
            auto val =d->param_val[key]->GetValue(name).toDouble();
            return val;
        }
        return 0.0;
    }
    auto ParameterTable::setScalarValue(QString name, double val, QString key)->void {
        if(d->param_val.contains(key)) {
            if(d->param_val[key]->ExistNode(name)) {
                d->param_val[key]->SetValue(name,val);
            }
        }
    }
    auto ParameterTable::parseParameter() -> void {
        auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };
        if(nullptr == d->param) {
            return;
        }
        d->NoList.Clear();
        d->list.Clear();
        d->type_list.clear();
        auto nameNtypes = d->param->GetNameAndTypes();
        for (const auto& nnt : nameNtypes) {
            auto node = param2node(d->param, std::get<0>(nnt));
            auto type = node.description;
            if (type.contains("DoNotMakeInterface")) {
                d->NoList.AppendNode(node);
                d->no_name.push_back(node.name);
                continue;
            }
            d->list.AppendNode(node);
            d->node_name.push_back(node.name);            
            d->type_list[node.name] = std::get<1>(nnt);
        }
        auto childrenNames = d->param->GetChildrenNames();
        for (const auto& childName : childrenNames) {
            auto child = d->param->GetChild(childName);
            if (child.get()) {
                auto nameNtypesChild = child->GetNameAndTypes();
                for (const auto& nntChild : nameNtypesChild) {
                    auto node = param2node(child, std::get<0>(nntChild));
                    node.name = QString("%1.%2").arg(childName).arg(std::get<0>(nntChild));
                    d->list.AppendNode(node);
                    d->type_list[node.name] = std::get<1>(nntChild);
                }
            }
        }
    }
    auto ParameterTable::setConverter(QStringList disList, QStringList realList) -> void {
        d->realNames = realList;
        d->displayNames = disList;                
    }

    auto ParameterTable::buildInterface() -> void {
        d->table->clearContents();
        d->real_table_header.clear();
        d->table->setColumnCount(d->dupList.size());
        //d->displayNames.clear();
        //d->realNames.clear();
        d->combos.clear();        
        d->executeBtn = nullptr;        
        parseParameter();
        QStringList verHeader;
        int row_cnt=0;
        for(auto i=0;i<d->node_name.size();i++) {
            auto node = d->list.GetNode(d->node_name[i]);
            auto type = node.unit;
            if(type.contains("FilePath")) {
                QLOG_INFO() << "Table do not provide path parameter ";
                continue;
            }
            if(type.contains("ScalarValue")) {
                //table provide only scalar value (int or double)
                auto scalarType = d->type_list[d->node_name[i]];
                if(scalarType == QJsonValue::Double) {
                    auto unit= node.unit;
                    QStringList split = unit.split(".");
                    auto typeString= split[1];
                    if(typeString.compare("int")==0) {
                        //set table Item's regular expression as integer                        
                        row_cnt++;
                        d->real_table_header.push_back(node.name);                        
                        //verHeader.push_back(toMultiRow(node.displayName,6));                        
                        verHeader.push_back(node.displayName);
                    }else if(typeString.compare("double")==0) {
                        //set table item's regular expression as double                        
                        row_cnt++;
                        d->real_table_header.push_back(node.name);                        
                        //verHeader.push_back(toMultiRow(node.displayName,6));
                        verHeader.push_back(node.displayName);
                    }
                }
            }
            if(type.contains("SelectOption")) {
                //make combo box and embed it to the table                
                row_cnt++;
                auto nam = node.name.split("!")[0];
                d->real_table_header.push_back(node.name);                
                //verHeader.push_back(toMultiRow(nam,6));
                verHeader.push_back(nam);
            }
        }
        d->table->setRowCount(row_cnt);                
        
        QStringList hoHeader;
        for(auto dup : d->dupList) {
            //hoHeader.push_back(toMultiRow(toDisplayName(dup),5));            
            hoHeader.push_back(toDisplayName(dup));
        }
        d->table->setHorizontalHeaderLabels(hoHeader);        
        d->table->setVerticalHeaderLabels(verHeader);        

        d->table->resizeColumnsToContents();
        d->table->resizeRowsToContents();
        //once again for table item setting
        auto rr = 0;
        for(auto i=0;i<d->node_name.size();i++) {
            auto node = d->list.GetNode(d->node_name[i]);
            auto type = node.unit;
            if(type.contains("FilePath")) {
                QLOG_INFO() << "Table do not provide path parameter ";
                continue;
            }
            if(type.contains("ScalarValue")) {
                //table provide only scalar value (int or double)
                auto scalarType = d->type_list[d->node_name[i]];
                if(scalarType == QJsonValue::Double) {
                    auto unit= node.unit;
                    QStringList split = unit.split(".");
                    auto typeString= split[1];
                    if(typeString.compare("int")==0) {
                        //set table Item's regular expression as integer
                        addIntValue(node,verHeader.size(),rr);
                        rr++;
                    }else if(typeString.compare("double")==0) {
                        //set table item's regular expression as double
                        addDoubleValue(node,verHeader.size(),rr);
                        rr++;
                    }
                }
            }
            if(type.contains("SelectOption")) {
                //make combo box and embed it to the table
                addComboValue(node,d->dupList.size(),rr,node.name);
                rr++;
            }
        }                

        if(!d->hideExecute) {
            d->executeBtn = new QPushButton;
            d->executeBtn->setText("Execute");
            connect(d->executeBtn,SIGNAL(clicked()),this,SLOT(OnExecuteClicked()));
            d->layout->addWidget(d->executeBtn);
        }
        if(d->disable) {
            d->table->setDisabled(true);            
        }        
    }
    auto ParameterTable::addComboValue(ParameterNode node, int cols,int row, QString key) -> void {
        QList<QComboBox*> dupCombo;
        auto values = node.minValue.toString().split("!");        
        for(auto c =0 ; c<cols;c++) {
            auto combo = new QComboBox;
            //auto combo = new StrComboBox();
            QFont myFont("Noto Sans KR", 14); //assuming you are using times new roman
            QFontMetrics fm(myFont);
            QStringList itemList;
            int pixelwide = -1;
            for(auto v:values) {                
                itemList << v;
            }                        
            itemList << "Customize";
            for(auto i =0;i<itemList.size();i++) {
                auto wh = fm.horizontalAdvance(itemList[i]);//fm.width(itemList[i]);
                if(pixelwide < wh){
                    pixelwide = wh;
                }                
            }
            QString maxDropdownLen,styleSheet;
            styleSheet = "QComboBox {padding: 0px} QAbstractItemView { min-width: %1px;}";
            maxDropdownLen = QString::number(pixelwide-10);
            combo->addItems(itemList);
            combo->setStyleSheet(styleSheet.arg(maxDropdownLen));
            combo->setObjectName(node.name +"."+ QString::number(c));
            dupCombo.push_back(combo);
            connect(combo,SIGNAL(currentIndexChanged(int)),this,SLOT(OnComboBoxChanged(int)));
            d->table->setCellWidget(row,c,combo);            
        }
        d->combos[key] = dupCombo;
    }
    void ParameterTable::OnComboBoxChanged(int idx) {
        auto cb = dynamic_cast<QComboBox*>(sender());
        if(cb) {
            auto value= cb->currentText().toStdString();
            auto organIdx = cb->objectName().split(".")[1].toInt();
            auto organKey = d->dupList[organIdx];
            auto nodeName = cb->objectName().split(".")[0];
            auto paramKey = nodeName.split("!")[0];
            if(value.compare("Customize")!=0) {                
                setCustomized(organIdx,true);
                for(auto nn : d->no_name) {
                    if(nn.contains(paramKey)) {
                        auto setteeKey = nn.split("!")[1];
                        auto type = d->list.GetNode(setteeKey).unit;
                        auto setValue = d->NoList.GetNode(nn).value.toString().split("!")[idx];
                                                
                        auto item = new QTableWidgetItem(setValue);
                        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                        auto rIdx = -1;
                        for(auto k = 0; k <d->real_table_header.size();k++) {
                            if(d->real_table_header[k].compare(setteeKey)==0) {
                                rIdx = k;
                                break;
                            }
                        }
                        d->table->blockSignals(true);
                        d->table->setItem(rIdx,organIdx,item);
                        d->table->blockSignals(false);
                        emit valChanged(setValue.toDouble(),d->dupList[organIdx],d->real_table_header[rIdx],d->param_name);
                    }
                }                                
            }else {                
                setCustomized(organIdx,false);                
            }
            emit comboChanged(idx,nodeName,organKey,d->param_name);
        }
    }
    auto ParameterTable::setCustomized(int col, bool isReadOnly) -> void {
        if(d->dupReadOnly[col] != isReadOnly) {//���� �ٸ�
            for(auto i=0;i<d->table->rowCount();i++) {
                auto item = d->table->item(i,col);
                if(nullptr!=item) {
                    item->setFlags(item->flags() | Qt::ItemIsEditable);
                }
            }
        }
        d->dupReadOnly[col] = isReadOnly;
    }
    auto ParameterTable::addDoubleValue(ParameterNode node, int cols, int row) -> void {
        Q_UNUSED(node)
        Q_UNUSED(cols)
        //in here just add regular expression table item
        d->table->setItemDelegateForRow(row,new DoubleDelegate);                
    }
    auto ParameterTable::addIntValue(ParameterNode node, int cols, int row) -> void {
        Q_UNUSED(node)
        Q_UNUSED(cols)
        //in here just add regular expression table item
        d->table->setItemDelegateForRow(row,new IntegerDelegate);
    }
    void ParameterTable::OnExecuteClicked() {
        emit execute(d->param_name);
    }
    auto ParameterTable::toMultiRow(QString txt,int letters) -> QString {
        auto div = txt;
        QString result= "";
        while(1){
            auto len = div.length();
            if(len<=letters) {
                result += div;
                break;
            }else {
                auto sub = div.left(letters);
                result+= sub;
                result+= "\n";
                div = div.right(len - letters);
            }
        }
        return result;
    }
    auto ParameterTable::toSingleRow(QString txt) -> QString {
        auto split = txt.split("\n");
        QString result = "";
        for(auto s: split) {
            result+=s;
        }
        return result;
    }
}