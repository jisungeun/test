#define LOGGER_TAG "[ParameterControl]"
#include <TCLogger.h>

#include <iostream>

#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>


#include "ParameterControl.h"

#include <QPushButton>

namespace TC {
    struct ParameterControl::Impl {
        //parameter information
        QString param_name;
        IParameter::Pointer param;
        ParameterList list;        
        QList<QString> node_name;        
        QList<QString> enablers;


        ParameterList NoList;
        ParameterList BoundList;
        QList<QString> no_name;
        TMap type_list;

        //interface
        QVBoxLayout* layout;
        RangeMap sliders;;
        ScalarMap scalars;
        PathMap paths;
        QMap<QString,QLabel*> labels;
        ComboMap combos;
        CheckMap checks;
        QPushButton* executeBtn;

        QMap<QString, QString> lowerBounds;
        QMap<QString, QString> upperBounds;

        bool hideExecute{false};

        bool disable{ false };
    };

    ParameterControl::ParameterControl(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->layout = new QVBoxLayout(this);
        d->layout->setContentsMargins(0, 0, 0, 0);
        setLayout(d->layout);
        setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);
    }
    ParameterControl::~ParameterControl() {
        this->disconnect();
    }

    auto ParameterControl::highlightBtn(bool isHi) -> void {
        if(d->executeBtn) {
            if(isHi) {
                d->executeBtn->setStyleSheet("background-color: rgb(91,139,151);");
            }else {
                d->executeBtn->setStyleSheet("");
            }
        }
    }


    auto ParameterControl::GetParameter() -> IParameter::Pointer {
        return d->param;
    }
    auto ParameterControl::setDisable(bool dis) -> void {
        d->disable = dis;
    }
    auto ParameterControl::setProcessingAlgorithm(IParameter::Pointer param) -> void {
        d->param = param;
    }
    auto ParameterControl::setAlgorithmValue(IParameter::Pointer param,bool blockSig) -> void {
        d->param = param;        
        auto names = param->GetNameAndTypes();
        for(const auto nnt:names) {
            auto name = std::get<0>(nnt);
            auto type = std::get<1>(nnt);
            auto desc = param->Description(name);
            if(desc.contains("DoNotMakeInterface")) {
                continue;
            }
            if(desc.contains("Bounder")) {
                continue;
            }
            if(name.contains("Enabler")) {
                auto chk = param->GetValue(name).toBool();
                if(chk) {
                    d->checks[name]->setChecked(true);
                }else {
                    d->checks[name]->setChecked(false);
                }
                
            }else if(name.contains("Setter")) {
                auto val = param->GetValue(name).toInt();               
                d->combos[name]->setCurrentIndex(val);
            }else if (type == QJsonValue::String) {
                auto vv = param->GetValue(name).toString();
                d->paths[name]->setPath(vv);
                //update interface
            }else if(type == QJsonValue::Double) {                
                auto val = param->GetValue(name).toDouble();                
                d->scalars[name]->setValue(val,blockSig);
            }
        }        
    }

    auto ParameterControl::addDoubleRange(ParameterNode node) -> void {

    }
    auto ParameterControl::addIntRange(ParameterNode node) -> void {

    }
    auto ParameterControl::setHideExecute(bool isHide) -> void {
        d->hideExecute = isHide;        
    }

    auto ParameterControl::setName(QString name) -> void {
        d->param_name = name;
    }
    auto ParameterControl::addIntValue(ParameterNode node) -> void {
        ScalarInput* input = new ScalarInput(this);                
        if (!node.description.contains("Hide")) {
            QLabel* label = new QLabel(node.displayName);
            d->labels[node.name] = label;
            d->layout->addWidget(label);
        }        
        d->layout->addWidget(input);
        d->scalars[node.name] = input;
        auto min = node.minValue.toDouble();
        auto max = node.maxValue.toDouble();        

        input->setName(node.name);
        input->setDecimals(0);
        input->setSingleStep(1.0);
        input->setValueRange(min, max);
        input->setValue(node.value.toInt());
        if (node.unit.contains("False")) {
            input->setEnabled(false);            
        }
        if (node.description.contains("Hide")) {            
            input->hide();
        }        
        connect(input, SIGNAL(scalarChanged(double,QString)), this, SLOT(OnValueChanged(double,QString)));
        input->setDisabled(d->disable);
    }

    void ParameterControl::OnPathChanged(QString val, QString val_name) {
        auto fullName = d->param_name;
        fullName += "!";
        fullName += val_name;
        emit pathChanged(val, fullName);
    }
    void ParameterControl::OnValueChanged(double val,QString val_name) {
        auto fullName = d->param_name;
        fullName += "!";
        fullName += val_name;                

        if(d->upperBounds.contains(val_name)) {
            //std::cout << "Set Upper Bound:" << d->upperBounds[val_name].toStdString()<< std::endl;
            d->scalars[d->upperBounds[val_name]]->setUpper(val);
        }
        if(d->lowerBounds.contains(val_name)) {
            //std::cout << "Set Lower Bound:" << d->lowerBounds[val_name].toStdString()<< std::endl;
            d->scalars[d->lowerBounds[val_name]]->setLower(val);
        }
        emit valChanged(val,fullName);
    }
    auto ParameterControl::addComboValue(ParameterNode node,bool hide) -> void {
        //QComboBox* input = new QComboBox(this);
        StrComboBox* input = new StrComboBox(this,hide && !d->hideExecute);
        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        input->SetToolTip(node.name);
        input->SetToolTipDuration(0);
        d->layout->addWidget(label);
        d->layout->addWidget(input);
        d->combos[node.name] = input;
        //pasre options;

        //auto values = node.value.toString().split("!");
        auto values = node.minValue.toString().split("!");
        for(auto i=0; i<values.size();i++) {
            input->addItem(values[i]);
        }
        auto setter = node.name.split("!");        
        if(setter.size()>1) {
            if(setter[1].compare("Setter")==0) {                
                auto curIdx = node.value.toInt();
                input->setCurrentIndex(curIdx);
                connect(input, SIGNAL(comboStr(QString,int)), this, SLOT(OnSetterChanged(QString,int)));
                connect(input,SIGNAL(comboApply(QString,int)),this,SLOT(OnSetterApply(QString,int)));
            }            
        }
        input->setDisabled(d->disable);
    }
    void ParameterControl::OnSetterApply(QString nodeName, int idx) {
        Q_UNUSED(idx)
        auto fullName = d->param_name;
        fullName += "*";
        fullName += nodeName;

    }
    void ParameterControl::OnSetterChanged(QString nodeName,int idx) {            
        auto key = nodeName.split("!")[0];        
        for(auto i=0;i<d->no_name.size();i++){
            if (d->no_name[i].contains(key)) {                
                auto setteeKey = d->no_name[i].split("!")[1];
                auto type = d->list.GetNode(setteeKey).unit;
                auto setValue = d->NoList.GetNode(d->no_name[i]).value.toString().split("!")[idx];
                if (type.contains("ScalarValue")) {                    
                    d->scalars[setteeKey]->setValue(setValue.toDouble());
                }
                else if (type.contains("Path")) {
                    d->paths[setteeKey]->setPath(setValue);
                }
                else if (type.contains("Select")) {
                    d->combos[setteeKey]->setCurrentIndex(setValue.toInt());
                }
                else if (type.contains("Check")) {
                    d->checks[setteeKey]->setChecked(setValue.toInt());                    
                }
            }
        }
        auto fullName = d->param_name;
        fullName += "*";
        fullName += nodeName;
        emit comboChanged(idx, fullName);
    }

    auto ParameterControl::addDoubleValue(ParameterNode node) -> void {
        ScalarInput* input = new ScalarInput(this);
        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        d->layout->addWidget(label);
        d->layout->addWidget(input);
        d->scalars[node.name] = input;
        auto min = node.minValue.toDouble();
        auto max = node.maxValue.toDouble();
        input->setName(node.name);
        input->setDecimals(4);
        input->setSingleStep(0.0001);//to be changed
        input->setSliderRange(1, 1000);
        input->setValueRange(min, max);
        input->setValue(node.value.toDouble());
        
        if(node.description.contains("Hide")) {
            input->hide();
        }
        connect(input, SIGNAL(scalarChanged(double,QString)), this, SLOT(OnValueChanged(double,QString)));
        input->setDisabled(d->disable);
    }
    auto ParameterControl::addCheck(ParameterNode node) -> void {
        StrCheckBox* check = new StrCheckBox(this);
        //QCheckBox* check = new QCheckBox(this);
        check->setText(node.displayName);//QLabel을 대신해준다        
        check->setToolTip(node.name);
        check->setToolTipDuration(0);
        bool default = false;
        if (node.value.toBool())
            default = true;                
        check->setChecked(default);
        d->layout->addWidget(check);
        d->checks[node.name] = check;
                
        if (node.name.contains("Enabler")) {            
            connect(check, SIGNAL(checkedStr(QString,int)), this, SLOT(OnInterfaceEnableChanged(QString,int)));
            d->enablers.push_back(node.name);
        }
        check->setDisabled(d->disable);
    }
    void ParameterControl::OnInterfaceEnableChanged(QString nodeName,int state) {
        Q_UNUSED(state)
        auto enable = d->checks[nodeName]->isChecked();
        auto key = nodeName.split("!")[0];
        auto trueKey = key + "True";
        auto falseKey = key + "False";        
        for(auto i=0;i<d->node_name.size();i++) {            
            auto node = d->list.GetNode(d->node_name[i]);
            auto type = node.unit;
            auto multiplier = false;
            if(node.unit.contains(trueKey)) {
                multiplier = false;
            }else if(node.unit.contains(falseKey)) {
                multiplier = true;
            }else {
                continue;
            }
            if (type.contains("ScalarValue")) {
                d->scalars[node.name]->setEnabled((enable^multiplier)&&(!d->disable));
                if(enable) {
                    d->scalars[node.name]->show();                    
                }else {
                    d->scalars[node.name]->hide();                    
                }
            }
            else if (type.contains("Path")) {
                d->paths[node.name]->setEnabled((enable^multiplier)&&(!d->disable));
                if(enable) {
                    d->paths[node.name]->show();                    
                }else {
                    d->paths[node.name]->hide();                    
                }
            }
            else if (type.contains("Select")) {
                d->combos[node.name]->setEnabled((enable^multiplier)&&(!d->disable));
                if(enable) {
                    d->combos[node.name]->show();
                }else {
                    d->combos[node.name]->hide();
                }
            }
            else if (type.contains("Check")) {
                d->checks[node.name]->setEnabled((enable^multiplier)&&(!d->disable));
                if(enable) {
                    d->checks[node.name]->show();
                }else {
                    d->checks[node.name]->hide();
                }
            }
            if(enable) {
                d->labels[node.name]->show();
                if(nullptr != d->executeBtn) {
                    d->executeBtn->show();
                }
            }else {
                d->labels[node.name]->hide();
                if(nullptr != d->executeBtn) {
                    d->executeBtn->hide();
                }
            }
        }
        auto fullName = d->param_name;
        fullName += "*";
        fullName += nodeName;
        emit checkChanged(enable, fullName);
    }

    auto ParameterControl::addPath(ParameterNode node) -> void {
        PathInput* path = new PathInput(this);
        path->setFolderType(true);
        path->setDefaultPath(node.value.toString());
        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        d->layout->addWidget(label);
        d->layout->addWidget(path);
        d->paths[node.name] = path;

        path->setName(node.name);
        if (node.description.contains("Hide")) {
            label->hide();
            path->hide();
        }
        connect(path, SIGNAL(sigPathChanged(QString, QString)), this, SLOT(OnPathChanged(QString, QString)));
        path->setDisabled(d->disable);
    }
    auto ParameterControl::addFile(ParameterNode node) ->void {
        PathInput* path = new PathInput(this);
        path->setFolderType(true);
        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        label->setObjectName("h9");
        d->layout->addWidget(label);
        d->layout->addWidget(path);
        d->paths[node.name] = path;

        path->setFolderType(false);
        path->setDefaultPath(node.value.toString());
        auto unit = node.unit;
        QStringList parser = unit.split(".");
        QString sum("");
        for (int i = 1; i < parser.size(); i++) {
            auto ext = parser[i];
            path->addExtension(ext);
        }        
        if (node.description.contains("Hide")) {
            label->hide();
            path->hide();
        }
        //path->setTypeName() add later for detail
        path->setDisabled(d->disable);
    }

    auto ParameterControl::getCheckValue(QString name) -> bool {
        return d->checks[name]->isChecked();
    }

    auto ParameterControl::setCheckValue(QString name, bool val) -> void {
        d->checks[name]->setChecked(val);
    }

    auto ParameterControl::getComboValue(QString name) -> int {
        return d->combos[name]->currentIndex();
    }

    auto ParameterControl::setComboValue(QString name, int val) -> void {
        d->combos[name]->setCurrentIndex(val);
    }

    auto ParameterControl::getScalarValue(QString name) -> double {
        return d->scalars[name]->getValue();
    }

    auto ParameterControl::setScalarValue(QString name, double val) -> void {
        d->scalars[name]->setValue(val);
    }

    auto ParameterControl::getStringValue(QString name) -> QString {
        return d->paths[name]->getPath();
    }

    auto ParameterControl::setStringValue(QString name, QString val) -> void {
        d->paths[name]->setPath(val);
    }

    auto ParameterControl::clearInterface() -> void {
        //TODO later
    }

    auto ParameterControl::parseParameter() -> bool {
        auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };
        if (d->param != nullptr) {
            if (!d->param.get()) {
                return false;
            }
            d->NoList.Clear();
            d->list.Clear();
            d->type_list.clear();
            d->BoundList.Clear();
            QList<int> tempOrder;
            QList<QString> tempName;
            auto nameNtypes = d->param->GetNameAndTypes();                            
            for (const auto& nnt : nameNtypes) {
                auto name = std::get<0>(nnt);                
                auto node = param2node(d->param, name);
                auto type = node.description;
                if (type.contains("DoNotMakeInterface")) {
                    d->NoList.AppendNode(node);
                    d->no_name.push_back(node.name);
                    continue;
                }
                if(type.contains("Bounder")) {
                    d->BoundList.AppendNode(node);
                    continue;
                }
                d->list.AppendNode(node);                
                tempName.push_back(node.name);
                d->node_name.push_back(QString());
                d->type_list[node.name] = std::get<1>(nnt);
                auto order = node.description.split("!")[1].toInt();                
                tempOrder.push_back(order);
            }            
            auto childrenNames = d->param->GetChildrenNames();
            for (const auto& childName : childrenNames) {                
                auto child = d->param->GetChild(childName);
                if (child.get()) {
                    auto nameNtypesChild = child->GetNameAndTypes();
                    for (const auto& nntChild : nameNtypesChild) {
                        auto node = param2node(child, std::get<0>(nntChild));
                        node.name = QString("%1.%2").arg(childName).arg(std::get<0>(nntChild));
                        d->list.AppendNode(node);
                        d->type_list[node.name] = std::get<1>(nntChild);                                                
                    }
                }
            }            
            for(auto i=0;i<tempOrder.count();i++) {
                auto idx = tempOrder[i];
                d->node_name[idx] = tempName[i];
            }
            return true;
        } else {
            QLOG_ERROR() << "Current Parameter is null";
            return false;
        }
    }
    void ParameterControl::OnExecuteClicked() {        
        emit execute(d->param_name);
    }

    auto ParameterControl::buildInterface() -> void {
        d->labels.clear();
        d->scalars.clear();
        d->combos.clear();
        d->paths.clear();
        d->sliders.clear();
        d->checks.clear();
        d->enablers.clear();
        d->executeBtn = nullptr;
        parseParameter();

        for(auto i : d->BoundList.GetNames()) {
            auto node = d->BoundList.GetNode(i);            
            if (node.unit.contains("Lower")) {
                auto split = node.name.split("!");
                d->lowerBounds[split[0]] = split[1];
            }
            else if (node.unit.contains("Upper")) {
                auto split = node.name.split("!");
                d->upperBounds[split[0]] = split[1];
            }            
        }

        for(auto i=0;i<d->node_name.size();i++){
            auto node = d->list.GetNode(d->node_name[i]);
            auto type = node.unit;            
            if (type.contains("FilePath")) {
                addFile(node);
            }
            else if (type.contains("ScalarValue")) {
                //auto scalarType = d->type_list[d->node_name[i]];
                auto scalarType = d->type_list[d->node_name[i]];
                if (scalarType == QJsonValue::Double) {
                    auto unit = node.unit;
                    QStringList parser = unit.split(".");
                    auto typeString = parser[1];
                    if (typeString.compare("int") == 0) {
                        addIntValue(node);
                    }
                    else if (typeString.compare("double") == 0) {
                        addDoubleValue(node);
                    }
                }
            }
            else if (type.contains("SelectOption")) {
                auto hide = type.contains("Hide");                
                addComboValue(node,hide);
            }
            else if (type.contains("ScalarRange")) {
                //auto scalarType = d->type_list[d->node_name[i]];
                auto scalarType = d->type_list[d->node_name[i]];
                if (scalarType == QJsonValue::Double) {
                    //not for now
                }
                else if (scalarType == QJsonValue::String) {
                    //not for now
                }
            }
            else if (type.contains("DirPath")) {
                addPath(node);
            }
            else if (type.contains("TFCheck")) {
                addCheck(node);
            }            
        }

        for(auto k : d->scalars.keys()) {
            if(d->lowerBounds.contains(k)) {
                auto lval  = d->scalars[k]->getValue();
                d->scalars[d->lowerBounds[k]]->setLower(lval);
            }
            if(d->upperBounds.contains(k)) {
                auto uval = d->scalars[k]->getValue();
                d->scalars[d->upperBounds[k]]->setUpper(uval);
            }
        }

        if (!d->hideExecute) {
            d->executeBtn = new QPushButton;
            d->executeBtn->setText("Execute");
            connect(d->executeBtn, SIGNAL(clicked()), this, SLOT(OnExecuteClicked()));
            d->layout->addWidget(d->executeBtn);
        }
        d->layout->addStretch();

        //init enabled node
        for(auto i=0;i<d->enablers.size();i++) {
            auto enable = d->checks[d->enablers[i]]->isChecked();
            OnInterfaceEnableChanged(d->enablers[i], enable);
        }
    }
}
