#define LOGGER_TAG "[ParamControl]"
#include <TCLogger.h>

#include <iostream>


#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>

#include "ParamConverter.h"
#include "ParamControl.h"
#include "ParamTitle.h"

namespace TC {
	struct ParamControl::Impl {
		//interface
		QVBoxLayout* layout{ nullptr };
		RangeMap sliders;
		ScalarMap scalars;
		PathMap paths;
		QMap<QString, ParamTitle*> titles;
		ComboMap combos;
		CheckMap checks;
		QPushButton* executeBtn{ nullptr };

		//constraints
		QMap<QString, QString> lowerBounds;
		QMap<QString, QString> upperBounds;
		//variables
		QString param_name;
		QString algo_name;
		IParameter::Pointer param{ nullptr };
		IUiParameter::Pointer uiParam{ nullptr };

		//parameter list
		ParameterList list;
		TMap type_list;

		//UI-parameter controls
		QMap<QString, EnablerNode> enablers;
		QMap<QString, SetterNode> setters;

		//UI-metainfo
		QStringList node_order;
		QStringList hide_list;
		QList<BounderNode> bound_list;

		bool hideExecution{ false };
		bool disable{ false };
	};
	ParamControl::ParamControl(QWidget* parent) : QWidget(parent), d{ new Impl } {
		d->layout = new QVBoxLayout(this);
		d->layout->setContentsMargins(0, 0, 0, 0);
		setLayout(d->layout);
		setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	}
	ParamControl::~ParamControl() {
		disconnect();
	}
	auto ParamControl::SetName(QString name) -> void {
		d->param_name = name;
	}
	auto ParamControl::SetAlgoName(QString name) -> void {
		d->algo_name = name;
    }
	auto ParamControl::SetHighLight(bool isHi) -> void {
		if (nullptr != d->executeBtn) {
			if (isHi) {
				d->executeBtn->setStyleSheet("background-color: rgb(91,139,151);");
			}
			else {
				d->executeBtn->setStyleSheet("");
			}
		}
	}
	auto ParamControl::SetParameter(IParameter::Pointer param) -> void {
		d->param = param;
	}
	auto ParamControl::GetParameter() -> IParameter::Pointer {
		return d->param;
	}
	auto ParamControl::SetUiParameter(IUiParameter::Pointer uiParam) -> void {
		d->uiParam = uiParam;
	}
	auto ParamControl::SetDiable(bool isDisable) -> void {
		d->disable = isDisable;
	}
	auto ParamControl::SetHideExecute(bool isHide) -> void {
		d->hideExecution = isHide;
	}
	auto ParamControl::SetParameterValue(IParameter::Pointer param, bool blockSig, bool isOld) -> void {
		d->param = param;
		auto names = param->GetNameAndTypes();		
		for (const auto nnt : names) {
			auto name = std::get<0>(nnt);
			auto type = std::get<1>(nnt);
			auto unit = param->Unit(name);

			auto uiName = name;
					
			if (isOld) {
				uiName = ParamConverter::GetInstance()->ConvertName(name);
				unit = ParamConverter::GetInstance()->ConvertUnit(unit);
			}

			if (unit.contains("TFCheck")) {
				auto chk = param->GetValue(name).toBool();
				if (d->checks.contains(uiName)) {
					d->checks[uiName]->setChecked(chk);
				}				
			}
			else if (unit.contains("SelectOption")) {
				auto val = param->GetValue(name).toInt();
				if (d->combos.contains(uiName)) {
					d->combos[uiName]->setCurrentIndex(val);
				}
			}
			else if (type == QJsonValue::String) {
				auto path = param->GetValue(name).toString();
				if (d->paths.contains(uiName)) {
					d->paths[uiName]->setPath(path);
				}
			}
			else if (type == QJsonValue::Double) {
				auto val = param->GetValue(name).toDouble();
				if (d->scalars.contains(uiName)) {
					d->scalars[uiName]->setValue(val, blockSig);
				}
			}
		}

	}
	auto ParamControl::GetScalarValue(QString name) -> double {
		return d->scalars[name]->getValue();
	}
	auto ParamControl::SetScalarValue(QString name, double val) -> void {
		d->scalars[name]->setValue(val);
	}
	auto ParamControl::GetStringValue(QString name) -> QString {
		return d->paths[name]->getPath();
	}
	auto ParamControl::SetStringValue(QString name, QString val) -> void {
		d->paths[name]->setPath(val);
	}
	auto ParamControl::GetCheckValue(QString name) -> bool {
		return d->checks[name]->isChecked();
	}
	auto ParamControl::SetCheckValue(QString name, bool val) -> void {
		d->checks[name]->setChecked(val);
	}
	auto ParamControl::GetComboValue(QString name) -> int {
		return d->combos[name]->currentIndex();
	}
	auto ParamControl::SetComboValue(QString name, int val) -> void {
		d->combos[name]->setCurrentIndex(val);
	}


	//SLOTS
	void ParamControl::OnValueChanged(double val, QString val_name) {		
		if (d->upperBounds.contains(val_name)) {
			d->scalars[d->upperBounds[val_name]]->setUpper(val);
		}
		if (d->lowerBounds.contains(val_name)) {
			d->scalars[d->lowerBounds[val_name]]->setLower(val);
		}

		emit valChanged(val, val_name,d->param_name,d->algo_name);
	}
	void ParamControl::OnInterfaceEnableChanged(QString nodeName, int state) {
		auto eNode = d->enablers[nodeName];
		auto enable = d->checks[nodeName]->isChecked();
		auto falseList = eNode.hideNodes;
		auto trueList = eNode.showNodes;
		for (auto i = 0; i < falseList.count(); i++) {
			auto falseNode = falseList[i];
			if (d->scalars.contains(falseNode)) {
				d->scalars[falseNode]->setEnabled(!enable && !d->disable);
				d->scalars[falseNode]->setVisible(!enable);
			}
			else if (d->checks.contains(falseNode)) {
				d->checks[falseNode]->setEnabled(!enable && !d->disable);
				d->checks[falseNode]->setVisible(!enable);
			}
			else if (d->paths.contains(falseNode)) {
				d->paths[falseNode]->setEnabled(!enable && !d->disable);
				d->paths[falseNode]->setVisible(!enable);
			}
			else if (d->combos.contains(falseNode)) {
				d->combos[falseNode]->setEnabled(!enable && !d->disable);
				d->combos[falseNode]->setVisible(!enable);
			}
			if (d->titles.contains(falseNode)) {
				d->titles[falseNode]->setVisible(!enable);
			}
		}
		for (auto i = 0; i < trueList.count(); i++) {
			auto trueNode = trueList[i];
			if (d->scalars.contains(trueNode)) {
				d->scalars[trueNode]->setEnabled(enable && !d->disable);
				d->scalars[trueNode]->setVisible(enable);
			}
			else if (d->checks.contains(trueNode)) {
				d->checks[trueNode]->setEnabled(enable && !d->disable);
				d->checks[trueNode]->setVisible(enable);
			}
			else if (d->paths.contains(trueNode)) {
				d->paths[trueNode]->setEnabled(enable && !d->disable);
				d->paths[trueNode]->setVisible(enable);
			}
			else if (d->combos.contains(trueNode)) {
				d->combos[trueNode]->setEnabled(enable && !d->disable);
				d->combos[trueNode]->setVisible(enable);
			}
			if (d->titles.contains(trueNode)) {
				d->titles[trueNode]->setVisible(enable);
			}
		}
		if (nullptr != d->executeBtn) {
			d->executeBtn->setEnabled(enable);
		}		
		emit checkChanged(enable, nodeName,d->param_name,d->algo_name);
	}
	void ParamControl::OnSetterChanged(QString nodeName, QString curText, int idx) {
		auto sNode = d->setters[nodeName];
		auto node_names = sNode.node_names;
		auto valList = sNode.valueList;
		auto selectedValList = valList[curText];
		for (auto i = 0; i < node_names.count(); i++) {
			auto key = node_names[i];
			auto val = selectedValList[key];
			if (d->scalars.contains(key)) {
				d->scalars[key]->setValue(val.toDouble());
			}
			else if (d->paths.contains(key)) {
				d->paths[key]->setPath(val.toString());
			}
			else if (d->combos.contains(key)) {
				d->combos[key]->setCurrentIndex(val.toInt());
			}
			else if (d->checks.contains(key)) {
				d->checks[key]->setChecked(val.toInt());
			}
		}
		//combo changed signal		
		emit comboChanged(idx, nodeName,d->param_name,d->algo_name);
	}
	void ParamControl::OnExecuteClicked() {		
		emit execute(d->param_name,d->algo_name);
	}
	void ParamControl::OnPathChanged(QString val, QString val_name) {		
		emit pathChanged(val, val_name,d->param_name,d->algo_name);
	}
	auto ParamControl::BuildInterface() -> void {
		d->titles.clear();
		d->scalars.clear();
		d->combos.clear();
		d->paths.clear();
		d->sliders.clear();
		d->checks.clear();

		d->enablers.clear();
		d->hide_list.clear();
		d->node_order.clear();
		d->bound_list.clear();
		d->setters.clear();

		d->executeBtn = nullptr;
		ParseParameter();
		ParseUiParameter();

		if (d->node_order.isEmpty()) {
			std::cout << "Parameter must has order" << std::endl;
			return;
		}

		for (auto i = 0; i < d->node_order.count(); i++) {
			auto key = d->node_order[i];
			if (d->list.GetNames().contains(key)) {
				//stand alone scalar nodes
				auto node = d->list.GetNode(key);
				auto type = node.unit;
				if (type.contains("FilePath")) {
					AddFile(node);
				}
				else if (type.contains("ScalarValue")) {
					auto scalarType = d->type_list[node.name];
					if (scalarType == QJsonValue::Double) {
						auto unit = node.unit;
						auto typeString = unit.split(".")[1];
						if (typeString.compare("int") == 0) {
							AddIntValue(node);
						}
						else if (typeString.compare("double") == 0) {
							AddDoubleValue(node);
						}
					}
				}
				else if (type.contains("TFCheck")) {
					AddCheck(node.name, node.value.toBool(), d->enablers[key]);
				}
				else if (type.contains("SelectOption")) {
					AddComboValue(node.name, node.value.toInt(), d->setters[key]);
				}
				else if (type.contains("DirPath")) {
					AddPath(node);
				}
			}
		}

		//set bounder (or other restriction info nodes)
		for (auto i = 0; i < d->bound_list.count(); i++) {
			auto bounder = d->bound_list[i];
			if (bounder.type == "Lower") {
				d->lowerBounds[bounder.bounder] = bounder.boundee;
			}
			else if (bounder.type == "Upper") {
				d->upperBounds[bounder.bounder] = bounder.boundee;
			}
		}

		if (false == d->bound_list.isEmpty()) {
			//init bounds
			auto scalarKeys = d->scalars.keys();
			for (auto sKey : scalarKeys) {
				if (d->lowerBounds.contains(sKey)) {
					auto lowerVal = d->scalars[sKey]->getValue();
					d->scalars[d->lowerBounds[sKey]]->setLower(lowerVal);
				}
				if (d->upperBounds.contains(sKey)) {
					auto upperVal = d->scalars[sKey]->getValue();
					d->scalars[d->upperBounds[sKey]]->setUpper(upperVal);
				}
			}
		}

		if (!d->hideExecution) {
			d->executeBtn = new QPushButton;
			d->executeBtn->setText("Execute");
			connect(d->executeBtn, SIGNAL(clicked()), this, SLOT(OnExecuteClicked()));
			d->layout->addWidget(d->executeBtn);
			if (false == d->enablers.isEmpty()) {
				d->executeBtn->setEnabled(false);
			}
		}
		d->layout->addStretch();

		for (auto i = 0; i < d->enablers.count(); i++) {
			auto key = d->enablers.keys()[i];
			auto enable = d->checks[key]->isChecked();
			OnInterfaceEnableChanged(key, enable);
		}
	}
	auto ParamControl::ParseUiParameter() -> void {
		if (nullptr == d->uiParam) {
			return;
		}
		if (nullptr == d->uiParam.get()) {
			return;
		}
		//parse enabler
		auto enableNames = d->uiParam->GetEnablerNames();
		for (auto eName : enableNames) {
			auto eTuple = d->uiParam->GetEnabler(eName);
			EnablerNode eNode;
			eNode.showNodes = std::get<0>(eTuple);
			eNode.hideNodes = std::get<1>(eTuple);
			eNode.displayName = std::get<2>(eTuple);
			d->enablers[eName] = eNode;
		}
		//parse sorter
		auto sortNames = d->uiParam->GetSorterNames();
		for (auto sName : sortNames) {
			d->node_order = d->uiParam->GetSorter(sName);
		}
		//parse hider
		auto hiderNames = d->uiParam->GetHiderNames();
		for (auto hName : hiderNames) {
			d->hide_list = d->uiParam->GetHider(hName);
		}
		//parse bounder
		auto bounderNames = d->uiParam->GetBounderNames();
		for (auto bName : bounderNames) {
			auto bTuple = d->uiParam->GetBounder(bName);
			BounderNode bNode;
			bNode.bounder = std::get<0>(bTuple);
			bNode.boundee = std::get<1>(bTuple);
			bNode.type = std::get<2>(bTuple);
			d->bound_list.push_back(bNode);
		}
		//parse setter
		auto setterNames = d->uiParam->GetSetterNames();
		for (auto sName : setterNames) {
			auto sTuple = d->uiParam->GetSetter(sName);
			SetterNode sNode;
			sNode.displayName = std::get<0>(sTuple);
			sNode.node_names = std::get<1>(sTuple);
			sNode.headers = std::get<2>(sTuple);
			sNode.valueList = std::get<3>(sTuple);
			d->setters[sName] = sNode;
		}
	}

	auto ParamControl::SetFixedControl(QString valueName, bool fixed) -> void {
        if(d->sliders.contains(valueName)) {
			d->sliders[valueName]->setEnabled(fixed);
        }else if(d->scalars.contains(valueName)) {
            d->scalars[valueName]->setEnabled(fixed);
        }else if(d->paths.contains(valueName)) {
			d->paths[valueName]->setEnabled(fixed);
        }else if(d->combos.contains(valueName)) {
			d->combos[valueName]->setEnabled(fixed);
        }else if(d->checks.contains(valueName)) {
			d->checks[valueName]->setEnabled(fixed);
        }
    }

	auto ParamControl::ParseParameter() -> void {
		auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
			ParameterNode node;
			node.name = name;
			node.displayName = param->DisplayName(name);
			node.description = param->Description(name);
			node.unit = param->Unit(name);
			node.value = param->GetValue(name);
			node.defaultValue = param->GetDefault(name);
			auto range = param->GetRange(name);
			node.minValue = std::get<0>(range);
			node.maxValue = std::get<1>(range);

			return node;
		};
		if (nullptr == d->param) {
			return;
		}
		if (nullptr == d->param.get()) {
			return;
		}

		//clear existing meta information
		d->list.Clear();
		d->type_list.clear();
		//Parse parameter first
		auto nameNtypes = d->param->GetNameAndTypes();
		for (const auto& nnt : nameNtypes) {
			auto name = std::get<0>(nnt);
			auto node = param2node(d->param, name);
			d->list.AppendNode(node);//here exist type information
			d->type_list[node.name] = std::get<1>(nnt);
		}
	}
	auto ParamControl::AddPath(ParameterNode node) -> void {
		PathInput* path = new PathInput(this);
		path->setFolderType(true);
		path->setDefaultPath(node.value.toString());

	    const auto title = new ParamTitle(node.displayName);
		d->titles[node.name] = title;
		d->layout->addWidget(title);
		d->layout->addWidget(path);
		d->paths[node.name] = path;

		path->setName(node.name);
		if (d->hide_list.contains(node.name)) {
			title->hide();
			path->hide();
		}
		connect(path, SIGNAL(sigPathChanged(QString, QString)), this, SLOT(OnPathChanged(QString, QString)));
		path->setDisabled(d->disable);
	}

	auto ParamControl::AddFile(ParameterNode node) -> void {
		PathInput* path = new PathInput(this);
		path->setFolderType(true);

	    const auto title = new ParamTitle(node.displayName);
		d->titles[node.name] = title;
		d->layout->addWidget(title);
		d->layout->addWidget(path);
		d->paths[node.name] = path;

		path->setFolderType(false);
		path->setDefaultPath(node.value.toString());
		auto unit = node.unit;
		QStringList parser = unit.split(".");
		QString sum("");
		for (int i = 1; i < parser.size(); i++) {
			auto ext = parser[i];
			path->addExtension(ext);
		}
		if (d->hide_list.contains(node.name)) {
			title->hide();
			path->hide();
		}
		//path->setTypeName() add later for detail
		path->setDisabled(d->disable);
	}

	auto ParamControl::AddDoubleValue(ParameterNode node) -> void {
		ScalarInput* input = new ScalarInput(this);
		if (!d->hide_list.contains(node.name)) {
			const auto title = new ParamTitle(node.displayName);
			d->titles[node.name] = title;
			d->layout->addWidget(title);
		}
		d->layout->addWidget(input);
		d->scalars[node.name] = input;
		auto min = node.minValue.toDouble();
		auto max = node.maxValue.toDouble();
		input->setName(node.name);
		input->setDecimals(4);
		input->setSingleStep(0.0001);//to be changed
		input->setSliderRange(1, 1000);
		input->setValueRange(min, max);
		input->setValue(node.value.toDouble());

		if (d->hide_list.contains(node.name)) {
			input->hide();
		}
		connect(input, SIGNAL(scalarChanged(double, QString)), this, SLOT(OnValueChanged(double, QString)));
		input->setDisabled(d->disable);
	}
	auto ParamControl::AddIntValue(ParameterNode node) -> void {
		ScalarInput* input = new ScalarInput(this);
		if (!d->hide_list.contains(node.name)) {
			const auto title = new ParamTitle(node.displayName);
			d->titles[node.name] = title;
			d->layout->addWidget(title);
		}
		d->layout->addWidget(input);
		d->scalars[node.name] = input;
		auto min = node.minValue.toDouble();
		auto max = node.maxValue.toDouble();

		input->setName(node.name);
		input->setDecimals(0);
		input->setSingleStep(1.0);
		input->setValueRange(min, max);
		input->setValue(node.value.toInt());
		if (d->hide_list.contains(node.name)) {
			input->hide();
		}
		connect(input, SIGNAL(scalarChanged(double, QString)), this, SLOT(OnValueChanged(double, QString)));
		input->setDisabled(d->disable);
	}
	auto ParamControl::AddCheck(QString name, bool defaultCheck, EnablerNode node) -> void {
		StrCheckBox* check = new StrCheckBox(this);
		check->setText(node.displayName);
		check->setToolTip(name);
		check->setToolTipDuration(0);
		d->layout->addWidget(check);
		d->checks[name] = check;

		check->setChecked(defaultCheck);
		connect(check, SIGNAL(checkedStr(QString, int)), this, SLOT(OnInterfaceEnableChanged(QString, int)));

		check->setDisabled(d->disable);
	}

	auto ParamControl::AddComboValue(QString name, int defaultIdx, SetterNode node) -> void {
		const auto title = new ParamTitle(node.displayName);
		d->titles[name] = title;

		auto hide = d->hide_list.contains(name);
		StrComboBox* input = new StrComboBox(this, hide && !d->hideExecution);
		input->SetToolTip(name);
		input->SetToolTipDuration(0);

		d->layout->addWidget(title);
		d->layout->addWidget(input);
		d->combos[name] = input;

		auto header = node.headers;
		for (auto i = 0; i < header.count(); i++) {
			input->addItem(header[i]);
		}
		input->setCurrentIndex(defaultIdx);
		connect(input, SIGNAL(comboStr(QString, QString, int)), this, SLOT(OnSetterChanged(QString, QString, int)));
		input->setDisabled(d->disable);
	}
}