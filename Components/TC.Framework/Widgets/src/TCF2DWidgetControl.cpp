#define LOGGER_TAG "[2DImageWidget]"
#include <TCLogger.h>

#include "TCF2DWidgetControl.h"

#include <iostream>

#include "TCFMetaReader.h"
#include "TCFRawDataReader.h"

namespace  TC {
    struct TCF2DWidgetControl::Impl {
        TC::IO::TCFMetaReader* metaReader{ nullptr };
        TC::IO::TCFRawDataReader* dataReader{ nullptr };

        TC::IO::TCFMetaReader::Meta::Pointer meta;

        QString path;
    };

    TCF2DWidgetControl::TCF2DWidgetControl()
    : d{ new Impl } {
    }

    TCF2DWidgetControl::~TCF2DWidgetControl() {
        
    }

    bool TCF2DWidgetControl::OpenTCF(const QString& path) const {
        if(true == path.isEmpty()) {
            return false;
        }

        d->metaReader = new TC::IO::TCFMetaReader;
        d->dataReader = new TC::IO::TCFRawDataReader;

        d->meta = d->metaReader->Read(path);
        if(nullptr == d->meta) {
            QLOG_ERROR() << "Meta Info Read Failed";
            return false;
        }

        if(false == d->dataReader->Open(path)) {
            QLOG_ERROR() << "Data Reader Open Failed";
            return false;
        }

        d->path = path;

        return true;
    }

    void TCF2DWidgetControl::Reset() {
        d.reset(new Impl);
    }

    auto TCF2DWidgetControl::GetFilePath() const ->QString {
        return d->path;
    }

    void TCF2DWidgetControl::GetUsableType(bool& ht, bool& fl, bool& bf) const {
        if(nullptr == d->meta) {
            ht = false;
            fl = false;
            bf = false;

            return;
        }

        ht = d->meta->data.data2DMIP.exist;
        fl = d->meta->data.data2DFLMIP.exist;
        bf = d->meta->data.dataBF.exist;
    }

    auto TCF2DWidgetControl::LoadHTMIPData(const int& frameIndex, std::shared_ptr<unsigned char[]>& rawData, int& w, int& h) const ->bool {
        if (nullptr == d->meta) {
            return false;
        }

        if (nullptr == d->dataReader) {
            return false;
        }

        auto min = d->meta->data.data2DMIP.riMin;
        auto max = d->meta->data.data2DMIP.riMax;
        int length = 0;
        if (!d->meta->data.isLDM) {
            length = d->meta->data.data2DMIP.sizeX * d->meta->data.data2DMIP.sizeY;
        }        
        //const auto length = d->meta->data.data2DMIP.sizeX * d->meta->data.data2DMIP.sizeY;

        auto temp = std::shared_ptr<unsigned short[]>();
        int x = 0;
        int y = 0;
        if(d->meta->data.isLDM) {            
            d->dataReader->ReadLdmHTMIP(frameIndex, temp,x,y);
            length = x * y;
        }else {
            d->dataReader->ReadHTMIP(frameIndex, temp);
        }

        if(nullptr == temp) {
            return false;
        }

        if (max == 0.0 || min == 0.0) {
            max = temp.get()[0] / 10000.f;
            min = temp.get()[0] / 10000.f;

            for (auto i = 0; i < length; ++i) {
                const auto ele = temp.get()[i] / 10000.f;
                if (ele > max) {
                    max = ele;
                }

                if (ele < min) {
                    min = ele;
                }

                d->meta->data.data2DMIP.riMin = min;
                d->meta->data.data2DMIP.riMax = max;
            }
        }

        if(d->meta->data.isLDM) {
            w = x;
            h = y;
        }else {
            w = d->meta->data.data2DMIP.sizeX;
            h = d->meta->data.data2DMIP.sizeY;
        }        
        const auto range = max - min;

        rawData = std::shared_ptr<unsigned char[]>(new unsigned char[length]);
        for(auto i = 0; i < length; ++i) {
            auto value = temp.get()[i] / 10000.f;
            value = std::max<double>(value, min);
            value = std::min<double>(value, max);
            value = value - min;

            rawData.get()[i] = static_cast<unsigned char>(value / range * 255);
        }

        return true;
    }

    auto TCF2DWidgetControl::LoadFLMIPData(const int& frameIndex, std::shared_ptr<uint32_t[]>& rawData, int& w, int& h) const ->bool {
        if (nullptr == d->dataReader) {
            return false;
        }

        if(d->meta->data.isLDM) {
            d->dataReader->ReadLdmFLMIP(frameIndex, rawData, w, h);
        }else {
            d->dataReader->ReadFLMIP(frameIndex, rawData);
            w = d->meta->data.data2DFLMIP.sizeX;
            h = d->meta->data.data2DFLMIP.sizeY;
        }

        return true;
    }

    auto TCF2DWidgetControl::LoadBFData(const int& frameIndex, std::shared_ptr<uint32_t[]>& rawData, int& w, int& h) const ->bool {
        if (nullptr == d->dataReader) {
            return false;
        }

        if(d->meta->data.isLDM) {
            d->dataReader->ReadLdmBF(frameIndex, rawData, w, h);
        }else {
            w = d->meta->data.dataBF.sizeX;
            h = d->meta->data.dataBF.sizeY;

            d->dataReader->ReadBF(frameIndex, rawData);
        }                

        return true;
    }

    auto TCF2DWidgetControl::GetHTMIPTimeFrame() const ->int {
        if(nullptr == d->meta) {
            return -1;
        }

        return d->meta->data.data2DMIP.dataCount;
    }

    auto TCF2DWidgetControl::GetFLMIPTimeFrame() const ->int {
        if (nullptr == d->meta) {
            return -1;
        }

        return d->meta->data.data2DFLMIP.dataCount;
    }
}

