#include "SimpleTCF2DViewer.h"

#include <iostream>
#include <QApplication>
#include <QEvent>
#include <QGraphicsItem>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QWheelEvent>
#include <QStyle>
#include <QMenu>
#include "ImageTypeControlWidget.h"
#include "TimelapseControlWidget.h"

#include "TCF2DWidgetControl.h"

namespace  TC {
    /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// ImageWidget ////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct ImageWidget::Impl {
        bool zoomable{ false };

        double zoom{ 1.0 };
        const double zoomFactor{ 1.002 };

        QGraphicsScene* scene{ nullptr };
        QGraphicsPixmapItem* imageItem{ nullptr };

        QPoint mousePoint;
        QPoint zoomPoint;

        QImage image;//here
    };

    ImageWidget::ImageWidget(QWidget* parent) : QGraphicsView(parent), d{ new Impl } {
        this->setContentsMargins(0, 0, 0, 0);

        installEventFilter(this);
        setMouseTracking(true);
    }

    ImageWidget::~ImageWidget(){
        
    }

    void ImageWidget::SetImage(const QImage& image, bool fitting,bool late_load) {
        //if (image.isNull()) return;                

        d->scene = new QGraphicsScene(this);
        setScene(d->scene);        
                
        d->imageItem = new QGraphicsPixmapItem();
        d->scene->addItem(d->imageItem);

        if(image.isNull()) {// if image is null use empty pixmap
            if(!late_load){
                QPixmap pixmap( 200, 200 );
                pixmap.fill(Qt::black );
                d->imageItem->setPixmap(pixmap);
            }
        }
        else if (image.format() == QImage::Format_Indexed8) {            
            d->image = image.convertToFormat(QImage::Format_RGB32);            
            if(!late_load){
                d->imageItem->setPixmap(QPixmap::fromImage(d->image));
            }
        }
        else {
            d->image = image;
            if(!late_load){                
                d->imageItem->setPixmap(QPixmap::fromImage(d->image));
            }
        }

        //setVisible(false);

        if (true == fitting) {
            //SetZoomFit();
        }
    }
    auto ImageWidget::ShowImage(const bool& show) -> void {
        if(show) {
            if(false == d->image.isNull()) {                
                if (false == d->image.hasAlphaChannel()) {
                    d->imageItem->setPixmap(QPixmap::fromImage(d->image));
                }
            }else {
                QPixmap pixmap( 200, 200 );
                pixmap.fill(Qt::black );
                d->imageItem->setPixmap(pixmap);
            }
        }else {
            //d->imageItem->setPixmap(QPixmap());
        }
    }

    void ImageWidget::SetNo() {
        d->scene = new QGraphicsScene(this);
        setScene(d->scene);

        d->imageItem = new QGraphicsPixmapItem();
        d->scene->addItem(d->imageItem);
    }

    void ImageWidget::Clear() {
        d->zoom = 1.0;
        if (d->scene) {
            d->scene->clear();
        }
    }

    void ImageWidget::SetZoomable(bool able) {
        d->zoomable = able;
    }

    auto ImageWidget::GetZoom() const ->double {
        return d->zoom;
    }

    auto ImageWidget::GetZoomPoint() const ->QPoint {
        return d->zoomPoint;
    }

    void ImageWidget::SetZoom(const QPoint& center, const double& factor) {
        d->zoomPoint = center;

        const auto sceneCenter = this->mapToScene(center);

        scale(factor, factor);
        centerOn(sceneCenter);

        d->zoom *= factor;

        const auto delta_viewport_pos = center - QPointF(viewport()->width() / 2.0, viewport()->height() / 2.0);
        const auto viewport_center = mapFromScene(sceneCenter) - delta_viewport_pos;
        centerOn(mapToScene(viewport_center.toPoint()));
    }

    auto ImageWidget::SetZoomFit() -> void {
        if(0.0 == this->width() || 0.0 == this->height()) {
            return;
        }

        if(nullptr == scene()) {
            return;
        }

        fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
    }

    auto ImageWidget::eventFilter(QObject* object, QEvent* event) -> bool {
        if(false == d->zoomable) {
            return false;
        }

        if (event->type() == QEvent::MouseMove) {
            d->mousePoint = static_cast<QMouseEvent*>(event)->pos();

        }
        else if (event->type() == QEvent::Wheel) {
            const auto wheelEvent = static_cast<QWheelEvent*>(event);

            if (QApplication::keyboardModifiers() == Qt::ControlModifier) {
                if (wheelEvent->angleDelta().y() != 0){
                    double angle = wheelEvent->angleDelta().y();
                    const double factor = std::pow(d->zoomFactor, angle);
                    SetZoom(d->mousePoint, factor);
                    return true;
                }
            }
        }
        else if (event->type() == QEvent::MouseButtonDblClick) {
            const auto mouseEvent = static_cast<QMouseEvent*>(event);

            if (mouseEvent->button() == Qt::RightButton) {
                SetZoomFit();
            }
        }

        Q_UNUSED(object)

        return false;
    }


    /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// TCF2DWidget ////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct TCF2DWidget::Impl {
        ImageWidget* imageWidget{ nullptr };
        TCF2DWidgetControl* control{ nullptr };

        bool selectable{ false };
        bool selected{ false };

        bool keepSquare{ false };
    };

    TCF2DWidget::TCF2DWidget(QWidget* parent)
        : QWidget(parent)
        , d{ new Impl } {
        this->setContentsMargins(0, 0, 0, 0);

        d->control = new TCF2DWidgetControl;

        d->imageWidget = new ImageWidget;
        d->imageWidget->hide();

        auto mainLayout = new QHBoxLayout();
        mainLayout->addWidget(d->imageWidget);
        mainLayout->setContentsMargins(0, 0, 0, 0);
        this->setLayout(mainLayout);

        d->imageWidget->setObjectName("panel-contents-image");
        d->imageWidget->setProperty("selected", false);
    }

    TCF2DWidget::~TCF2DWidget() = default;

    bool TCF2DWidget::SetTCFPath(const QString & path) const {
        if(true == path.isEmpty()) {
            return false;

        }

        if (false == d->control->OpenTCF(path)) {
            return false;
        }

        return true;
    }
    
    auto TCF2DWidget::GetTCFPath() const ->QString {
        return d->control->GetFilePath();
    }
    auto TCF2DWidget::SetVisible(bool vis) -> void {
        //setVisible(vis);        
        if(vis) {
            d->imageWidget->ShowImage(vis);
            d->imageWidget->show();
        }
    }
    bool TCF2DWidget::ShowImage(const ImageType & type, const int& frame,bool late_load) const {
        if (nullptr == d->imageWidget) {
            return false;
        }

        bool existHT = false;
        bool existFL = false;
        bool existBF = false;
        d->control->GetUsableType(existHT, existFL, existBF);

        int w = 0;
        int h = 0;

        if (ImageType::HT == type) {            
            auto hdata = std::shared_ptr<unsigned char[]>();            
            auto is_load = d->control->LoadHTMIPData(frame, hdata, w, h);
            if(is_load) {
                const QImage image(hdata.get(), w, h, w, QImage::Format_Indexed8);
                d->imageWidget->SetImage(image, true,late_load);
            }else {
                d->imageWidget->SetImage(QImage(), true,late_load);
            }            
        }
        else if (ImageType::FL == type) {
            if (false == existFL) {
                return false;
            }

            auto fdata = std::shared_ptr<uint32_t[]>();
            auto is_load = d->control->LoadFLMIPData(frame, fdata, w, h);

            if (is_load) {
                const QImage image(reinterpret_cast<uchar*>(fdata.get()), w, h, QImage::Format_ARGB32);
                d->imageWidget->SetImage(image, true, late_load);
            }else {
                d->imageWidget->SetImage(QImage(), true, late_load);
            }
        }
        else if (ImageType::BF == type) {
            if (false == existBF) {
                return false;
            }

            auto bdata = std::shared_ptr<uint32_t[]>();
            d->control->LoadBFData(frame, bdata, w, h);
            
            const QImage image(reinterpret_cast<uchar*>(bdata.get()), w, h, QImage::Format_ARGB32);
            d->imageWidget->SetImage(image, true,late_load);
        }
        else if(ImageType::No == type) {
            d->imageWidget->SetNo();
        }

        d->imageWidget->setToolTip(d->control->GetFilePath());

        return true;
    }

    bool TCF2DWidget::SetImageType(const ImageType & type) const {
        return this->ShowImage(type, 0);
    }

    void TCF2DWidget::SetPixmap(const QPixmap& pixmap) const {
        d->imageWidget->SetImage(pixmap.toImage(), true);
    }

    void TCF2DWidget::SetLayout(QLayout* layout) const {
        d->imageWidget->setLayout(layout);
    }

    void TCF2DWidget::GetUsableType(bool& ht, bool& fl, bool& bf) const {
        d->control->GetUsableType(ht, fl, bf);
    }

    auto TCF2DWidget::GetTimeFrameCount(const ImageType& type) const ->int {
        if(type == HT) {
            return d->control->GetHTMIPTimeFrame();
        }
        else if(type == FL) {
            return d->control->GetFLMIPTimeFrame();
        }
        else {
            return -1;
        }
    }

    auto TCF2DWidget::GetSelectable() const ->bool {
        return d->selectable;
    }

    void TCF2DWidget::SetSelectable(bool able) const {
        d->selectable = able;
    }

    auto TCF2DWidget::Selected() const -> bool {
        return d->selected;
    }

    void TCF2DWidget::SetSelected(bool selected) {
        if (false == d->selectable) {
            return;
        }

        d->imageWidget->setProperty("selected", selected);
        d->imageWidget->style()->unpolish(d->imageWidget);
        d->imageWidget->style()->polish(d->imageWidget);

        d->selected = selected;

        emit toggled(d->selected);
    }

    void TCF2DWidget::KeepSquare(bool keep) const {
        d->keepSquare = keep;
    }

    void TCF2DWidget::ZoomFit() const {
        d->imageWidget->SetZoomFit();
    }

    void TCF2DWidget::Clear() const {
        d->imageWidget->Clear();
        d->control->Reset();
    }

    void TCF2DWidget::paintEvent(QPaintEvent*) {
        if (true == d->keepSquare) {
            this->setFixedHeight(this->width());
            d->imageWidget->SetZoomFit();
            return;
        }

        const auto zoom = d->imageWidget->GetZoom();
        const auto zoomPoint = d->imageWidget->GetZoomPoint();

        d->imageWidget->SetZoomFit();
        d->imageWidget->SetZoom(zoomPoint, zoom);
    }


    /////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// SimpleTCF2DViewer /////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    struct SimpleTCF2DViewer::Impl {
        TCF2DWidget* tcfWidget{ nullptr };
        ImageTypeControlWidget* typeControlWidget{ nullptr };
        TimelapseControlWidget* timelapseControlWidget{ nullptr };

        bool typeControlVisible{ true };
        bool timelapseControlVisible{ true };

        bool isTextImage{ false };
        QString text;
    };

    SimpleTCF2DViewer::SimpleTCF2DViewer(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->tcfWidget = new TCF2DWidget;
        d->typeControlWidget = new ImageTypeControlWidget;
        d->timelapseControlWidget = new TimelapseControlWidget;

        auto modalityLayout = new QHBoxLayout;
        modalityLayout->addWidget(d->typeControlWidget);
        modalityLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
        modalityLayout->setContentsMargins(0, 0, 0, 0);

        auto toolLayout = new QVBoxLayout();
        toolLayout->addLayout(modalityLayout);
        toolLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        toolLayout->addWidget(d->timelapseControlWidget);
        toolLayout->setContentsMargins(0, 0, 10, 10);        

        auto viewLayout = new QVBoxLayout();
        viewLayout->addLayout(toolLayout);
        viewLayout->setContentsMargins(0, 0, 0, 0);
        d->tcfWidget->SetLayout(viewLayout);
        
        auto mainLayout = new QHBoxLayout();
        mainLayout->addWidget(d->tcfWidget);
        mainLayout->setContentsMargins(0, 0, 0, 0);
        this->setLayout(mainLayout);

        d->typeControlWidget->hide();
        d->timelapseControlWidget->hide();

        connect(d->typeControlWidget, SIGNAL(modalityButtonToggled(int)), this, SLOT(onImageTypeButtonToggled(int)));
        connect(d->timelapseControlWidget, SIGNAL(timelapseChanged(int)), this, SLOT(onTimelapseChanged(int)));
    }

    SimpleTCF2DViewer::~SimpleTCF2DViewer() = default;

    bool SimpleTCF2DViewer::SetTCFPath(const QString& path) const {
        if(nullptr == d->tcfWidget) {
            return false;
        }

        if(false == d->tcfWidget->SetTCFPath(path)) {
            return false;
        }        

        bool existHT = false;
        bool existFL = false;
        bool existBF = false;
        d->tcfWidget->GetUsableType(existHT, existFL, existBF);

        d->typeControlWidget->SetEnableButtons(existHT, existFL, existBF);

        if (true == existHT) {
            d->typeControlWidget->SetCurrentType(0);            
        }
        else if (true == existFL) {
            d->typeControlWidget->SetCurrentType(1);            
        }
        else if (true == existBF) {
            d->typeControlWidget->SetCurrentType(2);            
        }

        return true;
    }

    auto SimpleTCF2DViewer::GetTCFPath() const ->QString {
        if(nullptr == d->tcfWidget) {
            return QString();
        }

        return d->tcfWidget->GetTCFPath();
    }

    void SimpleTCF2DViewer::forceVisible() {
        if(nullptr != d->tcfWidget) {
            d->tcfWidget->SetVisible(true);
        }
    }


    bool SimpleTCF2DViewer::ShowImage(const TCF2DWidget::ImageType& type, const int& frame,bool late_load) const {
        if (nullptr == d->tcfWidget) {
            return false;
        }

        bool existHT = false;
        bool existFL = false;
        bool existBF = false;
        d->tcfWidget->GetUsableType(existHT, existFL, existBF);

        if (TCF2DWidget::ImageType::HT == type) {
            if(false == existHT) {
                this->ShowTextImage("No HT");
                return true;
            }
        }
        else if (TCF2DWidget::ImageType::FL == type) {
            if (false == existFL) {
                this->ShowTextImage("No FL");
                return true;
            }
        }
        else if (TCF2DWidget::ImageType::BF == type) {
            if (false == existBF) {
                this->ShowTextImage("No BF");
                return true;
            }
        }

        d->isTextImage = false;

        return d->tcfWidget->ShowImage(type, frame,late_load);
    }

    void SimpleTCF2DViewer::ShowTextImage(const QString& text) const {
        if(nullptr == d->tcfWidget) {
            return;
        }

        auto size = d->tcfWidget->size();
        QPixmap pixmap(size);
        pixmap.fill(Qt::transparent);

        QBrush brush;
        brush.setColor(QColor(255, 255, 255));

        QPainter painter(&pixmap);
        painter.setFont(QFont("Arial", 24));
        painter.setPen(QColor(255, 255, 255));
        painter.drawText(QRect(0, 0, size.width(), size.height()), Qt::AlignCenter, text);

        d->tcfWidget->SetPixmap(pixmap);

        d->isTextImage = true;
        d->text = text;
    }

    void SimpleTCF2DViewer::SetImageType(const TCF2DWidget::ImageType& type) const {
        if(nullptr == d->tcfWidget) {
            return;
        }

        auto result = this->ShowImage(type, 0);
        Q_UNUSED(result);
    }

    void SimpleTCF2DViewer::SetVisibleImageTypeControl(bool visible) const {
        d->typeControlVisible = visible;
        d->typeControlWidget->setVisible(visible);
    }

    void SimpleTCF2DViewer::SetVisibleTimelapseControl(bool visible) const {
        d->timelapseControlVisible = visible;
        d->timelapseControlWidget->setVisible(visible);
    }

    auto SimpleTCF2DViewer::GetSelectable() const ->bool {
        if (nullptr == d->tcfWidget) {
            return false;
        }

        return d->tcfWidget->GetSelectable();
    }

    void SimpleTCF2DViewer::SetSelectable(bool able) const {
        if (nullptr == d->tcfWidget) {
            return;
        }

        d->tcfWidget->SetSelectable(able);
    }

    auto SimpleTCF2DViewer::Selected() const -> bool {
        if (nullptr == d->tcfWidget) {
            return false;
        }

        return d->tcfWidget->Selected();
    }

    void SimpleTCF2DViewer::SetSelected(bool selected) {
        if (nullptr == d->tcfWidget) {
            return;
        }

        d->tcfWidget->SetSelected(selected);
    }

    void SimpleTCF2DViewer::KeepSquare(bool keep) const {
        if (nullptr == d->tcfWidget) {
            return;
        }

        d->tcfWidget->KeepSquare(keep);
    }

    void SimpleTCF2DViewer::ZoomFit() const {
        if (nullptr == d->tcfWidget) {
            return;
        }

        d->tcfWidget->ZoomFit();
    }

    void SimpleTCF2DViewer::Clear() const {
        if (nullptr == d->tcfWidget) {
            return;
        }

        d->tcfWidget->Clear();
    }

    void SimpleTCF2DViewer::onImageTypeButtonToggled(int index) const {
        auto type = static_cast<TCF2DWidget::ImageType>(index);

        this->ShowImage(type, 0);
        d->tcfWidget->ZoomFit();

        if(false == d->timelapseControlVisible) {
            return;
        }

        auto timeFrame = d->tcfWidget->GetTimeFrameCount(type);
       
        if(1 < timeFrame) {
            d->timelapseControlWidget->show();
            d->timelapseControlWidget->Init(timeFrame);
        }
        else {
            d->timelapseControlWidget->hide();
        }
    }

    void SimpleTCF2DViewer::onTimelapseChanged(int index) const {
        auto type = d->typeControlWidget->GetCurrentType();
        this->ShowImage(static_cast<TCF2DWidget::ImageType>(type), index);
    }

    void SimpleTCF2DViewer::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
        if(true == d->isTextImage) {
            this->ShowTextImage(d->text);
            return;
        }
    }
}
