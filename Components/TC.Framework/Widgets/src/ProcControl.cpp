
#include "ProcControl.h"

#include <iostream>
#include <QVBoxLayout>
#include <QLabel>
#include <QSpacerItem>
#include <QRadioButton>
#include <QApplication>
#include <QPushButton>
#include <QGridLayout>
#include <QScrollArea>
#include <QGroupBox>
#include <QJsonArray>

#include "GridSelection.h"
#include "CollapseWidget.h"

#include <ParameterList.h>
#include <ParameterRegistry.h>
#include <IProcessingAlgorithm.h>
#include <PluginRegistry.h>

#include <ParameterWriter.h>
#include "ParamControl.h"
#include "ParamTable.h"

namespace TC {
    typedef struct DefaultValue {
        QString dupName;
        QString valueName;
        QJsonValue value;
    }dValue;

    typedef struct SelectorValue {
        SelectorType type;
        int max_count;
        QStringList algoNames;
        QStringList dispNames;
    }selValue;

    struct ProcControl::Impl {
        QString proc_name;
        IParameter::Pointer param{ nullptr };
        ParameterList algo_list;

        IMetaParameter::Pointer metaParam{ nullptr };
        QStringList algo_order;
        QMap<QString, std::tuple<QString,QStringList>> connection_info;//sender , (receiver,valueName)
        QMap<QString, QStringList> duplicator;
        QMap<QString, QList<dValue>> defaultValues;
        QMap<QString, QList<dValue>> fixedValues;
        QMap<QString, selValue> selector;
        QStringList hider;
        
        QMap<QString, GridSelection*> algoSelector;
        QMap<QString, CollapseWidget*> algoContainer;
        QMap<QString, ParamControl*> algoControls;
        QMap<QString, ParamTable*> algoTables;

        QVBoxLayout* parentLayout{ nullptr };
        QVBoxLayout* layout{ nullptr };
        QWidget* scrollWidget{ nullptr };
        QScrollArea* scrollArea{ nullptr };
        QPushButton* executeWholeBtn{ nullptr };

        bool isHiddenExe{ false };
        bool isHiddenProcExe{ false };
        bool isDisable{ false };

        QStringList disList;
        QStringList realList;

        int calcHeight{ 0 };
        int curHeight{ INT_MAX };

        double procParamVersion{ 0.0 };

        QMap<QString, QString> highlight_rule;
    };
    ProcControl::ProcControl(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->scrollWidget = new QWidget;
        d->layout = new QVBoxLayout;
        d->layout->setContentsMargins(0, 0, 0, 0);
        d->layout->setSpacing(7);
        d->layout->setAlignment(Qt::AlignTop);

        d->parentLayout = new QVBoxLayout;
        d->parentLayout->setContentsMargins(14, 0, 6, 0);
        d->parentLayout->setSpacing(14);

        setLayout(d->parentLayout);

        d->scrollArea = new QScrollArea(parent);
        d->scrollArea->setWidgetResizable(true);
        d->scrollArea->setWidget(d->scrollWidget);
        d->parentLayout->addWidget(d->scrollArea);

        d->scrollWidget->setLayout(d->layout);

        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        setObjectName("panel-contents");
    }
    ProcControl::~ProcControl() {
        if(nullptr != d->executeWholeBtn) {
            delete d->executeWholeBtn;
        }
        for(auto wid : d->algoControls) {
            delete wid;
        }
        for(auto table : d->algoTables) {
            delete table;
        }
        for(auto cont : d->algoContainer) {
            delete cont;
        }
        delete d->layout;
        delete d->scrollWidget;
        delete d->scrollArea;
        delete d->parentLayout;
    }
    auto ProcControl::GetConnections() -> QMap<QString, std::tuple<QString, QStringList>>& {
        return d->connection_info;
    }

    auto ProcControl::SetCompetableMinSize(int size) -> void {
        if(d->calcHeight>size) {
            setMinimumHeight(size);
            d->curHeight = size;
        }else {
            setMinimumHeight(d->calcHeight);
        }
    }
    auto ProcControl::ClearInterface() -> void {        
        QLayoutItem* item;
        while((item = d->layout->takeAt(0))) {
            if(item->widget()) {
                delete item->widget();
            }
            delete item;
        }
        d->algo_list.Clear();
        d->algoContainer.clear();
        d->algoControls.clear();
        d->algoTables.clear();
        d->algoSelector.clear();
        //d->param = nullptr;
        //d->metaParam = nullptr;
        d->algo_order.clear();
        d->connection_info.clear();
        d->highlight_rule.clear();
        d->duplicator.clear();
        d->defaultValues.clear();
        d->fixedValues.clear();
    }

    auto ProcControl::BuildInterface() -> void {
        ClearInterface();
        ParseProcessingParameter();
        ParseMetaParameter();
        LoadRequiredAlgorithms();
        ArrangeProcessingAlgorithm();
    }

    auto ProcControl::ParseProcessingParameter()->void {
        if(nullptr == d->param) {
            return;
        }
        auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };
        
        auto nameNtypes = d->param->GetNameAndTypes();
        for(const auto& nnt:nameNtypes) {
            auto node = param2node(d->param, std::get<0>(nnt));
            if(node.description == "DoNotMakeInterface") {
                continue;
            }
            d->algo_list.AppendNode(node);            
        }
    }

    auto ProcControl::ParseMetaParameter()->void {
        if(nullptr == d->metaParam) {
            return;
        }        

        //parse algorithm order
        auto sortNames = d->metaParam->GetSorterNames();
        if(sortNames.count() ==0) {
            return;
        }
        d->algo_order = d->metaParam->GetSorter(sortNames[0]);

        //parse connection between algorithms
        auto connectNames = d->metaParam->GetConnecterNames();
        for(auto coName : connectNames) {
            auto connector = d->metaParam->GetConnector(coName);
            auto sender = std::get<0>(connector);
            auto receiver = std::get<1>(connector);
            auto valueNames = std::get<2>(connector);
            
            d->connection_info[sender] = std::make_tuple(receiver, valueNames);
        }

        //parse highlight information
        auto highlightNames = d->metaParam->GetHighlighterNames();
        for(auto hiName : highlightNames) {
            auto highlighter = d->metaParam->GetHighlighter(hiName);            
            auto sender = std::get<0>(highlighter);
            auto receiver = std::get<1>(highlighter);
            d->highlight_rule[sender] = receiver;
        }

        //parse duplication info
        auto duplicationNames = d->metaParam->GetDuplicatorNames();
        for(auto dupKey : duplicationNames) {
            auto duplicator = d->metaParam->GetDuplicator(dupKey);
            auto dupNames = std::get<0>(duplicator);            
            auto dupAlgoName = std::get<1>(duplicator);            
            d->duplicator[dupAlgoName] = dupNames;
        }

        //parse default value setting
        auto defaultNames = d->metaParam->GetDefaultValueNames();
        for(auto deKey : defaultNames) {
            auto defaulter = d->metaParam->GetDefaultValue(deKey);
            auto dupName = std::get<0>(defaulter);
            auto dupAlgoName = std::get<1>(defaulter);
            auto valueName = std::get<2>(defaulter);
            auto value = std::get<3>(defaulter);

            dValue info;
            info.dupName = dupName;
            info.valueName = valueName;
            info.value = value;
            if(false == d->defaultValues.contains(dupAlgoName)) {
                d->defaultValues[dupAlgoName] = QList<dValue>();
            }
            d->defaultValues[dupAlgoName].append(info);
        }

        auto hideAlgos = d->metaParam->GetHiderNames();
        for(auto hiKey : hideAlgos) {
            auto hider = d->metaParam->GetHider(hiKey);
            d->hider = hider;
        }

        //parse fixed value setting
        auto fixedNames = d->metaParam->GetFixerNames();
        for(auto fKey : fixedNames) {
            auto fixer = d->metaParam->GetFixer(fKey);
            auto dupName = std::get<0>(fixer);
            auto dupAlgoName = std::get<1>(fixer);
            auto valueName = std::get<2>(fixer);
            auto value = std::get<3>(fixer);

            dValue info;
            info.dupName = dupName;
            info.valueName = valueName;
            info.value = value;
            if(false == d->fixedValues.contains(dupAlgoName)) {
                d->fixedValues[dupAlgoName] = QList<dValue>();
            }
            d->fixedValues[dupAlgoName].append(info);
        }

        //parse selector value setting
        auto selectNames = d->metaParam->GetSelectorNames();
        for(auto seKey : selectNames) {
            auto selector = d->metaParam->GetSelector(seKey);
            auto targetNodeName = std::get<0>(selector);
            auto selectorType = std::get<1>(selector);
            auto maxCount = std::get<2>(selector);
            auto selectList = std::get<3>(selector);
            auto dispList = std::get<4>(selector);

            selValue info;
            info.type = static_cast<SelectorType>(selectorType);
            info.max_count = maxCount;
            info.algoNames = selectList;
            info.dispNames = dispList;
            d->selector[targetNodeName] = info;
        }
    }

    auto ProcControl::LoadRequiredAlgorithms() -> void {
        auto names = d->algo_list.GetNames();        
        
        for(auto i=0;i<names.count();i++) {
            if (d->selector.contains(names[i])) {
                //get select parameter
                auto sparam = d->param->GetChild(names[i]);
                auto sel = d->selector[names[i]];
                auto select = new GridSelection(nullptr);
                select->SetSelectorName(names[i]);
                select->SetDisplayNames(sel.dispNames);
                select->SetAlgorithmNames(sel.algoNames);
                select->SetType(sel.type);
                select->SetMaximumItemInRow(sel.max_count);
                select->BuildInterface();

                auto initial_state = d->param->GetValue(names[i]).toArray();
                for(auto st = 0;st<initial_state.count();st++) {
                    auto state = initial_state[st].toBool();
                    select->SetSelectionState(st, state);                    
                }

                connect(select, SIGNAL(sigSelection(QString, bool)), this, SLOT(OnAlgorithmToggle(QString, bool)));

                d->algoSelector[names[i]] = select;                        
            }
        }
        for(auto i=0;i<names.count();i++) {            
            auto node = d->algo_list.GetNode(names[i]);            
            auto plugin = PluginRegistry::GetPlugin(node.value.toString());            
            if(nullptr == plugin) {
                continue;
            }
            auto param = plugin->Parameter();
            auto procModule = std::dynamic_pointer_cast<IProcessingAlgorithm>(plugin);
            auto uiParam = procModule->UiParameter();

            QStringList dupList;
            //if(d->duplicator.contains(node.value.toString())) {
            if (d->duplicator.contains(names[i])) {
                //dupList = d->duplicator[node.value.toString()];
                dupList = d->duplicator[names[i]];
                for (auto dup : dupList) {
                    auto childFullName = node.name + " Parameter!" + dup;
                    auto child = std::shared_ptr<IParameter>(param->Clone());
                    d->param->SetChild(childFullName, child);
                }                
            }else {
                d->param->SetChild(node.name + " Parameter", param);
            }

            
            //set default values
            //if(d->defaultValues.contains(node.value.toString())) {
            if (d->defaultValues.contains(names[i])) {
                //auto defaulter = d->defaultValues[node.value.toString()];
                auto defaulter = d->defaultValues[names[i]];
                for(auto default_info: defaulter) {
                    auto targetName = default_info.dupName;                    
                    auto valueName = default_info.valueName;
                    auto val = default_info.value;
                    auto childFullName = node.name + " Parameter";
                    if(false == targetName.isEmpty()) {                        
                        childFullName += "!";
                        childFullName += targetName;                        
                    }
                    auto child = d->param->GetChild(childFullName);
                    child->SetValue(valueName, val);
                }
            }

            //auto hasFixer = d->fixedValues.contains(node.value.toString());
            auto hasFixer = d->fixedValues.contains(names[i]);

            if(hasFixer) {
                //auto fixer = d->fixedValues[node.value.toString()];
                auto fixer = d->fixedValues[names[i]];
                for (auto fix_info : fixer) {
                    auto targetName = fix_info.dupName;
                    auto valueName = fix_info.valueName;
                    auto val = fix_info.value;
                    auto childFullName = node.name + " Parameter";
                    if (false == targetName.isEmpty()) {                        
                        childFullName += "!";
                        childFullName +=targetName;                        
                    }
                    auto child = d->param->GetChild(childFullName);
                    child->SetValue(valueName, val);
                }
            }

            //Build UI
            if(uiParam->GetType()!="Default") {
                auto type = uiParam->GetType();
                if (type == "Table") {
                    if (dupList.size() < 1) {
                        dupList.push_back("default");
                    }

                    auto table = new ParamTable(nullptr);                    
                    table->SetName(node.name);
                    table->SetAlgoName(plugin->GetFullName());
                    table->SetDisable(d->isDisable);
                    table->SetParameter(param);
                    table->SetUiParameter(uiParam);
                    table->SetHideExecute(d->isHiddenProcExe);
                    table->SetDuplicator(dupList);
                    table->SetTextConverter(d->disList, d->realList);
                    table->BuildInterface();

                    //d->algoTables[plugin->GetFullName()] = table;
                    d->algoTables[names[i]] = table;
                    if (d->param->GetChildrenNames().count() > 0) {
                        for (auto chName : d->param->GetChildrenNames()) {
                            if (chName.contains("!")) {//table is always duplicated
                                table->SetParameterValue(d->param->GetChild(chName), chName.split("!")[1]);
                            }else{
                                table->SetParameterValue(param);
                            }
                        }
                    }
                    else {
                        table->SetParameterValue(d->param);
                    }

                    connect(table, SIGNAL(comboChanged(int, QString, QString, QString,QString)), this, SLOT(OnTableComboCall(int, QString, QString, QString,QString)));
                    connect(table, SIGNAL(valChanged(double, QString, QString, QString,QString)), this, SLOT(OnTableChangeCall(double, QString, QString, QString,QString)));
                    connect(table, SIGNAL(execute(QString,QString)), this, SLOT(OnParamaterExecuteCall(QString,QString)));
                    connect(table, SIGNAL(naiveValChanged(double, QString,QString,QString)), this, SLOT(OnTableValueCall(double,QString,QString,QString)));
                    connect(table, SIGNAL(enableChanged(bool, QString,QString,QString)), this, SLOT(OnTableEnableCall(bool,QString,QString,QString)));
                }
            }else {
                auto control = new ParamControl(nullptr);
                control->SetDiable(d->isDisable);                
                control->SetName(node.name);
                control->SetAlgoName(plugin->GetFullName());
                control->SetParameter(param);
                control->SetUiParameter(uiParam);
                control->SetHideExecute(d->isHiddenProcExe);
                control->BuildInterface();
                //d->algoControls[plugin->GetFullName()] = control;
                d->algoControls[names[i]] = control;
                                
                connect(control, SIGNAL(pathChanged(QString, QString,QString,QString)), this, SLOT(OnPathChangeCall(QString, QString,QString,QString)));
                connect(control, SIGNAL(valChanged(double, QString,QString,QString)), this, SLOT(OnParameterChangeCall(double, QString,QString,QString)));
                connect(control, SIGNAL(execute(QString,QString)), this, SLOT(OnParamaterExecuteCall(QString,QString)));
                connect(control, SIGNAL(checkChanged(bool, QString,QString,QString)), this, SLOT(OnCheckChangeCall(bool, QString,QString,QString)));
                connect(control, SIGNAL(comboChanged(int, QString,QString,QString)), this, SLOT(OnComboChangeCall(int, QString,QString,QString)));

                if (hasFixer) {
                    //auto fixer = d->fixedValues[node.value.toString()];
                    auto fixer = d->fixedValues[names[i]];
                    for (auto fix_info : fixer) {                        
                        auto valueName = fix_info.valueName;
                        control->SetFixedControl(valueName);
                    }
                }
            }
        }        
    }    

    auto ProcControl::SetProcessingParameter(IParameter::Pointer param) -> void {        
        d->param = param;
        auto versionText = d->param->GetVersion();

        auto versionToDouble = [](QString vText)->double {
            if (vText.isEmpty()) {
                return 0.0;
            }
            auto sp = vText.split(".");
            auto major = sp[0].toDouble(); auto majorlen = sp[0].length();
            auto minor = sp[1].toDouble(); auto minorlen = sp[1].length();
            auto patch = sp[2].toDouble(); auto pathlen = sp[2].length();

            auto result = 0.0;

            for (auto i = 0; i < majorlen; i++) {
                major /= 10.0;
            }
            result += major;
            for (auto i = 0; i < minorlen + majorlen; i++) {
                minor /= 10.0;
            }
            result += minor;
            for (auto i = 0; i < pathlen + minorlen + majorlen; i++) {
                patch /= 10.0;
            }
            result += patch;
            return result;
        };
        d->procParamVersion = versionToDouble(versionText);
    }

    auto ProcControl::SetMetaParameter(IMetaParameter::Pointer meta) -> void {
        d->metaParam = meta;
    }

    auto ProcControl::SetConverter(QStringList disList, QStringList realList) -> void {
        d->disList = disList;
        d->realList = realList;
    }

    auto ProcControl::SetHideExecute(bool isHide) -> void {
        d->isHiddenExe = isHide;
    }

    auto ProcControl::SetHideParamExecute(bool isHide) -> void {
        d->isHiddenProcExe = isHide;
    }

    auto ProcControl::SetDisableControl(bool disable) -> void {
        d->isDisable = disable;
    }

    auto ProcControl::ClearHighlights() -> void {
        for(auto al:d->algoControls) {
            al->SetHighLight(false);
        }
        for(auto ta : d->algoTables) {
            ta->SetHighlight(false);
        }
    }

    auto ProcControl::SetDefaultHighlight() -> void {
        auto startName = d->highlight_rule["Default"];        
        if(false == startName.isEmpty()) {
            if(d->algoControls.contains(startName)) {
                d->algoControls[startName]->SetHighLight(true);
            }else if(d->algoTables.contains(startName)) {
                d->algoTables[startName]->SetHighlight(true);
            }
        }
    }
    auto ProcControl::SetProcHighlight(bool isHi, QString algo_name) -> void {
        if(false == algo_name.isEmpty()) {
            if(d->algoControls.contains(algo_name)) {
                d->algoControls[algo_name]->SetHighLight(isHi);
                emit highlightApply(false);
            }else if(d->algoTables.contains(algo_name)) {
                d->algoTables[algo_name]->SetHighlight(isHi);
                emit highlightApply(false);
            }
        }
    }

    auto ProcControl::SetWholeHighlight(bool isHi) -> void {
        if(isHi) {
            d->executeWholeBtn->setStyleSheet("background-color: rgb(91,139,151);");
        }else {
            d->executeWholeBtn->setStyleSheet("");
        }
    }
    auto ProcControl::GetMetaParameter() -> IMetaParameter::Pointer {
        if(nullptr == d->metaParam) {
            return nullptr;
        }
        return std::shared_ptr<IMetaParameter>(d->metaParam->Clone());
    }
    auto ProcControl::GetParameter() -> IParameter::Pointer {
        if(nullptr == d->param) {            
            return nullptr;
        }
        return std::shared_ptr<IParameter>(d->param->Clone());
    }
    auto ProcControl::GetParameter(QString param_name,QString dupName) -> IParameter::Pointer {
        //auto plugin = PluginRegistry::GetPlugin(algo_name);
        if (dupName.isEmpty()) {
            //auto param = d->param->GetChild(plugin->GetName() + " Parameter");
            auto param = d->param->GetChild(param_name + " Parameter");
            return param;
        }
        auto dup_param = d->param->GetChild(param_name + " Parameter!" + dupName);
        return dup_param;
    }
    auto ProcControl::SetParameterValue(IParameter::Pointer param, QString algo_name, QString organ_name) -> void {
        auto isOld = param->GetVersion().isEmpty();
        d->algoTables[algo_name]->SetParameterValue(param, organ_name,true,isOld);
    }
    auto ProcControl::SetParameterValue(IParameter::Pointer param, QString algo_name, bool blockSig) -> void {
        if(nullptr == d->param) {            
            return;
        }
        d->param = param;
        auto version_string = param->GetVersion();
        auto isOld = version_string.isEmpty();        
        if(algo_name.isEmpty()) {
            auto childNames = param->GetChildrenNames();
            for(auto ch : childNames) {                
                d->param->SetChild(ch, param->GetChild(ch));
                if(ch.split("!").count()>1) {
                    auto al = ch.split("!")[0];
                    auto organ = ch.split("!")[1];
                    auto alName = al.chopped(10);                    
                    if(d->algoTables.contains(alName)) {                        
                        d->algoTables[alName]->SetParameterValue(param->GetChild(ch), organ, blockSig,isOld);
                    }
                }else {
                    auto alName = ch.chopped(10);                    
                    if(d->algoControls.contains(alName)) {                        
                        d->algoControls[alName]->SetParameterValue(param->GetChild(ch), blockSig,isOld);
                    }else if(d->algoTables.contains(alName)) {
                        d->algoTables[alName]->SetParameterValue(param->GetChild(ch), "default", blockSig,isOld);
                    }
                }
            }
            //for selector value
            auto names = param->GetNames();
            for(auto name:names) {
                if(d->algoSelector.contains(name)) {                    
                    auto val = param->GetValue(name).toArray();
                    for(auto v=0;v<val.count();v++) {                        
                        d->algoSelector[name]->SetSelectionState(v, val[v].toBool());
                    }                    
                }
            }
        }else {
            d->algoControls[algo_name]->SetParameterValue(param, blockSig,isOld);
        }
    }

    auto ProcControl::ArrangeProcessingAlgorithm() -> void {
        d->executeWholeBtn = nullptr;
        auto size = 0;
        //arrange                

        for(auto i=0;i<d->algo_order.count();i++) {
            auto algo_name = d->algo_order[i];
            auto procName = d->param->GetValue(algo_name).toString();
            //auto plugin = PluginRegistry::GetPlugin(algo_name);
            auto plugin = PluginRegistry::GetPlugin(procName);
            //auto algo = d->algo_list.GetNode(plugin->GetName());
            auto algo = d->algo_list.GetNode(algo_name);
            auto tmpWidget = new CollapseWidget;

            //d->algoContainer.push_back(tmpWidget);
            d->algoContainer[algo_name] = tmpWidget;
            if(d->algoSelector.contains(algo_name)) {
                tmpWidget->SetCollapsable(false);
                tmpWidget->SetWidget(d->algoSelector[algo_name], algo.displayName);
            }else if(d->algoControls.contains(algo_name)) {                
                tmpWidget->SetWidget(d->algoControls[algo_name], algo.name);                
            }else if(d->algoTables.contains(algo_name)) {                
                tmpWidget->SetWidget(d->algoTables[algo_name], algo.name);                
            }            
            d->layout->addWidget(tmpWidget);            
            tmpWidget->show();            
            tmpWidget->adjustSize();            
            if(size > 0) {
                size += 7;
            }
            size += tmpWidget->height();            
        }

        if(!d->isHiddenExe) {        
            d->executeWholeBtn = new QPushButton;
            d->executeWholeBtn->setText("Execute Whole");
            d->layout->addWidget(d->executeWholeBtn);
            d->executeWholeBtn->show();
            size += 7;
            size += d->executeWholeBtn->height();
            connect(d->executeWholeBtn, SIGNAL(clicked()), this, SLOT(OnProcessorExecuteCall()));            
        }
        d->calcHeight = size;
        if (d->curHeight > d->calcHeight) {            
            setMinimumHeight(size);
        }
        else {            
            setMinimumHeight(d->curHeight);
        }

        for(auto hi : d->hider) {
            if(d->algoContainer.contains(hi)) {
                d->algoContainer[hi]->hide();                
            }
        }
        for(auto sel : d->algoSelector) {
            auto states = sel->GetCurrentStatus();
            auto algo_names = sel->GetAlgorithmNames();
            for(auto sidx =0;sidx<states.count();sidx++) {
                if(d->algoContainer.contains(algo_names[sidx])) {
                    d->algoContainer[algo_names[sidx]]->setHidden(!states[sidx]);
                }
            }
        }        
    }


    //SLOTS
    void ProcControl::OnAlgorithmToggle(QString algorithm_name, bool state) {
        auto selector = qobject_cast<GridSelection*>(sender());
        auto sname = selector->GetSelectorName();
        auto algo_names =  selector->GetAlgorithmNames();
        auto selections = d->param->GetValue(sname).toArray();
        for(auto i=0;i<algo_names.count();i++) {
            if(algo_names[i] == algorithm_name) {
                selections[i] = state;
            }
        }
        d->param->SetValue(sname, selections);
        if(d->algoContainer.contains(algorithm_name)) {
            d->algoContainer[algorithm_name]->setHidden(!state);            
        }
    }

    void ProcControl::OnTableComboCall(int idx, QString node_name, QString organ_name, QString param_name,QString algo_name) {     
        auto nn = d->param->GetChildrenNames();
        if (d->procParamVersion < 0.11) {            
            auto s = param_name;
            auto key = node_name;
            if (d->duplicator.contains(s)) {
                auto plugin = PluginRegistry::GetPlugin(s);
                auto childName = plugin->GetName() + " Parameter!" + organ_name;
                auto child = d->param->GetChild(childName);                
                child->SetValue(key, idx);
            }
            else {
                for (auto n : nn) {
                    auto nodeName = n.chopped(10);
                    auto des = d->param->GetValue(nodeName).toString();
                    if (s.compare(des) == 0) {
                        auto child = d->param->GetChild(n);                        
                        child->SetValue(key, idx);
                    }
                }
            }
        }else {
            if(d->duplicator.contains(param_name)) {
                auto childName = param_name + " Parameter!" + organ_name;
                auto child = d->param->GetChild(childName);                
                child->SetValue(node_name, idx);
            }else {
                for (auto n : nn) {
                    auto nodeName = n.chopped(10);                    
                    if (param_name.compare(nodeName) == 0) {
                        auto child = d->param->GetChild(n);                        
                        child->SetValue(node_name, idx);
                    }
                }
            }
        }
    }

    void ProcControl::OnTableValueCall(double value, QString node_name,QString param_name,QString organ_name) {
        auto plugin = PluginRegistry::GetPlugin(param_name);
        //auto subKey = plugin->GetFullName();
        auto childName = plugin->GetName();        
        if(d->duplicator.contains(childName)) {
            auto childFullName = childName + " Parameter!" + organ_name;
            auto child = d->param->GetChild(childFullName);
            child->SetValue(node_name, value);
        }else {
            for(auto n: d->param->GetChildrenNames()) {                
                auto nodeName = n.chopped(10);
                auto des = d->param->GetValue(nodeName).toString();                
                if(param_name == des) {
                    auto child = d->param->GetChild(n);
                    child->SetValue(node_name, value);
                }
            }
        }
    }

    void ProcControl::OnTableEnableCall(bool value, QString node_name,QString param_name,QString organ_name) {
        auto plugin = PluginRegistry::GetPlugin(param_name);
        auto subKey = plugin->GetFullName();
        auto childName = plugin->GetName();        
        if (d->duplicator.contains(childName)) {            
            auto childFullName = childName + " Parameter!" + organ_name;
            auto child = d->param->GetChild(childFullName);            
            child->SetValue(node_name, value);
        }
        else {
            for (auto n : d->param->GetChildrenNames()) {
                auto nodeName = n.chopped(10);
                auto des = d->param->GetValue(nodeName).toString();
                if (param_name == des) {
                    auto child = d->param->GetChild(n);
                    child->SetValue(node_name, value);
                }
            }
        }
    }

    void ProcControl::OnTableChangeCall(double value, QString organ_name, QString node_name, QString param_name,QString algo_name) {        
        auto nn = d->param->GetChildrenNames();
        if (d->procParamVersion < 0.11) {
            auto s = param_name;
            auto key = node_name;
            if (d->duplicator.contains(s)) {
                auto plugin = PluginRegistry::GetPlugin(s);
                auto childName = plugin->GetName() + " Parameter!" + organ_name;
                auto child = d->param->GetChild(childName);                
                child->SetValue(key, value);                
            }
            else {
                for (auto n : nn) {
                    auto nodeName = n.chopped(10);
                    auto des = d->param->GetValue(nodeName).toString();
                    if (s.compare(des) == 0) {
                        auto child = d->param->GetChild(n);                        
                        child->SetValue(key, value);                        
                    }
                }
            }
            emit valueCall(value, param_name + "!" + node_name);
        }else {
            if (d->duplicator.contains(param_name)) {
                auto childName = param_name + " Parameter!" + organ_name;
                auto child = d->param->GetChild(childName);                
                child->SetValue(node_name, value);                
            }
            else {
                for (auto n : nn) {
                    auto nodeName = n.chopped(10);
                    if (param_name.compare(nodeName) == 0) {
                        auto child = d->param->GetChild(n);                        
                        child->SetValue(node_name, value);                       
                    }
                }
            }
        }
    }
    void ProcControl::OnParamaterExecuteCall(QString param_name,QString algo_name) {        
        emit executeCall(param_name,algo_name);
        /*if (d->highlight_rule.contains(param_name)) {
            auto target = d->highlight_rule[param_name];
            if (target == "Apply Parameter") {
                emit highlightApply(true);
            }
            else {
                SetProcHighlight(true, target);
            }
        }*/
    }

    auto ProcControl::SetNextHighlight(QString algo_name) -> void {        
        ClearHighlights();
        if(d->highlight_rule.contains(algo_name)) {
            auto target = d->highlight_rule[algo_name];
            if (target == "Apply Parameter") {
                emit highlightApply(true);
            }
            else {
                SetProcHighlight(true, target);
            }
        }
    }

    void ProcControl::OnPathChangeCall(QString path, QString path_name,QString param_name,QString algo_name) {
        //TODO
        //Do nothing for now
        Q_UNUSED(path)
        Q_UNUSED(path_name)
        Q_UNUSED(param_name)
        Q_UNUSED(algo_name)
    }
    void ProcControl::OnParameterChangeCall(double val, QString value_name,QString param_name,QString algo_name) {
        Q_UNUSED(algo_name);
        auto nn = d->param->GetChildrenNames();        
        auto key = value_name;
                
        auto child = d->param->GetChild(param_name + " Parameter");
        child->SetValue(key, val);
                
        emit valueCall(val,param_name);//Not used
    }
    void ProcControl::OnCheckChangeCall(bool state, QString state_name,QString param_name,QString algo_name) {
        Q_UNUSED(algo_name)
        auto child = d->param->GetChild(param_name + " Parameter");
        child->SetValue(state_name, state);                
        if (d->algoControls.contains(param_name)) {
            ClearHighlights();
            if (state) {
                //d->algoControls[s]->highlightBtn(true);
                d->algoControls[param_name]->SetHighLight(true);
                emit highlightApply(false);
            }
            else {
                emit refreshHighlight();
            }
        }
        else if (d->algoTables.contains(algo_name)) {
            ClearHighlights();
            if (state) {
                //d->algoControls[s]->highlightBtn(true);
                d->algoControls[param_name]->SetHighLight(true);
                emit highlightApply(false);
            }
            else {
                emit refreshHighlight();
            }
        }
    }
    void ProcControl::OnComboChangeCall(int idx, QString idx_name,QString param_name,QString algo_name) {
        Q_UNUSED(algo_name)
        auto child = d->param->GetChild(param_name + " Parameter");
        child->SetValue(idx_name, idx);                
    }
    void ProcControl::OnProcessorExecuteCall() {
        emit executeWhole();
    }

}