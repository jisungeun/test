#define LOGGER_TAG "[ParamTable]"
#include "ParamTable.h"

#include <TCLogger.h>

#include <iostream>
#include <QComboBox>
#include <QHeaderView>
#include <QPushButton>
#include <qstyleditemdelegate.h>
#include <QTableWidgetItem>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QItemDelegate>
#include <QLineEdit>
#include <QLabel>

#include <ScalarInput.h>
#include <StrComboBox.h>

#include "AdaptiveHeader.h"

#include "ParamConverter.h"
#include "ParameterTable.h"

namespace TC {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
            const QModelIndex& index) const override {
            Q_UNUSED(option)
            Q_UNUSED(index)
            QLineEdit* lineEdit = new QLineEdit(parent);
            QIntValidator* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };
    class DoubleDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
            const QModelIndex& index) const override {
            Q_UNUSED(option)
            Q_UNUSED(index)
            QLineEdit* lineEdit = new QLineEdit(parent);
            QDoubleValidator* validator = new QDoubleValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };
    struct ParamTable::Impl {
        IParameter::Pointer param{nullptr};
        IUiParameter::Pointer uiParam{ nullptr };
        QMap<QString, IParameter::Pointer> dupParam; // duplicated param
        QMap<QString, IUiParameter::Pointer> dupUiParam; // duplicated Ui Param
        QStringList dupList;
        QList<bool> dupReadOnly;
        QString param_name;
        QString algo_name;
        QList<QString> node_name;
        QStringList real_table_header;

        bool tableAdded{ false };
        QMap<QString, QLabel*> labels;

        QVBoxLayout* layout{ nullptr };
        QTableWidget* table{ nullptr };
        QMap<QString, QList<QComboBox*>> combos;
        QPushButton* executeBtn{ nullptr };

        ParameterList list;
        TMap type_list;

        QMap<QString, EnablerNode> enablers;
        QMap<QString, SetterNode> setters;        
        ScalarMap scalars;
        CheckMap checks;
        QStringList table_item;
        QStringList node_order;
        QStringList hide_list;
        //no bounder for now Jose T. Kim 22.01.13

        bool disable{ false };
        bool hideExecute{ false };
        
        QStringList displayNames;
        QStringList realNames;
    };
    ParamTable::ParamTable(QWidget* parent ) : QWidget(parent),d{new Impl} {
        d->layout = new QVBoxLayout(this);
        d->layout->setContentsMargins(0, 0, 0, 0);

        setLayout(d->layout);
        d->table = new QTableWidget;
        d->table->setShowGrid(true);
        d->table->setSizeAdjustPolicy(QTableWidget::SizeAdjustPolicy::AdjustToContents);
        d->table->setProperty("styleVariant", 1);
        auto horiheader = new AdaptiveHeader(Qt::Orientation::Horizontal);
        d->table->setHorizontalHeader(horiheader);
        auto verheader = new AdaptiveHeader(Qt::Orientation::Vertical);
        d->table->setVerticalHeader(verheader);
        //d->layout->addWidget(d->table);
        setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);

        connect(d->table, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(OnTableItemChanged(QTableWidgetItem*)));
    }
    ParamTable::~ParamTable() {
        disconnect();
    }
    auto ParamTable::toDisplayName(QString name) -> QString {
        for (auto i = 0; i < d->realNames.size(); i++) {
            if (name.compare(d->realNames[i]) == 0) {
                return d->displayNames[i];
            }
        }
        return name;
    }
    auto ParamTable::toRealName(QString name) -> QString {
        for (auto i = 0; i < d->displayNames.size(); i++) {
            if (name.compare(d->displayNames[i]) == 0) {
                return d->realNames[i];
            }
        }
        return name;
    }

    auto ParamTable::SetName(const QString& name) -> void {
        d->param_name = name;
    }

    auto ParamTable::SetAlgoName(const QString& name) -> void {
        d->algo_name = name;
    }

    auto ParamTable::SetHideExecute(bool isHide) -> void {
        d->hideExecute = isHide;
    }

    auto ParamTable::SetDisable(bool dis) -> void {
        d->disable = dis;
    }

    auto ParamTable::GetParameter(QString key) -> IParameter::Pointer {
        if(d->dupParam.contains(key)) {
            return d->dupParam[key];
        }
        return nullptr;
    }

    auto ParamTable::GetScalarValue(QString name, QString key) -> double {
        if(d->dupParam.contains(key)) {
            auto val = d->dupParam[key]->GetValue(name).toDouble();
            return val;
        }
        return 0.0;
    }

    auto ParamTable::SetScalarValue(QString name, double val, QString key) -> void {
        if(d->dupParam.contains(key)) {
            if(d->dupParam[key]->ExistNode(name)) {
                d->dupParam[key]->SetValue(name, val);
            }
        }
    }

    auto ParamTable::SetTextConverter(QStringList disList, QStringList realList) -> void {
        d->displayNames = disList;
        d->realNames = realList;
    }

    auto ParamTable::SetDuplicator(QStringList dupList) -> void {
        d->dupList = dupList;
        d->dupReadOnly.clear();
        for(auto dupName: dupList) {
            d->dupReadOnly.push_back(true);
            d->dupParam[dupName] = std::shared_ptr<IParameter>(d->param->Clone());
        }
    }

    auto ParamTable::SetParameter(IParameter::Pointer param) -> void {
        d->param = param;
    }

    auto ParamTable::SetUiParameter(IUiParameter::Pointer uiParam) -> void {
        d->uiParam = uiParam;
    }

    auto ParamTable::SetHighlight(bool isHi) -> void {
        if(nullptr!= d->executeBtn) {
            if(isHi) {
                d->executeBtn->setStyleSheet("background-color: rgb(91,139,151);");
            }else {
                d->executeBtn->setStyleSheet("");
            }
        }
    }

    auto ParamTable::BuildInterface() -> void {
        d->table->clearContents();
        d->real_table_header.clear();
        d->table->setColumnCount(d->dupList.count());
        d->combos.clear();
        d->executeBtn = nullptr;

        ParseParameter();
        ParseUiParameter();

        QStringList verHeader;
        int row_cnt = 0;
        //Table Creation based on parsed UI and parameter
        //for (auto i = 0; i < d->node_name.count();i++) {
        for(auto i=0;i<d->node_order.count();i++){
            if(false == d->table_item.contains(d->node_order[i])) {                
                continue;
            }
            auto node = d->list.GetNode(d->node_order[i]);
            auto type = node.unit;
            if(type.contains("FilePath")) {
                QLOG_INFO() << "Table do not provide path parameter ";
                continue;
            }
            if(type.contains("ScalarValue")) {
                auto scalarType = d->type_list[d->node_order[i]];
                if(scalarType == QJsonValue::Double) {
                    auto unit = node.unit;
                    auto typeString = unit.split(".")[1];
                    if(typeString.compare("int")==0) {
                        row_cnt++;
                        d->real_table_header.push_back(node.name);
                        verHeader.push_back(node.displayName);
                    }else if(typeString.compare("double")==0) {
                        row_cnt++;
                        d->real_table_header.push_back(node.name);
                        verHeader.push_back(node.displayName);
                    }
                }
            }else if(type.contains("SelectOption")){
                row_cnt++;
                d->real_table_header.push_back(node.name);
                verHeader.push_back(node.displayName);
            }
        }
        d->table->setRowCount(row_cnt);

        QStringList hoHeader;
        for(auto dup : d->dupList) {
            hoHeader.push_back(toDisplayName(dup));
        }
        d->table->setHorizontalHeaderLabels(hoHeader);
        d->table->setVerticalHeaderLabels(verHeader);

        d->table->resizeColumnsToContents();
        d->table->resizeRowsToContents();

        //build actual UIs
        row_cnt = 0;
        for(auto i=0;i<d->node_order.count();i++) {
            if (false == d->table_item.contains(d->node_order[i])) {
                AddNaiveControl(d->node_order[i]);
                continue;
            }
            if(false == d->tableAdded) {
                d->layout->addWidget(d->table);
                d->tableAdded = true;
            }
            auto node = d->list.GetNode(d->node_order[i]);
            auto type = node.unit;            
            if(type.contains("FilePath")) {
                continue;
            }            
            if(type.contains("ScalarValue")) {
                auto scalarType = d->type_list[d->node_order[i]];
                if (scalarType == QJsonValue::Double) {
                    auto unit = node.unit;
                    auto typeString = unit.split(".")[1];
                    if(typeString.compare("int")==0) {
                        AddIntValue(node, verHeader.count(), row_cnt++);
                    }else if(typeString.compare("double")==0) {
                        AddDoubleValue(node, verHeader.count(), row_cnt++);
                    }
                }
            }else if(type.contains("SelectOption")) {
                AddComboValue(node, d->dupList.count(), row_cnt++, node.name);
            }
        }

        if(false == d->hideExecute) {            
            d->executeBtn = new QPushButton;
            d->executeBtn->setText("Execute");
            connect(d->executeBtn, SIGNAL(clicked()), this, SLOT(OnExecuteClicked()));
            d->layout->addWidget(d->executeBtn);
        }
        if(d->disable) {
            d->table->setDisabled(true);
        }
    }

    auto ParamTable::SetParameterValue(IParameter::Pointer param, QString key, bool blockSig,bool isOld) -> void {
        Q_UNUSED(isOld)
        auto organ = key;
        auto kIdx = -1;
        for(auto i=0;i<d->dupList.size();i++) {
            if(d->dupList[i].compare(organ)==0) {
                kIdx = i;
                break;
            }
        }
        if(d->dupParam.contains(organ)) {
            d->dupParam[organ] = param;
            auto names = param->GetNameAndTypes();
            for(const auto nnt : names) {
                auto name = std::get<0>(nnt);
                auto type = std::get<1>(nnt);
                if(d->scalars.contains(name)) {
                    auto val = param->GetValue(name).toDouble();
                    d->scalars[name]->setValue(val);
                }else if(d->checks.contains(name)) {
                    auto val = param->GetValue(name).toBool();
                    d->checks[name]->setChecked(val);
                }else if(d->setters.contains(name)) {
                    auto val = param->GetValue(name).toInt();
                    if(kIdx > -1) {
                        if(blockSig) {
                            d->combos[name][kIdx]->blockSignals(true);
                        }
                        d->combos[name][kIdx]->setCurrentIndex(val);
                        if(d->combos[name][kIdx]->currentText().compare("Customize")==0) {
                            d->dupReadOnly[kIdx] = false;
                        }else {
                            d->dupReadOnly[kIdx] = true;
                        }
                        if(blockSig) {
                            d->combos[name][kIdx]->blockSignals(false);
                        }
                    }
                }else if(type == QJsonValue::Double) {
                    auto val = param->GetValue(name).toDouble();
                    auto rIdx = -1;
                    for (auto k = 0; k < d->real_table_header.count();k++) {
                        if(d->real_table_header[k].compare(name)==0) {
                            rIdx = k;
                            break;
                        }
                    }
                    if(rIdx > -1 && kIdx > -1) {
                        auto item = new QTableWidgetItem(QString::number(val));
                        if(d->dupReadOnly[kIdx]) {
                            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                        }
                        if(blockSig) {
                            d->table->blockSignals(true);
                        }
                        d->table->setItem(rIdx, kIdx, item);
                        if(blockSig) {
                            d->table->blockSignals(false);
                        }
                    }

                }
            }
        }
    }

    auto ParamTable::AddNaiveControl(const QString& nodeName) -> void {
        if(d->list.GetNames().contains(nodeName)) {            
            auto node = d->list.GetNode(nodeName);
            auto type = node.unit;
            if(type.contains("FilePath")) {
                AddNaiveFilePath(node);
            }else if(type.contains("ScalarValue")) {
                auto scalarType = d->type_list[node.name];
                if(scalarType == QJsonValue::Double) {
                    auto unit = node.unit;
                    auto typeString = unit.split(".")[1];
                    if(typeString == "int") {
                        AddNaiveIntValue(node);
                    }else if(typeString == "double") {
                        AddNaiveDoubleValue(node);
                    }
                }
            }else if(type.contains("SelectOption")) {
                AddNaiveComboValue(node);
            }
            else if (type.contains("TFCheck")) {
                AddNaiveCheck(node);                
            }
        }
    }

    void ParamTable::OnInterfaceEnableChanged(QString nodeName, int state) {
        Q_UNUSED(state)
        auto eNode = d->enablers[nodeName];
        auto enable = d->checks[nodeName]->isChecked();
        auto falseList = eNode.hideNodes;
        auto trueList = eNode.showNodes;

        for (auto i = 0; i < falseList.count(); i++) {
            auto falseNode = falseList[i];            
            if (d->scalars.contains(falseNode)) {
                d->scalars[falseNode]->setEnabled(!enable && !d->disable);
                d->scalars[falseNode]->setVisible(!enable);
            }
            if(d->labels.contains(falseNode)) {
                d->labels[falseNode]->setVisible(!enable);
            }
        }
        for (auto i = 0; i < trueList.count(); i++) {
            auto trueNode = trueList[i];            
            if (d->scalars.contains(trueNode)) {
                d->scalars[trueNode]->setEnabled(enable && !d->disable);
                d->scalars[trueNode]->setVisible(enable);
            }
            if(d->labels.contains(trueNode)) {
                d->labels[trueNode]->setVisible(enable);
            }
        }

        for(auto dupParam : d->dupParam) {
            if(dupParam->ExistNode(nodeName)) {
                dupParam->SetValue(nodeName, enable);
            }
        }
        if(d->param) {
            if(d->param->ExistNode(nodeName)) {
                d->param->SetValue(nodeName, enable);
            }
        }

        for(auto organ_name : d->dupList) {
            emit enableChanged(enable,nodeName,d->algo_name,organ_name);
        }        
    }


    auto ParamTable::AddNaiveCheck(ParameterNode node) -> void {
        StrCheckBox* check = new StrCheckBox(this);
        check->setText(node.displayName);
        check->setToolTip(node.name);
        check->setToolTipDuration(0);
        d->layout->addWidget(check);
        d->checks[node.name] = check;        
        check->setChecked(node.value.toBool());
        connect(check, SIGNAL(checkedStr(QString, int)), this, SLOT(OnInterfaceEnableChanged(QString, int)));

        check->setDisabled(d->disable);
    }

    auto ParamTable::AddNaiveFilePath(ParameterNode node) -> void {
        //TODO add file path input if required
    }

    auto ParamTable::AddNaiveIntValue(ParameterNode node) -> void {
        ScalarInput* input = new ScalarInput(this);
        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        d->layout->addWidget(label);
        
        d->layout->addWidget(input);
        d->scalars[node.name] = input;
        auto min = node.minValue.toDouble();
        auto max = node.maxValue.toDouble();

        input->setName(node.name);
        input->setDecimals(0);
        input->setSingleStep(1.0);
        input->setValueRange(min, max);
        input->setValue(node.value.toInt());
        if (d->hide_list.contains(node.name)) {
            label->hide();
            input->hide();
        }

        connect(input, SIGNAL(scalarChanged(double, QString)), this, SLOT(OnValueChanged(double, QString)));
        input->setDisabled(d->disable);
    }

    auto ParamTable::AddNaiveDoubleValue(ParameterNode node) -> void {
        ScalarInput* input = new ScalarInput(this);

        QLabel* label = new QLabel(node.displayName);
        d->labels[node.name] = label;
        d->layout->addWidget(label);        
        d->layout->addWidget(input);

        d->scalars[node.name] = input;
        auto min = node.minValue.toDouble();
        auto max = node.maxValue.toDouble();
        input->setName(node.name);
        input->setDecimals(4);
        input->setSingleStep(0.0001);//to be changed
        input->setSliderRange(1, 1000);
        input->setValueRange(min, max);
        input->setValue(node.value.toDouble());

        if (d->hide_list.contains(node.name)) {
            input->hide();
            label->hide();
        }
        connect(input, SIGNAL(scalarChanged(double, QString)), this, SLOT(OnValueChanged(double, QString)));
        input->setDisabled(d->disable);
    }

    void ParamTable::OnValueChanged(double val, QString node_name) {
        for (auto organ_name : d->dupList) {
            emit naiveValChanged(val, node_name, d->algo_name, organ_name);
        }        
    }

    auto ParamTable::AddNaiveComboValue(ParameterNode node) -> void {
        //TODO add combo input if required
    }

    auto ParamTable::AddIntValue(ParameterNode node, int cols, int row) -> void {
        Q_UNUSED(node)
        Q_UNUSED(cols)
        d->table->setItemDelegateForRow(row, new IntegerDelegate);
    }

    auto ParamTable::AddDoubleValue(ParameterNode node, int cols, int row) -> void {
        Q_UNUSED(node)
        Q_UNUSED(cols)
        d->table->setItemDelegateForRow(row, new DoubleDelegate);
    }

    auto ParamTable::AddComboValue(ParameterNode node, int cols, int row, QString key) -> void {
        QList<QComboBox*> dupCombo;
        auto setter = d->setters[node.name];
        auto values = setter.headers;
        for (auto c = 0; c < cols; c++) {
            auto combo = new QComboBox;            
            QFont myFont("Noto Sans KR", 14); //assuming you are using times new roman
            QFontMetrics fm(myFont);
            QStringList itemList;
            int pixelwide = -1;
            for (auto v : values) {
                itemList << v;
            }
            itemList << "Customize";
            for (auto i = 0; i < itemList.size(); i++) {
                auto wh = fm.horizontalAdvance(itemList[i]);
                if (pixelwide < wh) {
                    pixelwide = wh;
                }
            }
            QString maxDropdownLen, styleSheet;
            styleSheet = "QComboBox {padding: 0px} QAbstractItemView { min-width: %1px;}";
            maxDropdownLen = QString::number(pixelwide - 10);
            combo->addItems(itemList);
            combo->setStyleSheet(styleSheet.arg(maxDropdownLen));
            combo->setObjectName(node.name + "." + QString::number(c));
            dupCombo.push_back(combo);
            connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboBoxChanged(int)));
            d->table->setCellWidget(row, c, combo);
        }
        d->combos[key] = dupCombo;
    }


    auto ParamTable::ParseParameter() -> void {
        auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
            ParameterNode node;
            node.name = name;
            node.displayName = param->DisplayName(name);
            node.description = param->Description(name);
            node.unit = param->Unit(name);
            node.value = param->GetValue(name);
            node.defaultValue = param->GetDefault(name);
            auto range = param->GetRange(name);
            node.minValue = std::get<0>(range);
            node.maxValue = std::get<1>(range);

            return node;
        };
        if(nullptr == d->param) {
            return;
        }
        d->list.Clear();
        d->type_list.clear();
        auto nameNtypes = d->param->GetNameAndTypes();
        for(const auto &nnt : nameNtypes) {            
            auto node = param2node(d->param, std::get<0>(nnt));
            auto type = node.description;            
            d->list.AppendNode(node);
            d->node_name.push_back(node.name);
            d->type_list[node.name] = std::get<1>(nnt);
        }
    }    

    auto ParamTable::ParseUiParameter() -> void {
        if(nullptr == d->uiParam) {
            return;
        }
        d->enablers.clear();
        d->setters.clear();
        d->node_order.clear();
        d->table_item.clear();
        d->hide_list.clear();

        //parse enabler
        auto enableNames = d->uiParam->GetEnablerNames();
        for (auto eName : enableNames) {
            auto eTuple = d->uiParam->GetEnabler(eName);
            EnablerNode eNode;
            eNode.showNodes = std::get<0>(eTuple);
            eNode.hideNodes = std::get<1>(eTuple);
            eNode.displayName = std::get<2>(eTuple);
            d->enablers[eName] = eNode;
        }
        //parse sorter
        auto sortNames = d->uiParam->GetSorterNames();
        for (auto sName : sortNames) {
            d->node_order = d->uiParam->GetSorter(sName);            
        }
        //parse hider
        auto hiderNames = d->uiParam->GetHiderNames();
        for (auto hName : hiderNames) {
            d->hide_list = d->uiParam->GetHider(hName);
        }

        //parse table item
        auto tableNames = d->uiParam->GetTableItemNames();
        for(auto tName: tableNames) {
            d->table_item = d->uiParam->GetTableItems(tName);
        }
        //parse setter
        auto setterNames = d->uiParam->GetSetterNames();
        for (auto sName : setterNames) {
            auto sTuple = d->uiParam->GetSetter(sName);
            SetterNode sNode;
            sNode.displayName = std::get<0>(sTuple);
            sNode.node_names = std::get<1>(sTuple);
            sNode.headers = std::get<2>(sTuple);
            sNode.valueList = std::get<3>(sTuple);
            d->setters[sName] = sNode;
        }
    }

    //slots
    void ParamTable::OnExecuteClicked() {
        emit execute(d->param_name,d->algo_name);
    }
    void ParamTable::OnTableItemChanged(QTableWidgetItem* item) {
        auto val = item->text().toDouble();
        auto organ_name = d->dupList[item->column()];
        auto value_name = d->real_table_header[item->row()];
        emit valChanged(val, organ_name, value_name, d->param_name,d->algo_name);
    }
    void ParamTable::OnComboBoxChanged(int idx) {
        auto cb = dynamic_cast<QComboBox*>(sender());
        if(cb) {
            auto value = cb->currentText();
            auto organIdx = cb->objectName().split(".")[1].toInt();
            auto organKey = d->dupList[organIdx];
            auto nodeName = cb->objectName().split(".")[0];
            auto setter = d->setters[nodeName];
            auto target_node_names = setter.node_names;
            auto valueList = setter.valueList;            
            if (value.compare("Customize") != 0) {
                SetCustomized(organIdx, true);
                for(auto i=0;i<target_node_names.count();i++) {
                    auto setteeKey = target_node_names[i];                                        
                    auto setValue = valueList[value][setteeKey];
                    QTableWidgetItem* item;                    
                    if(setValue.type() == QJsonValue::Double) {
                        item = new QTableWidgetItem(QString::number(setValue.toDouble()));
                    }else if(setValue.type() == QJsonValue::String) {
                        item = new QTableWidgetItem(setValue.toString());
                    }else {
                        item = new QTableWidgetItem;
                    }
                    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                    auto rIdx = -1;
                    for(auto k=0;k<d->real_table_header.count();k++) {
                        if(d->real_table_header[k].compare(setteeKey)==0) {
                            rIdx = k;
                            break;
                        }
                    }
                    d->table->blockSignals(true);
                    d->table->setItem(rIdx, organIdx, item);
                    d->table->blockSignals(false);
                    emit valChanged(setValue.toDouble(), d->dupList[organIdx], d->real_table_header[rIdx], d->param_name,d->algo_name);
                }
            }else {
                SetCustomized(organIdx, false);
            }
            emit comboChanged(idx, nodeName, organKey, d->param_name,d->algo_name);
        }
    }
    auto ParamTable::SetCustomized(int col, bool isReadOnly) -> void {
        if(d->dupReadOnly[col]!= isReadOnly) {
            for(auto i=0;i<d->table->rowCount();i++) {
                auto item = d->table->item(i, col);
                if(nullptr != item) {
                    item->setFlags(item->flags() | Qt::ItemIsEditable);
                }
            }
        }
        d->dupReadOnly[col] = isReadOnly;
    }
}