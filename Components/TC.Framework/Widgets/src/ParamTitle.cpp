#define LOGGER_TAG "[ParamTitle]"
#include <TCLogger.h>

#include <iostream>

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include "ParamTitle.h"

namespace TC {
	struct ParamTitle::Impl {
		QLabel* label{ nullptr };
		QPushButton* lockButton{ nullptr };
		QPushButton* showButton{ nullptr };
	};

	ParamTitle::ParamTitle(const QString &text, QWidget *parent) : QWidget(parent), d{ std::make_unique<Impl>() } {
		d->label = new QLabel(text);

	    d->lockButton = new QPushButton("L");	// TODO: replace with icon
		d->lockButton->setFixedSize(24, 24);
		d->lockButton->setCheckable(true);

		d->showButton = new QPushButton("V");	// TODO: replace with icon
		d->showButton->setFixedSize(24, 24);
		d->showButton ->setCheckable(true);

		auto layout = new QHBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(4);
		layout->addWidget(d->label, 1);
		layout->addWidget(d->lockButton);
		layout->addWidget(d->showButton);
		setLayout(layout);

		SetButtons(ParamTitleButtons::None);
	}

	ParamTitle::~ParamTitle() {
	}

	auto ParamTitle::SetText(const QString& text) -> void {
        d->label->setText(text);
    }

	auto ParamTitle::SetButtons(ParamTitleButtons buttons) -> void {
        d->lockButton->setVisible(static_cast<bool>(buttons & ParamTitleButtons::LockButton));
        d->showButton->setVisible(static_cast<bool>(buttons & ParamTitleButtons::ShowButton));
    }

	auto ParamTitle::IsLocked() -> bool {
        return d->lockButton->isChecked();
    }

	auto ParamTitle::IsShown() -> bool {
        return d->showButton->isChecked();
    }

}