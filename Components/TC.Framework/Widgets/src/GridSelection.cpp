#include <QGridLayout>
#include <QRadioButton>
#include <QCheckBox>

#include "GridSelection.h"

namespace TC {
    struct GridSelection::Impl {
        QStringList display_names;
        QStringList algo_names;
        SelectorType ui_type{SEL_CHECK_BOX};
        int max_col_cnt{ INT_MAX };
        QList<bool> status;
        QString myName;

        //Interface
        QGridLayout* layout{ nullptr };
        QList<QCheckBox*> checkBoxes;
        QList<QRadioButton*> radioBtns;
    };
    GridSelection::GridSelection(QWidget* parent) :QWidget(parent), d{ new Impl }{
        this->InitUI();
    }
    GridSelection::~GridSelection() {
        
    }
    auto GridSelection::InitUI() -> void {
        //build empty interface
        d->layout = new QGridLayout(this);        
    }
    auto GridSelection::SetSelectorName(const QString& name) -> void {
        d->myName = name;
    }
    auto GridSelection::GetSelectorName() -> QString {
        return d->myName;
    }    
    auto GridSelection::BuildInterface() -> bool {
        if(d->display_names.count()<1) {
            return false;
        }
        d->checkBoxes.clear();
        d->radioBtns.clear();
        auto row = 0;
        for (auto i = 0; i < d->display_names.count();i++) {
            auto col = i % d->max_col_cnt;
            if(SEL_CHECK_BOX == d->ui_type) {
                auto chk = new QCheckBox(d->display_names[i]);
                chk->setWhatsThis(QString::number(i));
                d->checkBoxes.push_back(chk);
                d->layout->addWidget(chk, row, col);
                /*connect(chk, SIGNAL(clicked()), this, SLOT(OnCheckBoxClicked())); */
                connect(chk, SIGNAL(stateChanged(int)), this, SLOT(OnCheckBoxClicked(int)));

            }else if(SEL_RADIO_BUTTON == d->ui_type) {
                auto radio = new QRadioButton(d->display_names[i]);
                radio->setWhatsThis(QString::number(i));
                d->radioBtns.push_back(radio);
                d->layout->addWidget(radio, row, col);
                connect(radio, SIGNAL(clicked()), this, SLOT(OnRadioBtnClicked()));
            }
            if(i+1 % d->max_col_cnt == 0) {
                row++;
            }
        }        
        return true;
    }
    auto GridSelection::SetDisplayNames(const QStringList& dispNames) -> void {
        d->display_names = dispNames;
        for(auto i=0;i<d->display_names.count();i++) {
            d->status.push_back(false);
        }
    }
    auto GridSelection::SetAlgorithmNames(const QStringList& algoNames) -> void {
        d->algo_names = algoNames;
    }
    auto GridSelection::GetAlgorithmNames() const -> QStringList {
        return d->algo_names;
    }
    auto GridSelection::SetType(SelectorType type) -> void {
        d->ui_type = type;
    }
    auto GridSelection::SetMaximumItemInRow(int item_count) -> void {
        d->max_col_cnt = item_count;
    }
    auto GridSelection::GetCurrentStatus() -> QList<bool> {
        for(auto i=0;i<d->status.count();i++) {
            d->status[i] = d->checkBoxes[i]->isChecked();
        }
        return d->status;
    }
    auto GridSelection::SetSelectionState(int idx, bool state) -> void {
        if(d->status.count()>idx) {
            d->status[idx] = state;
            if(SEL_CHECK_BOX == d->ui_type) {
                //d->checkBoxes[idx]->blockSignals(true);
                d->checkBoxes[idx]->setChecked(state);
                //d->checkBoxes[idx]->blockSignals(false);
            }else if(SEL_RADIO_BUTTON == d->ui_type) {
                d->radioBtns[idx]->blockSignals(true);
                d->radioBtns[idx]->setChecked(state);
                d->radioBtns[idx]->blockSignals(false);
            }
        }
    }
    void GridSelection::OnRadioBtnClicked() {
        auto radio = qobject_cast<QRadioButton*>(sender());
        if(radio) {
            auto idx = radio->whatsThis().toInt();
            if (idx < d->algo_names.count()) {
                auto state = radio->isChecked();
                emit sigSelection(d->algo_names[idx], state);
            }
        }
    }
    void GridSelection::OnCheckBoxClicked(int state) {
        auto check = qobject_cast<QCheckBox*>(sender());
        if(check) {
            auto idx = check->whatsThis().toInt();
            if (idx < d->algo_names.count()) {
                auto state = check->isChecked();
                emit sigSelection(d->algo_names[idx], state);
            }
        }
    }
}