#define LOGGER_TAG "[ProcessorControl]"
#include <TCLogger.h>

#include <iostream>
#include <QVBoxLayout>
#include <QLabel>
#include <QSpacerItem>
#include <QRadioButton>

#include "CollapseWidget.h"

#include "ProcessorControl.h"

#include <QApplication>
#include <QPushButton>
#include <QGridLayout>
#include <QScrollArea>
#include <QGroupBox>

#include "ParameterControl.h"
#include "ParameterTable.h"
#include "ParameterList.h"
#include "PluginRegistry.h"

namespace TC {
	struct paramList {

	};
	struct ProcessorControl::Impl {
		QString proc_name;
		IParameter::Pointer param{ nullptr };
		ParameterList algo_list;
		QStringList algo_names;
		QStringList algo_fulls;
		QStringList algo_order;

		QMap<QString, QString> dupName;
		QMap<QString, QStringList> duplicator;
		QMap<QString, QString> dupOrder;

		QList<QRadioButton*> dupRadio;

		QList<CollapseWidget*> algoContainer;
		QMap<QString, ParameterControl*> algoControls;
		QMap<QString, ParameterTable*> algoTables;

		QVBoxLayout* parentlayout{nullptr};
		QVBoxLayout* layout{ nullptr };
		QWidget* scrollWidget{nullptr};
		QScrollArea* scrollArea{nullptr};		
		QPushButton* executeWholeBtn{nullptr};

		bool isHiddenExe{ false };
		bool isHiddenProcExe{ false };
		bool isDisable{ false };

		QStringList disList;
		QStringList realList;

		int calcHeight{ 0 };
		int curHeight{ 100000 };

		QMap<QString, QString> highlight_rule;
	};

	ProcessorControl::ProcessorControl(QWidget* parent) :QWidget(parent), d{ new Impl } {		
		d->scrollWidget = new QWidget;		
		d->layout = new QVBoxLayout;		
		d->layout->setContentsMargins(0, 0, 0, 0);
		d->layout->setSpacing(7);
		d->layout->setAlignment(Qt::AlignTop);

		d->parentlayout = new QVBoxLayout;
		d->parentlayout->setContentsMargins(14, 0, 6, 0);
		d->parentlayout->setSpacing(14);

		setLayout(d->parentlayout);

		d->scrollArea = new QScrollArea(parent);
		d->scrollArea->setWidgetResizable(true);
		d->scrollArea->setWidget(d->scrollWidget);
		d->parentlayout->addWidget(d->scrollArea);		

		d->scrollWidget->setLayout(d->layout);		

		setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Preferred);

		setObjectName("panel-contents");
	}
	ProcessorControl::~ProcessorControl() {
		if(nullptr != d->executeWholeBtn) {
			delete d->executeWholeBtn;
		}
		for(auto wid : d->algoControls) {			
			delete wid;
		}
	    for(auto table : d->algoTables) {			
			delete table;
		}
		for(auto cont : d->algoContainer) {			
			delete cont;
		}

	    delete d->layout;
	    delete d->scrollWidget;
		delete d->scrollArea;		
		delete d->parentlayout;		
	}

	auto ProcessorControl::setCompetableMinSize(int size) -> void {
        if(d->calcHeight > size) {			
			setMinimumHeight(size);
			d->curHeight = size;
        }else {
			setMinimumHeight(d->calcHeight);
        }
    }

	auto ProcessorControl::findDupKey(const QString& key) const -> QString {
		QString dupKey = QString();
		for (auto rad : d->dupRadio) {
			if (rad->isChecked() && rad->toolTip().compare(key) == 0) {
				dupKey = rad->text();
				break;
			}
		}
		return dupKey;
	}
	auto ProcessorControl::setDisableControl(bool disable) -> void {
		d->isDisable = disable;
	}

	void ProcessorControl::OnPathChangeCall(QString path, QString sender) {
		auto s = sender.split("*")[0];
		auto key = sender.split("*")[1];

		if (d->duplicator.contains(s)) {
			auto plugin = PluginRegistry::GetPlugin(s);
			QString dupKey = findDupKey(s);
			if (dupKey.isEmpty()) {
				return;
			}
			if (dupKey.compare("All") == 0) {
				for (auto dup : d->duplicator[s]) {
					auto childName = plugin->GetName() + " Parameter!" + dup;
					auto child = d->param->GetChild(childName);
					child->SetValue(key, path);
				}
			}
			else {
				auto childName = plugin->GetName() + " Parameter!" + dupKey;
				auto child = d->param->GetChild(childName);
				child->SetValue(key, path);
			}
		}
		//emit pathCall(path, sender); interactive?
	}	
	void ProcessorControl::OnTableComboCall(int idx, QString node_name, QString organ_name, QString param_name) {
        auto nn = d->param->GetChildrenNames();
		auto s = param_name;
		auto key = node_name;
		if(d->duplicator.contains(s)) {
		    auto plugin = PluginRegistry::GetPlugin(s);
			auto childName = plugin->GetName() + " Parameter!" + organ_name;				
			auto child = d->param->GetChild(childName);			
			child->SetValue(key, idx);
		}else {
		    for (auto n : nn) {
				auto nodeName = n.chopped(10);
				auto des = d->param->GetValue(nodeName).toString();
				if (s.compare(des) == 0) {
					auto child = d->param->GetChild(n);
					child->SetValue(key, idx);
				}
			}
		}
    }
	void ProcessorControl::OnComboChangeCall(int idx, QString sender) {
		auto nn = d->param->GetChildrenNames();
		auto s = sender.split("*")[0];
		auto key = sender.split("*")[1];
				
		if (d->duplicator.contains(s)) {
			auto plugin = PluginRegistry::GetPlugin(s);
			QString dupKey = findDupKey(s);

			if (dupKey.isEmpty()) {
				return;
			}

			if (dupKey.compare("All") == 0) {
				for (auto dup : d->duplicator[s]) {
					auto childName = plugin->GetName() + " Parameter!" + dup;
					auto child = d->param->GetChild(childName);
					child->SetValue(key, idx);
				}
			}
			else {
				auto childName = plugin->GetName() + " Parameter!" + dupKey;				
				auto child = d->param->GetChild(childName);
				child->SetValue(key, idx);
			}
		}
		else {
			for (auto n : nn) {
				auto nodeName = n.chopped(10);
				auto des = d->param->GetValue(nodeName).toString();
				if (s.compare(des) == 0) {
					auto child = d->param->GetChild(n);
					child->SetValue(key, idx);
				}
			}
		}
	}

	void ProcessorControl::OnCheckChangeCall(bool state, QString sender) {
		auto nn = d->param->GetChildrenNames();
		auto s = sender.split("*")[0];
		auto key = sender.split("*")[1];

		if (d->duplicator.contains(s)) {
			auto plugin = PluginRegistry::GetPlugin(s);
			QString dupKey = findDupKey(s);
			if (dupKey.isEmpty())
				return;
			if (dupKey.compare("All") == 0) {
				for (auto dup : d->duplicator[s]) {
					auto childName = plugin->GetName() + " Parameter!" + dup;
					auto child = d->param->GetChild(childName);
					if (state) {
						child->SetValue(key, true);
					}
					else {
						child->SetValue(key, false);
					}
				}
			}
			else {
				auto childName = plugin->GetName() + " Parameter!" + dupKey;
				auto child = d->param->GetChild(childName);
				if (state) {
					child->SetValue(key, true);
				}
				else {
					child->SetValue(key, false);
				}
			}
		}
		else {
			for (auto n : nn) {
				auto nodeName = n.chopped(10);
				auto des = d->param->GetValue(nodeName).toString();
				if (s.compare(des) == 0) {
					auto child = d->param->GetChild(n);					
					if (state) {
						child->SetValue(key,true);
					}
					else {
						child->SetValue(key, false);
					}
				}
			}
		}
		if (d->algoControls.contains(s)) {
			clearHighlights();
			if (state) {
				d->algoControls[s]->highlightBtn(true);
				emit highlightApply(false);
			}else {
				emit refreshHighlight();
			}
		}
		else if (d->algoTables.contains(s)) {
			clearHighlights();
			if (state) {
				d->algoControls[s]->highlightBtn(true);
				emit highlightApply(false);
			}else {
				emit refreshHighlight();
			}
		}
	}

	void ProcessorControl::OnTableChangeCall(double value, QString organ_name, QString node_name,QString param_name) {
        
		auto nn = d->param->GetChildrenNames();
		auto organ = organ_name;
	    auto s = param_name;
		auto key = node_name;

		auto plugin = PluginRegistry::GetPlugin(s);
		auto subKey = plugin->GetFullName();
		auto childName = plugin->GetName();

		if(d->duplicator.contains(subKey)) {		    
			auto childFullName = childName + " Parameter!" + organ_name;
			auto child = d->param->GetChild(childFullName);
			child->SetValue(key,value);
		}else {
		    for (auto n : nn) {
				auto nodeName = n.chopped(10);
				auto des = d->param->GetValue(nodeName).toString();
				if (s.compare(des) == 0) {
					auto child = d->param->GetChild(n);
					child->SetValue(key, value);
				}
			}
		}
		emit valueCall(value, param_name+"!"+node_name);//for interactive task
    }

	void ProcessorControl::OnParameterChangeCall(double val, QString sender) {
		auto nn = d->param->GetChildrenNames();
		auto s = sender.split("!")[0];
		auto key = sender.split("!")[1];

		auto plugin = PluginRegistry::GetPlugin(s);
		auto subKey = plugin->GetFullName();
		auto childName = plugin->GetName();

		if (d->duplicator.contains(subKey)) {
			//Find checked name
			QString checkName = findDupKey(subKey);
			if (checkName.compare("All") == 0) {
				for (auto n : d->duplicator[subKey]) {
					auto childFullName = childName + " Parameter!" + n;
					auto child = d->param->GetChild(childFullName);
					if (child) {
						child->SetValue(key, val);
					}
					else {
						QLOG_INFO() << "No Child info in processing parameter";
					}
				}
			}
			else {
				auto childFullName = childName + " Parameter!" + checkName;
				auto child = d->param->GetChild(childFullName);
				child->SetValue(key, val);
			}
		}
		else {
			for (auto n : nn) {
				auto nodeName = n.chopped(10);
				auto des = d->param->GetValue(nodeName).toString();
				if (s.compare(des) == 0) {
					auto child = d->param->GetChild(n);
					child->SetValue(key, val);
				}
			}
		}
		emit valueCall(val, sender);//for interactive task
	}

	auto ProcessorControl::clearInterface() -> void {
		QLayoutItem* item;
		while ((item = d->layout->takeAt(0))) {
			if (item->widget()) {
				delete item->widget();
			}
			delete item;
		}

		d->algo_list.Clear();
		d->algo_names.clear();
		d->algo_fulls.clear();
		d->algo_order.clear();
		d->algoContainer.clear();
		d->algoControls.clear();
		d->algoTables.clear();

		d->duplicator.clear();
		d->dupRadio.clear();
		d->dupOrder.clear();
		d->dupName.clear();

		d->highlight_rule.clear();

		d->param = nullptr;
	}
	auto ProcessorControl::setProcessingParameter(IParameter::Pointer param) -> void {
		clearInterface();
		d->param = param;
		parseProcessingAlgorithm();
		arrangeProcessingAlgorithm();
	}

	auto ProcessorControl::clearHighlights() -> void {
		for (auto al : d->algoControls) {
			al->highlightBtn(false);
		}
		for (auto table : d->algoTables) {
			table->highlightBtn(false);
		}
    }

	auto ProcessorControl::setDefaultHighlight() -> void {
		auto default = d->highlight_rule["Default"];
		if(false == default.isEmpty()) {
		    if(d->algoControls.contains(default)) {
				d->algoControls[default]->highlightBtn(true);
		    }else if(d->algoTables.contains(default)) {
				d->algoTables[default]->highlightBtn(true);
		    }
		}
    }

	auto ProcessorControl::setProcHighlight(bool isHi, QString algo_name) -> void {
        if(false == algo_name.isEmpty()) {
			if(d->algoControls.contains(algo_name)){
			    d->algoControls[algo_name]->highlightBtn(isHi);
				emit highlightApply(false);
			}else if(d->algoTables.contains(algo_name)) {
			    d->algoTables[algo_name]->highlightBtn(isHi);
				emit highlightApply(false);
			}
        }
    }
	auto ProcessorControl::setWholeHighlight(bool isHi) -> void {
        if(isHi) {
            d->executeWholeBtn->setStyleSheet("background-color: rgb(91,139,151);");
        }else {
            d->executeWholeBtn->setStyleSheet("");
        }
    }
	auto ProcessorControl::setParameterValue(IParameter::Pointer param, QString algo_name,bool blockSig) -> void {
		if (algo_name.isEmpty()) {
			if (nullptr == d->param) {
				d->param = param;
			}		
			auto childNames = param->GetChildrenNames();
			for (auto ch : childNames) {				
				d->param->SetChild(ch, param->GetChild(ch));
				if(ch.split("!").size()>1) {
					auto al = ch.split("!")[0];
					auto organ = ch.split("!")[1];
					auto alfull = param->GetValue(al.chopped(10)).toString();
					if(d->algoTables.contains(alfull)) {
					    d->algoTables[alfull]->setAlgorithmValue(param->GetChild(ch),organ,blockSig);
					}
				}else {
					auto alfull = param->GetValue(ch.chopped(10)).toString();
				    if(d->algoControls.contains(alfull)) {
				        d->algoControls[alfull]->setAlgorithmValue(param->GetChild(ch),blockSig);
				    }else if(d->algoTables.contains(alfull)) {
				        d->algoTables[alfull]->setAlgorithmValue(param->GetChild(ch),"default",blockSig);
				    }
				}
			}
		}
		else {			
			d->algoControls[algo_name]->setAlgorithmValue(param,blockSig);
		}
	}
	auto ProcessorControl::setParameterValue(IParameter::Pointer param, QString algo_name, QString organ_name) -> void {
		d->algoTables[algo_name]->setAlgorithmValue(param, organ_name);
	}

	auto ProcessorControl::getParameter() -> IParameter::Pointer {
		//무조건 processor parameter 전체를 받아서 활용하도록 한다
		if(nullptr == d->param) {
		    return nullptr;
		}
		return std::shared_ptr<IParameter>(d->param->Clone());
	}
	auto ProcessorControl::getParameter(QString algo_name) -> IParameter::Pointer {
		auto plugin = PluginRegistry::GetPlugin(algo_name);		
		auto param = d->param->GetChild(plugin->GetName() + " Parameter");

		return param;
	}
	auto ProcessorControl::loadRequiredAlgorithm() -> bool {
		auto names = d->algo_list.GetNames();
		for (int i = 0; i < names.size(); i++) {
			auto node = d->algo_list.GetNode(names[i]);			
			auto plugin = PluginRegistry::GetPlugin(node.value.toString());
			auto param = plugin->Parameter();

			QStringList dupList;
			if (d->duplicator.contains(node.value.toString())) {
				//duplicator가 있으면 그에 맞춰
				dupList = d->duplicator[node.value.toString()];
				for (auto n : d->duplicator[node.value.toString()]) {
					auto childFull = node.name + " Parameter!" + n;
					auto child = std::shared_ptr<IParameter>(param->Clone());
					if (n.contains("lipid") || n.contains("Lipid")) {//Lipid initial value 
						if (child->ExistNode("Preset!Setter")) {
							auto setters = std::get<0>(child->GetRange("Preset!Setter")).toString();
							auto split = setters.split("!");
							auto targetIdx = -1;
							for (auto sidx = 0; sidx < split.size(); sidx++) {
								if (split[sidx].contains("Lipid")) {
									targetIdx = sidx;
									break;
								}
							}
							if (targetIdx > -1) {
								child->SetValue("Preset!Setter", targetIdx);
								auto cnames = child->GetNames();
								QStringList targets;
								for(auto cn: cnames) {
								    if(cn.contains("Preset")&&!cn.contains("Setter")&&!cn.contains("Enabler")) {										
								        targets.push_back(cn.split("!")[1]);
								    }
								}
								for(auto tta : targets) {
								    auto val = child->GetValue("Preset!"+tta).toString().split("!")[targetIdx].toDouble();
									child->SetValue(tta,val);
								}
							}
						}
					}
					d->param->SetChild(childFull, child);
				}
			}
			else {
				//없다면 naive naming
				d->param->SetChild(node.name + " Parameter", param);
			}
			
			if (param->ExistNode("UIType")) {
				auto type = param->GetValue("UIType").toString();
				if (type.compare("Table") == 0) {					
					if (dupList.size() < 1) {						
						dupList.push_back("default");
					}
					auto table = new ParameterTable(nullptr);
					table->setName(plugin->GetFullName());
					table->setDisable(d->isDisable);
					table->setProcessingAlgorithm(param);					
					table->setHideExecute(d->isHiddenProcExe);
					table->setDuplicator(dupList);
					table->setConverter(d->disList,d->realList);
					table->buildInterface();
					d->algoTables[plugin->GetFullName()] = table;
					if(d->param->GetChildrenNames().size()>0) {
						for(auto chName: d->param->GetChildrenNames()){
							if(chName.contains("!")){
							    table->setAlgorithmValue(d->param->GetChild(chName),chName.split("!")[1]);
							}else if(d->param->GetChildDescription(chName).contains("measurement")) {
							    table->setAlgorithmValue(d->param->GetChild(chName));
							}
						}					    
					}else {						
					    table->setAlgorithmValue(d->param);
					}
												
					connect(table,SIGNAL(comboChanged(int,QString,QString,QString)),this,SLOT(OnTableComboCall(int,QString,QString,QString)));
					connect(table,SIGNAL(valChanged(double,QString,QString,QString)),this,SLOT(OnTableChangeCall(double,QString,QString,QString)));
					connect(table,SIGNAL(execute(QString)),this,SLOT(OnParamaterExecuteCall(QString)));
				}
			}
			else {
				auto control = new ParameterControl(nullptr);
				control->setDisable(d->isDisable);
				control->setName(plugin->GetFullName());
				control->setProcessingAlgorithm(param);
				control->setHideExecute(d->isHiddenProcExe);
				control->buildInterface();
				d->algoControls[plugin->GetFullName()] = control;				
				//d->algoControls.push_back(control);
				connect(control, SIGNAL(pathChanged(QString, QString)), this, SLOT(OnPathChangeCall(QString, QString)));
				connect(control, SIGNAL(valChanged(double, QString)), this, SLOT(OnParameterChangeCall(double, QString)));
				connect(control, SIGNAL(execute(QString)), this, SLOT(OnParamaterExecuteCall(QString)));
				connect(control, SIGNAL(checkChanged(bool, QString)), this, SLOT(OnCheckChangeCall(bool, QString)));
				connect(control, SIGNAL(comboChanged(int, QString)), this, SLOT(OnComboChangeCall(int, QString)));
			}
		}
		return true;
	}	
	auto ProcessorControl::setConverter(QStringList disList, QStringList realList) -> void {
        d->disList = disList;
		d->realList = realList;
    }

	void ProcessorControl::OnParamaterExecuteCall(QString param_name) {		
		emit executeCall(param_name);

	    clearHighlights();
		auto target = d->highlight_rule[param_name];
		if(target == "Apply Parameter") {			
			emit highlightApply(true);
		}else {
			setProcHighlight(true, target);	
		}
	}
	auto ProcessorControl::setHideExecute(bool isHide) -> void {
		d->isHiddenExe = isHide;
	}
	auto ProcessorControl::setHidePramExecute(bool isHide) -> void {
		d->isHiddenProcExe = isHide;
	}
	auto ProcessorControl::parseProcessingAlgorithm() -> bool {
		auto param2node = [](IParameter::Pointer param, const QString& name)->ParameterNode {
			ParameterNode node;
			node.name = name;
			node.displayName = param->DisplayName(name);
			node.description = param->Description(name);
			node.unit = param->Unit(name);
			node.value = param->GetValue(name);
			node.defaultValue = param->GetDefault(name);
			auto range = param->GetRange(name);
			node.minValue = std::get<0>(range);
			node.maxValue = std::get<1>(range);

			return node;
		};
		if (nullptr != d->param) {
			if (!d->param.get()) {
				return false;
			}
			d->algo_list.Clear();
			d->algo_names.clear();
			d->algo_fulls.clear();
			d->algo_order.clear();
			d->highlight_rule.clear();

			auto nameNtypes = d->param->GetNameAndTypes();
			for (const auto& nnt : nameNtypes) {
				auto node = param2node(d->param, std::get<0>(nnt));

				if (node.description.contains("DuplicateParam")) {
					auto dupCandiate = node.value.toString().split("!");
					QStringList nonDups;
					QStringList Dups;
					for (const auto& dup : dupCandiate) {
						if (dup.contains("*")) {
							nonDups.push_back(dup.split("*")[0]);
						}
						else {
							Dups.push_back(dup);
						}
					}
					d->duplicator[node.displayName] = Dups;
					d->dupOrder[node.displayName] = node.unit;
					d->dupName[node.displayName] = node.name;
				}else if(node.description.contains("DoNotMakeInterface")) {
					//parse highlight state machine
					if(node.name.contains("Highlight")) {
						auto sender = node.unit.split("!")[0];
						auto target = node.unit.split("!")[1];
						d->highlight_rule[sender] = target;
					}
				    continue;
				}
				else {
					d->algo_list.AppendNode(node);
					d->algo_names.push_back(node.name);
					d->algo_fulls.push_back(node.value.toString());
					d->algo_order.push_back(node.unit);
				}
			}

			loadRequiredAlgorithm();

			return true;
		}
		else {
			QLOG_ERROR() << "processor parameter is null";
			return false;
		}
	}	
	auto ProcessorControl::arrangeProcessingAlgorithm() -> void {
		//reorder the algo names;		
		d->executeWholeBtn = nullptr;
		auto size = 0;
		std::vector<int> order;
		for (int i = 0; i < d->algo_order.size(); i++) {
			auto idx = d->algo_order[i].toInt();
			order.push_back(idx);
		}
		//create Hidable Tab widget for each algorithm        
		for (int i = 0; i < d->algo_names.size(); i++) {//갯수만을 의미한다
			auto tmpWidget = new CollapseWidget;			
			auto ii = 0;
			for (auto j = 0; j < order.size(); j++) {
				if (order[j] == i) {
					ii = j;
					break;
				}
			}
			d->algoContainer.push_back(tmpWidget);
			if(d->algoControls.contains(d->algo_fulls[ii])) {
			    tmpWidget->SetWidget(d->algoControls[d->algo_fulls[ii]], d->algo_names[ii]);				
			}
			if(d->algoTables.contains(d->algo_fulls[ii])) {				
			    tmpWidget->SetWidget(d->algoTables[d->algo_fulls[ii]],d->algo_names[ii]);				
			}
			d->layout->addWidget(tmpWidget);			
			tmpWidget->show();
			tmpWidget->adjustSize();
			if (size > 0) {
				size += 7;
			}
			size += tmpWidget->height();			
			//hide duplicator
			/*
			for (auto ord : d->dupOrder) {
				if (ord.toInt() == i) {
					AddDuplicator(d->dupOrder.keys()[i]);
				}
			}*/
		}
		if (!d->isHiddenExe) {
			d->executeWholeBtn = new QPushButton;
			d->executeWholeBtn->setText("Execute Whole");
			d->layout->addWidget(d->executeWholeBtn);
			d->executeWholeBtn->show();
			size += 7;
			size += d->executeWholeBtn->height();
			connect(d->executeWholeBtn, SIGNAL(clicked()), this, SLOT(OnProcessorExecuteCall()));
		}				
		d->calcHeight = size;
		if(d->curHeight > d->calcHeight) {
			setMinimumHeight(size);
		}else {
			setMinimumHeight(d->curHeight);
		}
	}	

	auto ProcessorControl::AddDuplicator(const QString& key) -> void {
		auto tmpWidget = new CollapseWidget;
		auto contentsWidget = new QWidget(nullptr);
		auto contentLay = new QVBoxLayout;
		contentsWidget->setLayout(contentLay);
		tmpWidget->SetWidget(contentsWidget, d->dupName[key]);
		d->layout->addWidget(tmpWidget);

		if (d->duplicator[key].size() > 0) {
			auto dupGroup = new QGroupBox;
			auto dupHori = new QGridLayout;
			dupGroup->setLayout(dupHori);

			auto grid = 0;
			auto row = 0;
			for (auto dupdup : d->duplicator[key]) {
				if (grid % 2 == 0 && grid > 0) {
					row++;
					grid = 0;
				}
				auto rad = new QRadioButton(dupdup);
				rad->setToolTip(key);
				rad->setToolTipDuration(0);
				//auto chk = new QCheckBox(dupdup);
				connect(rad, SIGNAL(clicked()), this, SLOT(OnDupChanged()));
				//d->dupChk.push_back(chk);
				d->dupRadio.push_back(rad);
				dupHori->addWidget(rad, (row), grid);
				//dupHori->addWidget(chk,(row),grid);
				grid++;
			}
			auto all = new QRadioButton("All");
			all->setChecked(true);
			all->setToolTip(key);
			all->setToolTipDuration(0);
			connect(all, SIGNAL(clicked()), this, SLOT(OnDupChanged()));
			d->dupRadio.push_back(all);
			dupHori->addWidget(all, (row + 1), 0);
			dupHori->setSpacing(0);
			dupHori->setMargin(0);
			contentLay->addWidget(dupGroup);
		}
		contentLay->setMargin(0);
		contentLay->setSpacing(0);
		contentLay->addStretch();
		//tmpWidget->layout()->addWidget()        
	}
	void ProcessorControl::OnDupChanged() {
		auto pChk = qobject_cast<QRadioButton*>(sender());
		if (pChk) // this is the type we expect
		{
			QString buttonText = pChk->text();
			if (buttonText.compare("All") == 0) {
				return;
			}
			auto algo_full = pChk->toolTip();
			auto plugin = PluginRegistry::GetPlugin(algo_full);

			auto childName = plugin->GetName() + " Parameter!" + buttonText;

			auto childParam = d->param->GetChild(childName);

			if (childParam == nullptr) {				
				return;
			}

			d->algoControls[algo_full]->setAlgorithmValue(childParam);
		}
	}

	void ProcessorControl::OnProcessorExecuteCall() {
		emit executeWhole();
	}

}
