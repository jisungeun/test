#include <iostream>

#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QDir>

#include <ParamControl.h>
#include <PluginRegistry.h>
#include <IProcessingAlgorithm.h>

auto LoadAlgoModules() -> void {
    auto Load = [](QDir dir) {
        const auto appPath = dir.absolutePath();

        QStringList fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
        for (int i = 0; i < fileList.size(); i++) {
            auto fullPath = appPath + "/" + fileList[i];                
            if (PluginRegistry::LoadPlugin(fullPath) == 0) {
                std::cout << "Failed to load plugins in " << fullPath.toLocal8Bit().toStdString() << std::endl;
            }
        }    
    };

    QDir dir(qApp->applicationDirPath());
    dir.cd("algorithms");
    dir.cd("preprocessing");
    Load(dir);
}

auto GetUiParameter(const QString& pluginName) -> std::tuple<IParameter::Pointer, IUiParameter::Pointer> {
    const auto plugin = PluginRegistry::GetPlugin(pluginName);
    const auto module = std::dynamic_pointer_cast<IProcessingAlgorithm>(plugin);

    return { module->Parameter(), module->UiParameter() };
}

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	QWidget window;

    LoadAlgoModules();
    const auto [param, uiParam] = GetUiParameter("org.tomocube.algorithm.smoothing.medianfilter.3d");

    TC::ParamControl paramControl(&window);
    paramControl.SetParameter(param);
    paramControl.SetUiParameter(uiParam);
    paramControl.BuildInterface();

    auto layout = new QVBoxLayout;
	layout->addWidget(&paramControl);

    window.setLayout(layout);
	window.show();

	return app.exec();
}