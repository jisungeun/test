#pragma once
#include <QWidget>

#include <IParameter.h>


#include "ParameterList.h"
#include "ScalarInput.h"
#include "TCFrameworkWidgetsExport.h"
class QTableWidgetItem;

namespace TC {    
    class TCFrameworkWidgets_API ParameterTable : public QWidget {
        Q_OBJECT
        typedef QMap<QString,QJsonValue::Type> TMap;        
    public:
        ParameterTable(QWidget* parent);
        ~ParameterTable();

        auto setProcessingAlgorithm(IParameter::Pointer param)->void;
        auto setDuplicator(QStringList dupList)->void;
        auto setAlgorithmValue(IParameter::Pointer param, QString key = "default",bool blockSig = true)->void;

        auto setName(QString name)->void;

        auto setHideExecute(bool isHide)->void;
        auto setDisable(bool dis)->void;
        auto getScalarValue(QString name,QString key = "default")->double;
        auto setScalarValue(QString name,double val,QString key = "default")->void;

        auto GetParameter(QString key = "default")->IParameter::Pointer;
        auto buildInterface()->void;
        auto highlightBtn(bool isHi)->void;

        auto setConverter(QStringList disList,QStringList realList)->void;

    signals:
        void valChanged(double,QString,QString,QString);//value, node_name, key, param_name
        void execute(QString);
        void comboChanged(int,QString,QString,QString);        

    public slots:
        void OnExecuteClicked();
        void OnComboBoxChanged(int);        
        void OnTableItemChanged(QTableWidgetItem* previous);

    private:
        auto parseParameter()->void;
        auto toMultiRow(QString txt,int letters)->QString;
        auto toSingleRow(QString txt)->QString;

        auto addComboValue(ParameterNode node,int cols,int row,QString key)->void;
        auto addIntValue(ParameterNode node,int cols,int row)->void;
        auto addDoubleValue(ParameterNode node,int cols,int row)->void;
                
        auto setCustomized(int col,bool isReadOnly)->void;

        auto toDisplayName(QString name)->QString;
        auto toRealName(QString name)->QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
