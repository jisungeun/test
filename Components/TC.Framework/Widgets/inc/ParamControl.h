#pragma once

#include <QWidget>

#include <IParameter.h>
#include <IUiParameter.h>
#include <Parameterlist.h>//required?

#include "StrComboBox.h"
#include "StrCheckBox.h"
#include "PathInput.h"
#include "RangeSlider.h"
#include "ScalarInput.h"

#include "TCFrameworkWidgetsExport.h"

namespace TC {
	class TCFrameworkWidgets_API ParamControl : public QWidget {
		Q_OBJECT
		typedef QMap<QString, QJsonValue::Type> TMap;
		//interface
		typedef QMap<QString, RangeSlider*> RangeMap;
		typedef QMap<QString, PathInput*> PathMap;
		typedef QMap<QString, ScalarInput*> ScalarMap;
		typedef QMap<QString, StrComboBox*> ComboMap;
		typedef QMap<QString, StrCheckBox*> CheckMap;

	public:
		ParamControl(QWidget* parent);
		~ParamControl();

		auto SetParameter(IParameter::Pointer param)->void;
		auto SetUiParameter(IUiParameter::Pointer uiParam)->void;
		auto SetParameterValue(IParameter::Pointer param, bool blockSig = true,bool isOld = false)->void;
		auto BuildInterface()->void;

		auto SetHideExecute(bool isHide)->void;
		auto SetDiable(bool isDisable)->void;
		auto SetHighLight(bool isHi)->void;

		auto SetName(QString name)->void;
		auto SetAlgoName(QString name)->void;
		auto GetScalarValue(QString name)->double;
		auto SetScalarValue(QString name, double val)->void;
		auto GetStringValue(QString name)->QString;
		auto SetStringValue(QString name, QString val)->void;
		auto GetComboValue(QString name)->int;
		auto SetComboValue(QString name, int val)->void;
		auto GetCheckValue(QString name)->bool;
		auto SetCheckValue(QString name, bool val)->void;

		auto GetParameter()->IParameter::Pointer;

		auto SetFixedControl(QString valueName,bool fixed = false)->void;

	signals:
		void execute(QString,QString);
		void pathChanged(QString, QString,QString,QString);
		void valChanged(double, QString,QString,QString);
		void comboChanged(int, QString,QString,QString);
		void checkChanged(bool, QString,QString,QString);

	public slots:
		void OnValueChanged(double, QString);
		void OnInterfaceEnableChanged(QString, int);
		void OnSetterChanged(QString,QString, int);		
		void OnExecuteClicked();
		void OnPathChanged(QString, QString);

	private:
		auto ParseParameter()->void;
		auto ParseUiParameter()->void;
		auto AddFile(ParameterNode node)->void;
		auto AddPath(ParameterNode node)->void;
		auto AddDoubleValue(ParameterNode node)->void;
		auto AddIntValue(ParameterNode node)->void;
		//auto AddComboValue(ParameterNode node)->void;
		auto AddComboValue(QString name,int defaultIdx,SetterNode node)->void;
		auto AddCheck(QString name,bool defaultCheck, EnablerNode node)->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}