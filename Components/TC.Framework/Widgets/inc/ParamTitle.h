#pragma once

#include <QWidget>

namespace TC {
	enum class ParamTitleButtons : uint16_t {
		None = 0x00,
		LockButton = 0x01,
		ShowButton = 0x02,
		AllButton = LockButton | ShowButton
	};

	inline ParamTitleButtons operator&(ParamTitleButtons l, ParamTitleButtons r) {
        return static_cast<ParamTitleButtons>(static_cast<uint16_t>(l) & static_cast<uint16_t>(r));
    }

    inline ParamTitleButtons operator|(ParamTitleButtons l, ParamTitleButtons r) {
        return static_cast<ParamTitleButtons>(static_cast<uint16_t>(l) | static_cast<uint16_t>(r));
    }

    /**
	 * @brief ParamTitle is used for displaying algorithm parameter title with lock/unlock button and show/hide button.
	 */
	class ParamTitle : public QWidget {
		Q_OBJECT

	public:
		explicit ParamTitle(const QString &text, QWidget *parent = nullptr);
		~ParamTitle() override;

		auto SetText(const QString& text) -> void;
		auto SetButtons(ParamTitleButtons buttons) -> void;

		auto IsLocked() -> bool;
		auto IsShown() -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}