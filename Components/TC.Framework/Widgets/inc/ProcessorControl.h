#pragma once
#include <QWidget>

#include <IParameter.h>

#include "TCFrameworkWidgetsExport.h"

namespace TC {
    class TCFrameworkWidgets_API ProcessorControl : public QWidget {
        Q_OBJECT
    public:
        ProcessorControl(QWidget* parent = nullptr);
        ~ProcessorControl();

        auto setProcessingParameter(IParameter::Pointer param)->void;
        auto setParameterValue(IParameter::Pointer param,QString algo_name = QString(),bool blockSig = true)->void;
        auto setParameterValue(IParameter::Pointer param,QString algo_name,QString organ_name)->void;
        auto clearInterface()->void;
        auto setHideExecute(bool isHide)->void;
        auto setHidePramExecute(bool isHide)->void;
        auto getParameter(QString algo_name)->IParameter::Pointer;
        auto getParameter()->IParameter::Pointer;//return processor's parameter
        auto setDisableControl(bool disable)->void;

        //for highlight buttons
        auto clearHighlights()->void;
        auto setProcHighlight(bool isHi, QString algo_name)->void;
        auto setDefaultHighlight()->void;        
        auto setWholeHighlight(bool isHi)->void;

        auto setConverter(QStringList disList,QStringList realList)->void;

        auto setCompetableMinSize(int size)->void;

    signals:
        void valueCall(double, QString);
        void pathCall(QString, QString);        
        void executeCall(QString);
        void executeWhole();

        void maskParamCall(QString);
        void highlightApply(bool);
        void refreshHighlight();

    protected slots:
        void OnParameterChangeCall(double, QString);
        void OnTableChangeCall(double,QString,QString,QString);
        void OnTableComboCall(int,QString,QString,QString);
        void OnPathChangeCall(QString, QString);
        void OnParamaterExecuteCall(QString);
        void OnProcessorExecuteCall();
        void OnCheckChangeCall(bool, QString);
        void OnComboChangeCall(int, QString);        

        //void OnNonDupChanged(int);
        void OnDupChanged();

    private:
        auto CreateParameterTable()->bool;
        auto ClearParameterTable()->bool;        

        auto parseProcessingAlgorithm()->bool;
        auto loadRequiredAlgorithm()->bool;
        auto arrangeProcessingAlgorithm()->void;
        auto AddDuplicator(const QString& key)->void;
        auto findDupKey(const QString& key)const-> QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}