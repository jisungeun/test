#pragma once
#include <QWidget>

#include <IMetaParameter.h>

#include "TCFrameworkWidgetsExport.h"

namespace TC {
    class TCFrameworkWidgets_API GridSelection : public QWidget {
        Q_OBJECT
    public:
        GridSelection(QWidget* parent = nullptr);
        ~GridSelection();

        auto SetSelectorName(const QString& name)->void;
        auto GetSelectorName()->QString;
        auto SetDisplayNames(const QStringList& dispNames)->void;
        auto SetAlgorithmNames(const QStringList& algoNames)->void;
        auto SetType(SelectorType type)->void;
        auto SetMaximumItemInRow(int item_count)->void;
        auto BuildInterface()->bool;

        auto GetCurrentStatus()->QList<bool>;        
        auto SetSelectionState(int idx, bool state)->void;
        auto GetAlgorithmNames()const->QStringList;

    signals:
        void sigSelection(QString,bool);

    public slots:
        void OnRadioBtnClicked();
        void OnCheckBoxClicked(int);

    private:
        auto InitUI()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}