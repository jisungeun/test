#pragma once

#include <memory>
#include <QGraphicsView>
#include <QWidget>

#include "TCFrameworkWidgetsExport.h"

namespace TC {
    class TCFrameworkWidgets_API ImageWidget : public QGraphicsView {
        Q_OBJECT

    public:
        ImageWidget(QWidget* parent = nullptr);
        ~ImageWidget();

        void SetImage(const QImage& image, bool fitting = false,bool late_load = false);
        void SetNo();
        void Clear();
        auto ShowImage(const bool &show)->void;
        void SetZoomable(bool able);

        auto GetZoom() const ->double;
        auto GetZoomPoint() const ->QPoint;
        void SetZoom(const QPoint& center, const double& factor);
        auto SetZoomFit(void)->void;

    private:
        auto eventFilter(QObject* object, QEvent* event)->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class TCFrameworkWidgets_API TCF2DWidget : public QWidget {
        Q_OBJECT

    public:
        enum ImageType {
            HT = 0,
            FL = 1,
            BF = 2,
            No = 3
        };

        typedef TCF2DWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        TCF2DWidget(QWidget* parent = nullptr);
        ~TCF2DWidget();

        bool SetTCFPath(const QString& path) const;
        auto GetTCFPath() const->QString;
        bool ShowImage(const ImageType& type, const int& frame = 0,bool late_load = false) const;        
        auto SetVisible(bool vis)->void;
        bool SetImageType(const ImageType& type) const;

        void SetPixmap(const QPixmap& pixmap) const;

        void SetLayout(QLayout* layout) const;

        void GetUsableType(bool& ht, bool& fl, bool& bf) const;
        auto GetTimeFrameCount(const ImageType& type) const ->int;

        auto GetSelectable() const ->bool;
        void SetSelectable(bool able) const;

        bool Selected() const;
        void SetSelected(bool selected);

        void KeepSquare(bool keep) const;

        void ZoomFit() const;

        void Clear() const;                

    signals:
        void toggled(bool);

    protected:
        void paintEvent(QPaintEvent*) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class TCFrameworkWidgets_API SimpleTCF2DViewer : public QWidget  {
        Q_OBJECT

    public:  
        typedef SimpleTCF2DViewer Self;
        typedef std::shared_ptr<Self> Pointer;

        SimpleTCF2DViewer(QWidget* parent=nullptr);
        ~SimpleTCF2DViewer();

        bool SetTCFPath(const QString& path) const;
        auto GetTCFPath() const ->QString;
        bool ShowImage(const TCF2DWidget::ImageType& type, const int& frame=0,bool late_load = false) const;        
        void ShowTextImage(const QString& text) const;
        void SetImageType(const TCF2DWidget::ImageType& type) const;

        void SetVisibleImageTypeControl(bool visible) const;
        void SetVisibleTimelapseControl(bool visible) const;

        auto GetSelectable() const ->bool;
        void SetSelectable(bool able) const;

        bool Selected() const;
        void SetSelected(bool selected);

        void forceVisible();

        void KeepSquare(bool keep) const;

        void ZoomFit() const;

        void Clear() const;

    signals:
        void toggled(bool);

    protected slots:
        void onImageTypeButtonToggled(int index) const;
        void onTimelapseChanged(int index) const;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}