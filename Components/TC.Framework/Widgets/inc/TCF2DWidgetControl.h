#pragma once

#include <memory>
#include <QImage>
#include <QString>

#include "TCFMetaReader.h"

namespace TC {
    class TCF2DWidgetControl {
    public:
        TCF2DWidgetControl();
        ~TCF2DWidgetControl();

        bool OpenTCF(const QString& path) const;
        void Reset();

        auto GetFilePath() const ->QString;
        void GetUsableType(bool& ht, bool& fl, bool& bf) const;

        auto LoadHTMIPData(const int& frameIndex, std::shared_ptr<unsigned char[]>& rawData, int& w, int& h) const ->bool;
        auto LoadFLMIPData(const int& frameIndex, std::shared_ptr<uint32_t[]>& rawData, int& w, int& h) const ->bool;
        auto LoadBFData(const int& frameIndex, std::shared_ptr<uint32_t[]>& rawData, int& w, int& h) const ->bool;

        auto GetHTMIPTimeFrame() const ->int;
        auto GetFLMIPTimeFrame() const ->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
