#pragma once
#include <QWidget>

#include <IUiParameter.h>
#include <IParameter.h>

#include <ParameterList.h>
#include <ScalarInput.h>
#include "StrCheckBox.h"
#include "TCFrameworkWidgetsExport.h"

class QTableWidgetItem;

namespace TC {
    class TCFrameworkWidgets_API ParamTable : public QWidget {
        Q_OBJECT
        typedef QMap<QString, QJsonValue::Type> TMap;

        typedef QMap<QString, ScalarInput*> ScalarMap;
        typedef QMap<QString, StrCheckBox*> CheckMap;
    public:
        ParamTable(QWidget* parent);
        ~ParamTable();

        auto SetName(const QString& name)->void;
        auto SetAlgoName(const QString& name)->void;
        auto BuildInterface()->void;
        auto SetTextConverter(QStringList disList, QStringList realList)->void;
        auto SetDuplicator(QStringList dupList)->void;
        auto SetHighlight(bool isHi)->void;

        auto SetHideExecute(bool isHide)->void;
        auto SetDisable(bool dis)->void;       

        auto SetParameter(IParameter::Pointer param)->void;
        auto SetParameterValue(IParameter::Pointer param, QString key = "default", bool blockSig = true, bool isOld = false) ->void;
        auto SetUiParameter(IUiParameter::Pointer uiParam)->void;

        auto GetParameter(QString key = "default")->IParameter::Pointer;

        auto GetScalarValue(QString name, QString key = "default")->double;
        auto SetScalarValue(QString name, double val, QString key = "default")->void;

    signals:
        void execute(QString,QString);
        void valChanged(double, QString, QString, QString,QString);
        void comboChanged(int, QString, QString, QString,QString);
        void naiveValChanged(double, QString,QString,QString);
        void enableChanged(bool, QString,QString,QString);

    public slots:
        void OnExecuteClicked();
        void OnTableItemChanged(QTableWidgetItem*);
        void OnComboBoxChanged(int);
        void OnInterfaceEnableChanged(QString, int);
        void OnValueChanged(double,QString);

    private:
        auto ParseParameter()->void;
        auto ParseUiParameter()->void;

        auto AddIntValue(ParameterNode node,int cols,int row)->void;
        auto AddDoubleValue(ParameterNode node,int cols,int row)->void;
        auto AddComboValue(ParameterNode node,int cols,int row,QString key)->void;

        auto AddNaiveControl(const QString& nodeName)->void;
        auto AddNaiveFilePath(ParameterNode node)->void;
        auto AddNaiveCheck(ParameterNode node)->void;
        auto AddNaiveIntValue(ParameterNode node)->void;
        auto AddNaiveDoubleValue(ParameterNode node)->void;
        auto AddNaiveComboValue(ParameterNode node)->void;

        auto SetCustomized(int, bool)->void;

        auto toDisplayName(QString name)->QString;
        auto toRealName(QString name)->QString;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}