#pragma once
//#include <QComboBox>
//#include <QCheckBox>
#include <QWidget>

#include <StrComboBox.h>
#include <StrCheckBox.h>
#include <IParameter.h>
#include <PathInput.h>
#include <RangeSlider.h>
#include <ScalarInput.h>
#include "ParameterList.h"
#include "TCFrameworkWidgetsExport.h"

namespace TC {
    class TCFrameworkWidgets_API ParameterControl : public QWidget {
        Q_OBJECT
        typedef QMap<QString, QJsonValue::Type> TMap;
        //interface 
        typedef QMap<QString, RangeSlider*> RangeMap;
        typedef QMap<QString, PathInput*> PathMap;
        typedef QMap<QString, ScalarInput*> ScalarMap;
        //typedef QMap<QString, QComboBox*> ComboMap;
        typedef QMap<QString, StrComboBox*> ComboMap;
        //typedef QMap<QString, QCheckBox*> CheckMap;
        typedef QMap<QString, StrCheckBox*> CheckMap;


    public:
        ParameterControl(QWidget* parent);
        ~ParameterControl();

        auto setProcessingAlgorithm(IParameter::Pointer param)->void;
        auto setAlgorithmValue(IParameter::Pointer param,bool blockSig = true)->void;
        auto buildInterface()->void;
        auto clearInterface()->void;

        auto setName(QString name)->void;
        auto setHideExecute(bool isHide)->void;
        auto getScalarValue(QString name)->double;
        auto setScalarValue(QString name, double val)->void;
        auto getStringValue(QString name)->QString;
        auto setStringValue(QString name, QString val)->void;
        auto getComboValue(QString name)->int;
        auto setComboValue(QString name, int val)->void;
        auto getCheckValue(QString name)->bool;
        auto setCheckValue(QString name, bool val)->void;
        auto setDisable(bool dis)->void;

        auto highlightBtn(bool isHi)->void;

        auto GetParameter()->IParameter::Pointer;

    signals:
        void valChanged(double,QString);
        void pathChanged(QString, QString);
        void checkChanged(bool,QString);
        void comboChanged(int, QString);
        void execute(QString);

    public slots:
        void OnValueChanged(double,QString);
        void OnInterfaceEnableChanged(QString,int);
        void OnSetterChanged(QString,int);
        void OnSetterApply(QString,int);
        void OnExecuteClicked();
        void OnPathChanged(QString, QString);

    private:
        auto parseParameter()->bool;
        auto addDoubleRange(ParameterNode node)->void;
        auto addIntRange(ParameterNode node)->void;
        auto addDoubleValue(ParameterNode node)->void;
        auto addIntValue(ParameterNode node)->void;
        auto addComboValue(ParameterNode node,bool hide)->void;
        auto addPath(ParameterNode node)->void;
        auto addFile(ParameterNode node)->void;
        auto addCheck(ParameterNode node)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}