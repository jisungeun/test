#pragma once

#include <memory>

#include <QJsonValue>

#include "TCFrameworkWidgetsExport.h"

namespace TC {
    class TCFrameworkWidgets_API ParamConverter {
    public:
        typedef ParamConverter Self;
        typedef std::shared_ptr<Self> Pointer;

        ParamConverter();

        virtual ~ParamConverter();

        static auto GetInstance()->Pointer;

        auto ConvertName(const QString& name)->QString;
        auto ConvertUnit(const QString& unit)->QString;        

    private:
        auto BuildNameTable()->void;        
        auto BuildUnitTable()->void;

        struct Impl;
        std::shared_ptr<Impl> d;
    };
}