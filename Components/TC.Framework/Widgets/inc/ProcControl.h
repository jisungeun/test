#pragma once
#include <QWidget>

#include <IParameter.h>
#include <IMetaParameter.h>

#include "TCFrameworkWidgetsExport.h"

namespace TC {
	class TCFrameworkWidgets_API ProcControl : public QWidget {
		Q_OBJECT
	public:
		ProcControl(QWidget* parent = nullptr);
		~ProcControl();

		auto SetProcessingParameter(IParameter::Pointer param)->void;
		auto SetMetaParameter(IMetaParameter::Pointer meta)->void;
		auto BuildInterface()->void;
	    auto ClearInterface()->void;

		auto SetParameterValue(IParameter::Pointer param, QString algo_name = QString(), bool blockSig = true)->void;
		auto SetParameterValue(IParameter::Pointer param, QString algo_name, QString organ_name)->void;		
		auto SetHideExecute(bool isHide)->void;
		auto SetHideParamExecute(bool isHide)->void;
		auto GetParameter(QString algo_name,QString dup_name = QString())->IParameter::Pointer;
		auto GetParameter()->IParameter::Pointer;
		auto GetMetaParameter()->IMetaParameter::Pointer;
		auto GetConnections()->QMap<QString, std::tuple<QString, QStringList>>&;

		auto SetDisableControl(bool disable)->void;

		auto ClearHighlights()->void;
		auto SetProcHighlight(bool isHi, QString algo_name)->void;
		auto SetNextHighlight(QString algo_name)->void;
		auto SetDefaultHighlight()->void;
		auto SetWholeHighlight(bool isHi)->void;

		auto SetConverter(QStringList disList, QStringList realList)->void;
		auto SetCompetableMinSize(int size)->void;
	signals:
		void valueCall(double, QString);
		void executeCall(QString,QString);
		void executeWhole();

		void maskParamCall(QString);
		void highlightApply(bool);
		void refreshHighlight();		


	protected slots:
		void OnParamaterExecuteCall(QString, QString);

		void OnTableComboCall(int, QString, QString, QString,QString);
		void OnTableChangeCall(double, QString, QString, QString,QString);
		void OnTableValueCall(double, QString,QString,QString);
		void OnTableEnableCall(bool, QString,QString,QString);
			
		void OnPathChangeCall(QString, QString,QString,QString);
		void OnParameterChangeCall(double, QString,QString,QString);
		void OnCheckChangeCall(bool, QString,QString,QString);
		void OnComboChangeCall(int, QString,QString,QString);
		void OnProcessorExecuteCall();
		void OnAlgorithmToggle(QString, bool);

	private:
		auto ParseProcessingParameter()->void;
		auto ParseMetaParameter()->void;
		auto LoadRequiredAlgorithms()->void;
		auto ArrangeProcessingAlgorithm()->void;

		struct Impl;
		std::unique_ptr<Impl> d;		
	};
}