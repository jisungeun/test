#pragma once

#include "ImageRaw.h"
#include "VolumeData.h"
#include "VolumeAccessor.h"

#include "TCFVolumeAccessor.h"
#include "OivVolumeAccessor.h"

#include "VolumeViz/nodes/SoVolumeData.h"

struct ImageRaw::Impl {
    //fix default image scalar type as unsigned short
    std::shared_ptr<unsigned short> buffer;
    //unsigned short* buffer;

    int type; //0:From file 1: From Inventor Node

    float zOffset = {0.0};

    //whole image data holder
    //TC::IO::VolumeData::Pointer image;        

    //image meta information holder
    TC::IO::VolumeMetaInfo::Pointer info;

    //image pointer holder
    SoVolumeData* volume;

    //image accessors
    //access single voxel or part of image from file system
    TC::IO::TCFVolumeAccessor* tcfAcc;
    TC::IO::OivVolumeAccessor* oivAcc;
};

ImageRaw::ImageRaw() : IBaseImage(), d{ new Impl } {
    //d->image = new TC::IO::VolumeData;    
    d->tcfAcc = new TC::IO::TCFVolumeAccessor;
    d->oivAcc = new TC::IO::OivVolumeAccessor;
}

ImageRaw::~ImageRaw() {
    //delete d->image;    
    delete d->tcfAcc;
    delete d->oivAcc;
}

auto ImageRaw::New(TC::IO::VolumeMetaInfo::Pointer meta) -> IBaseImage::Pointer {
    Pointer image{ new ImageRaw() };
    image->d->info = meta;
    return image;
}

auto ImageRaw::New(SoVolumeData* vol) -> IBaseImage::Pointer {
    Pointer image{ new ImageRaw() };
    image->d->volume = vol;
    return image;
}


auto ImageRaw::clone()->IBaseImage* {
    auto image = new ImageRaw();
    image->d->info = d->info;
    return image;
}

auto ImageRaw::addOffset(const float offset)->void {
    d->zOffset = d->zOffset + offset;
}

auto ImageRaw::getValue() -> int {
    return 1;
}

//Get Whole Image
auto ImageRaw::GetBuffer() -> void* {
    auto type = d->info->GetFileType();
    using imageType = TC::IO::VolumeMetaInfo::FType;
    uint32_t x, y, z;
    auto dims = d->info->GetDimension();
    x = std::get<0>(dims);
    y = std::get<1>(dims);
    z = std::get<2>(dims);
    if(type == imageType::TCFType) {        
        d->tcfAcc->OpenFile();
        d->tcfAcc->SetROI(0, x - 1, 0, y - 1, 0, z - 1);
        d->buffer = d->tcfAcc->GetBufferAsUShort();
        d->tcfAcc->CloseFile();
    }else if(type == imageType::RAWType){
        //nothing
    }else if (type== imageType::LDMTCFType) {        
        d->oivAcc->SetVolumeData(d->volume);
        d->oivAcc->SetDimensions(x, y, z);
        d->oivAcc->SetRegion(0, x - 1, 0, y - 1, 0, z - 1);
        d->buffer = d->oivAcc->GetBufferAsUShort();
    }
    return d->buffer.get();
}

//Get Part of image
auto ImageRaw::GetBuffer(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void* {
    auto type = d->info->GetFileType();
    using imageType = TC::IO::VolumeMetaInfo::FType;
    if (type == imageType::TCFType) {
        d->tcfAcc->OpenFile();
        d->tcfAcc->SetROI(xMin,xMax,yMin,yMax,zMin,zMax);
        d->buffer = d->tcfAcc->GetBufferAsUShort();
        d->tcfAcc->CloseFile();
    } else if (type == imageType::RAWType) {
        //nothing
    } else if (type == imageType::LDMTCFType) {
        d->oivAcc->SetVolumeData();
        d->oivAcc->SetRegion(xMin,xMax,yMin,yMax,zMin,zMax);        
        d->buffer = d->oivAcc->GetBufferAsUShort();
    }
    return d->buffer.get();
}

auto ImageRaw::GetVolume(bool fromReader) -> SoVolumeData* {
    if (fromReader)
        return d->oivAcc->GetVolumeData();
    else
        return d->volume;
}

auto ImageRaw::SetMetaInfo(TC::IO::VolumeMetaInfo::Pointer info) -> void {
    d->info = info;
    auto type = d->info->GetFileType();
    using imageType = TC::IO::VolumeMetaInfo::FType;
    uint32_t x, y, z;
    auto dims = d->info->GetDimension();
    x = std::get<0>(dims);
    y = std::get<1>(dims);
    z = std::get<2>(dims);
    if(type == imageType::TCFType) {                
        d->tcfAcc->SetDimension(x, y, z);
        d->tcfAcc->SetFilePath(d->info->GetFilePath());                
    }else if(type == imageType::RAWType){
        //no implementation yet 2020.06.15 Jose T. Kim
    }else if(type == imageType::LDMTCFType){
        d->oivAcc->SetDimensions(x, y, z);
        d->oivAcc->SetFilePath(d->info->GetFilePath());
    }
}

auto ImageRaw::GetMetaInfo() -> TC::IO::VolumeMetaInfo::Pointer {
    return d->info;
}
