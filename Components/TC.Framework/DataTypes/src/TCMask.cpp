#include <QMap>
#include "TCMask.h"

#include <iostream>


struct MaskBlob::Impl {
	struct {
		int x, y, z;
	} center{ 0, 0, 0 };

	struct {
		int x0, y0, z0;
		int x1, y1, z1;
	} bbox{ 0, 0, 0, 1, 1, 1 };
	int code{ 0 };

	VolumePtr maskVolume{ nullptr };
};

MaskBlob::MaskBlob() : d{ new Impl } {
}

MaskBlob::MaskBlob(const MaskBlob& other) : d{ new Impl } {
	*d = *other.d;
}

MaskBlob::~MaskBlob() {

}

MaskBlob& MaskBlob::operator=(const MaskBlob& other) {
	*d = *other.d;
	return *this;
}

auto MaskBlob::GetCenter() const -> std::tuple<int, int, int> {
	return std::make_tuple(d->center.x, d->center.y, d->center.z);
}

auto MaskBlob::SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1) -> void {
	d->bbox.x0 = x0;
	d->bbox.y0 = y0;
	d->bbox.z0 = z0;
	d->bbox.x1 = x1;
	d->bbox.y1 = y1;
	d->bbox.z1 = z1;

	d->center.x = (d->bbox.x0 + d->bbox.x1) / 2;
	d->center.y = (d->bbox.y0 + d->bbox.y1) / 2;
	d->center.z = (d->bbox.z0 + d->bbox.z1) / 2;
}

auto MaskBlob::GetBoundingBox() const -> std::tuple<int, int, int, int, int, int> {
	return std::make_tuple(d->bbox.x0, d->bbox.y0, d->bbox.z0, d->bbox.x1, d->bbox.y1, d->bbox.z1);
}

auto MaskBlob::SetCode(int code) -> void {
	d->code = code;
}

auto MaskBlob::GetCode() const -> int {
	return d->code;
}

auto MaskBlob::SetMaskVolume(VolumePtr volume) -> void {
	d->maskVolume = volume;
}

auto MaskBlob::GetMaskVolume() -> VolumePtr {
	return d->maskVolume;
}

struct TCMask::Impl {
	QMap<int, MaskBlob> blobs;
	struct {
		int x, y, z;
	}size{ 1,1,1 };
	struct {
		double x, y, z;
	}res{ 1.0,1.0,1.0 };
	MaskTypeEnum type{ MaskTypeEnum::None };
	QStringList names;
	bool valid{ false };
	int time_step{ 0 };
	int maxLabelValue{ 0 };
	float zOffset{ 0.0 };

	QString typeText;
	QString maskName;
	QString path;

	VolumePtr maskVolume;
};
TCMask::TCMask() : d{ new Impl } {

}
TCMask::TCMask(const TCMask& other) : d{ new Impl } {
	*d = *other.d;
}

TCMask::~TCMask() {

}
auto TCMask::SetName(QString name) -> void {
	d->maskName = name;
}

auto TCMask::GetName() -> QString {
	return d->maskName;
}
auto TCMask::SetPath(const QString& path) -> void {
	d->path = path;
}

auto TCMask::GetPath() -> QString {
	return d->path;
}

auto TCMask::SetTimeStep(int step) -> void {
	d->time_step = step;
}
auto TCMask::GetTimeStep() -> int {
	return d->time_step;
}
auto TCMask::SetType(MaskTypeEnum type) -> void {
	d->type = type;
}
auto TCMask::GetType() -> MaskTypeEnum {
	return d->type;
}
auto TCMask::SetTypeText(const QString& type) -> void {
	d->typeText = type;
}
auto TCMask::GetTypeText() -> QString {
	return d->typeText;
}
auto TCMask::AppendBlob(int index, MaskBlob& blob,QString name) -> void {
	if (d->names.size() < 1) {
		if(name.isEmpty()) {
			d->names.push_back("cellInst");
		}
		else if(false == d->names.contains(name)){
			d->names.push_back(name);
		}
	}
	d->blobs[index] = blob;
	SetValid(true);
}
auto TCMask::SetLayerName(int index, QString name) -> void {
    if(d->names.count() == index) {//FL label
		d->names.push_back(name);
    }else if(d->names.count() > index){
		d->names[index] = name;
    }
}
auto TCMask::AppendLayer(QString name, MaskBlob& blob, int index) -> void {
	if (index > -1) {
		//Do something
	}
	auto idx = FindIndex(name);
	if (idx < 0) {
		idx = d->names.count();
		d->names.push_back(name);
	}
	d->blobs[idx] = blob;
	SetValid(true);
}
auto TCMask::GetLayerNames() -> QStringList {
	return d->names;
}

auto TCMask::GetBlobIndexes() const -> QList<int> {
	return d->blobs.keys();
}

auto TCMask::GetBlob(int index) const -> MaskBlob {
	if (!d->blobs.contains(index)) return MaskBlob();
	return d->blobs[index];
}
auto TCMask::GetBlob(QString name) const -> MaskBlob {
	auto idx = FindIndex(name);
	return d->blobs[idx];
}

auto TCMask::CountBlobs() const -> int {
	return d->blobs.size();
}
auto TCMask::GetTimeSteps() const -> int {
	return d->blobs.size();
}
auto TCMask::IsValid() const -> bool {
	return d->valid;
}

auto TCMask::SetValid(bool valid) -> void {
	d->valid = valid;
}

auto TCMask::UpdateCode(int index, int code) -> bool {
	if (!d->blobs.contains(index)) return false;
	d->blobs[index].SetCode(code);
	return true;
}

auto TCMask::GetMaskVolume(int index) const -> std::shared_ptr<uint8_t[]> {
	if (!d->blobs.contains(index)) return nullptr;
	return d->blobs[index].GetMaskVolume();
}
auto TCMask::FindIndex(QString key)const -> int {
	int idx = -1;
	for (auto i = 0; i < d->names.size(); i++) {
		if (key.compare(d->names[i]) == 0) {
			idx = i;
			break;
		}
	}
	return idx;
}
auto TCMask::GetLayerBlob(QString name) const -> MaskBlob {
	auto idx = FindIndex(name);
	if (idx < 0) {
		return MaskBlob();
	}
	if (!d->blobs.contains(idx)) {
		return MaskBlob();
	}
	return d->blobs[idx];
}

auto TCMask::GetMaskLayer(QString name) const -> MaskBlob::VolumePtr {
	auto idx = FindIndex(name);
	if (idx < 0) {
		return nullptr;
	}
	if (!d->blobs.contains(idx)) {
		return nullptr;
	}
	std::shared_ptr<unsigned char[]> result{ new unsigned char[d->size.x * d->size.y * d->size.z](),std::default_delete<unsigned char[]>() };
	auto bb = d->blobs[idx].GetBoundingBox();
	auto sizeX = std::get<3>(bb) - std::get<0>(bb) + 1;
	auto sizeY = std::get<4>(bb) - std::get<1>(bb) + 1;

	auto maskVol = d->blobs[idx].GetMaskVolume();
	for (auto k = std::get<2>(bb); k < std::get<5>(bb); k++) {
		for (auto j = std::get<1>(bb); j < std::get<4>(bb); j++) {
		    for (auto i = std::get<0>(bb); i < std::get<3>(bb); i++) {		
				auto temp_idx = k * d->size.y * d->size.x + j * d->size.y + i;
				auto blobX = i - std::get<0>(bb);
				auto blobY = j - std::get<1>(bb);
				auto blobZ = k - std::get<2>(bb);
				auto maskIdx = blobZ * sizeY * sizeX + blobY * sizeX + blobX;
				auto val = *(maskVol.get() + maskIdx);
				*(result.get() + temp_idx) = val;
			}
		}
	}
	return result;
}

auto TCMask::GetMaskVolume() const -> VolumePtr {
	//for all mask blobs, merge them into single multi-labeled ushort mask
	if (d->blobs.size() < 1) return nullptr;
	//create empty mask , default value is zero by adding ()
	std::shared_ptr<unsigned short[]> volume(new unsigned short[d->size.x * d->size.y * d->size.z](), std::default_delete<unsigned short[]>());

	for (auto& blob : d->blobs) {
		//for every blob set specific value by using code
		auto val = blob.GetCode();
		auto bb = blob.GetBoundingBox();
		auto blobSizeX = std::get<3>(bb) - std::get<0>(bb) + 1;
		auto blobSizeY = std::get<4>(bb) - std::get<1>(bb) + 1;
		auto count = 0;
		for (int k = std::get<2>(bb); k < std::get<5>(bb) + 1; k++) {
			for (int j = std::get<1>(bb); j < std::get<4>(bb) + 1; j++) {
			    for (int i = std::get<0>(bb); i < std::get<3>(bb) + 1; i++) {				
					int idx = k * d->size.y * d->size.x + j * d->size.x + i;
					int blobX = i - std::get<0>(bb);
					int blobY = j - std::get<1>(bb);
					int blobZ = k - std::get<2>(bb);
					int blobIdx = blobZ * blobSizeY * blobSizeX + blobY * blobSizeX + blobX;
					uint8_t blobVal;
					
					blobVal = *(blob.GetMaskVolume().get() + blobIdx);
					if (blobVal > 0) {
						*(volume.get() + idx) = val;
						count++;
					}
				}
			}
		}
	}

	d->maskVolume = volume;
	return d->maskVolume;
}

auto TCMask::AppendLayerVolume(QString name, MaskBlob::VolumePtr maskVolume, int index) -> void {
	auto idx = index;
	idx = FindIndex(name);
	d->blobs[idx].SetMaskVolume(maskVolume);
}


auto TCMask::AppendMaskVolume(int index, MaskBlob::VolumePtr maskVolume)->void {
	d->blobs[index].SetMaskVolume(maskVolume);
}

auto TCMask::SetMaxLabelValue(int value) -> void {
	d->maxLabelValue = value;
}

auto TCMask::GetMaxLabelValue() -> int {
	return d->maxLabelValue;
}

auto TCMask::SetSize(int x, int y, int z) -> void {
	d->size.x = x;
	d->size.y = y;
	d->size.z = z;
	//d->size.x = 683;
	//d->size.y = 489;
	//d->size.z = 46;
}
auto TCMask::GetSize() -> std::tuple<int, int, int> {
	return std::make_tuple(d->size.x, d->size.y, d->size.z);
}
auto TCMask::SetResolution(double res[3]) -> void {
	d->res.x = res[0];
	d->res.y = res[1];
	d->res.z = res[2];
}
auto TCMask::GetResolution() -> std::tuple<double, double, double> {
	return std::make_tuple(d->res.x, d->res.y, d->res.z);
}

auto TCMask::addOffset(const float offset) -> void {
	d->zOffset = offset;
}

auto TCMask::SetOffset(float offset) -> void {
	d->zOffset = offset;
}

auto TCMask::GetOffset() const -> float {
	return d->zOffset;
}

auto TCMask::getValue() -> unsigned short* {
	return GetMaskVolume().get();
}

auto TCMask::clone() -> IBaseMask* {
	auto mask = std::make_shared<TCMask>();
	*mask->d = *d;
	return mask.get();
}
