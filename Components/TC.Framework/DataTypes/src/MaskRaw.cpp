#include <QVector>

#include "MaskRaw.h"

struct MaskRaw::Impl {
    unsigned short* buf{nullptr};
    QVector<SoVolumeData*> volData;
    QVector<SoVolumeData*> refData;
    float zOffset = {0.0};
    MASK_VALUE mask_value {MASK_VALUE::NONE};
    int layer_num{ 0 };
    QVector<QString> name;
};

MaskRaw::MaskRaw() : IBaseMask(), d{ new Impl } {
}

MaskRaw::~MaskRaw() {
    d->volData.clear();
    d->refData.clear();
    if(nullptr!=d->buf) {
        delete d->buf;
    }
}

auto MaskRaw::GetLayerNames() -> QStringList {
    return QStringList();
}


auto MaskRaw::New(unsigned short* value) -> IBaseMask::Pointer {
    Pointer mask{ new MaskRaw() };
    mask->d->buf = value;
    return mask;
}

auto MaskRaw::New(SoVolumeData* vol) -> IBaseMask::Pointer {
    Pointer mask{ new MaskRaw() };
    mask->d->volData.push_back(vol);
    mask->d->layer_num = 1;
    mask->d->name.push_back("default");
    return mask;
}

auto MaskRaw::New(QVector<SoVolumeData*> vols) -> IBaseMask::Pointer {
    Pointer mask{ new MaskRaw() };
    mask->d->volData = vols;
    mask->d->layer_num = vols.size();
    for (int i = 0; i < vols.size(); i++) {
        mask->d->name.push_back("default");
    }
    return mask;
}

auto MaskRaw::setLayerName(int idx, QString name) -> void {
    d->name[idx] = name;
}


auto MaskRaw::getReferenceData(int idx) -> SoVolumeData* {
    if (d->refData.size() > idx)
        return d->refData[idx];
    else
        return d->refData[0];
}

auto MaskRaw::setReferenceData(SoVolumeData* refVol) -> void {
    d->refData.push_back(refVol);
}

auto MaskRaw::setReferenceData(QVector<SoVolumeData*> refs) -> void {
    d->refData = refs;
}


auto MaskRaw::setType(MASK_VALUE mv) -> void {
    d->mask_value = mv;
}

auto MaskRaw::getType() -> MASK_VALUE {
    return d->mask_value;
}
auto MaskRaw::clone()->IBaseMask* {
    auto mask = new MaskRaw();
    mask->d->buf = d->buf;
    return mask;
}

auto MaskRaw::addOffset(const float offset) -> void {
    d->zOffset = d->zOffset + offset;
}

auto MaskRaw::getValue() -> unsigned short*{
    return d->buf;
}

auto MaskRaw::getLayerNum() -> int {
    return d->layer_num;
}

auto MaskRaw::getLayerNames()->QVector<QString> {
    return d->name;
}

auto MaskRaw::getVolume(int idx) -> SoVolumeData* {
    if (d->volData.size() > idx && idx > -1) {
        return d->volData[idx];
    }
    return nullptr;
}

auto MaskRaw::getVolumes(void)->QVector<SoVolumeData*>
{
    return d->volData;
}