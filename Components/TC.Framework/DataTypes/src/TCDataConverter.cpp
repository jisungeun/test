#define LOGGER_TAG "[TCDataConverter]"
#include <TCLogger.h>

#include "TCDataConverter.h"

#include <ioformat/IOFormat.h>
#include <iolink/view/ImageViewProvider.h>
#include <iolink/view/ImageViewFactory.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)


using namespace imagedev;
using namespace ioformat;
using namespace iolink;


struct TCDataConverter::Impl {};

TCDataConverter::TCDataConverter() : d { new Impl } {}

TCDataConverter::~TCDataConverter() { }

auto TCDataConverter::MaskToSoVolumeData(TCMask::Pointer mask, QString name) -> SoVolumeData* {
	SoVolumeData* volumeData { nullptr };

	switch (mask->GetType()) {
		case MaskTypeEnum::MultiLabel:
			volumeData = MtoSoCellLabel(mask);
			break;
		case MaskTypeEnum::MultiLayer:
			volumeData = MtoSoOrganBinary(mask, name);
			break;
		case MaskTypeEnum::None:
			volumeData = MtoSoCellBinary(mask);
			break;
	}

	return volumeData;
}

auto TCDataConverter::SoVolumeDataToMask(QList<SoVolumeData*> volData, MaskTypeEnum type, QStringList names) -> TCMask::Pointer {
	std::shared_ptr<TCMask> result { nullptr };
	QString name = QString();
	switch (type) {
		case MaskTypeEnum::MultiLayer:
			result = SoToMOrganBinary(volData, names);
			break;
		case MaskTypeEnum::MultiLabel:
			if (!names.isEmpty()) {
				name = names[0];
			}
			result = SoToMCellLabel(volData[0], name);
			break;
		case MaskTypeEnum::MultiLayerLabel:
			result = SoToMOrganLabel(volData, names);
			break;
		case MaskTypeEnum::None:
			if (!names.isEmpty()) {
				name = names[0];
			}
			result = SoToMCellBinary(volData[0], name);
			break;
	}
	return result;
}


auto TCDataConverter::MtoSoCellBinary(TCMask::Pointer mask) -> SoVolumeData* {
	const auto blobIdx = mask->GetBlobIndexes();
	auto blob = mask->GetBlob(blobIdx[0]);

	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto volume = new unsigned short[static_cast<size_t>(dimX * dimY * dimZ)]();

	SbDataType type = SbDataType::UNSIGNED_SHORT;
	SoVolumeData* soVol = new SoVolumeData;
	soVol->ref();

	soVol->data.setValue(SbVec3i32(dimX, dimY, dimZ), type, type.getNumBits(), volume, SoSFArray::CopyPolicy::NO_COPY_AND_DELETE);
	if (resZ == 0) {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -0.5, dimX * resX / 2, dimY * resY / 2, 0.5));
	} else {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2, dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));
	}

	const auto [x0, y0, z0, x1, y1, z1] = blob.GetBoundingBox();
	const auto bBox = SbBox3i32(SbVec3i32(x0, y0, z0), SbVec3i32(x1, y1, z1));

	const auto dataInfoBox = soVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

	const SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize(static_cast<size_t>(dataInfoBox.bufferSize));

	soVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	const auto buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

	const auto blobVolume = blob.GetMaskVolume();
	std::copy_n(blobVolume.get(), dataBufferObj->getSize() / sizeof(unsigned short), buffer);
	dataBufferObj->unmap();

	int transaction;
	soVol->startEditing(transaction);
	soVol->editSubVolume(bBox, dataBufferObj.ptr());
	soVol->finishEditing(transaction);
	soVol->saveEditing();

	soVol->unrefNoDelete();

	return soVol;
}

auto TCDataConverter::MtoSoOrganBinary(TCMask::Pointer mask, QString name) -> SoVolumeData* {
	auto blob = mask->GetBlob(name);
	if (nullptr == blob.GetMaskVolume()) {
		return nullptr;
	}

	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto volume = new unsigned short[static_cast<size_t>(dimX * dimY * dimZ)]();

	SbDataType type = SbDataType::UNSIGNED_SHORT;
	SoVolumeData* soVol = new SoVolumeData;
	soVol->ref();

	soVol->data.setValue(SbVec3i32(dimX, dimY, dimZ), type, type.getNumBits(), volume, SoSFArray::CopyPolicy::NO_COPY_AND_DELETE);
	if (resZ == 0) {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -0.5, dimX * resX / 2, dimY * resY / 2, 0.5));
	} else {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2, dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));
	}

	const auto [x0, y0, z0, x1, y1, z1] = blob.GetBoundingBox();
	const auto bBox = SbBox3i32(SbVec3i32(x0, y0, z0), SbVec3i32(x1, y1, z1));

	SoLDMDataAccess::DataInfoBox dataInfoBox = soVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize(static_cast<size_t>(dataInfoBox.bufferSize));
	soVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	const auto buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

	const auto blobVolume = blob.GetMaskVolume();
	std::copy_n(blobVolume.get(), dataBufferObj->getSize() / sizeof(unsigned short), buffer);
	dataBufferObj->unmap();

	int transaction;
	soVol->startEditing(transaction);
	soVol->editSubVolume(bBox, dataBufferObj.ptr());
	soVol->finishEditing(transaction);
	soVol->saveEditing();

	soVol->unrefNoDelete();

	return soVol;
}

auto TCDataConverter::MtoSoCellLabel(TCMask::Pointer mask) -> SoVolumeData* {
	const auto blob_idxs = mask->GetBlobIndexes();

	//create emtpy SoVolumeData using size of whole mask volume
	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto volume = new unsigned short[static_cast<size_t>(dimX * dimY * dimZ)]();

	const SbDataType type = SbDataType::UNSIGNED_SHORT;
	SoVolumeData* soVol = new SoVolumeData;
	soVol->ref();

	soVol->data.setValue(SbVec3i32(dimX, dimY, dimZ), type, type.getNumBits(), volume, SoSFArray::CopyPolicy::NO_COPY_AND_DELETE);
	if (resZ == 0) {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -0.5, dimX * resX / 2, dimY * resY / 2, 0.5));
	} else {
		soVol->extent.setValue(SbBox3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2, dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));
	}

	for (const auto& idx : blob_idxs) {
		auto blob = mask->GetBlob(idx);
		const auto code = blob.GetCode();

		const auto [x0, y0, z0, x1, y1, z1] = blob.GetBoundingBox();
		const auto bBox = SbBox3i32(SbVec3i32(x0, y0, z0), SbVec3i32(x1, y1, z1));

		SoLDMDataAccess::DataInfoBox dataInfoBox = soVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

		SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
		dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
		soVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

		const auto buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_WRITE));
		auto blobVolume = blob.GetMaskVolume();
		for (size_t i = 0; i < dataBufferObj->getSize() / sizeof(unsigned short); i++) {
			buffer[i] += static_cast<unsigned short>(*(blobVolume.get() + i)) * static_cast<unsigned short>(code);
		}

		dataBufferObj->unmap();

		int transaction;
		soVol->startEditing(transaction);
		soVol->editSubVolume(bBox, dataBufferObj.ptr());
		soVol->finishEditing(transaction);
	}
	soVol->saveEditing();

	soVol->unrefNoDelete();

	return soVol;
}

auto TCDataConverter::SoToMCellBinary(SoVolumeData* vol, QString name) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();

	SoRef<SoVolumeData> SoVol = vol;
	auto res = SoVol->getVoxelSize();
	double ress[3] = { res[0], res[1], res[2] };

	auto dim = SoVol->getDimension();
	auto size = SoVol->getVoxelSize();
	SbBox3i32 bBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		SoVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	SoVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	unsigned short* buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

	const uint64_t rowCount = dim[0];
	const uint64_t colCount = dim[1];
	const uint64_t sliCount = dim[2];

	VectorXu64 imageShape { rowCount, colCount, sliCount };
	Vector3d spacing { size[0], size[1], size[2] };
	Vector3d origin { -dim[0] * size[0] / 2, -dim[1] * size[1] / 2, -dim[2] * size[2] / 2 };

	auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
	auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
	setDimensionalInterpretation(image, ImageTypeId::VOLUME);

	VectorXu64 imageOrig { 0, 0, 0 };
	RegionXu64 imageRegion { imageOrig, imageShape };

	image->writeRegion(imageRegion, buffer);

	dataBufferObj->unmap();

	auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_16_BIT);

	resultMask->SetResolution(ress);
	resultMask->SetSize(dim[0], dim[1], dim[2]);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::None);
	//resultMask->SetTimeStep(0);

	///////////Open Inventor Labeling Part//////////
	auto analysis = std::make_shared<AnalysisMsr>();
	auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
	auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
	auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

	auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
	auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
	auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

	labelAnalysis(label_image, image, analysis);

	auto x0 = bb_ox->value(0);
	auto y0 = bb_oy->value(0);
	auto z0 = bb_oz->value(0);
	auto xSize = bb_dx->value(0);
	auto ySize = bb_dy->value(0);
	auto zSize = bb_dz->value(0);

	SbVec3f start(x0, y0, z0);
	SbVec3f end(x0 + xSize, y0 + ySize, z0 + zSize);

	auto startCoord = SoVol->XYZToVoxel(start);
	auto endCoord = SoVol->XYZToVoxel(end);

	int startIdx[3] = { static_cast<int>(startCoord[0]), static_cast<int>(startCoord[1]), static_cast<int>(startCoord[2]) };
	int endIdx[3] = { static_cast<int>(endCoord[0] - 1), static_cast<int>(endCoord[1] - 1), static_cast<int>(endCoord[2] - 1) };

	auto vSizeX = endIdx[0] - startIdx[0] + 1;
	auto vSizeY = endIdx[1] - startIdx[1] + 1;
	auto vSizeZ = endIdx[2] - startIdx[2] + 1;

	SbBox3i32 bBox2(SbVec3i32(startIdx[0], startIdx[1], startIdx[2]), SbVec3i32(endIdx[0], endIdx[1], endIdx[2]));
	SoLDMDataAccess::DataInfoBox dataInfoBox2 =
		SoVol->getLdmDataAccess().getData(0, bBox2, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj2 = new SoCpuBufferObject;
	dataBufferObj2->setSize((size_t)dataInfoBox2.bufferSize);

	SoVol->getLdmDataAccess().getData(0, bBox2, dataBufferObj2.ptr());
	unsigned short* data = (unsigned short*)dataBufferObj2->map(SoBufferObject::READ_ONLY);

	MaskBlob blob;
	blob.SetBoundingBox(startIdx[0], startIdx[1], startIdx[2], endIdx[0], endIdx[1], endIdx[2]);
	std::shared_ptr<unsigned char[]> blobVolume(new unsigned char[static_cast<long long>(vSizeX) * vSizeY * vSizeZ](), std::default_delete<unsigned char[]>());

	std::copy_n(data, static_cast<long long>(vSizeX) * vSizeY * vSizeZ, blobVolume.get());
	dataBufferObj->unmap();

	blob.SetMaskVolume(blobVolume);
	if (name.isEmpty())
		resultMask->AppendLayer("default", blob);
	else
		resultMask->AppendLayer(name, blob);

	return resultMask;
}

auto TCDataConverter::ArrToLabelMask(unsigned short* mask, int dim[3], double resolution[3], QString name) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();

	const uint64_t rowCount = dim[0];
	const uint64_t colCount = dim[1];
	const uint64_t sliCount = (dim[2] > 1) ? dim[2] : 1;

	resultMask->SetResolution(resolution);
	resultMask->SetSize(rowCount, colCount, sliCount);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::MultiLabel);

	VectorXu64 imageShape { rowCount, colCount, sliCount };
	Vector3d spacing { 1, 1, 1 };
	Vector3d origin { 0, 0, 0 };

	try {
		auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
		auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
		setDimensionalInterpretation(image, ImageTypeId::VOLUME);

		VectorXu64 imageOrig { 0, 0, 0 };
		RegionXu64 imageRegion { imageOrig, imageShape };

		image->writeRegion(imageRegion, mask);

		auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_16_BIT);

		///////////Open Inventor Labeling Part//////////
		auto analysis = std::make_shared<AnalysisMsr>();
		auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
		auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
		auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

		auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
		auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
		auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

		auto mean = analysis->select(NativeMeasurements::intensityMean);

		labelAnalysis(label_image, image, analysis);

		auto numLabel = analysis->labelCount();
		auto maxLabel = -1;

		for (auto label = 0; label < numLabel; label++) {
			auto x0 = static_cast<int>(bb_ox->value(label));
			auto y0 = static_cast<int>(bb_oy->value(label));
			auto z0 = static_cast<int>(bb_oz->value(label));
			auto xSize = static_cast<int>(bb_dx->value(label));
			auto ySize = static_cast<int>(bb_dy->value(label));
			auto zSize = static_cast<int>(bb_dz->value(label));
			auto code = static_cast<int>(mean->value(label));

			if (code == 0)
				continue;
			if(maxLabel < code) {
				maxLabel = code;
			}
			MaskBlob blob;
			blob.SetCode(code);
			blob.SetBoundingBox(x0, y0, z0, x0 + xSize - 1, y0 + ySize - 1, z0 + zSize - 1);

			std::shared_ptr<uint8_t[]> blobVolume(new uint8_t[xSize * ySize * zSize](), std::default_delete<uint8_t[]>());

			std::shared_ptr<ImageView> cropped_image { nullptr };
			if (sliCount == 1) {
				Vector2i32 crop_origin { x0, y0 };
				Vector2i32 crop_size { xSize, ySize };
				cropped_image = cropImage2d(image, crop_origin, crop_size);
			} else {
				Vector3i32 crop_origin { x0, y0, z0 };
				Vector3i32 crop_size { xSize, ySize, zSize };
				cropped_image = cropImage3d(image, crop_origin, crop_size);
			}

			for (auto j = 0; j < xSize * ySize * zSize; j++) {
				auto ori_val = static_cast<const unsigned short*>(cropped_image->bufferReadOnly())[j];
				*(blobVolume.get() + j) = static_cast<uint8_t>(ori_val == static_cast<unsigned short>(code));
			}

			resultMask->AppendBlob(label, blob, name);
			resultMask->AppendMaskVolume(label, blobVolume);			
		}
		resultMask->SetMaxLabelValue(maxLabel);
	} catch (imagedev::Exception& e) {
		std::cout << e.what() << std::endl;
		resultMask = nullptr;
	}
	return resultMask;
}

auto TCDataConverter::SoToMCellLabel(SoVolumeData* vol, QString name) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();

	SoRef<SoVolumeData> SoVol = vol;

	auto res = SoVol->getVoxelSize();
	double ress[3] = { res[0], res[1], res[2] };

	auto dim = SoVol->getDimension();
	auto size = SoVol->getVoxelSize();

	SbBox3i32 bBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		SoVol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
	SoVol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	unsigned short* buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

	const uint64_t rowCount = dim[0];
	const uint64_t colCount = dim[1];
	const uint64_t sliCount = dim[2];

	VectorXu64 imageShape { rowCount, colCount, sliCount };
	Vector3d spacing { size[0], size[1], size[2] };
	Vector3d origin { -dim[0] * size[0] / 2, -dim[1] * size[1] / 2, -dim[2] * size[2] / 2 };

	auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
	auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
	setDimensionalInterpretation(image, ImageTypeId::VOLUME);

	VectorXu64 imageOrig { 0, 0, 0 };
	RegionXu64 imageRegion { imageOrig, imageShape };

	image->writeRegion(imageRegion, buffer);

	dataBufferObj->unmap();

	auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_16_BIT);

	resultMask->SetResolution(ress);
	resultMask->SetSize(dim[0], dim[1], dim[2]);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::MultiLabel);

	///////////Open Inventor Labeling Part//////////
	auto analysis = std::make_shared<AnalysisMsr>();
	auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
	auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
	auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

	auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
	auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
	auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

	auto mean = analysis->select(NativeMeasurements::intensityMean);

	labelAnalysis(label_image, image, analysis);

	auto numLabel = analysis->labelCount();

	for (auto label = 0; label < numLabel; label++) {
		//crop each label based on bounding box            
		auto x0 = bb_ox->value(label);
		auto y0 = bb_oy->value(label);
		auto z0 = bb_oz->value(label);
		auto xSize = bb_dx->value(label);
		auto ySize = bb_dy->value(label);
		auto zSize = bb_dz->value(label);
		auto code = mean->value(label);

		SbVec3f start(x0, y0, z0);
		SbVec3f end(x0 + xSize, y0 + ySize, z0 + zSize);

		auto startCoord = SoVol->XYZToVoxel(start);
		auto endCoord = SoVol->XYZToVoxel(end);

		int startIdx[3];
		int endIdx[3];
		for (auto i = 0; i < 3; i++) {
			startIdx[i] = floor(startCoord[i]);
			endIdx[i] = floor(endCoord[i]);
			if (startIdx[i] < 0)
				startIdx[i] = 0;
			if (endIdx[i] > dim[i] - 1)
				endIdx[i] = dim[i] - 1;
		}

		auto vSizeX = endIdx[0] - startIdx[0] + 1;
		auto vSizeY = endIdx[1] - startIdx[1] + 1;
		auto vSizeZ = endIdx[2] - startIdx[2] + 1;

		SbBox3i32 bBox2(SbVec3i32(startIdx[0], startIdx[1], startIdx[2]), SbVec3i32(endIdx[0], endIdx[1], endIdx[2]));
		SoLDMDataAccess::DataInfoBox dataInfoBox2 =
			SoVol->getLdmDataAccess().getData(0, bBox2, (SoBufferObject*)NULL);
		SoRef<SoCpuBufferObject> dataBufferObj2 = new SoCpuBufferObject;
		dataBufferObj2->setSize((size_t)dataInfoBox2.bufferSize);

		SoVol->getLdmDataAccess().getData(0, bBox2, dataBufferObj2.ptr());
		unsigned short* data = (unsigned short*)dataBufferObj2->map(SoBufferObject::READ_ONLY);

		MaskBlob blob;
		blob.SetCode(code);
		blob.SetBoundingBox(startIdx[0], startIdx[1], startIdx[2], endIdx[0], endIdx[1], endIdx[2]);

		std::shared_ptr<uint8_t[]> blobVolume(new uint8_t[vSizeX * vSizeY * vSizeZ](), std::default_delete<uint8_t[]>());

		for (auto i = 0; i < vSizeX * vSizeY * vSizeZ; i++) {
			*(blobVolume.get() + i) = static_cast<uint8_t>(*(data + i) == static_cast<unsigned short>(code));
		}
		dataBufferObj2->unmap();

		resultMask->AppendBlob(label, blob, name);
		resultMask->AppendMaskVolume(label, blobVolume);
	}

	return resultMask;
}

auto TCDataConverter::SoToMOrganLabel(QList<SoVolumeData*> vols, QStringList names) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();

	auto SoVol = vols[0];
	auto res = SoVol->getVoxelSize();
	double ress[3] = { res[0], res[1], res[2] };

	auto dim = SoVol->getDimension();

	resultMask->SetResolution(ress);
	resultMask->SetSize(dim[0], dim[1], dim[2]);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::MultiLayer);

	//for every layer in raw data crop masks
	auto layerNames = names;
	auto layerNum = names.size();

	for (auto i = 0; i < layerNum; i++) { }
	return nullptr;//TODO 21.02.09
}

auto TCDataConverter::ArrToImage(unsigned short* data, int dim[3], double resolution[3]) -> TCImage::Pointer {
	auto image = std::make_shared<TCImage>();

	if (dim[2] < 1)
		dim[2] = 1;

	const auto cnt = dim[0] * dim[1] * dim[2];
	std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt](), std::default_delete<unsigned short[]>());
	memcpy(arr.get(), data, cnt * sizeof(unsigned short));

	image->SetImageVolume(arr);
	image->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
	image->SetResolution(resolution);

	return image;
}

auto TCDataConverter::ArrToOrganMask(QList<uint8_t*> mask, int dim[3], double resolution[3], QStringList names) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();
	resultMask->SetResolution(resolution);
	resultMask->SetSize(dim[0], dim[1], dim[2]);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::MultiLayer);

	auto layerNames = names;
	auto layerNum = names.count();

	try {
		for (auto i = 0; i < layerNum; i++) {
			auto buffer = mask[i];

			const uint64_t rowCount = dim[0];
			const uint64_t colCount = dim[1];
			const uint64_t sliCount = dim[2];
			VectorXu64 imageShape { rowCount, colCount, sliCount };
			Vector3d spacing { 1, 1, 1 };
			Vector3d origin { 0, 0, 0 };

			auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT8));
			auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT8, properties, nullptr);
			setDimensionalInterpretation(image, ImageTypeId::VOLUME);

			VectorXu64 imageOrig { 0, 0, 0 };
			RegionXu64 imageRegion { imageOrig, imageShape };

			image->writeRegion(imageRegion, buffer);

			auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_8_BIT);

			auto analysis = std::make_shared<AnalysisMsr>();

			auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
			auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
			auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

			auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
			auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
			auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

			labelAnalysis(label_image, image, analysis);

			auto x0 = static_cast<int>(bb_ox->value(0));
			auto y0 = static_cast<int>(bb_oy->value(0));
			auto z0 = static_cast<int>(bb_oz->value(0));
			auto xSize = static_cast<int>(bb_dx->value(0));
			auto ySize = static_cast<int>(bb_dy->value(0));
			auto zSize = static_cast<int>(bb_dz->value(0));

			MaskBlob blob;
			blob.SetBoundingBox(x0, y0, z0, x0 + xSize - 1, y0 + ySize - 1, z0 + zSize - 1);
			blob.SetCode(1);

			std::shared_ptr<unsigned char[]> blobVolume(new unsigned char[static_cast<long long>(xSize) * ySize * zSize](), std::default_delete<unsigned char[]>());

			std::shared_ptr<ImageView> cropped_image { nullptr };
			if (sliCount == 1) {
				// slice�� �ϳ��� mask
				Vector2i32 crop_origin { x0, y0 };
				Vector2i32 crop_size { xSize, ySize };
				cropped_image = cropImage2d(image, crop_origin, crop_size);
			} else {
				Vector3i32 crop_origin { x0, y0, z0 };
				Vector3i32 crop_size { xSize, ySize, zSize };
				cropped_image = cropImage3d(image, crop_origin, crop_size);
			}

			for (auto j = 0; j < xSize * ySize * zSize; j++) {
				auto buf_val = static_cast<const uint8_t*>(cropped_image->bufferReadOnly())[j];
				*(blobVolume.get() + j) = buf_val;
			}

			auto name = names[i];

			blob.SetMaskVolume(blobVolume);
			resultMask->AppendLayer(name, blob);
			resultMask->AppendLayerVolume(name, blobVolume);
		}
	} catch (imagedev::Exception& e) {
		std::cout << e.what() << std::endl;
		resultMask = nullptr;
	}

	return resultMask;
}

auto TCDataConverter::SoToMOrganBinary(QList<SoVolumeData*> vols, QStringList names) -> TCMask::Pointer {
	auto resultMask = std::make_shared<TCMask>();

	SoRef<SoVolumeData> SoVol = vols[0];

	auto res = SoVol->getVoxelSize();
	double ress[3] = { res[0], res[1], res[2] };

	auto dim = SoVol->getDimension();
	auto size = SoVol->getVoxelSize();

	resultMask->SetResolution(ress);
	resultMask->SetSize(dim[0], dim[1], dim[2]);
	resultMask->SetValid(true);
	resultMask->SetType(MaskTypeEnum::MultiLayer);

	//for every layer in raw data crop masks
	auto layerNames = names;
	auto layerNum = names.size();

	for (auto i = 0; i < layerNum; i++) {
		SoRef<SoVolumeData> vol = vols[i];
		double mm, mma;
		vol->getMinMax(mm, mma);
		auto name = layerNames[i];
		if (mma < 1) {
			//create empty volume
			MaskBlob blob;
			blob.SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
			std::shared_ptr<unsigned char[]> blobVolume(new unsigned char[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned char[]>());
			blob.SetMaskVolume(blobVolume);
			resultMask->AppendLayer(name, blob);
			resultMask->AppendLayerVolume(name, blobVolume);
			continue;
		}
		if (vol->getDataType() == SoVolumeData::DataType::UNSIGNED_BYTE) {
			SbBox3i32 bBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
			SoLDMDataAccess::DataInfoBox dataInfoBox =
				vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
			SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
			dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
			vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
			auto* buffer = static_cast<unsigned char*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

			const uint64_t rowCount = dim[0];
			const uint64_t colCount = dim[1];
			const uint64_t sliCount = dim[2];
			VectorXu64 imageShape { rowCount, colCount, sliCount };
			Vector3d spacing { size[0], size[1], size[2] };
			Vector3d origin { -dim[0] * size[0] / 2, -dim[1] * size[1] / 2, -dim[2] * size[2] / 2 };

			auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT8));
			auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT8, properties, nullptr);
			setDimensionalInterpretation(image, ImageTypeId::VOLUME);

			VectorXu64 imageOrig { 0, 0, 0 };
			RegionXu64 imageRegion { imageOrig, imageShape };

			image->writeRegion(imageRegion, buffer);
			dataBufferObj->unmap();

			auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_8_BIT);

			auto analysis = std::make_shared<AnalysisMsr>();
			auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
			auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
			auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

			auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
			auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
			auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

			labelAnalysis(label_image, image, analysis);

			auto x0 = bb_ox->value(0);
			auto y0 = bb_oy->value(0);
			auto z0 = bb_oz->value(0);
			auto xSize = bb_dx->value(0);
			auto ySize = bb_dy->value(0);
			auto zSize = bb_dz->value(0);

			SbVec3f start(x0, y0, z0);
			SbVec3f end(x0 + xSize, y0 + ySize, z0 + zSize);

			auto startCoord = vol->XYZToVoxel(start);
			auto endCoord = vol->XYZToVoxel(end);

			int startIdx[3] = { static_cast<int>(startCoord[0]), static_cast<int>(startCoord[1]), static_cast<int>(startCoord[2]) };
			int endIdx[3] = { static_cast<int>(endCoord[0]) - 1, static_cast<int>(endCoord[1]) - 1, static_cast<int>(endCoord[2]) - 1 };

			auto vSizeX = endIdx[0] - startIdx[0] + 1;
			auto vSizeY = endIdx[1] - startIdx[1] + 1;
			auto vSizeZ = endIdx[2] - startIdx[2] + 1;

			SbBox3i32 bBox2(SbVec3i32(startIdx[0], startIdx[1], startIdx[2]), SbVec3i32(endIdx[0], endIdx[1], endIdx[2]));
			SoLDMDataAccess::DataInfoBox dataInfoBox2 =
				vol->getLdmDataAccess().getData(0, bBox2, (SoBufferObject*)NULL);
			SoRef<SoCpuBufferObject> dataBufferObj2 = new SoCpuBufferObject;
			dataBufferObj2->setSize((size_t)dataInfoBox2.bufferSize);

			vol->getLdmDataAccess().getData(0, bBox2, dataBufferObj2.ptr());

			MaskBlob blob;
			blob.SetBoundingBox(startIdx[0], startIdx[1], startIdx[2], endIdx[0], endIdx[1], endIdx[2]);
			blob.SetCode(1);
			std::shared_ptr<unsigned char[]> blobVolume(new unsigned char[vSizeX * vSizeY * vSizeZ](), std::default_delete<unsigned char[]>());

			unsigned char* data = static_cast<unsigned char*>(dataBufferObj2->map(SoBufferObject::READ_ONLY));
			memcpy(blobVolume.get(), data, vSizeX * vSizeY * vSizeZ);

			dataBufferObj2->unmap();
			blob.SetMaskVolume(blobVolume);
			resultMask->AppendLayer(name, blob);
			resultMask->AppendLayerVolume(name, blobVolume);

		} else if (vol->getDataType() == SoVolumeData::UNSIGNED_SHORT) {
			SbBox3i32 bBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
			SoLDMDataAccess::DataInfoBox dataInfoBox =
				vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
			SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
			dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
			vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
			auto* buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_ONLY));

			const uint64_t rowCount = dim[0];
			const uint64_t colCount = dim[1];
			const uint64_t sliCount = dim[2];
			VectorXu64 imageShape { rowCount, colCount, sliCount };
			Vector3d spacing { size[0], size[1], size[2] };
			Vector3d origin { -dim[0] * size[0] / 2, -dim[1] * size[1] / 2, -dim[2] * size[2] / 2 };

			auto properties = std::make_shared<ImageProperties>(SpatialCalibrationProperty(origin, spacing), ImageInfoProperty(DataTypeId::UINT16));
			auto image = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, properties, nullptr);
			setDimensionalInterpretation(image, ImageTypeId::VOLUME);

			VectorXu64 imageOrig { 0, 0, 0 };
			RegionXu64 imageRegion { imageOrig, imageShape };

			image->writeRegion(imageRegion, buffer);
			dataBufferObj->unmap();

			auto label_image = convertImage(image, ConvertImage::OutputType::LABEL_16_BIT);

			auto analysis = std::make_shared<AnalysisMsr>();
			auto bb_ox = analysis->select(NativeMeasurements::boundingBoxOX);
			auto bb_oy = analysis->select(NativeMeasurements::boundingBoxOY);
			auto bb_oz = analysis->select(NativeMeasurements::boundingBoxOZ);

			auto bb_dx = analysis->select(NativeMeasurements::boundingBoxDX);
			auto bb_dy = analysis->select(NativeMeasurements::boundingBoxDY);
			auto bb_dz = analysis->select(NativeMeasurements::boundingBoxDZ);

			labelAnalysis(label_image, image, analysis);

			auto x0 = bb_ox->value(0);
			auto y0 = bb_oy->value(0);
			auto z0 = bb_oz->value(0);
			auto xSize = bb_dx->value(0);
			auto ySize = bb_dy->value(0);
			auto zSize = bb_dz->value(0);

			SbVec3f start(x0, y0, z0);
			SbVec3f end(x0 + xSize, y0 + ySize, z0 + zSize);

			auto startCoord = vol->XYZToVoxel(start);
			auto endCoord = vol->XYZToVoxel(end);

			int startIdx[3] = { static_cast<int>(startCoord[0]), static_cast<int>(startCoord[1]), static_cast<int>(startCoord[2]) };
			int endIdx[3] = { static_cast<int>(endCoord[0]) - 1, static_cast<int>(endCoord[1]) - 1, static_cast<int>(endCoord[2]) - 1 };

			auto vSizeX = endIdx[0] - startIdx[0] + 1;
			auto vSizeY = endIdx[1] - startIdx[1] + 1;
			auto vSizeZ = endIdx[2] - startIdx[2] + 1;

			SbBox3i32 bBox2(SbVec3i32(startIdx[0], startIdx[1], startIdx[2]), SbVec3i32(endIdx[0], endIdx[1], endIdx[2]));
			SoLDMDataAccess::DataInfoBox dataInfoBox2 =
				vol->getLdmDataAccess().getData(0, bBox2, (SoBufferObject*)NULL);
			SoRef<SoCpuBufferObject> dataBufferObj2 = new SoCpuBufferObject;
			dataBufferObj2->setSize((size_t)dataInfoBox2.bufferSize);

			vol->getLdmDataAccess().getData(0, bBox2, dataBufferObj2.ptr());

			MaskBlob blob;
			blob.SetBoundingBox(startIdx[0], startIdx[1], startIdx[2], endIdx[0], endIdx[1], endIdx[2]);
			blob.SetCode(1);
			std::shared_ptr<unsigned char[]> blobVolume(new unsigned char[vSizeX * vSizeY * vSizeZ](), std::default_delete<unsigned char[]>());

			unsigned short* data = static_cast<unsigned short*>(dataBufferObj2->map(SoBufferObject::READ_ONLY));
			std::copy_n(data, static_cast<long long>(vSizeX) * vSizeY * vSizeZ, blobVolume.get());
			dataBufferObj2->unmap();
			blob.SetMaskVolume(blobVolume);
			resultMask->AppendLayer(name, blob);
			resultMask->AppendLayerVolume(name, blobVolume);
		}
	}

	return resultMask;
}

auto TCDataConverter::ImageToSoVolumeData(TCImage::Pointer image, bool copy) -> SoVolumeData* {
	//make SoVolume Structure Only no copy
	auto dims = image->GetSize();
	int dim[3] = { std::get<0>(dims), std::get<1>(dims), std::get<2>(dims) };
	double res[3];
	image->GetResolution(res);

	SoVolumeData* soVol = new SoVolumeData;
	soVol->ref();

	if (copy) {
		soVol->data.setValue(SbVec3i32(dim), SbDataType::UNSIGNED_SHORT, 0, (void*)image->GetBuffer(), SoSFArray::COPY);
	} else {
		soVol->data.setValue(SbVec3i32(dim), SbDataType::UNSIGNED_SHORT, 0, (void*)image->GetBuffer(), SoSFArray::NO_COPY);
	}
	if (res[2] == 0) {
		soVol->extent.setValue(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -0.5, dim[0] * res[0] / 2, dim[1] * res[1] / 2, 0.5);
	} else {
		soVol->extent.setValue(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2, dim[0] * res[0] / 2, dim[1] * res[1] / 2, dim[2] * res[2] / 2);
	}

	soVol->unrefNoDelete();

	return soVol;
}

auto TCDataConverter::SoVolumeDataToImage(SoVolumeData* volData) -> TCImage::Pointer {
	auto result = std::make_shared<TCImage>();

	auto res = volData->getVoxelSize();
	auto size = volData->getDimension();

	auto ext = volData->extent.getValue();

	double min, max;
	volData->getMinMax(min, max);

	double rres[3] = { res[0], res[1], res[2] };
	result->SetResolution(rres);
	result->SetBoundingBox(0, 0, 0, size[0], size[1], size[2]);
	result->SetMinMax(min, max);
	result->SetTimeStep(0);//TODO

	unsigned short cnt = size[0] * size[1] * size[2];

	const std::shared_ptr<unsigned short[]> imgBuffer(new unsigned short[cnt](), std::default_delete<unsigned short[]>());
	result->SetImageVolume(imgBuffer);

	return result;
}

auto TCDataConverter::ImageToImageView(const TCImage::Pointer image) -> std::shared_ptr<iolink::ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };

	try {
		if (image == nullptr)
			throw std::runtime_error("Invalid input");

		const auto [x, y, z] = image->GetSize();

		double resolution[] { 0.0, 0.0, 0.0 };
		image->GetResolution(resolution);

		if (z < 2) {	// 2D			
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																		{ resolution[0], resolution[1], 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

			auto buffer = image->GetBuffer();
			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, image->GetBuffer());

		} else {	// 3D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																		{ resolution[0], resolution[1], resolution[2] }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, image->GetBuffer());
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	} catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	} catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}

	return imageView;
}

auto TCDataConverter::ImageViewToImage(const std::shared_ptr<iolink::ImageView> imageView) -> TCImage::Pointer {
	if (imageView == nullptr || imageView->buffer() == nullptr)
		return nullptr;

	auto image = std::make_shared<TCImage>();

	auto dim = imageView->shape();
	if (dim[2] < 1)
		dim[2] = 1;

	auto spacing = imageView->properties()->calibration().spacing();
	double res[3] { spacing[0], spacing[1], spacing[2] };
	const auto cnt = dim[0] * dim[1] * dim[2];

	std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt](), std::default_delete<unsigned short[]>());
	memcpy(arr.get(), imageView->bufferReadOnly(), cnt * sizeof(unsigned short));

	image->SetImageVolume(arr);
	image->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
	image->SetResolution(res);

	return image;
}

auto TCDataConverter::ImageToProcImage(const TCImage::Pointer image) -> TCProcImage::Pointer {
	auto [dimX, dimY, dimZ] = image->GetSize();
	std::vector<uint64_t> dim = { static_cast<uint64_t>(dimX), static_cast<uint64_t>(dimY), static_cast<uint64_t>(dimZ) };

	std::vector<double> res(3);
	image->GetResolution(res.data());

	auto bufferSize = dimX * dimY * dimZ * sizeof(unsigned short);
	std::shared_ptr<unsigned char[]> buffer(new unsigned char[bufferSize](), std::default_delete<unsigned char[]>());
	memcpy(buffer.get(), image->GetBuffer(), bufferSize);

	auto procImage = std::make_shared<TCProcImage>();
	procImage->SetDimension(dim);
	procImage->SetResolution(res);
	procImage->SetDataType(ProcDataType::UINT16);
	procImage->SetImageInterpretation(ProcImageInterpretation::GRAYSCALE);
	procImage->SetImageType(ProcImageType::VOLUME);
	procImage->SetBuffer(buffer);
	procImage->SetBufferSize(bufferSize);

	return procImage;
}

auto TCDataConverter::MaskToImageView(const TCMask::Pointer mask, MaskType type) -> std::shared_ptr<iolink::ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };

	try {
		if (mask == nullptr)
			throw std::runtime_error("Invalid input");

		const auto [x, y, z] = mask->GetSize();
		const auto [resolutionX, resolutionY, resolutionZ] = mask->GetResolution();
		const auto imageInterpretation = (type == MaskType::Binary) ? ImageInterpretation::BINARY : ImageInterpretation::LABEL;

		if (z < 2) {
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolutionX / 2, -y * resolutionY / 2, 0 },	// origin
																		{ resolutionX, resolutionY, 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);
			setImageInterpretation(imageView, imageInterpretation);

			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, mask->GetMaskVolume().get());
		} else {
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolutionX / 2, -y * resolutionY / 2, -z * resolutionZ / 2 },	// origin
																		{ resolutionX, resolutionY, resolutionZ }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
			setImageInterpretation(imageView, imageInterpretation);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, mask->GetMaskVolume().get());
		}

		if (type == MaskType::Binary) {
			imageView = convertImage(imageView, ConvertImage::OutputType::BINARY);
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	} catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	} catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}


	return imageView;
}

auto TCDataConverter::ProcImageToImageView(TCProcImage::Pointer image) -> std::shared_ptr<ImageView> {
	const auto dimCount = image->GetDimensionCount();
	if (dimCount < 2)
		return nullptr;

	std::shared_ptr<ImageView> imageView { nullptr };

	try {
		const auto procImageDim = image->GetDimension();
		const auto procImageRes = image->GetResolution();

		auto shape = VectorXu64(dimCount);
		auto resolution = VectorXd(dimCount);
		auto origin = VectorXd(dimCount);

		for (auto i = 0; i < dimCount; i++) {
			shape[i] = procImageDim[i];
			resolution[i] = procImageRes[i];
			origin[i] = -shape[i] * resolution[i] / 2;
		}

		const auto dataType = static_cast<DataTypeId>(image->GetDataType()._to_integral());
		const auto imageInterpretation = static_cast<ImageInterpretation>(image->GetImageInterpretation()._to_integral());
		auto imageType = ImageType(ImageTypeId::UNKNOWN);
		switch (image->GetImageType()) {
			case ProcImageType::IMAGE:
				imageType = ImageType(ImageTypeId::IMAGE);
				break;
			case ProcImageType::VOLUME:
				imageType = ImageType(ImageTypeId::VOLUME);
				break;
			case ProcImageType::MULTISPECTRAL_IMAGE:
				imageType = ImageType(ImageTypeId::MULTISPECTRAL_IMAGE);
				break;
			case ProcImageType::MULTISPECTRAL_VOLUME:
				imageType = ImageType(ImageTypeId::MULTISPECTRAL_VOLUME);
				break;
			case ProcImageType::IMAGE_SEQUENCE:
				imageType = ImageType(ImageTypeId::IMAGE_SEQUENCE);
				break;
			case ProcImageType::VOLUME_SEQUENCE:
				imageType = ImageType(ImageTypeId::VOLUME_SEQUENCE);
				break;
			case ProcImageType::MULTISPECTRAL_IMAGE_SEQUENCE:
				imageType = ImageType(ImageTypeId::MULTISPECTRAL_IMAGE_SEQUENCE);
				break;
			case ProcImageType::MULTISPECTRAL_VOLUME_SEQUENCE:
				imageType = ImageType(ImageTypeId::MULTISPECTRAL_VOLUME_SEQUENCE);
				break;
			default:
				break;
		}

		if (dimCount < 3) {
			imageView = ImageViewFactory::allocate(shape, dataType);
			setDimensionalInterpretation(imageView, imageType);
			setImageInterpretation(imageView, imageInterpretation);

			const RegionXu64 imageRegion { { 0, 0 }, shape };
			imageView->writeRegion(imageRegion, image->GetBuffer().get());

		} else {
			const auto spatialCalibrationProperty = SpatialCalibrationProperty(origin, resolution);
			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(dataType));

			imageView = ImageViewFactory::allocate(shape, dataType, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, imageType);

			const RegionXu64 imageRegion { { 0, 0, 0 }, shape };
			imageView->writeRegion(imageRegion, image->GetBuffer().get());
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	} catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	} catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}

	return imageView;
}

auto TCDataConverter::ImageViewToProImage(std::shared_ptr<iolink::ImageView> image) -> TCProcImage::Pointer {
	if (image == nullptr || image->buffer() == nullptr)
		return nullptr;

	auto procImage = std::make_shared<TCProcImage>();

	const auto dimCount = image->dimensionCount();
	std::vector<uint64_t> dim;
	std::vector<double> res;

	auto spacing = image->properties()->calibration().spacing();

	for (auto i = 0; i < dimCount; i++) {
		dim.push_back(image->shape()[i]);
		res.push_back(spacing[i]);
	}

	std::shared_ptr<unsigned char[]> buffer(new unsigned char[image->bufferSize()](), std::default_delete<unsigned char[]>());
	memcpy(buffer.get(), image->buffer(), image->bufferSize());

	procImage->SetBufferSize(image->bufferSize());
	procImage->SetBuffer(buffer);
	procImage->SetDimension(dim);
	procImage->SetResolution(res);
	procImage->SetDataType(ProcDataType::_from_integral(image->dataType().id()));
	procImage->SetImageInterpretation(ProcImageInterpretation::_from_integral(static_cast<uint64_t>(getImageInterpretation(image))));

	ProcImageType procImageType { ProcImageType::UNKNOWN };
	const auto imageType = getDimensionalInterpretation(image);
	if (imageType == ImageTypeId::IMAGE)
		procImageType = ProcImageType::IMAGE;
	else if (imageType == ImageTypeId::VOLUME)
		procImageType = ProcImageType::VOLUME;
	else if (imageType == ImageTypeId::MULTISPECTRAL_IMAGE)
		procImageType = ProcImageType::MULTISPECTRAL_IMAGE;
	else if (imageType == ImageTypeId::MULTISPECTRAL_VOLUME)
		procImageType = ProcImageType::MULTISPECTRAL_VOLUME;
	else if (imageType == ImageTypeId::IMAGE_SEQUENCE)
		procImageType = ProcImageType::IMAGE_SEQUENCE;
	else if (imageType == ImageTypeId::VOLUME_SEQUENCE)
		procImageType = ProcImageType::VOLUME_SEQUENCE;
	else if (imageType == ImageTypeId::MULTISPECTRAL_IMAGE_SEQUENCE)
		procImageType = ProcImageType::MULTISPECTRAL_IMAGE_SEQUENCE;
	else
		if (imageType == ImageTypeId::MULTISPECTRAL_VOLUME_SEQUENCE)
			procImageType = ProcImageType::MULTISPECTRAL_VOLUME_SEQUENCE;

	procImage->SetImageType(procImageType);

	return procImage;
}

auto TCDataConverter::ProcImageToSoVolumData(const TCProcImage::Pointer procImage) -> SoVolumeData* {
	const auto dimCount = procImage->GetDimensionCount();
	if (dimCount < 2)
		return nullptr;

	const auto imageType = procImage->GetImageType();
	if (imageType != +ProcImageType::IMAGE && imageType != +ProcImageType::VOLUME)
		return nullptr;

	// convert TCProcImage to SoVolumeData
	const auto procImageDim = procImage->GetDimension();
	const auto procImageRes = procImage->GetResolution();

	int dim[3] { 0, 0, 0 };
	double res[3] { 0, 0, 0 };
	for (auto i = 0; i < dimCount; i++) {
		dim[i] = procImageDim[i];
		res[i] = procImageRes[i];
	}

	if (dim[2] == 0)
		dim[2] = 1;

	auto dataType = SbDataType::UNKNOWN;
	switch (procImage->GetDataType()) {
		case ProcDataType::UINT8:
			dataType = SbDataType::UNSIGNED_BYTE;
			break;
		case ProcDataType::INT8:
			dataType = SbDataType::SIGNED_BYTE;
			break;
		case ProcDataType::UINT16:
			dataType = SbDataType::UNSIGNED_SHORT;
			break;
		case ProcDataType::INT16:
			dataType = SbDataType::SIGNED_SHORT;
			break;
		case ProcDataType::UINT32:
			dataType = SbDataType::UNSIGNED_INT32;
			break;
		case ProcDataType::INT32:
			dataType = SbDataType::SIGNED_INT32;
			break;
		case ProcDataType::FLOAT:
			dataType = SbDataType::FLOAT;
			break;
		case ProcDataType::DOUBLE:
			dataType = SbDataType::DOUBLE;
			break;
		default:
			break;
	}

	if (dataType == SbDataType::UNKNOWN)
		return nullptr;

	SoVolumeData* vol = new SoVolumeData;
	vol->ref();

	vol->data.setValue(SbVec3i32(dim), dataType, 0, procImage->GetBuffer().get(), SoSFArray::COPY);

	if (res[2] == 0.) {
		vol->extent.setValue(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -0.5, dim[0] * res[0] / 2, dim[1] * res[1] / 2, 0.5);
	} else {
		vol->extent.setValue(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2, dim[0] * res[0] / 2, dim[1] * res[1] / 2, dim[2] * res[2] / 2);
	}

	vol->unrefNoDelete();

	return vol;
}

auto TCDataConverter::MapMaskGeometry(const std::shared_ptr<iolink::ImageView> refImage, const std::shared_ptr<iolink::ImageView> mask, double refOffset, double maskOffset) -> std::shared_ptr<iolink::ImageView> {
	std::shared_ptr<iolink::ImageView> mappedMask { nullptr };
	try {
		const auto targetShape = refImage->shape();

		if (targetShape.size() < 3 || targetShape[2] < 2) {	// 2D
			mappedMask = rescaleImage2d(mask, targetShape[0], targetShape[1], RescaleImage2d::LINEAR);
		} else {
			auto GetPhysicalPosition = [](int ix, int iy, int iz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<double, double, double> {
				auto phyX = originX + ix * spx;
				auto phyY = originY + iy * spy;
				auto phyZ = originZ + offset + iz * spz;

				return std::make_tuple(phyX, phyY, phyZ);
			};

			auto GetIndexPosition = [](double px, double py, double pz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<int, int, int> {
				auto ix = static_cast<int>(floor((px - originX) / spx));
				auto iy = static_cast<int>(floor((py - originY) / spy));
				auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));

				return std::make_tuple(ix, iy, iz);
			};

			const auto bufferSize = targetShape[0] * targetShape[1] * targetShape[2];
			if (bufferSize < 1)
				throw std::runtime_error("Data size is 0.");

			const auto arr = std::shared_ptr<uint16_t[]>(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());

			const auto targetRes = refImage->properties()->calibration().spacing();
			const auto targetOrigin = refImage->properties()->calibration().origin();

			const auto sourceRes = mask->properties()->calibration().spacing();
			const auto sourceOrigin = mask->properties()->calibration().origin();
			const auto sourceShape = mask->shape();

			const auto sourceArr = static_cast<const uint16_t*>(mask->bufferReadOnly());

			for (auto k = 0; k < targetShape[2]; k++) {
				for (auto i = 0; i < targetShape[0]; i++) {
					for (auto j = 0; j < targetShape[1]; j++) {
						const auto [px, py, pz] = GetPhysicalPosition(i, j, k, targetRes[0], targetRes[1], targetRes[2], targetOrigin[0], targetOrigin[1], targetOrigin[2], refOffset);
						const auto [ix, iy, iz] = GetIndexPosition(px, py, pz, sourceRes[0], sourceRes[1], sourceRes[2], sourceOrigin[0], sourceOrigin[1], sourceOrigin[2], maskOffset);

						if (ix > -1 && ix < sourceShape[0] && iy > -1 && iy < sourceShape[1] && iz > -1 && iz < sourceShape[2]) {
							const auto sourceidx = iz * sourceShape[0] * sourceShape[1] + iy * sourceShape[0] + ix;
							const auto val = *(sourceArr + sourceidx);
							const auto targetidx = k * targetShape[0] * targetShape[1] + j * targetShape[0] + i;
							*(arr.get() + targetidx) = val;
						}
					}
				}
			}

			const auto tempImage = ImageViewFactory::allocate(targetShape, DataTypeId::UINT16, refImage->properties()->clone(), nullptr);
			imagedev::setDimensionalInterpretation(tempImage, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, targetShape };
			tempImage->writeRegion(imageRegion, arr.get());

			mappedMask = convertImage(tempImage, ConvertImage::LABEL_16_BIT);
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	}
	catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	}
	catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}

	return mappedMask;
}

auto TCDataConverter::UShortArrToImageView(uint16_t* data, int x, int y, int z, double resolution[3]) -> std::shared_ptr<iolink::ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };
	try {
		if (data == nullptr)
			throw std::runtime_error("Invalid input");
		if (z < 2) {	// 2D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																		{ resolution[0], resolution[1], 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, data);

		} else {	// 3D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																		{ resolution[0], resolution[1], resolution[2] }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, data);
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	}
	catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	}
	catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}

	return imageView;
}

auto TCDataConverter::FloatArrToImageView(float* data, int x, int y, int z, double resolution[3]) -> std::shared_ptr<iolink::ImageView> {
	std::shared_ptr<iolink::ImageView> imageView { nullptr };
	try {
		if (data == nullptr)
			throw std::runtime_error("Invalid input");
		if (z < 2) {	// 2D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																		{ resolution[0], resolution[1], 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::FLOAT));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::FLOAT, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, data);

		} else {	// 3D
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																		{ resolution[0], resolution[1], resolution[2] }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::FLOAT));
			const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

			imageView = ImageViewFactory::allocate(imageShape, DataTypeId::FLOAT, imageProperties, nullptr);
			setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			imageView->writeRegion(imageRegion, data);
		}
	} catch (std::exception& e) {
		QLOG_ERROR() << e.what();
	}
	catch (Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.what());
	}
	catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
	}

	return imageView;
}
