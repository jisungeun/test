#include "TCProcImage.h"

#include <iostream>
#include <QVector>
#include <tuple>

struct TCProcImage::Impl {
	ProcDataType dataType{ ProcDataType::UNKNOWN };
    ProcImageType imageType{ ProcImageType::UNKNOWN };
    ProcImageInterpretation interpretation{ ProcImageInterpretation::UNKNOWN };

    std::vector<uint64_t> dim;
    std::vector<double> res;

    uint64_t dataSize{ 0 }; // byte size
	std::shared_ptr<unsigned char[]> data{ nullptr };
};

TCProcImage::TCProcImage() : d{ new Impl } {

}

TCProcImage::TCProcImage(const TCProcImage& other) : d{ new Impl } {
	*d = *other.d;
}

TCProcImage::~TCProcImage() {
	
}

auto TCProcImage::SetDataType(ProcDataType type) -> void {
    d->dataType = type;
}

auto TCProcImage::GetDataType() const -> ProcDataType {
    return d->dataType;
}

auto TCProcImage::SetImageType(ProcImageType type) -> void {
    d->imageType = type;
}

auto TCProcImage::GetImageType() const -> ProcImageType {
    return d->imageType;
}

auto TCProcImage::SetImageInterpretation(ProcImageInterpretation interpretation) -> void {
    d->interpretation = interpretation;
}

auto TCProcImage::GetImageInterpretation() const -> ProcImageInterpretation {
    return d->interpretation;
}

auto TCProcImage::GetDimensionCount() const -> uint64_t {
    return d->dim.size();
}

auto TCProcImage::SetDimension(std::vector<uint64_t> dimension) -> void {
    d->dim = dimension;
}

auto TCProcImage::GetDimension() const -> std::vector<uint64_t> {
    return d->dim;
}

auto TCProcImage::SetResolution(std::vector<double> resolution) -> void {
    d->res = resolution;
}

auto TCProcImage::GetResolution() const -> std::vector<double> {
    return d->res;
}

auto TCProcImage::SetBuffer(std::shared_ptr<unsigned char[]> buffer) -> void {
    d->data = buffer;
}

auto TCProcImage::GetBuffer() -> std::shared_ptr<unsigned char[]> {
    return d->data;
}

auto TCProcImage::SetBufferSize(uint64_t size) -> void {
    d->dataSize = size;
}

auto TCProcImage::GetBufferSize() const -> uint64_t {
    return d->dataSize;
}
