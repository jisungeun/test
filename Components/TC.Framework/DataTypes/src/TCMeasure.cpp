#include <QMap>

#include "TCMeasure.h"

struct TCMeasure::Impl {
	QVector<QMap<QString, double>> measure;
	QStringList organ_names;
	QStringList keys;
	int time_step{ 0 };
	double time_point{ 0.0 };
	double time_interval{ 0.0 };
};
TCMeasure::TCMeasure() : d{ new Impl } {

}
TCMeasure::TCMeasure(const TCMeasure& other) : d{ new Impl } {
	*d = *other.d;
}
TCMeasure::~TCMeasure() {

}
auto TCMeasure::AppendMeasure(const QString& key, int cell_idx, double val) -> void {
	if (d->measure.size() < cell_idx + 1) {
		d->measure.push_back(QMap < QString, double>());
	}
	auto organ_name = key.split(".")[0];
	auto kkey = key.split(".")[1];

	if (false == d->organ_names.contains(organ_name)) {
		d->organ_names.push_back(organ_name);
	}
	if (false == d->keys.contains(kkey)) {
		d->keys.push_back(kkey);
	}
	d->measure[cell_idx][key] = val;
}
auto TCMeasure::GetKeys() -> QStringList {
	return d->keys;
}
auto TCMeasure::GetOrganNames() -> QStringList {
	return d->organ_names;
}
auto TCMeasure::GetMeasure(const QString& key, int cell_idx) -> double {
	if (d->measure.size() < cell_idx + 1) {
		return -1.0;
	}
	else {
		if (d->measure[cell_idx].contains(key)) {
			return d->measure[cell_idx][key];
		}
		else {
			return -1.0;
		}
	}
}
auto TCMeasure::GetCellCount() -> int {
	return d->measure.size();
}

auto TCMeasure::GetKeys(int cell_idx) -> QList<QString> {
	return d->measure[cell_idx].keys();
}
auto TCMeasure::Clear() -> void {
	d->measure.clear();
}
auto TCMeasure::SetTimeIndex(int time_idx) -> void {
	d->time_step = time_idx;
}
auto TCMeasure::GetTimeIndex() -> int {
	return d->time_step;
}
auto TCMeasure::SetTimePoint(double time_point)->void {
	d->time_point = time_point;
}
auto TCMeasure::GetTimePoint()->double {
	return d->time_point;
}
auto TCMeasure::SetTimeInterval(double interval) -> void {
	d->time_interval = interval;
}
auto TCMeasure::GetTimeInterval() -> double {
	return d->time_interval;
}