#include "TCImage.h"

#include <iostream>
#include <QVector>
#include <tuple>

struct TCImage::Impl {
	struct {
		int x, y, z;
	}center{ 0,0,0, };
	struct {
		int x0, y0, z0;
		int x1, y1, z1;
	}bbox{ 0,0,0,1,1,1 };

	double resolution[3]{1,1,1};
	int time_step{ -1 };
	int min;
	int max;
	float original_min{1};
	float original_max{-1};

	int ch{ -1 };

	float zOffset = { 0.0 };

	VolumePtr imageVolume{ nullptr };

	QString path;
};
TCImage::TCImage() : d{ new Impl } {

}
TCImage::TCImage(const TCImage& other) : d{ new Impl } {
	*d = *other.d;
}
TCImage::~TCImage() {
	
}
auto TCImage::SetFilePath(const QString& path) -> void {
	d->path = path;
}
auto TCImage::GetFilePath() const -> QString {
	return d->path;
}

auto TCImage::SetOriginalMinMax(float min, float max) -> void {
	d->original_min = min;
	d->original_max = max;
}

auto TCImage::GetOriginalMinMax() const -> std::tuple<float, float> {
	return std::make_tuple(d->original_min, d->original_max);
}

auto TCImage::SetMinMax(int min, int max) -> void {
	d->min = min;
	d->max = max;
}
auto TCImage::GetMinMax() const -> std::tuple<int, int> {
	return std::make_tuple(d->min, d->max);
}
auto TCImage::GetResolution(double res[3]) -> void {
	for (auto i = 0; i < 3; i++) {
		res[i] = d->resolution[i];
	}
}
auto TCImage::SetResolution(double res[3]) -> void {
	for (auto i = 0; i < 3; i++) {
		d->resolution[i] = res[i];
	}
}

auto TCImage::GetCenter() const -> std::tuple<int, int, int> {
	return std::make_tuple(d->center.x, d->center.y, d->center.z);
}

auto TCImage::GetSize() const -> std::tuple<int, int, int> {
	return std::make_tuple(d->bbox.x1 - d->bbox.x0 + 1, d->bbox.y1 - d->bbox.y0 + 1, d->bbox.z1 - d->bbox.z0 + 1);
}

auto TCImage::SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1) -> void {
	d->bbox.x0 = x0;
	d->bbox.y0 = y0;
	d->bbox.z0 = z0;
	d->bbox.x1 = x1;
	d->bbox.y1 = y1;
	d->bbox.z1 = z1;

	d->center.x = (d->bbox.x0 + d->bbox.x1) / 2;
	d->center.y = (d->bbox.y0 + d->bbox.y1) / 2;
	d->center.z = (d->bbox.z0 + d->bbox.z1) / 2;
}

auto TCImage::GetBoundingBox() const -> std::tuple<int, int, int, int, int, int> {
	return std::make_tuple(d->bbox.x0, d->bbox.y0, d->bbox.z0, d->bbox.x1, d->bbox.y1, d->bbox.z1);
}
auto TCImage::SetTimeStep(int step) -> void {
	d->time_step = step;
}
auto TCImage::GetTimeStep() -> int {
	return d->time_step;
}

auto TCImage::SetImageVolume(VolumePtr volume) -> void {
	d->imageVolume = volume;
}

auto TCImage::GetImageVolume() -> VolumePtr {
	return d->imageVolume;
}

auto TCImage::Init() -> void {

}

auto TCImage::addOffset(const float offset) -> void {
	d->zOffset = offset;
}

auto TCImage::SetOffset(float offset) -> void {
	d->zOffset = offset;
}

auto TCImage::GetOffset() const -> float {
	return d->zOffset;
}

auto TCImage::GetBuffer(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void* {
	auto bb = std::make_tuple(xMin, xMax, yMin, yMax, zMin, zMax);
	return nullptr;//pending
}

auto TCImage::GetBuffer() -> void* {
	return d->imageVolume.get();
}

auto TCImage::GetTimeSteps() const -> int {
	return d->time_step;
}

auto TCImage::getValue() -> int {
	return 1;
}

auto TCImage::setChannel(int ch) -> void {
	d->ch = ch;
}

auto TCImage::getChannel() -> int {
	return d->ch;
}

auto TCImage::clone() -> IBaseImage* {
	auto img = std::make_shared<TCImage>();
	*img->d = *d;
	return img.get();
}
