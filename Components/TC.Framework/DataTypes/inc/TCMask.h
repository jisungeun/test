#pragma once

#include <memory>

#include <enum.h>

#include <QList>

#include <IBaseMask.h>

#include "TCDataTypesExport.h"

BETTER_ENUM(MaskTypeEnum, int,
	MultiLabel = 100,  //binary mask has one layer
	MultiLayer = 200,  //layer can overlap
	MultiLayerLabel = 300,
	None = 400
)

typedef struct MaskMetaInfo {
	QString name;
	int type;
	int ch{-1};
}MaskMeta;

class TCDataTypes_API MaskBlob {
public:
	typedef std::shared_ptr<uint8_t[]> VolumePtr;

public:
	MaskBlob();
	MaskBlob(const MaskBlob& other);
	~MaskBlob();

	MaskBlob& operator=(const MaskBlob& other);

	auto GetCenter(void) const->std::tuple<int, int, int>;
	auto SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1)->void;
	auto GetBoundingBox(void) const->std::tuple<int, int, int, int, int, int>;

	auto SetCode(int code)->void;
	auto GetCode() const->int;

	auto SetMaskVolume(VolumePtr volume)->void;
	auto GetMaskVolume()->VolumePtr;		

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};


class TCDataTypes_API TCMask : public IBaseMask {
public:

	typedef TCMask Self;
	typedef std::shared_ptr<Self> Pointer;
	typedef std::shared_ptr<unsigned short[]> VolumePtr;

	TCMask();
	TCMask(const TCMask& other);
	virtual ~TCMask();;

	auto AppendBlob(int index, MaskBlob& blob,QString name = QString())->void;
	auto AppendLayer(QString name, MaskBlob& blob, int index = -1)->void;
	auto AppendMaskVolume(int index, MaskBlob::VolumePtr maskVolume)->void;
	auto AppendLayerVolume(QString name, MaskBlob::VolumePtr maskVolume, int index = -1)->void;
	auto GetBlobIndexes() const->QList<int>;
	auto GetBlob(int index) const->MaskBlob;
	auto GetBlob(QString name)const->MaskBlob;
	auto CountBlobs() const->int;
	auto GetTimeSteps()const->int;
	auto SetName(QString name)->void;
	auto GetName()->QString;

	auto SetTimeStep(int step)->void;
	auto GetTimeStep()->int;
	auto SetLayerName(int index, QString name)->void;

	auto IsValid() const->bool;
	auto SetValid(bool valid)->void;

	auto UpdateCode(int index, int code)->bool;
	auto GetLayerNames()->QStringList override;
	auto GetMaskVolume(int index) const->MaskBlob::VolumePtr;
	auto GetMaskVolume()const->VolumePtr;
	auto GetMaskLayer(QString name)const->MaskBlob::VolumePtr;
	auto GetLayerBlob(QString name)const->MaskBlob;
	auto SetSize(int x, int y, int z)->void;
	auto GetSize()->std::tuple<int, int, int>;
	auto SetResolution(double res[3])->void;
	auto GetResolution()->std::tuple<double, double, double>;
	auto SetType(MaskTypeEnum type)->void;
	auto GetType()->MaskTypeEnum;
	auto SetTypeText(const QString& type)->void;
	auto GetTypeText()->QString;
	auto SetPath(const QString& path)->void;
	auto GetPath()->QString;

	auto addOffset(const float offset) -> void override;
	auto SetOffset(float offset)->void;
	auto GetOffset()const->float;
	auto getValue() -> unsigned short* override;

	auto SetMaxLabelValue(int value)->void;
	auto GetMaxLabelValue()->int;

	auto clone()->IBaseMask* override;

private:
	auto FindIndex(QString key)const ->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};
