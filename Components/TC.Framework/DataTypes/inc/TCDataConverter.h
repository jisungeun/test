#pragma once

#include <TCMask.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <iolink/view/ImageView.h>

#include "TCDataTypesExport.h"

class SoVolumeData;

class TCDataTypes_API TCDataConverter {
public:
	enum class MaskType {
		Binary,	// unsigned int 8bit 
		Label	// unsigned int 16bit
	};

	TCDataConverter();
	virtual ~TCDataConverter();

	auto MaskToSoVolumeData(TCMask::Pointer mask, QString name = QString())->SoVolumeData*;
	auto SoVolumeDataToMask(QList<SoVolumeData*> volData, MaskTypeEnum type, QStringList names = QStringList())->TCMask::Pointer;

	auto ImageToSoVolumeData(TCImage::Pointer image, bool copy = false)->SoVolumeData*;
	auto SoVolumeDataToImage(SoVolumeData* volData)->TCImage::Pointer;

	auto ArrToImage(unsigned short* data, int dim[3], double resolution[3])->TCImage::Pointer;
	auto ArrToOrganMask(QList<uint8_t*> mask, int dim[3],double resolution[3], QStringList names = QStringList())->TCMask::Pointer;
	auto ArrToLabelMask(unsigned short* mask, int dim[3], double resolution[3], QString name = QString())->TCMask::Pointer;
	auto UShortArrToImageView(uint16_t* data, int x, int y, int z, double resolution[3])->std::shared_ptr<iolink::ImageView>;
	auto FloatArrToImageView(float* data, int x, int y, int z, double resolution[3])->std::shared_ptr<iolink::ImageView>;

	auto ImageToImageView(const TCImage::Pointer image) -> std::shared_ptr<iolink::ImageView>;
	auto ImageViewToImage(const std::shared_ptr<iolink::ImageView> imageView) -> TCImage::Pointer;
	auto ImageToProcImage(const TCImage::Pointer image) -> TCProcImage::Pointer;

	auto MaskToImageView(const TCMask::Pointer mask, MaskType type = MaskType::Label) -> std::shared_ptr<iolink::ImageView>;

	auto ProcImageToImageView(TCProcImage::Pointer image) -> std::shared_ptr<iolink::ImageView>;
	auto ImageViewToProImage(std::shared_ptr<iolink::ImageView> image) -> TCProcImage::Pointer;

	auto ProcImageToSoVolumData(const TCProcImage::Pointer procImage) -> SoVolumeData*;

public:
	static auto MapMaskGeometry(const std::shared_ptr<iolink::ImageView> refImage, const std::shared_ptr<iolink::ImageView> mask, double refOffset = 0, double maskOffset = 0)->std::shared_ptr<iolink::ImageView>;

private:
	auto MtoSoCellBinary(TCMask::Pointer mask)->SoVolumeData*;
	auto MtoSoCellLabel(TCMask::Pointer mask)->SoVolumeData*;
	auto MtoSoOrganBinary(TCMask::Pointer mask, QString name = QString())->SoVolumeData*;

	auto SoToMCellBinary(SoVolumeData* vol, QString name)->TCMask::Pointer;
	auto SoToMCellLabel(SoVolumeData* vol, QString name)->TCMask::Pointer;
	auto SoToMOrganBinary(QList<SoVolumeData*> vols, QStringList names)->TCMask::Pointer;
	auto SoToMOrganLabel(QList<SoVolumeData*> vols, QStringList names)->TCMask::Pointer;

	struct Impl;
	std::unique_ptr<Impl> d;
};