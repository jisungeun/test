#pragma once

#include <memory>

#include <enum.h>
#include <QVariantMap>

#include <IBaseData.h>

#include "TCDataTypesExport.h"

BETTER_ENUM(ProcDataType, uint64_t,
    UNKNOWN = 0x0000000000000000,            ///< An opaque type
    UINT8 = 0x0000000000010108,              ///< 8 bits unsigned integer
    UINT16 = 0x0000000000010110,             ///< 16 bits unsigned integer
    UINT32 = 0x0000000000010120,             ///< 32 bits unsigned integer
    UINT64 = 0x0000000000010140,             ///< 64 bits unsigned integer
    INT8 = 0x0000000000020108,               ///< 8 bits signed integer
    INT16 = 0x0000000000020110,              ///< 16 bits signed integer
    INT32 = 0x0000000000020120,              ///< 32 bits signed integer
    INT64 = 0x0000000000020140,              ///< 64 bits signed integer
    FLOAT = 0x0000000000030120,              ///< Single precision floating point
    DOUBLE = 0x0000000000030140,             ///< Double precision floating point
    UTF8_STRING = 0x0000000000040108,        ///< Unicode string encoded in UTF-8
    UTF16_STRING = 0x0000000000040110,       ///< Unicode string encoded in UTF-16
    UTF32_STRING = 0x0000000000040120,       ///< Unicode string encoded in UTF-32
    VEC2_UINT8 = 0x0000000000010208,         ///< A vector of 2 elements that are 8 bits unsigned integer
    VEC2_UINT16 = 0x0000000000010210,        ///< A vector of 2 elements that are 16 bits unsigned integer
    VEC2_UINT32 = 0x0000000000010220,        ///< A vector of 2 elements that are 32 bits unsigned integer
    VEC2_UINT64 = 0x0000000000010240,        ///< A vector of 2 elements that are 64 bits unsigned integer
    VEC2_INT8 = 0x0000000000020208,          ///< A vector of 2 elements that are 8 bits signed integer
    VEC2_INT16 = 0x0000000000020210,         ///< A vector of 2 elements that are 16 bits signed integer
    VEC2_INT32 = 0x0000000000020220,         ///< A vector of 2 elements that are 32 bits signed integer
    VEC2_INT64 = 0x0000000000020240,         ///< A vector of 2 elements that are 64 bits signed integer
    VEC2_FLOAT = 0x0000000000030220,         ///< A vector of 2 elements that are single precision floating point
    VEC2_DOUBLE = 0x0000000000030240,        ///< A vector of 2 elements that are double precision floating point
    VEC3_UINT8 = 0x0000000000010308,         ///< A vector of 2 elements that are 8 bits unsigned integer
    VEC3_UINT16 = 0x0000000000010310,        ///< A vector of 3 elements that are 16 bits unsigned integer
    VEC3_UINT32 = 0x0000000000010320,        ///< A vector of 3 elements that are 32 bits unsigned integer
    VEC3_UINT64 = 0x0000000000010340,        ///< A vector of 3 elements that are 64 bits unsigned integer
    VEC3_INT8 = 0x0000000000020308,          ///< A vector of 3 elements that are 8 bits signed integer
    VEC3_INT16 = 0x0000000000020310,         ///< A vector of 3 elements that are 16 bits signed integer
    VEC3_INT32 = 0x0000000000020320,         ///< A vector of 3 elements that are 32 bits signed integer
    VEC3_INT64 = 0x0000000000020340,         ///< A vector of 3 elements that are 64 bits signed integer
    VEC3_FLOAT = 0x0000000000030320,         ///< A vector of 3 elements that are single precision floating point
    VEC3_DOUBLE = 0x0000000000030340,        ///< A vector of 3 elements that are double precision floating point
    VEC4_UINT8 = 0x0000000000010408,         ///< A vector of 4 elements that are 8 bits unsigned integer
    VEC4_UINT16 = 0x0000000000010410,        ///< A vector of 4 elements that are 16 bits unsigned integer
    VEC4_UINT32 = 0x0000000000010420,        ///< A vector of 4 elements that are 32 bits unsigned integer
    VEC4_UINT64 = 0x0000000000010440,        ///< A vector of 4 elements that are 64 bits unsigned integer
    VEC4_INT8 = 0x0000000000020408,          ///< A vector of 4 elements that are 8 bits signed integer
    VEC4_INT16 = 0x0000000000020410,         ///< A vector of 4 elements that are 16 bits signed integer
    VEC4_INT32 = 0x0000000000020420,         ///< A vector of 4 elements that are 32 bits signed integer
    VEC4_INT64 = 0x0000000000020440,         ///< A vector of 4 elements that are 64 bits signed integer
    VEC4_FLOAT = 0x0000000000030420,         ///< A vector of 4 elements that are single precision floating point
    VEC4_DOUBLE = 0x0000000000030440,        ///< A vector of 4 elements that are double precision floating point
    COMPLEX_FLOAT = 0x0000000100030220,      ///< A single precision floating point complex number
    COMPLEX_DOUBLE = 0x0000000100030240,     ///< A double precision floating point complex number
    MATRIX2_FLOAT = 0x0000000200030420,      ///< A single precision floating point 2x2 Matrix
    MATRIX2_DOUBLE = 0x0000000200030440,     ///< A double precision floating point 2x2 Matrix
    MATRIX3_FLOAT = 0x0000000200030920,      ///< A single precision floating point 3x3 Matrix
    MATRIX3_DOUBLE = 0x0000000200030940,     ///< A double precision floating point 3x3 Matrix
    MATRIX4_FLOAT = 0x0000000200031020,      ///< A single precision floating point 4x4 Matrix
    MATRIX4_DOUBLE = 0x0000000200031040,     ///< A double precision floating point 4x4 Matrix
    SYM_MATRIX2_FLOAT = 0x0000000300030320,  ///< A single precision floating point 2x2 symmetric Matrix
    SYM_MATRIX2_DOUBLE = 0x0000000300030340, ///< A double precision floating point 2x2 symmetric Matrix
    SYM_MATRIX3_FLOAT = 0x0000000300030620,  ///< A single precision floating point 3x3 symmetric Matrix
    SYM_MATRIX3_DOUBLE = 0x0000000300030640  ///< A double precision floating point 3x3 symmetric Matrix
);

BETTER_ENUM(ProcImageType, uint64_t,
    UNKNOWN,
    IMAGE,
    VOLUME,
    MULTISPECTRAL_IMAGE,
    MULTISPECTRAL_VOLUME,
    IMAGE_SEQUENCE,
    VOLUME_SEQUENCE,
    MULTISPECTRAL_IMAGE_SEQUENCE,
    MULTISPECTRAL_VOLUME_SEQUENCE
);

BETTER_ENUM(ProcImageInterpretation, uint64_t,
    UNKNOWN = 0x00,
    GRAYSCALE = 0x01,
    LABEL = 0x02,
    BINARY = 0x03,
    CIE_XYZ = 0x11,
    CIE_XYZ_CHROMATICITY = 0x12,
    CIE_UVW = 0x14,
    CIE_LUV = 0x15,
    CIE_LAB = 0x16,
    RGB = 0x21,
    RGB_CHROMATICITY = 0x22,
    SRGB = 0x23,
    YUV = 0x31,
    YCC = 0x32,
    YIQ = 0x33,
    YDD = 0x34,
    HSL = 0x41,
    HSV = 0x42,
    CMYK = 0x51,
    MULTISPECTRAL = 0xFF
);


class TCDataTypes_API TCProcImage : public IBaseData {
public:
    typedef TCProcImage Self;
    typedef std::shared_ptr<Self> Pointer;

    TCProcImage();
    TCProcImage(const TCProcImage& other);
    virtual ~TCProcImage();

    auto SetDataType(ProcDataType type) -> void;
    auto GetDataType() const -> ProcDataType;

    auto SetImageType(ProcImageType type) -> void;
    auto GetImageType() const -> ProcImageType;

    auto SetImageInterpretation(ProcImageInterpretation interpretation) -> void;
    auto GetImageInterpretation() const -> ProcImageInterpretation;

    auto GetDimensionCount() const -> uint64_t;

    auto SetDimension(std::vector<uint64_t> dimension) -> void;
    auto GetDimension() const -> std::vector<uint64_t>;

    auto SetResolution(std::vector<double> resolution) -> void;
    auto GetResolution() const -> std::vector<double>;

    auto SetBuffer(std::shared_ptr<unsigned char[]> buffer) -> void;
    auto GetBuffer() -> std::shared_ptr<unsigned char[]>;

    auto SetBufferSize(uint64_t size) -> void;
    auto GetBufferSize() const -> uint64_t;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};