#pragma once

#include "enum.h"

#include <memory>
#include "IBaseMask.h"
#include "TCDataTypesExport.h"

#include <VolumeViz/nodes/SoVolumeData.h>

BETTER_ENUM(MASK_VALUE, int,
            BINARY_BUFFER = 101,
            LABEL_BUFFER = 102,
            CELL_BINARY = 201,
            CELL_LABEL = 202,
            ORGAN_BINARY = 203,
            ORGAN_LABEL = 204,
            CELL_ORGAN = 301,
            NONE = 1
)

class TCDataTypes_API MaskRaw : public IBaseMask {       
    public:
    typedef std::shared_ptr<MaskRaw> Pointer;

public:
    MaskRaw();
    virtual ~MaskRaw();

    static auto New(unsigned short *value)->IBaseMask::Pointer;
    static auto New(SoVolumeData* vol)->IBaseMask::Pointer;
    static auto New(QVector<SoVolumeData*> vols)->IBaseMask::Pointer;

    auto clone()->IBaseMask* override;

    auto addOffset(const float offset)->void override; 
    auto getValue(void)->unsigned short* override;    

    auto setReferenceData(SoVolumeData* refVol) -> void;
    auto setReferenceData(QVector<SoVolumeData*> refs)->void;

    auto getReferenceData(int idx)->SoVolumeData*;

    auto setType(MASK_VALUE mv)->void;
    auto getType(void)->MASK_VALUE;
    auto getVolume(int idx)->SoVolumeData*;
    auto getVolumes(void)->QVector<SoVolumeData*>;
    auto getLayerNames(void)->QVector<QString>;
    auto GetLayerNames() -> QStringList override;
    auto setLayerName(int idx,QString name)->void;
    auto getLayerNum(void)->int;

protected:
    MaskRaw(const MaskRaw& ohter) = delete;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};