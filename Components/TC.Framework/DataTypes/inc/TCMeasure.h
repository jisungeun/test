#pragma once

#include <memory>

#include <QString>

#include <IBaseData.h>

#include "TCDataTypesExport.h"

class TCDataTypes_API TCMeasure : public IBaseData {
public:
	typedef TCMeasure Self;
	typedef std::shared_ptr<Self> Pointer;

	TCMeasure();
	TCMeasure(const TCMeasure& other);
	virtual ~TCMeasure();

	auto AppendMeasure(const QString& key, int cell_idx, double val)->void;
	auto GetOrganNames()->QStringList;
	auto GetKeys()->QStringList;
	auto GetMeasure(const QString& key, int cell_idx)->double;
	auto SetTimeIndex(int time_idx)->void;
	auto GetTimeIndex()->int;
	auto SetTimePoint(double time_point)->void;
	auto GetTimePoint()->double;
	auto SetTimeInterval(double interval)->void;
	auto GetTimeInterval()->double;
	auto GetKeys(int cell_idx)->QList<QString>;
	auto GetCellCount()->int;
	auto Clear()->void;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};