#pragma once

#include <memory>
#include <QMetaType>
#include <QString>
#include <QVariantMap>
#include <IBaseImage.h>

#include "TCDataTypesExport.h"


class TCDataTypes_API TCImage : public IBaseImage {
public:
    typedef std::shared_ptr<unsigned short[]> VolumePtr;

    typedef TCImage Self;
    typedef std::shared_ptr<Self> Pointer;

    TCImage();
    TCImage(const TCImage& other);
    virtual ~TCImage();

    auto SetFilePath(const QString& path)->void;
    auto GetFilePath()const->QString;

    auto GetTimeSteps()const->int;

    auto Init()->void;
    auto GetCenter(void)const->std::tuple<int, int, int>;
    auto GetSize(void)const->std::tuple<int, int, int>;
    auto SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1)->void;
    auto GetBoundingBox(void)const->std::tuple<int, int, int, int, int, int>;
    auto SetMinMax(int min, int max)->void;
    auto GetMinMax()const->std::tuple<int, int>;
    auto SetOriginalMinMax(float min, float max)->void;
    auto GetOriginalMinMax()const->std::tuple<float, float>;

    auto SetTimeStep(int step)->void;
    auto GetTimeStep()->int;

    auto SetImageVolume(VolumePtr volume)->void;
    auto GetImageVolume()->VolumePtr;

    auto addOffset(const float offset) -> void override;
    auto SetOffset(float offset)->void;
    auto GetOffset()const->float;
    auto getValue() -> int override;
    auto clone() -> IBaseImage* override;
    auto setChannel(int ch) -> void override;
    auto getChannel() -> int override;

    auto GetBuffer(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void* override;
    auto GetBuffer() -> void* override;

    auto SetResolution(double res[3])->void;
    auto GetResolution(double res[3])->void;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};

class TCDataTypes_API RoiXY {
public:
    RoiXY() {
        xOffset = -1;
        yOffset = -1;
        xSize = -1;
        ySize = -1;
    }
    ~RoiXY() = default;
    auto setOffset(int32_t offsetX, int32_t offsetY)->void {
        xOffset = offsetX;
        yOffset = offsetY;
    }
    auto setSize(int32_t sizeX, int32_t sizeY)->void {
        xSize = sizeX;
        ySize = sizeY;
    }
    auto getOffset()->std::tuple<int32_t, int32_t> {
        return std::make_tuple(xOffset, yOffset);
    }
    auto getSize()->std::tuple<int32_t, int32_t> {
        return std::make_tuple(xSize, ySize);
    }
    auto getMap()->QVariantMap {
        QVariantMap roiMap;
        roiMap["xOffset"] = xOffset;
        roiMap["yOffset"] = yOffset;
        roiMap["xSize"] = xSize;
        roiMap["ySize"] = ySize;
        return roiMap;
    }
private:
    int32_t xOffset;
    int32_t yOffset;
    int32_t xSize;
    int32_t ySize;
};

Q_DECLARE_METATYPE(RoiXY)