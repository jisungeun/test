#pragma once

#include <memory>
#include "IBaseImage.h"
#include "VolumeMetaInfo.h"
#include "TCDataTypesExport.h"

#include "VolumeViz/nodes/SoVolumeData.h"

class TCDataTypes_API ImageRaw : public IBaseImage{
public:
    using Self = ImageRaw;
    using SuperClass = IBaseImage;
    using Pointer = std::shared_ptr<Self>;
    
public:
    ImageRaw();
    virtual ~ImageRaw();

    static auto New(TC::IO::VolumeMetaInfo::Pointer meta)->IBaseImage::Pointer;
    static auto New(SoVolumeData* vol)->IBaseImage::Pointer;

    auto clone()->IBaseImage* override;

    auto addOffset(const float offset)->void override;
    auto getValue(void)->int override;

    auto GetBuffer(void)->void* override;
    auto GetBuffer(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void* override;

    auto SetMetaInfo(TC::IO::VolumeMetaInfo::Pointer info)->void;
    auto GetMetaInfo(void)->TC::IO::VolumeMetaInfo::Pointer;

    auto GetVolume(bool fromReader)->SoVolumeData*;

protected:
    ImageRaw(const ImageRaw& ohter) = delete;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};