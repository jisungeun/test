#include <ctkDictionary.h>
#include "TCEvent.h"

namespace TC::Framework {
    struct TCEvent::Impl {
        ctkDictionary properties;
    };

    TCEvent::TCEvent() : d{new Impl} {
    }

    TCEvent::~TCEvent() {
    }

    auto TCEvent::AddProperty(const QString& key, const QVariant& value) -> void {
        d->properties[key] = value;
    }

    auto TCEvent::GetProperty()const->ctkDictionary {
        return d->properties;
    }

    auto TCEvent::GetProperty(const QString& key) const -> QVariant {
        return d->properties[key];
    }

    auto TCEvent::GetPropertyNames() const -> QStringList {
        return d->properties.keys();
    }
}
