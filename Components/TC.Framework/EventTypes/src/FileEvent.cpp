#include "FileEvent.h"

namespace TC::Framework {
    using ValueCode = FileTypeEnum;
    struct FileEvent::Impl {
        ValueCode code{ ValueCode::NONE };
        ctkEvent e;
    };
    FileEvent::FileEvent() : d(new Impl) {

    }
    FileEvent::FileEvent(ctkEvent e) {
        d->e = e;
    }
    FileEvent::~FileEvent() {

    }
    auto FileEvent::New() -> Pointer {
        Pointer rEvent{ new FileEvent };
        return rEvent;
    }
    auto FileEvent::New(ctkEvent e) -> Pointer {
        Pointer rEvent{ new FileEvent(e) };
        return rEvent;
    }
    auto FileEvent::setEvent(ctkEvent e) -> void {
        d->e = e;
    }
}