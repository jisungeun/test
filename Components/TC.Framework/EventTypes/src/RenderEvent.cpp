#include "RenderEvent.h"

namespace TC::Framework {
    using ValueCode = RenderTypeEnum;
    struct RenderEvent::Impl {
        ValueCode code{ ValueCode::NONE };
        ctkEvent e;
    };
    RenderEvent::RenderEvent() : d(new Impl) {

    }
    RenderEvent::RenderEvent(ctkEvent e) {
        d->e = e;
    }
    RenderEvent::~RenderEvent() {

    }
    auto RenderEvent::New() -> Pointer {
        Pointer rEvent{ new RenderEvent };
        return rEvent;
    }
    auto RenderEvent::New(ctkEvent e) -> Pointer {
        Pointer rEvent{ new RenderEvent(e) };
        return rEvent;
    }
    auto RenderEvent::setEvent(ctkEvent e) -> void {
        d->e = e;
    }
}