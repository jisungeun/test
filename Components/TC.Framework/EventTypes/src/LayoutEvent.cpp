#include "LayoutEvent.h"

namespace TC::Framework {
    using  ValueCode = LayoutTypeEum;

    Qt::DockWidgetArea AreaType[6] = {
        Qt::DockWidgetArea::BottomDockWidgetArea,
        Qt::DockWidgetArea::TopDockWidgetArea,
        Qt::DockWidgetArea::LeftDockWidgetArea,
        Qt::DockWidgetArea::RightDockWidgetArea,
        Qt::DockWidgetArea::AllDockWidgetAreas,
        Qt::DockWidgetArea::NoDockWidgetArea
    };
    Qt::Orientation OrientationType[2] = {
        Qt::Orientation::Horizontal,
        Qt::Orientation::Vertical
    };

    struct LayoutEvent::Impl {        
        ctkEvent e;
        ValueCode action{ ValueCode::DEFAULT };
        single_layout slayout;
        TCLayout layout;
        QString proc_Name;
    };
    LayoutEvent::LayoutEvent() : d(new Impl){
        
    }
    LayoutEvent::LayoutEvent(ctkEvent e) : d(new Impl) {
        d->e = e;
    }
    LayoutEvent::~LayoutEvent() {

    }
    auto LayoutEvent::New() -> Pointer {
        Pointer lEvent{ new LayoutEvent };
        return lEvent;
    }
    auto LayoutEvent::New(ctkEvent e) -> Pointer {
        Pointer lEvent{ new LayoutEvent(e) };
        return lEvent;
    }
    auto LayoutEvent::setEvent(ctkEvent e) -> void {
        d->e = e;
    }
    auto LayoutEvent::getLayout() -> single_layout {
        if (!d->e.isNull()) {
            parseLayoutEvent();
        }
        return d->slayout;
    }
    auto LayoutEvent::getProccessingName() -> QString {
        return d->proc_Name;
    }
    auto LayoutEvent::getWholeLayout()->TCLayout{        
        return d->layout;
    }
    auto LayoutEvent::parseLayoutEvent() -> void {        
        for (int i = 0; i < ValueCode::_size(); i++) {            
            auto prop = d->e.getProperty(ValueCode::_from_index(i)._to_string());
            if (prop.isValid()) {
                auto idx = ValueCode::_from_index(i);
                if(idx < 200) {
                    //set layout action                    
                    d->action = idx;
                } else if(idx < 300){
                    //parse single dock parameter
                    switch (idx) {                        
                    case ValueCode::DOCK_AREA:
                        getDockArea();
                        break;
                    case ValueCode::DOCK_ORIENTATION:
                        getDockOrientation();
                        break;
                    case ValueCode::DOCK_LEFT_MARGIN:
                        getDockLeftMargin();
                        break;
                    case ValueCode::DOCK_RIGHT_MARGIN:
                        getDockRightMargin();
                        break;
                    case ValueCode::DOCK_WIDGET_WITGH:
                        getDockWidth();
                        break;
                    case ValueCode::DOCK_WIDGET_HEIGHT:
                        getDockHeight();
                        break;
                    case ValueCode::DOCK_NAME:
                        getDockName();
                        break;
                    case ValueCode::DOCK_REF_NAME:
                        getRefDockName();
                        break;
                    case ValueCode::DOCK_LOCATION:
                        getDockLocation();                        
                        break;
                    case ValueCode::PROC_TITLE:
                        getProcTitle();
                    }
                }else if(idx < 400) {
                    //parse overall layout parameter
                    switch(idx) {
                    case ValueCode::LAYOUT_LEFT_DOCKAREA:
                        break;
                    case ValueCode::LAYOUT_RIGHT_DOCKAREA:
                        break;
                    case ValueCode::LAYOUT_TOP_DOCKAREA:
                        break;
                    case ValueCode::LAYOUT_BOTTOM_DOCKAREA:
                        break;
                    }
                }
            }
        }                        
    }
    auto LayoutEvent::getProcTitle() -> void {
        ValueCode code = ValueCode::PROC_TITLE;
        auto prop = d->e.getProperty(code._to_string());
        d->proc_Name = prop.toString();
    }

    auto LayoutEvent::getDockName()->void {
        ValueCode code = ValueCode::DOCK_NAME;
        auto prop = d->e.getProperty(code._to_string());
        d->slayout.target_widget = prop.toString();
    }
    auto LayoutEvent::getRefDockName() -> void {
        ValueCode code = ValueCode::DOCK_REF_NAME;
        auto prop = d->e.getProperty(code._to_string());
        d->slayout.reference_widget = prop.toString();
    }

    auto LayoutEvent::getDockArea() -> void {
        ValueCode tmp = ValueCode::DOCK_AREA;
        auto prop = d->e.getProperty(tmp._to_string());
        auto idx = prop.toUInt();
        d->slayout.area = AreaType[idx];
    }
    auto LayoutEvent::getDockOrientation() -> void {
        ValueCode tmp = ValueCode::DOCK_ORIENTATION;
        auto prop = d->e.getProperty(tmp._to_string());
        auto idx = prop.toUInt();
        d->slayout.orientation = OrientationType[idx];
    }
    auto LayoutEvent::getDockLocation() -> void {
        ValueCode tmp = ValueCode::DOCK_LOCATION;
        auto prop = d->e.getProperty(tmp._to_string());
        auto idx = prop.toUInt();
        d->slayout.location = LocationTypeEnum::_from_index(idx);
    }

    auto LayoutEvent::getDockLeftMargin() -> void {
        ValueCode tmp = ValueCode::DOCK_LEFT_MARGIN;
        auto prop = d->e.getProperty(tmp._to_string());
        auto left_margin = prop.toDouble();
        d->slayout.leftMargin = left_margin;
    }
    auto LayoutEvent::getDockRightMargin() -> void {
        ValueCode tmp = ValueCode::DOCK_RIGHT_MARGIN;
        auto prop = d->e.getProperty(tmp._to_string());
        auto right_margin = prop.toDouble();
        d->slayout.rightMargin = right_margin;
    }
    auto LayoutEvent::getDockWidth() -> void {
        ValueCode tmp = ValueCode::DOCK_WIDGET_WITGH;
        auto prop = d->e.getProperty(tmp._to_string());
        auto width = prop.toDouble();
        d->slayout.width = width;
    }
    auto LayoutEvent::getDockHeight() -> void {
        ValueCode tmp = ValueCode::DOCK_WIDGET_HEIGHT;
        auto prop = d->e.getProperty(tmp._to_string());
        auto height = prop.toDouble();
        d->slayout.height = height;
    }
    auto LayoutEvent::getWindowName() -> void {
        ValueCode tmp = ValueCode::DOCK_WINDOW_TITLE;
        auto prop = d->e.getProperty(tmp._to_string());
        auto title = prop.toString();
        d->slayout.window_title = title;
    }

    auto LayoutEvent::getAction() -> int {
        if(!d->e.isNull()) {
            parseLayoutEvent();
        }
        return d->action;
    }

}