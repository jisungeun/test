#include "AppEvent.h"

namespace TC::Framework {
	struct AppEvent::Impl {
		AppTypeEnum appType{ AppTypeEnum::NONE };				
	};

	AppEvent::AppEvent() : TCEvent(), d{ new Impl } {

	}
	AppEvent::AppEvent(ctkEvent e) : TCEvent(), d{ new Impl } {
		SetEvent(e);
	}

	AppEvent::AppEvent(AppTypeEnum type) : TCEvent(), d{ new Impl } {
		d->appType = type;
		AddProperty(type._to_string(), "True");
	}

	AppEvent::~AppEvent() {

	}
	auto AppEvent::New() -> Pointer {
		Pointer newEvent{ new AppEvent };
		return newEvent;
	}
	auto AppEvent::New(ctkEvent e) -> Pointer {
		Pointer newEvent{ new AppEvent(e) };
		return newEvent;
	}

	auto AppEvent::getEventType() -> AppTypeEnum {
		return d->appType;
	}
	auto AppEvent::getFullName() -> QString {
		return GetProperty("FullName").toString();
	}
	auto AppEvent::getAppName() -> QString {
		return GetProperty("AppName").toString();
	}	

	auto AppEvent::isClosable() const -> bool {
		if (GetPropertyNames().contains("Closable")) {
			return GetProperty("Closable").toBool();
		}
		return true;
	}

	auto AppEvent::SetEvent(const ctkEvent& evt) -> bool {		
		for (size_t i = 0; i < AppTypeEnum::_size(); i++) {
			AppTypeEnum en = AppTypeEnum::_values()[i];
			if (evt.getProperty(en._to_string()).isValid()) {
				d->appType = en;				
				for (auto key : evt.getPropertyNames()) {
					AddProperty(key, evt.getProperty(key));
				}
				return true;
			}
		}
		return false;
	}

	auto AppEvent::GetEvent() const -> ctkEvent {		
		auto properties = GetProperty();
		ctkEvent evt(Topic(), properties);
		return evt;
	}

	auto AppEvent::getSenderName() -> QString {
		if (GetPropertyNames().contains("SenderName")) {
			return GetProperty("SenderName").toString();
		}
		return "";
	}
	auto AppEvent::setFullName(QString fullName) -> void {
		AddProperty("FullName", fullName);
	}
	auto AppEvent::setAppName(QString AppName) -> void {
		AddProperty("AppName", AppName);
	}
	auto AppEvent::addParameter(QString paramName, QVariant param) -> void {
		AddProperty(paramName, param);
	}

	auto AppEvent::getParameterNames() const -> QStringList {
		return GetPropertyNames();
	}

	auto AppEvent::getParameter(QString paramName) -> QVariant {
		return GetProperty(paramName);
	}
		
	auto AppEvent::setClosable(bool close) -> void {
		AddProperty("Closable", close);
	}
}