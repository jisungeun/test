#include "MenuEvent.h"

namespace TC::Framework {
    using ValueCode = MenuTypeEnum;
    struct MenuEvent::Impl {
        ValueCode code{ ValueCode::NONE };
        ctkEvent e;

        QString script = "";
        MenuTypeEnum menuType{ MenuTypeEnum::NONE };

        QList<QMenu*> tmp_menu;
        QList<QAction*> tmp_action;
        QStringList tmp_names;        
    };

    MenuEvent::MenuEvent() : TCEvent(), d{ new Impl } {
    }

    MenuEvent::MenuEvent(MenuTypeEnum menuType) : TCEvent(), d{new Impl} {
        d->menuType = menuType;
    }

    MenuEvent::MenuEvent(ctkEvent e) : TCEvent(), d{new Impl} {
        SetEvent(e);
    }

    MenuEvent::~MenuEvent() {
        
    }
    auto MenuEvent::getScript() const -> QString {
        //[AddCommand].[Parent].[Name]
        //[GroupCommand].[ActionList]
        return d->script;
    }

    auto MenuEvent::scriptAddMenu(QString menu_name, QString parent) -> void {
        QString tmp = "AddMenu.";
        if (parent.length() < 1) {//RootNode
            tmp += "Root.";
        }else {
            tmp += parent;
            tmp += ".";
        }
        tmp += menu_name;
        tmp += "!";
        d->script += tmp;
    }

    auto MenuEvent::scriptAddAction(QString action_name, QString parent_menu_name, int checked) -> void {
        QString tmp = "AddAction.";
        tmp += parent_menu_name;
        tmp += ".";
        tmp += action_name;        
        if(checked==1) {
            tmp += ".";
            tmp += "Checked";
        }
        else if(checked == 0) {
            tmp += ".";
            tmp += "UnChecked";
        }
        tmp += "!";
        d->script += tmp;
    }

    auto MenuEvent::scriptAddGroup(QStringList action_list) -> void {
        QString tmp = "AddGroup.";
        for(int i=0;i<action_list.size()-1;i++) {            
            tmp += action_list[i];
            tmp += "#";
        }
        tmp += action_list[action_list.size() - 1];
        tmp += "!";
        d->script += tmp;
    }

    auto MenuEvent::scriptSet(const QString& name) -> void {
        d->script = name;
    }

    auto MenuEvent::New() -> Pointer {
        Pointer newEvent{ new MenuEvent };
        return newEvent;
    }
    auto MenuEvent::New(ctkEvent e) -> Pointer {
        Pointer newEvent{ new MenuEvent(e) };
        return newEvent;
    }

    auto MenuEvent::SetEvent(const ctkEvent& evt) -> bool {
        d->e = evt;

        auto props = d->e.getPropertyNames();
        for(auto type : MenuTypeEnum::_values()) {
            if(props.contains(type._to_string())) {
                d->menuType = type;
                d->script = d->e.getProperty(type._to_string()).toString();
                break;
            }
        }

        return true;
    }

    auto MenuEvent::GetEvent() const -> ctkEvent {
        ctkDictionary properties;
        properties[d->menuType._to_string()] = getScript();

        auto commonProps = GetPropertyNames();
        for(auto prop : commonProps) {
            properties[prop] = GetProperty(prop);
        }

        ctkEvent evt(Topic(), properties);
        return evt;
    }

    auto MenuEvent::IsType(MenuTypeEnum type) const -> bool {
        return (d->menuType == type);
    }

    auto MenuEvent::getSenderName() -> QString {
        auto prop = d->e.getProperty("SenderName");
        if(prop.isValid()) {
            return prop.toString();
        }
        return"";
    }

    auto MenuEvent::getMenuScript() -> QString {
        if(d->e.isNull()) {
            return "";
        }

        d->code = ValueCode::MenuScript;
        auto prop = d->e.getProperty(d->code._to_string());
        if(!prop.isValid()) {
            return "";
        }

        auto script = prop.toString();
        return script;
    }

    auto MenuEvent::getTitleInfo() -> QString {
        if (d->e.isNull()) {
            return "";
        }
        d->code = ValueCode::TitleBar;
        auto prop = d->e.getProperty(d->code._to_string());
        if(!prop.isValid()) {
            return "";
        }

        return prop.toString();
    }

    auto MenuEvent::constructMenu(QMenuBar* mb) -> QStringList {
        d->tmp_action.clear();
        d->tmp_menu.clear();
        d->tmp_names.clear();

        auto script = getMenuScript();
        if (script.length() < 1)
            return d->tmp_names;
        auto list = script.split("!");
        for (int i = 0; i < list.size(); i++) {
            auto commandline = list[i].split(".");
            auto command = commandline[0];            
            if (command.compare("AddMenu") == 0) {
                auto parent = commandline[1];
                auto name = commandline[2];
                if (parent.compare("Root") == 0) {
                    addMenu(name, mb);
                } else {
                    auto pmenu = findMenu(parent);
                    addMenu(name, pmenu);
                }
            } else if (command.compare("AddAction") == 0) {
                auto parent = commandline[1];
                auto name = commandline[2];
                auto pmenu = findMenu(parent);
                if(commandline.size()<4)
                    addAction(name, pmenu);
                else {
                    auto ch = commandline[3];
                    if(ch.compare("UnChecked")==0) {
                        addAction(name, pmenu,2);
                    }else if(ch.compare("Checked")==0) {
                        addAction(name, pmenu, 1);
                    }else {
                        addAction(name, pmenu, 0);
                    }
                }
            } else if(command.compare("AddGroup")==0) {
                auto action_list = commandline[1];
                addGroup(action_list);
            }
        }
        return d->tmp_names;
    }

    auto MenuEvent::addMenu(QString menu_name, QMenuBar* menuBar) -> void {
        auto m = menuBar->addMenu(menu_name);        
        d->tmp_menu.push_back(m);
    }

    auto MenuEvent::addMenu(QString menu_name, QMenu* menu) -> void {
        auto m = menu->addMenu(menu_name);
        d->tmp_menu.push_back(m);
    }

    auto MenuEvent::addAction(QString action_name, QMenu* menu,int check) -> void {
        if (action_name.compare("Separator") == 0) {
            auto a = menu->addSeparator();
            d->tmp_action.push_back(a);
        } else {
            auto a = menu->addAction(action_name);
            d->tmp_names.push_back(action_name);
            if(check>0) {
                a->setCheckable(true);
                if (check == 1)
                    a->setChecked(true);
                else
                    a->setChecked(false);
            }
            d->tmp_action.push_back(a);
        }
    }

    auto MenuEvent::addGroup(QString action_list) -> void {
        auto actions = action_list.split("#");
        auto actionGroup = new QActionGroup(nullptr);
        for(int i=0;i<actions.size();i++) {
            auto a = findAction(actions[i]);
            if (a)
                actionGroup->addAction(a);            
        }        
    }    
    auto MenuEvent::findMenu(QString menu_name) -> QMenu* {
        auto idx = -1;
        for(int i=0;i<d->tmp_menu.size();i++) {
            if(menu_name.compare(d->tmp_menu[i]->title())==0) {
                idx = i;
            }
        }
        if (idx < 0)
            return nullptr;
        else
            return d->tmp_menu[idx];
    }
    auto MenuEvent::findAction(QString action_name) -> QAction* {
        auto idx = -1;
        for(int i=0;i<d->tmp_action.size();i++) {
            if(action_name.compare(d->tmp_action[i]->text())==0) {
                idx = i;
            }
        }
        if (idx < 0)
            return nullptr;
        else
            return d->tmp_action[idx];
    }

}