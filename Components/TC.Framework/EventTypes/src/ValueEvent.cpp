#include "ValueEvent.h"

namespace TC::Framework {
    struct ValueEvent::Impl {
        ScalarTypeEnum code{ ScalarTypeEnum::NONE };
        QVariant value;
        ctkEvent e;
    };

    ValueEvent::ValueEvent() : d( new Impl ) {
        
    }

    ValueEvent::ValueEvent(ScalarTypeEnum type) {
        d->code = type;
    }

    ValueEvent::ValueEvent(ctkEvent e) : d( new Impl ) {
        d->e = e;
    }
    ValueEvent::~ValueEvent() {
        
    }
    auto ValueEvent::New() -> Pointer {
        Pointer vEvent{ new ValueEvent };
        return vEvent;
    }
    auto ValueEvent::New(ctkEvent e) -> Pointer {
        Pointer vEvent{ new ValueEvent(e) };
        vEvent->d->e = e;
        return vEvent;
    }

    auto ValueEvent::SetEvent(const ctkEvent& evt) -> bool {
        d->e = evt;
        return true;
    }

    auto ValueEvent::GetEvent() const -> ctkEvent {
        ctkDictionary properties;
        properties[d->code._to_string()] = d->value;

        auto commonProps = GetPropertyNames();
        for(auto prop : commonProps) {
            properties[prop] = GetProperty(prop);
        }

        ctkEvent evt(Topic(), properties);
        return evt;
    }

    auto ValueEvent::getSenderName() -> QString {
        auto prop = d->e.getProperty("SenderName");
        if(prop.isValid()) {
            return prop.toString();
        }
        return "";
    }

    auto ValueEvent::setValue(const QVariant& value) -> void {
        d->value = value;
    }

    auto ValueEvent::getValue(QString& value) -> bool {
        if (d->e.isNull()) {
            value = QString();
            return false;
        }

        d->code = ScalarTypeEnum::STRING;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = QString();
            return false;
        }

        value = prop.toString();
        return true;
    }
    auto ValueEvent::getValue(bool& value) -> bool {
        if (d->e.isNull()) {
            value = false;
            return false;
        }

        d->code = ScalarTypeEnum::BOOL;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = false;
            return false;
        }

        value = prop.toBool();
        return true;
    }
    auto ValueEvent::getValue(char& value) -> bool {
        if (d->e.isNull()) {
            value = ' ';
            return false;
        }

        d->code = ScalarTypeEnum::CHAR;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = ' ';
            return false;
        }

        value = prop.toChar().toLatin1();
        return true;
    }
    auto ValueEvent::getValue(double& value) -> bool {
        if (d->e.isNull()) {
            value = -1.0;
            return false;
        }

        d->code = ScalarTypeEnum::DOUBLE;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = -1.0;
            return false;
        }

        value = prop.toDouble();
        return true;
    }
    auto ValueEvent::getValue(float& value) -> bool {
        if (d->e.isNull()) {
            value = -1.0;
            return false;
        }

        d->code = ScalarTypeEnum::FLOAT;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = -1.0;
            return false;
        }

        value = prop.toFloat();
        return true;
    }
    auto ValueEvent::getValue(int& value) -> bool {
        if (d->e.isNull()) {
            value = -1;
            return false;
        }

        d->code = ScalarTypeEnum::INTEGER;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = -1;
            return false;
        }

        value = prop.toInt();
        return true;
    }
    auto ValueEvent::getValue(long long& value) -> bool {
        if (d->e.isNull()) {
            value = -1;
            return false;
        }

        d->code = ScalarTypeEnum::LONG_LONG;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = -1;
            return false;
        }

        value = prop.toLongLong();
        return true;
    }
    auto ValueEvent::getValue(unsigned long long& value) -> bool {
        if (d->e.isNull()) {
            value = 0;
            return false;
        }

        d->code = ScalarTypeEnum::UNSIGNED_LONG_LONG;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = 0;
            return false;
        }

        value = prop.toLongLong();
        return true;
    }
    auto ValueEvent::getValue(unsigned& value) -> bool {
        if (d->e.isNull()) {
            value = 0;
            return false;
        }

        d->code = ScalarTypeEnum::UNSIGNED_INTEGER;
        auto prop = d->e.getProperty(d->code._to_string());
        if (!prop.isValid()) {
            value = 0;
            return false;
        }

        value = prop.toUInt();
        return true;
    }
}