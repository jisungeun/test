#pragma once

#include <memory>
#include <enum.h>
#include <TCEvent.h>

#include "TCEventTypesExport.h"

namespace TC::Framework {
	BETTER_ENUM(AppTypeEnum, int,
				NONE = -1,
				APP_WITHOUT_ARG = 0,
				APP_WITH_SHARE = 1,				
				APP_WITH_ARGS = 2,
				ARGS_ONLY = 3,
		        APP_UPDATE = 4,
		        APP_CLOSE =5
				)

	class TCEventTypes_API AppEvent : public TCEvent {
	public:
	    typedef AppEvent Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		AppEvent();
		AppEvent(ctkEvent e);
		AppEvent(AppTypeEnum type);
		virtual ~AppEvent();

		static auto New()->Pointer;
		static auto New(ctkEvent e)->Pointer;

		static auto Topic()->QString {
			return "Application";
		}

        auto SetEvent(const ctkEvent& evt)->bool override;
        auto GetEvent() const->ctkEvent override;

		auto getSenderName(void)->QString;

		//parse application event
		auto getEventType()->AppTypeEnum;
		auto getFullName()->QString;
		auto getAppName()->QString;
		auto getShareName()->QString;
		auto isClosable() const -> bool;

		//construction of app script
		auto setFullName(QString fullName)->void;
		auto setAppName(QString AppName)->void;		
		auto addParameter(QString paramName, QVariant param)->void;
		auto getParameterNames() const->QStringList;
		auto getParameter(QString paramName)->QVariant;
		auto setClosable(bool close) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}