#pragma once

#include <memory>

#include "service/event/ctkEvent.h"
#include "enum.h"
#include "TCEventTypesExport.h"

namespace TC::Framework {
    #define FILE_EVENT "file"
    BETTER_ENUM(FileTypeEnum, int,
        NONE = -1,
        OPEN = 0,
        SAVE = 1,
        SAVE_AS = 2
    )
    
    class TCEventTypes_API FileEvent {
    public:
        typedef std::shared_ptr<FileEvent> Pointer;

    public:
        FileEvent();
        FileEvent(ctkEvent e);

        virtual ~FileEvent();;

        static auto New()->Pointer;
        static auto New(ctkEvent e)->Pointer;

        auto setEvent(ctkEvent e)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}