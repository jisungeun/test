#pragma once

#include <memory>

#include "service/event/ctkEvent.h"
#include "enum.h"
#include "TCEventTypesExport.h"

namespace TC::Framework {
    #define RENDER_EVENT "Render"
    BETTER_ENUM(RenderTypeEnum, int,
                RENDER = 30,
                MODIFIED = 31,
                MOUSE_DRAG = 32,
                MOUSE_CLICK = 33,
                MOUSE_RELEASE = 34,
                MOUSE_WHEEL = 35,
                KEY_PRESS = 36,
                NONE = 37
    )    
    typedef struct mouseInfo {
        double pos_x;
        double pos_y;
        double pos_z;
        double pos_wheel;
        int btn; // 0:left 1:right 2:middle
    }Mouse;
    typedef struct keyboardInfo {
        QString key;
    }Keyboard;

    class TCEventTypes_API RenderEvent {
    public:
        typedef std::shared_ptr<RenderEvent> Pointer;        

    public:
        RenderEvent();
        RenderEvent(ctkEvent e);

        virtual ~RenderEvent();;

        static auto New()->Pointer;
        static auto New(ctkEvent e)->Pointer;

        auto setEvent(ctkEvent e)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}