#pragma once
#include <memory>
#include <QMenuBar>

#include <enum.h>
#include "TCEvent.h"
#include "TCEventTypesExport.h"

namespace TC::Framework {
    BETTER_ENUM(MenuTypeEnum, int,
                NONE = -1,
                MenuScript = 0,
                Action = 1,
                Separator = 2,
                ActionGroup = 3,
                ActionChecked = 4,
                ActionUnchecked = 5,
                TitleBar = 100,
                ParameterSet = 101,
                BatchFinish = 102,
                AbortTab = 103,
                CloseProgram = 200
                )

    class TCEventTypes_API MenuEvent : public TCEvent {
    public:
        typedef MenuEvent Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        MenuEvent();
        MenuEvent(MenuTypeEnum menuType);
        MenuEvent(ctkEvent e);

        virtual ~MenuEvent();

        static auto New()->Pointer;
        static auto New(ctkEvent e)->Pointer;

        static auto Topic()->QString {
            return "Menu";
        }

        auto SetEvent(const ctkEvent& evt)->bool override;
        auto GetEvent() const->ctkEvent override;

        auto IsType(MenuTypeEnum type) const->bool;

        auto getSenderName(void)->QString;//not used

        auto getTitleInfo(void)->QString;

        //construct menu
        auto getScript() const->QString;
        auto scriptAddMenu(QString menu_name, QString parent = QString())->void;
        auto scriptAddAction(QString action_name, QString parent_menu_name, int checked = -1)->void;
        auto scriptAddGroup(QStringList action_list)->void;
        auto scriptSet(const QString& name)->void;

        //parse menu
        auto constructMenu(QMenuBar* mb)->QStringList;
        
    private:
        auto getMenuScript(void)->QString;
        auto addMenu(QString menu_name, QMenuBar* menuBar)->void;
        auto addMenu(QString menu_name, QMenu* menu)->void;
        auto addAction(QString action_name, QMenu* menu, int check = 0)->void;
        auto addGroup(QString action_list)->void;

        auto findMenu(QString menu_name)->QMenu*;
        auto findAction(QString action_name)->QAction*;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
