#pragma once
#include <memory>
#include <enum.h>

#include "TCEvent.h"
#include "TCEventTypesExport.h"

namespace TC::Framework {
    BETTER_ENUM(ScalarTypeEnum, int,
                CHAR = 10,
                INTEGER = 11,
                UNSIGNED_INTEGER = 12,
                LONG_LONG = 13,
                UNSIGNED_LONG_LONG = 14,
                FLOAT = 15,
                DOUBLE = 16,
                BOOL = 17,
                STRING = 18,
                NONE = 19
    )

    class TCEventTypes_API ValueEvent : public TCEvent {
    public:
        typedef std::shared_ptr<ValueEvent> Pointer;
        //typedef ScalarTypeEnum ValueCode;        

    public:
        ValueEvent();
        ValueEvent(ScalarTypeEnum type);
        ValueEvent(ctkEvent e);

        virtual ~ValueEvent();

        static auto New()->Pointer;
        static auto New(ctkEvent e)->Pointer;

        static auto Topic()->QString {
            return "Value";
        }

        auto SetEvent(const ctkEvent& evt) -> bool override;
        auto GetEvent() const -> ctkEvent override;

        auto getSenderName(void)->QString;

        auto setValue(const QVariant& value)->void;

        auto getValue(char& value)->bool;
        auto getValue(int& value)->bool;
        auto getValue(unsigned int& value)->bool;
        auto getValue(long long& value)->bool;
        auto getValue(unsigned long long& value)->bool;
        auto getValue(float& value)->bool;
        auto getValue(double& value)->bool;
        auto getValue(bool& value)->bool;
        auto getValue(QString& value)->bool;

    private:

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}