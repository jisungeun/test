#pragma once
#include <memory>
#include <QStringList>

#include <service/event/ctkEvent.h>
#include "TCEventTypesExport.h"

namespace TC::Framework {
    class TCEventTypes_API TCEvent {
    public:
        TCEvent();
        virtual ~TCEvent();

        virtual auto SetEvent(const ctkEvent& evt)->bool = 0;
        virtual auto GetEvent() const->ctkEvent = 0;

        auto AddProperty(const QString& key, const QVariant& value)->void;
        auto GetProperty()const->ctkDictionary;
        auto GetProperty(const QString& key) const->QVariant;
        auto GetPropertyNames() const->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}