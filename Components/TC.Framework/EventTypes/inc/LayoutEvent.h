#pragma once

#include <memory>

#include "service/event/ctkEvent.h"
#include "enum.h"
#include "TCEventTypesExport.h"

namespace TC::Framework {
    #define LAYOUT_EVENT "Layout"
    BETTER_ENUM(LayoutTypeEum, int,
                SAVE_LAYOUT = 100,
                RESTORE_LAYOUT = 101,
                DOCK_ADD = 102,
                DOCK_REMOVE = 103,                
                DOCK_MOVE = 104,
                LAYOUT_RATIO = 105,
                TAB_ADD = 106,
                TAB_REMOVE = 107,

                DOCK_AREA = 200,                
                DOCK_ORIENTATION = 201,
                DOCK_LEFT_MARGIN = 202,
                DOCK_RIGHT_MARGIN = 203,
                DOCK_WIDGET_WITGH = 204,
                DOCK_WIDGET_HEIGHT = 205,
                DOCK_NAME = 206,
                DOCK_REF_NAME = 207,
                DOCK_LOCATION = 208,
                DOCK_WINDOW_TITLE = 209,
                PROC_TITLE = 210,

                LAYOUT_LEFT_DOCKAREA = 300,
                LAYOUT_RIGHT_DOCKAREA = 301,
                LAYOUT_BOTTOM_DOCKAREA = 302,
                LAYOUT_TOP_DOCKAREA = 303,

                DEFAULT = 400
    )
    BETTER_ENUM(LocationTypeEnum,int,
                LEFT_WINDOW = 0,
                RIGHT_WINDOW = 1,
                CENTER_WINDOW = 2,
                BOTTOM_WINDOW = 3,
                NO_WINDOW = -1
    )
    BETTER_ENUM(AreaTypeEnum, int,
                Bottom = 0,
                Top = 1,
                Left = 2,
                Right = 3,
                All = 4,
                No = 5
    )
    BETTER_ENUM(OrientationTypeEnum, int,
                Horizontal = 1,
                Vertical = 2
    )
    
    typedef struct layout_option{
        QString target_widget;
        QString window_title;        
        QString reference_widget = QString();
        double leftMargin;
        double rightMargin;
        double width;
        double height;
        LocationTypeEnum location{ LocationTypeEnum::NO_WINDOW };
        Qt::DockWidgetArea area;
        Qt::Orientation orientation;
        int floatingWidth = -1;
        int floatingHeight = -1;
    }single_layout;
    
    using TCLayout = std::vector<single_layout>;

    class TCEventTypes_API LayoutEvent {
    public:
        typedef std::shared_ptr<LayoutEvent> Pointer;        

    public:
        LayoutEvent();
        LayoutEvent(ctkEvent e);

        virtual ~LayoutEvent();;

        static auto New()->Pointer;
        static auto New(ctkEvent e)->Pointer;

        auto setEvent(ctkEvent e)->void;        
        auto getLayout()->single_layout;
        auto getWholeLayout()->TCLayout;
        auto getAction()->int;
        auto getProccessingName()->QString;

    private:
        auto parseLayoutEvent()->void;

        auto getDockName()->void;
        auto getRefDockName()->void;
        auto getWindowName()->void;

        auto getDockArea()->void;
        auto getDockLocation()->void;
        auto getDockOrientation()->void;
        auto getDockLeftMargin()->void;
        auto getDockRightMargin()->void;
        auto getDockWidth()->void;
        auto getDockHeight()->void;

        auto getProcTitle()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}