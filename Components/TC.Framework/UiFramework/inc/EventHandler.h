#pragma once

#include <memory>

#include <QString>

#include "ctkPluginContext.h"
#include "service/event/ctkEvent.h"
#include "TCUiFrameworkExport.h"

namespace TC::Framework {
	class TCUiFramework_API EventHandler :public QObject {
        Q_OBJECT	
	public:
		EventHandler();
		~EventHandler();

		auto setPluginContext(ctkPluginContext* context)->void;
		auto subscribeEvent(QString eventName)->void;
		auto closeSubscription(qlonglong iid)->void;

	    signals:
	    void sigCtkEvent(ctkEvent);

	public slots:
		void OnEventOccured(const ctkEvent& ctkEvent);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}