#pragma once

//Qt
#include <QStringList>
#include <QVariantList>

#include "IPackageManager.h"
#include "IAppInterface.h"

#include "IAppMeta.h"

#include "TCUiFrameworkExport.h"

namespace TC::Framework {
	class TCUiFramework_API IAppManager  {
	public:
		typedef IAppManager Self;
		typedef std::shared_ptr<Self> Pointer;

		explicit IAppManager();
		virtual ~IAppManager();

		auto Stop()->void;
				
			    
		virtual auto GetAppProperty(const QString& appName)->QVariantMap;
		virtual auto GetAppInterface(const QString& appName)->IAppInterface*;
		virtual auto AcquirePackage()->bool;		

		auto GetAppList()->QStringList;
		auto GetActiveAppList()->QStringList;
		auto Activate(const QString& appName)->bool;
		auto Deactivate(const QString& appName)->bool;

		auto GetPluginPath(const QString& appName)->QString;
		auto LoadAppPlugin(const QString& appName,const bool preLoad = false)->bool;
		auto RemoveAppPlugin(const QString& appName) -> void;
		auto UnloadAppPlugin(const QString& appName)->bool;
		auto AddPluginSearchPath(const QString& path)->void;
		auto GetPluginSearchPaths()->QStringList;		
		

	protected:
		virtual auto GetPackageManager()->IPackageManager::Pointer = 0;

	private:
		struct Impl;
		std::shared_ptr<Impl> d;
	};

	class TCUiFramework_API IAppMetaRegistry {
		typedef IAppMetaRegistry Self;
		typedef std::shared_ptr<Self> Pointer;
	public:
		~IAppMetaRegistry();

		static Pointer GetInstance();

		static auto LoadMeta(const QString& path)->int;
		static auto GetMeta(const QString& name, bool isPath = false)->IAppMeta::Pointer;

	protected:
		IAppMetaRegistry();
		IAppMetaRegistry(const IAppMetaRegistry& other) = delete;

		auto Register(IAppMeta* meta)->void;
		auto FindModule(const QString& name)->IAppMeta*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}