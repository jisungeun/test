#pragma once

#include <memory>
#include <QString>
#include <QVariantMap>

#include "TCUiFrameworkExport.h"

class TCUiFramework_API IAppMeta {
public:
	typedef std::shared_ptr<IAppMeta> Pointer;
public:
	IAppMeta() = default;
	virtual ~IAppMeta() = default;

	virtual auto GetName() const->QString = 0;
	virtual auto GetFullName() const->QString = 0;
	virtual auto GetMetaInfo() const->QVariantMap = 0;
	virtual auto clone() const->IAppMeta* = 0;	
};

#define IAppMeta_iid "org.tomocube.appmeta/1.0"
Q_DECLARE_INTERFACE(IAppMeta, IAppMeta_iid)