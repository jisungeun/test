#pragma once

#include <memory>

#include <QString>

#include <ctkPluginContext.h>

#include <TCEvent.h>

#include "TCUiFrameworkExport.h"

namespace TC::Framework {
    class TCUiFramework_API EventPublisher {	
    public:
		EventPublisher();
		~EventPublisher();

		auto setPluginContext(ctkPluginContext* context)->void;		
		auto publishSignal(TCEvent& evt, const QString& senderName)->void;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}