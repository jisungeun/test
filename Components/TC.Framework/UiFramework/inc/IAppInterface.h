#pragma once

#include <QObject>
#include <IMainWindow.h>

#include "TCUiFrameworkExport.h"

namespace TC::Framework {
    class TCUiFramework_API IAppInterface : public QObject {
        Q_OBJECT

    public:
        IAppInterface();
        ~IAppInterface();

        virtual auto GetDisplayTitle() const->QString = 0;
        virtual auto GetShortTitle() const->QString = 0;                        

        virtual auto GetInterfaceWidget()->IMainWindow* = 0;
        virtual auto Open(const QString& path)->bool = 0;
        virtual auto Execute(const QVariantMap& params)->bool = 0;

    protected:
        virtual auto Activate()->bool;
        virtual auto Deactivate()->bool;
        virtual auto isActivated()->bool;
        virtual auto GetProperties()->QVariantMap;

    private:
        friend class IAppManager;
    };
}

using namespace TC::Framework;
Q_DECLARE_INTERFACE(IAppInterface, "IAppInterface");