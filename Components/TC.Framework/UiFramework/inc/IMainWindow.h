#pragma once

#include <memory>

#include <QMainWindow>
#include <QWidget>
#include <IParameter.h>
#include "EventPublisher.h"
#include "EventHandler.h"


#include "TCUiFrameworkExport.h"

namespace TC::Framework {	

	class TCUiFramework_API IMainWindow : public QWidget {
		Q_OBJECT

	public:
		IMainWindow(QWidget* parent = nullptr);
		virtual ~IMainWindow();

		auto subscribeEvent(QString eventName)->void;
		auto publishSignal(TCEvent& evt, const QString& senderName)->void;
		auto connectEvent(const char* signal, const QObject* receiver, const char* member)->void;

	protected:
		virtual auto TryActivate()->bool = 0;
		virtual auto TryDeactivate()->bool = 0;
		virtual auto IsActivate()->bool = 0;
		virtual auto GetMetaInfo()->QVariantMap = 0;

	private:
		friend class IAppInterface;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}