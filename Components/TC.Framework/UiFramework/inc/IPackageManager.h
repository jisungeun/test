#pragma once

#include <QString>

#include <memory>

#include "TCUiFrameworkExport.h"

namespace TC::Framework {
    class TCUiFramework_API IPackageManager {
    public:
        typedef IPackageManager Self;
        typedef std::shared_ptr<Self> Pointer;

        explicit IPackageManager();
        virtual ~IPackageManager();
        virtual auto GetPackageAppList()->QStringList = 0;

    protected:
        virtual auto AcquirePackage()->bool = 0;
        virtual auto GetActivation(const QString& key)->bool = 0;

    private:
        friend class IAppManager;
    };
}