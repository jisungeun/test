#pragma once

#include <memory>
#include <QString>
#include <QMainWindow>

#include "IAppInterface.h"
#include "IAppManager.h"
#include "EventHandler.h"
#include "EventPublisher.h"
#include "TCUiFrameworkExport.h"

namespace TC::Framework {
    class TCUiFramework_API TabUiFramework : public QObject {
        Q_OBJECT
    public:
        typedef TabUiFramework Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        TabUiFramework(IAppManager::Pointer appManager);
        ~TabUiFramework();

        auto addUiFramework(const QString& name,QString tabName = QString()) -> void;
        auto findUiFramework(QString name)->QMainWindow*;
        auto getMainTab()->QTabWidget*;                
        auto initialize()->void;        
        auto setTabUnclosable(QString tabName)->void;
        auto findTabIdx(QString name)->int;
        auto setSuddenClose()->void;
        auto setTabsVisible(bool visible)->void;

        auto raiseTab(QString tabName)->void;
        auto focusTab(QString fullName)->void;

    signals:
        void sigTabClose(QString name);

    public slots:
        void OnCloseTab(int idx);
        void OnTabChanged(int idx);        
    
    private:
        TC::Framework::EventHandler* m_Handler;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}