#ifndef IAppActivator_H
#define IAppActivator_H

#include <memory>
#include <ctkPluginActivator.h>

#include <TCEvent.h>

#include "IAppInterface.h"
#include "TCUiFrameworkExport.h"

class SoSeparator;

namespace TC::Framework {
    class TCUiFramework_API IAppActivator : public QObject, public ctkPluginActivator {
        Q_OBJECT
        Q_INTERFACES(ctkPluginActivator)

    public:
        IAppActivator();
        virtual ~IAppActivator();

        virtual auto start(ctkPluginContext* context)->void = 0;
        virtual auto stop(ctkPluginContext* context)->void;

        virtual auto getPluginContext()->ctkPluginContext* const = 0;        
        
    protected:
        auto registerService(ctkPluginContext* context, IAppInterface* appInterface, QString name)->void;
    };
}

#endif