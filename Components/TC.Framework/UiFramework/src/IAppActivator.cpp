#include <QWidget>
#include <utility>

#include "IAppActivator.h"
#include "IAppInterface.h"

namespace TC::Framework {
    IAppActivator::IAppActivator() {
              
    }
    IAppActivator::~IAppActivator() {
        
    }
    
    auto IAppActivator::registerService(ctkPluginContext* context, IAppInterface* appInterface, QString name) -> void {
        ctkDictionary properties;
        properties["name"] = name;

        context->registerService<IAppInterface>(appInterface, properties);        
    }
    auto IAppActivator::stop(ctkPluginContext* context) -> void {
        Q_UNUSED(context);
        this->deleteLater();
    }
}