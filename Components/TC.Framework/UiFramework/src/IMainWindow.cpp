#include "IMainWindow.h"

#include <ctkPluginFrameworkLauncher.h>

namespace TC::Framework {
    struct IMainWindow::Impl {
        EventHandler* handler{ nullptr };
        EventPublisher* publisher{ nullptr };
    };
    IMainWindow::IMainWindow(QWidget* parent) :QWidget(parent), d{ new Impl }{
        d->handler = new EventHandler;
        d->publisher = new EventPublisher;

        d->handler->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
        d->publisher->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
    }
    IMainWindow::~IMainWindow() {
        delete d->handler;
        delete d->publisher;
    }
    auto IMainWindow::subscribeEvent(QString eventName) -> void {
        d->handler->subscribeEvent(eventName);
    }

    auto IMainWindow::publishSignal(TCEvent& evt, const QString& senderName) -> void {
        d->publisher->publishSignal(evt, senderName);
    }

    auto IMainWindow::connectEvent(const char* signal, const QObject* receiver, const char* member) -> void {
        connect(d->handler, signal, receiver, member);
    }
}