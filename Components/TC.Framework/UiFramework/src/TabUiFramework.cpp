#define LOGGER_TAG "[UiFramework]"

#include <iostream>
#include <filesystem>

//Qt
#include <QTabWidget>
#include <QTabBar>

#pragma warning(push)
#pragma warning(disable:4996)
#include "ctkPluginFrameworkLauncher.h"
#include "ctkPluginContext.h"
#include "ctkPluginException.h"
#include "service/event/ctkEventAdmin.h"
#pragma warning(pop)

#include <QLabel>

#include <ValueEvent.h>
#include <AdaptiveTabWidget.h>

#include "TabUiFramework.h"


#include <TCLogger.h>

namespace TC::Framework {
    struct TabUiFramework::Impl {        
        AdaptiveTabWidget* tabWidget;
        std::vector<QString> uiNameList;
        std::vector<QString> tabNameList;
        QString plugin_cache;
        
        bool sudden{ false };

        IAppManager::Pointer appManager{nullptr};
    };

    TabUiFramework::TabUiFramework(IAppManager::Pointer appManager) : d(new Impl) {
        //d->tabWidget = new QTabWidget;
        d->tabWidget = new AdaptiveTabWidget;
        d->appManager = appManager;
        
        connect(d->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(OnCloseTab(int)));
        connect(d->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(OnTabChanged(int)));

        d->tabWidget->setTabsClosable(true);
    }
    TabUiFramework::~TabUiFramework() {                
        auto tab_cnt = d->tabWidget->count();
        for (auto i = tab_cnt - 1; i > -1; i--) {
            d->tabWidget->removeTab(i);
        }
        delete d->tabWidget;
    }
   
    auto TabUiFramework::setSuddenClose() -> void {
        d->sudden = true;
    }

    auto TabUiFramework::setTabsVisible(bool visible) -> void {
        d->tabWidget->SetVisible(visible);
    }

    auto TabUiFramework::initialize() -> void {
        
    }

    auto TabUiFramework::focusTab(QString fullName) -> void {
        int idx = -1;
        for(auto i=0;i<d->uiNameList.size();i++) {
            if(d->uiNameList[i].compare(fullName)==0) {
                idx = i;
                break;
            }
        }
        if(idx>=0&&d->tabWidget->currentIndex()!=idx) {
            d->tabWidget->setCurrentIndex(idx);
        }
    }
    auto TabUiFramework::raiseTab(QString tabName) -> void {        
        int idx = -1;
        for (int i = 0;i < d->tabNameList.size();i++) {
            if(d->tabNameList[i].compare(tabName)==0) {
                idx = i;                
                break;
            }
        }

        if(idx==-1) {
            QLOG_WARN() << "It fails to find " << tabName << " from tab list";
        }

        if (idx >= 0 && d->tabWidget->currentIndex() != idx) {            
            d->tabWidget->setCurrentIndex(idx);            
        } else {            
            auto focusedTabName = tabName;
            auto context = ctkPluginFrameworkLauncher::getPluginContext();
            ctkServiceReference reference = context->getServiceReference<ctkEventAdmin>();
            if (reference) {
                auto eventAdmin = context->getService<ctkEventAdmin>(reference);
                ctkDictionary properties;
                properties["TabName"] = focusedTabName;
                properties["SenderName"] = "TabUiFramework";

                ctkEvent reportGenerateEvent("TabChange", properties);
                eventAdmin->sendEvent(reportGenerateEvent);
            }
        }
    }

    void TabUiFramework::OnTabChanged(int idx) {
        if (d->tabNameList.size() > idx && !d->sudden) {
            auto focusedTabName = d->tabNameList[idx];
            auto context = ctkPluginFrameworkLauncher::getPluginContext();
            ctkServiceReference reference = context->getServiceReference<ctkEventAdmin>();
            if (reference) {
                auto eventAdmin = context->getService<ctkEventAdmin>(reference);
                ctkDictionary properties;
                properties["TabName"] = focusedTabName;
                properties["SenderName"] = "TabUiFramework";

                ctkEvent reportGenerateEvent("TabChange", properties);
                eventAdmin->sendEvent(reportGenerateEvent);
            }
        }
    }

    auto TabUiFramework::addUiFramework(const QString& name,QString tabName) -> void {
        auto isNew = true;
        for (auto& item : d->uiNameList) {
            if(item.compare(name)==0) {
                isNew = false;
            }
        }

        if(!isNew) return;
                
        auto* appInterface = d->appManager->GetAppInterface(name);        
        if(!appInterface) {
            QLOG_ERROR() << "No plugin exists - " << name;
            return;
        }
                
        auto* pluginWidget = appInterface->GetInterfaceWidget();
        if(!pluginWidget) {
            QLOG_ERROR() << "No plugin widget exists - " << name;
            return;
        }

        auto fullName = appInterface->GetDisplayTitle();
        auto shortName = appInterface->GetShortTitle();
        if (false == tabName.isEmpty()) {
            fullName = tabName;
            shortName = tabName;
        }
        QLOG_INFO() << "Add Tab : " << fullName << "[" << shortName << "]";                

        d->tabWidget->addTab(pluginWidget, shortName);        
        d->uiNameList.push_back(name);
        d->tabNameList.push_back(fullName);      

        QLOG_INFO() << QString("Add AppUI %1 to tab widget").arg(name);
    }

    auto TabUiFramework::setTabUnclosable(QString tabName)->void {
        int idx = -1;        
        for(int i=0;i<d->tabNameList.size();i++) {
           if(d->tabNameList[i].compare(tabName)==0) {
               idx = i;
           }
        }
        if (idx>-1) {
            d->tabWidget->tabBar()->tabButton(idx, QTabBar::RightSide)->resize(0, 0);
        }
    }
    
    auto TabUiFramework::findUiFramework(QString name) -> QMainWindow* {
        int idx = -1;
        for (int i = 0; i < d->uiNameList.size();i++) {
            if(d->uiNameList[i].compare(name)==0) {
                idx = i;
                break;
            }
        }
        for(int i=0;i<d->tabNameList.size();i++) {
            if(d->tabNameList[i].compare(name)==0) {
                idx = i;
                break;
            }
        }
        if(idx < 0) {
            return nullptr;
        }else {
            return dynamic_cast<QMainWindow*>(d->tabWidget->widget(idx));
        }
    }
    auto TabUiFramework::findTabIdx(QString name) -> int {
        int idx = -1;
        for (int i = 0; i < d->uiNameList.size(); i++) {
            if (d->uiNameList[i].compare(name) == 0) {
                idx = i;
                break;
            }
        }
        for (int i = 0; i < d->tabNameList.size(); i++) {
            if (d->tabNameList[i].compare(name) == 0) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    auto TabUiFramework::getMainTab() -> QTabWidget* {
        return d->tabWidget;
    }
    void TabUiFramework::OnCloseTab(int idx) {        
        auto pluginName = d->uiNameList[idx];
        d->tabWidget->removeTab(idx);        
        for(int i = idx +1;i<d->uiNameList.size();i++) {
            d->uiNameList[i - 1] = d->uiNameList[i];
        }
        d->uiNameList.pop_back();
        for(int i=idx+1;i<d->tabNameList.size();i++) {
            d->tabNameList[i - 1] = d->tabNameList[i];
        }
        d->tabNameList.pop_back();

        d->appManager->Deactivate(pluginName);        

        //auto curTab = d->tabWidget->widget(idx);
        //delete curTab;

        emit sigTabClose(pluginName);

        auto iidx = d->tabWidget->currentIndex();
        if(iidx == idx) {
            OnTabChanged(iidx);
        }
    }
}
