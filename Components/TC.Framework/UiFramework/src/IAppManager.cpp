#include <QDir>
#include <QApplication>
#include <QPluginLoader>

#include <ctkPluginFrameworkLauncher.h>
#include <ctkPluginFrameworkContext_p.h>
#include <ctkPluginContext.h>
#include <ctkPluginException.h>
#include <service/event/ctkEventAdmin.h>

#include "IAppManager.h"

#define LOGGER_TAG "[IAppManager]"

#include <TCLogger.h>
#include <iostream>

namespace TC::Framework {
    struct IAppManager::Impl {
        QStringList plugin_search_path;
        QStringList appList;
        QStringList activeAppList;
    };
    IAppManager::IAppManager() :d{ new Impl } {
        ctkProperties prop;
        //prop.insert(ctkPluginConstants::FRAMEWORK_STORAGE, qApp->applicationDirPath());
        prop.insert(ctkPluginConstants::FRAMEWORK_STORAGE_CLEAN,
            ctkPluginConstants::FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT);        
        ctkPluginFrameworkLauncher::setFrameworkProperties(prop);        
        auto cache_path = qApp->applicationDirPath() + "/configuration";        
        QDir dir(cache_path);
        if (dir.exists()) {
            dir.removeRecursively();
        }        
        ctkPluginFrameworkLauncher::addSearchPath(qApp->applicationDirPath());//location of event admin plugin, can be changed        
        ctkPluginFrameworkLauncher::startup(nullptr);

        if (false == ctkPluginFrameworkLauncher::start()) {            
            QLOG_ERROR() << "Framework init failed";
        }
        if (false == ctkPluginFrameworkLauncher::start("org.commontk.eventadmin")) {            
            QLOG_ERROR() << "event admin init failed";
        }
    }
    IAppManager::~IAppManager() {
        
    }
    auto IAppManager::GetActiveAppList() -> QStringList {
        return d->activeAppList;
    }

    auto IAppManager::GetAppList() -> QStringList {
        return d->appList;
    }
    
    auto IAppManager::GetPluginPath(const QString& appName) -> QString {        
        auto path = ctkPluginFrameworkLauncher::getPluginPath(appName);
        return path;
    }

    auto IAppManager::AddPluginSearchPath(const QString& path)->void {
        ctkPluginFrameworkLauncher::addSearchPath(path);
        d->plugin_search_path.push_back(path);
    }

    auto IAppManager::GetPluginSearchPaths() -> QStringList {
        return d->plugin_search_path;
    }


    auto IAppManager::Activate(const QString& appName) -> bool {
        auto packageManager = GetPackageManager();
        auto appInterface = GetAppInterface(appName);        
        if(appInterface) {
            if (appInterface->isActivated()) {
                return true;
            }
            auto appProp = appInterface->GetProperties();
            auto appKey = appProp["AppKey"].toString();
            if(appKey.isEmpty()) {
                return false;
            }
            if(false == packageManager->GetActivation(appKey)) {                
                return false;
            }

            auto result = appInterface->Activate();
            if(result) {
                if (false == d->activeAppList.contains(appName)) {
                    d->activeAppList.push_back(appName);
                }
            }
            return result;
        }
        return false;
    }
    auto IAppManager::Deactivate(const QString& appName) -> bool {
        auto appInterface = GetAppInterface(appName);
        if(appInterface) {
            auto result = appInterface->Deactivate();
            if(result) {
                auto index = d->activeAppList.indexOf(appName);
                d->activeAppList.removeAt(index);
            }
            return result;
        }
        return false;
    }
    auto IAppManager::AcquirePackage() -> bool {
        auto packageManager = GetPackageManager();
        return packageManager->AcquirePackage();
    }

    auto IAppManager::GetAppProperty(const QString& appName) -> QVariantMap {
        auto appInterface = GetAppInterface(appName);
        if(appInterface) {
            return appInterface->GetProperties();
        }
        return QVariantMap();
    }

    auto IAppManager::Stop() -> void {        
        ctkPluginFrameworkLauncher::stop("org.commontk.eventadmin");
        ctkPluginFrameworkLauncher::stop();
    }
        
    auto IAppManager::LoadAppPlugin(const QString& appName,const bool preLoad) -> bool {
        if (preLoad) {            
            if (false == d->appList.contains(appName)) {
                d->appList.push_back(appName);
            }
            return true;
        }
        bool succeeded = false;
        try {            
            succeeded = ctkPluginFrameworkLauncher::start(appName);
        }
        catch (ctkPluginException& e)
        {
            QLOG_ERROR() << "Error in " << appName << " " << e.what();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();
            return false;
        }
        catch (ctkRuntimeException& e)
        {
            QLOG_ERROR() << "Error in " << appName << " " << e.what();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();
            return false;
        }
        if(succeeded) {
            if (false == d->appList.contains(appName)) {
                d->appList.push_back(appName);
            }
        }
        return succeeded;
    }

    auto IAppManager::RemoveAppPlugin(const QString& appName) -> void {
        d->appList.removeOne(appName);
    }

    auto IAppManager::UnloadAppPlugin(const QString& appName) -> bool {
        bool succeeded = false;
        try {
            succeeded = ctkPluginFrameworkLauncher::stop(appName);
        }
        catch (ctkPluginException& e)
        {
            QLOG_ERROR() << "Error in " << appName << " " << e.what();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();
            return false;
        }
        catch (ctkRuntimeException& e)
        {
            QLOG_ERROR() << "Error in " << appName << " " << e.what();
            const ctkException* e2 = e.cause();
            if (e2)
                QLOG_ERROR() << e2->message();
            return false;
        }
        return succeeded;
    }
    auto IAppManager::GetAppInterface(const QString& appName) -> IAppInterface* {
        auto pluginReferences = ctkPluginFrameworkLauncher::getPluginContext()->getServiceReferences<IAppInterface>();
        for (auto i = 0; i < pluginReferences.size(); i++) {            
            if (pluginReferences[i].getProperty("name").toString().compare(appName) == 0) {
                return ctkPluginFrameworkLauncher::getPluginContext()->getService<IAppInterface>(pluginReferences[i]);
            }
        }
        return nullptr;
    }

    /////////////////////////Meta Info Registry////////////////////////
    struct IAppMetaRegistry::Impl {
        QMap<QString, IAppMeta*> appMetaDB;
    };

    IAppMetaRegistry::IAppMetaRegistry() : d{ new Impl } {

    }
    IAppMetaRegistry::~IAppMetaRegistry() {

    }
    IAppMetaRegistry::Pointer IAppMetaRegistry::GetInstance() {
        static Pointer theInstance(new IAppMetaRegistry());
        return theInstance;
    }    
    auto IAppMetaRegistry::LoadMeta(const QString& path)->int {
        auto registry = IAppMetaRegistry::GetInstance();
        QPluginLoader loader(path);
        QObject* plugin = loader.instance();
        if (!plugin) {
            QLOG_ERROR() << " > error : " << loader.errorString();
            return -1;
        }
        auto target_module = dynamic_cast<IAppMeta*>(plugin);
        if (!target_module) {
            return -1;
        }

        return 0;
    }
    auto IAppMetaRegistry::GetMeta(const QString& name, bool isPath)->IAppMeta::Pointer {
        if (isPath) {
            QPluginLoader loader(name);

            auto plugin = loader.instance();
            if (!plugin) {
                QLOG_ERROR() << " > error : " << loader.errorString();
                return nullptr;
            }
            auto path_module = dynamic_cast<IAppMeta*>(plugin);
            if (!path_module) {
                return nullptr;
            }
            IAppMeta::Pointer newInstance(path_module->clone());
            return newInstance;
        }

        auto target_module = IAppMetaRegistry::GetInstance()->FindModule(name);
        if (!target_module) {
            return nullptr;
        }
        IAppMeta::Pointer newInstance(target_module->clone());
        return newInstance;
    }
    auto IAppMetaRegistry::Register(IAppMeta* meta)->void {
        const auto name = meta->GetFullName();
        d->appMetaDB[name] = meta;
    }
    auto IAppMetaRegistry::FindModule(const QString& name)->IAppMeta* {
        if (d->appMetaDB.contains(name)) {
            return d->appMetaDB[name];
        }
        return nullptr;
    }
}