#include "IAppInterface.h"

namespace TC::Framework {
    IAppInterface::IAppInterface() : QObject() {
    }    
    IAppInterface::~IAppInterface() {
    }
    auto IAppInterface::Activate() -> bool {
        auto window = GetInterfaceWidget();
        return window->TryActivate();
    }
    auto IAppInterface::Deactivate() -> bool {
        auto window = GetInterfaceWidget();
        return window->TryDeactivate();
    }
    auto IAppInterface::isActivated() -> bool {
        auto window = GetInterfaceWidget();
        return window->IsActivate();
    }
    auto IAppInterface::GetProperties() -> QVariantMap {
        auto window = GetInterfaceWidget();
        return window->GetMetaInfo();
    }
}