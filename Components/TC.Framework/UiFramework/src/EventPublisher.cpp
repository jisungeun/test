#include <iostream>

#include "EventPublisher.h"

#include "service/event/ctkEventAdmin.h"

namespace TC::Framework {
    struct EventPublisher::Impl {
		ctkPluginContext* context;
		std::vector<QString> topic_list;
		std::vector<std::vector<QString>> prop_list;	
    };
    EventPublisher::EventPublisher() : d(new Impl()) {
        
    }
    EventPublisher::~EventPublisher() {
        
    }
	auto EventPublisher::setPluginContext(ctkPluginContext* context)->void {
		d->context = context;
	}	

    auto EventPublisher::publishSignal(TCEvent& evt, const QString& senderName) -> void {
        ctkServiceReference reference = d->context->getServiceReference<ctkEventAdmin>();
        if(reference) {
			auto eventAdmin = d->context->getService<ctkEventAdmin>(reference);
            evt.AddProperty("SenderName", senderName);
            eventAdmin->sendEvent(evt.GetEvent());
        }
    }
}
