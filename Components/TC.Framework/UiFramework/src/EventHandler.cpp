#include <iostream>

#include "EventHandler.h"

#include "service/event/ctkEventAdmin.h"
#include "service/event/ctkEventConstants.h"

namespace TC::Framework {
	struct EventHandler::Impl {
		ctkPluginContext* context;
		std::vector<QString> topic_list;
		std::vector<std::vector<QString>> prop_list;
		std::vector<qlonglong> slotIds;
	};
	EventHandler::EventHandler() : d(new Impl()) {

	}
	EventHandler::~EventHandler() {
		/*for(int i=0;i<d->slotIds.size();i++) {
			this->closeSubscription(d->slotIds[i]);
		}*/
	}
	auto EventHandler::setPluginContext(ctkPluginContext* context)->void {
		d->context = context;
	}
	auto EventHandler::subscribeEvent(QString eventName) -> void {
		ctkServiceReference reference = d->context->getServiceReference<ctkEventAdmin>();
        if(reference) {
			auto eventAdmin = d->context->getService<ctkEventAdmin>(reference);
			ctkDictionary properties;
			properties[ctkEventConstants::EVENT_TOPIC] = eventName;
			auto iid = eventAdmin->subscribeSlot(this, SLOT(OnEventOccured(ctkEvent)), properties);
			d->slotIds.push_back(iid);
        }
    }
    auto EventHandler::closeSubscription(qlonglong iid) -> void {
		ctkServiceReference reference = d->context->getServiceReference<ctkEventAdmin>();
		if (reference) {
			auto eventAdmin = d->context->getService<ctkEventAdmin>(reference);
			eventAdmin->unsubscribeSlot(iid);			
		}
    }
    void EventHandler::OnEventOccured(const ctkEvent& ctkEvent) {
		emit sigCtkEvent(ctkEvent);
    }


}