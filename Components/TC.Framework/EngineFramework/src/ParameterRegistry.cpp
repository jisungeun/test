#include <iostream>

#include "ParameterRegistry.h"

auto ParameterRegistry::Register(const std::string& name, TCreateMethod funcCreate)->bool
{
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if (it == sMethods.end())
	{
		sMethods[name] = funcCreate;
		return true;
	}
	return false;
}

auto ParameterRegistry::Create(const std::string& name)->IParameter::Pointer
{
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if (it != sMethods.end()) {
		return it->second();
	}

	return nullptr;
}

auto ParameterRegistry::GetMap(void)->std::map<std::string, ParameterRegistry::TCreateMethod>& {
	static std::map<std::string, ParameterRegistry::TCreateMethod> sMethods;
	return sMethods;
}

auto UiParameterRegistry::Register(const std::string& name, TCreateMethod funcCreate) -> bool {
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if(it == sMethods.end()) {
		sMethods[name] = funcCreate;
		return true;
	}
	return false;
}

auto UiParameterRegistry::Create(const std::string& name) -> IUiParameter::Pointer {
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if(it != sMethods.end()) {
		return it->second();
	}
	return nullptr;
}

auto UiParameterRegistry::GetMap() -> std::map<std::string, UiParameterRegistry::TCreateMethod>& {
	static std::map<std::string, UiParameterRegistry::TCreateMethod> sMethods;
	return sMethods;
}

auto MetaParameterRegistry::Register(const std::string& name, TCreateMethod funcCreate) -> bool {
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if(it == sMethods.end()) {
		sMethods[name] = funcCreate;
		return true;
	}
	return false;
}

auto MetaParameterRegistry::Create(const std::string& name) -> IMetaParameter::Pointer {
	auto& sMethods = GetMap();
	auto it = sMethods.find(name);
	if(it != sMethods.end()) {
		return it->second();
	}
	return nullptr;
}

auto MetaParameterRegistry::GetMap() -> std::map<std::string, MetaParameterRegistry::TCreateMethod>& {
	static std::map<std::string, MetaParameterRegistry::TCreateMethod> sMethods;
	return sMethods;
}
