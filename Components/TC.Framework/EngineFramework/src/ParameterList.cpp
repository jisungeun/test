#pragma once

#include <QMap>

#include "ParameterList.h"

namespace local {
    int id = qRegisterMetaType<ParameterNode>();
}

struct ParameterList::Impl {
    Map nodes;
};

auto ParameterNode::IsScalarType() -> bool {
    return (value.type() == QJsonValue::Double);
}

auto ParameterNode::IsStringType() -> bool {
    return (value.type() == QJsonValue::String);
}

ParameterList::ParameterList() : d{ new Impl } {
}

ParameterList::ParameterList(const ParameterList& list) {
    d->nodes = list.d->nodes;
}

ParameterList::~ParameterList() {
}

auto ParameterList::Clear() -> void {
    d->nodes.clear();
}

auto ParameterList::AppendNode(const ParameterNode& node)->void {
    d->nodes[node.name] = node;
}

auto ParameterList::GetNames() const->QStringList {
    return d->nodes.keys();
}

auto ParameterList::GetNode(const QString& name) const->ParameterNode {
    if (d->nodes.contains(name)) return d->nodes[name];
    return ParameterNode();
}

auto ParameterList::GetMap(void) const->const Map* {
    return &(d->nodes);
}

ParameterList& ParameterList::operator=(const ParameterList& other) {
    d->nodes.clear();
    d->nodes = other.d->nodes;
    return *this;
}
