#include <iostream>
#include <QJsonArray>

#include "IMetaParameter.h"

//Duplicator
IMetaParameter::Duplicator::Duplicator() = default;
IMetaParameter::Duplicator::Duplicator(const Duplicator& dup) = default;
IMetaParameter::Duplicator::~Duplicator() = default;
IMetaParameter::Duplicator::Duplicator(QString key, QStringList names, QString target) :key{ std::move(key) }, mask_names{ std::move(names) }, targetAlgoName{ std::move(target) } {
}
auto IMetaParameter::Duplicator::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for(auto i=0;i<mask_names.count();i++) {
        auto jsonName = QString("mask%1").arg(i + 1);
        json[jsonName] = mask_names[i];
    }
    json["target"] = targetAlgoName;
}
auto IMetaParameter::Duplicator::Read(const QJsonObject& json) -> void {
    mask_names.clear();
    key = json["key"].toString();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("mask%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        mask_names.append(json[jsonName].toString());
        idx++;
    }
    targetAlgoName = json["target"].toString();
}

//Highlighter
IMetaParameter::Highlighter::Highlighter() = default;
IMetaParameter::Highlighter::Highlighter(const Highlighter& high) = default;
IMetaParameter::Highlighter::~Highlighter() = default;

IMetaParameter::Highlighter::Highlighter(QString key, QString sender, QString receiver) :key{ std::move(key) }, sender{ std::move(sender) }, receiver{ std::move(receiver) } {
}

auto IMetaParameter::Highlighter::Write(QJsonObject& json) -> void {
    json["key"] = key;    
    json["sender"] = sender;
    json["receiver"] = receiver;
}

auto IMetaParameter::Highlighter::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();    
    sender = json["sender"].toString();
    receiver = json["receiver"].toString();
}

//Connector
IMetaParameter::Connector::Connector() = default;
IMetaParameter::Connector::Connector(const Connector& connect) = default;
IMetaParameter::Connector::~Connector() = default;

IMetaParameter::Connector::Connector(QString key, QStringList value_names, QString sender_name, QString receiver_name) : key{ std::move(key) }, value_names { std::move(value_names) }, sender_name{ std::move(sender_name) }, receiver_name{ std::move(receiver_name) } {
    
}

auto IMetaParameter::Connector::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for (auto i = 0; i < value_names.count();i++) {
        auto jsonName = QString("value_name%1").arg(i + 1);
        json[jsonName] = value_names[i];
    }    
    json["sender_nam"] = sender_name;
    json["receiver_name"] = receiver_name;
}

auto IMetaParameter::Connector::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    value_names.clear();
    auto idx = 1;
    while (1) {
        auto jsonName = QString("value_name%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        value_names.push_back(json[jsonName].toString());
        idx++;
    }    
    sender_name = json["sender_name"].toString();
    receiver_name = json["receiver_name"].toString();
}

IMetaParameter::Fixer::Fixer() = default;
IMetaParameter::Fixer::Fixer(const Fixer& fix) = default;
IMetaParameter::Fixer::~Fixer() = default;
IMetaParameter::Fixer::Fixer(QString key, QString dupName, QString dupAlgoName, QString valueName, QJsonValue value) :key{ std::move(key) }, dupName{ std::move(dupName) }, dupAlgoName{ std::move(dupAlgoName) }, valueName{ std::move(valueName) }, value{ std::move(value) } {
    
}

auto IMetaParameter::Fixer::Write(QJsonObject& json) -> void {
    json["key"] = key;
    json["duplicator_name"] = dupName;
    json["algorithm_name"] = dupAlgoName;
    json["value_name"] = valueName;
    json["value"] = value;
}

auto IMetaParameter::Fixer::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    dupName = json["duplicator_name"].toString();
    dupAlgoName = json["algorithm_name"].toString();
    valueName = json["value_name"].toString();
    value = json["value"];
}

IMetaParameter::Sorter::Sorter() = default;
IMetaParameter::Sorter::Sorter(const Sorter& sort) = default;
IMetaParameter::Sorter::~Sorter() = default;
IMetaParameter::Sorter::Sorter(QString key, QStringList order) : key{ std::move(key) }, order{ std::move(order) } {
    
}

auto IMetaParameter::Sorter::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for(auto i=0;i<order.count();i++) {
        auto orderName = QString("sort%1").arg(i + 1);
        json[orderName] = order[i];
    }
}

auto IMetaParameter::Sorter::Read(const QJsonObject& json) -> void {
    order.clear();
    key = json["key"].toString();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("sort%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        order.push_back(json[jsonName].toString());
    }
}

IMetaParameter::DefaultValue::DefaultValue() = default;
IMetaParameter::DefaultValue::DefaultValue(const DefaultValue& def) = default;
IMetaParameter::DefaultValue::~DefaultValue() = default;
IMetaParameter::DefaultValue::DefaultValue(QString key, QString dupName, QString dupAlgoName, QString valueName, QJsonValue value) : key{ std::move(key) }, dupName{ std::move(dupName) }, dupAlgoName{ std::move(dupAlgoName) }, valueName{ std::move(valueName) }, value{ std::move(value) } {
    
}

auto IMetaParameter::DefaultValue::Write(QJsonObject& json) -> void {
    json["key"] = key;
    json["duplicator_name"] = dupName;
    json["algorithm_name"] = dupAlgoName;
    json["value_name"] = valueName;
    json["value"] = value;
}

auto IMetaParameter::DefaultValue::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    dupName = json["duplicator_name"].toString();
    dupAlgoName = json["algorithm_name"].toString();
    valueName = json["value_name"].toString();
    value = json["value"];
}

IMetaParameter::Hider::Hider() = default;
IMetaParameter::Hider::Hider(const Hider& hide) = default;
IMetaParameter::Hider::~Hider() = default;
IMetaParameter::Hider::Hider(QString key, QStringList algoNames) : key{ std::move(key) }, algoNames{ std::move(algoNames) } {
    
}

auto IMetaParameter::Hider::Write(QJsonObject& json) -> void {
    json["key"] = key;
    
    for (auto i = 0; i < algoNames.count(); i++) {
        auto jsonName = QString("algh_name%1").arg(i + 1);
        json[jsonName] = algoNames[i];
    } 
}

auto IMetaParameter::Hider::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    algoNames.clear();
    auto idx = 1;
    while (1) {
        auto jsonName = QString("algo_name%1").arg(idx);
        if (false == json.contains(jsonName)) {
            break;
        }
        algoNames.push_back(json[jsonName].toString());
        idx++;
    }
}

IMetaParameter::Selector::Selector() = default;
IMetaParameter::Selector::Selector(const Selector& select) = default;
IMetaParameter::Selector::~Selector() = default;
IMetaParameter::Selector::Selector(QString key, QString target_node, int type,int max_item, QStringList algoNames, QStringList dispNames) : key{ std::move(key) }, target_node{ std::move(target_node) }, type{ std::move(type) }, max_items_in_row{ std::move(max_item) }, algoNames{ std::move(algoNames) }, dispNames{ std::move(dispNames) } {
    
}

auto IMetaParameter::Selector::Write(QJsonObject& json) -> void {
    json["key"] = key;
    json["target_node"] = target_node;
    json["type"] = type;
    json["max_item"] = max_items_in_row;
    for(auto i=0;i<algoNames.count();i++) {
        auto jsonName = QString("algo_name%1").arg(i + 1);
        json[jsonName] = algoNames[i];
    }
    for(auto i=0;i<dispNames.count();i++) {
        auto jsonName = QString("disp_name%1").arg(i + 1);
        json[jsonName] = dispNames[i];
    }
}

auto IMetaParameter::Selector::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    target_node = json["target_node"].toString();
    type = json["type"].toInt();
    max_items_in_row = json["max_item"].toInt();
    algoNames.clear();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("algo_name%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        algoNames.push_back(json[jsonName].toString());
        idx++;
    }
    idx = 1;
    while(1) {
        auto jsonName = QString("disp_name%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        dispNames.push_back(json[jsonName].toString());
        idx++;
    }
}




struct IMetaParameter::Impl {    
    QMap<QString, Duplicator> dups;
    QMap<QString, Highlighter> highs;
    QMap<QString, Connector> connects;
    QMap<QString, Sorter> sorts;
    QMap<QString, DefaultValue> values;
    QMap<QString, Fixer> fixes;
    QMap<QString, Hider> hides;
    QMap<QString, Selector> selects;

    QString fullName;
};

IMetaParameter::IMetaParameter(QObject* parent) : QObject(parent), d{ new Impl } {
    
}

IMetaParameter::~IMetaParameter() {
    
}

auto IMetaParameter::SetFullName(const QString& fullname) -> void {
    d->fullName = fullname;
}

auto IMetaParameter::GetFullName() const -> QString {
    return d->fullName;
}

auto IMetaParameter::Clone() -> IMetaParameter* {
    auto param = new IMetaParameter();
    //copy pImpl variables
    param->d->dups = d->dups;
    param->d->highs = d->highs;
    param->d->connects = d->connects;
    param->d->sorts = d->sorts;
    param->d->values = d->values;
    param->d->fixes = d->fixes;
    param->d->hides = d->hides;
    param->d->selects = d->selects;
    return param;
}

auto IMetaParameter::RegisterDuplicator(const QString& key, const QStringList& node_names,const QString& target) -> void {
    d->dups[key] = Duplicator(key,node_names,target);
}

auto IMetaParameter::RegisterHighlighter(const QString& key, const QString& sender, const QString& receiver) -> void {
    d->highs[key] = Highlighter(key, sender, receiver);
}

auto IMetaParameter::RegisterConnector(const QString& key, const QStringList& value_names, const QString& sender_name, const QString& receiver_name) -> void {
    d->connects[key] = Connector(key,value_names, sender_name, receiver_name);
}

auto IMetaParameter::RegisterSorter(const QString& key, const QStringList& order) -> void {
    d->sorts[key] = Sorter(key, order);
}

auto IMetaParameter::RegisterDefaultValue(const QString& key, const QString& dupName, const QString& dupAlgoName, const QString& valueName, QJsonValue value) -> void {
    d->values[key] = DefaultValue(key, dupName, dupAlgoName, valueName, value);
}

auto IMetaParameter::RegisterFixer(const QString& key, const QString& dupName, const QString& dupAlgoName, const QString& valueName, QJsonValue value) -> void {
    d->fixes[key] = Fixer(key, dupName, dupAlgoName, valueName, value);
}

auto IMetaParameter::RegisterHider(const QString& key, const QStringList& algoNames) -> void {
    d->hides[key] = Hider(key, algoNames);
}

auto IMetaParameter::RegisterSelector(const QString& key,const QString& target_node, const int& type,const int&max_item, const QStringList& algoNames,const QStringList& dispNames) -> void {
    d->selects[key] = Selector(key,target_node, type,max_item, algoNames,dispNames);
}

auto IMetaParameter::Selects() -> QMap<QString, Selector>& {
    return d->selects;
}

auto IMetaParameter::Hides() -> QMap<QString, Hider>& {
    return d->hides;
}

auto IMetaParameter::Fixes() -> QMap<QString, Fixer>& {
    return d->fixes;
}

auto IMetaParameter::GetFixerNames() -> QStringList {
    return d->fixes.keys();
}

auto IMetaParameter::GetHiderNames() -> QStringList {
    return d->hides.keys();
}

auto IMetaParameter::GetHider(const QString& key) -> QStringList {
    if(false == d->hides.contains(key)) {
        return QStringList();
    }
    auto hide = d->hides[key];
    return hide.algoNames;
}

auto IMetaParameter::GetSelector(const QString& key) -> std::tuple<QString,int,int, QStringList,QStringList> {
    if(false == d->selects.contains(key)) {
        return std::make_tuple(QString(),-1, 0, QStringList(),QStringList());
    }
    auto select = d->selects[key];
    return std::make_tuple(select.target_node,select.type,select.max_items_in_row, select.algoNames,select.dispNames);
}

auto IMetaParameter::GetSelectorNames() -> QStringList {
    return d->selects.keys();
}

auto IMetaParameter::GetFixer(const QString& key) -> std::tuple<QString, QString, QString, QJsonValue> {
    if(false == d->fixes.contains(key)) {
        return std::make_tuple(QString(), QString(), QString(), QJsonValue());
    }
    auto fix = d->fixes[key];
    return std::make_tuple(fix.dupName, fix.dupAlgoName, fix.valueName, fix.value);
}


auto IMetaParameter::Values() -> QMap<QString, DefaultValue>& {
    return d->values;
}

auto IMetaParameter::GetDefaultValueNames() -> QStringList {
    return d->values.keys();
}

auto IMetaParameter::GetDefaultValue(const QString& key) -> std::tuple<QString, QString, QString, QJsonValue> {
    if(false == d->values.contains(key)) {
        return std::make_tuple(QString(), QString(), QString(), QJsonValue());
    }
    auto defaultValue = d->values[key];
    return std::make_tuple(defaultValue.dupName, defaultValue.dupAlgoName, defaultValue.valueName, defaultValue.value);
}

auto IMetaParameter::Dups() -> QMap<QString, Duplicator>& {
    return d->dups;
}

auto IMetaParameter::GetDuplicatorNames() -> QStringList {
    return d->dups.keys();
}

auto IMetaParameter::GetDuplicator(const QString& key) -> std::tuple<QStringList, QString> {
    if(false == d->dups.contains(key)) {
        return std::make_tuple(QStringList(), QString());
    }
    auto duplicator = d->dups[key];
    return std::make_tuple(duplicator.mask_names, duplicator.targetAlgoName);
}

auto IMetaParameter::Highs() -> QMap<QString, Highlighter>& {
    return d->highs;
}

auto IMetaParameter::GetHighlighterNames() -> QStringList {
    return d->highs.keys();
}

auto IMetaParameter::GetHighlighter(const QString& key) -> std::tuple<QString, QString> {
    if(false == d->highs.contains(key)) {
        return std::make_tuple( QString(), QString());
    }
    auto highlighter = d->highs[key];
    return std::make_tuple(highlighter.sender, highlighter.receiver);
}

auto IMetaParameter::Connects() -> QMap<QString, Connector>& {
    return d->connects;
}

auto IMetaParameter::GetConnecterNames() -> QStringList {
    return d->connects.keys();
}

auto IMetaParameter::GetConnector(const QString& key) -> std::tuple<QString, QString, QStringList> {
    if(false == d->connects.contains(key)) {
        return std::make_tuple(QString(), QString(), QStringList());
    }
    auto connector = d->connects[key];
    return std::make_tuple(connector.sender_name, connector.receiver_name, connector.value_names);
}

auto IMetaParameter::Sorts() -> QMap<QString, Sorter>& {
    return d->sorts;
}

auto IMetaParameter::GetSorterNames() -> QStringList {
    return d->sorts.keys();
}

auto IMetaParameter::GetSorter(const QString& key) -> QStringList {
    if(false == d->sorts.contains(key)) {
        return QStringList();
    }
    auto sorter = d->sorts[key];
    return sorter.order;
}

auto IMetaParameter::Write(QJsonObject& json)->void {
    QJsonArray dupArray;
    for(auto dup : d->dups) {
        QJsonObject dupObject;
        dup.Write(dupObject);
        dupArray.append(dupObject);
    }
    json["duplicator"] = dupArray;

    QJsonArray highArray;
    for(auto high : d->highs) {
        QJsonObject highObject;
        high.Write(highObject);
        highArray.append(highObject);
    }
    json["highlighter"] = highArray;

    QJsonArray conArray;
    for(auto con : d->connects) {
        QJsonObject conObject;
        con.Write(conObject);
        conArray.append(conObject);
    }
    json["connector"] = conArray;

    QJsonArray sortArray;
    for(auto so : d->sorts) {
        QJsonObject sortObject;
        so.Write(sortObject);
        sortArray.append(sortObject);
    }
    json["sorter"] = sortArray;

    QJsonArray valueArray;
    for(auto val : d->values) {
        QJsonObject valueObject;
        val.Write(valueObject);
        valueArray.append(valueObject);
    }
    json["defaulter"] = valueArray;
}

auto IMetaParameter::Read(const QJsonObject& json)->void {
    d->dups.clear();
    d->highs.clear();
    d->connects.clear();
    d->sorts.clear();
    d->values.clear();
    auto dupArray = json["duplicator"].toArray();
    for(auto item : dupArray) {
        Duplicator dup;
        dup.Read(item.toObject());
        d->dups[dup.key] = dup;
    }

    auto highArray = json["highlighter"].toArray();
    for(auto item : highArray) {
        Highlighter high;
        high.Read(item.toObject());
        d->highs[high.key] = high;
    }

    auto conArray = json["connector"].toArray();
    for(auto item: conArray) {
        Connector con;
        con.Read(item.toObject());
        d->connects[con.key] = con;
    }

    auto sortArray = json["sorter"].toArray();
    for(auto item : sortArray) {
        Sorter so;
        so.Read(item.toObject());
        d->sorts[so.key] = so;
    }

    auto valueArray = json["defaulter"].toArray();
    for (auto item : valueArray) {
        DefaultValue val;
        val.Read(item.toObject());
        d->values[val.key] = val;
    }
}