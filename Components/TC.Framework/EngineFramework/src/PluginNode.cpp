#include <QList>

#include "IPluginModule.h"
#include "PluginNode.h"

struct PluginNode::Impl {
    QString name;
    IPluginModule* target_module{ nullptr };
    PluginNode* parent{ nullptr };    
    QList<PluginNode*> childItems;
};

PluginNode::PluginNode(const QString& name, IPluginModule* target_module, PluginNode* parent) : d{ new Impl } {
    d->name = name;
    d->target_module = target_module;
    d->parent = parent;
}

PluginNode::~PluginNode() = default;

auto PluginNode::AppendChild(PluginNode* child) -> void {
    d->childItems.append(child);
}

auto PluginNode::Child(const int row) -> PluginNode* {
    if (ChildCount() <= row) return nullptr;
    return d->childItems.value(row);
}

auto PluginNode::Child(const QString& name) -> PluginNode* {
    for(auto child : d->childItems) {
        if (child->Name() == name) return child;
    }
    return nullptr;
}

auto PluginNode::ChildCount() const -> int {
    return d->childItems.count();
}

auto PluginNode::Name() const -> QString {
    return d->name;
}

auto PluginNode::Data() const -> IPluginModule* {
    return d->target_module;
}

auto PluginNode::Row() const -> int {
    auto& parent = d->parent;
    if (!parent) return 0;
    auto& siblings = parent->d->childItems;
    return siblings.indexOf(const_cast<PluginNode*>(this));
}

auto PluginNode::ParentItem() -> PluginNode* {
    return d->parent;
}

