#include <QList>
#include <QMap>
#include <QMutex>
#include "DataSet.h"

struct DataSet::Impl {
    QList<IBaseData::Pointer> data;
    QMap<int, QString> names;
    QMutex mutex;
};

DataSet::DataSet() : d( new Impl() ) {
}

DataSet::~DataSet() {
}

auto DataSet::New() -> Pointer {
    return std::make_shared<DataSet>();
}

auto DataSet::New(IBaseData::Pointer data, const QString& refName) -> Pointer {
    auto instance = std::make_shared<DataSet>();
    instance->AppendData(data, refName);
    return instance;
}

auto DataSet::AppendData(IBaseData::Pointer data, const QString& refName) -> void {
    QMutexLocker locker(&d->mutex);

    const auto prevCount = d->data.length();
    d->data.append(data);

    const auto curIndex = prevCount;
    d->names[curIndex] = refName;
}

auto DataSet::SetData(int index, IBaseData::Pointer data, const QString& refName) -> void {
    QMutexLocker locker(&d->mutex);

    d->data[index] = data;
    d->names[index] = refName;
}

auto DataSet::SetData(IBaseData::Pointer data, const QString& refName) -> void {
    SetData(0, data, refName);
}

auto DataSet::GetData() -> IBaseData::Pointer {
    return GetData(0);
}

auto DataSet::GetData(int index) -> IBaseData::Pointer {
    QMutexLocker locker(&d->mutex);
    if (index >= d->data.length()) return nullptr;
    return d->data.at(index);
}

auto DataSet::GetData(const QString& name) -> IBaseData::Pointer {
    QMutexLocker locker(&d->mutex);
    if (name.isEmpty()) return nullptr;

    const auto index = [=]()->int {
        for (auto key : d->names.keys()) {
            if (d->names.value(key) == name) {
                return key;
            }
        }
        return -1;
    }();
    if (index < 0) return nullptr;

    return d->data.at(index);
}

auto DataSet::Count() -> int {
    return d->data.length();
}

auto DataSet::Clear() -> void {
    QMutexLocker locker(&d->mutex);
    d->data.clear();
    d->names.clear();
}

