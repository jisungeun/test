#include <iostream>
#include <QJsonArray>

#include "IUiParameter.h"

//Enabler
IUiParameter::Enabler::Enabler() = default;
IUiParameter::Enabler::Enabler(const Enabler& enable) = default;
IUiParameter::Enabler::~Enabler() = default;
IUiParameter::Enabler::Enabler(QString key, QString dispName, QStringList showNodes, QStringList hideNodes) : key{ std::move(key) }, displayName { std::move(dispName) }, showNodes{ std::move(showNodes) }, hideNodes{ std::move(hideNodes) }  {
    
}
auto IUiParameter::Enabler::Write(QJsonObject& json) -> void {
    json["key"] = key;
    json["displayName"] = displayName;    
    for(auto i=0;i<showNodes.count();i++) {
        auto showName = QString("enable%1").arg(i + 1);
        json[showName] = showNodes[i];
    }
    for(auto i=0;i<hideNodes.count();i++) {
        auto hideName = QString("disable%1").arg(i + 1);
        json[hideName] = hideNodes[i];
    }
}

auto IUiParameter::Enabler::Read(const QJsonObject& json) -> void {
    showNodes.clear();
    hideNodes.clear();
    key = json["key"].toString();
    displayName = json["displayName"].toString();    
    auto idx = 1;
    while(1) {
        auto showName = QString("enable%1").arg(idx);
        if(false == json.contains(showName)) {
            break;
        }
        showNodes.push_back(json[showName].toString());
        idx++;
    }
    idx = 1;
    while(1) {
        auto hideName = QString("disable%1").arg(idx);
        if(false == json.contains(hideName)) {
            break;
        }
        hideNodes.push_back(json[hideName].toString());
        idx++;
    }
}

//Sorter
IUiParameter::Sorter::Sorter() = default;
IUiParameter::Sorter::Sorter(const Sorter& sorter) = default;
IUiParameter::Sorter::~Sorter() = default;
IUiParameter::Sorter::Sorter(QString key, QStringList ordered_name) :key{ std::move(key) }, ordered_name{ std::move(ordered_name) } {
    
}

auto IUiParameter::Sorter::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for(auto i=0;i<ordered_name.count();i++) {
        auto jsonName = QString("sort%1").arg(i + 1);
        json[jsonName] = ordered_name[i];
    }
}

auto IUiParameter::Sorter::Read(const QJsonObject& json) -> void {
    ordered_name.clear();
    key = json["key"].toString();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("sort%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        ordered_name.push_back(json[jsonName].toString());
    }
}

//TableItems
IUiParameter::TableItem::TableItem() = default;
IUiParameter::TableItem::TableItem(const TableItem& tableitem) = default;
IUiParameter::TableItem::~TableItem() = default;
IUiParameter::TableItem::TableItem(QString key, QStringList items) : key{ std::move(key) }, items{ std::move(items) } {
    
}

auto IUiParameter::TableItem::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for(auto i=0;i<items.count();i++) {
        auto jsonName = QString("tableitem%1").arg(i + 1);
        json[jsonName] = items[i];
    }
}

auto IUiParameter::TableItem::Read(const QJsonObject& json) -> void {
    items.clear();
    key = json["key"].toString();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("tableitem%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        items.push_back(json[jsonName].toString());
    }
}



//Hider
IUiParameter::Hider::Hider() = default;
IUiParameter::Hider::Hider(const Hider& hider) = default;
IUiParameter::Hider::~Hider() = default;
IUiParameter::Hider::Hider(QString key, QStringList node_names) :key{ std::move(key) }, node_names { std::move(node_names) } {
    
}

auto IUiParameter::Hider::Write(QJsonObject& json) -> void {
    json["key"] = key;
    for(auto i=0;i<node_names.count();i++) {
        auto jsonName = QString("hide%1").arg(i + 1);
        json[jsonName] = node_names[i];
    }
}

auto IUiParameter::Hider::Read(const QJsonObject& json) -> void {
    node_names.clear();
    key = json["key"].toString();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("hide%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        node_names.push_back(json[jsonName].toString());
    }
}

IUiParameter::Bounder::Bounder() = default;
IUiParameter::Bounder::Bounder(const Bounder& bounder) = default;
IUiParameter::Bounder::~Bounder() = default;
IUiParameter::Bounder::Bounder(QString key, QString bounder, QString boundee, QString type) :key{ std::move(key) }, bounder { std::move(bounder) }, boundee{ std::move(boundee) }, type{ std::move(type) } {
    
}

auto IUiParameter::Bounder::Write(QJsonObject& json) -> void {
    json["key"] = key;
    json["bounder"] = bounder;
    json["boundee"] = boundee;
    json["type"] = type;
}

auto IUiParameter::Bounder::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();
    bounder = json["bounder"].toString();
    boundee = json["boundee"].toString();
    type = json["type"].toString();
}

IUiParameter::Setter::Setter() = default;
IUiParameter::Setter::Setter(const Setter& setter) = default;
IUiParameter::Setter::~Setter() = default;
IUiParameter::Setter::Setter(QString key, QString displayName, QStringList node_names, QStringList headers) : key{ std::move(key) }, displayName{ std::move(displayName) }, node_names{ std::move(node_names) }, headers{ std::move(headers) } {
    
}

auto IUiParameter::Setter::Append(QString node_name, QString header, ValueType val) -> void {
    if (false == valueList.contains(header)) {
        valueList[header] = QMap<QString,ValueType>();
    }
    valueList[header][node_name] = val;
}

auto IUiParameter::Setter::Write(QJsonObject& json) -> void {
    json["key"] = key;    
    for(auto i =0;i<node_names.count();i++) {        
        auto jsonNode = QString("node%1").arg(i + 1);
        json[jsonNode] = node_names[i];
    }
    for(auto i=0;i<headers.count();i++) {
        auto header = headers[i];
        auto jsonHeader = QString("header%1").arg(i + 1);
        json[jsonHeader] = header;
        auto vals = valueList[header];
        for (auto j = 0; j < node_names.count(); j++) {
            auto valHeader = QString("val_%1_%2").arg(i+1).arg(j+1);            
            json[valHeader] = vals[node_names[j]];
        }
    }
}

auto IUiParameter::Setter::Read(const QJsonObject& json) -> void {
    key = json["key"].toString();    
    node_names.clear();
    headers.clear();
    valueList.clear();
    auto idx = 1;
    while(1) {
        auto jsonName = QString("nonde%1").arg(idx);
        if(false == json.contains(jsonName)) {
            break;
        }
        node_names.push_back(json[jsonName].toString());
    }
    idx = 1;
    while(1) {
        auto jsonHeader = QString("header%1").arg(idx);
        if(false == json.contains(jsonHeader)) {
            break;
        }
        headers.push_back(json[jsonHeader].toString());
        auto hidx = 1;
        QMap<QString,ValueType> vals = QMap<QString,ValueType>();
        while(1) {            
            auto valHeader = QString("val_%1_%2").arg(idx ).arg(hidx );
            if(false== json.contains(valHeader)) {
                break;
            }
            auto val = json[valHeader];
            vals[node_names[hidx - 1]] = val;
            hidx++;
        }
        valueList[headers[idx-1]] = vals;
        idx++;
    }
}

struct IUiParameter::Impl {
    QString type{ "Default" };
    QMap<QString, Enabler> enablers;
    QMap<QString, Sorter> sorters;
    QMap<QString, Hider> hiders;
    QMap<QString, TableItem> tableitems;
    QMap<QString, Bounder> bounders;
    QMap<QString, Setter> setters;

    QString fullName;
};

IUiParameter::IUiParameter(QObject* parameter) : d{ new Impl } {    
}

IUiParameter::~IUiParameter() {
    
}

auto IUiParameter::SetFullName(const QString& fullname) -> void {
    d->fullName = fullname;
}

auto IUiParameter::GetFullName() const -> QString {
    return d->fullName;
}

auto IUiParameter::SetType(const QString& type) -> void {
    d->type = type;
}

auto IUiParameter::GetType() const -> QString {
    return d->type;
}


auto IUiParameter::Clone() -> IUiParameter* {
    auto param = new IUiParameter();
    //copy pImpl variables;
    param->d->type = d->type;
    param->d->enablers = d->enablers;
    param->d->hiders = d->hiders;
    param->d->bounders = d->bounders;
    param->d->setters = d->setters;
    return param;
}

auto IUiParameter::RegisterEnabler(const QString& key, const QString& dispName, const QStringList& showNodes, const QStringList& hideNodes) -> void {
    d->enablers[key] = Enabler(key, dispName, showNodes, hideNodes);
}

auto IUiParameter::RegisterSorter(const QString& key, const QStringList& ordered_name) -> void {
    d->sorters[key] = Sorter(key, ordered_name);
}

auto IUiParameter::RegisterTableItem(const QString& key, const QStringList& items) -> void {
    d->tableitems[key] = TableItem(key, items);
}

auto IUiParameter::RegisterHider(const QString& key, const QStringList& node_names) -> void {
    d->hiders[key] = Hider(key, node_names);
}

auto IUiParameter::RegisterBounder(const QString& key, const QString& bounder, const QString& boundee, const QString& type) -> void {
    d->bounders[key] = Bounder(key, bounder, boundee, type);
}

auto IUiParameter::RegisterSetter(const QString& key,const QString& displayName, const QStringList& node_names, const QStringList& headers) -> void {
    d->setters[key] = Setter(key, displayName,node_names, headers);
}

auto IUiParameter::AppendSetter(const QString& key, const QString& node_name, const QString& header, const ValueType& val) -> void {
    d->setters[key].Append(node_name, header, val);
}

auto IUiParameter::Enables() -> QMap<QString, Enabler>& {
    return d->enablers;
}

auto IUiParameter::Hides() -> QMap<QString, Hider>& {
    return d->hiders;
}

auto IUiParameter::Bounds() -> QMap<QString, Bounder>& {
    return d->bounders;
}

auto IUiParameter::Sets() -> QMap<QString, Setter>& {
    return d->setters;
}

auto IUiParameter::TableItems() -> QMap<QString, TableItem>& {
    return d->tableitems;
}


auto IUiParameter::Write(QJsonObject& json) -> void {
    QJsonArray enArray;
    for(auto enabler : d->enablers) {
        QJsonObject enObject;
        enabler.Write(enObject);
        enArray.append(enObject);
    }
    json["enabler"] = enArray;

    QJsonArray hideArray;
    for(auto hider : d->hiders) {
        QJsonObject hideObject;
        hider.Write(hideObject);
        hideArray.append(hideObject);
    }
    json["hider"] = hideArray;

    QJsonArray tableitemArray;
    for(auto tableitem:d->tableitems) {
        QJsonObject tableitemObject;
        tableitem.Write(tableitemObject);
        tableitemArray.append(tableitemObject);
    }
    json["tableitem"] = tableitemArray;

    QJsonArray boundArray;
    for(auto bounder :d->bounders) {
        QJsonObject boundObject;
        bounder.Write(boundObject);
        boundArray.append(boundObject);
    }
    json["bounder"] = boundArray;

    QJsonArray setArray;
    for(auto setter: d->setters) {
        QJsonObject setObject;
        setter.Write(setObject);
        setArray.append(setObject);
    }
    json["setter"] = setArray;
}

auto IUiParameter::Read(const QJsonObject& json) -> void {
    d->enablers.clear();
    d->hiders.clear();
    d->tableitems.clear();
    d->bounders.clear();
    d->setters.clear();
    auto enArray = json["enabler"].toArray();
    for(auto item: enArray) {
        Enabler enabler;
        enabler.Read(item.toObject());
        d->enablers[enabler.key] = enabler;
    }

    auto hideArray = json["hider"].toArray();
    for(auto item:hideArray) {
        Hider hider;
        hider.Read(item.toObject());
        d->hiders[hider.key] = hider;
    }

    auto tableitemArray = json["tableitem"].toArray();
    for(auto item : tableitemArray) {
        TableItem tableitem;
        tableitem.Read(item.toObject());
        d->tableitems[tableitem.key] = tableitem;
    }

    auto boundArray = json["bounder"].toArray();
    for(auto item : boundArray) {
        Bounder bounder;
        bounder.Read(item.toObject());
        d->bounders[bounder.key] = bounder;
    }

    auto setArray = json["setter"].toArray();
    for(auto item : setArray) {
        Setter setter;
        setter.Read(item.toObject());
        d->setters[setter.key] = setter;
    }
}

auto IUiParameter::GetTableItemNames() const -> QStringList {
    return d->tableitems.keys();
}

auto IUiParameter::GetTableItems(const QString& key) const -> QStringList {
    if(false == d->tableitems.contains(key)) {
        return QStringList();
    }
    auto tableitem = d->tableitems[key];
    return tableitem.items;
}

auto IUiParameter::GetEnablerNames() const -> QStringList {
    return d->enablers.keys();
}

auto IUiParameter::GetEnabler(const QString& key) const -> std::tuple<QStringList, QStringList, QString> {
    if(false == d->enablers.contains(key)) {
        return std::make_tuple(QStringList(), QStringList(), QString());
    }
    auto enabler = d->enablers[key];
    return std::make_tuple(enabler.showNodes, enabler.hideNodes, enabler.displayName);
}

auto IUiParameter::GetSorterNames() const -> QStringList {
    return d->sorters.keys();
}

auto IUiParameter::GetSorter(const QString& key) const -> QStringList {
    if(false == d->sorters.contains(key)) {
        return QStringList();
    }
    auto sorter = d->sorters[key];
    return sorter.ordered_name;
}

auto IUiParameter::GetHiderNames() const -> QStringList {
    return d->hiders.keys();
}

auto IUiParameter::GetHider(const QString& key) const -> QStringList {
    if(false == d->hiders.contains(key)) {
        return QStringList();
    }
    auto hider = d->hiders[key];
    return hider.node_names;
}

auto IUiParameter::GetBounderNames() const -> QStringList {
    return d->bounders.keys();
}

auto IUiParameter::GetBounder(const QString& key) const -> std::tuple<QString, QString, QString> {
    if(false == d->bounders.contains(key)) {
        return std::make_tuple(QString(), QString(), QString());
    }
    auto bounder = d->bounders[key];
    return std::make_tuple(bounder.bounder, bounder.boundee, bounder.type);
}

auto IUiParameter::GetSetterNames() const -> QStringList {
    return d->setters.keys();
}

auto IUiParameter::GetSetter(const QString& key) const -> std::tuple<QString, QStringList, QStringList, QMap<QString, QMap<QString, ValueType>>> {
    if(false == d->setters.contains(key)) {
        return std::make_tuple(QString(), QStringList(), QStringList(), QMap<QString, QMap<QString, ValueType>>());
    }
    auto setter = d->setters[key];
    return std::make_tuple(setter.displayName, setter.node_names, setter.headers, setter.valueList);
}
