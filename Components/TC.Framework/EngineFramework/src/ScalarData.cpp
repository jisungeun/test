#pragma once

#include <QVariant>
#include <ScalarData.h>
#include <QMutex>

namespace TC::Framework {
	struct ScalarData::Impl {
	    QVariant value;
	    QMutex mutex;
	    int layer_idx {-1};
	    int label_idx{ -1 };
	};

	ScalarData::ScalarData() : IBaseData(), d { new Impl } {
	}

	ScalarData::ScalarData(char value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(int value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(unsigned int value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(long long value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(unsigned long long value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(float value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(double value) : IBaseData(), d { new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(bool value) : IBaseData(), d{ new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::ScalarData(QString value) : IBaseData(), d{ new Impl } {
	    QMutexLocker lock(&d->mutex);
	    d->value = value;
	}

	ScalarData::~ScalarData() {
	}

	auto ScalarData::New() -> Pointer {
	    Pointer data{ new ScalarData() };
	    return data;
	}

	auto ScalarData::New(char value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(int value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(unsigned int value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(long long value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(unsigned long long value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(float value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(double value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(bool value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::New(QString value) -> Pointer {
	    Pointer data{ new ScalarData(value) };
	    return data;
	}

	auto ScalarData::ValueAsChar(void) const->char {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toChar().toLatin1();
	}

	auto ScalarData::ValueAsInt(void) const->int {
	    return d->value.toInt();
	}

	auto ScalarData::ValueAsUInt(void) const->unsigned int {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toUInt();
	}

	auto ScalarData::ValueAsLLong(void) const->long long {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toLongLong();
	}

	auto ScalarData::ValueAsULLong(void) const->unsigned long long {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toULongLong();
	}

	auto ScalarData::ValueAsFloat(void) const->float {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toFloat();
	}

	auto ScalarData::ValueAsDouble(void) const->double {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toDouble();
	}

	auto ScalarData::ValueAsBool() const->bool {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toBool();
	}

	auto ScalarData::ValueAsString(void) const->QString {
	    QMutexLocker lock(&d->mutex);
	    return d->value.toString();
	}

	auto ScalarData::setLabelIdx(int idx) -> void {
	    QMutexLocker lock(&d->mutex);
	    d->label_idx = idx;
	}

	auto ScalarData::getLabelIdx() -> int {
	    QMutexLocker lock(&d->mutex);
	    return d->label_idx;
	}
}