#include <QList>
#include <utility>
#include "DataList.h"

#include <iostream>

namespace TC::Framework {
	struct DataList::Impl {
	    QList<DataSet::Pointer> list;
	};

	DataList::DataList() : d{ new Impl } {
	}

	DataList::~DataList() {
	    d->list.clear();
	}

	auto DataList::New() -> Pointer {
	    return std::make_shared<DataList>();
	}

	auto DataList::New(DataSet::Pointer data) -> Pointer {
	    auto list = std::make_shared<DataList>();
	    list->Append(data);
	    return list;
	}

	auto DataList::Append(IBaseData::Pointer data) -> void {
	    auto dataSet = DataSet::New();
	    dataSet->AppendData(std::move(data));
	    Append(dataSet);
	}

	auto DataList::Append(const DataSet::Pointer& dataSet) -> void {
	    d->list.append(dataSet);
	}

	auto DataList::GetData(const int index) const -> DataSet::Pointer {
	    if (index >= Count()) return nullptr;
	    return d->list[index];
	}

	auto DataList::GetList() -> QList<DataSet::Pointer>& {
	    return d->list;
	}

	auto DataList::Count() const -> int {
	    return d->list.length();
	}
}