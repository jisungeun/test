#include <QFile>
#include <QJsonDocument>
#include <QMutex>
#include "ParameterReader.h"

struct ParameterReader::Impl {
    QString path;
    QMutex mutex;
};

ParameterReader::ParameterReader(const QString& path)
    : d(new Impl) {
    d->path = path;

}

ParameterReader::~ParameterReader() {
    
}

auto ParameterReader::SetPath(const QString& path)->void {
    d->path = path;
}

auto ParameterReader::Read(IParameter::Pointer& parameter)->bool {
    parameter = Read();
    return parameter.get()!=nullptr;
}

#include <iostream>
auto ParameterReader::Read() -> IParameter::Pointer {
    QMutexLocker lock(&d->mutex);
    QFile loadFile(d->path);
    if (!loadFile.open(QIODevice::ReadOnly)) return IParameter::Pointer{ nullptr };

    auto doc{ QJsonDocument::fromJson(loadFile.readAll()) };
    
    IParameter::Pointer parameter{ new IParameter() };
    parameter->Read(doc.object());

    return parameter;
}

auto ParameterReader::ReadProp(const QString& key)const -> QString {
    QMutexLocker lock(&d->mutex);
    QFile loadFile(d->path);
    if (!loadFile.open(QIODevice::ReadOnly)) return QString();

    auto doc{ QJsonDocument::fromJson(loadFile.readAll()) };

    auto jObj = doc.object();
    if (!jObj.contains(key)) return QString();

    return jObj[key].toString();
}

auto ParameterReader::Read(IParameter::Pointer& parameter, const QString& strPath) -> bool {
    return ParameterReader(strPath).Read(parameter);
}

auto ParameterReader::Read(const QString& path) -> IParameter::Pointer {
    return ParameterReader(path).Read();
}
