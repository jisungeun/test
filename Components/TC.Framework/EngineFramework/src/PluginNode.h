#pragma once

#include <memory>
#include <QString>

class IPluginModule;

class PluginNode final {
public:
    explicit PluginNode(const QString& name, IPluginModule* target_module=nullptr, PluginNode* parent=nullptr);
    PluginNode(const PluginNode& node) = delete;
    virtual ~PluginNode();

    auto AppendChild(PluginNode* child)->void;

    auto Child(const int row)->PluginNode*;
    auto Child(const QString& name)->PluginNode*;
    auto ChildCount() const->int;
    auto Name() const->QString;
    auto Data() const->IPluginModule*;
    auto Row() const->int;
    auto ParentItem()->PluginNode*;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};