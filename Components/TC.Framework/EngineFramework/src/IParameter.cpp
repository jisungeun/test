#include <iostream>
#include <QMap>
#include <QJsonArray>

#include "IParameter.h"

struct IParameter::Impl {
	QMap<QString, Node> nodes;
    QMap<QString, Pointer> children;
    QMap<QString, QString> childrenDescription;

    QString version {QString()};
    QString fullName{ QString() };
};

auto IParameter::SetVersion(const QString& type_string) -> void {
    d->version = type_string;
}

auto IParameter::GetVersion() const -> QString {
    return d->version;
}

auto IParameter::SetFullName(const QString& fullName) -> void {
    d->fullName = fullName;
}

auto IParameter::GetFullName() const -> QString {
    return d->fullName;
}

IParameter::Node::Node() = default;
IParameter::Node::Node(const Node& node) = default;
IParameter::Node::~Node() = default;

IParameter::Node::Node(QString name, QString displayName, QString description, QString unit,
	                   ValueType value, ValueType defaultValue, ValueType minValue, ValueType maxValue)
    : name{ std::move(name) }
    , displayName{ std::move(displayName) }
    , description{ std::move(description) }
    , unit{ std::move(unit) }
    , value{ std::move(value) }
    , defaultValue{ std::move(defaultValue) }
    , minValue{ std::move(minValue) }
    , maxValue{ std::move(maxValue) }
{
    defaultValue = value;
}

auto IParameter::Node::Range(void) const->RangeType {
    return std::make_tuple(minValue, maxValue);
}

auto IParameter::Node::Range(const RangeType& range)->void {
    minValue = std::get<0>(range);
    maxValue = std::get<1>(range);
}

void IParameter::Node::Write(QJsonObject& json) {
    json["name"] = name;
    json["displayName"] = displayName;
    json["description"] = description;
    json["unit"] = unit;
    json["value"] = value;
    json["defaultValue"] = defaultValue;
    json["minValue"] = minValue;
    json["maxValue"] = maxValue;
}

void IParameter::Node::Read(const QJsonObject& json) {
    name = json["name"].toString();
    displayName = json["displayName"].toString();
    description = json["description"].toString();
    unit = json["unit"].toString();
    value = json["value"];
    defaultValue = json["defaultValue"];
    minValue = json["minValue"];
    maxValue = json["maxValue"];    
}

IParameter::IParameter(QObject* parent) : QObject(parent), d(new Impl) {
}

IParameter::~IParameter() {
}

auto IParameter::GetNames() const -> QStringList {
	return d->nodes.keys();
}

auto IParameter::GetNameAndTypes() const -> QList<std::tuple<QString, ValueType::Type>> {
	QList<std::tuple<QString, ValueType::Type>> name_types;

    for(const auto& node : d->nodes) {
		name_types.append(std::make_tuple(node.name, node.value.type()));
    }

	return name_types;
}

auto IParameter::ExistNode(const QString& name) const->bool {
    return d->nodes.contains(name);
}

auto IParameter::Type(const QString& name) const ->  ValueType::Type {
    if (!ExistNode(name)) return ValueType::Undefined;
    return d->nodes[name].value.type();
}

auto IParameter::DisplayName(const QString& name) const -> QString {
    if (!ExistNode(name)) return "";
    return d->nodes[name].displayName;
}

auto IParameter::Description(const QString& name) const -> QString {
    if (!ExistNode(name)) return "";
    return d->nodes[name].description;
}

auto IParameter::Unit(const QString& name) const -> QString {
    if (!ExistNode(name)) return "";
    return d->nodes[name].unit;
}

auto IParameter::SetValue(const QString& name, const ValueType& value)->bool {
    if (!ExistNode(name)) return false;
    d->nodes[name].value = value;
    return true;
}

auto IParameter::GetValue(const QString& name) const->ValueType {
    if (!ExistNode(name)) return ValueType();
    return d->nodes[name].value;
}

auto IParameter::GetValue(const QString& name, ValueType& value) const->bool {
    if (!ExistNode(name)) return false;
    value = d->nodes[name].value;
    return true;
}

auto IParameter::SetRange(const QString& name, const RangeType& range)->bool {
    if (!ExistNode(name)) return false;
    d->nodes[name].Range(range);
    return true;
}

auto IParameter::GetRange(const QString& name) const->RangeType {
    if (!ExistNode(name)) return RangeType();
    return d->nodes[name].Range();
}

auto IParameter::GetRange(const QString& name, RangeType& range) const->bool {
    if (!ExistNode(name)) return false;
    range = d->nodes[name].Range();
    return true;
}

auto IParameter::GetDefault(const QString& name) const->ValueType {
    if (!ExistNode(name)) return ValueType();
    return d->nodes[name].defaultValue;
}

auto IParameter::GetDefault(const QString& name, ValueType& value) const->bool {
    if (!ExistNode(name)) return false;
    value = d->nodes[name].defaultValue;
    return true;
}

auto IParameter::GetChildrenNames() const -> QStringList {
    return d->children.keys();
}

auto IParameter::SetChild(const QString& name, Pointer child) -> void {
    d->children[name] = child;
}

auto IParameter::GetChild(const QString& name)->Pointer {
    if (!ExistChild(name)) return nullptr;
    return d->children[name];
}

auto IParameter::GetChild(const QString& name) const -> Pointer {
    if (!ExistChild(name)) return nullptr;
    return d->children[name];
}

auto IParameter::GetChildDescription(const QString& name) const -> QString {
    if (!ExistChild(name)) return "";
    return d->childrenDescription[name];
}

auto IParameter::ExistChild(const QString& name) const -> bool {
    return d->children.contains(name);
}

IParameter::Iterator IParameter::begin(void) {
    return d->nodes.begin();
}

IParameter::Iterator IParameter::end(void) {
    return d->nodes.end();
}

IParameter::ConstIterator IParameter::constBegin(void) const {
    return d->nodes.constBegin();
}

IParameter::ConstIterator IParameter::constEnd(void) const {    
    return d->nodes.constEnd();
}

auto IParameter::IsEqual(const IParameter::Pointer other) const -> bool {
    if(nullptr == other) {
        return false;
    }
    return IsEqual(*(other.get()));
}
auto IParameter::IsValueEqual(const IParameter::Pointer other) const -> bool {
    if(nullptr == other) {
        return false;
    }
    return IsValueEqual(*(other.get()));
}

auto IParameter::IsEqual(const IParameter& other) const -> bool {    
    if (d->nodes.size() != other.d->nodes.size()) return false;
    if (d->children.size() != other.d->children.size()) return false;        

    return true;
}

auto IParameter::IsValueEqual(const IParameter& other) const -> bool {    
    for(auto name : d->nodes.keys()) {        
        if (!ExistNode(name)){            
            return false;
        }        
        if(d->nodes[name].value != other.d->nodes[name].value){            
            return false;
        }
    }

    for(auto name : d->children.keys()) {
        auto child = other.GetChild(name);
        if (child == nullptr) return false;
        //if (!d->children[name]->IsEqual(child)) return false;
        if (!d->children[name]->IsValueEqual(child)) return false;
    }
    return true;
}

auto IParameter::Clone() -> IParameter* {
    auto param = new IParameter();
    param->d->nodes = d->nodes;
    for(auto key : d->children.keys()) {
        param->d->children[key].reset(d->children.value(key)->Clone());
    }
    for(auto key : d->childrenDescription.keys()) {
        param->d->childrenDescription[key] = d->childrenDescription[key];
    }
    param->d->version = d->version;
    param->d->fullName = d->fullName;
    return param;
}

auto IParameter::RegisterNode(const QString& name, 
                              const QString& displayName, 
                              const QString& description,
                              const QString& unit,
                              const ValueType& value,
                              const ValueType& minValue,
                              const ValueType& maxValue)->void {
	d->nodes[name] = Node(name, displayName, description, unit, value, value, minValue, maxValue);
}

auto IParameter::RegisterChild(const QString& name, const QString& description, IParameter::Pointer child)->bool {
    if (ExistChild(name)) return false;
    d->children[name] = child;
    d->childrenDescription[name] = description;
    return true;
}



auto IParameter::Nodes()->QMap<QString, IParameter::Node>& {
    return d->nodes;
}

void IParameter::Write(QJsonObject& json) {
    QJsonArray nodesArray;
    for(auto node : d->nodes) {
        QJsonObject nodeObject;
        node.Write(nodeObject);
        nodesArray.append(nodeObject);
    }
    json["nodes"] = nodesArray;

    QJsonArray childrenArray;
    for(auto key : d->children.keys()) {
        QJsonObject childObject, paramObject;
        d->children.value(key)->Write(paramObject);
        childObject[key] = paramObject;
        childrenArray.append(childObject);
    }

    if(childrenArray.size()>0) {
        json["children"] = childrenArray;
    }
}

void IParameter::Read(const QJsonObject& json) {
    d->nodes.clear();
    auto nodesArray = json["nodes"].toArray();
    for(auto item : nodesArray) {
        Node node;
        node.Read(item.toObject());
        d->nodes[node.name] = node;
    }

    d->children.clear();
    auto childrenArray = json["children"].toArray();
    for (auto item : childrenArray) {
        auto object = item.toObject();
        auto name = object.keys().first();
        auto child_object = object.value(name).toObject();

        IParameter::Pointer parameter{ new IParameter() };
        parameter->Read(child_object);

        d->children[name] = parameter;
    }
}

