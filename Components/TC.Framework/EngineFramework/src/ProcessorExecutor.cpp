#include <QFutureWatcher>

#pragma warning(push)
#pragma warning(disable: 4458)
#include <QtConcurrent/QtConcurrent>
#pragma warning(pop)

#include <utility>
#include "ProcessorExecutor.h"

struct ProcessorExecutor::Impl {
    bool isBusy{ false };
    Error error{ Error::Success };
    QString errorMessage;

    QFutureWatcher<DataSet::Pointer>* watcher{ nullptr };
};

ProcessorExecutor::ProcessorExecutor(QObject* parent)
    : QObject(parent)
    , d{ new Impl() }
{
    d->watcher = new QFutureWatcher<DataSet::Pointer>(this);
    connect(d->watcher, &QFutureWatcher<DataSet::Pointer>::resultReadyAt, this, &ProcessorExecutor::reqProcessed);
    connect(d->watcher, &QFutureWatcher<DataSet::Pointer>::finished, this, &ProcessorExecutor::reqFinished);
}

ProcessorExecutor::~ProcessorExecutor() {
    d->watcher->cancel();
    d->watcher->waitForFinished();
    delete d->watcher;
}

auto ProcessorExecutor::Execute(IProcessor* processor, IBaseData::Pointer data, 
    const IParameter::Pointer& parameter) -> bool {
    auto dataSet = std::make_shared<DataSet>();
    dataSet->AppendData(std::move(data));
    return Execute(processor, dataSet, parameter);
}

auto ProcessorExecutor::Execute(IProcessor* processor, DataSet::Pointer dataSet,
    const IParameter::Pointer& parameter) -> bool {
    return Execute(processor, TC::Framework::DataList::New(dataSet), parameter);
}

auto ProcessorExecutor::Execute(IProcessor* processor, TC::Framework::DataList::Pointer dataList,
    const IParameter::Pointer& parameter) -> bool {
    if (!processor) return false;
    if (parameter.get()) {
        if (!processor->Parameter(parameter)) return false;
    }

    std::function<DataSet::Pointer(DataSet::Pointer)> runProcessor = [this, processor](DataSet::Pointer dataSet) {
        return Run(this, processor, dataSet);
    };

    SetBusy(true);
    d->watcher->setFuture(QtConcurrent::mapped(dataList->GetList(), runProcessor));

    return true;
}

auto ProcessorExecutor::IsBusy() const -> bool {
    return d->isBusy;
}

auto ProcessorExecutor::Progress() const -> double {
    return 0;
}

auto ProcessorExecutor::LastErrorCode() const -> Error {
    return d->error;
}

auto ProcessorExecutor::LastErrorString() const -> QString {
    return d->errorMessage;
}

auto ProcessorExecutor::ClearError() -> void {
    d->error = Error::Success;
    d->errorMessage = "";
}

auto ProcessorExecutor::SetError(Error error, const QString& errorMessage) -> void {
    d->error = error;
    d->errorMessage = errorMessage;
}

auto ProcessorExecutor::SetBusy(bool busy) -> void {
    d->isBusy = busy;
}

auto ProcessorExecutor::Run(ProcessorExecutor* executor, IProcessor* processor, DataSet::Pointer dataSet) -> DataSet::Pointer {
    DataSet::Pointer res;

    executor->ClearError();

    try {
        const auto len = dataSet->Count();
        processor->SetData(dataSet);
        if(processor->Execute()) {
            res = processor->GetResult();
        }
    }
    catch (std::exception& ex) {
        executor->SetError(Error::Fail, QString(ex.what()));
    }
    catch (...) {
        executor->SetError(Error::Fail, "Unknown error");
    }

    return res;
}

void ProcessorExecutor::reqProcessed(int index) {
    auto ret = d->watcher->resultAt(index);
    emit sigResult(ret);
}

void ProcessorExecutor::reqFinished(void) {
    SetBusy(false);
    emit sigFinished();
}