#include "IProcessor.h"
#include <iostream>

auto IProcessor::SetData(IBaseData::Pointer data, const QString& refName) -> void {
    SetData(DataSet::New(data, refName));
}

auto IProcessor::Parameter(const IParameter::Pointer parameter)->bool {
    auto param = Parameter();
    if (!param->IsEqual(parameter)) return false;

    RecursiveInput(param,parameter);
    return true;    
}

auto IProcessor::RecursiveInput(IParameter::Pointer target, IParameter::Pointer source) -> void {
    auto itr = source->constBegin();
    for (; itr != source->constEnd(); itr++) {        
        target->SetValue(itr->Name(), itr->Value());
    }
    auto childs = source->GetChildrenNames();
    for(auto ch:childs) {
        auto new_target = target->GetChild(ch);
        auto new_source = source->GetChild(ch);
        RecursiveInput(new_target,new_source);
    }
}

auto IProcessor::DuplicateParameter(const QStringList& keys) -> void {
    Q_UNUSED(keys)
}
