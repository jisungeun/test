#include "IProcessingAlgorithm.h"

#include <iostream>

auto IProcessingAlgorithm::Parameter(const IParameter::Pointer parameter, const QString& key) ->bool {
    if (key.isEmpty()) {
        auto param = Parameter();        
        if(parameter->GetVersion().isEmpty()) {                        
            for(auto m : parameter->GetNames()) {
                auto conv = ParamConverter(parameter, m);
                auto key = std::get<0>(conv);
                auto val = std::get<1>(conv);
                if (false == key.isEmpty()) {
                    if (param->ExistNode(key)) {
                        param->SetValue(key, val);
                    }
                }
            }            
        }else {
            if (!param->IsEqual(parameter)) {
                return false;
            }
            auto itr = parameter->constBegin();
            for (; itr != parameter->constEnd(); itr++) {
                param->SetValue(itr->Name(), itr->Value());
            }
        }        
    }else {
        auto param = Parameter(key);
        if (!param->IsEqual(parameter)){
            return false;
        }

        auto itr = parameter->constBegin();
        for(;itr !=parameter->constEnd();itr++) {
            param->SetValue(itr->Name(), itr->Value());
        }
    }

    return true;
}

auto IProcessingAlgorithm::CloneParameter() -> IParameter::Pointer {
    IParameter::Pointer clone{ Parameter()->Clone() };
    return clone;
}
