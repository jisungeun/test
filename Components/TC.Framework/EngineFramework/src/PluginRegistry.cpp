#define LOGGER_TAG "[PluginRegistry]"
#include <TCLogger.h>

#include <iostream>

#include <QPluginLoader>
#include <QDirIterator>
#include <QMap>

#include "PluginNode.h"
#include "PluginRegistry.h"

struct PluginRegistry::Impl {
	PluginNode* root{ nullptr };
	QStringList nodeDirectory;
};

PluginRegistry::PluginRegistry() : d(new Impl) {
	d->root = new PluginNode("org");
}

auto PluginRegistry::Register(const QString& fullname, IPluginModule* target_module) -> void {
	const auto toks = fullname.split('.');
	if (toks.at(0) != "org") return;

	auto parent = d->root;
	for(int idx=1; idx<toks.length()-1; idx++) {
		auto child = parent->Child(toks.at(idx));
		if (child) {
			parent = child;
			continue;
		} else {
			auto newChild = new PluginNode(toks.at(idx));
			parent->AppendChild(newChild);
			parent = newChild;
		}
	}

	parent->AppendChild(new PluginNode(toks.last(), target_module));
	d->nodeDirectory.push_back(fullname);
}

auto PluginRegistry::FindModule(const QString& name) -> IPluginModule* {
	auto toks = name.split('.');
	if (toks.at(0) != "org") return nullptr;

    auto parent = d->root;
	for (int idx = 1; idx < toks.length(); idx++) {
		auto child = parent->Child(toks.at(idx));
		if (child == nullptr) {
			return nullptr;
		}
		else {
			parent = child;
		}
	}

	auto target_module = parent->Data();
	return target_module;
}

auto PluginRegistry::FindModuleList(const QString& name) -> QStringList {
	QStringList list;

    for(auto node : d->nodeDirectory) {
		if (node.contains(name)) list.push_back(node);
	}

	return list;
}

PluginRegistry::~PluginRegistry() {
}

PluginRegistry::Pointer PluginRegistry::GetInstance() {
	static Pointer theInstance(new PluginRegistry());
	return theInstance;
}

auto PluginRegistry::LoadPlugin(const QString& path)->int {
	auto registry = PluginRegistry::GetInstance();
	QPluginLoader loader(path);	
	QObject* plugin = loader.instance();
	if (!plugin) {
		QLOG_ERROR() << " > error : " << loader.errorString();
		return -1;
	}

	auto target_module = dynamic_cast<IPluginModule*>(plugin);
	if (!target_module) return -1;

	if (!loader.metaData().contains("MetaData")) {
		QLOG_ERROR() << " > error : It failed to get metadata.";
		return -1;
	}

	const auto pluginMetadata = loader.metaData().value("MetaData").toObject();
	const auto fullName = pluginMetadata.value("FullName");
	if (fullName == QJsonValue::Undefined) {
		QLOG_ERROR() << " > error : It failed to get plugin full name.";
		return -1;
	}

	registry->Register(fullName.toString(), target_module);

	return registry->d->nodeDirectory.length();
}

auto PluginRegistry::LoadPlugins(const QStringList& pathList)->int {
	auto registry = PluginRegistry::GetInstance();

	for (auto pluginPath : pathList)
	{
		QDirIterator itr(pluginPath, QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
		while (itr.hasNext()) {
			auto strPath = itr.next();
			QDir pluginsDir(strPath);
			const auto entryList = pluginsDir.entryList(QStringList() << "*.dll", QDir::Files);
			for (const QString &fileName : entryList) {
				QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));

				QObject *plugin = loader.instance();
				if (!plugin) {
					QLOG_ERROR() << " > error : " << loader.errorString();
					continue;
				}

				auto target_module = dynamic_cast<IPluginModule*>(plugin);
				if (!target_module) continue;

				if (!loader.metaData().contains("MetaData")) {
		            QLOG_ERROR() << " > error : It failed to get metadata.";
		            continue;
	            }

				const auto pluginMetadata = loader.metaData().value("MetaData").toObject();
				const auto fullName = pluginMetadata.value("FullName");
				if (fullName == QJsonValue::Undefined) {
				    QLOG_ERROR() << " > error : It failed to get plugin full name.";
					continue;
				}

				registry->Register(fullName.toString(), target_module);
			}
		}
	}

	return registry->d->nodeDirectory.length();
}

auto PluginRegistry::GetPlugin(const QString& name,bool isPath)->IPluginModule::Pointer {
	if(isPath) {
		QPluginLoader loader(name);

		auto plugin = loader.instance();
		if(!plugin) {
			QLOG_ERROR() << " > error : " << loader.errorString();
			return nullptr;
		}
		auto path_module = dynamic_cast<IPluginModule*>(plugin);
		if(!path_module) {
		    return nullptr;
		}
		IPluginModule::Pointer newInstance(path_module->clone());
		return newInstance;
	}

    auto target_module = PluginRegistry::GetInstance()->FindModule(name);
    if (!target_module) return nullptr;

    IPluginModule::Pointer newInstance(target_module->clone());
    return newInstance;
}

auto PluginRegistry::GetPluginList(const QString& name) -> QStringList {
	return PluginRegistry::GetInstance()->FindModuleList(name);
}
