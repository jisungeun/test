#include <QJsonDocument>
#include <QFile>
#include <QMap>
#include <QMutex>
#include "ParameterWriter.h"

struct ParameterWriter::Impl {
    QString path;
    QMap<QString, QString> propList;
    QMutex mutex;
};

ParameterWriter::ParameterWriter(const QString& path)
    : d(new Impl) {
    d->path = path;
}
ParameterWriter::~ParameterWriter() {
    
}

auto ParameterWriter::SetPath(const QString& path)->void {
    d->path = path;
}

auto ParameterWriter::Write(IParameter::Pointer& parameter)->bool {
    QMutexLocker lock(&d->mutex);
    QFile fp(d->path);
    if (!fp.open(QIODevice::WriteOnly)) return false;

    QJsonObject jsonObject;
    parameter->Write(jsonObject);

    for(const auto &key : this->d->propList.keys()) {
        jsonObject[key] = this->d->propList[key];
    }

    QJsonDocument doc(jsonObject);
    const auto len = fp.write(doc.toJson(QJsonDocument::Indented));

    return (len>0);
}

auto ParameterWriter::WriteProp(const QString& key, const QString& value) -> void {    
    d->propList[key] = value;
}


auto ParameterWriter::Write(IParameter::Pointer& parameter, const QString& strPath) -> bool {
    return ParameterWriter(strPath).Write(parameter);
}
