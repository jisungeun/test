project(TCEngineFrameworkTest)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/TestMain.cpp
	src/IParameterTest.cpp
	src/ParameterIOTest.cpp
	src/PluginNodeTest.cpp
	src/PluginRegistryTest.cpp
	src/ProcessorExecutorTest.cpp
)

#Sources not exported
set(SOURCES_NOT_EXPORTED
	../src/PluginNode.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${SOURCES_NOT_EXPORTED}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
		TC::Components::EngineFramework
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases") 	

