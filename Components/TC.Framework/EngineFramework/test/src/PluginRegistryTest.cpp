#include <iostream>
#include <list>
#include "PluginRegistry.h"

#include <catch2/catch.hpp>

namespace _PluginRegistryTest {
    class PluginRegistyMock : public PluginRegistry {
    public:
        PluginRegistyMock() {}

        auto Register(const QString& fullname, IPluginModule* target_module)->void {
            PluginRegistry::Register(fullname, target_module);
        }

        auto FindModule(const QString& name)->IPluginModule* {
            return PluginRegistry::FindModule(name);
        }

        auto FindModuleList(const QString& name)->QStringList {
            return PluginRegistry::FindModuleList(name);
        }
    };

    class Algorithm1 : public IPluginModule {
    public:
        /*
         *  Name:           "Algorithm1"
         *  FullName:       "org.tomocube.plugins.algorithm.algorithm1"
         *  Description:    "Algorithm1"
         *  OutputFormat:   "test"
         */

        Algorithm1() = default;
        
        auto clone() const->IPluginModule* override { return new Algorithm1(); }
        auto DuplicateParameter(const QStringList& keys) -> void override {};
        auto Parameter(const QString& key)->IParameter::Pointer override { return nullptr; }
    };

    class Algorithm2 : public IPluginModule {
    public:
        /*
         *  Name:           "Algorithm2"
         *  FullName:       "org.tomocube.plugins.algorithm.algorithm2"
         *  Description:    "Algorithm2"
         *  OutputFormat:   "test"
         */

        Algorithm2() = default;

        auto clone() const->IPluginModule* override { return new Algorithm2(); }
        auto DuplicateParameter(const QStringList& keys) -> void override {};
        auto Parameter(const QString& key)->IParameter::Pointer override { return nullptr; }
    };

    class Processor1 : public IPluginModule {
    public:
        /*
         *  Name:           "Processor1"
         *  FullName:       "org.tomocube.plugins.processor.processor1"
         *  Description:    "Processor1"
         *  OutputFormat:   "test"
         */

        Processor1() = default;

        auto clone() const->IPluginModule* override { return new Processor1(); }
        auto DuplicateParameter(const QStringList& keys) -> void override {}
        auto Parameter(const QString& key)->IParameter::Pointer override { return nullptr; }
    };

    std::map<QString, IPluginModule*> plugins{
        { "org.tomocube.plugins.algorithm.algorithm1", new Algorithm1() },
        { "org.tomocube.plugins.algorithm.algorithm2", new Algorithm2() },
        { "org.tomocube.plugins.processor.processor1", new Processor1() }
    };
}

TEST_CASE("PluginRegistry", "[PluginRegistry]") {
    namespace test = _PluginRegistryTest;

    SECTION("Register modules") {
        auto registry = new test::PluginRegistyMock();

        for(auto it = test::plugins.begin(); it != test::plugins.end(); it++) {
            registry->Register(it->first, it->second);
        }

        for(auto it = test::plugins.begin(); it != test::plugins.end(); it++) {
            auto found = registry->FindModule(it->first);
            REQUIRE(found != nullptr);
        }
    }

    SECTION("Find module list") {
        auto registry = new test::PluginRegistyMock();

        for(auto it = test::plugins.begin(); it != test::plugins.end(); it++) {
            registry->Register(it->first, it->second);
        }

        auto list = registry->FindModuleList("plugins.algorithm");
        for(auto it = test::plugins.begin(); it != test::plugins.end(); it++) {
            auto fullname = it->first;
            if(fullname.contains("plugins.algorithm")) {
                auto res = list.contains(fullname);
                REQUIRE(true == res);
            }
        }
    }
}