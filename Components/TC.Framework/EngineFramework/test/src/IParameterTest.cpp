#include <QFile>
#include <QJsonDocument>

#include "IParameter.h"

#include <catch2/catch.hpp>

namespace _IParameterTest {
	struct Item {
		QString name;				//! name of node (unique key string)
		QString displayName;		//! display name of node
		QString description;		//! description about node
		QString unit;				//! unit in string
		QJsonValue value;				//! current value
		QJsonValue minValue;			//! minimum value
		QJsonValue maxValue;			//! maximum value

		QJsonValue::Type type;
	};

	QList<Item> tests{
		{"int", "int type", "int type parameter", "", 1, 1, 100, QJsonValue::Double},
	    {"double", "double type", "double type parameter", "", 1.1, 1.1, 100.1, QJsonValue::Double},
	    {"string", "string type", "string type parameter", "", "value", "", "", QJsonValue::String}
	};

	class ParameterMock : public IParameter {
	public:
		ParameterMock() {
			for(auto item : tests) {
				RegisterNode(item.name, item.displayName, item.description, item.unit, item.value, item.minValue, item.maxValue);
			}
		}

		void WriteTest(QJsonObject& json) {
			Write(json);
		}
	};

	class EmptyMock : public IParameter {
	public:
		EmptyMock() = default;

		void ReadTest(const QJsonObject& json) {
			Read(json);
		}
	};

};


TEST_CASE("IParameter", "[IParameter]") {
	namespace test = _IParameterTest;
	test::ParameterMock parameter;

	SECTION("Add nodes") {
		auto names = parameter.GetNames();
		REQUIRE(names.size() == test::tests.size());
	}

	SECTION("Get types") {
	    for(auto item : test::tests) {
			auto type = parameter.Type(item.name);
			REQUIRE(type == item.type);
	    }
	}

	SECTION("Check existence") {
	    for(auto item : test::tests) {
			auto exist = parameter.ExistNode(item.name);
			REQUIRE(exist == true);

			exist = parameter.ExistNode("_NO_EXIST");
			REQUIRE(exist == false);
	    }
	}

	SECTION("Default value is set automatically") {
	    for(auto item : test::tests) {
			auto defaultValue = parameter.GetDefault(item.name);
			REQUIRE(defaultValue == item.value);
	    }
	}

	SECTION("Export to file") {
		QFile saveFile("test.iparameter.json");
		auto res = saveFile.open(QIODevice::WriteOnly);
		REQUIRE(res == true);

		QJsonObject jsonObject;
		parameter.WriteTest(jsonObject);

		QJsonDocument doc(jsonObject);
		auto len = saveFile.write(doc.toJson());
		REQUIRE(len > 0);
	}

	SECTION("Import from file") {
		QFile loadFile("test.iparameter.json");
		auto res = loadFile.open(QIODevice::ReadOnly);
		REQUIRE(res == true);

		auto data = loadFile.readAll();

		auto doc{ QJsonDocument::fromJson(data) };

		test::EmptyMock loaded;
		loaded.ReadTest(doc.object());

		REQUIRE(parameter.IsEqual(loaded));
	}
}