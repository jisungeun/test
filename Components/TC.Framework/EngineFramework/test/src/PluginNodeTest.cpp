#include "../src/PluginNode.h"

#include <catch2/catch.hpp>

namespace _PluginNodeTest {
};


TEST_CASE("PluginNode", "[PluginNode]") {
	namespace test = _PluginNodeTest;

	SECTION("append child") {
		auto root = new PluginNode("org", nullptr);

	    root->AppendChild(new PluginNode("tomocube", nullptr));
		REQUIRE(root->ChildCount() == 1);

		root->AppendChild(new PluginNode("kbsi", nullptr));
		REQUIRE(root->ChildCount() == 2);
	}

	SECTION("get child by index") {
		auto root = new PluginNode("org", nullptr);

		root->AppendChild(new PluginNode("tomocube", nullptr));
		root->AppendChild(new PluginNode("kbsi", nullptr));

		auto child = root->Child(0);
		REQUIRE(child != nullptr);
		REQUIRE(child->Name() == "tomocube");

		child = root->Child(1);
		REQUIRE(child != nullptr);
		REQUIRE(child->Name() == "kbsi");

		child = root->Child(10000);
		REQUIRE(child == nullptr);
	}

	SECTION("get child by name") {
		auto root = new PluginNode("org", nullptr);

		root->AppendChild(new PluginNode("tomocube", nullptr));
		root->AppendChild(new PluginNode("kbsi", nullptr));

		auto child = root->Child("tomocube");
		REQUIRE(child != nullptr);
		REQUIRE(child->Name() == "tomocube");

		child = root->Child("kbsi");
		REQUIRE(child != nullptr);
		REQUIRE(child->Name() == "kbsi");

		child = root->Child("xxxx");
		REQUIRE(child == nullptr);
	}

	SECTION("get child count") {
		auto root = new PluginNode("org", nullptr);

		root->AppendChild(new PluginNode("tomocube", nullptr));
		root->AppendChild(new PluginNode("kbsi", nullptr));

		REQUIRE(root->ChildCount() == 2);
	}
}
