#include "ParameterReader.h"
#include "ParameterWriter.h"

#include <catch2/catch.hpp>

namespace _ParameterIOTest {
	struct Item {
		QString name;				//! name of node (unique key string)
		QString displayName;		//! display name of node
		QString description;		//! description about node
		QString unit;				//! unit in string
		QJsonValue value;				//! current value
		QJsonValue minValue;			//! minimum value
		QJsonValue maxValue;			//! maximum value

		QJsonValue::Type type;
	};

	QList<Item> tests{
		{"int", "int type", "int type parameter", "", 1, 1, 100, QJsonValue::Double},
		{"double", "double type", "double type parameter", "", 1.1, 1.1, 100.1, QJsonValue::Double},
		{"string", "string type", "string type parameter", "", "value", "", "", QJsonValue::String}
	};

	class ParameterChildMock : public IParameter {
	public:
		ParameterChildMock() {
			for (auto item : tests) {
				RegisterNode(item.name, item.displayName, item.description, item.unit, item.value, item.minValue, item.maxValue);
			}
		}
	};

	class ParameterMock : public IParameter {
	public:
		ParameterMock() {
			for (auto item : tests) {
				RegisterNode(item.name, item.displayName, item.description, item.unit, item.value, item.minValue, item.maxValue);
			}

			IParameter::Pointer child1{ new ParameterChildMock() };
			RegisterChild("child-1", "child-1", child1);

			IParameter::Pointer child2{ new ParameterChildMock() };
			RegisterChild("child-2", "child-1", child2);
		}
	};

	QString testfile{ "test.parameterio.json" };
};

TEST_CASE("ParameterIO", "[ParameterIO]") {
	namespace test = _ParameterIOTest;
	IParameter::Pointer parameter{ new test::ParameterMock() };

	SECTION("Export to file") {
		auto res = ParameterWriter::Write(parameter, test::testfile);
		REQUIRE(res == true);
	}

	SECTION("Import from file") {
		IParameter::Pointer loaded{ new IParameter() };
		auto res = ParameterReader::Read(loaded, test::testfile);
		REQUIRE(res == true);
		REQUIRE(parameter->IsEqual(loaded));
	}
}