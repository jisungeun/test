#pragma once

#include "IPluginModule.h"
#include "IParameter.h"
#include "IUiParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IProcessingAlgorithm : public IPluginModule {
public:
	virtual ~IProcessingAlgorithm() = default;

	virtual auto Parameter(const QString& key = QString()) -> IParameter::Pointer=0;
	virtual auto UiParameter()->IUiParameter::Pointer = 0;
	virtual auto DuplicateParameter(const QStringList& keys) -> void override = 0;
	virtual auto ParamConverter(IParameter::Pointer, QString key)->std::tuple<QString, QJsonValue> = 0;
	virtual auto CloneParameter(void)->IParameter::Pointer;	
	auto Parameter(const IParameter::Pointer parameter, const QString& key = QString())->bool;
	
	virtual auto Execute()->bool = 0;
};