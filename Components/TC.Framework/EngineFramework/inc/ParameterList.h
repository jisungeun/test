#pragma once

#include <memory>
#include <QObject>
#include <QStringList>
#include <QJsonValue>
#include <QMap>
#include <QMetaType>

#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ParameterNode {
public:
    ParameterNode() = default;
    ~ParameterNode() = default;
    ParameterNode(const ParameterNode&) = default;
    ParameterNode& operator=(const ParameterNode&) = default;

    auto IsScalarType(void)->bool;
    auto IsStringType(void)->bool;    

public:
    QString name;				//! name of node (unique key string)
    QString displayName;		//! display name of node
    QString description;		//! description about node
    QString unit;				//! unit in string
    QJsonValue value;			//! current value
    QJsonValue defaultValue;	//! default value which is set by constructor
    QJsonValue minValue;		//! minimum value
    QJsonValue maxValue;		//! maximum value
    int decimals{ -1 };         //! decimals (-1: No restrict)
};

class TCEngineFramework_API ParameterList {
public:
    typedef std::shared_ptr<ParameterList> Pointer;
    typedef QMap<QString, ParameterNode> Map;
    
public:
    ParameterList();
    ParameterList(const ParameterList& list);
    ~ParameterList();
    
    auto AppendNode(const ParameterNode& node)->void;
    auto GetNames() const->QStringList;
    auto GetNode(const QString& name) const->ParameterNode;
    
    auto GetMap(void) const->const Map*;
    auto Clear(void)->void;

    ParameterList& operator=(const ParameterList& other);
    
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};

Q_DECLARE_METATYPE(ParameterNode);
    