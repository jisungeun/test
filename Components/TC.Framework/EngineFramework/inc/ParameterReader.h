#pragma once

#include <memory>
#include <QString>
#include "IParameter.h"
#include "TCEngineFrameworkExport.h"

class IParameter;

class TCEngineFramework_API ParameterReader {
public:
    ParameterReader(const QString& path=QString());
    ~ParameterReader();

    auto SetPath(const QString& path)->void;
    auto Read(IParameter::Pointer& parameter)->bool;
    auto Read(void)->IParameter::Pointer;

    auto ReadProp(const QString& key)const->QString;

    static auto Read(IParameter::Pointer& parameter, const QString& strPath)->bool;
    static auto Read(const QString& path)->IParameter::Pointer;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};