#pragma once

#include <memory>
#include <QString>

#include "IBaseData.h"
#include "TCEngineFrameworkExport.h"

namespace TC::Framework {
	class TCEngineFramework_API ScalarData : public IBaseData {
	public:
	    typedef std::shared_ptr<ScalarData> Pointer;
	    
	public:
	    ScalarData();
	    ScalarData(char value);
	    ScalarData(int value);
	    ScalarData(unsigned int value);
	    ScalarData(long long value);
	    ScalarData(unsigned long long value);
	    ScalarData(float value);
	    ScalarData(double value);
	    ScalarData(bool value);
	    ScalarData(QString value);

	    virtual ~ScalarData();


	    static auto New()->Pointer;
	    static auto New(char value)->Pointer;
	    static auto New(int value)->Pointer;
	    static auto New(unsigned int value)->Pointer;
	    static auto New(long long value)->Pointer;
	    static auto New(unsigned long long value)->Pointer;
	    static auto New(float value)->Pointer;
	    static auto New(double value)->Pointer;
	    static auto New(bool value)->Pointer;
	    static auto New(QString value)->Pointer;
	    
	    auto ValueAsChar(void) const->char;
	    auto ValueAsInt(void) const->int;
	    auto ValueAsUInt(void) const->unsigned int;
	    auto ValueAsLLong(void) const->long long;
	    auto ValueAsULLong(void) const->unsigned long long;
	    auto ValueAsFloat(void) const->float;
	    auto ValueAsDouble(void) const->double;
	    auto ValueAsBool(void) const->bool;
	    auto ValueAsString(void) const->QString;
	        
	    auto setLabelIdx(int idx)->void;
	    auto getLabelIdx()->int;

	private:
	    struct Impl;
	    std::unique_ptr<Impl> d;
	};
}