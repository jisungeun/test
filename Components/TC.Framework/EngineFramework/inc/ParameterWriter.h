#pragma once

#include <memory>
#include <QString>
#include "IParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ParameterWriter {
public:
    ParameterWriter(const QString& path = QString());
    ~ParameterWriter();

    auto SetPath(const QString& path)->void;
    auto Write(IParameter::Pointer& parameter)->bool;

    auto WriteProp(const QString& key, const QString& value)->void;

    static auto Write(IParameter::Pointer& parameter, const QString& strPath)->bool;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};