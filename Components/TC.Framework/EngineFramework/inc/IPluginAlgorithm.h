#pragma once

#include "IPluginModule.h"
#include "IParameter.h"
#include "IUiParameter.h"
#include "IBaseData.h"

#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IPluginAlgorithm : public IPluginModule {
public:
	virtual ~IPluginAlgorithm() = default;

	virtual auto Parameter(const QString& key = QString()) -> IParameter::Pointer override = 0;

	virtual auto UiParameter() -> IUiParameter::Pointer = 0;
	virtual auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> = 0;
	virtual auto CloneParameter() -> IParameter::Pointer;

    virtual auto SetInput(int index, IBaseData::Pointer data) -> bool = 0;
	virtual auto GetOutput(int index) const -> IBaseData::Pointer = 0;
	virtual auto GetOutputs() const -> QList<IBaseData::Pointer> = 0;

	virtual auto Execute()->bool = 0;

    auto Parameter(const IParameter::Pointer parameter, const QString& key = QString()) -> bool;
};

#define IPluginAlgorithm_iid  "org.tomocube.processingalgorithm.ipluginalgorithm/1.0"
Q_DECLARE_INTERFACE(IPluginAlgorithm, IPluginAlgorithm_iid)