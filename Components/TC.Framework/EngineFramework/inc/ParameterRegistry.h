#pragma once

#include <memory>

#include "IParameter.h"
#include "IUiParameter.h"
#include "IMetaParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ParameterRegistry {
public:
	using TCreateMethod = std::shared_ptr<IParameter>(*)();

public:
	ParameterRegistry() = delete;

	static auto Register(const std::string& name, TCreateMethod funcCreate)->bool;
	static auto Create(const std::string& name)->IParameter::Pointer;

protected:
	static auto GetMap(void)->std::map<std::string, ParameterRegistry::TCreateMethod>&;
};

class TCEngineFramework_API UiParameterRegistry {
public:
	using  TCreateMethod = std::shared_ptr<IUiParameter>(*)();

	UiParameterRegistry() = delete;

	static auto Register(const std::string& name, TCreateMethod funcCreate)->bool;
	static auto Create(const std::string& name)->IUiParameter::Pointer;

protected:
	static auto GetMap(void)->std::map<std::string, UiParameterRegistry::TCreateMethod>&;
};

class TCEngineFramework_API MetaParameterRegistry {
public:
	using TCreateMethod = std::shared_ptr<IMetaParameter>(*)();

	MetaParameterRegistry() = delete;

	static auto Register(const std::string& name, TCreateMethod funcCreate)->bool;
	static auto Create(const std::string& name)->IMetaParameter::Pointer;

protected:
	static auto GetMap(void)->std::map<std::string, MetaParameterRegistry::TCreateMethod>&;
};