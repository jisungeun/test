#pragma once

#include <any>

#include <memory>
#include "IBaseData.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IBaseImage : public IBaseData {
public:
    using Self = IBaseImage;
    using SuperClass = IBaseData;
    using Pointer =  std::shared_ptr<Self>;

    using ImageDimensionType = unsigned int;        
    
public:
    IBaseImage();
    virtual ~IBaseImage();

    virtual auto clone()->IBaseImage* = 0;

    virtual auto addOffset(const float offset)->void = 0; 
    virtual auto getValue(void)->int = 0;
    virtual auto setChannel(int ch)->void = 0;
    virtual auto getChannel(void)->int = 0;

    virtual auto GetBuffer(void)->void* = 0;
    virtual auto GetBuffer(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax)->void* = 0;

protected:
    IBaseImage(const IBaseImage& ohter) = delete;
};