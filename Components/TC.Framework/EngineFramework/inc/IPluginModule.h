#pragma once

#include <memory>
#include <QString>

#include "IParameter.h"
#include "IUiParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IPluginModule {
public:
	typedef std::shared_ptr<IPluginModule> Pointer;

public:
	IPluginModule() = default;
	virtual ~IPluginModule() = default;

	//format - org.tomocube.category.interface.concrete_name
	//#1       org.tomocube.processor.cellcounter.processor_A
	//#2	   org.tomocube.algorithm.segmentation.segmentation_A
	virtual auto clone() const->IPluginModule* = 0;

	virtual auto Parameter(const QString& key = QString())->IParameter::Pointer = 0;	
	virtual auto DuplicateParameter(const QStringList& keys)->void = 0;
};