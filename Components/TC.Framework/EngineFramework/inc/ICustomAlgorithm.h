#pragma once

//#include "DataSet.h"
#include "DataList.h"
#include "IBaseData.h"
#include "IProcessingAlgorithm.h"
#include "TCEngineFrameworkExport.h"


class TCEngineFramework_API ICustomAlgorithm : public IProcessingAlgorithm {
public:
	virtual auto SetInput(IBaseData::Pointer data)->bool = 0;
	virtual auto SetInput2(IBaseData::Pointer data)->bool = 0;
	virtual auto SetInput3(IBaseData::Pointer data)->bool = 0;
	virtual auto GetOutput(void)->IBaseData::Pointer = 0;
	virtual auto FreeMemory(void)->void = 0;
};

#define ICustomAlgorithm_iid  "org.tomocube.processingalgorithm.icustomalgorithm/1.0"
Q_DECLARE_INTERFACE(ICustomAlgorithm, ICustomAlgorithm_iid)