#pragma once

#include "IBaseImage.h"
#include "IBaseMask.h"
#include "IProcessingAlgorithm.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ISegmentationAlgorithm : public IProcessingAlgorithm {
public:
	virtual auto SetInput(IBaseImage::Pointer image)->bool = 0;
	virtual auto SetInput(IBaseMask::Pointer input)->bool = 0;
	virtual auto GetOutput(int idx = -1)->IBaseMask::Pointer = 0;
	virtual auto GetLayerNames(QString key)->QStringList =0;
	virtual auto FreeMemory(void)->void = 0;
	//virtual auto SetDoubleValue(double val)->void = 0;

	virtual auto SetParameter(IParameter::Pointer param)->void=0;	
};

#define ISegmentationAlgorithm_iid "org.tomocube.processingalgorithm.isegmentationalgorithm/1.0"
Q_DECLARE_INTERFACE(ISegmentationAlgorithm, ISegmentationAlgorithm_iid)
