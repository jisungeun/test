#pragma once

#include "IParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ISegmentationAlgorithmParameter : public IParameter {
	Q_OBJECT
public:
	ISegmentationAlgorithmParameter();

};