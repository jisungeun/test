#pragma once

#include "IPluginModule.h"
#include "IMetaParameter.h"
#include "IParameter.h"
#include "DataSet.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IProcessor : public IPluginModule {
public:
    virtual ~IProcessor() = default;

    virtual auto Parameter(const QString& key = QString())->IParameter::Pointer = 0;
    virtual auto MetaParameter(const QString& key = QString())->IMetaParameter::Pointer = 0;
    virtual auto SetData(DataSet::Pointer data)->void = 0;
    virtual auto Execute()->bool = 0;
    virtual auto GetResult()->DataSet::Pointer = 0;
    virtual auto GetLayerName(QString key)->QStringList = 0;
    virtual auto FreeMemory()->void=0;

    auto SetData(IBaseData::Pointer data, const QString& refName=QString())->void;
    auto Parameter(const IParameter::Pointer parameter)->bool;
    auto DuplicateParameter(const QStringList& keys) -> void override;
private:
    auto RecursiveInput(IParameter::Pointer target,IParameter::Pointer source)->void;
};

#define IProcessor_iid "org.tomocube.plugins.processor/1.0"
Q_DECLARE_INTERFACE(IProcessor, IProcessor_iid)