#pragma once

#include <memory>
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IBaseData {
public:
    typedef std::shared_ptr<IBaseData> Pointer;

public:
    IBaseData();
    virtual ~IBaseData();

protected:
    IBaseData(const IBaseData& other) = delete;
};
