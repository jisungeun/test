#pragma once

#include <memory>

#include <QObject>
#include <QString>
#include <QMetaObject>
#include <QList>
#include <QMap>
#include <QJsonValue>
#include <QJsonObject>
#include <utility>
#include <tuple>

#include "TCEngineFrameworkExport.h"

enum TCEngineFramework_API SelectorType {
	SEL_RADIO_BUTTON = 0,
	SEL_CHECK_BOX = 1
};

class ParameterReader;
class ParameterWriter;

class TCEngineFramework_API IMetaParameter : public QObject {
	Q_OBJECT
public:
	typedef  std::shared_ptr<IMetaParameter> Pointer;

	struct Duplicator {	
		Duplicator();
		Duplicator(const Duplicator& dup);
		Duplicator(QString key,QStringList names,QString target);
		~Duplicator();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList mask_names;
		QString targetAlgoName;

		friend class IMetaParameter;
	};

	struct DefaultValue {
		DefaultValue();
		DefaultValue(const DefaultValue& def);
		DefaultValue(QString key, QString dupName, QString dupAlgoName, QString valueName, QJsonValue value);
		~DefaultValue();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QString dupName;
		QString dupAlgoName;
		QString valueName;
		QJsonValue value;

		friend class IMetaParameter;
	};

	struct Highlighter {	
		Highlighter();
		Highlighter(const Highlighter& high);
		Highlighter(QString key, QString sender, QString receiver);
		~Highlighter();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;		
		QString sender;//symbolic name
		QString receiver;

		friend class IMetaParameter;
	};

	struct Connector {	
		Connector();
		Connector(const Connector& connect);
		Connector(QString key,QStringList value_names, QString sender_name, QString receiver_name);
		~Connector();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList value_names;
		QString sender_name;
		QString receiver_name;

		friend class IMetaParameter;
	};

	struct Sorter {
		Sorter();
		Sorter(const Sorter& sort);
		Sorter(QString key, QStringList order);
		~Sorter();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList order;

		friend class IMetaParameter;
	};

	struct Fixer {
		Fixer();
		Fixer(const Fixer& fix);
		Fixer(QString key, QString dupName,QString dupAlgoName,QString valueName,QJsonValue value);
		~Fixer();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QString dupName;
		QString dupAlgoName;
		QString valueName;
		QJsonValue value;

		friend class IMetaParameter;
	};

	struct Selector {
		Selector();
		Selector(const Selector& select);
		Selector(QString key,QString target_node, int type, int max_item, QStringList algoNames,QStringList dispNames);
		~Selector();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QString target_node;
		int type;//0: checkbox , 1: radiobutton
		int max_items_in_row;
		QStringList algoNames;
		QStringList dispNames;

		friend class IMetaParameter;
	};

	struct Hider {
		Hider();
		Hider(const Hider& hide);
		Hider(QString key, QStringList algoNames);
		~Hider();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList algoNames;

		friend class IMetaParameter;
	};

	explicit IMetaParameter(QObject* parameter = nullptr);
	virtual ~IMetaParameter();

	auto Clone()->IMetaParameter*;

	auto GetFullName()const->QString;

    auto GetSorterNames()->QStringList;
	auto GetDuplicatorNames()->QStringList;
	auto GetConnecterNames()->QStringList;
	auto GetHighlighterNames()->QStringList;
	auto GetDefaultValueNames()->QStringList;
	auto GetFixerNames()->QStringList;
	auto GetHiderNames()->QStringList;
	auto GetSelectorNames()->QStringList;

	auto GetSorter(const QString& key)->QStringList;
	auto GetConnector(const QString& key)->std::tuple<QString,QString,QStringList>;
	auto GetHighlighter(const QString& key)->std::tuple<QString, QString>;
	auto GetDuplicator(const QString& key)->std::tuple<QStringList, QString>;
	auto GetDefaultValue(const QString& key)->std::tuple<QString,QString,QString,QJsonValue>;
	auto GetFixer(const QString& key)->std::tuple<QString, QString, QString, QJsonValue>;
	auto GetHider(const QString& key)->QStringList;
	auto GetSelector(const QString& key)->std::tuple<QString,int,int,QStringList,QStringList>;

protected:
	auto RegisterDuplicator(const QString& key, const QStringList& node_names,const QString& target)->void;
	auto RegisterHighlighter(const QString& key, const QString& sender,const QString& receiver)->void;
	auto RegisterConnector(const QString& key, const QStringList& value_names, const QString& sender_name, const QString& receiver_name)->void;
	auto RegisterSorter(const QString& key, const QStringList& order)->void;
	auto RegisterDefaultValue(const QString& key, const QString& dupName, const QString& dupAlgoName, const QString& valueName, QJsonValue value)->void;
	auto RegisterFixer(const QString& key, const QString& dupName, const QString& dupAlgoName, const QString& valueName, QJsonValue value)->void;
	auto RegisterHider(const QString& key, const QStringList& algoNames)->void;
	auto RegisterSelector(const QString& key,const QString&target_node, const int& type, const int& max_item, const QStringList& algoNames,const QStringList& dispNames)->void;

	auto SetFullName(const QString& fullname)->void;

	auto Dups()->QMap<QString, Duplicator>&;
	auto Highs()->QMap<QString, Highlighter>&;
	auto Connects()->QMap<QString, Connector>&;
	auto Sorts()->QMap<QString, Sorter>&;
	auto Values()->QMap<QString, DefaultValue>&;
	auto Fixes()->QMap<QString, Fixer>&;
	auto Hides()->QMap<QString, Hider>&;
	auto Selects()->QMap<QString, Selector>&;

	auto Write(QJsonObject& json)->void;
	auto Read(const QJsonObject& json)->void;

	friend class ParameterReader;
	friend class ParameterWriter;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};

Q_DECLARE_METATYPE(IMetaParameter::Duplicator)
Q_DECLARE_METATYPE(IMetaParameter::Highlighter)
Q_DECLARE_METATYPE(IMetaParameter::Connector)
Q_DECLARE_METATYPE(IMetaParameter::Sorter)
Q_DECLARE_METATYPE(IMetaParameter::DefaultValue)
Q_DECLARE_METATYPE(IMetaParameter::Fixer)
Q_DECLARE_METATYPE(IMetaParameter::Hider)
Q_DECLARE_METATYPE(IMetaParameter::Selector)