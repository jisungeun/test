#pragma once

#include <memory>
#include "IBaseData.h"
#include "TCEngineFrameworkExport.h"

#include <QString>

class DataSet {
public:
    typedef std::shared_ptr<DataSet> Pointer;

public:
    DataSet();
    virtual ~DataSet();

    static auto TCEngineFramework_API New(void)->Pointer;
    static auto TCEngineFramework_API New(IBaseData::Pointer data, const QString& refName = QString())->Pointer;

    auto TCEngineFramework_API AppendData(IBaseData::Pointer data, const QString& refName=QString())->void;
    auto TCEngineFramework_API SetData(int index, IBaseData::Pointer data, const QString& refName = QString())->void;
    auto TCEngineFramework_API SetData(IBaseData::Pointer data, const QString& refName = QString())->void;

    auto TCEngineFramework_API GetData(void)->IBaseData::Pointer;
    auto TCEngineFramework_API GetData(int index)->IBaseData::Pointer;
    auto TCEngineFramework_API GetData(const QString& name)->IBaseData::Pointer;

    auto TCEngineFramework_API Count(void)->int;
    auto TCEngineFramework_API Clear(void)->void;

protected:
    DataSet(const DataSet& other) = delete;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
