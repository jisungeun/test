#pragma once

#include <memory>
#include "IBaseData.h"
#include <QStringList>
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IBaseMask : public IBaseData {
public:
    typedef std::shared_ptr<IBaseMask> Pointer;

public:
    IBaseMask();
    virtual ~IBaseMask();

    virtual auto clone()->IBaseMask* = 0;

    virtual auto addOffset(const float offset)->void = 0; 
    virtual auto getValue(void)->unsigned short* = 0;
    virtual auto GetLayerNames(void)->QStringList = 0;

protected:
    IBaseMask(const IBaseMask& ohter) = delete;
};