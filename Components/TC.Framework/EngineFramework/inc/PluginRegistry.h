#pragma once

#include <memory>

#include <QStringList>

#include "IPluginModule.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API PluginRegistry {
	typedef PluginRegistry Self;
	typedef std::shared_ptr<Self> Pointer;

public:
	~PluginRegistry();

	static Pointer GetInstance();

	static auto LoadPlugin(const QString& path)->int;
	static auto LoadPlugins(const QStringList& pathList)->int;
	static auto GetPlugin(const QString& name,bool isPath = false)->IPluginModule::Pointer;
	static auto GetPluginList(const QString& name)->QStringList;

protected:
	PluginRegistry();
	PluginRegistry(const PluginRegistry& other) = delete;

	auto Register(const QString& fullname, IPluginModule* target_module)->void;
	auto FindModule(const QString& name)->IPluginModule*;
	auto FindModuleList(const QString& name)->QStringList;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};