#pragma once

#include <QObject>

#include <QVariant>
#include <QStringList>


class IAlgorithm {
public:
    virtual ~IAlgorithm() = default;

    virtual auto GetName() const -> QString = 0;
    virtual auto GetParameters() const -> QStringList = 0;
    virtual auto GetValue(const QString& parameter) const -> QVariant = 0;
    virtual auto SetValue(const QString& parameter, const QVariant& value) -> void = 0;
};

Q_DECLARE_INTERFACE(IAlgorithm, "IAlgorithm");