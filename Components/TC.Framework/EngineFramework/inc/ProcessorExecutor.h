#pragma once

#include <memory>
#include <QObject>

#include "IParameter.h"
#include "IProcessor.h"
#include "DataSet.h"
#include "DataList.h"

class ProcessorExecutor : public QObject {
    Q_OBJECT

public:
    enum class Error {
        Success,
        Fail
    };

public:
    ProcessorExecutor(QObject* parent=nullptr);
    ProcessorExecutor(const ProcessorExecutor& other) = delete;
    virtual ~ProcessorExecutor();

    auto Execute(IProcessor* processor, IBaseData::Pointer data, const IParameter::Pointer& parameter = nullptr)->bool;
    auto Execute(IProcessor* processor, DataSet::Pointer dataSet, const IParameter::Pointer& parameter = nullptr)->bool;
    auto Execute(IProcessor* processor, TC::Framework::DataList::Pointer dataSet, const IParameter::Pointer& parameter = nullptr)->bool;

    auto IsBusy(void) const->bool;
    auto Progress(void) const->double;
    auto LastErrorCode(void) const->Error;
    auto LastErrorString(void) const->QString;

protected:
    auto ClearError(void)->void;
    auto SetError(Error error, const QString& errorMessage)->void;
    auto SetBusy(bool busy)->void;

    static auto Run(ProcessorExecutor* executor, IProcessor* processor, DataSet::Pointer data)->DataSet::Pointer;

protected slots:
    void reqProcessed(int index);
    void reqFinished(void);

signals:
    void sigResult(DataSet::Pointer);
    void sigFinished(void);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
