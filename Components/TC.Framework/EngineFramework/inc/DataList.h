#pragma once

#include <memory>
#include "IBaseData.h"
#include "DataSet.h"

#include "TCEngineFrameworkExport.h"

namespace TC::Framework {
	class TCEngineFramework_API DataList : public IBaseData{
	public:
	    typedef std::shared_ptr<DataList> Pointer;

	public:
	    DataList();
	    DataList(const DataList& other) = delete;
	    ~DataList();

	    static auto New()->Pointer;
	    static auto New(DataSet::Pointer data)->Pointer;

	    auto Append(IBaseData::Pointer data)->void;
	    auto Append(const DataSet::Pointer& dataSet)->void;

	    auto GetData(const int index) const->DataSet::Pointer;
	    auto GetList(void)->QList<DataSet::Pointer>&;

	    auto Count(void) const->int;

	private:
	    struct Impl;
	    std::shared_ptr<Impl> d;
	};	
}
