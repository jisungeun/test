#pragma once


#include "IParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API ICustomAlgorithmParameter : public IParameter {
    Q_OBJECT
public:
    ICustomAlgorithmParameter();
};