#pragma once

#include "IParameter.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IDenoisingAlgorithmParameter : public IParameter {
    Q_OBJECT
public:
    IDenoisingAlgorithmParameter();
};