#pragma once

#include <memory>

#include <QObject>
#include <QString>
#include <QMetaObject>
#include <QList>
#include <QMap>
#include <QJsonValue>
#include <QJsonObject>
#include <utility>
#include <tuple>
#include "TCEngineFrameworkExport.h"

class ParameterReader;
class ParameterWriter;

class TCEngineFramework_API IParameter : public QObject {
	Q_OBJECT
public:
	typedef QJsonValue ValueType;
	typedef std::tuple<ValueType, ValueType> RangeType;
	typedef std::shared_ptr<IParameter> Pointer;

    struct Node {
    public:
        Node();
        Node(const Node& node);
		Node(QString name, QString displayName, QString description, QString unit,
			ValueType value, ValueType defaultValue, ValueType minValue, ValueType maxValue);
        ~Node();
		auto Range(void) const->RangeType;
		auto Range(const RangeType& range)->void;

		auto Name(void) const->QString { return name; }
		auto Value(void) const->ValueType { return value; }

    protected:
		void Write(QJsonObject& json);
		void Read(const QJsonObject& json);

    protected:
		QString name;				//! name of node (unique key string)
		QString displayName;		//! display name of node
		QString description;		//! description about node
		QString unit;				//! unit in string
		ValueType value;			//! current value
		ValueType defaultValue;		//! default value which is set by constructor
		ValueType minValue;			//! minimum value
		ValueType maxValue;			//! maximum value

		friend class IParameter;
    };

	typedef QMap<QString, Node>::Iterator Iterator;
	typedef QMap<QString, Node>::ConstIterator ConstIterator;

public:
    explicit IParameter(QObject* parent = nullptr);
    virtual ~IParameter();		

	auto SetVersion(const QString& versionString)->void;
	auto GetVersion()const->QString;

	auto SetFullName(const QString& fullName)->void;
	auto GetFullName()const->QString;

	auto GetNames() const->QStringList;
	auto GetNameAndTypes() const->QList<std::tuple<QString, ValueType::Type>>;

	auto ExistNode(const QString& name) const->bool;

	auto Type(const QString& name) const->ValueType::Type;

	auto DisplayName(const QString& name) const->QString;
	auto Description(const QString& name) const->QString;
	auto Unit(const QString& name) const->QString;

	auto SetValue(const QString& name, const ValueType& value)->bool;
	auto GetValue(const QString& name) const->ValueType;
	auto GetValue(const QString& name, ValueType& value) const->bool;

	auto SetRange(const QString& name, const RangeType& range)->bool;
	auto GetRange(const QString& name) const->RangeType;
	auto GetRange(const QString& name, RangeType& range) const->bool;

	auto GetDefault(const QString& name) const->ValueType;
	auto GetDefault(const QString& name, ValueType& value) const->bool;

	auto GetChildrenNames() const->QStringList;
	auto SetChild(const QString& name, Pointer child)->void;
	auto GetChild(const QString& name)->Pointer;
	auto GetChild(const QString& name) const->Pointer;
	auto GetChildDescription(const QString& name) const->QString;
    auto ExistChild(const QString& name) const->bool;		

	Iterator begin(void);
	Iterator end(void);
	ConstIterator constBegin(void) const;
	ConstIterator constEnd(void) const;

	auto IsEqual(const IParameter::Pointer other) const->bool;
	auto IsEqual(const IParameter& other) const->bool;

	auto IsValueEqual(const IParameter::Pointer other)const->bool;
	auto IsValueEqual(const IParameter& other)const ->bool;

	auto Clone(void)->IParameter*;

protected:
	auto RegisterNode(const QString& name, const QString& displayName, const QString& description, const QString& unit,
		              const ValueType& value, const ValueType& minValue, const ValueType& maxValue)->void;

	auto RegisterChild(const QString& name, const QString& description, IParameter::Pointer child)->bool;

	auto Nodes(void)->QMap<QString, Node>&;

	void Write(QJsonObject& json);
	void Read(const QJsonObject& json);

	friend class ParameterReader;
	friend class ParameterWriter;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};

Q_DECLARE_METATYPE(IParameter::Node);
Q_DECLARE_METATYPE(IParameter::Pointer)
