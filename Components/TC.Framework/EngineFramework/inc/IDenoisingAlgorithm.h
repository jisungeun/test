#pragma once

#include "IBaseImage.h"
#include "IProcessingAlgorithm.h"
#include "TCEngineFrameworkExport.h"

class TCEngineFramework_API IDenoisingAlgorithm : public IProcessingAlgorithm {
public:
	virtual auto SetInput(IBaseImage::Pointer image)->bool = 0;
	virtual auto GetOutput(void)->IBaseImage::Pointer = 0;
};

#define IDenoisingAlgorithm_iid "org.tomocube.processingalgorithm.idenoisingalgorithm/1.0"
Q_DECLARE_INTERFACE(IDenoisingAlgorithm, IDenoisingAlgorithm_iid)