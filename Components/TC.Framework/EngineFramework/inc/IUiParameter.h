#pragma once

#include <memory>

#include <QObject>
#include <QString>
#include <QMetaObject>
#include <QList>
#include <QMap>
#include <QJsonValue>
#include <QJsonObject>
#include <utility>
#include <tuple>

#include "TCEngineFrameworkExport.h"

class ParameterReader;
class ParameterWriter;

class TCEngineFramework_API EnablerNode {
public:
	EnablerNode() = default;
	~EnablerNode() = default;
	EnablerNode(const EnablerNode&) = default;
	EnablerNode& operator=(const EnablerNode&) = default;

public:
	QStringList showNodes;
	QStringList hideNodes;
	QString displayName;
};

class TCEngineFramework_API BounderNode {
public:
	BounderNode() = default;
	~BounderNode() = default;
	BounderNode(const BounderNode&) = default;
	BounderNode& operator=(const BounderNode&) = default;

public:
	QString bounder;
	QString boundee;
	QString type;
};

class TCEngineFramework_API SetterNode {
public:
	SetterNode() = default;
	~SetterNode() = default;
	SetterNode(const SetterNode&) = default;
	SetterNode& operator=(const SetterNode&) = default;

public:
	QString displayName;
	QStringList node_names;
	QStringList headers;
	QMap<QString, QMap<QString, QJsonValue>> valueList;
};

class TCEngineFramework_API IUiParameter : public QObject {
	Q_OBJECT
public:
	typedef QJsonValue ValueType;
	typedef std::shared_ptr<IUiParameter> Pointer;

	struct Enabler {
		Enabler();
		Enabler(const Enabler& enable);
		Enabler(QString key,QString dispName,QStringList showNodes,QStringList hideNodes);
		~Enabler();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList showNodes;
		QStringList hideNodes;
		QString displayName;

		friend class IUiParameter;
	};
	struct Sorter {
		Sorter();
		Sorter(const Sorter& sorter);
		Sorter(QString key, QStringList ordered_name);
		~Sorter();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList ordered_name;

		friend class IUiParameter;
	};
	struct TableItem {
		TableItem();
		TableItem(const TableItem& tableitem);
		TableItem(QString key, QStringList items);
		~TableItem();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList items;

		friend class IUiParameter;
	};
	struct Hider {
		Hider();
		Hider(const Hider& hider);
		Hider(QString key,QStringList node_names);
		~Hider();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QStringList node_names;

		friend class IUiParameter;
	};

	struct Bounder {
		Bounder();
		Bounder(const Bounder& bounder);
		Bounder(QString key,QString bounder, QString boundee,QString type);
		~Bounder();
	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QString bounder;
		QString boundee;
		QString type;

		friend class IUiParameter;
	};

	struct Setter {
		Setter();
		Setter(const Setter& setter);
		Setter(QString key,QString displayName,QStringList node_names,QStringList headers);
		~Setter();

		auto Append(QString node_name, QString header, ValueType val)->void;

	protected:
		auto Write(QJsonObject& json)->void;
		auto Read(const QJsonObject& json)->void;
	protected:
		QString key;
		QString displayName;
		QStringList node_names;
		QStringList headers;
		QMap<QString,QMap<QString,ValueType>> valueList;

		friend class IUiParameter;
	};

	explicit IUiParameter(QObject* parameter = nullptr);
	virtual ~IUiParameter();

	auto Clone()->IUiParameter*;
		
	auto GetType()const->QString;
	auto GetFullName()const->QString;

	auto GetEnablerNames()const->QStringList;
	auto GetEnabler(const QString& key)const->std::tuple<QStringList, QStringList, QString>;
	auto GetSorterNames()const->QStringList;
	auto GetSorter(const QString& key)const->QStringList;
	auto GetTableItemNames()const->QStringList;
	auto GetTableItems(const QString& key)const->QStringList;
	auto GetHiderNames()const->QStringList;
	auto GetHider(const QString& key)const->QStringList;
	auto GetBounderNames()const->QStringList;
	auto GetBounder(const QString& key)const->std::tuple<QString, QString, QString>;
	auto GetSetterNames()const->QStringList;
	auto GetSetter(const QString& key)const->std::tuple < QString, QStringList, QStringList, QMap<QString, QMap<QString, ValueType>>>;	

protected:
	auto SetType(const QString& type)->void;
	auto SetFullName(const QString& fullname)->void;

	auto RegisterEnabler(const QString& key, const QString& dispName, const QStringList& showNodes, const QStringList& hideNodes)->void;
	auto RegisterSorter(const QString& key, const QStringList& ordered_name)->void;
	auto RegisterHider(const QString& key, const QStringList& node_names)->void;
	auto RegisterTableItem(const QString& key, const QStringList& items)->void;
	auto RegisterBounder(const QString& key, const QString& bounder, const QString& boundee, const QString& type)->void;
	auto RegisterSetter(const QString& key,const QString& displayName, const QStringList& node_names, const QStringList& headers)->void;
	auto AppendSetter(const QString& key, const QString& node_name, const QString& header, const ValueType& val)->void;
		
	auto Enables()->QMap<QString, Enabler>&;
	auto Hides()->QMap<QString, Hider>&;
	auto Bounds()->QMap<QString, Bounder>&;
	auto Sets()->QMap<QString, Setter>&;
	auto TableItems()->QMap<QString, TableItem>&;

	auto Write(QJsonObject& json)->void;
	auto Read(const QJsonObject& json)->void;

	friend class ParameterReader;
	friend class ParameterWriter;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};

Q_DECLARE_METATYPE(IUiParameter::Enabler)
Q_DECLARE_METATYPE(IUiParameter::Sorter)
Q_DECLARE_METATYPE(IUiParameter::Hider)
Q_DECLARE_METATYPE(IUiParameter::Bounder)
Q_DECLARE_METATYPE(IUiParameter::Setter)
Q_DECLARE_METATYPE(IUiParameter::TableItem)