#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtConcurrent/QtConcurrent>
#include <QObject>

#include "HttpClient.h"

namespace TC {
	struct HttpClient::Impl {
		QString baseAddress;
		QNetworkAccessManager manager;

		auto Post(const QString& url, const QByteArray& json) ->QNetworkReply* {
			const QString dest = (url.startsWith("http")) ? QString("%2").arg(url) : QString("%1%2").arg(baseAddress).arg(url);
			QNetworkRequest req(dest);
			const QByteArray size = QByteArray::number(json.size());

			req.setRawHeader("Content-Type", "application/json");
			req.setRawHeader("Content-Length", size);

			return manager.post(req, json);
		}

		auto Get(const QString& url) ->QNetworkReply* {
			const QString dest = (url.startsWith("http")) ? QString("%2").arg(url) : QString("%1%2").arg(baseAddress).arg(url);
			QNetworkRequest req(dest);

			return manager.get(req);
		}

		auto ConcatUrl(const QString& url, const QMap<QString, QString>& params) -> QString {
			if (!params.empty()) {
				QString result = url + "?";

				foreach(const auto & p, params.keys())
					result.append(p + "=" + params[p]);

				return result;
			}

			return url;
		}
	};

	HttpClient::HttpClient() : d(new Impl) {
	}

	HttpClient::HttpClient(const QString& baseAddress) : d(new Impl) {
		SetBaseAddress(baseAddress);
	}

	HttpClient::~HttpClient() = default;

	auto HttpClient::GetBaseAddress() const -> const QString& {
		return d->baseAddress;
	}

	auto HttpClient::SetBaseAddress(const QString& url) const -> void {
		d->baseAddress = url;

		while (d->baseAddress.endsWith("/"))
			d->baseAddress.remove(d->baseAddress.length() - 1, 1);
	}

	auto HttpClient::Post(const QString& url, const QByteArray& json) const -> QNetworkReply* {
		auto* response = d->Post(url, json);
		return response;
	}

    auto HttpClient::Get(const QString& url) const -> QNetworkReply* {
		auto* response = d->Get(url);
		return response;
	}

	auto HttpClient::Get(const QString& url, const QMap<QString, QString>& params) const -> QNetworkReply* {
		const auto purl = d->ConcatUrl(url, params);
		auto* response = d->Get(purl);
		return response;
	}
}
