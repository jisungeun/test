#pragma once

#include <memory>

#include "TCHttpClientExport.h"

namespace TC {
	class TCHttpClient_API HttpClient {
	public:
		HttpClient();
		HttpClient(const QString& baseAddress);
		~HttpClient();

		[[nodiscard]] auto GetBaseAddress() const -> const QString&;
		auto SetBaseAddress(const QString& url) const -> void;
		
		[[nodiscard]] auto Post(const QString& url, const QByteArray& json) const->QNetworkReply*;
		[[nodiscard]] auto Get(const QString& url) const->QNetworkReply*;
		[[nodiscard]] auto Get(const QString& url, const QMap<QString, QString>& params) const->QNetworkReply*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}