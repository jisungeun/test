﻿#pragma once

#include <QDialog>
#include <QMap>
#include <QStringList>

#include <TCFMetaReader.h>

#include "TimePointSelectorDef.h"
#include "TCTimePointSelectorExport.h"

namespace TC::TimePointSelector {
	class TCTimePointSelector_API TimePointSettingDialog : public QDialog {
		Q_OBJECT
	public:
		explicit TimePointSettingDialog(const Config& config, QWidget* parent = nullptr);
		~TimePointSettingDialog() override;

		auto SetTCFs(const QStringList& tcfs) -> void;

		auto GetResult() -> QMap<QString, TimeSelectionOutput>;
		auto GetMeta(const QString& path) -> IO::TCFMetaReader::Meta::Pointer;

	protected:
		void keyPressEvent(QKeyEvent *e) override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
