﻿#pragma once

#include <QTableWidget>

#include "TimePointSelectorDef.h"

namespace TC::TimePointSelector {
    class TimePointTableWidget : public QTableWidget {
        Q_OBJECT
	public:
        explicit TimePointTableWidget(QWidget* parent = nullptr);
        ~TimePointTableWidget() override;

        auto SetTimePoints(const QMap<Modality, TimePoints>& allTimepoints, const QMap<Modality, TimePoints>& timepoints) -> void;

        auto SetFiltering(bool filtered) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
