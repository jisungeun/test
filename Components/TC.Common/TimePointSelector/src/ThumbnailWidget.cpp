﻿#include <QDebug>

#include "ThumbnailWidget.h"
#include "SceneManager.h"
#include "RenderWindow.h"

#include "ui_ThumbnailWidget.h"

namespace TC::TimePointSelector {
    using TCFMetaPtr = IO::TCFMetaReader::Meta::Pointer;

    enum class ThumbnailModality {
        None    = 0,
        HT,
        FL
    };

    struct ThumbnailWidget::Impl {
        Ui::ThumbnailWidget ui;

        std::unique_ptr<SceneManager> sceneManager;
        std::unique_ptr<RenderWindow> renderWindow;
        std::unique_ptr<QWidget> missingContainer;
        std::unique_ptr<QLabel> missingLabel;

        QMap<QString, TCFInfo> tcfs;    // key: TCF title, value, TCF meta data

        auto ClearTCFInfo() -> void;
        auto ClearTCFModalityInfo() -> void;

        auto UpdateModalityList() -> void;
        auto UpdateTCFModalityInfo() -> void;
        auto UpdateImage() -> void;
        auto UpdateDataRange() -> void;

        auto GetPhyTimes(TCFMetaPtr meta, ThumbnailModality modality) -> TimePoints;
        auto UpdateMissing()->void;        
    };
        
    auto ThumbnailWidget::Impl::UpdateMissing() -> void {
        const auto modality = static_cast<ThumbnailModality>(ui.modalityComboBox->currentData().toInt());
        QString modalTxt;
        if(modality == ThumbnailModality::HT) {
            modalTxt = "HT";
        }else if(modality == ThumbnailModality::FL) {
            modalTxt = "FL";
        }        

        missingLabel->setText("No " + modalTxt);        
	}

    auto ThumbnailWidget::Impl::ClearTCFInfo() -> void {
        ui.modalityComboBox->clear();
        ClearTCFModalityInfo();
    }

    auto ThumbnailWidget::Impl::ClearTCFModalityInfo() -> void {
        sceneManager->Clear();
        sceneManager->ClearScene();

        ui.rangeMinSpinBox->setValue(0);
        ui.rangeMaxSpinBox->setValue(255);
        ui.currentTimePointSpinBox->clear();
        ui.currentTimePointSpinBox->setRange(0, 0);
        ui.timeLabel->clear();
    }

    auto ThumbnailWidget::Impl::UpdateModalityList() -> void {
        ClearTCFInfo();

	    const auto tcfInfo = tcfs.value(ui.tcfComboBox->currentText());
        const auto tcfMeta = tcfInfo.meta;
		if (tcfMeta == nullptr) return;

        // update a combobox for modality list
        if (tcfMeta->thumbnail.ht.exist || tcfMeta->data.data2DMIP.exist || tcfMeta->data.data3D.exist) {
        	ui.modalityComboBox->addItem("HT", static_cast<int>(ThumbnailModality::HT));
        }

    	if (tcfMeta->thumbnail.fl.exist || tcfMeta->data.data2DFLMIP.exist || tcfMeta->data.data3DFL.exist) {
        	ui.modalityComboBox->addItem("FL", static_cast<int>(ThumbnailModality::FL));
        }
    }

    auto ThumbnailWidget::Impl::UpdateTCFModalityInfo() -> void {
        const auto prevIdx = ui.currentTimePointSpinBox->value();
        ClearTCFModalityInfo();

	    const auto tcfInfo = tcfs.value(ui.tcfComboBox->currentText());
        const auto tcfMeta = tcfInfo.meta;
        if (tcfMeta == nullptr) return;

        //const auto modality = static_cast<ThumbnailModality>(ui.modalityComboBox->currentData().toInt());
                
        //const auto phyTimes = GetPhyTimes(tcfMeta, modality);
        const auto phyTimes = tcfMeta->data.total_time;
        if (!phyTimes.isEmpty()) {
            const auto timeCount = phyTimes.count();

            ui.currentTimePointSpinBox->setRange(1, timeCount);

            const auto currentTimeIndex = prevIdx < 1 ? 1 : prevIdx;
            ui.currentTimePointSpinBox->setValue(currentTimeIndex);

        	const auto time = static_cast<int>(phyTimes.at(currentTimeIndex - 1));
            const auto timeStr = QString("%1:%2:%3").arg(time / 3600).arg((time % 3600) / 60, 2, 10, QChar('0')).arg((time % 3600) % 60, 2, 10, QChar('0'));

            ui.timeLabel->setText(QString("%1 (#%2/%3)").arg(timeStr).arg(currentTimeIndex).arg(timeCount));

            UpdateMissing();
        }
    }

    auto ThumbnailWidget::Impl::UpdateImage() -> void {
        sceneManager->Clear();

	    const auto tcfInfo = tcfs.value(ui.tcfComboBox->currentText());
        if (tcfInfo.path.isEmpty()) return;

        const auto tcfMeta = tcfInfo.meta;
        if (tcfMeta == nullptr) return;

        if (tcfInfo.path != sceneManager->GetTCFPath()) {
	        sceneManager->SetTCFPath(tcfInfo.path);
			sceneManager->SetMeta(tcfMeta);
        }

        const auto phyTime = tcfMeta->data.total_time[ui.currentTimePointSpinBox->value() - 1];                        

        const auto modality = static_cast<ThumbnailModality>(ui.modalityComboBox->currentData().toInt());
        switch (modality) {
        case ThumbnailModality::HT:
        {
            int timepoint = -1;
            if (tcfMeta->data.data2DMIP.timePoints.contains(phyTime)){
                timepoint = tcfMeta->data.data2DMIP.timePoints.indexOf(phyTime);
            }else if(tcfMeta->data.data3D.timePoints.contains(phyTime)) {
                timepoint = tcfMeta->data.data3D.timePoints.indexOf(phyTime);
            }
            if(timepoint < 0) {
                renderWindow->hide();
                missingContainer->show();
            }
            else {
                renderWindow->show();
                missingContainer->hide();
                sceneManager->ReadHT(timepoint);
            }
        }
            break;
        case ThumbnailModality::FL:
        {
            int timepoint = -1;
            for (auto i = 0; i < 3; i++) {
                if (tcfMeta->data.data2DFLMIP.timePoints[i].contains(phyTime)) {
                    timepoint = tcfMeta->data.data2DFLMIP.timePoints[i].indexOf(phyTime);
                    break;
                }
                if (tcfMeta->data.data3DFL.timePoints[i].contains(phyTime)) {
                    timepoint = tcfMeta->data.data3DFL.timePoints[i].indexOf(phyTime);
                    break;
                }
            }
            if(timepoint < 0 ) {
                renderWindow->hide();
                missingContainer->show();
            }
            else {
                renderWindow->show();
                missingContainer->hide();
                sceneManager->ReadFL(timepoint);
            }
        }
            break;
        }

        sceneManager->SetDataRange(ui.rangeMinSpinBox->value(), ui.rangeMaxSpinBox->value());
        renderWindow->ResetView2D();
    }

    auto ThumbnailWidget::Impl::UpdateDataRange() -> void {
        sceneManager->SetDataRange(ui.rangeMinSpinBox->value(), ui.rangeMaxSpinBox->value());
    }

    auto ThumbnailWidget::Impl::GetPhyTimes(TCFMetaPtr meta, ThumbnailModality modality) -> TimePoints {
    	TimePoints phyTimes;

        const auto AppendTimes = [&phyTimes](TimePoints times) {
	        for (auto time : times) {
				if (!phyTimes.contains(time))
                    phyTimes << time;
			}
        };

        if (modality == ThumbnailModality::HT) {
            if (meta->data.data2DMIP.exist) {
	            phyTimes = meta->data.data2DMIP.timePoints;
            }

            if (meta->data.data3D.exist) {
                if (phyTimes.empty()) {
                    phyTimes = meta->data.data3D.timePoints;
                } else {
                    AppendTimes(meta->data.data3D.timePoints);
                }
            }
        } else {
	        if (meta->data.data2DFLMIP.exist) {
		        for (auto channel : meta->data.data2DFLMIP.channelList) {
                    AppendTimes(meta->data.data2DFLMIP.timePoints[channel]);
		        }
	        }

            if (meta->data.data3DFL.exist) {
                for (auto channel : meta->data.data3DFL.channelList) {
                    AppendTimes(meta->data.data3DFL.timePoints[channel]);
		        }
            }
        }

        std::sort(phyTimes.begin(), phyTimes.end());

        return phyTimes;
    }

    ThumbnailWidget::ThumbnailWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

    	d->sceneManager = std::make_unique<SceneManager>();

    	d->renderWindow = std::make_unique<RenderWindow>(this);
        d->renderWindow->setSceneGraph(d->sceneManager->GetSceneGraph());
        d->renderWindow->setMinimumSize(200, 200);

        d->missingContainer = std::make_unique<QWidget>(this);
        d->missingLabel = std::make_unique<QLabel>();
        d->missingLabel->setAlignment(Qt::AlignCenter);
        d->missingLabel->setFont(QFont("Noto Sans KR", 40));
        const auto missingLayout = new QVBoxLayout;
        missingLayout->setContentsMargins(0, 0, 0, 0);
        missingLayout->setSpacing(0);
        missingLayout->addWidget(d->missingLabel.get());
        d->missingContainer->setLayout(missingLayout);
        d->missingContainer->setMinimumSize(200, 200);
        d->ui.imageLayout->insertWidget(0, d->renderWindow.get(), 1);
        d->ui.imageLayout->insertWidget(1, d->missingContainer.get(), 1);
        d->missingContainer->hide();                

        connect(d->ui.tcfComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
            if (index < 0)
                d->ClearTCFInfo();
            else
        		d->UpdateModalityList();
        });

        connect(d->ui.modalityComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
            if (index < 0)
            	d->ClearTCFModalityInfo();
            else
                d->UpdateTCFModalityInfo();
        });

        connect(d->ui.currentTimePointSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i) {
        	d->ui.timeLabel->clear();

        	if (i < 1) return;

			// update a slice index in renderer
            d->UpdateImage();

            // update time text
            const auto tcfInfo = d->tcfs.value(d->ui.tcfComboBox->currentText());
			const auto tcfMeta = tcfInfo.meta;
            if (tcfMeta == nullptr) return;

            const auto phyTimes = d->GetPhyTimes(
                tcfMeta,
                static_cast<ThumbnailModality>(d->ui.modalityComboBox->currentData().toInt())
            );

            if (phyTimes.isEmpty()) return;

            const auto time = static_cast<int>(phyTimes.at(i - 1));
            const auto timeCount = phyTimes.count();
			const auto timeStr = QString("%1:%2:%3").arg(time / 3600).arg((time % 3600) / 60, 2, 10, QChar('0')).arg((time % 3600) % 60, 2, 10, QChar('0'));

            d->ui.timeLabel->setText(QString("%1 (#%2/%3)").arg(timeStr).arg(i).arg(timeCount));
        });

        connect(d->ui.resetViewButton, &QPushButton::clicked, [=]() {
	        d->renderWindow->ResetView2D();
        });

        connect(d->ui.rangeMinSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i) {
	        d->UpdateDataRange();
        });

        connect(d->ui.rangeMaxSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i) {
	        d->UpdateDataRange();
        });

        setEnabled(false);
    }

    ThumbnailWidget::~ThumbnailWidget() {
    }

    auto ThumbnailWidget::SetTCFs(const QList<TCFInfo>& infos) -> void {
        d->ui.tcfComboBox->clear();
    	d->ClearTCFInfo();
        
        d->tcfs.clear();
        if(infos.count() < 1) {
            setEnabled(false);
            return;
        }

        setEnabled(true);
	    for (const auto& info : infos) {
			d->tcfs.insert(info.meta->common.title, info);
            d->ui.tcfComboBox->addItem(info.meta->common.title);
	    }
    }

}
