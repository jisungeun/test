﻿#include <QHeaderView>
#include <QMouseEvent>

#include <TCFMetaReader.h>

#include "TCFListWidget.h"

namespace TC::TimePointSelector {
    struct GroupInfo {
        QMap<Modality, int> timepoints;
        QMap<QString, TCFListItem> tcfs; // key: tcf id
    };

    struct TCFListWidget::Impl {
        QMap<int, GroupInfo> groups;   // key: group id

        Config config;

        bool isMousePressed{ false };
        bool isShiftPressed{ false };
        bool isControlPressed{ false };

        auto UpdateList(QTreeWidget* tree) -> void;
        auto GetColumnIndex(const Modality modality) -> int;
    };

	auto TCFListWidget::Impl::UpdateList(QTreeWidget* tree) -> void {
        QSignalBlocker blocker(tree);

		QTreeWidgetItem* selectedItem{nullptr};
        QString prevItemText;
        if (const auto currentTreeItem = tree->currentItem(); currentTreeItem) prevItemText = currentTreeItem->text(0);

        tree->clear();

        const auto GetTimeCountStr = [](int timeCount) -> QString {
	        if (timeCount > 0)
		        return QString::number(timeCount);

		    return "-";
        };

        const auto MakeGroupItem = [this, tree, GetTimeCountStr](int id, const GroupInfo& info) -> QTreeWidgetItem* {
            const auto setCount = tree->topLevelItemCount();

        	const auto groupItem = new QTreeWidgetItem(tree);
            groupItem->setData(0, Qt::UserRole, id);

            auto col = 0;
            if (id < 0) {
	            groupItem->setText(col++, "File set - Unavailable");
                groupItem->setFlags(Qt::NoItemFlags);
            } else {
	            groupItem->setText(col++, QString("File set %1").arg(setCount + 1));
            }
            
            groupItem->setText(GetColumnIndex(Modality::Ht2d), GetTimeCountStr(info.timepoints.value(Modality::Ht2d, 0)));
            groupItem->setText(GetColumnIndex(Modality::Ht3d), GetTimeCountStr(info.timepoints.value(Modality::Ht3d, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl2dCh1), GetTimeCountStr(info.timepoints.value(Modality::Fl2dCh1, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl2dCh2), GetTimeCountStr(info.timepoints.value(Modality::Fl2dCh2, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl2dCh3), GetTimeCountStr(info.timepoints.value(Modality::Fl2dCh3, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl3dCh1), GetTimeCountStr(info.timepoints.value(Modality::Fl3dCh1, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl3dCh2), GetTimeCountStr(info.timepoints.value(Modality::Fl3dCh2, 0)));
            groupItem->setText(GetColumnIndex(Modality::Fl3dCh3), GetTimeCountStr(info.timepoints.value(Modality::Fl3dCh3, 0)));
            groupItem->setText(GetColumnIndex(Modality::Bf), GetTimeCountStr(info.timepoints.value(Modality::Bf, 0)));

            auto tcfsIt = QMapIterator(info.tcfs);
            while (tcfsIt.hasNext()) {
	            tcfsIt.next();

                const auto tcf = tcfsIt.value();
                const auto tcfItem = new QTreeWidgetItem(groupItem, {tcf.displayName});
                if (id < 0)
                    tcfItem->setFlags(Qt::NoItemFlags);

                tcfItem->setToolTip(0, tcf.displayName);
                tcfItem->setData(0, Qt::UserRole, tcfsIt.key());
                if (!tcf.timepointCounts.isEmpty()) {
                	tcfItem->setIcon(0, QIcon(":/img/check"));

                    auto it = QMapIterator(tcf.timepointCounts);
                    while (it.hasNext()) {
                        it.next();

                        const auto tcfItemColumn = GetColumnIndex(it.key());
                        tcfItem->setText(tcfItemColumn, QString::number(it.value()));
                        tcfItem->setTextAlignment(tcfItemColumn, Qt::AlignCenter);
                    }
                }

                groupItem->addChild(tcfItem);
            }

            return groupItem;
	        
        };

        // check modalities and time-points
        auto groupIt = QMapIterator(groups);
        while (groupIt.hasNext()) {
	        groupIt.next();

            const auto groupId = groupIt.key();
            if (groupId < 0) continue;

            const auto info = groupIt.value();

            const auto groupItem = MakeGroupItem(groupId, info);
            for (auto i = 1; i < groupItem->columnCount(); i++)
                groupItem->setTextAlignment(i, Qt::AlignCenter);

            if (!prevItemText.isEmpty() && prevItemText == groupItem->text(0)) selectedItem = groupItem;

            tree->addTopLevelItem(groupItem);
            tree->expandItem(groupItem);
        }

        // if group list contains unavailable group(id is -1), it appends here.
        if (groups.find(-1) != groups.end()) {
            const auto unavailableGroup = MakeGroupItem(-1, groups[-1]);
            for (auto i = 1; i < unavailableGroup->columnCount(); i++)
                unavailableGroup->setTextAlignment(i, Qt::AlignCenter);

            tree->addTopLevelItem(unavailableGroup);
            tree->expandItem(unavailableGroup);
        }

        if (selectedItem) tree->setCurrentItem(selectedItem);
    }

    auto TCFListWidget::Impl::GetColumnIndex(const Modality modality) -> int {
		// TODO: refactoring

		if (modality == Modality::Ht2d) return 1;
		if (modality == Modality::Ht3d) return 2;
		if (modality == Modality::Fl2dCh1) return 3;
		if (modality == Modality::Fl2dCh2) return 4;
		if (modality == Modality::Fl2dCh3) return 5;
		if (modality == Modality::Fl3dCh1) return 6;
		if (modality == Modality::Fl3dCh2) return 7;
		if (modality == Modality::Fl3dCh3) return 8;
		if (modality == Modality::Bf) return 9;

		return -1;
    }

    TCFListWidget::TCFListWidget(QWidget* parent) : QTreeWidget(parent), d{std::make_unique<Impl>()} {
        setSelectionBehavior(QAbstractItemView::SelectRows);
        setSelectionMode(QAbstractItemView::ExtendedSelection);
        setEditTriggers(QAbstractItemView::NoEditTriggers);

        const auto treeHeader = header();
		treeHeader->setDefaultAlignment(Qt::AlignCenter);
        treeHeader->setStretchLastSection(false);
        treeHeader->setSectionResizeMode(QHeaderView::Fixed);
        treeHeader->setSectionResizeMode(0, QHeaderView::Stretch);

        const auto header = QStringList{"TCF", "HT2D", "HT3D", "FL2D\nCH1", "FL2D\nCH2", "FL2D\nCH3", "FL3D\nCH1", "FL3D\nCH2", "FL3D\nCH3", "BF"};
        setColumnCount(header.count());
        setHeaderLabels(header);
        
        for (auto col = 1; col < columnCount(); col++)
			setColumnWidth(col, 50);

        connect(this, &QTreeWidget::currentItemChanged, [=](QTreeWidgetItem* current, QTreeWidgetItem* previous) {
            if (current == nullptr) return;

            emit sigTCFSelected(current->data(0, Qt::UserRole).toString());
        });
    }

    TCFListWidget::~TCFListWidget() {
    }

    auto TCFListWidget::SetConfig(Config config) -> void {
	    d->config = config;

        for (auto col = 1; col < columnCount(); col++)
			hideColumn(col);

        const auto ShowColumn = [=](const QString& text) {
	        for (auto col = 1; col < columnCount(); col++) {
				if (headerItem()->text(col).contains(text))
	                showColumn(col);
	        }
        };

        const auto modalities = d->config.sources.keys();
        for (auto modality : modalities) {
            switch(modality) {
            case Modality::Ht2d:
                ShowColumn("HT2D");
                break;
			case Modality::Ht3d:
                ShowColumn("HT3D");
                break;
            case Modality::Fl2d:
                ShowColumn("FL2D");
                break;
            case Modality::Fl3d:
                ShowColumn("FL3D");
                break;
            case Modality::Bf:
                ShowColumn("BF");
                break;
            }
        }
    }

    auto TCFListWidget::Clear() -> void {
	    d->groups.clear();
        d->config = Config();

        clear();
    }

    auto TCFListWidget::AddGroup(int groupId, QMap<Modality, int> timepointCounts, const QMap<QString, TCFListItem>& tcfs) -> void {
        d->groups.insert(groupId, {timepointCounts, tcfs});

        d->UpdateList(this);
    }

    auto TCFListWidget::AddTCF(int groupId, const QString& tcfId, TCFListItem tcf) -> void {
	    if (d->groups.find(groupId) == d->groups.end()) return;

		auto info = d->groups[groupId];
        if (info.tcfs.contains(tcfId)) return;

        info.tcfs.insert(tcfId, tcf);
        d->groups.insert(groupId, info);

        d->UpdateList(this);
    }

    auto TCFListWidget::SetCurrentTCF(const QString& tcf) -> void {
        QSignalBlocker blocker(this);

		for (auto i = 0; i < topLevelItemCount(); i++) {
	        const auto item = topLevelItem(i);

        	for (auto childIndex = 0; childIndex < item->childCount(); childIndex++) {
        		const auto childItem = item->child(childIndex);
                if (tcf == childItem->data(0, Qt::UserRole)) {
	                setCurrentItem(childItem);
                    return;
                }
        	}
        }
    }

    auto TCFListWidget::SetTimePointsState(const QString& tcfId, const QMap<Modality, int>& timepointCounts) -> void {
	    auto groupIt = QMapIterator(d->groups);
        while (groupIt.hasNext()) {
            groupIt.next();

            auto groupInfo = groupIt.value();
            auto tcfs = groupInfo.tcfs;
            if (tcfs.find(tcfId) != tcfs.end()) {
                auto tcfItem = tcfs.value(tcfId);
                tcfItem.timepointCounts = timepointCounts;

                tcfs[tcfId] = tcfItem;
                groupInfo.tcfs = tcfs;
                d->groups[groupIt.key()] = groupInfo;

                d->UpdateList(this);

	            break;
            }
	    }
    }

    auto TCFListWidget::GetSelectedItems() const -> QStringList {
        QStringList result;

	    for (auto item : selectedItems())
		    result << item->data(0, Qt::UserRole).toString();

        result.sort();

        return result;
    }

    void TCFListWidget::mousePressEvent(QMouseEvent* event) {
        if (event->button() == Qt::LeftButton) {
            d->isMousePressed = true;

            const auto pressedItem = itemAt(event->pos());
            if (pressedItem == nullptr) return;

            const auto IsGroupItem = [=](QTreeWidgetItem* item) -> bool {
	            return indexOfTopLevelItem(item) >= 0;
            };

            if (IsGroupItem(pressedItem)) { // pressed a top level item (group item)
                const auto itemRect = visualItemRect(pressedItem);
	            const auto iconRect = QRect(
                    QPoint(0, itemRect.y()),
                    QSize(itemRect.x(), itemRect.height())
                );

	            if (iconRect.contains(event->pos())) {  // pressed a collapse/expand indicator
	                const auto index = indexAt(event->pos());
            		setExpanded(index, !isExpanded(index));
	            } else {
                    if (pressedItem->data(0, Qt::UserRole).toInt() < 0) return; // if unavailable group item pressed, ignore

		            clearSelection();

	                const auto childCount = pressedItem->childCount();
	                for (auto i = 0; i < childCount; i++)
	                    pressedItem->child(i)->setSelected(true);

	            	setCurrentItem(pressedItem->child(childCount - 1), 0, QItemSelectionModel::NoUpdate);
	            }
            } else { // pressed a child item (TCF item)
                if (pressedItem->parent()->data(0,Qt::UserRole).toInt() < 0) return;    // if tcf item in unavailable group pressed, ignore

                const auto currentItems = selectedItems();
                if (!currentItems.isEmpty()) {
		            const auto prevItem = currentItems.first();
                    const auto groupId = prevItem->parent()->data(0, Qt::UserRole);
                    if (groupId != pressedItem->parent()->data(0,Qt::UserRole)) {   // select other Set
	                    clearSelection();
                        setCurrentItem(pressedItem);
                    } else {
                        if (d->isShiftPressed) {
                            QSignalBlocker blocker(this);
                            QTreeWidget::mousePressEvent(event);
                            blocker.unblock();

                            setCurrentItem(pressedItem, 0, QItemSelectionModel::NoUpdate);
                            emit sigTCFSelected(pressedItem->data(0, Qt::UserRole).toString());
                        } else if (d->isControlPressed) {
                            const bool isSelectedState = pressedItem->isSelected();

                        	setCurrentItem(pressedItem, 0, QItemSelectionModel::Toggle | QItemSelectionModel::Rows);
                            if (isSelectedState) {
                                // change current item to first item on selected list
                                setCurrentItem(selectedItems().first(), 0, QItemSelectionModel::NoUpdate);
                            }
                        } else {
	                        setCurrentItem(pressedItem);
                        }
                    }
	            } else {
					setCurrentItem(pressedItem);
	            }
            }
        }
    }

    void TCFListWidget::mouseReleaseEvent(QMouseEvent* event) {
	    if (event->button() == Qt::LeftButton)
            d->isMousePressed = false;

        QTreeWidget::mouseReleaseEvent(event);
    }

    void TCFListWidget::mouseMoveEvent(QMouseEvent* event) {
	    if (d->isMousePressed)
		    return;

        QTreeWidget::mouseMoveEvent(event);
    }

    void TCFListWidget::keyPressEvent(QKeyEvent* event) {
        if (event->key() == Qt::Key_Shift)
            d->isShiftPressed = true;

        if (event->key() == Qt::Key_Control)
            d->isControlPressed = true;

	    QTreeWidget::keyPressEvent(event);
    }

    void TCFListWidget::keyReleaseEvent(QKeyEvent* event) {
        if (event->key() == Qt::Key_Shift)
            d->isShiftPressed = false;

        if (event->key() == Qt::Key_Control)
            d->isControlPressed = false;

	    QTreeWidget::keyReleaseEvent(event);
    }

}
