﻿#pragma once

#include <QWidget>

#include "TimePointSelectorDef.h"

namespace TC::TimePointSelector {
    class PointSettingWidget : public QWidget {
        Q_OBJECT
	public:
        explicit PointSettingWidget(Config::SelectionMode selectionMode, QWidget* parent = nullptr);
        ~PointSettingWidget() override;

        auto SetTimePoints(const QMap<Modality, TimePoints>& timepoints) -> void;
        auto SetFilteredTimePoints(const QMap<Modality, TimePoints>& timepoints) -> void;
        auto SetTimeIndexes(const QList<int>& indexes) -> void;

        auto GetSelectedTimeIndexes() const -> QList<int>;

    signals:
        void sigTimePointsValidityChanged(bool validity);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
