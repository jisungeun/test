﻿#pragma once

#include <QTreeWidget>

#include "TimePointSelectorDef.h"

namespace TC::TimePointSelector {
    struct TCFListItem {
        QString displayName;
        QMap<Modality, int> timepointCounts;
    };

    class TCFListWidget : public QTreeWidget {
        Q_OBJECT
	public:
        explicit TCFListWidget(QWidget* parent = nullptr);
        ~TCFListWidget() override;

    	auto SetConfig(Config config) -> void;

        auto Clear() -> void;

        auto AddGroup(int groupId, QMap<Modality, int> timepointCounts, const QMap<QString/*tcf id*/, TCFListItem>& tcfs) -> void;
        auto AddTCF(int groupId, const QString& tcfId, TCFListItem tcf) -> void;

    	auto SetCurrentTCF(const QString& tcf) -> void;
        auto SetTimePointsState(const QString& tcfId, const QMap<Modality, int>& timepointCount) -> void;

        auto GetSelectedItems() const -> QStringList;   // return TCF ID list

    protected:
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

        void keyPressEvent(QKeyEvent* event) override;
        void keyReleaseEvent(QKeyEvent* event) override;

    signals:
        void sigTCFSelected(const QString& tcfId);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
