﻿#pragma once

#include <QWidget>

#include "TimePointSelectorDef.h"

namespace TC::TimePointSelector {
    class ModalitySettingWidget : public QWidget {
        Q_OBJECT
	public:
        explicit ModalitySettingWidget(QWidget* parent = nullptr);
        ~ModalitySettingWidget() override;

        auto SetConfig(Config config) -> void;

        auto GetModalities() const -> ModalitySetting;

    signals:
        void sigModalityChanged(ModalitySetting setting);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
