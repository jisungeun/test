#include <SliceGeneral.h>
#include <Slice3Channel.h>

#include <OivThumbnailReader.h>
#include <OivHdf5Reader.h>
#include <OivXYReader.h>
#include <OivLdmReader.h>
#include <OivLdmReaderXY.h>
#include <OivLdmReaderFL.h>

#include "SceneManager.h"


using namespace Tomocube::Rendering::Image;


namespace TC::TimePointSelector {	
	struct SceneManager::Impl {
		SoRef<SoSwitch> sceneSwitch{ nullptr };
		std::shared_ptr<SliceGeneral> htScene{ nullptr };
		std::shared_ptr<Slice3Channel> flScene{ nullptr };

		QString tcfPath;
		IO::TCFMetaReader::Meta::Pointer meta{ nullptr };

		std::tuple<double, double> origHtDataRange{0, 0};
		QList<std::tuple<double, double>> origFlDataRange;

		// VolumeData
		SoRef<SoVolumeData> ht{ nullptr };
		QVector<SoRef<SoVolumeData>> fl = QVector<SoRef<SoVolumeData>>(3, nullptr);

		auto Init()->void;
		auto calcFLIndexFromHT(int idx)->int;
	};

	auto SceneManager::Impl::Init() -> void {
		sceneSwitch = new SoSwitch;
		
		htScene = std::make_shared<SliceGeneral>("ThumbnailHT");
		htScene->SetDataRange(0, 255);

		flScene = std::make_shared<Slice3Channel>("ThumbnailFL");
		flScene->SetColor(0, QColor(255, 0, 0));
		flScene->SetColor(1, QColor(0, 255, 0));
		flScene->SetColor(2, QColor(0, 0, 255));
		flScene->SetDataRange(0, 0, 255);

		sceneSwitch->addChild(htScene->GetRootSwitch());
		sceneSwitch->addChild(flScene->GetRootSwitch());

		ht = new SoVolumeData;
		for (auto& volume : fl)
			volume = new SoVolumeData;

		for (auto i = 0; i < 3; i++)
			origFlDataRange << std::make_tuple(0, 0);
	}

	auto SceneManager::Impl::calcFLIndexFromHT(int idx) -> int {
		const auto flRes = meta->data.data3DFL.resolutionZ;
		const auto offsetZ = meta->data.data3DFL.offsetZ - (meta->data.data3DFL.sizeZ * meta->data.data3DFL.resolutionZ / 2);

		auto phyZ = meta->data.data3D.resolutionZ * idx - offsetZ;
		auto idxFLz = static_cast<int>(phyZ / flRes);
		
		return idxFLz;
	}

	SceneManager::SceneManager() : d{ new Impl } {
		d->Init();
	}

	SceneManager::~SceneManager() {
	}

	auto SceneManager::Clear() -> void {		
		d->tcfPath = QString();
		d->meta = nullptr;
	}

	auto SceneManager::ClearScene() -> void {
		//d->htScene->Clear();
		//d->flScene->Clear();

		d->sceneSwitch->whichChild = SO_SWITCH_NONE;
	}
	
	auto SceneManager::ReadHT(int timestep) -> void {
		if (d->tcfPath.isEmpty()) return;
		if (d->meta == nullptr) return;

		d->ht = nullptr;

		if (d->meta->thumbnail.ht.exist) {	// use thumbnail data
			const SoRef<OivThumbnailReader> reader = new OivThumbnailReader;
			reader->SetTileIndex(timestep);
			reader->setFilename(d->tcfPath.toStdString());

			d->ht = new SoVolumeData;
			d->ht->setReader(*reader);
			reader->touch();

			d->htScene->SetVolume(d->ht.ptr());
			d->htScene->SetDataMinMax(0, 255);

			d->origHtDataRange = {0, 255};

		} else if (d->meta->data.data2DMIP.exist) {	// use MIP data
			const auto group = "/Data/2DMIP";
			const auto is8Bit = d->meta->data.data2DMIP.scalarType[timestep] == 1;
			const auto tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));

			if(d->meta->data.isLDM) {
				SoRef<OivLdmReader> reader = new OivLdmReader(true, is8Bit);
				reader->setTileDimension(d->meta->data.data2DMIP.tileSizeX, d->meta->data.data2DMIP.tileSizeY, 1);
				reader->setDataGroupPath(group);
				reader->setTileName(tileName.toStdString());
				reader->setFilename(d->tcfPath.toStdString());

				d->ht = new SoVolumeData;
				d->ht->setReader(*reader);
				reader->touch();

			} else {
				SoRef<OivHdf5Reader> reader = new OivHdf5Reader(false, is8Bit);
				reader->setDataGroupPath(group);
				reader->setTileName(tileName.toStdString());
				reader->setFilename(d->tcfPath.toStdString());

				d->ht = new SoVolumeData;
				d->ht->setReader(*reader);
				reader->touch();
			}

			const auto min = d->meta->data.data2DMIP.riMinList[timestep];
			const auto max = d->meta->data.data2DMIP.riMaxList[timestep];
			if (is8Bit) {
				const auto range = (max - min) * 1000;
				d->htScene->SetDataMinMax(0, range);
				d->origHtDataRange = {0, range};
			} else {
				d->htScene->SetDataMinMax(min * 10000.0, max * 10000.0);
				d->origHtDataRange = {min * 10000.0, max * 10000.0};
			}

			d->htScene->SetVolume(d->ht.ptr());

		} else if (d->meta->data.data3D.exist) {	// use 3D data
			const auto group = "/Data/3D";
			const auto is8Bit = d->meta->data.data2DMIP.scalarType[timestep] == 1;
			const auto tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));

			if (d->meta->data.isLDM) {
				SoRef<OivLdmReaderXY> reader = new OivLdmReaderXY;
				reader->setDataGroupPath(group);
				reader->set8Bit(is8Bit);
				reader->setTileName(tileName.toStdString());
				reader->setTileDimension(d->meta->data.data3D.tileSizeX, d->meta->data.data3D.tileSizeY, d->meta->data.data3D.tileSizeZ);
				reader->setFilename(d->tcfPath.toStdString());
				reader->setIndex(d->meta->data.data3D.sizeZ / 2);

				d->ht = new SoVolumeData;
				d->ht->setReader(*reader);
				reader->touch();
			} else {
				SoRef<OivXYReader> reader = new OivXYReader;
				reader->setDataGroupPath(group);
				reader->set8Bit(is8Bit);
				reader->setTileName(tileName.toStdString());
				reader->setFilename(d->tcfPath.toStdString());
				reader->setIndex(d->meta->data.data3D.sizeZ / 2);

				d->ht = new SoVolumeData;
				d->ht->setReader(*reader);
				reader->touch();
			}

			const auto min = d->meta->data.data3D.riMinList[timestep];
			const auto max = d->meta->data.data3D.riMaxList[timestep];
			if (is8Bit) {
				const auto range = (max - min) * 1000;
				d->htScene->SetDataMinMax(0, range);
				d->origHtDataRange = {0, range};
			} else {
				d->htScene->SetDataMinMax(min * 10000.0, max * 10000.0);
				d->origHtDataRange = {min * 10000.0, max * 10000.0};
			}

			d->htScene->SetVolume(d->ht.ptr());
		} else {
			
		}

		d->sceneSwitch->whichChild = d->ht.ptr() == nullptr ? SO_SWITCH_NONE : 0;
	}
	
	auto SceneManager::ReadFL(int timestep) -> void {
		if (d->tcfPath.isEmpty()) return;
		if (d->meta == nullptr) return;

		const auto tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));

		d->fl = QVector<SoRef<SoVolumeData>>(3, nullptr);

		d->origFlDataRange.clear();
		for (auto i = 0; i < 3; i++)
			d->origFlDataRange << std::make_tuple(0, 0);

		auto hasData = false;
		for (auto i = 0; i < d->fl.count(); i++) {
			if (d->meta->thumbnail.fl.exist) {	// use thumbnail data
				SoRef<OivThumbnailReader> reader = new OivThumbnailReader(true);
				reader->SetTileIndex(timestep);
				reader->SetColorChannel(i);
				reader->setFilename(d->tcfPath.toStdString());

				d->fl[i] = new SoVolumeData;
				d->fl[i]->setReader(*reader);
				reader->touch();

				d->flScene->SetDataMinMax(i, 0, 255);
				d->flScene->SetVolume(i, d->fl[i].ptr(), false);

				d->origFlDataRange[i] = {0, 255};
				hasData = true;
			} else if (d->meta->data.data2DFLMIP.exist) {	// use MIP data
				const auto channelList = QVector(d->meta->data.data2DFLMIP.channelList.begin(), d->meta->data.data2DFLMIP.channelList.end());
				if (!channelList.contains(i)) continue;

				const auto group = QString("/Data/2DFLMIP/CH%1").arg(i);
				const auto is8Bit = d->meta->data.data2DFLMIP.scalarType[i][timestep] == 1;

				if (d->meta->data.isLDM) {
					SoRef<OivLdmReaderFL> reader = new OivLdmReaderFL(i, true, is8Bit);			
					reader->setTileDimension(d->meta->data.data2DFLMIP.tileSizeX, d->meta->data.data2DFLMIP.tileSizeY, 1);
					reader->setDataGroupPath(group.toStdString());
					reader->setTileName(tileName.toStdString());
					reader->setFilename(d->tcfPath.toStdString());

					d->fl[i]->setReader(*reader);
					reader->touch();

				} else {
					SoRef<OivHdf5Reader> reader = new OivHdf5Reader(true, is8Bit);
					reader->setDataGroupPath(group.toStdString());
					reader->setTileName(tileName.toStdString());
					reader->setFilename(d->tcfPath.toStdString());

					d->fl[i] = new SoVolumeData;
					d->fl[i]->setReader(*reader);
					reader->touch();
				}

				const auto min = d->meta->data.data2DFLMIP.minIntensityList[i][timestep];
				const auto max = d->meta->data.data2DFLMIP.maxIntensityList[i][timestep];

				d->flScene->SetDataMinMax(i, min, max);
				d->flScene->SetVolume(i, d->fl[i].ptr(),false);

				d->origFlDataRange[i] = {min, max};
				hasData = true;
			} else if (d->meta->data.data3DFL.exist) {	// use 3D data
				const auto channelList = QVector(d->meta->data.data3DFL.channelList.begin(), d->meta->data.data3DFL.channelList.end());
				if (!channelList.contains(i)) continue;

				const auto group = QString("/Data/3DFL/CH%1").arg(i);
				const auto is8Bit = d->meta->data.data3DFL.scalarType[i][timestep] == 1;
				int flIdx = d->meta->data.data3DFL.sizeZ / 2;

				if (d->meta->data.isLDM) {
					SoRef<OivLdmReaderXY> reader = new OivLdmReaderXY(true);
					reader->setDataGroupPath(group.toStdString());
					reader->set8Bit(is8Bit);
					reader->setTileName(tileName.toStdString());
					reader->setTileDimension(d->meta->data.data3DFL.tileSizeX, d->meta->data.data3DFL.tileSizeY, d->meta->data.data3DFL.tileSizeZ);
					reader->setIndex(flIdx);
					reader->setFilename(d->tcfPath.toStdString());

					d->fl[i] = new SoVolumeData;
					d->fl[i]->setReader(*reader);
					reader->touch();

				} else {
					SoRef<OivXYReader> reader = new OivXYReader(true);
					reader->setDataGroupPath(group.toStdString());
					reader->set8Bit(is8Bit);
					reader->setTileName(tileName.toStdString());
					reader->setFilename(d->tcfPath.toStdString());			
					reader->setIndex(flIdx);

					d->fl[i] = new SoVolumeData;
					d->fl[i]->setReader(*reader);
					reader->touch();
				}

				const auto min = d->meta->data.data3DFL.minIntensityList[i][timestep];
				const auto max = d->meta->data.data3DFL.maxIntensityList[i][timestep];

				d->flScene->SetDataMinMax(i, min, max);
				d->flScene->SetVolume(i, d->fl[i].ptr(), false);		
				//d->flScene->ToggleViz(i, true);

				d->origFlDataRange[i] = {min, max};
				hasData = true;
			}
		}

		d->sceneSwitch->whichChild = hasData ? 1 : SO_SWITCH_NONE;
	}

	auto SceneManager::SetTCFPath(const QString& path) -> void {
		d->tcfPath = path;
	}

	auto SceneManager::GetTCFPath() const -> QString {
		return d->tcfPath;
	}

	auto SceneManager::SetMeta(IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
	}

	auto SceneManager::SetDataRange(int min, int max) -> void {
		// rescale 0 ~ 100 to current volume data range
		const auto rescale = [=](double newMin, double newMax, double value) -> double {
			const auto oldMin = 0;
			const auto oldMax = 100;

			return (newMax - newMin) / (oldMax - oldMin) * (value - oldMin) + newMin;
		};

		if (d->sceneSwitch->whichChild.getValue() == 0) {
			auto [newRangeMin, newRangeMax] = d->origHtDataRange;
			d->htScene->SetDataMinMax(rescale(newRangeMin, newRangeMax, min), rescale(newRangeMin, newRangeMax, max));
		} else {
			for (auto i = 0; i < d->origFlDataRange.count(); i++) {
				auto [newRangeMin, newRangeMax] = d->origFlDataRange[i];
				d->flScene->SetDataMinMax(i, rescale(newRangeMin, newRangeMax, min), rescale(newRangeMin, newRangeMax, max));
			}
		}
	}

	auto SceneManager::GetSceneGraph() -> SoSwitch* {
		return d->sceneSwitch.ptr();
	}
}