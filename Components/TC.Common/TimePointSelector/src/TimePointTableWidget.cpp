﻿#include <QDebug>

#include "TimePointTableWidget.h"

#include <QHeaderView>

namespace TC::TimePointSelector {
    struct TimePointTableWidget::Impl {
        TimePointTableWidget* self{nullptr};

        bool showFilteredTimes{ true };

        QMap<Modality, TimePoints> allTimes;
        QMap<Modality, TimePoints> filteredTimes;

        auto InitUI() -> void;
        auto Clear() -> void;
        auto UpdateTable() -> void;
    };

    auto TimePointTableWidget::Impl::InitUI() -> void {
        self->horizontalHeader()->hide();
        self->verticalHeader()->hide();
        self->setEditTriggers(QAbstractItemView::NoEditTriggers);

	    self->setRowCount(7);
        self->setColumnCount(1);

        // set vertical header
        self->setItem(0, 0, new QTableWidgetItem("Times"));
        self->setItem(2, 0, new QTableWidgetItem("HT"));
        self->setItem(3, 0, new QTableWidgetItem("FL Ch1"));
        self->setItem(4, 0, new QTableWidgetItem("FL Ch2"));
        self->setItem(5, 0, new QTableWidgetItem("FL Ch3"));
        self->setItem(6, 0, new QTableWidgetItem("BF"));

        self->setSpan(0, 0, 2, 1);

        for (auto row = 2; row < self->rowCount(); row++) {
	        self->setRowHeight(row, 40);
	        self->hideRow(row);
        }
    }

    auto TimePointTableWidget::Impl::Clear() -> void {
	    self->clear();
        InitUI();
    }

    auto TimePointTableWidget::Impl::UpdateTable() -> void {
	    Clear();

        // add a horizontal header, it has time index row and time row.
        // ---------------------------------------------------------
        // | Times |     0     |     1     |     2     |     3     |    Time index row
        // -       -------------------------------------------------
        // |       | 00:00:00  | 00:00:30  | 00:01:00  | 00:00:30  |    Time row
        // ---------------------------------------------------------

        const auto timepoints = showFilteredTimes ? filteredTimes : allTimes;

        const auto GetPhyTimes = [](const QMap<Modality, TimePoints>& times) -> TimePoints {
	        TimePoints result;
			for (auto& modalityTimepoints : times.values())
				for (auto time : modalityTimepoints)
					if(!result.contains(time)) result << time;

            std::sort(result.begin(), result.end());

            return result;
        };

        const auto allPhyTimes  = GetPhyTimes(allTimes);
        const auto displayPhyTimes = GetPhyTimes(timepoints);

        self->setColumnCount(self->columnCount() + displayPhyTimes.count());

        auto TimeToText = [](int64_t time) -> QString {
            return QString("%1:%2:%3")
        	.arg(time / 3600)
        	.arg((time % 3600) / 60, 2, 10, QChar('0'))
        	.arg((time % 3600) % 60, 2, 10, QChar('0'));
        };

        for (auto row = 2; row < self->rowCount(); row++)
            self->hideRow(row);

        // add cell items(2D, 3D)
        auto AddCellItem = [table = self](int row, int col, const QString& text) {
	        auto cellItem = table->item(row, col);
            if (cellItem && !cellItem->text().isEmpty()) {
                cellItem->setTextAlignment(Qt::AlignCenter);
                cellItem->setText(cellItem->text() + "\n" + text);
            } else {
                cellItem = new QTableWidgetItem(text);
                cellItem->setTextAlignment(Qt::AlignCenter);
                table->setItem(row, col, cellItem);
            }

            if (table->isRowHidden(row)) table->setRowHidden(row, false);
        };

        for (auto i = 0; i < displayPhyTimes.count(); i++) {
            const auto time = displayPhyTimes.at(i);
            const auto col = i + 1;

            const auto timeIndexItem = new QTableWidgetItem(QString::number(allPhyTimes.indexOf(time) + 1));
            timeIndexItem->setTextAlignment(Qt::AlignCenter);

            const auto timeTextItem = new QTableWidgetItem(TimeToText(time));
            timeTextItem->setTextAlignment(Qt::AlignCenter);

	        self->setItem(0, col, timeIndexItem);
            self->setItem(1, col, timeTextItem);

            auto it = QMapIterator(timepoints);
            while(it.hasNext()) {
	            it.next();

                if (!it.value().contains(time)) continue;

                switch(it.key()) {
                case Modality::Ht2d: AddCellItem(2, col, "2D"); break;
                case Modality::Ht3d: AddCellItem(2, col, "3D"); break;
                case Modality::Fl2dCh1: AddCellItem(3, col, "2D"); break;
                case Modality::Fl3dCh1: AddCellItem(3, col, "3D"); break;
                case Modality::Fl2dCh2: AddCellItem(4, col, "2D"); break;
                case Modality::Fl3dCh2: AddCellItem(4, col, "3D"); break;
                case Modality::Fl2dCh3: AddCellItem(5, col, "2D"); break;
                case Modality::Fl3dCh3: AddCellItem(5, col, "3D"); break;
                }
            }
        }
    }

    TimePointTableWidget::TimePointTableWidget(QWidget* parent) : QTableWidget(parent), d{std::make_unique<Impl>()} {
        d->self = this;

        d->InitUI();
    }

    TimePointTableWidget::~TimePointTableWidget() {
    }

    auto TimePointTableWidget::SetTimePoints(const QMap<Modality, TimePoints>& allTimepoints, const QMap<Modality, TimePoints>& timepoints) -> void {
        d->allTimes = allTimepoints;
        d->filteredTimes = timepoints;

        d->UpdateTable();
    }

    auto TimePointTableWidget::SetFiltering(bool filtered) -> void {
        if (d->showFilteredTimes == filtered) return;

    	d->showFilteredTimes = filtered;
        d->UpdateTable();
    }

}
