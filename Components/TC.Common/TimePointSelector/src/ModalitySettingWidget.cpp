﻿#include <QDebug>
#include <QComboBox>

#include "ModalitySettingWidget.h"
#include "ui_ModalitySettingWidget.h"

namespace TC::TimePointSelector {
    struct SourceContainer {
        Modality modality;
        QLabel* label;
        QComboBox* combobox;

        SourceContainer(Modality modality, QLabel* label, QComboBox* combobox) {
            this->modality = modality;
            this->label = label;
            this->combobox = combobox;
        }
    };

    struct ModalitySettingWidget::Impl {
        Ui::ModalitySettingWidget ui;

        Config config;

        QMap<QString, SourceContainer> sourcesContainers;

        auto InitUI() -> void;
        auto ClearModalities() -> void;
        auto CurrentModalities() -> ModalitySetting;
    };

    auto ModalitySettingWidget::Impl::InitUI() -> void {
  
    }

    auto ModalitySettingWidget::Impl::ClearModalities() -> void {
        qDeleteAll(ui.sourcesLayout->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
        sourcesContainers.clear();
    }

    auto ModalitySettingWidget::Impl::CurrentModalities() -> ModalitySetting {
        ModalitySetting settings;

        auto fl2d = QVector(3, Modality::None);
        auto fl3d = QVector(3, Modality::None);

        int fl2dIndex = 0;
        int fl3dIndex = 0;

        auto it = QMapIterator(sourcesContainers);
        while (it.hasNext()) {
            it.next();

            const auto container = it.value();
            switch (container.modality) {
            case Modality::Ht2d:
                settings.ht2d = true;
                break;
            case Modality::Ht3d:
                settings.ht3d = true;
                break;
            case Modality::Fl2d:
                if (fl2dIndex > fl2d.size() - 1) continue;

                settings.fl2d = true;
                fl2d[fl2dIndex++] = static_cast<Modality>(container.combobox->currentData().toInt());
                break;
            case Modality::Fl3d:
                if (fl3dIndex > fl3d.size() - 1) continue;

                settings.fl3d = true;
                fl3d[fl3dIndex++] = static_cast<Modality>(container.combobox->currentData().toInt());
                break;
            }
        }

        settings.fl2dChannelSocket = std::make_tuple(fl2d[0], fl2d[1], fl2d[2]);
        settings.fl3dChannelSocket = std::make_tuple(fl3d[0], fl3d[1], fl3d[2]);

        return settings;
    }
    
    ModalitySettingWidget::ModalitySettingWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

    	d->InitUI();
    }

    ModalitySettingWidget::~ModalitySettingWidget() {
    }

    auto ModalitySettingWidget::SetConfig(Config config) -> void {
        auto children = findChildren<QWidget*>();
        for (auto child : children) child->blockSignals(true);

        //d->ClearModalities();

    	qDeleteAll(d->ui.sourcesLayout->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
        d->sourcesContainers.clear();

        d->config = config;

        int sourceLayoutRow = 0;

        const auto MakeComboBox = []() -> QComboBox* {
            const auto combobox = new QComboBox;
            combobox->setFixedWidth(130);

            return combobox;
        };

        if (config.sources.find(Modality::Ht2d) != config.sources.end()) {
            const auto name = config.sources[Modality::Ht2d].first();
            const auto label = new QLabel(name + " (HT)");
            const auto combobox = MakeComboBox();
            combobox->addItem("HT");

            d->ui.sourcesLayout->addWidget(label, sourceLayoutRow, 0);
            d->ui.sourcesLayout->addWidget(combobox, sourceLayoutRow++, 1);

            d->sourcesContainers.insert(name, SourceContainer(Modality::Ht2d, label, combobox));
        }

        if (config.sources.find(Modality::Ht3d) != config.sources.end()) {
            const auto name = config.sources[Modality::Ht3d].first();
            const auto label = new QLabel(name + " (HT)");
            const auto combobox = MakeComboBox();
            combobox->addItem("HT");

            d->ui.sourcesLayout->addWidget(label, sourceLayoutRow, 0);
            d->ui.sourcesLayout->addWidget(combobox, sourceLayoutRow++, 1);

            d->sourcesContainers.insert(name, SourceContainer(Modality::Ht3d, label, combobox));
        }

        if (config.sources.find(Modality::Fl2d) != config.sources.end()) {
            int flIndex = 0;
            for (auto name : config.sources[Modality::Fl2d]) {
	            const auto label = new QLabel(name + " (FL)");
	            const auto combobox = MakeComboBox();
	            combobox->addItem("FL CH1", static_cast<int>(Modality::Fl2dCh1));
	            combobox->addItem("FL CH2", static_cast<int>(Modality::Fl2dCh2));
	            combobox->addItem("FL CH3", static_cast<int>(Modality::Fl2dCh3));

            	connect(combobox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) { emit sigModalityChanged(d->CurrentModalities()); });

                combobox->setCurrentIndex(flIndex++);

	            d->ui.sourcesLayout->addWidget(label, sourceLayoutRow, 0);
	            d->ui.sourcesLayout->addWidget(combobox, sourceLayoutRow++, 1);

	            d->sourcesContainers.insert(name, SourceContainer(Modality::Fl2d, label, combobox));
            }
        }

        if (config.sources.find(Modality::Fl3d) != config.sources.end()) {
            int flIndex = 0;
            for (auto name : config.sources[Modality::Fl3d]) {
	            const auto label = new QLabel(name + " (FL)");
	            const auto combobox = MakeComboBox();

	            combobox->addItem("FL CH1", static_cast<int>(Modality::Fl3dCh1));
	            combobox->addItem("FL CH2", static_cast<int>(Modality::Fl3dCh2));
	            combobox->addItem("FL CH3", static_cast<int>(Modality::Fl3dCh3));

                connect(combobox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) { emit sigModalityChanged(d->CurrentModalities()); });

                combobox->setCurrentIndex(flIndex++);

	            d->ui.sourcesLayout->addWidget(label, sourceLayoutRow, 0);
	            d->ui.sourcesLayout->addWidget(combobox, sourceLayoutRow++, 1);

	            d->sourcesContainers.insert(name, SourceContainer(Modality::Fl3d, label, combobox));
            }
        }

        d->ui.sourcesLayout->setColumnStretch(0, 1);

        for (auto child : children) child->blockSignals(false);
    }

    auto ModalitySettingWidget::GetModalities() const -> ModalitySetting {
	    return d->CurrentModalities();
    }
}
