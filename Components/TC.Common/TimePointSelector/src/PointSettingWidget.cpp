﻿#include <QDebug>
#include <QComboBox>

#include "PointSettingWidget.h"
#include "ui_PointSettingWidget.h"

namespace TC::TimePointSelector {
    struct PointSettingWidget::Impl {
        Ui::PointSettingWidget ui;

        TimePoints entireTimes;
        TimePoints times;

        auto InitUI(Config::SelectionMode mode) -> void;
        auto SetTimePointsUIEnable(bool enable) -> void;
        auto ClearTimePoints() -> void;
        auto GetTimeIndexes() -> QList<int>;
        auto ShowErrorMessage(const QString& message) -> void;
    };

    auto PointSettingWidget::Impl::InitUI(Config::SelectionMode mode) -> void {
        ui.intervalSpinBox->setRange(1, 9999);
        ui.intervalSpinBox->setValue(1);

        if (mode == Config::SelectionMode::Single) {
            ui.startSpinBox->setReadOnly(true);
            ui.startSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);

            ui.endSpinBox->setReadOnly(true);
            ui.endSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);

            ui.intervalLabel->hide();
            ui.intervalSpinBox->hide();
            ui.applyButton->hide();
            ui.totalTimeStepsLabel->hide();

	        QRegExp regexp("^[1-9]\\d*$");
			ui.timeIndexLineEdit->setValidator(new QRegExpValidator(regexp));
        } else {
	        QRegExp regexp("^(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*))(, *(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*)))*$");
			ui.timeIndexLineEdit->setValidator(new QRegExpValidator(regexp));
        }

        ui.timeInputLayout->setAlignment(ui.startSpinBox, Qt::AlignCenter);
        ui.timeInputLayout->setAlignment(ui.endSpinBox, Qt::AlignCenter);
        ui.timeInputLayout->setAlignment(ui.intervalSpinBox, Qt::AlignCenter);
    }

    auto PointSettingWidget::Impl::SetTimePointsUIEnable(bool enable) -> void {
	    ui.startSpinBox->setEnabled(enable);
        ui.endSpinBox->setEnabled(enable);
        ui.intervalSpinBox->setEnabled(enable);
        ui.applyButton->setEnabled(enable);
        ui.timeIndexLineEdit->setEnabled(enable);

        if (!enable) ui.timeIndexLineEdit->clear();
    }
    
    auto PointSettingWidget::Impl::ClearTimePoints() -> void {
	    ui.startSpinBox->setRange(0, 0);
        ui.startSpinBox->setValue(0);
        ui.endSpinBox->setRange(0, 0);
        ui.endSpinBox->setValue(0);
        ui.intervalSpinBox->setValue(1);

        ui.timeIndexLineEdit->clear();
        ui.phyTimesLineEdit->clear();
    }

    auto PointSettingWidget::Impl::GetTimeIndexes() -> QList<int> {
	    if(ui.timeIndexLineEdit->text().isEmpty()) return {};

        auto split = ui.timeIndexLineEdit->text().split(",");
		auto prev = -1;

		QList<int> timeIndexes;
		auto maximum = ui.endSpinBox->maximum();
				
		for (const auto& timestr : split) {
			if (timestr.contains("-")) {//range
				auto begin = timestr.split("-")[0].toInt();
				auto end = timestr.split("-")[1].toInt();

                // check input order
				//if (begin <= prev) {
    //                ShowErrorMessage(tr("Invalid time sequence information."));
    //                return {};
				//}

				if (begin > end) {
					ShowErrorMessage(tr("Invalid time sequence information."));
                    return {};
				}

				if (end > maximum) {
					ShowErrorMessage(QString(tr("Out of range.\n (put numbers less than image time points %1)").arg(maximum)));
                    return {};
				}

				for (auto i = begin; i <= end; i++) {					
					if (!timeIndexes.contains(i)) timeIndexes.push_back(i);
				}

				prev = end;
			} else {
                if (timestr.isEmpty()) continue;

				auto timePoint = timestr.toInt();

				// check input order
				//if (timePoint <= prev) {
				//	ShowErrorMessage(tr("Invalid time sequence information."));
    //                return {};
				//}

				if (timePoint > maximum) {
					ShowErrorMessage(QString(tr("Out of range.\n (put numbers less than image time points %1)").arg(maximum)));
                    return {};
				}

			    if (!timeIndexes.contains(timePoint)) timeIndexes.push_back(timePoint);
			    prev = timePoint;				
			}
		}

        std::sort(timeIndexes.begin(), timeIndexes.end());

        return timeIndexes;
    }

    auto PointSettingWidget::Impl::ShowErrorMessage(const QString& message) -> void {
        // TODO: move error message label to TimePointSettingDialog
	    ui.warningMessageLabel->setText(message);
        ui.warningMessageLabel->show();
    }

    PointSettingWidget::PointSettingWidget(Config::SelectionMode selectionMode, QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

    	d->InitUI(selectionMode);
        setEnabled(false);

        connect(d->ui.applyButton, &QPushButton::clicked, [=]() {
	        const auto start = d->ui.startSpinBox->value();
	        const auto end = d->ui.endSpinBox->value();
	        const auto interval = d->ui.intervalSpinBox->value();

            // TODO: display error message
            if (start > end) return;
            if (start < d->times.first() + 1) return;
            if (end > d->times.last() + 1) return;
            if (interval < 1) return;

            QString timePoints;
            auto index = start;
            while (index <= end) {
                if (!timePoints.isEmpty()) timePoints += ",";
	            timePoints += QString::number(index);
                index += interval;
            }

            d->ui.timeIndexLineEdit->setText(timePoints);
        });

        connect(d->ui.timeIndexLineEdit, &QLineEdit::textChanged, [=](const QString& text) {
            auto TimeToText = [](int64_t time) -> QString {
	            return QString("%1:%2:%3")
        		.arg(time / 3600)
        		.arg((time % 3600) / 60, 2, 10, QChar('0'))
        		.arg((time % 3600) % 60, 2, 10, QChar('0'));
	        };

        	d->ui.totalTimeStepsLabel->setText(QString("Total: 0"));
        	d->ui.phyTimesLineEdit->clear();
        	d->ui.warningMessageLabel->clear();

            QString phyTimes;
            const auto timeIndexes = d->GetTimeIndexes();
            if (timeIndexes.isEmpty()) {
                emit sigTimePointsValidityChanged(false);
                return;
            }

            QList<int> invalidTimeIndexes;
            for (auto timeIndex : timeIndexes) {
                const auto time = d->entireTimes.value(timeIndex - 1, -1);
                if (time < 0) {
                	d->ShowErrorMessage(QString(tr("Out of range.")));
                    emit sigTimePointsValidityChanged(false);
                    return;
                }

                if (!d->times.contains(time)) {
                    invalidTimeIndexes << timeIndex;
	                continue;
                }

                if (!phyTimes.isEmpty()) phyTimes += ",";
    			phyTimes += TimeToText(time);
            }

            if (invalidTimeIndexes.isEmpty()) {
		        d->ui.totalTimeStepsLabel->setText(QString("Total: %1").arg(timeIndexes.count()));
        		d->ui.phyTimesLineEdit->setText(phyTimes);

            	emit sigTimePointsValidityChanged(true);
            } else {
                QString indexesString;
                for (auto index : invalidTimeIndexes) {
	                if (!indexesString.isEmpty()) indexesString += ", ";
    				indexesString += QString::number(index);
                }

                d->ShowErrorMessage(tr("Contains invalid time index. - %1").arg(indexesString));
                emit sigTimePointsValidityChanged(false);
            }
            
        });
    }

    PointSettingWidget::~PointSettingWidget() {
    }
    
    auto PointSettingWidget::SetTimePoints(const QMap<Modality, TimePoints>& timepoints) -> void {
        d->ClearTimePoints();
    	d->entireTimes.clear();

        auto it = QMapIterator(timepoints);
        while (it.hasNext()) {
	        it.next();
            for (auto time : it.value())
	            if (!d->entireTimes.contains(time)) d->entireTimes << time;
        }

        std::sort(d->entireTimes.begin(), d->entireTimes.end());

        const auto lastTimeIndex = d->entireTimes.count();
        d->ui.startSpinBox->setRange(1, lastTimeIndex);
        d->ui.startSpinBox->setValue(1);

        d->ui.endSpinBox->setRange(1, lastTimeIndex);
        d->ui.endSpinBox->setValue(lastTimeIndex);
    }

    auto PointSettingWidget::SetFilteredTimePoints(const QMap<Modality, TimePoints>& timepoints) -> void {
    	d->times.clear();

        auto it = QMapIterator(timepoints);
        while (it.hasNext()) {
	        it.next();
            for (auto time : it.value())
	            if (!d->times.contains(time)) d->times << time;
        }

        if (d->times.isEmpty()) {
	        setEnabled(false);
            return;
        }

        setEnabled(true);

	    std::sort(d->times.begin(), d->times.end());

        if (d->times.count() == 1) {
            const auto index = d->entireTimes.indexOf(d->times.first());
	        d->ui.timeIndexLineEdit->setText(QString::number(index + 1));
        }
    }

    auto PointSettingWidget::SetTimeIndexes(const QList<int>& indexes) -> void {
    	QString points;
        QString phyTimes;

        auto TimeToText = [](int64_t time) -> QString {
            return QString("%1:%2:%3")
        	.arg(time / 3600)
        	.arg((time % 3600) / 60, 2, 10, QChar('0'))
        	.arg((time % 3600) % 60, 2, 10, QChar('0'));
        };

    	for (auto index : indexes) {
    		if (!points.isEmpty()) points += ",";
            points += QString::number(index);

    		auto time = d->entireTimes.at(index-1);
            if (!phyTimes.isEmpty()) phyTimes += ",";
    		phyTimes += TimeToText(time);
    	}

        QSignalBlocker timeStepBlocker(d->ui.timeIndexLineEdit);
        QSignalBlocker timePointBlocker(d->ui.phyTimesLineEdit);
        d->ui.timeIndexLineEdit->setText(points);
	    d->ui.phyTimesLineEdit->setText(phyTimes);

        d->ui.warningMessageLabel->clear();
    }

    auto PointSettingWidget::GetSelectedTimeIndexes() const -> QList<int> {
        return d->GetTimeIndexes();
    }

}
