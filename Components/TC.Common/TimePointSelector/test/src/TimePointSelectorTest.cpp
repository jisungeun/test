#include <QApplication>
#include <QFile>
#include <QDebug>

#include <OivThumbnailReader.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>
#include <OivXYReader.h>
#include <OivLdmReaderXY.h>
#include <OivLdmReaderFL.h>

#include <VolumeViz/nodes/SoVolumeRendering.h>

#include <TimePointSettingDialog.h>

using namespace TC::TimePointSelector;

int main(int argc,char** argv) {
    QApplication app(argc, argv);

    SoDB::init();
    SoVolumeRendering::init();

    OivThumbnailReader::initClass();
	OivHdf5Reader::initClass();
	OivLdmReader::initClass();
	OivXYReader::initClass();
	OivLdmReaderXY::initClass();
	OivLdmReaderFL::initClass();

    Config config;
    config.selectionMode = Config::SelectionMode::Multi;
    config.sources = {
        { Modality::Ht2d, {"HT 2d"}},
        { Modality::Fl2d, {"FL 2d CH1", "FL 2d CH2"}}
    };

	TimePointSettingDialog tps(config);
    tps.SetTCFs({
		// TODO: add your TCF paths
    });
    
    //tps.exec();
    tps.show();

    auto result = tps.GetResult();
    auto it = QMapIterator(result);
    while (it.hasNext()) {
	    it.next();

        auto tcf = it.key();
        auto output = it.value();

        qDebug() << "==========================================================================";
        qDebug() << tcf;
        qDebug() << "  - Time points:" << output.times;
        qDebug() << "  - Real times :" << output.realTimes;

        //QList<int> points;
        //for (auto modal : output.modalties) points << static_cast<int>(modal);
        //qDebug() << "  - Modalities :" << points;
    }

    app.exec();

	OivLdmReaderFL::exitClass();
	OivLdmReaderXY::exitClass();
	OivXYReader::exitClass();
	OivLdmReader::exitClass();
	OivHdf5Reader::exitClass();
	OivThumbnailReader::exitClass();

	SoVolumeRendering::finish();
    //SoQt::finish();
    SoDB::finish();

    return EXIT_SUCCESS;
}