#include <catch2/catch.hpp>

#include "CompareArray.h"
#include "BinaryFileIO.h"

namespace BinaryFileIOTest {
    const std::string testFolderPath = std::string{ TEST_DATA_FOLDR_PATH } + "/TestUtilityTest";
    const std::string dataFolder = testFolderPath + "/BinaryData";

    constexpr auto numberOfDataElements = 10;
    //raw data : 0,1,2,3,4,5,6,7,8,9

    TEST_CASE("BinaryFileIO : unit test") {
        SECTION("ReadFile_bool()") {
            const std::string dataFilePath = dataFolder + "/boolArray";
            const auto resultData = ReadFile_bool(dataFilePath, numberOfDataElements);
            constexpr bool answerData[numberOfDataElements] = 
                { false, true, true, true, true, true, true, true, true, true };

            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements) == true);
        }
        SECTION("ReadFile_int32_t()") {
            const std::string dataFilePath = dataFolder + "/int32Array";
            const auto resultData = ReadFile_int32_t(dataFilePath, numberOfDataElements);
            constexpr int32_t answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements) == true);
        }
        SECTION("ReadFile_char()") {
            const std::string dataFilePath = dataFolder + "/charArray";
            const auto resultData = ReadFile_char(dataFilePath, numberOfDataElements);
            constexpr char answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements) == true);
        }
        SECTION("ReadFile_uint8_t()") {
            const std::string dataFilePath = dataFolder + "/uint8Array";
            const auto resultData = ReadFile_uint8_t(dataFilePath, numberOfDataElements);
            constexpr uint8_t answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements) == true);
        }
        SECTION("ReadFile_uint16_t()") {
            const std::string dataFilePath = dataFolder + "/uint16Array";
            const auto resultData = ReadFile_uint16_t(dataFilePath, numberOfDataElements);
            constexpr uint16_t answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements) == true);
        }
        SECTION("ReadFile_float()") {
            const std::string dataFilePath = dataFolder + "/floatArray";
            const auto resultData = ReadFile_float(dataFilePath, numberOfDataElements);
            constexpr float answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            constexpr size_t decimalPointCount = 6;
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements, decimalPointCount) == true);
        }
        SECTION("ReadFile_double()") {
            const std::string dataFilePath = dataFolder + "/doubleArray";
            const auto resultData = ReadFile_double(dataFilePath, numberOfDataElements);
            constexpr double answerData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            constexpr size_t decimalPointCount = 12;
            CHECK(CompareArray(resultData.get(), answerData, numberOfDataElements, decimalPointCount) == true);
        }

        SECTION("WriteFile(bool)") {
            const std::string dataFilePath = "boolArray";
            const std::shared_ptr<bool[]> data{ new bool[numberOfDataElements]() };
            constexpr bool rawData[numberOfDataElements] =
            { false, true, true, true, true, true, true, true, true, true };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_bool(dataFilePath, numberOfDataElements);
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements) == true);
        }
        SECTION("WriteFile(int32_t)") {
            const std::string dataFilePath = "int32Array";
            const std::shared_ptr<int32_t[]> data{ new int32_t[numberOfDataElements]() };
            constexpr int32_t rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_int32_t(dataFilePath, numberOfDataElements);
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements) == true);
        }
        SECTION("WriteFile(char)") {
            const std::string dataFilePath = "charArray";
            const std::shared_ptr<char[]> data{ new char[numberOfDataElements]() };
            constexpr char rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_char(dataFilePath, numberOfDataElements);
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements) == true);
        }
        SECTION("WriteFile(uint8_t)") {
            const std::string dataFilePath = "uint8Array";
            const std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfDataElements]() };
            constexpr uint8_t rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_uint8_t(dataFilePath, numberOfDataElements);
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements) == true);
        }
        SECTION("WriteFile(uint16_t)") {
            const std::string dataFilePath = "uint16Array";
            const std::shared_ptr<uint16_t[]> data{ new uint16_t[numberOfDataElements]() };
            constexpr uint16_t rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_uint16_t(dataFilePath, numberOfDataElements);
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements) == true);
        }
        SECTION("WriteFile(float)") {
            const std::string dataFilePath = "floatArray";
            const std::shared_ptr<float[]> data{ new float[numberOfDataElements]() };
            constexpr float rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_float(dataFilePath, numberOfDataElements);
            constexpr size_t decimalPointCount = 6;
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements, decimalPointCount) == true);
        }
        SECTION("WriteFile(double)") {
            const std::string dataFilePath = "doubleArray";
            const std::shared_ptr<double[]> data{ new double[numberOfDataElements]() };
            constexpr double rawData[numberOfDataElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfDataElements, data.get());

            CHECK(WriteFile(dataFilePath, data, numberOfDataElements) == true);

            const auto resultData = ReadFile_double(dataFilePath, numberOfDataElements);
            constexpr size_t decimalPointCount = 12;
            CHECK(CompareArray(resultData.get(), rawData, numberOfDataElements, decimalPointCount) == true);
        }
    }
}
