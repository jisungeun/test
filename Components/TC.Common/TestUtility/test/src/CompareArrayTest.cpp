#include <catch2/catch.hpp>

#include "CompareArray.h"

namespace CompareArrayTest {
    TEST_CASE("CompareArray : unit test") {
        SECTION("CompareArray(bool, bool)") {
            SECTION("same array") {
                constexpr size_t numberOfElements = 2;
                constexpr bool result[numberOfElements] = { true,true };
                constexpr bool answer[numberOfElements] = { true,true };

                CHECK(CompareArray(result, answer, numberOfElements) == true);
            }
            SECTION("diff array") {
                constexpr size_t numberOfElements = 2;
                constexpr bool result[numberOfElements] = { false,false };
                constexpr bool answer[numberOfElements] = { false,true };

                CHECK(CompareArray(result, answer, numberOfElements) == false);
            }
        }
        SECTION("CompareArray(int32_t, int32_t)") {
            SECTION("same array") {
                constexpr size_t numberOfElements = 2;
                constexpr int32_t result[numberOfElements] = { 1,2 };
                constexpr int32_t answer[numberOfElements] = { 1,2 };

                CHECK(CompareArray(result, answer, numberOfElements) == true);
            }
            SECTION("diff array") {
                constexpr size_t numberOfElements = 2;
                constexpr int32_t result[numberOfElements] = { 1,2 };
                constexpr int32_t answer[numberOfElements] = { 1,3 };

                CHECK(CompareArray(result, answer, numberOfElements) == false);
            }
        }

        SECTION("CompareArray(char, char)") {
            SECTION("same array") {
                constexpr size_t numberOfElements = 2;
                constexpr char result[numberOfElements] = { 1,2 };
                constexpr char answer[numberOfElements] = { 1,2 };

                CHECK(CompareArray(result, answer, numberOfElements) == true);
            }
            SECTION("diff array") {
                constexpr size_t numberOfElements = 2;
                constexpr char result[numberOfElements] = { 1,2 };
                constexpr char answer[numberOfElements] = { 1,3 };

                CHECK(CompareArray(result, answer, numberOfElements) == false);
            }
        }

        SECTION("CompareArray(uint8_t, uint8_t)") {
            SECTION("same array") {
                constexpr size_t numberOfElements = 2;
                constexpr uint8_t result[numberOfElements] = { 1,2 };
                constexpr uint8_t answer[numberOfElements] = { 1,2 };

                CHECK(CompareArray(result, answer, numberOfElements) == true);
            }
            SECTION("diff array") {
                constexpr size_t numberOfElements = 2;
                constexpr uint8_t result[numberOfElements] = { 1,2 };
                constexpr uint8_t answer[numberOfElements] = { 1,3 };

                CHECK(CompareArray(result, answer, numberOfElements) == false);
            }
        }
        SECTION("CompareArray(uint16_t, uint16_t)") {
            SECTION("same array") {
                constexpr size_t numberOfElements = 2;
                constexpr uint16_t result[numberOfElements] = { 1,2 };
                constexpr uint16_t answer[numberOfElements] = { 1,2 };

                CHECK(CompareArray(result, answer, numberOfElements) == true);
            }
            SECTION("diff array") {
                constexpr size_t numberOfElements = 2;
                constexpr uint16_t result[numberOfElements] = { 1,2 };
                constexpr uint16_t answer[numberOfElements] = { 1,3 };

                CHECK(CompareArray(result, answer, numberOfElements) == false);
            }
        }

        SECTION("CompareArray(float, float)") {
            SECTION("same array") {
                SECTION("IN-BOUNDED Decimal point count") {
                    constexpr size_t numberOfElements = 2;
                    constexpr float result[numberOfElements] = { 1000000.f,2000000.f };
                    constexpr float answer[numberOfElements] = { 1000001.f,2000001.f };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == true);
                }
                SECTION("values are 0") {
                    constexpr size_t numberOfElements = 2;
                    constexpr float result[numberOfElements] = { 0.f,0.f };
                    constexpr float answer[numberOfElements] = { 0.f,0.f };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == true);
                }
            }
            SECTION("diff array") {
                SECTION("OUT-BOUNDED Decimal Point Count ") {
                    constexpr size_t numberOfElements = 2;
                    constexpr float result[numberOfElements] = { 100000.f,200000.f };
                    constexpr float answer[numberOfElements] = { 100000.f,200001.f };
                    constexpr size_t decimalPointCount = 6;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == false);
                }
                SECTION("array contains 0") {
                    constexpr size_t numberOfElements = 2;
                    constexpr float result[numberOfElements] = { 0.f,0.1f };
                    constexpr float answer[numberOfElements] = { 0.1f,0.f };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == false);
                }
            }
        }

        SECTION("CompareArray(double, double)") {
            SECTION("same array") {
                SECTION("IN-BOUNDED Decimal point count") {
                    constexpr size_t numberOfElements = 2;
                    constexpr double result[numberOfElements] = { 1000000,2000000 };
                    constexpr double answer[numberOfElements] = { 1000001,2000001 };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == true);
                }
                SECTION("values are 0") {
                    constexpr size_t numberOfElements = 2;
                    constexpr double result[numberOfElements] = { 0,0 };
                    constexpr double answer[numberOfElements] = { 0,0 };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == true);
                }
            }
            SECTION("diff array") {
                SECTION("OUT-BOUNDED Decimal Point Count ") {
                    constexpr size_t numberOfElements = 2;
                    constexpr double result[numberOfElements] = { 100000000000,200000000000 };
                    constexpr double answer[numberOfElements] = { 100000000001,300000000001 };
                    constexpr size_t decimalPointCount = 12;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == false);
                }
                SECTION("array contains 0") {
                    constexpr size_t numberOfElements = 2;
                    constexpr double result[numberOfElements] = { 0, 0.1 };
                    constexpr double answer[numberOfElements] = { 0.1, 0 };
                    constexpr size_t decimalPointCount = 4;

                    CHECK(CompareArray(result, answer, numberOfElements, decimalPointCount) == false);
                }
            }
        }
    }
}