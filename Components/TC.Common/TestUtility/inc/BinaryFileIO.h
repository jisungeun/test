#pragma once

#include <string>
#include <memory>
#include "TCTestUtilityExport.h"

auto TCTestUtility_API ReadFile_bool(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<bool[]>;
auto TCTestUtility_API ReadFile_int32_t(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<int32_t[]>;
auto TCTestUtility_API ReadFile_char(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<char[]>;
auto TCTestUtility_API ReadFile_uint8_t(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<uint8_t[]>;
auto TCTestUtility_API ReadFile_uint16_t(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<uint16_t[]>;
auto TCTestUtility_API ReadFile_float(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<float[]>;
auto TCTestUtility_API ReadFile_double(const std::string& filePath, const size_t& numberOfElements)->std::shared_ptr<double[]>;

auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<bool[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<int32_t[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<char[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<uint8_t[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<uint16_t[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<float[]>& data, const size_t& numberOfElements)->bool;
auto TCTestUtility_API WriteFile(const std::string& filePath, const std::shared_ptr<double[]>& data, const size_t& numberOfElements)->bool;