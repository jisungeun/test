#pragma once
#include "TCTestUtilityExport.h"
#include <cstdint>

auto TCTestUtility_API CompareArray(const bool* resultArray, const bool* answerArray, const size_t& numberOfElements)->bool;
auto TCTestUtility_API CompareArray(const int32_t* resultArray, const int32_t* answerArray, const size_t& numberOfElements)->bool;
auto TCTestUtility_API CompareArray(const char* resultArray, const char* answerArray, const size_t& numberOfElements)->bool;
auto TCTestUtility_API CompareArray(const uint8_t* resultArray, const uint8_t* answerArray, const size_t& numberOfElements)->bool;
auto TCTestUtility_API CompareArray(const uint16_t* resultArray, const uint16_t* answerArray, const size_t& numberOfElements)->bool;

auto TCTestUtility_API CompareArray(const float* resultArray, const float* answerArray, 
    const size_t& numberOfElements, const size_t& decimalPointCount = 6)->bool;
auto TCTestUtility_API CompareArray(const double* resultArray, const double* answerArray,
    const size_t& numberOfElements, const size_t& decimalPointCount = 15)->bool;
