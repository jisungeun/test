#include "BinaryFileIO.h"

#include <fstream>

auto ReadFile(const std::string& filePath, char* readData, const size_t& byteCount)->void {
    std::ifstream fileStream(filePath, std::ios::binary);
    if (!fileStream.is_open()) {
        return;
    }
    fileStream.read(readData, byteCount);
    fileStream.close();
}

auto WriteFile(const std::string& filePath, const char* writeData, const size_t& byteCount)->bool {
    std::ofstream fileStream(filePath, std::ios::binary);
    if(!fileStream.is_open()) {
        return false;
    }
    fileStream.write(writeData, byteCount);
    fileStream.close();
    return true;
}

auto ReadFile_bool(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<bool[]> {
    std::shared_ptr<bool[]> readData{ new bool[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(bool);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto ReadFile_int32_t(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<int32_t[]> {
    std::shared_ptr<int32_t[]> readData{ new int32_t[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(int32_t);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto ReadFile_char(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<char[]> {
    std::shared_ptr<char[]> readData{ new char[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(char);
    ReadFile(filePath, readData.get(), byteCount);
    return readData;
}

auto ReadFile_uint8_t(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<uint8_t[]> {
    std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(uint8_t);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto ReadFile_uint16_t(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<uint16_t[]> {
    std::shared_ptr<uint16_t[]> readData{ new uint16_t[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(uint16_t);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto ReadFile_float(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<float[]> {
    std::shared_ptr<float[]> readData{ new float[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(float);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto ReadFile_double(const std::string& filePath, const size_t& numberOfElements) -> std::shared_ptr<double[]> {
    std::shared_ptr<double[]> readData{ new double[numberOfElements]() };
    const size_t byteCount = numberOfElements * sizeof(double);
    ReadFile(filePath, reinterpret_cast<char*>(readData.get()), byteCount);
    return readData;
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<bool[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(bool);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<int32_t[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(int32_t);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<char[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(char);
    return WriteFile(filePath, data.get(), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<uint8_t[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(uint8_t);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<uint16_t[]>& data,
    const size_t& numberOfElements) -> bool {
    const size_t byteCount = numberOfElements * sizeof(uint16_t);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<float[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(float);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}

auto WriteFile(const std::string& filePath, const std::shared_ptr<double[]>& data, const size_t& numberOfElements)
    -> bool {
    const size_t byteCount = numberOfElements * sizeof(double);
    return WriteFile(filePath, reinterpret_cast<const char*>(data.get()), byteCount);
}
