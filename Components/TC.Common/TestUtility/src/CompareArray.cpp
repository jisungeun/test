#include "CompareArray.h"
#include <cmath>

template <typename T>
auto CompareArrayIntegerNumber(const T* resultArray, const T* answerArray, const size_t& numberOfElements)->bool {
    for (size_t index = 0; index < numberOfElements; ++index) {
        if (resultArray[index] != answerArray[index]) {
            return false;
        }
    }
    return true;
}

template <typename T>
auto CompareArrayRationalNumber(const T* resultArray, const T* answerArray, const size_t& numberOfElements,
    const size_t& decimelPointCount)->bool {
    const auto multiplingNumber = std::pow(10, decimelPointCount);

    for (size_t index = 0; index < numberOfElements; ++index) {
        const auto& resultValue = resultArray[index];
        const auto& answerValue = answerArray[index];

        const auto diffValue = resultValue - answerValue;
        if (diffValue == 0) {
            continue;
        }

        double absDiffRatio;
        if (answerValue != 0) {
            absDiffRatio = std::fabs(diffValue / answerValue);
        } else {
            absDiffRatio = std::fabs(diffValue / resultValue);
        }
        

        if (static_cast<int32_t>(absDiffRatio * multiplingNumber) != 0) {
            return false;
        }
    }
    return true;
}

auto CompareArray(const bool* resultArray, const bool* answerArray, const size_t& numberOfElements) -> bool {
    return CompareArrayIntegerNumber<bool>(resultArray, answerArray, numberOfElements);
}

auto CompareArray(const int32_t* resultArray, const int32_t* answerArray, const size_t& numberOfElements) -> bool {
    return CompareArrayIntegerNumber<int32_t>(resultArray, answerArray, numberOfElements);
}

auto CompareArray(const char* resultArray, const char* answerArray, const size_t& numberOfElements) -> bool {
    return CompareArrayIntegerNumber<char>(resultArray, answerArray, numberOfElements);
}

auto CompareArray(const uint8_t* resultArray, const uint8_t* answerArray, const size_t& numberOfElements) -> bool {
    return CompareArrayIntegerNumber<uint8_t>(resultArray, answerArray, numberOfElements);
}

auto CompareArray(const uint16_t* resultArray, const uint16_t* answerArray, const size_t& numberOfElements) -> bool {
    return CompareArrayIntegerNumber<uint16_t>(resultArray, answerArray, numberOfElements);
}

auto CompareArray(const float* resultArray, const float* answerArray, const size_t& numberOfElements,
    const size_t& decimalPointCount) -> bool {
    return CompareArrayRationalNumber<float>(resultArray, answerArray, numberOfElements, decimalPointCount);
}

auto CompareArray(const double* resultArray, const double* answerArray, const size_t& numberOfElements,
    const size_t& decimalPointCount) -> bool {
    return CompareArrayRationalNumber<double>(resultArray, answerArray, numberOfElements, decimalPointCount);
}
