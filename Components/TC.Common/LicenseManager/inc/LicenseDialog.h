#pragma once

#include <memory>
#include <QDialog>

#include "LicenseManager.h"
#include "TCLicenseManagerExport.h"


class TCLicenseManager_API LicenseDialog : public QDialog {
    Q_OBJECT

public:
    LicenseDialog(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~LicenseDialog() override;

    auto SetCloseOnDeactivation(bool close) -> void;
    auto IsCloseOnDeactivation() const -> bool;

protected slots:
    void done(int res) override;
    void reqOnlineActivation();
    void reqOfflineActivationRequest();
    void reqOfflineActivation();
    void reqOnlineDeactivation();
    void reqOfflineDeactivation();
    void reqDemoActivation();

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};