#pragma once

#pragma warning(disable:4239)
#pragma warning(disable:4458)

#include <memory>
#include <string>
#include <map>

#include <QString>

#include "TCLicenseManagerExport.h"


class TCLicenseManager_API LicenseManager
{
public:
	typedef std::shared_ptr<LicenseManager> Pointer;
    typedef std::map<QString, QString> MetaData;

	enum class Code {
	    OK,
		FAILURE,
		EXPIRED,
		SUSPENDED,
		GRACE_PERIOD_OVER,
		TRIAL_EXPIRED,
		LOCAL_TRIAL_EXPIRED,
        TRIAL_NOT_STARTED,
		RELEASE_UPDATE_AVAILABLE,
		RELEASE_NO_UPDATE_AVAILABLE,
		E_FILE_PATH,
		E_PRODUCT_FILE,
		E_PRODUCT_DATA,
		E_PRODUCT_ID,
		E_SYSTEM_PERMISSION,
		E_FILE_PERMISSION,
		E_WMIC,
		E_TIME,
		E_INET,
        E_NET_PROXY,
        E_HOST_URL,
        E_BUFFER_SIZE,
        E_APP_VERSION_LENGTH,
        E_REVOKED,
        E_LICENSE_KEY,
        E_LICENSE_TYPE,
        E_OFFLINE_RESPONSE_FILE,
        E_OFFLINE_RESPONSE_FILE_EXPIRED,
        E_ACTIVATION_LIMIT,
        E_ACTIVATION_NOT_FOUND,
        E_DEACTIVATION_LIMIT,
        E_TRIAL_NOT_ALLOWED,
        E_TRIAL_ACTIVATION_LIMIT,
        E_MACHINE_FINGERPRINT,
        E_METADATA_KEY_LENGTH,
        E_METADATA_VALUE_LENGTH,
        E_ACTIVATION_METADATA_LIMIT,
        E_TRIAL_ACTIVATION_METADATA_LIMIT,
        E_METADATA_KEY_NOT_FOUND,
        E_TIME_MODIFIED,
        E_RELEASE_VERSION_FORMAT,
        E_AUTHENTICATION_FAILED,
        E_METER_ATTRIBUTE_NOT_FOUND,
        E_METER_ATTRIBUTE_USES_LIMIT_REACHED,
        E_VM,
        E_COUNTRY,
        E_IP,
        E_RATE_LIMIT,
        E_SERVER,
        E_CLIENT
	};

    enum class LicenseType {
        Invalid,
        Commercial,
        Evaluation
    };

public:
    LicenseManager(LicenseManager&) = delete;
	~LicenseManager();
	static auto GetInstance(void)->Pointer;

    //Online Activation
    static auto SetLicenseInfo(const QString& product, const QString& productID, MetaData& metadata = MetaData())->void;
    static auto Activate(const QString& key)->bool;

	//Offline Activation
	static auto RequestOfflineActivation(const QString& licenseKey, const QString& path)->bool;
	static auto ActivateOfflineLicense(const QString& licenseKey, const QString& path)->bool;

    //Deactivation
    static auto Deactivate() -> bool;
    static auto RequestOfflineDeactivation(const QString& path) -> bool;

    //Trial License
    static auto ActivateTrial(const QString& name, const QString& email)->bool;
    static auto GetTrialLeftDays(void)->int;
    static auto GetTrialID(void)->QString;

    //Common
    static auto GetErrorAsString(void)->QString;
    static auto GetError(void)->Code;
    static auto CheckLicense(bool onlyCommercial=true)->bool;
    static auto CheckFeature(const QString& feature)->bool;
    static auto Check(const QString& feature)->bool {
        return CheckFeature(feature);
    }
    static auto GetUser(void)->QString;
    static auto GrantedLicenseType(void)->LicenseType;
    static auto GetProductMetaData(const QString& name)->QString;
    static auto GetLicenseMetaData(const QString& name)->QString;
    static auto GetProductEdition()->QString;

protected:
	LicenseManager();

    auto CheckLicenseCommercial()->bool;
    auto CheckLicenseTrial()->bool;

	auto SetLastError(int LexCode)->void;
    auto DecodeProductId(const std::string& key)->std::wstring;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
