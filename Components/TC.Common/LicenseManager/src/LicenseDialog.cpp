#include <QFileDialog>
#include <QMessageBox>

#include "ui_LicenseDialog.h"
#include "LicenseDialog.h"
#include "LicenseManager.h"

struct LicenseDialog::Impl {
	Ui::LicenseDialog ui;

	bool closeOnDeactivation = false;

	auto UpdateTabs() -> void {
		if (LicenseManager::GetInstance()->CheckLicense(false)) {
			ui.activationTab->setTabVisible(0, false);
			ui.activationTab->setTabVisible(1, false);
			ui.activationTab->setTabVisible(2, true);
			ui.activationTab->setTabVisible(3, false);

			ui.activationTab->setCurrentIndex(2);
		} else {
			ui.activationTab->setTabVisible(0, true);
			ui.activationTab->setTabVisible(1, true);
			ui.activationTab->setTabVisible(2, false);
			ui.activationTab->setTabVisible(3, true);

			ui.activationTab->setCurrentIndex(0);
		}
	}
};

LicenseDialog::LicenseDialog(QWidget* parent, Qt::WindowFlags f)
	: QDialog(parent, f)
	, d { new Impl() } {
	auto& ui = d->ui;
	ui.setupUi(this);

	ui.editDemoPassword->setEchoMode(QLineEdit::Password);

	connect(ui.btnOnlineActivate, SIGNAL(clicked()), this, SLOT(reqOnlineActivation()));
	connect(ui.btnOfflineRequest, SIGNAL(clicked()), this, SLOT(reqOfflineActivationRequest()));
	connect(ui.btnOfflineActive, SIGNAL(clicked()), this, SLOT(reqOfflineActivation()));
	connect(ui.btnOnlineDeactivate, SIGNAL(clicked()), this, SLOT(reqOnlineDeactivation()));
	connect(ui.btnOfflineDeactivate, SIGNAL(clicked()), this, SLOT(reqOfflineDeactivation()));
	connect(ui.btnDemoActivate, SIGNAL(clicked()), this, SLOT(reqDemoActivation()));

	d->UpdateTabs();
}

LicenseDialog::~LicenseDialog() {}

auto LicenseDialog::SetCloseOnDeactivation(bool close) -> void {
	d->closeOnDeactivation = close;
}

auto LicenseDialog::IsCloseOnDeactivation() const -> bool {
	return d->closeOnDeactivation;
}

void LicenseDialog::done(int res) {
	QDialog::done(res);
}

void LicenseDialog::reqDemoActivation() {
	const auto key = d->ui.editDemoPassword->text();
	if (key != "Tomocube2023") { // TODO: Password management
		return;
	}
	if(!LicenseManager::GetInstance()->ActivateTrial("Test", "testEmail@naver.com")) {
		QMessageBox::warning(this, "Trial License", QString("Trial License is not activated.\nError:%1")
			.arg(LicenseManager::GetInstance()->GetErrorAsString()));
	}else {
		QMessageBox::information(this, "License", "Successfully activated");
		d->UpdateTabs();
	}
}

void LicenseDialog::reqOnlineActivation() {
	const auto key = d->ui.editOnlineKey->text();
	if (!LicenseManager::GetInstance()->Activate(key)) {
		QMessageBox::warning(this, "License", QString("License is not activated.\nError:%1")
							.arg(LicenseManager::GetInstance()->GetErrorAsString()));
	} else {
		QMessageBox::information(this, "License", "Successfully activated");
		d->UpdateTabs();
	}
}

void LicenseDialog::reqOfflineActivationRequest() {
	const auto key = d->ui.editOfflineKey->text();
	const auto path = QFileDialog::getSaveFileName(this, "Select a file to save the license request file", QString(), tr("Text (*.txt)"));
	if (path.isEmpty())
		return;

	if (!LicenseManager::GetInstance()->RequestOfflineActivation(key, path)) {
		QMessageBox::warning(this, "License", QString("It fails to generated a license request file to %1").arg(path));
	} else {
		QMessageBox::information(this, "License", QString("Send the license file to support@tomocube.com\nPath:%1").arg(path));
		d->UpdateTabs();
	}
}

void LicenseDialog::reqOfflineActivation() {
	const auto key = d->ui.editOfflineKey->text();
	const auto path = QFileDialog::getOpenFileName(this, "Select a license file", QString(), tr("License Response (*.dat)"));
	if (path.isEmpty())
		return;

	if (!LicenseManager::GetInstance()->ActivateOfflineLicense(key, path)) {
		QMessageBox::warning(this, "License", QString("License is not activated.\nError:%1")
							.arg(LicenseManager::GetInstance()->GetErrorAsString()));
	} else {
		QMessageBox::information(this, "License", "Successfully activated");
		d->UpdateTabs();
	}
}

void LicenseDialog::reqOnlineDeactivation() {
	if (!LicenseManager::GetInstance()->Deactivate()) {
		QMessageBox::warning(this, "License", QString("License is not deactivated.\nError:%1")
							.arg(LicenseManager::GetInstance()->GetErrorAsString()));
	} else {
		QMessageBox::information(this, "License", "Successfully deactivated");

		if (d->closeOnDeactivation)
			close();
		else
			d->UpdateTabs();
	}
}

void LicenseDialog::reqOfflineDeactivation() {
	const auto path = QFileDialog::getSaveFileName(this, "Select folder to save license deactivation request file", QString(), tr("Text (*.txt)"));
	if (path.isEmpty())
		return;

	if (!LicenseManager::GetInstance()->RequestOfflineDeactivation(path)) {
		QMessageBox::warning(this, "License", QString("Failed to generate a deactivation offline file:\n%1").arg(path));
	} else {
		QMessageBox::information(this, "License", QString("Send the license file to support@tomocube.com\nPath:%1").arg(path));

		if (d->closeOnDeactivation)
			close();
		else
			d->UpdateTabs();
	}
}
