#include <string>
#include <ctime>

namespace Lex {
#define LEXACTIVATOR_STATIC
#include "LexActivator.h"

constexpr uint32_t LexUser = LA_USER;
constexpr uint32_t LexSystem = LA_SYSTEM;
constexpr uint32_t LexInMemory = LA_IN_MEMORY;
}

#include "LicenseManager.h"

#ifdef _DEBUG
#pragma comment(lib, "libcurl_MDd")
#pragma comment(lib, "LexActivatord")
#pragma comment(lib, "winhttp")
#else
#pragma comment(lib, "libcurl_MD")
#pragma comment(lib, "LexActivator")
#pragma comment(lib, "winhttp")
#endif

struct LicenseManager::Impl {
    Code code;
    int trialLeftDays{ -1 };
    LicenseManager::LicenseType licenseType{ LicenseType::Invalid };

    std::map<Code, QString> errorText{
        {Code::OK, "Success"},
        {Code::FAILURE, "Failure"},
        {Code::EXPIRED, "The license has expired or system time has been tampered with.\nEnsure your date and time settings are correct"},
        {Code::SUSPENDED, "The license has been suspended."},
        {Code::GRACE_PERIOD_OVER, "The grace period for server sync is over."},
        {Code::TRIAL_EXPIRED, "The trial has expired or system time has been tampered with.\nEnsure your date and time settings are correct."},
        {Code::LOCAL_TRIAL_EXPIRED, "The local trial has expired or system time has been tampered with.\nEnsure your date and time settings are correct."},
        {Code::TRIAL_NOT_STARTED, "Trial is not started"},
        {Code::RELEASE_UPDATE_AVAILABLE, "A new update is available for the product. This means a new release has been published for the product."},
        {Code::RELEASE_NO_UPDATE_AVAILABLE, "No new update is available for the product. The current version is latest."},
        {Code::E_FILE_PATH, "Invalid file path."},
        {Code::E_PRODUCT_FILE, "Invalid or corrupted product file."},
        {Code::E_PRODUCT_DATA, "Invalid product data."},
        {Code::E_PRODUCT_ID, "The product id is incorrect."},
        {Code::E_SYSTEM_PERMISSION, "Insufficent system permissions. Occurs when LA_SYSTEM flag is used but application is not run with admin privileges."},
        {Code::E_FILE_PERMISSION, "No permission to write to file."},
        {Code::E_WMIC, "Fingerprint couldn't be generated because Windows Management Instrumentation(WMI) service has been disabled."},
        {Code::E_TIME, "The difference between the network time and the system time is more than allowed clock offset."},
        {Code::E_INET, "Failed to connect to the server due to network error."},
        {Code::E_NET_PROXY, "Invalid network proxy."},
        {Code::E_HOST_URL, "Invalid licensing host url."},
        {Code::E_BUFFER_SIZE, "The buffer size was smaller than required."},
        {Code::E_APP_VERSION_LENGTH, "App version length is more than 256 characters."},
        {Code::E_REVOKED, "The license has been revoked."},
        {Code::E_LICENSE_KEY, "Invalid license key."},
        {Code::E_LICENSE_TYPE, "Invalid license type. Make sure floating license is not being used."},
        {Code::E_OFFLINE_RESPONSE_FILE, "Invalid offline activation response file."},
        {Code::E_OFFLINE_RESPONSE_FILE_EXPIRED, "The offline activation response has expired."},
        {Code::E_ACTIVATION_LIMIT, "The license has reached it's allowed activations limit."},
        {Code::E_ACTIVATION_NOT_FOUND, "The license activation was deleted on the server."},
        {Code::E_DEACTIVATION_LIMIT, "The license has reached it's allowed deactivations limit."},
        {Code::E_TRIAL_NOT_ALLOWED, "Trial not allowed for the product."},
        {Code::E_TRIAL_ACTIVATION_LIMIT, "Your account has reached it's trial activations limit."},
        {Code::E_MACHINE_FINGERPRINT, "Machine fingerprint has changed since activation."},
        {Code::E_METADATA_KEY_LENGTH, "Metadata key length is more than 256 characters."},
        {Code::E_METADATA_VALUE_LENGTH, "Metadata value length is more than 256 characters."},
        {Code::E_ACTIVATION_METADATA_LIMIT, "The license has reached it's metadata fields limit."},
        {Code::E_TRIAL_ACTIVATION_METADATA_LIMIT, "The trial has reached it's metadata fields limit."},
        {Code::E_METADATA_KEY_NOT_FOUND, "The metadata key does not exist."},
        {Code::E_TIME_MODIFIED, "The system time has been tampered (backdated)."},
        {Code::E_RELEASE_VERSION_FORMAT, "Invalid version format."},
        {Code::E_AUTHENTICATION_FAILED, "Incorrect email or password."},
        {Code::E_METER_ATTRIBUTE_NOT_FOUND, "The meter attribute does not exist."},
        {Code::E_METER_ATTRIBUTE_USES_LIMIT_REACHED, "The meter attribute has reached it's usage limit."},
        {Code::E_VM, "Application is being run inside a virtual machine / hypervisor, and activation has been disallowed in the VM."},
        {Code::E_COUNTRY, "Country is not allowed."},
        {Code::E_IP, "IP address is not allowed."},
        {Code::E_RATE_LIMIT, " Rate limit for API has reached, try again later."},
        {Code::E_SERVER, "Server error."},
        {Code::E_CLIENT, "Client error."},
    };
};

LicenseManager::LicenseManager() : d{ new Impl() } {
}

auto LicenseManager::CheckLicenseCommercial() -> bool {
    d->code = Code::OK;

    auto status = Lex::IsLicenseGenuine();
    if (!((Lex::LA_OK == status) || (Lex::LA_EXPIRED == status) || (Lex::LA_SUSPENDED == status))) {
        SetLastError(status);
        return false;
    }

    return true;
}

auto LicenseManager::CheckLicenseTrial() -> bool {
    d->code = Code::OK;

    auto status = Lex::IsTrialGenuine();
    if (Lex::LA_OK == status) {
        unsigned int trialExpiryDate = 0;
        Lex::GetTrialExpiryDate(&trialExpiryDate);
        d->trialLeftDays = (trialExpiryDate - time(nullptr)) / 86500;
    }
    else if (Lex::LA_TRIAL_EXPIRED == status) {
        SetLastError(status);
        return false;
    }
    else {
        SetLastError(status);
        return false;
    }

    return true;
}

LicenseManager::~LicenseManager() {
}

auto LicenseManager::GetInstance(void)->Pointer {
    static Pointer theInstance{ new LicenseManager() };
    return theInstance;
}

auto LicenseManager::SetLicenseInfo(const QString& product, const QString& productID, MetaData& metadata)->void {
    auto lm = GetInstance();

    const auto productData = product.toStdWString();
    const auto productId = productID.toStdString();

    auto status = Lex::SetProductData(productData.c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
    }

    status = Lex::SetProductId(lm->DecodeProductId(productId).c_str(), LA_USER);
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
    }

    for (auto data : metadata) {
        status = Lex::SetActivationMetadata(data.first.toStdWString().c_str(), data.second.toStdWString().c_str());
        if (Lex::LA_OK != status) {
            lm->SetLastError(status);
        }
    }
}

auto LicenseManager::Activate(const QString& key) -> bool {
    auto lm = GetInstance();

    auto status = Lex::SetLicenseKey(key.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    status = Lex::ActivateLicense();    
    if(!((Lex::LA_OK == status) || (Lex::LA_EXPIRED == status) || (Lex::LA_SUSPENDED == status))) {
        lm->SetLastError(status);
        return false;
    }

    return true;
}

auto LicenseManager::CheckLicense(bool onlyCommercial) -> bool {
    auto lm = GetInstance();

    if (lm->CheckLicenseCommercial()) {
        lm->d->licenseType = LicenseType::Commercial;
        return true;
    }

    if (!onlyCommercial) {
        if (lm->CheckLicenseTrial()) {
            lm->d->licenseType = LicenseType::Evaluation;
            return true;
        }
    }

    return false;
}

auto LicenseManager::CheckFeature(const QString& feature)->bool {
    uint32_t enabled;
    wchar_t value[512]{0, };

    auto lm = GetInstance();
    if(Lex::LA_OK == Lex::GetProductVersionFeatureFlag(feature.toStdWString().c_str(), &enabled, value, 512)) {
        return (enabled == 1);
    }
    return false;
}

auto LicenseManager::GetUser()->QString {
    auto lm = GetInstance();
    return lm->GetLicenseMetaData("customer_email");
}

auto LicenseManager::GrantedLicenseType() -> LicenseType {
    return GetInstance()->d->licenseType;
}

auto LicenseManager::RequestOfflineActivation(const QString& licenseKey, const QString& path)->bool {
    auto lm = GetInstance();
    auto& d = lm->d;

    d->code = Code::OK;

    auto status = Lex::SetLicenseKey(licenseKey.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    status = Lex::GenerateOfflineActivationRequest(path.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    return true;
}

auto LicenseManager::ActivateOfflineLicense(const QString& licenseKey, const QString& path)->bool {
    auto lm = GetInstance();
    auto& d = lm->d;

    d->code = Code::OK;

    auto status = Lex::SetLicenseKey(licenseKey.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    auto ret = Lex::ActivateLicenseOffline(path.toStdWString().c_str());
    if (ret != Lex::LA_OK) {
        lm->SetLastError(ret);
        return false;
    }

    return true;
}


auto LicenseManager::Deactivate() -> bool {
    auto lm = GetInstance();
    auto& d = lm->d;

    d->code = Code::OK;

    auto ret = Lex::DeactivateLicense();
    if (ret != Lex::LA_OK) {
        lm->SetLastError(ret);
        return false;
    }

    return true;
}

auto LicenseManager::RequestOfflineDeactivation(const QString& path) -> bool {
    auto lm = GetInstance();
    auto& d = lm->d;

    d->code = Code::OK;

    auto ret = Lex::GenerateOfflineDeactivationRequest(path.toStdWString().c_str());
    if (ret != Lex::LA_OK) {
        lm->SetLastError(ret);
        return false;
    }

    return true;
}

auto LicenseManager::ActivateTrial(const QString& name, const QString& email)->bool {
    auto lm = GetInstance();
    auto& d = lm->d;
    d->code = Code::OK;

    auto  status = Lex::SetTrialActivationMetadata(L"name", name.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

     status = Lex::SetTrialActivationMetadata(L"email", email.toStdWString().c_str());
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    status = Lex::ActivateTrial();
    if (Lex::LA_OK != status) {
        lm->SetLastError(status);
        return false;
    }

    return true;
}

auto LicenseManager::GetTrialLeftDays() -> int {
    return GetInstance()->d->trialLeftDays;
}

auto LicenseManager::GetTrialID() -> QString {
    auto strID = [=]()->QString {
        wchar_t trialID[512]{ 0, };
        if (Lex::LA_OK == Lex::GetTrialId(trialID, 512)) {
            return QString::fromStdWString((trialID));
        } else {
            return "Invalid";
        }
    }();
    return strID;
}

auto LicenseManager::GetProductMetaData(const QString& name) -> QString {
    auto data = [=]()->QString {
        wchar_t value[512]{ 0, };
        const auto ret = Lex::GetProductMetadata(name.toStdWString().c_str(), value, 512);
        if(Lex::LA_OK == ret) {
            return QString::fromStdWString(value);
        } else {
            return "";
        }
    }();
    return data;
}

auto LicenseManager::GetLicenseMetaData(const QString& name)->QString {
    auto data = [=]()->QString {
        wchar_t value[512]{ 0, };
        if (Lex::LA_OK == Lex::GetLicenseMetadata(name.toStdWString().c_str(), value, 512)) {
            return QString::fromStdWString(value);
        } else {
            return "";
        }
    }();
    return data;
}

auto LicenseManager::GetProductEdition() -> QString {
    auto data = [=]()->QString {
        wchar_t value[512]{ 0, };
        if (Lex::LA_OK == Lex::GetProductVersionName(value, 512)) {
            return QString::fromStdWString(value);
        } else {
            return "";
        }
    }();
    return data;
}

auto LicenseManager::GetErrorAsString()->QString {
    auto lm = GetInstance();
    auto& d = lm->d;

    const auto code = d->code;
    auto itr = d->errorText.find(code);
    return (itr == d->errorText.end()) ? QString("Unknown error [%1]").arg(static_cast<int>(code)) : itr->second;
}

auto LicenseManager::GetError() -> Code {
    return GetInstance()->d->code;
}

auto LicenseManager::SetLastError(int LexCode)->void {
    auto lm = GetInstance();
    auto& d = lm->d;

    d->code = [=]() {
        switch(LexCode) {
        case Lex::LA_OK:
            return Code::OK;
        case Lex::LA_FAIL:
            return Code::FAILURE;
        case Lex::LA_EXPIRED:
            return Code::EXPIRED;
        case Lex::LA_SUSPENDED:
            return Code::SUSPENDED;
        case Lex::LA_GRACE_PERIOD_OVER:
            return Code::GRACE_PERIOD_OVER;
        case Lex::LA_TRIAL_EXPIRED:
            return Code::TRIAL_EXPIRED;
        case Lex::LA_LOCAL_TRIAL_EXPIRED:
            return Code::LOCAL_TRIAL_EXPIRED;
        case Lex::LA_RELEASE_UPDATE_AVAILABLE:
            return Code::RELEASE_UPDATE_AVAILABLE;
        case Lex::LA_RELEASE_NO_UPDATE_AVAILABLE:
            return Code::RELEASE_NO_UPDATE_AVAILABLE;
        case Lex::LA_E_FILE_PATH:
            return Code::E_FILE_PATH;
        case Lex::LA_E_PRODUCT_FILE:
            return Code::E_PRODUCT_FILE;
        case Lex::LA_E_PRODUCT_DATA:
            return Code::E_PRODUCT_DATA;
        case Lex::LA_E_PRODUCT_ID:
            return Code::E_PRODUCT_ID;
        case Lex::LA_E_SYSTEM_PERMISSION:
            return Code::E_SYSTEM_PERMISSION;
        case Lex::LA_E_FILE_PERMISSION:
            return Code::E_FILE_PERMISSION;
        case Lex::LA_E_WMIC:
            return Code::E_WMIC;
        case Lex::LA_E_TIME:
            return Code::E_TIME;
        case Lex::LA_E_INET:
            return Code::E_INET;
        case Lex::LA_E_NET_PROXY:
            return Code::E_NET_PROXY;
        case Lex::LA_E_HOST_URL:
            return Code::E_HOST_URL;
        case Lex::LA_E_BUFFER_SIZE:
            return Code::E_BUFFER_SIZE;
        case Lex::LA_E_APP_VERSION_LENGTH:
            return Code::E_APP_VERSION_LENGTH;
        case Lex::LA_E_REVOKED:
            return Code::E_REVOKED;
        case Lex::LA_E_LICENSE_KEY:
            return Code::E_LICENSE_KEY;
        case Lex::LA_E_LICENSE_TYPE:
            return Code::E_LICENSE_TYPE;
        case Lex::LA_E_OFFLINE_RESPONSE_FILE:
            return Code::E_OFFLINE_RESPONSE_FILE;
        case Lex::LA_E_OFFLINE_RESPONSE_FILE_EXPIRED:
            return Code::E_OFFLINE_RESPONSE_FILE_EXPIRED;
        case Lex::LA_E_ACTIVATION_LIMIT:
            return Code::E_ACTIVATION_LIMIT;
        case Lex::LA_E_ACTIVATION_NOT_FOUND:
            return Code::E_ACTIVATION_NOT_FOUND;
        case Lex::LA_E_DEACTIVATION_LIMIT:
            return Code::E_DEACTIVATION_LIMIT;
        case Lex::LA_E_TRIAL_NOT_ALLOWED:
            return Code::E_TRIAL_NOT_ALLOWED;
        case Lex::LA_E_TRIAL_ACTIVATION_LIMIT:
            return Code::E_TRIAL_ACTIVATION_LIMIT;
        case Lex::LA_E_MACHINE_FINGERPRINT:
            return Code::E_MACHINE_FINGERPRINT;
        case Lex::LA_E_METADATA_KEY_LENGTH:
            return Code::E_METADATA_KEY_LENGTH;
        case Lex::LA_E_METADATA_VALUE_LENGTH:
            return Code::E_METADATA_KEY_NOT_FOUND;
        case Lex::LA_E_ACTIVATION_METADATA_LIMIT:
            return Code::E_ACTIVATION_METADATA_LIMIT;
        case Lex::LA_E_TRIAL_ACTIVATION_METADATA_LIMIT:
            return Code::E_TRIAL_ACTIVATION_METADATA_LIMIT;
        case Lex::LA_E_METADATA_KEY_NOT_FOUND:
            return Code::E_METADATA_KEY_NOT_FOUND;
        case Lex::LA_E_TIME_MODIFIED:
            return Code::E_TIME_MODIFIED;
        case Lex::LA_E_RELEASE_VERSION_FORMAT:
            return Code::E_RELEASE_VERSION_FORMAT;
        case Lex::LA_E_AUTHENTICATION_FAILED:
            return Code::E_AUTHENTICATION_FAILED;
        case Lex::LA_E_METER_ATTRIBUTE_NOT_FOUND:
            return Code::E_METER_ATTRIBUTE_NOT_FOUND;
        case Lex::LA_E_METER_ATTRIBUTE_USES_LIMIT_REACHED:
            return Code::E_METER_ATTRIBUTE_USES_LIMIT_REACHED;
        case Lex::LA_E_VM:
            return Code::E_VM;
        case Lex::LA_E_COUNTRY:
            return Code::E_COUNTRY;
        case Lex::LA_E_IP:
            return Code::E_IP;
        case Lex::LA_E_RATE_LIMIT:
            return Code::E_RATE_LIMIT;
        case Lex::LA_E_SERVER:
            return Code::E_SERVER;
        case Lex::LA_E_CLIENT:
            return Code::E_CLIENT;
        default:
            return Code::FAILURE;
        }
    }();
}

auto LicenseManager::DecodeProductId(const std::string& key)->std::wstring {
#if 0
    const size_t passwordLength = 16;
    static const char password[passwordLength] = "Tomocube, Inc  "; //the more obvious, the less obvious
    std::string result = key;
    for (size_t i = 0; i < key.length(); i++)
        result[i] ^= ~password[i % passwordLength];

    std::wstring wresult(result.begin(), result.end());
    return wresult;
#else
    std::wstring wresult(key.begin(), key.end());
    return wresult;
#endif
}
