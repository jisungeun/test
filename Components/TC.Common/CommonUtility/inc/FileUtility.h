#pragma once
#include <functional>
#include <QStringList>

#include "TCCommonUtilityExport.h"

namespace TC {
	//Copy files
    /**
	 * \brief 파일 복사하기 (CopyFilesRecursive를 사용하여 동작함)
	 * \param strSrc 소스 디렉토리
	 * \param strDest 목적지 디렉토리
	 * \param bIncludeSubdir 하위 디렉토리를 복사할지 여부
	 * \return true/false
	 */
	auto TCCommonUtility_API CopyFiles(const QString& strSrc, const QString& strDest, bool bIncludeSubdir = false)->bool;
    /**
	 * \brief 하위 디렉토리를 순회하면서 파일 복사하기 (CopyFiles를 사용하여 동작함)
	 * \param strSrc 소스 디렉토리
	 * \param strDest 목적지 디렉토리
	 * \param stopFunc 복사를 외부에서 중지하기
	 * \return true/false
	 */
	auto TCCommonUtility_API CopyFilesRecursive(const QString& strSrc, const QString& strDest, 
		std::function<bool()> stopFunc = {})->bool;

	/**
	 * \brief 지정된 폴더 하위에 있는 모든 파일 및 폴더 삭제
	 * \param 삭제하고자 하는 상위 폴더
	 * \param stopFunc 삭제를 외부에서 중지하기
	 */
	auto TCCommonUtility_API RemoveRecursive(const QString& strTarget, std::function<bool()> stopFunc = {})->bool;

	//Find files
    /**
	 * \brief 특정 패턴을 갖는 파일/디렉토리 목록을 찾아서 반환
	 * \param strTop 최상위 폴더 경로
	 * \param strPattern 찾을 패턴 (e.g. "*.TCF") 지정. 대소문자 가리지 않음
	 * \param bIncludeFile 파일도 검색할지 여부
	 * \return 일치하는 모든 파일/디렉토리 목록 (Full path)
	 */
	auto TCCommonUtility_API FindFolders(const QString& strTop, const QString& strPattern, bool bIncludeFile = false)->QStringList;
	auto TCCommonUtility_API FindFolders(const QString& strTop, const QStringList& strPattern, bool bIncludeFile = false)->QStringList;
    /**
	 * \brief 특정 패턴을 갖는 파일 목록을 찾아서 반환
	 * \param strTop strTop 최상위 폴더 경로
	 * \param strPattern 찾을 패턴 (e.g. "*.TCF") 지정. 대소문자 가리지 않음
	 * \return 일치하는 모든 파일 목록 (Full path)
	 */
	auto TCCommonUtility_API FindFiles(const QString& strTop, const QString& strPattern)->QStringList;
    auto TCCommonUtility_API FindFiles(const QString& strTop, const QStringList& strPattern)->QStringList;

	//Get Folder/File name
    /**
	 * \brief 경로에서 파일명 추출 (경로에 있는 마지막 / 또는 \ 뒤에 있는 모든 이름을 반환)
	 * \param strFullPath 입력 경로
	 * \return 파일명 또는 파일명을 포함하지 않은 경로의 경우 가장 내부 디렉토리 이름
	 */
	auto TCCommonUtility_API GetPathName(const QString& strFullPath)->QString;

    /**
	 * \brief 비어 있는 디렉토리인지 확인
	 * \param strPath 확인할 디렉토리 경로
	 * \return 비어 있는지 여부
	 */
	auto TCCommonUtility_API IsEmptyDirectory(const QString& strPath)->bool;

	/**
	 * \brief 경로의 마지막 폴더 생성
	 * \param path 경로
	 * \return 생성 성공 여부
	 */
	auto TCCommonUtility_API MakeDir(const QString& path)->bool;

    /**
	 * \brief 경로에서 확장자를 제외한 파일명을 가져온다
	 * \param path 경로
	 * \param ext 확장자, nii.gz 같이 point를 포함하는 확장자의 경우 별도 기입해서 활용
	 * \return 확장자를 제외한 파일명
	 */
	auto TCCommonUtility_API GetBaseName(const QString& path,const QString& ext = QString())->QString;
}