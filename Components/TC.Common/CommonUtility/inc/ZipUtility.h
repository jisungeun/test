#pragma once

#include <QStringList>

#include "TCCommonUtilityExport.h"

namespace TC::ZipUtil {
    auto TCCommonUtility_API SetBinPath(const QString& path)->void;
    auto TCCommonUtility_API Compress(const QStringList& sources, const QString& targetPath)->bool;
}