#pragma once

#include <QStringList>

#include "TCCommonUtilityExport.h"

namespace TC {
    /**
     * \brief 특정 경로를 문자열로 나누기
     * \param path 경로 (ex "/Data/3D/000000", "C:\Temp\data.tmp")
     * \return 나눠진 문자열 (ex ["Data","3D","000000"], ["C:","Temp","data.tmp"]);
     */
    auto TCCommonUtility_API DividePath(const QString& path)->QStringList;
}
