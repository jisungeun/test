#pragma once

#include <Windows.h>
#include "TCCommonUtilityExport.h"

namespace TC {
    auto TCCommonUtility_API GetAvailableMemoryInBytes() -> float;
    auto TCCommonUtility_API GetIncreasedMemoryInGB(const float& beforeAvailableMemoryInBytes,
        const float& afterAvailableMemoryInBytes)->float;
    auto TCCommonUtility_API GetIncreasedMemoryInMB(const float& beforeAvailableMemoryInBytes,
        const float& afterAvailableMemoryInBytes) -> float;
}