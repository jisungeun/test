#include <QMap>
#include <catch2/catch.hpp>

#include <FileUtility.h>

using namespace TC;

namespace _Test {
}

TEST_CASE("CopyFiles", "[FileUtility]") {
    namespace test = _Test;
    
    SECTION("Copy Single File") {
    }

    SECTION("Copy Multipe Files") {
    }
}

TEST_CASE("FindFiles", "[FileUtility]") {
    namespace test = _Test;

    SECTION("Find files") {
    }

    SECTION("Find folders") {
    }
}

TEST_CASE("GetPathName", "[FileUtility]") {
    namespace test = _Test;

    SECTION("Get path name") {
        QMap<QString, QString> testSet{
            {"C:\\Test\\Test.xxx", "Test.xxx"},
            {"C:/Test/Test.xxx", "Test.xxx"},
            {"C:\\Test/Test.xxx", "Test.xxx"},
            {"C:\\Test\\Test", "Test"}
        };

        for(auto itr=testSet.begin(); itr!=testSet.end(); ++itr) {
            const auto fname = TC::GetPathName(itr.key());
            REQUIRE(fname == itr.value());
        }
    }
}