#include <catch2/catch.hpp>
#include <QString>
#include <QRegExp>
#include <QList>

TEST_CASE("QString") {
    SECTION("Argument") {
        const auto arg_0 = QString("%1").arg(0);
        CHECK(arg_0 == "0");

        const auto arg_000000 = QString("%1").arg(0, 6, 10, QChar('0'));
        CHECK(arg_000000 == "000000");
    }

    SECTION("Split with QReg") {
        const auto src = QString("221026.151346.Test.014.Group1.C2.T013P01");

        auto list = src.split(QRegExp(".T\\d+P"));
        CHECK(list.size() == 2);
        CHECK(list.at(0).compare("221026.151346.Test.014.Group1.C2", Qt::CaseInsensitive) == 0);
        CHECK(list.at(1).compare("01", Qt::CaseInsensitive) == 0);
    }
}