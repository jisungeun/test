#include <catch2/catch.hpp>

#include <PathUtility.h>

using namespace TC;
namespace PathUtilityTest {
    auto CompareQStringList(const QStringList& list1, const QStringList& list2)->bool {
        if (list1.size() != list2.size()) {
            return false;
        }

        for (auto i = 0; i < list1.size(); ++i) {
            const auto& list1String = list1.at(i);
            const auto& list2String = list2.at(i);

            if (list1String != list2String) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("DividePath", "[PathUtility]") {

        SECTION("Divide TCF Path") {
            const QString path1 = "/Data/3D/000000";
            const QString path2 = "/Data/3DFL/CH0";
            const QString path3 = "/Info/Tiles";
            const QString path4 = "/a/b/c/d/e/f/g";

            const auto divided1 = DividePath(path1);
            const auto divided2 = DividePath(path2);
            const auto divided3 = DividePath(path3);
            const auto divided4 = DividePath(path4);

            CHECK(divided1.size() == 3);
            CHECK(divided2.size() == 3);
            CHECK(divided3.size() == 2);
            CHECK(divided4.size() == 7);

            CHECK(CompareQStringList(divided1, QStringList{ "Data","3D","000000" }));
            CHECK(CompareQStringList(divided2, QStringList{ "Data","3DFL","CH0" }));
            CHECK(CompareQStringList(divided3, QStringList{ "Info","Tiles" }));
            CHECK(CompareQStringList(divided4, QStringList{ "a","b","c","d","e","f","g" }));
        }

        SECTION("Divide File Path") {
            const QString path1 = "C:\\Data\\file.dat";
            const QString path2 = "D:\\Data2\\file2.dat";
            const QString path3 = "E:/Data3/Data4/file3.dat";
            const QString path4 = "F:/a/b/c/d/e/f/g";

            const auto divided1 = DividePath(path1);
            const auto divided2 = DividePath(path2);
            const auto divided3 = DividePath(path3);
            const auto divided4 = DividePath(path4);

            CHECK(divided1.size() == 3);
            CHECK(divided2.size() == 3);
            CHECK(divided3.size() == 4);
            CHECK(divided4.size() == 8);

            CHECK(CompareQStringList(divided1, QStringList{ "C:","Data","file.dat" }));
            CHECK(CompareQStringList(divided2, QStringList{ "D:","Data2","file2.dat" }));
            CHECK(CompareQStringList(divided3, QStringList{ "E:","Data3","Data4","file3.dat" }));
            CHECK(CompareQStringList(divided4, QStringList{ "F:","a","b","c","d","e","f","g" }));
        }
    }
}
