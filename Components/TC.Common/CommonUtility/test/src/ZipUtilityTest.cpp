#include <catch2/catch.hpp>
#include <QString>
#include <QFileInfo>
#include <QDir>

#include <ZipUtility.h>

TEST_CASE("ZipUtilityTest") {
    SECTION("Compress") {
        auto files = QDir().entryList(QDir::Files | QDir::NoDotAndDotDot);
        CHECK(files.size() > 0);

        TC::ZipUtil::SetBinPath(ZipBin);

        CHECK(TC::ZipUtil::Compress(files, "./compress.7z") == true);
        CHECK(QDir().exists("./compress.7z") == true);

        QFile::remove("./compress.7z");
    }
}