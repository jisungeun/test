#include <catch2/catch.hpp>

#include "MemoryCheckUtility.h"

using namespace TC;

namespace MemoryCheckUtilityTest {
    TEST_CASE("MemoryCheckUtility") {
        SECTION("GetIncreasedMemoryInGB()") {
            const float beforeAvailableMemoryInBytes = 3.f * 1024.f * 1024.f * 1024.f;
            const float afterAvailableMemoryInBytes = 1.f * 1024.f * 1024.f * 1024.f;

            CHECK(GetIncreasedMemoryInGB(beforeAvailableMemoryInBytes, afterAvailableMemoryInBytes) == 2);
        }
        SECTION("GetIncreasedMemoryInMB()") {
            const float beforeAvailableMemoryInBytes = 3.f * 1024.f * 1024.f;
            const float afterAvailableMemoryInBytes = 1.f * 1024.f * 1024.f;

            CHECK(GetIncreasedMemoryInMB(beforeAvailableMemoryInBytes, afterAvailableMemoryInBytes) == 2);
        }
    }
}