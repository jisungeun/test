#include "MemoryCheckUtility.h"

namespace TC {
    auto GetAvailableMemoryInBytes() -> float {
        MEMORYSTATUSEX memoryInfo;
        memoryInfo.dwLength = sizeof(MEMORYSTATUSEX);

        const auto resultIsValid = (GlobalMemoryStatusEx(&memoryInfo) != 0);
        if (resultIsValid) {
            const auto availableMemoryInBytes = static_cast<double>(memoryInfo.ullAvailPhys);
            return static_cast<float>(availableMemoryInBytes);
        } else {
            return 0;
        }
    }

    auto GetIncreasedMemoryInGB(const float& beforeAvailableMemoryInBytes,
        const float& afterAvailableMemoryInBytes) -> float {
        const auto increasedInBytes = beforeAvailableMemoryInBytes - afterAvailableMemoryInBytes;
        return increasedInBytes / 1024.f / 1024.f / 1024.f;
    }

    auto GetIncreasedMemoryInMB(const float& beforeAvailableMemoryInBytes,
        const float& afterAvailableMemoryInBytes) -> float {
        const auto increasedInBytes = beforeAvailableMemoryInBytes - afterAvailableMemoryInBytes;
        return increasedInBytes / 1024.f / 1024.f;
    }
}
