#include <QCoreApplication>
#include <QDir>
#include <QProcess>
#include "ZipUtility.h"

namespace TC::ZipUtil {
    static QString zipBin;
    
    auto SetBinPath(const QString& path)->void {
        zipBin = path;
    }
    
    auto Compress(const QStringList& sources, const QString& targetPath)->bool {
        if(zipBin.isEmpty()) {
            zipBin = QCoreApplication::applicationDirPath() + "/7zr.exe";
        }
        
        if(!QDir().exists(zipBin)) {
            return false;
        }
        
        QFileInfo finfo(targetPath);
        if(!QDir().exists(finfo.absolutePath())) {
            if(!QDir().mkpath(finfo.absolutePath())) return false;
        }

        QStringList arguments {
            "a",
            targetPath
        };
        arguments << sources;

        QProcess process;
        process.start(zipBin, arguments);
        if(!process.waitForFinished(20000)) {
            return false;
        }

        return QDir().exists(targetPath);
    }
}