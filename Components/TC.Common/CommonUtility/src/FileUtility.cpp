#include <iostream>
#include <QDirIterator>
#include <QDir>

#include "FileUtility.h"

namespace TC {
	auto CopyFiles(const QString& strSrc, const QString& strDest, bool bIncludeSubdir)->bool {
		const QDir srcDir(strSrc), dstDir(strDest);

		//copy only files at source directory....
		if (bIncludeSubdir == false) {
			auto srcList = srcDir.entryList(QDir::Files);

			if (!dstDir.exists()) {
				if (!QDir().mkpath(strDest)) return false;
			}

			foreach(auto srcFile, srcList) {
				const auto srcPath = srcDir.filePath(srcFile);
				const auto dstPath = dstDir.filePath(srcFile);

				if (!QFile::copy(srcPath, dstPath)) return false;
			}
		}
		else {
			if (!CopyFilesRecursive(strSrc, strDest)) return false;
		}

		return true;
	}

	auto CopyFilesRecursive(const QString& strSrc, const QString& strDest, std::function<bool()> stopFunc)->bool {
        QDir srcDir(strSrc);
        QDir destDir(strDest);

        if (!destDir.exists()) {
            if (!destDir.mkpath(".")) {
                return false;
            }
        }

        for (const QFileInfo& fileInfo : srcDir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs)) {
            const QString fileName = fileInfo.fileName();

			if(stopFunc && stopFunc()) return true;

            if (fileInfo.isDir()) {
                const QString subSrcPath = srcDir.filePath(fileName);
                const QString subDestPath = destDir.filePath(fileName);

                if (!CopyFilesRecursive(subSrcPath, subDestPath, stopFunc)) {
                    return false;
                }
            } else {
                const QString srcFilePath = srcDir.filePath(fileName);
                const QString destFilePath = destDir.filePath(fileName);

				if(QFile::exists(destFilePath)) QFile::remove(destFilePath);
                if (!QFile::copy(srcFilePath, destFilePath)) {
                    return false;
                }
            }
        }

        return true;
	}

    auto RemoveRecursive(const QString& strTarget, std::function<bool()> stopFunc) -> bool {
		QDir dir(strTarget);

        for (const auto& fileInfo : dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot)) {
			if(stopFunc && stopFunc()) return true;
            QFile::remove(fileInfo.absoluteFilePath());
        }

        for (const auto& fileInfo : dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            RemoveRecursive(fileInfo.absoluteFilePath(), stopFunc);
			if(stopFunc && stopFunc()) return true;
            QDir(fileInfo.absoluteFilePath()).rmdir(".");
        }

		QDir(strTarget).rmdir(".");

		return true;
    }

    auto FindFolders(const QString& strTop, const QString& strPattern, bool bIncludeFile)->QStringList {
		const QStringList nameFilter(strPattern);
		return FindFolders(strTop, nameFilter, bIncludeFile);
	}

	auto FindFolders(const QString& strTop, const QStringList& strPattern, bool bIncludeFile)->QStringList {
		QStringList ltFolder;

		const QDir::Filters filter = (bIncludeFile) ? (QDir::Dirs | QDir::Files) : (QDir::Dirs);

		QDirIterator itr(strTop, strPattern, filter, QDirIterator::Subdirectories);
		while (itr.hasNext()) {
			const auto strPath = itr.next();
			ltFolder.append(strPath);
		}

		return ltFolder;
	}

    auto FindFiles(const QString& strTop, const QString& strPattern) -> QStringList {
		return FindFiles(strTop, QStringList(strPattern));
    }

    auto FindFiles(const QString& strTop, const QStringList& strPattern) -> QStringList {
		QStringList ltFolder;

		const QDir::Filters filter = QDir::Files;

		QDirIterator itr(strTop, strPattern, filter, QDirIterator::Subdirectories);
		while (itr.hasNext()) {
			const auto strPath = itr.next();
			ltFolder.append(strPath);
		}

		return ltFolder;
    }

    auto GetPathName(const QString& strFullPath)->QString {
		auto strIn = strFullPath;
		strIn = strIn.replace("\\", "/");
		const auto tokens = strIn.split('/');
		return tokens.at(tokens.length() - 1);
	}

    auto IsEmptyDirectory(const QString& strPath) -> bool {
		QDir dir(strPath);
		const auto entries = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
		return (entries.length() == 0);
    }

	auto MakeDir(const QString& path)->bool {
		if(true == path.isEmpty()) {
			return false;
		}

		const QDir dir(path);
		if (false == dir.exists() && false == dir.mkpath(".")) {
            return false;
        }

		return true;
	}

	auto GetBaseName(const QString& path,const QString& ext)->QString {
		auto filename = QFileInfo(path).fileName();
	    auto split = filename.split(".");
		auto extensionLength = 0;

		if(ext.isEmpty()) {
		    extensionLength = split[split.length()-1].length();
		}else {
		    extensionLength = ext.length();
		}

		auto baseName = filename.chopped(extensionLength+1);

		//return QFileInfo(path).baseName();
		return baseName;
	}
}
