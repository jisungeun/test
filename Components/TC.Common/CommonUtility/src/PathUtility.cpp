#include "PathUtility.h"

namespace TC {
    auto DividePath(const QString& path) -> QStringList {
        auto replacedPath = path;
        replacedPath = replacedPath.replace(QChar('\\'), QChar('/'));

        if(replacedPath.at(0) == QChar('/')) {
            replacedPath = replacedPath.mid(1, replacedPath.length() - 1);
        }

        if(*replacedPath.end()==QChar('/')) {
            replacedPath = replacedPath.left(replacedPath.length() - 1);
        }

        return replacedPath.split("/");
    }
}


