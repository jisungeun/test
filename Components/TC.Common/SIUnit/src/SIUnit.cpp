#include "SIUnit.h"
#include <QMap>

QMap<LengthUnit, int32_t> lengthNumericalIndex{
    {LengthUnit::Meter, 0},
    {LengthUnit::Millimenter, -3},
    {LengthUnit::Micrometer, -6},
    {LengthUnit::Nanometer, -9}
};

QMap<TimeUnit, double> timeRatioToSecond{
    {TimeUnit::Day, 86400},
    {TimeUnit::Hour, 3600},
    {TimeUnit::Minute, 60},
    {TimeUnit::Second, 1},
    {TimeUnit::Millisecond, 0.001},
    {TimeUnit::Microsecond, 0.000001},
    {TimeUnit::Nanosecond, 0.000000001}
};

QMap<DataSizeUnit, double> sizeRatioToByte{
    {DataSizeUnit::Byte, 1},

    {DataSizeUnit::Kilobyte, std::pow(10,3)},
    {DataSizeUnit::Megabyte, std::pow(10,6)},
    {DataSizeUnit::Gigabyte, std::pow(10,9)},
    {DataSizeUnit::Terabyte, std::pow(10,12)},

    {DataSizeUnit::Kibibyte, std::pow(2,10)},
    {DataSizeUnit::Mebibyte, std::pow(2,20)},
    {DataSizeUnit::Gibibyte, std::pow(2,30)},
    {DataSizeUnit::Tebibyte, std::pow(2,40)},
};

auto ConvertUnit(const double& value, const LengthUnit& fromUnit, const LengthUnit& toUnit) -> double {
    const auto fromUnitNumericalIndex = lengthNumericalIndex[fromUnit];
    const auto toUnitNumericalIndex = lengthNumericalIndex[toUnit];

    const auto multiplicationNumericalIndex = fromUnitNumericalIndex - toUnitNumericalIndex;
    const auto conversionResult = value * std::pow(10, multiplicationNumericalIndex);
    return conversionResult;
}

auto ConvertUnit(const double& value, const TimeUnit& fromUnit, const TimeUnit& toUnit) -> double {
    const auto fromUnitRatioToSecond = timeRatioToSecond[fromUnit];
    const auto toUnitRatioToSecond = timeRatioToSecond[toUnit];

    const auto conversionResult = value * fromUnitRatioToSecond / toUnitRatioToSecond;
    return conversionResult;
}

auto ConvertUnit(const double& value, const DataSizeUnit& fromUnit, const DataSizeUnit& toUnit) -> double {
    const auto fromUnitRatioToByte = sizeRatioToByte[fromUnit];
    const auto toUnitRatioToByte = sizeRatioToByte[toUnit];

    const auto conversionResult = value * fromUnitRatioToByte / toUnitRatioToByte;
    return conversionResult;
}
