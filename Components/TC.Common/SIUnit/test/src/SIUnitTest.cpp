#include <catch2/catch.hpp>

#include "SIUnit.h"

namespace SIUnitTest {
    TEST_CASE("SIUnit : unit test") {
        SECTION("LengthUnit") {
            CHECK(LengthUnit::_size() == 4);
            CHECK((+LengthUnit::Meter)._to_index() == 0);
            CHECK((+LengthUnit::Millimenter)._to_index() == 1);
            CHECK((+LengthUnit::Micrometer)._to_index() == 2);
            CHECK((+LengthUnit::Nanometer)._to_index() == 3);
        }

        SECTION("ConvertUnit(LengthUnit)") {
            CHECK(ConvertUnit(1, LengthUnit::Meter, LengthUnit::Millimenter) == 1000);
            CHECK(ConvertUnit(1, LengthUnit::Millimenter, LengthUnit::Micrometer) == 1000);
            CHECK(ConvertUnit(1, LengthUnit::Micrometer, LengthUnit::Nanometer) == 1000);
            CHECK(ConvertUnit(1000000, LengthUnit::Nanometer, LengthUnit::Meter) == 0.001);
        }

        SECTION("TimeUnit") {
            CHECK(TimeUnit::_size() == 7);
            CHECK((+TimeUnit::Day)._to_index() == 0);
            CHECK((+TimeUnit::Hour)._to_index() == 1);
            CHECK((+TimeUnit::Minute)._to_index() == 2);
            CHECK((+TimeUnit::Second)._to_index() == 3);
            CHECK((+TimeUnit::Millisecond)._to_index() == 4);
            CHECK((+TimeUnit::Microsecond)._to_index() == 5);
            CHECK((+TimeUnit::Nanosecond)._to_index() == 6);
        }

        SECTION("ConvertUnit(TimeUnit)") {
            CHECK(ConvertUnit(1, TimeUnit::Day, TimeUnit::Second) == 86400);
            CHECK(ConvertUnit(1, TimeUnit::Hour, TimeUnit::Second) == 3600);
            CHECK(ConvertUnit(1, TimeUnit::Minute, TimeUnit::Second) == 60);
            CHECK(ConvertUnit(1, TimeUnit::Second, TimeUnit::Second) == 1);
            CHECK(ConvertUnit(1, TimeUnit::Millisecond, TimeUnit::Second) == 0.001);
            CHECK(ConvertUnit(1, TimeUnit::Microsecond, TimeUnit::Second) == 0.000001);
            CHECK(ConvertUnit(1, TimeUnit::Nanosecond, TimeUnit::Second) == 0.000000001);
        }

        SECTION("DataSizeUnit") {
            CHECK(DataSizeUnit::_size() == 9);
            CHECK((+DataSizeUnit::Byte)._to_index() == 0);
            CHECK((+DataSizeUnit::Kilobyte)._to_index() == 1);
            CHECK((+DataSizeUnit::Megabyte)._to_index() == 2);
            CHECK((+DataSizeUnit::Gigabyte)._to_index() == 3);
            CHECK((+DataSizeUnit::Terabyte)._to_index() == 4);
            CHECK((+DataSizeUnit::Kibibyte)._to_index() == 5);
            CHECK((+DataSizeUnit::Mebibyte)._to_index() == 6);
            CHECK((+DataSizeUnit::Gibibyte)._to_index() == 7);
            CHECK((+DataSizeUnit::Tebibyte)._to_index() == 8);
        }
        SECTION("ConvertUnit(DataSizeUnit)") {
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Byte)) == 1000000);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Kilobyte)) == 1000);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Megabyte)) == 1);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Gigabyte) * 1000) == 1);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Terabyte) * 1000000) == 1);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Kibibyte) * 1000) == 976562);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Mebibyte) * 1000000) == 953674);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Gibibyte) * 1000000000) == 931322);
            CHECK(static_cast<int32_t>(ConvertUnit(1, DataSizeUnit::Megabyte, DataSizeUnit::Tebibyte) * 1000000000) == 909);
        }
    }
}
