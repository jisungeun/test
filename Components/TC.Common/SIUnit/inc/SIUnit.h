#pragma once

#include <enum.h>

#include "TCSIUnitExport.h"

BETTER_ENUM(LengthUnit, int32_t,
    Meter,
    Millimenter,
    Micrometer,
    Nanometer);

BETTER_ENUM(TimeUnit, int32_t,
    Day,
    Hour,
    Minute,
    Second,
    Millisecond,
    Microsecond,
    Nanosecond
)

BETTER_ENUM(DataSizeUnit, int32_t,
    Byte,
    
    Kilobyte,
    Megabyte,
    Gigabyte,
    Terabyte,

    Kibibyte,
    Mebibyte,
    Gibibyte,
    Tebibyte
)

auto TCSIUnit_API ConvertUnit(const double& value, const LengthUnit& fromUnit, const LengthUnit& toUnit)->double;
auto TCSIUnit_API ConvertUnit(const double& value, const TimeUnit& fromUnit, const TimeUnit& toUnit)->double;
auto TCSIUnit_API ConvertUnit(const double& value, const DataSizeUnit& fromUnit, const DataSizeUnit& toUnit)->double;