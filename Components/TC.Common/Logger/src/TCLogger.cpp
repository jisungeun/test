#include <iostream>
#include <QFileInfo>
#include <QDir>

#include <QDebug>
#include <QsLogDest.h>

#include "TCLogger.h"

namespace TC {
    struct Logger::Impl {
        bool m_bInit{ false };

        QString m_strLogPath;
        QString m_strErrorLogPath;
    };

    namespace Qs = QsLogging;

    Logger::Logger() : d{ new Impl } {
    }

    auto Logger::GetInstance(void)->Logger* {
        static auto* pInstance = new Logger();
        return pInstance;
    }

    auto Logger::Initialize(const QString& strLogPath, const QString& strErrorLogPath, int level, int64_t maxBytes, int maxCount)->void {
        auto* LoggerInstance = Logger::GetInstance();

        if(LoggerInstance->d->m_bInit) return;

        QFileInfo logPath(strLogPath);
        if (!logPath.absoluteDir().exists()) {
            logPath.absoluteDir().mkpath("./");
        }

        QFileInfo errorPath(strErrorLogPath);
        if (!errorPath.absoluteDir().exists()) {
            errorPath.absoluteDir().mkpath("./");
        }

        auto& logger = Qs::Logger::instance();

        logger.setLoggingLevel(static_cast<Qs::Level>(level));

        Qs::DestinationPtr fileDestination(Qs::DestinationFactory::MakeFileDestination(
            strLogPath, Qs::EnableLogRotation, Qs::MaxSizeBytes(maxBytes), Qs::MaxOldLogCount(maxCount)));
        logger.addDestination(fileDestination);

        Qs::DestinationPtr fileErrorDestination(Qs::DestinationFactory::MakeFileLeveledDestination(
            strErrorLogPath, Qs::EnableLogRotation, Qs::MaxSizeBytes(maxBytes), Qs::MaxOldLogCount(maxCount), Qs::WarnLevel));
        logger.addDestination(fileErrorDestination);
        
        Logger::GetInstance()->d->m_bInit = true;

        LoggerInstance->d->m_strLogPath = strLogPath;
        LoggerInstance->d->m_strErrorLogPath = strErrorLogPath;
    }

    auto Logger::SetTraceLevel(void) -> void {
        Qs::Logger::instance().setLoggingLevel(Qs::TraceLevel);
    }

    auto Logger::SetLevel(int level) -> void {
        Qs::Logger::instance().setLoggingLevel(static_cast<Qs::Level>(level));
    }

    auto Logger::GetLevel()->int {
        return static_cast<int>(Qs::Logger::instance().loggingLevel());
    }

    auto Logger::IsHigher(QsLogging::Level level) -> bool {
        return (GetLevel() > level);
    }

    auto Logger::IsIncluded(QsLogging::Level level) -> bool {
        return (GetLevel() <= level);
    }

    auto Logger::GetLogPath(void) -> QString {
        if (!GetInstance()->d->m_bInit) return QString("");
        return GetInstance()->d->m_strLogPath;
    }

    auto Logger::GetErrorLogPath(void) -> QString {
        if (!GetInstance()->d->m_bInit) return QString("");
        return GetInstance()->d->m_strErrorLogPath;
    }
}