#pragma once 

#include <memory>
#include <QString>

#include <QsLog.h>

// Rewrite QLOG macros if MODULE_NAME is defined
#ifdef LOGGER_TAG

#undef QLOG_TRACE
#undef QLOG_DEBUG
#undef QLOG_INFO
#undef QLOG_WARN
#undef QLOG_ERROR
#undef QLOG_FATAL

#define QLOG_TRACE() \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::TraceLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::TraceLevel).stream() << LOGGER_TAG
#define QLOG_DEBUG() \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::DebugLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::DebugLevel).stream() << LOGGER_TAG
#define QLOG_INFO()  \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::InfoLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::InfoLevel).stream() << LOGGER_TAG
#define QLOG_WARN()  \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::WarnLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::WarnLevel).stream() << LOGGER_TAG
#define QLOG_ERROR() \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::ErrorLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::ErrorLevel).stream() << LOGGER_TAG
#define QLOG_FATAL() \
    if (QsLogging::Logger::instance().loggingLevel() > QsLogging::FatalLevel) {} \
    else QsLogging::Logger::Helper(QsLogging::FatalLevel).stream() << LOGGER_TAG
#endif

#ifdef TC_LOGGER_DISABLED
#include "QsLogDisableForThisFile.h"
#endif

#include "TCLoggerExport.h"

namespace TC {
    class TCLogger_API Logger
    {
    protected:
        Logger();

    public:
        static auto GetInstance()->Logger*;
        static auto Initialize(const QString& strLogPath, const QString& strErrorLogPath = QString(""),
                               int level=2, int64_t maxBytes= 2048000, int maxCount=50)->void;

        static auto SetTraceLevel(void)->void;
        static auto SetLevel(int level)->void;
        static auto GetLevel()->int;
        static auto IsHigher(QsLogging::Level level)->bool;
        static auto IsIncluded(QsLogging::Level level)->bool;

        static auto GetLogPath(void)->QString;
        static auto GetErrorLogPath(void)->QString;

    protected:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}