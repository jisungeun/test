#pragma once

#include <memory>

#include <QCryptographicHash>
#include <QString>

#include "TCOtpExport.h"

namespace TC {
	class TCOtp_API OtpGenerator {
	public:
		OtpGenerator(int otpLength, const QString& key);
		OtpGenerator(int otpLength, const QByteArray& key);
		~OtpGenerator();

		auto Generate(const QString& message) -> QString;
		auto Generate(const QByteArray& message) -> QString;

		static auto Generate(int otpLength, const QString& key, const QString& message) -> QString;
		static auto Generate(int otpLength, const QByteArray& key, const QByteArray& message) -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}