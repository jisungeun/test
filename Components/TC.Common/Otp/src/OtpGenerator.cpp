#include "OtpGenerator.h"

namespace TC {
	static const int size = QCryptographicHash::hashLength(QCryptographicHash::Sha512);

	struct OtpGenerator::Impl {
		int otpLength = -1;
		QByteArray key;

		auto Hmac(const QByteArray& message) const -> QByteArray {
			QCryptographicHash hash(QCryptographicHash::Sha512);
			QByteArray ipad(size, 0x36), opad(size, 0x5C);

			for (int i = 0; i < size; i++) {
				ipad[i] = static_cast<char>(ipad.at(i) ^ key.at(i));
				opad[i] = static_cast<char>(opad.at(i) ^ key.at(i));
			}

			hash.addData(ipad);
			hash.addData(message);
			const auto crypto = hash.result();

			hash.reset();
			hash.addData(opad);
			hash.addData(crypto);
			auto hmac = hash.result();

			return hmac;
		}
	};

	OtpGenerator::OtpGenerator(int otpLength, const QString& key) : OtpGenerator(otpLength, key.toLocal8Bit()) {}

	OtpGenerator::OtpGenerator(int otpLength, const QByteArray& key) : d(new Impl) {
		d->otpLength = otpLength;
		d->key = key;
		d->key.append(size - key.size(), 0);
	}

	OtpGenerator::~OtpGenerator() = default;

	auto OtpGenerator::Generate(const QString& message) -> QString {
		return Generate(message.toLocal8Bit());
	}

	auto OtpGenerator::Generate(const QByteArray& message) -> QString {
		auto crypto = d->Hmac(message);
		QString otp(d->otpLength, 0);

		for (int i = 0; i < d->otpLength; i++) {
			const auto index = static_cast<unsigned char>(crypto[i]) % size;
			auto code = static_cast<char>((static_cast<unsigned char>(crypto[index]) % 24) + 65);

			auto t = code;

			if (code >= 'I')
				code++;
			if (code >= 'O')
				code++;

			otp[i] = code;
		}

		return otp;
	}

	auto OtpGenerator::Generate(int otpLength, const QString& key, const QString& message) -> QString {
		return Generate(otpLength, key.toLocal8Bit(), message.toLocal8Bit());
	}

	auto OtpGenerator::Generate(int otpLength, const QByteArray& key, const QByteArray& message) -> QString {
		OtpGenerator generator(otpLength, key);
		return generator.Generate(message);
	}
}
