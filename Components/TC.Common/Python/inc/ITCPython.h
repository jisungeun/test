#pragma once

#include <any>
#include <memory>

#include <enum.h>

#include <QString>

#include "TCPythonModelExport.h"

namespace TC {
	BETTER_ENUM(PythonMode, int,
				PyFileInput = 0, PySingleInput = 1, PyEvalInput = 2);

	BETTER_ENUM(PythonArrType, int,
				arrUSHORT = 0, arrDouble = 1, arrFLOAT = 2,
				arrSHORT = 3, arrBYTE = 4, arrINT = 5,
				arrUBYTE = 6, arrUINT = 7);

	class TCPythonModel_API ITCPython {
	public:		
		virtual auto SetPythonHome(const QString& path) -> void = 0;
		virtual auto GetPythonHome() -> QString = 0;

		virtual auto InitPython() -> bool = 0;

		virtual auto ExecuteString(const QString& code, int stringmode = PythonMode::PySingleInput) -> bool =0;
		virtual auto ExecutePyFile(const QString& script_path) -> bool =0;

		virtual auto CopyArrToPy(void* arr, int dim, unsigned dims[3], const QString& varName, int type = PythonArrType::arrUSHORT, int targetType = PythonArrType::arrUSHORT) -> bool = 0;
		virtual auto CopyArrToCpp(std::any& arr, const QString& varName) -> std::tuple<QList<int>, std::string> = 0;

		virtual auto ConvertType(const QString& varname, PythonArrType type) -> bool =0;
		virtual auto Reshape(const QString& varname, unsigned dims[3]) -> bool = 0;

		virtual auto GetIntValue(const QString& varName) -> int =0;
		virtual auto GetDoubleValue(const QString& varName) -> double =0;
		virtual auto GetIntArray(const QString& varName) -> QList<int> =0;
		virtual auto GetDoubleArray(const QString& varName) -> QList<double> = 0;

		virtual auto TurnOffPython() -> void = 0;

	protected:
		auto copyBuffer(std::any& anyPtr, void* source, std::string typeString, unsigned dimSize) -> bool;
	};
}
