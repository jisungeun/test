#define LOGGER_TAG "[TCPython]"

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
//https://github.com/cython/cython/issues/2498
//https://stackoverflow.com/questions/52749662/using-deprecated-numpy-api
//https://stackoverflow.com/questions/29595222/multithreading-with-python-and-c-api
#include <iostream>
#include <Python.h>
#include <numpy/arrayobject.h>

#include <QFileInfo>
#include <QMutexLocker>

#include <TCLogger.h>

#include "TCPython.h"

namespace TC {
	struct PythonModule::Impl {
		PyObject* mainObject{ nullptr };
		bool initialized{ false };
		bool hadError{ false };
		QString python_path{ "C:/TomoPython" };
		QMutex mutex;
	};

	PythonModule::PythonModule() :d{ new Impl } {

	}
	PythonModule::~PythonModule() {
		if (Py_IsInitialized()) {
			Py_Finalize();
		}
	}
	auto PythonModule::TurnOffPython() -> void {
		QMutexLocker locker(&d->mutex);

		if (Py_IsInitialized()) {
			Py_Finalize();
		}
	}

	auto PythonModule::SetPythonHome(const QString& path) -> void {
		QMutexLocker locker(&d->mutex);

		if (d->initialized) return;

		Py_SetPythonHome(path.toStdWString().c_str());
		Py_Initialize();
		PyEval_InitThreads();
		d->initialized = Py_IsInitialized();

		if (d->initialized) {
			d->python_path = path;
			auto dict = PyImport_GetModuleDict();
			d->mainObject = PyDict_GetItemString(dict, "__main__");
			ExecuteString("import os");
			auto dirPath = QString("os.chdir('%1')").arg(d->python_path);
			ExecuteString(dirPath);
			ExecuteString("import numpy as np");//numpy is essential
			ExecuteString("import sys");
		}
	}

	auto PythonModule::GetPythonHome() -> QString {
		return d->python_path;
	}

	auto PythonModule::InitPython() -> bool {
		QMutexLocker locker(&d->mutex);
		if(d->initialized) return true;

		Py_SetPythonHome(d->python_path.toStdWString().c_str());//force python home to embedded system        
		Py_Initialize();
		d->initialized = Py_IsInitialized();

		if (d->initialized) {
			auto dict = PyImport_GetModuleDict();
			d->mainObject = PyDict_GetItemString(dict, "__main__");
			ExecuteString("import os");
			auto dirPath = QString("os.chdir('%1')").arg(d->python_path);
			ExecuteString(dirPath);
			ExecuteString("import numpy as np");//numpy is essential            
			ExecuteString("import sys");
		}

		return d->initialized;
	}

	auto PythonModule::GetInstance() -> Pointer {
		static Pointer theInstance{ new PythonModule() };
		return theInstance;
	}

	auto PythonModule::ExecuteString(const QString& code, int stringmode) -> bool {
		if (false == d->initialized) {
			if (false == InitPython()) {
				return false;
			}
		}
		if (nullptr == d->mainObject) {
			QLOG_ERROR() << "main object is nullptr";
			return false;
		}
		d->hadError = false;
		PyObject* dict = nullptr;
		if (PyModule_Check(d->mainObject)) {
			dict = PyModule_GetDict(d->mainObject);
		}
		else if (PyDict_Check(d->mainObject)) {
			dict = d->mainObject;
		}
		if (true) {
			int pyStrMode = Py_eval_input;
			switch (stringmode) {
			case PyScriptMode::PyFileInput:
				pyStrMode = Py_file_input;
				break;
			case PyScriptMode::PySingleInput:
				pyStrMode = Py_single_input;
				break;
			case PyScriptMode::PyEvalInput:
				pyStrMode = Py_eval_input;
				break;
			}
			QLOG_INFO() << code;
			//auto result = PyRun_String(code.toStdString().c_str(), pyStrMode, dict, dict);
			auto result = true;
			PyRun_SimpleString(code.toLocal8Bit().constData());
			if (!result) {
				QLOG_ERROR() << "error occur in python code";
				Py_Finalize();
				Py_Initialize();//reinit python                
				d->hadError = true;
				return false;
			}
			//Py_DECREF(result);
		}

		return true;
	}

	auto PythonModule::ExecutePyFile(const QString& script_path) -> bool {
		if (nullptr == d->mainObject) {
			QLOG_INFO() << "main object is nullptr";
			return false;
		}
		QString path = QFileInfo(script_path).absolutePath();
		// See http://nedbatchelder.com/blog/200711/rethrowing_exceptions_in_python.html
		// Re-throwing is only needed in Python 2.7
		QStringList code = QStringList()
			<< "import sys"
			<< QString("sys.path.insert(0, '%1')").arg(path)
			<< "_updated_globals = globals()"
			<< QString("_updated_globals['__file__'] = '%1'").arg(script_path)
#if PY_MAJOR_VERSION >= 3
			<< QString("exec(open('%1').read(), _updated_globals)").arg(script_path);
#else
			<< "executeFile_exc_info = None"
			<< "try:"
			<< QString("    execfile('%1', _updated_globals)").arg(script_path)
			<< "except Exception as e:"
			<< "    executeFile_exc_info = sys.exc_info()"
			<< "finally:"
			<< "    del _updated_globals"
			<< QString("    if sys.path[0] == '%1': sys.path.pop(0)").arg(path)
			<< "    if executeFile_exc_info:"
			<< "        raise executeFile_exc_info[1], None, executeFile_exc_info[2]";
#endif
		return ExecuteString(code.join("\n"));
	}
	
	auto PythonModule::CopyArrToPy(void* arr, int dim, unsigned dims[3], const QString& varName, int type, int targetType) -> bool {
		if (false == d->initialized) {
			if (false == InitPython()) {
				return false;
			}
		}
		PyObject* npyArray = nullptr;
		npy_intp* npy_dims = new npy_intp[1];
		npy_dims[0] = dims[0];
		QString dimensionString;
		dimensionString.append(QString("["));
		dimensionString.append(QString::number(dims[0]));
		for (auto i = 1; i < dim; ++i)
		{
			dimensionString.append(QString(","));
			dimensionString.append(QString::number(dims[i]));
			npy_dims[0] *= dims[i];
		}
		dimensionString.append("]");

		NPY_TYPES npy_type;
		switch (type) {
		case ArrType::arrUSHORT:
			npy_type = NPY_USHORT;
			break;
		case ArrType::arrSHORT:
			npy_type = NPY_SHORT;
			break;
		case ArrType::arrBYTE:
			npy_type = NPY_BYTE;
			break;
		case ArrType::arrDouble:
			npy_type = NPY_DOUBLE;
			break;
		case ArrType::arrFLOAT:
			npy_type = NPY_FLOAT;
			break;
		case ArrType::arrINT:
			npy_type = NPY_INT;
			break;
		case ArrType::arrUBYTE:
			npy_type = NPY_UBYTE;
			break;
		case ArrType::arrUINT:
			npy_type = NPY_UINT;
			break;
		}

		int npy_nd = 1;

		PyObject* pyDict = PyModule_GetDict(d->mainObject);

		import_array1(false);
		npyArray = PyArray_SimpleNewFromData(npy_nd, npy_dims, npy_type, arr);
		const int status = PyDict_SetItemString(pyDict, QString("%1")
			.arg(varName).toStdString().c_str(),
			npyArray);		

		switch (targetType) {
		case ArrType::arrUSHORT:
			ExecuteString(QString("%1 = %1.astype(np.uint16)").arg(varName));
			break;
		case ArrType::arrSHORT:
			ExecuteString(QString("%1 = %1.astype(np.int16)").arg(varName));
			break;
		case ArrType::arrBYTE:
			ExecuteString(QString("%1 = %1.astype(np.int8)").arg(varName));
			npy_type = NPY_BYTE;
			break;
		case ArrType::arrDouble:
			ExecuteString(QString("%1 = %1.astype(np.float64)").arg(varName));
			npy_type = NPY_DOUBLE;
			break;
		case ArrType::arrFLOAT:
			ExecuteString(QString("%1 = %1.astype(np.float32)").arg(varName));
			npy_type = NPY_FLOAT;
			break;
		case ArrType::arrINT:
			ExecuteString(QString("%1 = %1.astype(np.int32)").arg(varName));
			npy_type = NPY_INT;
			break;
		case ArrType::arrUBYTE:
			ExecuteString(QString("%1 = %1.astype(np.uint8)").arg(varName));
			npy_type = NPY_UBYTE;
			break;
		case ArrType::arrUINT:
			ExecuteString(QString("%1 = %1.astype(np.uint32)").arg(varName));
			npy_type = NPY_UINT;
			break;
		}		

		QString reshapeScript;
		if(dim==3) {
			reshapeScript = QString("%1 = np.reshape(%1,(%2,%3,%4))").arg(varName).arg(dims[0]).arg(dims[1]).arg(dims[2]);
		}else {
			reshapeScript = QString("%1 = np.reshape(%1,(%2,%3))").arg(varName).arg(dims[0]).arg(dims[1]);
		}

		ExecuteString(reshapeScript);
		ExecuteString(QString("print(np.min(%1))").arg(varName));
		ExecuteString(QString("print(np.max(%1))").arg(varName));

		return true;
	}

    auto PythonModule::CopyArrToCpp(std::any& arr, const QString& varName) ->std::tuple<QList<int>, std::string> {
		if (false == d->initialized) {
			if (false == InitPython()) {
				return std::make_tuple(QList<int>(), std::string());
			}
		}
		ExecuteString(QString("%1_dtype = %1.dtype.name").arg(varName));
		PyObject* pyDict = PyModule_GetDict(d->mainObject);
		PyObject* py_dtype = PyDict_GetItemString(pyDict, QString("%1_dtype").arg(varName).toStdString().c_str());
		if(py_dtype == nullptr) {
			QLOG_ERROR() << "No python variable with " << varName;
		    return std::make_tuple(QList<int>(), std::string());
		}

		std::string dtype = PyUnicode_AsUTF8(py_dtype);
		QLOG_INFO() << "dtype: " << dtype.c_str();
		PyArrayObject* py_data = (PyArrayObject*)PyDict_GetItemString(pyDict, QString("%1").arg(varName).toStdString().c_str());		
		auto pyDim = PyArray_NDIM(py_data);

		QList<int> nr_dim;
		auto dim_size = 1;
		for (auto i = 0; i < pyDim; i++) {
			auto dim_element = PyArray_DIMS(py_data)[pyDim - 1 - i];
			nr_dim.push_back(static_cast<int>(dim_element));
			dim_size *= static_cast<int>(dim_element);
		}
		ExecuteString(QString("%1_1d = %1.reshape(-1)").arg(varName));
		PyArrayObject* py_data_1d = (PyArrayObject*)PyDict_GetItemString(pyDict, QString("%1_1d").arg(varName).toStdString().c_str());

		if (false == copyBuffer(arr, PyArray_DATA(py_data_1d), dtype, dim_size)) {
			QLOG_ERROR() << "Fails to copy array to cpp - " << varName;
			ExecuteString(QString("del %1_1d").arg(varName));
			Py_DECREF(py_data_1d);
			Py_DECREF(py_data);
			Py_DECREF(py_dtype);			
			return std::make_tuple(nr_dim, dtype);
		}

		ExecuteString(QString("del %1_1d").arg(varName));

		return std::make_tuple(nr_dim, dtype);
	}

    auto PythonModule::ConvertType(const QString& varname, ArrType type) -> bool {
		const auto nptype = [&]()->QString {
			QString str = "np.uint16";
			switch(type) {
			case ArrType::arrUSHORT:
				str = "np.uint16";
    			break;
		    case ArrType::arrSHORT:
			    str = "np.int16";
			    break;
		    case ArrType::arrBYTE:
			    str = "np.int8";
			    break;
		    case ArrType::arrDouble:
			    str = "np.float64";
			    break;
		    case ArrType::arrFLOAT:
			    str = "np.float32";
			    break;
		    case ArrType::arrINT:
			    str = "np.int32";
			    break;
		    case ArrType::arrUBYTE:
			    str = "np.uint8";
			    break;
		    case ArrType::arrUINT:
			    str = "np.uint32";
			    break;
			}
			return str;
		}();

		ExecuteString(QString("%1 = %1.astype(%2)").arg(varname).arg(nptype));
		return true;
    }

    auto PythonModule::Reshape(const QString& varname, unsigned dims[3]) -> bool {
		ExecuteString(QString("%1 = np.reshape(%1,(%2,%3,%4))").arg(varname).arg(dims[0]).arg(dims[1]).arg(dims[2]));
		return true;
    }

    auto PythonModule::copyBuffer(std::any& anyPtr, void* source, std::string typeString, unsigned dimSize) -> bool {
		if (typeString == "uint16") {
			auto rawData = std::shared_ptr<uint16_t[]>(new uint16_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint16_t) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "uint8") {
			auto rawData = std::shared_ptr<uint8_t[]>(new uint8_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint8_t) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "int8") {
			auto rawData = std::shared_ptr<int8_t[]>(new int8_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(int8_t) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "float64") {
			auto rawData = std::shared_ptr<double[]>(new double[dimSize]);
			memcpy(rawData.get(), source, sizeof(double) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "float32") {
			auto rawData = std::shared_ptr<float[]>(new float[dimSize]);
			memcpy(rawData.get(), source, sizeof(float) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "int32") {
			auto rawData = std::shared_ptr<int[]>(new int[dimSize]);
			memcpy(rawData.get(), source, sizeof(int) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "int16") {
			auto rawData = std::shared_ptr<short[]>(new short[dimSize]);
			memcpy(rawData.get(), source, sizeof(short) * dimSize);
			anyPtr = rawData;
		}
		else if (typeString == "uint32") {
			auto rawData = std::shared_ptr<uint32_t[]>(new uint32_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint32_t) * dimSize);
			anyPtr = rawData;
		}
		else {
			return false;
		}
		return true;
	}

	auto PythonModule::GetIntValue(const QString& varName) -> int {
		PyObject* pyDict = PyModule_GetDict(d->mainObject);
		if (pyDict) {			
			auto obj = PyDict_GetItemString(pyDict, varName.toStdString().c_str());
			if (obj) {
				auto val = PyLong_AsLong(obj);				
				Py_DECREF(obj);				
				return val;
			}
		}
		return -1;
	}
	auto PythonModule::GetDoubleValue(const QString& varName) -> double {
		PyObject* pyDict = PyModule_GetDict(d->mainObject);
		if (pyDict) {
			auto obj = PyDict_GetItemString(pyDict, varName.toStdString().c_str());
			if (obj) {
				auto val = PyFloat_AsDouble(obj);
				Py_DECREF(obj);				
				return val;
			}
		}
		return -1.0;
	}
	auto PythonModule::GetIntArray(const QString& varName) -> QList<int> {
		PyObject* pyDict = PyModule_GetDict(d->mainObject);
		auto result = QList<int>();
		if (pyDict) {
			auto obj = PyDict_GetItemString(pyDict, varName.toStdString().c_str());
			if (PyTuple_Check(obj)) {
				for (Py_ssize_t i = 0; i < PyTuple_Size(obj); i++) {
					auto objElement = PyTuple_GetItem(obj, i);
					result.push_back(PyLong_AsLong(objElement));
					Py_DECREF(objElement);
				}
			}
			else if (PyList_Check(obj)) {
				for (Py_ssize_t i = 0; i < PyList_Size(obj); i++) {
					auto objElement = PyList_GetItem(obj, i);
					result.push_back(PyLong_AsLong(objElement));
					Py_DECREF(objElement);
				}
			}
			Py_DECREF(obj);
		}				
		return result;
	}
	auto PythonModule::GetDoubleArray(const QString& varName) -> QList<double> {
		PyObject* pyDict = PyModule_GetDict(d->mainObject);
		auto result = QList<double>();
		if (pyDict) {
			auto obj = PyDict_GetItemString(pyDict, varName.toStdString().c_str());
			if (PyTuple_Check(obj)) {
				for (Py_ssize_t i = 0; i < PyTuple_Size(obj); i++) {
					auto objElement = PyTuple_GetItem(obj, i);
					result.push_back(PyFloat_AsDouble(objElement));
					Py_DECREF(objElement);
				}
			}
			else if (PyList_Check(obj)) {
				for (Py_ssize_t i = 0; i < PyList_Size(obj); i++) {
					auto objElement = PyList_GetItem(obj, i);
					result.push_back(PyFloat_AsDouble(objElement));
					Py_DECREF(objElement);
				}
			}
			Py_DECREF(obj);
		}				
		return result;
	}
}
