#pragma once

#include <any>
#include <memory>

#include <enum.h>

#include <QString>

#include "TCPythonExport.h"

namespace TC {
	BETTER_ENUM(PyScriptMode, int,
		PyFileInput = 0, PySingleInput = 1, PyEvalInput =2);
	BETTER_ENUM(ArrType, int,
		arrUSHORT = 0, arrDouble=1, arrFLOAT=2,
		arrSHORT =3,arrBYTE =4,arrINT=5,
		arrUBYTE = 6,arrUINT=7);

	class TCPython_API PythonModule final {
	public:
		typedef std::shared_ptr<PythonModule> Pointer;
	private:
		PythonModule();
		
	public:
		~PythonModule();
		static auto GetInstance()->Pointer;

		auto SetPythonHome(const QString& path)->void;
		auto GetPythonHome()->QString;

		auto InitPython()->bool;

		auto ExecuteString(const QString& code, int stringmode = PyScriptMode::PySingleInput)->bool;
		auto ExecutePyFile(const QString& script_path)->bool;

		auto CopyArrToPy(void* arr,int dim, unsigned dims[3], const QString& varName,int type = ArrType::arrUSHORT,int targetType = ArrType::arrUSHORT)->bool;
		auto CopyArrToCpp(std::any& arr, const QString& varName)->std::tuple<QList<int>,std::string>;

		auto ConvertType(const QString& varname, ArrType type)->bool;
		auto Reshape(const QString& varname, unsigned dims[3])->bool;

		auto GetIntValue(const QString& varName)->int;
		auto GetDoubleValue(const QString& varName)->double;
		auto GetIntArray(const QString& varName)->QList<int>;
		auto GetDoubleArray(const QString& varName)->QList<double>;

		auto TurnOffPython()->void;

	private:
		auto copyBuffer(std::any& anyPtr,void* source,std::string typeString,unsigned dimSize)->bool;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
