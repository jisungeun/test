project(PythonModuleTest)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES	
	src/PythonModuleTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE		
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_directories(${PROJECT_NAME}
	PRIVATE				
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE					
		Qt5::Core				
		TC::Components::Python
		TC::Components::TCFIO
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases")