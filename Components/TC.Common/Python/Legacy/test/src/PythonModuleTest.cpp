#include <iostream>

#include <QList>

#include <TCPython.h>
#include <TCFSimpleReader.h>
#include <QtWidgets/qapplication.h>

using namespace TC;

void main(int argc,char** argv) {
	//Script Test
	PythonModule::GetInstance()->ExecuteString("import sys");
	PythonModule::GetInstance()->ExecuteString("print(sys.version)");
	PythonModule::GetInstance()->ExecuteString("import os");			
	PythonModule::GetInstance()->ExecuteString("import matplotlib.pyplot as plt");
	PythonModule::GetInstance()->ExecuteString("print('Hello from script')");
	PythonModule::GetInstance()->ExecuteString("print('hello world')");

	//File Test
	PythonModule::GetInstance()->ExecuteString("print('Hello from file')");
	QString filePath = "C:/Users/HP/Desktop/pythonBaseTest/Hello.py";//TODO change py file path
	PythonModule::GetInstance()->ExecutePyFile(filePath);

	//Variable Test
	PythonModule::GetInstance()->ExecuteString("varInt = 15");
	PythonModule::GetInstance()->ExecuteString("varInt2 = 100");	
	auto intValue = PythonModule::GetInstance()->GetIntValue("varInt");
	std::cout << "##Int value: " << intValue << std::endl;	
	auto intValue2 = PythonModule::GetInstance()->GetIntValue("varInt2");
	std::cout << "###Int value2: " << intValue2 << std::endl;
	PythonModule::GetInstance()->ExecuteString("varDouble = 3.14");
	PythonModule::GetInstance()->ExecuteString("print(type(varDouble))");
	auto doubleValue = PythonModule::GetInstance()->GetDoubleValue("varDouble");
	std::cout << "Double value: " << doubleValue << std::endl;
	PythonModule::GetInstance()->ExecuteString("IntArr = [1,3,5,7,9]");
	auto intArray = PythonModule::GetInstance()->GetIntArray("IntArr");
	std::cout << "# of element in int array: " << intArray.count() << std::endl;
	for(auto val : intArray) {
		std::cout << val << " ";
	}
	std::cout << std::endl;
	PythonModule::GetInstance()->ExecuteString("DoubleArr = [1.3,2.6,3.9]");
	auto doubleArray = PythonModule::GetInstance()->GetDoubleArray("DoubleArr");
	std::cout << "# of element in double array: " << doubleArray.count() << std::endl;
	for(auto val : doubleArray) {
		std::cout << val << " ";
	}
	std::cout << std::endl;

	//C++ to Python Array Test
	{
		auto reader = new IO::TCFSimpleReader;
		QString tcfPath = "F:/CellLibrary/220518_AI_sampledata/20200609.153947.886.Hep G2-094.TCF";
		reader->SetFilePath(tcfPath);
		auto volumeData = reader->ReadHT3D(0);
		auto dim = volumeData.GetDimension();
		unsigned dims[3]{ std::get<0>(dim) ,std::get<1>(dim),std::get<2>(dim) };

		PythonModule::GetInstance()->CopyArrToPy((void*)volumeData.GetDataAsUInt16().get(), 3, dims, "TestPyArr", ArrType::arrUSHORT);
		PythonModule::GetInstance()->ExecuteString("print(TestPyArr.shape)");
		PythonModule::GetInstance()->ExecuteString("plt.imshow(TestPyArr[105])");
		PythonModule::GetInstance()->ExecuteString("plt.show()");
		PythonModule::GetInstance()->ExecuteString("plt.close()");

		//Python to C++ Array Test
		//flip image using numpy function
		PythonModule::GetInstance()->ExecuteString("TestPyArr = np.flip(TestPyArr,axis=1)");
		//PythonModule::GetInstance()->ExecuteString("plt.imshow(TestPyArr[105])");
		//PythonModule::GetInstance()->ExecuteString("plt.show()");
		//PythonModule::GetInstance()->ExecuteString("plt.close()");

		//copy numpy array to c++ array
		std::any copyArr;
		auto pyTuple = PythonModule::GetInstance()->CopyArrToCpp(copyArr, "TestPyArr");
		auto pyDim = std::get<0>(pyTuple);
		auto pyType = std::get<1>(pyTuple);
		std::cout << "pyType: " << pyType << std::endl;
		std::cout << "pyNdim: " << pyDim.count() << std::endl;
		for (auto i = 0; i < pyDim.count(); i++) {
			std::cout << pyDim[i] << " ";
		}
		std::cout << std::endl;

		auto castArr = std::any_cast<std::shared_ptr<uint16_t[]>>(copyArr);
		PythonModule::GetInstance()->CopyArrToPy((void*)castArr.get(), 3, dims, "CopyArr", ArrType::arrUSHORT);
		PythonModule::GetInstance()->ExecuteString("print(CopyArr.shape)");
		PythonModule::GetInstance()->ExecuteString("plt.imshow(CopyArr[105])");
		PythonModule::GetInstance()->ExecuteString("plt.show()");
		PythonModule::GetInstance()->ExecuteString("plt.close()");

		PythonModule::GetInstance()->ExecuteString("del TestPyArr");
		PythonModule::GetInstance()->ExecuteString("del CopyArr");
				
		castArr = nullptr;
		copyArr.reset();
		delete reader;				
	}
	std::cout << "Python test finished" << std::endl;
}