#pragma once

#include <any>
#include <memory>

#include <enum.h>

#include <QString>

#include <ITCPython.h>

#include "TCPython38Export.h"

namespace TC {
	class TCPython38_API PythonModule38 final : public ITCPython {
	public:
		typedef std::shared_ptr<PythonModule38> Pointer;
	private:
		PythonModule38();

	public:
		~PythonModule38();
		static auto GetInstance() -> Pointer;

		auto SetPythonHome(const QString& path) -> void override;
		auto GetPythonHome() -> QString override;

		auto InitPython() -> bool override;

		auto ExecuteString(const QString& code, int stringmode = PythonMode::PySingleInput) -> bool override;
		auto ExecutePyFile(const QString& script_path) -> bool override;

		auto CopyArrToPy(void* arr, int dim, unsigned dims[3], const QString& varName, int type, int targetType) -> bool override;
		auto CopyArrToCpp(std::any& arr, const QString& varName) -> std::tuple<QList<int>, std::string> override;

		auto ConvertType(const QString& varname, PythonArrType type) -> bool override;
		auto Reshape(const QString& varname, unsigned dims[3]) -> bool override;

		auto GetIntValue(const QString& varName) -> int override;
		auto GetDoubleValue(const QString& varName) -> double override;
		auto GetIntArray(const QString& varName) -> QList<int> override;
		auto GetDoubleArray(const QString& varName) -> QList<double> override;

		auto TurnOffPython() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
