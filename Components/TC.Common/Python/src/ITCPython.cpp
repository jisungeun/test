#include "ITCPython.h"

namespace TC {
	auto ITCPython::copyBuffer(std::any& anyPtr, void* source, std::string typeString, unsigned dimSize) -> bool {
		if (typeString == "uint16") {
			auto rawData = std::shared_ptr<uint16_t[]>(new uint16_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint16_t) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "uint8") {
			auto rawData = std::shared_ptr<uint8_t[]>(new uint8_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint8_t) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "int8") {
			auto rawData = std::shared_ptr<int8_t[]>(new int8_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(int8_t) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "float64") {
			auto rawData = std::shared_ptr<double[]>(new double[dimSize]);
			memcpy(rawData.get(), source, sizeof(double) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "float32") {
			auto rawData = std::shared_ptr<float[]>(new float[dimSize]);
			memcpy(rawData.get(), source, sizeof(float) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "int32") {
			auto rawData = std::shared_ptr<int[]>(new int[dimSize]);
			memcpy(rawData.get(), source, sizeof(int) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "int16") {
			auto rawData = std::shared_ptr<short[]>(new short[dimSize]);
			memcpy(rawData.get(), source, sizeof(short) * dimSize);
			anyPtr = rawData;
		} else if (typeString == "uint32") {
			auto rawData = std::shared_ptr<uint32_t[]>(new uint32_t[dimSize]);
			memcpy(rawData.get(), source, sizeof(uint32_t) * dimSize);
			anyPtr = rawData;
		} else {
			return false;
		}
		return true;
	}
}
