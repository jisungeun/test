#include <QApplication>
#include <QTextStream>
#include <QFile>

#include "mainwindow.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	app.setApplicationName("KeyChain Tool");
	app.setOrganizationName("Tomocube, Inc.");
	app.setOrganizationDomain("www.tomocube.com");

	QFile f(":qdarkstyle/style.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

	MainWindow w;
	w.show();

	return app.exec();
}
