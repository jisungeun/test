#pragma once
#include <memory>
#include <QString>

class MainWindowControl {
public:
    MainWindowControl();
    virtual ~MainWindowControl();

    auto SetDatabase(const QString& path)->void;
    auto Add(const QString& account, const QString& password)->bool;
    auto Check(const QString& account, const QString& password)->bool;
    auto Delete(const QString& account)->bool;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};