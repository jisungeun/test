#pragma once
#include <memory>

#include <QMainWindow>

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    virtual ~MainWindow();

protected slots:
    void onQuit();
    void onOpen();
    void onAdd();
    void onCheck();
    void onDelete();

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};