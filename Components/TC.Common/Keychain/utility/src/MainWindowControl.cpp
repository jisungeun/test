#include <TCKeyChain.h>
#include "MainWindowControl.h"

using namespace TC::KeyChain;

struct MainWindowControl::Impl {
    QString path;
};

MainWindowControl::MainWindowControl() : d{std::make_unique<Impl>()} {
}

MainWindowControl::~MainWindowControl() {
}

auto MainWindowControl::SetDatabase(const QString& path) -> void {
    d->path = path;
}

auto MainWindowControl::Add(const QString& account, const QString& password) -> bool {
    if (d->path.isEmpty()) return false;
    KeyChain keyChain("Test", d->path);
    return keyChain.Write(account, password);
}

auto MainWindowControl::Check(const QString& account, const QString& password) -> bool {
    if (d->path.isEmpty()) return false;
    KeyChain keyChain("Test", d->path);
    return keyChain.Check(account, password);
}

auto MainWindowControl::Delete(const QString& account) -> bool {
    if (d->path.isEmpty()) return false;
    KeyChain keyChain("Test", d->path);
    return keyChain.Delete(account);
}

