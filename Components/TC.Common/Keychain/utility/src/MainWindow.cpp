#include <QMessageBox>
#include <QFileDialog>

#include "MainWindowControl.h"
#include "ui_MainWindow.h"
#include "MainWindow.h"

struct MainWindow::Impl { 
    Ui::MainWindow ui;
    MainWindowControl control;
};

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>()} {
    d->ui.setupUi(this);

    setWindowTitle("KeyChain Tool");

    connect(d->ui.actionQuit, SIGNAL(triggered()), this, SLOT(onQuit()));
    connect(d->ui.actionOpen, SIGNAL(triggered()), this, SLOT(onOpen()));
    connect(d->ui.addBtn, SIGNAL(clicked()), this, SLOT(onAdd()));
    connect(d->ui.checkBtn, SIGNAL(clicked()), this, SLOT(onCheck()));
    connect(d->ui.deleteBtn, SIGNAL(clicked()), this, SLOT(onDelete()));
}

MainWindow::~MainWindow() {
}

void MainWindow::onQuit() {
    qApp->quit();
}

void MainWindow::onOpen() {
    auto path = QFileDialog::getSaveFileName(this, "User Database");
    if (path.isEmpty()) return;

    d->control.SetDatabase(path);
}

void MainWindow::onAdd() {
    if (!d->control.Add(d->ui.addAccountEdit->text(), d->ui.addPasswordEdit->text())) {
        QMessageBox::warning(this, "Add", "Failed to add an account");
    } else {
        QMessageBox::information(this, "Add", QString("%1 is added").arg(d->ui.addAccountEdit->text()));
    }
}

void MainWindow::onCheck() {
    if (!d->control.Check(d->ui.checkAccountEdit->text(), d->ui.checkPasswordEdit->text())) {
        QMessageBox::warning(this, "Check", "Failed to check an account");
    } else {
        QMessageBox::information(this, "Check", "Valid");
    }
}

void MainWindow::onDelete() {
    if (!d->control.Delete(d->ui.deleteAccountEdit->text())) {
        QMessageBox::warning(this, "Delete", "Failed to delete an account");
    } else {
        QMessageBox::information(this, "Delete", QString("%1 is deleted").arg(d->ui.deleteAccountEdit->text()));
    }
}

