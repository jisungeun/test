#include <QCoreApplication>
#include <QSettings>

#include <qt5keychain/keychain.h>

#include "TCKeyChain.h"

namespace TC::KeyChain {
    struct KeyChain::Impl {
        QString service;
        QString filePath;
    };

    KeyChain::KeyChain(const QString& service, const QString& filePath) : d{std::make_unique<Impl>()} {
        d->service = service;
        d->filePath = filePath;
    }

    KeyChain::~KeyChain() {
    }

    auto KeyChain::Write(const QString& accout, const QString& password) -> bool {
        QKeychain::WritePasswordJob job(d->service.toLatin1());
        job.setAutoDelete(false);
        job.setKey(accout);
        job.setTextData(password);

        QSettings qs(d->filePath, QSettings::Format::IniFormat);
        if (!d->filePath.isEmpty()) job.setSettings(&qs);

        //QEventLoop loop;
        //connect(&job, SIGNAL(finished(QKeychain::Job*)), &loop, SLOT(quit()));

        job.start();
        //loop.exec();
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 5000);

        if (job.error()) {
            return false;
        }

        return true;
    }

    auto KeyChain::Check(const QString& accout, const QString& password) -> bool {
        QKeychain::ReadPasswordJob job(d->service.toLatin1());
        job.setAutoDelete(false);
        job.setKey(accout);

        QSettings qs(d->filePath, QSettings::Format::IniFormat);
        if (!d->filePath.isEmpty()) job.setSettings(&qs);

        //QEventLoop loop;
        //connect(&job, SIGNAL(finished(QKeychain::Job*)), &loop, SLOT(quit()));

        job.start();
        //loop.exec();
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 5000);

        auto pw = job.textData();
        if (job.error()) {
            return false;
        }

        return pw == password;
    }

    auto KeyChain::Delete(const QString& account) -> bool {
        QKeychain::DeletePasswordJob job(d->service.toLatin1());
        job.setAutoDelete(false);
        job.setKey(account);

        QSettings qs(d->filePath, QSettings::Format::IniFormat);
        if (!d->filePath.isEmpty()) job.setSettings(&qs);

        //QEventLoop loop;
        //connect(&job, SIGNAL(finished(QKeychain::Job*)), &loop, SLOT(quit()));

        job.start();
        //loop.exec();
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 5000);

        if (job.error()) {
            return false;
        }

        return true;
    }
}
