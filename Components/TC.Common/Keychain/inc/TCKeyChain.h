#pragma once
#include <memory>
#include <QObject>
#include <QString>

#include "TCKeyChainExport.h"

namespace QKeychain {
    class Job;
}

namespace TC::KeyChain {
    class TCKeyChain_API KeyChain : public QObject {
        Q_OBJECT

    public:
        KeyChain(const QString& service = QString("TCKeyChain"), const QString& filePath=QString());
        virtual ~KeyChain();

        auto Write(const QString& accout, const QString& password)->bool;
        auto Check(const QString& accout, const QString& password)->bool;
        auto Delete(const QString& account)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}