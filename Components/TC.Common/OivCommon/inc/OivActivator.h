#pragma once

#include <memory>
#include <QStringList>
#include "TCOivCommonExport.h"

class TCOivCommon_API OivActivator final {
public:
	typedef std::shared_ptr<OivActivator> Pointer;

public:
	OivActivator(const OivActivator&) = delete;
	~OivActivator();

	static auto Activate(const QStringList& keys)->void;
	static auto Convert(const QStringList& imageDevKeys)->QString;

protected:
	OivActivator();
};