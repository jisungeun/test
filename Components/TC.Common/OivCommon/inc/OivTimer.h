/*=======================================================================
** VSG_COPYRIGHT_TAG
**=======================================================================*/
/*=======================================================================
** Author      : Benjamin Grange (MMM YYYY)
**=======================================================================*/

#pragma once

#include <Inventor/sensors/SoSensor.h>
#include <Inventor/sensors/SoSystemTimer.h>
#include <Inventor/misc/SoRef.h>

#include <QObject>
#include <QTimer>

#include "TCOivCommonExport.h"

/**
 * @ingroup ViewerComponentsQt
 */
class TCOivCommon_API OivTimer : public QObject, public SoSystemTimer
{
	Q_OBJECT
public:
	using Pointer = std::shared_ptr<OivTimer>;

public:
	~OivTimer();
	OivTimer();

	static auto GetInstance()->OivTimer*;

	virtual void start();

	virtual void stop();

	virtual void setDelay(int time);

	virtual bool isPending() const;

	virtual void setRepeat(bool flag);

	virtual void setTask(SoSystemTimerTask* task);

private Q_SLOTS:
	void execCallback();
	void onStart();

Q_SIGNALS:
	void startRequired();
	void stopRequired();

private:
	QTimer timer;
	SoRef<SoSystemTimerTask> m_task;
	int m_delay;
};