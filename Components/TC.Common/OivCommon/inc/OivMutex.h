#pragma once
#include <memory>

#include "TCOivCommonExport.h"

namespace TC::IO {
    class TCOivCommon_API OIVMutex {
    public:
        typedef OIVMutex Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        virtual ~OIVMutex();
        static auto GetInstance(void)->Pointer;

        auto Lock()->void;
        auto Unlock()->void;

        auto GetMutex()->SbThreadMutex*;
    private:
        OIVMutex();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class TCOivCommon_API OIVMutexLocker {
    public:
        explicit OIVMutexLocker(OIVMutex::Pointer m);
        virtual ~OIVMutexLocker();

        auto Unlock()->void;
        auto Relock()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}