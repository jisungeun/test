#include <atomic>
#include <Inventor/threads/SbThreadMutex.h>

#include "OIVMutex.h"

namespace TC::IO {
    struct OIVMutex::Impl {
        SbThreadMutex* mutex{ nullptr };
    };
    OIVMutex::OIVMutex() : d{ new Impl } {
        d->mutex = new SbThreadMutex();
    }
    OIVMutex::~OIVMutex() {
        
    }
    auto OIVMutex::GetInstance() -> Pointer {
        static Pointer theInstance{ new OIVMutex() };
        return theInstance;
    }
    auto OIVMutex::Lock() -> void {
        d->mutex->lock();
    }
    auto OIVMutex::Unlock() -> void {
        d->mutex->unlock();
    }
    auto OIVMutex::GetMutex() -> SbThreadMutex* {
        return d->mutex;
    }
    struct OIVMutexLocker::Impl {
        OIVMutex::Pointer mutex{ nullptr };
        std::atomic<bool> locked;
    };

    OIVMutexLocker::OIVMutexLocker(OIVMutex::Pointer m) : d{ new Impl } {
        d->mutex = m;
        d->locked = false;

        if(d->mutex.get()) {
            d->mutex->Lock();
            d->locked = true;
        }
    }
    OIVMutexLocker::~OIVMutexLocker() {
        Unlock();
    }
    auto OIVMutexLocker::Unlock() -> void {
        if (d->mutex.get() && d->locked) {
            d->mutex->Unlock();
            d->locked = false;
        }
    }
    auto OIVMutexLocker::Relock() -> void {
        if(d->mutex.get() && false == d->locked) {
            d->mutex->Lock();
            d->locked = true;
        }
    }
}