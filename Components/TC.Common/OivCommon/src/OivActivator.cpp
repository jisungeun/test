#include <iostream>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbString.h>
#include <Inventor/lock/SoLockMgr.h>
#pragma warning(pop)

#include "OivActivator.h"

OivActivator::OivActivator() {}

OivActivator::~OivActivator() {
	SoLockManager::SetUnlockString(nullptr);
}

auto OivActivator::Convert(const QStringList& imageDevKeys) -> QString {
	QString decoded;

	auto decode = [](QString key)-> QString {
		QString str;

		auto inStr = key.toStdString();
		const auto len = key.length();
		for (int idx = 0; idx < len; idx++) {
			const auto ch = inStr.at(idx);
			const auto code = static_cast<int>(ch) - idx;
			str.append(static_cast<char>(code));
		}
		return str;
	};

	for (auto i = 0; i < imageDevKeys.count(); i++) {
		const auto key = imageDevKeys[i];
		decoded.append(decode(key));
		if (i < imageDevKeys.count() - 1) {
			decoded.append(" ");
		}
	}
	return decoded;
}

auto OivActivator::Activate(const QStringList& keys) -> void {
	static Pointer theInstance{ new OivActivator() };
	//for OIV 2023.~
	if (keys.count() < 1) {
		return;
	}
	const auto firstKey = keys.first();
	const char* firstsource = firstKey.toStdString().c_str();
	size_t firstlength = firstKey.length();
	std::vector<char> firstCharArray(firstsource, firstsource + firstlength);
	firstCharArray.push_back('\0');

	SoLockManager::SetUnlockString(firstCharArray.data());

	for (auto i = 1; i < keys.count(); i++) {
		const auto key = keys[i];
		const char* source = key.toStdString().c_str();
		size_t length = key.length();
		std::vector<char> destinationCharArray(source, source + length);
		destinationCharArray.push_back('\0');

		SoLockManager::AppendUnlockString(destinationCharArray.data());
	}
}
