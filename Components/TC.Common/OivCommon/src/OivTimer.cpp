/*=======================================================================
** VSG_COPYRIGHT_TAG
**=======================================================================*/

#include "OivTimer.h"

//------------------------------------------------------------------------------
OivTimer::OivTimer()
	: m_task(NULL), m_delay(0)
{
	connect(this, &OivTimer::startRequired, this, &OivTimer::onStart);
	connect(this, &OivTimer::stopRequired, &timer, &QTimer::stop);
}

OivTimer::~OivTimer() {
	
}

auto OivTimer::GetInstance() -> OivTimer* {
	//static Pointer theInstance{ new OivTimer() };
	static OivTimer* theInstance{ new OivTimer() };
	return theInstance;
}

//------------------------------------------------------------------------------
void
OivTimer::stop()
{
	// send a signal to execute in Qt GUI thread,
	// QTimer must be used in any thread that has an event loop
	emit stopRequired();
}

//------------------------------------------------------------------------------
void
OivTimer::start()
{
	// send a signal to execute in Qt GUI thread,
	// QTimer must be used in any thread that has an event loop
	emit startRequired();
}

//------------------------------------------------------------------------------
void
OivTimer::onStart()
{
	timer.start(m_delay);
}

//------------------------------------------------------------------------------
void
OivTimer::setDelay(int time)
{
	m_delay = time;
}

//------------------------------------------------------------------------------
bool
OivTimer::isPending() const
{
	return timer.isActive();
}

//------------------------------------------------------------------------------
void
OivTimer::setRepeat(bool flag)
{
	timer.setSingleShot(!flag);
}

//------------------------------------------------------------------------------
void
OivTimer::setTask(SoSystemTimerTask* task)
{
	m_task = task;
	connect(&timer, SIGNAL(timeout()), this, SLOT(execCallback()));
}

//------------------------------------------------------------------------------
void
OivTimer::execCallback()
{
	if (m_task.ptr() != NULL)
		m_task->run();
}
