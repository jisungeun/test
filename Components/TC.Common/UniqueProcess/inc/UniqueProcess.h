#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include <TCUniqueProcessExport.h>

namespace TC::UniqueProcess {
	class TCUniqueProcess_API UniqueProcess : public QObject {
		Q_OBJECT

	public:
		UniqueProcess(const QString& name, QObject* parent = nullptr);
		~UniqueProcess() override;

        /**
         * \return return true if other process already occupied the name.
         */
        auto IsExisting() -> bool;
        /**
         * \brief Registers this process to OS.
         * \param memoryLength Shared memory size. Parameter string length must be shorter than this.
         * \return return false if other process already occupied the name.
         */
        [[nodiscard]] auto Register(int memoryLength = 256) const -> bool;
        /**
         * \brief Sends parameter to the process occupying the name.
         * \return return false if failed sending.
         */
        auto SendToExisting(int argc, const char** argv) -> bool;
        /**
         * \brief Sends parameter to the process occupying the name.
         * \return return false if failed sending.
         */
		auto SendToExisting(const QStringList& argv) -> bool;

	protected slots:
		void OnConnected();

	signals:
		void Received(QStringList argv);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}