#include <QThread>
#include <QLocalSocket>
#include <QLocalServer>
#include <QSharedMemory>

#include "UniqueProcess.h"

#include <QtConcurrent/qtconcurrentrun.h>

constexpr char DELIMITER = ' ';

namespace TC::UniqueProcess {
	struct UniqueProcess::Impl {
		QString name;
		QLocalServer server;
		QSharedMemory memory;

		[[nodiscard]] auto SignalToServer() const -> bool {
			QLocalSocket socket;
			bool isChanged = false;

			connect(&socket, &QLocalSocket::stateChanged,
				[&isChanged]([[maybe_unused]] QLocalSocket::LocalSocketState error) {
					isChanged = true;
				}
			);

			socket.setServerName(name);
			socket.connectToServer();

			while (!isChanged)
				QThread::sleep(1);

			return (socket.state() == QLocalSocket::ConnectedState);
		}
	};

	UniqueProcess::UniqueProcess(const QString& name, QObject* parent) : QObject(parent), d(new Impl) {
		d->name = name;

		connect(&d->server, &QLocalServer::newConnection, this, &UniqueProcess::OnConnected);
	}

	UniqueProcess::~UniqueProcess() = default;

	auto UniqueProcess::IsExisting() -> bool {
		QSharedMemory memory(d->name, this);
		return memory.attach();
	}

	auto UniqueProcess::Register(int memoryLength) const -> bool {
		if (!d->memory.isAttached()) {
			d->memory.setKey(d->name);
			const auto memoryResult = d->memory.create(memoryLength);
			const auto pipeResult = d->server.listen(d->name);
			return memoryResult && pipeResult;
		}

		return false;
	}

	auto UniqueProcess::SendToExisting(int argc, const char** argv) -> bool {
		QSharedMemory memory(d->name, this);
		if (!memory.attach())
			return false;

		auto* data = static_cast<char*>(memory.data());

		for (unsigned long long i = 0, p = 0; i < argc; i++) {
			const auto len = strlen(argv[i]) + 1;
			strcpy_s(data + p, len, argv[i]);
			data[p + len - 1] = DELIMITER;
			p += len;
		}

		return d->SignalToServer();
	}

	auto UniqueProcess::SendToExisting(const QStringList& argv) -> bool {
		QSharedMemory memory(d->name, this);
		if (!memory.attach())
			return false;

		auto* data = static_cast<char*>(memory.data());
		unsigned long long p = 0;

		for (const auto& arg : argv) {
			const auto str = arg.toStdString();
			const auto len = strlen(str.c_str()) + 1;
			strcpy_s(data + p, len, str.c_str());
			data[p + len - 1] = DELIMITER;
			p += len;
		}

		return d->SignalToServer();
	}

	void UniqueProcess::OnConnected() {
		const auto* data = static_cast<const char*>(d->memory.constData());
		const QString unicode(data);
		const auto trim = unicode.trimmed();
		emit Received(trim.split(DELIMITER));
	}
}
