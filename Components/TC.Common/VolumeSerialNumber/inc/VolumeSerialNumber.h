#pragma once

#include <optional>

#include <QString>

#include "TCVolumeSerialNumberExport.h"

namespace TC {
	class TCVolumeSerialNumber_API VolumeSerialNumber {
	public:
		static auto Read(char driveLetter = 'C')->std::optional<QString>;
	};
}