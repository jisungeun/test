#include <catch2/catch.hpp>

#include "VolumeSerialNumber.h"

/*
 *
 * Windows OS MUST BE INSTALLED ON DRIVE "C"!!
 *
 */

namespace RegistryJwtTest {
	TEST_CASE("VolumeSerialNumber Operation Test") {
		SECTION("Volume number cannot be null or empty") {
			const auto number = TC::VolumeSerialNumber::Read();

			REQUIRE(number);
			REQUIRE(!number->isEmpty());
		}

		SECTION("Volume number requires '-' in itself") {
			const auto number = TC::VolumeSerialNumber::Read();

			REQUIRE(number->contains('-'));
		}

		SECTION("Volume number would not be full of '0'") {
			const auto number = TC::VolumeSerialNumber::Read();

			REQUIRE(*number != "0000-0000");
		}
	}
}
