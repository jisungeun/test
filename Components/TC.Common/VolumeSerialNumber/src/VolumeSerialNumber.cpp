#include <Windows.h>

#include "VolumeSerialNumber.h"

namespace TC {
	auto VolumeSerialNumber::Read(char driveLetter) -> std::optional<QString> {
		const char ar[] = { driveLetter, ':', '\\', 0};

		if (DWORD serial; GetVolumeInformation(ar, nullptr, 0, &serial, nullptr, nullptr, nullptr, 0)) {
			const auto lhs = serial >> 0x10uL;
			const auto rhs = serial & 0xFFFFuL;

			return QString("%1-%2").arg(lhs, 4, 16, QLatin1Char('0')).arg(rhs, 4, 16, QLatin1Char('0')).toUpper();
		}

		return {};
	}
}
