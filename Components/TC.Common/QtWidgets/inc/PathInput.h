#pragma once

#include <QWidget>

#include <memory>


#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API PathInput : public QWidget {
        Q_OBJECT
    public:
        PathInput(QWidget* parent);
        ~PathInput();

        auto addExtension(QString ext)->void;
        auto setTypeName(QString tn)->void;
        auto clearExtension()->void;
        auto setDefaultPath(QString dp)->void;
        auto getCurrentPath()->QString;
        auto setFolderType(bool folder)->void;
        auto setName(QString n)->void;
        auto getName()->QString;

        auto setPath(QString path)->void;
        auto getPath(void)->QString;

    public slots:        
        void OnOpenPathClicked(void);
        void OnPathChanged(const QString& path);
    signals:
        void sigPathChanged(QString,QString);

    private:
        auto InitializePanel()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}