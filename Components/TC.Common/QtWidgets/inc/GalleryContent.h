#pragma once

#include <memory>

#include <QWidget>
#include <QPixmap>
#include <QStringList>

#include "GalleryLayer.h"
#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API GalleryContent final : public QObject {
		Q_OBJECT

	public:
		GalleryContent(QObject* parent = nullptr);
		~GalleryContent() override;
		GalleryContent(GalleryContent&&) noexcept;
		GalleryContent(const GalleryContent&);
		auto operator=(const GalleryContent&)->GalleryContent&;
		auto operator=(GalleryContent&&) noexcept -> GalleryContent&;

		auto SetVisibility(bool visibility) -> void;
		[[nodiscard]] auto GetVisibility() const -> bool;

		// Main Image
		auto SetPixmap(const QPixmap& pixmap) -> void;
		auto SetPixmap(QPixmap&& pixmap) -> void;
		[[nodiscard]] auto GetPixmap() const->QPixmap&;

		// Layers
		auto CreateLayer(const QString& name)->GalleryLayer*;
		auto RemoveLayer(const QString& name) -> void;

		[[nodiscard]] auto GetLayer(const QString& name) const->GalleryLayer*;
		[[nodiscard]] auto ContainsLayer(const QString& name) const -> bool;
		[[nodiscard]] auto GetLayerNames() const->QStringList;

	signals:
		void sigUpdated();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}