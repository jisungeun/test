#pragma once

#include <QWidget>
#include <memory>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API StrComboBox : public QWidget {
        Q_OBJECT
    public:
        explicit StrComboBox(QWidget* parent = 0,bool showBtn = false);
        ~StrComboBox() override;

        auto setCurrentIndex(int idx)->void;
        auto addItem(QString text)->void;
        auto currentIndex()->int;

        auto SetToolTip(QString tooltip)->void;
        auto SetToolTipDuration(int msec)->void;
        auto ShowApplyButton(bool show)->void;

    signals:
        void comboStr(QString,QString, int);
        void comboApply(QString,int);

    private slots:
        void OnIndexChanged(int);
        void OnApplyButton();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };    
}