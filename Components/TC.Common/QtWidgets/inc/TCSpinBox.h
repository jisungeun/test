#pragma once

#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QValidator>
#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API TCSpinBox : public QSpinBox {
        Q_OBJECT
    public:
        TCSpinBox(QWidget* parent = nullptr);
        ~TCSpinBox() override;

        auto leaveFocus()->void;

    signals:
        void returnPressed();

    protected:
        auto fixup(QString& str) const -> void override;
        auto validate(QString& input, int& pos) const -> QValidator::State override;
    };
}

namespace TC {
    class TCQtWidgets_API TCDoubleSpinBox : public QDoubleSpinBox {
        Q_OBJECT
    public:
        TCDoubleSpinBox(QWidget* parent = nullptr);
        ~TCDoubleSpinBox() override;

        auto leaveFocus()->void;

    signals:
        void returnPressed();

    protected:
        auto fixup(QString& str) const -> void override;
        auto validate(QString& input, int& pos) const -> QValidator::State override;
    };
}

namespace TC {
    class TCSpinBoxLineEdit : public QLineEdit {
        Q_OBJECT
    public:
        enum class SpinBoxType{ IntType, DoubleType };

        TCSpinBoxLineEdit(SpinBoxType spinBoxType, QWidget* parent);
        ~TCSpinBoxLineEdit() override;

    protected:
        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto focusInEvent(QFocusEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}