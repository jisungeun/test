#pragma once

#include <QDialog>

#include "TCQtWidgetsExport.h"

namespace TC::PathWidget {
    class TCQtWidgets_API PathInputDialog : public QDialog {
        Q_OBJECT

    public:
        explicit PathInputDialog(QWidget* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
        ~PathInputDialog() override;

        auto SetLabelText(const QString& text) -> void;
        auto SetText(const QString& text) -> void;
        auto SetOkButtonText(const QString& text) -> void;
        auto SetCancelButtonText(const QString& text) -> void;
        auto SetTextMaxLength(int length) -> void;

        [[nodiscard]] auto GetLabelText() const -> QString;
        [[nodiscard]] auto GetText() const -> QString;
        [[nodiscard]] auto GetOkButtonText() const -> QString;
        [[nodiscard]] auto GetCancelButtonText() const -> QString;
        [[nodiscard]] auto GetTextMaxLength() const -> int;

    signals:
        void textValueChanged(const QString& text);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}