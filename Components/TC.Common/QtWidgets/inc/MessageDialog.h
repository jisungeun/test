#pragma once

#include <memory>

#include "CustomDialog.h"

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API MessageDialog final : public CustomDialog{
	public:
		MessageDialog(QWidget* parent = nullptr);
		MessageDialog(const QString& content, QWidget* parent = nullptr);
		MessageDialog(const QString& title, const QString& content, QWidget* parent = nullptr);
		~MessageDialog() override;
		
		auto GetContent() const->QString;
		
		auto SetContent(const QString& content) -> void;

		static StandardButton information(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons = StandardButton::Ok, StandardButton defaultButton = StandardButton::NoButton);
		static StandardButton question(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons = StandardButtons(StandardButton::Yes | StandardButton::No), StandardButton defaultButton = StandardButton::NoButton);
		static StandardButton warning(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons = StandardButton::Ok, StandardButton defaultButton = StandardButton::NoButton);
		static StandardButton critical(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons = StandardButton::Ok, StandardButton defaultButton = StandardButton::NoButton);

	protected:
		auto eventFilter(QObject*, QEvent*) -> bool override;
		auto GetMinimumWidth() const -> int override;

	private:
        struct Impl;
		std::unique_ptr<Impl> d;
	};
}
