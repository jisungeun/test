#pragma once

#include <memory>
#include <QWidget>
#include <QPainter>
#include <QMouseEvent>

#include "TCQtWidgetsExport.h"

namespace TC {
    /**
     * \brief Min/Max 값을 나타내는 슬라이더
     */
    class TCQtWidgets_API RangeSlider : public QWidget
    {
        Q_OBJECT
        Q_ENUMS(RangeSliderTypes)

    public:
        enum Option {
            NoHandle = 0x0,
            LeftHandle = 0x1,
            RightHandle = 0x2,
            DoubleHandles = LeftHandle | RightHandle
        };
        
        Q_DECLARE_FLAGS(Options, Option)

    public:
        RangeSlider(QWidget* parent = Q_NULLPTR);
        RangeSlider(Qt::Orientation ori, Options t = DoubleHandles, QWidget* parent = Q_NULLPTR);
        virtual ~RangeSlider();

        auto minimumSizeHint() const->QSize override;

        auto Minimum() const->int;
        auto SetMinimum(int value)->void;

        auto Maximum() const->int;
        auto SetMaximum(int value)->void;

        auto LowerValue() const->int;
        auto SetLowerValue(int value)->void;

        auto UpperValue() const->int;
        auto SetUpperValue(int value)->void;
        
        auto SetRange(int minValue, int maxValue)->void;

    protected:
        auto paintEvent(QPaintEvent* event)->void override;
        auto mousePressEvent(QMouseEvent* aEvent)->void override;
        auto mouseMoveEvent(QMouseEvent* aEvent)->void override;
        auto mouseReleaseEvent(QMouseEvent* aEvent)->void override;
        auto changeEvent(QEvent* aEvent)->void override;

        auto firstHandleRect() const->QRectF;
        auto secondHandleRect() const->QRectF;
        auto handleRect(int aValue) const->QRectF;

    signals:
        void lowerValueChanged(int value);
        void upperValueChanged(int value);
        void rangeChanged(int minValue, int maxValue);

    public slots:
        void onSetLowerValue(int value);
        void onSetUpperValue(int value);
        void onSetMinimum(int value);
        void onSetMaximum(int value);

    private:
        Q_DISABLE_COPY(RangeSlider)
        
        auto validLength() const->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        
        Options type;
    };
}

Q_DECLARE_OPERATORS_FOR_FLAGS(TC::RangeSlider::Options)