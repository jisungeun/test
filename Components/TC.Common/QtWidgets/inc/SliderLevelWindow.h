#include <memory>

#include <QWidget>

namespace TC {
    class SliderLevelWindow : public QWidget {
        Q_OBJECT
    public:
        SliderLevelWindow(QWidget* parent =nullptr);
        virtual ~SliderLevelWindow();

        auto SetLevelMinMax(float min, float max)->void;
        auto SetMinMax(float min, float max)->void;

        auto GetMinMax()->std::tuple<float, float>;
        auto ToggleScale(bool isVisible)->void;

    signals:
        void rangeChanged(float, float);
            
    protected:
        virtual void update();
        void paintEvent(QPaintEvent* paintEvent) override;
        void mouseMoveEvent(QMouseEvent* mouseEvent) override;
        void mousePressEvent(QMouseEvent* mouseEvent) override;
        void mouseReleaseEvent(QMouseEvent* mouseEvent) override;
        void mouseDoubleClickEvent(QMouseEvent* mouseEvent) override;
        void resizeEvent(QResizeEvent* resizeEvent) override;
        void enterEvent(QEvent* enterEvent) override;
        void leaveEvent(QEvent* leaveEvent) override;
    private:        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}