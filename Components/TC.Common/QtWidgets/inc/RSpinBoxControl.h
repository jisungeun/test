#pragma once

#include <memory>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API RSpinBoxControl {
    public:
        RSpinBoxControl();
        ~RSpinBoxControl();

        [[nodiscard]] auto GetValue() const -> double;
        [[nodiscard]] auto GetResolution() const -> double;
        [[nodiscard]] auto GetRevised() const -> double;
        [[nodiscard]] auto GetRevised(double value) const -> double;

        auto SetResolution(double resolution) -> void;
        auto SetValue(double value) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}