#pragma once

#include <QTabWidget>
#include <QAction>

namespace TC {
    class HidableTabWidget : public QTabWidget
    {
        Q_OBJECT
    public:
        explicit HidableTabWidget(QWidget* parent = 0);
        QAction hideAction;

        auto showTab(bool show)->void;

    private slots:
        void onHideAction(bool checked);
        void onTabBarClicked();
    };
}