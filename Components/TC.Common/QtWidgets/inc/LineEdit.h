#pragma once

#include <memory>

#include <QLineEdit>

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API LineEdit : public QLineEdit {
	public:
		LineEdit(QWidget* parent = nullptr);
		~LineEdit() override;

		auto SetPlaceholderOpacity(double opacity)-> void;
		auto GetPlaceholderOpacity() const ->double;

	public:
		auto paintEvent(QPaintEvent* event)->void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}