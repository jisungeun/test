#pragma once

#include <memory>
#include <QDockWidget>
#include <QResizeEvent>
#include "TCQtWidgetsExport.h"

namespace TC {
    /**
     * \brief 마지막 Float 사이즈를 기억하는 DockWidget
     */
    class TCQtWidgets_API RetrieveDockWidget : public QDockWidget {
        Q_OBJECT

    public:
        explicit RetrieveDockWidget(QWidget* parent = nullptr);
        virtual ~RetrieveDockWidget();

        auto SetFloatingSize(const int& w, const int& h)->void;
        void closeEvent(QCloseEvent* event) override;        
    protected slots:
        void onTopLevelChanged(bool topLevel);        

    protected:
        virtual void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}