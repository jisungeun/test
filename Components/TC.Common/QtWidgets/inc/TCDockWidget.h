#pragma once

#include <memory>

#include <QDockWidget>
#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API TCDockWidget : public QDockWidget {
        Q_OBJECT
    public:
        explicit TCDockWidget(QString title, QWidget* parent = nullptr);
        explicit TCDockWidget(QWidget* parent = nullptr);
        ~TCDockWidget();            

        void closeEvent(QCloseEvent* event) override;
    
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}