#pragma once

#include <memory>

#include <QWidget>

#include "GalleryContent.h"
#include "TCQtWidgetsExport.h"

namespace TC {
	struct Range {
		int from = -1, to = -1;
	};

	class TCQtWidgets_API GalleryView : public QWidget {
		Q_OBJECT

	public:
		GalleryView(QWidget* parent = nullptr);
		~GalleryView() override;

		auto SetSpacing(int space) -> void;
		auto GetSpacing() const -> int;

		auto SetColumnCount(int count) -> void;
		auto GetColumnCount() const->int;

		auto SetAnimative(bool animative) -> void;
		auto IsAnimative() const->bool;

		auto ScrollToIndex(int index) -> void;
		auto ScrollToHeight(int px) -> void;

		auto GetVisibleIndex() const->Range;
		
		auto AddItem()->void;
		auto AddItems(int count)->void;
		auto InsertItem(int index)->void;
		auto InsertItems(int index, int count)->void;
		
		auto RemoveItemAt(int index) -> void;
		auto Clear() -> void;

		auto GetIndexOf(const GalleryContent* item) const -> int;
		auto GetCount() const -> int;
		auto GetItemAt(int index) const->GalleryContent*;

		auto AddDefaultLayer(const QString& name, const QPixmap& pixmap, bool visibility = true) const -> void;
		auto GetDefaultLayerNames() const->QStringList;
		auto RemoveDefaultLayer(const QString& name) -> void;

		auto InstallEventFilterOnScrollViewer(QObject* object) -> void;

		auto SetIndexChangeInterval(int ms) -> void;

	protected:
		auto resizeEvent(QResizeEvent*) -> void override;

	signals:
		auto ItemDoubleClicked(int index, QMouseEvent* event) -> void;
		auto ItemClicked(int index, QMouseEvent* event) -> void;
		auto ItemPressed(int index, bool pressed, QMouseEvent* event) -> void;
		auto ItemHovered(int index, bool hovered) -> void;

		auto VisibleIndexChanged(int from, int to) -> void;

	protected slots:
		auto OnAnimationValueChanged(const QVariant& value) -> void;
		auto OnScrollBarValueChanged(int value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}