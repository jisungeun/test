#pragma once

#include <memory>

#include <QDialog>
#include <QDialogButtonBox>
#include <qmessagebox.h>

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API CustomDialog : public QDialog {
	public:
		using StandardButton = QDialogButtonBox::StandardButton;
		using StandardButtons = QDialogButtonBox::StandardButtons;

		CustomDialog(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
		~CustomDialog() override;

		auto GetTitle() const->QString;
		auto GetContext() const->QWidget*;
		auto GetDialogButtonBox() const->QDialogButtonBox*;
		auto GetButton(StandardButton button) const->QPushButton*;
		auto IsClosable() const -> bool;

		auto SetTitle(const QString& title) -> void;
		auto SetContext(QWidget* context) -> void;
		auto SetClosable(bool closable) -> void;
		auto SetStandardButtons(StandardButtons buttons) -> void;
		auto SetDefaultButton(StandardButton button) -> void;

		auto GetLastClickedButton()->StandardButton;
		
	protected:
		auto mousePressEvent(QMouseEvent* event) -> void override;
		auto mouseReleaseEvent(QMouseEvent* event) -> void override;
		auto mouseMoveEvent(QMouseEvent* event) -> void override;
		auto eventFilter(QObject*, QEvent*) -> bool override;

		auto UpdateSize() -> void;
		virtual auto GetMinimumWidth() const -> int;

	private:
		auto setWindowTitle(const QString& title) = delete;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
