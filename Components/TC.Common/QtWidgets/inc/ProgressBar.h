#pragma once

#include <QWidget>

#include <memory>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API ProgressBar : public QWidget {
        Q_OBJECT
    public:
        ProgressBar(QWidget* parent);
        ~ProgressBar();
            
    signals:
        void signalAbort(QString);//request abort process with additional information

    protected slots:
        void OnAbortRequested(void);

    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;        
    };
}