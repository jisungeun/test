#pragma once

#include <memory>
#include <QFrame>
#include <QWidget>
#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API CollapsibleGroupBox : public QFrame {
		Q_OBJECT

    public:
        explicit CollapsibleGroupBox(const QString& text = "", bool expanded = false, QWidget* parent = nullptr);
        virtual ~CollapsibleGroupBox();

	    auto Text() const ->QString;
        void SetText(const QString&) const;

        bool IsCollapsed() const;
	    bool IsExpanded() const;
        void SetExpanded(bool);

        void Toggle();
        void Expand();
        void Collapse();

        auto Child() const ->QWidget*;
        auto TakeChild() const->QWidget*;
        void SetChild(QWidget*) const;

    signals:
        void toggled(bool);

    private slots:
        void onToggled(const QString&);

	private:
        void UpdateCaption() const;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
