#pragma once

#include <memory>

#include <QWidget>

#include "GalleryContent.h"

namespace TC {
	class GalleryPainter : public QWidget {
		Q_OBJECT

	public:
		GalleryPainter(QWidget* parent = nullptr);
		~GalleryPainter() override;

		auto SetSpacing(int spacing) -> void;
		auto GetSpacing() const -> int;

		auto SetColumnCount(int count) -> void;
		auto GetColumnCount() const -> int;

		auto SetVisibleIndex(std::pair<int, int> range) -> void;
		auto GetVisibleIndex() const->std::pair<int, int>;

		auto SetAnimative(bool animative) -> void;
		auto Animative() const->bool;

		auto Add(int size = 1) ->void;
		auto Insert(int index, int size = 1) ->void;
		auto RemoveAt(int index) -> void;
		auto Clear() -> void;

		auto IndexOf(const GalleryContent* content) const -> int;
		auto GetItemCount() const -> int;
		auto Contains(int index) const -> bool;
		auto Get(int index) const->GalleryContent*;

		auto GetItemSize() const -> int;
		auto GetItemPosition(int index) const->QPoint;

		auto SetGradientPosition(qreal pos, int width) -> void;
		auto PaintedAll() const -> bool;

		auto AddDefaultLayer(const QString& name, const QPixmap& pixmap, bool visibility = true) const -> void;
		auto GetDefaultLayerNames() const->QStringList;
		auto RemoveDefaultLayer(const QString& name) -> void;

	protected:
		auto paintEvent(QPaintEvent*) -> void override;
		auto resizeEvent(QResizeEvent*) -> void override;

		auto minimumSizeHint() const->QSize override;
		auto mousePressEvent(QMouseEvent* event) -> void override;
		auto mouseReleaseEvent(QMouseEvent* event) -> void override;
		auto mouseDoubleClickEvent(QMouseEvent* event) -> void override;
		auto mouseMoveEvent(QMouseEvent* event) -> void override;
		auto leaveEvent(QEvent* event) -> void override;

	signals:
		auto DoubleClicked(int index, QMouseEvent* event) -> void;
		auto Clicked(int index, QMouseEvent* event) -> void;
		auto Pressed(int index, bool pressed, QMouseEvent* event) -> void;
		auto Hovered(int index, bool hovered) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}