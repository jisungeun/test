#pragma once

#include <memory>

#include <QDoubleSpinBox>

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API RSpinBox : public QDoubleSpinBox {
	public:
		RSpinBox(double resolution = 1, QWidget* parent = nullptr);
		~RSpinBox() override;

		auto SetResolution(double resolution) -> void;
		auto SetRange(double min, double max) -> void;
		auto SetValue(double value) -> void;

		[[nodiscard]] auto GetValueOriginal() const -> double;
		[[nodiscard]] auto GetValue() const -> double;
		[[nodiscard]] auto GetValueDisplayed() const -> double;

	protected:
		auto stepBy(int steps) -> void override;

	private:
		using QDoubleSpinBox::setRange;
		using QDoubleSpinBox::setValue;
		using QDoubleSpinBox::value;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}