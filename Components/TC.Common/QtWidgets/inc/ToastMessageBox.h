#pragma once

#include <memory>
#include <QWidget>
#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API ToastMessageBox : public QWidget {
		Q_OBJECT

	    Q_PROPERTY(float opacity READ GetOpacity WRITE SetOpacity)

	public:
		ToastMessageBox(QWidget* parent = nullptr);
        ~ToastMessageBox() override;

		void Show(const QString& text, const int32_t& durationMSec = 3000);

    protected:
        void paintEvent(QPaintEvent* event);    // The background will be drawn through the redraw method

    private slots:
        void onHideAnimation() const;
		void onHide();

	private:
		auto GetOpacity(void) const ->float;
		void SetOpacity(const float& opacity);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
