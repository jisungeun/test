#pragma once

#include <memory>

#include <QObject>
#include <QPixmap>

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API GalleryLayer : public QObject {
		Q_OBJECT

	public:
		GalleryLayer(QObject* parent = nullptr);
		~GalleryLayer() override;
		GalleryLayer(GalleryLayer&&) noexcept;
		GalleryLayer(const GalleryLayer&);
		auto operator=(const GalleryLayer&)->GalleryLayer&;
		auto operator=(GalleryLayer&&) noexcept -> GalleryLayer&;

		[[nodiscard]] auto GetPixmap() const->QPixmap&;
		[[nodiscard]] auto GetVisibility() const -> bool;

		auto SetPixmap(const QPixmap& pixmap) -> void;
		auto SetPixmap(QPixmap&& pixmap) -> void;
		auto SetVisibility(bool visiblity) -> void;


	signals:
		void sigUpdated();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}