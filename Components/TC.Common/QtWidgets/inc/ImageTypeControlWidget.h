#pragma once

#include <memory>
#include <QWidget>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API ImageTypeControlWidget : public QWidget  {
        Q_OBJECT
        
    public:
        ImageTypeControlWidget(QWidget* parent=nullptr);
        ~ImageTypeControlWidget();

        void SetEnableButtons(bool ht, bool fl, bool bf) const;

        void SetCurrentType(const int& type) const;
        auto GetCurrentType() const ->int;

    signals:
        void modalityButtonToggled(int index);

    protected slots:
        void on_htButton_toggled(bool checked);
        void on_flButton_toggled(bool checked);
        void on_bfButton_toggled(bool checked);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}