#pragma once

#include <memory>

#include <QPushButton>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API Button : public QPushButton {
        Q_OBJECT
    public:
        explicit Button(QWidget* parent = nullptr);
        ~Button();

        auto AddIcon(const QString& fileName, QIcon::Mode mode = QIcon::Normal)->void;
        auto SetEnabled(bool enabled)->void;

    protected:
        void enterEvent(QEvent* event) override;
        void leaveEvent(QEvent* event) override;
        //void mousePressEvent(QMouseEvent* event) override;
        //void mouseReleaseEvent(QMouseEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}