#pragma once

#include <memory>

#include <QDoubleSpinBox>
#include <QWidgetAction>

#include "RangeSlider.h"
#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API SpinBoxAction : public QWidgetAction {
        Q_OBJECT
    public:
        SpinBoxAction(const QString& title);
        ~SpinBoxAction();

        auto spinBox()->RangeSlider*;
        auto setMinMax(int smin, int smax)->void;
        auto setLower(int lower, bool setSlider = true)->void;
        auto setUpper(int upper, bool setSlider = true)->void;

    signals:
        void sigMinChanged(double);
        void sigMaxChanged(double);

    protected slots:
        void OnSpinMinChanged(double val);
        void OnSpinMaxChanged(double val);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}