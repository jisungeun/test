#pragma once

#include <memory>

#include <Qvalidator>
#include <QtWidgets/QLineEdit>

#include "TCQtWidgetsExport.h"

namespace TC::PathWidget {
	class TCQtWidgets_API PathLineEdit : public QLineEdit {
	public:
		explicit PathLineEdit(QWidget* parent = nullptr);
		explicit PathLineEdit(const QString&, QWidget* parent = nullptr);
		~PathLineEdit() override;

		auto SetMaxLength(int length) -> void;
		[[nodiscard]] auto GetMaxLength() const -> int;

	private:
		using QLineEdit::setValidator;
		using QLineEdit::setMaxLength;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
