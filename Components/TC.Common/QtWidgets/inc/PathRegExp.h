#pragma once

#include <memory>

#include <QRegExpValidator>
#include <QRegExp>

#include "TCQtWidgetsExport.h"

namespace TC::PathWidget {
	class TCQtWidgets_API PathRegExp : public QRegExp {
	public:
		PathRegExp();
		~PathRegExp();

		auto SetMaxLength(int length) -> void;
		[[nodiscard]] auto GetMaxLength() const -> int;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	class TCQtWidgets_API PathRegExpValidator : public QRegExpValidator {
	public:
		explicit PathRegExpValidator(QObject* parent = nullptr);
		~PathRegExpValidator() override;

		auto SetMaxLength(int length) -> void;
		[[nodiscard]] auto GetMaxLength() const -> int;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}