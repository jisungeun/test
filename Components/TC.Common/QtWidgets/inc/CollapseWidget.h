#pragma once

#include <memory>
#include <QWidget>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API CollapseWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit CollapseWidget(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
        ~CollapseWidget();

        auto SetWidget(QWidget* widget, const QString& label)->void;        
        auto SetCollapsable(bool isCollapse)->void;

    protected slots:
        void OnCollapsed(bool);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}