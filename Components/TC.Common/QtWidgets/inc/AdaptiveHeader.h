#pragma once

#include <memory>

#include <QHeaderView>

#include "TCQtWidgetsExport.h"

namespace TC {    
    class TCQtWidgets_API AdaptiveHeader : public QHeaderView {
        Q_OBJECT
    public:
        explicit  AdaptiveHeader(Qt::Orientation ori, QWidget* parent = nullptr);
        ~AdaptiveHeader();

        QSize sectionSizeFromContents(int logicalIndex) const override;

        void paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const override;

    protected:
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}