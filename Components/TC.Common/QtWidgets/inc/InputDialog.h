#pragma once

#include <memory>

#include <QLineEdit>

#include "CustomDialog.h"

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API InputDialog final : public CustomDialog{
		Q_OBJECT

	public:
		enum class InputMode {
			TextInput,
			ComboInput,
			IntInput,
			DoubleInput
		};

		InputDialog(QWidget* parent = nullptr);
		InputDialog(const QString& content, QWidget* parent = nullptr);
		InputDialog(const QString& title, const QString& content, QWidget* parent = nullptr);
		~InputDialog() override;

		auto GetContent() const->QString;

		auto SetContent(const QString& content) -> void;

		auto setInputMode(InputMode mode) -> void;
		auto inputMode() const->InputMode;

		void setTextValue(const QString& text);
		QString textValue() const;

		void setTextEchoMode(QLineEdit::EchoMode mode);
		QLineEdit::EchoMode textEchoMode() const;

		void setComboBoxEditable(bool editable);
		bool isComboBoxEditable() const;

		void setComboBoxItems(const QStringList& items);
		QStringList comboBoxItems() const;

		void setIntValue(int value);
		int intValue() const;

		void setIntMinimum(int min);
		int intMinimum() const;

		void setIntMaximum(int max);
		int intMaximum() const;

		void setIntRange(int min, int max);

		void setIntStep(int step);
		int intStep() const;

		void setDoubleValue(double value);
		double doubleValue() const;

		void setDoubleMinimum(double min);
		double doubleMinimum() const;

		void setDoubleMaximum(double max);
		double doubleMaximum() const;

		void setDoubleRange(double min, double max);

		void setDoubleDecimals(int decimals);
		int doubleDecimals() const;

		void setDoubleStep(double step);
		double doubleStep() const;

		void setOkButtonText(const QString& text);
		QString okButtonText() const;

		void setCancelButtonText(const QString& text);
		QString cancelButtonText() const;

		static QString getText(QWidget* parent, const QString& title, const QString& label,
			QLineEdit::EchoMode echo = QLineEdit::Normal,
			const QString& text = QString(), bool* ok = nullptr,
			Qt::WindowFlags flags = Qt::WindowFlags(),
			Qt::InputMethodHints inputMethodHints = Qt::ImhNone);

		static QString getItem(QWidget* parent, const QString& title, const QString& label,
			const QStringList& items, int current = 0, bool editable = true,
			bool* ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags(),
			Qt::InputMethodHints inputMethodHints = Qt::ImhNone);

		static int getInt(QWidget* parent, const QString& title, const QString& label, int value = 0,
			int minValue = -2147483647, int maxValue = 2147483647,
			int step = 1, bool* ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

		static double getDouble(QWidget* parent, const QString& title, const QString& label,
			double value = 0, double minValue = -2147483647,
			double maxValue = 2147483647, int decimals = 1, bool* ok = nullptr,
			Qt::WindowFlags flags = Qt::WindowFlags());

		static double getDouble(QWidget* parent, const QString& title, const QString& label,
			double value, double minValue, double maxValue, int decimals, bool* ok,
			Qt::WindowFlags flags, double step);

		void done(int result) override;

	signals:
		void textValueChanged(const QString& text);
		void textValueSelected(const QString& text);
		void intValueChanged(int value);
		void intValueSelected(int value);
		void doubleValueChanged(double value);
		void doubleValueSelected(double value);

	protected:
		auto eventFilter(QObject*, QEvent*) -> bool override;
		auto GetMinimumWidth() const -> int override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
