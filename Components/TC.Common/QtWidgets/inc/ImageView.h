#pragma once
#include <memory>

#include <QGraphicsView>
#include <QAbstractGraphicsShapeItem>
#include <QImage>

#include "TCQtWidgetsExport.h"

namespace TC::Widgets {
    class TCQtWidgets_API ImageView : public QGraphicsView {
        Q_OBJECT
    public:
        ImageView(QWidget* parent = nullptr);
        virtual ~ImageView();

        auto ShowImage(const QImage& image, bool fit = false)->void;
        auto ClearImage()->void;

        auto AddGraphicsItem(QGraphicsItem* item)->void;
        auto ClearItems()->void;

        auto GetImageSize() const->QSize;
        auto GetGraphicsItemsAt(QPoint& point)->QList<QGraphicsItem*>;
        auto GetGraphicsItemsAll()->QList<QGraphicsItem*>;

        auto DrawOnPainter(QPainter& painter)->void;

        auto EnableBoxDrawing(bool bEnable)->void;

        auto AdjustZoom(const double factor)->void;
        auto FitZoom()->void;

    protected:
        bool eventFilter(QObject* object, QEvent* event) override;

    signals:
        void mouseClicked(QPoint point);
        void rectSelected(QRectF rect);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
