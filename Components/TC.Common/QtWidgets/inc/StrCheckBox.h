#pragma once

#include <QCheckBox>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API StrCheckBox : public QCheckBox {
        Q_OBJECT
    public:
        explicit StrCheckBox(QWidget* parent = 0);

    signals:
        void checkedStr(QString,int);

    private slots:
        void OnStateChanged(int);
    };
}