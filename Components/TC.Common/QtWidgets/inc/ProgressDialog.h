#pragma once

#include <memory>

#include <QProgressBar>

#include "CustomDialog.h"

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API ProgressDialog final : public CustomDialog{
		Q_OBJECT

	public:
		ProgressDialog(QWidget * parent = nullptr);
		ProgressDialog(const QString& content, QWidget* parent = nullptr);
		ProgressDialog(const QString& content, const QString& cancelButtonText, int minimum, int maximum, QWidget* parent = nullptr);
		~ProgressDialog() override;

		auto GetContent() const->QString;
		auto GetBar() const->QProgressBar*;

		auto SetContent(const QString& content) -> void;

		auto cancel() -> void;
		auto reset() -> void;

		auto setMaximum(int maximum) -> void;
		auto setMinimum(int minimum) -> void;
		auto setRange(int minimum, int maximum) -> void;
		auto setValue(int progress) -> void;

		auto wasCanceled() const -> bool;
		auto minimum() const -> int;
		auto maximum() const -> int;
		auto value() const -> int;

		auto setAutoReset(bool reset) -> void;
		auto autoReset() const -> bool;
		auto setAutoClose(bool close) -> void;
		auto autoClose() const -> bool;

	protected:
		auto GetMinimumWidth() const -> int override;

	signals:
		auto canceled() -> void;

	protected slots:
		auto OnProgressValueChanged(int value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}