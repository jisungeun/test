#pragma once

#pragma warning(disable:4996)

#include <memory>
#include <QGraphicsView>
#include "TCQtWidgetsExport.h"

namespace TC {
    /**
     * \brief 2D 이미지를 보여주는 Qt Widget으로 다음 기능들을 제공함
     * - 이미지 표시
     * - Ctrl+Wheel : Zoom In/Out
     * - 우측 마우스 버튼 더블 클릭 : Zoom Fit
     * - 왼쪽 마우스 버튼 클릭 : 현재 위치 알림 (mouseClicked 시그널 발생)
     * - 왼쪽 마우스 버튼 더블 클릭 : 현재 위치 알림 (mouseDoubleClicked 시그널 발생)
     */
    class TCQtWidgets_API SimpleImageView : public QGraphicsView {
        Q_OBJECT

    public:
        explicit SimpleImageView(QWidget* parent = nullptr);
        virtual ~SimpleImageView();

        /**
         * \brief 뷰에 보여질 이미지 지정
         * \param image Image
         * \param bFitForce 이미지 크기를 뷰 크기에 맞춰서 보여줄지 여부 설정
         */
        auto ShowImage(const QImage& image, bool bFitForce = false)->void;

        /**
         * \brief 이미지를 제외한 모든 내용 지우기
         */
        auto ClearItems()->void;

        /**
         * \brief Graphics Item 추가하기
         * \param item graphics item
         */
        auto AddGraphicsItem(QGraphicsItem* item)->void;

        /**
         * \brief Scene position 기준으로 해당 위치에 있는 graphics item들을 반환
         * \param point scene position
         * \return graphics item 리스트
         */
        auto GraphicsItemsAt(QPoint& point)->QList<QGraphicsItem*>;

        auto Fit()->void;

    signals:
        /**
         * \brief View에서 마우스가 클릭된 위치를 반환
         * \param point Image 좌표계로 위치를 나타냄
         */
        void mouseClicked(QPoint point);
        void mouseDoubleClicked(QPoint point);

    protected:
        auto SetZoom(const double factor)->void;
        auto SetZoomFit(void)->void;

    private:
        auto eventFilter(QObject* object, QEvent* event)->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}