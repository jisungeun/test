#pragma once

#include <memory>
#include <QWidget>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API TimelapseControlWidget : public QWidget  {
        Q_OBJECT
        
    public:
        TimelapseControlWidget(QWidget* parent=nullptr);
        ~TimelapseControlWidget();

        void Init(const int& step) const;

    signals:
        void timelapseChanged(int index);

    protected slots:
        void on_progressSlider_valueChanged(int value);
        void on_prevButton_clicked() const;
        void on_nextButton_clicked() const;

    private:
        auto GetStepText(const int& currentIndex) const ->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}