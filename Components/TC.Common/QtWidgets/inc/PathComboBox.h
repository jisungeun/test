#pragma once

#include <QComboBox>

#include "TCQtWidgetsExport.h"

namespace TC::PathWidget {
	class TCQtWidgets_API PathComboBox : public QComboBox {
	public:
		explicit PathComboBox(QWidget* parent = nullptr);
		~PathComboBox() override;

		auto SetMaxLength(int length) -> void;
		[[nodiscard]] auto GetMaxLength() const -> int;

	private:
		using QComboBox::setValidator;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}