#pragma once

#include <memory>

#include <QLayout>

#include "TCQtWidgetsExport.h"

namespace TC {
	class TCQtWidgets_API FlowLayout : public QLayout {
	public:
		explicit FlowLayout(QWidget* parent = nullptr);
		~FlowLayout() override;

		auto SetItemCountOnRow(int count) -> void;
		[[nodiscard]] auto GetItemCountOnRow() const -> int;

		auto InsertWidget(int index, QWidget* widget) -> void;

		[[nodiscard]] auto GetItemSize(bool withSpacing = false) const->int;
		
	protected:
		auto addItem(QLayoutItem* item) -> void override;
		auto sizeHint() const->QSize override;
		auto setGeometry(const QRect& r) -> void override;
		auto takeAt(int index) ->QLayoutItem* override;
		auto count() const -> int override;
		auto itemAt(int index) const->QLayoutItem* override;

		auto minimumSize() const-> QSize override;
		auto hasHeightForWidth() const -> bool override;
		auto heightForWidth(int width) const -> int override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}