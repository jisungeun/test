#pragma once

#include <memory>
#include <QWidget>
#include <QVariant>
#include "TCQtWidgetsExport.h"

class QListWidgetItem;

namespace TC {
    /**
     * \brief QListWidget에 있는 각 Item 들이 Status 에 따라 Icon 등의 특성이 변경되도록 구현한 ListWidget
     */
    class TCQtWidgets_API StatusListWidget : public QWidget {
        Q_OBJECT

    public:
        typedef int Status;
        typedef int Index;

    public:
        StatusListWidget(QWidget* parent = nullptr);
        virtual ~StatusListWidget();

        /**
         * \brief Status 별로 Icon 지정
         * \param status Status
         * \param icon Icon
         */
        auto SetStatusIcon(Status status, const QIcon& icon)->void;
        /**
         * \brief Status 별로 tooltip 지정
         * \param status Status
         * \param tooltip Tooltip message
         */
        auto SetStatusTooltip(Status status, const QString& tooltip)->void;
        /**
         * \brief Status 별로 brush 지정
         * \param status Status
         * \param brush Brush message
         */
        auto SetBackground(Status status, const QBrush& brush)->void;

        /**
         * \brief 연속으로 AddItem을 많이 호출할 때 사용하면 화면이 Refresh 되는 것을 방지할 수 있음
         */
        auto BeginUpdate()->void;
        /**
         * \brief 화면 업데이트를 다시 시작하도록 함
         */
        auto EndUpdate()->void;

        /**
         * \brief 새로운 아이템 추가
         * \param label 새로운 아이템에 보여줄 텍스트
         * \param status 해당 아이템의 초기 Status
         * \return 이 아이템의 Index
         */
        auto AddItem(const QString& label, Status status = -1)->Index;
        /**
         * \brief 모든 아이템 삭제 (Status 설정은 유지됨)
         */
        auto ClearItems()->void;

        /**
         * \brief 사용자 데이타 저장
         * \param index Index
         * \param userData data
         */
        auto SetUserData(Index index, const QVariant userData)->void;
        /**
         * \brief 사용자 데이타 얻기
         * \param index Index
         * \return data as QVariant
         */
        auto GetUserData(Index index) const->QVariant;

        /**
         * \brief 특정 Item의 Status를 변경함
         * \param index 아이템의 Index
         * \param status Status
         */
        auto UpdateStatus(Index index, Status status)->void;
        /**
         * \brief 특정 label을 갖는 Item의 Status를 변경함
         * \param label Label
         * \param status Status
         */
        auto UpdateStatus(const QString& label, Status status)->void;

        /**
         * \brief 특정 아이템의 Status 얻기
         * \param index Index
         * \return Status
         */
        auto GetStatus(Index index)->Status;
        /**
         * \brief 특정 아이템의 Text 얻기
         * \param index Index
         * \return Text
         */
        auto GetText(Index index)->QString;

        /**
         * \brief 현재 선택된 아이템의 Index 얻기
         * \return Index
         */
        auto Selected() const->Index;
        /**
         * \brief 현재 선택된 모든 아이템들의 Index 얻기
         * \return QList<Index>
         */
        auto SelectedItems() const->QList<Index>;

    signals:
        void itemClicked(int index);
        void itemDoubleClicked(int index);

    protected slots:
        void onItemClicked(QListWidgetItem* item);
        void onItemDoubleClicked(QListWidgetItem* item);
        void onSelectionChanged();

    private:
        auto UpdateItem(QListWidgetItem* item, Status status)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}