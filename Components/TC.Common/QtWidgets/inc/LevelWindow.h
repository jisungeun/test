#include <memory>

#include <QWidget>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API LevelWindow : public QWidget {
        Q_OBJECT
    public:
        LevelWindow(QWidget* parent = nullptr);
        virtual ~LevelWindow();

        auto SetMinMax(float min, float max)->void;
        auto SetLevelMinMax(float min, float max)->void;

        auto GetMinMax()->std::tuple<float, float>;
        auto SetSpaceOnly(bool isEmptySpace)->void;

    signals:
        void levelChanged(float, float);

    protected slots:
        void onSliderChanged(float, float);
        void onMinSpinChanged(double);
        void onMaxSpinChanged(double);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}