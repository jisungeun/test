#pragma once
#include <memory>
#include <QPlainTextEdit>

#include "TCQtWidgetsExport.h"

namespace TC::Widgets {
    class TCQtWidgets_API LogText : public QPlainTextEdit {
        Q_OBJECT
    public:
        LogText(QWidget* parent = nullptr);
        virtual ~LogText();

        auto ClearBuffer()->void;

        auto SetColor(QColor color)->void;

        auto SetFollowTail(bool follow)->void;

        auto AppendLine(const QString& str)->void;

        auto Text() const->QString;

    protected:
        void scrollContentsBy(int dx, int dy) override;
        void wheelEvent(QWheelEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}