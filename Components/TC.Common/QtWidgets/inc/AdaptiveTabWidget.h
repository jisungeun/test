#pragma once

#include <memory>

#include <QTabWidget>
#include <QTabBar>
#include "TCQtWidgetsExport.h"

namespace TC {
    class TabBar : public QTabBar {
        Q_OBJECT
    public:
        explicit TabBar(QWidget* parent=nullptr);
    protected:
        QSize tabSizeHint(int index) const override;
        
    };

    class TCQtWidgets_API AdaptiveTabWidget : public QTabWidget {
        Q_OBJECT
    public:
        explicit AdaptiveTabWidget(QWidget* parent = nullptr);
        ~AdaptiveTabWidget();

        auto SetVisible(bool visible)->void;

        auto addTab(QWidget *page, const QString &label)->int32_t;
        auto addTab(QWidget *page, const QIcon &icon, const QString &label)->int32_t;

    protected:        

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}