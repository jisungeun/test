#pragma once

#include <QWidget>

#include <memory>

#include "TCQtWidgetsExport.h"

namespace TC {
    class TCQtWidgets_API ScalarInput : public QWidget {
        Q_OBJECT
    public:
        ScalarInput(QWidget* parent);
        ~ScalarInput();                

        auto setName(QString name)->void;
        auto setDecimals(int dec)->void;
        auto setSliderRange(int min, int max)->void;
        auto setValueRange(double min, double max)->void;
        auto setSingleStep(double step)->void;
        auto setValue(double val,bool blockSig = true)->void;
        auto getValue(void)->double;
        auto setUpper(double val)->void;
        auto setLower(double val)->void;

    public slots:        
        void OnSpinValueChanged(double);
        void OnSliderValueChanged(int);

    signals:
        void scalarChanged(double,QString);
    private:
        auto InitializePanel()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}