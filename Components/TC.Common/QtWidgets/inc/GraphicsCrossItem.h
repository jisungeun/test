#pragma once
#include <memory>

#include <QGraphicsItem>

#include "TCQtWidgetsExport.h"

namespace TC::Widgets {
    class TCQtWidgets_API GraphicsCrossItem : public QGraphicsItem {
    public:
	    explicit GraphicsCrossItem(QGraphicsItem *parent = nullptr);
	    ~GraphicsCrossItem(void) override;

	    auto SetCenter(int32_t x, int32_t y)->void;
		auto SetLength(int32_t thickness)->void;
		auto SetColor(const QColor& color)->void;
		auto SetThickness(int32_t thickness)->void;

	    auto boundingRect() const->QRectF override;
	    auto paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)->void override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}