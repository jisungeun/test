#include <iostream>

#include <QColor>
#include <QFrame>
#include <QGraphicsView>
#include <QPainter>
#include <QPushButton>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QImageReader>
#include <QScrollBar>

#include "GalleryContent.h"
#include "GalleryView.h"

int main(int argc, char** argv) {
	QApplication app(argc, argv);
	TC::GalleryView view;
	view.show();

	return app.exec();
}