#include <QVBoxLayout>
#include <QLabel>


#include "SpinBoxAction.h"

namespace TC {
    struct SpinBoxAction::Impl {
        RangeSlider* pRangeSlider{ nullptr };
        QDoubleSpinBox* min{ nullptr };
        QDoubleSpinBox* max{ nullptr };
        QWidget* pWidget{ nullptr };
    };
    SpinBoxAction::SpinBoxAction(const QString& title) : QWidgetAction(nullptr), d{ new Impl } {
        d->pWidget = new QWidget(nullptr);
        QVBoxLayout* pLayout = new QVBoxLayout();
        QLabel* pLabel = new QLabel(title);
        pLayout->addWidget(pLabel);
        d->pRangeSlider = new RangeSlider(nullptr);
        pLayout->addWidget(d->pRangeSlider);

        QHBoxLayout* hLayout = new QHBoxLayout;
        //min = new QLineEdit;
        d->min = new QDoubleSpinBox;
        d->min->setDecimals(4);
        d->min->setSingleStep(0.0001);
        connect(d->min, SIGNAL(valueChanged(double)), this, SLOT(OnSpinMinChanged(double)));

        //max = new QLineEdit;
        d->max = new QDoubleSpinBox;
        d->max->setDecimals(4);
        d->max->setSingleStep(0.0001);
        connect(d->max, SIGNAL(valueChanged(double)), this, SLOT(OnSpinMaxChanged(double)));

        QSpacerItem* spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Expanding);

        hLayout->addWidget(d->min);
        hLayout->addSpacerItem(spacer);
        hLayout->addWidget(d->max);

        pLayout->addLayout(hLayout);

        d->pWidget->setLayout(pLayout);
        d->pRangeSlider->setFixedSize(500, 20);
        setDefaultWidget(d->pWidget);
    }
    SpinBoxAction::~SpinBoxAction() = default;

    void SpinBoxAction::OnSpinMaxChanged(double val) {
        d->pRangeSlider->blockSignals(true);
        d->pRangeSlider->SetUpperValue(static_cast<int>(val * 10000.0));
        if (d->min->value() >= val) {
            d->min->blockSignals(true);
            d->min->setValue(val - 0.0001);
            d->min->blockSignals(false);
            d->pRangeSlider->SetLowerValue(static_cast<int>(val * 10000.0) - 1);
        }
        d->pRangeSlider->blockSignals(false);

        emit sigMaxChanged(val);
    }
    void SpinBoxAction::OnSpinMinChanged(double val) {
        d->pRangeSlider->blockSignals(true);
        d->pRangeSlider->SetLowerValue(static_cast<int>(val * 10000.0));
        if (d->max->value() <= val) {
            d->max->blockSignals(true);
            d->max->setValue(val + 0.0001);
            d->max->blockSignals(false);
            d->pRangeSlider->SetUpperValue(static_cast<int>(val * 10000.0) + 1);
        }
        d->pRangeSlider->blockSignals(false);

        emit sigMinChanged(val);
    }
    auto SpinBoxAction::spinBox() -> TC::RangeSlider* {
        return d->pRangeSlider;
    }

    auto SpinBoxAction::setMinMax(int smin, int smax) -> void {
        d->pRangeSlider->SetRange(smin, smax);
        d->min->setRange(static_cast<double>(smin) / 10000.0, static_cast<double>(smax) / 10000.0 - 0.0001);
        d->max->setRange(static_cast<double>(smin) / 10000.0 + 0.0001, static_cast<double>(smax) / 10000.0);
    }
    auto SpinBoxAction::setLower(int lower, bool setSlider) -> void {
        if (setSlider)
            d->pRangeSlider->SetLowerValue(lower);
        d->min->blockSignals(true);
        d->min->setValue(static_cast<double>(lower) / 10000.0);
        d->min->blockSignals(false);
        //min->setText(QString(std::to_string(lower).c_str()));
        //min->setText(QString::number(static_cast<double>(lower) / 10000.0, 'f', 4));
    }
    auto SpinBoxAction::setUpper(int upper, bool setSlider) -> void {
        if (setSlider) {
            d->pRangeSlider->SetUpperValue(upper);
        }
        d->max->blockSignals(true);
        d->max->setValue(static_cast<double>(upper) / 10000.0);
        d->max->blockSignals(false);
        //max->setText(QString(std::to_string(upper).c_str()));
        //max->setText(QString::number(static_cast<double>(upper) / 10000.0, 'f', 4));
    }
}