#include <iostream>

#include "ProgressBar.h"

#include "ui_ProgressBar.h"

namespace TC {
	struct ProgressBar::Impl {
		Ui::progressBar* ui{ nullptr };
	};

	ProgressBar::ProgressBar(QWidget* parent) : QWidget(parent),d{new Impl} {
		d->ui = new Ui::progressBar;
		d->ui->setupUi(this);

		Init();
	}
	ProgressBar::~ProgressBar() {
        
    }
	auto ProgressBar::Init() -> void {
		connect(d->ui->abortBtn, SIGNAL(clicked()), this, SLOT(OnAbortRequested()));
    }
	//slots
	void ProgressBar::OnAbortRequested() {
        
    }
}
