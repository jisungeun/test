#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QMouseEvent>
#include <QApplication>
#include <iostream>

#include "SimpleImageView.h"

namespace TC {
    class ImageItem : public QGraphicsPixmapItem
    {
    public:
        ImageItem(void) {
        }

        virtual ~ImageItem(void) {
        }

        auto ImageItem::setImage(const QImage& image)->void {
            if (image.isNull()) return;

            const auto aPixmap = QPixmap::fromImage(image);
            QBrush brush(aPixmap);

            setPixmap(aPixmap);
        }
    };

    struct SimpleImageView::Impl {
        const double zoomFactor{ 1.002 };
        QSize imageSize{ 256, 256 };

        QGraphicsScene* scene{ nullptr };
        ImageItem* imageItem{ nullptr };

        QPointF ptTargetViewport;
        QPointF ptTargetScene;
    };

    SimpleImageView::SimpleImageView(QWidget* parent) : QGraphicsView(parent), d{ new Impl } {
        d->scene = new QGraphicsScene(this);
        d->scene->setSceneRect(0, 0, d->imageSize.width(), d->imageSize.height());
        setScene(d->scene);

        d->imageItem = new ImageItem();
        d->scene->addItem(d->imageItem);

        viewport()->installEventFilter(this);
        setMouseTracking(true);
    }

    SimpleImageView::~SimpleImageView() {
    }

    auto SimpleImageView::ShowImage(const QImage& image, bool bFitForce) -> void {
        if (image.isNull()) return;

        if(d->imageSize != image.size()) {
            d->imageSize = image.size();
            d->scene->setSceneRect(0, 0, d->imageSize.width(), d->imageSize.height());
            SetZoomFit();
        }

        if(image.format() == QImage::Format_Indexed8) {
            QImage img = image.convertToFormat(QImage::Format_RGB32);
            d->imageItem->setImage(img);
        } else {
            d->imageItem->setImage(image);
        }

        if (bFitForce) SetZoomFit();
    }

    auto SimpleImageView::ClearItems() -> void {
        auto items = d->scene->items();
        for (auto item : items) {
            if (item == d->imageItem) continue;
            d->scene->removeItem(item);
            delete item;
        }
    }

    auto SimpleImageView::AddGraphicsItem(QGraphicsItem* item) -> void {
        d->scene->addItem(item);
    }

    auto SimpleImageView::GraphicsItemsAt(QPoint& point) -> QList<QGraphicsItem*> {
        return d->scene->items(point);
    }

    auto SimpleImageView::Fit()->void {
        fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
    }

    auto SimpleImageView::SetZoom(const double factor) -> void {
        scale(factor, factor);
        centerOn(d->ptTargetScene);

        const auto delta_viewport_pos = d->ptTargetViewport - QPointF(viewport()->width() / 2.0, viewport()->height() / 2.0);
        const auto viewport_center = mapFromScene(d->ptTargetScene) - delta_viewport_pos;
        centerOn(mapToScene(viewport_center.toPoint()));
    }

    auto SimpleImageView::SetZoomFit() -> void {
        QSize viewSize = size();
        double ratio = 1.0;
        if (viewSize.width() <= viewSize.height()) ratio = (viewSize.width() * 1.0f) / d->imageSize.width();
        else ratio = (viewSize.height() * 1.0f) / d->imageSize.height();

        QMatrix matrix;
        matrix.scale(ratio, ratio);
        setMatrix(matrix);        
    }

    auto SimpleImageView::eventFilter(QObject* object, QEvent* event) -> bool {
        if (event->type() == QEvent::MouseMove) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);
            QPointF delta = d->ptTargetViewport - mouseEvent->pos();
            if (qAbs(delta.x()) > 5 || qAbs(delta.y()) > 5) {
                d->ptTargetViewport = mouseEvent->pos();
                d->ptTargetScene = this->mapToScene(mouseEvent->pos());
            }
        } else if (event->type() == QEvent::Wheel) {
            auto wheelEvent = static_cast<QWheelEvent*>(event);
            if (QApplication::keyboardModifiers() == Qt::ControlModifier) {
                if (wheelEvent->orientation() == Qt::Vertical)
                {
                    double angle = wheelEvent->angleDelta().y();
                    const double factor = std::pow(d->zoomFactor, angle);
                    SetZoom(factor);
                    return true;
                }
            }
        } else if (event->type() == QEvent::MouseButtonPress) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::LeftButton) {
                QPointF clickPoint = this->mapToScene(mouseEvent->pos());
                emit mouseClicked(clickPoint.toPoint());
            }
        } else if (event->type() == QEvent::MouseButtonDblClick) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::RightButton) {
                SetZoomFit();
            } else if (mouseEvent->button() == Qt::LeftButton) {
                QPointF clickPoint = this->mapToScene(mouseEvent->pos());
                emit mouseDoubleClicked(clickPoint.toPoint());
            }
        }

        Q_UNUSED(object)
        return false;
    }
}
