#pragma warning(push)
#pragma warning(disable:4996)

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QMouseEvent>
#include <QKeyEvent>

#include "ImageView.h"

namespace TC::Widgets {
    class ImageItem : public QGraphicsPixmapItem {
    public:
        auto SetImage(const QImage& image)->void {
            auto pixmap = QPixmap::fromImage(image);
            setPixmap(pixmap);
        }

        auto Clear()->void {
            setPixmap(QPixmap());
        }
    };

    struct ImageView::Impl {
        QGraphicsScene* scene{ nullptr };
        QSize imageSize;
        ImageItem* imageItem;
        QPointF m_ptTargetViewport;
        QPointF m_ptTargetScene;
        double m_dZoomFactor{ 1.002 };
        bool firstUpdate{ true };
        bool boxDrawingMode{ false };
    };

    ImageView::ImageView(QWidget* parent) : QGraphicsView(parent), d{ new Impl } {
        d->scene = new QGraphicsScene(this);
        d->scene->setSceneRect(0, 0, 1024, 1024);
        setScene(d->scene);

        d->imageItem = new ImageItem;
        d->scene->addItem(d->imageItem);

        viewport()->installEventFilter(this);
        setMouseTracking(true);
    }

    ImageView::~ImageView() {
    }

    auto ImageView::ShowImage(const QImage& image, bool fit) -> void {
        if (image.isNull()) return;

        if (d->imageSize != image.size()) {
            d->imageSize = image.size();
            d->scene->setSceneRect(0, 0, d->imageSize.width(), d->imageSize.height());
            FitZoom();
        }

        if (image.format() == QImage::Format_Indexed8) {
            auto img = image.convertToFormat(QImage::Format_RGB32);
            d->imageItem->SetImage(img);
        } else {
            d->imageItem->SetImage(image);
        }

        if (fit || d->firstUpdate) {
            FitZoom();
            d->firstUpdate = false;
        }
    }

    auto ImageView::ClearImage() -> void {
        d->imageItem->Clear();
    }

    auto ImageView::AddGraphicsItem(QGraphicsItem* item) -> void {
        d->scene->addItem(item);
    }

    auto ImageView::ClearItems() -> void {
        auto items = d->scene->items();
        for (auto item : items) {
            if (item == d->imageItem) continue;
            d->scene->removeItem(item);
            delete item;
        }
    }

    auto ImageView::GetImageSize() const -> QSize {
        return d->imageSize;
    }

    auto ImageView::GetGraphicsItemsAt(QPoint& point) -> QList<QGraphicsItem*> {
        return d->scene->items(point);
    }

    auto ImageView::GetGraphicsItemsAll() -> QList<QGraphicsItem*> {
        return d->scene->items();
    }

    auto ImageView::DrawOnPainter(QPainter& painter) -> void {
        d->scene->render(&painter);
    }

    auto ImageView::EnableBoxDrawing(bool bEnable) -> void {
        d->boxDrawingMode = bEnable;
    }
    
    auto ImageView::AdjustZoom(const double factor) -> void {
        scale(factor, factor);
        centerOn(d->m_ptTargetScene);

        QPointF delta_viewport_pos = d->m_ptTargetViewport - QPointF(viewport()->width() / 2.0, viewport()->height() / 2.0);
        QPointF viewport_center = mapFromScene(d->m_ptTargetScene) - delta_viewport_pos;
        centerOn(mapToScene(viewport_center.toPoint()));
    }

    auto ImageView::FitZoom() -> void {
        QSize viewSize = size();
        double ratio = 1.0;
        if (viewSize.width() <= viewSize.height()) ratio = (viewSize.width() * 1.0f) / d->imageSize.width();
        else ratio = (viewSize.height() * 1.0f) / d->imageSize.height();

        QMatrix matrix;
        matrix.scale(ratio, ratio);
        setMatrix(matrix);
    }

    bool ImageView::eventFilter(QObject* object, QEvent* event) {
        static Qt::KeyboardModifiers ctrlKey{ Qt::ControlModifier };

		if (event->type() == QEvent::MouseMove) {
			const auto mouse_event = static_cast<QMouseEvent*>(event);
			QPointF delta = d->m_ptTargetViewport - mouse_event->pos();
			if (qAbs(delta.x()) > 5 || qAbs(delta.y()) > 5) {
                d->m_ptTargetViewport = mouse_event->pos();
                d->m_ptTargetScene = this->mapToScene(mouse_event->pos());
			}
        } else if (event->type() == QEvent::Wheel) {
            const auto wheel_event = static_cast<QWheelEvent*>(event);
            if (QApplication::keyboardModifiers() == ctrlKey) {
                if (wheel_event->orientation() == Qt::Vertical) {
                    double angle = wheel_event->angleDelta().y();
                    const double factor = std::pow(d->m_dZoomFactor, angle);
                    AdjustZoom(factor);
                    return true;
                }
            }
        } else if (event->type() == QEvent::MouseButtonPress) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);

            if (d->boxDrawingMode == false) {
                if (mouseEvent->button() == Qt::LeftButton) {
                    QPointF clickPoint = this->mapToScene(mouseEvent->pos());
                    emit mouseClicked(clickPoint.toPoint());
                }
            } else {
                if (mouseEvent->button() == Qt::LeftButton) {
                    setDragMode(DragMode::RubberBandDrag);
                }
            }
		} else if (event->type() == QEvent::MouseButtonDblClick) {
			const auto mouse_event = static_cast<QMouseEvent*> (event);

			if (mouse_event->button() == Qt::RightButton) {
				FitZoom();
			}
        } else if (event->type() == QEvent::MouseButtonRelease) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);

            if (d->boxDrawingMode) {
                if (mouseEvent->button() == Qt::LeftButton) {
                    auto rect = rubberBandRect();
                    d->boxDrawingMode = false;
                    setDragMode(DragMode::NoDrag);

                    auto rectScene = this->mapToScene(rect).boundingRect();

                    const auto x1 = rectScene.x() + rectScene.width();
                    const auto y1 = rectScene.y() + rectScene.height();

                    const auto& sceneRect = d->scene->sceneRect();
                    const auto sceneX1 = sceneRect.x() + sceneRect.width();
                    const auto sceneY1 = sceneRect.y() + sceneRect.height();

                    rectScene.setX(std::max(0.0, rectScene.x()));
                    rectScene.setY(std::max(0.0, rectScene.y()));
                    rectScene.setWidth(std::min(x1, sceneX1) - rectScene.x());
                    rectScene.setHeight(std::min(y1, sceneY1) - rectScene.y());

                    emit rectSelected(rectScene);
                }
            }
        }

		Q_UNUSED(object)
			return false;
    }
}

#pragma warning(pop)