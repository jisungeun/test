#include "StrCheckBox.h"

namespace TC{
    StrCheckBox::StrCheckBox(QWidget* parent)
        :QCheckBox(parent) {
        connect(this, SIGNAL(stateChanged(int)), this, SLOT(OnStateChanged(int)));
    }
    void StrCheckBox::OnStateChanged(int state) {
        auto text = toolTip();
        emit checkedStr(text, state);
    }

}