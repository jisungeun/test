#include <iostream>

#include "ScalarInput.h"

#include "ui_ScalarInputControl.h"

namespace TC{
    struct ScalarInput::Impl {
        Ui::ScalarUi* ui{ nullptr };        
        int sliderRange[2] = { 1,100 };
        float valueRange[2] = { 0.0,1.0 };
        int dec = 4;
        double singleStep = 0.0001;
        bool show_desc = false;

        bool isUpper{ false };
        bool isLower{ false };
        double upper{ 0.0 };
        double lower{ 0.0 };
        double  lastVal{ 0.0 };
        int lastSlider{ 0 };

        QString name;
    };
    ScalarInput::ScalarInput(QWidget *parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ScalarUi;
        d->ui->setupUi(this);                

        InitializePanel();
    }
    ScalarInput::~ScalarInput() {
        
    }    

    auto ScalarInput::setName(QString name) -> void {
        d->name = name;
    }
    auto ScalarInput::setUpper(double val)->void {
        d->upper = val;
        d->isUpper = true;
    }
    auto ScalarInput::setLower(double val)->void {
        d->lower = val;
        d->isLower = true;
    }


    auto ScalarInput::InitializePanel() -> void {
        d->ui->valueSlider->setRange(d->sliderRange[0], d->sliderRange[1]);
        d->ui->valueSlider->setSingleStep(1);

        d->ui->valueSpin->setRange(d->valueRange[0], d->valueRange[1]);
        d->ui->valueSpin->setDecimals(4);
        d->ui->valueSpin->setSingleStep(d->singleStep);

        connect(d->ui->valueSlider, SIGNAL(valueChanged(int)), this, SLOT(OnSliderValueChanged(int)));
        connect(d->ui->valueSpin, SIGNAL(valueChanged(double)), this, SLOT(OnSpinValueChanged(double)));    
    }

    auto ScalarInput::setDecimals(int dec) -> void {
        d->dec = dec;
        d->ui->valueSpin->setDecimals(dec);
    }

    auto ScalarInput::setValue(double val,bool blockSig)->void {
        if(blockSig){
            d->ui->valueSlider->blockSignals(true);
        }
        d->ui->valueSpin->setValue(val);
        if(blockSig){
            d->ui->valueSlider->blockSignals(false);
        }
    }

    auto ScalarInput::getValue() -> double {
        return d->ui->valueSpin->value();
    }

    auto ScalarInput::setSingleStep(double step) -> void {
        d->singleStep = step;
        d->ui->valueSpin->setSingleStep(step);
    }

    auto ScalarInput::setSliderRange(int min, int max) -> void {
        d->sliderRange[0] = min;
        d->sliderRange[1] = max;
        d->ui->valueSlider->blockSignals(true);
        d->ui->valueSlider->setRange(min, max);
        d->ui->valueSlider->blockSignals(false);
    }

    auto ScalarInput::setValueRange(double min, double max) -> void {
        d->valueRange[0] = min;
        d->valueRange[1] = max;
        d->ui->valueSpin->blockSignals(true);
        d->ui->valueSpin->setRange(min, max);        
        d->ui->valueSpin->blockSignals(false);
    }                

    void ScalarInput::OnSpinValueChanged(double v) {

        if(d->isLower) {
            if(v < d->lower) {
                d->ui->valueSpin->blockSignals(true);
                d->ui->valueSpin->setValue(d->lastVal);
                d->ui->valueSpin->blockSignals(false);
                return;
            }
        }

        if(d->isUpper) {
            if(v > d->upper) {
                d->ui->valueSpin->blockSignals(true);
                d->ui->valueSpin->setValue(d->lastVal);
                d->ui->valueSpin->blockSignals(false);
                return;
            }
        }

        auto srange = d->sliderRange[1] - d->sliderRange[0];
        auto ratio = (v-d->valueRange[0]) / (d->valueRange[1] - d->valueRange[0]);
        auto sval = (double)srange * ratio;

        d->ui->valueSlider->blockSignals(true);
        d->ui->valueSlider->setValue((int)sval);
        d->ui->valueSlider->blockSignals(false);

        d->lastVal = v;

        emit scalarChanged(v,d->name);
    }
    void ScalarInput::OnSliderValueChanged(int v) {
        auto vrange = d->valueRange[1] - d->valueRange[0];
        auto ratio = (double)v / (double)(d->sliderRange[1] - d->sliderRange[0]);
        auto vval = d->valueRange[0] +  vrange * ratio;

        if (d->isLower) {
            if (vval < d->lower) {
                d->ui->valueSlider->blockSignals(true);
                d->ui->valueSlider->setValue(d->lastSlider);
                d->ui->valueSlider->blockSignals(false);
                return;
            }
        }

        if (d->isUpper) {            
            if (vval > d->upper) {
                d->ui->valueSlider->blockSignals(true);
                d->ui->valueSlider->setValue(d->lastSlider);
                d->ui->valueSlider->blockSignals(false);
                return;
            }
        }

        d->ui->valueSpin->blockSignals(true);
        d->ui->valueSpin->setValue(vval);
        d->ui->valueSpin->blockSignals(false);        

        d->lastSlider = v;

        emit scalarChanged(vval,d->name);
    }
}
