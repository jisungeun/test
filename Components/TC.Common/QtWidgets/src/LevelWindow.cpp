#include <QVBoxLayout>
#include <QDoubleSpinBox>

#include "SliderLevelWindow.h"
#include "LevelWindow.h"

#define RI_FACTOR 10000.0
#define SINGLE_STEP 0.001

namespace TC {
    struct LevelWindow::Impl {
        SliderLevelWindow* slider{ nullptr };
        QDoubleSpinBox* minSpin{ nullptr };
        QDoubleSpinBox* maxSpin{ nullptr };
        float levelMin{ 0 };
        float levelMax{ 1 };
        float imgMin{ 0 };
        float imgMax{ 1 };
        auto CalcCeil(double val, int precision)->double;
        auto CalcFloor(double val, int precision)->double;
    };
    auto LevelWindow::Impl::CalcCeil(double val, int precision) -> double {
        double base = pow(10.0, precision);
        double result = val * base;
        result = ceil(result);
        result /= base;
        return result;
	}
    auto LevelWindow::Impl::CalcFloor(double val, int precision) -> double {
        double base = pow(10.0, precision);
        double result = val * base;
        result = floor(result);
        result /= base;
        return result;
	}
    LevelWindow::LevelWindow(QWidget* parent) : QWidget(parent), d{ new Impl } {        
        d->slider = new SliderLevelWindow();
        d->minSpin = new QDoubleSpinBox();
        d->minSpin->setMaximumWidth(50);
        d->minSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
        d->minSpin->setSingleStep(SINGLE_STEP);
        d->minSpin->setDecimals(3);
        d->maxSpin = new QDoubleSpinBox();
        d->maxSpin->setMaximumWidth(50);
        d->maxSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
        d->maxSpin->setSingleStep(SINGLE_STEP);
        d->maxSpin->setDecimals(3);

        auto layout = new QVBoxLayout;
        layout->addWidget(d->slider);        
        layout->addWidget(d->maxSpin);
        layout->addWidget(d->minSpin);

        this->setLayout(layout);

        connect(d->slider, SIGNAL(rangeChanged(float, float)), this, SLOT(onSliderChanged(float,float)));
        connect(d->minSpin, SIGNAL(valueChanged(double)), this, SLOT(onMinSpinChanged(double)));
        connect(d->maxSpin, SIGNAL(valueChanged(double)), this, SLOT(onMaxSpinChanged(double)));
    }
    LevelWindow::~LevelWindow() {
        
    }
    auto LevelWindow::SetMinMax(float min, float max)->void {
        d->imgMin = min;
        d->imgMax = max;
        d->levelMin = min + (max - min) / 7;
        d->levelMax = max - (max - min) / 7;

        d->slider->SetMinMax(d->imgMin, d->imgMax);
        d->slider->SetLevelMinMax(d->levelMin, d->levelMax);

        d->minSpin->blockSignals(true);
        d->minSpin->setRange(d->imgMin/ RI_FACTOR,d->levelMax/ RI_FACTOR);
        const auto finalMin = d->CalcCeil(d->levelMin / RI_FACTOR, 3);
        d->minSpin->setValue(finalMin);
        d->minSpin->blockSignals(false);

        d->maxSpin->blockSignals(true);
        d->maxSpin->setRange(d->levelMin/ RI_FACTOR, d->imgMax/ RI_FACTOR);
        const auto finalMax = d->CalcFloor(d->levelMax / RI_FACTOR, 3);
        d->maxSpin->setValue(finalMax);
        d->maxSpin->blockSignals(false);
    }
    auto LevelWindow::SetSpaceOnly(bool isEmptySpace) -> void {
        d->slider->setEnabled(!isEmptySpace);
        d->maxSpin->setEnabled(!isEmptySpace);
        d->minSpin->setEnabled(!isEmptySpace);
    }
    auto LevelWindow::SetLevelMinMax(float min,float max)->void {
        d->levelMin = min;
        d->levelMax = max;
        d->slider->SetLevelMinMax(d->levelMin, d->levelMax);

        d->minSpin->blockSignals(true);
        const auto finalMin = d->CalcCeil(min / RI_FACTOR, 3);
        d->minSpin->setValue(finalMin);
        d->minSpin->blockSignals(false);

        d->maxSpin->blockSignals(true);
        const auto finalMax = d->CalcFloor(max / RI_FACTOR, 3);
        d->maxSpin->setValue(finalMax);
        d->maxSpin->blockSignals(false);
    }
    auto LevelWindow::GetMinMax()->std::tuple<float, float> {
        return std::make_tuple(d->levelMin, d->levelMax);
    }

    //slots
    void LevelWindow::onSliderChanged(float min, float max) {
        d->minSpin->blockSignals(true);
        const auto finalMin = d->CalcCeil(min / RI_FACTOR, 3);
        d->minSpin->setValue(finalMin);
        d->minSpin->blockSignals(false);

        d->maxSpin->blockSignals(true);
        const auto finalMax = d->CalcFloor(max / RI_FACTOR, 3);
        d->maxSpin->setValue(finalMax);
        d->maxSpin->blockSignals(false);

        d->levelMin = min;
        d->levelMax = max;

        emit levelChanged(d->levelMin, d->levelMax);
    }

    void LevelWindow::onMinSpinChanged(double val) {
        auto curMaxVal = d->maxSpin->value();
        if(false == curMaxVal > val) {
            d->minSpin->blockSignals(true);
            d->minSpin->setValue(val - SINGLE_STEP);
            d->minSpin->blockSignals(false);
            return;
        }
        d->maxSpin->blockSignals(true);
        d->maxSpin->setRange(val, d->imgMax / RI_FACTOR);
        d->maxSpin->blockSignals(false);

        d->levelMin = val * RI_FACTOR;
        d->slider->SetLevelMinMax(d->levelMin, d->levelMax);

        emit levelChanged(d->levelMin, d->levelMax);
    }
    void LevelWindow::onMaxSpinChanged(double val) {
        auto curMinVal = d->minSpin->value();
        if(false == curMinVal < val) {
            d->maxSpin->blockSignals(true);
            d->maxSpin->setValue(val + SINGLE_STEP);
            d->maxSpin->blockSignals(false);
            return;
        }
        d->minSpin->blockSignals(true);
        d->minSpin->setRange(d->imgMin / RI_FACTOR, val);
        d->minSpin->blockSignals(false);

        d->levelMax = val * RI_FACTOR;
        d->slider->SetLevelMinMax(d->levelMin, d->levelMax);

        emit levelChanged(d->levelMin, d->levelMax);
    }

}