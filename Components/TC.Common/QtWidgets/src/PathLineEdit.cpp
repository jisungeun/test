#include <QRegExpValidator>

#include "PathRegExp.h"
#include "PathLineEdit.h"

namespace TC::PathWidget {
	struct PathLineEdit::Impl {
		PathRegExpValidator validator;
	};

	PathLineEdit::PathLineEdit(QWidget* parent) : QLineEdit(parent), d(new Impl) {
		this->setValidator(&d->validator);
	}

	PathLineEdit::PathLineEdit(const QString& text, QWidget* parent) : QLineEdit(text, parent), d(new Impl) {
		this->setValidator(&d->validator);
	}

	PathLineEdit::~PathLineEdit() = default;

    auto PathLineEdit::SetMaxLength(int length) -> void {
		this->setMaxLength(length);
		d->validator.SetMaxLength(length);
    }

    auto PathLineEdit::GetMaxLength() const -> int {
		return d->validator.GetMaxLength();
    }
}
