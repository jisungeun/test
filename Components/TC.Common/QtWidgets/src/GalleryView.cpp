#include <QScrollArea>
#include <QScrollBar>
#include <QHBoxLayout>
#include <QResizeEvent>
#include <QVariantAnimation>
#include <QTimer>

#include "GalleryPainter.h"
#include "GalleryView.h"

namespace TC {
	struct GalleryView::Impl {
		GalleryPainter* painter = new GalleryPainter;
		QScrollArea scroll;

		QVariantAnimation anime;

		mutable Range visibleIndex;
		mutable int hovered = -1;
		mutable int pressed = -1;
		mutable bool resizing = false;
		mutable double posRatio = 0.0;
		mutable qint64 lastIndexChanged = 0;
		QTimer indexTimer;

		Impl() {
			anime.setDuration(3500);
			anime.setStartValue(0.0);
			anime.setEndValue(1.0);
			anime.setEasingCurve(QEasingCurve::InOutQuint);
			anime.setLoopCount(-1);
		}

		~Impl() {
			delete painter;
		}

		auto IsVisibleIndexChanged(int height) -> bool {
			const auto newIndex = GetVisibleIndex(height);

			if (visibleIndex.from != newIndex.from || visibleIndex.to != newIndex.to) {
				visibleIndex = newIndex;
				return true;
			}

			return false;
		}

		auto GetVisibleIndex(int height) -> Range {
			const auto visibleY = scroll.verticalScrollBar()->value();
			const auto colCount = painter->GetColumnCount();
			const auto itemCount = painter->GetItemCount();

			const auto itemSize = (painter->width() + painter->GetSpacing()) / colCount;
			const auto yFrom = visibleY / itemSize;
			const auto yTo = (visibleY + height + itemSize - 1) / itemSize;

			const auto from = std::min(yFrom * colCount, itemCount);
			const auto to = std::min(yTo * colCount, itemCount);

			return { from, to };
		}

		auto UpdatePainter(int height) -> void {
			painter->SetVisibleIndex({ visibleIndex.from, visibleIndex.to });
			painter->update(0, scroll.verticalScrollBar()->value(), painter->width(), height);
		}

		auto EmitVisibleIndexChanged() -> void {
			indexTimer.start();
		}

		auto UpdateIndex(int height) -> void {
			if (IsVisibleIndexChanged(height)) {
				UpdatePainter(height);
				EmitVisibleIndexChanged();
			}
		}
	};

	GalleryView::GalleryView(QWidget* parent) : QWidget(parent), d(new Impl) {
		this->setLayout(new QHBoxLayout(this));
		this->layout()->addWidget(&d->scroll);
		this->layout()->setMargin(0);
		this->layout()->setSpacing(0);
		
		d->scroll.setWidget(d->painter);
		d->scroll.setFrameShape(QFrame::Shape::NoFrame);
		d->scroll.setWidgetResizable(true);

		connect(d->scroll.verticalScrollBar(), &QScrollBar::valueChanged, this, &GalleryView::OnScrollBarValueChanged);
		connect(&d->anime, &QVariantAnimation::valueChanged, this, &GalleryView::OnAnimationValueChanged);
		connect(&d->indexTimer, &QTimer::timeout, this, [this] { emit VisibleIndexChanged(d->visibleIndex.from, d->visibleIndex.to); });

		connect(d->painter, &GalleryPainter::DoubleClicked, this, [this](int idx, QMouseEvent* event) { emit ItemDoubleClicked(idx, event); });
		connect(d->painter, &GalleryPainter::Clicked, this, [this](int idx, QMouseEvent* event) { emit ItemClicked(idx, event); });
		connect(d->painter, &GalleryPainter::Pressed, this, [this](int idx, bool pressed, QMouseEvent* event) { emit ItemPressed(idx, pressed, event); });
		connect(d->painter, &GalleryPainter::Hovered, this, [this](int idx, bool hovered) { emit ItemHovered(idx, hovered); });

		d->indexTimer.setInterval(200);
		d->indexTimer.setSingleShot(true);

		d->anime.start();
	}

	GalleryView::~GalleryView() = default;

	auto GalleryView::SetSpacing(int space) -> void {
		d->painter->SetSpacing(space);
	}

	auto GalleryView::GetSpacing() const -> int {
		return d->painter->GetSpacing();
	}

	auto GalleryView::SetColumnCount(int count) -> void {
		d->painter->SetColumnCount(count);

		if (d->IsVisibleIndexChanged(height())) {
			d->UpdatePainter(height());
			d->EmitVisibleIndexChanged();
		}
	}

	auto GalleryView::GetColumnCount() const -> int {
		return d->painter->GetColumnCount();
	}

	auto GalleryView::SetAnimative(bool animative) -> void {
		d->painter->SetAnimative(animative);
	}

	auto GalleryView::IsAnimative() const -> bool {
		return d->painter->Animative();
	}

	auto GalleryView::ScrollToIndex(int index) -> void {
		d->scroll.verticalScrollBar()->setValue(d->painter->GetItemPosition(index).y());
	}

	auto GalleryView::ScrollToHeight(int px) -> void {
		d->scroll.verticalScrollBar()->setValue(px);
	}

	auto GalleryView::GetVisibleIndex() const -> Range {
		return d->visibleIndex;
	}

	auto GalleryView::AddItem() -> void {
		d->painter->Add();
		d->UpdateIndex(height());
	}

	auto GalleryView::AddItems(int count) -> void {
		d->painter->Add(count);
		d->UpdateIndex(height());
	}

	auto GalleryView::InsertItem(int index) -> void {
		d->painter->Insert(index);
		d->UpdateIndex(height());
	}

	auto GalleryView::InsertItems(int index, int count) -> void {
		d->painter->Insert(index, count);
		d->UpdateIndex(height());
	}

	auto GalleryView::RemoveItemAt(int index) -> void {
		d->painter->RemoveAt(index);
		d->UpdateIndex(height());
	}

	auto GalleryView::Clear() -> void {
		d->painter->Clear();
	}

	auto GalleryView::GetIndexOf(const GalleryContent* item) const -> int {
		return d->painter->IndexOf(item);
	}

	auto GalleryView::GetCount() const -> int {
		return d->painter->GetItemCount();
	}

	auto GalleryView::GetItemAt(int index) const -> GalleryContent* {
		if (index >= 0 && index < d->painter->GetItemCount()) {
			const auto created = d->painter->Contains(index);
			auto* item = d->painter->Get(index);

			if (!created)
				connect(item, &GalleryContent::sigUpdated, this, [this] { d->UpdatePainter(height()); });

			return item;
		}

		return nullptr;
	}

	auto GalleryView::AddDefaultLayer(const QString& name, const QPixmap& pixmap, bool visibility) const -> void {
		d->painter->AddDefaultLayer(name, pixmap, visibility);
	}

	auto GalleryView::GetDefaultLayerNames() const -> QStringList {
		return d->painter->GetDefaultLayerNames();
	}

	auto GalleryView::RemoveDefaultLayer(const QString& name) -> void {
		d->painter->RemoveDefaultLayer(name);
	}

	auto GalleryView::InstallEventFilterOnScrollViewer(QObject* object) -> void {
		d->scroll.verticalScrollBar()->installEventFilter(object);
	}

	auto GalleryView::SetIndexChangeInterval(int ms) -> void {
		d->indexTimer.setInterval(ms);
	}

	auto GalleryView::resizeEvent(QResizeEvent* resizeEvent) -> void {
		QWidget::resizeEvent(resizeEvent);

		d->resizing = true;
		d->scroll.verticalScrollBar()->setValue(static_cast<int>(d->painter->height() * d->posRatio));
		d->resizing = false;

		d->UpdateIndex(height());
	}

	auto GalleryView::OnAnimationValueChanged(const QVariant& value) -> void {
		if (d->painter->Animative() && !d->painter->PaintedAll()) {
			const auto scroll = d->scroll.verticalScrollBar()->value();
			const auto size = width() / 3;
			const auto yPos = (height() + width() * 2 + size * 2) * value.toDouble();

			d->painter->SetGradientPosition(yPos + scroll - width(), size);
			d->UpdatePainter(height());
		}
	}

	auto GalleryView::OnScrollBarValueChanged(int value) -> void {
		d->UpdateIndex(height());

		if (!d->resizing && d->painter->height() > 0)
			d->posRatio = value / static_cast<double>(d->painter->height());
	}
}
