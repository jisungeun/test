#include "AdaptiveHeader.h"

#include <iostream>
#include <QFontMetrics>
#include <QPainter>
//#include <QItem>

namespace TC {    
    struct AdaptiveHeader::Impl {
        
    };
    AdaptiveHeader::AdaptiveHeader(Qt::Orientation ori, QWidget* parent) : QHeaderView(ori,parent), d{ new Impl } {
        
    }
    AdaptiveHeader::~AdaptiveHeader() {
        
    }
    QSize AdaptiveHeader::sectionSizeFromContents(int logicalIndex) const {
        auto headerText = model()->headerData(logicalIndex, orientation()).toString();
        auto options = viewOptions();        
        QFontMetrics metrics(options.font);
        auto maxWidth = sectionSize(logicalIndex);
        auto rect = metrics.boundingRect(QRect(0, 0, maxWidth, 5000),
            defaultAlignment() | Qt::TextWrapAnywhere,
            headerText);
        auto ss = rect.size();
        ss.setWidth(ss.width()+10);
        ss.setHeight(ss.height()+10);
        //return rect.size();
        return ss;        
    }    
    void AdaptiveHeader::paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const {        
        painter->save();        
        auto mod = model();
        auto st = mod->headerData(logicalIndex, orientation());
        mod->setHeaderData(logicalIndex, orientation(), QVariant());
        QHeaderView::paintSection(painter, rect, logicalIndex);
        mod->setHeaderData(logicalIndex, orientation(), st);        
        painter->restore();
        auto headerText = model()->headerData(logicalIndex, orientation()).toString();
        auto sr = QRectF(rect.left()+5, rect.top()+5, rect.width(), rect.height());
        painter->drawText(sr, Qt::TextWrapAnywhere, headerText);
    }
}