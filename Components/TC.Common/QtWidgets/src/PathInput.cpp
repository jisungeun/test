#include "PathInput.h"

#include <iostream>
#include <QFileDialog>

#include "ui_PathInputControl.h"

namespace TC {
    struct PathInput::Impl {
        Ui::PathUi* ui{ nullptr };
        QString name;
        QString extension;
        QString type_name{"all"};

        bool isFolder{ false };
        bool show_desc = false;
    };

    PathInput::PathInput(QWidget* parent) : QWidget(parent),d{ new Impl } {
        d->ui = new Ui::PathUi;
        d->ui->setupUi(this);
        InitializePanel();        
    }

	PathInput::~PathInput() {

    }
    void PathInput::OnPathChanged(const QString& path) {
        emit sigPathChanged(path,d->name);
    }
    auto PathInput::setName(QString n) -> void {
        d->name = n;
    }
    auto PathInput::getName() -> QString {
        return d->name;
    }
    auto PathInput::setPath(QString path) -> void {
        d->ui->pathText->setText(path);
    }

    auto PathInput::getPath() -> QString {
        return d->ui->pathText->text();
    }

    auto PathInput::setFolderType(bool folder) -> void {
        d->isFolder = folder;
    }

    auto PathInput::addExtension(QString ext) -> void {
        d->extension += "*.";
        d->extension += ext;
        d->extension += " ";
    }

    auto PathInput::getCurrentPath() -> QString {
        return d->ui->pathText->text();
    }        

    auto PathInput::clearExtension() -> void {
        d->extension.clear();
    }

    auto PathInput::setDefaultPath(QString dp) -> void {
        d->ui->pathText->setText(dp);
    }

    void PathInput::OnOpenPathClicked() {
        //filepath dialog
        if(d->isFolder) {
            QString pathName = QFileDialog::getExistingDirectory(this, tr("Open Folder"),
                                                                 "/home");
            if(pathName.length() > 0)
                d->ui->pathText->setText(pathName);
        } else {
            QString cur_ext{ "" };
            if (d->extension.length() == 0) {
                cur_ext = "*.*";
            } else {
                cur_ext = d->extension;
            }
            QString filter;
            filter = d->type_name;
            filter += " (";
            filter += cur_ext;
            filter += ")";
            QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                            "/home", filter);
            if (fileName.length() > 0)
                d->ui->pathText->setText(fileName);
        }
    }
        
    auto PathInput::setTypeName(QString tn) -> void {
        d->type_name = tn;
    }


    auto PathInput::InitializePanel()->void {        

        d->ui->pathText->setReadOnly(true);

        connect(d->ui->openPathBtn, SIGNAL(clicked()), this, SLOT(OnOpenPathClicked()));
        connect(d->ui->pathText, SIGNAL(textChanged(const QString&)), this, SLOT(OnPathChanged(const QString&)));

        // set object names
        d->ui->openPathBtn->setObjectName("bt-round-gray700");
    }
}
