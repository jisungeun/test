#include "ui_CollapseWidget.h"
#include "CollapseWidget.h"

namespace TC {
    struct CollapseWidget::Impl {
        Ui::CollapseWidget* ui{ nullptr };

        bool collapsed{ false };
    };

    CollapseWidget::CollapseWidget(QWidget* parent, Qt::WindowFlags f)
        : QWidget(parent, f)
        , d{ new Impl } {
        d->ui = new Ui::CollapseWidget();
        d->ui->setupUi(this);

        connect(d->ui->collapseButton, SIGNAL(clicked(bool)), this, SLOT(OnCollapsed(bool)));
        connect(d->ui->arrowButton, SIGNAL(clicked(bool)), this, SLOT(OnCollapsed(bool)));

        auto contentsLayout = new QVBoxLayout;
        contentsLayout->setContentsMargins(0, 0, 0, 0);
        d->ui->contentsFrame->setLayout(contentsLayout);

        d->ui->collapseButton->setChecked(true);    // set initial state
        d->ui->arrowButton->setChecked(true);

        d->ui->widget->setObjectName("panel-contents");
        d->ui->collapseButton->setObjectName("bt-collapse");
        d->ui->arrowButton->setObjectName("bt-collapse");        
    }    

    CollapseWidget::~CollapseWidget() {
        delete d->ui;
    }    

    auto CollapseWidget::SetCollapsable(bool isCollapse) -> void {
        d->ui->collapseButton->setEnabled(isCollapse);
    }

    auto CollapseWidget::SetWidget(QWidget* widget, const QString& label)->void {
        d->ui->collapseButton->setText(label);

        auto layout = qobject_cast<QVBoxLayout*>(d->ui->contentsFrame->layout());
        if (layout == nullptr) {
            return;
        }

        // delete all widgets in a layout
        while (!layout->isEmpty()) {
            delete layout->takeAt(0);
        }

        layout->addWidget(widget);
    }

    void CollapseWidget::OnCollapsed(bool checked) {
        if (sender() == d->ui->collapseButton) {
            d->ui->arrowButton->blockSignals(true);
            d->ui->arrowButton->setChecked(checked);
            d->ui->arrowButton->blockSignals(false);
        } else {
            d->ui->collapseButton->blockSignals(true);
            d->ui->collapseButton->setChecked(checked);
            d->ui->collapseButton->blockSignals(false);
        }

        d->collapsed = !checked;

        d->ui->contentsFrame->setVisible(!d->collapsed);
    }
}
