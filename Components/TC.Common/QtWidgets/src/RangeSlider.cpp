#include <cmath>
#include <iostream>
#include "RangeSlider.h"

namespace TC {
    struct RangeSlider::Impl {
        struct {
            int min;
            int max;
        } Range{ 0, 100 };

        auto interval()->int { return Range.max - Range.min; };

        struct {
            int lower;
            int upper;
        } Value{ 0, 100 };

        struct {
            bool firstHandlePressed;
            bool secondHandlePressed;
            int delta;
        } Status{ false, false, 0};

        struct {
            QColor enabled;
            QColor disabled;
            QColor current;
        } BackgroundColor{ QColor(0x1E, 0x90, 0xFF), Qt::darkGray, QColor(0x1E, 0x90, 0xFF) };

        Qt::Orientation orientation{ Qt::Horizontal };

        auto isOrientation(Qt::Orientation value) const->bool { return orientation == value; }
        auto isHorizontal() const->bool { return orientation == Qt::Horizontal; }

        const int scHandleSideLength = 11;
        const int scSliderBarHeight = 5;
        const int scLeftRightMargin = 1;
    };

    RangeSlider::RangeSlider(QWidget* parent) : QWidget(parent), d{ new Impl }, type(DoubleHandles) {
        setMouseTracking(true);
    }

    RangeSlider::RangeSlider(Qt::Orientation orientation, Options option, QWidget* parent)
        : QWidget(parent), d{ new Impl }, type(option) {
        d->orientation = orientation;
        setMouseTracking(true);
    }

    RangeSlider::~RangeSlider() {
    }

    QSize RangeSlider::minimumSizeHint() const {
        return QSize(d->scHandleSideLength * 2 + d->scLeftRightMargin * 2, d->scHandleSideLength);
    }

    auto RangeSlider::Minimum() const->int {
        return d->Range.min;
    }

    auto RangeSlider::SetMinimum(int value)->void {
        onSetMinimum(value);
    }

    auto RangeSlider::Maximum() const->int {
        return d->Range.max;
    }

    auto RangeSlider::SetMaximum(int value)->void {
        onSetMaximum(value);
    }

    auto RangeSlider::LowerValue() const->int {
        return d->Value.lower;
    }

    void RangeSlider::SetLowerValue(int value) {
        onSetLowerValue(value);
    }

    auto RangeSlider::UpperValue() const->int {
        return d->Value.upper;
    }

    auto RangeSlider::SetUpperValue(int value)->void {
        onSetUpperValue(value);
    }

    auto RangeSlider::SetRange(int minValue, int maxValue)->void {
        d->Range.min = minValue;
        d->Range.max = maxValue;

        update();

        onSetLowerValue(d->Range.min);
        onSetUpperValue(d->Range.max);

        emit rangeChanged(d->Range.min, d->Range.max);
    }

    void RangeSlider::paintEvent(QPaintEvent* event)
    {
        Q_UNUSED(event);
        QPainter painter(this);

        // Background
        QRectF backgroundRect;
        if (d->isHorizontal()) {
            backgroundRect = QRectF(d->scLeftRightMargin, 
                                    (height() - d->scSliderBarHeight) / 2, 
                                    width() - (d->scLeftRightMargin * 2), 
                                    d->scSliderBarHeight);
        } else {
            backgroundRect = QRectF((width() - d->scSliderBarHeight) / 2, 
                                    d->scLeftRightMargin, 
                                    d->scSliderBarHeight, 
                                    height() - (d->scLeftRightMargin * 2));
        }

        QPen pen(Qt::gray, 0.8);
        painter.setPen(pen);
        painter.setRenderHint(QPainter::Qt4CompatiblePainting);
        QBrush backgroundBrush(QColor(0xD0, 0xD0, 0xD0));
        painter.setBrush(backgroundBrush);
        painter.drawRoundedRect(backgroundRect, 1, 1);

        // First value handle rect
        pen.setColor(Qt::darkGray);
        pen.setWidth(0.5);
        painter.setPen(pen);
        painter.setRenderHint(QPainter::Antialiasing);
        QBrush handleBrush(QColor(0xFA, 0xFA, 0xFA));
        painter.setBrush(handleBrush);
        QRectF leftHandleRect = firstHandleRect();
        if (type.testFlag(LeftHandle))
            painter.drawRoundedRect(leftHandleRect, 2, 2);

        // Second value handle rect
        QRectF rightHandleRect = secondHandleRect();
        if (type.testFlag(RightHandle))
            painter.drawRoundedRect(rightHandleRect, 2, 2);

        // Handles
        painter.setRenderHint(QPainter::Antialiasing, false);
        QRectF selectedRect(backgroundRect);
        if (d->isHorizontal()) {
            selectedRect.setLeft((type.testFlag(LeftHandle) ? leftHandleRect.right() : leftHandleRect.left()) + 0.5);
            selectedRect.setRight((type.testFlag(RightHandle) ? rightHandleRect.left() : rightHandleRect.right()) - 0.5);
        } else {
            selectedRect.setTop((type.testFlag(LeftHandle) ? leftHandleRect.bottom() : leftHandleRect.top()) + 0.5);
            selectedRect.setBottom((type.testFlag(RightHandle) ? rightHandleRect.top() : rightHandleRect.bottom()) - 0.5);
        }
        QBrush selectedBrush(d->BackgroundColor.current);
        painter.setBrush(selectedBrush);
        painter.drawRect(selectedRect);
    }

    void RangeSlider::mousePressEvent(QMouseEvent* aEvent)
    {
        if (aEvent->buttons() & Qt::LeftButton)
        {
            auto posCheck = (d->isHorizontal()) ? aEvent->pos().y() : aEvent->pos().x();
            auto posMax = (d->isHorizontal()) ? height() : width();
            auto posValue = (d->isHorizontal()) ? aEvent->pos().x() : aEvent->pos().y();
            auto firstHandleRectPosValue = (d->isHorizontal()) ? firstHandleRect().x() : firstHandleRect().y();
            auto secondHandleRectPosValue = (d->isHorizontal()) ? secondHandleRect().x() : secondHandleRect().y();

            d->Status.secondHandlePressed = secondHandleRect().contains(aEvent->pos());
            d->Status.firstHandlePressed = !d->Status.secondHandlePressed && firstHandleRect().contains(aEvent->pos());
            if (d->Status.firstHandlePressed) {
                d->Status.delta = posValue - (firstHandleRectPosValue + d->scHandleSideLength / 2);
            } else if (d->Status.secondHandlePressed) {
                d->Status.delta = posValue - (secondHandleRectPosValue + d->scHandleSideLength / 2);
            }

            if ((posCheck >= 2) && (posCheck <= posMax - 2)) {
                int step = d->interval() / 10 < 1 ? 1 : d->interval() / 10;
                if (posValue < firstHandleRectPosValue) {
                    onSetLowerValue(d->Value.lower - step);
                } else if (((posValue > firstHandleRectPosValue + d->scHandleSideLength) || !type.testFlag(LeftHandle))
                         && ((posValue < secondHandleRectPosValue) || !type.testFlag(RightHandle))) {
                    if (type.testFlag(DoubleHandles)) {
                        if (posValue - (firstHandleRectPosValue + d->scHandleSideLength) <
                            (secondHandleRectPosValue - (firstHandleRectPosValue + d->scHandleSideLength)) / 2) {
                            onSetLowerValue((d->Value.lower + step < d->Value.upper) ? d->Value.lower + step : d->Value.upper);
                        } else {
                            onSetUpperValue((d->Value.upper - step > d->Value.lower) ? d->Value.upper - step : d->Value.lower);
                        }
                    } else if (type.testFlag(LeftHandle)) {
                        onSetLowerValue((d->Value.lower + step < d->Value.upper) ? d->Value.lower + step : d->Value.upper);
                    } else if (type.testFlag(RightHandle)) {
                        onSetUpperValue((d->Value.upper - step > d->Value.lower) ? d->Value.upper - step : d->Value.lower);
                    }
                } else if (posValue > secondHandleRectPosValue + d->scHandleSideLength) {
                    onSetUpperValue(d->Value.upper + step);
                }
            }
        }
    }

    void RangeSlider::mouseMoveEvent(QMouseEvent* event)
    {
        if (event->buttons() & Qt::LeftButton) {
            int posValue, firstHandleRectPosValue, secondHandleRectPosValue;
            posValue = (d->isHorizontal()) ? event->pos().x() : event->pos().y();
            firstHandleRectPosValue = (d->isHorizontal()) ? firstHandleRect().x() : firstHandleRect().y();
            secondHandleRectPosValue = (d->isHorizontal()) ? secondHandleRect().x() : secondHandleRect().y();

            const auto delta = d->Status.delta;

            if (d->Status.firstHandlePressed && type.testFlag(LeftHandle)) {
                if (posValue - delta + d->scHandleSideLength / 2 <= secondHandleRectPosValue) {
                    onSetLowerValue((posValue - delta - d->scLeftRightMargin - d->scHandleSideLength / 2) * 1.0 / validLength() * d->interval() + d->Range.min);
                } else {
                    onSetLowerValue(d->Value.upper);
                }
            } else if (d->Status.secondHandlePressed && type.testFlag(RightHandle)) {
                if (firstHandleRectPosValue + d->scHandleSideLength * (type.testFlag(DoubleHandles) ? 1.5 : 0.5) <= posValue - delta) {
                    onSetUpperValue((posValue - delta - d->scLeftRightMargin - d->scHandleSideLength / 2 - (type.testFlag(DoubleHandles) ? d->scHandleSideLength : 0)) * 1.0 / validLength() * d->interval() + d->Range.min);
                } else {
                    onSetUpperValue(d->Value.lower);
                }
            }
        }
    }

    void RangeSlider::mouseReleaseEvent(QMouseEvent* event) {
        Q_UNUSED(event);
        d->Status.firstHandlePressed = false;
        d->Status.secondHandlePressed = false;
    }

    void RangeSlider::changeEvent(QEvent* event) {
        if (event->type() == QEvent::EnabledChange) {
            if (isEnabled()) {
                d->BackgroundColor.current = d->BackgroundColor.enabled;
            } else {
                d->BackgroundColor.current = d->BackgroundColor.disabled;
            }
            update();
        }
    }

    QRectF RangeSlider::firstHandleRect() const
    {
        float percentage = (d->Value.lower - d->Range.min) * 1.0 / d->interval();
        return handleRect(percentage * validLength() + d->scLeftRightMargin);
    }

    QRectF RangeSlider::secondHandleRect() const
    {
        float percentage = (d->Value.upper - d->Range.min) * 1.0 / d->interval();
        return handleRect(percentage * validLength() + d->scLeftRightMargin + (type.testFlag(LeftHandle) ? d->scHandleSideLength : 0));
    }

    QRectF RangeSlider::handleRect(int aValue) const
    {
        if (d->orientation == Qt::Horizontal) {
            return QRect(aValue, (height() - d->scHandleSideLength) / 2, d->scHandleSideLength, d->scHandleSideLength);
        } else {
            return QRect((width() - d->scHandleSideLength) / 2, aValue, d->scHandleSideLength, d->scHandleSideLength);
        }
    }

    void RangeSlider::onSetLowerValue(int value) {
        value = std::min<int>(value, d->Range.max);
        value = std::max<int>(value, d->Range.min);

        d->Value.lower = value;
        emit lowerValueChanged(d->Value.lower);
        
        update();
    }

    void RangeSlider::onSetUpperValue(int value) {
        value = std::min<int>(value, d->Range.max);
        value = std::max<int>(value, d->Range.min);

        d->Value.upper = value;
        emit upperValueChanged(d->Value.upper);
        
        update();
    }

    void RangeSlider::onSetMinimum(int value) {
        if (value > d->Range.max) {
            const auto oldMax = d->Range.max;
            d->Range.min = oldMax;
            d->Range.max = value;
        } else {
            d->Range.min = value;
        }

        update();

        onSetLowerValue(d->Range.min);
        onSetUpperValue(d->Range.max);

        emit rangeChanged(d->Range.min, d->Range.max);
    }

    void RangeSlider::onSetMaximum(int value) {
        if (value < d->Range.min) {
            const auto oldMin = d->Range.min;
            d->Range.max = oldMin;
            d->Range.min = value;
        } else {
            d->Range.max = value;
        }

        update();

        onSetLowerValue(d->Range.min);
        onSetUpperValue(d->Range.max);

        emit rangeChanged(d->Range.min, d->Range.max);
    }

    auto RangeSlider::validLength() const->int {
        const auto len = (d->isHorizontal()) ? width() : height();
        return len - d->scLeftRightMargin * 2 - d->scHandleSideLength * (type.testFlag(DoubleHandles) ? 2 : 1);
    }
}