#include <QLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QPushButton>

#include "MessageDialog.h"

#include "ui_MessageDialog.h"

namespace TC {
	struct MessageDialog::Impl {
		QFrame* frame = nullptr;
		Ui::MessageDialog ui{};
	};

	MessageDialog::MessageDialog(QWidget* parent) : CustomDialog(parent, Qt::WindowFlags()), d(new Impl) {
		d->frame = new QFrame(this);
		d->ui.setupUi(d->frame);

		d->ui.contentLabel->setObjectName("tc-messagedialog-content");

		SetStandardButtons(StandardButton::Ok);
		SetDefaultButton(StandardButton::Ok);

		SetContext(d->frame);
		this->installEventFilter(this);
	}

	MessageDialog::MessageDialog(const QString& content, QWidget* parent) : MessageDialog(parent) {
		SetContent(content);
	}

	MessageDialog::MessageDialog(const QString& title, const QString& content, QWidget* parent) : MessageDialog(content, parent) {
		SetTitle(title);
	}

	MessageDialog::~MessageDialog() = default;

	auto MessageDialog::GetContent() const -> QString {
		if (d->ui.contentLabel)
			return d->ui.contentLabel->text();

		return {};
	}

	auto MessageDialog::SetContent(const QString& content) -> void {
		d->ui.contentLabel->setText(content);
		UpdateSize();
	}

	MessageDialog::StandardButton MessageDialog::information(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons, StandardButton defaultButton) {
		MessageDialog dialog(title, text, parent);
		dialog.SetStandardButtons(buttons);
		dialog.SetDefaultButton(defaultButton);

		dialog.exec();
		return dialog.GetLastClickedButton();
	}

	MessageDialog::StandardButton MessageDialog::question(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons, StandardButton defaultButton) {
		return information(parent, title, text, buttons, defaultButton);
	}

	MessageDialog::StandardButton MessageDialog::warning(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons, StandardButton defaultButton) {
		return information(parent, title, text, buttons, defaultButton);
	}

	MessageDialog::StandardButton MessageDialog::critical(QWidget* parent, const QString& title, const QString& text, StandardButtons buttons, StandardButton defaultButton) {
		return information(parent, title, text, buttons, defaultButton);
	}

	auto MessageDialog::eventFilter(QObject* object, QEvent* event) -> bool {
		if (event->type() == QEvent::KeyPress) {
			const auto* keyEvent = dynamic_cast<QKeyEvent*>(event);

			switch (keyEvent->key()) {
			case Qt::Key_Enter:
			case Qt::Key_Return:
				return true;
			}
		}

		return CustomDialog::eventFilter(object, event);
	}

	auto MessageDialog::GetMinimumWidth() const -> int {
		d->ui.contentLabel->setWordWrap(false);
		const auto width = d->frame->minimumSizeHint().width();
		d->ui.contentLabel->setWordWrap(true);

		return width;
	}
}
