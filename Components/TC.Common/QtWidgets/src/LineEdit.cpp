#include <QPalette>

#include "LineEdit.h"

namespace TC {
	struct LineEdit::Impl {
		struct {
			double opacity{ 1.0 };
			double newOpacity{ 1.0 };
		} placeHolder;
	};

	LineEdit::LineEdit(QWidget* parent) : QLineEdit(parent), d{ new Impl } {}

	LineEdit::~LineEdit() = default;

	auto LineEdit::SetPlaceholderOpacity(double opacity) -> void {
		d->placeHolder.newOpacity = opacity;
	}

	auto LineEdit::GetPlaceholderOpacity() const -> double {
		return d->placeHolder.opacity;
	}

    void LineEdit::paintEvent(QPaintEvent* event) {
		if(d->placeHolder.newOpacity != d->placeHolder.opacity) {
			auto pal = palette();
			const auto alpha = static_cast<int>(d->placeHolder.newOpacity * 255.0);
		    auto color = pal.color(QPalette::PlaceholderText);
		    color.setAlpha(alpha);
			pal.setColor(QPalette::PlaceholderText, color);
			setPalette(pal);
			d->placeHolder.opacity = d->placeHolder.newOpacity;
		}

        QLineEdit::paintEvent(event);
    }
}
