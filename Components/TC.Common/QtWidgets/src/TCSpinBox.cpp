#include <QtWidgets>
#include <QWidget>

#include "TCSpinBox.h"

namespace TC {
    TCSpinBox::TCSpinBox(QWidget* parent) : QSpinBox(parent) {
        auto lineEdit = new TCSpinBoxLineEdit(TCSpinBoxLineEdit::SpinBoxType::IntType, this);
        auto re = QString("^-?[0-9]+$");
        lineEdit->setValidator(new QRegExpValidator(QRegExp(re)));
        setLineEdit(lineEdit);
        connect(lineEdit, SIGNAL(returnPressed()), this, SIGNAL(returnPressed()));
    }

    TCSpinBox::~TCSpinBox() {
    }

    auto TCSpinBox::leaveFocus() -> void {
        auto* edit = lineEdit();
        if(edit) edit->clearFocus();
    }

    auto TCSpinBox::fixup(QString& str) const -> void {
        auto isOk = false;
        auto value = str.toInt(&isOk);
        if (isOk) {
            value = qBound(minimum(), value, maximum());
            str = QString::number(value);
        }
        else {
            QSpinBox::fixup(str);
        }
    }

    auto TCSpinBox::validate(QString& input, int& pos) const -> QValidator::State {
        auto isOk = false;
        auto value = input.toInt(&isOk);
        if (isOk) {
            if (value >= minimum() && value <= maximum()) {
                return QValidator::Acceptable;
            }

            return QValidator::Intermediate;
        }

        return QSpinBox::validate(input, pos);
    }
}

namespace TC {
    TCDoubleSpinBox::TCDoubleSpinBox(QWidget* parent) : QDoubleSpinBox(parent) {
        setLineEdit(new TCSpinBoxLineEdit(TCSpinBoxLineEdit::SpinBoxType::DoubleType, this));
        connect(lineEdit(), SIGNAL(returnPressed()), this, SIGNAL(returnPressed()));
    }

    TCDoubleSpinBox::~TCDoubleSpinBox() {
    }

    auto TCDoubleSpinBox::leaveFocus() -> void {
        auto* edit = lineEdit();
        if(edit) edit->clearFocus();
    }

    auto TCDoubleSpinBox::fixup(QString& str) const -> void {
        auto isOk = false;
        auto value = str.toDouble(&isOk);

        if (isOk) {
            value = qBound(minimum(), value, maximum());
            str = QString::number(value);
        }
        else {
            QDoubleSpinBox::fixup(str);
        }
    }

    auto TCDoubleSpinBox::validate(QString& input, int& pos) const -> QValidator::State {
        auto isOk = false;
        auto value = input.toDouble(&isOk);

        if (isOk) {
            if (value >= minimum() && value <= maximum()) {
                return QValidator::Acceptable;
            }

            return QValidator::Intermediate;
        }

        return QDoubleSpinBox::validate(input, pos);
    }
}

namespace TC {
    struct TCSpinBoxLineEdit::Impl {
        SpinBoxType type{SpinBoxType::IntType};
        int32_t decimals{-1};
    };

    TCSpinBoxLineEdit::TCSpinBoxLineEdit(SpinBoxType spinBoxType, QWidget* parent) : QLineEdit(parent), d{std::make_unique<Impl>()} {
        d->type = spinBoxType;
    }

    TCSpinBoxLineEdit::~TCSpinBoxLineEdit() {
    }

    auto TCSpinBoxLineEdit::mousePressEvent(QMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            if (parent()) {
                const auto sp = qobject_cast<QAbstractSpinBox*>(parent());
                if (sp) {
                    sp->selectAll();
                }
            }
            event->accept();
            return;
        }

        QLineEdit::mousePressEvent(event);
    }

    auto TCSpinBoxLineEdit::focusInEvent(QFocusEvent* event) -> void {
        if(d->type == SpinBoxType::DoubleType && parent()) {
            const auto dsb = qobject_cast<QDoubleSpinBox*>(parent());
            if(d->decimals != dsb->decimals()) {
                auto re = QString("^-?[0-9]+(\\.[0-9]{0,%1})?$").arg(dsb->decimals());
                setValidator(new QRegExpValidator(QRegExp(re), this));
                d->decimals = dsb->decimals();
            }
        }

        QLineEdit::focusInEvent(event);
    }
}
