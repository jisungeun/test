#include "CollapsibleGroupbox.h"

#include <QtWidgets>

namespace TC {
    struct CollapsibleGroupBox::Impl {
        Impl() = default;
        ~Impl() = default;

        QLabel* caption = nullptr;
        QString text;
        bool expanded = false;
        QVBoxLayout* layout = nullptr;
        QWidget* child = nullptr;

        QMargins expandedCaptionMargins;
        QMargins collapsedCaptionMargins;
    };

    CollapsibleGroupBox::CollapsibleGroupBox(const QString& text, bool expanded, QWidget* parent)
        : QFrame(parent)
        , d(new Impl()) {
        d->caption = new QLabel;
        d->layout = new QVBoxLayout;

        this->SetText(text);
        this->SetExpanded(expanded);

        this->setLayout(d->layout);
        d->layout->addWidget(d->caption);

        d->caption->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

        this->setObjectName("CollapsibleGroupBox");
        this->setStyleSheet("QWidget#CollapsibleGroupBox{ border: 1px solid " + QPalette().light().color().name() + "; border-radius: 5px; }");

        this->SetChild(new QWidget());

        d->expandedCaptionMargins = d->collapsedCaptionMargins = d->caption->contentsMargins();
        d->expandedCaptionMargins.setBottom(d->collapsedCaptionMargins.bottom() + 5);

        connect(d->caption, SIGNAL(linkActivated(const QString&)), this, SLOT(onToggled(const QString&)));
    }

    CollapsibleGroupBox::~CollapsibleGroupBox() = default;

    auto CollapsibleGroupBox::Text() const ->QString {
        return d->text;
    }

    void CollapsibleGroupBox::SetText(const QString& text) const {
        d->text = text;
        this->UpdateCaption();
    }

    bool CollapsibleGroupBox::IsCollapsed() const {
        return !this->IsExpanded();
    }

    bool CollapsibleGroupBox::IsExpanded() const {
        return d->expanded;
    }

    void CollapsibleGroupBox::SetExpanded(bool expanded) {
        d->expanded = expanded;
        this->UpdateCaption();

        if (nullptr != d->child) {
            d->child->setVisible(d->expanded);
        }

        emit toggled(expanded);
    }

    void CollapsibleGroupBox::Toggle() {
        this->SetExpanded(this->IsCollapsed());
    }

    void CollapsibleGroupBox::Expand() {
        this->SetExpanded(true);
    }

    void CollapsibleGroupBox::Collapse() {
        this->SetExpanded(false);
    }

    auto CollapsibleGroupBox::Child() const ->QWidget* {
        return d->child;
    }

    auto CollapsibleGroupBox::TakeChild() const ->QWidget* {
        if (nullptr == d->child) {
            return nullptr;
        }

        d->child->hide();
        d->layout->removeWidget(d->child);

        const auto child = d->child;
        d->child = nullptr;

        return child;
    }

    void CollapsibleGroupBox::SetChild(QWidget* child) const {
        if (nullptr != d->child)
        {
            delete TakeChild();
        }

        d->child = child;
        if (d->child->layout()) {
            d->child->layout()->setContentsMargins(0, 0, 0, 0);
        }

        if (true == this->IsCollapsed()) {
            d->child->setVisible(false);
        }

        d->layout->addWidget(d->child);
        if (true == this->IsExpanded()) {
            d->child->setVisible(true);
        }
    }

    void CollapsibleGroupBox::onToggled(const QString&) {
        this->Toggle();
    }

    void CollapsibleGroupBox::UpdateCaption() const {
        const QString prefix = d->expanded ? "&#9660;" : "&#9658;";
        d->caption->setText(prefix + " <a href=\"#\">" + d->text + "</a>");
        d->caption->setContentsMargins(d->expanded ? d->expandedCaptionMargins : d->collapsedCaptionMargins);
    }
    
}
