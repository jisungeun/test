#include "GalleryLayer.h"

namespace TC {
	struct GalleryLayer::Impl {
		QPixmap pixmap;
		bool visibility = true;
	};

	GalleryLayer::GalleryLayer(QObject* parent) : QObject(parent), d(new Impl) {}

	GalleryLayer::~GalleryLayer() = default;

	GalleryLayer::GalleryLayer(GalleryLayer&& obj) noexcept : d(std::move(obj.d)) {}

	GalleryLayer::GalleryLayer(const GalleryLayer& obj) : d(new Impl{ *obj.d }) {}

	auto GalleryLayer::operator=(const GalleryLayer& obj) -> GalleryLayer& {
		if (this == &obj) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	auto GalleryLayer::operator=(GalleryLayer&& obj) noexcept -> GalleryLayer& {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	auto GalleryLayer::GetPixmap() const -> QPixmap& {
		return d->pixmap;
	}

	auto GalleryLayer::GetVisibility() const -> bool {
		return d->visibility;
	}

	auto GalleryLayer::SetPixmap(const QPixmap& pixmap) -> void {
		d->pixmap = pixmap;
		emit sigUpdated();
	}

	auto GalleryLayer::SetPixmap(QPixmap&& pixmap) -> void {
		d->pixmap = std::move(pixmap);
		emit sigUpdated();
	}

	auto GalleryLayer::SetVisibility(bool visiblity) -> void {
		d->visibility = visiblity;
		emit sigUpdated();
	}
}
