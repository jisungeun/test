#include <QDebug>
#include <QWidget>

#include "FlowLayout.h"

namespace TC {
	struct Range {
		int from = -1, to = -1;
	};

	struct FlowLayout::Impl {
		QList<QLayoutItem*> items;

		int rowCount = 5;
		int sizeCache = 0;

		int virtualCount = -1;

		[[nodiscard]] auto GetPosition(int index, int width, int spacing) const -> QPoint {
			const int size = (width + spacing) / rowCount;

			int x = (index % rowCount) * size;
			int y = (index / rowCount) * size;

			return { x, y };
		}

		[[nodiscard]] auto GetItemSize(int width, int spacing) const -> int {
			const int actualSize = (width - (spacing * (rowCount - 1))) / rowCount;

			return std::max(1, actualSize);
		}

		[[nodiscard]] auto GetPreferedHeight(int width, int spacing) const -> int {
			const int itemCount = std::max(items.count(), virtualCount);
			const int itemHeight = GetItemSize(width, spacing) * ((itemCount + rowCount - 1) / rowCount);
			const int spaceHeight = spacing * ((itemCount - 1) / rowCount);

			return itemHeight + spaceHeight;
		}

		[[nodiscard]] auto GetIndexOfPosition(int y, int width, int height, int spacing) const -> Range {
			const auto itemSize = (width + spacing) / rowCount;
			const auto yFrom = y / itemSize;
			const auto yTo = (y + height + itemSize - 1) / itemSize;
			const auto itemCount = std::max(items.count(), virtualCount);

			const auto from = std::min(yFrom * rowCount, itemCount);
			const auto to = std::min(yTo * rowCount, itemCount);

			return { from, to };
		}
	};

	FlowLayout::FlowLayout(QWidget* parent) : QLayout(parent), d(new Impl) {}

	FlowLayout::~FlowLayout() = default;

	auto FlowLayout::SetItemCountOnRow(int count) -> void {
		d->rowCount = count;
	}

	auto FlowLayout::GetItemCountOnRow() const -> int {
		return d->rowCount;
	}

	auto FlowLayout::InsertWidget(int index, QWidget* widget) -> void {
		addChildWidget(widget);

		if (index < 0)
			index = d->items.count();

		auto* b = new QWidgetItem(widget);

		d->items.insert(index, b);
		d->sizeCache = -1;
		// for refreshing geometry.
		invalidate();
	}

	auto FlowLayout::GetItemSize(bool withSpacing) const -> int {
		Q_UNUSED(withSpacing)
		return d->GetItemSize(geometry().width(), spacing());
	}

	auto FlowLayout::addItem(QLayoutItem* item) -> void {
		d->items.push_back(item);
		d->sizeCache = -1;
		// for refreshing geometry.
	}

	auto FlowLayout::sizeHint() const -> QSize {
		return minimumSize();
	}

	void FlowLayout::setGeometry(const QRect& r) {
		QLayout::setGeometry(r);

		if (r.width() == d->sizeCache)
			return;
		d->sizeCache = r.width();

		const int width = d->GetItemSize(r.width(), spacing());
		const auto range = d->GetIndexOfPosition(r.y(), r.width(), r.height(), spacing());

		for (int i = range.from; i < range.to; i++) {
			auto* widget = d->items[i]->widget();
			auto pos = d->GetPosition(i, r.width(), spacing());

			widget->setFixedSize(width, width);
			d->items[i]->setGeometry({ pos.x(), pos.y(), width, width });
		}
	}

	auto FlowLayout::itemAt(int index) const -> QLayoutItem* {
		if (index >= 0 && index < d->items.count())
			return d->items[index];

		return nullptr;
	}

	auto FlowLayout::takeAt(int index) -> QLayoutItem* {
		if (index >= 0 && index < d->items.count())
			return d->items.takeAt(index);

		return nullptr;
	}

	auto FlowLayout::count() const -> int {
		return d->items.count();
	}

	auto FlowLayout::minimumSize() const -> QSize {
		int min = spacing() * (d->rowCount - 1) + d->rowCount;
		int size = d->GetItemSize(geometry().width(), spacing());

		return { min, size };
	}

	auto FlowLayout::hasHeightForWidth() const -> bool {
		return true;
	}

	auto FlowLayout::heightForWidth(int width) const -> int {
		Q_UNUSED(width)
		const auto height = d->GetPreferedHeight(geometry().width(), spacing());
		return height;
	}
}