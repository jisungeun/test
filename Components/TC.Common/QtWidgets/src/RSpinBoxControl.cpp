#include "RSpinBoxControl.h"

namespace TC {
	struct RSpinBoxControl::Impl {
		double resolution = 1.0;
		double value = 0;
	};

	RSpinBoxControl::RSpinBoxControl() : d(new Impl) {}

	RSpinBoxControl::~RSpinBoxControl() = default;

    auto RSpinBoxControl::GetValue() const -> double {
		return d->value;
    }

	auto RSpinBoxControl::GetResolution() const -> double {
		return d->resolution;
    }

	auto RSpinBoxControl::GetRevised() const -> double {
		return GetRevised(d->value);
	}

    auto RSpinBoxControl::GetRevised(double value) const -> double {
		const double pulse = round(value * d->resolution);
		return pulse / d->resolution;
    }

    auto RSpinBoxControl::SetValue(double  value) -> void {
		d->value = value;
    }

	auto RSpinBoxControl::SetResolution(double resolution) -> void {
		d->resolution = resolution;
	}
}
