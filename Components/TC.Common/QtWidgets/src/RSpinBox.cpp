#include <QDebug>

#include "RSpinBox.h"
#include "RSpinBoxControl.h"

struct TC::RSpinBox::Impl {
	RSpinBoxControl control;
	
	bool valueChanged = false;
};

TC::RSpinBox::RSpinBox(double resolution, QWidget* parent) : QDoubleSpinBox(parent), d(std::make_unique<Impl>()) {
	d->control.SetResolution(resolution);

	connect(this, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
		Q_UNUSED(value)
		d->valueChanged = true;
		}
	);

	connect(this, &QDoubleSpinBox::editingFinished, [this]() {
		if (d->valueChanged) {
			d->valueChanged = false;
			d->control.SetValue(value());
			QDoubleSpinBox::setValue(d->control.GetRevised());
		}
		}
	);
}

TC::RSpinBox::~RSpinBox() = default;

auto TC::RSpinBox::SetResolution(double resolution) -> void {
	d->control.SetResolution(resolution);

	QDoubleSpinBox::setValue(d->control.GetRevised());
}

auto TC::RSpinBox::SetRange(double min, double max) -> void {
	QDoubleSpinBox::setRange(d->control.GetRevised(min), d->control.GetRevised(max));
}

auto TC::RSpinBox::SetValue(double value) -> void {
	d->control.SetValue(value);

	QDoubleSpinBox::setValue(d->control.GetRevised());
}

auto TC::RSpinBox::GetValueOriginal() const -> double {
	return d->control.GetValue();
}

[[nodiscard]] auto TC::RSpinBox::GetValue() const -> double {
	return d->control.GetRevised();
}

[[nodiscard]] auto TC::RSpinBox::GetValueDisplayed() const -> double {
	return QDoubleSpinBox::value();
}

void TC::RSpinBox::stepBy(int steps) {
	if (steps > 0) {
		d->control.SetValue(d->control.GetValue() + singleStep());
		QDoubleSpinBox::setValue(d->control.GetRevised());
		d->valueChanged = false;
	} else if (steps < 0) {
		d->control.SetValue(d->control.GetValue() - singleStep());
		QDoubleSpinBox::setValue(d->control.GetRevised());
		d->valueChanged = false;
	}
}
