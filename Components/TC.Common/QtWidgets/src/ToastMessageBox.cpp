#include "ToastMessageBox.h"

#include <QtWidgets>

namespace TC {
    struct ToastMessageBox::Impl {
        Impl() = default;
        ~Impl() = default;

        QLabel* label = nullptr;
        float opacity;
        QPropertyAnimation* animation = nullptr;
        QTimer* timer;
    };

	ToastMessageBox::ToastMessageBox(QWidget* parent)
    : QWidget(parent)
    , d(new Impl) {
        setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);       
        setAttribute(Qt::WA_TranslucentBackground);
        setAttribute(Qt::WA_ShowWithoutActivating);

        d->animation = new QPropertyAnimation;
        d->animation->setTargetObject(this);
        d->animation->setPropertyName("opacity");
        connect(d->animation, &QAbstractAnimation::finished, this, &ToastMessageBox::onHide);

        d->label = new QLabel;
        d->label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        d->label->setStyleSheet("QLabel { color : white; "
                                "margin-top: 6px;"
                                "margin-bottom: 6px;"
                                "margin-left: 10px;"
                                "margin-right: 10px; }");

        auto layout = new QGridLayout;
        layout->addWidget(d->label, 0, 0);
        this->setLayout(layout);

        d->timer = new QTimer();
        connect(d->timer, &QTimer::timeout, this, &ToastMessageBox::onHideAnimation);
	}

	ToastMessageBox::~ToastMessageBox() {
	    
	}

    void ToastMessageBox::Show(const QString& text, const int32_t& durationMSec) {
        d->label->setText(text);
        adjustSize();

        d->animation->setDuration(150);     // Configuring the durationMSec of the animation
        d->animation->setStartValue(0.0);   // The start value is 0 (fully transparent widget)
        d->animation->setEndValue(1.0);     // End - completely opaque widget        
        setGeometry(screen()->size().width() - 36 - width() + screen()->geometry().x(),
                    screen()->size().height() - 36 - height() + screen()->geometry().y(),
                    width(),
                    height());

        this->show();

        d->animation->start();
        d->timer->start(durationMSec);
    }

    void ToastMessageBox::paintEvent(QPaintEvent* e) {
        Q_UNUSED(e)
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);

        QRect roundedRect;
        roundedRect.setX(rect().x() + 5);
        roundedRect.setY(rect().y() + 5);
        roundedRect.setWidth(rect().width() - 10);
        roundedRect.setHeight(rect().height() - 10);

        painter.setBrush(QBrush(QColor(0, 0, 0, 180)));
        painter.setPen(Qt::NoPen);

        painter.drawRoundedRect(roundedRect, 10, 10);
    }

    void ToastMessageBox::onHideAnimation() const {
        d->timer->stop();
        d->animation->setDuration(1000);
        d->animation->setStartValue(1.0);
        d->animation->setEndValue(0.0);
        d->animation->start();
    }

    void ToastMessageBox::onHide() {
        if (this->GetOpacity() == 0.f) {
            QWidget::hide();
            QWidget::close();
        }
    }

    auto ToastMessageBox::GetOpacity(void) const ->float {
        return d->opacity;
    }

    void ToastMessageBox::SetOpacity(const float& opacity) {
        d->opacity = opacity;
        setWindowOpacity(opacity);
    }
}
