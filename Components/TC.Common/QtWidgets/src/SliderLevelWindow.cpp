#include "SliderLevelWindow.h"

#include <iostream>
#include <QPainter>
#include <QMouseEvent>

#define RI_FACTOR (10000.0)

namespace TC {
	struct SliderLevelWindow::Impl {
		bool isResize{ false };
		bool isBottom{ false };
		bool isCtrlDown{ false };
		bool isMouseDown{ false };
		bool scaleVisible{ true };
		bool isLeftBtn{ false };
		int moveHeight;
		QRect lowerBound;
		QRect upperBound;
		QPoint startPos;
		QRect rectInfo;
		QFont font;
		float levelMin{ 0 };
		float levelMax{ 1 };
		float imgMin{ 0 };
		float imgMax{ 1 };
	};
	SliderLevelWindow::SliderLevelWindow(QWidget* parent) : QWidget(parent), d{ new Impl } {
		d->font.setPointSize(6);
		d->moveHeight = height() - 25;
		//setMouseTracking(true);
		update();
	}
	SliderLevelWindow::~SliderLevelWindow() {

	}
	auto SliderLevelWindow::ToggleScale(bool isVisible)->void {
		d->scaleVisible = isVisible;
	}

	auto SliderLevelWindow::SetLevelMinMax(float min, float max) -> void {
		d->levelMin = min;
		d->levelMax = max;

		update();
	}

	auto SliderLevelWindow::SetMinMax(float min, float max) -> void {
		d->imgMin = min;
		d->imgMax = max;
    }

	auto SliderLevelWindow::GetMinMax() -> std::tuple<float, float> {
		return std::make_tuple(d->levelMin, d->levelMax);
    }

	void SliderLevelWindow::update() {
		int rectWidth;
		if (d->scaleVisible)
		{
			rectWidth = 17;
			setMinimumSize(QSize(50, 50));
			setMaximumSize(QSize(50, 2000));
			setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));
		}
		else
		{
			rectWidth = 26;
			setMinimumSize(QSize(40, 50));
			setMaximumSize(QSize(50, 2000));
			setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));
		}
		float mr = d->imgMax-d->imgMin;

		if (mr < 1) {
			mr = 1;
		}

		float fact = (float)d->moveHeight / mr;

		auto window = static_cast<double>(d->levelMax - d->levelMin);
		float rectHeight = window * fact;

		if (rectHeight < 15) {
			rectHeight = 15;
		}

		if (d->levelMin < 0) {
			d->rectInfo.setRect(2, (int)(d->moveHeight - (d->levelMax - d->imgMin) * fact), rectWidth, (int)rectHeight);
		}
		else {
			d->rectInfo.setRect(2, (int)(d->moveHeight - (d->levelMax - d->imgMin) * fact), rectWidth, (int)rectHeight);
		}			

		QWidget::repaint();
	}
		
	void SliderLevelWindow::enterEvent(QEvent* enterEvent) {
		Q_UNUSED(enterEvent)
		QPoint p = QCursor::pos();
		p = this->mapFromGlobal(p);
		QMouseEvent ev(QEvent::MouseMove, p, Qt::NoButton, Qt::NoButton, Qt::NoModifier);
		this->mouseMoveEvent(&ev);
		setMouseTracking(true);
    }
	void SliderLevelWindow::leaveEvent(QEvent* leaveEvent) {
		Q_UNUSED(leaveEvent)
	    setMouseTracking(false);
    }

	void SliderLevelWindow::mouseMoveEvent(QMouseEvent* mouseEvent) {		
		if(!mouseEvent) {
			return;
		}
		if(false == d->isMouseDown) {			
			if (mouseEvent->pos().y() >= (d->rectInfo.topLeft().y() - 3)
				&& mouseEvent->pos().y() <= (d->rectInfo.topLeft().y() + 3)) {
				setCursor(Qt::SizeVerCursor);
				d->upperBound.setRect(d->rectInfo.topLeft().x(), d->rectInfo.topLeft().y() - 3, 17, 7);
				this->setToolTip("Left click to change only upper bound"); 
				d->isResize = true;			    
			}
			else if (mouseEvent->pos().y() >= (d->rectInfo.bottomLeft().y() - 3) && mouseEvent->pos().y() <= (d->rectInfo.bottomLeft().y() + 3)) {
				setCursor(Qt::SizeVerCursor);
				d->lowerBound.setRect(d->rectInfo.bottomLeft().x(), d->rectInfo.bottomLeft().y() - 3, 17, 7);
				this->setToolTip("Left click to change only lower bound"); 
				d->isResize = true;
				d->isBottom = true;
			}else{
				setCursor(Qt::ArrowCursor);
				this->setToolTip("Left click and mouse move to adjust the slider");
				d->isResize = false;
				d->isBottom = false;
			}
		}else {
			float fact = static_cast<float>(d->moveHeight) / static_cast<float>(d->imgMax - d->imgMin);
			auto window = static_cast<double>(d->levelMax - d->levelMin);
			auto level = window / 2.0 + d->levelMin;
			if (d->isLeftBtn) {
				if (d->isResize && d->isCtrlDown) {
					double diff = (mouseEvent->pos().y()) / fact;
					diff -= d->startPos.y() / fact;
					d->startPos = mouseEvent->pos();

					if (diff == 0) return;
					float value;
					if (d->isBottom)
						value = window + ((2.0 * diff));
					else
						value = window - ((2.0 * diff));

					if (value < 0)
						value = 0;

					d->levelMin = level - value / 2;
					d->levelMax = level + value / 2;
				}
				else if (d->isResize && false == d->isCtrlDown)
				{
					if (false == d->isBottom)
					{
						double diff = mouseEvent->pos().y() / fact;
						diff -= d->startPos.y() / fact;
						d->startPos = mouseEvent->pos();

						if (diff == 0) return;						
						auto value = window - ((diff));

						if (value < 0)
							value = 0;
						
						auto newLevel = level + (value - window) / 2;

					    if (!((newLevel + value / 2) > d->imgMax)) {
							d->levelMin = newLevel - value/2;
							d->levelMax = newLevel + value/2;
						}
					}
					else
					{
						double diff = (mouseEvent->pos().y()) / fact;
						diff -= (d->startPos.y()) / fact;
						d->startPos = mouseEvent->pos();
						
						if (diff == 0) return;
						float value;						
						value = window + ((diff));

						if (value < 0)
							value = 0;
						
						auto newLevel = level - (value - window) / 2;
						if (!((newLevel - value / 2) < d->imgMin)
							)
						{
							d->levelMin = newLevel - value/2;
							d->levelMax = newLevel + value/2;
						}
					}
				}
				else
				{
					const float minv = d->imgMin;					

					const float slevel = (d->moveHeight - mouseEvent->pos().y()) / fact + minv;

					double diff = (mouseEvent->pos().y()) / fact;
					diff -= (d->startPos.y()) / fact;
					d->startPos = mouseEvent->pos();
					float value;

				    value = window - 2 * diff;

					if (value < 0)
						value = 0;

					const float default_minv = d->imgMin;
				    const float default_maxv = d->imgMax;
					if ((slevel <= default_maxv) && (slevel >= default_minv)) {
						d->levelMin = d->levelMin - diff;
						d->levelMax = d->levelMax - diff;
					}
				}
				emit rangeChanged(d->levelMin, d->levelMax);
				update();
			}
		}
	}
	void SliderLevelWindow::mouseDoubleClickEvent(QMouseEvent* mouseEvent) {
		if(mouseEvent->button() == Qt::LeftButton) {
			d->levelMin = d->imgMin + (d->imgMax - d->imgMin) / 7;//TODO - auto calibration
			d->levelMax = d->imgMax - (d->imgMax - d->imgMin) / 7;
			emit rangeChanged(d->levelMin, d->levelMax);
			update();
		}
	}
	void SliderLevelWindow::mousePressEvent(QMouseEvent* mouseEvent) {
		d->isMouseDown = true;
		d->startPos = mouseEvent->pos();
		if (mouseEvent->button() == Qt::LeftButton)
		{
			if (mouseEvent->modifiers() == Qt::ControlModifier || mouseEvent->modifiers() == Qt::ShiftModifier)
			{
				d->isCtrlDown = true;
			}
			else
			{
				d->isCtrlDown = false;
			}
			d->isLeftBtn = true;
		}
		else
			d->isLeftBtn = false;
		mouseMoveEvent(mouseEvent);
	}
	void SliderLevelWindow::mouseReleaseEvent(QMouseEvent* mouseEvent) {
		Q_UNUSED(mouseEvent);
		d->isMouseDown = false;
	}
	void SliderLevelWindow::paintEvent(QPaintEvent* paintEvent) {
		Q_UNUSED(paintEvent);
		QPixmap pm(width(), height());
		pm.fill(this->palette().color(this->backgroundRole()));
		QPainter painter(&pm);

		painter.setFont(d->font);
		painter.setPen(this->palette().color(this->foregroundRole()));

		//QColor c(93, 144, 169);
		QColor c(39, 162, 191);
		QColor cl = c.lighter();
		QColor cd = c.darker();

		painter.setBrush(c);
		painter.drawRect(d->rectInfo);

		float mr = d->imgMax - d->imgMin;

		if (mr < 1) {
			mr = 1;
		}

		float fact = (float)d->moveHeight / mr;

		//begin draw scale
		if (d->scaleVisible)
		{
			int minRange = d->imgMin;
			int maxRange = d->imgMax;
			int yValue = d->moveHeight + (int)(minRange * fact);
			QString s = " 0";
			if (minRange <= 0 && maxRange >= 0)
			{
				painter.drawLine(5, yValue, 15, yValue);
				painter.drawText(21, yValue + 3, s);
			}

			int count = 1;
			int k = 5;
			bool enoughSpace = false;
			bool enoughSpace2 = false;

			double dStepSize = pow(10, floor(log10(mr / 100)) + 1);

			for (int i = d->moveHeight + (int)(minRange * fact); i < d->moveHeight;)//negative
			{
				if (-count * dStepSize < minRange)
					break;
				yValue = d->moveHeight + (int)((minRange + count * dStepSize) * fact);

				s = QString::number(-count * dStepSize / RI_FACTOR, 'f', 3);
				if (count % k && ((dStepSize * fact) > 2.5))
				{
					painter.drawLine(8, yValue, 12, yValue);
					enoughSpace = true;
				}
				else if (!(count % k))
				{
					if ((k * dStepSize * fact) > 7)
					{
						painter.drawLine(5, yValue, 15, yValue);
						painter.drawText(21, yValue + 3, s);
						enoughSpace2 = true;
					}
					else
					{
						k += 5;
					}
				}
				if (enoughSpace)
				{
					i = yValue;
					count++;
				}
				else if (enoughSpace2)
				{
					i = yValue;
					count += k;
				}
				else
				{
					i = yValue;
					count = k;
				}
			}
			count = 1;
			k = 5;
			enoughSpace = false;
			enoughSpace2 = false;

			for (int i = d->moveHeight + (int)(minRange * fact); i >= 0;)
			{
				if (count * dStepSize > maxRange)
					break;
				yValue = d->moveHeight + (int)((minRange - count * dStepSize) * fact);

				s = QString::number(count * dStepSize / RI_FACTOR, 'f', 3);
				if (count % k && ((dStepSize * fact) > 2.5))
				{
					if (!(minRange > 0 && (count * dStepSize) < minRange))
						painter.drawLine(8, yValue, 12, yValue);
					enoughSpace = true;
				}
				else if (!(count % k))
				{
					if ((k * dStepSize * fact) > 7)
					{
						if (!(minRange > 0 && (count * dStepSize) < minRange))
						{
							painter.drawLine(5, yValue, 15, yValue);
							painter.drawText(21, yValue + 3, s);
						}
						enoughSpace2 = true;
					}
					else
					{
						k += 5;
					}
				}
				if (enoughSpace)
				{
					i = yValue;
					count++;
				}
				else if (enoughSpace2)
				{
					i = yValue;
					count += k;
				}
				else
				{
					i = yValue;
					count = k;
				}
			}
		}
		//end draw scale
		painter.setPen(cl);
		painter.drawLine(d->rectInfo.topLeft(), d->rectInfo.topRight());
		painter.drawLine(d->rectInfo.topLeft(), d->rectInfo.bottomLeft());

		painter.setPen(cd);
		painter.drawLine(d->rectInfo.topRight(), d->rectInfo.bottomRight());
		painter.drawLine(d->rectInfo.bottomRight(), d->rectInfo.bottomLeft());
		painter.end();

		QPainter p(this);
		p.drawPixmap(0, 0, pm);
	}
	void SliderLevelWindow::resizeEvent(QResizeEvent* resizeEvent) {
		d->moveHeight = resizeEvent->size().height() - 25;
		update();
	}
}
