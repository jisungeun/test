#include "RetrieveDockWidget.h"

namespace TC {
    struct RetrieveDockWidget::Impl {
        int lastWidth = 0;
        int lastHeight = 0;

        bool topLevel = false;
    };

    RetrieveDockWidget::RetrieveDockWidget(QWidget* parent)
        : QDockWidget(parent)
        , d{ new Impl } {
        connect(this, SIGNAL(topLevelChanged(bool)), this, SLOT(onTopLevelChanged(bool)));
    }   

    RetrieveDockWidget::~RetrieveDockWidget() {
        
    }

    auto RetrieveDockWidget::SetFloatingSize(const int& w, const int& h)->void {
        d->lastWidth = w;
        d->lastHeight = h;
    }

    void RetrieveDockWidget::onTopLevelChanged(bool topLevel) {
        if(0 > d->lastWidth || 0 > d->lastHeight) {
            return;
        }

        if (true == topLevel) {
            this->resize(d->lastWidth, d->lastHeight);
        }

        //d->topLevel = topLevel;
    }
    void RetrieveDockWidget::closeEvent(QCloseEvent* event) {
        event->ignore();
    }

    void RetrieveDockWidget::resizeEvent(QResizeEvent* event) {        

        QDockWidget::resizeEvent(event);
    }
}
