#include "PathComboBox.h"
#include "PathRegExp.h"

namespace TC::PathWidget {
    struct PathComboBox::Impl {
        PathRegExpValidator validator;
    };

    PathComboBox::PathComboBox(QWidget* parent) : QComboBox(parent), d(new Impl) {
        this->setValidator(&d->validator);
    }

    PathComboBox::~PathComboBox() = default;

    auto PathComboBox::SetMaxLength(int length) -> void {
        d->validator.SetMaxLength(length);
    }

    auto PathComboBox::GetMaxLength() const -> int {
        return d->validator.GetMaxLength();
    }
}
