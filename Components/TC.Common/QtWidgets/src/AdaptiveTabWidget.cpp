#include "AdaptiveTabWidget.h"

#include <iostream>
#include <QVariant>

namespace TC {
    TabBar::TabBar(QWidget* parent) : QTabBar(parent) {
        
    }
    QSize TabBar::tabSizeHint(int index) const {
        auto text = tabText(index);        
        auto hint = QTabBar::tabSizeHint(index);

        QFont font("Noto Sans KR",15,QFont::Bold);
        QFontMetrics fm(font);
        auto len = fm.horizontalAdvance(text);

        if(len < 100) {
            len = 100;
        }

        return QSize(len + 8,hint.height());//8px for left right margin
    }

    struct AdaptiveTabWidget::Impl {
        bool tabsVisible{ true };
    };

    AdaptiveTabWidget::AdaptiveTabWidget(QWidget* parent) : QTabWidget(parent), d{std::make_unique<Impl>()} {        
        setTabBar(new TabBar);
        tabBar()->setProperty("styleVariant", 1);
    }
    AdaptiveTabWidget::~AdaptiveTabWidget() {
        
    }

    auto AdaptiveTabWidget::SetVisible(bool visible) -> void {
        d->tabsVisible = visible;

        auto* bar = tabBar();
        if(!bar) return;

        const auto count = bar->count();
        for(auto idx=0; idx<count; idx++) {
            bar->setTabVisible(idx, d->tabsVisible);

        }
    }

    auto AdaptiveTabWidget::addTab(QWidget* page, const QString& label) -> int32_t {
        const auto idx = QTabWidget::addTab(page, label);

        auto* bar = tabBar();
        if(!bar) return idx;
        bar->setTabVisible(idx, d->tabsVisible);

        return idx;
    }

    auto AdaptiveTabWidget::addTab(QWidget* page, const QIcon& icon, const QString& label) -> int32_t {
        const auto idx = QTabWidget::addTab(page, icon, label);

        auto* bar = tabBar();
        if(!bar) return idx;
        bar->setTabVisible(idx, d->tabsVisible);

        return idx;
    }
}
