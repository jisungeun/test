#include "TCDockWidget.h"

#include <iostream>
#include <QMainWindow>
#include <QCloseEvent>

namespace TC {
    struct TCDockWidget::Impl {        
    };
    TCDockWidget::TCDockWidget(QString title, QWidget* parent) :QDockWidget(title,parent), d{ new Impl } {
        
    }

    TCDockWidget::TCDockWidget(QWidget* parent) : QDockWidget(parent), d{ new Impl } {
        
    }
    TCDockWidget::~TCDockWidget() {
        
    }

    void TCDockWidget::closeEvent(QCloseEvent* event) {        
        auto window = (QMainWindow*)(parent());
        if(window->isVisible()) {            
            event->ignore();
        }else {            
            event->accept();
        }                
    }
}
