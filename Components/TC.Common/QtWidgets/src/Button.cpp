#include "Button.h"

#include <QMap>

namespace TC {
    struct Button::Impl {
        QMap<QIcon::Mode, QIcon> icons;
    };

    Button::Button(QWidget* parent)
        : QPushButton(parent)
        , d{ new Impl } {

    }

    Button::~Button() {
        
    }

    auto Button::AddIcon(const QString& fileName, QIcon::Mode mode)->void {
        QIcon icon = QIcon(fileName);
        if (mode == QIcon::Normal) {
            setIcon(icon);
        }

        d->icons.insert(mode, icon);
    }

    auto Button::SetEnabled(bool enabled)->void {
        if (enabled) {
            if (d->icons.contains(QIcon::Normal)) {
                setIcon(d->icons.value(QIcon::Normal));
            }
        } else {
            if (d->icons.contains(QIcon::Disabled)) {
                setIcon(d->icons.value(QIcon::Disabled));
            }
        }
        
        setEnabled(enabled);
    }

    void Button::enterEvent(QEvent* event) {
        QPushButton::enterEvent(event);

        if (d->icons.contains(QIcon::Active)) {
            setIcon(d->icons.value(QIcon::Active));
        }
    }

    void Button::leaveEvent(QEvent* event) {
        QPushButton::leaveEvent(event);

        if (d->icons.contains(QIcon::Normal)) {
            setIcon(d->icons.value(QIcon::Normal));
        }
    }        
}