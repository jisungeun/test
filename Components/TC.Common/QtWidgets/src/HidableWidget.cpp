﻿#include "HidableWidget.h"

#include <QGroupBox>
#include <QTabBar>
#include <QToolButton>
#include <QTextCodec>


namespace TC {    
    HidableTabWidget::HidableTabWidget(QWidget* parent)
        :QTabWidget(parent),hideAction(this){
        //hideAction.setText(QString::fromUtf8("H"));
        hideAction.setIcon(QIcon(QPixmap(":/image/images/DownArrow.png")));
        hideAction.setCheckable(true);
        hideAction.setToolTip("Hide Panels");
        QToolButton* hideButton = new QToolButton();
        hideButton->setDefaultAction(&hideAction);
        hideButton->setAutoRaise(true);
        
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        this->setCornerWidget(hideButton);        

        connect(&hideAction, SIGNAL(toggled(bool)), this, SLOT(onHideAction(bool)));
        connect(this, SIGNAL(tabBarClicked(int)), this, SLOT(onTabBarClicked()));
    }
    auto HidableTabWidget::showTab(bool show) -> void {
        if (show)
            this->tabBar()->show();
        else
            this->tabBar()->hide();
    }
    void HidableTabWidget::onHideAction(bool checked)
    {
        if (checked)
        {
            this->setMaximumHeight(this->tabBar()->height());
            this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        }
        else
        {
            this->setMaximumHeight(10000); 
            this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        }
    }
    void HidableTabWidget::onTabBarClicked()
    {
        hideAction.setChecked(false);
    }
}