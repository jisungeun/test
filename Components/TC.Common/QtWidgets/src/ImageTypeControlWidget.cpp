#include "ui_ImageTypeControlWidget.h"
#include "ImageTypeControlWidget.h"

#include <qgraphicseffect.h>

#include "UIUtility.h"

namespace  TC {
    struct ImageTypeControlWidget::Impl {
        Ui::ImageTypeControlWidget* ui{ nullptr };
    };

    ImageTypeControlWidget::ImageTypeControlWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->ui = new Ui::ImageTypeControlWidget();
        d->ui->setupUi(this);

        auto oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.5);
        this->setGraphicsEffect(oe);
        this->setAutoFillBackground(true);

        //d->ui->bfButton->hide();

        d->ui->htButton->setEnabled(false);
        d->ui->flButton->setEnabled(false);
        d->ui->bfButton->setEnabled(false);
    }

    ImageTypeControlWidget::~ImageTypeControlWidget() {
        delete d->ui;
    }

    void ImageTypeControlWidget::SetEnableButtons(bool ht, bool fl, bool bf) const {
        d->ui->htButton->setEnabled(ht);
        d->ui->flButton->setEnabled(fl);
        d->ui->bfButton->setEnabled(bf);
    }

    void ImageTypeControlWidget::SetCurrentType(const int& type) const {
        switch (type) {
        case 0:
            d->ui->htButton->setChecked(true);
            break;
        case 1:
            d->ui->flButton->setChecked(true);
            break;
        case 2:
            d->ui->bfButton->setChecked(true);
            break;
        default: ;
        }
    }

    auto ImageTypeControlWidget::GetCurrentType() const ->int {
        if(true == d->ui->htButton->isChecked()) {
            return 0;
        }
        else if (true == d->ui->flButton->isChecked()) {
            return 1;
        }
        else if (true == d->ui->bfButton->isChecked()) {
            return 2;
        }

        return -1;
    }

    void ImageTypeControlWidget::on_htButton_toggled(bool checked) {
        if(false == checked) {
            return;
        }

        emit modalityButtonToggled(0);
    }

    void ImageTypeControlWidget::on_flButton_toggled(bool checked) {
        if (false == checked) {
            return;
        }

        emit modalityButtonToggled(1);
    }

    void ImageTypeControlWidget::on_bfButton_toggled(bool checked) {
        if (false == checked) {
            return;
        }

        emit modalityButtonToggled(2);
    }
}
