#include <QLayoutItem>
#include <QMouseEvent>
#include <QPushButton>

#include "CustomDialog.h"

#include "ui_CustomDialog.h"

namespace TC {
	struct CustomDialog::Impl {
		Ui::CustomDialog ui{};

		QPoint pos;
		QAbstractButton* lastClicked = nullptr;

		auto Collapse() const -> void {
			if (ui.titleLabel->text().isEmpty() && !ui.closeBtn->isVisible())
				ui.titleFrame->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
			else
				ui.titleFrame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);

			ui.controlFrame->setVisible(!ui.buttonBox->standardButtons().testFlag(StandardButton::NoButton));
			if (ui.buttonBox->standardButtons().testFlag(StandardButton::NoButton)) {
				ui.controlFrame->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
			} else {
				ui.controlFrame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
			}
		}

		auto GetMargin() const -> int {
			const auto geo = ui.contextFrame->geometry();
			const auto margin = ui.frame->contentsMargins() + ui.contextFrame->layout()->contentsMargins();
			return margin.left() + margin.right() + (geo.x() * 2) + 10;
		}
	};

	CustomDialog::CustomDialog(QWidget* parent, Qt::WindowFlags f) : QDialog(parent, f), d(new Impl) {
		d->ui.setupUi(this);

		setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
		setAttribute(Qt::WA_TranslucentBackground);

		d->ui.frame->setObjectName("tc-customdialog-frame");
		d->ui.controlFrame->setObjectName("tc-customdialog-controlframe");
		d->ui.contextFrame->setObjectName("tc-customdialog-contextframe");
		d->ui.titleFrame->setObjectName("tc-customdialog-titleframe");

		d->ui.closeBtn->setObjectName("tc-customdialog-close");
		d->ui.titleLabel->setObjectName("tc-customdialog-title");
		d->ui.buttonBox->setObjectName("tc-customdialog-buttonbox");

		connect(d->ui.closeBtn, &QPushButton::clicked, this, &QDialog::reject);
		connect(d->ui.buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
		connect(d->ui.buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
		connect(d->ui.buttonBox, &QDialogButtonBox::clicked, this, [this](QAbstractButton* btn) {
			d->lastClicked = btn;
			}
		);

		d->ui.closeBtn->setFocus();
		installEventFilter(this);
	}

	CustomDialog::~CustomDialog() = default;

	auto CustomDialog::GetTitle() const -> QString {
		return d->ui.titleLabel->text();
	}

	auto CustomDialog::GetContext() const -> QWidget* {
		if (!d->ui.contextFrame->layout()->isEmpty())
			return d->ui.contextFrame->layout()->itemAt(0)->widget();

		return {};
	}

	auto CustomDialog::GetDialogButtonBox() const -> QDialogButtonBox* {
		return d->ui.buttonBox;
	}

	auto CustomDialog::GetButton(StandardButton button) const -> QPushButton* {
		return d->ui.buttonBox->button(button);
	}

	auto CustomDialog::IsClosable() const -> bool {
		return d->ui.closeBtn->isEnabled();
	}

	auto CustomDialog::SetTitle(const QString& title) -> void {
		d->ui.titleLabel->setText(title);

		const auto policy = !IsClosable() && d->ui.titleLabel->text().isEmpty() ? QSizePolicy::Ignored : QSizePolicy::Preferred;
		d->ui.titleFrame->setSizePolicy(policy, policy);
		d->Collapse();
	}

	auto CustomDialog::SetContext(QWidget* context) -> void {
		while (!d->ui.contextFrame->layout()->isEmpty()) {
			auto* item = d->ui.contextFrame->layout()->takeAt(0);

			delete item->widget();
			delete item;
		}

		if (context) {
			context->setMaximumWidth(this->maximumWidth() - d->GetMargin());

			d->ui.contextFrame->layout()->addWidget(context);
		}

		UpdateSize();
	}

	auto CustomDialog::SetClosable(bool closable) -> void {
		d->ui.closeBtn->setEnabled(closable);
		d->ui.closeBtn->setVisible(closable);

		const auto policy = !IsClosable() && d->ui.titleLabel->text().isEmpty() ? QSizePolicy::Ignored : QSizePolicy::Preferred;
		d->ui.titleFrame->setSizePolicy(policy, policy);
		d->Collapse();
	}

	auto CustomDialog::SetStandardButtons(StandardButtons buttons) -> void {
		d->ui.buttonBox->setStandardButtons(buttons);
		d->Collapse();
	}

	auto CustomDialog::SetDefaultButton(StandardButton button) -> void {
		auto btns = d->ui.buttonBox->buttons();

		if (auto* btn = d->ui.buttonBox->button(button)) {
			btn->setFocus();
		}
	}

	auto CustomDialog::GetLastClickedButton() -> StandardButton {
		if (d->lastClicked)
			return d->ui.buttonBox->standardButton(d->lastClicked);
		return StandardButton::NoButton;
	}

	auto CustomDialog::mousePressEvent(QMouseEvent* event) -> void {
		QDialog::mousePressEvent(event);

		if (event->pos().y() < d->ui.titleFrame->geometry().bottom())
			d->pos = event->pos();
	}

	auto CustomDialog::mouseReleaseEvent(QMouseEvent* event) -> void {
		QDialog::mouseReleaseEvent(event);
		d->pos = QPoint(0, 0);
	}

	auto CustomDialog::mouseMoveEvent(QMouseEvent* event) -> void {
		if (d->pos != QPoint(0, 0))
			if (auto* window = this->window())
				window->move(window->pos() + (event->pos() - d->pos));

		QDialog::mouseMoveEvent(event);
	}

	auto CustomDialog::eventFilter(QObject* object, QEvent* event) -> bool {
		if (event->type() == QEvent::KeyPress) {
			if (!d->ui.closeBtn->isEnabled()) {
				const auto* keyEvent = dynamic_cast<QKeyEvent*>(event);

				switch (keyEvent->key()) {
				case Qt::Key_Escape:
					return true;
				}
			}
		}

		return QDialog::eventFilter(object, event);
	}

	auto CustomDialog::UpdateSize() -> void {
		const auto width = GetMinimumWidth() + d->GetMargin();
		resize((width > maximumWidth()) ? maximumWidth() : width, 0);
	}

	auto CustomDialog::GetMinimumWidth() const -> int {
		return d->ui.contextFrame->minimumSizeHint().width();
	}
}
