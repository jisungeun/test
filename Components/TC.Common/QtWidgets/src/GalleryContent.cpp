#include <QPainter>
#include <QPair>
#include <QVariantAnimation>
#include <QLinearGradient>
#include <QSequentialAnimationGroup>

#include "GalleryContent.h"

namespace TC {
	struct GalleryContent::Impl {
		QPixmap pixmap;
		QMap<QString, GalleryLayer> layers;
		bool visibility = true;
	};

	GalleryContent::GalleryContent(QObject* parent) : QObject(parent), d(new Impl) {}

	GalleryContent::~GalleryContent() = default;

	GalleryContent::GalleryContent(GalleryContent&& obj) noexcept : d(std::move(obj.d)) {}

	GalleryContent::GalleryContent(const GalleryContent& obj) : d(new Impl{ *obj.d }) {}

	auto GalleryContent::operator=(const GalleryContent& obj) -> GalleryContent& {
		if (this == &obj) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	auto GalleryContent::operator=(GalleryContent&& obj) noexcept -> GalleryContent& {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	auto GalleryContent::SetVisibility(bool visibility) -> void {
		d->visibility = visibility;
	}

	auto GalleryContent::GetVisibility() const -> bool {
		return d->visibility;
	}

	auto GalleryContent::SetPixmap(const QPixmap& pixmap) -> void {
		d->pixmap = pixmap;
		emit sigUpdated();
	}

	auto GalleryContent::SetPixmap(QPixmap&& pixmap) -> void {
		d->pixmap = std::move(pixmap);
		emit sigUpdated();
	}

	auto GalleryContent::GetPixmap() const -> QPixmap& {
		return d->pixmap;
	}

	auto GalleryContent::CreateLayer(const QString& name) -> GalleryLayer* {
		if (d->layers.contains(name))
			return &d->layers[name];
		
		d->layers[name] = { this };
		connect(&d->layers[name], &GalleryLayer::sigUpdated, this, [this] { emit sigUpdated(); });
		return &d->layers[name];
	}

	auto GalleryContent::RemoveLayer(const QString& name) -> void {
		d->layers.remove(name);
		emit sigUpdated();
	}

	auto GalleryContent::GetLayer(const QString& name) const -> GalleryLayer* {
		if (d->layers.contains(name))
			return &d->layers[name];
		return nullptr;
	}

	auto GalleryContent::ContainsLayer(const QString& name) const -> bool {
		return d->layers.contains(name);
	}

	auto GalleryContent::GetLayerNames() const -> QStringList {
		QStringList list;

		for (const auto & l : d->layers.keys())
			list << l;

		return list;
	}
}
