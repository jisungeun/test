#include <QPushButton>
#include <QLayout>

#include "ProgressDialog.h"

#include "ui_ProgressDialog.h"

namespace TC {
	struct ProgressDialog::Impl {
		Ui::ProgressDialog ui{};
		QFrame* frame = nullptr;

		bool closable = true;
		bool canceled = false;
		bool autoClose = true;
		bool autoReset = true;
	};

	ProgressDialog::ProgressDialog(QWidget* parent) : CustomDialog(parent, Qt::WindowFlags()), d(new Impl) {
		d->frame = new QFrame(this);
		d->ui.setupUi(d->frame);
		SetClosable(false);
		SetContext(d->frame);

		d->ui.progressBar->setObjectName("tc-progressdialog-progressbar");
		d->ui.contentLabel->setObjectName("tc-progressdialog-content");

		SetStandardButtons(StandardButton::Cancel);
		SetDefaultButton(StandardButton::Cancel);

		connect(d->ui.progressBar, &QProgressBar::valueChanged, this, &ProgressDialog::OnProgressValueChanged);
		connect(GetDialogButtonBox(), &QDialogButtonBox::clicked,
			[this](QAbstractButton* button) -> void {
				if (GetDialogButtonBox()->standardButton(button) == StandardButton::Cancel)
					cancel();
			}
		);
	}

	ProgressDialog::ProgressDialog(const QString& content, QWidget* parent) : ProgressDialog(parent) {
		SetContent(content);
	}

	ProgressDialog::ProgressDialog(const QString& content, const QString& cancelButtonText, int minimum, int maximum, QWidget* parent) : ProgressDialog(content, parent) {
		setRange(minimum, maximum);
		GetButton(StandardButton::Cancel)->setText(cancelButtonText);
	}

	ProgressDialog::~ProgressDialog() = default;

	auto ProgressDialog::GetContent() const -> QString {
		return d->ui.contentLabel->text();
	}

	auto ProgressDialog::SetContent(const QString& content) -> void {
		d->ui.contentLabel->setText(content);
		UpdateSize();
	}

	auto ProgressDialog::cancel() -> void {
		d->canceled = true;
		hide();
		emit canceled();
	}

	auto ProgressDialog::reset() -> void {
		d->ui.progressBar->setValue(minimum());
	}

	auto ProgressDialog::setMaximum(int maximum) -> void {
		d->ui.progressBar->setMaximum(maximum);
	}

	auto ProgressDialog::setMinimum(int minimum) -> void {
		d->ui.progressBar->setMinimum(minimum);
	}

	auto ProgressDialog::setRange(int minimum, int maximum) -> void {
		setMinimum(minimum);
		setMaximum(maximum);
	}

	auto ProgressDialog::setValue(int progress) -> void {
		d->ui.progressBar->setValue(progress);
	}

	auto ProgressDialog::wasCanceled() const -> bool {
		return d->canceled;
	}

	auto ProgressDialog::minimum() const -> int {
		return d->ui.progressBar->minimum();
	}

	auto ProgressDialog::maximum() const -> int {
		return d->ui.progressBar->maximum();
	}

	auto ProgressDialog::value() const -> int {
		return d->ui.progressBar->value();
	}

	auto ProgressDialog::setAutoReset(bool reset) -> void {
		d->autoReset = reset;
	}

	auto ProgressDialog::autoReset() const -> bool {
		return d->autoReset;
	}

	auto ProgressDialog::setAutoClose(bool close) -> void {
		d->autoClose = close;
	}

	auto ProgressDialog::autoClose() const -> bool {
		return d->autoClose;
	}

	auto ProgressDialog::OnProgressValueChanged(int value) -> void {
		if (value == maximum()) {
			if (d->autoClose)
				this->close();

			if (d->autoReset)
				reset();
		}
	}

	int ProgressDialog::GetMinimumWidth() const {
		d->ui.contentLabel->setWordWrap(false);
		const auto width = d->frame->minimumSizeHint().width();
		d->ui.contentLabel->setWordWrap(true);

		return width;
	}

	auto ProgressDialog::GetBar() const -> QProgressBar* {
		return d->ui.progressBar;
	}
}
