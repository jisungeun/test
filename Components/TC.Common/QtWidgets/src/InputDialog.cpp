#include <QLayout>
#include <QMouseEvent>
#include <QGraphicsEffect>
#include <QPushButton>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "InputDialog.h"

#include "ui_InputDialog.h"

namespace TC {
	struct InputDialog::Impl {
		Ui::InputDialog ui{};
		QFrame* frame = nullptr;
		
		InputMode mode = InputMode::TextInput;
	};

	InputDialog::InputDialog(QWidget* parent) : CustomDialog(parent, Qt::WindowFlags()), d(std::make_unique<Impl>()) {
		d->frame = new QFrame(this);
		d->ui.setupUi(d->frame);
		SetContext(d->frame);
		SetClosable(false);
		SetStandardButtons(StandardButton::Ok | StandardButton::Cancel);
		SetDefaultButton(StandardButton::Ok);

		d->ui.contentLabel->setObjectName("tc-inputdialog-content");
		d->ui.lineEdit->setObjectName("tc-inputdialog-lineedit");
		d->ui.comboBox->setObjectName("tc-inputdialog-combobox");
		d->ui.spinBox->setObjectName("tc-inputdialog-spinbox");
		d->ui.doubleSpinBox->setObjectName("tc-inputdialog-doublespinbox");
		
		connect(GetButton(StandardButton::Ok), &QPushButton::clicked, this, &QDialog::accept);
		connect(GetButton(StandardButton::Cancel), &QPushButton::clicked, this, &QDialog::reject);
		connect(d->ui.lineEdit, &QLineEdit::textChanged, this, [this](const QString& value) -> void { emit textValueChanged(value); });
		connect(d->ui.comboBox, &QComboBox::currentTextChanged, this, [this](const QString& value) -> void { emit textValueChanged(value); });
		connect(d->ui.spinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int value) -> void { emit intValueChanged(value); });
		connect(d->ui.doubleSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double value) -> void { emit doubleValueChanged(value); });
		connect(this, &QDialog::accepted, this,
			[this] {
				switch (d->mode) {
				case InputMode::TextInput:
					emit textValueSelected(d->ui.lineEdit->text());
					break;
				case InputMode::ComboInput:
					emit textValueSelected(d->ui.comboBox->currentText());
					break;
				case InputMode::IntInput:
					emit intValueSelected(d->ui.spinBox->value());
					break;
				case InputMode::DoubleInput:
					emit doubleValueSelected(d->ui.doubleSpinBox->value());
					break;
				}
			}
		);
		
		setInputMode(InputMode::TextInput);
		d->ui.lineEdit->setFocus();
	}

	InputDialog::InputDialog(const QString& content, QWidget* parent) : InputDialog(parent) {
		SetContent(content);
	}

	InputDialog::InputDialog(const QString& title, const QString& content, QWidget* parent) : InputDialog(content, parent) {
		SetTitle(title);

		installEventFilter(this);
	}

	InputDialog::~InputDialog() = default;

	auto InputDialog::GetContent() const -> QString {
		return d->ui.contentLabel->text();
	}

	auto InputDialog::SetContent(const QString& content) -> void {
		d->ui.contentLabel->setText(content);
		UpdateSize();
	}

	auto InputDialog::setInputMode(InputMode mode) -> void {
		d->mode = mode;

		d->ui.spinBox->setVisible(false);
		d->ui.doubleSpinBox->setVisible(false);
		d->ui.comboBox->setVisible(false);
		d->ui.lineEdit->setVisible(false);

		switch (mode) {
		case InputMode::TextInput:
			d->ui.lineEdit->setVisible(true);
			d->ui.lineEdit->setFocus();
			break;
		case InputMode::ComboInput:
			d->ui.comboBox->setVisible(true);
			d->ui.comboBox->setFocus();
			break;
		case InputMode::IntInput:
			d->ui.spinBox->setVisible(true);
			d->ui.spinBox->setFocus();
			break;
		case InputMode::DoubleInput:
			d->ui.doubleSpinBox->setVisible(true);
			d->ui.doubleSpinBox->setFocus();
			break;
		}
	}

	auto InputDialog::inputMode() const -> InputMode {
		return d->mode;
	}

	void InputDialog::setTextValue(const QString& text) {
		switch (d->mode) {
		case InputMode::TextInput:
			d->ui.lineEdit->setText(text);
			break;
		case InputMode::ComboInput:
			d->ui.comboBox->setCurrentText(text);
			break;
		default:
			break;
		}
	}

	QString InputDialog::textValue() const {
		switch (d->mode) {
		case InputMode::TextInput:
			return d->ui.lineEdit->text();
		case InputMode::ComboInput:
			return d->ui.comboBox->currentText();
		default:
			return {};
		}
	}

	void InputDialog::setTextEchoMode(QLineEdit::EchoMode mode) {
		d->ui.lineEdit->setEchoMode(mode);
	}

	QLineEdit::EchoMode InputDialog::textEchoMode() const {
		return d->ui.lineEdit->echoMode();
	}

	void InputDialog::setComboBoxEditable(bool editable) {
		d->ui.comboBox->setEditable(editable);
	}

	bool InputDialog::isComboBoxEditable() const {
		return d->ui.comboBox->isEditable();
	}

	void InputDialog::setComboBoxItems(const QStringList& items) {
		d->ui.comboBox->clear();
		d->ui.comboBox->addItems(items);
	}

	QStringList InputDialog::comboBoxItems() const {
		QStringList list;

		for (int i = 0; i < d->ui.comboBox->count(); i++)
			list << d->ui.comboBox->itemText(i);

		return list;
	}

	void InputDialog::setIntValue(int value) {
		d->ui.spinBox->setValue(value);
	}

	int InputDialog::intValue() const {
		return d->ui.spinBox->value();
	}

	void InputDialog::setIntMinimum(int min) {
		d->ui.spinBox->setMinimum(min);
	}

	int InputDialog::intMinimum() const {
		return d->ui.spinBox->minimum();
	}

	void InputDialog::setIntMaximum(int max) {
		d->ui.spinBox->setMaximum(max);
	}

	int InputDialog::intMaximum() const {
		return d->ui.spinBox->maximum();
	}

	void InputDialog::setIntRange(int min, int max) {
		d->ui.spinBox->setRange(min, max);
	}

	void InputDialog::setIntStep(int step) {
		d->ui.spinBox->setSingleStep(step);
	}

	int InputDialog::intStep() const {
		return d->ui.spinBox->singleStep();
	}

	void InputDialog::setDoubleValue(double value) {
		return d->ui.doubleSpinBox->setValue(value);
	}

	double InputDialog::doubleValue() const {
		return d->ui.doubleSpinBox->value();
	}

	void InputDialog::setDoubleMinimum(double min) {
		d->ui.doubleSpinBox->setMinimum(min);
	}

	double InputDialog::doubleMinimum() const {
		return d->ui.doubleSpinBox->minimum();
	}

	void InputDialog::setDoubleMaximum(double max) {
		d->ui.doubleSpinBox->setMaximum(max);
	}

	double InputDialog::doubleMaximum() const {
		return d->ui.doubleSpinBox->maximum();
	}

	void InputDialog::setDoubleRange(double min, double max) {
		d->ui.doubleSpinBox->setRange(min, max);
	}

	void InputDialog::setDoubleDecimals(int decimals) {
		d->ui.doubleSpinBox->setDecimals(decimals);
	}

	int InputDialog::doubleDecimals() const {
		return d->ui.doubleSpinBox->decimals();
	}

	void InputDialog::setDoubleStep(double step) {
		d->ui.doubleSpinBox->setSingleStep(step);
	}

	double InputDialog::doubleStep() const {
		return d->ui.doubleSpinBox->singleStep();
	}

	void InputDialog::setOkButtonText(const QString& text) {
		GetButton(StandardButton::Ok)->setText(text);
	}

	QString InputDialog::okButtonText() const {
		return GetButton(StandardButton::Ok)->text();
	}

	void InputDialog::setCancelButtonText(const QString& text) {
		GetButton(StandardButton::Cancel)->setText(text);
	}

	QString InputDialog::cancelButtonText() const {
		return GetButton(StandardButton::Cancel)->text();
	}

	QString InputDialog::getText(QWidget* parent, const QString& title, const QString& label, QLineEdit::EchoMode echo,
		const QString& text, bool* ok, Qt::WindowFlags flags, Qt::InputMethodHints inputMethodHints) {
		Q_UNUSED(flags)

		InputDialog dialog(title, label, parent);

		dialog.setTextEchoMode(echo);
		dialog.setTextValue(text);
		dialog.setInputMethodHints(inputMethodHints);
		const auto result = dialog.exec();
		if (ok)
			*ok = result == QDialog::Accepted;
		return dialog.textValue();
	}

	QString InputDialog::getItem(QWidget* parent, const QString& title, const QString& label, const QStringList& items,
		int current, bool editable, bool* ok, Qt::WindowFlags flags, Qt::InputMethodHints inputMethodHints) {
		Q_UNUSED(flags)

		InputDialog dialog(title, label, parent);
		dialog.d->ui.comboBox->setCurrentIndex(current);
		dialog.setInputMode(InputMode::ComboInput);
		dialog.setComboBoxItems(items);
		dialog.setComboBoxEditable(editable);
		dialog.setInputMethodHints(inputMethodHints);
		const auto result = dialog.exec();
		if (ok)
			*ok = result == QDialog::Accepted;
		return dialog.textValue();
	}

	int InputDialog::getInt(QWidget* parent, const QString& title, const QString& label, int value, int minValue,
		int maxValue, int step, bool* ok, Qt::WindowFlags flags) {
		Q_UNUSED(flags)

		InputDialog dialog(title, label, parent);
		dialog.setInputMode(InputMode::IntInput);
		dialog.setIntValue(value);
		dialog.setIntRange(minValue, maxValue);
		dialog.setIntStep(step);
		const auto result = dialog.exec();
		if (ok)
			*ok = result == QDialog::Accepted;
		return dialog.intValue();
	}

	double InputDialog::getDouble(QWidget* parent, const QString& title, const QString& label, double value,
		double minValue, double maxValue, int decimals, bool* ok, Qt::WindowFlags flags) {
		Q_UNUSED(flags)

		InputDialog dialog(title, label, parent);
		dialog.setInputMode(InputMode::DoubleInput);
		dialog.setDoubleValue(value);
		dialog.setDoubleRange(minValue, maxValue);
		dialog.setDoubleDecimals(decimals);
		const auto result = dialog.exec();
		if (ok)
			*ok = result == QDialog::Accepted;
		return dialog.doubleValue();
	}

	double InputDialog::getDouble(QWidget* parent, const QString& title, const QString& label, double value,
		double minValue, double maxValue, int decimals, bool* ok, Qt::WindowFlags flags, double step) {
		Q_UNUSED(flags)

		InputDialog dialog(title, label, parent);
		dialog.setInputMode(InputMode::DoubleInput);
		dialog.setDoubleValue(value);
		dialog.setDoubleRange(minValue, maxValue);
		dialog.setDoubleDecimals(decimals);
		dialog.setDoubleStep(step);
		const auto result = dialog.exec();
		if (ok)
			*ok = result == QDialog::Accepted;
		return dialog.doubleValue();
	}

	void InputDialog::done(int result) {
		QDialog::done(result);
	}

	bool InputDialog::eventFilter(QObject* object, QEvent* event) {
		if (event->type() == QEvent::KeyPress) {
			const auto* keyEvent = dynamic_cast<QKeyEvent*>(event);

			switch (keyEvent->key()) {
			case Qt::Key_Enter:
			case Qt::Key_Return:
				if (focusWidget() == d->ui.lineEdit ||
					focusWidget() == d->ui.doubleSpinBox ||
					focusWidget() == d->ui.spinBox) {
					accept();
				}
				return true;
			default:
				CustomDialog::eventFilter(object, event);
			}
		}

		return CustomDialog::eventFilter(object, event);
	}

	int InputDialog::GetMinimumWidth() const {
		d->ui.contentLabel->setWordWrap(false);
		const auto width = d->frame->minimumSizeHint().width();
		d->ui.contentLabel->setWordWrap(true);

		return width;
	}
}
