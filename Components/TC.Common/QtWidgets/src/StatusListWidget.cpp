#include <QMap>
#include <QBoxLayout>
#include <QListWidget>

#include "StatusListWidget.h"

namespace TC {
    struct StatusListWidget::Impl {
        enum {
            indexRole = Qt::UserRole + 1,
            userDataRole = Qt::UserRole + 2
        };

        QListWidget* listWidget{ nullptr };

        QMap<Status, QBrush> backgrounds;
        QMap<Status, QString> tooltips;
        QMap<Status, QIcon> icons;

        QList<QListWidgetItem*> itemsList;
        QList<Status> statusList;
    };

    StatusListWidget::StatusListWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        auto layout = new QVBoxLayout();
        layout->setContentsMargins(0, 0, 0, 0);
        {
            d->listWidget = new QListWidget(this);
            layout->addWidget(d->listWidget);
        }
        setLayout(layout);

        connect(d->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onItemClicked(QListWidgetItem*)));
        connect(d->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(onItemDoubleClicked(QListWidgetItem*)));
        connect(d->listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(onSelectionChanged()));
    }

    StatusListWidget::~StatusListWidget() {
    }

    auto StatusListWidget::SetBackground(Status status, const QBrush& brush)->void {
        d->backgrounds[status] = brush;
    }

    auto StatusListWidget::BeginUpdate()->void {
        //d->listWidget->setUpdatesEnabled(false);
    }

    auto StatusListWidget::EndUpdate() -> void {
        //d->listWidget->setUpdatesEnabled(true);
    }

    auto StatusListWidget::SetStatusTooltip(Status status, const QString& tooltip)->void {
        d->tooltips[status] = tooltip;
    }

    auto StatusListWidget::SetStatusIcon(Status status, const QIcon& icon)->void {
        d->icons[status] = icon;
    }

    auto StatusListWidget::AddItem(const QString& label, Status status)->Index {
        auto item = new QListWidgetItem(label);

        UpdateItem(item, status);

        d->listWidget->addItem(item);
        d->itemsList.append(item);
        d->statusList.append(status);

        const Index index = (d->itemsList.length() - 1);
        item->setData(d->indexRole, index);

        return index;
    }

    auto StatusListWidget::ClearItems() -> void {
        d->listWidget->clear();
        d->itemsList.clear();
        d->statusList.clear();
    }

    auto StatusListWidget::SetUserData(Index index, const QVariant userData) -> void {
        if (index >= d->itemsList.length()) return;
        auto item = d->itemsList.at(index);
        item->setData(d->userDataRole, userData);
    }

    auto StatusListWidget::GetUserData(Index index) const -> QVariant {
        if (index >= d->itemsList.length()) return QVariant();
        auto item = d->itemsList.at(index);
        return item->data(d->userDataRole);
    }

    auto StatusListWidget::UpdateStatus(Index index, Status status) -> void {
        if (index >= d->itemsList.length()) return;

        auto item = d->itemsList.at(index);
        const auto currentStatus = d->statusList.at(index);
        if (currentStatus == status) return;

        d->statusList[index] = status;

        UpdateItem(item, status);
    }

    auto StatusListWidget::UpdateStatus(const QString& label, Status status) -> void {
        for(auto item : d->itemsList) {
            if (label != item->text()) continue;
            const auto index = static_cast<Index>(item->data(d->indexRole).toInt());
            UpdateStatus(index, status);
        }
    }

    auto StatusListWidget::GetStatus(Index index) -> Status {
        if (d->statusList.contains(index)) return d->statusList[index];
        return -1;
    }

    auto StatusListWidget::GetText(Index index) -> QString {
        if (index < d->itemsList.size()) return d->itemsList.at(index)->text();
        return "";
    }

    auto StatusListWidget::Selected() const -> Index {
        const auto item = d->listWidget->currentItem();
        if (!item) return -1;
        return static_cast<Index>(item->data(d->indexRole).toInt());
    }

    auto StatusListWidget::SelectedItems() const -> QList<Index> {
        QList<Index> selected;
        auto selectedItems = d->listWidget->selectedItems();
        for(auto item : selectedItems) {
            selected.append(static_cast<Index>(item->data(d->indexRole).toInt()));
        }
        return selected;
    }

    void StatusListWidget::onItemClicked(QListWidgetItem* item) {
        auto index = item->data(d->indexRole).toInt();
        emit itemClicked(index);
    }

    void StatusListWidget::onItemDoubleClicked(QListWidgetItem* item) {
        auto index = item->data(d->indexRole).toInt();
        emit itemDoubleClicked(index);
    }

    void StatusListWidget::onSelectionChanged() {
        auto items = d->listWidget->selectedItems();
        if (items.size() == 0) return;
        auto item = items.at(0);
        auto index = item->data(d->indexRole).toInt();
        emit itemClicked(index);
    }

    auto StatusListWidget::UpdateItem(QListWidgetItem* item, Status status) -> void {
        if (!item) return;

        if (d->icons.contains(status)) item->setIcon(d->icons[status]);
        if (d->backgrounds.contains(status)) item->setBackground(d->backgrounds[status]);
        if (d->tooltips.contains(status)) item->setToolTip(d->tooltips[status]);
    }
}
