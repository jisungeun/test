#include "PathRegExp.h"

namespace TC::PathWidget {
	constexpr const char* ARGUMENTABLE_REGEX = R"((?=^[^\s.\\/:*?"<>|])[^\\/:*?"<>|]{0,%1}[^.\s\\/:*?"<>|]$)";
	constexpr const char* DEFAULT_REGEX = R"((?=^[^\s.\\/:*?"<>|])[^\\/:*?"<>|]{0,255}[^.\s\\/:*?"<>|]$)";

	struct PathRegExp::Impl {
		int length = -1;

        [[nodiscard]] auto ToRegex() const -> QString {
			if (length > 0) {
				QString regex = ARGUMENTABLE_REGEX;
				regex = regex.arg(length);
				return regex;
			} else {
				return DEFAULT_REGEX;
			}
		}
	};

	PathRegExp::PathRegExp() : QRegExp(), d(new Impl) {
		this->setPattern(d->ToRegex());
	}

	PathRegExp::~PathRegExp() = default;

	auto PathRegExp::SetMaxLength(int length) -> void {
		d->length = length;
		this->setPattern(d->ToRegex());
	}

	auto PathRegExp::GetMaxLength() const -> int {
		return d->length;
	}

	struct PathRegExpValidator::Impl {
		PathRegExp reg;
	};

	PathRegExpValidator::PathRegExpValidator(QObject* parent) : QRegExpValidator(parent), d(new Impl) {
		this->setRegExp(d->reg);
	}

	PathRegExpValidator::~PathRegExpValidator() = default;

	auto PathRegExpValidator::SetMaxLength(int length) -> void {
		d->reg.SetMaxLength(length);
	}

	auto PathRegExpValidator::GetMaxLength() const -> int {
		return d->reg.GetMaxLength();
	}
}
