#include <QPainter>
#include <QResizeEvent>

#include "GalleryPainter.h"

namespace TC {
	struct Layer {
		QPixmap pixmap;
		bool visibility = false;
	};

	struct GalleryPainter::Impl {
		QMap<int, GalleryContent> contents;
		QMap<QString, Layer> layers;
		int count = 0;

		int spacing = 5;
		int colCount = 3;
		int itemSize = 0;

		QLinearGradient gradient;
		bool animative = true;

		mutable std::pair<int, int> visible;
		mutable bool resizing = false;
		mutable bool paintedAll = false;
		mutable int hovered = -1;
		mutable int pressed = -1;

		Impl() {
			gradient.setColorAt(0.0, { 144, 144, 144, 30 });
			gradient.setColorAt(0.5, { 144, 144, 144, 120 });
			gradient.setColorAt(1.0, { 144, 144, 144, 30 });
		}

		auto UpdateItemSize(int width) -> void {
			const int size = (width - (spacing * (colCount - 1))) / colCount;
			itemSize = std::max(1, size);
		}

		auto GetPreferedHeight() const -> int {
			const int itemHeight = itemSize * ((count + colCount - 1) / colCount);
			const int spaceHeight = spacing * ((count - 1) / colCount);

			return std::max(itemHeight + spaceHeight, 0);
		}

		auto GetIndexOfPosition(int xPos, int yPos, int width) const -> int {
			const int size = (width + spacing) / colCount;

			const auto xIndex = static_cast<int>(xPos / static_cast<double>(size));
			const auto yIndex = static_cast<int>(yPos / static_cast<double>(size));

			if (xPos % size < itemSize && yPos % size < itemSize)
				return yIndex * colCount + xIndex;

			return -1;
		}
	};

	GalleryPainter::GalleryPainter(QWidget* parent) : QWidget(parent), d(std::make_unique<Impl>()) {
		setMouseTracking(true);
	}

	GalleryPainter::~GalleryPainter() = default;

	auto GalleryPainter::SetSpacing(int spacing) -> void {
		d->spacing = spacing;
	}

	auto GalleryPainter::GetSpacing() const -> int {
		return d->spacing;
	}

	auto GalleryPainter::SetColumnCount(int count) -> void {
		d->colCount = count;
		d->UpdateItemSize(width());
		this->setFixedHeight(d->GetPreferedHeight());
	}

	auto GalleryPainter::GetColumnCount() const -> int {
		return d->colCount;
	}

	auto GalleryPainter::Add(int size) -> void {
		d->count += size;
		this->setFixedHeight(d->GetPreferedHeight());
	}

	auto GalleryPainter::Insert(int index, int size) -> void {
		Q_UNUSED(index)

		const auto oldSize = d->count;
		d->count += size;

		for (int i = 0; i < size; i++) {
			if (d->contents.contains(oldSize - i - 1)) {
				d->contents[d->count - i - 1] = d->contents[oldSize - i - 1];
				d->contents.remove(oldSize - i - 1);
			}
		}

		this->setFixedHeight(d->GetPreferedHeight());
	}

	auto GalleryPainter::RemoveAt(int index) -> void {
		if (index < 0 && index >= d->count)
			return;

		if (d->contents.contains(index)) {
			d->contents.remove(index);
			this->setFixedHeight(d->GetPreferedHeight());
		}
	}

	auto GalleryPainter::Clear() -> void {
		d->count = 0;
		d->contents.clear();
	}

	auto GalleryPainter::IndexOf(const GalleryContent* content) const -> int {
		for (auto i : d->contents.keys()) {
			if (&d->contents[i] == content)
				return i;
		}

		return -1;
	}

	auto GalleryPainter::GetItemCount() const -> int {
		return d->count;
	}

	auto GalleryPainter::Contains(int index) const -> bool {
		return d->contents.contains(index);
	}

	auto GalleryPainter::Get(int index) const -> GalleryContent* {
		if (index < 0 || index >= d->count)
			return nullptr;

		if (d->contents.contains(index))
			return &d->contents[index];

		for (const auto& l : d->layers.keys()) {
			auto* layer = d->contents[index].CreateLayer(l);
			layer->SetPixmap(d->layers[l].pixmap);
			layer->SetVisibility(d->layers[l].visibility);
		}

		return &d->contents[index];
	}

	auto GalleryPainter::GetItemSize() const -> int {
		return d->itemSize;
	}

	auto GalleryPainter::GetItemPosition(int index) const -> QPoint {
		const int size = (width() + d->spacing) / d->colCount;

		int x = (index % d->colCount) * size;
		int y = (index / d->colCount) * size;

		return { x, y };
	}

	auto GalleryPainter::SetGradientPosition(qreal pos, int width) -> void {
		d->gradient.setStart(-width, pos - width);
		d->gradient.setFinalStop(width, pos + width);
	}

	auto GalleryPainter::PaintedAll() const -> bool {
		return d->paintedAll;
	}

	auto GalleryPainter::AddDefaultLayer(const QString& name, const QPixmap& pixmap, bool visibility) const -> void {
		for (auto& key : d->contents.keys()) {
			auto& content = d->contents[key];
			auto* layer = content.CreateLayer(name);
			layer->SetPixmap(pixmap);
			layer->SetVisibility(visibility);
		}

		d->layers[name] = { pixmap, visibility };
	}

	auto GalleryPainter::GetDefaultLayerNames() const -> QStringList {
		QStringList list;

		for (const auto& name : d->layers.keys())
			list << name;

		return list;
	}

	auto GalleryPainter::RemoveDefaultLayer(const QString& name) -> void {
		d->layers.remove(name);
	}

	auto GalleryPainter::SetVisibleIndex(std::pair<int, int> range) -> void {
		d->visible = range;
	}

	auto GalleryPainter::GetVisibleIndex() const -> std::pair<int, int> {
		return d->visible;
	}

	auto GalleryPainter::SetAnimative(bool animative) -> void {
		d->animative = animative;
	}

	auto GalleryPainter::Animative() const -> bool {
		return d->animative;
	}

	auto GalleryPainter::paintEvent(QPaintEvent* paintEvent) -> void {
		QWidget::paintEvent(paintEvent);
		QPainter painter(this);
		painter.setPen(Qt::PenStyle::NoPen);

		for (int i = d->visible.first; i < d->visible.second; i++) {
			const auto point = GetItemPosition(i);

			if (d->contents.contains(i)) {
				bool painted = false;

				if (!d->contents[i].GetPixmap().isNull() && d->contents[i].GetVisibility()) {
					const auto& pixmap = d->contents[i].GetPixmap();

					painter.drawPixmap(point.x(), point.y(), d->itemSize, d->itemSize, pixmap);
					d->paintedAll = true;
					painted = true;
				}

				const auto& layers = d->contents[i].GetLayerNames();

				for (const auto& l : layers) {
					if (const auto* layer = d->contents[i].GetLayer(l); layer->GetVisibility()) {
						const auto& layerPixmap = layer->GetPixmap();
						painter.drawPixmap(point.x(), point.y(), d->itemSize, d->itemSize, layerPixmap);
					}
				}

				if (painted)
					continue;
			}

			d->paintedAll = false;
			painter.setBrush((d->animative) ? QBrush(d->gradient) : QColor(128, 128, 128, 128));
			painter.drawRect(point.x(), point.y(), d->itemSize, d->itemSize);
		}
	}

	auto GalleryPainter::resizeEvent(QResizeEvent* resizeEvent) -> void {
		QWidget::resizeEvent(resizeEvent);

		if (!d->resizing) {
			d->resizing = true;

			if (resizeEvent->oldSize().width() != resizeEvent->size().width()) {
				d->UpdateItemSize(width());
				this->setFixedHeight(d->GetPreferedHeight());
			}

			d->resizing = false;
		}
	}


	auto GalleryPainter::mousePressEvent(QMouseEvent* event) -> void {
		QWidget::mousePressEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y(), width());

		if (idx >= d->count)
			return;

		emit Pressed(idx, true, event);
		d->pressed = idx;
	}

	auto GalleryPainter::mouseReleaseEvent(QMouseEvent* event) -> void {
		QWidget::mouseReleaseEvent(event);

		if (d->pressed > -1) {
			const auto idx = d->GetIndexOfPosition(event->x(), event->y(), width());

			if (idx >= d->count)
				return;

			emit Pressed(d->pressed, false, event);

			if (d->pressed == idx)
				emit Clicked(idx, event);
		}

		d->pressed = -1;
	}

	auto GalleryPainter::mouseDoubleClickEvent(QMouseEvent* event) -> void {
		QWidget::mouseDoubleClickEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y(), width());

		if (idx >= d->count)
			return;

		emit DoubleClicked(idx, event);
	}

	auto GalleryPainter::leaveEvent(QEvent* event) -> void {
		QWidget::leaveEvent(event);

		if (d->hovered > -1) {
			emit Hovered(d->hovered, false);
			d->hovered = -1;
		}
	}

	auto GalleryPainter::mouseMoveEvent(QMouseEvent* event) -> void {
		QWidget::mouseMoveEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y(), width());

		if (idx >= d->count)
			return;

		if (idx != d->hovered) {
			if (d->hovered > -1) {
				emit Hovered(d->hovered, false);
				d->hovered = -1;
			}

			if (idx > -1) {
				emit Hovered(idx, true);
				d->hovered = idx;
			}
		}
	}

	auto GalleryPainter::minimumSizeHint() const -> QSize {
		const auto width = (d->colCount - 1) * d->spacing + d->colCount;
		return { width, 50 };
	}
}
