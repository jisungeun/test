#include "StrComboBox.h"

#include <iostream>

#include "ui_StrComboBox.h"

namespace TC {
    struct StrComboBox::Impl {
        Ui::StrComboBox* ui;
    };
    StrComboBox::StrComboBox(QWidget* parent,bool showBtn)
        :QWidget(parent), d{new Impl} {        
        d->ui = new Ui::StrComboBox;
        d->ui->setupUi(this);
        //connection
        connect(d->ui->strCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnIndexChanged(int)));
        connect(d->ui->applyBtn,SIGNAL(clicked()),this,SLOT(OnApplyButton()));

        //show/hide apply button
        if(showBtn) {
            d->ui->applyBtn->show();
        }else {
            d->ui->applyBtn->hide();
        }
    }

    StrComboBox::~StrComboBox() {
    }

    auto StrComboBox::ShowApplyButton(bool show) -> void {
        if(show) {
            d->ui->applyBtn->show();
        }else {
            d->ui->applyBtn->hide();
        }
    }

    auto StrComboBox::setCurrentIndex(int idx) -> void {        
        d->ui->strCombo->setCurrentIndex(idx);
    }
    auto StrComboBox::addItem(QString text) -> void {
        d->ui->strCombo->addItem(text);
    }
    auto StrComboBox::currentIndex() -> int {
        return d->ui->strCombo->currentIndex();
    }
    auto StrComboBox::SetToolTip(QString tooltip) -> void {
        d->ui->strCombo->setToolTip(tooltip);
    }
    auto StrComboBox::SetToolTipDuration(int msec) -> void {
        d->ui->strCombo->setToolTipDuration(msec);
    }

    void StrComboBox::OnIndexChanged(int idx) {
        auto text = d->ui->strCombo->toolTip();
        auto curText = d->ui->strCombo->currentText();
        emit comboStr(text,curText, idx);
    }
    void StrComboBox::OnApplyButton() {        
        auto text = d->ui->strCombo->toolTip();
        emit comboApply(text,d->ui->strCombo->currentIndex());
    }

}