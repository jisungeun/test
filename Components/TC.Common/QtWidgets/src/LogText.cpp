#include <QScrollBar>
#include <QPainter>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QTextLayout>

#include "LogText.h"
#include <QsLog.h>

#define NoLimit (-1)
#define PART_OF_CONTENT_TO_KEEP 0.75

namespace TC::Widgets {
    struct LogText::Impl {
        int32_t limit{ NoLimit };
        bool followTail{ false };
        QColor color{ Qt::black };
    };

    LogText::LogText(QWidget* parent) : QPlainTextEdit(parent), d{ std::make_unique<Impl>() } {
        setFont(QFont("Courier", 10, QFont::Normal));

        setReadOnly(true);
        setTextInteractionFlags(textInteractionFlags() | Qt::TextSelectableByKeyboard);
        setTextInteractionFlags(textInteractionFlags() | Qt::TextSelectableByMouse);

        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        verticalScrollBar()->setPageStep(static_cast<int>(height() * 0.8));
    }

    LogText::~LogText() {
    }

    auto LogText::ClearBuffer() -> void {
        clear();
    }

    auto LogText::SetColor(QColor color) -> void {
        d->color = color;
    }

    auto LogText::SetFollowTail(bool follow) -> void {
        d->followTail = follow;
        if (follow) {
            verticalScrollBar()->setValue(verticalScrollBar()->maximum());
            verticalScrollBar()->setDisabled(true);
        } else {
            verticalScrollBar()->setDisabled(false);
        }
    }

    auto LogText::AppendLine(const QString& str) -> void {
        if (str.isEmpty()) return;
        appendPlainText(str);
    }

    auto LogText::Text() const -> QString {
        return toPlainText();
    }

    void LogText::scrollContentsBy(int dx, int dy) {
        if (!d->followTail) return;
        QPlainTextEdit::scrollContentsBy(dx, dy);
    }

    void LogText::wheelEvent(QWheelEvent* event) {
        const auto followTail = d->followTail;
        d->followTail = true;
        QPlainTextEdit::wheelEvent(event);
        d->followTail = followTail;
    }

    void LogText::mouseMoveEvent(QMouseEvent* event) {
        Q_UNUSED(event)
    }
}
