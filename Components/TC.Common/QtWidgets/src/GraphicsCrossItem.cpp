#include <QPen>
#include <QPainter>

#include "GraphicsCrossItem.h"

namespace TC::Widgets {
    struct GraphicsCrossItem::Impl {
        int32_t lineLength{ 10 };

        struct {
            QColor color{ Qt::white };
            int32_t thickness{ 0 };
        } prop;
    };

    GraphicsCrossItem::GraphicsCrossItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{new Impl} {
    }

    GraphicsCrossItem::~GraphicsCrossItem() {
    }

    auto GraphicsCrossItem::SetCenter(int32_t x, int32_t y) -> void {
        setPos(x, y);
    }

    auto GraphicsCrossItem::SetLength(int32_t thickness) -> void {
        d->lineLength = thickness;
    }

    auto GraphicsCrossItem::SetColor(const QColor& color) -> void {
        d->prop.color = color;
    }

    auto GraphicsCrossItem::SetThickness(int32_t thickness) -> void {
        d->prop.thickness = thickness;
    }

    auto GraphicsCrossItem::boundingRect() const -> QRectF {
        const auto half = d->lineLength / 2.0;
        return QRectF(0 - half, 0 - half, d->lineLength, d->lineLength);
    }

    auto GraphicsCrossItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        const auto pen{ QPen(d->prop.color, d->prop.thickness) };
		painter->setPen(pen);

        const auto center{ QPointF(0, 0) };

        const auto half = d->lineLength / 2.0;
        const auto y0 = static_cast<int32_t>(center.y() - half);
        const auto y1 = static_cast<int32_t>(center.y() + half);
        const auto x0 = static_cast<int32_t>(center.x() - half);
        const auto x1 = static_cast<int32_t>(center.x() + half);

        const auto cx = static_cast<int32_t>(center.x());
        const auto cy = static_cast<int32_t>(center.y());

		painter->drawLine(cx, y0, cx, y1);
		painter->drawLine(x0, cy, x1, cy);
    }
}
