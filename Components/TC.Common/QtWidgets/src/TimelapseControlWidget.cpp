#include "ui_TimelapseControlWidget.h"
#include "TimelapseControlWidget.h"

#include <qgraphicseffect.h>

namespace  TC {
    struct TimelapseControlWidget::Impl {
        Ui::TimelapseControlWidget* ui{ nullptr };
    };

    TimelapseControlWidget::TimelapseControlWidget(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        d->ui = new Ui::TimelapseControlWidget();
        d->ui->setupUi(this);

        QGraphicsOpacityEffect* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.5);
        this->setGraphicsEffect(oe);
        this->setAutoFillBackground(true);
    }

    TimelapseControlWidget::~TimelapseControlWidget() {
        delete d->ui;
    }

    void TimelapseControlWidget::Init(const int& step) const {
        d->ui->progressSlider->setMinimum(0);
        d->ui->progressSlider->setMaximum(step - 1);
        d->ui->progressSlider->setValue(0);

        d->ui->prevButton->setStyleSheet("color: #5f6f7a; background: #1b2629;");
        d->ui->prevButton->setShortcut(QKeySequence(Qt::Key_Left));
        d->ui->nextButton->setStyleSheet("color: #5f6f7a; background: #1b2629;");
        d->ui->nextButton->setShortcut(QKeySequence(Qt::Key_Right));

        d->ui->stepEdit->setText(this->GetStepText(1));
    }

    void TimelapseControlWidget::on_progressSlider_valueChanged(int value) {
        d->ui->stepEdit->setText(this->GetStepText(value + 1));

        emit timelapseChanged(value);
    }

    void TimelapseControlWidget::on_prevButton_clicked() const {
        if(0 >= d->ui->progressSlider->value()) {
            return;
        }

        d->ui->progressSlider->setValue(d->ui->progressSlider->value() - 1);
    }

    void TimelapseControlWidget::on_nextButton_clicked() const {
        if (d->ui->progressSlider->maximum() <= d->ui->progressSlider->value()) {
            return;
        }

        d->ui->progressSlider->setValue(d->ui->progressSlider->value() +1);
    }

    auto TimelapseControlWidget::GetStepText(const int& currentIndex) const ->QString {
        auto total = d->ui->progressSlider->maximum() + 1;

        return QString("%1/%2").arg(currentIndex).arg(total);
    }
}
