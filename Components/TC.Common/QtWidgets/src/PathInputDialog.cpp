#include <QString>
#include <QRegExpValidator>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include "PathLineEdit.h"
#include "PathInputDialog.h"

namespace TC::PathWidget {
	struct PathInputDialog::Impl {
		Impl() {
			layout.addWidget(&label);
			layout.addWidget(&lineEdit);
			layout.addLayout(&btnLayout);
			btnLayout.setAlignment(Qt::AlignRight);
			btnLayout.addWidget(&ok);
			btnLayout.addWidget(&cancel);
		}

		QRegExpValidator validator;
		PathLineEdit lineEdit;
		QLabel label = QLabel("Enter a value: ");
		QVBoxLayout layout;
		QHBoxLayout btnLayout;
		QPushButton ok = QPushButton("Ok");
		QPushButton cancel = QPushButton("Cancel");
	};

	PathInputDialog::PathInputDialog(QWidget* parent, Qt::WindowFlags flags) : QDialog(parent, flags), d(new Impl) {
		this->setAttribute(Qt::WA_QuitOnClose, false);
		this->setLayout(&d->layout);

		connect(&d->ok, &QPushButton::clicked, this, &QDialog::accept);
		connect(&d->cancel, &QPushButton::clicked, this, &QDialog::reject);
		connect(&d->lineEdit, &PathLineEdit::textChanged, this, [this] (const QString& text) { emit textValueChanged(text); });
		connect(&d->lineEdit, &PathLineEdit::textChanged, this, [this] (const QString&)  {
			const auto* validator = d->lineEdit.validator();
			auto pos = 0;
			auto text = d->lineEdit.text();
            const auto result = validator->validate(text, pos);
			d->ok.setEnabled(result == QValidator::Acceptable);
		});
	}

	PathInputDialog::~PathInputDialog() = default;

	auto PathInputDialog::SetLabelText(const QString& text) -> void {
		d->label.setText(text);
	}

	auto PathInputDialog::SetText(const QString& text) -> void {
		d->lineEdit.setText(text);
	}

	auto PathInputDialog::SetOkButtonText(const QString& text) -> void {
		d->ok.setText(text);
	}

	auto PathInputDialog::SetCancelButtonText(const QString& text) -> void {
		d->cancel.setText(text);
	}

    auto PathInputDialog::SetTextMaxLength(int length) -> void {
		d->lineEdit.SetMaxLength(length);
    }

    auto PathInputDialog::GetLabelText() const -> QString {
		return d->label.text();
	}

	auto PathInputDialog::GetText() const -> QString {
		return d->lineEdit.text();
	}

	auto PathInputDialog::GetOkButtonText() const -> QString {
		return d->ok.text();
	}

	auto PathInputDialog::GetCancelButtonText() const -> QString {
		return d->cancel.text();
	}

    auto PathInputDialog::GetTextMaxLength() const -> int {
		return d->lineEdit.GetMaxLength();
    }
}
