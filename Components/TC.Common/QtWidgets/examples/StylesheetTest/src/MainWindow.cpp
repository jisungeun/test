#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QFile>
#include <QTextStream>

namespace TC::StyleTest {
    struct MainWindow::Impl {
        explicit Impl(Self* self): self(self) {
            ui.setupUi(self);
        }

        Ui::MainWindow ui{};
        Self* self{};

        auto ApplyStylesheet() -> void;
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->ApplyStylesheet();

        d->ui.baseWidget->setObjectName("panel");
    }

    MainWindow::~MainWindow() {
    }

    auto MainWindow::Impl::ApplyStylesheet() -> void {
        QString stylesheet;

        QFile qssCommon(":/style/tcdarkcommon.qss");
        if (!qssCommon.exists()) {
            printf("Unable to set stylesheet, file not found\n");
        } else {
            qssCommon.open(QFile::ReadOnly | QFile::Text);
            stylesheet.append(QTextStream(&qssCommon).readAll());
        }

        QFile qssTSX(":/style/tctsx.qss");
        if (!qssTSX.exists()) {
            printf("Unable to set stylesheet, file not found\n");
        } else {
            qssTSX.open(QFile::ReadOnly | QFile::Text);
            stylesheet.append(QTextStream(&qssTSX).readAll());
        }

        if (!stylesheet.isEmpty()) {
            qApp->setStyleSheet(stylesheet);
        }
    }
}
