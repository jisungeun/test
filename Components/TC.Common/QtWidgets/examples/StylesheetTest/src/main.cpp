#include "MainWindow.h"

#include <QApplication>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Bold.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Regular.otf");

    QFont font;
    font.setFamily("Noto Sans KR");
    font.setPixelSize(12);
    font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);

    QApplication::setFont(font);

    TC::StyleTest::MainWindow w;
    w.show();
    return a.exec();
}
