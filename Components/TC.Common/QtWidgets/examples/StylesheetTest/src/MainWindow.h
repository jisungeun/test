#pragma once

#include <QMainWindow>

namespace TC::StyleTest {
    QT_END_NAMESPACE class MainWindow : public QMainWindow {
        Q_OBJECT
    public:
        using Self = MainWindow;
        MainWindow(QWidget* parent = nullptr);
        ~MainWindow() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
