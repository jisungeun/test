project(StylesheetExample)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
	src/MainWindow.h
)

#Sources
set(SOURCES
	src/main.cpp
	src/MainWindow.cpp
)

set(UI
	src/MainWindow.ui
)

set(RESOURCE
	${CMAKE_SOURCE_DIR}/Resources/style/tcdarkcommon/tcdarkcommon.qrc
    ${CMAKE_SOURCE_DIR}/Resources/style/tctsx/tctsx.qrc
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${RESOURCE}
	${UI}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Qt5::Core
		Qt5::Widgets
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
    PRIVATE
        /W4 /WX
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases") 	
