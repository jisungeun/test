#include <QCoreApplication>
#include <QGroupBox>
#include <QLineEdit>
#include <QBoxLayout>

#include <RangeSlider.h>

#include "mainwindow.h"

struct MainWindow::Impl {
    QLineEdit* leftMin{ nullptr };
    QLineEdit* leftMax{ nullptr };
    QLineEdit* rightMin{ nullptr };
    QLineEdit* rightMax{ nullptr };
};

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags) : QMainWindow(parent, flags), d{ new Impl } {
    setWindowTitle("RangeSlider Example");
    setMinimumSize(QSize(400, 400));

    auto box = new QGroupBox();
    {
        auto layout = new QGridLayout();
        {
            auto slider = new TC::RangeSlider(Qt::Horizontal);
            slider->SetRange(0, 100);
            layout->addWidget(slider, 0, 0, 1, 2);

            connect(slider, SIGNAL(lowerValueChanged(int)), this, SLOT(onLowerChangedLeft(int)));
            connect(slider, SIGNAL(upperValueChanged(int)), this, SLOT(onUpperChangedLeft(int)));

            d->leftMin = new QLineEdit();
            d->leftMin->setReadOnly(true);
            layout->addWidget(d->leftMin, 1, 0);

            d->leftMax = new QLineEdit();
            d->leftMax->setReadOnly(true);
            layout->addWidget(d->leftMax, 1, 1);

            slider = new TC::RangeSlider(Qt::Vertical);
            slider->SetRange(0, 100);
            layout->addWidget(slider, 0, 2, 1, 2);

            connect(slider, SIGNAL(lowerValueChanged(int)), this, SLOT(onLowerChangedRight(int)));
            connect(slider, SIGNAL(upperValueChanged(int)), this, SLOT(onUpperChangedRight(int)));

            d->rightMin = new QLineEdit();
            d->rightMin->setReadOnly(true);
            layout->addWidget(d->rightMin, 1, 2);

            d->rightMax = new QLineEdit();
            d->rightMax->setReadOnly(true);
            layout->addWidget(d->rightMax, 1, 3);
        }
        box->setLayout(layout);
    }
    setCentralWidget(box);
}

MainWindow::~MainWindow() {
}

void MainWindow::onLowerChangedLeft(int value) {
    d->leftMin->setText(QString::number(value));
}

void MainWindow::onUpperChangedLeft(int value) {
    d->leftMax->setText(QString::number(value));
}

void MainWindow::onLowerChangedRight(int value) {
    d->rightMin->setText(QString::number(value));

}

void MainWindow::onUpperChangedRight(int value) {
    d->rightMax->setText(QString::number(value));
}
