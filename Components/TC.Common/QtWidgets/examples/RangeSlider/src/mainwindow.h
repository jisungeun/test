#pragma once

#include <memory>
#include <QMainWindow>

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
    virtual ~MainWindow();

protected slots:
    void onLowerChangedLeft(int value);
    void onUpperChangedLeft(int value);
    void onLowerChangedRight(int value);
    void onUpperChangedRight(int value);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};