#include <QCoreApplication>
#include <QBoxLayout>
#include <QMenuBar>
#include <QFileDialog>
#include <QStatusBar>
#include <QImage>

#include <SimpleImageView.h>

#include "mainwindow.h"

struct MainWindow::Impl {
    TC::SimpleImageView* imageview{ nullptr };
};

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags) : QMainWindow(parent, flags), d{ new Impl } {
    setWindowTitle("SimpleImageView Example");
    setMinimumSize(QSize(400, 400));

    d->imageview = new TC::SimpleImageView();
    setCentralWidget(d->imageview);

    auto fileMenu = menuBar()->addMenu(tr("&File"));
    {
        auto openAct = new QAction("&Open");
        fileMenu->addAction(openAct);
        fileMenu->addSeparator();
        auto quitAct = new QAction("&Quit");
        fileMenu->addAction(quitAct);

        connect(openAct, SIGNAL(triggered()), this, SLOT(onOpen()));
        connect(quitAct, SIGNAL(triggered()), qApp, SLOT(quit()));
    }

    connect(d->imageview, SIGNAL(mouseClicked(QPoint)), this, SLOT(onClicked(QPoint)));
    connect(d->imageview, SIGNAL(mouseDoubleClicked(QPoint)), this, SLOT(onClicked(QPoint)));
}

MainWindow::~MainWindow() {
}

void MainWindow::onOpen() {
    auto filename = QFileDialog::getOpenFileName(this, "Select Image");
    if (filename.isEmpty()) return;

    QImage image(filename);
    if (image.isNull()) return;

    d->imageview->ShowImage(image, true);
}

void MainWindow::onClicked(QPoint point) {
    statusBar()->showMessage(QString("%1,%2").arg(point.x()).arg(point.y()), 2000);
}
