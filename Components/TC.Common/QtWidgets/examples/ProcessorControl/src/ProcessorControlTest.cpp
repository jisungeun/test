#include <iostream>
#include <qapplication.h>
#include <QDirIterator>
#include <QTextStream>

#include "PluginRegistry.h"

#include "ProcessorControl.h"
#include "MeasureBasicParameter.h"
#include "LabelingBasicParameter.h"
#include "SegmentationThresholdParameter.h"

int main(int argc, char** argv) {
	QApplication app(argc, argv);
    std::cout << "Hello PC Control" << std::endl;
	/*QFile f(":qdarkstyle/style.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	}
	else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}*/
	
    
	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("algorithms");
		dir.cd("ai");
		appPath = dir.absolutePath();		
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}
	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("algorithms");
		dir.cd("labeling");
		appPath = dir.absolutePath();
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}
	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("algorithms");
		dir.cd("masking");
		appPath = dir.absolutePath();
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}
	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("algorithms");
		dir.cd("measurement");
		appPath = dir.absolutePath();
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}
	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("algorithms");
		dir.cd("segmentation");
		appPath = dir.absolutePath();
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}

	{
		QStringList pathList;
		QStringList fileList;
		auto appPath = QString("D:/tss_2019/bin/Release");
		QDir dir(appPath);
		dir.cd("processor");
		dir.cd("basicanalysis");
		appPath = dir.absolutePath();
		fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
		for (int i = 0; i < fileList.size(); i++) {
			auto fullPath = appPath + "/" + fileList[i];
			std::cout << fileList[i].toStdString() << std::endl;
			if (PluginRegistry::LoadPlugin(fullPath) == 0) {//경로로 불러온다
				std::cout << "==== Failed to load plugins ====" << std::endl;
			}
		}
	}

	std::cout << "==========Loaded Plugins=========" << std::endl;
	auto processors = PluginRegistry::GetInstance()->GetPluginList("org.tomocube.processor.basicanalysis");
	for (auto i = 0; i < processors.size(); i++) {
		std::cout << processors[i].toStdString() << std::endl;
	}

	auto moduleInstance = PluginRegistry::GetPlugin(processors[2]);
	auto param = moduleInstance->Parameter();	

	auto procControl = new TC::ProcessorControl(nullptr);
	procControl->setHideExecute(false);
	procControl->setHidePramExecute(false);
	procControl->setProcessingParameter(param);
	
	procControl->show();	

	app.exec();
		
    return EXIT_SUCCESS;
}