#pragma once

#include <cstdint>

#include "TCImagingProfileExport.h"

namespace TC::ImagingProfile {
    struct TCImagingProfile_API ImagingProfile_v0_0_1 {
        int32_t step{};
        int32_t slice{};
    };
}