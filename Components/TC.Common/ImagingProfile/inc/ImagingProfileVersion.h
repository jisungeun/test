#pragma once

#include <enum.h>

namespace TC::ImagingProfile {
    BETTER_ENUM(ImagingProfileVersion, uint8_t, v0_0_1);
}