#pragma once

#include "TCUIUtilityExport.h"

#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QRadioButton>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSlider>
#include <QTableWidget>
#include <QAction>
#include <QLineEdit>

namespace TC {
	class TCUIUtility_API LabelSignalBlocker{
	public:
		LabelSignalBlocker(QLabel* obj);
		~LabelSignalBlocker();
		QLabel* operator->();

	private:
		QLabel* blocked;
		bool previous;
	};

	class TCUIUtility_API LineEditSignalBlocker {
	public:
		LineEditSignalBlocker(QLineEdit* obj);
		~LineEditSignalBlocker();
		QLineEdit* operator->();

	private:
		QLineEdit* blocked;
		bool previous;
	};

	class TCUIUtility_API ButtonSignalBlocker {
	public:
		ButtonSignalBlocker(QPushButton* obj);
		~ButtonSignalBlocker();
		QPushButton* operator->();

	private:
		QPushButton* blocked;
		bool previous;
	};

	class TCUIUtility_API CheckBoxSignalBlocker {
	public:
		CheckBoxSignalBlocker(QCheckBox* obj);
		~CheckBoxSignalBlocker();
		QCheckBox* operator->();

	private:
		QCheckBox* blocked;
		bool previous;
	};

	class TCUIUtility_API RadioButtonSignalBlocker {
	public:
		RadioButtonSignalBlocker(QRadioButton* obj);
		~RadioButtonSignalBlocker();
		QRadioButton* operator->();

	private:
		QRadioButton* blocked;
		bool previous;
	};

	class TCUIUtility_API ComboBoxSignalBlocker {
	public:
		ComboBoxSignalBlocker(QComboBox* obj);
		~ComboBoxSignalBlocker();
		QComboBox* operator->();

	private:
		QComboBox* blocked;
		bool previous;
	};

	class TCUIUtility_API SpinBoxSignalBlocker {
	public:
		SpinBoxSignalBlocker(QSpinBox* obj);
		~SpinBoxSignalBlocker();
		QSpinBox* operator->();

	private:
		QSpinBox* blocked;
		bool previous;
	};

	class TCUIUtility_API DoubleSpinBoxSignalBlocker {
	public:
		DoubleSpinBoxSignalBlocker(QDoubleSpinBox* obj);
		~DoubleSpinBoxSignalBlocker();
		QDoubleSpinBox* operator->();

	private:
		QDoubleSpinBox* blocked;
		bool previous;
	};

	class TCUIUtility_API SliderSignalBlocker {
	public:
		SliderSignalBlocker(QSlider* obj);
		~SliderSignalBlocker();
		QSlider* operator->();

	private:
		QSlider* blocked;
		bool previous;
	};

	class TCUIUtility_API TableWidgetSignalBlocker {
	public:
		TableWidgetSignalBlocker(QTableWidget* obj);
		~TableWidgetSignalBlocker();
		QTableWidget* operator->();

	private:
		QTableWidget* blocked;
		bool previous;
	};

	class TCUIUtility_API ActionBlocker {
	public:
		ActionBlocker(QAction* obj);
		~ActionBlocker();
		QAction* operator->();

	private:
		QAction* blocked;
		bool previous;
	};


	TCUIUtility_API auto SilentCall(QLabel* obj)->LabelSignalBlocker;
	TCUIUtility_API auto SilentCall(QLineEdit* obj)->LineEditSignalBlocker;
	TCUIUtility_API auto SilentCall(QPushButton* obj)->ButtonSignalBlocker;
	TCUIUtility_API auto SilentCall(QCheckBox* obj)->CheckBoxSignalBlocker;
	TCUIUtility_API auto SilentCall(QRadioButton* obj)->RadioButtonSignalBlocker;
	TCUIUtility_API auto SilentCall(QComboBox* obj)->ComboBoxSignalBlocker;
	TCUIUtility_API auto SilentCall(QSpinBox* obj)->SpinBoxSignalBlocker;
	TCUIUtility_API auto SilentCall(QDoubleSpinBox* obj)->DoubleSpinBoxSignalBlocker;
	TCUIUtility_API auto SilentCall(QSlider* obj)->SliderSignalBlocker;
	TCUIUtility_API auto SilentCall(QTableWidget* obj)->TableWidgetSignalBlocker;
	TCUIUtility_API auto SilentCall(QAction* obj)->ActionBlocker;
}

