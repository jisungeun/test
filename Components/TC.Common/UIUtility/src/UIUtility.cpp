#include "UIUtility.h"

namespace TC {
    // QLabel Signal blocker
    LabelSignalBlocker::LabelSignalBlocker(QLabel* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    LabelSignalBlocker::~LabelSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QLabel* LabelSignalBlocker::operator->() {
        return blocked;
    }

    // QLineEdit Signal blocker
    LineEditSignalBlocker::LineEditSignalBlocker(QLineEdit* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    LineEditSignalBlocker::~LineEditSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QLineEdit* LineEditSignalBlocker::operator->() {
        return blocked;
    }

    // QPushButton Signal blocker
    ButtonSignalBlocker::ButtonSignalBlocker(QPushButton* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    ButtonSignalBlocker::~ButtonSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QPushButton* ButtonSignalBlocker::operator->() {
        return blocked;
    }

    // QCheckBox Signal blocker
    CheckBoxSignalBlocker::CheckBoxSignalBlocker(QCheckBox* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    CheckBoxSignalBlocker::~CheckBoxSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QCheckBox* CheckBoxSignalBlocker::operator->() {
        return blocked;
    }

    // QRadioButton Signal blocker
    RadioButtonSignalBlocker::RadioButtonSignalBlocker(QRadioButton* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    RadioButtonSignalBlocker::~RadioButtonSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QRadioButton* RadioButtonSignalBlocker::operator->() {
        return blocked;
    }

    // QComboBox Signal blocker
    ComboBoxSignalBlocker::ComboBoxSignalBlocker(QComboBox* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    ComboBoxSignalBlocker::~ComboBoxSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QComboBox* ComboBoxSignalBlocker::operator->() {
        return blocked;
    }

    // QSpinBox Signal blocker
    SpinBoxSignalBlocker::SpinBoxSignalBlocker(QSpinBox* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    SpinBoxSignalBlocker::~SpinBoxSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QSpinBox* SpinBoxSignalBlocker::operator->() {
        return blocked;
    }

    // QDoubleSpinBox Signal blocker
    DoubleSpinBoxSignalBlocker::DoubleSpinBoxSignalBlocker(QDoubleSpinBox* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    DoubleSpinBoxSignalBlocker::~DoubleSpinBoxSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QDoubleSpinBox* DoubleSpinBoxSignalBlocker::operator->() {
        return blocked;
    }

    // QSlider Signal blocker
    SliderSignalBlocker::SliderSignalBlocker(QSlider* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    SliderSignalBlocker::~SliderSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QSlider* SliderSignalBlocker::operator->() {
        return blocked;
    }

    // QTableWidget Signal blocker
    TableWidgetSignalBlocker::TableWidgetSignalBlocker(QTableWidget* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    TableWidgetSignalBlocker::~TableWidgetSignalBlocker() {
        blocked->blockSignals(previous);
    }

    QTableWidget* TableWidgetSignalBlocker::operator->() {
        return blocked;
    }

    // QAction Signal blocker
    ActionBlocker::ActionBlocker(QAction* obj) {
        blocked = obj;
        previous = blocked->blockSignals(true);
    }

    ActionBlocker::~ActionBlocker() {
        blocked->blockSignals(previous);
    }

    QAction* ActionBlocker::operator->() {
        return blocked;
    }

    auto SilentCall(QLabel* obj)->LabelSignalBlocker {
        return LabelSignalBlocker(obj);
    }

    auto SilentCall(QLineEdit* obj)->LineEditSignalBlocker {
        return LineEditSignalBlocker(obj);
    }

    auto SilentCall(QPushButton* obj)->ButtonSignalBlocker {
        return ButtonSignalBlocker(obj);
    }

    auto SilentCall(QCheckBox* obj)->CheckBoxSignalBlocker {
        return CheckBoxSignalBlocker(obj);
    }

    auto SilentCall(QRadioButton* obj)->RadioButtonSignalBlocker {
        return RadioButtonSignalBlocker(obj);
    }

    auto SilentCall(QComboBox* obj)->ComboBoxSignalBlocker {
        return ComboBoxSignalBlocker(obj);
    }

    auto SilentCall(QSpinBox* obj)->SpinBoxSignalBlocker {
        return SpinBoxSignalBlocker(obj);
    }

    auto SilentCall(QDoubleSpinBox* obj)->DoubleSpinBoxSignalBlocker {
        return DoubleSpinBoxSignalBlocker(obj);
    }

    auto SilentCall(QSlider* obj)->SliderSignalBlocker {
        return SliderSignalBlocker(obj);
    }

    auto SilentCall(QTableWidget* obj)->TableWidgetSignalBlocker {
        return TableWidgetSignalBlocker(obj);
    }

    auto SilentCall(QAction* obj)->ActionBlocker {
        return ActionBlocker(obj);
    }
}