#include <catch2/catch.hpp>

#include "RegistryVerifier.h"
#include "RegistrySignature.h"

namespace RegistryJwtTest {
	TEST_CASE("RegistryJwt Test") {
        SECTION("non-exist key") {
            TC::RegistryVerifier ver("no", "no", "no", "no");

            REQUIRE(ver.Verify() == false);
        }
		SECTION("With same key") {
			TC::RegistrySignature sig("jwttest", "test", "abc", "key");
			TC::RegistryVerifier ver("jwttest", "test", "abc", "key");

			sig.Sign();
			REQUIRE(ver.Verify());
		}

		SECTION("With wrong key") {
			TC::RegistrySignature sig("jwttest", "test", "abc", "correct_key");
			TC::RegistryVerifier ver("jwttest", "test", "abc", "wrong_key");

			sig.Sign();
			REQUIRE(!ver.Verify());
		}

		SECTION("With already expired iat") {
			TC::RegistrySignature sig("jwttest", "test", "abc", "key", -50);
			TC::RegistryVerifier ver("jwttest", "test", "abc", "key");

			sig.Sign();
			REQUIRE(!ver.Verify());
		}
	}
}
