#include <QDateTime>

#include "RegistryVerifier.h"

#include "jwt-cpp/jwt.h"

namespace TC {
	struct RegistryVerifier::Impl {
		QString organization;
		QString application;
		QString path;
	    QString key;
	};

	RegistryVerifier::RegistryVerifier(const QString& organization,
									   const QString& application,
									   const QString& path,
									   const QString& key) : d(new Impl) {
		d->organization = organization;
		d->application = application;
		d->path = path;
		d->key = key;
	}

	RegistryVerifier::~RegistryVerifier() = default;

	auto RegistryVerifier::Verify() -> bool {
		QSettings qs(d->organization, d->application);

        const auto claims = qs.value(QString("%1/claims").arg(d->path), "").toString().toStdString();
        if (claims.empty()) {
            return false;
        }

		const auto key = d->key.toStdString();
		const jwt::algorithm::hs512 hash(key);

		const auto verifier = jwt::verify()
			.allow_algorithm(hash);
		const auto decodes = jwt::decode(claims);

		try {
			verifier.verify(decodes);
			return true;
		} catch (...) {
			return false;
		}
	}

	auto RegistryVerifier::GetExpireAt() const -> QDateTime {
		QSettings qs(d->organization, d->application);

		const auto claims = qs.value(QString("%1/claims").arg(d->path)).toString().toStdString();
		const auto key = d->key.toStdString();
		const jwt::algorithm::hs512 hash(key);

		const auto verifier = jwt::verify()
			.allow_algorithm(hash);
		const auto decodes = jwt::decode(claims);
		
		const auto iat = decodes.get_expires_at().time_since_epoch();
		const auto epoch = iat.count() / 10000;
		return QDateTime::fromMSecsSinceEpoch(epoch);

	}
}
