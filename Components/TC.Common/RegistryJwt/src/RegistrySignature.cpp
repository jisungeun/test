#include "RegistrySignature.h"

#include "jwt-cpp/jwt.h"

namespace TC {
	struct RegistrySignature::Impl {
		QString organization;
		QString application;
		QString path;
	    QString key;
		int expire = -1;

		auto GetSignature(int expireSecs) const -> QString;
	};

    auto RegistrySignature::Impl::GetSignature(int expireSecs) const -> QString {
		const auto str = key.toStdString();
		const jwt::algorithm::hs512 hash(str);
		const auto time = jwt::date::clock::now();

		const auto claims = jwt::create()
			.set_type("JWT")
			.set_expires_at(time + std::chrono::seconds{ expireSecs })
			.sign(hash);

		return QString::fromStdString(claims);
    }

    RegistrySignature::RegistrySignature(const QString& organization,
										 const QString& application,
										 const QString& path,
						                 const QString& key,
										 int expireSeconds) : d{new Impl} {
		d->organization = organization;
		d->application = application;
		d->path = path;
		d->key = key;
		d->expire = expireSeconds;
    }

    RegistrySignature::~RegistrySignature() = default;

	auto RegistrySignature::GetKey() const -> const QString& {
		return d->key;
	}

	auto RegistrySignature::SetExpireSeconds(int seconds) -> void {
		d->expire = seconds;
	}

	auto RegistrySignature::GetExpireSeconds() const -> int {
		return d->expire;
	}

	auto RegistrySignature::Sign() -> void {
		const auto signature = d->GetSignature(d->expire);
		QSettings qs(d->organization, d->application);
		qs.setValue(QString("%1/claims").arg(d->path), signature);
	}

    auto RegistrySignature::Unsign() -> void {
		QSettings qs(d->organization, d->application);
		qs.remove(d->path);
    }
}
