#pragma once

#include <memory>

#include <QString>
#include <QSettings>

#include "TCRegistryJwtExport.h"

namespace TC {
	class TCRegistryJwt_API RegistrySignature {
	public:
		RegistrySignature(const QString& organization,
						  const QString& application,
						  const QString& path,
						  const QString& key,
						  int expireSeconds = 600);
	    ~RegistrySignature();

		auto GetKey() const->const QString&;

		auto SetExpireSeconds(int seconds) -> void;
		auto GetExpireSeconds() const -> int;
		
		auto Sign()->void;
		auto Unsign()->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}