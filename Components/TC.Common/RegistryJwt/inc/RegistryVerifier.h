#pragma once

#include <memory>

#include <QString>
#include <QSettings>

#include "TCRegistryJwtExport.h"

namespace TC {
	class TCRegistryJwt_API RegistryVerifier {
	public:
		RegistryVerifier(const QString& organization,
						 const QString& application,
						 const QString& path,
						 const QString& key);
		~RegistryVerifier();

		auto Verify()->bool;
		auto GetExpireAt() const->QDateTime;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}