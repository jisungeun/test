#include <catch2/catch.hpp>

#include "Vessel.h"

namespace VesselTest {
    using namespace TC;
    TEST_CASE("Vessel : unit test") {
        SECTION("Vessel()") {
            Vessel vessel;
            CHECK(&vessel != nullptr);
        }
        SECTION("Vessel(other)") {
            Vessel srcVessel;
            srcVessel.SetRowIndex(1);

            Vessel destVessel(srcVessel);
            CHECK(destVessel.GetRowIndex() == 1);
        }
        SECTION("operator=()") {
            Vessel srcVessel;
            srcVessel.SetRowIndex(1);

            Vessel destVessel;
            destVessel = srcVessel;
            CHECK(destVessel.GetRowIndex() == 1);
        }
        SECTION("SetIndex()") {
            Vessel vessel;
            vessel.SetIndex(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetIndex()") {
            Vessel vessel;

            SECTION("default") {
                CHECK(vessel.GetIndex() == kInvalid);
            }

            SECTION("after setting value") {
                vessel.SetIndex(1);
                CHECK(vessel.GetIndex() == 1);
            }
        }
        SECTION("SetShape()") {
            Vessel vessel;
            vessel.SetShape(VesselShape::Circle);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetShape()") {
            Vessel vessel;
            vessel.SetShape(VesselShape::Circle);
            CHECK(vessel.GetShape() == +VesselShape::Circle);
        }
        SECTION("SetX()") {
            Vessel vessel;
            vessel.SetX(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetX()") {
            Vessel vessel;
            vessel.SetX(1);
            CHECK(vessel.GetX() == 1);
        }
        SECTION("SetY()") {
            Vessel vessel;
            vessel.SetY(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetY()") {
            Vessel vessel;
            vessel.SetY(1);
            CHECK(vessel.GetY() == 1);
        }
        SECTION("SetWidth()") {
            Vessel vessel;
            vessel.SetWidth(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetWidth()") {
            Vessel vessel;
            vessel.SetWidth(1);
            CHECK(vessel.GetWidth() == 1);
        }
        SECTION("SetHeight()") {
            Vessel vessel;
            vessel.SetHeight(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetHeight()") {
            Vessel vessel;
            vessel.SetHeight(1);
            CHECK(vessel.GetHeight() == 1);
        }
        SECTION("SetRowIndex()") {
            Vessel vessel;
            vessel.SetRowIndex(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetRowIndex()") {
            Vessel vessel;
            vessel.SetRowIndex(1);
            CHECK(vessel.GetRowIndex() == 1);
        }
        SECTION("SetColumnIndex()") {
            Vessel vessel;
            vessel.SetColumnIndex(1);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetColumnIndex()") {
            Vessel vessel;
            vessel.SetColumnIndex(1);
            CHECK(vessel.GetColumnIndex() == 1);
        }
        SECTION("SetWellCount()") {
            Vessel vessel;
            vessel.SetWellCount(1, 2);
            CHECK(&vessel != nullptr);
        }
        SECTION("GetWellRows()") {
            Vessel vessel;
            vessel.SetWellCount(1, 2);
            CHECK(vessel.GetWellRows() == 1);
        }
        SECTION("GetWellCols()") {
            Vessel vessel;
            vessel.SetWellCount(1, 2);
            CHECK(vessel.GetWellCols() == 2);
        }
    }

    TEST_CASE("Vessel : practical test") {
        Vessel vessel;
        vessel.SetIndex(1);
        vessel.SetShape(VesselShape::Rectangle);
        vessel.SetX(2);
        vessel.SetY(3);
        vessel.SetWidth(4);
        vessel.SetHeight(5);
        vessel.SetRowIndex(6);
        vessel.SetColumnIndex(7);
        vessel.SetWellCount(8, 9);

        CHECK(vessel.GetIndex() == 1);
        CHECK(vessel.GetShape() == +VesselShape::Rectangle);
        CHECK(vessel.GetX() == 2);
        CHECK(vessel.GetY() == 3);
        CHECK(vessel.GetWidth() == 4);
        CHECK(vessel.GetHeight() == 5);
        CHECK(vessel.GetRowIndex() == 6);
        CHECK(vessel.GetColumnIndex() == 7);
        CHECK(vessel.GetWellRows() == 8);
        CHECK(vessel.GetWellCols() == 9);
    }
}