﻿#include <catch2/catch.hpp>

#include <iostream>

#include "ImagingPointTableData.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapExternalData.h"

namespace imgptdatatest {
    using namespace TC;
    TEST_CASE("Custom Data Type operator== test") {
        SECTION("Position2D") {
            Position2D pos2d_1{1.1, 2.2};
            Position2D pos2d_2{1.1, 2.3};
            Position2D pos2d_3{1.2, 2.2};
            Position2D pos2d_4{1.2, 2.3};
            Position2D pos2d_5{1.1, 2.2};

            CHECK(pos2d_1!=pos2d_2);
            CHECK(pos2d_1!=pos2d_3);
            CHECK(pos2d_1!=pos2d_4);
            CHECK(pos2d_1==pos2d_5);
        }
        
        SECTION("Position3D") {
            Position3D pos3d_1{1.1, 2.2, 3.0};
            Position3D pos3d_2{1.1, 2.3, 3.0};
            Position3D pos3d_3{1.1, 2.2, 3.1};
            Position3D pos3d_4{1.2, 2.3, 3.0};
            Position3D pos3d_5{1.1, 2.2, 3.0};

            CHECK(pos3d_1!=pos3d_2);
            CHECK(pos3d_1!=pos3d_3);
            CHECK(pos3d_1!=pos3d_4);
            CHECK(pos3d_1==pos3d_5);
        }

        SECTION("Size2D") {
            Size2D pos2d_1{1.1, 2.2};
            Size2D pos2d_2{1.1, 2.3};
            Size2D pos2d_3{1.2, 2.2};
            Size2D pos2d_4{1.2, 2.3};
            Size2D pos2d_5{1.1, 2.2};

            CHECK(pos2d_1!=pos2d_2);
            CHECK(pos2d_1!=pos2d_3);
            CHECK(pos2d_1!=pos2d_4);
            CHECK(pos2d_1==pos2d_5);
        }
    }

    TEST_CASE("ImagingPointTableData Member Function test") {
        ImagingPointTableData data;

        CHECK(data.GetAcquisitionIndex() == 0);
        CHECK(data.GetWellIndex() == kInvalid);
        CHECK(data.GetPointIndexName() == "");
        CHECK(data.GetPosX() == 0.0);
        CHECK(data.GetPosY() == 0.0);
        CHECK(data.GetPosZ() == 0.0);
        CHECK(data.GetWellPosName() == "");
        CHECK(data.GetWidth() == 0.0);
        CHECK(data.GetSize().w == 0.0);
        CHECK(data.GetHeight() == 0.0);
        CHECK(data.GetSize().h == 0.0);

        data.SetAcquisitionIndex(1);
        data.SetWellIndex(2);
        data.SetPointIndexName("Point1");
        data.SetPosition(0.1, 0.2, 0.3);
        data.SetSize(0.4, 0.5);
        data.SetWellPosName("Pos1");

        CHECK(data.GetAcquisitionIndex() == 1);
        CHECK(data.GetWellIndex() == 2);
        CHECK(data.GetPointIndexName() == "Point1");
        CHECK(data.GetPointIndexName() != "Point1 ");
        CHECK(data.GetPointIndexName() != "point1");
        CHECK(data.GetPosX() == 0.1);
        CHECK(data.GetPosY() == 0.2);
        CHECK(data.GetPosZ() == 0.3);
        CHECK(data.GetWellPosName() == "Pos1");
        CHECK(data.GetWellPosName() != "Pos1 ");
        CHECK(data.GetWellPosName() != "pos1");
        CHECK(data.GetWidth() == 0.4);
        CHECK(data.GetSize().w == 0.4);
        CHECK(data.GetHeight() == 0.5);
        CHECK(data.GetSize().h == 0.5);
    }

    TEST_CASE("Table Data Compare test") {
        SECTION("Compare positive position - x") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(1, 2, 1);
            data2.SetPosition(1.1, 2, 1);
            data3.SetPosition(1.01, 2, 1);
            data4.SetPosition(1.001, 2, 1);
            data5.SetPosition(1.0001, 2, 1);
            data6.SetPosition(1.00001, 2, 1);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());
            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare negative position - x") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(-1, 2, 1);
            data2.SetPosition(-1.1, 2, 1);
            data3.SetPosition(-1.01, 2, 1);
            data4.SetPosition(-1.001, 2, 1);
            data5.SetPosition(-1.0001, 2, 1);
            data6.SetPosition(-1.00001, 2, 1);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());
            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare positive position - y") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(1, 2.0, 1);
            data2.SetPosition(1, 2.1, 1);
            data3.SetPosition(1, 2.01, 1);
            data4.SetPosition(1, 2.001, 1);
            data5.SetPosition(1, 2.0001, 1);
            data6.SetPosition(1, 2.00001, 1);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());

            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare negative position - y") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(1, -2.0, 1);
            data2.SetPosition(1, -2.1, 1);
            data3.SetPosition(1, -2.01, 1);
            data4.SetPosition(1, -2.001, 1);
            data5.SetPosition(1, -2.0001, 1);
            data6.SetPosition(1, -2.00001, 1);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());

            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare positive position - z") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(1, 2, 1.0);
            data2.SetPosition(1, 2, 1.1);
            data3.SetPosition(1, 2, 1.01);
            data4.SetPosition(1, 2, 1.001);
            data5.SetPosition(1, 2, 1.0001);
            data6.SetPosition(1, 2, 1.00001);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());

            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare negative position - z") {
            ImagingPointTableData data1;
            ImagingPointTableData data2;
            ImagingPointTableData data3;
            ImagingPointTableData data4;
            ImagingPointTableData data5;
            ImagingPointTableData data6;

            data1.SetPosition(1, -2, -1.0);
            data2.SetPosition(1, -2, -1.1);
            data3.SetPosition(1, -2, -1.01);
            data4.SetPosition(1, -2, -1.001);
            data5.SetPosition(1, -2, -1.0001);
            data6.SetPosition(1, -2, -1.00001);

            CHECK(data1.GetPosition()!=data2.GetPosition());
            CHECK(data1.GetPosition()!=data3.GetPosition());
            CHECK(data1.GetPosition()!=data4.GetPosition());

            CHECK(data1.GetPosition()==data5.GetPosition());
            CHECK(data1.GetPosition()==data6.GetPosition());
        }

        SECTION("Compare data size") {
            SECTION("Width") {
                ImagingPointTableData data1;
                ImagingPointTableData data2;
                ImagingPointTableData data3;
                ImagingPointTableData data4;
                ImagingPointTableData data5;
                ImagingPointTableData data6;

                data1.SetSize(25.0, 30);
                data2.SetSize(25.1, 30); // 0.1
                data3.SetSize(25.01, 30); // 0.01
                data4.SetSize(25.001, 30); // 0.001
                data5.SetSize(25.0001, 30); // 0.0001
                data6.SetSize(25.00001, 30); // 0.00001

                CHECK(data1.GetSize()!=data2.GetSize());
                CHECK(data1.GetSize()!=data3.GetSize());
                CHECK(data1.GetSize()!=data4.GetSize());

                CHECK(data1.GetSize()==data5.GetSize());
                CHECK(data1.GetSize()==data6.GetSize());
            }

            SECTION("Height") {
                ImagingPointTableData data1;
                ImagingPointTableData data2;
                ImagingPointTableData data3;
                ImagingPointTableData data4;
                ImagingPointTableData data5;
                ImagingPointTableData data6;

                data1.SetSize(10.0, 15.0);
                data2.SetSize(10.0, 15.1);
                data3.SetSize(10.0, 15.01);
                data4.SetSize(10.0, 15.001);
                data5.SetSize(10.0, 15.0001);
                data6.SetSize(10.0, 15.00001);

                CHECK(data1.GetSize()!=data2.GetSize());
                CHECK(data1.GetSize()!=data3.GetSize());
                CHECK(data1.GetSize()!=data4.GetSize());

                CHECK(data1.GetSize()==data5.GetSize());
                CHECK(data1.GetSize()==data6.GetSize());
            }
        }
    }

    TEST_CASE("Table Data Class Overloading test") {
        SECTION("Copy operator()") {
            ImagingPointTableData original;
            original.SetAcquisitionIndex(1);
            original.SetWellIndex(2);
            original.SetPointIndexName("Point1");
            original.SetPosition(0.1, 0.2, 0.3);
            original.SetSize(0.4, 0.5);
            original.SetWellPosName("Pos1");

            ImagingPointTableData copied(original);

            CHECK(original == copied);

            CHECK(original.GetAcquisitionIndex() == 1);
            CHECK(original.GetWellIndex() == 2);
            CHECK(original.GetPointIndexName() == "Point1");
            CHECK(original.GetPointIndexName() != "Point1 ");
            CHECK(original.GetPointIndexName() != "point1");
            CHECK(original.GetPosX() == 0.1);
            CHECK(original.GetPosY() == 0.2);
            CHECK(original.GetPosZ() == 0.3);
            CHECK(original.GetWellPosName() == "Pos1");
            CHECK(original.GetWellPosName() != "Pos1 ");
            CHECK(original.GetWellPosName() != "pos1");
            CHECK(original.GetWidth() == 0.4);
            CHECK(original.GetSize().w == 0.4);
            CHECK(original.GetHeight() == 0.5);
            CHECK(original.GetSize().h == 0.5);
        }

        SECTION("Pointer Copy operator()") {
            auto pOriginal = std::make_shared<ImagingPointTableData>();
            pOriginal->SetAcquisitionIndex(1);
            pOriginal->SetWellIndex(2);
            pOriginal->SetPointIndexName("Point1");
            pOriginal->SetPosition(0.1, 0.2, 0.3);
            pOriginal->SetSize(0.4, 0.5);
            pOriginal->SetWellPosName("Pos1");

            auto pCopied = pOriginal;

            CHECK(pCopied->GetAcquisitionIndex() == 1);
            CHECK(pCopied->GetWellIndex() == 2);
            CHECK(pCopied->GetPointIndexName() == "Point1");
            CHECK(pCopied->GetPosX() == 0.1);
            CHECK(pCopied->GetPosY() == 0.2);
            CHECK(pCopied->GetPosZ() == 0.3);
            CHECK(pCopied->GetWellPosName() == "Pos1");
            CHECK(pCopied->GetWidth() == 0.4);
            CHECK(pCopied->GetSize().w == 0.4);
            CHECK(pCopied->GetHeight() == 0.5);
            CHECK(pCopied->GetSize().h == 0.5);

            pOriginal = nullptr;

            CHECK(pCopied->GetAcquisitionIndex() == 1);
            CHECK(pCopied->GetWellIndex() == 2);
            CHECK(pCopied->GetPointIndexName() == "Point1");
            CHECK(pCopied->GetPosX() == 0.1);
            CHECK(pCopied->GetPosY() == 0.2);
            CHECK(pCopied->GetPosZ() == 0.3);
            CHECK(pCopied->GetWellPosName() == "Pos1");
            CHECK(pCopied->GetWidth() == 0.4);
            CHECK(pCopied->GetSize().w == 0.4);
            CHECK(pCopied->GetHeight() == 0.5);
            CHECK(pCopied->GetSize().h == 0.5);
        }

        SECTION("Move operator()") {
            ImagingPointTableData original;
            original.SetAcquisitionIndex(1);
            original.SetWellIndex(2);
            original.SetPointIndexName("Point1");
            original.SetPosition(0.1, 0.2, 0.3);
            original.SetSize(0.4, 0.5);
            original.SetWellPosName("Pos1");

            ImagingPointTableData moved(std::move(original));

            CHECK(original != moved);

            CHECK(original.GetAcquisitionIndex() == 0);
            CHECK(original.GetWellIndex() == kInvalid);
            CHECK(original.GetPointIndexName() == "");
            CHECK(original.GetPosX() == 0.0);
            CHECK(original.GetPosY() == 0.0);
            CHECK(original.GetPosZ() == 0.0);
            CHECK(original.GetWellPosName() == "");
            CHECK(original.GetWidth() == 0.0);
            CHECK(original.GetSize().w == 0.0);
            CHECK(original.GetHeight() == 0.0);
            CHECK(original.GetSize().h == 0.0);

            CHECK(moved.GetAcquisitionIndex() == 1);
            CHECK(moved.GetWellIndex() == 2);
            CHECK(moved.GetPointIndexName() == "Point1");
            CHECK(moved.GetPosX() == 0.1);
            CHECK(moved.GetPosY() == 0.2);
            CHECK(moved.GetPosZ() == 0.3);
            CHECK(moved.GetWellPosName() == "Pos1");
            CHECK(moved.GetWidth() == 0.4);
            CHECK(moved.GetSize().w == 0.4);
            CHECK(moved.GetHeight() == 0.5);
            CHECK(moved.GetSize().h == 0.5);
        }

        SECTION("Pointer Move operator()") {
            auto original = std::make_unique<ImagingPointTableData>();
            original->SetAcquisitionIndex(1);
            original->SetWellIndex(2);
            original->SetPointIndexName("Point1");
            original->SetPosition(0.1, 0.2, 0.3);
            original->SetSize(0.4, 0.5);
            original->SetWellPosName("Pos1");

            auto moved = std::move(original);

            CHECK(original != moved);
            CHECK(original == nullptr);

            CHECK(moved->GetAcquisitionIndex() == 1);
            CHECK(moved->GetWellIndex() == 2);
            CHECK(moved->GetPointIndexName() == "Point1");
            CHECK(moved->GetPosX() == 0.1);
            CHECK(moved->GetPosY() == 0.2);
            CHECK(moved->GetPosZ() == 0.3);
            CHECK(moved->GetWellPosName() == "Pos1");
            CHECK(moved->GetWidth() == 0.4);
            CHECK(moved->GetSize().w == 0.4);
            CHECK(moved->GetHeight() == 0.5);
            CHECK(moved->GetSize().h == 0.5);
        }
    }
}
