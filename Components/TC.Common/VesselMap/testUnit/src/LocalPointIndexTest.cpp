#include <catch2/catch.hpp>

#include <QString>
#include <QList>

namespace TC::VesselTest {
    TEST_CASE("Get int value from QString") {
        SECTION("Make point name format P###") {
            QString pointName = QString("P%1").arg(1, 3, 10, QChar('0'));
            CHECK(pointName == "P001"); // 한 자리

            pointName = QString("P%1").arg(11, 3, 10, QChar('0'));
            CHECK(pointName == "P011"); // 두 자리

            pointName = QString("P%1").arg(111, 3, 10, QChar('0'));
            CHECK(pointName == "P111"); // 세 자리

            pointName = QString("P%1").arg(1111, 3, 10, QChar('0'));
            CHECK(pointName == "P1111"); // 세 자리 초과

            pointName = QString("P%1").arg(12345, 3, 10, QChar('0'));
            CHECK(pointName == "P12345");
        }

        SECTION("Get int value from QString") {
            QString pointName = "P001";
            int32_t index = pointName.mid(1).toInt();
            CHECK(index == 1);

            pointName = "P010";
            index = pointName.mid(1).toInt();
            CHECK(index == 10);

            pointName = "P105";
            index = pointName.mid(1).toInt();
            CHECK(index == 105);

            pointName = "P1234";
            index = pointName.mid(1).toInt();
            CHECK(index == 1234);
        }
    }

    TEST_CASE("Imaging order container test") {
        SECTION("QLIST") {
            QList<int> list;
            list.push_back(5);
            list.push_back(4);
            list.push_back(1);
            list.push_back(3);

            CHECK(list.at(0) == 5);
            CHECK(list.at(1) == 4);
            CHECK(list.at(2) == 1);
            CHECK(list.at(3) == 3);

            CHECK(list.first() == 5);
            CHECK(list.last() == 3);

            CHECK(list.size() == 4);

            auto val = list.takeFirst();
            CHECK(val == 5);
            CHECK(list.first() == 4);
            CHECK(list.size() == 3);

        }
    }
}