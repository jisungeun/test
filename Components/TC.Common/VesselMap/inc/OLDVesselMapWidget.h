#pragma once

#include <QtWidgets/QWidget>
#include <memory>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"
#include "OLDVesselMap.h"
#include "VesselMapCustomDataTypes.h"

#include "VesselMap.h"

namespace TC {
    class TCVesselMap_API OLDVesselMapWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = OLDVesselMapWidget;
        using Pointer = std::shared_ptr<Self>;

        OLDVesselMapWidget(QWidget* parent = nullptr);
        ~OLDVesselMapWidget() override;

        auto SetViewMode(const ViewMode& viewMode) -> void;

        auto SetVesselMap(const OLDVesselMap::Pointer& vesselMap) -> void;

        // current vessel index set/get
        auto SetCurrentVessel(const VesselIndex& vesselIndex) -> void;
        auto GetCurrentVessel() const -> VesselIndex;

        // obj lens location
        auto SetObjectiveLensPos(const double& x, const double& y, const double& z) -> void;
        auto GetObjectObjectiveLensPosX() const -> double;
        auto GetObjectObjectiveLensPosY() const -> double;
        auto GetObjectObjectiveLensPosZ() const -> double;

        auto SetCurrentSelectedLocation(const WellIndex& wellIndex, const double& x, const double& y, const double& z) -> void;

        // mark location
        auto ImportMarkToAcqusitionPoint(const MarkIndex& markIndex) -> AcquisitionIndex;
        auto AddMarkLocationByWellCoord(const WellIndex& wellIndex, const double& x, const double& y, const double& z) -> MarkIndex; // 1.이미징포인트->마크로 강등, 2.스냅샷(?)버튼 누르면 current lens pos->마크포인트가 된다.
        auto AddMarkLocationBySystemCoord(const WellIndex& wellIndex, const double& x, const double& y, const double& z) -> MarkIndex;
        auto DeleteMarkLocation(const MarkIndex& markIndex) -> void;
        auto GetMarkLocations(const WellIndex& wellIndex) -> QList<QPointF>;
        auto GetMarkLocation(const MarkIndex& markIndex) const -> QPointF;
        auto ClearlAllMarkLocations() -> void;

        // acquisition location
        auto AddAcquisitionLocation(const WellIndex& wellIndex, const AcquisitionType& acqType, const double& x, const double& y, const double& z, double tileW = 0.0, double tileH = 0.0) -> AcquisitionIndex;
        auto DeleteAcquisitionLocation(const AcquisitionIndex& acqIndex) -> void;
        auto GetAcquisitionLocations(const WellIndex& wellIndex) const -> QList<QPointF>;
        auto GetAcquisitionLocation(const AcquisitionIndex& acqIndex) const -> QPointF;
        auto ClearlAllAcquisitionLocations() -> void;

        // preivew location
        auto AddPreviewLocation(const WellIndex& wellIndex, const double& x, const double& y, const double& width, const double& height) -> PreviewIndex;
        auto DeletePreviewLocation(const PreviewIndex& previewellIndex) -> void;
        auto GetPreviewLocations(const WellIndex& wellIndex) const -> QList<QPointF>;
        auto GetPreviewLocation(const PreviewIndex& previewellIndex) const -> QPointF;
        auto ClearAllPreviewLocations() -> void;

        // set preset size for preview / acq tile
        auto SetPreviewPresetSize(const QSizeF& size) -> void;
        auto SetAcqTilePresetSize(const QSizeF& size) -> void;

        // delete all
        auto ClearAll() -> void;

        // well canvas zoom
        auto WellCanvasZoomFit() -> void;
        auto WellCanvasZoomIn() -> void;
        auto WellCanvasZoomOut() -> void;

        // get index related
    public:
        auto GetAllWellIndices() const -> QList<WellIndex>;
        auto GetAllVesselIndices() const -> QList<VesselIndex>;
        auto GetAllGroupIndices() const -> QList<GroupIndex>;
        auto GetAllMarkIndices() const -> QList<MarkIndex>;
        auto GetAllAcquistionIndices() const -> QList<AcquisitionIndex>;
        auto GetAllPreviewIndices() const -> QList<PreviewIndex>;

        auto GetPreivewIndicesByWellIndex(const WellIndex& wellIndex) const -> QList<PreviewIndex>;

        auto SetCurrentFocusWellIndex(const WellIndex& wellIndex) -> void;
        auto GetCurrentFocusWellIndex() const -> WellIndex;

        // location data set/get
        auto GetCurrentSelectedMarkIndices() const -> QList<MarkIndex>;
        auto SetCurrentSelectedMarkIndices(const QList<MarkIndex>& markIndices) -> void;
        auto GetCurrentSelectedAcquisitionIndices() const -> QList<AcquisitionIndex>;
        auto SetCurrentSelectedAcquisitionIndices(const QList<AcquisitionIndex>& acqIndices) -> void;
        auto GetCurrentSelectedPreviewIndices() const -> QList<PreviewIndex>;
        auto SetCurrentSelectedPreviewIndices(const QList<PreviewIndex>& previewIndices) -> void;

        // group manage
    public:
        auto GetCurrentSelectedGroupIndices() const -> QList<GroupIndex>;
        auto CreateNewGroup(const QList<WellIndex>& wellIndices) -> GroupIndex;
        auto DeleteGroup(const GroupIndex& deleteGroupIndex) -> void;
        auto AddWellsToGroup(const GroupIndex& toGroupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void;
        auto ChangeGroupName(const GroupIndex& targetGroupIndex, const QString& newGroupName) -> void;
        auto ChangeGroupColor(const GroupIndex& targetGroupIndex, const QColor& newGroupColor) -> void;

    public:
        auto SetCurrentSelectedWellIndices(QList<WellIndex> wellIndices) -> void;
        auto GetCurrentSelectedWellIndices() const -> QList<WellIndex>;

    public:
        auto ShowLensPosOnVesselCanvas(bool show) -> void;
        auto ShowLensPosOnWellCanvas(bool show) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    signals:
        // to connect to widgets
        void sigSendLensPosOnWellCanvas(const Position3D& lensPos);
        void sigSendLensPosOnVesselCanvas(const Position3D& lensPos);

        void sigSendCurrentWellIndex(WellIndex wellIndex);
        void sigSendCurrentWellRowColumn(int32_t row, int32_t column);

    private slots:
        void onRecvCurrentFocusedWellIndex(const WellIndex& wellIndex);
        void onRecvCurrentSelectedWellIndices(const QList<WellIndex>& wellIndices);
        void onRecvCurrentSelectedGroupIndices(const QList<GroupIndex>& groupIndices);
        void onRecvCurrentSelectedMarkIndices(const QList<MarkIndex>& markIndices);
        void onRecvCurrentSelectedAcqIndices(const QList<AcquisitionIndex>& acqIndices);
        void onRecvCurrentSelectedPreviewIndices(const QList<PreviewIndex>& previewIndices);
        void onAddMarkLocationRequested(const QPointF& pos);
        void onAddAcqPointLocationRequested(const QPointF& pos);
        void onAddAcqTileLocationRequested(const QPointF& pos);
        void onAddPreviewLocationRequested(const QPointF& pos);
    };
}
