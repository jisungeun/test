﻿#pragma once

#include <memory>

#include <QPointF>
#include <QSizeF>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"
#include "OLDVesselMap.h"

namespace TC {
    class TCVesselMap_API OLDVesselMapBuilder {
    public:
        using Self = OLDVesselMapBuilder;
        using Pointer = std::shared_ptr<Self>;

        OLDVesselMapBuilder();
        ~OLDVesselMapBuilder();

        auto SetHolder(const QSizeF& size) -> void;

        auto SetVessel(const QSizeF& size, const VesselShape& shape, const QPointF& startPos, 
                       const int32_t& rows, const int32_t& cols, double hSpacing = 0.0, double vSpacing = 0.0) -> void;

        auto SetWell(const QSizeF& size, const WellShape& shape, const QPointF& startPos, 
                     const int32_t& rows, const int32_t& cols, double hSpacing = 0.0, double vSpacing = 0.0) -> void;

        auto SetWellImagingArea(const ImagingAreaShape& shape, const double& x, const double& y, 
                                const double& width, const double& height) -> void;

        auto GenerateVesselMap() const -> OLDVesselMap::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
