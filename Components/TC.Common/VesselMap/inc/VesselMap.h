﻿#pragma once

#include <memory>

#include <QList>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"

namespace TC {
    class Holder;
    class Vessel;
    class Well;
    class ImagingArea;
    class Roi;

    class TCVesselMap_API VesselMap {
    public:
        using Self = VesselMap;
        using Pointer = std::shared_ptr<Self>;
        using RangeX = RangeInMM;
        using RangeY = RangeInMM;

        VesselMap();
        VesselMap(const VesselMap& other);
        ~VesselMap();

        auto operator=(const VesselMap& other) -> VesselMap&;

        auto SetHolder(std::shared_ptr<Holder> holder) -> void;
        auto GetHolder() const -> std::shared_ptr<Holder>;

        auto AddVessel(std::shared_ptr<Vessel> vessel) -> void;
        auto GetVessels() const -> QList<std::shared_ptr<Vessel>>&;

        auto AddWell(std::shared_ptr<Well> well) -> void;
        auto GetWells() const -> QList<std::shared_ptr<Well>>;

        auto SetImagingArea(std::shared_ptr<ImagingArea> imagingArea) -> void;
        auto SetImagingArea(ImagingAreaShape shape, double centerX, double centerY, double width, double height) -> void;
        auto GetImagingArea() const -> std::shared_ptr<ImagingArea>;
        auto GetImagingAreaRange() const -> Range2D;

        auto SetHolderSize(double width, double height) -> void;

        auto SetVesselShape(VesselShape shape) -> void;
        auto SetVesselSize(double width, double height) -> void;
        auto SetVessel(VesselIndex vesselIndex, double centerX, double centerY, int32_t rowCount, int32_t columnCount) -> void;

        auto SetWellShape(WellShape shape) -> void;
        auto SetWellSize(double width, double height) -> void;
        auto SetWell(WellIndex wellIndex, int32_t row, int32_t column, double centerX, double centerY, QString name = "") -> void;

        auto AddRoi(WellIndex wellIndex, RoiIndex roiIndex, const QString& name, RoiShape shape, double centerX, double centerY, double width, double height) -> void;
        auto SetRoiList(const QMap<WellIndex, QList<std::shared_ptr<Roi>>>& roiList) -> void;
        auto GetRoiList() const -> QMap<WellIndex, QList<std::shared_ptr<Roi>>>;
        auto GetRoiList(const WellIndex& wellIndex) const -> QList<std::shared_ptr<Roi>>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
