﻿#pragma once

#include <QWidget>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"
#include "VesselMap.h"

namespace TC {
    class TCVesselMap_API VesselMapWidget : public QWidget {
        Q_OBJECT
	public:
        using Self = VesselMapWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselMapWidget(QWidget* parent = nullptr);
        ~VesselMapWidget() override;

        auto SetViewMode(ViewMode mode) -> void;
        auto ClearAll() const -> void;

        auto SetVesselMap(VesselMap::Pointer vesselMap) -> void;
        auto SetSafePositionRange(const VesselAxis& axis, const double& minMM, const double& maxMM) -> void;
        auto SetSelectedWell(WellIndex wellIndex) -> void;

        auto SetCurrentAcquisitionPointByWellPos(LocationType locationType, double xMM, double yMM) -> void;
        
        auto SetPreviewPresetSize(double width, double height) -> void;
        auto SetAcqTilePresetSize(double width, double height) -> void;

        auto GetWellIndex(int32_t row, int32_t column) const -> WellIndex;
        auto GetWellRow(WellIndex wellIndex) const -> int32_t;
        auto GetWellColumn(WellIndex wellIndex) const -> int32_t;

        auto ShowObjectiveLens(bool vessel) -> void;
        auto SetObjectiveLensPos(double x, double y, double z) -> void;
        auto GetObjectiveLensPosX() const -> double;
        auto GetObjectiveLensPosY() const -> double;
        auto GetObjectiveLensPosZ() const -> double;

        auto SetCurrentVessel(VesselIndex vesselIndex) -> void;
        auto GetCurrentVessel() const -> VesselIndex;

        auto GetCurrentSelectedWellIndices() const -> QList<WellIndex>&;
        auto GetSelectedWellIndicesHasNoGroup() const -> QList<WellIndex>;
        auto GetAllVesselIndices() const -> QList<VesselIndex>;
        auto GetSelectedGroupIndices() const -> QList<GroupIndex>&;
        auto GetFocusedWellIndex() const -> WellIndex;

        auto CreateNewGroup(GroupIndex groupIndex, 
                            const QString& groupName,
                            const QColor& groupColor,
                            const QList<WellIndex>& wellIndices) -> void;

        auto DeleteGroup(GroupIndex groupIndex) -> void;
        auto DeleteGroups(const QList<GroupIndex>& groupIndices) -> void;
        auto AddWellsToGroup(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void;
        auto ChangeGroupName(GroupIndex groupIndex, const QString& name) -> void;
        auto ChangeGroupColor(GroupIndex groupIndex, const QColor& color) -> void;
        auto MoveGroup(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void;
        auto ChangeWellName(WellIndex wellIndex, const QString& name) -> void;

        auto AddMarkLocationBySystemPos(WellIndex wellIndex, MarkType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> MarkIndex;
        auto AddMarkLocationByWellPos(WellIndex wellIndex, MarkType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> MarkIndex;

        auto SetMarkLocationBySystemPos(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> void;
        auto SetMarkLocationByWellPos(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> void;
        auto DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;

        // tile imaging activation 되면 well canvas에 편집 가능한 tile img 영역이 나타남
        auto ShowTileImagingArea(bool show) -> void;
        auto SetTileImagingArea(double x, double y, double z, double w, double h) -> void;
        auto SetTileImagingAreaCenter(double xInMM, double yInMM, double zInMM)->void;

        // TC::AcquisitionIndex value should be the same as AppEntity::LocationIndex value
        auto AddAcquisitionLocationBySystemPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> void;
        auto AddAcquisitionLocationByWellPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w = 0.0, double h = 0.0) -> void;

        // TODO acqIndex invalid 하면 새로 생성하지 말고 false 리턴하도록 변경
        auto SetAcquisitionLocationBySystemPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w= 0.0, double h = 0.0) -> void;
        auto SetAcquisitionLocationByWellPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w= 0.0, double h = 0.0) -> void;
        auto DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetAcquisitionLocationDeleteEnabled(bool enabled) -> void;

        auto SetPreviewLocation() -> void;
        auto SetPreviewLocationByWellPos(WellIndex wellIndex, double x, double y, double w, double h) -> void;
        auto SetPreviewLocationEditable(bool editable) -> void;
        auto ShowPreviewLocation() -> void;
        auto HidePreviewLocation() -> void;

        auto SetWellCanvasFixedSize(int w, int h)->void;
        auto FitWellCanvas()->void;

        auto StartCustomPreviewAreaSetting() -> void;
        auto StopCustomPreviewAreaSetting() -> void;

        auto SetWellImagingOrder(const QList<WellIndex>& wells) -> void;

        auto SetSelectedWellIndices(const QList<TC::WellIndex>& wellIndices) -> void;
        auto SetMatrixItemVisible(bool visible) -> void;
        auto SetMatrixItems(const QList<QPair<double,double>>& positions) -> void;

    signals:
        // vessel canvas에서 선택되어 있는 well 정보
        void sigSelectedWellIndices(const QList<TC::WellIndex>& wellIndices);

        // vessel canvas에서 선택되어 있는 group 정보
        void sigSelectedGroupIndices(const QList<TC::GroupIndex>& groupIndices);

        // vessel canvas에서 focus well이 변경됐을 때 변경된 well의 index, row, column 정보
        void sigChangeFocusedWell(TC::WellIndex wellIndex, int32_t row, int32_t column);

        // imaging pt list에서 focus well이 변경됐을 때 변경된 well의 index 정보
        void sigChangeFocusedWell(TC::WellIndex wellIndex);

        // group table에서 group name 변경 시 알림
        void sigChangedGroupName(TC::GroupIndex groupIndex, const QString& groupName);

        // group table에서 group color 변경 시 알림
        void sigChangedGroupColor(TC::GroupIndex groupIndex, const QColor& groupColor);

        // group table에서 group이 다른 group로 이동될 시 알림
        void sigMovedGroup(TC::GroupIndex movingGroupIndex, TC::GroupIndex targetGroupIndex);

        // group table에서 well name 변경 시 알림
        void sigChangedWellName(TC::WellIndex wellIndex, const QString& wellName);

        // well canvas에서 마우스 왼쪽 더블클릭된 위치 정보(location 아이템 제외)
        void sigDoubleClickedWellCanvasPosition(double xInMM, double yInMM);

        // well canvas에서 location item을 더블클릭 했을 때 location 아이템의 위치 정보
        void sigDoubleClickedLocationPosition(double xInMM, double yInMM, double zInMM);

        // well canvas에서 preview item 편집 시 알림
        void sigPreviewGeometryChanged(const TC::WellIndex& wellIndex, const double& centerX, const double& centerY, const double& width, const double& height);
        void sigPreviewAreaChanged(const double& centerXInMM, const double& centerYInMM, const double& widthMM, const double& heightMM);

        // img pt list에서 tile item에 대해 show detail 메뉴 선택 시 알림
        void sigRequestTileConditionPopUp(const TC::WellIndex&, const TC::AcquisitionIndex&);

        // when mark item deleted
        void sigMarkItemDeleted(const TC::WellIndex&, const TC::MarkIndex&);

        // when acq item deleted
        void sigAcquisitionItemDeleted(const TC::WellIndex&, const TC::AcquisitionIndex&);

        // mark item 더블 클릭 시 well index 및 mark index 반환
        void sigMarkItemDoubleClikced(const TC::WellIndex&, const TC::MarkIndex&);

        // acquisition item 더블 클릭 시 well index 및 acquisition index 반환
        void sigAcquisitionItemDoubleClicked(const TC::WellIndex&, const TC::AcquisitionIndex&);

        // tile imaging area geometry 변경 시 발생
        void sigTileImagingAreaChanged(const double& centerXInMM, const double& centerYInMM, const double& z, const double& widthInMM, const double& heightInMM);

        // timelapse 수행 중 point 선택
        void sigImagingPointSelected(const QString& wellPosition, const QString& pointID);

        // 사용자가 Objective Lens 위치를 변경 시 발생
        void sigChangeLensPosition(const TC::VesselAxis axis,const double position);

        // Vessel map에서 Imaging order를 변경한 경우 발생
        void sigChangeImagingOrder(const TC::ImagingOrder order);

        // Imaging point list에서 copy 버튼 눌렀을 때 발생
        void sigCopyImagingPoints(const WellIndex& sourceWellIndex, const QList<AcquisitionIndex>& copyPointIndices);

        // internal use only
    private slots:
        void onRecvFocusedWellIndexChanged(const WellIndex& wellIndex, int32_t row, int32_t column);
        void onRecvFocusedWellIndexChanged(const WellIndex& wellIndex);
        void onRecvGroupNameChanged(GroupIndex groupIndex, const QString& groupName);
        void onRecvGroupColorChanged(GroupIndex groupIndex, const QColor& groupColor);
        void onRecvWellNameChanged(WellIndex wellIndex, const QString& wellName);
        void onRecvPreviewGeoChangedOnWellCanvas(const double& centerX, const double& centerY, const double& width, const double& height);
        void onRecvTileImagingGeoChangedOnWellCanvas(const double& centerX, const double& centerY, const double& z, const double& width, const double& height);
        void onRecvDeletedMark(const WellIndex& wellIndex, const MarkIndex& markIndex);
        void onRecvDeletedAcquisition(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex);
        void onRecvMarkItemDblClicked(const WellIndex& wellIndex, const MarkIndex& markIndex, double x, double y);
        void onRecvAcquisitionItemDblClicked(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex, double x, double y, double z);
        void onRecvImagingPointShowAllButtonToggled(bool showAll);
        void onRecvImagingOrderChanged(const TC::ImagingOrder& order);
        void onRecvImagingOrderShowRequest(bool show);
        void onImportAcquisitionToMark(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex);
        void onRecvImagingPointCopyTargets(const WellIndex& sourceWellIndex, const QList<AcquisitionIndex>& copyPointIndices);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
