#pragma once

#include <memory>

#include <QString>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"

namespace TC {
    class TCVesselMap_API OLDVesselMap {
    public:
        using Self = OLDVesselMap;
        using Pointer = std::shared_ptr<Self>;

        OLDVesselMap();
        ~OLDVesselMap();

        auto SetHolderSize(const double& w, const double& h) -> void;
        auto GetHolderWidth() const -> double;
        auto GetHolderHeight() const -> double;

        auto SetVesselSize(const double& w, const double& h) -> void;
        auto GetVesselWidth() const -> double;
        auto GetVesselHeight() const -> double;

        auto SetVesselShape(const VesselShape& shape) -> void;
        auto GetVesselShape() const -> VesselShape;

        auto InitVesselContainer(const VesselIndex& vesselIndex) -> void;

        auto SetVesselPos(const VesselIndex& vesselIndex, const double& x, const double& y) -> void;
        auto GetVesselPosX(const VesselIndex& vesselIndex) const -> double;
        auto GetVesselPosY(const VesselIndex& vesselIndex) const -> double;

        auto GetVesselIndices() const -> QList<VesselIndex>;
        auto GetFirstVesselIndex() const -> VesselIndex;

        auto SetWellSize(const double& w, const double& h) -> void;
        auto GetWellWidth() const -> double;
        auto GetWellHeight() const -> double;

        auto SetWellShape(const WellShape& shape) -> void;
        auto GetWellShape() const -> WellShape;

        auto SetWellName(const VesselIndex& vesselIndex, const WellIndex& wellIndex, const QString& wellName) -> void;
        auto GetWellName(const VesselIndex& vesselIndex, const WellIndex& wellIndex) const -> QString;

        auto SetWellPos(const VesselIndex& vesselIndex, const WellIndex& wellIndex, const double& x, const double& y) -> void;
        auto GetWellPosX(const VesselIndex& vesselIndex, const WellIndex& wellIndex) const -> double;
        auto GetWellPosY(const VesselIndex& vesselIndex, const WellIndex& wellIndex) const -> double;

        auto GetWellIndicesByVesselIndex(const VesselIndex& vesselIndex) const -> QList<WellIndex>;
        auto GetVesselIndexByWellIndex(const WellIndex& wellIndex) const -> VesselIndex;

        auto SetVesselCount(int32_t rows, int32_t columns) -> void;
        auto SetWellCount(int32_t rows, int32_t columns) -> void;

        auto GetVesselRows() const -> int32_t;
        auto GetVesselColumns() const -> int32_t;

        auto GetWellRows() const -> int32_t;
        auto GetWellColumns() const-> int32_t;

        auto SetVesselHorizontalSpacing(double spacing) -> void;
        auto GetVesselHorizontalSpacing() const -> double;

        auto SetVesselVerticalSpacing(double spacing) -> void;
        auto GetVesselVerticalSpacing() const -> double;

        auto SetWellHorizontalSpacing(double spacing) -> void;
        auto GetWellHorizontalSpacing() const -> double;

        auto SetWellVerticalSpacing(double spacing) -> void;
        auto GetWellVerticalSpacing() const -> double;

        auto SetImagingAreaShape(const ImagingAreaShape& shape)->void;
        auto GetImagingAreaShape() const->TC::ImagingAreaShape;

        auto SetImagingArea(double x, double y, double width, double height) -> void;
        auto GetImagingArea() const -> std::tuple<double, double, double, double>;

        auto SetWellCount(const VesselIndex& vesselIndex, const WellIndex& wellIndex, int32_t row, int32_t col) -> void;
        auto GetWellRow(const VesselIndex& vesselIndex, const WellIndex& wellIndex) const -> int32_t;
        auto GetWellColumn(const VesselIndex& vesselIndex, const WellIndex& wellIndex) const -> int32_t;


    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
