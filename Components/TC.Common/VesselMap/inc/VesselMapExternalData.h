﻿#pragma once
#include <enum.h>
#include <QMetaType>

namespace TC {
    using VesselIndex = int32_t;
    using WellIndex = int32_t;
    using GroupIndex = int32_t;
    using MarkIndex = int32_t;
    using AcquisitionIndex = int32_t;
    using RoiIndex = int32_t;

    constexpr int32_t kInvalid = -1;

    BETTER_ENUM(ImagingAreaShape, int32_t, Circle, Rectangle);

    BETTER_ENUM(MarkType, int32_t, Point, Tile);

    BETTER_ENUM(AcquisitionType, int32_t, Point, Tile);

    BETTER_ENUM(ViewMode, int32_t, SetupMode, PerformMode, CanvasMode, InfoMode, TimelapseMode, PreviewMode, DataNaviMode, CopyDlgMode);

    BETTER_ENUM(VesselShape, int32_t, Circle, Rectangle);

    BETTER_ENUM(WellShape, int32_t, Circle, Rectangle);

    BETTER_ENUM(EditMode, int32_t, Editable, ReadOnly);

    BETTER_ENUM(ImagingOrder, int32_t, ByWell = 8, BySpecimen);

    BETTER_ENUM(LocationType, int32_t, MarkLocation, AcquisitionLocation);

    BETTER_ENUM(RoiShape, int32_t, Ellipse, Rectangle);

    enum class VesselAxis {
        X,
        Y,
        Z,
        Inavlid
    };

    struct RangeInMM {
        double min{};
        double max{};
    };
    using RangeX = RangeInMM;
    using RangeY = RangeInMM;

    class Range2D {
        public:
            auto setXRange(const RangeX& range) -> void {
                this->x = range;
            }

            auto setYRange(const RangeY& range) -> void {
                this->y = range;
            }

            auto rangeX() const -> const RangeInMM& {
                return this->x;
            }

            auto rangeY() const -> const RangeInMM& {
                return this->y;
            }

        private:
            RangeX x{};
            RangeY y{};
    };
}

Q_DECLARE_METATYPE(TC::VesselAxis)
