﻿#pragma once

#include <memory>

#include "TCVesselMapExport.h"
#include "VesselMapExternalData.h"
#include "VesselMap.h"

namespace TC {
    struct HolderInput;
    struct VesselInput;
    struct WellInput;
    struct ImagingAreaInput;

    class TCVesselMap_API VesselMapBuilder {
    public:
        using Self = VesselMapBuilder;
        using Pointer = std::shared_ptr<Self>;

        VesselMapBuilder();
        VesselMapBuilder(const VesselMapBuilder& other);
        ~VesselMapBuilder();

        auto operator=(const VesselMapBuilder& other) -> VesselMapBuilder&;

        auto SetHolder(double width, double height) -> void;
        auto SetHolder(HolderInput holderInput) -> void;

        auto SetVessel(VesselShape shape, double startX, double startY, double width, double height, int32_t rows, int32_t columns, double horizontalSpacing, double verticalSpacing) -> void;
        auto SetVessel(VesselInput vesselInput) -> void;

        auto SetWell(WellShape shape, double startX, double startY, double width, double height, int32_t rows, int32_t columns, double horizontalSpacing, double verticalSpacing) -> void;
        auto SetWell(WellInput wellInput) -> void;

        auto SetImagingArea(ImagingAreaShape shape, double centerX, double centerY, double width, double height) -> void;

        auto GenerateVesselMap() -> VesselMap::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    struct HolderInput {
        double width;
        double height;
    };

    struct VesselInput {
        VesselShape shape{VesselShape::Rectangle};
        double startX{0.0};
        double startY{0.0};
        double width{0.0};
        double height{0.0};
        int32_t rows{0};
        int32_t columns{0};
        double horizontalSpacing{0.0};
        double verticalSpacing{0.0};
    };

    struct WellInput {
        WellShape shape{WellShape::Circle};
        double startX{0.0};
        double startY{0.0};
        double width{0.0};
        double height{0.0};
        int32_t rows{0};
        int32_t columns{0};
        double horizontalSpacing{0.0};
        double verticalSpacing{0.0};
    };

    struct ImagingAreaInput {
        ImagingAreaShape shape{ImagingAreaShape::Circle};
        double centerX{0.0};
        double centerY{0.0};
        double width{0.0};
        double height{0.0};
    };
}
