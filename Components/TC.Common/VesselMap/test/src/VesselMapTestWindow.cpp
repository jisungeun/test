#include <QDebug>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QColorDialog>
#include <QCheckBox>
#include <QRandomGenerator>

#include <enum.h>
#include "VesselMapTestWindow.h"
#include "ui_VesselMapTestWindow.h"

#include "VesselMap.h"
#include "VesselMapBuilder.h"
#include "VesselMapWidget.h"
#include "../src/Well.h"

#include "TileConditionPanel.h"
#include "TileConditionConfig.h"

using HTXpress::AppComponents::TileConditionPanel::TileConditionPanel;
using HTXpress::AppComponents::TileConditionPanel::TileConditionConfig;

namespace TC::TEST {
    struct VesselMapTestWindow::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};
        Ui::TestWindow ui;
        VesselMapWidget* widget{nullptr};
        VesselMap::Pointer vesselMap{nullptr};

        void Connect();
        void InitVesselMapView();
    };

    VesselMapTestWindow::VesselMapTestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->InitVesselMapView();
        d->Connect();
        d->ui.tabWidget->setCurrentIndex(0);

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, [&](int index) {
            if(index == 2) {
                d->widget->SetViewMode(ViewMode::PerformMode);
            }
        });
    }

    VesselMapTestWindow::~VesselMapTestWindow() {
    }

    void VesselMapTestWindow::onClearAll() {
        d->widget->ClearAll();
    }

    void VesselMapTestWindow::onSetViewMode(int32_t idx) {
        d->widget->SetViewMode(ViewMode::_from_index_unchecked(idx));
    }

    void VesselMapTestWindow::onCreateVesselMap() {
        const QSizeF holderSize{d->ui.hWidth->value(), d->ui.hHeight->value()};

        const QSizeF vesselSize{d->ui.vWidth->value(), d->ui.vHeight->value()};
        const QPointF vesselStartPos{d->ui.vStartPosX->value(), d->ui.vStartPosY->value()};
        const int vesselRows{d->ui.vRows->value()};
        const int vesselCols{d->ui.vColums->value()};
        const VesselShape vesselShape{VesselShape::_from_index(d->ui.vShape->currentIndex())};
        const double vesselHSpacing{d->ui.spVesselHSpacing->value()};
        const double vesselVSpacing{d->ui.spVesselVSpacing->value()};

        const QSizeF wellSize{d->ui.wWidth->value(), d->ui.wHeight->value()};
        const WellShape wellShape{WellShape::_from_index(d->ui.wShape->currentIndex())};
        const QPointF wellStartPos{d->ui.wStartPosX->value(), d->ui.wStartPosY->value()};
        const int wellRows{d->ui.wRows->value()};
        const int wellCols{d->ui.wColums->value()};
        const double wellHSpacing{d->ui.spWellHSpacing->value()};
        const double wellVSpacing{d->ui.spWellVSpacing->value()};

        const double imgX = {d->ui.imagingAreaX->value()};
        const double imgY = {d->ui.imagingAreaY->value()};
        const double imgW = {d->ui.imagingAreaW->value()};
        const double imgH = {d->ui.imagingAreaH->value()};
        const auto imgAreaShape{ImagingAreaShape::_from_index(d->ui.imgAreaShape->currentIndex())};

        auto newBuilder = std::make_shared<VesselMapBuilder>();
        newBuilder->SetHolder(holderSize.width(), holderSize.height());
        newBuilder->SetVessel(vesselShape, vesselStartPos.x(), vesselStartPos.y(), vesselSize.width(), vesselSize.height(), vesselRows, vesselCols, vesselHSpacing, vesselVSpacing);
        newBuilder->SetWell(wellShape, wellStartPos.x(), wellStartPos.y(), wellSize.width(), wellSize.height(), wellRows, wellCols, wellHSpacing, wellVSpacing);
        newBuilder->SetImagingArea(imgAreaShape, imgX, imgY, imgW, imgH);

        d->vesselMap = newBuilder->GenerateVesselMap();
        d->widget->SetVesselMap(d->vesselMap);

    }

    void VesselMapTestWindow::onChangeLensPos() {
        const auto x = d->ui.lensPosX->value();
        const auto y = d->ui.lensPosY->value();
        const auto z = d->ui.lensPosZ->value();
        d->widget->SetObjectiveLensPos(x, y, z);
    }

    void VesselMapTestWindow::onShowLensItem() {
        const auto show = d->ui.chkShowLens->isChecked();
        d->widget->ShowObjectiveLens(show);
    }

    void VesselMapTestWindow::onAddMark() {
        WellIndex index = d->widget->GetFocusedWellIndex();
        const auto x = d->ui.lensPosX->value();
        const auto y = d->ui.lensPosY->value();
        const auto z = d->ui.lensPosZ->value();
        const auto type = MarkType::_from_integral(d->ui.comboMarkType->currentIndex());
        const auto w = d->ui.spMarkWidth->value();
        const auto h = d->ui.spMarkHeight->value();
        const auto newMarkIndex = d->widget->AddMarkLocationBySystemPos(index, type, x, y, z, w, h);
        d->ui.markIndex->setValue(newMarkIndex + 1);
    }

    void VesselMapTestWindow::onSetMark() {
        WellIndex wellIndex = d->ui.locationWellIndex->value();
        MarkIndex markIndex = d->ui.markIndex->value();
        const auto x = d->ui.lensPosX->value();
        const auto y = d->ui.lensPosY->value();
        const auto z = d->ui.lensPosZ->value();
        const auto type = MarkType::_from_integral(d->ui.comboMarkType->currentIndex());
        const auto w = d->ui.spMarkWidth->value();
        const auto h = d->ui.spMarkHeight->value();
        d->widget->SetMarkLocationBySystemPos(wellIndex, markIndex, type, x, y, z, w, h);
    }

    void VesselMapTestWindow::onDelMark() {
        WellIndex wellIndex = d->ui.locationWellIndex->value();
        MarkIndex markIndex = d->ui.markIndex->value();
        d->widget->DeleteMarkLocation(wellIndex, markIndex);
    }

    void VesselMapTestWindow::onAddAcq() {
        WellIndex index = d->widget->GetFocusedWellIndex();
        AcquisitionIndex acqIndex = d->ui.acqIndex->value();
        const auto x = d->ui.lensPosX->value();
        const auto y = d->ui.lensPosY->value();
        const auto z = d->ui.lensPosZ->value();
        const auto type = AcquisitionType::_from_integral(d->ui.comboAcqType->currentIndex());
        const auto w = d->ui.acqWidth->value();
        const auto h = d->ui.acqHeight->value();
        d->widget->AddAcquisitionLocationBySystemPos(index, acqIndex, type, x, y, z, w, h);
        d->ui.acqIndex->setValue(acqIndex+1);
    }

    void VesselMapTestWindow::onSetAcq() {
        WellIndex wellIndex = d->ui.locationWellIndex->value();
        AcquisitionIndex acqIndex = d->ui.acqIndex->value();
        const auto x = d->ui.lensPosX->value();
        const auto y = d->ui.lensPosY->value();
        const auto z = d->ui.lensPosZ->value();
        const auto type = AcquisitionType::_from_integral(d->ui.comboAcqType->currentIndex());
        const auto w = d->ui.acqWidth->value();
        const auto h = d->ui.acqHeight->value();
        d->widget->SetAcquisitionLocationBySystemPos(wellIndex, acqIndex, type, x, y, z, w, h);
    }

    void VesselMapTestWindow::onDelAcq() {
        WellIndex wellIndex = d->ui.locationWellIndex->value();
        AcquisitionIndex acqIndex = d->ui.acqIndex->value();
        d->widget->DeleteAcquisitionLocation(wellIndex, acqIndex);
    }

    void VesselMapTestWindow::onAddPreview() {
        //WellIndex index = d->widget->GetFocusedWellIndex();
        //const auto x = d->ui.lensPosX->value();
        //const auto y = d->ui.lensPosY->value();
        //const auto w = d->ui.previewWidth->value();
        //const auto h = d->ui.previewHeight->value();
    }

    void VesselMapTestWindow::onSetPreview() {
        //WellIndex index = d->widget->GetFocusedWellIndex();
        //const auto x = d->ui.lensPosX->value();
        //const auto y = d->ui.lensPosY->value();
        //const auto w = d->ui.previewWidth->value();
        //const auto h = d->ui.previewHeight->value();
    }

    void VesselMapTestWindow::onDelPreview() {
        //d->widget->HidePreviewLocation();
    }
    
    void VesselMapTestWindow::onSetPreviewEditable(bool editable) {
        d->widget->SetPreviewLocationEditable(editable);
    }

    void VesselMapTestWindow::onCreateNewGroup() {
        const auto newGroupIndex = d->ui.spNewGroupIndex->value();
        const auto r = QRandomGenerator::global()->bounded(0,255);
        const auto b = QRandomGenerator::global()->bounded(0,255);
        const auto g = QRandomGenerator::global()->bounded(0,255);
        d->widget->CreateNewGroup(newGroupIndex,
                                  QString("TestGroup%1").arg(newGroupIndex),
                                  QColor(r,b,g),
                                  d->widget->GetCurrentSelectedWellIndices());
        d->ui.spNewGroupIndex->setValue(newGroupIndex+1);
    }

    void VesselMapTestWindow::onDeleteGroup() {
        qDebug() << "onDeleteGroup idx" << d->widget->GetSelectedGroupIndices();
        d->widget->DeleteGroups(d->widget->GetSelectedGroupIndices());
    }

    void VesselMapTestWindow::onAddWellToGroup() {
        d->widget->AddWellsToGroup(d->ui.spGroupIndexSelect->value(), d->widget->GetCurrentSelectedWellIndices());
    }

    void VesselMapTestWindow::onRemoveWellFromGroup() {
        d->widget->RemoveWellsFromGroup(d->widget->GetCurrentSelectedWellIndices());
    }

    void VesselMapTestWindow::onChangeGroupName() {
        const auto name = d->ui.editNewGroupName->text();
        const auto idx = d->ui.spGroupIndexSelect->value();
        d->widget->ChangeGroupName(idx, name);
    }

    void VesselMapTestWindow::onChangeGroupColor() {
        const auto r = d->ui.spR->value();
        const auto g = d->ui.spG->value();
        const auto b = d->ui.spB->value();
        const auto a = d->ui.spA->value();
        const auto idx = d->ui.spGroupIndexSelect->value();
        d->widget->ChangeGroupColor(idx, QColor(r, g, b, a));
    }

    void VesselMapTestWindow::onTileImagingToggled(bool show) {
        d->widget->ShowTileImagingArea(show);
    }

    void VesselMapTestWindow::onSetTileImagingArea() {
        if(!d->ui.chkTileImaging->isChecked()) {
            return;
        }

        double x = d->ui.tileImgX->value();
        double y = d->ui.tileImgY->value();
        double z = d->ui.tileImgZ->value();
        double w = d->ui.tileImgW->value();
        double h = d->ui.tileImgH->value();
        d->widget->SetTileImagingArea(x,y,z,w,h);
    }

    void VesselMapTestWindow::onAddTileImagingAreaToAcqPoint() {
        WellIndex wellIndex = d->widget->GetFocusedWellIndex();
        AcquisitionIndex acqIndex = d->ui.acqIndex->value();
        double x = d->ui.tileImgX->value();
        double y = d->ui.tileImgY->value();
        double z = d->ui.tileImgZ->value();
        double w = d->ui.tileImgW->value();
        double h = d->ui.tileImgH->value();

        d->widget->AddAcquisitionLocationByWellPos(wellIndex, acqIndex, AcquisitionType::Tile, x, y, z, w, h);
        d->ui.acqIndex->setValue(acqIndex+1);
    }

    void VesselMapTestWindow::onRecvChangedWellName(WellIndex idx, const QString& name) {
        qDebug() << "onRecvChangedWellName" << idx << name;
    }

    void VesselMapTestWindow::onRecvChangedGroupName(GroupIndex idx, const QString& name) {
        qDebug() << "onRecvChangedGroupName" << idx << name;
    }

    void VesselMapTestWindow::onRecvChangedGroupColor(GroupIndex idx, const QColor& color) {
        qDebug() << "onRecvChangedGroupColor" << idx << color;
    }

    void VesselMapTestWindow::onRecvChangeFocusedWell(WellIndex idx, int32_t row, int32_t col) {
        qDebug() << "onRecvChangeFocusedWell" << idx << row << col;
        d->ui.locationWellIndex->setValue(idx);
        d->ui.currWellIdx->setText(QString::number(idx));
        d->ui.currWellRow->setText(QString::number(row));
        d->ui.currWellCol->setText(QString::number(col));
        for(const auto& well : d->vesselMap->GetWells()) {
            if(well->GetIndex() == idx) {
                d->widget->SetObjectiveLensPos(well->GetX(), well->GetY(), d->ui.lensPosZ->value());
                d->ui.lensPosX->setValue(well->GetX());
                d->ui.lensPosY->setValue(well->GetY());
                break;
            }
        }
    }

    void VesselMapTestWindow::onRecvChangePreviewLocation(WellIndex well, double x, double y, double w, double h) {
        qDebug() << "onRecvChangePreviewLocation" << well << x << y << w << h;
    }

    void VesselMapTestWindow::onRecvSelectedWellIndices(const QList<WellIndex>& wells) {
        qDebug() << "onRecvSelectedWellIndices" << wells;
    }

    void VesselMapTestWindow::onRecvSelectedGroupIndices(const QList<GroupIndex>& groups) {
        qDebug() << "onRecvSelectedGroupIndices" << groups;
    }

    void VesselMapTestWindow::onRecvDoubleClickedWellCanvasPosition(double x, double y) {
        qDebug() << "onRecvDoubleClickedWellCanvasPosition" << x << y;
    }

    void VesselMapTestWindow::onRecvDoubleClickedWellCanvasMarkIndex(WellIndex wellIndex, MarkIndex markIndex) {
        qDebug() << "onRecvDoubleClickedWellCanvasMarkIndex" << wellIndex << markIndex;
    }

    void VesselMapTestWindow::onRecvDoubleClickedWellCanvasAcqIndex(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) {
        qDebug() << "onRecvDoubleClickedWellCanvasIndex" << wellIndex << acquisitionIndex;
    }

    void VesselMapTestWindow::onRecvRequestTileConditionPopUp(const WellIndex& well, const AcquisitionIndex& acq) {
        qDebug() << "onRecvRequestTileConditionPopUp" << well << acq;
    }

    void VesselMapTestWindow::onRecvChangedTileImagingArea(double x, double y, double z, double w, double h) {
        qDebug() << "onRecvChangedTileImagingArea" << x << y << z << w << h;
        d->ui.tileImgX->setValue(x);
        d->ui.tileImgY->setValue(y);
        d->ui.tileImgZ->setValue(z);
        d->ui.tileImgW->setValue(w);
        d->ui.tileImgH->setValue(h);
    }

    void VesselMapTestWindow::Impl::Connect() {
        connect(ui.btnClearAll, &QPushButton::clicked, self, &Self::onClearAll);

        connect(ui.comboVMMode, qOverload<int>(&QComboBox::currentIndexChanged), self, &Self::onSetViewMode);
        connect(ui.btnCreateVesselMap, &QPushButton::clicked, self, &Self::onCreateVesselMap);
        connect(ui.btnAddGroup, &QPushButton::clicked, self, &Self::onCreateNewGroup);
        connect(ui.btnDeleteGroup, &QPushButton::clicked, self, &Self::onDeleteGroup);
        connect(ui.btnAddWellToGroup, &QPushButton::clicked, self, &Self::onAddWellToGroup);
        connect(ui.btnRemoveWellFromGroup, &QPushButton::clicked, self, &Self::onRemoveWellFromGroup);
        connect(ui.btnChangeGroupName, &QPushButton::clicked, self, &Self::onChangeGroupName);
        connect(ui.btnChangeGroupColor, &QPushButton::clicked, self, &Self::onChangeGroupColor);

        connect(ui.lensPosX, &QDoubleSpinBox::textChanged, self, &Self::onChangeLensPos);
        connect(ui.lensPosY, &QDoubleSpinBox::textChanged, self, &Self::onChangeLensPos);
        connect(ui.lensPosZ, &QDoubleSpinBox::textChanged, self, &Self::onChangeLensPos);

        connect(ui.btnAddMark, &QPushButton::clicked, self, &Self::onAddMark);
        connect(ui.btnSetMark, &QPushButton::clicked, self, &Self::onSetMark);
        connect(ui.btnDelMark, &QPushButton::clicked, self, &Self::onDelMark);

        connect(ui.btnAddAcq, &QPushButton::clicked, self, &Self::onAddAcq);
        connect(ui.btnSetAcq, &QPushButton::clicked, self, &Self::onSetAcq);
        connect(ui.btnDelAcq, &QPushButton::clicked, self, &Self::onDelAcq);

        connect(ui.btnAddPreview, &QPushButton::clicked, self, &Self::onAddPreview);
        connect(ui.btnSetPreview, &QPushButton::pressed, self, &Self::onSetPreview);
        connect(ui.btnDelPreview, &QPushButton::pressed, self, &Self::onDelPreview);
        connect(ui.previewEditable, &QCheckBox::toggled, self, &Self::onSetPreviewEditable);

        connect(ui.chkShowLens, &QCheckBox::clicked, self, &Self::onShowLensItem);

        connect(ui.chkTileImaging, &QCheckBox::toggled, self, &Self::onTileImagingToggled);
        connect(ui.btnSetTileImg, &QPushButton::clicked, self, &Self::onSetTileImagingArea);
        connect(ui.btnAddTileImg, &QPushButton::clicked, self, &Self::onAddTileImagingAreaToAcqPoint);

        connect(widget, &VesselMapWidget::sigTileImagingAreaChanged, self, &Self::onRecvChangedTileImagingArea);

        connect(widget, &VesselMapWidget::sigChangedWellName, self, &Self::onRecvChangedWellName);
        connect(widget, &VesselMapWidget::sigChangedGroupName, self, &Self::onRecvChangedGroupName);
        connect(widget, &VesselMapWidget::sigChangedGroupColor, self, &Self::onRecvChangedGroupColor);

        connect(widget, qOverload<WellIndex, int32_t, int32_t>(&VesselMapWidget::sigChangeFocusedWell), self, &Self::onRecvChangeFocusedWell);

        connect(widget, &VesselMapWidget::sigPreviewGeometryChanged, self, &Self::onRecvChangePreviewLocation);

        connect(widget, &VesselMapWidget::sigSelectedWellIndices, self, &Self::onRecvSelectedWellIndices);
        connect(widget, &VesselMapWidget::sigSelectedGroupIndices, self, &Self::onRecvSelectedGroupIndices);
        connect(widget, &VesselMapWidget::sigDoubleClickedWellCanvasPosition, self, &Self::onRecvDoubleClickedWellCanvasPosition);
        connect(widget, &VesselMapWidget::sigMarkItemDoubleClikced, self, &Self::onRecvDoubleClickedWellCanvasMarkIndex);
        connect(widget, &VesselMapWidget::sigAcquisitionItemDoubleClicked, self, &Self::onRecvDoubleClickedWellCanvasAcqIndex);
        connect(widget, &VesselMapWidget::sigRequestTileConditionPopUp, self, &Self::onRecvRequestTileConditionPopUp);
    }

    void VesselMapTestWindow::Impl::InitVesselMapView() {
        widget = new VesselMapWidget(self);
        auto layout = new QGridLayout;
        layout->addWidget(widget);
        layout->setSpacing(0);
        layout->setMargin(0);
        ui.frame->setLayout(layout);
    }
}
