#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

#include "VesselMapExternalData.h"

namespace TC::TEST {
	class VesselMapTestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = VesselMapTestWindow;

		explicit VesselMapTestWindow(QWidget* parent = nullptr);
		~VesselMapTestWindow() override;


	private slots:
	    void onClearAll();
		void onSetViewMode(int32_t idx);

		// init
		void onCreateVesselMap();

		void onChangeLensPos();
        void onShowLensItem();

		// location
		void onAddMark();
		void onSetMark();
		void onDelMark();

		void onAddAcq();
		void onSetAcq();
		void onDelAcq();

		void onAddPreview();
		void onSetPreview();
		void onDelPreview();
		void onSetPreviewEditable(bool editable);
		
		// group func
		void onCreateNewGroup();
		void onDeleteGroup();
		void onAddWellToGroup();
		void onRemoveWellFromGroup();
		void onChangeGroupName();
		void onChangeGroupColor();

		// tile imaging
		void onTileImagingToggled(bool show);
		void onSetTileImagingArea();
		void onAddTileImagingAreaToAcqPoint();

		// from vessel map widget signal interface
		void onRecvChangedWellName(WellIndex idx, const QString& name);
		void onRecvChangedGroupName(GroupIndex idx, const QString& name);
		void onRecvChangedGroupColor(GroupIndex idx, const QColor& color);

		void onRecvChangeFocusedWell(WellIndex idx, int32_t row, int32_t col);
		void onRecvChangePreviewLocation(WellIndex well, double x, double y, double w, double h);

		void onRecvSelectedWellIndices(const QList<WellIndex>& wells);
		void onRecvSelectedGroupIndices(const QList<GroupIndex>& groups);
		void onRecvDoubleClickedWellCanvasPosition(double x, double y);
		void onRecvDoubleClickedWellCanvasMarkIndex(WellIndex wellIndex, MarkIndex markIndex);
		void onRecvDoubleClickedWellCanvasAcqIndex(WellIndex wellIndex, AcquisitionIndex acquisitionIndex);

		void onRecvRequestTileConditionPopUp(const WellIndex& well, const AcquisitionIndex& acq);

		void onRecvChangedTileImagingArea(double x, double y, double z, double w, double h);


	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
