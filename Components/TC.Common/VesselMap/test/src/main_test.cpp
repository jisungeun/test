#include "VesselMapTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TC::TEST::VesselMapTestWindow w;
    w.show();
    return a.exec();
}