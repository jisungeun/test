﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"

namespace TC {
    class ImagingArea {
    public:
        using Self = ImagingArea;
        using Pointer = std::shared_ptr<Self>;

        ImagingArea();
        ImagingArea(const ImagingArea& other);
        ~ImagingArea();

        auto operator=(const ImagingArea& other) -> ImagingArea&;

        auto SetShape(ImagingAreaShape shape) -> void;
        auto GetShape() const -> ImagingAreaShape;

        auto SetX(double x) -> void;
        auto GetX() const -> double;
        auto SetY(double y) -> void;
        auto GetY() const -> double;

        auto SetWidth(double width) -> void;
        auto GetWidth() const -> double;
        auto SetHeight(double height) -> void;
        auto GetHeight() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
