﻿#pragma once

#include <memory>

#include <QObject>
#include <QGraphicsItem>

#include "VesselMapCustomDataTypes.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasAcquisitionItem : public QObject, public QGraphicsItem {
        Q_OBJECT
        Q_INTERFACES(QGraphicsItem)
    public:
        using Self = WellCanvasAcquisitionItem;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasAcquisitionItem(const WellIndex& wellIndex,
                                  const AcquisitionIndex& acquisitionIndex, 
                                  const Position3D& pos3D,
                                  QGraphicsItem* parent = nullptr);
        ~WellCanvasAcquisitionItem() override;

        auto GetAcquisitionIndex() const -> AcquisitionIndex;
        auto GetWellIndex() const -> WellIndex;

        auto SetPosition3D(double x, double y, double z) -> void;
        auto GetPositionX() const -> double;
        auto GetPositionY() const -> double;
        auto GetPositionZ() const -> double;

        auto CreateContextMenu() -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto contextMenuEvent(QGraphicsSceneContextMenuEvent* event) -> void override;

        auto GetPointColor() const -> QColor;
        auto GetSelectedColor() const -> QColor;
        auto UpdatePointSize(const double& lod) -> void;
        auto GetPointSize() const -> double;

    signals:
        void sigAcquisitionItemDoubleClicked(const WellIndex&, const AcquisitionIndex&, double x, double y, double z);
        void sigRequestImportAcquisitionToMark(const WellIndex&, const AcquisitionIndex&);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class WellCanvasAcquisitionTileItem : public WellCanvasAcquisitionItem {
        Q_OBJECT
    public:
        using Self = WellCanvasAcquisitionTileItem;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvasAcquisitionTileItem(const WellIndex& wellIndex,
                                               const AcquisitionIndex& acquisitionIndex,
                                               const Position3D& pos3D,
                                               const Size2D& tileSize,
                                               QGraphicsItem* parent = nullptr);
        ~WellCanvasAcquisitionTileItem() override;

        auto SetTileSize(const double& w, const double& h) -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
