﻿#pragma once

#include "VesselMapExternalData.h"

namespace TC {
    struct Position2D {
        double x{};
        double y{};

        auto operator==(const Position2D& rhs) const -> bool {
            if(std::abs(x-rhs.x)>=0.0001) return false;
            if(std::abs(y-rhs.y)>=0.0001) return false;
            return true;
        }
        auto operator!=(const Position2D& rhs) const -> bool {
            return !(*this == rhs);
        }
    };

    struct Position3D {
        double x{};
        double y{};
        double z{};

        auto operator==(const Position3D& rhs) const -> bool {
            if(std::abs(x-rhs.x)>=0.0001) return false;
            if(std::abs(y-rhs.y)>=0.0001) return false;
            if(std::abs(z-rhs.z)>=0.0001) return false;
            return true;
        }
        auto operator!=(const Position3D& rhs) const -> bool {
            return !(*this == rhs);
        }
    };

    struct Size2D {
        double w{};
        double h{};

        auto operator==(const Size2D& rhs) const -> bool {
            if(std::abs(w-rhs.w)>=0.0001) return false;
            if(std::abs(h-rhs.h)>=0.0001) return false;
            return true;
        }
        auto operator!=(const Size2D& rhs) const -> bool {
            return !(*this == rhs);
        }
    };

    struct WellInformation {
        WellIndex index{kInvalid};
        int32_t row{kInvalid};
        int32_t column{kInvalid};
    };
}
