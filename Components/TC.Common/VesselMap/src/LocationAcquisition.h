﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    class LocationAcquisition {
    public:
        using Self = LocationAcquisition;
        using Pointer = std::shared_ptr<Self>;

        LocationAcquisition();
        LocationAcquisition(const LocationAcquisition& other);
        ~LocationAcquisition();

        auto operator=(const LocationAcquisition& other) -> LocationAcquisition&;

        auto SetIndex(AcquisitionIndex index) -> void;
        auto GetIndex() const -> AcquisitionIndex;

        auto SetType(AcquisitionType type) -> void;
        auto GetType() const -> AcquisitionType;

        auto SetPosition(double x, double y, double z) -> void;
        auto GetPosition() const -> Position3D;
        auto GetPosX() const -> double;
        auto GetPosY() const -> double;
        auto GetPosZ() const -> double;

        auto SetSize(double w, double h) -> void;
        auto GetSize() const -> Size2D;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
