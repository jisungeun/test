﻿#include <QApplication>
#include <QEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <qmath.h>
#include <QDebug>

#include "CanvasZoomSupport.h"

namespace TC {
    struct CanvasZoomSupport::Impl {
        QGraphicsView* view{nullptr};
        Qt::KeyboardModifiers modifiers{Qt::ControlModifier};
        double baseFactor{1.0015};
        QPointF targetScenePos;
        QPointF targetViewportPos;
        double zoomInFactor{1.19706};
        double zoomOutFactor{0.835383};
    };

    CanvasZoomSupport::CanvasZoomSupport(QGraphicsView* view) : d{new Impl} {
        d->view = view;
        d->view->viewport()->installEventFilter(this);
        d->view->setMouseTracking(true);
    }

    CanvasZoomSupport::~CanvasZoomSupport() {
    }

    auto CanvasZoomSupport::GentleZoom(double factor, bool usingBtn) -> void {
        d->view->scale(factor, factor);
        if (usingBtn) {
            auto scenePos = d->view->mapToScene(d->view->size().width() / 2, d->view->size().height() / 2);
            d->view->centerOn(scenePos);
        }
        else {
            d->view->centerOn(d->targetScenePos);
        }
    }

    auto CanvasZoomSupport::GetZoomInFactor() const -> double {
        return d->zoomInFactor;
    }

    auto CanvasZoomSupport::GetZoomOutFactor() const -> double {
        return d->zoomOutFactor;
    }

    auto CanvasZoomSupport::SetZoomByMouseWheel(double factor) -> void {
        d->view->scale(factor, factor);
        d->view->centerOn(d->targetScenePos);
    }

    auto CanvasZoomSupport::SetModifiers(Qt::KeyboardModifiers modifiers) -> void {
        d->modifiers = modifiers;
    }

    auto CanvasZoomSupport::SetBaseFactor(double value) -> void {
        d->baseFactor = value;
    }

    auto CanvasZoomSupport::eventFilter(QObject* watched, QEvent* event) -> bool {
        Q_UNUSED(watched)

        if (QApplication::keyboardModifiers() == d->modifiers) {
            if (event->type() == QEvent::MouseMove) {
                auto mouseEvent = static_cast<QMouseEvent*>(event);
                QPointF delta = d->targetViewportPos - mouseEvent->pos();
                d->targetScenePos = d->view->mapToScene(mouseEvent->pos());
            }
            else if (event->type() == QEvent::Wheel) {
                auto wheelEvent = static_cast<QWheelEvent*>(event);
                double angle = wheelEvent->angleDelta().y();
                double factor = qPow(d->baseFactor, angle);
                if (factor > 1) d->zoomInFactor = factor;
                else d->zoomOutFactor = factor;
                SetZoomByMouseWheel(factor);
                emit sigZoomed();
                return true;
            }
            else if (event->type() == QEvent::MouseButtonDblClick) {
                auto mouseEvent = static_cast<QMouseEvent*>(event);
                if (mouseEvent->button() == Qt::RightButton) {
                    d->view->fitInView(d->view->scene()->sceneRect(), Qt::KeepAspectRatio);
                    emit sigZoomed();
                }
            }
        }

        return false;
    }
}
