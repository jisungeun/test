﻿#pragma once

#include <QObject>
#include <QGraphicsView>

#include "GraphicsItemDefine.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasMenuSupport : public QObject {
        Q_OBJECT
	public:
        using Self = WellCanvasMenuSupport;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasMenuSupport(QGraphicsView* view, QObject* parent = nullptr);
        ~WellCanvasMenuSupport() override;

        auto AddAction(const WellCanvasMenu& actionIndex, 
                       const QString& title) -> void;

        auto AddSeperator() -> void;

        auto ExecuteMenu(const QPoint& pos) -> WellCanvasMenu;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
