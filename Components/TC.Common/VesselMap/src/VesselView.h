﻿#pragma once

#include <QWidget>

#include "VesselMapExternalData.h"
#include "VesselMapDataRepo.h"

namespace TC {
    class VesselView : public QWidget {
        Q_OBJECT
    public:
        using Self = VesselView;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselView(QWidget* parent = nullptr);
        ~VesselView() override;

        auto SetViewMode(ViewMode mode) -> void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;

        auto ClearAll() -> void;
        auto ClearScene() -> void;
        auto UpdateSceneRect(VesselIndex vesselIndex = kInvalid) -> void;
        auto DrawVesselItem() -> void;
        auto DrawWellItem() -> void;
        auto DrawRowColumnTextItem(VesselIndex vesselIndex) -> void;

        auto SetEditMode(EditMode mode) -> void;
        auto SetLensPosition(double x, double y) -> void;
        auto SetSelectedWell(WellIndex wellIndex) -> void;

        auto GetSelectedWellIndices() const -> QList<WellIndex>&;
        auto GetSelectedGroupIndices() const -> QList<GroupIndex>&;
        auto GetFocusedWellIndex() const -> WellIndex;

        auto ShowImagingOrder(bool show) -> void;
        auto SetWellImagingOrder(const QList<WellIndex>& wells) -> void;

        auto UpdateWellGroupCreated(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto UpdateWellGroupDeleted(GroupIndex groupIndex) -> void;
        auto UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto UpdateWellGroupMemberRemoved(const QList<WellIndex>& wellIndices) -> void;
        auto UpdateWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void;
        auto UpdateWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void;

        auto UpdateWellNameChanged(WellIndex wellIndex, const QString& name) -> void;


    signals:
        // TODO connect with canvas
        void sigSelectedWellIndices(const QList<WellIndex>& wellIdices);
        void sigSelectedGroupIndices(const QList<GroupIndex>& groupIndices);
        void sigVesselCanvasDblClicked(const WellIndex& wellIndex, int32_t row, int32_t column);
        void sigChangeSelectWell(const WellIndex& wellIndex);

    public slots:
        void onChangeSelectedWellIndexOnGroupTable(const QList<WellIndex>& wellIndices);
        void onChangeSelectedWellIndexByButton(const QList<WellIndex>& wellIndices);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
