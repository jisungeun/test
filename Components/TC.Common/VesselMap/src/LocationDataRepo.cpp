﻿#include <QMap>

#include "LocationDataRepo.h"
#include "DefaultSettingHelper.h"

namespace TC {
    struct LocationDataRepo::Impl {
        QMap<WellIndex, QList<LocationMark::Pointer>> markLocations;
        QMap<WellIndex, QList<LocationAcquisition::Pointer>> acquisitionLocations;

        auto GetNewMarkIndex(WellIndex wellIndex) -> MarkIndex;
        auto IsExistAcquisitionData(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) -> bool;
    };

    auto LocationDataRepo::Impl::GetNewMarkIndex(WellIndex wellIndex) -> MarkIndex {
        // TODO 가장 큰 index 값 찾아서 +1 해주기
        auto comparison = [](const LocationMark::Pointer& l1, const LocationMark::Pointer& l2) {
            return l1->GetIndex() < l2->GetIndex();
        };

        auto locations = markLocations[wellIndex];
        if (locations.isEmpty()) return 0;

        auto maxIndexLoc = std::max_element(locations.begin(), locations.end(), comparison);
        return (*maxIndexLoc)->GetIndex() + 1;
    }

    auto LocationDataRepo::Impl::IsExistAcquisitionData(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) -> bool {
        for(const auto& a : acquisitionLocations[wellIndex]) {
            if(a->GetIndex() == acquisitionIndex)
                return true;
        }
        return false;
    }

    LocationDataRepo::LocationDataRepo() : d{std::make_unique<Impl>()} {
    }

    LocationDataRepo::~LocationDataRepo() {
    }

    auto LocationDataRepo::ClearAll() -> void {
        // Clear Data
        d->markLocations.clear();
        d->acquisitionLocations.clear();
    }

    auto LocationDataRepo::AddMarkLocation(WellIndex wellIndex, MarkType type, double x, double y, double z, double w, double h) const -> MarkIndex {
        const auto location = std::make_shared<LocationMark>();
        const auto markIndex = d->GetNewMarkIndex(wellIndex);
        location->SetIndex(markIndex);
        location->SetPosition(x, y, z);
        location->SetType(type);
        location->SetSize(w, h);
        d->markLocations[wellIndex].push_back(location);
        return markIndex;
    }

    auto LocationDataRepo::SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w, double h) -> void {
        bool isExist = false;
        for (auto& loc : d->markLocations[wellIndex]) {
            if (loc->GetIndex() == markIndex) {
                loc->SetPosition(x, y, z);
                loc->SetType(type);
                loc->SetSize(w, h);
                isExist = true;
                break;
            }
        }

        if (!isExist) {
            const auto newLoc = std::make_shared<LocationMark>();
            newLoc->SetIndex(markIndex);
            newLoc->SetPosition(x, y, z);
            newLoc->SetType(type);
            newLoc->SetSize(w, h);
            d->markLocations[wellIndex].push_back(newLoc);
        }
    }

    auto LocationDataRepo::DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        for (auto& loc : d->markLocations[wellIndex]) {
            if (loc->GetIndex() == markIndex) {
                d->markLocations[wellIndex].removeOne(loc);
                break;
            }
        }
    }

    auto LocationDataRepo::GetMarkLocationByMarkIndex(WellIndex wellIndex, MarkIndex markIndex) -> LocationMark::Pointer {
        for (auto& loc : d->markLocations[wellIndex]) {
            if (loc->GetIndex() == markIndex) {
                return loc;
            }
        }

        return nullptr;
    }

    auto LocationDataRepo::AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool {
        if(d->IsExistAcquisitionData(wellIndex, acquisitionIndex)) return false;

        const auto newLoc = std::make_shared<LocationAcquisition>();
        newLoc->SetIndex(acquisitionIndex);
        newLoc->SetPosition(x, y, z);
        newLoc->SetType(type);
        newLoc->SetSize(w, h);
        d->acquisitionLocations[wellIndex].push_back(newLoc);
        return true;
    }

    auto LocationDataRepo::SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void {
        for (auto& loc : d->acquisitionLocations[wellIndex]) {
            if (loc->GetIndex() == acquisitionIndex) {
                loc->SetPosition(x, y, z);
                loc->SetType(type);
                loc->SetSize(w, h);
                break;
            }
        }
    }

    auto LocationDataRepo::DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        for (auto& loc : d->acquisitionLocations[wellIndex]) {
            if (loc->GetIndex() == acquisitionIndex) {
                d->acquisitionLocations[wellIndex].removeOne(loc);
                break;
            }
        }
    }

    auto LocationDataRepo::GetAcquisitionLocationByAcquisitionIndex(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> LocationAcquisition::Pointer {
        for (auto& loc : d->acquisitionLocations[wellIndex]) {
            if (loc->GetIndex() == acquisitionIndex) {
                return loc;
            }
        }

        return nullptr;
    }

    auto LocationDataRepo::GetWellIndexByMarkLIndex(MarkIndex markIndex) const -> WellIndex {
        auto it = d->markLocations.constBegin();
        while (it != d->markLocations.constEnd()) {
            auto list = it.value();
            for (const auto& mark : list) {
                if (mark->GetIndex() == markIndex) {
                    return it.key();
                }
            }
            ++it;
        }

        return kInvalid;
    }

    auto LocationDataRepo::GetWellIndexByAcquisitionIndex(AcquisitionIndex acquisitionIndex) const -> WellIndex {
        auto it = d->acquisitionLocations.constBegin();
        while (it != d->acquisitionLocations.constEnd()) {
            auto list = it.value();
            for (const auto& mark : list) {
                if (mark->GetIndex() == acquisitionIndex) {
                    return it.key();
                }
            }
            ++it;
        }

        return kInvalid;
    }

    auto LocationDataRepo::GetMarkIndicesByWellIndex(WellIndex wellIndex) -> QList<MarkIndex> {
        QList<MarkIndex> list;

        for (auto a : d->markLocations[wellIndex]) {
            list.push_back(a->GetIndex());
        }

        return list;
    }

    auto LocationDataRepo::GetAcquisitionIndicesByWellIndex(WellIndex wellIndex) -> QList<AcquisitionIndex> {
        QList<AcquisitionIndex> list;

        for (auto a : d->acquisitionLocations[wellIndex]) {
            if (a != nullptr) {
                if (a->GetIndex() != kInvalid) {
                    list.push_back(a->GetIndex());
                }
            }
        }

        return list;
    }
}
