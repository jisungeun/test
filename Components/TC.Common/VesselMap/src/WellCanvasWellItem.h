﻿#pragma once

#include <memory>

#include <QGraphicsItem>

#include "Roi.h"
#include "VesselMapExternalData.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    class WellCanvasWellItem : public QObject, public QGraphicsItem {
        Q_OBJECT
        Q_INTERFACES(QGraphicsItem)

    public:
        using Self = WellCanvasWellItem;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasWellItem(WellIndex index, QGraphicsItem* parent = nullptr);
        ~WellCanvasWellItem() override;

        auto SetWellShape(WellShape shape) -> void;
        auto SetSize(double w, double h) -> void;
        auto GetSize() const -> Size2D;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto ShowGridLine(bool show) -> void;
        auto SetImagingArea(double x, double y, double w, double h) -> void;
        auto SetImagingAreaShape(ImagingAreaShape shape) -> void;

        auto SetRois(const Roi::List& rois) -> void;
        auto ShowRoiIndex(bool show) -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;

    signals:
        void sigWellItemDoubleClicked(double x, double y);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
