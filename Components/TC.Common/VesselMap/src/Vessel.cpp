﻿#include "Vessel.h"

namespace TC {
    struct Vessel::Impl {
        VesselIndex index{kInvalid};
        VesselShape shape{VesselShape::Rectangle};

        double x{0.0};
        double y{0.0};

        double width{0.0};
        double height{0.0};

        int32_t row{kInvalid}; // vessel's row index
        int32_t column{kInvalid}; // vessel's col index

        struct {
            int32_t rows{kInvalid};
            int32_t cols{kInvalid};
        } wellCount;
    };

    Vessel::Vessel() : d{std::make_unique<Impl>()} {
    }

    Vessel::Vessel(const Vessel& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    Vessel::~Vessel() {
    }

    auto Vessel::operator=(const Vessel& other) -> Vessel& {
        *d = *other.d;
        return *this;
    }

    auto Vessel::SetIndex(VesselIndex index) -> void {
        d->index = index;
    }

    auto Vessel::GetIndex() const -> VesselIndex {
        return d->index;
    }

    auto Vessel::SetShape(VesselShape shape) -> void {
        d->shape = shape;
    }

    auto Vessel::GetShape() const -> VesselShape {
        return d->shape;
    }

    auto Vessel::SetX(double x) -> void {
        d->x = x;
    }

    auto Vessel::GetX() const -> double {
        return d->x;
    }

    auto Vessel::SetY(double y) -> void {
        d->y = y;
    }

    auto Vessel::GetY() const -> double {
        return d->y;
    }

    auto Vessel::SetWidth(double width) -> void {
        d->width = width;
    }

    auto Vessel::GetWidth() const -> double {
        return d->width;
    }

    auto Vessel::SetHeight(double height) -> void {
        d->height = height;
    }

    auto Vessel::GetHeight() const -> double {
        return d->height;
    }

    auto Vessel::SetRowIndex(int32_t row) -> void {
        d->row = row;
    }

    auto Vessel::GetRowIndex() const -> int32_t {
        return d->row;
    }

    auto Vessel::SetColumnIndex(int32_t column) -> void {
        d->column = column;
    }

    auto Vessel::GetColumnIndex() const -> int32_t {
        return d->column;
    }

    auto Vessel::SetWellCount(int32_t wellRows, int32_t wellCols) -> void {
        d->wellCount = {wellRows, wellCols};
    }

    auto Vessel::GetWellRows() const -> int32_t {
        return d->wellCount.rows;
    }

    auto Vessel::GetWellCols() const -> int32_t {
        return d->wellCount.cols;
    }
}
