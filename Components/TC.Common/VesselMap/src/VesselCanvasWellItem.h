﻿#pragma once

#include <QGraphicsItem>

#include "Roi.h"
#include "VesselMapExternalData.h"

namespace TC {
    class VesselCanvasWellItem : public QGraphicsItem {
    public:
        using Self = VesselCanvasWellItem;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselCanvasWellItem(QGraphicsItem* parent = nullptr);
        ~VesselCanvasWellItem() override;

        auto SetViewMode(ViewMode mode) -> void;

        auto SetIndex(WellIndex index) -> void;
        auto GetIndex() const -> WellIndex;

        auto SetShape(WellShape shape) -> void;
        auto GetShape() const -> WellShape;

        auto SetWidth(double w) -> void;
        auto GetWidth() const -> double;

        auto SetHeight(double h) -> void;
        auto GetHeight() const -> double;

        auto SetX(double x) -> void;
        auto GetX() const -> double;

        auto SetY(double y) -> void;
        auto GetY() const -> double;

        auto SetRowColumn(int32_t row, int32_t col) -> void;
        auto GetRow() const -> int32_t;
        auto GetColumn() const -> int32_t;

        auto SetName(const QString& name) -> void;
        auto GetName() -> QString;

        auto SetColor(const QColor& color) -> void;
        auto GetColor() -> QColor;

        auto SetGroupIndex(const GroupIndex& groupIndex) -> void;
        auto GetGroupIndex() const -> GroupIndex;

        auto LeaveGroup() -> void;

        auto SetLensFocus(bool onLens) -> void;
        auto SetShowOnWellCanvas(bool onWellCanvas) -> void;

        auto SetImagingArea(ImagingAreaShape shape, double x, double y, double w, double h) -> void;

        auto ShowImagingOrder(bool show) -> void;
        auto SetImagingOrder(int32_t order) -> void;

        auto SetRois(const Roi::List& rois) -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

        auto mousePressEvent(QGraphicsSceneMouseEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };


}
