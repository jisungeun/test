﻿#include <QGraphicsView>
#include <QSet>
#include <QMap>

#include "VesselCanvasControl.h"
#include "VesselCanvasVesselItem.h"
#include "VesselCanvasWellItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapDataRepo.h"
#include "VesselMapUtil.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct VesselCanvasControl::Impl {
        VesselMapDataRepo::Pointer repo{nullptr};
        VesselMapUtil::Pointer util{nullptr};

        ViewMode viewMode{ViewMode::SetupMode};

        QRectF sceneRect{};
        Position2D lensPos{};

        QGraphicsView::DragMode dragMode{QGraphicsView::RubberBandDrag};
        bool tracking{true};

        QList<WellIndex> selectedWellIndices;
        QList<GroupIndex> selectedGroupIndices;
        WellInformation wellInfo{};

        struct {
            double horizontalPadding;
            double verticalPadding;
        } textPaddingInfo{};

        auto SetTextPaddingInfo(VesselIndex vesselIndex) -> void;
        auto GetFontSize() -> double;
    };
    
    auto VesselCanvasControl::Impl::SetTextPaddingInfo(VesselIndex vesselIndex) -> void {
        auto vessel = util->GetVesselByVesselIndex(vesselIndex);

        if(vessel == nullptr) return;

        auto well = repo->GetWells().first();
        textPaddingInfo.verticalPadding = well->GetWidth()/2;
        textPaddingInfo.horizontalPadding = well->GetHeight()/2;
    }

    auto VesselCanvasControl::Impl::GetFontSize() -> double {
        if(viewMode == +ViewMode::SetupMode ||
           viewMode == +ViewMode::DataNaviMode ||
           viewMode == +ViewMode::PreviewMode) {
            return 20;
        }
        return 10;
    }

    VesselCanvasControl::VesselCanvasControl() : d{std::make_unique<Impl>()} {
        d->util = std::make_shared<VesselMapUtil>();
    }

    VesselCanvasControl::~VesselCanvasControl() {
    }

    auto VesselCanvasControl::ClearData() -> void {
        d->wellInfo = {kInvalid, kInvalid, kInvalid};
        d->selectedWellIndices.clear();
        d->selectedGroupIndices.clear();
    }

    auto VesselCanvasControl::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->repo = vesselMapDataRepo;
        d->util->SetVesselMapDataRepo(d->repo);
    }

    auto VesselCanvasControl::SetViewMode(ViewMode viewMode) -> void {
        d->viewMode = viewMode;
    }

    auto VesselCanvasControl::GetViewMode() const -> ViewMode {
        return d->viewMode;
    }

    auto VesselCanvasControl::SetSceneRect(VesselIndex vesselIndex) -> void {
        d->SetTextPaddingInfo(vesselIndex);

        auto w = d->repo->GetHolder()->GetWidth();
        auto h = d->repo->GetHolder()->GetHeight();
        auto x = 0.0;
        auto y = 0.0;
        const auto vessel = d->util->GetVesselByVesselIndex(vesselIndex);
        if (vessel != nullptr) {
            w = vessel->GetWidth() + d->textPaddingInfo.verticalPadding;
            h = vessel->GetHeight() + d->textPaddingInfo.horizontalPadding;
            x = vessel->GetX() - d->textPaddingInfo.verticalPadding/2;
            y = -(vessel->GetY()) - d->textPaddingInfo.horizontalPadding/2;
        }

        d->sceneRect = DefaultSettingHelper::GetBoundingRect(x, y, w, h);
    }

    auto VesselCanvasControl::GetSceneRect() const -> QRectF {
        return d->sceneRect;
    }

    auto VesselCanvasControl::GetVesselCount() const -> int32_t {
        return d->repo->GetVessels().size();
    }

    auto VesselCanvasControl::GetWellCount() const -> int32_t {
        return d->repo->GetWells().size();
    }

    auto VesselCanvasControl::SetLensPosition(double x, double y) -> void {
        d->lensPos = {x, y};
    }

    auto VesselCanvasControl::GetLensPosX() const -> double {
        return d->lensPos.x;
    }

    auto VesselCanvasControl::GetLensPosY() const -> double {
        return d->lensPos.y;
    }

    auto VesselCanvasControl::GenerateVesselItemConfig() const -> QList<VesselConfig> {
        QList<VesselConfig> list;

        for (auto vessel : d->repo->GetVessels()) {
            VesselConfig config;
            config.index = vessel->GetIndex();
            config.shape = vessel->GetShape();
            config.w = vessel->GetWidth();
            config.h = vessel->GetHeight();
            config.x = vessel->GetX();
            config.y = vessel->GetY();
            config.wellRows = vessel->GetWellRows();
            config.wellCols = vessel->GetWellCols();
            list.push_back(config);
        }

        return list;
    }

    auto VesselCanvasControl::GenerateWellItemConfig() const -> QList<WellConfig> {
        QList<WellConfig> list;

        for (auto well : d->repo->GetWells()) {
            WellConfig config;
            config.index = well->GetIndex();
            config.shape = well->GetShape();
            config.w = well->GetWidth();
            config.h = well->GetHeight();
            config.x = well->GetX();
            config.y = well->GetY();
            config.name = well->GetName();
            config.row = well->GetRow();
            config.col = well->GetColumn();
            config.rois = d->repo->GetRois(well->GetIndex());
            list.push_back(config);
        }
        return list;
    }

    auto VesselCanvasControl::GnerateRowColumnTextItemConfig(VesselIndex vesselIndex) const -> QMap<RowColumnConfig, RowColumn> {
        QMap<RowColumnConfig, RowColumn> rowColumnConfigs;

        const auto vessel = d->util->GetVesselByVesselIndex(vesselIndex);
        if(vessel == nullptr) return {};

        for(int32_t row = 0; row < vessel->GetWellRows(); ++row) {
            RowColumnConfig config;
            auto wellIndex = d->util->GetWellIndexByRowColumn(row, 0);
            if (kInvalid == wellIndex) continue;

            auto well = d->util->GetWellByWellIndex(wellIndex);
            if (nullptr == well) continue;

            config.w = well->GetWidth() / 2;
            config.h = well->GetHeight();

            double x = -(vessel->GetWidth()+config.w)/2;
            if(d->viewMode == +ViewMode::SetupMode ||
               d->viewMode == +ViewMode::DataNaviMode ||
               d->viewMode == +ViewMode::PreviewMode) {
                x = -vessel->GetWidth()/2;
            }

            config.x = x;
            config.y = well->GetY();
            config.txt = DefaultSettingHelper::GetWellRowName(row);
            config.alignment = Qt::AlignLeft | Qt::AlignVCenter;
            rowColumnConfigs[config] = RowColumn::Row;
        }

        for (int32_t col = 0; col < vessel->GetWellCols(); ++col) {
            RowColumnConfig config;
            const auto wellIndex = d->util->GetWellIndexByRowColumn(0, col);
            if (kInvalid == wellIndex) continue;

            const auto well = d->util->GetWellByWellIndex(wellIndex);
            if (nullptr == well) continue;

            config.w = well->GetWidth();
            config.h = well->GetHeight() / 2;

            double y = (vessel->GetHeight()+config.h)/2;
            if(d->viewMode == +ViewMode::SetupMode ||
               d->viewMode == +ViewMode::DataNaviMode ||
               d->viewMode == +ViewMode::PreviewMode) {
                y = vessel->GetHeight()/2;
            }

            config.x = well->GetX();
            config.y = y;
            config.txt = QString::number(col + 1);
            config.alignment = Qt::AlignTop | Qt::AlignHCenter;
            rowColumnConfigs[config] = RowColumn::Column;
        }

        return rowColumnConfigs;
    }

    auto VesselCanvasControl::SetCanvasMode(EditMode mode) -> void {
        switch (mode) {
            case EditMode::Editable: {
                d->dragMode = QGraphicsView::RubberBandDrag;
                d->tracking = true;
                break;
            }
            case EditMode::ReadOnly: {
                d->dragMode = QGraphicsView::NoDrag;
                d->tracking = false;
                break;
            }
        }
    }

    auto VesselCanvasControl::GetCanvasDragMode() const -> QGraphicsView::DragMode {
        return d->dragMode;
    }

    auto VesselCanvasControl::GetCanvasTracking() const -> bool {
        return d->tracking;
    }

    auto VesselCanvasControl::UpdateSelectedIndices(const QList<QGraphicsItem*>& selectedItems) -> void {
        QList<WellIndex> selectedWellIndices;
        QSet<GroupIndex> selectedGroupIndices;

        for (auto item : selectedItems) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto selectedWellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                selectedWellIndices.push_back(selectedWellItem->GetIndex());
                if (selectedWellItem->GetGroupIndex() != kInvalid) {
                    selectedGroupIndices.insert(selectedWellItem->GetGroupIndex());
                }
            }
        }

        std::sort(selectedWellIndices.begin(), selectedWellIndices.end());
        d->selectedWellIndices = selectedWellIndices;
        d->selectedGroupIndices = selectedGroupIndices.values();
    }

    auto VesselCanvasControl::GetSelectedWellIndices() const -> QList<WellIndex>& {
        return d->selectedWellIndices;
    }

    auto VesselCanvasControl::GetSelectedGroupIndices() const -> QList<GroupIndex>& {
        return d->selectedGroupIndices;
    }

    auto VesselCanvasControl::GetWellItemListByWellIndices(const QList<int>& wellIndices, const QList<QGraphicsItem*>& allItemsOnCanvas) -> QList<QGraphicsItem*> {
        QList<QGraphicsItem*> items;
        for (const auto item : allItemsOnCanvas) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                for (const auto wellIndex : wellIndices) {
                    if (wellIndex == wellItem->GetIndex()) {
                        items.push_back(wellItem);
                    }
                }
            }
        }
        return items;
    }

    auto VesselCanvasControl::GetWellItemListByGroupIndex(GroupIndex groupIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) -> QList<QGraphicsItem*> {
        QList<QGraphicsItem*> items;
        for (const auto item : allItemsOnCanvas) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                if (wellItem->GetGroupIndex() == groupIndex) {
                    items.push_back(wellItem);
                }
            }
        }
        return items;
    }

    auto VesselCanvasControl::GetWellItemByWellIndex(WellIndex wellIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) const -> QGraphicsItem* {
        for (const auto item : allItemsOnCanvas) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                if (wellItem->GetIndex() == wellIndex) {
                    return wellItem;
                }
            }
        }
        return nullptr;
    }

    auto VesselCanvasControl::UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<QGraphicsItem*>& targetWellItems) -> void {
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);
        QColor groupColor;
        if (group != nullptr) {
            groupColor = group->GetColor();

            for (const auto item : targetWellItems) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                wellItem->SetGroupIndex(groupIndex);
                const auto wellName = d->util->GetWellByWellIndex(wellItem->GetIndex())->GetName();
                wellItem->SetName(wellName);
                wellItem->SetColor(groupColor);
            }
        }
    }

    auto VesselCanvasControl::UpdateWellGroupDeleted(GroupIndex groupIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) -> void {
        for (auto item : allItemsOnCanvas) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                if (wellItem->GetGroupIndex() == groupIndex) {
                    wellItem->LeaveGroup();
                }
            }
        }
    }

    auto VesselCanvasControl::UpdateWellGroupMemberRemoved(const QList<QGraphicsItem*> targetWellItems) -> void {
        for (auto item : targetWellItems) {
            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
            wellItem->LeaveGroup();
        }
    }

    auto VesselCanvasControl::UpdateWellGroupColorChanged(const QList<QGraphicsItem*> targetWellItems, const QColor& color) -> void {
        for (auto item : targetWellItems) {
            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
            wellItem->SetColor(color);
        }
    }

    auto VesselCanvasControl::UpdateWellNameChanged(QGraphicsItem* item, const QString& name) -> void {
        const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
        wellItem->SetName(name);
    }

    auto VesselCanvasControl::SetFocusedWellInformation(QGraphicsItem* focusItem) -> void {
        if (focusItem == nullptr) return;

        if (focusItem->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
            const auto item = qgraphicsitem_cast<VesselCanvasWellItem*>(focusItem);
            d->wellInfo.index = item->GetIndex();
            d->wellInfo.row = item->GetRow();
            d->wellInfo.column = item->GetColumn();
        }
    }

    auto VesselCanvasControl::SetFocusedWellInformation(WellIndex wellIndex, int32_t row, int32_t column) -> void {
        d->wellInfo = {wellIndex, row, column};
    }

    auto VesselCanvasControl::GetFocusedWellIndex() const -> WellIndex {
        return d->wellInfo.index;
    }

    auto VesselCanvasControl::GetFocusedWellRow() const -> int32_t {
        return d->wellInfo.row;
    }

    auto VesselCanvasControl::GetFocusedWellColumn() const -> int32_t {
        return d->wellInfo.column;
    }

    auto VesselCanvasControl::GetWellByWellIndex(WellIndex wellIndex) const -> Well::Pointer {
        return d->util->GetWellByWellIndex(wellIndex);
    }

    auto VesselCanvasControl::GetImagingArea() const -> ImagingArea::Pointer {
        return d->repo->GetImagingArea();
    }
}
