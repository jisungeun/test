﻿#include <QGraphicsScene>
#include <QResizeEvent>
#include <QLabel>

#include "VesselCanvas.h"

#include "VesselCanvasControl.h"
#include "GraphicsItemDefine.h"
#include "VesselCanvasVesselItem.h"
#include "VesselCanvasWellItem.h"
#include "VesselCanvasTextItem.h"
#include "WellCanvasLensItem.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct VesselCanvas::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};

        VesselCanvasControl control;
        QGraphicsScene* scene{nullptr};
        WellCanvasLensItem* lensItem{nullptr};

        bool isWellItemHighlightOnWellCanvas{false};
        bool isVesselChangeable{false};

        auto InitView() -> void;
        auto InitScene() -> void;
        auto InitLensItem() -> void;

        auto ClearSelection() -> void;

        auto ClearScene() -> void;
        auto ChangeCanvasProperty() -> void;

        auto Connect() -> void;
        auto SetSceneBlockSignals(bool blocked) -> void;

        auto SetCurrentWellOnLens() -> void;
        auto FitInView() -> void;

        auto UpdateLensOnStatus() const -> void;
        auto UpdateWellItemDisplayText() const -> void;
        auto UpdateWellImagingOrder(const QList<WellIndex>& wells) -> void;
    };

    VesselCanvas::VesselCanvas(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>(this)} {
        d->InitView();
        d->InitScene();
        d->InitLensItem();
        d->Connect();

        setStyleSheet("border:none;");
    }

    VesselCanvas::~VesselCanvas() {
    }

    auto VesselCanvas::ClearAll() -> void {
        // TODO Clear All Data
        ClearScene();
        d->control.ClearData();
    }

    auto VesselCanvas::SetViewMode(ViewMode mode) -> void {
        d->control.SetViewMode(mode);
        // SetupMode, DataNaviMode, PerformMode, InfoMode, Timelapse, PreviewMode
        switch(mode) {
            case ViewMode::SetupMode:
                d->isWellItemHighlightOnWellCanvas = false;
                SetEditMode(EditMode::Editable);
                break;
            case ViewMode::PreviewMode:
                d->isWellItemHighlightOnWellCanvas = false;
                SetEditMode(EditMode::ReadOnly);
                break;
            case ViewMode::PerformMode:
                d->isWellItemHighlightOnWellCanvas = true;
                d->isVesselChangeable = true;
                SetEditMode(EditMode::ReadOnly);
                break;
            case ViewMode::TimelapseMode:
                d->isWellItemHighlightOnWellCanvas = true;
                d->isVesselChangeable = true;
                SetEditMode(EditMode::ReadOnly);
                break;
            case ViewMode::DataNaviMode:
                d->isWellItemHighlightOnWellCanvas = false;
                SetEditMode(EditMode::ReadOnly);
                break;
            case ViewMode::InfoMode:
                d->isWellItemHighlightOnWellCanvas = true;
                SetEditMode(EditMode::ReadOnly);
                break;
            case ViewMode::CopyDlgMode:
                d->isWellItemHighlightOnWellCanvas = true;
                SetEditMode(EditMode::Editable);
                break;
            default:
                break;
        }

        d->UpdateLensOnStatus();
        d->UpdateWellItemDisplayText();
    }

    auto VesselCanvas::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->control.SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto VesselCanvas::ClearScene() -> void {
        // signal block 안 하면 selectionChanged signal 계속 emit 됨
        d->SetSceneBlockSignals(true);
        d->ClearScene();
        d->SetSceneBlockSignals(false);
    }

    auto VesselCanvas::UpdateSceneRect(VesselIndex vesselIndex) -> void {
        d->control.SetSceneRect(vesselIndex);
        d->scene->setSceneRect(d->control.GetSceneRect());
        d->FitInView();
    }

    auto VesselCanvas::DrawVesselItem() -> void {
        for (const auto& config : d->control.GenerateVesselItemConfig()) {
            const auto item = new VesselCanvasVesselItem();
            item->SetIndex(config.index);
            item->SetShape(config.shape);
            item->SetSize(config.w, config.h);
            item->SetPosition(config.x, config.y);
            item->SetWellCount(config.wellRows, config.wellCols);
            d->scene->addItem(item);
        }
    }

    auto VesselCanvas::DrawWellItem() -> void {
        for (const auto& config : d->control.GenerateWellItemConfig()) {
            const auto item = new VesselCanvasWellItem();
            item->SetViewMode(d->control.GetViewMode());

            item->SetIndex(config.index);
            item->SetWidth(config.w);
            item->SetHeight(config.h);
            item->SetX(config.x);
            item->SetY(config.y);
            item->SetRowColumn(config.row, config.col);
            item->SetShape(config.shape);
            item->SetName(config.name);
            item->SetRois(config.rois);
            item->SetImagingArea(d->control.GetImagingArea()->GetShape(), d->control.GetImagingArea()->GetX(), d->control.GetImagingArea()->GetY(), d->control.GetImagingArea()->GetWidth(), d->control.GetImagingArea()->GetHeight());
            d->scene->addItem(item);
        }
    }

    auto VesselCanvas::DrawRowColumnTextItem(VesselIndex vesselIndex) -> void {
        const auto& configs = d->control.GnerateRowColumnTextItemConfig(vesselIndex);

        for (auto it = configs.cbegin(); it != configs.cend(); ++it) {
            const auto item = new VesselCanvasTextItem();
            const auto& [w, h, x, y, txt, alignment] = it.key();
            item->SetViewMode(d->control.GetViewMode());
            item->SetPosition(x, y);
            item->SetSize(w, h);
            item->SetLabel(txt);
            item->SetTextAlignment(alignment);
            d->scene->addItem(item);
        }
    }

    auto VesselCanvas::SetEditMode(EditMode mode) -> void {
        d->control.SetCanvasMode(mode);
        d->ChangeCanvasProperty();
    }

    auto VesselCanvas::SetLensPosition(double x, double y) -> void {
        d->control.SetLensPosition(x, y);
        d->lensItem->SetPosition(d->control.GetLensPosX(), d->control.GetLensPosY());
        d->SetCurrentWellOnLens();
    }

    auto VesselCanvas::SetSelectedWell(WellIndex wellIndex) -> void {
        // TODO mouseDoubleClickEvent 중복 코드 많음. 개선 필요
        VesselCanvasWellItem* item = nullptr;
        bool found = false;

        for (auto i : d->scene->items()) {
            if (i->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                item = qgraphicsitem_cast<VesselCanvasWellItem*>(i);
                if (item->GetIndex() == wellIndex) {
                    found = true;
                    break;
                }
            }
        }

        if (item == nullptr || !found) return;

        const auto row = item->GetRow();
        const auto col = item->GetColumn();

        d->control.SetFocusedWellInformation(wellIndex, row, col);
        emit sigChangeSelectWell(wellIndex);
        d->UpdateLensOnStatus();
    }

    auto VesselCanvas::GetSelectedWellIndices() const -> QList<WellIndex>& {
        return d->control.GetSelectedWellIndices();
    }

    auto VesselCanvas::GetSelectedGroupIndices() const -> QList<GroupIndex>& {
        return d->control.GetSelectedGroupIndices();
    }

    auto VesselCanvas::GetFocusedWellIndex() const -> WellIndex {
        return d->control.GetFocusedWellIndex();
    }

    auto VesselCanvas::ShowImagingOrder(bool show) -> void {
         for(const auto& item : d->scene->items()) {
            if (item->type() != static_cast<int32_t>(GraphicsItemType::WellItem)) continue;

            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
            wellItem->ShowImagingOrder(show);
        }
    }

    auto VesselCanvas::SetWellItemOrderNumber(QList<WellIndex> wells) -> void {
        d->UpdateWellImagingOrder(wells);
    }

    auto VesselCanvas::UpdateWellGroupCreated(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void {
        const auto items = d->control.GetWellItemListByWellIndices(wellIndices, d->scene->items());
        d->control.UpdateWellGroupMemberAdded(groupIndex, items);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellGroupDeleted(GroupIndex groupIndex) -> void {
        d->control.UpdateWellGroupDeleted(groupIndex, d->scene->items());
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void {
        const auto items = d->control.GetWellItemListByWellIndices(wellIndices, d->scene->items());
        d->control.UpdateWellGroupMemberAdded(groupIndex, items);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellGroupMemberRemoved(const QList<WellIndex>& wellIdices) -> void {
        const auto items = d->control.GetWellItemListByWellIndices(wellIdices, d->scene->items());
        d->control.UpdateWellGroupMemberRemoved(items);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void {
        const auto items = d->control.GetWellItemListByGroupIndex(groupIndex, d->scene->items());
        d->control.UpdateWellGroupColorChanged(items, color);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void {
        const auto items = d->control.GetWellItemListByGroupIndex(movingGroupIndex, d->scene->items());
        d->control.UpdateWellGroupMemberAdded(targetGroupIndex, items);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateWellNameChanged(WellIndex wellIndex, const QString& name) -> void {
        const auto item = d->control.GetWellItemByWellIndex(wellIndex, d->scene->items());
        d->control.UpdateWellNameChanged(item, name);
        d->ClearSelection();
    }

    auto VesselCanvas::UpdateSelectedWells(const QList<WellIndex>& wellIndices) -> void {
        const auto items = d->control.GetWellItemListByWellIndices(wellIndices, d->scene->items());
        d->scene->clearSelection();
        for(const auto& item : items) {
            item->setSelected(true);
        }
    }

    auto VesselCanvas::resizeEvent(QResizeEvent* event) -> void {
        d->FitInView();
        QGraphicsView::resizeEvent(event);
    }

    auto VesselCanvas::mouseDoubleClickEvent(QMouseEvent* event) -> void {
        if(d->isVesselChangeable) {
            if (event->button() == Qt::LeftButton) {
                const auto x = event->pos().x();
                const auto y = event->pos().y();
                const auto item = itemAt(x, y);

                if (item == nullptr) return;

                if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                    const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
                    const auto wellIndex = wellItem->GetIndex();
                    const auto row = wellItem->GetRow();
                    const auto col = wellItem->GetColumn();

                    d->control.SetFocusedWellInformation(wellIndex, row, col);
                    // TODO SetupMode에서 필요한 시그널인가?
                    emit sigVesselCanvasDblClicked(wellIndex, row, col);
                }

                d->UpdateLensOnStatus();
            }
        }

        QGraphicsView::mouseDoubleClickEvent(event);
    }

    auto VesselCanvas::mousePressEvent(QMouseEvent* event) -> void {
        QGraphicsView::mousePressEvent(event);

        if(itemAt(event->pos())) {
            const auto item = itemAt(event->pos());
            if(item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                item->setSelected(true);
            }
        }
    }

    auto VesselCanvas::onSceneSelectionChanged() -> void {
        const auto selectedItems = d->scene->selectedItems();
        d->control.UpdateSelectedIndices(selectedItems);
        emit sigSelectedWellIndices(d->control.GetSelectedWellIndices());
        emit sigSelectedGroupIndices(d->control.GetSelectedGroupIndices());
    }

    auto VesselCanvas::Impl::InitView() -> void {
        self->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
        self->setOptimizationFlags(DontSavePainterState);
        self->setViewportUpdateMode(SmartViewportUpdate);
        self->setTransformationAnchor(AnchorUnderMouse);
        self->setMouseTracking(control.GetCanvasTracking());
        self->setDragMode(control.GetCanvasDragMode());
    }

    auto VesselCanvas::Impl::InitScene() -> void {
        scene = new QGraphicsScene(self);
        scene->setSceneRect(-64, -64, 128, 128);
        // TODO apply new background color
        scene->setBackgroundBrush(SceneBackgroundColor);
        self->setScene(scene);
    }

    auto VesselCanvas::Impl::InitLensItem() -> void {
        const Position2D lensPos{0.0, 0.0};
        lensItem = new WellCanvasLensItem(lensPos);

        if (scene != nullptr && lensItem != nullptr) {
            scene->addItem(lensItem);
            lensItem->setVisible(false); // always false on VesselCanvas
        }
    }

    auto VesselCanvas::Impl::ClearSelection() -> void {
        scene->clearSelection();
    }

    auto VesselCanvas::Impl::ClearScene() -> void {
        for (auto a : scene->items()) {
            if (a->type() != static_cast<int32_t>(GraphicsItemType::LensItem)) {
                scene->removeItem(a);
            }
        }
        scene->clearFocus();
        scene->clearSelection();
    }

    auto VesselCanvas::Impl::ChangeCanvasProperty() -> void {
        self->setDragMode(control.GetCanvasDragMode());
        self->setMouseTracking(control.GetCanvasTracking());
    }

    auto VesselCanvas::Impl::Connect() -> void {
        connect(scene, &QGraphicsScene::selectionChanged, self, &Self::onSceneSelectionChanged);
    }

    auto VesselCanvas::Impl::SetSceneBlockSignals(bool blocked) -> void {
        scene->blockSignals(blocked);
    }

    auto VesselCanvas::Impl::SetCurrentWellOnLens() -> void {
        for (const auto& a : scene->items()) {
            if (a->type() != static_cast<int32_t>(GraphicsItemType::WellItem)) continue;

            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(a);
            const bool cib = lensItem->collidesWithItem(wellItem, Qt::ContainsItemBoundingRect);

            if (cib) {
                wellItem->SetLensFocus(true);
            }
            else {
                wellItem->SetLensFocus(false);
            }
        }
    }

    auto VesselCanvas::Impl::FitInView() -> void {
        QRectF rect = scene->sceneRect();
        self->fitInView(rect, Qt::KeepAspectRatio);
    }

    auto VesselCanvas::Impl::UpdateLensOnStatus() const -> void {
        foreach(auto a, scene->items()) {
            if (a->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(a);
                if (wellItem->GetIndex() == control.GetFocusedWellIndex() && isWellItemHighlightOnWellCanvas) {
                    wellItem->SetShowOnWellCanvas(true);
                }
                else {
                    wellItem->SetShowOnWellCanvas(false);
                }
            }
        }
    }

    auto VesselCanvas::Impl::UpdateWellItemDisplayText() const -> void {
        for (const auto& item : scene->items()) {
            if (item->type() != static_cast<int32_t>(GraphicsItemType::WellItem)) continue;

            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
            wellItem->SetViewMode(control.GetViewMode());
        }
    }

    auto VesselCanvas::Impl::UpdateWellImagingOrder(const QList<WellIndex>& wells) -> void {
        for(const auto& item : scene->items()) {
            if (item->type() != static_cast<int32_t>(GraphicsItemType::WellItem)) continue;

            const auto wellItem = qgraphicsitem_cast<VesselCanvasWellItem*>(item);
            if(wells.contains(wellItem->GetIndex())) {
                auto order = wells.indexOf(wellItem->GetIndex());
                order += 1;
                wellItem->SetImagingOrder(order);
            }
            else {
                wellItem->SetImagingOrder(-1);
            }
        }
    }
}
