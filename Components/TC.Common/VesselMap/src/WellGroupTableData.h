﻿#pragma once

#include <memory>

#include <QString>
#include <QColor>

#include "VesselMapExternalData.h"
#include "WellGroupTableDefine.h"

namespace TC {
    class WellGroupTableData {
    public:
        using Self = WellGroupTableData;
        using Pointer = std::shared_ptr<Self>;

        WellGroupTableData();
        WellGroupTableData(const WellGroupTableData& other);
        ~WellGroupTableData();

        auto operator=(const WellGroupTableData& other) -> WellGroupTableData&;

        auto SetGroupIndex(GroupIndex index) -> void;
        auto GetGroupIndex() const -> GroupIndex;

        auto SetGroupName(const QString& name) -> void;
        auto GetGroupName() const -> QString;

        auto SetGroupColor(const QColor& color) -> void;
        auto GetGroupColor() const -> QColor;

        auto SetWellIndex(WellIndex index) -> void;
        auto GetWellIndex() const -> WellIndex;

        auto SetWellName(const QString& name) -> void;
        auto GetWellName() const -> QString;

        auto SetWellPosition(const QString& position) -> void;
        auto GetWellPosition() const -> QString;

        auto SetWellImgPointCount(int32_t count) -> void;
        auto GetWellImgPointCount() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
