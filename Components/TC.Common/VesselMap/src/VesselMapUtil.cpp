﻿#include "VesselMapUtil.h"
#include "VesselMapDataRepo.h"

namespace TC {
    struct VesselMapUtil::Impl {
        VesselMapDataRepo::Pointer repo{nullptr};
    };

    VesselMapUtil::VesselMapUtil() : d{std::make_unique<Impl>()} {
    }

    VesselMapUtil::~VesselMapUtil() {
    }

    auto VesselMapUtil::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->repo = vesselMapDataRepo;
    }

    auto VesselMapUtil::GetAllVesselIndices() -> QList<VesselIndex> {
        QList<VesselIndex> vesselList;

        for (const auto vessel : d->repo->GetVessels()) {
            vesselList.push_back(vessel->GetIndex());
        }

        return vesselList;
    }

    auto VesselMapUtil::GetGroupByGroupIndex(GroupIndex groupIndex) const -> WellGroup::Pointer {
        WellGroup::Pointer group = nullptr;

        for (const auto& a : d->repo->GetWellGroups()) {
            if (a->GetIndex() == groupIndex) {
                group = a;
                break;
            }
        }

        return group;
    }

    auto VesselMapUtil::GetGroupByGroupName(const QString& groupName) const -> WellGroup::Pointer {
        WellGroup::Pointer group = nullptr;

        for (const auto& a : d->repo->GetWellGroups()) {
            if (a->GetName() == groupName) {
                group = a;
                break;
            }
        }

        return group;
    }

    auto VesselMapUtil::GetGroupByWellIndex(WellIndex wellIndex) const -> WellGroup::Pointer {
        for (const auto& group : d->repo->GetWellGroups()) {
            for (const auto& idx : group->GetWells()) {
                if (idx == wellIndex) {
                    return group;
                }
            }
        }
        return nullptr;
    }

    auto VesselMapUtil::GetWellByWellIndex(WellIndex wellIndex) const -> Well::Pointer {
        Well::Pointer well = nullptr;

        for (const auto& a : d->repo->GetWells()) {
            if (a->GetIndex() == wellIndex) {
                well = a;
                break;
            }
        }

        return well;
    }

    auto VesselMapUtil::IsWellAlreadyHasGroup(WellIndex wellIndex) -> bool {
        if (d->repo->GetWellGroups().isEmpty()) { return false; }

        for (const auto& group : d->repo->GetWellGroups()) {
            for (const auto& idx : group->GetWells()) {
                if (idx == wellIndex) {
                    return true;
                }
            }
        }
        return false;
    }

    auto VesselMapUtil::GetWellIndexByRowColumn(int32_t row, int32_t column) const -> WellIndex {
        for (const auto& w : d->repo->GetWells()) {
            if (w->GetRow() == row && w->GetColumn() == column) {
                return w->GetIndex();
            }
        }
        return kInvalid;
    }

    auto VesselMapUtil::GetWellRowByWellIndex(WellIndex wellIndex) const -> int32_t {
        for (const auto& w : d->repo->GetWells()) {
            if (w->GetIndex() == wellIndex) {
                return w->GetRow();
            }
        }
        return kInvalid;
    }

    auto VesselMapUtil::GetWellColumnByWellIndex(WellIndex wellIndex) const -> int32_t {
        for (const auto& w : d->repo->GetWells()) {
            if (w->GetIndex() == wellIndex) {
                return w->GetColumn();
            }
        }
        return kInvalid;
    }

    auto VesselMapUtil::GetVesselByVesselIndex(VesselIndex vesselIndex) -> Vessel::Pointer {
        for (const auto& v : d->repo->GetVessels()) {
            if (v->GetIndex() == vesselIndex) {
                return v;
            }
        }
        return nullptr;
    }

    auto VesselMapUtil::ConvertLensPosToWellPos(WellIndex wellIndex, Position2D lensPos) -> Position2D {
        const auto& well = GetWellByWellIndex(wellIndex);
        auto x = 0.0;
        auto y = 0.0;

        if (well != nullptr) {
            x = lensPos.x - well->GetX();
            y = lensPos.y - well->GetY();
        }
        return Position2D{x, y};
    }
}
