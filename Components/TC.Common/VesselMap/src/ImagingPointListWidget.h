﻿#pragma once

#include <memory>
#include <QWidget>

#include "LocationDataRepo.h"
#include "VesselMapDataRepo.h"
#include "VesselMapExternalData.h"

namespace TC {
    class ImagingPointListWidget : public QWidget{
        Q_OBJECT
    public:
        using Self = ImagingPointListWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit ImagingPointListWidget(QWidget* parent = nullptr);
        ~ImagingPointListWidget() override;

        auto ClearAll() -> void;
        auto InitColumnHeader() -> void;

        auto SetViewMode(ViewMode mode)->void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto SetCurrentWellIndex(WellIndex wellIndex) -> void;
        auto AddAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto DeleteAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetDeleteEnabled(bool enabled) -> void;

        auto SetShowAllPoints(bool showAll) -> void;

    signals:
        void sigShowTileCondition(const WellIndex&, const AcquisitionIndex&);
        void sigCurrentSelectedItems(const QList<QPair<WellIndex, AcquisitionIndex>>&);
        void sigAcqItemDoubleClicked(const WellIndex&, const AcquisitionIndex&, double x, double y, double z);
        void sigDeleteAcquisitionItem(const WellIndex&, const AcquisitionIndex&);
        void sigUpdateTableSectionByWellCanvas(const QList<QPair<WellIndex, AcquisitionIndex>>& selected);

        void sigImagingPointSelected(const QString& wellPosition, const QString& pointID);
        void sigShowAllButtonToggled(bool showAll);
        void sigCopyAcquisitionItems(const WellIndex& sourceWellIndex, const QList<AcquisitionIndex>& copyPointIndices);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
