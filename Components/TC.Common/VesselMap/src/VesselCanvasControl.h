﻿#pragma once

#include <memory>

#include "Holder.h"
#include "Vessel.h"
#include "Well.h"
#include "ImagingArea.h"
#include "VesselMapDataRepo.h"

class QGraphicsView;
class QGraphicsItem;
class QRectF;

namespace TC {
    class VesselCanvasControl {
    public:
        using Self = VesselCanvasControl;
        using Pointer = std::shared_ptr<Self>;

        struct VesselConfig {
            VesselIndex index;
            VesselShape::_enumerated shape;
            double w;
            double h;
            double x;
            double y;
            int32_t wellRows;
            int32_t wellCols;
        };

        struct WellConfig {
            WellIndex index;
            WellShape::_enumerated shape;
            double w;
            double h;
            double x;
            double y;
            int32_t row;
            int32_t col;
            QString name;
            Roi::List rois{};
        };

        struct RowColumnConfig {
            double w;
            double h;
            double x;
            double y;
            QString txt;
            Qt::Alignment alignment;

            auto operator<(const RowColumnConfig& other) const -> bool {
                if (x < other.x) return true;
                if (y < other.y) return true;
                return false;
            }
        };

        struct RoiConfig {
            RoiIndex index;
            RoiShape::_enumerated shape;
            double w;
            double h;
            double x;
            double y;
            QString name;
        };

        enum class RowColumn { Row, Column };

        VesselCanvasControl();
        ~VesselCanvasControl();

        auto ClearData() -> void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;

        auto SetViewMode(ViewMode viewMode) -> void;
        auto GetViewMode() const -> ViewMode;

        auto SetSceneRect(VesselIndex vesselIndex) -> void;
        auto GetSceneRect() const -> QRectF;

        auto GetVesselCount() const -> int32_t;
        auto GetWellCount() const -> int32_t;

        auto SetLensPosition(double x, double y) -> void;
        auto GetLensPosX() const -> double;
        auto GetLensPosY() const -> double;

        auto GenerateVesselItemConfig() const -> QList<VesselConfig>;
        auto GenerateWellItemConfig() const -> QList<WellConfig>;
        auto GnerateRowColumnTextItemConfig(VesselIndex vesselIndex) const -> QMap<RowColumnConfig, RowColumn>;
        auto GenerateRoiItemConfig() const -> QList<RoiConfig>;

        auto SetCanvasMode(EditMode mode) -> void;
        auto GetCanvasDragMode() const -> QGraphicsView::DragMode;
        auto GetCanvasTracking() const -> bool;

        auto UpdateSelectedIndices(const QList<QGraphicsItem*>& selectedItems) -> void;
        auto GetSelectedWellIndices() const -> QList<WellIndex>&;
        auto GetSelectedGroupIndices() const -> QList<GroupIndex>&;

        auto GetWellItemListByWellIndices(const QList<WellIndex>& wellIndices, const QList<QGraphicsItem*>& allItemsOnCanvas) -> QList<QGraphicsItem*>;
        auto GetWellItemListByGroupIndex(GroupIndex groupIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) -> QList<QGraphicsItem*>;
        auto GetWellItemByWellIndex(WellIndex wellIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) const -> QGraphicsItem*;
        auto UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<QGraphicsItem*>& targetWellItems) -> void;
        auto UpdateWellGroupDeleted(GroupIndex groupIndex, const QList<QGraphicsItem*>& allItemsOnCanvas) -> void;
        auto UpdateWellGroupMemberRemoved(QList<QGraphicsItem*> targetWellItems) -> void;
        auto UpdateWellGroupColorChanged(QList<QGraphicsItem*> targetWellItems, const QColor& color) -> void;
        auto UpdateWellNameChanged(QGraphicsItem* item, const QString& name) -> void;

        auto SetFocusedWellInformation(QGraphicsItem* focusItem) -> void;
        auto SetFocusedWellInformation(WellIndex wellIndex, int32_t row, int32_t column) -> void;
        auto GetFocusedWellIndex() const -> WellIndex;
        auto GetFocusedWellRow() const -> int32_t;
        auto GetFocusedWellColumn() const -> int32_t;

        auto GetWellByWellIndex(WellIndex wellIndex) const -> Well::Pointer;

        auto GetImagingArea() const -> ImagingArea::Pointer;

        auto SetImagingOrderMode(const ImagingOrder& imagingOrder) -> void;
        auto GetImagingOrderMode() const -> ImagingOrder;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };


}
