﻿#include <QResizeEvent>

#include <UIUtility.h>

#include "WellView.h"
#include "WellCanvas.h"
#include "ui_WellView.h"
#include "VesselMapWidgetColorCodes.h"
#include "WellViewControl.h"

namespace TC {
    struct WellView::Impl {
        Ui::WellView ui;
        WellViewControl control;

        const QIcon gridOnIcon{":/img/ic-sub-grid.svg"};
        const QIcon gridOffIcon{":/img/ic-sub-gridoff.svg"};

        auto ClearWellInfo() -> void;
    };

    auto WellView::Impl::ClearWellInfo() -> void {
        control.SetCurrentFocusWell(kInvalid);
        ui.posName->clear();
        ui.posX->clear();
        ui.posY->clear();
        ui.posZ->clear();
    }

    WellView::WellView(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()}{
        d->ui.setupUi(this);
        d->ClearWellInfo();

        d->ui.baseWidget->setObjectName("panel");
        d->ui.xLabel->setObjectName("label-h5");
        d->ui.yLabel->setObjectName("label-h5");
        d->ui.zLabel->setObjectName("label-h5");
        d->ui.unitLabelX->setObjectName("label-h7");
        d->ui.unitLabelY->setObjectName("label-h7");
        d->ui.unitLabelZ->setObjectName("label-h7");

        d->ui.posX->setProperty("AxisIndex", static_cast<int32_t>(VesselAxis::X));
        d->ui.posY->setProperty("AxisIndex", static_cast<int32_t>(VesselAxis::Y));
        d->ui.posZ->setProperty("AxisIndex", static_cast<int32_t>(VesselAxis::Z));

        d->ui.posX->setSingleStep(0.1);
        d->ui.posY->setSingleStep(0.1);
        d->ui.posZ->setSingleStep(0.001);

        connect(d->ui.canvas, &WellCanvas::sigWellCanvasDblClicked, this, &Self::sigDoubleClicked);
        connect(d->ui.canvas, &WellCanvas::sigPreviewItemGeometryChanged, this, &Self::sigPreviewGeometryChangedOnWellCanvas);
        connect(d->ui.canvas, &WellCanvas::sigTileAreaGeometryChanged, this, &Self::sigTileImagingGeoChangedOnWellCanvas);
        connect(d->ui.canvas, &WellCanvas::sigDeleteMarkItem, this, &Self::sigDeleteMarkItem);
        connect(d->ui.canvas, &WellCanvas::sigDeleteAcqItem, this, &Self::sigDeleteAcqItem);
        connect(d->ui.canvas, &WellCanvas::sigCurrentSelectedAcqItems, this, &Self::sigCurrentSelectedAcqItems);
        connect(d->ui.canvas, &WellCanvas::sigAcquisitionItemDblClicked, this, &Self::sigAcqItemDblClicked);
        connect(d->ui.canvas, &WellCanvas::sigMarkItemDblClicked, this, &Self::sigMarkItemDblClicked);
        connect(d->ui.canvas, &WellCanvas::sigImportAcquisitionToMark, this, &Self::sigImportAcquisitionToMark);
        connect(d->ui.gridBtn, &QToolButton::toggled, this, &Self::onChangeShowGridState);

        connect(d->ui.posX, &TCDoubleSpinBox::returnPressed, this, &Self::onPositionSpinBoxEditingFinished);
        connect(d->ui.posY, &TCDoubleSpinBox::returnPressed, this, &Self::onPositionSpinBoxEditingFinished);
        connect(d->ui.posZ, &TCDoubleSpinBox::returnPressed, this, &Self::onPositionSpinBoxEditingFinished);

        auto highlightColor = WellPosNameHighlightColor;
        highlightColor.setAlphaF(0.3);
        d->ui.posName->setStyleSheet(QString("QLineEdit{border-color:%1; background-color:%2;}").arg(highlightColor.name(QColor::HexRgb)).arg(highlightColor.name(QColor::HexArgb)));

        d->ui.gridBtn->setStyleSheet("QToolButton {border:none; background-color:transparent;}");
        d->ui.gridBtn->setFixedSize(24,24);
        d->ui.gridBtn->setIconSize({24,24});
        d->ui.gridBtn->setCheckable(true);
        d->ui.gridBtn->setChecked(true);
        d->ui.gridBtn->setIcon(d->gridOnIcon);
    }

    WellView::~WellView() {
    }

    auto WellView::SetViewMode(ViewMode mode) -> void {
        d->ui.canvas->SetViewMode(mode);
        switch (mode) {
            case ViewMode::CanvasMode:
            case ViewMode::InfoMode:
                SetToolVisible(false);
                ShowLensItem(false);
                break;
            case ViewMode::PerformMode:
                SetLensPositionEditable(true);
                SetToolVisible(true);
                ShowLensItem(true);
                break;
            case ViewMode::TimelapseMode:
                SetToolVisible(true);
                ShowLensItem(true);
                break;
            case ViewMode::CopyDlgMode:
                SetToolVisible(false);
                ShowLensItem(false);
                break;
            default: 
                ShowLensItem(false);
                SetToolVisible(true);
                break;
        }
    }

    auto WellView::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->ui.canvas->SetVesselMapDataRepo(vesselMapDataRepo);
        d->control.InitWellPosition(vesselMapDataRepo->GetWells());
    }

    auto WellView::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->ui.canvas->SetLocationDataRepo(locationDataRepo);
    }

    auto WellView::ClearAll() -> void {
        d->ui.canvas->ClearAll();
        d->ClearWellInfo();
    }

    auto WellView::ClearScene() -> void {
        d->ui.canvas->ClearScene();
        d->ClearWellInfo();
    }

    auto WellView::ShowLensItem(bool show) -> void {
         d->ui.canvas->ShowLensItem(show);
    }

    auto WellView::SetLensPosition(double x, double y, double z) -> void {
        d->ui.canvas->SetLensPosition(x,y);

        const auto position3D = d->control.GetWellPosByLensPos(x,y,z);

        blockSignals(true);
        d->ui.posX->setValue(position3D.x);
        d->ui.posY->setValue(position3D.y);
        d->ui.posZ->setValue(position3D.z);
        blockSignals(false);
    }

    auto WellView::SetLensPositionRange(const VesselAxis& axis, const double& min, const double& max) -> void {
        d->control.SetSafePosRange(axis, min, max);
    }

    auto WellView::SetLensPositionEditable(bool editable) -> void {
        d->ui.posX->setReadOnly(!editable);
        d->ui.posY->setReadOnly(!editable);
        d->ui.posZ->setReadOnly(!editable);
    }

    auto WellView::SetSceneRect(WellIndex wellIndex) -> void {
        d->ui.canvas->SetSceneRectAndItemSize(wellIndex);
    }

    auto WellView::SetCurrentFocusWell(WellIndex wellIndex) -> void {
        d->ui.canvas->SetCurrentWell(wellIndex);
        d->ui.canvas->ShowGridLine(d->ui.gridBtn->isChecked());
        d->control.SetCurrentFocusWell(wellIndex);
        d->ui.posName->setText(d->control.GetWellPosName());
    }

    auto WellView::AddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        d->ui.canvas->AddMarkLocation(wellIndex, markIndex);
    }

    auto WellView::SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        d->ui.canvas->SetMarkLocation(wellIndex, markIndex);
    }

    auto WellView::DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        d->ui.canvas->DeleteMarkLocation(wellIndex, markIndex);
    }

    auto WellView::AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui.canvas->AddAcquisitionLocation(wellIndex, acquisitionIndex);
    }

    auto WellView::SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui.canvas->SetAcquisitionLocation(wellIndex, acquisitionIndex);
    }

    auto WellView::DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui.canvas->DeleteAcquisitionLocation(wellIndex, acquisitionIndex);
    }

    auto WellView::SetPreviewLocationEditable(bool editable) -> void {
        d->ui.canvas->SetPreviewLocationEditable(editable);
    }

    auto WellView::SetPreviewLocation(double x, double y, double w, double h) -> void {
        d->ui.canvas->SetPreviewLocation(x, y, w, h);
    }

    auto WellView::SetPreviewLocation(const Geometry2D& geometry) -> void {
        SetPreviewLocation(geometry.x, geometry.y, geometry.w, geometry.h);
    }

    auto WellView::SetPreviewLocationVisible(bool show) -> void {
        d->ui.canvas->ShowPreviewArea(show);
    }

    auto WellView::ShowTileImagingArea(bool show) -> void {
        d->ui.canvas->ShowTileImagingArea(show);
    }

    auto WellView::SetTileImagingArea(double x, double y, double z, double w, double h) -> void {
        d->ui.canvas->SetTileImagingArea(x,y,z,w,h);
    }

    auto WellView::SetTileImagingAreaCenter(double xInMm, double yInMm, double zInMm) -> void {
        d->ui.canvas->SetTileImagingAreaCenter(xInMm, yInMm, zInMm);
    }

    auto WellView::SetToolVisible(bool visible)->void {
        d->ui.toolFrame->setVisible(visible);
    }

    auto WellView::SetCurrentAcquisitionLocation(LocationType locationType, double xMM, double yMM) -> void {
        d->ui.canvas->SetCurrentAcquisitionLocation(locationType, xMM, yMM);
    }

    auto WellView::FitWellCanvas()->void {
        d->ui.canvas->FitInView();
    }

    auto WellView::SavePreview() -> void {
        d->control.SavePreviewStatus(d->ui.canvas->IsPreviewAreaVisible(), d->ui.canvas->GetPreviewLocation());
    }

    auto WellView::RestorePreview() -> void {
        SetPreviewLocationVisible(d->control.GetLastPreviewAreaVisibleStatus());
        SetPreviewLocation(d->control.GetLastPreviewArea());

    }

    auto WellView::SetPreviewItemSelected(bool selected) -> void{
        d->ui.canvas->SetPreviewLocationSelected(selected);
    }

    auto WellView::SetMatrixItemVisible(bool visible) -> void {
        d->ui.canvas->SetMatrixItemVisible(visible);
    }

    auto WellView::SetMatrixItems(const QList<QPair<double, double>>& positions) -> void {
        d->ui.canvas->CreateMatrixItems(positions);
    }

    void WellView::onUpdateTableSectionByImgPtTable(const QList<QPair<WellIndex, AcquisitionIndex>>& selected) {
        d->ui.canvas->SetSelectedAcqItems(selected);
    }

    void WellView::onChangeShowGridState(bool toggled) {
        if(toggled) {
            d->ui.gridBtn->setIcon(d->gridOnIcon);
        }
        else {
            d->ui.gridBtn->setIcon(d->gridOffIcon);
        }

        d->ui.canvas->ShowGridLine(d->ui.gridBtn->isChecked());
    }

    void WellView::onPositionSpinBoxEditingFinished() {
        const auto who = qobject_cast<TC::TCDoubleSpinBox*>(sender());

        bool ok{};
        const auto axis = static_cast<VesselAxis>(who->property("AxisIndex").toInt(&ok));
        if(ok) {
            const auto inputVal = who->value();
            const auto safeVal = d->control.GetSafePosValue(axis, inputVal);
            who->leaveFocus();
            emit sigChangeLensPosition(axis, safeVal);
        }
    }

    void WellView::resizeEvent(QResizeEvent* event) {
        d->ui.canvas->FitInView();
        QWidget::resizeEvent(event);
    }
}
