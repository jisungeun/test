﻿#pragma once

#include <memory>

#include "WellCanvasLensItem.h"
#include "VesselMapExternalData.h"
#include "WellCanvas.h"
#include "WellCanvasPreviewItem.h"
#include "WellCanvasTileImagingItem.h"
#include "WellCanvasSelectedPointItem.h"

namespace TC {
    class WellCanvasItemCreator {
    public:
        using Self = WellCanvasItemCreator;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasItemCreator();
        ~WellCanvasItemCreator();

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        [[nodiscard]] auto CreateLensItem() -> WellCanvasLensItem*;
        [[nodiscard]] auto CreateTileImagingItem() -> WellCanvasTileImagingItem*;
        [[nodiscard]] auto CreatePreviewItem() -> WellCanvasPreviewItem*;
        [[nodiscard]] auto CreateSelectedLocInfoItem() -> WellCanvasSelectedPointItem*;

        [[nodiscard]] auto CreateWellCanvasItems(WellIndex wellIndex, bool supportContextMenu) -> QList<QGraphicsItem*>;
        [[nodiscard]] auto CreateWellItem(WellIndex wellIndex) -> QGraphicsItem*;

        [[nodiscard]] auto CreateMarkItem(WellIndex wellIndex, MarkIndex markIndex) -> QGraphicsItem*;
        [[nodiscard]] auto CreateAcquisitionItem(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, bool supportContextMenu) -> QGraphicsItem*;

        [[nodiscard]] auto CreateMatrixItem(AcquisitionType type, double x, double y, double width, double height) -> QGraphicsItem*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
