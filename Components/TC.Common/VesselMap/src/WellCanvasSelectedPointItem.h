﻿#pragma once
#include <memory>

#include <QGraphicsItem>

#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasSelectedPointItem : public QGraphicsItem{
    public:
        using Self = WellCanvasSelectedPointItem;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvasSelectedPointItem(QGraphicsItem* parent = nullptr);
        ~WellCanvasSelectedPointItem() override;

        auto SetLocatinoType(LocationType locType) -> void;
        auto SetPosition(double x, double y) -> void;

    protected:
        auto type() const -> int override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto boundingRect() const -> QRectF override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
