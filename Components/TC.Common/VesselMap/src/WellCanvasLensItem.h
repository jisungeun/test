﻿#pragma once

#include <QGraphicsItem>

#include "VesselMapCustomDataTypes.h"

namespace TC {
    class WellCanvasLensItem : public QGraphicsItem {
    public:
        using Self = WellCanvasLensItem;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasLensItem(Position2D lensPos, QGraphicsItem* parent = nullptr);
        ~WellCanvasLensItem() override;

        auto SetVisible(bool show) -> void;
        auto SetPosition(double x, double y) -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
