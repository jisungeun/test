﻿#pragma once

#include <QGraphicsView>
#include <QList>

#include "VesselMapExternalData.h"
#include "Holder.h"
#include "Vessel.h"
#include "VesselMapDataRepo.h"
#include "Well.h"

namespace TC {
    class VesselCanvas : public QGraphicsView {
        Q_OBJECT
	public:
        using Self = VesselCanvas;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselCanvas(QWidget* parent = nullptr);
        ~VesselCanvas() override;

        auto SetViewMode(ViewMode mode) -> void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;

        auto ClearAll() -> void;
        auto ClearScene() -> void;
        auto UpdateSceneRect(VesselIndex vesselIndex = kInvalid) -> void;
        auto DrawVesselItem() -> void;
        auto DrawWellItem() -> void;
        auto DrawRowColumnTextItem(VesselIndex vesselIndex) -> void;

        auto SetEditMode(EditMode mode) -> void;
        auto SetLensPosition(double x, double y) -> void;
        auto SetSelectedWell(WellIndex wellIndex) -> void;

        auto GetSelectedWellIndices() const -> QList<WellIndex>&;
        auto GetSelectedGroupIndices() const -> QList<GroupIndex>&;
        auto GetFocusedWellIndex() const -> WellIndex;

        auto ShowImagingOrder(bool show) -> void;
        auto SetWellItemOrderNumber(QList<WellIndex> wells) -> void;

        auto UpdateWellGroupCreated(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto UpdateWellGroupDeleted(GroupIndex groupIndex) -> void;
        auto UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto UpdateWellGroupMemberRemoved(const QList<WellIndex>& wellIdices) -> void;
        auto UpdateWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void;
        auto UpdateWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void;

        auto UpdateWellNameChanged(WellIndex wellIndex, const QString& name) -> void;

        auto UpdateSelectedWells(const QList<WellIndex>& wellIndices) -> void; 

    signals:
        void sigSelectedWellIndices(const QList<WellIndex>& wellIdices);
        void sigSelectedGroupIndices(const QList<GroupIndex>& groupIndices);
        void sigVesselCanvasDblClicked(const WellIndex& wellIndex, int32_t row, int32_t column);
        void sigChangeSelectWell(const WellIndex& wellIndex);

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;
        auto mouseDoubleClickEvent(QMouseEvent* event) -> void override;
        auto mousePressEvent(QMouseEvent* event) -> void override;

    private slots:
        void onSceneSelectionChanged();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };


}
