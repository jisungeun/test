﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"

namespace TC {
    class VesselMapCalculator {
    public:
        using Self = VesselMapCalculator;
        using Pointer = std::shared_ptr<Self>;

        VesselMapCalculator();
        VesselMapCalculator(const VesselMapCalculator& other);
        ~VesselMapCalculator();

        auto operator=(const VesselMapCalculator& other) -> VesselMapCalculator&;

        auto SetVesselWidth(double width) -> void;
        auto SetVesselHeight(double height) -> void;

        auto SetVesselRows(int32_t rows) -> void;
        auto SetVesselColumns(int32_t columns) -> void;

        auto SetWellWidth(double width) -> void;
        auto SetWellHeight(double height) -> void;

        auto SetWellRows(int32_t rows) -> void;
        auto SetWellColumns(int32_t columns) -> void;

        auto CalculateVesselIndex(int32_t row, int32_t column) -> VesselIndex;
        auto CalculateWellIndex(int32_t row, int32_t column) -> WellIndex;

        auto CalculateRowOffset(int32_t row, double height, double rowSpacing) -> double;
        auto CalculateColumnOffset(int32_t column, double width, double columnSpacing) -> double;

        auto CalculatePosX(double startX, int32_t column, double width, double offset) -> double;
        auto CalculatePosY(double startY, int32_t row, double height, double offset) -> double;

        auto CalculateWellSystemPos(double localWellPos, double vesselPos) -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
