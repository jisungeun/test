﻿#include <QPainter>

#include "VesselCanvasVesselItem.h"
#include "GraphicsItemDefine.h"
#include "DefaultSettingHelper.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct VesselCanvasVesselItem::Impl {
        VesselIndex index{kInvalid};
        VesselShape shape{VesselShape::Rectangle};

        struct {
            double w{0.0};
            double h{0.0};
        } size;

        struct {
            int32_t rows{-1};
            int32_t cols{-1};
        } wellCount;

        auto DrawShape(QPainter* painter) -> void;
        auto GetDrawRect() -> QRectF;
    };

    VesselCanvasVesselItem::VesselCanvasVesselItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
    }

    VesselCanvasVesselItem::~VesselCanvasVesselItem() {
    }

    auto VesselCanvasVesselItem::SetIndex(VesselIndex index) -> void {
        d->index = index;
    }

    auto VesselCanvasVesselItem::GetIndex() const -> VesselIndex {
        return d->index;
    }

    auto VesselCanvasVesselItem::SetShape(VesselShape shape) -> void {
        d->shape = shape;
    }

    auto VesselCanvasVesselItem::GetShape() const -> VesselShape {
        return d->shape;
    }

    auto VesselCanvasVesselItem::SetSize(double width, double height) -> void {
        d->size = {width, height};
    }

    auto VesselCanvasVesselItem::GetWidth() const -> double {
        return d->size.w;
    }

    auto VesselCanvasVesselItem::GetHeight() const -> double {
        return d->size.h;
    }

    auto VesselCanvasVesselItem::SetPosition(double x, double y) -> void {
        setPos(x, -y);
    }

    auto VesselCanvasVesselItem::GetX() const -> double {
        return x();
    }

    auto VesselCanvasVesselItem::GetY() const -> double {
        return -y();
    }

    auto VesselCanvasVesselItem::SetWellCount(int32_t rows, int32_t cols) -> void {
        d->wellCount = {rows, cols};
    }

    int32_t VesselCanvasVesselItem::type() const {
        return static_cast<int32_t>(GraphicsItemType::VesselItem);
    }

    QPainterPath VesselCanvasVesselItem::shape() const {
        QPainterPath path;
        switch (d->shape) {
            case VesselShape::Circle: path.addEllipse(d->GetDrawRect());
                break;
            case VesselShape::Rectangle: path.addRect(d->GetDrawRect());
                break;
        }
        return path;
    }

    auto VesselCanvasVesselItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->size.w, d->size.h);
    }

    auto VesselCanvasVesselItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        auto pen = QPen(VesselBorderColor);
        pen.setWidthF(0.0);

        painter->setRenderHint(QPainter::Antialiasing);
        painter->setPen(pen);
        painter->setBrush(VesselBackgroundColor);

        d->DrawShape(painter);
    }

    auto VesselCanvasVesselItem::Impl::DrawShape(QPainter* painter) -> void {
        switch (shape) {
            case VesselShape::Circle: {
                painter->drawEllipse(GetDrawRect());
                break;
            }
            case VesselShape::Rectangle: {
                painter->drawRect(GetDrawRect());
            }
        }
    }

    auto VesselCanvasVesselItem::Impl::GetDrawRect() -> QRectF {
        const auto drawRect = DefaultSettingHelper::GetBoundingRect(size.w, size.h);
        return drawRect;
    }
}
