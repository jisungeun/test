﻿#include <QList>
#include <QMap>

#include "VesselMap.h"
#include "Holder.h"
#include "Vessel.h"
#include "Well.h"
#include "ImagingArea.h"
#include "Roi.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    struct VesselMap::Impl {
        Holder::Pointer holder{nullptr};
        QList<Vessel::Pointer> vessels;
        QList<Well::Pointer> wells;
        ImagingArea::Pointer imagingArea{nullptr};
        QMap<WellIndex, QList<Roi::Pointer>> roiList{};

        auto GetVessel(VesselIndex vesselIndex) -> Vessel::Pointer;
        auto GetWell(WellIndex wellIndex) -> Well::Pointer;

        // values from VesselMapUtility
        Size2D vesselSize{0.0, 0.0};
        VesselShape vesselShape{VesselShape::Rectangle};

        Size2D wellSize{0.0, 0.0};
        WellShape wellShape{WellShape::Circle};

        Range2D imagingAreaRange{};
    };

    VesselMap::VesselMap() : d{std::make_unique<Impl>()} {
        d->holder = std::make_shared<Holder>();
        d->imagingArea = std::make_shared<ImagingArea>();
    }

    VesselMap::VesselMap(const VesselMap& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    VesselMap::~VesselMap() {
    }

    auto VesselMap::operator=(const VesselMap& other) -> VesselMap& {
        *d = *other.d;
        return *this;
    }

    auto VesselMap::SetHolder(std::shared_ptr<Holder> holder) -> void {
        d->holder = holder;
    }

    auto VesselMap::GetHolder() const -> std::shared_ptr<Holder> {
        return d->holder;
    }

    auto VesselMap::AddVessel(std::shared_ptr<Vessel> vessel) -> void {
        d->vessels.push_back(vessel);
    }

    auto VesselMap::GetVessels() const -> QList<std::shared_ptr<Vessel>>& {
        return d->vessels;
    }

    auto VesselMap::AddWell(std::shared_ptr<Well> well) -> void {
        d->wells.push_back(well);
    }

    auto VesselMap::GetWells() const -> QList<std::shared_ptr<Well>> {
        return d->wells;
    }

    auto VesselMap::SetImagingArea(std::shared_ptr<ImagingArea> imagingArea) -> void {
        d->imagingArea = imagingArea;
    }

    auto VesselMap::SetImagingArea(ImagingAreaShape shape, double centerX, double centerY, double width, double height) -> void {
        d->imagingArea->SetShape(shape);
        d->imagingArea->SetX(centerX);
        d->imagingArea->SetY(centerY);
        d->imagingArea->SetWidth(width);
        d->imagingArea->SetHeight(height);

        auto minX = -width/2 + centerX;
        auto maxX = width/2 + centerX;
        auto minY = -height/2 + centerY;
        auto maxY = height/2 + centerY;

        d->imagingAreaRange.setXRange({minX, maxX});
        d->imagingAreaRange.setYRange({minY, maxY});
    }

    auto VesselMap::GetImagingArea() const -> std::shared_ptr<ImagingArea> {
        return d->imagingArea;
    }

    auto VesselMap::GetImagingAreaRange() const -> Range2D {
        return d->imagingAreaRange;
        
    }

    auto VesselMap::SetHolderSize(double width, double height) -> void {
        d->holder->SetWidth(width);
        d->holder->SetHeight(height);
    }

    auto VesselMap::SetVesselShape(VesselShape shape) -> void {
        d->vesselShape = shape;
    }

    auto VesselMap::SetVesselSize(double width, double height) -> void {
        d->vesselSize = {width, height};
    }

    auto VesselMap::SetVessel(VesselIndex vesselIndex, double centerX, double centerY, int32_t rowCount, int32_t columnCount) -> void {
        Vessel::Pointer vessel = nullptr;
        bool exist = (d->GetVessel(vesselIndex) == nullptr) ? false : true;
        if (exist) {
            vessel = d->GetVessel(vesselIndex);
        }
        else {

            vessel = std::make_shared<Vessel>();
        }

        vessel->SetIndex(vesselIndex);
        vessel->SetWidth(d->vesselSize.w);
        vessel->SetHeight(d->vesselSize.h);
        vessel->SetShape(d->vesselShape);
        vessel->SetRowIndex(0);
        vessel->SetColumnIndex(0);
        vessel->SetX(centerX);
        vessel->SetY(centerY);
        vessel->SetWellCount(rowCount, columnCount);

        if (!exist) d->vessels.push_back(vessel);
    }

    auto VesselMap::SetWellShape(WellShape shape) -> void {
        d->wellShape = shape;
    }

    auto VesselMap::SetWellSize(double width, double height) -> void {
        d->wellSize = {width, height};
    }

    auto VesselMap::SetWell(WellIndex wellIndex, int32_t row, int32_t column, double centerX, double centerY, QString name) -> void {
        if(wellIndex == kInvalid) return;

        Well::Pointer well = nullptr;
        bool exist = (d->GetWell(wellIndex) == nullptr) ? false : true;

        if (exist) {
            well = d->GetWell(wellIndex);
        }
        else {
            well = std::make_shared<Well>();
        }

        well->SetIndex(wellIndex);
        well->SetWidth(d->wellSize.w);
        well->SetHeight(d->wellSize.h);
        well->SetShape(d->wellShape);
        well->SetRow(row);
        well->SetColumn(column);
        well->SetX(centerX);
        well->SetY(centerY);
        well->SetName(name);

        if (!exist) d->wells.push_back(well);
    }

    auto VesselMap::AddRoi(WellIndex wellIndex, RoiIndex roiIndex, const QString& name, RoiShape shape, double centerX, double centerY, double width, double height) -> void {
        d->roiList[wellIndex].push_back(std::make_shared<Roi>(roiIndex, shape, centerX, centerY, width, height, name));
    }

    auto VesselMap::SetRoiList(const QMap<WellIndex, QList<std::shared_ptr<Roi>>>& roiList) -> void {
        d->roiList = roiList;
    }

    auto VesselMap::GetRoiList() const -> QMap<WellIndex, QList<std::shared_ptr<Roi>>> {
        return d->roiList;
    }

    auto VesselMap::GetRoiList(const WellIndex& wellIndex) const -> QList<std::shared_ptr<Roi>> {
        if(d->roiList.contains(wellIndex)) {
            return d->roiList[wellIndex];
        }
        return {};
    }

    auto VesselMap::Impl::GetWell(WellIndex wellIndex) -> Well::Pointer {
        for (auto& well : wells) {
            if (well->GetIndex() == wellIndex) {
                return well;
            }
        }

        return nullptr;
    }

    auto VesselMap::Impl::GetVessel(VesselIndex vesselIndex) -> Vessel::Pointer {
        for (auto& vessel : vessels) {
            if (vessel->GetIndex() == vesselIndex) {
                return vessel;
            }
        }

        return nullptr;
    }
}
