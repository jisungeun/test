﻿#include <QMap>
#include <QPainterPath>

#include "ImagingPointTableControl.h"

#include "DefaultSettingHelper.h"
#include "LocationDataRepo.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapDataRepo.h"

namespace TC {
    struct ImagingPointTableControl::Impl {
        LocationDataRepo::Pointer location{nullptr};
        VesselMapDataRepo::Pointer repo{nullptr};

        WellIndex currentWellIndex{kInvalid};

        QMap<ImagingTableHeader, QString> columnHeaders;
        int32_t columnCount{-1};
        QStringList labels;

        bool showAll{false};

        auto InitColumnHeader() -> void;
        auto GetLocalPointIndexString(const int32_t& rowIndex) const -> QString;
        auto GetRoiIndexString(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) const -> QString;
    };

    ImagingPointTableControl::ImagingPointTableControl() : d{std::make_unique<Impl>()} {
        d->InitColumnHeader();
        d->columnCount = static_cast<int32_t>(ImagingTableHeader::_size());
    }

    ImagingPointTableControl::~ImagingPointTableControl() {
    }

    void ImagingPointTableControl::Clear() {
        d->currentWellIndex = kInvalid;
    }

    auto ImagingPointTableControl::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->repo = vesselMapDataRepo;
    }

    auto ImagingPointTableControl::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->location = locationDataRepo;
    }

    auto ImagingPointTableControl::GetWellIndices() const -> QList<WellIndex> {
        QList<WellIndex> wellIndices;

        if(!d->repo) {
            return wellIndices;
        }

        for(const auto well: d->repo->GetWells()) {
            wellIndices.push_back(well->GetIndex());
        }

        return wellIndices;
    }

    int32_t ImagingPointTableControl::GetColumnCount() const {
        return d->columnCount;
    }

    QStringList ImagingPointTableControl::GetColumnHeaderLabels() {
        QStringList labels;
        for (int32_t i = 0; i < d->columnCount; ++i) {
            labels << d->columnHeaders[ImagingTableHeader::_from_integral(i)];
        }
        return labels;
    }

    auto ImagingPointTableControl::SetCurrentWellIndex(WellIndex wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto ImagingPointTableControl::GetCurrentWellIndex() const -> WellIndex {
        return d->currentWellIndex;
    }

    ImagingPointTableData::Pointer ImagingPointTableControl::GenerateAcquisitionTableData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, int32_t rowPointIndex) {
        auto data = std::make_shared<ImagingPointTableData>();

        QString posName = "NA";
        for(const auto& well : d->repo->GetWells()) {
            if(well->GetIndex() == wellIndex) {
                posName = DefaultSettingHelper::MakeWellPositionName(well->GetRow(), well->GetColumn());
                break;
            }
        }

        auto acqLocation = d->location->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);
        if (acqLocation == nullptr) return nullptr;

        const auto acqlIndex = acqLocation->GetIndex();
        const auto pointIndex = d->GetLocalPointIndexString(rowPointIndex);
        const auto pos3d = acqLocation->GetPosition();
        const auto size2d = acqLocation->GetSize();
        const auto roiName = d->GetRoiIndexString(wellIndex, acquisitionIndex);
        data->SetWellIndex(wellIndex);
        data->SetWellPosName(posName);
        data->SetRoiIndexName(roiName);
        data->SetAcquisitionIndex(acqlIndex);
        data->SetPosition(pos3d);
        data->SetPointIndexName(pointIndex);
        data->SetSize(size2d);
        return data;
    }

    QList<AcquisitionIndex> ImagingPointTableControl::GetCurrentWellAcquisitionIndices() const {
        return d->location->GetAcquisitionIndicesByWellIndex(d->currentWellIndex);
    }

    auto ImagingPointTableControl::GetWellAcquisitionIndices(WellIndex wellIndex) const -> QList<AcquisitionIndex> {
        return d->location->GetAcquisitionIndicesByWellIndex(wellIndex);
    }

    auto ImagingPointTableControl::GetAcquisitionType(AcquisitionIndex acquisitionIndex) -> AcquisitionType {
        const auto loc = d->location->GetAcquisitionLocationByAcquisitionIndex(d->currentWellIndex, acquisitionIndex);

        if(loc) return loc->GetType();

        return AcquisitionType::Point;
    }

    auto ImagingPointTableControl::SetShowAll(bool showAll) -> void {
        d->showAll = showAll;
    }

    auto ImagingPointTableControl::IsShowAll() const -> bool {
        return d->showAll;
    }

    auto ImagingPointTableControl::IsRoiExist() const -> bool {
        for(const auto& well : d->repo->GetWells()) {
            if(!d->repo->GetRois(well->GetIndex()).isEmpty()) {
                return true;
            }
        }
        return false;
    }

    void ImagingPointTableControl::Impl::InitColumnHeader() {
        columnHeaders[ImagingTableHeader::AcquisitionIndex] = "Index";
        columnHeaders[ImagingTableHeader::Well] = "Well";
        columnHeaders[ImagingTableHeader::WellIndex] = "WellIdx";
        columnHeaders[ImagingTableHeader::PosX] = "X (mm)";
        columnHeaders[ImagingTableHeader::PosY] = "Y (mm)";
        columnHeaders[ImagingTableHeader::PosZ] = "Z (mm)";
        columnHeaders[ImagingTableHeader::Size] = "Size";
        columnHeaders[ImagingTableHeader::PointID] = "ID";
        columnHeaders[ImagingTableHeader::ROI] = "ROI";
    }

    auto ImagingPointTableControl::Impl::GetRoiIndexString(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) const -> QString {
        const auto rois = repo->GetRois(wellIndex);
        const auto loc = location->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);

        if(rois.isEmpty()) {
            return {};
        }
        if(!loc) {
            return {};
        }

        auto roiName = QString("-");

        const auto locX = loc->GetPosX();
        const auto locY = loc->GetPosY();
        const auto locW = loc->GetWidth();
        const auto locH = loc->GetHeight();

        for(const auto& roi : rois) {
            const auto roiIndex = roi->GetIndex();
            const auto roiShape = roi->GetShape();
            const auto roiX = roi->GetX();
            const auto roiY = roi->GetY();
            const auto roiW = roi->GetWidth();
            const auto roiH = roi->GetHeight();

            QRectF roiRect(QPointF{roiX - roiW / 2, roiY - roiH / 2}, QPointF{roiX + roiW / 2, roiY + roiH / 2});
            QRectF locRect(QPointF{locX - locW / 2, locY - locH / 2}, QPointF{locX + locW / 2, locY + locH / 2});
            
            QPainterPath roiPath;
            QPainterPath locPath;
            locPath.addRect(locRect);

            if (roiShape == +RoiShape::Rectangle) {
                roiPath.addRect(roiRect);
            }
            else if (roiShape == +RoiShape::Ellipse) {
                roiPath.addEllipse(roiRect);
            }
            if(roiPath.intersects(locPath)) {
                roiName = QString("R%1").arg(roiIndex);
                break;
            }

        }
        return roiName;
    }

    auto ImagingPointTableControl::Impl::GetLocalPointIndexString(const int32_t& rowIndex) const -> QString {
        return QString("P%1").arg(rowIndex, 2, 10, QChar('0'));
    }
}
