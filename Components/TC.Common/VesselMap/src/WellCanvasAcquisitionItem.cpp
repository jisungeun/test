﻿#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>

#include "WellCanvasAcquisitionItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapWidgetColorCodes.h"
#include "WellCanvasMenuSupport.h"

namespace TC {
    /// <summary>
    /// WellCanvasAcquisitionItem
    /// </summary>
    struct WellCanvasAcquisitionItem::Impl {
        AcquisitionIndex acquisitionIndex{kInvalid};
        WellIndex wellIndex{kInvalid};

        Geometry3D geo3D{};

        QColor pointColor{AcquisitionPointColor};
        QColor selectedColor{SelectedWellBorderColor};

        WellCanvasMenuSupport::Pointer menuSupport{nullptr};

        double pointSize{kMinLocPointSize};

        auto InitContextMenu() -> void;
    };

    auto WellCanvasAcquisitionItem::Impl::InitContextMenu() -> void {
        menuSupport = std::make_shared<WellCanvasMenuSupport>(nullptr);
        menuSupport->AddAction(WellCanvasMenu::AcquisitionToMark, tr("Import to &mark point"));
    }

    WellCanvasAcquisitionItem::WellCanvasAcquisitionItem(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex, const Position3D& pos3D, QGraphicsItem* parent)
    : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
        d->wellIndex = wellIndex;
        d->acquisitionIndex = acquisitionIndex;

        SetPosition3D(pos3D.x, pos3D.y, pos3D.z);
        setFlag(ItemIsSelectable, true);
        setCacheMode(DeviceCoordinateCache);
        setAcceptHoverEvents(true);
        setZValue(GraphicsItemZValue::Acquisition);
    }

    WellCanvasAcquisitionItem::~WellCanvasAcquisitionItem() {
    }

    auto WellCanvasAcquisitionItem::GetAcquisitionIndex() const -> AcquisitionIndex {
        return d->acquisitionIndex;
    }

    auto WellCanvasAcquisitionItem::GetWellIndex() const -> WellIndex {
        return d->wellIndex;
    }

    auto WellCanvasAcquisitionItem::SetPosition3D(double x, double y, double z) -> void {
        d->geo3D.x = x;
        d->geo3D.y = y;
        d->geo3D.z = z;
        setPos(x, -y);
    }

    auto WellCanvasAcquisitionItem::GetPositionX() const -> double {
        return d->geo3D.x;
    }

    auto WellCanvasAcquisitionItem::GetPositionY() const -> double {
        return d->geo3D.y;
    }

    auto WellCanvasAcquisitionItem::GetPositionZ() const -> double {
        return d->geo3D.z;
    }

    auto WellCanvasAcquisitionItem::CreateContextMenu() -> void {
        d->InitContextMenu();
    }

    auto WellCanvasAcquisitionItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::AcqPointItem);
    }

    auto WellCanvasAcquisitionItem::shape() const -> QPainterPath {
        QPainterPath path;
        path.addEllipse(boundingRect());
        return path;
    }

    auto WellCanvasAcquisitionItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize);
    }

    auto WellCanvasAcquisitionItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)

        UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        auto brush = d->pointColor;
        auto pen = QPen(Qt::NoPen);
        if (option->state & QStyle::State_Selected) {
                pen.setColor(d->selectedColor);
                pen.setWidth(0);
                pen.setStyle(Qt::SolidLine);
                brush = brush.darker();
        }

        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize));
    }

    auto WellCanvasAcquisitionItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            emit sigAcquisitionItemDoubleClicked(d->wellIndex, 
                                                 d->acquisitionIndex, 
                                                 d->geo3D.x, 
                                                 d->geo3D.y, 
                                                 d->geo3D.z);
        }
        QGraphicsItem::mouseDoubleClickEvent(event);
    }

    auto WellCanvasAcquisitionItem::contextMenuEvent(QGraphicsSceneContextMenuEvent* event) -> void {
        if(d->menuSupport) {
            setSelected(true);

            const auto menuIndex = d->menuSupport->ExecuteMenu(QCursor::pos());
            switch (menuIndex) {
                case WellCanvasMenu::AcquisitionToMark:
                    emit sigRequestImportAcquisitionToMark(d->wellIndex, d->acquisitionIndex);
                    break;
                default: 
                    break;
            }
        }
        QGraphicsItem::contextMenuEvent(event);
    }

    auto WellCanvasAcquisitionItem::GetPointColor() const -> QColor {
        return d->pointColor;
    }

    auto WellCanvasAcquisitionItem::GetSelectedColor() const -> QColor {
        return d->selectedColor;
    }
    
    auto WellCanvasAcquisitionItem::UpdatePointSize(const double& lod) -> void {
        d->pointSize = 10/lod;
        if(d->pointSize < kMinLocPointSize) {
            d->pointSize = kMinLocPointSize;
        }
        update();
    }

    auto WellCanvasAcquisitionItem::GetPointSize() const -> double {
        return d->pointSize;
    }

    /// <summary>
    /// WellCanWellCanvasAcquisitionTileItem// </summary>
    struct WellCanvasAcquisitionTileItem::Impl {
        Size2D tileSize;
        QColor penColor;
        double penWidth{};
    };

    WellCanvasAcquisitionTileItem::WellCanvasAcquisitionTileItem(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex, const Position3D& pos3D, const Size2D& tileSize, QGraphicsItem* parent)
    : WellCanvasAcquisitionItem(wellIndex, acquisitionIndex, pos3D, parent), d{std::make_unique<Impl>()} {
        d->penColor = GetPointColor();
        SetTileSize(tileSize.w, tileSize.h);
    }

    WellCanvasAcquisitionTileItem::~WellCanvasAcquisitionTileItem() {
    }

    auto WellCanvasAcquisitionTileItem::SetTileSize(const double& w, const double& h) -> void {
        d->tileSize = {w, h};
    }

    int WellCanvasAcquisitionTileItem::type() const {
        return static_cast<int32_t>(GraphicsItemType::AcqTileItem);
    }

    QPainterPath WellCanvasAcquisitionTileItem::shape() const {
        QPainterPath path;
        path.addRect(boundingRect());
        return path;
    }

    QRectF WellCanvasAcquisitionTileItem::boundingRect() const {
        return DefaultSettingHelper::GetBoundingRect(d->tileSize.w, d->tileSize.h);
    }

    void WellCanvasAcquisitionTileItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
        Q_UNUSED(widget)
        UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        QPen pen(d->penColor, 0, Qt::SolidLine);
        QColor brushColor = Qt::transparent;
        if (option->state & QStyle::State_Selected) {
            pen.setColor(GetSelectedColor());
            pen.setWidth(0);
            pen.setStyle(Qt::SolidLine);

            brushColor = d->penColor;
            brushColor.setAlpha(50);
        }

        painter->save();
        painter->setPen(pen);
        const auto rect = DefaultSettingHelper::GetBoundingRect(d->tileSize.w, d->tileSize.h);
        QPainterPath path;
        painter->setBrush(QBrush(brushColor, Qt::SolidPattern));
        path.addRect(rect);
        painter->drawPath(path);
        painter->restore();

        if(GetPointSize() < d->tileSize.w && GetPointSize() < d->tileSize.h) {
            WellCanvasAcquisitionItem::paint(painter, option, widget);
        }
    }
}
