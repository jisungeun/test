﻿#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QCursor>

#include "ResizeHandleItem.h"
#include "ResizableItemHandler.h"

namespace TC {
    struct ResizeHandleItem::Impl {
        Position position;

        struct {
            double x;
            double y;
        } limitPos;
    };

    ResizeHandleItem::ResizeHandleItem(Position position, QGraphicsItem* parent) : QGraphicsEllipseItem(parent), d{std::make_unique<Impl>()} {
        setFlags(ItemIsMovable);
        d->position = position;

        setPen(Qt::NoPen);
        setBrush(QBrush(QColor(69, 255, 0)));
        setAcceptHoverEvents(true);
    }

    auto ResizeHandleItem::SetLimits(double x, double y) -> void {
        d->limitPos = {x, y};
    }

    auto ResizeHandleItem::HandlePosition() const -> Position {
        return d->position;
    }

    auto ResizeHandleItem::IsMoveAvailable() const -> bool {
        if(!scene()) {
            return true;
        }

        const auto sceneRect = scene()->sceneRect();
        QRectF boundingFrameRect{};

        if (const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem())) {
            boundingFrameRect = rectItem->GetSelectorFrameBounds();
        }
        else {
            return true;
        }

        switch (d->position) {
            case TopLeft: {
                return sceneRect.contains(mapToScene(boundingFrameRect.topLeft()));
            }
            case TopRight: {
                return sceneRect.contains(mapToScene(boundingFrameRect.topRight()));
            }
            case BottomLeft: {
                return sceneRect.contains(mapToScene(boundingFrameRect.bottomLeft()));
            }
            case BottomRight: {
                return sceneRect.contains(mapToScene(boundingFrameRect.bottomRight()));
            }
            default: 
                return true;
        }
    }

    auto ResizeHandleItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void {
        const auto sceneRect = scene()->sceneRect();
        const auto pos = event->pos();
        const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem());
        auto boundingFrameRect = rectItem->GetSelectorFrameBounds();

        switch (d->position) {
            case TopLeft: {
                // 오른쪽, 아래 제한
                const auto sceneTopLeft = mapFromScene(sceneRect.topLeft());
                const auto x = std::max(sceneTopLeft.x(), std::min(pos.x(), d->limitPos.x));
                const auto y = std::max(sceneTopLeft.y(), std::min(pos.y(), d->limitPos.y));
                boundingFrameRect.setTopLeft({x, y});
                rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
            }
            break;

            case TopRight: {
                // 왼쪽, 아래 제한
                const auto sceneTopRight = mapFromScene(sceneRect.topRight());
                const auto x = std::max(d->limitPos.x, std::min(pos.x(), sceneTopRight.x()));
                const auto y = std::max(sceneTopRight.y(), std::min(pos.y(), d->limitPos.y));
                boundingFrameRect.setTopRight({x, y});
                rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
            }
            break;

            case BottomLeft: {
                // 오른쪽, 위 제한
                const auto sceneBottomLeft = mapFromScene(sceneRect.bottomLeft());
                boundingFrameRect.setBottom(std::max(d->limitPos.y, std::min(pos.y(), sceneBottomLeft.y())));
                boundingFrameRect.setLeft(std::max(sceneBottomLeft.x(), std::min(pos.x(), d->limitPos.x)));
                rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
            }
            break;

            case BottomRight: {
                // 왼쪽, 위 제한
                const auto sceneBottomRight = mapFromScene(sceneRect.bottomRight());
                boundingFrameRect.setBottom(std::max(d->limitPos.y, std::min(pos.y(), sceneBottomRight.y())));
                boundingFrameRect.setRight(std::max(d->limitPos.x, std::min(pos.x(), sceneBottomRight.x())));
                rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
            }
            break;

            default: ;
        }
    }

    auto ResizeHandleItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void {
        setCursor(Qt::CrossCursor);
        QGraphicsEllipseItem::hoverEnterEvent(event);
    }

    auto ResizeHandleItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void {
        setCursor(Qt::ArrowCursor);
        QGraphicsEllipseItem::hoverLeaveEvent(event);
    }

    void ResizeHandleItem::onOwnerItemMove(const QPointF& lastPos, const QPointF& currPos) {
        if (IsMoveAvailable()) {
            return;
        }

        const auto sceneRect = scene()->sceneRect();
        const auto dx = currPos.x() - lastPos.x();
        const auto dy = currPos.y() - lastPos.y();
        const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem());
        auto boundingFrameRect = rectItem->GetSelectorFrameBounds();

        switch (d->position) {
            case TopLeft: {
                const auto sceneTopLeft = mapFromScene(sceneRect.topLeft());
                if (dx < 0 && boundingFrameRect.left() < sceneTopLeft.x()) {
                    boundingFrameRect.moveLeft(sceneTopLeft.x());
                }
                if (dy < 0 && boundingFrameRect.top() < sceneTopLeft.y()) {
                    boundingFrameRect.moveTop(sceneTopLeft.y());
                }
            }
            break;
            case TopRight: {
                const auto sceneTopRight = mapFromScene(sceneRect.topRight());
                if (dx > 0 && boundingFrameRect.right() > sceneTopRight.x()) {
                    boundingFrameRect.moveRight(sceneTopRight.x());
                }
                if (dy < 0 && boundingFrameRect.top() < sceneTopRight.y()) {
                    boundingFrameRect.moveTop(sceneTopRight.y());
                }
            }
            break;
            case BottomLeft: {
                const auto sceneBottomLeft = mapFromScene(sceneRect.bottomLeft());
                if (dx < 0 && boundingFrameRect.left() < sceneBottomLeft.x()) {
                    boundingFrameRect.moveLeft(sceneBottomLeft.x());
                }
                if (dy > 0 && boundingFrameRect.bottom() > sceneBottomLeft.y()) {
                    boundingFrameRect.moveBottom(sceneBottomLeft.y());
                }
            }
            break;
            case BottomRight: {
                const auto sceneBottomRight = mapFromScene(sceneRect.bottomRight());
                if (dx > 0 && boundingFrameRect.right() > sceneBottomRight.x()) {
                    boundingFrameRect.moveRight(sceneBottomRight.x());
                }
                if (dy > 0 && boundingFrameRect.bottom() > sceneBottomRight.y()) {
                    boundingFrameRect.moveBottom(sceneBottomRight.y());
                }
            }
            break;

            default: break;
        }
        rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
    }
}
