﻿#pragma once

#include <memory>
#include <QGraphicsRectItem>

#include "ResizableItemHandler.h"
#include "GraphicsItemDefine.h"

namespace TC {
    class WellCanvasTileImagingItem :  public ResizableItemHandler, public QGraphicsRectItem {
    public:
        explicit WellCanvasTileImagingItem(QGraphicsItem* parent = nullptr);
        ~WellCanvasTileImagingItem() override;

        auto SetGeometry3D(const Geometry3D& geo) -> void;
        auto GetGeometry3D() const -> Geometry3D&;

        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto type() const -> int override;

        auto SetSelectorFrameBounds(const QRectF& boundRect) -> void override;
        auto GetSelectorFrameBounds() const -> QRectF override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
