﻿#pragma once

#include <QGraphicsItem>

#include "VesselMapExternalData.h"

namespace TC {
    class VesselCanvasVesselItem : public QGraphicsItem {
    public:
        using Self = VesselCanvasVesselItem;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselCanvasVesselItem(QGraphicsItem* parent = nullptr);
        ~VesselCanvasVesselItem() override;

        auto SetIndex(VesselIndex index) -> void;
        auto GetIndex() const -> VesselIndex;

        auto SetShape(VesselShape shape) -> void;
        auto GetShape() const -> VesselShape;

        auto SetSize(double width, double height) -> void;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetPosition(double x, double y) -> void;
        auto GetX() const -> double;
        auto GetY() const -> double;

        auto SetWellCount(int32_t rows, int32_t cols) -> void;
        auto GetWellRows() const -> int32_t;
        auto GetWellColumns() const -> int32_t;

    protected:
        auto type() const -> int32_t override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
