﻿#pragma once

#include <memory>

#include <QWidget>

namespace TC {
    class WellCanvasZoomButtonGroup : public QWidget {
        Q_OBJECT
    public:
        using Self = WellCanvasZoomButtonGroup;

        explicit WellCanvasZoomButtonGroup(QWidget* parent = nullptr);
        ~WellCanvasZoomButtonGroup() override;

    protected:
        void enterEvent(QEvent* event) override;
        void leaveEvent(QEvent* event) override;

    signals:
        void sigZoomIn();
        void sigZoomOut();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
