﻿#include "VesselMapLayoutManager.h"

namespace TC {
    struct VesselMapLayoutManager::Impl {
        QMap<SubWidgetType, QWidget*> widgets;
        QGridLayout* mainLayout{nullptr};
    };

    VesselMapLayoutManager::VesselMapLayoutManager() : d{std::make_unique<Impl>()} {
        d->mainLayout = new QGridLayout;
    }

    VesselMapLayoutManager::~VesselMapLayoutManager() {

    }

    auto VesselMapLayoutManager::InsertWidget(SubWidgetType key, QWidget* value) -> void {
        d->widgets[key] = value;
    }

    auto VesselMapLayoutManager::GetLayout() const -> QLayout* {
        return d->mainLayout;
    }

    auto VesselMapLayoutManager::SetLayout(ViewMode mode) const -> void {
        for (const auto w : d->widgets.values()) {
            d->mainLayout->removeWidget(w);
        }

        switch (mode) {
            case ViewMode::PreviewMode: {
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(true);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(false);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(false);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(false);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(false);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::VesselCanvas]);
                break;
            }
            case ViewMode::SetupMode: 
            case ViewMode::DataNaviMode: {
                d->widgets[SubWidgetType::VesselCanvas]->setStyleSheet("QWidget{"
                                         "border: 1px solid #394C53;"
                                         "border-radius: 6px;}");
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(true);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(true);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(false);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(false);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(false);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::VesselCanvas], 0, 0);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::WellGroupList], 0, 1);
                d->mainLayout->setContentsMargins(6,6,6,6);
                d->mainLayout->setMargin(10);
                d->mainLayout->setSpacing(30);
                d->mainLayout->setColumnStretch(0, 9);
                d->mainLayout->setColumnStretch(1, 6);
                break;
            }
            case ViewMode::PerformMode:
            case ViewMode::TimelapseMode:{
                d->widgets[SubWidgetType::VesselCanvas]->setStyleSheet("border:none;");
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(true);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(true);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(true);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(true);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(false);

                const auto leftTopLayout = new QHBoxLayout;
                leftTopLayout->addWidget(d->widgets[SubWidgetType::ImgOrder]);
                leftTopLayout->addWidget(d->widgets[SubWidgetType::VesselCanvas]);
                leftTopLayout->setSpacing(0);

                d->mainLayout->addLayout(leftTopLayout, 0, 0);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::WellCanvas], 1, 0);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::ImgPtList], 0, 1, -1, -1);
                d->mainLayout->setRowStretch(0, 1);
                d->mainLayout->setSpacing(6);
                d->mainLayout->setRowStretch(1, 3);
                d->mainLayout->setColumnStretch(0, 1);
                d->mainLayout->setColumnStretch(1, 1);
                d->mainLayout->setContentsMargins(0,0,0,0);

                break;
            }
            case ViewMode::CanvasMode: {
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(false);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(true);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(false);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(false);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(false);

                d->mainLayout->addWidget(d->widgets[SubWidgetType::WellCanvas]);

                break;
            }
            case ViewMode::InfoMode: {
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(true);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(true);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(false);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(false);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(false);

                d->mainLayout->addWidget(d->widgets[SubWidgetType::VesselCanvas], 0, 0);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::WellCanvas], 0, 1);
                d->mainLayout->setSpacing(20);
                d->mainLayout->setColumnStretch(0, 1);
                d->mainLayout->setColumnStretch(1, 0);
                d->mainLayout->setContentsMargins(0, 0, 0, 0);
                break;
            }
            case ViewMode::CopyDlgMode: {
                const QString ss = "QWidget{border: none;}";
                d->widgets[SubWidgetType::VesselCanvas]->setStyleSheet(ss);
                d->widgets[SubWidgetType::WellCanvas]->setStyleSheet(ss);

                d->widgets[SubWidgetType::ImgPtList]->blockSignals(true);
                d->widgets[SubWidgetType::ImgOrder]->blockSignals(true);
                d->widgets[SubWidgetType::WellGroupList]->blockSignals(true);
                d->widgets[SubWidgetType::VesselCanvas]->setVisible(true);
                d->widgets[SubWidgetType::WellCanvas]->setVisible(true);
                d->widgets[SubWidgetType::ImgPtList]->setVisible(false);
                d->widgets[SubWidgetType::ImgOrder]->setVisible(false);
                d->widgets[SubWidgetType::WellGroupList]->setVisible(false);

                d->mainLayout->addWidget(d->widgets[SubWidgetType::VesselCanvas], 0, 0);
                d->mainLayout->addWidget(d->widgets[SubWidgetType::WellCanvas], 0, 1);
                d->mainLayout->setSpacing(20);
                d->mainLayout->setContentsMargins(0, 0, 0, 0);
                break;
            }
                default:
                break;
        }
    }
}
