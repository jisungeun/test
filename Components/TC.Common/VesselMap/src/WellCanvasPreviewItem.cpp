﻿#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include "WellCanvasPreviewItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "ResizeHandleItem.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    namespace PreviewUtil {
        auto doubleCompare = [](const double& v1, const double& v2) -> bool {
        return fabs(v1 - v2) > 0.0001;
        };
    }

    struct WellCanvasPreviewItem::Impl {
        WellIndex wellIndex{kInvalid};
        Geometry2D geometry;
        QColor areaColor{PreviewAreaColor.red(), PreviewAreaColor.green(), PreviewAreaColor.blue(), 50};
        double penWidth{0.0};
        bool isEditable{};
        QRectF lastFrameBounds{};
    };

    WellCanvasPreviewItem::WellCanvasPreviewItem(bool isVisible, QGraphicsItem* parent) : QGraphicsRectItem(parent), d{std::make_unique<Impl>()} {
        setVisible(isVisible);
        SetOwnerItem(this);
        setAcceptHoverEvents(true);
        setFlag(ItemSendsGeometryChanges);
    }

    WellCanvasPreviewItem::~WellCanvasPreviewItem() {
    }
    
    auto WellCanvasPreviewItem::SetGeometry(double x, double y, double w, double h) -> void {
        prepareGeometryChange();
        d->geometry = {x, -y, w, h};
        setPos({x, -y});
        setRect(DefaultSettingHelper::GetBoundingRect(w, h));
        update();
    }

    auto WellCanvasPreviewItem::GetGeometry() const -> Geometry2D& {
        return d->geometry;
    }

    auto WellCanvasPreviewItem::SetSelectorFrameBounds(const QRectF& boundRect) -> void {
        prepareGeometryChange();
        setRect(boundRect);
        update();
    }

    auto WellCanvasPreviewItem::GetSelectorFrameBounds() const -> QRectF {
        return rect();
    }

    auto WellCanvasPreviewItem::SetEditable(bool editable) -> void {
        d->isEditable = editable;
        setFlag(ItemIsMovable, editable);
        setFlag(ItemIsSelectable, editable);
        if(editable) {
            setZValue(GraphicsItemZValue::PreviewEditable);
        }
        else {
            setZValue(GraphicsItemZValue::PreviewNonEditable);
            ClearHandles();
        }
    }

    auto WellCanvasPreviewItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::PreviewItem);
    }

    auto WellCanvasPreviewItem::shape() const -> QPainterPath {
        QPainterPath path;
        path.addRect(boundingRect());
        return path;
    }

    auto WellCanvasPreviewItem::boundingRect() const -> QRectF {
        return GetSelectorFrameBounds().adjusted(-d->penWidth, 
                                                 -d->penWidth, 
                                                 d->penWidth, 
                                                 d->penWidth);
    }

    auto WellCanvasPreviewItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)

        Geometry2D geo = {
            mapToScene(GetSelectorFrameBounds().center()).x(),
            -mapToScene(GetSelectorFrameBounds().center()).y(),
            GetSelectorFrameBounds().width(),
            GetSelectorFrameBounds().height()
        };
        d->geometry = geo;

        QBrush brush;
        brush.setColor(d->areaColor);
        brush.setStyle(Qt::SolidPattern);

        QPen pen;
        pen.setStyle(Qt::NoPen);
        if(d->isEditable) {
            pen.setStyle(Qt::DotLine);
            pen.setColor(PreviewBorderColor);
            pen.setWidthF(d->penWidth);
        }

        if(option->state & QStyle::State_Selected) {
            brush = brush.color().darker();
        }

        painter->setRenderHint(QPainter::Antialiasing);
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(rect());
        SetCurrentLOD(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));
        DrawHandlesIfNeeded();
    }

    auto WellCanvasPreviewItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void {
        const auto& lastPos = event->lastScenePos();
        const auto& currentPos = event->scenePos();
        sigOwnerItemMove(lastPos, currentPos);
        QGraphicsRectItem::mouseMoveEvent(event);
    }
}
