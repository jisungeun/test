﻿#pragma once

#include <memory>

#include <QMap>
#include <QWidget>
#include <QGridLayout>

#include "VesselMapExternalData.h"

namespace TC {
    class VesselMapLayoutManager {
    public:
        enum class SubWidgetType {
            VesselCanvas = 0,
            WellCanvas,
            WellGroupList,
            ImgPtList,
            ImgOrder,
        };

        using Self = VesselMapLayoutManager;
        using Pointer = std::shared_ptr<Self>;

        VesselMapLayoutManager();
        ~VesselMapLayoutManager();

        auto InsertWidget(SubWidgetType key, QWidget* value) -> void;
        auto GetLayout() const -> QLayout*;
        auto SetLayout(ViewMode mode) const -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}

Q_DECLARE_METATYPE(TC::VesselMapLayoutManager::SubWidgetType)
