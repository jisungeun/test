﻿#include <QGraphicsScene>

#include "ResizableItemHandler.h"
#include "ResizeHandleItem.h"

namespace TC {
    struct ResizableItemHandler::Impl {
        QRectF topleftHandleRect;
        QRectF topRightHandleRect;
        QRectF bottomRightHandleRect;
        QRectF bottomLeftHandleRect;
        QList<ResizeHandleItem*> handleList;
        bool handlesAddedToScene{false};
        QGraphicsItem* ownerItem{nullptr};
        double itemSize{1.0};
        double offset{0.0};

        auto drawHandles(const Self* self) -> void;
    };

    ResizableItemHandler::ResizableItemHandler() : d{std::make_unique<Impl>()} {
    }

    ResizableItemHandler::~ResizableItemHandler() {
    }

    auto ResizableItemHandler::SetCurrentLOD(const double& lod) -> void {
        auto itemSize = 9/lod;
        const auto minSize = 0.05;
        if(itemSize < minSize) {
            itemSize = minSize;
        }
        d->itemSize = itemSize;
    }

    auto ResizableItemHandler::SetCurrentPenWidth(const double& penWidth) -> void {
        d->offset = penWidth;
    }

    auto ResizableItemHandler::DrawHandlesIfNeeded() -> void {
        if (!d->ownerItem) return;

        if (d->ownerItem->isSelected()) {
            d->drawHandles(this);
            d->handlesAddedToScene = true;
        }
        else {
            ClearHandles();
        }
    }

    auto ResizableItemHandler::ClearHandles() -> void {
        for(const auto handleItem : d->handleList) {
            if (handleItem->scene()) {
                d->ownerItem->scene()->removeItem(handleItem);
            }
        }
        qDeleteAll(d->handleList);
        d->handleList.clear();
        d->handlesAddedToScene = false;
    }

    auto ResizableItemHandler::SetOwnerItem(QGraphicsItem* owner) -> void {
        d->ownerItem = owner;
    }
    
    auto ResizableItemHandler::Impl::drawHandles(const Self* self) -> void {
        //add handles in list
        if (handleList.count() == 0) {
            handleList.append(new ResizeHandleItem(ResizeHandleItem::TopLeft));
            handleList.append(new ResizeHandleItem(ResizeHandleItem::TopRight));
            handleList.append(new ResizeHandleItem(ResizeHandleItem::BottomLeft));
            handleList.append(new ResizeHandleItem(ResizeHandleItem::BottomRight));
            connect(self, &ResizableItemHandler::sigOwnerItemMove, handleList[0], &ResizeHandleItem::onOwnerItemMove);
            connect(self, &ResizableItemHandler::sigOwnerItemMove, handleList[1], &ResizeHandleItem::onOwnerItemMove);
            connect(self, &ResizableItemHandler::sigOwnerItemMove, handleList[2], &ResizeHandleItem::onOwnerItemMove);
            connect(self, &ResizableItemHandler::sigOwnerItemMove, handleList[3], &ResizeHandleItem::onOwnerItemMove);
        }

        //Top left = 0
        QPointF topLeftCorner = self->GetSelectorFrameBounds().topLeft() + QPointF(-(itemSize+offset)/2, -(itemSize+offset)/2);
        topleftHandleRect = QRectF(topLeftCorner, QSizeF(itemSize, itemSize));
        handleList[0]->setRect(topleftHandleRect);
        handleList[0]->SetLimits(self->GetSelectorFrameBounds().right(), self->GetSelectorFrameBounds().bottom());
        if (!handleList.isEmpty() && (!handlesAddedToScene)) {
            ownerItem->scene()->addItem(handleList[0]);
            handleList[0]->setParentItem(ownerItem);
        }

        //Top right = 1
        QPointF topRightCorner = self->GetSelectorFrameBounds().topRight() + QPointF(-(itemSize+offset)/2, -(itemSize+offset)/2);
        topRightHandleRect = QRectF(topRightCorner, QSizeF(itemSize, itemSize));
        handleList[1]->setRect(topRightHandleRect);
        handleList[1]->SetLimits(self->GetSelectorFrameBounds().left(), self->GetSelectorFrameBounds().bottom());
        if (!handleList.isEmpty() && (!handlesAddedToScene)) {
            ownerItem->scene()->addItem(handleList[1]);
            handleList[1]->setParentItem(ownerItem);
        }

        //Bottom left = 2
        QPointF bottomLeftCorner = self->GetSelectorFrameBounds().bottomLeft() + QPointF(-(itemSize+offset)/2, -(itemSize+offset)/2);
        bottomLeftHandleRect = QRectF(bottomLeftCorner, QSizeF(itemSize, itemSize));
        handleList[2]->setRect(bottomLeftHandleRect);
        handleList[2]->SetLimits(self->GetSelectorFrameBounds().right(), self->GetSelectorFrameBounds().top());
        if (!handleList.isEmpty() && (!handlesAddedToScene)) {
            ownerItem->scene()->addItem(handleList[2]);
            handleList[2]->setParentItem(ownerItem);
        }

        //Bottom right = 3
        QPointF bottomRightCorner = self->GetSelectorFrameBounds().bottomRight() + QPointF(-(itemSize+offset)/2, -(itemSize+offset)/2);
        bottomRightHandleRect = QRectF(bottomRightCorner, QSizeF(itemSize, itemSize));
        handleList[3]->setRect(bottomRightHandleRect);
        handleList[3]->SetLimits(self->GetSelectorFrameBounds().left(), self->GetSelectorFrameBounds().top());
        if (!handleList.isEmpty() && (!handlesAddedToScene)) {
            ownerItem->scene()->addItem(handleList[3]);
            handleList[3]->setParentItem(ownerItem);
        }

        handlesAddedToScene = true;
    }
}
