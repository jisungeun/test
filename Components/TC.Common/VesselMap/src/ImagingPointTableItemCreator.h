﻿#pragma once

#include <memory>

#include <QTableWidgetItem>

#include "ImagingPointTableDefine.h"
#include "ImagingPointTableData.h"

namespace TC {
    class ImagingTableItem;

    class ImagingPointTableItemCreator {
    public:
        using Self = ImagingPointTableItemCreator;
        using Pointer = std::shared_ptr<Self>;

        ImagingPointTableItemCreator();
        ~ImagingPointTableItemCreator();

        [[nodiscard]] auto CreateAcquisitionTableItem(ImagingTableHeader header, ImagingPointTableData::Pointer data) -> QTableWidgetItem*;

        static auto GetUmSizeText(double wInMM, double hInMM) -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class ImagingTableItem : public QTableWidgetItem {
    public:
        ImagingTableItem(QString value, bool readOnly = true) {
            this->setTextAlignment(Qt::AlignCenter);
            if (readOnly) {
                this->setFlags(this->flags() ^ Qt::ItemIsEditable);
            }
            this->setText(value);
        }

        ~ImagingTableItem() override = default;
        auto operator=(const ImagingTableItem& other) -> ImagingTableItem& = default;

        auto operator<(const QTableWidgetItem& other) const -> bool override {
            return text().toInt() < other.text().toInt();
        }
    };
}
