﻿#pragma once

#include <memory>
#include <QStringList>

#include "VesselMapExternalData.h"
#include "ImagingPointTableData.h"
#include "ImagingPointTableDefine.h"
#include "LocationDataRepo.h"
#include "VesselMapDataRepo.h"

namespace TC {
    class ImagingPointTableControl {
    public:
        using Self = ImagingPointTableControl;
        using Pointer = std::shared_ptr<Self>;

        ImagingPointTableControl();
        ~ImagingPointTableControl();

        auto Clear() -> void;

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto GetWellIndices() const -> QList<WellIndex>;

        auto GetColumnCount() const -> int32_t;
        auto GetColumnHeaderLabels() -> QStringList;

        auto SetCurrentWellIndex(WellIndex wellIndex) -> void;
        auto GetCurrentWellIndex() const -> WellIndex;

        auto GenerateAcquisitionTableData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, int32_t rowPointIndex) -> ImagingPointTableData::Pointer;
        auto GetCurrentWellAcquisitionIndices() const -> QList<AcquisitionIndex>;
        auto GetWellAcquisitionIndices(WellIndex wellIndex) const -> QList<AcquisitionIndex>;

        auto GetAcquisitionType(AcquisitionIndex acquisitionIndex) -> AcquisitionType;

        auto SetShowAll(bool showAll) -> void;
        auto IsShowAll() const -> bool;
        auto IsRoiExist() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
