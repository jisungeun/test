﻿#pragma once

#include <memory>

#include <QTableWidgetItem>

#include "WellGroupTableData.h"
#include "WellGroupTableDefine.h"

namespace TC {
    class GroupTableWidgetItem;
    class WellGroupTableData;

    class WellGroupTableItemCreator {
    public:
        using Self = WellGroupTableItemCreator;
        using Pointer = std::shared_ptr<Self>;

        WellGroupTableItemCreator();
        ~WellGroupTableItemCreator();

        [[nodiscard]] auto CreateTableWidgetItem(GroupTableHeader header, WellGroupTableData::Pointer tableData) -> QTableWidgetItem*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class GroupTableWidgetItem : public QTableWidgetItem {
    public:
        explicit GroupTableWidgetItem(bool readOnly = true) {
            this->setTextAlignment(Qt::AlignCenter);
            if (readOnly) {
                this->setFlags(this->flags() ^ Qt::ItemIsEditable);
            }
        }

        ~GroupTableWidgetItem() override = default;
        auto operator=(const GroupTableWidgetItem& other) -> GroupTableWidgetItem& = default;

        auto operator<(const QTableWidgetItem& other) const -> bool override {
            return text().toInt() < other.text().toInt();
        }
    };

}
