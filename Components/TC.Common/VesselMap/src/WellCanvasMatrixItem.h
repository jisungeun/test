﻿#pragma once
#include <QGraphicsItem>

#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasMatrixItem : public QGraphicsItem {
    public:
        using Self = WellCanvasMatrixItem;

        WellCanvasMatrixItem(AcquisitionType acqType, double x, double y, double width, double height, QGraphicsItem* parent = nullptr);
        ~WellCanvasMatrixItem() override;

        auto type() const -> int override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
