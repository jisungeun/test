﻿#include "WellCanvasItemCreator.h"
#include "VesselMapDataRepo.h"
#include "VesselMapUtil.h"
#include "WellCanvasMarkItem.h"
#include "WellCanvasWellItem.h"
#include "WellCanvasPreviewItem.h"
#include "WellCanvasAcquisitionItem.h"
#include "WellCanvasMatrixItem.h"
#include "WellCanvasLensItem.h"
#include "LocationDataRepo.h"

namespace TC {
    struct WellCanvasItemCreator::Impl {
        VesselMapDataRepo::Pointer repo{nullptr};
        LocationDataRepo::Pointer location{nullptr};
        VesselMapUtil::Pointer util{nullptr};
    };

    WellCanvasItemCreator::WellCanvasItemCreator() : d{std::make_unique<Impl>()} {
        d->util = std::make_shared<VesselMapUtil>();
    }

    WellCanvasItemCreator::~WellCanvasItemCreator() {
    }

    auto WellCanvasItemCreator::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->repo = vesselMapDataRepo;
        d->util->SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto WellCanvasItemCreator::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->location = locationDataRepo;
    }

    auto WellCanvasItemCreator::CreateLensItem() -> WellCanvasLensItem* {
        return new WellCanvasLensItem({0,0});
    }

    auto WellCanvasItemCreator::CreateTileImagingItem() -> WellCanvasTileImagingItem* {
        return new WellCanvasTileImagingItem();
    }
    
    auto WellCanvasItemCreator::CreatePreviewItem() -> WellCanvasPreviewItem* {
        return new WellCanvasPreviewItem();
    }

    auto WellCanvasItemCreator::CreateSelectedLocInfoItem() -> WellCanvasSelectedPointItem* {
        return new WellCanvasSelectedPointItem();
    }

    auto WellCanvasItemCreator::CreateWellCanvasItems(WellIndex wellIndex, bool supportContextMenu) -> QList<QGraphicsItem*> {
        const auto well = d->util->GetWellByWellIndex(wellIndex);
        if (well == nullptr) return {};

        QList<QGraphicsItem*> itemList;
        const auto wellItem = CreateWellItem(wellIndex);
        itemList.push_back(wellItem);

        for (const auto markIndex : d->location->GetMarkIndicesByWellIndex(wellIndex)) {
            const auto item = CreateMarkItem(wellIndex, markIndex);
            itemList.push_back(item);
        }

        for (const auto acquisitionIndex : d->location->GetAcquisitionIndicesByWellIndex(wellIndex)) {
            const auto item = CreateAcquisitionItem(wellIndex, acquisitionIndex, supportContextMenu);
            itemList.push_back(item);
        }

        return itemList;
    }

    auto WellCanvasItemCreator::CreateWellItem(WellIndex wellIndex) -> QGraphicsItem* {
        const auto well = d->util->GetWellByWellIndex(wellIndex);
        if (well == nullptr) return nullptr;

        const auto imagingArea = d->repo->GetImagingArea();
        auto imgX = 0.0;
        auto imgY = 0.0;
        auto imgW = 0.0;
        auto imgH = 0.0;
        auto imgShape = ImagingAreaShape::Circle;
        if (imagingArea != nullptr) {
            imgX = imagingArea->GetX();
            imgY = imagingArea->GetY();
            imgW = imagingArea->GetWidth();
            imgH = imagingArea->GetHeight();
            imgShape = imagingArea->GetShape();
        }
        const auto rois = d->repo->GetRois(well->GetIndex());
        const auto wellItem = new WellCanvasWellItem(wellIndex);
        wellItem->SetSize(well->GetWidth(), well->GetHeight());
        wellItem->SetWellShape(well->GetShape());
        wellItem->SetImagingArea(imgX, imgY, imgW, imgH);
        wellItem->SetImagingAreaShape(imgShape);
        if (!rois.isEmpty()) {
            wellItem->SetRois(rois);
        }
        return wellItem;
    }

    auto WellCanvasItemCreator::CreateMarkItem(WellIndex wellIndex, MarkIndex markIndex) -> QGraphicsItem* {
        const auto mark = d->location->GetMarkLocationByMarkIndex(wellIndex, markIndex);

        if (mark == nullptr) return nullptr;

        const Position3D pos = {mark->GetPosX(), mark->GetPosY(), mark->GetPosZ()};
        const auto tileSize = mark->GetSize();

        switch (mark->GetType()) {
            case MarkType::Point: {
                const auto item = new WellCanvasMarkItem(wellIndex, markIndex, pos);
                return item;
            }
            case MarkType::Tile: {
                const auto item = new WellCanvasMarkTileItem(wellIndex, markIndex, pos, tileSize);
                return item;
            }
            default: return nullptr;
        }
    }

    auto WellCanvasItemCreator::CreateAcquisitionItem(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, bool supportContextMenu) -> QGraphicsItem* {
        const auto acq = d->location->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);

        if (acq == nullptr) return nullptr;

        const Position3D pos = {acq->GetPosX(), acq->GetPosY(), acq->GetPosZ()};
        const auto tileSize = acq->GetSize();

        switch (acq->GetType()) {
            case AcquisitionType::Point: {
                const auto item = new WellCanvasAcquisitionItem(wellIndex, acquisitionIndex, pos);
                if(supportContextMenu) item->CreateContextMenu();
                return item;
            }
            case AcquisitionType::Tile: {
                const auto item = new WellCanvasAcquisitionTileItem(wellIndex, acquisitionIndex, pos, tileSize);
                if(supportContextMenu) item->CreateContextMenu();
                return item;
            }
            default: return nullptr;
        }
    }

    auto WellCanvasItemCreator::CreateMatrixItem(AcquisitionType type, double x, double y, double width, double height) -> QGraphicsItem* {
        return new WellCanvasMatrixItem(type, x, y, width, height);
    }
}
