﻿#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>

#include "VesselCanvasWellItem.h"
#include "GraphicsItemDefine.h"
#include "DefaultSettingHelper.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct VesselCanvasWellItem::Impl {
        int32_t txtSizeRatioByMode{3};

        WellIndex index{kInvalid};
        WellShape shape{WellShape::Circle};
        double width{};
        double height{};
        int32_t row{};
        int32_t column{};

        QString name{};
        QString posName{};
        QColor color{DefaultWellColor};
        GroupIndex groupIndex{kInvalid};

        bool onLens{false};
        bool onWellCanvas{false};
        bool showImagingArea{false};
        bool showName{true};
        bool mousePressIgnore{false};

        ImagingAreaShape imgShape{ImagingAreaShape::Circle};
        Geometry2D imgGeo;

        bool showOrder{};
        int32_t imagingOrder{};

        Roi::List rois{};

        auto GetDrawRect() const -> QRectF;
        auto GetDrawImagingRect() const -> QRectF;
        auto DrawWellShape(QPainter* painter) -> void;
        auto DrawImagingAreaShape(QPainter* painter) -> void;
        auto DrawText(QPainter* painter) -> void;
        auto DrawFocusIndicator(QPainter* painter) -> void;
        auto FitTextToWellWidth(QFont& font, const QString& displayText) const -> void;
        auto UpdateToolTip(Self* self) -> void;
        auto DrawImagingOrder(QPainter* painter) -> void;
        auto DrawRois(QPainter* painter) -> void;
    };

    VesselCanvasWellItem::VesselCanvasWellItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
        setAcceptHoverEvents(true);
        setCacheMode(DeviceCoordinateCache);
    }

    VesselCanvasWellItem::~VesselCanvasWellItem() {
    }

    auto VesselCanvasWellItem::SetViewMode(ViewMode mode) -> void {
        switch (mode) {
            case ViewMode::PerformMode: {
                setFlag(ItemIsSelectable, true);
                d->showName = false;
                break;
            } 
            case ViewMode::TimelapseMode: {
                setFlag(ItemIsSelectable, true);
                d->showName = false;
                break;
            }
            case ViewMode::SetupMode: {
                setFlag(ItemIsSelectable, true);
                d->mousePressIgnore = true;
                d->txtSizeRatioByMode = 5;
                break;
            }
            case ViewMode::DataNaviMode: {
                setFlag(ItemIsSelectable, false);
                d->txtSizeRatioByMode = 5;
                break;
            }
            case ViewMode::InfoMode: {
                setFlag(ItemIsSelectable, false);
                d->showName = false;
                break;
            }
            case ViewMode::PreviewMode: {
                d->showImagingArea = true;
                d->txtSizeRatioByMode = 5;
                break;
            }
            case ViewMode::CopyDlgMode: {
                setFlag(ItemIsSelectable, true);
                d->mousePressIgnore = true;
                d->showName = false;
            }
            default: break;
        }
    }

    auto VesselCanvasWellItem::SetIndex(WellIndex index) -> void {
        d->index = index;
    }

    auto VesselCanvasWellItem::GetIndex() const -> WellIndex {
        return d->index;
    }

    auto VesselCanvasWellItem::SetShape(WellShape shape) -> void {
        d->shape = shape;
    }

    auto VesselCanvasWellItem::GetShape() const -> WellShape {
        return d->shape;
    }

    auto VesselCanvasWellItem::SetWidth(double w) -> void {
        d->width = w;
    }

    auto VesselCanvasWellItem::GetWidth() const -> double {
        return d->width;
    }

    auto VesselCanvasWellItem::SetHeight(double h) -> void {
        d->height = h;
    }

    auto VesselCanvasWellItem::GetHeight() const -> double {
        return d->height;
    }

    auto VesselCanvasWellItem::SetX(double x) -> void {
        setX(x);
    }

    auto VesselCanvasWellItem::GetX() const -> double {
        return x();
    }

    auto VesselCanvasWellItem::SetY(double y) -> void {
        setY(-y);
    }

    auto VesselCanvasWellItem::GetY() const -> double {
        return -y();
    }

    auto VesselCanvasWellItem::SetRowColumn(int32_t row, int32_t col) -> void {
        d->row = row;
        d->column = col;
        d->posName = DefaultSettingHelper::MakeWellPositionName(row, col);
        d->UpdateToolTip(this);
    }

    auto VesselCanvasWellItem::GetRow() const -> int32_t {
        return d->row;
    }

    auto VesselCanvasWellItem::GetColumn() const -> int32_t {
        return d->column;
    }

    auto VesselCanvasWellItem::SetName(const QString& name) -> void {
        d->name = name;
        d->UpdateToolTip(this);
    }

    auto VesselCanvasWellItem::GetName() -> QString {
        return d->name;
    }

    auto VesselCanvasWellItem::SetColor(const QColor& color) -> void {
        d->color = color;
    }

    auto VesselCanvasWellItem::GetColor() -> QColor {
        return d->color;
    }

    auto VesselCanvasWellItem::SetGroupIndex(const GroupIndex& groupIndex) -> void {
        d->groupIndex = groupIndex;
    }

    auto VesselCanvasWellItem::GetGroupIndex() const -> GroupIndex {
        return d->groupIndex;
    }

    auto VesselCanvasWellItem::LeaveGroup() -> void {
        d->name = d->posName;
        d->color = DefaultWellColor;
        d->groupIndex = kInvalid;
        d->UpdateToolTip(this);
    }

    auto VesselCanvasWellItem::SetLensFocus(bool onLens) -> void {
        d->onLens = onLens;
        update();
    }

    auto VesselCanvasWellItem::SetShowOnWellCanvas(bool onWellCanvas) -> void {
        d->onWellCanvas = onWellCanvas;
        update();
    }

    auto VesselCanvasWellItem::SetImagingArea(ImagingAreaShape shape, double x, double y, double w, double h) -> void {
        d->imgShape = shape;
        d->imgGeo = {x, -y, w, h};
    }

    auto VesselCanvasWellItem::ShowImagingOrder(bool show) -> void {
        d->showOrder = show;
        update();
    }

    auto VesselCanvasWellItem::SetImagingOrder(int32_t order) -> void {
        d->imagingOrder = order;
        update();
    }

    auto VesselCanvasWellItem::SetRois(const Roi::List& rois) -> void {
        d->rois = rois;
    }

    auto VesselCanvasWellItem::type() const -> int32_t {
        return static_cast<int32_t>(GraphicsItemType::WellItem);
    }

    auto VesselCanvasWellItem::shape() const -> QPainterPath {
        QPainterPath path;
        switch (d->shape) {
            case WellShape::Circle: path.addEllipse(d->GetDrawRect());
                break;
            case WellShape::Rectangle: path.addRect(d->GetDrawRect());
                break;
        }
        return path;
    }

    auto VesselCanvasWellItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->width, d->height);
    }

    auto VesselCanvasWellItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);

        auto wellBrush = QBrush(d->color, Qt::SolidPattern);
        auto wellPen = QPen(Qt::NoPen);
        if (option->state & QStyle::State_Selected) {
            wellBrush.setColor(wellBrush.color().darker());
        }

        if (d->onWellCanvas) {
            wellBrush.setColor(wellBrush.color().darker());

            wellPen.setBrush(WellBorderColor);
            wellPen.setStyle(Qt::DotLine);
            wellPen.setWidthF(1);
            //d->DrawFocusIndicator(painter); // currently not in use
        }

        painter->save();
        painter->setPen(wellPen);
        painter->setBrush(wellBrush);
        d->DrawWellShape(painter);
        painter->restore();

        if (d->showName && d->groupIndex != kInvalid) {
            d->DrawText(painter);
        }
        if (d->showImagingArea) {
            d->DrawImagingAreaShape(painter);
        }
        if(d->showOrder && d->groupIndex != kInvalid) {
            d->DrawImagingOrder(painter);
        }
        if(!d->rois.isEmpty()) {
            d->DrawRois(painter);
        }
    }

    auto VesselCanvasWellItem::mousePressEvent(QGraphicsSceneMouseEvent* event) -> void {
        if(d->mousePressIgnore) {
            event->ignore();
        }
        QGraphicsItem::mousePressEvent(event);
    }
    
    auto VesselCanvasWellItem::Impl::GetDrawRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(width, height);
    }

    auto VesselCanvasWellItem::Impl::GetDrawImagingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(imgGeo.x, imgGeo.y, imgGeo.w, imgGeo.h);
    }

    auto VesselCanvasWellItem::Impl::DrawWellShape(QPainter* painter) -> void {
        switch (shape) {
            case WellShape::Circle: {
                painter->drawEllipse(GetDrawRect());
                break;
            }
            case WellShape::Rectangle: {
                painter->drawRect(GetDrawRect());
                break;
            }
        }
    }

    auto VesselCanvasWellItem::Impl::DrawImagingAreaShape(QPainter* painter) -> void {
        painter->setPen(QPen(ImagingAreaBorderColor, 0));
        painter->setBrush(ImagingAreaCenterColor);
        switch (imgShape) {
            case ImagingAreaShape::Circle: {
                painter->drawEllipse(GetDrawImagingRect());
                break;
            }
            case ImagingAreaShape::Rectangle: {
                painter->drawRect(GetDrawImagingRect());
                break;
            }
        }
    }

    auto VesselCanvasWellItem::Impl::DrawText(QPainter* painter) -> void {
        QFont newFont = painter->font();
        QTextOption textOption;

        newFont.setBold(true);

        textOption.setAlignment(Qt::AlignCenter);
        textOption.setWrapMode(QTextOption::WordWrap);

        QString displayText = name;

        FitTextToWellWidth(newFont, displayText);

        painter->save();
        painter->setPen(Qt::white);
        painter->setFont(newFont);
        painter->drawText(GetDrawRect(), displayText, textOption);
        painter->restore();
    }

    auto VesselCanvasWellItem::Impl::DrawFocusIndicator(QPainter* painter) -> void {
        painter->save();
        painter->setBrush(FocusIndicatorColor);
        painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(width / 4, height / 4));
        painter->restore();
    }

    auto VesselCanvasWellItem::Impl::FitTextToWellWidth(QFont& font, const QString& displayText) const -> void {
        // TODO space없이 길이가 well size보다 길 경우 2nd parameter displayText 튜닝필요
        if (displayText.isEmpty()) return;

        QFontMetrics fm(font);
        font.setPointSizeF(width * 0.14 * 0.75);
    }

    auto VesselCanvasWellItem::Impl::UpdateToolTip(Self* self) -> void {
        if (name.isEmpty()) {
            self->setToolTip(posName);
        }
        else {
            self->setToolTip(posName + "(" + name + ")");
        }
    }

    auto VesselCanvasWellItem::Impl::DrawImagingOrder(QPainter* painter) -> void {
        QFont newFont = painter->font();
        QTextOption textOption;

        newFont.setBold(true);

        textOption.setAlignment(Qt::AlignCenter);
        textOption.setWrapMode(QTextOption::WordWrap);

        painter->save();
        painter->setFont(newFont);
        painter->drawText(GetDrawRect(), QString::number(imagingOrder), textOption);
        painter->restore();
    }

    auto VesselCanvasWellItem::Impl::DrawRois(QPainter* painter) -> void {
        painter->save();
        painter->setPen(Qt::NoPen);
        auto brushColor = RoiColor;
        brushColor.setAlpha(150);
        painter->setBrush(brushColor);
        for(const auto& roi : rois) {
            const auto roiShape = roi->GetShape();
            const auto roiName = roi->GetName();
            const auto roiIndex = roi->GetIndex();
            if(roiShape == +RoiShape::Rectangle) {
                painter->drawRect(DefaultSettingHelper::GetBoundingRect(roi->GetX(), -roi->GetY(), roi->GetWidth(), roi->GetHeight()));
            }
            else {
                painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(roi->GetX(), -roi->GetY(), roi->GetWidth(), roi->GetHeight()));
            }
        }
        painter->restore();
    }
}
