﻿#include "WellGroupTableData.h"

namespace TC {
    struct WellGroupTableData::Impl {
        GroupIndex groupIndex;
        QString groupName;
        WellIndex wellIndex;
        QString wellName;
        QString wellPosition;
        int32_t imgPointCount;
        QColor groupColor;
    };

    WellGroupTableData::WellGroupTableData() : d{std::make_unique<Impl>()} {
    }

    WellGroupTableData::WellGroupTableData(const WellGroupTableData& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    WellGroupTableData::~WellGroupTableData() {
    }

    auto WellGroupTableData::operator=(const WellGroupTableData& other) -> WellGroupTableData& {
        *d = *other.d;
        return *this;
    }

    auto WellGroupTableData::SetGroupIndex(GroupIndex index) -> void {
        d->groupIndex = index;
    }

    auto WellGroupTableData::GetGroupIndex() const -> GroupIndex {
        return d->groupIndex;
    }

    auto WellGroupTableData::SetGroupName(const QString& name) -> void {
        d->groupName = name;
    }

    auto WellGroupTableData::GetGroupName() const -> QString {
        return d->groupName;
    }

    auto WellGroupTableData::SetGroupColor(const QColor& color) -> void {
        d->groupColor = color;
    }

    auto WellGroupTableData::GetGroupColor() const -> QColor {
        return d->groupColor;
    }

    auto WellGroupTableData::SetWellIndex(WellIndex index) -> void {
        d->wellIndex = index;
    }

    auto WellGroupTableData::GetWellIndex() const -> WellIndex {
        return d->wellIndex;
    }

    auto WellGroupTableData::SetWellName(const QString& name) -> void {
        d->wellName = name;
    }

    auto WellGroupTableData::GetWellName() const -> QString {
        return d->wellName;
    }

    auto WellGroupTableData::SetWellPosition(const QString& position) -> void {
        d->wellPosition = position;
    }

    auto WellGroupTableData::GetWellPosition() const -> QString {
        return d->wellPosition;
    }

    auto WellGroupTableData::SetWellImgPointCount(int32_t count) -> void {
        d->imgPointCount = count;
    }

    auto WellGroupTableData::GetWellImgPointCount() const -> int32_t {
        return d->imgPointCount;
    }
}
