﻿#include <QStyleOptionGraphicsItem>
#include <QPainter>

#include "WellCanvasMatrixItem.h"

#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct WellCanvasMatrixItem::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};

        double pointSize{};
        double tileWidth{};
        double tileheight{};
        QColor pointColor{MatrixPointColor};
        AcquisitionType::_enumerated acqType{};

        auto UpdatePointSize(const double& lod) -> void;
    };

    auto WellCanvasMatrixItem::Impl::UpdatePointSize(const double& lod) -> void {
        pointSize = 10 / lod;
        if (pointSize < kMinLocPointSize) {
            pointSize = kMinLocPointSize;
        }
        self->update();
    }

    WellCanvasMatrixItem::WellCanvasMatrixItem(AcquisitionType acqType, double x, double y, double width, double height, QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>(this)} {
        if (acqType == +AcquisitionType::Tile) {
            d->tileWidth = width;
            d->tileheight = height;
        }
        d->acqType = acqType;

        setPos(x, -y);

        setFlag(ItemIsSelectable, false);
        setCacheMode(DeviceCoordinateCache);
        setAcceptHoverEvents(true);
        setZValue(GraphicsItemZValue::Matrix);
    }

    WellCanvasMatrixItem::~WellCanvasMatrixItem() {
    }

    auto WellCanvasMatrixItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::MatrixItem);
    }

    auto WellCanvasMatrixItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        Q_UNUSED(option)

        d->UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        const auto brush = d->pointColor;
        const auto pointPen = QPen(Qt::NoPen);
        painter->setPen(pointPen);
        painter->setBrush(brush);

        if (d->acqType == AcquisitionType::Point) {
            painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize));
        } else if (d->acqType == AcquisitionType::Tile) {
            if (d->tileWidth > d->pointSize && d->tileheight > d->pointSize) {
                painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize));
            }

            painter->save();
            const QPen tilePen(MatrixPointColor, 0, Qt::SolidLine);
            const QColor brushColor = Qt::transparent;

            painter->setPen(tilePen);
            const auto rect = DefaultSettingHelper::GetBoundingRect(d->tileWidth, d->tileheight);
            QPainterPath path;
            painter->setBrush(QBrush(brushColor, Qt::SolidPattern));
            path.addRect(rect);
            painter->drawPath(path);
            painter->restore();
        }
    }

    QPainterPath WellCanvasMatrixItem::shape() const {
        QPainterPath path;
        if (d->acqType == AcquisitionType::Tile) {
            path.addRect(boundingRect());
            return path;
        }
        path.addEllipse(boundingRect());
        return path;
    }

    auto WellCanvasMatrixItem::boundingRect() const -> QRectF {
        if (d->acqType == AcquisitionType::Tile) {
            return DefaultSettingHelper::GetBoundingRect(d->tileWidth, d->tileheight);
        }

        return DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize);
    }
}
