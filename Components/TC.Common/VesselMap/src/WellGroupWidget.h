﻿#pragma once

#include <QTableWidget>

#include "LocationDataRepo.h"
#include "VesselMapDataRepo.h"
#include "WellGroupTableData.h"

namespace TC {
    class WellGroupWidget : public QTableWidget {
        Q_OBJECT
	public:
        using Self = WellGroupWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit WellGroupWidget(QWidget* parent = nullptr);
        ~WellGroupWidget() override;

        auto ClearAll() -> void;
        auto ClearTable() -> void;

        auto SetViewMode(ViewMode mode) -> void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto AddWellsToTable(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto DeleteGroup(const GroupIndex& groupIndex) -> void;
        auto RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void;
        auto ChangeGroupName(const GroupIndex& groupIndex, const QString& name) -> void;
        auto ChangeGroupColor(const GroupIndex& groupIndex, const QColor& color) -> void;
        auto ChangeWellName(const WellIndex& wellIndex, const QString& name) -> void;
        auto MoveGroup(const GroupIndex& movingGroupIndex, const GroupIndex& targetGroupIndex) -> void;

        auto UpdateAcquisitionCount() -> void;

    protected:
        auto viewportEvent(QEvent*) -> bool override;

    signals:
        void sigChangeWellName(WellIndex wellIndex, const QString& name);
        void sigChangeGroupName(GroupIndex groupIndex, const QString& name);
        void sigChangeGroupColor(GroupIndex groupIndex, const QColor& color);
        void sigCurrentSelectedWellIndces(const QList<WellIndex>&);

    public slots:
        void onChangeSelectedWellIndexOnVesselCanvas(const QList<WellIndex>& wellIndices);

    private slots:
        void onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles = QVector<int>());
        void onItemDoubleClicked(QTableWidgetItem* item);
        void onItemSelectionChanged();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
