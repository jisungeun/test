﻿#pragma once

#include <enum.h>

namespace TC {
    BETTER_ENUM(GroupTableHeader, int32_t, 
                GroupName = 0, 
                WellName, 
                WellPosition, 
                ImgPointCount, 
                GroupColor, 
                GroupIndex, 
                WellIndex);
}
