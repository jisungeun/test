﻿#pragma once

#include <memory>

#include <QList>

#include "Holder.h"
#include "ImagingArea.h"
#include "Vessel.h"
#include "WellGroup.h"
#include "Well.h"
#include "Roi.h"

namespace TC {
    class VesselMapDataRepo {
    public:
        using Self = VesselMapDataRepo;
        using Pointer = std::shared_ptr<Self>;

        VesselMapDataRepo();
        ~VesselMapDataRepo();

        auto ClearAll() -> void;

        auto SetHolder(const Holder::Pointer& holder) -> void;
        auto AddVessel(const Vessel::Pointer& vessel) -> void;
        auto AddWell(const Well::Pointer& well) -> void;
        auto SetImagingArea(const ImagingArea::Pointer& imagingArea) -> void;

        auto GetHolder() const -> Holder::Pointer&;
        auto GetVessels() const -> QList<Vessel::Pointer>&;
        auto GetWells() const -> QList<Well::Pointer>&;
        auto GetImagingArea() const -> ImagingArea::Pointer&;

        auto GetWellGroups() const -> QList<WellGroup::Pointer>&;
        auto AddWellGroup(const WellGroup::Pointer& group) -> void;
        auto DeleteWellGroup(const WellGroup::Pointer& group) -> void;

        auto SetRois(const Roi::Map& rois) -> void;
        auto GetRois(const WellIndex& wellIndex) const -> Roi::List;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
