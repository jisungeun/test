﻿#pragma once

#include <memory>
#include <QString>

#include "VesselMapExternalData.h"

namespace TC {
    class Well {
    public:
        using Self = Well;
        using Pointer = std::shared_ptr<Self>;

        Well();
        Well(const Well& other);
        ~Well();

        auto operator=(const Well& other) -> Well&;

        auto SetIndex(WellIndex index) -> void;
        auto GetIndex() const -> WellIndex;

        auto SetShape(WellShape shape) -> void;
        auto GetShape() const -> WellShape;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetX(double x) -> void;
        auto GetX() const -> double;
        auto SetY(double y) -> void;
        auto GetY() const -> double;

        auto SetWidth(double width) -> void;
        auto GetWidth() const -> double;
        auto SetHeight(double height) -> void;
        auto GetHeight() const -> double;

        auto SetRow(int32_t row) -> void;
        auto GetRow() const -> int32_t;
        auto SetColumn(int32_t column) -> void;
        auto GetColumn() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
