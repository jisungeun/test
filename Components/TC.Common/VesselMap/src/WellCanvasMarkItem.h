﻿#pragma once

#include <memory>

#include <QObject>
#include <QGraphicsItem>

#include "VesselMapCustomDataTypes.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasMarkItem : public QObject, public QGraphicsItem {
        Q_OBJECT
        Q_INTERFACES(QGraphicsItem)
    public:
        using Self = WellCanvasMarkItem;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvasMarkItem(WellIndex wellIndex, MarkIndex markIndex, Position3D pos, QGraphicsItem* parent = nullptr);
        ~WellCanvasMarkItem() override;

        auto GetWellIndex() const -> WellIndex;
        auto GetMarkIndex() const -> MarkIndex;

        auto SetPosition(double x, double y, double z) -> void;

    protected:
        auto type() const -> int override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;

        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;

        auto GetPointColor() const -> QColor;
        auto GetSelectedColor() const -> QColor;
        auto UpdatePointSize(const double& lod) -> void;
        auto GetPointSize() const -> double;

    signals:
        void sigMarkItemDoubleClicked(const WellIndex&, const MarkIndex&, double x, double y);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class WellCanvasMarkTileItem : public WellCanvasMarkItem {
        Q_OBJECT
    public:
        using Self = WellCanvasMarkTileItem;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvasMarkTileItem(WellIndex wellIndex, MarkIndex markIndex, Position3D pos, Size2D tileSize, QGraphicsItem* parent = nullptr);
        ~WellCanvasMarkTileItem() override;

        auto SetTileSize(double w, double h) -> void;

    protected:
        auto type() const -> int override;
        auto boundingRect() const -> QRectF override;
        auto shape() const -> QPainterPath override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
