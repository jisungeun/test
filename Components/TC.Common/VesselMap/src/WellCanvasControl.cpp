﻿#include <QGraphicsView>
#include <QPainterPath>

#include "WellCanvasControl.h"
#include "VesselMapDataRepo.h"
#include "VesselMapUtil.h"
#include "DefaultSettingHelper.h"
#include "VesselMapCustomDataTypes.h"
#include "LocationDataRepo.h"

namespace TC {
    struct WellCanvasControl::Impl {
        VesselMapDataRepo::Pointer repo{nullptr};
        LocationDataRepo::Pointer location{nullptr};
        VesselMapUtil::Pointer util{nullptr};

        QRectF sceneRect{};
        QGraphicsView::DragMode dragMode{QGraphicsView::RubberBandDrag};
        bool tracking{true};
        Position2D lensPos{};

        WellIndex currentWellIndex{kInvalid};
    };

    WellCanvasControl::WellCanvasControl() : d{std::make_unique<Impl>()} {
        d->util = std::make_shared<VesselMapUtil>();
    }

    WellCanvasControl::~WellCanvasControl() {
    }

    auto WellCanvasControl::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->repo = vesselMapDataRepo;
        d->util->SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto WellCanvasControl::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->location = locationDataRepo;
    }

    auto WellCanvasControl::ClearData() -> void {
        d->currentWellIndex = kInvalid;
    }

    auto WellCanvasControl::SetCurrentWellIndex(WellIndex wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto WellCanvasControl::GetCurrentWellIndex() const -> WellIndex {
        return d->currentWellIndex;
    }

    auto WellCanvasControl::SetCanvasMode(EditMode mode) -> void {
        switch (mode) {
            case EditMode::Editable: {
                d->dragMode = QGraphicsView::RubberBandDrag;
                break;
            }
            case EditMode::ReadOnly: {
                d->dragMode = QGraphicsView::NoDrag;
                break;
            }
        }
    }

    auto WellCanvasControl::GetCanvasDragMode() const -> QGraphicsView::DragMode {
        return d->dragMode;
    }

    auto WellCanvasControl::GetCanvasTracking() const -> bool {
        return d->tracking;
    }

    auto WellCanvasControl::GetSceneRect(WellIndex wellIndex) const -> QRectF {
        auto x = 0.0;
        auto y = 0.0;
        auto w = 0.0;
        auto h = 0.0;

        const auto well = d->util->GetWellByWellIndex(wellIndex);
        if (well != nullptr) {
            w = well->GetWidth();
            h = well->GetHeight();
        }

        const auto imgArea = d->repo->GetImagingArea();
        if (imgArea != nullptr) {
            x = d->repo->GetImagingArea()->GetX();
            y = -d->repo->GetImagingArea()->GetY();
            w = d->repo->GetImagingArea()->GetWidth();
            h = d->repo->GetImagingArea()->GetHeight();
        }

        return DefaultSettingHelper::GetBoundingRect(x, y, w, h);
    }

    auto WellCanvasControl::SetLensPosition(double x, double y) -> void {
        const auto wellPos = d->util->ConvertLensPosToWellPos(d->currentWellIndex, {x, y});
        d->lensPos = wellPos;
    }

    auto WellCanvasControl::GetLensPosX() const -> double {
        return d->lensPos.x;
    }

    auto WellCanvasControl::GetLensPosY() const -> double {
        return d->lensPos.y;
    }

    auto WellCanvasControl::GetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) const -> LocationMark::Pointer {
        return d->location->GetMarkLocationByMarkIndex(wellIndex, markIndex);
    }

    auto WellCanvasControl::GetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) const -> LocationAcquisition::Pointer {
        return d->location->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);
    }

    auto WellCanvasControl::GetImagingAreaCenterX() const -> double {
        if(!d->repo->GetImagingArea()) {
            return 0.0;
        }

        return d->repo->GetImagingArea()->GetX();
    }

    auto WellCanvasControl::GetImagingAreaCenterY() const -> double {
        if(!d->repo->GetImagingArea()) {
            return 0.0;
        }

        return d->repo->GetImagingArea()->GetY();
    }

    auto WellCanvasControl::IsAvailablePreviewArea(const QRectF& previewAreaRect) const -> bool {
        const auto imagingArea = d->repo->GetImagingArea();

        if(!imagingArea) {
            return false;
        }

        QPainterPath imagingAreaPainterPath{};
        const auto imagingAreaBoundingRect = DefaultSettingHelper::GetBoundingRect(imagingArea->GetX(), 
                                                                                   imagingArea->GetY(),
                                                                                   imagingArea->GetWidth(), 
                                                                                   imagingArea->GetHeight());
        const auto previewBoundingRect = DefaultSettingHelper::GetBoundingRect(previewAreaRect.x(),
                                                                               previewAreaRect.y(),
                                                                               previewAreaRect.width(),
                                                                               previewAreaRect.height());

        imagingAreaPainterPath.addRect(imagingAreaBoundingRect);

        return imagingAreaPainterPath.contains(previewBoundingRect);
    }

    auto WellCanvasControl::GetAcquisitionLocationIndices(WellIndex wellIndex) const -> QList<AcquisitionIndex> {
        return d->location->GetAcquisitionIndicesByWellIndex(wellIndex);
    }
}
