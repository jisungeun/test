﻿#pragma once

#include <memory>
#include <QGraphicsEllipseItem>
#include <QObject>

namespace TC {
    class ResizeHandleItem : public QObject, public QGraphicsEllipseItem {
        Q_OBJECT
    public:
        using Self = ResizeHandleItem;

        enum Position {
            TopLeft = 0,
            TopRight,
            BottomLeft,
            BottomRight
        };

        explicit ResizeHandleItem(Position position, QGraphicsItem* parent = nullptr);

        auto SetLimits(double x, double y) -> void;

        auto HandlePosition() const -> Position;
        auto IsMoveAvailable() const -> bool;

    protected:
        auto mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void override;
        auto hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void override;

    public slots:
        void onOwnerItemMove(const QPointF& lastPos, const QPointF& currPos);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };


}
