﻿#define LOGGER_TAG "[VesselMapWidgetControl]"

#include <TCLogger.h>
#include <MessageDialog.h>

#include "VesselMapWidgetControl.h"
#include "VesselMapDataRepo.h"
#include "Holder.h"
#include "Vessel.h"
#include "Well.h"
#include "ImagingArea.h"
#include "WellGroup.h"
#include "DefaultSettingHelper.h"
#include "VesselMapUtil.h"
#include "LocationDataRepo.h"

namespace TC {
    struct VesselMapWidgetControl::Impl {
        VesselMapDataRepo::Pointer repo{nullptr};
        LocationDataRepo::Pointer location{nullptr};
        VesselMapUtil::Pointer util{nullptr};

        Position3D lensPos{0.0, 0.0, 0.0};
        VesselIndex currentVesselIndex{kInvalid};
        WellIndex currentWellIndex{kInvalid};

        ImagingOrder imagingOrder{ImagingOrder::ByWell};
        
        struct {
            double xMM{};
            double yMM{};
        } previewPresetSize{0.6, 0.6};

        auto DeleteGroupHasNoMember(WellGroup::Pointer group) -> void;

        auto WellsByWellIndex() -> QList<WellIndex>;
        auto WellsBySpecimen() -> QList<WellIndex>;
    };

    VesselMapWidgetControl::VesselMapWidgetControl() : d{std::make_unique<Impl>()} {
        d->repo = std::make_shared<VesselMapDataRepo>();
        d->util = std::make_shared<VesselMapUtil>();
        d->location = std::make_shared<LocationDataRepo>();
    }

    VesselMapWidgetControl::~VesselMapWidgetControl() {
    }

    auto VesselMapWidgetControl::ClearAll() -> void {
        // TODO Clear All Data
        d->repo->ClearAll();
        d->location->ClearAll();
        d->currentVesselIndex = kInvalid;
        d->currentWellIndex = kInvalid;
        d->lensPos = {0.0, 0.0, 0.0};
    }

    auto VesselMapWidgetControl::Initialize() -> void {
        d->repo->ClearAll();
    }

    auto VesselMapWidgetControl::SetVesselMap(const VesselMap::Pointer& vesselMap) -> void {
        d->repo->SetHolder(vesselMap->GetHolder());

        for (auto vessel : vesselMap->GetVessels()) {
            d->repo->AddVessel(vessel);
        }

        for (auto w : vesselMap->GetWells()) {
            d->repo->AddWell(w);
        }

        d->repo->SetImagingArea(vesselMap->GetImagingArea());
        d->repo->SetRois(vesselMap->GetRoiList());

        d->util->SetVesselMapDataRepo(d->repo);
    }

    auto VesselMapWidgetControl::GetVesselMapDataRepo() const -> VesselMapDataRepo::Pointer {
        return d->repo;
    }

    auto VesselMapWidgetControl::GetLocationDataRepo() const -> LocationDataRepo::Pointer {
        return d->location;
    }

    auto VesselMapWidgetControl::GetWellIndex(int32_t row, int32_t column) const -> WellIndex {
        return d->util->GetWellIndexByRowColumn(row, column);
    }

    auto VesselMapWidgetControl::GetWellRow(WellIndex wellIndex) const -> int32_t {
        return d->util->GetWellRowByWellIndex(wellIndex);
    }

    auto VesselMapWidgetControl::GetWellColumn(WellIndex wellIndex) const -> int32_t {
        return d->util->GetWellColumnByWellIndex(wellIndex);
    }

    auto VesselMapWidgetControl::SetCurrentWellIndex(WellIndex wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto VesselMapWidgetControl::GetCurrentWellIndex() const -> WellIndex {
        return d->currentWellIndex;
    }

    auto VesselMapWidgetControl::SetLensPosition(double x, double y, double z) -> void {
        d->lensPos = {x, y, z};
    }

    auto VesselMapWidgetControl::GetLensPosX() const -> double {
        return d->lensPos.x;
    }

    auto VesselMapWidgetControl::GetLensPosY() const -> double {
        return d->lensPos.y;
    }

    auto VesselMapWidgetControl::GetLensPosZ() const -> double {
        return d->lensPos.z;
    }

    auto VesselMapWidgetControl::GetWellPos2DByGlobalLensPos() const -> Position2D {
        const auto convertPos2D = d->util->ConvertLensPosToWellPos(d->currentWellIndex, {d->lensPos.x, d->lensPos.y});
        return convertPos2D;
    }

    auto VesselMapWidgetControl::GetHolder() const -> std::shared_ptr<Holder> {
        return d->repo->GetHolder();
    }

    auto VesselMapWidgetControl::GetVessels() const -> QList<std::shared_ptr<Vessel>> {
        return d->repo->GetVessels();
    }

    auto VesselMapWidgetControl::GetWells() const -> QList<std::shared_ptr<Well>> {
        return d->repo->GetWells();
    }

    auto VesselMapWidgetControl::GetImagingArea() const -> std::shared_ptr<ImagingArea> {
        return d->repo->GetImagingArea();
    }

    auto VesselMapWidgetControl::GetGroups() const -> QList<std::shared_ptr<WellGroup>> {
        return d->repo->GetWellGroups();
    }

    auto VesselMapWidgetControl::GetGroupIndex(const QString& groupName) const -> GroupIndex {
        return d->util->GetGroupByGroupName(groupName)->GetIndex();
    }

    auto VesselMapWidgetControl::IsExistGroup(const QString& groupName) const -> bool {
        return d->util->GetGroupByGroupName(groupName) != nullptr;
    }

    auto VesselMapWidgetControl::CreateNewGroup(GroupIndex groupIndex, const QString& groupName, const QColor& groupColor, const QList<int>& wellIndices) -> bool {
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);

        if (group != nullptr) {
            QLOG_ERROR() << QString("Failed to create well group! Index [%1] is already in use").arg(groupIndex); 
            MessageDialog::warning(nullptr, tr("Group index error"), tr("This Group index [%1] is in use.").arg(groupIndex));
            return false;
        }

        if (wellIndices.isEmpty() == false) {
            const auto newGroup = std::make_shared<WellGroup>();
            newGroup->SetIndex(groupIndex);
            newGroup->SetName(groupName);
            newGroup->SetColor(groupColor);
            newGroup->SetWells(wellIndices);
            d->repo->AddWellGroup(newGroup);

            return true;
        }
        return false;
    }

    auto VesselMapWidgetControl::DeleteGroup(GroupIndex groupIndex) -> bool {
        auto group = d->util->GetGroupByGroupIndex(groupIndex);
        if (group == nullptr) {
            return false;
        }
        d->repo->DeleteWellGroup(group);
        return true;
    }

    auto VesselMapWidgetControl::GetWellsNotInGroup(const QList<WellIndex>& wellIndices) -> QList<WellIndex> {
        QList<WellIndex> wells;
        for (const auto wellIndex : wellIndices) {
            if (false == d->util->IsWellAlreadyHasGroup(wellIndex)) {
                wells.push_back(wellIndex);
            }
        }
        return wells;
    }

    auto VesselMapWidgetControl::GetWellsInGroup(const QList<WellIndex>& wellIndices) -> QList<WellIndex> {
        QList<WellIndex> wells;
        for (const auto wellIndex : wellIndices) {
            if (true == d->util->IsWellAlreadyHasGroup(wellIndex)) {
                wells.push_back(wellIndex);
            }
        }
        return wells;
    }


    auto VesselMapWidgetControl::AddWellsToGroup(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> bool {
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);
        if ((wellIndices.isEmpty() == false) && (group != nullptr)) {
            group->AddWells(wellIndices);
            return true;
        }

        return false;
    }

    auto VesselMapWidgetControl::RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> bool {
        if (wellIndices.isEmpty()) return false;

        WellGroup::Pointer group = nullptr;
        for (const auto wellIndex : wellIndices) {
            group = d->util->GetGroupByWellIndex(wellIndex);
            if (group == nullptr) {
                return false;
            }
            group->DeleteWell(wellIndex);

            // group에 well이 1개도 없을 때 그룹자체를 삭제
            if (group->GetWells().isEmpty()) {
                d->DeleteGroupHasNoMember(group);
            }
        }

        return true;
    }

    auto VesselMapWidgetControl::ChangeGroupName(GroupIndex groupIndex, const QString& name) -> void {
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);
        if (group != nullptr) group->SetName(name);
    }

    auto VesselMapWidgetControl::ChangeGroupColor(GroupIndex groupIndex, const QColor& color) -> void {
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);
        if (group != nullptr) group->SetColor(color);
    }

    auto VesselMapWidgetControl::ChangeWellName(WellIndex wellIndex, const QString& name) -> void {
        const auto well = d->util->GetWellByWellIndex(wellIndex);
        if (well != nullptr) well->SetName(name);
    }

    auto VesselMapWidgetControl::MoveGroup(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void {
        auto movingGroup = d->util->GetGroupByGroupIndex(movingGroupIndex);
        auto targetGroup = d->util->GetGroupByGroupIndex(targetGroupIndex);
        if (movingGroup && targetGroup) {
            targetGroup->AddWells(movingGroup->GetWells());
            d->repo->DeleteWellGroup(movingGroup);
        }
    }

    auto VesselMapWidgetControl::GetAllVesselIndices() -> QList<VesselIndex> {
        return d->util->GetAllVesselIndices();
    }

    auto VesselMapWidgetControl::SetCurrentVesselIndex(VesselIndex vesselIndex) -> void {
        d->currentVesselIndex = vesselIndex;
    }

    auto VesselMapWidgetControl::GetCurrentVesselIndex() const -> VesselIndex {
        return d->currentVesselIndex;
    }

    auto VesselMapWidgetControl::AddMarkLocationBySystemCoord(WellIndex wellIndex, const MarkType& type, double x, double y, double z, double w, double h) -> MarkIndex {
        const auto convertPos2D = d->util->ConvertLensPosToWellPos(wellIndex, {x, y});
        const auto markIndex = d->location->AddMarkLocation(wellIndex, type, convertPos2D.x, convertPos2D.y, z, w, h);
        return markIndex;
    }

    auto VesselMapWidgetControl::AddMarkLocationByWellCoord(WellIndex wellIndex, const MarkType& type, double x, double y, double z, double w, double h) -> MarkIndex {
        const auto markIndex = d->location->AddMarkLocation(wellIndex, type, x, y, z, w, h);
        return markIndex;
    }

    auto VesselMapWidgetControl::SetMarkLocationBySystemCoord(WellIndex wellIndex, MarkIndex markIndex, const MarkType& type, double x, double y, double z, double w, double h) -> void {
        const auto convertPos2D = d->util->ConvertLensPosToWellPos(wellIndex, {x, y});
        d->location->SetMarkLocation(wellIndex, markIndex, type, convertPos2D.x, convertPos2D.y, z, w, h);
    }

    auto VesselMapWidgetControl::SetMarkLocationByWellCoord(WellIndex wellIndex, MarkIndex markIndex, const MarkType& type, double x, double y, double z, double w, double h) -> void {
        d->location->SetMarkLocation(wellIndex, markIndex, type, x, y, z, w, h);
    }

    auto VesselMapWidgetControl::DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        d->location->DeleteMarkLocation(wellIndex, markIndex);
    }
    auto VesselMapWidgetControl::GetMarkLocationPosition(WellIndex wellIndex, MarkIndex markIndex) const -> std::optional<Position3D> {
        const auto loc = d->location->GetMarkLocationByMarkIndex(wellIndex, markIndex);
        if(loc) {
            return loc->GetPosition();
        }
        return std::nullopt;
    }

    auto VesselMapWidgetControl::AddAcquisitionLocationByWorldCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool {
        const auto convertPos2D = d->util->ConvertLensPosToWellPos(wellIndex, {x, y});
        return d->location->AddAcquisitionLocation(wellIndex, acquisitionIndex, type, convertPos2D.x, convertPos2D.y, z, w, h);
    }

    auto VesselMapWidgetControl::AddAcquisitionLocationByWellCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool {
        return d->location->AddAcquisitionLocation(wellIndex, acquisitionIndex, type, x, y, z, w, h);
    }

    auto VesselMapWidgetControl::SetAcquisitionLocationBySystemCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void {
        const auto convertPos2D = d->util->ConvertLensPosToWellPos(wellIndex, {x, y});
        d->location->SetAcquisitionLocation(wellIndex, acquisitionIndex, type, convertPos2D.x,convertPos2D.y,z,w,h);
    }

    auto VesselMapWidgetControl::SetAcquisitionLocationByWellCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void {
        d->location->SetAcquisitionLocation(wellIndex, acquisitionIndex, type, x, y, z, w, h);
    }

    auto VesselMapWidgetControl::DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->location->DeleteAcquisitionLocation(wellIndex, acquisitionIndex);
    }

    auto VesselMapWidgetControl::GetAcquisitionLocationPosition(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) const -> std::optional<Position3D> {
        const auto loc = d->location->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);
        if(loc) {
            return loc->GetPosition();
        }
        return std::nullopt;
    }

    auto VesselMapWidgetControl::GetFirstWellIndex() -> WellIndex {
        // TODO 여러 vessel 중에 특정 well이라면 첫 번째 well index 찾아서 반환해야 함, 0이 아닐 수 있다.
        return 0;
    }

    auto VesselMapWidgetControl::SetPreviewPresetSize(double widthMM, double heightMM) -> void {
        d->previewPresetSize = {widthMM, heightMM};
    }

    auto VesselMapWidgetControl::GetPreviewPresetWidth() const -> double {
        return d->previewPresetSize.xMM;
    }

    auto VesselMapWidgetControl::GetPreviewPresetHeight() const -> double {
        return d->previewPresetSize.yMM;
    }

    auto VesselMapWidgetControl::SetImagingOrder(const ImagingOrder& imagingOrder) -> void {
        d->imagingOrder = imagingOrder;
    }

    auto VesselMapWidgetControl::GetImagingOrder() const -> ImagingOrder {
        return d->imagingOrder;
    }

    auto VesselMapWidgetControl::GenerateSortedWells() -> QList<WellIndex> {
        switch (d->imagingOrder) {
            case ImagingOrder::ByWell:
                return d->WellsByWellIndex();
            case ImagingOrder::BySpecimen:
                return d->WellsBySpecimen();
            default:
                return {};
        }
    }

    auto VesselMapWidgetControl::Impl::DeleteGroupHasNoMember(WellGroup::Pointer group) -> void {
        const auto oldGroupSize = repo->GetWellGroups().size();
        repo->DeleteWellGroup(group);
    }

    auto VesselMapWidgetControl::Impl::WellsByWellIndex() -> QList<WellIndex> {
        QList<WellIndex> wells;
        for(const auto& group : repo->GetWellGroups()) {
            for(const auto& well : group->GetWells()) {
                wells.push_back(well);
            }
        }
        std::sort(wells.begin(), wells.end());

        return wells;
    }

    auto VesselMapWidgetControl::Impl::WellsBySpecimen() -> QList<WellIndex> {
        QMap<GroupIndex, QList<WellIndex>> tempGroupMap; // group index sorting
        QList<WellIndex> wells;
        for(const auto& group : repo->GetWellGroups()) {
            QList<WellIndex> tempWells;
            tempWells = group->GetWells();
            std::sort(tempWells.begin(), tempWells.end());
            tempGroupMap[group->GetIndex()] = tempWells;
            wells.append(tempWells);
        }

        return wells;
    }
}
