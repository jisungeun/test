﻿#include "ImagingOrderWidget.h"
#include "ui_ImagingOrderWidget.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct ImagingOrderWidget::Impl {
        Self* self{};
        Ui::ImagingOrderWidget* ui{nullptr};

        auto HideSubwidgets() -> void;

        auto InitOrderSelectWidget() -> void;
        auto InitShowOrderWidget() -> void;
    };

    auto ImagingOrderWidget::Impl::HideSubwidgets() -> void {
        ui->imagingOrderLabel->hide();
        ui->orderSelectCombo->hide();
        ui->showOrderBtn->hide();
    }

    auto ImagingOrderWidget::Impl::InitOrderSelectWidget() -> void {
        ui->orderSelectCombo->addItem("by Well", ImagingOrder::ByWell);
        ui->orderSelectCombo->addItem("by Specimen", ImagingOrder::BySpecimen);
        connect(ui->orderSelectCombo, qOverload<int>(&QComboBox::currentIndexChanged), self, &Self::onImagingOrderChanged);
        
        // TSX1.1 by Well만 제공
        ui->orderSelectCombo->setDisabled(true);
    }

    auto ImagingOrderWidget::Impl::InitShowOrderWidget() -> void {
        ui->showOrderBtn->setObjectName("bt-toggle");
        ui->showOrderBtn->setCheckable(true);
        connect(ui->showOrderBtn, &QPushButton::toggled, self, &Self::sigShowImaigngOrder);
    }

    ImagingOrderWidget::ImagingOrderWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->self = this;
        d->ui = new Ui::ImagingOrderWidget();
        d->ui->setupUi(this);

        d->ui->baseWidget->setObjectName("panel");
        d->ui->title->setObjectName("label-h2");
        d->ui->imagingOrderLabel->setObjectName("label-h5");

        d->InitShowOrderWidget();
        d->InitOrderSelectWidget();
    }

    ImagingOrderWidget::~ImagingOrderWidget() {
        delete d->ui;
    }

    auto ImagingOrderWidget::SetViewMode(const ViewMode& mode) -> void {
        Q_UNUSED(mode)
        // TODO imaging order 변경 기능 제공 시 perform mode에서 UI 보여줄 것
        d->HideSubwidgets();
    }

    auto ImagingOrderWidget::ClearAll() -> void {
        d->ui->showOrderBtn->setChecked(false);
        d->ui->orderSelectCombo->setCurrentIndex(0);
    }

    void ImagingOrderWidget::onImagingOrderChanged(int comboBoxIndex) {
        Q_UNUSED(comboBoxIndex)

        bool ok{};
        const auto userData = d->ui->orderSelectCombo->currentData().toInt(&ok);
        if (ok) {
            emit sigChangeImagingOrder(ImagingOrder::_from_integral(userData));
        }

    }
}
