﻿#pragma once

#include <QTableWidget>

#include "LocationDataRepo.h"
#include "VesselMapDataRepo.h"
#include "VesselMapExternalData.h"

namespace TC {
    class ImagingPointTable : public QTableWidget {
        Q_OBJECT
	public:
        using Self = ImagingPointTable;
        using Pointer = std::shared_ptr<Self>;

        explicit ImagingPointTable(QWidget* parent = nullptr);
        ~ImagingPointTable() override;

        auto ClearAll() -> void;

        auto InitColumnHeader() -> void;

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMap) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto SetViewMode(ViewMode mode) -> void;
        auto SetCurrentWellIndex(WellIndex wellIndex) -> void;

        auto AddAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto DeleteAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;


        auto SetShowAll(bool showAll) -> void;

    signals:
        void sigShowTileCondition(const WellIndex&, const AcquisitionIndex&);
        void sigCurrentSelectedItems(const QList<QPair<WellIndex, AcquisitionIndex>>&);
        void sigAcqItemDoubleClicked(const WellIndex&, const AcquisitionIndex&, double x, double y, double z);
        void sigDeleteAcquisitionItem(const WellIndex&, const AcquisitionIndex&);
        void sigImagingPointSelected(const QString& wellPosition, const QString& pointID); // ROI Index가 존재하면, wellPosition 이름에 ROI Index가 추가되어 emit된다.
        void sigCopyAcquisitionItems(const WellIndex& sourceWellIndex, const QList<AcquisitionIndex>& copyPointIndices);

    protected:
        auto keyPressEvent(QKeyEvent* event) -> void override;
        auto mousePressEvent(QMouseEvent* event) -> void override;

    public slots:
        void onAcquisitionDataDeleteRequested();
        void onAcquisitionDataCopyRequested();
        void onUpdateTableSectionByWellCanvas(const QList<QPair<WellIndex, AcquisitionIndex>>& selected);

    private slots:
        void onTableSelectionChanged();
        void onItemDoubleClicked(QTableWidgetItem* item);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
