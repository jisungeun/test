﻿#include <QRandomGenerator>

#include "DefaultSettingHelper.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    DefaultSettingHelper::DefaultSettingHelper() = default;
    DefaultSettingHelper::~DefaultSettingHelper() = default;

    auto DefaultSettingHelper::MakeWellPositionName(int32_t row, int32_t column) -> QString {
        return GetWellRowName(row) + QString::number(column + 1);
    }

    auto DefaultSettingHelper::GetWellRowName(int32_t row) -> QString {
        char rowString[50];
        int32_t i = 0;
        int32_t rowPlusOne = row + 1;
        while (rowPlusOne > 0) {
            int32_t remainder = rowPlusOne % 26;
            if (remainder == 0) {
                rowString[i++] = 'Z';
                rowPlusOne = (rowPlusOne / 26) - 1;
            }
            else {
                rowString[i++] = (remainder - 1) + 'A';
                rowPlusOne /= 26;
            }
        }

        rowString[i] = '\0';

        std::reverse(rowString, rowString + strlen(rowString));

        return QString::fromLatin1(rowString);
    }

    auto DefaultSettingHelper::GetBoundingRect(double w, double h) -> QRectF {
        const auto x = -w / 2;
        const auto y = -h / 2;

        return QRectF(x, y, w, h);
    }

    auto DefaultSettingHelper::GetBoundingRect(double x, double y, double w, double h) -> QRectF {
        const auto convertX = (-w / 2) + x;
        const auto convertY = (-h / 2) + y;
        return QRectF(convertX, convertY, w, h);
    }

    auto DefaultSettingHelper::CalculateCenterPosX(double start, double end) -> double {
        return (start + end) / 2;
    }

    auto DefaultSettingHelper::CalculateCenterPosY(double start, double end) -> double {
        return -(start + end) / 2;
    }

    auto DefaultSettingHelper::CalculateSize(double start, double end) -> double {
        return fabs(start - end);
    }
}
