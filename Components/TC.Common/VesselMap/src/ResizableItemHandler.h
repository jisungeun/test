﻿#pragma once

#include <memory>

#include <QRectF>
#include <QObject>

#include "GraphicsItemDefine.h"

namespace TC {
    class ResizableItemHandler : public QObject {
        Q_OBJECT
    public:
        using Self = ResizableItemHandler;

        ResizableItemHandler();
        ~ResizableItemHandler() override;

        auto SetCurrentLOD(const double& lod) -> void;
        auto SetCurrentPenWidth(const double& penWidth) -> void;

        auto DrawHandlesIfNeeded() -> void;
        auto ClearHandles() -> void;
        auto SetOwnerItem(QGraphicsItem* owner) -> void;

        virtual auto GetSelectorFrameBounds() const -> QRectF = 0;
        virtual auto SetSelectorFrameBounds(const QRectF& boundRect) -> void = 0;

    signals:
        void sigOwnerItemMove(const QPointF& lastPos, const QPointF& currentPos);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
