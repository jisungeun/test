﻿#include <QApplication>
#include <QMouseEvent>
#include <qmath.h>

#include "WellCanvasEventFilter.h"
#include "DefaultSettingHelper.h"

namespace TC {
    struct WellCanvasEventFilter::Impl {
        QGraphicsView* view{nullptr};

        const Qt::KeyboardModifiers zoomModifier{Qt::ControlModifier};

                                        const double scaleFactor{1.15};
        const double scaleMax{8.0};
        const double scaleMin{1.0};
        double currentScale{1.0};
    };

    WellCanvasEventFilter::WellCanvasEventFilter(QGraphicsView* view) : QObject(view), d{std::make_unique<Impl>()} {
        d->view = view;
        d->view->viewport()->installEventFilter(this);
        d->view->setMouseTracking(true);
    }

    WellCanvasEventFilter::~WellCanvasEventFilter() {
    }

    auto WellCanvasEventFilter::ZoomIn(bool centerZoom) const -> void {
        if (centerZoom) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
            
        }
        else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        }

        if (d->currentScale <= d->scaleMax) {
            d->view->scale(d->scaleFactor, d->scaleFactor);
            d->currentScale *= d->scaleFactor;
        }
    }

    auto WellCanvasEventFilter::ZoomOut(bool centerZoom) const -> void {
        if (centerZoom) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
            
        }
        else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        }

        if (d->currentScale > d->scaleMin) {
            d->view->scale(1 / d->scaleFactor, 1 / d->scaleFactor);
            d->currentScale /= d->scaleFactor;
        }
    }

    auto WellCanvasEventFilter::ZoomFit() const -> void {
        d->currentScale = 1.0;
        d->view->fitInView(d->view->scene()->sceneRect(), Qt::KeepAspectRatio);
    }

    auto WellCanvasEventFilter::eventFilter(QObject* watched, QEvent* event) -> bool {
        Q_UNUSED(watched)
        if (event->type() == QEvent::MouseButtonDblClick) {
            const auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::RightButton) {
                ZoomFit();
            }
        }

        if (QApplication::keyboardModifiers() == d->zoomModifier) {
            if (event->type() == QEvent::Wheel) {
                const auto wheelEvent = static_cast<QWheelEvent*>(event);
                if (QApplication::keyboardModifiers() == d->zoomModifier) {
                    if(wheelEvent->delta() > 0) {
                        ZoomIn();
                    }
                    else if (wheelEvent->delta() < 0) {
                        ZoomOut();
                    }

                    return true;
                }
            }
        }
        return false;
    }
}
