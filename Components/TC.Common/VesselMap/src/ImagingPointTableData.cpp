﻿#include <cmath>

#include "ImagingPointTableData.h"

namespace TC {
    struct ImagingPointTableData::Impl {
        AcquisitionIndex acquisitionIndex{};
        Position3D pos{0.0, 0.0, 0.0};
        Size2D size{0.0, 0.0};
        QString pointIndexName{};
        QString wellPosName{};
        WellIndex wellIndex{kInvalid};
        QString roiIndexName{};

        auto operator==(const Impl& rhs) const -> bool; 
        auto operator!=(const Impl& rhs) const -> bool; 

        auto Round(const double& original) const -> double;
    };

    auto ImagingPointTableData::Impl::operator==(const Impl& rhs) const -> bool {
        if(acquisitionIndex != rhs.acquisitionIndex) return false;
        if(pos != rhs.pos) return false;
        if(size != rhs.size) return false;
        if(pointIndexName != rhs.pointIndexName) return false;
        if(wellPosName != rhs.wellPosName) return false;
        if(wellIndex != rhs.wellIndex) return false;
        if(roiIndexName != rhs.roiIndexName) return false;
        return true;
    }

    auto ImagingPointTableData::Impl::operator!=(const Impl& rhs) const -> bool {
        return !this->operator==(rhs);
    }

    auto ImagingPointTableData::Impl::Round(const double& original) const -> double {
        return std::round(original*1000) / 1000.0;
    }

    ImagingPointTableData::ImagingPointTableData() : d{std::make_unique<Impl>()} {
    }

    ImagingPointTableData::ImagingPointTableData(const Self& rhs) : d{std::make_unique<Impl>()} {
        this->operator=(rhs);
    }

    ImagingPointTableData::ImagingPointTableData(Self&& rhs) noexcept : d{std::make_unique<Impl>()} {
        this->operator=(std::move(rhs));
    }

    ImagingPointTableData::~ImagingPointTableData(){
    }

    auto ImagingPointTableData::operator=(const Self& rhs) -> Self& {
        if(this != &rhs) {
            *d = *rhs.d;
        }
        return *this;
    }

    auto ImagingPointTableData::operator=(Self&& rhs) noexcept -> Self& {
        if(this != &rhs) {
            *d = *rhs.d;
            rhs.d.reset(new Impl);
        }
        return *this; 
    }

    auto ImagingPointTableData::operator==(const Self& rhs) const -> bool {
        return *d == *rhs.d;
    }

    auto ImagingPointTableData::operator!=(const Self& rhs) const -> bool {
        return *d != *rhs.d;
    }

    void ImagingPointTableData::SetAcquisitionIndex(const AcquisitionIndex& index) {
        d->acquisitionIndex = index;
    }

    AcquisitionIndex ImagingPointTableData::GetAcquisitionIndex() const {
        return d->acquisitionIndex;
    }

    auto ImagingPointTableData::SetWellPosName(const QString& wellPosName) -> void {
        d->wellPosName = wellPosName;
    }

    auto ImagingPointTableData::GetWellPosName() const -> QString {
        return d->wellPosName;
    }

    auto ImagingPointTableData::SetWellIndex(const WellIndex& wellIndex) -> void {
        d->wellIndex = wellIndex;
    }

    auto ImagingPointTableData::GetWellIndex() const -> WellIndex {
        return d->wellIndex;
    }

    auto ImagingPointTableData::SetPointIndexName(const QString& pointIndexString) const -> void {
        d->pointIndexName = pointIndexString;
    }

    auto ImagingPointTableData::GetPointIndexName() const -> QString {
        return d->pointIndexName;
    }

    void ImagingPointTableData::SetPosition(const Position3D& pos) {
        d->pos = {d->Round(pos.x), d->Round(pos.y), d->Round(pos.z)};
    }

    void ImagingPointTableData::SetPosition(const double& x, const double& y, const double& z) {
        d->pos = {d->Round(x), d->Round(y), d->Round(z)};
    }

    Position3D ImagingPointTableData::GetPosition() const {
        return d->pos;
    }

    double ImagingPointTableData::GetPosX() const {
        return d->pos.x;
    }

    double ImagingPointTableData::GetPosY() const {
        return d->pos.y;
    }

    double ImagingPointTableData::GetPosZ() const {
        return d->pos.z;
    }

    void ImagingPointTableData::SetSize(const Size2D& size) {
        d->size = size;
    }

    void ImagingPointTableData::SetSize(const double& w, const double& h) {
        d->size = {w, h};
    }

    Size2D ImagingPointTableData::GetSize() const {
        return d->size;
    }

    double ImagingPointTableData::GetWidth() const {
        return d->size.w;
    }

    double ImagingPointTableData::GetHeight() const {
        return d->size.h;
    }

    auto ImagingPointTableData::SetRoiIndexName(const QString& roiName) -> void {
        d->roiIndexName = roiName;
    }

    auto ImagingPointTableData::GetRoiIndexName() const -> QString {
        return d->roiIndexName;
    }
}
