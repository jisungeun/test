﻿#pragma once

#include <QGraphicsItem>

#include "VesselMapExternalData.h"

namespace TC {
    class VesselCanvasTextItem : public QGraphicsItem {
    public:
        using Self = VesselCanvasTextItem;
        using Pointer = std::shared_ptr<Self>;

        explicit VesselCanvasTextItem(QGraphicsItem* parent = nullptr);
        ~VesselCanvasTextItem() override;

        auto SetViewMode(ViewMode viewMode) -> void;
        auto SetLabel(const QString& label) -> void;
        auto SetSize(double w, double h) -> void;
        auto SetPosition(double x, double y) -> void;
        auto SetTextAlignment(Qt::Alignment alignment) -> void;

    protected:
        auto type() const -> int override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
