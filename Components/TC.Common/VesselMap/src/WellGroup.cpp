﻿#include "WellGroup.h"

namespace TC {
    struct WellGroup::Impl {
        GroupIndex index;
        QString name;
        QColor color;
        QList<WellIndex> members;

        auto Sort() -> void;
    };

    WellGroup::WellGroup() : d{std::make_unique<Impl>()} {
    }

    WellGroup::WellGroup(const WellGroup& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    auto WellGroup::operator=(const WellGroup& other) -> WellGroup& {
        *d = *other.d;
        return *this;
    }

    WellGroup::~WellGroup() {
    }

    auto WellGroup::SetIndex(GroupIndex index) -> void {
        d->index = index;
    }

    auto WellGroup::GetIndex() const -> GroupIndex {
        return d->index;
    }

    auto WellGroup::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto WellGroup::GetName() const -> QString {
        return d->name;
    }

    auto WellGroup::SetColor(const QColor& color) -> void {
        d->color = color;
    }

    auto WellGroup::GetColor() const -> QColor {
        return d->color;
    }

    auto WellGroup::AddWell(WellIndex wellIndex) -> void {
        d->members.push_back(wellIndex);
        d->Sort();
    }

    auto WellGroup::AddWells(const QList<WellIndex>& wellIndices) -> void {
        d->members.append(wellIndices);
        d->Sort();
    }

    auto WellGroup::DeleteWell(WellIndex wellIndex) -> void {
        d->members.removeOne(wellIndex);
    }

    auto WellGroup::DeleteWells(const QList<WellIndex>& wellIndices) -> void {
        for (const auto wellIndex : wellIndices) {
            d->members.removeOne(wellIndex);
        }
    }

    auto WellGroup::SetWells(const QList<WellIndex>& wellIndices) -> void {
        d->members = wellIndices;
        d->Sort();
    }

    auto WellGroup::GetWells() const -> QList<WellIndex>& {
        return d->members;
    }

    auto WellGroup::Impl::Sort() -> void {
        std::sort(members.begin(), members.end());
    }
}
