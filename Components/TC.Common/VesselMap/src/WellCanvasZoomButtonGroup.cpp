﻿#include <QPixmap>
#include <QGraphicsOpacityEffect>

#include "WellCanvasZoomButtonGroup.h"
#include "ui_WellCanvasZoomButtonGroup.h"

namespace TC {
    struct WellCanvasZoomButtonGroup::Impl {
        Ui::WellCanvasZoomButtonGroup *ui{nullptr};
    };

    WellCanvasZoomButtonGroup::WellCanvasZoomButtonGroup(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::WellCanvasZoomButtonGroup();
        d->ui->setupUi(this);

        setFixedWidth(24);
        connect(d->ui->zoomInBtn, &QToolButton::clicked, this, &Self::sigZoomIn);
        connect(d->ui->zoomOutBtn, &QToolButton::clicked, this, &Self::sigZoomOut);

        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
	    setAutoFillBackground(true);
    }

    WellCanvasZoomButtonGroup::~WellCanvasZoomButtonGroup() {
        delete d->ui;
    }

    void WellCanvasZoomButtonGroup::enterEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(1.0);
	    setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    void WellCanvasZoomButtonGroup::leaveEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }

}
