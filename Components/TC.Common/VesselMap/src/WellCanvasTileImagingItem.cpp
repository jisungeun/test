﻿#include <QPen>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>

#include "WellCanvasTileImagingItem.h"
#include "DefaultSettingHelper.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    namespace TileImagingUtil {
        auto doubleCompare = [](const double& v1, const double& v2) -> bool {
            return fabs(v1 - v2) > 0.0001;
        };
    }

    struct WellCanvasTileImagingItem::Impl {
        Geometry3D geometry3D;
        double penWidth{};
        QColor lineColor{TileBorderColor};
    };

    WellCanvasTileImagingItem::WellCanvasTileImagingItem(QGraphicsItem* parent) : QGraphicsRectItem(parent), d{std::make_unique<Impl>()} {
        SetOwnerItem(this);
        setFlag(ItemIsSelectable, false);
        setFlag(ItemIsMovable, false);
        setVisible(false);
        SetGeometry3D(d->geometry3D);
        SetCurrentPenWidth(d->penWidth);
        setZValue(GraphicsItemZValue::TileImaging);
    }

    WellCanvasTileImagingItem::~WellCanvasTileImagingItem() {
    }

    auto WellCanvasTileImagingItem::SetGeometry3D(const Geometry3D& geo) -> void {
        if (TileImagingUtil::doubleCompare(d->geometry3D.x, geo.x) || TileImagingUtil::doubleCompare(d->geometry3D.y, geo.y)) {
            prepareGeometryChange();
        }
        d->geometry3D = geo;
        setPos(d->geometry3D.x, -d->geometry3D.y);
        setRect(DefaultSettingHelper::GetBoundingRect(d->geometry3D.w, d->geometry3D.h));
        update();
    }

    auto WellCanvasTileImagingItem::GetGeometry3D() const -> Geometry3D& {
        return d->geometry3D;
    }

    auto WellCanvasTileImagingItem::boundingRect() const -> QRectF {
        return GetSelectorFrameBounds().adjusted(-d->penWidth, -d->penWidth, d->penWidth, d->penWidth);
    }

    auto WellCanvasTileImagingItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        painter->setRenderHint(QPainter::Antialiasing);
        Geometry2D newGeo = {mapToScene(GetSelectorFrameBounds().center()).x(),
            -mapToScene(GetSelectorFrameBounds().center()).y(),
            GetSelectorFrameBounds().width(),
            GetSelectorFrameBounds().height()};

        if (TileImagingUtil::doubleCompare(newGeo.x, d->geometry3D.x) || TileImagingUtil::doubleCompare(newGeo.y, d->geometry3D.y) || 
            TileImagingUtil::doubleCompare(newGeo.w, d->geometry3D.w) || TileImagingUtil::doubleCompare(newGeo.h, d->geometry3D.h)) {
            d->geometry3D.x = newGeo.x;
            d->geometry3D.y = newGeo.y;
            d->geometry3D.w = newGeo.w;
            d->geometry3D.h = newGeo.h;
        }

        QColor innerColor = Qt::transparent;
        if(option->state & QStyle::State_Selected) {
            innerColor = d->lineColor;
            innerColor.setAlpha(40);
        }
        painter->setPen(QPen(d->lineColor, d->penWidth, Qt::SolidLine));
        painter->setBrush(innerColor);
        painter->drawRect(rect());

        SetCurrentLOD(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        // TODO 편집가능하게 하려면 주석 해제
        //DrawHandlesIfNeeded(); // non-editable
    }

    auto WellCanvasTileImagingItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::TileImagingItem);
    }

    auto WellCanvasTileImagingItem::SetSelectorFrameBounds(const QRectF& boundRect) -> void {
        prepareGeometryChange();
        setRect(boundRect);
        update();
    }

    auto WellCanvasTileImagingItem::GetSelectorFrameBounds() const -> QRectF {
        return rect();
    }
}
