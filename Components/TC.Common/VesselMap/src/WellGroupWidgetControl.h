﻿#pragma once

#include <memory>

#include <QList>

#include "VesselMapDataRepo.h"
#include "VesselMapExternalData.h"
#include "WellGroupTableDefine.h"
#include "WellGroupTableData.h"
#include "LocationDataRepo.h"

namespace TC {
    class WellGroupWidgetControl {
    public:
        using Self = WellGroupWidgetControl;
        using Pointer = std::shared_ptr<Self>;

        WellGroupWidgetControl();
        ~WellGroupWidgetControl();

        auto ClearData() -> void;

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto GetGroupName(const GroupIndex& groupIndex) const -> QString;
        auto GetWellName(const WellIndex& wellIndex) const -> QString;

        auto GetColumnCount() -> int32_t;
        auto GetColumnHeaderLabels() -> QStringList;

        auto GenerateTableData(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> QList<WellGroupTableData::Pointer>;
        auto GetImagingPointCount(WellIndex wellIndex) -> int32_t;

        auto IsExistGroupName(const QString& groupName) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
