﻿#include "ImagingPointTableItemCreator.h"

namespace TC {
    struct ImagingPointTableItemCreator::Impl {

    };

    ImagingPointTableItemCreator::ImagingPointTableItemCreator() : d{std::make_unique<Impl>()} {

    }

    ImagingPointTableItemCreator::~ImagingPointTableItemCreator() {

    }

    auto ImagingPointTableItemCreator::CreateAcquisitionTableItem(ImagingTableHeader header, ImagingPointTableData::Pointer data) -> QTableWidgetItem* {
        switch (header) {
            case ImagingTableHeader::AcquisitionIndex: return new ImagingTableItem(QString::number(data->GetAcquisitionIndex()));

            case ImagingTableHeader::Well: return new ImagingTableItem(data->GetWellPosName());

            case ImagingTableHeader::PointID: return new ImagingTableItem(data->GetPointIndexName());

            case ImagingTableHeader::PosX: return new ImagingTableItem(QString::number(data->GetPosX()));

            case ImagingTableHeader::PosY: return new ImagingTableItem(QString::number(data->GetPosY()));

            case ImagingTableHeader::PosZ: return new ImagingTableItem(QString::number(data->GetPosZ()));

            case ImagingTableHeader::Size: return new ImagingTableItem(GetUmSizeText(data->GetWidth(),data->GetHeight()));

            case ImagingTableHeader::WellIndex: return new ImagingTableItem(QString::number(data->GetWellIndex()));

            case ImagingTableHeader::ROI: return new ImagingTableItem(data->GetRoiIndexName());

            default: return nullptr;
        }

    }

    auto ImagingPointTableItemCreator::GetUmSizeText(double wInMM, double hInMM) -> QString {
        const auto wInUM = wInMM*1000.;
        const auto hInUM = hInMM*1000.;
        return QString("%1x%2").arg(wInUM, 0, 'f', 0).arg(hInUM, 0, 'f', 0);
    }
}
