﻿#pragma once

#include <enum.h>

namespace TC {
    BETTER_ENUM(ImagingTableHeader, int32_t, AcquisitionIndex = 0, Well, WellIndex, PointID, ROI, Size, PosX, PosY, PosZ);
}
