﻿#pragma once

#include <memory>

#include <QList>

#include "LocationMark.h"
#include "LocationAcquisition.h"

#include "VesselMapExternalData.h"

namespace TC {
    class LocationDataRepo {
    public:
        using Self = LocationDataRepo;
        using Pointer = std::shared_ptr<Self>;

        LocationDataRepo();
        ~LocationDataRepo();

        auto ClearAll() -> void;

        auto AddMarkLocation(WellIndex wellIndex, MarkType type, double x, double y, double z, double w, double h) const -> MarkIndex;
        auto SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w, double h) -> void;
        auto DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto GetMarkLocationByMarkIndex(WellIndex wellIndex, MarkIndex markIndex) -> LocationMark::Pointer;

        auto AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool;
        auto SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void;
        auto DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto GetAcquisitionLocationByAcquisitionIndex(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> LocationAcquisition::Pointer;

        auto GetWellIndexByMarkLIndex(MarkIndex markIndex) const -> WellIndex;
        auto GetWellIndexByAcquisitionIndex(AcquisitionIndex acquisitionIndex) const -> WellIndex;

        auto GetMarkIndicesByWellIndex(WellIndex wellIndex) -> QList<MarkIndex>;
        auto GetAcquisitionIndicesByWellIndex(WellIndex wellIndex) -> QList<AcquisitionIndex>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
