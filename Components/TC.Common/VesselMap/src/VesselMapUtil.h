﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"
#include "Vessel.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapDataRepo.h"
#include "Well.h"
#include "WellGroup.h"

namespace TC {
    class VesselMapUtil {
    public:
        using Self = VesselMapUtil;
        using Pointer = std::shared_ptr<Self>;

        VesselMapUtil();
        ~VesselMapUtil();

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;

        auto GetAllVesselIndices() -> QList<VesselIndex>;

        auto GetGroupByGroupIndex(GroupIndex groupIndex) const -> WellGroup::Pointer;
        auto GetGroupByGroupName(const QString& groupName) const -> WellGroup::Pointer;
        auto GetGroupByWellIndex(WellIndex wellIndex) const -> WellGroup::Pointer;
        auto GetWellByWellIndex(WellIndex wellIndex) const -> Well::Pointer;

        auto IsWellAlreadyHasGroup(WellIndex wellIndex) -> bool;

        auto GetWellIndexByRowColumn(int32_t row, int32_t column) const -> WellIndex;
        auto GetWellRowByWellIndex(WellIndex wellIndex) const -> int32_t;
        auto GetWellColumnByWellIndex(WellIndex wellIndex) const -> int32_t;

        auto GetVesselByVesselIndex(VesselIndex vesselIndex) -> Vessel::Pointer;

        auto ConvertLensPosToWellPos(WellIndex wellIndex, Position2D lensPos) -> Position2D;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
