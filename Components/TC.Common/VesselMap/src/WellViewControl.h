﻿#pragma once

#include <memory>
#include <QString>

#include "GraphicsItemDefine.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapExternalData.h"
#include "Well.h"

namespace TC {
    class WellViewControl {
    public:
        using Self = WellViewControl;
        using Pointer = std::shared_ptr<Self>;

        WellViewControl();
        ~WellViewControl();

        auto InitWellPosition(const QList<Well::Pointer>& wells) -> void;

        auto SetCurrentFocusWell(WellIndex wellIndex) -> void;
        auto GetCurrentFocusWell() const -> WellIndex;

        auto GetWellPosName() const -> QString;
        auto GetWellPosByLensPos(double x, double y, double z) const -> Position3D;

        auto SavePreviewStatus(bool isPreviewAreaVisible, const Geometry2D& geometry2D) -> void;

        auto GetLastPreviewAreaVisibleStatus() const -> bool;
        auto GetLastPreviewArea() const -> Geometry2D;

        auto SetSafePosRange(const VesselAxis& axis, const double& min, const double& max) -> void;
        auto GetSafePosValue(const VesselAxis& axis, const double& inputValue) const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
