﻿#include "Well.h"

namespace TC {
    struct Well::Impl {
        WellIndex index{kInvalid};
        WellShape shape{WellShape::Circle};

        QString name{};

        double x{0.0};
        double y{0.0};

        double width{0.0};
        double height{0.0};

        int32_t row{kInvalid};
        int32_t column{kInvalid};
    };

    Well::Well() : d{std::make_unique<Impl>()} {
    }

    Well::Well(const Well& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    Well::~Well() {
    }

    auto Well::operator=(const Well& other) -> Well& {
        *d = *other.d;
        return *this;
    }

    auto Well::SetIndex(WellIndex index) -> void {
        d->index = index;
    }

    auto Well::GetIndex() const -> WellIndex {
        return d->index;
    }

    auto Well::SetShape(WellShape shape) -> void {
        d->shape = shape;
    }

    auto Well::GetShape() const -> WellShape {
        return d->shape;
    }

    auto Well::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto Well::GetName() const -> QString {
        return d->name;
    }

    auto Well::SetX(double x) -> void {
        d->x = x;
    }

    auto Well::GetX() const -> double {
        return d->x;
    }

    auto Well::SetY(double y) -> void {
        d->y = y;
    }

    auto Well::GetY() const -> double {
        return d->y;
    }

    auto Well::SetWidth(double width) -> void {
        d->width = width;
    }

    auto Well::GetWidth() const -> double {
        return d->width;
    }

    auto Well::SetHeight(double height) -> void {
        d->height = height;
    }

    auto Well::GetHeight() const -> double {
        return d->height;
    }

    auto Well::SetRow(int32_t row) -> void {
        d->row = row;
    }

    auto Well::GetRow() const -> int32_t {
        return d->row;
    }

    auto Well::SetColumn(int32_t column) -> void {
        d->column = column;
    }

    auto Well::GetColumn() const -> int32_t {
        return d->column;
    }
}
