﻿#include "VesselMapBuilderControl.h"
#include "VesselMapCalculator.h"
#include "Holder.h"
#include "Vessel.h"
#include "Well.h"
#include "ImagingArea.h"

namespace TC {
    struct VesselMapBuilderControl::Impl {
        double holderWidth{0.0};
        double holderHeight{0.0};

        VesselShape vesselShape{VesselShape::Rectangle};
        double vesselStartX{0.0};
        double vesselStartY{0.0};
        double vesselWidth{0.0};
        double vesselHeight{0.0};
        int32_t vesselRows{kInvalid};
        int32_t vesselColumns{kInvalid};
        double vesselHorizontalSpacing{0.0};
        double vesselVerticalSpacing{0.0};

        WellShape wellShape{WellShape::Circle};
        double wellStartX{0.0};
        double wellStartY{0.0};
        double wellWidth{0.0};
        double wellHeight{0.0};
        int32_t wellRows{kInvalid};
        int32_t wellColumns{kInvalid};
        double wellHorizontalSpacing{0.0};
        double wellVerticalSpacing{0.0};

        ImagingAreaShape imagingAreaShape{ImagingAreaShape::Circle};
        double imagingAreaX{0.0};
        double imagingAreaY{0.0};
        double imagingAreaWidth{0.0};
        double imagingAreaHeight{0.0};

        auto CreateVesselMap() -> VesselMap::Pointer;

        auto SetDefaultWellName(int32_t row, int32_t column) -> QString;
        auto ConvertRowNumToChar(int32_t row) -> QString;
    };

    auto VesselMapBuilderControl::Impl::SetDefaultWellName(int32_t row, int32_t column) -> QString {
        return ConvertRowNumToChar(row) + QString::number(column + 1);
    }

    auto VesselMapBuilderControl::Impl::ConvertRowNumToChar(int32_t row) -> QString {
        char rowString[50];
        int32_t i = 0;
        int32_t rowPlusOne = row + 1;
        while (rowPlusOne > 0) {
            int32_t remainder = rowPlusOne % 26;
            if (remainder == 0) {
                rowString[i++] = 'Z';
                rowPlusOne = (rowPlusOne / 26) - 1;
            }
            else {
                rowString[i++] = (remainder - 1) + 'A';
                rowPlusOne /= 26;
            }
        }

        rowString[i] = '\0';

        std::reverse(rowString, rowString + strlen(rowString));

        return QString::fromLatin1(rowString);
    }

    VesselMapBuilderControl::VesselMapBuilderControl() : d{std::make_unique<Impl>()} {
    }

    VesselMapBuilderControl::VesselMapBuilderControl(const VesselMapBuilderControl& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    VesselMapBuilderControl::~VesselMapBuilderControl() {
    }

    auto VesselMapBuilderControl::operator=(const VesselMapBuilderControl& other) -> VesselMapBuilderControl& {
        *d = *other.d;
        return *this;
    }

    auto VesselMapBuilderControl::SetHolderWidth(double width) -> void {
        d->holderWidth = width;
    }

    auto VesselMapBuilderControl::SetHolderHeight(double height) -> void {
        d->holderHeight = height;
    }

    auto VesselMapBuilderControl::SetVesselShape(VesselShape shape) -> void {
        d->vesselShape = shape;
    }

    auto VesselMapBuilderControl::SetVesselStartX(double x) -> void {
        d->vesselStartX = x;
    }

    auto VesselMapBuilderControl::SetVesselStartY(double y) -> void {
        d->vesselStartY = y;
    }

    auto VesselMapBuilderControl::SetVesselWidth(double width) -> void {
        d->vesselWidth = width;
    }

    auto VesselMapBuilderControl::SetVesselHeight(double height) -> void {
        d->vesselHeight = height;
    }

    auto VesselMapBuilderControl::SetVesselRows(int32_t rows) -> void {
        d->vesselRows = rows;
    }

    auto VesselMapBuilderControl::SetVesselColumns(int32_t columns) -> void {
        d->vesselColumns = columns;
    }

    auto VesselMapBuilderControl::SetVesselHSpacing(double spacing) -> void {
        d->vesselHorizontalSpacing = spacing;
    }

    auto VesselMapBuilderControl::SetVesselVSpacing(double spacing) -> void {
        d->vesselVerticalSpacing = spacing;
    }

    auto VesselMapBuilderControl::SetWellShape(WellShape shape) -> void {
        d->wellShape = shape;
    }

    auto VesselMapBuilderControl::SetWellStartX(double x) -> void {
        d->wellStartX = x;
    }

    auto VesselMapBuilderControl::SetWellStartY(double y) -> void {
        d->wellStartY = y;
    }

    auto VesselMapBuilderControl::SetWellWidth(double width) -> void {
        d->wellWidth = width;
    }

    auto VesselMapBuilderControl::SetWellHeight(double height) -> void {
        d->wellHeight = height;
    }

    auto VesselMapBuilderControl::SetWellRows(int32_t rows) -> void {
        d->wellRows = rows;
    }

    auto VesselMapBuilderControl::SetWellColumns(int32_t columns) -> void {
        d->wellColumns = columns;
    }

    auto VesselMapBuilderControl::SetWellHSpacing(double spacing) -> void {
        d->wellHorizontalSpacing = spacing;
    }

    auto VesselMapBuilderControl::SetWellVSpacing(double spacing) -> void {
        d->wellVerticalSpacing = spacing;
    }

    auto VesselMapBuilderControl::SetImagingAreaShape(ImagingAreaShape shape) -> void {
        d->imagingAreaShape = shape;
    }

    auto VesselMapBuilderControl::SetImagingAreaCenterX(double x) -> void {
        d->imagingAreaX = x;
    }

    auto VesselMapBuilderControl::SetImagingAreaCenterY(double y) -> void {
        d->imagingAreaY = y;
    }

    auto VesselMapBuilderControl::SetImagingAreaWidth(double width) -> void {
        d->imagingAreaWidth = width;
    }

    auto VesselMapBuilderControl::SetImagingAreaHeight(double height) -> void {
        d->imagingAreaHeight = height;
    }

    auto VesselMapBuilderControl::GenerateVesselMap() const -> VesselMap::Pointer {
        return d->CreateVesselMap();
    }

    auto VesselMapBuilderControl::Impl::CreateVesselMap() -> VesselMap::Pointer {
        auto vesselMap = std::make_shared<VesselMap>();

        const auto holder = std::make_shared<Holder>();
        holder->SetWidth(holderWidth);
        holder->SetHeight(holderHeight);

        const auto imagingArea = std::make_shared<ImagingArea>();
        imagingArea->SetShape(imagingAreaShape);
        imagingArea->SetX(imagingAreaX);
        imagingArea->SetY(imagingAreaY);
        imagingArea->SetWidth(imagingAreaWidth);
        imagingArea->SetHeight(imagingAreaHeight);

        vesselMap->SetHolder(holder);
        vesselMap->SetImagingArea(imagingArea);

        VesselMapCalculator calculator;
        calculator.SetVesselRows(vesselRows);
        calculator.SetVesselColumns(vesselColumns);
        calculator.SetVesselWidth(vesselWidth);
        calculator.SetVesselHeight(vesselHeight);
        calculator.SetWellRows(wellRows);
        calculator.SetWellColumns(wellColumns);
        calculator.SetWellWidth(wellWidth);
        calculator.SetWellHeight(wellHeight);

        for (auto vesselRow = 0; vesselRow < vesselRows; ++vesselRow) {
            for (auto vesselCol = 0; vesselCol < vesselColumns; ++vesselCol) {
                const auto vesselIndex = calculator.CalculateVesselIndex(vesselRow, vesselCol);
                const auto convertVesselHorizontalSpacing = calculator.CalculateColumnOffset(vesselCol, vesselWidth, vesselHorizontalSpacing);
                const auto convertVesselVerticalSpacing = calculator.CalculateRowOffset(vesselRow, vesselHeight, vesselVerticalSpacing);
                const auto vesselPosX = calculator.CalculatePosX(vesselStartX, vesselCol, vesselWidth, convertVesselHorizontalSpacing);
                const auto vesselPosY = calculator.CalculatePosY(vesselStartY, vesselRow, vesselHeight, convertVesselVerticalSpacing);

                const auto vessel = std::make_shared<Vessel>();

                vessel->SetIndex(vesselIndex);
                vessel->SetRowIndex(vesselRow);
                vessel->SetColumnIndex(vesselCol);
                vessel->SetShape(vesselShape);
                vessel->SetWidth(vesselWidth);
                vessel->SetHeight(vesselHeight);
                vessel->SetX(vesselPosX);
                vessel->SetY(vesselPosY);
                vessel->SetWellCount(wellRows, wellColumns);
                vesselMap->AddVessel(vessel);
                
                for (auto wellRow = 0; wellRow < wellRows; ++wellRow) {
                    for (auto wellCol = 0; wellCol < wellColumns; ++wellCol) {
                        const auto wellIndex = calculator.CalculateWellIndex(wellRow, wellCol);
                        const auto offsetWellHorizontalSpacing = calculator.CalculateColumnOffset(wellCol, wellWidth, wellHorizontalSpacing);
                        const auto offsetWellVerticalSpacing = calculator.CalculateRowOffset(wellRow, wellHeight, wellVerticalSpacing);
                        const auto wellPosX = calculator.CalculatePosX(wellStartX, wellCol, wellWidth, offsetWellHorizontalSpacing);
                        const auto wellPosY = calculator.CalculatePosY(wellStartY, wellRow, wellHeight, offsetWellVerticalSpacing);
                        const auto wellSystemPosX = calculator.CalculateWellSystemPos(wellPosX, vesselPosX);
                        const auto wellSystemPosY = calculator.CalculateWellSystemPos(wellPosY, vesselPosY);

                        const auto well = std::make_shared<Well>();
                        well->SetIndex(wellIndex);
                        well->SetRow(wellRow);
                        well->SetColumn(wellCol);
                        well->SetShape(wellShape);
                        well->SetWidth(wellWidth);
                        well->SetHeight(wellHeight);
                        well->SetX(wellSystemPosX);
                        well->SetY(wellSystemPosY);
                        well->SetName(SetDefaultWellName(wellRow, wellCol));

                        vesselMap->AddWell(well);
                    }
                }
            }
        }

        return vesselMap;
    }
}
