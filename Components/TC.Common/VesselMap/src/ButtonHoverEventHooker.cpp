﻿#include <QEvent>
#include <QPushButton>

#include "ButtonHoverEventHooker.h"

namespace TC {
    struct ButtonHoverEventHooker::Impl {
        QString mouseHoverImage{};
        QString mouseLeaveImage{};
    };

    ButtonHoverEventHooker::ButtonHoverEventHooker(const QString& normal, const QString& hover, QObject* parent)
    : QObject(parent), d{std::make_unique<Impl>()} {
        d->mouseLeaveImage = normal;

        if (hover.isEmpty()) {
            d->mouseHoverImage = normal;
        }
        else {
            d->mouseHoverImage = hover;
        }
    }

    ButtonHoverEventHooker::~ButtonHoverEventHooker() {
    }

    auto ButtonHoverEventHooker::eventFilter(QObject* watched, QEvent* event) -> bool {
        if (!watched) {
            return false;
        }

        const auto button = qobject_cast<QAbstractButton*>(watched);
        
        if (!button) {
            return false;
        }

        if (event->type() == QEvent::Enter) {
            button->setIcon(QIcon(d->mouseHoverImage));
            return true;
        }
        if (event->type() == QEvent::Leave) {
            button->setIcon(QIcon(d->mouseLeaveImage));
            return true;
        }

        return QObject::eventFilter(watched, event);
    }
}
