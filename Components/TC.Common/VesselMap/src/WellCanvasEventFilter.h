﻿#pragma once
#include <QObject>
#include <QGraphicsView>

#include "GraphicsItemDefine.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvasEventFilter final : public QObject {
        Q_OBJECT
	public:
        using Self = WellCanvasEventFilter;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvasEventFilter(QGraphicsView* view);
        ~WellCanvasEventFilter() override;

        auto ZoomIn(bool centerZoom = false) const -> void;
        auto ZoomOut(bool centerZoom = false) const -> void;
        auto ZoomFit() const -> void;

    private:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    signals:
        void sigMenuSelected(WellCanvasMenu);
        void sigRequestDrawPreview(double x, double y, double w, double h);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
