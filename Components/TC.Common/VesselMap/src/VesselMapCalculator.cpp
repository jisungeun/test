﻿#include "VesselMapCalculator.h"

namespace TC {
    struct VesselMapCalculator::Impl {
        double vesselWidth{0.0};
        double vesselHeight{0.0};
        int32_t vesselRows{kInvalid};
        int32_t vesselColumns{kInvalid};

        double wellWidth{0.0};
        double wellHeight{0.0};
        int32_t wellRows{kInvalid};
        int32_t wellColumns{kInvalid};

        VesselIndex currentVesselIndex{kInvalid};
    };

    VesselMapCalculator::VesselMapCalculator() : d{std::make_unique<Impl>()} {
    }

    VesselMapCalculator::VesselMapCalculator(const VesselMapCalculator& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    VesselMapCalculator::~VesselMapCalculator() {
    }

    auto VesselMapCalculator::operator=(const VesselMapCalculator& other) -> VesselMapCalculator& {
        *d = *other.d;
        return *this;
    }

    auto VesselMapCalculator::SetVesselWidth(double width) -> void {
        d->vesselWidth = width;
    }

    auto VesselMapCalculator::SetVesselHeight(double height) -> void {
        d->vesselHeight = height;
    }

    auto VesselMapCalculator::SetVesselRows(int32_t rows) -> void {
        d->vesselRows = rows;
    }

    auto VesselMapCalculator::SetVesselColumns(int32_t columns) -> void {
        d->vesselColumns = columns;
    }

    auto VesselMapCalculator::SetWellWidth(double width) -> void {
        d->wellWidth = width;
    }

    auto VesselMapCalculator::SetWellHeight(double height) -> void {
        d->wellHeight = height;
    }

    auto VesselMapCalculator::SetWellRows(int32_t rows) -> void {
        d->wellRows = rows;
    }

    auto VesselMapCalculator::SetWellColumns(int32_t columns) -> void {
        d->wellColumns = columns;
    }

    auto VesselMapCalculator::CalculateVesselIndex(int32_t row, int32_t column) -> VesselIndex {
        const auto index = row * d->vesselColumns + column;
        d->currentVesselIndex = index; // to calculate wellindex
        return index;
    }

    auto VesselMapCalculator::CalculateWellIndex(int32_t row, int32_t column) -> WellIndex {
        const auto totalWellsByEachVessel = d->wellRows * d->wellColumns;
        const auto index = (row * d->wellColumns) + column + (totalWellsByEachVessel * d->currentVesselIndex);
        return index;
    }

    auto VesselMapCalculator::CalculateRowOffset(int32_t row, double height, double rowSpacing) -> double {
        const auto offset = (row == 0) ? 0.0 : ((rowSpacing - height) * row);
        return offset;
    }

    auto VesselMapCalculator::CalculateColumnOffset(int32_t column, double width, double columnSpacing) -> double {
        const auto offset = (column == 0) ? 0.0 : ((columnSpacing - width) * column);
        return offset;
    }

    auto VesselMapCalculator::CalculatePosX(double startX, int32_t column, double width, double offset) -> double {
        const auto x = startX + (column * width + offset);
        return x;
    }

    auto VesselMapCalculator::CalculatePosY(double startY, int32_t row, double height, double offset) -> double {
        const auto y = startY - (row * height + offset);
        return /*-*/y;
    }

    auto VesselMapCalculator::CalculateWellSystemPos(double localWellPos, double vesselPos) -> double {
        return localWellPos + vesselPos;
    }
}
