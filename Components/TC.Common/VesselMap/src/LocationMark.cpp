﻿#include "LocationMark.h"

namespace TC {
    struct LocationMark::Impl {
        MarkIndex index{kInvalid};
        MarkType type{MarkType::Point};
        Position3D pos{0.0, 0.0, 0.0};
        Size2D size{0.0, 0.0};
    };

    LocationMark::LocationMark() : d{std::make_unique<Impl>()} {
    }

    LocationMark::LocationMark(const LocationMark& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    LocationMark::~LocationMark() {
    }

    auto LocationMark::operator=(const LocationMark& other) -> LocationMark& {
        *d = *other.d;
        return *this;
    }

    //auto LocationMark::operator==(const LocationMark& other) const -> bool {
    //    return *this == other;
    //}

    auto LocationMark::SetIndex(MarkIndex index) -> void {
        d->index = index;
    }

    auto LocationMark::GetIndex() const -> MarkIndex {
        return d->index;
    }

    auto LocationMark::SetType(MarkType type) -> void {
        d->type = type;
    }

    auto LocationMark::GetType() const -> MarkType {
        return d->type;
    }

    auto LocationMark::SetPosition(double x, double y, double z) -> void {
        d->pos = {x, y, z};
    }

    auto LocationMark::GetPosition() const -> Position3D {
        return d->pos;
    }

    auto LocationMark::GetPosX() const -> double {
        return d->pos.x;
    }

    auto LocationMark::GetPosY() const -> double {
        return d->pos.y;
    }

    auto LocationMark::GetPosZ() const -> double {
        return d->pos.z;
    }

    auto LocationMark::SetSize(double w, double h) -> void {
        d->size = {w, h};
    }

    auto LocationMark::GetSize() const -> Size2D {
        return d->size;
    }

    auto LocationMark::GetWidth() const -> double {
        return d->size.w;
    }

    auto LocationMark::GetHeight() const -> double {
        return d->size.h;
    }
}
