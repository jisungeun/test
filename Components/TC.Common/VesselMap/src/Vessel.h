﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"

namespace TC {
    class Vessel {
    public:
        using Self = Vessel;
        using Pointer = std::shared_ptr<Self>;

        Vessel();
        Vessel(const Vessel& other);
        ~Vessel();

        auto operator=(const Vessel& other) -> Vessel&;

        auto SetIndex(VesselIndex index) -> void;
        auto GetIndex() const -> VesselIndex;

        auto SetShape(VesselShape shape) -> void;
        auto GetShape() const -> VesselShape;

        auto SetX(double x) -> void;
        auto GetX() const -> double;
        auto SetY(double y) -> void;
        auto GetY() const -> double;

        auto SetWidth(double width) -> void;
        auto GetWidth() const -> double;
        auto SetHeight(double height) -> void;
        auto GetHeight() const -> double;

        auto SetRowIndex(int32_t row) -> void;
        auto GetRowIndex() const -> int32_t;
        auto SetColumnIndex(int32_t column) -> void;
        auto GetColumnIndex() const -> int32_t;

        auto SetWellCount(int32_t wellRows, int32_t wellCols) -> void;
        auto GetWellRows() const -> int32_t;
        auto GetWellCols() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
