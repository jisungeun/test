﻿#include "ImagingArea.h"

namespace TC {
    struct ImagingArea::Impl {
        ImagingAreaShape shape{ImagingAreaShape::Circle};

        double x{0.0};
        double y{0.0};

        double width{0.0};
        double height{0.0};
    };

    ImagingArea::ImagingArea() : d{std::make_unique<Impl>()} {
    }

    ImagingArea::ImagingArea(const ImagingArea& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    ImagingArea::~ImagingArea() {
    }

    auto ImagingArea::operator=(const ImagingArea& other) -> ImagingArea& {
        *d = *other.d;
        return *this;
    }

    auto ImagingArea::SetShape(ImagingAreaShape shape) -> void {
        d->shape = shape;
    }

    auto ImagingArea::GetShape() const -> ImagingAreaShape {
        return d->shape;
    }

    auto ImagingArea::SetX(double x) -> void {
        d->x = x;
    }

    auto ImagingArea::GetX() const -> double {
        return d->x;
    }

    auto ImagingArea::SetY(double y) -> void {
        d->y = y;
    }

    auto ImagingArea::GetY() const -> double {
        return d->y;
    }

    auto ImagingArea::SetWidth(double width) -> void {
        d->width = width;
    }

    auto ImagingArea::GetWidth() const -> double {
        return d->width;
    }

    auto ImagingArea::SetHeight(double height) -> void {
        d->height = height;
    }

    auto ImagingArea::GetHeight() const -> double {
        return d->height;
    }
}
