﻿#pragma once

#include <QGraphicsView>

#include "GraphicsItemDefine.h"
#include "LocationDataRepo.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapDataRepo.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellCanvas : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = WellCanvas;
        using Pointer = std::shared_ptr<Self>;

        explicit WellCanvas(QWidget* parent = nullptr);
        ~WellCanvas() override;

        auto SetViewMode(ViewMode mode)->void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto ClearAll() -> void;

        auto ClearScene() -> void;

        auto ShowLensItem(bool show) -> void;
        auto SetLensPosition(double x, double y) -> void;
        auto GetLensPosition() const -> Position2D;

        auto SetSceneRectAndItemSize(WellIndex wellIndex) -> void;
        auto GetSceneRect() const -> QRectF;
        auto SetCurrentWell(WellIndex wellIndex) -> void;

        auto AddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;

        auto AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetSelectedAcqItems(const QList<QPair<WellIndex, AcquisitionIndex>>& selected) ->void;
        auto SetShowAcqItem(AcquisitionIndex index)->void;

        auto ShowPreviewArea(bool show) -> void;
        auto IsPreviewAreaVisible() -> bool;
        auto SetPreviewLocation(double x, double y, double w, double h) -> void;
        auto GetPreviewLocation() const -> Geometry2D;
        auto SetPreviewLocationEditable(bool editable) -> void;
        auto SetPreviewLocationSelected(bool selected) -> void;

        auto ShowTileImagingArea(bool show) -> void;
        auto SetTileImagingArea(double x, double y, double z, double w, double h) -> void;
        auto SetTileImagingAreaCenter(double xInMm, double yInMm, double zInMm)->void;

        auto ShowGridLine(bool show) -> void;

        auto SetCurrentAcquisitionLocation(LocationType type, double xInMM, double yInMM)->void;
        auto FitInView()->void;

        auto SetMatrixItemVisible(bool visible) -> void;
        auto CreateMatrixItems(const QList<QPair<double,double>>& positions) -> void;

    protected:
        auto keyPressEvent(QKeyEvent* event) -> void override;
        auto keyReleaseEvent(QKeyEvent* event) -> void override;

        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QMouseEvent* event) -> void override;
        auto mouseMoveEvent(QMouseEvent* event) -> void override;
        auto resizeEvent(QResizeEvent* event) -> void override;

    signals:
        void sigWellCanvasDblClicked(double x, double y);
        void sigTileAreaGeometryChanged(const double& x, const double& y, const double& z, const double& w, const double& h);
        void sigPreviewItemGeometryChanged(const double& x, const double& y, const double& w, const double& h);

        void sigDeleteMarkItem(const WellIndex& wellIndex, const MarkIndex& markIndex);
        void sigDeleteAcqItem(const WellIndex& wellINdex, const AcquisitionIndex& acqIndex);

        void sigCurrentSelectedAcqItems(const QList<QPair<WellIndex, AcquisitionIndex>>&);

        void sigMarkItemDblClicked(const WellIndex&, const MarkIndex&, double x, double y);
        void sigAcquisitionItemDblClicked(const WellIndex&, const AcquisitionIndex&, double x, double y, double z);

        void sigImportAcquisitionToMark(const WellIndex&, const AcquisitionIndex&);

    private slots:
        void onSceneSelectionChanged();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
