﻿#pragma once

#include <QGraphicsItem>

#include <enum.h>

namespace TC {
    enum class GraphicsItemType {
        VesselItem = QGraphicsItem::UserType,
        WellItem,
        ImagingAreaItem,
        MarkPointItem,
        MarkTileItem,
        AcqPointItem,
        AcqTileItem,
        PreviewItem,
        LensItem,
        TileImagingItem,
        RowColIndexItem,
        CurrLocationItem,
        MatrixItem
    };

    BETTER_ENUM (WellCanvasMenu, 
                 int32_t,
                 None,
                 MarkToAcquisition,
                 AcquisitionToMark,
                 AddMark,
                 AddAcquisitionPoint,
                 AddAcquisitionTile,
                 AddPreview
    );

    struct Geometry2D {
        double x{0.0};
        double y{0.0}; // 실제 저장된 값의 반대 부호를 넣어줘야 함
        double w{0.0};
        double h{0.0};
        auto operator==(const Geometry2D& other) const -> bool {
                if(fabs(x-other.x)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(y-other.y)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(w-other.w)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(h-other.h)>std::numeric_limits<double>::epsilon()) return false;
                return true;
            }
            auto operator!=(const Geometry2D& other) const -> bool { return !(*this==other);}
    };
    
    struct Geometry3D {
        double x{0.0};
        double y{0.0}; // 실제 저장된 값의 반대 부호를 넣어줘야 함
        double z{0.0};
        double w{2.5};
        double h{2.5};
        auto operator==(const Geometry2D& other) const -> bool {
                if(fabs(x-other.x)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(y-other.y)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(w-other.w)>std::numeric_limits<double>::epsilon()) return false;
                if(fabs(h-other.h)>std::numeric_limits<double>::epsilon()) return false;
                return true;
            }
            auto operator!=(const Geometry2D& other) const -> bool { return !(*this==other);}
    };

    // 클 수록 실제 보이는 건 작아짐
    constexpr double kDataNaviPointRatio = 25;
    constexpr double kPointRatio = 40;
    constexpr double kLensRatio = 55;
    constexpr double kHandleRatio = 60;
    constexpr double kLineWidthRatio = 140; // 클 수록 실제 보이는건 얇아짐

    constexpr double kMinLocPointSize = 0.07;
    constexpr double kMinLensPointSize = 0.06;

    BETTER_ENUM(GraphicsItemZValue, int32_t,
        PreviewNonEditable = 1,
        Mark = 15,
        Matrix = 40,
        Acquisition = 50,
        SelectedLocation = 70,
        PreviewEditable = 85,
        Lens = 90,
        TileImaging = 300
    );
}
