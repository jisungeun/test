﻿#pragma once

#include <QColor>

namespace TC {
    // common color

    namespace odcolorlist {
        const QColor odGray900("#172023");
        const QColor odGray800("#1B2629"); // TSX 배경
        const QColor odGray700("#243135");
        const QColor odGray600("#2D3E43");
        const QColor odGray500("#334449"); // default well color
        const QColor odGray400("#394C53");
        const QColor odGray300("#5f6F7A");
        const QColor odGray200("#6F8290"); // label color
        const QColor odWhite("#FFFFFF");
        const QColor odWhiteGray("#BBBBBB");
        const QColor odBlack("#000000");
        const QColor odBackground("#12191B");
        const QColor odPrimary("#00C2D3"); // 하늘색(토모큐브 로고색)
        const QColor odPrimarySolid("#27A2BF"); // 좀 진한 하늘색
        const QColor odSecondary("#5B8B97"); // 좀 짙은 하늘색
        const QColor odRed("#D55C56");
        const QColor odGreen("#314B33"); // 짙은 초록
        const QColor odGreenFocus("#64BC6A"); // 짙은 초록
    }

    const QColor WellPosNameHighlightColor(odcolorlist::odPrimary);

    const QColor TileBorderColor("#ff8522");
    const QColor GridLineColor(odcolorlist::odGray200);
    const QColor GridCenterColor(odcolorlist::odRed);

    const QColor WellMapBorderColor(odcolorlist::odPrimary);
    const QColor ImagingAreaBorderColor(odcolorlist::odGray300);
    const QColor ImagingAreaCenterColor(odcolorlist::odGray400);

    const QColor SceneBackgroundColor(odcolorlist::odGray800);
    const QColor PreviewAreaColor(odcolorlist::odWhite);
    const QColor PreviewBorderColor(odcolorlist::odWhiteGray);

    const QColor RowColumnIndexColor(odcolorlist::odGray200); // vessel canvas
    const QColor VesselBackgroundColor(odcolorlist::odGray800); // vessel canvas
    const QColor DefaultWellColor(odcolorlist::odGray500); // vessel canvas
    const QColor WellNameColor(odcolorlist::odWhite); // vessel canvas
    const QColor WellBorderColor(odcolorlist::odPrimary); // vessel canvas
    const QColor FocusIndicatorColor(odcolorlist::odWhiteGray); // vessel canvas
    const QColor VesselBorderColor(odcolorlist::odGray200);

    const QColor LensPointColor(odcolorlist::odPrimary);
    const QColor MarkPointColor(odcolorlist::odSecondary);
    const QColor AcquisitionPointColor(odcolorlist::odGreenFocus);
    const QColor SelectedWellBorderColor(odcolorlist::odPrimarySolid);
    const QColor MatrixPointColor(odcolorlist::odRed);

    const QColor RoiColor(odcolorlist::odPrimary);
}
