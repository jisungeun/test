﻿#pragma once

#include <memory>
#include <optional>

#include <QCoreApplication>
#include <QColor>

#include "LocationDataRepo.h"
#include "VesselMap.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapDataRepo.h"

namespace TC {
    class WellGroup;
    class Well;

    class VesselMapWidgetControl {
        Q_DECLARE_TR_FUNCTIONS(VesselMapWidgetControl)

    public:

        using Self = VesselMapWidgetControl;
        using Pointer = std::shared_ptr<Self>;

        VesselMapWidgetControl();
        ~VesselMapWidgetControl();

        auto ClearAll() -> void;

        auto Initialize() -> void;
        auto SetVesselMap(const VesselMap::Pointer& vesselMap) -> void;
        auto GetVesselMapDataRepo() const -> VesselMapDataRepo::Pointer;
        auto GetLocationDataRepo() const -> LocationDataRepo::Pointer;

        auto GetWellIndex(int32_t row, int32_t column) const -> WellIndex;
        auto GetWellRow(WellIndex wellIndex) const -> int32_t;
        auto GetWellColumn(WellIndex wellIndex) const -> int32_t;

        auto SetCurrentWellIndex(WellIndex wellIndex) -> void;
        auto GetCurrentWellIndex() const -> WellIndex;

        auto SetLensPosition(double x, double y, double z) -> void;
        auto GetLensPosX() const -> double;
        auto GetLensPosY() const -> double;
        auto GetLensPosZ() const -> double;
        auto GetWellPos2DByGlobalLensPos() const -> Position2D;

        auto GetHolder() const -> std::shared_ptr<Holder>;
        auto GetVessels() const -> QList<std::shared_ptr<Vessel>>;
        auto GetWells() const -> QList<std::shared_ptr<Well>>;
        auto GetImagingArea() const -> std::shared_ptr<ImagingArea>;
        auto GetGroups() const -> QList<std::shared_ptr<WellGroup>>;
        auto GetGroupIndex(const QString& groupName) const -> GroupIndex;
        auto IsExistGroup(const QString& groupName) const -> bool;
        
        auto CreateNewGroup(GroupIndex groupIndex, 
                            const QString& groupName,
                            const QColor& groupColor,
                            const QList<int>& wellIndices) -> bool;
        auto DeleteGroup(GroupIndex groupIndex) -> bool;

        auto GetWellsNotInGroup(const QList<WellIndex>& wellIndices) -> QList<WellIndex>;
        auto GetWellsInGroup(const QList<WellIndex>& wellIndices) -> QList<WellIndex>;

        auto AddWellsToGroup(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> bool;
        auto RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> bool;
        auto ChangeGroupName(GroupIndex groupIndex, const QString& name) -> void;
        auto ChangeGroupColor(GroupIndex groupIndex, const QColor& color) -> void;
        auto ChangeWellName(WellIndex wellIndex, const QString& name) -> void;
        auto MoveGroup(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void;

        auto GetAllVesselIndices() -> QList<VesselIndex>;
        auto SetCurrentVesselIndex(VesselIndex vesselIndex) -> void;
        auto GetCurrentVesselIndex() const -> VesselIndex;

        auto AddMarkLocationBySystemCoord(WellIndex wellIndex, const MarkType& type, double x, double y, double z, double w, double h) -> MarkIndex;
        auto AddMarkLocationByWellCoord(WellIndex wellIndex, const MarkType& type, double x, double y, double z, double w, double h) -> MarkIndex;
        auto SetMarkLocationBySystemCoord(WellIndex wellIndex, MarkIndex markIndex, const MarkType& type, double x, double y, double z, double w, double h) -> void;
        auto SetMarkLocationByWellCoord(WellIndex wellIndex, MarkIndex markIndex, const MarkType& type, double x, double y, double z, double w, double h) -> void;
        auto DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto GetMarkLocationPosition(WellIndex wellIndex, MarkIndex markIndex) const ->std::optional<Position3D>;

        auto AddAcquisitionLocationByWorldCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool;
        auto AddAcquisitionLocationByWellCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> bool;
        auto SetAcquisitionLocationBySystemCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void;
        auto SetAcquisitionLocationByWellCoord(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, const AcquisitionType& type, double x, double y, double z, double w, double h) -> void;
        auto DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto GetAcquisitionLocationPosition(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) const -> std::optional<Position3D>;

        auto GetFirstWellIndex() -> WellIndex;

        auto SetPreviewPresetSize(double widthMM, double heightMM) -> void;
        auto GetPreviewPresetWidth() const -> double;
        auto GetPreviewPresetHeight() const -> double;

        auto SetImagingOrder(const ImagingOrder& imagingOrder) -> void;
        auto GetImagingOrder() const -> ImagingOrder;

        auto GenerateSortedWells() -> QList<WellIndex>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
