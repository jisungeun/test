﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"
#include "VesselMap.h"

namespace TC {
    class VesselMapBuilderControl {
    public:
        using Self = VesselMapBuilderControl;
        using Pointer = std::shared_ptr<Self>;

        VesselMapBuilderControl();
        VesselMapBuilderControl(const VesselMapBuilderControl& other);
        ~VesselMapBuilderControl();

        auto operator=(const VesselMapBuilderControl& other) -> VesselMapBuilderControl&;

        auto SetHolderWidth(double width) -> void;
        auto SetHolderHeight(double height) -> void;

        auto SetVesselShape(VesselShape shape) -> void;
        auto SetVesselStartX(double x) -> void;
        auto SetVesselStartY(double y) -> void;
        auto SetVesselWidth(double width) -> void;
        auto SetVesselHeight(double height) -> void;
        auto SetVesselRows(int32_t rows) -> void;
        auto SetVesselColumns(int32_t columns) -> void;
        auto SetVesselHSpacing(double spacing) -> void;
        auto SetVesselVSpacing(double spacing) -> void;

        auto SetWellShape(WellShape shape) -> void;
        auto SetWellStartX(double x) -> void;
        auto SetWellStartY(double y) -> void;
        auto SetWellWidth(double width) -> void;
        auto SetWellHeight(double height) -> void;
        auto SetWellRows(int32_t rows) -> void;
        auto SetWellColumns(int32_t columns) -> void;
        auto SetWellHSpacing(double spacing) -> void;
        auto SetWellVSpacing(double spacing) -> void;

        auto SetImagingAreaShape(ImagingAreaShape shape) -> void;
        auto SetImagingAreaCenterX(double x) -> void;
        auto SetImagingAreaCenterY(double y) -> void;
        auto SetImagingAreaWidth(double width) -> void;
        auto SetImagingAreaHeight(double height) -> void;

        auto GenerateVesselMap() const -> VesselMap::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
