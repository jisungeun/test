﻿#pragma once
#include <QObject>

namespace TC {
    class ButtonHoverEventHooker final : public QObject {
        Q_OBJECT
    public:
        explicit ButtonHoverEventHooker(const QString& normal,
                                        const QString& hover = {},
                                        QObject* parent = nullptr);

        ~ButtonHoverEventHooker() override;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
