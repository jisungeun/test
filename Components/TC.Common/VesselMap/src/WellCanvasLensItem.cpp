﻿#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "WellCanvasLensItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct WellCanvasLensItem::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        double pointSize{kMinLensPointSize};
        QColor pointColor{LensPointColor};
        auto UpdatePointSize(const double& lod) -> void;
    };

    auto WellCanvasLensItem::Impl::UpdatePointSize(const double& lod) -> void {
        pointSize = 9 / lod;
        if (pointSize < kMinLensPointSize) {
            pointSize = kMinLensPointSize;
        }
        self->update();
    }

    WellCanvasLensItem::WellCanvasLensItem(Position2D lensPos, QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>(this)} {
        setZValue(GraphicsItemZValue::Lens);
        SetPosition(lensPos.x, lensPos.y);
        setCacheMode(DeviceCoordinateCache);
    }

    WellCanvasLensItem::~WellCanvasLensItem() {
    }

    auto WellCanvasLensItem::SetVisible(bool show) -> void {
        setVisible(show);
    }

    auto WellCanvasLensItem::SetPosition(double x, double y) -> void {
        setPos(x, -y);
    }

    auto WellCanvasLensItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::LensItem);
    }

    auto WellCanvasLensItem::shape() const -> QPainterPath {
        QPainterPath path;
        path.addEllipse(boundingRect());
        return path;
    }

    auto WellCanvasLensItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize);
    }

    auto WellCanvasLensItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        d->UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        painter->setPen(Qt::NoPen);
        painter->setBrush(d->pointColor);
        painter->drawEllipse(boundingRect());
    }
}
