﻿#pragma once

#include <memory>
#include <QString>

#include "VesselMapExternalData.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    class ImagingPointTableData final {
    public:
        using Self = ImagingPointTableData;
        using Pointer = std::shared_ptr<Self>;

        ImagingPointTableData();
        ImagingPointTableData(const Self& rhs);
        ImagingPointTableData(Self&& rhs) noexcept;

        ~ImagingPointTableData();

        auto operator=(const Self& rhs) -> Self&;
        auto operator=(Self&& rhs) noexcept -> Self&; 

        auto operator==(const Self& rhs) const -> bool;
        auto operator!=(const Self& rhs) const -> bool;

        auto SetAcquisitionIndex(const AcquisitionIndex& index) -> void;
        auto GetAcquisitionIndex() const -> AcquisitionIndex;

        auto SetWellPosName(const QString& wellPosName) -> void;
        auto GetWellPosName() const -> QString;

        auto SetWellIndex(const WellIndex& wellIndex) -> void;
        auto GetWellIndex() const -> WellIndex;

        auto SetPointIndexName(const QString& pointIndexString) const -> void;
        auto GetPointIndexName() const -> QString;

        auto SetPosition(const Position3D& pos) -> void;
        auto SetPosition(const double& x, const double& y, const double& z) -> void;
        auto GetPosition() const -> Position3D;
        auto GetPosX() const -> double;
        auto GetPosY() const -> double;
        auto GetPosZ() const -> double;

        auto SetSize(const Size2D& size) -> void;
        auto SetSize(const double& w, const double& h) -> void;
        auto GetSize() const -> Size2D;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetRoiIndexName(const QString& roiName) -> void;
        auto GetRoiIndexName() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
