﻿#include <QStyleOptionGraphicsItem>
#include <QPainter>

#include "WellCanvasSelectedPointItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct WellCanvasSelectedPointItem::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        QColor pointColor{};
        QColor borderColor{SelectedWellBorderColor};
        double pointSize{};

        auto UpdatePointSize(const double& lod) -> void;
    };

    auto WellCanvasSelectedPointItem::Impl::UpdatePointSize(const double& lod) -> void {
        pointSize = 9 / lod;
        if (pointSize < kMinLocPointSize) {
            pointSize = kMinLocPointSize;
        }
        self->update();
    }

    WellCanvasSelectedPointItem::WellCanvasSelectedPointItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>(this)} {
        setFlag(ItemIsSelectable, true);
        setFlag(ItemIsFocusable, true);
        setFlag(ItemIsMovable, false);

        setZValue(GraphicsItemZValue::SelectedLocation);
    }

    WellCanvasSelectedPointItem::~WellCanvasSelectedPointItem() {
    }

    auto WellCanvasSelectedPointItem::SetLocatinoType(LocationType locType) -> void {
        switch (locType) {
            case LocationType::MarkLocation:
                d->pointColor = MarkPointColor;
                break;
            case LocationType::AcquisitionLocation:
                d->pointColor = AcquisitionPointColor;
                break;
        }
        update();
    }

    auto WellCanvasSelectedPointItem::SetPosition(double x, double y) -> void {
        prepareGeometryChange();
        setPos(x, -y);
        update();
    }

    auto WellCanvasSelectedPointItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::CurrLocationItem);
    }

    auto WellCanvasSelectedPointItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize);
    }

    auto WellCanvasSelectedPointItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)

        d->UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        auto brush = d->pointColor;
        auto pen = QPen(Qt::NoPen);
        if (option->state & QStyle::State_Selected) {
            pen.setColor(d->borderColor);
            pen.setWidth(0);
            pen.setStyle(Qt::SolidLine);

            brush = brush.darker();
        }

        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawEllipse(boundingRect());
    }
}
