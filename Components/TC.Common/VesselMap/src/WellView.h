﻿#pragma once

#include <memory>

#include <QWidget>

#include "GraphicsItemDefine.h"
#include "LocationDataRepo.h"
#include "VesselMapCustomDataTypes.h"
#include "VesselMapDataRepo.h"
#include "VesselMapExternalData.h"

namespace TC {
    class WellView : public QWidget {
        Q_OBJECT
    public:
        using Self = WellView;

        explicit WellView(QWidget* parent = nullptr);
        ~WellView() override;

        auto SetViewMode(ViewMode mode)->void;
        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto ClearAll() -> void;
        auto ClearScene() -> void;

        auto ShowLensItem(bool show) -> void;
        auto SetLensPosition(double x, double y, double z) -> void;
        auto SetLensPositionRange(const VesselAxis& axis, const double& min, const double& max) -> void;
        auto SetLensPositionEditable(bool editable) -> void;

        auto SetSceneRect(WellIndex wellIndex) -> void;
        auto SetCurrentFocusWell(WellIndex wellIndex) -> void;

        auto AddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;

        auto AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;

        auto SetPreviewLocationEditable(bool editable) -> void;
        auto SetPreviewLocation(double x, double y, double w, double h) -> void;
        auto SetPreviewLocation(const Geometry2D& geometry) -> void;
        auto SetPreviewLocationVisible(bool show) -> void;

        auto ShowTileImagingArea(bool show) -> void;;
        auto SetTileImagingArea(double x, double y, double z, double w, double h) -> void;
        auto SetTileImagingAreaCenter(double xInMm, double yInMm, double zInMm)->void;

        auto SetToolVisible(bool visible)->void;

        auto SetCurrentAcquisitionLocation(LocationType locationType, double xMM, double yMM)->void;
        auto FitWellCanvas()->void;

        auto SavePreview() -> void;
        auto RestorePreview() -> void;

        auto SetPreviewItemSelected(bool selected) -> void;

        auto SetMatrixItemVisible(bool visible) -> void;
        auto SetMatrixItems(const QList<QPair<double,double>>& positions) -> void;

    signals:
        void sigDoubleClicked(double x, double y);
        void sigPreviewGeometryChangedOnWellCanvas(const double& x, const double& y, const double& w, const double& h);
        void sigTileImagingGeoChangedOnWellCanvas(const double&x, const double& y, const double& z, const double& w, const double& h);

        void sigDeleteMarkItem(const WellIndex& wellIndex, const MarkIndex& markIndex);
        void sigDeleteAcqItem(const WellIndex& wellINdex, const AcquisitionIndex& acqIndex);

        void sigCurrentSelectedAcqItems(const QList<QPair<WellIndex, AcquisitionIndex>>&);

        void sigMarkItemDblClicked(const WellIndex&, const MarkIndex&, double x, double y);
        void sigAcqItemDblClicked(const WellIndex&, const AcquisitionIndex&, double x, double y, double z);

        void sigChangeLensPosition(const TC::VesselAxis axis,const double position);

        void sigImportAcquisitionToMark(const WellIndex&, const AcquisitionIndex&);

    public slots:
        void onUpdateTableSectionByImgPtTable(const QList<QPair<WellIndex, AcquisitionIndex>>& selected);
        void onChangeShowGridState(bool toggled);
        void onPositionSpinBoxEditingFinished();

    private:
        auto resizeEvent(QResizeEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
