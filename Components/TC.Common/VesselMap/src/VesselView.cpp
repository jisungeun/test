﻿#include <QMouseEvent>

#include "VesselView.h"
#include "ui_VesselView.h"

namespace TC {
    struct VesselView::Impl {
        Ui::VesselView *ui{nullptr};

        auto Connect(const Self* self) -> void;
    };

    auto VesselView::Impl::Connect(const Self* self) -> void {
        connect(ui->canvas, &VesselCanvas::sigSelectedWellIndices, self, &Self::sigSelectedWellIndices);
        connect(ui->canvas, &VesselCanvas::sigSelectedGroupIndices, self, &Self::sigSelectedGroupIndices);
        connect(ui->canvas, &VesselCanvas::sigVesselCanvasDblClicked, self, &Self::sigVesselCanvasDblClicked);
        connect(ui->canvas, &VesselCanvas::sigChangeSelectWell, self, &Self::sigChangeSelectWell);
    }

    VesselView::VesselView(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()}{
        d->ui = new Ui::VesselView();
        d->ui->setupUi(this);
        d->ui->baseWidget->setObjectName("panel");

        d->Connect(this);
    }

    VesselView::~VesselView() {
        delete d->ui;
    }
    
    auto VesselView::SetViewMode(ViewMode mode) -> void {
        // TODO row / column widget font size / position setting
        d->ui->canvas->SetViewMode(mode);
    }

    auto VesselView::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->ui->canvas->SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto VesselView::ClearAll() -> void {
        d->ui->canvas->ClearAll();
    }

    auto VesselView::ClearScene() -> void {
        d->ui->canvas->ClearScene();
    }

    auto VesselView::UpdateSceneRect(VesselIndex vesselIndex) -> void {
        d->ui->canvas->UpdateSceneRect(vesselIndex);
    }

    auto VesselView::DrawVesselItem() -> void {
        d->ui->canvas->DrawVesselItem();
    }

    auto VesselView::DrawWellItem() -> void {
        d->ui->canvas->DrawWellItem();
    }

    auto VesselView::DrawRowColumnTextItem(VesselIndex vesselIndex) -> void {
        d->ui->canvas->DrawRowColumnTextItem(vesselIndex);
    }

    auto VesselView::SetEditMode(EditMode mode) -> void {
        d->ui->canvas->SetEditMode(mode);
    }

    auto VesselView::SetLensPosition(double x, double y) -> void {
        d->ui->canvas->SetLensPosition(x,y);
    }

    auto VesselView::SetSelectedWell(WellIndex wellIndex) -> void {
        d->ui->canvas->SetSelectedWell(wellIndex);
    }

    auto VesselView::GetSelectedWellIndices() const -> QList<WellIndex>& {
        return d->ui->canvas->GetSelectedWellIndices();
    }

    auto VesselView::GetSelectedGroupIndices() const -> QList<GroupIndex>& {
        return d->ui->canvas->GetSelectedGroupIndices();
    }

    auto VesselView::GetFocusedWellIndex() const -> WellIndex {
        return d->ui->canvas->GetFocusedWellIndex();
    }

    auto VesselView::ShowImagingOrder(bool show) -> void {
        d->ui->canvas->ShowImagingOrder(show);
    }

    auto VesselView::SetWellImagingOrder(const QList<WellIndex>& wells) -> void {
        d->ui->canvas->SetWellItemOrderNumber(wells);
    }

    auto VesselView::UpdateWellGroupCreated(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void {
        d->ui->canvas->UpdateWellGroupCreated(groupIndex, wellIndices);
    }

    auto VesselView::UpdateWellGroupDeleted(GroupIndex groupIndex) -> void {
        d->ui->canvas->UpdateWellGroupDeleted(groupIndex);
    }

    auto VesselView::UpdateWellGroupMemberAdded(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void {
        d->ui->canvas->UpdateWellGroupMemberAdded(groupIndex, wellIndices);
    }

    auto VesselView::UpdateWellGroupMemberRemoved(const QList<WellIndex>& wellIndices) -> void {
        d->ui->canvas->UpdateWellGroupMemberRemoved(wellIndices);
    }

    auto VesselView::UpdateWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void {
        d->ui->canvas->UpdateWellGroupColorChanged(groupIndex, color);
    }

    auto VesselView::UpdateWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void {
        d->ui->canvas->UpdateWellGroupMoved(movingGroupIndex, targetGroupIndex);
    }

    auto VesselView::UpdateWellNameChanged(WellIndex wellIndex, const QString& name) -> void {
        d->ui->canvas->UpdateWellNameChanged(wellIndex, name);
    }

    void VesselView::onChangeSelectedWellIndexOnGroupTable(const QList<WellIndex>& wellIndices) {
        d->ui->canvas->UpdateSelectedWells(wellIndices);
    }

    void VesselView::onChangeSelectedWellIndexByButton(const QList<WellIndex>& wellIndices) {
        d->ui->canvas->UpdateSelectedWells(wellIndices);
    }
}
