﻿#pragma once

#include <memory>

#include <QObject>
#include <QGraphicsView>

/// <summary>
/// (default modifier)ctrl + mouse wheel = zoom in/out
///	(default modifier)ctrl + mouse right btn double clicked = fit in view
/// </summary>

namespace TC {
    class CanvasZoomSupport : public QObject {
        Q_OBJECT
	public:
        using Self = CanvasZoomSupport;
        using Pointer = std::shared_ptr<Self>;

        CanvasZoomSupport(QGraphicsView* view);
        ~CanvasZoomSupport() override;

        auto GentleZoom(double factor, bool usingBtn = false) -> void;
        auto GetZoomInFactor() const -> double;
        auto GetZoomOutFactor() const -> double;

        auto SetZoomByMouseWheel(double factor) -> void;

        auto SetModifiers(Qt::KeyboardModifiers modifiers) -> void;
        auto SetBaseFactor(double value = 1.0015) -> void;

        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    signals:
        void sigZoomed();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
