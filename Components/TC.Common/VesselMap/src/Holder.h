﻿#pragma once

#include <memory>

namespace TC {
    class Holder {
    public:
        using Self = Holder;
        using Pointer = std::shared_ptr<Self>;

        Holder();
        Holder(const Holder& other);
        ~Holder();

        auto operator=(const Holder& other) -> Holder&;

        auto SetWidth(double width) -> void;
        auto GetWidth() const -> double;
        auto SetHeight(double height) -> void;
        auto GetHeight() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
