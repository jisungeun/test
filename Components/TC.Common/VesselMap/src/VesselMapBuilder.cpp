﻿#include "VesselMapBuilder.h"
#include "VesselMapBuilderControl.h"

namespace TC {
    struct VesselMapBuilder::Impl {
        VesselMapBuilderControl control;
    };

    VesselMapBuilder::VesselMapBuilder() : d{std::make_unique<Impl>()} {
    }

    VesselMapBuilder::VesselMapBuilder(const VesselMapBuilder& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    VesselMapBuilder::~VesselMapBuilder() {
    }

    auto VesselMapBuilder::operator=(const VesselMapBuilder& other) -> VesselMapBuilder& {
        *d = *other.d;
        return *this;
    }

    auto VesselMapBuilder::SetHolder(double width, double height) -> void {
        d->control.SetHolderWidth(width);
        d->control.SetHolderHeight(height);
    }

    auto VesselMapBuilder::SetHolder(HolderInput holderInput) -> void {
        d->control.SetHolderWidth(holderInput.width);
        d->control.SetHolderHeight(holderInput.height);
    }

    auto VesselMapBuilder::SetVessel(VesselInput vesselInput) -> void {
        d->control.SetVesselShape(vesselInput.shape);
        d->control.SetVesselStartX(vesselInput.startX);
        d->control.SetVesselStartY(vesselInput.startY);
        d->control.SetVesselWidth(vesselInput.width);
        d->control.SetVesselHeight(vesselInput.height);
        d->control.SetVesselRows(vesselInput.rows);
        d->control.SetVesselColumns(vesselInput.columns);
        d->control.SetVesselHSpacing(vesselInput.horizontalSpacing);
        d->control.SetVesselVSpacing(vesselInput.verticalSpacing);
    }

    auto VesselMapBuilder::SetWell(WellInput wellInput) -> void {
        d->control.SetWellShape(wellInput.shape);
        d->control.SetWellStartX(wellInput.startX);
        d->control.SetWellStartY(wellInput.startY);
        d->control.SetWellWidth(wellInput.width);
        d->control.SetWellHeight(wellInput.height);
        d->control.SetWellRows(wellInput.rows);
        d->control.SetWellColumns(wellInput.columns);
        d->control.SetWellHSpacing(wellInput.horizontalSpacing);
        d->control.SetWellVSpacing(wellInput.verticalSpacing);
    }

    auto VesselMapBuilder::SetVessel(VesselShape shape, double startX, double startY, double width, double height, int32_t rows, int32_t columns, double horizontalSpacing, double verticalSpacing) -> void {
        d->control.SetVesselShape(shape);
        d->control.SetVesselStartX(startX);
        d->control.SetVesselStartY(startY);
        d->control.SetVesselWidth(width);
        d->control.SetVesselHeight(height);
        d->control.SetVesselRows(rows);
        d->control.SetVesselColumns(columns);
        d->control.SetVesselHSpacing(horizontalSpacing);
        d->control.SetVesselVSpacing(verticalSpacing);
    }

    auto VesselMapBuilder::SetWell(WellShape shape, double startX, double startY, double width, double height, int32_t rows, int32_t columns, double horizontalSpacing, double verticalSpacing) -> void {
        d->control.SetWellShape(shape);
        d->control.SetWellStartX(startX);
        d->control.SetWellStartY(startY);
        d->control.SetWellWidth(width);
        d->control.SetWellHeight(height);
        d->control.SetWellRows(rows);
        d->control.SetWellColumns(columns);
        d->control.SetWellHSpacing(horizontalSpacing);
        d->control.SetWellVSpacing(verticalSpacing);
    }

    auto VesselMapBuilder::SetImagingArea(ImagingAreaShape shape, double centerX, double centerY, double width, double height) -> void {
        d->control.SetImagingAreaShape(shape);
        d->control.SetImagingAreaCenterX(centerX);
        d->control.SetImagingAreaCenterY(centerY);
        d->control.SetImagingAreaWidth(width);
        d->control.SetImagingAreaHeight(height);
    }

    auto VesselMapBuilder::GenerateVesselMap() -> VesselMap::Pointer {
        auto vesselMap = d->control.GenerateVesselMap();
        return vesselMap;
    }
}
