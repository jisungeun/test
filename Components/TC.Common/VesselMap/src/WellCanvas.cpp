﻿#include <QMouseEvent>
#include <QScrollBar>

#include "WellCanvas.h"
#include "WellCanvasControl.h"
#include "WellCanvasItemCreator.h"
#include "WellCanvasEventFilter.h"
#include "WellCanvasMarkItem.h"
#include "WellCanvasAcquisitionItem.h"
#include "WellCanvasLensItem.h"
#include "VesselMapWidgetColorCodes.h"
#include "WellCanvasPreviewItem.h"
#include "WellCanvasWellItem.h"
#include "WellCanvasTileImagingItem.h"
#include "WellCanvasZoomButtonGroup.h"
#include "WellCanvasSelectedPointItem.h"

namespace TC {
    struct WellCanvas::Impl {
        explicit Impl(Self* self) : self(self) {}

        Self* self{};
        WellCanvasControl control;
        WellCanvasItemCreator creator;
        QGraphicsScene* scene{nullptr};
        WellCanvasEventFilter* eventFilter{nullptr};
        WellCanvasZoomButtonGroup* zoomButtonGroup{nullptr};
        WellCanvasLensItem* lensItem{nullptr};

        WellCanvasSelectedPointItem* selectedLocationInfoItem{nullptr}; // using only datanavi app
        WellCanvasTileImagingItem* tileImagingItem{nullptr};
        WellCanvasPreviewItem* previewItem{nullptr};

        bool mouseLeftPressed{false};
        bool pointEditable{false};
        bool supportContextMenu{false};
        bool firstCalled{true};

        auto SetInstallFilter() -> void;
        auto InitView() -> void;
        auto InitScene() -> void;
        auto InitLensItem() -> void;
        auto InitTileImagingItem() -> void;
        auto InitPreviewItem() -> void;
        auto InitSelectedLocInfoItem() -> void;
        auto InitZoomButtonGroup() -> void;
        auto Connect() -> void;
        auto SetBlockSignals(bool blocked) -> void;

        auto ClearScene() -> void;
        auto AddWellCanvasItems() -> void;
        auto HideMarkAndAcqItems() -> void;
    };

    WellCanvas::WellCanvas(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>(this)} {
        d->SetInstallFilter();
        d->InitView();
        d->InitScene();
        d->InitLensItem();
        d->InitTileImagingItem();
        d->InitPreviewItem();
        d->InitSelectedLocInfoItem();
        d->InitZoomButtonGroup();
        d->Connect();
    }

    WellCanvas::~WellCanvas() {
    }

    auto WellCanvas::SetViewMode(ViewMode mode) -> void {
        switch (mode) {
        case ViewMode::PerformMode:
            d->pointEditable = true;
            d->supportContextMenu = true;
            d->control.SetCanvasMode(EditMode::Editable);
            d->zoomButtonGroup->show();
            break;
        case ViewMode::TimelapseMode:
            d->control.SetCanvasMode(EditMode::ReadOnly);
            d->zoomButtonGroup->show();
            break;
        case ViewMode::InfoMode:
        case ViewMode::CanvasMode:
        case ViewMode::CopyDlgMode:
            d->zoomButtonGroup->hide();
            d->control.SetCanvasMode(EditMode::ReadOnly);
            break;
        default:
            d->zoomButtonGroup->show();
            break;
        }
        
        setMouseTracking(d->control.GetCanvasTracking());
        setDragMode(d->control.GetCanvasDragMode());
    }

    auto WellCanvas::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->control.SetVesselMapDataRepo(vesselMapDataRepo);
        d->creator.SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto WellCanvas::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->control.SetLocationDataRepo(locationDataRepo);
        d->creator.SetLocationDataRepo(locationDataRepo);
    }

    auto WellCanvas::ClearAll() -> void {
        ClearScene();
        d->control.ClearData();
    }

    auto WellCanvas::ClearScene() -> void {
        d->SetBlockSignals(true);
        d->ClearScene();
        d->SetBlockSignals(false);
    }

    void WellCanvas::ShowLensItem(bool show) {
        d->lensItem->SetVisible(show);
    }

    auto WellCanvas::SetLensPosition(double x, double y) -> void {
        d->control.SetLensPosition(x, y);
        if (d->lensItem != nullptr) {
            d->lensItem->SetPosition(d->control.GetLensPosX(), d->control.GetLensPosY());
        }
    }

    auto WellCanvas::GetLensPosition() const -> Position2D {
        return {d->control.GetLensPosX(), d->control.GetLensPosY()};
    }

    auto WellCanvas::SetSceneRectAndItemSize(WellIndex wellIndex) -> void {
        if (wellIndex != kInvalid) {
            d->scene->setSceneRect(d->control.GetSceneRect(wellIndex));
            d->eventFilter->ZoomFit();
        }
    }

    auto WellCanvas::GetSceneRect() const -> QRectF {
        return d->scene->sceneRect();
    }

    auto WellCanvas::SetCurrentWell(WellIndex wellIndex) -> void {
        ClearScene();
        d->control.SetCurrentWellIndex(wellIndex);
        d->AddWellCanvasItems();
        d->scene->update();
    }

    auto WellCanvas::AddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        if (d->control.GetCurrentWellIndex() != wellIndex) return;

        const auto item = d->creator.CreateMarkItem(wellIndex, markIndex);
        if (item != nullptr) {
            d->scene->addItem(item);
            d->scene->update();
            connect(dynamic_cast<WellCanvasMarkItem*>(item), &WellCanvasMarkItem::sigMarkItemDoubleClicked, this, &Self::sigMarkItemDblClicked);
        }
    }

    auto WellCanvas::SetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        const auto loc = d->control.GetMarkLocation(wellIndex, markIndex);
        bool found = false;
        if (loc == nullptr) return;

        for (auto& item : d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::MarkPointItem)) {
                const auto target = dynamic_cast<WellCanvasMarkItem*>(item);
                if (target->GetWellIndex() == wellIndex && target->GetMarkIndex() == markIndex) {
                    if (loc->GetType()._to_integral() == MarkType::Point) {
                        found = true;
                        target->SetPosition(loc->GetPosX(), loc->GetPosY(), loc->GetPosZ());
                    }
                    else {
                        d->scene->removeItem(target);
                    }
                    break;
                }
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::MarkTileItem)) {
                const auto target = dynamic_cast<WellCanvasMarkTileItem*>(item);
                if (target->GetWellIndex() == wellIndex && target->GetMarkIndex() == markIndex) {
                    if (loc->GetType()._to_integral() == MarkType::Tile) {
                        found = true;
                        target->SetPosition(loc->GetPosX(), loc->GetPosY(), loc->GetPosZ());
                        target->SetTileSize(loc->GetWidth(), loc->GetHeight());

                    }
                    else {
                        d->scene->removeItem(target);
                    }
                    break;
                }
            }
        }
        if (!found) {
            AddMarkLocation(wellIndex, markIndex);
        }
    }

    auto WellCanvas::DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        for (auto item : d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::MarkPointItem) || item->type() == static_cast<int32_t>(GraphicsItemType::MarkTileItem)) {
                const auto target = dynamic_cast<WellCanvasMarkItem*>(item);
                if (target->GetMarkIndex() == markIndex && target->GetWellIndex() == wellIndex) {
                    d->scene->removeItem(target);
                    break;
                }
            }
        }
    }

    auto WellCanvas::AddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        if (d->control.GetCurrentWellIndex() != wellIndex) return;

        const auto item = d->creator.CreateAcquisitionItem(wellIndex, acquisitionIndex, d->supportContextMenu);
        if (item != nullptr) {
            d->scene->addItem(item);
            d->scene->update();

            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto tileItem = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                connect(tileItem, &WellCanvasAcquisitionTileItem::sigAcquisitionItemDoubleClicked, this, &Self::sigAcquisitionItemDblClicked);
                connect(tileItem, &WellCanvasAcquisitionTileItem::sigRequestImportAcquisitionToMark, this, &Self::sigImportAcquisitionToMark);
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                const auto pointItem = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                connect(pointItem, &WellCanvasAcquisitionItem::sigAcquisitionItemDoubleClicked, this, &Self::sigAcquisitionItemDblClicked);
                connect(pointItem, &WellCanvasAcquisitionItem::sigRequestImportAcquisitionToMark, this, &Self::sigImportAcquisitionToMark);
            }
        }
    }

    auto WellCanvas::SetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        const auto loc = d->control.GetAcquisitionLocation(wellIndex, acquisitionIndex);
        bool found = false;
        if (loc == nullptr) return;

        for (auto& item : d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                const auto target = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                if (target->GetWellIndex() == wellIndex && target->GetAcquisitionIndex() == acquisitionIndex) {
                    if (loc->GetType()._to_integral() == AcquisitionType::Point) {
                        found = true;
                        target->SetPosition3D(loc->GetPosX(), loc->GetPosY(), loc->GetPosZ());
                    }
                    else {
                        d->scene->removeItem(target);
                    }
                    break;
                }
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto target = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                if (target->GetWellIndex() == wellIndex && target->GetAcquisitionIndex() == acquisitionIndex) {
                    if (loc->GetType()._to_integral() == AcquisitionType::Tile) {
                        found = true;
                        target->SetPosition3D(loc->GetPosX(), loc->GetPosY(), loc->GetPosZ());
                        target->SetTileSize(loc->GetWidth(), loc->GetHeight());
                    }
                    else {
                        d->scene->removeItem(target);
                    }
                    break;
                }
            }
        }
        if (!found) {
            AddAcquisitionLocation(wellIndex, acquisitionIndex);
        }
    }

    auto WellCanvas::DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        for (auto item : d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                auto target = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                if (target->GetAcquisitionIndex() == acquisitionIndex && target->GetWellIndex() == wellIndex) {
                    d->scene->removeItem(target);
                    d->scene->update();
                    break;
                }
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                auto target = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                if (target->GetAcquisitionIndex() == acquisitionIndex && target->GetWellIndex() == wellIndex) {
                    d->scene->removeItem(target);
                    d->scene->update();
                    break;
                }
            }
        }
    }

    auto WellCanvas::SetSelectedAcqItems(const QList<QPair<WellIndex, AcquisitionIndex>>& selected) -> void {
        this->blockSignals(true);
        d->SetBlockSignals(true);
        d->scene->clearSelection();
        foreach(auto& item, d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                const auto acqItem = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                if (selected.contains({d->control.GetCurrentWellIndex(), acqItem->GetAcquisitionIndex()})) {
                    acqItem->setSelected(true);
                }
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto acqItem = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                if (selected.contains({d->control.GetCurrentWellIndex(), acqItem->GetAcquisitionIndex()})) {
                    acqItem->setSelected(true);
                }
            }
        }
        d->SetBlockSignals(false);
        this->blockSignals(false);
    }

    auto WellCanvas::SetShowAcqItem(AcquisitionIndex index)->void {
        foreach(auto & item, d->scene->items()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem) || 
                item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto acqItem = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                if(acqItem->GetAcquisitionIndex() == index) {
                    acqItem->show();
                }
                else {
                    acqItem->hide();
                }
            }
        }
    }

    auto WellCanvas::ShowPreviewArea(bool show) -> void {
        if(d->previewItem) {
            d->previewItem->setVisible(show);
            d->previewItem->update();
        }
    }

    auto WellCanvas::IsPreviewAreaVisible() -> bool {
        if(d->previewItem) {
            return d->previewItem->isVisible();
        }
        return false;
    }

    auto WellCanvas::SetPreviewLocationEditable(bool editable) -> void {
        if(d->previewItem) {
            d->previewItem->SetEditable(editable);
        }
    }

    auto WellCanvas::SetPreviewLocationSelected(bool selected) -> void {
        if(d->previewItem) {
            d->previewItem->setSelected(selected);
        }
    }

    auto WellCanvas::SetPreviewLocation(double x, double y, double w, double h) -> void {
        if(d->previewItem) {
            d->previewItem->SetGeometry(x,y,w,h);
        }
    }

    auto WellCanvas::GetPreviewLocation() const -> Geometry2D {
        if(d->previewItem) {
            return d->previewItem->GetGeometry();
        }

        return {0,0,0,0};
    }

    auto WellCanvas::ShowTileImagingArea(bool show) -> void {
        if (d->tileImagingItem != nullptr) {
            d->tileImagingItem->setVisible(show);
            d->tileImagingItem->update();
        }
    }

    auto WellCanvas::SetTileImagingArea(double x, double y, double z, double w, double h) -> void {
        if (d->tileImagingItem != nullptr) {
            d->tileImagingItem->SetGeometry3D({x, y, z, w, h});
        }
    }

    auto WellCanvas::SetTileImagingAreaCenter(double xInMm, double yInMm, double zInMm) -> void {
        if(d->tileImagingItem == nullptr) return;
        const auto geo = d->tileImagingItem->GetGeometry3D();
        d->tileImagingItem->SetGeometry3D({xInMm, yInMm, zInMm, geo.w, geo.h});
    }

    auto WellCanvas::ShowGridLine(bool show) -> void {
        for(const auto& item : d->scene->items()) {
            if(item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = dynamic_cast<WellCanvasWellItem*>(item);
                wellItem->ShowGridLine(show);
                wellItem->update();
                break;
            }
        }
    }

    auto WellCanvas::SetCurrentAcquisitionLocation(LocationType type, double xInMM, double yInMM) -> void {
        d->HideMarkAndAcqItems();
        if (d->selectedLocationInfoItem) {
            d->selectedLocationInfoItem->SetLocatinoType(type);
            d->selectedLocationInfoItem->SetPosition(xInMM, yInMM);
            if(!d->selectedLocationInfoItem->isVisible()) {
                d->selectedLocationInfoItem->setVisible(true);
            }
        }
    }

    auto WellCanvas::FitInView()->void {
        d->eventFilter->ZoomFit();
    }

    auto WellCanvas::SetMatrixItemVisible(bool visible) -> void {
        for(const auto& item : d->scene->items()) {
            if(item->type() == static_cast<int32_t>(GraphicsItemType::MatrixItem)) {
                item->setVisible(visible);
            }
        }
    }

    auto WellCanvas::CreateMatrixItems(const QList<QPair<double, double>>& positions) -> void {
        for(const auto& item : d->scene->items()) {
            if(item->type() == static_cast<int32_t>(GraphicsItemType::MatrixItem)) {
                d->scene->removeItem(item);
            }
        }

        const auto acqIndices = d->control.GetAcquisitionLocationIndices(d->control.GetCurrentWellIndex());
        if(acqIndices.size() != 1) {
            return;
        }

        const auto acqIndex = acqIndices.first();
        const auto location = d->control.GetAcquisitionLocation(d->control.GetCurrentWellIndex(), acqIndex);
        const auto type = location->GetType();
        const auto width = location->GetWidth();
        const auto height = location->GetHeight();

        for(const auto& pos : positions) {
            const auto x = pos.first;
            const auto y = pos.second;
            d->scene->addItem(d->creator.CreateMatrixItem(type, x, y, width, height));
        }
    }

    void WellCanvas::keyPressEvent(QKeyEvent* event) {
        if (event->key() == Qt::Key_Delete && d->pointEditable) {
            // 삭제 요청만 하고 실제 지우지 않음.
            for (const auto graphicsItem : d->scene->selectedItems()) {
                const auto type = static_cast<GraphicsItemType>(graphicsItem->type());
                switch (type) {
                    case GraphicsItemType::AcqTileItem: {
                        const auto item = dynamic_cast<WellCanvasAcquisitionTileItem*>(graphicsItem);
                        const auto wIdx = item->GetWellIndex();
                        const auto lIdx = item->GetAcquisitionIndex();
                        emit sigDeleteAcqItem(wIdx, lIdx);
                        break;
                    }
                    case GraphicsItemType::AcqPointItem: {
                        const auto item = dynamic_cast<WellCanvasAcquisitionItem*>(graphicsItem);
                        const auto wIdx = item->GetWellIndex();
                        const auto lIdx = item->GetAcquisitionIndex();
                        emit sigDeleteAcqItem(wIdx, lIdx);
                        break;
                    }
                    case GraphicsItemType::MarkPointItem: {
                        const auto item = dynamic_cast<WellCanvasMarkItem*>(graphicsItem);
                        const auto wIdx = item->GetWellIndex();
                        const auto lIdx = item->GetMarkIndex();
                        emit sigDeleteMarkItem(wIdx, lIdx);
                        break;
                    }
                    case GraphicsItemType::MarkTileItem: {
                        const auto item = dynamic_cast<WellCanvasMarkTileItem*>(graphicsItem);
                        const auto wIdx = item->GetWellIndex();
                        const auto lIdx = item->GetMarkIndex();
                        emit sigDeleteMarkItem(wIdx, lIdx);
                        break;
                    }
                    default: break;
                }
            }
            return;
        }

        if (event->key() == Qt::Key_Shift) {
            for (const auto& item : d->scene->items()) {
                if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                    const auto wellItem = dynamic_cast<WellCanvasWellItem*>(item);
                    wellItem->ShowRoiIndex(true);
                    break;
                }
            }
        }

        QGraphicsView::keyPressEvent(event);
    }

    void WellCanvas::keyReleaseEvent(QKeyEvent* event) {
        if (event->key() == Qt::Key_Shift) {
            for (const auto& item : d->scene->items()) {
                if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                    const auto wellItem = dynamic_cast<WellCanvasWellItem*>(item);
                    wellItem->ShowRoiIndex(false);
                    break;
                }
            }
        }
        QGraphicsView::keyReleaseEvent(event);
    }

    auto WellCanvas::mousePressEvent(QMouseEvent* event) -> void {
        if(event->button() == Qt::LeftButton) {
            d->mouseLeftPressed = true;
        }
        QGraphicsView::mousePressEvent(event);
    }

    auto WellCanvas::mouseReleaseEvent(QMouseEvent* event) -> void {
        if (d->mouseLeftPressed) {
            if (d->tileImagingItem->isSelected() && d->tileImagingItem) {
                const auto& geo = d->tileImagingItem->GetGeometry3D();
                emit sigTileAreaGeometryChanged(geo.x, geo.y, geo.z, geo.w, geo.h);
            }

            if (d->previewItem->isSelected() && d->previewItem) {
                // preview item
                const auto& geo = d->previewItem->GetGeometry();
                emit sigPreviewItemGeometryChanged(geo.x, geo.y, geo.w, geo.h);
            }
        }
        d->mouseLeftPressed = false;
        QGraphicsView::mouseReleaseEvent(event);
    }

    auto WellCanvas::mouseMoveEvent(QMouseEvent* event) -> void {
        QGraphicsView::mouseMoveEvent(event);
    }

    auto WellCanvas::resizeEvent(QResizeEvent* event) -> void{
        auto h_padding = 10;
        auto w_padding = 10;

        if(this->horizontalScrollBar() && this->verticalScrollBar()) {
            h_padding = this->horizontalScrollBar()->height();
            w_padding = this->verticalScrollBar()->width();
        }

        const auto viewGeo = this->geometry();
        d->zoomButtonGroup->setGeometry({
            viewGeo.width() - d->zoomButtonGroup->geometry().width() - w_padding, // left
            viewGeo.height() - d->zoomButtonGroup->geometry().height() - h_padding, // top
            d->zoomButtonGroup->geometry().width(), // width
            d->zoomButtonGroup->geometry().height() // height
        });

        if(d->firstCalled) {
            if (nullptr != this->scene() && nullptr != d->eventFilter) {
                d->eventFilter->ZoomFit();
                d->firstCalled = false;
            }
        }

        QGraphicsView::resizeEvent(event);
    }

    void WellCanvas::onSceneSelectionChanged() {
        QList<QPair<WellIndex, AcquisitionIndex>> selected;
        auto wellIndex = d->control.GetCurrentWellIndex();
        for (const auto& item : d->scene->selectedItems()) {
            if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                const auto acqItem = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                selected.push_back({wellIndex, acqItem->GetAcquisitionIndex()});
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto acqItem = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                selected.push_back({wellIndex, acqItem->GetAcquisitionIndex()});
            }
        }
        emit sigCurrentSelectedAcqItems(selected);
    }

    auto WellCanvas::Impl::SetInstallFilter() -> void {
        eventFilter = new WellCanvasEventFilter(self);
    }

    auto WellCanvas::Impl::InitView() -> void {
        self->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        self->setOptimizationFlags(DontSavePainterState);
        self->setViewportUpdateMode(SmartViewportUpdate);
        self->setTransformationAnchor(AnchorUnderMouse);
        self->setMouseTracking(control.GetCanvasTracking());
        self->setDragMode(control.GetCanvasDragMode());
    }

    auto WellCanvas::Impl::InitScene() -> void {
        scene = new QGraphicsScene(self);
        scene->setSceneRect(-64, -64, 128, 128);
        scene->setBackgroundBrush(SceneBackgroundColor);
        self->setScene(scene);
    }

    auto WellCanvas::Impl::InitLensItem() -> void {
        lensItem = creator.CreateLensItem();
        if (scene != nullptr && lensItem != nullptr) scene->addItem(lensItem);
    }

    auto WellCanvas::Impl::InitTileImagingItem() -> void {
        tileImagingItem = creator.CreateTileImagingItem();
        if (scene != nullptr && tileImagingItem != nullptr) {
            scene->addItem(tileImagingItem);
        }
    }

    auto WellCanvas::Impl::InitPreviewItem() -> void {
        previewItem = creator.CreatePreviewItem();
        if(scene!=nullptr && previewItem!=nullptr) {
            scene->addItem(previewItem);
        }
    }

    auto WellCanvas::Impl::InitSelectedLocInfoItem() -> void {
        selectedLocationInfoItem = creator.CreateSelectedLocInfoItem();
        if(scene!=nullptr && selectedLocationInfoItem!=nullptr) {
            selectedLocationInfoItem->setVisible(false);
            scene->addItem(selectedLocationInfoItem);
        }
    }

    auto WellCanvas::Impl::InitZoomButtonGroup() -> void {
        if(eventFilter==nullptr) SetInstallFilter();

        zoomButtonGroup = new WellCanvasZoomButtonGroup(self);
        connect(zoomButtonGroup, &WellCanvasZoomButtonGroup::sigZoomIn, self, [this] {
           eventFilter->ZoomIn(true);
        });
        connect(zoomButtonGroup, &WellCanvasZoomButtonGroup::sigZoomOut, self, [this] {
            eventFilter->ZoomOut(true);
        });
    }

    auto WellCanvas::Impl::Connect() -> void {
        connect(scene, &QGraphicsScene::selectionChanged, self, &Self::onSceneSelectionChanged);
    }

    auto WellCanvas::Impl::SetBlockSignals(bool blocked) -> void {
        scene->blockSignals(blocked);
    }

    auto WellCanvas::Impl::ClearScene() -> void {
        if(selectedLocationInfoItem) {
            selectedLocationInfoItem->hide();
        }
        for (const auto& a : scene->items()) {
            if (a->type() != static_cast<int32_t>(GraphicsItemType::LensItem) && 
                a->type() != static_cast<int32_t>(GraphicsItemType::TileImagingItem) &&
                a->type() != static_cast<int32_t>(GraphicsItemType::PreviewItem) &&
                a->type() != static_cast<int32_t>(GraphicsItemType::CurrLocationItem)) {
                scene->removeItem(a);
            }
        }
        scene->clearFocus();
        scene->clearSelection();
    }

    auto WellCanvas::Impl::AddWellCanvasItems() -> void {
        for (const auto item : creator.CreateWellCanvasItems(control.GetCurrentWellIndex(), supportContextMenu)) {
            scene->addItem(item);
            if (item->type() == static_cast<int32_t>(GraphicsItemType::WellItem)) {
                const auto wellItem = dynamic_cast<WellCanvasWellItem*>(item);
                connect(wellItem, &WellCanvasWellItem::sigWellItemDoubleClicked, self, &Self::sigWellCanvasDblClicked);
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::MarkPointItem) || item->type() == static_cast<int32_t>(GraphicsItemType::MarkTileItem)) {
                const auto mark = dynamic_cast<WellCanvasMarkItem*>(item);
                connect(mark, &WellCanvasMarkItem::sigMarkItemDoubleClicked, self, &Self::sigMarkItemDblClicked);
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqTileItem)) {
                const auto tileItem = dynamic_cast<WellCanvasAcquisitionTileItem*>(item);
                connect(tileItem, &WellCanvasAcquisitionTileItem::sigAcquisitionItemDoubleClicked, self, &Self::sigAcquisitionItemDblClicked);
                connect(tileItem, &WellCanvasAcquisitionTileItem::sigRequestImportAcquisitionToMark, self, &Self::sigImportAcquisitionToMark);
            }
            else if (item->type() == static_cast<int32_t>(GraphicsItemType::AcqPointItem)) {
                const auto acqPoint = dynamic_cast<WellCanvasAcquisitionItem*>(item);
                connect(acqPoint, &WellCanvasAcquisitionItem::sigAcquisitionItemDoubleClicked, self, &Self::sigAcquisitionItemDblClicked);
                connect(acqPoint, &WellCanvasAcquisitionItem::sigRequestImportAcquisitionToMark, self, &Self::sigImportAcquisitionToMark);
            }
        }
    }

    auto WellCanvas::Impl::HideMarkAndAcqItems() -> void {
        auto isTarget = [=](const QGraphicsItem* item) -> bool{
            const auto itemType = static_cast<GraphicsItemType>(item->type());
            if(itemType == GraphicsItemType::MarkPointItem) return true;
            if(itemType == GraphicsItemType::MarkTileItem) return true;
            if(itemType == GraphicsItemType::AcqPointItem) return true;
            if(itemType == GraphicsItemType::AcqTileItem) return true;
            return false;
        };

        for(auto& item : scene->items()) {
            if(isTarget(item)) {
                item->hide();
            }
        }
    }
}
