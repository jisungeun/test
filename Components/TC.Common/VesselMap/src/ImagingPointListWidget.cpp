﻿#include "ImagingPointListWidget.h"
#include "ui_ImagingPointListWidget.h"
#include "VesselMapWidgetColorCodes.h"
#include "ImagingPointTable.h"
#include "ButtonHoverEventHooker.h"

namespace TC {
    struct ImagingPointListWidget::Impl {
        Ui::ImagingPointListWidget *ui{nullptr};
    };

    ImagingPointListWidget::ImagingPointListWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::ImagingPointListWidget();
        d->ui->setupUi(this);
        d->ui->baseWidget->setObjectName("panel");
        d->ui->tableTitle->setObjectName("label-h2");

        connect(d->ui->imagingPointTable, &ImagingPointTable::sigShowTileCondition, this, &Self::sigShowTileCondition);
        connect(d->ui->imagingPointTable, &ImagingPointTable::sigCurrentSelectedItems, this, &Self::sigCurrentSelectedItems);
        connect(d->ui->imagingPointTable, &ImagingPointTable::sigAcqItemDoubleClicked, this, &Self::sigAcqItemDoubleClicked);
        connect(d->ui->imagingPointTable, &ImagingPointTable::sigDeleteAcquisitionItem, this, &Self::sigDeleteAcquisitionItem);
        connect(d->ui->imagingPointTable, &ImagingPointTable::sigCopyAcquisitionItems, this, &Self::sigCopyAcquisitionItems);

        connect(this, &Self::sigUpdateTableSectionByWellCanvas, d->ui->imagingPointTable, &ImagingPointTable::onUpdateTableSectionByWellCanvas);
        connect(d->ui->deleteBtn, &QAbstractButton::clicked, d->ui->imagingPointTable, &ImagingPointTable::onAcquisitionDataDeleteRequested);
        connect(d->ui->copyBtn, &QAbstractButton::clicked, d->ui->imagingPointTable, &ImagingPointTable::onAcquisitionDataCopyRequested);

        connect(d->ui->imagingPointTable, &ImagingPointTable::sigImagingPointSelected, this, &Self::sigImagingPointSelected);
        connect(d->ui->chkShowAll, &QCheckBox::toggled, this, &Self::sigShowAllButtonToggled);

        d->ui->chkShowAll->setObjectName("bt-toggle");
        d->ui->line->setObjectName("line-divider");

        d->ui->deleteBtn->setIcon(QIcon(":/img/del-d"));
        d->ui->deleteBtn->installEventFilter(new ButtonHoverEventHooker(":/img/del-d", ":/img/del-s", this));

        d->ui->copyBtn->setVisible(false);
        d->ui->copyBtn->setIcon(QIcon(":/img/copy-d"));
        d->ui->copyBtn->installEventFilter(new ButtonHoverEventHooker(":/img/copy-d", ":/img/copy-s", this));
    }

    ImagingPointListWidget::~ImagingPointListWidget() {
        delete d->ui;
    }

    auto ImagingPointListWidget::ClearAll() -> void {
        d->ui->imagingPointTable->ClearAll();
    }

    auto ImagingPointListWidget::InitColumnHeader() -> void {
        d->ui->imagingPointTable->InitColumnHeader();
    }

    auto ImagingPointListWidget::SetViewMode(ViewMode mode) -> void {
        d->ui->imagingPointTable->SetViewMode(mode);
        switch (mode) {
            case ViewMode::PerformMode: 
                d->ui->copyBtn->setVisible(true);
                break;
            case ViewMode::TimelapseMode: 
                d->ui->chkShowAll->setChecked(true);
                d->ui->chkShowAll->setDisabled(true);
                SetShowAllPoints(true);
                break;
            default: break;
        }
    }

    auto ImagingPointListWidget::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->ui->imagingPointTable->SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto ImagingPointListWidget::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->ui->imagingPointTable->SetLocationDataRepo(locationDataRepo);
    }

    auto ImagingPointListWidget::SetCurrentWellIndex(WellIndex wellIndex) -> void {
        d->ui->imagingPointTable->SetCurrentWellIndex(wellIndex);
    }

    auto ImagingPointListWidget::AddAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui->imagingPointTable->AddAcquisitionData(wellIndex, acquisitionIndex);
    }

    auto ImagingPointListWidget::SetAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui->imagingPointTable->SetAcquisitionData(wellIndex, acquisitionIndex);
    }

    auto ImagingPointListWidget::DeleteAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        d->ui->imagingPointTable->DeleteAcquisitionData(wellIndex, acquisitionIndex);
    }

    auto ImagingPointListWidget::SetDeleteEnabled(bool enabled) -> void {
        d->ui->deleteBtn->setEnabled(enabled);
    }

    auto ImagingPointListWidget::SetShowAllPoints(bool showAll) -> void {
        d->ui->imagingPointTable->SetShowAll(showAll);
    }
}
