﻿#pragma once

#include <memory>

#include "VesselMapExternalData.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    class LocationMark {
    public:
        using Self = LocationMark;
        using Pointer = std::shared_ptr<Self>;

        LocationMark();
        LocationMark(const LocationMark& other);
        ~LocationMark();

        auto operator=(const LocationMark& other) -> LocationMark&;
        //auto operator==(const LocationMark& other) const -> bool;

        auto SetIndex(MarkIndex index) -> void;
        auto GetIndex() const -> MarkIndex;

        auto SetType(MarkType type) -> void;
        auto GetType() const -> MarkType;

        auto SetPosition(double x, double y, double z) -> void;
        auto GetPosition() const -> Position3D;
        auto GetPosX() const -> double;
        auto GetPosY() const -> double;
        auto GetPosZ() const -> double;

        auto SetSize(double w, double h) -> void;
        auto GetSize() const -> Size2D;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
