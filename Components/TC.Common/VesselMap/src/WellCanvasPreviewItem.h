﻿#pragma once

#include <memory>

#include <QGraphicsItem>

#include "ResizableItemHandler.h"
#include "VesselMapExternalData.h"
#include "VesselMapCustomDataTypes.h"

namespace TC {
    class WellCanvasPreviewItem : public ResizableItemHandler, public QGraphicsRectItem {
    public:
        using Self = WellCanvasPreviewItem;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasPreviewItem(bool isVisible = false, QGraphicsItem* parent = nullptr);
        ~WellCanvasPreviewItem() override;

        auto SetGeometry(double x, double y, double w, double h) -> void;
        auto GetGeometry() const -> Geometry2D&;

        auto SetSelectorFrameBounds(const QRectF& boundRect) -> void override;
        auto GetSelectorFrameBounds() const -> QRectF override;

        auto SetEditable(bool editable) -> void;

    protected:
        auto type() const -> int override;
        auto shape() const -> QPainterPath override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

        auto mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
