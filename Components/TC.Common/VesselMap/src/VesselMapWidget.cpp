﻿#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>

#include "VesselMapWidget.h"
#include "ImagingOrderWidget.h"
#include "ImagingPointListWidget.h"
#include "LocationAcquisition.h"
#include "LocationDataRepo.h"
#include "ui_VesselMapWidget.h"
#include "Vessel.h"
#include "VesselMapLayoutManager.h"
#include "VesselMapWidgetControl.h"
#include "VesselView.h"
#include "WellGroupWidget.h"
#include "WellView.h"

namespace TC {
    static auto _id = qRegisterMetaType<TC::VesselAxis>("TC::VesselAxis");

    struct VesselMapWidget::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};
        Ui::VesselMapWidget ui;
        VesselMapWidgetControl control;
        VesselMapLayoutManager layoutManager;

        VesselView* vesselView{nullptr};
        WellGroupWidget* wellGroupWidget{nullptr};
        WellView* wellView{nullptr};
        ImagingPointListWidget* imagingPointWidget{nullptr};
        ImagingOrderWidget* imagingOrderWidget{nullptr};

        auto InitSubWidgets() -> void;
        auto InitLocationDataRepo() -> void;
        auto SetLayout() -> void;
        auto Connect() -> void;

        auto NotifyCurrentVesselChanged() -> void;
        auto NotifySetVesselMapDataRepo() -> void;
        auto NotifyInitialize() -> void;
        auto NotifyLensPositionChanged() -> void;
        auto NotifyCreateNewGroup(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto NotifyGroupDeleted(GroupIndex groupIndex) -> void;

        auto NotifyExistGroupInfoChanged(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void;
        auto NotifyRemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void;
        auto NotifyWellGroupNameChanged(GroupIndex groupIndex, const QString& name) -> void;
        auto NotifyWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void;
        auto NotifyWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void;
        auto NotifyFocusedWellIndexChanged(WellIndex wellIndex) -> void;
        auto NotifyWellNameChanged(WellIndex wellIndex, const QString& name) -> void;

        auto NotifyAddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto NotifySetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;
        auto NotifyDeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void;

        auto NotifyAddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto NotifySetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;
        auto NotifyDeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;

        auto NotifySetPreviewLocation(WellIndex wellIndex, double x, double y, double w, double h) -> void;
        auto NotifySetPreviewLocationEditable(bool editable) -> void;
        auto NotifyShowPreviewLocation() -> void;
        auto NotifyHidePreviewLocation() -> void;

        auto NotifyTileAcqusitionGeometryChanged(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void;

        auto NotifySetImagingPointDeleteEnabled(bool enabled) -> void;
        auto NotifySaveCurrentPreviewStatus() -> void;
        auto NotifyRestoreLastPreviewStatus() -> void;

        auto NotifySetSelectedWellIndices(const QList<WellIndex>& wellIndices) -> void;
    };

    VesselMapWidget::VesselMapWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->InitSubWidgets();
        d->SetLayout();
        d->Connect();
        d->InitLocationDataRepo();

        // Init
        SetViewMode(ViewMode::SetupMode);
        setObjectName("panel");

    }

    VesselMapWidget::~VesselMapWidget() {
        if (d != nullptr) {
            d->vesselView->ClearScene();
            d->wellView->ClearScene();
        }
    }

    auto VesselMapWidget::SetViewMode(ViewMode mode) -> void {
        d->layoutManager.SetLayout(mode);
        d->vesselView->SetViewMode(mode);
        d->wellView->SetViewMode(mode);
        d->imagingPointWidget->SetViewMode(mode);
        d->imagingOrderWidget->SetViewMode(mode);
        d->wellGroupWidget->SetViewMode(mode);
    }

    auto VesselMapWidget::ClearAll() const -> void {
        d->control.ClearAll();
        d->imagingPointWidget->ClearAll();
        d->vesselView->ClearAll();
        d->wellView->ClearAll();
        d->wellGroupWidget->ClearAll();
        d->imagingOrderWidget->ClearAll();
    }

    auto VesselMapWidget::SetVesselMap(VesselMap::Pointer vesselMap) -> void {
        // TODO Init All Data
        d->control.Initialize();
        d->control.SetVesselMap(vesselMap);
        d->NotifySetVesselMapDataRepo();

        if (vesselMap->GetVessels().isEmpty() == false) {
            d->control.SetCurrentVesselIndex(vesselMap->GetVessels().first()->GetIndex());
        }

        d->NotifyInitialize();
    }

    auto VesselMapWidget::SetSafePositionRange(const VesselAxis& axis, const double& minMM, const double& maxMM) -> void {
        d->wellView->SetLensPositionRange(axis, minMM, maxMM);
    }

    auto VesselMapWidget::SetSelectedWell(WellIndex wellIndex) -> void {
        d->vesselView->SetSelectedWell(wellIndex);
    }

    auto VesselMapWidget::SetCurrentAcquisitionPointByWellPos(LocationType locationType, double xMM, double yMM) -> void {
        d->wellView->SetCurrentAcquisitionLocation(locationType, xMM, yMM);
    }

    auto VesselMapWidget::SetPreviewPresetSize(double width, double height) -> void {
        d->control.SetPreviewPresetSize(width, height);
    }

    auto VesselMapWidget::SetAcqTilePresetSize(double width, double height) -> void {
        // TODO Set tile acquisition preset size
        Q_UNUSED(width)
        Q_UNUSED(height)
    }

    auto VesselMapWidget::GetWellIndex(int32_t row, int32_t column) const -> WellIndex {
        return d->control.GetWellIndex(row, column);
    }

    auto VesselMapWidget::GetWellRow(WellIndex wellIndex) const -> int32_t {
        return d->control.GetWellRow(wellIndex);
    }

    auto VesselMapWidget::GetWellColumn(WellIndex wellIndex) const -> int32_t {
        return d->control.GetWellColumn(wellIndex);
    }

    void VesselMapWidget::ShowObjectiveLens(bool show) {
        d->wellView->ShowLensItem(show);
    }

    auto VesselMapWidget::SetObjectiveLensPos(double x, double y, double z) -> void {
        d->control.SetLensPosition(x, y, z);
        d->NotifyLensPositionChanged();
    }

    auto VesselMapWidget::GetObjectiveLensPosX() const -> double {
        return d->control.GetLensPosX();
    }

    auto VesselMapWidget::GetObjectiveLensPosY() const -> double {
        return d->control.GetLensPosY();
    }

    auto VesselMapWidget::GetObjectiveLensPosZ() const -> double {
        return d->control.GetLensPosZ();
    }

    auto VesselMapWidget::SetCurrentVessel(VesselIndex vesselIndex) -> void {
        d->control.SetCurrentVesselIndex(vesselIndex);
        d->NotifyCurrentVesselChanged();
    }

    auto VesselMapWidget::GetCurrentVessel() const -> VesselIndex {
        return d->control.GetCurrentVesselIndex();
    }

    auto VesselMapWidget::GetCurrentSelectedWellIndices() const -> QList<WellIndex>& {
        return d->vesselView->GetSelectedWellIndices();
    }

    auto VesselMapWidget::GetSelectedWellIndicesHasNoGroup() const -> QList<WellIndex> {
        return std::move(d->control.GetWellsNotInGroup(d->vesselView->GetSelectedWellIndices()));
    }

    auto VesselMapWidget::GetSelectedGroupIndices() const -> QList<GroupIndex>& {
        return d->vesselView->GetSelectedGroupIndices();
    }

    auto VesselMapWidget::GetFocusedWellIndex() const -> WellIndex {
        return d->vesselView->GetFocusedWellIndex();
    }

    auto VesselMapWidget::CreateNewGroup(GroupIndex groupIndex, const QString& groupName, const QColor& groupColor, const QList<WellIndex>& wellIndices) -> void {
        const auto newJoiners = d->control.GetWellsNotInGroup(wellIndices);
        if(groupIndex != kInvalid || !newJoiners.empty()) {
            const auto created = d->control.CreateNewGroup(groupIndex,
                                                           groupName,
                                                           groupColor,
                                                           wellIndices);
            if(created) d->NotifyCreateNewGroup(groupIndex, newJoiners);
        }
    }

    auto VesselMapWidget::DeleteGroup(GroupIndex groupIndex) -> void {
        if (true == d->control.DeleteGroup(groupIndex)) {
            d->NotifyGroupDeleted(groupIndex);
        }
    }

    auto VesselMapWidget::DeleteGroups(const QList<GroupIndex>& groupIndices) -> void {
        for (const auto groupIndex : groupIndices) {
            DeleteGroup(groupIndex);
        }
    }

    auto VesselMapWidget::AddWellsToGroup(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void {
        const auto newJoiners = d->control.GetWellsNotInGroup(wellIndices);
        if (true == d->control.AddWellsToGroup(groupIndex, newJoiners)) {
            d->NotifyExistGroupInfoChanged(groupIndex, newJoiners);
        }
    }

    auto VesselMapWidget::RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void {
        const auto deleteTargets = d->control.GetWellsInGroup(wellIndices);
        if (true == d->control.RemoveWellsFromGroup(deleteTargets)) {
            d->NotifyRemoveWellsFromGroup(deleteTargets);
        }
    }

    auto VesselMapWidget::ChangeGroupName(GroupIndex groupIndex, const QString& name) -> void {
        d->control.ChangeGroupName(groupIndex, name);
        d->NotifyWellGroupNameChanged(groupIndex, name);
    }

    auto VesselMapWidget::ChangeGroupColor(GroupIndex groupIndex, const QColor& color) -> void {
        d->control.ChangeGroupColor(groupIndex, color);
        d->NotifyWellGroupColorChanged(groupIndex, color);
    }

    auto VesselMapWidget::MoveGroup(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void {
        d->control.MoveGroup(movingGroupIndex, targetGroupIndex);
        d->NotifyWellGroupMoved(movingGroupIndex, targetGroupIndex);
    }

    auto VesselMapWidget::ChangeWellName(WellIndex wellIndex, const QString& name) -> void {
        d->control.ChangeWellName(wellIndex, name);
        d->NotifyWellNameChanged(wellIndex, name);
    }

    auto VesselMapWidget::AddMarkLocationBySystemPos(WellIndex wellIndex, MarkType type, double x, double y, double z, double w, double h) -> MarkIndex {
        if (wellIndex == kInvalid) return kInvalid;

        auto markIndex = d->control.AddMarkLocationBySystemCoord(wellIndex, type, x, y, z, w, h);
        if (markIndex != kInvalid) {
            d->NotifyAddMarkLocation(wellIndex, markIndex);
        }
        return markIndex;
    }

    auto VesselMapWidget::AddMarkLocationByWellPos(WellIndex wellIndex, MarkType type, double x, double y, double z, double w, double h) -> MarkIndex {
        if (wellIndex == kInvalid) return kInvalid;

        auto markIndex = d->control.AddMarkLocationByWellCoord(wellIndex, type, x, y, z, w, h);
        if (markIndex != kInvalid) {
            d->NotifyAddMarkLocation(wellIndex, markIndex);
        }
        return markIndex;
    }

    auto VesselMapWidget::SetMarkLocationBySystemPos(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w, double h) -> void {
        if (wellIndex != kInvalid && markIndex != kInvalid) {
            d->control.SetMarkLocationBySystemCoord(wellIndex, markIndex, type, x, y, z, w, h);
            d->NotifySetMarkLocation(wellIndex, markIndex);
        }
    }

    auto VesselMapWidget::SetMarkLocationByWellPos(WellIndex wellIndex, MarkIndex markIndex, MarkType type, double x, double y, double z, double w, double h) -> void {
        if (wellIndex != kInvalid && markIndex != kInvalid) {
            d->control.SetMarkLocationByWellCoord(wellIndex, markIndex, type, x, y, z, w, h);
            d->NotifySetMarkLocation(wellIndex, markIndex);
        }
    }

    auto VesselMapWidget::DeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        if (wellIndex == kInvalid) return;

        d->control.DeleteMarkLocation(wellIndex, markIndex);
        d->NotifyDeleteMarkLocation(wellIndex, markIndex);
    }

    auto VesselMapWidget::ShowTileImagingArea(bool show) -> void {
        d->wellView->ShowTileImagingArea(show);
    }

    auto VesselMapWidget::SetTileImagingArea(double x, double y, double z, double w, double h) -> void {
        d->wellView->SetTileImagingArea(x,y,z,w,h);
    }

    auto VesselMapWidget::SetTileImagingAreaCenter(double xInMM, double yInMM, double zInMM) -> void {
        d->wellView->SetTileImagingAreaCenter(xInMM, yInMM, zInMM);
    }

    auto VesselMapWidget::AddAcquisitionLocationBySystemPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w, double h) -> void {
        if(wellIndex != kInvalid && acquisitionIndex != kInvalid) {
            if(d->control.AddAcquisitionLocationByWorldCoord(wellIndex, acquisitionIndex, type, x, y, z, w, h))
                d->NotifyAddAcquisitionLocation(wellIndex, acquisitionIndex);
        }
    }

    auto VesselMapWidget::AddAcquisitionLocationByWellPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w, double h) -> void {
        if(wellIndex != kInvalid && acquisitionIndex != kInvalid) {
            if(d->control.AddAcquisitionLocationByWellCoord(wellIndex, acquisitionIndex, type, x, y, z, w, h))
                d->NotifyAddAcquisitionLocation(wellIndex, acquisitionIndex);
        }
    }

    auto VesselMapWidget::SetAcquisitionLocationBySystemPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w, double h) -> void {
        if (wellIndex != kInvalid && acquisitionIndex != kInvalid) {
            d->control.SetAcquisitionLocationBySystemCoord(wellIndex, acquisitionIndex, type, x, y, z, w, h);
            d->NotifySetAcquisitionLocation(wellIndex, acquisitionIndex);
        }
    }

    auto VesselMapWidget::SetAcquisitionLocationByWellPos(WellIndex wellIndex, AcquisitionIndex acquisitionIndex, AcquisitionType type, double x, double y, double z, double w, double h) -> void {
        if (wellIndex != kInvalid && acquisitionIndex != kInvalid) {
            d->control.SetAcquisitionLocationByWellCoord(wellIndex, acquisitionIndex, type, x, y, z, w, h);
            d->NotifySetAcquisitionLocation(wellIndex, acquisitionIndex);
        }
    }

    auto VesselMapWidget::DeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        if(wellIndex == kInvalid) return;
        d->control.DeleteAcquisitionLocation(wellIndex, acquisitionIndex);
        d->NotifyDeleteAcquisitionLocation(wellIndex, acquisitionIndex);
    }

    auto VesselMapWidget::SetAcquisitionLocationDeleteEnabled(bool enabled) -> void {
        d->NotifySetImagingPointDeleteEnabled(enabled);
    }

    auto VesselMapWidget::SetPreviewLocation() -> void {
        const auto defaultWidth = d->control.GetPreviewPresetWidth(); // mm
        const auto defaultHeight = d->control.GetPreviewPresetHeight(); // mm
        const auto centerX = d->control.GetWellPos2DByGlobalLensPos().x;
        const auto centerY = d->control.GetWellPos2DByGlobalLensPos().y;
        
        d->NotifySetPreviewLocation(d->control.GetCurrentWellIndex(),
                                    centerX, centerY, defaultWidth, defaultHeight);

        emit sigPreviewAreaChanged(centerX, centerY, defaultWidth, defaultHeight);
    }

    auto VesselMapWidget::SetPreviewLocationByWellPos(WellIndex wellIndex, double x, double y, double w, double h) -> void {
        d->NotifySetPreviewLocation(wellIndex, x,y,w,h);
    }

    auto VesselMapWidget::SetPreviewLocationEditable(bool editable) -> void {
        d->NotifySetPreviewLocationEditable(editable);
    }

    auto VesselMapWidget::ShowPreviewLocation() -> void {
        d->NotifyShowPreviewLocation();
    }

    auto VesselMapWidget::HidePreviewLocation() -> void {
        d->NotifyHidePreviewLocation();
    }

    auto VesselMapWidget::GetAllVesselIndices() const -> QList<VesselIndex> {
        return d->control.GetAllVesselIndices();
    }

    auto VesselMapWidget::SetWellCanvasFixedSize(int w, int h)->void {
        d->wellView->setFixedSize(w, h);
    }

    auto VesselMapWidget::FitWellCanvas()->void {
        d->wellView->FitWellCanvas();
    }

    auto VesselMapWidget::StartCustomPreviewAreaSetting() -> void {
        d->NotifySaveCurrentPreviewStatus();
        d->NotifyShowPreviewLocation();
        d->NotifySetPreviewLocationEditable(true);
        SetPreviewLocation();
        d->wellView->SetPreviewItemSelected(true);
    }

    auto VesselMapWidget::StopCustomPreviewAreaSetting() -> void {
        d->NotifyRestoreLastPreviewStatus();
        d->NotifySetPreviewLocationEditable(false);
    }

    auto VesselMapWidget::SetWellImagingOrder(const QList<WellIndex>& wells) -> void {
        auto wellList = wells;
        if(wellList.isEmpty()) {
            wellList = d->control.GenerateSortedWells();
        }
        d->vesselView->SetWellImagingOrder(wellList);
    }

    auto VesselMapWidget::SetSelectedWellIndices(const QList<TC::WellIndex>& wellIndices) -> void {
        d->NotifySetSelectedWellIndices(wellIndices);
    }

    auto VesselMapWidget::SetMatrixItemVisible(bool visible) -> void {
        d->wellView->SetMatrixItemVisible(visible);
    }

    auto VesselMapWidget::SetMatrixItems(const QList<QPair<double, double>>& positions) -> void {
        d->wellView->SetMatrixItems(positions);
    }

    void VesselMapWidget::onRecvFocusedWellIndexChanged(const WellIndex& wellIndex, int32_t row, int32_t column) {
        if(d->control.GetCurrentWellIndex() == wellIndex) {
            return;
        }

        d->NotifyFocusedWellIndexChanged(wellIndex);
        d->NotifyHidePreviewLocation();
        emit sigChangeFocusedWell(wellIndex, row, column);
        d->control.SetCurrentWellIndex(wellIndex);
    }

    void VesselMapWidget::onRecvFocusedWellIndexChanged(const WellIndex& wellIndex) {
        if(d->control.GetCurrentWellIndex() == wellIndex) return;

        d->NotifyFocusedWellIndexChanged(wellIndex);
        d->NotifyHidePreviewLocation();
        d->control.SetCurrentWellIndex(wellIndex);
    }

    void VesselMapWidget::onRecvGroupNameChanged(GroupIndex groupIndex, const QString& groupName) {
        if (d->control.IsExistGroup(groupName)) {
            auto targetGroupIndex = d->control.GetGroupIndex(groupName);
            this->MoveGroup(groupIndex, targetGroupIndex);
            emit sigMovedGroup(groupIndex, targetGroupIndex);
        } else {
            this->ChangeGroupName(groupIndex, groupName);
            emit sigChangedGroupName(groupIndex, groupName);    
        }
    }

    void VesselMapWidget::onRecvGroupColorChanged(GroupIndex groupIndex, const QColor& groupColor) {
        this->ChangeGroupColor(groupIndex, groupColor);
        emit sigChangedGroupColor(groupIndex, groupColor);
    }

    void VesselMapWidget::onRecvWellNameChanged(WellIndex wellIndex, const QString& wellName) {
        this->ChangeWellName(wellIndex, wellName);
        emit sigChangedWellName(wellIndex, wellName);
    }

    void VesselMapWidget::onRecvPreviewGeoChangedOnWellCanvas(const double& centerX, const double& centerY, const double& width, const double& height) {
        emit sigPreviewAreaChanged(centerX, centerY, width, height);
    }

    void VesselMapWidget::onRecvTileImagingGeoChangedOnWellCanvas(const double& centerX, const double& centerY, const double& z, const double& width, const double& height) {
        emit sigTileImagingAreaChanged(centerX, centerY, z, width, height);
    }

    void VesselMapWidget::onRecvDeletedMark(const WellIndex& wellIndex, const MarkIndex& markIndex) {
        DeleteMarkLocation(wellIndex, markIndex);
        emit sigMarkItemDeleted(wellIndex, markIndex);
    }

    void VesselMapWidget::onRecvDeletedAcquisition(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) {
        DeleteAcquisitionLocation(wellIndex, acquisitionIndex);
        emit sigAcquisitionItemDeleted(wellIndex, acquisitionIndex);
    }

    void VesselMapWidget::onRecvMarkItemDblClicked(const WellIndex& wellIndex, const MarkIndex& markIndex, double x, double y) {
        Q_UNUSED(x)
        Q_UNUSED(y)

        const auto pos = d->control.GetMarkLocationPosition(wellIndex, markIndex);
        if(pos) {
            emit sigDoubleClickedLocationPosition(pos->x, pos->y, pos->z);
        }
        emit sigMarkItemDoubleClikced(wellIndex, markIndex);
    }

    void VesselMapWidget::onRecvAcquisitionItemDblClicked(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex, double x, double y, double z) {
        Q_UNUSED(x)
        Q_UNUSED(y)
        Q_UNUSED(z)

        if(sender() == d->imagingPointWidget) {
            emit sigChangeFocusedWell(wellIndex);
        }

        const auto pos = d->control.GetAcquisitionLocationPosition(wellIndex, acquisitionIndex);
        if (pos) {
            emit sigDoubleClickedLocationPosition(pos->x, pos->y, pos->z);
        }
        emit sigAcquisitionItemDoubleClicked(wellIndex, acquisitionIndex);
    }

    void VesselMapWidget::onRecvImagingPointShowAllButtonToggled(bool showAll) {
        d->imagingPointWidget->SetShowAllPoints(showAll);
    }

    void VesselMapWidget::onRecvImagingOrderChanged(const TC::ImagingOrder& order) {
        d->control.SetImagingOrder(order);
        emit sigChangeImagingOrder(order);
    }

    void VesselMapWidget::onRecvImagingOrderShowRequest(bool show) {
        d->vesselView->ShowImagingOrder(show);
    }

    void VesselMapWidget::onImportAcquisitionToMark(const WellIndex& wellIndex, const AcquisitionIndex& acquisitionIndex) {
        const auto targetAcqLoc = d->control.GetLocationDataRepo()->GetAcquisitionLocationByAcquisitionIndex(wellIndex, acquisitionIndex);
        if(targetAcqLoc) {
            auto GetMarkTypeFromAcqType = [](AcquisitionType acqType) -> MarkType {
                switch(acqType) {
                    case AcquisitionType::Point: return MarkType::Point;
                    case AcquisitionType::Tile: return MarkType::Tile;
                    default: return MarkType::Point;
                }
            };

            AddMarkLocationByWellPos(wellIndex, 
                                     GetMarkTypeFromAcqType(targetAcqLoc->GetType()),
                                     targetAcqLoc->GetPosX(),
                                     targetAcqLoc->GetPosY(),
                                     targetAcqLoc->GetPosZ(),
                                     targetAcqLoc->GetWidth(),
                                     targetAcqLoc->GetHeight());

            onRecvDeletedAcquisition(wellIndex, acquisitionIndex);
        }
    }

    void VesselMapWidget::onRecvImagingPointCopyTargets(const WellIndex& sourceWellIndex, const QList<AcquisitionIndex>& copyPointIndices) {
        emit sigCopyImagingPoints(sourceWellIndex, copyPointIndices);
    }

    auto VesselMapWidget::Impl::InitSubWidgets() -> void {
        vesselView = new VesselView(self);
        wellGroupWidget = new WellGroupWidget(self);
        wellView = new WellView(self);
        imagingPointWidget = new ImagingPointListWidget(self);
        imagingOrderWidget = new ImagingOrderWidget(self);

        layoutManager.InsertWidget(VesselMapLayoutManager::SubWidgetType::VesselCanvas, vesselView);
        layoutManager.InsertWidget(VesselMapLayoutManager::SubWidgetType::WellCanvas, wellView);
        layoutManager.InsertWidget(VesselMapLayoutManager::SubWidgetType::ImgPtList, imagingPointWidget);
        layoutManager.InsertWidget(VesselMapLayoutManager::SubWidgetType::WellGroupList, wellGroupWidget);
        layoutManager.InsertWidget(VesselMapLayoutManager::SubWidgetType::ImgOrder, imagingOrderWidget);
    }

    auto VesselMapWidget::Impl::InitLocationDataRepo() -> void {
        imagingPointWidget->SetLocationDataRepo(control.GetLocationDataRepo());
        wellView->SetLocationDataRepo(control.GetLocationDataRepo());
        wellGroupWidget->SetLocationDataRepo(control.GetLocationDataRepo());
    }

    auto VesselMapWidget::Impl::SetLayout() -> void {
        self->setLayout(layoutManager.GetLayout());
    }

    auto VesselMapWidget::Impl::Connect() -> void {
        connect(vesselView, &VesselView::sigSelectedWellIndices, wellGroupWidget, &WellGroupWidget::onChangeSelectedWellIndexOnVesselCanvas);
        connect(vesselView, &VesselView::sigSelectedWellIndices, self, &Self::sigSelectedWellIndices);
        connect(vesselView, &VesselView::sigSelectedGroupIndices, self, &Self::sigSelectedGroupIndices);
        connect(vesselView, 
                &VesselView::sigVesselCanvasDblClicked, 
                self, 
                qOverload<const WellIndex&, int32_t, int32_t>(&Self::onRecvFocusedWellIndexChanged));
        connect(vesselView, 
                &VesselView::sigChangeSelectWell, 
                self, 
                qOverload<const WellIndex&>(&Self::onRecvFocusedWellIndexChanged));

        connect(wellGroupWidget, &WellGroupWidget::sigChangeGroupName, self, &Self::onRecvGroupNameChanged);
        connect(wellGroupWidget, &WellGroupWidget::sigChangeWellName, self, &Self::onRecvWellNameChanged);
        connect(wellGroupWidget, &WellGroupWidget::sigChangeGroupColor, self, &Self::onRecvGroupColorChanged);
        connect(wellGroupWidget, &WellGroupWidget::sigCurrentSelectedWellIndces, vesselView, &VesselView::onChangeSelectedWellIndexOnGroupTable);

        connect(wellView, &WellView::sigDoubleClicked, self, &Self::sigDoubleClickedWellCanvasPosition);
        connect(wellView, &WellView::sigPreviewGeometryChangedOnWellCanvas, self, &Self::onRecvPreviewGeoChangedOnWellCanvas);
        connect(wellView, &WellView::sigTileImagingGeoChangedOnWellCanvas, self, &Self::onRecvTileImagingGeoChangedOnWellCanvas);
        connect(wellView, &WellView::sigDeleteMarkItem, self, &Self::onRecvDeletedMark);
        connect(wellView, &WellView::sigDeleteAcqItem, self, &Self::onRecvDeletedAcquisition);
        connect(wellView, &WellView::sigCurrentSelectedAcqItems, imagingPointWidget, &ImagingPointListWidget::sigUpdateTableSectionByWellCanvas);
        connect(wellView, &WellView::sigAcqItemDblClicked, self, &Self::onRecvAcquisitionItemDblClicked);
        connect(wellView, &WellView::sigMarkItemDblClicked, self, &Self::onRecvMarkItemDblClicked);
        connect(wellView, &WellView::sigChangeLensPosition, self, &Self::sigChangeLensPosition);
        connect(wellView, &WellView::sigImportAcquisitionToMark, self, &Self::onImportAcquisitionToMark);

        connect(imagingPointWidget, &ImagingPointListWidget::sigShowTileCondition, self, &Self::sigRequestTileConditionPopUp);
        connect(imagingPointWidget, &ImagingPointListWidget::sigAcqItemDoubleClicked, self, &Self::onRecvAcquisitionItemDblClicked);
        connect(imagingPointWidget, &ImagingPointListWidget::sigCurrentSelectedItems, wellView, &WellView::onUpdateTableSectionByImgPtTable);
        connect(imagingPointWidget, &ImagingPointListWidget::sigDeleteAcquisitionItem, self, &Self::onRecvDeletedAcquisition);
        connect(imagingPointWidget, &ImagingPointListWidget::sigImagingPointSelected, self, &Self::sigImagingPointSelected);
        connect(imagingPointWidget, &ImagingPointListWidget::sigShowAllButtonToggled, self, &Self::onRecvImagingPointShowAllButtonToggled);
        connect(imagingPointWidget, &ImagingPointListWidget::sigCopyAcquisitionItems, self, &Self::onRecvImagingPointCopyTargets);
        connect(imagingOrderWidget, &ImagingOrderWidget::sigChangeImagingOrder, self, &Self::onRecvImagingOrderChanged);
        connect(imagingOrderWidget, &ImagingOrderWidget::sigShowImaigngOrder, self, &Self::onRecvImagingOrderShowRequest);
    }

    auto VesselMapWidget::Impl::NotifyCurrentVesselChanged() -> void {
        vesselView->UpdateSceneRect(control.GetCurrentVesselIndex());
    }

    auto VesselMapWidget::Impl::NotifySetVesselMapDataRepo() -> void {
        const auto dataRepo = control.GetVesselMapDataRepo();
        vesselView->SetVesselMapDataRepo(dataRepo);
        wellView->SetVesselMapDataRepo(dataRepo);
        wellGroupWidget->SetVesselMapDataRepo(dataRepo);
        imagingPointWidget->SetVesselMapDataRepo(dataRepo);
    }

    auto VesselMapWidget::Impl::NotifyInitialize() -> void {
        // TODO Clear All Data/Widget (Initialize)
        vesselView->ClearScene();
        vesselView->UpdateSceneRect(control.GetCurrentVesselIndex());
        vesselView->DrawVesselItem();
        vesselView->DrawWellItem();
        vesselView->DrawRowColumnTextItem(control.GetCurrentVesselIndex());

        wellView->ClearScene();

        wellGroupWidget->ClearTable();

        imagingPointWidget->ClearAll();
        imagingPointWidget->InitColumnHeader();

        imagingOrderWidget->ClearAll();
    }

    auto VesselMapWidget::Impl::NotifyLensPositionChanged() -> void {
        const auto x = control.GetLensPosX();
        const auto y = control.GetLensPosY();
        const auto z = control.GetLensPosZ();
        vesselView->SetLensPosition(x, y);
        wellView->SetLensPosition(x, y, z);
    }

    auto VesselMapWidget::Impl::NotifyCreateNewGroup(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void {
        vesselView->UpdateWellGroupCreated(groupIndex, wellIndices);
        wellGroupWidget->AddWellsToTable(groupIndex, wellIndices);
    }

    auto VesselMapWidget::Impl::NotifyGroupDeleted(GroupIndex groupIndex) -> void {
        vesselView->UpdateWellGroupDeleted(groupIndex);
        wellGroupWidget->DeleteGroup(groupIndex);
    }

    auto VesselMapWidget::Impl::NotifyExistGroupInfoChanged(GroupIndex groupIndex, const QList<WellIndex>& wellIndices) -> void {
        vesselView->UpdateWellGroupMemberAdded(groupIndex, wellIndices);
        wellGroupWidget->AddWellsToTable(groupIndex, wellIndices);
    }

    auto VesselMapWidget::Impl::NotifyRemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void {
        vesselView->UpdateWellGroupMemberRemoved(wellIndices);
        wellGroupWidget->RemoveWellsFromGroup(wellIndices);
    }

    auto VesselMapWidget::Impl::NotifyWellGroupNameChanged(GroupIndex groupIndex, const QString& name) -> void {
        wellGroupWidget->ChangeGroupName(groupIndex, name);
    }

    auto VesselMapWidget::Impl::NotifyWellGroupColorChanged(GroupIndex groupIndex, const QColor& color) -> void {
        vesselView->UpdateWellGroupColorChanged(groupIndex, color);
        wellGroupWidget->ChangeGroupColor(groupIndex, color);
    }

    auto VesselMapWidget::Impl::NotifyWellGroupMoved(GroupIndex movingGroupIndex, GroupIndex targetGroupIndex) -> void {
        vesselView->UpdateWellGroupMoved(movingGroupIndex, targetGroupIndex);
        wellGroupWidget->MoveGroup(movingGroupIndex, targetGroupIndex);
    }

    auto VesselMapWidget::Impl::NotifyFocusedWellIndexChanged(WellIndex wellIndex) -> void {
        wellView->SetSceneRect(wellIndex);
        wellView->SetCurrentFocusWell(wellIndex);
        imagingPointWidget->SetCurrentWellIndex(wellIndex);
    }

    auto VesselMapWidget::Impl::NotifyWellNameChanged(WellIndex wellIndex, const QString& name) -> void {
        vesselView->UpdateWellNameChanged(wellIndex, name);
        wellGroupWidget->ChangeWellName(wellIndex, name);
    }

    auto VesselMapWidget::Impl::NotifyAddMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        wellView->AddMarkLocation(wellIndex, markIndex);
    }

    auto VesselMapWidget::Impl::NotifySetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        wellView->SetMarkLocation(wellIndex, markIndex);
    }

    auto VesselMapWidget::Impl::NotifyDeleteMarkLocation(WellIndex wellIndex, MarkIndex markIndex) -> void {
        wellView->DeleteMarkLocation(wellIndex, markIndex);
    }

    auto VesselMapWidget::Impl::NotifyAddAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        wellView->AddAcquisitionLocation(wellIndex, acquisitionIndex);
        imagingPointWidget->AddAcquisitionData(wellIndex, acquisitionIndex);
        wellGroupWidget->UpdateAcquisitionCount();
    }

    auto VesselMapWidget::Impl::NotifySetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        wellView->SetAcquisitionLocation(wellIndex, acquisitionIndex);
        imagingPointWidget->SetAcquisitionData(wellIndex, acquisitionIndex);
        wellGroupWidget->UpdateAcquisitionCount();
    }

    auto VesselMapWidget::Impl::NotifyDeleteAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        wellView->DeleteAcquisitionLocation(wellIndex, acquisitionIndex);
        imagingPointWidget->DeleteAcquisitionData(wellIndex, acquisitionIndex);
        wellGroupWidget->UpdateAcquisitionCount();
    }

    auto VesselMapWidget::Impl::NotifySetPreviewLocation(WellIndex wellIndex, double x, double y, double w, double h) -> void {
        Q_UNUSED(wellIndex)
        wellView->SetPreviewLocation(x,y,w,h);
    }

    auto VesselMapWidget::Impl::NotifySetPreviewLocationEditable(bool editable) -> void {
        wellView->SetPreviewLocationEditable(editable);
    }

    auto VesselMapWidget::Impl::NotifyShowPreviewLocation() -> void {
        wellView->SetPreviewLocationVisible(true);
    }

    auto VesselMapWidget::Impl::NotifyHidePreviewLocation() -> void {
        wellView->SetPreviewLocationVisible(false);
    }

    auto VesselMapWidget::Impl::NotifyTileAcqusitionGeometryChanged(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        imagingPointWidget->SetAcquisitionData(wellIndex, acquisitionIndex);
        wellGroupWidget->UpdateAcquisitionCount();
    }

    auto VesselMapWidget::Impl::NotifySetImagingPointDeleteEnabled(bool enabled) -> void {
        imagingPointWidget->SetDeleteEnabled(enabled);
    }

    auto VesselMapWidget::Impl::NotifySaveCurrentPreviewStatus() -> void {
        wellView->SavePreview();
    }

    auto VesselMapWidget::Impl::NotifyRestoreLastPreviewStatus() -> void {
        wellView->RestorePreview();
    }

    auto VesselMapWidget::Impl::NotifySetSelectedWellIndices(const QList<WellIndex>& wellIndices) -> void {
        vesselView->onChangeSelectedWellIndexByButton(wellIndices);
    }
}
