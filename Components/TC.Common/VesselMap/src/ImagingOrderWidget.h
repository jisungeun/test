﻿#pragma once

#include <memory>

#include <QWidget>

#include "VesselMapExternalData.h"

namespace TC {
    class ImagingOrderWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ImagingOrderWidget;
        using Pointer = std::unique_ptr<Self>;

        explicit ImagingOrderWidget(QWidget* parent = nullptr);
        ~ImagingOrderWidget() override;

        auto SetViewMode(const ViewMode& mode) -> void;
        auto ClearAll() -> void;

    signals:
        void sigShowImaigngOrder(bool);
        void sigChangeImagingOrder(const TC::ImagingOrder);

    private slots:
        void onImagingOrderChanged(int comboBoxIndex);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
