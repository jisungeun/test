﻿#include <QMenu>
#include <QAction>
#include <QGraphicsView>

#include "WellCanvasMenuSupport.h"

namespace TC {
    struct WellCanvasMenuSupport::Impl {
        QMenu* mainMenu{nullptr};
    };

    WellCanvasMenuSupport::WellCanvasMenuSupport(QGraphicsView* view, QObject* parent)
    : QObject(parent), d{std::make_unique<Impl>()} {
        d->mainMenu = new QMenu(view);
    }

    WellCanvasMenuSupport::~WellCanvasMenuSupport() {
    }

    auto WellCanvasMenuSupport::AddAction(const WellCanvasMenu& actionIndex, const QString& title) -> void {
        const auto action = new QAction(title);
        action->setData(actionIndex._to_integral());
        d->mainMenu->addAction(action);
    }

    auto WellCanvasMenuSupport::AddSeperator() -> void {
        d->mainMenu->addSeparator();
    }

    auto WellCanvasMenuSupport::ExecuteMenu(const QPoint& pos) -> WellCanvasMenu {
        const auto action = d->mainMenu->exec(pos);
        bool ok = false;
        if(action) {
            const auto menuIndex = WellCanvasMenu::_from_integral(action->data().toInt(&ok));
            if(ok) {
                return menuIndex;
            }
        }

        return WellCanvasMenu::None;
    }
}
