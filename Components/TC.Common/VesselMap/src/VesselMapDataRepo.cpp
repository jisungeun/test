﻿#include "VesselMapDataRepo.h"

#include <QMap>

namespace TC {
    struct VesselMapDataRepo::Impl {
        Holder::Pointer holder{nullptr};
        QList<Vessel::Pointer> vessels;
        QList<Well::Pointer> wells;
        ImagingArea::Pointer imagingArea{nullptr};
        QList<WellGroup::Pointer> groups;
        Roi::Map rois{};
    };

    VesselMapDataRepo::VesselMapDataRepo() : d{std::make_unique<Impl>()} {
    }

    VesselMapDataRepo::~VesselMapDataRepo() {
    }

    auto VesselMapDataRepo::ClearAll() -> void {
        d->holder = nullptr;
        d->vessels.clear();
        d->wells.clear();
        d->imagingArea = nullptr;
        d->groups.clear();
    }

    auto VesselMapDataRepo::SetHolder(const Holder::Pointer& holder) -> void {
        d->holder = holder;
    }

    auto VesselMapDataRepo::AddVessel(const Vessel::Pointer& vessel) -> void {
        d->vessels.push_back(vessel);
    }

    auto VesselMapDataRepo::AddWell(const Well::Pointer& well) -> void {
        d->wells.push_back(well);
    }

    auto VesselMapDataRepo::SetImagingArea(const ImagingArea::Pointer& imagingArea) -> void {
        d->imagingArea = imagingArea;
    }

    auto VesselMapDataRepo::GetHolder() const -> Holder::Pointer& {
        return d->holder;
    }

    auto VesselMapDataRepo::GetVessels() const -> QList<Vessel::Pointer>& {
        return d->vessels;
    }

    auto VesselMapDataRepo::GetWells() const -> QList<Well::Pointer>& {
        return d->wells;
    }

    auto VesselMapDataRepo::GetImagingArea() const -> ImagingArea::Pointer& {
        return d->imagingArea;
    }

    auto VesselMapDataRepo::GetWellGroups() const -> QList<WellGroup::Pointer>& {
        return d->groups;
    }

    auto VesselMapDataRepo::AddWellGroup(const WellGroup::Pointer& group) -> void {
        Q_ASSERT(group != nullptr);
        if (group != nullptr) {
            d->groups.push_back(group);
        }
    }

    auto VesselMapDataRepo::DeleteWellGroup(const WellGroup::Pointer& group) -> void {
        d->groups.removeOne(group);
    }

    auto VesselMapDataRepo::SetRois(const Roi::Map& rois) -> void {
        d->rois = rois;
    }

    auto VesselMapDataRepo::GetRois(const WellIndex& wellIndex) const -> Roi::List {
        if(d->rois.contains(wellIndex)) {
            return d->rois[wellIndex];
        }
        return {};
    }
}
