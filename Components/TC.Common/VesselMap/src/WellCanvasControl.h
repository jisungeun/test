﻿#pragma once

#include <memory>

#include "VesselMapCustomDataTypes.h"
#include "VesselMapExternalData.h"
#include "LocationMark.h"
#include "LocationAcquisition.h"
#include "LocationDataRepo.h"
#include "VesselMapDataRepo.h"

class QGraphicsView;
class QRectF;

namespace TC {
    class WellCanvasControl {
    public:
        using Self = WellCanvasControl;
        using Pointer = std::shared_ptr<Self>;

        WellCanvasControl();
        ~WellCanvasControl();

        auto SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void;
        auto SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void;

        auto ClearData() -> void;

        auto SetCurrentWellIndex(WellIndex wellIndex) -> void;
        auto GetCurrentWellIndex() const -> WellIndex;

        auto SetCanvasMode(EditMode mode) -> void;
        auto GetCanvasDragMode() const -> QGraphicsView::DragMode;
        auto GetCanvasTracking() const -> bool;
        auto GetSceneRect(WellIndex wellIndex = kInvalid) const -> QRectF;

        auto SetLensPosition(double x, double y) -> void;
        auto GetLensPosX() const -> double;
        auto GetLensPosY() const -> double;

        auto GetMarkLocation(WellIndex wellIndex, MarkIndex markIndex) const -> LocationMark::Pointer;
        auto GetAcquisitionLocation(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) const -> LocationAcquisition::Pointer;

        auto GetImagingAreaCenterX() const -> double;
        auto GetImagingAreaCenterY() const -> double;

        auto IsAvailablePreviewArea(const QRectF& previewAreaRect) const -> bool;

        auto GetAcquisitionLocationIndices(WellIndex wellIndex) const -> QList<AcquisitionIndex>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
