﻿#include "WellGroupTableItemCreator.h"

namespace TC {
    struct WellGroupTableItemCreator::Impl {

    };

    WellGroupTableItemCreator::WellGroupTableItemCreator() : d{std::make_unique<Impl>()} {
    }

    WellGroupTableItemCreator::~WellGroupTableItemCreator() {
    }

    auto WellGroupTableItemCreator::CreateTableWidgetItem(GroupTableHeader header, WellGroupTableData::Pointer tableData) -> QTableWidgetItem* {
        GroupTableWidgetItem* item = nullptr;

        constexpr bool editable = false;
        constexpr bool readonly = true;

        switch (header) {
            case GroupTableHeader::GroupIndex: {
                item = new GroupTableWidgetItem(readonly);
                item->setText(QString::number(tableData->GetGroupIndex()));
                break;
            }
            case GroupTableHeader::WellIndex: {
                item = new GroupTableWidgetItem(readonly);
                item->setText(QString::number(tableData->GetWellIndex()));
                break;
            }
            case GroupTableHeader::GroupName: {
                item = new GroupTableWidgetItem(editable);
                item->setText(tableData->GetGroupName());
                break;
            }
            case GroupTableHeader::ImgPointCount: {
                item = new GroupTableWidgetItem(readonly);
                item->setText(QString::number(tableData->GetWellImgPointCount()));
                break;
            }
            case GroupTableHeader::WellName: {
                item = new GroupTableWidgetItem(editable);
                item->setText(tableData->GetWellName());
                break;
            }
            case GroupTableHeader::WellPosition: {
                item = new GroupTableWidgetItem(readonly);
                item->setText(tableData->GetWellPosition());
                break;
            }
            case GroupTableHeader::GroupColor: {
                item = new GroupTableWidgetItem(readonly);
                item->setBackground(tableData->GetGroupColor());
            }
        }

        return item;
    }
}
