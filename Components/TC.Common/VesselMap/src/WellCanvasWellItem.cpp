﻿#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QStyleOptionGraphicsItem>

#include "WellCanvasWellItem.h"
#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct WellCanvasWellItem::Impl {
        explicit Impl(Self* self) : self(self){}
        Self* self{};
        double lod{};

        WellIndex index{kInvalid}; // need?
        WellShape shape{WellShape::Circle};

        QRectF boundRect{};
        double width{};
        double height{};

        bool showGrid{false};
        QColor gridLineColor{GridLineColor};

        ImagingAreaShape imagingAreaShape{ImagingAreaShape::Circle};
        double imagingAreaCenterX{};
        double imagingAreaCenterY{};
        double imagingAreaWidth{};
        double imagingAreaHeight{};
        QRectF imagingAreaRect{};

        Roi::List rois{};
        bool showRoiIndex{};

        auto DrawWell(QPainter* painter, const QRectF& rect) -> void;
        auto DrawImagingArea(QPainter* painter, const QRectF& rect) -> void;
        auto IsCircle(double w, double h) -> bool;
        auto DrawGridLine(QPainter* painter, const QRectF& rect) -> void;
        auto IsContainsImagingArea(const QPointF& dblClickedPos) const -> bool;
        auto DrawRoi(QPainter* painter) -> void;
        auto DrawRoiIndex(QPainter* painter) -> void;
    };

    WellCanvasWellItem::WellCanvasWellItem(WellIndex index, QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>(this)} {
        d->index = index;
        setCacheMode(DeviceCoordinateCache);
    }

    WellCanvasWellItem::~WellCanvasWellItem() {
    }

    void WellCanvasWellItem::SetWellShape(WellShape shape) {
        d->shape = shape;
    }

    auto WellCanvasWellItem::SetSize(double w, double h) -> void {
        d->width = w;
        d->height = h;
        d->boundRect = DefaultSettingHelper::GetBoundingRect(d->width, d->height);
    }

    auto WellCanvasWellItem::GetSize() const -> Size2D {
        return {d->width, d->height};
    }

    auto WellCanvasWellItem::GetWidth() const -> double {
        return d->width;
    }

    auto WellCanvasWellItem::GetHeight() const -> double {
        return d->height;
    }

    auto WellCanvasWellItem::ShowGridLine(bool show) -> void {
        d->showGrid = show;
    }

    auto WellCanvasWellItem::SetImagingArea(double x, double y, double w, double h) -> void {
        d->imagingAreaCenterX = x;
        d->imagingAreaCenterY = y;
        d->imagingAreaWidth = w;
        d->imagingAreaHeight = h;
        d->imagingAreaRect = DefaultSettingHelper::GetBoundingRect(d->imagingAreaCenterX, -d->imagingAreaCenterY, d->imagingAreaWidth, d->imagingAreaHeight);
    }

    void WellCanvasWellItem::SetImagingAreaShape(ImagingAreaShape shape) {
        d->imagingAreaShape = shape;
    }

    auto WellCanvasWellItem::SetRois(const Roi::List& rois) -> void {
        d->rois = rois;
    }

    auto WellCanvasWellItem::ShowRoiIndex(bool show) -> void {
        d->showRoiIndex = show;
        update();
    }

    auto WellCanvasWellItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::WellItem);
    }

    auto WellCanvasWellItem::shape() const -> QPainterPath {
        QPainterPath path;
        switch (d->shape) {
            case WellShape::Circle: {
                path.addEllipse(boundingRect());
                break;
            }
            case WellShape::Rectangle: {
                path.addRect(boundingRect());
                break;
            }
        }
        return path;
    }

    auto WellCanvasWellItem::boundingRect() const -> QRectF {
        return d->boundRect;
    }

    auto WellCanvasWellItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->setRenderHint(QPainter::Antialiasing);

        d->lod = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());

        d->DrawWell(painter, boundingRect());
        d->DrawImagingArea(painter, d->imagingAreaRect);
        if (d->showGrid) {
            d->DrawGridLine(painter, d->imagingAreaRect);
        }
        if(!d->rois.isEmpty()) {
            d->DrawRoi(painter);
        }
        if(d->showRoiIndex) {
            d->DrawRoiIndex(painter);
        }
    }

    void WellCanvasWellItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) {
        if (event->button() == Qt::LeftButton) {
            if (d->IsContainsImagingArea(event->pos())) {
                emit sigWellItemDoubleClicked(event->pos().x(), -event->pos().y());
            }
        }
        QGraphicsItem::mouseDoubleClickEvent(event);
    }

    auto WellCanvasWellItem::Impl::DrawWell(QPainter* painter, const QRectF& rect) -> void {
        painter->save();
        painter->setPen(Qt::NoPen);
        painter->setBrush(Qt::NoBrush);
        switch (shape) {
            case WellShape::Circle: {
                painter->drawEllipse(rect);
                break;
            }
            case WellShape::Rectangle: {
                painter->drawRect(rect);
                break;
            }
        }
        painter->restore();
    }

    auto WellCanvasWellItem::Impl::DrawImagingArea(QPainter* painter, const QRectF& rect) -> void {
        painter->save();
        painter->setPen(QPen(WellMapBorderColor, 0));
        painter->setBrush(Qt::NoBrush);
        switch (imagingAreaShape) {
            case ImagingAreaShape::Circle: {
                painter->drawEllipse(rect);
                break;
            }
            case ImagingAreaShape::Rectangle: {
                painter->drawRect(rect);
                break;
            }
        }
        painter->restore();
    }

    auto WellCanvasWellItem::Impl::IsCircle(double w, double h) -> bool {
        return fabs(w - h) < std::numeric_limits<double>::epsilon();
    }

    auto WellCanvasWellItem::Impl::DrawGridLine(QPainter* painter, const QRectF& rect) -> void {
        auto drawRectGridLine = [=]() -> void {
            for (auto x = static_cast<int32_t>(rect.left()); x <= static_cast<int32_t>(rect.right()); ++x) {
                painter->setPen(QPen(gridLineColor, 0.005));
                if(x == static_cast<int32_t>(rect.center().x())) {
                    painter->setPen(QPen(GridCenterColor, 0.005));
                }
                painter->drawLine(QPointF(x, rect.top()), QPointF(x, rect.bottom()));
            }
            for (auto y = static_cast<int32_t>(rect.top()); y <= static_cast<int32_t>(rect.bottom()); ++y) {
                painter->setPen(QPen(gridLineColor, 0.005));
                if(y == static_cast<int32_t>(rect.center().y())) {
                    painter->setPen(QPen(GridCenterColor, 0.005));
                }

                painter->drawLine(QPointF(rect.left(), y), QPointF(rect.right(), y));
            }
        };

        painter->save();
        painter->setPen(QPen(gridLineColor, 0.005));

        switch (imagingAreaShape) {
            case ImagingAreaShape::Circle: {
                if (IsCircle(rect.width(), rect.height())) {
                    const double offsetX = rect.center().x();
                    const double offsetY = rect.center().y();

                    const double radius = rect.width() / 2;
                    if (radius < std::numeric_limits<double>::epsilon()) break;

                    const double stepX = 1.0;
                    const double stepY = 1.0;

                    const int stepsInX = radius / stepX;
                    const int stepsInY = radius / stepY;

                    // draw vertical lines
                    for (int i = 0; i <= stepsInX; i++) {
                        if (i == 0) {
                            painter->setPen(QPen(GridCenterColor, 0.005));
                        }
                        else {
                            painter->setPen(QPen(gridLineColor, 0.005));
                        }
                        const double angle = acos(i * stepX / radius);
                        const double y = radius * sin(angle);

                        const double xRight = i * stepX + offsetX; // 중심에서 오른쪽
                        const double xLeft = -(i * stepX - offsetX); // 중심에서 왼쪽

                        const double y1 = y + offsetY;
                        const double y2 = -y + offsetY;

                        painter->drawLine(QPointF(xRight, y1), QPointF(xRight, y2));
                        painter->drawLine(QPointF(xLeft, y1), QPointF(xLeft, y2));
                    }

                    // draw horizontal lines
                    for (int i = 0; i <= stepsInY; i++) {
                        if (i == 0) {
                            painter->setPen(QPen(GridCenterColor, 0.005));
                        }
                        else {
                            painter->setPen(QPen(gridLineColor, 0.005));
                        }

                        const double angle = asin(i * stepY / radius);
                        const double x = radius * cos(angle);

                        const double yAboveCenter = -(i * stepY - offsetY); // 중심에서 위쪽
                        const double yBelowCenter = i * stepY + offsetY; // 중심에서 아래쪽

                        const double x1 = x + offsetX;
                        const double x2 = -x + offsetX;

                        painter->drawLine(QPointF(x1, yAboveCenter), QPointF(x2, yAboveCenter));
                        painter->drawLine(QPointF(x1, yBelowCenter), QPointF(x2, yBelowCenter));
                    }
                }
                else {
                    drawRectGridLine();
                }
                break;
            }

            case ImagingAreaShape::Rectangle: {
                drawRectGridLine();
                break;
            }
            default: break;
        }
        painter->restore();
    }

    auto WellCanvasWellItem::Impl::IsContainsImagingArea(const QPointF& dblClickedPos) const -> bool {
        QPainterPath path;
        const auto targetRect = imagingAreaRect;

        switch (imagingAreaShape) {
            case ImagingAreaShape::Circle: {
                path.addEllipse(targetRect);
                break;
            }
            case ImagingAreaShape::Rectangle: {
                path.addRect(targetRect);
                break;
            }
        }

        return path.contains(dblClickedPos);
    }

    auto WellCanvasWellItem::Impl::DrawRoi(QPainter* painter) -> void {
        painter->save();
        painter->setPen(Qt::NoPen);
        auto brushColor = RoiColor;
        brushColor.setAlpha(50);
        painter->setBrush(brushColor);
        for(const auto& roi : rois) {
            const auto roiShape = roi->GetShape();
            const auto roiName = roi->GetName();
            const auto roiIndex = roi->GetIndex();
            if(roiShape == +RoiShape::Rectangle) {
                painter->drawRect(DefaultSettingHelper::GetBoundingRect(roi->GetX(), -roi->GetY(), roi->GetWidth(), roi->GetHeight()));
            }
            else {
                painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(roi->GetX(), -roi->GetY(), roi->GetWidth(), roi->GetHeight()));
            }
        }
        painter->restore();
    }

    auto WellCanvasWellItem::Impl::DrawRoiIndex(QPainter* painter) -> void {
        painter->save();
        for (const auto& roi : rois) {
            auto r = DefaultSettingHelper::GetBoundingRect(roi->GetX(), -roi->GetY(), roi->GetWidth(), roi->GetHeight());
            auto font = painter->font();
            font.setPixelSize(1);
            painter->setFont(font);
            auto pen = painter->pen();
            pen.setColor(Qt::white);
            pen.setWidthF(0.1);
            QTextOption textOption;
            textOption.setAlignment(Qt::AlignCenter);
            textOption.setWrapMode(QTextOption::WordWrap);
            QFontMetrics fm(painter->font());

            painter->setPen(pen);
            painter->drawText(QRectF(r.bottomLeft(), QSizeF{r.width(), static_cast<double>(fm.height())}), roi->GetName(), textOption);
            painter->drawText(r, QString::number(roi->GetIndex()), textOption);
        }
        painter->restore();
    }
}
