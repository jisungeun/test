﻿#pragma once

#include <memory>

#include <QColor>
#include <QRect>
#include <QVector>

#include "VesselMapExternalData.h"

namespace TC {
    class DefaultSettingHelper {
    public:
        using Self = DefaultSettingHelper;
        using Pointer = std::shared_ptr<DefaultSettingHelper>;

    protected:
        DefaultSettingHelper();

    public:
        ~DefaultSettingHelper();

        static auto MakeWellPositionName(int32_t row, int32_t column) -> QString;

        static auto GetWellRowName(int32_t row) -> QString;

        static auto GetBoundingRect(double w, double h) -> QRectF;
        static auto GetBoundingRect(double x, double y, double w, double h) -> QRectF;

        static auto CalculateCenterPosX(double start, double end) -> double;
        static auto CalculateCenterPosY(double start, double end) -> double;
        static auto CalculateSize(double start, double end) -> double;
    };
}
