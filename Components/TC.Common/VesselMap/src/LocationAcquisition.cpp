﻿#include "LocationAcquisition.h"

namespace TC {
    struct LocationAcquisition::Impl {
        AcquisitionIndex index;
        AcquisitionType type{AcquisitionType::Point};
        Position3D pos;
        Size2D size{0.0, 0.0};
    };

    LocationAcquisition::LocationAcquisition() : d{std::make_unique<Impl>()} {
    }

    LocationAcquisition::LocationAcquisition(const LocationAcquisition& other) :d{std::make_unique<Impl>()}{
        *d = *other.d;
    }

    LocationAcquisition::~LocationAcquisition() {
    }

    auto LocationAcquisition::operator=(const LocationAcquisition& other) -> LocationAcquisition& {
        *d = *other.d;
        return *this;
    }

    auto LocationAcquisition::SetIndex(AcquisitionIndex index) -> void {
        d->index = index;
    }

    auto LocationAcquisition::GetIndex() const -> AcquisitionIndex {
        return d->index;
    }

    auto LocationAcquisition::SetType(AcquisitionType type) -> void {
        d->type = type;
    }

    auto LocationAcquisition::GetType() const -> AcquisitionType {
        return d->type;
    }

    auto LocationAcquisition::SetPosition(double x, double y, double z) -> void {
        d->pos = {x, y, z};
    }

    auto LocationAcquisition::GetPosition() const -> Position3D {
        return d->pos;
    }

    auto LocationAcquisition::GetPosX() const -> double {
        return d->pos.x;
    }

    auto LocationAcquisition::GetPosY() const -> double {
        return d->pos.y;
    }

    auto LocationAcquisition::GetPosZ() const -> double {
        return d->pos.z;
    }

    auto LocationAcquisition::SetSize(double w, double h) -> void {
        d->size = {w, h};
    }

    auto LocationAcquisition::GetSize() const -> Size2D {
        return d->size;
    }

    auto LocationAcquisition::GetWidth() const -> double {
        return d->size.w;
    }

    auto LocationAcquisition::GetHeight() const -> double {
        return d->size.h;
    }
}
