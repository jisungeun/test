﻿#include "Roi.h"

namespace TC {
    struct Roi::Impl {
        RoiIndex index{};
        double x{}; // center pos in mm
        double y{}; // center pos in mm
        double w{}; // size in mm
        double h{}; // size in mm
        RoiShape::_enumerated shape{};
        QString name{};
    };

    Roi::Roi(RoiIndex idx, RoiShape shape, double x, double y, double w, double h, const QString& name) : d{std::make_unique<Impl>()} {
        d->index = idx;
        d->shape = shape;
        d->x = x;
        d->y = y;
        d->w = w;
        d->h = h;
        d->name = name;
    }

    Roi::~Roi() = default;

    auto Roi::SetIndex(const RoiIndex& index) -> void {
        d->index = index;
    }

    auto Roi::GetIndex() const -> RoiIndex {
        return d->index;
    }

    auto Roi::SetPosition(const double& x, const double& y) -> void {
        d->x = x;
        d->y = y;
    }

    auto Roi::GetX() const -> double {
        return d->x;
    }

    auto Roi::GetY() const -> double {
        return d->y;
    }

    auto Roi::SetSize(const double& w, const double& h) -> void {
        d->w = w;
        d->h = h;
    }

    auto Roi::GetWidth() const -> double {
        return d->w;
    }

    auto Roi::GetHeight() const -> double {
        return d->h;
    }

    auto Roi::SetShape(const RoiShape& shape) -> void {
        d->shape = shape;
    }

    auto Roi::GetShape() const -> RoiShape {
        return d->shape;
    }

    auto Roi::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto Roi::GetName() const -> QString {
        return d->name;
    }
}
