﻿#include <QMap>

#include "WellViewControl.h"
#include "DefaultSettingHelper.h"

namespace TC {
    struct WellViewControl::Impl {
        WellIndex focusedWellIndex{kInvalid};
        QList<Well::Pointer> wells;

        struct WellInfo {
            int32_t row{kInvalid};
            int32_t col{kInvalid};
            double x{0.0};
            double y{0.0};
        };

        QMap<WellIndex, WellInfo> wellInfo;

        struct {
            bool visible{false};
            Geometry2D geometry{0,0,0,0};
        } lastPreview;

        QMap<VesselAxis, RangeInMM> safeRanges;
    };

    WellViewControl::WellViewControl() : d{std::make_unique<Impl>()} {
    }

    WellViewControl::~WellViewControl() {
    }

    auto WellViewControl::InitWellPosition(const QList<Well::Pointer>& wells) -> void {
        if(!d->wellInfo.isEmpty()) {
            d->wellInfo.clear();
        }

        for(const auto& well : wells) {
            d->wellInfo[well->GetIndex()] = {well->GetRow(), well->GetColumn(), well->GetX(), well->GetY()};
        }
    }

    auto WellViewControl::SetCurrentFocusWell(WellIndex wellIndex) -> void {
        d->focusedWellIndex = wellIndex;
    }

    auto WellViewControl::GetCurrentFocusWell() const -> WellIndex {
        return d->focusedWellIndex;
    }

    auto WellViewControl::GetWellPosName() const -> QString {
        if(d->wellInfo.isEmpty()) return "";
        return DefaultSettingHelper::MakeWellPositionName(d->wellInfo[d->focusedWellIndex].row, d->wellInfo[d->focusedWellIndex].col);        
    }

    auto WellViewControl::GetWellPosByLensPos(double x, double y, double z) const -> Position3D {
        if(d->focusedWellIndex == kInvalid || d->wellInfo.isEmpty()) return {0.,0.,0.};

        const auto dx = x - d->wellInfo[d->focusedWellIndex].x;
        const auto dy = y - d->wellInfo[d->focusedWellIndex].y;

        return {dx, dy, z};
    }

    auto WellViewControl::SavePreviewStatus(bool isPreviewAreaVisible, const Geometry2D& geometry2D) -> void {
        d->lastPreview = {isPreviewAreaVisible, geometry2D};
    }

    auto WellViewControl::GetLastPreviewAreaVisibleStatus() const -> bool {
        return d->lastPreview.visible;
    }

    auto WellViewControl::GetLastPreviewArea() const -> Geometry2D {
        return d->lastPreview.geometry;
    }

    auto WellViewControl::SetSafePosRange(const VesselAxis& axis, const double& min, const double& max) -> void {
        d->safeRanges[axis] = {min, max};
    }

    auto WellViewControl::GetSafePosValue(const VesselAxis& axis, const double& inputValue) const -> double {
        if(d->safeRanges.contains(axis)) {
            if(inputValue <= d->safeRanges[axis].min)
                return d->safeRanges[axis].min;
            if(inputValue >= d->safeRanges[axis].max) {
                return d->safeRanges[axis].max;
            }
        }

        return inputValue;
    }
}
