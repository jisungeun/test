﻿#include <QColorDialog>
#include <QHeaderView>
#include <QMouseEvent>
#include <QToolTip>

#include <FileNameValidator.h>
#include <MessageDialog.h>

#include "WellGroupWidget.h"
#include "WellGroupTableDefine.h"
#include "WellGroupWidgetControl.h"
#include "WellGroupTableItemCreator.h"

namespace TC {
    struct WellGroupWidget::Impl {
        explicit Impl(Self* self) : self(self) {}

        Self* self{};
        WellGroupWidgetControl control;
        WellGroupTableItemCreator itemCreator;

        auto InitColumnHeader() -> void;
        auto InitTableProperty() -> void;
        auto Connect() -> void;
        auto Sort() -> void;
        auto IsValidGroupName(const QString& name) -> bool;
        auto IsValidWellName(const WellIndex& wellIndex, const QString& name) -> bool;
        auto NameChangingCanceled(const int32_t& row, const int32_t& column) const -> void;
        auto IsExistGroupName(const QString& groupName) -> bool;
    };

    WellGroupWidget::WellGroupWidget(QWidget* parent) : QTableWidget(parent), d{std::make_unique<Impl>(this)} {
        setMaximumWidth(750);
        d->InitColumnHeader();
        d->InitTableProperty();
        d->Connect();

        setMouseTracking(true);
    }

    WellGroupWidget::~WellGroupWidget() {
    }

    auto WellGroupWidget::SetViewMode(ViewMode mode) -> void {
        switch(mode) {
            case ViewMode::SetupMode:
                break;
            default:
                setEditTriggers(NoEditTriggers);
                break;
        }
    }

    auto WellGroupWidget::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->control.SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto WellGroupWidget::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->control.SetLocationDataRepo(locationDataRepo); 
    }

    auto WellGroupWidget::ClearAll() -> void {
        // TODO Clear All Data
        ClearTable();
        d->control.ClearData();
    }

    auto WellGroupWidget::ClearTable() -> void {
        this->setRowCount(0);
    }

    auto WellGroupWidget::AddWellsToTable(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> void {
        auto tableDataList = d->control.GenerateTableData(groupIndex, wellIndices);
        const auto dataCount = tableDataList.size();
        const auto currentRowCount = rowCount();
        const auto maxRowSize = dataCount + currentRowCount;

        for (int32_t row = currentRowCount; row < maxRowSize; ++row) {
            insertRow(row);
            const auto tableData = tableDataList.takeFirst(); // row가 변경되기 전에 tableData 한 개를 가져옴.
            for (int32_t col = 0; col < columnCount(); ++col) {
                setItem(row, col, d->itemCreator.CreateTableWidgetItem(GroupTableHeader::_from_integral(col), tableData));
            }
        }

        d->Sort();
    }

    auto WellGroupWidget::DeleteGroup(const GroupIndex& groupIndex) -> void {
        for (int32_t row = rowCount() - 1; row >= 0; --row) {
            if (groupIndex == item(row, GroupTableHeader::GroupIndex)->text().toInt()) {
                removeRow(row);
            }
        }
    }

    auto WellGroupWidget::RemoveWellsFromGroup(const QList<WellIndex>& wellIndices) -> void {
        QList<int32_t> removeTargetRows;

        for (const auto wellIndex : wellIndices) {
            for (int32_t row = 0; row < rowCount(); ++row) {
                if (wellIndex == item(row, GroupTableHeader::WellIndex)->text().toInt()) {
                    removeTargetRows.append(row);
                    break;
                }
            }
        }

        std::sort(removeTargetRows.begin(), removeTargetRows.end(), std::greater<int32_t>());
        for (auto r : removeTargetRows) {
            removeRow(r);
        }
    }

    auto WellGroupWidget::ChangeGroupName(const GroupIndex& groupIndex, const QString& name) -> void {
        for (int32_t row = 0; row < rowCount(); ++row) {
            if (groupIndex == item(row, GroupTableHeader::GroupIndex)->text().toInt()) {
                this->model()->blockSignals(true);
                const auto groupNameItem = item(row, GroupTableHeader::GroupName);
                groupNameItem->setText(name);
                this->model()->blockSignals(false);
            }
        }
    }

    auto WellGroupWidget::ChangeGroupColor(const GroupIndex& groupIndex, const QColor& color) -> void {
        for (int32_t row = 0; row < rowCount(); ++row) {
            if (groupIndex == item(row, GroupTableHeader::GroupIndex)->text().toInt()) {
                this->model()->blockSignals(true);
                const auto groupColorItem = item(row, GroupTableHeader::GroupColor);
                groupColorItem->setBackground(color);
                this->model()->blockSignals(false);
            }
        }
    }

    auto WellGroupWidget::ChangeWellName(const WellIndex& wellIndex, const QString& name) -> void {
        for (int32_t row = 0; row < rowCount(); ++row) {
            if (wellIndex == item(row, GroupTableHeader::WellIndex)->text().toInt()) {
                this->model()->blockSignals(true);
                if (name != item(row, GroupTableHeader::WellName)->text()) {
                    item(row, GroupTableHeader::WellName)->setText(name);
                }
                this->model()->blockSignals(false);
                break;
            }
        }
    }

    auto WellGroupWidget::MoveGroup(const GroupIndex& movingGroupIndex, const GroupIndex& targetGroupIndex) -> void {
        QString name;
        QBrush brush;
        for (int32_t row = 0; row < rowCount(); ++row) {
            if (targetGroupIndex == item(row, GroupTableHeader::GroupIndex)->text().toInt()) {
                name = item(row, GroupTableHeader::GroupName)->text();
                brush = item(row, GroupTableHeader::GroupColor)->background();
                break;
            }
        }

        if (name.isEmpty()) {
            return;
        }

        for (int32_t row = 0; row < rowCount(); ++row) {
            if (movingGroupIndex == item(row, GroupTableHeader::GroupIndex)->text().toInt()) {
                this->model()->blockSignals(true);
                item(row, GroupTableHeader::GroupName)->setText(name);
                item(row, GroupTableHeader::GroupColor)->setBackground(brush);
                item(row, GroupTableHeader::GroupIndex)->setText(QString::number(targetGroupIndex));
                this->model()->blockSignals(false);
            }
        }
    }

    auto WellGroupWidget::UpdateAcquisitionCount() -> void {
        for (int32_t row = 0; row < rowCount(); ++row) {
            const WellIndex wellIndex = item(row, GroupTableHeader::WellIndex)->text().toInt();
            const auto count = d->control.GetImagingPointCount(wellIndex);
            this->model()->blockSignals(true);
            item(row, GroupTableHeader::ImgPointCount)->setText(QString::number(count));
            this->model()->blockSignals(false);
        }
    }

    auto WellGroupWidget::viewportEvent(QEvent* event) -> bool {
        if (event->type() == QEvent::ToolTip) {
            const auto helpEvent = dynamic_cast<QHelpEvent*>(event);
            const auto index = indexAt(helpEvent->pos());

            if (index.isValid()) {
                QSize sizeHint = itemDelegate(index)->sizeHint(viewOptions(), index);
                QRect rectItem(0, 0, sizeHint.width(), sizeHint.height());

                if (rectItem.width() > visualRect(index).width()) {
                    QToolTip::showText(helpEvent->globalPos(), index.data(Qt::DisplayRole).toString(), nullptr, rectItem, 2000);
                }
                return true;
            }
        }
        return QTableWidget::viewportEvent(event);
    }

    void WellGroupWidget::onChangeSelectedWellIndexOnVesselCanvas(const QList<WellIndex>& wellIndices) {
        blockSignals(true);
        clearSelection();
        for(const auto& selectedWellIdx : wellIndices) {
            for(auto row = 0; row < rowCount(); ++row) {
                if(item(row, GroupTableHeader::WellIndex)->data(Qt::DisplayRole).toInt() == selectedWellIdx) {
                    item(row, GroupTableHeader::GroupName)->setSelected(true);
                    item(row, GroupTableHeader::WellName)->setSelected(true);
                    item(row, GroupTableHeader::WellPosition)->setSelected(true);
                    item(row, GroupTableHeader::ImgPointCount)->setSelected(true);
                    break;
                }
            }
        }
        blockSignals(false);
    }

    void WellGroupWidget::onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
        Q_UNUSED(bottomRight)
        if (roles.contains(Qt::EditRole) || roles.contains(Qt::BackgroundRole)) {
            switch (topLeft.column()) {
                case GroupTableHeader::GroupName: {
                    if(d->IsValidGroupName(topLeft.data().toString())) {
                        if(!d->IsExistGroupName(topLeft.data().toString())) {
                            const auto groupName = topLeft.data().toString();
                            const auto groupIndex = topLeft.siblingAtColumn(GroupTableHeader::GroupIndex).data().toInt();
                            emit sigChangeGroupName(groupIndex, groupName);
                        }
                        else{
                            d->NameChangingCanceled(topLeft.row(), topLeft.column());
                        }
                    }
                    else {
                        d->NameChangingCanceled(topLeft.row(), topLeft.column());
                    }
                    break;
                }
                case GroupTableHeader::WellName: {
                    const auto wellName = topLeft.data().toString();
                    const auto wellIndex = topLeft.siblingAtColumn(GroupTableHeader::WellIndex).data().toInt();
                    if(d->IsValidWellName(wellIndex, topLeft.data().toString())) {
                        emit sigChangeWellName(wellIndex, wellName);
                    }
                    else {
                        d->NameChangingCanceled(topLeft.row(), topLeft.column());
                    }
                    break;
                }
                case GroupTableHeader::GroupColor: {
                    const auto groupIndex = topLeft.siblingAtColumn(GroupTableHeader::GroupIndex).data().toInt();
                    const auto item = this->item(topLeft.row(), topLeft.column());
                    const auto groupColor = item->background().color();
                    emit sigChangeGroupColor(groupIndex, groupColor);
                }
                default: break;
            }
        }
    }

    void WellGroupWidget::onItemDoubleClicked(QTableWidgetItem* item) {
        if (item->column() == GroupTableHeader::GroupColor) {
            const auto itemRow = item->row();
            const auto groupIndex = this->item(itemRow, GroupTableHeader::GroupIndex)->data(Qt::EditRole).toInt();
            const auto currentColor = item->background();
            QColorDialog colorDlg(this);
            colorDlg.setCurrentColor(currentColor.color());
            colorDlg.setOption(QColorDialog::ShowAlphaChannel);
            if (colorDlg.exec() == QColorDialog::Accepted) {
                const auto newColor = colorDlg.currentColor();
                emit sigChangeGroupColor(groupIndex, newColor);
            }
        }
    }

    void WellGroupWidget::onItemSelectionChanged() {
        QList<WellIndex> selectedWellIndices;
        QMap<int32_t, bool> usedRows;

        for(const auto& selectedItem : selectedItems()) {
            bool ok{};
            if(!usedRows[selectedItem->row()]) {
                const auto wellIdx = item(selectedItem->row(), GroupTableHeader::WellIndex)->data(Qt::DisplayRole).toInt(&ok);
                if(ok) {
                    usedRows[selectedItem->row()] = true;
                    selectedWellIndices.push_back(wellIdx);
                }
            }
        }
        emit sigCurrentSelectedWellIndces(selectedWellIndices);
    }

    auto WellGroupWidget::Impl::InitColumnHeader() -> void {
        self->setColumnCount(control.GetColumnCount());
        self->setHorizontalHeaderLabels(control.GetColumnHeaderLabels());

        const auto header = self->horizontalHeader();
        if (!header) return;

        header->setMinimumSectionSize(130);
        header->setSectionResizeMode(GroupTableHeader::WellPosition, QHeaderView::Fixed);
        header->setSectionResizeMode(GroupTableHeader::ImgPointCount, QHeaderView::Fixed);
        header->setSectionResizeMode(GroupTableHeader::WellName, QHeaderView::Fixed);
        header->setSectionResizeMode(GroupTableHeader::GroupName, QHeaderView::Stretch);

        self->hideColumn(GroupTableHeader::WellIndex);
        self->hideColumn(GroupTableHeader::GroupIndex);
        self->hideColumn(GroupTableHeader::GroupColor);
    }

    auto WellGroupWidget::Impl::InitTableProperty() -> void {
        self->setSelectionMode(/*ContiguousSelection*/ExtendedSelection);
        self->verticalHeader()->hide();
        self->setAlternatingRowColors(true);
    }

    auto WellGroupWidget::Impl::Connect() -> void {
        connect(self->model(), &QAbstractItemModel::dataChanged, self, &Self::onDataChanged);
        connect(self, &Self::itemDoubleClicked, self, &Self::onItemDoubleClicked);
        connect(self, &Self::itemSelectionChanged, self, &Self::onItemSelectionChanged);
    }

    auto WellGroupWidget::Impl::Sort() -> void {
        // WellIndex 순으로 정렬 후 GroupIndex 순으로 정렬해준다.
        // int 정렬을 위해 item을 서브클래싱했음
        self->sortItems(GroupTableHeader::WellIndex);
        self->sortItems(GroupTableHeader::GroupIndex);
    }

    auto WellGroupWidget::Impl::IsValidGroupName(const QString& name) -> bool {
        FileNameValidator validator;
        validator.SetMaxLength(24);
        validator.AddInavlidCharacter('.');
        const auto result = validator.IsValid(name);
        if(!result) {
            TC::MessageDialog::warning(nullptr, tr("Invalid specimen name"), validator.GetErrorString());
        }

        return result;
    }

    auto WellGroupWidget::Impl::IsValidWellName(const WellIndex& wellIndex, const QString& name) -> bool {
        if(name.isEmpty()) {
            TC::MessageDialog::warning(nullptr, tr("Invalid well name"), tr("Empty string is not allowed."));
            return false;
        }


        // [TODO] error when try to set name to default position name
        Q_UNUSED(wellIndex)

        return true;
    }

    auto WellGroupWidget::Impl::NameChangingCanceled(const int32_t& row, const int32_t& column) const -> void {
        if(column != GroupTableHeader::GroupName && 
           column != GroupTableHeader::WellName) {
            return;
        }

        if(row >= self->rowCount()) {
            return;
        }

        const auto item = self->item(row, column);

        if(!item) {
            return;
        }

        self->model()->blockSignals(true);
        switch(column) {
            case GroupTableHeader::GroupName: {
                const auto groupIndex = self->item(row, GroupTableHeader::GroupIndex)->text().toInt();
                const auto previousName = control.GetGroupName(groupIndex);
                item->setText(previousName);
                break;
            }
            case GroupTableHeader::WellName: {
                const auto wellIndex = self->item(row, GroupTableHeader::WellIndex)->text().toInt();
                const auto previousName = control.GetWellName(wellIndex);
                item->setText(previousName);
                break;
            }
            default:
                break;
        }
        self->model()->blockSignals(false);
    }

    auto WellGroupWidget::Impl::IsExistGroupName(const QString& groupName) -> bool {
        const auto exist = control.IsExistGroupName(groupName);
        if(exist) {
            TC::MessageDialog::warning(nullptr, tr("Failed to change specimen name"), tr("\"%1\" is already existed.").arg(groupName));
        }
        return exist;
    }
}
