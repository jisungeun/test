﻿#include <QMap>

#include "WellGroupWidgetControl.h"
#include "VesselMapUtil.h"
#include "DefaultSettingHelper.h"
#include "LocationDataRepo.h"

namespace TC {
    struct WellGroupWidgetControl::Impl {
        VesselMapUtil::Pointer util{nullptr};
        LocationDataRepo::Pointer location{nullptr};
        QMap<GroupTableHeader, QString> columnHeaders;
        int32_t columnCount{-1};
        QStringList labels;

        auto InitColumnHeader() -> void;
    };

    auto WellGroupWidgetControl::Impl::InitColumnHeader() -> void {
        columnHeaders[GroupTableHeader::GroupIndex] = "Group Index";
        columnHeaders[GroupTableHeader::WellIndex] = "Well Index";
        columnHeaders[GroupTableHeader::GroupName] = "Specimen Name";
        columnHeaders[GroupTableHeader::ImgPointCount] = "Points";
        columnHeaders[GroupTableHeader::WellPosition] = "Position";
        columnHeaders[GroupTableHeader::WellName] = "Well Name";
        columnHeaders[GroupTableHeader::GroupColor] = "Color";
    }

    WellGroupWidgetControl::WellGroupWidgetControl() : d{std::make_unique<Impl>()} {
        d->InitColumnHeader();
        d->columnCount = static_cast<int32_t>(GroupTableHeader::_size());
        d->util = std::make_shared<VesselMapUtil>();
    }

    WellGroupWidgetControl::~WellGroupWidgetControl() {
    }

    auto WellGroupWidgetControl::ClearData() -> void {
        // nothing to do
    }

    auto WellGroupWidgetControl::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMapDataRepo) -> void {
        d->util->SetVesselMapDataRepo(vesselMapDataRepo);
    }

    auto WellGroupWidgetControl::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->location = locationDataRepo;
    }

    auto WellGroupWidgetControl::GetGroupName(const GroupIndex& groupIndex) const -> QString {
        if(d->util->GetGroupByGroupIndex(groupIndex) == nullptr) return "";
        return d->util->GetGroupByGroupIndex(groupIndex)->GetName();
    }

    auto WellGroupWidgetControl::GetWellName(const WellIndex& wellIndex) const -> QString {
        if(d->util->GetWellByWellIndex(wellIndex) == nullptr) return "";
        return d->util->GetWellByWellIndex(wellIndex)->GetName();
    }

    auto WellGroupWidgetControl::GetColumnCount() -> int32_t {
        return d->columnCount;
    }

    auto WellGroupWidgetControl::GetColumnHeaderLabels() -> QStringList {
        QStringList labels;
        for (int32_t i = 0; i < d->columnCount; ++i) {
            labels << d->columnHeaders[GroupTableHeader::_from_integral(i)];
        }
        return labels;
    }

    auto WellGroupWidgetControl::GenerateTableData(const GroupIndex& groupIndex, const QList<WellIndex>& wellIndices) -> QList<WellGroupTableData::Pointer> {
        QList<WellGroupTableData::Pointer> tableDataList;
        const auto group = d->util->GetGroupByGroupIndex(groupIndex);
        if (group != nullptr) {
            const auto groupName = group->GetName();
            const auto groupColor = group->GetColor();

            for (const auto wellIndex : wellIndices) {
                auto tableData = std::make_shared<WellGroupTableData>();
                const auto well = d->util->GetWellByWellIndex(wellIndex);
                if (nullptr == well) continue;

                const auto wellName = well->GetName();
                const auto wellPos = DefaultSettingHelper::MakeWellPositionName(well->GetRow(), well->GetColumn());
                const auto imgPtCount = d->location->GetAcquisitionIndicesByWellIndex(wellIndex).size();

                tableData->SetGroupIndex(groupIndex);
                tableData->SetGroupName(groupName);
                tableData->SetGroupColor(groupColor);
                tableData->SetWellIndex(wellIndex);
                tableData->SetWellName(wellName);
                tableData->SetWellPosition(wellPos);
                tableData->SetWellImgPointCount(imgPtCount);

                tableDataList.push_back(tableData);
            }
        }

        return tableDataList;
    }

    int32_t WellGroupWidgetControl::GetImagingPointCount(WellIndex wellIndex) {
        return d->location->GetAcquisitionIndicesByWellIndex(wellIndex).size();
    }

    auto WellGroupWidgetControl::IsExistGroupName(const QString& groupName) const -> bool {
        if(d->util->GetGroupByGroupName(groupName) == nullptr) return false;
        return true;
    }

}
