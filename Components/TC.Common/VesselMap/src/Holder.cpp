﻿#include "Holder.h"

namespace TC {
    struct Holder::Impl {
        double width{0.0};
        double height{0.0};
    };

    Holder::Holder() : d{std::make_unique<Impl>()} {
    }

    Holder::Holder(const Holder& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    Holder::~Holder() {
    }

    auto Holder::operator=(const Holder& other) -> Holder& {
        *d = *other.d;
        return *this;
    }

    auto Holder::SetWidth(double width) -> void {
        d->width = width;
    }

    auto Holder::GetWidth() const -> double {
        return d->width;
    }

    auto Holder::SetHeight(double height) -> void {
        d->height = height;
    }

    auto Holder::GetHeight() const -> double {
        return d->height;
    }
}
