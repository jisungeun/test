﻿#include <QContextMenuEvent>
#include <QMenu>
#include <QHeaderView>
#include <MessageDialog.h>

#include "ImagingPointTable.h"

#include "ImagingPointTableControl.h"
#include "ImagingPointTableItemCreator.h"
#include "ImagingPointTableData.h"

namespace TC {
    struct ImagingPointTable::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};
        ImagingPointTableControl control;
        ImagingPointTableItemCreator itemCreator;

        ViewMode viewMode{ViewMode::PerformMode};

        auto InitTableHeader() -> void;
        auto InitTableProperty() -> void;
        auto MakeConnect() -> void;

        auto ClearTable() -> void;
        auto SelectImagingPointOnTimelapseMode(const int32_t& row) -> void;
        auto GetSourceWellIndex(bool& isValid) const -> WellIndex;
        auto RefreshTable() -> void;

        auto GetWellNameAndRoiIndex(const int32_t& row) const -> QString;
    };

    ImagingPointTable::ImagingPointTable(QWidget* parent) : QTableWidget(parent), d{std::make_unique<Impl>(this)} {
        d->InitTableHeader();
        d->InitTableProperty();
        d->MakeConnect();
    }

    ImagingPointTable::~ImagingPointTable() {
    }

    auto ImagingPointTable::ClearAll() -> void {
        d->control.Clear();
        d->ClearTable();
    }

    auto ImagingPointTable::InitColumnHeader() -> void {
        const auto hideROI = !d->control.IsRoiExist();
        horizontalHeader()->setSectionHidden(+ImagingTableHeader::ROI, hideROI);

        if(hideROI) {
            horizontalHeader()->setDefaultSectionSize(60);
            horizontalHeader()->setMinimumSectionSize(60);
        }
        else {
            horizontalHeader()->setDefaultSectionSize(52);
            horizontalHeader()->setMinimumSectionSize(52);
        }
        
        horizontalHeader()->hideSection(+ImagingTableHeader::AcquisitionIndex);
        horizontalHeader()->hideSection(+ImagingTableHeader::PosZ);
        horizontalHeader()->hideSection(+ImagingTableHeader::WellIndex);
        
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::Well, QHeaderView::ResizeToContents);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::ROI, QHeaderView::ResizeToContents);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::PointID, QHeaderView::ResizeToContents);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::PosX, QHeaderView::Stretch);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::PosY, QHeaderView::Stretch);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::PosZ, QHeaderView::Stretch);
        horizontalHeader()->setSectionResizeMode(+ImagingTableHeader::Size, QHeaderView::Stretch);
    }

    auto ImagingPointTable::SetVesselMapDataRepo(const VesselMapDataRepo::Pointer& vesselMap) -> void {
        d->control.SetVesselMapDataRepo(vesselMap);
    }

    auto ImagingPointTable::SetLocationDataRepo(const LocationDataRepo::Pointer& locationDataRepo) -> void {
        d->control.SetLocationDataRepo(locationDataRepo);
    }

    auto ImagingPointTable::SetViewMode(ViewMode mode) -> void {
        d->viewMode = mode;
        switch (mode) {
            case ViewMode::TimelapseMode: 
                setSelectionMode(SingleSelection);
                break;
            default: 
                setSelectionMode(ExtendedSelection);
                break;
        }
    }

    auto ImagingPointTable::SetCurrentWellIndex(WellIndex wellIndex) -> void {
        if (d->viewMode == +ViewMode::TimelapseMode) { return; }
        d->control.SetCurrentWellIndex(wellIndex);
        d->RefreshTable();
    }

    auto ImagingPointTable::AddAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        Q_UNUSED(wellIndex)
        Q_UNUSED(acquisitionIndex)
        d->RefreshTable();
    }

    auto ImagingPointTable::SetAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        Q_UNUSED(wellIndex)
        Q_UNUSED(acquisitionIndex)
        d->RefreshTable();
    }

    auto ImagingPointTable::DeleteAcquisitionData(WellIndex wellIndex, AcquisitionIndex acquisitionIndex) -> void {
        Q_UNUSED(wellIndex)
        Q_UNUSED(acquisitionIndex)
        d->RefreshTable();
    }

    void ImagingPointTable::onAcquisitionDataDeleteRequested() {
        if (selectedItems().isEmpty()) return;

        const auto answer = MessageDialog::question(this, tr("Delete Acquisition Point"), tr("Do you want to delete selected acquisition point(s)?"),
                                QDialogButtonBox::Yes|QDialogButtonBox::No, QDialogButtonBox::Yes);
        if(answer != QDialogButtonBox::Yes) {
            return;
        }

        QList<QPair<WellIndex, AcquisitionIndex>> deleteTargets;
        for (const auto& selectedItem : selectedItems()) {
            if (selectedItem->column() == +ImagingTableHeader::Well) {
                const auto wellIndex = this->item(selectedItem->row(), +ImagingTableHeader::WellIndex)->data(Qt::EditRole).toInt();
                const auto acqIndex = this->item(selectedItem->row(), +ImagingTableHeader::AcquisitionIndex)->data(Qt::EditRole).toInt();
                deleteTargets.push_back({wellIndex, acqIndex});
            }
        }

        if (deleteTargets.isEmpty()) return;

        for (const auto& pointPair : deleteTargets) {
            emit sigDeleteAcquisitionItem(pointPair.first, pointPair.second);
        }
    }

    void ImagingPointTable::onAcquisitionDataCopyRequested() {
        if (selectedItems().isEmpty()) return;
        bool isValid{};
        const auto wellIndex = d->GetSourceWellIndex(isValid);
        if(!isValid) {
            MessageDialog::warning(this, tr("Error"), tr("Select only points with the same well."));
            return;
        }

        QList<AcquisitionIndex> pointIndices;
        for (const auto& selectedItem : selectedItems()) {
            if(selectedItem->column() == +ImagingTableHeader::Well) {
                bool okAcq{};
                const auto acqIndex = this->item(selectedItem->row(), +ImagingTableHeader::AcquisitionIndex)->data(Qt::EditRole).toInt(&okAcq);
                if(okAcq) {
                    pointIndices.push_back(acqIndex);
                }
            }
        }

        emit sigCopyAcquisitionItems(wellIndex, pointIndices);
    }

    auto ImagingPointTable::SetShowAll(bool showAll) -> void {
        d->control.SetShowAll(showAll);
        d->RefreshTable();
    }
    
    auto ImagingPointTable::keyPressEvent(QKeyEvent* event) -> void {
        if (selectedItems().isEmpty()) return;

        if (d->viewMode == +ViewMode::PerformMode) {
            if (event->key() == Qt::Key_Delete) {
                onAcquisitionDataDeleteRequested();
            }
        }

        QTableWidget::keyPressEvent(event);
    }

    auto ImagingPointTable::mousePressEvent(QMouseEvent* event) -> void {
        if(!indexAt(event->pos()).isValid()) {
            clearSelection();
            emit sigImagingPointSelected({},{});
        }

        QTableWidget::mousePressEvent(event);
    }

    void ImagingPointTable::onTableSelectionChanged() {
        QList<QPair<WellIndex, AcquisitionIndex>> selected;
        auto row{-1};
        for (const auto& selectedItem : selectedItems()) {
            if (selectedItem->column() == +ImagingTableHeader::Well) {
                const auto wellIndex = this->item(selectedItem->row(), +ImagingTableHeader::WellIndex)->data(Qt::EditRole).toInt();
                const auto acqIndex = this->item(selectedItem->row(), +ImagingTableHeader::AcquisitionIndex)->data(Qt::EditRole).toInt();
                selected.push_back({wellIndex, acqIndex});
                row = selectedItem->row();
            }
        }
        emit sigCurrentSelectedItems(selected);

        if (d->viewMode == +ViewMode::TimelapseMode) {
            d->SelectImagingPointOnTimelapseMode(row);
        }
    }

    void ImagingPointTable::onItemDoubleClicked(QTableWidgetItem* item) {
        const auto row = item->row();
        const WellIndex wellIndex = this->item(row, +ImagingTableHeader::WellIndex)->data(Qt::EditRole).toInt();
        const AcquisitionIndex acquisitionIndex = this->item(row, +ImagingTableHeader::AcquisitionIndex)->data(Qt::EditRole).toInt();
        const double x = this->item(row, +ImagingTableHeader::PosX)->data(Qt::EditRole).toDouble();
        const double y = this->item(row, +ImagingTableHeader::PosY)->data(Qt::EditRole).toDouble();
        const double z = this->item(row, +ImagingTableHeader::PosZ)->data(Qt::EditRole).toDouble();

        emit sigAcqItemDoubleClicked(wellIndex, acquisitionIndex, x, y, z);
    }

    void ImagingPointTable::onUpdateTableSectionByWellCanvas(const QList<QPair<WellIndex, AcquisitionIndex>>& selected) {
        this->blockSignals(true);
        this->clearSelection();

        QList<int32_t> selectedRows;
        for (int32_t r = 0; r < this->rowCount(); ++r) {
            const auto wellIdx = item(r, +ImagingTableHeader::WellIndex)->data(Qt::EditRole).toInt();
            const auto acqIdx = item(r, +ImagingTableHeader::AcquisitionIndex)->data(Qt::EditRole).toInt();
            if(selected.contains({wellIdx,acqIdx})) {
                selectedRows.push_back(r);
            }
        }
        for(const auto& r : selectedRows) {
            for(auto c = 0; c < this->columnCount(); ++c) {
                this->item(r, c)->setSelected(true);
            }
        }

        this->blockSignals(false);

        if (d->viewMode == +ViewMode::TimelapseMode) {
            if(selectedRows.size()==1) {
                d->SelectImagingPointOnTimelapseMode(selectedRows.first());
            }
        }
    }

    void ImagingPointTable::Impl::InitTableHeader() {
        self->verticalHeader()->hide();

        self->setColumnCount(control.GetColumnCount());
        self->setHorizontalHeaderLabels(control.GetColumnHeaderLabels());
    }

    void ImagingPointTable::Impl::InitTableProperty() {
        self->setSelectionMode(ExtendedSelection);
        self->setSelectionBehavior(SelectRows);
    }

    void ImagingPointTable::Impl::MakeConnect() {
        connect(self, &Self::itemSelectionChanged, self, &Self::onTableSelectionChanged);
        connect(self, &Self::itemDoubleClicked, self, &Self::onItemDoubleClicked);
    }

    void ImagingPointTable::Impl::ClearTable() {
        if (self->rowCount() != 0) {
            self->setRowCount(0);
        }
    }

    auto ImagingPointTable::Impl::SelectImagingPointOnTimelapseMode(const int32_t& row) -> void {
        if (row >= 0 && row < self->rowCount()) {
            const QString wellNameWithRoi = GetWellNameAndRoiIndex(row);
            const QString pointID = self->item(row, +ImagingTableHeader::PointID)->data(Qt::EditRole).toString();
            emit self->sigImagingPointSelected(wellNameWithRoi, pointID);
        }
    }

    auto ImagingPointTable::Impl::GetSourceWellIndex(bool& isValid) const -> WellIndex {
        QMap<WellIndex, bool> map;
        for(const auto& item : self->selectedItems()) {
            if(item->column() == +ImagingTableHeader::Well) {
                bool ok{};
                const WellIndex wellindex = self->item(item->row(), +ImagingTableHeader::WellIndex)->data(Qt::EditRole).toInt(&ok);
                if(ok) {
                    map[wellindex] = true;
                }
            }
        }
        if(map.size()==1) {
            isValid = true;
            return map.key(true);
        }
        else {
            return -1;
        }
    }

    auto ImagingPointTable::Impl::RefreshTable() -> void {
        ClearTable();
        if (control.IsShowAll()) {
            for (const auto& wellIndex : control.GetWellIndices()) {
                auto pointIndex = 1;
                for (const auto& acq : control.GetWellAcquisitionIndices(wellIndex)) {
                    const auto rowPointIndex = pointIndex++;
                    auto tableData = control.GenerateAcquisitionTableData(wellIndex, acq, rowPointIndex);
                    if (tableData == nullptr) continue;
                    self->insertRow(self->rowCount());
                    for (auto col = 0; col < self->columnCount(); ++col) {
                        self->setItem(self->rowCount() - 1, col, itemCreator.CreateAcquisitionTableItem(ImagingTableHeader::_from_integral(col), tableData));
                    }
                }
            }
        }
        else {
            auto pointIndex = 1;
            for (const auto& acq : control.GetCurrentWellAcquisitionIndices()) {
                const auto rowPointIndex = pointIndex++;
                auto tableData = control.GenerateAcquisitionTableData(control.GetCurrentWellIndex(), acq, rowPointIndex);
                if (tableData == nullptr) continue;
                self->insertRow(self->rowCount());
                for (auto col = 0; col < self->columnCount(); ++col) {
                    self->setItem(self->rowCount() - 1, col, itemCreator.CreateAcquisitionTableItem(ImagingTableHeader::_from_integral(col), tableData));
                }
            }
        }
    }

    auto ImagingPointTable::Impl::GetWellNameAndRoiIndex(const int32_t& row) const -> QString {
        auto wellName = self->item(row, +ImagingTableHeader::Well)->data(Qt::EditRole).toString();
        const auto roiIndex = self->item(row, +ImagingTableHeader::ROI)->data(Qt::EditRole).toString();

        // ROI column에 ROI 인덱스가 등록되어 있으면, well name에 ROI 인덱스를 추가한다. (이유: 데이터 폴더명이 ROI가 존재하는 경우 'A1R1', 존재하지 않는 경우 'A1'으로 생성되기 때문) 
        // ROI index가 있는 경우 'R#'(숫자), 없는 경우 '-'로 시작하므로 roiIndex가 'R'로 시작하는지 확인한다.
        if (roiIndex.startsWith(QChar('R'))) {
            wellName = wellName + roiIndex;
        }
        return wellName;
    }
}
