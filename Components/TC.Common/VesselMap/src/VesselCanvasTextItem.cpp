﻿#include "VesselCanvasTextItem.h"

#include <QPainter>

#include "DefaultSettingHelper.h"
#include "GraphicsItemDefine.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    struct VesselCanvasTextItem::Impl {
        ViewMode viewMode{ViewMode::SetupMode};
        auto FitFontToItem(QFont& font) -> void;
        double width{};
        double height{};
        QString label{};
        QColor penColor{RowColumnIndexColor};
        Qt::Alignment alignment{Qt::AlignCenter};
    };

    auto VesselCanvasTextItem::Impl::FitFontToItem(QFont& font) -> void {
        QFontMetrics fm(font);
        double pointSizeF = 1.;
        switch (viewMode) {
        case ViewMode::SetupMode:
        case ViewMode::DataNaviMode:
        case ViewMode::CanvasMode:
        case ViewMode::InfoMode:
        case ViewMode::PreviewMode:
            pointSizeF = width < height ? width/4 : height/4;
            break;
        case ViewMode::PerformMode:
        case ViewMode::TimelapseMode:
        case ViewMode::CopyDlgMode:
            pointSizeF = width < height ? width/2 : height/2;
            break;
        default:
            pointSizeF = width < height ? width/4 : height/4;
            break;
        }

        font.setPointSizeF(pointSizeF);
    }

    VesselCanvasTextItem::VesselCanvasTextItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
        setCacheMode(DeviceCoordinateCache);
    }

    VesselCanvasTextItem::~VesselCanvasTextItem() {

    }

    auto VesselCanvasTextItem::SetViewMode(ViewMode viewMode) -> void {
        d->viewMode = viewMode;
    }

    auto VesselCanvasTextItem::SetLabel(const QString& label) -> void {
        d->label = label;
    }

    auto VesselCanvasTextItem::SetSize(double w, double h) -> void {
        d->width = w;
        d->height = h;
    }

    auto VesselCanvasTextItem::SetPosition(double x, double y) -> void {
        setPos(x, -y);
    }

    auto VesselCanvasTextItem::SetTextAlignment(Qt::Alignment alignment) -> void {
        d->alignment = alignment;
    }

    auto VesselCanvasTextItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::RowColIndexItem);
    }

    auto VesselCanvasTextItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->width, d->height);
    }

    auto VesselCanvasTextItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        Q_UNUSED(option)

        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);

        painter->save();
        painter->setPen(d->penColor);
        QFont newFont = painter->font();
        QTextOption textOption;
        textOption.setAlignment(d->alignment);
        textOption.setWrapMode(QTextOption::WordWrap);
        d->FitFontToItem(newFont);
        painter->setFont(newFont);
        painter->drawText(boundingRect(), d->label, textOption);
        painter->restore();
    }
}
