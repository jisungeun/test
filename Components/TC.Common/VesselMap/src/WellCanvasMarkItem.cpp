﻿#include <QGraphicsSceneEvent>
#include <QStyleOptionGraphicsItem>
#include <QPainter>

#include "WellCanvasMarkItem.h"
#include "GraphicsItemDefine.h"
#include "DefaultSettingHelper.h"
#include "VesselMapWidgetColorCodes.h"

namespace TC {
    /// <summary>
    /// WellCanvasMarkItem
    /// </summary>
    struct WellCanvasMarkItem::Impl {
        MarkIndex markIndex{kInvalid};
        WellIndex wellIndex{kInvalid};

        Position3D pos{};

        QColor pointColor{MarkPointColor};
        QColor selectedColor{SelectedWellBorderColor};
        double pointSize{kMinLocPointSize};
    };

    WellCanvasMarkItem::WellCanvasMarkItem(WellIndex wellIndex, MarkIndex markIndex, Position3D pos, QGraphicsItem* parent)
    : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
        d->wellIndex = wellIndex;
        d->markIndex = markIndex;

        setFlag(ItemIsSelectable, true);
        setCacheMode(DeviceCoordinateCache);
        setAcceptHoverEvents(true);
        SetPosition(pos.x, pos.y, pos.z);
        setZValue(GraphicsItemZValue::Mark);
    }

    WellCanvasMarkItem::~WellCanvasMarkItem() {
    }

    auto WellCanvasMarkItem::GetWellIndex() const -> WellIndex {
        return d->wellIndex;
    }

    auto WellCanvasMarkItem::GetMarkIndex() const -> MarkIndex {
        return d->markIndex;
    }

    auto WellCanvasMarkItem::SetPosition(double x, double y, double z) -> void {
        d->pos.x = x;
        d->pos.y = y;
        d->pos.z = z;
        setPos(x, -y);
    }

    auto WellCanvasMarkItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::MarkPointItem);
    }

    auto WellCanvasMarkItem::shape() const -> QPainterPath {
        QPainterPath path;
        path.addEllipse(boundingRect());
        return path;
    }

    auto WellCanvasMarkItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize);
    }

    auto WellCanvasMarkItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void {
        if(event->button() == Qt::LeftButton) {
            emit sigMarkItemDoubleClicked(d->wellIndex, d->markIndex, pos().x(), -pos().y());
        }
        QGraphicsItem::mouseDoubleClickEvent(event);
    }

    auto WellCanvasMarkItem::GetPointColor() const -> QColor {
        return d->pointColor;
    }

    auto WellCanvasMarkItem::GetSelectedColor() const -> QColor {
        return d->selectedColor;
    }

    auto WellCanvasMarkItem::UpdatePointSize(const double& lod) -> void {
        d->pointSize = 10/lod;
        if(d->pointSize < kMinLocPointSize) {
            d->pointSize = kMinLocPointSize;
        }
        update();
    }

    auto WellCanvasMarkItem::GetPointSize() const -> double {
        return d->pointSize;
    }

    auto WellCanvasMarkItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        auto brush = d->pointColor;
        auto pen = QPen(Qt::NoPen);
        if(option->state & QStyle::State_Selected) {
            pen.setColor(d->selectedColor);
            pen.setWidth(0);
            pen.setStyle(Qt::SolidLine);
            brush = brush.darker();
        }

        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawEllipse(DefaultSettingHelper::GetBoundingRect(d->pointSize, d->pointSize));
    }

    /// <summary>
    /// WellCanvasMarkTileItem
    /// </summary>
    struct WellCanvasMarkTileItem::Impl {
        Size2D tileSize{};
        double penWidth{};
        QColor penColor{};
    };

    WellCanvasMarkTileItem::WellCanvasMarkTileItem(WellIndex wellIndex, MarkIndex markIndex, Position3D pos, Size2D tileSize, QGraphicsItem* parent) :
    WellCanvasMarkItem(wellIndex, markIndex, pos, parent), d{std::make_unique<Impl>()} {
        d->penColor = GetPointColor();
        SetTileSize(tileSize.w, tileSize.h);
    }

    WellCanvasMarkTileItem::~WellCanvasMarkTileItem() {
    }

    auto WellCanvasMarkTileItem::SetTileSize(double w, double h) -> void {
        d->tileSize = {w, h};
    }

    auto WellCanvasMarkTileItem::type() const -> int {
        return static_cast<int32_t>(GraphicsItemType::MarkTileItem);
    }

    auto WellCanvasMarkTileItem::boundingRect() const -> QRectF {
        return DefaultSettingHelper::GetBoundingRect(d->tileSize.w, d->tileSize.h);
    }

    auto WellCanvasMarkTileItem::shape() const -> QPainterPath {
        QPainterPath path;
        path.addRect(boundingRect());
        return path;
    }

    auto WellCanvasMarkTileItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        UpdatePointSize(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        QPen pen(d->penColor, 0, Qt::SolidLine);
        QColor brushColor = Qt::transparent;
        if (option->state & QStyle::State_Selected) {
            pen.setColor(GetSelectedColor());
            pen.setWidth(0);
            pen.setStyle(Qt::SolidLine);

            brushColor = d->penColor;
            brushColor.setAlpha(50);
        }

        painter->save();
        painter->setPen(pen);
        const auto rect = DefaultSettingHelper::GetBoundingRect(d->tileSize.w, d->tileSize.h);
        QPainterPath path;
        painter->setBrush(QBrush(brushColor, Qt::SolidPattern));
        path.addRect(rect);
        painter->drawPath(path);
        painter->restore();

        if(GetPointSize() < d->tileSize.w && GetPointSize() < d->tileSize.h) {
            WellCanvasMarkItem::paint(painter, option, widget);
        }
    }
}
