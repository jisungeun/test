﻿#pragma once
#include <memory>
#include "VesselMapExternalData.h"

namespace TC {
    class Roi {
    public:
        using Self = Roi;
        using Pointer = std::shared_ptr<Roi>;
        using List = QList<Pointer>;
        using Map = QMap<WellIndex, List>;

        Roi(RoiIndex idx, RoiShape shape, double x, double y, double w, double h, const QString& name);
        ~Roi();
        
        auto SetIndex(const RoiIndex& index) -> void;
        auto GetIndex() const -> RoiIndex;

        auto SetPosition(const double& x, const double& y) -> void; // well's center position in mm;
        auto GetX() const -> double;
        auto GetY() const -> double;

        auto SetSize(const double& w, const double& h) -> void;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetShape(const RoiShape& shape) -> void;
        auto GetShape() const -> RoiShape;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
