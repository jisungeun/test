﻿#pragma once

#include <memory>
#include <QColor>
#include <QString>

#include "VesselMapExternalData.h"

namespace TC {
    class WellGroup {
    public:
        using Self = WellGroup;
        using Pointer = std::shared_ptr<Self>;

        WellGroup();
        WellGroup(const WellGroup& other);
        ~WellGroup();

        auto operator=(const WellGroup& other) -> WellGroup&;

        auto SetIndex(GroupIndex index) -> void;
        auto GetIndex() const -> GroupIndex;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetColor(const QColor& color) -> void;
        auto GetColor() const -> QColor;

        auto AddWell(WellIndex wellIndex) -> void;
        auto AddWells(const QList<WellIndex>& wellIndices) -> void;

        auto DeleteWell(WellIndex wellIndex) -> void;
        auto DeleteWells(const QList<WellIndex>& wellIndices) -> void;

        auto SetWells(const QList<WellIndex>& wellIndices) -> void;
        auto GetWells() const -> QList<WellIndex>&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
