﻿#pragma once

#include <QDialog>
#include <QMap>
#include <QStringList>

#include <TCFMetaReader.h>
#include <TimePointSelectorDef.h>

#include "TCSingleTimeSelectorExport.h"

namespace TC::SingleTimeSelector {    
    class TCSingleTimeSelector_API TimePointSettingDialog : public QDialog {
        Q_OBJECT
	public:
        explicit TimePointSettingDialog(QWidget* parent = nullptr);
        ~TimePointSettingDialog() override;

        auto SetTCFs(const QStringList& tcfs) -> void;
        auto SetInputConfig(const TimePointSelector::Config &config) -> void;

        auto GetResult() -> QMap<QString, std::tuple<TimePointSelector::TimeSelectionOutput,QList<double>>>;
        auto GetMeta(const QString& path)->IO::TCFMetaReader::Meta::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
