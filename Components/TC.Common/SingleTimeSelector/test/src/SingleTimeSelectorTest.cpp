#include <iostream>
#include <QApplication>

#include <SingleTimeSettingDialog.h>

using namespace TC::SingleTimeSelector;
using namespace TC::TimePointSelector;

int main(int argc,char** argv) {
    QApplication app(argc, argv);

    TimePointSettingDialog tps;
    tps.SetTCFs({
        "F:/CellLibrary/230726_Record/230724.180056.test.006.Group1.A1.T002P01/230724.180056.test.006.Group1.A1.T002P01.TCF"        
    });

    Config config;
    config.modalities = Modality::Ht2d | Modality::Fl2d;
    config.fl2dCount = 1;

    tps.SetInputConfig(config);

    tps.exec();
    const auto rtuple = tps.GetResult().first();
	const auto result = std::get<0>(rtuple);
	for (const auto modality : result.modalties) {
		switch (modality) {
		case Modality::Ht2d:
			std::cout << "HT2d" << std::endl;
			break;
		case Modality::Ht3d:
			std::cout << "HT3d" << std::endl;
			break;
		case Modality::Fl2dCh1:
			std::cout << "FL2dCh1" << std::endl;
			break;
		case Modality::Fl2dCh2:
			std::cout << "FL2dCh2" << std::endl;
			break;
		case Modality::Fl2dCh3:
			std::cout << "FL2dCh3" << std::endl;
			break;
		case Modality::Fl3dCh1:
			std::cout << "FL3dCh1" << std::endl;
			break;
		case Modality::Fl3dCh2:
			std::cout << "FL3dCh2" << std::endl;
			break;
		case Modality::Fl3dCh3:
			std::cout << "FL3dCh3" << std::endl;
			break;
		case Modality::Bf:
			std::cout << "BF" << std::endl;
			break;
		}
	}
    
	const auto realTime = std::get<1>(rtuple);
    std::cout << "Selected time : " << result.times.first() << std::endl;
	std::cout << "Selected Real Time: " << static_cast<int>(realTime.first()) << std::endl;
        
    return EXIT_SUCCESS;
}