﻿#include <QDebug>
#include <QHeaderView>
#include <QFileInfo>

#include <TCFMetaReader.h>

#include "TCFListWidget.h"

namespace TC::SingleTimeSelector {
    struct TCFListWidget::Impl {
        auto InitUI() -> void;
    };
    
    TCFListWidget::TCFListWidget(QWidget* parent) : QTableWidget(parent), d{std::make_unique<Impl>()} {
        horizontalHeader()->hide();
        verticalHeader()->hide();

        setSelectionBehavior(QAbstractItemView::SelectRows);
        setSelectionMode(QAbstractItemView::SingleSelection);
        setEditTriggers(QAbstractItemView::NoEditTriggers);

        setRowCount(2);
        setColumnCount(10);

        // set hor header
        setItem(0, 0, new QTableWidgetItem("TCF"));
        setItem(0, 1, new QTableWidgetItem("HT2D"));
        setItem(0, 2, new QTableWidgetItem("HT3D"));
        setItem(0, 3, new QTableWidgetItem("FL2D"));
        setItem(0, 6, new QTableWidgetItem("FL3D"));
        setItem(0, 9, new QTableWidgetItem("BF"));

        for (auto i = 0; i < 2; i++) {
            auto col = 3 * (i + 1);

            setSpan(0, col, 1, 3);

		    setItem(1, col++, new QTableWidgetItem("Ch1"));
	        setItem(1, col++, new QTableWidgetItem("Ch2"));
	        setItem(1, col, new QTableWidgetItem("Ch3"));
        }

        setSpan(0, 0, 2, 1);
        setSpan(0, 1, 2, 1);
        setSpan(0, 2, 2, 1);
        setSpan(0, 9, 2, 1);

        connect(this, &QTableWidget::itemSelectionChanged, [=]() {
            const auto currentItem = item(currentRow(), 0);
        	if (currentItem == nullptr) return;

            emit sigCurrentItemChanged(currentItem->data(Qt::UserRole).toString());
        });
    }

    TCFListWidget::~TCFListWidget() {
    }

    auto TCFListWidget::AddTCF(const QString& tcfId, const QString& displayName, QMap<TimePointSelector::Modality, int> timepoints) -> void {
        const auto newRow = rowCount();
        auto col = 0;

    	setRowCount(newRow + 1);

        const auto tcfItem = new QTableWidgetItem(displayName);
        tcfItem->setData(Qt::UserRole, tcfId);
        setItem(newRow, col++, tcfItem);

		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Ht2d, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Ht3d, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl2dCh1, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl2dCh2, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl2dCh3, 0))));
        setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl3dCh1, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl3dCh2, 0))));
		setItem(newRow, col++, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Fl3dCh3, 0))));
		setItem(newRow, col, new QTableWidgetItem(QString::number(timepoints.value(TimePointSelector::Modality::Bf, 0))));
    }

    auto TCFListWidget::SetCurrentTCF(const QString& tcfId) -> void {
        QSignalBlocker blocker(this);

        for (auto row = 2; row < rowCount(); row++) {
    		auto tcfCell = item(row, 0);
            if (tcfCell == nullptr) continue;

            if (tcfCell->data(Qt::UserRole).toString() == tcfId) {
            	setCurrentCell(row, 0);
                break;
            }
        }
    }

}
