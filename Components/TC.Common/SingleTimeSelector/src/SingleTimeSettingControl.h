﻿#pragma once

//#include <memory>

#include <QString>

#include <TCFMetaReader.h>

#include "TimePointSelectorDef.h"

namespace TC::SingleTimeSelector {
    class TimePointSettingControl {
	public:
        struct TCFInfo {
	        QString path;
            IO::TCFMetaReader::Meta::Pointer meta;
            QList<int> timeIndexes;
            QList<TimePointSelector::Modality> modalities;
        };

        explicit TimePointSettingControl();
        ~TimePointSettingControl();

        /**
         * @brief       Register TCF into internal repository(QMap)
         * @param tcf   TCF file path
         */
		auto RegisterTCF(const QString& tcf) const -> IO::TCFMetaReader::Meta::Pointer;

    	auto GetTimePoints(const QString& tcfId) const -> QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>;

        auto SetCurrentTCFId(const QString& tcfId) const -> void;
        auto GetCurrentTCFId() const -> QString;

        auto SetModalities(const QString& tcfId, const QList<TimePointSelector::Modality>& modalities) -> void;
        auto GetModalities(const QString& tcfId) const -> QList<TimePointSelector::Modality>;

        auto SetTimeIndexes(const QString& tcfId, const QList<int>& indexes) const -> void;
        auto GetTimeIndexes(const QString& tcfId) const -> QList<int>;

        auto GetAllTimePointInfo() const -> QMap<QString, TCFInfo>;

		auto SetConfig(const TimePointSelector::Config& config) -> void;
		auto GetConfig() const ->TimePointSelector::Config;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
