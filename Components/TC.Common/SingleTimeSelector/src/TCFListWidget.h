﻿#pragma once

#include <QTableWidget>

#include "TimePointSelectorDef.h"

namespace TC::SingleTimeSelector {
    class TCFListWidget : public QTableWidget {
        Q_OBJECT
	public:
        explicit TCFListWidget(QWidget* parent = nullptr);
        ~TCFListWidget() override;

        /**
         * @param tcf           TCF file name.
         * @param timepoints    Number of time points each modality.
         */
        auto AddTCF(const QString& tcfId, const QString& displayName, QMap<TimePointSelector::Modality, int> timepoints) -> void;
        auto SetCurrentTCF(const QString& tcfId) -> void;

    signals:
        void sigCurrentItemChanged(const QString& tcfId);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
