﻿#include <QVBoxLayout>
#include <QSplitter>
#include <QCheckBox>
#include <QVector>

#include "SingleTimeSettingControl.h"
#include "TCFListWidget.h"
#include "TimePointTableWidget.h"
#include "SingleTimeSettingDialog.h"
#include "PointSettingWidget.h"
#include "ui_SingleTimeSettingDialog.h"

namespace TC::SingleTimeSelector {
    struct SingleTimeSelector::TimePointSettingDialog::Impl {
        TimePointSettingControl control;

        Ui::TimePointSettingDialog ui;
        std::unique_ptr<TCFListWidget> tcfList{nullptr};
        std::unique_ptr<TimePointTableWidget> timepointTable{nullptr};
        std::unique_ptr<PointSettingWidget> settingWidget{nullptr};

        QCheckBox* tableTypeCheckbox{nullptr};

        auto InitUI() -> void;
        auto SetCurrentTCF(const QString& tcfId) -> void;
        auto FiltersTimepoints(const QString& tcfId, const QList<TimePointSelector::Modality>& modalities) -> QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>;
    };

    auto TimePointSettingDialog::Impl::InitUI() -> void {
        tcfList = std::make_unique<TCFListWidget>();
        tableTypeCheckbox = new QCheckBox(tr("Show all time points"));
        timepointTable = std::make_unique<TimePointTableWidget>();

        auto tableLayout = new QVBoxLayout;
		tableLayout->addWidget(tableTypeCheckbox, 0);
        tableLayout->addWidget(timepointTable.get(), 1);

        settingWidget = std::make_unique<PointSettingWidget>();

        auto rightSideLayout = new QVBoxLayout;
        rightSideLayout->addLayout(tableLayout);
        rightSideLayout->addWidget(settingWidget.get());

    	auto rightSideWidget = new QWidget;
        rightSideWidget->setLayout(rightSideLayout);
        
        auto splitter = new QSplitter(ui.centralWidget);
        splitter->addWidget(tcfList.get());
        splitter->addWidget(rightSideWidget);

        auto layout = new QVBoxLayout;
        layout->addWidget(splitter);
        ui.centralWidget->setLayout(layout);

        for (auto i = 0; i< splitter->count(); i++)
	        splitter->setCollapsible(i, false);
    }

    auto TimePointSettingDialog::Impl::SetCurrentTCF(const QString& tcfId) -> void {
	    control.SetCurrentTCFId(tcfId);

        const auto entireTimepoints = control.GetTimePoints(tcfId);
        auto modalityList = control.GetModalities(tcfId);
        const auto filteredTimepoints = FiltersTimepoints(tcfId, modalityList);

        timepointTable->SetTimePoints(tableTypeCheckbox->isChecked() ? entireTimepoints : filteredTimepoints);

        settingWidget->SetTimePoints(entireTimepoints);
    	settingWidget->SetFilteredTimePoints(filteredTimepoints);
        settingWidget->SetModalites(modalityList);

    	const auto timeIndexes = control.GetTimeIndexes(tcfId);
        if (!timeIndexes.isEmpty()) settingWidget->SetTimeIndexes(timeIndexes);
    }

    auto TimePointSettingDialog::Impl::FiltersTimepoints(const QString& tcfId, const QList<TimePointSelector::Modality>& modalities) -> QMap<TimePointSelector::Modality, TimePointSelector::TimePoints> {
        QMap<TimePointSelector::Modality, TimePointSelector::TimePoints> result;

        const auto entireTimes = control.GetTimePoints(tcfId);

        TimePointSelector::TimePoints allTimes;
        for (auto modal : modalities) {
            result.insert(modal, {});

            for (auto time : entireTimes.value(modal))
	            if (!allTimes.contains(time)) allTimes << time;
        }

        TimePointSelector::TimePoints resultTimes;
        for (auto time : allTimes) {
	        bool hasAll = true;

            for (auto modal : modalities) {
	            if (!entireTimes.value(modal).contains(time)) {
		            hasAll = false;
                    break;
	            }
            }

            if (hasAll) resultTimes << time;
        }

        // set filtered times into selected modalities
        for (auto modal : modalities)
            result.insert(modal, resultTimes);
        
        return result;
    }

    TimePointSettingDialog::TimePointSettingDialog(QWidget* parent) : QDialog(parent, Qt::Dialog | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint), d{std::make_unique<Impl>()} {
	    d->ui.setupUi(this);

        d->InitUI();

        connect(d->tcfList.get(), &TCFListWidget::sigCurrentItemChanged, [=](const QString& tcfId) { d->SetCurrentTCF(tcfId); });

        connect(d->tableTypeCheckbox, &QCheckBox::stateChanged, [=](int state) {
            const auto tcfId = d->control.GetCurrentTCFId();

	        if (state == Qt::Checked) {
                const auto timepoints = d->control.GetTimePoints(tcfId);
                d->timepointTable->SetTimePoints(timepoints);
	        } else {
                const auto modalities = d->settingWidget->GetModalities();
		        d->timepointTable->SetTimePoints(d->FiltersTimepoints(tcfId, modalities));
	        }
        });

        connect(d->settingWidget.get(), &PointSettingWidget::sigModalityChanged, [=](QList<TimePointSelector::Modality> modalities) {
            const auto filteredTimepoints = d->FiltersTimepoints(d->control.GetCurrentTCFId(), modalities);
            d->settingWidget->SetFilteredTimePoints(filteredTimepoints);
	        if (!d->tableTypeCheckbox->isChecked()) d->timepointTable->SetTimePoints(filteredTimepoints);
        });

        connect(d->ui.okButton, &QPushButton::clicked, [=]() {
            const auto id = d->control.GetCurrentTCFId();
            const auto [modalities, timeIndexes] = d->settingWidget->GetSelectedTimeIndexes();
            d->control.SetModalities(id, modalities);
            d->control.SetTimeIndexes(id, timeIndexes);

            accept();
        });

        connect(d->ui.applyButton, &QPushButton::clicked, [=]() {
            const auto id = d->control.GetCurrentTCFId();
            const auto [modalities, timeIndexes] = d->settingWidget->GetSelectedTimeIndexes();
            d->control.SetModalities(id, modalities);
            d->control.SetTimeIndexes(id, timeIndexes);
        });

        connect(d->ui.cancelButton, &QPushButton::clicked, [=]() { reject(); });
    }

    TimePointSettingDialog::~TimePointSettingDialog() {
    }

    auto TimePointSettingDialog::SetTCFs(const QStringList& tcfs) -> void {
        QString firstId;

	    for (auto tcf : tcfs) {
            const auto meta = d->control.RegisterTCF(tcf);

            QMap<TimePointSelector::Modality, int> timepoints;

            if (meta->data.data2DMIP.exist) timepoints.insert(TimePointSelector::Modality::Ht2d, meta->data.data2DMIP.timePoints.count());
            if (meta->data.data3D.exist) timepoints.insert(TimePointSelector::Modality::Ht3d, meta->data.data3D.timePoints.count());
            if (meta->data.data2DFLMIP.exist) {
                const auto channels = QVector(meta->data.data2DFLMIP.channelList.begin(), meta->data.data2DFLMIP.channelList.end());
                if (channels.contains(0)) timepoints.insert(TimePointSelector::Modality::Fl2dCh1, meta->data.data2DFLMIP.timePoints[0].count());
                if (channels.contains(1)) timepoints.insert(TimePointSelector::Modality::Fl2dCh2, meta->data.data2DFLMIP.timePoints[1].count());
                if (channels.contains(2)) timepoints.insert(TimePointSelector::Modality::Fl2dCh3, meta->data.data2DFLMIP.timePoints[2].count());
            }
            if (meta->data.data3DFL.exist) {
                const auto channels = QVector(meta->data.data3DFL.channelList.begin(), meta->data.data3DFL.channelList.end());
                if (channels.contains(0)) timepoints.insert(TimePointSelector::Modality::Fl3dCh1, meta->data.data3DFL.timePoints[0].count());
                if (channels.contains(1)) timepoints.insert(TimePointSelector::Modality::Fl3dCh2, meta->data.data3DFL.timePoints[1].count());
                if (channels.contains(2)) timepoints.insert(TimePointSelector::Modality::Fl3dCh3, meta->data.data3DFL.timePoints[2].count());
            }
            if (meta->data.dataBF.exist) timepoints.insert(TimePointSelector::Modality::Bf, meta->data.dataBF.timePoints.count());

		    d->tcfList->AddTCF(meta->common.uniqueID, meta->common.title, timepoints);
            if (firstId.isEmpty()) firstId = meta->common.uniqueID;
	    }

        d->tcfList->SetCurrentTCF(firstId);

        const auto entireTimepoints = d->control.GetTimePoints(firstId);
        const auto modalityList = d->control.GetModalities(firstId);
        const auto filteredTimepoints = d->FiltersTimepoints(firstId, modalityList);

    	d->timepointTable->SetTimePoints(d->tableTypeCheckbox->isChecked() ? entireTimepoints : filteredTimepoints);

        d->settingWidget->SetTimePoints(entireTimepoints);
        d->settingWidget->SetFilteredTimePoints(filteredTimepoints);
        d->settingWidget->SetModalites(modalityList);

        d->control.SetCurrentTCFId(firstId);
    }

    auto TimePointSettingDialog::SetInputConfig(const TimePointSelector::Config &config) -> void {
        d->control.SetConfig(config);
	    d->settingWidget->SetConfig(config);

        const auto tcfId = d->control.GetCurrentTCFId();
        const auto modalityList = d->control.GetModalities(tcfId);
    	const auto entireTimepoints = d->control.GetTimePoints(tcfId);
        const auto filteredTimepoints = d->FiltersTimepoints(tcfId, d->control.GetModalities(tcfId));

    	d->timepointTable->SetTimePoints(d->tableTypeCheckbox->isChecked() ? entireTimepoints : filteredTimepoints);

        d->settingWidget->SetTimePoints(entireTimepoints);
        d->settingWidget->SetFilteredTimePoints(filteredTimepoints);
        d->settingWidget->SetModalites(modalityList);
    }

    auto TimePointSettingDialog::GetMeta(const QString& path) -> IO::TCFMetaReader::Meta::Pointer {
        auto it = QMapIterator(d->control.GetAllTimePointInfo());
        while(it.hasNext()) {
            it.next();

        	auto info = it.value();
            if(info.path == path) {                
                return info.meta;
            }
        }
        return nullptr;
	}

    auto TimePointSettingDialog::GetResult() ->QMap<QString, std::tuple<TimePointSelector::TimeSelectionOutput, QList<double>>> {
        QMap<QString, std::tuple<TimePointSelector::TimeSelectionOutput, QList<double>>> result;  // key: tcf path

        auto it = QMapIterator(d->control.GetAllTimePointInfo());
        while (it.hasNext()) {
	        it.next();

            auto info = it.value();            
            TimePointSelector::TimeSelectionOutput output;
            output.modalties = info.modalities;
            output.times = info.timeIndexes;
            QList<double> realTime;
            for(const auto time : info.timeIndexes) {
                realTime.append(info.meta->data.total_time[time - 1]);
            }

            result.insert(info.path, std::make_tuple(output,realTime));
        }
        return result;
    }

}
