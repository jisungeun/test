﻿#include <QDebug>

#include "TimePointTableWidget.h"

#include <QHeaderView>

namespace TC::SingleTimeSelector {
    struct TimePointTableWidget::Impl {
        TimePointTableWidget* self{nullptr};

        auto InitUI() -> void;
        auto Clear() -> void;
    };

    auto TimePointTableWidget::Impl::InitUI() -> void {
        self->horizontalHeader()->hide();
        self->verticalHeader()->hide();
        self->setEditTriggers(QAbstractItemView::NoEditTriggers);

	    self->setRowCount(7);
        self->setColumnCount(1);

        // set vertical header
        self->setItem(0, 0, new QTableWidgetItem("Times"));
        self->setItem(2, 0, new QTableWidgetItem("HT"));
        self->setItem(3, 0, new QTableWidgetItem("FL Ch1"));
        self->setItem(4, 0, new QTableWidgetItem("FL Ch2"));
        self->setItem(5, 0, new QTableWidgetItem("FL Ch3"));
        self->setItem(6, 0, new QTableWidgetItem("BF"));

        self->setSpan(0, 0, 2, 1);

        for (auto row = 2; row < self->rowCount(); row++)
	        self->setRowHeight(row, 40);
    }

    auto TimePointTableWidget::Impl::Clear() -> void {
	    self->clear();
        InitUI();
    }

    TimePointTableWidget::TimePointTableWidget(QWidget* parent) : QTableWidget(parent), d{std::make_unique<Impl>()} {
        d->self = this;

        d->InitUI();
    }

    TimePointTableWidget::~TimePointTableWidget() {
    }

    auto TimePointTableWidget::SetTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void {
	    d->Clear();

        // add a horizontal header, it has time index row and time row.
        // ---------------------------------------------------------
        // | Times |     0     |     1     |     2     |     3     |    Time index row
        // -       -------------------------------------------------
        // |       | 00:00:00  | 00:00:30  | 00:01:00  | 00:00:30  |    Time row
        // ---------------------------------------------------------

        TimePointSelector::TimePoints times;
        for (auto& modalityTimepoints : timepoints.values())
	        for (auto time : modalityTimepoints)
		        if(!times.contains(time)) times << time;

        std::sort(times.begin(), times.end());

        setColumnCount(columnCount() + times.count());

        auto TimeToText = [](int64_t time) -> QString {
            return QString("%1:%2:%3")
        	.arg(time / 3600)
        	.arg((time % 3600) / 60, 2, 10, QChar('0'))
        	.arg((time % 3600) % 60, 2, 10, QChar('0'));
        };

        for (auto row = 2; row < rowCount(); row++) hideRow(row);

        // add cell items(2D, 3D)
        auto AddCellItem = [table = this](int row, int col, const QString& text) {
	        auto cellItem = table->item(row, col);
            if (cellItem && !cellItem->text().isEmpty()) {
                cellItem->setText(cellItem->text() + "\n" + text);
            } else {
                table->setItem(row, col, new QTableWidgetItem(text));
            }

            if (table->isRowHidden(row)) table->setRowHidden(row, false);
        };

        for (auto i = 0; i < times.count(); i++) {
            const auto time = times.at(i);
            const auto col = i + 1;

	        setItem(0, col, new QTableWidgetItem(QString::number(i + 1)));
            setItem(1, col, new QTableWidgetItem(TimeToText(time)));

            auto it = QMapIterator(timepoints);
            while(it.hasNext()) {
	            it.next();

                if (!it.value().contains(time)) continue;

                switch(it.key()) {
                case TimePointSelector::Modality::Ht2d: AddCellItem(2, col, "2D"); break;
                case TimePointSelector::Modality::Ht3d: AddCellItem(2, col, "3D"); break;
                case TimePointSelector::Modality::Fl2dCh1: AddCellItem(3, col, "2D"); break;
                case TimePointSelector::Modality::Fl3dCh1: AddCellItem(3, col, "3D"); break;
                case TimePointSelector::Modality::Fl2dCh2: AddCellItem(4, col, "2D"); break;
                case TimePointSelector::Modality::Fl3dCh2: AddCellItem(4, col, "3D"); break;
                case TimePointSelector::Modality::Fl2dCh3: AddCellItem(5, col, "2D"); break;
                case TimePointSelector::Modality::Fl3dCh3: AddCellItem(5, col, "3D"); break;
                case TimePointSelector::Modality::Bf: AddCellItem(6, col, "2D"); break;
                }
            }
        }
    }

}
