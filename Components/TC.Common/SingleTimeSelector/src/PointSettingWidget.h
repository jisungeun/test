﻿#pragma once

#include <QWidget>

#include "TimePointSelectorDef.h"

namespace TC::SingleTimeSelector {
    class PointSettingWidget : public QWidget {
        Q_OBJECT
	public:
        explicit PointSettingWidget(QWidget* parent = nullptr);
        ~PointSettingWidget() override;

        auto SetConfig(TimePointSelector::Config config) -> void;
        auto SetModalites(const QList<TimePointSelector::Modality>& modalities) -> void;

        auto SetTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void;
        auto SetFilteredTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void;
        auto SetTimeIndexes(const QList<int>& indexes) -> void;

        auto GetModalities() const -> QList<TimePointSelector::Modality>;
        auto GetSelectedTimeIndexes() const -> std::tuple<QList<TimePointSelector::Modality>, QList<int>>;

    signals:
        void sigModalityChanged(QList<TimePointSelector::Modality> modalities);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
