﻿#include <QDebug>
#include <QComboBox>

#include "PointSettingWidget.h"
#include "ui_PointSettingWidget.h"

namespace TC::SingleTimeSelector {
    struct PointSettingWidget::Impl {
        Ui::PointSettingWidget ui;

        TimePointSelector::Config config;
        TimePointSelector::TimePoints entireTimes;
        TimePointSelector::TimePoints times;

        auto InitUI() -> void;
        auto SetTimePointsUIEnable(bool enable) -> void;
        auto ClearModalities() -> void;
        auto ClearTimePoints() -> void;
        auto CurrentModalities() -> QList<TimePointSelector::Modality>;
        auto CheckModalitiesCondition() -> bool;
        auto GetTimeSteps() -> QList<int>;
        auto GetTimePoints() -> QList<int>;
        auto ShowErrorMessage(const QString& message) -> void;

        auto OnModalityChanged() -> void;
    };

    auto PointSettingWidget::Impl::InitUI() -> void {
    	auto InitFLComboBox = [](QComboBox* comboBox, bool is2D = true) -> void {
            comboBox->clear();
	        comboBox->addItem("Ch1", static_cast<int>(is2D ? TimePointSelector::Modality::Fl2dCh1 : TimePointSelector::Modality::Fl3dCh1));
	        comboBox->addItem("Ch2", static_cast<int>(is2D ? TimePointSelector::Modality::Fl2dCh2 : TimePointSelector::Modality::Fl3dCh2));
	        comboBox->addItem("Ch3", static_cast<int>(is2D ? TimePointSelector::Modality::Fl2dCh3 : TimePointSelector::Modality::Fl3dCh3));
        };

        for (auto combobox : {ui.fl2d1stChComboBox, ui.fl2d2ndChComboBox, ui.fl2d3rdChComboBox})
	        InitFLComboBox(combobox);

        for (auto combobox : {ui.fl3d1stChComboBox, ui.fl3d2ndChComboBox, ui.fl3d3rdChComboBox})
	        InitFLComboBox(combobox, false);

        ui.selectionSpinBox->setRange(1, 9999);
        ui.selectionSpinBox->setValue(1);

        QRegExp positiveIntegerRegex("^[1-9]\\d*$");
		ui.timeStepsLineEdit->setValidator(new QRegExpValidator(positiveIntegerRegex));

        ui.totalTimeStepsLabel->hide();
        ui.startSpinBox->setReadOnly(true);
        ui.endSpinBox->setReadOnly(true);
    }

    auto PointSettingWidget::Impl::SetTimePointsUIEnable(bool enable) -> void {
	    ui.startSpinBox->setEnabled(enable);
        ui.endSpinBox->setEnabled(enable);
        ui.selectionSpinBox->setEnabled(enable);
        ui.applyButton->setEnabled(enable);
        ui.timeStepsLineEdit->setEnabled(enable);

        if (!enable) ui.timeStepsLineEdit->clear();
    }

    auto PointSettingWidget::Impl::ClearModalities() -> void {
    	ui.ht2dCheckBox->setCheckState(Qt::Unchecked);
        ui.ht3dCheckBox->setCheckState(Qt::Unchecked);
    	ui.bfCheckBox->setCheckState(Qt::Unchecked);

        for (auto combobox : {ui.fl2d1stChComboBox, ui.fl2d2ndChComboBox, ui.fl2d3rdChComboBox, ui.fl3d1stChComboBox, ui.fl3d2ndChComboBox, ui.fl3d3rdChComboBox})
	        combobox->setCurrentIndex(-1);
    }

    auto PointSettingWidget::Impl::ClearTimePoints() -> void {
	    ui.startSpinBox->setRange(0, 0);
        ui.startSpinBox->setValue(0);
        ui.endSpinBox->setRange(0, 0);
        ui.endSpinBox->setValue(0);
        ui.selectionSpinBox->setValue(1);

        ui.timeStepsLineEdit->clear();
        ui.timePointsLineEdit->clear();
    }

    auto PointSettingWidget::Impl::CurrentModalities() -> QList<TimePointSelector::Modality> {
        QList<TimePointSelector::Modality> modalities;

        if (ui.ht2dCheckBox->isEnabled() && ui.ht2dCheckBox->checkState() == Qt::Checked) modalities << TimePointSelector::Modality::Ht2d;
        if (ui.ht3dCheckBox->isEnabled() && ui.ht3dCheckBox->checkState() == Qt::Checked) modalities << TimePointSelector::Modality::Ht3d;

        for (auto chCombobox : {ui.fl2d1stChComboBox, ui.fl2d2ndChComboBox, ui.fl2d3rdChComboBox}) {
            if (chCombobox->isEnabled()) {
	            auto ch = static_cast<TimePointSelector::Modality>(chCombobox->currentData().toInt());
                if (!modalities.contains(ch)) modalities << ch;
            }
        }

        for (auto chCombobox : {ui.fl3d1stChComboBox, ui.fl3d2ndChComboBox, ui.fl3d3rdChComboBox}) {
            if (chCombobox->isEnabled()) {
	            auto ch = static_cast<TimePointSelector::Modality>(chCombobox->currentData().toInt());
                if (!modalities.contains(ch)) modalities << ch;
            }
        }

        if (ui.bfCheckBox->isEnabled() && ui.bfCheckBox->checkState() == Qt::Checked) modalities << TimePointSelector::Modality::Bf;

        return modalities;
    }

    auto PointSettingWidget::Impl::CheckModalitiesCondition() -> bool {
	    if (config.modalities & TimePointSelector::Modality::Ht2d && !ui.ht2dCheckBox->isChecked()) return false;
	    if (config.modalities & TimePointSelector::Modality::Ht3d && !ui.ht3dCheckBox->isChecked()) return false;

        if (config.modalities & TimePointSelector::Modality::Fl2d) {
            QList<int> selectedChannels;
            for (auto chCombobox : {ui.fl2d1stChComboBox, ui.fl2d2ndChComboBox, ui.fl2d3rdChComboBox}) {
	            if (chCombobox->isEnabled()) {
		            auto ch = chCombobox->currentData().toInt();
	                if (!selectedChannels.contains(ch)) selectedChannels << ch;
	            }
            }

            if (selectedChannels.count() != config.fl2dCount) return false;
        }

        if (config.modalities & TimePointSelector::Modality::Fl3d) {
	        QList<int> selectedChannels;
            for (auto chCombobox : {ui.fl3d1stChComboBox, ui.fl3d2ndChComboBox, ui.fl3d3rdChComboBox}) {
	            if (chCombobox->isEnabled()) {
		            auto ch = chCombobox->currentData().toInt();
	                if (!selectedChannels.contains(ch)) selectedChannels << ch;
	            }
            }

            if (selectedChannels.count() != config.fl3dCount) return false;
        }

        if (config.modalities & TimePointSelector::Modality::Bf && !ui.bfCheckBox->isChecked()) return false;

        return true;
    }

    auto PointSettingWidget::Impl::GetTimeSteps() -> QList<int> {
        if(ui.timeStepsLineEdit->text().isEmpty()) return {};

		auto split = ui.timeStepsLineEdit->text().split(",");
		auto prev = -1;
		QList<int> timeSteps;
		auto maximum = ui.endSpinBox->maximum();
				
		for (const auto& timestr : split) {
			if (timestr.contains("-")) {//range
				auto begin = timestr.split("-")[0].toInt();
				auto end = timestr.split("-")[1].toInt();
				if (begin <= prev) {
                    ShowErrorMessage(tr("Wrong time_step order\n (put numbers in ascending order)"));
                    return {};
				}

				if (begin > end) {
					ShowErrorMessage(tr("Wrong sub-time_step range\n (put numbers in ascending order)"));
                    return {};
				}

				if (end > maximum) {
					ShowErrorMessage(QString(tr("Out of range\n (put numbers less than image time_steps %1)").arg(maximum)));
                    return {};
				}

				for (auto i = begin; i <= end; i++) {					
					timeSteps.push_back(i);
				}

				prev = end;
			} else {
				auto timePoint = timestr.toInt();
				if (timePoint <= prev) {
					ShowErrorMessage(tr("Wrong time_step order\n (put numbers in ascending order)"));
                    return {};
				}
				if (timePoint > maximum) {
					ShowErrorMessage(QString(tr("Out of range\n (put numbers less than image time_steps %1)").arg(maximum)));
                    return {};
				}

			    timeSteps.push_back(timePoint);
			    prev = timePoint;				
			}
		}

        return timeSteps;
    }

    auto PointSettingWidget::Impl::GetTimePoints() -> QList<int> {
        const auto timeSteps = GetTimeSteps();
        if (timeSteps.isEmpty()) return {};

        QList<int> timePoints;
        for (auto step : timeSteps)
            timePoints << entireTimes.indexOf(times.at(step-1)) + 1;

        return timePoints;
    }

    auto PointSettingWidget::Impl::ShowErrorMessage(const QString& message) -> void {
	    ui.warningMessageLabel->setText(message);
        ui.warningMessageLabel->show();
    }

    auto PointSettingWidget::Impl::OnModalityChanged() -> void {
	    const auto isValid = CheckModalitiesCondition();
        ShowErrorMessage(isValid ? "" : tr("The modality selection is not sufficient."));
        SetTimePointsUIEnable(isValid);
    }

    PointSettingWidget::PointSettingWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

    	d->InitUI();

        connect(d->ui.applyButton, &QPushButton::clicked, [=]() {	        
            d->ui.timeStepsLineEdit->setText(QString::number(d->ui.selectionSpinBox->value()));
        });

        connect(d->ui.ht2dCheckBox, &QCheckBox::stateChanged, [=](int state) {
            d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });
        connect(d->ui.ht3dCheckBox, &QCheckBox::stateChanged, [=](int state) {
            d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });
        connect(d->ui.bfCheckBox, &QCheckBox::stateChanged, [=](int state) {
        	d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl2d1stChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl2d2ndChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl2d3rdChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl3d1stChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl3d2ndChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.fl3d3rdChComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {
	        d->OnModalityChanged();
            emit sigModalityChanged(d->CurrentModalities());
        });

        connect(d->ui.timeStepsLineEdit, &QLineEdit::textChanged, [=](const QString& text) {
            d->ui.warningMessageLabel->clear();

            QString timePoints;
            const auto timeSteps = d->GetTimeSteps();
            if (timeSteps.isEmpty()) {
	            d->ui.timePointsLineEdit->clear();
                return;
            }

            for (auto step : timeSteps) {
                if (!timePoints.isEmpty()) timePoints += ",";
	            timePoints += QString::number(d->entireTimes.indexOf(d->times.at(step-1)) + 1);
            }

            d->ui.totalTimeStepsLabel->setText(QString("Total: %1").arg(timeSteps.count()));
        	d->ui.timePointsLineEdit->setText(timePoints);
        });
    }

    PointSettingWidget::~PointSettingWidget() {
    }

    auto PointSettingWidget::SetConfig(TimePointSelector::Config config) -> void {
        auto children = findChildren<QWidget*>();
        for (auto child : children) child->blockSignals(true);

        d->ClearModalities();

        d->config = config;

        // set modality check and enable states
	    auto contained = config.modalities & TimePointSelector::Modality::Ht2d;
        d->ui.ht2dCheckBox->setCheckState(contained ? Qt::Checked : Qt::Unchecked);
        d->ui.ht2dCheckBox->setEnabled(contained);

        contained = config.modalities & TimePointSelector::Modality::Ht3d;
        d->ui.ht3dCheckBox->setCheckState(contained ? Qt::Checked : Qt::Unchecked);
        d->ui.ht3dCheckBox->setEnabled(contained);

        const auto fl2dComboboxes = QList({d->ui.fl2d1stChComboBox, d->ui.fl2d2ndChComboBox, d->ui.fl2d3rdChComboBox});
        for (auto i = 0; i < fl2dComboboxes.count(); i++) {
            auto combobox = fl2dComboboxes.at(i);
	        if (i < config.fl2dCount) {
		        combobox->setEnabled(true);
                combobox->setCurrentIndex(i);
	        } else {
		        combobox->setEnabled(false);
	        }
        }

        const auto fl3dComboboxes = QList({d->ui.fl3d1stChComboBox, d->ui.fl3d2ndChComboBox, d->ui.fl3d3rdChComboBox});
        for (auto i = 0; i < fl3dComboboxes.count(); i++) {
            auto combobox = fl3dComboboxes.at(i);
	        if (i < config.fl3dCount) {
		        combobox->setEnabled(true);
                combobox->setCurrentIndex(i);
	        } else {
		        combobox->setEnabled(false);
	        }
        }

        contained = config.modalities & TimePointSelector::Modality::Bf;
        d->ui.bfCheckBox->setCheckState(contained ? Qt::Checked : Qt::Unchecked);
        d->ui.bfCheckBox->setEnabled(contained);

        for (auto child : children) child->blockSignals(false);
    }

    auto PointSettingWidget::SetModalites(const QList<TimePointSelector::Modality>& modalities) -> void {
        d->ClearModalities();

        const auto fl2dComboboxes = QList({d->ui.fl2d1stChComboBox, d->ui.fl2d2ndChComboBox, d->ui.fl2d3rdChComboBox});
        const auto fl3dComboboxes = QList({d->ui.fl3d1stChComboBox, d->ui.fl3d2ndChComboBox, d->ui.fl3d3rdChComboBox});

        int fl2dComboboxIndex = 0;
        int fl3dComboboxIndex = 0;

        for (auto modality : modalities) {
	        if (modality == TimePointSelector::Modality::Ht2d)
                d->ui.ht2dCheckBox->setCheckState(Qt::Checked);
	        else if (modality == TimePointSelector::Modality::Ht3d)
                d->ui.ht3dCheckBox->setCheckState(Qt::Checked);
	        else if (modality == TimePointSelector::Modality::Bf)
                d->ui.bfCheckBox->setCheckState(Qt::Checked);
            else if (modality == TimePointSelector::Modality::Fl2dCh1) {
                fl2dComboboxes.at(fl2dComboboxIndex)->setCurrentIndex(0);
                fl2dComboboxIndex++;
            } else if (modality == TimePointSelector::Modality::Fl2dCh2) {
                fl2dComboboxes.at(fl2dComboboxIndex)->setCurrentIndex(1);
                fl2dComboboxIndex++;
			} else if (modality == TimePointSelector::Modality::Fl2dCh3) {
                fl2dComboboxes.at(fl2dComboboxIndex)->setCurrentIndex(2);
                fl2dComboboxIndex++;
			} else if (modality == TimePointSelector::Modality::Fl3dCh1) {
                fl3dComboboxes.at(fl3dComboboxIndex)->setCurrentIndex(0);
                fl3dComboboxIndex++;
            } else if (modality == TimePointSelector::Modality::Fl3dCh2) {
                fl3dComboboxes.at(fl3dComboboxIndex)->setCurrentIndex(1);
                fl3dComboboxIndex++;
			} else if (modality == TimePointSelector::Modality::Fl3dCh3) {
                fl3dComboboxes.at(fl3dComboboxIndex)->setCurrentIndex(2);
                fl3dComboboxIndex++;
			}
        }
    }

    auto PointSettingWidget::SetTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void {
	    d->entireTimes.clear();

        auto it = QMapIterator(timepoints);
        while (it.hasNext()) {
	        it.next();
            for (auto time : it.value())
	            if (!d->entireTimes.contains(time)) d->entireTimes << time;
        }

        std::sort(d->entireTimes.begin(), d->entireTimes.end());
    }

    auto PointSettingWidget::SetFilteredTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void {
        d->ClearTimePoints();
    	d->times.clear();

        auto it = QMapIterator(timepoints);
        while (it.hasNext()) {
	        it.next();
            for (auto time : it.value())
	            if (!d->times.contains(time)) d->times << time;
        }
        
        std::sort(d->times.begin(), d->times.end());

        const auto lastTimeIndex = d->times.count();

        d->ui.startSpinBox->setRange(1, lastTimeIndex);
        d->ui.startSpinBox->setValue(1);

        d->ui.endSpinBox->setRange(1, lastTimeIndex);
        d->ui.endSpinBox->setValue(lastTimeIndex);

        d->ui.selectionSpinBox->setRange(1, lastTimeIndex);
    }

    auto PointSettingWidget::SetTimeIndexes(const QList<int>& indexes) -> void {
        QString steps;
    	QString points;
    	for (auto index : indexes) {
    		if (!points.isEmpty()) points += ",";
            points += QString::number(index);

    		auto time = d->entireTimes.at(index-1);
            if (!steps.isEmpty()) steps += ",";
    		steps += QString::number(d->times.indexOf(time) + 1);
    	}

        QSignalBlocker timeStepBlocker(d->ui.timeStepsLineEdit);
        QSignalBlocker timePointBlocker(d->ui.timePointsLineEdit);
        d->ui.timeStepsLineEdit->setText(steps);
	    d->ui.timePointsLineEdit->setText(points);

        d->ui.warningMessageLabel->clear();
    }

    auto PointSettingWidget::GetModalities() const -> QList<TimePointSelector::Modality> {
	    return d->CurrentModalities();
    }

    auto PointSettingWidget::GetSelectedTimeIndexes() const -> std::tuple<QList<TimePointSelector::Modality>, QList<int>> {
        if(d->ui.timeStepsLineEdit->text().isEmpty()) return {};

        QList<TimePointSelector::Modality> selectedModality;

    	if (d->ui.ht2dCheckBox->isEnabled() && d->ui.ht2dCheckBox->isChecked()) selectedModality << TimePointSelector::Modality::Ht2d;
        if (d->ui.ht3dCheckBox->isEnabled() && d->ui.ht3dCheckBox->isChecked()) selectedModality << TimePointSelector::Modality::Ht3d;

        for (auto combobox : {d->ui.fl2d1stChComboBox, d->ui.fl2d2ndChComboBox, d->ui.fl2d3rdChComboBox})
        if (combobox->isEnabled()) {
            auto modality = static_cast<TimePointSelector::Modality>(combobox->currentData().toInt());
	        if (!selectedModality.contains(modality)) selectedModality << modality;
        }

        for (auto combobox : {d->ui.fl3d1stChComboBox, d->ui.fl3d2ndChComboBox, d->ui.fl3d3rdChComboBox})
        if (combobox->isEnabled()) {
            auto modality = static_cast<TimePointSelector::Modality>(combobox->currentData().toInt());
	        if (!selectedModality.contains(modality)) selectedModality << modality;
        }

    	if (d->ui.bfCheckBox->isEnabled() && d->ui.bfCheckBox->isChecked()) selectedModality << TimePointSelector::Modality::Bf;

        return {selectedModality, d->GetTimePoints()};
    }

}
