﻿#include "SingleTimeSettingControl.h"

#include <QMap>

namespace TC::SingleTimeSelector {    
    struct TimePointSettingControl::Impl {
        QMap<QString, TCFInfo> tcfInfos;
        QString currentTCFId;
        TimePointSelector::Config config;
    };

    TimePointSettingControl::TimePointSettingControl() : d{std::make_unique<Impl>()} {}

	TimePointSettingControl::~TimePointSettingControl() {}

    auto TimePointSettingControl::RegisterTCF(const QString& tcf) const -> IO::TCFMetaReader::Meta::Pointer {
        TCFInfo info;

        IO::TCFMetaReader reader;
        info.meta = reader.Read(tcf);
        info.path = tcf;

        d->tcfInfos.insert(info.meta->common.uniqueID, info);

    	return info.meta;
    }

    auto TimePointSettingControl::GetTimePoints(const QString& tcfId) const -> QMap<TimePointSelector::Modality, TimePointSelector::TimePoints> {
        if (!d->tcfInfos.contains(tcfId)) return {};

        const auto meta = d->tcfInfos[tcfId].meta;

        QMap<TimePointSelector::Modality, TimePointSelector::TimePoints> timepoints;
        if (meta->data.data2DMIP.exist) timepoints.insert(TimePointSelector::Modality::Ht2d, meta->data.data2DMIP.timePoints);
        if (meta->data.data3D.exist) timepoints.insert(TimePointSelector::Modality::Ht3d, meta->data.data3D.timePoints);
        if (meta->data.data2DFLMIP.exist) {
            const auto channels = QVector(meta->data.data2DFLMIP.channelList.begin(), meta->data.data2DFLMIP.channelList.end());
            if (channels.contains(0)) timepoints.insert(TimePointSelector::Modality::Fl2dCh1, meta->data.data2DFLMIP.timePoints[0]);
            if (channels.contains(1)) timepoints.insert(TimePointSelector::Modality::Fl2dCh2, meta->data.data2DFLMIP.timePoints[1]);
            if (channels.contains(2)) timepoints.insert(TimePointSelector::Modality::Fl2dCh3, meta->data.data2DFLMIP.timePoints[2]);
        }
        if (meta->data.data3DFL.exist) {
	        const auto channels = QVector(meta->data.data3DFL.channelList.begin(), meta->data.data3DFL.channelList.end());
            if (channels.contains(0)) timepoints.insert(TimePointSelector::Modality::Fl3dCh1, meta->data.data3DFL.timePoints[0]);
            if (channels.contains(1)) timepoints.insert(TimePointSelector::Modality::Fl3dCh2, meta->data.data3DFL.timePoints[1]);
            if (channels.contains(2)) timepoints.insert(TimePointSelector::Modality::Fl3dCh3, meta->data.data3DFL.timePoints[2]);
        }
        if (meta->data.dataBF.exist) timepoints.insert(TimePointSelector::Modality::Bf, meta->data.dataBF.timePoints);

        return timepoints;
    }

    auto TimePointSettingControl::SetCurrentTCFId(const QString& tcfId) const -> void {
	    d->currentTCFId = tcfId;
    }

    auto TimePointSettingControl::GetCurrentTCFId() const -> QString {
	    return d->currentTCFId;
    }

    auto TimePointSettingControl::SetModalities(const QString& tcfId, const QList<TimePointSelector::Modality>& modalities) -> void {
        if (!d->tcfInfos.contains(tcfId)) return;

        auto info = d->tcfInfos[tcfId];
        info.modalities = modalities;

        d->tcfInfos[tcfId] = info;
    }

    auto TimePointSettingControl::GetModalities(const QString& tcfId) const -> QList<TimePointSelector::Modality> {
        if (!d->tcfInfos.contains(tcfId)) return {};
        return d->tcfInfos[tcfId].modalities;
    }

    auto TimePointSettingControl::SetTimeIndexes(const QString& tcfId, const QList<int>& indexes) const -> void {
        if (!d->tcfInfos.contains(tcfId)) return;

        auto info = d->tcfInfos[tcfId];
        info.timeIndexes = indexes;

        d->tcfInfos[tcfId] = info;
    }

    auto TimePointSettingControl::GetTimeIndexes(const QString& tcfId) const -> QList<int> {
    	if (!d->tcfInfos.contains(tcfId)) return {};
        return d->tcfInfos[tcfId].timeIndexes;
    }

    auto TimePointSettingControl::GetAllTimePointInfo() const -> QMap<QString, TCFInfo> {
        return d->tcfInfos;
    }

    auto TimePointSettingControl::SetConfig(const TimePointSelector::Config& config) -> void {
	    d->config = config;

        // init modalities
        QList<TimePointSelector::Modality> modalities;
        if (config.modalities & TimePointSelector::Modality::Ht2d) modalities << TimePointSelector::Modality::Ht2d;
        if (config.modalities & TimePointSelector::Modality::Ht3d) modalities << TimePointSelector::Modality::Ht3d;
        if (config.modalities & TimePointSelector::Modality::Bf) modalities << TimePointSelector::Modality::Bf;

        if (config.modalities & TimePointSelector::Modality::Fl2d) {
            const auto fl2dChannels = QList({ TimePointSelector::Modality::Fl2dCh1, TimePointSelector::Modality::Fl2dCh2, TimePointSelector::Modality::Fl2dCh3});
            for (auto i = 0; i < fl2dChannels.count(); i++) {
	            if (i < config.fl2dCount)
		            modalities << fl2dChannels.at(i);
	        }
        }

        if (config.modalities & TimePointSelector::Modality::Fl3d) {
            const auto fl3dChannels = QList({ TimePointSelector::Modality::Fl3dCh1, TimePointSelector::Modality::Fl3dCh2, TimePointSelector::Modality::Fl3dCh3});
            for (auto i = 0; i < fl3dChannels.count(); i++) {
	            if (i < config.fl3dCount)
		            modalities << fl3dChannels.at(i);
	        }
        }

        auto it = QMapIterator(d->tcfInfos);
        while (it.hasNext()) {
	        it.next();

        	auto info = it.value();
            info.modalities = modalities;

            d->tcfInfos[it.key()] = info;
        }
    }

    auto TimePointSettingControl::GetConfig() const -> TimePointSelector::Config {
		return d->config;   
    }

}
