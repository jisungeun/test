﻿#pragma once

#include <QTableWidget>

#include "TimePointSelectorDef.h"

namespace TC::SingleTimeSelector {
    class TimePointTableWidget : public QTableWidget {
        Q_OBJECT
	public:
        explicit TimePointTableWidget(QWidget* parent = nullptr);
        ~TimePointTableWidget() override;

        auto SetTimePoints(const QMap<TimePointSelector::Modality, TimePointSelector::TimePoints>& timepoints) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
