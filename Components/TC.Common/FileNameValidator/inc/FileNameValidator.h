#pragma once

#include <enum.h>

#include <QCoreApplication>
#include <QString>

#include "TCFileNameValidatorExport.h"

namespace TC {
    BETTER_ENUM(FileNameValid, int32_t,
        NoError,
        EmptyStringError,
        MinLengthError,
        MaxLenghtOverError,
        WhitespaceAtTheEndError,
        InvalidCharacterContainedError,
        ReservedNameError,
        UnknownError
    );

    class TCFileNameValidator_API FileNameValidator {
        Q_DECLARE_TR_FUNCTIONS(FileNameValidator)

    public:
        FileNameValidator();
        ~FileNameValidator();

        auto SetMinLength(int32_t length) -> void;
        auto GetMinLength() const -> int32_t;
        auto SetMaxLength(int32_t length) -> void;
        auto GetMaxLength() const -> int32_t;

        auto AddInavlidCharacter(const QChar& invalidChar) -> void;
        auto GetInvalidCharacters() const -> QString;

        auto IsValid(const QString& path) -> bool;

        auto GetError() const -> FileNameValid;
        auto GetErrorString() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

