#include "FileNameValidator.h"

#include <QRegularExpression>

namespace TC {
    struct FileNameValidator::Impl {
        int32_t minLength{ 0 };
        int32_t maxLength{ 0 };
        QString invalidCharacters{"<>:\"\\\\/|?*"}; // \\\\ : backslash(\)
        FileNameValid error{ FileNameValid::NoError };
    };

    FileNameValidator::FileNameValidator() : d{ new Impl } {
        
    }

    FileNameValidator::~FileNameValidator() {
        
    }

    auto FileNameValidator::SetMinLength(int32_t length) -> void {
        d->minLength = length;
    }

    auto FileNameValidator::GetMinLength() const -> int32_t {
        return d->minLength;
    }

    auto FileNameValidator::SetMaxLength(int32_t length) -> void {
        d->maxLength = length;
    }

    auto FileNameValidator::GetMaxLength() const -> int32_t {
        return d->maxLength;
    }

    auto FileNameValidator::AddInavlidCharacter(const QChar& invalidChar) -> void {
        if(!d->invalidCharacters.contains(invalidChar)) {
            if(invalidChar == '[' || invalidChar == ']') {
                d->invalidCharacters.append(QString("\\%1").arg(invalidChar));
            }
            else {
                d->invalidCharacters.append(invalidChar);
            }
        }
    }

    auto FileNameValidator::GetInvalidCharacters() const -> QString {
        return d->invalidCharacters;
    }

    auto FileNameValidator::IsValid(const QString& path) -> bool {
        // 빈 문자 확인
        if(path.isEmpty()) {
            d->error = FileNameValid::EmptyStringError;
            return false;
        }

        // 최소 길이 확인
        if(d->minLength > 0 && path.length() < d->minLength) {
            d->error = FileNameValid::MinLengthError;
            return false;
        }

        // 최대 길이 확인
        if (d->maxLength > 0 && path.length() > d->maxLength) {
            d->error = FileNameValid::MaxLenghtOverError;
            return false;
        }

        // 공백문자로 끝나는지 확인
        auto match = QRegularExpression("\\s$").match(path);
        if (match.hasMatch()) {
            d->error = FileNameValid::WhitespaceAtTheEndError;
            return false;
        }

        // 유효하지 않은 문자가 포함되었는지 확인
        match = QRegularExpression(QString("[%1]").arg(d->invalidCharacters)).match(path);
        if (match.hasMatch()) {
            d->error = FileNameValid::InvalidCharacterContainedError;
            return false;
        }

        match = QRegularExpression("^CON$|PRN$|AUX$|NUL$|COM[1-9]$|LPT[1-9]$", QRegularExpression::CaseInsensitiveOption).match(path);
        if (match.hasMatch()) {
            d->error = FileNameValid::ReservedNameError;
            return false;
        }

        d->error = FileNameValid::NoError;
        return true;
    }

    auto FileNameValidator::GetError() const -> FileNameValid {
        return d->error;
    }

    auto FileNameValidator::GetErrorString() const -> QString {
        QString errorString;

        switch (d->error) {
        case FileNameValid::EmptyStringError:
            errorString = tr("Empty string is not allowed.");
            break;
        case FileNameValid::MinLengthError:
            errorString = tr("It is shorter than the minimum length of %1.").arg(d->minLength);
            break;
        case FileNameValid::MaxLenghtOverError:
            errorString = tr("It exceeds the maximum length of %1.").arg(d->maxLength);
            break;
        case FileNameValid::InvalidCharacterContainedError:
            errorString = tr("It contains invalid characters.(%1).").arg(d->invalidCharacters);
            break;
        case FileNameValid::WhitespaceAtTheEndError:
            errorString = tr("It ends in whitespace.");
            break;
        case FileNameValid::ReservedNameError:
            errorString = tr("It's system reserved name.");
            break;
        case FileNameValid::UnknownError:
            errorString = tr("An unknown error occurred.");
            break;
        }

        return errorString;
    }

}