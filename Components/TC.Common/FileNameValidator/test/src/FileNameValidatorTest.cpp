#define CATCH_CONFIG_MAIN

#include <QDebug>
#include <catch2/catch.hpp>

#include <FileNameValidator.h>

using namespace TC;
namespace FileNameValidatorTest {
    TEST_CASE("File name validator test") {
        FileNameValidator validator;
        
        SECTION("Normal file name") {
            CHECK(validator.IsValid("normal_file_name"));
        }

        SECTION("Over maxiumum length") {
            validator.SetMaxLength(10);
            CHECK(!validator.IsValid("longfilename"));
            CHECK(validator.GetError() == +FileNameValid::MaxLenghtOverError);
        }

        SECTION("End of whitespace") {
            CHECK(!validator.IsValid("endofwhitespace "));
            CHECK(validator.GetError() == +FileNameValid::WhitespaceAtTheEndError);

            CHECK(!validator.IsValid("endofwhitespace\t"));
            CHECK(validator.GetError() == +FileNameValid::WhitespaceAtTheEndError);
        }

        SECTION("Contains invalid characters") {
            const QString invalidCharacters = "<>:\"/\\|?*";
            
            for (auto i = 0; i < invalidCharacters.length(); ++i) {
                auto name = QString("invalid%1Name").arg(invalidCharacters.at(i));
                CAPTURE(name.toStdString());
                CHECK(!validator.IsValid(name));
                CHECK(validator.GetError() == +FileNameValid::InvalidCharacterContainedError);
            }
        }

        SECTION("Equals to system reserved name") {
            const QStringList reservedNames = {
                "CON", "PRN", "AUX", "NUL",
                "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
                "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
            };
            
            for (auto name : reservedNames) {
                CAPTURE(name.toStdString());
                CHECK(!validator.IsValid(name));
                CHECK(validator.GetError() == +FileNameValid::ReservedNameError);
            }
        }

        SECTION("Starts with reserved name but OK") {
            const QStringList names = {
                "CONtrol", "control", "condition",
                "PRN0", "PRN1", "PRN2",
                "AUX1", "AUX2", "aux3",
                "COM10", "COM100", "LTP10", "LPT100"
            };

            for(auto name : names) {
                CAPTURE(name.toStdString());
                CHECK(validator.IsValid(name));
                CHECK(validator.GetError() == +FileNameValid::NoError);
            }
        }

        SECTION("Add invalid char") {
            FileNameValidator v;
            v.SetMinLength(2);
            v.SetMaxLength(8);

            QStringList invalidNames = {
                ".asdf", "asdf.", "as...df", "a", "asdfasdfasdf", "<asdf", ">asdf", "as:df", "as\"df", "as\\df",
                "asdf|", "asdf?", "asdf ", "", " ", "  ", "   ", "          "
            };


            for(const auto& name : invalidNames) {
                if(name.contains('.')) {
                    CHECK(v.IsValid(name) == true);
                }
                else{
                    CHECK(v.IsValid(name) == false);
                }
                qDebug() << QString("name[%1] result:%2 reason:%3")
                .arg(name,32,'_')
                .arg(v.IsValid(name))
                .arg(v.GetErrorString());
            }

            v.AddInavlidCharacter('.');
            QStringList invalidNames2 = {
                ".asdf", "asdf.", "as...df", "a", "asdfasdfasdf", "<asdf", ">asdf", "as:df", "as\"df", "as\\df",
                "asdf|", "asdf?", "asdf ", "", " ", "  ", "   ", "          ",
                "as df"
            };

            for(const auto& name : invalidNames2) {
                if(name == "as df") {
                CHECK(v.IsValid(name) == true);
                    
                }
                else {
                    CHECK(v.IsValid(name) == false);
                }
                qDebug() << QString("name[%1] result:%2 reason:%3")
                .arg(name,32,'_')
                .arg(v.IsValid(name))
                .arg(v.GetErrorString());
            }

            v.AddInavlidCharacter(' ');
            QStringList invalidNames3 = {
                "as.df", "as df", " asdf", "asdf ", "asdf\'", "asdf'", "as\'df", "as'df"
            };

            for(const auto& name : invalidNames3) {
                if(name.contains('\'')) {
                    CHECK(v.IsValid(name) == true);
                }
                else {
                    CHECK(v.IsValid(name) == false);
                }
                qDebug() << QString("name[%1] result:%2 reason:%3")
                .arg(name,32,'_')
                .arg(v.IsValid(name))
                .arg(v.GetErrorString());
            }


            v.AddInavlidCharacter('\'');
            QStringList invalidNames4 = {
                "as.df", "as df", " asdf", "asdf ", "asdf\'", "asdf'", "as\'df", "as'df"
            };

            for(const auto& name : invalidNames4) {
                CHECK(v.IsValid(name) == false);
                qDebug() << QString("name[%1] result:%2 reason:%3")
                .arg(name,32,'_')
                .arg(v.IsValid(name))
                .arg(v.GetErrorString());
            }

            v.AddInavlidCharacter(']');
            v.AddInavlidCharacter('[');
            v.AddInavlidCharacter('^');
            QStringList invalidNames5 = {
                "[asdf", "as[df", "a[sdf]", "[asdf]", "as[]df", "as^^df"
            };

            for(const auto& name : invalidNames5) {
                CHECK(v.IsValid(name) == false);
                qDebug() << QString("name[%1] result:%2 reason:%3")
                .arg(name,16,'_')
                .arg(v.IsValid(name))
                .arg(v.GetErrorString());
            }
        }
        SECTION("[ ] test") {
            FileNameValidator v;
            v.AddInavlidCharacter(']');

            QString str = "asdf]";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "]asdf";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "asdf[";
            v.AddInavlidCharacter('[');
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "[asdf";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "[asdf]";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();
        }

        SECTION("[ ] test2") {
            FileNameValidator v;
            
            v.AddInavlidCharacter('[');

            QString str = "asdf[";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "[asdf";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            v.AddInavlidCharacter(']');
            str = "asdf]";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "]asdf\\";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();

            str = "[asdf]";
            CHECK(v.IsValid(str) == false);
            qDebug() << v.GetErrorString();
        }
    }
}
