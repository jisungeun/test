#include <vector>
#include <opencv2/opencv.hpp>

#include "TCBestSliceFinder.h"

namespace TC::Components::Algorithm {
    struct TCBestSliceFinder::Impl {
        Kernel kernel;
        Aggregator aggregator;
        Pivot pivot;

        static auto GetCvF64Slice(uint16_t const* arr, size_t sizeX, size_t sizeY)->cv::Mat;
        auto GetKernelResult(cv::Mat mat) const->cv::Mat;
        auto AggregateSlice(cv::Mat mat) const -> double;
    };

    TCBestSliceFinder::TCBestSliceFinder(Kernel kernel, Aggregator aggregator, Pivot pivot) : d{ std::make_unique<Impl>() } {
        d->kernel = kernel;
        d->aggregator = aggregator;
        d->pivot = pivot;
    }

    auto TCBestSliceFinder::Impl::GetCvF64Slice(uint16_t const* arr, size_t sizeX, size_t sizeY) -> cv::Mat {
        cv::Mat ret(static_cast<int>(sizeX), static_cast<int>(sizeY), CV_64F);
        const auto matData = ret.ptr<double>(0);
        for (size_t i = 0; i < sizeX * sizeY; ++i) {
            matData[i] = static_cast<double>(arr[i]) / 1e+4;
        }
        return ret;
    }

    auto TCBestSliceFinder::Impl::GetKernelResult(cv::Mat mat) const -> cv::Mat {
        cv::Mat dx, dy, ret;
        switch (kernel) {
        case Kernel::SobelXYNorm:
            cv::Sobel(mat, dx, CV_64F, 1, 0, 3, 1, 0, cv::BORDER_REPLICATE);
            cv::Sobel(mat, dy, CV_64F, 0, 1, 3, 1, 0, cv::BORDER_REPLICATE);
            cv::pow(dx, 2.0, dx);
            cv::pow(dy, 2.0, dy);
            cv::add(dx, dy, ret);
            cv::sqrt(ret, ret);
            break;
        }

        return ret;
    }

    auto TCBestSliceFinder::Impl::AggregateSlice(cv::Mat mat) const -> double {
        double ret = 0;
        switch (aggregator) {
        case Aggregator::Sum:
            ret = cv::sum(mat)[0];
            break;
        case Aggregator::Mean:
            ret = cv::mean(mat)[0];
            break;
        }
        return ret;
    }

    TCBestSliceFinder::~TCBestSliceFinder() = default;

    auto TCBestSliceFinder::GetXYSliceScore(uint16_t const* arr, size_t sizeX, size_t sizeY) const -> double {
        const auto slice = d->GetCvF64Slice(arr, sizeX, sizeY);
        const auto processed = d->GetKernelResult(slice);
        const auto score = d->AggregateSlice(processed);
        return score;
    }

    auto TCBestSliceFinder::GetXYSlicesScores(uint16_t const* arr, size_t sizeX, size_t sizeY, size_t sizeZ) const -> std::vector<double> {
        auto ret = std::vector(sizeZ, static_cast<double>(0));
        for (size_t z = 0; z < sizeZ; ++z) {
            ret[z] = GetXYSliceScore(arr + z * sizeX * sizeY, sizeX, sizeY);
        }

        return ret;
    }

    auto TCBestSliceFinder::SelectBestSlice(const std::vector<double>&scores) const->std::optional<size_t> {
        size_t ret = 0;
        switch (d->pivot) {
        case Pivot::Max:
            ret = std::max_element(scores.cbegin(), scores.cend()) - scores.cbegin();
            break;
        case Pivot::Min:
            ret = std::min_element(scores.cbegin(), scores.cend()) - scores.cbegin();
            break;
        }

        if (ret >= scores.size()) {
            return std::nullopt;
        }
        return ret;

    }

    auto TCBestSliceFinder::FindXYSlice(uint16_t const* arr, size_t sizeX, size_t sizeY, size_t sizeZ) const -> std::optional<size_t> {
        const auto scores = GetXYSlicesScores(arr, sizeX, sizeY, sizeZ);
        return SelectBestSlice(scores);
    }

}