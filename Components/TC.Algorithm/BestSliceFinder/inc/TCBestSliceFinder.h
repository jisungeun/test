#pragma once

#include <memory>
#include <optional>

#include "TCBestSliceFinderExport.h"

namespace TC::Components::Algorithm {
    class TCBestSliceFinder_API TCBestSliceFinder {
    public:
        enum class Kernel {
            SobelXYNorm,
        };
        enum class Aggregator {
            Sum,
            Mean,
        };
        enum class Pivot {
            Max,
            Min,
        };
        explicit TCBestSliceFinder(Kernel kernel = Kernel::SobelXYNorm, Aggregator aggregator = Aggregator::Mean, Pivot pivot = Pivot::Max);
        virtual ~TCBestSliceFinder();

        /**
         * \brief Calculate the focus score of a XY-slice
         * \param arr Linear array of slice in XY order
         * \param sizeX
         * \param sizeY
         * \return The aggregated focus score
         */
        auto GetXYSliceScore(uint16_t const* arr, size_t sizeX, size_t sizeY) const-> double;

        /**
         * \brief Calculate the focus score of each slice within a volume
         * \param arr Linear array of volume in ZXY order
         * \param sizeX
         * \param sizeY
         * \param sizeZ
         * \return The list of aggregated focus scores for each XY slice
         */
        auto GetXYSlicesScores(uint16_t const* arr, size_t sizeX, size_t sizeY, size_t sizeZ) const->std::vector<double>;

        /**
         * \brief Select the best slice given the focus scores and the selection pivot
         * \param scores The list of aggregated focus scores for each XY slice
         * \return The 0-based index of the best slice, or null if it is invalid
         */
        [[nodiscard]] auto SelectBestSlice(const std::vector<double>& scores) const->std::optional<size_t>;

        /**
         * \brief Find the best xy-slice w.r.t. the configurations given a whole volume
         * \param arr Linear array of volume in ZXY order
         * \param sizeX
         * \param sizeY
         * \param sizeZ
         * \return Z-index (0-based) of the best slice
         */
        [[nodiscard]] auto FindXYSlice(uint16_t const* arr, size_t sizeX, size_t sizeY, size_t sizeZ) const->std::optional<size_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}