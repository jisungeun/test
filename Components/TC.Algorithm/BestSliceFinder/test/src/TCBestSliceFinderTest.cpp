#include <catch2/catch.hpp>

#include <opencv2/opencv.hpp>

#include "TCBestSliceFinder.h"

namespace TCDataInterpolatorSphereTest {
    using namespace TC::Components::Algorithm;

    struct TestMultiArray {
        std::unique_ptr<uint16_t[]> arr;
        size_t x, y, z;

        TestMultiArray(size_t x, size_t y, size_t z): arr(std::make_unique<uint16_t[]>(x * y * z)), x(x), y(y), z(z) {}
        TestMultiArray(const TestMultiArray&) = delete;
        auto operator=(const TestMultiArray&)->TestMultiArray& = delete;
        TestMultiArray(TestMultiArray&&) = default;
        auto operator=(TestMultiArray&&)->TestMultiArray& = default;
        ~TestMultiArray() = default;

        [[nodiscard]] auto GetLinearIndex(size_t ix, size_t iy, size_t iz) const -> size_t {
            return iy + y * (ix + x * iz);
        }

        auto operator[](size_t i) -> uint16_t& {
            return arr[i];
        }

        auto operator[](size_t i) const -> const uint16_t& {
            return arr[i];
        }

        auto operator()(size_t ix, size_t iy, size_t iz) -> uint16_t& {
            return operator[](GetLinearIndex(ix, iy, iz));
        }

        auto operator()(size_t ix, size_t iy, size_t iz) const -> const uint16_t& {
            return operator[](GetLinearIndex(ix, iy, iz));
        }
    };

    auto MakeBlur(TestMultiArray& img, size_t zIndex, int kernelSize, double sigma)-> void {
        cv::Mat ret(static_cast<int>(img.x), static_cast<int>(img.y), CV_64F);
        const auto matData = ret.ptr<double>(0);
        for(size_t j = 0; j < img.y; ++j) {
            for(size_t i = 0; i < img.x; ++i) {
                matData[i + j * img.x] = static_cast<double>(img(i, j, zIndex));
            }
        }
        cv::GaussianBlur(ret, ret, cv::Size(kernelSize, kernelSize), sigma);
        ret = ret * (1.0 / kernelSize);
        for(size_t j = 0; j < img.y; ++j) {
            for(size_t i = 0; i < img.x; ++i) {
                img(i, j, zIndex) = static_cast<uint16_t>(matData[i + j * img.x]);
            }
        }
    }

    auto WriteSphereVolume(TestMultiArray& img, double cx, double cy, double cz, double radius, uint16_t value, bool l1Dist)->void {
        const auto distanceLimit = l1Dist ? radius : radius * radius;
        for(size_t iz = 0; iz < img.z; ++iz) {
            const auto dz = cz - static_cast<double>(iz);
            const auto dz2 = dz * dz;
            const auto dz1 = std::abs(dz);
            for(size_t ix = 0; ix < img.x; ++ix) {
                const auto dx = cx - static_cast<double>(ix);
                const auto dx2 = dx * dx;
                const auto dx1 = std::abs(dx);
                for(size_t iy = 0; iy < img.y; ++iy) {
                    const auto dy = cy - static_cast<double>(iy);
                    const auto dy2 = dy * dy;
                    const auto dy1 = std::abs(dy);

                    if (const auto distance = l1Dist ? dx1 + dy1 + dz1 : dx2 + dy2 + dz2; distance < distanceLimit) {
                        img(ix, iy, iz) = value;
                    }
                }
            }
        }
    }

    auto Print(const TestMultiArray& img) {
        for (int k = 0; k < img.z; ++k) {
            std::cout << "slice " << k << '\n';
            for (int j = 0; j < img.y; ++j) {
                for (int i = 0; i < img.x; ++i) {
                    std::cout << img(i, j, k) << ' ';
                }
                std::cout << '\n';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

    TEST_CASE("TCBestSliceFinderTest") {
        auto arr = TestMultiArray(30, 30, 30);
        WriteSphereVolume(arr, 15.0, 15.0, 15.0, 10.0, static_cast<uint16_t>(1.337 * 10000), true);
        constexpr auto bestFocus = static_cast<size_t>(13);
        for(size_t z = 0; z < 30; ++z) {
            if(z == bestFocus) {
                continue;
            }
            const auto deltaZ = std::abs(static_cast<int>(z) - static_cast<int>(bestFocus));
            constexpr auto outOfFocusKernelSizeCoefficient = 1;
            const auto kernelSize = outOfFocusKernelSizeCoefficient * (2 * deltaZ + 1);
            constexpr auto gaussianSigma = 2;
            MakeBlur(arr, z, kernelSize, gaussianSigma * deltaZ);
        }

        SECTION("FindXYSlice()") {
            const auto finder = TCBestSliceFinder{};

            const auto output = finder.FindXYSlice(arr.arr.get(), arr.x, arr.y, arr.z);
            CHECK(output == bestFocus);
        }

        SECTION("SelectBestSlice()") {
            const auto scores = std::vector<double>{ 1, 0, 3, 2, 4 };
            const auto minFinder = TCBestSliceFinder(TCBestSliceFinder::Kernel::SobelXYNorm, TCBestSliceFinder::Aggregator::Mean, TCBestSliceFinder::Pivot::Min);
            const auto maxFinder = TCBestSliceFinder(TCBestSliceFinder::Kernel::SobelXYNorm, TCBestSliceFinder::Aggregator::Mean, TCBestSliceFinder::Pivot::Max);

            CHECK(minFinder.SelectBestSlice(scores) == 1);
            CHECK(maxFinder.SelectBestSlice(scores) == 4);
        }
    }
}