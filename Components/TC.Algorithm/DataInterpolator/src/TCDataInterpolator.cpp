#define LOGGER_TAG "[TCDataInterpolator]"
#include <TCLogger.h>

#include <vector>
#include <unordered_set>

#include <opencv2/opencv.hpp>
#include <iolink/view/ImageViewFactory.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include "TCDataInterpolator.h"

namespace TC::Components::Algorithm {
    struct TCDataInterpolator::Impl {
        static auto GetCoordsFromIndex(size_t index,
            const iolink::VectorXu64& shape) -> std::tuple<size_t, size_t, size_t>;
        static auto GetPhysical(int index, double spacing, double origin, double offset) -> double;
        static auto GetIndex(double p, double spacing, double origin, double offset) -> int;
        static auto GetInterpolateCoefficients(std::size_t targetSize, double targetSpacing, double targetOrigin,
            double targetOffset, double sourceSpacing, double sourceOrigin,
            double sourceOffset) -> std::vector<std::pair<int, double>>;
        static auto GetContour(const cv::Mat& im) -> cv::Mat;
        static auto GetSignedDistance(cv::Mat& im) -> cv::Mat;
        static auto InterpolateTwoSlices(cv::Mat& lower, cv::Mat& upper, double coefficient) -> cv::Mat;
        static auto InterpolateBinarySlices(const std::vector<cv::Mat>& binarySourceSlices,
            const std::vector<std::pair<int, double>>& intZ, bool nearest) -> std::vector<cv::Mat>;
    };

    auto TCDataInterpolator::Impl::GetCoordsFromIndex(size_t index,
        const iolink::VectorXu64& shape) -> std::tuple<size_t, size_t, size_t> {
        return {index % shape[0], index / shape[0] % shape[1], index / shape[0] / shape[1] % shape[2]};
    }

    auto TCDataInterpolator::Impl::GetPhysical(int index, double spacing, double origin, double offset) -> double {
        return origin + offset + index * spacing;
    }

    auto TCDataInterpolator::Impl::GetIndex(double p, double spacing, double origin, double offset) -> int {
        return static_cast<int>(floor((p - origin - offset) / spacing));
    }

    auto TCDataInterpolator::Impl::GetInterpolateCoefficients(std::size_t targetSize, double targetSpacing,
        double targetOrigin, double targetOffset, double sourceSpacing, double sourceOrigin,
        double sourceOffset) -> std::vector<std::pair<int, double>> {
        auto ret = std::vector<std::pair<int, double>>(targetSize);
        for (auto targetIndex = 0u; targetIndex < targetSize; ++targetIndex) {
            const auto loc = GetPhysical(static_cast<int>(targetIndex), targetSpacing, targetOrigin, targetOffset);
            const auto sourceLowerIndex = GetIndex(loc, sourceSpacing, sourceOrigin, sourceOffset);
            const auto sourceUpperIndex = sourceLowerIndex + 1;
            const auto sourceLowerLoc = GetPhysical(sourceLowerIndex, sourceSpacing, sourceOrigin, sourceOffset);
            const auto sourceUpperLoc = GetPhysical(sourceUpperIndex, sourceSpacing, sourceOrigin, sourceOffset);

            const auto coefficient = (loc - sourceLowerLoc) / (sourceUpperLoc - sourceLowerLoc);
            ret[targetIndex] = {sourceLowerIndex, coefficient};
        }
        return ret;
    }

    auto TCDataInterpolator::Impl::GetContour(const cv::Mat& im) -> cv::Mat {
        cv::Mat dilated{};
        dilate(im, dilated, cv::Mat());
        return 0 - (im - dilated);
    }

    auto TCDataInterpolator::Impl::GetSignedDistance(cv::Mat& im) -> cv::Mat {
        const auto contour = GetContour(im);
        cv::Mat dist{};
        distanceTransform(1 - contour, dist, cv::DIST_L2, cv::DIST_MASK_PRECISE);

        cv::Mat result = cv::Mat::zeros(im.size(), CV_64F);
        for (int i = 0; i < im.rows; ++i) {
            for (int j = 0; j < im.cols; ++j) {
                result.at<double>(i, j) = dist.at<float>(i, j) * static_cast<float>(im.at<uchar>(i, j) == 0 ? -1 : 1);
            }
        }
        return result;
    }

    auto TCDataInterpolator::Impl::InterpolateTwoSlices(cv::Mat& lower, cv::Mat& upper,
        const double coefficient) -> cv::Mat {
        const auto resultX = lower.cols;
        const auto resultY = lower.rows;
        cv::Mat result = cv::Mat::zeros(resultY, resultX, CV_8UC1);
        const auto lowerDist = GetSignedDistance(lower);
        const auto upperDist = GetSignedDistance(upper);
        const auto interpolatedDist = cv::Mat{lowerDist * (1 - coefficient) + upperDist * coefficient};

        for (int i = 0; i < result.rows; ++i) {
            for (int j = 0; j < result.cols; ++j) {
                result.at<uchar>(i, j) = interpolatedDist.at<double>(i, j) > 0 ? 1 : 0;
            }
        }
        return result;
    }

    auto TCDataInterpolator::Impl::InterpolateBinarySlices(const std::vector<cv::Mat>& binarySourceSlices,
        const std::vector<std::pair<int, double>>& intZ, bool nearest) -> std::vector<cv::Mat> {
        const auto resultSize = binarySourceSlices[0].size();
        const auto sz = static_cast<int>(binarySourceSlices.size());
        const auto tz = intZ.size();

        std::vector<cv::Mat> result{};
        for (auto k = 0u; k < tz; ++k) {
            const auto [sourceZLower, sourceZCoefficient] = intZ[k];
            cv::Mat lower = 0 <= sourceZLower && sourceZLower < sz
                                ? binarySourceSlices[sourceZLower]
                                : cv::Mat::zeros(resultSize, CV_8UC1);
            const auto sourceZUpper = sourceZLower + 1;
            cv::Mat upper = 0 <= sourceZUpper && sourceZUpper < sz
                                ? binarySourceSlices[sourceZUpper]
                                : cv::Mat::zeros(resultSize, CV_8UC1);
            const auto interpolated = nearest
                                          ? (sourceZCoefficient > 0.5 ? upper : lower)
                                          : InterpolateTwoSlices(lower, upper, sourceZCoefficient);
            result.push_back(interpolated);
        }
        return result;
    }


    TCDataInterpolator::TCDataInterpolator() : d{new Impl} {
    }

    // WARNING : This function is same as TCDataConverter::MapMaskGeometry, and would be no longer valid afterward.
    auto TCDataInterpolator::MapMaskGeometry(const std::shared_ptr<iolink::ImageView>& refImage,
        const std::shared_ptr<iolink::ImageView>& mask, double refOffset,
        double maskOffset) -> std::shared_ptr<iolink::ImageView> {
        std::shared_ptr<iolink::ImageView> mappedMask{nullptr};
        try {
            const auto targetShape = refImage->shape();
            const auto targetShapeI32 = iolink::VectorXi32(targetShape);

            if (targetShape.size() < 3 || targetShape[2] < 2) {
                // 2D
                mappedMask = rescaleImage2d(mask, targetShapeI32[0], targetShapeI32[1],
                                            imagedev::RescaleImage2d::LINEAR);
            } else {
                auto GetPhysicalPosition = [](int ix, int iy, int iz, double spx, double spy, double spz,
                    double originX, double originY, double originZ,
                    double offset) -> std::tuple<double, double, double> {
                    auto phyX = originX + ix * spx;
                    auto phyY = originY + iy * spy;
                    auto phyZ = originZ + offset + iz * spz;

                    return std::make_tuple(phyX, phyY, phyZ);
                };

                auto GetIndexPosition = [](double px, double py, double pz, double spx, double spy, double spz,
                    double originX, double originY, double originZ, double offset) -> std::tuple<int, int, int> {
                    auto ix = static_cast<int>(floor((px - originX) / spx));
                    auto iy = static_cast<int>(floor((py - originY) / spy));
                    auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));

                    return std::make_tuple(ix, iy, iz);
                };

                const auto bufferSize = targetShape[0] * targetShape[1] * targetShape[2];
                if (bufferSize < 1) throw std::runtime_error("Data size is 0.");

                const auto arr = std::shared_ptr<uint16_t[]>(new uint16_t[bufferSize](),
                                                             std::default_delete<uint16_t[]>());

                const auto targetRes = refImage->properties()->calibration().spacing();
                const auto targetOrigin = refImage->properties()->calibration().origin();

                const auto sourceRes = mask->properties()->calibration().spacing();
                const auto sourceOrigin = mask->properties()->calibration().origin();
                const auto sourceShape = mask->shape();
                const auto sourceShapeI32 = iolink::VectorXi32(sourceShape);

                const auto sourceArr = static_cast<const uint16_t*>(mask->bufferReadOnly());

                for (auto k = 0; k < targetShapeI32[2]; k++) {
                    for (auto i = 0; i < targetShapeI32[0]; i++) {
                        for (auto j = 0; j < targetShapeI32[1]; j++) {
                            const auto [px, py, pz] = GetPhysicalPosition(
                                i, j, k, targetRes[0], targetRes[1], targetRes[2], targetOrigin[0], targetOrigin[1],
                                targetOrigin[2], refOffset);
                            const auto [ix, iy, iz] = GetIndexPosition(px, py, pz, sourceRes[0], sourceRes[1],
                                                                       sourceRes[2], sourceOrigin[0], sourceOrigin[1],
                                                                       sourceOrigin[2], maskOffset);

                            if (ix > -1 && ix < sourceShapeI32[0] && iy > -1 && iy < sourceShapeI32[1] && iz > -1 && iz
                                < sourceShapeI32[2]) {
                                const auto sourceIdx = iz * sourceShape[0] * sourceShape[1] + iy * sourceShape[0] + ix;
                                const auto val = *(sourceArr + sourceIdx);
                                const auto targetIdx = k * targetShape[0] * targetShape[1] + j * targetShape[0] + i;
                                *(arr.get() + targetIdx) = val;
                            }
                        }
                    }
                }

                const auto tempImage = iolink::ImageViewFactory::allocate(
                    targetShape, iolink::DataTypeId::UINT16, refImage->properties()->clone(), nullptr);
                imagedev::setDimensionalInterpretation(tempImage, iolink::ImageTypeId::VOLUME);

                const iolink::RegionXu64 imageRegion{{0, 0, 0}, targetShape};
                tempImage->writeRegion(imageRegion, arr.get());

                mappedMask = convertImage(tempImage, imagedev::ConvertImage::LABEL_16_BIT);
            }
        }
        catch (std::exception& e) {
            QLOG_ERROR() << e.what();
        } catch (imagedev::Exception& e) {
            QLOG_ERROR() << QString::fromStdString(e.what());
        } catch (...) {
            QLOG_ERROR() << "UNKNOWN ERROR";
        }

        return mappedMask;
    }

    /**
     * \brief Interpolating indexed mask volume into the reference image resolution.
     * \param refImage Image view of the reference
     * \param mask Image view of the mask
     * \param refOffset Z-offset of the reference
     * \param maskOffset Z-offset of the mask
     * \param nearestNeighbor Whether the z-axis interpolations follow a nearest-neighbor manner
     * \return New image view of the mask, with the same shape of refImage
     */
    auto TCDataInterpolator::InterpolateMaskGeometry(const std::shared_ptr<iolink::ImageView>& refImage,
        const std::shared_ptr<iolink::ImageView>& mask, double refOffset, double maskOffset,
        bool nearestNeighbor) -> std::shared_ptr<iolink::ImageView> {
        std::shared_ptr<iolink::ImageView> mappedMask{nullptr};
        try {
            const auto sourceShape = mask->shape();

            if (const auto targetShape = refImage->shape(); targetShape.size() < 3 || targetShape[2] < 2) {
                // 2D
                const auto rescaleMode = nearestNeighbor
                                             ? imagedev::RescaleImage2d::NEAREST_NEIGHBOR
                                             : imagedev::RescaleImage2d::LINEAR;
                mappedMask = rescaleImage2d(mask, static_cast<int>(targetShape[0]), static_cast<int>(targetShape[1]),
                                            rescaleMode);
            } else {
                // 3D
                const auto sourceSize = sourceShape[0] * sourceShape[1] * sourceShape[2];
                const auto targetSize = targetShape[0] * targetShape[1] * targetShape[2];
                if (targetSize < 1) throw std::runtime_error("Data size is 0.");

                const auto arr = std::shared_ptr<uint16_t[]>(new uint16_t[targetSize](),
                                                             std::default_delete<uint16_t[]>());
                for (auto i = 0ull; i < targetSize; ++i) {
                    *(arr.get() + i) = 0;
                }

                const auto targetRes = refImage->properties()->calibration().spacing();
                const auto targetOrigin = refImage->properties()->calibration().origin();

                const auto sourceRes = mask->properties()->calibration().spacing();
                const auto sourceOrigin = mask->properties()->calibration().origin();

                const auto sourceArr = static_cast<const uint16_t*>(mask->bufferReadOnly());

                // Step 0. get interpolation coefficients along Z axis
                const auto interpolateZ = Impl::GetInterpolateCoefficients(
                    targetShape[2], targetRes[2], targetOrigin[2], refOffset, sourceRes[2], sourceOrigin[2],
                    maskOffset);

                // Step 1. make binary masks for each label index, and resize into target shape (nearest neighbor)
                auto maskIndices = std::unordered_set<uint16_t>{};
                for (auto i = 0ull; i < sourceSize; ++i) {
                    const auto val = sourceArr[i];
                    if (val == 0) continue;
                    maskIndices.insert(val);
                }
                auto binaryMaskSource = std::unordered_map<uint16_t, std::vector<cv::Mat>>{};
                for (auto v : maskIndices) {
                    binaryMaskSource[v] = std::vector<cv::Mat>(sourceShape[2]);
                    for (auto k = 0u; k < sourceShape[2]; ++k) {
                        binaryMaskSource[v][k] = cv::Mat::zeros(static_cast<int>(sourceShape[1]),
                                                                static_cast<int>(sourceShape[0]), CV_8UC1);
                    }
                }
                for (auto i = 0ull; i < sourceSize; ++i) {
                    const auto v = sourceArr[i];
                    if (v != 0) {
                        const auto [x, y, z] = Impl::GetCoordsFromIndex(i, sourceShape);
                        binaryMaskSource[v][z].at<uchar>(static_cast<int>(y), static_cast<int>(x)) = 1;
                    }
                }
                for (auto v : maskIndices) {
                    for (auto k = 0u; k < sourceShape[2]; ++k) {
                        cv::Mat resized = cv::Mat::zeros(static_cast<int>(targetShape[1]),
                                                         static_cast<int>(targetShape[0]), CV_8UC1);
                        resize(binaryMaskSource[v][k], resized, resized.size(), 0, 0, cv::INTER_NEAREST);
                        binaryMaskSource[v][k] = resized;
                    }
                }

                // Step 2. interpolate along Z-axis
                for (auto v : maskIndices) {
                    const auto binaryMaskTarget = Impl::InterpolateBinarySlices(binaryMaskSource[v], interpolateZ,
                        nearestNeighbor);

                    for (size_t i = 0; i < targetSize; ++i) {
                        const auto [x, y, z] = Impl::GetCoordsFromIndex(i, targetShape);
                        const auto pixel = binaryMaskTarget[z].at<uchar>(static_cast<int>(y), static_cast<int>(x));
                        if (pixel != 0) {
                            arr[static_cast<ptrdiff_t>(i)] = v;
                        }
                    }
                }

                // Step 3. finished, construct the mask view
                const auto tempImage = iolink::ImageViewFactory::allocate(targetShape, iolink::DataTypeId::UINT16,
                                                                          refImage->properties()->clone(), nullptr);
                imagedev::setDimensionalInterpretation(tempImage, iolink::ImageTypeId::VOLUME);

                const iolink::RegionXu64 imageRegion{{0, 0, 0}, targetShape};
                tempImage->writeRegion(imageRegion, arr.get());

                mappedMask = convertImage(tempImage, imagedev::ConvertImage::LABEL_16_BIT);
            }
        }
        catch (std::exception& e) {
            QLOG_ERROR() << e.what();
        } catch (imagedev::Exception& e) {
            QLOG_ERROR() << QString::fromStdString(e.what());
        } catch (...) {
            QLOG_ERROR() << "UNKNOWN ERROR";
        }

        return mappedMask;
    }
}
