#include <catch2/catch.hpp>

#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Initialization.h>
#include <ImageDev/Exception.h>
#include <iolink/view/ImageView.h>
#include <iolink/view/ImageViewFactory.h>

#include "TCDataInterpolator.h"

namespace TCDataInterpolatorSphereTest {
    using namespace TC::Components::Algorithm;
    using namespace iolink;
    using namespace imagedev;
    using ImagePtr = std::shared_ptr<ImageView>;

    auto MakeVolume(size_t x, size_t y, size_t z, double resX, double resY, double resZ)->ImagePtr {
        const auto spatialCalibrationProperty = SpatialCalibrationProperty(
            { -static_cast<double>(x) * resX / 2, -static_cast<double>(y) * resY / 2, -static_cast<double>(z) * resZ / 2 },
            { resX, resY, resZ }
        );
        const auto imageProp = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(
                                                                     DataTypeId::UINT16));
        const auto imageShape = VectorXu64{ x, y, z };

        const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProp, nullptr);
        setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

        const auto imageRegion = RegionXu64{ {0, 0, 0}, imageShape };
        const auto zeros = std::make_unique<uint16_t[]>(x * y * z);
        imageView->writeRegion(imageRegion, zeros.get());
        return imageView;
    }

    auto WriteSphereVolume(const ImagePtr& img, double cx, double cy, double cz, double radius, uint16_t label, bool l1Dist)->void {
        const auto distanceLimit = l1Dist ? radius : radius * radius;
        const auto dim = img->shape();
        const auto x = dim[0];
        const auto y = dim[1];
        const auto z = dim[2];
        const auto spacing = img->properties()->calibration().spacing();
        const double res[3]{ spacing[0], spacing[1], spacing[2] };
        const auto buffer = static_cast<uint16_t*>(img->buffer());
        for(size_t iz = 0; iz < z; ++iz) {
            const auto dz = cz - static_cast<double>(iz) * res[2];
            const auto dz2 = dz * dz;
            const auto dz1 = std::abs(dz);
            for(size_t ix = 0; ix < x; ++ix) {
                const auto dx = cx - static_cast<double>(ix) * res[0];
                const auto dx2 = dx * dx;
                const auto dx1 = std::abs(dx);
                for(size_t iy = 0; iy < y; ++iy) {
                    const auto dy = cy - static_cast<double>(iy) * res[1];
                    const auto dy2 = dy * dy;
                    const auto dy1 = std::abs(dy);

                    if (const auto distance = l1Dist ? dx1 + dy1 + dz1 : dx2 + dy2 + dz2; distance < distanceLimit) {
                        buffer[iy + y * (ix + x * iz)] = label;
                    }
                }
            }
        }
    }

    auto IsSubsetMaskSlice(const ImagePtr& small, size_t smallZIndex, const ImagePtr& big, size_t bigZIndex, uint16_t label) -> bool {
        const auto dimSmall = small->shape();
        const auto dimBig = big->shape();
        if(dimSmall[0] != dimBig[0] ||
            dimSmall[1] != dimBig[1]) {
            return false;
        }

        const auto smallBuffer = static_cast<const uint16_t*>(small->bufferReadOnly());
        const auto bigBuffer = static_cast<const uint16_t*>(big->bufferReadOnly());

        const auto x = dimSmall[0];
        const auto y = dimSmall[1];

        for(size_t ix = 0; ix < x; ++ix) {
            for(size_t iy = 0; iy < y; ++iy) {
                const auto smallIdx = iy + y * (ix + x * smallZIndex);
                const auto bigIdx = iy + y * (ix + x * bigZIndex);
                if(smallBuffer[smallIdx] == label && bigBuffer[bigIdx] != label) {
                    return false;
                }
            }
        }
        return true;
    }

    auto IsSupersetMaskSlice(const ImagePtr& big, size_t bigZIndex, const ImagePtr& small, size_t smallZIndex, uint16_t label) -> bool {
        return IsSubsetMaskSlice(small, smallZIndex, big, bigZIndex, label);
    }

    auto Print(const ImagePtr& img) {
        const auto dim = img->shape();
        const auto x = dim[0];
        const auto y = dim[1];
        const auto z = dim[2];
        for (int k = 0; k < z; ++k) {
            std::cout << "slice " << k << '\n';
            for (int i = 0; i < x; ++i) {
                for (int j = 0; j < y; ++j) {
                    std::cout << static_cast<uint16_t*>(img->buffer())[j + y * (i + x * k)] << ' ';
                }
                std::cout << '\n';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

    TEST_CASE("TCDataInterpolator : sphere with single label") {
        try {
            imagedev::init();
        }
        catch (imagedev::Exception& e) {
            FAIL(e.what().c_str());
        }


        // this will be the target to interpolate
        const auto refImage = MakeVolume(10, 10, 10, 1, 1, 1);

        SECTION("More sparse mask") {
            const auto mask = MakeVolume(10, 10, 5, 1, 1, 2);
            WriteSphereVolume(mask, 5, 5, 5, 5, 1, true);

            const auto interpolated = TCDataInterpolator::InterpolateMaskGeometry(refImage, mask);

            REQUIRE(interpolated != nullptr);
            CHECK(IsSubsetMaskSlice(interpolated, 4, mask, 2, 1));
            CHECK(IsSupersetMaskSlice(interpolated, 4, mask, 3, 1));
            CHECK(IsSubsetMaskSlice(interpolated, 6, mask, 2, 1));
            CHECK(IsSupersetMaskSlice(interpolated, 6, mask, 4, 1));
        }

        SECTION("Denser mask") {
            const auto mask = MakeVolume(10, 10, 20, 1, 1, 0.5);
            WriteSphereVolume(mask, 5, 5, 5, 10, 1, true);

            const auto interpolated = TCDataInterpolator::InterpolateMaskGeometry(refImage, mask);

            REQUIRE(interpolated != nullptr);
            CHECK(IsSubsetMaskSlice(mask, 11, interpolated, 5, 1));
            CHECK(IsSupersetMaskSlice(mask, 11, interpolated, 6, 1));
            CHECK(IsSubsetMaskSlice(mask, 9, interpolated, 5, 1));
            CHECK(IsSupersetMaskSlice(mask, 9, interpolated, 4, 1));
        }

        imagedev::finish();
    }

    TEST_CASE("TCDataInterpolator : sphere with double label") {
        try {
            imagedev::init();
        }
        catch (imagedev::Exception& e) {
            FAIL(e.what().c_str());
        }

        // this will be the target to interpolate
        const auto refImage = MakeVolume(20, 20, 10, 1, 1, 1);

        SECTION("Label 1 getting larger upwards, Label 2 getting larger downwards") {
            const auto mask = MakeVolume(20, 20, 5, 1, 1, 2);
            WriteSphereVolume(mask, 5, 5, 0, 9, 1, true);
            WriteSphereVolume(mask, 15, 15, 20, 9, 2, true);

            const auto interpolated = TCDataInterpolator::InterpolateMaskGeometry(refImage, mask);

            REQUIRE(interpolated != nullptr);
            CHECK(IsSubsetMaskSlice(mask, 2, interpolated, 2, 1));
            CHECK(IsSupersetMaskSlice(mask, 2, interpolated, 2, 2));
            CHECK(IsSubsetMaskSlice(mask, 4, interpolated, 8, 1));
            CHECK(IsSupersetMaskSlice(mask, 4, interpolated, 8, 2));
        }

        imagedev::finish();
    }
}