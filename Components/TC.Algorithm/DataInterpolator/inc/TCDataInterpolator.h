#pragma once

#include <memory>
#include <iolink/view/ImageView.h>

#include "TCDataInterpolatorExport.h"

namespace TC::Components::Algorithm {
	/**
	 * @brief This class offer help to interpolate volume between modalities
	 */
	class TCDataInterpolator_API TCDataInterpolator {
	public:
		TCDataInterpolator();
		[[deprecated, nodiscard]] static auto MapMaskGeometry(const std::shared_ptr<iolink::ImageView>& refImage, const std::shared_ptr<iolink::ImageView>& mask, double refOffset = 0, double maskOffset = 0) -> std::shared_ptr<iolink::ImageView>;
		[[nodiscard]] static auto InterpolateMaskGeometry(const std::shared_ptr<iolink::ImageView>& refImage, const std::shared_ptr<iolink::ImageView>& mask, double refOffset = 0, double maskOffset = 0, bool nearestNeighbor = false) -> std::shared_ptr<iolink::ImageView>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
