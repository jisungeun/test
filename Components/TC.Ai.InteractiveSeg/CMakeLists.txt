project(TCAiInteractiveSeg)

#Header files for external use
set(INTERFACE_HEADERS
	inc/TCAiInteractiveSeg.h
)

#Header files for internal use
set(PRIVATE_HEADERS
	
)

#Sources
set(SOURCES		
	src/TCAiInteractiveSeg.cpp	
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TC::Components::Ai::InteractiveSegmentation ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src        
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/AI")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

target_link_libraries( ${PROJECT_NAME} 
	PUBLIC
	PRIVATE		
		Qt5::Core		
		TC::Components::TCLogger		
		TC::Components::Python
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT ai)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)
install(DIRECTORY ${TOMO_CONTROL_DIR}/models/interactive_segmentation_v2/ DESTINATION ${CMAKE_INSTALL_BINDIR}/pyScript/interSeg COMPONENT ai)
# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${TOMO_CONTROL_DIR}/models/interactive_segmentation_v2 $<TARGET_FILE_DIR:${PROJECT_NAME}>/pyScript/interSeg)