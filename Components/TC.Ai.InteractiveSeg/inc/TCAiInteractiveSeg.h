#pragma once

#include <memory>
#include <tuple>

#include "TCAiInteractiveSegExport.h"

namespace TC::AI {
    class TCAiInteractiveSeg_API TCAiInteractiveSeg {
    public:
        TCAiInteractiveSeg();
        ~TCAiInteractiveSeg();

        auto InitModel()->void;

        auto SetInputImage(unsigned short* inputImg, unsigned dim[3],unsigned char* inputMask=nullptr)->void;
        auto SelectSlice(int axis, int index)->void;
        auto SetCustomSlice(unsigned short* inputMask, unsigned dim[3])->void;
        auto Add2dPoint(int a, int b, bool positive = true)->void;
        auto Add3dPoint(int x, int y, int z, bool positive = true)->void;        
        auto Generate3dMask()->void;
        auto GetSliceRecommendation()->std::tuple<int, int>;
        auto ReleaseMemory()->void;

        auto GetOutputSlice(unsigned short* outBuffer,long long dstSize)->void;
        auto GetOutputMask(unsigned short* outBuffer,long long dstSize)->void;

        auto SetValue(int val)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };    
}