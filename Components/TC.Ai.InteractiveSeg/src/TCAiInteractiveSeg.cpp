#include <iostream>

#include <QList>

#include <TCPython.h>

#include <QtWidgets/qapplication.h>

#include "TCAiInteractiveSeg.h"

namespace TC::AI {
    struct TCAiInteractiveSeg::Impl{
        long long dim[3]{ 1,1,1 };
        QString script_path;
    };
    TCAiInteractiveSeg::TCAiInteractiveSeg() : d{ new Impl } {
        d->script_path = qApp->applicationDirPath() + "/pyScript/interSeg/";
    }
    TCAiInteractiveSeg::~TCAiInteractiveSeg() {

    }
    auto TCAiInteractiveSeg::InitModel() -> void {       
        PythonModule::GetInstance()->ExecuteString("snc_path = './Lib/weights/snc/best_model'");
        PythonModule::GetInstance()->ExecuteString("is_3d_path = './Lib/weights/is_3d/best_model'");
        PythonModule::GetInstance()->ExecuteString("interactive_model = InteractiveModel(is_3d_path, snc_path)");
    }        
    auto TCAiInteractiveSeg::SelectSlice(int axis, int index) -> void {        
        PythonModule::GetInstance()->ExecuteString(QString("axis = %1").arg(axis));
        PythonModule::GetInstance()->ExecuteString(QString("slice_index = %1").arg(index));        
        PythonModule::GetInstance()->ExecuteString("interactive_model.set_image_slice(axis, slice_index)");
        
        PythonModule::GetInstance()->ExecuteString("slice_img, slice_mask = interactive_model.show_slice()");        
    }
    auto TCAiInteractiveSeg::SetCustomSlice(unsigned short* inputMask, unsigned dim[3]) -> void {
        PythonModule::GetInstance()->CopyArrToPy(inputMask, 2, dim, "custom_slice",ArrType::arrUSHORT,ArrType::arrUBYTE);
        PythonModule::GetInstance()->ExecuteString("interactive_model.update_2d_mask(custom_slice)");
    }

    auto TCAiInteractiveSeg::ReleaseMemory() -> void {
        auto local_script = QString("if '%1' in locals():");        
        local_script += "\n";
        local_script += QString("  del %1");

        //free following python local variables
        QStringList varList{"ri_np","input_mask","slice_img","slice_mask","axis","slice_index","point","mask","maskVal","conv_mask"};

        for(auto var :varList) {
            PythonModule::GetInstance()->ExecuteString(local_script.arg(var));
        }
    }

    auto TCAiInteractiveSeg::SetInputImage(unsigned short* inputImg, unsigned dim[3], unsigned char* inputMask) -> void {
        //copy array to python 'ri_np' variable
        PythonModule::GetInstance()->CopyArrToPy(inputImg, 3, dim, "ri_np");
        unsigned reshaped[3]{ dim[2],dim[0],dim[1] };
        PythonModule::GetInstance()->Reshape("ri_np", reshaped);
        if (nullptr != inputMask) {
            PythonModule::GetInstance()->CopyArrToPy(inputMask, 3, dim, "input_mask", ArrType::arrUBYTE, ArrType::arrUBYTE);
            unsigned reshaped[3]{ dim[2],dim[0],dim[1] };
            PythonModule::GetInstance()->Reshape("input_mask", reshaped);
            PythonModule::GetInstance()->ExecuteString("interactive_model.set_input_image(ri_np,input_mask)");
        }else {
            PythonModule::GetInstance()->ExecuteString("interactive_model.set_input_image(ri_np)");
        }        
        for (auto i = 0; i < 3; i++) {
            d->dim[i] = dim[i];
        }
    }
    auto TCAiInteractiveSeg::Add2dPoint(int a, int b, bool positive) -> void {
        QString tfString = "False";
        if(positive) {
            tfString = "True";
        }
        PythonModule::GetInstance()->ExecuteString(QString("point = [%1, %2]").arg(a).arg(b));
        PythonModule::GetInstance()->ExecuteString(QString("mask = interactive_model.get_prediction(point, %1)").arg(tfString));
        //temporal mask is stored in mask variable
    }
    auto TCAiInteractiveSeg::SetValue(int val) -> void {
        PythonModule::GetInstance()->ExecuteString(QString("maskVal = %1").arg(val));
    }
    auto TCAiInteractiveSeg::Add3dPoint(int x, int y, int z, bool positive) -> void {
        QString tfString = "False";
        if (positive) {
            tfString = "True";
        }
        PythonModule::GetInstance()->ExecuteString(QString("point =  [%1, %2, %3]").arg(x).arg(y).arg(z));
        PythonModule::GetInstance()->ExecuteString(QString("mask = interactive_model.get_prediction(point, %1)").arg(tfString));
    }
    auto TCAiInteractiveSeg::Generate3dMask() -> void {
        PythonModule::GetInstance()->ExecuteString("mask = interactive_model.get_3d_mask()");
    }
    auto TCAiInteractiveSeg::GetSliceRecommendation() -> std::tuple<int, int> {
        PythonModule::GetInstance()->ExecuteString("axis, slice_index = interactive_model.slice_recommendation()");
        PythonModule::GetInstance()->ExecuteString("print(axis)");
        PythonModule::GetInstance()->ExecuteString("print(slice_index)");
        auto axis = PythonModule::GetInstance()->GetIntValue("axis");
        auto slice_index = PythonModule::GetInstance()->GetIntValue("slice_index");        
        return std::make_tuple(axis, slice_index);
    }
    auto TCAiInteractiveSeg::GetOutputSlice(unsigned short* outBuffer,long long dstSize) -> void {
        //convert np mask
        PythonModule::GetInstance()->ExecuteString(QString("conv_mask = np.where(mask > 0.5,maskVal,0)"));
        PythonModule::GetInstance()->ExecuteString("conv_mask = conv_mask.astype(np.uint16)");
        std::any anyPtr;
        auto metaInfo = PythonModule::GetInstance()->CopyArrToCpp(anyPtr, "conv_mask");
        auto dim = std::get<0>(metaInfo);
        auto typestr = std::get<1>(metaInfo);

        if("uint16" != typestr) {
            std::cout << "type: " << typestr << std::endl;
            anyPtr.reset();
            return;
        }
        long long calcSize = 1;
        for(auto dim_element : dim) {
            calcSize *= dim_element;
        }        
        if(calcSize != dstSize) {
            std::cout << QString("Src size: %1, Dst size: %2").arg(calcSize).arg(dstSize).toStdString() << std::endl;
            anyPtr.reset();
            return;
        }

        auto castArr = std::any_cast<std::shared_ptr<uint16_t[]>>(anyPtr);

        memcpy(outBuffer, castArr.get(), dstSize * sizeof(uint16_t));

        castArr = nullptr;
        anyPtr.reset();
    }
    auto TCAiInteractiveSeg::GetOutputMask(unsigned short* outBuffer,long long dstSize) -> void {
        //convert np mask
        PythonModule::GetInstance()->ExecuteString(QString("conv_mask = np.where(mask > 0.5,maskVal,0)"));
        PythonModule::GetInstance()->ExecuteString("conv_mask = conv_mask.astype(np.uint16)");
        std::any anyPtr;
        auto metaInfo = PythonModule::GetInstance()->CopyArrToCpp(anyPtr, "conv_mask");
        auto dim = std::get<0>(metaInfo);
        auto typestr = std::get<1>(metaInfo);

        if ("uint16" != typestr) {
            std::cout << "type: " << typestr << std::endl;
            anyPtr.reset();
            return;
        }
        long long calcSize = 1;
        for (auto dim_element : dim) {
            calcSize *= dim_element;
        }
        if (calcSize != dstSize) {
            std::cout << QString("Src size: %1, Dst size: %2").arg(calcSize).arg(dstSize).toStdString() << std::endl;
            anyPtr.reset();
            return;
        }

        auto castArr = std::any_cast<std::shared_ptr<uint16_t[]>>(anyPtr);

        memcpy(outBuffer, castArr.get(), dstSize * sizeof(uint16_t));

        castArr = nullptr;
        anyPtr.reset();
    }
}
