#pragma once

#include "TC.ComponentModelExport.h"

namespace Tomocube {
	class TC_ComponentModel_API IService {
	public:
		IService() = default;
		virtual ~IService() = default;
	};
}
