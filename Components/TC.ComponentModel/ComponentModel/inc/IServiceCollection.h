#pragma once

#include "IServiceProvider.h"

#include "TC.ComponentModelExport.h"

namespace Tomocube {
	class TC_ComponentModel_API IServiceCollection {
	public:
		IServiceCollection() = default;
		virtual ~IServiceCollection() = default;

		virtual auto MergeProvider(const IServiceProvider* rhs) const -> void;
		virtual auto BuildProvider() -> std::shared_ptr<IServiceProvider> = 0;

	protected:
		virtual auto GetProvider() const -> IServiceProvider* = 0;
	};
}
