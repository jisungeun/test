#pragma once

#include <memory>

#include <QVariantMap>

#include "IService.h"

#include "TC.ComponentModelExport.h"

namespace Tomocube {
	class TC_ComponentModel_API IServiceFactory {
	public:
		IServiceFactory() = default;
		virtual ~IServiceFactory() = default;

		virtual auto CreateService(void* provider) -> std::shared_ptr<IService> = 0;
		virtual auto ReadMetadata() -> QVariantMap = 0;
		virtual auto GetTypeId() -> size_t = 0;
	};
}
