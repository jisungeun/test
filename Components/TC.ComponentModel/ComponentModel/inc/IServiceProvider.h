#pragma once

#include <memory>

#include "IServiceFactory.h"

#include "TC.ComponentModelExport.h"

namespace Tomocube {
	class TC_ComponentModel_API IServiceProvider {
	public:
		IServiceProvider() = default;
		virtual ~IServiceProvider() = default;

		template <typename T>
		auto GetServices() -> QVector<std::shared_ptr<T>>;
		template <typename T>
		auto GetService() -> std::shared_ptr<T>;
		template <typename T>
		auto GetService(int index) -> std::shared_ptr<T>;

		template <typename T>
		auto GetMetadata() -> QVector<QVariantMap>;
		template <typename T>
		auto GetMetadata(int index) -> QVariantMap;

		template <typename T>
		auto Count() -> int;
		template <typename T>
		auto IsEmpty() -> bool;
		template <typename T>
		auto Contains() -> bool;

		template <typename T>
		auto Clear() -> void;

	protected:
		virtual auto AddFactory(size_t typeId, const std::shared_ptr<IServiceFactory>& factory) -> void = 0;
		virtual auto RemoveFactory(const std::shared_ptr<IServiceFactory>& factory) -> void = 0;
		virtual auto ClearFactory(size_t typeId) -> void = 0;

		virtual auto GetFactoryTypes() const -> QVector<size_t> = 0;
		virtual auto GetFactories(size_t typeId) const -> QVector<std::shared_ptr<IServiceFactory>> = 0;
		virtual auto GetFactory(size_t typeId, int index) const -> std::shared_ptr<IServiceFactory> = 0;

	private:
		template <typename T>
		auto GetService(const std::shared_ptr<IServiceFactory>& factory) -> std::shared_ptr<T>;
		
		friend class IServiceCollection;
	};

	template <typename T>
	auto IServiceProvider::GetServices() -> QVector<std::shared_ptr<T>> {
		QVector<std::shared_ptr<T>> services;

		if (const auto factories = GetFactories(typeid(T).hash_code()); !factories.isEmpty()) {
			for (const auto& f : factories) {
				if (const auto service = GetService<T>(f))
					services.push_back(service);
			}
		}

		return services;
	}

	template <typename T>
	auto IServiceProvider::GetService() -> std::shared_ptr<T> {
		return GetService<T>(0);
	}

	template <typename T>
	auto IServiceProvider::GetService(int index) -> std::shared_ptr<T> {
		if (const auto factory = GetFactory(typeid(T).hash_code(), index); factory)
			return GetService<T>(factory);

		return nullptr;
	}

	template <typename T>
	auto IServiceProvider::GetMetadata() -> QVector<QVariantMap> {
		QVector<QVariantMap> metadata;

		if (const auto factories = GetFactories(typeid(T).hash_code()); !factories.isEmpty()) {
			for (const auto& f : factories) {
				if (const auto m = f->ReadMetadata(); !m.isEmpty())
					metadata.push_back(m);
			}
		}

		return metadata;
	}

	template <typename T>
	auto IServiceProvider::GetMetadata(int index) -> QVariantMap {
		if (const auto factory = GetFactory(typeid(T).hash_code(), index); factory)
			return factory->ReadMetadata();

		return {};
	}

	template <typename T>
	auto IServiceProvider::Count() -> int {
		return GetFactories(typeid(T).hash_code()).count();
	}

	template <typename T>
	auto IServiceProvider::IsEmpty() -> bool {
		return Count<T>() == 0;
	}

	template <typename T>
	auto IServiceProvider::Contains() -> bool {
		return Count<T>() != 0;
	}

	template <typename T>
	auto IServiceProvider::Clear() -> void {
		ClearFactory(typeid(T).hash_code());
	}

	template <typename T>
	auto IServiceProvider::GetService(const std::shared_ptr<IServiceFactory>& factory) -> std::shared_ptr<T> {
		if (const auto service = factory->CreateService(this); dynamic_cast<T*>(service.get()))
			return std::dynamic_pointer_cast<T>(service);

		return {};
	}
}
