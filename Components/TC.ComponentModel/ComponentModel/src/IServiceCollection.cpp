#include "IServiceCollection.h"

namespace Tomocube {
	auto IServiceCollection::MergeProvider(const IServiceProvider* rhs) const -> void {
		auto* provider = GetProvider();

		for (const auto& t : rhs->GetFactoryTypes()) {
			for (const auto& f : rhs->GetFactories(t))
				provider->AddFactory(t, f);
		}
	}
}
