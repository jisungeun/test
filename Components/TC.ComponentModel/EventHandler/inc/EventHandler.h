#pragma once

#include <QVector>

#include "Event.h"

namespace Tomocube {
	template <typename... T>
	class EventHandler {
	public:
		auto GetCount() const -> int {
			return events.count();
		}

		auto AddEvent(const Event<T...>& event) -> void {
			for (const auto& e : events) {
				if (e.lock() == event.Func().lock())
					return;
			}

			events.push_back(event.Func().lock());
		}

		auto RemoveEvent(const Event<T...>& event) -> void {
			for (auto i = 0; i < events.count(); i++) {
				if (events[i].lock() == event.Func().lock()) {
					events.remove(i);
					break;
				}
			}
		}

		auto Invoke(T... params) -> void {
			for (auto i = 0; i < events.count(); i++) {
				if (events[i].expired())
					events.remove(i--);
				else
					(*events[i].lock())(params...);
			}
		}

		auto operator+=(const Event<T...>& event) -> void {
			AddEvent(event);
		}

		auto operator-=(const Event<T...>& event) -> void {
			RemoveEvent(event);
		}

		auto operator()(T... params) -> void {
			Invoke(params...);
		}

	private:
		QVector<std::weak_ptr<EventDelegate<T...>>> events;
	};

	template <>
	class EventHandler<void> {
	public:
		auto GetCount() const -> int {
			return events.count();
		}

		auto AddEvent(const Event<void>& event) -> void {
			for (const auto& e : events) {
				if (e.lock() == event.Func().lock())
					return;
			}

			events.push_back(event.Func().lock());
		}

		auto RemoveEvent(const Event<void>& event) -> void {
			for (auto i = 0; i < events.count(); i++) {
				if (events[i].lock() == event.Func().lock()) {
					events.remove(i);
					break;
				}
			}
		}

		auto Invoke() -> void {
			for (auto i = 0; i < events.count(); i++) {
				if (events[i].expired())
					events.remove(i--);
				else
					(*events[i].lock())();
			}
		}

		auto operator+=(const Event<void>& event) -> void {
			AddEvent(event);
		}

		auto operator-=(const Event<void>& event) -> void {
			RemoveEvent(event);
		}

		auto operator()() -> void {
			Invoke();
		}

	private:
		QVector<std::weak_ptr<EventDelegateVoid>> events;
	};
}
