#pragma once

#include <functional>
#include <memory>

namespace Tomocube {
	template <typename... T>
	using EventDelegate = std::function<void(T...)>;
	using EventDelegateVoid = std::function<void()>;

	template <typename... T>
	class Event {
	public:
		Event() {
			func = std::make_shared<EventDelegate<T...>>([this](T... args) {
				OnInvoked(args...);
			});
		}

		Event(const Event& event) = default;

		virtual ~Event() = default;

		auto operator=(const EventDelegate<T...>& delegate) -> Event& {
			func = std::make_shared<EventDelegate<T...>>(delegate);
			return *this;
		}

		auto operator=(const Event& event) -> Event& = default;

		auto Func() const -> std::weak_ptr<EventDelegate<T...>> {
			return {};
		}

	protected:
		virtual auto OnInvoked(T... args) -> void {}

	private:
		std::shared_ptr<EventDelegate<T...>> func = nullptr;
	};

	template <>
	class Event<void> {
	public:
		Event() {
			func = std::make_shared<EventDelegateVoid>([this] {
				OnInvoked();
			});
		}

		Event(const Event& event) = default;

		virtual ~Event() = default;

		auto operator=(const EventDelegateVoid& delegate) -> Event& {
			func = std::make_shared<EventDelegateVoid>(delegate);
			return *this;
		}

		auto operator=(const Event& event) -> Event& = default;

		auto Func() const -> std::weak_ptr<EventDelegateVoid> {
			return func;
		}

	protected:
		virtual auto OnInvoked() -> void {}

	private:
		std::shared_ptr<EventDelegateVoid> func = nullptr;
	};
}
