#pragma once

#include <QObject>

#include "IPluginMock.h"
#include "TC.PluginInjection.TestMockExport.h"

class TC_PluginInjection_TestMock_API PluginMock : public QObject, public IPluginMock {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PluginMock" FILE "metadata.json")
	Q_INTERFACES(IPluginMock)

public:
	PluginMock() : QObject(), IPluginMock() { }

	~PluginMock() override = default;

	auto GetImplementedTypeId() -> size_t override {
		return typeid(*this).hash_code();
	}
};

Q_DECLARE_INTERFACE(PluginMock, "PluginMock")
