#pragma once

#include <QMap>

#include "IServiceFactory.h"
#include "IServiceProvider.h"

#include "TC.PluginInjectionExport.h"

namespace Tomocube::PluginInjection {
	class TC_PluginInjection_API PluginProvider final : public IServiceProvider {
	public:
		PluginProvider();
		explicit PluginProvider(const PluginProvider* provider);
		~PluginProvider() override;

		auto AddFactory(size_t typeId, const std::shared_ptr<IServiceFactory>& factory) -> void override;
		auto RemoveFactory(const std::shared_ptr<IServiceFactory>& factory) -> void override;
		auto ClearFactory(size_t typeId) -> void override;

		auto GetFactoryTypes() const -> QVector<size_t> override;
		auto GetFactories(size_t typeId) const -> QVector<std::shared_ptr<IServiceFactory>> override;
		auto GetFactory(size_t typeId, int index) const -> std::shared_ptr<IServiceFactory> override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
