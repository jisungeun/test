#pragma once

#include <memory>

#include <QString>

#include "IServiceFactory.h"
#include "TC.PluginInjectionExport.h"

namespace Tomocube::PluginInjection {
	class TC_PluginInjection_API PluginFactory final : public IServiceFactory {
	public:
		enum class LifeTime {
			Scoped,
			Singleton
		};

		PluginFactory(const QString& filepath, LifeTime time);
		~PluginFactory() override;

		auto IsValid() const -> bool;

		auto CreateService(void* provider) -> std::shared_ptr<IService> override;
		auto ReadMetadata() -> QVariantMap override;
		auto GetTypeId() -> size_t override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
