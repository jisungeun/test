#pragma once

#include <QDir>
#include <QJsonObject>
#include <QMap>

#include "IServiceCollection.h"
#include "PluginFactory.h"
#include "PluginProvider.h"

#include "TC.PluginInjectionExport.h"

namespace Tomocube::PluginInjection {
	class TC_PluginInjection_API PluginCollection final : public IServiceCollection {
	public:
		PluginCollection() = default;
		explicit PluginCollection(const PluginCollection* collection);
		~PluginCollection() override = default;

		template <typename T, typename = std::enable_if_t<std::is_base_of_v<IService, T>, T>>
		auto AddScoped(const QString& filepath) -> PluginCollection*;
		template <typename T, typename = std::enable_if_t<std::is_base_of_v<IService, T>, T>>
		auto AddSingleton(const QString& filepath) -> PluginCollection*;

		auto BuildProvider() -> std::shared_ptr<IServiceProvider> override;

	protected:
		auto GetProvider() const -> IServiceProvider* override;

		template <typename T>
		auto AddService(const QString& filepath, PluginFactory::LifeTime time) const -> void;

	private:
		std::shared_ptr<PluginProvider> provider = std::make_shared<PluginProvider>();
	};

	template <typename T, typename>
	auto PluginCollection::AddScoped(const QString& filepath) -> PluginCollection* {
		AddService<T>(filepath, PluginFactory::LifeTime::Scoped);
		return this;
	}

	template <typename T, typename>
	auto PluginCollection::AddSingleton(const QString& filepath) -> PluginCollection* {
		AddService<T>(filepath, PluginFactory::LifeTime::Singleton);
		return this;
	}

	template <typename T>
	auto PluginCollection::AddService(const QString& filepath, PluginFactory::LifeTime time) const -> void {
		const auto typeId = typeid(T).hash_code();

		if (const auto factory = std::make_shared<PluginFactory>(filepath, time); factory->IsValid())
			provider->AddFactory(typeId, factory);
	}
}
