#include "PluginCollection.h"

#include "PluginProvider.h"

namespace Tomocube::PluginInjection {
	PluginCollection::PluginCollection(const PluginCollection* collection) : IServiceCollection(), provider(collection->provider) {}

	auto PluginCollection::BuildProvider() -> std::shared_ptr<IServiceProvider> {
		return provider;
	}

	auto PluginCollection::GetProvider() const -> IServiceProvider* {
		return provider.get();
	}
}
