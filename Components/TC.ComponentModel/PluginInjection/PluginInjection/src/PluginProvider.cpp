#include "PluginProvider.h"

namespace Tomocube::PluginInjection {
	struct PluginProvider::Impl {
		QMap<size_t, QVector<std::shared_ptr<IServiceFactory>>> factories;
	};

	PluginProvider::PluginProvider() : IServiceProvider(), d(new Impl) {}

	PluginProvider::PluginProvider(const PluginProvider* provider) : IServiceProvider(), d(new Impl) {
		d->factories = provider->d->factories;
	}

	PluginProvider::~PluginProvider() = default;

	auto PluginProvider::AddFactory(size_t typeId, const std::shared_ptr<IServiceFactory>& factory) -> void {
		if (!d->factories[typeId].contains(factory))
			d->factories[typeId].push_back(factory);
	}

	auto PluginProvider::RemoveFactory(const std::shared_ptr<IServiceFactory>& factory) -> void {
		for (const auto key : d->factories.keys())
			d->factories[key].removeAll(factory);
	}

	auto PluginProvider::ClearFactory(size_t typeId) -> void {
		d->factories.remove(typeId);
	}

	auto PluginProvider::GetFactoryTypes() const -> QVector<size_t> {
		return d->factories.keys().toVector();
	}

	auto PluginProvider::GetFactories(size_t typeId) const -> QVector<std::shared_ptr<IServiceFactory>> {
		if (d->factories.contains(typeId))
			return d->factories[typeId];

		return {};
	}

	auto PluginProvider::GetFactory(size_t typeId, int index) const -> std::shared_ptr<IServiceFactory> {
		if (const auto factories = GetFactories(typeId); factories.count() > index)
			return factories[index];

		return {};
	}
}
