#include <QPluginLoader>

#include "PluginFactory.h"

#include <QFile>

#include "IServiceProvider.h"

namespace Tomocube::PluginInjection {
	struct PluginFactory::Impl {
		IService* instance = nullptr;
		int refCount = 0;
		LifeTime time = LifeTime::Scoped;

		QString filepath;

		struct ScopedDeleter {
			IService** instance = nullptr;
			int* refCount = nullptr;

			auto operator()(IService* const&) const noexcept -> void {
				if (--(*refCount) == 0) {
					delete (*instance);
					*instance = nullptr;
				}
			}
		};

		struct SingletonDeleter {
			auto operator()(IService* const&) const noexcept -> void {}
		};
	};

	PluginFactory::PluginFactory(const QString& filepath, LifeTime time) : IServiceFactory(), d(new Impl) {
		d->time = time;
		d->filepath = filepath;
	}

	PluginFactory::~PluginFactory() = default;

	auto PluginFactory::IsValid() const -> bool {
		return QFile::exists(d->filepath) && QLibrary::isLibrary(d->filepath);
	}

	auto PluginFactory::CreateService(void* provider) -> std::shared_ptr<IService> {
		IService* instance = nullptr;
		QPluginLoader loader(d->filepath);

		if (!QLibrary::isLibrary(d->filepath))
			return {};

		auto* service = loader.instance();

		if (instance = dynamic_cast<IService*>(service); !instance) {
			delete service;
			return {};
		}

		switch (d->time) {
			case LifeTime::Scoped:
				if (instance)
					d->instance = instance;
				return std::shared_ptr<IService>(d->instance, Impl::ScopedDeleter { &d->instance, &(++d->refCount) });
			case LifeTime::Singleton:
				if (instance)
					d->instance = instance;
				return std::shared_ptr<IService>(d->instance, Impl::SingletonDeleter {});
		}

		return {};
	}

	auto PluginFactory::ReadMetadata() -> QVariantMap {
		const QPluginLoader loader(d->filepath);

		if (loader.metaData().contains("MetaData"))
			return loader.metaData()["MetaData"].toObject().toVariantMap();
		return {};
	}

	auto PluginFactory::GetTypeId() -> size_t {
		return 0;
	}
}
