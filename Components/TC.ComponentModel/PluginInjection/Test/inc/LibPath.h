#pragma once

#include <QApplication>
#include <QString>

inline QCoreApplication app(*(new int(0)), nullptr);
static inline auto FILEPATH = QString("%1%3").arg(QCoreApplication::applicationDirPath()).arg("/TC.PluginInjection.TestMock.dll");
