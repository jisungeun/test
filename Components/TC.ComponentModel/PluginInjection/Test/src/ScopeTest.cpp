#include <QCoreApplication>
#include <catch2/catch.hpp>

#include "IPluginMock.h"
#include "LibPath.h"

#include "PluginCollection.h"

using namespace Tomocube;
using namespace PluginInjection;

/*
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 *
 * MUST BUILD "TA_PluginInjection_PluginInjectionTestMock" BEFORE THE TEST.
 */

TEST_CASE("Plugin Provider can load and create instance") {
	SECTION("provider gives instance by interface type") {
		PluginCollection collection;
		collection.AddScoped<IPluginMock>(FILEPATH);
		const auto provider = collection.BuildProvider();
		const auto mock = provider->GetService<IPluginMock>();

		REQUIRE(mock->Exists());
	}

	SECTION("scoped instances share same pointer and get deleted both") {
		PluginCollection collection;
		collection.AddScoped<IPluginMock>(FILEPATH);
		const auto provider = collection.BuildProvider();
		auto isDeleted = false;

		{
			const auto mock1 = provider->GetService<IPluginMock>();
			const auto mock2 = provider->GetService<IPluginMock>();
			mock1->SetDeleted(&isDeleted);

			REQUIRE(!isDeleted);
			REQUIRE(mock1 == mock2);
		}

		REQUIRE(isDeleted);
	}

	SECTION("singleton instances share same pointer and never get deleted") {
		PluginCollection collection;
		collection.AddSingleton<IPluginMock>(FILEPATH);
		const auto provider = collection.BuildProvider();
		auto isDeleted = false;

		{
			const auto mock1 = provider->GetService<IPluginMock>();
			const auto mock2 = provider->GetService<IPluginMock>();
			mock1->SetDeleted(&isDeleted);

			REQUIRE(!isDeleted);
			REQUIRE(mock1 == mock2);
		}

		REQUIRE(!isDeleted);
	}
}
