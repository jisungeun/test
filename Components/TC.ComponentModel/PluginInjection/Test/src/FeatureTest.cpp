#include <catch2/catch.hpp>

#include "IPluginMock.h"
#include "LibPath.h"

#include "PluginCollection.h"

using namespace Tomocube;
using namespace PluginInjection;

/*
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 * WARNING!!!
 *
 * MUST BUILD "TA_DependencyInjection_PluginInjectionTestMock" BEFORE THE TEST.
 */

TEST_CASE("Service Provider works well") {
	std::shared_ptr<IServiceProvider> provider = nullptr;

	{
		PluginCollection collection;

		collection.AddScoped<IPluginMock>(FILEPATH);

		provider = collection.BuildProvider();
	}

	SECTION("provider can clear services by type") {
		provider->Clear<IPluginMock>();
		const auto mock = provider->GetService<IPluginMock>();

		REQUIRE(mock == nullptr);
	}

	SECTION("provider shows count of its services") {
		REQUIRE(provider->Contains<IPluginMock>());
		REQUIRE(!provider->IsEmpty<IPluginMock>());
		REQUIRE(provider->Count<IPluginMock>() == 1);
		REQUIRE(!provider->Contains<int>());
		REQUIRE(provider->IsEmpty<int>());
		REQUIRE(provider->Count<int>() == 0);
	}

	SECTION("provider can get metadata") {
		auto metadata = provider->GetMetadata<IPluginMock>();

		// PluginMock doesn't have metadata while PluginMockB has one.
		REQUIRE(metadata.count() == 1);
		REQUIRE(metadata[0]["Name"] == "Helloworld");
	}
}
