#include <catch2/catch.hpp>

#include "ServiceMock.h"

#include "ServiceCollection.h"

using namespace Tomocube;
using namespace DependencyInjection;

TEST_CASE("Service Provider can create temporary instance") {
	std::shared_ptr<IServiceProvider> provider = nullptr;

	{
		ServiceCollection collection;

		collection.AddTransient<ServiceMockA, IServiceMock>();

		provider = collection.BuildProvider();
	}

	SECTION("provider gives instance by interface type") {
		const auto mock = provider->GetService<IServiceMock>();

		REQUIRE(mock->Exists());
	}

	SECTION("provider gives instance by type of itself") {
		const auto mock = provider->GetService<ServiceMockA>();

		REQUIRE(mock->Exists());
	}

	SECTION("provider creates instance for single request") {
		IServiceMock::ClearCreationCount();

		provider->GetService<ServiceMockA>();
		provider->GetService<ServiceMockA>();
		provider->GetService<ServiceMockA>();

		REQUIRE(IServiceMock::CreationCount() == 3);
	}

	SECTION("provider gives implemented instance") {
		const auto mock1 = provider->GetService<IServiceMock>();
		const auto mock2 = provider->GetService<ServiceMockA>();
		const auto mock3 = provider->GetService<ServiceMockA>();

		REQUIRE(mock1->GetImplementedTypeId() == typeid(ServiceMockA).hash_code());
		REQUIRE(mock2->GetImplementedTypeId() == typeid(ServiceMockA).hash_code());
		REQUIRE(mock3->GetImplementedTypeId() == typeid(ServiceMockA).hash_code());
	}

	SECTION("given instance gets deleted as out of scope") {
		auto deleted = false;

		{
			const auto mock = provider->GetService<IServiceMock>();
			mock->SetDeleted(&deleted);
		}

		REQUIRE(deleted);
	}

	SECTION("given instance contains provider") {
		const auto mock = provider->GetService<IServiceMock>();
		REQUIRE(mock->IsProviderProvided());
	}
}
