#include <catch2/catch.hpp>

#include "ServiceMock.h"

#include "ServiceCollection.h"

using namespace Tomocube;
using namespace DependencyInjection;

TEST_CASE("Service Provider can create scoped instance") {
	std::shared_ptr<IServiceProvider> provider = nullptr;

	{
		ServiceCollection collection;

		collection.AddScoped<ServiceMockA, IServiceMock>();

		provider = collection.BuildProvider();
	}

	SECTION("provider gives instance by interface type") {
		const auto mock = provider->GetService<IServiceMock>();

		REQUIRE(mock->Exists());
	}

	SECTION("provider gives instance by type of itself") {
		const auto mock = provider->GetService<ServiceMockA>();

		REQUIRE(mock->Exists());
	}

	SECTION("provider creates instance once") {
		IServiceMock::ClearCreationCount();

		const auto mock1 = provider->GetService<ServiceMockA>();
		const auto mock2 = provider->GetService<ServiceMockA>();
		const auto mock3 = provider->GetService<ServiceMockA>();

		REQUIRE(IServiceMock::CreationCount() == 1);
	}

	SECTION("provider creates instance on single request") {
		IServiceMock::ClearCreationCount();

		provider->GetService<ServiceMockA>();
		provider->GetService<ServiceMockA>();
		provider->GetService<ServiceMockA>();

		REQUIRE(IServiceMock::CreationCount() == 3);
	}

	SECTION("provider gives implemented instance") {
		const auto mock1 = provider->GetService<IServiceMock>();
		const auto mock2 = provider->GetService<ServiceMockA>();

		REQUIRE(mock1->GetImplementedTypeId() == typeid(ServiceMockA).hash_code());
		REQUIRE(mock2->GetImplementedTypeId() == typeid(ServiceMockA).hash_code());
	}

	SECTION("given instance gets deleted within getting out of scope") {
		auto deleted = false;

		{
			const auto mock = provider->GetService<IServiceMock>();
			mock->SetDeleted(&deleted);
		}

		REQUIRE(deleted);
	}

	SECTION("given instance gets deleted within all references get deleted") {
		auto deleted = false;

		{
			const auto mock1 = provider->GetService<IServiceMock>();
			const auto mock2 = provider->GetService<IServiceMock>();

			mock2->SetDeleted(&deleted);
		}

		REQUIRE(deleted);
	}

	SECTION("given instances points the same in scope") {
		const auto mock1 = provider->GetService<IServiceMock>();
		const auto mock2 = provider->GetService<ServiceMockA>();

		REQUIRE(mock1 == mock2);
	}

	SECTION("given instance contains provider") {
		const auto mock = provider->GetService<IServiceMock>();
		REQUIRE(mock->IsProviderProvided());
	}
}
