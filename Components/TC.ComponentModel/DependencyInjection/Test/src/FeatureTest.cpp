#include <catch2/catch.hpp>

#include "ServiceMock.h"

#include "ServiceCollection.h"

using namespace Tomocube;
using namespace DependencyInjection;

TEST_CASE("Service Provider works well") {
	std::shared_ptr<IServiceProvider> provider = nullptr;

	{
		ServiceCollection collection;

		collection.AddScoped<ServiceMockA, IServiceMock>();

		provider = collection.BuildProvider();
	}

	SECTION("provider can clear services by type") {
		provider->Clear<IServiceMock>();
		const auto mock = provider->GetService<IServiceMock>();

		REQUIRE(mock == nullptr);
	}

	SECTION("provider shows count of its services") {
		REQUIRE(provider->Contains<IServiceMock>());
		REQUIRE(!provider->IsEmpty<IServiceMock>());
		REQUIRE(provider->Count<IServiceMock>() == 1);
		REQUIRE(!provider->Contains<int>());
		REQUIRE(provider->IsEmpty<int>());
		REQUIRE(provider->Count<int>() == 0);
	}

	SECTION("provider can merge other provider") {
		ServiceCollection collection;
		collection.MergeProvider(provider.get());
		collection.AddScoped<ServiceMockA, IServiceMock>()
				->AddScoped<ServiceMockB, IServiceMock>();
		auto rhs = collection.BuildProvider();

		REQUIRE(rhs->Contains<IServiceMock>());
		REQUIRE(!rhs->IsEmpty<IServiceMock>());
		REQUIRE(rhs->Count<IServiceMock>() == 2);
		REQUIRE(rhs->Contains<ServiceMockB>());
		REQUIRE(!rhs->IsEmpty<ServiceMockB>());
		REQUIRE(rhs->Count<ServiceMockB>() == 1);
		REQUIRE(!rhs->Contains<int>());
		REQUIRE(rhs->IsEmpty<int>());
		REQUIRE(rhs->Count<int>() == 0);
	}

	SECTION("provider can get metadata") {
		ServiceCollection collection;
		collection.MergeProvider(provider.get());
		collection.AddScoped<ServiceMockB, IServiceMock>();
		auto rhs = collection.BuildProvider();
		auto metadata = rhs->GetMetadata<IServiceMock>();

		// ServiceMockA doesn't have metadata while ServiceMockB has one.
		REQUIRE(metadata.count() == 1);
		REQUIRE(metadata[0]["Name"] == "ServiceMockB");
	}
}
