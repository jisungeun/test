#pragma once

#include "IServiceMock.h"

class ServiceMockA : public IServiceMock {
public:
	using IServiceMock::IServiceMock;
	~ServiceMockA() override = default;

	auto GetImplementedTypeId() -> size_t override {
		return typeid(*this).hash_code();
	}
};

class ServiceMockB : public IServiceMock {
public:
	using IServiceMock::IServiceMock;
	~ServiceMockB() override = default;

	auto GetImplementedTypeId() -> size_t override {
		return typeid(*this).hash_code();
	}

	static auto ServiceMetadata() -> QVariantMap {
		QVariantMap metadata;
		metadata["Name"] = "ServiceMockB";

		return metadata;
	}
};
