#pragma once

#include <QMutexLocker>

#include "IService.h"
#include "IServiceProvider.h"

static int count = 0;
static QMutex mutex;

class IServiceMock : public Tomocube::IService {
public:
	explicit IServiceMock(Tomocube::IServiceProvider* provider) : IService(), provider(provider) {
		QMutexLocker locker(&mutex);
		this->memoryCheckValue = 1234;
		count++;
	}

	~IServiceMock() override {
		if (deleted)
			*deleted = true;

		memoryCheckValue = -1;
	}

	auto Init() -> void {
		this->init = true;
	}

	auto SetDeleted(bool* deleted) -> void {
		this->deleted = deleted;
	}

	auto SetProvider(Tomocube::IServiceProvider* provider) -> void {
		if (provider) {
			this->provider = provider;
			this->providerProvided = true;
		}
	}

	auto Exists() const -> bool {
		return memoryCheckValue == 1234;
	}

	auto IsInitted() const -> bool {
		return this->init;
	}

	auto IsProviderProvided() const -> bool {
		return provider;
	}

	auto IsProviderProvidedByInitializer() const -> bool {
		return provider && this->providerProvided;
	}

	static auto CreationCount() -> int {
		return count;
	}

	static auto ClearCreationCount() -> void {
		QMutexLocker locker(&mutex);
		count = 0;
	}

	virtual auto GetImplementedTypeId() -> size_t = 0;

private:
	int memoryCheckValue = -1;
	bool* deleted = nullptr;
	bool init = false;
	bool providerProvided = false;

	Tomocube::IServiceProvider* provider = nullptr;
};
