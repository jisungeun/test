#include "ServiceCollection.h"

#include "ServiceProvider.h"

namespace Tomocube::DependencyInjection {
	ServiceCollection::~ServiceCollection() = default;

	ServiceCollection::ServiceCollection() : IServiceCollection() { }

	ServiceCollection::ServiceCollection(const ServiceCollection* collection) : IServiceCollection(), provider(collection->provider), factories(collection->factories) { }

	auto ServiceCollection::BuildProvider() -> std::shared_ptr<IServiceProvider> {
		return provider;
	}

	auto ServiceCollection::GetProvider() const -> IServiceProvider* {
		return provider.get();
	}
}
