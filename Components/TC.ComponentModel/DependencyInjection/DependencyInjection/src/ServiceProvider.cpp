#include "ServiceProvider.h"

namespace Tomocube::DependencyInjection {
	struct ServiceProvider::Impl {
		QMap<size_t, QVector<std::shared_ptr<IServiceFactory>>> factories;
	};

	ServiceProvider::ServiceProvider() : IServiceProvider(), d(new Impl) {}

	ServiceProvider::ServiceProvider(const ServiceProvider* provider) : IServiceProvider(), d(new Impl) {
		d->factories = provider->d->factories;
	}

	ServiceProvider::~ServiceProvider() = default;

	auto ServiceProvider::AddFactory(size_t typeId, const std::shared_ptr<IServiceFactory>& factory) -> void {
		if (!d->factories[typeId].contains(factory) &&
			!std::any_of(d->factories[typeId].begin(), d->factories[typeId].end(), [factory](const std::shared_ptr<IServiceFactory>& f) {
				return f->GetTypeId() == factory->GetTypeId();
			}))
			d->factories[typeId].push_back(factory);

		if (!d->factories[factory->GetTypeId()].contains(factory) &&
			!std::any_of(d->factories[factory->GetTypeId()].begin(), d->factories[factory->GetTypeId()].end(), [factory](const std::shared_ptr<IServiceFactory>& f) {
				return f->GetTypeId() == factory->GetTypeId();
			}))
			d->factories[factory->GetTypeId()].push_back(factory);
	}

	auto ServiceProvider::RemoveFactory(const std::shared_ptr<IServiceFactory>& factory) -> void {
		for (const auto key : d->factories.keys())
			d->factories[key].removeAll(factory);
	}

	auto ServiceProvider::ClearFactory(size_t typeId) -> void {
		d->factories.remove(typeId);
	}

	auto ServiceProvider::GetFactoryTypes() const -> QVector<size_t> {
		return d->factories.keys().toVector();
	}

	auto ServiceProvider::GetFactories(size_t typeId) const -> QVector<std::shared_ptr<IServiceFactory>> {
		if (d->factories.contains(typeId))
			return d->factories[typeId];

		return {};
	}

	auto ServiceProvider::GetFactory(size_t typeId, int index) const -> std::shared_ptr<IServiceFactory> {
		if (const auto factories = GetFactories(typeId); factories.count() > index)
			return factories[index];

		return {};
	}
}
