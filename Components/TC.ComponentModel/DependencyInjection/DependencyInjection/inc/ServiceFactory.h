#pragma once

#include <QMutex>

#include "IServiceFactory.h"

namespace Tomocube::DependencyInjection {
	template <typename T>
	class ContainsMetadata {
		using ok = char;

		struct no {
			char x[2];
		};

		template <typename C>
		static auto check(decltype(&C::ServiceMetadata)) -> ok;
		template <typename C>
		static auto check(...) -> no;

	public:
		enum { value = sizeof(check<T>(0)) == sizeof(char) };
	};

	enum class LifeTime {
		Transient,
		Scoped,
		Singleton
	};

	template <typename T>
	class ServiceFactory final : public IServiceFactory {
	public:
		explicit ServiceFactory(LifeTime time);

		auto CreateService(void* provider) -> std::shared_ptr<IService> override;
		auto ReadMetadata() -> QVariantMap override;
		auto GetTypeId() -> size_t override;

	private:
		LifeTime time;
		int refCount = 0;
		QMutex mutex;
		IService* instance = nullptr;

		struct SingletonDeleter {
			auto operator()(IService* const&) const noexcept -> void {}
		};

		struct ScopedDeleter {
			IService*& instance;
			int& refCount;
			QMutex& mutex;

			ScopedDeleter(IService*& instance, int& refCount, QMutex& mutex) : instance(instance), refCount(refCount), mutex(mutex) {
				QMutexLocker locker(&mutex);
				++refCount;
			}

			auto operator()(IService* const&) const noexcept -> void {
				QMutexLocker locker(&mutex);

				if (--(refCount) == 0) {
					delete instance;
					instance = nullptr;
				}
			}
		};
	};

	template <typename T>
	ServiceFactory<T>::ServiceFactory(LifeTime time) : IServiceFactory(), time(time) {}

	template <typename T>
	auto ServiceFactory<T>::CreateService(void* provider) -> std::shared_ptr<IService> {
		T* instance = nullptr;

		if (time == LifeTime::Transient || (time != LifeTime::Transient && !this->instance)) {
			if constexpr (std::is_constructible_v<T, IServiceProvider*>)
				instance = new T(static_cast<IServiceProvider*>(provider));
			else
				instance = new T;
		}

		switch (time) {
			case LifeTime::Transient:
				return std::shared_ptr<IService>(instance);
			case LifeTime::Scoped:
				this->instance = (instance) ? instance : this->instance;
				return std::shared_ptr<IService>(this->instance, ScopedDeleter { this->instance, refCount, mutex });
			case LifeTime::Singleton:
				this->instance = (instance) ? instance : this->instance;
				return std::shared_ptr<IService>(this->instance, SingletonDeleter());
		}

		return {};
	}

	template <typename T>
	auto ServiceFactory<T>::ReadMetadata() -> QVariantMap {
		if constexpr (ContainsMetadata<T>::value)
			return T::ServiceMetadata();

		return {};
	}

	template <typename T>
	auto ServiceFactory<T>::GetTypeId() -> size_t {
		return typeid(T).hash_code();
	}
}
