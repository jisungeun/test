#pragma once

#include "IServiceCollection.h"

#include "ServiceFactory.h"
#include "ServiceProvider.h"

#include "TC.DependencyInjectionExport.h"

namespace Tomocube::DependencyInjection {
	class TC_DependencyInjection_API ServiceCollection final : public IServiceCollection {
	public:
		ServiceCollection();
		explicit ServiceCollection(const ServiceCollection* collection);
		~ServiceCollection() override;

		template <typename T, typename... K, typename = std::conjunction<std::enable_if_t<std::is_base_of_v<IService, T>, T>, std::enable_if_t<(std::is_base_of_v<K, T> && ...)>>>
		auto AddTransient() -> ServiceCollection*;
		template <typename T, typename... K, typename = std::conjunction<std::enable_if_t<std::is_base_of_v<IService, T>, T>, std::enable_if_t<(std::is_base_of_v<K, T> && ...)>>>
		auto AddScoped() -> ServiceCollection*;
		template <typename T, typename... K, typename = std::conjunction<std::enable_if_t<std::is_base_of_v<IService, T>, T>, std::enable_if_t<(std::is_base_of_v<K, T> && ...)>>>
		auto AddSingleton() -> ServiceCollection*;

		template <typename T>
		auto Remove() -> ServiceCollection*;
		
		auto BuildProvider() -> std::shared_ptr<IServiceProvider> override;

	protected:
		auto GetProvider() const -> IServiceProvider* override;
		
		template <typename T>
		auto CreateFactory() -> std::shared_ptr<IServiceFactory>;

		template <typename T, typename K>
		auto AddService(LifeTime time) -> void;

	private:
		std::shared_ptr<ServiceProvider> provider = std::make_shared<ServiceProvider>();

		QMap<size_t, std::shared_ptr<IServiceFactory>> factories;
	};


	template <typename T, typename... K, typename>
	auto ServiceCollection::AddTransient() -> ServiceCollection* {
		(AddService<T, K>(LifeTime::Transient), ...);
		return this;
	}

	template <typename T, typename... K, typename>
	auto ServiceCollection::AddScoped() -> ServiceCollection* {
		(AddService<T, K>(LifeTime::Scoped), ...);
		return this;
	}

	template <typename T, typename... K, typename>
	auto ServiceCollection::AddSingleton() -> ServiceCollection* {
		(AddService<T, K>(LifeTime::Singleton), ...);
		return this;
	}

	template <typename T>
	auto ServiceCollection::Remove() -> ServiceCollection* {
		const auto implId = typeid(T).hash_code();

		if (factories.contains(implId)) {
			provider->RemoveFactory(factories[implId]);
			factories.remove(implId);
		}

		return this;
	}

	template <typename T>
	auto ServiceCollection::CreateFactory() -> std::shared_ptr<IServiceFactory> {
		if (const auto implID = typeid(T).hash_code(); !factories.contains(implID)) {
			auto factory = std::make_shared<ServiceFactory<T>>(time);

			factories[implID] = factory;
			return factory;
		}

		return {};
	}

	template <typename T, typename K>
	auto ServiceCollection::AddService(LifeTime time) -> void {
		const auto baseID = typeid(K).hash_code();
		const auto implID = typeid(T).hash_code();

		if (!factories.contains(implID))
			factories[implID] = std::make_shared<ServiceFactory<T>>(time);

		provider->AddFactory(baseID, factories[implID]);
	}
}
