#pragma once

#include <QMap>

#include "IServiceFactory.h"
#include "IServiceProvider.h"

#include "TC.DependencyInjectionExport.h"

namespace Tomocube::DependencyInjection {
	class TC_DependencyInjection_API ServiceProvider final : public IServiceProvider {
	public:
		ServiceProvider();
		explicit ServiceProvider(const ServiceProvider* provider);
		~ServiceProvider() override;

		auto AddFactory(size_t typeId, const std::shared_ptr<IServiceFactory>& factory) -> void override;
		auto RemoveFactory(const std::shared_ptr<IServiceFactory>& factory) -> void override;
		auto ClearFactory(size_t typeId) -> void override;
		
		auto GetFactoryTypes() const -> QVector<size_t> override;
		auto GetFactories(size_t typeId) const -> QVector<std::shared_ptr<IServiceFactory>> override;
		auto GetFactory(size_t typeId, int index) const -> std::shared_ptr<IServiceFactory> override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
