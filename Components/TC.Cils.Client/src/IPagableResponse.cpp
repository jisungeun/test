#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "IPagableResponse.h"

namespace TC::Cils::Client {
	struct IPagableResponse::Impl {
		int pageLen = -1;
		int currentPage = -1;
	};

	IPagableResponse::IPagableResponse(QObject* parent) : IResponse(parent), d(new Impl) {}

	IPagableResponse::~IPagableResponse() = default;

	auto IPagableResponse::GetPageLength() const -> int {
		return d->pageLen;
	}

	auto IPagableResponse::GetCurrentPage() const -> int {
		return d->currentPage;
	}

    auto IPagableResponse::Deserialize(const QJsonDocument& doc) -> bool {
		const auto obj = doc.object();
		const auto total = obj["totalCount"].toInt();
		const double pageSize = obj["pageSize"].toInt();

		d->currentPage = obj["page"].toInt();
		d->pageLen = (total == 0) ? 0 : static_cast<int>(std::ceil(total / pageSize));

		const auto arr = obj["list"].toArray();
		return DeserializeList(arr);
    }
}
