#include "ImageResponse.h"

namespace TC::Cils::Client {
	struct ImageResponse::Impl {
		QByteArray image;
	};

	ImageResponse::ImageResponse(QObject* parent) : IResponse(parent), d(new Impl{ }) {}

	ImageResponse::~ImageResponse() = default;

	auto ImageResponse::GetImage() const -> const QByteArray& {
		return d->image;
	}

	auto ImageResponse::Initialize(const QByteArray& contents, int errorCode) -> void {
		const auto doc = QJsonDocument::fromJson(contents);

		if (errorCode == 0 || errorCode == 200)
			d->image = contents;
		else
			SetHttpError(errorCode);

		emit Initialized();
	}

    auto ImageResponse::Deserialize(const QJsonDocument& doc) -> bool {
		return false;
    }
}
