#include "IResponse.h"

namespace TC::Cils::Client {
	struct IResponse::Impl {
		bool initialized = false;

		std::optional<Error> error;
	};

	IResponse::IResponse(QObject* parent) : QObject(parent), d(new Impl{ }) {}

	IResponse::~IResponse() = default;

	auto IResponse::IsError() const -> bool {
		return d->error.has_value();
	}

	auto IResponse::GetError() const -> const Error* {
		if (d->error.has_value())
			return &d->error.value();
		else
			return nullptr;
	}

	auto IResponse::IsInitialized() const -> bool {
		return d->initialized;
	}

	auto IResponse::Initialize(int errorCode) -> void {
		d->initialized = true;
		d->error = Error();

		QJsonObject errorJson;
		errorJson["status"] = errorCode;
		errorJson["code"] = errorCode;
		errorJson["message"] = "HTTP Error";

		d->error->Deserialize(errorJson);

		emit Initialized();
	}

	auto IResponse::Initialize(const QByteArray& contents, int errorCode) -> void {
		const auto doc = QJsonDocument::fromJson(contents);
		const auto obj = doc.object();

		if (errorCode == 200 || errorCode == 0) {
			if (!Deserialize(doc)) {
				Error error;
				error.Deserialize(obj);

				if (error.IsInitialized())
					d->error = error;
			}
		} else {
			Error error;

			if (error.Deserialize(obj))
				d->error = error;
			else
				SetHttpError(errorCode, contents);
		}

		d->initialized = true;

		emit Initialized();
	}

	auto IResponse::SetHttpError(int errorCode, const QString& message) -> void {
		Error error;
		QJsonObject errorJson;

		errorJson["status"] = errorCode;
		errorJson["code"] = errorCode;
		errorJson["message"] = message;

		error.Deserialize(errorJson);
		d->error = std::move(error);
	}

    auto IResponse::SetHttpError(int errorCode) -> void {
		SetHttpError(errorCode, GetHttpErrorMessage(errorCode));
    }

    auto IResponse::GetHttpErrorMessage(int errorCode) -> QString {
		switch (errorCode) {
		case 100: return QStringLiteral("Continue");
		case 101: return QStringLiteral("Switching Protocols");
		case 102: return QStringLiteral("Processing");
		case 103: return QStringLiteral("Early Hints");
			
		case 200: return QStringLiteral("OK");
		case 201: return QStringLiteral("Created");
		case 202: return QStringLiteral("Accepted");
		case 203: return QStringLiteral("Non-Authoritative Information");
		case 204: return QStringLiteral("No Content");
		case 205: return QStringLiteral("Reset Content");
		case 206: return QStringLiteral("Partial Content");
		case 207: return QStringLiteral("Multi-Status");
		case 208: return QStringLiteral("Already Reported");
		case 226: return QStringLiteral("IM Used");
			
		case 300: return QStringLiteral("Multiple Choices");
		case 301: return QStringLiteral("Moved Permanently");
		case 302: return QStringLiteral("Found");
		case 303: return QStringLiteral("See Other");
		case 304: return QStringLiteral("Not Modified");
		case 305: return QStringLiteral("Use Proxy");
		case 307: return QStringLiteral("Temporary Redirect");
		case 308: return QStringLiteral("Permanent Redirect");
			
		case 400: return QStringLiteral("Bad Request");
		case 401: return QStringLiteral("Unauthorized");
		case 402: return QStringLiteral("Payment Required");
		case 403: return QStringLiteral("Forbidden");
		case 404: return QStringLiteral("Not Found");
		case 405: return QStringLiteral("Method Not Allowed");
		case 406: return QStringLiteral("Not Acceptable");
		case 407: return QStringLiteral("Proxy Authentication Required");
		case 408: return QStringLiteral("Request Timeout");
		case 409: return QStringLiteral("Conflict");
		case 410: return QStringLiteral("Gone");
		case 411: return QStringLiteral("Length Required");
		case 412: return QStringLiteral("Precondition Failed");
		case 413: return QStringLiteral("Content Too Large");
		case 414: return QStringLiteral("URI Too Long");
		case 415: return QStringLiteral("Unsupported Media Type");
		case 416: return QStringLiteral("Range Not Satisfiable");
		case 417: return QStringLiteral("Expectation Failed");
		case 418: return QStringLiteral("I'm a teapot");
		case 421: return QStringLiteral("Misdirected Request");
		case 422: return QStringLiteral("Unprocessable Content");
		case 423: return QStringLiteral("Locked");
		case 424: return QStringLiteral("Failed Dependency");
		case 425: return QStringLiteral("Too Early");
		case 426: return QStringLiteral("Upgrade Required");
		case 428: return QStringLiteral("Precondition Required");
		case 429: return QStringLiteral("Too Many Requests");
		case 431: return QStringLiteral("Request Header Fields Too Large");
		case 451: return QStringLiteral("Unavailable For Legal Reasons");
			
		case 500: return QStringLiteral("Internal Server Error");
		case 501: return QStringLiteral("Not Implemented");
		case 502: return QStringLiteral("Bad Gateway");
		case 503: return QStringLiteral("Service Unavailable");
		case 504: return QStringLiteral("Gateway Timeout");
		case 505: return QStringLiteral("HTTP Version Not Supported");
		case 506: return QStringLiteral("Variant Also Negotiates");
		case 507: return QStringLiteral("Insufficient Storage");
		case 508: return QStringLiteral("Loop Detected");
		case 510: return QStringLiteral("Not Extended");
		case 511: return QStringLiteral("Network Authentication Required");

		default: return {};
		}
	}
}
