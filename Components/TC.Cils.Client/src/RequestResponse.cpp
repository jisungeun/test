#include <QJsonDocument>

#include "RequestResponse.h"

namespace TC::Cils::Client {
	struct RequestResponse::Impl {
		bool done = false;
	};

	RequestResponse::RequestResponse(QObject* parent) : IResponse(parent), d(new Impl) {}

	RequestResponse::~RequestResponse() = default;

	auto RequestResponse::IsDone() const -> bool {
		return d->done;
	}

	auto RequestResponse::Deserialize(const QJsonDocument& doc) -> bool {
		const auto obj = doc.object();
		return d->done = (obj["ok"].toInt() == 1);
	}
}
