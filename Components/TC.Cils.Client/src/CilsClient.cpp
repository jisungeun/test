#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>

#include "CilsClient.h"
#include "HttpClient.h"

namespace TC::Cils::Client {
	struct CilsClient::Impl {
		HttpClient client;

		template<typename T>
		static auto Connect(QNetworkReply* reply, T* response) -> T* {
			connect(reply, &QNetworkReply::finished,
				[reply, response] () -> void {
					response->Initialize(reply->readAll(), reply->error());
					reply->close();
					delete reply;
				}
			);

			return response;
		}
	};

	CilsClient::CilsClient(QObject* parent) : QObject(parent), d(new Impl) {
		d->client.SetBaseAddress("http://localhost:4000");
	}

	CilsClient::~CilsClient() = default;

	auto CilsClient::GetLoginInfo() -> SingleResponse<User>* {
		auto* reply = d->client.Get("/loginInfo");
		auto* response = new SingleResponse<User>("loginInfo", this);

		return d->Connect<SingleResponse<User>>(reply, response);
	}

	auto CilsClient::GetProjects(const QString* filter, int page) -> ListResponse<Project>* {
		QMap<QString, QString> params = { { "page", QString::number(page)} };
		if (filter)
			params["filter"] = *filter;

		auto* reply = d->client.Get("/projects", params);

		auto* response = new ListResponse<Project>(this);
		return d->Connect<ListResponse<Project>>(reply, response);
	}

	auto CilsClient::GetItemExecutions(int projectId, const QString* filter, const QString* reviewerId,
		const QString* assigneeId, ExecutionStatus review, ExecutionStatus assign,
		int page) -> ListResponse<ItemExecution>* {
		const QString url = QString("/project/%1/itemExecutions").arg(projectId);
		QMap<QString, QString> params = { {"page", QString::number(page)} };
		if (filter)
			params["filter"] = *filter;
		if (reviewerId)
			params["reviewerId"] = *reviewerId;
		if (assigneeId)
			params["reviewerId"] = *assigneeId;
		if (review != ExecutionStatus::Any)
			params["review"] = ItemExecution::ExecutionStatusToString(review);
		if (assign != ExecutionStatus::Any)
			params["assign"] = ItemExecution::ExecutionStatusToString(assign);

		auto* reply = d->client.Get(url, params);
		auto* response = new ListResponse<ItemExecution>(this);
		return d->Connect<ListResponse<ItemExecution>>(reply, response);
	}

	auto CilsClient::GetItemExecution(int executionId) -> SingleResponse<ItemExecution>* {
		const QString url = QString("/project/itemExecution/%1").arg(executionId);

		auto* reply = d->client.Get(url);
		auto* response = new SingleResponse<ItemExecution>("itemExecution", this);
		return d->Connect<SingleResponse<ItemExecution>>(reply, response);
	}

	auto CilsClient::PrepareExecution(int projectId, const QVector<int>& executionId, UserType type, bool includeResult) -> RequestResponse* {
		const QString url = QString("/project/%1/prepareExecution").arg(UserTypeToString(type).toLower());
		QJsonArray arr;
		QJsonObject obj;

		for (auto i : executionId)
			arr.push_back(i);
		obj["projectId"] = projectId;
		obj["ieIds"] = arr;
		obj["includeResult"] = includeResult;

		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::PrepareExecution(int projectId, int executionId, UserType type, bool includeResult) -> RequestResponse* {
		return PrepareExecution(projectId, QVector<int> { executionId }, type, includeResult);
	}

	auto CilsClient::GetOnExecutionReady(UserType type) -> ArrayResponse<OnExecutionReady>* {
		const QString url = QString("/project/%1/onExecutionReady").arg(UserTypeToString(type).toLower());

		auto* reply = d->client.Get(url);
		auto* response = new ArrayResponse<OnExecutionReady>(this);
		return d->Connect<ArrayResponse<OnExecutionReady>>(reply, response);
	}

	auto CilsClient::GetOnExecution(int projectId) -> ArrayResponse<OnExecution>* {
		const QString url = QString("/project/%1/onExecution").arg(QString::number(projectId));

		auto* reply = d->client.Get(url);
		auto* response = new ArrayResponse<OnExecution>(this);
		return d->Connect<ArrayResponse<OnExecution>>(reply, response);
	}

	auto CilsClient::StartExecution(int executionId, UserType type) -> RequestResponse* {
		const QString url = QString("/project/%1/startExecution").arg(UserTypeToString(type).toLower());
		QJsonObject obj;
		obj["ieId"] = executionId;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::UpdateExecution(int executionId, const QString& message, UserType type) -> RequestResponse* {
		const QString url = QString("/project/%1/updateExecution").arg(UserTypeToString(type).toLower());
		QJsonObject obj;
		obj["ieId"] = executionId;
		obj["message"] = message;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::FinishExecution(int executionId, const QString& outputPath, UserType type) -> RequestResponse* {
		const QString url = QString("/project/%1/finishExecution").arg(UserTypeToString(type).toLower());
		QJsonObject obj;
		obj["ieId"] = executionId;
		obj["outputPath"] = outputPath;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::GetItemDetail(const QString& dataId) -> SingleResponse<Item>* {
		const QString url = QString("/api/item/%1").arg(dataId);

		auto* reply = d->client.Get(url);
		auto* response = new SingleResponse<Item>(this);
		return d->Connect<SingleResponse<Item>>(reply, response);
	}

	auto CilsClient::GetItemPreview(const QString& dataId) -> ImageResponse* {
		const QString url = QString("/api/item/preview/%1").arg(dataId);

		auto* urlReply = d->client.Get(url);
		auto* response = new ImageResponse(this);

		connect(urlReply, &QNetworkReply::finished,
			[this, urlReply, response] {
				auto* imgReply = d->client.Get(urlReply->rawHeader("Location"));
				urlReply->close();
				delete urlReply;

				d->Connect<ImageResponse>(imgReply, response);
			}
		);

		return response;
	}

	auto CilsClient::DownloadItem(const QString& dataId) -> RequestResponse* {
		const QString url = QString("/api/download/item");
		QJsonObject obj;
		obj["dataId"] = dataId;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::GetDownloadingItems() -> ListResponse<SyncExecution>* {
		const QString url = QString("/api/download/items");

		auto* reply = d->client.Get(url);
		auto* response = new ListResponse<SyncExecution>(this);
		return d->Connect<ListResponse<SyncExecution>>(reply, response);
	}

	auto CilsClient::PauseDownloadItem(const QString& infoId) -> RequestResponse* {
		const QString url = QString("/api/download/pause");
		QJsonObject obj;
		obj["infoId"] = infoId;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::ResumeDownloadItem(const QString& infoId) -> RequestResponse* {
		const QString url = QString("/api/download/resume");
		QJsonObject obj;
		obj["infoId"] = infoId;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::CancelDownloadItem(const QString& infoId) -> RequestResponse* {
		const QString url = QString("/api/download/item/cancel");
		QJsonObject obj;
		obj["infoId"] = infoId;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::ChangeItemDownloadPriority(const QString& infoId, int priority) -> RequestResponse* {
		const QString url = QString("/api/download/priority/change");
		QJsonObject obj;
		obj["infoId"] = infoId;
		obj["priority"] = priority;
		const QJsonDocument doc(obj);

		auto* reply = d->client.Post(url, doc.toJson());
		auto* response = new RequestResponse(this);
		return d->Connect<RequestResponse>(reply, response);
	}

	auto CilsClient::GetWorksets(int page) -> ListResponse<Workset>* {
		const QString url = QString("/api/worksets");
		const QMap<QString, QString> params = { {"page", QString::number(page)} };

		auto* reply = d->client.Get(url, params);
		auto* response = new ListResponse<Workset>(this);
		return d->Connect<ListResponse<Workset>>(reply, response);
	}

	auto CilsClient::GetWorksetItems(int worksetId, int page) -> ListResponse<Item>* {
		const QString url = QString("/api/workset/%1/items").arg(worksetId);
		const QMap<QString, QString> params = { {"page", QString::number(page)} };

		auto* reply = d->client.Get(url, params);
		auto* response = new ListResponse<Item>(this);
		return d->Connect<ListResponse<Item>>(reply, response);
	}

	auto CilsClient::StringToUserType(const QString& type) -> UserType {
		if (type == "Assignee")
			return UserType::Assignee;
		else if (type == "Reviewer")
			return UserType::Reviewer;
		return UserType::None;
	}

	auto CilsClient::UserTypeToString(UserType type) -> QString {
		switch (type) {
		case UserType::Assignee:
			return "Assignee";
		case UserType::Reviewer:
			return "Reviewer";
		default:
			return "None";
		}
	}

	auto CilsClient::CompatibleVersion() -> QString {
		return "1.7.4";
	}
}
