#pragma once

#include <memory>

#include <QObject>
#include <QJsonDocument>

#include "Error.h"
#include "TCCilsClientExport.h"

namespace TC::Cils::Client {
	using namespace JsonEntity;

	class TCCilsClient_API IResponse : public QObject {
		Q_OBJECT

	public:
		explicit IResponse(QObject* parent = nullptr);
		~IResponse() override;
        
        [[nodiscard]] auto IsInitialized() const -> bool;
        [[nodiscard]] auto IsError() const -> bool;
        [[nodiscard]] auto GetError() const->const Error*;
		
        virtual auto Initialize(int errorCode) -> void;
        virtual auto Initialize(const QByteArray& contents, int errorCode) -> void;

	signals:
		void Initialized();

	protected:
		virtual auto Deserialize(const QJsonDocument& doc) -> bool = 0;
		auto SetHttpError(int errorCode, const QString& message) -> void;
		auto SetHttpError(int errorCode) -> void;

	private:
		static auto GetHttpErrorMessage(int errorCode)->QString;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}