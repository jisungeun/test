#pragma once

#include <optional>

#include "IResponse.h"
#include "ItemExecution.h"

namespace TC::Cils::Client {
	template<typename T>
	class SingleResponse : public IResponse {
	public:
		SingleResponse(QString objectName, QObject* parent = nullptr);
		explicit SingleResponse(QObject* parent = nullptr);
		~SingleResponse() override;

		[[nodiscard]] auto GetValue() const -> const std::optional<T>&;

		auto SetObjectName(const QString& name) -> void;
		[[nodiscard]] auto GetObjectName() const -> const QString&;

	protected:
		auto Deserialize(const QJsonDocument& doc) -> bool override;

	private:
		QString objectName;
		std::optional<T> value;
	};

	template <typename T>
	SingleResponse<T>::SingleResponse(QString objectName, QObject* parent) : IResponse(parent), objectName(std::move(objectName)) {}

	template <typename T>
	SingleResponse<T>::SingleResponse(QObject* parent) : IResponse(parent) {}

	template <typename T>
	SingleResponse<T>::~SingleResponse() = default;

	template <typename T>
	auto SingleResponse<T>::GetValue() const -> const std::optional<T>& {
		return value;
	}

	template <typename T>
	auto SingleResponse<T>::SetObjectName(const QString& name) -> void {
		objectName = name;
	}

	template <typename T>
	auto SingleResponse<T>::GetObjectName() const -> const QString& {
		return objectName;
	}

	template <typename T>
	auto SingleResponse<T>::Deserialize(const QJsonDocument& doc) -> bool {
		const auto obj = doc.object();

		value = T();

		if (objectName.isEmpty())
			return value->Deserialize(obj);
		else
			return value->Deserialize(obj[objectName].toObject());
	}
}
