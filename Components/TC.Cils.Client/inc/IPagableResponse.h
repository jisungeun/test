#pragma once

#include <memory>

#include "IResponse.h"
#include "TCCilsClientExport.h"

namespace TC::Cils::Client {
	class TCCilsClient_API IPagableResponse : public IResponse {
	public:
		explicit IPagableResponse(QObject* parent = nullptr);
		~IPagableResponse() override;
		
        [[nodiscard]] auto GetPageLength() const -> int;
        [[nodiscard]] auto GetCurrentPage() const -> int;

	protected:
		auto Deserialize(const QJsonDocument& doc) -> bool override;
		virtual auto DeserializeList(const QJsonArray& arr) -> bool = 0;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}