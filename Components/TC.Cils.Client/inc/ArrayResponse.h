#pragma once

#include <QJsonArray>

#include "IResponse.h"

namespace TC::Cils::Client {
	template<typename T>
	class ArrayResponse : public IResponse {
	public:
		explicit ArrayResponse(QObject* parent = nullptr);
		~ArrayResponse() override;

		[[nodiscard]] auto GetArray() const -> const QVector<T>&;

	protected:
        auto Deserialize(const QJsonDocument& doc) -> bool override;

    private:
		QVector<T> value;
	};

	template <typename T>
	ArrayResponse<T>::ArrayResponse(QObject* parent) : IResponse(parent) {}

	template <typename T>
	ArrayResponse<T>::~ArrayResponse() = default;

	template <typename T>
	auto ArrayResponse<T>::GetArray() const -> const QVector<T>& {
		return value;
	}

    template <typename T>
    auto ArrayResponse<T>::Deserialize(const QJsonDocument& doc) -> bool {
		const auto arr = doc.array();

		foreach(const auto & ar, arr) {
			T type;

			if (!type.Deserialize(ar.toObject()))
				return false;

			value.push_back(type);
		}

		return true;
    }
}
