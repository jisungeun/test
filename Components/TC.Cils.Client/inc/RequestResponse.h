#pragma once

#include "IResponse.h"
#include "User.h"
#include "TCCilsClientExport.h"

namespace TC::Cils::Client {
	class TCCilsClient_API RequestResponse : public IResponse {
	public:
		explicit RequestResponse(QObject* parent = nullptr);
		~RequestResponse() override;

		[[nodiscard]] auto IsDone() const -> bool;

	protected:
		auto Deserialize(const QJsonDocument& doc) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}