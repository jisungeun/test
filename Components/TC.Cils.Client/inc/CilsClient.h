#pragma once

#include <memory>

#include <QObject>

#include "SingleResponse.h"
#include "ArrayResponse.h"
#include "ImageResponse.h"
#include "ListResponse.h"
#include "RequestResponse.h"
#include "ItemExecution.h"
#include "SyncExecution.h"
#include "OnExecution.h"
#include "OnExecutionReady.h"
#include "Project.h"

#include "TCCilsClientExport.h"

namespace TC::Cils::Client {
	enum class TCCilsClient_API UserType {
		None,
		Assignee,
		Reviewer
	};

	class TCCilsClient_API CilsClient final : public QObject {
	public:
		explicit CilsClient(QObject* parent = nullptr);
		~CilsClient() override;

		[[nodiscard]] auto GetLoginInfo() ->SingleResponse<User>*;
		[[nodiscard]] auto GetProjects(const QString* filter = nullptr, int page = 1) -> ListResponse<Project>*;
		[[nodiscard]] auto GetItemExecutions(int projectId, const QString* filter = nullptr, const QString* reviewerId = nullptr, const QString* assigneeId = nullptr, ExecutionStatus review = ExecutionStatus::Any, ExecutionStatus assign = ExecutionStatus::Any, int page = 1) -> ListResponse<ItemExecution>*;
		[[nodiscard]] auto GetItemExecution(int executionId) ->SingleResponse<ItemExecution>*;
		// GET /project/{projectId}/itemExecutions/results/meta
		// GET /project/{projectId}/itemExecution/{ieId}/results/meta
		[[nodiscard]] auto PrepareExecution(int projectId, const QVector<int>& executionId, UserType type, bool includeResult = true) -> RequestResponse*;
		[[nodiscard]] auto PrepareExecution(int projectId, int executionId, UserType type, bool includeResult = true) -> RequestResponse*;
		[[nodiscard]] auto GetOnExecutionReady(UserType type) -> ArrayResponse<OnExecutionReady>*;
		[[nodiscard]] auto GetOnExecution(int projectId) -> ArrayResponse<OnExecution>*;
		[[nodiscard]] auto StartExecution(int executionId, UserType type) -> RequestResponse*;
		[[nodiscard]] auto UpdateExecution(int executionId, const QString& message, UserType type) ->RequestResponse*;
		[[nodiscard]] auto FinishExecution(int executionId, const QString& outputPath, UserType type) ->RequestResponse*;
		// GET /project/{projectId}/meta

		[[nodiscard]] auto GetItemDetail(const QString& dataId)->SingleResponse<Item>*;
		[[nodiscard]] auto GetItemPreview(const QString& dataId)->ImageResponse*;

		[[nodiscard]] auto DownloadItem(const QString& dataId) -> RequestResponse*;
		[[nodiscard]] auto GetDownloadingItems()->ListResponse<SyncExecution>*;
		[[nodiscard]] auto PauseDownloadItem(const QString& infoId) -> RequestResponse*;
		[[nodiscard]] auto ResumeDownloadItem(const QString& infoId)->RequestResponse*;
		[[nodiscard]] auto CancelDownloadItem(const QString& infoId)->RequestResponse*;
		[[nodiscard]] auto ChangeItemDownloadPriority(const QString& infoId, int priority)->RequestResponse*;

		[[nodiscard]] auto GetWorksets(int page = 1)->ListResponse<Workset>*;
		[[nodiscard]] auto GetWorksetItems(int worksetId, int page)->ListResponse<Item>*;

		[[nodiscard]] static auto StringToUserType(const QString& type) -> UserType;
		[[nodiscard]] static auto UserTypeToString(UserType type) -> QString;
		[[nodiscard]] static auto CompatibleVersion() -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}