#pragma once

#include <memory>

#include <QObject>
#include <QJsonDocument>

#include "Error.h"
#include "IResponse.h"
#include "TCCilsClientExport.h"

namespace TC::Cils::Client {
	using namespace JsonEntity;

	class TCCilsClient_API ImageResponse : public IResponse {
		Q_OBJECT

	public:
		explicit ImageResponse(QObject* parent = nullptr);
		~ImageResponse() override;
		
		[[nodiscard]] auto GetImage() const->const QByteArray&;
		
		auto Initialize(const QByteArray& contents, int errorCode) -> void override;

    protected:
        auto Deserialize(const QJsonDocument& doc) -> bool override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}