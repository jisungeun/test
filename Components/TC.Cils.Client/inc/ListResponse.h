#pragma once

#include "IPagableResponse.h"

namespace TC::Cils::Client {
	template<typename T>
	class ListResponse : public IPagableResponse {
	public:
		explicit ListResponse(QObject* parent = nullptr);
		~ListResponse() override;

		[[nodiscard]] auto GetList() const -> const QVector<T>&;

	protected:
		auto DeserializeList(const QJsonArray& arr) -> bool override;

	private:
		QVector<T> value;
	};

	template <typename T>
	ListResponse<T>::ListResponse(QObject* parent) : IPagableResponse(parent) {}

	template <typename T>
	ListResponse<T>::~ListResponse() = default;

	template <typename T>
	auto ListResponse<T>::GetList() const -> const QVector<T>& {
		return value;
	}

	template <typename T>
	auto ListResponse<T>::DeserializeList(const QJsonArray& arr) -> bool {
		foreach(const auto & ar, arr) {
			T type;

			if (!type.Deserialize(ar.toObject()))
				return false;

			value.push_back(type);
		}

		return true;
	}
}
