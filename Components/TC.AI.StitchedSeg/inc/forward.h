#include <torch/script.h>

class forward {
public:
	forward();
	~forward();

	auto setFileName(std::string fn)->void;
	auto getFileName(void)->std::string;

	auto setModelName(std::string mn)->void;
	auto getModelName()->std::string;
		
	auto produceStitchedSeg(void* inst, void* membrane, void* nucleus, void* nucleoli, void* lipid)->void;
	auto produceStitchedSeg()->void;
	auto getSuccess()->bool;
	
	auto setTimeStep(int step)->void;
	auto setTileSize(int tile)->void;
	
	auto setImage(unsigned short* img)->void;
	auto setBlockSize(int size[3])->void;
	auto getInstExist()->bool;

	auto getInst()->void*;
	auto releaseInst()->void;
	auto getMem()->void*;
	auto releaseMem()->void;
	auto getNuc()->void*;
	auto releaseNuc()->void;
	auto getNucli()->void*;
	auto releaseNucli()->void;
	auto getLip()->void*;
	auto releaseLip()->void;

private:
	auto run(torch::jit::script::Module& seg_model,
		torch::Tensor& ri, const torch::DeviceType device)->torch::Tensor;
	auto run_tuple(torch::jit::script::Module& seg_model,
		torch::Tensor& ri, const torch::DeviceType device)->std::tuple<torch::Tensor, torch::Tensor>;

	struct Impl;
	std::unique_ptr<Impl> d;
};