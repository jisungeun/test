#include <string>
#include <memory>

#include "TCAiStitchedSegExport.h"

namespace TC {
	namespace AI {
		class TCAiStitchedSeg_API TCAiStitchedSeg {
		public:
			TCAiStitchedSeg();
			~TCAiStitchedSeg();

			auto setFileName(std::string fn)->void;
			auto setModelName(std::string mn)->void;

			auto getFileName(void)->std::string;
			auto getModelName(void)->std::string;

			auto setTileSize(int)->void;

			auto produceUnifiedSegmentation(void* inst, void* mem, void* nuc, void* nucli, void* lip)->void;
			auto produceAiSegmentation()->void;

			auto getInstance()->void*;
			auto delInstance()->void;
			auto getMembrane()->void*;
			auto delMembrane()->void;
			auto getNucleus()->void*;
			auto delNucleus()->void;
			auto getNucleoli()->void*;
			auto delNucleoli()->void;
			auto getLipid()->void*;
			auto delLipid()->void;

			auto getSuccess()->bool;
			auto setTimStep(int step)->void;
			auto setBufferSize(int size[3])->void;
			auto setInputImage(unsigned short* img)->void;
			auto getInstSuccess()->bool;

		private:
			struct Impl;
			std::unique_ptr<Impl> d;
		};
	}
}