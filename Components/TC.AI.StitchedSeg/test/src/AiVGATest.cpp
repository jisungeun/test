#include <iostream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <chrono>
#include <numeric>
#include <functional>

#include <cuda.h>
#include <torch/torch.h>
#include <torch/script.h>
#include <ATen/autocast_mode.h>
#include <c10/cuda/CUDACachingAllocator.h>

#include "cuda_runtime.h"

#include <tomo.h>
#include <ops/connected_components.h>


void save_tensor(const std::string& path, const torch::Tensor& data)
{
    auto bytes = torch::jit::pickle_save(data);
    std::ofstream fout(path, std::ios::out | std::ios::binary);
    fout.write(bytes.data(), bytes.size());
    fout.close();
}

auto load_tensor(const std::string& path)->torch::Tensor {
    std::cout << "Load : " << path << "\n";
    std::ifstream fin(path, std::ios::in | std::ios::binary);
    std::vector<char> buffer(std::istreambuf_iterator<char>(fin), {});
    fin.close();

    auto tensor = torch::jit::pickle_load(buffer);
    return tensor.toTensor();
}

int main(int argc, const char* argv[])
{
    tomo::cuda_version();
    at::globalContext().setBenchmarkCuDNN(true);

    torch::autograd::GradMode::set_enabled(false);
    torch::autograd::AutoGradMode guard(false);
    // c10::InferenceMode infer_guard(true);

    const torch::DeviceType device = torch::kCUDA;
    std::string nuc = "nuclues_tensor.pth";//TODO change model path due to test folder location
    auto x = load_tensor(nuc);
    auto seed = tomo::cc_3d(x.to(torch::kCUDA, torch::kUInt8));
    save_tensor("win_cpp_seed.pth", seed.to(torch::kCPU));
    std::cout << "cpp seed";
    return 0;
}
