project(AiVGATest)

find_package(Python3 ${Python_VERSION} COMPONENTS Development)
find_package(Torch REQUIRED)

set(CMAKE_CXX_FLAGS_RELEASE "-Od -Ob0")

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES	
	src/AiVGATest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE		
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${TORCH_INCLUDE_DIRS}
)

target_link_directories(${PROJECT_NAME}
	PRIVATE				
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE					
		Qt5::Core
		${TORCH_LIBRARIES}
		${CMAKE_DL_LIBS}
		Extern::Tomo
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases")