#include <iostream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <chrono>
#include <numeric>
#include <functional>
#include <exception>

#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4324)
#pragma warning(disable:4458)
#pragma warning(disable:4127)
#pragma warning(disable:4624)
#pragma warning(disable:4189)
#pragma warning(disable:4477)
#pragma warning(disable:4505)
#pragma warning(disable:4702)

#include <cuda.h>
#include <torch/torch.h>
#include <torch/script.h>
#include <ATen/autocast_mode.h>
#include <c10/cuda/CUDACachingAllocator.h>

#include "cuda_runtime.h"

#include <tomo.h>

#include "forward.h"

#define LOGGER_TAG "[AI inference]"
#include <TCLogger.h>

using namespace torch::indexing;
namespace F = torch::nn::functional;

const size_t kInferBatchSize = 1;
const int64_t kPatchSize = 128;
const int64_t kNumClass = 4;

#define DEBUGDEPTH_____

template <typename T>
auto tcf2tensor(T* arr, const torch::IntArrayRef shape)->torch::Tensor {
	// tensor memory sharing with arr
	torch::Tensor tensor = torch::from_blob(arr, shape, at::kShort);

	// `tensor` and `float_tensor` got different target_pointer
	torch::Tensor float_tensor = tensor.toType(torch::kFloat32);
	//float_tensor /= 10000.0;	
	//float_tensor = (float_tensor - 13370) / (13900 - 13370);
	float_tensor = (float_tensor.clamp(13370, 13900) - 13370) / (13900 - 13370);

	// indexing has not support yet
	// auto z = shape[0] / 2;
	// auto sliced_tensor = float_tensor.index({::,torch::indexing::None, torch::indexing::Slice(z-32, z+32)})
	return float_tensor;
}

struct forward::Impl {
	std::string file_name;

	std::string model_name;

	int cur_time_step;

	int block_size{ 0 };
	int block_dim[3] = { 0, };
	int tile_size{ 1 };
	unsigned short* img{ nullptr };

	bool instExist{ false };
	bool success{ false };

	torch::Tensor out_membrane;
	torch::Tensor out_nucleus;
	torch::Tensor out_nucleoli;
	torch::Tensor out_lipid;
	torch::Tensor out_instance;
};

forward::forward()
	:d(new Impl()) {
	d->file_name = "file";
	d->model_name = "pth/stitched/cell_segmentation_v2.1.jit";
}

forward::~forward() {

}

auto forward::getSuccess() -> bool {
	return d->success;
}


auto forward::setBlockSize(int size[3]) -> void {
	d->block_dim[0] = size[0];
	d->block_dim[1] = size[1];
	d->block_dim[2] = size[2];
	d->block_size = size[0] * size[1] * size[2];
}


auto forward::setImage(unsigned short* img) -> void {
	d->img = img;
}


auto forward::setTimeStep(int step) -> void {
	d->cur_time_step = step;
}

auto forward::getFileName() -> std::string {
	return d->file_name;
}

auto forward::setFileName(std::string fn) -> void {
	d->file_name = fn;
}

auto forward::setModelName(std::string mn) -> void {
	d->model_name = mn;
}

auto forward::getModelName() -> std::string {
	return d->model_name;
}

auto forward::setTileSize(int tile) -> void {
	d->tile_size = tile;
}

auto forward::run(torch::jit::script::Module& seg_model, torch::Tensor& ri, const torch::DeviceType device) -> torch::Tensor {
	torch::NoGradGuard no_grad;
	at::autocast::set_enabled(true);

	std::vector<torch::jit::IValue> inputs;
	inputs.push_back(ri);
	torch::Tensor results = seg_model.forward(inputs).toTensor();

	at::autocast::clear_cache();
	at::autocast::set_enabled(false);

	//save_tensor("cpp_out.pth", results.to(torch::kCPU));

	return results;
}

auto forward::run_tuple(torch::jit::script::Module& seg_model, torch::Tensor& ri, const torch::DeviceType device) -> std::tuple<torch::Tensor, torch::Tensor> {
	torch::NoGradGuard no_grad;
	at::autocast::set_enabled(true);

	std::vector<torch::jit::IValue> inputs;
	inputs.push_back(ri);
	// torch::Tensor results = seg_model.forward(inputs).toTensor();

	auto output = seg_model.forward(inputs);
	torch::Tensor multi_organ_results = output.toTuple()->elements()[0].toTensor();
	torch::Tensor cell_inst_results = output.toTuple()->elements()[1].toTensor();

	at::autocast::clear_cache();
	at::autocast::set_enabled(false);

	//save_tensor("cpp_out_multi.pth", multi_organ_results.to(torch::kCPU));
	//save_tensor("cpp_out_cell.pth", cell_inst_results.to(torch::kCPU));

	return { multi_organ_results, cell_inst_results };
}

auto forward::produceStitchedSeg() -> void {
	tomo::cuda_version();
	at::globalContext().setBenchmarkCuDNN(true);

	torch::autograd::GradMode::set_enabled(false);
	torch::autograd::AutoGradMode guard(false);
	// c10::InferenceMode infer_guard(true);

	int cudaVersion;
	cudaRuntimeGetVersion(&cudaVersion);

	std::cout << "CUDA VERSION: " << cudaVersion << std::endl;

	std::cout << "PyTorch version: "
		<< TORCH_VERSION_MAJOR << "."
		<< TORCH_VERSION_MINOR << "."
		<< TORCH_VERSION_PATCH << std::endl;

	const torch::DeviceType device = torch::kCUDA;
	// load model

	std::cout << "model name: " << d->model_name << std::endl;

	try {
		torch::jit::script::Module seg_model = torch::jit::load(d->model_name, device);
		seg_model.eval();

		auto data = [&]() {
			int64_t dims[3];
			dims[0] = d->block_dim[2];
			dims[1] = d->block_dim[1];
			dims[2] = d->block_dim[0];

			auto tensor_shape = torch::IntList(dims);
			auto data = tcf2tensor<uint16_t>(d->img, tensor_shape);
			return (data.unsqueeze_(0));
		}();

		auto ri = data.unsqueeze(0);

		//TODO: for model v2.2
		/*
		auto result = run(seg_model, ri, device);
		auto org_results = result;
		org_results = org_results.squeeze_(0);

		d->out_membrane = org_results[0];
		std::cout << "set membrane" << std::endl;
		if (!d->out_membrane.numel()) {
			d->success = false;
		}
		else if (d->out_membrane.max().item().to<float>() < 1.0) {
			d->success = false;
		}
		else {
			d->success = true;
		}
		std::cout << "set instance" << std::endl;
		d->out_instance = org_results[4];

		if (!d->out_instance.numel()) {
			std::cout << "instance segmentation fail by numel() check" << std::endl;
			d->instExist = false;
		}
		else if (d->out_instance.max().item().to<float>() < 1.0) {
			std::cout << "instance segmentation fail by no result check (smaller than 1)" << std::endl;
			d->instExist = false;
		}
		else {
			std::cout << "instance type" << d->out_instance.type().toString() << std::endl;
			d->instExist = true;
		}
		std::cout << "set nucleus" << std::endl;
		d->out_nucleus = org_results[1];
		std::cout << "set nucleoli" << std::endl;
		d->out_nucleoli = org_results[2];
		std::cout << "set lipid" << std::endl;
		d->out_lipid = org_results[3];
		*/

		auto result = run_tuple(seg_model, ri, device);
				
		auto org_results = std::get<0>(result);
		auto inst_result = std::get<1>(result);

		org_results = org_results.squeeze_(0);
		inst_result = inst_result.squeeze_(0).squeeze_(0);

		std::cout << "organ size: " << org_results.sizes() << std::endl;
		std::cout << "inst size: " << inst_result.sizes() << std::endl;
		
		d->out_membrane = org_results[0];
		std::cout << "set membrane" << std::endl;
		if (!d->out_membrane.numel()) {
			d->success = false;
		}
		else if (d->out_membrane.max().item().to<float>() < 1.0) {
			d->success = false;
		}
		else {
			d->success = true;
		}
		std::cout << "set instance" << std::endl;
		d->out_instance = inst_result;

		if (!d->out_instance.numel()) {
			std::cout << "instance segmentation fail by numel() check" << std::endl;
			d->instExist = false;
		}
		else if (d->out_instance.max().item().to<float>() < 1.0) {
			std::cout << "instance segmentation fail by no result check (smaller than 1)" << std::endl;
			d->instExist = false;
		}
		else {
			std::cout <<"instance type"<< d->out_instance.type().toString() << std::endl;
			d->instExist = true;
		}

		std::cout << "set nucleus" << std::endl;
		d->out_nucleus = org_results[1];
		std::cout << "set nucleoli" << std::endl;
		d->out_nucleoli = org_results[2];
		std::cout << "set lipid" << std::endl;
		d->out_lipid = org_results[3];
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		d->success = false;
		std::cout << "c10 memory error: " << e.what() << std::endl;
	}
	catch (c10::Error& e) {
		d->success = false;
		std::cout << "c10 error: " << e.what() << std::endl;
	}
	catch (std::exception& ex) {
		d->success = false;
		std::cout << "std exception: " << ex.what() << std::endl;
	}
	catch (...) {
		d->success = false;
		std::cout << "unknown exception" << std::endl;
	}
}

auto forward::getInst() -> void* {
	std::cout << "get instance" << std::endl;
	//return d->out_instance.to(torch::kCPU).to(torch::kLong).data_ptr<long long>();
	try {
		return d->out_instance.to(torch::kCPU).data_ptr<float>();
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		std::cout << "c10 memory error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (c10::Error& e) {
		std::cout << "c10 error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (std::exception& ex) {
		std::cout << "std exception: " << ex.what() << std::endl;
		return nullptr;
	}
	catch (...) {
		std::cout << "unknown exception" << std::endl;
		return nullptr;
	}
}

auto forward::releaseInst() -> void {
	d->out_instance = torch::Tensor();
}

auto forward::getMem() -> void* {
	std::cout << "get membrane" << std::endl;
	try {
		return d->out_membrane.to(torch::kCPU).data_ptr<float>();
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		std::cout << "c10 memory error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (c10::Error& e) {
		std::cout << "c10 error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (std::exception& ex) {
		std::cout << "std exception: " << ex.what() << std::endl;
		return nullptr;
	}
	catch (...) {
		std::cout << "unknown exception" << std::endl;
		return nullptr;
	}
}

auto forward::releaseMem() -> void {
	d->out_membrane = torch::Tensor();
}

auto forward::getNuc() -> void* {
	std::cout << "get nucleus" << std::endl;
	try {
		return d->out_nucleus.to(torch::kCPU).data_ptr<float>();
	}
	catch (c10::CUDAOutOfMemoryError& e) {		
		std::cout << "c10 memory error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (c10::Error& e) {		
		std::cout << "c10 error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (std::exception& ex) {		
		std::cout << "std exception: " << ex.what() << std::endl;
		return nullptr;
	}
	catch (...) {		
		std::cout << "unknown exception" << std::endl;
		return nullptr;
	}
}

auto forward::releaseNuc() -> void {
	d->out_nucleus = torch::Tensor();
}

auto forward::getNucli() -> void* {
	std::cout << "get nucleoli" << std::endl;
	try {
		return d->out_nucleoli.to(torch::kCPU).data_ptr<float>();
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		std::cout << "c10 memory error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (c10::Error& e) {
		std::cout << "c10 error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (std::exception& ex) {
		std::cout << "std exception: " << ex.what() << std::endl;
		return nullptr;
	}
	catch (...) {
		std::cout << "unknown exception" << std::endl;
		return nullptr;
	}
}

auto forward::releaseNucli() -> void {	
	d->out_nucleoli = torch::Tensor();
}

auto forward::getLip() -> void* {
	std::cout << "get lipid" << std::endl;
	try {
		return d->out_lipid.to(torch::kCPU).data_ptr<float>();
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		std::cout << "c10 memory error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (c10::Error& e) {
		std::cout << "c10 error: " << e.what() << std::endl;
		return nullptr;
	}
	catch (std::exception& ex) {
		std::cout << "std exception: " << ex.what() << std::endl;
		return nullptr;
	}
	catch (...) {
		std::cout << "unknown exception" << std::endl;
		return nullptr;
	}
}

auto forward::releaseLip() -> void {
	d->out_lipid = torch::Tensor();
}

auto forward::produceStitchedSeg(void* inst, void* membrane, void* nucleus, void* nucleoli, void* lipid) -> void {
	tomo::cuda_version();
	at::globalContext().setBenchmarkCuDNN(true);

	torch::autograd::GradMode::set_enabled(false);
	torch::autograd::AutoGradMode guard(false);
	// c10::InferenceMode infer_guard(true);

	int cudaVersion;
	cudaRuntimeGetVersion(&cudaVersion);

	std::cout << "CUDA VERSION: " << cudaVersion << std::endl;

	std::cout << "PyTorch version: "
		<< TORCH_VERSION_MAJOR << "."
		<< TORCH_VERSION_MINOR << "."
		<< TORCH_VERSION_PATCH << std::endl;

	const torch::DeviceType device = torch::kCUDA;
	// load model
	torch::jit::script::Module seg_model = torch::jit::load(d->model_name, device);
	seg_model.eval();

	auto data = [&]() {
		int64_t dims[3];
		dims[0] = d->block_dim[2];
		dims[1] = d->block_dim[1];
		dims[2] = d->block_dim[0];

		auto tensor_shape = torch::IntList(dims);
		auto data = tcf2tensor<uint16_t>(d->img, tensor_shape);
		return (data.unsqueeze_(0));
	}();

	//auto ri = data.squeeze_(0);
	auto ri = data.unsqueeze(0);

	try {
		auto result = run(
			seg_model, ri, device);

		result = result.to(torch::kCPU);

		result.squeeze_(0);

		std::cout << result.sizes() << std::endl;

		auto out_membrane = result[0];
		if (!out_membrane.numel()) {
			d->success = false;
		}
		else if (out_membrane.max().item().to<float>() < 1.0) {
			d->success = false;
		}
		else {
			d->success = true;
		}

		auto instTensor = result[4];

		if (!instTensor.numel()) {
			std::cout << "instance segmentation fail by numel() check" << std::endl;
			d->instExist = false;
		}
		else if (instTensor.max().item().to<float>() < 1.0) {
			std::cout << "instance segmentation fail by no result check (smaller than 1)" << std::endl;
			d->instExist = false;
		}
		else {
			auto cpu_result = instTensor.to(torch::kLong);

			memcpy(inst, cpu_result.data_ptr<long long>(), sizeof(long long) * d->block_size);
			d->instExist = true;
		}

		memcpy(membrane, out_membrane.data_ptr<float>(), sizeof(float) * d->block_size);
		memcpy(nucleus, result[1].data_ptr<float>(), sizeof(float) * d->block_size);
		memcpy(nucleoli, result[2].data_ptr<float>(), sizeof(float) * d->block_size);
		memcpy(lipid, result[3].data_ptr<float>(), sizeof(float) * d->block_size);
	}
	catch (c10::CUDAOutOfMemoryError& e) {
		d->success = false;
		std::cout << "c10 memory error: " << e.what() << std::endl;
	}
	catch (c10::Error& e) {
		d->success = false;
		std::cout << "c10 error: " << e.what() << std::endl;
	}
	catch (std::exception& ex) {
		d->success = false;
		std::cout << "std exception: " << ex.what() << std::endl;
	}
	catch (...) {
		d->success = false;
		std::cout << "unknown exception" << std::endl;
	}
}
auto forward::getInstExist() -> bool {
	return d->instExist;
}

#pragma warning(pop)