#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4324)
#pragma warning(disable:4458)
#pragma warning(disable:4127)
#pragma warning(disable:4624)
#pragma warning(disable:4189)
#pragma warning(disable:4477)
#pragma warning(disable:4505)
#pragma warning(disable:4018)
#pragma warning(disable:4702)

#include "TCAiStitchedSeg.h"
#include "forward.h"

namespace TC {
    namespace AI {        
        struct TCAiStitchedSeg::Impl {
            forward* ai_engine{nullptr};
        };

        TCAiStitchedSeg::TCAiStitchedSeg() :d{ new Impl } {
            d->ai_engine = new forward;
        }

        TCAiStitchedSeg::~TCAiStitchedSeg() {
            delete d->ai_engine;
        }

        auto TCAiStitchedSeg::getFileName() -> std::string {
            return d->ai_engine->getFileName();
        }

        auto TCAiStitchedSeg::getModelName() -> std::string {
            return d->ai_engine->getModelName();
        }

        auto TCAiStitchedSeg::setTileSize(int size) -> void {
            d->ai_engine->setTileSize(size);
        }

        auto TCAiStitchedSeg::getSuccess() -> bool {
            return d->ai_engine->getSuccess();
        }                

        auto TCAiStitchedSeg::setFileName(std::string fn) -> void {
            d->ai_engine->setFileName(fn);
        }

        auto TCAiStitchedSeg::setModelName(std::string mn) -> void {
            d->ai_engine->setModelName(mn);
        }


        auto TCAiStitchedSeg::setTimStep(int step) -> void {
            d->ai_engine->setTimeStep(step);
        }

        auto TCAiStitchedSeg::setBufferSize(int size[3]) -> void {
            d->ai_engine->setBlockSize(size);
        }

        auto TCAiStitchedSeg::setInputImage(unsigned short* img) -> void {
            d->ai_engine->setImage(img);
        }

        auto TCAiStitchedSeg::produceUnifiedSegmentation(void* inst, void* mem, void* nuc, void* nucli, void* lip) -> void {            
            d->ai_engine->produceStitchedSeg(inst, mem, nuc, nucli, lip);
        }

        auto TCAiStitchedSeg::produceAiSegmentation() -> void {
            d->ai_engine->produceStitchedSeg();
        }

        auto TCAiStitchedSeg::getInstance() -> void* {
            return d->ai_engine->getInst();
        }

        auto TCAiStitchedSeg::delInstance() -> void {
            d->ai_engine->releaseInst();
        }

        auto TCAiStitchedSeg::getMembrane() -> void* {
            return d->ai_engine->getMem();
        }

        auto TCAiStitchedSeg::delMembrane() -> void {
            d->ai_engine->releaseMem();
        }

        auto TCAiStitchedSeg::getNucleus() -> void* {
            return d->ai_engine->getNuc();
        }

        auto TCAiStitchedSeg::delNucleus() -> void {
            d->ai_engine->releaseNuc();
        }

        auto TCAiStitchedSeg::getNucleoli() -> void* {
            return d->ai_engine->getNucli();
        }

        auto TCAiStitchedSeg::delNucleoli() -> void {
            d->ai_engine->releaseNucli();
        }

        auto TCAiStitchedSeg::getLipid() -> void* {
            return d->ai_engine->getLip();
        }

        auto TCAiStitchedSeg::delLipid() -> void {
            d->ai_engine->releaseLip();
        }

        auto TCAiStitchedSeg::getInstSuccess() -> bool {
            return d->ai_engine->getInstExist();
        }
    }
}

#pragma warning(pop)