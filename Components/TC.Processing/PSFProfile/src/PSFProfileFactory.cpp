#include "PSFProfileFactory.h"

namespace TC::PSFProfile {
    auto PSFProfileFactory::Generate_v1_4_1_c(const Input_v1_4_1_c& input)-> PSFProfile_v1_4_1_c {
        PSFProfile_v1_4_1_c profile;

        const auto& naCond = input.naCond;
        const auto& imageSizeX = input.imageSizeX;
        const auto& imageSizeY = input.imageSizeY;

        if (naCond > 0.6) {
            profile.useRealPartOnly = false;
        } else {
            profile.useRealPartOnly = true;
        }

        const auto maximumImageSize = std::max(imageSizeX, imageSizeY);
        if (maximumImageSize > 1464) {
            profile.psfSizeX = 2036;
        } else {
            profile.psfSizeX = 1564;
        }
        profile.psfSizeY = 1564;
        profile.psfSizeZ = 180;

        profile.denoiseThres = "high";
        profile.zComputationSplit = 3;
        profile.secondCropNaObj = naCond;
        profile.secondCropNaCond = naCond;
        profile.secondCropNAObj02 = 0.7f;
        profile.secondCropNACond02 = 0.75f * naCond;
        profile.spectrumSize = 20;
        profile.filterSnrSupport = true;
        profile.axialUpSampleRatio = 2;
        profile.lateralVoxelCount = 1100;

        return profile;
    }
}
