#pragma once

#include <enum.h>

namespace TC::PSFProfile {
    BETTER_ENUM(PSFProfileVersion, uint8_t, v1_4_1_c);
}