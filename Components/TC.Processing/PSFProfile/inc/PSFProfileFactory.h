#pragma once

#include "TCPSFProfileExport.h"

#include "PSFProfileVersion.h"
#include "PSFProfile_v1_4_1_c.h"

namespace TC::PSFProfile {
    class TCPSFProfile_API PSFProfileFactory {
    public:
        struct Input_v1_4_1_c {
            double naCond{};
            int32_t imageSizeX{};
            int32_t imageSizeY{};
        };

        static auto Generate_v1_4_1_c(const Input_v1_4_1_c& input)->PSFProfile_v1_4_1_c;
    };
}
