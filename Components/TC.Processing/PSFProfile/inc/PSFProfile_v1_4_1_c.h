#pragma once

#include <QString>

#include "TCPSFProfileExport.h"

namespace TC::PSFProfile {
    struct TCPSFProfile_API PSFProfile_v1_4_1_c {
        bool useRealPartOnly{};
        int32_t psfSizeX{};
        int32_t psfSizeY{};
        int32_t psfSizeZ{};
        QString denoiseThres{};
        int32_t zComputationSplit{};
        float secondCropNaObj{};
        float secondCropNaCond{};
        float secondCropNAObj02{};
        float secondCropNACond02{};
        int32_t spectrumSize{};
        bool filterSnrSupport{};
        float axialUpSampleRatio{};
        int32_t lateralVoxelCount{};
    };
}