#define LOGGER_TAG "TCHoloprocessor::TomoBuilderNonNegative"
#include <TCLogger.h>

#include "TomoBuilderUtility.h"
#include "TomoBuilderNonNegative.h"
#include "AfArrayProxy.h"
#include "AfArrayProfiler.h"

namespace TC {

    TomoBuilderNonNegative::TomoBuilderNonNegative(ProgressReporter* reporter, StopChecker* stopchecker)
        : TomoBuilderInterface(reporter, stopchecker) {

    }

    TomoBuilderNonNegative::~TomoBuilderNonNegative() {

    }

    bool TomoBuilderNonNegative::Regularization(af::array& ORytov_index, af::array& ORytov, float /*normFact*/) {
        bool bSuccess = true;
        char fname_in[128] = { 0, };
        const char* fname_out = "regulization.arr";

        const double lambda = m_parameter[PN_LAMBDA];
        const double n_m = m_parameter[PN_NM];
        const unsigned int iterNumber = m_parameter[PN_ITERATION];

        //Non negative constraint
        const double k0 = 1 / lambda;

        AF_MEMINFO(GetDeviceIndex(), "Regularization-begin");

        dbg_saveArray("arr_ORytov_index_1", ORytov_index, fname_out, false);
        dbg_saveArray("arr_ORytov_1", ORytov, fname_out, true);

        const size_t availableMem = AF_AVAILABLE(GetDeviceIndex());
        const size_t usedMem = AF_USED(GetDeviceIndex());
        const size_t orytovMem = ORytov.allocated();
        const bool useProxy = availableMem < (orytovMem * 10);
        const bool useRelease = useProxy;
        const double mega = std::pow(1024, 2);

        QLOG_INFO() << "[" << GetDeviceIndex() << "] Use proxy=" << useProxy << " available=" << (availableMem / mega) << "MB Orytov=" << (orytovMem / mega) << "MB used=" << (usedMem / mega) << "MB";

        AfArrayProxy ORytovProxy;
        AfArrayProxy ORytovIndexProxy;

        if (useProxy) ORytovProxy.Set(ORytov);
        if (useProxy) ORytovIndexProxy.Set(ORytov_index);

        TC::release();

        if (useProxy) ORytovIndexProxy.Get(ORytov_index);

        dbg_saveArray("arr_ORytov_index_2", ORytov_index, fname_out, true);

        dbg_saveArray("arr_Reconimg_1", m_Reconimg, fname_out, true);

        AF_MEMINFO(GetDeviceIndex(), "Regularization - before entering the loop");

        for (unsigned int mm = 0; mm < iterNumber; mm++) {
#ifdef DEEP_DEBUG
            sprintf_s<128>(fname_in, "recon_%03d.arr", mm);
#endif
            //if (isStopRequested())
            //{
            //	bSuccess = false;
            //	break;
            //}

            reportProgress(10 + 90 * ((mm + 1) / (1.0 * iterNumber)));

            dbg_saveArray("arr_Reconimg_0", m_Reconimg, fname_in, false);

            {
                //! MATLAB: id = (real(Reconimg)<0);
                af::array id = (af::real(m_Reconimg) < 0);
                id.eval();  //TOMV-234

                dbg_saveArray("arr_id", id, fname_in, true);

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 01");

                //! MATLAB: Reconimg(id)=0-1i*imag(Reconimg(id));     //TOMV-1506
                // Use element wise operations instead of assignment
                m_Reconimg = id * (0 - af::complex(0, af::imag(m_Reconimg))) + (1 - id) * m_Reconimg;

                dbg_saveArray("arr_Reconimg_1", m_Reconimg, fname_in, true);
                //dbg_saveArray("arr_Reconimg_pow2", pow2complex(m_Reconimg), fname_in, true);
            }

            if (useRelease) TC::release();

            AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 02");

            dbg_saveArray("arr_Reconimg_2", m_Reconimg, fname_in, true);
            {
                //! MATLAB: ORytov_new=fftshift(fftn(ifftshift(Reconimg()); //TOMV-1506
                m_Reconimg = ifftshift3(m_Reconimg);
                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 05");

                if (useRelease) TC::release_fft_plans();

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 05-1");

                af::fft3InPlace(m_Reconimg);

                af::array& ORytov_new = fftshift3(m_Reconimg);
                if (useRelease) TC::release_fft_plans();

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 06");
                if (useRelease) ORytov_new.eval();

                if (useRelease) TC::release();
                dbg_saveArray("arr_ORytov_new_1", ORytov_new, fname_in, true);

                if (useProxy) ORytovProxy.Get(ORytov);

                dbg_saveArray("arr_ORytov_3", ORytov, fname_in, true);

                //! MATLAB: ORytov_new=ORytov_new.*ORytov_index+ORytov;
                ORytov_new = ORytov_new * ORytov_index + ORytov;

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 07");

                if (useRelease) ORytov_new.eval();

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 07-1");

                if (useProxy) ORytovProxy.Set(ORytov);

                dbg_saveArray("arr_ORytov_new_2", ORytov_new, fname_in, true);

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 08");

                if (useRelease) TC::release();

                AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 08-1");
                //TC_MEM_LIST("Regularization loop - 08-1");

                //! MATLAB: Reconimg=fftshift(ifftn(ifftshift(ORytov_new)));    //TOMV-1506
                ORytov_new = ifftshift3(ORytov_new);
                if (useRelease) TC::release_fft_plans();

                af::ifft3InPlace(ORytov_new);

                m_Reconimg = fftshift3(ORytov_new);
                if (useRelease) m_Reconimg.eval();

                if (useRelease) TC::release_fft_plans();

                dbg_saveArray("arr_Reconimg_3", m_Reconimg, fname_in, true);
            }
            if (useRelease) TC::release();
        }

        AF_MEMINFO(GetDeviceIndex(), "Regularization loop - 09");

        //! MATLAB: id = (real(Reconimg)<0);            //TOMV-1506
        //! MATLAB: Reconimg(id) = 0 - 1i*imag(Reconimg(id));
        //! MATLAB: Reconimg = n_m*sqrt(4 * pi*Reconimg / (2 * pi*n_m*k0) ^ 2 + 1);
        af::array id = (af::real(m_Reconimg) < 0);
        id.eval();  //TOMV-234
        dbg_saveArray("arr_id", id, fname_out, true);

        m_Reconimg = id * (0 - af::complex(0, af::imag(m_Reconimg))) + (1 - id) * m_Reconimg;
        dbg_saveArray("arr_Reconimg_2", m_Reconimg, fname_out, true);

        m_Reconimg = 4 * af::Pi * m_Reconimg / std::pow(2 * af::Pi * n_m * k0, 2) + 1;
        m_Reconimg.eval();
        dbg_saveArray("arr_Reconimg_3", m_Reconimg, fname_out, true);

        if (useRelease) TC::release();

        {
            af::array arReal, arImag;
            sqrt(arReal, arImag, m_Reconimg);
            m_Reconimg = af::complex(n_m * arReal, n_m * arImag);
            m_Reconimg.eval();
        }

        dbg_saveArray("arr_Reconimg_4", m_Reconimg, fname_out, true);

        if (useRelease) TC::release();
        if (useRelease) TC::release_fft_plans();

        AF_MEMINFO(GetDeviceIndex(), "Regularization - after loop");

        TC::release();

        AF_MEMINFO(GetDeviceIndex(), "Regularization");

        // Ensure final result is evaluated
        m_Reconimg.eval();
        TC::release_fft_plans();

        return bSuccess;
    }

}