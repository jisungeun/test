#include <map>
#include <algorithm>

#include "colormap.h"

namespace TC {
    static ColorMap GlobalColorMap = ColorMap::Init();

    af::array& ColorMap::jet() { return colormap(COLORMAP_JET); }
    af::array& ColorMap::gray() { return colormap(COLORMAP_GRAY); }
    af::array& ColorMap::bone() { return colormap(COLORMAP_BONE); }

    ColorMap ColorMap::Init() {
        return ColorMap();
    }

    ColorMap::ColorMap() {
    }

    ColorMap::~ColorMap() {
        m_cmap.clear();
    }

    af::array& ColorMap::colormap(int type) {
        std::map<int, af::array>::iterator itr = GlobalColorMap.m_cmap.find(type);
        if (itr != GlobalColorMap.m_cmap.end()) return itr->second;

        af::array cmap = af::constant<unsigned char>(0, 256, 3, u8);

        if (type == COLORMAP_JET) {
            GlobalColorMap.prepareJet(cmap);
        } else if (type == COLORMAP_GRAY) {
            GlobalColorMap.prepareGray(cmap);
        } else if (type == COLORMAP_BONE) {
            GlobalColorMap.prepareBone(cmap);
        }

        GlobalColorMap.m_cmap[type] = cmap;

        return GlobalColorMap.m_cmap[type];
    }

    void ColorMap::prepareJet(af::array& cmap) {
        unsigned char r, g, b;
        for (unsigned int i = 0; i < 256; i++) {
            calcJet(i, r, g, b);
            cmap(i, 0) = r;
            cmap(i, 1) = g;
            cmap(i, 2) = b;
        }
    }

    void ColorMap::prepareGray(af::array& cmap) {
        for (unsigned int i = 0; i < 256; i++) {
            cmap(i, 0) = i;
            cmap(i, 1) = i;
            cmap(i, 2) = i;
        }
    }

    void ColorMap::prepareBone(af::array& cmap) {
        unsigned char r, g, b;
        for (unsigned int i = 0; i < 256; i++) {
            calcBone(i, r, g, b);
            cmap(i, 0) = r;
            cmap(i, 1) = g;
            cmap(i, 2) = b;
        }
    }

    void ColorMap::calcJet(const unsigned char val, unsigned char& r, unsigned char& g, unsigned char& b) {
#undef min
        const double v = (val / 255.0);

        //red
        if (v < 0.375) r = 0;
        else if (v < 0.623) r = (unsigned char)std::min(255.0, ((1.0 - 0.0) / (0.623 - 0.375) * (v - 0.375) + 0.0) * 255);
        else if (v < 0.890) r = 255;
        else                r = (unsigned char)std::min(255.0, ((0.5 - 1.0) / (1.0 - 0.890) * (v - 0.890) + 1.0) * 255);

        //green
        if (v < 0.125) g = 0;
        else if (v < 0.375) g = (unsigned char)std::min(255.0, ((1.0 - 0.0) / (0.375 - 0.125) * (v - 0.125) + 0.0) * 255);
        else if (v < 0.623) g = 255;
        else if (v < 0.874) g = (unsigned char)std::min(255.0, ((0.0 - 1.0) / (0.874 - 0.623) * (v - 0.623) + 1.0) * 255);
        else                g = 0;

        //blue
        if (v < 0.125) b = (unsigned char)std::min(255.0, ((1.0 - 0.5) / (0.125 - 0.0) * (v - 0.0) + 0.5) * 255);
        else if (v < 0.375) b = 255;
        else if (v < 0.623) b = (unsigned char)std::min(255.0, ((0.0 - 1.0) / (0.623 - 0.375) * (v - 0.375) + 1.0) * 255);
        else                b = 0;
    }

    void ColorMap::calcBone(const unsigned char val, unsigned char& r, unsigned char& g, unsigned char& b) {
        const double v = (val / 255.0);

        r = g = b = 255;

        if (v < 0.365079) {
            r = (unsigned char)(((0.652778 - 0.0) / (0.746032 - 0.0) * (v - 0.0)) * 255);
            g = (unsigned char)(((0.319444 - 0.0) / (0.365079 - 0.0) * (v - 0.0)) * 255);
            b = (unsigned char)(((0.444444 - 0.0) / (0.365079 - 0.0) * (v - 0.0)) * 255);
        } else if (v < 0.746032) {
            r = (unsigned char)(((0.652778 - 0.0) / (0.746032 - 0.0) * (v - 0.0)) * 255);
            g = (unsigned char)(((0.777778 - 0.319444) / (0.746032 - 0.365079) * (v - 0.365079) + 0.319444) * 255);
            b = (unsigned char)(((1.0 - 0.444444) / (1.0 - 0.365079) * (v - 0.365079) + 0.444444) * 255);
        } else {
            r = (unsigned char)(((1.0 - 0.652778) / (1.0 - 0.746032) * (v - 0.746032) + 0.652778) * 255);
            g = (unsigned char)(((1.0 - 0.777778) / (1.0 - 0.746032) * (v - 0.746032) + 0.777778) * 255);
            b = (unsigned char)(((1.0 - 0.444444) / (1.0 - 0.365079) * (v - 0.365079) + 0.444444) * 255);
        }
    }
};