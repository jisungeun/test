#pragma once

#include <memory>
#include <QString>
#include <QList>
#include <QMutex>

#include "JobParameter.h"
#include "JobItem.h"

class SystemStatus {
public:
    typedef SystemStatus Self;
    typedef std::shared_ptr<SystemStatus> Pointer;

public:
    static Pointer GetInstance(void);

    void setJobParam(const JobParameter& param) {
        QMutexLocker locker(&m_mutex);
        m_curJobParam = param;
    }

    void getJobParam(JobParameter& param) {
        QMutexLocker locker(&m_mutex);
        param = m_curJobParam;
    }

    void setJobItems(QList<JobItemPtr>& jobItems, int nRepeat = 1, double dInterval = 0.0) {
        QMutexLocker locker(&m_mutex);
        m_jobItems.clear();

        m_jobItems = jobItems;
        m_nRepeat = nRepeat;
        m_dInterval = dInterval;
    }

    void getJobItems(QList<JobItemPtr>& jobItems) {
        QMutexLocker locker(&m_mutex);
        jobItems = m_jobItems;
    }

    void getJobItems(QList<JobItemPtr>& jobItems, int& nRepeat, double& dInterval) {
        QMutexLocker locker(&m_mutex);
        jobItems = m_jobItems;
        nRepeat = m_nRepeat;
        dInterval = m_dInterval;
    }

    int getRepeat(void) {
        QMutexLocker locker(&m_mutex);
        return m_nRepeat;
    }

    double getInterval(void) {
        QMutexLocker locker(&m_mutex);
        return m_dInterval;
    }

    QString getNewAcquisitionName(void);
    void getNewAcquisitionName(QString& strParent, QString& strName);

    void setLatestAcquisitionPath(const QString& strPath) { m_strLatestAcquisitionPath = strPath; }
    QString getLatestAcquisitionPath(void) { return m_strLatestAcquisitionPath; }

    inline void setCurrentImageSize(int nSize) { m_nImageSize = nSize; }
    int getCurrentImageSize(void) { return m_nImageSize; }

    void acquisitionHTFLZOffset(const double offset) {
        m_dHTFLZOffsets.clear();
        m_dHTFLZOffsets.push_back(offset);
    }
    void acquisitionHTFLZOffset(QList<double>& offsets) {
        m_dHTFLZOffsets = offsets;
    }
    double getAcquisitionHTFLZOffset(uint ptIndex) {
        const unsigned int count = m_dHTFLZOffsets.size();
        if ((count == 0) || (ptIndex >= count)) return 0;
        return m_dHTFLZOffsets.at(ptIndex);
    }

    bool useCustomDataPath(void) const { return m_bUseCustomDataPath; }
    void useCustomDataPath(bool bUse) { m_bUseCustomDataPath = bUse; }
    QString customDataPath(void) const { return m_strCustomDataPath; }
    void customDataPath(const QString& strPath) { m_strCustomDataPath = strPath; }

protected:
    SystemStatus(void);


protected:
    JobParameter m_curJobParam;
    QString m_strLatestAcquisitionPath;

    QList<JobItemPtr> m_jobItems;
    int m_nRepeat;
    double m_dInterval;
    QList<double> m_dHTFLZOffsets;

    int m_nImageSize;

    bool m_bUseCustomDataPath;
    QString m_strCustomDataPath;

    QMutex m_mutex;
};
