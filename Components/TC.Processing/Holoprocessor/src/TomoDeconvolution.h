#pragma once

#include <QString>
#include <QObject>
#include <QMutex>
#include <QTimer>
#include <memory>

#include "aqiDataInfoStructs.h"
#include "aqi3dDecon.h"

#include "TomoDeconvolutionConfig.h"
#include "ProgressReporter.h"

class TomoDeconvolutionParam {
public:
    enum {
        DETECT_SA,
        DETECT_SAMPLE_RI,
        DETECT_SAMPLE_DEPTH
    };

public:
    TomoDeconvolutionParam();
    ~TomoDeconvolutionParam();

    bool LoadParameters(const QString& deconvConfig, int chidx = 0);
    bool LoadParameters(TomoDeconvolutionConfig& config, int chidx = 0);

protected:
    void Init(void);

public:
    aqiStructImgInfo		m_ImgInfo;
    aqiStructPsfInfo		m_PsfInInfo;
    aqiStructPsfInfo		m_PsfOutInfo;
    aqiStructDeconOpsStd	m_DeconOpsStd;
    aqiStructDeconOpsExp	m_DeconOpsExp;

    int m_nDetectMethod;
    bool m_bFastDeconvolution;
};

class TomoDeconvolutionActivator {
public:
    typedef TomoDeconvolutionActivator Self;
    typedef std::shared_ptr<Self> Pointer;

    static bool Unlock(void);
    static bool IsUnlocked(void);

protected:
    TomoDeconvolutionActivator();

    static Pointer GetInstance(void);

private:
    bool m_bUnlocked;
};

class TomoDeconvolution {
public:
    TomoDeconvolution(ProgressReporter* reporter = new NoProgressReporter());
    ~TomoDeconvolution(void);

    bool LoadParameters(const QString& strDeconvConfig, int chidx = 0);
    bool LoadParameters(TomoDeconvolutionConfig& config, int chidx = 0);
    bool SetSystemInfo(float NA, float ImmersionRI, float WaveLength);
    bool SetImageInfo(int imgWidth, int imgHeight, int imgSlices, float fPixelX, float fPixelY, float fPixelZ);
    bool SetSampleInfo(float mediumRI);
    bool Process(const QString& strInput, const QString strDir, int chidx, bool isFirstFrame);
    QString GetOutputName(void);

    QString GetLastErrorMessage(void) const;

    void UpdateProgress(double proc);

protected:
    bool Detect(TomoDeconvolutionParam& param, bool bUseStoredValue = false);
    void UpdateStatus(short nCode);
    bool IsValidParameter(TomoDeconvolutionParam& param);

private:
    TomoDeconvolutionParam m_param;
    QString m_strError;
    QString m_strOutput;
    ProgressReporter* m_pReporter;
};