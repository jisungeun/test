#define LOGGER_TAG "TCHoloprocessor::TomoBuilderFieldRetrievalSingle"
#include <TCLogger.h>

#include "TomoBuilderGlobal.h"
#include "TomoBuilderUtility.h"
#include "TomoBuilderFieldRetrievalSingle.h"

TC::TomoBuilderFieldRetrievalSingle::TomoBuilderFieldRetrievalSingle(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam)
    : TomoBuilderFieldRetrievalInterface(param, pBgImageSet, pRawImageSet, pFieldRetrievalParam) {
}

bool TC::TomoBuilderFieldRetrievalSingle::FieldRetrievalBgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool miscCallibrationOnly, bool ignorePhaseSign) {
#undef max

    //TC_MEM_INFO("before field retrieval");

    char fname_in[128] = { 0, };
    const char* fname_out = "field_retrieval_bg.arr";

    const double pixelSize = m_parameter[PN_PIXEL_SIZE];
    const double mag = m_parameter[PN_M];
    const double NA = m_parameter[PN_NA];
    const double lambda = m_parameter[PN_LAMBDA];
    const double PhaseSign = (ignorePhaseSign) ? -1 : m_parameter[PN_PHASE_SIGN];
    const double res = pixelSize / mag;

    m_pFieldRetrievalParam->res = res;

    af::array& bg_data = m_pBgImageSet->arrData;

    dbg_saveArray("arr_bg_data", bg_data, fname_out, false);

    //! MATLAB:  rawSize=size(img,1);
    const unsigned int rawSize = m_pBgImageSet->width();
    m_pFieldRetrievalParam->rawSize = rawSize;

    const unsigned int squre_of_rawSize = std::pow(rawSize, 2);

    //! MATLAB: img=squeeze(bg_data(:,:,1));
    af::array img = af::moddims(bg_data(af::span, af::span, 0), bg_data.dims(0), bg_data.dims(1));

    //! MATLAB: Fimg = fftshift(fft2(img))/(rawSize^2); %FFT
    af::array Fimg = fftshift(af::fft2(img)) / squre_of_rawSize;

    dbg_saveArray("arr_img", img, fname_out, true);
    dbg_saveArray("arr_Fimg", Fimg, fname_out, true);

    const unsigned int c0 = std::round(rawSize * 0.55);
    const unsigned int c1 = std::round(rawSize * 0.99); //TOMV-789

    //! MATLAB: FimgMax = max(max(Fimg(:, round(rawSize*0.55):round(rawSize*0.99) )));
    af::cfloat FimgMax = af::max<af::cfloat>(Fimg(af::span, af::seq(c0, c1)));

    //! MATLAB: [mi,mj]=find(Fimg==FimgMax);
    unsigned int& mi = m_pFieldRetrievalParam->mi;
    unsigned int& mj = m_pFieldRetrievalParam->mj;
    Find(Fimg == FimgMax, mi, mj);

    //! MATLAB: mi=round(mi-rawSize/2-1); mj=round(mj-rawSize/2-1);
    mi = std::round(mi - rawSize / 2.0 - 1);
    mj = std::round(mj - rawSize / 2.0 - 1);

    //! MATLAB: r=round(rawSize*res*NA/lambda);
    unsigned int r = std::round(rawSize * res * NA / lambda);

    //! MATLAB: yr = r;
    unsigned int yr = r;

    //! MATLAB: c1mask = ~(mk_ellipse(r,yr,rawSize,rawSize));
    af::array tmpMask;
    mk_ellipse(tmpMask, r, yr, rawSize, rawSize);

    dbg_saveArray("arr_tmpMask", tmpMask, fname_out, true);

    af::array c1mask = !(tmpMask);

    dbg_saveArray("arr_c1mask", c1mask, fname_out, true);

    //! MATLAB: c3mask = circshift(c1mask,[mi mj]);
    af::array c3mask = af::shift(c1mask, mi + 1, mj + 1);
    m_pFieldRetrievalParam->c3mask = c3mask;

    dbg_saveArray("arr_c3mask", c3mask, fname_out, true);

    const unsigned int nBgSize = (onlyNormalOnBg) ? 1 : m_pBgImageSet->images();

    //! MATLAB: f_dx= zeros(size(bg_data,3)-1,1);
    //! MATLAB: f_dy= zeros(size(bg_data,3)-1,1);
    m_pFieldRetrievalParam->f_dx = af::constant(0, nBgSize);
    m_pFieldRetrievalParam->f_dy = af::constant(0, nBgSize);

    af::array& f_dx = m_pFieldRetrievalParam->f_dx;
    af::array& f_dy = m_pFieldRetrievalParam->f_dy;

    //! MATALB: fsMargin = 2*round(r/6);
    const unsigned int fsMargin = 2 * std::round(r / 6.0);

    //! MATLAB:  Fbg=zeros(round(2*r)+fsMargin,round(2*r)+fsMargin,bgEnd-bgStart+1,'single');
    unsigned int& nArrSize = m_pFieldRetrievalParam->nArrSize;
    const unsigned int nArrSize_no_padding = std::round(2 * r) + fsMargin;
    nArrSize = (zeroPadding == 0) ? nArrSize_no_padding : zeroPadding;

    //QLOG_INFO() << "pixel=" << pixelSize << " mag=" << mag << "NA=" << NA << " rawSize=" << rawSize << " r=" << r << " fsMargin=" << fsMargin << " nArrSize=" << nArrSize;

    af::array& Fbg = m_pFieldRetrievalParam->Fbg;
    Fbg = af::constant(0, nArrSize, nArrSize, nBgSize, c32);

    af::array& bp_masks = m_pFieldRetrievalParam->bp_masks;
    bp_masks = af::constant(0, nArrSize_no_padding, nArrSize_no_padding, nBgSize, b8);

    unsigned int& a0 = m_pFieldRetrievalParam->a0;
    unsigned int& a1 = m_pFieldRetrievalParam->a1;

    a0 = (rawSize / 2 - r - fsMargin / 2 + 1) - 1;
    a1 = (rawSize / 2 + r + fsMargin / 2) - 1;
    const unsigned int sizeFimg = nArrSize;

    unsigned int& idx_offset = m_pFieldRetrievalParam->idx_offset;

    if (onlyNormalOnBg) idx_offset = 0;		//! there is only normal angle image
    else if (hasNormal) 									//! the first image of bg is normal angle
    {
        m_pFieldRetrievalParam->hasNormalSample = true;

        if (miscCallibrationOnly) {
            //caution: it must have normal angle image on fg image set if this function is called at calibration stage...
            idx_offset = 0;
        } else {
            //caution: m_pRawImageSet is empty when miscCalibrationOnly is true
            if (nBgSize == m_pRawImageSet->images())	//! fg also has normal angle
            {
                idx_offset = 0;
            } else idx_offset = 1;
        }
    } else                idx_offset = 0;		//! there is no normal angle image (not common)

    for (unsigned int i = 0; i < nBgSize; i++) {
#ifdef DEEP_DEBUG
        sprintf_s<128>(fname_in, "bg_loop_%02d.arr", i);
#endif
        //! MATLAB: img=squeeze(bg_data(:,:,bgNum));
        af::array img = bg_data(af::span, af::span, i);
        //img = af::moddims(img, img.dims(0), img.dims(1));

        dbg_saveArray("arr_img", img, fname_in, false);

        //! MATLAB: Fimg = fftshift(fft2(img))/(rawSize^2);
        Fimg = fftshift(af::fft2(img)) / squre_of_rawSize;

        dbg_saveArray("arr_Fimg_1", Fimg, fname_in, true);

        //! MATLAB: [f_x,f_y]=find(Fimg==max(max(Fimg(:, round(rawSize*0.55):round(rawSize*0.99) ))));
        FimgMax = af::max<af::cfloat>(Fimg(af::span, af::seq(c0, c1)));

        unsigned int f_x, f_y;
        Find(Fimg == FimgMax, f_x, f_y);

        //! MATLAB: f_dx(bgIdx)=f_x;
        f_dx(i) = f_x;

        //! MATLAB: f_dy(bgIdx)=f_y;
        f_dy(i) = f_y;

        dbg_saveArray("arr_f_dx", f_dx, fname_in, true);
        dbg_saveArray("arr_f_dy", f_dy, fname_in, true);

        //! MATLAB: Fimg = Fimg.*c3mask;
        Fimg = Fimg * c3mask;

        dbg_saveArray("arr_Fimg_2", Fimg, fname_in, true);

        //! MATLAB: Fimg = circshift(Fimg,-[round(mi) round(mj)]);
        Fimg = af::shift(Fimg, -1 * (mi + 1), -1 * (mj + 1));

        dbg_saveArray("arr_Fimg_3", Fimg, fname_in, true);

        //! MATLAB: Fimg = Fimg(rawSize/2-r-fsMargin/2+1:rawSize/2+r+fsMargin/2,rawSize/2-r-fsMargin/2+1:rawSize/2+r+fsMargin/2);
        af::array Fimg2 = Fimg(af::seq(a0, a1), af::seq(a0, a1));

        dbg_saveArray("arr_Fimg_4", Fimg2, fname_in, true);

        //! MATLAB: mask = GenerateBandPassMask(Fimg, r)
        af::array bp_mask = GenerateBandPassMask(Fimg2, r);
        const int _x = bp_mask.dims(0);
        const int _y = bp_mask.dims(1);
        bp_masks(af::span, af::span, i) = bp_mask(af::span, af::span);

        //! MATALB: Fimg = Fimg.*mask;
        Fimg2 = Fimg2 * bp_mask;

        //! MATLAB: Fimg = ifft2(ifftshift(Fimg))*(sizeFimg^2);
        if (zeroPadding > 0) Fimg2 = zeropadding_2d(Fimg2, zeroPadding, true);
        Fimg2 = af::ifft2(ifftshift(Fimg2)) * pow(sizeFimg, 2);

        dbg_saveArray("arr_Fimg_5", Fimg2, fname_in, true);

        //! MATLAB: Fbg(:,:,bgIdx)=Fimg;
        Fbg(af::span, af::span, i) = Fimg2(af::span, af::span);

        dbg_saveArray("arr_Fbg", Fbg, fname_in, true);
    }

    dbg_saveArray("arr_Fbg", Fbg, fname_out, true);
    dbg_saveArray("arr_f_dx", f_dx, fname_out, true);
    dbg_saveArray("arr_f_dy", f_dy, fname_out, true);

    // Ensure final result is evaluated
    bp_masks.eval();
    f_dx.eval();
    f_dy.eval();
    Fbg.eval();

    m_pBgImageSet->clear();

    return true;
}

bool TC::TomoBuilderFieldRetrievalSingle::FieldRetrievalFgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool ignorePhaseSign, bool chkQuality) {
#undef max

    //TC_MEM_INFO("before field retrieval");

    char fname_in[128] = { 0, };
    const char* fname_out = "field_retrieval_fg.arr";

    const double PhaseSign = (ignorePhaseSign) ? -1 : m_parameter[PN_PHASE_SIGN];
    const double res = m_pFieldRetrievalParam->res;

    const unsigned int rawSize = m_pFieldRetrievalParam->rawSize;
    const unsigned int squre_of_rawSize = std::pow(rawSize, 2);

    af::array& raw_data = m_pRawImageSet->arrData;

    dbg_saveArray("arr_raw_data", raw_data, fname_out, true);

    //Sample field
    const unsigned int nFgSize = m_pRawImageSet->images();
    const unsigned int nArrSize = m_pFieldRetrievalParam->nArrSize;
    const unsigned int mi = m_pFieldRetrievalParam->mi;
    const unsigned int mj = m_pFieldRetrievalParam->mj;
    const unsigned int a0 = m_pFieldRetrievalParam->a0;
    const unsigned int a1 = m_pFieldRetrievalParam->a1;
    const unsigned int sizeFimg = nArrSize;
    const unsigned int idx_offset = m_pFieldRetrievalParam->idx_offset;
    af::array& Fbg = m_pFieldRetrievalParam->Fbg;
    af::array& bp_masks = m_pFieldRetrievalParam->bp_masks;

    //! MATLAB: retPhase=zeros(round(2*r)-4,round(2*r)-4,size(raw_data,3),'single');
    m_pFieldRetrievalParam->retAmplitude = af::constant(0, nArrSize - 4, nArrSize - 4, nFgSize);

    //! MATLAB: retAmplitude=zeros(round(2*r)-4,round(2*r)-4,size(raw_data,3),'single');
    m_pFieldRetrievalParam->retPhase = af::constant(0, nArrSize - 4, nArrSize - 4, nFgSize);

    af::array& retAmplitude = m_pFieldRetrievalParam->retAmplitude;
    af::array& retPhase = m_pFieldRetrievalParam->retPhase;
    auto& fieldQuality = m_pFieldRetrievalParam->fieldQuality;
    auto& badFields = m_pFieldRetrievalParam->badFields;
    auto& residues = m_pFieldRetrievalParam->residues;
    fieldQuality.resize(nFgSize, true);

    for (unsigned int fgIdx = 0; fgIdx < nFgSize; fgIdx++) {
#ifdef DEEP_DEBUG
        sprintf_s<128>(fname_in, "sample_loop_%02d.arr", fgIdx);
#endif
        //! MATLAB: img = squeeze(raw_data(:,:,iter));
        af::array img = raw_data(af::span, af::span, fgIdx);
        img = af::moddims(img, img.dims(0), img.dims(1));

        dbg_saveArray("arr_img", img, fname_in, false);

        //! MATLAB: Fimg = fftshift(fft2(img))/(rawSize^2);
        af::array Fimg = fftshift(af::fft2(img)) / squre_of_rawSize;

        dbg_saveArray("arr_Fimg_1", Fimg, fname_in, true);

        //! MATLAB: Fimg = Fimg.*c3mask
        af::array& c3mask = m_pFieldRetrievalParam->c3mask;
        Fimg = Fimg * c3mask;

        dbg_saveArray("arr_Fimg_2", Fimg, fname_in, true);

        //! MATLAB: Fimg = circshift(Fimg,-[round(mi) round(mj)]);
        Fimg = af::shift(Fimg, -1 * (mi + 1), -1 * (mj + 1));

        dbg_saveArray("arr_Fimg_3", Fimg, fname_in, true);

        //! MATLAB: Fimg = Fimg(rawSize/2-r+1:rawSize/2+r,rawSize/2-r+1:rawSize/2+r);
        af::array Fimg2 = Fimg(af::seq(a0, a1), af::seq(a0, a1));

        dbg_saveArray("arr_Fimg_4", Fimg2, fname_in, true);

        const int fbgIdx = (onlyNormalOnBg) ? 0 : (fgIdx + idx_offset);

        //! MATLAB: Fimg = Fimg.*squeeze(masks(:,:,iter));
        af::array bp_mask = bp_masks(af::span, af::span, fbgIdx);
        Fimg2 = Fimg2 * bp_mask;

        //! MATLAB: Fimg = ifft2(ifftshift(Fimg))*(sizeFimg^2);
        if (zeroPadding > 0) Fimg2 = zeropadding_2d(Fimg2, zeroPadding, true);
        Fimg2 = af::ifft2(ifftshift(Fimg2)) * pow(sizeFimg, 2);

        dbg_saveArray("arr_Fimg_5", Fimg2, fname_in, true);

        //! MATLAB: Fimg=Fimg./squeeze(Fbg(:,:, iter));
        Fimg2 = Fimg2 / Fbg(af::span, af::span, fbgIdx);

        dbg_saveArray("arr_Fimg_6", Fimg2, fname_in, true);

        //! MATLAB: Fimg=Fimg(3:end-2,3:end-2);
        Fimg2 = Fimg2(af::seq(2, af::end - 2), af::seq(2, af::end - 2));

        dbg_saveArray("arr_Fimg_7", Fimg2, fname_in, true);

        //! MATLAB: retAmplitude(:,:,iter)=abs(Fimg);
        retAmplitude(af::span, af::span, fgIdx) = af::abs(Fimg2);

        dbg_saveArray("arr_retAmplitue", retAmplitude, fname_in, true);

        //! MATLAB: ptemp=(PhaseSign)*(unwrap2(double(angle(Fimg))));
        af::array Fimg2Angle = angle(Fimg2);
        dbg_saveArray("arr_Fimg_Angle", Fimg2Angle, fname_in, true);

        bool bGoodQuality = false;
        int score = 0;
        af::array ptemp = PhaseSign * unwrap2(Fimg2Angle, bGoodQuality, score);
        dbg_saveArray("arr_ptemp_1", ptemp, fname_in, true);

        residues[fgIdx] = score;
        if (chkQuality) {
            fieldQuality[fgIdx] = ((fgIdx == 0) || bGoodQuality);    //! Assume that the field of normal angle is always good
            if (!bGoodQuality) {
                QLOG_INFO() << "> low quality field=" << fgIdx << " score=" << score;
                badFields[fgIdx] = score;
            }
        } else {
            fieldQuality[fgIdx] = true;
        }

        //! MATLAB: ptemp=ptemp-median(median(ptemp)); % mask�� ����� ���� �켱 global phase�� 0���� ��Ͽ� ������
        const float median_v = af::median<float>(ptemp);
        ptemp = ptemp - median_v;

        dbg_saveArray("arr_median_v", af::constant<float>(median_v, 1, f32), fname_in, true);
        dbg_saveArray("arr_ptemp_2", ptemp, fname_in, true);

        //! MATLAB: pmap=ptemp<0.5;
        af::array pmap = ptemp < 0.5;
        dbg_saveArray("arr_pmap", pmap, fname_in, true);

        //! MATLAB: ptemp=PhiShiftJH(ptemp,pmap);
        ptemp = TC::PhiShiftJH(ptemp, pmap);
        dbg_saveArray("arr_ptemp_3", ptemp, fname_in, true);

        //! MATLAB: ptemp=ptemp-sum(sum(ptemp.*pmap))./sum(sum(pmap));
        ptemp = ptemp - af::sum<float>(ptemp * pmap) / af::sum<float>(pmap);
        dbg_saveArray("arr_ptemp_4", ptemp, fname_in, true);

        //! MATLAB: retPhase(:,:,iter)=ptemp;
        retPhase(af::span, af::span, fgIdx) = ptemp;

        dbg_saveArray("arr_retPhase", retPhase, fname_in, true);
    }

    dbg_saveArray("arr_retAmplitude", retAmplitude, fname_out, true);
    dbg_saveArray("arr_retPhase", retPhase, fname_out, true);

    // Ensure final result is evaluated
    retAmplitude.eval();
    retPhase.eval();

    return true;
}

af::array TC::TomoBuilderFieldRetrievalSingle::GenerateBandPassMask(af::array& Fimg, unsigned int radius) {
    //!MATLAB: [f_x, f_y] = find(Fimg == max(Fimg(:)));
    af::cfloat FimgMax = af::max<af::cfloat>(Fimg(af::span, af::span));

    unsigned int f_x, f_y;
    Find(Fimg == FimgMax, f_x, f_y);

    //!MATLAB: sizeFimg = size(Fimg, 1);
    unsigned int sizeFimg = Fimg.dims(0);

    //!MATLAB: mask = ~(mk_mask_cricle(r, f_y, f_x, sizeFimg, sizeFimg));
    af::array mask = !(TC::mk_mask_circle(radius, f_y, f_x, sizeFimg, sizeFimg));

    //!MATLAB: mask_center = ~(mk_mask_circle(25, f_y, f_x, sizeFimg, sizeFimg));
    af::array mask_center = !(TC::mk_mask_circle(25, f_y, f_x, sizeFimg, sizeFimg, 3));

    //!MATLAB: Fimg = Fimg.*mask;
    af::array Fimg_masked = Fimg * mask;

    //!MATLAB: Fimg = Fimg.*(~mask_center);
    Fimg_masked = Fimg_masked * (!mask_center);

    //!MATLAB: [f_xs,f_ys]=find(abs(Fimg)>0.1);
    af::array noise = af::abs(Fimg_masked) > 0.1;

#if 0
    af::array f_xs, f_ys;
    TC::Find(noise, f_ys, f_xs);

    static int idx = 0;
    char fname[256] = { 0, };

    sprintf_s<256>(fname, "bandpass_%03d.arr", idx++);

    af::saveArray("Fimg", Fimg, fname, false);
    af::saveArray("mask", mask, fname, true);
    af::saveArray("mask_center", mask_center, fname, true);
    af::saveArray("Fimg_masked", Fimg_masked, fname, true);
    af::saveArray("noise", noise, fname, true);
    af::saveArray("f_xs", f_xs, fname, true);
    af::saveArray("f_ys", f_ys, fname, true);
#endif

    af::array dilate_mask = af::constant(1, 4, 4);
    noise = af::dilate(noise, dilate_mask);

    mask = mask & (!noise);

    //!MATLAB: mask = mask | mask_center;
    mask = mask | mask_center;

    mask.eval();

    return mask;
}
