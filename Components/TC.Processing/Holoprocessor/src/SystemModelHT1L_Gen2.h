#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"


class SystemModelHT1L_Gen2 : public SystemModelInterface {
public:
    typedef SystemModelHT1L_Gen2 Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    virtual QString Model() const override { return "HT-1L Gen2"; }
    virtual int Generation() const override { return 2; }

    virtual int MaxFramerate(unsigned int pixels) override {
        return (pixels < 700) ? 130 : 100;
    }

    //Laser
    virtual unsigned int Wavelength() override { return 532; }

    //Camera
    virtual QString Camera() override { return "FLIR BFS-U3-28S5M"; }

    //Objective Lens
    virtual double ObjectiveLensMagnification() override { return 58.33; }
    virtual double ObjectiveLensNA() override { return 1.2; }
    virtual double ObjectiveLensReadyPos() override { return 0.0; }
    virtual double ObjectiveLensLimitPos() override { return 1.0; }

    //Condenser Lens
    virtual double CondenserLensMagnification() override { return 60; }
    virtual double CondenserLensNA() override { return 0.7; }
    virtual double CondenserLensReadyPos() override { return 19.0; }
    virtual double CondenserLensLimitPos() override { return 20.0; }

    //Condenser Lens Auto Calibration
    virtual double CondenserLensStepAmount(TC::CalibrationStepType type) override {
        double step = 0;

        switch (type) {
        case TC::STEP_OUTOFRANGE:
            step = 0.035;
            break;
        case TC::STEP_FAR:
            step = 0.01;
            break;
        case TC::STEP_NEAR:
            step = 0.002;
            break;
        case TC::STEP_FINE:
            step = 0.001;
            break;
        case TC::STEP_ESTIMATION:
            step = 0.0005;
            break;
        }

        return step;
    }

    virtual double CondenserLensDistThreshold(TC::CalibrationDistThreshold type) override {
        double threshold = 0;

        switch (type) {
        case TC::DIST_NEAR_THRESHOLD:
            threshold = 0.2;
            break;
        case TC::DIST_FINE_THRESHOLD:
            threshold = 0.05;
            break;
        }

        return threshold;
    }

    //Processing
    virtual unsigned int ZeroPaddingMax2D() override { return 256; }
    virtual unsigned int ZeroPaddingMax3D() override { return 312; }
    virtual unsigned int NumberOfIteration() override { return 40; }

    //Fluorscence
    virtual double FluorescenceScanDepth() override { return 0; }
    virtual double FluorescenceScanStep() override { return 0; }
    virtual double FluorescenceScanStepUnit() override { return 0.050; }
    virtual double DarkpixelCalibrationGain() override { return 40; }
    virtual double DarkpixelCalibrationExposure() override { return 20; }

    //Motion
    virtual double MotionMovableLowerX() override { return -4.0; }
    virtual double MotionMovableUpperX() override { return  4.0; }
    virtual double MotionMovableLowerY() override { return -4.0; }
    virtual double MotionMovableUpperY() override { return  4.0; }

    virtual bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) override {
        if (tarCPos <= curCPos) return true;
        return (tarCPos <= (24.5 + curZPos));
    }

    virtual bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) override {
        if (tarZPos >= curZPos) return true;
        return (tarZPos >= (curCPos - 24.5));
    }

    //=== Model-specific functionality ===
    virtual bool doesSupportXYZControl() override { return true; }
    virtual bool doesSupportFluorescence() override { return false; }
    virtual bool doesSupportBrightfield() override { return true; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase | Modes::BrightFieldMono);
    }
};

