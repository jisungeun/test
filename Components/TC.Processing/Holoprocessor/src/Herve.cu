#define LOGGER_TAG "TCHoloprocessor::FilteredUnwrap"
#include <TCLogger.h>

#include "CudaErrorLog.h"

#include "Herve.h"
#include "mycuMirror.h"
#include "mycuPQarray.h"
#include "mycuArrayCal.h"

void HerveUnwrap_optimized_revised(float* devicePointerWrappedPhase, float* devicePointerUnwrappedPhase,
    const int arraySizeX, const int arraySizeY) {

    const auto mirroredArraySizeX = 2 * arraySizeX;
    const auto mirroredArraySizeY = 2 * arraySizeY;
    const auto mirrorArrayDataLength = mirroredArraySizeX * mirroredArraySizeY;

    const auto threadPerBlock = 32;
    const auto arrayBlockNumberX = ((arraySizeX - 1) / threadPerBlock) + 1;
    const auto arrayBlockNumberY = ((arraySizeY - 1) / threadPerBlock) + 1;
    const auto mirroredArrayBlockNumberX = ((mirroredArraySizeX - 1) / threadPerBlock) + 1;
    const auto mirroredArrayBlockNumberY = ((mirroredArraySizeY - 1) / threadPerBlock) + 1;


    dim3 threads(threadPerBlock, threadPerBlock);
    dim3 arrayBlocks(arrayBlockNumberX, arrayBlockNumberY);
    dim3 mirrorBlocks(mirroredArrayBlockNumberX, mirroredArrayBlockNumberY);

    cudaError_t cudaError;

    cufftComplex* devicePointerBufferArray;
    cudaError = cudaMalloc((void**)&devicePointerBufferArray, sizeof(cufftComplex) * mirrorArrayDataLength);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMalloc devicePointerBufferArray failed");
        return;
    }

    cufftComplex* devicePointerExpMirrorPhase;
    cudaError = cudaMalloc((void**)&devicePointerExpMirrorPhase, sizeof(cufftComplex) * mirrorArrayDataLength);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMalloc devicePointerExpMirrorPhase failed");
        return;
    }

    int* devicePointerPSqaureQSqaureArray;
    cudaError = cudaMalloc((void**)&devicePointerPSqaureQSqaureArray, sizeof(int) * mirrorArrayDataLength);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMalloc devicePointerPSqaureQSqaureArray failed");
        return;
    }

    make_P2Q2array << < mirrorBlocks, threads >> > (devicePointerPSqaureQSqaureArray, mirroredArraySizeX, mirroredArraySizeY);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "make_P2Q2array failed");
        return;
    }

    makeExpImagMirrorImage << < mirrorBlocks, threads >> > (devicePointerWrappedPhase, devicePointerExpMirrorPhase, arraySizeX, arraySizeY);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "makeExpImagMirrorImage failed");
        return;
    }

    cufftHandle planC2C;
    cufftResult_t cufftResult;
    cufftResult = cufftPlan2d(&planC2C, mirroredArraySizeX, mirroredArraySizeY, CUFFT_C2C);
    if (cufftResult != CUFFT_SUCCESS) {
        LogCufftPlanError(cufftResult, "cufftPlan2d", mirroredArraySizeX, mirroredArraySizeY, CUFFT_C2C);
        return;
    }

    cufftResult = cufftExecC2C(planC2C, devicePointerExpMirrorPhase, devicePointerBufferArray, CUFFT_FORWARD);
    if (cufftResult != CUFFT_SUCCESS) {
        LogCufftExecError(cufftResult, "cufftExecError", "devicePointerExpMirrorPhase", "devicePointerBufferArray",
            CUFFT_FORWARD);
        return;
    }

    ElementMultiply << <mirrorBlocks, threads >> > (devicePointerPSqaureQSqaureArray, devicePointerBufferArray,
        devicePointerBufferArray, mirrorArrayDataLength);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "ElementMultiply failed");
        return;
    }

    cufftResult = cufftExecC2C(planC2C, devicePointerBufferArray, devicePointerBufferArray, CUFFT_INVERSE);
    if (cufftResult != CUFFT_SUCCESS) {
        LogCufftExecError(cufftResult, "cufftExecC2C", "devicePointerBufferArray", "devicePointerBufferArray",
            CUFFT_INVERSE);
        return;
    }

    Element_opt1 << <mirrorBlocks, threads >> > (devicePointerBufferArray, devicePointerBufferArray,
        mirrorArrayDataLength, devicePointerExpMirrorPhase, mirrorArrayDataLength);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "Element_opt1 failed");
        return;
    }

    cufftResult = cufftExecC2C(planC2C, devicePointerBufferArray, devicePointerBufferArray, CUFFT_FORWARD);
    if (cufftResult != CUFFT_SUCCESS) {
        LogCufftExecError(cufftResult, "cufftExecC2C", "devicePointerBufferArray", "devicePointerBufferArray",
            CUFFT_FORWARD);
        return;
    }

    ElementDivide << <mirrorBlocks, threads >> > (devicePointerBufferArray, devicePointerPSqaureQSqaureArray,
        devicePointerBufferArray, mirrorArrayDataLength);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "ElementDivide failed");
        return;
    }

    cufftResult = cufftExecC2C(planC2C, devicePointerBufferArray, devicePointerBufferArray, CUFFT_INVERSE);
    if (cufftResult != CUFFT_SUCCESS) {
        LogCufftExecError(cufftResult, "cufftExecC2C", "devicePointerBufferArray", "devicePointerBufferArray",
            CUFFT_INVERSE);
        return;
    }

    ElementDivide << <mirrorBlocks, threads >> > (devicePointerBufferArray, mirrorArrayDataLength,
        devicePointerBufferArray, mirrorArrayDataLength);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "ElementDivide failed");
        return;
    }

    extract_from_mirror << <arrayBlocks, threads >> > (devicePointerBufferArray, devicePointerUnwrappedPhase,
        mirroredArraySizeX, mirroredArraySizeY, arraySizeX, arraySizeY);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "extract_from_mirror failed");
        return;
    }

    cufftDestroy(planC2C);
    cudaFree(devicePointerExpMirrorPhase);
    cudaFree(devicePointerPSqaureQSqaureArray);
    cudaFree(devicePointerBufferArray);
}