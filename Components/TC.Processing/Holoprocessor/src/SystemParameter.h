#pragma once

#include <QString>
#include <memory>
#include <QSettings>

#include "GlobalDefines.h"

class LaserParam {
public:
    LaserParam();
    LaserParam(const LaserParam& param);

    bool operator==(const LaserParam& param);
    bool operator!=(const LaserParam& param) { return !(*this == param); }

    int wavelength(void);
    void wavelength(int nWaveLength);

    double wavelengthUM(void);

protected:
    int m_nWaveLength;		//! wavelength in nanometer
};

class CameraParam {
public:
    CameraParam();
    CameraParam(const CameraParam& param);

    bool operator==(const CameraParam& param);
    bool operator!=(const CameraParam& param) { return !(*this == param); }

    QString modelName(void);
    void modelName(const QString& strName);

    double pixelSizeH(void);
    double pixelSizeV(void);
    int maxPixelsH(void);
    int maxPixelsV(void);
    double acqusitionTime(int fovH, int fovV);
    int fovStep(void);
    double maxGain(void);
    double minExposure(void) const;
    double maxExposure(void) const;
    auto exposure(void) const;
    auto outputBitsInHDR(void) const->int;

    TC::CameraType type(void);

protected:
    QString m_strModelName;		//! camera model name
};

class LensParam {
public:
    LensParam();
    LensParam(const LensParam& param);

    bool operator==(const LensParam& param);
    bool operator!=(const LensParam& param) { return !(*this == param); }

    double magnification(void);
    void magnification(double fMag);

    double NA(void);
    void NA(double dNA);

protected:
    double m_dMagnification;	//! magnification
    double m_dNA;				//! Numerical apeature
};

class HostParam {
public:
    HostParam();
    HostParam(const HostParam& param);

    bool operator==(const HostParam& param);
    bool operator!=(const HostParam& param) { return !(*this == param); }

    int ZPMax(void);
    void ZPMax(int nZPMax);

    int ZP2Max(void);
    void ZP2Max(int nZP2Max);

    double ObjectiveReadyPos(void);
    void ObjectiveReadyPos(double dPos);

    double ObjectiveLimitPos(void);
    void ObjectiveLimitPos(double dPos);

    double CondenserReadyPos(void);
    void CondenserReadyPos(double dPos);

    double CondenserUserFocusPos(void);
    void CondenserUserFocusPos(double dPos);

    double CondenserLimitPos(void);
    void CondenserLimitPos(double dPos);

    double HTFLZOffsetCompensation(void);
    void HTFLZOffsetCompensation(double dValue);

protected:
    int m_nZPMax;			//! max value of zero-padding
    int m_nZP2Max;			//! max value of zero-padding 2

    double m_dObjectiveReadyPos;		//! condenser len's ready position (um)
    double m_dObjectiveLimitPos;		//! condenser len's limit position (um)
    double m_dCondenserReadyPos;		//! condenser len's ready position (um)
    double m_dCondenserLimitPos;		//! condenser len's limit position (um)
    double m_dCondenserUserFocusPos;	//! condenser len's user-defined focus position (um)

    double m_dHTFLZOffsetCompensation;    //! HT/FL Z Offset compensation (nm)
};

class MotionControllerParam {
public:
    MotionControllerParam();
    MotionControllerParam(const MotionControllerParam& param);

    bool operator==(const MotionControllerParam& param);
    bool operator!=(const MotionControllerParam& param) { return !(*this == param); }

    QString Port(void) const;
    void Port(const QString& strPort);

protected:
    QString m_strPort;			//! COMM Port
};

class SystemParameterData {
public:
    SystemParameterData();
    SystemParameterData(const SystemParameterData& other);

    void operator=(const SystemParameterData& other);
    bool operator==(const SystemParameterData& other);
    bool operator!=(const SystemParameterData& other) { return !(*this == other); }

    double maxFOVH(void);
    double maxFOVV(void);

    double sysMagnification(void);
    double bfMagnification(void);

    void setModel(const QString& model);
    QString getModel(void) const;

    void setMinimumShutter(double dValue);
    double getMinimumShutter(void) const;

    int PhaseSign(void);
    int MappingSign(void);

    void setDMDInformation(const QString& strModel, const QString& strType, const int majorVersion);
    void setSerial(const QString& strSerial);
    QString getSerial(void) const;
    QString getSerialAsLow(void) const;

    void setHostName(const QString& strHost);
    QString getHostName(void) const;

    void setSoftwareVersion(const QString& strVersion);
    QString getSoftwareVersion(void) const;

    TC::DMDPatternType getDMDPatternType(void) const;

    bool supportDMDFastSwitching(void) const;

protected:
    void set(const SystemParameterData& other);

public:
    LaserParam				m_laser;
    CameraParam				m_camera;
    CameraParam				m_camera_bf;
    LensParam				m_objectiveLens;
    LensParam				m_condenserLens;
    HostParam				m_host;
    MotionControllerParam	m_motion;
    QString m_strModel;

    //DMD Firmware information...
    QString m_strDMDModel;
    QString m_strDMDPatternType;
    int m_nDMDMajorVersion;
    double m_dMiniumShutter;

    QString m_strSerial;
    QString m_strHostName;
    QString m_strSWVersion;
};

class SystemParameter : public SystemParameterData {
public:
    typedef SystemParameter Self;
    typedef std::shared_ptr<SystemParameter> Pointer;

public:

    static Pointer GetInstance(void);
    ~SystemParameter();

    bool isEqual(const SystemParameterData& other);

protected:
    SystemParameter();
};
