#define LOGGER_TAG "TCHoloprocessor::SystemParameter"
#include <TCLogger.h>

#include <QCoreApplication>

#include "SystemParameter.h"
#include "CameraModels.h"

LaserParam::LaserParam() {
    m_nWaveLength = 532;
}

LaserParam::LaserParam(const LaserParam& param) {
    m_nWaveLength = param.m_nWaveLength;
}

int LaserParam::wavelength(void) {
    return m_nWaveLength;
}

double LaserParam::wavelengthUM(void) {
    return m_nWaveLength / 1000.f;
}

void LaserParam::wavelength(int nWaveLength) {
    m_nWaveLength = nWaveLength;
    QLOG_INFO() << "Laser.Wavelength: " << m_nWaveLength;
}

bool LaserParam::operator==(const LaserParam& param) {
    return (m_nWaveLength == param.m_nWaveLength);
}

CameraParam::CameraParam() {
    m_strModelName = "Unknown";
}

CameraParam::CameraParam(const CameraParam& param) {
    m_strModelName = param.m_strModelName;
}

bool CameraParam::operator==(const CameraParam& param) {
    return (m_strModelName == param.m_strModelName);
}

QString CameraParam::modelName(void) {
    return m_strModelName;
}

void CameraParam::modelName(const QString& strName) {
    m_strModelName = strName;
    QLOG_INFO() << "Camera.Model: " << m_strModelName;
}

double CameraParam::pixelSizeH(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->pixelSizeH();
}

double CameraParam::pixelSizeV(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->pixelSizeV();
}

int CameraParam::maxPixelsH(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->maxPixelsH();
}

int CameraParam::maxPixelsV(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->maxPixelsV();
}

double CameraParam::acqusitionTime(int fovH, int fovV) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->acqusitionTime(fovH, fovV);
}

int CameraParam::fovStep(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->fovStep();
}

double CameraParam::maxGain(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);
    return camera->maxGain();
}

double CameraParam::minExposure(void) const {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);
    return camera->minExposure();
}

double CameraParam::maxExposure(void) const {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);
    return camera->maxExposure();
}

auto CameraParam::exposure(void) const {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);
    return camera->exposure();
}

auto CameraParam::outputBitsInHDR(void) const->int {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);
    return camera->outputBitsInHDR();
}

TC::CameraType CameraParam::type(void) {
    CameraModelsPtr camera = CameraModels::GetInstance(m_strModelName);

    return camera->type();
}

LensParam::LensParam() {
    m_dMagnification = 60.0;
    m_dNA = 0.8;
}

LensParam::LensParam(const LensParam& param) {
    m_dMagnification = param.m_dMagnification;
    m_dNA = param.m_dNA;
}

bool LensParam::operator==(const LensParam& param) {
    if (m_dMagnification != param.m_dMagnification) return false;
    if (m_dNA != param.m_dNA) return false;

    return true;
}

double LensParam::magnification(void) {
    return m_dMagnification;
}

void LensParam::magnification(double dMag) {
    m_dMagnification = dMag;
    QLOG_INFO() << "ObjectiveLens.Magnification: " << m_dMagnification;
}

double LensParam::NA(void) {
    return m_dNA;
}

void LensParam::NA(double dNA) {
    m_dNA = dNA;
    QLOG_INFO() << "ObjectiveLens.NA: " << m_dNA;
}

HostParam::HostParam() {
    m_nZPMax = 256;
    m_nZP2Max = 380;
    m_dCondenserReadyPos = 11.0;
    m_dCondenserLimitPos = 12.0;
    m_dHTFLZOffsetCompensation = 0;
}

HostParam::HostParam(const HostParam& param) {
    m_nZPMax = param.m_nZPMax;
    m_nZP2Max = param.m_nZP2Max;
    m_dObjectiveReadyPos = param.m_dObjectiveReadyPos;
    m_dObjectiveLimitPos = param.m_dObjectiveLimitPos;
    m_dCondenserReadyPos = param.m_dCondenserReadyPos;
    m_dCondenserLimitPos = param.m_dCondenserLimitPos;
    m_dHTFLZOffsetCompensation = param.m_dHTFLZOffsetCompensation;
}

bool HostParam::operator==(const HostParam& param) {
    if (m_nZPMax != param.m_nZPMax) return false;
    if (m_nZP2Max != param.m_nZP2Max) return false;
    if (m_dObjectiveReadyPos != param.m_dObjectiveReadyPos) return false;
    if (m_dObjectiveLimitPos != param.m_dObjectiveLimitPos) return false;
    if (m_dCondenserReadyPos != param.m_dCondenserReadyPos) return false;
    if (m_dCondenserLimitPos != param.m_dCondenserLimitPos) return false;
    if (m_dHTFLZOffsetCompensation != param.m_dHTFLZOffsetCompensation) return false;

    return true;
}

int HostParam::ZPMax(void) {
    return m_nZPMax;
}
void HostParam::ZPMax(int nZPMax) {
    m_nZPMax = nZPMax;
    QLOG_INFO() << "Host.ZPMax: " << m_nZPMax;
}

int HostParam::ZP2Max(void) {
    return m_nZP2Max;
}

void HostParam::ZP2Max(int nZP2Max) {
    m_nZP2Max = nZP2Max;
    QLOG_INFO() << "Host.ZP2Max: " << m_nZP2Max;
}

double HostParam::ObjectiveReadyPos(void) {
    return m_dObjectiveReadyPos;
}

void HostParam::ObjectiveReadyPos(double dPos) {
    m_dObjectiveReadyPos = dPos;
    QLOG_INFO() << "Host.Objective Ready pos: " << m_dObjectiveReadyPos;
}

double HostParam::ObjectiveLimitPos(void) {
    return m_dObjectiveLimitPos;
}

void HostParam::ObjectiveLimitPos(double dPos) {
    m_dObjectiveLimitPos = dPos;
    QLOG_INFO() << "Host.Objective Limit pos: " << m_dObjectiveLimitPos;
}

double HostParam::CondenserReadyPos(void) {
    return m_dCondenserReadyPos;
}

void HostParam::CondenserReadyPos(double dPos) {
    m_dCondenserReadyPos = dPos;
    QLOG_INFO() << "Host.Condenser Ready pos: " << m_dCondenserReadyPos;
}

double  HostParam::CondenserUserFocusPos(void) {
    return m_dCondenserUserFocusPos;
}

void  HostParam::CondenserUserFocusPos(double dPos) {
    m_dCondenserUserFocusPos = dPos;
    QLOG_INFO() << "Host.Condenser User-defined focus pos: " << m_dCondenserUserFocusPos;
}

double HostParam::CondenserLimitPos(void) {
    return m_dCondenserLimitPos;
}

void HostParam::CondenserLimitPos(double dPos) {
    m_dCondenserLimitPos = dPos;
    QLOG_INFO() << "Host.Condenser Limit pos: " << m_dCondenserLimitPos;
}

double HostParam::HTFLZOffsetCompensation(void) {
    return m_dHTFLZOffsetCompensation;
}

void HostParam::HTFLZOffsetCompensation(double dValue) {
    m_dHTFLZOffsetCompensation = dValue;
    QLOG_INFO() << "Host.HTFL Z Offset Compensation (nm) : " << m_dHTFLZOffsetCompensation;
}

MotionControllerParam::MotionControllerParam() {
}

MotionControllerParam::MotionControllerParam(const MotionControllerParam& param) {
    m_strPort = param.m_strPort;
}

bool MotionControllerParam::operator==(const MotionControllerParam& param) {
    if (m_strPort != param.m_strPort) return false;

    return true;
}

QString MotionControllerParam::Port(void) const {
    return m_strPort;
}

void MotionControllerParam::Port(const QString& strPort) {
    m_strPort = strPort;
}


//!
SystemParameterData::SystemParameterData()
    : m_nDMDMajorVersion(0)
    , m_dMiniumShutter(0.006)
    , m_strHostName("N/A")
    , m_strSWVersion("N/A") {
}

SystemParameterData::SystemParameterData(const SystemParameterData& other) {
    set(other);
}

void SystemParameterData::operator=(const SystemParameterData& other) {
    set(other);
}

bool SystemParameterData::operator==(const SystemParameterData& other) {
    if (m_laser != other.m_laser) return false;
    if (m_camera != other.m_camera) return false;
    if (m_objectiveLens != other.m_objectiveLens) return false;
    if (m_condenserLens != other.m_condenserLens) return false;
    if (m_host != other.m_host) return false;
    if (m_motion != other.m_motion) return false;
    if (m_strModel != other.m_strModel) return false;
    if (m_strDMDModel != other.m_strDMDModel) return false;
    if (m_strDMDPatternType != other.m_strDMDPatternType) return false;
    if (m_nDMDMajorVersion != other.m_nDMDMajorVersion) return false;
    if (m_dMiniumShutter != other.m_dMiniumShutter) return false;
    if (m_strSerial != other.m_strSerial) return false;
    if (m_strHostName != other.m_strHostName) return false;
    if (m_strSWVersion != other.m_strSWVersion) return false;

    return true;
}

void SystemParameterData::set(const SystemParameterData& other) {
    m_laser = other.m_laser;
    m_camera = other.m_camera;
    m_objectiveLens = other.m_objectiveLens;
    m_condenserLens = other.m_condenserLens;
    m_host = other.m_host;
    m_motion = other.m_motion;
    m_strModel = other.m_strModel;
    m_strDMDModel = other.m_strDMDModel;
    m_strDMDPatternType = other.m_strDMDPatternType;
    m_nDMDMajorVersion = other.m_nDMDMajorVersion;
    m_dMiniumShutter = other.m_dMiniumShutter;
    m_strSerial = other.m_strSerial;
    m_strHostName = other.m_strHostName;
    m_strSWVersion = other.m_strSWVersion;
}

double SystemParameterData::maxFOVH(void) {
    const double mag = m_objectiveLens.magnification();
    const double maxval = m_camera.pixelSizeH() * m_camera.maxPixelsH() / mag;

    return maxval;
}

double SystemParameterData::maxFOVV(void) {
    const double mag = m_objectiveLens.magnification();
    const double maxval = m_camera.pixelSizeV() * m_camera.maxPixelsV() / mag;

    return maxval;
}

double SystemParameterData::sysMagnification(void) {
    return m_objectiveLens.magnification();
}

double SystemParameterData::bfMagnification(void) {
    return 33.3;
}

void SystemParameterData::setModel(const QString& model) {
    m_strModel = model;
}

QString SystemParameterData::getModel(void) const {
    return m_strModel;
}

void SystemParameterData::setMinimumShutter(double dValue) {
    m_dMiniumShutter = dValue;
}

double SystemParameterData::getMinimumShutter(void) const {
    return m_dMiniumShutter;
}

int SystemParameterData::PhaseSign(void) {
    int sign = -1;
    TC::DMDPatternType type = getDMDPatternType();

    switch (type) {
    case TC::PATTERN_LEGACY:
    case TC::PATTERN_LEGACY_V2:
        sign = -1;
        break;
    case TC::PATTERN_MULTIBITS_V001:	//SIM
        sign = 1;
        break;
    case TC::PATTERN_MULTIBITS_LEE_V001:
    case TC::PATTERN_MULTIBITS_LEE_V002:
        sign = -1;
        break;
    }

    return sign;
}

int SystemParameterData::MappingSign(void) {
    return PhaseSign();	//! is valid unless their signs are opposite for all cases....
}

void SystemParameterData::setDMDInformation(const QString& strModel, const QString& strType, const int majorVersion) {
    m_strDMDModel = strModel;
    m_strDMDPatternType = strType;
    m_nDMDMajorVersion = majorVersion;
}

TC::DMDPatternType SystemParameterData::getDMDPatternType(void) const {
    TC::DMDPatternType type;

    if (m_strDMDPatternType.compare("Legacy", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_LEGACY;
    } else if (m_strDMDPatternType.compare("11", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_LEGACY;
    } else if (m_strDMDPatternType.compare("12", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_LEGACY_V2;
    } else if (m_strDMDPatternType.compare("41", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_MULTIBITS_V001;
    } else if (m_strDMDPatternType.compare("42", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_MULTIBITS_LEE_V001;
    } else if (m_strDMDPatternType.compare("43", Qt::CaseInsensitive) == 0) {
        type = TC::PATTERN_MULTIBITS_LEE_V002;
    } else {
        type = TC::PATTERN_LEGACY;
    }

    return type;
}

bool SystemParameterData::supportDMDFastSwitching(void) const {
    return (m_nDMDMajorVersion >= 100);
}

void SystemParameterData::setSerial(const QString& strSerial) {
    m_strSerial = strSerial.trimmed();
}

QString SystemParameterData::getSerial(void) const {
    return m_strSerial;
}

QString SystemParameterData::getSerialAsLow(void) const {
    return m_strSerial.toLower();
}

void SystemParameterData::setHostName(const QString& strHost) {
    if (strHost.isEmpty()) m_strHostName = "N/A";
    else m_strHostName = strHost;
}

QString SystemParameterData::getHostName(void) const {
    return m_strHostName;
}

void SystemParameterData::setSoftwareVersion(const QString& strVersion) {
    if (strVersion.isEmpty()) m_strSWVersion = "N/A";
    else m_strSWVersion = strVersion;
}

QString SystemParameterData::getSoftwareVersion(void) const {
    return m_strSWVersion;
}

//!
SystemParameter::Pointer SystemParameter::GetInstance(void) {
    static SystemParameter::Pointer theSystemParameter(new SystemParameter());

    return theSystemParameter;
}

SystemParameter::SystemParameter()
    : SystemParameterData() {
}

SystemParameter::~SystemParameter() {
}

bool SystemParameter::isEqual(const SystemParameterData& other) {
    return (*this == other);
}

