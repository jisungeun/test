#define LOGGER_TAG "TCHoloprocessor::SystemConfig"

#include <iostream>

#include <QDir>
#include <QSettings>
#include <QImage>

#include <TCLogger.h>

#include "SystemConfig.h"

#include <QTextStream>

#include "SystemModelFactory.h"
#include "SystemParameter.h"
#include "CameraModels.h"

#include "DataArchive.h"

struct SystemConfig::Impl {
    struct {
        QString m_strTempFolder;		//! temp folder
        QString m_strLogFolder;			//! logger folder
        QString m_strErrorLogFolder;	//! error log folder
    } System;

    struct {
        QString m_strConfigFolder;		//! configurations		
        QString m_strCalibrationFolder;	//! calibration
        QString m_strDataFolder;		//! data
        QString m_strAnnotatoins;		//! annotations
        QString m_strPresets;			//! presets
    } Data;

    struct {
        bool m_bSCMOSCamera;
        bool m_bBrightFieldCamera;
    } Dedicated;

    struct {
        QString m_strImageFolder;        //! images for simulation
        bool m_bSimulation;              //! d->Simulation...
    } Simulation;

    struct {
        bool m_bEngineeringMode;			//! engineering mode
        bool m_bSystemAnalysisMode;			//! system analysis mode
        bool m_bLegacyModel;				//! legacy model which has Objective with Z-Axis
        int m_nImageSize;					//! image size
        bool m_bReadyOnly;					//! used only for ready information...
    } Misc;

    struct {
        QString DataDirectory{ "Holoscope/Property/DataDirectory" };
        QString EngineeringMode{ "Holoscope/Property/EngineeringMode" };
        QString SystemAnalysisMode{ "Holoscope/Property/SystemAnalysisMode" };
        QString LegacyModel{ "Holoscope/Property/LegacyModel" };
        QString UseSCMOSCamera{ "Holoscope/Property/UseSCMOSCamera" };
        QString UseBrightfieldCamera{ "Holoscope/Property/UseBrightfieldCamera" };
    } Str;
};

SystemConfig::Pointer SystemConfig::GetInstance(const QString& strRootPath, bool isReadOnly) {
    static SystemConfig::Pointer theSystemConfig(new SystemConfig(strRootPath, isReadOnly));

    return theSystemConfig;
}

SystemConfig::SystemConfig(const QString& strRootPath, bool isReadyOnly, QObject* parent)
    : d(new Impl) {
    d->Misc.m_bEngineeringMode = false;
    d->Misc.m_bLegacyModel = false;
    d->Misc.m_nImageSize = -1;
    d->Misc.m_bReadyOnly = isReadyOnly;

    d->Dedicated.m_bSCMOSCamera = false;
    d->Dedicated.m_bBrightFieldCamera = false;

    try {
        setSystemFolders(strRootPath);
    } catch (...) {
        QLOG_ERROR() << "SystemConfig> Failed to prepare SystemConfig.ini";
    }
}

SystemConfig::~SystemConfig(void) {
}

QString SystemConfig::getConfigPath() const {
    return d->Data.m_strConfigFolder;
}

QString SystemConfig::getDataPath() const {
    return d->Data.m_strDataFolder;
}

QString SystemConfig::getCalibrationPath() const {
    return d->Data.m_strCalibrationFolder;
}

QString SystemConfig::getAnnotationsPath() const {
    return d->Data.m_strAnnotatoins;
}

QString SystemConfig::GetPresetsPath() const {
    return d->Data.m_strPresets;
}

QString SystemConfig::getLogPath() const {
    return d->System.m_strLogFolder;
}

QString SystemConfig::getErrorLogPath() const {
    return d->System.m_strErrorLogFolder;
}

QString SystemConfig::getTempPath() const {
    return d->System.m_strTempFolder;
}

void SystemConfig::updateLatestCalibration(const QString& strName, int nImageSize) {
    QFile file(QString("%1/.latest_calibration.dat").arg(getConfigPath()));
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << strName;

    d->Misc.m_nImageSize = nImageSize;
}

bool SystemConfig::getLatestCalibration(QString& strName) {
    QFile file(QString("%1/.latest_calibration.dat").arg(getConfigPath()));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QString strLatestCalibrationPath;
    QTextStream in(&file);
    if (!in.atEnd()) strLatestCalibrationPath = in.readLine();

    //check validness
    const QString strCalibrationPath = QString("%1/bgImages").arg(strLatestCalibrationPath);
    QDir dir(strCalibrationPath);
    if (dir.exists() == false) return false;

    strName = strLatestCalibrationPath;

    return true;
}

void SystemConfig::loadConfig(const QString& strPath, QMap<QString, double>& configs) {
    configs.clear();

    QFile file(strPath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;

    while (!file.atEnd()) {
        QString line = file.readLine();

        QStringList strList = line.split(",");
        if (strList.length() != 2) continue;

        configs[strList.at(0)] = strList.at(1).toDouble();
    }
}

bool SystemConfig::isValidCalibration(int nImageSize) {
    if (d->Misc.m_nImageSize > -1) {
        const bool isValid = (d->Misc.m_nImageSize == nImageSize);
        if (!isValid)
            QLOG_ERROR() << "Calibration image=" << d->Misc.m_nImageSize << " Current image=" << nImageSize;

        return isValid;
    }

    //Check system parameters in calibration
    QString strPath;
    if (!getLatestCalibration(strPath)) return false;

    const QString cfgPath = QString("%1/config.dat").arg(strPath);
    QMap<QString, double> configs;
    loadConfig(cfgPath, configs);

    SystemModelInterface::Pointer pModel = SystemModelFactory::Model(SystemParameter::GetInstance()->getModel());
    if (!pModel.get()) return false;

    if (configs.find("M") == configs.end()) return false;
    if (configs.find("NA") == configs.end()) return false;
    if (configs.find("Pixel size") == configs.end()) return false;
    if (configs.find("Lambda") == configs.end()) return false;

    if (configs["M"] != pModel->ObjectiveLensMagnification()) return false;
    if (configs["NA"] != pModel->ObjectiveLensNA()) return false;
    if (configs["Pixel size"] != CameraModels::GetInstance(pModel->Camera())->pixelSizeH()) return false;
    if (configs["Lambda"] != (pModel->Wavelength() / 1000.0)) return false;

    //Check the size of image in calibration
    DataArchive archive;

    const QString arcPath = QString("%1/bgImages/calibration.dat").arg(strPath);
    archive.setFilename(arcPath);

    const QString bgImgPath = QString("%1/background.png").arg(getTempPath());

    bool isValid = false;
    if (archive.restore("/images", "000000", bgImgPath)) {
        QImage bg(bgImgPath);

        if (bg.isNull()) return false;
        if (bg.width() == 0 || bg.height() == 0) return false;

        d->Misc.m_nImageSize = bg.width();

        return (bg.width() == nImageSize);
    }

    return false;
}

void SystemConfig::setSystemFolders(const QString& strRootPath) {
    //System folder will be placed under strRootPath
    d->System.m_strTempFolder = QString("%1/sys/temp").arg(strRootPath);
    d->System.m_strLogFolder = QString("%1/sys/log").arg(strRootPath);

    //prepare folders...
    if (d->Misc.m_bReadyOnly != true) {
        QDir().mkpath(d->System.m_strTempFolder);
        QDir().mkpath(d->System.m_strLogFolder);
    }

    //before logger is prepared...
    std::cout << "Temp folder - " << d->System.m_strTempFolder.toStdString() << std::endl;
    std::cout << "Log folder - " << d->System.m_strLogFolder.toStdString() << std::endl;
}

void SystemConfig::setDataFolders(const QString& strDataRootPath) {
    QSettings().setValue(d->Str.DataDirectory, strDataRootPath);

    d->System.m_strErrorLogFolder = QString("%1/errors").arg(strDataRootPath);

    if (d->Misc.m_bReadyOnly != true)
        QDir().mkpath(d->System.m_strErrorLogFolder);

    d->Data.m_strConfigFolder = QString("%1/sys/configs").arg(strDataRootPath);
    d->Data.m_strCalibrationFolder = QString("%1/calibration").arg(strDataRootPath);
    d->Data.m_strDataFolder = QString("%1/data").arg(strDataRootPath);
    d->Data.m_strAnnotatoins = QString("%1/sys/annotations").arg(strDataRootPath);
    d->Data.m_strPresets = QString("%1/sys/presets").arg(strDataRootPath);

    if (d->Misc.m_bReadyOnly != true) {
        QDir().mkpath(d->Data.m_strConfigFolder);
        QDir().mkpath(d->Data.m_strCalibrationFolder);
        QDir().mkpath(d->Data.m_strDataFolder);
        QDir().mkpath(d->Data.m_strAnnotatoins);
        QDir().mkpath(d->Data.m_strPresets);
    }

    loadSimulation();
    saveSimulation();

    QLOG_INFO() << "Config folder - " << d->Data.m_strConfigFolder;
    QLOG_INFO() << "Calibration folder - " << d->Data.m_strCalibrationFolder;
    QLOG_INFO() << "Data folder - " << d->Data.m_strDataFolder;
    QLOG_INFO() << "Annotations folder - " << d->Data.m_strAnnotatoins;
}

#define TAG_IMAGE_FOLDER		"ImageFolder"
#define TAG_ENABLE				"Enable"

bool SystemConfig::getSimulation() {
    return d->Simulation.m_bSimulation;
}

QString SystemConfig::getSimulatinImagePath() {
    return d->Simulation.m_strImageFolder;
}

void SystemConfig::loadSimulation() {
    QString strSysType = QSettings().value("SystemType").toString();
    if (strSysType.compare("Dev", Qt::CaseInsensitive) == 0) {
        d->Simulation.m_strImageFolder = QSettings().value("Simulation/ImageFolder").toString();
        d->Simulation.m_bSimulation = true;
    } else {
        d->Simulation.m_bSimulation = false;
    }
}

void SystemConfig::saveSimulation() {
    const QString strPath = QString("%1/.d->Simulation.ini").arg(getTempPath());
    std::cout << "save simulation config at " << strPath.toStdString() << std::endl;

    QSettings settings(strPath, QSettings::IniFormat);
    settings.beginGroup("Simulation");
    {
        settings.setValue(TAG_IMAGE_FOLDER, d->Simulation.m_strImageFolder);
        settings.setValue(TAG_ENABLE, d->Simulation.m_bSimulation);
    }
    settings.endGroup();
}

void SystemConfig::setEngineeringMode(bool bMode) {
    d->Misc.m_bEngineeringMode = bMode;
    QSettings().setValue(d->Str.EngineeringMode, bMode);
}

bool SystemConfig::isEngineeringMode(void) const {
    return QSettings().value(d->Str.EngineeringMode, d->Misc.m_bEngineeringMode).toBool();
}

void  SystemConfig::setSystemAnalysisMode(bool bMode) {
    d->Misc.m_bSystemAnalysisMode = bMode;
    QSettings().setValue(d->Str.SystemAnalysisMode, bMode);
}

bool  SystemConfig::isSystemAnalysisMode(void) const {
    return QSettings().value(d->Str.SystemAnalysisMode, d->Misc.m_bSystemAnalysisMode).toBool();
}

void SystemConfig::setLegacyModel(bool bLegacy) {
    d->Misc.m_bLegacyModel = bLegacy;
    QSettings().setValue(d->Str.LegacyModel, bLegacy);
}

bool SystemConfig::isLegacyModel(void) const {
    return QSettings().value(d->Str.LegacyModel, d->Misc.m_bLegacyModel).toBool();
}

bool SystemConfig::useWideFieldCamera(void) const {
    return QSettings().value("Misc/Use Dedicated Brightfield Camera", false).toBool();
}

void SystemConfig::setUseSCMOSCamera(bool bUse) {
    d->Dedicated.m_bSCMOSCamera = bUse;
    QSettings().setValue(d->Str.UseSCMOSCamera, bUse);
}

bool SystemConfig::useSCMOSCamera(void) const {
    return QSettings().value(d->Str.UseSCMOSCamera, d->Dedicated.m_bSCMOSCamera).toBool();
}

void SystemConfig::setUseBrightFieldCamera(bool bUse) {
    d->Dedicated.m_bBrightFieldCamera = bUse;
    QSettings().value(d->Str.UseBrightfieldCamera, bUse);
}

bool SystemConfig::useBrightFieldCamera(void) const {
    return QSettings().value(d->Str.UseBrightfieldCamera, d->Dedicated.m_bBrightFieldCamera).toBool();
}
