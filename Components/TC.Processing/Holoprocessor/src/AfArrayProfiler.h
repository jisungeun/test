#pragma once

#include <nvml.h>
#include <arrayfire.h>
#include <memory>

#include <QString>
#include <QList>
#include <QVector>

#ifndef AF_ENABLEPROFILER
#define AF_ENABLEPROFILER(enable) AfArrayProfiler::GetInstance(enable)
#endif

#ifndef AF_RESETPROFILER
#define AF_RESETPROFILER() AfArrayProfiler::GetInstance()->Reset()
#endif

#ifndef AF_MEMINFO
#define AF_MEMINFO(idx, msg) AfArrayProfiler::GetInstance()->LogMemInfo(idx, msg, __FILE__, __LINE__)
#endif

#ifndef AF_EXPORT
#define AF_EXPORT(path, idx) AfArrayProfiler::GetInstance()->Export(QString("%1_%2").arg(path).arg(idx))
#endif

#ifndef AF_AVAILABLE
#define AF_AVAILABLE(idx) AfArrayProfiler::GetInstance()->Available(idx)
#endif

#ifndef AF_USED
#define AF_USED(idx) AfArrayProfiler::GetInstance()->Used(idx)
#endif

#ifndef AF_RESETHANDLE
#define AF_RESETHANDLE(idx) AfArrayProfiler::GetInstance()->ResetHandle(idx)
#endif

class AfArrayProfiler {
public:
    typedef AfArrayProfiler Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    ~AfArrayProfiler();

    static Pointer GetInstance(bool bEnable = false);
    void ResetHandle(int nIndex);

    void LogMemInfo(const int idx, const QString& tag, char* file_name, int line_number);

    void Export(const QString& strPath);
    void Reset(void);

    size_t Available(const int idx);
    size_t Used(const int idx);

protected:
    AfArrayProfiler(bool bEnable = false);

private:
    QVector<nvmlDevice_t> m_devices;
    bool m_bEnable;
    bool m_bNVMLLoaded;

    QList<QString> m_ltInfoLine;
    QList<QString> m_ltInfoMemory;
};