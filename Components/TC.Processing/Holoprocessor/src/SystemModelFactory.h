#pragma once

#include <memory>

#include <QString>
#include <QMap>

#include "SystemModel.h"

class SystemModelFactory {
public:
    typedef SystemModelFactory Self;
    typedef std::shared_ptr<SystemModelFactory> Pointer;

public:
    SystemModelFactory();

    static QStringList GetModels();

    static SystemModelInterface::Pointer Model(const QString& strModel);

protected:
    void AddModel(SystemModelInterface::Pointer pModel);

protected:
    QMap<QString, SystemModelInterface::Pointer> m_models;
};
