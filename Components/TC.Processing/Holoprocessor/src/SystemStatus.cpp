#include <QDateTime>

#include "SystemStatus.h"

SystemStatus::Pointer SystemStatus::GetInstance(void) {
    static SystemStatus::Pointer theSystemStatus;

    if (!theSystemStatus.get())
        theSystemStatus.reset(new SystemStatus());

    return theSystemStatus;
}

SystemStatus::SystemStatus()
    : m_nImageSize(0)
    , m_bUseCustomDataPath(false) {

}

QString SystemStatus::getNewAcquisitionName(void) {
    QString strParent, strName;
    getNewAcquisitionName(strParent, strName);

    const QString strDateTime = QString("%1/%2").arg(strParent).arg(strName);

    return strDateTime;
}

void SystemStatus::getNewAcquisitionName(QString& strParent, QString& strName) {
    QDateTime dateTime = QDateTime::currentDateTime();
    strParent = dateTime.toString("yyyyMMdd");
    strName = dateTime.toString("yyyyMMdd.hhmmss.zzz");
}