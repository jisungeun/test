#include "FilteredUnwrap.h"
#include "PhaseUnwrapperFilteredLaplacian.h"

af::array PhaseUnwrapperFilteredLaplacian::execute(const af::array& arrIn, bool& bGoodQuality, int& score) {
    const auto arrayLength = arrIn.elements();
    const auto arraySizeX = arrIn.dims(0);
    const auto arraySizeY = arrIn.dims(0);

    af::array unwrappedPhase(arraySizeX, arraySizeY, f32);

    arrIn.eval();
    unwrappedPhase.eval();

    auto devicePointerWrappedPhase = arrIn.device<float>();
    auto devicePointerUnwrappedPhase = unwrappedPhase.device<float>();

    const auto residueThreshold = 50;
    auto residueNumber{INT_MAX};

    FilteredUnwrap(devicePointerWrappedPhase, arraySizeX, arraySizeY, devicePointerUnwrappedPhase, residueNumber);

    bGoodQuality = residueNumber <= residueThreshold;

    arrIn.unlock();
    unwrappedPhase.unlock();

    score = residueNumber;

    return unwrappedPhase;
}
