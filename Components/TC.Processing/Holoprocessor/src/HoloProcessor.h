#pragma once

#include <memory>
#include <QString>
#include <QStringList>
#include <QMutex>

#include "ProgressReporter.h"
#include "GlobalDefines.h"

class HoloProcessor {
public:
    HoloProcessor(ProgressReporter* reporter = new NoProgressReporter(), QCoreApplication* pApplication = 0, int nDebugLevel = 0);
    ~HoloProcessor();

    typedef enum {
        CURRENT,
        ARRAYFIRE,
        CUDA
    } ProcessorType;

    static std::shared_ptr<HoloProcessor> createInstance(ProcessorType type = CURRENT, ProgressReporter* reporter = new NoProgressReporter(), QCoreApplication* pApplication = 0, int nDebugLevel = 0);

    void start(void);
    void stop(void);

    virtual bool initialize() = 0;
    virtual bool isInitialized() = 0;
    virtual bool isInitTimeout() = 0;

    virtual bool clear() = 0;
    virtual bool procHologram2DSingle(const QString& srcImgPath, const QString& bgImgPath, const QString& bgImgPath2, const QString& configPath, const QString& outputPath, TC::ImageType type, double dRange, bool bCompensateAnalysis) = 0;
    virtual bool procHologram2D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, const QString& jobParameterPath, const bool& htConsider, bool isReprocess = false, const int32_t& timeFrameIndex = -1) = 0;
    virtual bool procHologram3D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, bool isReprocess = false) = 0;
    virtual bool procHologram3DFL(const QString& configPath, const QString& darkpixelPath, const QString& workingPath, const QString& jobParameterPath, bool inclDeconv, bool firstSetPerChannel, const int32_t& channelIndex, const bool& htConsider) = 0;
    virtual bool procBrightfield(const QString& configPath, const QString& workingPath, const QString& outputPath, bool isReprocessing = false) = 0;
    virtual bool saveTCF(const QString& configPath, const QString& annotationPath, const QString& tilePath, const QString& commentPath, const QString& timestampPath, const QString& outputPath) = 0;

    void setSysInformation(const QString& strDevice, const QString& strSWVer, const QString& strUser);
    void setFileInformation(const QString& strTitle, const QString& strDesc);

    virtual bool procReference(const QString& strCalPath, bool bSave) = 0;

    virtual bool procDarkpixel(const QString& strImgPath, const QString& strOutPath) = 0;

    virtual bool loadDarkPixel(const QString& strDarkPixel) = 0;
    virtual bool procRemoveDarkPixel(const QString& strImgPath, const QString& strOutPath) = 0;

    void setDeviceIndex(const int deviceIndex) { m_nDeviceIndex = deviceIndex; }
    int getDeviceIndex(void) const { return m_nDeviceIndex; }

protected:
    void reportProgress(int value);

protected:
    QCoreApplication* m_pApplication;
    int m_nDebugLevel;
    int m_nDeviceIndex;

    QString m_strSWVer;
    QString m_strUser;
    QString m_strTitle;
    QString m_strDesc;

    bool m_bRunning;
    QMutex m_mutex;

    ProgressReporter* m_pReporter;
};

typedef std::shared_ptr<HoloProcessor> HoloProcessorPtr;