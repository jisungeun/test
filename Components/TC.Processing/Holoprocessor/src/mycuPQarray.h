#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__global__ void make_Parray(int* dev_Parray, const int input_Xsize, const int input_Ysize);

__global__ void make_Qarray(int* dev_Qarray, const int input_Xsize, const int input_Ysize);


__global__ void make_Parray2(int* dev_Parray, const int input_Xsize, const int input_Ysize);

__global__ void make_Qarray2(int* dev_Qarray, const int input_Xsize, const int input_Ysize);


__global__ void make_P2Q2array(int* dev_Parray, int* dev_Qarray, int* dev_P2Q2array, const int input_Xsize, const int input_Ysize);

__global__ void make_P2Q2array(int* dev_P2Q2array, const int input_Xsize, const int input_Ysize);

