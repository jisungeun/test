#pragma once

#include "cufft.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__global__ void ElementMultiply(int* dev_PQarray, cufftComplex* dev_fft_array, cufftComplex* dev_output_array, const int Size);

__global__ void ElementMultiply(float* dev_Array1, float* dev_Array2, float* dev_output_array, const int Size);

__global__ void ElementMultiply(float* dev_Array, float mulitplier, float* dev_output_array, const int Size);

__global__ void ElementDivide(cufftComplex* dev_fft_array, int* dev_PQarray, cufftComplex* dev_output_array, const int Size);

__global__ void ElementDivide(cufftComplex* targetarray, cufftComplex* dev_dividerarray, cufftComplex* dev_output_array, const int Size);

__global__ void ElementDivide(float* dev_fft_array, int divider, float* dev_output_array, const int Size);

__global__ void ElementDivide(cufftComplex* dev_array, const int divider, cufftComplex* dev_output_array, const int Size);

__global__ void ElementAdd(float* dev_Array1, float* dev_Array2, float* dev_output_array, const int Size);

__global__ void ElementSubtract(float* dev_Array, float* dev_Array_minus, float* dev_output_array, const int Size);

__global__ void ElementRound(float* dev_Array, float* dev_output_array, const int Size);

__global__ void ElementImag(cufftComplex* dev_input_array, cufftComplex* dev_output_array, const int Size);

__global__ void ElementReal(cufftComplex* dev_input_array, float* dev_output_array, const int Size);


__global__ void Element_opt1(cufftComplex* dev_input_array, cufftComplex* dev_output_array, int divider, cufftComplex* dev_divider_array, const int Size);

