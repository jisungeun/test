#include <QFile>
#include <QDir>
#include <QDirIterator>
#include <QTextStream>

#include "GlobalDefines.h"
#include "SystemStatus.h"
#include "SystemConfig.h"
#include "SystemParameter.h"
#include "DataArchive.h"
#include "HoloscopeUtility.h"

void TC::convJobParameter2Inputfile(const JobParameter& jobParam, const QString& strOutPath, JobItemSwitch* jobItem) {
    SystemStatus::Pointer pSysStatus = SystemStatus::GetInstance();
    SystemParameter::Pointer sysParam = SystemParameter::GetInstance();

    uint ptIndex = (jobItem) ? jobItem->m_nIndex : 0;

    const double dMagnification = sysParam->sysMagnification();
    const double dNA = sysParam->m_objectiveLens.NA();
    const double dNACDS = sysParam->m_condenserLens.NA();
    const double dPixelSize = sysParam->m_camera.pixelSizeH();
    const double dLambda = sysParam->m_laser.wavelengthUM();
    const double dRI = jobParam.mediumRI();
    const double dImmersionRI = jobParam.immersionRI();
    const double dInterval2D = jobParam.acquisitionInterval2D();
    const double dInterval3D = jobParam.acquisitionInterval3D();
    const double dZOffset = pSysStatus->getAcquisitionHTFLZOffset(ptIndex);
    const double dZOffsetCompensation = sysParam->m_host.HTFLZOffsetCompensation();
    const double dInterval = pSysStatus->getInterval();
    const int nRepeat = pSysStatus->getRepeat();
    const int nPhaseSign = sysParam->PhaseSign();
    const int nMappingSign = sysParam->MappingSign();
    const int nIteration = jobParam.IterationCount();
    const int nPatternType = sysParam->getDMDPatternType();
    const bool bDisableZLimit = jobParam.DisableZLimit();
    const auto nIgnorePoor = jobParam.IgnorePoor();

    const double dShutter = jobParam.cameraShutter();
    const double dGain = jobParam.cameraGain();

    QList<JobItemPtr> jobItems;
    pSysStatus->getJobItems(jobItems);

    auto countJobTypes = [=](QList<JobItemPtr>& list, int type)->uint32_t {
        uint32_t count = 0;
        uint32_t switchCount = 0;

        for (auto item : list) {
            if (item->type() == type) {
                if (type == JobItem::JOBITEM_2D) count = 1;
                else count += item->GetBins();
            }

            if (item->type() == JobItem::JOBITEM_SWITCH) switchCount++;
        }
        return count*nRepeat / std::max<int>(1, switchCount);
    };

    const auto jobCountHT2D = countJobTypes(jobItems, JobItem::JOBITEM_2D);
    const auto jobCountHT3D = countJobTypes(jobItems, JobItem::JOBITEM_3D);
    const auto jobCountFL3D = countJobTypes(jobItems, JobItem::JOBITEM_3DFL);
    const auto jobCountBF = countJobTypes(jobItems, JobItem::JOBITEM_BF);

#ifdef _DEBUG
    //	qDebug() << strPath;
#endif

    QFile file(strOutPath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << "Serial," << sysParam->getSerial() << "\n";
    out << "Computer," << sysParam->getHostName() << "\n";
    out << "SW Version," << sysParam->getSoftwareVersion() << "\n";
    out << "M," << fixed << dMagnification << "\n";
    out << "NA," << fixed << dNA << "\n";
    out << "NA_CDS," << fixed << dNACDS << "\n";
    out << "Pixel size," << fixed << dPixelSize << "\n";
    out << "Lambda," << fixed << dLambda << "\n";
    out << "n_m," << fixed << dRI << "\n";
    out << "Immersion_RI" << fixed << dImmersionRI << "\n";
    out << "Acquisition interval 2D," << fixed << dInterval2D << "\n";
    out << "Acquisition interval 3D," << fixed << dInterval3D << "\n";
    out << "Acquisition Z Offset," << fixed << dZOffset << "\n";
    out << "Acquisition Z Offset Compensation," << fixed << dZOffsetCompensation << "\n";
    out << "Acquisition Time Interval," << fixed << dInterval << "\n";
    out << "Acquisition Count," << nRepeat << "\n";
    out << "phase sign," << nPhaseSign << "\n";
    out << "mapping sign," << nMappingSign << "\n";
    out << "iteration," << nIteration << "\n";
    out << "Pattern type," << nPatternType << "\n";
    out << "Disable ZLimit," << bDisableZLimit << "\n";
    out << "Ignore Poor," << nIgnorePoor << "\n";

    out << "Camera Shutter," << dShutter << "\n";
    out << "Camera Gain," << dGain << "\n";
    out << "Images HT2D," << jobCountHT2D << "\n";
    out << "Images HT3D," << jobCountHT3D << "\n";
    out << "Images FL3D," << jobCountFL3D << "\n";
    out << "Images BF," << jobCountBF << "\n";

    //only for tiles/matrix imaging....
    if (jobItem && (jobItem->m_nImagingMode > JobItemSwitch::MULTI_POINTS)) {
        out << "Tiles Imaging Index," << jobItem->m_nIndex + 1 << "\n";
        out << "Tiles Imaging Total," << jobItem->m_nTotal << "\n";
        out << "Tiles Imaging Overlap H," << jobItem->m_dOverlapH << "\n";
        out << "Tiles Imaging Overlap V," << jobItem->m_dOverlapV << "\n";
        out << "Tiles Imaging Row," << jobItem->m_nRowIdx << "\n";
        out << "Tiles Imaging Column," << jobItem->m_nColIdx << "\n";
    }

    out << TAG_FL_CAMERA_SHUTTER(0) << "," << fixed << jobParam.FLCameraShutter((TC::FLChannel)0) << "\n";
    out << TAG_FL_CAMERA_GAIN(0) << "," << fixed << jobParam.FLCameraGain((TC::FLChannel)0) << "\n";
    out << TAG_FL_LIGHT_INTENSITY(0) << "," << fixed << jobParam.FLLightIntensity((TC::FLChannel)0) << "\n";
    out << TAG_FL_FLUOROPHORE_EMISSION(0) << "," << jobParam.FLFluorophoreEmission((TC::FLChannel)0) << "\n";

    out << TAG_FL_CAMERA_SHUTTER(1) << "," << fixed << jobParam.FLCameraShutter((TC::FLChannel)1) << "\n";
    out << TAG_FL_CAMERA_GAIN(1) << "," << fixed << jobParam.FLCameraGain((TC::FLChannel)1) << "\n";
    out << TAG_FL_LIGHT_INTENSITY(1) << "," << fixed << jobParam.FLLightIntensity((TC::FLChannel)1) << "\n";
    out << TAG_FL_FLUOROPHORE_EMISSION(1) << "," << jobParam.FLFluorophoreEmission((TC::FLChannel)1) << "\n";

    out << TAG_FL_CAMERA_SHUTTER(2) << "," << fixed << jobParam.FLCameraShutter((TC::FLChannel)2) << "\n";
    out << TAG_FL_CAMERA_GAIN(2) << "," << fixed << jobParam.FLCameraGain((TC::FLChannel)2) << "\n";
    out << TAG_FL_LIGHT_INTENSITY(2) << "," << fixed << jobParam.FLLightIntensity((TC::FLChannel)2) << "\n";
    out << TAG_FL_FLUOROPHORE_EMISSION(2) << "," << jobParam.FLFluorophoreEmission((TC::FLChannel)2) << "\n";

    out << TAG_FL_SCANSTEP << "," << fixed << jobParam.FLScanStep() << "\n";
    out << TAG_FL_SCANRANGE << "," << fixed << jobParam.FLScanRange() << "\n";
    out << TAG_FL_SCANCENTER << "," << fixed << jobParam.FLScanCenter() << "\n";
    out << TAG_FL_ACQUISITION_INTERVAL_3D << "," << fixed << jobParam.FLAcquisitionInterval3D() << "\n";
}

void TC::updateInputfile(const QString& strPath, const QString& param, const QVariant& val) {
    QStringList keys;
    QStringList vals;

    if (1) {
        bool found = false;

        QFile file(strPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;

        while (!file.atEnd()) {
            QString line = file.readLine();

            QStringList strList = line.split(",");
            if (strList.length() != 2) continue;

            keys.push_back(strList.at(0));
            if (param.compare(strList.at(0)) == 0) {
                found = true;
                vals.push_back(val.toString());
            } else vals.push_back(strList.at(1));
        }

        if (!found) {
            keys.push_back(param);
            vals.push_back(val.toString());
        }

        file.close();
    }

    if (1) {
        QFile file(strPath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

        QTextStream out(&file);

        const int counts = keys.size();
        for (int idx = 0; idx < counts; idx++) {
            QString val = vals.at(idx);
            val.remove("\n");
            out << keys.at(idx) << "," << val << "\n";
        }
    }
}

QString TC::get_background_path(const QString& key) {
    SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();
    return QString("%1/background/%2").arg(sysConfig->getTempPath()).arg(key);
}

void TC::clear_background(const QString& key) {
    const QString strPath = get_background_path(key);

    QDir dir(strPath, "*.*", QDir::NoSort, QDir::Files);
    QStringList files = dir.entryList();
    foreach(QString strFile, files) {
        QFile(QString("%1/%2").arg(strPath).arg(strFile)).remove();
    }
}

bool TC::restore_background(const QString& strPath, const QString& key, bool bRestoreReference) {
    clear_background(key);

    DataArchive archive;

    const QString arcPath = QString("%1/bgImages/calibration.dat").arg(strPath);
    if (!QFileInfo(arcPath).exists()) return false;

    archive.setFilename(arcPath);

    const QString bgImgFolder = get_background_path(key);
    QDir().mkpath(bgImgFolder);

    if (bRestoreReference) {
        const QString refPath = QString("%1/reference.dat").arg(bgImgFolder);
        if (!archive.restore("/proc", "reference", refPath)) {
            //if reference data does not exist in calibration file, it restores all background...
            if (!archive.restoreAll("/images", bgImgFolder, 3, 1)) return false;
        } else //if reference data exists, it does not need all background images....
        {
            const QString firstImgPath = QString("%1/001.png").arg(bgImgFolder);
            if (!archive.restore("/images", "000000", firstImgPath)) return false;
        }
    } else {
        //it restores all backround images...
        if (!archive.restoreAll("/images", bgImgFolder, 3, 1)) return false;
    }

    return true;
}

QString TC::get_sample_images_path(const QString& key) {
    SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();
    return QString("%1/sample/%2").arg(sysConfig->getTempPath()).arg(key);
}

void TC::clear_sample_images(const QString& key) {
    const QString strPath = get_sample_images_path(key);

    QDir dir(strPath, "*.*", QDir::NoSort, QDir::Files);
    QStringList files = dir.entryList();
    foreach(QString strFile, files) {
        QFile(QString("%1/%2").arg(strPath).arg(strFile)).remove();
    }
}

void TC::clear_flsample_images(const QString& key) {
    const QString strPath = get_sample_images_path(key);

    QStringList nameFilter("ch*");
    QStringList chdirs = QDir(strPath).entryList(nameFilter, QDir::Dirs, QDir::Name);

    foreach(QString chdir, chdirs) {
        const QString strChPath = QString("%1/%2").arg(strPath).arg(chdir);

        QDir dir(strChPath, "*.*", QDir::NoSort, QDir::Files);
        QStringList files = dir.entryList();
        foreach(QString strFile, files) {
            QFile(QString("%1/%2").arg(strChPath).arg(strFile)).remove();
        }

        QDir().rmdir(strChPath);
    }
}

bool TC::restore_sample_images(const QString& strPath, const QString& key) {
    clear_sample_images(key);

    DataArchive archive;

    const QString arcPath = QString("%1/images.dat").arg(strPath);
    if (!QFileInfo(arcPath).exists()) return false;

    archive.setFilename(arcPath);

    const QString fgImgFolder = get_sample_images_path(key);
    QDir().mkpath(fgImgFolder);

    if (!archive.restoreAll("/images", fgImgFolder, 3, 0)) return false;

    const QString srcTimestamp = QString("%1/timestamp.txt").arg(strPath);
    const QString dstTimestamp = QString("%1/timestamp.txt").arg(fgImgFolder);
    QFile::copy(srcTimestamp, dstTimestamp);

    const QString srcPosition = QString("%1/position.txt").arg(strPath);
    const QString dstPosition = QString("%1/position.txt").arg(fgImgFolder);
    QFile::copy(srcPosition, dstPosition);

    return true;
}

bool TC::restore_flsample_images(const QString& strPath, const QString& key) {
    QStringList nameFilter("ch*");
    QStringList chdirs = QDir(strPath).entryList(nameFilter, QDir::Dirs, QDir::Name);

    clear_flsample_images(key);

    DataArchive archive;

    const QString fgImgFolder = get_sample_images_path(key);
    QDir().mkpath(fgImgFolder);

    foreach(QString chdir, chdirs) {
        const QString strChPath = QString("%1/%2").arg(strPath).arg(chdir);

        const QString arcPath = QString("%1/images.dat").arg(strChPath);
        if (!QFileInfo(arcPath).exists()) return false;

        archive.setFilename(arcPath);

        const QString fgChFolder = QString("%1/%2").arg(fgImgFolder).arg(chdir);
        QDir().mkpath(fgChFolder);

        if (!archive.restoreAll("/images", fgChFolder, 3, 0)) return false;

        const QString srcTimestamp = QString("%1/timestamp.txt").arg(strChPath);
        const QString dstTimestamp = QString("%1/timestamp.txt").arg(fgChFolder);
        QFile::copy(srcTimestamp, dstTimestamp);
    }
    return true;
}
