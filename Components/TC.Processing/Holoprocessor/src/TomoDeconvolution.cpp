#define LOGGER_TAG "TCHoloprocessor::TomoDeconvolution"
#include <TCLogger.h>

#include <QSettings>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QMap>

#include "aqiCommonInit.h"
#include "aqiStatusCodes.h"
#include "aqi3dDecon.h"
#include "aqidllcst.h"

#include "TomoDeconvolution.h"
#include "TomoDeconvolutionConfig.h"

#define RET_KEY "HKEY_CURRENT_USER\\Software\\TomoCube, Inc\\TomoStudio\\Strings"
#define REG_INIT_VAL "3"
#define REG_DECON_VAL "0"

#ifdef HL
#undef HL
#endif
#define HL(a)   ((a) + 0x50)

#ifdef UNHIDE_STRING
#undef UNHIDE_STRING
#endif
#define UNHIDE_STRING(strI, strC) do {		\
	int* ptrI = strI;						\
	char* ptrC = strC;						\
	while (*ptrI) {							\
		*ptrC++ = (char)((*ptrI++)-0x50);	\
	}									\
 } while(0)

#ifdef HIDE_STRING
#undef HIDE_STRING
#endif
#define HIDE_STRING(str)  do {char * ptr = str ; while (*ptr) *ptr++ = '0';} while(0)

QMap<float, float> g_fAberrationProxy;

short cbDetect(struct aqiStructProgressInfo* pstructProgressInfo, void* pCallerInfo) {
    if (!pCallerInfo) return AQI_STATUS_CONTINUE;

    TomoDeconvolution* pInstance = static_cast<TomoDeconvolution*>(pCallerInfo);

    pInstance->UpdateProgress(0.25 * pstructProgressInfo->fPercentComplete / 100);

    return AQI_STATUS_CONTINUE;
}

short cbDeconvolution(struct aqiStructProgressInfo* pstructProgressInfo, void* pCallerInfo) {
    if (!pCallerInfo) return AQI_STATUS_CONTINUE;

    TomoDeconvolution* pInstance = static_cast<TomoDeconvolution*>(pCallerInfo);

    pInstance->UpdateProgress(0.5 + 0.5 * pstructProgressInfo->fPercentComplete / 100);

    return AQI_STATUS_CONTINUE;
}

TomoDeconvolutionParam::TomoDeconvolutionParam() {
    Init();
}

TomoDeconvolutionParam::~TomoDeconvolutionParam() {
    delete[] m_ImgInfo.lpstrImgFile;
    delete[] m_ImgInfo.lpstrRealName;
    delete[] m_PsfInInfo.lpstrPsfFile;
    delete[] m_PsfOutInfo.lpstrPsfFile;
    delete[] m_DeconOpsStd.lpstrImgOutFile;
    delete[] m_DeconOpsStd.lpstrTempDir;
}

void TomoDeconvolutionParam::Init(void) {
    aqiInitStructImgInfo(&m_ImgInfo);
    aqiInitStructPsfInfo(&m_PsfInInfo);
    aqiInitStructPsfInfo(&m_PsfOutInfo);
    aqiInitStructDeconOpsStd(&m_DeconOpsStd);
    aqiInitStructDeconOpsExp(&m_DeconOpsExp);

    // Allocate memory for the structures' strings
    m_ImgInfo.lpstrImgFile = new char[512];
    memset(m_ImgInfo.lpstrImgFile, 0, 512 * sizeof(char));

    m_ImgInfo.lpstrRealName = new char[512];
    memset(m_ImgInfo.lpstrRealName, 0, 512 * sizeof(char));

    m_PsfInInfo.lpstrPsfFile = new char[512];
    memset(m_PsfInInfo.lpstrPsfFile, 0, 512 * sizeof(char));

    m_PsfOutInfo.lpstrPsfFile = new char[512];
    memset(m_PsfOutInfo.lpstrPsfFile, 0, 512 * sizeof(char));

    m_DeconOpsStd.lpstrImgOutFile = new char[512];
    memset(m_DeconOpsStd.lpstrImgOutFile, 0, 512 * sizeof(char));

    m_DeconOpsStd.lpstrTempDir = new char[512];
    memset(m_DeconOpsStd.lpstrTempDir, 0, 512 * sizeof(char));

    m_nDetectMethod = DETECT_SA;
    m_bFastDeconvolution = false;

    // PSF input information structure
    m_PsfInInfo.lpstrPsfFile[0] = 0;
    m_PsfInInfo.shWidth = 0;
    m_PsfInInfo.shHeight = 0;
    m_PsfInInfo.shNumSlices = 0;
    m_PsfInInfo.fPixelSizeX = 0;
    m_PsfInInfo.fPixelSizeY = 0;
    m_PsfInInfo.fPixelSizeZ = 0;
    m_PsfInInfo.enPsfSource = (enumPsfSources)PSFSRC_CALCULATED;

    // PSF output information structure
    m_PsfOutInfo.lpstrPsfFile[0] = 0;
}

bool TomoDeconvolutionParam::LoadParameters(const QString& deconvConfig, int chidx) {
    try {
        TomoDeconvolutionConfig config(deconvConfig);
        LoadParameters(config, chidx);
    } catch (...) {
        return false;
    }

    return true;
}

bool TomoDeconvolutionParam::LoadParameters(TomoDeconvolutionConfig& config, int chidx) {
    // Sample information
    m_ImgInfo.fSampleDepth = config.m_fSampleDepth;

    // Standard algorithm settings
    m_DeconOpsStd.enDeconMeth = (enumDeconMeths)config.m_nDeconvolutionMethod;
    m_DeconOpsStd.enDarkCurMeth = (enumDarkCurMeths)config.m_nDarkCurrentCalculation[chidx];
    m_DeconOpsStd.fDarkCurrent = config.m_fDarkCurrent[chidx];
    m_DeconOpsStd.shNumIterations = config.m_nNumberOfIteration;
    m_DeconOpsStd.shSaveInterval = config.m_nSaveInterval;
    m_DeconOpsStd.shBinFactorXY = config.m_nBinFactorXY;
    m_DeconOpsStd.shBinFactorZ = config.m_nBinFactorZ;
    m_DeconOpsStd.bEnableGpuProcessing = config.m_bGPUProcessing;

    // Expert algorithm settings
    m_DeconOpsExp.enImgGuessMeth = (enumImgGuessMeths)config.m_nImageFirstGuess;
    m_DeconOpsExp.enFreqConsMeth = (enumFreqConsMeths)config.m_nFrequencyConstraint;
    m_DeconOpsExp.enPsfGuessMeth = (enumPsfGuessMeths)config.m_nPSFFirstGuess;
    m_DeconOpsExp.enSubVolMeth = (enumSubVolMeths)config.m_nSubvolumeCalculation;
    m_DeconOpsExp.shGuardBand = config.m_nGuardbandXY;
    m_DeconOpsExp.shGuardBandZ = config.m_nGuardbandZ;
    m_DeconOpsExp.shSubVolOverlap = config.m_nSubvolumeOverlap;
    m_DeconOpsExp.bMontageXY = config.m_bSubvolumeInXY;
    m_DeconOpsExp.bMontageZ = config.m_bSubvolumeInZ;
    m_DeconOpsExp.bEnablePsfCons = config.m_bEnablePSFConstraints;
    m_DeconOpsExp.bEnableClassicConfocalAlgorithm = config.m_bEnableClassicConfocal;
    m_DeconOpsExp.fSuppressNoiseFactor = config.m_fNoiseSmoothingFactor;
    m_DeconOpsExp.fPsfStretchFactor = config.m_fPSFStretchFactor;
    m_DeconOpsExp.fPsfCentralRadius = config.m_fPSFWaistRadius;
    m_DeconOpsExp.shGoldsSmoothIteration = config.m_nGoldsGaussInterval;
    m_DeconOpsExp.fGoldsSmoothGauss = config.m_fGoldsGaussFWHM;
    m_DeconOpsExp.enPsfGenMeth = (enumTheoreticalPsfGenMeths)config.m_nPSFGenerationAlgorithm;

    m_DeconOpsExp.shIntensityCorrection = (config.m_bPerformIntensityCorrection) ? 1 : 0;

    return true;
}

TomoDeconvolutionActivator::TomoDeconvolutionActivator(void)
    : m_bUnlocked(false) {
}

TomoDeconvolutionActivator::Pointer TomoDeconvolutionActivator::GetInstance(void) {
    static TomoDeconvolutionActivator::Pointer theInstance(new TomoDeconvolutionActivator());
    return theInstance;
}

bool TomoDeconvolutionActivator::Unlock(void) {
    Pointer theInstance = GetInstance();

    if (theInstance->m_bUnlocked) return true;

    //2812 F100 98CE 42B1 0146 5000 5482 9622
    int pInitProgKeyI[] = { HL('2'), HL('8'), HL('1'), HL('2'),
     HL('F'), HL('1'), HL('0'), HL('0'),
     HL('9'), HL('8'), HL('C'), HL('E'),
     HL('4'), HL('2'), HL('B'), HL('1'),
     HL('0'), HL('1'), HL('4'), HL('6'),
     HL('5'), HL('0'), HL('0'), HL('0'),
     HL('5'), HL('4'), HL('8'), HL('2'),
     HL('9'), HL('6'), HL('2'), HL('2'), 0 };
    char pInitProgKeyC[33] = { 0, };
    UNHIDE_STRING(pInitProgKeyI, pInitProgKeyC);
    if (AQI_STATUS_DONE != aqiUnlockCommonInitDll(pInitProgKeyC, RET_KEY, REG_INIT_VAL)) {
        HIDE_STRING(pInitProgKeyC);
        QLOG_ERROR() << "Failed to init the deconvolution module";
        return false;
    }
    HIDE_STRING(pInitProgKeyC);

    //701B D040 9C1E 02A1 0756 2004 1083 1622
    int pDeconProgKeyI[] = { HL('7'), HL('0'), HL('1'), HL('B'),
     HL('D'), HL('0'), HL('4'), HL('0'),
     HL('9'), HL('C'), HL('1'), HL('E'),
     HL('0'), HL('2'), HL('A'), HL('1'),
     HL('0'), HL('7'), HL('5'), HL('6'),
     HL('2'), HL('0'), HL('0'), HL('4'),
     HL('1'), HL('0'), HL('8'), HL('3'),
     HL('1'), HL('6'), HL('2'), HL('2'), 0 };

    char pDeconProgKeyC[33] = { 0, };
    UNHIDE_STRING(pDeconProgKeyI, pDeconProgKeyC);
    if (AQI_STATUS_DONE != aqiUnlock3dBlindDeconDll(pDeconProgKeyC, RET_KEY, REG_DECON_VAL)) {
        HIDE_STRING(pDeconProgKeyC);
        QLOG_ERROR() << "Failed to unlock the deconvolution module";
        return false;
    }
    HIDE_STRING(pDeconProgKeyC);

    theInstance->m_bUnlocked = true;

    QLOG_INFO() << "Deconvolution module is activated";

    return true;
}

bool TomoDeconvolutionActivator::IsUnlocked(void) {
    Pointer theInstance = GetInstance();
    if (!theInstance->m_bUnlocked) {
        TomoDeconvolutionActivator::Unlock();  //Unlock deconvolution solution...
    }
    return theInstance->m_bUnlocked;
}

TomoDeconvolution::TomoDeconvolution(ProgressReporter* reporter)
    : m_pReporter((reporter == 0) ? new NoProgressReporter() : reporter) {
}

TomoDeconvolution::~TomoDeconvolution(void) {
}

bool TomoDeconvolution::SetSystemInfo(float NA, float ImmersionRI, float WaveLength) {
    if (!TomoDeconvolutionActivator::IsUnlocked()) return false;

    // Image information structure		
    m_param.m_ImgInfo.enDataType = (enumDataTypes)DT_UINT_8BIT;
    m_param.m_ImgInfo.enScopeModality = (enumModalities)MOD_WF_FLUORESCENCE;
    m_param.m_ImgInfo.fNumericAperture = NA;
    m_param.m_ImgInfo.fRefractiveIndex = ImmersionRI;
    m_param.m_ImgInfo.fEmmWavelength = WaveLength;
    m_param.m_ImgInfo.fEmbeddingRefractiveIndex = 0;
    m_param.m_ImgInfo.fSphereAberrationFactor = 0;
    m_param.m_ImgInfo.fSphereAberrationFactorO2 = 0;

    return true;
}

bool TomoDeconvolution::LoadParameters(const QString& strDeconvConfig, int chidx) {
    if (!TomoDeconvolutionActivator::IsUnlocked()) return false;
    return m_param.LoadParameters(strDeconvConfig, chidx);
}

bool TomoDeconvolution::LoadParameters(TomoDeconvolutionConfig& config, int chidx) {
    if (!TomoDeconvolutionActivator::IsUnlocked()) return false;
    return m_param.LoadParameters(config, chidx);
}

bool TomoDeconvolution::SetImageInfo(int imgWidth, int imgHeight, int imgSlices, float fPixelX, float fPixelY, float fPixelZ) {
    if (!TomoDeconvolutionActivator::IsUnlocked()) return false;
    // Image information structure		
    m_param.m_ImgInfo.shWidth = imgWidth;
    m_param.m_ImgInfo.shHeight = imgHeight;
    m_param.m_ImgInfo.shNumSlices = imgSlices;
    m_param.m_ImgInfo.fPixelSizeX = fPixelX;
    m_param.m_ImgInfo.fPixelSizeY = fPixelY;
    m_param.m_ImgInfo.fPixelSizeZ = fPixelZ;

    return true;
}

bool TomoDeconvolution::SetSampleInfo(float mediumRI) {
    m_param.m_ImgInfo.fRefractiveIndex = mediumRI;
    return true;
}

bool TomoDeconvolution::Process(const QString& strInput, const QString strDir, int chidx, bool isFirstFrame) {
    if (!TomoDeconvolutionActivator::IsUnlocked()) return false;
    const QString strOutput = QString("%1\\deconv_out_%2.raw").arg(strDir).arg(chidx);

    m_strOutput.clear();

    // Image information structure
    strcpy_s(m_param.m_ImgInfo.lpstrImgFile, 512, strInput.toLocal8Bit().constData());
    strcpy_s(m_param.m_ImgInfo.lpstrRealName, 512, strInput.toLocal8Bit().constData());

    // Standard algorithm settings
    const QString strTemp = QFileInfo(strOutput).absoluteDir().absolutePath();
    strcpy_s(m_param.m_DeconOpsStd.lpstrTempDir, 512, strTemp.toLatin1().constData());
    strcpy_s(m_param.m_DeconOpsStd.lpstrImgOutFile, 512, strOutput.toLocal8Bit().constData());

    // Output Psf settings
    m_param.m_PsfInInfo.lpstrPsfFile[0] = 0;
    m_param.m_PsfOutInfo.lpstrPsfFile[0] = 0;


    //validate input parameters...
    if (!IsValidParameter(m_param)) return false;

    //fix parameters, if necessary
    QLOG_TRACE() << "Decon iteration = " << m_param.m_DeconOpsStd.shNumIterations << " save interval = " << m_param.m_DeconOpsStd.shSaveInterval;
    m_param.m_DeconOpsStd.shSaveInterval = std::min<short>(m_param.m_DeconOpsStd.shSaveInterval, m_param.m_DeconOpsStd.shNumIterations);

    //start deconvolution...
    try {
        QLOG_TRACE() << "Start deconvolution";
        QLOG_TRACE() << "> Deconvolution Method = " << m_param.m_DeconOpsStd.enDeconMeth;
        QLOG_TRACE() << "> Number of Iteration = " << m_param.m_DeconOpsStd.shNumIterations;
        QLOG_TRACE() << "> PSF Generation Algorithm = " << m_param.m_DeconOpsExp.enPsfGenMeth;
        QLOG_TRACE() << "> Image First Guess = " << m_param.m_DeconOpsExp.enImgGuessMeth;
        QLOG_TRACE() << "> Sample Depth = " << m_param.m_ImgInfo.fSampleDepth;


        short retVal = aqi3dBlindDeconvolution(&m_param.m_ImgInfo,
            &m_param.m_PsfInInfo,
            &m_param.m_PsfOutInfo,
            &m_param.m_DeconOpsStd,
            &m_param.m_DeconOpsExp,
            static_cast<aqiCbfnDllStatus>(cbDeconvolution),
            this);

        UpdateStatus(retVal);

        if (retVal != AQI_STATUS_NOERROR) {
            IsValidParameter(m_param);
            return false;
        }
    } catch (std::exception & ex) {
        QLOG_ERROR() << "Exception occurred during deconvolution : " << ex.what();
        return false;
    } catch (...) {
        QLOG_ERROR() << "Exception occurred during deconvolution...";
        return false;
    }

    QFileInfo finfo(strOutput);
    const QString dirPath = finfo.absoluteDir().absolutePath();
    const int iterCount = m_param.m_DeconOpsStd.shNumIterations;
    const QString fileName = finfo.fileName();
    m_strOutput = QString("%1/%2_%3").arg(dirPath).arg(iterCount).arg(fileName);

    return true;
}

QString TomoDeconvolution::GetOutputName(void) {
    return m_strOutput;
}

bool TomoDeconvolution::Detect(TomoDeconvolutionParam& param, bool bUseStoredValue) {
    short retVal;

    // Decide which detection option is selected and call the appropriate routine
    switch (param.m_nDetectMethod) {
    case TomoDeconvolutionParam::DETECT_SA:
        // Detecting spherical aberration - the PSF generation expert setting will
        // determine which SA detection method is used internally
        if (bUseStoredValue && g_fAberrationProxy.find(param.m_ImgInfo.fEmmWavelength) != g_fAberrationProxy.end()) {
            retVal = AQI_STATUS_NOERROR;
            param.m_ImgInfo.fSphereAberrationFactor = g_fAberrationProxy[param.m_ImgInfo.fEmmWavelength];

            QLOG_INFO() << "Deconv : Use pre-stored SA value " << param.m_ImgInfo.fEmmWavelength << ":" << param.m_ImgInfo.fSphereAberrationFactor;
        } else {
            retVal = aqi3dDeconDetectSA(&param.m_ImgInfo,
                0,
                &param.m_DeconOpsStd,
                &param.m_DeconOpsExp,
                -15.0f,
                15.0f,
                1,
                static_cast<aqiCbfnDllStatus>(cbDetect),
                this);

            if (retVal == AQI_STATUS_NOERROR) {
                g_fAberrationProxy[param.m_ImgInfo.fEmmWavelength] = param.m_ImgInfo.fSphereAberrationFactor;
                QLOG_INFO() << "Deconv : Detect SA " << param.m_ImgInfo.fEmmWavelength << ":" << param.m_ImgInfo.fSphereAberrationFactor;
            }
        }
        break;
    case TomoDeconvolutionParam::DETECT_SAMPLE_RI:
        // Detecting sample refractive index - the PSF generation expert setting
        // will be set to Gibson-Lanni by this routine
        retVal = aqi3dDeconDetectSampleRI(&param.m_ImgInfo,
            0,
            &param.m_DeconOpsStd,
            &param.m_DeconOpsExp,
            0.0f,
            0.0f,
            1,
            static_cast<aqiCbfnDllStatus>(cbDetect),
            this);
        break;
    case TomoDeconvolutionParam::DETECT_SAMPLE_DEPTH:
        // Detecting sample depth - the PSF generation expert setting
        // will be set to Gibson-Lanni by this routine
        // When this method completes processing, the detected sample depth value 
        // will be placed into the fSampleDepth field of the aqiStructImgInfo structure.
        retVal = aqi3dDeconDetectSampleDepth(&param.m_ImgInfo,
            0,
            &param.m_DeconOpsStd,
            &param.m_DeconOpsExp,
            0.0f,
            0.0f,
            1,
            static_cast<aqiCbfnDllStatus>(cbDetect),
            this);
        break;
    }

    UpdateStatus(retVal);

    return (retVal == AQI_STATUS_NOERROR);
}

QString TomoDeconvolution::GetLastErrorMessage(void) const {
    return m_strError;
}

void TomoDeconvolution::UpdateStatus(short nCode) {
    int nBufferSize = -1;

    switch (nCode) {
    case AQI_STATUS_NOERROR:
        m_strError = "No Error";
        break;
    case AQI_STATUS_ERROR:
        // Try to retrieve an error message
        aqi3dDeconGetLastError(nullptr, &nBufferSize);
        if (nBufferSize == 0) {
            m_strError = "General Error";
        } else {
            char* strError = new char[nBufferSize];
            aqi3dDeconGetLastError(strError, &nBufferSize);
            m_strError = QString("%1").arg(strError);
            delete[] strError;
        }
        break;
    case AQI_STATUS_CANCEL:
        m_strError = "Operation was cancelled";
        break;
    case AQI_STATUS_BLANKCHANNEL:
        m_strError = "Image is blank";
        break;
    case AQI_STATUS_INVALIDINPUT:
        m_strError = "Invalid input parameters";
        break;
    case AQI_STATUS_DATAFORMATERROR:
        m_strError = "Data format error";
        break;
    case AQI_STATUS_OUTOFDISK:
        m_strError = "Ran out of disk space";
        break;
    case AQI_STATUS_OUTOFMEMORY:
        m_strError = "Ran out of memory";
        break;
    case AQI_STATUS_FILEREADERROR:
        m_strError = "Error reading file";
        break;
    case AQI_STATUS_FILEWRITEERROR:
        m_strError = "Error writing to file";
        break;
    case AQI_STATUS_DLLLOCKED:
        m_strError = "DLL is locked";
        break;
    default:
        m_strError = QString("Error code: %1").arg(nCode);
        break;
    }
}

bool TomoDeconvolution::IsValidParameter(TomoDeconvolutionParam& param) {
    aqiStructDecon3dValidationFlags flags;
    short retVal = aqi3dDeconValidateInputs(&param.m_ImgInfo,
        &param.m_PsfInInfo,
        &param.m_PsfOutInfo,
        &param.m_DeconOpsStd,
        &param.m_DeconOpsExp,
        &flags);

    if (retVal != AQI_STATUS_NOERROR) {
        QLOG_ERROR() << "Deconvolution input parameter validation flags...";
        QLOG_INFO() << "  ImgInfo=" << flags.lImgInfoValidationFlags;
        QLOG_INFO() << "  PsfInfoInput=" << flags.lPsfInfoInputValidationFlags;
        QLOG_INFO() << "  PsfInfoOutput=" << flags.lPsfInfoOutputValidationFlags;
        QLOG_INFO() << "  OpsStd=" << flags.lOpsStdValidationFlags;
        QLOG_INFO() << "  OpsExp=" << flags.lOpsExpValidationFlags;
        QLOG_INFO() << "  DeconMixed=" << flags.lDeconMixedValidationFlags;
        return false;
    }

    return true;
}

void TomoDeconvolution::UpdateProgress(double proc) {
    m_pReporter->notify(100 * proc);
}
