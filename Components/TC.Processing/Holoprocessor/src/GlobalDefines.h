#pragma once

namespace TC {
    //! Acquisition mode
    enum {
        SINGLESHOT_MODE = 1,
        TIMELAPSE_MODE = 2
    };

    //! Motion axis
    enum {
        AXIS_ALL = -1,		// Special code to access all axes at the same time
        AXIS_X = 0,
        AXIS_Y = 1,
        AXIS_Z = 2,
        AXIS_C = 3,
        AXIS_MAX = 4
    };

    enum {
        AXIS_X_PLUS = 1,
        AXIS_X_MINUS = AXIS_X_PLUS * (-1),
        AXIS_Y_PLUS = 1,
        AXIS_Y_MINUS = AXIS_Y_PLUS * (-1),
        AXIS_Z_UP = -1,
        AXIS_Z_DOWN = AXIS_Z_UP * (-1),
        AXIS_C_UP = -1,
        AXIS_C_DOWN = AXIS_C_UP * (-1)
    };

    typedef enum {
        STEP_OUTOFRANGE,
        STEP_FAR,
        STEP_NEAR,
        STEP_FINE,
        STEP_ESTIMATION
    } CalibrationStepType;

    typedef enum {
        DIST_NEAR_THRESHOLD,
        DIST_FINE_THRESHOLD
    } CalibrationDistThreshold;

    //Fluorscence channel - excitation wavelenth 기준
    enum class FLChannel {
        CHANNEL_BLUE = 0,
        CHANNEL_GREEN = 1,
        CHANNEL_RED = 2,


        CHANNEL_COUNT,					//! total number of channels
        CHANNEL_NOTDEFINED = 100		//! indicate not-defined channel
    };

    //Brightfield light channel
    enum class BFChannel {
        CHANNEL_RED = 0,
        CHANNEL_GREEN = 1,
        CHANNEL_BLUE = 2,

        CHANNEL_COUNT,              //! total number of channels
        CHANNEL_COLOR = 99,
        CHANNEL_NOTDEFINED = 100    //! indicate not-defined channel
    };

    //Trigger mode
    typedef enum {
        TRIGGER_OFF = 0,
        TRIGGER_ON_HW = 1,
        TRIGGER_ON_SW = 2,
        TRIGGER_NOT_SET = 1000,
    } TriggerMode;

    //Camera tyep
    typedef enum {
        CAMERA_SIMULATOR,
        CAMERA_POINTGREY,
        CAMERA_BASLER,
        CAMERA_UNKNOWN
    } CameraType;

    //TODO Pattern Type을 Class로 변경하기
    //     - type : lee hologram / sim - processing 방법 결정
    //     - bit : single / multi - acqusition 방법 결정
    typedef enum {
        //Legacy patterns - 1000~1999
        PATTERN_LEGACY = 1000,					//! 1bit lee hologram
        PATTERN_LEGACY_V2,						//! 1bit lee hologram

        //SIM patterns - 2000~2999
        PATTERN_MULTIBITS_V001 = 2000,			//! 4bit sim

        //Lee hologram patterns - 3000~3999
        PATTERN_MULTIBITS_LEE_V001 = 3000,		//! 4bit lee hologram
        PATTERN_MULTIBITS_LEE_V002 = 3001,		//! 4bit lee hologram
    } DMDPatternType;

    typedef enum {
        PATTERN_GROUP_UNKNOWN,
        PATTERN_GROUP_LEGACY,
        PATTERN_GROUP_LEE_HOLOGRAMM,
        PATTERN_GROUP_SIM
    } DMDPatternGroupType;

    typedef enum {
        IMG_HOLOGRAM = 0x000000,

        IMG_PROCESSED = 0x000100,
        IMG_PHASE,
        IMG_BRIGHTFIELD,
        IMG_DIC,
        IMG_PHASE_CONTRAST,
        IMG_ANALYSIS,

        //Fluorescence
        IMG_FLUORESCENCE = 0x000200,
        IMG_FLUORESCENCE_CH0,
        IMG_FLUORESCENCE_CH1,
        IMG_FLUORESCENCE_CH2,

        //Invalid
        IMG_INVALID = 0x010000
    } ImageType;

    enum class ImagingMode {
        Hologram,
        Fluorescence,
        BrightField
    };
};