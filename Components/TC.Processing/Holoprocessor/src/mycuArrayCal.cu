#include "mycuArrayCal.h"

__global__ void
ElementMultiply(int* dev_PQarray, cufftComplex* dev_fft_array, cufftComplex* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index].x = dev_PQarray[index] * dev_fft_array[index].x;
        dev_output_array[index].y = dev_PQarray[index] * dev_fft_array[index].y;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementMultiply(float* dev_Array1, float* dev_Array2, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_Array1[index] * dev_Array2[index];
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementMultiply(float* dev_Array, float mulitplier, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_Array[index] * mulitplier;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementDivide(cufftComplex* dev_fft_array, int* dev_PQarray, cufftComplex* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        if (dev_PQarray[index] != 0) {
            dev_output_array[index].x = dev_fft_array[index].x / dev_PQarray[index];
            dev_output_array[index].y = dev_fft_array[index].y / dev_PQarray[index];
        } else {
            dev_output_array[index].x = 0;
            dev_output_array[index].y = 0;
        }
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementDivide(cufftComplex* targetarray, cufftComplex* dev_dividerarray, cufftComplex* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        const float a = targetarray[index].x;
        const float b = targetarray[index].y;
        const float c = dev_dividerarray[index].x;
        const float d = dev_dividerarray[index].y;


        const float divider_term = c * c + d * d;
        const float real_term = a * c + b * d;
        const float imag_term = b * c - a * d;


        if ((dev_dividerarray[index].x != 0) && (dev_dividerarray[index].y != 0)) {
            dev_output_array[index].x = real_term / divider_term;
            dev_output_array[index].y = imag_term / divider_term;
        } else {
            dev_output_array[index].x = 0;
            dev_output_array[index].y = 0;
        }
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementDivide(float* dev_fft_array, int divider, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_fft_array[index] / divider;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementDivide(cufftComplex* dev_array, const int divider, cufftComplex* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index].x = dev_array[index].x / divider;
        dev_output_array[index].y = dev_array[index].y / divider;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementAdd(float* dev_Array1, float* dev_Array2, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_Array1[index] + dev_Array2[index];
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementSubtract(float* dev_Array, float* dev_Array_minus, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_Array[index] - dev_Array_minus[index];
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}


__global__ void
ElementRound(float* dev_Array, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = roundf(dev_Array[index]);
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementImag(cufftComplex* dev_input_array, cufftComplex* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index].x = dev_input_array[index].y;
        dev_output_array[index].y = 0;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
ElementReal(cufftComplex* dev_input_array, float* dev_output_array, const int Size) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        dev_output_array[index] = dev_input_array[index].x;
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}


__global__ void
Element_opt1(cufftComplex* dev_input_array, cufftComplex* dev_output_array, int divider, cufftComplex* dev_divider_array, const int Size) {
    //Three funcion is in here
    //1. Normalization division of inverse fourier transform
    //2. element divide of two arrays
    //3. Imagine value extraction

    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);


    while (index < Size) {
        const float a = dev_input_array[index].x / divider;
        const float b = dev_input_array[index].y / divider;
        const float c = dev_divider_array[index].x;
        const float d = dev_divider_array[index].y;


        const float divider_term = c * c + d * d;
        const float imag_term = b * c - a * d;


        if ((dev_divider_array[index].x != 0) && (dev_divider_array[index].y != 0)) {
            dev_output_array[index].x = imag_term / divider_term;
            dev_output_array[index].y = 0;
        } else {
            dev_output_array[index].x = 0;
            dev_output_array[index].y = 0;
        }
        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }



}