#pragma once

#include <QString>
#include <QMap>

namespace TC {
    class ImageSet;
    class FieldRetrievalParam;

    class TomoBuilderFieldRetrievalInterface {
    public:
        TomoBuilderFieldRetrievalInterface(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam);

    public:
        //ignorePhaseSign - if ture, use phase sign value as -1 instead of parameter
        virtual bool FieldRetrievalBgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool miscCallibrationOnly = false, bool ignorePhaseSign = false) = 0;
        virtual bool FieldRetrievalFgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool ignorePhaseSign = false, bool checkQuality = true) = 0;

        bool FieldRetrieval(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool checkQuality = true);
        bool FieldRetrievalAtCalibration();

        void CalcZeroPaddingSize(unsigned int& ZP, unsigned int& ZP2, unsigned int& ZP3, unsigned int crop_size, unsigned int original_size, double res);

    protected:
        QMap<QString, double>& m_parameter;
        ImageSet* m_pBgImageSet;
        ImageSet* m_pRawImageSet;
        FieldRetrievalParam* m_pFieldRetrievalParam;

    public:
        double m_dCropSize;
        double m_dCropFactor;
        double m_dRes2;
        double m_dRes3;
        double m_dRes4;
    };
};
