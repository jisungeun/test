#pragma once

#include <cufft.h>
#include "cuda_runtime_api.h"

#include <string>

auto LogCudaError(const cudaError_t& cudaError, const std::string& contents)->void;

auto LogCufftPlanError(const cufftResult_t& cufftResult, const std::string& functionName, const int32_t& arraySizeX,
    const int32_t& arraySizeY, const cufftType_t& cufftType)->void;
auto LogCufftExecError(const cufftResult_t& cufftResult, const std::string& functionName,
    const std::string& inputArrayName, const std::string& outputArrayName, const int32_t& direction) -> void;

auto GetCufftResultName(const cufftResult_t& cufftResult)->std::string;
auto GetCufftTypeName(const cufftType_t& cufftType)->std::string;
auto GetCufftDirection(const int32_t& direction)->std::string;
