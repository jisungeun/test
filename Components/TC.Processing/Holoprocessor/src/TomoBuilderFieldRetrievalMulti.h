#pragma once

#include "TomoBuilderFieldRetrievalInterface.h"

namespace TC {
    class TomoBuilderFieldRetrievalMulti : public TomoBuilderFieldRetrievalInterface {
    public:
        TomoBuilderFieldRetrievalMulti(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam);

    public:
        //ignorePhaseSign - if ture, use phase sign value as -1 instead of parameter
        bool FieldRetrievalBgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool miscCallibrationOnly = false, bool ignorePhaseSign = false) override;
        bool FieldRetrievalFgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool ignorePhaseSign = false, bool checkQuality = true) override;

    protected:
        void NormalField(const af::array& image, af::array& EnormF, af::array& NAmask, af::array& addRamp, double& cx, double& cy, const int tag = 0);

        af::array Decomposition(const af::array& images, const int tag = 0);

        void CalcCropSize(int rawSize, double pixelRes, double NA, double wl, int& xSize, int& ySize, int& d0, int& d1);
    };
};