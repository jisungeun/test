#include "TomoBuilderUtility.h"
#include "AfArrayProxy.h"

TC::AfArrayProxy::AfArrayProxy(void)
    : m_dtype(f32)
    , m_pRawArray(0) {
}

TC::AfArrayProxy::AfArrayProxy(af::array& inArray)
    : m_dtype(f32)
    , m_pRawArray(0) {
    m_dtype = inArray.type();

    Set(inArray);
}

TC::AfArrayProxy::~AfArrayProxy() {
    if (m_pRawArray) delete[] m_pRawArray;
}

void TC::AfArrayProxy::Set(af::array& inArray) {
    if (m_pRawArray) delete[] m_pRawArray;

    inArray.eval();

    m_dims = inArray.dims();

    switch (m_dtype) {
    case f32: ///< 32-bit floating point values
        m_pRawArray = copy_to_host_f32(inArray);
        break;
    case c32: ///< 32-bit complex floating point values
        m_pRawArray = copy_to_host_c32(inArray);
        break;
    case f64: ///< 64-bit complex floating point values
        m_pRawArray = copy_to_host_f64(inArray);
        break;
    case c64: ///< 64-bit complex floating point values
        m_pRawArray = copy_to_host_c64(inArray);
        break;
    case b8: ///< 8-bit boolean values
        m_pRawArray = copy_to_host_b8(inArray);
        break;
    case s32: ///< 32-bit signed integral values
        m_pRawArray = copy_to_host_s32(inArray);
        break;
    case u32: ///< 32-bit unsigned integral values
        m_pRawArray = copy_to_host_u32(inArray);
        break;
    case u8: ///< 8-bit unsigned integral values
        m_pRawArray = copy_to_host_u8(inArray);
        break;
    case s64: ///< 64-bit signed integral values
        m_pRawArray = copy_to_host_s64(inArray);
        break;
    case u64: ///< 64-bit unsigned integral values
        m_pRawArray = copy_to_host_u64(inArray);
        break;
    case s16: ///< 16-bit signed integral values
        m_pRawArray = copy_to_host_s16(inArray);
        break;
    case u16: ///< 16-bit unsigned integral values
        m_pRawArray = copy_to_host_u16(inArray);
        break;
    }

    inArray = af::constant(0, 1, 1);
    inArray.eval();

    TC::release();
}

bool TC::AfArrayProxy::Get(af::array& outArray) {
    if (!m_pRawArray) return false;

    switch (m_dtype) {
    case f32: ///< 32-bit floating point values
        outArray = af::array(m_dims, (float*)m_pRawArray);
        break;
    case c32: ///< 32-bit complex floating point values
        outArray = af::array(m_dims, (af::cfloat*)m_pRawArray);
        break;
    case f64: ///< 64-bit complex floating point values
        outArray = af::array(m_dims, (double*)m_pRawArray);
        break;
    case c64: ///< 64-bit complex floating point values
        outArray = af::array(m_dims, (af::cdouble*)m_pRawArray);
        break;
    case b8: ///< 8-bit boolean values
        outArray = af::array(m_dims, (char*)m_pRawArray);
        break;
    case s32: ///< 32-bit signed integral values
        outArray = af::array(m_dims, (int*)m_pRawArray);
        break;
    case u32: ///< 32-bit unsigned integral values
        outArray = af::array(m_dims, (unsigned int*)m_pRawArray);
        break;
    case u8: ///< 8-bit unsigned integral values
        outArray = af::array(m_dims, (unsigned char*)m_pRawArray);
        break;
    case s64: ///< 64-bit signed integral values
        outArray = af::array(m_dims, (long long*)m_pRawArray);
        break;
    case u64: ///< 64-bit unsigned integral values
        outArray = af::array(m_dims, (unsigned long long*)m_pRawArray);
        break;
    case s16: ///< 16-bit signed integral values
        outArray = af::array(m_dims, (int16_t*)m_pRawArray);
        break;
    case u16: ///< 16-bit unsigned integral values
        outArray = af::array(m_dims, (uint16_t*)m_pRawArray);
        break;
    }

    outArray.eval();

    return true;
}

void* TC::AfArrayProxy::copy_to_host_f32(af::array& inArray) {
    const dim_t elements = inArray.elements();

    float* hA = new float[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_c32(af::array& inArray) {
    const dim_t elements = inArray.elements();

    af::cfloat* hA = new af::cfloat[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_f64(af::array& inArray) {
    const dim_t elements = inArray.elements();

    double* hA = new double[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_c64(af::array& inArray) {
    const dim_t elements = inArray.elements();

    af::cdouble* hA = new af::cdouble[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_b8(af::array& inArray) {
    const dim_t elements = inArray.elements();

    char* hA = new char[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_s32(af::array& inArray) {
    const dim_t elements = inArray.elements();

    int* hA = new int[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_u32(af::array& inArray) {
    const dim_t elements = inArray.elements();

    unsigned int* hA = new unsigned int[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_u8(af::array& inArray) {
    const dim_t elements = inArray.elements();

    unsigned char* hA = new unsigned char[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_s64(af::array& inArray) {
    const dim_t elements = inArray.elements();

    long long* hA = new long long[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_u64(af::array& inArray) {
    const dim_t elements = inArray.elements();

    unsigned long long* hA = new unsigned long long[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_s16(af::array& inArray) {
    const dim_t elements = inArray.elements();

    int16_t* hA = new int16_t[elements];
    inArray.host(hA);

    return hA;
}

void* TC::AfArrayProxy::copy_to_host_u16(af::array& inArray) {
    const dim_t elements = inArray.elements();

    uint16_t* hA = new uint16_t[elements];
    inArray.host(hA);

    return hA;
}

