#define LOGGER_TAG "TCHoloprocessor::TomoBuilderFieldRetrievalInterface"
#include <TCLogger.h>

#include "TomoBuilderGlobal.h"
#include "TomoBuilderFieldRetrievalInterface.h"

TC::TomoBuilderFieldRetrievalInterface::TomoBuilderFieldRetrievalInterface(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam)
    : m_parameter(param)
    , m_pBgImageSet(pBgImageSet)
    , m_pRawImageSet(pRawImageSet)
    , m_pFieldRetrievalParam(pFieldRetrievalParam) {
}

bool TC::TomoBuilderFieldRetrievalInterface::FieldRetrieval(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool checkQuality) {
    if (!FieldRetrievalBgOnly(hasNormal, onlyNormalOnBg, zeroPadding, false)) return false;
    return FieldRetrievalFgOnly(hasNormal, onlyNormalOnBg, zeroPadding, false, checkQuality);
}

bool TC::TomoBuilderFieldRetrievalInterface::FieldRetrievalAtCalibration() {
    return FieldRetrievalBgOnly(true, false, 0, true);
}

void TC::TomoBuilderFieldRetrievalInterface::CalcZeroPaddingSize(unsigned int& ZP, unsigned int& ZP2, unsigned int& ZP3, unsigned int crop_size, unsigned int original_size, double res) {
    const double NA = m_parameter[PN_NA];
    const double NA_CDS = m_parameter[PN_NA_CDS];
    const double n_m = m_parameter[PN_NM];
    const double lambda = m_parameter[PN_LAMBDA];

    m_dCropSize = crop_size;

    //! MATLAB: crop_factor=crop_size/original_size;
    const double crop_factor = crop_size / (1.0 * original_size);
    m_dCropFactor = crop_factor;

    //! MATLAB: res2=res/crop_factor;
    const double res2 = res / crop_factor;
    m_dRes2 = res2;


    //! MATALB: ZP = min(512, pow2(ceil(log2(crop_size*3))));
    ZP = std::min(512.0, std::pow(2, std::ceil(log(crop_size * 3) / log(2))));

    //! MATLAB: ZP2 = ZP
    ZP2 = ZP;

    //! MATLAB: res3 = res2*ZP/ZP2;
    const double res3 = res2 * ZP / ZP2;
    m_dRes3 = res3;

    //! MATLAB: res4 = lambda/((n_m-sqrt(n_m.^2-NA.^2))+(n_m-sqrt(n_m.^2-NA.^2)));
    const double denom1 = (n_m - std::sqrt(std::pow(n_m, 2) - std::pow(NA, 2)));
    const double denom2 = (n_m - std::sqrt(std::pow(n_m, 2) - std::pow(NA_CDS, 2)));
    const double res4 = lambda / (denom1 + denom2);
    m_dRes4 = res4;

    //! MATLAB: ZP3 = round(res2*ZP/res4/2)*2;
    ZP3 = std::round(res2*ZP / res4 / 2.0) * 2;

    //QLOG_INFO() << "crop_size=" << crop_size << " original_size=" << original_size << " crop_factor=" << crop_factor << " ZP=" << ZP << " ZP2=" << ZP2 << " ZP3=" << ZP3;
    //QLOG_INFO() << "res=" << res << " res2=" << res2 << " res3=" << res3 << " res4=" << res4;
}