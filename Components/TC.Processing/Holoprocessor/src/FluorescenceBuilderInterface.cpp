#include <QFile>
#include <QStringList>
#include <QDir>
#include <memory>
#include <af/macros.h>
#include <limits>

#include <TCLogger.h>
#include "TomoBuilderGlobal.h"
#include "TomoBuilderUtility.h"
#include "FluorescenceBuilderInterface.h"
#include "TomoDeconvolution.h"
#include "TomoDeconvolutionConfig.h"
#include "SystemConfig.h"

namespace TC {
    class DeconProgressReporter : public ProgressReporter {
    public:
        DeconProgressReporter(FluorescenceBuilderInterface* fluorescenceBuilderInterface)
            : ProgressReporter()
            , m_interface(fluorescenceBuilderInterface) {
        }

        void notify(int value) override {
            m_interface->reportDeconvProgress(value);
        }

    private:
        FluorescenceBuilderInterface* m_interface;
    };

    FluorescenceBuilderInterface::FluorescenceBuilderInterface(ProgressReporter* reporter, StopChecker* stopchecker, const int32_t& deviceId)
        : m_bHasDarkPixelMask(false)
        , m_pDarkpixel_mask(0)
        , m_pProgress(reporter)
        , m_pStopChecker(stopchecker)
        , m_bDeconvolution(false)
        , deviceId(deviceId) {
    }

    FluorescenceBuilderInterface::~FluorescenceBuilderInterface() {
        QMap<int, ImageSet*>::iterator itr = m_mapImageSet.begin();
        for (; itr != m_mapImageSet.end(); itr++) {
            delete itr.value();
        }

        if (m_pDarkpixel_mask) delete m_pDarkpixel_mask;
    }

    void FluorescenceBuilderInterface::release(void) {
        TC::release();
    }

    auto FluorescenceBuilderInterface::TransferData(std::any& data3d, std::any& dataMip, TC::MinMax& minMax3d,
        TC::MinMax& minMaxMip, TC::Resolution& resolution, TC::Sizes& sizes, const double& htSizeInum) -> void {
        const auto fPixelXY = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
        const auto fPixelZ = (m_parameter[PN_FL_SCANSTEP] == 0) ? 0.1 : m_parameter[PN_FL_SCANSTEP] * 1000;

        const auto flImagMap = m_mapImageSet[0];
        auto reconImg = af::reorder(flImagMap->arrData, 1, 0, 2).as(u16);

        if (htSizeInum > 0) {
            const auto flPixelLength = reconImg.dims(0);
            const auto flLengthInum = static_cast<double>(flPixelLength)* static_cast<double>(fPixelXY);

            if (htSizeInum < flLengthInum) {
                const auto fovdiff = flLengthInum - htSizeInum;
                auto ofs = static_cast<int32_t>(fovdiff / static_cast<double>(fPixelXY) / 2);
                ofs = ofs + (ofs % 2);

                const auto cropBeginPoint = static_cast<double>(ofs);
                const auto cropEndPoint = static_cast<double>(flPixelLength - static_cast<dim_t>(ofs) - 1);

                const auto boundary = af::seq(cropBeginPoint, cropEndPoint);

                reconImg = reconImg(boundary, boundary);
            }
        }

        const auto mipImg = GenerateMipImage(reconImg);

        const auto reconRawData3d = reconImg.host<uint16_t>();
        data3d = std::shared_ptr<uint16_t>(reconRawData3d, TC::AfHostDeleter());
        minMax3d.minValue = static_cast<double>(af::min<uint16_t>(reconImg));
        minMax3d.maxValue = static_cast<double>(af::max<uint16_t>(reconImg));

        const auto mipRawData = mipImg.host<uint16_t>();
        dataMip = std::shared_ptr<uint16_t>(mipRawData, TC::AfHostDeleter());
        minMaxMip.minValue = static_cast<double>(af::min<uint16_t>(mipImg));
        minMaxMip.maxValue = static_cast<double>(af::max<uint16_t>(mipImg));


        resolution.resolutionX = fPixelXY;
        resolution.resolutionY = fPixelXY;
        resolution.resolutionZ = fPixelZ;

        sizes.sizeX = static_cast<size_t>(reconImg.dims(1));
        sizes.sizeY = static_cast<size_t>(reconImg.dims(0));
        sizes.sizeZ = static_cast<size_t>(reconImg.dims(2));
    }

    bool FluorescenceBuilderInterface::SetParameter(const QString& strPath) {
        if (!LoadParameter(strPath, m_parameter)) return false;

        //set default values...
        for (int i = 0; i < 3; i++) {
            if (m_parameter.find(PN_FL_CAMERA_SHUTTER(i)) == m_parameter.end()) {
                m_parameter[PN_FL_CAMERA_SHUTTER(i)] = 0;
            }

            if (m_parameter.find(PN_FL_CAMERA_GAIN(i)) == m_parameter.end()) {
                m_parameter[PN_FL_CAMERA_GAIN(i)] = 0;
            }

            if (m_parameter.find(PN_FL_LIGHT_INTENSITY(i)) == m_parameter.end()) {
                m_parameter[PN_FL_LIGHT_INTENSITY(i])] = 0;
            }
        }

        if (m_parameter.find(PN_FL_SCANRANGE) == m_parameter.end()) {
            m_parameter[PN_FL_SCANRANGE] = 0;
        }

        if (m_parameter.find(PN_FL_SCANSTEP) == m_parameter.end()) {
            m_parameter[PN_FL_SCANSTEP] = 0;
        }

        if (m_parameter.find(PN_FL_SCANCENTER) == m_parameter.end()) {
            m_parameter[PN_FL_SCANCENTER] = 0;
        }

        if (m_parameter.find(PN_FL_ACQUISITION_INTERVAL_3D) == m_parameter.end()) {
            m_parameter[PN_FL_ACQUISITION_INTERVAL_3D] = 1.0;
        }

        if (m_parameter.find(PN_ACQUISITION_INTERVAL) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_INTERVAL] = -1;    //Old parameter
        }

        if (m_parameter.find(PN_ACQUISITION_COUNT) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_COUNT] = -1;    //Old parameter
        }

        if (m_parameter.find(PN_ACQUISITION_HTFL_ZOFFSET) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_HTFL_ZOFFSET] = 0;
        }

        if (m_parameter.find(PN_ACQUISITION_HTFL_ZOFFSET_COMPENSATION) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_HTFL_ZOFFSET_COMPENSATION] = 0;
        }

        if (m_parameter.find(PN_IMMERSION_RI) == m_parameter.end()) {
            m_parameter[PN_IMMERSION_RI] = 1.337;
        }

        //set deconvolution path
        m_strDeconvPath = strPath;
        m_strDeconvPath = m_strDeconvPath.replace("config.dat", "deconvolution.dat");

        return true;
    }

    bool FluorescenceBuilderInterface::LoadRawImages(const QString& strPath, const int32_t& channelIndex) {
        QStringList nameFilter(QString("ch%1").arg(channelIndex));
        QStringList chdirs = QDir(strPath).entryList(nameFilter, QDir::Dirs, QDir::Name);


        const int chidx = 0;

        m_mapImageSet[chidx] = new ImageSet();

        const QString strChPath = QString("%1/%2").arg(strPath).arg(chdirs[chidx]);
        const bool bRes = LoadImages(strChPath, "png", 0, false, m_mapImageSet[chidx]->arrData, m_mapImageSet[chidx]->nImages, false, true);	//as u8-type
        if (!bRes) {
            //there could be empty channel folder because Thumbs.db file exists...
            delete m_mapImageSet[chidx];
            m_mapImageSet.remove(chidx);
        } else {
            const int nImages = m_mapImageSet[chidx]->nImages;
            for (int i = 0; i < nImages; i++) {
                af::array image = m_mapImageSet[chidx]->arrData(af::span, af::span, i);
                compensateDarkpixel(image);
                m_mapImageSet[chidx]->arrData(af::span, af::span, i) = image(af::span, af::span);
            }
        }


        return true;
    }

    bool FluorescenceBuilderInterface::BuildTomogram(bool inclDeconv, bool isFirstFrameOfChannel, const int32_t& channelIndex) {
        if (!inclDeconv) {
            QLOG_INFO() << "Deconvolution is not applied by user";
            return true;
        }

        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();
        const QString strTemp = sysConfig->getTempPath();

        if (!TomoDeconvolutionActivator::IsUnlocked()) {
            QLOG_INFO() << "Deconvolution is not activated.";
            return true;
        }

        m_bDeconvolution = true;

        const int imgWidth = m_mapImageSet.first()->width();
        const int imgHeight = m_mapImageSet.first()->height();
        const int imgSlices = m_mapImageSet.first()->images();
        const float fPixelXY = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
        const float fPixelZ = (m_parameter[PN_FL_SCANSTEP] == 0) ? 0.1 : m_parameter[PN_FL_SCANSTEP] * 1000;

        //if (imgSlices < 2) return true;

        reportProgress(0);

        std::shared_ptr<DeconProgressReporter> deconProc(new DeconProgressReporter(this));
        TomoDeconvolution deconv(deconProc.get());

        if (!deconv.LoadParameters(m_strDeconvPath)) return false;
        if (!deconv.SetImageInfo(imgWidth, imgHeight, imgSlices, fPixelXY, fPixelXY, fPixelZ)) return false;

        const int channels = m_mapImageSet.size();

        TomoDeconvolutionConfig deconConfig(m_strDeconvPath);

        m_nCurrentProcIndex = 0;

        QMap<int, ImageSet*>::iterator itr = m_mapImageSet.begin() + m_nCurrentProcIndex;

        const int chidx = channelIndex;

        deconv.LoadParameters(deconConfig, chidx);    //reload channel specific parameters

        af::array& image = itr.value()->arrData;

        QString strInput = QString("%1\\deconv_src_%2.raw").arg(strTemp).arg(chidx);
        if (!deconv.SetSystemInfo(m_parameter[PN_NA], m_parameter[PN_IMMERSION_RI], EmissionWavelength(chidx))) return false;
        if (!deconv.SetSampleInfo(m_parameter[PN_NM])) return false;

        TC::exportRAW(image, strInput);

        if (!deconv.Process(strInput, strTemp, chidx, isFirstFrameOfChannel)) {
        	QLOG_ERROR() << "Deconvolution error = " << deconv.GetLastErrorMessage().toLocal8Bit().constData();
            return false;
        }
        af::setDevice(deviceId);
        if (!TC::importRAW32_16(image, deconv.GetOutputName(), imgWidth, imgHeight, imgSlices)) {
        	QLOG_ERROR() << "Deconvolution error = failed to get the result";
            return false;
        }

        reportProgress(100 * ((1.0 / channels) * (m_nCurrentProcIndex + 1.0)));

        if (isStopRequested()) return false;

        reportProgress(100);

        return true;
    }

    bool FluorescenceBuilderInterface::Save(const QString& savePath, TC::Position& pos, unsigned long long timestamp) {
        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        const int channels = m_mapImageSet.size();

        //common parameters
        saveVal("Channels", channels, pPath);
        saveVal("ScanRange", m_parameter[PN_FL_SCANRANGE], pPath, true);
        saveVal("ScanStep", m_parameter[PN_FL_SCANSTEP], pPath, true);
        saveVal("ScanCenter", m_parameter[PN_FL_SCANCENTER], pPath, true);
        saveVal("Interval", m_parameter[PN_FL_ACQUISITION_INTERVAL_3D], pPath, true);
        saveVal("timestamp", timestamp, pPath, true);
        saveVal("posx", pos.x, pPath, true);
        saveVal("posy", pos.y, pPath, true);
        saveVal("posz", pos.z, pPath, true);
        saveVal("posc", pos.c, pPath, true);
        saveVal("Deconvolution", m_bDeconvolution, pPath, true);

        QMap<int, ImageSet*>::iterator itr = m_mapImageSet.begin();
        for (int i = 0; itr != m_mapImageSet.end(); itr++, i++) {
            af::array& arrData = itr.value()->arrData;
            af::dtype dtype = arrData.type();

            float minv, maxv;
            if (dtype == u8) {
                minv = (float)af::min<uchar>(itr.value()->arrData);
                maxv = (float)af::max<uchar>(itr.value()->arrData);
            } else {
                minv = (float)af::min<ushort>(itr.value()->arrData);
                maxv = (float)af::max<ushort>(itr.value()->arrData);
            }

            int chidx = itr.key();
            char keyStr[32] = { 0, };

            sprintf_s<32>(keyStr, "Channel%d", i);
            saveVal(keyStr, chidx, pPath, true);

            sprintf_s<32>(keyStr, "Reconimg%d", chidx);
            af::saveArray(keyStr, itr.value()->arrData, pPath, true);

            sprintf_s<32>(keyStr, "Shutter%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_CAMERA_SHUTTER(chidx)], pPath, true);

            sprintf_s<32>(keyStr, "Gain%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_CAMERA_GAIN(chidx)], pPath, true);

            sprintf_s<32>(keyStr, "Intensity%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_LIGHT_INTENSITY(chidx)], pPath, true);

            sprintf_s<32>(keyStr, "EmissionWavelength%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_EMISSION_WAVELENGTH(chidx)], pPath, true);
        }

        af::sync();

        return true;
    }

    bool FluorescenceBuilderInterface::Load(const QString& strArrFile) {
        if (!QFile::exists(strArrFile)) {
            QLOG_ERROR() << "No file exists - " << QSTR2CSTR_CPTR(strArrFile);
            return false;
        }

        try {
            for (int chidx = 0; chidx < 3; chidx++) {
                char keyStr[32] = { 0, };

                sprintf_s<32>(keyStr, "Reconimg%d", chidx);
                int keyIdx = af::readArrayCheck(QSTR2CSTR_CPTR(strArrFile), keyStr);
                if (keyIdx == -1) continue;

                m_mapImageSet[keyIdx] = new ImageSet();
                m_mapImageSet[keyIdx]->arrData = af::readArray(QSTR2CSTR_CPTR(strArrFile), keyStr);
            }
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at FluorescenceBuilderInterface::Load  " << e.what() << "  path - " << QSTR2CSTR_CPTR(strArrFile);

            return false;
        }

        return true;
    }

    bool FluorescenceBuilderInterface::Load(const QStringList& strArrFileList, bool& bHasFLData) {
        bool bCommonInfoLoaded = false;
        bHasFLData = false;

        if (strArrFileList.size() == 0) return false;

        try {
            m_dataList_fltomogram.clear();
            m_dataList_fl_mip.clear();

            foreach(QString strPath, strArrFileList) {
                if (!QFile::exists(strPath)) {
                    m_dataList_fltomogram.clear();
                    m_dataList_fl_mip.clear();
                    return false;
                }

                if (strPath.indexOf("FLTomogram.arr") > -1) {
                    if (!bCommonInfoLoaded) {
                        //TOMV-325 load common information...
                        if (!Load(strPath)) return false;
                        bCommonInfoLoaded = true;
                    }

                    m_dataList_fltomogram.push_back(strPath);

                    //MIP
                    strPath.replace(QString("/FLTomogram"), QString("/FLMIP"));
                    if (QFile::exists(strPath)) {
                        m_dataList_fl_mip.push_back(strPath);
                    }
                }
            }

            bHasFLData = true;
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at FluorescenceBuilderInterface::Load " << e.what();

            return false;
        }

        if (!bCommonInfoLoaded) {
            //TOMV-325 load common information...
            if (!Load(strArrFileList.at(0))) return false;
        }

        return true;
    }

    auto FluorescenceBuilderInterface::GenerateMipImage(const af::array& reconimg) -> af::array {
        auto d0 = reconimg.dims(0);
        auto d1 = reconimg.dims(1);
        auto d2 = reconimg.dims(2);

        auto mipImage = af::constant(0, d0, d1, reconimg.type());

        if (d2 > 1) {
            for (auto zIdx = 0; zIdx<d2;++zIdx) {
                const auto zSlice = reconimg(af::span, af::span, zIdx);

                mipImage = af::max(mipImage, zSlice);

                mipImage.eval();
            }
        } else {
            mipImage(af::span, af::span) = reconimg(af::span, af::span);
        }

        mipImage.eval();

        return mipImage;
    }

    bool FluorescenceBuilderInterface::SaveMIPImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp) {
#undef max
        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        const int channels = m_mapImageSet.size();

        //common parameters
        saveVal("Channels", channels, pPath);
        saveVal("ScanRange", m_parameter[PN_FL_SCANRANGE], pPath, true);
        saveVal("ScanStep", m_parameter[PN_FL_SCANSTEP], pPath, true);
        saveVal("ScanCenter", m_parameter[PN_FL_SCANCENTER], pPath, true);
        saveVal("Interval", m_parameter[PN_FL_ACQUISITION_INTERVAL_3D], pPath, true);
        saveVal("timestamp", timestamp, pPath, true);
        saveVal("posx", pos.x, pPath, true);
        saveVal("posy", pos.y, pPath, true);
        saveVal("posz", pos.z, pPath, true);
        saveVal("posc", pos.c, pPath, true);
        saveVal("Deconvolution", m_bDeconvolution, pPath, true);

        QMap<int, ImageSet*>::iterator itr = m_mapImageSet.begin();
        for (int i = 0; itr != m_mapImageSet.end(); itr++, i++) {
            int chidx = itr.key();
            char keyStr[32] = { 0, };

            af::array& Reconimg = itr.value()->arrData;

            dim_t d0 = Reconimg.dims(0);
            dim_t d1 = Reconimg.dims(1);
            dim_t d2 = Reconimg.dims(2);

            af::array mip_image = af::constant(0, d0, d1);

            if (d2 > 1) {
                for (dim_t d0_idx = 0; d0_idx < d0; d0_idx++) {
                    gfor(af::seq d1_idx, d1) {
                        mip_image(d0_idx, d1_idx) = af::max(Reconimg(d0_idx, d1_idx, af::span));
                    }
                }
            } else {
                mip_image(af::span, af::span) = Reconimg(af::span, af::span);
            }

            mip_image.eval();

            sprintf_s<32>(keyStr, "Channel%d", i);
            saveVal(keyStr, chidx, pPath, true);

            sprintf_s<32>(keyStr, "Image%d", chidx);
            af::saveArray(keyStr, mip_image, pPath, true);

            sprintf_s<32>(keyStr, "Shutter%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_CAMERA_SHUTTER(chidx)], pPath, true);

            sprintf_s<32>(keyStr, "Gain%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_CAMERA_GAIN(chidx)], pPath, true);

            sprintf_s<32>(keyStr, "Intensity%d", chidx);
            saveVal(keyStr, m_parameter[PN_FL_LIGHT_INTENSITY(chidx)], pPath, true);
        }

        af::sync();

        return true;
    }

    QString FluorescenceBuilderInterface::GetChannelPath(const QString& strPath, int idx) const {
        QString strOutPath = "";

        QStringList nameFilter("ch*");
        QStringList chdirs = QDir(strPath).entryList(nameFilter, QDir::Dirs, QDir::Name);

        if (chdirs.size() > idx) {
            strOutPath = QString("%1/%2").arg(strPath).arg(chdirs.at(idx));
        }

        return strOutPath;
    }

    bool FluorescenceBuilderInterface::procDarkpixel(const QString& strImgPath, const QString& strOutPath) {
        try {
            af::array img;

            if (!LoadImage(strImgPath, img)) return false;

            af::array filtered = af::medfilt2(img, 3, 3, AF_PAD_ZERO);

            af::array diff = img - filtered;

            const float stdev = af::stdev<float>(diff);
            const float threshold = 6 * stdev;

            af::dim4 dim = img.dims();
            af::array mask = af::constant<float>(0, dim[0], dim[1]);

            mask(diff > threshold) = 1;

            //On some machine, af::saveArray fails to write file, so
            //it will create empty file here... !! BAD-SOLUTION !!
            for (int idx = 0; idx < 3; idx++) {
                try {
                    QFile file(strOutPath);
                    file.open(QIODevice::WriteOnly);
                    file.close();
                } catch (...) {
                    continue;
                }

                if (QFile(strOutPath).exists()) break;
            }

            char pOutPath[2048] = { 0, };
            strcpy_s<2048>(pOutPath, strOutPath.toLocal8Bit().constData());

            af::saveArray("image", img, pOutPath, false);
            af::saveArray("hotpixel", mask, pOutPath, true);
            af::saveArray("threshold", af::constant<float>(threshold, 1), pOutPath, true);

#if 0	//! codes for debugging......
            af::saveImage("d:/temp/test/hotpixel/source.png", img);
            af::saveImage("d:/temp/test/hotpixel/filtered.png", filtered);
            af::saveImage("d:/temp/test/hotpixel/diff.png", diff);

            af::array mask_img = af::constant<unsigned char>(0, dim[0], dim[1]);
            mask_img(mask > 0) = 255;
            af::saveImage("d:/temp/test/hotpixel/mask.png", mask_img);
#endif
        } catch (af::exception& ex) {
            QLOG_ERROR() << "Exception : " << ex.what();
            QLOG_ERROR() << "Darkpixel image path : " << strImgPath;
            QLOG_ERROR() << "Darkpixel calibration path : " << strOutPath;
            return false;
        }

        return true;
    }

    bool FluorescenceBuilderInterface::LoadDarkpixelCal(const QString& strPath) {
        //if there isn't darkpixel calibration file, it just returns true...
        if (!QFile::exists(strPath)) {
            QLOG_INFO() << "No darkpixel calibration data at " << strPath;
            return true;
        }

        char pPath[2048];
        strcpy_s<2048>(pPath, strPath.toLocal8Bit().constData());

        try {
            if (m_pDarkpixel_mask) delete m_pDarkpixel_mask;

            m_pDarkpixel_mask = new af::array(af::readArray(pPath, "hotpixel"));
            m_bHasDarkPixelMask = true;
        } catch (af::exception& ex) {
            QLOG_ERROR() << "Failed to read hotpixel data from " << strPath;
            QLOG_ERROR() << "Exception : " << ex.what();
            m_bHasDarkPixelMask = false;
            return false;
        }

        return true;
    }

    bool FluorescenceBuilderInterface::compensateDarkpixel(af::array& image) {
        static int idx = 0;

        if (!m_bHasDarkPixelMask) return false;

        af::array& mask = *m_pDarkpixel_mask;

        af::dim4 dims = image.dims();
        af::dim4 mask_dims = mask.dims();
        if (dims != mask_dims) {
            const int d01 = std::round((mask_dims[0] - dims[0]) / 2);
            const int d02 = d01 + dims[0] - 1;
            const int d11 = std::round((mask_dims[1] - dims[1]) / 2);
            const int d12 = d11 + dims[1] - 1;

            mask = mask(af::seq(d01, d02), af::seq(d11, d12));
        }

        af::array filtered = af::medfilt2(image, 3, 3, AF_PAD_ZERO);

        af::array id = (mask == 1);
        id.eval();

        image = id * filtered + (1 - id) * image;

        return true;
    }

    bool FluorescenceBuilderInterface::removeDarkPixel(const QString& strImgPath, const QString& strOutPath) {
        if (!QFile::exists(strImgPath)) {
            QLOG_ERROR() << "No image is at " << strImgPath;
            return false;
        }

        char pPath[2048];
        strcpy_s<2048>(pPath, strImgPath.toLocal8Bit().constData());

        af::array arrImage = af::loadImage(pPath, 0);
        if (arrImage.isempty()) {
            QLOG_ERROR() << "Image is empty : " << strImgPath;
            return false;
        }

        if (!compensateDarkpixel(arrImage)) return false;

        strcpy_s<2048>(pPath, strOutPath.toLocal8Bit().constData());

        try {
            af::saveImage(pPath, arrImage);
        } catch (af::exception& ex) {
            QLOG_ERROR() << "Image is not saved because " << ex.what();
        }

        return true;
    }

    float FluorescenceBuilderInterface::EmissionWavelength(int channel) {
        float default_vals[3] = { 0.461f, 0.509f, 0.610f };

        if (channel < 0 || channel>2) return 0;

        if (m_parameter.find(PN_FL_EMISSION_WAVELENGTH(channel)) == m_parameter.end()) {
            return default_vals[channel];
        }

        return m_parameter[PN_FL_EMISSION_WAVELENGTH(channel)] / 1000.f;	//! nm to um
    }

    void FluorescenceBuilderInterface::reportDeconvProgress(int deconvProc) {
        const int channels = m_mapImageSet.size();

        reportProgress(100 * ((1.0 / channels) * (m_nCurrentProcIndex + 1.0) * (deconvProc / 100.0)));
    }

    void FluorescenceBuilderInterface::reportProgress(int progress) {
        m_pProgress->notify(progress);
    }
}