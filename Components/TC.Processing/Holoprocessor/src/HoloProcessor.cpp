#include <QMutex>
#include <QMutexLocker>
#include <QDir>
#include <QFile>
#include <QTextStream>

#include <iostream>

#include "HoloProcessor.h"
#include "HoloProcessorArrayFire.h"

#include "TomoDeconvolution.h"

HoloProcessor::HoloProcessor(ProgressReporter* reporter, QCoreApplication* pApplication, int nDebugLevel)
    : m_pApplication(pApplication)
    , m_nDebugLevel(nDebugLevel)
    , m_strUser("N/A")
    , m_strTitle("")
    , m_strDesc("")
    , m_bRunning(false)
    , m_pReporter(reporter)
    , m_nDeviceIndex(0) {
}

HoloProcessor::~HoloProcessor() {

}

std::shared_ptr<HoloProcessor> HoloProcessor::createInstance(ProcessorType type, ProgressReporter* reporter, QCoreApplication* pApplication, int nDebugLevel) {
    static QMutex mutex;
    static std::shared_ptr<HoloProcessor> theInstance;

    QMutexLocker mutexLocker(&mutex);

    if (theInstance.get()) return theInstance;

    if (type == ARRAYFIRE) theInstance = HoloProcessorArrayFire::create(reporter, pApplication, nDebugLevel);

    return theInstance;
}

void HoloProcessor::start(void) {
    QMutexLocker locker(&m_mutex);
    m_bRunning = true;
}

void HoloProcessor::stop(void) {
    QMutexLocker locker(&m_mutex);
    m_bRunning = false;
}

void HoloProcessor::setSysInformation(const QString& /*strDevice*/, const QString& strSWVer, const QString& strUser) {
    m_strSWVer = strSWVer;
    m_strUser = strUser;
}

void HoloProcessor::setFileInformation(const QString& strTitle, const QString& strDesc) {
    m_strTitle = strTitle;
    m_strDesc = strDesc;
}

void HoloProcessor::reportProgress(int value) {
    m_pReporter->notify(value);
}