#pragma once

#include <QString>

class TomoDeconvolutionConfig {
public:
    typedef enum {
        ORIGINAL_DATA = 0,
        FILTERED_ORIGINAL_DATA,
        FLAT_SHEET_IMAGE,
        USER_INPUT_FIRST_GUESS
    } ImageFirstGuess;

    typedef enum {
        THEORETICAL_ESTIMATE = 0,
        FLAT_SHEET_PSF,
        AUTOCORRECTION,
        USER_INPUT_PSF
    } PSFFirstGuess;

    typedef enum {
        AUTOMATIC = 0,
        THEORETICAL_LIMIT,
        DETECTED_LIMIT
    } FrequencyConstraint;

    typedef enum {
        STATIC = 0,
        DYNAMIC
    } SubvolumeCalculation;

    typedef enum {
        AUTOQUANT = 0,
        GIBSON_LANNI,
        ZERNIKE
    } PSFGenerationAlgorithm;

    typedef enum {
        EXPECTATION_MAXIMIZATION = 0,
        POWER_ACCELERATION,
        EXTRAPOLATION_ACCELERATION,
        GOLDS_METHOD
    } DeconvolutionMethod;

    typedef enum {
        AUTOMATIC_CALCULATION = 0,
        MANUAL_INPUT
    } DarkCurrentCalculation;

public:
    TomoDeconvolutionConfig();
    TomoDeconvolutionConfig(const QString& strPath);
    ~TomoDeconvolutionConfig();

    bool Save(const QString& strPath);
    bool Load(const QString& strPath);

    void ClearOptions(void);

protected:
    void Init(void);

    QString toString(ImageFirstGuess value);
    ImageFirstGuess ConvImageFirstGuess(const QString& strVal);

    QString toString(PSFFirstGuess value);
    PSFFirstGuess ConvPSFFirstGuess(const QString& strVal);

    QString toString(FrequencyConstraint value);
    FrequencyConstraint ConvFrequencyConstraint(const QString& strVal);

    QString toString(SubvolumeCalculation value);
    SubvolumeCalculation ConvSubvolumeCalculation(const QString& strVal);

    QString toString(PSFGenerationAlgorithm value);
    PSFGenerationAlgorithm ConvPSFGenerationAlgorithm(const QString& strVal);

    QString toString(DeconvolutionMethod value);
    DeconvolutionMethod ConvDeconvolutionMethod(const QString& strVal);

    QString toString(DarkCurrentCalculation value);
    DarkCurrentCalculation ConvDarkCurrentCalculation(const QString& strVal);

    QString toString(float value, int precision = 6);

public:
    //Sample Information
    float m_fSampleDepth;                    // Sample depth from coverslip (um)

    // Standard algorithm settings
    DeconvolutionMethod		m_nDeconvolutionMethod;			// Deconvolution method to use
    DarkCurrentCalculation	m_nDarkCurrentCalculation[3];	// Darkcurrent calculation method to use

    float m_fDarkCurrent[3];				// Average intensity value of the dataset��s background
    int m_nNumberOfIteration;				// Number of iterations through which to process the image
    int m_nBinFactorXY;						// Binning factor to apply to the XY plane
    int m_nBinFactorZ;						// Binning factor to apply to the Z plane
    int m_nSaveInterval;					// Iteration interval at which to save processing "so far"

    // Expert algorithm settings
    ImageFirstGuess			m_nImageFirstGuess;			// Image initial guess calculation method
    PSFFirstGuess			m_nPSFFirstGuess;			// PSF initial guess calculation method
    FrequencyConstraint		m_nFrequencyConstraint;		// Frequency bandlimit calculation method
    SubvolumeCalculation	m_nSubvolumeCalculation;	// Subvolume size calculation method
    PSFGenerationAlgorithm	m_nPSFGenerationAlgorithm;	// Algorithm used for generation of the theoretical PSF

    int m_nGuardbandXY;						// Padding size to add to XY image border (in pixels)
    int m_nGuardbandZ;						// Padding size to add to Z image border (in pixels)
    float m_fPSFWaistRadius;				// Radius (in pixels) of initial PSF hourglass "waist"
    float m_fPSFStretchFactor;				// Stretch factor to apply to calculated PSF
    int m_nSubvolumeOverlap;				// Number of common pixels between adjacent subvolumes
    float m_fNoiseSmoothingFactor;			// Value for noise suppression factor (2=low, 20=medium, 100=high)
    int m_nGoldsGaussInterval;				// Iteration interval for applying smoothing with Golds Method
    float m_fGoldsGaussFWHM;				// FWHM of Gaussian applied with Golds Method

    bool m_bGPUProcessing;
    bool m_bSubvolumeInXY;					// Toggles subvolume decomposition in XY plane
    bool m_bSubvolumeInZ;					// Toggles subvolume decomposition in Z dimension
    bool m_bPerformIntensityCorrection;		// Flicker/attenuation correction
    bool m_bEnablePSFConstraints;			// Toggles use of theoretical constraints on PSF
    bool m_bEnableClassicConfocal;			// Determines whether to use "classic" confocal algorithm
};
