#pragma once

class StopChecker {
public:
    StopChecker() {}
    ~StopChecker() {}

    virtual bool isStopRequested(void) = 0;
};

class NoStopChecker : public StopChecker {
public:
    bool isStopRequested(void) override { return false; }
};
