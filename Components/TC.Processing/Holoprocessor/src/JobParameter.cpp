#define LOGGER_TAG "TCHoloprocessor::JobParameter"
#include <TCLogger.h>

#include <cmath>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QSettings>

#include <memory>

#include "JobParameter.h"
#include "SystemParameter.h"
#include "SystemModelFactory.h"

JobParameter::JobParameter(QObject* parent)
    : QObject(parent)
    , m_strLoadedPath("")
    , m_strJobName("")
    , m_strAnnotationTitle("")
    , m_strUserDefinedTitle("NA")
    , m_bValid(false) {
    m_nFieldOfViewHPixel = 0;
    m_nFieldOfViewVPixel = 0;

    odt.m_strMedium = "PBS";
    odt.m_dMediumRI = 1.337;
    odt.m_dImmersionRI = 1.337;
    odt.m_nIteration = 100;
    odt.m_dCameraShutter = 0.4;
    odt.m_dCameraGain = 0.0;
    odt.m_bDisableZLimit = false;
    odt.m_nIgnorePoor = 1;

    wf.m_dCameraShutter = 0.2;
    wf.m_dIntesnity = 50;

    for (int i = 0; i < 3; i++) {
        ft[i].m_dCameraShutter = 1.0;
        ft[i].m_dGain = 1.0;
        ft[i].m_dIntensity = 50;
        ft[i].m_bEnable = false;
    }

    m_dFLScanRange = 20;
    m_dFLScanStep = 1;
    m_nFLScanSteps = 1;
    m_dFLScanCenter = 1.0;

    updateFromModel();

    m_dtCreated = QDateTime::currentDateTime();
}

JobParameter::JobParameter(const QString& strPath, QObject* parent)
    : QObject(parent)
    , m_strLoadedPath("")
    , m_strJobName("") {
    Load(strPath);
}

JobParameter::JobParameter(const JobParameter& other) {
    *this = other;
}

JobParameter::~JobParameter() {

}

void JobParameter::updateFromModel(void) {
    SystemParameter::Pointer sysParam = SystemParameter::GetInstance();
    SystemModelInterface::Pointer pModel = SystemModelFactory::Model(sysParam->getModel());

    if (pModel.get()) {
        IterationCount(pModel->NumberOfIteration());

        FLScanStep(pModel->FluorescenceScanStep());
        FLScanRange(pModel->FluorescenceScanDepth());
    }
}

void JobParameter::Save() {
    if (m_strLoadedPath == "") return;

    Save(m_strLoadedPath);
}

void JobParameter::Save(const QString& strPath) {
    QFileInfo fileInfo(strPath);
    m_strJobName = fileInfo.baseName();

    std::shared_ptr<QSettings> pSettings(new QSettings(strPath, QSettings::IniFormat));

    m_dtUpdated = QDateTime::currentDateTime();

    pSettings->beginGroup("Job parameter");
    {
        pSettings->setValue(TAG_CREATED_DATETIME, m_dtCreated);
        pSettings->setValue(TAG_UPDATED_DATETIME, m_dtUpdated);
        pSettings->setValue(TAG_JOB_TITLE, m_strJobName);
        pSettings->setValue(TAG_ANNOTATION_TITLE, m_strAnnotationTitle);
        pSettings->setValue(TAG_USERDEFINED_TITLE, m_strUserDefinedTitle);
        pSettings->setValue(TAG_FIELDOFVIEW_H, m_dFieldOfViewH);
        pSettings->setValue(TAG_FIELDOFVIEW_V, m_dFieldOfViewV);
        pSettings->setValue(TAG_FIELDOFVIEW_H_PX, m_nFieldOfViewHPixel);
        pSettings->setValue(TAG_FIELDOFVIEW_V_PX, m_nFieldOfViewVPixel);
        pSettings->setValue(TAG_MEDIUM_NAME, odt.m_strMedium);
        pSettings->setValue(TAG_MEDIUM_RI, odt.m_dMediumRI);
        pSettings->setValue(TAG_IMMERSION_RI, odt.m_dImmersionRI);
        pSettings->setValue(TAG_CAMERA_SHUTTER, odt.m_dCameraShutter);
        pSettings->setValue(TAG_CAMERA_GAIN, odt.m_dCameraGain);
        pSettings->setValue(TAG_ACQUISITION_QUALITY, 1);
        pSettings->setValue(TAG_ACQUISITION_TIME_2D, m_dAcquisitionTime2D);
        pSettings->setValue(TAG_ACQUISITION_TIME_3D, m_dAcquisitionTime3D);
        pSettings->setValue(TAG_ACQUISITION_INTERVAL_2D, m_dAcquisitionInterval2D);
        pSettings->setValue(TAG_ACQUISITION_INTERVAL_3D, m_dAcquisitionInterval3D);
        pSettings->setValue(TAG_ITERATION, odt.m_nIteration);
        pSettings->setValue(TAG_DISABLE_ZLIMIT, odt.m_bDisableZLimit);
        pSettings->setValue(TAG_IGNORE_POOR, odt.m_nIgnorePoor);

        //Widefield
        pSettings->setValue(TAG_BF_CAMERA_SHUTTER, wf.m_dCameraShutter);
        pSettings->setValue(TAG_BF_LIGHT_INTENSITY, wf.m_dIntesnity);

        //Fluorescence
        pSettings->setValue(TAG_FL_CAMERA_SHUTTER(0), ft[0].m_dCameraShutter);
        pSettings->setValue(TAG_FL_CAMERA_GAIN(0), ft[0].m_dGain);
        pSettings->setValue(TAG_FL_LIGHT_INTENSITY(0), ft[0].m_dIntensity);
        pSettings->setValue(TAG_FL_ENABLE(0), ft[0].m_bEnable);
        pSettings->setValue(TAG_FL_FLUOROPHORE_NAME(0), ft[0].m_strFluorophore);
        pSettings->setValue(TAG_FL_FLUOROPHORE_EMISSION(0), ft[0].m_nEmission);

        pSettings->setValue(TAG_FL_CAMERA_SHUTTER(1), ft[1].m_dCameraShutter);
        pSettings->setValue(TAG_FL_CAMERA_GAIN(1), ft[1].m_dGain);
        pSettings->setValue(TAG_FL_LIGHT_INTENSITY(1), ft[1].m_dIntensity);
        pSettings->setValue(TAG_FL_ENABLE(1), ft[1].m_bEnable);
        pSettings->setValue(TAG_FL_FLUOROPHORE_NAME(1), ft[1].m_strFluorophore);
        pSettings->setValue(TAG_FL_FLUOROPHORE_EMISSION(1), ft[1].m_nEmission);

        pSettings->setValue(TAG_FL_CAMERA_SHUTTER(2), ft[2].m_dCameraShutter);
        pSettings->setValue(TAG_FL_CAMERA_GAIN(2), ft[2].m_dGain);
        pSettings->setValue(TAG_FL_LIGHT_INTENSITY(2), ft[2].m_dIntensity);
        pSettings->setValue(TAG_FL_ENABLE(2), ft[2].m_bEnable);
        pSettings->setValue(TAG_FL_FLUOROPHORE_NAME(2), ft[2].m_strFluorophore);
        pSettings->setValue(TAG_FL_FLUOROPHORE_EMISSION(2), ft[2].m_nEmission);

        pSettings->setValue(TAG_FL_SCANSTEP, m_dFLScanStep);
        pSettings->setValue(TAG_FL_SCANSTEPS, m_nFLScanSteps);
        pSettings->setValue(TAG_FL_SCANRANGE, m_dFLScanRange);
        pSettings->setValue(TAG_FL_SCANCENTER, m_dFLScanCenter);

        pSettings->setValue(TAG_FL_ACQUISITION_TIME_3D, m_dFLAcquisitionTime3D);
        pSettings->setValue(TAG_FL_ACQUISITION_INTERVAL_3D, m_dFLAcquisitionInterval3D);
    }
    pSettings->endGroup();

    m_strLoadedPath = strPath;
}

bool JobParameter::Load(const QString& strPath) {
    if (!QFile::exists(strPath)) return false;

    m_strLoadedPath = strPath;

    std::shared_ptr<QSettings> pSettings(new QSettings(strPath, QSettings::IniFormat));

    pSettings->beginGroup("Job parameter");
    {
        m_dtCreated = pSettings->value(TAG_CREATED_DATETIME, QDateTime::currentDateTime()).toDateTime();
        m_dtUpdated = pSettings->value(TAG_UPDATED_DATETIME, QDateTime::currentDateTime()).toDateTime();
        m_strJobName = pSettings->value(TAG_JOB_TITLE, "Legacy").toString();
        m_strAnnotationTitle = pSettings->value(TAG_ANNOTATION_TITLE, "Default").toString();
        m_strUserDefinedTitle = pSettings->value(TAG_USERDEFINED_TITLE, "NA").toString();
        m_dFieldOfViewH = pSettings->value(TAG_FIELDOFVIEW_H, 100.0).toDouble();
        m_dFieldOfViewV = pSettings->value(TAG_FIELDOFVIEW_V, 100.0).toDouble();

        //Old job parameter does not include these parameters... TOMV-819
        if (pSettings->contains(TAG_FIELDOFVIEW_H_PX)) {
            m_nFieldOfViewHPixel = pSettings->value(TAG_FIELDOFVIEW_H_PX, 480).toInt();
            m_nFieldOfViewVPixel = pSettings->value(TAG_FIELDOFVIEW_V_PX, 480).toInt();
        } else {
            fieldOfView(m_dFieldOfViewH, m_dFieldOfViewV);
        }

        odt.m_strMedium = pSettings->value(TAG_MEDIUM_NAME, "PBS").toString();
        odt.m_dMediumRI = pSettings->value(TAG_MEDIUM_RI, 1.337).toDouble();
        odt.m_dImmersionRI = pSettings->value(TAG_IMMERSION_RI, 1.337).toDouble();
        odt.m_dCameraShutter = pSettings->value(TAG_CAMERA_SHUTTER, 0.400).toDouble();
        odt.m_dCameraGain = pSettings->value(TAG_CAMERA_GAIN, 0.0).toDouble();
        odt.m_nAcquisitionQuality = pSettings->value(TAG_ACQUISITION_QUALITY, 1).toInt();
        m_dAcquisitionTime2D = pSettings->value(TAG_ACQUISITION_TIME_2D, 10).toDouble();
        m_dAcquisitionTime3D = pSettings->value(TAG_ACQUISITION_TIME_3D, 10).toDouble();
        m_dAcquisitionInterval2D = pSettings->value(TAG_ACQUISITION_INTERVAL_2D, 2.0).toDouble();
        m_dAcquisitionInterval3D = pSettings->value(TAG_ACQUISITION_INTERVAL_3D, 2.0).toDouble();
        odt.m_nIteration = pSettings->value(TAG_ITERATION, 100).toInt();
        odt.m_bDisableZLimit = pSettings->value(TAG_DISABLE_ZLIMIT, false).toBool();
        odt.m_nIgnorePoor = pSettings->value(TAG_IGNORE_POOR, false).toBool();

        //Widefield
        wf.m_dCameraShutter = pSettings->value(TAG_BF_CAMERA_SHUTTER, 0.2).toDouble();
        wf.m_dIntesnity = pSettings->value(TAG_BF_LIGHT_INTENSITY, 50).toDouble();

        //Fluorescence
        ft[0].m_dCameraShutter = pSettings->value(TAG_FL_CAMERA_SHUTTER(0), 1).toDouble();
        ft[0].m_dGain = pSettings->value(TAG_FL_CAMERA_GAIN(0), 1).toDouble();
        ft[0].m_dIntensity = pSettings->value(TAG_FL_LIGHT_INTENSITY(0), 50).toDouble();
        ft[0].m_bEnable = pSettings->value(TAG_FL_ENABLE(0), false).toBool();
        ft[0].m_strFluorophore = pSettings->value(TAG_FL_FLUOROPHORE_NAME(0), "DAPI").toString();
        ft[0].m_nEmission = pSettings->value(TAG_FL_FLUOROPHORE_EMISSION(0), 461).toInt();

        ft[1].m_dCameraShutter = pSettings->value(TAG_FL_CAMERA_SHUTTER(1), 1).toDouble();
        ft[1].m_dGain = pSettings->value(TAG_FL_CAMERA_GAIN(1), 1).toDouble();
        ft[1].m_dIntensity = pSettings->value(TAG_FL_LIGHT_INTENSITY(1), 50).toDouble();
        ft[1].m_bEnable = pSettings->value(TAG_FL_ENABLE(1), false).toBool();
        ft[1].m_strFluorophore = pSettings->value(TAG_FL_FLUOROPHORE_NAME(1), "GFP").toString();
        ft[1].m_nEmission = pSettings->value(TAG_FL_FLUOROPHORE_EMISSION(1), 509).toInt();

        ft[2].m_dCameraShutter = pSettings->value(TAG_FL_CAMERA_SHUTTER(2), 1).toDouble();
        ft[2].m_dGain = pSettings->value(TAG_FL_CAMERA_GAIN(2), 1).toDouble();
        ft[2].m_dIntensity = pSettings->value(TAG_FL_LIGHT_INTENSITY(2), 50).toDouble();
        ft[2].m_bEnable = pSettings->value(TAG_FL_ENABLE(2), false).toBool();
        ft[2].m_strFluorophore = pSettings->value(TAG_FL_FLUOROPHORE_NAME(2), "mCherry").toString();
        ft[2].m_nEmission = pSettings->value(TAG_FL_FLUOROPHORE_EMISSION(2), 610).toInt();

        m_dFLScanRange = pSettings->value(TAG_FL_SCANRANGE, 1).toDouble();
        m_dFLScanStep = pSettings->value(TAG_FL_SCANSTEP, 1).toDouble();
        m_nFLScanSteps = pSettings->value(TAG_FL_SCANSTEPS, 1).toInt();
        m_dFLScanCenter = pSettings->value(TAG_FL_SCANCENTER, 1).toDouble();

        m_dFLAcquisitionTime3D = pSettings->value(TAG_FL_ACQUISITION_TIME_3D, 10).toDouble();
        m_dFLAcquisitionInterval3D = pSettings->value(TAG_FL_ACQUISITION_INTERVAL_3D, 5.0).toDouble();

        //for preventing exception....
        if (m_dAcquisitionTime2D == 0) m_dAcquisitionTime2D = 1;
        if (m_dAcquisitionTime3D == 0) m_dAcquisitionTime3D = 1;
        if (m_dFLAcquisitionTime3D == 0) m_dFLAcquisitionTime3D = 1;
        if (m_dAcquisitionInterval2D == 0) m_dAcquisitionInterval2D = 1;
        if (m_dAcquisitionInterval3D == 0) m_dAcquisitionInterval3D = 1;
        if (m_dFLAcquisitionInterval3D == 0) m_dFLAcquisitionInterval3D = 1;

        QFileInfo fileInfo(strPath);
        m_strJobName = fileInfo.baseName();
    }
    pSettings->endGroup();

    m_bValid = true;

    return true;
}

void JobParameter::operator=(const JobParameter& other) {
    m_strJobName = other.m_strJobName;
    m_dFieldOfViewH = other.m_dFieldOfViewH;
    m_dFieldOfViewV = other.m_dFieldOfViewV;
    m_nFieldOfViewHPixel = other.m_nFieldOfViewHPixel;
    m_nFieldOfViewVPixel = other.m_nFieldOfViewVPixel;
    odt.m_strMedium = other.odt.m_strMedium;
    odt.m_dMediumRI = other.odt.m_dMediumRI;
    odt.m_dImmersionRI = other.odt.m_dImmersionRI;
    odt.m_dCameraShutter = other.odt.m_dCameraShutter;
    odt.m_dCameraGain = other.odt.m_dCameraGain;
    odt.m_nAcquisitionQuality = other.odt.m_nAcquisitionQuality;
    m_dAcquisitionTime2D = other.m_dAcquisitionTime2D;
    m_dAcquisitionTime3D = other.m_dAcquisitionTime3D;
    m_dAcquisitionInterval2D = other.m_dAcquisitionInterval2D;
    m_dAcquisitionInterval3D = other.m_dAcquisitionInterval3D;
    odt.m_nIteration = other.odt.m_nIteration;
    odt.m_bDisableZLimit = other.odt.m_bDisableZLimit;
    odt.m_nIgnorePoor = other.odt.m_nIgnorePoor;
    m_strAnnotationTitle = other.m_strAnnotationTitle;
    m_strUserDefinedTitle = other.m_strUserDefinedTitle;

    //Widefield
    wf.m_dCameraShutter = other.wf.m_dCameraShutter;
    wf.m_dIntesnity = other.wf.m_dIntesnity;

    //Fluorscence
    for (int i = 0; i < 3; i++) {
        ft[i].m_dCameraShutter = other.ft[i].m_dCameraShutter;
        ft[i].m_dGain = other.ft[i].m_dGain;
        ft[i].m_dIntensity = other.ft[i].m_dIntensity;
        ft[i].m_bEnable = other.ft[i].m_bEnable;
        ft[i].m_strFluorophore = other.ft[i].m_strFluorophore;
        ft[i].m_nEmission = other.ft[i].m_nEmission;
    }

    m_dFLScanRange = other.m_dFLScanRange;
    m_dFLScanStep = other.m_dFLScanStep;
    m_nFLScanSteps = other.m_nFLScanSteps;
    m_dFLScanCenter = other.m_dFLScanCenter;

    m_dFLAcquisitionTime3D = other.m_dFLAcquisitionTime3D;
    m_dFLAcquisitionInterval3D = other.m_dFLAcquisitionInterval3D;

    m_dtCreated = other.m_dtCreated;
    m_dtUpdated = other.m_dtUpdated;

    //not a parameter.......	
    m_strLoadedPath = other.m_strLoadedPath;
    m_bValid = other.m_bValid;
}

bool JobParameter::operator==(const JobParameter& param) {
    if (m_dFieldOfViewH != param.m_dFieldOfViewH) return false;
    if (m_dFieldOfViewV != param.m_dFieldOfViewV) return false;
    if (m_nFieldOfViewHPixel != param.m_nFieldOfViewHPixel) return false;
    if (m_nFieldOfViewVPixel != param.m_nFieldOfViewVPixel) return false;
    if (odt.m_strMedium != param.odt.m_strMedium) return false;
    if (odt.m_dMediumRI != param.odt.m_dMediumRI) return false;
    if (odt.m_dImmersionRI != param.odt.m_dImmersionRI) return false;
    if (odt.m_dCameraShutter != param.odt.m_dCameraShutter) return false;
    if (odt.m_dCameraGain != param.odt.m_dCameraGain) return false;
    if (odt.m_nAcquisitionQuality != param.odt.m_nAcquisitionQuality) return false;
    if (m_dAcquisitionTime2D != param.m_dAcquisitionTime2D) return false;
    if (m_dAcquisitionTime3D != param.m_dAcquisitionTime3D) return false;
    if (m_dAcquisitionInterval2D != param.m_dAcquisitionInterval2D) return false;
    if (m_dAcquisitionInterval3D != param.m_dAcquisitionInterval3D) return false;
    if (odt.m_nIteration != param.odt.m_nIteration) return false;
    if (odt.m_bDisableZLimit != param.odt.m_bDisableZLimit) return false;
    if (odt.m_nIgnorePoor != param.odt.m_nIgnorePoor) return false;
    if (m_strAnnotationTitle != param.m_strAnnotationTitle) return false;
    //if (m_strUserDefinedTitle != param.m_strUserDefinedTitle) return false;  //! not necessary...

    //Widefield
    if (wf.m_dCameraShutter != param.wf.m_dCameraShutter) return false;
    if (wf.m_dIntesnity != param.wf.m_dIntesnity) return false;

    //Fluorescence
    for (int i = 0; i < 3; i++) {
        if (ft[i].m_dCameraShutter != param.ft[i].m_dCameraShutter) return false;
        if (ft[i].m_dGain != param.ft[i].m_dGain) return false;
        if (ft[i].m_dIntensity != param.ft[i].m_dIntensity) return false;
        if (ft[i].m_bEnable != param.ft[i].m_bEnable) return false;
        if (ft[i].m_strFluorophore != param.ft[i].m_strFluorophore) return false;
        if (ft[i].m_nEmission != param.ft[i].m_nEmission) return false;
    }

    if (m_dFLScanRange != param.m_dFLScanRange) return false;
    if (m_dFLScanStep != param.m_dFLScanStep) return false;
    if (m_nFLScanSteps != param.m_nFLScanSteps) return false;
    if (m_dFLScanCenter != param.m_dFLScanCenter) return false;

    if (m_dFLAcquisitionTime3D != param.m_dFLAcquisitionTime3D) return false;
    if (m_dFLAcquisitionInterval3D != param.m_dFLAcquisitionInterval3D) return false;

    return true;
}

QString JobParameter::name(void) const {
    return m_strJobName;
}

QString JobParameter::title(void) const {
    return m_strUserDefinedTitle;
}
void JobParameter::title(const QString& strTitle) {
    m_strUserDefinedTitle = strTitle;
}

QString JobParameter::annotationTitle(void) const {
    return m_strAnnotationTitle;
}

void JobParameter::annotationTitle(const QString& strTitle) {
    m_strAnnotationTitle = strTitle;
}

double JobParameter::fieldOfViewH(void) const {
    return m_dFieldOfViewH;
}

double JobParameter::fieldOfViewV(void) const {
    return m_dFieldOfViewV;
}

int JobParameter::fieldOfViewHPixels(void) const {
    return m_nFieldOfViewHPixel;
}

int JobParameter::fieldOfViewVPixels(void) const {
    return m_nFieldOfViewVPixel;
}

void JobParameter::fieldOfView(const double dFOVH, const double dFOVV) {
    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();

    const double maxFOVH = sysparam->maxFOVH();
    const double maxFOVV = sysparam->maxFOVV();

    m_dFieldOfViewH = std::min(dFOVH, maxFOVH);
    m_dFieldOfViewV = std::min(dFOVV, maxFOVV);

    const double pixelSizeH = sysparam->m_camera.pixelSizeH();
    const double pixelSizeV = sysparam->m_camera.pixelSizeV();
    const double sysMagnification = sysparam->sysMagnification();

    m_nFieldOfViewHPixel = (m_dFieldOfViewH * sysMagnification / pixelSizeH);
    m_nFieldOfViewVPixel = (m_dFieldOfViewV * sysMagnification / pixelSizeV);
}

void JobParameter::fieldOfViewPixels(const int nFOVH, const int nFOVV) {
    m_nFieldOfViewHPixel = nFOVH;
    m_nFieldOfViewVPixel = nFOVV;

    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();

    const double pixelSizeH = sysparam->m_camera.pixelSizeH();
    const double pixelSizeV = sysparam->m_camera.pixelSizeV();
    const double sysMagnification = sysparam->sysMagnification();

    const double dFOVH = (nFOVH * pixelSizeH / sysMagnification);
    const double dFOVV = (nFOVV * pixelSizeV / sysMagnification);

    const double maxFOVH = sysparam->maxFOVH();
    const double maxFOVV = sysparam->maxFOVV();

    m_dFieldOfViewH = std::min(dFOVH, maxFOVH);
    m_dFieldOfViewV = std::min(dFOVV, maxFOVV);

    QLOG_INFO() << "Set FOV = (" << nFOVH << ", " << nFOVV << ")px (" << m_dFieldOfViewH << ", " << m_dFieldOfViewV << ")um";
}

int JobParameter::pixelsH(void) const {
    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();
    const int maxPixels = sysparam->m_camera.maxPixelsH();

    return std::min(maxPixels, m_nFieldOfViewHPixel);
}

int JobParameter::pixelsV(void) const {
    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();
    const int maxPixels = sysparam->m_camera.maxPixelsV();

    return std::min(maxPixels, m_nFieldOfViewVPixel);
}

QString JobParameter::mediumName(void) const {
    return odt.m_strMedium;
}

void JobParameter::mediumName(const QString& strName) {
    odt.m_strMedium = strName;
}

double JobParameter::mediumRI(void) const {
    return odt.m_dMediumRI;
}

double JobParameter::immersionRI(void) const {
    return odt.m_dImmersionRI;
}

void JobParameter::immersionRI(const double dRI) {
    odt.m_dImmersionRI = dRI;
}

void JobParameter::mediumRI(const double dRI) {
    odt.m_dMediumRI = dRI;
}

double JobParameter::cameraShutter(void) const {
    return odt.m_dCameraShutter;
}

void JobParameter::cameraShutter(const double dValue) {
    odt.m_dCameraShutter = dValue;
}

double JobParameter::cameraGain(void) const {
    return odt.m_dCameraGain;
}

void JobParameter::cameraGain(const double dValue) {
    odt.m_dCameraGain = dValue;
}

double JobParameter::BFCameraShutter(void) const {
    return wf.m_dCameraShutter;
}

void JobParameter::BFCameraShutter(const double dValue) {
    wf.m_dCameraShutter = dValue;
}

double JobParameter::BFLightIntensity(void) const {
    return wf.m_dIntesnity;
}

void JobParameter::BFLightIntensity(const double dValue) {
    wf.m_dIntesnity = dValue;
}

int JobParameter::BFPixelsH(void) const {
    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();
    const double pixelSizeH = sysparam->m_camera_bf.pixelSizeH();
    const double sysMagnification = sysparam->bfMagnification();

    return (m_dFieldOfViewH * sysMagnification / pixelSizeH);
}

int JobParameter::BFPixelsV(void) const {
    SystemParameter::Pointer sysparam = SystemParameter::GetInstance();
    const double pixelSizeV = sysparam->m_camera_bf.pixelSizeH();
    const double sysMagnification = sysparam->bfMagnification();

    return (m_dFieldOfViewV * sysMagnification / pixelSizeV);
}

int JobParameter::acquisitionQuality(void) const {
    return odt.m_nAcquisitionQuality;
}

void JobParameter::acquisitionQuality(const int nQuality) {
    odt.m_nAcquisitionQuality = nQuality;
}

int JobParameter::acqusitionFrames(void) const {
    return 96;
}

int JobParameter::reconstructionMethod(void) {
    return 1;
}

double JobParameter::acquisitionTime2D(void) const {
    return m_dAcquisitionTime2D;
}

void JobParameter::acquisitionTime2D(const double dSecond) {
    m_dAcquisitionTime2D = dSecond;
}

double JobParameter::acquisitionTime3D(void) const {
    return m_dAcquisitionTime3D;
}

void JobParameter::acquisitionTime3D(const double nSecond) {
    m_dAcquisitionTime3D = nSecond;
}

double JobParameter::acquisitionInterval2D(void) const {
    return m_dAcquisitionInterval2D;
}

void JobParameter::acquisitionInterval2D(const double nSecond) {
    m_dAcquisitionInterval2D = nSecond;
}

double JobParameter::acquisitionInterval3D(void) const {
    return m_dAcquisitionInterval3D;
}

void JobParameter::acquisitionInterval3D(const double nSecond) {
    m_dAcquisitionInterval3D = nSecond;
}

int JobParameter::IterationCount(void) const {
    return odt.m_nIteration;
}

void JobParameter::IterationCount(int nIteration) {
    odt.m_nIteration = nIteration;
}

bool JobParameter::DisableZLimit(void) const {
    return odt.m_bDisableZLimit;
}

void JobParameter::DisableZLimit(bool bDisable) {
    odt.m_bDisableZLimit = bDisable;
}

int JobParameter::IgnorePoor() const {
    return odt.m_nIgnorePoor;
}

void JobParameter::IgnorePoor(int nIgnore) {
    odt.m_nIgnorePoor = nIgnore;
}


double JobParameter::FLCameraShutter(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return 0;
    return ft[nChannel].m_dCameraShutter;
}

void JobParameter::FLCameraShutter(TC::FLChannel channel, double dValue) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_dCameraShutter = dValue;
}

double JobParameter::FLCameraGain(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return 0;
    return ft[nChannel].m_dGain;
}

void JobParameter::FLCameraGain(TC::FLChannel channel, double dValue) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_dGain = dValue;
}

double JobParameter::FLLightIntensity(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return 0;
    return ft[nChannel].m_dIntensity;
}

void JobParameter::FLLightIntensity(TC::FLChannel channel, double dValue) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_dIntensity = dValue;
}

bool JobParameter::FLEnableChannel(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return false;
    return ft[nChannel].m_bEnable;
}

void JobParameter::FLEnableChannel(TC::FLChannel channel, bool bEnable) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_bEnable = bEnable;
}

QString JobParameter::FLFluorophoreName(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return "NA";
    return ft[nChannel].m_strFluorophore;
}

void JobParameter::FLFluorophoreName(TC::FLChannel channel, const QString name) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_strFluorophore = name;
}

int JobParameter::FLFluorophoreEmission(TC::FLChannel channel) const {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return 0;
    return ft[nChannel].m_nEmission;
}

void JobParameter::FLFluorophoreEmission(TC::FLChannel channel, int emission) {
    const auto nChannel = static_cast<int>(channel);
    if (nChannel < 0) return;
    ft[nChannel].m_nEmission = emission;
}

double JobParameter::FLScanRange(void) const {
    return m_dFLScanRange;
}

void JobParameter::FLScanRange(double dValue) {
    m_dFLScanRange = dValue;
}

double JobParameter::FLScanStep(void) const {
    return m_dFLScanStep;
}

void JobParameter::FLScanStep(double dValue) {
    m_dFLScanStep = dValue;
}

int JobParameter::FLScanSteps(void) const {
    return m_nFLScanSteps;
}

void JobParameter::FLScanSteps(int nValue) {
    m_nFLScanSteps = nValue;
}

double JobParameter::FLScanCenter(void) const {
    return m_dFLScanCenter;
}

void JobParameter::FLScanCenter(double dValue) {
    m_dFLScanCenter = dValue;
}

double JobParameter::FLAcquisitionTime3D(void) const {
    return m_dFLAcquisitionTime3D;
}

void JobParameter::FLAcquisitionTime3D(const double dSecond) {
    m_dFLAcquisitionTime3D = dSecond;
}

double JobParameter::FLAcquisitionInterval3D(void) const {
    return m_dFLAcquisitionInterval3D;
}

void JobParameter::FLAcquisitionInterval3D(const double nSecond) {
    m_dFLAcquisitionInterval3D = nSecond;
}

QString JobParameter::deconvConfigPath(void) {
    if (m_strLoadedPath.isEmpty()) return m_strLoadedPath;
    QString strPath = m_strLoadedPath;

    return strPath.replace(".tcp", ".deconv", Qt::CaseInsensitive);
}



