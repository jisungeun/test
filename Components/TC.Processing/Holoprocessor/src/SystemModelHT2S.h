#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"
#include "SystemModelHT1S.h"


class SystemModelHT2S : public SystemModelHT1S {
public:
    typedef SystemModelHT2S Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    virtual QString Model() const override { return "HT-2S"; }
    virtual int Generation() const override { return 1; }

    //Fluorscence
    virtual double FluorescenceScanDepth() override { return 20; }
    virtual double FluorescenceScanStep() override { return 0.4; }
    virtual double FluorescenceScanStepUnit() override { return 0.15625; }

    //Motion
    virtual double MotionMovableLowerX() override { return -4.0; }
    virtual double MotionMovableUpperX() override { return  4.0; }
    virtual double MotionMovableLowerY() override { return -4.0; }
    virtual double MotionMovableUpperY() override { return  4.0; }

    virtual bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) override {
        if (tarCPos <= curCPos) return true;
        return (tarCPos <= (24.5 + curZPos));
    }

    virtual bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) override {
        if (tarZPos >= curZPos) return true;
        return (tarZPos >= (curCPos - 24.5));
    }

    //=== Model-specific functionality ===
    bool doesSupportFluorescence() override { return true; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase | Modes::Fluorescence);
    }
};

