#pragma once

#include <any>

#include <QString>
#include <QMap>

#include <arrayfire.h>

#include "GlobalDefines.h"
#include "ProgressReporter.h"
#include "StopChecker.h"
#include "TomoBuilderFieldRetrievalInterface.h"
#include "TomoBuilderGlobal.h"

namespace TC {
    class TCFWriter;
    class ImageSet;
    class FieldRetrievalParam;
    class PhaseImageParam;
    class TomographicMappingParam;

    class TomoBuilderInterface {
    public:
        TomoBuilderInterface(ProgressReporter* reporter = new NoProgressReporter(), StopChecker* stopchecker = new NoStopChecker());
        ~TomoBuilderInterface();

        bool SetParameter(const QString& strPath);
        bool LoadBackgroundImages(const QString& strPath, bool bOnlyNormal = false);
        bool LoadRawImages(const QString& strPath);
        bool LoadBFImages(const QString& strPath);

        bool FieldRetrievalBgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool miscCallibrationOnly = false, bool genPhaseOnly = false);
        bool FieldRetrievalFgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool genPhaseOnly = false, bool checkQuality = true);
        bool FieldRetrieval(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool genPhaseOnly = false, bool checkQuality = true);
        bool FieldRetrievalAtCalibration();

        bool BuildTomogram();

        bool GenPhaseImage(PhaseImageParam& param);

        bool GetWidth2D(double& width);

        bool SaveFieldRetrievalBg(const QString& strPath, int mode = FILE_AF);
        bool LoadFieldRetrievalBg(const QString& strPath);

        bool Save(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L, int mode = FILE_AF, bool isHTTomogram = true);
        bool Load(const QStringList& strArrFileList, bool& bHasRIData);

        bool SavePhasemapImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L);
        auto GenerateMipImage()->af::array;
        bool SaveMIPImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L);
        bool SaveBFImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L);

        void release();

        bool IsValidData(void);

        void SaveBadFieldsLog(const QString& strPath);

        void SetDeviceIndex(const int deviceIndex) { m_nDeviceIndex = deviceIndex; }
        int GetDeviceIndex(void) const { return m_nDeviceIndex; }

        auto TransferDataHT(std::any& data3d, std::any& dataMip, TC::MinMax& minMax3d, TC::MinMax& minMaxMip, TC::Resolution& resolution, TC::Sizes& sizes)->void;
        auto TransferDataPhase(std::any& data2d, TC::MinMax& minMax2d, TC::Resolution& resolution, TC::Sizes& sizes, const double& htSizeInum)->void;
        auto TransferDataBrightField(std::any& data3d, TC::Resolution& resolution, TC::Sizes& sizes)->void;

    protected:
        bool TomographicMapping(af::array& ORytov_index, af::array& ORytov);

        virtual bool Regularization(af::array& ORytov_index, af::array& ORytov, float normFact) = 0;

        //! method to fill some prameters using arr file provided
        bool Load(const QString& strArrFile);

        inline bool isStopRequested(void) const { return m_pStopChecker->isStopRequested(); }

        void reportProgress(int progress);

        DMDPatternGroupType GetPatternGroup(DMDPatternType type);

    protected:
        int m_nDeviceIndex;

        QMap<QString, double> m_parameter;
        ImageSet* m_pBgImageSet;
        ImageSet* m_pRawImageSet;
        FieldRetrievalParam* m_pFieldRetrievalParam;

        double m_dRes2;
        double m_dRes3;
        double m_dRes4;

        double m_dCropSize;
        double m_dCropFactor;

        //output of BuildTomogram...
        af::array m_Reconimg;

        //to export data to tcf file
        QList<QString> m_dataList_tomogram;
        QList<QString> m_dataList_phasemap;
        QList<QString> m_dataList_mip;
        QList<QString> m_dataList_bf;

        TomoBuilderFieldRetrievalInterface* m_pFieldRetrieval;
        TomoBuilderFieldRetrievalInterface* m_pFieldRetrievalSingle;

        bool m_bUseDecompositionFilter;

        ProgressReporter* m_pProgress;
        StopChecker* m_pStopChecker;

        struct Misc {
            bool m_bEnable;         //! use misc option
            bool m_bApplyFilter;    //! apply filter (valid only if m_bEnable is true)
            int m_nResidueCount;    //! residue count threshold (valid only if m_bEnable is true)
            int m_nMostCount;       //! how many images will be filtered (valid only if m_bEnable is true)
        } misc;

        friend class TCFWriter;
    };

}	//namespace TC