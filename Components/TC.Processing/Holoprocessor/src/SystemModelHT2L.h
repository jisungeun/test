#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"


class SystemModelHT2L : public SystemModelInterface {
public:
    typedef SystemModelHT2L Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    QString Model() const override { return "HT-2L"; }
    virtual int Generation() const override { return 1; }

    int MaxFramerate(unsigned int pixels) {
        return (pixels < 700) ? 150 : 100;
    }

    //Laser
    unsigned int Wavelength() override { return 532; }

    //Camera
    QString Camera() override { return "Pointgrey FL3-U3-13Y3M-C"; }

    //Objective Lens
    double ObjectiveLensMagnification() override { return 58.33; }
    double ObjectiveLensNA() override { return 1.2; }
    double ObjectiveLensReadyPos() override { return 0.0; }
    double ObjectiveLensLimitPos() override { return 1.0; }

    //Condenser Lens
    double CondenserLensMagnification() override { return 60; }
    double CondenserLensNA() override { return 0.7; }
    double CondenserLensReadyPos() override { return 19.0; }
    double CondenserLensLimitPos() override { return 20.0; }

    //Condenser Lens Auto Calibration
    double CondenserLensStepAmount(TC::CalibrationStepType type) override {
        double step = 0;

        switch (type) {
        case TC::STEP_OUTOFRANGE:
            step = 0.035;
            break;
        case TC::STEP_FAR:
            step = 0.01;
            break;
        case TC::STEP_NEAR:
            step = 0.002;
            break;
        case TC::STEP_FINE:
            step = 0.001;
            break;
        case TC::STEP_ESTIMATION:
            step = 0.0005;
            break;
        }

        return step;
    }

    virtual double CondenserLensDistThreshold(TC::CalibrationDistThreshold type) override {
        double threshold = 0;

        switch (type) {
        case TC::DIST_NEAR_THRESHOLD:
            threshold = 0.2;
            break;
        case TC::DIST_FINE_THRESHOLD:
            threshold = 0.05;
            break;
        }

        return threshold;
    }

    //Processing
    unsigned int ZeroPaddingMax2D() override { return 256; }
    unsigned int ZeroPaddingMax3D() override { return 312; }
    unsigned int NumberOfIteration() override { return 40; }

    //Fluorscence
    double FluorescenceScanDepth() override { return 20; }
    double FluorescenceScanStep() override { return 0.2; }
    double FluorescenceScanStepUnit() override { return 0.1563; }

    //Motion
    double MotionMovableLowerX() override { return -4.0; }
    double MotionMovableUpperX() override { return  4.0; }
    double MotionMovableLowerY() override { return -4.0; }
    double MotionMovableUpperY() override { return  4.0; }

    bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) override {
        if (tarCPos <= curCPos) return true;
        return (tarCPos <= (24.5 + curZPos));
    }

    bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) override {
        if (tarZPos >= curZPos) return true;
        return (tarZPos >= (curCPos - 24.5));
    }

    //=== Model-specific functionality ===
    bool doesSupportXYZControl() override { return true; }
    bool doesSupportFluorescence() override { return true; }
    bool doesSupportBrightfield() override { return false; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase | Modes::Fluorescence);
    }
};

