#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"
#include "SystemModelHT1L_Gen2.h"


class SystemModelHT2L_Gen2 : public SystemModelHT1L_Gen2 {
public:
    typedef SystemModelHT2L_Gen2 Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    virtual QString Model() const override { return "HT-2L Gen2"; }
    virtual int Generation() const override { return 2; }

    //Fluorescence
    virtual double FluorescenceScanDepth() override { return 0; }
    virtual double FluorescenceScanStep() override { return 0; }
    virtual double FluorescenceScanStepUnit() override { return 0.050; }
    virtual double DarkpixelCalibrationGain() override { return 40; }
    virtual double DarkpixelCalibrationExposure() override { return 20; }

    //=== Model-specific functionality ===
    virtual bool doesSupportFluorescence() override { return true; }
    virtual bool doesSupportBrightfield() override { return true; }
    virtual bool doesSupportBrightfieldColor() override { return true; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase | Modes::BrightFieldColor | Modes::Fluorescence);
    }
};

