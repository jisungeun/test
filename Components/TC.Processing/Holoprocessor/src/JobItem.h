#pragma once

#include <memory>

class JobItem {
public:
    enum {
        JOBITEM_2D,
        JOBITEM_3D,
        JOBITEM_3DFL,
        JOBITEM_BF,
        JOBITEM_SWITCH,
        JOBITEM_MOVE
    };

public:
    JobItem() {}

    virtual int type(void) const = 0;
    virtual double GetZPos(void) const = 0;
    virtual int GetBins(void) const = 0;
};

typedef std::shared_ptr<JobItem> JobItemPtr;

class JobItem2D : public JobItem {
public:
    JobItem2D() {}
    JobItem2D(const JobItem2D& other) {
        m_dInterval = other.m_dInterval;
        m_nImages = other.m_nImages;
        m_dZPos = other.m_dZPos;
    }

    int type(void) const override { return JOBITEM_2D; }
    double GetZPos(void) const override { return m_dZPos; }
    int GetBins(void) const override { return 1; }

public:
    double m_dInterval;			//! acquisition time interval in second
    int m_nImages;				//! number of images
    double m_dZPos;				//! acquisition height
};

class JobItem3D : public JobItem {
public:
    JobItem3D()
        : m_nBins(1) {
    }

    JobItem3D(const JobItem3D& other) {
        m_nImages = other.m_nImages;
        m_dInterval = other.m_dInterval;
        m_nBins = other.m_nBins;
        m_dZPos = other.m_dZPos;
    }

    int type(void) const override { return JOBITEM_3D; }
    double GetZPos(void) const override { return m_dZPos; }
    int GetBins(void) const override { return m_nBins; }

public:
    int m_nImages;				//! number of images
    double m_dInterval;			//! acquisition time interval in second
    int m_nBins;				//! number of bins
    double m_dZPos;				//! acquisition height
};

class JobItem3DFL : public JobItem {
public:
    JobItem3DFL()
        : m_nBins(1)
        , m_nImages(1)
        , m_nScanSteps(1) {
    }

    JobItem3DFL(const JobItem3DFL& other) {
        m_nImages = other.m_nImages;
        m_nBins = other.m_nBins;
        m_dInterval = other.m_dInterval;

        for (int i = 0; i < 3; i++) {
            channel[i].m_dCameraShutter = other.channel[i].m_dCameraShutter;
            channel[i].m_dGain = other.channel[i].m_dGain;
            channel[i].m_dIntensity = other.channel[i].m_dIntensity;
            channel[i].m_bEnable = other.channel[i].m_bEnable;
        }

        m_nEnabledChannels = other.m_nEnabledChannels;

        m_dScanStep = other.m_dScanStep;
        m_nScanSteps = other.m_nScanSteps;
        m_dScanRange = other.m_dScanRange;
        m_dScanCenter = other.m_dScanCenter;
    }

    int type(void) const override { return JOBITEM_3DFL; }
    double GetZPos(void) const override { return m_dScanCenter; }

public:
    int m_nImages;
    double m_dInterval;
    int m_nBins;				//! number of bins

    struct {
        double m_dCameraShutter;		//! camera shutter
        double m_dGain;					//! camera gain
        double m_dIntensity;			//! light intensity
        bool m_bEnable;					//! on/off light channel
    } channel[3];

    int m_nEnabledChannels;				//! enabled channels

    double m_dScanStep;			//! scan step amount
    int m_nScanSteps;			//! number of steps
    double m_dScanRange;		//! scan range
    double m_dScanCenter;		//! the center of scan range
};

class JobItemBF : public JobItem {
public:
    JobItemBF() {}
    JobItemBF(const JobItemBF& other) {
        m_dInterval = other.m_dInterval;
        m_dIntensity = other.m_dIntensity;
        m_dShutter = other.m_dShutter;
        m_nImages = other.m_nImages;
        m_dZPos = other.m_dZPos;
    }

    int type(void) const override { return JOBITEM_BF; }
    double GetZPos(void) const override { return m_dZPos; }
    int GetBins(void) const override { return 1; }

public:
    double m_dInterval;			//! acquisition time interval in second
    double m_dIntensity;		//! intensity
    double m_dShutter;			//! shutter
    int m_nImages;				//! number of images
    double m_dZPos;				//! acquisition height
};

class JobItemSwitch : public JobItem {
public:
    enum {
        MULTI_POINTS,
        TILES,
        MATRIX
    };

public:
    JobItemSwitch()
        : m_nImagingMode(0)
        , m_dOverlapH(0)
        , m_dOverlapV(0)
        , m_nTotalH(0)
        , m_nTotalV(0)
        , m_bInclFL(false) {
    }

    JobItemSwitch(const JobItemSwitch& other) {
        m_nIndex = other.m_nIndex;
        m_dXPos = other.m_dXPos;
        m_dYPos = other.m_dYPos;
        m_dZPos = other.m_dZPos;
        m_dCPos = other.m_dCPos;
        m_strPath = other.m_strPath;

        m_bInclFL = other.m_bInclFL;

        m_nImagingMode = other.m_nImagingMode;
        m_nTotalH = other.m_nTotalH;
        m_nTotalV = other.m_nTotalV;
        m_nTotal = other.m_nTotal;
        m_dOverlapH = other.m_dOverlapH;
        m_dOverlapV = other.m_dOverlapV;
        m_nColIdx = other.m_nColIdx;
        m_nRowIdx = other.m_nRowIdx;
    }

    int type(void) const { return JOBITEM_SWITCH; }
    double GetZPos(void) const override { return m_dZPos; }
    int GetBins(void) const override { return 1; }

public:
    int m_nIndex;				//! index of positions...
    double m_dXPos;				//! X-axis position
    double m_dYPos;				//! Y-axis position
    double m_dZPos;				//! Z-axis position
    double m_dCPos;				//! C-axis position
    QString m_strPath;			//! Acqusition path from reconstruction policy...

    bool m_bInclFL;				//! include FL acquisition

    //Tiles/Matrix imaging related...
    int m_nImagingMode;			//! 0: multi points, 1: tiles, 2: matrix
    int m_nTotal;				//! total points
    int m_nTotalH;				//! total points in horizontal direction
    int m_nTotalV;				//! total points in vertical direction
    double m_dOverlapH;			//! horizontal overlap (mm)
    double m_dOverlapV;			//! vertical overlap (mm)
    int m_nColIdx;				//! column index
    int m_nRowIdx;				//! row index
};

class JobItemMove : public JobItem {
public:
    JobItemMove() {
    }

    JobItemMove(const JobItemSwitch& other) {
        m_dXPos = other.m_dXPos;
        m_dYPos = other.m_dYPos;
        m_dZPos = other.m_dZPos;
    }

    int type(void) const { return JOBITEM_MOVE; }
    double GetZPos(void) const override { return m_dZPos; }
    int GetBins(void) const override { return 1; }

public:
    double m_dXPos;				//! X-axis position
    double m_dYPos;				//! Y-axis position
    double m_dZPos;				//! Z-axis position
};
