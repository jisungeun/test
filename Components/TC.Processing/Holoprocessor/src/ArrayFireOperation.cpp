#include "ArrayFireOperation.h"

#include "TomoBuilderUtility.h"

auto Fft2() -> void {
    auto randomArray = af::randn(1000, 1000, f32);
    //auto divider = af::constant(1000 * 1000, 1000, 1000, f32);
    randomArray.eval();
    auto fftRandomArray = fftshift(af::fft2(randomArray)) / 1000 / 1000; //leak
    //auto fftRandomArray = af::fft2(randomArray) / 1000 / 1000; //leak
    //auto fftRandomArray = randomArray / 1000 / 1000; //leak
    //auto fftRandomArray = randomArray / divider; //leak
    //auto fftRandomArray = fftshift(randomArray); //leak

    //auto fftRandomArray = af::fft2(randomArray); //doesn't leak
    fftRandomArray.eval();
}
