#define LOGGER_TAG "TCHoloprocessor::TomoBuilderInterface"
#include <TCLogger.h>

#include <QFile>
#include <QStringList>
#include <QSettings>
#include <QTextStream>

#include <set>
#include <memory>
#include <af/macros.h>

#include "GlobalDefines.h"

#include "TomoBuilderUtility.h"

#include "TomoBuilderFieldRetrievalSingle.h"
#include "TomoBuilderFieldRetrievalMulti.h"

#include "TomoBuilderInterface.h"
#include "AfArrayProfiler.h"
#include "AfArrayProxy.h"

namespace TC {
    TomoBuilderInterface::TomoBuilderInterface(ProgressReporter* reporter, StopChecker* stopchecker)
        : m_pFieldRetrieval(nullptr)
        , m_pFieldRetrievalSingle(nullptr)
        , m_bUseDecompositionFilter(false)
        , m_pProgress(reporter)
        , m_pStopChecker(stopchecker)
        , m_nDeviceIndex(0) {
        AF_ENABLEPROFILER(false);
        AF_RESETPROFILER();

        m_pBgImageSet = new ImageSet();
        m_pRawImageSet = new ImageSet();
        m_pFieldRetrievalParam = new FieldRetrievalParam();

        misc.m_bEnable = QSettings().value("Misc/Poor Quality Filter/Enable", false).toBool();
        misc.m_bApplyFilter = QSettings().value("Misc/Poor Quality Filter/Do Filtering", true).toBool();
        misc.m_nResidueCount = QSettings().value("Misc/Poor Quality Filter/Quality Threshold", 50).toInt();
        misc.m_nMostCount = QSettings().value("Misc/Poor Quality Filter/Remove Count", 5).toInt();

        if (misc.m_bEnable) {
            QLOG_INFO() << "Misc - override poor quality filter : " << misc.m_bEnable;
            QLOG_INFO() << "Misc - use poor quality filter : " << misc.m_bApplyFilter;
            QLOG_INFO() << "Misc - poor quality threshold : " << misc.m_nResidueCount;
            QLOG_INFO() << "Misc - number of poor quality : " << misc.m_nMostCount;
        }
    }

    TomoBuilderInterface::~TomoBuilderInterface() {
        delete m_pFieldRetrieval;
        delete m_pFieldRetrievalSingle;
        delete m_pBgImageSet;
        delete m_pRawImageSet;
        delete m_pFieldRetrievalParam;

        af::sync();

        TC::release();
        TC::release_fft_plans();

        static int logidx = 0;
        AF_EXPORT("D:/Temp/ArrayFireMemory", logidx++);
    }

    void TomoBuilderInterface::release(void) {
        TC::release();
    }

    bool TomoBuilderInterface::IsValidData(void) {
        dim_t bgCount = 0, fgCount = 0;

        bgCount = m_pFieldRetrievalParam->f_dx.elements();
        if (bgCount == 0) bgCount = m_pBgImageSet->images();

        fgCount = m_pRawImageSet->images();

        const bool isValid = (bgCount == fgCount) && (fgCount > 0);

        if (!isValid)
            QLOG_ERROR() << "Invalid data set : bgCount=" << bgCount << " fgCount=" << fgCount;

        return isValid;
    }

    void TomoBuilderInterface::SaveBadFieldsLog(const QString& strPath) {
        std::map<int, int>& badFields = m_pFieldRetrievalParam->badFields;
        if (badFields.size() == 0) return;

        QFile file(strPath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);

        auto iter = badFields.begin();
        for (; iter != badFields.end(); iter++) {
            out << iter->first << "=" << iter->second << "\n";
        }
    }

    bool TomoBuilderInterface::SetParameter(const QString& strPath) {
        if (!LoadParameter(strPath, m_parameter)) return false;

        //set default values...
        if (m_parameter.find(PN_PHASE_SIGN) == m_parameter.end()) {
            m_parameter[PN_PHASE_SIGN] = -1;
        }

        if (m_parameter.find(PN_ITERATION) == m_parameter.end()) {
            m_parameter[PN_ITERATION] = 20;
        }

        if (m_parameter.find(PN_NIR_RADIUS) == m_parameter.end()) {
            m_parameter[PN_NIR_RADIUS] = 10;
        }

        if (m_parameter.find(PN_ACQUISITION_INTERVAL_2D) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_INTERVAL_2D] = 0.008;
        }

        if (m_parameter.find(PN_ACQUISITION_INTERVAL_3D) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_INTERVAL_3D] = 1.0;
        }

        if (m_parameter.find(PN_ACQUISITION_INTERVAL) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_INTERVAL] = -1;    //Old parameter
        }

        if (m_parameter.find(PN_ACQUISITION_COUNT) == m_parameter.end()) {
            m_parameter[PN_ACQUISITION_COUNT] = -1;    //Old parameter
        }

        if (m_parameter.find(PN_MAPPGIN_SIGN) == m_parameter.end()) {
            m_parameter[PN_MAPPGIN_SIGN] = -1;
        }

        if (m_parameter.find(PN_PATTERN_TYPE) == m_parameter.end()) {
            m_parameter[PN_PATTERN_TYPE] = TC::PATTERN_LEGACY;
        }

        if (m_parameter.find(PN_IMMERSION_RI) == m_parameter.end()) {
            m_parameter[PN_IMMERSION_RI] = 1.337;
        }

        if (m_parameter.find(PN_NA_CDS) == m_parameter.end()) {
            m_parameter[PN_NA_CDS] = m_parameter[PN_NA];
        }

        //create TomoBuilderRetrieval instance....
        auto type = static_cast<TC::DMDPatternType>((int)m_parameter[PN_PATTERN_TYPE]);
        DMDPatternGroupType ptnGroup = GetPatternGroup(type);

        switch (ptnGroup) {
        case TC::PATTERN_GROUP_LEGACY:
        case TC::PATTERN_GROUP_LEE_HOLOGRAMM:
            m_pFieldRetrieval = new TomoBuilderFieldRetrievalSingle(m_parameter, m_pBgImageSet, m_pRawImageSet, m_pFieldRetrievalParam);
            m_bUseDecompositionFilter = false;
            break;
        case TC::PATTERN_GROUP_SIM:
            m_pFieldRetrieval = new TomoBuilderFieldRetrievalMulti(m_parameter, m_pBgImageSet, m_pRawImageSet, m_pFieldRetrievalParam);
            m_bUseDecompositionFilter = true;
            break;
        default:
            QLOG_WARN() << "Unknown pattern type (=" << (int)type << ") but it will try to process it";
            m_pFieldRetrieval = new TomoBuilderFieldRetrievalSingle(m_parameter, m_pBgImageSet, m_pRawImageSet, m_pFieldRetrievalParam);
            m_bUseDecompositionFilter = false;
            break;
        }

        //common
        m_pFieldRetrievalSingle = new TomoBuilderFieldRetrievalSingle(m_parameter, m_pBgImageSet, m_pRawImageSet, m_pFieldRetrievalParam);

        return true;
    }


    bool TomoBuilderInterface::LoadBackgroundImages(const QString& strPath, bool bOnlyNormal) {
        const bool bRes = LoadImages(strPath, "png", 0, bOnlyNormal, m_pBgImageSet->arrData, m_pBgImageSet->nImages);
        return bRes;
    }

    bool TomoBuilderInterface::LoadRawImages(const QString& strPath) {
        const bool bRes = LoadImages(strPath, "png", 0, false, m_pRawImageSet->arrData, m_pRawImageSet->nImages);
        return bRes;
    }

    bool TomoBuilderInterface::LoadBFImages(const QString& strPath) {
        const bool bRes = LoadImagesColor(strPath, "png", 0, m_pRawImageSet->arrData, m_pRawImageSet->nImages);
        return bRes;
    }

    bool TomoBuilderInterface::FieldRetrievalAtCalibration() {
        return FieldRetrievalBgOnly(true, false, 0, true);
    }

    //!
    // miscCallibrationOnly : true if it is called at calibration stage (default: false)
    bool TomoBuilderInterface::FieldRetrievalBgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool miscCallibrationOnly, bool genPhaseOnly) {
        if (!m_pFieldRetrieval || !m_pFieldRetrievalSingle) return false;

        TomoBuilderFieldRetrievalInterface* pInterface = m_pFieldRetrieval;
        if (genPhaseOnly) pInterface = m_pFieldRetrievalSingle;

        bool ignorePhaseSign = (genPhaseOnly) ? true : false;

        return pInterface->FieldRetrievalBgOnly(hasNormal, onlyNormalOnBg, zeroPadding, miscCallibrationOnly, ignorePhaseSign);
    }

    bool TomoBuilderInterface::FieldRetrievalFgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool genPhaseOnly, bool checkQuality) {
        const char* fname_out = "field_retrieval_fg_only.arr";

        if (!m_pFieldRetrieval || !m_pFieldRetrievalSingle) return false;

        TomoBuilderFieldRetrievalInterface* pInterface = m_pFieldRetrieval;
        if (genPhaseOnly) pInterface = m_pFieldRetrievalSingle;

        bool ignorePhaseSign = (genPhaseOnly) ? true : false;

        const bool bRes = pInterface->FieldRetrievalFgOnly(hasNormal, onlyNormalOnBg, zeroPadding, ignorePhaseSign, checkQuality);

        if (bRes) {
            const double res = m_pFieldRetrievalParam->res;
            af::array& f_dx = m_pFieldRetrievalParam->f_dx;
            af::array& f_dy = m_pFieldRetrievalParam->f_dy;
            af::array& retPhase = m_pFieldRetrievalParam->retPhase;
            af::array& retAmplitude = m_pFieldRetrievalParam->retAmplitude;

            dbg_saveArray("arr_retPhase_1", retPhase, fname_out, false);
            dbg_saveArray("arr_retAmplitude_1", retAmplitude, fname_out, true);

            const int d0 = retPhase.dims(0);
            const int d1 = retPhase.dims(1);
            const af::seq a0 = af::seq(2, d0 - 3);
            const af::seq a1 = af::seq(2, d1 - 3);
            retPhase = retPhase(a0, a1, af::span);
            retAmplitude = retAmplitude(a0, a1, af::span);

            dbg_saveArray("arr_retPhase_2", retPhase, fname_out, false);
            dbg_saveArray("arr_retAmplitude_2", retAmplitude, fname_out, true);

            dbg_saveArray("arr_f_dx", f_dx, fname_out, true);
            dbg_saveArray("arr_f_dy", f_dy, fname_out, true);

            // !MATLAB: [xSize, ySize, thetaSize] = size(retPhase);
            const unsigned int xSize = retPhase.dims(0);
            const unsigned int ySize = retPhase.dims(1);
            const unsigned int thetaSize = retPhase.dims(2);

            //! MATLAB: original_size=rawSize; crop_size=ySize;
            const unsigned int original_size = m_pFieldRetrievalParam->rawSize;

            //QLOG_INFO() << "xSize=" << xSize << " original_size=" << original_size << " res=" << res;

            unsigned int ZP, ZP2, ZP3;
            pInterface->CalcZeroPaddingSize(ZP, ZP2, ZP3, xSize + 8, original_size, res);

            dbg_saveArray("arr_ZP", af::constant<unsigned int>(ZP, 1), fname_out, true);
            dbg_saveArray("arr_ZP2", af::constant<unsigned int>(ZP2, 1), fname_out, true);
            dbg_saveArray("arr_ZP3", af::constant<unsigned int>(ZP3, 1), fname_out, true);

            m_pFieldRetrievalParam->SetZeropadding(ZP, ZP2, ZP3);

            //TC_MEM_INFO("after field retrieval");

            m_pRawImageSet->clear();

            m_dCropSize = pInterface->m_dCropSize;
            m_dCropFactor = pInterface->m_dCropFactor;
            m_dRes2 = pInterface->m_dRes2;
            m_dRes3 = pInterface->m_dRes3;
            m_dRes4 = pInterface->m_dRes4;

            if (!genPhaseOnly) {
                QLOG_INFO() << "(ZP, ZP2, ZP3) = (" << ZP << "," << ZP2 << "," << ZP3 << ") (xSize, original_size) = (" << xSize << "," << original_size << ")";
                QLOG_INFO() << "(res,res2,res3,res4) = " << res << "," << m_dRes2 << "," << m_dRes3 << "," << m_dRes4 << ")";
            }

            dbg_saveArray("arr_original_size", af::constant<unsigned int>(original_size, 1), fname_out, true);
            dbg_saveArray("arr_crop_size", af::constant<unsigned int>(m_dCropSize, 1), fname_out, true);
            dbg_saveArray("arr_crop_factor", af::constant<double>(m_dCropFactor, 1), fname_out, true);

            dbg_saveArray("arr_res", af::constant<double>(res, 1), fname_out, true);
            dbg_saveArray("arr_res2", af::constant<double>(m_dRes2, 1), fname_out, true);
            dbg_saveArray("arr_res3", af::constant<double>(m_dRes3, 1), fname_out, true);
            dbg_saveArray("arr_res4", af::constant<double>(m_dRes4, 1), fname_out, true);
        }

        return bRes;
    }

    //!
    //hasNormal - true if the first image of bg is normal angle image (default: true)
    //onlyNormalOnBg - true if there is only a normal angle image on bg image set (default: false)
    //               - used for 2d phasemap processing
    //zeropadding - size of zero padding (default: 0)
    //genPhaseOnly - true if it is called for phase image (special purpose) (default: false)
    bool TomoBuilderInterface::FieldRetrieval(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool genPhaseOnly, bool checkQuality) {
        if (!FieldRetrievalBgOnly(hasNormal, onlyNormalOnBg, zeroPadding, false, genPhaseOnly)) return false;
        return FieldRetrievalFgOnly(hasNormal, onlyNormalOnBg, zeroPadding, genPhaseOnly, checkQuality);
    }

    bool TomoBuilderInterface::GenPhaseImage(PhaseImageParam& param) {
        const int mode = param.mode;
        const int zeropadding = param.zeropadding;
        const double dParam = param.dParam;
        const bool bCompensateAnalysis = param.bCompensateAnalysis;

        const QString bgImgPath = param.bgImgNormalPath;
        const QString bgImgPath2 = param.bgImgPath;
        const QString rawImgPath = param.rawImgPath;
        const QString strOutPath = param.outputPath;

        const double res = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
        const double NA = m_parameter[PN_NA];
        const double lambda = m_parameter[PN_LAMBDA];

        m_pBgImageSet->nImages = 1;
        m_pRawImageSet->nImages = 1;

        if (!TC::LoadImage(bgImgPath, m_pBgImageSet->arrData)) {
            QLOG_ERROR() << "Failed to load bg = " << bgImgPath;
            return false;
        }

        if (!TC::LoadImage(rawImgPath, m_pRawImageSet->arrData)) {
            QLOG_ERROR() << "Failed to load img = " << rawImgPath;
            return false;
        }

        if (mode != TC::IMG_ANALYSIS)	//! TOMV-337
        {
            FieldRetrieval(false, false, zeropadding, true, false);
        }

        af::array& retPhase = m_pFieldRetrievalParam->retPhase;
        af::array& retAmplitude = m_pFieldRetrievalParam->retAmplitude;

        af::array pcolor;

        if (mode == TC::IMG_PHASE) {
            //! MATLAB: pmax = 4;
            const float pmax = dParam;

            //! MATLAB: pidx = uint8((retPhase / (pmax * 2)) * 255 + 127);
            //! MATLAB: Map = jet(255);
            //! MATLAB: pcolor = ind2rgb(pidx, Map);
            pcolor = phase2rgb(retPhase, 0, pmax);
        } else if (mode == TC::IMG_BRIGHTFIELD) {
            //! MATLAB: pidx = min(uint8((retAmplitude / 2) * 255), 255);
            //! MATLAB: Map = gray(255);
            //! MATLAB: pcolor = ind2rgb(pidx, Map);
            pcolor = amplitue2rgb(retAmplitude);
        } else if (mode == TC::IMG_DIC) {
            //! MATLAB: pmax = 2;
            //! MATLAB: DIC = retPhase - circshift(retPhase, [3 3]);
            //! MATLAB: DIC = DIC(3:end - 2, 3 : end - 2);
            //! MATLAB: pidx = min(uint8((DIC / (pmax * 2) * 255) + 127), 255);
            //! MATLAB: Map = gray(255);
            //! MATLAB: pcolor = ind2rgb(pidx, Map);
            pcolor = phase2dic(retPhase, 2.0f, (unsigned int)dParam);
        } else if (mode == TC::IMG_PHASE_CONTRAST) {
            //! PC = abs(retAmplitude).*abs(1 + retPhase);
            //! pmax = (max(PC(:)) + 1) / 2;
            //! pidx = min(uint8((PC / (pmax * 2) * 255) + 127), 255);
            //! Map = bone(255);
            //! pcolor = ind2rgb(pidx, Map);
            pcolor = phase2contrast(retAmplitude, retPhase, dParam);
        } else if (mode == TC::IMG_ANALYSIS) {
            af::array& bg_data = m_pBgImageSet->arrData;
            af::array& raw_data = m_pRawImageSet->arrData;

            af::array img = af::moddims(raw_data(af::span, af::span, 0), raw_data.dims(0), raw_data.dims(1));
            af::array fImg;

            if (bCompensateAnalysis) {
                //-- normal angle
                af::array bg = af::moddims(bg_data(af::span, af::span, 0), bg_data.dims(0), bg_data.dims(1));
                const uint rawSize = bg.dims(0);
                const unsigned int squre_of_rawSize = std::pow(rawSize, 2);

                //! MATLAB: Fimg = fftshift(fft2(img))/(rawSize^2);
                af::array Fimg = fftshift(af::fft2(bg)) / squre_of_rawSize;

                const unsigned int c0 = std::round(rawSize * 0.55);
                const unsigned int c1 = std::round(rawSize * 0.99); //TOMV-789

                //! MATLAB: FimgMax = max(max(Fimg(:, round(rawSize*0.55):round(rawSize*0.99) )));
                af::cfloat FimgMax = af::max<af::cfloat>(Fimg(af::span, af::seq(c0, c1)));

                //! MATLAB: [mi,mj]=find(Fimg==FimgMax);
                unsigned int mi, mj;
                Find(Fimg == FimgMax, mi, mj);

                const uint f_x0 = mi;
                const uint f_y0 = mj;

                //! MATLAB: mi=round(mi-rawSize/2-1); mj=round(mj-rawSize/2-1);
                mi = std::round(mi - rawSize / 2.0 - 1);
                mj = std::round(mj - rawSize / 2.0 - 1);

                //! MATLAB: r=round(rawSize*res*NA/lambda);
                const unsigned int r = std::round(rawSize * res * NA / lambda);

                //! MATLAB: yr = r;
                const unsigned int yr = r;

                //! MATLAB: c1mask = ~(mk_ellipse(r,yr,rawSize,rawSize));
                af::array tmpMask;
                mk_ellipse(tmpMask, r, yr, rawSize, rawSize);
                af::array c1mask = !(tmpMask);

                //! MATLAB: c3mask = circshift(c1mask,[mi mj]);
                af::array c3mask = af::shift(c1mask, mi + 1, mj + 1);

                //! MATALB: fsMargin = 2*round(r/6);
                const uint fsMargin = 2 * std::round(r / 6);

                const uint a0 = (rawSize / 2 - r - fsMargin / 2 + 1) - 1;
                const uint a1 = (rawSize / 2 + r + fsMargin / 2) - 1;


                //-- bg for specific angle
                af::array img;
                TC::LoadImage(bgImgPath2, img);

                //! MATLAB: Fimg = fftshift(fft2(img))/(rawSize^2);
                Fimg = fftshift(af::fft2(img)) / squre_of_rawSize;

                //! MATLAB: FimgMax = max(max(Fimg(:, round(rawSize*0.55):round(rawSize*0.99) )));
                FimgMax = af::max<af::cfloat>(Fimg(af::span, af::seq(c0, c1)));

                //! MATLAB: [f_x,f_y]=find(Fimg==FimgMax);
                unsigned int f_x, f_y;
                Find(Fimg == FimgMax, f_x, f_y);

                //! MATLAB: Fimg = Fimg.*c3mask;
                Fimg = Fimg * c3mask;

                //! MATLAB: Fimg = circshift(Fimg, -[round(mi) round(mj)]);
                Fimg = af::shift(Fimg, -1 * (mi + 1), -1 * (mj + 1));

                //! MATLAB: Fimg = Fimg(rawSize / 2 - r - fsMargin / 2 + 1:rawSize / 2 + r + fsMargin / 2, rawSize / 2 - r - fsMargin / 2 + 1 : rawSize / 2 + r + fsMargin / 2);
                af::array Fimg2 = Fimg(af::seq(a0, a1), af::seq(a0, a1));

                //! MATLAB: sizeFimg = length(Fimg);
                const uint sizeFimg = Fimg2.dims(0);

                //! MATLAB: Fbg = ifft2(ifftshift(Fimg))*(sizeFimg ^ 2);
                af::array Fbg = af::ifft2(ifftshift(Fimg)) * std::pow(sizeFimg, 2);


                //-- sample image
                //! MATLAB: img = imread(['data/' d(ii).name]);
                img = m_pRawImageSet->arrData;

                //! MATLAB:  Fimg = fftshift(fft2(img)) / (rawSize ^ 2); %FFT
                Fimg = fftshift(af::fft2(img)) / squre_of_rawSize;

                //! MATLAB: Fimg = Fimg.*c3mask;
                Fimg = Fimg * c3mask;

                //! MATLAB: Fimg = circshift(Fimg, -[round(mi) round(mj)]);
                Fimg = af::shift(Fimg, -1 * (mi + 1), -1 * (mj + 1));

                //! MATLAB: Fimg = Fimg(rawSize / 2 - r - fsMargin / 2 + 1:rawSize / 2 + r + fsMargin / 2, rawSize / 2 - r - fsMargin / 2 + 1 : rawSize / 2 + fsMargin / 2 + r);
                Fimg2 = Fimg(af::seq(a0, a1), af::seq(a0, a1));

                //! MATLAB: sizeFimg = length(Fimg);
                //! MATLAB: Fimg = ifft2(ifftshift(Fimg))*(sizeFimg ^ 2);
                Fimg = af::ifft2(ifftshift(Fimg)) * std::pow(sizeFimg, 2);

                //! MATLAB: Fimg = Fimg./Fbg;
                fImg = Fimg / Fbg;

                //! MATLAB: Fsimg = fftshift(fft2(ifftshift(Fimg)))/(sizeFimg^2);
                fImg = fftshift(af::fft2(ifftshift(fImg))) / std::pow(sizeFimg, 2);

                //! MATLAB: Fsimg = circshift(Fsimg, [f_x - f_dx(1) f_y - f_dy(1)]);
                fImg = af::shift(fImg, f_x - f_x0, f_y - f_y0);
            } else {
                //Fimg = fftshift(fft2(ifftshift(img)));
                //figure, imagesc(log10(abs(Fimg))), axis image, axis off, colormap('jet')
                fImg = fftshift(af::fft2(ifftshift(img)));
            }

            af::array absImg = af::log10(af::abs(fImg));
            float fmax = af::max<float>(absImg);

            pcolor = phase2rgb(absImg, 1, fmax);
        } else {
            return false;
        }

        //! MATLAB: imwrite(pcolor, outImgPath);
        af::saveImage(strOutPath.toLocal8Bit().constData(), pcolor);

        return true;
    }

    bool TomoBuilderInterface::GetWidth2D(double& width) {
        if (m_Reconimg.isempty()) return false;

        const unsigned int d0 = m_Reconimg.dims(0);
        const double fov = d0 * m_dRes3;

        width = fov;

        return true;
    }

    bool TomoBuilderInterface::SaveFieldRetrievalBg(const QString& strPath, int mode) {
        char pPath[2048];
        strcpy_s<2048>(pPath, strPath.toLocal8Bit().constData());

        FieldRetrievalParam* pParam = m_pFieldRetrievalParam;

        af::saveArray("ver", af::constant(RETRIEVAL_ALGO_VERSION, 1, u64), pPath);
        af::saveArray("f_dx", pParam->f_dx, pPath, true);
        af::saveArray("f_dy", pParam->f_dy, pPath, true);
        af::saveArray("c3mask", pParam->c3mask, pPath, true);
        af::saveArray("Fbg", pParam->Fbg, pPath, true);
        af::saveArray("bp_masks", pParam->bp_masks, pPath, true);

        unsigned int hasNormalSample = (pParam->hasNormalSample) ? 1 : 0;
        af::saveArray("hasNormalSample", af::constant(hasNormalSample, 1, u32), pPath, true);

        af::saveArray("rawSize", af::constant(pParam->rawSize, 1, u32), pPath, true);
        af::saveArray("res", af::constant(pParam->res, 1, f64), pPath, true);
        af::saveArray("nArrSize", af::constant(pParam->nArrSize, 1, u32), pPath, true);
        af::saveArray("mi", af::constant(pParam->mi, 1, u32), pPath, true);
        af::saveArray("mj", af::constant(pParam->mj, 1, u32), pPath, true);
        af::saveArray("a0", af::constant(pParam->a0, 1, u32), pPath, true);
        af::saveArray("a1", af::constant(pParam->a1, 1, u32), pPath, true);
        af::saveArray("idx_offset", af::constant(pParam->idx_offset, 1, u32), pPath, true);

        return true;
    }

    bool TomoBuilderInterface::LoadFieldRetrievalBg(const QString& strPath) {
        if (!QFile::exists(strPath)) {
            QLOG_ERROR() << "No file exists - " << strPath;
            return false;
        }

        try {
            FieldRetrievalParam* pParam = m_pFieldRetrievalParam;

            pParam->f_dx = af::readArray(QSTR2CSTR_CPTR(strPath), "f_dx");
            pParam->f_dy = af::readArray(QSTR2CSTR_CPTR(strPath), "f_dy");
            pParam->c3mask = af::readArray(QSTR2CSTR_CPTR(strPath), "c3mask");
            pParam->Fbg = af::readArray(QSTR2CSTR_CPTR(strPath), "Fbg");
            pParam->bp_masks = af::readArray(QSTR2CSTR_CPTR(strPath), "bp_masks");

            unsigned int hasNormalSample = af::readArray(QSTR2CSTR_CPTR(strPath), "hasNormalSample").scalar<unsigned int>();
            pParam->hasNormalSample = (hasNormalSample == 1) ? true : false;

            pParam->rawSize = af::readArray(QSTR2CSTR_CPTR(strPath), "rawSize").scalar<unsigned int>();
            pParam->res = af::readArray(QSTR2CSTR_CPTR(strPath), "res").scalar<double>();
            pParam->nArrSize = af::readArray(QSTR2CSTR_CPTR(strPath), "nArrSize").scalar<unsigned int>();
            pParam->mi = af::readArray(QSTR2CSTR_CPTR(strPath), "mi").scalar<unsigned int>();
            pParam->mj = af::readArray(QSTR2CSTR_CPTR(strPath), "mj").scalar<unsigned int>();
            pParam->a0 = af::readArray(QSTR2CSTR_CPTR(strPath), "a0").scalar<unsigned int>();
            pParam->a1 = af::readArray(QSTR2CSTR_CPTR(strPath), "a1").scalar<unsigned int>();

            //some files may not include idx_offset because they are generated before this key is introduced...
            if (af::readArrayCheck(QSTR2CSTR_CPTR(strPath), "idx_offset") > -1) {
                pParam->idx_offset = af::readArray(QSTR2CSTR_CPTR(strPath), "idx_offset").scalar<unsigned int>();
            } else {
                pParam->idx_offset = 0;
            }
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at TomoBuilderInterface::Load : " << e.what();
            QLOG_ERROR() << "  path - " << QSTR2CSTR_CPTR(strPath);

            return false;
        }

        return true;
    }

    bool TomoBuilderInterface::Save(const QString& savePath, TC::Position& pos, unsigned long long timestamp, int mode, bool isHTTomogram) {
        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        try {
            af::saveArray("Oxsize", af::constant(m_pFieldRetrievalParam->rawSize, 1, f64), pPath);
            af::saveArray("lambda", af::constant(m_parameter[PN_LAMBDA], 1, f64), pPath, true);
            af::saveArray("n_m", af::constant(m_parameter[PN_NM], 1, f64), pPath, true);
            af::saveArray("pixelsize", af::constant(m_pFieldRetrievalParam->res, 1, f64), pPath, true);
            af::saveArray("res2", af::constant(m_dRes2, 1, f64), pPath, true);
            af::saveArray("res3", af::constant(m_dRes3, 1, f64), pPath, true);
            af::saveArray("res4", af::constant(m_dRes4, 1, f64), pPath, true);
            af::saveArray("Reconimg", m_Reconimg, pPath, true);
            af::saveArray("timestamp", af::constant(timestamp, 1, u64), pPath, true);
            af::saveArray("retPhase", m_pFieldRetrievalParam->retPhase, pPath, true);
            af::saveArray("retAmplitude", m_pFieldRetrievalParam->retAmplitude, pPath, true);
            saveVal("posx", pos.x, pPath, true);
            saveVal("posy", pos.y, pPath, true);
            saveVal("posz", pos.z, pPath, true);
            saveVal("posc", pos.c, pPath, true);

            af::sync();
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at TomoBuilderInterface::Save : " << e.what();
            QLOG_ERROR() << "  path - " << QSTR2CSTR_CPTR(savePath);

            return false;
        }

        return true;
    }

    bool TomoBuilderInterface::Load(const QString& strArrFile) {
        if (!QFile::exists(strArrFile)) {
            QLOG_ERROR() << "No file exists - " << strArrFile;
            return false;
        }

        try {
            m_Reconimg = af::readArray(QSTR2CSTR_CPTR(strArrFile), "Reconimg");
            m_dRes4 = af::readArray(QSTR2CSTR_CPTR(strArrFile), "res4").scalar<double>();
            m_dRes3 = af::readArray(QSTR2CSTR_CPTR(strArrFile), "res3").scalar<double>();
            m_dRes2 = af::readArray(QSTR2CSTR_CPTR(strArrFile), "res2").scalar<double>();
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at TomoBuilderInterface::Load : " << e.what();
            QLOG_ERROR() << "  path - " << strArrFile;

            return false;
        }

        return true;
    }

    bool TomoBuilderInterface::Load(const QStringList& strArrFileList, bool& bHasRIData) {
        bool bCommonInfoLoaded = false;
        bHasRIData = false;

        if (strArrFileList.size() == 0) return false;

        try {
            //store all list of files...
            m_dataList_tomogram.clear();
            m_dataList_phasemap.clear();
            m_dataList_mip.clear();
            m_dataList_bf.clear();

            foreach(QString strPath, strArrFileList) {
                //std::cout << "Load from " << strPath.toStdString().c_str() << std::endl; 
                strPath.replace("\\", "/");

                //Tomogram
                if (!QFile::exists(strPath)) {
                    m_dataList_tomogram.clear();
                    m_dataList_phasemap.clear();
                    m_dataList_mip.clear();
                    m_dataList_bf.clear();
                    return false;
                }

                if (strPath.indexOf("/Tomogram.arr") > -1) {
                    if (!bCommonInfoLoaded) {
                        //TOMV-325 load common information...
                        if (!Load(strPath)) return false;
                        bCommonInfoLoaded = true;
                    }

                    m_dataList_tomogram.push_back(strPath);

                    //MIP
                    strPath.replace(QString("/Tomogram"), QString("/MIP"));
                    if (QFile::exists(strPath)) {
                        m_dataList_mip.push_back(strPath);
                    }

                    bHasRIData = true;
                } else if (strPath.indexOf("/Phasemap.arr") > -1) {
                    //Phasemap
                    m_dataList_phasemap.push_back(strPath);

                    bHasRIData = true;
                } else if (strPath.indexOf("/Brightfield.arr") > -1) {
                    //Brightfield
                    m_dataList_bf.push_back(strPath);
                }
            }
        } catch (af::exception& e) {
            QLOG_ERROR() << "Exception at TomoBuilderInterface::Load : " << e.what();
            return false;
        }

        if (!bCommonInfoLoaded && bHasRIData) {
            //TOMV-325 load common information...
            if (!Load(strArrFileList.at(0))) return false;
        }

        return true;
    }

    bool TomoBuilderInterface::SavePhasemapImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp) {
        bool bRes = Save(savePath, pos, timestamp);
        if (!bRes) return false;

        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        //calc stdPhase and then save to file
        af::array stdPhase = af::stdev(m_pFieldRetrievalParam->retPhase, 2);
        af::saveArray("stdPhase", stdPhase, pPath, true);
        saveVal("posx", pos.x, pPath, true);
        saveVal("posy", pos.y, pPath, true);
        saveVal("posz", pos.z, pPath, true);
        saveVal("posc", pos.c, pPath, true);

        return true;
    }

    auto TomoBuilderInterface::GenerateMipImage() -> af::array {
        dim_t d0 = m_Reconimg.dims(0);
        dim_t d1 = m_Reconimg.dims(1);
        dim_t d2 = m_Reconimg.dims(2);

        //! MATLAB: filtered = image;
        //! MATLAB: filtered(image<(RIRef + 0.005)) = 0;
        af::array filtered = m_Reconimg;
        filtered(m_Reconimg < (m_parameter[PN_NM] + 0.005)) = 0;

        //! MATLAB: projected = squeeze(sum(filtered, 1));
        af::array projected = af::sum(filtered, 0);
        projected = af::moddims(projected, d1, d2);

        //! MATLAB: line = squeeze(sum(projected, 1));
        af::array line = af::sum(projected, 0);
        line = af::moddims(line, d2);

        //! MATLAB: maxidx = find(line==max(line(:)));
        af::array maxVals;
        af::array maxIdxes;
        af::max(maxVals, maxIdxes, line);
        const unsigned int maxidx = maxIdxes.scalar<unsigned int>();

        //! MATLAB: left = line_shift(1:maxidx);
        //! MATLAB: right = line_shift(maxidx:end);
        af::array left = line(af::seq(0, maxidx));
        af::array right = line(af::seq(maxidx, af::end));

        //! MATLAB: th_low = (max(left(:)) - min(left(:)))*0.1 + min(left(:));
        //! MATLAB: th_hi = (max(right(:)) - min(right(:)))*0.1 + min(right(:));
        float maxv = af::max<float>(left);
        float minv = af::min<float>(left);
        const float th_low = (maxv - minv) * 0.1 + minv;

        maxv = af::max<float>(right);
        minv = af::min<float>(right);
        const float th_hi = (maxv - minv) * 0.1 + minv;

        //! MATLAB: idx_low = find(left<th_low, 1, 'last');
        //! MATLAB: idx_hi = find(right<th_hi, 1) + maxidx - 1;
        int idx_low = Find1D(left < th_low, false);
        int idx_high = Find1D(right < th_hi, true) + maxidx;

        //! MATLAB: idx_low = round(max(1, idx_low - 5));
        //! MATLAB: idx_hi = round(min(length(line), idx_hi + 5));
        idx_low = std::max(0, (int)round(idx_low - 5));
        idx_high = std::min((int)(line.elements() - 1), (int)round(idx_high + 5));

        af::array mip_image = af::constant(0, d0, d1);

        for (dim_t d0_idx = 0; d0_idx < d0; d0_idx++) {
            gfor(af::seq d1_idx, d1) {
                mip_image(d0_idx, d1_idx) = af::max(m_Reconimg(d0_idx, d1_idx, af::seq(idx_low, idx_high)));
            }
        }

        mip_image.eval();

        return mip_image;
    }

    bool TomoBuilderInterface::SaveMIPImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp) {
#undef max
        dim_t d0 = m_Reconimg.dims(0);
        dim_t d1 = m_Reconimg.dims(1);
        dim_t d2 = m_Reconimg.dims(2);

        //! MATLAB: filtered = image;
        //! MATLAB: filtered(image<(RIRef + 0.005)) = 0;
        af::array filtered = m_Reconimg;
        filtered(m_Reconimg < (m_parameter[PN_NM] + 0.005)) = 0;

        //! MATLAB: projected = squeeze(sum(filtered, 1));
        af::array projected = af::sum(filtered, 0);
        projected = af::moddims(projected, d1, d2);

        //! MATLAB: line = squeeze(sum(projected, 1));
        af::array line = af::sum(projected, 0);
        line = af::moddims(line, d2);

        //! MATLAB: maxidx = find(line==max(line(:)));
        af::array maxVals;
        af::array maxIdxes;
        af::max(maxVals, maxIdxes, line);
        const unsigned int maxidx = maxIdxes.scalar<unsigned int>();

        //! MATLAB: left = line_shift(1:maxidx);
        //! MATLAB: right = line_shift(maxidx:end);
        af::array left = line(af::seq(0, maxidx));
        af::array right = line(af::seq(maxidx, af::end));

        //! MATLAB: th_low = (max(left(:)) - min(left(:)))*0.1 + min(left(:));
        //! MATLAB: th_hi = (max(right(:)) - min(right(:)))*0.1 + min(right(:));
        float maxv = af::max<float>(left);
        float minv = af::min<float>(left);
        const float th_low = (maxv - minv) * 0.1 + minv;

        maxv = af::max<float>(right);
        minv = af::min<float>(right);
        const float th_hi = (maxv - minv) * 0.1 + minv;

        //! MATLAB: idx_low = find(left<th_low, 1, 'last');
        //! MATLAB: idx_hi = find(right<th_hi, 1) + maxidx - 1;
        int idx_low = Find1D(left < th_low, false);
        int idx_high = Find1D(right < th_hi, true) + maxidx;

        //! MATLAB: idx_low = round(max(1, idx_low - 5));
        //! MATLAB: idx_hi = round(min(length(line), idx_hi + 5));
        idx_low = std::max(0, (int)round(idx_low - 5));
        idx_high = std::min((int)(line.elements() - 1), (int)round(idx_high + 5));
        QLOG_INFO() << "MIP> idx_low=" << idx_low << " idx_hi=" << idx_high;

        af::array mip_image = af::constant(0, d0, d1);

        for (dim_t d0_idx = 0; d0_idx < d0; d0_idx++) {
            gfor(af::seq d1_idx, d1) {
                mip_image(d0_idx, d1_idx) = af::max(m_Reconimg(d0_idx, d1_idx, af::seq(idx_low, idx_high)));
            }
        }

        mip_image.eval();

        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        //into array file
        af::saveArray("image", mip_image, pPath);
        af::saveArray("res", af::constant(m_dRes3, 1, f64), pPath, true);
        af::saveArray("timestamp", af::constant(timestamp, 1, u64), pPath, true);
        saveVal("posx", pos.x, pPath, true);
        saveVal("posy", pos.y, pPath, true);
        saveVal("posz", pos.z, pPath, true);
        saveVal("posc", pos.c, pPath, true);
        saveVal("zrange_low", idx_low, pPath, true);
        saveVal("zrange_high", idx_high, pPath, true);

        af::sync();

        return true;
    }

    bool TomoBuilderInterface::SaveBFImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp) {
        char pPath[2048] = { 0, };
        strcpy_s<2048>(pPath, savePath.toLocal8Bit().constData());

        //into array file
        af::saveArray("image", m_pRawImageSet->arrData, pPath);
        af::saveArray("res", af::constant(m_dRes2, 1, f64), pPath, true);
        af::saveArray("timestamp", af::constant(timestamp, 1, u64), pPath, true);
        saveVal("posx", pos.x, pPath, true);
        saveVal("posy", pos.y, pPath, true);
        saveVal("posz", pos.z, pPath, true);
        saveVal("posc", pos.c, pPath, true);

        af::sync();

        return true;
    }

    bool TomoBuilderInterface::BuildTomogram() {
        const char* fname_out = "build_tomogram.arr";

        af::array ORytov_index, ORytov;

        m_pFieldRetrievalParam->ReleaseIntermidiate();

        TC::release();

        reportProgress(0);

        AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-Before_Tomographic_Mapping");

        try {
            af::timer foo = af::timer::start();

            if (!TomographicMapping(ORytov_index, ORytov)) return false;

            double elapsed = af::timer::stop(foo);
            QLOG_INFO() << "[" << GetDeviceIndex() << "] Elapsed for tomographic mapping - " << elapsed << "sec";
        } catch (af::exception& ex) {
            QLOG_ERROR() << "[" << GetDeviceIndex() << "] Exception while tomographic mapping... : " << ex.what();
            return false;
        } catch (std::exception& ex) {
            QLOG_ERROR() << "[" << GetDeviceIndex() << "] Exception while tomographic mapping... : " << ex.what();
            return false;
        }

        AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-After_Tomographic_Mapping");

        dbg_saveArray("arr_ORytov_index_1", ORytov_index, fname_out, false);
        dbg_saveArray("arr_ORytov_1", ORytov, fname_out, true);

        reportProgress(10);

        TC::release();

        //if (isStopRequested()) return false;

        AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-Before_Regularization");

        bool bRes = false;
        try {
            //! MATLAB: normFact=1./(res4*(res3^2));
            const float normFact = 1 / (m_dRes4 * std::pow(m_dRes3, 2));

            af::timer foo = af::timer::start();

            bRes = Regularization(ORytov_index, ORytov, normFact);

            double elapsed = af::timer::stop(foo);
            QLOG_INFO() << "[" << GetDeviceIndex() << "] Elapsed for regularization - " << elapsed << "sec";
        } catch (af::exception& ex) {
            QLOG_ERROR() << "[" << GetDeviceIndex() << "] Exception while non-negativity regularization... :" << ex.what();
            return false;
        } catch (std::exception& ex) {
            QLOG_ERROR() << "[" << GetDeviceIndex() << "] Exception while non-negativity mapping... :" << ex.what();
            return false;
        }

        AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-After_Regularization");

        dbg_saveArray("arr_ORytov_index_2", ORytov_index, fname_out, true);
        dbg_saveArray("arr_ORytov_2", ORytov, fname_out, true);

        TC::release(ORytov_index);
        TC::release(ORytov);

        m_pFieldRetrievalParam->ReleaseAll();

        TC::release();

        QLOG_TRACE() << "[" << GetDeviceIndex() << "] Available=" << AF_AVAILABLE(GetDeviceIndex()) / std::pow(1024, 2) << "MB Used=" << AF_USED(GetDeviceIndex()) / std::pow(1024, 2) << "MB";

        //reverse y-direction to match the direction of the phase image....
        m_Reconimg = af::flip(m_Reconimg, 0);
        m_Reconimg = af::flip(m_Reconimg, 1);

        dbg_saveArray("arr_m_Reconimg_1", m_Reconimg, fname_out, true);

        //crop tomogram
        if (bRes) {
            const unsigned int& ZP = m_pFieldRetrievalParam->ZP;
            const unsigned int& ZP2 = m_pFieldRetrievalParam->ZP2;

            //Crop Z
            const double crop_ratio = ZP / m_pFieldRetrieval->m_dCropSize;
            if (crop_ratio > 1) {
                const int offset = (ZP2 - std::round(ZP2 / (2 * crop_ratio)) * 2) / 2;		//always even number
                const int newZP2 = ZP2 - offset;
                const int d0_bound = (newZP2 > 30) ? 10 : 0;

                af::seq d0 = af::seq(offset + d0_bound, ZP2 - offset - d0_bound - 1);

                m_Reconimg = m_Reconimg(d0, d0, af::span);
                m_pFieldRetrievalParam->ZP2 = m_Reconimg.dims(0);
            }
        }

        //interpolate tomogram via z direction
#if 1
        if (bRes && (m_dRes4 > 0.3)) {
            AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-Before interpolation");

            const unsigned int ZP2 = m_pFieldRetrievalParam->ZP2;
            unsigned int ZP3 = m_pFieldRetrievalParam->ZP3;

            //std::cout << boost::format("ZP2=%i ZP3=%i XY=%.4f Z=%.4f") % ZP2 % ZP3 % m_dRes3 % m_dRes4 << std::endl;

            const double dof = ZP3 * m_dRes4;
            const double dof_max = 40.0;
            const bool notLimitDOF = (m_parameter[PN_DISABLE_ZLIMIT] == 1.0);

            QLOG_INFO() << "DOF=" << dof << " DOF_Max=" << dof_max << " Limit DOF=" << !notLimitDOF;

            if ((dof > dof_max) && !notLimitDOF) {
                unsigned int new_ZP3 = ZP3 * dof_max / dof;
                new_ZP3 = new_ZP3 + (new_ZP3 % 2);
                //std::cout << "new_ZP3=" << new_ZP3 << std::endl;

                const unsigned int a0 = (ZP3 - new_ZP3) / 2;
                const unsigned int a1 = ZP3 - (a0 + 1);

                m_Reconimg = m_Reconimg(af::span, af::span, af::seq(a0, a1));
                m_Reconimg.eval();

                ZP3 = new_ZP3;
                m_pFieldRetrievalParam->ZP3 = ZP3;

                //std::cout << boost::format("ZP2=%i ZP3=%i XY=%.4f Z=%.4f") % ZP2 % ZP3 % m_dRes3 % m_dRes4 << std::endl;
            }

            const double new_dRes4 = m_dRes3;	//NOTE - ZP2 = ZP 조건이므로 m_dRes3 = m_dRes2 인 상태임...
            unsigned int new_z = ZP3 * m_dRes4 / new_dRes4;
            new_z = new_z + (new_z % 2);

            //std::cout << boost::format("New ZP3=%i Z=%.4f") % new_z % new_dRes4 << std::endl;
            TC::release_fft_plans();
            TC::release();

            af::array tempArray = m_Reconimg.copy();
            AfArrayProxy proxyArray(tempArray);

            bool bInterpolated = false;
            try {
                m_Reconimg = TC::interp_xy2z(m_Reconimg, new_z);

                m_pFieldRetrievalParam->ZP2 = m_Reconimg.dims(0);
                m_pFieldRetrievalParam->ZP3 = m_Reconimg.dims(2);
                m_dRes3 = (ZP2 * m_dRes3) / m_pFieldRetrievalParam->ZP2;
                m_dRes4 = (ZP3 * m_dRes4) / m_pFieldRetrievalParam->ZP3;

                bInterpolated = true;
            } catch (std::exception& ex) {
                QLOG_ERROR() << "Can't interpolate tomogram in Z direction because of GPU memory limitation : " << ex.what();
                proxyArray.Get(m_Reconimg);
            }

            if (!bInterpolated) {    //try to interpolate XY only
                TC::release_fft_plans();
                TC::release();

                try {
                    m_Reconimg = TC::interp_xy2z(m_Reconimg, ZP3);

                    m_pFieldRetrievalParam->ZP2 = m_Reconimg.dims(0);
                    m_pFieldRetrievalParam->ZP3 = m_Reconimg.dims(2);
                    m_dRes3 = (ZP2 * m_dRes3) / m_pFieldRetrievalParam->ZP2;
                    m_dRes4 = (ZP3 * m_dRes4) / m_pFieldRetrievalParam->ZP3;
                    bInterpolated = true;
                } catch (std::exception& ex) {
                    QLOG_ERROR() << "Can't interpolate tomogram in XY direction because of GPU memory limitation : " << ex.what();
                    proxyArray.Get(m_Reconimg);
                }
            }

            //std::cout << boost::format("New ZP3=%i Z=%.4f") % m_pFieldRetrievalParam->ZP3 % m_dRes4 << std::endl;
            AF_MEMINFO(GetDeviceIndex(), "BuildTomogram-After_Interpolation");
        }

        dbg_saveArray("arr_m_Reconimg_2", m_Reconimg, fname_out, true);
#endif

        dbg_saveArray("arr_res2", af::constant<double>(m_dRes2, 1), fname_out, true);
        dbg_saveArray("arr_res3", af::constant<double>(m_dRes3, 1), fname_out, true);
        dbg_saveArray("arr_res4", af::constant<double>(m_dRes4, 1), fname_out, true);
        dbg_saveArray("arr_lambda", af::constant<double>(m_parameter[PN_LAMBDA], 1), fname_out, true);
        dbg_saveArray("arr_n_m", af::constant<double>(m_parameter[PN_NM], 1), fname_out, true);

        return bRes;
    }

    auto TomoBuilderInterface::TransferDataHT(std::any& data3d, std::any& dataMip, TC::MinMax& minMax3d,
        TC::MinMax& minMaxMip, TC::Resolution& resolution, TC::Sizes& sizes) -> void {

        auto mipImg = GenerateMipImage();

        minMax3d.minValue = static_cast<double>(af::min<float>(m_Reconimg));
        minMax3d.maxValue = static_cast<double>(af::max<float>(m_Reconimg));

        minMaxMip.minValue = static_cast<double>(af::min<float>(mipImg));
        minMaxMip.maxValue = static_cast<double>(af::max<float>(mipImg));


        m_Reconimg = af::round(m_Reconimg * 10000);
        m_Reconimg = m_Reconimg.as(u16);
        m_Reconimg = af::reorder(m_Reconimg, 1, 0, 2);

        const auto reconRawData3d = m_Reconimg.host<uint16_t>();
        data3d = std::shared_ptr<uint16_t>(reconRawData3d, TC::AfHostDeleter());

        mipImg = af::round(mipImg * 10000);
        mipImg = mipImg.as(u16);
        mipImg = af::reorder(mipImg, 1, 0);

        const auto mipRawData = mipImg.host<uint16_t>();
        dataMip = std::shared_ptr<uint16_t>(mipRawData, TC::AfHostDeleter());

        resolution.resolutionX = m_dRes3;
        resolution.resolutionY = m_dRes3;
        resolution.resolutionZ = m_dRes4;

        sizes.sizeX = static_cast<size_t>(m_Reconimg.dims(1));
        sizes.sizeY = static_cast<size_t>(m_Reconimg.dims(0));
        sizes.sizeZ = static_cast<size_t>(m_Reconimg.dims(2));
    }

    auto TomoBuilderInterface::TransferDataPhase(std::any& data2d, TC::MinMax& minMax2d, TC::Resolution& resolution,
        TC::Sizes& sizes, const double& htSizeInum) -> void {

        m_pFieldRetrievalParam->retPhase = af::reorder(m_pFieldRetrievalParam->retPhase, 1, 0, 2);
        auto& phaseImg = m_pFieldRetrievalParam->retPhase;
        
        if (htSizeInum > 0) {
            const auto phasePixelLength = phaseImg.dims(0);
            const auto phaseLengthInum = static_cast<double>(phasePixelLength)* static_cast<double>(m_dRes2);

            if (htSizeInum < phaseLengthInum) {
                const auto fovdiff = phaseLengthInum - htSizeInum;
                auto ofs = static_cast<int32_t>(fovdiff / static_cast<double>(m_dRes2) / 2);
                ofs = ofs + (ofs % 2);

                const auto cropBeginPoint = static_cast<double>(ofs);
                const auto cropEndPoint = static_cast<double>(phasePixelLength - static_cast<dim_t>(ofs) - 1);

                const auto boundary = af::seq(cropBeginPoint, cropEndPoint);

                phaseImg = phaseImg(boundary, boundary).as(f32);
            }
        }

        const auto reconRawData2d = phaseImg.host<float>();
        data2d = std::shared_ptr<float>(reconRawData2d, TC::AfHostDeleter());
        minMax2d.minValue = static_cast<float>(af::min<float>(phaseImg));
        minMax2d.maxValue = static_cast<float>(af::max<float>(phaseImg));

        resolution.resolutionX = m_dRes2;
        resolution.resolutionY = m_dRes2;

        sizes.sizeX = static_cast<size_t>(m_pFieldRetrievalParam->retPhase.dims(1));
        sizes.sizeY = static_cast<size_t>(m_pFieldRetrievalParam->retPhase.dims(0));
        sizes.sizeZ = static_cast<size_t>(m_pFieldRetrievalParam->retPhase.dims(2));
    }

    auto TomoBuilderInterface::TransferDataBrightField(std::any& data3d, TC::Resolution& resolution,
        TC::Sizes& sizes) -> void {
        auto brightFieldData = m_pRawImageSet->arrData.as(u8);
        brightFieldData = af::reorder(brightFieldData, 1, 0, 2);

        const auto reconRawData3d = brightFieldData.host<uint8_t>();
        data3d = std::shared_ptr<uint8_t>(reconRawData3d, TC::AfHostDeleter());

        const auto resolutionXY = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
        resolution.resolutionX = resolutionXY;
        resolution.resolutionY = resolutionXY;

        sizes.sizeX = brightFieldData.dims(0);
        sizes.sizeY = brightFieldData.dims(1);
    }

    bool TomoBuilderInterface::TomographicMapping(af::array& ORytov_index, af::array& ORytov) {
        char fname_in[128] = { 0, };
        const char* fname_out = "tomographic_mapping.arr";

        const double lambda = m_parameter[PN_LAMBDA];
        const double n_m = m_parameter[PN_NM];
        const double NA = m_parameter[PN_NA];
        const unsigned int iterNumber = m_parameter[PN_ITERATION];
        const double mapping_sign = m_parameter[PN_MAPPGIN_SIGN];

        const double res = m_pFieldRetrievalParam->res;

        af::array& retPhase = m_pFieldRetrievalParam->retPhase;
        af::array& retAmplitude = m_pFieldRetrievalParam->retAmplitude;
        af::array& f_dx = m_pFieldRetrievalParam->f_dx;
        af::array& f_dy = m_pFieldRetrievalParam->f_dy;
        auto& fieldQuality = m_pFieldRetrievalParam->fieldQuality;
        auto& residues = m_pFieldRetrievalParam->residues;
        const auto ignoreFieldEnabled = [=]() {
            if (misc.m_bEnable) {
                return misc.m_bApplyFilter;
            } else {
                return (m_parameter[PN_IGNORE_POOR] != 0);
            }
        }();
        const auto ignoreCount = [=]() {
            const auto mode = static_cast<int>(m_parameter[PN_IGNORE_POOR]);
            switch (mode) {
            case 1:
                return 5;
            case 2:
                return 1000; //all
            default:
                return 0;
            }
        }();

        typedef std::function<bool(std::pair<int, int>, std::pair<int, int>)> Comparator;

        Comparator compFunctor = [](std::pair<int, int> elem1, std::pair<int, int> elem2) {
            return elem1.second > elem2.second;
        };

        std::set<std::pair<int, int>, Comparator> orderedResidueCount(residues.begin(), residues.end(), compFunctor);

        auto inRange = [=](const int residue, const int nthIndex) {
            auto index = 0;
            for (const auto element : orderedResidueCount) {
                if (index >= nthIndex) return false;
                if (residue >= element.second) return true;
                index++;
            }
            return false;
        };

        auto ignoreField = [=](const int residue) {
            if (!ignoreFieldEnabled) return false;
            if (misc.m_bEnable) {
                if (residue < misc.m_nResidueCount) return false;
                else return inRange(residue, misc.m_nMostCount);
            } else {
                if (residue < 150) return false;
                else return inRange(residue, ignoreCount);
            }
        };

        const unsigned int idx_offset = m_pFieldRetrievalParam->idx_offset;

        dbg_saveArray("arr_retPhase", retPhase, fname_out, false);
        dbg_saveArray("arr_Amplitude", retAmplitude, fname_out, true);
        dbg_saveArray("arr_fdx_1", f_dx, fname_out, true);
        dbg_saveArray("arr_fdy_1", f_dy, fname_out, true);

        //! MATLAB: [xSize, ySize, thetaSize]=size(retPhase);
        const unsigned int xSize = retPhase.dims(0);
        const unsigned int ySize = retPhase.dims(1);
        const unsigned int thetaSize = retPhase.dims(2);

        //! MATLAB: original_size=rawSize; crop_size=ySize;
        const unsigned int crop_size = m_dCropSize;

        //! MATLAB: crop_factor=crop_size/original_size;
        const double crop_factor = m_dCropFactor;

        //! MATLAB: res2=res/crop_factor;
        const double res2 = m_dRes2;

        const unsigned int ZP = m_pFieldRetrievalParam->ZP;
        const unsigned int ZP2 = m_pFieldRetrievalParam->ZP2;
        const unsigned int ZP3 = m_pFieldRetrievalParam->ZP3;

        //! MATLAB: padd_factor=ZP/crop_size;
        const double padd_factor = ZP / (1.0 * crop_size);

        //! MATLAB: kres=1/(res*ZP)*crop_factor;
        const double kres = 1 / (res * ZP) * crop_factor;

        //! MATLAB: kresz=1/(res4*ZP3);
        const double kresz = 1 / (m_dRes4 * ZP3);

        std::unique_ptr<float[]> shift_dim0, shift_dim1;
        std::unique_ptr<float[]> k0_x_kk, k0_y_kk, k0_z_kk;

        //TOMV-234 to clear intermidate memory at the end of scope
        if (1) {
            //! MATLAB: f_dx=f_dx-(f_dx(1)+f_dx(round(thetaSize/2)))/2;
            //! MATLAB: f_dy=f_dy-(f_dy(1)+f_dy(round(thetaSize/2)))/2;
            if (m_pFieldRetrievalParam->hasNormalSample) {
                af::array f_dx_normal = f_dx(0);
                af::array f_dy_normal = f_dy(0);

                f_dx = f_dx - f_dx_normal.scalar<float>();
                f_dy = f_dy - f_dy_normal.scalar<float>();
            } else {
                const unsigned int thetaSizeHalf = std::round(thetaSize / 2.0);
                af::array f_dx_temp = f_dx(0) + f_dx(thetaSizeHalf - 1);
                af::array f_dy_temp = f_dy(0) + f_dy(thetaSizeHalf - 1);
                f_dx = f_dx - (f_dx_temp.scalar<float>() / 2.0);
                f_dy = f_dy - (f_dy_temp.scalar<float>() / 2.0);
            }

            dbg_saveArray("arr_f_dx_2", f_dx, fname_out, true);
            dbg_saveArray("arr_f_dy_2", f_dy, fname_out, true);

            //! MATLAB: f_dx=f_dx*padd_factor;f_dy=f_dy*padd_factor;
            f_dx = f_dx * padd_factor;
            f_dy = f_dy * padd_factor;

            dbg_saveArray("arr_f_dx_3", f_dx, fname_out, true);
            dbg_saveArray("arr_f_dy_3", f_dy, fname_out, true);

            //! MATLAB: k0_x=MappingSign*kres*f_dx;
            //! MATLAB: k0_y=MappingSign*kres*f_dy;
            af::array k0_x = mapping_sign * f_dx * kres;
            af::array k0_y = mapping_sign * f_dy * kres;

            //printf("padd_factor=%f kres=%f\n", padd_factor, kres);

            //! MATLAB: k0=1/lambda;
            const double k0 = 1 / lambda;

            //! MATLAB: k0_z=real(sqrt((n_m*k0)^2-(k0_x).^2-(k0_y).^2));
            af::array k0_z, dummy;
            //sqrt(k0_z, dummy, (std::pow(n_m*k0, 2) - af::pow(k0_x, 2) - af::pow(k0_y, 2)));
            k0_z = sqrt_real(std::pow(n_m * k0, 2) - af::pow(k0_x, 2) - af::pow(k0_y, 2));

            //printf("res3=%f\n", res3);
            dbg_saveArray("arr_k0_x", k0_x, fname_out, true);
            dbg_saveArray("arr_k0_y", k0_y, fname_out, true);
            dbg_saveArray("arr_k0_z", k0_z, fname_out, true);

            //! MATLAB: ORytov=gpuArray(zeros(ZP2,ZP2,ZP3,'single'));
            ORytov = af::constant(0, ZP2, ZP2, ZP3, c32);

            //! MATLAB: Count=gpuArray(zeros(ZP2,ZP2,ZP3,'single'));
            af::array Count = af::constant(0, ZP2, ZP2, ZP3);

            unsigned int retPhaseSize = thetaSize;

            const unsigned int padSizeRow = (ZP > xSize) ? round((ZP - xSize) / 2) : 0;
            const unsigned int padSizeCol = (ZP > ySize) ? round((ZP - ySize) / 2) : 0;

            QLOG_INFO() << "padSizeRow=" << padSizeRow << " padSizeCol" << padSizeCol;

            af::array FRytov = af::constant(0, xSize, ySize, c32);
            af::array FRytov2 = af::constant(0, xSize + padSizeRow * 2, ySize + padSizeCol * 2, c32);

            //! MATLAB: xr=(ZP*res2*NA/lambda);
            const float xr = (ZP * res2 * NA) / lambda;
            af::array mk_ellipse_not = !(mk_ellipse(xr, xr, ZP, ZP));
            dbg_saveArray("arr_xr", af::constant<float>(xr, 1, f32), fname_out, true);
            dbg_saveArray("arr_mk_ellipse_not", mk_ellipse_not, fname_out, true);

            af::array f_dx_rounded = af::round(f_dx);
            af::array f_dy_rounded = af::round(f_dy);

            dbg_saveArray("arr_f_dx_rounded", f_dx_rounded, fname_out, true);
            dbg_saveArray("arr_f_dy_rounded", f_dy_rounded, fname_out, true);

            shift_dim0.reset(new float[retPhaseSize]);
            shift_dim1.reset(new float[retPhaseSize]);

            f_dx_rounded.eval();
            f_dy_rounded.eval();

            f_dx_rounded.host(shift_dim0.get());
            f_dy_rounded.host(shift_dim1.get());

            //! MATLAB: Kx=kx-k0_x(kk);Ky=ky-k0_y(kk);Kz=kz-k0_z(kk);
            k0_x_kk.reset(new float[retPhaseSize]);
            k0_y_kk.reset(new float[retPhaseSize]);
            k0_z_kk.reset(new float[retPhaseSize]);

            k0_x.eval();
            k0_y.eval();
            k0_z.eval();

            k0_x.host(k0_x_kk.get());
            k0_y.host(k0_y_kk.get());
            k0_z.host(k0_z_kk.get());

            //! MATLAB: [ky kx]=meshgrid(kres*(-floor(ZP/2)+1:floor(ZP/2)),kres*(-floor(ZP/2)+1:floor(ZP/2)));
            af::array kx, ky, kz;
            const float seq0 = std::floor(ZP / 2.0);
            const unsigned int seqLen = 2 * floor(ZP / 2.0);
            af::array xgv = af::seq(0, seqLen - 1);
            xgv = kres * (xgv - seq0 + 1);

            meshgrid_square(ky, kx, xgv, xgv); //TOMV-344

            dbg_saveArray("arr_xgv", xgv, fname_out, true);
            dbg_saveArray("arr_kx", kx, fname_out, true);
            dbg_saveArray("arr_ky", ky, fname_out, true);

            //! MATLAB: kz=real(sqrt((n_m*k0)^2-kx.^2-ky.^2));
            //sqrt(kz, dummy, (std::pow(n_m*k0, 2) - af::pow(kx, 2) - af::pow(ky, 2)));
            kz = sqrt_real(std::pow(n_m * k0, 2) - af::pow(kx, 2) - af::pow(ky, 2));

            dbg_saveArray("arr_kz", kz, fname_out, true);

            for (unsigned int kk = 0; kk < retPhaseSize; kk++) {
#ifdef DEEP_DEBUG
                sprintf_s<128>(fname_in, "loop_%02d.arr", kk);
                QLOG_TRACE() << "fname_in = " << fname_in;
#endif
                if (ignoreField(residues[kk])) {
                    QLOG_TRACE() << "Field (#" << kk + 1 << "/" << retPhaseSize << ") is ignored. (score=" << residues[kk] << ")";
                    continue;
                }

                af::timer foo = af::timer::start();
                double prev_time = af::timer::stop(foo);

                //! MATLAB:  FRytov=squeeze(log(retAmplitude(:,:,kk))+1i*retPhase(:,:,kk));
                FRytov = af::complex(af::log(retAmplitude(af::span, af::span, kk)), retPhase(af::span, af::span, kk));
                dbg_saveArray("arr_FRytov_1", FRytov, fname_in, false);

                //QLOG_INFO() << "padSizeRow=" << padSizeRow << " padSizeCol=" << padSizeCol;

                if ((padSizeRow > 0) && (padSizeCol > 0)) {
                    af::array mean_fr = af::mean(FRytov(0, af::span));
                    af::array mean_lr = af::mean(FRytov(af::end, af::span));
                    af::array mean_fc = af::mean(FRytov(af::span, 0));
                    af::array mean_lc = af::mean(FRytov(af::span, af::end));

                    af::array padValArr = (ySize * (mean_fr + mean_lr) + xSize * (mean_fc + mean_lc)) / (2 * (xSize + ySize) - 4);
                    dbg_saveArray("arr_padValArr", padValArr, fname_in, true);

                    padValArr.eval();

                    //const unsigned int padSizeRow = round((ZP - xSize) / 2);
                    //const unsigned int padSizeCol = round((ZP - ySize) / 2);

                    af::cfloat padVal = padValArr.scalar<af::cfloat>();
                    FRytov2 = padarray<af::cfloat>(FRytov, padSizeRow, padSizeCol, padVal);
                    //FRytov2 = FRytov2(af::seq(0, ZP-1), af::seq(0, ZP-1));
                    dbg_saveArray("arr_FRytov_2", FRytov2, fname_in, true);
                } else {
                    FRytov2 = FRytov;
                }

                //! MATLAB: UsRytov=fftshift(fft2(ifftshift(FRytov))).*(res2)^2;        //TOMV-1506
                af::array UsRytov = fftshift(af::fft2(ifftshift(FRytov2))) * std::pow(res2, 2);

                //printf("res2=%f\n", res2);
                dbg_saveArray("arr_UsRytov_1", UsRytov, fname_in, true);

                //! MATLAB: UsRytov=circshift(UsRytov,MappingSign*[round(f_dx(kk)) round(f_dy(kk))]);
                const int shift_d0 = mapping_sign * shift_dim0.get()[kk + idx_offset];
                const int shift_d1 = mapping_sign * shift_dim1.get()[kk + idx_offset];
                UsRytov = af::shift(UsRytov, shift_d0, shift_d1);

                dbg_saveArray("arr_shift_d0", af::constant<int>(shift_d0, 1), fname_in, true);
                dbg_saveArray("arr_shift_d1", af::constant<int>(shift_d1, 1), fname_in, true);
                dbg_saveArray("arr_UsRytov_2", UsRytov, fname_in, true);

                //! MATLAB: xr=(ZP*res2*NA/lambda); !==> moved to outside of loop

                if (m_bUseDecompositionFilter) {
                    //! MATLAB: Hfilter=ones(ZP,ZP);
                    af::array Hfilter = af::constant(1.0, ZP, ZP);
                    if (kk > 0) {
                        //! MATLAB: Hfilter=circshift(mk_ellipse(round(5*padd_factor),round(5*padd_factor),ZP,ZP),(-1*MappingSign)*[round(f_dx(kk)) round(f_dy(kk))]);
                        Hfilter = af::shift(mk_ellipse(round(6 * padd_factor), round(6 * padd_factor), ZP, ZP), (-1) * shift_d0, (-1) * shift_d1);
                    }

                    //! MATLAB:  UsRytov=UsRytov.*~mk_ellipse(xr,xr,ZP,ZP).*Hfilter;
                    UsRytov = UsRytov * mk_ellipse_not * Hfilter;
                    dbg_saveArray("arr_UsRytov_3", UsRytov, fname_in, true);
                } else {
                    //! MATLAB:  UsRytov=UsRytov.*~mk_ellipse(xr,xr,ZP,ZP);
                    UsRytov = UsRytov * mk_ellipse_not;
                    dbg_saveArray("arr_UsRytov_3", UsRytov, fname_in, true);
                }

                //! MATLAB: [ky kx]=meshgrid(kres*(-floor(ZP/2)+1:floor(ZP/2)),kres*(-floor(ZP/2)+1:floor(ZP/2)));	//==> move to outside of loop
                //! MATLAB: kz=real(sqrt((n_m*k0)^2-kx.^2-ky.^2));	//==> move to outside of loop


                //! MATLAB: Kx=kx-k0_x(kk);Ky=ky-k0_y(kk);Kz=kz-k0_z(kk);

                af::array Kx = kx - k0_x_kk.get()[kk + idx_offset];
                af::array Ky = ky - k0_y_kk.get()[kk + idx_offset];
                af::array Kz = kz - k0_z_kk.get()[kk + idx_offset];

                // Avoid FMAD because it loses precision
                Kx.eval();
                Ky.eval();
                Kz.eval();

                dbg_saveArray("arr_Kx_1", Kx, fname_in, true);
                dbg_saveArray("arr_Ky_1", Ky, fname_in, true);
                dbg_saveArray("arr_Kz_1", Kz, fname_in, true);

                //! MATLAB: Uprime=1i.*2*2*pi*kz.*UsRytov;   //TOMV-1506
                //af::array Uprime = af::complex(0, (2 * 2 * af::Pi * kz)) * UsRytov;
                //dbg_saveArray("arr_Uprime_1", Uprime, fname_in, true);

                //! MATLAB: xind=find((kz>0).*~mk_ellipse(xr,xr,ZP,ZP)...
                af::array cond0 = (kz > 0);
                af::array cond1 = (Kx > (kres * (-floor(ZP2 / 2) + 1)));
                af::array cond2 = (Ky > (kres * (-floor(ZP2 / 2) + 1)));
                af::array cond3 = (Kz > (kresz * (-floor(ZP3 / 2) + 1)));
                af::array cond4 = (Kx < (kres * (floor(ZP2 / 2))));
                af::array cond5 = (Ky < (kres * (floor(ZP2 / 2))));
                af::array cond6 = (Kz < (kresz * (floor(ZP3 / 2))));

                af::array allconds = cond0 * mk_ellipse_not * cond1 * cond2 * cond3 * cond4 * cond5 * cond6;

                dbg_saveArray("arr_allconds", allconds, fname_in, true);

                if (af::sum<int>(allconds) == 0) {
                    //std::cout << "kk=" << kk << std::endl;
                    continue;
                }

                //#af::array xind = af::where(allconds);
                af::array xind = (allconds == 1);
                dbg_saveArray("arr_xind", xind, fname_in, true);

                //! MATLAB:  Uprime=(kz/1i).*UsRytov;  //TOMV-1506
                //                 = (-1i*kz).*UsRytov;
                af::array Uprime = af::complex(0, -kz) * UsRytov;

                //! MATLAB:  Uprime=Uprime(xind);
                //#Uprime = Uprime(xind);
                Uprime = xind * Uprime;
                dbg_saveArray("arr_Uprime_2", Uprime, fname_in, true);

                //! MATLAB:  Kx=Kx(xind);
                //! MATLAB:  Ky=Ky(xind);
                //! MATLAB:  Kz=Kz(xind);
                //! MATLAB: Kx=round(Kx/kres+ZP2/2);Ky=round(Ky/kres+ZP2/2);Kz=round(Kz/kresz+ZP3/2);    
                Kx = af::round(Kx / kres + ZP2 / 2) * xind;
                Ky = af::round(Ky / kres + ZP2 / 2) * xind;
                Kz = af::round(Kz / kresz + ZP3 / 2) * xind;

                dbg_saveArray("arr_Kx_2", Kx, fname_in, true);
                dbg_saveArray("arr_Ky_2", Ky, fname_in, true);
                dbg_saveArray("arr_Kz_2", Kz, fname_in, true);

                //! MATLAB: Kzp=(Kz-1)*ZP2^2+(Ky-1)*ZP2+Kx;
#if 1
                af::array Kzu = Kz.as(u32);
                af::array Kyu = Ky.as(u32);
                af::array Kxu = Kx.as(u32);
                af::array Kzp = (Kzu - 1) * (ZP2 * ZP2) + (Kyu - 1) * ZP2 + Kxu;
#else
    //single precision으로 계산 시 결과 이상 현상 있음 - 20160324
                af::array Kzp = (Kz - 1) * (ZP2 * ZP2) + (Ky - 1) * ZP2 + Kx;
#endif
                Kzp = Kzp * xind;
                dbg_saveArray("arr_Kzp_1", Kzp, fname_in, true);

                //! MATLAB: [trash, idx]=unique(Kzp,'first');
                af::array idx = uniqueIdx(Kzp);
                dbg_saveArray("arr_idx", idx, fname_in, true);

                //! MATLAB: Kzp=shiftdim(Kzp(sort(idx)),1);
                Kzp = Kzp(idx); // No need to sort all subsequent indexing forllows same order
                Kzp = af::transpose(Kzp);

                //! MATLAB: Uprime=Uprime(sort(idx))';
                Uprime = Uprime(idx);
                Uprime = af::transpose(Uprime, true);

                dbg_saveArray("arr_Kzp_2", Kzp, fname_in, true);
                dbg_saveArray("arr_Uprime_3", Uprime, fname_in, true);

                //NOTE - Index difference compensation....
                af::array Kzp2 = Kzp - 1;

                //! MATLAB: temp=ORytov(Kzp);
                af::array temp = af::transpose(ORytov(Kzp2));
                dbg_saveArray("arr_temp", temp, fname_in, true);

                //! MATLAB: ORytov(Kzp)=temp+Uprime;
                af::array temp2 = temp + Uprime;
                ORytov(Kzp2) = af::transpose(temp2);

                dbg_saveArray("arr_temp2", temp2, fname_in, true);
                dbg_saveArray("arr_temp2_tranpose", af::transpose(temp2), fname_in, true);
                dbg_saveArray("arr_ORytov", ORytov, fname_in, true);

                //! MATLAB: Count(Kzp)=Count(Kzp)+1;
                Count(Kzp2) = Count(Kzp2) + 1;

                dbg_saveArray("arr_Count", Count, fname_in, true);
            }

            dbg_saveArray("arr_Count", Count, fname_out, true);
            dbg_saveArray("arr_ORytov_1", ORytov, fname_out, true);

            //! MATLAB: ORytov(Count>0)=ORytov(Count>0)./Count(Count>0)/(res3^2*res4);        //TOMV-1506
            ORytov(Count > 0) = ORytov(Count > 0) / Count(Count > 0) / (std::pow(m_dRes3, 2) * m_dRes4);
            ORytov.eval();

            dbg_saveArray("arr_ORytov_2", ORytov, fname_out, true);
        }

        TC::release(); //TOMV-234

#if 0
  //! MATLAB: Reconimg=ifftn(ifftshift(ORytov))./(res4*(res3^2));
        if (1) {
            af::array ORtytov_shifted_ifft;

            //TOMV-234 to clear intermidate memory at the end of scope
            if (1) {
                af::array ORytov_shifted = ifftshift3(ORytov);

                dbg_saveArray("arr_ORytov_shifted", ORytov_shifted, fname_out, true);

                ORtytov_shifted_ifft = af::ifft3(ORytov_shifted);
                ORtytov_shifted_ifft.eval();

                dbg_saveArray("ORtytov_shifted_ifft", ORtytov_shifted_ifft, fname_out, true);
            }

            TC::release(); //TOMV-234

            const double constVal = m_dRes4 * std::pow(m_dRes3, 2);
            m_Reconimg = ORtytov_shifted_ifft / constVal;
            m_Reconimg.eval();
        }
#else
        if (1) {
            //! MATLAB: Reconimg=fftshift(ifftn(ifftshift(ORytov)));	    //TOMV-1506		
            //m_Reconimg = ifftshift3(ORytov);
            _ifftshift3(ORytov, m_Reconimg);
            m_Reconimg.eval();

            dbg_saveArray("arr_ORytov_shifted", m_Reconimg, fname_out, true);

            af::ifft3InPlace(m_Reconimg);
            dbg_saveArray("ORtytov_shifted_ifft", m_Reconimg, fname_out, true);

            m_Reconimg = fftshift3(m_Reconimg);
        }

        TC::release_fft_plans();

        m_Reconimg.eval();
        TC::release();

        dbg_saveArray("arr_Reconimg_final", m_Reconimg, fname_out, true);

#endif

        AF_MEMINFO(GetDeviceIndex(), "TomographicMapping");

        //! MATLAB: ORytov_index=((abs(ORytov)==0));
        ORytov_index = (af::abs(ORytov) == 0);
        dbg_saveArray("arr_ORytov_index", ORytov_index, fname_out, true);

        ORytov_index.eval();

        return true;
    }

    void TomoBuilderInterface::reportProgress(int progress) {
        m_pProgress->notify(progress);
    }

    DMDPatternGroupType TomoBuilderInterface::GetPatternGroup(DMDPatternType type) {
        DMDPatternGroupType group = PATTERN_GROUP_UNKNOWN;

        const int nType = static_cast<int>(type);
        if ((nType >= 1000) && (nType <= 1999)) group = PATTERN_GROUP_LEGACY;
        else if ((nType >= 2000) && (nType <= 2999)) group = PATTERN_GROUP_SIM;
        else if ((nType >= 3000) && (nType <= 3999)) group = PATTERN_GROUP_LEE_HOLOGRAMM;

        return group;
    }
}