#define LOGGER_TAG "TCHoloprocessor::TomoBuilderFieldRetrievalMulti"
#include <TCLogger.h>

#include "TomoBuilderGlobal.h"
#include "TomoBuilderUtility.h"
#include "TomoBuilderFieldRetrievalMulti.h"

#define CROP_AFTER_DECOMPOSITION 0

TC::TomoBuilderFieldRetrievalMulti::TomoBuilderFieldRetrievalMulti(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam)
    : TomoBuilderFieldRetrievalInterface(param, pBgImageSet, pRawImageSet, pFieldRetrievalParam) {
}

bool TC::TomoBuilderFieldRetrievalMulti::FieldRetrievalBgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool miscCallibrationOnly, bool ignorePhaseSign) {
    const char* fname_out = "field_retrieval_bg_multi.arr";

    const double pixelSize = m_parameter[PN_PIXEL_SIZE];
    const double mag = m_parameter[PN_M];
    const double res = pixelSize / mag;

    m_pFieldRetrievalParam->res = res;

    af::array& bg_data = m_pBgImageSet->arrData;

    dbg_saveArray("arr_bg_data", bg_data, fname_out, false);

    const unsigned int rawSize = m_pBgImageSet->width();
    m_pFieldRetrievalParam->rawSize = rawSize;

    m_pFieldRetrievalParam->hasNormalSample = true;
    m_pFieldRetrievalParam->idx_offset = 0;

    //! MATLAB: [bgField] = FieldDecomposition(bg_data, NA, res, lambda);
    af::array& Fbg = m_pFieldRetrievalParam->Fbg;
    Fbg = Decomposition(bg_data, 0);

    dbg_saveArray("arr_Fbg", Fbg, fname_out, true);

    Fbg.eval();

    m_pBgImageSet->clear();

    return true;
}

bool TC::TomoBuilderFieldRetrievalMulti::FieldRetrievalFgOnly(bool hasNormal, bool onlyNormalOnBg, int zeroPadding, bool ignorePhaseSign, bool checkQuality) {
    const char* fname_out = "field_retrieval_fg_multi.arr";

    const double PhaseSign = m_parameter[PN_PHASE_SIGN];
    const double res = m_pFieldRetrievalParam->res;

    af::array& raw_data = m_pRawImageSet->arrData;
    af::array& bgField = m_pFieldRetrievalParam->Fbg;

    dbg_saveArray("arr_raw_data", raw_data, fname_out, false);
    dbg_saveArray("arr_bgField", bgField, fname_out, true);

    //! MATLAB: [fgField] = FieldDecomposition(raw_data, NA, res, lambda);
    af::array fgField = Decomposition(raw_data, 1);

    dbg_saveArray("arr_fgField", fgField, fname_out, true);

    //! MATLAB: [sx, sy, sz] = size(fgField);
    const unsigned int sx = fgField.dims(0);
    const unsigned int sy = fgField.dims(1);
    const unsigned int sz = fgField.dims(2);

    //std::cout << "sx=" << sx << " sy=" << sy << " sz=" << sz << std::endl;

    //! MATALB: f_dx = zeros(sz, 1);
    //! MATLAB: f_dy = zeros(sz, 1);
    m_pFieldRetrievalParam->f_dx = af::constant(0, sz);
    m_pFieldRetrievalParam->f_dy = af::constant(0, sz);

    af::array& f_dx = m_pFieldRetrievalParam->f_dx;
    af::array& f_dy = m_pFieldRetrievalParam->f_dy;

    //! MATLAB: retPhase = zeros(sx, sy, sz, 'single');
    //! MATLAB: retAmplitude = zeros(sx, sy, sz, 'single');
    m_pFieldRetrievalParam->retPhase = af::constant(0, sx, sy, sz);
    m_pFieldRetrievalParam->retAmplitude = af::constant(0, sx, sy, sz);

    af::seq seq_d0 = af::seq(4, 14);
    af::seq seq_d1 = af::seq(4, 14);

    af::array& retPhase = m_pFieldRetrievalParam->retPhase;
    af::array& retAmplitude = m_pFieldRetrievalParam->retAmplitude;

    for (unsigned int iter = 0; iter < sz; iter++) //! MATLAB:for iter = 1:sz
    {
        //! MATLAB: Fimg = squeeze(fgField(:, : , iter));
        af::array Fimg = fgField(af::span, af::span, iter);

        dbg_saveArrayIdx("arr_Fimg_1", iter, Fimg, fname_out, true);

        //! MATLAB: % use normal image only
        //! MATLAB: if UseNormal == 1
        //! MATLAB:   Fbg = squeeze(bgField(:, : ));
        //! MATLAB:   Fimg = Fimg. / Fbg;
        //! MATLAB: else
        //! MATLAB: 	Fbg = squeeze(bgField(:, : , iter));
        //! MATLAB:   Fimg = Fimg. / Fbg;
        //! MATLAB: end
        const int fbgIdx = (onlyNormalOnBg) ? 0 : iter;
        af::array Fbg = bgField(af::span, af::span, fbgIdx);
        Fimg = Fimg / Fbg;

        dbg_saveArrayIdx("arr_Fbg", iter, Fbg, fname_out, true);
        dbg_saveArrayIdx("arr_Fimg_2", iter, Fimg, fname_out, true);

        //! MATLAB:   Fresult2d = fftshift(fft2(ifftshift(Fbg)));
        af::array Fresult2d = fftshift(af::fft2(ifftshift(Fbg)));
        dbg_saveArrayIdx("arr_Fresult2d", iter, Fresult2d, fname_out, true);

        //! MATLAB:   [f_x, f_y] = find(Fresult2d == max(max(Fresult2d)), 1);
        //! MATLAB:   f_dx(iter) = f_x;
        //! MATLAB:   f_dy(iter) = f_y;
        af::cfloat Fresult2dMax = af::max<af::cfloat>(Fresult2d(af::span, af::span));

        unsigned int f_x, f_y;
        Find(Fresult2d == Fresult2dMax, f_x, f_y);
        f_dx(iter) = f_x;
        f_dy(iter) = f_y;

        dbg_saveArrayIdx("arr_f_x", iter, af::constant<unsigned int>(f_x, 1), fname_out, true);
        dbg_saveArrayIdx("arr_f_y", iter, af::constant<unsigned int>(f_y, 1), fname_out, true);

        //! MATLAB:   [f_x, f_y] = find(Fresult2d == max(max(Fresult2d)), 1);

        //! MATLAB: retAmplitude(:, : , iter) = abs(Fimg);
        af::array FimgAbs = af::abs(Fimg);
        retAmplitude(af::span, af::span, iter) = FimgAbs;

        dbg_saveArrayIdx("arr_Fimg_abs", iter, FimgAbs, fname_out, true);

        //! MATLAB: ptemp = (PhaseSign)*(PhiShift(unwrap2(double(angle(Fimg)))));
        bool bGoodQuality = false;
        int score = 0;
        af::array ptemp = PhaseSign * (TC::PhiShift(unwrap2(angle(Fimg), bGoodQuality, score)));

        dbg_saveArrayIdx("arr_ptemp_1", iter, ptemp, fname_out, true);

        if (iter == 0) {
            //! MATLAB: phase_offset = 0.5;
            const float phase_offset = 0.5;

            //! MATLAB: minv = min(min(ptemp(10:end-10,10:end-10)));
            float minv;
            unsigned int minidx;

            TC::Min(ptemp(af::seq(10, af::end - 10), af::seq(10, af::end - 10)), minv, minidx);

            //! MATLAB: ptemp_mask = zeros(sx, sy);
            af::array ptemp_mask = af::constant(0, sx, sy);

            //! MATLAB: ptemp_mask_inner = zeros(sx, sy);
            //! MATLAB: ptemp_mask_inner(10:end - 10, 10 : end - 10) = 1;
            af::array ptemp_mask_inner = af::constant(0, sx, sy);
            ptemp_mask_inner(af::seq(10, af::end - 10), af::seq(10, af::end - 10)) = 1;

            //! MATLAB: ptemp_mask(ptemp<(minv + phase_offset)) = 1;
            ptemp_mask(ptemp < (minv + phase_offset)) = 1;

            //! MATLAB: se = strel('disk', 10);
            af::array se = TC::strel_disk(10);

            //! MATLAB: ptemp_mask = imdilate(ptemp_mask, se);
            ptemp_mask = af::dilate(ptemp_mask, se);

            //! MATLAB: se = strel('disk', 20);
            //! MATLAB: ptemp_mask = imerode(ptemp_mask, se);
            ptemp_mask = af::erode(ptemp_mask, se);    //! CUDA Support only upto 19 x 19 mask
            ptemp_mask = af::erode(ptemp_mask, se);    //! so, do it twice...

            //! MATLAB: ptemp_mask = ptemp_mask.*ptemp_mask_inner;
            ptemp_mask = ptemp_mask * ptemp_mask_inner;

            //! MATLAB: se = strel('square', 10);
            //se = TC::strel_square(10);

            //! MATLAB: ptemp_mask = imerode(ptemp_mask, se);
            ptemp_mask = af::erode(ptemp_mask, se);
            ptemp_mask.eval();

            //! MATLAB: [r, c] = find(ptemp_mask == 1);
            unsigned int row, col;
            if (TC::Find(ptemp_mask, row, col)) {
                QLOG_INFO() << "row=" << row << ", col=" << col;

                const unsigned int mw = 5;

                //check boundary condition...
                if ((row >= mw) && (col >= mw) && (row < (sx - mw)) && (col < (sy - mw))) {
                    //! MATLAB: mean_range = [r(1) - 5, r(1) + 5, c(1) - 5, c(1) + 5];
                    seq_d0 = af::seq(row - 5, row + 5);
                    seq_d1 = af::seq(col - 5, col + 5);
                }
            } else {
                QLOG_WARN() << "Failed to find background area.";
            }
        }

        //! MATLAB: ptemp = ptemp - mean2(ptemp(5:15, 5 : 15));
        const float ptemp_mean = af::mean<float>(ptemp(seq_d0, seq_d1));
        ptemp = ptemp - ptemp_mean;

        dbg_saveArrayIdx("arr_ptemp_mean", iter, af::constant<float>(ptemp_mean, 1), fname_out, true);
        dbg_saveArrayIdx("arr_ptemp_2", iter, ptemp, fname_out, true);

        //! MATLAB: retPhase(:, : , iter) = ptemp;
        retPhase(af::span, af::span, iter) = ptemp;
    } //! MATLAB: end

    retAmplitude.eval();
    retPhase.eval();
    f_dx.eval();
    f_dy.eval();

    dbg_saveArray("arr_retAmplitude", retAmplitude, fname_out, true);
    dbg_saveArray("arr_retPhase", retPhase, fname_out, true);
    dbg_saveArray("arr_f_dx", f_dx, fname_out, true);
    dbg_saveArray("arr_f_dy", f_dy, fname_out, true);

    return true;
}

af::array TC::TomoBuilderFieldRetrievalMulti::Decomposition(const af::array& images, const int tag) {
#undef max
    char fname_in[128] = { 0, };
    const char* fname_out = (tag == 0) ? "decomposition_bg.arr" : "decomposition_fg.arr";

    //! MATLAB: numfig = 2;
    const int numfig = 2;

    //! MATLAB: N = size(images, 3);
    const int N = images.dims(2);

    dbg_saveArray("arr_N", af::constant<int>(N, 1), fname_out, false);

    //! MATLAB: meanval = zeros(N, 1);
    //! MATLAB: for kk = 1:1 : N
    //! MATLAB: 	meanval(kk) = mean2(squeeze(images(:, : , kk)));
    //! MATLAB: end
    //! MATLAB: [~, maxind] = max(diff([meanval; meanval(1)]));
    //! MATLAB: figInd = circshift((1:1 : N).',-maxind);
    std::shared_ptr<unsigned int> figInd(new unsigned int[N]);
    for (int i = 0; i < N; i++) figInd.get()[i] = i;

    //! MATLAB: totfig = N; %% No dummy images....
    //! MATLAB: figInd = figInd(1:totfig);
    const int totfig = N;

    //! MATLAB: image = squeeze(images(:, : , figInd(1)));
    af::array image = images(af::span, af::span, figInd.get()[0]);

    //! MATLAB: [EnormF, NAmask, addRamp, cx, cy] = NormalField(image, NA, pixelsRes, wl);
    af::array EnormF, NAmask, addRamp;
    double cx, cy;
    NormalField(image, EnormF, NAmask, addRamp, cx, cy, tag);

    dbg_saveArray("arr_image", image, fname_out, true);
    dbg_saveArray("arr_EnormF", EnormF, fname_out, true);
    dbg_saveArray("arr_NAmask", NAmask, fname_out, true);
    dbg_saveArray("arr_addRamp", addRamp, fname_out, true);
    dbg_saveArray("arr_cx", af::constant<double>(cx, 1), fname_out, true);
    dbg_saveArray("arr_cy", af::constant<double>(cy, 1), fname_out, true);

#if CROP_AFTER_DECOMPOSITION
    //! MATLAB: [yy, xx] = size(image);
    const unsigned int yy = image.dims(0);
    const unsigned int xx = image.dims(1);

    dbg_saveArray("arr_yy", af::constant<unsigned int>(yy, 1), fname_out, true);
    dbg_saveArray("arr_xx", af::constant<unsigned int>(xx, 1), fname_out, true);
#else
    const double pixelRes = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
    const double NA = m_parameter[PN_NA];
    const double wl = m_parameter[PN_LAMBDA];

    //! MATLAB: rawSize = size(image, 1);
    const int rawSize = image.dims(0);

    //! MATLAB: [xx_crop, yy_crop, d0, d1] = calc_cropsize(rawSize, pixelsRes, NA, wl);
    int xCropSize, yCropSize, d0, d1;
    CalcCropSize(rawSize, pixelRes, NA, wl, xCropSize, yCropSize, d0, d1);

    //! MATLAB: xx = xx_crop;
    //! MATLAB: yy = yy_crop;
    const unsigned int xx = xCropSize;
    const unsigned int yy = yCropSize;

    dbg_saveArray("arr_yy", af::constant<unsigned int>(yy, 1), fname_out, true);
    dbg_saveArray("arr_xx", af::constant<unsigned int>(xx, 1), fname_out, true);
#endif

    //! MATLAB: delayLib = zeros(totfig - 1, xx*yy);
    af::array delayLib = af::constant(0, totfig - 1, xx * yy, c32);

    dbg_saveArray("arr_delayLib_0", delayLib, fname_out, true);

    for (int kk = 0; kk < (totfig - 1); kk++) //! MATLAB: for kk = 1:1 : totfig - 1
    {
#ifdef DEEP_DEBUG
        sprintf_s<128>(fname_in, "decomposition_loop_%s_%02d.arr", (tag == 0) ? "bg" : "fg", kk);
#endif

        //! MATLAB:   image = squeeze(images(:, : , figInd(kk + 1)));
        //! MATLAB:   tempRaw = double(image);
        af::array tempRaw = images(af::span, af::span, figInd.get()[kk + 1]);

        dbg_saveArray("arr_tempRaw", tempRaw, fname_in, false);

#if CROP_AFTER_DECOMPOSITION
        //! MATLAB:   Ftemp = circshift(fftshift(fft2(ifftshift(tempRaw))), [+cy, -cx]).*NAmask;
        af::array tempRaw1 = fftshift(af::fft2(ifftshift(tempRaw)));
        af::array Ftemp = af::shift(tempRaw1, cy, (-1) * cx) * NAmask;

        dbg_saveArray("arr_tempRaw1", tempRaw1, fname_in, true);
        dbg_saveArray("arr_Ftemp", Ftemp, fname_in, true);

        //! MATLAB:   Etemp = fftshift(ifft2(ifftshift(Ftemp))).*addRamp;
        af::array Etemp = fftshift(af::ifft2(ifftshift(Ftemp))) * addRamp;

        dbg_saveArray("arr_Etemp", Etemp, fname_in, true);
#else
        //! MATLAB:   Ftemp = circshift(fftshift(fft2(ifftshift(tempRaw))), [+cy, -cx]) / (rawSize ^ 2);
        //! MATLAB:   Ftemp = Ftemp(d0:d1, d0 : d1);
        //! MATLAB:   Ftemp = Ftemp.*NAmask;
        af::array tempRaw1 = fftshift(af::fft2(ifftshift(tempRaw)));
        af::array Ftemp = af::shift(tempRaw1, cy, (-1) * cx);
        Ftemp = Ftemp(af::seq(d0, d1), af::seq(d0, d1));
        Ftemp = Ftemp * NAmask;

        dbg_saveArray("arr_tempRaw1", tempRaw1, fname_in, true);
        dbg_saveArray("arr_Ftemp", Ftemp, fname_in, true);

        //! MATLAB:   sizeFtemp = length(Ftemp);
        const int sizeFtemp = Ftemp.dims(0);

        //! MATLAB:   Etemp = fftshift(ifft2(ifftshift(Ftemp)))*(sizeFtemp ^ 2);
        //! MATLAB:   Etemp = Etemp.*addRamp;
        af::array Etemp = fftshift(af::ifft2(ifftshift(Ftemp))) * (sizeFtemp ^ 2);
        Etemp = Etemp * addRamp;

        dbg_saveArray("arr_Etemp", Etemp, fname_in, true);
#endif

        //! MATLAB:   EtempF = (Etemp / mean(Etemp(:)) - EnormF) / 2;
        af::array EtempF = (Etemp / af::mean<af::cfloat>(Etemp) - EnormF) / 2;

        dbg_saveArray("arr_EtempF", EtempF, fname_in, true);

        //! MATLAB:   delayLib(kk, :) = EtempF(:);
        af::array EtempF_Col = af::moddims(EtempF, 1, xx * yy);
        dbg_saveArray("arr_EtempF_Col", EtempF_Col, fname_in, true);

        delayLib(kk, af::span) = EtempF_Col(af::span, af::span);
        dbg_saveArray("arr_delayLib", delayLib, fname_in, true);
    } //! MATLAB: end

    dbg_saveArray("arr_delayLib_1", delayLib, fname_out, true);

    //! MATLAB: angStep = linspace(0, pi, numfig + 1);
    af::array angStep = linspace(0, af::Pi, numfig + 1);

    dbg_saveArray("arr_angStep_1", angStep, fname_out, true);

    //! MATLAB: angStep = angStep(1:end - 1)';
    angStep = angStep(af::seq(0, af::end - 1));

    dbg_saveArray("arr_angStep_2", angStep, fname_out, true);

    //! MATLAB: decoMat = [1 / 4 * exp(1i * angStep), 1 / 4 * exp(-1i * angStep)];
    af::array angStepComplex = af::complex(0, angStep);

    dbg_saveArray("arr_angStepComplex", angStepComplex, fname_out, true);

    af::array decoMat = af::constant(0, numfig, 2, c32);

    af::array tmp1 = 1.0 / 4.0 * af::exp(angStepComplex);
    af::array tmp2 = 1.0 / 4.0 * af::exp((-1) * angStepComplex);

    dbg_saveArray("arr_tmp1", tmp1, fname_out, true);
    dbg_saveArray("arr_tmp2", tmp2, fname_out, true);

    decoMat(af::span, 0) = tmp1(af::span);
    decoMat(af::span, 1) = tmp2(af::span);

    dbg_saveArray("arr_decoMat", decoMat, fname_out, true);

    //! MATLAB: results_tmp = zeros(yy, xx, (totfig - 1) / numfig * 2 + 1);
    af::array results_tmp = af::constant(0, yy, xx, (totfig - 1) / numfig * 2 + 1, c32);

    //! MATLAB: results_tmp(:, : , 1) = EnormF;
    results_tmp(af::span, af::span, 0) = EnormF(af::span, af::span);

    dbg_saveArray("arr_results_tmp_0", results_tmp, fname_out, true);


    for (int kk = 0; kk < (totfig - 1) / numfig; kk++) //! MATLAB: for kk = 1:1 : (totfig - 1) / numfig
    {
#ifdef DEEP_DEBUG
        sprintf_s<128>(fname_in, "decomposition_loop2_%s_%02d.arr", (tag == 0) ? "bg" : "fg", kk);
#endif
        dbg_saveArray("arr_delayLib", delayLib, fname_in, false);

        //! MATLAB: 	tempLib = delayLib((kk - 1)*numfig + 1:kk*numfig, : );
        af::array tempLib = delayLib(af::seq(kk * numfig, kk * numfig + 1), af::span);

        dbg_saveArray("arr_tempLib", tempLib, fname_in, true);
        dbg_saveArray("arr_decoMat", decoMat, fname_in, true);

        //! MATLAB:   results_tmp(:, :, 2 * kk : 2 * kk + 1) = shiftdim(reshape(decoMat\tempLib, 2, yy, xx), 1);
        af::array solveRes = af::solve(decoMat, tempLib);

        dbg_saveArray("arr_solveRes", solveRes, fname_in, true);

        af::array tmpRes = af::moddims(solveRes, 2, yy, xx);

        dbg_saveArray("arr_tmpRes", tmpRes, fname_in, true);

        results_tmp(af::span, af::span, af::seq(2 * kk + 1, 2 * kk + 2)) = af::reorder(tmpRes, 1, 2, 0);

        dbg_saveArray("arr_results_tmp", results_tmp, fname_out, true);
    } //! MATLAB: end

#if CROP_AFTER_DECOMPOSITION
 //! MATLAB: rawSize = size(images, 1);
 //! MATLAB: r = round(rawSize*pixelsRes*NA / wl);
 //! MATLAB: fsMargin = 2 * round(r / 6);
    const double pixelRes = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
    const double NA = m_parameter[PN_NA];
    const double wl = m_parameter[PN_LAMBDA];

    const unsigned int rawSize = yy;
    const double r = std::round(rawSize * pixelRes * NA / wl);
    const double fsMargin = 2 * std::round(r / 6.0);

    dbg_saveArray("arr_rawSize", af::constant<unsigned int>(rawSize, 1), fname_out, true);
    dbg_saveArray("arr_r", af::constant<double>(r, 1), fname_out, true);
    dbg_saveArray("arr_fsMargin", af::constant<double>(fsMargin, 1), fname_out, true);

    //! MATLAB: xx_crop = round(2 * r) + fsMargin;
    //! MATLAB: yy_crop = round(2 * r) + fsMargin;
    const unsigned int xx_crop = std::round(2 * r) + fsMargin;
    const unsigned int yy_crop = std::round(2 * r) + fsMargin;

    //std::cout << "xx_crop=" << xx_crop << " yy_crop=" << yy_crop << " r=" << r << " fsMargin=" << fsMargin << std::endl;

    dbg_saveArray("arr_xx_crop", af::constant<unsigned int>(xx_crop, 1), fname_out, true);
    dbg_saveArray("arr_yy_crop", af::constant<unsigned int>(yy_crop, 1), fname_out, true);

    //! MATLAB: results = zeros(yy_crop, xx_crop, size(results_tmp, 3));
    const unsigned int d3count = results_tmp.dims(2);
    af::array results = af::constant(0, yy_crop, xx_crop, d3count, c32);

    const unsigned int a0 = (rawSize / 2 - r - fsMargin / 2 + 1) - 1;
    const unsigned int a1 = (rawSize / 2 + r + fsMargin / 2) - 1;

    dbg_saveArray("arr_a0", af::constant<unsigned int>(a0, 1), fname_out, true);
    dbg_saveArray("arr_a1", af::constant<unsigned int>(a1, 1), fname_out, true);

    for (unsigned int kk = 0; kk < d3count; kk++)  //! MATLAB: for kk=1:1:size(results_tmp, 3)
    {
        //! MATLAB: img = squeeze(results_tmp(:, : , kk));
        af::array img = results_tmp(af::span, af::span, kk);

        dbg_saveArrayIdx("arr_crop_img", kk, img, fname_out, true);

        //! MATLAB: Fimg = fftshift(fft2(img)) / (rawSize ^ 2);
        af::array Fimg = fftshift(af::fft2(img)) / std::pow(rawSize, 2);

        dbg_saveArrayIdx("arr_crop_Fimg1", kk, Fimg, fname_out, true);

        //! MATLAB: Fimg = Fimg(rawSize / 2 - r - fsMargin / 2 + 1:rawSize / 2 + r + fsMargin / 2, rawSize / 2 - r - fsMargin / 2 + 1 : rawSize / 2 + r + fsMargin / 2);
        Fimg = Fimg(af::seq(a0, a1), af::seq(a0, a1));

        dbg_saveArrayIdx("arr_crop_Fimg2", kk, Fimg, fname_out, true);

        //! MATLAB: sizeFimg = length(Fimg);
        const unsigned int sizeFimg = yy_crop;

        //! MATLAB: Fimg = ifft2(ifftshift(Fimg))*(sizeFimg ^ 2);
        Fimg = af::ifft2(ifftshift(Fimg)) * std::pow(sizeFimg, 2);

        dbg_saveArrayIdx("arr_crop_Fimg3", kk, Fimg, fname_out, true);

        //! MATLAB: results(:, : , kk) = Fimg(:, : );
        results(af::span, af::span, kk) = Fimg(af::span, af::span);
    }

    dbg_saveArray("arr_results_tmp_1", results, fname_out, true);

    results.eval();
#else
    af::array& results = results_tmp;
#endif

    //! MATLAB: results = results_tmp;
    return results;
    }

void TC::TomoBuilderFieldRetrievalMulti::NormalField(const af::array& image, af::array& EnormF, af::array& NAmask, af::array& addRamp, double& cx, double& cy, const int tag) {
    const char* fname_out = (tag == 0) ? "NormalField_bg.arr" : "NormalField_fg.arr";

    const double NA = m_parameter[PN_NA];
    const double pixelsRes = m_parameter[PN_PIXEL_SIZE] / m_parameter[PN_M];
    const double wl = m_parameter[PN_LAMBDA];

    //! MATLAB: normRaw = double(image);
    const af::array& normRaw = image;

    dbg_saveArray("arr_normRaw", normRaw, fname_out, false);

    //! MATLAB: [yy, xx] = size(normRaw);
    af::dim4 dims = normRaw.dims();
    const int yy = dims.dims[0];
    const int xx = dims.dims[1];

#if 0
    //! MATLAB: [Enorm, cx, cy] = HT2KR(normRaw, NA, pixelsRes, wl, 1);
    af::array Enorm;
    hilbert2(normRaw, NA, pixelsRes, wl, 1, Enorm, cx, cy, ((tag == 0) ? "hilbert2_bg.arr" : "hilbert2_fg.arr"));
#else
    //! MATLAB: r = xx*pixelsRes*NA/wl;  % insert : give the appropriate value corresponds to the numerical aperture / lambda
    //! MATLAB: yr = r*yy/xx;
    const double r = xx * pixelsRes * NA / wl;
    const double yr = r * yy / xx;

    dbg_saveArray("arr_r", af::constant<double>(r, 1), fname_out, true);
    dbg_saveArray("arr_yr", af::constant<double>(yr, 1), fname_out, true);

    //! MATLAB: c0mask = ~(mk_ellipse(r, yr, xx, yy));
    af::array c0mask = !(mk_ellipse(r, yr, xx, yy));

    dbg_saveArray("arr_c0mask", c0mask, fname_out, true);

    //! MATLAB: Fimg = fftshift(fft2(ifftshift(normRaw))); %FFT
    af::array Fimg = fftshift(af::fft2(ifftshift(normRaw)));

    dbg_saveArray("arr_Fimg_1", Fimg, fname_out, true);

    //! MATLAB: [~, mV] = max(abs(Fimg(1:floor(end / 2))));
    //! MATLAB: my = mod(mV, yy);
    //! MATLAB: mx = (mV - my) / yy + 1;
    af::array absFimg = af::abs(Fimg);
    unsigned int a0 = round(absFimg.dims(1) / 2) - 2;
    unsigned int a1 = absFimg.dims(1) - 1;
    absFimg(af::span, af::seq(a0, a1)) = 0;
    absFimg.eval();

    dbg_saveArray("arr_absFimg", absFimg, fname_out, true);

    float mvalue = af::max<float>(absFimg);

    dbg_saveArray("arr_mvalue", af::constant<float>(mvalue, 1), fname_out, true);

    unsigned int mxIdx, myIdx;
    af::array absFimgMax = (absFimg == mvalue);
    absFimgMax.eval();

    Find(absFimgMax, myIdx, mxIdx);

    dbg_saveArray("arr_mx_1", af::constant<unsigned int>(mxIdx, 1), fname_out, true);
    dbg_saveArray("arr_my_1", af::constant<unsigned int>(myIdx, 1), fname_out, true);

    //! MATLAB: my = my - (floor(yy / 2) + 1);
    //! MATLAB: mx = mx - (floor(xx / 2) + 1);
    const double my = myIdx - (std::floor(yy / 2.0));
    const double mx = mxIdx - (std::floor(xx / 2.0));

    dbg_saveArray("arr_mx_2", af::constant<int>(mx, 1), fname_out, true);
    dbg_saveArray("arr_my_2", af::constant<int>(my, 1), fname_out, true);

#if CROP_AFTER_DECOMPOSITION
    //! MATLAB: Fimg2 = circshift(Fimg, [-my, -mx]).*c0mask;
    Fimg = af::shift(Fimg, (-1) * my, (-1) * mx) * c0mask;

    dbg_saveArray("arr_Fimg_2", Fimg, fname_out, true);

    //! MATLAB: Enorm = fftshift(ifft2(ifftshift(Fimg2))); % insert : Inverse fourier transform of Fimg
    af::array Enorm = fftshift(af::ifft2(ifftshift(Fimg)));

    dbg_saveArray("arr_Enorm", Enorm, fname_out, true);
#else
    //! MATLAB: [xx_crop, yy_crop, d0, d1] = calc_cropsize(xx, pixelsRes, NA, wl);
    int xCropSize, yCropSize, d0, d1;
    CalcCropSize(xx, pixelsRes, NA, wl, xCropSize, yCropSize, d0, d1);

    //! MATLAB: Fimg2 = circshift(Fimg,[-my,-mx]).*c0mask / (xx^2);
    Fimg = af::shift(Fimg, (-1) * my, (-1) * mx) * c0mask / (xx ^ 2);

    //! MATLAB: Fimg2 = Fimg2(d0:d1,d0:d1);
    Fimg = Fimg(af::seq(d0, d1), af::seq(d0, d1));

    dbg_saveArray("arr_Fimg_2", Fimg, fname_out, true);

    //! MATLAB: sizeFimg2 = length(Fimg2);
    int sizeFimg = Fimg.dims(0);

    //! MATLAB: Enorm = fftshift(ifft2(ifftshift(Fimg2)))*(sizeFimg2 ^ 2);
    af::array Enorm = fftshift(af::ifft2(ifftshift(Fimg))) * (sizeFimg ^ 2);

    dbg_saveArray("arr_Enorm", Enorm, fname_out, true);
#endif

    //! MATLAB: cy = -my(1);
    //! MATLAB: cx = mx(1);
    cy = -1 * my;
    cx = mx;
#endif

    dbg_saveArray("arr_cx", af::constant<double>(cx, 1), fname_out, true);
    dbg_saveArray("arr_cy", af::constant<double>(cy, 1), fname_out, true);

#if CROP_AFTER_DECOMPOSITION
    //! MATLAB: NArx = NA / wl * pixelsRes * xx;
    //! MATLAB: NAry = NA / wl * pixelsRes * yy;
    const double NArx = NA / wl * pixelsRes * xx;
    const double NAry = NA / wl * pixelsRes * yy;

    const int xx_new = xx;
    const int yy_new = yy;
#else
    //! MATLAB: xx = xx_crop;
    //! MATLAB: yy = yy_crop;
    const int xx_new = xCropSize;
    const int yy_new = yCropSize;

    //! MATLAB: NArx = r;
    //! MATLAB: NAry = yr;
    const double NArx = r;
    const double NAry = yr;
#endif

    dbg_saveArray("arr_NArx", af::constant<double>(NArx, 1), fname_out, true);
    dbg_saveArray("arr_NAry", af::constant<double>(NAry, 1), fname_out, true);

    //! MATLAB: NAmask = ~mk_ellipse(NArx, NAry, xx, yy);
    NAmask = !(mk_ellipse(NArx, NAry, xx_new, yy_new));

    dbg_saveArray("arr_NAmask", NAmask, fname_out, true);

    //! MATLAB: BGmask = ~mk_ellipse(xx / 2, yy / 2, xx, yy);
    af::array BGmask = !(mk_ellipse(xx_new / 2, yy_new / 2, xx_new, yy_new));

    dbg_saveArray("arr_BGmask", BGmask, fname_out, true);

    //! MATLAB: angTemp = PhiShiftKR(angle(Enorm), 1, BGmask);
    af::array angTemp = PhiShiftKR3(angle(Enorm), 1, BGmask, ((tag == 0) ? "PhiShiftKR_bg.arr" : "PhiShiftKR_fg.arr"));

    dbg_saveArray("arr_angTemp", angTemp, fname_out, true);

    //! MATLAB: addRamp = exp(1i * (angTemp - angle(Enorm)));
    addRamp = af::exp(af::complex(0, angTemp - angle(Enorm)));

    dbg_saveArray("arr_addRamp", addRamp, fname_out, true);

    //! MATLAB: Enorm2 = abs(Enorm).*exp(1i * angTemp);
    af::array Enorm2 = af::abs(Enorm) * af::exp(af::complex(0, angTemp));

    dbg_saveArray("arr_Enorm2", Enorm2, fname_out, true);

    //! MATLAB: EnormF = Enorm2 / mean(Enorm2(:));
    EnormF = Enorm2 / af::mean<af::cfloat>(Enorm2);

    dbg_saveArray("arr_EnormF", EnormF, fname_out, true);
}

void TC::TomoBuilderFieldRetrievalMulti::CalcCropSize(int rawSize, double pixelRes, double NA, double wl, int& xSize, int& ySize, int& d0, int& d1) {
    //! MATLAB: r = round(rawSize*pixelsRes*NA / wl);
    const double r = std::round(rawSize * pixelRes * NA / wl);

    //! MATLAB: fsMargin = 2 * round(r / 6);
    const int fsMargin = 2 * std::round(r / 6);

    //! MATLAB: xx_crop = round(2 * r) + fsMargin;
    //! MATLAB: yy_crop = round(2 * r) + fsMargin;
    xSize = std::round(2 * r) + fsMargin;
    ySize = xSize;

    //! MATLAB: d0 = rawSize / 2 - r - fsMargin / 2 + 1;
    //! MATLAB: d1 = rawSize / 2 + r + fsMargin / 2;
    d0 = ((rawSize / 2) - r - (fsMargin / 2) + 1) - 1;
    d1 = ((rawSize / 2) + r + (fsMargin / 2)) - 1;
}
