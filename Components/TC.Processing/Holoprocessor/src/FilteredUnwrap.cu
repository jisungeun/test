#define LOGGER_TAG "TCHoloprocessor::FilteredUnwrap"

#include <TCLogger.h>

#include "CudaErrorLog.h"

#include "Herve.h"

#include "FilteredUnwrap.h"
#include "Residue.h"

auto FilteredUnwrap(float* devicePointerWrappedPhase, const int arrayXSize, const int arrayYSize,
    float* devicePointerUnwrappedPhase, int& residueNumber) -> bool {
    const auto unwrappingFailed{ false };
    const auto unwrappingSucceed{ true };

    const auto length = arrayXSize * arrayYSize;

    const auto threadsPerBlock = 32;
    const auto blockNumberX = ((arrayXSize - 1) / threadsPerBlock) + 1;
    const auto blockNumberY = ((arrayYSize - 1) / threadsPerBlock) + 1;

    dim3 threads(threadsPerBlock, threadsPerBlock);
    dim3 blocks(blockNumberX, blockNumberY);

    int* devicePointerResidueNumber;
    cudaError_t cudaError;

    cudaError = cudaMalloc((void**)&devicePointerResidueNumber, sizeof(float) * length);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMalloc devicePointerResidueNumber failed");
        return unwrappingFailed;
    }

    const auto setValue = 0;
    cudaError = cudaMemset(devicePointerResidueNumber, setValue, sizeof(int));
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMemset devicePointerResidueNumber failed");
        cudaFree(devicePointerResidueNumber);
        return unwrappingFailed;
    }

    Residue << <blocks, threads >> > (devicePointerWrappedPhase, arrayXSize, arrayYSize, devicePointerResidueNumber);
    cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "Residue failed");
        return unwrappingFailed;
    }

    cudaError = cudaMemcpy(&residueNumber, devicePointerResidueNumber, sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaError != cudaSuccess) {
        LogCudaError(cudaError, "cudaMemcpy residueNumber failed");
        cudaFree(devicePointerResidueNumber);
        return unwrappingFailed;
    }

    HerveUnwrap_optimized_revised(devicePointerWrappedPhase, devicePointerUnwrappedPhase, arrayXSize, arrayYSize);

    cudaFree(devicePointerResidueNumber);

    return unwrappingSucceed;
}
