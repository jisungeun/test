#pragma once

auto FilteredUnwrap(float* devicePointerWrappedPhase, const int arrayXSize, const int arrayYSize,
    float* devicePointerUnwrappedPhase, int& residueNumber)->bool;
