#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"


class SystemModelHT1H : public SystemModelInterface {
public:
    typedef SystemModelHT1H Self;
    typedef std::shared_ptr<SystemModelHT1H> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    virtual QString Model() const override { return "HT-1H"; }
    virtual int Generation() const override { return 1; }

    virtual int MaxFramerate(unsigned int pixels) override {
        return (pixels < 700) ? 150 : 100;
    }

    //Laser
    virtual unsigned int Wavelength() override { return 532; }

    //Camera
    virtual QString Camera() override { return "Pointgrey FL3-U3-13Y3M-C"; }

    //Objective Lens
    virtual double ObjectiveLensMagnification() override { return 58.33; }
    virtual double ObjectiveLensNA() override { return 1.2; }
    double ObjectiveLensReadyPos() override { return 0.6; }
    double ObjectiveLensLimitPos() override { return 1.2; }

    //Condenser Lens
    double CondenserLensReadyPos() override { return 23.5; }
    double CondenserLensLimitPos() override { return 25.0; }

    //Condenser Lens Auto Calibration
    virtual double CondenserLensStepAmount(TC::CalibrationStepType type) override {
        double step = 0;

        switch (type) {
        case TC::STEP_OUTOFRANGE:
            step = 0.035;
            break;
        case TC::STEP_FAR:
            step = 0.01;
            break;
        case TC::STEP_NEAR:
            step = 0.002;
            break;
        case TC::STEP_FINE:
            step = 0.001;
            break;
        case TC::STEP_ESTIMATION:
            step = 0.0005;
            break;
        }

        return step;
    }

    virtual double CondenserLensDistThreshold(TC::CalibrationDistThreshold type) override {
        double threshold = 0;

        switch (type) {
        case TC::DIST_NEAR_THRESHOLD:
            threshold = 0.2;
            break;
        case TC::DIST_FINE_THRESHOLD:
            threshold = 0.05;
            break;
        }

        return threshold;
    }

    //Processing
    unsigned int ZeroPaddingMax2D() override { return 256; }
    unsigned int ZeroPaddingMax3D() override { return 312; }
    unsigned int NumberOfIteration() override { return 40; }

    //Fluorscence
    virtual double FluorescenceScanDepth() override { return 0; }
    virtual double FluorescenceScanStep() override { return 0; }
    virtual double FluorescenceScanStepUnit() override { return 1; }

    //Motion
    virtual double MotionMovableLowerX() override { return -4.0; }
    virtual double MotionMovableUpperX() override { return  4.0; }
    virtual double MotionMovableLowerY() override { return -4.0; }
    virtual double MotionMovableUpperY() override { return  4.0; }

    virtual bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) override {
        if (tarCPos <= curCPos) return true;
        return (tarCPos <= (24.5 + curZPos));
    }

    virtual bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) override {
        if (tarZPos >= curZPos) return true;
        return (tarZPos >= (curCPos - 24.5));
    }

    //=== Model-specific functionality ===
    virtual bool doesSupportXYZControl() override { return true; }
    virtual bool doesSupportFluorescence() override { return false; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase);
    }
};

