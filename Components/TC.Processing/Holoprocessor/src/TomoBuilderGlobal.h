#pragma once

#include <arrayfire.h>
#include <af/data.h>
#include <vector>
#include <map>

#include <QString>

//#define DEEP_DEBUG		//! for saving array variables into file
//#define SHALLOW_DEBUG		//! print-out array variable but not saving it int file

//Performance measure
#define PERFORMANCE_MEASURE_LOOP	1

#define TCF_VERSION_STR	"1.3.6"
#define RETRIEVAL_ALGO_VERSION				(100)
#define ITERATIVE_ALGO_VERSION				(100)
#define NONITERATIVE_ALGO_VERSION			(001)

#define PN_SERIAL								"Serial"
#define PN_HOSTCOMPUTER                            "Computer"
#define PN_SWVERSION                            "SW Version"
#define PN_PHASE_SIGN							"phase sign"
#define PN_ITERATION							"iteration"
#define PN_DISABLE_ZLIMIT                        "Disable ZLimit"
#define PN_IGNORE_POOR                            "Ignore Poor"
#define PN_NIR_RADIUS							"NIR Radius"
#define PN_NA									"NA"
#define PN_NA_CDS								"NA_CDS"
#define PN_NM									"n_m"
#define PN_IMMERSION_RI							"Immersion_RI"
#define PN_PIXEL_SIZE							"Pixel size"
#define PN_LAMBDA								"Lambda"
#define PN_M									"M"
#define PN_CAMERA_SHUTTER						"Camera Shutter"
#define PN_CAMERA_GAIN							"Camera Gain"
#define PN_ACQUISITION_INTERVAL_2D				"Acquisition interval 2D"
#define PN_ACQUISITION_INTERVAL_3D				"Acquisition interval 3D"
#define PN_ACQUISITION_INTERVAL                 "Acquisition Time Interval"
#define PN_ACQUISITION_COUNT                    "Acquisition Count"
#define PN_ACQUISITION_HTFL_ZOFFSET				"Acquisition Z Offset"
#define PN_ACQUISITION_HTFL_ZOFFSET_COMPENSATION    "Acquisition Z Offset Compensation"
#define PN_MAPPGIN_SIGN							"mapping sign"
#define PN_PATTERN_TYPE							"Pattern type"
#define PN_FL_CAMERA_SHUTTER(ch)				"FLCH" #ch "_Camera_Shutter"
#define PN_FL_CAMERA_GAIN(ch)					"FLCH" #ch "_Camera_Gain"
#define PN_FL_LIGHT_INTENSITY(ch)				"FLCH" #ch "_Light_Intensity"
#define PN_FL_EMISSION_WAVELENGTH(ch)			"FLCH" #ch "_Fluorophore_Emission"
#define PN_FL_SCANRANGE							"FL_Scan_Range"
#define PN_FL_SCANSTEP							"FL_Scan_Step"
#define PN_FL_SCANCENTER						"FL_Scan_Center"
#define PN_FL_ACQUISITION_INTERVAL_3D			"FL_Acquisition_interval_3D"
#define PN_TILES_INDEX							"Tiles Imaging Index"
#define PN_TILES_TOTAL							"Tiles Imaging Total"
#define PN_TILES_OVERLAP_H						"Tiles Imaging Overlap H"
#define PN_TILES_OVERLAP_V						"Tiles Imaging Overlap V"

namespace TC {
    enum {
        FILE_TCF,
        FILE_AF
    };

    class ImageSet {
    public:
        unsigned int width() {
            return (unsigned int)arrData.dims(1);
        }

        unsigned int height() {
            return (unsigned int)arrData.dims(0);
        }

        unsigned int images() {
            return nImages;
        }

        void clear() {
            arrData = af::constant(0, 1, 1);
        }

    public:
        af::array arrData;
        unsigned int nImages;
    };

    class FieldRetrievalParam {
    public:
        FieldRetrievalParam()
            : hasNormalSample(false)
            , idx_offset(0) {
        }

        void SetZeropadding(unsigned int zp, unsigned int zp2, unsigned int zp3) {
            ZP = zp;
            ZP2 = zp2;
            ZP3 = zp3;
        }

        void ReleaseIntermidiate(void) {
            c3mask = af::constant(0, 1, 1);
            Fbg = af::constant(0, 1, 1);
            bp_masks = af::constant(0, 1, 1);

            c3mask.eval();
            Fbg.eval();
            bp_masks.eval();
        }

        void ReleaseAll(void) {
            ReleaseIntermidiate();

            retAmplitude = af::constant(0, 1, 1);
            retPhase = af::constant(0, 1, 1);

            retAmplitude.eval();
            retPhase.eval();
        }

    public:
        //variables are filled after FieldRetrievalBgOnly...
        af::array f_dx;
        af::array f_dy;
        af::array c3mask;
        af::array Fbg;
        af::array bp_masks;
        bool hasNormalSample;
        unsigned int rawSize;
        double res;
        unsigned int nArrSize;
        unsigned int mi, mj, a0, a1;
        unsigned int idx_offset;

        //variables are filled after FieldRetrievalFgOnly...
        af::array retAmplitude;
        af::array retPhase;
        std::vector<bool> fieldQuality;
        std::map<int, int> badFields;       //! fields over pre-defined threshold (=50)
        std::map<int, int> residues;
        unsigned int ZP;
        unsigned int ZP2;
        unsigned int ZP3;
    };

    class Position {
    public:
        Position() : x(0), y(0), z(0), c(0) {}
        double x, y, z, c;
    };

    class PhaseImageParam {
    public:
        PhaseImageParam() : zeropadding(0) {}

    public:
        QString bgImgNormalPath;    //normal angle
        QString bgImgPath;          //specific angle
        QString rawImgPath;         //sample image
        QString outputPath;

        int mode;
        double dParam;
        bool bCompensateAnalysis;
        int zeropadding;
    };

    struct Resolution {
        double resolutionX{ 0 };
        double resolutionY{ 0 };
        double resolutionZ{ 0 };
    };

    struct MinMax {
        double minValue{ 0 };
        double maxValue{ 0 };
    };

    struct Sizes {
        size_t sizeX{ 0 };
        size_t sizeY{ 0 };
        size_t sizeZ{ 0 };
    };

    struct AfHostDeleter {
        void operator() (float* ptr) const {
            af_free_host(ptr);
        }

        void operator() (double* ptr) const {
            af_free_host(ptr);
        }

        void operator() (uint8_t* ptr) const {
            af_free_host(ptr);
        }

        void operator() (uint16_t* ptr) const {
            af_free_host(ptr);
        }

        void operator() (uint32_t* ptr) const {
            af_free_host(ptr);
        }

        void operator() (int16_t* ptr) const {
            af_free_host(ptr);
        }

        void operator() (int32_t* ptr) const {
            af_free_host(ptr);
        }
    };
}
