#pragma once

#include <arrayfire.h>

class PhaseUnwrapper {
public:
    PhaseUnwrapper() {}
    ~PhaseUnwrapper() {}

    virtual af::array execute(const af::array& arrIn, bool& bGoodQuality, int& score) = 0;
};