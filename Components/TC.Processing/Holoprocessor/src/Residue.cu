#include "Residue.h"

__global__ void
Residue(float* dev_Phase, const int Xsize, const int Ysize, int* dev_Residue_num) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int m, n;
    float d1, d2, d3, d4;
    const float pi = CUDART_PI_F;

    while (index < Xsize * Ysize - 1) {
        m = index % Xsize;
        n = index / Ysize;

        const bool in_Xscope = (m != (Xsize - 1));
        const bool in_Yscope = (n != (Ysize - 1));


        if (in_Xscope && in_Yscope) {
            const float left_up = dev_Phase[index];
            const float left_down = dev_Phase[index + Xsize];
            const float right_up = dev_Phase[index + 1];
            const float right_down = dev_Phase[index + Xsize + 1];

            d1 = left_down - left_up;
            if (d1 > pi) d1 -= 2 * pi;
            else if (d1 < -pi) d1 += 2 * pi;

            d2 = right_down - left_down;
            if (d2 > pi) d2 -= 2 * pi;
            else if (d2 < -pi) d2 += 2 * pi;

            d3 = right_up - right_down;
            if (d3 > pi) d3 -= 2 * pi;
            else if (d3 < -pi) d3 += 2 * pi;

            d4 = left_up - right_up;
            if (d4 > pi) d4 -= 2 * pi;
            else if (d4 < -pi) d4 += 2 * pi;

            if ((d1 + d2 + d3 + d4) > 6) {
                atomicAdd(dev_Residue_num, 1);
            } else if ((d1 + d2 + d3 + d4) < -6) {
                atomicAdd(dev_Residue_num, 1);
            }
        }
        index += blockDim.x * gridDim.x * blockDim.y * gridDim.y;
    }
}
