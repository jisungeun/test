#define LOGGER_TAG "TCHoloprocessor::AfArrayProfiler"
#include <Windows.h>

#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QCoreApplication>

#include <TCLogger.h>

#include "AfArrayProfiler.h"

#define dlsym (void *) GetProcAddress
#define dlclose FreeLibrary

static char* (*NVML_nvmlErrorString)(nvmlReturn_t);
static nvmlReturn_t(*NVML_nvmlInit)();
static nvmlReturn_t(*NVML_nvmlDeviceGetCount)(uint*);
static nvmlReturn_t(*NVML_nvmlDeviceGetHandleByIndex)(uint, nvmlDevice_t*);
static nvmlReturn_t(*NVML_nvmlDeviceGetName)(nvmlDevice_t, char*, uint);
static nvmlReturn_t(*NVML_nvmlDeviceGetPciInfo)(nvmlDevice_t, nvmlPciInfo_t*);
static nvmlReturn_t(*NVML_nvmlDeviceGetTemperature)(nvmlDevice_t, nvmlTemperatureSensors_t, uint*);
static nvmlReturn_t(*NVML_nvmlDeviceGetFanSpeed)(nvmlDevice_t, uint*);
static nvmlReturn_t(*NVML_nvmlDeviceGetMemoryInfo)(nvmlDevice_t, nvmlMemory_t*);
static nvmlReturn_t(*NVML_nvmlShutdown)();

AfArrayProfiler::AfArrayProfiler(bool bEnable)
    : m_bEnable(bEnable)
    , m_bNVMLLoaded(true) {
    QStringList dllPathes;
    char dllPath[512];

    //C:\\windows\\system32\\nvml.dll
    ExpandEnvironmentStringsA("%windir%\\system32\\nvml.dll", dllPath, sizeof(dllPath));
    dllPathes.push_back(dllPath);

    //C:\\Program Files\\NVIDIA Corporation\\NVSMI\\nvml.dll
    ExpandEnvironmentStringsA("%ProgramFiles%\\NVIDIA Corporation\\NVSMI\\nvml.dll", dllPath, sizeof(dllPath));
    dllPathes.push_back(dllPath);

    //C:\\Program Files\\TomoStudio\\bin\\nvml.dll
    dllPathes.push_back(QString("%1\\nvml.dll").arg(QCoreApplication::applicationDirPath()));

    HINSTANCE hDLL = 0;
    foreach(QString strPath, dllPathes) {
        hDLL = LoadLibrary(strPath.toStdString().c_str());
        if (hDLL) break;
    }

    if (!hDLL) {
        QLOG_ERROR() << "Failed to load NVML module at default path";
        m_bNVMLLoaded = false;
        return;
    }

    NVML_nvmlInit = (nvmlReturn_t(*)()) dlsym(hDLL, "nvmlInit_v2");
    if (!NVML_nvmlInit) {
        NVML_nvmlInit = (nvmlReturn_t(*)()) dlsym(hDLL, "nvmlInit");
        if (!NVML_nvmlInit) {
            QLOG_ERROR() << "Failed to init NVML";
            m_bNVMLLoaded = false;
            return;
        } else {
            NVML_nvmlDeviceGetCount = (nvmlReturn_t(*)(uint*))dlsym(hDLL, "nvmlDeviceGetCount");
            NVML_nvmlDeviceGetHandleByIndex = (nvmlReturn_t(*)(uint, nvmlDevice_t*))dlsym(hDLL, "nvmlDeviceGetHandleByIndex");
            NVML_nvmlDeviceGetPciInfo = (nvmlReturn_t(*)(nvmlDevice_t, nvmlPciInfo_t*))dlsym(hDLL, "nvmlDeviceGetPciInfo");
        }
    } else {
        NVML_nvmlDeviceGetCount = (nvmlReturn_t(*)(uint*))dlsym(hDLL, "nvmlDeviceGetCount_v2");
        NVML_nvmlDeviceGetHandleByIndex = (nvmlReturn_t(*)(uint, nvmlDevice_t*))dlsym(hDLL, "nvmlDeviceGetHandleByIndex_v2");
        NVML_nvmlDeviceGetPciInfo = (nvmlReturn_t(*)(nvmlDevice_t, nvmlPciInfo_t*))dlsym(hDLL, "nvmlDeviceGetPciInfo_v2");
    }

    NVML_nvmlErrorString = (char* (*)(nvmlReturn_t))dlsym(hDLL, "nvmlErrorString");
    NVML_nvmlDeviceGetName = (nvmlReturn_t(*)(nvmlDevice_t, char*, uint))dlsym(hDLL, "nvmlDeviceGetName");
    NVML_nvmlDeviceGetTemperature = (nvmlReturn_t(*)(nvmlDevice_t, nvmlTemperatureSensors_t, uint*))dlsym(hDLL, "nvmlDeviceGetTemperature");
    NVML_nvmlDeviceGetFanSpeed = (nvmlReturn_t(*)(nvmlDevice_t, uint*))dlsym(hDLL, "nvmlDeviceGetFanSpeed");
    NVML_nvmlDeviceGetMemoryInfo = (nvmlReturn_t(*)(nvmlDevice_t, nvmlMemory_t*))dlsym(hDLL, "nvmlDeviceGetMemoryInfo");
    NVML_nvmlShutdown = (nvmlReturn_t(*)())dlsym(hDLL, "nvmlShutdown");

    nvmlReturn_t ret = NVML_nvmlInit();
    if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to init NVML : " << NVML_nvmlErrorString(ret);

    uint devices = 0;
    ret = NVML_nvmlDeviceGetCount(&devices);
    if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to get device count : " << NVML_nvmlErrorString(ret);

    //QLOG_INFO() << "Found " << devices << " CUDA devices";

    for (uint idx = 0; idx < devices; idx++) {
        nvmlDevice_t device;
        ret = NVML_nvmlDeviceGetHandleByIndex(idx, &device);
        if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to get NVML handle (idx=" << 0 << ") : " << NVML_nvmlErrorString(ret);

        m_devices.push_back(device);
    }

    //Check available memory
    for (uint idx = 0; idx < devices; idx++) {
        QLOG_INFO() << "[" << idx << "] Available=" << (Available(idx) / std::pow(1024, 2)) << "MB Used=" << (Used(idx) / std::pow(1024, 2)) << "MB";
    }
}

AfArrayProfiler::~AfArrayProfiler() {
    if (m_bNVMLLoaded) NVML_nvmlShutdown();
}

AfArrayProfiler::Pointer AfArrayProfiler::GetInstance(bool bEnable) {
    static AfArrayProfiler::Pointer profiler(new AfArrayProfiler(bEnable));
    return profiler;
}

void AfArrayProfiler::ResetHandle(int nIndex) {
    if (!m_bNVMLLoaded) return;

    nvmlReturn_t ret = NVML_nvmlDeviceGetHandleByIndex(nIndex, &m_devices[nIndex]);
    if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to get NVML handle (idx=" << nIndex << ") : " << NVML_nvmlErrorString(ret);
}

void AfArrayProfiler::LogMemInfo(const int idx, const QString& tag, char* file_name, int line_number) {
    nvmlMemory_t mem;
    size_t abytes = 0;
    size_t abuffs = 0;
    size_t lbytes = 0;
    size_t lbuffs = 0;

    if (!m_bEnable) return;

    af::deviceMemInfo(&abytes, &abuffs, &lbytes, &lbuffs);
    if (m_bNVMLLoaded) NVML_nvmlDeviceGetMemoryInfo(m_devices[idx], &mem);

    QDateTime curr = QDateTime::currentDateTime();
    QString timeStr = curr.toString("hh:mm:ss.zzz");

    const QString strInfoLine = QString("%1,%2:%3").arg(timeStr).arg(file_name).arg(line_number);
    const QString strInfoMemory = QString("%1,%2,%3,%4,%5,%6,%7").arg(tag).arg(abytes).arg(abuffs).arg(lbytes).arg(lbuffs).arg(mem.used).arg(mem.total);

    m_ltInfoLine.push_back(strInfoLine);
    m_ltInfoMemory.push_back(strInfoMemory);

    //if (m_bEnable) QLOG_INFO() << "[" << idx << "] " << strInfoLine << "," << strInfoMemory;
}

void AfArrayProfiler::Export(const QString& strPath) {
    if (!m_bEnable) return;

    QFile infoFile(QString("%1_Info.csv").arg(strPath));
    const int nInfoCount = m_ltInfoMemory.size();

    if (nInfoCount > 0 && infoFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
        QTextStream out(&infoFile);

        for (int i = 0; i < nInfoCount; i++) {
            out << m_ltInfoLine.at(i) << "," << m_ltInfoMemory.at(i) << "\n";
        }
    }
}

void AfArrayProfiler::Reset(void) {
    m_ltInfoLine.clear();
    m_ltInfoMemory.clear();
}

size_t AfArrayProfiler::Available(const int idx) {
    if (!m_bNVMLLoaded) return 0;

    nvmlMemory_t mem;
    nvmlReturn_t ret = NVML_nvmlDeviceGetMemoryInfo(m_devices[idx], &mem);
    if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to check available GPU memory : " << NVML_nvmlErrorString(ret);

    return mem.free;
}

size_t AfArrayProfiler::Used(const int idx) {
    if (!m_bNVMLLoaded) return 0;

    nvmlMemory_t mem;
    nvmlReturn_t ret = NVML_nvmlDeviceGetMemoryInfo(m_devices[idx], &mem);
    if (ret != NVML_SUCCESS) QLOG_ERROR() << "Failed to check used GPU memory : " << NVML_nvmlErrorString(ret);

    return mem.used;
}
