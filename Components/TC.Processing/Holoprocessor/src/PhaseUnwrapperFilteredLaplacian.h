#pragma once

#include "PhaseUnwrapper.h"

class PhaseUnwrapperFilteredLaplacian : public PhaseUnwrapper {
public:
    PhaseUnwrapperFilteredLaplacian() : PhaseUnwrapper() {}
    ~PhaseUnwrapperFilteredLaplacian() {}

    af::array execute(const af::array& arrIn, bool& bGoodQuality, int& score) override;
};