#include <map>

#include "CameraModels.h"

CameraModelsPtr CameraModels::GetInstance(const QString& name) {
    static CameraModelsPtr theModel;
    bool bNewInstance = false;

    if (theModel.get() == NULL) bNewInstance = true;
    else if (theModel->isSameCamera(name) == false) bNewInstance = true;

    if (bNewInstance) {
        if (name == CameraPGFL3U3::modelName()) {
            theModel.reset(new CameraPGFL3U3());
        } else if (name == CameraBFSU328S5M::modelName()) {
            theModel.reset(new CameraBFSU328S5M());
        } else {
            theModel.reset(new CameraVirtual());
        }
    }

    return theModel;
}

QStringList CameraModels::getList(void) {
    QStringList strList;

    strList.push_back(CameraPGFL3U3::modelName());
    strList.push_back(CameraBFSU328S5M::modelName());
    strList.push_back(CameraVirtual::modelName());

    return strList;
}

double CameraVirtual::acqusitionTime(int fovH, int fovV) const {
    const double totalPixels = maxPixelsH() * maxPixelsV();
    const double slowestTime = 0.01;

    return slowestTime * (fovH * fovV) / totalPixels;
}

int CameraVirtual::fovStep(void) const {
    return 1;
}

//Mode-0, 8-bit(Raw)
//150fps @ 1280x1024
//159fps @ 1280x960
//314fps @ 640x480
//610fps @ 320x240
//610fps @ 160x120
double CameraPGFL3U3::acqusitionTime(int fovH, int fovV) const {
    std::map<double, double> fpsList;

    fpsList.insert(std::pair<double, double>(1280 * 1024, 150));
    fpsList.insert(std::pair<double, double>(1280 * 960, 159));
    fpsList.insert(std::pair<double, double>(640 * 480, 314));
    fpsList.insert(std::pair<double, double>(320 * 240, 610));

    const double pixels = fovH * fovV;
    double fps = 150.0;

    if (pixels <= (328 * 240)) fps = 610.0;
    else {
        std::map<double, double>::iterator lower = fpsList.lower_bound(pixels);
        std::map<double, double>::iterator upper = fpsList.upper_bound(pixels);

        fps = (upper->second - lower->second) * pixels / (upper->first - lower->first) + lower->second;
    }

    if (fps == 0) fps = 1;

    return 1 / fps;
}

int CameraPGFL3U3::fovStep(void) const {
    return 16;
}

double CameraBasleracA244075uc::acqusitionTime(int fovH, int fovV) const {
    const int maxPixels = maxPixelsH() * maxPixelsV();
    double ratio = (1.0 * fovH * fovV) / maxPixels;

    return 1 / 75 * ratio;
}

int CameraBasleracA244075uc::fovStep(void) const {
    return 4;
}

double CameraBFSU328S5M::acqusitionTime(int fovH, int fovV) const {
    return 130;
}

int CameraBFSU328S5M::fovStep(void) const {
    return 4;
}