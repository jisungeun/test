#pragma once

#include <any>

#include <QString>
#include <QMap>

#include <arrayfire.h>

#include "TomoBuilderGlobal.h"
#include "ProgressReporter.h"
#include "StopChecker.h"

namespace TC {
    class TCFWriter;
    class ImageSet;
    class FieldRetrievalParam;
    class TomographicMappingParam;
    class DeconProgressReporter;

    class FluorescenceBuilderInterface : public QObject {
    public:
        FluorescenceBuilderInterface(ProgressReporter* reporter = new NoProgressReporter(), StopChecker* stopchecker = new NoStopChecker(), const int32_t& deviceId = 0);
        ~FluorescenceBuilderInterface();

        bool SetParameter(const QString& strPath);
        bool LoadRawImages(const QString& strPath, const int32_t& channelIndex);
        bool BuildTomogram(bool inclDeconv, bool isFirstFrameOfChannel, const int32_t& channelIndex);

        bool Save(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L);
        bool Load(const QString& strArrFile);
        bool Load(const QStringList& strArrFileList, bool& bHasFLData);

        auto GenerateMipImage(const af::array& reconimg)->af::array;
        bool SaveMIPImage(const QString& savePath, TC::Position& pos, unsigned long long timestamp = 19970101000000000L);

        QString GetChannelPath(const QString& strPath, int idx) const;

        static bool procDarkpixel(const QString& strImgPath, const QString& strOutPath);
        bool LoadDarkpixelCal(const QString& strPath);

        bool removeDarkPixel(const QString& strImgPath, const QString& strOutPath);

        void release();

        auto TransferData(std::any& data3d, std::any& dataMip, TC::MinMax& minMax3d, TC::MinMax& minMaxMip, TC::Resolution& resolution, TC::Sizes& sizes, const double& htSizeInum = 0)->void;

    protected:
        bool compensateDarkpixel(af::array& image);
        float EmissionWavelength(int channel);
        void reportProgress(int progress);
        void reportDeconvProgress(int progress);

        inline bool isStopRequested(void) const { return m_pStopChecker->isStopRequested(); }

    protected:
        QMap<QString, double> m_parameter;
        QMap<int, ImageSet*> m_mapImageSet;
        bool m_bDeconvolution;

        //to export data to tcf file
        QList<QString> m_dataList_fltomogram;
        QList<QString> m_dataList_fl_mip;

        //darkpixel calibration mask
        af::array* m_pDarkpixel_mask;
        bool m_bHasDarkPixelMask;

        friend class TCFWriter;

        int m_nCurrentProcIndex;
        QString m_strDeconvPath;

        ProgressReporter* m_pProgress;
        StopChecker* m_pStopChecker;

        int32_t deviceId;

        friend class DeconProgressReporter;
    };

}	//namespace TC