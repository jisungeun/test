#pragma once

#include <map>

#include <arrayfire.h>
#include <af/data.h>

namespace TC {
    enum {
        COLORMAP_JET,
        COLORMAP_GRAY,
        COLORMAP_BONE
    };

    class ColorMap {
    public:
        ~ColorMap();

        static ColorMap Init();

        static af::array& jet();
        static af::array& gray();
        static af::array& bone();

        static af::array& colormap(int type);

    protected:
        ColorMap();

        void prepareJet(af::array& cmap);
        void prepareGray(af::array& cmap);
        void prepareBone(af::array& cmap);

        void calcJet(const unsigned char val, unsigned char& r, unsigned char& g, unsigned char& b);
        void calcBone(const unsigned char val, unsigned char& r, unsigned char& g, unsigned char& b);

    private:
        std::map<int, af::array> m_cmap;
    };
};