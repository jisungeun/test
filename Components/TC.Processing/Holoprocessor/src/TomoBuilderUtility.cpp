#define LOGGER_TAG "TCHoloprocessor::TomoBuilderUtility"
#include <TCLogger.h>

#include <QFile>
#include <QStringList>
#include <QDir>
#include <QTime>
#include <QUuid>

#include "SystemParameter.h"
#include "TomoBuilderUtility.h"
#include "colormap.h"
#include "AfArrayProfiler.h"
#include "PhaseUnwrapperFilteredLaplacian.h"

#include <iostream>
#include <iomanip>
#include <cassert>

#include <af/macros.h>
#include <QTextStream>

#include "JobParameter.h"
#pragma warning(disable:4477)

namespace TC {
    void release(void) {
        try {
            af::deviceGC();
        } catch (af::exception & ex) {
            QLOG_INFO() << "Exception during af::deviceGC() - " << ex.what();
        }
    }

    void release(af::array& arrIn, bool bGarbeCollection) {
        arrIn = af::constant(0, 1, 1);
        arrIn.eval();
        if (bGarbeCollection) release();
    }

    void release_fft_plans(void) {
        af::setFFTPlanCacheSize(0);
        af::setFFTPlanCacheSize(1);
    }

    bool LoadFullParameter(const QString& strPath, QMap<QString, QString>& map) {
        map.clear();

        QFile file(strPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;

        while (!file.atEnd()) {
            QString line = file.readLine();

            QStringList strList = line.split(",");
            if (strList.length() != 2) continue;

            const auto& key = strList.at(0);
            const auto valueString = [=]() {
                auto str = strList.at(1);
                return str.remove('\n');
            }();

            map[key] = valueString;
        }

        return true;
    }

    bool LoadParameter(const QString& strPath, QMap<QString, double>& map) {
        map.clear();

        QFile file(strPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;

        //clear information
        SystemParameter::GetInstance()->setSerial("N/A");
        SystemParameter::GetInstance()->setHostName("N/A");
        SystemParameter::GetInstance()->setSoftwareVersion("N/A");

        map[PN_IGNORE_POOR] = 1; // set 1 as default if this parameter is not in the file...

        while (!file.atEnd()) {
            QString line = file.readLine();

            QStringList strList = line.split(",");
            if (strList.length() != 2) continue;

            const auto& key = strList.at(0);
            const auto valueString = [=]() {
                auto str = strList.at(1);
                return str.remove('\n');
            }();

            if (strList.at(0).compare(PN_SERIAL, Qt::CaseInsensitive) == 0) {
                SystemParameter::GetInstance()->setSerial(strList.at(1));
            } else if (strList.at(0).compare(PN_HOSTCOMPUTER, Qt::CaseInsensitive) == 0) {
                SystemParameter::GetInstance()->setHostName(strList.at(1));
            } else if (strList.at(0).compare(PN_SWVERSION, Qt::CaseInsensitive) == 0) {
                SystemParameter::GetInstance()->setSoftwareVersion(strList.at(1));
            } else if (valueString.compare("true", Qt::CaseInsensitive) == 0) {
                map[key] = true;
            } else if (valueString.compare("false", Qt::CaseInsensitive) == 0) {
                map[key] = false;
            } else {
                map[strList.at(0)] = strList.at(1).toDouble();
            }
        }

        return true;
    }

    bool LoadImage(const QString& strPath, af::array& outArray) {
        af::array arrImage = af::loadImage(strPath.toLocal8Bit().constData(), false);
        outArray = arrImage.as(f32);	//u8 type is not supported by fft2
        outArray.eval();

        return true;
    }

    bool LoadImages(const QString& strPath, const QString& strExt, int nImgOffset, bool bOnlyNormal, af::array& outArray, unsigned int& nImages, bool bAsFloat, bool bReverse) {
        af::dtype pixelType = (bAsFloat) ? f32 : u8;

        QDir dir(strPath);

        QStringList nameFilters;
        nameFilters << QString("*.%1").arg(strExt);

        QFileInfoList fileInfoList = dir.entryInfoList(nameFilters, QDir::Files, QDir::Name);
        unsigned int numImages = fileInfoList.size();
        nImages = numImages;
        if (numImages < 1) return false;

        if (bOnlyNormal) numImages = 0;		//! The first image is normal angle image...

        for (unsigned int i = nImgOffset; i < numImages; ++i) {
            QString strPath = fileInfoList.at(i).absoluteFilePath();

            af::array arrImage = af::loadImage(strPath.toLocal8Bit().constData(), 0);

            if (i == 0) {
                af::dim4 dim = arrImage.dims();
                outArray = af::array(dim[0], dim[1], numImages - nImgOffset, pixelType);	//u8 type is not supported by fft2
            }

            const int idx = (bReverse) ? ((numImages - nImgOffset) - (i - nImgOffset) - 1) : (i - nImgOffset);
            outArray(af::span, af::span, idx) = arrImage;
        }

        outArray.eval();

        return true;
    }

    bool LoadImagesColor(const QString& strPath, const QString& strExt, int nImgOffset, af::array& outArray, unsigned int& nImages) {
        QDir dir(strPath);

        QStringList nameFilters;
        nameFilters << QString("*.%1").arg(strExt);

        QFileInfoList fileInfoList = dir.entryInfoList(nameFilters, QDir::Files, QDir::Name);
        unsigned int numImages = fileInfoList.size();
        nImages = numImages;
        if (numImages < 1) return false;

        for (unsigned int i = nImgOffset; i < numImages; ++i) {
            QString strPath = fileInfoList.at(i).absoluteFilePath();

            af::array arrImage = af::loadImageNative(strPath.toLocal8Bit().constData());

            af::dim4 dim = arrImage.dims();
            if (i == 0) {
                outArray = af::array(dim[0], dim[1], 3, numImages - nImgOffset, u8);
            }

            const int idx = (i - nImgOffset);

            if (dim[2] == 1) {
                outArray(af::span, af::span, 0, idx) = arrImage;
                outArray(af::span, af::span, 1, idx) = arrImage;
                outArray(af::span, af::span, 2, idx) = arrImage;
            } else {
                outArray(af::span, af::span, af::span, idx) = arrImage;
            }
        }

        outArray.eval();

        return true;
    }

    bool Find(const af::array& arrIn, af::array& arrRow, af::array& arrCol) {
        af::array arrOut = af::where(arrIn);

        if (arrOut.isempty()) return false;

        const unsigned int rows = arrIn.dims(0);

        arrRow = arrOut % rows;
        arrCol = arrOut / rows;

        return true;
    }

    bool Find(const af::array& arrIn, unsigned int& nRow, unsigned int& nCol) {
        af::array arrRow, arrCol;

        if (!Find(arrIn, arrRow, arrCol)) return false;

        nRow = arrRow.scalar<unsigned int>();
        nCol = arrCol.scalar<unsigned int>();

        return true;
    }

    int Find1D(const af::array& arrIn, bool first) {
        af::array idxes = af::where(arrIn);
        if (idxes.elements() == 0) return -1;

        if (first) return idxes.scalar<unsigned>();

        return idxes(af::end).scalar<unsigned>();
    }

    void _ifftshift3(af::array& arrIn) {
        af::array arrOut;
        _ifftshift3(arrIn, arrOut);
        arrIn = arrOut;
        arrIn.eval();
    }

    void _ifftshift3(const af::array& arrIn, af::array& arrOut) {
        const int sz0 = arrIn.dims(0);
        const int sz1 = arrIn.dims(1);
        const int sz2 = arrIn.dims(2);

        arrOut = arrIn;

        af::seq s00 = af::seq(0, (sz0 / 2) - 1);
        af::seq s01 = af::seq(sz0 / 2, sz0 - 1);
        af::seq s10 = af::seq(0, (sz1 / 2) - 1);
        af::seq s11 = af::seq(sz1 / 2, sz1 - 1);
        af::seq s20 = af::seq(0, (sz2 / 2) - 1);
        af::seq s21 = af::seq(sz2 / 2, sz2 - 1);

        arrOut(s01, af::span, af::span) = arrIn(s00, af::span, af::span);
        arrOut(s00, af::span, af::span) = arrIn(s01, af::span, af::span);
        arrOut.eval();

        {
            af::array arrTmp = arrOut(af::span, s11, af::span);
            arrTmp.eval();

            arrOut(af::span, s11, af::span) = arrOut(af::span, s10, af::span);
            arrOut(af::span, s10, af::span) = arrTmp(af::span, af::span, af::span);
            arrOut.eval();
        }
        TC::release();

        {
            af::array arrTmp = arrOut(af::span, af::span, s21);
            arrTmp.eval();

            arrOut(af::span, af::span, s21) = arrOut(af::span, af::span, s20);
            arrOut(af::span, af::span, s20) = arrTmp(af::span, af::span, af::span);
            arrOut.eval();
        }
        TC::release();
    }

    bool Max(const af::array& arrIn, float& value, unsigned int& idx) {
#undef max
        af::array colarr = af::moddims(arrIn, arrIn.elements());
        af::array maxvals, maxidxs;

        af::max(maxvals, maxidxs, colarr);

        value = maxvals.scalar<float>();
        idx = maxidxs.scalar<unsigned int>();

        return true;
    }

    bool Min(const af::array& arrIn, float& value, unsigned int& idx) {
#undef min
        af::array colarr = af::moddims(arrIn, arrIn.elements());
        af::array minvals, minidxs;

        af::min(minvals, minidxs, colarr);

        value = minvals.scalar<float>();
        idx = minidxs.scalar<unsigned int>();

        return true;
    }

    bool mk_ellipse(af::array& arrOut, double XR, double YR, unsigned int X, unsigned int Y) {
        arrOut = mk_ellipse(XR, YR, X, Y);

        return true;
    }

    af::array mk_ellipse(double XR, double YR, unsigned int X, unsigned int Y) {
        af::array XX, YY, arrRes;

        if (meshgrid(XX, YY, 1, X, 1, Y)) {
            arrRes = af::pow((XX - X / 2.0) / XR, 2) + af::pow((YY - Y / 2.0) / YR, 2);
        }

        return (arrRes > 1.0);
    }

    af::array strel_disk(unsigned int radius) {
        const unsigned int XS = radius * 2 - 1;
        const unsigned int YS = radius * 2 - 1;
        const unsigned int X0 = radius - 1;
        const unsigned int Y0 = radius - 1;

        return mk_mask_circle(radius, X0, Y0, XS, YS);
    }

    af::array strel_square(unsigned int width) {
        return af::constant(1, width, width);
    }

    af::array mk_mask_circle(unsigned int R, unsigned int X0, unsigned int Y0, unsigned int XS, unsigned int YS, unsigned int CW) {
        af::array XX, YY, arrRes;

        if (meshgrid(XX, YY, 1, XS, 1, YS)) {
            arrRes = af::pow((XX - X0) / R, 2) + af::pow((YY - Y0) / R, 2);

            if (CW > 1) {
                CW = (CW % 2 == 0) ? (CW + 1) : CW;

                unsigned int hCW = (CW - 1) / 2;

                const unsigned int xl = std::max<unsigned int>(0, X0 - hCW);
                const unsigned int xr = std::min<unsigned int>(XS - 1, X0 + hCW);
                const unsigned int yt = std::max<unsigned int>(0, Y0 - hCW);
                const unsigned int yb = std::min<unsigned int>(YS - 1, Y0 + hCW);

                arrRes(af::span, af::seq(xl, xr)) = 1;
                arrRes(af::seq(yt, yb), af::span) = 1;
            }
        }

        return (arrRes > 1.0);
    }

    bool meshgrid(af::array& arrXX, af::array& arrYY, unsigned int X0, unsigned int X1, unsigned int Y0, unsigned int Y1) {
        af::array arrXGV = af::seq(X0, X1);
        af::array arrYGV = af::seq(Y0, Y1);

        return meshgrid(arrXX, arrYY, arrXGV, arrYGV);
    }

    bool meshgrid(af::array& arrXX, af::array& arrYY, const af::array& arrXGV, const af::array& arrYGV) {
        const int nRows = arrYGV.dims(0);
        const int nCols = arrXGV.dims(0);

        arrXX = af::tile(arrXGV.T(), nRows, 1);
        arrYY = af::tile(arrYGV, 1, nCols);

        return true;
    }

    //CAUTION - the function above does not work with CUDA 8.0
    bool meshgrid_square(af::array& arrXX, af::array& arrYY, const af::array& arrXGV, const af::array& arrYGV) {
        const int nRows = arrYGV.dims(0);
        const int nCols = arrXGV.dims(0);

        //arrXX = af::tile(arrXGV.T(), nRows, 1);
        arrYY = af::tile(arrYGV, 1, nCols);
        arrXX = arrYY.T();

        return true;
    }

    //[X,Y,Z] = meshgrid(xgv,ygv,zgv) produces three-dimensional coordinate arrays. 
    //The output coordinate arrays X, Y, and Z contain copies of the grid vectors xgv, ygv, and zgv respectively. 
    //The sizes of the output arrays are determined by the length of the grid vectors. 
    //For grid vectors xgv, ygv, and zgv of length M, N, and P respectively, X, Y, and Z will have N rows, M columns, and P pages.
    bool meshgrid2(af::array& outX, af::array& outY, af::array& outZ, const af::array& XGV, const af::array& YGV, const af::array& ZGV) {
        const int M = XGV.dims(0);
        const int N = YGV.dims(0);
        const int P = ZGV.dims(0);

        outX = af::tile(XGV.T(), N, 1, P);
        outY = af::tile(YGV, 1, M, P);
        outZ = af::constant(0, N, M, P);
        for (int i = 0; i < P; i++) {
            outZ(af::span, af::span, i) = af::tile(ZGV(i), N, M);
        }

        return true;
    }

    bool PhiShift(af::array& arrOut, const af::array& arrIn) {
        arrOut = PhiShift(arrIn);

        return true;
    }

    af::array PhiShift(const af::array& arrIn) {
        af::dim4 dims = arrIn.dims();

        const unsigned int ie = dims[0];
        const unsigned int je = dims[1];

        const unsigned int b1 = 1;
        const unsigned int bSize = 4;

        const unsigned int il0 = b1;
        const unsigned int il1 = b1 + bSize;
        const unsigned int ir0 = je - b1 - 1 - bSize;
        const unsigned int ir1 = je - b1 - 1;
        const unsigned int it0 = il0;
        const unsigned int it1 = il1;
        const unsigned int ib0 = ie - b1 - 1 - bSize;
        const unsigned int ib1 = ie - b1 - 1;

        const float left = af::mean<float>(arrIn(af::span, af::seq(il0, il1)));
        const float right = af::mean<float>(arrIn(af::span, af::seq(ir0, ir1)));
        const float top = af::mean<float>(arrIn(af::seq(it0, it1), af::span));
        const float bottom = af::mean<float>(arrIn(af::seq(ib0, ib1), af::span));

        const float px = (right - left) / je;
        const float py = (bottom - top) / ie;

        af::array XX, YY;
        meshgrid(XX, YY, 1, je, 1, ie);

        return (arrIn - (XX * px + YY * py));
    }

    af::array PhiShiftJH(const af::array& Uimg, const af::array& BGmaskIn) {
        dbg_saveArray("arr_Uimg", Uimg, "PhiShiftJH.arr", false);
        dbg_saveArray("arr_BGmask", BGmaskIn, "PhiShiftJH.arr", true);

        //! MATLAB:[yy, xx] = size(Uimg);
        unsigned int yy = Uimg.dims(0);
        unsigned int xx = Uimg.dims(1);

        af::array BGmask(BGmaskIn.dims(0), BGmaskIn.dims(1), f32);
        BGmask(af::span, af::span) = BGmaskIn(af::span, af::span);

        //! MATLAB:% diff를 하면 한 픽셀씩 크기가 줄어듦으로
        //! MATLAB:% diff(Uimg)에 적용할 mask도 만들어 준다.
        //! MATLAB:BGmasky = BGmask(1:end - 1, : );
        //! MATLAB:BGmaskx = BGmask(:, 1 : end - 1);
        af::array BGmasky = BGmask(af::seq(0, yy - 2), af::span);
        af::array BGmaskx = BGmask(af::span, af::seq(0, xx - 2));

        //! MATLAB:dBGy = diff(BGmask, 1, 1);
        //! MATLAB:dBGx = diff(BGmask, 1, 2);
        af::array dBGy = af::diff1(BGmask, 0);
        af::array dBGx = af::diff1(BGmask, 1);

        dbg_saveArray("arr_dBGy", dBGy, "PhiShiftJH.arr", true);
        dbg_saveArray("arr_dBGx", dBGx, "PhiShiftJH.arr", true);

        //! MATLAB:BGmasky(dBGy == -1) = 0;
        af::array idxy = (dBGy == -1);
        BGmasky = (1 - idxy) * BGmasky;

        dbg_saveArray("arr_idxy", idxy, "PhiShiftJH.arr", true);
        dbg_saveArray("arr_BGmasky", BGmasky, "PhiShiftJH.arr", true);

        //! MATLAB:BGmaskx(dBGx == -1) = 0;
        af::array idxx = (dBGx == -1);
        BGmaskx = (1 - idxx) * BGmaskx;

        dbg_saveArray("arr_idxx", idxx, "PhiShiftJH.arr", true);
        dbg_saveArray("arr_BGmaskx", BGmaskx, "PhiShiftJH.arr", true);

        //! MATLAB:dy_Uimg = diff(Uimg, 1, 1).*BGmasky;
        af::array dy_Uimg = af::diff1(Uimg, 0) * BGmasky;

        dbg_saveArray("arr_dy_Uimg", dy_Uimg, "PhiShiftJH.arr", true);

        //! MATLAB:dx_Uimg = diff(Uimg, 1, 2).*BGmaskx;
        af::array dx_Uimg = af::diff1(Uimg, 1) * BGmaskx;

        dbg_saveArray("arr_dx_Uimg", dx_Uimg, "PhiShiftJH.arr", true);

        //! MATLAB:mdy = sum(sum(dy_Uimg)). / sum(sum(BGmasky)); % y(아래)방향 평균 기울기
        const float mdy = af::sum<float>(dy_Uimg) / af::sum<float>(BGmasky);

        //! MATLAB:mdx = sum(sum(dx_Uimg)). / sum(sum(BGmaskx)); % x(우측)방향 평균 기울기
        const float mdx = af::sum<float>(dx_Uimg) / af::sum<float>(BGmaskx);

        //! MATLAB:[XX, YY] = meshgrid(1:xx, 1 : yy);
        af::array XX, YY;
        TC::meshgrid(XX, YY, 0, xx - 1, 0, yy - 1);

        dbg_saveArray("arr_XX", XX, "PhiShiftJH.arr", true);
        dbg_saveArray("arr_YY", YY, "PhiShiftJH.arr", true);

        //! MATLAB:cphi = mdy*YY + mdx*XX;
        af::array cphi = mdy * YY + mdx * XX;

        dbg_saveArray("arr_cphi", cphi, "PhiShiftJH.arr", true);

        //! MATLAB:goodimg = (Uimg - cphi);
        //! MATLAB:goodimg = goodimg - sum(sum(goodimg.*BGmask)) / sum(sum(BGmask));
        af::array goodimg = Uimg - cphi;

        dbg_saveArray("arr_goodimg_1", goodimg, "PhiShiftJH.arr", true);

        goodimg = goodimg - af::sum<float>(goodimg * BGmask) / af::sum<float>(BGmask);

        dbg_saveArray("arr_goodimg_2", goodimg, "PhiShiftJH.arr", true);

        return goodimg;
    }

    af::array PhiShiftKR3(const af::array& angimg, int rampStyle, const af::array& BGmask, const char* logname) {
        double mdx, mdy;
        return PhiShiftKR3(angimg, rampStyle, BGmask, mdx, mdy, logname);
    }

    af::array PhiShiftKR3(const af::array& angimg, int rampStyle, const af::array& BGmask, double& mdx, double& mdy, const char* logname) {
        const char* fname_out = logname;

        dbg_saveArray("arr_angimg", angimg, fname_out, false);
        dbg_saveArray("arr_rampStyle", af::constant<int>(rampStyle, 1), fname_out, true);
        dbg_saveArray("arr_BGmask", BGmask, fname_out, true);

        af::array goodimage;

        if (rampStyle == 1) {
            //! MATLAB: [yy, xx] = size(angimg);
            af::dim4 dims = angimg.dims();
            const int yy = dims.dims[0];
            const int xx = dims.dims[1];

            //! MATLAB: BGmasky = BGmask(2:end, : ).*circshift(BGmask(1:end - 1, : ), [0, 1]);
            //! MATLAB: BGmaskx = BGmask(:, 2 : end).*circshift(BGmask(:, 1 : end - 1), [1, 0]);
            af::array BGmasky = BGmask(af::seq(1, af::end), af::span) * af::shift(BGmask(af::seq(0, af::end - 1), af::span), 0, 1);
            af::array BGmaskx = BGmask(af::span, af::seq(1, af::end)) * af::shift(BGmask(af::span, af::seq(0, af::end - 1)), 1, 0);

            dbg_saveArray("arr_BGmasky", BGmasky, fname_out, true);
            dbg_saveArray("arr_BGmaskx", BGmaskx, fname_out, true);

            //! MATLAB: dy_angimg = angle(exp(-1i * diff(angimg, 1, 1))).*BGmasky;
            //! MATLAB: dx_angimg = angle(exp(1i * diff(angimg, 1, 2))).*BGmaskx;
            af::array dy_angimg = angle(af::exp((-1) * af::complex(0, af::diff1(angimg, 0)))) * BGmasky;
            af::array dx_angimg = angle(af::exp(af::complex(0, af::diff1(angimg, 1)))) * BGmaskx;

            dbg_saveArray("arr_dy_angimg", dy_angimg, fname_out, true);
            dbg_saveArray("arr_dx_angimg", dx_angimg, fname_out, true);

            //! MATLAB: mdy = sum(sum(dy_angimg(2:end - 1, 2 : end - 1))). / sum(sum(BGmasky(2:end - 1, 2 : end - 1)));
            //! MATLAB: mdx = sum(sum(dx_angimg(2:end - 1, 2 : end - 1))). / sum(sum(BGmaskx(2:end - 1, 2 : end - 1)));
            mdy = af::sum<float>(dy_angimg(af::seq(1, af::end - 1), af::seq(1, af::end - 1))) / af::sum<float>(BGmasky(af::seq(1, af::end - 1), af::seq(1, af::end - 1)));
            mdx = af::sum<float>(dx_angimg(af::seq(1, af::end - 1), af::seq(1, af::end - 1))) / af::sum<float>(BGmaskx(af::seq(1, af::end - 1), af::seq(1, af::end - 1)));

            dbg_saveArray("arr_mdy", af::constant<double>(mdy, 1), fname_out, true);
            dbg_saveArray("arr_mdx", af::constant<double>(mdx, 1), fname_out, true);

            //! MATLAB: [XX, YY] = meshgrid(1:xx, 1 : yy);
            af::array XX, YY;
            meshgrid_square(XX, YY, af::seq(0, xx - 1), af::seq(0, yy - 1));

            dbg_saveArray("arr_XX_1", XX, fname_out, true);
            dbg_saveArray("arr_YY_1", YY, fname_out, true);

            //! MATLAB: XX = XX - (floor(xx / 2) + 1);
            XX = XX - (std::floor(xx / 2.0) + 1);

            //! MATLAB: YY = (floor(yy / 2) + 1) - YY;
            YY = (std::floor(yy / 2.0) + 1) - YY;

            dbg_saveArray("arr_XX_2", XX, fname_out, true);
            dbg_saveArray("arr_YY_2", YY, fname_out, true);

            //! MATLAB: cphi = mdy*YY + mdx*XX;
            af::array cphi = mdy * YY + mdx * XX;

            dbg_saveArray("arr_cphi", cphi, fname_out, true);

            //! MATLAB: goodimg = angle(exp(1i.*(angimg - cphi)));
            goodimage = angle(af::exp(af::complex(0, angimg - cphi)));

            dbg_saveArray("arr_goodimage", goodimage, fname_out, true);
        } else if (rampStyle == 2) {
            //! MATLAB: [yy, xx] = size(angimg);
            //! MATLAB: [XX, YY] = meshgrid(1:xx, 1 : yy);
            //! MATLAB: XX = XX - (floor(xx / 2) + 1);
            //! MATLAB: YY = (floor(yy / 2) + 1) - YY;

            //! MATLAB: tl_img = double(angimg.*BGmask);

            //! MATLAB: for ap = 0:0.1 : pi;
            //! MATLAB:   tl_img = angle(exp(1i * (tl_img + ap)));
            //! MATLAB:   tl_img = tl_img.*BGmask;
            //! MATLAB:		if max(max(abs(tl_img))) < 3.13
            //! MATLAB:			break;
            //! MATLAB:		end
            //! MATLAB: end

            //! MATLAB: Dy = abs([diff(tl_img, 1, 1); zeros(1, xx)]) > 6;
            //! MATLAB: Dx = abs([diff(tl_img, 1, 2), zeros(yy, 1)]) > 6;
            //! MATLAB: Dy2 = Dy + circshift(Dy, [1, 0]);
            //! MATLAB: Dx2 = Dx + circshift(Dx, [0, 1]);
            //! MATLAB: Dxy = find(boolean(Dy2 + Dx2));

            //! MATLAB: r = 10;
            //! MATLAB: Disk = find(~mk_ellipse(r, r, xx, yy)) - (floor(xx / 2) + 1)*yy + (floor(yy / 2) + 1);

            //! MATLAB: Dxy2d = repmat(Dxy, [1, numel(Disk)]) + repmat(Disk(:).',[numel(Dxy),1]);
            //! MATLAB: vDxy2d = (Dxy2d(:) > 0).*(Dxy2d(:) < xx*yy);
            //! MATLAB: temp = zeros(yy, xx);
            //! MATLAB: temp(Dxy2d(vDxy2d > 0)) = 1;

            //! MATLAB: [sect, maxnum] = bwlabel(~temp);
            //! MATLAB: numsect = zeros(maxnum, 1);
            //! MATLAB: for uu = 1:1 : maxnum
            //! MATLAB: 	numsect(uu) = numel(find(sect == uu));
            //! MATLAB: end
            //! MATLAB: [~, bb] = max(numsect);
            //! MATLAB: NBGmask = BGmask .*(sect == bb);

            //! MATLAB: XXbg = XX.*NBGmask;
            //! MATLAB: YYbg = YY.*NBGmask;

            //! MATLAB: myFit = @(a, b, c, x, y) a.*x + b.*y + c;
            //! MATLAB: vv = find(NBGmask);
            //! MATLAB: [FitResult, ~, ~] = fit([XXbg(vv), YYbg(vv)], tl_img(vv), myFit, 'StartPoint', [0, 0, 0]);
            //! MATLAB: coeffFitResult = coeffvalues(FitResult);

            //! MATLAB: mdx = coeffFitResult(1);
            //! MATLAB: mdy = coeffFitResult(2);

            //! MATLAB: cphi = mdy*YY + mdx*XX;
            //! MATLAB: goodimg = angle(exp(1i * (angimg - cphi)));
        }

        return goodimage;
    }

    bool angle(af::array& arrOut, const af::array& arrIn) {
        arrOut = angle(arrIn);

        return true;
    }

    af::array angle(const af::array& arrIn) {
        return af::atan2(af::imag(arrIn), af::real(arrIn));
    }

    //REF : https://en.wikipedia.org/wiki/Complex_number#Square_root
    //      The square roots of a + bi (with b ≠ 0) are  +/-(g + di), where
    //      g = sqrt((a + sqrt(a^2+b^2))/2)
    //      d = sgn(b) * sqrt((-a + sqrt(a^2+b^2))/2)
    bool sqrt(af::array& arrGamma, af::array& arrDelta, const af::array& arrIn) {
        af::array arrReal = af::real(arrIn);
        af::array arrImag = af::imag(arrIn);

        arrReal.eval();
        arrImag.eval();

        //sgn(b) = sign of b
        //NOTE: the output of af::sign(array): 1 - negative 0 - positive
        af::array sgn_b = 1 - 2 * af::sign(arrImag);
        sgn_b.eval();

        af::array a2_b2 = af::pow(arrReal, 2) + af::pow(arrImag, 2);
        af::array sqrtInside = af::sqrt(a2_b2);
        sqrtInside.eval();

        arrGamma = af::sqrt((arrReal + sqrtInside) / 2);
        arrDelta = sgn_b * af::sqrt(((-1) * arrReal + sqrtInside) / 2);

        //Handle NaN
        nan2zero(arrGamma);
        nan2zero(arrDelta);
        TC::release();

        return true;
    }

    bool sqrt(af::array& arrOut, const af::array& arrIn) {
        af::array arrGamma, arrDelta;
        bool bRet = sqrt(arrGamma, arrDelta, arrIn);

        arrOut = af::complex(arrGamma, arrDelta);

        return bRet;
    }

    af::array sqrt_real(af::array& arrIn) {
        af::array arrOut = af::sqrt(arrIn);

        af::array arrNon = af::isNaN(arrOut);
        if (!af::allTrue<char>(arrNon == 0)) arrOut(arrNon) = 0;

        return arrOut;
    }

    void nan2zero(af::array& arrIn) {
        const int dims = arrIn.numdims();

        if (dims < 3) {
            af::array arrNon = af::isNaN(arrIn);
            if (!af::allTrue<char>(arrNon == 0)) arrIn(arrNon) = 0;
        } else {
            //below codes are for saving memory usage....
            const int d2 = arrIn.dims(2);
            //gfor(af::seq kk, d2)
            for (int kk = 0; kk < d2; kk++) {
                af::array subArr = arrIn(af::span, af::span, kk);
                af::array arrNon = af::isNaN(subArr);
                if (!af::allTrue<char>(arrNon == 0)) {
                    subArr(arrNon) = 0;
                    arrIn(af::span, af::span, kk) = subArr(af::span, af::span);
                }
            }
        }

        arrIn.eval();
    }

    float otsuthresh(const af::array& arrIn, const int bins) {
        const float maxVal = af::max<float>(arrIn);

        const unsigned int total = arrIn.elements();
        af::array hist = af::histogram(arrIn, bins, 0.0f, maxVal);

        af::array wts = af::range(256);
        af::array wtB = af::accum(hist);
        af::array wtF = total - wtB;
        af::array sumB = af::accum(wts * hist);
        af::array meanB = sumB / wtB;

        float lastElemInSumB;
        sumB(af::seq(255, 255, 1)).host((void*)&lastElemInSumB);

        af::array meanF = (lastElemInSumB - sumB) / wtF;
        af::array mDiff = meanB - meanF;
        af::array interClsVar = wtB * wtF * mDiff * mDiff;

        const float max = af::max<float>(interClsVar);
        const float threshold2 = where(interClsVar == max).scalar<unsigned>();
        af::array threshIdx = where(interClsVar >= max);
        const float threshold1 = threshIdx.elements() > 0 ? threshIdx.scalar<unsigned>() : 0.0f;

        return (threshold1 + threshold2) / 2.0f / bins * maxVal;
    }

    af::array pow2complex(const af::array& arrIn) {
        af::array arrReal = af::real(arrIn);
        af::array arrImag = af::imag(arrIn);

        return af::complex(af::pow(arrReal, 2) - af::pow(arrImag, 2), 2 * arrReal * arrImag);
    }

    bool unique(af::array& arrVal, af::array& arrIdx, const af::array& arrIn) {
#undef max

        // where value indicies
        af::array arrFlat = af::flat(arrIn);
        af::array Is, Ix;
        af::sort(Is, Ix, arrFlat, 0, true);

        af::array df1 = af::constant(1, arrFlat.elements());
        af::array df2 = af::diff1(flat(Is));
        af::array df3 = (df2 != 0);
        df1(af::seq(1, af::end)) = df3;

        af::array idx = af::where(df1);
        arrVal = Is(idx);
        arrIdx = Ix(idx);

        arrVal.eval();
        arrIdx.eval();

        return true;
    }

    af::array uniqueVal(const af::array& arrIn) {
        af::array arrFlat = af::flat(arrIn);
        return af::setUnique(arrFlat);
    }

    af::array uniqueIdx(const af::array& arrIn) {
        af::array arrVal, arrIdx;
        unique(arrVal, arrIdx, arrIn);
        return arrIdx;
    }

    af::array maskarray2d(const af::array& arrIdx, int d0, int d1) {
        af::array out = af::constant(0, d0, d1, s32);

        const int elems = arrIdx.dims(0);

        af::array row = af::mod(arrIdx, d0);
        af::array col_tmp = af::floor(arrIdx / d0);
        af::array col(elems, s32);
        col(af::span) = col_tmp(af::span);

        int* hRow = new int[elems];
        int* hCol = new int[elems];

        row.host(hRow);
        col.host(hCol);

        for (int ii = 0; ii < elems; ii++) {
            out(hRow[ii], hCol[ii]) = 1;
        }

        delete[] hRow;
        delete[] hCol;

        return out;
    }

    af::array unwrap2(const af::array& arrIn, bool& bGoodQuality, int& score) {
        return PhaseUnwrapperFilteredLaplacian().execute(arrIn, bGoodQuality, score);
    }

    af::array imagesc(const af::array arrIn) {
#undef min
#undef max
        af::array& jetColormap = ColorMap::jet();

        float minv, maxv;
        unsigned int minidx, maxidx;

        TC::Min(arrIn, minv, minidx);
        TC::Max(arrIn, maxv, maxidx);

        const int nRows = arrIn.dims(0);
        const int nCols = arrIn.dims(1);

        af::array arrOut(nRows, nCols, 3, f32);

        af::array pidx(nRows, nCols, u8);
        pidx(af::span, af::span) = af::min(255, af::floor((arrIn - minv) / (maxv - minv) * 255));

        for (int i = 0; i < nCols; i++) {
            af::array arrTmp = af::lookup(jetColormap, pidx(af::span, i), 0);
            arrOut(af::span, i, af::span) = af::moddims(arrTmp, nRows, 1, 3);
        }

        return arrOut;
    }

    af::array phase2rgb(const af::array& arrIn, int mode, float fmax) {
#undef min
        af::array& jetColormap = ColorMap::jet();

        const int nRows = arrIn.dims(0);
        const int nCols = arrIn.dims(1);

        af::array arrOut(nRows, nCols, 3, f32);

        af::array pidx(nRows, nCols, u8);
        if (mode == 0) {
            pidx(af::span, af::span) = af::min(255, af::floor((arrIn / (fmax * 2)) * 255 + 127));
        } else {
            const float fmin = af::min<float>(arrIn);
            pidx(af::span, af::span) = af::min(255, af::floor(((arrIn - fmin) / (fmax - fmin)) * 255));
        }

        for (int i = 0; i < nCols; i++) {
            af::array arrTmp = af::lookup(jetColormap, pidx(af::span, i), 0);
            arrOut(af::span, i, af::span) = af::moddims(arrTmp, nRows, 1, 3);
        }

        return arrOut;
    }

    af::array amplitue2rgb(const af::array& arrIn) {
#undef min
        af::array& grayColormap = ColorMap::gray();

        const int nRows = arrIn.dims(0);
        const int nCols = arrIn.dims(1);

        af::array arrOut(nRows, nCols, 3, f32);

        af::array pidx(nRows, nCols, u8);
        pidx(af::span, af::span) = af::min(255, af::floor((arrIn / 2) * 255));

        for (int i = 0; i < nCols; i++) {
            af::array arrTmp = af::lookup(grayColormap, pidx(af::span, i), 0);
            arrOut(af::span, i, af::span) = af::moddims(arrTmp, nRows, 1, 3);
        }

        return arrOut;
    }

    af::array phase2dic(const af::array& arrIn, float fmax, unsigned int nShift) {
#undef min
        if (nShift == 0) nShift = 1;

        af::array& grayColormap = ColorMap::gray();

        const int nRows = arrIn.dims(0);
        const int nCols = arrIn.dims(1);

        af::array DIC = arrIn - af::shift(arrIn, nShift, nShift);
        DIC = DIC(af::seq(nShift - 1, nRows - nShift), af::seq(nShift - 1, nCols - nShift));

        const int nRows1 = DIC.dims(0);
        const int nCols1 = DIC.dims(1);

        af::array pidx(nRows1, nCols1, u8);
        pidx(af::span, af::span) = af::min(255, af::floor((DIC / (fmax * 2) * 255) + 127));

        af::array arrOut(nRows1, nCols1, 3, f32);

        for (int i = 0; i < nCols1; i++) {
            af::array arrTmp = af::lookup(grayColormap, pidx(af::span, i), 0);
            arrOut(af::span, i, af::span) = af::moddims(arrTmp, nRows1, 1, 3);
        }

        return arrOut;
    }

    af::array phase2contrast(const af::array& arrAmp, const af::array& arrPha, double dOffset) {
#undef min
        af::array& boneColormap = ColorMap::bone();

        af::array PC = af::abs(arrAmp) * af::abs(dOffset + arrPha);
        float fmax = (af::max<float>(PC) + 1) / 2;

        const int nRows = PC.dims(0);
        const int nCols = PC.dims(1);

        af::array pidx(nRows, nCols, u8);
        pidx(af::span, af::span) = af::min(255, af::floor((PC / (fmax * 2) * 255) + 127));

        af::array arrOut(nRows, nCols, 3, f32);

        for (int i = 0; i < nCols; i++) {
            af::array arrTmp = af::lookup(boneColormap, pidx(af::span, i), 0);
            arrOut(af::span, i, af::span) = af::moddims(arrTmp, nRows, 1, 3);
        }

        return arrOut;
    }

    //ref - http://www.arrayfire.com/docs/image_processing_2binary_thresholding_8cpp-example.htm
    af::array threshold(const af::array& in, float thresholdValue) {
        af::array ret_val = in.copy();
        ret_val = (ret_val < thresholdValue) * 0.0f + 1.0f * (ret_val > thresholdValue);
        return ret_val;
    }

    //ref - http://www.arrayfire.com/docs/image_processing_2binary_thresholding_8cpp-example.htm
    float graythresh(const af::array& in) {
        unsigned total = in.elements();
        af::array hist = histogram(in, 512, 0.0f, 1.0f);

        af::array wts = af::range(512);
        af::array wtB = accum(hist);
        af::array wtF = total - wtB;
        af::array sumB = accum(wts * hist);
        af::array meanB = sumB / wtB;

        float lastElemInSumB;
        sumB(af::seq(511, 511, 1)).host((void*)&lastElemInSumB);

        af::array meanF = (lastElemInSumB - sumB) / wtF;
        af::array mDiff = meanB - meanF;
        af::array interClsVar = wtB * wtF * mDiff * mDiff;
        const float maxv = af::max<float>(interClsVar);
        const float threshold2 = where(interClsVar == maxv).scalar<unsigned>();

        af::array threshIdx = where(interClsVar >= maxv);
        const float threshold1 = threshIdx.elements() > 0 ? threshIdx.scalar<unsigned>() : 0.0f;

        return ((threshold1 + threshold2) / 2.0f) / 512.0f;
    }

    //ref - http://www.arrayfire.com/docs/image_processing_2binary_thresholding_8cpp-example.htm
    af::array im2bw(const af::array& in, float level) {
        return threshold(in, level);
    }

    //! MATLAB: K>> A = fspecial('disk',1)
    //! MATLAB: A =
    //! MATLAB: 	0.0251    0.1453    0.0251
    //! MATLAB: 	0.1453    0.3183    0.1453
    //! MATLAB: 	0.0251    0.1453    0.0251
    af::array fspecial_disk_1() {
        float hIn[] = { 0.0251f, 0.1453f, 0.0251f, 0.1453f, 0.3183f, 0.1453f, 0.0251f, 0.1453f, 0.0251f };
        af::array out(3, 3, hIn);

        return out;
    }

    //! MATLAB: >> A = fspecial('disk',2)
    //! MATLAB: A =
    //! MATLAB: 	     0    0.0170    0.0381    0.0170         0
    //! MATLAB: 	0.0170    0.0784    0.0796    0.0784    0.0170
    //! MATLAB: 	0.0381    0.0796    0.0796    0.0796    0.0381
    //! MATLAB: 	0.0170    0.0784    0.0796    0.0784    0.0170
    //! MATLAB: 	     0    0.0170    0.0381    0.0170         0
    af::array fspecial_disk_2() {
        float hIn[] = { 0.f, 0.0170f, 0.0381f, 0.0170f, 0.f,
         0.0170f, 0.0784f, 0.0796f, 0.0784f, 0.0170f,
         0.0381f, 0.0796f, 0.0796f, 0.0796f, 0.0381f,
         0.0170f, 0.0784f, 0.0796f, 0.0784f, 0.0170f,
         0.f, 0.0170f, 0.0381f, 0.0170f, 0.f };
        af::array out(5, 5, hIn);

        return out;
    }

    af::array zeropadding_2d(af::array& arrIn, unsigned int nSize, bool bShift) {
        const unsigned int dimSZ = arrIn.numdims();
        if (dimSZ != 2) return arrIn;

        const unsigned int dim0 = arrIn.dims(0);
        const unsigned int dim1 = arrIn.dims(1);

        const unsigned int nzsr = nSize - dim0;
        const unsigned int nzsc = nSize - dim1;

        af::array arrIn1;
        if (bShift) arrIn1 = fftshift(arrIn);
        else        arrIn1 = arrIn;

        const unsigned int m0 = dim0 / 2 - 1;
        const unsigned int n0 = dim1 / 2 - 1;
        const unsigned int m1 = dim0 - 1;
        const unsigned int n1 = dim1 - 1;

        //quadrants of the FFT, starting in the upper left
        af::array q1 = arrIn1(af::seq(0, m0), af::seq(0, n0));
        af::array q2 = arrIn1(af::seq(0, m0), af::seq(n0 + 1, n1));
        af::array q3 = arrIn1(af::seq(m0 + 1, m1), af::seq(n0 + 1, n1));
        af::array q4 = arrIn1(af::seq(m0 + 1, m1), af::seq(0, n0));

        //zpdFy = [[q1; zpdr; q4], zpdc, [q2; zpdr; q3]]; % insert the zeros
        const unsigned int M0 = 0;					const unsigned int N0 = 0;
        const unsigned int M1 = m0;					const unsigned int N1 = n0;
        const unsigned int M2 = nSize - (dim0 / 2);	const unsigned int N2 = nSize - (dim1 / 2);
        const unsigned int M3 = nSize - 1;			const unsigned int N3 = nSize - 1;

        af::array arrOut = af::constant<af::cfloat>(0, nSize, nSize, c32);
        arrOut(af::seq(M0, M1), af::seq(N0, N1)) = q1(af::span, af::span);
        arrOut(af::seq(M2, M3), af::seq(N0, N1)) = q4(af::span, af::span);
        arrOut(af::seq(M0, M1), af::seq(N2, N3)) = q2(af::span, af::span);
        arrOut(af::seq(M2, M3), af::seq(N2, N3)) = q3(af::span, af::span);

        return arrOut;
    }

    af::array interp_z(af::array& arrIn, unsigned int new_z, int xyratio) {
        const unsigned int dim0 = arrIn.dims(0);
        const unsigned int dim1 = arrIn.dims(1);
        const unsigned int dim2 = arrIn.dims(2);

        const unsigned int new_d0 = dim0 * xyratio;
        const unsigned int new_d1 = dim1 * xyratio;
        const unsigned int new_d2 = (dim2 == new_z) ? dim2 : (new_z + (new_z % 2));;

        af::array arr_padded;
        {
            af::array arr_fft = fftshift3(af::fft3(arrIn));
            TC::release_fft_plans();
            TC::release();

            arr_padded = af::constant<af::cfloat>(0, new_d0, new_d1, new_d2, c32);
            af::seq s0 = af::seq(std::round((new_d0 - dim0) / 2), std::round((new_d0 - dim0) / 2) + dim0 - 1);
            af::seq s1 = af::seq(std::round((new_d1 - dim1) / 2), std::round((new_d1 - dim1) / 2) + dim1 - 1);
            af::seq s2 = af::seq(std::round((new_d2 - dim2) / 2), std::round((new_d2 - dim2) / 2) + dim2 - 1);

            arr_padded(s0, s1, s2) = arr_fft;
            arr_padded.eval();
        }

        arr_padded = ifftshift3(arr_padded);
        arr_padded.eval();
        TC::release(arrIn, true);

        arr_padded = af::ifft3(arr_padded);
        arr_padded.eval();
        TC::release_fft_plans();

        return af::real(arr_padded / (dim0 * dim1 * dim2 * 1.0) * (new_d0 * new_d1 * new_d2));
    }

    af::array interp_xy2z(af::array& arrIn, unsigned int new_z) {
        return interp_z(arrIn, new_z, 2);
    }

    void hilbert2(const af::array& arrIn, double pixelsize, double NA, double lambda, unsigned int order, af::array& Pimg, double& mx, double& my, const char* logname) {
#undef max
        const char* fname_out = logname;

        dbg_saveArray("arr_In", arrIn, fname_out, false);

        //! MATLAB: Rank = order
        unsigned int Rank = order;

        dbg_saveArray("arr_Rank", af::constant<unsigned int>(Rank, 1), fname_out, true);

        //! MATLAB: [yy, xx] = size(img);
        af::dim4 dims = arrIn.dims();
        const int yy = dims.dims[0];
        const int xx = dims.dims[1];

        //! MATLAB: r = xx*pixelsize*NA / lambda;  
        //! % insert : give the appropriate value corresponds to the numerical aperture / lambda
        const double r = xx * pixelsize * NA / lambda;

        dbg_saveArray("arr_r", af::constant<double>(r, 1), fname_out, true);

        //! MATLAB: yr = r*yy / xx;
        const double yr = r * yy / xx;

        dbg_saveArray("arr_yr", af::constant<double>(yr, 1), fname_out, true);

        //! MATLAB: Fimg = fftshift(fft2(ifftshift(img))); %FFT
        af::array Fimg = fftshift(af::fft2(ifftshift(arrIn)));

        dbg_saveArray("arr_Fimg", Fimg, fname_out, true);

        //! MATLAB: previous_mvalue = max(max(abs(Fimg)));
        float previous_mvalue = af::max<float>(af::abs(Fimg));

        dbg_saveArray("arr_previous_mvalue", af::constant<float>(previous_mvalue, 1), fname_out, true);


        af::array Fimg2;

        //! MATLAB: while Rank >= 0;
        unsigned int iter = 0;
        for (unsigned int kk = 0; kk <= Rank; iter++) {
            dbg_saveArrayIdx("arr_kk_prev", iter + 1, af::constant<unsigned int>(kk, 1), fname_out, true);

            //! MATLAB: % find the modulation spatial frequency
            //! MATLAB: [mvalue, mV] = max(abs(Fimg(:)));
            //! MATLAB: my = mod(mV, yy);
            //! MATLAB: mx = (mV - my) / yy + 1;
            af::array absFimg = af::abs(Fimg);
            absFimg.eval();

            dbg_saveArrayIdx("arr_absFimg", iter + 1, absFimg, fname_out, true);

            float mvalue = af::max<float>(absFimg);

            dbg_saveArrayIdx("arr_mvalue", iter + 1, af::constant<float>(mvalue, 1), fname_out, true);

            unsigned int mxIdx, myIdx;
            af::array absFimgMax = (absFimg == mvalue);
            absFimgMax.eval();

            Find(absFimgMax, myIdx, mxIdx);

            dbg_saveArrayIdx("arr_mx_1", iter + 1, af::constant<unsigned int>(mxIdx, 1), fname_out, true);
            dbg_saveArrayIdx("arr_my_1", iter + 1, af::constant<unsigned int>(myIdx, 1), fname_out, true);

            //! MATLAB: my = my - (floor(yy / 2) + 1);
            //! MATLAB: mx = mx - (floor(xx / 2) + 1);
            my = myIdx - (std::floor(yy / 2.0));
            mx = mxIdx - (std::floor(xx / 2.0));

            dbg_saveArrayIdx("arr_mx_2", iter + 1, af::constant<int>(mx, 1), fname_out, true);
            dbg_saveArrayIdx("arr_my_2", iter + 1, af::constant<int>(my, 1), fname_out, true);

            //! MATLAB: % select modulated information
            //! MATLAB: c0mask = ~(mk_ellipse(r, yr, xx, yy));
            af::array c0mask = !(mk_ellipse(r, yr, xx, yy));

            dbg_saveArrayIdx("arr_c0mask", iter + 1, c0mask, fname_out, true);

            //! MATLAB: c1mask = circshift(c0mask, [my, mx]);
            af::array c1mask = af::shift(c0mask, my, mx);

            dbg_saveArrayIdx("arr_c1mask", iter + 1, c1mask, fname_out, true);

            //! MATLAB: % shift to DC
            //! MATLAB: % apply c3mask  to Fimg, and then shift masked information to the center
            //! MATLAB: Fimg2 = circshift(Fimg, [-my, -mx]).*c0mask;
            Fimg2 = af::shift(Fimg, (-1) * my, (-1) * mx) * c0mask;

            dbg_saveArrayIdx("arr_Fimg2", iter + 1, Fimg2, fname_out, true);

            //! MATLAB: Fimg = Fimg.*(~c1mask);
            Fimg = Fimg * !(c1mask);

            dbg_saveArrayIdx("arr_Fimg", iter + 1, Fimg, fname_out, true);

            //! MATLAB: if previous_mvalue == mvalue
            //! MATLAB:		Rank = Rank - 1;
            //! MATLAB: end
            if (std::abs(previous_mvalue - mvalue) < 1.0) //if (previous_mvalue == mvalue)
            {
                kk = kk + 1;
            }

            dbg_saveArrayIdx("arr_kk_next", iter + 1, af::constant<unsigned int>(kk, 1), fname_out, true);

            //! MATLAB: previous_mvalue = mvalue;
            previous_mvalue = mvalue;
            dbg_saveArrayIdx("arr_previous_mvalue", iter + 1, af::constant<float>(previous_mvalue, 1), fname_out, true);
        } //! MATLAB: end

        dbg_saveArray("arr_iter", af::constant<unsigned int>(iter, 1), fname_out, true);
        dbg_saveArray("arr_Fimg2", Fimg2, fname_out, true);

        //! MATLAB: % inverse FFT
        //! MATLAB: Pimg = fftshift(ifft2(ifftshift(Fimg2)));
        //! MATLAB: % insert : Inverse fourier transform of Fimg
        Pimg = fftshift(af::ifft2(ifftshift(Fimg2)));

        dbg_saveArray("arr_Pimg", Pimg, fname_out, true);

        //! MATLAB:my = -my(1);
        //! MATLAB:mx = mx(1);
        my = -1 * my;

        dbg_saveArray("arr_mx_final", af::constant<double>(mx, 1), fname_out, true);
        dbg_saveArray("arr_my_final", af::constant<double>(my, 1), fname_out, true);
    }

    af::array linspace(double x1, double x2, unsigned int n) {
        af::array result = af::constant(0, n);

        const double ratio = (x2 - x1) / (n - 1);
        for (unsigned int i = 0; i < n; i++)
            result(i) = x1 + i * ratio;

        return result;
    }

    int saveVal(const char* key, float value, const char* path, bool append) {
        return af::saveArray(key, af::constant(value, 1, f32), path, append);
    }

    int saveVal(const char* key, double value, const char* path, bool append) {
        return af::saveArray(key, af::constant(value, 1, f64), path, append);
    }

    int saveVal(const char* key, int value, const char* path, bool append) {
        return af::saveArray(key, af::constant(value, 1, s32), path, append);
    }

    int saveVal(const char* key, unsigned int value, const char* path, bool append) {
        return af::saveArray(key, af::constant(value, 1, u32), path, append);
    }

    int saveVal(const char* key, unsigned long long value, const char* path, bool append) {
        return af::saveArray(key, af::constant(value, 1, u64), path, append);
    }

    void imwrite(const af::array& arrIn, const char* filename) {
        af::saveImage(filename, arrIn);
    }

    bool exportRAW(af::array& image, const QString& strPath) {
        size_t size = image.bytes();

        char* pdata = new char[size];
        image.host(pdata);

        QFile file(strPath);
        if (!file.open(QIODevice::WriteOnly)) {
            QLOG_ERROR() << "Faile to open " << strPath;
            return false;
        }

        file.write(pdata, size);

        delete[] pdata;

        return true;
    }

    bool importRAW(af::array& image, const QString& strPath, int d0, int d1, int d2) {
        QFile file(strPath);

        if (!file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Faile to open " << strPath;
            return false;
        }

        QByteArray blob = file.readAll();

        image = af::array(d0, d1, d2, blob.constData());
        image.eval();

        return true;
    }

    bool importRAW32(af::array& image, const QString& strPath, float& minV, float& maxV, int d0, int d1, int d2) {
        QFile file(strPath);

        if (!file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Faile to open " << strPath;
            return false;
        }

        QByteArray blob = file.readAll();

        image = af::array(d0, d1, d2, (float*)blob.constData());
        image.eval();

        af::sync();

        minV = af::min<float>(image);
        maxV = af::max<float>(image);

        return true;
    }

    bool importRAW32_8(af::array& image, const QString& strPath, int d0, int d1, int d2) {
        QFile file(strPath);

        if (!file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Faile to open " << strPath;
            return false;
        }

        QByteArray blob = file.readAll();

        image = af::array(d0, d1, d2, (float*)blob.constData());
        image.eval();

        af::sync();

        float max_val = af::max<float>(image);
        float min_val = af::min<float>(image);

        image = image * 255 / max_val;
        image = image.as(u8);
        image.eval();

        return true;
    }

    bool importRAW32_16(af::array& image, const QString& strPath, int d0, int d1, int d2) {
        QFile file(strPath);

        if (!file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Faile to open " << strPath;
            return false;
        }

        QByteArray blob = file.readAll();

        image = af::array(d0, d1, d2, (float*)blob.constData());
        image.eval();

        af::sync();

#if 0   //TOMV-1490
        float max_val = af::max<float>(image);
        float min_val = af::min<float>(image);

        image = image * 255 / max_val;
#endif

        image = image.as(u16);
        image.eval();

        return true;
    }

    QStringList getDataList(const QString& strPath) {
        QDir dir(strPath);

        QStringList dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        return dirList;
    }

    unsigned long long loadTimestamp(const QString& strPath) {
        QString line = TC::loadTimestampStr(strPath);
        return line.toULongLong();
    }

    QString loadTimestampStr(const QString& strPath) {
        QFile file(strPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return "19970101000000000";

        QTextStream in(&file);
        QString line = in.readLine();

        return line;
    }

    QString TimeStamp2String(uint64_t timestamp) {
        QDateTime datetime = TimeStamp2DateTime(timestamp);
        return datetime.toString("yyyy-MM-dd hh:mm:ss.zzz");
    }

    QDateTime TimeStamp2DateTime(uint64_t timestamp) {
        const QString s = QString::number(timestamp);

        const QString year = s.mid(0, 4);
        const QString month = s.mid(4, 2);
        const QString day = s.mid(6, 2);
        const QString hour = s.mid(8, 2);
        const QString minute = s.mid(10, 2);
        const QString second = s.mid(12, 2);
        const QString millisecond = s.mid(14, 3);

        const QDate date(year.toInt(), month.toInt(), day.toInt());
        const QTime time(hour.toInt(), minute.toInt(), second.toInt(), millisecond.toInt());

        return QDateTime(date, time);
    }

    Position loadPosition(const QString& strPath) {
        Position pos;

        QFile file(strPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return pos;

        QTextStream in(&file);
        QString line;

        if (in.readLineInto(&line)) pos.x = line.toDouble();
        if (in.readLineInto(&line)) pos.y = line.toDouble();
        if (in.readLineInto(&line)) pos.z = line.toDouble();
        if (in.readLineInto(&line)) pos.c = line.toDouble();

        return pos;
    }

    QString LoadComment(const QString& commentPath) {
        QFile tmpFile(commentPath);
        if (!tmpFile.exists()) return QString(" ");

        if (!tmpFile.open(QIODevice::ReadOnly | QIODevice::Text)) return QString(" ");

        QTextStream inStream(&tmpFile);
        inStream.setCodec("UTF-8");
        inStream.setGenerateByteOrderMark(true);

        return inStream.readAll();
    }

    auto LoadAnnotation(const QString& annotationPath) -> QMap<QString, QString> {
        QSettings qs(annotationPath, QSettings::IniFormat);

        QMap<QString, QString> annotationMap;

        QStringList keys = qs.allKeys();
        foreach(QString key, keys) {
            const QString val = qs.value(key).toString();
            annotationMap[key] = val;
        }
        return annotationMap;
    }

    auto LoadTile(const QString& tilePath) -> QMap<QString, QString> {
        QMap<QString, QString> tileMap;

        QFile file(tilePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return tileMap;

        while (!file.atEnd()) {
            QString line = file.readLine();

            QStringList strList = line.split(",");
            if (strList.length() != 2) continue;

            tileMap[strList.at(0)] = strList.at(1);
        }
        return tileMap;
    }

    auto CurrentDateTime() -> QString {
        return QDateTime(QDateTime::currentDateTime()).toString("yyyy-MM-dd HH:mm:ss");
    }

    auto GenUniqueID(QString strDevice, QString strRecodingTime, QString strCurrentDate) -> QString {
        strRecodingTime.replace(QRegExp("[- .]"), ":");
        strCurrentDate.replace(QRegExp("[- ]"), ":");
        QString uuid = QUuid::createUuid().toString();
        uuid.replace(QRegExp("[\\{\\-\\}]"), "");

        QStringList strUUID;

        strUUID << strDevice
            << strRecodingTime
            << strCurrentDate
            << uuid;

        return strUUID.join("-");
    }

    auto GenDataID(QString strDevice, QString strRecodingTime) -> QString {
        strRecodingTime.replace(QRegExp("[- .]"), ":");

        QStringList strDataID;

        strDataID << strDevice
            << strRecodingTime;

        return strDataID.join("-");
    }

    auto FileExists(QString filePath) -> bool {
        QFile file(filePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return false;
        } else {
            file.close();
            return true;
        }
    }

    auto GetHtXWorldLength(const QString& configPath, const QString& jobParameterPath) -> double {
        QMap<QString, QString> configMap;
        if (!TC::LoadFullParameter(configPath, configMap)) {
            return 0;
        }

        JobParameter jobParameter(jobParameterPath);
        if (!jobParameter.isValid()) {
            return 0;
        }

        if (configMap.find("Pixel size") == configMap.end()) {
            return 0;
        }
        if (configMap.find("M") == configMap.end()) {
            return 0;
        }
        if (configMap.find("NA") == configMap.end()) {
            return 0;
        }
        if (configMap.find("Lambda") == configMap.end()) {
            return 0;
        }

        const auto rawSize = static_cast<double>(jobParameter.fieldOfViewHPixels());

        const auto pixelSize = configMap["Pixel size"].toDouble();
        const auto M = configMap["M"].toDouble();
        const auto res = pixelSize / M;

        const auto NA = configMap["NA"].toDouble();
        const auto lambda = configMap["Lambda"].toDouble();

        const auto r = std::round(rawSize * res * NA / lambda);

        const auto fsMargin = 2 * std::round(r / 6);

        const auto nArrSize_no_padding = std::round(2 * r) + fsMargin;

        const auto crop_factor = nArrSize_no_padding / rawSize;

        const auto res2 = res / crop_factor;

        const auto ZP = std::min(512.0, std::pow(2, std::ceil(log(nArrSize_no_padding * 3) / log(2))));
        auto ZP2 = ZP;

        const auto crop_ratio = ZP / nArrSize_no_padding;

        if (crop_ratio > 1) {
            const int offset = (ZP2 - std::round(ZP2 / (2 * crop_ratio)) * 2) / 2;		//always even number
            const int newZP2 = ZP2 - offset;
            const int d0_bound = (newZP2 > 30) ? 10 : 0;

            ZP2 = ZP2 - (2 * (d0_bound + offset));
        }

        const auto htLength = ZP2 * res2;

        return htLength;
    }

    auto GetHtZWorldLength(const QString& configPath, const QString& jobParameterPath) -> double {
        QMap<QString, QString> configMap;
        if (!TC::LoadFullParameter(configPath, configMap)) {
            return 0;
        }

        JobParameter jobParameter(jobParameterPath);
        if (!jobParameter.isValid()) {
            return 0;
        }

        if (configMap.find("Pixel size") == configMap.end()) {
            return 0;
        }
        if (configMap.find("M") == configMap.end()) {
            return 0;
        }
        if (configMap.find("NA") == configMap.end()) {
            return 0;
        }
        if (configMap.find("Lambda") == configMap.end()) {
            return 0;
        }
        if (configMap.find("n_m") == configMap.end()) {
            return 0;
        }
        if (configMap.find("NA_CDS") == configMap.end()) {
            return 0;
        }
        if (configMap.find("Disable ZLimit") == configMap.end()) {
            return 0;
        }



        const auto rawSize = static_cast<double>(jobParameter.fieldOfViewHPixels());

        const auto pixelSize = configMap["Pixel size"].toDouble();
        const auto M = configMap["M"].toDouble();
        const auto res = pixelSize / M;

        const auto NA = configMap["NA"].toDouble();
        const auto lambda = configMap["Lambda"].toDouble();
        const auto n_m = configMap["n_m"].toDouble();
        const auto NA_CDS = configMap["NA_CDS"].toDouble();

        const auto r = std::round(rawSize * res * NA / lambda);

        const auto fsMargin = 2 * std::round(r / 6);

        const auto nArrSize_no_padding = std::round(2 * r) + fsMargin;

        const auto crop_factor = nArrSize_no_padding / rawSize;

        const auto res2 = res / crop_factor;

        const auto ZP = std::min(512.0, std::pow(2, std::ceil(log(nArrSize_no_padding * 3) / log(2))));
        auto ZP2 = ZP;

        const double res3 = res2 * ZP / ZP2;
        const double denom1 = (n_m - std::sqrt(std::pow(n_m, 2) - std::pow(NA, 2)));
        const double denom2 = (n_m - std::sqrt(std::pow(n_m, 2) - std::pow(NA_CDS, 2)));
        double res4 = lambda / (denom1 + denom2);

        auto ZP3 = std::round(res2 * ZP / res4 / 2.0) * 2;

        unsigned int new_ZP3 = 0;

        if (res4 > 0.3) {
            ZP3 = static_cast<uint32_t>(ZP3);

            const double dof = static_cast<double>(static_cast<uint32_t>(ZP3))* res4;
            const double dof_max = 40.0;
            const bool notLimitDOF = (configMap["Disable ZLimit"].toDouble() == 1.0);


            if ((dof > dof_max) && !notLimitDOF) {
                new_ZP3 = static_cast<uint32_t>(ZP3 * dof_max / dof);
                new_ZP3 = new_ZP3 + (new_ZP3 % 2);
                ZP3 = new_ZP3;
            }
        }

        const double new_dRes4 = res3;	//NOTE - ZP2 = ZP 조건이므로 m_dRes3 = m_dRes2 인 상태임...
        unsigned int new_z = ZP3 * res4 / new_dRes4;
        new_z = new_z + (new_z % 2);

        auto interpolated = false;
        if (new_ZP3 != new_z) {
            new_z = (new_z + (new_z % 2));
            res4 = (new_ZP3 * res4) / new_z;
            interpolated = true;
        }

        ZP3 = new_z;

        const auto htZLength = static_cast<double>(ZP3)* res4;

        return htZLength;
    }

    void dbgPrint(const af::array& arrIn, const char* varName, const char* atFile, int atLine) {
        const unsigned int dimSZ = arrIn.numdims();
        unsigned int maxElements = arrIn.dims(0);
        const unsigned int nElements = 3;

        for (unsigned int i = 1; i < dimSZ; i++) {
            if (maxElements < arrIn.dims(i)) maxElements = arrIn.dims(i);
        }


        printf("var:%s at %s:%d\n", (varName) ? varName : "N/A", (atFile) ? atFile : "N/A", atLine);

        dbgPrintType(arrIn.type());

        if ((dimSZ < 3) && maxElements <= nElements) {
            af_print(arrIn);
        } else {
            printf("[");
            for (unsigned int i = 0; i < dimSZ; i++)
                printf("%d ", arrIn.dims(i));
            printf("]\n");

            if (dimSZ == 1) {
                af_print(arrIn(af::seq(0, nElements)));
            } else if (dimSZ == 2) {
                const unsigned int dim0 = arrIn.dims(0);
                const unsigned int dim1 = arrIn.dims(1);

                const unsigned int n0 = std::min<unsigned int>(nElements, dim0) - 1;
                const unsigned int n1 = std::min<unsigned int>(nElements, dim1) - 1;

                af_print(arrIn(af::seq(0, n0), af::seq(0, n1)));
            } else if (dimSZ == 3) {
                const unsigned int dim0 = arrIn.dims(0);
                const unsigned int dim1 = arrIn.dims(1);
                const unsigned int dim2 = arrIn.dims(2);

                const unsigned int n0 = std::min<unsigned int>(nElements, dim0) - 1;
                const unsigned int n1 = std::min<unsigned int>(nElements, dim1) - 1;
                const unsigned int n2 = std::min<unsigned int>(nElements, dim2) - 1;

                af_print(arrIn(af::seq(0, n0), af::seq(0, n1), 0));
                //af_print(arrIn(af::seq(0, n0), af::seq(0, n1), af::seq(0,n2)));
            }
        }
    }

    void dbgPrintType(af::dtype type) {
        static char* typeStr[] = {
         "f32 - 32-bit floating point",
         "c32 - 32-bit complex floating point",
         "f64 - 64-bit floating point",
         "c64 - 64-bit complex floating point",
         "b8 - 8-bit boolean",
         "s32 - 32-bit signed integral",
         "u32 - 32-bit unsigned integral"
         "u8 - 8-bit unsigned integral",
         "s64 - 64-bit signed integral",
         "u64 - 64-bit unsigned integral",
         "s16 - 16-bit signed integral",
         "u16 - 16-bit unsigned integral",
         "error"
        };

        const unsigned int idx = (type > u16) ? (u16 + 1) : type;

        printf("type = %s\n", typeStr[idx]);
    }

    void dbgIsNan(const af::array& arrIn, const char* varName, const char* atFile, int atLine) {
        af::array arrNan = af::isNaN(arrIn);
        bool bNoNans = af::allTrue<char>(arrNan == 0);

        if (!bNoNans) {
            printf("var:%s has NaN at %s:%d\n", (varName) ? varName : "N/A", (atFile) ? atFile : "N/A", atLine);
        }
    }
}

