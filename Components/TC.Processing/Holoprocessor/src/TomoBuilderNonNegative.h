#pragma once

#include "TomoBuilderInterface.h"

namespace TC {
    class TomoBuilderNonNegative : public TomoBuilderInterface {
    public:
        TomoBuilderNonNegative(ProgressReporter* reporter = new NoProgressReporter(), StopChecker* stopchecker = new NoStopChecker());
        ~TomoBuilderNonNegative();

    protected:
        bool Regularization(af::array& ORytov_index, af::array& ORytov, float normFact);
    };
}