#pragma once

#include "cufft.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__global__ void makeMirrorImage(float* dev_input, float* dev_mirrored_output, const int input_Xsize, const int input_Ysize);
__global__ void extract_from_mirror(float* dev_mirrored_output, float* dev_output, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize);
__global__ void extract_from_mirror(cufftComplex* dev_mirrored_output, float* dev_output, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize);

__global__ void subtracted_extraction(cufftReal* dev_term1, cufftReal* dev_term2, cufftReal* dev_extracted_phase, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize);

__global__ void makeExpImagMirrorImage(float* dev_input, cufftComplex* dev_mirrored_output, const int input_Xsize, const int input_Ysize);

__global__ void makeSinCosMirrorImage(float* dev_input, cufftReal* dev_Sin_mirrored_output, cufftReal* dev_Cos_mirrored_output, const int input_Xsize, const int input_Ysize);
