#include "CudaErrorLog.h"
#include <TCLogger.h>

auto LogCudaError(const cudaError_t& cudaError, const std::string& contents) -> void {
    const auto errorName = cudaGetErrorName(cudaError);
    const auto errorString = cudaGetErrorString(cudaError);
    QLOG_ERROR() << contents.c_str() << " : (" << errorName << ")" << errorString;
}

auto LogCufftPlanError(const cufftResult_t& cufftResult, const std::string& functionName, const int32_t& arraySizeX,
    const int32_t& arraySizeY, const cufftType_t& cufftType) -> void {
    const auto cufftResultName = GetCufftResultName(cufftResult);
    const auto cufftTypeName = GetCufftTypeName(cufftType);

    QLOG_ERROR()
        << functionName.c_str() << "failed : cufftResult (" << cufftResultName.c_str() << ")"
        << "\nsizeX : " << arraySizeX
        << "\nsizeY : " << arraySizeY
        << "\nType : " << cufftTypeName.c_str();
}

auto LogCufftExecError(const cufftResult_t& cufftResult, const std::string& functionName,
    const std::string& inputArrayName, const std::string& outputArrayName, const int32_t& direction) -> void {
    const auto cufftResultName = GetCufftResultName(cufftResult);
    const auto directionName = GetCufftDirection(direction);

    QLOG_ERROR()
        << functionName.c_str() << " failed : cufftResult (" << cufftResultName.c_str() << ")"
        << "\ninputArray : " << inputArrayName.c_str()
        << "\noutputArray : " << outputArrayName.c_str()
        << "\nDirection : " << directionName.c_str();
}

auto GetCufftResultName(const cufftResult_t& cufftResult) -> std::string {
    std::string cufftResultName{ "" };

    switch (cufftResult) {
    case CUFFT_SUCCESS:
        cufftResultName = "CUFFT_SUCCESS";
        break;
    case CUFFT_INVALID_PLAN:
        cufftResultName = "CUFFT_INVALID_PLAN";
        break;
    case CUFFT_ALLOC_FAILED:
        cufftResultName = "CUFFT_ALLOC_FAILED";
        break;
    case CUFFT_INVALID_TYPE:
        cufftResultName = "CUFFT_INVALID_TYPE";
        break;
    case CUFFT_INVALID_VALUE:
        cufftResultName = "CUFFT_INVALID_VALUE";
        break;
    case CUFFT_INTERNAL_ERROR:
        cufftResultName = "CUFFT_INTERNAL_ERROR";
        break;
    case CUFFT_EXEC_FAILED:
        cufftResultName = "CUFFT_EXEC_FAILED";
        break;
    case CUFFT_SETUP_FAILED:
        cufftResultName = "CUFFT_SETUP_FAILED";
        break;
    case CUFFT_INVALID_SIZE:
        cufftResultName = "CUFFT_INVALID_SIZE";
        break;
    case CUFFT_UNALIGNED_DATA:
        cufftResultName = "CUFFT_UNALIGNED_DATA";
        break;
    case CUFFT_INCOMPLETE_PARAMETER_LIST:
        cufftResultName = "CUFFT_INCOMPLETE_PARAMETER_LIST";
        break;
    case CUFFT_INVALID_DEVICE:
        cufftResultName = "CUFFT_INVALID_DEVICE";
        break;
    case CUFFT_PARSE_ERROR:
        cufftResultName = "CUFFT_PARSE_ERROR";
        break;
    case CUFFT_NO_WORKSPACE:
        cufftResultName = "CUFFT_NO_WORKSPACE";
        break;
    case CUFFT_NOT_IMPLEMENTED:
        cufftResultName = "CUFFT_NOT_IMPLEMENTED";
        break;
    case CUFFT_LICENSE_ERROR:
        cufftResultName = "CUFFT_LICENSE_ERROR";
        break;
    case CUFFT_NOT_SUPPORTED:
        cufftResultName = "CUFFT_NOT_SUPPORTED";
        break;
    default:
        cufftResultName = "Unknown";
        break;
    }
    return cufftResultName;
}

auto GetCufftTypeName(const cufftType_t& cufftType) -> std::string {
    std::string cufftTypeName{ "" };
    switch (cufftType) {
    case CUFFT_R2C:
        cufftTypeName = "CUFFT_R2C";
        break;
    case CUFFT_C2R:
        cufftTypeName = "CUFFT_C2R";
        break;
    case CUFFT_C2C:
        cufftTypeName = "CUFFT_C2C";
        break;
    case CUFFT_D2Z:
        cufftTypeName = "CUFFT_D2Z";
        break;
    case CUFFT_Z2D:
        cufftTypeName = "CUFFT_Z2D";
        break;
    case CUFFT_Z2Z:
        cufftTypeName = "CUFFT_Z2Z";
        break;
    default:
        cufftTypeName = "Unknown";
        break;
    }
    return cufftTypeName;
}

auto GetCufftDirection(const int32_t& direction) -> std::string {
    std::string directionName{ "" };
    if (direction == CUFFT_FORWARD) {
        directionName = "CUFFT_FORWORD";
    } else if (direction == CUFFT_INVERSE) {
        directionName = "CUFFT_INVERSE";
    } else {
        directionName = "Unknown";
    }
    return directionName;
}
