#include "mycuPQarray.h"


__global__ void
make_Parray(int* dev_Parray, const int input_Xsize, const int input_Ysize) {
    //padding version 
    //at R2C Fourier CUDA transform output size is xsize*[ysize/2 +1]
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inParray = (x < input_Xsize) && (y < input_Ysize);

    while (inParray) {


        dev_Parray[index] = x;

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inParray = (x < input_Xsize) && (y < input_Ysize);
    }
}


__global__ void
make_Qarray(int* dev_Qarray, const int input_Xsize, const int input_Ysize) {
    //padding version 
    //at R2C Fourier CUDA transform output size is xsize*[ysize/2 +1]
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inQarray = (x < input_Xsize) && (y < input_Ysize);

    while (inQarray) {
        if (y > ((input_Ysize / 2) - 1)) { // after center
            dev_Qarray[index] = y - input_Ysize;
        } else { // before center
            dev_Qarray[index] = y;
        }

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inQarray = (x < input_Xsize) && (y < input_Ysize);
    }
}

__global__ void
make_Parray2(int* dev_Parray, const int input_Xsize, const int input_Ysize) {
    //no padding version 
    //at C2C Fourier CUDA transform output size is same with input size
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inParray = (x < input_Xsize) && (y < input_Ysize);

    while (inParray) {
        if (x > ((input_Xsize / 2) - 1)) { // after center
            dev_Parray[index] = x - input_Xsize;
        } else { // before center
            dev_Parray[index] = x;
        }

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inParray = (x < input_Xsize) && (y < input_Ysize);
    }
}

__global__ void
make_Qarray2(int* dev_Qarray, const int input_Xsize, const int input_Ysize) {
    //no padding version 
    //at C2C Fourier CUDA transform output size is same with input size
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inQarray = (x < input_Xsize) && (y < input_Ysize);

    while (inQarray) {
        if (y > ((input_Ysize / 2) - 1)) { // after center
            dev_Qarray[index] = y - input_Ysize;
        } else { // before center
            dev_Qarray[index] = y;
        }

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inQarray = (x < input_Xsize) && (y < input_Ysize);
    }
}

__global__ void
make_P2Q2array(int* dev_Parray, int* dev_Qarray, int* dev_P2Q2array, const int input_Xsize, const int input_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inPQarray = (x < input_Xsize) && (y < input_Ysize);

    while (inPQarray) {

        dev_P2Q2array[index] = ((dev_Parray[index]) * (dev_Parray[index])) + ((dev_Qarray[index]) * (dev_Qarray[index]));

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inPQarray = (x < input_Xsize) && (y < input_Ysize);
    }
}


__global__ void
make_P2Q2array(int* dev_P2Q2array, const int input_Xsize, const int input_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int index = i + j * (blockDim.x * gridDim.x);

    int x = index % input_Xsize;
    int y = index / input_Xsize;

    bool inBoundary = (x < input_Xsize) && (y < input_Ysize);

    while (inBoundary) {
        int m = 0; //x coordinate of P2Q2array
        int n = 0; //y coordinate of P2Q2array
        if (x > ((input_Xsize / 2) - 1)) { // after center
            m = x - input_Xsize;
        } else { // before center
            m = x;
        }

        if (y > ((input_Ysize / 2) - 1)) { // after center
            n = y - input_Ysize;
        } else { // before center
            n = y;
        }

        dev_P2Q2array[index] = (m * m) + (n * n);

        index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        x = index % input_Xsize;
        y = index / input_Xsize;
        inBoundary = (x < input_Xsize) && (y < input_Ysize);
    }
}