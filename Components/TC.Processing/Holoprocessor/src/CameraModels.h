#pragma once

#include <tuple>
#include <memory>
#include <QString>
#include <QStringList>

#include "GlobalDefines.h"

class CameraModels {
public:
    static std::shared_ptr<CameraModels> GetInstance(const QString& name);
    static QStringList getList(void);

    virtual bool isSameCamera(const QString& name) const = 0;
    virtual double pixelSizeH(void) const = 0;
    virtual double pixelSizeV(void) const = 0;
    auto pixelSize(void) const { return std::tuple<double, double>(pixelSizeH(), pixelSizeV()); }

    virtual int maxPixelsH(void) const = 0;
    virtual int maxPixelsV(void) const = 0;
    auto maxPixels(void) const { return std::tuple<int, int>(maxPixelsH(), maxPixelsV()); }

    virtual double acqusitionTime(int fovH, int fovV) const = 0;
    virtual int fovStep(void) const = 0;
    virtual bool supportHDR(void) const { return false; }
    virtual int outputBitsInHDR(void) const { return 8; }
    virtual int imageBitsInHDR(void) const { return 8; }
    virtual double maxGain(void) const = 0;

    virtual double minExposure(void) const { return 1.0; }      //It depends on DMD trigger control specification
    virtual double maxExposure(void) const { return 990; }
    auto exposure(void) const { return std::tuple<double, double>(minExposure(), maxExposure()); }

    virtual TC::CameraType type(void) const = 0;
};

//Virtual camera
class CameraVirtual : public CameraModels {
public:
    static QString modelName(void) { return "Virtual"; }

    bool isSameCamera(const QString& name) const override { return modelName() == name; }
    double pixelSizeH(void) const override { return 4.80; }
    double pixelSizeV(void) const override { return 4.80; }
    int maxPixelsH(void) const override { return 1000; }
    int maxPixelsV(void) const override { return 1000; }
    double acqusitionTime(int fovH, int fovV) const override;
    int fovStep(void) const override;
    double maxGain(void) const override { return 100; }

    TC::CameraType type(void) const override { return TC::CAMERA_SIMULATOR; }
};


//Pointgrey FL3-U3-13Y3M-C
class CameraPGFL3U3 : public CameraModels {
public:
    static QString modelName(void) { return "Pointgrey FL3-U3-13Y3M-C"; }

    bool isSameCamera(const QString& name) const override { return modelName() == name; }
    double pixelSizeH(void) const override { return 4.80; }
    double pixelSizeV(void) const override { return 4.80; }
    int maxPixelsH(void) const override { return 1280; }
    int maxPixelsV(void) const override { return 1024; }
    double acqusitionTime(int fovH, int fovV) const override;
    int fovStep(void) const override;
    double maxGain(void) const override { return 18; }

    TC::CameraType type(void) const override { return TC::CAMERA_POINTGREY; }
};

//Basler acA2440-75uc
class CameraBasleracA244075uc : public CameraModels {
public:
    static QString modelName(void) { return "Basler acA2440-75uc"; }

    bool isSameCamera(const QString& name) const override { return modelName() == name; }
    double pixelSizeH(void) const override { return 3.45; }
    double pixelSizeV(void) const override { return 3.45; }
    int maxPixelsH(void) const override { return 2448; }
    int maxPixelsV(void) const override { return 2048; }
    double acqusitionTime(int fovH, int fovV) const override;
    int fovStep(void) const override;
    double maxGain(void) const override { return 18; }

    TC::CameraType type(void) const override { return TC::CAMERA_BASLER; }
};

//FLIR BFS-U3-28S5M
class CameraBFSU328S5M : public CameraModels {
public:
    static QString modelName(void) { return "FLIR BFS-U3-28S5M"; }

    bool isSameCamera(const QString& name) const override { return modelName() == name; }
    double pixelSizeH(void) const override { return 4.50; }
    double pixelSizeV(void) const override { return 4.50; }
    int maxPixelsH(void) const override { return 1080; }
    int maxPixelsV(void) const override { return 1080; }
    double acqusitionTime(int fovH, int fovV) const override;
    int fovStep(void) const override;
    bool supportHDR(void) const override { return false; }      //temporarily disabled (TOMV-1559)
    int outputBitsInHDR(void) const override { return (supportHDR()) ? 12 : 8; }
    int imageBitsInHDR(void) const override { return (supportHDR()) ? 16 : 8; }
    double maxGain(void) const override { return 47.0; }
    double minExposure(void) const { return 1.0; }              //It depends on DMD trigger control specification
    double maxExposure(void) const { return 990; }

    TC::CameraType type(void) const override { return TC::CAMERA_POINTGREY; }
};

typedef std::shared_ptr<CameraModels> CameraModelsPtr;