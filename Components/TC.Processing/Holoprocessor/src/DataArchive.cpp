#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <Windows.h>

#include <QStringList>
#include <QFileInfo>

#include <TCLogger.h>
#include "HDF5Mutex.h"
#include "DataArchive.h"


DataArchive::DataArchive() {
}

void DataArchive::setFilename(const QString& strName) {
    m_strFilePath = strName;

    QFileInfo file(strName);
    if (!file.exists()) {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_TRUNC);
    }
}

unsigned int DataArchive::count(const QString& strGroup) const {
    unsigned int items = 0;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
        H5::Group group = file.openGroup(strGroup.toStdString());
        items = group.getNumObjs();
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(co) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(co) - " << strGroup;
    }

    return items;
}

bool DataArchive::store(const QString& strGroup, const QString& strName, const QString& strPath, const int retry) {
    bool bSuccess = true;
    int retryCnt = 0;

    do {
        if (retryCnt > 0)
            QLOG_ERROR() << "DataArchive(s) - retry: " << retryCnt;

        bSuccess = store_internal(strGroup, strName, strPath);
        if (bSuccess) break;

        Sleep(200);
    } while ((retryCnt++) < retry);

    if (retryCnt > 0) {
        if (bSuccess)
            QLOG_INFO() << "DataArchive(s) - it is saved after " << retryCnt << " times retry(ies)";
        else
            QLOG_ERROR() << "DataArchives(s) - it fails to store data(group=" << strGroup << ",name=" << strName << ",path=" << strPath << ")";
    }

    return bSuccess;
}

bool DataArchive::store_internal(const QString& strGroup, const QString& strName, const QString& strPath) {
    bool bSuccess = true;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDWR);

        if (!prepareGroups(file, strGroup)) return false;

        std::ifstream raw_file(strPath.toLocal8Bit().constData(), std::ios::in | std::ios::binary | std::ios::ate);
        if (raw_file.is_open()) {
            std::streampos size = raw_file.tellg();
            std::unique_ptr<char> memblock(new char[size]);
            raw_file.seekg(0, std::ios::beg);
            raw_file.read(memblock.get(), size);
            raw_file.close();

            H5::Group group = file.openGroup(strGroup.toStdString());

            hsize_t  dims[1];
            dims[0] = size;

            H5::DataSpace space(1, dims);
            H5::DataSet dataSet = group.createDataSet(strName.toStdString(), H5::PredType::NATIVE_UCHAR, space);
            dataSet.write(memblock.get(), H5::PredType::NATIVE_UCHAR, space);
        } else {
            bSuccess = false;
            QLOG_ERROR() << "DataArchive(s) - file is not opened : " << strPath.toLocal8Bit().constData();
        }
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(s) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(s) - " << strGroup << "," << strName;
        bSuccess = false;
    }

    return bSuccess;
}

bool DataArchive::restore(const QString& strGroup, const QString& strName, const QString& strPath, const int retry) {
    bool bSuccess = true;
    int retryCnt = 0;

    do {
        if (retryCnt > 0)
            QLOG_ERROR() << "DataArchive(r) - retry: " << retryCnt;
        bSuccess = restore_internal(strGroup, strName, strPath);
        if (bSuccess) break;

        Sleep(200);
    } while ((retryCnt++) < retry);

    if (retryCnt > 0) {
        if (bSuccess)
            QLOG_INFO() << "DataArchive(r) - it is restored after " << retryCnt << " times retry(ies)";
        else
            QLOG_ERROR() << "DataArchive(r) - it fails to resotre data";
    }

    return bSuccess;
}

bool DataArchive::restore_internal(const QString& strGroup, const QString& strName, const QString& strPath) {
    bool bSuccess = true;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

        H5::Group group = file.openGroup(strGroup.toStdString());

        if (!group.exists(strName.toStdString())) return false;
        H5::DataSet dataSet = group.openDataSet(strName.toStdString());

        H5::DataSpace dataSpace = dataSet.getSpace();
        std::unique_ptr<hsize_t> dims_out(new hsize_t[1]);
        int ndims = dataSpace.getSimpleExtentDims(dims_out.get(), NULL);

        std::unique_ptr<char> pData(new char[dims_out.get()[0]]);
        dataSet.read(pData.get(), H5::PredType::NATIVE_UCHAR);

        std::ofstream raw_file(strPath.toStdString(), std::ios::out | std::ios::binary);
        if (raw_file.is_open()) {
            raw_file.write(pData.get(), dims_out.get()[0]);
            raw_file.close();
        } else {
            bSuccess = false;
        }
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(r) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(r) - " << strGroup << "," << strName;
        bSuccess = false;
    }

    return bSuccess;
}

bool DataArchive::restore(const QString& strGroup, const int nIndex, const QString& strPath, const int retry) {
    bool bSuccess = true;
    int retryCnt = 0;

    do {
        if (retryCnt > 0)
            QLOG_ERROR() << "DataArchive(r) - retry: " << retryCnt;

        bSuccess = restore_internal(strGroup, nIndex, strPath);
        if (bSuccess) break;

        Sleep(200);
    } while ((retryCnt++) < retry);

    if (retryCnt > 0) {
        if (bSuccess)
            QLOG_INFO() << "DataArchive(r) - it is restored after " << retryCnt << " times retry(ies)";
        else
            QLOG_ERROR() << "DataArchive(r) - it fails to resotre data";
    }

    return bSuccess;
}

bool DataArchive::restore_internal(const QString& strGroup, const int nIndex, const QString& strPath) {
    bool bSuccess = true;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

        H5::Group group = file.openGroup(strGroup.toStdString());

        H5::DataSet dataSet = group.openDataSet(group.getObjnameByIdx(nIndex));

        H5::DataSpace dataSpace = dataSet.getSpace();
        std::unique_ptr<hsize_t> dims_out(new hsize_t[1]);
        int ndims = dataSpace.getSimpleExtentDims(dims_out.get(), NULL);

        std::unique_ptr<char> pData(new char[dims_out.get()[0]]);
        dataSet.read(pData.get(), H5::PredType::NATIVE_UCHAR);

        std::ofstream raw_file(strPath.toStdString(), std::ios::out | std::ios::binary);
        if (raw_file.is_open()) {
            raw_file.write(pData.get(), dims_out.get()[0]);
            raw_file.close();
        } else {
            bSuccess = false;
        }
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(r) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(r) - " << strGroup << "," << nIndex;
        bSuccess = false;
    }

    return bSuccess;
}

bool DataArchive::restoreAll(const QString& strGroup, const QString& strPath, int nWidth, int nStart, const int retry) {
    bool bSuccess = true;
    int retryCnt = 0;

    do {
        if (retryCnt > 0)
            QLOG_ERROR() << "DataArchive(ra) - retry: " << retryCnt;

        bSuccess = restoreAll_internal(strGroup, strPath, nWidth, nStart);
        if (bSuccess) break;

        Sleep(200);
    } while ((retryCnt++) < retry);

    if (retryCnt > 0) {
        if (bSuccess)
            QLOG_INFO() << "DataArchive(ra) - it is restored after " << retryCnt << " times retry(ies)";
        else
            QLOG_ERROR() << "DataArchive(ra) - it fails to restore data";
    }

    return bSuccess;
}

bool DataArchive::restoreAll_internal(const QString& strGroup, const QString& strPath, int nWidth, int nStart) {
    bool bSuccess = true;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

        H5::Group group = file.openGroup(strGroup.toStdString());

        int index = 0;

        hsize_t objs = group.getNumObjs();
        for (int ii = 0; ii < objs; ii++) {
            H5G_obj_t type = group.getObjTypeByIdx(ii);
            if (type != H5G_DATASET) continue;

            std::string objName = group.getObjnameByIdx(ii);

            H5::DataSet dataSet = group.openDataSet(objName);

            H5::DataSpace dataSpace = dataSet.getSpace();
            std::unique_ptr<hsize_t> dims_out(new hsize_t[1]);
            int ndims = dataSpace.getSimpleExtentDims(dims_out.get(), NULL);

            std::unique_ptr<char> pData(new char[dims_out.get()[0]]);
            dataSet.read(pData.get(), H5::PredType::NATIVE_UCHAR);

            QString strFile = QString("%1/%2.png").arg(strPath).arg(index + nStart, nWidth, 10, QLatin1Char('0'));
            std::ofstream raw_file(strFile.toStdString(), std::ios::out | std::ios::binary);
            if (raw_file.is_open()) {
                raw_file.write(pData.get(), dims_out.get()[0]);
                raw_file.close();
            } else {
                bSuccess = false;
            }

            index++;
        }
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(ra) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(ra) - " << strGroup;
        bSuccess = false;
    }

    return bSuccess;
}

bool DataArchive::prepareGroups(H5::H5File& file, const QString& strGroup) {
    bool bExists = true;

    try {
        //H5::Exception::dontPrint();
        H5::Group group = file.openGroup(strGroup.toStdString());
    } catch (H5::Exception& /*ex*/) {
        bExists = false;
    }

    if (!bExists) {
        try {
            file.createGroup(strGroup.toStdString());
            bExists = true;
        } catch (H5::Exception& /*ex*/) {
            bExists = false;
        }
    }

    return true;
}

bool DataArchive::setValue(const QString& strGroup, const QString& strName, const QVariant& value) {
    bool bSuccess = true;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDWR);

        if (!prepareGroups(file, strGroup)) return false;

        H5::Group group = file.openGroup(strGroup.toStdString());

        H5::DataType h5Type;
        const auto dataType = value.type();

        switch (dataType) {
        case QVariant::Double:
            h5Type = H5::DataType(H5::PredType::NATIVE_DOUBLE);
            break;
        case QVariant::Int:
            h5Type = H5::DataType(H5::PredType::NATIVE_INT32);
            break;
        case QVariant::UInt:
            h5Type = H5::DataType(H5::PredType::NATIVE_UINT32);
            break;
        case QVariant::LongLong:
            h5Type = H5::DataType(H5::PredType::NATIVE_INT64);
            break;
        case QVariant::ULongLong:
            h5Type = H5::DataType(H5::PredType::NATIVE_UINT64);
            break;
        default:
            QLOG_WARN() << "DataArchive(s) - Not supported data type (" << strGroup << "," << strName << ")";
            return false;
        }

        hsize_t dims[1]{ 1 };
        const H5::DataSpace space(1, dims);
        auto attr = group.createAttribute(strName.toStdString().c_str(), h5Type, space);
        attr.write(h5Type, value.constData());
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(s) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(s) - " << strGroup << "," << strName;
        bSuccess = false;
    }

    return bSuccess;
}

QVariant DataArchive::getValue(const QString& strGroup, const QString& strName) {
    bool bSuccess = true;

    QVariant data;

    try {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        H5::H5File file(m_strFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
        H5::Group group = file.openGroup(strGroup.toStdString());

        auto attr = group.openAttribute(strName.toStdString().c_str());
        auto h5_type = attr.getDataType();
        QVariant::Type type;

        if (h5_type == H5::DataType(H5::PredType::NATIVE_DOUBLE)) type = QVariant::Double;
        else if (h5_type == H5::DataType(H5::PredType::NATIVE_INT32)) type = QVariant::Int;
        else if (h5_type == H5::DataType(H5::PredType::NATIVE_UINT32)) type = QVariant::UInt;
        else if (h5_type == H5::DataType(H5::PredType::NATIVE_INT64)) type = QVariant::LongLong;
        else if (h5_type == H5::DataType(H5::PredType::NATIVE_UINT64)) type = QVariant::ULongLong;
        else {
            QLOG_WARN() << "DataArchive(r) - Not supported data type (" << strGroup << "," << strName << ")";
            return false;
        }

        char temp[16]{ 0 };
        attr.read(h5_type, temp);
        data = QVariant(type, temp);
    } catch (H5::Exception & ex) {
        QLOG_ERROR() << "DataArchive(s) - " << ex.getDetailMsg().c_str();
        QLOG_ERROR() << "DataArchive(s) - " << strGroup << "," << strName;
        bSuccess = false;
    }

    return data;
}