#pragma once
#include <iostream>

#include "cufft.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

void HerveUnwrap_optimized_revised(float* dev_wrapped_phase, float* devicePointerUnwrappedPhase, const int arraySizeX, const int arraySizeY);