#pragma once

#include <memory>

#include <QString>



#include "SystemModel.h"
#include "SystemModelHT1H_Gen2.h"


class SystemModelHT2H_Gen2 : public SystemModelHT1H_Gen2 {
public:
    typedef SystemModelHT2H_Gen2 Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    static SystemModelInterface::Pointer New() {
        SystemModelInterface::Pointer self(new Self());
        return self;
    }

    virtual QString Model() const override { return "HT-2H Gen2"; }
    virtual int Generation() const override { return 2; }

    //Fluorscence
    virtual double FluorescenceScanDepth() override { return 20; }
    virtual double FluorescenceScanStep() override { return 0.2; }
    virtual double FluorescenceScanStepUnit() override { return 0.15625; }
    virtual double DarkpixelCalibrationGain() override { return 40; }
    virtual double DarkpixelCalibrationExposure() override { return 20; }

    //Motion
    virtual double MotionMovableLowerX() override { return -4.0; }
    virtual double MotionMovableUpperX() override { return  4.0; }
    virtual double MotionMovableLowerY() override { return -4.0; }
    virtual double MotionMovableUpperY() override { return  4.0; }

    virtual bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) override {
        if (tarCPos <= curCPos) return true;
        return (tarCPos <= (24.5 + curZPos));
    }

    virtual bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) override {
        if (tarZPos >= curZPos) return true;
        return (tarZPos >= (curCPos - 24.5));
    }

    //=== Model-specific functionality ===
    virtual bool doesSupportFluorescence() override { return true; }
    virtual bool doesSupportBrightfield() override { return true; }
    virtual uint32_t supportedViewModes() const override {
        return static_cast<uint32_t>(Modes::Hologram | Modes::Phase | Modes::BrightFieldMono | Modes::Fluorescence);
    }
};

