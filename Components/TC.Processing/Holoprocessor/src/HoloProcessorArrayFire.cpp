#define LOGGER_TAG "TCHoloprocessor::HoloProcessorArrayFire"
#include <TCLogger.h>

#include <cmath>
#include <iostream>
#include <string>
#include <windows.h>

#include <QHostAddress>
#include <QElapsedTimer>
#include <QDebug>
#include <QTimer>
#include <QFileInfo>
#include <QFile>

#include "SystemParameter.h"
#include "JobParameter.h"
#include "HoloscopeUtility.h"
#include "HoloProcessorArrayFire.h"
#include "DataArchive.h"

#include "ProgressReporter.h"
#include "StopChecker.h"
#include "TomoBuilderUtility.h"
#include "TomoBuilderNonNegative.h"
#include "FluorescenceBuilderInterface.h"

#define QSTR2CHARPTR(qstr) ((qstr).toLatin1().data())

class BuilderProgressReporter : public ProgressReporter {
public:
    BuilderProgressReporter(HoloProcessorArrayFire* processor)
        : ProgressReporter()
        , m_pProcessor(processor) {
    }

    void notify(int value) override {
        m_pProcessor->reportBuilderProgress(value);
    }

private:
    HoloProcessorArrayFire* m_pProcessor;
};

class BuilderStopChecker : public StopChecker {
public:
    BuilderStopChecker(HoloProcessorArrayFire* processor)
        : m_pProcessor(processor) {
    }

    bool isStopRequested(void) override {
        return false;
    }

private:
    HoloProcessorArrayFire* m_pProcessor;
};

HoloProcessorArrayFire::HoloProcessorArrayFire(ProgressReporter* reporter, QCoreApplication* pApplication, int nDebugLevel)
    : HoloProcessor(reporter, pApplication, nDebugLevel)
    , m_pFlInterface(new TC::FluorescenceBuilderInterface()) {
}

HoloProcessorArrayFire::~HoloProcessorArrayFire() {
    TC::release();
    delete m_pFlInterface;
}

bool HoloProcessorArrayFire::initialize() {
    return false;
}

bool HoloProcessorArrayFire::isInitialized() {
    return false;
}

bool HoloProcessorArrayFire::isInitTimeout() {
    return false;
}

bool HoloProcessorArrayFire::clear() {
    return false;
}

bool HoloProcessorArrayFire::procHologram2DSingle(const QString& srcImgPath, const QString& bgImgPath,
    const QString& bgImgPath2, const QString& configPath, const QString& outputPath, TC::ImageType type, double dRange,
    bool bCompensateAnalysis) {
    return false;
}

bool HoloProcessorArrayFire::procHologram2D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, const QString& jobParameterPath, const bool& htConsider, bool isReprocess, const int32_t& timeFrameIndex) {
    std::shared_ptr<BuilderProgressReporter> reporter(new BuilderProgressReporter(this));
    std::shared_ptr<BuilderStopChecker> stopchecker(new BuilderStopChecker(this));

    TC::TomoBuilderNonNegative instance(reporter.get(), stopchecker.get());
    instance.release();

    reportProgress(0);

    if (!instance.SetParameter(configPath)) {
        QLOG_ERROR() << "Failed to set parameters";
        return false;
    }

    reportProgress(5);

    const bool bReferenceExist = (isReprocess) ? false : loadReference(&instance, referencePath);

    if (!bReferenceExist) {
        if (!instance.LoadBackgroundImages(referencePath)) {
            QLOG_ERROR() << "Failed to load reference";
            return false;
        }
    }

    reportProgress(10);

    if (!instance.LoadRawImages(workingPath)) return false;

    reportProgress(15);

    if (bReferenceExist) {
        if (!instance.FieldRetrievalFgOnly(true, true, 0, false, false)) return false;
    } else {
        if (!instance.FieldRetrieval(true, true, 0, false, false)) return false;
    }

    instance.release();
    reportProgress(95);

    double htLength = 0;
    if (htConsider) {
        htLength = TC::GetHtXWorldLength(configPath, jobParameterPath);
    }


    instance.TransferDataPhase(rawData, minMax, resolution, sizes, htLength);

    JobParameter jobParameter(jobParameterPath);
    if (!jobParameter.isValid()) {
        return false;
    }

    resolution.resolutionZ = jobParameter.acquisitionInterval2D();

    instance.release();

    return true;
}

//if newset is on, it means it is the first tomogram for the set such as 4d
bool HoloProcessorArrayFire::procHologram3D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, bool isReprocess) {
    std::shared_ptr<BuilderProgressReporter> reporter(new BuilderProgressReporter(this));
    std::shared_ptr<BuilderStopChecker> stopchecker(new BuilderStopChecker(this));

    TC::TomoBuilderNonNegative instance(reporter.get(), stopchecker.get());
    instance.SetDeviceIndex(getDeviceIndex());

    instance.release();

    reportProgress(0);

    if (!instance.SetParameter(configPath)) {
        QLOG_ERROR() << "Failed to set parameters";
        return false;
    }

    reportProgress(5);

    const bool bReferenceExist = (isReprocess) ? false : loadReference(&instance, referencePath);

    if (!bReferenceExist) {
        if (!instance.LoadBackgroundImages(referencePath)) {
            QLOG_ERROR() << "Failed to load reference";
            return false;
        }
    }

    reportProgress(10);

    if (!instance.LoadRawImages(workingPath)) {
        QLOG_ERROR() << "Failed to load images";
        return false;
    }

    reportProgress(15);

    if (bReferenceExist) {
        if (!instance.FieldRetrievalFgOnly()) {
            QLOG_ERROR() << "Failed to proc sample images";
            return false;
        }
    } else {
        if (!instance.FieldRetrieval()) {
            QLOG_ERROR() << "Failed to proc background and sample images";
            return false;
        }
    }

    //TOMV-1498 save bad fields information into file only when it ignores more than one field...
    instance.SaveBadFieldsLog(QString("%1_%2.log")
        .arg(QString(configPath).replace("config.dat", "log/bad_fields"))
        .arg(QString(outputPath).split("/").back()));

    if (!isValidData(&instance)) {
        QLOG_ERROR() << "Data set is invalid";
        return false;
    }

    instance.release();
    reportProgress(50);

    if (!instance.BuildTomogram()) {
        QLOG_ERROR() << "Failed to build RI tomogram";
        return false;
    }

    reportProgress(95);

    instance.TransferDataHT(rawData, rawDataMip, minMax, minMaxMip, resolution, sizes);

    instance.release();

    return true;
}

bool HoloProcessorArrayFire::procHologram3DFL(const QString& configPath, const QString& darkpixelPath, const QString& workingPath, const QString& jobParameterPath, bool inclDeconv, bool firstSetPerChannel, const int32_t& channelIndex, const bool& htConsider) {
    std::shared_ptr<BuilderProgressReporter> reporter(new BuilderProgressReporter(this));
    std::shared_ptr<BuilderStopChecker> stopchecker(new BuilderStopChecker(this));

    TC::FluorescenceBuilderInterface instance(reporter.get(), stopchecker.get(), af::getDevice());

    instance.release();

    reportProgress(0);

    if (!instance.SetParameter(configPath)) {
        QLOG_ERROR() << "Failed to set parameters from " << configPath;
        return false;
    }

    if (!instance.LoadDarkpixelCal(darkpixelPath)) return false;

    reportProgress(10);

    if (!instance.LoadRawImages(workingPath, channelIndex)) return false;

    reportProgress(30);

    instance.release();

    if (!instance.BuildTomogram(inclDeconv, firstSetPerChannel, channelIndex)) {
        QLOG_ERROR() << "Failed to build FL tomogram";
        return false;
    }

    reportProgress(95);

    double htLength = 0;
    if (htConsider) {
        htLength = TC::GetHtXWorldLength(configPath, jobParameterPath);
    }
    instance.TransferData(rawData, rawDataMip, minMax, minMaxMip, resolution, sizes, htLength);

    instance.release();

    return true;
}

bool HoloProcessorArrayFire::procBrightfield(const QString& configPath, const QString& workingPath, const QString& outputPath, bool isReprocessing) {
    std::shared_ptr<BuilderProgressReporter> reporter(new BuilderProgressReporter(this));
    std::shared_ptr<BuilderStopChecker> stopchecker(new BuilderStopChecker(this));

    TC::TomoBuilderNonNegative instance(reporter.get(), stopchecker.get());

    instance.release();

    reportProgress(0);

    if (!instance.SetParameter(configPath)) {
        QLOG_ERROR() << "Failed to set parameters";
        return false;
    }

    reportProgress(10);

    if (!instance.LoadBFImages(workingPath)) return false;

    reportProgress(50);

    instance.release();
    reportProgress(95);

    instance.TransferDataBrightField(rawData, resolution, sizes);

    instance.release();

    return true;
}

bool HoloProcessorArrayFire::saveTCF(const QString& configPath, const QString& annotationPath, const QString& tilePath,
    const QString& commentPath, const QString& timestampPath, const QString& outputPath) {
    return false;
}

bool HoloProcessorArrayFire::procReference(const QString& strCalPath, bool bSave) {
    return false;
}

bool HoloProcessorArrayFire::procDarkpixel(const QString& strImgPath, const QString& strOutPath) {
    return false;
}

bool HoloProcessorArrayFire::loadDarkPixel(const QString& strDarkPixel) {
    return false;
}

bool HoloProcessorArrayFire::procRemoveDarkPixel(const QString& strImgPath, const QString& strOutPath) {
    return false;
}

bool HoloProcessorArrayFire::loadReference(TC::TomoBuilderInterface* pInterface, const QString& referencePath) {
    if (!pInterface) return false;

    const QString strPath = QString("%1/reference.dat").arg(referencePath);

    if (!QFile(strPath).exists()) return false;

    return pInterface->LoadFieldRetrievalBg(strPath);
}

bool HoloProcessorArrayFire::isValidData(TC::TomoBuilderInterface* pInterface) {
    return pInterface->IsValidData();
}

void HoloProcessorArrayFire::reportBuilderProgress(int value) {
    reportProgress(50 + (0.45 * value));
}

auto HoloProcessorArrayFire::GetRawData() const -> std::any {
    return rawData;
}

auto HoloProcessorArrayFire::GetRawDataMip() const -> std::any {
    return rawDataMip;
}

auto HoloProcessorArrayFire::GetMinMax() const -> TC::MinMax {
    return minMax;
}

auto HoloProcessorArrayFire::GetMinMaxMip() const -> TC::MinMax {
    return minMaxMip;
}

auto HoloProcessorArrayFire::GetResolution() const -> TC::Resolution {
    return resolution;
}

auto HoloProcessorArrayFire::GetSizes() const -> TC::Sizes {
    return sizes;
}
