#pragma once

#include "TomoBuilderFieldRetrievalInterface.h"

namespace TC {
    class TomoBuilderFieldRetrievalSingle : public TomoBuilderFieldRetrievalInterface {
    public:
        TomoBuilderFieldRetrievalSingle(QMap<QString, double>& param, ImageSet* pBgImageSet, ImageSet* pRawImageSet, FieldRetrievalParam* pFieldRetrievalParam);

    public:
        //ignorePhaseSign - if ture, use phase sign value as -1 instead of parameter
        bool FieldRetrievalBgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool miscCallibrationOnly = false, bool ignorePhaseSign = false) override;
        bool FieldRetrievalFgOnly(bool hasNormal = true, bool onlyNormalOnBg = false, int zeroPadding = 0, bool ignorePhaseSign = false, bool chkQuality = true) override;

    protected:
        af::array GenerateBandPassMask(af::array& Fimg, unsigned int radius);
    };
};
