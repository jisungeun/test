#pragma once

#include <memory>

#include <QString>
#include <QSettings>

#include "GlobalDefines.h"


class SystemModelInterface {
public:
    typedef SystemModelInterface Self;
    typedef std::shared_ptr<SystemModelInterface> Pointer;

    enum Modes {
        Hologram = 0x00000001,
        Phase = 0x00000002,
        BrightField = 0x00000100,
        BrightFieldMono = 0x00000110,
        BrightFieldColor = 0x00000120,
        DIC = 0x00001000,
        PhaseContrast = 0x00002000,
        Fluorescence = 0x00004000
    };

public:
    virtual QString Model() const = 0;

    virtual int Generation() const = 0;

    //Laser
    virtual unsigned int Wavelength() = 0;

    //Camera
    virtual QString Camera() = 0;
    virtual int MaxFramerate(unsigned int pixels) = 0;
    virtual bool CenterROIMode() const { return true; }

    //Objective Lens
    virtual double ObjectiveLensMagnification() = 0;
    virtual double ObjectiveLensNA() = 0;
    virtual double ObjectiveLensReadyPos() = 0;
    virtual double ObjectiveLensLimitPos() = 0;

    //Condenser Lens
    virtual double CondenserLensMagnification() { return ObjectiveLensMagnification(); }
    virtual double CondenserLensNA() { return ObjectiveLensNA(); }
    virtual double CondenserLensReadyPos() = 0;
    virtual double CondenserLensLimitPos() = 0;

    //Condenser Lens Auto Calibration
    virtual double CondenserLensStepAmount(TC::CalibrationStepType type) = 0;
    virtual double CondenserLensDistThreshold(TC::CalibrationDistThreshold type) = 0;

    //Processing
    virtual unsigned int ZeroPaddingMax2D() = 0;
    virtual unsigned int ZeroPaddingMax3D() = 0;
    virtual unsigned int NumberOfIteration() = 0;

    //Fluorescence
    virtual double FluorescenceScanDepth() = 0;
    virtual double FluorescenceScanStep() = 0;
    virtual double FluorescenceScanStepUnit() = 0;
    virtual double DarkpixelCalibrationGain() { return 12; }        //12dB
    virtual double DarkpixelCalibrationExposure() { return 1000; }  //1000msec

    //Motion
    virtual double MotionMovableLowerX() = 0;
    virtual double MotionMovableUpperX() = 0;
    virtual double MotionMovableLowerY() = 0;
    virtual double MotionMovableUpperY() = 0;

    virtual bool isCAxisSafeToMove(double curCPos, double tarCPos, double curZPos) = 0;
    virtual bool isZAxisSafeToMove(double curZPos, double tarZPos, double curCPos) = 0;

    //=== Model-specific functionality ===
    virtual bool doesSupportXYZControl() = 0;
    virtual bool doesSupportFluorescence() = 0;
    virtual bool doesSupportBrightfield() { return false; }
    virtual bool doesSupportBrightfieldMono() { return false; }
    virtual bool doesSupportBrightfieldColor() { return false; }
    virtual uint32_t supportedViewModes() const = 0;
    bool doesSupportMode(const Modes mode) const {
        const auto modes = supportedViewModes();
        return (modes & mode) == mode;
    }
};
