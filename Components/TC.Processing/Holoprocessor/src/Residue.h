#pragma once

#include <math.h>

#include "math_constants.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


__global__ void Residue(float* dev_Phase, const int Xsize, const int Ysize, int* dev_Residue_num);
