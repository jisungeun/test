#pragma once

#include <arrayfire.h>

#include <memory>

namespace TC {
    class AfArrayProxy {
    public:
        typedef AfArrayProxy Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        AfArrayProxy(void);
        AfArrayProxy(af::array& inArray);
        ~AfArrayProxy();

        void Set(af::array& inArray);
        bool Get(af::array& outArray);

    private:
        void* copy_to_host_f32(af::array& inArray);
        void* copy_to_host_c32(af::array& inArray);
        void* copy_to_host_f64(af::array& inArray);
        void* copy_to_host_c64(af::array& inArray);
        void* copy_to_host_b8(af::array& inArray);
        void* copy_to_host_s32(af::array& inArray);
        void* copy_to_host_u32(af::array& inArray);
        void* copy_to_host_u8(af::array& inArray);
        void* copy_to_host_s64(af::array& inArray);
        void* copy_to_host_u64(af::array& inArray);
        void* copy_to_host_s16(af::array& inArray);
        void* copy_to_host_u16(af::array& inArray);

    private:
        af::dtype m_dtype;
        af::dim4 m_dims;
        void* m_pRawArray;
    };
};
