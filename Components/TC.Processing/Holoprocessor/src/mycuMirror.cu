

#include "mycuMirror.h"

__global__ void
makeMirrorImage(float* dev_input, float* dev_mirrored_output, const int input_Xsize, const int input_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int mirror_index = i + j * (blockDim.x * gridDim.x);

    const int Mirror_Xsize = 2 * input_Xsize;
    const int Mirror_Ysize = 2 * input_Ysize;
    const int Mirrored_Image_length = Mirror_Xsize * Mirror_Xsize;
    while (mirror_index < Mirrored_Image_length) {
        const int Mirror_x = mirror_index % Mirror_Xsize;
        const int Mirror_y = mirror_index / Mirror_Xsize;

        const bool input_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Right_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Down_mirror_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        const bool Diagonal_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        if (input_condi) {
            const int input_x = Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index] = dev_input[input_index];
        } else if (Right_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index] = dev_input[input_index];
        } else if (Down_mirror_condi) {
            const int input_x = Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index] = dev_input[input_index];
        } else if (Diagonal_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index] = dev_input[input_index];
        }

        mirror_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }





}

__global__ void
extract_from_mirror(float* dev_mirrored_output, float* dev_output, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int extraction_index = i + j * (blockDim.x * gridDim.x);

    int extraction_x = extraction_index % extraction_Xsize;
    int extraction_y = extraction_index / extraction_Ysize;

    bool in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);

    while (in_output_array) {
        int mirror_index = extraction_x + extraction_y * mirror_xsize;
        dev_output[extraction_index] = dev_mirrored_output[mirror_index];

        extraction_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        extraction_x = extraction_index % extraction_Xsize;
        extraction_y = extraction_index / extraction_Xsize;
        in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);
    }
}


__global__ void
extract_from_mirror(cufftComplex* dev_mirrored_output, float* dev_output, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int extraction_index = i + j * (blockDim.x * gridDim.x);

    int extraction_x = extraction_index % extraction_Xsize;
    int extraction_y = extraction_index / extraction_Ysize;

    bool in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);

    while (in_output_array) {
        int mirror_index = extraction_x + extraction_y * mirror_xsize;
        dev_output[extraction_index] = dev_mirrored_output[mirror_index].x;

        extraction_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        extraction_x = extraction_index % extraction_Xsize;
        extraction_y = extraction_index / extraction_Xsize;
        in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);
    }
}

__global__ void
subtracted_extraction(cufftReal* dev_term1, cufftReal* dev_term2, cufftReal* dev_extracted_phase, const int mirror_xsize, const int mirror_ysize, const int extraction_Xsize, const int extraction_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int extraction_index = i + j * (blockDim.x * gridDim.x);

    int extraction_x = extraction_index % extraction_Xsize;
    int extraction_y = extraction_index / extraction_Ysize;

    bool in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);

    while (in_output_array) {
        int mirror_index = extraction_x + extraction_y * mirror_xsize;
        dev_extracted_phase[extraction_index] = dev_term1[mirror_index] - dev_term2[mirror_index];

        extraction_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
        extraction_x = extraction_index % extraction_Xsize;
        extraction_y = extraction_index / extraction_Xsize;
        in_output_array = (extraction_x < extraction_Xsize) && (extraction_y < extraction_Ysize);
    }
}

__global__ void makeExpImagMirrorImage(float* dev_input, cufftComplex* dev_mirrored_output, const int input_Xsize, const int input_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int mirror_index = i + j * (blockDim.x * gridDim.x);

    const int Mirror_Xsize = 2 * input_Xsize;
    const int Mirror_Ysize = 2 * input_Ysize;
    const int Mirrored_Image_length = Mirror_Xsize * Mirror_Xsize;
    while (mirror_index < Mirrored_Image_length) {
        const int Mirror_x = mirror_index % Mirror_Xsize;
        const int Mirror_y = mirror_index / Mirror_Xsize;

        const bool input_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Right_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Down_mirror_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        const bool Diagonal_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        if (input_condi) {
            const int input_x = Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index].x = cosf(dev_input[input_index]);
            dev_mirrored_output[mirror_index].y = sinf(dev_input[input_index]);
        } else if (Right_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index].x = cosf(dev_input[input_index]);
            dev_mirrored_output[mirror_index].y = sinf(dev_input[input_index]);
        } else if (Down_mirror_condi) {
            const int input_x = Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index].x = cosf(dev_input[input_index]);
            dev_mirrored_output[mirror_index].y = sinf(dev_input[input_index]);
        } else if (Diagonal_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_mirrored_output[mirror_index].x = cosf(dev_input[input_index]);
            dev_mirrored_output[mirror_index].y = sinf(dev_input[input_index]);
        }

        mirror_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

__global__ void
makeSinCosMirrorImage(float* dev_input, cufftReal* dev_Sin_mirrored_output, cufftReal* dev_Cos_mirrored_output, const int input_Xsize, const int input_Ysize) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int mirror_index = i + j * (blockDim.x * gridDim.x);

    const int Mirror_Xsize = 2 * input_Xsize;
    const int Mirror_Ysize = 2 * input_Ysize;
    const int Mirrored_Image_length = Mirror_Xsize * Mirror_Xsize;

    while (mirror_index < Mirrored_Image_length) {
        const int Mirror_x = mirror_index % Mirror_Xsize;
        const int Mirror_y = mirror_index / Mirror_Xsize;

        const bool input_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Right_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= 0) &&
            (Mirror_y < input_Ysize);

        const bool Down_mirror_condi =
            (Mirror_x >= 0) &&
            (Mirror_x < input_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        const bool Diagonal_mirror_condi =
            (Mirror_x >= input_Xsize) &&
            (Mirror_x < Mirror_Xsize) &&
            (Mirror_y >= input_Ysize) &&
            (Mirror_y < Mirror_Ysize);

        if (input_condi) {
            const int input_x = Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_Sin_mirrored_output[mirror_index] = cosf(dev_input[input_index]);
            dev_Cos_mirrored_output[mirror_index] = sinf(dev_input[input_index]);
        } else if (Right_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_Sin_mirrored_output[mirror_index] = cosf(dev_input[input_index]);
            dev_Cos_mirrored_output[mirror_index] = sinf(dev_input[input_index]);
        } else if (Down_mirror_condi) {
            const int input_x = Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_Sin_mirrored_output[mirror_index] = cosf(dev_input[input_index]);
            dev_Cos_mirrored_output[mirror_index] = sinf(dev_input[input_index]);
        } else if (Diagonal_mirror_condi) {
            const int input_x = (Mirror_Xsize - 1) - Mirror_x;
            const int input_y = (Mirror_Ysize - 1) - Mirror_y;
            const int input_index = input_x + input_y * input_Xsize;
            dev_Sin_mirrored_output[mirror_index] = cosf(dev_input[input_index]);
            dev_Cos_mirrored_output[mirror_index] = sinf(dev_input[input_index]);
        }

        mirror_index += blockDim.x * blockDim.y * gridDim.x * gridDim.y;
    }
}

