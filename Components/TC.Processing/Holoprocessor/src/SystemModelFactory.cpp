#include <iostream>

#include <QStringList>

#include "SystemModelFactory.h"
#include "SystemModelHT1SM.h"
#include "SystemModelHT1S.h"
#include "SystemModelHT1H.h"
#include "SystemModelHT2S.h"
#include "SystemModelHT2H.h"
#include "SystemModelHT1L.h"
#include "SystemModelHT2L.h"
#include "SystemModelHT1H_Gen2.h"
#include "SystemModelHT2H_Gen2.h"
#include "SystemModelHT1L_Gen2.h"
#include "SystemModelHT2L_Gen2.h"

SystemModelFactory::Pointer theFactory;

SystemModelFactory::SystemModelFactory() {
    AddModel(SystemModelHT1SM::New());
    AddModel(SystemModelHT1S::New());
    AddModel(SystemModelHT1H::New());
    AddModel(SystemModelHT2S::New());
    AddModel(SystemModelHT2H::New());
    AddModel(SystemModelHT1L::New());
    AddModel(SystemModelHT2L::New());
    AddModel(SystemModelHT1H_Gen2::New());
    AddModel(SystemModelHT2H_Gen2::New());
    AddModel(SystemModelHT1L_Gen2::New());
    AddModel(SystemModelHT2L_Gen2::New());
}

void SystemModelFactory::AddModel(SystemModelInterface::Pointer pModel) {
    m_models[pModel->Model()] = pModel;
}

QStringList SystemModelFactory::GetModels() {
    if (theFactory.get() == NULL) theFactory.reset(new SystemModelFactory());

    QStringList strList;

    for (auto it = theFactory->m_models.begin(); it != theFactory->m_models.end(); ++it) {
        strList.push_back(it.value()->Model());
    }

    return strList;
}

SystemModelInterface::Pointer SystemModelFactory::Model(const QString& strModel) {
    if (theFactory.get() == NULL) theFactory.reset(new SystemModelFactory());

    SystemModelInterface::Pointer model;

    auto it = theFactory->m_models.find(strModel);
    if (it != theFactory->m_models.end()) model = it.value();

    return model;
}
