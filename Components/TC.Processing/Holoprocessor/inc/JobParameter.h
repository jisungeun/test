#pragma once

#include <QObject>
#include <QString>
#include <QDateTime>

#include "GlobalDefines.h"
#include "TCHoloprocessorExport.h"

#define TAG_CREATED_DATETIME			"Created"
#define TAG_UPDATED_DATETIME			"Updated"
#define TAG_JOB_TITLE					"Title"
#define TAG_ANNOTATION_TITLE			"Annotation"
#define TAG_USERDEFINED_TITLE		"Job_Title"
#define TAG_FIELDOFVIEW_H				"Field_of_View_-_H"
#define TAG_FIELDOFVIEW_V				"Field_of_View_-_V"
#define TAG_FIELDOFVIEW_H_PX		"Field_of_View_-_H_px"
#define TAG_FIELDOFVIEW_V_PX		"Field_of_View_-_V_px"
#define TAG_MEDIUM_NAME					"Medium_Name"
#define TAG_MEDIUM_RI					"Medium_RI"
#define TAG_IMMERSION_RI                "Immersion_RI"
#define TAG_CAMERA_SHUTTER				"Camera_Shutter"
#define TAG_CAMERA_GAIN					"Camera_Gain"
#define TAG_ACQUISITION_QUALITY			"Acquisition_quality"
#define	TAG_ACQUISITION_TIME_2D			"Acquisition_time_2D"
#define	TAG_ACQUISITION_TIME_3D			"Acquisition_time_3D"
#define TAG_ACQUISITION_INTERVAL_2D		"Acquisition_interval_2D"
#define TAG_ACQUISITION_INTERVAL_3D		"Acquisition_interval_3D"
#define TAG_ITERATION					"Iteration"
#define TAG_DISABLE_ZLIMIT              "Disable ZLimit"
#define TAG_IGNORE_POOR                 "Ignore Poor"
#define TAG_FL_CAMERA_SHUTTER(ch)		"FLCH" #ch "_Camera_Shutter"
#define TAG_FL_CAMERA_GAIN(ch)			"FLCH" #ch "_Camera_Gain"
#define TAG_FL_LIGHT_INTENSITY(ch)	"FLCH" #ch "_Light_Intensity"
#define TAG_FL_ENABLE(ch)						"FLCH" #ch "_Enable"
#define TAG_FL_FLUOROPHORE_NAME(ch)		"FLCH" #ch "_Fluorophore_Name"
#define TAG_FL_FLUOROPHORE_EMISSION(ch)	"FLCH" #ch "_Fluorophore_Emission"
#define TAG_FL_SCANRANGE						"FL_Scan_Range"
#define TAG_FL_SCANSTEP							"FL_Scan_Step"
#define TAG_FL_SCANSTEPS						"FL_Scan_Steps"
#define TAG_FL_SCANCENTER						"FL_Scan_Center"
#define	TAG_FL_ACQUISITION_TIME_3D			"FL_Acquisition_time_3D"
#define TAG_FL_ACQUISITION_INTERVAL_3D		"FL_Acquisition_interval_3D"
#define TAG_BF_CAMERA_SHUTTER			"BF_Camera_Shutter"
#define TAG_BF_LIGHT_INTENSITY			"BF_Light_Intensity"

class TCHoloprocessor_API JobParameter : public QObject {
public:
    JobParameter(QObject* parent = 0);
    JobParameter(const QString& strPath, QObject* parent = 0);
    JobParameter(const JobParameter& other);
    ~JobParameter();

    void Save(void);
    void Save(const QString& strPath);
    bool Load(const QString& strPath);

    void operator=(const JobParameter& param);
    bool operator==(const JobParameter& param);
    bool operator!=(const JobParameter& param) { return !(*this == param); }

    bool isValid(void) { return m_bValid; }

    QString name(void) const;

    QString title(void) const;
    void title(const QString& strTitle);

    QString annotationTitle(void) const;
    void annotationTitle(const QString& strTitle);

    double fieldOfViewH(void) const;
    double fieldOfViewV(void) const;
    int fieldOfViewHPixels(void) const;
    int fieldOfViewVPixels(void) const;
    void fieldOfView(const double dFOVH, const double dFOVV);
    void fieldOfViewPixels(const int nFOVH, const int nFOVV);

    int pixelsH(void) const;
    int pixelsV(void) const;

    QString mediumName(void) const;
    void mediumName(const QString& strName);

    double mediumRI(void) const;
    void mediumRI(const double dRI);

    double immersionRI(void) const;
    void immersionRI(const double dRI);

    double cameraShutter(void) const;
    void cameraShutter(const double dValue);

    double cameraGain(void) const;
    void cameraGain(const double dValue);

    double BFCameraShutter(void) const;
    void BFCameraShutter(const double dValue);

    double BFLightIntensity(void) const;
    void BFLightIntensity(const double dValue);

    int BFPixelsH(void) const;
    int BFPixelsV(void) const;

    int acquisitionQuality(void) const;
    void acquisitionQuality(const int nQuality);

    int acqusitionFrames(void) const;
    int reconstructionMethod(void);

    double acquisitionTime2D(void) const;
    void acquisitionTime2D(const double dSecond);

    double acquisitionTime3D(void) const;
    void acquisitionTime3D(const double dSecond);

    double acquisitionInterval2D(void) const;
    void acquisitionInterval2D(const double nSecond);

    double acquisitionInterval3D(void) const;
    void acquisitionInterval3D(const double nSecond);

    int IterationCount(void) const;
    void IterationCount(int nCount);

    bool DisableZLimit(void) const;
    void DisableZLimit(bool bDisable);

    int IgnorePoor(void) const;
    void IgnorePoor(int nIgnore);

    double FLCameraShutter(TC::FLChannel channel) const;
    void FLCameraShutter(TC::FLChannel channel, double dValue);

    double FLCameraGain(TC::FLChannel channel) const;
    void FLCameraGain(TC::FLChannel channel, double dValue);

    double FLLightIntensity(TC::FLChannel channel) const;
    void FLLightIntensity(TC::FLChannel channel, double dValue);

    QString FLFluorophoreName(TC::FLChannel channel) const;
    void FLFluorophoreName(TC::FLChannel channel, const QString name);

    int FLFluorophoreEmission(TC::FLChannel channel) const;
    void FLFluorophoreEmission(TC::FLChannel channel, int emission);

    double FLScanRange(void) const;
    void FLScanRange(double dValue);

    double FLScanStep(void) const;
    void FLScanStep(double dValue);

    int FLScanSteps(void) const;
    void FLScanSteps(int nValue);

    double FLScanCenter(void) const;
    void FLScanCenter(double dValue);

    bool FLEnableChannel(TC::FLChannel channel) const;
    void FLEnableChannel(TC::FLChannel channel, bool bEnable);

    double FLAcquisitionTime3D(void) const;
    void FLAcquisitionTime3D(const double nSecond);

    double FLAcquisitionInterval3D(void) const;
    void FLAcquisitionInterval3D(const double nSecond);

    QString deconvConfigPath(void);

protected:
    void updateFromModel(void);

protected:
    QDateTime m_dtCreated;
    QDateTime m_dtUpdated;

    QString m_strJobName;
    QString m_strUserDefinedTitle;		//! title for acquisition
    QString m_strAnnotationTitle;		//! title of annotation configuration

    double m_dFieldOfViewH;				//field of view in um
    double m_dFieldOfViewV;

    int m_nFieldOfViewHPixel;			//field of view in pixels
    int m_nFieldOfViewVPixel;

    double m_dAcquisitionTime2D;			//! acquisition time in second
    double m_dAcquisitionTime3D;
    double m_dAcquisitionInterval2D;	//! acquisition interval in second
    double m_dAcquisitionInterval3D;

    //ODT Parameter
    struct {
        QString m_strMedium;
        double m_dMediumRI;
        double m_dImmersionRI;

        double m_dCameraShutter;		//! camera shutter
        double m_dCameraGain;
        int m_nAcquisitionQuality;
        int m_nIteration;				//! iteration
        bool m_bDisableZLimit;
        int m_nIgnorePoor;
    } odt;

    //Widefield Parameter
    struct {
        double m_dCameraShutter;		//! camera shutter
        double m_dIntesnity;			//! light intensity
    } wf;

    //FT Parameter
    struct {
        double m_dCameraShutter;		//! camera shutter
        double m_dGain;					//! camera gain
        double m_dIntensity;			//! light intensity
        bool m_bEnable;					//! on/off light channel
        QString m_strFluorophore;		//! fluorophore name
        int m_nEmission;				//! emission wavelength in nm
    } ft[3];

    double m_dFLScanStep;				//! scan step amount
    int m_nFLScanSteps;					//! number of steps
    double m_dFLScanRange;				//! scan range
    double m_dFLScanCenter;				//! the center of scan range

    double m_dFLAcquisitionTime3D;		//! acquisition time in second
    double m_dFLAcquisitionInterval3D;	//! acquisition interval in second

private:
    QString m_strLoadedPath;			//! loaded path (not a parameter)
    bool m_bValid;
};
