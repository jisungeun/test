#pragma once

#include <QString>
#include <QMap>
#include <QDateTime>

#include <arrayfire.h>
#include <af/data.h>

#include "TomoBuilderGlobal.h"

#include "TCHoloprocessorExport.h"

#define QSTR2CSTR_CPTR(var)		((var).toLocal8Bit().constData())

#ifndef TC_MEMO_INFO
#define TC_MEM_INFO(msg) do {                                                           \
    size_t abytes = 0, abuffs = 0, lbytes = 0, lbuffs = 0;                              \
    af_err err = af_device_mem_info(&abytes, &abuffs, &lbytes, &lbuffs);                \
    if(err == AF_SUCCESS) {                                                             \
        printf("AF Memory at %s:%d: " msg "\n", __FILE__, __LINE__);                    \
        printf("Allocated [ Bytes | Buffers ] = [ %lld | %lld ]\n", abytes, abuffs);      \
        printf("In Use    [ Bytes | Buffers ] = [ %lld | %lld ]\n", lbytes, lbuffs);      \
				    } else {                                                                      \
        fprintf(stderr, "AF Memory at %s:%d: " msg "\nAF Error %d\n",                   \
                __FILE__, __LINE__, err);                                               \
				    }                                                                           \
} while(0)
#endif

#define TC_MEM_LIST(msg) do {																														\
	printf("AF Memory at %s:%d: " msg "\n", __FILE__, __LINE__);													\
	af::printMemInfo("");																																	\
} while(0)

namespace TC {
#define fftshift(in)							af::shift(in, std::floor(in.dims(0)/2), std::floor(in.dims(1)/2))
#define ifftshift(in)							af::shift(in, std::ceil(in.dims(0)/2), std::ceil(in.dims(1)/2))
#define fftshift3(in)							af::shift(in, std::floor(in.dims(0)/2), std::floor(in.dims(1)/2), std::floor(in.dims(2)/2))
#define ifftshift3(in)						af::shift(in, std::ceil(in.dims(0)/2), std::ceil(in.dims(1)/2), std::ceil(in.dims(2)/2))
#define fftshift3n(in,d0,d1,d2)		af::shift(in, d0*std::floor(in.dims(0)/2), d1*std::floor(in.dims(1)/2), d2*std::floor(in.dims(2)/2))
#define ifftshift3n(in,d0,d1,d2)	af::shift(in, d0*std::ceil(in.dims(0)/2), d1*std::ceil(in.dims(1)/2), d2*std::ceil(in.dims(2)/2))

    //! release device meory
    void TCHoloprocessor_API release(void);
    void TCHoloprocessor_API release(af::array& arrIn, bool bGarbeCollection = false);

    //! clean up cufft plans
    void TCHoloprocessor_API release_fft_plans(void);

    //! load parameter file
    bool TCHoloprocessor_API LoadFullParameter(const QString& strPath, QMap<QString, QString>& map);
    bool TCHoloprocessor_API LoadParameter(const QString& strPath, QMap<QString, double>& map);

    //! load image
    bool TCHoloprocessor_API LoadImage(const QString& strPath, af::array& outArray);

    //! load images from folder
    bool TCHoloprocessor_API LoadImages(const QString& strPath, const QString& strExt, int nImgOffset, bool bOnlyNormal, af::array& outArray, unsigned int& nImages, bool bAsFloat = true, bool bReverse = false);
    bool TCHoloprocessor_API LoadImagesColor(const QString& strPath, const QString& strExt, int nImgOffset, af::array& outArray, unsigned int& nImages);

    //! find
    bool TCHoloprocessor_API Find(const af::array& arrIn, af::array& arrRow, af::array& arrCol);
    bool TCHoloprocessor_API Find(const af::array& arrIn, unsigned int& nRow, unsigned int& nCol);
    int TCHoloprocessor_API Find1D(const af::array& arrIn, bool first = true);

    //! ifftshift3 for saving memory usage
    void TCHoloprocessor_API _ifftshift3(const af::array& arrIn, af::array& arrOut);
    void TCHoloprocessor_API _ifftshift3(af::array& arrIn);

    //! max
    bool TCHoloprocessor_API Max(const af::array& arrIn, float& value, unsigned int& idx);

    //! min
    bool TCHoloprocessor_API Min(const af::array& arrIn, float& value, unsigned int& idx);

    //! mk_ellipse
    bool TCHoloprocessor_API mk_ellipse(af::array& arrOut, double XR, double YR, unsigned int X, unsigned int Y);
    af::array TCHoloprocessor_API mk_ellipse(double XR, double YR, unsigned int X, unsigned int Y);


    //! strel('disk', radius)
    af::array TCHoloprocessor_API strel_disk(unsigned int radius);

    //! strel('square', width)
    af::array TCHoloprocessor_API strel_square(unsigned int width);

    //! mk_mask_circle
    af::array TCHoloprocessor_API mk_mask_circle(unsigned int R, unsigned int X0, unsigned int Y0, unsigned int XS, unsigned int YS, unsigned int CW = 0);

    //! meshgrid
    bool TCHoloprocessor_API meshgrid(af::array& arrXX, af::array& arrYY, unsigned int X0, unsigned int X1, unsigned int Y0, unsigned int Y1);
    bool TCHoloprocessor_API meshgrid(af::array& arrXX, af::array& arrYY, const af::array& arrXGV, const af::array& arrYGV);
    bool TCHoloprocessor_API meshgrid_square(af::array& arrXX, af::array& arrYY, const af::array& arrXGV, const af::array& arrYGV);
    bool TCHoloprocessor_API meshgrid2(af::array& outX, af::array& outY, af::array& outZ, const af::array& XGV, const af::array& YGV, const af::array& ZGV);

    //! PhiShift
    bool TCHoloprocessor_API PhiShift(af::array& arrOut, const af::array& arrIn);
    af::array TCHoloprocessor_API PhiShift(const af::array& arrIn);
    af::array TCHoloprocessor_API PhiShiftJH(const af::array& arrIn, const af::array& arrMask);
    af::array TCHoloprocessor_API PhiShiftKR3(const af::array& angimg, int rampStyle, const af::array& BGmask, const char* logname = "PhiShiftKR3.arr");
    af::array TCHoloprocessor_API PhiShiftKR3(const af::array& angimg, int rampStyle, const af::array& BGmask, double& mdx, double& mdy, const char* logname = "PhiShiftKR3.arr");

    //! angle
    bool TCHoloprocessor_API angle(af::array& arrOut, const af::array& arrIn);
    af::array TCHoloprocessor_API angle(const af::array& arrIn);

    //! unwrap2
    af::array TCHoloprocessor_API unwrap2(const af::array& arrIn, bool& bGoodQuality, int& score);

    //! sqrt - sqrt for complex
    //REF : https://en.wikipedia.org/wiki/Complex_number#Square_root
    //      The square roots of a + bi (with b �� 0) are  +/-(g + di), where
    //      g = sqrt((a + sqrt(a^2+b^))/2)
    //      d = sgn(b) * sqrt((-a + sqrt(a^2+b^2))/2)
    bool TCHoloprocessor_API sqrt(af::array& arrGamma, af::array& arrDelta, const af::array& arrIn);
    bool TCHoloprocessor_API sqrt(af::array& arrOut, const af::array& arrIn);

    //! sqrt - sqrt for real with handling NaN
    af::array sqrt_real(af::array& arrIn);

    //! pow2complex
    //af::pow doesn't support complex number
    af::array TCHoloprocessor_API pow2complex(const af::array& arrIn);

    //padarray
    template<typename T>
    af::array padarray(const af::array& arrIn, unsigned int padRow, unsigned int padCol, T padVal) {
        const unsigned int rows = arrIn.dims(0);
        const unsigned int cols = arrIn.dims(1);

        af::array arrOut = af::constant<T>(padVal, (padRow * 2 + rows), (padCol * 2 + cols));

        const unsigned int r0 = padRow;
        const unsigned int r1 = padRow + rows - 1;
        const unsigned int c0 = padCol;
        const unsigned int c1 = padCol + cols - 1;

        arrOut(af::seq(r0, r1), af::seq(c0, c1)) = arrIn(af::span, af::span);

        return arrOut;
    }

    //unique
    bool TCHoloprocessor_API unique(af::array& arrVal, af::array& arrIdx, const af::array& arrIn);
    af::array TCHoloprocessor_API uniqueVal(const af::array& arrIn);
    af::array TCHoloprocessor_API uniqueIdx(const af::array& arrIn);

    //maskarray
    af::array TCHoloprocessor_API maskarray2d(const af::array& arrIdx, int d0, int d1);

    //phase2rgb
    af::array TCHoloprocessor_API phase2rgb(const af::array& arrIn, int mode, float fmax);
    af::array TCHoloprocessor_API amplitue2rgb(const af::array& arrIn);
    af::array TCHoloprocessor_API phase2dic(const af::array& arrIn, float fmax, unsigned int nShift = 3);
    af::array TCHoloprocessor_API phase2contrast(const af::array& arrAmp, const af::array& arrPha, double dOffset = 1.0);

    //imagesc with jet colormap
    af::array TCHoloprocessor_API imagesc(const af::array arrIn);

    //graythresh
    float TCHoloprocessor_API graythresh(const af::array& in);
    af::array TCHoloprocessor_API im2bw(const af::array& in, float level);

    //fit_gaussian
    void TCHoloprocessor_API fit_gaussian(float* pData, unsigned int nLen, const std::vector<float>& start, std::vector<float>& param);

    //fspecial_disk_1
    af::array TCHoloprocessor_API fspecial_disk_1();
    af::array TCHoloprocessor_API fspecial_disk_2();

    af::array TCHoloprocessor_API zeropadding_2d(af::array& arrIn, unsigned int nSize, bool bShift = false);

    af::array TCHoloprocessor_API interp_z(af::array& arrIn, unsigned int new_z, int xyratio = 1);
    af::array TCHoloprocessor_API interp_xy2z(af::array& arrIn, unsigned int new_z);

    //hilbert transform
    void TCHoloprocessor_API hilbert2(const af::array& arrIn, double pixelsize, double NA, double lambda, unsigned int order, af::array& Pimg, double& mx, double& my, const char* logname = "hilbert2.arr");

    //linspace
    af::array TCHoloprocessor_API linspace(double x1, double x2, unsigned int n);

    //replace nan to zero
    void TCHoloprocessor_API nan2zero(af::array& arrIn);

    //otsu
    float TCHoloprocessor_API otsuthresh(const af::array& arrIn, const int bins = 256);

    //utility
    int TCHoloprocessor_API saveVal(const char* key, float value, const char* path, bool append = false);
    int TCHoloprocessor_API saveVal(const char* key, double value, const char* path, bool append = false);
    int TCHoloprocessor_API saveVal(const char* key, int value, const char* path, bool append = false);
    int TCHoloprocessor_API saveVal(const char* key, unsigned int value, const char* path, bool append = false);
    int TCHoloprocessor_API saveVal(const char* key, unsigned long long value, const char* path, bool append = false);

    //imwrite
    void TCHoloprocessor_API imwrite(const af::array& arrIn, const char* filename);

    //export/import raw image
    bool TCHoloprocessor_API exportRAW(af::array& image, const QString& strPath);
    bool TCHoloprocessor_API importRAW(af::array& image, const QString& strPath, int d0, int d1, int d2);
    bool TCHoloprocessor_API importRAW32(af::array& image, const QString& strPath, float& minV, float& maxV, int d0, int d1, int d2);
    bool TCHoloprocessor_API importRAW32_8(af::array& image, const QString& strPath, int d0, int d1, int d2);
    bool TCHoloprocessor_API importRAW32_16(af::array& image, const QString& strPath, int d0, int d1, int d2);

    //utility
    QStringList TCHoloprocessor_API getDataList(const QString& strPath);
    unsigned long long TCHoloprocessor_API loadTimestamp(const QString& strPath);
    QString TCHoloprocessor_API loadTimestampStr(const QString& strPath);
    QString TCHoloprocessor_API TimeStamp2String(uint64_t timestamp);
    QDateTime TCHoloprocessor_API TimeStamp2DateTime(uint64_t timestamp);
    Position TCHoloprocessor_API loadPosition(const QString& strPath);
    QString TCHoloprocessor_API LoadComment(const QString& commentPath);
    auto TCHoloprocessor_API LoadAnnotation(const QString& annotationPath)->QMap<QString,QString>;
    auto TCHoloprocessor_API LoadTile(const QString& tilePath)->QMap<QString, QString>;
    auto TCHoloprocessor_API CurrentDateTime()->QString;
    auto TCHoloprocessor_API GenUniqueID(QString strDevice, QString strRecodingTime, QString strCurrentDate)->QString;
    auto TCHoloprocessor_API GenDataID(QString strDevice, QString strRecodingTime)->QString;
    auto TCHoloprocessor_API FileExists(QString filePath)->bool;

    auto TCHoloprocessor_API GetHtXWorldLength(const QString& configPath, const QString& jobParameterPath)->double;
    auto TCHoloprocessor_API GetHtZWorldLength(const QString& configPath, const QString& jobParameterPath)->double;


    //! display
    void TCHoloprocessor_API dbgPrint(const af::array& arrIn, const char* varName = 0, const char* atFile = 0, int atLine = 0);
    void TCHoloprocessor_API dbgPrintType(af::dtype type);
    void TCHoloprocessor_API dbgIsNan(const af::array& arrIn, const char* varName = 0, const char* atFile = 0, int atLine = 0);
}

#ifdef DEEP_DEBUG

#include <stdio.h>

#define dbg_print(exp) TC::dbgPrint(exp, #exp, __FILE__, __LINE__);
#define dbg_isnan(exp) TC::dbgIsNan(exp, #exp, __FILE__, __LINE__);
#define dbg_saveArray(name, var, file, append) dbg_print(var); dbg_isnan(var); af::saveArray(name, var, file, append);
#define dbg_saveArrayIdx(name, idx, var, file, append) dbg_print(var); dbg_isnan(var); { char name_str[128] = {0, };  sprintf_s(name_str, 128, "%s_%d", name, idx); af::saveArray(name_str, var, file, append); }

#else //DEEP_DEBUG

#ifdef SHALLOW_DEBUG

#define dbg_print(exp) TC::dbgPrint(exp, #exp, __FILE__, __LINE__);
#define dbg_isnan(exp) TC::dbgIsNan(exp, #exp, __FILE__, __LINE__);
#define dbg_saveArray(name, var, file, append) dbg_print(var); dbg_isnan(var);
#define dbg_saveArrayIdx(name, idx, var, file, append) dbg_print(var); dbg_isnan(var);

#else //SHALLOW_DEBUG

#define dbg_print(exp) ;
#define dbg_isnan(exp) ;
#define dbg_saveArray(name, var, file, append) var.eval() ;
#define dbg_saveArrayIdx(name, idx, var, file, append) ;

#endif //SHALLOW_DEBUG

#endif //DEEP_DEBUG
