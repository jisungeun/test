#pragma once
#include <any>

#include "HoloProcessor.h"
#include "TomoBuilderInterface.h"
#include "FluorescenceBuilderInterface.h"

#include "TCHoloprocessorExport.h"

class BuilderProgressReporter;
class BuilderStopChecker;

class TCHoloprocessor_API HoloProcessorArrayFire : public HoloProcessor {
    class Deleter {
    public:
        void operator()(HoloProcessorArrayFire* p) {
            delete p;
        }
    };

public:
    HoloProcessorArrayFire(ProgressReporter* reporter = 0, QCoreApplication* pApplication = 0, int nDebugLevel = 0);
    ~HoloProcessorArrayFire();

    static std::shared_ptr<HoloProcessor> create(ProgressReporter* reporter, QCoreApplication* pApplication, int nDebugLevel) {
        std::shared_ptr<HoloProcessor> p(new HoloProcessorArrayFire(reporter, pApplication, nDebugLevel), HoloProcessorArrayFire::Deleter());
        return p;
    }

    bool initialize() override;
    bool isInitialized() override;
    bool isInitTimeout() override;

    bool clear() override;
    bool procHologram2DSingle(const QString& srcImgPath, const QString& bgImgPath, const QString& bgImgPath2, const QString& configPath, const QString& outputPath, TC::ImageType type, double dRange, bool bCompensateAnalysis) override;
    bool procHologram2D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, const QString& jobParameterPath, const bool& htConsider, bool isReprocess = false, const int32_t& timeFrameIndex = -1) override;
    bool procHologram3D(const QString& configPath, const QString& workingPath, const QString& referencePath, const QString& outputPath, bool isReprocess = false) override;
    bool procHologram3DFL(const QString& configPath, const QString& darkpixelPath, const QString& workingPath, const QString& jobParameterPath, bool inclDeconv, bool firstSetPerChannel, const int32_t& channelIndex, const bool& htConsider) override;
    bool procBrightfield(const QString& configPath, const QString& workingPath, const QString& outputPath, bool isReprocessing = false) override;
    bool saveTCF(const QString& configPath, const QString& annotationPath, const QString& tilePath, const QString& commentPath, const QString& timestampPath, const QString& outputPath) override;

    bool procReference(const QString& strCalPath, bool bSave) override;
    bool procDarkpixel(const QString& strImgPath, const QString& strOutPath) override;

    bool loadDarkPixel(const QString& strDarkPixel) override;
    bool procRemoveDarkPixel(const QString& strImgPath, const QString& strOutPath) override;

    void reportBuilderProgress(int value);

    auto GetRawData() const->std::any;
    auto GetRawDataMip() const->std::any;
    auto GetMinMax() const->TC::MinMax;
    auto GetMinMaxMip() const->TC::MinMax;
    auto GetResolution() const->TC::Resolution;
    auto GetSizes() const->TC::Sizes;
protected:
    bool loadReference(TC::TomoBuilderInterface* pInterface, const QString& referencePath);
    bool isValidData(TC::TomoBuilderInterface* pInterface);

private:
    QStringList m_dataSet;
    TC::FluorescenceBuilderInterface* m_pFlInterface;	//! only for monitoring purpose...

    std::any rawData;
    std::any rawDataMip;
    TC::MinMax minMax;
    TC::MinMax minMaxMip;
    TC::Resolution resolution;
    TC::Sizes sizes;

    friend class BuilderProgressReporter;
    friend class BuilderStopChecker;
};