#pragma once

#include <QString>
#include <QStringList>

#include "JobParameter.h"
#include "JobItem.h"

#include "TCHoloprocessorExport.h"

namespace TC {
    void TCHoloprocessor_API convJobParameter2Inputfile(const JobParameter& jobParam, const QString& strOutPath, JobItemSwitch* jobItem = 0);
    void TCHoloprocessor_API updateInputfile(const QString& strPath, const QString& param, const QVariant& val);

    QString TCHoloprocessor_API get_background_path(const QString& key = QString("default"));
    void TCHoloprocessor_API clear_background(const QString& key = QString("default"));
    bool TCHoloprocessor_API restore_background(const QString& strPath, const QString& key = QString("default"), bool bRestoreReference = true);

    QString TCHoloprocessor_API get_sample_images_path(const QString& key = QString("default"));
    void TCHoloprocessor_API clear_sample_images(const QString& key = QString("default"));
    void TCHoloprocessor_API clear_flsample_images(const QString& key = QString("default"));
    bool TCHoloprocessor_API restore_sample_images(const QString& strPath, const QString& key = QString("default"));
    bool TCHoloprocessor_API restore_flsample_images(const QString& strPath, const QString& key = QString("default"));
};