#pragma once

#include <QString>

#include <H5Cpp.h>
#include <H5File.h>

#include "TCHoloprocessorExport.h"

class TCHoloprocessor_API DataArchive {
public:
    DataArchive();

    void setFilename(const QString& strName);

    unsigned int count(const QString& strGroup) const;

    bool store(const QString& strGroup, const QString& strName, const QString& strPath, const int retry = 3);
    bool restore(const QString& strGroup, const QString& strName, const QString& strPath, const int retry = 3);
    bool restoreAll(const QString& strGroup, const QString& strPath, int nWidth = 3, int nStart = 1, const int retry = 3);
    bool restore(const QString& strGroup, const int nIndex, const QString& strPath, const int retry = 3);

    bool setValue(const QString& strGroup, const QString& strName, const QVariant& value);
    QVariant getValue(const QString& strGroup, const QString& strName);

private:
    bool store_internal(const QString& strGroup, const QString& strName, const QString& strPath);
    bool restore_internal(const QString& strGroup, const QString& strName, const QString& strPath);
    bool restoreAll_internal(const QString& strGroup, const QString& strPath, int nWidth = 3, int nStart = 1);
    bool restore_internal(const QString& strGroup, const int nIndex, const QString& strPath);

protected:
    bool prepareGroups(H5::H5File& file, const QString& strGroup);

protected:
    QString m_strFilePath;
};