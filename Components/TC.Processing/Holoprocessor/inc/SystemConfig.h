#pragma once

#include <memory>

#include "TCHoloprocessorExport.h"

class TCHoloprocessor_API SystemConfig {
public:
    typedef SystemConfig Self;
    typedef std::shared_ptr<SystemConfig> Pointer;

public:
    static Pointer GetInstance(const QString& strRootPath = "", bool readOnly = false);
    ~SystemConfig();

    QString getConfigPath() const;
    QString getDataPath() const;
    QString getCalibrationPath() const;
    QString getAnnotationsPath() const;
    QString GetPresetsPath() const;
    QString getLogPath() const;
    QString getErrorLogPath() const;
    QString getTempPath() const;

    void updateLatestCalibration(const QString& strName, int nImageSize);
    bool getLatestCalibration(QString& strName);
    bool isValidCalibration(int nImageSize);		//! it checks only if the size of an image in calibration file is same as the current image

    bool getSimulation();
    QString getSimulatinImagePath();

    void setDataFolders(const QString& strDataRootPath);

    void setEngineeringMode(bool bMode);
    bool isEngineeringMode(void) const;

    void setSystemAnalysisMode(bool bMode);
    bool isSystemAnalysisMode(void) const;

    void setLegacyModel(bool bLegacy);
    bool isLegacyModel(void) const;

    bool useWideFieldCamera(void) const;

    void setUseSCMOSCamera(bool bUse);
    bool useSCMOSCamera(void) const;

    void setUseBrightFieldCamera(bool bUse);
    bool useBrightFieldCamera(void) const;

protected:
    SystemConfig(const QString& strRootPath, bool isReadyOnly = false, QObject* parent = 0);

    void setSystemFolders(const QString& strRootPath);
    void loadSimulation();
    void saveSimulation();

    void loadConfig(const QString& strPath, QMap<QString, double>& configs);

protected:
    struct Impl;
    std::unique_ptr<Impl> d;
};
