#include <catch2/catch.hpp>
#include <iostream>
#include <chrono>
#include <QString>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>

#include <thread>

#include "ArrayFireOperation.h"
#include "HoloProcessorArrayFire.h"
#include "HoloscopeUtility.h"
#include "SystemConfig.h"
#include "TomoBuilderUtility.h"

namespace HoloProcessorArrayFireTest {

    class DummyProcessorReporter : public ProgressReporter {
    public:
        DummyProcessorReporter()
            : ProgressReporter(){
        }

        void notify(int value) override {
        }

    };

    TEST_CASE("HoloProcessorArrayFire") {
        SECTION("Memory Leak") {
            for (auto i = 0; i < 5000; ++i) {
                std::cout << i;
                std::cout << " start ";
                auto start = std::chrono::system_clock::now();
                std::thread a(Fft2);
                a.join();
                //Fft2();
                auto end = std::chrono::system_clock::now();
                auto micro = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
                std::cout << micro.count();
                std::cout << " End" << std::endl;

                size_t abytes = 0, abuffs = 0, lbytes = 0, lbuffs = 0;
                af_err err = af_device_mem_info(&abytes, &abuffs, &lbytes, &lbuffs);
                if (err == AF_SUCCESS) {

                    printf("AF Memory at %s:%d: \n", __FILE__, __LINE__);
                    printf("Allocated [ Bytes | Buffers ] = [ %lld | %lld ]\n", abytes, abuffs);
                    printf("In Use    [ Bytes | Buffers ] = [ %lld | %lld ]\n", lbytes, lbuffs);
                } else {

                    fprintf(stderr, "AF Memory at %s:%d: \nAF Error %d\n",
                        __FILE__, __LINE__, err);
                }

                TC::release_fft_plans();
                af::deviceGC();
            }

            //int argc = 0;
            //char** argv = nullptr;

            //QCoreApplication coreApplication(argc, argv);

            //auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/Temp";
            //SystemConfig::Pointer sysConfig = SystemConfig::GetInstance(appDataPath);

            //const QString topPath = "E:/00_Data/20200708_HTFL/Crop";

            //const QDir dir(topPath);
            //auto dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

            //int i = 0;
            //for (const auto& folder : dirList) {
            //    std::cout << "start " << i;

            //    const auto strTopPath = QString("%1/%2").arg(topPath).arg(folder);
            //    const auto reprocessingFlag = true;

            //    TC::restore_background(strTopPath, QString("%1").arg(i), !reprocessingFlag);

            //    const auto strConfigPath = QString("%1/config.dat").arg(strTopPath);
            //    auto strDataPath =
            //        QString("%1/data3d/000000").arg(strTopPath);
            //    auto strBgPath = QString("%1/bgImages").arg(strTopPath);
            //    const auto strOutPath =
            //        QString("%1/3dRecon/%2").arg(strTopPath).arg(i);

            //    if (TC::restore_sample_images(strDataPath, QString("%1").arg(i))) {
            //        strDataPath = TC::get_sample_images_path(QString("%1").arg(i));
            //        strBgPath = TC::get_background_path(QString("%1").arg(i));
            //    }

            //    std::shared_ptr<DummyProcessorReporter> reporter(new DummyProcessorReporter());
            //    auto holoProcessorArrayFire =
            //        HoloProcessorArrayFire::create(reporter.get(), nullptr, 1);

            //    const auto processSuccess = holoProcessorArrayFire->procHologram3D(strConfigPath, strDataPath, strBgPath,
            //        strOutPath, reprocessingFlag);

            //    TC::clear_sample_images(QString("%1").arg(i));

            //    auto rawData = dynamic_cast<HoloProcessorArrayFire*>(holoProcessorArrayFire.get())->GetRawData();

            //    auto castedData = *rawData._Cast<std::shared_ptr<uint16_t>>();

            //    std::cout << castedData.get()[0] << " " << castedData.get()[1] << " ";

            //    std::cout << " End" << std::endl;
            //    //i++;
            //}


        }
    }
}
