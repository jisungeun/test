#include <catch2/catch.hpp>
#include <iostream>

#include "TomoBuilderUtility.h"

namespace TomoBuilderUtilityTest {
    TEST_CASE("TomoBuilderUtility") {
        SECTION("LoadParameter()") {
            const QString dataPath = "E:/00_Data/20200521_BFStitchingFail/20200521/20200521.133913.925.SG test-001";

            QMap<QString, double> infoMap;
            const QString configPath = QString("%1/config.dat").arg(dataPath);
            TC::LoadParameter(configPath, infoMap);

            for(auto iter = infoMap.begin();iter!=infoMap.end();++iter) {
                const auto QStringKey = iter.key();
                const auto doubleValue = iter.value();

                std::cout << QStringKey.toStdString() << std::endl;
                std::cout << doubleValue << std::endl;
                std::cout << "============" << std::endl;
            }
        }
    }
}
