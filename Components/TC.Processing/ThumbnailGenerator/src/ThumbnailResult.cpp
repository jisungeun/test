#include "ThumbnailResult.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailResult::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        std::shared_ptr<uint8_t[]> resultData{};
        int32_t sizeX{};
        int32_t sizeY{};
        bool colorFlag{ false };
    };

    ThumbnailResult::ThumbnailResult() : d(new Impl()) {
    }

    ThumbnailResult::ThumbnailResult(const ThumbnailResult& other) : d(new Impl(*other.d)) {
    }

    ThumbnailResult::~ThumbnailResult() = default;

    auto ThumbnailResult::operator=(const ThumbnailResult& other) -> ThumbnailResult& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto ThumbnailResult::SetResultData(const std::shared_ptr<uint8_t[]>& resultData) -> void {
        d->resultData = resultData;
    }

    auto ThumbnailResult::SetResultSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ThumbnailResult::SetColorFlag(const bool& colorFlag) -> void {
        d->colorFlag = colorFlag;
    }

    auto ThumbnailResult::GetResultData() const -> const std::shared_ptr<uint8_t[]>& {
        return d->resultData;
    }

    auto ThumbnailResult::GetResultSizeXY() const -> std::tuple<int32_t, int32_t> {
        return { d->sizeX, d->sizeY };
    }

    auto ThumbnailResult::GetColorFlag() const -> const bool& {
        return d->colorFlag;
    }
}
