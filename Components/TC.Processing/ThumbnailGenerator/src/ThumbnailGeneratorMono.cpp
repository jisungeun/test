#include "ThumbnailGeneratorMono.h"

#include "DataResizerSampling.h"
#include "IntensityMapper.h"
#include "MinMaxFinderBasic.h"
#include "Normalizer.h"
#include "ResizeCalculator.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailGeneratorMono::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        ThumbnailInputData thumbnailInputData{};
        IThumbnailOutput::Pointer output{};

        QList<uint8_t> intensityTable{};
    };

    ThumbnailGeneratorMono::ThumbnailGeneratorMono() : d(new Impl()) {
    }

    ThumbnailGeneratorMono::~ThumbnailGeneratorMono() = default;

    auto ThumbnailGeneratorMono::SetThumbnailInputData(const ThumbnailInputData& thumbnailInputData) -> void {
        d->thumbnailInputData = thumbnailInputData;
    }

    auto ThumbnailGeneratorMono::SetThumbnailOutput(const IThumbnailOutput::Pointer& output) -> void {
        d->output = output;
    }

    auto ThumbnailGeneratorMono::SetIntensityTable(const QList<uint8_t>& intensityTable) -> void {
        d->intensityTable = intensityTable;
    }

    auto ThumbnailGeneratorMono::Generate()-> bool {
        if (d->thumbnailInputData.IsDataGetterSet() == false) { return false; }
        if (d->output == nullptr) { return false; }

        const auto sizeX = d->thumbnailInputData.GetSizeX();
        const auto sizeY = d->thumbnailInputData.GetSizeY();
        const auto colorFlag = d->thumbnailInputData.GetColorFlag();

        if ((colorFlag == false) && d->intensityTable.isEmpty()) { return false; }
        if ((colorFlag == false) && (d->intensityTable.size() != 256)) { return false; }

        if (sizeX <= 0) { return false; }
        if (sizeY <= 0) { return false; }

        const auto inputData = d->thumbnailInputData.GetData();

        constexpr auto maxResizingX = 1024;
        constexpr auto maxResizingY = 1024;

        ResizeCalculator resizeCalculator;
        resizeCalculator.SetDataSize(sizeX, sizeY);
        resizeCalculator.SetMaximumResizeInfo(maxResizingX, maxResizingY);
        if (!resizeCalculator.Calculate()) { return false; }

        const auto resizingX = resizeCalculator.GetResizingX();
        const auto resizingY = resizeCalculator.GetResizingY();
        
        DataResizerSampling dataResizer;
        dataResizer.SetData(inputData, sizeX, sizeY, colorFlag);
        dataResizer.SetResizingInfo(resizingX, resizingY);
        if (!dataResizer.Resize()) { return false; }
        const auto resizedData = dataResizer.GetResizedData();

        if (colorFlag == true) {
            const auto numberOfResizedElements = resizingX * resizingY * 3;

            Normalizer normalizer;
            normalizer.SetData(resizedData, numberOfResizedElements);
            normalizer.SetNormalizationValues(255, 0);
            if (!normalizer.Normalize()) { return false; }
            const auto normalizedData = normalizer.GetNormalizedData();

            ThumbnailResult thumbnailResult;
            thumbnailResult.SetColorFlag(colorFlag);
            thumbnailResult.SetResultSize(resizingX, resizingY);
            thumbnailResult.SetResultData(normalizedData);

            d->output->SetThumbnailResult(thumbnailResult);
        } else {
            const auto numberOfResizedElements = resizingX * resizingY;

            MinMaxFinderBasic minMaxFinderBasic;
            minMaxFinderBasic.SetData(resizedData, numberOfResizedElements);
            if (!minMaxFinderBasic.Find()) { return false; }
            const auto minValue = minMaxFinderBasic.GetMinValue();
            const auto maxValue = minMaxFinderBasic.GetMaxValue();

            Normalizer normalizer;
            normalizer.SetData(resizedData, numberOfResizedElements);
            normalizer.SetNormalizationValues(maxValue, minValue);
            if (!normalizer.Normalize()) { return false; }
            const auto normalizedData = normalizer.GetNormalizedData();

            IntensityMapper intensityMapper;
            intensityMapper.SetData(normalizedData, numberOfResizedElements);
            intensityMapper.SetMappingTable(d->intensityTable);
            if (!intensityMapper.DoMapping()) { return false; }
            const auto thumbnailData = intensityMapper.GetResultData();

            ThumbnailResult thumbnailResult;
            thumbnailResult.SetColorFlag(colorFlag);
            thumbnailResult.SetResultSize(resizingX, resizingY);
            thumbnailResult.SetResultData(thumbnailData);

            d->output->SetThumbnailResult(thumbnailResult);
        }
        
        return true;
    }
}
