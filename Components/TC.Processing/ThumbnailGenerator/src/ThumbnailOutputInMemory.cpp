#include "ThumbnailOutputInMemory.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailOutputInMemory::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        ThumbnailResult result{};
    };

    ThumbnailOutputInMemory::ThumbnailOutputInMemory() : d(new Impl()) {
    }

    ThumbnailOutputInMemory::~ThumbnailOutputInMemory() = default;

    auto ThumbnailOutputInMemory::SetThumbnailResult(const ThumbnailResult& result) -> void {
        d->result = result;
    }

    auto ThumbnailOutputInMemory::GetResultData() const -> std::shared_ptr<uint8_t[]> {
        return d->result.GetResultData();
    }

    auto ThumbnailOutputInMemory::GetDataSizeX() const -> int32_t {
        return std::get<0>(d->result.GetResultSizeXY());
    }

    auto ThumbnailOutputInMemory::GetDataSizeY() const -> int32_t {
        return std::get<1>(d->result.GetResultSizeXY());
    }

    auto ThumbnailOutputInMemory::IsColor() const -> bool {
        return d->result.GetColorFlag();
    }
}
