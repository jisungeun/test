#include "ThumbnailBF.h"

#include "ThumbnailGeneratorMono.h"
#include "ThumbnailInputData.h"
#include "ThumbnailInputDataGetterInMemory.h"
#include "ThumbnailOutputInMemory.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailBF::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<uint8_t[]> data{};
        uint64_t sizeX{};
        uint64_t sizeY{};

        std::shared_ptr<uint8_t[]> resultData{};
        int32_t resultDataSizeX{};
        int32_t resultDataSizeY{};
    };

    ThumbnailBF::ThumbnailBF() : d(new Impl()) {
    }

    ThumbnailBF::~ThumbnailBF() = default;

    auto ThumbnailBF::SetBFData(const std::shared_ptr<uint8_t[]>& data) -> void {
        d->data = data;
    }

    auto ThumbnailBF::SetDataSize(const uint64_t& sizeX, const uint64_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ThumbnailBF::Generate() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->sizeX <= 0) { return false; }
        if (d->sizeY <= 0) { return false; }

        const auto numberOfElements = d->sizeX * d->sizeY * 3;

        std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
        for (uint64_t index = 0; index < numberOfElements; ++index) {
            convertedData.get()[index] = static_cast<float>(d->data.get()[index]);
        }

        auto thumbnailGetterPointer = new ThumbnailInputDataGetterInMemory;
        thumbnailGetterPointer->SetColorFlag(true);
        thumbnailGetterPointer->SetInputData(convertedData);
        thumbnailGetterPointer->SetSize(d->sizeX, d->sizeY);

        IThumbnailInputDataGetter::Pointer dataGetter{ thumbnailGetterPointer };

        ThumbnailInputData inputData;
        inputData.SetDataGetter(dataGetter);

        auto outputPointer = new ThumbnailOutputInMemory;
        IThumbnailOutput::Pointer output{ outputPointer };

        ThumbnailGeneratorMono thumbnailGeneratorMono;
        thumbnailGeneratorMono.SetThumbnailInputData(inputData);
        thumbnailGeneratorMono.SetThumbnailOutput(output);

        if (!thumbnailGeneratorMono.Generate()) { return false; }

        d->resultDataSizeX = outputPointer->GetDataSizeX();
        d->resultDataSizeY = outputPointer->GetDataSizeY();
        d->resultData = outputPointer->GetResultData();

        return true;
    }

    auto ThumbnailBF::GetThumbnailData() -> std::shared_ptr<uint8_t[]> {
        return d->resultData;
    }

    auto ThumbnailBF::GetThumbnailSizeX() -> int32_t {
        return d->resultDataSizeX;
    }

    auto ThumbnailBF::GetThumbnailSizeY() -> int32_t {
        return d->resultDataSizeY;
    }
}
