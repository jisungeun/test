#pragma once

#include <memory>
#include <QList>

namespace TC::Processing::ThumbnailGenerator {
    class IntensityMapper {
    public:
        IntensityMapper();
        ~IntensityMapper();

        auto SetData(const std::shared_ptr<uint8_t[]>& data, const uint64_t& numberOfElements)->void;
        auto SetMappingTable(const QList<uint8_t>& mappingTable)->void;

        auto DoMapping()->bool;

        auto GetResultData()const->std::shared_ptr<uint8_t[]>;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}