#pragma once

#include <memory>

namespace TC::Processing::ThumbnailGenerator {
    class Normalizer {
    public:
        Normalizer();
        ~Normalizer();

        auto SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements)->void;
        auto SetNormalizationValues(const float& topValue, const float& bottomValue)->void;

        auto Normalize()->bool;

        auto GetNormalizedData()const->std::shared_ptr<uint8_t[]>;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}