#include "MinMaxFinder3Sigma.h"

#include <cmath>
#include <algorithm>

namespace TC::Processing::ThumbnailGenerator {
    class MinMaxFinder3Sigma::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<float[]> data{};
        uint64_t numberOfElements{};

        float minValue{};
        float maxValue{};
    };

    MinMaxFinder3Sigma::MinMaxFinder3Sigma() : d(new Impl()) {
    }

    MinMaxFinder3Sigma::~MinMaxFinder3Sigma() = default;

    auto MinMaxFinder3Sigma::SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements) -> void {
        d->data = data;
        d->numberOfElements = numberOfElements;
    }

    auto MinMaxFinder3Sigma::Find() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->numberOfElements == 0) { return false; }

        float minValue = std::numeric_limits<float>::max();
        float maxValue = std::numeric_limits<float>::lowest();

        double sum{ 0 };
        double squareSum{ 0 };

        for (uint64_t index = 0; index < d->numberOfElements; ++index) {
            const auto value = d->data.get()[index];
            minValue = std::min(minValue, value);
            maxValue = std::max(maxValue, value);

            sum += static_cast<double>(value);
            squareSum += static_cast<double>(value * value);
        }

        const auto mean = sum / static_cast<double>(d->numberOfElements);
        const auto squareMean = squareSum / static_cast<double>(d->numberOfElements);

        const auto variance = squareMean - (mean * mean);
        const auto standardDeviation = std::sqrt(variance);

        d->minValue = std::max(minValue, static_cast<float>(mean - 3 * standardDeviation));
        d->maxValue = std::min(maxValue, static_cast<float>(mean + 3 * standardDeviation));
        
        return true;
    }

    auto MinMaxFinder3Sigma::GetMinValue() -> float {
        return d->minValue;
    }

    auto MinMaxFinder3Sigma::GetMaxValue() -> float {
        return d->maxValue;
    }
}
