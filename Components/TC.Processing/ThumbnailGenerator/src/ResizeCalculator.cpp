#include "ResizeCalculator.h"
#include <cmath>

namespace TC::Processing::ThumbnailGenerator {
    class ResizeCalculator::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        int32_t sizeX{};
        int32_t sizeY{};

        int32_t maxResizingX{};
        int32_t maxResizingY{};

        int32_t resizingX{};
        int32_t resizingY{};
    };

    ResizeCalculator::ResizeCalculator() : d(new Impl()) {
    }

    ResizeCalculator::~ResizeCalculator() = default;

    auto ResizeCalculator::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ResizeCalculator::SetMaximumResizeInfo(const int32_t& maxResizingX, const int32_t& maxResizingY) -> void {
        d->maxResizingX = maxResizingX;
        d->maxResizingY = maxResizingY;
    }

    auto ResizeCalculator::Calculate() -> bool {
        if (d->sizeX <= 0) { return false; }
        if (d->sizeY <= 0) { return false; }
        if (d->maxResizingX <= 0) { return false; }
        if (d->maxResizingY <= 0) { return false; }

        const auto resizingRatioX = static_cast<float>(d->sizeX) / static_cast<float>(d->maxResizingX);
        const auto resizingRatioY = static_cast<float>(d->sizeY) / static_cast<float>(d->maxResizingY);

        if (resizingRatioX <= 1 && resizingRatioY <= 1) {
            d->resizingX = d->sizeX;
            d->resizingY = d->sizeY;

            return true;
        }

        if (resizingRatioX > resizingRatioY) {
            d->resizingX = d->maxResizingX;
            d->resizingY = static_cast<int32_t>(std::round(static_cast<float>(d->sizeY) / resizingRatioX));
        } else {
            d->resizingX = static_cast<int32_t>(std::round(static_cast<float>(d->sizeX) / resizingRatioY));
            d->resizingY = d->maxResizingY;
        }

        return true;
    }

    auto ResizeCalculator::GetResizingX() const -> int32_t {
        return d->resizingX;
    }

    auto ResizeCalculator::GetResizingY() const -> int32_t {
        return d->resizingY;
    }
}
