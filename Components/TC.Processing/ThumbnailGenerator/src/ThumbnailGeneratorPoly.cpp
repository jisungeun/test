#include "ThumbnailGeneratorPoly.h"

#include "DataResizerSampling.h"
#include "ImageMerger.h"
#include "IntensityMapper.h"
#include "MinMaxFinderBasic.h"
#include "Normalizer.h"
#include "ResizeCalculator.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailGeneratorPoly::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QList<std::tuple<ThumbnailInputData, RGB>> thumbnailInputDataList{};
        IThumbnailOutput::Pointer output{};

        QList<uint8_t> intensityTable{};
    };

    ThumbnailGeneratorPoly::ThumbnailGeneratorPoly() : d(new Impl()) {
    }

    ThumbnailGeneratorPoly::~ThumbnailGeneratorPoly() = default;

    auto ThumbnailGeneratorPoly::SetThumbnailInputDataList(
        const QList<std::tuple<ThumbnailInputData, RGB>>& thumbnailInputDataList) -> void {
        d->thumbnailInputDataList = thumbnailInputDataList;
    }

    auto ThumbnailGeneratorPoly::SetThumbnailOutput(const IThumbnailOutput::Pointer& output) -> void {
        d->output = output;
    }

    auto ThumbnailGeneratorPoly::SetIntensityTable(const QList<uint8_t>& intensityTable) -> void {
        d->intensityTable = intensityTable;
    }

    auto ThumbnailGeneratorPoly::Generate() -> bool {
        if (d->thumbnailInputDataList.isEmpty()) { return false; }
        if (d->output == nullptr) { return false; }
        if (d->intensityTable.isEmpty()) { return false; }
        if (d->intensityTable.size() != 256) { return false; }

        int32_t inputDataSizeX{};
        int32_t inputDataSizeY{};

        for (auto index = 0; index < d->thumbnailInputDataList.size(); ++index) {
            const auto thumbnailInputData = d->thumbnailInputDataList.at(index);

            const auto inputData = std::get<0>(thumbnailInputData);

            const auto sizeX = inputData.GetSizeX();
            const auto sizeY = inputData.GetSizeY();
            const auto colorFlag = inputData.GetColorFlag();

            if (sizeX <= 0) { return false; }
            if (sizeY <= 0) { return false; }
            if (colorFlag == true) { return false; }
            if (inputData.IsDataGetterSet() == false) { return false; }

            if (index == 0) {
                inputDataSizeX = sizeX;
                inputDataSizeY = sizeY;
            }

            if (sizeX != inputDataSizeX) { return false; }
            if (sizeY != inputDataSizeY) { return false; }
        }

        constexpr auto maxResizingX = 1024;
        constexpr auto maxResizingY = 1024;

        ResizeCalculator resizeCalculator;
        resizeCalculator.SetDataSize(inputDataSizeX, inputDataSizeY);
        resizeCalculator.SetMaximumResizeInfo(maxResizingX, maxResizingY);
        if (!resizeCalculator.Calculate()) { return false; }

        const auto resizingX = resizeCalculator.GetResizingX();
        const auto resizingY = resizeCalculator.GetResizingY();
        const auto numberOfResizedElements = resizingX * resizingY;

        QList<std::tuple<std::shared_ptr<uint8_t[]>, RGB>> thumbnailDataList;
        for (const auto& thumbnailInputData : d->thumbnailInputDataList) {
            const auto inputData = std::get<0>(thumbnailInputData);
            const auto rgb = std::get<1>(thumbnailInputData);

            DataResizerSampling dataResizer;
            dataResizer.SetData(inputData.GetData(), inputDataSizeX, inputDataSizeY, false);
            dataResizer.SetResizingInfo(resizingX, resizingY);
            if (!dataResizer.Resize()) { return false; }
            const auto resizedData = dataResizer.GetResizedData();

            MinMaxFinderBasic minMaxFinderBasic;
            minMaxFinderBasic.SetData(resizedData, numberOfResizedElements);
            if (!minMaxFinderBasic.Find()) { return false; }
            const auto minValue = minMaxFinderBasic.GetMinValue();
            const auto maxValue = minMaxFinderBasic.GetMaxValue();

            Normalizer normalizer;
            normalizer.SetData(resizedData, numberOfResizedElements);
            normalizer.SetNormalizationValues(maxValue, minValue);
            if (!normalizer.Normalize()) { return false; }
            const auto normalizedData = normalizer.GetNormalizedData();

            IntensityMapper intensityMapper;
            intensityMapper.SetData(normalizedData, numberOfResizedElements);
            intensityMapper.SetMappingTable(d->intensityTable);
            if (!intensityMapper.DoMapping()) { return false; }
            const auto thumbnailData = intensityMapper.GetResultData();

            thumbnailDataList.push_back({ thumbnailData, rgb });
        }

        QList<std::tuple<ImageMerger::ImageData, ImageMerger::RGB>> mergingDataList;
        for (const auto& thumbnailData : thumbnailDataList) {
            const auto data = std::get<0>(thumbnailData);
            const auto rgb = std::get<1>(thumbnailData);

            mergingDataList.push_back({ data,{rgb.r,rgb.g,rgb.b} });
        }

        ImageMerger imageMerger;
        imageMerger.SetDataList(mergingDataList, numberOfResizedElements);
        if (!imageMerger.Merge()) { return false; }

        const auto thumbnailResultData = imageMerger.GetMergedImageData();

        ThumbnailResult thumbnailResult;
        thumbnailResult.SetColorFlag(true);
        thumbnailResult.SetResultSize(resizingX, resizingY);
        thumbnailResult.SetResultData(thumbnailResultData);

        d->output->SetThumbnailResult(thumbnailResult);
        
        return true;
    }
}
