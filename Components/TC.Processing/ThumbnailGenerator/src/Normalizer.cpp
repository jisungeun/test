#include "Normalizer.h"
#include <cmath>

namespace TC::Processing::ThumbnailGenerator {
    class Normalizer::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<float[]> data{};
        uint64_t numberOfElements{};

        float topValue{};
        float bottomValue{};

        std::shared_ptr<uint8_t[]> normalizedData{};
    };

    Normalizer::Normalizer() : d(new Impl()) {
    }

    Normalizer::~Normalizer() = default;

    auto Normalizer::SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements) -> void {
        d->data = data;
        d->numberOfElements = numberOfElements;
    }

    auto Normalizer::SetNormalizationValues(const float& topValue, const float& bottomValue) -> void {
        d->topValue = topValue;
        d->bottomValue = bottomValue;
    }

    auto Normalizer::Normalize() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->numberOfElements == 0) { return false; }
        if (d->topValue <= d->bottomValue) { return false; }

        std::shared_ptr<uint8_t[]> normalizedData{new uint8_t[d->numberOfElements]};

        for (uint64_t index = 0; index < d->numberOfElements; ++index) {
            const auto value = d->data.get()[index];
            if (value >= d->topValue) {
                normalizedData.get()[index] = 255;
            } else if (value <= d->bottomValue) {
                normalizedData.get()[index] = 0;
            } else {
                normalizedData.get()[index] = static_cast<uint8_t>(
                    std::round((value - d->bottomValue) / (d->topValue - d->bottomValue) * 255));
            }
        }

        d->normalizedData = normalizedData;

        return true;
    }

    auto Normalizer::GetNormalizedData() const -> std::shared_ptr<uint8_t[]> {
        return d->normalizedData;
    }
}
