#pragma once
#include <memory>

namespace TC::Processing::ThumbnailGenerator {
    class ResizeCalculator {
    public:
        ResizeCalculator();
        ~ResizeCalculator();

        auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto SetMaximumResizeInfo(const int32_t& maxResizingX, const int32_t& maxResizingY)->void;

        auto Calculate()->bool;

        auto GetResizingX()const->int32_t;
        auto GetResizingY()const->int32_t;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
