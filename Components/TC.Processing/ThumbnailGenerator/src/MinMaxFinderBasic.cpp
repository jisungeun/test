#include "MinMaxFinderBasic.h"
#include <algorithm>

namespace TC::Processing::ThumbnailGenerator {
    class MinMaxFinderBasic::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<float[]> data{};
        uint64_t numberOfElements{};

        float minValue{};
        float maxValue{};
    };
    MinMaxFinderBasic::MinMaxFinderBasic() : d(new Impl()) {
    }

    MinMaxFinderBasic::~MinMaxFinderBasic() = default;

    auto MinMaxFinderBasic::SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements) -> void {
        d->data = data;
        d->numberOfElements = numberOfElements;
    }

    auto MinMaxFinderBasic::Find() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->numberOfElements == 0) { return false; }

        float minValue = std::numeric_limits<float>::max();
        float maxValue = std::numeric_limits<float>::lowest();

        for (uint64_t index = 0; index < d->numberOfElements; ++index) {
            const auto value = d->data.get()[index];
            minValue = std::min(minValue, value);
            maxValue = std::max(maxValue, value);
        }

        d->minValue = minValue;
        d->maxValue = maxValue;

        return true;
    }

    auto MinMaxFinderBasic::GetMinValue() -> float {
        return d->minValue;
    }

    auto MinMaxFinderBasic::GetMaxValue() -> float {
        return d->maxValue;
    }
}
