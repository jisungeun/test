#include "ThumbnailFL.h"

#include "ThumbnailGeneratorPoly.h"
#include "ThumbnailInputDataGetterInMemory.h"
#include "ThumbnailOutputInMemory.h"

namespace TC::Processing::ThumbnailGenerator {
    struct RGB {
        uint8_t r{};
        uint8_t g{};
        uint8_t b{};
    };

    class ThumbnailFL::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<uint16_t[]> ch0Data{};
        std::shared_ptr<uint16_t[]> ch1Data{};
        std::shared_ptr<uint16_t[]> ch2Data{};

        RGB ch0Color{};
        RGB ch1Color{};
        RGB ch2Color{};

        bool ch0DataIsSet{ false };
        bool ch1DataIsSet{ false };
        bool ch2DataIsSet{ false };

        int32_t sizeX{};
        int32_t sizeY{};

        std::shared_ptr<uint8_t[]> resultData{};
        int32_t resultDataSizeX{};
        int32_t resultDataSizeY{};
    };

    ThumbnailFL::ThumbnailFL() : d(new Impl()) {
    }

    ThumbnailFL::~ThumbnailFL() = default;

    auto ThumbnailFL::SetFLCH0Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g,
        const uint8_t& b) -> void {
        d->ch0Data = data;
        d->ch0Color = RGB{ r,g,b };
        d->ch0DataIsSet = true;
    }

    auto ThumbnailFL::SetFLCH1Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g,
        const uint8_t& b) -> void {
        d->ch1Data = data;
        d->ch1Color = RGB{ r,g,b };
        d->ch1DataIsSet = true;
    }

    auto ThumbnailFL::SetFLCH2Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g,
        const uint8_t& b) -> void {
        d->ch2Data = data;
        d->ch2Color = RGB{ r,g,b };
        d->ch2DataIsSet = true;
    }

    auto ThumbnailFL::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ThumbnailFL::Generate() -> bool {
        if (!(d->ch0DataIsSet || d->ch1DataIsSet || d->ch2DataIsSet)) { return false; }
        if (d->sizeX <= 0) { return false; }
        if (d->sizeY <= 0) { return false; }

        const auto numberOfElements = static_cast<uint64_t>(d->sizeX * d->sizeY);
        
        QList<std::tuple<ThumbnailInputData, IThumbnailPolyInput::RGB>> inputDataList;

        if (d->ch0DataIsSet) {
            std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
            for (uint64_t index = 0; index < numberOfElements; ++index) {
                convertedData.get()[index] = static_cast<float>(d->ch0Data.get()[index]);
            }

            auto thumbnailGetterPointer = new ThumbnailInputDataGetterInMemory;
            thumbnailGetterPointer->SetColorFlag(false);
            thumbnailGetterPointer->SetInputData(convertedData);
            thumbnailGetterPointer->SetSize(d->sizeX, d->sizeY);

            IThumbnailInputDataGetter::Pointer dataGetter{ thumbnailGetterPointer };

            ThumbnailInputData inputData;
            inputData.SetDataGetter(dataGetter);

            inputDataList.push_back({ inputData,{d->ch0Color.r,d->ch0Color.g,d->ch0Color.b} });
        }

        if (d->ch1DataIsSet) {
            std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
            for (uint64_t index = 0; index < numberOfElements; ++index) {
                convertedData.get()[index] = static_cast<float>(d->ch1Data.get()[index]);
            }

            auto thumbnailGetterPointer = new ThumbnailInputDataGetterInMemory;
            thumbnailGetterPointer->SetColorFlag(false);
            thumbnailGetterPointer->SetInputData(convertedData);
            thumbnailGetterPointer->SetSize(d->sizeX, d->sizeY);

            IThumbnailInputDataGetter::Pointer dataGetter{ thumbnailGetterPointer };

            ThumbnailInputData inputData;
            inputData.SetDataGetter(dataGetter);

            inputDataList.push_back({ inputData,{d->ch1Color.r,d->ch1Color.g,d->ch1Color.b} });
        }

        if (d->ch2DataIsSet) {
            std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
            for (uint64_t index = 0; index < numberOfElements; ++index) {
                convertedData.get()[index] = static_cast<float>(d->ch2Data.get()[index]);
            }

            auto thumbnailGetterPointer = new ThumbnailInputDataGetterInMemory;
            thumbnailGetterPointer->SetColorFlag(false);
            thumbnailGetterPointer->SetInputData(convertedData);
            thumbnailGetterPointer->SetSize(d->sizeX, d->sizeY);

            IThumbnailInputDataGetter::Pointer dataGetter{ thumbnailGetterPointer };

            ThumbnailInputData inputData;
            inputData.SetDataGetter(dataGetter);

            inputDataList.push_back({ inputData,{d->ch2Color.r,d->ch2Color.g,d->ch2Color.b} });
        }

        QList<uint8_t> intensityTable;
        for (auto index = 0; index < 256; ++index) {
            intensityTable.push_back(index);
        }

        auto outputPointer = new ThumbnailOutputInMemory;
        IThumbnailOutput::Pointer output{ outputPointer };

        ThumbnailGeneratorPoly thumbnailGeneratorPoly;
        thumbnailGeneratorPoly.SetThumbnailInputDataList(inputDataList);
        thumbnailGeneratorPoly.SetIntensityTable(intensityTable);
        thumbnailGeneratorPoly.SetThumbnailOutput(output);

        if (!thumbnailGeneratorPoly.Generate()) { return false; }

        d->resultDataSizeX = outputPointer->GetDataSizeX();
        d->resultDataSizeY = outputPointer->GetDataSizeY();
        d->resultData = outputPointer->GetResultData();
        
        return true;
    }

    auto ThumbnailFL::GetThumbnailData() -> std::shared_ptr<uint8_t[]> {
        return d->resultData;
    }

    auto ThumbnailFL::GetThumbnailSizeX() -> int32_t {
        return d->resultDataSizeX;
    }

    auto ThumbnailFL::GetThumbnailSizeY() -> int32_t {
        return d->resultDataSizeY;
    }
}
