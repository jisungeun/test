#pragma once

#include <memory>
#include <QList>

namespace TC::Processing::ThumbnailGenerator {
    class ImageMerger {
    public:
        using ImageData = std::shared_ptr<uint8_t[]>;
        using RGB = std::tuple<uint8_t, uint8_t, uint8_t>;

        ImageMerger();
        ~ImageMerger();

        auto SetDataList(const QList<std::tuple<ImageData, RGB>>& dataList, const uint64_t& numberOfElements)->void;

        auto Merge()->bool;

        auto GetMergedImageData()->ImageData;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}