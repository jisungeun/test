#include "ThumbnailInputDataGetterInMemory.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailInputDataGetterInMemory::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<float[]> data{};
        int32_t sizeX{};
        int32_t sizeY{};
        bool colorFlag{ false };

    };

    ThumbnailInputDataGetterInMemory::ThumbnailInputDataGetterInMemory() : d(new Impl()) {
    }

    ThumbnailInputDataGetterInMemory::~ThumbnailInputDataGetterInMemory() = default;

    auto ThumbnailInputDataGetterInMemory::SetInputData(const std::shared_ptr<float[]>& data) -> void {
        d->data = data;
    }

    auto ThumbnailInputDataGetterInMemory::SetSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ThumbnailInputDataGetterInMemory::SetColorFlag(const bool& colorFlag) -> void {
        d->colorFlag = colorFlag;
    }

    auto ThumbnailInputDataGetterInMemory::GetInputData() -> std::shared_ptr<float[]> {
        return d->data;
    }

    auto ThumbnailInputDataGetterInMemory::GetSizeXY() -> std::tuple<int32_t, int32_t> {
        return { d->sizeX, d->sizeY };
    }

    auto ThumbnailInputDataGetterInMemory::GetColorFlag() -> bool {
        return d->colorFlag;
    }
}
