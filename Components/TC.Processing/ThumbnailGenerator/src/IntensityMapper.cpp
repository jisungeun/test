#include "IntensityMapper.h"

namespace TC::Processing::ThumbnailGenerator {
    class IntensityMapper::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<uint8_t[]> data{};
        uint64_t numberOfElements{};
        QList<uint8_t> mappingTable{};

        std::shared_ptr<uint8_t[]> resultData{};
    };

    IntensityMapper::IntensityMapper() : d(new Impl()) {
    }

    IntensityMapper::~IntensityMapper() = default;

    auto IntensityMapper::SetData(const std::shared_ptr<uint8_t[]>& data, const uint64_t& numberOfElements) -> void {
        d->data = data;
        d->numberOfElements = numberOfElements;
    }
    
    auto IntensityMapper::SetMappingTable(const QList<uint8_t>& mappingTable) -> void {
        d->mappingTable = mappingTable;
    }

    auto IntensityMapper::DoMapping() -> bool {
        if (d->data == nullptr) { return false;}
        if (d->numberOfElements == 0) { return false; }
        if (d->mappingTable.isEmpty()) { return false; }
        if (d->mappingTable.size() != 256) { return false; }

        std::shared_ptr<uint8_t[]> resultData{ new uint8_t[d->numberOfElements] };

        for (uint64_t index = 0; index < d->numberOfElements; ++index) {
            resultData.get()[index] = d->mappingTable.at(d->data.get()[index]);
        }

        d->resultData = resultData;

        return true;
    }

    auto IntensityMapper::GetResultData() const -> std::shared_ptr<uint8_t[]> {
        return d->resultData;
    }
}
