#include "ImageMerger.h"
#include <cmath>

namespace TC::Processing::ThumbnailGenerator {
    class ImageMerger::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QList<std::tuple<ImageData, RGB>> imageDataList{};
        uint64_t numberOfElements{};

        ImageData mergedImageData{};
    };

    ImageMerger::ImageMerger() : d(new Impl()) {
    }

    ImageMerger::~ImageMerger() = default;

    auto ImageMerger::SetDataList(const QList<std::tuple<ImageData, RGB>>& dataList, const uint64_t& numberOfElements)
        -> void {
        d->imageDataList = dataList;
        d->numberOfElements = numberOfElements;
    }

    auto ImageMerger::Merge() -> bool {
        if (d->imageDataList.isEmpty()) { return false; }
        if (d->numberOfElements == 0) { return false; }

        std::shared_ptr<float[]> rgbAddedData{ new float[3 * d->numberOfElements]() };
        for (const auto& imageData : d->imageDataList) {
            const auto imageDataPointer = std::get<0>(imageData);
            const auto [r, g, b] = std::get<1>(imageData);

            for (uint64_t index = 0; index < d->numberOfElements; ++index) {
                const auto rValue = static_cast<float>(r) * static_cast<float>(imageDataPointer.get()[index]);
                const auto gValue = static_cast<float>(g) * static_cast<float>(imageDataPointer.get()[index]);
                const auto bValue = static_cast<float>(b) * static_cast<float>(imageDataPointer.get()[index]);

                const auto rIndex = index + 0 * d->numberOfElements;
                const auto gIndex = index + 1 * d->numberOfElements;
                const auto bIndex = index + 2 * d->numberOfElements;

                rgbAddedData.get()[rIndex] = rgbAddedData.get()[rIndex] + rValue;
                rgbAddedData.get()[gIndex] = rgbAddedData.get()[gIndex] + gValue;
                rgbAddedData.get()[bIndex] = rgbAddedData.get()[bIndex] + bValue;
            }
        }

        ImageData mergedImageData{ new uint8_t[3 * d->numberOfElements] };
        for (uint64_t index = 0; index < 3 * d->numberOfElements; ++index) {
            mergedImageData.get()[index] = std::min<uint8_t>(255, static_cast<uint8_t>(std::round(rgbAddedData.get()[index] / 255)));
        }

        d->mergedImageData = mergedImageData;

        return true;
    }

    auto ImageMerger::GetMergedImageData() -> ImageData {
        return d->mergedImageData;
    }
}
