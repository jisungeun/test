#include "DataResizerSampling.h"
#include <cmath>

namespace TC::Processing::ThumbnailGenerator {
    class DataResizerSampling::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<float[]> data{};
        uint64_t sizeX{};
        uint64_t sizeY{};
        bool colorFlag{ false };

        uint64_t resizingX{};
        uint64_t resizingY{};

        std::shared_ptr<float[]> resizedData{};
    };

    DataResizerSampling::DataResizerSampling() : d(new Impl()) {
    }

    DataResizerSampling::~DataResizerSampling() = default;

    auto DataResizerSampling::SetData(const std::shared_ptr<float[]>& data, const int32_t& sizeX, const int32_t& sizeY,
        const bool& colorFlag) -> void {
        d->data = data;
        d->sizeX = static_cast<uint64_t>(sizeX);
        d->sizeY = static_cast<uint64_t>(sizeY);
        d->colorFlag = colorFlag;
    }

    auto DataResizerSampling::SetResizingInfo(const int32_t& resizingX, const int32_t& resizingY) -> void {
        d->resizingX = static_cast<uint64_t>(resizingX);
        d->resizingY = static_cast<uint64_t>(resizingY);
    }

    auto DataResizerSampling::Resize() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->sizeX <= 0) { return false; }
        if (d->sizeY <= 0) { return false; }
        if (d->resizingX <= 0) { return false; }
        if (d->resizingY <= 0) { return false; }
        if (d->resizingX > d->sizeX) { return false; }
        if (d->resizingY > d->sizeY) { return false; }

        const auto stepSizeX = static_cast<double>(d->sizeX) / static_cast<double>(d->resizingX);
        const auto stepSizeY = static_cast<double>(d->sizeY) / static_cast<double>(d->resizingY);

        const auto sizeZ = static_cast<uint64_t>(d->colorFlag ? (3) : (1));

        std::shared_ptr<float[]> resizedData{ new float[d->resizingX * d->resizingY * sizeZ] };

        for (uint64_t indexZ = 0; indexZ < sizeZ; ++indexZ) {
            for (uint64_t indexX = 0; indexX < d->resizingX; ++indexX) {
                for (uint64_t indexY = 0; indexY < d->resizingY; ++indexY) {
                    const auto dataIndex = static_cast<uint64_t>(std::round(static_cast<double>(indexY) * stepSizeY))
                    + static_cast<uint64_t>(std::round(static_cast<double>(indexX) * stepSizeX)) * d->sizeY
                    + indexZ * d->sizeX * d->sizeY;
                    const auto resizedDataIndex = indexY + indexX * d->resizingY + indexZ * d->resizingX * d->resizingY;

                    resizedData.get()[resizedDataIndex] = d->data.get()[dataIndex];
                }
            }
        }

        d->resizedData = resizedData;

        return true;
    }

    auto DataResizerSampling::GetResizedData() const -> std::shared_ptr<float[]> {
        return d->resizedData;
    }
}
