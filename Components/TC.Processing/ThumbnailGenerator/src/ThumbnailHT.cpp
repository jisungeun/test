#include "ThumbnailHT.h"

#include "ThumbnailInputDataGetterInMemory.h"
#include "ThumbnailOutputInMemory.h"
#include "ThumbnailGeneratorMono.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailHT::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        std::shared_ptr<uint16_t[]> data{};
        int32_t sizeX{};
        int32_t sizeY{};

        std::shared_ptr<uint8_t[]> resultData{};
        int32_t resultDataSizeX{};
        int32_t resultDataSizeY{};
    };

    ThumbnailHT::ThumbnailHT() : d(new Impl()) {
    }

    ThumbnailHT::~ThumbnailHT() = default;

    auto ThumbnailHT::SetHTData(const std::shared_ptr<uint16_t[]>& data) -> void {
        d->data = data;
    }

    auto ThumbnailHT::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto ThumbnailHT::Generate() -> bool {
        if (d->data == nullptr) { return false; }
        if (d->sizeX <= 0) { return false; }
        if (d->sizeY <= 0) { return false; }

        const auto numberOfElements = static_cast<uint64_t>(d->sizeX * d->sizeY);

        std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
        for (uint64_t index = 0; index < numberOfElements; ++index) {
            convertedData.get()[index] = static_cast<float>(d->data.get()[index]);
        }
        
        auto thumbnailGetterPointer = new ThumbnailInputDataGetterInMemory;
        thumbnailGetterPointer->SetColorFlag(false);
        thumbnailGetterPointer->SetInputData(convertedData);
        thumbnailGetterPointer->SetSize(d->sizeX, d->sizeY);

        IThumbnailInputDataGetter::Pointer dataGetter{ thumbnailGetterPointer };

        ThumbnailInputData inputData;
        inputData.SetDataGetter(dataGetter);

        QList<uint8_t> intensityTable;
        for (auto index = 0; index < 256; ++index) {
            intensityTable.push_back(index);
        }

        auto outputPointer = new ThumbnailOutputInMemory;
        IThumbnailOutput::Pointer output{ outputPointer };
        
        ThumbnailGeneratorMono thumbnailGeneratorMono;
        thumbnailGeneratorMono.SetThumbnailInputData(inputData);
        thumbnailGeneratorMono.SetIntensityTable(intensityTable);
        thumbnailGeneratorMono.SetThumbnailOutput(output);

        if (!thumbnailGeneratorMono.Generate()) { return false; }

        d->resultDataSizeX = outputPointer->GetDataSizeX();
        d->resultDataSizeY = outputPointer->GetDataSizeY();
        d->resultData = outputPointer->GetResultData();

        return true;
    }

    auto ThumbnailHT::GetThumbnailData() -> std::shared_ptr<uint8_t[]> {
        return d->resultData;
    }

    auto ThumbnailHT::GetThumbnailSizeX() -> int32_t {
        return d->resultDataSizeX;
    }

    auto ThumbnailHT::GetThumbnailSizeY() -> int32_t {
        return d->resultDataSizeY;
    }
}
