#include "ThumbnailInputData.h"

namespace TC::Processing::ThumbnailGenerator {
    class ThumbnailInputData::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IThumbnailInputDataGetter::Pointer dataGetter{};
    };

    ThumbnailInputData::ThumbnailInputData() : d(new Impl()) {
    }

    ThumbnailInputData::ThumbnailInputData(const ThumbnailInputData& other) : d(new Impl(*other.d)) {
    }

    ThumbnailInputData::~ThumbnailInputData() = default;

    auto ThumbnailInputData::operator=(const ThumbnailInputData& other) -> ThumbnailInputData& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto ThumbnailInputData::SetDataGetter(const IThumbnailInputDataGetter::Pointer& getter) -> void {
        d->dataGetter = getter;
    }

    auto ThumbnailInputData::IsDataGetterSet() const -> bool {
        return d->dataGetter != nullptr;
    }

    auto ThumbnailInputData::GetData() const -> std::shared_ptr<float[]> {
        return d->dataGetter->GetInputData();
    }

    auto ThumbnailInputData::GetSizeX() const -> int32_t {
        return std::get<0>(d->dataGetter->GetSizeXY());
    }

    auto ThumbnailInputData::GetSizeY() const -> int32_t {
        return std::get<1>(d->dataGetter->GetSizeXY());
    }

    auto ThumbnailInputData::GetColorFlag() const -> bool {
        return d->dataGetter->GetColorFlag();
    }
}
