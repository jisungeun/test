#include <catch2/catch.hpp>

#include "ThumbnailFL.h"

namespace ThumbnailFLTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailFL : unit test") {
        SECTION("ThumbnailFL()") {
            ThumbnailFL thumbnailFl;
            CHECK(&thumbnailFl != nullptr);
        }
        SECTION("SetFLCH0Data()") {
            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH0Data({}, 0, 0, 0);
            CHECK(&thumbnailFl != nullptr);
        }
        SECTION("SetFLCH1Data()") {
            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH1Data({}, 0, 0, 0);
            CHECK(&thumbnailFl != nullptr);
        }
        SECTION("SetFLCH2Data()") {
            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH2Data({}, 0, 0, 0);
            CHECK(&thumbnailFl != nullptr);
        }
        SECTION("SetDataSize()") {
            ThumbnailFL thumbnailFl;
            thumbnailFl.SetDataSize(1, 2);
            CHECK(&thumbnailFl != nullptr);
        }
        SECTION("Generate()") {
            ThumbnailFL thumbnailFl;
            SECTION("Without data") {
                CHECK(thumbnailFl.Generate() == false);
            }
            SECTION("With data") {
                constexpr auto sizeX = 3;
                constexpr auto sizeY = 1;
                constexpr auto numberOfElements = sizeX * sizeY;

                std::shared_ptr<uint16_t[]> ch0Data{ new uint16_t[numberOfElements]() };
                std::shared_ptr<uint16_t[]> ch1Data{ new uint16_t[numberOfElements]() };
                std::shared_ptr<uint16_t[]> ch2Data{ new uint16_t[numberOfElements]() };

                ch0Data.get()[0] = 2;
                ch0Data.get()[1] = 10;
                ch0Data.get()[2] = 20;

                ch1Data.get()[0] = 5;
                ch1Data.get()[1] = 9;
                ch1Data.get()[2] = 10;

                ch2Data.get()[0] = 20;
                ch2Data.get()[1] = 21;
                ch2Data.get()[2] = 30;
                
                thumbnailFl.SetFLCH0Data(ch0Data, 32, 64, 128);
                thumbnailFl.SetFLCH0Data(ch0Data, 128, 32, 64);
                thumbnailFl.SetFLCH0Data(ch0Data, 64, 128, 32);

                thumbnailFl.SetDataSize(sizeX, sizeY);

                CHECK(thumbnailFl.Generate() == true);
            }
        }
        SECTION("GetThumbnailData()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 1;
            constexpr auto numberOfElements = sizeX * sizeY;

            std::shared_ptr<uint16_t[]> ch0Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch1Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch2Data{ new uint16_t[numberOfElements]() };

            ch0Data.get()[0] = 2;
            ch0Data.get()[1] = 10;
            ch0Data.get()[2] = 20;

            ch1Data.get()[0] = 5;
            ch1Data.get()[1] = 9;
            ch1Data.get()[2] = 10;

            ch2Data.get()[0] = 20;
            ch2Data.get()[1] = 21;
            ch2Data.get()[2] = 30;

            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH0Data(ch0Data, 32, 64, 128);
            thumbnailFl.SetFLCH1Data(ch1Data, 128, 32, 64);
            thumbnailFl.SetFLCH2Data(ch2Data, 64, 128, 32);

            thumbnailFl.SetDataSize(sizeX, sizeY);
            thumbnailFl.Generate();

            const auto thumbnailData = thumbnailFl.GetThumbnailData();
            CHECK(thumbnailData.get()[0] == 0);
            CHECK(thumbnailData.get()[1] == 123);
            CHECK(thumbnailData.get()[2] == 224);
            CHECK(thumbnailData.get()[3] == 0);
            CHECK(thumbnailData.get()[4] == 67);
            CHECK(thumbnailData.get()[5] == 224);
            CHECK(thumbnailData.get()[6] == 0);
            CHECK(thumbnailData.get()[7] == 111);
            CHECK(thumbnailData.get()[8] == 224);
        }
        SECTION("GetThumbnailSizeX()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 1;
            constexpr auto numberOfElements = sizeX * sizeY;

            std::shared_ptr<uint16_t[]> ch0Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch1Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch2Data{ new uint16_t[numberOfElements]() };

            ch0Data.get()[0] = 2;
            ch0Data.get()[1] = 10;
            ch0Data.get()[2] = 20;

            ch1Data.get()[0] = 5;
            ch1Data.get()[1] = 9;
            ch1Data.get()[2] = 10;

            ch2Data.get()[0] = 20;
            ch2Data.get()[1] = 21;
            ch2Data.get()[2] = 30;

            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH0Data(ch0Data, 32, 64, 128);
            thumbnailFl.SetFLCH0Data(ch0Data, 128, 32, 64);
            thumbnailFl.SetFLCH0Data(ch0Data, 64, 128, 32);

            thumbnailFl.SetDataSize(sizeX, sizeY);
            thumbnailFl.Generate();

            CHECK(thumbnailFl.GetThumbnailSizeX() == 3);
        }
        SECTION("GetThumbnailSizeY()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 1;
            constexpr auto numberOfElements = sizeX * sizeY;

            std::shared_ptr<uint16_t[]> ch0Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch1Data{ new uint16_t[numberOfElements]() };
            std::shared_ptr<uint16_t[]> ch2Data{ new uint16_t[numberOfElements]() };

            ch0Data.get()[0] = 2;
            ch0Data.get()[1] = 10;
            ch0Data.get()[2] = 20;

            ch1Data.get()[0] = 5;
            ch1Data.get()[1] = 9;
            ch1Data.get()[2] = 10;

            ch2Data.get()[0] = 20;
            ch2Data.get()[1] = 21;
            ch2Data.get()[2] = 30;

            ThumbnailFL thumbnailFl;
            thumbnailFl.SetFLCH0Data(ch0Data, 32, 64, 128);
            thumbnailFl.SetFLCH0Data(ch0Data, 128, 32, 64);
            thumbnailFl.SetFLCH0Data(ch0Data, 64, 128, 32);

            thumbnailFl.SetDataSize(sizeX, sizeY);
            thumbnailFl.Generate();

            CHECK(thumbnailFl.GetThumbnailSizeY() == 1);
        }
    }
    TEST_CASE("ThumbnailFL : practical test") {
        
    }
}