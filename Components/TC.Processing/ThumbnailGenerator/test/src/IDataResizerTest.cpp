#include <catch2/catch.hpp>

#include "IDataResizer.h"

namespace IDataResizerTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class DataResizerForTest final : public IDataResizer {
    public:
        DataResizerForTest() = default;
        ~DataResizerForTest() = default;
        auto SetData(const std::shared_ptr<float[]>& data, const int32_t& sizeX, const int32_t& sizeY,
            const bool& colorFlag) -> void override {
            this->setDataTriggered = true;
        }
        auto SetResizingInfo(const int32_t& resizingX, const int32_t& resizingY) -> void override {
            this->setResizingInfoTriggered = true;
        }
        auto Resize() -> bool override {
            this->resizeTriggered = true;
            return false;
        }
        auto GetResizedData() const -> std::shared_ptr<float[]> override {
            this->getResizedDataTriggered = true;
            return nullptr;
        }

        bool setDataTriggered{ false };
        bool setResizingInfoTriggered{ false };
        bool resizeTriggered{ false };
        mutable bool getResizedDataTriggered{ false };
    };

    TEST_CASE("IDataResizer : unit test") {
        SECTION("SetData()") {
            DataResizerForTest iDataResizer;
            CHECK(iDataResizer.setDataTriggered == false);
            iDataResizer.SetData({}, 0, 0, false);
            CHECK(iDataResizer.setDataTriggered == true);
        }
        SECTION("SetResizingInfo()") {
            DataResizerForTest iDataResizer;
            CHECK(iDataResizer.setResizingInfoTriggered == false);
            iDataResizer.SetResizingInfo(0, 0);
            CHECK(iDataResizer.setResizingInfoTriggered == true);
        }
        SECTION("Resize()") {
            DataResizerForTest iDataResizer;
            CHECK(iDataResizer.resizeTriggered == false);
            iDataResizer.Resize();
            CHECK(iDataResizer.resizeTriggered == true);
        }
        SECTION("GetResizedData()") {
            DataResizerForTest iDataResizer;
            CHECK(iDataResizer.getResizedDataTriggered == false);
            iDataResizer.GetResizedData();
            CHECK(iDataResizer.getResizedDataTriggered == true);
        }
    }
}