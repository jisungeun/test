#include <catch2/catch.hpp>

#include "ThumbnailBF.h"

namespace ThumbnailBFTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailBF : unit test") {
        SECTION("ThumbnailBF()") {
            ThumbnailBF thumbnailBF;
            CHECK(&thumbnailBF != nullptr);
        }
        SECTION("SetBFData()") {
            ThumbnailBF thumbnailBF;
            thumbnailBF.SetBFData({});
            CHECK(&thumbnailBF != nullptr);
        }
        SECTION("SetDataSize()") {
            ThumbnailBF thumbnailBF;
            thumbnailBF.SetDataSize(1, 2);
            CHECK(&thumbnailBF != nullptr);
        }
        SECTION("Generate()") {
            ThumbnailBF thumbnailBF;
            SECTION("Without data") {
                CHECK(thumbnailBF.Generate() == false);
            }
            SECTION("With data") {
                constexpr auto sizeX = 1;
                constexpr auto sizeY = 2;
                constexpr auto numberOfElements = sizeX * sizeY * 3;

                std::shared_ptr<uint8_t[]> bfData{ new uint8_t[numberOfElements] };
                bfData.get()[0] = 125;
                bfData.get()[1] = 84;
                bfData.get()[2] = 170;
                bfData.get()[3] = 130;
                bfData.get()[4] = 255;
                bfData.get()[5] = 220;

                thumbnailBF.SetBFData(bfData);
                thumbnailBF.SetDataSize(sizeX, sizeY);

                CHECK(thumbnailBF.Generate() == true);
            }
        }
        SECTION("GetThumbnailData()") {
            constexpr auto sizeX = 1;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> bfData{ new uint8_t[numberOfElements] };
            bfData.get()[0] = 125;
            bfData.get()[1] = 84;
            bfData.get()[2] = 170;
            bfData.get()[3] = 130;
            bfData.get()[4] = 255;
            bfData.get()[5] = 220;

            ThumbnailBF thumbnailBF;
            thumbnailBF.SetBFData(bfData);
            thumbnailBF.SetDataSize(sizeX, sizeY);

            thumbnailBF.Generate();

            const auto thumbnailData = thumbnailBF.GetThumbnailData();
            CHECK(thumbnailData.get()[0] == 125);
            CHECK(thumbnailData.get()[1] == 84);
            CHECK(thumbnailData.get()[2] == 170);
            CHECK(thumbnailData.get()[3] == 130);
            CHECK(thumbnailData.get()[4] == 255);
            CHECK(thumbnailData.get()[5] == 220);
        }
        SECTION("GetThumbnailSizeX()") {
            constexpr auto sizeX = 1;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> bfData{ new uint8_t[numberOfElements] };
            bfData.get()[0] = 125;
            bfData.get()[1] = 84;
            bfData.get()[2] = 170;
            bfData.get()[3] = 130;
            bfData.get()[4] = 255;
            bfData.get()[5] = 220;

            ThumbnailBF thumbnailBF;
            thumbnailBF.SetBFData(bfData);
            thumbnailBF.SetDataSize(sizeX, sizeY);

            thumbnailBF.Generate();

            CHECK(thumbnailBF.GetThumbnailSizeX() == 1);
        }
        SECTION("GetThumbnailSizeY()") {
            constexpr auto sizeX = 1;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> bfData{ new uint8_t[numberOfElements] };
            bfData.get()[0] = 125;
            bfData.get()[1] = 84;
            bfData.get()[2] = 170;
            bfData.get()[3] = 130;
            bfData.get()[4] = 255;
            bfData.get()[5] = 220;

            ThumbnailBF thumbnailBF;
            thumbnailBF.SetBFData(bfData);
            thumbnailBF.SetDataSize(sizeX, sizeY);

            thumbnailBF.Generate();

            CHECK(thumbnailBF.GetThumbnailSizeY() == 2);
        }
    }
    TEST_CASE("ThumbnailBF : practical test") {
        //TODO : Implement test
    }
}