#include <catch2/catch.hpp>

#include "MinMaxFinder3Sigma.h"

namespace MinMaxFinder3SigmaTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("MinMaxFinder3Sigma : unit test") {
        SECTION("MinMaxFinder3Sigma()") {
            MinMaxFinder3Sigma minMaxFinder3Sigma;
            CHECK(&minMaxFinder3Sigma != nullptr);
        }
        SECTION("SetData()") {
            MinMaxFinder3Sigma minMaxFinder3Sigma;
            minMaxFinder3Sigma.SetData({}, 0);
            CHECK(&minMaxFinder3Sigma != nullptr);
        }
        SECTION("Find()") {
            MinMaxFinder3Sigma minMaxFinder3Sigma;

            SECTION("Without data") {
                CHECK(minMaxFinder3Sigma.Find() == false);
            }

            SECTION("With data") {
                constexpr auto numberOfElements = 10;
                std::shared_ptr<float[]> data{ new float[numberOfElements]() };

                constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
                std::copy_n(rawData, numberOfElements, data.get());

                minMaxFinder3Sigma.SetData(data, numberOfElements);
                CHECK(minMaxFinder3Sigma.Find() == true);
            }
        }
        SECTION("GetMinValue()") {
            constexpr auto numberOfElements = 10;
            std::shared_ptr<float[]> data{ new float[numberOfElements]() };

            constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
            std::copy_n(rawData, numberOfElements, data.get());

            MinMaxFinder3Sigma minMaxFinder3Sigma;
            minMaxFinder3Sigma.SetData(data, numberOfElements);
            minMaxFinder3Sigma.Find();

            CHECK(minMaxFinder3Sigma.GetMinValue() == 2);
        }
        SECTION("GetMaxValue()") {
            constexpr auto numberOfElements = 10;
            std::shared_ptr<float[]> data{ new float[numberOfElements]() };

            constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
            std::copy_n(rawData, numberOfElements, data.get());

            MinMaxFinder3Sigma minMaxFinder3Sigma;
            minMaxFinder3Sigma.SetData(data, numberOfElements);
            minMaxFinder3Sigma.Find();

            CHECK(minMaxFinder3Sigma.GetMaxValue() == 21);
        }
    }
    TEST_CASE("MinMaxFinder3Sigma : practical test") {
        //TODO Implement test
    }
}