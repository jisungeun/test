#include <catch2/catch.hpp>

#include "IThumbnailOutput.h"

namespace IThumbnailOutputTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailOutputForTest final : public IThumbnailOutput {
    public:
        ThumbnailOutputForTest() = default;
        ~ThumbnailOutputForTest() = default;

        auto SetThumbnailResult(const ThumbnailResult& result) -> void override {
            this->setThumbnailResultIsTriggered = true;
        }

        bool setThumbnailResultIsTriggered{ false };
    };

    TEST_CASE("IThumbnailOutput : unit test") {
        SECTION("SetThumbnailResult()") {
            ThumbnailOutputForTest iThumbnailOutput;
            CHECK(iThumbnailOutput.setThumbnailResultIsTriggered == false);
            iThumbnailOutput.SetThumbnailResult({});
            CHECK(iThumbnailOutput.setThumbnailResultIsTriggered == true);
        }
    }
}
