#include <catch2/catch.hpp>

#include "IMinMaxFinder.h"

namespace IMinMaxFinderTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class MinMaxFinderForTest final : public IMinMaxFinder {
    public:
        MinMaxFinderForTest() = default;
        ~MinMaxFinderForTest() = default;

        auto Find() -> bool override {
            this->findTriggered = true;
            return false;
        }
        auto GetMinValue() -> float override {
            this->getMinValueTriggered = true;
            return 0;
        }
        auto GetMaxValue() -> float override {
            this->getMaxValueTriggered = true;
            return 0;
        }

        bool findTriggered{ false };
        bool getMinValueTriggered{ false };
        bool getMaxValueTriggered{ false };
    };

    TEST_CASE("IMinMaxFinder : unit test") {
        SECTION("Find()") {
            MinMaxFinderForTest iMinMaxFinder;
            CHECK(iMinMaxFinder.findTriggered == false);
            iMinMaxFinder.Find();
            CHECK(iMinMaxFinder.findTriggered == true);
        }
        SECTION("GetMinValue()") {
            MinMaxFinderForTest iMinMaxFinder;
            CHECK(iMinMaxFinder.getMinValueTriggered == false);
            iMinMaxFinder.GetMinValue();
            CHECK(iMinMaxFinder.getMinValueTriggered == true);
        }
        SECTION("GetMaxValue()") {
            MinMaxFinderForTest iMinMaxFinder;
            CHECK(iMinMaxFinder.getMaxValueTriggered == false);
            iMinMaxFinder.GetMaxValue();
            CHECK(iMinMaxFinder.getMaxValueTriggered == true);
        }
    }
}