#include <catch2/catch.hpp>

#include "ThumbnailInputData.h"

namespace ThumbnailInputDataTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailInputDataGetterForTest final : public IThumbnailInputDataGetter {
    public:
        ThumbnailInputDataGetterForTest() = default;
        ~ThumbnailInputDataGetterForTest() = default;

        auto GetInputData() -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> inputData{ new float[1] };
            inputData.get()[0] = 1;
            return inputData;
        }
        auto GetSizeXY() -> std::tuple<int32_t, int32_t> override {
            return { 1,2 };
        }
        auto GetColorFlag() -> bool override {
            return true;
        }
    };

    TEST_CASE("ThumbnailInputData : unit test") {
        SECTION("ThumbnailInputData()") {
            ThumbnailInputData thumbnailInputData;
            CHECK(&thumbnailInputData != nullptr);
        }
        SECTION("ThumbnailInputData(other)") {
            const IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData srcThumbnailInputData;
            srcThumbnailInputData.SetDataGetter(dataGetter);

            ThumbnailInputData destThumbnailInputData(srcThumbnailInputData);
            CHECK(destThumbnailInputData.GetSizeX() == 1);
        }
        SECTION("operator=()") {
            const IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData srcThumbnailInputData;
            srcThumbnailInputData.SetDataGetter(dataGetter);

            ThumbnailInputData destThumbnailInputData;
            destThumbnailInputData = srcThumbnailInputData;
            CHECK(destThumbnailInputData.GetSizeX() == 1);
        }
        SECTION("SetDataGetter()") {
            ThumbnailInputData thumbnailInputData;
            thumbnailInputData.SetDataGetter({});

            CHECK(&thumbnailInputData != nullptr);
        }
        SECTION("IsDataGetter()") {
            ThumbnailInputData thumbnailInputData;
            SECTION("without setting getter") {
                CHECK(thumbnailInputData.IsDataGetterSet() == false);
            }
            SECTION("with setting getter") {
                const IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };
                thumbnailInputData.SetDataGetter(dataGetter);
                CHECK(thumbnailInputData.IsDataGetterSet() == true);
            }
        }
        SECTION("GetData()") {
            IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData thumbnailInputData;
            thumbnailInputData.SetDataGetter(dataGetter);

            CHECK(thumbnailInputData.GetData().get()[0] == 1);
        }
        SECTION("GetSizeX()") {
            IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData thumbnailInputData;
            thumbnailInputData.SetDataGetter(dataGetter);

            CHECK(thumbnailInputData.GetSizeX() == 1);
        }
        SECTION("GetSizeY()") {
            IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData thumbnailInputData;
            thumbnailInputData.SetDataGetter(dataGetter);

            CHECK(thumbnailInputData.GetSizeY() == 2);
        }
        SECTION("GetColorFlag()") {
            IThumbnailInputDataGetter::Pointer dataGetter{ new ThumbnailInputDataGetterForTest };

            ThumbnailInputData thumbnailInputData;
            thumbnailInputData.SetDataGetter(dataGetter);

            CHECK(thumbnailInputData.GetColorFlag() == true);
        }
    }

    TEST_CASE("ThumbnailInputData : practical test") {
        //TODO Implement
    }
}