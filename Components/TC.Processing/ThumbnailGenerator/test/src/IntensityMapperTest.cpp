#include <catch2/catch.hpp>

#include "IntensityMapper.h"

namespace IntensityMapperTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("IntensityMapper : unit test") {
        SECTION("IntensityMapper()") {
            IntensityMapper mapper;
            CHECK(&mapper != nullptr);
        }
        SECTION("SetData()") {
            IntensityMapper mapper;
            mapper.SetData({}, 0);
            CHECK(&mapper != nullptr);
        }
        SECTION("SetMappingTable()") {
            IntensityMapper mapper;
            mapper.SetMappingTable({});
            CHECK(&mapper != nullptr);
        }
        SECTION("DoMapping()") {
            IntensityMapper mapper;
            SECTION("Without data") {
                CHECK(mapper.DoMapping() == false);
            }
            SECTION("With data") {
                constexpr auto numberOfElements = 10;

                std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
                constexpr uint8_t rawData[numberOfElements] = { 0,1,2,3,4,5,6,7,8,9 };
                std::copy_n(rawData, numberOfElements, data.get());

                QList<uint8_t> mappingTable;
                for (auto index = 0; index < 256; ++index) {
                    mappingTable.push_back(255 - index);
                }

                mapper.SetData(data, numberOfElements);
                mapper.SetMappingTable(mappingTable);

                CHECK(mapper.DoMapping() == true);
            }
        }
        SECTION("GetResultData()") {
            constexpr auto numberOfElements = 10;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
            constexpr uint8_t rawData[numberOfElements] = { 0,1,2,3,4,5,6,7,8,9 };
            std::copy_n(rawData, numberOfElements, data.get());

            QList<uint8_t> mappingTable;
            for (auto index = 0; index < 256; ++index) {
                mappingTable.push_back(255 - index);
            }

            IntensityMapper mapper;
            mapper.SetData(data, numberOfElements);
            mapper.SetMappingTable(mappingTable);

            mapper.DoMapping();

            const auto resultData = mapper.GetResultData();

            CHECK(resultData.get()[0] == 255);
            CHECK(resultData.get()[1] == 254);
            CHECK(resultData.get()[2] == 253);
            CHECK(resultData.get()[3] == 252);
            CHECK(resultData.get()[4] == 251);
            CHECK(resultData.get()[5] == 250);
            CHECK(resultData.get()[6] == 249);
            CHECK(resultData.get()[7] == 248);
            CHECK(resultData.get()[8] == 247);
            CHECK(resultData.get()[9] == 246);
        }
    }
    TEST_CASE("IntensityMapper : practical test") {
        //TODO Implement test
    }
}