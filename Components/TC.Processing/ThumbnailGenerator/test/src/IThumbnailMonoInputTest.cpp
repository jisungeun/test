#include <catch2/catch.hpp>

#include "IThumbnailMonoInput.h"

namespace IThumbnailMonoInputTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailMonoInputForTest final : public IThumbnailMonoInput {
    public:
        ThumbnailMonoInputForTest() = default;
        ~ThumbnailMonoInputForTest() = default;

        auto SetThumbnailInputData(const ThumbnailInputData& thumbnailInputData) -> void override {
            this->setThumbnailInputDataTriggered = true;
        }

        bool setThumbnailInputDataTriggered{ false };
    };

    TEST_CASE("IThumbnailMonoInput : unit test") {
        SECTION("SetThumbnailInputData()") {
            ThumbnailMonoInputForTest iThumbnailMonoInput;
            CHECK(iThumbnailMonoInput.setThumbnailInputDataTriggered == false);
            iThumbnailMonoInput.SetThumbnailInputData({});
            CHECK(iThumbnailMonoInput.setThumbnailInputDataTriggered == true);
        }
    }
}