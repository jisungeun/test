#include <catch2/catch.hpp>

#include "DataResizerSampling.h"

namespace DataResizerSamplingTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("DataResizerSampling : unit test") {
        SECTION("DataResizerSampling()") {
            DataResizerSampling dataResizerSampling;
            CHECK(&dataResizerSampling != nullptr);
        }
        SECTION("SetData()") {
            DataResizerSampling dataResizerSampling;
            dataResizerSampling.SetData({}, 0, 0, false);
            CHECK(&dataResizerSampling != nullptr);
        }
        SECTION("SetResizingInfo()") {
            DataResizerSampling dataResizerSampling;
            dataResizerSampling.SetResizingInfo(0, 0);
            CHECK(&dataResizerSampling != nullptr);
        }
        SECTION("Resize()") {
            DataResizerSampling dataResizerSampling;

            SECTION("without setting data") {
                CHECK(dataResizerSampling.Resize() == false);
            }

            SECTION("with setting data") {
                constexpr auto sizeX = 3;
                constexpr auto sizeY = 7;
                constexpr auto colorFlag = false;

                std::shared_ptr<float[]> data{ new float[sizeX * sizeY] };

                dataResizerSampling.SetData(data, sizeX, sizeY, colorFlag);

                constexpr auto resizingX = 2;
                constexpr auto resizingY = 3;

                dataResizerSampling.SetResizingInfo(resizingX, resizingY);

                CHECK(dataResizerSampling.Resize() == true);
            }
        }
        SECTION("GetResizedData()") {
            DataResizerSampling dataResizerSampling;
            SECTION("non-color") {
                constexpr auto sizeX = 3;
                constexpr auto sizeY = 7;
                constexpr auto colorFlag = false;

                std::shared_ptr<float[]> data{ new float[sizeX * sizeY] };
                const float rawData[sizeX * sizeY] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 };

                std::copy_n(rawData, sizeX * sizeY, data.get());

                dataResizerSampling.SetData(data, sizeX, sizeY, colorFlag);

                constexpr auto resizingX = 2;
                constexpr auto resizingY = 3;

                dataResizerSampling.SetResizingInfo(resizingX, resizingY);

                dataResizerSampling.Resize();

                const auto resizedData = dataResizerSampling.GetResizedData();
                CHECK(resizedData.get()[0] == 0);
                CHECK(resizedData.get()[1] == 2);
                CHECK(resizedData.get()[2] == 5);
                CHECK(resizedData.get()[3] == 14);
                CHECK(resizedData.get()[4] == 16);
                CHECK(resizedData.get()[5] == 19);
            }

            SECTION("color") {
                constexpr auto sizeX = 5;
                constexpr auto sizeY = 3;
                constexpr auto colorFlag = true;

                std::shared_ptr<float[]> data{ new float[sizeX * sizeY * 3] };
                const float rawData[sizeX * sizeY * 3] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
                    22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44 };

                std::copy_n(rawData, sizeX * sizeY * 3, data.get());

                dataResizerSampling.SetData(data, sizeX, sizeY, colorFlag);

                constexpr auto resizingX = 2;
                constexpr auto resizingY = 2;

                dataResizerSampling.SetResizingInfo(resizingX, resizingY);

                dataResizerSampling.Resize();

                const auto resizedData = dataResizerSampling.GetResizedData();
                CHECK(resizedData.get()[0] == 0);
                CHECK(resizedData.get()[1] == 2);
                CHECK(resizedData.get()[2] == 9);
                CHECK(resizedData.get()[3] == 11);
                CHECK(resizedData.get()[4] == 15);
                CHECK(resizedData.get()[5] == 17);
                CHECK(resizedData.get()[6] == 24);
                CHECK(resizedData.get()[7] == 26);
                CHECK(resizedData.get()[8] == 30);
                CHECK(resizedData.get()[9] == 32);
                CHECK(resizedData.get()[10] == 39);
                CHECK(resizedData.get()[11] == 41);
            }
        }
    }

    TEST_CASE("DataResizerSampling : practical test") {
        //TODO Implement test
    }
}