#include <catch2/catch.hpp>

#include "MinMaxFinderBasic.h"

namespace MinMaxFinderBasicTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("MinMaxFinderBasic : unit test") {
        SECTION("MinMaxFinderBasic()") {
            MinMaxFinderBasic minMaxFinderBasic;
            CHECK(&minMaxFinderBasic != nullptr);
        }
        SECTION("SetData()") {
            MinMaxFinderBasic minMaxFinderBasic;
            minMaxFinderBasic.SetData({}, 0);
            CHECK(&minMaxFinderBasic != nullptr);
        }
        SECTION("Find()") {
            MinMaxFinderBasic minMaxFinderBasic;
            
            SECTION("Without data") {
                CHECK(minMaxFinderBasic.Find() == false);
            }

            SECTION("With data") {
                constexpr auto numberOfElements = 10;
                std::shared_ptr<float[]> data{ new float[numberOfElements]() };

                constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
                std::copy_n(rawData, numberOfElements, data.get());

                minMaxFinderBasic.SetData(data, numberOfElements);
                CHECK(minMaxFinderBasic.Find() == true);
            }
        }
        SECTION("GetMinValue()") {
            constexpr auto numberOfElements = 10;
            std::shared_ptr<float[]> data{ new float[numberOfElements]() };

            constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
            std::copy_n(rawData, numberOfElements, data.get());

            MinMaxFinderBasic minMaxFinderBasic;
            minMaxFinderBasic.SetData(data, numberOfElements);
            minMaxFinderBasic.Find();

            CHECK(minMaxFinderBasic.GetMinValue() == 2);
        }
        SECTION("GetMaxValue()") {
            constexpr auto numberOfElements = 10;
            std::shared_ptr<float[]> data{ new float[numberOfElements]() };

            constexpr float rawData[numberOfElements] = { 2,4,5,6,7,11,13,17,19,21 };
            std::copy_n(rawData, numberOfElements, data.get());

            MinMaxFinderBasic minMaxFinderBasic;
            minMaxFinderBasic.SetData(data, numberOfElements);
            minMaxFinderBasic.Find();

            CHECK(minMaxFinderBasic.GetMaxValue() == 21);
        }
    }
    TEST_CASE("MinMaxFinderBasic : practical test") {
        //TODO Implement test
    }
}