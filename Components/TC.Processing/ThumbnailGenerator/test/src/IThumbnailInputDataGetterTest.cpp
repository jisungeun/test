#include <catch2/catch.hpp>

#include "IThumbnailInputDataGetter.h"

namespace IThumbnailInputDataGetterTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailInputDataGetterForTest final : public IThumbnailInputDataGetter {
    public:
        ThumbnailInputDataGetterForTest() = default;
        ~ThumbnailInputDataGetterForTest() = default;

        auto GetInputData() -> std::shared_ptr<float[]> override {
            getInputDataTriggered = true;
            return nullptr;
        }

        auto GetSizeXY() -> std::tuple<int32_t, int32_t> override {
            getSizeXYTriggered = true;
            return { 0,0 };
        }

        auto GetColorFlag() -> bool override {
            getColorFlagTriggered = true;
            return false;
        }

        bool getInputDataTriggered{ false };
        bool getSizeXYTriggered{ false };
        bool getColorFlagTriggered{ false };
    };

    TEST_CASE("IThumbnailInputDataGetter : unit test") {
        SECTION("GetInputData()") {
            ThumbnailInputDataGetterForTest thumbnailInputDataGetter;
            CHECK(thumbnailInputDataGetter.getInputDataTriggered == false);
            thumbnailInputDataGetter.GetInputData();
            CHECK(thumbnailInputDataGetter.getInputDataTriggered == true);
        }
        SECTION("GetSizeXY()") {
            ThumbnailInputDataGetterForTest thumbnailInputDataGetter;
            CHECK(thumbnailInputDataGetter.getSizeXYTriggered == false);
            thumbnailInputDataGetter.GetSizeXY();
            CHECK(thumbnailInputDataGetter.getSizeXYTriggered == true);
        }
        SECTION("GetColorFlag()") {
            ThumbnailInputDataGetterForTest thumbnailInputDataGetter;
            CHECK(thumbnailInputDataGetter.getColorFlagTriggered == false);
            thumbnailInputDataGetter.GetColorFlag();
            CHECK(thumbnailInputDataGetter.getColorFlagTriggered == true);
        }
    }
}