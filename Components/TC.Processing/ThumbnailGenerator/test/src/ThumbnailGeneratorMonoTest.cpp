#include <catch2/catch.hpp>

#include "ThumbnailGeneratorMono.h"

namespace ThumbnailGeneratorMonoTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailInputDataGetterForTest final : public IThumbnailInputDataGetter {
    public:
        ThumbnailInputDataGetterForTest() = default;
        ~ThumbnailInputDataGetterForTest() = default;

        auto SetInfo(const std::shared_ptr<float[]>& data, const int32_t& sizeX, const int32_t& sizeY, const bool& colorFlag)->void {
            this->data = data;
            this->sizeX = sizeX;
            this->sizeY = sizeY;
            this->colorFlag = colorFlag;
        }
        auto GetInputData() -> std::shared_ptr<float[]> override {
            return this->data;
        }
        auto GetSizeXY() -> std::tuple<int32_t, int32_t> override {
            return { this->sizeX, this->sizeY };
        }
        auto GetColorFlag() -> bool override {
            return this->colorFlag;
        }

    private:
        std::shared_ptr<float[]> data{};
        int32_t sizeX{};
        int32_t sizeY{};
        bool colorFlag{ false };
    };

    class ThumbnailOutputForTest final : public IThumbnailOutput {
    public:
        ThumbnailOutputForTest() = default;
        ~ThumbnailOutputForTest() = default;

        auto SetThumbnailResult(const ThumbnailResult& result) -> void override {
            this->result = result;
        }

        auto GetResult()const->ThumbnailResult {
            return this->result;
        }

    private:
        ThumbnailResult result{};
    };

    TEST_CASE("ThumbnailGeneratorMono : unit test") {
        SECTION("ThumbnailGeneratorMono()") {
            ThumbnailGeneratorMono thumbnailGeneratorMono;
            CHECK(&thumbnailGeneratorMono != nullptr);
        }
        SECTION("SetThumbnailInputData()") {
            ThumbnailGeneratorMono thumbnailGeneratorMono;
            thumbnailGeneratorMono.SetThumbnailInputData({});
            CHECK(&thumbnailGeneratorMono != nullptr);
        }
        SECTION("SetThumbnailOutput()") {
            ThumbnailGeneratorMono thumbnailGeneratorMono;
            thumbnailGeneratorMono.SetThumbnailOutput({});
            CHECK(&thumbnailGeneratorMono != nullptr);
        }
        SECTION("SetIntensityTable") {
            ThumbnailGeneratorMono thumbnailGeneratorMono;
            thumbnailGeneratorMono.SetThumbnailOutput({});
            CHECK(&thumbnailGeneratorMono != nullptr);
        }
        SECTION("Generate()") {
            ThumbnailGeneratorMono thumbnailGeneratorMono;

            SECTION("Without data") {
                CHECK(thumbnailGeneratorMono.Generate() == false);
            }

            SECTION("With data") {
                SECTION("black & white data") {
                    constexpr auto sizeX = 3;
                    constexpr auto sizeY = 4;
                    constexpr bool colorFlag = false;
                    constexpr auto numberOfElements = sizeX * sizeY;

                    std::shared_ptr<float[]> data{ new float[numberOfElements] };
                    constexpr float rawData[numberOfElements] = { 0,2,4,6,8,10,12,14,16,18,20,22 };
                    std::copy_n(rawData, numberOfElements, data.get());

                    auto inputDataGetter = new ThumbnailInputDataGetterForTest;
                    inputDataGetter->SetInfo(data, sizeX, sizeY, colorFlag);

                    IThumbnailInputDataGetter::Pointer dataGetter{ inputDataGetter };

                    ThumbnailInputData thumbnailInputData;
                    thumbnailInputData.SetDataGetter(dataGetter);

                    auto output = new ThumbnailOutputForTest;
                    IThumbnailOutput::Pointer outputPointer{ output };

                    QList<uint8_t> intensityTable;
                    for (auto index = 0; index < 256; ++index) {
                        intensityTable.push_back(static_cast<uint8_t>(index));
                    }

                    thumbnailGeneratorMono.SetThumbnailInputData(thumbnailInputData);
                    thumbnailGeneratorMono.SetThumbnailOutput(outputPointer);
                    thumbnailGeneratorMono.SetIntensityTable(intensityTable);

                    CHECK(thumbnailGeneratorMono.Generate() == true);

                    const auto thumbnailResult = output->GetResult();
                    CHECK(thumbnailResult.GetColorFlag() == colorFlag);
                    CHECK(std::get<0>(thumbnailResult.GetResultSizeXY()) == sizeX);
                    CHECK(std::get<1>(thumbnailResult.GetResultSizeXY()) == sizeY);

                    const auto resultData = thumbnailResult.GetResultData();
                    CHECK(resultData.get()[0] == 0);
                    CHECK(resultData.get()[1] == 23);
                    CHECK(resultData.get()[2] == 46);
                    CHECK(resultData.get()[3] == 70);
                    CHECK(resultData.get()[4] == 93);
                    CHECK(resultData.get()[5] == 116);
                    CHECK(resultData.get()[6] == 139);
                    CHECK(resultData.get()[7] == 162);
                    CHECK(resultData.get()[8] == 185);
                    CHECK(resultData.get()[9] == 209);
                    CHECK(resultData.get()[10] == 232);
                    CHECK(resultData.get()[11] == 255);
                }
                SECTION("Color data") {
                    constexpr auto sizeX = 1;
                    constexpr auto sizeY = 4;
                    constexpr bool colorFlag = true;
                    constexpr auto numberOfElements = sizeX * sizeY * 3;

                    std::shared_ptr<float[]> data{ new float[numberOfElements] };
                    constexpr float rawData[numberOfElements] = { 0,2,4,6,8,10,12,14,16,18,20,22 };
                    std::copy_n(rawData, numberOfElements, data.get());

                    auto inputDataGetter = new ThumbnailInputDataGetterForTest;
                    inputDataGetter->SetInfo(data, sizeX, sizeY, colorFlag);

                    IThumbnailInputDataGetter::Pointer dataGetter{ inputDataGetter };

                    ThumbnailInputData thumbnailInputData;
                    thumbnailInputData.SetDataGetter(dataGetter);

                    auto output = new ThumbnailOutputForTest;
                    IThumbnailOutput::Pointer outputPointer{ output };

                    thumbnailGeneratorMono.SetThumbnailInputData(thumbnailInputData);
                    thumbnailGeneratorMono.SetThumbnailOutput(outputPointer);

                    CHECK(thumbnailGeneratorMono.Generate() == true);

                    const auto thumbnailResult = output->GetResult();
                    CHECK(thumbnailResult.GetColorFlag() == colorFlag);
                    CHECK(std::get<0>(thumbnailResult.GetResultSizeXY()) == sizeX);
                    CHECK(std::get<1>(thumbnailResult.GetResultSizeXY()) == sizeY);

                    const auto resultData = thumbnailResult.GetResultData();
                    CHECK(resultData.get()[0] == 0);
                    CHECK(resultData.get()[1] == 2);
                    CHECK(resultData.get()[2] == 4);
                    CHECK(resultData.get()[3] == 6);
                    CHECK(resultData.get()[4] == 8);
                    CHECK(resultData.get()[5] == 10);
                    CHECK(resultData.get()[6] == 12);
                    CHECK(resultData.get()[7] == 14);
                    CHECK(resultData.get()[8] == 16);
                    CHECK(resultData.get()[9] == 18);
                    CHECK(resultData.get()[10] == 20);
                    CHECK(resultData.get()[11] == 22);
                }

            }
        }
    }
    TEST_CASE("ThumbnailGeneratorMono : practical test") {
        //TODO : Implement test
    }
}
