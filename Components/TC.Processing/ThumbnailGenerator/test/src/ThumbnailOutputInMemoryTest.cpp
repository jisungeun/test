#include <catch2/catch.hpp>

#include "ThumbnailOutputInMemory.h"

namespace ThumbnailOutputInMemoryTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailOutputInMemory : unit test") {
        SECTION("ThumbnailOutputInMemory()") {
            ThumbnailOutputInMemory thumbnailOutputInMemory;
            CHECK(&thumbnailOutputInMemory != nullptr);
        }
        SECTION("SetThumbnailResult()") {
            ThumbnailOutputInMemory thumbnailOutputInMemory;
            thumbnailOutputInMemory.SetThumbnailResult({});
            CHECK(&thumbnailOutputInMemory != nullptr);
        }
        SECTION("GetResultData()") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
            data.get()[0] = 0;
            data.get()[1] = 1;
            data.get()[2] = 2;
            data.get()[3] = 3;
            data.get()[4] = 4;
            data.get()[5] = 5;
            data.get()[6] = 6;
            data.get()[7] = 7;
            data.get()[8] = 8;
            data.get()[9] = 9;
            data.get()[10] = 10;
            data.get()[11] = 11;

            ThumbnailResult result;
            result.SetColorFlag(true);
            result.SetResultData(data);
            result.SetResultSize(sizeX, sizeY);

            ThumbnailOutputInMemory thumbnailOutputInMemory;
            thumbnailOutputInMemory.SetThumbnailResult(result);

            CHECK(thumbnailOutputInMemory.GetResultData().get()[0] == 0);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[1] == 1);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[2] == 2);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[3] == 3);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[4] == 4);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[5] == 5);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[6] == 6);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[7] == 7);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[8] == 8);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[9] == 9);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[10] == 10);
            CHECK(thumbnailOutputInMemory.GetResultData().get()[11] == 11);
        }
        SECTION("GetDataSizeX()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
            data.get()[0] = 0;
            data.get()[1] = 1;
            data.get()[2] = 2;
            data.get()[3] = 3;
            data.get()[4] = 4;
            data.get()[5] = 5;
            data.get()[6] = 6;
            data.get()[7] = 7;
            data.get()[8] = 8;
            data.get()[9] = 9;
            data.get()[10] = 10;
            data.get()[11] = 11;

            ThumbnailResult result;
            result.SetColorFlag(true);
            result.SetResultData(data);
            result.SetResultSize(sizeX, sizeY);

            ThumbnailOutputInMemory thumbnailOutputInMemory;
            thumbnailOutputInMemory.SetThumbnailResult(result);

            CHECK(thumbnailOutputInMemory.GetDataSizeX() == 2);
        }
        SECTION("GetDataSizeY()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
            data.get()[0] = 0;
            data.get()[1] = 1;
            data.get()[2] = 2;
            data.get()[3] = 3;
            data.get()[4] = 4;
            data.get()[5] = 5;
            data.get()[6] = 6;
            data.get()[7] = 7;
            data.get()[8] = 8;
            data.get()[9] = 9;
            data.get()[10] = 10;
            data.get()[11] = 11;

            ThumbnailResult result;
            result.SetColorFlag(true);
            result.SetResultData(data);
            result.SetResultSize(sizeX, sizeY);

            ThumbnailOutputInMemory thumbnailOutputInMemory;
            thumbnailOutputInMemory.SetThumbnailResult(result);

            CHECK(thumbnailOutputInMemory.GetDataSizeY() == 2);
        }
        SECTION("IsColor()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto numberOfElements = sizeX * sizeY * 3;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
            data.get()[0] = 0;
            data.get()[1] = 1;
            data.get()[2] = 2;
            data.get()[3] = 3;
            data.get()[4] = 4;
            data.get()[5] = 5;
            data.get()[6] = 6;
            data.get()[7] = 7;
            data.get()[8] = 8;
            data.get()[9] = 9;
            data.get()[10] = 10;
            data.get()[11] = 11;

            ThumbnailResult result;
            result.SetColorFlag(true);
            result.SetResultData(data);
            result.SetResultSize(sizeX, sizeY);

            ThumbnailOutputInMemory thumbnailOutputInMemory;
            thumbnailOutputInMemory.SetThumbnailResult(result);

            CHECK(thumbnailOutputInMemory.IsColor() == true);
        }
    }
    TEST_CASE("ThumbnailOutputInMemory : practical test") {
        //TODO : Implement test
    }
}