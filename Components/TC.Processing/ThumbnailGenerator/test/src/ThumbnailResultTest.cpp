#include <catch2/catch.hpp>

#include "ThumbnailResult.h"

namespace ThumbnailResultTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailResult : unit test") {
        SECTION("ThumbnailResult()") {
            ThumbnailResult thumbnailResult;
            CHECK(&thumbnailResult != nullptr);
        }
        SECTION("ThumbnailResult(other)") {
            ThumbnailResult srcThumbnailResult;
            srcThumbnailResult.SetColorFlag(true);

            ThumbnailResult destThumbnailResult(srcThumbnailResult);
            CHECK(destThumbnailResult.GetColorFlag() == true);
        }
        SECTION("operator=()") {
            ThumbnailResult srcThumbnailResult;
            srcThumbnailResult.SetColorFlag(true);

            ThumbnailResult destThumbnailResult;
            destThumbnailResult = srcThumbnailResult;
            CHECK(destThumbnailResult.GetColorFlag() == true);
        }
        SECTION("SetResultData()") {
            ThumbnailResult thumbnailResult;
            thumbnailResult.SetResultData({});
            CHECK(&thumbnailResult != nullptr);
        }
        SECTION("SetResultSize()") {
            ThumbnailResult thumbnailResult;
            thumbnailResult.SetResultSize(1, 2);
            CHECK(&thumbnailResult != nullptr);
        }
        SECTION("SetColorFlag()") {
            ThumbnailResult thumbnailResult;
            thumbnailResult.SetColorFlag(true);
            CHECK(&thumbnailResult != nullptr);
        }
        SECTION("GetResultData()") {
            std::shared_ptr<uint8_t[]> resultData{ new uint8_t[1] };
            resultData.get()[0] = 1;

            ThumbnailResult thumbnailResult;
            thumbnailResult.SetResultData(resultData);
            CHECK(thumbnailResult.GetResultData().get()[0] == 1);
        }
        SECTION("GetResultSizeXY()") {
            constexpr auto sizeX = 1;
            constexpr auto sizeY = 2;

            ThumbnailResult thumbnailResult;
            thumbnailResult.SetResultSize(sizeX, sizeY);

            const auto [resultSizeX, resultSizeY] = thumbnailResult.GetResultSizeXY();
            CHECK(resultSizeX == sizeX);
            CHECK(resultSizeY == sizeY);
        }
        SECTION("GetColorFlag()") {
            constexpr auto colorFlag = true;

            ThumbnailResult thumbnailResult;
            thumbnailResult.SetColorFlag(colorFlag);

            CHECK(thumbnailResult.GetColorFlag() == colorFlag);
        }
    }
    TEST_CASE("ThumbnailResult : practical test") {
        constexpr auto sizeX = 1;
        constexpr auto sizeY = 2;
        constexpr auto colorFlag = true;

        constexpr auto numberOfElements = sizeX * sizeY * 3;
        
        std::shared_ptr<uint8_t[]> resultData{ new uint8_t[numberOfElements] };
        resultData.get()[0] = 1;
        resultData.get()[1] = 2;
        resultData.get()[2] = 3;
        resultData.get()[3] = 4;
        resultData.get()[4] = 5;
        resultData.get()[5] = 6;

        ThumbnailResult thumbnailResult;
        thumbnailResult.SetResultData(resultData);
        thumbnailResult.SetResultSize(sizeX, sizeY);
        thumbnailResult.SetColorFlag(colorFlag);

        CHECK(thumbnailResult.GetResultData().get()[0] == 1);
        CHECK(thumbnailResult.GetResultData().get()[1] == 2);
        CHECK(thumbnailResult.GetResultData().get()[2] == 3);
        CHECK(thumbnailResult.GetResultData().get()[3] == 4);
        CHECK(thumbnailResult.GetResultData().get()[4] == 5);
        CHECK(thumbnailResult.GetResultData().get()[5] == 6);

        CHECK(std::get<0>(thumbnailResult.GetResultSizeXY()) == sizeX);
        CHECK(std::get<1>(thumbnailResult.GetResultSizeXY()) == sizeY);

        CHECK(thumbnailResult.GetColorFlag() == true);
    }
}