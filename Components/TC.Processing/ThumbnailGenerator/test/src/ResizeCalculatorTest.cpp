#include <catch2/catch.hpp>

#include "ResizeCalculator.h"

namespace ResizeCalculatorTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ResizeCalculator : unit test") {
        SECTION("ResizeCalculator()") {
            ResizeCalculator resizeCalculator;
            CHECK(&resizeCalculator != nullptr);
        }
        SECTION("SetDataSize()") {
            ResizeCalculator resizeCalculator;
            resizeCalculator.SetDataSize(1, 2);
            CHECK(&resizeCalculator != nullptr);
        }
        SECTION("SetMaximumSizeInfo()") {
            ResizeCalculator resizeCalculator;
            resizeCalculator.SetMaximumResizeInfo(1, 2);
            CHECK(&resizeCalculator != nullptr);
        }
        SECTION("Calculate()") {
            ResizeCalculator resizeCalculator;
            SECTION("without setting sizes") {
                CHECK(resizeCalculator.Calculate() == false);
            }
            SECTION("with setting sizes") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(100, 100);
                CHECK(resizeCalculator.Calculate() == true);
            }
        }
        SECTION("GetResizingX()") {
            ResizeCalculator resizeCalculator;
            SECTION("maximum size is larger than data size") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(100, 100);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingX() == 10);
            }
            SECTION("fit to x axis") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(5, 100);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingX() == 5);
            }
            SECTION("fit to y axis") {
                resizeCalculator.SetDataSize(10, 21);
                resizeCalculator.SetMaximumResizeInfo(100, 4);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingX() == 2);
            }
            SECTION("resize both axis") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(5, 4);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingX() == 4);
            }
        }
        SECTION("GetResizingY()") {
            ResizeCalculator resizeCalculator;
            SECTION("maximum size is larger than data size") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(100, 100);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingY() == 10);
            }
            SECTION("fit to x axis") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(5, 100);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingY() == 5);
            }
            SECTION("fit to y axis") {
                resizeCalculator.SetDataSize(10, 21);
                resizeCalculator.SetMaximumResizeInfo(100, 4);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingY() == 4);
            }
            SECTION("resize both axis") {
                resizeCalculator.SetDataSize(10, 10);
                resizeCalculator.SetMaximumResizeInfo(4, 5);
                resizeCalculator.Calculate();
                CHECK(resizeCalculator.GetResizingY() == 4);
            }
        }
    }
    TEST_CASE("ResizeCalculator : practical test") {
        //TODO Implement test
    }
}