#include <catch2/catch.hpp>

#include "ThumbnailHT.h"

namespace ThumbnailHTTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailHT : unit test") {
        SECTION("ThumbnailHT()") {
            ThumbnailHT thumbnailHT;
            CHECK(&thumbnailHT != nullptr);
        }
        SECTION("SetHTData()") {
            ThumbnailHT thumbnailHT;
            thumbnailHT.SetHTData({});
            CHECK(&thumbnailHT != nullptr);
        }
        SECTION("SetDataSize()") {
            ThumbnailHT thumbnailHT;
            thumbnailHT.SetDataSize(1, 2);
            CHECK(&thumbnailHT != nullptr);
        }
        SECTION("Generate()") {
            ThumbnailHT thumbnailHT;
            SECTION("Without data") {
                CHECK(thumbnailHT.Generate() == false);
            }
            SECTION("With data") {
                constexpr auto sizeX = 2;
                constexpr auto sizeY = 3;

                std::shared_ptr<uint16_t[]> htData{ new uint16_t[sizeX * sizeY] };
                htData.get()[0] = 13370;
                htData.get()[1] = 13450;
                htData.get()[2] = 13500;
                htData.get()[3] = 13365;
                htData.get()[4] = 13440;
                htData.get()[5] = 13550;
                
                thumbnailHT.SetHTData(htData);
                thumbnailHT.SetDataSize(sizeX, sizeY);

                CHECK(thumbnailHT.Generate() == true);
            }
        }
        SECTION("GetThumbnailData()") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;

            std::shared_ptr<uint16_t[]> htData{ new uint16_t[sizeX * sizeY] };
            htData.get()[0] = 13370;
            htData.get()[1] = 13450;
            htData.get()[2] = 13500;
            htData.get()[3] = 13365;
            htData.get()[4] = 13440;
            htData.get()[5] = 13550;

            ThumbnailHT thumbnailHT;
            thumbnailHT.SetHTData(htData);
            thumbnailHT.SetDataSize(sizeX, sizeY);

            thumbnailHT.Generate();

            const auto thumbnailData = thumbnailHT.GetThumbnailData();
            CHECK(thumbnailData.get()[0] == 7);
            CHECK(thumbnailData.get()[1] == 117);
            CHECK(thumbnailData.get()[2] == 186);
            CHECK(thumbnailData.get()[3] == 0);
            CHECK(thumbnailData.get()[4] == 103);
            CHECK(thumbnailData.get()[5] == 255);
        }
        SECTION("GetThumbnailSizeX()") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;

            std::shared_ptr<uint16_t[]> htData{ new uint16_t[sizeX * sizeY] };
            htData.get()[0] = 13370;
            htData.get()[1] = 13450;
            htData.get()[2] = 13500;
            htData.get()[3] = 13365;
            htData.get()[4] = 13440;
            htData.get()[5] = 13550;

            ThumbnailHT thumbnailHT;
            thumbnailHT.SetHTData(htData);
            thumbnailHT.SetDataSize(sizeX, sizeY);

            thumbnailHT.Generate();

            CHECK(thumbnailHT.GetThumbnailSizeX() == sizeX);
        }
        SECTION("GetThumbnailSizeY()") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;

            std::shared_ptr<uint16_t[]> htData{ new uint16_t[sizeX * sizeY] };
            htData.get()[0] = 13370;
            htData.get()[1] = 13450;
            htData.get()[2] = 13500;
            htData.get()[3] = 13365;
            htData.get()[4] = 13440;
            htData.get()[5] = 13550;

            ThumbnailHT thumbnailHT;
            thumbnailHT.SetHTData(htData);
            thumbnailHT.SetDataSize(sizeX, sizeY);

            thumbnailHT.Generate();

            CHECK(thumbnailHT.GetThumbnailSizeY() == sizeY);
        }
    }
    TEST_CASE("ThumbnailHT : practical test") {
        //TODO : Implement test
    }
}
