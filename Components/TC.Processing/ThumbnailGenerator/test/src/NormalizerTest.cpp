#include <catch2/catch.hpp>

#include "Normalizer.h"

namespace NormalizerTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("Normalizer : unit test") {
        SECTION("Normalizer()") {
            Normalizer normalizer;
            CHECK(&normalizer != nullptr);
        }
        SECTION("SetData()") {
            Normalizer normalizer;
            normalizer.SetData({}, 0);
            CHECK(&normalizer != nullptr);
        }
        SECTION("SetNormalizationValues()") {
            Normalizer normalizer;
            normalizer.SetNormalizationValues(1, 2);
            CHECK(&normalizer != nullptr);
        }
        SECTION("Normalize()") {
            Normalizer normalizer;

            SECTION("Without data") {
                CHECK(normalizer.Normalize() == false);
            }

            SECTION("With data") {
                constexpr auto numberOfElements = 10;
                std::shared_ptr<float[]> data{ new float[numberOfElements] };

                constexpr float rawData[numberOfElements] = {1,2,3,4,5,6,7,8,9,10};
                std::copy_n(rawData, numberOfElements, data.get());

                normalizer.SetData(data, numberOfElements);
                normalizer.SetNormalizationValues(8, 2);

                CHECK(normalizer.Normalize() == true);
            }
        }
        SECTION("GetNormalizedData()") {
            constexpr auto numberOfElements = 10;
            std::shared_ptr<float[]> data{ new float[numberOfElements] };

            constexpr float rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10 };
            std::copy_n(rawData, numberOfElements, data.get());

            Normalizer normalizer;
            normalizer.SetData(data, numberOfElements);
            normalizer.SetNormalizationValues(8, 2);

            normalizer.Normalize();

            const auto normalizedData = normalizer.GetNormalizedData();
            CHECK(normalizedData.get()[0] == 0);
            CHECK(normalizedData.get()[1] == 0);
            CHECK(normalizedData.get()[2] == 43);
            CHECK(normalizedData.get()[3] == 85);
            CHECK(normalizedData.get()[4] == 128);
            CHECK(normalizedData.get()[5] == 170);
            CHECK(normalizedData.get()[6] == 213);
            CHECK(normalizedData.get()[7] == 255);
            CHECK(normalizedData.get()[8] == 255);
            CHECK(normalizedData.get()[9] == 255);
        }
    }
    TEST_CASE("Normalizer : practical test") {
        //TODO Implement test
    }
}