#include <catch2/catch.hpp>

#include "ThumbnailGeneratorPoly.h"

namespace ThumbnailGeneratorPolyTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailInputDataGetterForTest final : public IThumbnailInputDataGetter {
    public:
        ThumbnailInputDataGetterForTest() = default;
        ~ThumbnailInputDataGetterForTest() = default;
        auto SetInputData(const std::shared_ptr<float[]>& inputData)->void {
            this->inputData = inputData;
        }

        auto GetInputData() -> std::shared_ptr<float[]> override {
            return this->inputData;
        }
        auto GetSizeXY() -> std::tuple<int32_t, int32_t> override {
            return { 3,1 };
        }
        auto GetColorFlag() -> bool override {
            return false;
        }

    private:
        std::shared_ptr<float[]> inputData{};
    };

    class ThumbnailOutputForTest final : public IThumbnailOutput {
    public:
        ThumbnailOutputForTest() = default;
        ~ThumbnailOutputForTest() = default;
        auto SetThumbnailResult(const ThumbnailResult& result) -> void override {
            this->result = result;
        }
        auto GetThumbnailResult()const->ThumbnailResult {
            return this->result;
        }

    private:
        ThumbnailResult result;
    };

    TEST_CASE("ThumbnailGeneratorPoly : unit test") {
        SECTION("ThumbnailGeneratorPoly()") {
            ThumbnailGeneratorPoly thumbnailGeneratorPoly;
            CHECK(&thumbnailGeneratorPoly != nullptr);
        }
        SECTION("SetThumbnailInputDataList()") {
            ThumbnailGeneratorPoly thumbnailGeneratorPoly;
            thumbnailGeneratorPoly.SetThumbnailInputDataList({});
            CHECK(&thumbnailGeneratorPoly != nullptr);
        }
        SECTION("SetThumbnailOutput()") {
            ThumbnailGeneratorPoly thumbnailGeneratorPoly;
            thumbnailGeneratorPoly.SetThumbnailOutput({});
            CHECK(&thumbnailGeneratorPoly != nullptr);
        }
        SECTION("SetIntensityTable()") {
            ThumbnailGeneratorPoly thumbnailGeneratorPoly;
            thumbnailGeneratorPoly.SetIntensityTable({});
            CHECK(&thumbnailGeneratorPoly != nullptr);
        }
        SECTION("Generate()") {
            ThumbnailGeneratorPoly thumbnailGeneratorPoly;

            SECTION("Without data") {
                CHECK(thumbnailGeneratorPoly.Generate() == false);
            }
            SECTION("With data") {
                constexpr auto numberOfElements = 3;

                auto dataGetterPointer1 = new ThumbnailInputDataGetterForTest;
                std::shared_ptr<float[]> inputData1{ new float[numberOfElements] };
                inputData1.get()[0] = 10;
                inputData1.get()[1] = 20;
                inputData1.get()[2] = 30;
                dataGetterPointer1->SetInputData(inputData1);

                auto dataGetterPointer2 = new ThumbnailInputDataGetterForTest;
                std::shared_ptr<float[]> inputData2{ new float[numberOfElements] };
                inputData2.get()[0] = 20;
                inputData2.get()[1] = 60;
                inputData2.get()[2] = 80;
                dataGetterPointer2->SetInputData(inputData2);

                IThumbnailInputDataGetter::Pointer dataGetter1{ dataGetterPointer1 };
                IThumbnailInputDataGetter::Pointer dataGetter2{ dataGetterPointer2 };
                
                ThumbnailInputData thumbnailInputData1;
                thumbnailInputData1.SetDataGetter(dataGetter1);

                ThumbnailInputData thumbnailInputData2;
                thumbnailInputData2.SetDataGetter(dataGetter2);

                QList<std::tuple<ThumbnailInputData, IThumbnailPolyInput::RGB>> thumbnailInputDataList;
                thumbnailInputDataList.push_back({ thumbnailInputData1, {32, 64, 128} });
                thumbnailInputDataList.push_back({ thumbnailInputData2, {128, 64, 32} });

                auto outputPointer = new ThumbnailOutputForTest;
                IThumbnailOutput::Pointer output{ outputPointer };

                QList<uint8_t> intensityTable;
                for (auto index = 0; index < 256; ++index) {
                    intensityTable.push_back(index);
                }

                thumbnailGeneratorPoly.SetThumbnailInputDataList(thumbnailInputDataList);
                thumbnailGeneratorPoly.SetThumbnailOutput(output);
                thumbnailGeneratorPoly.SetIntensityTable(intensityTable);

                CHECK(thumbnailGeneratorPoly.Generate() == true);

                const auto result = outputPointer->GetThumbnailResult();
                CHECK(result.GetColorFlag() == true);
                CHECK(std::get<0>(result.GetResultSizeXY()) == 3);
                CHECK(std::get<1>(result.GetResultSizeXY()) == 1);

                const auto resultData = result.GetResultData();
                CHECK(int32_t(resultData.get()[0]) == 0);
                CHECK(int32_t(resultData.get()[1]) == 101);
                CHECK(int32_t(resultData.get()[2]) == 160);
                CHECK(int32_t(resultData.get()[3]) == 0);
                CHECK(int32_t(resultData.get()[4]) == 75);
                CHECK(int32_t(resultData.get()[5]) == 128);
                CHECK(int32_t(resultData.get()[6]) == 0);
                CHECK(int32_t(resultData.get()[7]) == 86);
                CHECK(int32_t(resultData.get()[8]) == 160);
            }
        }
    }
    TEST_CASE("ThumbnailGeneratorPoly : practical test") {
        //TODO : Implement test
    }
}