#include <catch2/catch.hpp>

#include "ThumbnailInputDataGetterInMemory.h"

namespace ThumbnailInputDataGetterInMemoryTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ThumbnailInputDataGetterInMemory : unit test") {
        SECTION("ThumbnailInputDataGetterInMemory()") {
            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            CHECK(&thumbnailInputDataGetterInMemory != nullptr);
        }
        SECTION("SetInputData()") {
            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetInputData({});
            CHECK(&thumbnailInputDataGetterInMemory != nullptr);
        }
        SECTION("SetSize()") {
            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetSize(1, 2);
            CHECK(&thumbnailInputDataGetterInMemory != nullptr);
        }
        SECTION("SetColorFlag()") {
            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetColorFlag(true);
            CHECK(&thumbnailInputDataGetterInMemory != nullptr);
        }
        SECTION("GetInputData()") {
            std::shared_ptr<float[]> data{ new float[1] };
            data.get()[0] = 1;

            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetInputData(data);

            CHECK(thumbnailInputDataGetterInMemory.GetInputData().get()[0] == 1);
        }
        SECTION("GetSizeXY()") {
            constexpr auto sizeX = 1;
            constexpr auto sizeY = 2;

            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetSize(sizeX, sizeY);

            CHECK(std::get<0>(thumbnailInputDataGetterInMemory.GetSizeXY()) == 1);
            CHECK(std::get<1>(thumbnailInputDataGetterInMemory.GetSizeXY()) == 2);
        }
        SECTION("GetColorFlag()") {
            ThumbnailInputDataGetterInMemory thumbnailInputDataGetterInMemory;
            thumbnailInputDataGetterInMemory.SetColorFlag(true);

            CHECK(thumbnailInputDataGetterInMemory.GetColorFlag() == true);
        }
    }
    TEST_CASE("ThumbnailInputDataGetterInMemory : practical test") {
        //TODO : Implement test
    }
}