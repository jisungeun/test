#include <catch2/catch.hpp>

#include "ImageMerger.h"

namespace ImageMergerTest {
    using namespace TC::Processing::ThumbnailGenerator;

    TEST_CASE("ImageMerger : unit test") {
        SECTION("ImageMerger()") {
            ImageMerger imageMerger;
            CHECK(&imageMerger != nullptr);
        }
        SECTION("SetDataList()") {
            ImageMerger imageMerger;
            imageMerger.SetDataList({}, 0);
            CHECK(&imageMerger != nullptr);
        }
        SECTION("Merge()") {
            ImageMerger imageMerger;

            SECTION("Without data") {
                CHECK(imageMerger.Merge() == false);
            }
            SECTION("With data") {
                constexpr uint64_t numberOfElements = 3;
                ImageMerger::ImageData data1{ new uint8_t[numberOfElements] };
                ImageMerger::ImageData data2{ new uint8_t[numberOfElements] };
                data1.get()[0] = 10;
                data1.get()[1] = 20;
                data1.get()[2] = 30;
                data2.get()[0] = 30;
                data2.get()[1] = 20;
                data2.get()[2] = 10;

                ImageMerger::RGB rgb1 = { 128, 64, 32 };
                ImageMerger::RGB rgb2 = { 32, 64, 128 };

                QList<std::tuple<ImageMerger::ImageData, ImageMerger::RGB>> dataList{ {data1, rgb1},{data2, rgb2} };

                imageMerger.SetDataList(dataList, numberOfElements);
                CHECK(imageMerger.Merge() == true);
            }
        }
        SECTION("GetMergedImageData()") {
            constexpr uint64_t numberOfElements = 3;
            ImageMerger::ImageData data1{ new uint8_t[numberOfElements] };
            ImageMerger::ImageData data2{ new uint8_t[numberOfElements] };
            data1.get()[0] = 10;
            data1.get()[1] = 20;
            data1.get()[2] = 30;
            data2.get()[0] = 30;
            data2.get()[1] = 20;
            data2.get()[2] = 10;

            ImageMerger::RGB rgb1 = { 128, 64, 32 };
            ImageMerger::RGB rgb2 = { 32, 64, 128 };

            QList<std::tuple<ImageMerger::ImageData, ImageMerger::RGB>> dataList{ {data1, rgb1},{data2, rgb2} };

            ImageMerger imageMerger;
            imageMerger.SetDataList(dataList, numberOfElements);
            imageMerger.Merge();

            const auto mergedImageData = imageMerger.GetMergedImageData();
            CHECK(mergedImageData.get()[0] == 9);
            CHECK(mergedImageData.get()[1] == 13);
            CHECK(mergedImageData.get()[2] == 16);
            CHECK(mergedImageData.get()[3] == 10);
            CHECK(mergedImageData.get()[4] == 10);
            CHECK(mergedImageData.get()[5] == 10);
            CHECK(mergedImageData.get()[6] == 16);
            CHECK(mergedImageData.get()[7] == 13);
            CHECK(mergedImageData.get()[8] == 9);
        }
    }
    TEST_CASE("ImageMerger : practical test") {
        //TODO : Implement test
    }
}