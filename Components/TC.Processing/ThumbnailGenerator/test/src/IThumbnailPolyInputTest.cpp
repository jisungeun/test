#include <catch2/catch.hpp>

#include "IThumbnailPolyInput.h"

namespace IThumbnailPolyInputTest {
    using namespace TC::Processing::ThumbnailGenerator;

    class ThumbnailPolyInputForTest final : public IThumbnailPolyInput {
    public:
        ThumbnailPolyInputForTest() = default;
        ~ThumbnailPolyInputForTest() = default;

        auto SetThumbnailInputDataList(const QList<std::tuple<ThumbnailInputData, RGB>>& thumbnailInputDataList) -> void override {
            this->setThumbnailInputDataListTriggered = true;
        }

        bool setThumbnailInputDataListTriggered{ false };
    };

    TEST_CASE("IThumbnailPolyInput : unit test") {
        SECTION("SetThumbnailInputDataList()") {
            ThumbnailPolyInputForTest iThumbnailPolyInput;
            CHECK(iThumbnailPolyInput.setThumbnailInputDataListTriggered == false);
            iThumbnailPolyInput.SetThumbnailInputDataList({});
            CHECK(iThumbnailPolyInput.setThumbnailInputDataListTriggered == true);
        }
    }
}