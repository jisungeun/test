#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

#include "IThumbnailInputDataGetter.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailInputDataGetterInMemory final : public IThumbnailInputDataGetter {
    public:
        ThumbnailInputDataGetterInMemory();
        ~ThumbnailInputDataGetterInMemory();

        auto SetInputData(const std::shared_ptr<float[]>& data)->void;
        auto SetSize(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto SetColorFlag(const bool& colorFlag)->void;

        auto GetInputData()->std::shared_ptr<float[]> override;
        auto GetSizeXY()->std::tuple<int32_t, int32_t> override;
        auto GetColorFlag() -> bool override;
        
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}