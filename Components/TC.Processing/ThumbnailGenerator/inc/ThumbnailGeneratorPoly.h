#pragma once

#include <memory>

#include "IThumbnailOutput.h"
#include "TCThumbnailGeneratorExport.h"
#include "IThumbnailPolyInput.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailGeneratorPoly final : public IThumbnailPolyInput{
    public:
        ThumbnailGeneratorPoly();
        ~ThumbnailGeneratorPoly();

        auto SetThumbnailInputDataList(const QList<std::tuple<ThumbnailInputData, RGB>>& thumbnailInputDataList)
            -> void override;
        auto SetThumbnailOutput(const IThumbnailOutput::Pointer& output)->void;
        auto SetIntensityTable(const QList<uint8_t>& intensityTable)->void;

        auto Generate()->bool;
        
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}