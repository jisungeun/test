#pragma once

#include "TCThumbnailGeneratorExport.h"
#include <tuple>
#include <QList>

#include "ThumbnailInputData.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API IThumbnailPolyInput {
    public:
        struct RGB {
            uint8_t r{ 0 };
            uint8_t g{ 0 };
            uint8_t b{ 0 };
        };

        virtual ~IThumbnailPolyInput() = default;

        virtual auto SetThumbnailInputDataList(const QList<std::tuple<ThumbnailInputData, RGB>>& thumbnailInputDataList)->void = 0;
    };
}