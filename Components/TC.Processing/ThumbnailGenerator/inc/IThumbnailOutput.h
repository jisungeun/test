#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"
#include "ThumbnailResult.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API IThumbnailOutput {
    public:
        using Pointer = std::shared_ptr<IThumbnailOutput>;
        virtual ~IThumbnailOutput() = default;

        virtual auto SetThumbnailResult(const ThumbnailResult& result)->void = 0;
    };
}