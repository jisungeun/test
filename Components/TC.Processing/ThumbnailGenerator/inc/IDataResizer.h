#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator{
    class TCThumbnailGenerator_API IDataResizer {
    public:
        using Pointer = std::shared_ptr<IDataResizer>;
        virtual ~IDataResizer() = default;

        virtual auto SetData(const std::shared_ptr<float[]>& data, const int32_t& sizeX, const int32_t& sizeY, 
            const bool& colorFlag)->void = 0;

        virtual auto SetResizingInfo(const int32_t& resizingX, const int32_t& resizingY)->void = 0;

        virtual auto Resize()->bool = 0;
        virtual auto GetResizedData()const->std::shared_ptr<float[]> = 0;
    };
}