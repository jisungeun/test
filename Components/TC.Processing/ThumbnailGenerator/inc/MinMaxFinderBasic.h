#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

#include "IMinMaxFinder.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API MinMaxFinderBasic final : public IMinMaxFinder {
    public:
        MinMaxFinderBasic();
        ~MinMaxFinderBasic();

        auto SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements)->void;

        auto Find() -> bool override;
        auto GetMinValue() -> float override;
        auto GetMaxValue() -> float override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}