#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

#include "IMinMaxFinder.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API MinMaxFinder3Sigma final : public IMinMaxFinder {
    public:
        MinMaxFinder3Sigma();
        ~MinMaxFinder3Sigma();

        auto SetData(const std::shared_ptr<float[]>& data, const uint64_t& numberOfElements)->void;
        auto Find() -> bool override;
        auto GetMinValue() -> float override;
        auto GetMaxValue() -> float override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}