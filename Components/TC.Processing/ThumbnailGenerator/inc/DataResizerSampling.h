#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"
#include "IDataResizer.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API DataResizerSampling final : public IDataResizer {
    public:
        DataResizerSampling();
        ~DataResizerSampling();

        auto SetData(const std::shared_ptr<float[]>& data, const int32_t& sizeX, const int32_t& sizeY,
            const bool& colorFlag) -> void override;
        auto SetResizingInfo(const int32_t& resizingX, const int32_t& resizingY) -> void override;
        auto Resize() -> bool override;

        auto GetResizedData() const -> std::shared_ptr<float[]> override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}