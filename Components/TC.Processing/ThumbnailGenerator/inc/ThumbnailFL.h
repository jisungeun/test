#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailFL {
    public:
        ThumbnailFL();
        ~ThumbnailFL();

        auto SetFLCH0Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g, const uint8_t& b)->void;
        auto SetFLCH1Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g, const uint8_t& b)->void;
        auto SetFLCH2Data(const std::shared_ptr<uint16_t[]>& data, const uint8_t& r, const uint8_t& g, const uint8_t& b)->void;
        auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY)->void;

        auto Generate()->bool;

        auto GetThumbnailData()->std::shared_ptr<uint8_t[]>;
        auto GetThumbnailSizeX()->int32_t;
        auto GetThumbnailSizeY()->int32_t;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}