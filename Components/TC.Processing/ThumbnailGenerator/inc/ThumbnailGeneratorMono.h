#pragma once

#include <memory>
#include <QList>
#include "TCThumbnailGeneratorExport.h"

#include "IThumbnailMonoInput.h"
#include "IThumbnailOutput.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailGeneratorMono final : public IThumbnailMonoInput {
    public:
        ThumbnailGeneratorMono();
        ~ThumbnailGeneratorMono();

        auto SetThumbnailInputData(const ThumbnailInputData& thumbnailInputData) -> void override;
        auto SetThumbnailOutput(const IThumbnailOutput::Pointer& output)->void;

        auto SetIntensityTable(const QList<uint8_t>& intensityTable)->void;

        auto Generate()->bool;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}