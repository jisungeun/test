#pragma once

#include "TCThumbnailGeneratorExport.h"

#include "ThumbnailInputData.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API IThumbnailMonoInput {
    public:
        virtual ~IThumbnailMonoInput() = default;

        virtual auto SetThumbnailInputData(const ThumbnailInputData& thumbnailInputData)->void = 0;
    };
}