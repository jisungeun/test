#pragma once

#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API IMinMaxFinder {
    public:
        virtual ~IMinMaxFinder() = default;

        virtual auto Find()->bool = 0;
        virtual auto GetMinValue()->float = 0;
        virtual auto GetMaxValue()->float = 0;
    };
}