#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

#include "IThumbnailOutput.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailOutputInMemory final : public IThumbnailOutput{
    public:
        ThumbnailOutputInMemory();
        ~ThumbnailOutputInMemory();

        auto SetThumbnailResult(const ThumbnailResult& result) -> void override;

        auto GetResultData()const->std::shared_ptr<uint8_t[]>;
        auto GetDataSizeX()const->int32_t;
        auto GetDataSizeY()const->int32_t;
        auto IsColor()const->bool;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}