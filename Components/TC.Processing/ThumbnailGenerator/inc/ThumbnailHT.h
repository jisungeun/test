#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailHT {
    public:
        ThumbnailHT();
        ~ThumbnailHT();

        auto SetHTData(const std::shared_ptr<uint16_t[]>& data)->void;
        auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY)->void;

        auto Generate()->bool;

        auto GetThumbnailData()->std::shared_ptr<uint8_t[]>;
        auto GetThumbnailSizeX()->int32_t;
        auto GetThumbnailSizeY()->int32_t;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}