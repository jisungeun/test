#pragma once

#include <memory>
#include "IThumbnailInputDataGetter.h"
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailInputData {
    public:
        ThumbnailInputData();
        ThumbnailInputData(const ThumbnailInputData& other);
        ~ThumbnailInputData();

        auto operator=(const ThumbnailInputData& other)->ThumbnailInputData&;

        auto SetDataGetter(const IThumbnailInputDataGetter::Pointer& getter)->void;
        auto IsDataGetterSet()const->bool;

        auto GetData() const ->std::shared_ptr<float[]>;
        auto GetSizeX() const ->int32_t;
        auto GetSizeY() const ->int32_t;
        auto GetColorFlag() const ->bool;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}