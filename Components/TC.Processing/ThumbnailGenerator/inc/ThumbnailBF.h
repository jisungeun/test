#pragma once

#include <memory>
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailBF {
    public:
        ThumbnailBF();
        ~ThumbnailBF();

        auto SetBFData(const std::shared_ptr<uint8_t[]>& data)->void;
        auto SetDataSize(const uint64_t& sizeX, const uint64_t& sizeY)->void;

        auto Generate()->bool;

        auto GetThumbnailData()->std::shared_ptr<uint8_t[]>;
        auto GetThumbnailSizeX()->int32_t;
        auto GetThumbnailSizeY()->int32_t;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}