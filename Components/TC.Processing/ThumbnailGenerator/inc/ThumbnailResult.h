#pragma once

#include <memory>
#include <tuple>

#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API ThumbnailResult {
    public:
        ThumbnailResult();
        ThumbnailResult(const ThumbnailResult& other);
        ~ThumbnailResult();

        auto operator=(const ThumbnailResult& other)->ThumbnailResult&;

        auto SetResultData(const std::shared_ptr<uint8_t[]>& resultData)->void;
        auto SetResultSize(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto SetColorFlag(const bool& colorFlag)->void;

        auto GetResultData()const->const std::shared_ptr<uint8_t[]>&;
        auto GetResultSizeXY()const->std::tuple<int32_t, int32_t>;
        auto GetColorFlag()const->const bool&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
