#pragma once

#include <memory>
#include <tuple>
#include "TCThumbnailGeneratorExport.h"

namespace TC::Processing::ThumbnailGenerator {
    class TCThumbnailGenerator_API IThumbnailInputDataGetter {
    public:
        using Pointer = std::shared_ptr<IThumbnailInputDataGetter>;
        virtual ~IThumbnailInputDataGetter() = default;

        virtual auto GetInputData()->std::shared_ptr<float[]> = 0;
        virtual auto GetSizeXY()->std::tuple<int32_t, int32_t> = 0;
        virtual auto GetColorFlag()->bool = 0;
    };
}
