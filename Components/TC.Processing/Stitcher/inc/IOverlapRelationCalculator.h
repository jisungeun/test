#pragma once

#include <memory>

#include "TCStitcherExport.h"

#include "TileSet.h"
#include "TileConfiguration.h"
#include "OverlapRelationSet.h"

class TCStitcher_API IOverlapRelationCalculator {
public:
    typedef std::shared_ptr<IOverlapRelationCalculator> Pointer;
    virtual ~IOverlapRelationCalculator() = default;

    virtual auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void = 0;
    virtual auto SetTileSet(const TileSet& tileSet)->void = 0;

    virtual auto Calculate()->OverlapRelationSet = 0;
};
