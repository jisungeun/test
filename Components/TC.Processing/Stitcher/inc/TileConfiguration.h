#pragma once

#include <memory>

#include "TCStitcherExport.h"

class TCStitcher_API TileConfiguration {
public:
    TileConfiguration();
    TileConfiguration(const TileConfiguration& other);
    ~TileConfiguration();

    auto operator=(const TileConfiguration& other)->TileConfiguration&;

    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;
    auto SetTileSizeInPixel(const int32_t& tileSizeX, const int32_t& tileSizeY, const int32_t& tileSizeZ)->void;
    auto SetOverlapLengthInPixel(const int32_t& overlapLengthX, const int32_t& overlapLengthY)->void;

    auto GetTileNumberX() const ->const int32_t&;
    auto GetTileNumberY() const ->const int32_t&;

    auto GetTileSizeXInPixel() const ->const int32_t&;
    auto GetTileSizeYInPixel() const ->const int32_t&;
    auto GetTileSizeZInPixel() const ->const int32_t&;

    auto GetOverlapLengthXInPixel() const ->const int32_t&;
    auto GetOverlapLengthYInPixel() const ->const int32_t&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};