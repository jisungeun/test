#pragma once

#include "TCStitcherExport.h"
#include "TileConfiguration.h"

class TCStitcher_API IMissingTileChecker {
public:
    virtual ~IMissingTileChecker() = default;

    virtual auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void = 0;
    virtual auto SetTimelapseNumber(const int32_t& timelapseNumber)->void = 0;

    virtual auto Check()->bool = 0;
};
