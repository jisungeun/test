#pragma once

#include <memory>

#include "IOverlapRelationCalculator.h"

#include "TCStitcherExport.h"

class TCStitcher_API OverlapRelationCalculatorPhaseCorrelation final : public IOverlapRelationCalculator{
public:
    OverlapRelationCalculatorPhaseCorrelation();
    ~OverlapRelationCalculatorPhaseCorrelation();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override;
    auto SetTileSet(const TileSet& tileSet) -> void override;
    auto Calculate() -> OverlapRelationSet override;
    
private:
    class Impl;
    std::unique_ptr<Impl> d;
};