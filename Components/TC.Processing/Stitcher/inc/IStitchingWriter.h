#pragma once

#include "TCStitcherExport.h"

#include "TileSet.h"
#include "TilePositionSet.h"
#include "TileConfiguration.h"

#include "StitchingWriterResult.h"

class TCStitcher_API IStitchingWriter {
public:
    virtual ~IStitchingWriter() = default;

    virtual auto SetTileSet(const TileSet& tileSet)->void = 0;
    virtual auto SetTilePositionSet(const TilePositionSet& tilePositionSet)->void = 0;
    virtual auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void = 0;

    virtual auto Run()->bool = 0;

    virtual auto GetStitchingWriterResult() const -> const StitchingWriterResult& = 0;
};