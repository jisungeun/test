#pragma once

#include <memory>

#include "ITilePositionCalculator.h"

#include "TCStitcherExport.h"

class TCStitcher_API TilePositionCalculatorBestOverlap final : public ITilePositionCalculator{
public:
    TilePositionCalculatorBestOverlap();
    ~TilePositionCalculatorBestOverlap();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override;
    auto SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet) -> void;
    auto Calculate()->TilePositionSet override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};