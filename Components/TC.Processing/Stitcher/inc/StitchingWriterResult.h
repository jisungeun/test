#pragma once

#include <memory>

#include "TCStitcherExport.h"

class TCStitcher_API StitchingWriterResult {
public:
    StitchingWriterResult();
    StitchingWriterResult(const StitchingWriterResult& other);
    ~StitchingWriterResult();

    auto operator=(const StitchingWriterResult& other)->StitchingWriterResult&;

    auto SetSuccessFlag(const bool& successFlag)->void;
    auto GetSuccessFlag() const -> const bool&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};