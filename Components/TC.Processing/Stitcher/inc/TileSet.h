#pragma once

#include <memory>

#include "TCStitcherExport.h"
#include "Tile.h"

class TCStitcher_API TileSet {
public:
    TileSet();
    TileSet(const TileSet& other);
    ~TileSet();

    auto operator=(const TileSet& other)->TileSet&;

    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;

    auto InsertTile(const Tile& tile, const int32_t& tileIndexX, const int32_t& tileIndexY)->void; // most left-up index is (0, 0)
    auto GetTile(const int32_t& tileIndexX, const int32_t& tileIndexY) const ->Tile;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
