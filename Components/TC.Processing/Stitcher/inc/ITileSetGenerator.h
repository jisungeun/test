#pragma once

#include "TCStitcherExport.h"
#include "TileConfiguration.h"
#include "TileSet.h"

class TCStitcher_API ITileSetGenerator {
public:
    virtual ~ITileSetGenerator() = default;

    virtual auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void = 0;
    virtual auto Generate()->TileSet = 0;
};
