#pragma once

#include "TCStitcherExport.h"

#include "StitchingWriterResult.h"

class TCStitcher_API IStitchingOutput {
public:
    virtual ~IStitchingOutput() = default;

    virtual auto SetStitchingWriterResult(const StitchingWriterResult& stitchingWriterResult)->void = 0;
};