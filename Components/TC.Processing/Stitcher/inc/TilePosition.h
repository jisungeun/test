#pragma once

#include <memory>

#include "TCStitcherExport.h"

class TCStitcher_API TilePosition {
public:
    TilePosition();
    TilePosition(const TilePosition& other);
    ~TilePosition();

    auto operator=(const TilePosition& other)->TilePosition&;

    auto SetPositions(const float& tilePositionX, const float& tilePositionY, const float& tilePositionZ)->void;
    auto GetTilePositionX() const -> const float&;
    auto GetTilePositionY() const -> const float&;
    auto GetTilePositionZ() const -> const float&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};