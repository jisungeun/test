#pragma once

#include "TCStitcherExport.h"

#include "OverlapRelationSet.h"
#include "TileConfiguration.h"
#include "TilePositionSet.h"

class TCStitcher_API ITilePositionCalculator {
public:
    typedef std::shared_ptr<ITilePositionCalculator> Pointer;

    virtual ~ITilePositionCalculator() = default;

    virtual auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void = 0;

    virtual auto Calculate()->TilePositionSet = 0;
};
