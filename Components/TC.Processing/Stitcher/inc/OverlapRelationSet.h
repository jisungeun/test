#pragma once

#include <memory>

#include "TCStitcherExport.h"

#include "OverlapRelation.h"

class TCStitcher_API OverlapRelationSet {
public:
    OverlapRelationSet();
    OverlapRelationSet(const OverlapRelationSet& other);
    ~OverlapRelationSet();

    auto operator=(const OverlapRelationSet& other)->OverlapRelationSet&;

    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;

    auto InsertOverlapRelation(const int32_t& overlapIndexX, const int32_t& overlapIndexY, 
        const OverlapRelation& overlapRelation)->void;

    auto GetOverlapRelation(const int32_t& overlapIndexX, const int32_t& overlapIndexY) const ->OverlapRelation;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};