#pragma once

#include <memory>

#include <QString>

#include "TCStitcherExport.h"
#include "IStitchingWriter.h"

class TCStitcher_API StitchingWriterHDF5FL final : public IStitchingWriter{
public:
    StitchingWriterHDF5FL();
    ~StitchingWriterHDF5FL();

    auto SetTileSet(const TileSet& tileSet) -> void override;
    auto SetTilePositionSet(const TilePositionSet& tilePositionSet) -> void override;
    auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override;
    auto SetBoundaryCropFlag(const bool& flag)->void;
    auto SetDataSizeLimit(const int32_t& sizeX, const int32_t& sizeY)->void;

    auto SetHDF5FilePath(const QString& hdf5FilePath)->void;

    auto Run() -> bool override;

    auto GetStitchingWriterResult() const -> const StitchingWriterResult & override;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};