#pragma once

#include "TileConfiguration.h"

#include "TCStitcherExport.h"

class TCStitcher_API ITileConfigurationReader {
public:
    virtual ~ITileConfigurationReader() = default;
    virtual auto Read()->bool = 0;
    virtual auto GetTileConfiguration() const ->const TileConfiguration& = 0;
};
