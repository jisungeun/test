#pragma once

#include <memory>

#include "ITilePositionCalculator.h"

#include "TCStitcherExport.h"

class TCStitcher_API TilePositionCalculatorStaged final : public ITilePositionCalculator{
public:
    TilePositionCalculatorStaged();
    ~TilePositionCalculatorStaged();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override;
    auto Calculate()->TilePositionSet override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};