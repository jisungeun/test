#pragma once

#include <memory>

#include "TCStitcherExport.h"

class TCStitcher_API ITileDataGetter {
public:
    typedef std::shared_ptr<ITileDataGetter> Pointer;

    virtual ~ITileDataGetter() = default;

    virtual auto GetData() const ->std::shared_ptr<float[]> = 0;
    virtual auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, 
        const int32_t& z0, const int32_t& z1) const ->std::shared_ptr<float[]> = 0;
};