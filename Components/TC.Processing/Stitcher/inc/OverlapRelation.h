#pragma once

#include <memory>

#include "TCStitcherExport.h"

class TCStitcher_API OverlapRelation {
public:
    OverlapRelation();
    OverlapRelation(const OverlapRelation& other);
    ~OverlapRelation();

    auto operator=(const OverlapRelation& other)->OverlapRelation&;

    auto SetShiftValue(const float& valueX, const float& valueY, const float& valueZ)->void;
    
    auto GetShiftValueX() const -> const float&;
    auto GetShiftValueY() const -> const float&;
    auto GetShiftValueZ() const -> const float&;

    auto SetReliability(const float& reliability)->void;

    auto GetReliability() const -> const float&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};