#pragma once

#include <memory>

#include "TCStitcherExport.h"

#include "ITileDataGetter.h"

class TCStitcher_API Tile {
public:
    enum class OverlapDirection { Left, Right, Up, Down };

    Tile();
    Tile(const Tile& other);
    ~Tile();

    auto operator=(const Tile& other)->Tile&;

    auto SetTileSize(const int32_t& tileSizeX, const int32_t& tileSizeY, const int32_t& tileSizeZ)->void;
    auto GetTileSizeX() const -> const int32_t&;
    auto GetTileSizeY() const -> const int32_t&;
    auto GetTileSizeZ() const -> const int32_t&;

    auto SetOverlapSize(const int32_t& overlapSizeX, const int32_t& overlapSizeY)->void;
    auto GetOverlapSizeX() const -> const int32_t&;
    auto GetOverlapSizeY() const -> const int32_t&;

    auto SetTileDataGetter(const ITileDataGetter::Pointer& tileDataGetter)->void;

    auto GetData() const ->std::shared_ptr<float[]>;
    auto GetOverlapData(const OverlapDirection& overlapDirection) const ->std::shared_ptr<float[]>;
    auto GetSliceData(const int32_t& zIndex) const ->std::shared_ptr<float[]>;
    auto GetSliceStackData(const int32_t& z0, const int32_t& z1) const ->std::shared_ptr<float[]>;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};