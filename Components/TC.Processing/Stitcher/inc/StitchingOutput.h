#pragma once

#include <memory>

#include "TCStitcherExport.h"

#include "IStitchingOutput.h"

class TCStitcher_API StitchingOutput final : public IStitchingOutput{
public:
    StitchingOutput();
    ~StitchingOutput();

    auto SetStitchingWriterResult(const StitchingWriterResult& stitchingWriterResult) -> void override;
private:
    class Impl;
    std::unique_ptr<Impl> d;

};