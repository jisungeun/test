#pragma once

#include <memory>

#include "TCStitcherExport.h"
#include "TilePosition.h"

class TCStitcher_API TilePositionSet {
public:
    TilePositionSet();
    TilePositionSet(const TilePositionSet& other);
    ~TilePositionSet();

    auto operator=(const TilePositionSet& other)->TilePositionSet&;

    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;
    auto GetTileNumberX()const->const int32_t&;
    auto GetTileNumberY()const->const int32_t&;

    auto InsertTilePosition(const int32_t& tileIndexX, const int32_t& tileIndexY, const TilePosition& tilePosition)
        ->void;

    auto GetTilePosition(const int32_t& tileIndexX, const int32_t& tileIndexY) const ->TilePosition;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
