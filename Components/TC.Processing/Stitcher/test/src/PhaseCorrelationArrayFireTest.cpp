#include <catch2/catch.hpp>

#include <QString>

#include "H5Cpp.h"

#include "PhaseCorrelationArrayFire.h"


namespace PhaseCorrelationArrayFireTest {
    const QString testTopFolderPath = TEST_DATA_FOLDR_PATH;
    const QString dataFilePath = testTopFolderPath + "/SampleData/PhaseCorrelation/PhaseCorrelationSimulationData.h5";

    constexpr int32_t leftRightDataSizeX = 91;
    constexpr int32_t leftRightDataSizeY = 254;
    constexpr int32_t leftRightDataSizeZ = 20;
    constexpr int32_t leftRightDataSize = leftRightDataSizeX * leftRightDataSizeY * leftRightDataSizeZ;

    constexpr int32_t upDownDataSizeX = 254;
    constexpr int32_t upDownDataSizeY = 91;
    constexpr int32_t upDownDataSizeZ = 20;
    constexpr int32_t upDownDataSize = upDownDataSizeX * upDownDataSizeY * upDownDataSizeZ;


    auto GetData(const std::string& dataSetName, const int32_t& dataSize)-> std::shared_ptr<float[]> {
        H5::H5File file(dataFilePath.toStdString(), H5F_ACC_RDONLY);

        std::shared_ptr<float[]> data{ new float[dataSize] };

        auto dataSet = file.openDataSet(dataSetName);
        dataSet.read(data.get(), H5::PredType::NATIVE_FLOAT);

        dataSet.close();
        file.close();

        return data;
    }

    auto GetLeftData()->std::shared_ptr<float[]> {
        return GetData("left", leftRightDataSize);
    }
    auto GetRightData()->std::shared_ptr<float[]> {
        return GetData("right", leftRightDataSize);
    }
    auto GetUpData()->std::shared_ptr<float[]> {
        return GetData("up", upDownDataSize);
    }
    auto GetDownData()->std::shared_ptr<float[]> {
        return GetData("down", upDownDataSize);
    }

    TEST_CASE("PhaseCorrelationArrayFire : unit test") {
        SECTION("PhaseCorrelationArrayFire()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            CHECK(&phaseCorrelationArrayFire != nullptr);
        }
        SECTION("SetData()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetData({}, {});
            CHECK(&phaseCorrelationArrayFire != nullptr);
        }
        SECTION("SetDataSize()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(0, 0, 0);
            CHECK(&phaseCorrelationArrayFire != nullptr);
        }
        SECTION("Run()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            CHECK(phaseCorrelationArrayFire.Run());
        }
        SECTION("GetReliability()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            phaseCorrelationArrayFire.Run();

            CHECK(static_cast<int32_t>(phaseCorrelationArrayFire.GetReliability() * 10000) == 9857);
        }
        SECTION("GetShiftValueX()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            phaseCorrelationArrayFire.Run();

            CHECK(phaseCorrelationArrayFire.GetShiftValueX() == 1);
        }
        SECTION("GetShiftValueY()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            phaseCorrelationArrayFire.Run();

            CHECK(phaseCorrelationArrayFire.GetShiftValueY() == 2);
        }
        SECTION("GetShiftValueZ()") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            phaseCorrelationArrayFire.Run();

            CHECK(phaseCorrelationArrayFire.GetShiftValueZ() == 0);
        }
    }

    TEST_CASE("PhaseCorrelationArrayFire : practical test") {
        SECTION("left right tile") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(leftRightDataSizeX, leftRightDataSizeY, leftRightDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetLeftData(), GetRightData());

            phaseCorrelationArrayFire.Run();

            CHECK(static_cast<int32_t>(phaseCorrelationArrayFire.GetReliability() * 10000) == 9857);
            CHECK(phaseCorrelationArrayFire.GetShiftValueX() == 1);
            CHECK(phaseCorrelationArrayFire.GetShiftValueY() == 2);
            CHECK(phaseCorrelationArrayFire.GetShiftValueZ() == 0);
        }
        SECTION("up down tile") {
            PhaseCorrelationArrayFire phaseCorrelationArrayFire;
            phaseCorrelationArrayFire.SetDataSize(upDownDataSizeX, upDownDataSizeY, upDownDataSizeZ);
            phaseCorrelationArrayFire.SetData(GetUpData(), GetDownData());

            phaseCorrelationArrayFire.Run();

            CHECK(static_cast<int32_t>(phaseCorrelationArrayFire.GetReliability() * 10000) == 9873);
            CHECK(phaseCorrelationArrayFire.GetShiftValueX() == 2);
            CHECK(phaseCorrelationArrayFire.GetShiftValueY() == -1);
            CHECK(phaseCorrelationArrayFire.GetShiftValueZ() == 0);
        }
    }
}