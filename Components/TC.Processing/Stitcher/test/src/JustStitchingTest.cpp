//#include <iostream>
//#include <catch2/catch.hpp>
//
//#include <memory>
//
//#include <QString>
//#include <QStringList>
//#include <QMap>
//
//#include "OverlapRelationCalculatorPhaseCorrelation.h"
//#include "StitchingWriterHDF5.h"
//#include "TileConfigurationReaderTCFHT.h"
//#include "TileDataGetterTCFHT.h"
//#include "TilePositionCalculatorBestOverlap.h"
//#include "TileSetGeneratorTCF.h"
//
//namespace JustStitchingTest {
//    TEST_CASE("just stitching 3x5") {
//        auto start = std::chrono::system_clock::now();
//
//        const QString tcfFilePath00 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133044.860.NIH3T3 apoptosis-015T001.TCF";
//        const QString tcfFilePath01 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133044.937.NIH3T3 apoptosis-015T002.TCF";
//        const QString tcfFilePath02 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.002.NIH3T3 apoptosis-015T003.TCF";
//        const QString tcfFilePath03 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.060.NIH3T3 apoptosis-015T004.TCF";
//        const QString tcfFilePath04 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.139.NIH3T3 apoptosis-015T005.TCF";
//        const QString tcfFilePath05 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.212.NIH3T3 apoptosis-015T006.TCF";
//        const QString tcfFilePath06 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.277.NIH3T3 apoptosis-015T007.TCF";
//        const QString tcfFilePath07 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.344.NIH3T3 apoptosis-015T008.TCF";
//        const QString tcfFilePath08 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.421.NIH3T3 apoptosis-015T009.TCF";
//        const QString tcfFilePath09 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.488.NIH3T3 apoptosis-015T010.TCF";
//        const QString tcfFilePath10 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.550.NIH3T3 apoptosis-015T011.TCF";
//        const QString tcfFilePath11 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.768.NIH3T3 apoptosis-015T012.TCF";
//        const QString tcfFilePath12 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.877.NIH3T3 apoptosis-015T013.TCF";
//        const QString tcfFilePath13 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133045.953.NIH3T3 apoptosis-015T014.TCF";
//        const QString tcfFilePath14 = "F:/00_Data/20190312_CellforStitch/OnlyTCF/20190312.133046.020.NIH3T3 apoptosis-015T015.TCF";
//
//        const auto tileDataGetter00 = new TileDataGetterTCFHT;
//        const auto tileDataGetter01 = new TileDataGetterTCFHT;
//        const auto tileDataGetter02 = new TileDataGetterTCFHT;
//        const auto tileDataGetter03 = new TileDataGetterTCFHT;
//        const auto tileDataGetter04 = new TileDataGetterTCFHT;
//        const auto tileDataGetter05 = new TileDataGetterTCFHT;
//        const auto tileDataGetter06 = new TileDataGetterTCFHT;
//        const auto tileDataGetter07 = new TileDataGetterTCFHT;
//        const auto tileDataGetter08 = new TileDataGetterTCFHT;
//        const auto tileDataGetter09 = new TileDataGetterTCFHT;
//        const auto tileDataGetter10 = new TileDataGetterTCFHT;
//        const auto tileDataGetter11 = new TileDataGetterTCFHT;
//        const auto tileDataGetter12 = new TileDataGetterTCFHT;
//        const auto tileDataGetter13 = new TileDataGetterTCFHT;
//        const auto tileDataGetter14 = new TileDataGetterTCFHT;
//
//
//        tileDataGetter00->SetTcfFilePath(tcfFilePath00);
//        tileDataGetter01->SetTcfFilePath(tcfFilePath01);
//        tileDataGetter02->SetTcfFilePath(tcfFilePath02);
//        tileDataGetter03->SetTcfFilePath(tcfFilePath03);
//        tileDataGetter04->SetTcfFilePath(tcfFilePath04);
//        tileDataGetter05->SetTcfFilePath(tcfFilePath05);
//        tileDataGetter06->SetTcfFilePath(tcfFilePath06);
//        tileDataGetter07->SetTcfFilePath(tcfFilePath07);
//        tileDataGetter08->SetTcfFilePath(tcfFilePath08);
//        tileDataGetter09->SetTcfFilePath(tcfFilePath09);
//        tileDataGetter10->SetTcfFilePath(tcfFilePath10);
//        tileDataGetter11->SetTcfFilePath(tcfFilePath11);
//        tileDataGetter12->SetTcfFilePath(tcfFilePath12);
//        tileDataGetter13->SetTcfFilePath(tcfFilePath13);
//        tileDataGetter14->SetTcfFilePath(tcfFilePath14);
//
//        tileDataGetter00->SetTimeFrameIndex(0);
//        tileDataGetter01->SetTimeFrameIndex(0);
//        tileDataGetter02->SetTimeFrameIndex(0);
//        tileDataGetter03->SetTimeFrameIndex(0);
//        tileDataGetter04->SetTimeFrameIndex(0);
//        tileDataGetter05->SetTimeFrameIndex(0);
//        tileDataGetter06->SetTimeFrameIndex(0);
//        tileDataGetter07->SetTimeFrameIndex(0);
//        tileDataGetter08->SetTimeFrameIndex(0);
//        tileDataGetter09->SetTimeFrameIndex(0);
//        tileDataGetter10->SetTimeFrameIndex(0);
//        tileDataGetter11->SetTimeFrameIndex(0);
//        tileDataGetter12->SetTimeFrameIndex(0);
//        tileDataGetter13->SetTimeFrameIndex(0);
//        tileDataGetter14->SetTimeFrameIndex(0);
//
//        const auto tileDataGetterPointer00 = ITileDataGetter::Pointer{ tileDataGetter00 };
//        const auto tileDataGetterPointer01 = ITileDataGetter::Pointer{ tileDataGetter01 };
//        const auto tileDataGetterPointer02 = ITileDataGetter::Pointer{ tileDataGetter02 };
//        const auto tileDataGetterPointer03 = ITileDataGetter::Pointer{ tileDataGetter03 };
//        const auto tileDataGetterPointer04 = ITileDataGetter::Pointer{ tileDataGetter04 };
//        const auto tileDataGetterPointer05 = ITileDataGetter::Pointer{ tileDataGetter05 };
//        const auto tileDataGetterPointer06 = ITileDataGetter::Pointer{ tileDataGetter06 };
//        const auto tileDataGetterPointer07 = ITileDataGetter::Pointer{ tileDataGetter07 };
//        const auto tileDataGetterPointer08 = ITileDataGetter::Pointer{ tileDataGetter08 };
//        const auto tileDataGetterPointer09 = ITileDataGetter::Pointer{ tileDataGetter09 };
//        const auto tileDataGetterPointer10 = ITileDataGetter::Pointer{ tileDataGetter10 };
//        const auto tileDataGetterPointer11 = ITileDataGetter::Pointer{ tileDataGetter11 };
//        const auto tileDataGetterPointer12 = ITileDataGetter::Pointer{ tileDataGetter12 };
//        const auto tileDataGetterPointer13 = ITileDataGetter::Pointer{ tileDataGetter13 };
//        const auto tileDataGetterPointer14 = ITileDataGetter::Pointer{ tileDataGetter14 };
//
//        QMap<QString, ITileDataGetter::Pointer> tileDataGetterMap;
//        tileDataGetterMap[tcfFilePath00] = tileDataGetterPointer00;
//        tileDataGetterMap[tcfFilePath01] = tileDataGetterPointer01;
//        tileDataGetterMap[tcfFilePath02] = tileDataGetterPointer02;
//        tileDataGetterMap[tcfFilePath03] = tileDataGetterPointer03;
//        tileDataGetterMap[tcfFilePath04] = tileDataGetterPointer04;
//        tileDataGetterMap[tcfFilePath05] = tileDataGetterPointer05;
//        tileDataGetterMap[tcfFilePath06] = tileDataGetterPointer06;
//        tileDataGetterMap[tcfFilePath07] = tileDataGetterPointer07;
//        tileDataGetterMap[tcfFilePath08] = tileDataGetterPointer08;
//        tileDataGetterMap[tcfFilePath09] = tileDataGetterPointer09;
//        tileDataGetterMap[tcfFilePath10] = tileDataGetterPointer10;
//        tileDataGetterMap[tcfFilePath11] = tileDataGetterPointer11;
//        tileDataGetterMap[tcfFilePath12] = tileDataGetterPointer12;
//        tileDataGetterMap[tcfFilePath13] = tileDataGetterPointer13;
//        tileDataGetterMap[tcfFilePath14] = tileDataGetterPointer14;
//
//
//        const QStringList tcfFilePathList{
//            tcfFilePath00,
//            tcfFilePath01,
//            tcfFilePath02,
//            tcfFilePath03,
//            tcfFilePath04,
//            tcfFilePath05,
//            tcfFilePath06,
//            tcfFilePath07,
//            tcfFilePath08,
//            tcfFilePath09,
//            tcfFilePath10,
//            tcfFilePath11,
//            tcfFilePath12,
//            tcfFilePath13,
//            tcfFilePath14
//        };
//
//        TileConfigurationReaderTCFHT tileConfigurationReaderTCFHT;
//        tileConfigurationReaderTCFHT.SetTileTCFFilePathList(tcfFilePathList);
//        tileConfigurationReaderTCFHT.Read();
//
//        const auto tileConfiguration = tileConfigurationReaderTCFHT.GetTileConfiguration();
//
//        TileSetGeneratorTCF tileSetGeneratorTCF;
//        tileSetGeneratorTCF.SetTcfFilePathList(tcfFilePathList);
//        tileSetGeneratorTCF.SetTileConfiguration(tileConfiguration);
//        tileSetGeneratorTCF.SetTileDataGetterMap(tileDataGetterMap);
//
//        const auto tileSet = tileSetGeneratorTCF.Generate();
//
//        OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
//        overlapRelationCalculatorPhaseCorrelation.SetTileConfiguration(tileConfiguration);
//        overlapRelationCalculatorPhaseCorrelation.SetTileSet(tileSet);
//        const auto overlapRelationSet = overlapRelationCalculatorPhaseCorrelation.Calculate();
//
//        TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
//        tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
//        tilePositionCalculatorBestOverlap.SetOverlapRelationSet(overlapRelationSet);
//        const auto tilePositionSet = tilePositionCalculatorBestOverlap.Calculate();
//
//        StitchingWriterHDF5 stitchingWriterHdf5;
//        stitchingWriterHdf5.SetTileSet(tileSet);
//        stitchingWriterHdf5.SetTilePositionSet(tilePositionSet);
//        stitchingWriterHdf5.SetTileConfiguration(tileConfiguration);
//        stitchingWriterHdf5.SetHDF5FilePath("test3x5.h5");
//        stitchingWriterHdf5.Run();
//
//        auto end = std::chrono::system_clock::now();
//
//        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
//
//        std::cout << duration.count() << "millisec" << std::endl;
//
//        const auto writerResult = stitchingWriterHdf5.GetStitchingWriterResult();
//        CHECK(writerResult.GetSuccessFlag() == true);
//    }
//}
