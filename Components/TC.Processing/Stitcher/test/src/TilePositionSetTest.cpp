#include <catch2/catch.hpp>

#include "TilePositionSet.h"

namespace TilePositionSetTest {
    TEST_CASE("TilePositionSet") {
        SECTION("TilePositionSet()") {
            TilePositionSet tilePositionSet;
            CHECK(&tilePositionSet != nullptr);
        }
        SECTION("TilePositionSet(other)") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);
            
            TilePositionSet srcTilePositionSet;
            srcTilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            srcTilePositionSet.InsertTilePosition(0, 0, tilePosition);

            TilePositionSet destTilePositionSet(srcTilePositionSet);
            const auto resultTilePosition = destTilePositionSet.GetTilePosition(0, 0);
            
            CHECK(resultTilePosition.GetTilePositionX() == 1.f);
            CHECK(resultTilePosition.GetTilePositionY() == 2.f);
            CHECK(resultTilePosition.GetTilePositionZ() == 3.f);
        }
        SECTION("operator=()") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);

            TilePositionSet srcTilePositionSet;
            srcTilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            srcTilePositionSet.InsertTilePosition(0, 0, tilePosition);

            TilePositionSet destTilePositionSet(srcTilePositionSet);
            const auto resultTilePosition = destTilePositionSet.GetTilePosition(0, 0);

            CHECK(resultTilePosition.GetTilePositionX() == 1.f);
            CHECK(resultTilePosition.GetTilePositionY() == 2.f);
            CHECK(resultTilePosition.GetTilePositionZ() == 3.f);
        }
        SECTION("SetTileNumber()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            CHECK(&tilePositionSet != nullptr);
        }
        SECTION("GetTileNumberX()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 2);
            CHECK(tilePositionSet.GetTileNumberX() == 1);
        }
        SECTION("GetTileNumberY()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 2);
            CHECK(tilePositionSet.GetTileNumberY() == 2);
        }
        SECTION("InsertTilePosition()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.InsertTilePosition(0, 0, TilePosition{});
            CHECK(&tilePositionSet != nullptr);
        }
        SECTION("GetTilePosition()") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            const auto resultTilePosition = tilePositionSet.GetTilePosition(0, 0);

            CHECK(resultTilePosition.GetTilePositionX() == 1.f);
            CHECK(resultTilePosition.GetTilePositionY() == 2.f);
            CHECK(resultTilePosition.GetTilePositionZ() == 3.f);
        }
    }
}