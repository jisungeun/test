#include <catch2/catch.hpp>
#include <QMap>

#include "StitchingWriterHDF5HT.h"

#include "OverlapRelationCalculatorPhaseCorrelation.h"
#include "TilePositionCalculatorBestOverlap.h"

namespace StitchingWriterHDF5Test {
    const QString testTopFolderPath = TEST_DATA_FOLDR_PATH;
    const QString tileDataFolderPath = testTopFolderPath + "/StitchingData/Cell3";

    TEST_CASE("StitchingWriterHDF5 : unit test") {
        SECTION("StitchingWriterHDF5()") {
            StitchingWriterHDF5HT stitchingWriterHdf5HT;
            CHECK(&stitchingWriterHdf5HT != nullptr);
        }
        SECTION("SetTileSet()") {
            StitchingWriterHDF5HT stitchingWriterHdf5HT;
            stitchingWriterHdf5HT.SetTileSet({});
            CHECK(&stitchingWriterHdf5HT != nullptr);
        }
        SECTION("SetTilePositionSet()") {
            StitchingWriterHDF5HT stitchingWriterHdf5HT;
            stitchingWriterHdf5HT.SetTilePositionSet({});
            CHECK(&stitchingWriterHdf5HT != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            StitchingWriterHDF5HT stitchingWriterHdf5HT;
            stitchingWriterHdf5HT.SetTileConfiguration({});
            CHECK(&stitchingWriterHdf5HT != nullptr);
        }
        SECTION("SetHDF5FilePath()") {
            StitchingWriterHDF5HT stitchingWriterHdf5HT;
            stitchingWriterHdf5HT.SetHDF5FilePath("");
            CHECK(&stitchingWriterHdf5HT != nullptr);
        }
        SECTION("Run()") {
            //TODO Write test code without using TCF Files
        }
        SECTION("GetStitchingWriterResult()") {
            //TODO Write test code without using TCF Files
        }
    }

    TEST_CASE("StitchingWriterHDF5 : practical test") {
        //TODO Write test code without using TCF Files
    }
}