#include <catch2/catch.hpp>

#include "TilePosition.h"

namespace TilePositionTest {
    TEST_CASE("TilePosition") {
        SECTION("TilePosition()") {
            TilePosition tilePosition;
            CHECK(&tilePosition != nullptr);
        }
        SECTION("TilePosition(other)") {
            TilePosition srcTilePosition;
            srcTilePosition.SetPositions(1.f, 2.f, 3.f);

            TilePosition destTilePosition(srcTilePosition);
            CHECK(destTilePosition.GetTilePositionX() == 1.f);
            CHECK(destTilePosition.GetTilePositionY() == 2.f);
            CHECK(destTilePosition.GetTilePositionZ() == 3.f);
        }
        SECTION("operator=()") {
            TilePosition srcTilePosition;
            srcTilePosition.SetPositions(1.f, 2.f, 3.f);

            TilePosition destTilePosition;
            destTilePosition = srcTilePosition;
            CHECK(destTilePosition.GetTilePositionX() == 1.f);
            CHECK(destTilePosition.GetTilePositionY() == 2.f);
            CHECK(destTilePosition.GetTilePositionZ() == 3.f);
        }
        SECTION("SetPositions()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);
            CHECK(&tilePosition != nullptr);
        }
        SECTION("GetTilePositionX()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);
            CHECK(tilePosition.GetTilePositionX() == 1.f);
        }
        SECTION("GetTilePositionY()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);
            CHECK(tilePosition.GetTilePositionY() == 2.f);
        }
        SECTION("GetTilePositionZ()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1.f, 2.f, 3.f);
            CHECK(tilePosition.GetTilePositionZ() == 3.f);
        }
    }
}