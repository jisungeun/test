#include <catch2/catch.hpp>

#include "TileConfiguration.h"

namespace TileConfigurationTest {
    TEST_CASE("TileConfiguration") {
        SECTION("TileConfiguration()") {
            TileConfiguration tileConfiguration;
            CHECK(&tileConfiguration != nullptr);
        }
        SECTION("TileConfiguration(other)") {
            TileConfiguration srcTileConfiguration;
            srcTileConfiguration.SetTileNumber(1, 2);

            TileConfiguration destTileConfiguration(srcTileConfiguration);
            CHECK(destTileConfiguration.GetTileNumberX() == 1);
            CHECK(destTileConfiguration.GetTileNumberY() == 2);
        }
        SECTION("operator=(other)") {
            TileConfiguration srcTileConfiguration;
            srcTileConfiguration.SetTileNumber(1, 2);

            TileConfiguration destTileConfiguration;
            destTileConfiguration = srcTileConfiguration;
            CHECK(destTileConfiguration.GetTileNumberX() == 1);
            CHECK(destTileConfiguration.GetTileNumberY() == 2);
        }
        SECTION("SetTileNumber()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(0, 0);
            CHECK(&tileConfiguration != nullptr);
        }
        SECTION("SetTileSizeInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(0, 0, 0);
            CHECK(&tileConfiguration != nullptr);
        }
        SECTION("SetOverlapLengthInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetOverlapLengthInPixel(0, 0);
            CHECK(&tileConfiguration != nullptr);
        }
        SECTION("GetTileNumberX()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(1, 2);
            CHECK(tileConfiguration.GetTileNumberX() == 1);
        }
        SECTION("GetTileNumberY()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(1, 2);
            CHECK(tileConfiguration.GetTileNumberY() == 2);
        }
        SECTION("GetTileSizeXInPixel()"){
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(1, 2, 3);
            CHECK(tileConfiguration.GetTileSizeXInPixel() == 1);
        }
        SECTION("GetTileSizeYInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(1, 2, 3);
            CHECK(tileConfiguration.GetTileSizeYInPixel() == 2);
        }
        SECTION("GetTileSizeZInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(1, 2, 3);
            CHECK(tileConfiguration.GetTileSizeZInPixel() == 3);
        }
        SECTION("GetOverlapLengthXInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetOverlapLengthInPixel(1, 2);
            CHECK(tileConfiguration.GetOverlapLengthXInPixel() == 1);
        }
        SECTION("GetOverlapLengthYInPixel()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetOverlapLengthInPixel(1, 2);
            CHECK(tileConfiguration.GetOverlapLengthYInPixel() == 2);
        }
    }

}