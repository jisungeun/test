#include <catch2/catch.hpp>

#include "ITileConfigurationReader.h"

namespace ITileConfigurationReaderTest {
    class TileConfigurationReaderForTest final : public ITileConfigurationReader {
    public:
        TileConfigurationReaderForTest() = default;
        ~TileConfigurationReaderForTest() = default;

        auto Read() -> bool override {
            readTriggered = true;
            return true;
        }
        auto GetTileConfiguration() const -> const TileConfiguration& override {
            getTileConfigurationTriggered = true;
            return tileConfiguration;
        }

        bool readTriggered = false;
        mutable bool getTileConfigurationTriggered = false;
    private:
        TileConfiguration tileConfiguration;
    };

    TEST_CASE("ITileConfigurationReader") {
        SECTION("Read()") {
            TileConfigurationReaderForTest tileConfigurationReader;
            CHECK(tileConfigurationReader.readTriggered == false);
            tileConfigurationReader.Read();
            CHECK(tileConfigurationReader.readTriggered == true);
        }
        SECTION("GetTileConfiguration()") {
            TileConfigurationReaderForTest tileConfigurationReader;
            CHECK(tileConfigurationReader.getTileConfigurationTriggered == false);
            tileConfigurationReader.GetTileConfiguration();
            CHECK(tileConfigurationReader.getTileConfigurationTriggered == true);
        }
    }
}