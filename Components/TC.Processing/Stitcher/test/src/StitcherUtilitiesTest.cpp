#include <catch2/catch.hpp>

#include "StitcherUtilities.h"
#include "StitchingMapGenerator.h"

namespace StitcherUtilitiesTest {
    TEST_CASE("StitcherUtilities : unit test") {
        SECTION("GetMinPosition()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(1.1f, 2.2f, 3.3f);

            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            const auto resultMinPosition = GetMinPosition(tilePositionSet);

            CHECK(resultMinPosition.GetTilePositionX() == 1.1f);
            CHECK(resultMinPosition.GetTilePositionY() == 2.2f);
            CHECK(resultMinPosition.GetTilePositionZ() == 3.3f);
        }
        SECTION("GetMaxPosition()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(1.1f, 2.2f, 3.3f);

            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            const auto resultMaxPosition = GetMaxPosition(tilePositionSet);

            CHECK(resultMaxPosition.GetTilePositionX() == 1.1f);
            CHECK(resultMaxPosition.GetTilePositionY() == 2.2f);
            CHECK(resultMaxPosition.GetTilePositionZ() == 3.3f);
        }
        SECTION("GetMinMaxPosition()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(1.1f, 2.2f, 3.3f);

            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            const auto [resultMinPosition, resultMaxPosition] = GetMinMaxPosition(tilePositionSet);

            CHECK(resultMinPosition.GetTilePositionX() == 1.1f);
            CHECK(resultMinPosition.GetTilePositionY() == 2.2f);
            CHECK(resultMinPosition.GetTilePositionZ() == 3.3f);

            CHECK(resultMaxPosition.GetTilePositionX() == 1.1f);
            CHECK(resultMaxPosition.GetTilePositionY() == 2.2f);
            CHECK(resultMaxPosition.GetTilePositionZ() == 3.3f);
        }
        SECTION("GetBoundaryLength()") {
            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(1.1f, 2.2f, 3.3f);

            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            const auto resultBoundaryLength = GetBoundaryLength(tilePositionSet);

            CHECK(resultBoundaryLength.left == 0);
            CHECK(resultBoundaryLength.right == 0);
            CHECK(resultBoundaryLength.up == 0);
            CHECK(resultBoundaryLength.down == 0);
        }
        SECTION("CropBoundary(map, length)") {
            constexpr auto tileNumberX = 1;
            constexpr auto tileNumberY = 1;

            constexpr auto stitchingTileNumberX = 2 * tileNumberX - 1;
            constexpr auto stitchingTileNumberY = 2 * tileNumberY - 1;

            constexpr auto tileSizeX = 11;
            constexpr auto tileSizeY = 13;
            constexpr auto tileSizeZ = 5;

            constexpr auto centerPositionX = 5;
            constexpr auto centerPositionY = 5;

            IndexRange indexRangeToDataTile;
            indexRangeToDataTile.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            IndexRange indexRangeToStitchingTile;
            indexRangeToStitchingTile.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(tileNumberX, tileNumberY);
            stitchingTile.AddLayer(0, 0, indexRangeToDataTile, indexRangeToStitchingTile);

            IndexRange indexRange;
            indexRange.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(stitchingTileNumberX, stitchingTileNumberY);
            stitchingMap.SetStitchingTile(0, stitchingTile);
            stitchingMap.SetIndexRange(0, indexRange);
            stitchingMap.SetStitchedDataSize(tileSizeX, tileSizeY, tileSizeZ);
            stitchingMap.SetCenterPosition(centerPositionX, centerPositionY);

            constexpr auto cropLeftLength = 1;
            constexpr auto cropRightLength = 2;
            constexpr auto cropUpLength = 2;
            constexpr auto cropDownLength = 1;

            const auto resultStitchingMap = CropBoundary(stitchingMap, BoundaryLength{ cropLeftLength, cropRightLength, cropUpLength, cropDownLength });

            CHECK(resultStitchingMap.GetStitchingTileNumberX() == stitchingTileNumberX);
            CHECK(resultStitchingMap.GetStitchingTileNumberY() == stitchingTileNumberY);

            const auto resultStitchingTile = resultStitchingMap.GetStitchingTile(0, 0);

            CHECK(resultStitchingTile.GetLayerDataTileIndexXYList().size() == 1);
            CHECK(std::get<0>(resultStitchingTile.GetLayerDataTileIndexXYList().at(0)) == 0);
            CHECK(std::get<1>(resultStitchingTile.GetLayerDataTileIndexXYList().at(0)) == 0);

            const auto resultIndexRangeToDataTile = resultStitchingTile.GetIndexRangeToDataTile(0, 0);
            CHECK(resultIndexRangeToDataTile.GetX0() == 1);
            CHECK(resultIndexRangeToDataTile.GetX1() == 8);
            CHECK(resultIndexRangeToDataTile.GetY0() == 2);
            CHECK(resultIndexRangeToDataTile.GetY1() == 11);
            CHECK(resultIndexRangeToDataTile.GetZ0() == 0);
            CHECK(resultIndexRangeToDataTile.GetZ1() == tileSizeZ - 1);

            const auto resultIndexRangeToStitchingTile = resultStitchingTile.GetIndexRangeToStitchingTile(0, 0);
            CHECK(resultIndexRangeToStitchingTile.GetX0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetX1() == 7);
            CHECK(resultIndexRangeToStitchingTile.GetY0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetY1() == 9);
            CHECK(resultIndexRangeToStitchingTile.GetZ0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetZ1() == tileSizeZ - 1);

            CHECK(resultStitchingMap.GetStitchedDataSizeX() == 8);
            CHECK(resultStitchingMap.GetStitchedDataSizeY() == 10);
            CHECK(resultStitchingMap.GetStitchedDataSizeZ() == 5);

            CHECK(resultStitchingMap.GetCenterPositionX() == centerPositionX - cropLeftLength);
            CHECK(resultStitchingMap.GetCenterPositionY() == centerPositionY - cropUpLength);
        }
        SECTION("CropBoundary(map, size)"){
            constexpr auto tileNumberX = 1;
            constexpr auto tileNumberY = 1;

            constexpr auto stitchingTileNumberX = 2 * tileNumberX - 1;
            constexpr auto stitchingTileNumberY = 2 * tileNumberY - 1;

            constexpr auto tileSizeX = 11;
            constexpr auto tileSizeY = 13;
            constexpr auto tileSizeZ = 5;

            constexpr auto centerPositionX = 5;
            constexpr auto centerPositionY = 5;

            IndexRange indexRangeToDataTile;
            indexRangeToDataTile.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            IndexRange indexRangeToStitchingTile;
            indexRangeToStitchingTile.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(tileNumberX, tileNumberY);
            stitchingTile.AddLayer(0, 0, indexRangeToDataTile, indexRangeToStitchingTile);

            IndexRange indexRange;
            indexRange.SetRange(0, tileSizeX - 1, 0, tileSizeY - 1, 0, tileSizeZ - 1);

            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(stitchingTileNumberX, stitchingTileNumberY);
            stitchingMap.SetStitchingTile(0, stitchingTile);
            stitchingMap.SetIndexRange(0, indexRange);
            stitchingMap.SetStitchedDataSize(tileSizeX, tileSizeY, tileSizeZ);
            stitchingMap.SetCenterPosition(centerPositionX, centerPositionY);

            constexpr auto targetSizeX = 8;
            constexpr auto targetSizeY = 9;

            const auto resultStitchingMap = CropBoundary(stitchingMap, TargetSize{ targetSizeX,targetSizeY });

            CHECK(resultStitchingMap.GetStitchingTileNumberX() == stitchingTileNumberX);
            CHECK(resultStitchingMap.GetStitchingTileNumberY() == stitchingTileNumberY);

            const auto resultStitchingTile = resultStitchingMap.GetStitchingTile(0, 0);

            CHECK(resultStitchingTile.GetLayerDataTileIndexXYList().size() == 1);
            CHECK(std::get<0>(resultStitchingTile.GetLayerDataTileIndexXYList().at(0)) == 0);
            CHECK(std::get<1>(resultStitchingTile.GetLayerDataTileIndexXYList().at(0)) == 0);

            const auto resultIndexRangeToDataTile = resultStitchingTile.GetIndexRangeToDataTile(0, 0);
            CHECK(resultIndexRangeToDataTile.GetX0() == 1);
            CHECK(resultIndexRangeToDataTile.GetX1() == 8);
            CHECK(resultIndexRangeToDataTile.GetY0() == 2);
            CHECK(resultIndexRangeToDataTile.GetY1() == 10);
            CHECK(resultIndexRangeToDataTile.GetZ0() == 0);
            CHECK(resultIndexRangeToDataTile.GetZ1() == tileSizeZ - 1);

            const auto resultIndexRangeToStitchingTile = resultStitchingTile.GetIndexRangeToStitchingTile(0, 0);
            CHECK(resultIndexRangeToStitchingTile.GetX0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetX1() == 7);
            CHECK(resultIndexRangeToStitchingTile.GetY0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetY1() == 8);
            CHECK(resultIndexRangeToStitchingTile.GetZ0() == 0);
            CHECK(resultIndexRangeToStitchingTile.GetZ1() == tileSizeZ - 1);

            CHECK(resultStitchingMap.GetStitchedDataSizeX() == 8);
            CHECK(resultStitchingMap.GetStitchedDataSizeY() == 9);
            CHECK(resultStitchingMap.GetStitchedDataSizeZ() == 5);

            CHECK(resultStitchingMap.GetCenterPositionX() == centerPositionX - 1);
            CHECK(resultStitchingMap.GetCenterPositionY() == centerPositionY - 1);
        }
    }

    struct AnswerIndexRange {
        int32_t x0{};
        int32_t x1{};
        int32_t y0{};
        int32_t y1{};
        int32_t z0{};
        int32_t z1{};
    };
    auto CompareIndexRange(const IndexRange& indexRange, const AnswerIndexRange& answer) ->bool {
        if (indexRange.GetX0() != answer.x0) { return false; }
        if (indexRange.GetX1() != answer.x1) { return false; }
        if (indexRange.GetY0() != answer.y0) { return false; }
        if (indexRange.GetY1() != answer.y1) { return false; }
        if (indexRange.GetZ0() != answer.z0) { return false; }
        if (indexRange.GetZ1() != answer.z1) { return false; }
        return true;
    }

    TEST_CASE("StitcherUtilities : practical test") {
        SECTION("GetMinPosition()") {
            SECTION("2x2") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(2, 2);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition01;
                TilePosition tilePosition11;

                tilePosition00.SetPositions(-1, 2, 1);
                tilePosition10.SetPositions(80, 4, 2);
                tilePosition01.SetPositions(-2, 82, -1);
                tilePosition11.SetPositions(82, 84, 3);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

                const auto minPosition = GetMinPosition(tilePositionSet);
                CHECK(minPosition.GetTilePositionX() == -2);
                CHECK(minPosition.GetTilePositionY() == 2);
                CHECK(minPosition.GetTilePositionZ() == -1);
            }
            SECTION("3x3") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(3, 3);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition20;
                TilePosition tilePosition01;
                TilePosition tilePosition11;
                TilePosition tilePosition21;
                TilePosition tilePosition02;
                TilePosition tilePosition12;
                TilePosition tilePosition22;

                tilePosition00.SetPositions(-3, 0, 0);
                tilePosition10.SetPositions(55, -1, 1);
                tilePosition20.SetPositions(121, -4, 2);
                tilePosition01.SetPositions(-2, 60, 3);
                tilePosition11.SetPositions(62, 70, -1);
                tilePosition21.SetPositions(125, 65, -2);
                tilePosition02.SetPositions(-4, 124, -3);
                tilePosition12.SetPositions(63, 122, 0);
                tilePosition22.SetPositions(120, 126, 0);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
                tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
                tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
                tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
                tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

                const auto minPosition = GetMinPosition(tilePositionSet);
                CHECK(minPosition.GetTilePositionX() == -4);
                CHECK(minPosition.GetTilePositionY() == -4);
                CHECK(minPosition.GetTilePositionZ() == -3);
            }
        }
        SECTION("GetMaxPosition()") {
            SECTION("2x2") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(2, 2);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition01;
                TilePosition tilePosition11;

                tilePosition00.SetPositions(-1, 2, 1);
                tilePosition10.SetPositions(80, 4, 2);
                tilePosition01.SetPositions(-2, 82, -1);
                tilePosition11.SetPositions(82, 84, 3);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

                const auto maxPosition = GetMaxPosition(tilePositionSet);
                CHECK(maxPosition.GetTilePositionX() == 82);
                CHECK(maxPosition.GetTilePositionY() == 84);
                CHECK(maxPosition.GetTilePositionZ() == 3);
            }
            SECTION("3x3") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(3, 3);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition20;
                TilePosition tilePosition01;
                TilePosition tilePosition11;
                TilePosition tilePosition21;
                TilePosition tilePosition02;
                TilePosition tilePosition12;
                TilePosition tilePosition22;

                tilePosition00.SetPositions(-3, 0, 0);
                tilePosition10.SetPositions(55, -1, 1);
                tilePosition20.SetPositions(121, -4, 2);
                tilePosition01.SetPositions(-2, 60, 3);
                tilePosition11.SetPositions(62, 70, -1);
                tilePosition21.SetPositions(125, 65, -2);
                tilePosition02.SetPositions(-4, 124, -3);
                tilePosition12.SetPositions(63, 122, 0);
                tilePosition22.SetPositions(120, 126, 0);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
                tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
                tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
                tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
                tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

                const auto maxPosition = GetMaxPosition(tilePositionSet);
                CHECK(maxPosition.GetTilePositionX() == 125);
                CHECK(maxPosition.GetTilePositionY() == 126);
                CHECK(maxPosition.GetTilePositionZ() == 3);
            }
        }
        SECTION("GetMinMaxPosition()") {
            SECTION("2x2") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(2, 2);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition01;
                TilePosition tilePosition11;

                tilePosition00.SetPositions(-1, 2, 1);
                tilePosition10.SetPositions(80, 4, 2);
                tilePosition01.SetPositions(-2, 82, -1);
                tilePosition11.SetPositions(82, 84, 3);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

                const auto [minPosition, maxPosition] = GetMinMaxPosition(tilePositionSet);

                CHECK(minPosition.GetTilePositionX() == -2);
                CHECK(minPosition.GetTilePositionY() == 2);
                CHECK(minPosition.GetTilePositionZ() == -1);
                CHECK(maxPosition.GetTilePositionX() == 82);
                CHECK(maxPosition.GetTilePositionY() == 84);
                CHECK(maxPosition.GetTilePositionZ() == 3);
            }
            SECTION("3x3") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(3, 3);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition20;
                TilePosition tilePosition01;
                TilePosition tilePosition11;
                TilePosition tilePosition21;
                TilePosition tilePosition02;
                TilePosition tilePosition12;
                TilePosition tilePosition22;

                tilePosition00.SetPositions(-3, 0, 0);
                tilePosition10.SetPositions(55, -1, 1);
                tilePosition20.SetPositions(121, -4, 2);
                tilePosition01.SetPositions(-2, 60, 3);
                tilePosition11.SetPositions(62, 70, -1);
                tilePosition21.SetPositions(125, 65, -2);
                tilePosition02.SetPositions(-4, 124, -3);
                tilePosition12.SetPositions(63, 122, 0);
                tilePosition22.SetPositions(120, 126, 0);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
                tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
                tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
                tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
                tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

                const auto [minPosition, maxPosition] = GetMinMaxPosition(tilePositionSet);

                CHECK(minPosition.GetTilePositionX() == -4);
                CHECK(minPosition.GetTilePositionY() == -4);
                CHECK(minPosition.GetTilePositionZ() == -3);
                CHECK(maxPosition.GetTilePositionX() == 125);
                CHECK(maxPosition.GetTilePositionY() == 126);
                CHECK(maxPosition.GetTilePositionZ() == 3);
            }
        }
        SECTION("GetBoundaryLength()") {
            SECTION("2x2") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(2, 2);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition01;
                TilePosition tilePosition11;

                tilePosition00.SetPositions(-1, 2, 1);
                tilePosition10.SetPositions(80, 4, 2);
                tilePosition01.SetPositions(-2, 82, -1);
                tilePosition11.SetPositions(82, 84, 3);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

                const auto [left, right, up, down] = GetBoundaryLength(tilePositionSet);
                CHECK(left == 1);
                CHECK(right == 2);
                CHECK(up == 2);
                CHECK(down == 2);
            }
            SECTION("3x3") {
                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(3, 3);

                TilePosition tilePosition00;
                TilePosition tilePosition10;
                TilePosition tilePosition20;
                TilePosition tilePosition01;
                TilePosition tilePosition11;
                TilePosition tilePosition21;
                TilePosition tilePosition02;
                TilePosition tilePosition12;
                TilePosition tilePosition22;

                tilePosition00.SetPositions(-3, 0, 0);
                tilePosition10.SetPositions(55, -1, 1);
                tilePosition20.SetPositions(121, -4, 2);
                tilePosition01.SetPositions(-2, 60, 3);
                tilePosition11.SetPositions(62, 70, -1);
                tilePosition21.SetPositions(125, 65, -2);
                tilePosition02.SetPositions(-4, 124, -3);
                tilePosition12.SetPositions(63, 122, 0);
                tilePosition22.SetPositions(120, 126, 0);

                tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
                tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
                tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
                tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
                tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
                tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

                const auto [left, right, up, down] = GetBoundaryLength(tilePositionSet);
                CHECK(left == 2);
                CHECK(right == 5);
                CHECK(up == 4);
                CHECK(down == 4);
            }
        }
        SECTION("CropBoundary(map, length)") {
            SECTION("2x2") {
                //TODO Implement test
                //constexpr auto tileNumberX = 2;
                //constexpr auto tileNumberY = 2;

                //TilePositionSet tilePositionSet;
                //tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);

                //TilePosition tilePosition00;
                //TilePosition tilePosition10;
                //TilePosition tilePosition01;
                //TilePosition tilePosition11;

                //tilePosition00.SetPositions(-1, 2, 0);
                //tilePosition10.SetPositions(80, 4, 0);
                //tilePosition01.SetPositions(-2, 80, 0);
                //tilePosition11.SetPositions(82, 84, 0);

                //tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                //tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                //tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                //tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

                //TileConfiguration tileConfiguration;
                //tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
                //tileConfiguration.SetOverlapLengthInPixel(20, 20);
                //tileConfiguration.SetTileSizeInPixel(100, 100, 20);

                //StitchingMapGenerator generator;
                //generator.SetDataTilePositionSet(tilePositionSet);
                //generator.SetTileConfiguration(tileConfiguration);

                //generator.GenerateMap();

                //const auto stitchingMap = generator.GetStitchingMap();

                //const auto croppedStitchingMap = CropBoundary(stitchingMap, BoundaryLength{ 5,5,5,5 });
                //{
                //    const auto stitchingTileNumberX = croppedStitchingMap.GetStitchingTileNumberX();
                //    const auto stitchingTileNumberY = croppedStitchingMap.GetStitchingTileNumberY();

                //    CHECK(stitchingTileNumberX == 3);
                //    CHECK(stitchingTileNumberY == 3);

                //    const auto stitchingTile00 = croppedStitchingMap.GetStitchingTile(0, 0);
                //    const auto stitchingTile10 = croppedStitchingMap.GetStitchingTile(1, 0);
                //    const auto stitchingTile20 = croppedStitchingMap.GetStitchingTile(2, 0);
                //    const auto stitchingTile01 = croppedStitchingMap.GetStitchingTile(0, 1);
                //    const auto stitchingTile11 = croppedStitchingMap.GetStitchingTile(1, 1);
                //    const auto stitchingTile21 = croppedStitchingMap.GetStitchingTile(2, 1);
                //    const auto stitchingTile02 = croppedStitchingMap.GetStitchingTile(0, 2);
                //    const auto stitchingTile12 = croppedStitchingMap.GetStitchingTile(1, 2);
                //    const auto stitchingTile22 = croppedStitchingMap.GetStitchingTile(2, 2);



                //    const auto indexRange00 = croppedStitchingMap.GetIndexRange(0, 0);
                //    const auto indexRange10 = croppedStitchingMap.GetIndexRange(1, 0);
                //    const auto indexRange20 = croppedStitchingMap.GetIndexRange(2, 0);
                //    const auto indexRange01 = croppedStitchingMap.GetIndexRange(0, 1);
                //    const auto indexRange11 = croppedStitchingMap.GetIndexRange(1, 1);
                //    const auto indexRange21 = croppedStitchingMap.GetIndexRange(2, 1);
                //    const auto indexRange02 = croppedStitchingMap.GetIndexRange(0, 2);
                //    const auto indexRange12 = croppedStitchingMap.GetIndexRange(1, 2);
                //    const auto indexRange22 = croppedStitchingMap.GetIndexRange(2, 2);

                //    CHECK(CompareIndexRange(indexRange00, { 0,75,0,72,0,19 }));
                //    CHECK(CompareIndexRange(indexRange10, { 76,94,73,96,0,19 }));
                //    CHECK(CompareIndexRange(indexRange20, { 95,172,97,171,0,19 }));
                //    CHECK(CompareIndexRange(indexRange01, { 0,75,0,72,0,19 }));
                //    CHECK(CompareIndexRange(indexRange11, { 76,94,73,96,0,19 }));
                //    CHECK(CompareIndexRange(indexRange21, { 95,172,97,171,0,19 }));
                //    CHECK(CompareIndexRange(indexRange02, { 0,75,0,72,0,19 }));
                //    CHECK(CompareIndexRange(indexRange12, { 76,94,73,96,0,19 }));
                //    CHECK(CompareIndexRange(indexRange22, { 95,172,97,171,0,19 }));

                //    const auto stitchedDataSizeX = croppedStitchingMap.GetStitchedDataSizeX();
                //    const auto stitchedDataSizeY = croppedStitchingMap.GetStitchedDataSizeY();
                //    const auto stitchedDataSizeZ = croppedStitchingMap.GetStitchedDataSizeZ();

                //    const auto centerPositionX = croppedStitchingMap.GetCenterPositionX();
                //    const auto centerPositionY = croppedStitchingMap.GetCenterPositionY();
                //}

            }
            SECTION("3x3") {
                //TODO Implement test
                //TilePositionSet tilePositionSet;
                //tilePositionSet.SetTileNumber(3, 3);

                //TilePosition tilePosition00;
                //TilePosition tilePosition10;
                //TilePosition tilePosition20;
                //TilePosition tilePosition01;
                //TilePosition tilePosition11;
                //TilePosition tilePosition21;
                //TilePosition tilePosition02;
                //TilePosition tilePosition12;
                //TilePosition tilePosition22;

                //tilePosition00.SetPositions(-3, 0, 0);
                //tilePosition10.SetPositions(55, -1, 1);
                //tilePosition20.SetPositions(121, -4, 2);
                //tilePosition01.SetPositions(-2, 60, 3);
                //tilePosition11.SetPositions(62, 70, -1);
                //tilePosition21.SetPositions(125, 65, -2);
                //tilePosition02.SetPositions(-4, 124, -3);
                //tilePosition12.SetPositions(63, 122, 0);
                //tilePosition22.SetPositions(120, 126, 0);

                //tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
                //tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
                //tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
                //tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
                //tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
                //tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
                //tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
                //tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
                //tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

                //const auto [left, right, up, down] = GetBoundaryLength(tilePositionSet);
                //CHECK(left == 2);
                //CHECK(right == 5);
                //CHECK(up == 4);
                //CHECK(down == 4);
            }
        }
        SECTION("CropBoundary(map, size)") {}
    }
}