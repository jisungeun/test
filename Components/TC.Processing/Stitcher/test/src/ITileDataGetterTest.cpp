#include <catch2/catch.hpp>

#include "ITileDataGetter.h"

namespace ITileDataGetterTest {
    class TileDataGetterForTest final : public ITileDataGetter {
    public:
        TileDataGetterForTest() = default;
        ~TileDataGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            getData1Triggered = true;
            return nullptr;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, 
            const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> override {
            getData2Triggered = true;
            return nullptr;
        }

        mutable bool getData1Triggered{ false };
        mutable bool getData2Triggered{ false };
    };

    TEST_CASE("ITileDataGetter") {
        SECTION("GetData()") {
            TileDataGetterForTest tileDataGetter;
            CHECK(tileDataGetter.getData1Triggered == false);
            tileDataGetter.GetData();
            CHECK(tileDataGetter.getData1Triggered == true);
        }
        SECTION("GetData(x0, x1, y0, y1, z0, z1)") {
            TileDataGetterForTest tileDataGetter;
            CHECK(tileDataGetter.getData2Triggered == false);
            tileDataGetter.GetData(0, 0, 0, 0, 0, 0);
            CHECK(tileDataGetter.getData2Triggered == true);
        }
    }
}