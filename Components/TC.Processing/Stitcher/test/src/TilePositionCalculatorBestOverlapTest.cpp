#include <iostream>
#include <catch2/catch.hpp>

#include "TilePositionCalculatorBestOverlap.h"

namespace TilePositionCalculatorBestOverlapTest {
    TEST_CASE("TilePositionCalculatorBestOverlap : unit test") {
        SECTION("TilePositionCalculatorBestOverlap") {
            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            CHECK(&tilePositionCalculatorBestOverlap != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetTileConfiguration({});
            CHECK(&tilePositionCalculatorBestOverlap != nullptr);
        }
        SECTION("SetOverlapRelationSet()") {
            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetOverlapRelationSet({});
            CHECK(&tilePositionCalculatorBestOverlap != nullptr);
        }
        SECTION("Calculate") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileSizeInPixel(100, 100, 20);
            
            OverlapRelation overlapRelation10;
            OverlapRelation overlapRelation01;
            OverlapRelation overlapRelation21;
            OverlapRelation overlapRelation12;

            overlapRelation10.SetShiftValue(1, 1, 0);
            overlapRelation01.SetShiftValue(2, 2, 0);
            overlapRelation21.SetShiftValue(3, 3, 0);
            overlapRelation12.SetShiftValue(4, 4, 0);
            overlapRelation10.SetReliability(0.4f);
            overlapRelation01.SetReliability(0.5f);
            overlapRelation21.SetReliability(0.9f);
            overlapRelation12.SetReliability(0.7f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation10);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation01);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation21);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation12);

            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
            tilePositionCalculatorBestOverlap.SetOverlapRelationSet(overlapRelationSet);

            const auto resultTilePositionSet = tilePositionCalculatorBestOverlap.Calculate();

            const auto tilePosition00 = resultTilePositionSet.GetTilePosition(0, 0);
            const auto tilePosition10 = resultTilePositionSet.GetTilePosition(1, 0);
            const auto tilePosition01 = resultTilePositionSet.GetTilePosition(0, 1);
            const auto tilePosition11 = resultTilePositionSet.GetTilePosition(1, 1);

            CHECK(tilePosition00.GetTilePositionX() == 0);
            CHECK(tilePosition00.GetTilePositionY() == 0);
            CHECK(tilePosition00.GetTilePositionZ() == 0);

            CHECK(tilePosition10.GetTilePositionX() == 83);
            CHECK(tilePosition10.GetTilePositionY() == 3);
            CHECK(tilePosition10.GetTilePositionZ() == 0);

            CHECK(tilePosition01.GetTilePositionX() == 2);
            CHECK(tilePosition01.GetTilePositionY() == 82);
            CHECK(tilePosition01.GetTilePositionZ() == 0);

            CHECK(tilePosition11.GetTilePositionX() == 86);
            CHECK(tilePosition11.GetTilePositionY() == 86);
            CHECK(tilePosition11.GetTilePositionZ() == 0);
        }
    }

    typedef std::vector<std::vector<std::tuple<float, float>>> ShiftArray;
    typedef std::vector<std::vector<float>> ReliabilityArray;
    typedef std::vector<std::vector<std::tuple<float, float>>> TilePositionArray;

    auto GenerateOverlapRelationSet(const ShiftArray& shiftArray, const ReliabilityArray& reliabilityArray,
        const int32_t& tileNumberX, const int32_t& tileNumberY) -> OverlapRelationSet {
        OverlapRelationSet overlapRelationSet;
        overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);

        const auto overlapNumberX = 2 * tileNumberX - 1;
        const auto overlapNumberY = 2 * tileNumberY - 1;

        for (auto overlapIndexX = 0; overlapIndexX < overlapNumberX; ++overlapIndexX) {
            for (auto overlapIndexY = 0; overlapIndexY < overlapNumberY; ++overlapIndexY) {
                if ((overlapIndexX + overlapIndexY) % 2 == 0) continue;
                const auto& [shiftX, shiftY] = shiftArray[overlapIndexY][overlapIndexX];
                const auto& reliability = reliabilityArray[overlapIndexY][overlapIndexX];

                OverlapRelation overlapRelation;
                overlapRelation.SetShiftValue(shiftX, shiftY, 0);
                overlapRelation.SetReliability(reliability);

                overlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);
            }
        }

        return overlapRelationSet;
    }

    auto CompareAnswer(const TilePositionSet& resultTilePositionSet, const TilePositionArray& answerTilePositionArray,
        const int32_t& tileNumberX, const int32_t& tileNumberY)->bool {
        for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
            for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
                const auto resultTilePosition = resultTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);
                const auto answerTilePosition = answerTilePositionArray[tileIndexY][tileIndexX];

                const auto& resultTilePositionX = resultTilePosition.GetTilePositionX();
                const auto& resultTilePositionY = resultTilePosition.GetTilePositionY();
                const auto& resultTilePositionZ = resultTilePosition.GetTilePositionZ();

                const auto& [answerTilePositionX, answerTilePositionY] = answerTilePosition;

                const auto diffX = resultTilePositionX != answerTilePositionX;
                const auto diffY = resultTilePositionY != answerTilePositionY;
                const auto diffZ = resultTilePositionZ != 0;

                if (diffX||diffY||diffZ) {
                    return false;
                }
            }
        }
        return true;
    }

    TEST_CASE("TilePositionCalculatorBestOverlap : practical test") {
        SECTION("3x3") {
            constexpr auto tileNumberX = 3;
            constexpr auto tileNumberY = 3;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileSizeInPixel(100, 100, 20);

            ShiftArray shiftArray{
                {{0,0},{1,12},{0,0},{2,11},{0,0}},
                {{3,10},{0,0},{4,9},{0,0},{5,8}},
                {{0,0},{6,7},{0,0},{7,6},{0,0}},
                {{8,5},{0,0},{9,4},{0,0},{10,3}},
                {{0,0},{11,2},{0,0},{12,1},{0,0}}
            };

            ReliabilityArray reliabilityArray{
                {0,0.292,0,0.234,0},
                {0.460,0,0.447,0,0.736},
                {0,0.455,0,0.351,0},
                {0.506,0,0.042,0,0.175},
                {0,0.705,0,0.533,0}
            };


            const auto overlapRelationSet = GenerateOverlapRelationSet(shiftArray, reliabilityArray, tileNumberX, tileNumberY);

            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
            tilePositionCalculatorBestOverlap.SetOverlapRelationSet(overlapRelationSet);

            const auto resultTilePositionSet = tilePositionCalculatorBestOverlap.Calculate();

            TilePositionArray answerTilePositionArray{
                {{0,0},{85,8},{171,15}},
                {{3,90},{89,97},{176,103}},
                {{11,175},{102,177},{194,178}}
            };

            CHECK(CompareAnswer(resultTilePositionSet, answerTilePositionArray, tileNumberX, tileNumberY));
        }

        SECTION("5x5, case 1") {
            constexpr auto tileNumberX = 5;
            constexpr auto tileNumberY = 5;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileSizeInPixel(100, 100, 20);

            ShiftArray shiftArray{
                {{0,0},{1,1},{0,0},{2,2},{0,0},{3,3},{0,0},{4,4},{0,0}},
                {{5,5},{0,0},{6,6},{0,0},{7,7},{0,0},{8,8},{0,0},{9,9}},
                {{0,0},{-1,-1},{0,0},{-2,-2},{0,0},{-3,-3},{0,0},{-4,-4},{0,0}},
                {{-5,-5},{0,0},{-6,-6},{0,0},{-7,-7},{0,0},{-8,-8},{0,0},{-9,-9}},
                {{0,0},{1,1},{0,0},{2,2},{0,0},{3,3},{0,0},{4,4},{0,0}},
                {{5,5},{0,0},{6,6},{0,0},{7,7},{0,0},{8,8},{0,0},{9,9}},
                {{0,0},{-1,-1},{0,0},{-2,-2},{0,0},{-3,-3},{0,0},{-4,-4},{0,0}},
                {{-5,-5},{0,0},{-6,-6},{0,0},{-7,-7},{0,0},{-8,-8},{0,0},{-9,-9}},
                {{0,0},{1,1},{0,0},{2,2},{0,0},{3,3},{0,0},{4,4},{0,0}}
            };

            ReliabilityArray reliabilityArray{
                {0,0.949,0,0.021,0,0.418,0,0.007,0},
                {0.470,0,0.401,0,0.908,0,0.875,0,0.573},
                {0,0.358,0,0.665,0,0.134,0,0.987,0},
                {0.446,0,0.535,0,0.627,0,0.684,0,0.421},
                {0,0.814,0,0.385,0,0.858,0,0.188,0},
                {0.026,0,0.621,0,0.030,0,0.260,0,0.422},
                {0,0.674,0,0.187,0,0.742,0,0.916,0},
                {0.786,0,0.005,0,0.910,0,0.289,0,0.727},
                {0,0.314,0,0.258,0,0.506,0,0.032,0}
            };


            const auto overlapRelationSet = GenerateOverlapRelationSet(shiftArray, reliabilityArray, tileNumberX, tileNumberY);

            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
            tilePositionCalculatorBestOverlap.SetOverlapRelationSet(overlapRelationSet);

            const auto resultTilePositionSet = tilePositionCalculatorBestOverlap.Calculate();

            TilePositionArray answerTilePositionArray{
                {{0,0},{81,1},{158,-2},{241,1},{316,-4}},
                {{5,85},{87,87},{165,85},{249,89},{325,85}},
                {{0,160},{81,161},{158,158},{241,161},{316,156}},
                {{8,248},{87,247},{172,252},{249,249},{325,245}},
                {{3,323},{84,324},{165,325},{248,328},{316,316}}
            };

            CHECK(CompareAnswer(resultTilePositionSet, answerTilePositionArray, tileNumberX, tileNumberY));
        }

        SECTION("5x5, case 2 closed circle generated at (x0,y5)") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetOverlapLengthInPixel(133, 133);
            tileConfiguration.SetTileNumber(5, 5);
            tileConfiguration.SetTileSizeInPixel(1240, 1240, 60);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(5, 5);

            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(0.f, -4.f, 0.f); overlapRelation.SetReliability(0.00893747f); overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(0.f, -13.f, 0.f); overlapRelation.SetReliability(0.822795f); overlapRelationSet.InsertOverlapRelation(0, 3, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-53.f, -40.f, 0.f); overlapRelation.SetReliability(0.444187f); overlapRelationSet.InsertOverlapRelation(0, 5, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(8.f, -25.f, 0.f); overlapRelation.SetReliability(0.784773f); overlapRelationSet.InsertOverlapRelation(0, 7, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-13.f, 2.f, 0.f); overlapRelation.SetReliability(0.168268f); overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(9.f, 2.f, 0.f); overlapRelation.SetReliability(0.814249f); overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-10.f, 2.f, 0.f); overlapRelation.SetReliability(0.699208f); overlapRelationSet.InsertOverlapRelation(1, 4, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(12.f, 1.f, 0.f); overlapRelation.SetReliability(0.168774f); overlapRelationSet.InsertOverlapRelation(1, 6, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-11.f, 2.f, 0.f); overlapRelation.SetReliability(0.454721f); overlapRelationSet.InsertOverlapRelation(1, 8, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(21.f, -6.f, 0.f); overlapRelation.SetReliability(0.696007f); overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-18.f, -13.f, 0.f); overlapRelation.SetReliability(0.657974f); overlapRelationSet.InsertOverlapRelation(2, 3, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-32.f, -40.f, 0.f); overlapRelation.SetReliability(-0.152049f); overlapRelationSet.InsertOverlapRelation(2, 5, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-16.f, -24.f, 0.f); overlapRelation.SetReliability(0.153494f); overlapRelationSet.InsertOverlapRelation(2, 7, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-19.f, 2.f, 0.f); overlapRelation.SetReliability(0.0128238f); overlapRelationSet.InsertOverlapRelation(3, 0, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-12.f, 1.f, 0.f); overlapRelation.SetReliability(0.223762f); overlapRelationSet.InsertOverlapRelation(3, 2, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-17.f, 1.f, 0.f); overlapRelation.SetReliability(0.626937f); overlapRelationSet.InsertOverlapRelation(3, 4, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-8.f, 1.f, 0.f); overlapRelation.SetReliability(-0.165716f); overlapRelationSet.InsertOverlapRelation(3, 6, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-11.f, 1.f, 0.f); overlapRelation.SetReliability(0.807377f); overlapRelationSet.InsertOverlapRelation(3, 8, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(29.f, -5.f, 0.f); overlapRelation.SetReliability(0.831625f); overlapRelationSet.InsertOverlapRelation(4, 1, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-24.f, -14.f, 0.f); overlapRelation.SetReliability(0.501692f); overlapRelationSet.InsertOverlapRelation(4, 3, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-22.f, -40.f, 0.f); overlapRelation.SetReliability(0.812394f); overlapRelationSet.InsertOverlapRelation(4, 5, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(52.f, -24.f, 0.f); overlapRelation.SetReliability(0.18301f); overlapRelationSet.InsertOverlapRelation(4, 7, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(0.f, 1.f, 0.f); overlapRelation.SetReliability(0.389334f); overlapRelationSet.InsertOverlapRelation(5, 0, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-4.f, 2.f, 0.f); overlapRelation.SetReliability(0.83301f); overlapRelationSet.InsertOverlapRelation(5, 2, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(4.f, 1.f, 0.f); overlapRelation.SetReliability(0.798958f); overlapRelationSet.InsertOverlapRelation(5, 4, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-1.f, 2.f, 0.f); overlapRelation.SetReliability(0.749598f); overlapRelationSet.InsertOverlapRelation(5, 6, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(7.f, 2.f, 0.f); overlapRelation.SetReliability(0.680064f); overlapRelationSet.InsertOverlapRelation(5, 8, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(6.f, 4.f, 0.f); overlapRelation.SetReliability(0.651435f); overlapRelationSet.InsertOverlapRelation(6, 1, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-16.f, -12.f, 0.f); overlapRelation.SetReliability(0.878685f); overlapRelationSet.InsertOverlapRelation(6, 3, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-25.f, -39.f, 0.f); overlapRelation.SetReliability(0.747492f); overlapRelationSet.InsertOverlapRelation(6, 5, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-12.f, -25.f, 0.f); overlapRelation.SetReliability(0.735157f); overlapRelationSet.InsertOverlapRelation(6, 7, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-4.f, 1.f, 0.f); overlapRelation.SetReliability(0.917705f); overlapRelationSet.InsertOverlapRelation(7, 0, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-21.f, 2.f, 0.f); overlapRelation.SetReliability(0.785071f); overlapRelationSet.InsertOverlapRelation(7, 2, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-3.f, 1.f, 0.f); overlapRelation.SetReliability(0.75194f); overlapRelationSet.InsertOverlapRelation(7, 4, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-17.f, 1.f, 0.f); overlapRelation.SetReliability(0.776504f); overlapRelationSet.InsertOverlapRelation(7, 6, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-1.f, 1.f, 0.f); overlapRelation.SetReliability(0.814025f); overlapRelationSet.InsertOverlapRelation(7, 8, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(9.f, -5.f, 0.f); overlapRelation.SetReliability(0.790957f); overlapRelationSet.InsertOverlapRelation(8, 1, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(0.f, -12.f, 0.f); overlapRelation.SetReliability(-0.0882146f); overlapRelationSet.InsertOverlapRelation(8, 3, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(-39.f, -42.f, 0.f); overlapRelation.SetReliability(0.750107f); overlapRelationSet.InsertOverlapRelation(8, 5, overlapRelation); }
            { OverlapRelation overlapRelation; overlapRelation.SetShiftValue(4.f, -25.f, 0.f); overlapRelation.SetReliability(0.702441f); overlapRelationSet.InsertOverlapRelation(8, 7, overlapRelation); }

            TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
            tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
            tilePositionCalculatorBestOverlap.SetOverlapRelationSet(overlapRelationSet);

            auto tilePositionSet = tilePositionCalculatorBestOverlap.Calculate();
        }
    }
}
