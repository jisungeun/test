#include <catch2/catch.hpp>

#include "IOverlapRelationCalculator.h"

namespace IOverlapRelationCalculatorTest {
    class OverlapRelationCalculatorForTest final : public IOverlapRelationCalculator {
    public:
        OverlapRelationCalculatorForTest() = default;
        ~OverlapRelationCalculatorForTest() = default;

        auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override {
            setTileConfigurationTriggered = true;
        }
        auto SetTileSet(const TileSet& tileSet) -> void override {
            setTileSetTriggered = true;
        }
        auto Calculate() -> OverlapRelationSet override {
            calculateTriggered = true;
            return OverlapRelationSet{};
        }

        bool setTileConfigurationTriggered{ false };
        bool setTileSetTriggered{ false };
        bool calculateTriggered{ false };
    };

    TEST_CASE("IOverlapRelationCalculator") {
        SECTION("SetTileConfiguration()") {
            OverlapRelationCalculatorForTest overlapRelationCalculator;
            CHECK(overlapRelationCalculator.setTileConfigurationTriggered == false);
            overlapRelationCalculator.SetTileConfiguration(TileConfiguration{});
            CHECK(overlapRelationCalculator.setTileConfigurationTriggered == true);
        }
        SECTION("SetTileSet()") {
            OverlapRelationCalculatorForTest overlapRelationCalculator;
            CHECK(overlapRelationCalculator.setTileSetTriggered == false);
            overlapRelationCalculator.SetTileSet(TileSet{});
            CHECK(overlapRelationCalculator.setTileSetTriggered == true);
        }
        SECTION("Calculate()") {
            OverlapRelationCalculatorForTest overlapRelationCalculator;
            CHECK(overlapRelationCalculator.calculateTriggered == false);
            overlapRelationCalculator.Calculate();
            CHECK(overlapRelationCalculator.calculateTriggered == true);
        }
    }
}
