#include <catch2/catch.hpp>

#include "ITilePositionCalculator.h"

namespace ITilePositionCalculatorTest {
    class TilePositionCalculatorForTest final : public ITilePositionCalculator {
    public:
        TilePositionCalculatorForTest() = default;
        ~TilePositionCalculatorForTest() = default;

        auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override {
            setTileConfigurationTriggered = true;
        }
        auto Calculate() -> TilePositionSet override {
            calculateTriggered = true;
            return TilePositionSet{};
        }
        
        bool setTileConfigurationTriggered{ false };
        bool setOverlapRelationSetTriggered{ false };
        bool calculateTriggered{ false };
    };

    TEST_CASE("ITilePositionCalculator") {
        SECTION("SetTileConfiguration()") {
            TilePositionCalculatorForTest tilePositionCalculator;
            CHECK(tilePositionCalculator.setTileConfigurationTriggered == false);
            tilePositionCalculator.SetTileConfiguration(TileConfiguration{});
            CHECK(tilePositionCalculator.setTileConfigurationTriggered == true);
        }
        SECTION("Calculate()") {
            TilePositionCalculatorForTest tilePositionCalculator;
            CHECK(tilePositionCalculator.calculateTriggered == false);
            tilePositionCalculator.Calculate();
            CHECK(tilePositionCalculator.calculateTriggered == true);
        }
    }
}
