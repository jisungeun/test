#include <catch2/catch.hpp>

#include "StitchingTile.h"

namespace StitchingTileTest {
    TEST_CASE("StitchingTile") {
        SECTION("StitchingTile()") {
            StitchingTile stitchingTile;
            CHECK(&stitchingTile != nullptr);
        }
        SECTION("StitchingTile(other)") {
            StitchingTile srcStitchingTile;
            srcStitchingTile.SetDataTileNumber(1, 1);
            srcStitchingTile.AddLayer(0, 0, {}, {});

            const StitchingTile destStitchingTile(srcStitchingTile);
            CHECK(destStitchingTile.GetLayerDataTileIndexXYList().size() == 1);
        }
        SECTION("operator=()") {
            StitchingTile srcStitchingTile;
            srcStitchingTile.SetDataTileNumber(1, 1);
            srcStitchingTile.AddLayer(0, 0, {}, {});

            StitchingTile destStitchingTile;
            destStitchingTile = srcStitchingTile;
            CHECK(destStitchingTile.GetLayerDataTileIndexXYList().size() == 1);
        }
        SECTION("SetDataTileNumber()") {
            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(1, 2);
            CHECK(&stitchingTile != nullptr);
        }
        SECTION("AddLayer") {
            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(1, 1);
            stitchingTile.AddLayer(0, 0, {}, {});
            CHECK(&stitchingTile != nullptr);
        }
        SECTION("GetIndexRangeToDataTile()") {
            IndexRange indexRangeToDataTile;
            indexRangeToDataTile.SetRange(1, 2, 3, 4, 5, 6);

            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(1, 1);
            stitchingTile.AddLayer(0, 0, indexRangeToDataTile, {});

            const auto resultIndexRangeToDataTile = stitchingTile.GetIndexRangeToDataTile(0, 0);

            CHECK(resultIndexRangeToDataTile.GetX0() == 1);
            CHECK(resultIndexRangeToDataTile.GetX1() == 2);
            CHECK(resultIndexRangeToDataTile.GetY0() == 3);
            CHECK(resultIndexRangeToDataTile.GetY1() == 4);
            CHECK(resultIndexRangeToDataTile.GetZ0() == 5);
            CHECK(resultIndexRangeToDataTile.GetZ1() == 6);
        }
        SECTION("GetIndexRangeToStitchingTile()") {
            IndexRange indexRangeToStitchingTile;
            indexRangeToStitchingTile.SetRange(1, 2, 3, 4, 5, 6);

            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(1, 1);
            stitchingTile.AddLayer(0, 0, {}, indexRangeToStitchingTile);

            const auto resultIndexRangeToStitchingTile = stitchingTile.GetIndexRangeToStitchingTile(0, 0);

            CHECK(resultIndexRangeToStitchingTile.GetX0() == 1);
            CHECK(resultIndexRangeToStitchingTile.GetX1() == 2);
            CHECK(resultIndexRangeToStitchingTile.GetY0() == 3);
            CHECK(resultIndexRangeToStitchingTile.GetY1() == 4);
            CHECK(resultIndexRangeToStitchingTile.GetZ0() == 5);
            CHECK(resultIndexRangeToStitchingTile.GetZ1() == 6);
        }
        SECTION("GetLayerDataTileIndexXYList()") {
            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(2, 2);
            stitchingTile.AddLayer(0, 0, {}, {});
            stitchingTile.AddLayer(1, 0, {}, {});
            stitchingTile.AddLayer(0, 1, {}, {});
            stitchingTile.AddLayer(1, 1, {}, {});

            const auto layerDataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();
            CHECK(layerDataTileIndexXYList.size() == 4);
            CHECK(std::get<0>(layerDataTileIndexXYList[0]) == 0);
            CHECK(std::get<1>(layerDataTileIndexXYList[0]) == 0);

            CHECK(std::get<0>(layerDataTileIndexXYList[1]) == 1);
            CHECK(std::get<1>(layerDataTileIndexXYList[1]) == 0);

            CHECK(std::get<0>(layerDataTileIndexXYList[2]) == 0);
            CHECK(std::get<1>(layerDataTileIndexXYList[2]) == 1);

            CHECK(std::get<0>(layerDataTileIndexXYList[3]) == 1);
            CHECK(std::get<1>(layerDataTileIndexXYList[3]) == 1);


        }
    }
}