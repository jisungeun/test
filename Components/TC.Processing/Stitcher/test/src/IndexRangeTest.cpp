#include <catch2/catch.hpp>

#include "IndexRange.h"

namespace IndexRangeTest {
    TEST_CASE("IndexRange") {
        SECTION("IndexRange()") {
            IndexRange indexRange;
            CHECK(&indexRange != nullptr);
        }
        SECTION("IndexRange(other)") {
            IndexRange srcIndexRange;
            srcIndexRange.SetRange(0, 1, 2, 3, 4, 5);

            IndexRange destIndexRange(srcIndexRange);
            CHECK(destIndexRange.GetX0() == 0);
            CHECK(destIndexRange.GetX1() == 1);
            CHECK(destIndexRange.GetY0() == 2);
            CHECK(destIndexRange.GetY1() == 3);
            CHECK(destIndexRange.GetZ0() == 4);
            CHECK(destIndexRange.GetZ1() == 5);
        }
        SECTION("operator=()") {
            IndexRange srcIndexRange;
            srcIndexRange.SetRange(0, 1, 2, 3, 4, 5);

            IndexRange destIndexRange;
            destIndexRange = srcIndexRange;
            CHECK(destIndexRange.GetX0() == 0);
            CHECK(destIndexRange.GetX1() == 1);
            CHECK(destIndexRange.GetY0() == 2);
            CHECK(destIndexRange.GetY1() == 3);
            CHECK(destIndexRange.GetZ0() == 4);
            CHECK(destIndexRange.GetZ1() == 5);
        }
        SECTION("IsEmpty()") {
            IndexRange indexRange;
            SECTION("defautl") {
                CHECK(indexRange.IsEmpty() == true);
            }
            SECTION("valid range") {
                indexRange.SetRange(0, 1, 2, 3, 4, 5);
                CHECK(indexRange.IsEmpty() == false);
            }
            SECTION("invalid range") {
                indexRange.SetRange(5, 4, 3, 2, 1, 0);
                CHECK(indexRange.IsEmpty() == true);
            }
        }
        SECTION("SetRange()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(&indexRange != nullptr);
        }
        SECTION("GetX0()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetX0() == 0);
        }
        SECTION("GetX1()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetX1() == 1);
        }
        SECTION("GetY0()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetY0() == 2);
        }
        SECTION("GetY1()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetY1() == 3);
        }
        SECTION("GetZ0()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetZ0() == 4);
        }
        SECTION("GetZ1()") {
            IndexRange indexRange;
            indexRange.SetRange(0, 1, 2, 3, 4, 5);
            CHECK(indexRange.GetZ1() == 5);
        }
        SECTION("operator&(IndexRange, IndexRange)") {
            SECTION("valid") {
                IndexRange indexRange1;
                indexRange1.SetRange(2, 10, 4, 10, 0, 10);

                IndexRange indexRange2;
                indexRange2.SetRange(4, 15, 8, 16, 0, 10);

                const auto overlapIndexRange = indexRange1 & indexRange2;

                CHECK(overlapIndexRange.IsEmpty() == false);
                CHECK(overlapIndexRange.GetX0() == 4);
                CHECK(overlapIndexRange.GetX1() == 10);
                CHECK(overlapIndexRange.GetY0() == 8);
                CHECK(overlapIndexRange.GetY1() == 10);
                CHECK(overlapIndexRange.GetZ0() == 0);
                CHECK(overlapIndexRange.GetZ1() == 10);
            }

            SECTION("invalid") {
                IndexRange indexRange1;
                indexRange1.SetRange(2, 10, 4, 10, 0, 10);

                IndexRange indexRange2;
                indexRange2.SetRange(11, 12, 8, 16, 0, 10);

                const auto overlapIndexRange = indexRange1 & indexRange2;

                CHECK(overlapIndexRange.IsEmpty() == true);
            }
        }
    }
}