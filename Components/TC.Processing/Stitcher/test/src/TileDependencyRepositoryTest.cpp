#include <catch2/catch.hpp>

#include "TileDependencyRepository.h"

namespace TileDependencyRepositoryTest {
    TEST_CASE("TileDependencyRepository") {
        SECTION("TileDependencyRepository()") {
            TileDependencyRepository tileDependencyRepository;
            CHECK(&tileDependencyRepository != nullptr);
        }
        SECTION("TileDependencyRepository(other)") {
            TileDependencyRepository srcTileDependencyRepository;
            srcTileDependencyRepository.SetTileNumber(2, 1);
            srcTileDependencyRepository.Initialize();

            srcTileDependencyRepository.SetReference({ 1,0 }, { 0,0 });

            TileDependencyRepository destTileDependencyRepository(srcTileDependencyRepository);
            CHECK(destTileDependencyRepository.GetTileDependency({ 1, 0 }).GetReferenceTileIndexX() == 0);
            CHECK(destTileDependencyRepository.GetTileDependency({ 1, 0 }).GetReferenceTileIndexY() == 0);
        }
        SECTION("SetTileNumber()") {
            TileDependencyRepository tileDependencyRepository;
            tileDependencyRepository.SetTileNumber(0, 0);
            CHECK(&tileDependencyRepository != nullptr);
        }
        SECTION("Initialize()") {
            TileDependencyRepository tileDependencyRepository;
            tileDependencyRepository.SetTileNumber(1, 1);
            tileDependencyRepository.Initialize();
            CHECK(&tileDependencyRepository != nullptr);

        }
        SECTION("SetReference()") {
            TileDependencyRepository tileDependencyRepository;
            tileDependencyRepository.SetTileNumber(5, 5);
            tileDependencyRepository.Initialize();

            SECTION("just filp referencing") {
                tileDependencyRepository.SetReference({ 1, 2 }, { 0, 2 });
                tileDependencyRepository.SetReference({ 1, 2 }, { 1, 1 });

                CHECK(tileDependencyRepository.GetTileDependency({ 1, 2 }).GetReferenceTileIndexX() == 0);
                CHECK(tileDependencyRepository.GetTileDependency({ 1, 2 }).GetReferenceTileIndexY() == 2);

                CHECK(tileDependencyRepository.GetTileDependency({ 1, 1 }).GetReferenceTileIndexX() == 1);
                CHECK(tileDependencyRepository.GetTileDependency({ 1, 1 }).GetReferenceTileIndexY() == 2);
            }
            SECTION("Chain flip referencing") {
                tileDependencyRepository.SetReference({ 3, 1 }, { 3, 0 });
                tileDependencyRepository.SetReference({ 3, 2 }, { 3, 1 });
                tileDependencyRepository.SetReference({ 3, 3 }, { 2, 3 });
                tileDependencyRepository.SetReference({ 3, 3 }, { 3, 2 });

                CHECK(tileDependencyRepository.GetTileDependency({ 3, 0 }).GetReferenceTileIndexX() == 3);
                CHECK(tileDependencyRepository.GetTileDependency({ 3, 0 }).GetReferenceTileIndexY() == 1);

                CHECK(tileDependencyRepository.GetTileDependency({ 3, 1 }).GetReferenceTileIndexX() == 3);
                CHECK(tileDependencyRepository.GetTileDependency({ 3, 1 }).GetReferenceTileIndexY() == 2);

                CHECK(tileDependencyRepository.GetTileDependency({ 3, 2 }).GetReferenceTileIndexX() == 3);
                CHECK(tileDependencyRepository.GetTileDependency({ 3, 2 }).GetReferenceTileIndexY() == 3);

                CHECK(tileDependencyRepository.GetTileDependency({ 3, 3 }).GetReferenceTileIndexX() == 2);
                CHECK(tileDependencyRepository.GetTileDependency({ 3, 3 }).GetReferenceTileIndexY() == 3);
            }

        }
        SECTION("GetTileDependency()") {
            TileDependencyRepository tileDependencyRepository;
            tileDependencyRepository.SetTileNumber(2, 1);
            tileDependencyRepository.Initialize();

            tileDependencyRepository.SetReference({ 1, 0 }, { 0, 0 });

            CHECK(tileDependencyRepository.GetTileDependency({ 1, 0 }).GetReferenceTileIndexX() == 0);
            CHECK(tileDependencyRepository.GetTileDependency({ 1, 0 }).GetReferenceTileIndexY() == 0);
            CHECK(tileDependencyRepository.GetTileDependency({ 1, 0 }).IsReferencing() == true);
        }
    }
}