#include <catch2/catch.hpp>

#include "IStitchingOutput.h"

namespace IStitchingOutputTest {
    class StitchingOutputForTest final : public IStitchingOutput {
    public:
        StitchingOutputForTest() = default;
        ~StitchingOutputForTest() = default;

        auto SetStitchingWriterResult(const StitchingWriterResult& stitchingWriterResult) -> void override {
            setStitchingWriterResultTriggered = true;
        }

        bool setStitchingWriterResultTriggered{ false };
    };

    TEST_CASE("IStitchingOutput") {
        SECTION("SetStitchingWriterResult()") {
            StitchingOutputForTest stitchingOutput;
            CHECK(stitchingOutput.setStitchingWriterResultTriggered == false);
            stitchingOutput.SetStitchingWriterResult(StitchingWriterResult{});
            CHECK(stitchingOutput.setStitchingWriterResultTriggered == true);
        }
    }
}
