#include <catch2/catch.hpp>

#include "IMissingTileChecker.h"

namespace IMissingTileCheckerTest {
    class MissingTileCheckerForTest final : public IMissingTileChecker {
    public:
        MissingTileCheckerForTest() = default;
        ~MissingTileCheckerForTest() = default;

        auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override {
            setTileConfigurationTriggered = true;
        }
        auto SetTimelapseNumber(const int32_t& timelapseNumber) -> void override {
            setTimelapseNumberTriggered = true;
        }
        auto Check() -> bool override {
            checkTriggered = true;
            return false;
        }

        bool setTileConfigurationTriggered{ false };
        bool setTimelapseNumberTriggered{ false };
        bool checkTriggered{ false };
    };

    TEST_CASE("IMissingTileChecker") {
        SECTION("SetTileConfiguration()") {
            MissingTileCheckerForTest missingTileChecker;
            CHECK(missingTileChecker.setTileConfigurationTriggered == false);
            missingTileChecker.SetTileConfiguration(TileConfiguration());
            CHECK(missingTileChecker.setTileConfigurationTriggered == true);
        }
        SECTION("SetTimelapseNumber()") {
            MissingTileCheckerForTest missingTileChecker;
            CHECK(missingTileChecker.setTimelapseNumberTriggered == false);
            missingTileChecker.SetTimelapseNumber(0);
            CHECK(missingTileChecker.setTimelapseNumberTriggered == true);
        }
        SECTION("Check()") {
            MissingTileCheckerForTest missingTileChecker;
            CHECK(missingTileChecker.checkTriggered == false);
            missingTileChecker.Check();
            CHECK(missingTileChecker.checkTriggered == true);
        }
    }
}
