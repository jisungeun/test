#include <iostream>

#include <catch2/catch.hpp>

#include "OverlapNodeRepository.h"

namespace OverlapNodeRepositoryTest {
    auto GenerateOverlaRelationSet() -> OverlapRelationSet{
        constexpr auto tileNumberX = 4;
        constexpr auto tileNumberY = 4;

        constexpr auto overlapNumberX = 2 * tileNumberX - 1;
        constexpr auto overlapNumberY = 2 * tileNumberY - 1;

        OverlapRelationSet overlapRelationSet;
        overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
        for (auto indexX = 0; indexX < overlapNumberX; ++indexX) {
            for (auto indexY = 0; indexY < overlapNumberY; ++indexY) {
                if ((indexX + indexY) % 2 != 1) { continue; }

                OverlapRelation overlapRelation;
                overlapRelationSet.InsertOverlapRelation(indexX, indexY, overlapRelation);
            }
        }
        
        return overlapRelationSet;
    }

    TEST_CASE("OverlapNodeRepository : unit test") {
        SECTION("OverlapNodeRepository()") {
            OverlapNodeRepository overlapNodeRepository;
            CHECK(&overlapNodeRepository != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration({});
            CHECK(&overlapNodeRepository != nullptr);
        }
        SECTION("SetOverlapRelationSet()") {
            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetOverlapRelationSet({});
            CHECK(&overlapNodeRepository != nullptr);
        }
        SECTION("Initialize()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(2, 2);

            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();
            CHECK(&overlapNodeRepository != nullptr);
        }
        SECTION("GetOverlapNode()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(2, 2);

            OverlapRelation overlapRelation1;
            overlapRelation1.SetReliability(0.1f);
            OverlapRelation overlapRelation2;
            overlapRelation2.SetReliability(0.2f);
            OverlapRelation overlapRelation3;
            overlapRelation3.SetReliability(0.3f);
            OverlapRelation overlapRelation4;
            overlapRelation4.SetReliability(0.4f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileConfiguration.GetTileNumberX(), tileConfiguration.GetTileNumberY());
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation4);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();

            CHECK(overlapNodeRepository.GetOverlapNode(1, 0).GetScore() == 0.1f);
            CHECK(overlapNodeRepository.GetOverlapNode(0, 1).GetScore() == 0.2f);
            CHECK(overlapNodeRepository.GetOverlapNode(2, 1).GetScore() == 0.3f);
            CHECK(overlapNodeRepository.GetOverlapNode(1, 2).GetScore() == 0.4f);
        }
        SECTION("IsConnectable()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(4, 4);

            auto overlapRelationSet = GenerateOverlaRelationSet();

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();

            SECTION("All Open case") {
                CHECK(overlapNodeRepository.IsConnectable(2, 3) == true);
            }

            SECTION("Directly next to Close path") {
                SECTION("Closed") {
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Closed);
                    overlapNodeRepository.GetOverlapNode(4, 3).SetStatus(OverlapNode::Status::Closed);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Closed);
                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == true);
                }
                SECTION("Connected") {
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(4, 3).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Connected);
                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == false);
                    
                }
                SECTION("Connected or Closed") {
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(4, 3).SetStatus(OverlapNode::Status::Closed);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Connected);
                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == true);
                }
            }

            SECTION("Not-Directly next to Close path") {
                SECTION("1 step, straight") {
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(6, 3).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 4).SetStatus(OverlapNode::Status::Connected);
                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == false);
                }
                SECTION("3 step, turned") {
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(6, 3).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(6, 5).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 6).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 6).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(2, 5).SetStatus(OverlapNode::Status::Connected);

                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == false);
                }
                SECTION("both side") {
                    //right side
                    overlapNodeRepository.GetOverlapNode(3, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 4).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 2).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(6, 3).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(6, 5).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(5, 6).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(3, 6).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(2, 5).SetStatus(OverlapNode::Status::Connected);

                    //left down side
                    overlapNodeRepository.GetOverlapNode(0, 5).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(2, 5).SetStatus(OverlapNode::Status::Connected);
                    overlapNodeRepository.GetOverlapNode(1, 6).SetStatus(OverlapNode::Status::Connected);

                    CHECK(overlapNodeRepository.IsConnectable(2, 3) == false);
                }
            }
            
        }
        SECTION("GetMaximumScoreOpenedNode()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(2, 2);

            OverlapRelation overlapRelation1;
            overlapRelation1.SetReliability(0.1f);
            OverlapRelation overlapRelation2;
            overlapRelation2.SetReliability(0.2f);
            OverlapRelation overlapRelation3;
            overlapRelation3.SetReliability(0.3f);
            OverlapRelation overlapRelation4;
            overlapRelation4.SetReliability(0.4f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileConfiguration.GetTileNumberX(), tileConfiguration.GetTileNumberY());
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation4);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();

            overlapNodeRepository.GetOverlapNode(1, 2).SetStatus(OverlapNode::Status::Closed);
            overlapNodeRepository.GetOverlapNode(2, 1).SetStatus(OverlapNode::Status::Connected);

            CHECK(overlapNodeRepository.GetMaximumScoreOpenedNode()->GetScore() == 0.2f);
        }

        SECTION("ConnectionCompleted()") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(2, 2);

            OverlapRelation overlapRelation1;
            overlapRelation1.SetReliability(0.1f);
            OverlapRelation overlapRelation2;
            overlapRelation2.SetReliability(0.2f);
            OverlapRelation overlapRelation3;
            overlapRelation3.SetReliability(0.3f);
            OverlapRelation overlapRelation4;
            overlapRelation4.SetReliability(0.4f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileConfiguration.GetTileNumberX(), tileConfiguration.GetTileNumberY());
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation4);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();

            overlapNodeRepository.Connect(1, 0);
            CHECK(overlapNodeRepository.ConnectionCompleted() == false);

            overlapNodeRepository.Connect(0, 1);
            CHECK(overlapNodeRepository.ConnectionCompleted() == false);

            overlapNodeRepository.Connect(2, 1);
            CHECK(overlapNodeRepository.ConnectionCompleted() == true);
        }
    }

    TEST_CASE("OverlapNodeRepository : practical test") {
        SECTION("Random Generator") {
            constexpr auto tileNumberX = 30;
            constexpr auto tileNumberY = 30;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            constexpr auto overlapNumberX = 2 * tileNumberX - 1;
            constexpr auto overlapNumberY = 2 * tileNumberY - 1;

            constexpr float randMinValue = 0.3f;
            constexpr float randMaxValue = 0.98f;

            std::random_device rd;
            std::mt19937 gen(rd());
            const std::uniform_real_distribution<float> dis(randMinValue, randMaxValue);

            double time = 0;
            constexpr int32_t iteration = 10;
            for (auto i = 0; i < iteration; ++i) {
                OverlapRelationSet overlapRelationSet;
                overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);

                for (auto indexX = 0; indexX < overlapNumberX; ++indexX) {
                    for (auto indexY = 0; indexY < overlapNumberY; ++indexY) {
                        if ((indexX + indexY) % 2 == 1) {
                            const auto randomReliability = dis(gen);

                            OverlapRelation overlapRelation;
                            overlapRelation.SetReliability(randomReliability);

                            overlapRelationSet.InsertOverlapRelation(indexX, indexY, overlapRelation);
                        }
                    }
                }

                OverlapNodeRepository overlapNodeRepository;
                overlapNodeRepository.SetTileConfiguration(tileConfiguration);
                overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);
                overlapNodeRepository.Initialize();

                const auto start = std::chrono::system_clock::now();

                while (!overlapNodeRepository.ConnectionCompleted()) {
                    const auto maximumScoredOpenedNode = overlapNodeRepository.GetMaximumScoreOpenedNode();
                    const auto nodeOverlapIndexX = maximumScoredOpenedNode->GetOverlapIndexX();
                    const auto nodeOverlapIndexY = maximumScoredOpenedNode->GetOverlapIndexY();

                    if (overlapNodeRepository.IsConnectable(nodeOverlapIndexX, nodeOverlapIndexY)) {
                        overlapNodeRepository.Connect(nodeOverlapIndexX, nodeOverlapIndexY);
                    } else {
                        overlapNodeRepository.Close(nodeOverlapIndexX, nodeOverlapIndexY);
                    }
                }

                for (auto indexX = 0; indexX < overlapNumberX; ++indexX) {
                    for (auto indexY = 0; indexY < overlapNumberY; ++indexY) {
                        if ((indexX + indexY) % 2 == 1) {
                            const auto overlapNode = overlapNodeRepository.GetOverlapNode(indexX, indexY);
                            if (overlapNode.GetStatus() == OverlapNode::Status::Opened) {
                                overlapNodeRepository.Close(indexX, indexY);
                            }
                        }
                    }
                }
                
                const auto end = std::chrono::system_clock::now();

                const auto timeCount = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

                time += timeCount;
                std::cout << i << ": " << timeCount << "ms" << std::endl;

                auto connectAnswerCount = tileNumberX * tileNumberY - 1;
                auto connectedCount = 0;
                for (auto indexX = 0; indexX < overlapNumberX; ++indexX) {
                    for (auto indexY = 0; indexY < overlapNumberY; ++indexY) {
                        if ((indexX + indexY) % 2 == 1) {
                            const auto overlapNode = overlapNodeRepository.GetOverlapNode(indexX, indexY);
                            if (overlapNode.GetStatus() == OverlapNode::Status::Connected) {
                                connectedCount++;
                            }
                        }
                    }
                }

                if (connectAnswerCount!=connectedCount) {
                    std::cout << i << std::endl;
                    
                    for (auto indexX = 0; indexX < overlapNumberX; ++indexX) {
                        for (auto indexY = 0; indexY < overlapNumberY; ++indexY) {
                            if ((indexX + indexY) % 2 == 1) {
                                const auto reliability = overlapRelationSet.GetOverlapRelation(indexX, indexY).GetReliability();

                                std::cout << indexX << ":" << indexY << ":" << reliability << std::endl;
                            }
                        }
                    }
                }

                CHECK(connectAnswerCount == connectedCount);
            }

            std::cout <<"average : " << time / static_cast<double>(iteration) << "ms" << std::endl;
        }

        SECTION("3x3 case") {
            constexpr auto tileNumberX = 3;
            constexpr auto tileNumberY = 3;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            OverlapRelation overlapRelation0; overlapRelation0.SetReliability(0.692826f);
            OverlapRelation overlapRelation1; overlapRelation1.SetReliability(0.631896f);
            OverlapRelation overlapRelation2; overlapRelation2.SetReliability(0.589436f);
            OverlapRelation overlapRelation3; overlapRelation3.SetReliability(0.432007f);
            OverlapRelation overlapRelation4; overlapRelation4.SetReliability(0.900331f);
            OverlapRelation overlapRelation5; overlapRelation5.SetReliability(0.38216f);
            OverlapRelation overlapRelation6; overlapRelation6.SetReliability(0.391623f);
            OverlapRelation overlapRelation7; overlapRelation7.SetReliability(0.632373f);
            OverlapRelation overlapRelation8; overlapRelation8.SetReliability(0.300311f);
            OverlapRelation overlapRelation9; overlapRelation9.SetReliability(0.953001f);
            OverlapRelation overlapRelation10; overlapRelation10.SetReliability(0.604274f);
            OverlapRelation overlapRelation11; overlapRelation11.SetReliability(0.86638f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation0);
            overlapRelationSet.InsertOverlapRelation(0, 3, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(1, 4, overlapRelation4);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation5);
            overlapRelationSet.InsertOverlapRelation(2, 3, overlapRelation6);
            overlapRelationSet.InsertOverlapRelation(3, 0, overlapRelation7);
            overlapRelationSet.InsertOverlapRelation(3, 2, overlapRelation8);
            overlapRelationSet.InsertOverlapRelation(3, 4, overlapRelation9);
            overlapRelationSet.InsertOverlapRelation(4, 1, overlapRelation10);
            overlapRelationSet.InsertOverlapRelation(4, 3, overlapRelation11);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);
            overlapNodeRepository.Initialize();

            while (!overlapNodeRepository.ConnectionCompleted()) {
                const auto maximumScoredOpenedNode = overlapNodeRepository.GetMaximumScoreOpenedNode();
                const auto nodeOverlapIndexX = maximumScoredOpenedNode->GetOverlapIndexX();
                const auto nodeOverlapIndexY = maximumScoredOpenedNode->GetOverlapIndexY();

                if (overlapNodeRepository.IsConnectable(nodeOverlapIndexX, nodeOverlapIndexY)) {
                    overlapNodeRepository.Connect(nodeOverlapIndexX, nodeOverlapIndexY);
                } else {
                    overlapNodeRepository.Close(nodeOverlapIndexX, nodeOverlapIndexY);
                }
            }

        }

        SECTION("including (-) value score") {
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(2, 2);

            OverlapRelation overlapRelation1;
            overlapRelation1.SetReliability(-0.1f);
            OverlapRelation overlapRelation2;
            overlapRelation2.SetReliability(0.2f);
            OverlapRelation overlapRelation3;
            overlapRelation3.SetReliability(0.3f);
            OverlapRelation overlapRelation4;
            overlapRelation4.SetReliability(0.4f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileConfiguration.GetTileNumberX(), tileConfiguration.GetTileNumberY());
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation4);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);

            overlapNodeRepository.Initialize();

            while (!overlapNodeRepository.ConnectionCompleted()) {
                const auto maximumScoredOpenedNode = overlapNodeRepository.GetMaximumScoreOpenedNode();
                const auto nodeOverlapIndexX = maximumScoredOpenedNode->GetOverlapIndexX();
                const auto nodeOverlapIndexY = maximumScoredOpenedNode->GetOverlapIndexY();

                if (overlapNodeRepository.IsConnectable(nodeOverlapIndexX, nodeOverlapIndexY)) {
                    overlapNodeRepository.Connect(nodeOverlapIndexX, nodeOverlapIndexY);
                } else {
                    overlapNodeRepository.Close(nodeOverlapIndexX, nodeOverlapIndexY);
                }
            }

            CHECK(overlapNodeRepository.ConnectionCompleted() == true);
        }

        SECTION("failure case") {
            // t : tile
            // c : connected
            // x : checking start
            // --------------------------
            // |  t |  c |  t |  c |  t |
            // --------------------------
            // |  c |    |    |    |  c |
            // --------------------------
            // |  t |  c |  t |  x |  t |
            // --------------------------
            // noraml result : x is closed
            // failure : x is connected

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(3, 2);

            OverlapRelation overlapRelation1;
            OverlapRelation overlapRelation2;
            OverlapRelation overlapRelation3;
            OverlapRelation overlapRelation4;
            OverlapRelation overlapRelation5;
            OverlapRelation overlapRelation6;
            OverlapRelation overlapRelation7;

            overlapRelation1.SetReliability(0.1f);
            overlapRelation2.SetReliability(0.2f);
            overlapRelation3.SetReliability(0.3f);
            overlapRelation4.SetReliability(0.4f);
            overlapRelation5.SetReliability(0.5f);
            overlapRelation6.SetReliability(0.6f);
            overlapRelation7.SetReliability(0.7f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileConfiguration.GetTileNumberX(), tileConfiguration.GetTileNumberY());
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(3, 0, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation4);
            overlapRelationSet.InsertOverlapRelation(4, 1, overlapRelation5);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation6);
            overlapRelationSet.InsertOverlapRelation(3, 2, overlapRelation7);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);
            overlapNodeRepository.Initialize();

            overlapNodeRepository.GetOverlapNode(1, 0).SetStatus(OverlapNode::Status::Connected);
            overlapNodeRepository.GetOverlapNode(0, 1).SetStatus(OverlapNode::Status::Connected);
            overlapNodeRepository.GetOverlapNode(1, 2).SetStatus(OverlapNode::Status::Connected);
            overlapNodeRepository.GetOverlapNode(3, 0).SetStatus(OverlapNode::Status::Connected);
            overlapNodeRepository.GetOverlapNode(4, 1).SetStatus(OverlapNode::Status::Connected);

            CHECK(overlapNodeRepository.IsConnectable(3, 2) == false);
        }

        SECTION("7x7 case") {
            constexpr auto tileNumberX = 7;
            constexpr auto tileNumberY = 7;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            OverlapRelation overlapRelation0; overlapRelation0.SetReliability(0.783138f);
            OverlapRelation overlapRelation1; overlapRelation1.SetReliability(0.813785f);
            OverlapRelation overlapRelation2; overlapRelation2.SetReliability(0.865963f);
            OverlapRelation overlapRelation3; overlapRelation3.SetReliability(0.855118f);
            OverlapRelation overlapRelation4; overlapRelation4.SetReliability(0.73836f);
            OverlapRelation overlapRelation5; overlapRelation5.SetReliability(0.896934f);
            OverlapRelation overlapRelation6; overlapRelation6.SetReliability(0.851415f);
            OverlapRelation overlapRelation7; overlapRelation7.SetReliability(0.836478f);
            OverlapRelation overlapRelation8; overlapRelation8.SetReliability(0.925235f);
            OverlapRelation overlapRelation9; overlapRelation9.SetReliability(0.857459f);
            OverlapRelation overlapRelation10; overlapRelation10.SetReliability(0.921903f);
            OverlapRelation overlapRelation11; overlapRelation11.SetReliability(0.841356f);
            OverlapRelation overlapRelation12; overlapRelation12.SetReliability(0.905847f);
            OverlapRelation overlapRelation13; overlapRelation13.SetReliability(0.720772f);
            OverlapRelation overlapRelation14; overlapRelation14.SetReliability(0.773677f);
            OverlapRelation overlapRelation15; overlapRelation15.SetReliability(0.812092f);
            OverlapRelation overlapRelation16; overlapRelation16.SetReliability(0.710674f);
            OverlapRelation overlapRelation17; overlapRelation17.SetReliability(0.720035f);
            OverlapRelation overlapRelation18; overlapRelation18.SetReliability(0.85508f);
            OverlapRelation overlapRelation19; overlapRelation19.SetReliability(0.91846f);
            OverlapRelation overlapRelation20; overlapRelation20.SetReliability(0.683402f);
            OverlapRelation overlapRelation21; overlapRelation21.SetReliability(0.896105f);
            OverlapRelation overlapRelation22; overlapRelation22.SetReliability(0.883083f);
            OverlapRelation overlapRelation23; overlapRelation23.SetReliability(0.862561f);
            OverlapRelation overlapRelation24; overlapRelation24.SetReliability(0.891325f);
            OverlapRelation overlapRelation25; overlapRelation25.SetReliability(0.903154f);
            OverlapRelation overlapRelation26; overlapRelation26.SetReliability(0.637256f);
            OverlapRelation overlapRelation27; overlapRelation27.SetReliability(0.849008f);
            OverlapRelation overlapRelation28; overlapRelation28.SetReliability(0.887493f);
            OverlapRelation overlapRelation29; overlapRelation29.SetReliability(0.791599f);
            OverlapRelation overlapRelation30; overlapRelation30.SetReliability(0.859001f);
            OverlapRelation overlapRelation31; overlapRelation31.SetReliability(0.76534f);
            OverlapRelation overlapRelation32; overlapRelation32.SetReliability(0.895876f);
            OverlapRelation overlapRelation33; overlapRelation33.SetReliability(0.901081f);
            OverlapRelation overlapRelation34; overlapRelation34.SetReliability(0.747768f);
            OverlapRelation overlapRelation35; overlapRelation35.SetReliability(0.847336f);
            OverlapRelation overlapRelation36; overlapRelation36.SetReliability(0.904768f);
            OverlapRelation overlapRelation37; overlapRelation37.SetReliability(0.904608f);
            OverlapRelation overlapRelation38; overlapRelation38.SetReliability(0.903499f);
            OverlapRelation overlapRelation39; overlapRelation39.SetReliability(0.813041f);
            OverlapRelation overlapRelation40; overlapRelation40.SetReliability(0.825905f);
            OverlapRelation overlapRelation41; overlapRelation41.SetReliability(0.783047f);
            OverlapRelation overlapRelation42; overlapRelation42.SetReliability(0.72189f);
            OverlapRelation overlapRelation43; overlapRelation43.SetReliability(0.841301f);
            OverlapRelation overlapRelation44; overlapRelation44.SetReliability(0.822066f);
            OverlapRelation overlapRelation45; overlapRelation45.SetReliability(0.895843f);
            OverlapRelation overlapRelation46; overlapRelation46.SetReliability(0.907437f);
            OverlapRelation overlapRelation47; overlapRelation47.SetReliability(0.891539f);
            OverlapRelation overlapRelation48; overlapRelation48.SetReliability(0.48385f);
            OverlapRelation overlapRelation49; overlapRelation49.SetReliability(0.87281f);
            OverlapRelation overlapRelation50; overlapRelation50.SetReliability(0.896787f);
            OverlapRelation overlapRelation51; overlapRelation51.SetReliability(0.864157f);
            OverlapRelation overlapRelation52; overlapRelation52.SetReliability(0.654072f);
            OverlapRelation overlapRelation53; overlapRelation53.SetReliability(0.756889f);
            OverlapRelation overlapRelation54; overlapRelation54.SetReliability(0.880316f);
            OverlapRelation overlapRelation55; overlapRelation55.SetReliability(0.799075f);
            OverlapRelation overlapRelation56; overlapRelation56.SetReliability(0.737371f);
            OverlapRelation overlapRelation57; overlapRelation57.SetReliability(0.874576f);
            OverlapRelation overlapRelation58; overlapRelation58.SetReliability(0.921539f);
            OverlapRelation overlapRelation59; overlapRelation59.SetReliability(0.909625f);
            OverlapRelation overlapRelation60; overlapRelation60.SetReliability(0.68433f);
            OverlapRelation overlapRelation61; overlapRelation61.SetReliability(0.910984f);
            OverlapRelation overlapRelation62; overlapRelation62.SetReliability(0.872f);
            OverlapRelation overlapRelation63; overlapRelation63.SetReliability(0.828119f);
            OverlapRelation overlapRelation64; overlapRelation64.SetReliability(0.894428f);
            OverlapRelation overlapRelation65; overlapRelation65.SetReliability(0.848307f);
            OverlapRelation overlapRelation66; overlapRelation66.SetReliability(0.815649f);
            OverlapRelation overlapRelation67; overlapRelation67.SetReliability(0.532884f);
            OverlapRelation overlapRelation68; overlapRelation68.SetReliability(0.761832f);
            OverlapRelation overlapRelation69; overlapRelation69.SetReliability(0.85447f);
            OverlapRelation overlapRelation70; overlapRelation70.SetReliability(0.818569f);
            OverlapRelation overlapRelation71; overlapRelation71.SetReliability(0.909707f);
            OverlapRelation overlapRelation72; overlapRelation72.SetReliability(0.703023f);
            OverlapRelation overlapRelation73; overlapRelation73.SetReliability(0.845711f);
            OverlapRelation overlapRelation74; overlapRelation74.SetReliability(0.500366f);
            OverlapRelation overlapRelation75; overlapRelation75.SetReliability(0.884436f);
            OverlapRelation overlapRelation76; overlapRelation76.SetReliability(0.939223f);
            OverlapRelation overlapRelation77; overlapRelation77.SetReliability(0.887565f);
            OverlapRelation overlapRelation78; overlapRelation78.SetReliability(0.802336f);
            OverlapRelation overlapRelation79; overlapRelation79.SetReliability(0.606703f);
            OverlapRelation overlapRelation80; overlapRelation80.SetReliability(0.848161f);
            OverlapRelation overlapRelation81; overlapRelation81.SetReliability(0.87633f);
            OverlapRelation overlapRelation82; overlapRelation82.SetReliability(0.298707f);
            OverlapRelation overlapRelation83; overlapRelation83.SetReliability(0.246916f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation0);
            overlapRelationSet.InsertOverlapRelation(0, 3, overlapRelation1);
            overlapRelationSet.InsertOverlapRelation(0, 5, overlapRelation2);
            overlapRelationSet.InsertOverlapRelation(0, 7, overlapRelation3);
            overlapRelationSet.InsertOverlapRelation(0, 9, overlapRelation4);
            overlapRelationSet.InsertOverlapRelation(0, 11, overlapRelation5);
            overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation6);
            overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation7);
            overlapRelationSet.InsertOverlapRelation(1, 4, overlapRelation8);
            overlapRelationSet.InsertOverlapRelation(1, 6, overlapRelation9);
            overlapRelationSet.InsertOverlapRelation(1, 8, overlapRelation10);
            overlapRelationSet.InsertOverlapRelation(1, 10, overlapRelation11);
            overlapRelationSet.InsertOverlapRelation(1, 12, overlapRelation12);
            overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation13);
            overlapRelationSet.InsertOverlapRelation(2, 3, overlapRelation14);
            overlapRelationSet.InsertOverlapRelation(2, 5, overlapRelation15);
            overlapRelationSet.InsertOverlapRelation(2, 7, overlapRelation16);
            overlapRelationSet.InsertOverlapRelation(2, 9, overlapRelation17);
            overlapRelationSet.InsertOverlapRelation(2, 11, overlapRelation18);
            overlapRelationSet.InsertOverlapRelation(3, 0, overlapRelation19);
            overlapRelationSet.InsertOverlapRelation(3, 2, overlapRelation20);
            overlapRelationSet.InsertOverlapRelation(3, 4, overlapRelation21);
            overlapRelationSet.InsertOverlapRelation(3, 6, overlapRelation22);
            overlapRelationSet.InsertOverlapRelation(3, 8, overlapRelation23);
            overlapRelationSet.InsertOverlapRelation(3, 10, overlapRelation24);
            overlapRelationSet.InsertOverlapRelation(3, 12, overlapRelation25);
            overlapRelationSet.InsertOverlapRelation(4, 1, overlapRelation26);
            overlapRelationSet.InsertOverlapRelation(4, 3, overlapRelation27);
            overlapRelationSet.InsertOverlapRelation(4, 5, overlapRelation28);
            overlapRelationSet.InsertOverlapRelation(4, 7, overlapRelation29);
            overlapRelationSet.InsertOverlapRelation(4, 9, overlapRelation30);
            overlapRelationSet.InsertOverlapRelation(4, 11, overlapRelation31);
            overlapRelationSet.InsertOverlapRelation(5, 0, overlapRelation32);
            overlapRelationSet.InsertOverlapRelation(5, 2, overlapRelation33);
            overlapRelationSet.InsertOverlapRelation(5, 4, overlapRelation34);
            overlapRelationSet.InsertOverlapRelation(5, 6, overlapRelation35);
            overlapRelationSet.InsertOverlapRelation(5, 8, overlapRelation36);
            overlapRelationSet.InsertOverlapRelation(5, 10, overlapRelation37);
            overlapRelationSet.InsertOverlapRelation(5, 12, overlapRelation38);
            overlapRelationSet.InsertOverlapRelation(6, 1, overlapRelation39);
            overlapRelationSet.InsertOverlapRelation(6, 3, overlapRelation40);
            overlapRelationSet.InsertOverlapRelation(6, 5, overlapRelation41);
            overlapRelationSet.InsertOverlapRelation(6, 7, overlapRelation42);
            overlapRelationSet.InsertOverlapRelation(6, 9, overlapRelation43);
            overlapRelationSet.InsertOverlapRelation(6, 11, overlapRelation44);
            overlapRelationSet.InsertOverlapRelation(7, 0, overlapRelation45);
            overlapRelationSet.InsertOverlapRelation(7, 2, overlapRelation46);
            overlapRelationSet.InsertOverlapRelation(7, 4, overlapRelation47);
            overlapRelationSet.InsertOverlapRelation(7, 6, overlapRelation48);
            overlapRelationSet.InsertOverlapRelation(7, 8, overlapRelation49);
            overlapRelationSet.InsertOverlapRelation(7, 10, overlapRelation50);
            overlapRelationSet.InsertOverlapRelation(7, 12, overlapRelation51);
            overlapRelationSet.InsertOverlapRelation(8, 1, overlapRelation52);
            overlapRelationSet.InsertOverlapRelation(8, 3, overlapRelation53);
            overlapRelationSet.InsertOverlapRelation(8, 5, overlapRelation54);
            overlapRelationSet.InsertOverlapRelation(8, 7, overlapRelation55);
            overlapRelationSet.InsertOverlapRelation(8, 9, overlapRelation56);
            overlapRelationSet.InsertOverlapRelation(8, 11, overlapRelation57);
            overlapRelationSet.InsertOverlapRelation(9, 0, overlapRelation58);
            overlapRelationSet.InsertOverlapRelation(9, 2, overlapRelation59);
            overlapRelationSet.InsertOverlapRelation(9, 4, overlapRelation60);
            overlapRelationSet.InsertOverlapRelation(9, 6, overlapRelation61);
            overlapRelationSet.InsertOverlapRelation(9, 8, overlapRelation62);
            overlapRelationSet.InsertOverlapRelation(9, 10, overlapRelation63);
            overlapRelationSet.InsertOverlapRelation(9, 12, overlapRelation64);
            overlapRelationSet.InsertOverlapRelation(10, 1, overlapRelation65);
            overlapRelationSet.InsertOverlapRelation(10, 3, overlapRelation66);
            overlapRelationSet.InsertOverlapRelation(10, 5, overlapRelation67);
            overlapRelationSet.InsertOverlapRelation(10, 7, overlapRelation68);
            overlapRelationSet.InsertOverlapRelation(10, 9, overlapRelation69);
            overlapRelationSet.InsertOverlapRelation(10, 11, overlapRelation70);
            overlapRelationSet.InsertOverlapRelation(11, 0, overlapRelation71);
            overlapRelationSet.InsertOverlapRelation(11, 2, overlapRelation72);
            overlapRelationSet.InsertOverlapRelation(11, 4, overlapRelation73);
            overlapRelationSet.InsertOverlapRelation(11, 6, overlapRelation74);
            overlapRelationSet.InsertOverlapRelation(11, 8, overlapRelation75);
            overlapRelationSet.InsertOverlapRelation(11, 10, overlapRelation76);
            overlapRelationSet.InsertOverlapRelation(11, 12, overlapRelation77);
            overlapRelationSet.InsertOverlapRelation(12, 1, overlapRelation78);
            overlapRelationSet.InsertOverlapRelation(12, 3, overlapRelation79);
            overlapRelationSet.InsertOverlapRelation(12, 5, overlapRelation80);
            overlapRelationSet.InsertOverlapRelation(12, 7, overlapRelation81);
            overlapRelationSet.InsertOverlapRelation(12, 9, overlapRelation82);
            overlapRelationSet.InsertOverlapRelation(12, 11, overlapRelation83);

            OverlapNodeRepository overlapNodeRepository;
            overlapNodeRepository.SetTileConfiguration(tileConfiguration);
            overlapNodeRepository.SetOverlapRelationSet(overlapRelationSet);
            overlapNodeRepository.Initialize();

            while (!overlapNodeRepository.ConnectionCompleted()) {
                const auto maximumScoredOpenedNode = overlapNodeRepository.GetMaximumScoreOpenedNode();
                const auto nodeOverlapIndexX = maximumScoredOpenedNode->GetOverlapIndexX();
                const auto nodeOverlapIndexY = maximumScoredOpenedNode->GetOverlapIndexY();

                if (overlapNodeRepository.IsConnectable(nodeOverlapIndexX, nodeOverlapIndexY)) {
                    overlapNodeRepository.Connect(nodeOverlapIndexX, nodeOverlapIndexY);
                } else {
                    overlapNodeRepository.Close(nodeOverlapIndexX, nodeOverlapIndexY);
                }
            }

        }
    }
}