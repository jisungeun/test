#include <catch2/catch.hpp>

#include "TileDependency.h"

namespace TileDependencyTest {
    TEST_CASE("TileDependency") {
        SECTION("TileDependency()") {
            TileDependency tileDependency;
            CHECK(&tileDependency != nullptr);
        }
        SECTION("TileDependency(other)") {
            TileDependency srcTileDependency;
            srcTileDependency.SetReferenceTileIndex(0, 0);

            TileDependency destTileDependency(srcTileDependency);
            CHECK(destTileDependency.IsReferencing() == true);
        }
        SECTION("operator=()") {
            TileDependency srcTileDependency;
            srcTileDependency.SetReferenceTileIndex(0, 0);

            TileDependency destTileDependency;
            destTileDependency = srcTileDependency;
            CHECK(destTileDependency.IsReferencing() == true);
        }
        SECTION("IsReferencing()") {
            TileDependency tileDependency;
            tileDependency.SetReferenceTileIndex(0, 0);
            CHECK(tileDependency.IsReferencing() == true);
        }
        SECTION("SetReferenceTileIndex()") {
            TileDependency tileDependency;
            tileDependency.SetReferenceTileIndex(0, 0);
            CHECK(&tileDependency != nullptr);
        }
        SECTION("GetReferenceTileIndexX()") {
            TileDependency tileDependency;
            tileDependency.SetReferenceTileIndex(1, 2);
            CHECK(tileDependency.GetReferenceTileIndexX() == 1);
        }
        SECTION("GetReferenceTileIndexY()") {
            TileDependency tileDependency;
            tileDependency.SetReferenceTileIndex(1, 2);
            CHECK(tileDependency.GetReferenceTileIndexY() == 2);
        }
    }
}