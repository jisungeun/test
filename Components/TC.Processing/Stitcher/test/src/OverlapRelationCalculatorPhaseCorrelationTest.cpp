#include <QString>
#include <QMap>
#include <catch2/catch.hpp>

#include "OverlapRelationCalculatorPhaseCorrelation.h"

namespace OverlapRelationCalculatorPhaseCorrelationTest {
    const QString testTopFolderPath = TEST_DATA_FOLDR_PATH;
    const QString dataFolderPath = testTopFolderPath + "/StitchingData/Cell3";

    TEST_CASE("OverlapRelationCalculatorPhaseCorrelation") {
        SECTION("OverlapRelationCalculatorPhaseCorrelation") {
            OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
            CHECK(&overlapRelationCalculatorPhaseCorrelation != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
            overlapRelationCalculatorPhaseCorrelation.SetTileConfiguration({});
            CHECK(&overlapRelationCalculatorPhaseCorrelation != nullptr);
        }
        SECTION("SetTileSet()") {
            OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
            overlapRelationCalculatorPhaseCorrelation.SetTileSet({});
            CHECK(&overlapRelationCalculatorPhaseCorrelation != nullptr);
        }
        SECTION("Calculate()") {
            //TODO Write test code without using TCF Files
        }
    }
}