#include <catch2/catch.hpp>

#include "OverlapRelation.h"

namespace OverlapRelationTest {
    TEST_CASE("OverlapRelation") {
        SECTION("OverlapRelation()") {
            OverlapRelation overlapRelation;
            CHECK(&overlapRelation != nullptr);
        }
        SECTION("OverlapRelation(other)") {
            OverlapRelation srcOverlapRelation;
            srcOverlapRelation.SetReliability(0.1f);

            OverlapRelation destOverlapRelation(srcOverlapRelation);
            CHECK(destOverlapRelation.GetReliability() == 0.1f);
        }
        SECTION("operator=()") {
            OverlapRelation srcOverlapRelation;
            srcOverlapRelation.SetReliability(0.1f);

            OverlapRelation destOverlapRelation;
            destOverlapRelation = srcOverlapRelation;
            CHECK(destOverlapRelation.GetReliability() == 0.1f);
        }
        SECTION("SetShiftValue()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetShiftValue(1.f, 2.f, 3.f);
            CHECK(&overlapRelation != nullptr);
        }
        SECTION("GetShiftValueX()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetShiftValue(1.f, 2.f, 3.f);
            CHECK(overlapRelation.GetShiftValueX() == 1.f);
        }
        SECTION("GetShiftValueY()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetShiftValue(1.f, 2.f, 3.f);
            CHECK(overlapRelation.GetShiftValueY() == 2.f);
        }
        SECTION("GetShiftValueZ()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetShiftValue(1.f, 2.f, 3.f);
            CHECK(overlapRelation.GetShiftValueZ() == 3.f);
        }
        SECTION("SetReliability()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);
            CHECK(&overlapRelation != nullptr);
        }
        SECTION("GetReliability()") {
            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);
            CHECK(overlapRelation.GetReliability() == 0.1f);
        }
    }
}