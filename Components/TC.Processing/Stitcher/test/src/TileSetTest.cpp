#include <catch2/catch.hpp>

#include "TileSet.h"

namespace TileSetTest {
    TEST_CASE("TileSet") {
        SECTION("TileSet()") {
            TileSet tileSet;
            CHECK(&tileSet != nullptr);
        }
        SECTION("TileSet(other)") {
            Tile tile;
            tile.SetOverlapSize(1, 2);

            TileSet srcTileSet;
            srcTileSet.SetTileNumber(1, 1);
            srcTileSet.InsertTile(tile, 0, 0);

            TileSet destTileSet(srcTileSet);
            const auto resultTile = destTileSet.GetTile(0, 0);
            CHECK(resultTile.GetOverlapSizeX() == 1);
            CHECK(resultTile.GetOverlapSizeY() == 2);
        }
        SECTION("operator=()") {
            Tile tile;
            tile.SetOverlapSize(1, 2);

            TileSet srcTileSet;
            srcTileSet.SetTileNumber(1, 1);
            srcTileSet.InsertTile(tile, 0, 0);

            TileSet destTileSet;
            destTileSet = srcTileSet;
            const auto resultTile = destTileSet.GetTile(0, 0);
            CHECK(resultTile.GetOverlapSizeX() == 1);
            CHECK(resultTile.GetOverlapSizeY() == 2);
        }
        SECTION("SetTileNumber()") {
            TileSet tileSet;
            tileSet.SetTileNumber(1, 1);
            CHECK(&tileSet != nullptr);
        }
        SECTION("InsertTile()") {
            TileSet tileSet;
            tileSet.InsertTile(Tile{}, 0, 0);
            CHECK(&tileSet != nullptr);
        }
        SECTION("GetTile()") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            Tile tile00, tile01, tile10, tile11;
            tile00.SetOverlapSize(0, 0);
            tile01.SetOverlapSize(0, 1);
            tile10.SetOverlapSize(1, 0);
            tile11.SetOverlapSize(1, 1);

            TileSet tileSet;
            tileSet.SetTileNumber(tileNumberX, tileNumberY);
            tileSet.InsertTile(tile00, 0, 0);
            tileSet.InsertTile(tile01, 0, 1);
            tileSet.InsertTile(tile10, 1, 0);
            tileSet.InsertTile(tile11, 1, 1);

            const auto resultTile00 = tileSet.GetTile(0, 0);
            const auto resultTile01 = tileSet.GetTile(0, 1);
            const auto resultTile10 = tileSet.GetTile(1, 0);
            const auto resultTile11 = tileSet.GetTile(1, 1);

            CHECK(resultTile00.GetOverlapSizeX() == 0);
            CHECK(resultTile00.GetOverlapSizeY() == 0);

            CHECK(resultTile01.GetOverlapSizeX() == 0);
            CHECK(resultTile01.GetOverlapSizeY() == 1);

            CHECK(resultTile10.GetOverlapSizeX() == 1);
            CHECK(resultTile10.GetOverlapSizeY() == 0);

            CHECK(resultTile11.GetOverlapSizeX() == 1);
            CHECK(resultTile11.GetOverlapSizeY() == 1);
        }
    }
}