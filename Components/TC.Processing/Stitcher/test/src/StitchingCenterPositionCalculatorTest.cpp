#include <catch2/catch.hpp>

#include "StitchingCenterPositionCalculator.h"

namespace StitchingCenterPositionCalculatorTest {
    TEST_CASE("StitchingCenterPositionCalculator : unit test") {
        SECTION("StitchingCenterPositionCalculator()") {
            StitchingCenterPositionCalculator calculator;
            CHECK(&calculator != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            StitchingCenterPositionCalculator calculator;
            calculator.SetTileConfiguration({});
            CHECK(&calculator != nullptr);
        }
        SECTION("SetTilePositionSet()") {
            StitchingCenterPositionCalculator calculator;
            calculator.SetTilePositionSet({});
            CHECK(&calculator != nullptr);
        }
        SECTION("Calculate()") {
            StitchingCenterPositionCalculator calculator;

            SECTION("without setting") {
                CHECK(calculator.Calculate() == false);
            }

            SECTION("normal case") {
                TileConfiguration tileConfiguration;
                tileConfiguration.SetTileSizeInPixel(100, 100, 100);
                tileConfiguration.SetOverlapLengthInPixel(20, 20);
                tileConfiguration.SetTileNumber(1, 1);

                TilePosition tilePosition;
                tilePosition.SetPositions(0, 0, 0);

                TilePositionSet tilePositionSet;
                tilePositionSet.SetTileNumber(1, 1);
                tilePositionSet.InsertTilePosition(0, 0, tilePosition);

                calculator.SetTileConfiguration(tileConfiguration);
                calculator.SetTilePositionSet(tilePositionSet);

                CHECK(calculator.Calculate() == true);
            }

        }
        SECTION("GetCenterPositionX()") {
            StitchingCenterPositionCalculator calculator;
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(0, 0, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 49);
        }
        SECTION("GetCenterPositionY()") {
            StitchingCenterPositionCalculator calculator;
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(0, 0, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionY() == 49);
        }
    }
    TEST_CASE("StitchingCenterPositionCalculator : practical test") {
        SECTION("1x1 configuration") {
            StitchingCenterPositionCalculator calculator;
            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(1, 1);

            TilePosition tilePosition;
            tilePosition.SetPositions(0, 0, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 49);
            CHECK(calculator.GetCenterPositionY() == 49);
        }
        SECTION("1x2 configuration"){
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 1;
            constexpr int32_t tileNumberY = 2;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition01;
            tilePosition01.SetPositions(0, 82, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 49);
            CHECK(calculator.GetCenterPositionY() == 91);
        }
        SECTION("2x1 configuration") {
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 2;
            constexpr int32_t tileNumberY = 1;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition10;
            tilePosition10.SetPositions(82, 0, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 91);
            CHECK(calculator.GetCenterPositionY() == 49);
        }
        SECTION("2x2 configuration"){
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 2;
            constexpr int32_t tileNumberY = 2;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition10;
            tilePosition10.SetPositions(82, 0, 0);

            TilePosition tilePosition01;
            tilePosition01.SetPositions(0, 82, 0);

            TilePosition tilePosition11;
            tilePosition11.SetPositions(80, 80, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
            
            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 89);
            CHECK(calculator.GetCenterPositionY() == 89);
        }
        SECTION("2x3 configuration") {
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 2;
            constexpr int32_t tileNumberY = 3;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition10;
            tilePosition10.SetPositions(82, 0, 0);

            TilePosition tilePosition01;
            tilePosition01.SetPositions(0, 82, 0);

            TilePosition tilePosition11;
            tilePosition11.SetPositions(80, 80, 0);

            TilePosition tilePosition02;
            tilePosition02.SetPositions(1, 160, 0);

            TilePosition tilePosition12;
            tilePosition12.SetPositions(83, 161, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
            tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
            tilePositionSet.InsertTilePosition(1, 2, tilePosition12);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 89);
            CHECK(calculator.GetCenterPositionY() == 129);
        }
        SECTION("3x2 configuration"){
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 3;
            constexpr int32_t tileNumberY = 2;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition10;
            tilePosition10.SetPositions(82, 0, 0);

            TilePosition tilePosition20;
            tilePosition20.SetPositions(160, 1, 0);

            TilePosition tilePosition01;
            tilePosition01.SetPositions(0, 80, 0);

            TilePosition tilePosition11;
            tilePosition11.SetPositions(80, 82, 0);

            TilePosition tilePosition21;
            tilePosition21.SetPositions(162, 78, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
            tilePositionSet.InsertTilePosition(2, 1, tilePosition21);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 129);
            CHECK(calculator.GetCenterPositionY() == 91);
        }
        SECTION("3x3 configuration"){
            StitchingCenterPositionCalculator calculator;

            constexpr int32_t tileNumberX = 3;
            constexpr int32_t tileNumberY = 3;

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileSizeInPixel(100, 100, 100);
            tileConfiguration.SetOverlapLengthInPixel(20, 20);
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            tilePosition00.SetPositions(0, 0, 0);

            TilePosition tilePosition10;
            tilePosition10.SetPositions(82, 0, 0);

            TilePosition tilePosition20;
            tilePosition20.SetPositions(160, 1, 0);

            TilePosition tilePosition01;
            tilePosition01.SetPositions(0, 80, 0);

            TilePosition tilePosition11;
            tilePosition11.SetPositions(80, 82, 0);

            TilePosition tilePosition21;
            tilePosition21.SetPositions(162, 78, 0);

            TilePosition tilePosition02;
            tilePosition02.SetPositions(0, 160, 0);

            TilePosition tilePosition12;
            tilePosition12.SetPositions(80, 162, 0);

            TilePosition tilePosition22;
            tilePosition22.SetPositions(162, 158, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
            tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
            tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
            tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
            tilePositionSet.InsertTilePosition(2, 2, tilePosition22);

            calculator.SetTileConfiguration(tileConfiguration);
            calculator.SetTilePositionSet(tilePositionSet);

            calculator.Calculate();

            CHECK(calculator.GetCenterPositionX() == 129);
            CHECK(calculator.GetCenterPositionY() == 131);
        }


    }
}