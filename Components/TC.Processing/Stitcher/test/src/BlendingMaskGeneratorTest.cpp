#include <catch2/catch.hpp>

#include "BlendingMaskGenerator.h"

#include <QString>

#include "CompareArray.h"
#include "BinaryFileIO.h"

namespace BlendingMaskGeneratorTest {
    const QString testTopFolderPath = TEST_DATA_FOLDR_PATH;
    const std::string blendingMaskAnswerFilePath = (testTopFolderPath + "/StitchingData/blendingMask10_10_2_2.raw").toStdString();

    TEST_CASE("BlendingMaskGenerator : unit test") {
        SECTION("BlendingMaskGenerator()") {
            BlendingMaskGenerator generator;
            CHECK(&generator != nullptr);
        }
        SECTION("SetMaskSize()") {
            BlendingMaskGenerator generator;
            generator.SetMaskSize(1, 2);
            CHECK(&generator != nullptr);
        }
        SECTION("SetBlendingLength()") {
            BlendingMaskGenerator generator;
            generator.SetBlendingLength(1, 2);
            CHECK(&generator != nullptr);
        }
        SECTION("Generate()") {
            BlendingMaskGenerator generator;
            generator.SetMaskSize(10, 10);
            generator.SetBlendingLength(2, 2);
            CHECK(generator.Generate() == true);
        }
        SECTION("GetBlendingMask()") {
            BlendingMaskGenerator generator;
            generator.SetMaskSize(10, 10);
            generator.SetBlendingLength(2, 2);
            generator.Generate();

            const auto blendingMaskResult = generator.GetBlendingMask();

            std::shared_ptr<float[]> blendingMaskResultPtr{ new float[10 * 10]() };
            blendingMaskResult.host(blendingMaskResultPtr.get());

            const auto blendingMaskAnswer = ReadFile_float(blendingMaskAnswerFilePath, 10 * 10);

            CompareArray(blendingMaskResultPtr.get(), blendingMaskAnswer.get(), 10 * 10);
        }
    }
    TEST_CASE("BlendingMaskGenerator : practical test") {
        //TODO Implement test
    }
}