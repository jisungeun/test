#include <catch2/catch.hpp>

#include "ITileSetGenerator.h"

namespace ITleSetGeneratorTest {
    TEST_CASE("ITileSetGenerator") {
        class TileSetGeneratorForTest final : public ITileSetGenerator {
        public:
            TileSetGeneratorForTest() = default;
            ~TileSetGeneratorForTest() = default;

            auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override {
                setTileconfigurationTriggered = true;
            }

            auto Generate() -> TileSet override {
                generateTriggered = true;
                return TileSet{};
            }

            bool setTileconfigurationTriggered{ false };
            bool generateTriggered{ false };
        };

        SECTION("SetTileConfiguration()") {
            TileSetGeneratorForTest tileSetGenerator;
            CHECK(tileSetGenerator.setTileconfigurationTriggered == false);
            tileSetGenerator.SetTileConfiguration(TileConfiguration{});
            CHECK(tileSetGenerator.setTileconfigurationTriggered == true);
        }
        SECTION("Generate()") {
            TileSetGeneratorForTest tileSetGenerator;
            CHECK(tileSetGenerator.generateTriggered == false);
            tileSetGenerator.Generate();
            CHECK(tileSetGenerator.generateTriggered == true);
        }
    }
}