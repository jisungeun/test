#include <catch2/catch.hpp>

#include "StitchingMapGenerator.h"

namespace StitchingMapGeneratorTest {
    TEST_CASE("StitchingMapGenerator : unit test") {
        SECTION("StitchingMapGenerator") {
            StitchingMapGenerator generator;
            CHECK(&generator != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            StitchingMapGenerator generator;
            generator.SetTileConfiguration({});
            CHECK(&generator != nullptr);
        }
        SECTION("SetDataTilePositionSet()") {
            StitchingMapGenerator generator;
            generator.SetDataTilePositionSet({});
            CHECK(&generator != nullptr);
        }
        SECTION("GenerateMap()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1, 2, 3);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(1, 1);
            tileConfiguration.SetTileSizeInPixel(10, 10, 10);

            StitchingMapGenerator generator;
            generator.SetTileConfiguration(tileConfiguration);
            generator.SetDataTilePositionSet(tilePositionSet);
            CHECK(generator.GenerateMap() == true);
        }
        SECTION("GetStitchingMap()") {
            TilePosition tilePosition;
            tilePosition.SetPositions(1, 2, 3);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(1, 1);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition);

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(1, 1);
            tileConfiguration.SetTileSizeInPixel(10, 10, 10);

            StitchingMapGenerator generator;
            generator.SetTileConfiguration(tileConfiguration);
            generator.SetDataTilePositionSet(tilePositionSet);
            generator.GenerateMap();

            const auto stitchingMap = generator.GetStitchingMap();
            CHECK(stitchingMap.GetStitchedDataSizeX() == 10);
            CHECK(stitchingMap.GetStitchedDataSizeY() == 10);
            CHECK(stitchingMap.GetStitchedDataSizeZ() == 10);
        }
    }

    auto CompareIndexRange(const IndexRange& indexRange, const QList<int32_t>& answerArray)->bool {
        if (indexRange.GetX0() != answerArray[0]) { return false; }
        if (indexRange.GetX1() != answerArray[1]) { return false; }
        if (indexRange.GetY0() != answerArray[2]) { return false; }
        if (indexRange.GetY1() != answerArray[3]) { return false; }
        if (indexRange.GetZ0() != answerArray[4]) { return false; }
        if (indexRange.GetZ1() != answerArray[5]) { return false; }
        return true;
    }

    typedef std::tuple<QList<int32_t>, QList<int32_t>, QList<int32_t>> TileIndexAndRange;

    auto CompareStitchingTile(const StitchingTile& stitchingTile, const TileIndexAndRange& answer,
        const int32_t& targetIndex)->bool {
        const auto dataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();

        const auto dataTileIndexX = std::get<0>(dataTileIndexXYList[targetIndex]);
        const auto dataTileIndexY = std::get<1>(dataTileIndexXYList[targetIndex]);

        const auto answerDataTileIndexX = std::get<0>(answer)[0];
        const auto answerDataTileIndexY = std::get<0>(answer)[1];

        if (dataTileIndexX != answerDataTileIndexX) { return false; }
        if (dataTileIndexY != answerDataTileIndexY) { return false; }

        const auto resultIndexRangeToDataTile =
            stitchingTile.GetIndexRangeToDataTile(dataTileIndexX, dataTileIndexY);
        const auto resultIndexRangeToStitchingTile =
            stitchingTile.GetIndexRangeToStitchingTile(dataTileIndexX, dataTileIndexY);

        const auto answerIndexRangeToDataTile = std::get<1>(answer);
        const auto answerIndexRangeToStitchingTile = std::get<2>(answer);

        if (resultIndexRangeToDataTile.GetX0() != answerIndexRangeToDataTile[0]) { return false; }
        if (resultIndexRangeToDataTile.GetX1() != answerIndexRangeToDataTile[1]) { return false; }
        if (resultIndexRangeToDataTile.GetY0() != answerIndexRangeToDataTile[2]) { return false; }
        if (resultIndexRangeToDataTile.GetY1() != answerIndexRangeToDataTile[3]) { return false; }
        if (resultIndexRangeToDataTile.GetZ0() != answerIndexRangeToDataTile[4]) { return false; }
        if (resultIndexRangeToDataTile.GetZ1() != answerIndexRangeToDataTile[5]) { return false; }

        if (resultIndexRangeToStitchingTile.GetX0() != answerIndexRangeToStitchingTile[0]) { return false; }
        if (resultIndexRangeToStitchingTile.GetX1() != answerIndexRangeToStitchingTile[1]) { return false; }
        if (resultIndexRangeToStitchingTile.GetY0() != answerIndexRangeToStitchingTile[2]) { return false; }
        if (resultIndexRangeToStitchingTile.GetY1() != answerIndexRangeToStitchingTile[3]) { return false; }
        if (resultIndexRangeToStitchingTile.GetZ0() != answerIndexRangeToStitchingTile[4]) { return false; }
        if (resultIndexRangeToStitchingTile.GetZ1() != answerIndexRangeToStitchingTile[5]) { return false; }

        return true;
    }

    auto CompareStitchingTile(const StitchingTile& stitchingTile, const TileIndexAndRange& answer) {
        const auto dataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();
        if (dataTileIndexXYList.size() != 1) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer, 0)) { return false; }
        return true;
    }

    auto CompareStitchingTile(const StitchingTile& stitchingTile, const TileIndexAndRange& answer1,
        const TileIndexAndRange& answer2) {
        const auto dataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();
        if (dataTileIndexXYList.size() != 2) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer1, 0)) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer2, 1)) { return false; }
        return true;
    }

    auto CompareStitchingTile(const StitchingTile& stitchingTile, const TileIndexAndRange& answer1,
        const TileIndexAndRange& answer2, const TileIndexAndRange& answer3, const TileIndexAndRange& answer4) {
        const auto dataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();
        if (dataTileIndexXYList.size() != 4) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer1, 0)) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer2, 1)) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer3, 2)) { return false; }
        if (!CompareStitchingTile(stitchingTile, answer4, 3)) { return false; }
        return true;
    }

    TEST_CASE("StitchingMapGenerator : practical test") {
        SECTION("2x2") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            constexpr auto tileSizeX = 100;
            constexpr auto tileSizeY = 100;
            constexpr auto tileSizeZ = 20;

            constexpr auto overlapLengthX = 20;
            constexpr auto overlapLengthY = 20;

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);

            TilePosition tilePosition00;
            TilePosition tilePosition10;
            TilePosition tilePosition01;
            TilePosition tilePosition11;

            tilePosition00.SetPositions(-1, 2, 0);
            tilePosition10.SetPositions(80, 4, 0);
            tilePosition01.SetPositions(-2, 80, 0);
            tilePosition11.SetPositions(82, 84, 0);

            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
            tileConfiguration.SetOverlapLengthInPixel(overlapLengthX, overlapLengthY);
            tileConfiguration.SetTileSizeInPixel(tileSizeX, tileSizeY, tileSizeZ);

            StitchingMapGenerator generator;
            generator.SetDataTilePositionSet(tilePositionSet);
            generator.SetTileConfiguration(tileConfiguration);

            CHECK(generator.GenerateMap() == true);

            const auto stitchingMap = generator.GetStitchingMap();

            {
                const auto stitchingTileNumberX = stitchingMap.GetStitchingTileNumberX();
                const auto stitchingTileNumberY = stitchingMap.GetStitchingTileNumberY();

                CHECK(stitchingTileNumberX == 3);
                CHECK(stitchingTileNumberY == 3);

                const auto stitchingTile00 = stitchingMap.GetStitchingTile(0, 0);
                const auto stitchingTile10 = stitchingMap.GetStitchingTile(1, 0);
                const auto stitchingTile20 = stitchingMap.GetStitchingTile(2, 0);
                const auto stitchingTile01 = stitchingMap.GetStitchingTile(0, 1);
                const auto stitchingTile11 = stitchingMap.GetStitchingTile(1, 1);
                const auto stitchingTile21 = stitchingMap.GetStitchingTile(2, 1);
                const auto stitchingTile02 = stitchingMap.GetStitchingTile(0, 2);
                const auto stitchingTile12 = stitchingMap.GetStitchingTile(1, 2);
                const auto stitchingTile22 = stitchingMap.GetStitchingTile(2, 2);

                //TODO Write test
                //CHECK(CompareStitchingTile(stitchingTile00, { {0,0},{1,81,0,77,0,19},{} }));
                //CHECK(CompareStitchingTile(stitchingTile10, { {0,0},{82,100,0,77,0,19},{} }, { {1,0},{82,100,2,77,0,19},{} }));
                //CHECK(CompareStitchingTile(stitchingTile20, { {1,0},{101,181,2,77,0,19},{} }));
                //CHECK(CompareStitchingTile(stitchingTile01, { {0,0},{1,81,78,99,0,19},{} }, { {0,1},{0,81,101,0,19},{} }));
                //CHECK(CompareStitchingTile(stitchingTile11, { {0,0},{84,100,78,99,0,19},{} }, { {1,0},{84,100,82,101,0,19},{} }, { {0,1},{82,99,78,101,0,19},{} }, { {1,1},{},{} }));
                //CHECK(CompareStitchingTile(stitchingTile21, { {1,0},{},{} }, { {1,1},{},{} }));
                //CHECK(CompareStitchingTile(stitchingTile02, { {0,1},{},{} }));
                //CHECK(CompareStitchingTile(stitchingTile12, { {0,1},{},{} }, { {1,1},{},{} }));
                //CHECK(CompareStitchingTile(stitchingTile22, { {1,1},{},{} }));


                const auto indexRange00 = stitchingMap.GetIndexRange(0, 0);
                const auto indexRange10 = stitchingMap.GetIndexRange(1, 0);
                const auto indexRange20 = stitchingMap.GetIndexRange(2, 0);
                const auto indexRange01 = stitchingMap.GetIndexRange(0, 1);
                const auto indexRange11 = stitchingMap.GetIndexRange(1, 1);
                const auto indexRange21 = stitchingMap.GetIndexRange(2, 1);
                const auto indexRange02 = stitchingMap.GetIndexRange(0, 2);
                const auto indexRange12 = stitchingMap.GetIndexRange(1, 2);
                const auto indexRange22 = stitchingMap.GetIndexRange(2, 2);

                CHECK(CompareIndexRange(indexRange00, { 0,81,0,77,0,19 }));
                CHECK(CompareIndexRange(indexRange10, { 82,100,0,77,0,19 }));
                CHECK(CompareIndexRange(indexRange20, { 101,183,0,77,0,19 }));

                CHECK(CompareIndexRange(indexRange01, { 0,81,78,101,0,19 }));
                CHECK(CompareIndexRange(indexRange11, { 82,100,78,101,0,19 }));
                CHECK(CompareIndexRange(indexRange21, { 101,183,78,101,0,19 }));

                CHECK(CompareIndexRange(indexRange02, { 0,81,102,181,0,19 }));
                CHECK(CompareIndexRange(indexRange12, { 82,100,102,181,0,19 }));
                CHECK(CompareIndexRange(indexRange22, { 101,183,102,181,0,19 }));

                const auto stitchedDataSizeX = stitchingMap.GetStitchedDataSizeX();
                const auto stitchedDataSizeY = stitchingMap.GetStitchedDataSizeY();
                const auto stitchedDataSizeZ = stitchingMap.GetStitchedDataSizeZ();

                CHECK(stitchedDataSizeX == 184);
                CHECK(stitchedDataSizeY == 182);
                CHECK(stitchedDataSizeZ == 20);

                const auto centerPositionX = stitchingMap.GetCenterPositionX();
                const auto centerPositionY = stitchingMap.GetCenterPositionY();

                CHECK(centerPositionX == 93);
                CHECK(centerPositionY == 91);
            }
        }
        SECTION("3x3") {
            constexpr auto tileSizeX = 100;
            constexpr auto tileSizeY = 100;
            constexpr auto tileSizeZ = 20;

            constexpr auto tileNumberX = 3;
            constexpr auto tileNumberY = 4;

            TilePosition tilePosition00, tilePosition10, tilePosition20,
                tilePosition01, tilePosition11, tilePosition21,
                tilePosition02, tilePosition12, tilePosition22,
                tilePosition03, tilePosition13, tilePosition23;

            tilePosition00.SetPositions(-1, 2, 0);
            tilePosition10.SetPositions(79, 3, 0);
            tilePosition20.SetPositions(162, 4, 0);
            tilePosition01.SetPositions(0, 81, 0);
            tilePosition11.SetPositions(83, 85, 0);
            tilePosition21.SetPositions(159, 85, 0);
            tilePosition02.SetPositions(3, 164, 0);
            tilePosition12.SetPositions(85, 160, 0);
            tilePosition22.SetPositions(167, 162, 0);
            tilePosition03.SetPositions(-2, 247, 0);
            tilePosition13.SetPositions(80, 240, 0);
            tilePosition23.SetPositions(160, 235, 0);

            TilePositionSet tilePositionSet;
            tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
            tilePositionSet.InsertTilePosition(0, 0, tilePosition00);
            tilePositionSet.InsertTilePosition(1, 0, tilePosition10);
            tilePositionSet.InsertTilePosition(2, 0, tilePosition20);
            tilePositionSet.InsertTilePosition(0, 1, tilePosition01);
            tilePositionSet.InsertTilePosition(1, 1, tilePosition11);
            tilePositionSet.InsertTilePosition(2, 1, tilePosition21);
            tilePositionSet.InsertTilePosition(0, 2, tilePosition02);
            tilePositionSet.InsertTilePosition(1, 2, tilePosition12);
            tilePositionSet.InsertTilePosition(2, 2, tilePosition22);
            tilePositionSet.InsertTilePosition(0, 3, tilePosition03);
            tilePositionSet.InsertTilePosition(1, 3, tilePosition13);
            tilePositionSet.InsertTilePosition(2, 3, tilePosition23);

            TileConfiguration tileConfiguration;
            tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
            tileConfiguration.SetTileSizeInPixel(tileSizeX, tileSizeY, tileSizeZ);

            StitchingMapGenerator generator;
            generator.SetDataTilePositionSet(tilePositionSet);
            generator.SetTileConfiguration(tileConfiguration);
            CHECK(generator.GenerateMap() == true);

            const auto stitchingMap = generator.GetStitchingMap();

            CHECK(stitchingMap.GetStitchedDataSizeX() == 269);
            CHECK(stitchingMap.GetStitchedDataSizeY() == 345);
            CHECK(stitchingMap.GetStitchedDataSizeZ() == 20);

            const auto indexRange00 = stitchingMap.GetIndexRange(0, 0);
            const auto indexRange10 = stitchingMap.GetIndexRange(1, 0);
            const auto indexRange20 = stitchingMap.GetIndexRange(2, 0);
            const auto indexRange30 = stitchingMap.GetIndexRange(3, 0);
            const auto indexRange40 = stitchingMap.GetIndexRange(4, 0);
            CHECK(CompareIndexRange(indexRange00, { 0,80,    0,78, 0,19 }));
            CHECK(CompareIndexRange(indexRange10, { 81,104,  0,78, 0,19 }));
            CHECK(CompareIndexRange(indexRange20, { 105,160, 0,78, 0,19 }));
            CHECK(CompareIndexRange(indexRange30, { 161,186, 0,78, 0,19 }));
            CHECK(CompareIndexRange(indexRange40, { 187,268, 0,78, 0,19 }));

            const auto indexRange01 = stitchingMap.GetIndexRange(0, 1);
            const auto indexRange11 = stitchingMap.GetIndexRange(1, 1);
            const auto indexRange21 = stitchingMap.GetIndexRange(2, 1);
            const auto indexRange31 = stitchingMap.GetIndexRange(3, 1);
            const auto indexRange41 = stitchingMap.GetIndexRange(4, 1);
            CHECK(CompareIndexRange(indexRange01, { 0,80,    79,101, 0,19 }));
            CHECK(CompareIndexRange(indexRange11, { 81,104,  79,101, 0,19 }));
            CHECK(CompareIndexRange(indexRange21, { 105,160, 79,101, 0,19 }));
            CHECK(CompareIndexRange(indexRange31, { 161,186, 79,101, 0,19 }));
            CHECK(CompareIndexRange(indexRange41, { 187,268, 79,101, 0,19 }));

            const auto indexRange02 = stitchingMap.GetIndexRange(0, 2);
            const auto indexRange12 = stitchingMap.GetIndexRange(1, 2);
            const auto indexRange22 = stitchingMap.GetIndexRange(2, 2);
            const auto indexRange32 = stitchingMap.GetIndexRange(3, 2);
            const auto indexRange42 = stitchingMap.GetIndexRange(4, 2);
            CHECK(CompareIndexRange(indexRange02, { 0,80,    102,157, 0,19 }));
            CHECK(CompareIndexRange(indexRange12, { 81,104,  102,157, 0,19 }));
            CHECK(CompareIndexRange(indexRange22, { 105,160, 102,157, 0,19 }));
            CHECK(CompareIndexRange(indexRange32, { 161,186, 102,157, 0,19 }));
            CHECK(CompareIndexRange(indexRange42, { 187,268, 102,157, 0,19 }));

            const auto indexRange03 = stitchingMap.GetIndexRange(0, 3);
            const auto indexRange13 = stitchingMap.GetIndexRange(1, 3);
            const auto indexRange23 = stitchingMap.GetIndexRange(2, 3);
            const auto indexRange33 = stitchingMap.GetIndexRange(3, 3);
            const auto indexRange43 = stitchingMap.GetIndexRange(4, 3);
            CHECK(CompareIndexRange(indexRange03, { 0,80,    158,182, 0,19 }));
            CHECK(CompareIndexRange(indexRange13, { 81,104,  158,182, 0,19 }));
            CHECK(CompareIndexRange(indexRange23, { 105,160, 158,182, 0,19 }));
            CHECK(CompareIndexRange(indexRange33, { 161,186, 158,182, 0,19 }));
            CHECK(CompareIndexRange(indexRange43, { 187,268, 158,182, 0,19 }));

            const auto indexRange04 = stitchingMap.GetIndexRange(0, 4);
            const auto indexRange14 = stitchingMap.GetIndexRange(1, 4);
            const auto indexRange24 = stitchingMap.GetIndexRange(2, 4);
            const auto indexRange34 = stitchingMap.GetIndexRange(3, 4);
            const auto indexRange44 = stitchingMap.GetIndexRange(4, 4);
            CHECK(CompareIndexRange(indexRange04, { 0,80,    183,232, 0,19 }));
            CHECK(CompareIndexRange(indexRange14, { 81,104,  183,232, 0,19 }));
            CHECK(CompareIndexRange(indexRange24, { 105,160, 183,232, 0,19 }));
            CHECK(CompareIndexRange(indexRange34, { 161,186, 183,232, 0,19 }));
            CHECK(CompareIndexRange(indexRange44, { 187,268, 183,232, 0,19 }));

            const auto indexRange05 = stitchingMap.GetIndexRange(0, 5);
            const auto indexRange15 = stitchingMap.GetIndexRange(1, 5);
            const auto indexRange25 = stitchingMap.GetIndexRange(2, 5);
            const auto indexRange35 = stitchingMap.GetIndexRange(3, 5);
            const auto indexRange45 = stitchingMap.GetIndexRange(4, 5);
            CHECK(CompareIndexRange(indexRange05, { 0,80,    233,261, 0,19 }));
            CHECK(CompareIndexRange(indexRange15, { 81,104,  233,261, 0,19 }));
            CHECK(CompareIndexRange(indexRange25, { 105,160, 233,261, 0,19 }));
            CHECK(CompareIndexRange(indexRange35, { 161,186, 233,261, 0,19 }));
            CHECK(CompareIndexRange(indexRange45, { 187,268, 233,261, 0,19 }));

            const auto indexRange06 = stitchingMap.GetIndexRange(0, 6);
            const auto indexRange16 = stitchingMap.GetIndexRange(1, 6);
            const auto indexRange26 = stitchingMap.GetIndexRange(2, 6);
            const auto indexRange36 = stitchingMap.GetIndexRange(3, 6);
            const auto indexRange46 = stitchingMap.GetIndexRange(4, 6);
            CHECK(CompareIndexRange(indexRange06, { 0,80,    262,344, 0,19 }));
            CHECK(CompareIndexRange(indexRange16, { 81,104,  262,344, 0,19 }));
            CHECK(CompareIndexRange(indexRange26, { 105,160, 262,344, 0,19 }));
            CHECK(CompareIndexRange(indexRange36, { 161,186, 262,344, 0,19 }));
            CHECK(CompareIndexRange(indexRange46, { 187,268, 262,344, 0,19 }));

            const auto stitchingTile00 = stitchingMap.GetStitchingTile(0, 0);
            const auto stitchingTile10 = stitchingMap.GetStitchingTile(1, 0);
            const auto stitchingTile20 = stitchingMap.GetStitchingTile(2, 0);
            const auto stitchingTile30 = stitchingMap.GetStitchingTile(3, 0);
            const auto stitchingTile40 = stitchingMap.GetStitchingTile(4, 0);
            CHECK(CompareStitchingTile(stitchingTile00, { {0,0},{0,79,0,78,0,19},{1,80,0,78,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile10, { {0,0},{80,99,0,78,0,19},{0,19,0,78,0,19} }, { {1,0},{0,23,0,77,0,19},{0,23,1,78,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile20, { {1,0},{24,79,0,77,0,19},{0,55,1,78,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile30, { {1,0},{80,99,0,77,0,19},{0,19,1,78,0,19} }, { {2,0},{0,22,0,76,0,19},{3,25,2,78,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile40, { {2,0},{23,99,0,76,0,19},{0,76,2,78,0,19} }));

            const auto stitchingTile01 = stitchingMap.GetStitchingTile(0, 1);
            const auto stitchingTile11 = stitchingMap.GetStitchingTile(1, 1);
            const auto stitchingTile21 = stitchingMap.GetStitchingTile(2, 1);
            const auto stitchingTile31 = stitchingMap.GetStitchingTile(3, 1);
            const auto stitchingTile41 = stitchingMap.GetStitchingTile(4, 1);
            CHECK(CompareStitchingTile(stitchingTile01, { {0,0},{0,79,79,99,0,19},{1,80,0,20,0,19} }, { {0,1},{0,78,0,22,0,19},{2,80,0,22,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile11, { {0,0},{80,99,79,99,0,19},{0,19,0,20,0,19} }, { {1,0},{0,23,78,99,0,19},{0,23,0,21,0,19} }, { {0,1},{79,99,0,22,0,19},{0,20,0,22,0,19} }, { {1,1},{0,19,0,18,0,19},{4,23,4,22,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile21, { {1,0},{24,79,78,99,0,19},{0,55,0,21,0,19} }, { {1,1},{20,75,0,18,0,19},{0,55,4,22,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile31, { {1,0},{80,99,78,99,0,19},{0,19,0,21,0,19} }, { {2,0},{0,22,77,99,0,19},{3,25,0,22,0,19} }, { {1,1},{76,99,0,18,0,19},{0,23,4,22,0,19} }, { {2,1},{0,25,0,18,0,19},{0,25,4,22,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile41, { {2,0},{23,99,77,99,0,19},{0,76,0,22,0,19} }, { {2,1},{26,99,0,18,0,19},{0,73,4,22,0,19} }));

            const auto stitchingTile02 = stitchingMap.GetStitchingTile(0, 2);
            const auto stitchingTile12 = stitchingMap.GetStitchingTile(1, 2);
            const auto stitchingTile22 = stitchingMap.GetStitchingTile(2, 2);
            const auto stitchingTile32 = stitchingMap.GetStitchingTile(3, 2);
            const auto stitchingTile42 = stitchingMap.GetStitchingTile(4, 2);
            CHECK(CompareStitchingTile(stitchingTile02, { {0,1},{0,78,23,78,0,19},{2,80,0,55,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile12, { {0,1},{79,99,23,78,0,19},{0,20,0,55,0,19} }, { {1,1},{0,19,19,74,0,19},{4,23,0,55,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile22, { {1,1},{20,75,19,74,0,19},{0,55,0,55,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile32, { {1,1},{76,99,19,74,0,19},{0,23,0,55,0,19} }, { {2,1},{0,25,19,74,0,19},{0,25,0,55,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile42, { {2,1},{26,99,19,74,0,19},{0,73,0,55,0,19} }));

            const auto stitchingTile03 = stitchingMap.GetStitchingTile(0, 3);
            const auto stitchingTile13 = stitchingMap.GetStitchingTile(1, 3);
            const auto stitchingTile23 = stitchingMap.GetStitchingTile(2, 3);
            const auto stitchingTile33 = stitchingMap.GetStitchingTile(3, 3);
            const auto stitchingTile43 = stitchingMap.GetStitchingTile(4, 3);
            CHECK(CompareStitchingTile(stitchingTile03, { {0,1},{0,78,79,99,0,19},{2,80,0,20,0,19} }, { {0,2},{0,75,0,20,0,19},{5,80,4,24,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile13, { {0,1},{79,99,79,99,0,19},{0,20,0,20,0,19} }, { {1,1},{0,19,75,99,0,19},{4,23,0,24,0,19} }, { {0,2},{76,99,0,20,0,19},{0,23,4,24,0,19} }, { {1,2},{0,17,0,24,0,19},{6,23,0,24,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile23, { {1,1},{20,75,75,99,0,19},{0,55,0,24,0,19} }, { {1,2},{18,73,0,24,0,19},{0,55,0,24,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile33, { {1,1},{76,99,75,99,0,19},{0,23,0,24,0,19} }, { {2,1},{0,25,75,99,0,19},{0,25,0,24,0,19} }, { {1,2},{74,99,0,24,0,19},{0,25,0,24,0,19} }, { {2,2},{0,17,0,22,0,19},{8,25,2,24,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile43, { {2,1},{26,99,75,99,0,19},{0,73,0,24,0,19} }, { {2,2},{18,99,0,22,0,19},{0,81,2,24,0,19} }));

            const auto stitchingTile04 = stitchingMap.GetStitchingTile(0, 4);
            const auto stitchingTile14 = stitchingMap.GetStitchingTile(1, 4);
            const auto stitchingTile24 = stitchingMap.GetStitchingTile(2, 4);
            const auto stitchingTile34 = stitchingMap.GetStitchingTile(3, 4);
            const auto stitchingTile44 = stitchingMap.GetStitchingTile(4, 4);
            CHECK(CompareStitchingTile(stitchingTile04, { {0,2},{0,75,21,70,0,19},{5,80,0,49,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile14, { {0,2},{76,99,21,70,0,19},{0,23,0,49,0,19} }, { {1,2},{0,17,25,74,0,19},{6,23,0,49,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile24, { {1,2},{18,73,25,74,0,19},{0,55,0,49,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile34, { {1,2},{74,99,25,74,0,19},{0,25,0,49,0,19} }, { {2,2},{0,17,23,72,0,19},{8,25,0,49,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile44, { {2,2},{18,99,23,72,0,19},{0,81,0,49,0,19} }));

            const auto stitchingTile05 = stitchingMap.GetStitchingTile(0, 5);
            const auto stitchingTile15 = stitchingMap.GetStitchingTile(1, 5);
            const auto stitchingTile25 = stitchingMap.GetStitchingTile(2, 5);
            const auto stitchingTile35 = stitchingMap.GetStitchingTile(3, 5);
            const auto stitchingTile45 = stitchingMap.GetStitchingTile(4, 5);
            CHECK(CompareStitchingTile(stitchingTile05, { {0,2},{0,75,71,99,0,19},{5,80,0,28,0,19} }, { {0,3},{0,80,0,16,0,19},{0,80,12,28,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile15, { {0,2},{76,99,71,99,0,19},{0,23,0,28,0,19} }, { {1,2},{0,17,75,99,0,19},{6,23,0,24,0,19} }, { {0,3},{81,99,0,16,0,19},{0,18,12,28,0,19} }, { {1,3},{0,22,0,23,0,19},{1,23,5,28,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile25, { {1,2},{18,73,75,99,0,19},{0,55,0,24,0,19} }, { {1,3},{23,78,0,23,0,19},{0,55,5,28,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile35, { {1,2},{74,99,75,99,0,19},{0,25,0,24,0,19} }, { {2,2},{0,17,73,99,0,19},{8,25,0,26,0,19} }, { {1,3},{79,99,0,23,0,19},{0,20,5,28,0,19} }, { {2,3},{0,24,0,28,0,19},{1,25,0,28,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile45, { {2,2},{18,99,73,99,0,19},{0,81,0,26,0,19} }, { {2,3},{25,99,0,28,0,19},{0,74,0,28,0,19} }));

            const auto stitchingTile06 = stitchingMap.GetStitchingTile(0, 6);
            const auto stitchingTile16 = stitchingMap.GetStitchingTile(1, 6);
            const auto stitchingTile26 = stitchingMap.GetStitchingTile(2, 6);
            const auto stitchingTile36 = stitchingMap.GetStitchingTile(3, 6);
            const auto stitchingTile46 = stitchingMap.GetStitchingTile(4, 6);
            CHECK(CompareStitchingTile(stitchingTile06, { {0,3},{0,80,17,99,0,19},{0,80,0,82,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile16, { {0,3},{81,99,17,99,0,19},{0,18,0,82,0,19} }, { {1,3},{0,22,24,99,0,19},{1,23,0,75,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile26, { {1,3},{23,78,24,99,0,19},{0,55,0,75,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile36, { {1,3},{79,99,24,99,0,19},{0,20,0,75,0,19} }, { {2,3},{0,24,29,99,0,19},{1,25,0,70,0,19} }));
            CHECK(CompareStitchingTile(stitchingTile46, { {2,3},{25,99,29,99,0,19},{0,74,0,70,0,19} }));           
        }
 
    }
}