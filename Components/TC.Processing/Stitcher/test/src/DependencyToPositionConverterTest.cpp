#include <catch2/catch.hpp>

#include "DependencyToPositionConverter.h"

namespace DependencyToPositionConverterTest {
    TEST_CASE("DependencyToPositionConverter") {
        SECTION("DependencyToPositionConverter()") {
            DependencyToPositionConverter dependencyToPositionConverter;
            CHECK(&dependencyToPositionConverter != nullptr);
        }
        SECTION("SetTileConfiguration()") {
            DependencyToPositionConverter dependencyToPositionConverter;
            dependencyToPositionConverter.SetTileConfiguration({});
            CHECK(&dependencyToPositionConverter != nullptr);
        }
        SECTION("SetOverlapRelationSet()") {
            DependencyToPositionConverter dependencyToPositionConverter;
            dependencyToPositionConverter.SetOverlapRelationSet({});
            CHECK(&dependencyToPositionConverter != nullptr);
        }
        SECTION("SetTileDependencyRepository()") {
            DependencyToPositionConverter dependencyToPositionConverter;
            dependencyToPositionConverter.SetTileDependencyRepository({});
            CHECK(&dependencyToPositionConverter != nullptr);
        }
        SECTION("Convert()") {
            SECTION("2x2 tile, non inversion") {
                constexpr int32_t tileNumberX = 2;
                constexpr int32_t tileNumberY = 2;

                TileConfiguration tileConfiguration;
                tileConfiguration.SetOverlapLengthInPixel(10, 10);
                tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
                tileConfiguration.SetTileSizeInPixel(50, 50, 20);

                OverlapRelation overlapRelation10;
                OverlapRelation overlapRelation01;
                OverlapRelation overlapRelation21;
                OverlapRelation overlapRelation12;
                overlapRelation10.SetShiftValue(1, 1, 0);
                overlapRelation01.SetShiftValue(2, 2, 0);
                overlapRelation21.SetShiftValue(3, 3, 0);
                overlapRelation12.SetShiftValue(4, 4, 0);

                OverlapRelationSet overlapRelationSet;
                overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
                overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation10);
                overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation01);
                overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation21);
                overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation12);

                TileDependencyRepository tileDependencyRepository;
                tileDependencyRepository.SetTileNumber(tileNumberX, tileNumberY);
                tileDependencyRepository.Initialize();
                tileDependencyRepository.SetReference({ 1,0 }, { 0,0 });
                tileDependencyRepository.SetReference({ 1,1 }, { 1,0 });
                tileDependencyRepository.SetReference({ 0,1 }, { 0,0 });

                DependencyToPositionConverter dependencyToPositionConverter;
                dependencyToPositionConverter.SetTileConfiguration(tileConfiguration);
                dependencyToPositionConverter.SetOverlapRelationSet(overlapRelationSet);
                dependencyToPositionConverter.SetTileDependencyRepository(tileDependencyRepository);

                const auto tilePositionSet = dependencyToPositionConverter.Convert();

                const auto tilePosition00 = tilePositionSet.GetTilePosition(0, 0);
                const auto tilePosition01 = tilePositionSet.GetTilePosition(0, 1);
                const auto tilePosition10 = tilePositionSet.GetTilePosition(1, 0);
                const auto tilePosition11 = tilePositionSet.GetTilePosition(1, 1);

                CHECK(tilePosition00.GetTilePositionX() == 0);
                CHECK(tilePosition00.GetTilePositionY() == 0);
                CHECK(tilePosition00.GetTilePositionZ() == 0);

                CHECK(tilePosition01.GetTilePositionX() == 2);
                CHECK(tilePosition01.GetTilePositionY() == 42);
                CHECK(tilePosition01.GetTilePositionZ() == 0);

                CHECK(tilePosition10.GetTilePositionX() == 41);
                CHECK(tilePosition10.GetTilePositionY() == 1);
                CHECK(tilePosition10.GetTilePositionZ() == 0);

                CHECK(tilePosition11.GetTilePositionX() == 44);
                CHECK(tilePosition11.GetTilePositionY() == 44);
                CHECK(tilePosition11.GetTilePositionZ() == 0);
            }

            SECTION("3x3 tile, inversion") {
                constexpr int32_t tileNumberX = 3;
                constexpr int32_t tileNumberY = 3;

                TileConfiguration tileConfiguration;
                tileConfiguration.SetOverlapLengthInPixel(10, 10);
                tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
                tileConfiguration.SetTileSizeInPixel(50, 50, 20);

                OverlapRelation overlapRelation10;
                OverlapRelation overlapRelation30;
                OverlapRelation overlapRelation01;
                OverlapRelation overlapRelation21;
                OverlapRelation overlapRelation41;
                OverlapRelation overlapRelation12;
                OverlapRelation overlapRelation32;
                OverlapRelation overlapRelation03;
                OverlapRelation overlapRelation23;
                OverlapRelation overlapRelation43;
                OverlapRelation overlapRelation14;
                OverlapRelation overlapRelation34;

                overlapRelation10.SetShiftValue(1, 1, 0);
                overlapRelation30.SetShiftValue(1, 1, 0);
                overlapRelation01.SetShiftValue(1, 1, 0);
                overlapRelation21.SetShiftValue(1, 1, 0);
                overlapRelation41.SetShiftValue(2, 2, 0);
                overlapRelation12.SetShiftValue(2, 2, 0);
                overlapRelation32.SetShiftValue(2, 2, 0);
                overlapRelation03.SetShiftValue(2, 2, 0);
                overlapRelation23.SetShiftValue(3, 3, 0);
                overlapRelation43.SetShiftValue(3, 3, 0);
                overlapRelation14.SetShiftValue(3, 3, 0);
                overlapRelation34.SetShiftValue(3, 3, 0);

                OverlapRelationSet overlapRelationSet;
                overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
                overlapRelationSet.InsertOverlapRelation(1, 0, overlapRelation10);
                overlapRelationSet.InsertOverlapRelation(3, 0, overlapRelation30);
                overlapRelationSet.InsertOverlapRelation(0, 1, overlapRelation01);
                overlapRelationSet.InsertOverlapRelation(2, 1, overlapRelation21);
                overlapRelationSet.InsertOverlapRelation(4, 1, overlapRelation41);
                overlapRelationSet.InsertOverlapRelation(1, 2, overlapRelation12);
                overlapRelationSet.InsertOverlapRelation(3, 2, overlapRelation32);
                overlapRelationSet.InsertOverlapRelation(0, 3, overlapRelation03);
                overlapRelationSet.InsertOverlapRelation(2, 3, overlapRelation23);
                overlapRelationSet.InsertOverlapRelation(4, 3, overlapRelation43);
                overlapRelationSet.InsertOverlapRelation(1, 4, overlapRelation14);
                overlapRelationSet.InsertOverlapRelation(3, 4, overlapRelation34);


                TileDependencyRepository tileDependencyRepository;
                tileDependencyRepository.SetTileNumber(tileNumberX, tileNumberY);
                tileDependencyRepository.Initialize();
                tileDependencyRepository.SetReference({ 0,0 }, { 1,0 });
                tileDependencyRepository.SetReference({ 1,0 }, { 2,0 });
                tileDependencyRepository.SetReference({ 2,0 }, { 2,1 });
                tileDependencyRepository.SetReference({ 2,1 }, { 2,2 });
                tileDependencyRepository.SetReference({ 2,2 }, { 1,2 });
                tileDependencyRepository.SetReference({ 1,2 }, { 1,1 });
                tileDependencyRepository.SetReference({ 1,1 }, { 0,1 });
                tileDependencyRepository.SetReference({ 0,1 }, { 0,2 });

                DependencyToPositionConverter dependencyToPositionConverter;
                dependencyToPositionConverter.SetTileConfiguration(tileConfiguration);
                dependencyToPositionConverter.SetOverlapRelationSet(overlapRelationSet);
                dependencyToPositionConverter.SetTileDependencyRepository(tileDependencyRepository);

                const auto tilePositionSet = dependencyToPositionConverter.Convert();

                const auto tilePosition00 = tilePositionSet.GetTilePosition(0, 0);
                const auto tilePosition10 = tilePositionSet.GetTilePosition(1, 0);
                const auto tilePosition20 = tilePositionSet.GetTilePosition(2, 0);
                const auto tilePosition01 = tilePositionSet.GetTilePosition(0, 1);
                const auto tilePosition11 = tilePositionSet.GetTilePosition(1, 1);
                const auto tilePosition21 = tilePositionSet.GetTilePosition(2, 1);
                const auto tilePosition02 = tilePositionSet.GetTilePosition(0, 2);
                const auto tilePosition12 = tilePositionSet.GetTilePosition(1, 2);
                const auto tilePosition22 = tilePositionSet.GetTilePosition(2, 2);

                CHECK(tilePosition00.GetTilePositionX() == 0);
                CHECK(tilePosition00.GetTilePositionY() == 0);
                CHECK(tilePosition00.GetTilePositionZ() == 0);

                CHECK(tilePosition10.GetTilePositionX() == 41);
                CHECK(tilePosition10.GetTilePositionY() == 1);
                CHECK(tilePosition10.GetTilePositionZ() == 0);

                CHECK(tilePosition20.GetTilePositionX() == 82);
                CHECK(tilePosition20.GetTilePositionY() == 2);
                CHECK(tilePosition20.GetTilePositionZ() == 0);

                CHECK(tilePosition01.GetTilePositionX() == -1);
                CHECK(tilePosition01.GetTilePositionY() == 39);
                CHECK(tilePosition01.GetTilePositionZ() == 0);

                CHECK(tilePosition11.GetTilePositionX() == 41);
                CHECK(tilePosition11.GetTilePositionY() == 41);
                CHECK(tilePosition11.GetTilePositionZ() == 0);

                CHECK(tilePosition21.GetTilePositionX() == 84);
                CHECK(tilePosition21.GetTilePositionY() == 44);
                CHECK(tilePosition21.GetTilePositionZ() == 0);

                CHECK(tilePosition02.GetTilePositionX() == 1);
                CHECK(tilePosition02.GetTilePositionY() == 81);
                CHECK(tilePosition02.GetTilePositionZ() == 0);

                CHECK(tilePosition12.GetTilePositionX() == 44);
                CHECK(tilePosition12.GetTilePositionY() == 84);
                CHECK(tilePosition12.GetTilePositionZ() == 0);

                CHECK(tilePosition22.GetTilePositionX() == 87);
                CHECK(tilePosition22.GetTilePositionY() == 87);
                CHECK(tilePosition22.GetTilePositionZ() == 0);


            }

        }
    }
}