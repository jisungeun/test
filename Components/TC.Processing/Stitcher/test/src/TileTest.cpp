#include <catch2/catch.hpp>

#include "Tile.h"

namespace TileTest {
    class TileDataGetterForTest final :public ITileDataGetter {
    public:
        TileDataGetterForTest() = default;
        ~TileDataGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1.f;
            return data;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
            const int32_t& z1) const -> std::shared_ptr<float[]> override {
            this->x0In = x0;
            this->x1In = x1;
            this->y0In = y0;
            this->y1In = y1;
            this->z0In = z0;
            this->z1In = z1;

            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 2.f;
            return data;
        }

        mutable int32_t x0In{}, x1In{}, y0In{}, y1In{}, z0In{}, z1In{};
    };

    TEST_CASE("Tile") {
        SECTION("Tile()") {
            Tile tile;
            CHECK(&tile != nullptr);
        }
        SECTION("Tile(other)") {
            Tile srcTile;
            srcTile.SetTileSize(1, 2, 3);
            Tile destTile(srcTile);
            CHECK(destTile.GetTileSizeX() == 1);
            CHECK(destTile.GetTileSizeY() == 2);
            CHECK(destTile.GetTileSizeZ() == 3);
        }
        SECTION("operator=()") {
            Tile srcTile;
            srcTile.SetTileSize(1, 2, 3);
            Tile destTile;
            destTile = srcTile;
            CHECK(destTile.GetTileSizeX() == 1);
            CHECK(destTile.GetTileSizeY() == 2);
            CHECK(destTile.GetTileSizeZ() == 3);
        }
        SECTION("SetTileSize()") {
            Tile tile;
            tile.SetTileSize(1, 2, 3);
            CHECK(&tile != nullptr);
        }
        SECTION("GetTileSizeX()") {
            Tile tile;
            tile.SetTileSize(1, 2, 3);
            CHECK(tile.GetTileSizeX() == 1);
        }
        SECTION("GetTileSizeY()") {
            Tile tile;
            tile.SetTileSize(1, 2, 3);
            CHECK(tile.GetTileSizeY() == 2);
        }
        SECTION("GetTileSizeZ()") {
            Tile tile;
            tile.SetTileSize(1, 2, 3);
            CHECK(tile.GetTileSizeZ() == 3);
        }
        SECTION("SetOverlapSize()") {
            Tile tile;
            tile.SetOverlapSize(1, 2);
            CHECK(&tile != nullptr);
        }
        SECTION("GetOverlapSizeX()") {
            Tile tile;
            tile.SetOverlapSize(1, 2);
            CHECK(tile.GetOverlapSizeX() == 1);
        }
        SECTION("GetOverlapSizeY()") {
            Tile tile;
            tile.SetOverlapSize(1, 2);
            CHECK(tile.GetOverlapSizeY() == 2);
        }
        SECTION("SetTileDataGetter()") {
            Tile tile;
            tile.SetTileDataGetter(ITileDataGetter::Pointer{});
            CHECK(&tile != nullptr);
        }
        SECTION("GetData()") {
            Tile tile;
            tile.SetTileDataGetter(ITileDataGetter::Pointer{new TileDataGetterForTest});
            CHECK(tile.GetData().get()[0] == 1);
        }
        SECTION("GetOverlapData()") {
            auto tileDataGetter = new TileDataGetterForTest;
            auto tileDataGetterPointer = ITileDataGetter::Pointer{ tileDataGetter };

            Tile tile;
            tile.SetTileDataGetter(tileDataGetterPointer);
            tile.SetTileSize(10, 20, 30);
            tile.SetOverlapSize(2, 4);

            SECTION("return is same with tileDataGetter") {
                CHECK(tile.GetOverlapData(Tile::OverlapDirection::Left).get()[0] == 2.f);
            }
            SECTION("Left") {
                const auto overlapData = tile.GetOverlapData(Tile::OverlapDirection::Left);
                CHECK(tileDataGetter->x0In == 0);
                CHECK(tileDataGetter->x1In == 1);
                CHECK(tileDataGetter->y0In == 0);
                CHECK(tileDataGetter->y1In == 19);
                CHECK(tileDataGetter->z0In == 0);
                CHECK(tileDataGetter->z1In == 29);
            }
            SECTION("Right") {
                const auto overlapData = tile.GetOverlapData(Tile::OverlapDirection::Right);
                CHECK(tileDataGetter->x0In == 8);
                CHECK(tileDataGetter->x1In == 9);
                CHECK(tileDataGetter->y0In == 0);
                CHECK(tileDataGetter->y1In == 19);
                CHECK(tileDataGetter->z0In == 0);
                CHECK(tileDataGetter->z1In == 29);
            }
            SECTION("Up") {
                const auto overlapData = tile.GetOverlapData(Tile::OverlapDirection::Up);
                CHECK(tileDataGetter->x0In == 0);
                CHECK(tileDataGetter->x1In == 9);
                CHECK(tileDataGetter->y0In == 0);
                CHECK(tileDataGetter->y1In == 3);
                CHECK(tileDataGetter->z0In == 0);
                CHECK(tileDataGetter->z1In == 29);
            }
            SECTION("Down") {
                const auto overlapData = tile.GetOverlapData(Tile::OverlapDirection::Down);
                CHECK(tileDataGetter->x0In == 0);
                CHECK(tileDataGetter->x1In == 9);
                CHECK(tileDataGetter->y0In == 16);
                CHECK(tileDataGetter->y1In == 19);
                CHECK(tileDataGetter->z0In == 0);
                CHECK(tileDataGetter->z1In == 29);
            }
        }
        SECTION("GetSliceData()") {
            auto tileDataGetter = new TileDataGetterForTest;
            auto tileDataGetterPointer = ITileDataGetter::Pointer{ tileDataGetter };

            Tile tile;
            tile.SetTileDataGetter(tileDataGetterPointer);
            tile.SetTileSize(10, 20, 30);
            tile.SetOverlapSize(2, 4);

            CHECK(tile.GetSliceData(1).get()[0] == 2.f);

            CHECK(tileDataGetter->x0In == 0);
            CHECK(tileDataGetter->x1In == 9);
            CHECK(tileDataGetter->y0In == 0);
            CHECK(tileDataGetter->y1In == 19);
            CHECK(tileDataGetter->z0In == 1);
            CHECK(tileDataGetter->z1In == 1);
        }

        SECTION("GetSliceData()") {
            auto tileDataGetter = new TileDataGetterForTest;
            auto tileDataGetterPointer = ITileDataGetter::Pointer{ tileDataGetter };

            Tile tile;
            tile.SetTileDataGetter(tileDataGetterPointer);
            tile.SetTileSize(10, 20, 30);
            tile.SetOverlapSize(2, 4);

            CHECK(tile.GetSliceStackData(1, 2).get()[0] == 2.f);

            CHECK(tileDataGetter->x0In == 0);
            CHECK(tileDataGetter->x1In == 9);
            CHECK(tileDataGetter->y0In == 0);
            CHECK(tileDataGetter->y1In == 19);
            CHECK(tileDataGetter->z0In == 1);
            CHECK(tileDataGetter->z1In == 2);
        }
    }
}
