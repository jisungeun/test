#include <catch2/catch.hpp>

#include "OverlapRelationSet.h"

namespace OverlapRelationSetTest {
    TEST_CASE("OverlaprelationSet") {
        SECTION("OverlapRelationSet()") {
            OverlapRelationSet overlapRelationSet;
            CHECK(&overlapRelationSet != nullptr);
        }
        SECTION("OverlapRelationSet(other)") {
            constexpr auto overlapIndexX = 1;
            constexpr auto overlapIndexY = 0;

            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);

            OverlapRelationSet srcOverlapRelationSet;
            srcOverlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            srcOverlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);

            OverlapRelationSet destOverlapRelationSet(srcOverlapRelationSet);
            const auto resultOverlapRelation = destOverlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

            CHECK(resultOverlapRelation.GetReliability() == 0.1f);
        }
        SECTION("operator=()") {
            constexpr auto overlapIndexX = 1;
            constexpr auto overlapIndexY = 0;

            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);

            OverlapRelationSet srcOverlapRelationSet;
            srcOverlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            srcOverlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);

            OverlapRelationSet destOverlapRelationSet;
            destOverlapRelationSet = srcOverlapRelationSet;
            const auto resultOverlapRelation = destOverlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

            CHECK(resultOverlapRelation.GetReliability() == 0.1f);
        }
        SECTION("SetTileNumber()") {
            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(1, 2);
            CHECK(&overlapRelationSet != nullptr);
        }
        SECTION("InsertOverlapRelation()") {
            constexpr auto overlapIndexX = 1;
            constexpr auto overlapIndexY = 0;

            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            overlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);

            CHECK(&overlapRelationSet != nullptr);
        }
        SECTION("GetOverlapRelation()") {
            constexpr auto overlapIndexX = 1;
            constexpr auto overlapIndexY = 0;

            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            OverlapRelation overlapRelation;
            overlapRelation.SetReliability(0.1f);

            OverlapRelationSet overlapRelationSet;
            overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
            overlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);

            const auto resultOverlapRelation = overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);
            CHECK(resultOverlapRelation.GetReliability() == 0.1f);
        }
    }
}