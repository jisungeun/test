#include <catch2/catch.hpp>

#include "StitchingWriterResult.h"

namespace StitchingWriterResultTest {
    TEST_CASE("StitchingWriterResult") {
        SECTION("StitchingWriterResult()") {
            StitchingWriterResult stitchingWriterResult;
            CHECK(&stitchingWriterResult != nullptr);
        }
        SECTION("StitchingWriterResult(other)") {
            StitchingWriterResult srcStitchingWriterResult;
            srcStitchingWriterResult.SetSuccessFlag(true);

            const StitchingWriterResult destStitchingWriterResult(srcStitchingWriterResult);
            CHECK(destStitchingWriterResult.GetSuccessFlag() == true);
        }
        SECTION("operator=()") {
            StitchingWriterResult srcStitchingWriterResult;
            srcStitchingWriterResult.SetSuccessFlag(true);

            StitchingWriterResult destStitchingWriterResult;
            destStitchingWriterResult = srcStitchingWriterResult;
            CHECK(destStitchingWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetSuccessFlag()") {
            StitchingWriterResult stitchingWriterResult;
            stitchingWriterResult.SetSuccessFlag(true);
            CHECK(&stitchingWriterResult != nullptr);
        }
        SECTION("GetSuccessFlag()") {
            StitchingWriterResult stitchingWriterResult;
            stitchingWriterResult.SetSuccessFlag(true);
            CHECK(stitchingWriterResult.GetSuccessFlag() == true);
        }
    }
}