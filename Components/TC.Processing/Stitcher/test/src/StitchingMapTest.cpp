#include <iostream>
#include <catch2/catch.hpp>

#include "StitchingMap.h"

namespace StitchingMapTest{
    TEST_CASE("StitchingMap : unit test") {
        SECTION("StitchingMap()") {
            StitchingMap stitchingMap;
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("StitchingMap(other)") {
            StitchingMap srcStitchingMap;
            srcStitchingMap.SetStitchedDataSize(1, 2, 3);

            StitchingMap destStitchingMap(srcStitchingMap);
            CHECK(destStitchingMap.GetStitchedDataSizeX() == 1);
            CHECK(destStitchingMap.GetStitchedDataSizeY() == 2);
            CHECK(destStitchingMap.GetStitchedDataSizeZ() == 3);
        }
        SECTION("operator=()") {
            StitchingMap srcStitchingMap;
            srcStitchingMap.SetStitchedDataSize(1, 2, 3);

            StitchingMap destStitchingMap;
            destStitchingMap = srcStitchingMap;

            CHECK(destStitchingMap.GetStitchedDataSizeX() == 1);
            CHECK(destStitchingMap.GetStitchedDataSizeY() == 2);
            CHECK(destStitchingMap.GetStitchedDataSizeZ() == 3);
        }
        SECTION("SetStitchingTileNumber()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(1, 2);
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("GetStitchingTileNumberX()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(1, 2);
            CHECK(stitchingMap.GetStitchingTileNumberX() == 1);
        }
        SECTION("GetStitchingTileNumberY()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(1, 2);
            CHECK(stitchingMap.GetStitchingTileNumberY() == 2);
        }
        SECTION("SetStitchingTile") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTile(0, {});
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("GetStitchingTile()") {
            IndexRange indexRange0;
            indexRange0.SetRange(0, 1, 2, 3, 4, 5);

            IndexRange indexRange1;
            indexRange1.SetRange(6, 7, 8, 9, 10, 11);

            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(2, 2);
            stitchingTile.AddLayer(1, 2, indexRange0, indexRange1);

            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(2, 2);
            stitchingMap.SetStitchingTile(3, stitchingTile);

            const auto resultStitchingTile = stitchingMap.GetStitchingTile(1, 1);

            CHECK(resultStitchingTile.GetLayerDataTileIndexXYList().size() == 1);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetX0() == 0);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetX1() == 1);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetY0() == 2);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetY1() == 3);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetZ0() == 4);
            CHECK(resultStitchingTile.GetIndexRangeToDataTile(1, 2).GetZ1() == 5);

            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetX0() == 6);
            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetX1() == 7);
            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetY0() == 8);
            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetY1() == 9);
            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetZ0() == 10);
            CHECK(resultStitchingTile.GetIndexRangeToStitchingTile(1, 2).GetZ1() == 11);
        }
        SECTION("SetIndexRange()") {
            StitchingMap stitchingMap;
            stitchingMap.SetIndexRange(0, {});
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("GetIndexRange()") {
            IndexRange indexRange;
            indexRange.SetRange(1, 2, 3, 4, 5, 6);

            StitchingMap stitchingMap;
            stitchingMap.SetStitchingTileNumber(3, 3);
            stitchingMap.SetIndexRange(3, indexRange);

            const auto resultIndexRange = stitchingMap.GetIndexRange(0, 1);

            CHECK(resultIndexRange.GetX0() == 1);
            CHECK(resultIndexRange.GetX1() == 2);
            CHECK(resultIndexRange.GetY0() == 3);
            CHECK(resultIndexRange.GetY1() == 4);
            CHECK(resultIndexRange.GetZ0() == 5);
            CHECK(resultIndexRange.GetZ1() == 6);
        }
        SECTION("SetStitchedDataSize()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchedDataSize(1, 2, 3);
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("GetStitchedDataSizeX()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchedDataSize(1, 2, 3);

            CHECK(stitchingMap.GetStitchedDataSizeX() == 1);
        }
        SECTION("GetStitchedDataSizeY()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchedDataSize(1, 2, 3);

            CHECK(stitchingMap.GetStitchedDataSizeY() == 2);
        }
        SECTION("GetStitchedDataSizeZ()") {
            StitchingMap stitchingMap;
            stitchingMap.SetStitchedDataSize(1, 2, 3);

            CHECK(stitchingMap.GetStitchedDataSizeZ() == 3);
        }
        SECTION("SetCenterPosition()") {
            StitchingMap stitchingMap;
            stitchingMap.SetCenterPosition(1, 2);
            CHECK(&stitchingMap != nullptr);
        }
        SECTION("GetCenterPositionX()") {
            StitchingMap stitchingMap;
            stitchingMap.SetCenterPosition(1, 2);
            CHECK(stitchingMap.GetCenterPositionX() == 1);
        }
        SECTION("GetCenterPositionY()") {
            StitchingMap stitchingMap;
            stitchingMap.SetCenterPosition(1, 2);
            CHECK(stitchingMap.GetCenterPositionY() == 2);
        }
    }
    TEST_CASE("StitchingMap : practical test") {
        //TODO Implement practical test
    }
}