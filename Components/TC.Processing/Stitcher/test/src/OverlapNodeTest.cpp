#include <catch2/catch.hpp>

#include "OverlapNode.h"

namespace OverlapNodeTest {
    TEST_CASE("OveralpNode") {
        SECTION("OverlapNode()") {
            OverlapNode overlapNode;
            CHECK(&overlapNode != nullptr);
        }
        SECTION("OverlapNode(other)") {
            OverlapNode srcOverlapNode;
            srcOverlapNode.SetScore(1.f);

            const OverlapNode destOverlapNode(srcOverlapNode);
            CHECK(destOverlapNode.GetScore() == 1.f);
        }
        SECTION("operator=()") {
            OverlapNode srcOverlapNode;
            srcOverlapNode.SetScore(1.f);

            OverlapNode destOverlapNode;
            destOverlapNode = srcOverlapNode;
            CHECK(destOverlapNode.GetScore() == 1.f);
        }
        SECTION("SetOverlapIndex()") {
            OverlapNode overlapNode;
            overlapNode.SetOverlapIndex(1, 2);
            CHECK(&overlapNode != nullptr);
        }
        SECTION("GetOverlapIndexX()") {
            OverlapNode overlapNode;
            overlapNode.SetOverlapIndex(1, 2);
            CHECK(overlapNode.GetOverlapIndexX() == 1);
        }
        SECTION("GetOverlapIndexY()") {
            OverlapNode overlapNode;
            overlapNode.SetOverlapIndex(1, 2);
            CHECK(overlapNode.GetOverlapIndexY() == 2);
        }
        SECTION("SetScore()") {
            OverlapNode overlapNode;
            overlapNode.SetScore(1.f);
            CHECK(&overlapNode != nullptr);
        }
        SECTION("GetScore()") {
            OverlapNode overlapNode;
            overlapNode.SetScore(1.f);
            CHECK(overlapNode.GetScore() == 1.f);
        }
        SECTION("SetStatus()") {
            OverlapNode overlapNode;
            overlapNode.SetStatus(OverlapNode::Status::Connected);
            CHECK(&overlapNode != nullptr);
        }
        SECTION("GetStatus()") {
            OverlapNode overlapNode;
            overlapNode.SetStatus(OverlapNode::Status::Connected);
            CHECK(overlapNode.GetStatus() == OverlapNode::Status::Connected);
        }
        SECTION("SetCheckFlag()") {
            OverlapNode overlapNode;
            overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
            CHECK(&overlapNode != nullptr);
        }
        SECTION("GetCheckFlag()") {
            OverlapNode overlapNode;
            overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
            CHECK(overlapNode.GetCheckFlag() == OverlapNode::CheckFlag::Checking);
        }
    }
}