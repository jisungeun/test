#include <catch2/catch.hpp>

#include "IStitchingWriter.h"

namespace IStitchingWriterTest {
    class StitchingWriterForTest final : public IStitchingWriter {
    public:
        StitchingWriterForTest() = default;
        ~StitchingWriterForTest() = default;

        auto SetTileSet(const TileSet& tileSet) -> void override {
            setTileSetTriggered = true;
        }
        auto SetTilePositionSet(const TilePositionSet& tilePositionSet) -> void override {
            setTilePositionSetTriggered = true;
        }
        auto SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void override {
            setTileConfigurationTriggered = true;
        }
        auto Run() -> bool override {
            runTriggered = true;
            return false;
        }
        auto GetStitchingWriterResult() const -> const StitchingWriterResult& override {
            getStitchingWriterResultTriggered = true;
            return stitchingWriterResult;
        }

        StitchingWriterResult stitchingWriterResult;
        bool setTileSetTriggered{ false };
        bool setTilePositionSetTriggered{ false };
        bool setTileConfigurationTriggered{ false };
        bool runTriggered{ false };
        mutable bool getStitchingWriterResultTriggered{ false };
    };


    TEST_CASE("IStitchingWriter") {
        SECTION("SetTileSet()") {
            StitchingWriterForTest stitchingWriter;
            CHECK(stitchingWriter.setTileSetTriggered == false);
            stitchingWriter.SetTileSet(TileSet{});
            CHECK(stitchingWriter.setTileSetTriggered == true);
        }
        SECTION("SetTilePositionSet()") {
            StitchingWriterForTest stitchingWriter;
            CHECK(stitchingWriter.setTilePositionSetTriggered == false);
            stitchingWriter.SetTilePositionSet(TilePositionSet{});
            CHECK(stitchingWriter.setTilePositionSetTriggered == true);
        }
        SECTION("SetTileConfiguration()") {
            StitchingWriterForTest stitchingWriter;
            CHECK(stitchingWriter.setTileConfigurationTriggered == false);
            stitchingWriter.SetTileConfiguration(TileConfiguration{});
            CHECK(stitchingWriter.setTileConfigurationTriggered == true);
        }
        SECTION("Run()") {
            StitchingWriterForTest stitchingWriter;
            CHECK(stitchingWriter.runTriggered == false);
            stitchingWriter.Run();
            CHECK(stitchingWriter.runTriggered == true);
        }
        SECTION("GetStitchingWriterResult()") {
            StitchingWriterForTest stitchingWriter;
            CHECK(stitchingWriter.getStitchingWriterResultTriggered == false);
            stitchingWriter.GetStitchingWriterResult();
            CHECK(stitchingWriter.getStitchingWriterResultTriggered == true);
        }
    }
}
