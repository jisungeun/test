#include <catch2/catch.hpp>

#include "TileAcquisitionPosition.h"

namespace TileAcquisitionPositionTest {
    TEST_CASE("TileAcquisitionPositionTest") {
        SECTION("TileAcquisitionPosition()") {
            TileAcquisitionPosition tileAcquisitionPosition;
            CHECK(&tileAcquisitionPosition != nullptr);
        }
        SECTION("TileAcquisitionPosition(other)") {
            TileAcquisitionPosition srcTileAcquisitionPosition;
            srcTileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            TileAcquisitionPosition destTileAcquisitionPosition(srcTileAcquisitionPosition);
            CHECK(destTileAcquisitionPosition.GetPositionXInMillimeter() == 1);
            CHECK(destTileAcquisitionPosition.GetPositionYInMillimeter() == 2);
            CHECK(destTileAcquisitionPosition.GetPositionZInMillimeter() == 3);
        }
        SECTION("operator=()") {
            TileAcquisitionPosition srcTileAcquisitionPosition;
            srcTileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            TileAcquisitionPosition destTileAcquisitionPosition;
            destTileAcquisitionPosition = srcTileAcquisitionPosition;
            CHECK(destTileAcquisitionPosition.GetPositionXInMillimeter() == 1);
            CHECK(destTileAcquisitionPosition.GetPositionYInMillimeter() == 2);
            CHECK(destTileAcquisitionPosition.GetPositionZInMillimeter() == 3);
        }
        SECTION("SetPositionInMillimeter()") {
            TileAcquisitionPosition tileAcquisitionPosition;
            tileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            CHECK(&tileAcquisitionPosition != nullptr);
        }
        SECTION("GetPositionXInMillimeter()") {
            TileAcquisitionPosition tileAcquisitionPosition;
            tileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            CHECK(tileAcquisitionPosition.GetPositionXInMillimeter() == 1);
        }
        SECTION("GetPositionYInMillimeter()") {
            TileAcquisitionPosition tileAcquisitionPosition;
            tileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            CHECK(tileAcquisitionPosition.GetPositionYInMillimeter() == 2);
        }
        SECTION("GetPositionZInMillimeter()") {
            TileAcquisitionPosition tileAcquisitionPosition;
            tileAcquisitionPosition.SetPositionInMillimeter(1, 2, 3);
            CHECK(tileAcquisitionPosition.GetPositionZInMillimeter() == 3);
        }
    }
}