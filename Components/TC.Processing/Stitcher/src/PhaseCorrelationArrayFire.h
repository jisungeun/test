#pragma once

#include <memory>

class PhaseCorrelationArrayFire {
public:
    PhaseCorrelationArrayFire();
    ~PhaseCorrelationArrayFire();

    auto SetData(const std::shared_ptr<float[]>& referenceData, const std::shared_ptr<float[]>& comparingData)->void;
    auto SetDataSize(const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ)->void;

    auto Run()->bool;

    auto GetReliability()const ->const float&;
    auto GetShiftValueX()const ->const float&;
    auto GetShiftValueY()const ->const float&;
    auto GetShiftValueZ()const ->const float&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};