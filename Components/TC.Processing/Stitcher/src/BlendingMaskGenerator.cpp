#include "BlendingMaskGenerator.h"

class BlendingMaskGenerator::Impl {
public:
	Impl() = default;
	~Impl() = default;

	int32_t sizeX;
	int32_t sizeY;

	int32_t blendingLengthX;
	int32_t blendingLengthY;

	af::array blendingMask;
};

BlendingMaskGenerator::BlendingMaskGenerator() : d(new Impl()) {
}

BlendingMaskGenerator::~BlendingMaskGenerator() = default;

auto BlendingMaskGenerator::SetMaskSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
	d->sizeX = sizeX;
	d->sizeY = sizeY;
}

auto BlendingMaskGenerator::SetBlendingLength(const int32_t& blendingLengthX, const int32_t& blendingLengthY) -> void {
    d->blendingLengthX = blendingLengthX;
    d->blendingLengthY = blendingLengthY;
}

auto BlendingMaskGenerator::Generate() -> bool {
    try {
        constexpr float pyramidHeight = 100.f;

        af::array blendingMaskXSlope = af::constant(pyramidHeight, d->sizeX, d->sizeY, f32);
        if (d->blendingLengthX > 1) {
            const auto gradient = pyramidHeight / static_cast<float>(d->blendingLengthX);

            af::array slopeLineX = af::seq(1, d->blendingLengthX);
            slopeLineX = slopeLineX * gradient;

            auto blendingLineX = af::constant(0, d->blendingLengthX, 1);
            blendingLineX(af::seq(0, af::end), 0) = slopeLineX;

            const af::array blendingPlaneX = af::tile(blendingLineX, 1, d->sizeX);

            const auto startSlopeArea = af::seq(0, d->blendingLengthX - 1);
            const auto endSlopeArea = af::seq(d->sizeX - d->blendingLengthX, d->sizeX - 1);

            blendingMaskXSlope(startSlopeArea, af::span) = blendingPlaneX;
            blendingMaskXSlope(endSlopeArea, af::span) = af::flip(blendingPlaneX, 0);
        }

        af::array blendingMaskYSlope = af::constant(pyramidHeight, d->sizeX, d->sizeY, f32);
        if (d->blendingLengthY > 1) {
            const float gradient = pyramidHeight / static_cast<float>(d->blendingLengthY);

            af::array slopeLineY = af::seq(1, d->blendingLengthY);
            slopeLineY = slopeLineY * gradient;

            auto blendingLineY = af::constant(0, 1, d->blendingLengthY);
            blendingLineY(0, af::seq(0, af::end)) = slopeLineY;

            const auto blendingPlaneY = af::tile(blendingLineY, d->sizeX, 1);

            const auto startSlopeArea = af::seq(0, d->blendingLengthY - 1);
            const auto endSlopeArea = af::seq(d->sizeY - d->blendingLengthY, d->sizeY - 1);

            blendingMaskYSlope(af::span, startSlopeArea) = blendingPlaneY;
            blendingMaskYSlope(af::span, endSlopeArea) = af::flip(blendingPlaneY, 1);
        }

        const auto blendingMask = blendingMaskXSlope * blendingMaskYSlope;

        d->blendingMask = blendingMask;

        af::deviceGC();
    } catch (af::exception& afException) {
        af::deviceGC();
        return false;
    }
    
    return true;
}

auto BlendingMaskGenerator::GetBlendingMask() const -> const af::array& {
	return d->blendingMask;
}

