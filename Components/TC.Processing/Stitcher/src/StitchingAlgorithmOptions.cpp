#include "StitchingAlgorithmOptions.h"

struct StitchingAlgorithmOptions::Impl {
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    bool subDataStitchingFlag{ false };
};

StitchingAlgorithmOptions::StitchingAlgorithmOptions()
    : d(new Impl()) {
}

StitchingAlgorithmOptions::StitchingAlgorithmOptions(const Self& other)
    : d(new Impl(*other.d)) {
}

StitchingAlgorithmOptions::~StitchingAlgorithmOptions() = default;

auto StitchingAlgorithmOptions::operator=(const Self & other) -> Self& {
    *d = *(other.d);
    return *this;
}

auto StitchingAlgorithmOptions::SetSubDataStitchingFlag(const bool& subDataStitchingFlag) -> void {
    d->subDataStitchingFlag = subDataStitchingFlag;
}

auto StitchingAlgorithmOptions::GetSubDataStitchingFlag() const -> bool {
    return d->subDataStitchingFlag;
}
