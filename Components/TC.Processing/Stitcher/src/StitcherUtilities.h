#pragma once

#include <tuple>
#include "TilePositionSet.h"
#include "StitchingMap.h"

auto GetMinPosition(const TilePositionSet& tilePositionSet)->TilePosition;
auto GetMaxPosition(const TilePositionSet& tilePositionSet)->TilePosition;
auto GetMinMaxPosition(const TilePositionSet& tilePositionSet)->std::tuple<TilePosition, TilePosition>;

struct BoundaryLength {
    int32_t left{};
    int32_t right{};
    int32_t up{};
    int32_t down{};
};

struct TargetSize {
    int32_t sizeX{};
    int32_t sizeY{};
};

auto GetBoundaryLength(const TilePositionSet& tilePositionSet)->BoundaryLength;

auto CropBoundary(const StitchingMap& stitchingMap, const BoundaryLength& boundaryLength)->StitchingMap;
auto CropBoundary(const StitchingMap& stitchingMap, const TargetSize& size)->StitchingMap;