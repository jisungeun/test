#include "StitchingMap.h"
#include <QMap>

#include "StitchingCenterPositionCalculator.h"

typedef int32_t StitchingTileIndex;

class StitchingMap::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QMap<StitchingTileIndex, StitchingTile> stitchingTileIndexMap;
    QMap<StitchingTileIndex, IndexRange> stitchingTileIndexRangeMap;

    int32_t stitchingTileSizeX{};
    int32_t stitchingTileSizeY{};

    int32_t stitchingTileNumberX{};
    int32_t stitchingTileNumberY{};

    int32_t stitchedDataSizeX{};
    int32_t stitchedDataSizeY{};
    int32_t stitchedDataSizeZ{};

    int32_t centerPositionX{};
    int32_t centerPositionY{};
};

StitchingMap::StitchingMap() : d(new Impl()) {
}

StitchingMap::StitchingMap(const StitchingMap& other) : d(new Impl(*other.d)) {
}

StitchingMap::~StitchingMap() = default;

auto StitchingMap::operator=(const StitchingMap& other) -> StitchingMap& {
    *(this->d) = *(other.d);
    return *this;
}

auto StitchingMap::SetStitchingTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->stitchingTileNumberX = tileNumberX;
    d->stitchingTileNumberY = tileNumberY;
}

auto StitchingMap::GetStitchingTileNumberX() const -> const int32_t& {
    return d->stitchingTileNumberX;
}

auto StitchingMap::GetStitchingTileNumberY() const -> const int32_t& {
    return d->stitchingTileNumberY;
}

auto StitchingMap::SetStitchingTile(const int32_t& stitchingIndex, const StitchingTile& stitchingTile) -> void {
    d->stitchingTileIndexMap[stitchingIndex] = stitchingTile;
}

auto StitchingMap::GetStitchingTile(const int32_t& stitchingTileIndexX, const int32_t& stitchingTileIndexY) const
    -> StitchingTile {
    const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * d->stitchingTileNumberX;
    return d->stitchingTileIndexMap[stitchingTileIndex];
}

auto StitchingMap::SetIndexRange(const int32_t& stitchingTileIndex, const IndexRange& indexRange) -> void {
    d->stitchingTileIndexRangeMap[stitchingTileIndex] = indexRange;
}

auto StitchingMap::GetIndexRange(const int32_t& stitchingTileIndexX, const int32_t& stitchingTileIndexY) const
    -> IndexRange {
    const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * d->stitchingTileNumberX;
    return d->stitchingTileIndexRangeMap[stitchingTileIndex];
}

auto StitchingMap::SetStitchedDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->stitchedDataSizeX = sizeX;
    d->stitchedDataSizeY = sizeY;
    d->stitchedDataSizeZ = sizeZ;
}

auto StitchingMap::GetStitchedDataSizeX() const -> const int32_t& {
    return d->stitchedDataSizeX;
}

auto StitchingMap::GetStitchedDataSizeY() const -> const int32_t& {
    return d->stitchedDataSizeY;
}

auto StitchingMap::GetStitchedDataSizeZ() const -> const int32_t& {
    return d->stitchedDataSizeZ;
}

auto StitchingMap::SetCenterPosition(const int32_t& positionX, const int32_t& positionY)->void {
    d->centerPositionX = positionX;
    d->centerPositionY = positionY;
}

auto StitchingMap::GetCenterPositionX() const -> const int32_t& {
    return d->centerPositionX;
}

auto StitchingMap::GetCenterPositionY() const -> const int32_t& {
    return d->centerPositionY;
}
