#include "StitchingTile.h"

#include <QMap>

typedef int32_t DataTileIndex;

class StitchingTile::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t dataTileNumberX{};
    int32_t dataTileNumberY{};

    QMap<DataTileIndex, IndexRange> indexRangeMapToDataTile;
    QMap<DataTileIndex, IndexRange> indexRangeMapToStitchingTile;

    auto ToDataTileIndex(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const ->DataTileIndex;
    auto ToDataTileIndexXY(const DataTileIndex& dataTileIndex) const ->std::tuple<int32_t, int32_t>;
};

auto StitchingTile::Impl::ToDataTileIndex(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
    -> DataTileIndex {
    return dataTileIndexX + dataTileIndexY * this->dataTileNumberX;
}

auto StitchingTile::Impl::ToDataTileIndexXY(const DataTileIndex& dataTileIndex) const -> std::tuple<int32_t, int32_t> {
    const auto dataTileIndexX = dataTileIndex % this->dataTileNumberX;
    const auto dataTileIndexY = dataTileIndex / this->dataTileNumberX;
    return { dataTileIndexX, dataTileIndexY };
}

StitchingTile::StitchingTile() : d(new Impl()) {
}

StitchingTile::StitchingTile(const StitchingTile& other) : d(new Impl(*other.d)) {
}

StitchingTile::~StitchingTile() = default;

auto StitchingTile::operator=(const StitchingTile& other) -> StitchingTile& {
    *(this->d) = *(other.d);
    return *this;
}

auto StitchingTile::SetDataTileNumber(const int32_t& dataTileNumberX, const int32_t& dataTileNumberY) -> void {
    d->dataTileNumberX = dataTileNumberX;
    d->dataTileNumberY = dataTileNumberY;
}

auto StitchingTile::AddLayer(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY,
    const IndexRange& indexRangeToDataTile, const IndexRange& indexRangeToStitchingTile) -> void {
    const auto dataTileIndex = d->ToDataTileIndex(dataTileIndexX, dataTileIndexY);

    d->indexRangeMapToDataTile[dataTileIndex] = indexRangeToDataTile;
    d->indexRangeMapToStitchingTile[dataTileIndex] = indexRangeToStitchingTile;
}

auto StitchingTile::GetIndexRangeToDataTile(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
    -> const IndexRange& {
    const auto dataTileIndex = d->ToDataTileIndex(dataTileIndexX, dataTileIndexY);
    return d->indexRangeMapToDataTile[dataTileIndex];

}

auto StitchingTile::GetIndexRangeToStitchingTile(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
    -> const IndexRange& {
    const auto dataTileIndex = d->ToDataTileIndex(dataTileIndexX, dataTileIndexY);
    return d->indexRangeMapToStitchingTile[dataTileIndex];
}

auto StitchingTile::GetLayerDataTileIndexXYList() const -> QList<std::tuple<int32_t, int32_t>> {
    QList<std::tuple<int32_t, int32_t>> layerDataTileIndexXYList;

    for (auto iterator = d->indexRangeMapToDataTile.begin(); iterator != d->indexRangeMapToDataTile.end();
        ++iterator) {

        const auto dataTileIndex = iterator.key();
        const auto [dataTileIndexX, dataTileIndexY] = d->ToDataTileIndexXY(dataTileIndex);

        layerDataTileIndexXYList.push_back({ dataTileIndexX, dataTileIndexY });
    }

    return layerDataTileIndexXYList;
}
