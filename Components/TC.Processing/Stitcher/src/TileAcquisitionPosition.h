#pragma once

#include <memory>

class TileAcquisitionPosition {
public:
    TileAcquisitionPosition();
    TileAcquisitionPosition(const TileAcquisitionPosition& other);
    ~TileAcquisitionPosition();

    auto operator=(const TileAcquisitionPosition& other)->TileAcquisitionPosition&;

    auto SetPositionInMillimeter(const double& positionX, const double& positionY, const double& positionZ)->void;

    auto GetPositionXInMillimeter() const -> const double&;
    auto GetPositionYInMillimeter() const -> const double&;
    auto GetPositionZInMillimeter() const -> const double&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};