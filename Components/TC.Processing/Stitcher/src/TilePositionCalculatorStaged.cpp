#include "TilePositionCalculatorStaged.h"

class TilePositionCalculatorStaged::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration{};
};

TilePositionCalculatorStaged::TilePositionCalculatorStaged() : d{ std::make_unique<Impl>() } {
}

TilePositionCalculatorStaged::~TilePositionCalculatorStaged() = default;

auto TilePositionCalculatorStaged::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto TilePositionCalculatorStaged::Calculate() -> TilePositionSet {
    const auto& tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto& tileNumberY = d->tileConfiguration.GetTileNumberY();

    TilePositionSet tilePositionSet;
    tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);

    const auto tileSizeX = d->tileConfiguration.GetTileSizeXInPixel();
    const auto tileSizeY = d->tileConfiguration.GetTileSizeYInPixel();

    const auto overlapSizeX = d->tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapSizeY = d->tileConfiguration.GetOverlapLengthYInPixel();

    const auto nonOverlapSizeX = tileSizeX - overlapSizeX;
    const auto nonOverlapSizeY = tileSizeY - overlapSizeY;

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {

            const auto tilePositionX = static_cast<float>(tileIndexX * nonOverlapSizeX);
            const auto tilePositionY = static_cast<float>(tileIndexY * nonOverlapSizeY);
            constexpr auto tilePositionZ = 0.f;

            TilePosition tilePosition;
            tilePosition.SetPositions(tilePositionX, tilePositionY, tilePositionZ);
            tilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, tilePosition);
        }
    }

    return tilePositionSet;
}
