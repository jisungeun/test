#include "TilePosition.h"

class TilePosition::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    float tilePositionX{};
    float tilePositionY{};
    float tilePositionZ{};
};

TilePosition::TilePosition() : d(new Impl()) {
}

TilePosition::TilePosition(const TilePosition& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TilePosition::~TilePosition() = default;

auto TilePosition::operator=(const TilePosition& other) -> TilePosition& {
    *(this->d) = *(other.d);
    return *this;
}

auto TilePosition::SetPositions(const float& tilePositionX, const float& tilePositionY, const float& tilePositionZ)
    -> void {
    d->tilePositionX = tilePositionX;
    d->tilePositionY = tilePositionY;
    d->tilePositionZ = tilePositionZ;
}

auto TilePosition::GetTilePositionX() const -> const float& {
    return d->tilePositionX;
}

auto TilePosition::GetTilePositionY() const -> const float& {
    return d->tilePositionY;
}

auto TilePosition::GetTilePositionZ() const -> const float& {
    return d->tilePositionZ;
}
