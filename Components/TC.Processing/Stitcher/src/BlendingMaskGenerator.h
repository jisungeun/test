#pragma once

#include <memory>

#include "arrayfire.h"

class BlendingMaskGenerator {
public:
	BlendingMaskGenerator();
	~BlendingMaskGenerator();

	auto SetMaskSize(const int32_t& sizeX, const int32_t& sizeY)->void;
	auto SetBlendingLength(const int32_t& blendingLengthX, const int32_t& blendingLengthY)->void;

	auto Generate()->bool;

	auto GetBlendingMask() const -> const af::array&;

private:
	class Impl;
	std::unique_ptr<Impl> d;
};