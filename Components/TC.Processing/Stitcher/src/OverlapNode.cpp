#include "OverlapNode.h"

class OverlapNode::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t overlapIndexX{ -1 };
    int32_t overlapIndexY{ -1 };

    float score{ 0.f };

    Status status{ Status::Opened };
    CheckFlag checkFlag{ CheckFlag::Ready };
};

OverlapNode::OverlapNode() : d(new Impl()) {
}

OverlapNode::OverlapNode(const OverlapNode& other) : d(new Impl(*other.d)) {
}

OverlapNode::~OverlapNode() = default;

auto OverlapNode::operator=(const OverlapNode& other) -> OverlapNode& {
    *(this->d) = *(other.d);
    return *this;
}

auto OverlapNode::SetOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> void {
    d->overlapIndexX = overlapIndexX;
    d->overlapIndexY = overlapIndexY;
}

auto OverlapNode::GetOverlapIndexX() const -> const int32_t& {
    return d->overlapIndexX;
}

auto OverlapNode::GetOverlapIndexY() const -> const int32_t& {
    return d->overlapIndexY;
}

auto OverlapNode::SetScore(const float& score) -> void {
    d->score = score;
}

auto OverlapNode::GetScore() const -> const float& {
    return d->score;
}

auto OverlapNode::SetStatus(const Status& status) -> void {
    d->status = status;
}

auto OverlapNode::GetStatus() const -> const Status& {
    return d->status;
}

auto OverlapNode::SetCheckFlag(const CheckFlag& checkFlag) -> void {
    d->checkFlag = checkFlag;
}

auto OverlapNode::GetCheckFlag() const -> const CheckFlag& {
    return d->checkFlag;
}
