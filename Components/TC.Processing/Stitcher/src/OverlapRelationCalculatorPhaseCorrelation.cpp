#include "OverlapRelationCalculatorPhaseCorrelation.h"

#include "PhaseCorrelationArrayFire.h"

class OverlapRelationCalculatorPhaseCorrelation::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration;
    TileSet tileSet;

    auto GetOverlapRelationLeftRight(const Tile& leftTile, const Tile& rightTile)->OverlapRelation;
    auto GetOverlapRelationUpDown(const Tile& upTile, const Tile& downTile)->OverlapRelation;
};

auto OverlapRelationCalculatorPhaseCorrelation::Impl::GetOverlapRelationLeftRight(const Tile& leftTile, 
    const Tile& rightTile)-> OverlapRelation {
    const auto leftTileRightSideOverlapData = leftTile.GetOverlapData(Tile::OverlapDirection::Right);
    const auto rightTileLeftSideOverlapData = rightTile.GetOverlapData(Tile::OverlapDirection::Left);

    const auto overlapDataSizeX = leftTile.GetOverlapSizeX();
    const auto overlapDataSizeY = leftTile.GetTileSizeY();
    const auto overlapDataSizeZ = leftTile.GetTileSizeZ();

    PhaseCorrelationArrayFire phaseCorrelationArrayFire;
    phaseCorrelationArrayFire.SetData(leftTileRightSideOverlapData, rightTileLeftSideOverlapData);
    phaseCorrelationArrayFire.SetDataSize(overlapDataSizeX, overlapDataSizeY, overlapDataSizeZ);

    phaseCorrelationArrayFire.Run();

    const auto reliability = phaseCorrelationArrayFire.GetReliability();
    const auto shiftValueX = phaseCorrelationArrayFire.GetShiftValueX();
    const auto shiftValueY = phaseCorrelationArrayFire.GetShiftValueY();
    const auto shiftValueZ = phaseCorrelationArrayFire.GetShiftValueZ();

    OverlapRelation overlapRelation;
    overlapRelation.SetReliability(reliability);
    overlapRelation.SetShiftValue(shiftValueX,shiftValueY,shiftValueZ);

    return overlapRelation;
}

auto OverlapRelationCalculatorPhaseCorrelation::Impl::GetOverlapRelationUpDown(const Tile& upTile, 
    const Tile& downTile)-> OverlapRelation {
    const auto upTileDownSideOverlapData = upTile.GetOverlapData(Tile::OverlapDirection::Down);
    const auto downTileUpSideOverlapData = downTile.GetOverlapData(Tile::OverlapDirection::Up);

    const auto overlapDataSizeX = upTile.GetTileSizeX();
    const auto overlapDataSizeY = upTile.GetOverlapSizeY();
    const auto overlapDataSizeZ = upTile.GetTileSizeZ();

    PhaseCorrelationArrayFire phaseCorrelationArrayFire;
    phaseCorrelationArrayFire.SetData(upTileDownSideOverlapData, downTileUpSideOverlapData);
    phaseCorrelationArrayFire.SetDataSize(overlapDataSizeX, overlapDataSizeY, overlapDataSizeZ);

    phaseCorrelationArrayFire.Run();

    const auto reliability = phaseCorrelationArrayFire.GetReliability();
    const auto shiftValueX = phaseCorrelationArrayFire.GetShiftValueX();
    const auto shiftValueY = phaseCorrelationArrayFire.GetShiftValueY();
    const auto shiftValueZ = phaseCorrelationArrayFire.GetShiftValueZ();

    OverlapRelation overlapRelation;
    overlapRelation.SetReliability(reliability);
    overlapRelation.SetShiftValue(shiftValueX, shiftValueY, shiftValueZ);

    return overlapRelation;
}

OverlapRelationCalculatorPhaseCorrelation::OverlapRelationCalculatorPhaseCorrelation() : d(new Impl()) {
}

OverlapRelationCalculatorPhaseCorrelation::~OverlapRelationCalculatorPhaseCorrelation() = default;

auto OverlapRelationCalculatorPhaseCorrelation::SetTileConfiguration(const TileConfiguration& tileConfiguration)
    -> void {
    d->tileConfiguration = tileConfiguration;
}

auto OverlapRelationCalculatorPhaseCorrelation::SetTileSet(const TileSet& tileSet) -> void {
    d->tileSet = tileSet;
}

auto OverlapRelationCalculatorPhaseCorrelation::Calculate() -> OverlapRelationSet {
    const auto& tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto& tileNumberY = d->tileConfiguration.GetTileNumberY();

    OverlapRelationSet overlapRelationSet;
    overlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            if (tileIndexX != 0) {
                const auto tile = d->tileSet.GetTile(tileIndexX, tileIndexY);
                const auto leftTile = d->tileSet.GetTile(tileIndexX - 1, tileIndexY);
                const auto overlapRelation = d->GetOverlapRelationLeftRight(leftTile, tile);

                const auto overlapIndexX = 2 * tileIndexX - 1;
                const auto overlapIndexY = 2 * tileIndexY;
                
                overlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);
            }

            if (tileIndexY != 0) {
                const auto tile = d->tileSet.GetTile(tileIndexX, tileIndexY);
                const auto upTile = d->tileSet.GetTile(tileIndexX, tileIndexY - 1);
                const auto overlapRelation = d->GetOverlapRelationUpDown(upTile, tile);

                const auto overlapIndexX = 2 * tileIndexX;
                const auto overlapIndexY = 2 * tileIndexY - 1;

                overlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, overlapRelation);
            }
        }
    }

    return overlapRelationSet;
}
