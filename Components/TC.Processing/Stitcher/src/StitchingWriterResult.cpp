#include "StitchingWriterResult.h"

class StitchingWriterResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool successFlag{};
};

StitchingWriterResult::StitchingWriterResult() : d(new Impl()) {
}

StitchingWriterResult::StitchingWriterResult(const StitchingWriterResult& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

StitchingWriterResult::~StitchingWriterResult() = default;

auto StitchingWriterResult::operator=(const StitchingWriterResult& other) -> StitchingWriterResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto StitchingWriterResult::SetSuccessFlag(const bool& successFlag) -> void {
    d->successFlag = successFlag;
}

auto StitchingWriterResult::GetSuccessFlag() const -> const bool& {
    return d->successFlag;
}
