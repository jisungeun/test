#include "StitchingCenterPositionCalculator.h"

class StitchingCenterPositionCalculator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration{};
    TilePositionSet tilePositionSet{};

    int32_t centerPositionX{};
    int32_t centerPositionY{};
};

StitchingCenterPositionCalculator::StitchingCenterPositionCalculator() : d(new Impl()) {
}

StitchingCenterPositionCalculator::~StitchingCenterPositionCalculator() = default;

auto StitchingCenterPositionCalculator::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto StitchingCenterPositionCalculator::SetTilePositionSet(const TilePositionSet& tilePositionSet) -> void {
    d->tilePositionSet = tilePositionSet;
}

auto StitchingCenterPositionCalculator::Calculate() -> bool {
    const auto tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto tileNumberY = d->tileConfiguration.GetTileNumberY();

    if ((tileNumberX == 0)||(tileNumberY == 0)) {
        return false;
    }

    const auto overlapLengthX = d->tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapLengthY = d->tileConfiguration.GetOverlapLengthYInPixel();

    if ((overlapLengthX < 0) || (overlapLengthY < 0)) {
        return false;
    }

    const auto tileSizeX = d->tileConfiguration.GetTileSizeXInPixel();
    const auto tileSizeY = d->tileConfiguration.GetTileSizeYInPixel();

    if ((tileSizeX <= 0) || (tileSizeY <= 0)) {
        return false;
    }

    const auto centerTileIndexX = tileNumberX / 2;
    const auto centerTileIndexY = tileNumberY / 2;

    int32_t calculatedCenterPositionX, calculatedCenterPositionY;

    const auto centerTilePosition = d->tilePositionSet.GetTilePosition(centerTileIndexX, centerTileIndexY);
    const auto centerTilePositionX = static_cast<int32_t>(centerTilePosition.GetTilePositionX());
    const auto centerTilePositionY = static_cast<int32_t>(centerTilePosition.GetTilePositionY());

    if (tileNumberX % 2 == 0) {
        calculatedCenterPositionX = centerTilePositionX + overlapLengthX / 2 - 1;
    } else {
        calculatedCenterPositionX = centerTilePositionX + tileSizeX / 2 - 1;
    }

    if (tileNumberY % 2 == 0) {
        calculatedCenterPositionY = centerTilePositionY + overlapLengthY / 2 - 1;
    } else {
        calculatedCenterPositionY = centerTilePositionY + tileSizeY / 2 - 1;
    }

    d->centerPositionX = calculatedCenterPositionX;
    d->centerPositionY = calculatedCenterPositionY;

    return true;
}

auto StitchingCenterPositionCalculator::GetCenterPositionX() const -> const int32_t& {
    return d->centerPositionX;
}

auto StitchingCenterPositionCalculator::GetCenterPositionY() const -> const int32_t& {
    return d->centerPositionY;
}
