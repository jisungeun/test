#include <QMap>

#include "TileSet.h"

#include "ITileSetGenerator.h"
#include "ITileConfigurationReader.h"
#include "IMissingTileChecker.h"

typedef int32_t TileIndex;

class TileSet::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    auto CalculateTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY) const ->TileIndex;

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    QMap<TileIndex, Tile> tileMap{};
};

auto TileSet::Impl::CalculateTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY) const -> TileIndex {
    return (tileIndexY * tileNumberX) + tileIndexX;
}

TileSet::TileSet() : d(new Impl()) {
}

TileSet::TileSet(const TileSet& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TileSet::~TileSet() = default;

auto TileSet::operator=(const TileSet& other) -> TileSet& {
    *(this->d) = *(other.d);
    return *this;
}

auto TileSet::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto TileSet::InsertTile(const Tile& tile, const int32_t& tileIndexX, const int32_t& tileIndexY) -> void {
    const auto tileIndex = d->CalculateTileIndex(tileIndexX, tileIndexY);
    d->tileMap[tileIndex] = tile;
}

auto TileSet::GetTile(const int32_t& tileIndexX, const int32_t& tileIndexY) const -> Tile {
    const auto tileIndex = d->CalculateTileIndex(tileIndexX, tileIndexY);
    return d->tileMap[tileIndex];
}
