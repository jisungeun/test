#include "StitcherUtilities.h"

auto GetMinPosition(const TilePositionSet& tilePositionSet) -> TilePosition {
    const auto tileNumberX = tilePositionSet.GetTileNumberX();
    const auto tileNumberY = tilePositionSet.GetTileNumberY();

    auto minTilePositionX = std::numeric_limits<float>::max();
    auto minTilePositionY = std::numeric_limits<float>::max();
    auto minTilePositionZ = std::numeric_limits<float>::max();

    for (auto tileIndexX = 0; tileIndexX < tileNumberX;++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePosition = tilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto tilePositionX = tilePosition.GetTilePositionX();
            const auto tilePositionY = tilePosition.GetTilePositionY();
            const auto tilePositionZ = tilePosition.GetTilePositionZ();

            minTilePositionX = std::min(minTilePositionX, tilePositionX);
            minTilePositionY = std::min(minTilePositionY, tilePositionY);
            minTilePositionZ = std::min(minTilePositionZ, tilePositionZ);
        }
    }
    
    TilePosition minTilePosition;
    minTilePosition.SetPositions(minTilePositionX, minTilePositionY, minTilePositionZ);

    return minTilePosition;
}

auto GetMaxPosition(const TilePositionSet& tilePositionSet) -> TilePosition {
    const auto tileNumberX = tilePositionSet.GetTileNumberX();
    const auto tileNumberY = tilePositionSet.GetTileNumberY();

    auto maxTilePositionX = std::numeric_limits<float>::min();
    auto maxTilePositionY = std::numeric_limits<float>::min();
    auto maxTilePositionZ = std::numeric_limits<float>::min();

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePosition = tilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto tilePositionX = tilePosition.GetTilePositionX();
            const auto tilePositionY = tilePosition.GetTilePositionY();
            const auto tilePositionZ = tilePosition.GetTilePositionZ();

            maxTilePositionX = std::max(maxTilePositionX, tilePositionX);
            maxTilePositionY = std::max(maxTilePositionY, tilePositionY);
            maxTilePositionZ = std::max(maxTilePositionZ, tilePositionZ);
        }
    }

    TilePosition maxTilePosition;
    maxTilePosition.SetPositions(maxTilePositionX, maxTilePositionY, maxTilePositionZ);

    return maxTilePosition;
}

auto GetMinMaxPosition(const TilePositionSet& tilePositionSet) -> std::tuple<TilePosition, TilePosition> {
    const auto tileNumberX = tilePositionSet.GetTileNumberX();
    const auto tileNumberY = tilePositionSet.GetTileNumberY();

    auto minTilePositionX = std::numeric_limits<float>::max();
    auto minTilePositionY = std::numeric_limits<float>::max();
    auto minTilePositionZ = std::numeric_limits<float>::max();

    auto maxTilePositionX = std::numeric_limits<float>::min();
    auto maxTilePositionY = std::numeric_limits<float>::min();
    auto maxTilePositionZ = std::numeric_limits<float>::min();

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePosition = tilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto tilePositionX = tilePosition.GetTilePositionX();
            const auto tilePositionY = tilePosition.GetTilePositionY();
            const auto tilePositionZ = tilePosition.GetTilePositionZ();

            minTilePositionX = std::min(minTilePositionX, tilePositionX);
            minTilePositionY = std::min(minTilePositionY, tilePositionY);
            minTilePositionZ = std::min(minTilePositionZ, tilePositionZ);

            maxTilePositionX = std::max(maxTilePositionX, tilePositionX);
            maxTilePositionY = std::max(maxTilePositionY, tilePositionY);
            maxTilePositionZ = std::max(maxTilePositionZ, tilePositionZ);
        }
    }

    TilePosition minTilePosition;
    minTilePosition.SetPositions(minTilePositionX, minTilePositionY, minTilePositionZ);

    TilePosition maxTilePosition;
    maxTilePosition.SetPositions(maxTilePositionX, maxTilePositionY, maxTilePositionZ);

    return { minTilePosition, maxTilePosition };
}

auto GetBoundaryLength(const TilePositionSet& tilePositionSet)-> BoundaryLength {
    const auto [minTilePosition, maxTilePosition] = GetMinMaxPosition(tilePositionSet);

    const auto tileNumberX = tilePositionSet.GetTileNumberX();
    const auto tileNumberY = tilePositionSet.GetTileNumberY();

    auto boundaryLengthUp = 0;
    auto boundaryLengthDown = 0;
    {
        constexpr auto upTileIndexY = 0;
        const auto downTileIndexY = tileNumberY - 1;
        for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
            const auto upTilePosition = tilePositionSet.GetTilePosition(tileIndexX, upTileIndexY);
            const auto downTilePosition = tilePositionSet.GetTilePosition(tileIndexX, downTileIndexY);

            const auto upLength = upTilePosition.GetTilePositionY() - minTilePosition.GetTilePositionY();
            const auto downLength = maxTilePosition.GetTilePositionY() - downTilePosition.GetTilePositionY();

            boundaryLengthUp = std::max(boundaryLengthUp, static_cast<int32_t>(std::floor(upLength)));
            boundaryLengthDown = std::max(boundaryLengthDown, static_cast<int32_t>(std::floor(downLength)));
        }
    }

    auto boundaryLengthLeft = 0;
    auto boundaryLengthRight = 0;
    {
        constexpr auto leftTileIndexX = 0;
        const auto rightTileIndexX = tileNumberX - 1;
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto leftTilePosition = tilePositionSet.GetTilePosition(leftTileIndexX, tileIndexY);
            const auto rightTilePosition = tilePositionSet.GetTilePosition(rightTileIndexX, tileIndexY);

            const auto leftLength = leftTilePosition.GetTilePositionX() - minTilePosition.GetTilePositionX();
            const auto rightLength = maxTilePosition.GetTilePositionX() - rightTilePosition.GetTilePositionX();

            boundaryLengthLeft = std::max(boundaryLengthLeft, static_cast<int32_t>(std::floor(leftLength)));
            boundaryLengthRight = std::max(boundaryLengthRight, static_cast<int32_t>(std::floor(rightLength)));
        }
    }

    return { boundaryLengthLeft, boundaryLengthRight, boundaryLengthUp, boundaryLengthDown };
}

auto CropBoundary(const StitchingMap& stitchingMap, const BoundaryLength& boundaryLength) -> StitchingMap {
    const auto stitchingTileNumberX = stitchingMap.GetStitchingTileNumberX();
    const auto stitchingTileNumberY = stitchingMap.GetStitchingTileNumberY();

    const auto stitchedDataSizeX = stitchingMap.GetStitchedDataSizeX();
    const auto stitchedDataSizeY = stitchingMap.GetStitchedDataSizeY();
    const auto stitchedDataSizeZ = stitchingMap.GetStitchedDataSizeZ();

    const auto centerPositionX = stitchingMap.GetCenterPositionX();
    const auto centerPositionY = stitchingMap.GetCenterPositionY();

    const auto [cropLeftLength, cropRightLength, cropUpLength, cropDownLength] = boundaryLength;

    const auto croppedStitchedDataSizeX = stitchedDataSizeX - (cropLeftLength + cropRightLength);
    const auto croppedStitchedDataSizeY = stitchedDataSizeY - (cropUpLength + cropDownLength);

    const auto croppedCenterPositionX = centerPositionX - cropLeftLength;
    const auto croppedCenterPositionY = centerPositionY - cropUpLength;

    StitchingMap croppedStitchingMap;
    croppedStitchingMap.SetStitchingTileNumber(stitchingTileNumberX, stitchingTileNumberY);
    croppedStitchingMap.SetStitchedDataSize(croppedStitchedDataSizeX, croppedStitchedDataSizeY, stitchedDataSizeZ);
    croppedStitchingMap.SetCenterPosition(croppedCenterPositionX, croppedCenterPositionY);

    for (auto tileIndexX = 0; tileIndexX < stitchingTileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < stitchingTileNumberY; ++tileIndexY) {
            const auto tileIndex = tileIndexX + stitchingTileNumberX * tileIndexY;

            const auto indexRange = stitchingMap.GetIndexRange(tileIndexX, tileIndexY);

            const auto x0 = indexRange.GetX0();
            const auto x1 = indexRange.GetX1();
            const auto y0 = indexRange.GetY0();
            const auto y1 = indexRange.GetY1();
            const auto z0 = indexRange.GetZ0();
            const auto z1 = indexRange.GetZ1();

            auto cropX0 = x0 - cropLeftLength;
            auto cropX1 = x1 - cropLeftLength;
            auto cropY0 = y0 - cropUpLength;
            auto cropY1 = y1 - cropUpLength;
            const auto cropZ0 = z0;
            const auto cropZ1 = z1;

            if (tileIndexX == 0) {
                cropX0 = 0;
            }

            if (tileIndexX == stitchingTileNumberX - 1) {
                cropX1 = x1 - cropLeftLength - cropRightLength;
            }

            if (tileIndexY == 0) {
                cropY0 = 0;
            }

            if (tileIndexY == stitchingTileNumberY - 1) {
                cropY1 = y1 - cropUpLength - cropDownLength;
            }

            IndexRange croppedIndexRange;
            croppedIndexRange.SetRange(cropX0, cropX1, cropY0, cropY1, cropZ0, cropZ1);

            croppedStitchingMap.SetIndexRange(tileIndex, croppedIndexRange);


            const auto stitchingTile = stitchingMap.GetStitchingTile(tileIndexX, tileIndexY);
            const auto layerXYList = stitchingTile.GetLayerDataTileIndexXYList();

            StitchingTile croppedStitchingTile;
            croppedStitchingTile.SetDataTileNumber(stitchingTileNumberX / 2 + 1, stitchingTileNumberY / 2 + 1);
            for (const auto& layerXY : layerXYList) {
                const auto& [layerIndexX, layerIndexY] = layerXY;

                const auto& stitchingTileIndexRange = stitchingTile.GetIndexRangeToStitchingTile(layerIndexX, layerIndexY);
                const auto globalStitchingRangeX0 = x0 + stitchingTileIndexRange.GetX0();
                const auto globalStitchingRangeX1 = x0 + stitchingTileIndexRange.GetX1();
                const auto globalStitchingRangeY0 = y0 + stitchingTileIndexRange.GetY0();
                const auto globalStitchingRangeY1 = y0 + stitchingTileIndexRange.GetY1();
                const auto globalStitchingRangeZ0 = z0 + stitchingTileIndexRange.GetZ0();
                const auto globalStitchingRangeZ1 = z0 + stitchingTileIndexRange.GetZ1();

                const auto globalCropIndexRangeX0 = cropX0 + cropLeftLength;
                const auto globalCropIndexRangeX1 = cropX1 + cropLeftLength;
                const auto globalCropIndexRangeY0 = cropY0 + cropUpLength;
                const auto globalCropIndexRangeY1 = cropY1 + cropUpLength;
                const auto globalCropIndexRangeZ0 = cropZ0;
                const auto globalCropIndexRangeZ1 = cropZ1;

                IndexRange globalCropIndexRange;
                globalCropIndexRange.SetRange(globalCropIndexRangeX0, globalCropIndexRangeX1,
                    globalCropIndexRangeY0, globalCropIndexRangeY1,
                    globalCropIndexRangeZ0, globalCropIndexRangeZ1);

                IndexRange toStitchingTileCropIndexRange;
                {
                    IndexRange globalStitchingRange;
                    globalStitchingRange.SetRange(globalStitchingRangeX0, globalStitchingRangeX1,
                        globalStitchingRangeY0, globalStitchingRangeY1,
                        globalStitchingRangeZ0, globalStitchingRangeZ1);

                    IndexRange globalStitchingTileCropIndexRange = globalStitchingRange & globalCropIndexRange;

                    auto toStitchingTileCropX0 = globalStitchingTileCropIndexRange.GetX0() - globalCropIndexRangeX0;
                    auto toStitchingTileCropX1 = globalStitchingTileCropIndexRange.GetX1() - globalCropIndexRangeX0;
                    auto toStitchingTileCropY0 = globalStitchingTileCropIndexRange.GetY0() - globalCropIndexRangeY0;
                    auto toStitchingTileCropY1 = globalStitchingTileCropIndexRange.GetY1() - globalCropIndexRangeY0;
                    auto toStitchingTileCropZ0 = globalStitchingTileCropIndexRange.GetZ0();
                    auto toStitchingTileCropZ1 = globalStitchingTileCropIndexRange.GetZ1();

                    toStitchingTileCropIndexRange.SetRange(toStitchingTileCropX0, toStitchingTileCropX1,
                        toStitchingTileCropY0, toStitchingTileCropY1,
                        toStitchingTileCropZ0, toStitchingTileCropZ1);
                }

                IndexRange toDataTileCropIndexRange;
                {
                    const auto toStitchingTileCropX0 = toStitchingTileCropIndexRange.GetX0();
                    const auto toStitchingTileCropX1 = toStitchingTileCropIndexRange.GetX1();
                    const auto toStitchingTileCropY0 = toStitchingTileCropIndexRange.GetY0();
                    const auto toStitchingTileCropY1 = toStitchingTileCropIndexRange.GetY1();

                    const auto globalStitchingTileCropIndexX0 = globalCropIndexRangeX0 + toStitchingTileCropX0;
                    const auto globalStitchingTileCropIndexX1 = globalCropIndexRangeX0 + toStitchingTileCropX1;
                    const auto globalStitchingTileCropIndexY0 = globalCropIndexRangeY0 + toStitchingTileCropY0;
                    const auto globalStitchingTileCropIndexY1 = globalCropIndexRangeY0 + toStitchingTileCropY1;

                    const auto dataCutLengthLeft = globalStitchingTileCropIndexX0 - globalStitchingRangeX0;
                    const auto dataCutLengthRight = globalStitchingRangeX1 - globalStitchingTileCropIndexX1;
                    const auto dataCutLengthUp = globalStitchingTileCropIndexY0 - globalStitchingRangeY0;
                    const auto dataCutLengthDown = globalStitchingRangeY1 - globalStitchingTileCropIndexY1;

                    const auto& toDataTileIndexRange = stitchingTile.GetIndexRangeToDataTile(layerIndexX, layerIndexY);
                    const auto toDataTileX0 = toDataTileIndexRange.GetX0();
                    const auto toDataTileX1 = toDataTileIndexRange.GetX1();
                    const auto toDataTileY0 = toDataTileIndexRange.GetY0();
                    const auto toDataTileY1 = toDataTileIndexRange.GetY1();
                    const auto toDataTileZ0 = toDataTileIndexRange.GetZ0();
                    const auto toDataTileZ1 = toDataTileIndexRange.GetZ1();

                    auto toDataTileCropX0 = toDataTileX0 + dataCutLengthLeft;
                    auto toDataTileCropX1 = toDataTileX1 - dataCutLengthRight;
                    auto toDataTileCropY0 = toDataTileY0 + dataCutLengthUp;
                    auto toDataTileCropY1 = toDataTileY1 - dataCutLengthDown;
                    const auto toDataTileCropZ0 = toDataTileZ0;
                    const auto toDataTileCropZ1 = toDataTileZ1;

                    toDataTileCropIndexRange.SetRange(toDataTileCropX0, toDataTileCropX1, 
                        toDataTileCropY0, toDataTileCropY1,
                        toDataTileCropZ0, toDataTileCropZ1);
                }

                croppedStitchingTile.AddLayer(layerIndexX, layerIndexY, toDataTileCropIndexRange, toStitchingTileCropIndexRange);

            }

            croppedStitchingMap.SetStitchingTile(tileIndex, croppedStitchingTile);
        }
    }

    return croppedStitchingMap;
}

auto CropBoundary(const StitchingMap& stitchingMap, const TargetSize& size) -> StitchingMap {
    const auto [targetSizeX, targetSizeY] = size;

    const auto halfTargetSizeX = targetSizeX / 2;
    const auto halfTargetSizeY = targetSizeY / 2;

    const auto centerPositionX = stitchingMap.GetCenterPositionX();
    const auto centerPositionY = stitchingMap.GetCenterPositionY();

    const auto startPositionX = centerPositionX - halfTargetSizeX;
    const auto endPositionX = startPositionX + targetSizeX - 1;

    const auto startPositionY = centerPositionY - halfTargetSizeY;
    const auto endPositionY = startPositionY + targetSizeY - 1;

    const auto stitchedDataSizeX = stitchingMap.GetStitchedDataSizeX();
    const auto stitchedDataSizeY = stitchingMap.GetStitchedDataSizeY();

    const auto cropLeftLength = startPositionX;
    const auto cropRightLength = stitchedDataSizeX - endPositionX - 1;
    const auto cropUpLength = startPositionY;
    const auto cropDownLength = stitchedDataSizeY - endPositionY - 1;

    return CropBoundary(stitchingMap, { cropLeftLength, cropRightLength, cropUpLength, cropDownLength });
}
