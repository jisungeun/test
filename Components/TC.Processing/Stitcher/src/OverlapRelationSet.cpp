#include "OverlapRelationSet.h"
#include <QMap>

typedef int32_t OverlapIndex;

class OverlapRelationSet::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    QMap<OverlapIndex, OverlapRelation> overlapRelationMap{};

    auto CalculateOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->OverlapIndex;
};

auto OverlapRelationSet::Impl::CalculateOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> OverlapIndex {
	const auto overlapNumberX = 2 * tileNumberX - 1;
    const auto overlapIndex = overlapIndexY * overlapNumberX + overlapIndexX;
    return overlapIndex;
}

OverlapRelationSet::OverlapRelationSet() : d(new Impl()) {
}

OverlapRelationSet::OverlapRelationSet(const OverlapRelationSet& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

OverlapRelationSet::~OverlapRelationSet() = default;

auto OverlapRelationSet::operator=(const OverlapRelationSet& other) -> OverlapRelationSet& {
    *(this->d) = *(other.d);
    return *this;
}

auto OverlapRelationSet::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto OverlapRelationSet::InsertOverlapRelation(const int32_t& overlapIndexX, const int32_t& overlapIndexY,
    const OverlapRelation& overlapRelation) -> void {
    const auto overlapIndex = d->CalculateOverlapIndex(overlapIndexX, overlapIndexY);
    d->overlapRelationMap[overlapIndex] = overlapRelation;
}

auto OverlapRelationSet::GetOverlapRelation(const int32_t& overlapIndexX, const int32_t& overlapIndexY) const
    -> OverlapRelation {
    const auto overlapIndex = d->CalculateOverlapIndex(overlapIndexX, overlapIndexY);
    return d->overlapRelationMap[overlapIndex];
}
