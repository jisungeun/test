#include "TileDependencyRepository.h"

#include <QList>

class TileDependencyRepository::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    QList<TileDependency> tileList{};

    auto GetTileDependency(const TileIndexXY& tileIndexXY)->TileDependency&;
    auto ChainFlipReference(const TileIndexXY& srcTileIndexXY, const TileIndexXY& destTileIndexXY)->void;
};

auto TileDependencyRepository::Impl::GetTileDependency(const TileIndexXY& tileIndexXY) -> TileDependency& {
    const auto& [tileIndexX, tileIndexY] = tileIndexXY;
    const auto tileIndex = tileIndexX + tileIndexY * this->tileNumberX;
    return this->tileList[tileIndex];
}

auto TileDependencyRepository::Impl::ChainFlipReference(const TileIndexXY& srcTileIndexXY,
    const TileIndexXY& destTileIndexXY) -> void {
    const auto& [destTileIndexX, destTileIndexY] = destTileIndexXY;

    auto& srcTileDependency = this->GetTileDependency(srcTileIndexXY);

    if (srcTileDependency.IsReferencing()) {
        const auto referenceTileIndexX = srcTileDependency.GetReferenceTileIndexX();
        const auto referenceTileIndexY = srcTileDependency.GetReferenceTileIndexY();

        const TileIndexXY destTileIndexXY = { referenceTileIndexX, referenceTileIndexY };

        ChainFlipReference(destTileIndexXY, srcTileIndexXY);
    }

    srcTileDependency.SetReferenceTileIndex(destTileIndexX, destTileIndexY);
}

TileDependencyRepository::TileDependencyRepository() : d(new Impl()) {
}

TileDependencyRepository::TileDependencyRepository(const TileDependencyRepository& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TileDependencyRepository::~TileDependencyRepository() = default;

auto TileDependencyRepository::operator=(const TileDependencyRepository& other) -> TileDependencyRepository& {
    *(this->d) = *(other.d);
    return *this;
}

auto TileDependencyRepository::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto TileDependencyRepository::Initialize() -> void {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;

    d->tileList = QList<TileDependency>{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        d->tileList.push_back(TileDependency{});
    }
}

auto TileDependencyRepository::SetReference(const TileIndexXY& srcTileIndexXY, const TileIndexXY& destTileIndexXY)
    -> void {
    auto& srcTileDependency = GetTileDependency(srcTileIndexXY);

    if (srcTileDependency.IsReferencing()) {
        d->ChainFlipReference(destTileIndexXY, srcTileIndexXY);
    } else {
        const auto& [destTileIndexX, destTileIndexY] = destTileIndexXY;
        srcTileDependency.SetReferenceTileIndex(destTileIndexX, destTileIndexY);
    }
}

auto TileDependencyRepository::GetTileDependency(const TileIndexXY& tileIndexXY) -> TileDependency& {
    return d->GetTileDependency(tileIndexXY);
}
