#pragma once

#include <memory>

#include "OverlapNode.h"
#include "OverlapRelationSet.h"
#include "TileConfiguration.h"

class OverlapNodeRepository {
public:
    OverlapNodeRepository();
    ~OverlapNodeRepository();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void;
    auto SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet)->void;
    auto Initialize()->void;

    auto GetOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->OverlapNode&;
    auto IsConnectable(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;
    auto Connect(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->void;
    auto Close(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->void;
    auto GetMaximumScoreOpenedNode() const -> OverlapNode*;
    auto ConnectionCompleted() const ->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
