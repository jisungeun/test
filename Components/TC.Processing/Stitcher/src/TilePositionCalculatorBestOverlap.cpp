#include "TilePositionCalculatorBestOverlap.h"

#include <vector>

#include "DependencyToPositionConverter.h"
#include "OverlapNodeRepository.h"
#include "TileDependencyRepository.h"

struct OverlapIndexXY {
    int32_t overlapIndexX{};
    int32_t overlapIndexY{};
};

class TilePositionCalculatorBestOverlap::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration;
    OverlapRelationSet overlapRelationSet;
};

TilePositionCalculatorBestOverlap::TilePositionCalculatorBestOverlap() : d(new Impl()) {
}

TilePositionCalculatorBestOverlap::~TilePositionCalculatorBestOverlap() = default;

auto TilePositionCalculatorBestOverlap::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto TilePositionCalculatorBestOverlap::SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet) -> void {
    d->overlapRelationSet = overlapRelationSet;
}

auto TilePositionCalculatorBestOverlap::Calculate() -> TilePositionSet {
    const auto& tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto& tileNumberY = d->tileConfiguration.GetTileNumberY();

    OverlapNodeRepository overlapNodeRepository;
    overlapNodeRepository.SetTileConfiguration(d->tileConfiguration);
    overlapNodeRepository.SetOverlapRelationSet(d->overlapRelationSet);
    overlapNodeRepository.Initialize();

    while (!overlapNodeRepository.ConnectionCompleted()) {
        const auto maximumScoredOpenedNode = overlapNodeRepository.GetMaximumScoreOpenedNode();
        const auto nodeOverlapIndexX = maximumScoredOpenedNode->GetOverlapIndexX();
        const auto nodeOverlapIndexY = maximumScoredOpenedNode->GetOverlapIndexY();

        if (overlapNodeRepository.IsConnectable(nodeOverlapIndexX, nodeOverlapIndexY)) {
            overlapNodeRepository.Connect(nodeOverlapIndexX, nodeOverlapIndexY);
        } else {
            overlapNodeRepository.Close(nodeOverlapIndexX, nodeOverlapIndexY);
        }
    }

    TileDependencyRepository tileDependencyRepository;
    tileDependencyRepository.SetTileNumber(tileNumberX, tileNumberY);
    tileDependencyRepository.Initialize();

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            if ((tileIndexX == 0) && (tileIndexY == 0)) {
                continue;
            }
            if (tileIndexX != 0) {
                const auto leftOverlapIndexX = 2 * tileIndexX - 1;
                const auto leftOverlapIndexY = 2 * tileIndexY;

                const auto& leftOverlapNode = overlapNodeRepository.GetOverlapNode(leftOverlapIndexX, leftOverlapIndexY);
                if (leftOverlapNode.GetStatus() == OverlapNode::Status::Connected) {
                    tileDependencyRepository.SetReference({ tileIndexX, tileIndexY }, { tileIndexX - 1, tileIndexY });
                }
            }
            if (tileIndexY != 0) {
                const auto upOverlapIndexX = 2 * tileIndexX;
                const auto upOverlapIndexY = 2 * tileIndexY - 1;

                const auto& upOverlapNode = overlapNodeRepository.GetOverlapNode(upOverlapIndexX, upOverlapIndexY);
                if (upOverlapNode.GetStatus() == OverlapNode::Status::Connected) {
                    tileDependencyRepository.SetReference({ tileIndexX, tileIndexY }, { tileIndexX, tileIndexY - 1 });
                }
            } 
        }
    }

    DependencyToPositionConverter dependencyToPositionConverter;
    dependencyToPositionConverter.SetTileConfiguration(d->tileConfiguration);
    dependencyToPositionConverter.SetOverlapRelationSet(d->overlapRelationSet);
    dependencyToPositionConverter.SetTileDependencyRepository(tileDependencyRepository);

    const auto tilePositionSet = dependencyToPositionConverter.Convert();
    
    return tilePositionSet;
}
