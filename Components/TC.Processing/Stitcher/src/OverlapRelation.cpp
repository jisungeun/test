#include "OverlapRelation.h"

class OverlapRelation::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    float shiftValueX{};
    float shiftValueY{};
    float shiftValueZ{};

    float reliability{};
};

OverlapRelation::OverlapRelation() : d(new Impl()) {
}

OverlapRelation::OverlapRelation(const OverlapRelation& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

OverlapRelation::~OverlapRelation() = default;

auto OverlapRelation::operator=(const OverlapRelation& other) -> OverlapRelation& {
    *(this->d) = *(other.d);
    return *this;
}

auto OverlapRelation::SetShiftValue(const float& valueX, const float& valueY, const float& valueZ) -> void {
    d->shiftValueX = valueX;
    d->shiftValueY = valueY;
    d->shiftValueZ = valueZ;
}

auto OverlapRelation::GetShiftValueX() const -> const float& {
    return d->shiftValueX;
}

auto OverlapRelation::GetShiftValueY() const -> const float& {
    return d->shiftValueY;
}

auto OverlapRelation::GetShiftValueZ() const -> const float& {
    return d->shiftValueZ;
}

auto OverlapRelation::SetReliability(const float& reliability) -> void {
    d->reliability = reliability;
}

auto OverlapRelation::GetReliability() const -> const float& {
    return d->reliability;
}
