#include "TilePositionSet.h"

#include <QMap>

typedef int32_t TileIndex;

class TilePositionSet::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    QMap<TileIndex, TilePosition> tilePositionMap;

    auto CalculateTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY)->TileIndex;
};

auto TilePositionSet::Impl::CalculateTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY) -> TileIndex {
    const auto tileIndex = tileIndexY * this->tileNumberX + tileIndexX;
    return tileIndex;
}

TilePositionSet::TilePositionSet() : d(new Impl()) {
}

TilePositionSet::TilePositionSet(const TilePositionSet& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TilePositionSet::~TilePositionSet() = default;

auto TilePositionSet::operator=(const TilePositionSet& other) -> TilePositionSet& {
    *(this->d) = *(other.d);
    return *this;
}

auto TilePositionSet::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto TilePositionSet::GetTileNumberX() const -> const int32_t& {
    return d->tileNumberX;
}

auto TilePositionSet::GetTileNumberY() const -> const int32_t& {
    return d->tileNumberY;
}

auto TilePositionSet::InsertTilePosition(const int32_t& tileIndexX, const int32_t& tileIndexY,
    const TilePosition& tilePosition) -> void {
    const auto tileIndex = d->CalculateTileIndex(tileIndexX, tileIndexY);
    d->tilePositionMap[tileIndex] = tilePosition;
}

auto TilePositionSet::GetTilePosition(const int32_t& tileIndexX, const int32_t& tileIndexY) const -> TilePosition {
    const auto tileIndex = d->CalculateTileIndex(tileIndexX, tileIndexY);
    return d->tilePositionMap[tileIndex];
}
