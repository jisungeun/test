#include "TileConfiguration.h"

class TileConfiguration::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl&;

    int32_t tileNumberX = 0;
    int32_t tileNumberY = 0;

    int32_t tileSizeXInPixel = 0;
    int32_t tileSizeYInPixel = 0;
    int32_t tileSizeZInPixel = 0;

    int32_t overlapLengthXInPixel = 0;
    int32_t overlapLengthYInPixel = 0;
};

auto TileConfiguration::Impl::operator=(const Impl& other) -> Impl& {
    this->tileNumberX = other.tileNumberX;
    this->tileNumberY = other.tileNumberY;
    this->tileSizeXInPixel = other.tileSizeXInPixel;
    this->tileSizeYInPixel = other.tileSizeYInPixel;
    this->tileSizeZInPixel = other.tileSizeZInPixel;
    this->overlapLengthXInPixel = other.overlapLengthXInPixel;
    this->overlapLengthYInPixel = other.overlapLengthYInPixel;
    return *this;
}

TileConfiguration::TileConfiguration() : d(new Impl()) {
}

TileConfiguration::TileConfiguration(const TileConfiguration& other) : d(new Impl()) {
    *d = *other.d;
}

TileConfiguration::~TileConfiguration() = default;

auto TileConfiguration::operator=(const TileConfiguration& other) -> TileConfiguration& {
    *(this->d) = *(other.d);
    return *this;
}

auto TileConfiguration::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto TileConfiguration::SetTileSizeInPixel(const int32_t& tileSizeX, const int32_t& tileSizeY, const int32_t& tileSizeZ) -> void {
    d->tileSizeXInPixel = tileSizeX;
    d->tileSizeYInPixel = tileSizeY;
    d->tileSizeZInPixel = tileSizeZ;
}

auto TileConfiguration::SetOverlapLengthInPixel(const int32_t& overlapLengthX, const int32_t& overlapLengthY) -> void {
    d->overlapLengthXInPixel = overlapLengthX;
    d->overlapLengthYInPixel = overlapLengthY;
}

auto TileConfiguration::GetTileNumberX() const -> const int32_t& {
    return d->tileNumberX;
}

auto TileConfiguration::GetTileNumberY() const -> const int32_t& {
    return d->tileNumberY;
}

auto TileConfiguration::GetTileSizeXInPixel() const -> const int32_t& {
    return d->tileSizeXInPixel;
}

auto TileConfiguration::GetTileSizeYInPixel() const -> const int32_t& {
    return d->tileSizeYInPixel;
}

auto TileConfiguration::GetTileSizeZInPixel() const -> const int32_t& {
    return d->tileSizeZInPixel;
}

auto TileConfiguration::GetOverlapLengthXInPixel() const -> const int32_t& {
    return d->overlapLengthXInPixel;
}

auto TileConfiguration::GetOverlapLengthYInPixel() const -> const int32_t& {
    return d->overlapLengthYInPixel;
}
