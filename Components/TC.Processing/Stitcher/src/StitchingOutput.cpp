#include "StitchingOutput.h"

class StitchingOutput::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    StitchingWriterResult stitchingWriterResult;
};

StitchingOutput::StitchingOutput() : d(new Impl()) {
}

StitchingOutput::~StitchingOutput() = default;

auto StitchingOutput::SetStitchingWriterResult(const StitchingWriterResult& stitchingWriterResult) -> void {
    d->stitchingWriterResult = stitchingWriterResult;
}
