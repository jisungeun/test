#pragma once

#include <memory>

class TileDependency {
public:
    TileDependency();
    TileDependency(const TileDependency& other);
    ~TileDependency();

    auto operator=(const TileDependency& other)->TileDependency&;

    auto IsReferencing() const ->const bool&;

    auto SetReferenceTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY)->void;
    auto GetReferenceTileIndexX() const -> const int32_t&;
    auto GetReferenceTileIndexY() const -> const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};