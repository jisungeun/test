#pragma once

#include <memory>

#include "StitchingTile.h"

class StitchingMap {
public:
    StitchingMap();
    StitchingMap(const StitchingMap& other);
    ~StitchingMap();

    auto operator=(const StitchingMap& other)->StitchingMap&;

    auto SetStitchingTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;
    auto GetStitchingTileNumberX()const->const int32_t&;
    auto GetStitchingTileNumberY()const->const int32_t&;

    auto SetStitchingTile(const int32_t& stitchingIndex, const StitchingTile& stitchingTile)->void;
    auto GetStitchingTile(const int32_t& stitchingTileIndexX, const int32_t& stitchingTileIndexY) const->StitchingTile;

    auto SetIndexRange(const int32_t& stitchingTileIndex, const IndexRange& indexRange)->void;
    auto GetIndexRange(const int32_t& stitchingTileIndexX, const int32_t& stitchingTileIndexY) const ->IndexRange;

    auto SetStitchedDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;

    auto GetStitchedDataSizeX() const -> const int32_t&;
    auto GetStitchedDataSizeY() const -> const int32_t&;
    auto GetStitchedDataSizeZ() const -> const int32_t&;

    auto SetCenterPosition(const int32_t& positionX, const int32_t& positionY)->void;

    auto GetCenterPositionX() const -> const int32_t&;
    auto GetCenterPositionY() const -> const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
