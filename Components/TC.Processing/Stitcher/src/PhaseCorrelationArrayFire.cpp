#define NOMINMAX

#include "PhaseCorrelationArrayFire.h"
#include "arrayfire.h"

typedef std::shared_ptr<float[]> DataPointer;
struct PeakPointIndexXYZ {
    float x;
    float y;
    float z;
};

struct ShiftValueXYZ {
    float x;
    float y;
    float z;
};

struct CropScope {
    af::seq referenceCropScope;
    af::seq comparingCropScope;
};

struct Result {
    float pearsonCorrelationCoefficient;
    ShiftValueXYZ shiftValue;
};

class PhaseCorrelationArrayFire::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    DataPointer referenceData{};
    DataPointer comparingData{};

    float reliability{};
    float shiftValueX{};
    float shiftValueY{};
    float shiftValueZ{};

    auto GetPhaseCorrelationData(const af::array& referenceArray, const af::array& comparingArray)->af::array;
    auto FindPeakPointIndex(const af::array& phaseCorrelation)->PeakPointIndexXYZ;
    static auto CalculateShiftValue(const float& peakPointIndex, const int32_t& dataSize)->float;
    auto ConvertToShiftValue(const PeakPointIndexXYZ& peakPointIndexXYZ)->ShiftValueXYZ;

    static auto CalculateCropScope(const float& shiftValue, const int32_t& dataSize)->CropScope;
    auto GetPearsonCorrelationCoefficient(const af::array& referenceArray, const af::array& comparingArray, 
        const ShiftValueXYZ& shiftValueXYZ)->float;

    auto SelectBestResult(const std::vector<Result>& resultVector)->Result;
};

auto PhaseCorrelationArrayFire::Impl::GetPhaseCorrelationData(const af::array& referenceArray, 
    const af::array& comparingArray) -> af::array {

    if (referenceArray.dims() != 3 && comparingArray.dims() != 3) {
        auto dataSpace1 = af::fft3(af::pad(referenceArray, { 0,0,0 }, { 0,0,1 }, af::borderType::AF_PAD_PERIODIC));
        auto dataSpace2 = af::fft3(af::pad(comparingArray, { 0,0,0 }, { 0,0,1 }, af::borderType::AF_PAD_PERIODIC));

        dataSpace2 = af::conjg(dataSpace2);
        dataSpace1 = dataSpace1 * dataSpace2;

        dataSpace2 = (dataSpace1 == 0);
        dataSpace1(dataSpace2) = 1;

        dataSpace2 = af::abs(dataSpace1);
        dataSpace1 = dataSpace1 / dataSpace2;

        dataSpace1 = af::ifft3(dataSpace1);
        dataSpace1 = real(dataSpace1);

        auto& phaseCorrelationArray = dataSpace1;
        phaseCorrelationArray.eval();

        return phaseCorrelationArray;

    } else {
        auto dataSpace1 = af::fft3(referenceArray);
        auto dataSpace2 = af::fft3(comparingArray);

        dataSpace2 = af::conjg(dataSpace2);
        dataSpace1 = dataSpace1 * dataSpace2;

        dataSpace2 = (dataSpace1 == 0);
        dataSpace1(dataSpace2) = 1;

        dataSpace2 = af::abs(dataSpace1);
        dataSpace1 = dataSpace1 / dataSpace2;

        dataSpace1 = af::ifft3(dataSpace1);
        dataSpace1 = real(dataSpace1);

        auto& phaseCorrelationArray = dataSpace1;
        phaseCorrelationArray.eval();

        return phaseCorrelationArray;
    }
}

auto PhaseCorrelationArrayFire::Impl::FindPeakPointIndex(const af::array& phaseCorrelation) -> PeakPointIndexXYZ {
    af::array dim0MaxValues, dim0MaxIndices;
    af::array dim1MaxValues, dim1MaxIndices;
    af::array dim2MaxIndices;
    af::array maxValue;

    af::max(dim0MaxValues, dim0MaxIndices, phaseCorrelation, 0);
    af::max(dim1MaxValues, dim1MaxIndices, dim0MaxValues, 1);
    af::max(maxValue, dim2MaxIndices, dim1MaxValues, 2);

    const auto peakPointIndexZ = static_cast<float>(dim2MaxIndices.scalar<uint32_t>());
    const auto peakPointIndexX = static_cast<float>(dim1MaxIndices(dim2MaxIndices).scalar<uint32_t>());
    const auto peakPointIndexY = static_cast<float>(dim0MaxIndices(0, peakPointIndexX, peakPointIndexZ).scalar<uint32_t>());

    return { peakPointIndexX, peakPointIndexY, peakPointIndexZ };
}

auto PhaseCorrelationArrayFire::Impl::CalculateShiftValue(const float& peakPointIndex, const int32_t& dataSize)
    -> float {
    float shiftValue;
    if (peakPointIndex > static_cast<float>(dataSize / 2)) {
        shiftValue = peakPointIndex - static_cast<float>(dataSize);
    } else {
        shiftValue = peakPointIndex;
    }

    return shiftValue;
}

auto PhaseCorrelationArrayFire::Impl::ConvertToShiftValue(const PeakPointIndexXYZ& peakPointIndexXYZ)
    -> ShiftValueXYZ {
    const auto& [peakPointIndexX, peakPointIndexY, peakPointIndexZ] = peakPointIndexXYZ;
    
    const auto convertedShiftValueX = this->CalculateShiftValue(peakPointIndexX, this->dataSizeX);
    const auto convertedShiftValueY = this->CalculateShiftValue(peakPointIndexY, this->dataSizeY);
    const auto convertedShiftValueZ = this->CalculateShiftValue(peakPointIndexZ, this->dataSizeZ);

    return { convertedShiftValueX, convertedShiftValueY, convertedShiftValueZ };
}

auto PhaseCorrelationArrayFire::Impl::CalculateCropScope(const float& shiftValue, const int32_t& dataSize)
    -> CropScope {
    af::seq referenceCropScope, comparingCropScope;

    if (shiftValue > 0) {
        referenceCropScope = af::seq(static_cast<int32_t>(shiftValue), dataSize - 1);
        comparingCropScope = af::seq(0, dataSize - 1 - static_cast<int32_t>(std::round(shiftValue)));
    } else {
        referenceCropScope = af::seq(0, dataSize - 1 + static_cast<int32_t>(std::round(shiftValue)));
        comparingCropScope = af::seq(-static_cast<int32_t>(std::round(shiftValue)), dataSize - 1);
    }

    return { referenceCropScope, comparingCropScope };
}

auto PhaseCorrelationArrayFire::Impl::GetPearsonCorrelationCoefficient(const af::array& referenceArray,
    const af::array& comparingArray, const ShiftValueXYZ& shiftValueXYZ) -> float {
    const auto& [shiftValueX, shiftValueY, shiftValueZ] = shiftValueXYZ;

    const auto [referenceCropScopeX, comparingCropScopeX] = this->CalculateCropScope(shiftValueX,this->dataSizeX);
    const auto [referenceCropScopeY, comparingCropScopeY] = this->CalculateCropScope(shiftValueY,this->dataSizeY);
    const auto [referenceCropScopeZ, comparingCropScopeZ] = this->CalculateCropScope(shiftValueZ,this->dataSizeZ);

    const auto croppedReferenceArray = referenceArray(referenceCropScopeY, referenceCropScopeX, referenceCropScopeZ).copy();
    const auto croppedComparingArray = comparingArray(comparingCropScopeY, comparingCropScopeX, comparingCropScopeZ).copy();

    const auto pearsonCorrelationCoefficient = af::corrcoef<float>(croppedReferenceArray.as(f64), croppedComparingArray.as(f64));

    return pearsonCorrelationCoefficient;
}

auto PhaseCorrelationArrayFire::Impl::SelectBestResult(const std::vector<Result>& resultVector) -> Result {
    float bestCoefficient = std::numeric_limits<float>::lowest();
    ShiftValueXYZ bestShiftValue{};
    for (const auto& result : resultVector) {
        const auto& resultCoefficient = result.pearsonCorrelationCoefficient;
        const auto& resultShiftValue = result.shiftValue;

        if (resultCoefficient > bestCoefficient) {
            bestCoefficient = resultCoefficient;
            bestShiftValue = resultShiftValue;
        }
    }

    return { bestCoefficient, bestShiftValue };
}

PhaseCorrelationArrayFire::PhaseCorrelationArrayFire() : d(new Impl()) {
}

PhaseCorrelationArrayFire::~PhaseCorrelationArrayFire() = default;

auto PhaseCorrelationArrayFire::SetData(const std::shared_ptr<float[]>& referenceData,
    const std::shared_ptr<float[]>& comparingData) -> void {
    d->referenceData = referenceData;
    d->comparingData = comparingData;
}

auto PhaseCorrelationArrayFire::SetDataSize(const int32_t& dataSizeX, const int32_t& dataSizeY,
    const int32_t& dataSizeZ) -> void {
    d->dataSizeX = dataSizeX;
    d->dataSizeY = dataSizeY;
    d->dataSizeZ = dataSizeZ;
}

auto PhaseCorrelationArrayFire::Run() -> bool {
    {
        const af::array referenceArray{ d->dataSizeY, d->dataSizeX, d->dataSizeZ, d->referenceData.get() };
        const af::array comparingArray{ d->dataSizeY, d->dataSizeX, d->dataSizeZ, d->comparingData.get() };

        auto phaseCorrelationData = d->GetPhaseCorrelationData(referenceArray, comparingArray);
        af::deviceGC();

        constexpr auto iterationNumber = 10;
        std::vector<Result> resultVector;
        resultVector.assign(iterationNumber, Result{});
        for (auto index = 0; index < iterationNumber; ++index) {
            const auto peakPointIndexXYZ = d->FindPeakPointIndex(phaseCorrelationData);
            const auto convertedShiftValueXYZ = d->ConvertToShiftValue(peakPointIndexXYZ);
            const auto pearsonCoefficient =
                d->GetPearsonCorrelationCoefficient(referenceArray, comparingArray, convertedShiftValueXYZ);

            resultVector.at(index) = Result{ pearsonCoefficient,convertedShiftValueXYZ };

            phaseCorrelationData(static_cast<int32_t>(peakPointIndexXYZ.y), static_cast<int32_t>(peakPointIndexXYZ.x),
                static_cast<int32_t>(peakPointIndexXYZ.z)) = 0;
            af::deviceGC();
        }

        const auto bestResult = d->SelectBestResult(resultVector);

        d->reliability = bestResult.pearsonCorrelationCoefficient;
        d->shiftValueX = bestResult.shiftValue.x;
        d->shiftValueY = bestResult.shiftValue.y;
        d->shiftValueZ = bestResult.shiftValue.z;
    }
    af::deviceGC();
    return true;
}

auto PhaseCorrelationArrayFire::GetReliability() const -> const float& {
    return d->reliability;
}

auto PhaseCorrelationArrayFire::GetShiftValueX() const -> const float& {
    return d->shiftValueX;
}

auto PhaseCorrelationArrayFire::GetShiftValueY() const -> const float& {
    return d->shiftValueY;
}

auto PhaseCorrelationArrayFire::GetShiftValueZ() const -> const float& {
    return d->shiftValueZ;
}
