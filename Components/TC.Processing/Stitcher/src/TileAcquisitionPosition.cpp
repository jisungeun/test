#include "TileAcquisitionPosition.h"

class TileAcquisitionPosition::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;
    auto operator=(const Impl& other)->Impl&;

    double positionX = 0;
    double positionY = 0;
    double positionZ = 0;
};

auto TileAcquisitionPosition::Impl::operator=(const Impl& other) -> Impl& {
    this->positionX = other.positionX;
    this->positionY = other.positionY;
    this->positionZ = other.positionZ;
    return *this;
}

TileAcquisitionPosition::TileAcquisitionPosition() : d(new Impl()) {
}

TileAcquisitionPosition::TileAcquisitionPosition(const TileAcquisitionPosition& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TileAcquisitionPosition::~TileAcquisitionPosition() = default;

auto TileAcquisitionPosition::operator=(const TileAcquisitionPosition& other) -> TileAcquisitionPosition& {
    *(this->d) = *(other.d);
    return *this;
}

auto TileAcquisitionPosition::SetPositionInMillimeter(const double& positionX, const double& positionY,
    const double& positionZ) -> void {
    d->positionX = positionX;
    d->positionY = positionY;
    d->positionZ = positionZ;
}

auto TileAcquisitionPosition::GetPositionXInMillimeter() const -> const double& {
    return d->positionX;
}

auto TileAcquisitionPosition::GetPositionYInMillimeter() const -> const double& {
    return d->positionY;
}

auto TileAcquisitionPosition::GetPositionZInMillimeter() const -> const double& {
    return d->positionZ;
}
