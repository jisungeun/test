#include "StitchingMapGenerator.h"

#include <QMap>

#include "StitchingCenterPositionCalculator.h"

typedef int32_t StitchingTileIndex;

struct MinTilePosition {
    int32_t x{};
    int32_t y{};
    int32_t z{};
};

struct MaxTilePosition {
    int32_t x{};
    int32_t y{};
    int32_t z{};
};

class StitchingMapGenerator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration{};
    TilePositionSet dataTilePositionSet{};

    QMap<StitchingTileIndex, StitchingTile> stitchingTileIndexMap;
    QMap<StitchingTileIndex, IndexRange> stitchingTileIndexRangeMap;
    QMap<StitchingTileIndex, IndexRange> normalizedStitchingTileIndexRangeMap;

    int32_t stitchedDataSizeX{};
    int32_t stitchedDataSizeY{};
    int32_t stitchedDataSizeZ{};

    int32_t centerPositionX{};
    int32_t centerPositionY{};

    StitchingMap stitchingMap{};
    
    auto GenerateStitchingTileIndexRangeMap()const->QMap<StitchingTileIndex, IndexRange>;
    auto NormalizeStitchingTileIndexRangeMap(const QMap<StitchingTileIndex, IndexRange>& stitchingTileIndexRangeMap)
        const->QMap<StitchingTileIndex, IndexRange>;
    auto GetMinMaxTilePosition() const->std::tuple<MinTilePosition, MaxTilePosition>;
    auto GenerateStitchingTileIndexMap()const->QMap<StitchingTileIndex, StitchingTile>;
    auto GetIndexRangeToDataTileAndStitchingTile(const int32_t& stitchingTileIndexX,
        const int32_t& stitchingTileIndexY, const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
        ->std::tuple<IndexRange, IndexRange>;

    auto GetStitchedDataDimension() const->std::tuple<int32_t, int32_t, int32_t>;


private:
    auto GenerateDivisionLine() const->std::tuple<std::vector<int32_t>, std::vector<int32_t>>;
};

auto StitchingMapGenerator::Impl::GenerateStitchingTileIndexRangeMap() const -> QMap<StitchingTileIndex, IndexRange> {
    const auto [divisionLinePositionSetX, divisionLinePositionSetY] = this->GenerateDivisionLine();

    const auto dataTileNumberX = this->tileConfiguration.GetTileNumberX();
    const auto dataTileNumberY = this->tileConfiguration.GetTileNumberY();

    const auto dataTileSizeZ = this->tileConfiguration.GetTileSizeZInPixel();

    const auto positionZ0 = this->dataTilePositionSet.GetTilePosition(0, 0).GetTilePositionZ();

    const auto stitchingTileNumberX = 2 * dataTileNumberX - 1;
    const auto stitchingTileNumberY = 2 * dataTileNumberY - 1;

    QMap<StitchingTileIndex, IndexRange> stitchingTileIndexRangeMap;
    for (auto stitchingTileIndexX = 0; stitchingTileIndexX < stitchingTileNumberX; ++stitchingTileIndexX) {
        for (auto stitchingTileIndexY = 0; stitchingTileIndexY < stitchingTileNumberY; ++stitchingTileIndexY) {
            const auto stitchingTileIndexRangeX0 = divisionLinePositionSetX.at(stitchingTileIndexX);
            const auto stitchingTileIndexRangeX1 = divisionLinePositionSetX.at(stitchingTileIndexX + 1) - 1;

            const auto stitchingTileIndexRangeY0 = divisionLinePositionSetY.at(stitchingTileIndexY);
            const auto stitchingTileIndexRangeY1 = divisionLinePositionSetY.at(stitchingTileIndexY + 1) - 1;

            const auto stitchingTileIndexRangeZ0 = static_cast<int32_t>(std::round(positionZ0));
            const auto stitchingTileIndexRangeZ1 = stitchingTileIndexRangeZ0 + dataTileSizeZ - 1;

            const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * stitchingTileNumberX;

            IndexRange stitchingTileIndexRange;
            stitchingTileIndexRange.SetRange(
                stitchingTileIndexRangeX0, stitchingTileIndexRangeX1,
                stitchingTileIndexRangeY0, stitchingTileIndexRangeY1,
                stitchingTileIndexRangeZ0, stitchingTileIndexRangeZ1);

            stitchingTileIndexRangeMap[stitchingTileIndex] = stitchingTileIndexRange;
        }
    }

    return stitchingTileIndexRangeMap;
}

auto StitchingMapGenerator::Impl::NormalizeStitchingTileIndexRangeMap(
    const QMap<StitchingTileIndex, IndexRange>& stitchingTileIndexRangeMap) const
    -> QMap<StitchingTileIndex, IndexRange> {

    auto [minTilePosition, maxTilePosition] = this->GetMinMaxTilePosition();

    QMap<StitchingTileIndex, IndexRange> normalizedStitchingTileIndexRangeMap;
    for (auto iterator = stitchingTileIndexRangeMap.begin(); iterator != stitchingTileIndexRangeMap.end(); ++iterator) {
        const auto stitchingTileIndex = iterator.key();
        const auto& indexRange = iterator.value();

        IndexRange normalizedIndexRange;
        normalizedIndexRange.SetRange(
            indexRange.GetX0() - minTilePosition.x, indexRange.GetX1() - minTilePosition.x,
            indexRange.GetY0() - minTilePosition.y, indexRange.GetY1() - minTilePosition.y,
            indexRange.GetZ0() - minTilePosition.z, indexRange.GetZ1() - minTilePosition.z);

        normalizedStitchingTileIndexRangeMap[stitchingTileIndex] = normalizedIndexRange;
    }

    return normalizedStitchingTileIndexRangeMap;
}

auto StitchingMapGenerator::Impl::GetMinMaxTilePosition() const -> std::tuple<MinTilePosition, MaxTilePosition> {
    auto minTilePositionX = std::numeric_limits<float>::max();
    auto minTilePositionY = std::numeric_limits<float>::max();
    auto minTilePositionZ = std::numeric_limits<float>::max();

    auto maxTilePositionX = std::numeric_limits<float>::lowest();
    auto maxTilePositionY = std::numeric_limits<float>::lowest();
    auto maxTilePositionZ = std::numeric_limits<float>::lowest();

    const auto dataTileNumberX = this->tileConfiguration.GetTileNumberX();
    const auto dataTileNumberY = this->tileConfiguration.GetTileNumberY();

    for (auto tileIndexX = 0; tileIndexX < dataTileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < dataTileNumberY; ++tileIndexY) {
            const auto tilePosition = this->dataTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto tilePositionX = tilePosition.GetTilePositionX();
            const auto tilePositionY = tilePosition.GetTilePositionY();
            const auto tilePositionZ = tilePosition.GetTilePositionZ();

            minTilePositionX = std::min(tilePositionX, minTilePositionX);
            minTilePositionY = std::min(tilePositionY, minTilePositionY);
            minTilePositionZ = std::min(tilePositionZ, minTilePositionZ);

            maxTilePositionX = std::max(tilePositionX, maxTilePositionX);
            maxTilePositionY = std::max(tilePositionY, maxTilePositionY);
            maxTilePositionZ = std::max(tilePositionZ, maxTilePositionZ);
        }
    }

    MinTilePosition minTilePosition;
    minTilePosition.x = static_cast<int32_t>(minTilePositionX);
    minTilePosition.y = static_cast<int32_t>(minTilePositionY);
    minTilePosition.z = static_cast<int32_t>(minTilePositionZ);

    MaxTilePosition maxTilePosition;
    maxTilePosition.x = static_cast<int32_t>(maxTilePositionX);
    maxTilePosition.y = static_cast<int32_t>(maxTilePositionY);
    maxTilePosition.z = static_cast<int32_t>(maxTilePositionZ);

    return { minTilePosition, maxTilePosition };
}

auto StitchingMapGenerator::Impl::GenerateStitchingTileIndexMap() const -> QMap<StitchingTileIndex, StitchingTile> {
    const auto dataTileNumberX = this->tileConfiguration.GetTileNumberX();
    const auto dataTileNumberY = this->tileConfiguration.GetTileNumberY();

    const auto stitchingTileNumberX = 2 * dataTileNumberX - 1;
    const auto stitchingTileNumberY = 2 * dataTileNumberY - 1;

    QMap<StitchingTileIndex, StitchingTile> stitchingTileMap;
    for (auto stitchingTileIndexX = 0; stitchingTileIndexX < stitchingTileNumberX; ++stitchingTileIndexX) {
        for (auto stitchingTileIndexY = 0; stitchingTileIndexY < stitchingTileNumberY; ++stitchingTileIndexY) {
            StitchingTile stitchingTile;
            stitchingTile.SetDataTileNumber(dataTileNumberX, dataTileNumberY);

            if ((stitchingTileIndexX % 2 == 0) && (stitchingTileIndexY % 2 == 0)) {
                const auto dataTileIndexX = stitchingTileIndexX / 2;
                const auto dataTileIndexY = stitchingTileIndexY / 2;

                const auto& [indexRangeToDataTile, indexRangeToStitchingTile] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexX, dataTileIndexY);
                stitchingTile.AddLayer(dataTileIndexX, dataTileIndexY, indexRangeToDataTile, indexRangeToStitchingTile);
            } else if ((stitchingTileIndexX % 2 == 0) && (stitchingTileIndexY % 2 == 1)) {
                const auto dataTileIndexX = stitchingTileIndexX / 2;
                const auto dataTileIndexYUp = (stitchingTileIndexY - 1) / 2;
                const auto dataTileIndexYDown = (stitchingTileIndexY + 1) / 2;

                const auto [indexRangeToDataTileUp, indexRangeToStitchingTileUp] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexX, dataTileIndexYUp);
                const auto [indexRangeToDataTileDown, indexRangeToStitchingTileDown] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexX, dataTileIndexYDown);

                stitchingTile.AddLayer(dataTileIndexX, dataTileIndexYUp,
                    indexRangeToDataTileUp, indexRangeToStitchingTileUp);
                stitchingTile.AddLayer(dataTileIndexX, dataTileIndexYDown,
                    indexRangeToDataTileDown, indexRangeToStitchingTileDown);
            } else if ((stitchingTileIndexX % 2 == 1) && (stitchingTileIndexY % 2 == 0)) {
                const auto dataTileIndexXLeft = (stitchingTileIndexX - 1) / 2;
                const auto dataTileIndexXRight = (stitchingTileIndexX + 1) / 2;
                const auto dataTileIndexY = stitchingTileIndexY / 2;

                const auto& [indexRangeToDataTileLeft, indexRangeToStitchingTileLeft] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXLeft, dataTileIndexY);
                const auto& [indexRangeToDataTileRight, indexRangeToStitchingTileRight] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXRight, dataTileIndexY);

                stitchingTile.AddLayer(dataTileIndexXLeft, dataTileIndexY,
                    indexRangeToDataTileLeft, indexRangeToStitchingTileLeft);
                stitchingTile.AddLayer(dataTileIndexXRight, dataTileIndexY,
                    indexRangeToDataTileRight, indexRangeToStitchingTileRight);
            } else if ((stitchingTileIndexX % 2 == 1) && (stitchingTileIndexY % 2 == 1)) {
                const auto dataTileIndexXLeft = (stitchingTileIndexX - 1) / 2;
                const auto dataTileIndexXRight = (stitchingTileIndexX + 1) / 2;
                const auto dataTileIndexYUp = (stitchingTileIndexY - 1) / 2;
                const auto dataTileIndexYDown = (stitchingTileIndexY + 1) / 2;

                const auto& [indexRangeToDataTileLeftUp, indexRangeToStitchingTileLeftUp] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXLeft, dataTileIndexYUp);
                const auto& [indexRangeToDataTileLeftDown, indexRangeToStitchingTileLeftDown] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXLeft, dataTileIndexYDown);
                const auto& [indexRangeToDataTileRightUp, indexRangeToStitchingTileRightUp] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXRight, dataTileIndexYUp);
                const auto& [indexRangeToDataTileRightDown, indexRangeToStitchingTileRightDown] =
                    this->GetIndexRangeToDataTileAndStitchingTile(stitchingTileIndexX, stitchingTileIndexY,
                        dataTileIndexXRight, dataTileIndexYDown);

                stitchingTile.AddLayer(dataTileIndexXLeft, dataTileIndexYUp,
                    indexRangeToDataTileLeftUp, indexRangeToStitchingTileLeftUp);
                stitchingTile.AddLayer(dataTileIndexXLeft, dataTileIndexYDown,
                    indexRangeToDataTileLeftDown, indexRangeToStitchingTileLeftDown);
                stitchingTile.AddLayer(dataTileIndexXRight, dataTileIndexYUp,
                    indexRangeToDataTileRightUp, indexRangeToStitchingTileRightUp);
                stitchingTile.AddLayer(dataTileIndexXRight, dataTileIndexYDown,
                    indexRangeToDataTileRightDown, indexRangeToStitchingTileRightDown);
            }

            const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * stitchingTileNumberX;
            stitchingTileMap[stitchingTileIndex] = stitchingTile;
        }
    }

    return stitchingTileMap;
}

auto StitchingMapGenerator::Impl::GetIndexRangeToDataTileAndStitchingTile(const int32_t& stitchingTileIndexX,
    const int32_t& stitchingTileIndexY, const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
    -> std::tuple<IndexRange, IndexRange> {

    const auto dataTileNumberX = this->tileConfiguration.GetTileNumberX();

    const auto dataTileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto dataTileSizeY = this->tileConfiguration.GetTileSizeYInPixel();
    const auto dataTileSizeZ = this->tileConfiguration.GetTileSizeZInPixel();

    const auto stitchingTileNumberX = 2 * dataTileNumberX - 1;
    const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * stitchingTileNumberX;

    const auto stitchingTileIndexRange = this->stitchingTileIndexRangeMap[stitchingTileIndex];

    const auto dataTilePosition = this->dataTilePositionSet.GetTilePosition(dataTileIndexX, dataTileIndexY);
    const auto dataTilePositionX = static_cast<int32_t>(dataTilePosition.GetTilePositionX());
    const auto dataTilePositionY = static_cast<int32_t>(dataTilePosition.GetTilePositionY());
    const auto dataTilePositionZ = static_cast<int32_t>(dataTilePosition.GetTilePositionZ());

    IndexRange dataTileIndexRange;
    dataTileIndexRange.SetRange(dataTilePositionX, dataTilePositionX + dataTileSizeX - 1,
        dataTilePositionY, dataTilePositionY + dataTileSizeY - 1,
        dataTilePositionZ, dataTilePositionZ + dataTileSizeZ - 1);

    const auto overlapIndexRange = stitchingTileIndexRange & dataTileIndexRange;

    IndexRange indexRangeToDataTile;
    indexRangeToDataTile.SetRange(
        overlapIndexRange.GetX0() - dataTilePositionX, overlapIndexRange.GetX1() - dataTilePositionX,
        overlapIndexRange.GetY0() - dataTilePositionY, overlapIndexRange.GetY1() - dataTilePositionY,
        overlapIndexRange.GetZ0() - dataTilePositionZ, overlapIndexRange.GetZ1() - dataTilePositionZ);

    IndexRange indexRangeToStitchingTile;
    indexRangeToStitchingTile.SetRange(
        overlapIndexRange.GetX0() - stitchingTileIndexRange.GetX0(), overlapIndexRange.GetX1() - stitchingTileIndexRange.GetX0(),
        overlapIndexRange.GetY0() - stitchingTileIndexRange.GetY0(), overlapIndexRange.GetY1() - stitchingTileIndexRange.GetY0(),
        overlapIndexRange.GetZ0() - stitchingTileIndexRange.GetZ0(), overlapIndexRange.GetZ1() - stitchingTileIndexRange.GetZ0());

    return { indexRangeToDataTile, indexRangeToStitchingTile };
}

auto StitchingMapGenerator::Impl::GetStitchedDataDimension() const -> std::tuple<int32_t, int32_t, int32_t> {
    auto [minTilePosition, maxTilePosition] = this->GetMinMaxTilePosition();

    const auto dataTileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto dataTileSizeY = this->tileConfiguration.GetTileSizeYInPixel();
    const auto dataTileSizeZ = this->tileConfiguration.GetTileSizeZInPixel();

    const auto stitchedDataDimensionX = maxTilePosition.x + dataTileSizeX - minTilePosition.x;
    const auto stitchedDataDimensionY = maxTilePosition.y + dataTileSizeY - minTilePosition.y;
    const auto stitchedDataDimensionZ = maxTilePosition.z + dataTileSizeZ - minTilePosition.z;

    return { stitchedDataDimensionX, stitchedDataDimensionY, stitchedDataDimensionZ };
}

auto StitchingMapGenerator::Impl::GenerateDivisionLine() const -> std::tuple<std::vector<int32_t>, std::vector<int32_t>> {
    const auto dataTileNumberX = this->tileConfiguration.GetTileNumberX();
    const auto dataTileNumberY = this->tileConfiguration.GetTileNumberY();

    const auto dataTileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto dataTileSizeY = this->tileConfiguration.GetTileSizeYInPixel();

    std::vector<int32_t> leftDivisionLinePositionSetX, rightDivisionLinePositionSetX;
    std::vector<int32_t> upDivisionLinePositionSetY, downDivisionLinePositionSetY;

    leftDivisionLinePositionSetX.assign(dataTileNumberX, std::numeric_limits<int32_t>::max());
    rightDivisionLinePositionSetX.assign(dataTileNumberX, std::numeric_limits<int32_t>::min());
    upDivisionLinePositionSetY.assign(dataTileNumberY, std::numeric_limits<int32_t>::max());
    downDivisionLinePositionSetY.assign(dataTileNumberY, std::numeric_limits<int32_t>::min());

    for (auto tileIndexX = 0; tileIndexX < dataTileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < dataTileNumberY; ++tileIndexY) {
            const auto tilePosition = this->dataTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto tilesLeftPositionX = static_cast<int32_t>(std::round(tilePosition.GetTilePositionX()));
            const auto tileUpPositionY = static_cast<int32_t>(std::round(tilePosition.GetTilePositionY()));

            const auto tileRightPositionX = tilesLeftPositionX + dataTileSizeX;
            const auto tileDownPositionY = tileUpPositionY + dataTileSizeY;

            const auto minTilesLeftPositionX = std::min(leftDivisionLinePositionSetX.at(tileIndexX), tilesLeftPositionX);
            const auto maxTilesRightPositionX = std::max(rightDivisionLinePositionSetX.at(tileIndexX), tileRightPositionX);

            leftDivisionLinePositionSetX.at(tileIndexX) = minTilesLeftPositionX;
            rightDivisionLinePositionSetX.at(tileIndexX) = maxTilesRightPositionX;

            const auto minTilesUpPositionY = std::min(upDivisionLinePositionSetY.at(tileIndexY), tileUpPositionY);
            const auto maxTileDownPositionY = std::max(downDivisionLinePositionSetY.at(tileIndexY), tileDownPositionY);

            upDivisionLinePositionSetY.at(tileIndexY) = minTilesUpPositionY;
            downDivisionLinePositionSetY.at(tileIndexY) = maxTileDownPositionY;
        }
    }

    std::vector<int32_t> divisionLinePositionSetX = leftDivisionLinePositionSetX;
    divisionLinePositionSetX.insert(divisionLinePositionSetX.end(), rightDivisionLinePositionSetX.begin(),
        rightDivisionLinePositionSetX.end());
    std::sort(divisionLinePositionSetX.begin(), divisionLinePositionSetX.end());

    std::vector<int32_t> divisionLinePositionSetY = upDivisionLinePositionSetY;
    divisionLinePositionSetY.insert(divisionLinePositionSetY.end(), downDivisionLinePositionSetY.begin(),
        downDivisionLinePositionSetY.end());
    std::sort(divisionLinePositionSetY.begin(), divisionLinePositionSetY.end());

    return { divisionLinePositionSetX, divisionLinePositionSetY };
}

StitchingMapGenerator::StitchingMapGenerator() : d(std::make_unique<Impl>()) {
}

StitchingMapGenerator::~StitchingMapGenerator() = default;

auto StitchingMapGenerator::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto StitchingMapGenerator::SetDataTilePositionSet(const TilePositionSet& dataTilePositionSet) -> void {
    d->dataTilePositionSet = dataTilePositionSet;
}

auto StitchingMapGenerator::GenerateMap() -> bool {
    {
        const auto stitchingTileIndexRangeMap = d->GenerateStitchingTileIndexRangeMap();
        d->stitchingTileIndexRangeMap = stitchingTileIndexRangeMap;
        d->normalizedStitchingTileIndexRangeMap = d->NormalizeStitchingTileIndexRangeMap(stitchingTileIndexRangeMap);

        const auto stitchingTileIndexMap = d->GenerateStitchingTileIndexMap();
        d->stitchingTileIndexMap = stitchingTileIndexMap;
    }
    {
        const auto [stitchedDataSizeX, stitchedDataSizeY, stitchedDataSizeZ] = d->GetStitchedDataDimension();
        d->stitchedDataSizeX = stitchedDataSizeX;
        d->stitchedDataSizeY = stitchedDataSizeY;
        d->stitchedDataSizeZ = stitchedDataSizeZ;
    }
    {
        StitchingCenterPositionCalculator stitchingCenterPositionCalculator;
        stitchingCenterPositionCalculator.SetTileConfiguration(d->tileConfiguration);
        stitchingCenterPositionCalculator.SetTilePositionSet(d->dataTilePositionSet);

        if (!stitchingCenterPositionCalculator.Calculate()) {
            return false;
        }

        const auto centerPositionX = stitchingCenterPositionCalculator.GetCenterPositionX();
        const auto centerPositionY = stitchingCenterPositionCalculator.GetCenterPositionY();

        auto [minTilePosition, maxTilePosition] = d->GetMinMaxTilePosition();

        const auto normalizedCenterPositionX = centerPositionX - minTilePosition.x;
        const auto normalizedCenterPositionY = centerPositionY - minTilePosition.y;

        d->centerPositionX = normalizedCenterPositionX;
        d->centerPositionY = normalizedCenterPositionY;
    }

    const auto dataTileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto dataTileNumberY = d->tileConfiguration.GetTileNumberY();

    const auto stitchingTileNumberX = 2 * dataTileNumberX - 1;
    const auto stitchingTileNumberY = 2 * dataTileNumberY - 1;

    StitchingMap generatedStitchingMap;
    generatedStitchingMap.SetStitchingTileNumber(stitchingTileNumberX, stitchingTileNumberY);
    generatedStitchingMap.SetStitchedDataSize(d->stitchedDataSizeX, d->stitchedDataSizeY, d->stitchedDataSizeZ);
    generatedStitchingMap.SetCenterPosition(d->centerPositionX, d->centerPositionY);

    for (auto iterator = d->stitchingTileIndexMap.begin();
        iterator != d->stitchingTileIndexMap.end(); ++iterator) {
        const auto stitchingTileIndex = iterator.key();
        const auto stitchingTile = iterator.value();

        generatedStitchingMap.SetStitchingTile(stitchingTileIndex, stitchingTile);
    }

    for (auto iterator = d->normalizedStitchingTileIndexRangeMap.begin(); 
        iterator != d->normalizedStitchingTileIndexRangeMap.end(); ++iterator) {
        const auto stitchingTileIndex = iterator.key();
        const auto indexRange = iterator.value();

        generatedStitchingMap.SetIndexRange(stitchingTileIndex, indexRange);
    }
    
    d->stitchingMap = generatedStitchingMap;

    return true;
}

auto StitchingMapGenerator::GetStitchingMap() const -> StitchingMap {
    return d->stitchingMap;
}
