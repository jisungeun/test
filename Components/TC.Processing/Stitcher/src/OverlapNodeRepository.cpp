#include "OverlapNodeRepository.h"

#include <iostream>
#include <QMap>

typedef int32_t OverlapIndex;

class OverlapNodeRepository::Impl {
public:
    Impl() = default;
    ~Impl() = default;
    QMap<OverlapIndex, OverlapNode> overlapNodeMap{};

    int32_t overlapNumberX{};
    int32_t overlapNumberY{};

    TileConfiguration tileConfiguration{};
    OverlapRelationSet overlapRelationSet{};

    int32_t connectedCount{};

    auto GetOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->OverlapNode&;

    static auto IsOddNumber(const int32_t& number)->bool;
    auto IsOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;
    auto CalculateOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->OverlapIndex;

    auto ChangeAllToReadyFlag()->void;

    auto IsBoundaryNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;
    auto IsIslandConnectingNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;

    auto GetAdjacentNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetBridgeNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetUpNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetDownNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetLeftNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetRightNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;

    auto IsInBoundedNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;
    auto GetInBoundedUpNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetInBoundedDownNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetInBoundedLeftNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;
    auto GetInBoundedRightNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->QList<OverlapNode*>;

    static auto FilterNotConnectedNode(const QList<OverlapNode*>& overlapNodePointerList)->QList<OverlapNode*>;
    static auto FilterOpenNode(const QList<OverlapNode*>& overlapNodePointerList)->QList<OverlapNode*>;
    static auto FilterReadyNode(const QList<OverlapNode*>& overlapNodePointerList)->QList<OverlapNode*>;
    
    auto IsConnectable(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;

    auto IsPickedNodeBoundaryReachable(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->bool;

    auto IsBoundaryReachable(const QList<OverlapNode*>& nodePointerList)->bool;
    auto IsBoundaryReachable(OverlapNode& overlapNode)->bool;
};

auto OverlapNodeRepository::Impl::GetAdjacentNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> QList<OverlapNode*> {
    const auto upSide = (overlapIndexY == 0);
    const auto downSide = (overlapIndexY == this->overlapNumberY - 1);
    const auto leftSide = (overlapIndexX == 0);
    const auto rightSide = (overlapIndexX == this->overlapNumberX - 1);

    QList<OverlapNode*> nodePointerList{};

    if (upSide) {
        nodePointerList = this->GetDownNodePointerList(overlapIndexX, overlapIndexY);
    } else if (downSide) {
        nodePointerList = this->GetUpNodePointerList(overlapIndexX, overlapIndexY);
    } else if (leftSide) {
        nodePointerList = this->GetRightNodePointerList(overlapIndexX, overlapIndexY);
    } else if (rightSide) {
        nodePointerList = this->GetLeftNodePointerList(overlapIndexX, overlapIndexY);
    } else {
        const auto isVerticallyOpenedNodePath = this->IsOddNumber(overlapIndexX);

        if (isVerticallyOpenedNodePath) {
            const auto upNodePointerList = this->GetUpNodePointerList(overlapIndexX, overlapIndexY);
            const auto downNodePointerList = this->GetDownNodePointerList(overlapIndexX, overlapIndexY);

            nodePointerList.append(upNodePointerList);
            nodePointerList.append(downNodePointerList);
        } else { // horizontallyOpenedNodePath
            const auto leftNodePointerList = this->GetLeftNodePointerList(overlapIndexX, overlapIndexY);
            const auto rightNodePointerList = this->GetRightNodePointerList(overlapIndexX, overlapIndexY);

            nodePointerList.append(leftNodePointerList);
            nodePointerList.append(rightNodePointerList);
        }
    }

    return nodePointerList;
}

auto OverlapNodeRepository::Impl::GetBridgeNodePointerList(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> QList<OverlapNode*> {
    const auto isHorizontalConnectingNode = this->IsOddNumber(overlapIndexX);

    QList<OverlapNode*> bridgeNodePointerList;
    if (isHorizontalConnectingNode) {
        bridgeNodePointerList.append(this->GetInBoundedLeftNodePointerList(overlapIndexX, overlapIndexY));
        bridgeNodePointerList.append(this->GetInBoundedRightNodePointerList(overlapIndexX, overlapIndexY));
    } else { // verticalConnectingNode 
        bridgeNodePointerList.append(this->GetInBoundedUpNodePointerList(overlapIndexX, overlapIndexY));
        bridgeNodePointerList.append(this->GetInBoundedDownNodePointerList(overlapIndexX, overlapIndexY));
    }

    return bridgeNodePointerList;
}

auto OverlapNodeRepository::Impl::GetUpNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    const auto upNode1OverlapIndexX = overlapIndexX - 1;
    const auto upNode1OverlapIndexY = overlapIndexY - 1;

    const auto upNode2OverlapIndexX = overlapIndexX;
    const auto upNode2OverlapIndexY = overlapIndexY - 2;

    const auto upNode3OverlapIndexX = overlapIndexX + 1;
    const auto upNode3OverlapIndexY = overlapIndexY - 1;

    auto& upNode1 = GetOverlapNode(upNode1OverlapIndexX, upNode1OverlapIndexY);
    auto& upNode2 = GetOverlapNode(upNode2OverlapIndexX, upNode2OverlapIndexY);
    auto& upNode3 = GetOverlapNode(upNode3OverlapIndexX, upNode3OverlapIndexY);

    return { &upNode1, &upNode2, &upNode3 };
}

auto OverlapNodeRepository::Impl::GetDownNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    const auto downNode1OverlapIndexX = overlapIndexX - 1;
    const auto downNode1OverlapIndexY = overlapIndexY + 1;;

    const auto downNode2OverlapIndexX = overlapIndexX;
    const auto downNode2OverlapIndexY = overlapIndexY + 2;

    const auto downNode3OverlapIndexX = overlapIndexX + 1;
    const auto downNode3OverlapIndexY = overlapIndexY + 1;

    auto& downNode1 = GetOverlapNode(downNode1OverlapIndexX, downNode1OverlapIndexY);
    auto& downNode2 = GetOverlapNode(downNode2OverlapIndexX, downNode2OverlapIndexY);
    auto& downNode3 = GetOverlapNode(downNode3OverlapIndexX, downNode3OverlapIndexY);

    return { &downNode1, &downNode2, &downNode3 };
}

auto OverlapNodeRepository::Impl::GetLeftNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    const auto leftNode1OverlapIndexX = overlapIndexX - 1;
    const auto leftNode1OverlapIndexY = overlapIndexY + 1;

    const auto leftNode2OverlapIndexX = overlapIndexX - 2;
    const auto leftNode2OverlapIndexY = overlapIndexY;

    const auto leftNode3OverlapIndexX = overlapIndexX - 1;
    const auto leftNode3OverlapIndexY = overlapIndexY - 1;

    auto& leftNode1 = GetOverlapNode(leftNode1OverlapIndexX, leftNode1OverlapIndexY);
    auto& leftNode2 = GetOverlapNode(leftNode2OverlapIndexX, leftNode2OverlapIndexY);
    auto& leftNode3 = GetOverlapNode(leftNode3OverlapIndexX, leftNode3OverlapIndexY);

    return { &leftNode1, &leftNode2, &leftNode3 };
}

auto OverlapNodeRepository::Impl::GetRightNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    const auto rightNode1OverlapIndexX = overlapIndexX + 1;
    const auto rightNode1OverlapIndexY = overlapIndexY + 1;;

    const auto rightNode2OverlapIndexX = overlapIndexX + 2;
    const auto rightNode2OverlapIndexY = overlapIndexY;

    const auto rightNode3OverlapIndexX = overlapIndexX + 1;
    const auto rightNode3OverlapIndexY = overlapIndexY - 1;

    auto& rightNode1 = GetOverlapNode(rightNode1OverlapIndexX, rightNode1OverlapIndexY);
    auto& rightNode2 = GetOverlapNode(rightNode2OverlapIndexX, rightNode2OverlapIndexY);
    auto& rightNode3 = GetOverlapNode(rightNode3OverlapIndexX, rightNode3OverlapIndexY);

    return { &rightNode1, &rightNode2, &rightNode3 };
}

auto OverlapNodeRepository::Impl::IsInBoundedNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> bool {
    if ((overlapIndexX < 0) || (overlapIndexX >= this->overlapNumberX)) { return false; }
    if ((overlapIndexY < 0) || (overlapIndexY >= this->overlapNumberY)) { return false; }
    return true;
}

auto OverlapNodeRepository::Impl::GetInBoundedUpNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    QList<OverlapNode*> nodePointerList;

    const auto upNode1OverlapIndexX = overlapIndexX - 1;
    const auto upNode1OverlapIndexY = overlapIndexY - 1;
    if (this->IsInBoundedNode(upNode1OverlapIndexX, upNode1OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(upNode1OverlapIndexX, upNode1OverlapIndexY));
    }

    const auto upNode2OverlapIndexX = overlapIndexX;
    const auto upNode2OverlapIndexY = overlapIndexY - 2;
    if (this->IsInBoundedNode(upNode2OverlapIndexX, upNode2OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(upNode2OverlapIndexX, upNode2OverlapIndexY));
    }

    const auto upNode3OverlapIndexX = overlapIndexX + 1;
    const auto upNode3OverlapIndexY = overlapIndexY - 1;
    if (this->IsInBoundedNode(upNode3OverlapIndexX, upNode3OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(upNode3OverlapIndexX, upNode3OverlapIndexY));
    }

    return nodePointerList;
}

auto OverlapNodeRepository::Impl::GetInBoundedDownNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    QList<OverlapNode*> nodePointerList;

    const auto downNode1OverlapIndexX = overlapIndexX - 1;
    const auto downNode1OverlapIndexY = overlapIndexY + 1;;
    if (this->IsInBoundedNode(downNode1OverlapIndexX, downNode1OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(downNode1OverlapIndexX, downNode1OverlapIndexY));
    }

    const auto downNode2OverlapIndexX = overlapIndexX;
    const auto downNode2OverlapIndexY = overlapIndexY + 2;
    if (this->IsInBoundedNode(downNode2OverlapIndexX, downNode2OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(downNode2OverlapIndexX, downNode2OverlapIndexY));
    }

    const auto downNode3OverlapIndexX = overlapIndexX + 1;
    const auto downNode3OverlapIndexY = overlapIndexY + 1;
    if (this->IsInBoundedNode(downNode3OverlapIndexX, downNode3OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(downNode3OverlapIndexX, downNode3OverlapIndexY));
    }

    return nodePointerList;
}

auto OverlapNodeRepository::Impl::GetInBoundedLeftNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    QList<OverlapNode*> nodePointerList;

    const auto leftNode1OverlapIndexX = overlapIndexX - 1;
    const auto leftNode1OverlapIndexY = overlapIndexY + 1;
    if (this->IsInBoundedNode(leftNode1OverlapIndexX, leftNode1OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(leftNode1OverlapIndexX, leftNode1OverlapIndexY));
    }

    const auto leftNode2OverlapIndexX = overlapIndexX - 2;
    const auto leftNode2OverlapIndexY = overlapIndexY;
    if (this->IsInBoundedNode(leftNode2OverlapIndexX, leftNode2OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(leftNode2OverlapIndexX, leftNode2OverlapIndexY));
    }

    const auto leftNode3OverlapIndexX = overlapIndexX - 1;
    const auto leftNode3OverlapIndexY = overlapIndexY - 1;
    if (this->IsInBoundedNode(leftNode3OverlapIndexX, leftNode3OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(leftNode3OverlapIndexX, leftNode3OverlapIndexY));
    }
    return nodePointerList;
}

auto OverlapNodeRepository::Impl::GetInBoundedRightNodePointerList(const int32_t& overlapIndexX,
    const int32_t& overlapIndexY) -> QList<OverlapNode*> {
    QList<OverlapNode*> nodePointerList;

    const auto rightNode1OverlapIndexX = overlapIndexX + 1;
    const auto rightNode1OverlapIndexY = overlapIndexY + 1;
    if (this->IsInBoundedNode(rightNode1OverlapIndexX, rightNode1OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(rightNode1OverlapIndexX, rightNode1OverlapIndexY));
    }

    const auto rightNode2OverlapIndexX = overlapIndexX + 2;
    const auto rightNode2OverlapIndexY = overlapIndexY;
    if (this->IsInBoundedNode(rightNode2OverlapIndexX, rightNode2OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(rightNode2OverlapIndexX, rightNode2OverlapIndexY));
    }

    const auto rightNode3OverlapIndexX = overlapIndexX + 1;
    const auto rightNode3OverlapIndexY = overlapIndexY - 1;
    if (this->IsInBoundedNode(rightNode3OverlapIndexX, rightNode3OverlapIndexY)) {
        nodePointerList.push_back(&GetOverlapNode(rightNode3OverlapIndexX, rightNode3OverlapIndexY));
    }
    return nodePointerList;
}

auto OverlapNodeRepository::Impl::FilterNotConnectedNode(const QList<OverlapNode*>& overlapNodePointerList)
    -> QList<OverlapNode*> {
    QList<OverlapNode*> openNodePointerList{};
    for (const auto& overlapNodePointer : overlapNodePointerList) {
        if (overlapNodePointer->GetStatus() != OverlapNode::Status::Connected) {
            openNodePointerList.push_back(overlapNodePointer);
        }
    }
    return openNodePointerList;
}

auto OverlapNodeRepository::Impl::FilterOpenNode(const QList<OverlapNode*>& overlapNodePointerList)
    -> QList<OverlapNode*> {
    QList<OverlapNode*> openNodePointerList{};
    for (const auto& overlapNodePointer : overlapNodePointerList) {
        if (overlapNodePointer->GetStatus() == OverlapNode::Status::Opened) {
            openNodePointerList.push_back(overlapNodePointer);
        }
    }
    return openNodePointerList;
}

auto OverlapNodeRepository::Impl::FilterReadyNode(const QList<OverlapNode*>& overlapNodePointerList)
    -> QList<OverlapNode*> {
    QList<OverlapNode*> openNodePointerList{};
    for (const auto& overlapNodePointer : overlapNodePointerList) {
        if (overlapNodePointer->GetCheckFlag() == OverlapNode::CheckFlag::Ready) {
            openNodePointerList.push_back(overlapNodePointer);
        }
    }
    return openNodePointerList;
}

auto OverlapNodeRepository::Impl::IsConnectable(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> bool {
    //std::cout << overlapIndexX << ":" << overlapIndexY << ":Picked" << std::endl;
    bool connectable;
    if ((this->overlapNumberY == 1) || (this->overlapNumberX == 1)) {
        connectable = true;
    } else if (this->IsIslandConnectingNode(overlapIndexX, overlapIndexY)) {
        connectable = true;
    } else if (this->IsPickedNodeBoundaryReachable(overlapIndexX, overlapIndexY)) {
        connectable = true;
    } else {
        connectable = false;
    }

    return connectable;
}

auto OverlapNodeRepository::Impl::IsPickedNodeBoundaryReachable(const int32_t& overlapIndexX, 
    const int32_t& overlapIndexY) -> bool {
    bool pickedNodeBoundaryReachable;

    auto& overlapNode = this->GetOverlapNode(overlapIndexX, overlapIndexY);
    if (this->IsBoundaryNode(overlapIndexX, overlapIndexY)) {
        const auto nodePointerList = this->FilterNotConnectedNode(this->GetAdjacentNodePointerList(overlapIndexX, overlapIndexY));

        overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
        const bool boundaryReachable = this->IsBoundaryReachable(nodePointerList);
        this->ChangeAllToReadyFlag();

        pickedNodeBoundaryReachable = boundaryReachable;
    } else {
        const auto isVerticallyOpenedNodePath = this->IsOddNumber(overlapIndexX);

        if (isVerticallyOpenedNodePath) {
            const auto upNodePointerList = this->FilterNotConnectedNode(this->GetUpNodePointerList(overlapIndexX, overlapIndexY));
            const auto downNodePointerList = this->FilterNotConnectedNode(this->GetDownNodePointerList(overlapIndexX, overlapIndexY));

            overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
            const bool upSideBoundaryReachable = this->IsBoundaryReachable(upNodePointerList);
            //std::cout << overlapIndexX << ":" << overlapIndexY - 1 << ":" << (upSideBoundaryReachable ? "Confirmed" : "NConfirmed") << std::endl;
            this->ChangeAllToReadyFlag();

            if (upSideBoundaryReachable) {
                overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
                const bool downSideBoundaryReachable = this->IsBoundaryReachable(downNodePointerList);
                //std::cout << overlapIndexX << ":" << overlapIndexY + 1 << ":" << (downSideBoundaryReachable ? "Confirmed" : "NConfirmed") << std::endl;
                this->ChangeAllToReadyFlag();

                pickedNodeBoundaryReachable = downSideBoundaryReachable;
            } else {
                pickedNodeBoundaryReachable = false;
            }
        } else { // horizontalNodePath
            const auto leftNodePointerList = this->FilterNotConnectedNode(this->GetLeftNodePointerList(overlapIndexX, overlapIndexY));
            const auto rightNodePointerList = this->FilterNotConnectedNode(this->GetRightNodePointerList(overlapIndexX, overlapIndexY));

            overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
            const bool leftSideBoundaryReachable = this->IsBoundaryReachable(leftNodePointerList);
            //std::cout << overlapIndexX - 1 << ":" << overlapIndexY << ":" << (leftSideBoundaryReachable ? "Confirmed" : "NConfirmed") << std::endl;
            this->ChangeAllToReadyFlag();

            if (leftSideBoundaryReachable) {
                overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Checking);
                const bool rightSideBoundaryReachable = this->IsBoundaryReachable(rightNodePointerList);
                //std::cout << overlapIndexX + 1 << ":" << overlapIndexY << ":" << (rightSideBoundaryReachable ? "Confirmed" : "NConfirmed") << std::endl;
                this->ChangeAllToReadyFlag();

                pickedNodeBoundaryReachable = rightSideBoundaryReachable;
            } else {
                pickedNodeBoundaryReachable = false;
            }
        }
    }

    return pickedNodeBoundaryReachable;
}

auto OverlapNodeRepository::Impl::IsBoundaryReachable(const QList<OverlapNode*>& nodePointerList) -> bool {
    if (nodePointerList.isEmpty()) {
        return false;
    }
    for (auto& overlapNode : nodePointerList) {
        overlapNode->SetCheckFlag(OverlapNode::CheckFlag::Checking);
        //std::cout << overlapNode->GetOverlapIndexX() << ":" << overlapNode->GetOverlapIndexY() << ":Checked" << std::endl;
    }

    for (auto& overlapNode : nodePointerList) {
        const auto nodeBoundaryReachable = this->IsBoundaryReachable(*overlapNode);
        if (nodeBoundaryReachable == true) {
            //std::cout << overlapNode->GetOverlapIndexX() << ":" << overlapNode->GetOverlapIndexY() << ":Reachable" << std::endl;
            return true;
        }
    }

    return false;
}

auto OverlapNodeRepository::Impl::IsBoundaryReachable(OverlapNode& overlapNode) -> bool {
    const auto& overlapIndexX = overlapNode.GetOverlapIndexX();
    const auto& overlapIndexY = overlapNode.GetOverlapIndexY();
    ////std::cout << "[" << overlapIndexX << ", " << overlapIndexY << "] is started" << std::endl;

    if (this->IsBoundaryNode(overlapIndexX, overlapIndexY)) {
        //std::cout << overlapIndexX << ":" << overlapIndexY << ":BoundaryNode" << std::endl;
        return true;
    } else {
        const auto adjacentNodePointerList = 
            this->FilterReadyNode(this->FilterNotConnectedNode(this->GetAdjacentNodePointerList(overlapIndexX, overlapIndexY)));

        for (auto& adjacentOverlapNode : adjacentNodePointerList) {
            adjacentOverlapNode->SetCheckFlag(OverlapNode::CheckFlag::Checking);
            //std::cout << adjacentOverlapNode->GetOverlapIndexX() << ":" << adjacentOverlapNode->GetOverlapIndexY() << ":Checked" << std::endl;
        }

        for (auto& adjacentOverlapNode : adjacentNodePointerList) {
            if (IsBoundaryReachable(*adjacentOverlapNode)) {
                //std::cout << adjacentOverlapNode->GetOverlapIndexX() << ":" << adjacentOverlapNode->GetOverlapIndexY() << ":Reachable" << std::endl;
                return true;
            }
        }
    }

    //std::cout << overlapIndexX << ":" << overlapIndexY << ":NotReachable" << std::endl;
    return false;
}

auto OverlapNodeRepository::Impl::GetOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> OverlapNode& {
    const auto overlapIndex = this->CalculateOverlapIndex(overlapIndexX, overlapIndexY);
    return this->overlapNodeMap[overlapIndex];
}

auto OverlapNodeRepository::Impl::IsOddNumber(const int32_t& number) -> bool {
    return (number % 2) == 1;
}

auto OverlapNodeRepository::Impl::IsOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> bool {
    return IsOddNumber(overlapIndexX + overlapIndexY);
}

auto OverlapNodeRepository::Impl::CalculateOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> OverlapIndex {
    return overlapIndexX + (overlapIndexY * this->overlapNumberX);
}

auto OverlapNodeRepository::Impl::ChangeAllToReadyFlag() -> void {
    for (auto& overlapNode : this->overlapNodeMap) {
        overlapNode.SetCheckFlag(OverlapNode::CheckFlag::Ready);
    }
    //std::cout << "-1:-1:ChangeAllToReadyFlag" << std::endl;
}

auto OverlapNodeRepository::Impl::IsBoundaryNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> bool {
    const auto upSide = (overlapIndexY == 0);
    const auto downSide = (overlapIndexY == this->overlapNumberY - 1);
    const auto leftSide = (overlapIndexX == 0);
    const auto rightSide = (overlapIndexX == this->overlapNumberX - 1);

    return upSide || downSide || leftSide || rightSide;
}

auto OverlapNodeRepository::Impl::IsIslandConnectingNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY)
    -> bool {
    const auto bridgeNodePointerList = this->GetBridgeNodePointerList(overlapIndexX, overlapIndexY);
    for (const auto& bridgeNodePointer : bridgeNodePointerList) {
        if (bridgeNodePointer->GetStatus() != OverlapNode::Status::Opened) {
            return false;
        }
    }

    return true;
}

OverlapNodeRepository::OverlapNodeRepository() : d(new Impl()) {
}

OverlapNodeRepository::~OverlapNodeRepository() = default;

auto OverlapNodeRepository::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;

    d->overlapNumberX = 2 * tileConfiguration.GetTileNumberX() - 1;
    d->overlapNumberY = 2 * tileConfiguration.GetTileNumberY() - 1;
}

auto OverlapNodeRepository::SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet) -> void {
    d->overlapRelationSet = overlapRelationSet;
}

auto OverlapNodeRepository::Initialize() -> void {
    for (auto overlapIndexX = 0; overlapIndexX < d->overlapNumberX; ++overlapIndexX) {
        for (auto overlapIndexY = 0; overlapIndexY < d->overlapNumberY; ++overlapIndexY) {
            if (!d->IsOverlapNode(overlapIndexX, overlapIndexY)) { continue; }

            const auto overlapRelation = d->overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);
            const auto& score = overlapRelation.GetReliability();

            OverlapNode overlapNode;
            overlapNode.SetOverlapIndex(overlapIndexX, overlapIndexY);
            overlapNode.SetScore(score);
            overlapNode.SetStatus(OverlapNode::Status::Opened);

            const auto overlapIndex = d->CalculateOverlapIndex(overlapIndexX, overlapIndexY);
            d->overlapNodeMap[overlapIndex] = overlapNode;
        }
    }
    d->connectedCount = 0;
}

auto OverlapNodeRepository::GetOverlapNode(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> OverlapNode& {
    return d->GetOverlapNode(overlapIndexX, overlapIndexY);
}

auto OverlapNodeRepository::IsConnectable(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> bool {
    return d->IsConnectable(overlapIndexX, overlapIndexY);
}

auto OverlapNodeRepository::Connect(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> void {
    auto& overlapNode = d->GetOverlapNode(overlapIndexX, overlapIndexY);
    overlapNode.SetStatus(OverlapNode::Status::Connected);
    d->connectedCount++;
    //std::cout << overlapIndexX << ":" << overlapIndexY << ":1:Connected" << std::endl;
}

auto OverlapNodeRepository::Close(const int32_t& overlapIndexX, const int32_t& overlapIndexY) -> void {
    auto& overlapNode = d->GetOverlapNode(overlapIndexX, overlapIndexY);
    overlapNode.SetStatus(OverlapNode::Status::Closed);
    //std::cout << overlapIndexX << ":" << overlapIndexY << ":0:Closed" << std::endl;
}

auto OverlapNodeRepository::GetMaximumScoreOpenedNode() const -> OverlapNode* {
    float maximumScore = std::numeric_limits<float>::lowest();

    OverlapNode* overlapNodePointer = nullptr;
    for (auto& overlapNode : d->overlapNodeMap) {
        const auto score = overlapNode.GetScore();

        if (maximumScore < score) {
            if (overlapNode.GetStatus() == OverlapNode::Status::Opened) {
                maximumScore = score;
                overlapNodePointer = &overlapNode;
            }
        }
    }

    return overlapNodePointer;
}

auto OverlapNodeRepository::ConnectionCompleted() const -> bool {
    const auto tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto tileNumberY = d->tileConfiguration.GetTileNumberY();
    return d->connectedCount == tileNumberX * tileNumberY - 1;
}

