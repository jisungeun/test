#pragma once

#include <memory>

#include <QList>

#include "IndexRange.h"

class StitchingTile {
public:
    StitchingTile();
    StitchingTile(const StitchingTile& other);
    ~StitchingTile();

    auto operator=(const StitchingTile& other)->StitchingTile&;

    auto SetDataTileNumber(const int32_t& dataTileNumberX, const int32_t& dataTileNumberY)->void;
    auto AddLayer(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY, const IndexRange& indexRangeToDataTile,
        const IndexRange& indexRangeToStitchingTile)->void;

    auto GetIndexRangeToDataTile(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
        ->const IndexRange&;
    auto GetIndexRangeToStitchingTile(const int32_t& dataTileIndexX, const int32_t& dataTileIndexY) const
        ->const IndexRange&;

    auto GetLayerDataTileIndexXYList() const ->QList<std::tuple<int32_t, int32_t>>;
    
private:
    class Impl;
    std::unique_ptr<Impl> d;
};