#pragma once

#include <memory>

class IndexRange {
public:
    IndexRange();
    IndexRange(const IndexRange& other);
    ~IndexRange();

    auto operator=(const IndexRange& other)->IndexRange&;

    auto IsEmpty()const->const bool&;

    auto SetRange(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0, const int32_t& z1)->void;
    auto GetX0() const-> const int32_t&;
    auto GetX1() const-> const int32_t&;
    auto GetY0() const-> const int32_t&;
    auto GetY1() const-> const int32_t&;
    auto GetZ0() const-> const int32_t&;
    auto GetZ1() const-> const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};

auto operator&(const IndexRange& range1, const IndexRange& range2)->IndexRange;
