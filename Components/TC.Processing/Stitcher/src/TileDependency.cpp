#include "TileDependency.h"

class TileDependency::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& impl)->Impl & = default;

    bool isReferencing{ false };

    int32_t referenceTileIndexX{};
    int32_t referenceTileIndexY{};
};

TileDependency::TileDependency() : d(new Impl()) {
}

TileDependency::TileDependency(const TileDependency& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

TileDependency::~TileDependency() = default;

auto TileDependency::operator=(const TileDependency& other) -> TileDependency& {
    *(this->d) = *(other.d);
    return *this;
}

auto TileDependency::IsReferencing() const -> const bool& {
    return d->isReferencing;
}

auto TileDependency::SetReferenceTileIndex(const int32_t& tileIndexX, const int32_t& tileIndexY) -> void {
    d->referenceTileIndexX = tileIndexX;
    d->referenceTileIndexY = tileIndexY;

    d->isReferencing = true;
}

auto TileDependency::GetReferenceTileIndexX() const -> const int32_t& {
    return d->referenceTileIndexX;
}

auto TileDependency::GetReferenceTileIndexY() const -> const int32_t& {
    return d->referenceTileIndexY;
}
