#pragma once

#include <memory>

#include "StitchingMap.h"
#include "TileConfiguration.h"
#include "TilePositionSet.h"

class StitchingMapGenerator {
public:
    StitchingMapGenerator();
    ~StitchingMapGenerator();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void;
    auto SetDataTilePositionSet(const TilePositionSet& dataTilePositionSet)->void;

    auto GenerateMap()->bool;

    auto GetStitchingMap()const->StitchingMap;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};