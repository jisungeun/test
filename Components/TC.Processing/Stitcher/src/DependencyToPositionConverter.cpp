#include "DependencyToPositionConverter.h"

#include <vector>

struct TileIndexXY {
    int32_t x;
    int32_t y;
};

struct RelativeShiftXYZ {
    float x;
    float y;
    float z;
};

struct TilePositionXYZ {
    float x;
    float y;
    float z;
};

struct NonOverlapLengthXY {
    int32_t x;
    int32_t y;
};

class DependencyToPositionConverter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    TileConfiguration tileConfiguration;
    OverlapRelationSet overlapRelationSet;
    TileDependencyRepository tileDependencyRepository;

    std::vector<bool> positionCalculatedFlag{};

    auto AllPositionCalculated()->bool;

    auto GetNonOverlapLength()->NonOverlapLengthXY;
    auto CalculateRelativeShiftXYZ(const TileIndexXY& srcTileIndex, const TileIndexXY& destTileIndex)->RelativeShiftXYZ;
    auto CalculateDestTileStackPosition(const TileIndexXY& srcTileIndexXY, const TileIndexXY& destTileIndexXY,
        const TilePositionSet& tilePositionSet)->TilePositionXYZ;

    auto IsInvertedDirection(const TileIndexXY& srcTileIndex, const TileIndexXY& destTileIndex)->bool;
    auto TilePositionPropagatingCalculationIter(const TileIndexXY& tileIndexXY, TilePositionSet& tilePositionSet)->void;
};

auto DependencyToPositionConverter::Impl::AllPositionCalculated() -> bool {
    for (const auto& positionFlag : this->positionCalculatedFlag) {
        if (!positionFlag) {
            return false;
        }
    }
    return true;
}

auto DependencyToPositionConverter::Impl::GetNonOverlapLength() -> NonOverlapLengthXY {
    const auto tileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto tileSizeY = this->tileConfiguration.GetTileSizeYInPixel();

    const auto overlapLengthX = this->tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapLengthY = this->tileConfiguration.GetOverlapLengthYInPixel();

    const auto nonOverlapLengthX = tileSizeX - overlapLengthX;
    const auto nonOverlapLengthY = tileSizeY - overlapLengthY;

    return NonOverlapLengthXY{ nonOverlapLengthX,nonOverlapLengthY };
}

auto DependencyToPositionConverter::Impl::CalculateRelativeShiftXYZ(const TileIndexXY& srcTileIndex,
    const TileIndexXY& destTileIndex) -> RelativeShiftXYZ {
    const auto& [srcTileIndexX, srcTileIndexY] = srcTileIndex;
    const auto& [destTileIndexX, destTileIndexY] = destTileIndex;

    const auto overlapIndexX = srcTileIndexX + destTileIndexX;
    const auto overlapIndexY = srcTileIndexY + destTileIndexY;

    const auto overlapRelation = this->overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

    RelativeShiftXYZ relativeShiftXYZ;
    if (this->IsInvertedDirection(srcTileIndex, destTileIndex)) {
        relativeShiftXYZ.x = -overlapRelation.GetShiftValueX();
        relativeShiftXYZ.y = -overlapRelation.GetShiftValueY();
        relativeShiftXYZ.z = overlapRelation.GetShiftValueZ();
    } else {
        relativeShiftXYZ.x = overlapRelation.GetShiftValueX();
        relativeShiftXYZ.y = overlapRelation.GetShiftValueY();
        relativeShiftXYZ.z = overlapRelation.GetShiftValueZ();
    }

    return relativeShiftXYZ;
}

auto DependencyToPositionConverter::Impl::CalculateDestTileStackPosition(const TileIndexXY& srcTileIndexXY,
    const TileIndexXY& destTileIndexXY, const TilePositionSet& tilePositionSet) -> TilePositionXYZ {
    const auto nonOverlapLengthXY = this->GetNonOverlapLength();

    const auto& [srcTileIndexX, srcTileIndexY] = srcTileIndexXY;
    const auto& [destTileIndexX, destTileIndexY] = destTileIndexXY;

    const auto upDirection = (srcTileIndexX == destTileIndexX) && (destTileIndexY < srcTileIndexY);
    const auto downDirection = (srcTileIndexX == destTileIndexX) && (destTileIndexY > srcTileIndexY);
    const auto leftDirection = (srcTileIndexX > destTileIndexX) && (destTileIndexY == srcTileIndexY);
    const auto rightDirection = (srcTileIndexX < destTileIndexX) && (destTileIndexY == srcTileIndexY);

    const auto nonOverlapLengthX = static_cast<float>(nonOverlapLengthXY.x);
    const auto nonOverlapLengthY = static_cast<float>(nonOverlapLengthXY.y);

    auto destTilePosition = tilePositionSet.GetTilePosition(destTileIndexX, destTileIndexY);
    const auto& destTilePositionX = destTilePosition.GetTilePositionX();
    const auto& destTilePositionY = destTilePosition.GetTilePositionY();
    const auto& destTilePositionZ = destTilePosition.GetTilePositionZ();

    float tilePositionX{}, tilePositionY{};
    if (upDirection) {
        tilePositionX = destTilePositionX;
        tilePositionY = destTilePositionY + nonOverlapLengthY;
    } else if (downDirection) {
        tilePositionX = destTilePositionX;
        tilePositionY = destTilePositionY - nonOverlapLengthY;
    } else if (leftDirection) {
        tilePositionX = destTilePositionX + nonOverlapLengthX;
        tilePositionY = destTilePositionY;
    } else if (rightDirection) {
        tilePositionX = destTilePositionX - nonOverlapLengthX;
        tilePositionY = destTilePositionY;
    }

    const auto tilePositionZ = destTilePositionZ;
    
    return {tilePositionX, tilePositionY, tilePositionZ};
}

auto DependencyToPositionConverter::Impl::IsInvertedDirection(const TileIndexXY& srcTileIndex,
    const TileIndexXY& destTileIndex) -> bool {
    const auto& [srcTileIndexX, srcTileIndexY] = srcTileIndex;
    const auto& [destTileIndexX, destTileIndexY] = destTileIndex;

    const auto rightDirection = srcTileIndexX < destTileIndexX;
    const auto downDirection = destTileIndexY > srcTileIndexY;

    if (rightDirection || downDirection) {
        return true;
    } else {
        return false;
    }
}

auto DependencyToPositionConverter::Impl::TilePositionPropagatingCalculationIter(const TileIndexXY& tileIndexXY, TilePositionSet& tilePositionSet)
    -> void {
    const auto& tileNumberX = this->tileConfiguration.GetTileNumberX();

    auto& positionCalculatedFlag = this->positionCalculatedFlag;
    auto& tileDependencyRepository = this->tileDependencyRepository;

    const auto& [tileIndexX, tileIndexY] = tileIndexXY;

    const auto& tileDependency = tileDependencyRepository.GetTileDependency({ tileIndexX, tileIndexY });

    if (!tileDependency.IsReferencing()) { return; }

    const auto& referenceTileIndexX = tileDependency.GetReferenceTileIndexX();
    const auto& referenceTileIndexY = tileDependency.GetReferenceTileIndexY();
    const auto referenceTileIndexXY = TileIndexXY{ referenceTileIndexX,referenceTileIndexY };

    const auto tileIndex = tileIndexX + tileIndexY * tileNumberX;
    const auto referenceTileIndex = referenceTileIndexX + referenceTileIndexY * tileNumberX;

    TilePosition tilePosition;


    const auto tilePositionExists = positionCalculatedFlag.at(tileIndex);
    const auto referenceTilePositionExists = positionCalculatedFlag.at(referenceTileIndex);

    if (tilePositionExists) {
        const auto [relativeShiftX, relativeShiftY, relativeShiftZ] =
            this->CalculateRelativeShiftXYZ(referenceTileIndexXY, tileIndexXY);

        const auto [tileStackPositionX, tileStackPositionY, tileStackPositionZ] =
            this->CalculateDestTileStackPosition(referenceTileIndexXY, tileIndexXY, tilePositionSet);

        const auto referenceTilePositionX = tileStackPositionX + relativeShiftX;
        const auto referenceTilePositionY = tileStackPositionY + relativeShiftY;
        //const auto referenceTilePositionZ = tileStackPositionZ + relativeShiftZ;
        constexpr auto referenceTilePositionZ = 0;

        tilePosition.SetPositions(referenceTilePositionX, referenceTilePositionY, referenceTilePositionZ);

        tilePositionSet.InsertTilePosition(referenceTileIndexX, referenceTileIndexY, tilePosition);

        positionCalculatedFlag.at(referenceTileIndex) = true;
    } else if (referenceTilePositionExists) {
        const auto [relativeShiftX, relativeShiftY, relativeShiftZ] =
            this->CalculateRelativeShiftXYZ(tileIndexXY, referenceTileIndexXY);

        const auto [tileStackPositionX, tileStackPositionY, tileStackPositionZ] =
            this->CalculateDestTileStackPosition(tileIndexXY, referenceTileIndexXY, tilePositionSet);

        const auto tilePositionX = tileStackPositionX + relativeShiftX;
        const auto tilePositionY = tileStackPositionY + relativeShiftY;
        //const auto tilePositionZ = tileStackPositionZ + relativeShiftZ;
        constexpr auto tilePositionZ = 0;

        tilePosition.SetPositions(tilePositionX, tilePositionY, tilePositionZ);

        tilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, tilePosition);

        positionCalculatedFlag.at(tileIndex) = true;
    } else {
        TilePositionPropagatingCalculationIter({ referenceTileIndexX, referenceTileIndexY }, tilePositionSet);
    }
}

DependencyToPositionConverter::DependencyToPositionConverter() : d(new Impl()) {
}

DependencyToPositionConverter::~DependencyToPositionConverter() = default;

auto DependencyToPositionConverter::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto DependencyToPositionConverter::SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet) -> void {
    d->overlapRelationSet = overlapRelationSet;
}

auto DependencyToPositionConverter::SetTileDependencyRepository(
    const TileDependencyRepository& tileDependencyRepository) -> void {
    d->tileDependencyRepository = tileDependencyRepository;
}

auto DependencyToPositionConverter::Convert() -> TilePositionSet {
    const auto& tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto& tileNumberY = d->tileConfiguration.GetTileNumberY();
    const auto tileNumber = tileNumberX * tileNumberY;

    d->positionCalculatedFlag.clear();
    d->positionCalculatedFlag.assign(tileNumber, false);

    auto& positionCalculatedFlag = d->positionCalculatedFlag;

    TilePositionSet tilePositionSet;
    tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);


    TilePosition tilePosition;
    tilePosition.SetPositions(0, 0, 0);
    tilePositionSet.InsertTilePosition(0, 0, tilePosition);
    positionCalculatedFlag.at(0) = true;

    while (!d->AllPositionCalculated()) {
        for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
            for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
                const auto& tileDependency = d->tileDependencyRepository.GetTileDependency({ tileIndexX,tileIndexY });
                if (!tileDependency.IsReferencing()) { continue; }

                const auto referenceTileIndex = 
                    tileDependency.GetReferenceTileIndexX() + tileDependency.GetReferenceTileIndexY() * tileNumberX;
                const auto tileIndex = tileIndexX + tileIndexY * tileNumberX;

                if (d->positionCalculatedFlag.at(referenceTileIndex) && d->positionCalculatedFlag.at(tileIndex)) {
                    continue;
                }

                d->TilePositionPropagatingCalculationIter({ tileIndexX, tileIndexY }, tilePositionSet);
            }
        }
    }

    return tilePositionSet;
}
