#include "Tile.h"

struct DataRange {
    int32_t x0{};
    int32_t x1{};
    int32_t y0{};
    int32_t y1{};
    int32_t z0{};
    int32_t z1{};
};

class Tile::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;
    auto operator=(const Impl& other)->Impl& = default;

    int32_t tileSizeX{};
    int32_t tileSizeY{};
    int32_t tileSizeZ{};

    int32_t overlapSizeX{};
    int32_t overlapSizeY{};

    ITileDataGetter::Pointer tileDataGetter{};

    auto ConvertDirectionToRange(const OverlapDirection& overlapDirection)->DataRange;
};

auto Tile::Impl::ConvertDirectionToRange(const OverlapDirection& overlapDirection) -> DataRange {
    int32_t x0{}, x1{}, y0{}, y1{};

    if (overlapDirection == OverlapDirection::Left) {
        x0 = 0;
        x1 = this->overlapSizeX - 1;
        y0 = 0;
        y1 = this->tileSizeY - 1;
    }
    else if (overlapDirection == OverlapDirection::Right){
        x0 = this->tileSizeX - this->overlapSizeX;
        x1 = this->tileSizeX - 1;
        y0 = 0;
        y1 = this->tileSizeY - 1;
    }
    else if (overlapDirection == OverlapDirection::Up){
        x0 = 0;
        x1 = this->tileSizeX - 1;
        y0 = 0;
        y1 = this->overlapSizeY - 1;
    }
    else if (overlapDirection == OverlapDirection::Down){
        x0 = 0;
        x1 = this->tileSizeX - 1;
        y0 = this->tileSizeY - this->overlapSizeY;
        y1 = this->tileSizeY - 1;
    }

    constexpr int32_t z0 = 0;
    const int32_t z1 = this->tileSizeZ - 1;

    return DataRange{ x0,x1,y0,y1,z0,z1 };
}

Tile::Tile() : d(new Impl()) {
}

Tile::Tile(const Tile& other) : d(new Impl()) {
    *(this->d) = *(other.d);
}

Tile::~Tile() = default;

auto Tile::operator=(const Tile& other) -> Tile& {
    *(this->d) = *(other.d);
    return *this;
}

auto Tile::SetTileSize(const int32_t& tileSizeX, const int32_t& tileSizeY, const int32_t& tileSizeZ) -> void {
    d->tileSizeX = tileSizeX;
    d->tileSizeY = tileSizeY;
    d->tileSizeZ = tileSizeZ;
}

auto Tile::GetTileSizeX() const -> const int32_t& {
    return d->tileSizeX;
}

auto Tile::GetTileSizeY() const -> const int32_t& {
    return d->tileSizeY;
}

auto Tile::GetTileSizeZ() const -> const int32_t& {
    return d->tileSizeZ;
}

auto Tile::SetOverlapSize(const int32_t& overlapSizeX, const int32_t& overlapSizeY) -> void {
    d->overlapSizeX = overlapSizeX;
    d->overlapSizeY = overlapSizeY;
}

auto Tile::GetOverlapSizeX() const -> const int32_t& {
    return d->overlapSizeX;
}

auto Tile::GetOverlapSizeY() const -> const int32_t& {
    return d->overlapSizeY;
}

auto Tile::SetTileDataGetter(const ITileDataGetter::Pointer& tileDataGetter) -> void {
    d->tileDataGetter = tileDataGetter;
}

auto Tile::GetData() const -> std::shared_ptr<float[]> {
    return d->tileDataGetter->GetData();
}

auto Tile::GetOverlapData(const OverlapDirection& overlapDirection) const -> std::shared_ptr<float[]> {
    const auto [x0, x1, y0, y1, z0, z1] = d->ConvertDirectionToRange(overlapDirection);
    return d->tileDataGetter->GetData(x0, x1, y0, y1, z0, z1);
}

auto Tile::GetSliceData(const int32_t& zIndex) const -> std::shared_ptr<float[]> {
    return d->tileDataGetter->GetData(0, d->tileSizeX - 1, 0, d->tileSizeY - 1, zIndex, zIndex);
}

auto Tile::GetSliceStackData(const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> {
    return d->tileDataGetter->GetData(0, d->tileSizeX - 1, 0, d->tileSizeY - 1, z0, z1);
}
