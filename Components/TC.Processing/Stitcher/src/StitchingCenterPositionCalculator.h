#pragma once

#include <memory>

#include "TileConfiguration.h"
#include "TilePositionSet.h"

class StitchingCenterPositionCalculator {
public:
    StitchingCenterPositionCalculator();
    ~StitchingCenterPositionCalculator();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void;
    auto SetTilePositionSet(const TilePositionSet& tilePositionSet)->void;

    auto Calculate()->bool;

    auto GetCenterPositionX()const ->const int32_t&;
    auto GetCenterPositionY()const ->const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
