#pragma once

#include <memory>

#include "TileDependency.h"

class TileDependencyRepository {
public:
    struct TileIndexXY {
        int32_t tileIndexX;
        int32_t tileIndexY;
    };

    TileDependencyRepository();
    TileDependencyRepository(const TileDependencyRepository& other);
    ~TileDependencyRepository();

    auto operator=(const TileDependencyRepository& other)->TileDependencyRepository&;

    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void;
    auto Initialize()->void;

    auto SetReference(const TileIndexXY& srcTileIndexXY, const TileIndexXY& destTileIndexXY)->void;

    auto GetTileDependency(const TileIndexXY& tileIndexXY)->TileDependency&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
