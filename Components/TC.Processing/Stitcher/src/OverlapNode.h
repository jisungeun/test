#pragma once

#include <memory>

class OverlapNode {
public:
    enum class Status { Connected, Opened, Closed};
    enum class CheckFlag{ Ready, Checking};

    OverlapNode();
    OverlapNode(const OverlapNode& other);
    ~OverlapNode();

    auto operator=(const OverlapNode& other)->OverlapNode&;

    auto SetOverlapIndex(const int32_t& overlapIndexX, const int32_t& overlapIndexY)->void;
    auto GetOverlapIndexX() const -> const int32_t&;
    auto GetOverlapIndexY() const -> const int32_t&;

    auto SetScore(const float& score)->void;
    auto GetScore() const -> const float&;

    auto SetStatus(const Status& status)->void;
    auto GetStatus() const -> const Status&;

    auto SetCheckFlag(const CheckFlag& checkFlag)->void;
    auto GetCheckFlag()const ->const CheckFlag&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};