#include "IndexRange.h"

class IndexRange::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl & other)->Impl& = default;

    bool empty{ true };

    int32_t x0;
    int32_t x1;
    int32_t y0;
    int32_t y1;
    int32_t z0;
    int32_t z1;
};

IndexRange::IndexRange() : d(new Impl()) {
}

IndexRange::IndexRange(const IndexRange& other) : d(new Impl(*other.d)) {
}

IndexRange::~IndexRange() = default;

auto IndexRange::operator=(const IndexRange& other) -> IndexRange& {
    *(this->d) = *(other.d);
    return *this;
}

auto IndexRange::IsEmpty() const -> const bool& {
    return d->empty;
}

auto IndexRange::SetRange(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
    const int32_t& z0, const int32_t& z1) -> void {
    if ((x0 <= x1) && (y0 <= y1) && (z0 <= z1)) {
        d->empty = false;

        d->x0 = x0;
        d->x1 = x1;
        d->y0 = y0;
        d->y1 = y1;
        d->z0 = z0;
        d->z1 = z1;
    } else {
        d->empty = true;
    }
}

auto IndexRange::GetX0() const -> const int32_t&{
    return d->x0;
}

auto IndexRange::GetX1() const -> const int32_t&{
    return d->x1;
}

auto IndexRange::GetY0() const -> const int32_t&{
    return d->y0;
}

auto IndexRange::GetY1() const -> const int32_t&{
    return d->y1;
}

auto IndexRange::GetZ0() const -> const int32_t&{
    return d->z0;
}

auto IndexRange::GetZ1() const -> const int32_t&{
    return d->z1;
}

struct OverlapRange {
    int32_t start;
    int32_t end;
    bool invalid{ true };
};

auto GetOverlapRange(const int32_t& start0, const int32_t& end0, const int32_t& start1, const int32_t& end1)
    ->OverlapRange {
    int32_t overlapStart{}, overlapEnd{};
    bool invalidFlag{ true };

    if ((start0 > end1) || (end0 < start1)) {
        overlapStart = 0;
        overlapEnd = 0;
        invalidFlag = true;
    } else if ((start0 >= start1) && ((end1 >= start0) && (end1 <= end0))) {
        overlapStart = start0;
        overlapEnd = end1;
        invalidFlag = false;
    } else if ((start0 >= start1) && (end1 >= end0)) {
        overlapStart = start0;
        overlapEnd = end0;
        invalidFlag = false;
    } else if ((start0 <= start1) && (end1 <= end0)) {
        overlapStart = start1;
        overlapEnd = end1;
        invalidFlag = false;
    } else if (((start0 <= start1) && (start1 <= end0)) && (end1 >= end0)) {
        overlapStart = start1;
        overlapEnd = end0;
        invalidFlag = false;
    }

    return { overlapStart,overlapEnd,invalidFlag };
}

auto operator&(const IndexRange& range1, const IndexRange& range2)->IndexRange {
    if (range1.IsEmpty() || range2.IsEmpty()) {
        return IndexRange{};
    }
    const auto [overlapX0, overlapX1, overlapXInvalid] = 
        GetOverlapRange(range1.GetX0(), range1.GetX1(), range2.GetX0(), range2.GetX1());
    const auto [overlapY0, overlapY1, overlapYInvalid] = 
        GetOverlapRange(range1.GetY0(), range1.GetY1(), range2.GetY0(), range2.GetY1());
    const auto [overlapZ0, overlapZ1, overlapZInvalid] = 
        GetOverlapRange(range1.GetZ0(), range1.GetZ1(), range2.GetZ0(), range2.GetZ1());

    if (overlapXInvalid || overlapYInvalid || overlapZInvalid) {
        return IndexRange{};
    }

    IndexRange indexRange;
    indexRange.SetRange(overlapX0, overlapX1, overlapY0, overlapY1, overlapZ0, overlapZ1);

    return indexRange;
}