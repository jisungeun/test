#pragma once

#include <memory>

#include "OverlapRelationSet.h"
#include "TileConfiguration.h"
#include "TileDependencyRepository.h"
#include "TilePositionSet.h"

class DependencyToPositionConverter {
public:
    DependencyToPositionConverter();
    ~DependencyToPositionConverter();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void;
    auto SetOverlapRelationSet(const OverlapRelationSet& overlapRelationSet)->void;
    auto SetTileDependencyRepository(const TileDependencyRepository& tileDependencyRepository)->void;

    auto Convert()->TilePositionSet;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
