#define NOMINMAX

#include "StitchingWriterHDF5HT.h"

#include <cmath>

#include "arrayfire.h"
#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "StitcherUtilities.h"
#include "StitchingMapGenerator.h"

typedef uint16_t HTDataType;
typedef std::shared_ptr<HTDataType[]> Data3D;
typedef std::shared_ptr<HTDataType[]> DataMIP;

struct BlendedData {
    Data3D data3d{};
    DataMIP dataMIP{};
};

struct MinMaxValue {
    float minValue3D = std::numeric_limits<float>::max();
    float maxValue3D = std::numeric_limits<float>::lowest();
    float minValueMIP = std::numeric_limits<float>::max();
    float maxValueMIP = std::numeric_limits<float>::lowest();
};

class StitchingWriterHDF5HT::Impl {
public:
    Impl() = default;
    ~Impl() {
        this->writingH5File.close();
        this->writingDataSet3D.close();
    }

    TileSet tileSet;
    TilePositionSet tilePositionSet;
    TileConfiguration tileConfiguration;

    bool boundaryCropFlag{ false };

    int32_t limitDataSizeX{};
    int32_t limitDataSizeY{};
    bool dataSizeLimited{ false };

    QString hdf5FilePath;

    StitchingWriterResult stitchingWriterResult{};

    StitchingMap stitchingMap;

    H5::H5File writingH5File;
    H5::DataSet writingDataSet3D;
    H5::DataSet writingDataSetMIP;

    af::array blendingMaskPlane;

    MinMaxValue minMaxValue;

    auto GenerateStitchingMap()->StitchingMap;

    static auto CreateChunkPropertyList(const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ)->H5::DSetCreatPropList;
    static auto CreateChunkPropertyList(const int32_t& dataSizeX, const int32_t& dataSizeY)->H5::DSetCreatPropList;
    static auto CalculateMaximumNumberPowerOf2LessThan(const int32_t& targetNumber)->int32_t;
    auto CreateDataSet3D()->H5::DataSet;
    auto CreateDataSetMIP()->H5::DataSet;
    auto CreateBlendingMask()->af::array;

    auto WriteStitchingTile(const int32_t& stitchingTileIndexX, const int32_t& stitchingTileIndexY,
        std::vector<bool>& writtenFlag)->void;
    auto WriteStitchingTileGroup(const int32_t& groupCenterIndexX, const int32_t& groupCenterIndexY,
        std::vector<bool>& writtenFlag)->void;

    auto CalculateBlendedData(const StitchingTile& stitchingTile, const IndexRange& writingIndexRange)->BlendedData;
    auto WriteBlendedData3D(const Data3D& blendedData, const IndexRange& writingRange) const ->void;
    auto WriteBlendedDataMIP(const DataMIP& blendedData, const IndexRange& writingRange) const ->void;

    auto WriteMinMaxValue()->void;
    auto WriteSizes()->void;
    auto WriteTilePosition()->void;
    auto WriteCenterPosition() const ->bool;

    auto WriteDoneFlag()->void;
};

auto StitchingWriterHDF5HT::Impl::GenerateStitchingMap() -> StitchingMap {
    const auto& config = this->tileConfiguration;

    StitchingMapGenerator stitchingMapGenerator;
    stitchingMapGenerator.SetTileConfiguration(config);
    stitchingMapGenerator.SetDataTilePositionSet(this->tilePositionSet);
    stitchingMapGenerator.GenerateMap();

    auto generatedStitchingMap = stitchingMapGenerator.GetStitchingMap();

    if (this->dataSizeLimited == true) {
        generatedStitchingMap = CropBoundary(generatedStitchingMap, TargetSize{this->limitDataSizeX, this->limitDataSizeY});
        return generatedStitchingMap;
    }

    if (this->boundaryCropFlag == true) {
        const auto [left, right, up, down] = GetBoundaryLength(this->tilePositionSet);
        generatedStitchingMap = CropBoundary(generatedStitchingMap, { left,right,up,down });
    }

    return generatedStitchingMap;
}

auto StitchingWriterHDF5HT::Impl::CreateChunkPropertyList(const int32_t& dataSizeX, const int32_t& dataSizeY,
    const int32_t& dataSizeZ) -> H5::DSetCreatPropList {
    hsize_t chunkSizeX = 64;
    hsize_t chunkSizeY = 64;
    hsize_t chunkSizeZ = 64;

    if (dataSizeX < chunkSizeX) {
        chunkSizeX = CalculateMaximumNumberPowerOf2LessThan(dataSizeX);
    }

    if (dataSizeY < chunkSizeY) {
        chunkSizeY = CalculateMaximumNumberPowerOf2LessThan(dataSizeY);
    }

    if (dataSizeZ < chunkSizeZ) {
        chunkSizeZ = CalculateMaximumNumberPowerOf2LessThan(dataSizeZ);
    }
    
    std::shared_ptr<hsize_t> chunkDimension{ new hsize_t[3]() };
    chunkDimension.get()[0] = chunkSizeZ;
    chunkDimension.get()[1] = chunkSizeY;
    chunkDimension.get()[2] = chunkSizeX;


    H5::DSetCreatPropList chunkPropertyList;
    chunkPropertyList.setChunk(3, chunkDimension.get());

    return chunkPropertyList;
}

auto StitchingWriterHDF5HT::Impl::CreateChunkPropertyList(const int32_t& dataSizeX, const int32_t& dataSizeY)
    -> H5::DSetCreatPropList {
    hsize_t chunkSizeX = 128;
    hsize_t chunkSizeY = 128;

    if (dataSizeX < chunkSizeX) {
        chunkSizeX = CalculateMaximumNumberPowerOf2LessThan(dataSizeX);
    }

    if (dataSizeY < chunkSizeY) {
        chunkSizeY = CalculateMaximumNumberPowerOf2LessThan(dataSizeY);
    }

    std::shared_ptr<hsize_t> chunkDimension{ new hsize_t[2]() };
    chunkDimension.get()[0] = chunkSizeY;
    chunkDimension.get()[1] = chunkSizeX;


    H5::DSetCreatPropList chunkPropertyList;
    chunkPropertyList.setChunk(2, chunkDimension.get());

    return chunkPropertyList;
}

auto StitchingWriterHDF5HT::Impl::CalculateMaximumNumberPowerOf2LessThan(const int32_t& targetNumber) -> int32_t {
    const auto power = std::floor(std::log(targetNumber) / std::log(2));
    return static_cast<int32_t>(std::pow(2, power));
}

auto StitchingWriterHDF5HT::Impl::CreateDataSet3D() -> H5::DataSet {
    const auto writingDataDimensionX = static_cast<hsize_t>(this->stitchingMap.GetStitchedDataSizeX());
    const auto writingDataDimensionY = static_cast<hsize_t>(this->stitchingMap.GetStitchedDataSizeY());
    const auto writingDataDimensionZ = static_cast<hsize_t>(this->stitchingMap.GetStitchedDataSizeZ());

    const auto rank = 3;
    const hsize_t dataSpaceDimension[rank] = { writingDataDimensionZ, writingDataDimensionY, writingDataDimensionX };
    
    const auto dataType = H5::PredType::NATIVE_USHORT;
    const auto dataSpace = H5::DataSpace{ rank, dataSpaceDimension };
    const auto chunkPropertyList = this->CreateChunkPropertyList(writingDataDimensionX, writingDataDimensionY, writingDataDimensionZ);

    auto writingDataSet = this->writingH5File.createDataSet("StitchingData3D", dataType, dataSpace, chunkPropertyList);

    return writingDataSet;
}

auto StitchingWriterHDF5HT::Impl::CreateDataSetMIP() -> H5::DataSet {
    const auto writingDataDimensionX = static_cast<hsize_t>(this->stitchingMap.GetStitchedDataSizeX());
    const auto writingDataDimensionY = static_cast<hsize_t>(this->stitchingMap.GetStitchedDataSizeY());

    const auto rank = 2;
    const hsize_t dataSpaceDimension[rank] = { writingDataDimensionY, writingDataDimensionX };

    const auto dataType = H5::PredType::NATIVE_USHORT;
    const auto dataSpace = H5::DataSpace{ rank, dataSpaceDimension };
    const auto chunkPropertyList = this->CreateChunkPropertyList(writingDataDimensionX, writingDataDimensionY);

    auto writingDataSet = this->writingH5File.createDataSet("StitchingDataMIP", dataType, dataSpace, chunkPropertyList);

    return writingDataSet;
}

auto StitchingWriterHDF5HT::Impl::CreateBlendingMask() -> af::array {
    const auto tileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto tileSizeY = this->tileConfiguration.GetTileSizeYInPixel();

    af::array blendingMask = af::constant(1, tileSizeX, tileSizeY, f32);

    const auto blendingLengthX = this->tileConfiguration.GetOverlapLengthXInPixel();
    const auto blendingLengthY = this->tileConfiguration.GetOverlapLengthYInPixel();

    if (blendingLengthX > 1) {
        const auto gradient = 1.f / static_cast<float>(blendingLengthX);

        af::array slopeLineX = af::seq(1, blendingLengthX);
        slopeLineX = slopeLineX * gradient;

        auto blendingLineX = af::constant(0, 1, blendingLengthX);
        blendingLineX(0, af::seq(0, af::end)) = slopeLineX;

        const auto blendingPlaneX = af::tile(blendingLineX, tileSizeY, 1);

        const auto startSlopeArea = af::seq(0, blendingLengthX - 1);
        const auto endSlopeArea = af::seq(tileSizeX - blendingLengthX, tileSizeX - 1);

        blendingMask(af::span, startSlopeArea) = blendingMask(af::span, startSlopeArea) * blendingPlaneX;
        blendingMask(af::span, endSlopeArea) = blendingMask(af::span, endSlopeArea) * af::flip(blendingPlaneX, 1);
    }

    if (blendingLengthY > 1) {
        const auto gradient = 1.f / static_cast<float>(blendingLengthY);

        af::array slopeLineY = af::seq(1, blendingLengthY);
        slopeLineY = slopeLineY * gradient;

        auto blendingLineY = af::constant(0, blendingLengthY, 1);
        blendingLineY(af::seq(0, af::end), 0) = slopeLineY;

        const auto blendingPlaneY = af::tile(blendingLineY, 1, tileSizeX);

        const auto startSlopeArea = af::seq(0, blendingLengthY - 1);
        const auto endSlopeArea = af::seq(tileSizeY - blendingLengthY, tileSizeY - 1);

        blendingMask(startSlopeArea, af::span) = blendingMask(startSlopeArea, af::span) * blendingPlaneY;
        blendingMask(endSlopeArea, af::span) = blendingMask(endSlopeArea, af::span) * af::flip(blendingPlaneY, 0);
    }

    af::deviceGC();
    return blendingMask;
}

auto StitchingWriterHDF5HT::Impl::WriteStitchingTile(const int32_t& stitchingTileIndexX,
    const int32_t& stitchingTileIndexY, std::vector<bool>& writtenFlag) -> void {
    const auto stitchedTileNumberX = 2 * this->tileConfiguration.GetTileNumberX() - 1;

    const auto stitchingTileIndex = stitchingTileIndexX + stitchingTileIndexY * stitchedTileNumberX;
    if (writtenFlag.at(stitchingTileIndex)) { return; }

    const auto stitchingTile = this->stitchingMap.GetStitchingTile(stitchingTileIndexX, stitchingTileIndexY);
    const auto writingIndexRange = this->stitchingMap.GetIndexRange(stitchingTileIndexX, stitchingTileIndexY);
    const auto blendedData = this->CalculateBlendedData(stitchingTile, writingIndexRange);

    this->WriteBlendedData3D(blendedData.data3d, writingIndexRange);
    this->WriteBlendedDataMIP(blendedData.dataMIP, writingIndexRange);
    writtenFlag.at(stitchingTileIndex) = true;
}

auto StitchingWriterHDF5HT::Impl::WriteStitchingTileGroup(const int32_t& groupCenterIndexX,
    const int32_t& groupCenterIndexY, std::vector<bool>& writtenFlag) -> void {
    constexpr int32_t diffX[3] = { -1,0,1 };
    constexpr int32_t diffY[3] = { -1,0,1 };

    for (const auto& diffIndexX : diffX) {
        for (const auto& diffIndexY : diffY) {
            const auto stitchingTileIndexX = groupCenterIndexX + diffIndexX;
            const auto stitchingTileIndexY = groupCenterIndexY + diffIndexY;

            this->WriteStitchingTile(stitchingTileIndexX, stitchingTileIndexY, writtenFlag);
        }
    }
}

auto StitchingWriterHDF5HT::Impl::CalculateBlendedData(const StitchingTile& stitchingTile, 
    const IndexRange& writingIndexRange)-> BlendedData {
    const auto writingDataDimensionX = writingIndexRange.GetX1() - writingIndexRange.GetX0() + 1;
    const auto writingDataDimensionY = writingIndexRange.GetY1() - writingIndexRange.GetY0() + 1;
    const auto writingDataDimensionZ = writingIndexRange.GetZ1() - writingIndexRange.GetZ0() + 1;
    const auto writingDataLength3D = writingDataDimensionX * writingDataDimensionY * writingDataDimensionZ;
    const auto writingDataLengthMIP = writingDataDimensionX * writingDataDimensionY;
    
    const auto dataTileIndexXYList = stitchingTile.GetLayerDataTileIndexXYList();

    Data3D blendedHostData3D{ new HTDataType[writingDataLength3D]() };
    DataMIP blendedHostDataMIP{ new HTDataType[writingDataLengthMIP]() };

    if (dataTileIndexXYList.size() == 1) {
        const auto& [tileIndexX, tileIndexY] = dataTileIndexXYList.first();

        const auto& indexRangeToDataTile = stitchingTile.GetIndexRangeToDataTile(tileIndexX, tileIndexY);
        const auto& indexRangeToStitchingTile = stitchingTile.GetIndexRangeToStitchingTile(tileIndexX, tileIndexY);

        const auto dataTileIndexX0 = indexRangeToDataTile.GetX0();
        const auto dataTileIndexX1 = indexRangeToDataTile.GetX1();

        const auto dataTileIndexY0 = indexRangeToDataTile.GetY0();
        const auto dataTileIndexY1 = indexRangeToDataTile.GetY1();

        const auto dataTileIndexZ0 = indexRangeToDataTile.GetZ0();
        const auto dataTileIndexZ1 = indexRangeToDataTile.GetZ1();

        const auto dataTile = this->tileSet.GetTile(tileIndexX, tileIndexY);
        const auto tileSizeX = dataTile.GetTileSizeX();
        const auto tileSizeY = dataTile.GetTileSizeY();
        const auto tileSizeZ = dataTile.GetTileSizeZ();

        const auto indexRangeX = af::seq(dataTileIndexX0, dataTileIndexX1);
        const auto indexRangeY = af::seq(dataTileIndexY0, dataTileIndexY1);
        const auto indexRangeZ = af::seq(dataTileIndexZ0, dataTileIndexZ1);

        const auto stitchingTileIndexX0 = indexRangeToStitchingTile.GetX0();
        const auto stitchingTileIndexX1 = indexRangeToStitchingTile.GetX1();

        const auto stitchingTileIndexY0 = indexRangeToStitchingTile.GetY0();
        const auto stitchingTileIndexY1 = indexRangeToStitchingTile.GetY1();

        const auto stitchingTileIndexZ0 = indexRangeToStitchingTile.GetZ0();
        const auto stitchingTileIndexZ1 = indexRangeToStitchingTile.GetZ1();

        const auto stitchingTileIndexRangeX = af::seq(stitchingTileIndexX0, stitchingTileIndexX1);
        const auto stitchingTileIndexRangeY = af::seq(stitchingTileIndexY0, stitchingTileIndexY1);
        const auto stitchingTileIndexRangeZ = af::seq(stitchingTileIndexZ0, stitchingTileIndexZ1);

        const auto dataTileMemory = dataTile.GetData();
        af::array dataTileArray{ tileSizeY, tileSizeX, tileSizeZ, dataTileMemory.get() };
        auto dataTileCropArray = dataTileArray(indexRangeY, indexRangeX, indexRangeZ).copy();
        this->minMaxValue.minValue3D = std::min(af::min<float>(dataTileArray), this->minMaxValue.minValue3D);
        this->minMaxValue.maxValue3D = std::max(af::max<float>(dataTileArray), this->minMaxValue.maxValue3D);

        auto dataTileArrayMIP = af::max(dataTileArray, 2);
        this->minMaxValue.minValueMIP = std::min(af::min<float>(dataTileArrayMIP), this->minMaxValue.minValueMIP);
        this->minMaxValue.maxValueMIP = std::max(af::max<float>(dataTileArrayMIP), this->minMaxValue.maxValueMIP);

        af::array writingData{ writingDataDimensionY, writingDataDimensionX, writingDataDimensionZ, f32 };
        writingData(stitchingTileIndexRangeY, stitchingTileIndexRangeX, stitchingTileIndexRangeZ) = dataTileCropArray;

        writingData = reorder(writingData, 1, 0, 2);
        writingData = writingData * 10000;
        writingData = af::round(writingData).as(u16);
        writingData.eval();
        writingData.host(blendedHostData3D.get());

        auto writingDataMIP = af::max(writingData, 2);
        writingDataMIP.eval();
        writingDataMIP.host(blendedHostDataMIP.get());

    } else {
        af::array blendedData = af::constant(0, writingDataDimensionY, writingDataDimensionX, writingDataDimensionZ, f32);
        af::array blendingMask3D = af::constant(0, writingDataDimensionY, writingDataDimensionX, writingDataDimensionZ, f32);
        af::array dataMask3D = af::constant(0, writingDataDimensionY, writingDataDimensionX, writingDataDimensionZ, b8);

        for (const auto dataTileIndexXY : dataTileIndexXYList) {
            const auto& [tileIndexX, tileIndexY] = dataTileIndexXY;

            const auto dataTile = this->tileSet.GetTile(tileIndexX, tileIndexY);
            const auto tileSizeX = dataTile.GetTileSizeX();
            const auto tileSizeY = dataTile.GetTileSizeY();
            const auto tileSizeZ = dataTile.GetTileSizeZ();

            const auto dataTileMemory = dataTile.GetData();
            af::array dataTileArray{ tileSizeY, tileSizeX, tileSizeZ, dataTileMemory.get() };

            const auto& indexRangeToDataTile = stitchingTile.GetIndexRangeToDataTile(tileIndexX, tileIndexY);
            const auto& indexRangeToStitchingTile = stitchingTile.GetIndexRangeToStitchingTile(tileIndexX, tileIndexY);
            const auto dataTileIndexX0 = indexRangeToDataTile.GetX0();
            const auto dataTileIndexX1 = indexRangeToDataTile.GetX1();

            const auto dataTileIndexY0 = indexRangeToDataTile.GetY0();
            const auto dataTileIndexY1 = indexRangeToDataTile.GetY1();

            const auto dataTileIndexZ0 = indexRangeToDataTile.GetZ0();
            const auto dataTileIndexZ1 = indexRangeToDataTile.GetZ1();

            const auto dataTileIndexRangeX = af::seq(dataTileIndexX0, dataTileIndexX1);
            const auto dataTileIndexRangeY = af::seq(dataTileIndexY0, dataTileIndexY1);
            const auto dataTileIndexRangeZ = af::seq(dataTileIndexZ0, dataTileIndexZ1);

            const auto stitchingTileIndexX0 = indexRangeToStitchingTile.GetX0();
            const auto stitchingTileIndexX1 = indexRangeToStitchingTile.GetX1();

            const auto stitchingTileIndexY0 = indexRangeToStitchingTile.GetY0();
            const auto stitchingTileIndexY1 = indexRangeToStitchingTile.GetY1();

            const auto stitchingTileIndexZ0 = indexRangeToStitchingTile.GetZ0();
            const auto stitchingTileIndexZ1 = indexRangeToStitchingTile.GetZ1();
            
            const auto stitchingTileIndexRangeX = af::seq(stitchingTileIndexX0, stitchingTileIndexX1);
            const auto stitchingTileIndexRangeY = af::seq(stitchingTileIndexY0, stitchingTileIndexY1);
            const auto stitchingTileIndexRangeZ = af::seq(stitchingTileIndexZ0, stitchingTileIndexZ1);

            af::array cropBlendingMaskPlane = this->blendingMaskPlane(dataTileIndexRangeY, dataTileIndexRangeX).copy();
            const auto cropBlendingMask3D = af::tile(cropBlendingMaskPlane, 1, 1, tileSizeZ);

            auto dataTileCropArray = 
                dataTileArray(dataTileIndexRangeY, dataTileIndexRangeX, dataTileIndexRangeZ).copy();
            auto blendedCropArray = dataTileCropArray * cropBlendingMask3D;

            blendedData(stitchingTileIndexRangeY, stitchingTileIndexRangeX, stitchingTileIndexRangeZ) += 
                blendedCropArray;
            blendingMask3D(stitchingTileIndexRangeY, stitchingTileIndexRangeX, stitchingTileIndexRangeZ) += 
                cropBlendingMask3D;
            dataMask3D(stitchingTileIndexRangeY, stitchingTileIndexRangeX, stitchingTileIndexRangeZ) = true;

            blendedData.eval();
            blendingMask3D.eval();
            dataMask3D.eval();
            af::deviceGC();
        }

        blendingMask3D(blendingMask3D == 0) = 1;
        blendingMask3D.eval();

        blendedData = blendedData / blendingMask3D;

        this->minMaxValue.minValue3D = std::min(af::min<float>(blendedData(dataMask3D == 1)), this->minMaxValue.minValue3D);
        this->minMaxValue.maxValue3D = std::max(af::max<float>(blendedData(dataMask3D == 1)), this->minMaxValue.maxValue3D);

        auto blendedDataMIP = af::max(blendedData, 2);
        auto dataMask2D = af::max(dataMask3D, 2);
        this->minMaxValue.minValueMIP = std::min(af::min<float>(blendedDataMIP(dataMask2D == 1)), this->minMaxValue.minValueMIP);
        this->minMaxValue.maxValueMIP = std::max(af::max<float>(blendedDataMIP(dataMask2D == 1)), this->minMaxValue.maxValueMIP);

        blendedData = reorder(blendedData, 1, 0, 2);
        blendedData = blendedData * 10000;
        blendedData = af::round(blendedData).as(u16);
        blendedData.eval();
        blendedData.host(blendedHostData3D.get());

        blendedDataMIP = reorder(blendedDataMIP, 1, 0);
        blendedDataMIP = blendedDataMIP * 10000;
        blendedDataMIP = af::round(blendedDataMIP).as(u16);
        blendedDataMIP.eval();
        blendedDataMIP.host(blendedHostDataMIP.get());
    }

    af::deviceGC();
    return { blendedHostData3D, blendedHostDataMIP };
}

auto StitchingWriterHDF5HT::Impl::WriteBlendedData3D(const Data3D& blendedData, const IndexRange& writingRange) const
    -> void {
    const auto dataPositionX = writingRange.GetX0();
    const auto dataPositionY = writingRange.GetY0();
    const auto dataPositionZ = writingRange.GetZ0();

    const auto dataSizeX = writingRange.GetX1() - writingRange.GetX0() + 1;
    const auto dataSizeY = writingRange.GetY1() - writingRange.GetY0() + 1;
    const auto dataSizeZ = writingRange.GetZ1() - writingRange.GetZ0() + 1;

    const auto dataType = H5::PredType::NATIVE_USHORT;

    constexpr hsize_t rank = 3;
    const hsize_t count[rank] = {
        static_cast<hsize_t>(dataSizeZ),
        static_cast<hsize_t>(dataSizeY),
        static_cast<hsize_t>(dataSizeX) };

    const hsize_t offset[rank] = {
        static_cast<hsize_t>(dataPositionZ),
        static_cast<hsize_t>(dataPositionY),
        static_cast<hsize_t>(dataPositionX) };

    const auto selectedSpace = this->writingDataSet3D.getSpace();
    selectedSpace.selectHyperslab(H5S_SELECT_SET, count, offset);

    const auto writingSpace = H5::DataSpace{ rank, count };

    this->writingDataSet3D.write(blendedData.get(), dataType, writingSpace, selectedSpace);
}

auto StitchingWriterHDF5HT::Impl::WriteBlendedDataMIP(const DataMIP& blendedData, const IndexRange& writingRange)
    const -> void {
    const auto dataPositionX = writingRange.GetX0();
    const auto dataPositionY = writingRange.GetY0();

    const auto dataSizeX = writingRange.GetX1() - writingRange.GetX0() + 1;
    const auto dataSizeY = writingRange.GetY1() - writingRange.GetY0() + 1;

    const auto dataType = H5::PredType::NATIVE_USHORT;

    constexpr hsize_t rank = 2;
    const hsize_t count[rank] = {
        static_cast<hsize_t>(dataSizeY),
        static_cast<hsize_t>(dataSizeX) };

    const hsize_t offset[rank] = {
        static_cast<hsize_t>(dataPositionY),
        static_cast<hsize_t>(dataPositionX) };

    const auto selectedSpace = this->writingDataSetMIP.getSpace();
    selectedSpace.selectHyperslab(H5S_SELECT_SET, count, offset);

    const auto writingSpace = H5::DataSpace{ rank, count };

    this->writingDataSetMIP.write(blendedData.get(), dataType, writingSpace, selectedSpace);
}

auto StitchingWriterHDF5HT::Impl::WriteMinMaxValue() -> void {
    const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);
    {
        const auto minValue3D = this->minMaxValue.minValue3D;
        const auto maxValue3D = this->minMaxValue.maxValue3D;

        const auto attMaxValue = this->writingDataSet3D.createAttribute("minValue", H5::PredType::NATIVE_FLOAT, attributeDataSpace);
        const auto attMinValue = this->writingDataSet3D.createAttribute("maxValue", H5::PredType::NATIVE_FLOAT, attributeDataSpace);

        attMaxValue.write(H5::PredType::NATIVE_FLOAT, &minValue3D);
        attMinValue.write(H5::PredType::NATIVE_FLOAT, &maxValue3D);
    }
    {
        const auto minValueMIP = this->minMaxValue.minValueMIP;
        const auto maxValueMIP = this->minMaxValue.maxValueMIP;

        const auto attMaxValue = this->writingDataSetMIP.createAttribute("minValue", H5::PredType::NATIVE_FLOAT, attributeDataSpace);
        const auto attMinValue = this->writingDataSetMIP.createAttribute("maxValue", H5::PredType::NATIVE_FLOAT, attributeDataSpace);

        attMaxValue.write(H5::PredType::NATIVE_FLOAT, &minValueMIP);
        attMinValue.write(H5::PredType::NATIVE_FLOAT, &maxValueMIP);
    }
}

auto StitchingWriterHDF5HT::Impl::WriteSizes() -> void {
    const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);
    
    const auto writingDataDimensionX = static_cast<int32_t>(this->stitchingMap.GetStitchedDataSizeX());
    const auto writingDataDimensionY = static_cast<int32_t>(this->stitchingMap.GetStitchedDataSizeY());
    const auto writingDataDimensionZ = static_cast<int32_t>(this->stitchingMap.GetStitchedDataSizeZ());
    {
        const auto attDataSizeX = this->writingDataSet3D.createAttribute("dataSizeX", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attDataSizeY = this->writingDataSet3D.createAttribute("dataSizeY", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attDataSizeZ = this->writingDataSet3D.createAttribute("dataSizeZ", H5::PredType::NATIVE_INT32, attributeDataSpace);

        attDataSizeX.write(H5::PredType::NATIVE_INT32, &writingDataDimensionX);
        attDataSizeY.write(H5::PredType::NATIVE_INT32, &writingDataDimensionY);
        attDataSizeZ.write(H5::PredType::NATIVE_INT32, &writingDataDimensionZ);
    }
    {
        const auto attDataSizeX = this->writingDataSetMIP.createAttribute("dataSizeX", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attDataSizeY = this->writingDataSetMIP.createAttribute("dataSizeY", H5::PredType::NATIVE_INT32, attributeDataSpace);

        attDataSizeX.write(H5::PredType::NATIVE_INT32, &writingDataDimensionX);
        attDataSizeY.write(H5::PredType::NATIVE_INT32, &writingDataDimensionY);
    }
}

auto StitchingWriterHDF5HT::Impl::WriteTilePosition() -> void {
    const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);
    {
        const auto tileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
        const auto tileSizeY = this->tileConfiguration.GetTileSizeYInPixel();
        const auto tileSizeZ = this->tileConfiguration.GetTileSizeZInPixel();

        const auto attTileSizeX = this->writingH5File.createAttribute("tileSizeX", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attTileSizeY = this->writingH5File.createAttribute("tileSizeY", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attTileSizeZ = this->writingH5File.createAttribute("tileSizeZ", H5::PredType::NATIVE_INT32, attributeDataSpace);

        attTileSizeX.write(H5::PredType::NATIVE_INT32, &tileSizeX);
        attTileSizeY.write(H5::PredType::NATIVE_INT32, &tileSizeY);
        attTileSizeZ.write(H5::PredType::NATIVE_INT32, &tileSizeZ);
    }

    const auto tileNumberX = this->tileConfiguration.GetTileNumberX();
    const auto tileNumberY = this->tileConfiguration.GetTileNumberY();

    int32_t minTilePositionX = std::numeric_limits<int32_t>::max();
    int32_t minTilePositionY = std::numeric_limits<int32_t>::max();
    for (auto indexX = 0; indexX < tileNumberX; ++indexX) {
        for (auto indexY = 0; indexY < tileNumberY; ++indexY) {
            const auto tilePosition = this->tilePositionSet.GetTilePosition(indexX, indexY);
            const auto tilePositionX = static_cast<int32_t>(tilePosition.GetTilePositionX());
            const auto tilePositionY = static_cast<int32_t>(tilePosition.GetTilePositionY());

            minTilePositionX = std::min(tilePositionX, minTilePositionX);
            minTilePositionY = std::min(tilePositionY, minTilePositionY);
        }
    }

    for (auto indexX = 0; indexX < tileNumberX; ++indexX) {
        for (auto indexY = 0; indexY < tileNumberY; ++indexY) {
            const auto tilePosition = this->tilePositionSet.GetTilePosition(indexX, indexY);
            const auto tilePositionX = static_cast<int32_t>(tilePosition.GetTilePositionX()) - minTilePositionX;
            const auto tilePositionY = static_cast<int32_t>(tilePosition.GetTilePositionY()) - minTilePositionY;

            const auto attributeNameX = QString("tilePositionX_x%1_y%2").arg(indexX).arg(indexY).toStdString();
            const auto attributeNameY = QString("tilePositionY_x%1_y%2").arg(indexX).arg(indexY).toStdString();

            const auto attTilePositionX = this->writingH5File.createAttribute(attributeNameX, H5::PredType::NATIVE_INT32, attributeDataSpace);
            const auto attTilePositionY = this->writingH5File.createAttribute(attributeNameY, H5::PredType::NATIVE_INT32, attributeDataSpace);

            attTilePositionX.write(H5::PredType::NATIVE_INT32, &tilePositionX);
            attTilePositionY.write(H5::PredType::NATIVE_INT32, &tilePositionY);
        }
    }
}

auto StitchingWriterHDF5HT::Impl::WriteCenterPosition() const -> bool {
    const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);
    const auto attCenterPositionX = this->writingH5File.createAttribute("centerPositionX", H5::PredType::NATIVE_INT32, attributeDataSpace);
    const auto attCenterPositionY = this->writingH5File.createAttribute("centerPositionY", H5::PredType::NATIVE_INT32, attributeDataSpace);

    const auto centerPositionX = this->stitchingMap.GetCenterPositionX();
    const auto centerPositionY = this->stitchingMap.GetCenterPositionY();

    attCenterPositionX.write(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.write(H5::PredType::NATIVE_INT32, &centerPositionY);

    return true;
}

auto StitchingWriterHDF5HT::Impl::WriteDoneFlag() -> void {
    const auto attDone = this->writingH5File.createAttribute("done", H5::PredType::NATIVE_INT, H5S_SCALAR);
    constexpr auto doneFlag = 1;
    attDone.write(H5::PredType::NATIVE_INT, &doneFlag);
}

StitchingWriterHDF5HT::StitchingWriterHDF5HT() : d(new Impl()) {
}

StitchingWriterHDF5HT::~StitchingWriterHDF5HT() = default;

auto StitchingWriterHDF5HT::SetTileSet(const TileSet& tileSet) -> void {
    d->tileSet = tileSet;
}

auto StitchingWriterHDF5HT::SetTilePositionSet(const TilePositionSet& tilePositionSet) -> void {
    d->tilePositionSet = tilePositionSet;
}

auto StitchingWriterHDF5HT::SetTileConfiguration(const TileConfiguration& tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto StitchingWriterHDF5HT::SetBoundaryCropFlag(const bool& flag) -> void {
    d->boundaryCropFlag = flag;
}

auto StitchingWriterHDF5HT::SetDataSizeLimit(const int32_t& sizeX, const int32_t& sizeY) -> void {
    d->limitDataSizeX = sizeX;
    d->limitDataSizeY = sizeY;
    d->dataSizeLimited = true;
}

auto StitchingWriterHDF5HT::SetHDF5FilePath(const QString& hdf5FilePath) -> void {
    d->hdf5FilePath = hdf5FilePath;
}

auto StitchingWriterHDF5HT::Run() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    d->stitchingMap = d->GenerateStitchingMap();

    d->writingH5File = H5::H5File(d->hdf5FilePath.toStdString(), H5F_ACC_TRUNC);
    d->writingDataSet3D = d->CreateDataSet3D();
    d->writingDataSetMIP = d->CreateDataSetMIP();

    d->blendingMaskPlane = d->CreateBlendingMask();
    
    const auto stitchedTileNumberX = 2 * d->tileConfiguration.GetTileNumberX() - 1;
    const auto stitchedTileNumberY = 2 * d->tileConfiguration.GetTileNumberY() - 1;
    const auto stitchedTileNumber = stitchedTileNumberX * stitchedTileNumberY;

    std::vector<bool> stitchingTileWrittenFlag;
    stitchingTileWrittenFlag.assign(stitchedTileNumber, false);

    if ((stitchedTileNumberX > 2) && (stitchedTileNumberY > 2)) {
        for (auto stitchedTileIndexX = 0; stitchedTileIndexX < stitchedTileNumberX; ++stitchedTileIndexX) {
            for (auto stitchedTileIndexY = 0; stitchedTileIndexY < stitchedTileNumberY; ++stitchedTileIndexY) {
                if ((stitchedTileIndexX % 2 == 0) || (stitchedTileIndexY % 2 == 0)) { continue; }
                const auto& groupCenterTileIndexX = stitchedTileIndexX;
                const auto& groupCenterTileIndexY = stitchedTileIndexY;
                d->WriteStitchingTileGroup(groupCenterTileIndexX, groupCenterTileIndexY, stitchingTileWrittenFlag);
            }
        }
    } else {
        for (auto stitchedTileIndexX = 0; stitchedTileIndexX < stitchedTileNumberX; ++stitchedTileIndexX) {
            for (auto stitchedTileIndexY = 0; stitchedTileIndexY < stitchedTileNumberY; ++stitchedTileIndexY) {
                d->WriteStitchingTile(stitchedTileIndexX, stitchedTileIndexY, stitchingTileWrittenFlag);
            }
        }
    }

    d->WriteMinMaxValue();
    d->WriteSizes();
    d->WriteTilePosition();
    if (!d->WriteCenterPosition()) {
        return false;
    }
    d->WriteDoneFlag();

    d->writingDataSet3D.close();
    d->writingDataSetMIP.close();
    d->writingH5File.close();

    StitchingWriterResult stitchingWriterResult;
    stitchingWriterResult.SetSuccessFlag(true);
    d->stitchingWriterResult = stitchingWriterResult;

    return true;
}

auto StitchingWriterHDF5HT::GetStitchingWriterResult() const -> const StitchingWriterResult& {
    return d->stitchingWriterResult;
}
