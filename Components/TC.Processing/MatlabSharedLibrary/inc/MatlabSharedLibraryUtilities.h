#pragma once

#include <QString>

#include "MatlabCppSharedLib.hpp"
#include "TCMatlabSharedLibraryExport.h"

namespace TC::MatlabSharedLibrary {
    enum class StringConversionType { CharArray, String };

    auto TCMatlabSharedLibrary_API ToMatlabInput(const QString& string, const StringConversionType& conversionType = StringConversionType::CharArray)->matlab::data::Array;
    auto TCMatlabSharedLibrary_API ToMatlabInput(const double& value)->matlab::data::Array;
    auto TCMatlabSharedLibrary_API ToMatlabInput(const int64_t& value)->matlab::data::Array;
    auto TCMatlabSharedLibrary_API ToMatlabInput(const bool& value)->matlab::data::Array;
}