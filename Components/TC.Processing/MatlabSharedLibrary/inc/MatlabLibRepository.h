#pragma once
#include <memory>

#include <QList>

#include "MatlabCppSharedLib.hpp"
#include "TCMatlabSharedLibraryExport.h"

namespace TC::MatlabSharedLibrary {
    namespace mc = matlab::cpplib;
    namespace md = matlab::data;

    class TCMatlabSharedLibrary_API MatlabLibRepository {
    public:
        typedef MatlabLibRepository Self;
        typedef std::shared_ptr<Self> Pointer;

        ~MatlabLibRepository();
        static auto GetInstance()->Pointer;

        auto IsMatlabAppInitialized()->bool;

        auto InitializeLibrary(const std::u16string& libraryFilePath)->bool;
        auto IsLibraryInitialized(const std::u16string& libraryFilePath)->bool;

        auto GetInitializedLibraryList() const ->const QList<std::u16string>&;
        auto GetLibrary(const std::u16string& libraryFilePath) const ->mc::MATLABLibrary*;

        auto ClearAllLibraries()->void;
    protected:
        MatlabLibRepository();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    auto TCMatlabSharedLibrary_API GetMatlabLibrary(const std::u16string& libraryFilePath)->mc::MATLABLibrary*;
}