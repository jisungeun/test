#include <catch2/catch.hpp>

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"

using namespace TC::MatlabSharedLibrary;

namespace MatlabLibRepositoryTest {
    const QString testFolderPath = TEST_DATA_FOLDR_PATH;

    const std::u16string libFolderPath = testFolderPath.toStdU16String();
    const std::u16string lib1 = libFolderPath + u"/Add.ctf";
    const std::u16string lib2 = libFolderPath + u"/Multiply.ctf";
    const std::u16string lib3 = libFolderPath + u"/Subtract.ctf";
    const std::u16string wrongLib = libFolderPath + u"/wrong.ctf";

    TEST_CASE("MatlabLibRepository") {
        SECTION("GetInstance()") {
            auto matlabLibRepository1 = MatlabLibRepository::GetInstance();
            auto matlabLibRepository2 = MatlabLibRepository::GetInstance();
            CHECK(matlabLibRepository1 == matlabLibRepository2);
        }
        SECTION("IsMatlabAppInitialized()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            CHECK(matlabLibRepository->IsMatlabAppInitialized() == true);
        }
        SECTION("InitializeLibrary()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            matlabLibRepository->ClearAllLibraries();
            SECTION("existing library") {
                CHECK(matlabLibRepository->InitializeLibrary(lib1) == true);
            }
            SECTION("wrong library") {
                CHECK(matlabLibRepository->InitializeLibrary(wrongLib) == false);
            }
        }
        SECTION("IsLibraryInitialized()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            matlabLibRepository->ClearAllLibraries();
            CHECK(matlabLibRepository->IsLibraryInitialized(lib1) == false);
            matlabLibRepository->InitializeLibrary(lib1);
            CHECK(matlabLibRepository->IsLibraryInitialized(lib1) == true);
        }
        SECTION("GetInitializedLibraryList()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            matlabLibRepository->ClearAllLibraries();
            matlabLibRepository->InitializeLibrary(lib1);
            matlabLibRepository->InitializeLibrary(lib2);
            matlabLibRepository->InitializeLibrary(lib3);

            const auto libraryList = matlabLibRepository->GetInitializedLibraryList();
            CHECK(libraryList.size() == 3);
            CHECK(libraryList[0] == lib1);
            CHECK(libraryList[1] == lib2);
            CHECK(libraryList[2] == lib3);
        }
        SECTION("GetLibrary()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            matlabLibRepository->ClearAllLibraries();
            matlabLibRepository->InitializeLibrary(lib1);
            const auto library = matlabLibRepository->GetLibrary(lib1);
            CHECK(library != nullptr);
        }
        SECTION("ClearAllLibraries()") {
            auto matlabLibRepository = MatlabLibRepository::GetInstance();
            matlabLibRepository->ClearAllLibraries();
            matlabLibRepository->InitializeLibrary(lib1);
            matlabLibRepository->InitializeLibrary(lib2);
            matlabLibRepository->InitializeLibrary(lib3);
            CHECK(!matlabLibRepository->GetInitializedLibraryList().isEmpty());

            matlabLibRepository->ClearAllLibraries();
            CHECK(matlabLibRepository->GetInitializedLibraryList().isEmpty());
        }
    }

    TEST_CASE("using matlab struct input") {
        const std::u16string libraryFilePath = u"D:/temp/Add.ctf";
        const auto matlabLibrary = GetMatlabLibrary(libraryFilePath);

        constexpr double aValue = 1;
        constexpr double bValue = 2;

        const matlab::data::ArrayDimensions structDimension{ 1,1 };
       
        matlab::data::ArrayFactory factory;
        auto structArray = factory.createStructArray(structDimension, { "a","b" });
        structArray[0]["a"] = ToMatlabInput(aValue);
        structArray[0]["b"] = ToMatlabInput(bValue);

        const auto inputStruct = static_cast<matlab::data::Array>(structArray);

        const std::vector inputs{inputStruct};
        constexpr auto numberOfOutputs = 1;

        const auto addResultArray = matlabLibrary->feval(u"Add", numberOfOutputs, inputs);

        auto resultArray = static_cast<matlab::data::TypedArray<double>>(addResultArray[0]);

        const auto resultValue = resultArray[0];

        std::cout << "addResult " << aValue << " + " << bValue << " = " << resultValue << std::endl;

    }

    //TEST_CASE("MatlabLibRepository Multiple Library working test") {
    //    for (auto j = 0; j < 10; ++j) {
    //        const auto t1 = std::chrono::system_clock::now();
    //        auto matlabLibRepository = MatlabLibRepository::GetInstance();
    //        const auto t2 = std::chrono::system_clock::now();
    //        matlabLibRepository->ClearAllLibraries();
    //        const auto t3 = std::chrono::system_clock::now();
    //        matlabLibRepository->InitializeLibrary(lib1);
    //        const auto t4 = std::chrono::system_clock::now();
    //        matlabLibRepository->InitializeLibrary(lib2);
    //        const auto t5 = std::chrono::system_clock::now();
    //        matlabLibRepository->InitializeLibrary(lib3);
    //        const auto t6 = std::chrono::system_clock::now();

    //        const auto matlabInitializationMicroSec = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
    //        const auto clearMicroSec = std::chrono::duration_cast<std::chrono::microseconds>(t3 - t2);
    //        const auto lib1InitializationMicroSec = std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3);
    //        const auto lib2InitializationMicroSec = std::chrono::duration_cast<std::chrono::microseconds>(t5 - t4);
    //        const auto lib3InitializationMicroSec = std::chrono::duration_cast<std::chrono::microseconds>(t6 - t5);

    //        std::cout << "matlab Initialization : " << static_cast<float>(matlabInitializationMicroSec.count()) / 1000.f << "msec" << std::endl;
    //        std::cout << "clear library : " << static_cast<float>(clearMicroSec.count()) / 1000.f << "msec" << std::endl;
    //        std::cout << "lib1 Initialization : " << static_cast<float>(lib1InitializationMicroSec.count()) / 1000.f << "msec" << std::endl;
    //        std::cout << "lib2 Initialization : " << static_cast<float>(lib2InitializationMicroSec.count()) / 1000.f << "msec" << std::endl;
    //        std::cout << "lib3 Initialization : " << static_cast<float>(lib3InitializationMicroSec.count()) / 1000.f << "msec" << std::endl;

    //        for (auto i = 0; i < 10; ++i) {
    //            const auto startTime = std::chrono::system_clock::now();

    //            // Get matlab library
    //            auto addLib = matlabLibRepository->GetLibrary(lib1);
    //            const auto multiplyLib = matlabLibRepository->GetLibrary(lib2);
    //            const auto subtractLib = matlabLibRepository->GetLibrary(lib3);

    //            // Input Settings
    //            constexpr auto inputBufferSize = 1;
    //            const md::ArrayDimensions inputDimension{ inputBufferSize };
    //            md::ArrayFactory factory;
    //            auto aBuffer = factory.createBuffer<float>(inputBufferSize);
    //            auto bBuffer = factory.createBuffer<float>(inputBufferSize);
    //            aBuffer.get()[0] = 1;
    //            bBuffer.get()[0] = 2;

    //            auto a = static_cast<md::Array>(factory.createArrayFromBuffer(inputDimension, std::move(aBuffer)));
    //            auto b = static_cast<md::Array>(factory.createArrayFromBuffer(inputDimension, std::move(bBuffer)));

    //            const std::vector inputs{ a, b };

    //            // Run matlab library
    //            constexpr auto numberOfOutputs = 1;
    //            const auto addResultArray = addLib->feval(u"Add", numberOfOutputs, inputs);
    //            const auto multiplyResultArray = multiplyLib->feval(u"Multiply", numberOfOutputs, inputs);
    //            const auto subtractResultArray = subtractLib->feval(u"Subtract", numberOfOutputs, inputs);

    //            // Get Result
    //            const auto addResult = static_cast<md::TypedArray<float>>(addResultArray[0]);
    //            const auto multiplyResult = static_cast<md::TypedArray<float>>(multiplyResultArray[0]);
    //            const auto subtractResult = static_cast<md::TypedArray<float>>(subtractResultArray[0]);

    //            const auto addResultDimension = addResult.getDimensions();
    //            const auto multiplyDimension = multiplyResult.getDimensions();
    //            const auto subtractDimension = subtractResult.getDimensions();

    //            const auto addResultValue = addResult[0];
    //            const auto multiplyResultValue = multiplyResult[0];
    //            const auto subtractResultValue = subtractResult[0];

    //            CHECK(addResultValue == 3);
    //            CHECK(multiplyResultValue == 2);
    //            CHECK(subtractResultValue == -1);

    //            const auto endTime = std::chrono::system_clock::now();

    //            const auto microsec = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);


    //            std::cout << i << " : " << static_cast<float>(microsec.count()) / 1000.f << " msec" << std::endl;
    //        }
    //    }
    //}
}