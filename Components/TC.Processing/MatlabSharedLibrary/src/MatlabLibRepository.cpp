#include <QFile>

#include "MatlabLibRepository.h"
namespace TC::MatlabSharedLibrary {
    struct MatlabLibRepository::Impl {
        Impl() = default;
        ~Impl() = default;

        bool matlabAppInitialized{ false };

        QList<std::u16string> matlabLibraryFilePathList;

        std::vector<std::unique_ptr<mc::MATLABLibrary>> matlabLibraryVector;
        std::shared_ptr<mc::MATLABApplication> matlabApplication{ nullptr };

        auto InitializeMatlabApp()->bool;
        static auto InitializeMatlabApplication()->std::shared_ptr<mc::MATLABApplication>;
        auto InitializeLibrary(const std::u16string& libraryFilePath)->std::unique_ptr<mc::MATLABLibrary>;
    };

    auto MatlabLibRepository::Impl::InitializeMatlabApp() -> bool {
        this->matlabApplication = this->InitializeMatlabApplication();
        if (this->matlabApplication == nullptr) {
            return false;
        } else {
            return true;
        }
    }

    auto MatlabLibRepository::Impl::InitializeMatlabApplication() -> std::shared_ptr<mc::MATLABApplication> {
        constexpr auto mode = mc::MATLABApplicationMode::IN_PROCESS;
        std::vector<std::u16string> options = { u"-nojvm" };
        auto matlabApplication = mc::initMATLABApplication(mode, options);
        return matlabApplication;
    }

    auto MatlabLibRepository::Impl::InitializeLibrary(const std::u16string& libraryFilePath)
        -> std::unique_ptr<mc::MATLABLibrary> {
        if (!QFile::exists(QString::fromStdU16String(libraryFilePath))) {
            return nullptr;
        } else {
            return initMATLABLibrary(this->matlabApplication, libraryFilePath);
        }
    }

    MatlabLibRepository::MatlabLibRepository()
        : d(new Impl()) {
        if (d->matlabAppInitialized == false) {
            d->matlabAppInitialized = d->InitializeMatlabApp();
        }
    }

    auto GetMatlabLibrary(const std::u16string& libraryFilePath) -> mc::MATLABLibrary* {
        const auto matlabRepository = MatlabLibRepository::GetInstance();
        const auto initializationResult = matlabRepository->IsLibraryInitialized(libraryFilePath);
        if (initializationResult == true) {
            return matlabRepository->GetLibrary(libraryFilePath);
        } else {
            const auto initializationResultFlag = matlabRepository->InitializeLibrary(libraryFilePath);
            if (initializationResultFlag) {
                return matlabRepository->GetLibrary(libraryFilePath);
            } else {
                return nullptr;
            }
        }
    }

    MatlabLibRepository::~MatlabLibRepository() = default;

    auto MatlabLibRepository::GetInstance() -> Pointer {
        static Pointer instance(new MatlabLibRepository);
        return instance;
    }

    auto MatlabLibRepository::IsMatlabAppInitialized() -> bool {
        return d->matlabAppInitialized;
    }

    auto MatlabLibRepository::InitializeLibrary(const std::u16string& libraryFilePath) -> bool {
        if (d->matlabLibraryFilePathList.contains(libraryFilePath)) {
            return true;
        } else {
            auto library = d->InitializeLibrary(libraryFilePath);
            if (library == nullptr) {
                return false;
            } else {
                d->matlabLibraryFilePathList.push_back(libraryFilePath);
                d->matlabLibraryVector.emplace_back(std::move(library));
            }
        }
        return true;
    }

    auto MatlabLibRepository::IsLibraryInitialized(const std::u16string& libraryFilePath) -> bool {
        return d->matlabLibraryFilePathList.contains(libraryFilePath);
    }

    auto MatlabLibRepository::GetInitializedLibraryList() const -> const QList<std::u16string>& {
        return d->matlabLibraryFilePathList;
    }

    auto MatlabLibRepository::GetLibrary(const std::u16string& libraryFilePath) const -> mc::MATLABLibrary* {
        if (d->matlabLibraryFilePathList.contains(libraryFilePath)) {
            return d->matlabLibraryVector[d->matlabLibraryFilePathList.indexOf(libraryFilePath)].get();
        } else {
            return nullptr;
        }
    }

    auto MatlabLibRepository::ClearAllLibraries() -> void {
        d->matlabLibraryFilePathList.clear();
        d->matlabLibraryVector.clear();
    }
}
