#include "MatlabSharedLibraryUtilities.h"

namespace TC::MatlabSharedLibrary {
    auto ToMatlabInput(const QString& string, const StringConversionType& conversionType) -> matlab::data::Array {
        matlab::data::ArrayFactory factory;

        if (conversionType == StringConversionType::String) {
            const auto convertedString = string.toStdU16String();
            return static_cast<matlab::data::Array>(factory.createScalar(convertedString));
        }
        return static_cast<matlab::data::Array>(factory.createCharArray(string.toStdString()));
    }

    auto ToMatlabInput(const double& value) -> matlab::data::Array {
        matlab::data::ArrayFactory factory;
        return static_cast<matlab::data::Array>(factory.createScalar(value));
    }

    auto ToMatlabInput(const int64_t& value) -> matlab::data::Array {
        matlab::data::ArrayFactory factory;
        return static_cast<matlab::data::Array>(factory.createScalar(value));
    }

    auto ToMatlabInput(const bool& value) -> matlab::data::Array {
        matlab::data::ArrayFactory factory;
        return static_cast<matlab::data::Array>(factory.createScalar(value));
    }
}