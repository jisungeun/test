//TODO Write PsfNamerTest

//#include <catch2/catch.hpp>
//
//#include "PsfNamer.h"
//
//using namespace TC::HTProcessingMatlab;
//
//namespace PsfNamerTest {
//    TEST_CASE("PsfNamer") {
//        SECTION("PsfNamer()") {
//            PsfNamer psfNamer;
//            CHECK(&psfNamer != nullptr);
//        }
//        SECTION("SetPsfSize()") {
//            PsfNamer psfNamer;
//            psfNamer.SetPsfSize(1, 1, 1);
//            CHECK(&psfNamer != nullptr);
//        }
//        SECTION("SetAcquisitionInfo()") {
//            PsfNamer psfNamer;
//            psfNamer.SetAcquisitionInfo(AcquisitionInfo{});
//            CHECK(&psfNamer != nullptr);
//        }
//        SECTION("GetName()") {
//            constexpr auto sizeX = 1;
//            constexpr auto sizeY = 2;
//            constexpr auto sizeZ = 3;
//
//            AcquisitionInfo acquisitionInfo;
//            acquisitionInfo.mediumRi = 4.44444;
//            acquisitionInfo.condenserNA = 5.55555;
//            acquisitionInfo.objectiveNA = 6.66666;
//            acquisitionInfo.pixelSizeOfImageSensor = 7.77777;
//            acquisitionInfo.magnificationOfCamera = 8.88888;
//            acquisitionInfo.zStepLengthInMicrometer = 9.99999;
//
//            PsfNamer psfNamer;
//            psfNamer.SetPsfSize(sizeX, sizeY, sizeZ);
//            psfNamer.SetAcquisitionInfo(acquisitionInfo);
//            psfNamer.SetOptions(true, SFCMethod::PSFThreshold);
//
//            const QString answer = "PSF_HTX_1x2x3_4.444_NAc5.56_NAo6.67_7.8pxSz_mag9_zStep10.000um_PoPver2_OnlyReal_1_psfThreshold";
//            const auto result = psfNamer.GetName();
//
//            CHECK(answer == result);
//        }
//    }
//}