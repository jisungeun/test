#define LOGGER_TAG "[PSFHandler_v1_4_1_c]"

#include "PSFHandler_v1_4_1_c.h"

#include <QFile>
#include "TCLogger.h"

#include "PSFNamer_v1_4_1_c.h"
#include "PSFBuilder_v1_4_1_c.h"
#include "PSFReader_v1_4_1_c.h"

namespace TC::PSFMatlab {
    constexpr auto maximumCount = 3;
    using PSFNamer = PSFNamer_v1_4_1_c;
    using PSFBuilder = PSFBuilder_v1_4_1_c;
    using PSFReader = PSFReader_v1_4_1_c;
    using PSFBuildingParameters = PSFBuildingParameters_v1_4_1_c;
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    struct PSF {
        int32_t imagRealCount{};
        int32_t patternCount{};
        int32_t sizeX{};
        int32_t sizeY{};
        int32_t sizeZ{};

        std::shared_ptr<float[]> psfData{};
    };

    struct Support {
        int32_t sizeX{};
        int32_t sizeY{};
        int32_t sizeZ{};

        std::shared_ptr<float[]> supportData{};
    };

    struct KResXYZ {
        double x{};
        double y{};
        double z{};
    };

    class PSFHandler_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString psfFolderPath{};
        QString latestPSFFilePath{};
        QString psfModuleFilePath{};

        PSFBuildingParameters psfBuildingParameters{};
        Profile psfProfile{};

        QMap<QString, std::tuple<PSF, Support, KResXYZ>> psfSupportMap{};
        
        auto GeneratePsfFilePath() const ->QString;

    };

    auto PSFHandler_v1_4_1_c::Impl::GeneratePsfFilePath() const -> QString {
        PSFNamer psfNamer;
        psfNamer.SetPSFBuildingParameters(this->psfBuildingParameters);
        psfNamer.SetPSFProfile(this->psfProfile);

        const auto psfFileName = psfNamer.GetName();
        const auto psfFilePath = QString("%1/%2.mat").arg(this->psfFolderPath).arg(psfFileName);

        return psfFilePath;
    }

    PSFHandler_v1_4_1_c::~PSFHandler_v1_4_1_c() = default;

    auto PSFHandler_v1_4_1_c::GetInstance() -> std::shared_ptr<PSFHandler_v1_4_1_c> {
        static auto instance = std::shared_ptr<PSFHandler_v1_4_1_c>{ new PSFHandler_v1_4_1_c };
        return instance;
    }

    auto PSFHandler_v1_4_1_c::SetPSFFolderPath(const QString& psfFolderPath) -> void {
        d->psfFolderPath = psfFolderPath;
    }

    auto PSFHandler_v1_4_1_c::SetPSFModuleFilePath(const QString& psfModuleFilePath) -> void {
        d->psfModuleFilePath = psfModuleFilePath;
    }

    auto PSFHandler_v1_4_1_c::SetPSFBuildingParameters(const PSFBuildingParameters& psfBuildingParameters) -> void {
        d->psfBuildingParameters = psfBuildingParameters;
    }

    auto PSFHandler_v1_4_1_c::SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile) -> void {
        d->psfProfile = psfProfile;
    }

    auto PSFHandler_v1_4_1_c::Update() -> bool {
        const auto psfFilePath = d->GeneratePsfFilePath();

        if (d->psfSupportMap.contains(psfFilePath)) {
            QLOG_INFO() << "PSF Already Loaded";
            d->latestPSFFilePath = psfFilePath;
        } else {
            d->psfSupportMap.remove(psfFilePath);

            PSF resultPSF;
            Support resultSupport;
            KResXYZ kResXYZ;
            if (QFile::exists(psfFilePath)) {
                QLOG_INFO() << "Reading PSF";
                
                PSFReader psfReader;
                psfReader.SetPSFModuleFilePath(d->psfModuleFilePath);
                psfReader.SetPSFFilePath(psfFilePath);

                if (!psfReader.Read()) {
                    QLOG_ERROR() << "psfReader.Read() in PSFHandler_v1_4_1_c::Update()";
                    return false;
                }

                {
                    resultPSF.psfData = psfReader.GetPsfData();
                    std::tie(resultPSF.imagRealCount, resultPSF.patternCount, 
                        resultPSF.sizeY, resultPSF.sizeX, resultPSF.sizeZ) = psfReader.GetPsfSize();
                }
                {
                    resultSupport.supportData = psfReader.GetSupportData();
                    std::tie(resultSupport.sizeY, resultSupport.sizeX, resultSupport.sizeZ) = 
                        psfReader.GetSupportSize();
                }
                {
                    const auto kResXYZData = psfReader.GetKResXYZData();
                    std::tie(kResXYZ.x, kResXYZ.y, kResXYZ.z) = kResXYZData;
                }
            } else {
                QLOG_INFO() << "Generating PSF & Support";

                PSFBuilder psfBuilder;
                psfBuilder.SetPSFModuleFilePath(d->psfModuleFilePath);
                psfBuilder.SetPSFFilePath(psfFilePath);
                psfBuilder.SetPSFBuildingParameters(d->psfBuildingParameters);
                psfBuilder.SetPSFProfile(d->psfProfile);

                if (!psfBuilder.Build()) {
                    QLOG_ERROR() << "psfBuilder.Build() in PSFHandler_v1_4_1_c::Update()";
                    return false;
                }
                {
                    resultPSF.psfData = psfBuilder.GetPsfData();
                    std::tie(resultPSF.imagRealCount, resultPSF.patternCount,
                        resultPSF.sizeY, resultPSF.sizeX, resultPSF.sizeZ) = psfBuilder.GetPsfSize();
                }
                {
                    resultSupport.supportData = psfBuilder.GetSupportData();
                    std::tie(resultSupport.sizeY, resultSupport.sizeX, resultSupport.sizeZ) = 
                        psfBuilder.GetSupportSize();
                }
                {
                    const auto kResXYZData = psfBuilder.GetKResXYZData();
                    std::tie(kResXYZ.x, kResXYZ.y, kResXYZ.z) = kResXYZData;
                }
            }

            if (d->psfSupportMap.count() >= maximumCount) {
                d->psfSupportMap.remove(d->psfSupportMap.begin().key());
            }

            d->psfSupportMap[psfFilePath] = { resultPSF, resultSupport, kResXYZ };
            d->latestPSFFilePath = psfFilePath;
        }

        return true;
    }

    auto PSFHandler_v1_4_1_c::GetPSFData() -> std::shared_ptr<float[]> {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).psfData;
        }
        return nullptr;
    }

    auto PSFHandler_v1_4_1_c::GetPSFImagRealCount() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).imagRealCount;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetPSFPatternCount() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).patternCount;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetPSFSizeX() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).sizeX;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetPSFSizeY() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).sizeY;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetPSFSizeZ() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<0>(d->psfSupportMap[d->latestPSFFilePath]).sizeZ;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetSupportData() -> std::shared_ptr<float[]> {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<1>(d->psfSupportMap[d->latestPSFFilePath]).supportData;
        }
        return nullptr;
    }

    auto PSFHandler_v1_4_1_c::GetSupportSizeX() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<1>(d->psfSupportMap[d->latestPSFFilePath]).sizeX;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetSupportSizeY() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<1>(d->psfSupportMap[d->latestPSFFilePath]).sizeY;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetSupportSizeZ() -> int32_t {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<1>(d->psfSupportMap[d->latestPSFFilePath]).sizeZ;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetKResX() -> double {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<2>(d->psfSupportMap[d->latestPSFFilePath]).x;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetKResY() -> double {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<2>(d->psfSupportMap[d->latestPSFFilePath]).y;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::GetKResZ() -> double {
        if (d->psfSupportMap.contains(d->latestPSFFilePath)) {
            return std::get<2>(d->psfSupportMap[d->latestPSFFilePath]).z;
        }
        return 0;
    }

    auto PSFHandler_v1_4_1_c::Clear() ->void {
        d = std::unique_ptr<Impl>{ new Impl };
    }

    PSFHandler_v1_4_1_c::PSFHandler_v1_4_1_c() : d(new Impl()) {
    }
}
