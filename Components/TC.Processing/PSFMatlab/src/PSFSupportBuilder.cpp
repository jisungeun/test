#define LOGGER_TAG "[PSFSupportBuilder]"

#include "PSFSupportBuilder.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"

#include "TCLogger.h"

namespace TC::PSFMatlab {
    const QString psfSupportBuilderFunctionName = "BuildAndWritePSFAndSupport_SharedLibrary";

    class PSFSupportBuilder::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString psfFilePath{};
        PSFGeneratingParameters psfGeneratingParameters{};
        QString matlabLibraryFilePath{};

        std::shared_ptr<float[]> psfData{};
        std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> psfSize{};

        std::shared_ptr<float[]> supportData{};
        std::tuple<int32_t, int32_t, int32_t> supportSize{};

        std::tuple<double, double, double> kResXYZData{};
    };

    PSFSupportBuilder::PSFSupportBuilder() : d(new Impl()) {
    }

    PSFSupportBuilder::~PSFSupportBuilder() = default;

    auto PSFSupportBuilder::SetPSFFilePath(const QString& psfFilePath) -> void {
        d->psfFilePath = psfFilePath;
    }

    auto PSFSupportBuilder::SetPSFGeneratingParameters(const PSFGeneratingParameters& psfGeneratingParameters) -> void {
        d->psfGeneratingParameters = psfGeneratingParameters;
    }

    auto PSFSupportBuilder::SetMatlabLibraryFilePath(const QString& matlabLibraryFilePath) -> void {
        d->matlabLibraryFilePath = matlabLibraryFilePath;
    }

    auto PSFSupportBuilder::Build() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->matlabLibraryFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto mediumRIValue = d->psfGeneratingParameters.GetMediumRI();
            const auto objectiveNAValue = d->psfGeneratingParameters.GetObjectiveNA();
            const auto condenserNAValue = d->psfGeneratingParameters.GetCondenserNA();
            const auto voxelSizeXYValue = d->psfGeneratingParameters.GetVoxelSizeXY();
            const auto voxelSizeZValue = d->psfGeneratingParameters.GetVoxelSizeZ();
            const auto psfSizeXValue = static_cast<double>(d->psfGeneratingParameters.GetPSFSizeX());
            const auto psfSizeYValue = static_cast<double>(d->psfGeneratingParameters.GetPSFSizeY());
            const auto psfSizeZValue = static_cast<double>(d->psfGeneratingParameters.GetPSFSizeZ());


            matlab::data::ArrayFactory factory;
            const auto mediumRI = MatlabSharedLibrary::ToMatlabInput(mediumRIValue);
            const auto naCond = MatlabSharedLibrary::ToMatlabInput(condenserNAValue);
            const auto naObj = MatlabSharedLibrary::ToMatlabInput(objectiveNAValue);
            const auto voxelSizeXY = MatlabSharedLibrary::ToMatlabInput(voxelSizeXYValue);
            const auto voxelSizeZ = MatlabSharedLibrary::ToMatlabInput(voxelSizeZValue);
            const auto psfFilePath = MatlabSharedLibrary::ToMatlabInput(d->psfFilePath);
            const auto psfSizeX = MatlabSharedLibrary::ToMatlabInput(psfSizeXValue);
            const auto psfSizeY = MatlabSharedLibrary::ToMatlabInput(psfSizeYValue);
            const auto psfSizeZ = MatlabSharedLibrary::ToMatlabInput(psfSizeZValue);

            const std::vector inputs{ mediumRI, naCond, naObj, voxelSizeXY, voxelSizeZ, psfFilePath, psfSizeX, psfSizeY, psfSizeZ };

            constexpr auto numberOfOutputs = 3;
            const auto psfSupportBuilderResultArray =
                matlabLibrary->feval(psfSupportBuilderFunctionName.toStdU16String(), numberOfOutputs, inputs);

            auto psfData = static_cast<matlab::data::TypedArray<float>>(psfSupportBuilderResultArray[0]);
            auto supportData = static_cast<matlab::data::TypedArray<float>>(psfSupportBuilderResultArray[1]);
            auto kResXYZData = static_cast<matlab::data::TypedArray<double>>(psfSupportBuilderResultArray[2]);

            {
                const auto psfDimension = psfData.getDimensions();
                d->psfSize = { psfDimension[0], psfDimension[1], psfDimension[2], psfDimension[3], psfDimension[4] };

                const auto psfNumberOfElements = psfData.getNumberOfElements();
                const auto psfDataUniquePtr = psfData.release();

                d->psfData = std::shared_ptr<float[]>{ new float[psfNumberOfElements]() };
                std::copy_n(psfDataUniquePtr.get(), psfNumberOfElements, d->psfData.get());
            }
            {
                const auto supportDimension = supportData.getDimensions();
                d->supportSize = { supportDimension[0], supportDimension[1], supportDimension[2] };

                const auto supportNumberOfElements = supportData.getNumberOfElements();
                const auto supportDataUniquePtr = supportData.release();

                d->supportData = std::shared_ptr<float[]>{ new float[supportNumberOfElements]() };
                std::copy_n(supportDataUniquePtr.get(), supportNumberOfElements, d->supportData.get());
            }
            {
                const auto kResXYZDataUniquePtr = kResXYZData.release();

                const auto kResX = kResXYZDataUniquePtr.get()[0];
                const auto kResY = kResXYZDataUniquePtr.get()[1];
                const auto kResZ = kResXYZDataUniquePtr.get()[2];

                d->kResXYZData = { kResX, kResY, kResZ };
            }
        } catch (const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }

        return true;
    }

    auto PSFSupportBuilder::GetPsfData() const -> std::shared_ptr<float[]> {
        return d->psfData;
    }

    auto PSFSupportBuilder::GetPsfSize() const -> std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> {
        return d->psfSize;
    }

    auto PSFSupportBuilder::GetSupportData() const -> std::shared_ptr<float[]> {
        return d->supportData;
    }

    auto PSFSupportBuilder::GetSupportSize() const -> std::tuple<int32_t, int32_t, int32_t> {
        return d->supportSize;
    }

    auto PSFSupportBuilder::GetKResXYZData() const -> std::tuple<double, double, double> {
        return d->kResXYZData;
    }
}
