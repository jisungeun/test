#include "PSFBuildingParameters_v1_4_1_c.h"

namespace TC::PSFMatlab {
    class PSFBuildingParameters_v1_4_1_c::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        double mediumRI{};
        double condenserNA{};
        double objectiveNA{};
        double voxelSizeXY{};
        double voxelSizeZ{};
    };

    PSFBuildingParameters_v1_4_1_c::PSFBuildingParameters_v1_4_1_c() : d(new Impl()) {
    }

    PSFBuildingParameters_v1_4_1_c::PSFBuildingParameters_v1_4_1_c(const PSFBuildingParameters_v1_4_1_c& other)
        : d(new Impl(*other.d)) {
    }

    PSFBuildingParameters_v1_4_1_c::~PSFBuildingParameters_v1_4_1_c() = default;

    auto PSFBuildingParameters_v1_4_1_c::operator=(const PSFBuildingParameters_v1_4_1_c& other)
        -> PSFBuildingParameters_v1_4_1_c& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto PSFBuildingParameters_v1_4_1_c::SetMediumRI(const double& mediumRI) -> void {
        d->mediumRI = mediumRI;
    }

    auto PSFBuildingParameters_v1_4_1_c::SetCondenserNA(const double& condenserNA) -> void {
        d->condenserNA = condenserNA;
    }

    auto PSFBuildingParameters_v1_4_1_c::SetObjectiveNA(const double& objectiveNA) -> void {
        d->objectiveNA = objectiveNA;
    }

    auto PSFBuildingParameters_v1_4_1_c::SetVoxelSizeXY(const double& voxelSizeXY) -> void {
        d->voxelSizeXY = voxelSizeXY;
    }

    auto PSFBuildingParameters_v1_4_1_c::SetVoxelSizeZ(const double& voxelSizeZ) -> void {
        d->voxelSizeZ = voxelSizeZ;
    }

    auto PSFBuildingParameters_v1_4_1_c::GetMediumRI() const -> const double& {
        return d->mediumRI;
    }

    auto PSFBuildingParameters_v1_4_1_c::GetCondenserNA() const -> const double& {
        return d->condenserNA;
    }

    auto PSFBuildingParameters_v1_4_1_c::GetObjectiveNA() const -> const double& {
        return d->objectiveNA;
    }

    auto PSFBuildingParameters_v1_4_1_c::GetVoxelSizeXY() const -> const double& {
        return d->voxelSizeXY;
    }

    auto PSFBuildingParameters_v1_4_1_c::GetVoxelSizeZ() const -> const double& {
        return d->voxelSizeZ;
    }
}