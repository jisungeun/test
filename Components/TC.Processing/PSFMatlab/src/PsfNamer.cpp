#include "PsfNamer.h"

namespace TC::PSFMatlab {
    class PsfNamer::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        PSFGeneratingParameters psfGeneratingParameters{};
    };

    PsfNamer::PsfNamer() : d(new Impl()) {
    }

    PsfNamer::~PsfNamer() = default;

    auto PsfNamer::SetPSFGeneratingParameters(const PSFGeneratingParameters& psfGeneratingParameters)->void {
        d->psfGeneratingParameters = psfGeneratingParameters;
    }

    auto PsfNamer::GetName() -> QString {
        const auto mediumRI = d->psfGeneratingParameters.GetMediumRI();
        const auto condenserNA = d->psfGeneratingParameters.GetCondenserNA();
        const auto objectiveNA = d->psfGeneratingParameters.GetObjectiveNA();
        const auto voxelSizeXY = d->psfGeneratingParameters.GetVoxelSizeXY();
        const auto voxelSizeZ = d->psfGeneratingParameters.GetVoxelSizeZ();
        const auto psfSizeX = d->psfGeneratingParameters.GetPSFSizeX();
        const auto psfSizeY = d->psfGeneratingParameters.GetPSFSizeY();
        const auto psfSizeZ = d->psfGeneratingParameters.GetPSFSizeZ();

        return QString("PSFv1.4.1_NAo%1_NAc%2_n_m%3_voxSz%4-%5_Size%6x%7x%8")
            .arg(objectiveNA, 0, 'f', 2) // 1
            .arg(condenserNA, 0, 'f', 2) // 2
            .arg(mediumRI, 0, 'f', 4) // 3
            .arg(voxelSizeXY, 0, 'f', 4) // 4
            .arg(voxelSizeZ, 0, 'f', 4) // 5
            .arg(psfSizeX) // 6
            .arg(psfSizeY) // 7
            .arg(psfSizeZ); // 8
    }
}
