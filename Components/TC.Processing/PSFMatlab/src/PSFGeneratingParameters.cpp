#include "PSFGeneratingParameters.h"

namespace TC::PSFMatlab {
    class PSFGeneratingParameters::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        double mediumRI{};
        double condenserNA{};
        double objectiveNA{};
        double voxelSizeXY{};
        double voxelSizeZ{};

        int32_t psfSizeX{};
        int32_t psfSizeY{};
        int32_t psfSizeZ{};
    };

    PSFGeneratingParameters::PSFGeneratingParameters() : d(new Impl()) {
    }

    PSFGeneratingParameters::PSFGeneratingParameters(const PSFGeneratingParameters& other) : d(new Impl(*other.d)) {
    }

    PSFGeneratingParameters::~PSFGeneratingParameters() = default;

    auto PSFGeneratingParameters::operator=(const PSFGeneratingParameters& other) -> PSFGeneratingParameters& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto PSFGeneratingParameters::SetMediumRI(const double& mediumRI) -> void {
        d->mediumRI = mediumRI;
    }

    auto PSFGeneratingParameters::SetCondenserNA(const double& condenserNA) -> void {
        d->condenserNA = condenserNA;
    }

    auto PSFGeneratingParameters::SetObjectiveNA(const double& objectiveNA) -> void {
        d->objectiveNA = objectiveNA;
    }

    auto PSFGeneratingParameters::SetVoxelSizeXY(const double& voxelSizeXY) -> void {
        d->voxelSizeXY = voxelSizeXY;
    }

    auto PSFGeneratingParameters::SetVoxelSizeZ(const double& voxelSizeZ) -> void {
        d->voxelSizeZ = voxelSizeZ;
    }

    auto PSFGeneratingParameters::SetPSFSizeXYZ(const int32_t& psfSizeX, const int32_t& psfSizeY, const int32_t& psfSizeZ)
        -> void {
        d->psfSizeX = psfSizeX;
        d->psfSizeY = psfSizeY;
        d->psfSizeZ = psfSizeZ;
    }

    auto PSFGeneratingParameters::GetMediumRI() const -> const double& {
        return d->mediumRI;
    }

    auto PSFGeneratingParameters::GetCondenserNA() const -> const double& {
        return d->condenserNA;
    }

    auto PSFGeneratingParameters::GetObjectiveNA() const -> const double& {
        return d->objectiveNA;
    }

    auto PSFGeneratingParameters::GetVoxelSizeXY() const -> const double& {
        return d->voxelSizeXY;
    }

    auto PSFGeneratingParameters::GetVoxelSizeZ() const -> const double& {
        return d->voxelSizeZ;
    }

    auto PSFGeneratingParameters::GetPSFSizeX() const -> const int32_t& {
        return d->psfSizeX;
    }

    auto PSFGeneratingParameters::GetPSFSizeY() const -> const int32_t& {
        return d->psfSizeY;
    }

    auto PSFGeneratingParameters::GetPSFSizeZ() const -> const int32_t& {
        return d->psfSizeZ;
    }
}