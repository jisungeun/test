#define LOGGER_TAG "[PSFBuilder_v1_4_1_c]"

#include "PSFBuilder_v1_4_1_c.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"

#include "TCLogger.h"

namespace TC::PSFMatlab {
    const QString psfSupportBuilderFunctionName = "BuildAndWritePSFAndSupport_SharedLibrary";
    using PSFBuildingParameters = PSFBuildingParameters_v1_4_1_c;
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    class PSFBuilder_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        auto GeneratePSFBuildingInputArray()->matlab::data::Array;
        auto GeneratePSFProfileArray()->matlab::data::Array;

        QString psfFilePath{};
        PSFBuildingParameters psfBuildingParameters{};
        Profile psfProfile;
        QString psfModuleFilePath{};

        std::shared_ptr<float[]> psfData{};
        std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> psfSize{};

        std::shared_ptr<float[]> supportData{};
        std::tuple<int32_t, int32_t, int32_t> supportSize{};

        std::tuple<double, double, double> kResXYZData{};
    };

    auto PSFBuilder_v1_4_1_c::Impl::GeneratePSFBuildingInputArray() -> matlab::data::Array {
        matlab::data::ArrayFactory factory;
        const auto mediumRI = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfBuildingParameters.GetMediumRI()));
        const auto naCond = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfBuildingParameters.GetCondenserNA()));
        const auto naObj = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfBuildingParameters.GetObjectiveNA()));
        const auto voxelSizeXY = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfBuildingParameters.GetVoxelSizeXY()));
        const auto voxelSizeZ = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfBuildingParameters.GetVoxelSizeZ()));
        const auto psfFilePath = MatlabSharedLibrary::ToMatlabInput(this->psfFilePath);

        auto psfBuildingInputStruct = factory.createStructArray({ 1,1 },
            { "mediumRI", "naCond", "naObj", "voxelSizeXY", "voxelSizeZ", "psfFilePath"});

        psfBuildingInputStruct[0]["mediumRI"] = mediumRI;
        psfBuildingInputStruct[0]["naCond"] = naCond;
        psfBuildingInputStruct[0]["naObj"] = naObj;
        psfBuildingInputStruct[0]["voxelSizeXY"] = voxelSizeXY;
        psfBuildingInputStruct[0]["voxelSizeZ"] = voxelSizeZ;
        psfBuildingInputStruct[0]["psfFilePath"] = psfFilePath;

        return static_cast<matlab::data::Array>(psfBuildingInputStruct);
    }

    auto PSFBuilder_v1_4_1_c::Impl::GeneratePSFProfileArray() -> matlab::data::Array {
        matlab::data::ArrayFactory factory;

        const auto useRealPartOnly = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.useRealPartOnly));
        const auto psfSizeX = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.psfSizeX));
        const auto psfSizeY = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.psfSizeY));
        const auto psfSizeZ = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.psfSizeZ));
        const auto denoiseThres = MatlabSharedLibrary::ToMatlabInput(this->psfProfile.denoiseThres);
        const auto zComputationSplit = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.zComputationSplit));
        const auto secondCropNaObj = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.secondCropNaObj));
        const auto secondCropNaCond = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.secondCropNaCond));
        const auto secondCropNaObj02 = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.secondCropNAObj02));
        const auto secondCropNaCond02 = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.secondCropNACond02));
        const auto spectrumSize = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.spectrumSize));
        const auto filterSnrSupport = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.filterSnrSupport));
        const auto axialUpSampleRatio = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.axialUpSampleRatio));
        const auto lateralVoxelCount = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->psfProfile.lateralVoxelCount));

        auto psfProfileStruct = factory.createStructArray({ 1,1 },
            { "useRealPartOnly", "psfSizeX", "psfSizeY", "psfSizeZ", "denoiseThres", "zComputationSplit", "secondCropNaObj",
            "secondCropNaCond", "secondCropNaObj02", "secondCropNaCond02", "spectrumSize", "filterSnrSupport",
            "axialUpsampleRatio", "lateralVoxelCount"});

        psfProfileStruct[0]["useRealPartOnly"] = useRealPartOnly;
        psfProfileStruct[0]["psfSizeX"] = psfSizeX;
        psfProfileStruct[0]["psfSizeY"] = psfSizeY;
        psfProfileStruct[0]["psfSizeZ"] = psfSizeZ;
        psfProfileStruct[0]["denoiseThres"] = denoiseThres;
        psfProfileStruct[0]["zComputationSplit"] = zComputationSplit;
        psfProfileStruct[0]["secondCropNaObj"] = secondCropNaObj;
        psfProfileStruct[0]["secondCropNaCond"] = secondCropNaCond;
        psfProfileStruct[0]["secondCropNaObj02"] = secondCropNaObj02;
        psfProfileStruct[0]["secondCropNaCond02"] = secondCropNaCond02;
        psfProfileStruct[0]["spectrumSize"] = spectrumSize;
        psfProfileStruct[0]["filterSnrSupport"] = filterSnrSupport;
        psfProfileStruct[0]["axialUpsampleRatio"] = axialUpSampleRatio;
        psfProfileStruct[0]["lateralVoxelCount"] = lateralVoxelCount;

        return static_cast<matlab::data::Array>(psfProfileStruct);
    }

    PSFBuilder_v1_4_1_c::PSFBuilder_v1_4_1_c() : d(new Impl()) {
    }

    PSFBuilder_v1_4_1_c::~PSFBuilder_v1_4_1_c() = default;

    auto PSFBuilder_v1_4_1_c::SetPSFFilePath(const QString& psfFilePath) -> void {
        d->psfFilePath = psfFilePath;
    }

    auto PSFBuilder_v1_4_1_c::SetPSFBuildingParameters(const PSFBuildingParameters& psfBuildingParameters) -> void {
        d->psfBuildingParameters = psfBuildingParameters;
    }

    auto PSFBuilder_v1_4_1_c::SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile) -> void {
        d->psfProfile = psfProfile;
    }

    auto PSFBuilder_v1_4_1_c::SetPSFModuleFilePath(const QString& psfModuleFilePath) -> void {
        d->psfModuleFilePath = psfModuleFilePath;
    }

    auto PSFBuilder_v1_4_1_c::Build() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->psfModuleFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto psfBuildingInputArray = d->GeneratePSFBuildingInputArray();
            const auto psfProfileArray = d->GeneratePSFProfileArray();

            const std::vector inputs{ psfBuildingInputArray, psfProfileArray };

            constexpr auto numberOfOutputs = 3;
            const auto psfSupportBuilderResultArray =
                matlabLibrary->feval(psfSupportBuilderFunctionName.toStdU16String(), numberOfOutputs, inputs);

            auto psfData = static_cast<matlab::data::TypedArray<float>>(psfSupportBuilderResultArray[0]);
            auto supportData = static_cast<matlab::data::TypedArray<float>>(psfSupportBuilderResultArray[1]);
            auto kResXYZData = static_cast<matlab::data::TypedArray<double>>(psfSupportBuilderResultArray[2]);

            {
                const auto psfDimension = psfData.getDimensions();
                d->psfSize = { psfDimension[0], psfDimension[1], psfDimension[2], psfDimension[3], psfDimension[4] };

                const auto psfNumberOfElements = psfData.getNumberOfElements();
                const auto psfDataUniquePtr = psfData.release();

                d->psfData = std::shared_ptr<float[]>{ new float[psfNumberOfElements]() };
                std::copy_n(psfDataUniquePtr.get(), psfNumberOfElements, d->psfData.get());
            }
            {
                const auto supportDimension = supportData.getDimensions();
                d->supportSize = { supportDimension[0], supportDimension[1], supportDimension[2] };

                const auto supportNumberOfElements = supportData.getNumberOfElements();
                const auto supportDataUniquePtr = supportData.release();

                d->supportData = std::shared_ptr<float[]>{ new float[supportNumberOfElements]() };
                std::copy_n(supportDataUniquePtr.get(), supportNumberOfElements, d->supportData.get());
            }
            {
                const auto kResXYZDataUniquePtr = kResXYZData.release();

                const auto kResX = kResXYZDataUniquePtr.get()[0];
                const auto kResY = kResXYZDataUniquePtr.get()[1];
                const auto kResZ = kResXYZDataUniquePtr.get()[2];

                d->kResXYZData = { kResX, kResY, kResZ };
            }
        } catch (const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }

        return true;
    }

    auto PSFBuilder_v1_4_1_c::GetPsfData() const -> std::shared_ptr<float[]> {
        return d->psfData;
    }

    auto PSFBuilder_v1_4_1_c::GetPsfSize() const -> std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> {
        return d->psfSize;
    }

    auto PSFBuilder_v1_4_1_c::GetSupportData() const -> std::shared_ptr<float[]> {
        return d->supportData;
    }

    auto PSFBuilder_v1_4_1_c::GetSupportSize() const -> std::tuple<int32_t, int32_t, int32_t> {
        return d->supportSize;
    }

    auto PSFBuilder_v1_4_1_c::GetKResXYZData() const -> std::tuple<double, double, double> {
        return d->kResXYZData;
    }
}
