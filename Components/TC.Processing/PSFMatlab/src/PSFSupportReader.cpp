#define LOGGER_TAG "[PSFReader_v1_4_1_c]"

#include "PSFReader_v1_4_1_c.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"
#include "TCLogger.h"

namespace TC::PSFMatlab {
    const QString psfSupportReaderFunctionName = "ReadPSFAndSupport_SharedLibrary";

    class PSFReader_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString psfFilePath{};
        QString psfModuleFilePath{};

        std::shared_ptr<float[]> psfData{};
        std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> psfSize{};

        std::shared_ptr<float[]> supportData{};
        std::tuple<int32_t, int32_t, int32_t> supportSize{};

        std::tuple<double, double, double> kResXYZData{};
    };

    PSFReader_v1_4_1_c::PSFReader_v1_4_1_c() : d(new Impl()) {
    }

    PSFReader_v1_4_1_c::~PSFReader_v1_4_1_c() = default;

    auto PSFReader_v1_4_1_c::SetPSFFilePath(const QString& psfFilePath) -> void {
        d->psfFilePath = psfFilePath;
    }

    auto PSFReader_v1_4_1_c::SetPSFModuleFilePath(const QString& psfModuleFilePath) -> void {
        d->psfModuleFilePath = psfModuleFilePath;
    }

    auto PSFReader_v1_4_1_c::Read() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->psfModuleFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto psfFilePathValue = d->psfFilePath;

            matlab::data::ArrayFactory factory;
            const auto psfFilePath = MatlabSharedLibrary::ToMatlabInput(psfFilePathValue);

            const std::vector inputs{ psfFilePath };

            constexpr auto numberOfOutputs = 3;
            const auto psfSupportReaderResultArray =
                matlabLibrary->feval(psfSupportReaderFunctionName.toStdU16String(), numberOfOutputs, inputs);

            auto psfData = static_cast<matlab::data::TypedArray<float>>(psfSupportReaderResultArray[0]);
            auto supportData = static_cast<matlab::data::TypedArray<float>>(psfSupportReaderResultArray[1]);
            auto kResXYZData = static_cast<matlab::data::TypedArray<double>>(psfSupportReaderResultArray[2]);

            {
                const auto psfDimension = psfData.getDimensions();
                d->psfSize = { psfDimension[0], psfDimension[1], psfDimension[2], psfDimension[3], psfDimension[4] };

                const auto psfNumberOfElements = psfData.getNumberOfElements();
                const auto psfDataUniquePtr = psfData.release();

                d->psfData = std::shared_ptr<float[]>{ new float[psfNumberOfElements]() };
                std::copy_n(psfDataUniquePtr.get(), psfNumberOfElements, d->psfData.get());
            }
            {
                const auto supportDimension = supportData.getDimensions();
                d->supportSize = { supportDimension[0], supportDimension[1], supportDimension[2] };

                const auto supportNumberOfElements = supportData.getNumberOfElements();
                const auto supportDataUniquePtr = supportData.release();

                d->supportData = std::shared_ptr<float[]>{ new float[supportNumberOfElements]() };
                std::copy_n(supportDataUniquePtr.get(), supportNumberOfElements, d->supportData.get());
            }
            {
                const auto kResXYZDataUniquePtr = kResXYZData.release();

                const auto kResX = kResXYZDataUniquePtr.get()[0];
                const auto kResY = kResXYZDataUniquePtr.get()[1];
                const auto kResZ = kResXYZDataUniquePtr.get()[2];

                d->kResXYZData = { kResX, kResY, kResZ };
            }
            
        } catch(const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }

        return true;
    }

    auto PSFReader_v1_4_1_c::GetPsfData() const -> std::shared_ptr<float[]> {
        return d->psfData;
    }

    auto PSFReader_v1_4_1_c::GetPsfSize() const -> std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t> {
        return d->psfSize;
    }

    auto PSFReader_v1_4_1_c::GetSupportData() const -> std::shared_ptr<float[]> {
        return d->supportData;
    }

    auto PSFReader_v1_4_1_c::GetSupportSize() const -> std::tuple<int32_t, int32_t, int32_t> {
        return d->supportSize;
    }

    auto PSFReader_v1_4_1_c::GetKResXYZData() const -> std::tuple<double, double, double> {
        return d->kResXYZData;
    }
}
