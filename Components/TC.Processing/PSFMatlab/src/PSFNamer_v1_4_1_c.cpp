#include "PSFNamer_v1_4_1_c.h"

namespace TC::PSFMatlab {
    using PSFBuildingParameters = PSFBuildingParameters_v1_4_1_c;
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    class PSFNamer_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        PSFBuildingParameters psfBuildingParameters{};
        Profile psfProfile{};
    };

    PSFNamer_v1_4_1_c::PSFNamer_v1_4_1_c() : d(new Impl()) {
    }

    PSFNamer_v1_4_1_c::~PSFNamer_v1_4_1_c() = default;

    auto PSFNamer_v1_4_1_c::SetPSFBuildingParameters(const PSFBuildingParameters_v1_4_1_c& psfBuildingParameters)->void {
        d->psfBuildingParameters = psfBuildingParameters;
    }

    auto PSFNamer_v1_4_1_c::SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile) -> void {
        d->psfProfile = psfProfile;
    }

    auto PSFNamer_v1_4_1_c::GetName() -> QString {
        const QString versionString = "v1.4.1c";
        const auto mediumRI = d->psfBuildingParameters.GetMediumRI();
        const auto condenserNA = d->psfBuildingParameters.GetCondenserNA();
        const auto objectiveNA = d->psfBuildingParameters.GetObjectiveNA();
        const auto voxelSizeXY = d->psfBuildingParameters.GetVoxelSizeXY();
        const auto voxelSizeZ = d->psfBuildingParameters.GetVoxelSizeZ();
        const auto psfSizeX = d->psfProfile.psfSizeX;
        const auto psfSizeY = d->psfProfile.psfSizeY;
        const auto psfSizeZ = d->psfProfile.psfSizeZ;

        return QString("PSF%0_NAo%1_NAc%2_n_m%3_voxSz%4-%5_Size%6x%7x%8")
            .arg(versionString) //0
            .arg(objectiveNA, 0, 'f', 2) // 1
            .arg(condenserNA, 0, 'f', 2) // 2
            .arg(mediumRI, 0, 'f', 4) // 3
            .arg(voxelSizeXY, 0, 'f', 4) // 4
            .arg(voxelSizeZ, 0, 'f', 4) // 5
            .arg(psfSizeX) // 6
            .arg(psfSizeY) // 7
            .arg(psfSizeZ); // 8
    }
}
