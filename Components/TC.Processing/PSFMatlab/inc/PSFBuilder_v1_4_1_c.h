#pragma once

#include <memory>
#include <QString>
#include "PSFBuildingParameters_v1_4_1_c.h"
#include "PSFProfile_v1_4_1_c.h"
#include "TCPSFMatlabExport.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PSFBuilder_v1_4_1_c {
    public:
        PSFBuilder_v1_4_1_c();
        ~PSFBuilder_v1_4_1_c();

        auto SetPSFFilePath(const QString& psfFilePath)->void;
        auto SetPSFBuildingParameters(const PSFBuildingParameters_v1_4_1_c& psfBuildingParameters)->void;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void;
        auto SetPSFModuleFilePath(const QString& psfModuleFilePath)->void;

        auto Build()->bool;

        auto GetPsfData() const->std::shared_ptr<float[]>;
        auto GetPsfSize() const->std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t>;

        auto GetSupportData() const->std::shared_ptr<float[]>;
        auto GetSupportSize() const->std::tuple<int32_t, int32_t, int32_t>;

        auto GetKResXYZData() const->std::tuple<double, double, double>;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}