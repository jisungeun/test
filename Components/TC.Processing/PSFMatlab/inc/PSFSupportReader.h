#pragma once

#include <memory>
#include <QString>
#include "TCPSFMatlabExport.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PSFReader_v1_4_1_c {
    public:
        PSFReader_v1_4_1_c();
        ~PSFReader_v1_4_1_c();

        auto SetPSFFilePath(const QString& psfFilePath)->void;
        auto SetPSFModuleFilePath(const QString& psfModuleFilePath)->void;

        auto Read()->bool;

        auto GetPsfData() const->std::shared_ptr<float[]>;
        auto GetPsfSize() const->std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t>;

        auto GetSupportData() const->std::shared_ptr<float[]>;
        auto GetSupportSize() const->std::tuple<int32_t, int32_t, int32_t>;

        auto GetKResXYZData() const->std::tuple<double, double, double>;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}