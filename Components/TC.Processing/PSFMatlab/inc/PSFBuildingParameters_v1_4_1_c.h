#pragma once

#include <memory>

#include "TCPSFMatlabExport.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PSFBuildingParameters_v1_4_1_c {
    public:
        PSFBuildingParameters_v1_4_1_c();
        PSFBuildingParameters_v1_4_1_c(const PSFBuildingParameters_v1_4_1_c& other);
        ~PSFBuildingParameters_v1_4_1_c();

        auto operator=(const PSFBuildingParameters_v1_4_1_c& other)->PSFBuildingParameters_v1_4_1_c&;

        auto SetMediumRI(const double& mediumRI)->void;
        auto SetCondenserNA(const double& condenserNA)->void;
        auto SetObjectiveNA(const double& objectiveNA)->void;
        auto SetVoxelSizeXY(const double& voxelSizeXY)->void;
        auto SetVoxelSizeZ(const double& voxelSizeZ)->void;

        auto GetMediumRI() const ->const double&;
        auto GetCondenserNA() const ->const double&;
        auto GetObjectiveNA() const ->const double&;
        auto GetVoxelSizeXY() const ->const double&;
        auto GetVoxelSizeZ() const ->const double&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}