#pragma once

#include <memory>
#include <QString>
#include "TCPSFMatlabExport.h"

#include "PSFGeneratingParameters.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PsfNamer {
    public:
        PsfNamer();
        ~PsfNamer();

        auto SetPSFGeneratingParameters(const PSFGeneratingParameters& psfGeneratingParameters)->void;
        auto GetName()->QString;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}