#pragma once

#include <memory>
#include <QString>
#include "TCPSFMatlabExport.h"

#include "PSFBuildingParameters_v1_4_1_c.h"
#include "PSFProfile_v1_4_1_c.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PSFNamer_v1_4_1_c {
    public:
        PSFNamer_v1_4_1_c();
        ~PSFNamer_v1_4_1_c();

        auto SetPSFBuildingParameters(const PSFBuildingParameters_v1_4_1_c& psfBuildingParameters)->void;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void;
        auto GetName()->QString;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
