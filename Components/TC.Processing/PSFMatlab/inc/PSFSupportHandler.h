#pragma once

#include <memory>
#include <QString>
#include "PSFBuildingParameters_v1_4_1_c.h"
#include "PSFProfile_v1_4_1_c.h"
#include "TCPSFMatlabExport.h"

namespace TC::PSFMatlab {
    class TCPSFMatlab_API PSFHandler_v1_4_1_c {
    public:
        ~PSFHandler_v1_4_1_c();

        static auto GetInstance()->std::shared_ptr<PSFHandler_v1_4_1_c>;

        auto SetPSFFolderPath(const QString& psfFolderPath)->void;
        auto SetPSFModuleFilePath(const QString& psfModuleFilePath)->void;
        auto SetPSFBuildingParameters(const PSFBuildingParameters_v1_4_1_c& psfBuildingParameters)->void;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void;

        auto Update()->bool;

        auto GetPSFData()->std::shared_ptr<float[]>;
        auto GetPSFImagRealCount()->int32_t;
        auto GetPSFPatternCount()->int32_t;
        auto GetPSFSizeX()->int32_t;
        auto GetPSFSizeY()->int32_t;
        auto GetPSFSizeZ()->int32_t;

        auto GetSupportData()->std::shared_ptr<float[]>;
        auto GetSupportSizeX()->int32_t;
        auto GetSupportSizeY()->int32_t;
        auto GetSupportSizeZ()->int32_t;

        auto GetKResX()->double;
        auto GetKResY()->double;
        auto GetKResZ()->double;
        
        auto Clear()->void;
    protected:
        PSFHandler_v1_4_1_c();
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
