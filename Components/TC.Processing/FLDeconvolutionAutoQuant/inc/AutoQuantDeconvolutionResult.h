#pragma once

#include <memory>
#include <QString>

#include "TCFLDeconvolutionAutoQuantExport.h"

#include "FLDeconData.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionResult {
public:
    AutoQuantDeconvolutionResult();
    AutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& other);
    ~AutoQuantDeconvolutionResult();

    auto operator=(const AutoQuantDeconvolutionResult& other)->AutoQuantDeconvolutionResult&;

    auto SetFLDeconData(const FLDeconData& data)->void;
    auto GetFLDeconData()const->const FLDeconData&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};