#pragma once

#include <memory>

#include "IAutoQuantDeconvolutionOutput.h"
#include "TCFLDeconvolutionAutoQuantExport.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionOutput final : public IAutoQuantDeconvolutionOutput{
public:
    AutoQuantDeconvolutionOutput();
    ~AutoQuantDeconvolutionOutput();
    auto SetAutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& result) -> void override;

    auto GetDataMemory(const FLMemoryOrder& order)->std::shared_ptr<float[]>;
    auto GetSizeX()const->int32_t;
    auto GetSizeY()const->int32_t;
    auto GetSizeZ()const->int32_t;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};