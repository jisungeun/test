#pragma once

#include "enum.h"

BETTER_ENUM(FLMemoryOrder, uint8_t, XYZ, YXZ);