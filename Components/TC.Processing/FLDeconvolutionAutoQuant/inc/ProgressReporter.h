#pragma once

class ProgressReporter {
public:
    ProgressReporter() {}
    ~ProgressReporter() {}

    virtual void notify(int value) = 0;
};

class NoProgressReporter : public ProgressReporter {
public:
    void notify(int value) override {}
};
