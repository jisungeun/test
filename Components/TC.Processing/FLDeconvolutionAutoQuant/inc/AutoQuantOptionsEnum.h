#pragma once

#include "enum.h"

BETTER_ENUM(Modality, uint32_t, 
    Invalid, 
    Confocal, 
    SpinDiskConfocal, 
    TwoPhoton, 
    WidefieldFluorescence, 
    TransmittedLightBrightField, 
    IndoCyanineGreenFluorescence, 
    ReflectedLight,
    NonSpecific);

BETTER_ENUM(DeconvolutionMethod, uint8_t, 
    ExpectationMaximization, 
    PowerEcceleration, 
    PSFExtrapolationAcceleration, 
    GoldsMethod);

BETTER_ENUM(DarkCurrentMethod, uint8_t, 
    AutoCalculation, 
    ManualInput);

BETTER_ENUM(InitialImageGuessGenerationMethod, uint8_t, 
    OriginalData, 
    LinearFilteredOriginal, 
    ConstantValue, 
    UserInput);

BETTER_ENUM(PSFGuessMethod, uint8_t, 
    TheoreticalEsitimation, 
    ConstantValue, 
    Autocorrelation, 
    UserInput);

BETTER_ENUM(FrequencyBandlimitDeterminationMethod, uint8_t, 
    AutoSelection, 
    TheoreticalLimit, 
    DetectedLimit);

BETTER_ENUM(SubVolumeCalculationMethod, uint8_t, 
    Predetermined, 
    Dynamic);

BETTER_ENUM(TheoreticalPSFGenerationMethod, uint8_t, 
    LagacyAutoQuant, 
    GibsonLanni, 
    Zernike);