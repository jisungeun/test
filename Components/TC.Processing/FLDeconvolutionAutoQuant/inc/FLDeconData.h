#pragma once

#include <memory>
#include "TCFLDeconvolutionAutoQuantExport.h"
#include "FLMemoryOrder.h"

class TCFLDeconvolutionAutoQuant_API FLDeconData {
public:
    FLDeconData();
    FLDeconData(const FLDeconData& other);
    ~FLDeconData();

    auto operator=(const FLDeconData& other)->FLDeconData&;

    auto SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetData(const std::shared_ptr<float[]>& data, const FLMemoryOrder& memoryOrder)->void;

    auto GetSizeX()const->const int32_t&;
    auto GetSizeY()const->const int32_t&;
    auto GetSizeZ()const->const int32_t&;

    auto GetData(const FLMemoryOrder& memoryOrder)const->std::shared_ptr<float[]>;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};