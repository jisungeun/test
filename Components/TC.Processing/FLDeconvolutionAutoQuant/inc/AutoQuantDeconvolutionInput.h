#pragma once

#include <memory>
#include <QString>
#include "TCFLDeconvolutionAutoQuantExport.h"

#include "FLRawData.h"
#include "SIUnit.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionInput {
public:
    AutoQuantDeconvolutionInput();
    AutoQuantDeconvolutionInput(const AutoQuantDeconvolutionInput& other);
    ~AutoQuantDeconvolutionInput();

    auto operator=(const AutoQuantDeconvolutionInput& other)->AutoQuantDeconvolutionInput&;

    auto SetFLRawData(const FLRawData& flRawData)->void;
    auto GetFLRawData()const ->const FLRawData&;

    auto SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY, const float& pixelWorldSizeZ, const LengthUnit& unit)->void;
    auto GetPixelWorldSizeX(const LengthUnit& unit)const->float;
    auto GetPixelWorldSizeY(const LengthUnit& unit)const->float;
    auto GetPixelWorldSizeZ(const LengthUnit& unit)const->float;

    auto SetNumericalAperture(const float& numericalAperture)->void;
    auto GetNumericalAperture()const->const float&;

    auto SetMediumRI(const float& mediumRI)->void;
    auto GetMediumRI()const->const float&;

    auto SetEmissionWaveLength(const float& emissionWaveLength, const LengthUnit& unit)->void;
    auto GetEmissionWaveLength(const LengthUnit& unit)const->float;

    auto SetSampleDepth(const float& sampleDepth, const LengthUnit& unit)->void;
    auto GetSampleDepth(const LengthUnit& unit)const->float;

    auto SetTempFolderPath(const QString& tempFolderPath)->void;
    auto GetTempFolderPath()const->const QString&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};

