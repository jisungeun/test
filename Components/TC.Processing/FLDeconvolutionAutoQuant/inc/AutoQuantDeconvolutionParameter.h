#pragma once

#include <memory>
#include <QString>

#include "AutoQuantDeconvolutionConfig.h"

#include "TCFLDeconvolutionAutoQuantExport.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionParameter {
public:
    enum class DetectMethod {
        DETECT_SA,
        DETECT_SAMPLE_RI,
        DETECT_SAMPLE_DEPTH
    };

    AutoQuantDeconvolutionParameter();
    ~AutoQuantDeconvolutionParameter();

    bool LoadParameters(AutoQuantDeconvolutionConfig& config, int chidx = 0);

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
