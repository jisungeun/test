#pragma once

#include <memory>

#include <QString>
#include "TCFLDeconvolutionAutoQuantExport.h"

#include "AutoQuantOptionsEnum.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionOptions {
public:
    AutoQuantDeconvolutionOptions();
    AutoQuantDeconvolutionOptions(const AutoQuantDeconvolutionOptions& other);
    ~AutoQuantDeconvolutionOptions();

    auto operator=(const AutoQuantDeconvolutionOptions& other)->AutoQuantDeconvolutionOptions&;

    auto SetLogFilePath(const QString& logFilePath)->void;
    auto GetLogFilePath()const->const QString&;

    auto SetModality(const Modality& modality)->void;
    auto GetModality()const->const Modality&;

    auto SetDeconvolutionMethod(const DeconvolutionMethod& method)->void;
    auto GetDeconvolutionMethod()const->const DeconvolutionMethod&;

    auto SetDarkCurrentMethod(const DarkCurrentMethod& method)->void;
    auto GetDarkCurrentMethod()const->const DarkCurrentMethod&;

    auto SetInitialImageGuessGenerationMethod(const InitialImageGuessGenerationMethod& method)->void;
    auto GetInitialImageGuessGenerationMethod()const->const InitialImageGuessGenerationMethod&;

    auto SetPSFGuessMethod(const PSFGuessMethod& method)->void;
    auto GetPSFGuessMethod()const->const PSFGuessMethod&;

    auto SetFrequencyBandlimitDeterminationMethod(const FrequencyBandlimitDeterminationMethod& method)->void;
    auto GetFrequencyBandlimitDeterminationMethod()const->const FrequencyBandlimitDeterminationMethod&;

    auto SetSubVolumeCalculationMethod(const SubVolumeCalculationMethod& method)->void;
    auto GetSubVolumeCalculationMethod()const->const SubVolumeCalculationMethod&;

    auto SetTheoreticalPSFGenerationMethod(const TheoreticalPSFGenerationMethod& method)->void;
    auto GetTheoreticalPSFGenerationMethod()const->const TheoreticalPSFGenerationMethod&;

    auto SetDarkCurrentValue(const float& value)->void;
    auto GetDarkCurrentValue()const->const float&;

    auto SetNumberOfIteration(const int32_t& numberOfIteration)->void;
    auto GetNumberOfIteration()const->const int32_t&;

    auto SetSaveInterval(const int32_t& saveInterval)->void;
    auto GetSaveInterval()const->const int32_t&;

    auto SetBinningFactorToXYPlane(const int32_t& factor)->void;
    auto GetBinningFactorToXYPlane()const->const int32_t&;

    auto SetBinningFactorToZPlane(const int32_t& factor)->void;
    auto GetBinningFactorToZPlane()const->const int32_t&;

    auto SetEnableGPUProcessing(const bool& enableGPUProcessing)->void;
    auto GetEnableGPUProcessing()const->const bool&;

    auto SetPaddingSizeToXYBorder(const int32_t& paddingSize)->void;
    auto GetPaddingSizeToXYBorder()const->const int32_t&;

    auto SetPaddingSizeToZBorder(const int32_t& paddingSize)->void;
    auto GetPaddingSizeToZBorder()const->const int32_t&;

    auto SetSubVolumeOverlapSize(const int32_t& overlapSize)->void;
    auto GetSubVolumeOverlapSize()const->const int32_t&;

    auto SetEnableSubVolumeInXY(const bool& enable)->void;
    auto GetEnableSubVolumeInXY()const->const bool&;

    auto SetEnableSubVolumeInZ(const bool& enable)->void;
    auto GetEnableSubVolumeInZ()const->const bool&;

    auto SetEnablePSFConstraint(const bool& enable)->void;
    auto GetEnablePSFConstraint()const->const bool&;

    auto SetNoiseSmoothingFactor(const float& factor)->void;
    auto GetNoiseSmoothingFactor()const->const float&;

    auto SetPSFStretchFactor(const float& factor)->void;
    auto GetPSFStretchFactor()const->const float&;

    auto SetPSFWaistRadius(const float& radius)->void;
    auto GetPSFWaistRadius()const->const float&;

    auto SetGoldGaussianInterval(const int32_t& interval)->void;
    auto GetGoldGaussianInterval()const->const int32_t&;

    auto SetGoldGaussianFWHM(const float& fwhm)->void;
    auto GetGoldGaussianFWHM()const->const float&;

    auto SetEnableClassicConfocal(const bool& enable)->void;
    auto GetEnableClassicConfocal()const->const bool&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};