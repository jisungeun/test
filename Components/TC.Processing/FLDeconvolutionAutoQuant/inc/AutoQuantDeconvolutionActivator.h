#pragma once

#include <memory>
#include <string>
#include "TCFLDeconvolutionAutoQuantExport.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolutionActivator {
public:
    typedef AutoQuantDeconvolutionActivator Self;
    typedef std::shared_ptr<Self> Pointer;

    static auto Unlock()->bool;
    static auto IsUnlocked()->bool;
    static auto SetRegistryKeyLocation(const std::string& registryKeyLocation)->void;

protected:
    AutoQuantDeconvolutionActivator();
    static auto GetInstance()->Pointer;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};