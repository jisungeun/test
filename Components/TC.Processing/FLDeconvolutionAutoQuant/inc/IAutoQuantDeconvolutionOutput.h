#pragma once

#include <memory>

#include "AutoQuantDeconvolutionResult.h"

#include "TCFLDeconvolutionAutoQuantExport.h"

class TCFLDeconvolutionAutoQuant_API IAutoQuantDeconvolutionOutput {
public:
    typedef std::shared_ptr<IAutoQuantDeconvolutionOutput> Pointer;

    virtual ~IAutoQuantDeconvolutionOutput() = default;
    virtual auto SetAutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& result)->void = 0;
};