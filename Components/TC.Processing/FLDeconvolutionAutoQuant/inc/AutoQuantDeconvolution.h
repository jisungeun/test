#pragma once

#include <memory>

#include "AutoQuantDeconvolutionInput.h"
#include "AutoQuantDeconvolutionOptions.h"
#include "IAutoQuantDeconvolutionOutput.h"

#include "TCFLDeconvolutionAutoQuantExport.h"

class TCFLDeconvolutionAutoQuant_API AutoQuantDeconvolution {
public:
    AutoQuantDeconvolution();
    ~AutoQuantDeconvolution();

    auto SetOutputPort(const IAutoQuantDeconvolutionOutput::Pointer& outputPort)->void;
    auto SetInput(const AutoQuantDeconvolutionInput& input)->void;
    auto SetOption(const AutoQuantDeconvolutionOptions& option)->void;

    auto Deconvolute()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};