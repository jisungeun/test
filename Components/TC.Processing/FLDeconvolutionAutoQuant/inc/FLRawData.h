#pragma once

#include <memory>
#include "TCFLDeconvolutionAutoQuantExport.h"
#include "FLMemoryOrder.h"

class TCFLDeconvolutionAutoQuant_API FLRawData {
public:
    FLRawData();
    FLRawData(const FLRawData& other);
    ~FLRawData();

    auto operator=(const FLRawData& other)->FLRawData&;

    auto SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetData(const std::shared_ptr<uint8_t[]>& data, const FLMemoryOrder& memoryOrder)->void;

    auto GetSizeX()const->const int32_t&;
    auto GetSizeY()const->const int32_t&;
    auto GetSizeZ()const->const int32_t&;

    auto GetData(const FLMemoryOrder& memoryOrder)const->std::shared_ptr<uint8_t[]>;
    
private:
    class Impl;
    std::unique_ptr<Impl> d;
};