#include <QSettings>
#include <QFile>

#include "AutoQuantDeconvolutionConfig.h"

AutoQuantDeconvolutionConfig::AutoQuantDeconvolutionConfig() {
    Init();
}

AutoQuantDeconvolutionConfig::AutoQuantDeconvolutionConfig(const QString& strPath) {
    Init();
    Load(strPath);
}

AutoQuantDeconvolutionConfig::~AutoQuantDeconvolutionConfig() {
}

void AutoQuantDeconvolutionConfig::Init(void) {
    m_fSampleDepth = 3.0f;

    m_nImageFirstGuess = ImageFirstGuess::FILTERED_ORIGINAL_DATA;
    m_nPSFFirstGuess = PSFFirstGuess::THEORETICAL_ESTIMATE;
    m_nFrequencyConstraint = FrequencyConstraint::AUTOMATIC;
    m_nSubvolumeCalculation = SubvolumeCalculation::DYNAMIC;
    m_nPSFGenerationAlgorithm = PSFGenerationAlgorithm::GIBSON_LANNI;
    m_nDeconvolutionMethod = DeconvolutionMethod::EXPECTATION_MAXIMIZATION;

    for (int idx = 0; idx < 3; idx++) {
        m_nDarkCurrentCalculation[idx] = DarkCurrentCalculation::AUTOMATIC_CALCULATION;
        m_fDarkCurrent[idx] = 0.0f;
    }

    m_nNumberOfIteration = 10;
    m_nBinFactorXY = 1;
    m_nBinFactorZ = 1;
    m_nSaveInterval = 10;

    m_nGuardbandXY = 28;
    m_nGuardbandZ = 6;
    m_fPSFWaistRadius = 1.0f;
    m_fPSFStretchFactor = 1.0f;
    m_nSubvolumeOverlap = 28;
    m_fNoiseSmoothingFactor = 2.f;
    m_nGoldsGaussInterval = 3;
    m_fGoldsGaussFWHM = 1.0f;

    m_bGPUProcessing = true;
    m_bSubvolumeInXY = true;
    m_bSubvolumeInZ = false;
    m_bPerformIntensityCorrection = true;
    m_bEnablePSFConstraints = true;
    m_bEnableClassicConfocal = false;
}

bool AutoQuantDeconvolutionConfig::Save(const QString& strPath) {
    QSettings settings(strPath, QSettings::IniFormat);

    settings.beginGroup("Sample");
    {
        settings.setValue("Sample Depth", (double)m_fSampleDepth);
    }
    settings.endGroup();

    settings.beginGroup("Standard");
    {
        settings.setValue("Deconvolution Method", toString(m_nDeconvolutionMethod));

        QStringList dcCalc;
        for (int idx = 0; idx < 3; idx++) dcCalc << toString(m_nDarkCurrentCalculation[idx]);
        settings.setValue("Dark Current Calculation", dcCalc.join(","));

        QStringList dcVal;
        for (int idx = 0; idx < 3; idx++) dcVal << toString(m_fDarkCurrent[idx]);
        settings.setValue("Dark Current", dcVal.join(","));

        settings.setValue("Number Of Iteration", m_nNumberOfIteration);
        settings.setValue("Bin Factor XY", m_nBinFactorXY);
        settings.setValue("Bin Factor Z", m_nBinFactorZ);
        settings.setValue("Save Interval", m_nSaveInterval);
    }
    settings.endGroup();

    settings.beginGroup("Expert");
    {
        settings.setValue("Image First Guess", toString(m_nImageFirstGuess));
        settings.setValue("PSF First Guess", toString(m_nPSFFirstGuess));
        settings.setValue("Frequency Constraint", toString(m_nFrequencyConstraint));
        settings.setValue("Subvolume Calculation", toString(m_nSubvolumeCalculation));
        settings.setValue("PSF Generation Algorithm", toString(m_nPSFGenerationAlgorithm));

        settings.setValue("Guardband XY", m_nGuardbandXY);
        settings.setValue("Guardband Z", m_nGuardbandZ);
        settings.setValue("PSF Waist Radius", (double)m_fPSFWaistRadius);
        settings.setValue("PSF Stretch Factor", (double)m_fPSFStretchFactor);
        settings.setValue("Subvolume Overlap", m_nSubvolumeOverlap);
        settings.setValue("Noise Smoothing Factor", (double)m_fNoiseSmoothingFactor);
        settings.setValue("Golds Gauss Interval", m_nGoldsGaussInterval);
        settings.setValue("Golds Gauss FWHM", (double)m_fGoldsGaussFWHM);

        //settings.setValue("GPU Processing", m_bGPUProcessing); //always true...
        settings.setValue("Subvolume in XY", m_bSubvolumeInXY);
        settings.setValue("Subvolume in Z", m_bSubvolumeInZ);
        settings.setValue("Perform Intensity Correction", m_bPerformIntensityCorrection);
        settings.setValue("Enable PSF Constraints", m_bEnablePSFConstraints);
        settings.setValue("Enable Classic Confocal", m_bEnableClassicConfocal);
    }
    settings.endGroup();

    return true;
}

bool AutoQuantDeconvolutionConfig::Load(const QString& strPath) {
    if (!QFile::exists(strPath)) return false;

    QSettings settings(strPath, QSettings::IniFormat);

    settings.beginGroup("Sample");
    {
        m_fSampleDepth = settings.value("Sample Depth", 3.0f).toFloat();
    }
    settings.endGroup();

    settings.beginGroup("Standard");
    {
        m_nDeconvolutionMethod = ConvDeconvolutionMethod(settings.value("Deconvolution Method").toString());

        QString strValue = settings.value("Dark Current Calculation").toString();
        QStringList toks = strValue.split(",");
        if (toks.size() == 1) {
            for (int idx = 0; idx < 3; idx++)
                m_nDarkCurrentCalculation[idx] = ConvDarkCurrentCalculation(strValue);
        } else {
            const int items = toks.size();
            if (items != 3) return false;

            for (int idx = 0; idx < items; idx++)
                m_nDarkCurrentCalculation[idx] = ConvDarkCurrentCalculation(toks.at(idx));
        }

        strValue = settings.value("Dark Current").toString();
        toks = strValue.split(",");
        if (toks.size() == 1) {
            for (int idx = 0; idx < 3; idx++)
                m_fDarkCurrent[idx] = strValue.toFloat();
        } else {
            const int items = toks.size();
            if (items != 3) return false;

            for (int idx = 0; idx < items; idx++)
                m_fDarkCurrent[idx] = toks.at(idx).toFloat();
        }

        m_nNumberOfIteration = settings.value("Number Of Iteration").toInt();
        m_nBinFactorXY = settings.value("Bin Factor XY").toInt();
        m_nBinFactorZ = settings.value("Bin Factor Z").toInt();
        m_nSaveInterval = settings.value("Save Interval").toInt();
    }
    settings.endGroup();

    settings.beginGroup("Expert");
    {
        m_nImageFirstGuess = ConvImageFirstGuess(settings.value("Image First Guess").toString());
        m_nPSFFirstGuess = ConvPSFFirstGuess(settings.value("PSF First Guess").toString());
        m_nFrequencyConstraint = ConvFrequencyConstraint(settings.value("Frequency Constraint").toString());
        m_nSubvolumeCalculation = ConvSubvolumeCalculation(settings.value("Subvolume Calculation").toString());
        m_nPSFGenerationAlgorithm = ConvPSFGenerationAlgorithm(settings.value("PSF Generation Algorithm").toString());

        m_nGuardbandXY = settings.value("Guardband XY").toInt();
        m_nGuardbandZ = settings.value("Guardband Z").toInt();
        m_fPSFWaistRadius = settings.value("PSF Waist Radius").toFloat();
        m_fPSFStretchFactor = settings.value("PSF Stretch Factor").toFloat();
        m_nSubvolumeOverlap = settings.value("Subvolume Overlap").toInt();
        m_fNoiseSmoothingFactor = settings.value("Noise Smoothing Factor").toFloat();
        m_nGoldsGaussInterval = settings.value("Golds Gauss Interval").toInt();
        m_fGoldsGaussFWHM = settings.value("Golds Gauss FWHM").toFloat();

        //settings.value("GPU Processing", m_bGPUProcessing); //always true...
        m_bSubvolumeInXY = settings.value("Subvolume in XY").toBool();
        m_bSubvolumeInZ = settings.value("Subvolume in Z").toBool();
        m_bPerformIntensityCorrection = settings.value("Perform Intensity Correction").toBool();
        m_bEnablePSFConstraints = settings.value("Enable PSF Constraints").toBool();
        m_bEnableClassicConfocal = settings.value("Enable Classic Confocal").toBool();
    }
    settings.endGroup();

    //validation
    m_nSaveInterval = m_nNumberOfIteration;

    return true;
}

void AutoQuantDeconvolutionConfig::ClearOptions(void) {
}

QString AutoQuantDeconvolutionConfig::toString(ImageFirstGuess value) {
    QString strRet;

    switch (value) {
    case ImageFirstGuess::ORIGINAL_DATA:
        strRet = "Original Data";
        break;
    case ImageFirstGuess::FILTERED_ORIGINAL_DATA:
        strRet = "Filtered Original Data";
        break;
    case ImageFirstGuess::FLAT_SHEET_IMAGE:
        strRet = "Flat Sheet";
        break;
    case ImageFirstGuess::USER_INPUT_FIRST_GUESS:
        strRet = "User Input First Guess";
        break;
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::ImageFirstGuess AutoQuantDeconvolutionConfig::ConvImageFirstGuess(const QString& strVal) {
    if (toString(ImageFirstGuess::ORIGINAL_DATA).compare(strVal) == 0)
        return ImageFirstGuess::ORIGINAL_DATA;

    if (toString(ImageFirstGuess::FILTERED_ORIGINAL_DATA).compare(strVal) == 0)
        return ImageFirstGuess::FILTERED_ORIGINAL_DATA;

    if (toString(ImageFirstGuess::FLAT_SHEET_IMAGE) == 0)
        return ImageFirstGuess::FLAT_SHEET_IMAGE;

    if (toString(ImageFirstGuess::USER_INPUT_FIRST_GUESS) == 0)
        return ImageFirstGuess::USER_INPUT_FIRST_GUESS;

    return ImageFirstGuess::FILTERED_ORIGINAL_DATA;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::PSFFirstGuess value) {
    QString strRet;

    switch (value) {
    case PSFFirstGuess::THEORETICAL_ESTIMATE:
        return "Theoretical Estimate";
    case PSFFirstGuess::FLAT_SHEET_PSF:
        return "Flat Sheet";
    case PSFFirstGuess::AUTOCORRECTION:
        return "Autocorrection";
    case PSFFirstGuess::USER_INPUT_PSF:
        return "User Input PSF";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::PSFFirstGuess AutoQuantDeconvolutionConfig::ConvPSFFirstGuess(const QString& strVal) {
    if (toString(PSFFirstGuess::THEORETICAL_ESTIMATE).compare(strVal) == 0)
        return PSFFirstGuess::THEORETICAL_ESTIMATE;

    if (toString(PSFFirstGuess::FLAT_SHEET_PSF).compare(strVal) == 0)
        return PSFFirstGuess::FLAT_SHEET_PSF;

    if (toString(PSFFirstGuess::AUTOCORRECTION).compare(strVal) == 0)
        return PSFFirstGuess::AUTOCORRECTION;

    if (toString(PSFFirstGuess::USER_INPUT_PSF).compare(strVal) == 0)
        return PSFFirstGuess::USER_INPUT_PSF;

    return PSFFirstGuess::THEORETICAL_ESTIMATE;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::FrequencyConstraint value) {
    QString strRet;

    switch (value) {
    case FrequencyConstraint::AUTOMATIC:
        return "Automatic";
    case FrequencyConstraint::THEORETICAL_LIMIT:
        return "Theoretical Limit";
    case FrequencyConstraint::DETECTED_LIMIT:
        return "Detected Limit";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::FrequencyConstraint AutoQuantDeconvolutionConfig::ConvFrequencyConstraint(const QString& strVal) {
    if (toString(FrequencyConstraint::AUTOMATIC).compare(strVal) == 0)
        return FrequencyConstraint::AUTOMATIC;

    if (toString(FrequencyConstraint::THEORETICAL_LIMIT).compare(strVal) == 0)
        return FrequencyConstraint::THEORETICAL_LIMIT;

    if (toString(FrequencyConstraint::DETECTED_LIMIT).compare(strVal) == 0)
        return FrequencyConstraint::DETECTED_LIMIT;

    return FrequencyConstraint::AUTOMATIC;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::SubvolumeCalculation value) {
    QString strRet;

    switch (value) {
    case SubvolumeCalculation::STATIC:
        return "Static";
    case SubvolumeCalculation::DYNAMIC:
        return "Dynamic";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::SubvolumeCalculation AutoQuantDeconvolutionConfig::ConvSubvolumeCalculation(const QString& strVal) {
    if (toString(SubvolumeCalculation::STATIC).compare(strVal) == 0)
        return SubvolumeCalculation::STATIC;

    if (toString(SubvolumeCalculation::DYNAMIC).compare(strVal) == 0)
        return SubvolumeCalculation::DYNAMIC;

    return SubvolumeCalculation::DYNAMIC;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::PSFGenerationAlgorithm value) {
    QString strRet;

    switch (value) {
    case PSFGenerationAlgorithm::AUTOQUANT:
        return "AutoQuant";
    case PSFGenerationAlgorithm::GIBSON_LANNI:
        return "Gibson-Lanni";
    case PSFGenerationAlgorithm::ZERNIKE:
        return "Zernike";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::PSFGenerationAlgorithm AutoQuantDeconvolutionConfig::ConvPSFGenerationAlgorithm(const QString& strVal) {
    if (toString(PSFGenerationAlgorithm::AUTOQUANT).compare(strVal) == 0)
        return PSFGenerationAlgorithm::AUTOQUANT;

    if (toString(PSFGenerationAlgorithm::GIBSON_LANNI).compare(strVal) == 0)
        return PSFGenerationAlgorithm::GIBSON_LANNI;

    if (toString(PSFGenerationAlgorithm::ZERNIKE).compare(strVal) == 0)
        return PSFGenerationAlgorithm::ZERNIKE;

    return PSFGenerationAlgorithm::ZERNIKE;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::DeconvolutionMethod value) {
    QString strRet;

    switch (value) {
    case DeconvolutionMethod::EXPECTATION_MAXIMIZATION:
        return "Expectation Maximization";
    case DeconvolutionMethod::POWER_ACCELERATION:
        return "Power Acceleration";
    case DeconvolutionMethod::EXTRAPOLATION_ACCELERATION:
        return "Extrapolation Acceleration";
    case DeconvolutionMethod::GOLDS_METHOD:
        return "Golds Method";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::DeconvolutionMethod AutoQuantDeconvolutionConfig::ConvDeconvolutionMethod(const QString& strVal) {
    if (toString(DeconvolutionMethod::EXPECTATION_MAXIMIZATION).compare(strVal) == 0)
        return DeconvolutionMethod::EXPECTATION_MAXIMIZATION;

    if (toString(DeconvolutionMethod::POWER_ACCELERATION).compare(strVal) == 0)
        return DeconvolutionMethod::POWER_ACCELERATION;

    if (toString(DeconvolutionMethod::EXTRAPOLATION_ACCELERATION).compare(strVal) == 0)
        return DeconvolutionMethod::EXTRAPOLATION_ACCELERATION;

    if (toString(DeconvolutionMethod::GOLDS_METHOD).compare(strVal) == 0)
        return DeconvolutionMethod::GOLDS_METHOD;

    return DeconvolutionMethod::EXPECTATION_MAXIMIZATION;
}

QString AutoQuantDeconvolutionConfig::toString(AutoQuantDeconvolutionConfig::DarkCurrentCalculation value) {
    QString strRet;

    switch (value) {
    case DarkCurrentCalculation::AUTOMATIC_CALCULATION:
        return "Automatic Calculation";
    case DarkCurrentCalculation::MANUAL_INPUT:
        return "Manual Input";
    }

    return strRet;
}

AutoQuantDeconvolutionConfig::DarkCurrentCalculation AutoQuantDeconvolutionConfig::ConvDarkCurrentCalculation(const QString& strVal) {
    if (toString(DarkCurrentCalculation::AUTOMATIC_CALCULATION).compare(strVal) == 0)
        return DarkCurrentCalculation::AUTOMATIC_CALCULATION;

    if (toString(DarkCurrentCalculation::MANUAL_INPUT).compare(strVal) == 0)
        return DarkCurrentCalculation::MANUAL_INPUT;

    return DarkCurrentCalculation::AUTOMATIC_CALCULATION;
}

QString AutoQuantDeconvolutionConfig::toString(float value, int precision) {
    return QString::number(value, 'f', precision);
}
