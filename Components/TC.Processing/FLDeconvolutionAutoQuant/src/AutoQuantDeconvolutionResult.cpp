#include "AutoQuantDeconvolutionResult.h"

class AutoQuantDeconvolutionResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    FLDeconData data{};
    QString logFilePath{};
};

AutoQuantDeconvolutionResult::AutoQuantDeconvolutionResult() : d(new Impl()) {
}

AutoQuantDeconvolutionResult::AutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& other)
    : d(new Impl(*other.d)) {
}

AutoQuantDeconvolutionResult::~AutoQuantDeconvolutionResult() = default;

auto AutoQuantDeconvolutionResult::operator=(const AutoQuantDeconvolutionResult& other)
    -> AutoQuantDeconvolutionResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto AutoQuantDeconvolutionResult::SetFLDeconData(const FLDeconData& data) -> void {
    d->data = data;
}

auto AutoQuantDeconvolutionResult::GetFLDeconData() const -> const FLDeconData& {
    return d->data;
}
