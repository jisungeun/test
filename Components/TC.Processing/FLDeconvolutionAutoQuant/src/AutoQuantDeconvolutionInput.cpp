#include "AutoQuantDeconvolutionInput.h"

class AutoQuantDeconvolutionInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    FLRawData flRawData{};

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    float pixelWorldSizeZ{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Micrometer };

    float numericalAperture{};
    float mediumRI{};

    float emissionWaveLength{};
    LengthUnit emissionWaveLengthUnit{ LengthUnit::Micrometer };

    float sampleDepth{};
    LengthUnit sampleDepthUnit{ LengthUnit::Micrometer };

    QString tempFolderPath{};
};

AutoQuantDeconvolutionInput::AutoQuantDeconvolutionInput() : d(new Impl()) {
}

AutoQuantDeconvolutionInput::AutoQuantDeconvolutionInput(const AutoQuantDeconvolutionInput& other) : d(new Impl(*other.d)) {
}

AutoQuantDeconvolutionInput::~AutoQuantDeconvolutionInput() = default;

auto AutoQuantDeconvolutionInput::operator=(const AutoQuantDeconvolutionInput& other) -> AutoQuantDeconvolutionInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto AutoQuantDeconvolutionInput::SetFLRawData(const FLRawData& flRawData) -> void {
    d->flRawData = flRawData;
}

auto AutoQuantDeconvolutionInput::GetFLRawData() const -> const FLRawData& {
    return d->flRawData;
}

auto AutoQuantDeconvolutionInput::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const float& pixelWorldSizeZ, const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeZ = pixelWorldSizeZ;
    d->pixelWorldSizeUnit = unit;
}

auto AutoQuantDeconvolutionInput::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit);
}

auto AutoQuantDeconvolutionInput::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit);
}

auto AutoQuantDeconvolutionInput::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
    return ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit);
}

auto AutoQuantDeconvolutionInput::SetNumericalAperture(const float& numericalAperture) -> void {
    d->numericalAperture = numericalAperture;
}

auto AutoQuantDeconvolutionInput::GetNumericalAperture() const -> const float& {
    return d->numericalAperture;
}

auto AutoQuantDeconvolutionInput::SetMediumRI(const float& mediumRI) -> void {
    d->mediumRI = mediumRI;
}

auto AutoQuantDeconvolutionInput::GetMediumRI() const -> const float& {
    return d->mediumRI;
}

auto AutoQuantDeconvolutionInput::SetEmissionWaveLength(const float& emissionWaveLength, const LengthUnit& unit)
    -> void {
    d->emissionWaveLength = emissionWaveLength;
    d->emissionWaveLengthUnit = unit;
}

auto AutoQuantDeconvolutionInput::GetEmissionWaveLength(const LengthUnit& unit) const -> float {
    return ConvertUnit(d->emissionWaveLength, d->emissionWaveLengthUnit, unit);
}

auto AutoQuantDeconvolutionInput::SetSampleDepth(const float& sampleDepth, const LengthUnit& unit) -> void {
    d->sampleDepth = sampleDepth;
    d->sampleDepthUnit = unit;
}

auto AutoQuantDeconvolutionInput::GetSampleDepth(const LengthUnit& unit) const -> float {
    return ConvertUnit(d->sampleDepth, d->sampleDepthUnit, unit);
}

auto AutoQuantDeconvolutionInput::SetTempFolderPath(const QString& tempFolderPath) -> void {
    d->tempFolderPath = tempFolderPath;
}

auto AutoQuantDeconvolutionInput::GetTempFolderPath() const -> const QString& {
    return d->tempFolderPath;
}
