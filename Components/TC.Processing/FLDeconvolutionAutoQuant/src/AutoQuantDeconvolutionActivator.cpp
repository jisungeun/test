#include "AutoQuantDeconvolutionActivator.h"

#include "aqiStatusCodes.h"
#include "aqiCommonInit.h"
#include "aqi3dDecon.h"

constexpr auto REG_INIT_VAL = "3";
constexpr auto REG_DECON_VAL = "0";

#ifdef HL
#undef HL
#endif
#define HL(a)   ((a) + 0x50)

#ifdef UNHIDE_STRING
#undef UNHIDE_STRING
#endif
#define UNHIDE_STRING(strI, strC) do {		\
	int* ptrI = strI;						\
	char* ptrC = strC;						\
	while (*ptrI) {							\
		*ptrC++ = (char)((*ptrI++)-0x50);	\
	}									\
 } while(0)

#ifdef HIDE_STRING
#undef HIDE_STRING
#endif
#define HIDE_STRING(str)  do {char * ptr = str ; while (*ptr) *ptr++ = '0';} while(0)

class AutoQuantDeconvolutionActivator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    bool unlocked{ false };
    std::string registryKeyLocation{};

    auto UnlockCommonInitDll()->bool;
    auto UnlockDeconvolutionDll()->bool;
    auto SetRegistryKeyLocation(const std::string& registryKeyLocation)->void;
};

auto AutoQuantDeconvolutionActivator::Impl::UnlockCommonInitDll() -> bool {
    //2812 F100 98CE 42B1 0146 5000 5482 9622
    int32_t initialProgramIntegerKey[] = {
        HL('2'), HL('8'), HL('1'), HL('2'),
        HL('F'), HL('1'), HL('0'), HL('0'),
        HL('9'), HL('8'), HL('C'), HL('E'),
        HL('4'), HL('2'), HL('B'), HL('1'),
        HL('0'), HL('1'), HL('4'), HL('6'),
        HL('5'), HL('0'), HL('0'), HL('0'),
        HL('5'), HL('4'), HL('8'), HL('2'),
        HL('9'), HL('6'), HL('2'), HL('2'),
        0 };

    char initialProgramCharKey[33] = {0, };

    UNHIDE_STRING(initialProgramIntegerKey, initialProgramCharKey);
    if (AQI_STATUS_DONE != aqiUnlockCommonInitDll(initialProgramCharKey, this->registryKeyLocation.c_str(), REG_INIT_VAL)) {
        HIDE_STRING(initialProgramCharKey);
        return false;
    }

    HIDE_STRING(initialProgramCharKey);
    return true;
}

auto AutoQuantDeconvolutionActivator::Impl::UnlockDeconvolutionDll() -> bool {
    //701B D040 9C1E 02A1 0756 2004 1083 1622
    int32_t deconvolutionProgramIntegerKey[] = {
        HL('7'), HL('0'), HL('1'), HL('B'),
        HL('D'), HL('0'), HL('4'), HL('0'),
        HL('9'), HL('C'), HL('1'), HL('E'),
        HL('0'), HL('2'), HL('A'), HL('1'),
        HL('0'), HL('7'), HL('5'), HL('6'),
        HL('2'), HL('0'), HL('0'), HL('4'),
        HL('1'), HL('0'), HL('8'), HL('3'),
        HL('1'), HL('6'), HL('2'), HL('2'),
        0 };

    char deconvolutionProgramCharKey[33] = {0, };

    UNHIDE_STRING(deconvolutionProgramIntegerKey, deconvolutionProgramCharKey);
    if (AQI_STATUS_DONE != aqiUnlock3dBlindDeconDll(deconvolutionProgramCharKey, this->registryKeyLocation.c_str(), REG_DECON_VAL)) {
        HIDE_STRING(deconvolutionProgramCharKey);
        return false;
    }

    HIDE_STRING(deconvolutionProgramCharKey);
    return true;
}

auto AutoQuantDeconvolutionActivator::Impl::SetRegistryKeyLocation(const std::string& registryKeyLocation) -> void {
    this->registryKeyLocation = registryKeyLocation;
}

AutoQuantDeconvolutionActivator::AutoQuantDeconvolutionActivator() : d(new Impl()) {
}

auto AutoQuantDeconvolutionActivator::GetInstance() ->Pointer {
    static Pointer theInstance(new AutoQuantDeconvolutionActivator);
    return theInstance;
}

auto AutoQuantDeconvolutionActivator::Unlock()->bool {
    const auto theInstance = GetInstance();
    if (theInstance->d->unlocked) return true;

    if (!theInstance->d->UnlockCommonInitDll()) {
        //"Failed to init the deconvolution module";
        return false;
    }

    if (!theInstance->d->UnlockDeconvolutionDll()) {
        //"Failed to unlock the deconvolution module";
        return false;
    }
    theInstance->d->unlocked = true;

    return true;
}

bool AutoQuantDeconvolutionActivator::IsUnlocked(void) {
    const Pointer theInstance = GetInstance();
    return theInstance->d->unlocked;
}

auto AutoQuantDeconvolutionActivator::SetRegistryKeyLocation(const std::string& registryKeyLocation) -> void {
    const Pointer theInstance = GetInstance();
    theInstance->d->SetRegistryKeyLocation(registryKeyLocation);
}
