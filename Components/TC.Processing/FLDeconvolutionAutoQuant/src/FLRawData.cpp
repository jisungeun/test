#include "FLRawData.h"

class FLRawData::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};

    std::shared_ptr<uint8_t[]> data{};
    FLMemoryOrder memoryOrder{ FLMemoryOrder::YXZ };

    auto ConvertMemoryOrder(const FLMemoryOrder& from, const FLMemoryOrder& to)->std::shared_ptr<uint8_t[]>;
};

auto FLRawData::Impl::ConvertMemoryOrder(const FLMemoryOrder& from, const FLMemoryOrder& to) -> std::shared_ptr<uint8_t[]> {
    const auto numberOfElements = this->sizeX * this->sizeY * this->sizeZ;
    std::shared_ptr<uint8_t[]> convertedData{ new uint8_t[numberOfElements]() };

    if ((from == +FLMemoryOrder::XYZ) && (to == +FLMemoryOrder::YXZ)) {
        for (auto indexX = 0; indexX < this->sizeX; ++indexX) {
            for (auto indexY = 0; indexY < this->sizeY; ++indexY) {
                for (auto indexZ = 0; indexZ < this->sizeZ; ++indexZ) {
                    const auto fromIndex = indexX + indexY * this->sizeX + indexZ * this->sizeX * this->sizeY;
                    const auto toIndex = indexY + indexX * this->sizeY + indexZ * this->sizeX * this->sizeY;

                    convertedData.get()[toIndex] = this->data.get()[fromIndex];
                }
            }
        }
    } else if ((from == +FLMemoryOrder::YXZ) && (to == +FLMemoryOrder::XYZ)) {
        for (auto indexX = 0; indexX < this->sizeX; ++indexX) {
            for (auto indexY = 0; indexY < this->sizeY; ++indexY) {
                for (auto indexZ = 0; indexZ < this->sizeZ; ++indexZ) {
                    const auto fromIndex = indexY + indexX * this->sizeX + indexZ * this->sizeX * this->sizeY;
                    const auto toIndex = indexX + indexY * this->sizeY + indexZ * this->sizeX * this->sizeY;

                    convertedData.get()[toIndex] = this->data.get()[fromIndex];
                }
            }
        }
    }

    return convertedData;
}

FLRawData::FLRawData() : d(new Impl()) {
}

FLRawData::FLRawData(const FLRawData& other) : d(new Impl(*other.d)) {
}

FLRawData::~FLRawData() = default;

auto FLRawData::operator=(const FLRawData& other) -> FLRawData& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLRawData::SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;
}

auto FLRawData::SetData(const std::shared_ptr<uint8_t[]>& data, const FLMemoryOrder& memoryOrder) -> void {
    d->data = data;
    d->memoryOrder = memoryOrder;
}

auto FLRawData::GetSizeX() const -> const int32_t& {
    return d->sizeX;
}

auto FLRawData::GetSizeY() const -> const int32_t& {
    return d->sizeY;
}

auto FLRawData::GetSizeZ() const -> const int32_t& {
    return d->sizeZ;
}

auto FLRawData::GetData(const FLMemoryOrder& memoryOrder) const -> std::shared_ptr<uint8_t[]> {
    if (d->memoryOrder == memoryOrder) {
        return d->data;
    } else {
        return d->ConvertMemoryOrder(d->memoryOrder, memoryOrder);
    }
}
