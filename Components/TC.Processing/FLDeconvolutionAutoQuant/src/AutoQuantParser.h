#pragma once

#include <memory>

#include "AutoQuantDeconvolutionOptions.h"
#include "AutoQuantDeconvolutionInput.h"

#include "aqiDataInfoStructs.h"
#include "aqiOptionStructs.h"

class AutoQuantParser {
public:
    AutoQuantParser();
    ~AutoQuantParser();

    auto SetInput(const AutoQuantDeconvolutionInput& input)->void;
    auto SetOption(const AutoQuantDeconvolutionOptions& option)->void;

    auto GetImageInfo()const->aqiStructImgInfo;
    auto GetPsfInInfo()const->aqiStructPsfInfo;
    auto GetPsfOutInfo()const->aqiStructPsfInfo;
    auto GetDeconOptionsStandard()const->aqiStructDeconOpsStd;
    auto GetDeconOptionsExpert()const->aqiStructDeconOpsExp;

    auto GetInputWritingFilePath()const->QString;
    auto GetOutputWritingFilePath()const->QString;
    auto GetLogFilePath()const->QString;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};