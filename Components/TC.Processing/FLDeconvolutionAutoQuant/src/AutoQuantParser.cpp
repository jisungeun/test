#include "AutoQuantParser.h"

#include <fstream>
#include <QDir>

#include <QString>
#include <QMap>

#include "AutoQuantOptionsEnum.h"
#include "aqiEnumerations.h"

static QMap<Modality, enumModalities> modalityMap {
    {Modality::Invalid, MOD_INVALID_VALUE},
    {Modality::Confocal, MOD_CONFOCAL},
    {Modality::SpinDiskConfocal, MOD_SPIN_DISK_CF},
    {Modality::TwoPhoton, MOD_TWO_PHOTON},
    {Modality::WidefieldFluorescence, MOD_WF_FLUORESCENCE},
    {Modality::TransmittedLightBrightField, MOD_TL_BRIGHTFIELD},
    {Modality::IndoCyanineGreenFluorescence, MOD_SLO_ICG_FA},
    {Modality::ReflectedLight, MOD_SLO_REFLIGHT},
    {Modality::NonSpecific, MOD_GENERIC}
};

static QMap<DeconvolutionMethod, enumDeconMeths> deconMethodMap {
    {DeconvolutionMethod::ExpectationMaximization, DM_EXPECTATION_MAX},
    {DeconvolutionMethod::PowerEcceleration, DM_POWER_ACCEL},
    {DeconvolutionMethod::PSFExtrapolationAcceleration, DM_EXTRAPOLATION_ACCEL},
    {DeconvolutionMethod::GoldsMethod, DM_GOLDS_METHOD}
};
static QMap<DarkCurrentMethod, enumDarkCurMeths> darkCurrentMethodMap {
    {DarkCurrentMethod::AutoCalculation, DCM_AUTO_CALCULATION},
    {DarkCurrentMethod::ManualInput, DCM_MANUAL_INPUT}
};

static QMap<InitialImageGuessGenerationMethod, enumImgGuessMeths> imageGuessMethodMap {
    {InitialImageGuessGenerationMethod::OriginalData, IGM_ORIGINAL_DATA},
    {InitialImageGuessGenerationMethod::LinearFilteredOriginal, IGM_FILTERED_ORIGINAL},
    {InitialImageGuessGenerationMethod::ConstantValue, IGM_FLAT_SHEET},
    {InitialImageGuessGenerationMethod::UserInput, IGM_OBJECT_INPUT}
};

static QMap<PSFGuessMethod, enumPsfGuessMeths> psfGuessMethodMap {
    {PSFGuessMethod::TheoreticalEsitimation, PGM_THEORETICAL_EST},
    {PSFGuessMethod::ConstantValue, PGM_FLAT_SHEET},
    {PSFGuessMethod::Autocorrelation, PGM_AUTOCORRELATION},
    {PSFGuessMethod::UserInput, PGM_PSF_INPUT}
};

static QMap<FrequencyBandlimitDeterminationMethod, enumFreqConsMeths> frequencyBandMethodMap {
    {FrequencyBandlimitDeterminationMethod::AutoSelection, FCM_AUTO_SELECTION},
    {FrequencyBandlimitDeterminationMethod::TheoreticalLimit, FCM_THEORETICAL_LIMIT},
    {FrequencyBandlimitDeterminationMethod::DetectedLimit, FCM_DETECTED_LIMIT}
};

static QMap<SubVolumeCalculationMethod, enumSubVolMeths> subVolumeMethodMap {
    {SubVolumeCalculationMethod::Predetermined, SVM_STATIC},
    {SubVolumeCalculationMethod::Dynamic, SVM_DYNAMIC}
};

static QMap<TheoreticalPSFGenerationMethod, enumTheoreticalPsfGenMeths> theoreticalPsfGenMethodMap {
    {TheoreticalPSFGenerationMethod::LagacyAutoQuant, TPM_AUTOQUANT},
    {TheoreticalPSFGenerationMethod::GibsonLanni, TPM_GIBSON_LANNI},
    {TheoreticalPSFGenerationMethod::Zernike, TPM_ZERNIKE}
};

const QString inputFLFileName = "tempFLRawBinData";
const QString logFileName = "tempFLLog";
const QString outputFLFileName = "tempFLDeconData";

class AutoQuantParser::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    AutoQuantDeconvolutionInput input;
    AutoQuantDeconvolutionOptions option;

    auto WriteTempFolder()->void;
    auto GetInputWritingFilePath()->QString;
    auto WriteInputDataToFile(const QString& inputWritingFilePath) const ->void;

    auto GetTempLogFilePath() const ->QString;

    auto GetOutputWritingFilePath() const ->QString;
};

auto AutoQuantParser::Impl::WriteTempFolder() -> void {
    QDir().mkdir(this->input.GetTempFolderPath());
}

auto AutoQuantParser::Impl::GetInputWritingFilePath() -> QString {
    const auto& tempFolderPath = this->input.GetTempFolderPath();
    const auto inputWritingFilePath = tempFolderPath + "/" + inputFLFileName;
    return inputWritingFilePath;
}

auto AutoQuantParser::Impl::WriteInputDataToFile(const QString& inputWritingFilePath) const -> void {
    const auto& flRawData = this->input.GetFLRawData();

    const auto& data = flRawData.GetData(FLMemoryOrder::YXZ);
    const auto& sizeX = flRawData.GetSizeX();
    const auto& sizeY = flRawData.GetSizeY();
    const auto& sizeZ = flRawData.GetSizeZ();

    const auto numberOfElements = sizeX * sizeY * sizeZ;

    std::ofstream fileStream(inputWritingFilePath.toStdString(), std::ios::binary);
    fileStream.write(reinterpret_cast<const char*>(data.get()), numberOfElements);
    fileStream.close();
}

auto AutoQuantParser::Impl::GetTempLogFilePath() const -> QString {
    const auto& tempFolderPath = this->input.GetTempFolderPath();
    const auto logFilePath = tempFolderPath + "/" + logFileName;
    return logFilePath;
}

auto AutoQuantParser::Impl::GetOutputWritingFilePath() const -> QString {
    const auto& tempFolderPath = this->input.GetTempFolderPath();
    const auto outputWritingFilePath = tempFolderPath + "\\" + outputFLFileName;
    return outputWritingFilePath;
}

AutoQuantParser::AutoQuantParser() : d(new Impl()) {
}

AutoQuantParser::~AutoQuantParser() = default;

auto AutoQuantParser::SetInput(const AutoQuantDeconvolutionInput& input) -> void {
    d->input = input;
}

auto AutoQuantParser::SetOption(const AutoQuantDeconvolutionOptions& option) -> void {
    d->option = option;
}

auto AutoQuantParser::GetImageInfo() const -> aqiStructImgInfo {
    const auto inputWritingFilePath = d->GetInputWritingFilePath();
    d->WriteTempFolder();
    d->WriteInputDataToFile(inputWritingFilePath);

    const auto tempLogFilePath = d->GetTempLogFilePath();

    const auto imageFilePath = new char[inputWritingFilePath.length() + 1]();
    std::copy_n(inputWritingFilePath.toStdString().c_str(), inputWritingFilePath.length(), imageFilePath);

    const auto logFilePath = new char[tempLogFilePath.length() + 1]();
    std::copy_n(tempLogFilePath.toStdString().c_str(), tempLogFilePath.length(), logFilePath);

    aqiStructImgInfo imageInfo{};
    aqiInitStructImgInfo(&imageInfo);
    
    imageInfo.lpstrImgFile = imageFilePath;
    imageInfo.lpstrRealName = logFilePath;
    //imageInfo.lpbBadSlice = ;
    //imageInfo.lpfImgData = ;
    imageInfo.enDataType = DT_UINT_8BIT;
    imageInfo.enScopeModality = modalityMap[d->option.GetModality()];
    imageInfo.shWidth = static_cast<short>(d->input.GetFLRawData().GetSizeX());
    imageInfo.shHeight = static_cast<short>(d->input.GetFLRawData().GetSizeY());
    imageInfo.shNumSlices = static_cast<short>(d->input.GetFLRawData().GetSizeZ());
    //imageInfo.shTotalChannels = ;
    imageInfo.fPixelSizeX = d->input.GetPixelWorldSizeX(LengthUnit::Micrometer);
    imageInfo.fPixelSizeY = d->input.GetPixelWorldSizeY(LengthUnit::Micrometer);
    imageInfo.fPixelSizeZ = d->input.GetPixelWorldSizeZ(LengthUnit::Micrometer);
    imageInfo.fNumericAperture = d->input.GetNumericalAperture();
    imageInfo.fRefractiveIndex = d->input.GetMediumRI();
    imageInfo.fEmmWavelength = d->input.GetEmissionWaveLength(LengthUnit::Micrometer);
    //imageInfo.fSphereAberrationFactor = ;
    //imageInfo.fConfocalPinholeSize = ;
    //imageInfo.lpstrImgFirstGuess = ;
    //imageInfo.fEmbeddingRefractiveIndex = ;
    imageInfo.fSampleDepth = d->input.GetSampleDepth(LengthUnit::Micrometer);
    //imageInfo.fSphereAberrationFactorO2 = ;

    return imageInfo;
}

auto AutoQuantParser::GetPsfInInfo() const -> aqiStructPsfInfo {
    aqiStructPsfInfo psfInInfo;
    aqiInitStructPsfInfo(&psfInInfo);
    return psfInInfo;
}

auto AutoQuantParser::GetPsfOutInfo() const -> aqiStructPsfInfo {
    aqiStructPsfInfo psfOutInfo;
    aqiInitStructPsfInfo(&psfOutInfo);
    return psfOutInfo;
}

auto AutoQuantParser::GetDeconOptionsStandard() const -> aqiStructDeconOpsStd {
    aqiStructDeconOpsStd deconOptionsStandard;
    aqiInitStructDeconOpsStd(&deconOptionsStandard);

    const auto& outputFilePathString = d->GetOutputWritingFilePath();
    const auto& tempFolderPathString = d->input.GetTempFolderPath();

    const auto outputFilePath = new char[static_cast<size_t>(outputFilePathString.length()) + 1]();
    std::copy_n(outputFilePathString.toStdString().c_str(), outputFilePathString.length(), outputFilePath);

    const auto tempFolderPath = new char[static_cast<size_t>(tempFolderPathString.length()) + 1]();
    std::copy_n(tempFolderPathString.toStdString().c_str(), tempFolderPathString.length(), tempFolderPath);

    deconOptionsStandard.lpstrImgOutFile = outputFilePath;
    deconOptionsStandard.lpstrTempDir = tempFolderPath;
    deconOptionsStandard.enDeconMeth = deconMethodMap[d->option.GetDeconvolutionMethod()];
    deconOptionsStandard.enDarkCurMeth = darkCurrentMethodMap[d->option.GetDarkCurrentMethod()];
    deconOptionsStandard.fDarkCurrent = d->option.GetDarkCurrentValue();
    deconOptionsStandard.shNumIterations = static_cast<short>(d->option.GetNumberOfIteration());
    deconOptionsStandard.shSaveInterval = static_cast<short>(d->option.GetSaveInterval());
    deconOptionsStandard.shBinFactorXY = static_cast<short>(d->option.GetBinningFactorToXYPlane());
    deconOptionsStandard.shBinFactorZ = static_cast<short>(d->option.GetBinningFactorToZPlane());
    deconOptionsStandard.bEnableGpuProcessing = d->option.GetEnableGPUProcessing();

    return deconOptionsStandard;
}

auto AutoQuantParser::GetDeconOptionsExpert() const -> aqiStructDeconOpsExp {
    aqiStructDeconOpsExp deconOptionsExpert;
    aqiInitStructDeconOpsExp(&deconOptionsExpert);

    deconOptionsExpert.enImgGuessMeth = imageGuessMethodMap[d->option.GetInitialImageGuessGenerationMethod()];
    deconOptionsExpert.enPsfGuessMeth = psfGuessMethodMap[d->option.GetPSFGuessMethod()];
    deconOptionsExpert.enFreqConsMeth = frequencyBandMethodMap[d->option.GetFrequencyBandlimitDeterminationMethod()];
    deconOptionsExpert.enSubVolMeth = subVolumeMethodMap[d->option.GetSubVolumeCalculationMethod()];
    deconOptionsExpert.shGuardBand = static_cast<short>(d->option.GetPaddingSizeToXYBorder());
    deconOptionsExpert.shGuardBandZ = static_cast<short>(d->option.GetPaddingSizeToZBorder());
    deconOptionsExpert.shSubVolOverlap = static_cast<short>(d->option.GetSubVolumeOverlapSize());
    deconOptionsExpert.bMontageXY = d->option.GetEnableSubVolumeInXY();
    deconOptionsExpert.bMontageZ = d->option.GetEnableSubVolumeInZ();
    //deconOptionsExpert.bDetectBlanks = ;
    //deconOptionsExpert.bSuppressNoise = ;
    //deconOptionsExpert.bDenseSample = ;
    deconOptionsExpert.bEnablePsfCons = d->option.GetEnablePSFConstraint();
    //deconOptionsExpert.bEnableGuidedDecon = ;
    //deconOptionsExpert.bSuppressMessages = ;
    deconOptionsExpert.fSuppressNoiseFactor = d->option.GetNoiseSmoothingFactor();
    deconOptionsExpert.fPsfStretchFactor = d->option.GetPSFStretchFactor();
    deconOptionsExpert.fPsfCentralRadius = d->option.GetPSFWaistRadius();
    deconOptionsExpert.shGoldsSmoothIteration = static_cast<short>(d->option.GetGoldGaussianInterval());
    deconOptionsExpert.fGoldsSmoothGauss = d->option.GetGoldGaussianFWHM();
    //deconOptionsExpert.shSubpixelXYFactor = ;
    //deconOptionsExpert.shSubpixelZFactor = ;
    //deconOptionsExpert.shIntensityCorrection = 1;
    //deconOptionsExpert.fMaxMemoryUsage = ;
    deconOptionsExpert.bEnableClassicConfocalAlgorithm = d->option.GetEnableClassicConfocal();
    deconOptionsExpert.enPsfGenMeth = theoreticalPsfGenMethodMap[d->option.GetTheoreticalPSFGenerationMethod()];

    return deconOptionsExpert;
}

auto AutoQuantParser::GetInputWritingFilePath() const -> QString {
    return d->GetInputWritingFilePath();
}

auto AutoQuantParser::GetOutputWritingFilePath() const -> QString {
    const auto tempFolderPath = d->input.GetTempFolderPath();
    const auto iterationNumber = d->option.GetNumberOfIteration();

    const auto outputWritingFilePath = 
        QString("%1/%2_%3").arg(tempFolderPath).arg(iterationNumber).arg(outputFLFileName);
    return outputWritingFilePath;
}

auto AutoQuantParser::GetLogFilePath() const -> QString {
    const auto tempLogFilePath = d->GetTempLogFilePath();
    const auto logFilePath = tempLogFilePath + ".log";

    return logFilePath;
}

