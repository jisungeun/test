#include "AutoQuantDeconvolutionParameter.h"

#include "aqiDataInfoStructs.h"
#include "aqiOptionStructs.h"
#include "aqiEnumerations.h"

class AutoQuantDeconvolutionParameter::Impl {
public:
    Impl() = default;
    ~Impl() {
        delete[] this->imgInfo.lpstrImgFile;
        delete[] this->imgInfo.lpstrRealName;
        delete[] this->psfInInfo.lpstrPsfFile;
        delete[] this->psfOutInfo.lpstrPsfFile;
        delete[] this->deconOpsStd.lpstrImgOutFile;
        delete[] this->deconOpsStd.lpstrTempDir;
    }

    aqiStructImgInfo imgInfo{};
    aqiStructPsfInfo psfInInfo{};
    aqiStructPsfInfo psfOutInfo{};
    aqiStructDeconOpsStd deconOpsStd{};
    aqiStructDeconOpsExp deconOpsExp{};

    DetectMethod m_nDetectMethod;
    bool m_bFastDeconvolution;

    auto Init()->void;
};

auto AutoQuantDeconvolutionParameter::Impl::Init() -> void {
    aqiInitStructImgInfo(&this->imgInfo);
    aqiInitStructPsfInfo(&this->psfInInfo);
    aqiInitStructPsfInfo(&this->psfOutInfo);
    aqiInitStructDeconOpsStd(&this->deconOpsStd);
    aqiInitStructDeconOpsExp(&this->deconOpsExp);

    constexpr int32_t stringMaximumLength = 512;

    // Allocate memory for the structures' strings
    this->imgInfo.lpstrImgFile = new char[stringMaximumLength]();
    this->imgInfo.lpstrRealName = new char[stringMaximumLength]();
    this->psfInInfo.lpstrPsfFile = new char[stringMaximumLength]();
    this->psfOutInfo.lpstrPsfFile = new char[stringMaximumLength]();
    this->deconOpsStd.lpstrImgOutFile = new char[stringMaximumLength]();
    this->deconOpsStd.lpstrTempDir = new char[stringMaximumLength]();

    this->m_nDetectMethod = DetectMethod::DETECT_SA;
    this->m_bFastDeconvolution = false;

    // PSF input information structure
    this->psfInInfo.lpstrPsfFile[0] = 0;
    this->psfInInfo.shWidth = 0;
    this->psfInInfo.shHeight = 0;
    this->psfInInfo.shNumSlices = 0;
    this->psfInInfo.fPixelSizeX = 0;
    this->psfInInfo.fPixelSizeY = 0;
    this->psfInInfo.fPixelSizeZ = 0;
    this->psfInInfo.enPsfSource = PSFSRC_CALCULATED;

    // PSF output information structure
    this->psfOutInfo.lpstrPsfFile[0] = 0;
}

AutoQuantDeconvolutionParameter::AutoQuantDeconvolutionParameter() : d(new Impl()) {
    d->Init();
}

AutoQuantDeconvolutionParameter::~AutoQuantDeconvolutionParameter() = default;

bool AutoQuantDeconvolutionParameter::LoadParameters(AutoQuantDeconvolutionConfig& config, int chidx) {
    // Sample information
    d->imgInfo.fSampleDepth = config.m_fSampleDepth;

    // Standard algorithm settings
    d->deconOpsStd.enDeconMeth = (enumDeconMeths)config.m_nDeconvolutionMethod;
    d->deconOpsStd.enDarkCurMeth = (enumDarkCurMeths)config.m_nDarkCurrentCalculation[chidx];
    d->deconOpsStd.fDarkCurrent = config.m_fDarkCurrent[chidx];
    d->deconOpsStd.shNumIterations = config.m_nNumberOfIteration;
    d->deconOpsStd.shSaveInterval = config.m_nSaveInterval;
    d->deconOpsStd.shBinFactorXY = config.m_nBinFactorXY;
    d->deconOpsStd.shBinFactorZ = config.m_nBinFactorZ;
    d->deconOpsStd.bEnableGpuProcessing = config.m_bGPUProcessing;

    // Expert algorithm settings
    d->deconOpsExp.enImgGuessMeth = (enumImgGuessMeths)config.m_nImageFirstGuess;
    d->deconOpsExp.enFreqConsMeth = (enumFreqConsMeths)config.m_nFrequencyConstraint;
    d->deconOpsExp.enPsfGuessMeth = (enumPsfGuessMeths)config.m_nPSFFirstGuess;
    d->deconOpsExp.enSubVolMeth = (enumSubVolMeths)config.m_nSubvolumeCalculation;
    d->deconOpsExp.shGuardBand = config.m_nGuardbandXY;
    d->deconOpsExp.shGuardBandZ = config.m_nGuardbandZ;
    d->deconOpsExp.shSubVolOverlap = config.m_nSubvolumeOverlap;
    d->deconOpsExp.bMontageXY = config.m_bSubvolumeInXY;
    d->deconOpsExp.bMontageZ = config.m_bSubvolumeInZ;
    d->deconOpsExp.bEnablePsfCons = config.m_bEnablePSFConstraints;
    d->deconOpsExp.bEnableClassicConfocalAlgorithm = config.m_bEnableClassicConfocal;
    d->deconOpsExp.fSuppressNoiseFactor = config.m_fNoiseSmoothingFactor;
    d->deconOpsExp.fPsfStretchFactor = config.m_fPSFStretchFactor;
    d->deconOpsExp.fPsfCentralRadius = config.m_fPSFWaistRadius;
    d->deconOpsExp.shGoldsSmoothIteration = config.m_nGoldsGaussInterval;
    d->deconOpsExp.fGoldsSmoothGauss = config.m_fGoldsGaussFWHM;
    d->deconOpsExp.enPsfGenMeth = (enumTheoreticalPsfGenMeths)config.m_nPSFGenerationAlgorithm;

    d->deconOpsExp.shIntensityCorrection = (config.m_bPerformIntensityCorrection) ? 1 : 0;

    return true;
}