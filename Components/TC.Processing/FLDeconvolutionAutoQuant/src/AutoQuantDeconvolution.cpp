#include <fstream>

#include "AutoQuantDeconvolution.h"

#include <aqi3dDecon.h>
#include <QFile>

#include "AutoQuantParser.h"

class AutoQuantDeconvolution::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    IAutoQuantDeconvolutionOutput::Pointer outputPort{};
    AutoQuantDeconvolutionInput input;
    AutoQuantDeconvolutionOptions option;

    auto ReadResultData(const QString& outputFilePath) const ->std::shared_ptr<float[]>;
};

auto AutoQuantDeconvolution::Impl::ReadResultData(const QString& outputFilePath) const -> std::shared_ptr<float[]> {
    const auto sizeX = this->input.GetFLRawData().GetSizeX();
    const auto sizeY = this->input.GetFLRawData().GetSizeY();
    const auto sizeZ = this->input.GetFLRawData().GetSizeZ();

    const auto numberOfElements = sizeX * sizeY * sizeZ;

    std::shared_ptr<float[]> outputData{ new float[numberOfElements]() };

    const auto byteCount = numberOfElements * 4;

    std::ifstream fileStream(outputFilePath.toStdString(), std::ios::binary);
    fileStream.read(reinterpret_cast<char*>(outputData.get()), byteCount);
    fileStream.close();

    return outputData;
}

AutoQuantDeconvolution::AutoQuantDeconvolution() : d(new Impl()) {
}

AutoQuantDeconvolution::~AutoQuantDeconvolution() = default;

auto AutoQuantDeconvolution::SetOutputPort(const IAutoQuantDeconvolutionOutput::Pointer& outputPort) -> void {
    d->outputPort = outputPort;
}

auto AutoQuantDeconvolution::SetInput(const AutoQuantDeconvolutionInput& input) -> void {
    d->input = input;
}

auto AutoQuantDeconvolution::SetOption(const AutoQuantDeconvolutionOptions& option) -> void {
    d->option = option;
}

auto AutoQuantDeconvolution::Deconvolute() -> bool {
    AutoQuantParser autoQuantParser;
    autoQuantParser.SetInput(d->input);
    autoQuantParser.SetOption(d->option);

    auto imageInfo = autoQuantParser.GetImageInfo();
    auto psfInInfo = autoQuantParser.GetPsfInInfo();
    auto psfOutInfo = autoQuantParser.GetPsfOutInfo();
    auto deconOptionStandard = autoQuantParser.GetDeconOptionsStandard();
    auto deconOptionExpert = autoQuantParser.GetDeconOptionsExpert();

    aqiStructDecon3dValidationFlags decon3dValidationFlags{};

    const auto validationResult =
        aqi3dDeconValidateInputs(&imageInfo, &psfInInfo, &psfOutInfo, &deconOptionStandard, &deconOptionExpert, &decon3dValidationFlags);

    if (validationResult != AQI_STATUS_NOERROR) {
        return false;
    }

    const auto resultStatus = 
        aqi3dBlindDeconvolution(&imageInfo, &psfInInfo, &psfOutInfo, &deconOptionStandard, &deconOptionExpert);

    delete[] imageInfo.lpstrImgFile;
    delete[] imageInfo.lpstrRealName;

    delete[] deconOptionStandard.lpstrImgOutFile;
    delete[] deconOptionStandard.lpstrTempDir;

    const auto inputWritingFilePath = autoQuantParser.GetInputWritingFilePath();
    QFile::remove(inputWritingFilePath);

    const auto sizeX = static_cast<int64_t>(d->input.GetFLRawData().GetSizeX());
    const auto sizeY = static_cast<int64_t>(d->input.GetFLRawData().GetSizeY());
    const auto sizeZ = static_cast<int64_t>(d->input.GetFLRawData().GetSizeZ());

    FLDeconData flDeconData;
    if (resultStatus == AQI_STATUS_DONE) {
        const auto outputWritingFilePath = autoQuantParser.GetOutputWritingFilePath();

        const auto resultData = d->ReadResultData(outputWritingFilePath);
        QFile::remove(outputWritingFilePath);

        flDeconData.SetData(resultData, FLMemoryOrder::YXZ);
        flDeconData.SetSize(sizeX, sizeY, sizeZ);

        const auto logFilePath = autoQuantParser.GetLogFilePath();
        if (!d->option.GetLogFilePath().isEmpty()) {
            QFile::rename(logFilePath, d->option.GetLogFilePath());
        }
    } else if (resultStatus == AQI_STATUS_BLANKCHANNEL) {
        const std::shared_ptr<float[]> resultData{ new float[sizeX * sizeY * sizeZ]() };

        const auto value = d->input.GetFLRawData().GetData(FLMemoryOrder::YXZ).get()[0];
        for (int64_t index = 0; index < sizeX * sizeY * sizeZ; ++index) {
            resultData.get()[index] = static_cast<float>(value);
        }

        flDeconData.SetData(resultData, FLMemoryOrder::YXZ);
        flDeconData.SetSize(sizeX, sizeY, sizeZ);
    } else {
        return false;
    }

    AutoQuantDeconvolutionResult result;
    result.SetFLDeconData(flDeconData);
    d->outputPort->SetAutoQuantDeconvolutionResult(result);

    return true;
}
