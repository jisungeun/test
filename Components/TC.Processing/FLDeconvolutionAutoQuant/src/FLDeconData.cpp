#include "FLDeconData.h"

class FLDeconData::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};

    std::shared_ptr<float[]> data{};
    FLMemoryOrder memoryOrder{ FLMemoryOrder::YXZ };

    auto ConvertMemoryOrder(const FLMemoryOrder& from, const FLMemoryOrder& to)->std::shared_ptr <float[]>;
};

auto FLDeconData::Impl::ConvertMemoryOrder(const FLMemoryOrder& from, const FLMemoryOrder& to)
    -> std::shared_ptr<float[]> {
    const auto numberOfElements = this->sizeX * this->sizeY * this->sizeZ;
    std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };

    if ((from == +FLMemoryOrder::XYZ) && (to == +FLMemoryOrder::YXZ)) {
        for (auto indexX = 0; indexX < this->sizeX; ++indexX) {
            for (auto indexY = 0; indexY < this->sizeY; ++indexY) {
                for (auto indexZ = 0; indexZ < this->sizeZ; ++indexZ) {
                    const auto fromIndex = indexX + indexY * this->sizeX + indexZ * this->sizeX * this->sizeY;
                    const auto toIndex = indexY + indexX * this->sizeY + indexZ * this->sizeX * this->sizeY;

                    convertedData.get()[toIndex] = this->data.get()[fromIndex];
                }
            }
        }
    } else if ((from == +FLMemoryOrder::YXZ) && (to == +FLMemoryOrder::XYZ)) {
        for (auto indexX = 0; indexX < this->sizeX; ++indexX) {
            for (auto indexY = 0; indexY < this->sizeY; ++indexY) {
                for (auto indexZ = 0; indexZ < this->sizeZ; ++indexZ) {
                    const auto fromIndex = indexY + indexX * this->sizeX + indexZ * this->sizeX * this->sizeY;
                    const auto toIndex = indexX + indexY * this->sizeY + indexZ * this->sizeX * this->sizeY;

                    convertedData.get()[toIndex] = this->data.get()[fromIndex];
                }
            }
        }
    }

    return convertedData;
}

FLDeconData::FLDeconData() : d(new Impl()) {
}

FLDeconData::FLDeconData(const FLDeconData& other) : d(new Impl(*other.d)) {
}

FLDeconData::~FLDeconData() = default;

auto FLDeconData::operator=(const FLDeconData & other) -> FLDeconData& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLDeconData::SetSize(const int32_t & sizeX, const int32_t & sizeY, const int32_t & sizeZ) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;
}

auto FLDeconData::SetData(const std::shared_ptr<float[]>&data, const FLMemoryOrder & memoryOrder) -> void {
    d->data = data;
    d->memoryOrder = memoryOrder;
}

auto FLDeconData::GetSizeX() const -> const int32_t& {
    return d->sizeX;
}

auto FLDeconData::GetSizeY() const -> const int32_t& {
    return d->sizeY;
}

auto FLDeconData::GetSizeZ() const -> const int32_t& {
    return d->sizeZ;
}

auto FLDeconData::GetData(const FLMemoryOrder & memoryOrder) const -> std::shared_ptr<float[]> {
    if (d->memoryOrder == memoryOrder) {
        return d->data;
    } else {
        return d->ConvertMemoryOrder(d->memoryOrder, memoryOrder);
    }
}
