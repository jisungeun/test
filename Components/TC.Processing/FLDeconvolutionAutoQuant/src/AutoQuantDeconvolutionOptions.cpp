#include "AutoQuantDeconvolutionOptions.h"

class AutoQuantDeconvolutionOptions::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    QString logFilePath{};

    Modality modality{ Modality::WidefieldFluorescence };
    DeconvolutionMethod deconvolutionMethod{ DeconvolutionMethod::ExpectationMaximization };
    DarkCurrentMethod darkCurrentMethod{ DarkCurrentMethod::AutoCalculation };
    InitialImageGuessGenerationMethod initialImageGuessGenerationMethod{ InitialImageGuessGenerationMethod::LinearFilteredOriginal };
    PSFGuessMethod psfGuessMethod{ PSFGuessMethod::TheoreticalEsitimation };
    FrequencyBandlimitDeterminationMethod frequencyBandlimitDeterminationMethod{ FrequencyBandlimitDeterminationMethod::AutoSelection };
    SubVolumeCalculationMethod subVolumeCalculationMethod{ SubVolumeCalculationMethod::Dynamic };
    TheoreticalPSFGenerationMethod theoreticalPsfGenerationMethod{ TheoreticalPSFGenerationMethod::GibsonLanni };

    float darkCurrentValue{ 0.f };
    int32_t numberOfIteration{ 10 };
    int32_t saveInterval{ 10 };
    int32_t binningFactorToXYPlane{ 1 };
    int32_t binningFactorToZPlane{ 1 };
    bool enableGPUProcessing{ true };

    int32_t paddingSizeToXYBorder{ 28 };
    int32_t paddingSizeToZBorder{ 6 };
    int32_t subVolumeOverlapSize{ 28 };
    bool enableSubVolumeInXY{ true };
    bool enableSubVolumeInZ{ false };
    bool enablePSFConstraint{ true };
    float noiseSmoothingFactor{ 2.0f };
    float psfStretchFactor{ 1.0f };
    float psfWaistRadius{ 1.0f };
    int32_t goldsGaussInterval{ 3 };
    float goldsGaussFWHM{ 1.0f };
    bool enableClassicConfocal{ false };
};

AutoQuantDeconvolutionOptions::AutoQuantDeconvolutionOptions() : d(new Impl()) {
}

AutoQuantDeconvolutionOptions::AutoQuantDeconvolutionOptions(const AutoQuantDeconvolutionOptions& other)
    : d(new Impl(*other.d)) {
}

AutoQuantDeconvolutionOptions::~AutoQuantDeconvolutionOptions() = default;

auto AutoQuantDeconvolutionOptions::operator=(const AutoQuantDeconvolutionOptions& other)
    -> AutoQuantDeconvolutionOptions& {
    *(this->d) = *(other.d);
    return *this;
}

auto AutoQuantDeconvolutionOptions::SetLogFilePath(const QString& logFilePath) -> void {
    d->logFilePath = logFilePath;
}

auto AutoQuantDeconvolutionOptions::GetLogFilePath() const -> const QString& {
    return d->logFilePath;
}

auto AutoQuantDeconvolutionOptions::SetModality(const Modality& modality) -> void {
    d->modality = modality;
}

auto AutoQuantDeconvolutionOptions::GetModality() const -> const Modality& {
    return d->modality;
}

auto AutoQuantDeconvolutionOptions::SetDeconvolutionMethod(const DeconvolutionMethod& method) -> void {
    d->deconvolutionMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetDeconvolutionMethod() const -> const DeconvolutionMethod& {
    return d->deconvolutionMethod;
}

auto AutoQuantDeconvolutionOptions::SetDarkCurrentMethod(const DarkCurrentMethod& method) -> void {
    d->darkCurrentMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetDarkCurrentMethod() const -> const DarkCurrentMethod& {
    return d->darkCurrentMethod;
}

auto AutoQuantDeconvolutionOptions::SetInitialImageGuessGenerationMethod(
    const InitialImageGuessGenerationMethod& method) -> void {
    d->initialImageGuessGenerationMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetInitialImageGuessGenerationMethod() const
    -> const InitialImageGuessGenerationMethod& {
    return d->initialImageGuessGenerationMethod;
}

auto AutoQuantDeconvolutionOptions::SetPSFGuessMethod(const PSFGuessMethod& method) -> void {
    d->psfGuessMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetPSFGuessMethod() const -> const PSFGuessMethod& {
    return d->psfGuessMethod;
}

auto AutoQuantDeconvolutionOptions::SetFrequencyBandlimitDeterminationMethod(
    const FrequencyBandlimitDeterminationMethod& method) -> void {
    d->frequencyBandlimitDeterminationMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetFrequencyBandlimitDeterminationMethod() const
    -> const FrequencyBandlimitDeterminationMethod& {
    return d->frequencyBandlimitDeterminationMethod;
}

auto AutoQuantDeconvolutionOptions::SetSubVolumeCalculationMethod(const SubVolumeCalculationMethod& method) -> void {
    d->subVolumeCalculationMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetSubVolumeCalculationMethod() const -> const SubVolumeCalculationMethod& {
    return d->subVolumeCalculationMethod;
}

auto AutoQuantDeconvolutionOptions::SetTheoreticalPSFGenerationMethod(const TheoreticalPSFGenerationMethod& method)
    -> void {
    d->theoreticalPsfGenerationMethod = method;
}

auto AutoQuantDeconvolutionOptions::GetTheoreticalPSFGenerationMethod() const
    -> const TheoreticalPSFGenerationMethod& {
    return d->theoreticalPsfGenerationMethod;
}

auto AutoQuantDeconvolutionOptions::SetDarkCurrentValue(const float& value) -> void {
    d->darkCurrentValue = value;
}

auto AutoQuantDeconvolutionOptions::GetDarkCurrentValue() const -> const float& {
    return d->darkCurrentValue;
}

auto AutoQuantDeconvolutionOptions::SetNumberOfIteration(const int32_t& numberOfIteration) -> void {
    d->numberOfIteration = numberOfIteration;
}

auto AutoQuantDeconvolutionOptions::GetNumberOfIteration() const -> const int32_t& {
    return d->numberOfIteration;
}

auto AutoQuantDeconvolutionOptions::SetSaveInterval(const int32_t& saveInterval) -> void {
    d->saveInterval = saveInterval;
}

auto AutoQuantDeconvolutionOptions::GetSaveInterval() const -> const int32_t& {
    return d->saveInterval;
}

auto AutoQuantDeconvolutionOptions::SetBinningFactorToXYPlane(const int32_t& factor) -> void {
    d->binningFactorToXYPlane = factor;
}

auto AutoQuantDeconvolutionOptions::GetBinningFactorToXYPlane() const -> const int32_t& {
    return d->binningFactorToXYPlane;
}

auto AutoQuantDeconvolutionOptions::SetBinningFactorToZPlane(const int32_t& factor) -> void {
    d->binningFactorToZPlane = factor;
}

auto AutoQuantDeconvolutionOptions::GetBinningFactorToZPlane() const -> const int32_t& {
    return d->binningFactorToZPlane;
}

auto AutoQuantDeconvolutionOptions::SetEnableGPUProcessing(const bool& enableGPUProcessing) -> void {
    d->enableGPUProcessing = enableGPUProcessing;
}

auto AutoQuantDeconvolutionOptions::GetEnableGPUProcessing() const -> const bool& {
    return d->enableGPUProcessing;
}

auto AutoQuantDeconvolutionOptions::SetPaddingSizeToXYBorder(const int32_t& paddingSize) -> void {
    d->paddingSizeToXYBorder = paddingSize;
}

auto AutoQuantDeconvolutionOptions::GetPaddingSizeToXYBorder() const -> const int32_t& {
    return d->paddingSizeToXYBorder;
}

auto AutoQuantDeconvolutionOptions::SetPaddingSizeToZBorder(const int32_t& paddingSize) -> void {
    d->paddingSizeToZBorder = paddingSize;
}

auto AutoQuantDeconvolutionOptions::GetPaddingSizeToZBorder() const -> const int32_t& {
    return  d->paddingSizeToZBorder;
}

auto AutoQuantDeconvolutionOptions::SetSubVolumeOverlapSize(const int32_t& overlapSize) -> void {
    d->subVolumeOverlapSize = overlapSize;
}

auto AutoQuantDeconvolutionOptions::GetSubVolumeOverlapSize() const -> const int32_t& {
    return d->subVolumeOverlapSize;
}

auto AutoQuantDeconvolutionOptions::SetEnableSubVolumeInXY(const bool& enable) -> void {
    d->enableSubVolumeInXY = enable;
}

auto AutoQuantDeconvolutionOptions::GetEnableSubVolumeInXY() const -> const bool& {
    return d->enableSubVolumeInXY;
}

auto AutoQuantDeconvolutionOptions::SetEnableSubVolumeInZ(const bool& enable) -> void {
    d->enableSubVolumeInZ = enable;
}

auto AutoQuantDeconvolutionOptions::GetEnableSubVolumeInZ() const -> const bool& {
    return d->enableSubVolumeInZ;
}

auto AutoQuantDeconvolutionOptions::SetEnablePSFConstraint(const bool& enable) -> void {
    d->enablePSFConstraint = enable;
}

auto AutoQuantDeconvolutionOptions::GetEnablePSFConstraint() const -> const bool& {
    return d->enablePSFConstraint;
}

auto AutoQuantDeconvolutionOptions::SetNoiseSmoothingFactor(const float& factor) -> void {
    d->noiseSmoothingFactor = factor;
}

auto AutoQuantDeconvolutionOptions::GetNoiseSmoothingFactor() const -> const float& {
    return d->noiseSmoothingFactor;
}

auto AutoQuantDeconvolutionOptions::SetPSFStretchFactor(const float& factor) -> void {
    d->psfStretchFactor = factor;
}

auto AutoQuantDeconvolutionOptions::GetPSFStretchFactor() const -> const float& {
    return d->psfStretchFactor;
}

auto AutoQuantDeconvolutionOptions::SetPSFWaistRadius(const float& radius) -> void {
    d->psfWaistRadius = radius;
}

auto AutoQuantDeconvolutionOptions::GetPSFWaistRadius() const -> const float& {
    return d->psfWaistRadius;
}

auto AutoQuantDeconvolutionOptions::SetGoldGaussianInterval(const int32_t& interval) -> void {
    d->goldsGaussInterval = interval;
}

auto AutoQuantDeconvolutionOptions::GetGoldGaussianInterval() const -> const int32_t& {
    return d->goldsGaussInterval;
}

auto AutoQuantDeconvolutionOptions::SetGoldGaussianFWHM(const float& fwhm) -> void {
    d->goldsGaussFWHM = fwhm;
}

auto AutoQuantDeconvolutionOptions::GetGoldGaussianFWHM() const -> const float& {
    return d->goldsGaussFWHM;
}

auto AutoQuantDeconvolutionOptions::SetEnableClassicConfocal(const bool& enable) -> void {
    d->enableClassicConfocal = enable;
}

auto AutoQuantDeconvolutionOptions::GetEnableClassicConfocal() const -> const bool& {
    return d->enableClassicConfocal;
}
