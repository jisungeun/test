#include "AutoQuantDeconvolutionOutput.h"

class AutoQuantDeconvolutionOutput::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    AutoQuantDeconvolutionResult result;
};

AutoQuantDeconvolutionOutput::AutoQuantDeconvolutionOutput() : d(new Impl()) {
}

AutoQuantDeconvolutionOutput::~AutoQuantDeconvolutionOutput() = default;

auto AutoQuantDeconvolutionOutput::SetAutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& result)
    -> void {
    d->result = result;
}

auto AutoQuantDeconvolutionOutput::GetDataMemory(const FLMemoryOrder& order) -> std::shared_ptr<float[]> {
    return d->result.GetFLDeconData().GetData(order);
}

auto AutoQuantDeconvolutionOutput::GetSizeX() const -> int32_t {
    return d->result.GetFLDeconData().GetSizeX();
}

auto AutoQuantDeconvolutionOutput::GetSizeY() const -> int32_t {
    return d->result.GetFLDeconData().GetSizeY();
}

auto AutoQuantDeconvolutionOutput::GetSizeZ() const -> int32_t {
    return d->result.GetFLDeconData().GetSizeZ();
}
