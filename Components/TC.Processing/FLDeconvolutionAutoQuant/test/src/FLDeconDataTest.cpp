#include <catch2/catch.hpp>

#include "FLDeconData.h"

namespace FLDeconDataTest {
    auto CompareArray(const float* array1, const float* array2, const size_t& arraySize) ->bool {
        for (size_t arrayIndex = 0; arrayIndex < arraySize; ++arrayIndex) {
            const auto value1 = array1[arrayIndex];
            const auto value2 = array2[arrayIndex];

            if (value1 != value2) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("FLDeconDataTest : unit test") {
        SECTION("FLDeconData()") {
            FLDeconData flDeconData;
            CHECK(&flDeconData != nullptr);
        }
        SECTION("FLDeconData(other)") {
            FLDeconData srcFLDeconData;
            srcFLDeconData.SetSize(1, 2, 3);

            FLDeconData destFLDeconData(srcFLDeconData);
            CHECK(destFLDeconData.GetSizeX() == 1);
            CHECK(destFLDeconData.GetSizeY() == 2);
            CHECK(destFLDeconData.GetSizeZ() == 3);
        }
        SECTION("operator=()") {
            FLDeconData srcFLDeconData;
            srcFLDeconData.SetSize(1, 2, 3);

            FLDeconData destFLDeconData;
            destFLDeconData = srcFLDeconData;
            CHECK(destFLDeconData.GetSizeX() == 1);
            CHECK(destFLDeconData.GetSizeY() == 2);
            CHECK(destFLDeconData.GetSizeZ() == 3);
        }
        SECTION("SetSize()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);
            CHECK(&flDeconData != nullptr);
        }
        SECTION("SetData()") {
            FLDeconData flDeconData;
            flDeconData.SetData({}, FLMemoryOrder::YXZ);
            CHECK(&flDeconData != nullptr);
        }
        SECTION("GetSizeX()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);
            CHECK(flDeconData.GetSizeX() == 1);
        }
        SECTION("GetSizeY()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);
            CHECK(flDeconData.GetSizeY() == 2);
        }
        SECTION("GetSizeZ()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);
            CHECK(flDeconData.GetSizeZ() == 3);
        }
        SECTION("GetData()") {
            constexpr int32_t sizeX = 3;
            constexpr int32_t sizeY = 3;
            constexpr int32_t sizeZ = 2;
            constexpr int32_t numberOfElements = sizeX * sizeY * sizeZ;

            constexpr float data[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };

            std::shared_ptr<float[]> inputData{ new float[numberOfElements]() };
            std::copy_n(data, numberOfElements, inputData.get());

            FLDeconData flDeconData;
            flDeconData.SetSize(sizeX, sizeY, sizeZ);
            SECTION("same memory order") {
                const auto& answer = data;
                flDeconData.SetData(inputData, FLMemoryOrder::XYZ);
                const auto resultData = flDeconData.GetData(FLMemoryOrder::XYZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
            SECTION("different memory order : XYZ -> YXZ") {
                constexpr float answer[numberOfElements] = { 1,4,7,2,5,8,3,6,9,10,13,16,11,14,17,12,15,18 };

                flDeconData.SetData(inputData, FLMemoryOrder::XYZ);
                const auto resultData = flDeconData.GetData(FLMemoryOrder::YXZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
            SECTION("differnet memory order : YXZ -> XYZ") {
                constexpr float answer[numberOfElements] = { 1,4,7,2,5,8,3,6,9,10,13,16,11,14,17,12,15,18 };
                flDeconData.SetData(inputData, FLMemoryOrder::YXZ);
                const auto resultData = flDeconData.GetData(FLMemoryOrder::XYZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
        }
    }

    TEST_CASE("FLDeconDataTest : practical test") {
        constexpr int32_t sizeX = 3;
        constexpr int32_t sizeY = 3;
        constexpr int32_t sizeZ = 2;
        constexpr int32_t numberOfElements = sizeX * sizeY * sizeZ;

        constexpr float data[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };

        std::shared_ptr<float[]> inputData{ new float[numberOfElements]() };
        std::copy_n(data, numberOfElements, inputData.get());

        FLDeconData flDeconData;
        flDeconData.SetSize(sizeX, sizeY, sizeZ);
        flDeconData.SetData(inputData, FLMemoryOrder::YXZ);
        const auto resultData = flDeconData.GetData(FLMemoryOrder::YXZ);

        CHECK(flDeconData.GetSizeX() == sizeX);
        CHECK(flDeconData.GetSizeY() == sizeY);
        CHECK(flDeconData.GetSizeZ() == sizeZ);
        CHECK(CompareArray(resultData.get(), data, numberOfElements));
    }
}