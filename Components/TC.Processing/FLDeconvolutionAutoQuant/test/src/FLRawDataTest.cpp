#include <catch2/catch.hpp>

#include "FLRawData.h"

namespace FLRawDataTest {
    auto CompareArray(const uint8_t* array1, const uint8_t* array2, const size_t& arraySize) ->bool {
        for (size_t arrayIndex = 0; arrayIndex < arraySize; ++arrayIndex) {
            const auto value1 = array1[arrayIndex];
            const auto value2 = array2[arrayIndex];

            if (value1 != value2) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("FLRawDataTest : unit test") {
        SECTION("FLRawData()") {
            FLRawData flRawData;
            CHECK(&flRawData != nullptr);
        }
        SECTION("FLRawData(other)") {
            FLRawData srcFLRawData;
            srcFLRawData.SetSize(1, 2, 3);

            FLRawData destFLRawData(srcFLRawData);
            CHECK(destFLRawData.GetSizeX() == 1);
            CHECK(destFLRawData.GetSizeY() == 2);
            CHECK(destFLRawData.GetSizeZ() == 3);
        }
        SECTION("operator=()") {
            FLRawData srcFLRawData;
            srcFLRawData.SetSize(1, 2, 3);

            FLRawData destFLRawData;
            destFLRawData = srcFLRawData;
            CHECK(destFLRawData.GetSizeX() == 1);
            CHECK(destFLRawData.GetSizeY() == 2);
            CHECK(destFLRawData.GetSizeZ() == 3);
        }
        SECTION("SetSize()") {
            FLRawData flRawData;
            flRawData.SetSize(1, 2, 3);
            CHECK(&flRawData != nullptr);
        }
        SECTION("SetData()") {
            FLRawData flRawData;
            flRawData.SetData({}, FLMemoryOrder::YXZ);
            CHECK(&flRawData != nullptr);
        }
        SECTION("GetSizeX()") {
            FLRawData flRawData;
            flRawData.SetSize(1, 2, 3);
            CHECK(flRawData.GetSizeX() == 1);
        }
        SECTION("GetSizeY()") {
            FLRawData flRawData;
            flRawData.SetSize(1, 2, 3);
            CHECK(flRawData.GetSizeY() == 2);
        }
        SECTION("GetSizeZ()") {
            FLRawData flRawData;
            flRawData.SetSize(1, 2, 3);
            CHECK(flRawData.GetSizeZ() == 3);
        }
        SECTION("GetData()") {
            constexpr int32_t sizeX = 3;
            constexpr int32_t sizeY = 3;
            constexpr int32_t sizeZ = 2;
            constexpr int32_t numberOfElements = sizeX * sizeY * sizeZ;

            constexpr uint8_t data[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };

            std::shared_ptr<uint8_t[]> inputData{ new uint8_t[numberOfElements]() };
            std::copy_n(data, numberOfElements, inputData.get());
            
            FLRawData flRawData;
            flRawData.SetSize(sizeX, sizeY, sizeZ);
            SECTION("same memory order") {
                const auto& answer = data;
                flRawData.SetData(inputData, FLMemoryOrder::XYZ);
                const auto resultData = flRawData.GetData(FLMemoryOrder::XYZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
            SECTION("different memory order : XYZ -> YXZ") {
                constexpr uint8_t answer[numberOfElements] = { 1,4,7,2,5,8,3,6,9,10,13,16,11,14,17,12,15,18 };

                flRawData.SetData(inputData, FLMemoryOrder::XYZ);
                const auto resultData = flRawData.GetData(FLMemoryOrder::YXZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
            SECTION("differnet memory order : YXZ -> XYZ") {
                constexpr uint8_t answer[numberOfElements] = { 1,4,7,2,5,8,3,6,9,10,13,16,11,14,17,12,15,18 };
                flRawData.SetData(inputData, FLMemoryOrder::YXZ);
                const auto resultData = flRawData.GetData(FLMemoryOrder::XYZ);
                CHECK(CompareArray(resultData.get(), answer, numberOfElements));
            }
        }
    }

    TEST_CASE("FLRawDataTest : practical test") {
        constexpr int32_t sizeX = 3;
        constexpr int32_t sizeY = 3;
        constexpr int32_t sizeZ = 2;
        constexpr int32_t numberOfElements = sizeX * sizeY * sizeZ;

        constexpr uint8_t data[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };

        std::shared_ptr<uint8_t[]> inputData{ new uint8_t[numberOfElements]() };
        std::copy_n(data, numberOfElements, inputData.get());

        FLRawData flRawData;
        flRawData.SetSize(sizeX, sizeY, sizeZ);
        flRawData.SetData(inputData, FLMemoryOrder::YXZ);
        const auto resultData = flRawData.GetData(FLMemoryOrder::YXZ);

        CHECK(flRawData.GetSizeX() == sizeX);
        CHECK(flRawData.GetSizeY() == sizeY);
        CHECK(flRawData.GetSizeZ() == sizeZ);
        CHECK(CompareArray(resultData.get(), data, numberOfElements));
    }
}