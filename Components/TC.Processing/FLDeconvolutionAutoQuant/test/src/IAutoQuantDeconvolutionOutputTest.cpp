#include <catch2/catch.hpp>

#include "IAutoQuantDeconvolutionOutput.h"

namespace IAutoQuantDeconvolutionOutputTest {
    class AutoQuantDeconvolutionOutputForTest final : public IAutoQuantDeconvolutionOutput {
    public:
        AutoQuantDeconvolutionOutputForTest() = default;
        ~AutoQuantDeconvolutionOutputForTest() = default;

        auto SetAutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& result) -> void override {
            setAutoQuantDeconvolutionResultTriggered = true;
        }

        bool setAutoQuantDeconvolutionResultTriggered{ false };
    };

    TEST_CASE("IAutoQuantDeconvolutionOutput : unitTest") {
        SECTION("SetAutoQuantDeconvolutionResult()") {
            AutoQuantDeconvolutionOutputForTest autoQuantDeconvolutionOutput;
            CHECK(autoQuantDeconvolutionOutput.setAutoQuantDeconvolutionResultTriggered == false);
            autoQuantDeconvolutionOutput.SetAutoQuantDeconvolutionResult({});
            CHECK(autoQuantDeconvolutionOutput.setAutoQuantDeconvolutionResultTriggered == true);
        }
    }
}