#include <catch2/catch.hpp>

#include "AutoQuantDeconvolutionOptions.h"

namespace AutoQuantDeconvolutionOptionsTest {
    TEST_CASE("AutoQuantDeconvolutionOptions : unit test") {
        SECTION("AutoQuantDeconvolutionOptions()") {
            AutoQuantDeconvolutionOptions options;
            CHECK(&options != nullptr);
        }
        SECTION("AutoQuantDeconvolutionOptions(other)") {
            AutoQuantDeconvolutionOptions srcOptions;
            srcOptions.SetLogFilePath("test");

            AutoQuantDeconvolutionOptions destOptions(srcOptions);
            CHECK(destOptions.GetLogFilePath() == "test");
        }
        SECTION("operator=()") {
            AutoQuantDeconvolutionOptions srcOptions;
            srcOptions.SetLogFilePath("test");

            AutoQuantDeconvolutionOptions destOptions;
            destOptions = srcOptions;
            CHECK(destOptions.GetLogFilePath() == "test");
        }
        SECTION("SetLogFilePath()") {
            AutoQuantDeconvolutionOptions options;
            options.SetLogFilePath("test");
            CHECK(&options != nullptr);
        }
        SECTION("GetLogFilePath()") {
            AutoQuantDeconvolutionOptions options;
            options.SetLogFilePath("test");
            CHECK(options.GetLogFilePath() == "test");
        }
        SECTION("SetModality()") {
            AutoQuantDeconvolutionOptions options;
            options.SetModality(Modality::Confocal);
            CHECK(&options != nullptr);
        }
        SECTION("GetModality()") {
            AutoQuantDeconvolutionOptions options;
            options.SetModality(Modality::Confocal);
            CHECK(options.GetModality() == +Modality::Confocal);
        }
        SECTION("SetDeconvolutionMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDeconvolutionMethod(DeconvolutionMethod::GoldsMethod);
            CHECK(&options != nullptr);
        }
        SECTION("GetDeconvolutionMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDeconvolutionMethod(DeconvolutionMethod::GoldsMethod);
            CHECK(options.GetDeconvolutionMethod() == +DeconvolutionMethod::GoldsMethod);
        }
        SECTION("SetDarkCurrentMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDarkCurrentMethod(DarkCurrentMethod::ManualInput);
            CHECK(&options != nullptr);
        }
        SECTION("GetDarkCurrentMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDarkCurrentMethod(DarkCurrentMethod::ManualInput);
            CHECK(options.GetDarkCurrentMethod() == +DarkCurrentMethod::ManualInput);
        }
        SECTION("SetInitialImageGuessGenerationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::UserInput);
            CHECK(&options != nullptr);
        }
        SECTION("GetInitialImageGuessGenerationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::UserInput);
            CHECK(options.GetInitialImageGuessGenerationMethod() == +InitialImageGuessGenerationMethod::UserInput);
        }
        SECTION("SetPSFGuessMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFGuessMethod(PSFGuessMethod::ConstantValue);
            CHECK(&options != nullptr);
        }
        SECTION("GetPSFGuessMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFGuessMethod(PSFGuessMethod::ConstantValue);
            CHECK(options.GetPSFGuessMethod() == +PSFGuessMethod::ConstantValue);
        }
        SECTION("SetFrequencyBandlimitDeterminationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::DetectedLimit);
            CHECK(&options != nullptr);
        }
        SECTION("GetFrequencyBandlimitDeterminationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::DetectedLimit);
            CHECK(options.GetFrequencyBandlimitDeterminationMethod() == +FrequencyBandlimitDeterminationMethod::DetectedLimit);
        }
        SECTION("SetSubVolumeCalculationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Predetermined);
            CHECK(&options != nullptr);
        }
        SECTION("GetSubVolumeCalculationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Predetermined);
            CHECK(options.GetSubVolumeCalculationMethod() == +SubVolumeCalculationMethod::Predetermined);
        }
        SECTION("SetTheoreticalPSFGenerationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::Zernike);
            CHECK(&options != nullptr);
        }
        SECTION("GetTheoreticalPSFGenerationMethod()") {
            AutoQuantDeconvolutionOptions options;
            options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::Zernike);
            CHECK(options.GetTheoreticalPSFGenerationMethod() == +TheoreticalPSFGenerationMethod::Zernike);
        }
        SECTION("SetDarkCurrentValue()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDarkCurrentValue(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetDarkCurrentValue()") {
            AutoQuantDeconvolutionOptions options;
            options.SetDarkCurrentValue(1.f);
            CHECK(options.GetDarkCurrentValue() == 1.f);
        }
        SECTION("SetNumberOfIteration()") {
            AutoQuantDeconvolutionOptions options;
            options.SetNumberOfIteration(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetNumberOfIteration()") {
            AutoQuantDeconvolutionOptions options;
            options.SetNumberOfIteration(1);
            CHECK(options.GetNumberOfIteration() == 1);
        }
        SECTION("SetSaveInterval()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSaveInterval(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetSaveInterval()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSaveInterval(1);
            CHECK(options.GetSaveInterval() == 1);
        }
        SECTION("SetBinningFactorToXYPlane()") {
            AutoQuantDeconvolutionOptions options;
            options.SetBinningFactorToXYPlane(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetBinningFactorToXYPlane()") {
            AutoQuantDeconvolutionOptions options;
            options.SetBinningFactorToXYPlane(1);
            CHECK(options.GetBinningFactorToXYPlane() == 1);
        }
        SECTION("SetBinningFactorToZPlane()") {
            AutoQuantDeconvolutionOptions options;
            options.SetBinningFactorToZPlane(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetBinningFactorToZPlane()") {
            AutoQuantDeconvolutionOptions options;
            options.SetBinningFactorToZPlane(1);
            CHECK(options.GetBinningFactorToZPlane() == 1);
        }
        SECTION("SetEnableGPUProcessing()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableGPUProcessing(false);
            CHECK(&options != nullptr);
        }
        SECTION("GetEnableGPUProcessing()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableGPUProcessing(false);
            CHECK(&options != nullptr);
        }
        SECTION("SetPaddingSizeToXYBorder()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPaddingSizeToXYBorder(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetPaddingSizeToXYBorder()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPaddingSizeToXYBorder(1);
            CHECK(options.GetPaddingSizeToXYBorder() == 1);
        }
        SECTION("SetPaddingSizeToZBorder()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPaddingSizeToZBorder(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetPaddingSizeToZBorder()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPaddingSizeToZBorder(1);
            CHECK(options.GetPaddingSizeToZBorder() == 1);
        }
        SECTION("SetSubVolumeOverlapSize()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSubVolumeOverlapSize(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetSubVolumeOverlapSize()") {
            AutoQuantDeconvolutionOptions options;
            options.SetSubVolumeOverlapSize(1);
            CHECK(options.GetSubVolumeOverlapSize() == 1);
        }
        SECTION("SetEnableSubVolumeInXY()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableSubVolumeInXY(true);
            CHECK(&options != nullptr);
        }
        SECTION("GetEnableSubVolumeInXY()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableSubVolumeInXY(true);
            CHECK(options.GetEnableSubVolumeInXY() == true);
        }
        SECTION("SetEnableSubVolumeInZ()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableSubVolumeInZ(true);
            CHECK(&options != nullptr);
        }
        SECTION("GetEnableSubVolumeInZ()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableSubVolumeInZ(true);
            CHECK(options.GetEnableSubVolumeInZ() == true);
        }
        SECTION("SetEnablePSFConstraint()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnablePSFConstraint(true);
            CHECK(&options != nullptr);
        }
        SECTION("GetEnablePSFConstraint()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnablePSFConstraint(true);
            CHECK(options.GetEnablePSFConstraint() == true);
        }
        SECTION("SetNoiseSmoothingFactor()") {
            AutoQuantDeconvolutionOptions options;
            options.SetNoiseSmoothingFactor(1.f);
            CHECK(&options != nullptr);
        }
        SECTION("GetNoiseSmoothingFactor()") {
            AutoQuantDeconvolutionOptions options;
            options.SetNoiseSmoothingFactor(1.f);
            CHECK(options.GetNoiseSmoothingFactor() == 1.f);
        }
        SECTION("SetPSFStretchFactor()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFStretchFactor(1.f);
            CHECK(&options != nullptr);
        }
        SECTION("GetPSFStretchFactor()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFStretchFactor(1.f);
            CHECK(options.GetPSFStretchFactor() == 1.f);
        }
        SECTION("SetPSFWaistRadius()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFWaistRadius(1.f);
            CHECK(&options != nullptr);
        }
        SECTION("GetPSFWaistRadius()") {
            AutoQuantDeconvolutionOptions options;
            options.SetPSFWaistRadius(1.f);
            CHECK(options.GetPSFWaistRadius() == 1.f);
        }
        SECTION("SetGoldGaussianInterval()") {
            AutoQuantDeconvolutionOptions options;
            options.SetGoldGaussianInterval(1);
            CHECK(&options != nullptr);
        }
        SECTION("GetGoldGaussianInterval()") {
            AutoQuantDeconvolutionOptions options;
            options.SetGoldGaussianInterval(1);
            CHECK(options.GetGoldGaussianInterval() == 1);
        }
        SECTION("SetGoldGaussianFWHM()") {
            AutoQuantDeconvolutionOptions options;
            options.SetGoldGaussianFWHM(1.f);
            CHECK(&options != nullptr);
        }
        SECTION("GetGoldGaussianFWHM()") {
            AutoQuantDeconvolutionOptions options;
            options.SetGoldGaussianFWHM(1.f);
            CHECK(options.GetGoldGaussianFWHM() == 1.f);
        }
        SECTION("SetEnableClassicConfocal()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableClassicConfocal(true);
            CHECK(&options != nullptr);
        }
        SECTION("GetEnableClassicConfocal()") {
            AutoQuantDeconvolutionOptions options;
            options.SetEnableClassicConfocal(true);
            CHECK(options.GetEnableClassicConfocal() == true);
        }
    }
    TEST_CASE("AutoQuantDeconvolutionOptions : practical test") {
        SECTION("default") {
            AutoQuantDeconvolutionOptions options;
            CHECK(options.GetLogFilePath() == "");
            CHECK(options.GetModality() == +Modality::WidefieldFluorescence);
            CHECK(options.GetDeconvolutionMethod() == +DeconvolutionMethod::ExpectationMaximization);
            CHECK(options.GetDarkCurrentMethod() == +DarkCurrentMethod::AutoCalculation);
            CHECK(options.GetInitialImageGuessGenerationMethod() == +InitialImageGuessGenerationMethod::LinearFilteredOriginal);
            CHECK(options.GetPSFGuessMethod() == +PSFGuessMethod::TheoreticalEsitimation);
            CHECK(options.GetFrequencyBandlimitDeterminationMethod() == +FrequencyBandlimitDeterminationMethod::AutoSelection);
            CHECK(options.GetSubVolumeCalculationMethod() == +SubVolumeCalculationMethod::Dynamic);
            CHECK(options.GetTheoreticalPSFGenerationMethod() == +TheoreticalPSFGenerationMethod::GibsonLanni);
            CHECK(options.GetDarkCurrentValue() == 0.f);
            CHECK(options.GetNumberOfIteration() == 10);
            CHECK(options.GetSaveInterval() == 10);
            CHECK(options.GetBinningFactorToXYPlane() == 1);
            CHECK(options.GetBinningFactorToZPlane() == 1);
            CHECK(options.GetEnableGPUProcessing() == true);
            CHECK(options.GetPaddingSizeToXYBorder() == 28);
            CHECK(options.GetPaddingSizeToZBorder() == 6);
            CHECK(options.GetSubVolumeOverlapSize() == 28);
            CHECK(options.GetEnableSubVolumeInXY() == true);
            CHECK(options.GetEnableSubVolumeInZ() == false);
            CHECK(options.GetEnablePSFConstraint() == true);
            CHECK(options.GetNoiseSmoothingFactor() == 2.0f);
            CHECK(options.GetPSFStretchFactor() == 1.0f);
            CHECK(options.GetPSFWaistRadius() == 1.0f);
            CHECK(options.GetGoldGaussianInterval() == 3);
            CHECK(options.GetGoldGaussianFWHM() == 1.0f);
            CHECK(options.GetEnableClassicConfocal() == false);
        }

        SECTION("Customize") {
            AutoQuantDeconvolutionOptions options;
            options.SetLogFilePath("test");
            options.SetModality(Modality::Confocal);
            options.SetDeconvolutionMethod(DeconvolutionMethod::GoldsMethod);
            options.SetDarkCurrentMethod(DarkCurrentMethod::ManualInput);
            options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::UserInput);
            options.SetPSFGuessMethod(PSFGuessMethod::UserInput);
            options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::TheoreticalLimit);
            options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Predetermined);
            options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::LagacyAutoQuant);
            options.SetDarkCurrentValue(1.f);
            options.SetNumberOfIteration(2);
            options.SetSaveInterval(3);
            options.SetBinningFactorToXYPlane(4);
            options.SetBinningFactorToZPlane(5);
            options.SetEnableGPUProcessing(false);
            options.SetPaddingSizeToXYBorder(6);
            options.SetPaddingSizeToZBorder(7);
            options.SetSubVolumeOverlapSize(8);
            options.SetEnableSubVolumeInXY(false);
            options.SetEnableSubVolumeInZ(true);
            options.SetEnablePSFConstraint(false);
            options.SetNoiseSmoothingFactor(9.f);
            options.SetPSFStretchFactor(10.f);
            options.SetPSFWaistRadius(11.f);
            options.SetGoldGaussianInterval(12);
            options.SetGoldGaussianFWHM(13.f);
            options.SetEnableClassicConfocal(true);

            CHECK(options.GetLogFilePath() == "test");
            CHECK(options.GetModality() == +Modality::Confocal);
            CHECK(options.GetDeconvolutionMethod() == +DeconvolutionMethod::GoldsMethod);
            CHECK(options.GetDarkCurrentMethod() == +DarkCurrentMethod::ManualInput);
            CHECK(options.GetInitialImageGuessGenerationMethod() == +InitialImageGuessGenerationMethod::UserInput);
            CHECK(options.GetPSFGuessMethod() == +PSFGuessMethod::UserInput);
            CHECK(options.GetFrequencyBandlimitDeterminationMethod() == +FrequencyBandlimitDeterminationMethod::TheoreticalLimit);
            CHECK(options.GetSubVolumeCalculationMethod() == +SubVolumeCalculationMethod::Predetermined);
            CHECK(options.GetTheoreticalPSFGenerationMethod() == +TheoreticalPSFGenerationMethod::LagacyAutoQuant);
            CHECK(options.GetDarkCurrentValue() == 1.f);
            CHECK(options.GetNumberOfIteration() == 2);
            CHECK(options.GetSaveInterval() == 3);
            CHECK(options.GetBinningFactorToXYPlane() == 4);
            CHECK(options.GetBinningFactorToZPlane() == 5);
            CHECK(options.GetEnableGPUProcessing() == false);
            CHECK(options.GetPaddingSizeToXYBorder() == 6);
            CHECK(options.GetPaddingSizeToZBorder() == 7);
            CHECK(options.GetSubVolumeOverlapSize() == 8);
            CHECK(options.GetEnableSubVolumeInXY() == false);
            CHECK(options.GetEnableSubVolumeInZ() == true);
            CHECK(options.GetEnablePSFConstraint() == false);
            CHECK(options.GetNoiseSmoothingFactor() == 9.f);
            CHECK(options.GetPSFStretchFactor() == 10.f);
            CHECK(options.GetPSFWaistRadius() == 11.f);
            CHECK(options.GetGoldGaussianInterval() == 12);
            CHECK(options.GetGoldGaussianFWHM() == 13.f);
            CHECK(options.GetEnableClassicConfocal() == true);

        }
    }
}