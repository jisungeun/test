#include <catch2/catch.hpp>

#include "AutoQuantOptionsEnum.h"

namespace AutoQuantEnumTest {
    TEST_CASE("AutoQuantEnum : unit test") {
        SECTION("Modality") {
            CHECK(Modality::_size() == 9);
            CHECK((+Modality::Invalid)._to_index() == 0);
            CHECK((+Modality::Confocal)._to_index() == 1);
            CHECK((+Modality::SpinDiskConfocal)._to_index() == 2);
            CHECK((+Modality::TwoPhoton)._to_index() == 3);
            CHECK((+Modality::WidefieldFluorescence)._to_index() == 4);
            CHECK((+Modality::TransmittedLightBrightField)._to_index() == 5);
            CHECK((+Modality::IndoCyanineGreenFluorescence)._to_index() == 6);
            CHECK((+Modality::ReflectedLight)._to_index() == 7);
            CHECK((+Modality::NonSpecific)._to_index() == 8);
        }

        SECTION("DeconvolutionMethod") {
            CHECK(DeconvolutionMethod::_size() == 4);
            CHECK((+DeconvolutionMethod::ExpectationMaximization)._to_index() == 0);
            CHECK((+DeconvolutionMethod::PowerEcceleration)._to_index() == 1);
            CHECK((+DeconvolutionMethod::PSFExtrapolationAcceleration)._to_index() == 2);
            CHECK((+DeconvolutionMethod::GoldsMethod)._to_index() == 3);
        }
        SECTION("DarkCurrentMethod") {
            CHECK(DarkCurrentMethod::_size() == 2);
            CHECK((+DarkCurrentMethod::AutoCalculation)._to_index() == 0);
            CHECK((+DarkCurrentMethod::ManualInput)._to_index() == 1);
        }
        SECTION("InitialImageGuessGenerationMethod") {
            CHECK(InitialImageGuessGenerationMethod::_size() == 4);
            CHECK((+InitialImageGuessGenerationMethod::OriginalData)._to_index() == 0);
            CHECK((+InitialImageGuessGenerationMethod::LinearFilteredOriginal)._to_index() == 1);
            CHECK((+InitialImageGuessGenerationMethod::ConstantValue)._to_index() == 2);
            CHECK((+InitialImageGuessGenerationMethod::UserInput)._to_index() == 3);
        }
        SECTION("PSFGuessMethod"){
            CHECK(PSFGuessMethod::_size() == 4);
            CHECK((+PSFGuessMethod::TheoreticalEsitimation)._to_index() == 0);
            CHECK((+PSFGuessMethod::ConstantValue)._to_index() == 1);
            CHECK((+PSFGuessMethod::Autocorrelation)._to_index() == 2);
            CHECK((+PSFGuessMethod::UserInput)._to_index() == 3);
        }
        SECTION("FrequencyBandlimitDeterminationMethod"){
            CHECK(FrequencyBandlimitDeterminationMethod::_size() == 3);
            CHECK((+FrequencyBandlimitDeterminationMethod::AutoSelection)._to_index() == 0);
            CHECK((+FrequencyBandlimitDeterminationMethod::TheoreticalLimit)._to_index() == 1);
            CHECK((+FrequencyBandlimitDeterminationMethod::DetectedLimit)._to_index() == 2);
        }
        SECTION("SubVolumeCalculationMethod"){
            CHECK(SubVolumeCalculationMethod::_size() == 2);
            CHECK((+SubVolumeCalculationMethod::Predetermined)._to_index() == 0);
            CHECK((+SubVolumeCalculationMethod::Dynamic)._to_index() == 1);
        }
        SECTION("TheoreticalPSFGenerationMethod"){
            CHECK(TheoreticalPSFGenerationMethod::_size() == 3);
            CHECK((+TheoreticalPSFGenerationMethod::LagacyAutoQuant)._to_index() == 0);
            CHECK((+TheoreticalPSFGenerationMethod::GibsonLanni)._to_index() == 1);
            CHECK((+TheoreticalPSFGenerationMethod::Zernike)._to_index() == 2);
        }
    }
}