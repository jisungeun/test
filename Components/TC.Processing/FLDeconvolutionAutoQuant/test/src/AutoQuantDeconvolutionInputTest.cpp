#include <catch2/catch.hpp>

#include "AutoQuantDeconvolutionInput.h"

namespace AutoQuantDeconvolutionInputTest {
    TEST_CASE("AutoQuantDeconvolutionInput : unit test") {
        SECTION("AutoQuantDeconvolutionInput()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("AutoQuantDeconvolutionInput(other)"){
            AutoQuantDeconvolutionInput srcAutoQuantDeconvolutionInput;
            srcAutoQuantDeconvolutionInput.SetNumericalAperture(1);

            AutoQuantDeconvolutionInput destAutoQuantDeconvolutionInput(srcAutoQuantDeconvolutionInput);
            CHECK(destAutoQuantDeconvolutionInput.GetNumericalAperture() == 1);
        }
        SECTION("operator=()"){
            AutoQuantDeconvolutionInput srcAutoQuantDeconvolutionInput;
            srcAutoQuantDeconvolutionInput.SetNumericalAperture(1);

            AutoQuantDeconvolutionInput destAutoQuantDeconvolutionInput;
            destAutoQuantDeconvolutionInput = srcAutoQuantDeconvolutionInput;
            CHECK(destAutoQuantDeconvolutionInput.GetNumericalAperture() == 1);
        }
        SECTION("SetFLRawData()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetFLRawData({});
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetFLRawData()") {
            FLRawData flRawData;
            flRawData.SetSize(1, 2, 3);

            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetFLRawData(flRawData);
            const auto resultFLRawData = autoQuantDeconvolutionInput.GetFLRawData();
            CHECK(resultFLRawData.GetSizeX() == 1);
            CHECK(resultFLRawData.GetSizeY() == 2);
            CHECK(resultFLRawData.GetSizeZ() == 3);
        }
        SECTION("SetPixelWorldSize()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeX(LengthUnit::Nanometer) == 1000);
        }
        SECTION("GetPixelWorldSizeY()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeY(LengthUnit::Nanometer) == 2000);
        }
        SECTION("GetPixelWorldSizeZ()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeZ(LengthUnit::Nanometer) == 3000);
        }
        SECTION("SetNumericalAperture()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetNumericalAperture(1);
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetNumericalAperture()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetNumericalAperture(1);
            CHECK(autoQuantDeconvolutionInput.GetNumericalAperture() == 1);
        }
        SECTION("SetMediumRI()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetMediumRI(1);
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetMediumRI()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetMediumRI(1);
            CHECK(autoQuantDeconvolutionInput.GetMediumRI() == 1);
        }
        SECTION("SetEmissionWaveLength()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetEmissionWaveLength(1, LengthUnit::Micrometer);
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetEmissionWaveLength()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetEmissionWaveLength(1, LengthUnit::Micrometer);
            CHECK(autoQuantDeconvolutionInput.GetEmissionWaveLength(LengthUnit::Nanometer) == 1000);
        }
        SECTION("SetSampleDepth()"){
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetSampleDepth(1, LengthUnit::Micrometer);
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetSampleDepth()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetSampleDepth(1, LengthUnit::Micrometer);
            CHECK(autoQuantDeconvolutionInput.GetSampleDepth(LengthUnit::Nanometer) == 1000);
        }
        SECTION("SetTempFolderPath()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetTempFolderPath("");
            CHECK(&autoQuantDeconvolutionInput != nullptr);
        }
        SECTION("GetTempFolderPath()") {
            AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
            autoQuantDeconvolutionInput.SetTempFolderPath("test");
            CHECK(autoQuantDeconvolutionInput.GetTempFolderPath() == "test");
        }
    }
    TEST_CASE("AutoQuantDeconvolutionInput : practical test") {
        constexpr auto sizeX = 10;
        constexpr auto sizeY = 10;
        constexpr auto sizeZ = 4;

        const std::shared_ptr<uint8_t[]> data{ new uint8_t[sizeX * sizeY * sizeZ]() };

        FLRawData flRawData;
        flRawData.SetData(data, FLMemoryOrder::XYZ);
        flRawData.SetSize(sizeX, sizeY, sizeZ);

        constexpr float pixelWorldSizeX = 2;
        constexpr float pixelWorldSizeY = 2;
        constexpr float pixelWorldSizeZ = 3;
        constexpr LengthUnit pixelWorldSizeUnit = LengthUnit::Micrometer;

        constexpr float numericalAperture = 1.2f;

        constexpr float mediumRI = 1.3f;

        constexpr float emissionWaveLength = 0.532f;
        constexpr LengthUnit emissionWaveLengthUnit = LengthUnit::Micrometer;

        constexpr float sampleDepth = 3.0f;
        constexpr LengthUnit sampleDepthUnit = LengthUnit::Micrometer;

        const QString tempFolderPath = "C:/Temp";

        AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
        autoQuantDeconvolutionInput.SetFLRawData(flRawData);
        autoQuantDeconvolutionInput.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        autoQuantDeconvolutionInput.SetNumericalAperture(numericalAperture);
        autoQuantDeconvolutionInput.SetMediumRI(mediumRI);
        autoQuantDeconvolutionInput.SetEmissionWaveLength(emissionWaveLength, emissionWaveLengthUnit);
        autoQuantDeconvolutionInput.SetSampleDepth(sampleDepth, sampleDepthUnit);
        autoQuantDeconvolutionInput.SetTempFolderPath(tempFolderPath);

        const auto resultFLRawData = autoQuantDeconvolutionInput.GetFLRawData();
        CHECK(resultFLRawData.GetSizeX() == sizeX);
        CHECK(resultFLRawData.GetSizeY() == sizeY);
        CHECK(resultFLRawData.GetSizeZ() == sizeZ);
        CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeX(LengthUnit::Nanometer) == 2000);
        CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeY(LengthUnit::Nanometer) == 2000);
        CHECK(autoQuantDeconvolutionInput.GetPixelWorldSizeZ(LengthUnit::Nanometer) == 3000);
        CHECK(autoQuantDeconvolutionInput.GetNumericalAperture() == 1.2f);
        CHECK(autoQuantDeconvolutionInput.GetMediumRI() == 1.3f);
        CHECK(autoQuantDeconvolutionInput.GetEmissionWaveLength(LengthUnit::Nanometer) == 532);
        CHECK(autoQuantDeconvolutionInput.GetSampleDepth(LengthUnit::Nanometer) == 3000);
        CHECK(autoQuantDeconvolutionInput.GetTempFolderPath() == tempFolderPath);
    }
}
