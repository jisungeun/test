#include <catch2/catch.hpp>

#include <fstream>
#include <QFile>
#include <QSettings>

#include "AutoQuantDeconvolution.h"
#include "AutoQuantDeconvolutionActivator.h"

namespace AutoQuantDeconvolutionTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/AutoQuantTest";
    const QString rawFL3DFilePath = testFolderPath + "/rawBinary.data";
    const QString deconTestFolderPath = "C:/Temp/decontest";

    constexpr auto sizeX = 1080;
    constexpr auto sizeY = 1080;
    constexpr auto sizeZ = 57;
    constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

    auto ReadInputData()->std::shared_ptr<uint8_t[]> {
        std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };

        std::ifstream fileStream{ rawFL3DFilePath.toStdString(), std::ios::binary };
        fileStream.read(reinterpret_cast<char*>(readData.get()), numberOfElements);
        fileStream.close();

        return readData;
    }

    class AutoQuantDeconvolutionOutputForTest final : public IAutoQuantDeconvolutionOutput {
    public:
        AutoQuantDeconvolutionOutputForTest() = default;
        ~AutoQuantDeconvolutionOutputForTest() = default;

        auto SetAutoQuantDeconvolutionResult(const AutoQuantDeconvolutionResult& result) -> void override {
            setAutoQuantDeconvolutionResultTriggered = true;
            this->result = result;
        }

        bool setAutoQuantDeconvolutionResultTriggered{ false };
        AutoQuantDeconvolutionResult result;
    };

    auto GenerateBaseKeyStringToRegistry(const std::string& registryKeyPath)->void {
        QSettings settings(registryKeyPath.c_str(), QSettings::NativeFormat);
        settings.setValue("0", QString("BC86 3219 4926 1418 105A 66B2 A052 A2C0"));
        settings.setValue("1", QString("8318 6214 EB09 3A1B 0623 A881 0C23 11B8"));
        settings.setValue("2", QString("C280 662D E180 323B BC80 0209 0183 90C8"));
        settings.setValue("3", QString("1D80 1698 4102 1619 085A 61B2 82D2 B2C0"));
        settings.setValue("4", QString("1640 A219 9236 18A8 3291 B2DD D123 C2A1"));
        settings.setValue("5", QString("A823 1B20 52B1 D920 82A1 70B1 C0C1 9918"));
        settings.setValue("6", QString("2301 AB10 9012 E83E 2322 44A3 C125 125E"));
        settings.setValue("7", QString("BA12 90D1 105A 66B2 1131 B123 12A2 9012"));
        settings.setValue("8", QString("C120 5312 3320 A029 01AB 43B2 C087 5420"));
        settings.setValue("9", QString("19B0 B6D2 4E20 1B13 06C2 81DD B238 A289"));
    }

    auto CompareArray(const float* array1, const float* array2, const size_t& arraySize) ->bool {
        for (size_t arrayIndex = 0; arrayIndex < arraySize; ++arrayIndex) {
            const auto value1 = array1[arrayIndex];
            const auto value2 = array2[arrayIndex];

            if (value1 != value2) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("AutoQuantDeconvolution : unit test") {
        const std::string registryKeyPath = "HKEY_CURRENT_USER\\Software\\Tomocube_dev\\AutoQuantDeconvolution\\Strings";
        GenerateBaseKeyStringToRegistry(registryKeyPath);

        AutoQuantDeconvolutionActivator::SetRegistryKeyLocation(registryKeyPath);
        AutoQuantDeconvolutionActivator::Unlock();

        SECTION("AutoQuantDeconvolution()") {
            AutoQuantDeconvolution autoQuantDeconvolution;
            CHECK(&autoQuantDeconvolution != nullptr);
        }
        SECTION("SetOutputPort()") {
            AutoQuantDeconvolution autoQuantDeconvolution;
            autoQuantDeconvolution.SetOutputPort({});
            CHECK(&autoQuantDeconvolution != nullptr);
        }
        SECTION("SetInput()") {
            AutoQuantDeconvolution autoQuantDeconvolution;
            autoQuantDeconvolution.SetInput({});
            CHECK(&autoQuantDeconvolution != nullptr);
        }
        SECTION("SetOption()") {
            AutoQuantDeconvolution autoQuantDeconvolution;
            autoQuantDeconvolution.SetOption({});
            CHECK(&autoQuantDeconvolution != nullptr);
        }
        SECTION("Deconvolute()") {
            auto output = new AutoQuantDeconvolutionOutputForTest;
            IAutoQuantDeconvolutionOutput::Pointer outputPort{ output };

            FLRawData flRawData;
            flRawData.SetData(ReadInputData(), FLMemoryOrder::YXZ);
            flRawData.SetSize(sizeX, sizeY, sizeZ);

            constexpr float pixelSizeOfImagingSensor = 4.5f; //micrometer
            constexpr float magnification = 50.f;
            constexpr float zScanStep = 0.156f; //micrometer
            constexpr float mediumRI = 1.337f;
            constexpr float numericalAperture = 1.2f;
            constexpr float emmissionWaveLength = 0.519f; // micrometer
            constexpr float sampleDepth = 3.0f;

            constexpr float pixelWorldSizeX = pixelSizeOfImagingSensor / magnification;
            constexpr float pixelWorldSizeY = pixelSizeOfImagingSensor / magnification;
            constexpr float pixelWorldSizeZ = zScanStep * mediumRI;

            AutoQuantDeconvolutionInput input;
            input.SetFLRawData(flRawData);
            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
            input.SetNumericalAperture(numericalAperture);
            input.SetMediumRI(mediumRI);
            input.SetEmissionWaveLength(emmissionWaveLength, LengthUnit::Micrometer);
            input.SetSampleDepth(sampleDepth, LengthUnit::Micrometer);
            input.SetTempFolderPath(deconTestFolderPath);

            AutoQuantDeconvolutionOptions options;
            options.SetLogFilePath(deconTestFolderPath + "/testLog");
            options.SetModality(Modality::WidefieldFluorescence);
            options.SetDeconvolutionMethod(DeconvolutionMethod::ExpectationMaximization);
            options.SetDarkCurrentMethod(DarkCurrentMethod::AutoCalculation);
            options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::LinearFilteredOriginal);
            options.SetPSFGuessMethod(PSFGuessMethod::TheoreticalEsitimation);
            options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::AutoSelection);
            options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Dynamic);
            options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::GibsonLanni);
            options.SetDarkCurrentValue(0.f);
            options.SetNumberOfIteration(10);
            options.SetSaveInterval(10);
            options.SetBinningFactorToXYPlane(1);
            options.SetBinningFactorToZPlane(1);
            options.SetEnableGPUProcessing(true);
            options.SetPaddingSizeToXYBorder(28);
            options.SetPaddingSizeToZBorder(6);
            options.SetSubVolumeOverlapSize(28);
            options.SetEnableSubVolumeInXY(true);
            options.SetEnableSubVolumeInZ(false);
            options.SetEnablePSFConstraint(true);
            options.SetNoiseSmoothingFactor(2.0f);
            options.SetPSFStretchFactor(1.0f);
            options.SetPSFWaistRadius(1.0f);
            options.SetGoldGaussianInterval(3);
            options.SetGoldGaussianFWHM(1.0f);
            options.SetEnableClassicConfocal(false);

            AutoQuantDeconvolution autoQuantDeconvolution;
            autoQuantDeconvolution.SetOutputPort(outputPort);
            autoQuantDeconvolution.SetInput(input);
            autoQuantDeconvolution.SetOption(options);
            const auto result = autoQuantDeconvolution.Deconvolute();
            CHECK(result == true);
        }
    }
    TEST_CASE("AutoQuantDeconvolution : practical test") {
        auto output = new AutoQuantDeconvolutionOutputForTest;
        IAutoQuantDeconvolutionOutput::Pointer outputPort{ output };

        const QString logFilePath = deconTestFolderPath + "/testLog";

        FLRawData flRawData;
        flRawData.SetData(ReadInputData(), FLMemoryOrder::YXZ);
        flRawData.SetSize(sizeX, sizeY, sizeZ);

        constexpr float pixelSizeOfImagingSensor = 4.5f; //micrometer
        constexpr float magnification = 50.f;
        constexpr float zScanStep = 0.156f; //micrometer
        constexpr float mediumRI = 1.337f;
        constexpr float numericalAperture = 1.2f;
        constexpr float emmissionWaveLength = 0.519f; // micrometer
        constexpr float sampleDepth = 3.0f;

        constexpr float pixelWorldSizeX = pixelSizeOfImagingSensor / magnification;
        constexpr float pixelWorldSizeY = pixelSizeOfImagingSensor / magnification;
        constexpr float pixelWorldSizeZ = zScanStep * mediumRI;

        AutoQuantDeconvolutionInput input;
        input.SetFLRawData(flRawData);
        input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
        input.SetNumericalAperture(numericalAperture);
        input.SetMediumRI(mediumRI);
        input.SetEmissionWaveLength(emmissionWaveLength, LengthUnit::Micrometer);
        input.SetSampleDepth(sampleDepth, LengthUnit::Micrometer);
        input.SetTempFolderPath(deconTestFolderPath);

        AutoQuantDeconvolutionOptions options;
        options.SetLogFilePath(logFilePath);
        options.SetModality(Modality::WidefieldFluorescence);
        options.SetDeconvolutionMethod(DeconvolutionMethod::ExpectationMaximization);
        options.SetDarkCurrentMethod(DarkCurrentMethod::AutoCalculation);
        options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::LinearFilteredOriginal);
        options.SetPSFGuessMethod(PSFGuessMethod::TheoreticalEsitimation);
        options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::AutoSelection);
        options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Dynamic);
        options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::GibsonLanni);
        options.SetDarkCurrentValue(0.f);
        options.SetNumberOfIteration(10);
        options.SetSaveInterval(10);
        options.SetBinningFactorToXYPlane(1);
        options.SetBinningFactorToZPlane(1);
        options.SetEnableGPUProcessing(true);
        options.SetPaddingSizeToXYBorder(28);
        options.SetPaddingSizeToZBorder(6);
        options.SetSubVolumeOverlapSize(28);
        options.SetEnableSubVolumeInXY(true);
        options.SetEnableSubVolumeInZ(false);
        options.SetEnablePSFConstraint(true);
        options.SetNoiseSmoothingFactor(2.0f);
        options.SetPSFStretchFactor(1.0f);
        options.SetPSFWaistRadius(1.0f);
        options.SetGoldGaussianInterval(3);
        options.SetGoldGaussianFWHM(1.0f);
        options.SetEnableClassicConfocal(false);

        AutoQuantDeconvolution autoQuantDeconvolution;
        autoQuantDeconvolution.SetOutputPort(outputPort);
        autoQuantDeconvolution.SetInput(input);
        autoQuantDeconvolution.SetOption(options);
        const auto result = autoQuantDeconvolution.Deconvolute();
        CHECK(result == true);
        CHECK(output->setAutoQuantDeconvolutionResultTriggered == true);

        const auto deconResult = output->result;

        CHECK(deconResult.GetFLDeconData().GetData(FLMemoryOrder::YXZ) != nullptr);
        CHECK(deconResult.GetFLDeconData().GetSizeX() == sizeX);
        CHECK(deconResult.GetFLDeconData().GetSizeY() == sizeY);
        CHECK(deconResult.GetFLDeconData().GetSizeZ() == sizeZ);

        CHECK(QFile::exists(logFilePath));
        CHECK(QFile::remove(logFilePath));
    }
}
