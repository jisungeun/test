#include <catch2/catch.hpp>

#include "FLMemoryOrder.h"

namespace FLMemoryOrderTest {
    TEST_CASE("FLMemoryOrder : unit test") {
        CHECK(FLMemoryOrder::_size() == 2);
        CHECK((+FLMemoryOrder::XYZ)._to_index() == 0);
        CHECK((+FLMemoryOrder::YXZ)._to_index() == 1);
    }
}