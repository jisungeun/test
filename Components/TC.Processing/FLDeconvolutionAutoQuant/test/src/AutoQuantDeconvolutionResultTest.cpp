#include <catch2/catch.hpp>

#include "AutoQuantDeconvolutionResult.h"

namespace AutoQuantDeconvolutionResultTest {
    auto CompareArray(const float* array1, const float* array2, const size_t& arraySize) ->bool {
        for (size_t arrayIndex = 0; arrayIndex < arraySize; ++arrayIndex) {
            const auto value1 = array1[arrayIndex];
            const auto value2 = array2[arrayIndex];

            if (value1 != value2) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("AutoQuantDeconvolutionResult : unit test") {
        SECTION("AutoQuantDeconvolutionResult()") {
            AutoQuantDeconvolutionResult autoQuantDeconvolutionResult;
            CHECK(&autoQuantDeconvolutionResult != nullptr);
        }
        SECTION("AutoQuantDeconvolutionResult(other)") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);

            AutoQuantDeconvolutionResult srcAutoQuantDeconvolutionResult;
            srcAutoQuantDeconvolutionResult.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionResult destAutoQuantDeconvolutionResult(srcAutoQuantDeconvolutionResult);
            const auto resultFLDeconData = destAutoQuantDeconvolutionResult.GetFLDeconData();
            CHECK(resultFLDeconData.GetSizeX() == 1);
            CHECK(resultFLDeconData.GetSizeY() == 2);
            CHECK(resultFLDeconData.GetSizeZ() == 3);
        }
        SECTION("operator=()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);

            AutoQuantDeconvolutionResult srcAutoQuantDeconvolutionResult;
            srcAutoQuantDeconvolutionResult.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionResult destAutoQuantDeconvolutionResult;
            destAutoQuantDeconvolutionResult = srcAutoQuantDeconvolutionResult;
            const auto resultFLDeconData = destAutoQuantDeconvolutionResult.GetFLDeconData();
            CHECK(resultFLDeconData.GetSizeX() == 1);
            CHECK(resultFLDeconData.GetSizeY() == 2);
            CHECK(resultFLDeconData.GetSizeZ() == 3);
        }
        SECTION("SetFLDeconData()") {
            AutoQuantDeconvolutionResult autoQuantDeconvolutionResult;
            autoQuantDeconvolutionResult.SetFLDeconData({});
            CHECK(&autoQuantDeconvolutionResult != nullptr);
        }
        SECTION("GetFLDeconData()") {
            FLDeconData flDeconData;
            flDeconData.SetSize(1, 2, 3);

            AutoQuantDeconvolutionResult autoQuantDeconvolutionResult;
            autoQuantDeconvolutionResult.SetFLDeconData(flDeconData);

            const auto resultFLDeconData = autoQuantDeconvolutionResult.GetFLDeconData();
            CHECK(resultFLDeconData.GetSizeX() == 1);
            CHECK(resultFLDeconData.GetSizeY() == 2);
            CHECK(resultFLDeconData.GetSizeZ() == 3);
        }
    }

    TEST_CASE("AutoQuantDeconvolutionResult : practical test") {
        constexpr int32_t sizeX = 3;
        constexpr int32_t sizeY = 3;
        constexpr int32_t sizeZ = 2;
        constexpr int32_t numberOfElements = sizeX * sizeY * sizeZ;

        constexpr float data[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };

        std::shared_ptr<float[]> inputData{ new float[numberOfElements]() };
        std::copy_n(data, numberOfElements, inputData.get());

        FLDeconData flDeconData;
        flDeconData.SetSize(sizeX, sizeY, sizeZ);
        flDeconData.SetData(inputData, FLMemoryOrder::YXZ);

        AutoQuantDeconvolutionResult autoQuantDeconvolutionResult;
        autoQuantDeconvolutionResult.SetFLDeconData(flDeconData);
        const auto resultFLDeconData = autoQuantDeconvolutionResult.GetFLDeconData();
        const auto resultData = resultFLDeconData.GetData(FLMemoryOrder::YXZ);

        CHECK(resultFLDeconData.GetSizeX() == sizeX);
        CHECK(resultFLDeconData.GetSizeY() == sizeY);
        CHECK(resultFLDeconData.GetSizeZ() == sizeZ);
        CHECK(CompareArray(resultData.get(), data, numberOfElements));
    }
}