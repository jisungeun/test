#include <catch2/catch.hpp>

#include "AutoQuantDeconvolutionOutput.h"

namespace AutoQuantDeconvolutionOutputTest {
    auto CompareArray(const float* result, const float* answer, const size_t& dataCount) ->bool {
        for (size_t i = 0; i < dataCount; ++i) {
            if (result[i] != answer[i]) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("AutoQuantDeconvolutionOutput : unit test") {
        SECTION("AutoQuantDeconvolutionOutput()") {
            AutoQuantDeconvolutionOutput output;
            CHECK(&output != nullptr);
        }
        SECTION("SetAutoQuantDeconvolutionResult()") {
            AutoQuantDeconvolutionOutput output;
            output.SetAutoQuantDeconvolutionResult({});
            CHECK(&output != nullptr);
        }
        SECTION("GetDataMemory()") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto sizeZ = 3;

            const float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<float[]> dataMemory{ new float[sizeX * sizeY * sizeZ]() };
            std::copy_n(data, 12, dataMemory.get());

            FLDeconData flDeconData;
            flDeconData.SetData(dataMemory, FLMemoryOrder::YXZ);
            flDeconData.SetSize(sizeX, sizeY, sizeZ);

            AutoQuantDeconvolutionResult result;
            result.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionOutput output;
            output.SetAutoQuantDeconvolutionResult(result);

            const auto resultData = output.GetDataMemory(FLMemoryOrder::YXZ);
            CHECK(CompareArray(resultData.get(), data, 12));
        }
        SECTION("GetSizeX()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto sizeZ = 3;

            const float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<float[]> dataMemory{ new float[sizeX * sizeY * sizeZ]() };
            std::copy_n(data, 12, dataMemory.get());

            FLDeconData flDeconData;
            flDeconData.SetData(dataMemory, FLMemoryOrder::YXZ);
            flDeconData.SetSize(sizeX, sizeY, sizeZ);

            AutoQuantDeconvolutionResult result;
            result.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionOutput output;
            output.SetAutoQuantDeconvolutionResult(result);

            CHECK(output.GetSizeZ() == sizeZ);
        }
        SECTION("GetSizeY()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto sizeZ = 3;

            const float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<float[]> dataMemory{ new float[sizeX * sizeY * sizeZ]() };
            std::copy_n(data, 12, dataMemory.get());

            FLDeconData flDeconData;
            flDeconData.SetData(dataMemory, FLMemoryOrder::YXZ);
            flDeconData.SetSize(sizeX, sizeY, sizeZ);

            AutoQuantDeconvolutionResult result;
            result.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionOutput output;
            output.SetAutoQuantDeconvolutionResult(result);

            CHECK(output.GetSizeY() == sizeY);
        }
        SECTION("GetSizeZ()"){
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 2;
            constexpr auto sizeZ = 3;

            const float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<float[]> dataMemory{ new float[sizeX * sizeY * sizeZ]() };
            std::copy_n(data, 12, dataMemory.get());

            FLDeconData flDeconData;
            flDeconData.SetData(dataMemory, FLMemoryOrder::YXZ);
            flDeconData.SetSize(sizeX, sizeY, sizeZ);

            AutoQuantDeconvolutionResult result;
            result.SetFLDeconData(flDeconData);

            AutoQuantDeconvolutionOutput output;
            output.SetAutoQuantDeconvolutionResult(result);

            CHECK(output.GetSizeZ() == sizeZ);
        }
    }
    TEST_CASE("AutoQuantDeconvolutionOutput : practical test") {
        constexpr auto sizeX = 2;
        constexpr auto sizeY = 2;
        constexpr auto sizeZ = 3;

        const float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

        std::shared_ptr<float[]> dataMemory{ new float[sizeX * sizeY * sizeZ]() };
        std::copy_n(data, 12, dataMemory.get());

        FLDeconData flDeconData;
        flDeconData.SetData(dataMemory, FLMemoryOrder::YXZ);
        flDeconData.SetSize(sizeX, sizeY, sizeZ);

        AutoQuantDeconvolutionResult result;
        result.SetFLDeconData(flDeconData);

        AutoQuantDeconvolutionOutput output;
        output.SetAutoQuantDeconvolutionResult(result);

        const auto resultData = output.GetDataMemory(FLMemoryOrder::YXZ);
        CHECK(CompareArray(resultData.get(), data, 12));
        CHECK(output.GetSizeX() == sizeX);
        CHECK(output.GetSizeY() == sizeY);
        CHECK(output.GetSizeZ() == sizeZ);
    }
}