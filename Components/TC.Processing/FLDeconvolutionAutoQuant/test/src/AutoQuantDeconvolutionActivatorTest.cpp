#include <catch2/catch.hpp>

#include <QSettings>

#include "AutoQuantDeconvolutionActivator.h"

namespace AutoQuantDeconvolutionActivatorTest {
    auto GenerateBaseKeyStringToRegistry(const std::string& registryKeyPath)->void {
        QSettings settings(registryKeyPath.c_str(), QSettings::NativeFormat);
        settings.setValue("0", QString("BC86 3219 4926 1418 105A 66B2 A052 A2C0"));
        settings.setValue("1", QString("8318 6214 EB09 3A1B 0623 A881 0C23 11B8"));
        settings.setValue("2", QString("C280 662D E180 323B BC80 0209 0183 90C8"));
        settings.setValue("3", QString("1D80 1698 4102 1619 085A 61B2 82D2 B2C0"));
        settings.setValue("4", QString("1640 A219 9236 18A8 3291 B2DD D123 C2A1"));
        settings.setValue("5", QString("A823 1B20 52B1 D920 82A1 70B1 C0C1 9918"));
        settings.setValue("6", QString("2301 AB10 9012 E83E 2322 44A3 C125 125E"));
        settings.setValue("7", QString("BA12 90D1 105A 66B2 1131 B123 12A2 9012"));
        settings.setValue("8", QString("C120 5312 3320 A029 01AB 43B2 C087 5420"));
        settings.setValue("9", QString("19B0 B6D2 4E20 1B13 06C2 81DD B238 A289"));
    }


    TEST_CASE("AutoQuantDeconvolutionActivator : unit test") {
        const std::string registryKeyPath = "HKEY_CURRENT_USER\\Software\\Tomocube_dev\\AutoQuantDeconvolutionActivator\\Strings";
        GenerateBaseKeyStringToRegistry(registryKeyPath);

        CHECK(AutoQuantDeconvolutionActivator::IsUnlocked() == false);
        CHECK(AutoQuantDeconvolutionActivator::Unlock() == false);
        AutoQuantDeconvolutionActivator::SetRegistryKeyLocation(registryKeyPath);
        CHECK(AutoQuantDeconvolutionActivator::Unlock() == true);
        CHECK(AutoQuantDeconvolutionActivator::IsUnlocked() == true);
    }
}