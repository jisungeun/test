#include <catch2/catch.hpp>

#include <QFile>

#include "AutoQuantParser.h"
#include "aqiEnumerations.h"


namespace AutoQuantParserTest {
    auto GetInputForUnitTest()->AutoQuantDeconvolutionInput {
        constexpr auto sizeX = 1;
        constexpr auto sizeY = 2;
        constexpr auto sizeZ = 3;

        const std::shared_ptr<uint8_t[]> data{ new uint8_t[sizeX * sizeY * sizeZ]() };

        FLRawData flRawData;
        flRawData.SetData(data, FLMemoryOrder::XYZ);
        flRawData.SetSize(sizeX, sizeY, sizeZ);

        AutoQuantDeconvolutionInput autoQuantDeconvolutionInput;
        autoQuantDeconvolutionInput.SetFLRawData(flRawData);
        autoQuantDeconvolutionInput.SetPixelWorldSize(4.f, 5.f, 6.f, LengthUnit::Micrometer);
        autoQuantDeconvolutionInput.SetNumericalAperture(7.f);
        autoQuantDeconvolutionInput.SetMediumRI(8.f);
        autoQuantDeconvolutionInput.SetEmissionWaveLength(9.f, LengthUnit::Micrometer);
        autoQuantDeconvolutionInput.SetSampleDepth(10.f, LengthUnit::Micrometer);
        autoQuantDeconvolutionInput.SetTempFolderPath("11");

        const auto resultFLRawData = autoQuantDeconvolutionInput.GetFLRawData();

        return autoQuantDeconvolutionInput;
    }

    auto GetOptionForUnitTest()->AutoQuantDeconvolutionOptions {
        AutoQuantDeconvolutionOptions options;
        options.SetLogFilePath("test");
        options.SetModality(Modality::Confocal);
        options.SetDeconvolutionMethod(DeconvolutionMethod::GoldsMethod);
        options.SetDarkCurrentMethod(DarkCurrentMethod::ManualInput);
        options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::UserInput);
        options.SetPSFGuessMethod(PSFGuessMethod::UserInput);
        options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::TheoreticalLimit);
        options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Predetermined);
        options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::LagacyAutoQuant);
        options.SetDarkCurrentValue(1.f);
        options.SetNumberOfIteration(2);
        options.SetSaveInterval(3);
        options.SetBinningFactorToXYPlane(4);
        options.SetBinningFactorToZPlane(5);
        options.SetEnableGPUProcessing(false);
        options.SetPaddingSizeToXYBorder(6);
        options.SetPaddingSizeToZBorder(7);
        options.SetSubVolumeOverlapSize(8);
        options.SetEnableSubVolumeInXY(false);
        options.SetEnableSubVolumeInZ(true);
        options.SetEnablePSFConstraint(false);
        options.SetNoiseSmoothingFactor(9.f);
        options.SetPSFStretchFactor(10.f);
        options.SetPSFWaistRadius(11.f);
        options.SetGoldGaussianInterval(12);
        options.SetGoldGaussianFWHM(13.f);
        options.SetEnableClassicConfocal(true);

        return options;
    }

    auto CompareString(const std::string& array1, const std::string& array2) ->bool {
        return (array1 == array2);
    }

    TEST_CASE("AutoQuantParser : unit test") {
        SECTION("AutoQuantParser()") {
            AutoQuantParser autoQuantParser;
            CHECK(&autoQuantParser != nullptr);
        }
        SECTION("SetInput()") {
            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput({});
            CHECK(&autoQuantParser != nullptr);
        }
        SECTION("SetOutput()") {
            AutoQuantParser autoQuantParser;
            autoQuantParser.SetOption({});
            CHECK(&autoQuantParser != nullptr);
        }
        SECTION("GetImageInfo()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            const auto resultImageInfo = autoQuantParser.GetImageInfo();
            CHECK(CompareString(resultImageInfo.lpstrImgFile, "11/tempFLRawBinData"));
            CHECK(CompareString(resultImageInfo.lpstrRealName, "11/tempFLLog"));
            CHECK(resultImageInfo.enDataType == DT_UINT_8BIT);
            CHECK(resultImageInfo.enScopeModality == MOD_CONFOCAL);
            CHECK(resultImageInfo.shWidth == 1);
            CHECK(resultImageInfo.shHeight == 2);
            CHECK(resultImageInfo.shNumSlices == 3);
            CHECK(resultImageInfo.fPixelSizeX == 4.f);
            CHECK(resultImageInfo.fPixelSizeY == 5.f);
            CHECK(resultImageInfo.fPixelSizeZ == 6.f);
            CHECK(resultImageInfo.fNumericAperture == 7.f);
            CHECK(resultImageInfo.fRefractiveIndex == 8.f);
            CHECK(resultImageInfo.fEmmWavelength == 9.f);
            CHECK(resultImageInfo.fSampleDepth == 10.f);

            CHECK(QFile::exists("11/tempFLRawBinData"));
            CHECK(QFile::remove("11/tempFLRawBinData"));

            delete[] resultImageInfo.lpstrImgFile;
            delete[] resultImageInfo.lpstrRealName;
        }
        SECTION("GetPsfInInfo()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            const auto resultPsfInInfo = autoQuantParser.GetPsfInInfo();
            CHECK(resultPsfInInfo.lpstrPsfFile == nullptr);
            CHECK(resultPsfInInfo.lpstrPsfOutFile == nullptr);
            CHECK(resultPsfInInfo.lpfPsfEstimate == nullptr);
            CHECK(resultPsfInInfo.shWidth == 0);
            CHECK(resultPsfInInfo.shHeight == 0);
            CHECK(resultPsfInInfo.shNumSlices == 0);
            CHECK(resultPsfInInfo.fPixelSizeX == 0.f);
            CHECK(resultPsfInInfo.fPixelSizeY == 0.f);
            CHECK(resultPsfInInfo.fPixelSizeZ == 0.f);
            CHECK(resultPsfInInfo.enPsfSource == PSFSRC_CALCULATED);
        }
        SECTION("GetPsfOutInfo()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            const auto resultPsfOutInfo = autoQuantParser.GetPsfInInfo();
            CHECK(resultPsfOutInfo.lpstrPsfFile == nullptr);
            CHECK(resultPsfOutInfo.lpstrPsfOutFile == nullptr);
            CHECK(resultPsfOutInfo.lpfPsfEstimate == nullptr);
            CHECK(resultPsfOutInfo.shWidth == 0);
            CHECK(resultPsfOutInfo.shHeight == 0);
            CHECK(resultPsfOutInfo.shNumSlices == 0);
            CHECK(resultPsfOutInfo.fPixelSizeX == 0.f);
            CHECK(resultPsfOutInfo.fPixelSizeY == 0.f);
            CHECK(resultPsfOutInfo.fPixelSizeZ == 0.f);
            CHECK(resultPsfOutInfo.enPsfSource == PSFSRC_CALCULATED);
        }
        SECTION("GetDeconOptionsStandard()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            const auto resultDeconOptionsStandard = autoQuantParser.GetDeconOptionsStandard();

            CHECK(CompareString(resultDeconOptionsStandard.lpstrImgOutFile, "11\\tempFLDeconData"));
            CHECK(CompareString(resultDeconOptionsStandard.lpstrTempDir, "11"));
            CHECK(resultDeconOptionsStandard.enDeconMeth == DM_GOLDS_METHOD);
            CHECK(resultDeconOptionsStandard.enDarkCurMeth == DCM_MANUAL_INPUT);
            CHECK(resultDeconOptionsStandard.fDarkCurrent == 1.f);
            CHECK(resultDeconOptionsStandard.shNumIterations == 2);
            CHECK(resultDeconOptionsStandard.shSaveInterval == 3);
            CHECK(resultDeconOptionsStandard.shBinFactorXY == 4);
            CHECK(resultDeconOptionsStandard.shBinFactorZ == 5);
            CHECK(resultDeconOptionsStandard.bEnableGpuProcessing == false);

            delete[] resultDeconOptionsStandard.lpstrImgOutFile;
            delete[] resultDeconOptionsStandard.lpstrTempDir;
        }
        SECTION("GetDeconOptionsExpert()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            const auto resultDeconOptionsExpert = autoQuantParser.GetDeconOptionsExpert();
            CHECK(resultDeconOptionsExpert.enImgGuessMeth == IGM_OBJECT_INPUT);
            CHECK(resultDeconOptionsExpert.enPsfGuessMeth == PGM_PSF_INPUT);
            CHECK(resultDeconOptionsExpert.enFreqConsMeth == FCM_THEORETICAL_LIMIT);
            CHECK(resultDeconOptionsExpert.enSubVolMeth == SVM_STATIC);
            CHECK(resultDeconOptionsExpert.shGuardBand == 6);
            CHECK(resultDeconOptionsExpert.shGuardBandZ == 7);
            CHECK(resultDeconOptionsExpert.shSubVolOverlap == 8);
            CHECK(resultDeconOptionsExpert.bMontageXY == false);
            CHECK(resultDeconOptionsExpert.bMontageZ == true);
            CHECK(resultDeconOptionsExpert.bEnablePsfCons == false);
            CHECK(resultDeconOptionsExpert.fSuppressNoiseFactor == 9.f);
            CHECK(resultDeconOptionsExpert.fPsfStretchFactor == 10.f);
            CHECK(resultDeconOptionsExpert.fPsfCentralRadius == 11.f);
            CHECK(resultDeconOptionsExpert.shGoldsSmoothIteration == 12);
            CHECK(resultDeconOptionsExpert.fGoldsSmoothGauss == 13.f);
            CHECK(resultDeconOptionsExpert.bEnableClassicConfocalAlgorithm == true);
            CHECK(resultDeconOptionsExpert.enPsfGenMeth == TPM_AUTOQUANT);
        }
        SECTION("GetInputWritingFilePath()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            CHECK(autoQuantParser.GetInputWritingFilePath() == "11/tempFLRawBinData");
        }
        SECTION("GetOutputWritingFilePath()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            CHECK(autoQuantParser.GetOutputWritingFilePath() == "11/2_tempFLDeconData");
        }
        SECTION("GetLogFilePath()") {
            const auto input = GetInputForUnitTest();
            const auto option = GetOptionForUnitTest();

            AutoQuantParser autoQuantParser;
            autoQuantParser.SetInput(input);
            autoQuantParser.SetOption(option);

            CHECK(autoQuantParser.GetLogFilePath() == "11/tempFLLog.log");
        }
    }
    TEST_CASE("AutoQuantParser : practical test") {
        constexpr auto sizeX = 10;
        constexpr auto sizeY = 10;
        constexpr auto sizeZ = 4;

        const std::shared_ptr<uint8_t[]> data{ new uint8_t[sizeX * sizeY * sizeZ]() };

        FLRawData flRawData;
        flRawData.SetData(data, FLMemoryOrder::XYZ);
        flRawData.SetSize(sizeX, sizeY, sizeZ);

        constexpr float pixelWorldSizeX = 2;
        constexpr float pixelWorldSizeY = 2;
        constexpr float pixelWorldSizeZ = 3;
        constexpr LengthUnit pixelWorldSizeUnit = LengthUnit::Micrometer;

        constexpr float numericalAperture = 1.2f;

        constexpr float mediumRI = 1.3f;

        constexpr float emissionWaveLength = 0.532f;
        constexpr LengthUnit emissionWaveLengthUnit = LengthUnit::Micrometer;

        constexpr float sampleDepth = 3.0f;
        constexpr LengthUnit sampleDepthUnit = LengthUnit::Micrometer;

        const QString tempFolderPath = "C:/Temp";

        AutoQuantDeconvolutionInput input;
        input.SetFLRawData(flRawData);
        input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        input.SetNumericalAperture(numericalAperture);
        input.SetMediumRI(mediumRI);
        input.SetEmissionWaveLength(emissionWaveLength, emissionWaveLengthUnit);
        input.SetSampleDepth(sampleDepth, sampleDepthUnit);
        input.SetTempFolderPath(tempFolderPath);

        AutoQuantDeconvolutionOptions option;
        option.SetLogFilePath("flDecon");
        option.SetModality(Modality::WidefieldFluorescence);
        option.SetDeconvolutionMethod(DeconvolutionMethod::ExpectationMaximization);
        option.SetDarkCurrentMethod(DarkCurrentMethod::AutoCalculation);
        option.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::LinearFilteredOriginal);
        option.SetPSFGuessMethod(PSFGuessMethod::TheoreticalEsitimation);
        option.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::AutoSelection);
        option.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Dynamic);
        option.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::GibsonLanni);
        option.SetDarkCurrentValue(0.f);
        option.SetNumberOfIteration(10);
        option.SetSaveInterval(10);
        option.SetBinningFactorToXYPlane(1);
        option.SetBinningFactorToZPlane(1);
        option.SetEnableGPUProcessing(true);
        option.SetPaddingSizeToXYBorder(28);
        option.SetPaddingSizeToZBorder(6);
        option.SetSubVolumeOverlapSize(28);
        option.SetEnableSubVolumeInXY(true);
        option.SetEnableSubVolumeInZ(false);
        option.SetEnablePSFConstraint(true);
        option.SetNoiseSmoothingFactor(2.0f);
        option.SetPSFStretchFactor(1.0f);
        option.SetPSFWaistRadius(1.0f);
        option.SetGoldGaussianInterval(3);
        option.SetGoldGaussianFWHM(1.0f);
        option.SetEnableClassicConfocal(false);

        AutoQuantParser autoQuantParser;
        autoQuantParser.SetInput(input);
        autoQuantParser.SetOption(option);

        const auto imageInfo = autoQuantParser.GetImageInfo();
        const auto psfInInfo = autoQuantParser.GetPsfInInfo();
        const auto psfOutInfo = autoQuantParser.GetPsfOutInfo();
        const auto deconOptionStandard = autoQuantParser.GetDeconOptionsStandard();
        const auto deconOptionExpert = autoQuantParser.GetDeconOptionsExpert();
        const auto inputWritingFilePath = autoQuantParser.GetInputWritingFilePath();
        const auto outputWritingFilePath = autoQuantParser.GetOutputWritingFilePath();
        const auto logFilePath = autoQuantParser.GetLogFilePath();

        CHECK(CompareString(imageInfo.lpstrImgFile, "C:/Temp/tempFLRawBinData"));
        CHECK(CompareString(imageInfo.lpstrRealName, "C:/Temp/tempFLLog"));
        CHECK(imageInfo.enDataType == DT_UINT_8BIT);
        CHECK(imageInfo.enScopeModality == MOD_WF_FLUORESCENCE);
        CHECK(imageInfo.shWidth == 10);
        CHECK(imageInfo.shHeight == 10);
        CHECK(imageInfo.shNumSlices == 4);
        CHECK(imageInfo.fPixelSizeX == 2.f);
        CHECK(imageInfo.fPixelSizeY == 2.f);
        CHECK(imageInfo.fPixelSizeZ == 3.f);
        CHECK(imageInfo.fNumericAperture == 1.2f);
        CHECK(imageInfo.fRefractiveIndex == 1.3f);
        CHECK(imageInfo.fEmmWavelength == 0.532f);
        CHECK(imageInfo.fSampleDepth == 3.0f);

        CHECK(QFile::exists("C:/Temp/tempFLRawBinData"));
        CHECK(QFile::remove("C:/Temp/tempFLRawBinData"));

        delete[] imageInfo.lpstrImgFile;
        delete[] imageInfo.lpstrRealName;

        CHECK(psfInInfo.lpstrPsfFile == nullptr);
        CHECK(psfInInfo.lpstrPsfOutFile == nullptr);
        CHECK(psfInInfo.lpfPsfEstimate == nullptr);
        CHECK(psfInInfo.shWidth == 0);
        CHECK(psfInInfo.shHeight == 0);
        CHECK(psfInInfo.shNumSlices == 0);
        CHECK(psfInInfo.fPixelSizeX == 0.f);
        CHECK(psfInInfo.fPixelSizeY == 0.f);
        CHECK(psfInInfo.fPixelSizeZ == 0.f);
        CHECK(psfInInfo.enPsfSource == PSFSRC_CALCULATED);

        CHECK(psfOutInfo.lpstrPsfFile == nullptr);
        CHECK(psfOutInfo.lpstrPsfOutFile == nullptr);
        CHECK(psfOutInfo.lpfPsfEstimate == nullptr);
        CHECK(psfOutInfo.shWidth == 0);
        CHECK(psfOutInfo.shHeight == 0);
        CHECK(psfOutInfo.shNumSlices == 0);
        CHECK(psfOutInfo.fPixelSizeX == 0.f);
        CHECK(psfOutInfo.fPixelSizeY == 0.f);
        CHECK(psfOutInfo.fPixelSizeZ == 0.f);
        CHECK(psfOutInfo.enPsfSource == PSFSRC_CALCULATED);

        CHECK(CompareString(deconOptionStandard.lpstrImgOutFile, "C:/Temp\\tempFLDeconData"));
        CHECK(CompareString(deconOptionStandard.lpstrTempDir, "C:/Temp"));
        CHECK(deconOptionStandard.enDeconMeth == DM_EXPECTATION_MAX);
        CHECK(deconOptionStandard.enDarkCurMeth == DCM_AUTO_CALCULATION);
        CHECK(deconOptionStandard.fDarkCurrent == 0.f);
        CHECK(deconOptionStandard.shNumIterations == 10);
        CHECK(deconOptionStandard.shSaveInterval == 10);
        CHECK(deconOptionStandard.shBinFactorXY == 1);
        CHECK(deconOptionStandard.shBinFactorZ == 1);
        CHECK(deconOptionStandard.bEnableGpuProcessing == true);

        delete[] deconOptionStandard.lpstrImgOutFile;
        delete[] deconOptionStandard.lpstrTempDir;

        CHECK(deconOptionExpert.enImgGuessMeth == IGM_FILTERED_ORIGINAL);
        CHECK(deconOptionExpert.enPsfGuessMeth == PGM_THEORETICAL_EST);
        CHECK(deconOptionExpert.enFreqConsMeth == FCM_AUTO_SELECTION);
        CHECK(deconOptionExpert.enSubVolMeth == SVM_DYNAMIC);
        CHECK(deconOptionExpert.shGuardBand == 28);
        CHECK(deconOptionExpert.shGuardBandZ == 6);
        CHECK(deconOptionExpert.shSubVolOverlap == 28);
        CHECK(deconOptionExpert.bMontageXY == true);
        CHECK(deconOptionExpert.bMontageZ == false);
        CHECK(deconOptionExpert.bEnablePsfCons == true);
        CHECK(deconOptionExpert.fSuppressNoiseFactor == 2.0f);
        CHECK(deconOptionExpert.fPsfStretchFactor == 1.0f);
        CHECK(deconOptionExpert.fPsfCentralRadius == 1.0f);
        CHECK(deconOptionExpert.shGoldsSmoothIteration == 3);
        CHECK(deconOptionExpert.fGoldsSmoothGauss == 1.0f);
        CHECK(deconOptionExpert.bEnableClassicConfocalAlgorithm == false);
        CHECK(deconOptionExpert.enPsfGenMeth == TPM_GIBSON_LANNI);

        CHECK(inputWritingFilePath == "C:/Temp/tempFLRawBinData");
        CHECK(outputWritingFilePath == "C:/Temp/10_tempFLDeconData");
        CHECK(logFilePath == "C:/Temp/tempFLLog.log");
    }
}