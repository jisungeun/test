#pragma once

#include <memory>
#include "arrayfire.h"

class HotPixelMaskGenerator {
public:
    HotPixelMaskGenerator();
    ~HotPixelMaskGenerator();

    auto SetImageArray(const af::array& imageArray)->void;
    auto SetWindowSize(const int32_t& windowSize)->void;
    auto SetThresholdValue(const double& thresholdValue)->void;

    auto Generate()->bool;

    auto GetHotPixelMask()const->const af::array&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};