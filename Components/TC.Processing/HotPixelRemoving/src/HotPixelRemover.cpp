#include "HotPixelRemover.h"

#include "arrayfire.h"
#include "ImageStackHotPixelMaskGenerator.h"

class HotPixelRemover::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    std::shared_ptr<uint8_t[]> imageStack;
    int32_t imageSizeX{};
    int32_t imageSizeY{};
    int32_t imageSizeZ{};

    int32_t samplingZStep{};
    int32_t windowSize{};
    double thresholdValue{};

    std::shared_ptr<uint8_t[]> hotPixelRemovedImageStack;
};

HotPixelRemover::HotPixelRemover() : d(new Impl()) {
}

HotPixelRemover::~HotPixelRemover() = default;

auto HotPixelRemover::SetImageStack(const std::shared_ptr<uint8_t[]>& imageStack, const int32_t& imageSizeX,
    const int32_t& imageSizeY, const int32_t& imageSizeZ) -> void {
    d->imageStack = imageStack;
    d->imageSizeX = imageSizeX;
    d->imageSizeY = imageSizeY;
    d->imageSizeZ = imageSizeZ;
}

auto HotPixelRemover::SetSamplingZStep(const int32_t& samplingZStep) -> void {
    d->samplingZStep = samplingZStep;
}

auto HotPixelRemover::SetWindowSize(const int32_t& windowSize) -> void {
    d->windowSize = windowSize;
}

auto HotPixelRemover::SetThresholdValue(const double& thresholdValue) -> void {
    d->thresholdValue = thresholdValue;
}

auto HotPixelRemover::Remove() -> bool {
    auto imageStackArray = af::array(d->imageSizeY, d->imageSizeX, d->imageSizeZ, d->imageStack.get()).as(f32).copy();

    ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
    imageStackHotPixelMaskGenerator.SetSamplingZStep(d->samplingZStep);
    imageStackHotPixelMaskGenerator.SetWindowSize(d->windowSize);
    imageStackHotPixelMaskGenerator.SetThresholdValue(d->thresholdValue);
    imageStackHotPixelMaskGenerator.SetImageStackArray(imageStackArray);

    if (!imageStackHotPixelMaskGenerator.Generate()) {
        return false;
    }

    const auto hotPixelMask = imageStackHotPixelMaskGenerator.GetHotPixelMask().as(f32).copy();

    for (auto zIndex = 0; zIndex < d->imageSizeZ; ++zIndex) {
        const auto zSliceImageArray = imageStackArray(af::span, af::span, zIndex);

        const auto filteredZSliceImageArray = af::medfilt2(zSliceImageArray, 3, 3, AF_PAD_SYM);

        imageStackArray(af::span, af::span, zIndex) = 
            (zSliceImageArray * (1 - hotPixelMask)) +
            (filteredZSliceImageArray * hotPixelMask);
    }
    imageStackArray.eval();

    imageStackArray = af::round(imageStackArray);
    imageStackArray = imageStackArray.as(u8).copy();

    std::shared_ptr<uint8_t[]> hotPixelRemovedImageStack{ new uint8_t[d->imageSizeY * d->imageSizeX * d->imageSizeZ]() };
    imageStackArray.host(hotPixelRemovedImageStack.get());

    d->hotPixelRemovedImageStack = hotPixelRemovedImageStack;

    return true;
}

auto HotPixelRemover::GetHotPixelRemovedImageStack() const -> const std::shared_ptr<uint8_t[]>& {
    return d->hotPixelRemovedImageStack;
}
