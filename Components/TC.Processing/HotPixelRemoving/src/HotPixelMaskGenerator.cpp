#include "HotPixelMaskGenerator.h"

class HotPixelMaskGenerator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    af::array imageArray{};
    int32_t windowSize{};
    double thresholdValue{};

    af::array hotPixelMask{};
};

HotPixelMaskGenerator::HotPixelMaskGenerator() : d(new Impl()) {
}

HotPixelMaskGenerator::~HotPixelMaskGenerator() = default;

auto HotPixelMaskGenerator::SetImageArray(const af::array& imageArray) -> void {
    d->imageArray = imageArray;
}

auto HotPixelMaskGenerator::SetWindowSize(const int32_t& windowSize) -> void {
    d->windowSize = windowSize;
}

auto HotPixelMaskGenerator::SetThresholdValue(const double& thresholdValue) -> void {
    d->thresholdValue = thresholdValue;
}

auto HotPixelMaskGenerator::Generate() -> bool {
    const auto imageData = d->imageArray.as(f32).copy();
    imageData.eval();

    const auto imageSizeX = imageData.dims(1);
    const auto imageSizeY = imageData.dims(0);

    const auto paddingLengthX = (d->windowSize - (imageSizeX % d->windowSize)) % d->windowSize;
    const auto paddingLengthY = (d->windowSize - (imageSizeY % d->windowSize)) % d->windowSize;

    const auto startPadSize = af::dim4{ 0,0,0,0 };
    const auto endImagePadSize = af::dim4{ paddingLengthY,paddingLengthX,0,0 };

    const auto paddedImageData = af::pad(imageData, startPadSize, endImagePadSize, af::borderType::AF_PAD_CLAMP_TO_EDGE);

    const auto unwrappedImageData = af::unwrap(paddedImageData, d->windowSize, d->windowSize, d->windowSize, d->windowSize);

    const auto meanData = af::mean(unwrappedImageData, 0);

    const auto endMeanPadSize = af::dim4{ d->windowSize * d->windowSize - 1,0,0,0 };
    const auto paddedMeanData = af::pad(meanData, startPadSize, endMeanPadSize, af::borderType::AF_PAD_CLAMP_TO_EDGE);

    const auto paddedSizeX = imageSizeX + paddingLengthX;
    const auto paddedSizeY = imageSizeY + paddingLengthY;

    const auto windowMeanPaddedData = af::wrap(paddedMeanData, paddedSizeY, paddedSizeX, d->windowSize, d->windowSize, d->windowSize, d->windowSize);
    const auto windowMeanData = windowMeanPaddedData(af::seq(0, imageSizeY - 1), af::seq(0, imageSizeX - 1));

    const auto meanSubtractedData = imageData - windowMeanData;

    d->hotPixelMask = meanSubtractedData >= d->thresholdValue;
    d->hotPixelMask.eval();

    return true;
}

auto HotPixelMaskGenerator::GetHotPixelMask() const -> const af::array& {
    return d->hotPixelMask;
}
