#pragma once

#include <memory>
#include "arrayfire.h"

class ImageStackHotPixelMaskGenerator {
public:
    ImageStackHotPixelMaskGenerator();
    ~ImageStackHotPixelMaskGenerator();

    auto SetImageStackArray(const af::array& imageStackArray)->void;
    auto SetSamplingZStep(const int32_t& samplingZStep)->void;
    auto SetWindowSize(const int32_t& windowSize)->void;
    auto SetThresholdValue(const double& thresholdValue)->void;

    auto Generate()->bool;

    auto GetHotPixelMask()const ->const af::array&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};