#include "ImageStackHotPixelMaskGenerator.h"

#include "HotPixelMaskGenerator.h"

class ImageStackHotPixelMaskGenerator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    af::array imageStackArray{};
    int32_t samplingZStep{};
    int32_t windowSize{};
    double thresholdValue{};

    af::array hotPixelMask{};
};

ImageStackHotPixelMaskGenerator::ImageStackHotPixelMaskGenerator() : d(new Impl()) {
}

ImageStackHotPixelMaskGenerator::~ImageStackHotPixelMaskGenerator() = default;

auto ImageStackHotPixelMaskGenerator::SetImageStackArray(const af::array& imageStackArray) -> void {
    d->imageStackArray = imageStackArray;
}

auto ImageStackHotPixelMaskGenerator::SetSamplingZStep(const int32_t& samplingZStep) -> void {
    d->samplingZStep = samplingZStep;
}

auto ImageStackHotPixelMaskGenerator::SetWindowSize(const int32_t& windowSize) -> void {
    d->windowSize = windowSize;
}

auto ImageStackHotPixelMaskGenerator::SetThresholdValue(const double& thresholdValue) -> void {
    d->thresholdValue = thresholdValue;
}

auto ImageStackHotPixelMaskGenerator::Generate() -> bool {
    const auto imageSizeX = static_cast<int32_t>(d->imageStackArray.dims(1));
    const auto imageSizeY = static_cast<int32_t>(d->imageStackArray.dims(0));
    const auto imageSizeZ = static_cast<int32_t>(d->imageStackArray.dims(2));

    const auto sampleNumber = imageSizeZ / d->samplingZStep;

    auto hotPixelMask = af::constant(1, imageSizeY, imageSizeX, b8);

    for (auto sampleIndex = 0; sampleIndex < sampleNumber; ++sampleIndex) {
        const auto samplingZIndex = sampleIndex * d->samplingZStep;
        const auto zSliceImageArray = d->imageStackArray(af::span, af::span, samplingZIndex);

        HotPixelMaskGenerator hotPixelMaskGenerator;
        hotPixelMaskGenerator.SetImageArray(zSliceImageArray);
        hotPixelMaskGenerator.SetWindowSize(d->windowSize);
        hotPixelMaskGenerator.SetThresholdValue(d->thresholdValue);

        if (!hotPixelMaskGenerator.Generate()) {
            return false;
        }

        const auto zSliceHotPixelMask = hotPixelMaskGenerator.GetHotPixelMask();
        hotPixelMask *= zSliceHotPixelMask;
    }

    d->hotPixelMask = hotPixelMask;
    
    return true;
}

auto ImageStackHotPixelMaskGenerator::GetHotPixelMask() const -> const af::array& {
    return d->hotPixelMask;
}
