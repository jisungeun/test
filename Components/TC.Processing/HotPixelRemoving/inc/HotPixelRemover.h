#pragma once

#include <memory>

#include "TCHotPixelRemovingExport.h"

class TCHotPixelRemoving_API HotPixelRemover {
public:
    HotPixelRemover();
    ~HotPixelRemover();

    auto SetImageStack(const std::shared_ptr<uint8_t[]>& imageStack, const int32_t& imageSizeX, 
        const int32_t& imageSizeY, const int32_t& imageSizeZ)->void; // memory order yxz;
    auto SetSamplingZStep(const int32_t& samplingZStep)->void;
    auto SetWindowSize(const int32_t& windowSize)->void;
    auto SetThresholdValue(const double& thresholdValue)->void;

    auto Remove()->bool;

    auto GetHotPixelRemovedImageStack()const->const std::shared_ptr<uint8_t[]>&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};