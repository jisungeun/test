#include <QDir>
#include <QString>
#include <catch2/catch.hpp>

#include "ImageStackHotPixelMaskGenerator.h"

#include "CompareArray.h"
#include "BinaryFileIO.h"

namespace ImageStackHotPixelMaskGeneratorTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCHotPixelRemoving/FLDataSet";

    TEST_CASE("ImageStackHotPixelMaskGenerator : unit test") {
        SECTION("ImageStackHotPixelMaskGenerator()") {
            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            CHECK(&imageStackHotPixelMaskGenerator != nullptr);
        }
        SECTION("SetImageStackArray()") {
            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetImageStackArray({});
            CHECK(&imageStackHotPixelMaskGenerator != nullptr);
        }
        SECTION("SetSamplingZStep()") {
            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetSamplingZStep(1);
            CHECK(&imageStackHotPixelMaskGenerator != nullptr);
        }
        SECTION("SetWindowSize()") {
            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetWindowSize(1);
            CHECK(&imageStackHotPixelMaskGenerator != nullptr);
        }
        SECTION("SetThresholdValue()") {
            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetThresholdValue(1);
            CHECK(&imageStackHotPixelMaskGenerator != nullptr);
        }
        SECTION("Generate()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 3;
            constexpr auto imageSizeZ = 2;
            constexpr auto numberOfElements = imageSizeX * imageSizeY * imageSizeZ;

            std::shared_ptr<float[]> imageStack{ new float[numberOfElements]() };

            for (auto index = 0; index < numberOfElements; ++index) {
                imageStack.get()[index] = index;
            }

            af::array imageStackArray{ imageSizeY, imageSizeX, imageSizeZ, imageStack.get() };

            constexpr auto windowSize = 3;
            constexpr auto samplingZStep = 1;
            constexpr auto thresholdValue = 20;

            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetWindowSize(windowSize);
            imageStackHotPixelMaskGenerator.SetImageStackArray(imageStackArray);
            imageStackHotPixelMaskGenerator.SetSamplingZStep(samplingZStep);
            imageStackHotPixelMaskGenerator.SetThresholdValue(thresholdValue);

            CHECK(imageStackHotPixelMaskGenerator.Generate() == true);
        }
        SECTION("GetHotPixelMask()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 3;
            constexpr auto imageSizeZ = 2;
            constexpr auto numberOfElements = imageSizeX * imageSizeY * imageSizeZ;

            std::shared_ptr<float[]> imageStack{ new float[numberOfElements]() };

            for (auto index = 0; index < numberOfElements; ++index) {
                imageStack.get()[index] = index;
            }

            af::array imageStackArray{ imageSizeY, imageSizeX, imageSizeZ, imageStack.get() };

            constexpr auto windowSize = 3;
            constexpr auto samplingZStep = 1;
            constexpr auto thresholdValue = 20;

            ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
            imageStackHotPixelMaskGenerator.SetWindowSize(windowSize);
            imageStackHotPixelMaskGenerator.SetImageStackArray(imageStackArray);
            imageStackHotPixelMaskGenerator.SetSamplingZStep(samplingZStep);
            imageStackHotPixelMaskGenerator.SetThresholdValue(thresholdValue);

            imageStackHotPixelMaskGenerator.Generate();

            const auto resultHotPixelMask = imageStackHotPixelMaskGenerator.GetHotPixelMask();
            CHECK(af::sum<uint8_t>(resultHotPixelMask) == 0);
        }
    }
    TEST_CASE("ImageStackHotPixelMaskGenerator : practical test") {
        const auto testFolderDir = QDir(testFolderPath);
        const auto pngFileNameList = testFolderDir.entryList({ "*.png" });

        constexpr auto imageSizeX = 1416;
        constexpr auto imageSizeY = 1416;
        constexpr auto imageSizeZ = 39;
        constexpr auto numberOfElements = imageSizeX * imageSizeY;

        af::array imageStackArray = af::constant(0, imageSizeY, imageSizeX, imageSizeZ);

        for (auto zIndex = 0; zIndex < imageSizeZ; ++zIndex) {
            const auto imageFilePath = testFolderPath + "/" + pngFileNameList.at(zIndex);
            const auto sliceImageArray = af::loadImageNative(imageFilePath.toStdString().c_str()).as(f32).copy();

            imageStackArray(af::span, af::span, zIndex) = sliceImageArray;
        }

        constexpr auto windowSize = 3;
        constexpr auto samplingZStep = 5;
        constexpr auto thresholdValue = 20;

        ImageStackHotPixelMaskGenerator imageStackHotPixelMaskGenerator;
        imageStackHotPixelMaskGenerator.SetWindowSize(windowSize);
        imageStackHotPixelMaskGenerator.SetImageStackArray(imageStackArray);
        imageStackHotPixelMaskGenerator.SetSamplingZStep(samplingZStep);
        imageStackHotPixelMaskGenerator.SetThresholdValue(thresholdValue);

        CHECK(imageStackHotPixelMaskGenerator.Generate() == true);

        const auto hotPixelAnswerFilePath = testFolderPath + "/stackHotPixel.raw";

        const auto hotPixelAnswerData = ReadFile_uint8_t(hotPixelAnswerFilePath.toStdString(), numberOfElements);
        std::shared_ptr<uint8_t[]> hotPixelResultData{ new uint8_t[numberOfElements]() };

        const auto resultHotPixelMask = imageStackHotPixelMaskGenerator.GetHotPixelMask();

        resultHotPixelMask.host(hotPixelResultData.get());
        CHECK(CompareArray(hotPixelAnswerData.get(), hotPixelResultData.get(), numberOfElements));
    }
}