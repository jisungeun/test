//#include <iostream>
#include <catch2/catch.hpp>

#include "HotPixelMaskGenerator.h"
#include <QString>

#include "BinaryFileIO.h"
#include "CompareArray.h"

namespace HotPixelMaskGeneratorTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCHotPixelRemoving/FLDataSet";

    TEST_CASE("HotPixelMaskGenerator : unit test") {
        SECTION("HotPixelMaskGenerator()") {
            HotPixelMaskGenerator hotPixelMaskGenerator;
            CHECK(&hotPixelMaskGenerator != nullptr);
        }
        SECTION("SetImageArray()") {
            HotPixelMaskGenerator hotPixelMaskGenerator;
            hotPixelMaskGenerator.SetImageArray({});
            CHECK(&hotPixelMaskGenerator != nullptr);
        }
        SECTION("SetWindowSize()"){
            HotPixelMaskGenerator hotPixelMaskGenerator;
            hotPixelMaskGenerator.SetWindowSize(1);
            CHECK(&hotPixelMaskGenerator != nullptr);
        }
        SECTION("SetThresholdValue()"){
            HotPixelMaskGenerator hotPixelMaskGenerator;
            hotPixelMaskGenerator.SetThresholdValue(1);
            CHECK(&hotPixelMaskGenerator != nullptr);
        }
        SECTION("Generate()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 6;
            constexpr auto numberOfElements = imageSizeX * imageSizeY;

            auto imageArray = af::constant(1, imageSizeY, imageSizeX, f32);
            for (auto index = 0; index < numberOfElements; ++index) {
                imageArray(index) = index;
            }

            constexpr int32_t windowSize = 3;
            constexpr double thresholdValue = 20;

            HotPixelMaskGenerator hotPixelMaskGenerator;
            hotPixelMaskGenerator.SetImageArray(imageArray);
            hotPixelMaskGenerator.SetWindowSize(windowSize);
            hotPixelMaskGenerator.SetThresholdValue(thresholdValue);

            CHECK(hotPixelMaskGenerator.Generate() == true);
        }
        SECTION("GetHotPixelMask()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 6;
            constexpr auto numberOfElements = imageSizeX * imageSizeY;

            auto imageArray = af::constant(1, imageSizeY, imageSizeX, f32);
            for (auto index = 0; index < numberOfElements; ++index) {
                imageArray(index) = index;
            }

            constexpr int32_t windowSize = 3;
            constexpr double thresholdValue = 20;

            HotPixelMaskGenerator hotPixelMaskGenerator;
            hotPixelMaskGenerator.SetImageArray(imageArray);
            hotPixelMaskGenerator.SetWindowSize(windowSize);
            hotPixelMaskGenerator.SetThresholdValue(thresholdValue);

            hotPixelMaskGenerator.Generate();

            const auto hotPixelMask = hotPixelMaskGenerator.GetHotPixelMask();
            CHECK(hotPixelMask.type() == b8);
            CHECK(hotPixelMask.dims(1) == imageSizeX);
            CHECK(hotPixelMask.dims(0) == imageSizeY);
            CHECK(af::sum<float>(hotPixelMask) == 0);
        }
    }
    TEST_CASE("HotPixelMaskGenerator : practical test") {
        const auto imageFilePath = testFolderPath + "/0000.png";
        const auto imageData = af::loadImageNative(imageFilePath.toStdString().c_str()).as(f32).copy();

        constexpr double thresholdValue = 20;
        constexpr int32_t windowSize = 3;

        HotPixelMaskGenerator hotPixelMaskGenerator;
        hotPixelMaskGenerator.SetImageArray(imageData);
        hotPixelMaskGenerator.SetThresholdValue(thresholdValue);
        hotPixelMaskGenerator.SetWindowSize(windowSize);

        //const auto start = std::chrono::system_clock::now();
        //for(auto i = 0; i< 100; ++i) {
            CHECK(hotPixelMaskGenerator.Generate() == true);
        //}
        //const auto end = std::chrono::system_clock::now();

        //const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        //std::cout << duration.count() / 100 << " micro-sec" << std::endl;


        const auto hotPixelMask = hotPixelMaskGenerator.GetHotPixelMask();

        constexpr auto imageSizeX = 1416;
        constexpr auto imageSizeY = 1416;
        constexpr auto numberOfElements = imageSizeX * imageSizeY;

        const std::shared_ptr<uint8_t[]> hotPixelData{ new uint8_t[numberOfElements]() };

        hotPixelMask.host(hotPixelData.get());

        const auto answerMaskFilePath = testFolderPath + "/0000HotPixel.raw";
        const auto answerHotPixelData = ReadFile_uint8_t(answerMaskFilePath.toStdString(), numberOfElements);
        CHECK(CompareArray(hotPixelData.get(), answerHotPixelData.get(), numberOfElements));
    }
}