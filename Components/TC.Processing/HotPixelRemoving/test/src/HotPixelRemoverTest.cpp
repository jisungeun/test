#include <QString>
#include <catch2/catch.hpp>

#include <QDir>

#include "arrayfire.h"

#include "HotPixelRemover.h"
#include "CompareArray.h"
#include "BinaryFileIO.h"

namespace HotPixelRemoverTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCHotPixelRemoving/FLDataSet";

    TEST_CASE("HotPixelRemover : unit test") {
        SECTION("HotPixelRemover()") {
            HotPixelRemover hotPixelRemover;
            CHECK(&hotPixelRemover != nullptr);
        }
        SECTION("SetImageStack()") {
            std::shared_ptr<uint8_t[]> imageStack{ new uint8_t[1]() };
            constexpr auto imageSizeX = 1;
            constexpr auto imageSizeY = 1;
            constexpr auto imageSizeZ = 1;

            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetImageStack(imageStack, imageSizeX, imageSizeY, imageSizeZ);
            CHECK(&hotPixelRemover != nullptr);
        }
        SECTION("SetSamplingZStep()"){
            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetSamplingZStep(1);
            CHECK(&hotPixelRemover != nullptr);
        }
        SECTION("SetWindowSize()"){
            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetWindowSize(1);
            CHECK(&hotPixelRemover != nullptr);
        }
        SECTION("SetThresholdValue()"){
            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetThresholdValue(1);
            CHECK(&hotPixelRemover != nullptr);
        }
        SECTION("Remove()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 3;
            constexpr auto imageSizeZ = 2;
            constexpr auto numberOfElements = imageSizeX * imageSizeY * imageSizeZ;
            
            std::shared_ptr<uint8_t[]> imageStack{ new uint8_t[numberOfElements]() };

            for (auto index = 0; index < numberOfElements; ++index) {
                imageStack.get()[index] = index;
            }

            constexpr auto windowSize = 3;
            constexpr auto samplingZStep = 1;
            constexpr auto thresholdValue = 20;

            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetImageStack(imageStack, imageSizeX, imageSizeY, imageSizeZ);
            hotPixelRemover.SetWindowSize(windowSize);
            hotPixelRemover.SetSamplingZStep(samplingZStep);
            hotPixelRemover.SetThresholdValue(thresholdValue);

            CHECK(hotPixelRemover.Remove() == true);
        }
        SECTION("GetHotPixelRemovedImageStack()") {
            constexpr auto imageSizeX = 3;
            constexpr auto imageSizeY = 3;
            constexpr auto imageSizeZ = 2;
            constexpr auto numberOfElements = imageSizeX * imageSizeY * imageSizeZ;

            std::shared_ptr<uint8_t[]> imageStack{ new uint8_t[numberOfElements]() };

            for (auto index = 0; index < numberOfElements; ++index) {
                imageStack.get()[index] = index;
            }

            constexpr auto windowSize = 3;
            constexpr auto samplingZStep = 1;
            constexpr auto thresholdValue = 20;

            HotPixelRemover hotPixelRemover;
            hotPixelRemover.SetImageStack(imageStack, imageSizeX, imageSizeY, imageSizeZ);
            hotPixelRemover.SetWindowSize(windowSize);
            hotPixelRemover.SetSamplingZStep(samplingZStep);
            hotPixelRemover.SetThresholdValue(thresholdValue);

            hotPixelRemover.Remove();

            const auto hotPixelRemovedImageStack = hotPixelRemover.GetHotPixelRemovedImageStack();
            CHECK(CompareArray(imageStack.get(), hotPixelRemovedImageStack.get(), numberOfElements));
        }

    }
    TEST_CASE("HotPixelRemover : practical test") {
        const auto testFolderDir = QDir(testFolderPath);
        const auto pngFileNameList = testFolderDir.entryList({ "*.png" });

        constexpr auto imageSizeX = 1416;
        constexpr auto imageSizeY = 1416;
        constexpr auto imageSizeZ = 39;
        constexpr auto numberOfElements = imageSizeX * imageSizeY * imageSizeZ;

        af::array imageStackArray = af::constant(0, imageSizeY, imageSizeX, imageSizeZ, u8);

        for (auto zIndex = 0; zIndex < imageSizeZ; ++zIndex) {
            const auto imageFilePath = testFolderPath + "/" + pngFileNameList.at(zIndex);
            const auto sliceImageArray = af::loadImageNative(imageFilePath.toStdString().c_str()).as(u8).copy();

            imageStackArray(af::span, af::span, zIndex) = sliceImageArray;
        }

        std::shared_ptr<uint8_t[]> imageStackData{ new uint8_t[numberOfElements]() };
        imageStackArray.host(imageStackData.get());
        
        constexpr auto windowSize = 3;
        constexpr auto samplingZStep = 5;
        constexpr auto thresholdValue = 20;

        HotPixelRemover hotPixelRemover;
        hotPixelRemover.SetImageStack(imageStackData, imageSizeX, imageSizeY, imageSizeZ);
        hotPixelRemover.SetWindowSize(windowSize);
        hotPixelRemover.SetSamplingZStep(samplingZStep);
        hotPixelRemover.SetThresholdValue(thresholdValue);

        CHECK(hotPixelRemover.Remove() == true);

        const auto hotPixelRemovedResultData = hotPixelRemover.GetHotPixelRemovedImageStack();

        const auto answerFilePath = testFolderPath + "/hotPixelRemovedData.raw";
        const auto hotPixelRemovedAnswerData = ReadFile_uint8_t(answerFilePath.toStdString(), numberOfElements);

        CHECK(CompareArray(hotPixelRemovedResultData.get(), hotPixelRemovedAnswerData.get(), numberOfElements));
    }
}