#include <catch2/catch.hpp>

#include "IBFProcessorOutput.h"

namespace IBFProcessorOutputTest {
    class BFProcessorOutputForTest final : public IBFProcessorOutput {
    public:
        BFProcessorOutputForTest() = default;
        ~BFProcessorOutputForTest() = default;

        auto SetBFProcessorResult(const BFProcessorResult& flProcessorResult) -> void override {
            this->setBFProcessorResultTriggered = true;
        }

        bool setBFProcessorResultTriggered{ false };
    };

    TEST_CASE("IBFProcessorOutputTest") {
        SECTION("SetBFProcessorResult()") {
            BFProcessorOutputForTest bfProcessorOutput;
            CHECK(bfProcessorOutput.setBFProcessorResultTriggered == false);
            bfProcessorOutput.SetBFProcessorResult({});
            CHECK(bfProcessorOutput.setBFProcessorResultTriggered == true);
        }
    }
}