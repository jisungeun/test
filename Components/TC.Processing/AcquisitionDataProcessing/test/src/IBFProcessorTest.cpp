#include <catch2/catch.hpp>

#include "IBFProcessor.h"

namespace IBFProcessorTest {
    class BFProcessorForTest final : public IBFProcessor {
    public:
        BFProcessorForTest() = default;
        ~BFProcessorForTest() = default;

        auto SetBFProcessorInput(const BFProcessorInput& input) -> void override {
            this->setBFProcessorInputTriggered = true;
        }
        auto SetOutputPort(const IBFProcessorOutput::Pointer& outputPort) -> void override {
            this->setOutputPortTriggered = true;
        }
        auto Process() -> bool override {
            this->processTriggered = true;
            return true;
        }

        bool setBFProcessorInputTriggered{ false };
        bool setOutputPortTriggered{ false };
        bool processTriggered{ false };
    };

    TEST_CASE("IBFProcessorTest") {
        SECTION("SetBFProcessorInput()") {
            BFProcessorForTest bfProcessor;
            CHECK(bfProcessor.setBFProcessorInputTriggered == false);
            bfProcessor.SetBFProcessorInput({});
            CHECK(bfProcessor.setBFProcessorInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            BFProcessorForTest bfProcessor;
            CHECK(bfProcessor.setOutputPortTriggered == false);
            bfProcessor.SetOutputPort({});
            CHECK(bfProcessor.setOutputPortTriggered == true);
        }
        SECTION("Process()") {
            BFProcessorForTest bfProcessor;
            CHECK(bfProcessor.processTriggered == false);
            bfProcessor.Process();
            CHECK(bfProcessor.processTriggered == true);
        }
    }
}