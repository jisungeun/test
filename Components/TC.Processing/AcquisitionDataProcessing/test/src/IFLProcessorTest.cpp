#include <catch2/catch.hpp>

#include "IFLProcessor.h"

namespace IFLProcessorTest {
    class FLProcessorForTest final : public IFLProcessor {
    public:
        FLProcessorForTest() = default;
        ~FLProcessorForTest() = default;

        auto SetFLProcessorInput(const FLProcessorInput& input) -> void override {
            this->setFLProcessorInputTriggered = true;
        }
        auto SetOutputPort(const IFLProcessorOutput::Pointer& outputPort) -> void override {
            this->setOutputPortTriggered = true;
        }
        auto Process() -> bool override {
            this->processTriggered = true;
            return true;
        }

        bool setFLProcessorInputTriggered{ false };
        bool setOutputPortTriggered{ false };
        bool processTriggered{ false };
    };

    TEST_CASE("IFLProcessorTest") {
        SECTION("SetFLProcessorInput()") {
            FLProcessorForTest flProcessor;
            CHECK(flProcessor.setFLProcessorInputTriggered == false);
            flProcessor.SetFLProcessorInput({});
            CHECK(flProcessor.setFLProcessorInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            FLProcessorForTest flProcessor;
            CHECK(flProcessor.setOutputPortTriggered == false);
            flProcessor.SetOutputPort({});
            CHECK(flProcessor.setOutputPortTriggered == true);
        }
        SECTION("Process()") {
            FLProcessorForTest flProcessor;
            CHECK(flProcessor.processTriggered == false);
            flProcessor.Process();
            CHECK(flProcessor.processTriggered == true);
        }
    }
}