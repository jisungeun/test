#include <catch2/catch.hpp>

#include "IFLProcessorOutput.h"

namespace IFLProcessorOutputTest {
    class FLProcessorOutputForTest final : public IFLProcessorOutput {
    public:
        FLProcessorOutputForTest() = default;
        ~FLProcessorOutputForTest() = default;

        auto SetFLProcessorResult(const FLProcessorResult& flProcessorResult) -> void override {
            this->setFLProcessorResultTriggered = true;
        }

        bool setFLProcessorResultTriggered{ false };
    };

    TEST_CASE("IFLProcessorOutputTest") {
        SECTION("SetFLProcessorResult()") {
            FLProcessorOutputForTest flProcessorOutput;
            CHECK(flProcessorOutput.setFLProcessorResultTriggered == false);
            flProcessorOutput.SetFLProcessorResult({});
            CHECK(flProcessorOutput.setFLProcessorResultTriggered == true);
        }
    }
}