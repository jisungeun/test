#include <catch2/catch.hpp>

#include "HTProcessorInput.h"

namespace HTProcessorInputTest {
    TEST_CASE("HTProcessorInput : unit test") {
        SECTION("HTProcessorInput()") {
            HTProcessorInput htProcessorInput;
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("HTProcessorInput(other)") {
            HTProcessorInput srcHtProcessorInput;
            srcHtProcessorInput.SetMediumRI(1);

            HTProcessorInput destHtProcessorInput(srcHtProcessorInput);
            CHECK(destHtProcessorInput.GetMediumRI() == 1);
        }
        SECTION("operator=()") {
            HTProcessorInput srcHtProcessorInput;
            srcHtProcessorInput.SetMediumRI(1);

            HTProcessorInput destHtProcessorInput;
            destHtProcessorInput = srcHtProcessorInput;
            CHECK(destHtProcessorInput.GetMediumRI() == 1);
        }
        SECTION("SetSampleDataFolderPath()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetSampleDataFolderPath("");
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetSampleDataFolderPath()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetSampleDataFolderPath("test");
            CHECK(htProcessorInput.GetSampleDataFolderPath() == "test");
        }
        SECTION("SetBackgroundDataFolderPath()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetBackgroundDataFolderPath("test");
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetBackgroundDataFolderPath()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetBackgroundDataFolderPath("test");
            CHECK(htProcessorInput.GetBackgroundDataFolderPath() == "test");
        }
        SECTION("SetObjectiveNA()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetObjectiveNA(1);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetObjectiveNA()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetObjectiveNA(1);
            CHECK(htProcessorInput.GetObjectiveNA() == 1);
        }
        SECTION("SetCondenserNA()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetCondenserNA(1);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetCondenserNA()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetCondenserNA(1);
            CHECK(htProcessorInput.GetCondenserNA() == 1);
        }
        SECTION("SetPixelSizeOfImagingSensor()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetPixelSizeOfImagingSensor()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(htProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Meter) == 1);
        }
        SECTION("SetMagnificationOfSystem()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetMagnificationOfSystem(1);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetMagnificationOfSystem()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetMagnificationOfSystem(1);
            CHECK(htProcessorInput.GetMagnificationOfSystem() == 1);
        }
        SECTION("SetZStepLength()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetZStepLength(1, LengthUnit::Meter);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetZStepLength()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetZStepLength(1, LengthUnit::Meter);
            CHECK(htProcessorInput.GetZStepLength(LengthUnit::Meter) == 1);
        }
        SECTION("SetMediumRI()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetMediumRI(1);
            CHECK(&htProcessorInput != nullptr);
        }
        SECTION("GetMediumRI()") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetMediumRI(1);
            CHECK(htProcessorInput.GetMediumRI() == 1);
        }
    }

    TEST_CASE("HTProcessorInput : practical test") {
        SECTION("DataStructure") {
            HTProcessorInput htProcessorInput;
            htProcessorInput.SetSampleDataFolderPath("1");
            htProcessorInput.SetBackgroundDataFolderPath("2");
            htProcessorInput.SetObjectiveNA(3);
            htProcessorInput.SetCondenserNA(4);
            htProcessorInput.SetPixelSizeOfImagingSensor(5, LengthUnit::Meter);
            htProcessorInput.SetMagnificationOfSystem(6);
            htProcessorInput.SetZStepLength(7, LengthUnit::Meter);
            htProcessorInput.SetMediumRI(8);

            CHECK(htProcessorInput.GetSampleDataFolderPath() == "1");
            CHECK(htProcessorInput.GetBackgroundDataFolderPath() == "2");
            CHECK(htProcessorInput.GetObjectiveNA() == 3);
            CHECK(htProcessorInput.GetCondenserNA() == 4);
            CHECK(htProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Meter) == 5);
            CHECK(htProcessorInput.GetMagnificationOfSystem() == 6);
            CHECK(htProcessorInput.GetZStepLength(LengthUnit::Meter) == 7);
            CHECK(htProcessorInput.GetMediumRI() == 8);
        }
        
    }
}