#include <catch2/catch.hpp>

#include <fstream>
#include "BFProcessor.h"

namespace BFProcessorTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCAcquisitionDataProcessingTest";
    const QString pngFolderPath = testFolderPath + "/ColorImage/RGB";

    constexpr int32_t sizeX = 171;
    constexpr int32_t sizeY = 170;
    constexpr int32_t numberOfElements = sizeX * sizeY * 3; // rgb : 3

    auto CompareArray(const uint8_t* result, const uint8_t* answer, const size_t& numberOfElements)->bool {
        for (auto index = 0; index < numberOfElements; ++index) {
            const auto resultValue = result[index];
            const auto answerValue = answer[index];

            if (resultValue != answerValue) {
                return false;
            }
        }
        return true;
    }

    auto ReadAnswerData()->std::shared_ptr<uint8_t[]> {
        const auto answerFilePath = pngFolderPath + "/binaryData";
        constexpr auto byteCount = numberOfElements;

        std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };

        std::ifstream fileStream(answerFilePath.toStdString(), std::ios::binary);
        fileStream.read(reinterpret_cast<char*>(readData.get()), byteCount);
        fileStream.close();

        return readData;
    }

    class BFProcessorOutputForTest final : public IBFProcessorOutput {
    public:
        BFProcessorOutputForTest() = default;
        ~BFProcessorOutputForTest() = default;

        auto SetBFProcessorResult(const BFProcessorResult& bfProcessorResult) -> void override {
            this->result = bfProcessorResult;
        }

        BFProcessorResult result;
    };

    TEST_CASE("BFProcessor : unit test") {
        SECTION("BFProcessor()") {
            BFProcessor processor;
            CHECK(&processor != nullptr);
        }
        SECTION("SetBFProcessorInput()") {
            BFProcessor processor;
            processor.SetBFProcessorInput({});
            CHECK(&processor != nullptr);
        }
        SECTION("SetOutputPort()") {
            BFProcessor processor;
            processor.SetOutputPort({});
            CHECK(&processor != nullptr);
        }
        SECTION("Process()") {
            constexpr float magnificationOfSystem = 50.f;
            constexpr float pixelSizeOfImagingSensor = 4.5f;
            const QString sampleDataFolderPath = pngFolderPath;

            BFProcessorInput input;
            input.SetMagnificationOfSystem(magnificationOfSystem);
            input.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
            input.SetSampleDataFolderPath(sampleDataFolderPath);

            auto output = new BFProcessorOutputForTest;
            IBFProcessorOutput::Pointer outputPort{ output };
            
            BFProcessor processor;
            processor.SetBFProcessorInput(input);
            processor.SetOutputPort(outputPort);
            const auto processResult = processor.Process();

            CHECK(processResult == true);
        }
    }
    TEST_CASE("BFProcessor : practical test") {
        constexpr float magnificationOfSystem = 50.f;
        constexpr float pixelSizeOfImagingSensor = 4.5f;
        const QString sampleDataFolderPath = pngFolderPath;

        BFProcessorInput input;
        input.SetMagnificationOfSystem(magnificationOfSystem);
        input.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
        input.SetSampleDataFolderPath(sampleDataFolderPath);

        auto output = new BFProcessorOutputForTest;
        IBFProcessorOutput::Pointer outputPort{ output };

        BFProcessor processor;
        processor.SetBFProcessorInput(input);
        processor.SetOutputPort(outputPort);
        const auto processResult = processor.Process();

        CHECK(processResult == true);

        constexpr auto pixelWorldSizeX = pixelSizeOfImagingSensor / magnificationOfSystem;
        constexpr auto pixelWorldSizeY = pixelSizeOfImagingSensor / magnificationOfSystem;

        const auto resultData = output->result.GetData();
        const auto answerData = ReadAnswerData();

        CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));

        CHECK(output->result.GetDataSizeX() == sizeX);
        CHECK(output->result.GetDataSizeY() == sizeY);

        CHECK(static_cast<int32_t>(output->result.GetPixelWorldSizeX(LengthUnit::Micrometer) * 1000) == static_cast<int32_t>(pixelWorldSizeX * 1000));
        CHECK(static_cast<int32_t>(output->result.GetPixelWorldSizeY(LengthUnit::Micrometer) * 1000) == static_cast<int32_t>(pixelWorldSizeY * 1000));
        
    }
}