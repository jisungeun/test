#include <catch2/catch.hpp>

#include "FLProcessorInput.h"

namespace FLProcessorInputTest {
    TEST_CASE("FLProcessorInput : unit test") {
        SECTION("FLProcessorInput()") {
            FLProcessorInput flProcessorInput;
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("FLProcessorInput(other)") {
            FLProcessorInput srcFlProcessorInput;
            srcFlProcessorInput.SetMediumRI(1);

            FLProcessorInput destFlProcessorInput(srcFlProcessorInput);
            CHECK(destFlProcessorInput.GetMediumRI() == 1);
        }
        SECTION("operator=()") {
            FLProcessorInput srcFlProcessorInput;
            srcFlProcessorInput.SetMediumRI(1);

            FLProcessorInput destFlProcessorInput;
            destFlProcessorInput = srcFlProcessorInput;
            CHECK(destFlProcessorInput.GetMediumRI() == 1);
        }
        SECTION("SetSampleDataFolderPath()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetSampleDataFolderPath("");
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetSampleDataFolderPath()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetSampleDataFolderPath("test");
            CHECK(flProcessorInput.GetSampleDataFolderPath() == "test");
        }
        SECTION("SetObjectiveNA()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetObjectiveNA(1);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetObjectiveNA()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetObjectiveNA(1);
            CHECK(flProcessorInput.GetObjectiveNA() == 1);
        }
        SECTION("SetCondenserNA()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetCondenserNA(1);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetCondenserNA()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetCondenserNA(1);
            CHECK(flProcessorInput.GetCondenserNA() == 1);
        }
        SECTION("SetPixelSizeOfImagingSensor()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetPixelSizeOfImagingSensor()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(flProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Meter) == 1);
        }
        SECTION("SetMagnificationOfSystem()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetMagnificationOfSystem(1);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetMagnificationOfSystem()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetMagnificationOfSystem(1);
            CHECK(flProcessorInput.GetMagnificationOfSystem() == 1);
        }
        SECTION("SetZStepLength()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetZStepLength(1, LengthUnit::Meter);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("SetZStepLength()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetZStepLength(1, LengthUnit::Meter);
            CHECK(flProcessorInput.GetZStepLength(LengthUnit::Meter) == 1);
        }
        SECTION("SetMediumRI()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetMediumRI(1);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetMediumRI()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetMediumRI(1);
            CHECK(flProcessorInput.GetMediumRI() == 1);
        }
        SECTION("SetWaveLength()"){
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetWaveLength(1, LengthUnit::Meter);
            CHECK(&flProcessorInput != nullptr);
        }
        SECTION("GetWaveLength()") {
            FLProcessorInput flProcessorInput;
            flProcessorInput.SetWaveLength(1, LengthUnit::Meter);
            CHECK(flProcessorInput.GetWaveLength(LengthUnit::Meter) == 1);
        }
    }

    TEST_CASE("FLProcessorInput : practical test") {
        FLProcessorInput flProcessorInput;
        flProcessorInput.SetSampleDataFolderPath("1");
        flProcessorInput.SetObjectiveNA(2);
        flProcessorInput.SetCondenserNA(3);
        flProcessorInput.SetPixelSizeOfImagingSensor(4, LengthUnit::Meter);
        flProcessorInput.SetMagnificationOfSystem(5);
        flProcessorInput.SetZStepLength(6, LengthUnit::Meter);
        flProcessorInput.SetMediumRI(7);
        flProcessorInput.SetWaveLength(8, LengthUnit::Meter);

        CHECK(flProcessorInput.GetSampleDataFolderPath() == "1");
        CHECK(flProcessorInput.GetObjectiveNA() == 2);
        CHECK(flProcessorInput.GetCondenserNA() == 3);
        CHECK(flProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Meter) == 4);
        CHECK(flProcessorInput.GetMagnificationOfSystem() == 5);
        CHECK(flProcessorInput.GetZStepLength(LengthUnit::Meter) == 6);
        CHECK(flProcessorInput.GetMediumRI() == 7);
        CHECK(flProcessorInput.GetWaveLength(LengthUnit::Meter) == 8);
    }
}