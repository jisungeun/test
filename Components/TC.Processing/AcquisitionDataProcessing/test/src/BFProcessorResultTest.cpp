#include <catch2/catch.hpp>

#include "BFProcessorResult.h"

namespace BFProcessorResultTest {
    TEST_CASE("BFProcessorResultTest") {
        SECTION("BFProcessorResult()") {
            BFProcessorResult bfProcessorResult;
            CHECK(&bfProcessorResult != nullptr);
        }
        SECTION("BFProcessorResult(other)") {
            BFProcessorResult srcBFProcessorResult;
            srcBFProcessorResult.SetDataSize(1, 2);

            BFProcessorResult destBFProcessorResult(srcBFProcessorResult);
            CHECK(destBFProcessorResult.GetDataSizeX() == 1);
            CHECK(destBFProcessorResult.GetDataSizeY() == 2);
        }
        SECTION("operator=()") {
            BFProcessorResult srcBFProcessorResult;
            srcBFProcessorResult.SetDataSize(1, 2);

            BFProcessorResult destBFProcessorResult;
            destBFProcessorResult = srcBFProcessorResult;
            CHECK(destBFProcessorResult.GetDataSizeX() == 1);
            CHECK(destBFProcessorResult.GetDataSizeY() == 2);
        }
        SECTION("SetDataSize()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetDataSize(1, 2);
            CHECK(&bfProcessorResult != nullptr);
        }
        SECTION("GetDataSizeX()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetDataSize(1, 2);
            CHECK(bfProcessorResult.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetDataSize(1, 2);
            CHECK(bfProcessorResult.GetDataSizeY() == 2);
        }
        SECTION("SetPixelWorldSize()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(&bfProcessorResult != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(bfProcessorResult.GetPixelWorldSizeX(LengthUnit::Meter) == 1);
        }
        SECTION("GetPixelWorldSizeY()") {
            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(bfProcessorResult.GetPixelWorldSizeY(LengthUnit::Meter) == 2);
        }
        SECTION("SetData()") {
            std::shared_ptr<uint8_t[]> data{ new uint8_t[1]() };

            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetData(data);
            CHECK(&bfProcessorResult != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<uint8_t[]> data{ new uint8_t[1]() };

            BFProcessorResult bfProcessorResult;
            bfProcessorResult.SetData(data);
            CHECK(bfProcessorResult.GetData() == data);
        }
    }
}