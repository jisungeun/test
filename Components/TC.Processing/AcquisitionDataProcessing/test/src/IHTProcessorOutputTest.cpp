#include <catch2/catch.hpp>

#include "IHTProcessorOutput.h"

namespace IHTProcessorOutputTest {
    class HTProcessorOutputForTest final : public IHTProcessorOutput {
    public:
        HTProcessorOutputForTest() = default;
        ~HTProcessorOutputForTest() = default;

        auto SetHTProcessorResult(const HTProcessorResult& htProcessorResult) -> void override {
            this->setHTProcessorResultTriggered = true;
        }

        bool setHTProcessorResultTriggered{ false };
    };

    TEST_CASE("IHTProcessorOutputTest") {
        SECTION("SetHTProcessorResult()") {
            HTProcessorOutputForTest htProcessorOutput;
            CHECK(htProcessorOutput.setHTProcessorResultTriggered == false);
            htProcessorOutput.SetHTProcessorResult({});
            CHECK(htProcessorOutput.setHTProcessorResultTriggered == true);
        }
    }
}