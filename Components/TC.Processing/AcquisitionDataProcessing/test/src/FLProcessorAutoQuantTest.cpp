#include <QSettings>
#include <catch2/catch.hpp>

#include "FLProcessorAutoQuant.h"

namespace FLProcessorAutoQuantTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCAcquisitionDataProcessingTest";
    const QString pngFolderPath = testFolderPath + "/DataSet";

    constexpr auto sizeX = 1080;
    constexpr auto sizeY = 1080;
    constexpr auto sizeZ = 57;

    const QString registryKeyPath = "HKEY_CURRENT_USER\\Software\\Tomocube_dev\\TCAcquisitionDataProcessingTest\\Strings";

    class FLProcessorOutputForTest final : public IFLProcessorOutput {
    public:
        FLProcessorOutputForTest() = default;
        ~FLProcessorOutputForTest() = default;

        auto SetFLProcessorResult(const FLProcessorResult& flProcessorResult) -> void override {
            this->result = flProcessorResult;
        }
        FLProcessorResult result;
    };

    auto WriteRegistry()->void {
        QSettings settings(registryKeyPath, QSettings::NativeFormat);
        settings.setValue("0", QString("BC86 3219 4926 1418 105A 66B2 A052 A2C0"));
        settings.setValue("1", QString("8318 6214 EB09 3A1B 0623 A881 0C23 11B8"));
        settings.setValue("2", QString("C280 662D E180 323B BC80 0209 0183 90C8"));
        settings.setValue("3", QString("1D80 1698 4102 1619 085A 61B2 82D2 B2C0"));
        settings.setValue("4", QString("1640 A219 9236 18A8 3291 B2DD D123 C2A1"));
        settings.setValue("5", QString("A823 1B20 52B1 D920 82A1 70B1 C0C1 9918"));
        settings.setValue("6", QString("2301 AB10 9012 E83E 2322 44A3 C125 125E"));
        settings.setValue("7", QString("BA12 90D1 105A 66B2 1131 B123 12A2 9012"));
        settings.setValue("8", QString("C120 5312 3320 A029 01AB 43B2 C087 5420"));
        settings.setValue("9", QString("19B0 B6D2 4E20 1B13 06C2 81DD B238 A289"));
    }

    TEST_CASE("FLProcessorAutoQuant : unit test") {
        SECTION("FLProcessorAutoQuant()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("SetFLProcessorInput()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetFLProcessorInput({});
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("SetOutputPort()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetOutputPort({});
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("SetSampleDepth()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetSampleDepth(0, LengthUnit::Micrometer);
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("SetProcessingTempFolderPath()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetProcessingTempFolderPath("");
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("SetDllUnlockRegistryKeyLocation()") {
            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetDllUnlockRegistryKeyLocation("");
            CHECK(&flProcessorAutoQuant != nullptr);
        }
        SECTION("Process()") {
            WriteRegistry();

            const QString sampleDataFolderPath = pngFolderPath;
            constexpr float objectiveNA = 1.2f;
            constexpr float condenserNA = objectiveNA; //no use
            constexpr float pixelSizeOfImagingSensor = 4.5f;
            constexpr float magnificationOfSystem = 50.f;
            constexpr float zStepLength = 0.156f;
            constexpr float mediumRI = 1.337f;
            constexpr float waveLength = 0.519f;

            FLProcessorInput input;
            input.SetSampleDataFolderPath(sampleDataFolderPath);
            input.SetObjectiveNA(objectiveNA);
            input.SetCondenserNA(condenserNA);
            input.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
            input.SetMagnificationOfSystem(magnificationOfSystem);
            input.SetZStepLength(zStepLength, LengthUnit::Micrometer);
            input.SetMediumRI(mediumRI);
            input.SetWaveLength(waveLength, LengthUnit::Micrometer);

            auto output = new FLProcessorOutputForTest;
            IFLProcessorOutput::Pointer outputPort{ output };

            constexpr float sampleDepth = 3.0f;
            const QString tempFolderPath = "C:/Temp/ProcessingTempFolder";

            FLProcessorAutoQuant flProcessorAutoQuant;
            flProcessorAutoQuant.SetDllUnlockRegistryKeyLocation(registryKeyPath);
            flProcessorAutoQuant.SetFLProcessorInput(input);
            flProcessorAutoQuant.SetOutputPort(outputPort);
            flProcessorAutoQuant.SetSampleDepth(sampleDepth, LengthUnit::Micrometer);
            flProcessorAutoQuant.SetProcessingTempFolderPath(tempFolderPath);

            const auto processResult = flProcessorAutoQuant.Process();
            CHECK(processResult == true);
        }
    }

    TEST_CASE("FLProcessorAutoQuant : practical test") {
        WriteRegistry();

        const QString sampleDataFolderPath = pngFolderPath;
        constexpr float objectiveNA = 1.2f;
        constexpr float condenserNA = objectiveNA; //no use
        constexpr float pixelSizeOfImagingSensor = 4.5f;
        constexpr float magnificationOfSystem = 50.f;
        constexpr float zStepLength = 0.156f;
        constexpr float mediumRI = 1.337f;
        constexpr float waveLength = 0.519f;

        FLProcessorInput input;
        input.SetSampleDataFolderPath(sampleDataFolderPath);
        input.SetObjectiveNA(objectiveNA);
        input.SetCondenserNA(condenserNA);
        input.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
        input.SetMagnificationOfSystem(magnificationOfSystem);
        input.SetZStepLength(zStepLength, LengthUnit::Micrometer);
        input.SetMediumRI(mediumRI);
        input.SetWaveLength(waveLength, LengthUnit::Micrometer);

        auto output = new FLProcessorOutputForTest;
        IFLProcessorOutput::Pointer outputPort{ output };

        constexpr float sampleDepth = 3.0f;
        const QString tempFolderPath = "C:/Temp/ProcessingTempFolder";

        FLProcessorAutoQuant flProcessorAutoQuant;
        flProcessorAutoQuant.SetDllUnlockRegistryKeyLocation(registryKeyPath);
        flProcessorAutoQuant.SetFLProcessorInput(input);
        flProcessorAutoQuant.SetOutputPort(outputPort);
        flProcessorAutoQuant.SetSampleDepth(sampleDepth, LengthUnit::Micrometer);
        flProcessorAutoQuant.SetProcessingTempFolderPath(tempFolderPath);

        const auto processResult = flProcessorAutoQuant.Process();
        CHECK(processResult == true);

        constexpr auto pixelWorldSizeX = pixelSizeOfImagingSensor / magnificationOfSystem;
        constexpr auto pixelWorldSizeY = pixelSizeOfImagingSensor / magnificationOfSystem;
        constexpr auto pixelWorldSizeZ = zStepLength * mediumRI;

        const auto resultData = output->result.GetData();
        CHECK(output->result.GetDataSizeX() == sizeX);
        CHECK(output->result.GetDataSizeY() == sizeY);
        CHECK(output->result.GetDataSizeZ() == sizeZ);
        CHECK(static_cast<int32_t>(output->result.GetPixelWorldSizeX(LengthUnit::Micrometer) * 1000) == static_cast<int32_t>(pixelWorldSizeX * 1000));
        CHECK(static_cast<int32_t>(output->result.GetPixelWorldSizeY(LengthUnit::Micrometer) * 1000) == static_cast<int32_t>(pixelWorldSizeY * 1000));
        CHECK(static_cast<int32_t>(output->result.GetPixelWorldSizeZ(LengthUnit::Micrometer) * 1000) == static_cast<int32_t>(pixelWorldSizeZ * 1000));
        CHECK(output->result.GetDataPath() == "");
    }
}