#include <catch2/catch.hpp>

#include "HTProcessorResult.h"

namespace HTProcessorResultTest {
    TEST_CASE("HTProcessorResult") {
        SECTION("HTProcessorResult()") {
            HTProcessorResult htProcessorResult;
            CHECK(&htProcessorResult != nullptr);
        }
        SECTION("HTProcessorResult(other)") {
            HTProcessorResult srcHTProcessorResult;
            srcHTProcessorResult.SetDataPath("test");

            HTProcessorResult destHTProcessorResult(srcHTProcessorResult);
            CHECK(destHTProcessorResult.GetDataPath() == "test");
        }
        SECTION("operator=()") {
            HTProcessorResult srcHTProcessorResult;
            srcHTProcessorResult.SetDataPath("test");

            HTProcessorResult destHTProcessorResult;
            destHTProcessorResult = srcHTProcessorResult;
            CHECK(destHTProcessorResult.GetDataPath() == "test");
        }
        SECTION("SetDataSize()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataSize(1, 2, 3);
            CHECK(&htProcessorResult != nullptr);
        }
        SECTION("GetDataSizeX()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataSize(1, 2, 3);
            CHECK(htProcessorResult.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataSize(1, 2, 3);
            CHECK(htProcessorResult.GetDataSizeY() == 2);
        }
        SECTION("GetDataSizeZ()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataSize(1, 2, 3);
            CHECK(htProcessorResult.GetDataSizeZ() == 3);
        }
        SECTION("SetPixelWorldSize()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(&htProcessorResult != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htProcessorResult.GetPixelWorldSizeX(LengthUnit::Meter) == 1);
        }
        SECTION("GetPixelWorldSizeY()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htProcessorResult.GetPixelWorldSizeY(LengthUnit::Meter) == 2);
        }
        SECTION("GetPixelWorldSizeZ()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htProcessorResult.GetPixelWorldSizeZ(LengthUnit::Meter) == 3);
        }
        SECTION("SetData()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetData({});
            CHECK(&htProcessorResult != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<float[]> data{ new float[1]() };

            HTProcessorResult htProcessorResult;
            htProcessorResult.SetData(data);
            CHECK(htProcessorResult.GetData() == data);
        }
        SECTION("SetDataPath()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataPath("test");
            CHECK(&htProcessorResult != nullptr);
        }
        SECTION("GetDataPath()") {
            HTProcessorResult htProcessorResult;
            htProcessorResult.SetDataPath("test");
            CHECK(htProcessorResult.GetDataPath() == "test");
        }
    }
}