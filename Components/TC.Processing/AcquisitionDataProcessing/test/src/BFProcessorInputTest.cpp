#include <catch2/catch.hpp>

#include "BFProcessorInput.h"

namespace BFProcessorInputTest {
    TEST_CASE("BFProcessorInput : unit test") {
        SECTION("BFProcessorInput()") {
            BFProcessorInput bfProcessorInput;
            CHECK(&bfProcessorInput != nullptr);
        }
        SECTION("BFProcessorInput(other)") {
            BFProcessorInput srcBFProcessorInput;
            srcBFProcessorInput.SetMagnificationOfSystem(1);

            BFProcessorInput destBFProcessorInput(srcBFProcessorInput);
            CHECK(destBFProcessorInput.GetMagnificationOfSystem() == 1);
        }
        SECTION("operator=()") {
            BFProcessorInput srcBFProcessorInput;
            srcBFProcessorInput.SetMagnificationOfSystem(1);

            BFProcessorInput destBFProcessorInput;
            destBFProcessorInput = srcBFProcessorInput;
            CHECK(destBFProcessorInput.GetMagnificationOfSystem() == 1);
        }
        SECTION("SetSampleDataFolderPath()") {
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetSampleDataFolderPath("test");
            CHECK(&bfProcessorInput != nullptr);
        }
        SECTION("GetSampleDataFolderPath()") {
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetSampleDataFolderPath("test");
            CHECK(bfProcessorInput.GetSampleDataFolderPath() == "test");
        }
        SECTION("SetPixelSizeOfImagingSensor()"){
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(&bfProcessorInput != nullptr);
        }
        SECTION("GetPixelSizeOfImagingSensor()") {
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetPixelSizeOfImagingSensor(1, LengthUnit::Meter);
            CHECK(bfProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Meter) == 1);
        }
        SECTION("SetMagnificationOfSystem()"){
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetMagnificationOfSystem(1);
            CHECK(&bfProcessorInput != nullptr);
        }
        SECTION("GetMagnificationOfSystem()") {
            BFProcessorInput bfProcessorInput;
            bfProcessorInput.SetMagnificationOfSystem(1);
            CHECK(bfProcessorInput.GetMagnificationOfSystem() == 1);
        }
    }

    TEST_CASE("BFProcessorInput : practical test") {
        BFProcessorInput bfProcessorInput;
        bfProcessorInput.SetSampleDataFolderPath("test");
        bfProcessorInput.SetPixelSizeOfImagingSensor(1.f, LengthUnit::Micrometer);
        bfProcessorInput.SetMagnificationOfSystem(2.f);

        CHECK(bfProcessorInput.GetSampleDataFolderPath() == "test");
        CHECK(bfProcessorInput.GetPixelSizeOfImagingSensor(LengthUnit::Nanometer) == 1000.f);
        CHECK(bfProcessorInput.GetMagnificationOfSystem() == 2.f);
    }
}
