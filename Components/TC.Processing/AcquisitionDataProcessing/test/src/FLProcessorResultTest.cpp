#include <catch2/catch.hpp>

#include "FLProcessorResult.h"

namespace FLProcessorResultTest {
    TEST_CASE("FLProcessorResult") {
        SECTION("FLProcessorResult()") {
            FLProcessorResult flProcessorResult;
            CHECK(&flProcessorResult != nullptr);
        }
        SECTION("FLProcessorResult(other)") {
            FLProcessorResult srcFLProcessorResult;
            srcFLProcessorResult.SetDataPath("test");

            FLProcessorResult destFLProcessorResult(srcFLProcessorResult);
            CHECK(destFLProcessorResult.GetDataPath() == "test");
        }
        SECTION("operator=()") {
            FLProcessorResult srcFLProcessorResult;
            srcFLProcessorResult.SetDataPath("test");

            FLProcessorResult destFLProcessorResult;
            destFLProcessorResult = srcFLProcessorResult;
            CHECK(destFLProcessorResult.GetDataPath() == "test");
        }
        SECTION("SetDataSize()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataSize(1, 2, 3);
            CHECK(&flProcessorResult != nullptr);
        }
        SECTION("GetDataSizeX()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataSize(1, 2, 3);
            CHECK(flProcessorResult.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataSize(1, 2, 3);
            CHECK(flProcessorResult.GetDataSizeY() == 2);
        }
        SECTION("GetDataSizeZ()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataSize(1, 2, 3);
            CHECK(flProcessorResult.GetDataSizeZ() == 3);
        }
        SECTION("SetPixelWorldSize()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(&flProcessorResult != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flProcessorResult.GetPixelWorldSizeX(LengthUnit::Meter) == 1);
        }
        SECTION("GetPixelWorldSizeY()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flProcessorResult.GetPixelWorldSizeY(LengthUnit::Meter) == 2);
        }
        SECTION("GetPixelWorldSizeZ()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flProcessorResult.GetPixelWorldSizeZ(LengthUnit::Meter) == 3);
        }
        SECTION("SetData()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetData({});
            CHECK(&flProcessorResult != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<float[]> data{ new float[1]() };

            FLProcessorResult flProcessorResult;
            flProcessorResult.SetData(data);
            CHECK(flProcessorResult.GetData() == data);
        }
        SECTION("SetDataPath()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataPath("test");
            CHECK(&flProcessorResult != nullptr);
        }
        SECTION("GetDataPath()") {
            FLProcessorResult flProcessorResult;
            flProcessorResult.SetDataPath("test");
            CHECK(flProcessorResult.GetDataPath() == "test");
        }
    }
}