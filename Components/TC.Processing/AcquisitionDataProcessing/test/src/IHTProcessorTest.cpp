#include <catch2/catch.hpp>

#include "IHTProcessor.h"

namespace IHTProcessorTest {
    class HTProcessorForTest final : public IHTProcessor {
    public:
        HTProcessorForTest() = default;
        ~HTProcessorForTest() = default;

        auto SetHTProcessorInput(const HTProcessorInput& input) -> void override {
            this->setHTProcessorInputTriggered = true;
        }
        auto SetOutputPort(const IHTProcessorOutput::Pointer& outputPort) -> void override {
            this->setOutputPortTriggered = true;
        }
        auto Process() -> bool override {
            this->processTriggered = true;
            return true;
        }

        bool setHTProcessorInputTriggered{ false };
        bool setOutputPortTriggered{ false };
        bool processTriggered{ false };
    };

    TEST_CASE("IHTProcessorTest") {
        SECTION("SetHTProcessorInput()") {
            HTProcessorForTest htProcessor;
            CHECK(htProcessor.setHTProcessorInputTriggered == false);
            htProcessor.SetHTProcessorInput({});
            CHECK(htProcessor.setHTProcessorInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            HTProcessorForTest htProcessor;
            CHECK(htProcessor.setOutputPortTriggered == false);
            htProcessor.SetOutputPort({});
            CHECK(htProcessor.setOutputPortTriggered == true);
        }
        SECTION("Process()") {
            HTProcessorForTest htProcessor;
            CHECK(htProcessor.processTriggered == false);
            htProcessor.Process();
            CHECK(htProcessor.processTriggered == true);
        }
    }
}