#include <catch2/catch.hpp>

#include <fstream>

#include "HTProcessorMatlabApp.h"

namespace HTProcessorMatlabAppTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCHTProcessingMatlabTest";
    const QString htProcessingMatlabLibraryPath = testFolderPath + "/SharedLibraryMatlabLibrary.ctf";
    const QString answerDataFilePath = testFolderPath + "/CellDataCrop/tomogram.data";
    const QString psfFolderPath = "C:/Temp";

    class HTProcessorOutput final : public IHTProcessorOutput {
    public:
        HTProcessorOutput() = default;
        ~HTProcessorOutput() = default;
        auto SetHTProcessorResult(const HTProcessorResult& htProcessorResult) -> void override {
            this->htProcessorResult = htProcessorResult;
        }

        HTProcessorResult htProcessorResult;
    };

    auto ReadAnswerTomogram()->std::shared_ptr<float[]> {
        constexpr auto sizeX = 400;
        constexpr auto sizeY = 400;
        constexpr auto sizeZ = 60;

        constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

        std::shared_ptr<float[]> answerPSF{ new float[numberOfElements]() };

        constexpr auto byteCount = numberOfElements * 4;

        std::ifstream answerDataStream(answerDataFilePath.toStdString(), std::ios::binary);
        answerDataStream.read(reinterpret_cast<char*>(answerPSF.get()), byteCount);

        return answerPSF;
    }

    auto CompareArray(const float* result, const float* answer)->bool {
        constexpr auto sizeX = 400;
        constexpr auto sizeY = 400;
        constexpr auto sizeZ = 60;

        constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

        for (auto index = 0; index < numberOfElements; ++index) {
            const auto resultValue = static_cast<int32_t>(result[index] * 1000000);
            const auto answerValue = static_cast<int32_t>(answer[index] * 1000000);

            if (resultValue != answerValue) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("HTProcessorMatlabApp : unit test") {
        SECTION("HTProcessorMatlabApp()") {
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetHTProcessingModuleFilePath()") {
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetHTProcessingModuleFilePath("test");
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetPsfFolderPath()") {
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetPsfFolderPath("test");
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetProcessingAppPath()") {
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetProcessingAppPath("");
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetProcessingTempFolderPath()") {
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetProcessingTempFolderPath("");
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetHTProcessorInput()"){
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetHTProcessorInput({});
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("SetOutput()"){
            HTProcessorMatlabApp htProcessorHTProcessingMatlabApp;
            htProcessorHTProcessingMatlabApp.SetOutputPort({});
            CHECK(&htProcessorHTProcessingMatlabApp != nullptr);
        }
        SECTION("Process()") {
            constexpr auto numberOfZSlices = 60;
            
            const QString sampleDataFolderPath = testFolderPath + "/CellDataCrop/HeLa_04";
            const QString backgroundDataFolderPath = testFolderPath + "/CellDataCrop/HeLa_BG";
            constexpr float objectiveNA = 0.72f;
            constexpr float condenserNA = 0.72f;
            constexpr float pixelSizeOfImagingSensor = 4.5f;
            constexpr float magnificationOfSystem = 40;
            constexpr float zStepLength = 0.546f;
            constexpr float mediumRI = 1.337f;

            const QString processingTempFolderPath = "C:/Temp/scanningTest";
            const QString appPath = "D:/Work/ProjectBuild/HTX_Release/bin/Release/TCHTProcessingMatlabApp.exe";

            HTProcessorInput htProcessorInput;
            htProcessorInput.SetSampleDataFolderPath(sampleDataFolderPath);
            htProcessorInput.SetBackgroundDataFolderPath(backgroundDataFolderPath);
            htProcessorInput.SetObjectiveNA(objectiveNA);
            htProcessorInput.SetCondenserNA(condenserNA);
            htProcessorInput.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
            htProcessorInput.SetMagnificationOfSystem(magnificationOfSystem);
            htProcessorInput.SetZStepLength(zStepLength, LengthUnit::Micrometer);
            htProcessorInput.SetMediumRI(mediumRI);

            auto outputPort = new HTProcessorOutput;
            auto outputPortPointer = IHTProcessorOutput::Pointer{ outputPort };

            HTProcessorMatlabApp htProcessorMatlabApp;
            htProcessorMatlabApp.SetHTProcessingModuleFilePath(htProcessingMatlabLibraryPath);
            htProcessorMatlabApp.SetPsfFolderPath(psfFolderPath);
            htProcessorMatlabApp.SetHTProcessorInput(htProcessorInput);
            htProcessorMatlabApp.SetOutputPort(outputPortPointer);
            htProcessorMatlabApp.SetProcessingAppPath(appPath);
            htProcessorMatlabApp.SetProcessingTempFolderPath(processingTempFolderPath);
            

            CHECK(htProcessorMatlabApp.Process() == true);

            const auto& result = outputPort->htProcessorResult;

            CHECK(result.GetDataSizeX() == 400);
            CHECK(result.GetDataSizeY() == 400);
            CHECK(result.GetDataSizeZ() == 60);
            CHECK(static_cast<int32_t>(result.GetPixelWorldSizeX(LengthUnit::Micrometer) * 1000000) == 112500);
            CHECK(static_cast<int32_t>(result.GetPixelWorldSizeY(LengthUnit::Micrometer) * 1000000) == 112500);
            CHECK(static_cast<int32_t>(result.GetPixelWorldSizeZ(LengthUnit::Micrometer) * 1000000) == 730002);
            CHECK(CompareArray(result.GetData().get(), ReadAnswerTomogram().get()));
            CHECK(result.GetDataPath() == "");
        }
    }

    //auto ReadAnswerTomogramFull()->std::shared_ptr<float[]> {
    //    constexpr auto sizeX = 1464;
    //    constexpr auto sizeY = 1464;
    //    constexpr auto sizeZ = 60;

    //    constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

    //    std::shared_ptr<float[]> answerPSF{ new float[numberOfElements]() };

    //    constexpr auto byteCount = numberOfElements * 4;

    //    std::ifstream answerDataStream((testFolderPath + "/CellData/tomogramFull.data").toStdString(), std::ios::binary);
    //    answerDataStream.read(reinterpret_cast<char*>(answerPSF.get()), byteCount);

    //    return answerPSF;
    //}


    //auto CompareArrayFull(const float* result, const float* answer)->bool {
    //    constexpr auto sizeX = 1464;
    //    constexpr auto sizeY = 1464;
    //    constexpr auto sizeZ = 60;

    //    constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

    //    for (auto index = 0; index < numberOfElements; ++index) {
    //        const auto resultValue = static_cast<int32_t>(result[index] * 1000000);
    //        const auto answerValue = static_cast<int32_t>(answer[index] * 1000000);

    //        if (resultValue != answerValue) {
    //            return false;
    //        }
    //    }
    //    return true;
    //}

    //TEST_CASE("HTProcessorMatlabApp : practical test") {
    //    constexpr auto numberOfZSlices = 60;
    //    constexpr auto usePSFForMaskFlag = true;
    //    constexpr auto nonNegativeReference = -0.005f;

    //    const QString sampleDataFolderPath = testFolderPath + "/CellData/HeLa_04";
    //    const QString backgroundDataFolderPath = testFolderPath + "/CellData/HeLa_BG";
    //    constexpr float objectiveNA = 0.72f;
    //    constexpr float condenserNA = 0.72f;
    //    constexpr float pixelSizeOfImagingSensor = 4.5f;
    //    constexpr float magnificationOfSystem = 40;
    //    constexpr float zStepLength = 0.546f;
    //    constexpr float mediumRI = 1.337f;

    //    HTProcessorInput htProcessorInput;
    //    htProcessorInput.SetSampleDataFolderPath(sampleDataFolderPath);
    //    htProcessorInput.SetBackgroundDataFolderPath(backgroundDataFolderPath);
    //    htProcessorInput.SetObjectiveNA(objectiveNA);
    //    htProcessorInput.SetCondenserNA(condenserNA);
    //    htProcessorInput.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
    //    htProcessorInput.SetMagnificationOfSystem(magnificationOfSystem);
    //    htProcessorInput.SetZStepLength(zStepLength, LengthUnit::Micrometer);
    //    htProcessorInput.SetMediumRI(mediumRI);

    //    auto outputPort = new HTProcessorOutput;
    //    auto outputPortPointer = IHTProcessorOutput::Pointer{ outputPort };

    //    const QString processingTempFolderPath = "C:/Temp/scanningTest";
    //    const QString appPath = "D:/Work/ProjectBuild/HTX_Release/bin/Release/TCHTProcessingMatlabApp.exe";

    //    HTProcessorMatlabApp htProcessorMatlabApp;
    //    htProcessorMatlabApp.SetHTProcessingModuleFilePath(htProcessingMatlabLibraryPath);
    //    htProcessorMatlabApp.SetPsfThesMapFolderPath(psfThresMapFolderPath);
    //    htProcessorMatlabApp.SetNumberOfZSlices(numberOfZSlices);
    //    htProcessorMatlabApp.SetRegularizationOptions(usePSFForMaskFlag, nonNegativeReference);
    //    htProcessorMatlabApp.SetHTProcessorInput(htProcessorInput);
    //    htProcessorMatlabApp.SetOutputPort(outputPortPointer);
    //    htProcessorMatlabApp.SetProcessingAppPath(appPath);
    //    htProcessorMatlabApp.SetProcessingTempFolderPath(processingTempFolderPath);

    //    CHECK(htProcessorMatlabApp.Process() == true);

    //    const auto& result = outputPort->htProcessorResult;

    //    CHECK(result.GetDataSizeX() == 1464);
    //    CHECK(result.GetDataSizeY() == 1464);
    //    CHECK(result.GetDataSizeZ() == 60);
    //    CHECK(static_cast<int32_t>(result.GetPixelWorldSizeX(LengthUnit::Micrometer) * 1000000) == 112500);
    //    CHECK(static_cast<int32_t>(result.GetPixelWorldSizeY(LengthUnit::Micrometer) * 1000000) == 112500);
    //    CHECK(static_cast<int32_t>(result.GetPixelWorldSizeZ(LengthUnit::Micrometer) * 1000000) == 730002);
    //    CHECK(CompareArrayFull(result.GetData().get(), ReadAnswerTomogramFull().get()));
    //    CHECK(result.GetDataPath() == "");
    //}
}