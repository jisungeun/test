#pragma once

#include <memory>
#include <QString>

#include "TCAcquisitionDataProcessingExport.h"
#include "SIUnit.h"

class TCAcquisitionDataProcessing_API HTProcessorInput {
public:
    struct CropInfo {
        int32_t cropStartIndexX{};
        int32_t cropStartIndexY{};
        int32_t cropSizeX{};
        int32_t cropSizeY{};
    };

    HTProcessorInput();
    HTProcessorInput(const HTProcessorInput& other);
    ~HTProcessorInput();

    auto operator=(const HTProcessorInput& other)->HTProcessorInput&;

    auto SetSampleDataFolderPath(const QString& sampleDataFolderPath)->void;
    auto GetSampleDataFolderPath() const -> const QString&;

    auto SetBackgroundDataFolderPath(const QString& backgroundDataFolderPath)->void;
    auto GetBackgroundDataFolderPath() const ->const QString&;

    auto SetCropInfo(const CropInfo& sampleCropInfo, const CropInfo& backgroundCropInfo)->void;
    auto GetSampleCropInfo()->CropInfo;
    auto GetBackgroundCropInfo()->CropInfo;

    auto SetObjectiveNA(const float& objectiveNA)->void;
    auto GetObjectiveNA() const ->const float&;

    auto SetCondenserNA(const float& condenserNA)->void;
    auto GetCondenserNA() const ->const float&;

    auto SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor, const LengthUnit& lengthUnit)->void;
    auto GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit) const -> float;

    auto SetMagnificationOfSystem(const float& magnificationOfSystem)->void;
    auto GetMagnificationOfSystem() const -> const float&;

    auto SetZStepLength(const float& zStepLength, const LengthUnit& lengthUnit)->void;
    auto GetZStepLength(const LengthUnit& lengthUnit) const ->float;

    auto SetMediumRI(const float& mediumRI)->void;
    auto GetMediumRI() const ->const float&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};