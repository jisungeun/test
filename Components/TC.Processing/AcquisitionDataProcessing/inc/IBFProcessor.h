#pragma once

#include "TCAcquisitionDataProcessingExport.h"

#include "BFProcessorInput.h"
#include "IBFProcessorOutput.h"

class TCAcquisitionDataProcessing_API IBFProcessor {
public:
    virtual ~IBFProcessor() = default;

    virtual auto SetBFProcessorInput(const BFProcessorInput& input)->void = 0;
    virtual auto SetOutputPort(const IBFProcessorOutput::Pointer& outputPort)->void = 0;

    virtual auto Process()->bool = 0;
};
