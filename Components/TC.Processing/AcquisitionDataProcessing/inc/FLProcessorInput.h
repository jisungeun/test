#pragma once

#include <memory>
#include <QString>

#include "TCAcquisitionDataProcessingExport.h"

#include "SIUnit.h"

class TCAcquisitionDataProcessing_API FLProcessorInput {
public:
    FLProcessorInput();
    FLProcessorInput(const FLProcessorInput& other);
    ~FLProcessorInput();

    auto operator=(const FLProcessorInput& other)->FLProcessorInput&;

    auto SetSampleDataFolderPath(const QString& sampleDataFolderPath)->void;
    auto GetSampleDataFolderPath()const->const QString&;

    auto SetObjectiveNA(const float& objectiveNA)->void;
    auto GetObjectiveNA() const ->const float&;

    auto SetCondenserNA(const float& condenserNA)->void;
    auto GetCondenserNA() const ->const float&;

    auto SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor, const LengthUnit& lengthUnit)->void;
    auto GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit) const -> float;

    auto SetMagnificationOfSystem(const float& magnificationOfSystem)->void;
    auto GetMagnificationOfSystem() const -> const float&;

    auto SetZStepLength(const float& zStepLength, const LengthUnit& lengthUnit)->void;
    auto GetZStepLength(const LengthUnit& lengthUnit) const ->float;

    auto SetMediumRI(const float& mediumRI)->void;
    auto GetMediumRI() const ->const float&;

    auto SetWaveLength(const float& waveLength, const LengthUnit& lengthUnit)->void;
    auto GetWaveLength(const LengthUnit& lengthUnit)const->float;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};