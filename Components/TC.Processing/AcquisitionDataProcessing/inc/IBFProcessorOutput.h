#pragma once

#include <memory>

#include "TCAcquisitionDataProcessingExport.h"

#include "BFProcessorResult.h"

class TCAcquisitionDataProcessing_API IBFProcessorOutput {
public:
    typedef std::shared_ptr<IBFProcessorOutput> Pointer;
    virtual ~IBFProcessorOutput() = default;

    virtual auto SetBFProcessorResult(const BFProcessorResult& bfProcessorResult)->void = 0;
};