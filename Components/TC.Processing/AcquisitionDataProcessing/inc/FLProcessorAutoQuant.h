#pragma once

#include <memory>

#include "IFLProcessor.h"
#include "TCAcquisitionDataProcessingExport.h"

class TCAcquisitionDataProcessing_API FLProcessorAutoQuant final : public IFLProcessor{
public:
    FLProcessorAutoQuant();
    ~FLProcessorAutoQuant();

    auto SetFLProcessorInput(const FLProcessorInput& input) -> void override;
    auto SetOutputPort(const IFLProcessorOutput::Pointer& outputPort) -> void override;

    auto SetSampleDepth(const float& sampleDepth, const LengthUnit& unit)->void;
    auto SetProcessingTempFolderPath(const QString& processingTempFolderPath)->void;

    auto SetDllUnlockRegistryKeyLocation(const QString& keyLocation)->void;

    auto Process() -> bool override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};