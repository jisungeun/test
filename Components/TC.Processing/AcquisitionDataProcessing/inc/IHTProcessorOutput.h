#pragma once

#include <memory>

#include "TCAcquisitionDataProcessingExport.h"

#include "HTProcessorResult.h"

class TCAcquisitionDataProcessing_API IHTProcessorOutput {
public:
    typedef std::shared_ptr<IHTProcessorOutput> Pointer;
    virtual ~IHTProcessorOutput() = default;

    virtual auto SetHTProcessorResult(const HTProcessorResult& htProcessorResult)->void = 0;
};