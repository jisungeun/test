#pragma once

#include <memory>
#include <QString>

#include "TCAcquisitionDataProcessingExport.h"

#include "SIUnit.h"

class TCAcquisitionDataProcessing_API BFProcessorInput {
public:
    BFProcessorInput();
    BFProcessorInput(const BFProcessorInput& other);
    ~BFProcessorInput();

    auto operator=(const BFProcessorInput& other)->BFProcessorInput&;

    auto SetSampleDataFolderPath(const QString& sampleDataFolderPath)->void;
    auto GetSampleDataFolderPath()const->const QString&;

    auto SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor, const LengthUnit& lengthUnit)->void;
    auto GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit)const->float;

    auto SetMagnificationOfSystem(const float& magnificationOfSystem)->void;
    auto GetMagnificationOfSystem()const->const float&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};