#pragma once

#include <memory>

#include "TCAcquisitionDataProcessingExport.h"

#include "FLProcessorResult.h"

class TCAcquisitionDataProcessing_API IFLProcessorOutput {
public:
    typedef std::shared_ptr<IFLProcessorOutput> Pointer;
    virtual ~IFLProcessorOutput() = default;

    virtual auto SetFLProcessorResult(const FLProcessorResult& flProcessorResult)->void = 0;
};