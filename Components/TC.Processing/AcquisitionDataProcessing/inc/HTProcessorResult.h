#pragma once

#include <memory>
#include <QString>

#include "SIUnit.h"

#include "TCAcquisitionDataProcessingExport.h"

class TCAcquisitionDataProcessing_API HTProcessorResult {
public:
    HTProcessorResult();
    HTProcessorResult(const HTProcessorResult& other);
    ~HTProcessorResult();

    auto operator=(const HTProcessorResult& other)->HTProcessorResult&;

    auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto GetDataSizeX()const -> const int32_t&;
    auto GetDataSizeY()const -> const int32_t&;
    auto GetDataSizeZ()const -> const int32_t&;

    auto SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY, const float& pixelWorldSizeZ, 
        const LengthUnit& unit)->void;
    auto GetPixelWorldSizeX(const LengthUnit& unit)const -> float;
    auto GetPixelWorldSizeY(const LengthUnit& unit)const -> float;
    auto GetPixelWorldSizeZ(const LengthUnit& unit)const -> float;

    auto SetData(const std::shared_ptr<float[]>& data)->void;
    auto GetData()const->const std::shared_ptr<float[]>&;

    auto SetDataPath(const QString& dataPath)->void;
    auto GetDataPath()const -> const QString&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};