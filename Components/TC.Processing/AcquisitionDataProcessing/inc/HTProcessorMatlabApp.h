#pragma once

#include <memory>
#include <QString>

#include "IHTProcessor.h"

#include "TCAcquisitionDataProcessingExport.h"

class TCAcquisitionDataProcessing_API HTProcessorMatlabApp final : public IHTProcessor{
public:
    HTProcessorMatlabApp();
    ~HTProcessorMatlabApp();

    auto SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath)->void;
    auto SetPSFModuleFilePath(const QString& psfModuleFilePath)->void;
    auto SetPsfFolderPath(const QString& psfFolderPath)->void;
    auto SetProcessingAppPath(const QString& processingAppFilePath)->void;
    auto SetProcessingTempFolderPath(const QString& processingTempFolderPath)->void;
    auto SetHTProcessingProfileFilePath(const QString& htProcessingProfileFilePath)->void;
    auto SetPSFProfileFilePath(const QString& psfProfileFilePath)->void;

    auto SetHTProcessorInput(const HTProcessorInput& input) -> void override;
    auto SetOutputPort(const IHTProcessorOutput::Pointer& outputPort) -> void override;
    auto Process() -> bool override;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
