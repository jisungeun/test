#pragma once

#include "TCAcquisitionDataProcessingExport.h"

#include "HTProcessorInput.h"
#include "IHTProcessorOutput.h"

class TCAcquisitionDataProcessing_API IHTProcessor {
public:
    virtual ~IHTProcessor() = default;

    virtual auto SetHTProcessorInput(const HTProcessorInput& input)->void = 0;
    virtual auto SetOutputPort(const IHTProcessorOutput::Pointer& outputPort)->void = 0;

    virtual auto Process()->bool = 0;
};
