#pragma once

#include <memory>

#include "IBFProcessor.h"
#include "TCAcquisitionDataProcessingExport.h"

class TCAcquisitionDataProcessing_API BFProcessor final : public IBFProcessor {
public:
    BFProcessor();
    ~BFProcessor();

    auto SetBFProcessorInput(const BFProcessorInput& input) -> void override;
    auto SetOutputPort(const IBFProcessorOutput::Pointer& outputPort) -> void override;
    auto SetChannelCount(const int32_t& channelCount)->void;

    auto Process() -> bool override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
