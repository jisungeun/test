#pragma once

#include "TCAcquisitionDataProcessingExport.h"

#include "FLProcessorInput.h"
#include "IFLProcessorOutput.h"

class TCAcquisitionDataProcessing_API IFLProcessor {
public:
    virtual ~IFLProcessor() = default;

    virtual auto SetFLProcessorInput(const FLProcessorInput& input)->void = 0;
    virtual auto SetOutputPort(const IFLProcessorOutput::Pointer& outputPort)->void = 0;

    virtual auto Process()->bool = 0;
};
