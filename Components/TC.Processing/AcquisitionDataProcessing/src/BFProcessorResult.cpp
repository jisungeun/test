#include "BFProcessorResult.h"

class BFProcessorResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t channelCount{};

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };

    std::shared_ptr<uint8_t[]> data{};
};

BFProcessorResult::BFProcessorResult() : d(new Impl()) {
}

BFProcessorResult::BFProcessorResult(const BFProcessorResult& other) : d(new Impl(*other.d)) {
}

BFProcessorResult::~BFProcessorResult() = default;

auto BFProcessorResult::operator=(const BFProcessorResult & other) -> BFProcessorResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto BFProcessorResult::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
}

auto BFProcessorResult::SetChannelCount(const int32_t& channelCount) -> void {
    d->channelCount = channelCount;
}

auto BFProcessorResult::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto BFProcessorResult::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto BFProcessorResult::GetChannelCount() const -> const int32_t& {
    return d->channelCount;
}

auto BFProcessorResult::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeUnit = unit;
}

auto BFProcessorResult::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto BFProcessorResult::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}

auto BFProcessorResult::SetData(const std::shared_ptr<uint8_t[]>& data) -> void {
    d->data = data;
}

auto BFProcessorResult::GetData() const -> const std::shared_ptr<uint8_t[]>& {
    return d->data;
}
