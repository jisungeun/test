#include "HTProcessorResult.h"

class HTProcessorResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    float pixelWorldSizeZ{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };

    std::shared_ptr<float[]> data{};

    QString dataPath{};
};

HTProcessorResult::HTProcessorResult() : d(new Impl()) {
}

HTProcessorResult::HTProcessorResult(const HTProcessorResult& other) : d(new Impl(*other.d)) {
}

HTProcessorResult::~HTProcessorResult() = default;

auto HTProcessorResult::operator=(const HTProcessorResult& other) -> HTProcessorResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto HTProcessorResult::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
    d->dataSizeZ = sizeZ;
}

auto HTProcessorResult::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto HTProcessorResult::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto HTProcessorResult::GetDataSizeZ() const -> const int32_t& {
    return d->dataSizeZ;
}

auto HTProcessorResult::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const float& pixelWorldSizeZ, const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeZ = pixelWorldSizeZ;
    d->pixelWorldSizeUnit = unit;
}

auto HTProcessorResult::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto HTProcessorResult::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}

auto HTProcessorResult::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit));
}

auto HTProcessorResult::SetData(const std::shared_ptr<float[]>& data) -> void {
    d->data = data;
}

auto HTProcessorResult::GetData() const -> const std::shared_ptr<float[]>& {
    return d->data;
}

auto HTProcessorResult::SetDataPath(const QString& dataPath) -> void {
    d->dataPath = dataPath;
}

auto HTProcessorResult::GetDataPath() const -> const QString& {
    return d->dataPath;
}
