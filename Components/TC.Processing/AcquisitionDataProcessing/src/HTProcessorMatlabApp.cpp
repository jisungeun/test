#define LOGGER_TAG "[HTProcessorMatlabApp]"

#include "HTProcessorMatlabApp.h"

#include <chrono>

#include <QDir>
#include <QThread>

#include "InputWriter.h"
#include "OutputReader.h"
#include "ProcessHandler.h"
#include "TCLogger.h"

constexpr auto scanningPeriodInSec = 1;
constexpr auto retryThreshold = 5;

class HTProcessorMatlabApp::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString htProcessingModuleFilePath{};
    QString psfModuleFilePath{};
    QString psfFolderPath{};
    QString htProcessingProfileFilePath{};
    QString psfProfileFilePath{};

    HTProcessorInput input{};
    IHTProcessorOutput::Pointer outputPort{};

    QString processingAppFilePath{};
    QString processingTempFolderPath{};
};

HTProcessorMatlabApp::HTProcessorMatlabApp() : d(new Impl()) {
}

HTProcessorMatlabApp::~HTProcessorMatlabApp() = default;

auto HTProcessorMatlabApp::SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath) -> void {
    d->htProcessingModuleFilePath = htProcessingModuleFilePath;
}

auto HTProcessorMatlabApp::SetPSFModuleFilePath(const QString& psfModuleFilePath) -> void {
    d->psfModuleFilePath = psfModuleFilePath;
}

auto HTProcessorMatlabApp::SetPsfFolderPath(const QString& psfFolderPath) -> void {
    d->psfFolderPath = psfFolderPath;
}

auto HTProcessorMatlabApp::SetProcessingAppPath(const QString& processingAppFilePath) -> void {
    d->processingAppFilePath = processingAppFilePath;
}

auto HTProcessorMatlabApp::SetProcessingTempFolderPath(const QString& processingTempFolderPath) -> void {
    d->processingTempFolderPath = processingTempFolderPath;
}

auto HTProcessorMatlabApp::SetHTProcessingProfileFilePath(const QString& htProcessingProfileFilePath) -> void {
    d->htProcessingProfileFilePath = htProcessingProfileFilePath;
}

auto HTProcessorMatlabApp::SetPSFProfileFilePath(const QString& psfProfileFilePath) -> void {
    d->psfProfileFilePath = psfProfileFilePath;
}

auto HTProcessorMatlabApp::SetHTProcessorInput(const HTProcessorInput& input) -> void {
    d->input = input;
}

auto HTProcessorMatlabApp::SetOutputPort(const IHTProcessorOutput::Pointer& outputPort) -> void {
    d->outputPort = outputPort;
}

auto HTProcessorMatlabApp::Process() -> bool {
    Inputs inputs;

    inputs.sampleImageFolderPath = d->input.GetSampleDataFolderPath();
    inputs.backgroundImageFolderPath = d->input.GetBackgroundDataFolderPath();
    inputs.psfFolderPath = d->psfFolderPath;

    inputs.htProcessingProfileFilePath = d->htProcessingProfileFilePath;
    inputs.psfProfileFilePath = d->psfProfileFilePath;

    inputs.sampleDataCropStartIndexX = d->input.GetSampleCropInfo().cropStartIndexX;
    inputs.sampleDataCropStartIndexY = d->input.GetSampleCropInfo().cropStartIndexY;

    inputs.backgroundDataCropStartIndexX = d->input.GetBackgroundCropInfo().cropStartIndexX;
    inputs.backgroundDataCropStartIndexY = d->input.GetBackgroundCropInfo().cropStartIndexY;

    inputs.cropSizeX = d->input.GetSampleCropInfo().cropSizeX;
    inputs.cropSizeY = d->input.GetSampleCropInfo().cropSizeY;

    inputs.mediumRi = d->input.GetMediumRI();
    inputs.objectiveNA = d->input.GetObjectiveNA();
    inputs.condenserNA = d->input.GetCondenserNA();
    inputs.voxelSizeXY = d->input.GetPixelSizeOfImagingSensor(LengthUnit::Micrometer) / d->input.GetMagnificationOfSystem();
    inputs.voxelSizeZ = d->input.GetZStepLength(LengthUnit::Micrometer) * d->input.GetMediumRI();
    
    inputs.htProcessingModuleFilePath = d->htProcessingModuleFilePath;
    inputs.psfModuleFilePath = d->psfModuleFilePath;

    const QString inputFilePath = QString("%1/inputFile").arg(d->processingTempFolderPath);
    
    InputWriter inputWriter;
    inputWriter.SetWritingFilePath(inputFilePath);
    inputWriter.SetInputs(inputs);
    if (!inputWriter.Write()) {
        return false;
    }

    const QString outputFilePath = QString("%1/outputFile").arg(d->processingTempFolderPath);
    const QString outputDoneFilePath = QString("%1/outputFile_done").arg(d->processingTempFolderPath);
    const QString errorFilePath = QString("%1/error").arg(d->processingTempFolderPath);

    if (QFile::exists(outputFilePath)) { QFile::remove(outputFilePath); }
    if (QFile::exists(outputDoneFilePath)) { QFile::remove(outputDoneFilePath); }
    if (QFile::exists(errorFilePath)) { QFile::remove(errorFilePath); }
    
    auto processHandler = ProcessHandler::GetInstance();
    if (!ProcessHandler::GetInstance()->IsStarted()) {
        processHandler->SetAppPath(d->processingAppFilePath);
        processHandler->SetTempFolderPath(d->processingTempFolderPath);
        processHandler->Start();
    }

    // scanning output file
    Outputs output;

    auto retryCount = 0;
    while (true) {
        if (QFile::exists(errorFilePath)) {
            QFile::remove(errorFilePath);
            processHandler->ForceClose();
            return false;
        }

        if (QFile::exists(outputDoneFilePath)) {
            OutputReader outputReader;
            outputReader.SetOutputFilePath(outputFilePath);

            if (!outputReader.Read()) {
                return false;
            }

            QFile::remove(outputDoneFilePath);

            output = outputReader.GetOutput();
            break;
        }
        QThread::sleep(scanningPeriodInSec);

        if (!processHandler->IsRunning()) {
            retryCount++;
            processHandler->ForceClose();
            if (retryCount > retryThreshold) {
                processHandler->ForceClose();
                QLOG_ERROR() << "retry count is over 5";
                return false;
            }
            QLOG_INFO() << "retry (" << retryCount << ")";
            InputWriter inputWriter;
            inputWriter.SetWritingFilePath(inputFilePath);
            inputWriter.SetInputs(inputs);
            if (!inputWriter.Write()) {
                return false;
            }

            processHandler->SetTempFolderPath(d->processingTempFolderPath);
            processHandler->Start();
        }
    }
    
    //get results
    const auto sizeX = output.sizeX;
    const auto sizeY = output.sizeY;
    const auto sizeZ = output.sizeZ;

    const auto pixelWorldSizeX = output.resolutionX;
    const auto pixelWorldSizeY = output.resolutionY;
    const auto pixelWorldSizeZ = output.resolutionZ;

    const auto resultData = output.tomogram;

    HTProcessorResult htProcessorResult;
    htProcessorResult.SetDataSize(sizeX, sizeY, sizeZ);
    htProcessorResult.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    htProcessorResult.SetData(resultData);
    htProcessorResult.SetDataPath("");
    d->outputPort->SetHTProcessorResult(htProcessorResult);
    
    return true;
}