#include "HTProcessorInput.h"

class HTProcessorInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    QString sampleDataFolderPath{};
    QString backgroundDataFolderPath{};
    float objectiveNA{};
    float condenserNA{};
    float pixelSizeOfImagingSensor{};
    LengthUnit pixelSizeLengthUnit{LengthUnit::Meter};

    float magnificationOfSystem{};
    float zStepLength{};
    LengthUnit zStepLengthUnit{ LengthUnit::Meter };

    CropInfo sampleCropInfo{};
    CropInfo backgroundCropInfo{};

    float mediumRI{};
};

HTProcessorInput::HTProcessorInput() : d(new Impl()) {
}

HTProcessorInput::HTProcessorInput(const HTProcessorInput& other) : d(new Impl(*other.d)) {
}

HTProcessorInput::~HTProcessorInput() = default;

auto HTProcessorInput::operator=(const HTProcessorInput& other) -> HTProcessorInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto HTProcessorInput::SetSampleDataFolderPath(const QString& sampleDataFolderPath) -> void {
    d->sampleDataFolderPath = sampleDataFolderPath;
}

auto HTProcessorInput::GetSampleDataFolderPath() const -> const QString& {
    return d->sampleDataFolderPath;
}

auto HTProcessorInput::SetBackgroundDataFolderPath(const QString& backgroundDataFolderPath) -> void {
    d->backgroundDataFolderPath = backgroundDataFolderPath;
}

auto HTProcessorInput::GetBackgroundDataFolderPath() const -> const QString& {
    return d->backgroundDataFolderPath;
}

auto HTProcessorInput::SetCropInfo(const CropInfo& sampleCropInfo, const CropInfo& backgroundCropInfo) -> void {
    d->sampleCropInfo = sampleCropInfo;
    d->backgroundCropInfo = backgroundCropInfo;
}

auto HTProcessorInput::GetSampleCropInfo() -> CropInfo {
    return d->sampleCropInfo;
}

auto HTProcessorInput::GetBackgroundCropInfo() -> CropInfo {
    return d->backgroundCropInfo;
}

auto HTProcessorInput::SetObjectiveNA(const float& objectiveNA) -> void {
    d->objectiveNA = objectiveNA;
}

auto HTProcessorInput::GetObjectiveNA() const -> const float& {
    return d->objectiveNA;
}

auto HTProcessorInput::SetCondenserNA(const float& condenserNA) -> void {
    d->condenserNA = condenserNA;
}

auto HTProcessorInput::GetCondenserNA() const -> const float& {
    return d->condenserNA;
}

auto HTProcessorInput::SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor,
    const LengthUnit& lengthUnit) -> void {
    d->pixelSizeOfImagingSensor = pixelSizeOfImagingSensor;
    d->pixelSizeLengthUnit = lengthUnit;
}

auto HTProcessorInput::GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit) const -> float {
    return ConvertUnit(d->pixelSizeOfImagingSensor, d->pixelSizeLengthUnit, lengthUnit);
}

auto HTProcessorInput::SetZStepLength(const float& zStepLength, const LengthUnit& lengthUnit) -> void {
    d->zStepLength = zStepLength;
    d->zStepLengthUnit = lengthUnit;
}

auto HTProcessorInput::GetZStepLength(const LengthUnit& lengthUnit) const -> float {
    return ConvertUnit(d->zStepLength, d->zStepLengthUnit, lengthUnit);
}

auto HTProcessorInput::SetMagnificationOfSystem(const float& magnificationOfSystem) -> void {
    d->magnificationOfSystem = magnificationOfSystem;
}

auto HTProcessorInput::GetMagnificationOfSystem() const -> const float& {
    return d->magnificationOfSystem;
}

auto HTProcessorInput::SetMediumRI(const float& mediumRI) -> void {
    d->mediumRI = mediumRI;
}

auto HTProcessorInput::GetMediumRI() const -> const float& {
    return d->mediumRI;
}
