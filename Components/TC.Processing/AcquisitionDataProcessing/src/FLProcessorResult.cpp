#include "FLProcessorResult.h"

class FLProcessorResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    float pixelWorldSizeZ{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };

    std::shared_ptr<float[]> data{};

    QString dataPath{};
};

FLProcessorResult::FLProcessorResult() : d(new Impl()) {
}

FLProcessorResult::FLProcessorResult(const FLProcessorResult& other) : d(new Impl(*other.d)) {
}

FLProcessorResult::~FLProcessorResult() = default;

auto FLProcessorResult::operator=(const FLProcessorResult& other) -> FLProcessorResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLProcessorResult::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
    d->dataSizeZ = sizeZ;
}

auto FLProcessorResult::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto FLProcessorResult::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto FLProcessorResult::GetDataSizeZ() const -> const int32_t& {
    return d->dataSizeZ;
}

auto FLProcessorResult::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const float& pixelWorldSizeZ, const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeZ = pixelWorldSizeZ;
    d->pixelWorldSizeUnit = unit;
}

auto FLProcessorResult::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto FLProcessorResult::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}

auto FLProcessorResult::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit));
}

auto FLProcessorResult::SetData(const std::shared_ptr<float[]>& data) -> void {
    d->data = data;
}

auto FLProcessorResult::GetData() const -> const std::shared_ptr<float[]>& {
    return d->data;
}

auto FLProcessorResult::SetDataPath(const QString& dataPath) -> void {
    d->dataPath = dataPath;
}

auto FLProcessorResult::GetDataPath() const -> const QString& {
    return d->dataPath;
}
