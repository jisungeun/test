#include "BFProcessor.h"

#include "PNGReader3DArrayFireSquare.h"
#include "PNGReaderUtility.h"

class BFProcessor::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    BFProcessorInput input{};
    IBFProcessorOutput::Pointer outputPort{};
    int32_t channelCount{};
};

BFProcessor::BFProcessor() : d(new Impl()) {
}

BFProcessor::~BFProcessor() = default;

auto BFProcessor::SetBFProcessorInput(const BFProcessorInput& input) -> void {
    d->input = input;
}

auto BFProcessor::SetOutputPort(const IBFProcessorOutput::Pointer& outputPort) -> void {
    d->outputPort = outputPort;
}

auto BFProcessor::SetChannelCount(const int32_t& channelCount) -> void {
    d->channelCount = channelCount;
}

auto BFProcessor::Process() -> bool {
    const auto sampleDataFolderPath = d->input.GetSampleDataFolderPath();
    const auto pixelSizeOfImagingSensor = d->input.GetPixelSizeOfImagingSensor(LengthUnit::Micrometer);
    const auto magnificationOfSystem = d->input.GetMagnificationOfSystem();

    const auto pngFilePathList = GetPngFilePathList(sampleDataFolderPath);
    if (pngFilePathList.size() == d->channelCount) {
        return false;
    }

    PNGReader3DArrayFireSquare pngReader;
    pngReader.SetInputFilePathList(pngFilePathList);
    const auto readingResult = pngReader.Read();

    if (readingResult != true) {
        return false;
    }

    const auto pixelWorldSizeX = pixelSizeOfImagingSensor / magnificationOfSystem;
    const auto pixelWorldSizeY = pixelSizeOfImagingSensor / magnificationOfSystem;

    const auto sizeX = pngReader.GetSizeX();
    const auto sizeY = pngReader.GetSizeY();
    const auto channelCount = pngReader.GetSizeZ();

    const auto data = pngReader.GetData();

    BFProcessorResult result;
    result.SetDataSize(sizeX, sizeY);
    result.SetChannelCount(channelCount);
    result.SetData(data);
    result.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);

    d->outputPort->SetBFProcessorResult(result);

    return true;
}
