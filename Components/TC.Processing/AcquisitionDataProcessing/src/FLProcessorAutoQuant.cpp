#include "FLProcessorAutoQuant.h"

#include <QDir>
#include <QStringList>

#include "AutoQuantDeconvolution.h"
#include "AutoQuantDeconvolutionOutput.h"
#include "AutoQuantDeconvolutionActivator.h"
#include "HotPixelRemover.h"

#include "PNGReader3DArrayFireSquare.h"
#include "PNGReaderUtility.h"

struct ImageSize {
    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};
};

class FLProcessorAutoQuant::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    FLProcessorInput input{};
    IFLProcessorOutput::Pointer outputPort{};

    float sampleDepth{};
    LengthUnit sampleDepthUnit{ LengthUnit::Micrometer };
    QString processingTempFolderPath{};
    QString unlockRegistryKeyLocation{};

    auto GenerateAutoQuantInput()->AutoQuantDeconvolutionInput;
    auto GenerateAutoQuantOption()->AutoQuantDeconvolutionOptions;
};

auto FLProcessorAutoQuant::Impl::GenerateAutoQuantInput() -> AutoQuantDeconvolutionInput {
    const auto sampleDataFolderPath = this->input.GetSampleDataFolderPath();
    const auto pngFilePathList = GetPngFilePathList(sampleDataFolderPath);

    PNGReader3DArrayFireSquare pngReader;
    pngReader.SetInputFilePathList(pngFilePathList);
    pngReader.Read();

    const auto imageData = pngReader.GetData();
    const auto sizeX = pngReader.GetSizeX();
    const auto sizeY = pngReader.GetSizeY();
    const auto sizeZ = pngReader.GetSizeZ();

    constexpr auto windowSize = 3;
    constexpr auto thresholdValue = 1;
    constexpr auto samplingZStep = 5;

    HotPixelRemover hotPixelRemover;
    hotPixelRemover.SetImageStack(imageData, sizeX, sizeY, sizeZ);
    hotPixelRemover.SetWindowSize(windowSize);
    hotPixelRemover.SetThresholdValue(thresholdValue);
    hotPixelRemover.SetSamplingZStep(samplingZStep);
    hotPixelRemover.Remove();

    const auto hotPixelRemovedImageData = hotPixelRemover.GetHotPixelRemovedImageStack();

    FLRawData flRawData;
    flRawData.SetData(hotPixelRemovedImageData, FLMemoryOrder::YXZ);
    flRawData.SetSize(sizeX, sizeY, sizeZ);

    const auto pixelSizeOfImagingSensor = this->input.GetPixelSizeOfImagingSensor(LengthUnit::Micrometer);
    const auto magnificationOfSystem = this->input.GetMagnificationOfSystem();

    const auto zStepLength = this->input.GetZStepLength(LengthUnit::Micrometer);
    const auto mediumRI = this->input.GetMediumRI();

    const auto pixelWorldSizeX = pixelSizeOfImagingSensor / magnificationOfSystem;
    const auto pixelWorldSizeY = pixelSizeOfImagingSensor / magnificationOfSystem;
    const auto pixelWorldSizeZ = zStepLength * mediumRI;

    const auto numericalAperture = this->input.GetObjectiveNA();
    const auto emissionWavelength = this->input.GetWaveLength(LengthUnit::Micrometer);
    const auto sampleDepthLength = 
        static_cast<float>(ConvertUnit(this->sampleDepth, this->sampleDepthUnit, LengthUnit::Micrometer));

    AutoQuantDeconvolutionInput input;
    input.SetFLRawData(flRawData);
    input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    input.SetNumericalAperture(numericalAperture);
    input.SetMediumRI(mediumRI);
    input.SetEmissionWaveLength(emissionWavelength, LengthUnit::Micrometer);
    input.SetSampleDepth(sampleDepthLength, LengthUnit::Micrometer);
    input.SetTempFolderPath(this->processingTempFolderPath);

    return input;
}

auto FLProcessorAutoQuant::Impl::GenerateAutoQuantOption() -> AutoQuantDeconvolutionOptions {
    AutoQuantDeconvolutionOptions options;
    options.SetLogFilePath("");
    options.SetModality(Modality::WidefieldFluorescence);
    options.SetDeconvolutionMethod(DeconvolutionMethod::ExpectationMaximization);
    options.SetDarkCurrentMethod(DarkCurrentMethod::AutoCalculation);
    options.SetInitialImageGuessGenerationMethod(InitialImageGuessGenerationMethod::LinearFilteredOriginal);
    options.SetPSFGuessMethod(PSFGuessMethod::TheoreticalEsitimation);
    options.SetFrequencyBandlimitDeterminationMethod(FrequencyBandlimitDeterminationMethod::AutoSelection);
    options.SetSubVolumeCalculationMethod(SubVolumeCalculationMethod::Dynamic);
    options.SetTheoreticalPSFGenerationMethod(TheoreticalPSFGenerationMethod::GibsonLanni);
    options.SetDarkCurrentValue(0.f);
    options.SetNumberOfIteration(10);
    options.SetSaveInterval(10);
    options.SetBinningFactorToXYPlane(1);
    options.SetBinningFactorToZPlane(1);
    options.SetEnableGPUProcessing(true);
    options.SetPaddingSizeToXYBorder(28);
    options.SetPaddingSizeToZBorder(6);
    options.SetSubVolumeOverlapSize(28);
    options.SetEnableSubVolumeInXY(true);
    options.SetEnableSubVolumeInZ(false);
    options.SetEnablePSFConstraint(true);
    options.SetNoiseSmoothingFactor(2.0f);
    options.SetPSFStretchFactor(1.0f);
    options.SetPSFWaistRadius(1.0f);
    options.SetGoldGaussianInterval(3);
    options.SetGoldGaussianFWHM(1.0f);
    options.SetEnableClassicConfocal(false);

    return options;
}

FLProcessorAutoQuant::FLProcessorAutoQuant() : d(new Impl()) {
}

FLProcessorAutoQuant::~FLProcessorAutoQuant() = default;

auto FLProcessorAutoQuant::SetFLProcessorInput(const FLProcessorInput& input) -> void {
    d->input = input;
}

auto FLProcessorAutoQuant::SetOutputPort(const IFLProcessorOutput::Pointer& outputPort) -> void {
    d->outputPort = outputPort;
}

auto FLProcessorAutoQuant::SetSampleDepth(const float& sampleDepth, const LengthUnit& unit) -> void {
    d->sampleDepth = sampleDepth;
    d->sampleDepthUnit = unit;
}

auto FLProcessorAutoQuant::SetProcessingTempFolderPath(const QString& processingTempFolderPath) -> void {
    d->processingTempFolderPath = processingTempFolderPath;
}

auto FLProcessorAutoQuant::SetDllUnlockRegistryKeyLocation(const QString& keyLocation) -> void {
    d->unlockRegistryKeyLocation = keyLocation;
}

auto FLProcessorAutoQuant::Process() -> bool {
    AutoQuantDeconvolutionActivator::SetRegistryKeyLocation(d->unlockRegistryKeyLocation.toStdString());
    AutoQuantDeconvolutionActivator::Unlock();

    if (!AutoQuantDeconvolutionActivator::IsUnlocked()) {
        return false;
    }

    const auto autoQuantInput = d->GenerateAutoQuantInput();
    const auto autoQuantOption = d->GenerateAutoQuantOption();

    auto autoQuantOutput = new AutoQuantDeconvolutionOutput;
    IAutoQuantDeconvolutionOutput::Pointer autoQuantOutputPort{ autoQuantOutput };

    AutoQuantDeconvolution deconvolution;
    deconvolution.SetInput(autoQuantInput);
    deconvolution.SetOption(autoQuantOption);
    deconvolution.SetOutputPort(autoQuantOutputPort);

    const auto deconvolutionResult = deconvolution.Deconvolute();
    if (deconvolutionResult != true) {
        return false;
    }

    const auto resultFLData = autoQuantOutput->GetDataMemory(FLMemoryOrder::YXZ);

    const auto sizeX = autoQuantOutput->GetSizeX();
    const auto sizeY = autoQuantOutput->GetSizeY();
    const auto sizeZ = autoQuantOutput->GetSizeZ();

    const auto pixelSizeX = autoQuantInput.GetPixelWorldSizeX(LengthUnit::Micrometer);
    const auto pixelSizeY = autoQuantInput.GetPixelWorldSizeY(LengthUnit::Micrometer);
    const auto pixelSizeZ = autoQuantInput.GetPixelWorldSizeZ(LengthUnit::Micrometer);

    FLProcessorResult processorResult;
    processorResult.SetData(resultFLData);
    processorResult.SetDataSize(sizeX, sizeY, sizeZ);
    processorResult.SetPixelWorldSize(pixelSizeX, pixelSizeY, pixelSizeZ, LengthUnit::Micrometer);

    d->outputPort->SetFLProcessorResult(processorResult);

    return true;
}
