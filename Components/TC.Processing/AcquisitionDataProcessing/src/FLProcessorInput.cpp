#include "FLProcessorInput.h"

class FLProcessorInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    QString sampleDataFolderPath{};
    float objectiveNA{};
    float condenserNA{};
    float pixelSizeOfImagingSensor{};
    LengthUnit pixelSizeLengthUnit{ LengthUnit::Meter };

    float magnificationOfSystem{};
    float zStepLength{};
    LengthUnit zStepLengthUnit{ LengthUnit::Meter };

    float mediumRI{};
    float waveLength{};
    LengthUnit waveLengthUnit{ LengthUnit::Meter };
};

FLProcessorInput::FLProcessorInput() : d(new Impl()) {
}

FLProcessorInput::FLProcessorInput(const FLProcessorInput& other) : d(new Impl(*other.d)) {
}

FLProcessorInput::~FLProcessorInput() = default;

auto FLProcessorInput::operator=(const FLProcessorInput& other) -> FLProcessorInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLProcessorInput::SetSampleDataFolderPath(const QString& sampleDataFolderPath) -> void {
    d->sampleDataFolderPath = sampleDataFolderPath;
}

auto FLProcessorInput::GetSampleDataFolderPath() const -> const QString& {
    return d->sampleDataFolderPath;
}

auto FLProcessorInput::SetObjectiveNA(const float& objectiveNA) -> void {
    d->objectiveNA = objectiveNA;
}

auto FLProcessorInput::GetObjectiveNA() const -> const float& {
    return d->objectiveNA;
}

auto FLProcessorInput::SetCondenserNA(const float& condenserNA) -> void {
    d->condenserNA = condenserNA;
}

auto FLProcessorInput::GetCondenserNA() const -> const float& {
    return d->condenserNA;
}

auto FLProcessorInput::SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor, const LengthUnit& lengthUnit)
    -> void {
    d->pixelSizeOfImagingSensor = pixelSizeOfImagingSensor;
    d->pixelSizeLengthUnit = lengthUnit;
}

auto FLProcessorInput::GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit) const -> float {
    return ConvertUnit(d->pixelSizeOfImagingSensor, d->pixelSizeLengthUnit, lengthUnit);
}

auto FLProcessorInput::SetZStepLength(const float& zStepLength, const LengthUnit& lengthUnit) -> void {
    d->zStepLength = zStepLength;
    d->zStepLengthUnit = lengthUnit;
}

auto FLProcessorInput::GetZStepLength(const LengthUnit& lengthUnit) const -> float {
    return ConvertUnit(d->zStepLength, d->zStepLengthUnit, lengthUnit);
}

auto FLProcessorInput::SetWaveLength(const float& waveLength, const LengthUnit& lengthUnit) -> void {
    d->waveLength = waveLength;
    d->waveLengthUnit = lengthUnit;
}

auto FLProcessorInput::GetWaveLength(const LengthUnit& lengthUnit) const -> float {
    return ConvertUnit(d->waveLength, d->waveLengthUnit, lengthUnit);
}

auto FLProcessorInput::SetMagnificationOfSystem(const float& magnificationOfSystem) -> void {
    d->magnificationOfSystem = magnificationOfSystem;
}

auto FLProcessorInput::GetMagnificationOfSystem() const -> const float& {
    return d->magnificationOfSystem;
}

auto FLProcessorInput::SetMediumRI(const float& mediumRI) -> void {
    d->mediumRI = mediumRI;
}

auto FLProcessorInput::GetMediumRI() const -> const float& {
    return d->mediumRI;
}
