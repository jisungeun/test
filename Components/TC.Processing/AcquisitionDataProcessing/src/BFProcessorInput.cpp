#include "BFProcessorInput.h"

class BFProcessorInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    QString sampleDataFolderPath{};
    float pixelSizeOfImagingSensor{};
    LengthUnit pixelSizeUnit{ LengthUnit::Meter };

    float magnificationOfSystem{};
};

BFProcessorInput::BFProcessorInput() : d(new Impl()) {
}

BFProcessorInput::BFProcessorInput(const BFProcessorInput& other) : d(new Impl(*other.d)) {
}

BFProcessorInput::~BFProcessorInput() = default;

auto BFProcessorInput::operator=(const BFProcessorInput & other) -> BFProcessorInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto BFProcessorInput::SetSampleDataFolderPath(const QString& sampleDataFolderPath) -> void {
    d->sampleDataFolderPath = sampleDataFolderPath;
}

auto BFProcessorInput::GetSampleDataFolderPath() const -> const QString& {
    return d->sampleDataFolderPath;
}

auto BFProcessorInput::SetPixelSizeOfImagingSensor(const float& pixelSizeOfImagingSensor, const LengthUnit& lengthUnit)
    -> void {
    d->pixelSizeOfImagingSensor = pixelSizeOfImagingSensor;
    d->pixelSizeUnit = lengthUnit;
}

auto BFProcessorInput::GetPixelSizeOfImagingSensor(const LengthUnit& lengthUnit) const ->float {
    return ConvertUnit(d->pixelSizeOfImagingSensor, d->pixelSizeUnit, lengthUnit);
}

auto BFProcessorInput::SetMagnificationOfSystem(const float& magnificationOfSystem) -> void {
    d->magnificationOfSystem = magnificationOfSystem;
}

auto BFProcessorInput::GetMagnificationOfSystem() const -> const float& {
    return d->magnificationOfSystem;
}
