#include <QFile>
#include <QSettings>
#include <catch2/catch.hpp>

#include "BeadWriter.h"
#include "BinaryFileIO.h"
#include "CompareArray.h"

using namespace TC::Processing::BeadEvaluation;

namespace BeadWriterTest {
    TEST_CASE("BeadWriter : unit test") {
        SECTION("BeadWriter()") {
            BeadWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetFilePath()") {
            BeadWriter writer;
            writer.SetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetData()") {
            BeadWriter writer;
            writer.SetData({});
            CHECK(&writer != nullptr);
        }
        SECTION("SetDataSize()") {
            BeadWriter writer;
            writer.SetDataSize(1,2,3);
            CHECK(&writer != nullptr);
        }
        SECTION("SetVoxelSize()") {
            BeadWriter writer;
            writer.SetVoxelSize(1, 2, 3);
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString beadFilePath = "beadFile";
            const auto beadRawFilePath = QString("%1.raw").arg(beadFilePath);

            if (QFile::exists(beadFilePath)) {
                QFile::remove(beadFilePath);
            }

            if (QFile::exists(beadRawFilePath)) {
                QFile::remove(beadRawFilePath);
            }

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 3;
            constexpr auto dataSizeZ = 4;

            constexpr double voxelSizeX = 1.1;
            constexpr double voxelSizeY = 2.2;
            constexpr double voxelSizeZ = 3.3;

            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            std::shared_ptr<float[]> beadData{ new float[numberOfElements]() };
            for (auto i = 0; i < numberOfElements; ++i) {
                beadData.get()[i] = i;
            }

            BeadWriter writer;
            writer.SetFilePath(beadFilePath);
            writer.SetData(beadData);
            writer.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            writer.SetVoxelSize(voxelSizeX, voxelSizeY, voxelSizeZ);

            CHECK(writer.Write());
            CHECK(QFile::exists(beadFilePath));
            CHECK(QFile::exists(QString("%1.raw").arg(beadFilePath)));

            QSettings resultBeadFile{ beadFilePath, QSettings::IniFormat };
            CHECK(resultBeadFile.value("rawFilePath").toString() == QString("%1.raw").arg(beadFilePath));

            CHECK(resultBeadFile.value("dataSizeX").toString() == "2");
            CHECK(resultBeadFile.value("dataSizeY").toString() == "3");
            CHECK(resultBeadFile.value("dataSizeZ").toString() == "4");

            CHECK(resultBeadFile.value("voxelSizeX").toString() == "1.1");
            CHECK(resultBeadFile.value("voxelSizeY").toString() == "2.2");
            CHECK(resultBeadFile.value("voxelSizeZ").toString() == "3.3");

            const auto resultBeadData = ReadFile_float(QString("%1.raw").arg(beadFilePath).toStdString(), numberOfElements);
            CHECK(CompareArray(beadData.get(), resultBeadData.get(), numberOfElements));
        }
    }
    TEST_CASE("BeadWriter : practical test") {
        //TODO Implement practical test
    }
}