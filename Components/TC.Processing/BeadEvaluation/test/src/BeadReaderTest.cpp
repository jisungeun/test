#include <QFile>
#include <catch2/catch.hpp>

#include "BeadReader.h"
#include "BeadWriter.h"
#include "CompareArray.h"

using namespace TC::Processing::BeadEvaluation;

namespace BeadReaderTest {
    auto WriteBeadUnitData(const QString& beadFilePath)->void {
        const auto beadRawFilePath = QString("%1.raw").arg(beadFilePath);

        if (QFile::exists(beadFilePath)) {
            QFile::remove(beadFilePath);
        }

        if (QFile::exists(beadRawFilePath)) {
            QFile::remove(beadRawFilePath);
        }

        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 3;
        constexpr auto dataSizeZ = 4;

        constexpr double voxelSizeX = 1.1;
        constexpr double voxelSizeY = 2.2;
        constexpr double voxelSizeZ = 3.3;

        constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

        std::shared_ptr<float[]> beadData{ new float[numberOfElements]() };
        for (auto i = 0; i < numberOfElements; ++i) {
            beadData.get()[i] = i;
        }

        BeadWriter writer;
        writer.SetFilePath(beadFilePath);
        writer.SetData(beadData);
        writer.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        writer.SetVoxelSize(voxelSizeX, voxelSizeY, voxelSizeZ);

        writer.Write();
    }

    TEST_CASE("BeadReader : unit test") {
        SECTION("BeadReader()") {
            BeadReader reader;
            CHECK(&reader != nullptr);
        }
        SECTION("SetBeadFilePath()") {
            BeadReader reader;
            reader.SetBeadFilePath("");
            CHECK(&reader != nullptr);
        }
        SECTION("Read()") {
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            CHECK(reader.Read() == true);
        }
        SECTION("GetBeadData()") {
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            const auto resultBeadData = reader.GetBeadData();

            std::shared_ptr<float[]> answerBeadData{ new float[24]() };
            for (auto i = 0; i < 24; ++i) {
                answerBeadData.get()[i] = i;
            }

            CHECK(CompareArray(resultBeadData.get(), answerBeadData.get(), 24));
        }
        SECTION("GetBeadDataSizeX()") {
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadDataSizeX() == 2);
        }
        SECTION("GetBeadDataSizeY()"){
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadDataSizeY() == 3);
        }
        SECTION("GetBeadDataSizeZ()") {
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadDataSizeZ() == 4);
        }
        SECTION("GetBeadVoxelSizeX()"){
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadVoxelSizeX() == 1.1);
        }
        SECTION("GetBeadVoxelSizeY()"){
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadVoxelSizeY() == 2.2);
        }
        SECTION("GetBeadVoxelSizeZ()"){
            const QString beadFilePath = "beadFile";
            WriteBeadUnitData(beadFilePath);

            BeadReader reader;
            reader.SetBeadFilePath(beadFilePath);
            reader.Read();

            CHECK(reader.GetBeadVoxelSizeZ() == 3.3);
        }
    }
    TEST_CASE("BeadReader : practical test") {
        //TODO Implement practical test
    }
}