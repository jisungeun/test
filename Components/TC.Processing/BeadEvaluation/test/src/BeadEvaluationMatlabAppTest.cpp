#include <iostream>
#include <QDir>
#include <catch2/catch.hpp>

#include <QFile>

#include "BeadEvaluationMatlabApp.h"

using namespace TC::Processing::BeadEvaluation;

namespace BeadEvaluationMatlabAppTest {
    TEST_CASE("BeadEvaluationMatlabApp : unit test") {
        
    }

    TEST_CASE("BeadEvaluationMatlabApp : practical test") {
        if (QDir().exists("D:/temp/beadEval/output")) {
            QDir("D:/temp/beadEval/output").removeRecursively();
        }
        QDir().mkpath("D:/temp/beadEval/output");

        BeadEvaluationInput input;
        input.SetRootFolderPath("E:/00_Data/20221020 BeadScoring/formatting");
        input.SetPSFFolderPath("C:/Users/Johnny/AppData/Local/Tomocube, Inc/TomoStudioX/PSF");
        input.SetPSFModuleFilePath("D:/Work/SourceTreeRepository/HTX/Components/TC.Processing.PSFMatlab/extern/PSFModule.ctf");
        input.SetBeadModuleFilePath("D:/Work/SourceTreeRepository/HTX2/Components/TC.Processing.BeadEvaluationMatlab/extern/BeadModule.ctf");
        input.SetBeadCenterPosition(109.35, 81);
        input.SetOutputFolderPath("D:/temp/beadEval/output");
        
        BeadEvaluationMatlabApp evaluation;
        evaluation.SetInput(input);
        evaluation.SetProcessingAppFilePath("D:/HTX_R_B/bin/Release/TCBeadEvaluationMatlabApp.exe");
        evaluation.SetProcessingTempFolderPath("D:/temp/beadEval");

        CHECK(evaluation.Evaluate());

        const auto result = evaluation.GetResult();
        
        CHECK(QFile::exists(result.GetReconBeadFilePath()));
        CHECK(QFile::exists(result.GetDeconBeadFilePath()));
        CHECK(QFile::exists(result.GetSimulatedBeadFilePath()));

        CHECK(QFile::exists(QString("%1.raw").arg(result.GetReconBeadFilePath())));
        CHECK(QFile::exists(QString("%1.raw").arg(result.GetDeconBeadFilePath())));
        CHECK(QFile::exists(QString("%1.raw").arg(result.GetSimulatedBeadFilePath())));
        
        //std::cout << "correlation : " << result.GetCorrelation() << std::endl;
        //std::cout << "volume : " << result.GetVolume() << std::endl;
        //std::cout << "dryMass : " << result.GetDryMass() << std::endl;
        //std::cout << "meanDeltaRI : " << result.GetMeanDeltaRI() << std::endl;
        //std::cout << "recon Bead File Path : " << result.GetReconBeadFilePath().toStdString() << std::endl;
        //std::cout << "decon Bead File Path : " << result.GetDeconBeadFilePath().toStdString() << std::endl;
        //std::cout << "simulated Bead File Path : " << result.GetSimulatedBeadFilePath().toStdString() << std::endl;
    }
}