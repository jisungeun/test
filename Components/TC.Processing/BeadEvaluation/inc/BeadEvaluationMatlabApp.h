#pragma once

#include <memory>
#include "TCBeadEvaluationExport.h"

#include "BeadEvaluationInput.h"
#include "BeadEvaluationResult.h"

namespace TC::Processing::BeadEvaluation {
    class TCBeadEvaluation_API BeadEvaluationMatlabApp {
    public:
        BeadEvaluationMatlabApp();
        ~BeadEvaluationMatlabApp();

        auto SetInput(const BeadEvaluationInput& input)->void;

        auto SetProcessingAppFilePath(const QString& processingAppFilePath)->void;
        auto SetProcessingTempFolderPath(const QString& processingTempFolderPath)->void;

        auto Evaluate()->bool;

        auto GetResult()const->BeadEvaluationResult;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
