#pragma once

#include <memory>
#include <QString>
#include "TCBeadEvaluationExport.h"

namespace TC::Processing::BeadEvaluation {
    class TCBeadEvaluation_API BeadReader {
    public:
        BeadReader();
        ~BeadReader();

        auto SetBeadFilePath(const QString& beadFilePath)->void;
        auto Read()->bool;

        auto GetBeadData()const->std::shared_ptr<float[]>;

        auto GetBeadDataSizeX()const->int32_t;
        auto GetBeadDataSizeY()const->int32_t;
        auto GetBeadDataSizeZ()const->int32_t;

        auto GetBeadVoxelSizeX()const->double;
        auto GetBeadVoxelSizeY()const->double;
        auto GetBeadVoxelSizeZ()const->double;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}