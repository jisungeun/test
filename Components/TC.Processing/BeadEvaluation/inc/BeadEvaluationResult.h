#pragma once

#include <memory>
#include <QString>

#include "TCBeadEvaluationExport.h"

namespace TC::Processing::BeadEvaluation {
    class TCBeadEvaluation_API BeadEvaluationResult {
    public:
        BeadEvaluationResult();
        BeadEvaluationResult(const BeadEvaluationResult& other);
        ~BeadEvaluationResult();

        auto operator=(const BeadEvaluationResult& other)->BeadEvaluationResult&;

        auto SetVolume(const double& volume)->void;
        auto GetVolume()const->const double&;

        auto SetDryMass(const double& dryMass)->void;
        auto GetDryMass()const->const double&;

        auto SetCorrelation(const double& correlation)->void;
        auto GetCorrelation()const->const double&;

        auto SetMeanDeltaRI(const double& meanDeltaRI)->void;
        auto GetMeanDeltaRI()const->const double&;

        auto SetReconBeadFilePath(const QString& reconBeadFilePath)->void;
        auto GetReconBeadFilePath()const->const QString&;

        auto SetDeconBeadFilePath(const QString& deconBeadFilePath)->void;
        auto GetDeconBeadFilePath()const->const QString&;

        auto SetSimulatedBeadFilePath(const QString& simulatedBeadFilePath)->void;
        auto GetSimulatedBeadFilePath()const->const QString&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}