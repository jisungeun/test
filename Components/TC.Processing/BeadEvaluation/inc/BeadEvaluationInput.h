#pragma once

#include <memory>
#include <QString>
#include "TCBeadEvaluationExport.h"

namespace TC::Processing::BeadEvaluation {
    class TCBeadEvaluation_API BeadEvaluationInput {
    public:
        BeadEvaluationInput();
        BeadEvaluationInput(const BeadEvaluationInput& other);
        ~BeadEvaluationInput();

        auto operator=(const BeadEvaluationInput& other)->BeadEvaluationInput&;

        auto SetRootFolderPath(const QString& rootFolderPath)->void;
        auto GetRootFolderPath()const->const QString&;

        auto SetPSFFolderPath(const QString& psfFolderPath)->void;
        auto GetPSFFolderPath()const->const QString&;

        auto SetPSFModuleFilePath(const QString& psfModuleFilePath)->void;
        auto GetPSFModuleFilePath()const->const QString&;

        auto SetBeadModuleFilePath(const QString& beadModuleFilePath)->void;
        auto GetBeadModuleFilePath()const->const QString&;

        auto SetBeadCenterPosition(const int32_t& positionXInPixel, const int32_t& positionYInPixel)->void;
        auto GetBeadCenterPositionXInPixel()const->const int32_t&;
        auto GetBeadCenterPositionYInPixel()const->const int32_t&;

        auto SetOutputFolderPath(const QString& outputFolderPath)->void;
        auto GetOutputFolderPath()const->const QString&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}