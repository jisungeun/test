#include "OutputReader.h"

#include <fstream>
#include <QFile>
#include <QSettings>

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class OutputReader::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString outputFilePath{};
        Outputs outputs;
    };

    OutputReader::OutputReader() : d(new Impl()) {
    }

    OutputReader::~OutputReader() = default;

    auto OutputReader::SetOutputFilePath(const QString& outputFilePath) -> void {
        d->outputFilePath = outputFilePath;
    }

    auto OutputReader::Read() -> bool {
        if (!QFile::exists(d->outputFilePath)) {
            return false;
        }

        QSettings outputFile(d->outputFilePath, QSettings::IniFormat);

        const auto volume = outputFile.value("volume").toDouble();
        const auto dryMass = outputFile.value("dryMass").toDouble();
        const auto correlation = outputFile.value("correlation").toDouble();
        const auto meanDeltaRI = outputFile.value("meanDeltaRI").toDouble();

        const auto reconBeadFilePath = outputFile.value("reconBeadFilePath").toString();
        const auto reconBeadSizeX = outputFile.value("reconBeadSizeX").toInt();
        const auto reconBeadSizeY = outputFile.value("reconBeadSizeY").toInt();
        const auto reconBeadSizeZ = outputFile.value("reconBeadSizeZ").toInt();
        const auto reconBeadVoxelSizeX = outputFile.value("reconBeadVoxelSizeX").toDouble();
        const auto reconBeadVoxelSizeY = outputFile.value("reconBeadVoxelSizeY").toDouble();
        const auto reconBeadVoxelSizeZ = outputFile.value("reconBeadVoxelSizeZ").toDouble();

        const auto deconBeadFilePath = outputFile.value("deconBeadFilePath").toString();
        const auto deconBeadSizeX = outputFile.value("deconBeadSizeX").toInt();
        const auto deconBeadSizeY = outputFile.value("deconBeadSizeY").toInt();
        const auto deconBeadSizeZ = outputFile.value("deconBeadSizeZ").toInt();
        const auto deconBeadVoxelSizeX = outputFile.value("deconBeadVoxelSizeX").toDouble();
        const auto deconBeadVoxelSizeY = outputFile.value("deconBeadVoxelSizeY").toDouble();
        const auto deconBeadVoxelSizeZ = outputFile.value("deconBeadVoxelSizeZ").toDouble();

        const auto simulatedBeadFilePath = outputFile.value("simulatedBeadFilePath").toString();
        const auto simulatedBeadSizeX = outputFile.value("simulatedBeadSizeX").toInt();
        const auto simulatedBeadSizeY = outputFile.value("simulatedBeadSizeY").toInt();
        const auto simulatedBeadSizeZ = outputFile.value("simulatedBeadSizeZ").toInt();
        const auto simulatedBeadVoxelSizeX = outputFile.value("simulatedBeadVoxelSizeX").toDouble();
        const auto simulatedBeadVoxelSizeY = outputFile.value("simulatedBeadVoxelSizeY").toDouble();
        const auto simulatedBeadVoxelSizeZ = outputFile.value("simulatedBeadVoxelSizeZ").toDouble();

        const std::shared_ptr<float[]> reconBeadData{ new float[reconBeadSizeX * reconBeadSizeY * reconBeadSizeZ] };
        const std::shared_ptr<float[]> deconBeadData{ new float[deconBeadSizeX * deconBeadSizeY * deconBeadSizeZ] };
        const std::shared_ptr<float[]> simulatedBeadData{ new float[simulatedBeadSizeX * simulatedBeadSizeY * simulatedBeadSizeZ] };

        {
            std::ifstream fileStream{ reconBeadFilePath.toStdString(), std::ios::binary };
            const auto byteCount = reconBeadSizeX * reconBeadSizeY * reconBeadSizeZ * static_cast<int32_t>(sizeof(float));
            fileStream.read(reinterpret_cast<char*>(reconBeadData.get()), byteCount);
            fileStream.close();
        }
        {
            std::ifstream fileStream{ deconBeadFilePath.toStdString(), std::ios::binary };
            const auto byteCount = deconBeadSizeX * deconBeadSizeY * deconBeadSizeZ * static_cast<int32_t>(sizeof(float));
            fileStream.read(reinterpret_cast<char*>(deconBeadData.get()), byteCount);
            fileStream.close();
        }
        {
            std::ifstream fileStream{ simulatedBeadFilePath.toStdString(), std::ios::binary };
            const auto byteCount = simulatedBeadSizeX * simulatedBeadSizeY * simulatedBeadSizeZ * static_cast<int32_t>(sizeof(float));
            fileStream.read(reinterpret_cast<char*>(simulatedBeadData.get()), byteCount);
            fileStream.close();
        }


        QFile::remove(d->outputFilePath);
        QFile::remove(reconBeadFilePath);
        QFile::remove(deconBeadFilePath);
        QFile::remove(simulatedBeadFilePath);

        Outputs outputs;
        outputs.volume = volume;
        outputs.dryMass = dryMass;
        outputs.correlation = correlation;
        outputs.meanDeltaRI = meanDeltaRI;
        outputs.reconBeadData = reconBeadData;
        outputs.reconBeadSizeX = reconBeadSizeX;
        outputs.reconBeadSizeY = reconBeadSizeY;
        outputs.reconBeadSizeZ = reconBeadSizeZ;
        outputs.reconBeadVoxelSizeX = reconBeadVoxelSizeX;
        outputs.reconBeadVoxelSizeY = reconBeadVoxelSizeY;
        outputs.reconBeadVoxelSizeZ = reconBeadVoxelSizeZ;

        outputs.deconBeadData = deconBeadData;
        outputs.deconBeadSizeX = deconBeadSizeX;
        outputs.deconBeadSizeY = deconBeadSizeY;
        outputs.deconBeadSizeZ = deconBeadSizeZ;
        outputs.deconBeadVoxelSizeX = deconBeadVoxelSizeX;
        outputs.deconBeadVoxelSizeY = deconBeadVoxelSizeY;
        outputs.deconBeadVoxelSizeZ = deconBeadVoxelSizeZ;

        outputs.simulatedBeadData = simulatedBeadData;
        outputs.simulatedBeadSizeX = simulatedBeadSizeX;
        outputs.simulatedBeadSizeY = simulatedBeadSizeY;
        outputs.simulatedBeadSizeZ = simulatedBeadSizeZ;
        outputs.simulatedBeadVoxelSizeX = simulatedBeadVoxelSizeX;
        outputs.simulatedBeadVoxelSizeY = simulatedBeadVoxelSizeY;
        outputs.simulatedBeadVoxelSizeZ = simulatedBeadVoxelSizeZ;

        d->outputs = outputs;

        return true;
    }

    auto OutputReader::GetOutput() const -> const Outputs& {
        return d->outputs;
    }
}