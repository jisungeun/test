#include "InputWriter.h"

#include <QSettings>

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class InputWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString writingFilePath{};
        Inputs inputs{};
    };

    InputWriter::InputWriter() : d(new Impl()) {
    }

    InputWriter::~InputWriter() = default;

    auto InputWriter::SetWritingFilePath(const QString& writingFilePath) -> void {
        d->writingFilePath = writingFilePath;
    }

    auto InputWriter::SetInputs(const Inputs& inputs) -> void {
        d->inputs = inputs;
    }

    auto InputWriter::Write() -> bool {
        {
            const auto beadModuleFilePath = d->inputs.beadModuleFilePath;
            const auto psfModuleFilePath = d->inputs.psfModuleFilePath;
            const auto sampleFolderPath = d->inputs.sampleFolderPath;
            const auto backgroundFolderPath = d->inputs.backgroundFolderPath;
            const auto psfFolderPath = d->inputs.psfFolderPath;
            const auto mediumRI = d->inputs.mediumRI;
            const auto naCond = d->inputs.naCond;
            const auto naObj = d->inputs.naObj;
            const auto magnification = d->inputs.magnification;
            const auto pixelSize = d->inputs.pixelSize;
            const auto zStepLength = d->inputs.zStepLength;
            const auto sampleCropOffsetX = d->inputs.sampleCropOffsetX;
            const auto sampleCropOffsetY = d->inputs.sampleCropOffsetY;
            const auto backgroundCropOffsetX = d->inputs.backgroundCropOffsetX;
            const auto backgroundCropOffsetY = d->inputs.backgroundCropOffsetY;
            const auto imageSizeX = d->inputs.imageSizeX;
            const auto imageSizeY = d->inputs.imageSizeY;
            const auto beadCenterPositionXInMicrometer = d->inputs.beadCenterPositionXInMicrometer;
            const auto beadCenterPositionYInMicrometer = d->inputs.beadCenterPositionYInMicrometer;
            const auto beadCropSizeXInMicrometer = d->inputs.beadCropSizeXInMicrometer;
            const auto beadCropSizeYInMicrometer = d->inputs.beadCropSizeYInMicrometer;

            QSettings inputFile(d->writingFilePath, QSettings::IniFormat);
            inputFile.setValue("beadModuleFilePath", beadModuleFilePath);
            inputFile.setValue("psfModuleFilePath", psfModuleFilePath);
            inputFile.setValue("sampleFolderPath", sampleFolderPath);
            inputFile.setValue("backgroundFolderPath", backgroundFolderPath);
            inputFile.setValue("psfFolderPath", psfFolderPath);
            inputFile.setValue("mediumRI", mediumRI);
            inputFile.setValue("naCond", naCond);
            inputFile.setValue("naObj", naObj);
            inputFile.setValue("magnification", magnification);
            inputFile.setValue("pixelSize", pixelSize);
            inputFile.setValue("zStepLength", zStepLength);
            inputFile.setValue("sampleCropOffsetX", sampleCropOffsetX);
            inputFile.setValue("sampleCropOffsetY", sampleCropOffsetY);
            inputFile.setValue("backgroundCropOffsetX", backgroundCropOffsetX);
            inputFile.setValue("backgroundCropOffsetY", backgroundCropOffsetY);
            inputFile.setValue("imageSizeX", imageSizeX);
            inputFile.setValue("imageSizeY", imageSizeY);
            inputFile.setValue("beadCenterPositionXInMicrometer", beadCenterPositionXInMicrometer);
            inputFile.setValue("beadCenterPositionYInMicrometer", beadCenterPositionYInMicrometer);
            inputFile.setValue("beadCropSizeXInMicrometer", beadCropSizeXInMicrometer);
            inputFile.setValue("beadCropSizeYInMicrometer", beadCropSizeYInMicrometer);
        }
        {
            QSettings inputWritingDoneFile(d->writingFilePath + "_done", QSettings::IniFormat);
            inputWritingDoneFile.setValue("flag", "done");
        }

        return true;
    }
}