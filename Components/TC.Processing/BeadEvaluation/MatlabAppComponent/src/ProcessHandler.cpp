#include "ProcessHandler.h"

#include <Windows.h>

#include <QFile>
#include <QProcess>
#include <fstream>
#include <QFileInfo>

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class ProcessHandler::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString appPath;
        QString tempFolderPath{};

        bool isStarted{ false };
        QProcess process;

        int64_t processID{};

        auto MakeFinishFile() noexcept -> void;
    };

    auto ProcessHandler::Impl::MakeFinishFile() noexcept -> void {
        const auto finishFilePath = this->tempFolderPath + "/finish";

        std::ofstream finishFile(finishFilePath.toStdString(), std::ios::binary);
        finishFile.write("finish", 6);
        finishFile.close();
    }

    ProcessHandler::~ProcessHandler() {
        d->MakeFinishFile();
    }

    auto ProcessHandler::GetInstance() -> std::shared_ptr<ProcessHandler> {
        static std::shared_ptr<ProcessHandler> instance{ new ProcessHandler };
        return instance;
    }

    auto ProcessHandler::IsStarted() -> bool {
        return d->isStarted;
    }

    auto ProcessHandler::SetAppPath(const QString& appPath) -> void {
        d->appPath = appPath;
    }

    auto ProcessHandler::SetTempFolderPath(const QString& tempFolderPath) -> void {
        d->tempFolderPath = tempFolderPath;
    }

    auto ProcessHandler::Start() -> void {
        if (d->isStarted) {
            return;
        }

        if (!QFile::exists(d->appPath)) {
            return;
        }

        if (QFile::exists(d->tempFolderPath + "/finish")) {
            QFile::remove(d->tempFolderPath + "/finish");
        }

        const QFileInfo fileInfo(d->appPath);
        const auto folderPath = fileInfo.absolutePath();

        qint64 processID{};

        d->process.setWorkingDirectory(folderPath);
        d->process.setProgram(d->appPath);
        d->process.setArguments({ d->tempFolderPath });
        d->process.startDetached(&processID);

        d->processID = processID;

        d->isStarted = true;
    }

    auto ProcessHandler::ForceClose() -> void {
        d->process.close();

        QProcess htProcessingApp;
        htProcessingApp.start("C:/Windows/System32/taskkill.exe", { "/f","/im","TCBeadEvaluationMatlabApp.exe" });
        htProcessingApp.waitForFinished();

        d->processID = 0;
        d->isStarted = false;
    }

    auto ProcessHandler::IsRunning() -> bool {
        bool processRunningFlag = false;

        if (d->processID == 0) {
            return processRunningFlag;
        }

        const HANDLE processHandle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, d->processID);
        if (processHandle) {
            DWORD exitCode = 0;
            if (GetExitCodeProcess(processHandle, &exitCode) && exitCode == STILL_ACTIVE) {
                processRunningFlag = true;
            }
            CloseHandle(processHandle);
        }

        return processRunningFlag;
    }

    ProcessHandler::ProcessHandler() : d(new Impl()) {
    }

}