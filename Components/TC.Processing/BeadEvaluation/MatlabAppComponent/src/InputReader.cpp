#include "InputReader.h"

#include <QFile>
#include <QSettings>

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class InputReader::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString inputFilePath{};
        Inputs readInput{};
    };

    InputReader::InputReader() : d(new Impl()) {
    }

    InputReader::~InputReader() = default;

    auto InputReader::SetInputFilePath(const QString& inputFilePath) -> void {
        d->inputFilePath = inputFilePath;
    }

    auto InputReader::Read() -> bool {
        QSettings inputFile(d->inputFilePath, QSettings::IniFormat);
        const auto beadModuleFilePath = inputFile.value("beadModuleFilePath").toString();
        const auto psfModuleFilePath = inputFile.value("psfModuleFilePath").toString();
        const auto sampleFolderPath = inputFile.value("sampleFolderPath").toString();
        const auto backgroundFolderPath = inputFile.value("backgroundFolderPath").toString();
        const auto psfFolderPath = inputFile.value("psfFolderPath").toString();
        const auto mediumRI = inputFile.value("mediumRI").toDouble();
        const auto naCond = inputFile.value("naCond").toDouble();
        const auto naObj = inputFile.value("naObj").toDouble();
        const auto magnification = inputFile.value("magnification").toDouble();
        const auto pixelSize = inputFile.value("pixelSize").toDouble();
        const auto zStepLength = inputFile.value("zStepLength").toDouble();
        const auto sampleCropOffsetX = inputFile.value("sampleCropOffsetX").toInt();
        const auto sampleCropOffsetY = inputFile.value("sampleCropOffsetY").toInt();
        const auto backgroundCropOffsetX = inputFile.value("backgroundCropOffsetX").toInt();
        const auto backgroundCropOffsetY = inputFile.value("backgroundCropOffsetY").toInt();
        const auto imageSizeX = inputFile.value("imageSizeX").toInt();
        const auto imageSizeY = inputFile.value("imageSizeY").toInt();
        const auto beadCenterPositionXInMicrometer = inputFile.value("beadCenterPositionXInMicrometer").toDouble();
        const auto beadCenterPositionYInMicrometer = inputFile.value("beadCenterPositionYInMicrometer").toDouble();
        const auto beadCropSizeXInMicrometer = inputFile.value("beadCropSizeXInMicrometer").toDouble();
        const auto beadCropSizeYInMicrometer = inputFile.value("beadCropSizeYInMicrometer").toDouble();

        QFile::remove(d->inputFilePath);

        Inputs inputs;
        inputs.beadModuleFilePath = beadModuleFilePath;
        inputs.psfModuleFilePath = psfModuleFilePath;
        inputs.sampleFolderPath = sampleFolderPath;
        inputs.backgroundFolderPath = backgroundFolderPath;
        inputs.psfFolderPath = psfFolderPath;
        inputs.mediumRI = mediumRI;
        inputs.naCond = naCond;
        inputs.naObj = naObj;
        inputs.magnification = magnification;
        inputs.pixelSize = pixelSize;
        inputs.zStepLength = zStepLength;
        inputs.sampleCropOffsetX = sampleCropOffsetX;
        inputs.sampleCropOffsetY = sampleCropOffsetY;
        inputs.backgroundCropOffsetX = backgroundCropOffsetX;
        inputs.backgroundCropOffsetY = backgroundCropOffsetY;
        inputs.imageSizeX = imageSizeX;
        inputs.imageSizeY = imageSizeY;
        inputs.beadCenterPositionXInMicrometer = beadCenterPositionXInMicrometer;
        inputs.beadCenterPositionYInMicrometer = beadCenterPositionYInMicrometer;
        inputs.beadCropSizeXInMicrometer = beadCropSizeXInMicrometer;
        inputs.beadCropSizeYInMicrometer = beadCropSizeYInMicrometer;

        d->readInput = inputs;

        return true;
    }

    auto InputReader::GetInput() const -> const Inputs& {
        return d->readInput;
    }
}