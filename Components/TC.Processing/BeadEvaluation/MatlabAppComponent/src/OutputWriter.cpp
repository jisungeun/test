#include "OutputWriter.h"

#include <fstream>
#include <QSettings>

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    static int32_t globalDataIndex = 0;

    class OutputWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString tempFolderPath{};
        QString outputFilePath{};
        Outputs outputs{};
    };

    OutputWriter::OutputWriter() : d(new Impl()) {
    }

    OutputWriter::~OutputWriter() = default;

    auto OutputWriter::SetTempFolderPath(const QString& tempFolderPath) -> void {
        d->tempFolderPath = tempFolderPath;
    }

    auto OutputWriter::SetOutputFilePath(const QString& outputFilePath) -> void {
        d->outputFilePath = outputFilePath;
    }

    auto OutputWriter::SetOutput(const Outputs& outputs) -> void {
        d->outputs = outputs;
    }

    auto OutputWriter::Write() -> bool {
        {
            const auto volume = d->outputs.volume;
            const auto dryMass = d->outputs.dryMass;
            const auto correlation = d->outputs.correlation;
            const auto meanDeltaRI = d->outputs.meanDeltaRI;

            const auto reconBeadData = d->outputs.reconBeadData;
            const auto reconBeadSizeX = d->outputs.reconBeadSizeX;
            const auto reconBeadSizeY = d->outputs.reconBeadSizeY;
            const auto reconBeadSizeZ = d->outputs.reconBeadSizeZ;
            const auto reconBeadVoxelSizeX = d->outputs.reconBeadVoxelSizeX;
            const auto reconBeadVoxelSizeY = d->outputs.reconBeadVoxelSizeY;
            const auto reconBeadVoxelSizeZ = d->outputs.reconBeadVoxelSizeZ;

            const auto deconBeadData = d->outputs.deconBeadData;
            const auto deconBeadSizeX = d->outputs.deconBeadSizeX;
            const auto deconBeadSizeY = d->outputs.deconBeadSizeY;
            const auto deconBeadSizeZ = d->outputs.deconBeadSizeZ;
            const auto deconBeadVoxelSizeX = d->outputs.deconBeadVoxelSizeX;
            const auto deconBeadVoxelSizeY = d->outputs.deconBeadVoxelSizeY;
            const auto deconBeadVoxelSizeZ = d->outputs.deconBeadVoxelSizeZ;

            const auto simulatedBeadData = d->outputs.simulatedBeadData;
            const auto simulatedBeadSizeX = d->outputs.simulatedBeadSizeX;
            const auto simulatedBeadSizeY = d->outputs.simulatedBeadSizeY;
            const auto simulatedBeadSizeZ = d->outputs.simulatedBeadSizeZ;
            const auto simulatedBeadVoxelSizeX = d->outputs.simulatedBeadVoxelSizeX;
            const auto simulatedBeadVoxelSizeY = d->outputs.simulatedBeadVoxelSizeY;
            const auto simulatedBeadVoxelSizeZ = d->outputs.simulatedBeadVoxelSizeZ;

            const auto reconBeadFilePath = QString("%1/R_%2").arg(d->tempFolderPath).arg(globalDataIndex);
            const auto deconBeadFilePath = QString("%1/D_%2").arg(d->tempFolderPath).arg(globalDataIndex);
            const auto simulatedBeadFilePath = QString("%1/S_%2").arg(d->tempFolderPath).arg(globalDataIndex);

            QSettings outputFile(d->outputFilePath, QSettings::IniFormat);
            outputFile.setValue("volume", volume);
            outputFile.setValue("dryMass", dryMass);
            outputFile.setValue("correlation", correlation);
            outputFile.setValue("meanDeltaRI", meanDeltaRI);

            outputFile.setValue("reconBeadFilePath", reconBeadFilePath);
            outputFile.setValue("reconBeadSizeX", reconBeadSizeX);
            outputFile.setValue("reconBeadSizeY", reconBeadSizeY);
            outputFile.setValue("reconBeadSizeZ", reconBeadSizeZ);
            outputFile.setValue("reconBeadVoxelSizeX", reconBeadVoxelSizeX);
            outputFile.setValue("reconBeadVoxelSizeY", reconBeadVoxelSizeY);
            outputFile.setValue("reconBeadVoxelSizeZ", reconBeadVoxelSizeZ);

            {
                std::ofstream fileStream{ reconBeadFilePath.toStdString(), std::ios::binary };
                const auto byteCount = reconBeadSizeX * reconBeadSizeY * reconBeadSizeZ * static_cast<int32_t>(sizeof(float));
                fileStream.write(reinterpret_cast<const char*>(reconBeadData.get()), byteCount);
                fileStream.close();
            }

            outputFile.setValue("deconBeadFilePath", deconBeadFilePath);
            outputFile.setValue("deconBeadSizeX", deconBeadSizeX);
            outputFile.setValue("deconBeadSizeY", deconBeadSizeY);
            outputFile.setValue("deconBeadSizeZ", deconBeadSizeZ);
            outputFile.setValue("deconBeadVoxelSizeX", deconBeadVoxelSizeX);
            outputFile.setValue("deconBeadVoxelSizeY", deconBeadVoxelSizeY);
            outputFile.setValue("deconBeadVoxelSizeZ", deconBeadVoxelSizeZ);

            {
                std::ofstream fileStream{ deconBeadFilePath.toStdString(), std::ios::binary };
                const auto byteCount = deconBeadSizeX * deconBeadSizeY * deconBeadSizeZ * static_cast<int32_t>(sizeof(float));
                fileStream.write(reinterpret_cast<const char*>(deconBeadData.get()), byteCount);
                fileStream.close();
            }

            outputFile.setValue("simulatedBeadFilePath", simulatedBeadFilePath);
            outputFile.setValue("simulatedBeadSizeX", simulatedBeadSizeX);
            outputFile.setValue("simulatedBeadSizeY", simulatedBeadSizeY);
            outputFile.setValue("simulatedBeadSizeZ", simulatedBeadSizeZ);
            outputFile.setValue("simulatedBeadVoxelSizeX", simulatedBeadVoxelSizeX);
            outputFile.setValue("simulatedBeadVoxelSizeY", simulatedBeadVoxelSizeY);
            outputFile.setValue("simulatedBeadVoxelSizeZ", simulatedBeadVoxelSizeZ);

            {
                std::ofstream fileStream{ simulatedBeadFilePath.toStdString(), std::ios::binary };
                const auto byteCount = simulatedBeadSizeX * simulatedBeadSizeY * simulatedBeadSizeZ * static_cast<int32_t>(sizeof(float));
                fileStream.write(reinterpret_cast<const char*>(simulatedBeadData.get()), byteCount);
                fileStream.close();
            }

        }
        {
            QSettings outputFile(d->outputFilePath + "_done", QSettings::IniFormat);
            outputFile.setValue("e", "e");
        }
        globalDataIndex++;
        return true;
    }

    auto OutputWriter::GetGlobalFileIndex() const -> const int32_t& {
        return globalDataIndex;
    }
}