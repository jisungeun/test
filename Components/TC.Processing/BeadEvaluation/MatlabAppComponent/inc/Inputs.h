#pragma once

#include <QString>

#include "TCBeadEvaluationMatlabAppComponentExport.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    struct TCBeadEvaluationMatlabAppComponent_API Inputs {
        QString beadModuleFilePath{};
        QString psfModuleFilePath{};
        QString sampleFolderPath{};
        QString backgroundFolderPath{};
        QString psfFolderPath{};
        double mediumRI{};
        double naCond{};
        double naObj{};
        double magnification{};
        double pixelSize{};
        double zStepLength{};
        int32_t sampleCropOffsetX{};
        int32_t sampleCropOffsetY{};
        int32_t backgroundCropOffsetX{};
        int32_t backgroundCropOffsetY{};
        int32_t imageSizeX{};
        int32_t imageSizeY{};
        double beadCenterPositionXInMicrometer{};
        double beadCenterPositionYInMicrometer{};
        double beadCropSizeXInMicrometer{};
        double beadCropSizeYInMicrometer{};
    };
}