#pragma once

#include <memory>
#include <QString>

#include "TCBeadEvaluationMatlabAppComponentExport.h"

#include "Inputs.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class TCBeadEvaluationMatlabAppComponent_API InputWriter {
    public:
        InputWriter();
        ~InputWriter();

        auto SetWritingFilePath(const QString& writingFilePath)->void;
        auto SetInputs(const Inputs& inputs)->void;
        auto Write()->bool;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}