#pragma once

#include <memory>
#include <QString>

#include "TCBeadEvaluationMatlabAppComponentExport.h"
#include "Outputs.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class TCBeadEvaluationMatlabAppComponent_API OutputWriter {
    public:
        OutputWriter();
        ~OutputWriter();

        auto SetTempFolderPath(const QString& tempFolderPath)->void;
        auto SetOutputFilePath(const QString& outputFilePath)->void;
        auto SetOutput(const Outputs& outputs)->void;

        auto Write()->bool;
        auto GetGlobalFileIndex()const->const int32_t&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}