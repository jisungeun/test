#pragma once

#include <QString>

#include "TCBeadEvaluationMatlabAppComponentExport.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    struct TCBeadEvaluationMatlabAppComponent_API Outputs {
        double volume{};
        double dryMass{};
        double correlation{};
        double meanDeltaRI{};

        std::shared_ptr<float[]> reconBeadData{};
        int32_t reconBeadSizeX{};
        int32_t reconBeadSizeY{};
        int32_t reconBeadSizeZ{};
        double reconBeadVoxelSizeX{};
        double reconBeadVoxelSizeY{};
        double reconBeadVoxelSizeZ{};

        std::shared_ptr<float[]> deconBeadData{};
        int32_t deconBeadSizeX{};
        int32_t deconBeadSizeY{};
        int32_t deconBeadSizeZ{};
        double deconBeadVoxelSizeX{};
        double deconBeadVoxelSizeY{};
        double deconBeadVoxelSizeZ{};

        std::shared_ptr<float[]> simulatedBeadData{};
        int32_t simulatedBeadSizeX{};
        int32_t simulatedBeadSizeY{};
        int32_t simulatedBeadSizeZ{};
        double simulatedBeadVoxelSizeX{};
        double simulatedBeadVoxelSizeY{};
        double simulatedBeadVoxelSizeZ{};
    };
}