#pragma once

#include <memory>
#include <QString>

#include "TCBeadEvaluationMatlabAppComponentExport.h"
#include "Outputs.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class TCBeadEvaluationMatlabAppComponent_API OutputReader {
    public:
        OutputReader();
        ~OutputReader();

        auto SetOutputFilePath(const QString& outputFilePath)->void;

        auto Read()->bool;

        auto GetOutput()const->const Outputs&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}