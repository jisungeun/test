#pragma once

#include <memory>

#include "TCBeadEvaluationMatlabAppComponentExport.h"

#include "Inputs.h"

namespace TC::Processing::BeadEvaluationMatlabAppComponent {
    class TCBeadEvaluationMatlabAppComponent_API InputReader {
    public:
        InputReader();
        ~InputReader();

        auto SetInputFilePath(const QString& inputFilePath)->void;
        auto Read()->bool;

        auto GetInput()const->const Inputs&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}