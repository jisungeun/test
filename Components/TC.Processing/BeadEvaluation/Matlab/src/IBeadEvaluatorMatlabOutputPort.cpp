#include "IBeadEvaluatorMatlabOutputPort.h"

namespace TC::Processing::BeadEvaluationMatlab {
    IBeadEvaluatorMatlabOutputPort::~IBeadEvaluatorMatlabOutputPort() = default;
}
