#include "BeadEvaluatorMatlabOutputPort.h"

#include <QMutexLocker>

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluatorMatlabOutputPort::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        Progress progress{};
        QMutex mutex;

        BeadEvaluatorMatlabOutput output{};
    };

    BeadEvaluatorMatlabOutputPort::BeadEvaluatorMatlabOutputPort() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluatorMatlabOutputPort::~BeadEvaluatorMatlabOutputPort() = default;

    auto BeadEvaluatorMatlabOutputPort::SetProgress(const float& percentage, const QString& description) -> void {
        QMutexLocker locker{ &d->mutex };
        d->progress.percentage = percentage;
        d->progress.description = description;
    }

    auto BeadEvaluatorMatlabOutputPort::SetOutput(const BeadEvaluatorMatlabOutput& output) -> void {
        d->output = output;
    }

    auto BeadEvaluatorMatlabOutputPort::GetProgress() const -> Progress {
        QMutexLocker locker{ &d->mutex };
        return d->progress;
    }

    auto BeadEvaluatorMatlabOutputPort::GetOutput() const -> BeadEvaluatorMatlabOutput {
        return d->output;
    }
}
