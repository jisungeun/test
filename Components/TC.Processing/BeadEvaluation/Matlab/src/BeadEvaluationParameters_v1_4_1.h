#pragma once

#include <memory>
#include <QString>

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluationParameters_v1_4_1 {
    public:
        BeadEvaluationParameters_v1_4_1();
        BeadEvaluationParameters_v1_4_1(const BeadEvaluationParameters_v1_4_1& other);
        ~BeadEvaluationParameters_v1_4_1();

        auto operator=(const BeadEvaluationParameters_v1_4_1& other)->BeadEvaluationParameters_v1_4_1&;

        auto SetSampleFolderPath(const QString& sampleFolderPath)->void;
        auto GetSampleFolderPath()const->const QString&;

        auto SetBackgroundFolderPath(const QString& backgroundFolderPath)->void;
        auto GetBackgroundFolderPath()const->const QString&;

        auto SetMediumRI(const double& mediumRI)->void;
        auto GetMediumRI()const->const double&;

        auto SetNACond(const double& naCond)->void;
        auto GetNACond()const->const double&;

        auto SetVoxelSize(const double& voxelSizeX, const double& voxelSizeY, const double& voxelSizeZ)->void;
        auto GetVoxelSizeX()const->const double&;
        auto GetVoxelSizeY()const->const double&;
        auto GetVoxelSizeZ()const->const double&;

        auto SetSampleCropOffset(const int32_t& offsetX, const int32_t& offsetY)->void;
        auto GetSampleCropOffsetX()const->const int32_t&;
        auto GetSampleCropOffsetY()const->const int32_t&;

        auto SetBackgroundCropOffset(const int32_t& offsetX, const int32_t& offsetY)->void;
        auto GetBackgroundCropOffsetX()const->const int32_t&;
        auto GetBackgroundCropOffsetY()const->const int32_t&;

        auto SetCropSize(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto GetCropSizeX()const->const int32_t&;
        auto GetCropSizeY()const->const int32_t&;

        auto SetPSF(const std::shared_ptr<float[]>& psf)->void;
        auto GetPSF()const->const std::shared_ptr<float[]>&;

        auto SetPSFSize(const int32_t& realImagCount, const int32_t& patternNumber, const int32_t& sizeX, 
            const int32_t& sizeY, const int32_t& sizeZ)->void;
        auto GetPSFRealImagCount()const->const int32_t&;
        auto GetPSFPatternNumber()const->const int32_t&;
        auto GetPSFSizeX()const->const int32_t&;
        auto GetPSFSizeY()const->const int32_t&;
        auto GetPSFSizeZ()const->const int32_t&;

        auto SetSupport(const std::shared_ptr<float[]>& support)->void;
        auto GetSupport()const->const std::shared_ptr<float[]>&;

        auto SetSupportSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
        auto GetSupportSizeX()const->const int32_t&;
        auto GetSupportSizeY()const->const int32_t&;
        auto GetSupportSizeZ()const->const int32_t&;

        auto SetKRes(const double& resX, const double& resY, const double& resZ)->void;
        auto GetKResX()const->const double&;
        auto GetKResY()const->const double&;
        auto GetKResZ()const->const double&;

        auto SetBeadCenterPositionInMicrometer(const double& x, const double& y)->void;
        auto GetBeadCenterPositionXInMicrometer()const->const double&;
        auto GetBeadCenterPositionYInMicrometer()const->const double&;

        auto SetBeadCropSizeInMicrometer(const double& x, const double& y)->void;
        auto GetBeadCropSizeXInMicrometer()const->const double&;
        auto GetBeadCropSizeYInMicrometer()const->const double&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}