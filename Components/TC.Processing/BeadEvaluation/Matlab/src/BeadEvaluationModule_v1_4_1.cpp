#include "BeadEvaluationModule_v1_4_1.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"
#include "TCLogger.h"

namespace TC::Processing::BeadEvaluationMatlab {
    const QString beadEvaluationFunctionName = "ReconstructForBeadScoring_SharedLibrary";

    class BeadEvaluationModule_v1_4_1::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString beadEvaluationModuleFilePath{};
        BeadEvaluationParameters_v1_4_1 parameters{};

        double volume{};
        double dryMass{};
        double correlation{};
        double meanDeltaRI{};

        std::shared_ptr<float[]> reconBeadTomogram{};
        TomogramSize reconBeadTomogramSize{};
        VoxelSize reconBeadTomogramVoxelSize{};

        std::shared_ptr<float[]> deconBeadTomogram{};
        TomogramSize deconBeadTomogramSize{};
        VoxelSize deconBeadTomogramVoxelSize{};

        std::shared_ptr<float[]> simulatedBeadTomogram{};
        TomogramSize simulatedBeadTomogramSize{};
        VoxelSize simulatedBeadTomogramVoxelSize{};
    };

    BeadEvaluationModule_v1_4_1::BeadEvaluationModule_v1_4_1() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluationModule_v1_4_1::~BeadEvaluationModule_v1_4_1() = default;

    auto BeadEvaluationModule_v1_4_1::SetBeadEvaluationModuleFilePath(const QString& beadEvaluationModuleFilePath) -> void {
        d->beadEvaluationModuleFilePath = beadEvaluationModuleFilePath;
    }

    auto BeadEvaluationModule_v1_4_1::SetBeadEvaluationModuleParameters(const BeadEvaluationParameters_v1_4_1& parameters) -> void {
        d->parameters = parameters;
    }

    auto BeadEvaluationModule_v1_4_1::Evaluate() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->beadEvaluationModuleFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto sampleFolderPathValue = d->parameters.GetSampleFolderPath();
            const auto backgroundFolderPathValue = d->parameters.GetBackgroundFolderPath();
            const auto mediumRIValue = d->parameters.GetMediumRI();
            const auto naCondValue = d->parameters.GetNACond();
            const auto voxelSizeXValue = d->parameters.GetVoxelSizeX();
            const auto voxelSizeYValue = d->parameters.GetVoxelSizeY();
            const auto voxelSizeZValue = d->parameters.GetVoxelSizeZ();
            const auto sampleCropOffsetXValue = static_cast<int64_t>(d->parameters.GetSampleCropOffsetX() + 1);
            const auto sampleCropOffsetYValue = static_cast<int64_t>(d->parameters.GetSampleCropOffsetY() + 1);
            const auto backgroundCropOffsetXValue = static_cast<int64_t>(d->parameters.GetBackgroundCropOffsetX() + 1);
            const auto backgroundCropOffsetYValue = static_cast<int64_t>(d->parameters.GetBackgroundCropOffsetY() + 1);
            const auto cropSizeXValue = static_cast<int64_t>(d->parameters.GetCropSizeX());
            const auto cropSizeYValue = static_cast<int64_t>(d->parameters.GetCropSizeY());
            const auto psfData = d->parameters.GetPSF();
            const auto supportData = d->parameters.GetSupport();
            const auto kResXValue = d->parameters.GetKResX();
            const auto kResYValue = d->parameters.GetKResY();
            const auto kResZValue = d->parameters.GetKResZ();
            const auto beadCenterPositionXInMicrometerValue = d->parameters.GetBeadCenterPositionXInMicrometer();
            const auto beadCenterPositionYInMicrometerValue = d->parameters.GetBeadCenterPositionYInMicrometer();
            const auto beadCropSizeXInMicrometerValue = d->parameters.GetBeadCropSizeXInMicrometer();
            const auto beadCropSizeYInMicrometerValue = d->parameters.GetBeadCropSizeYInMicrometer();

            auto psfDimension = matlab::data::ArrayDimensions{};
            psfDimension.push_back(d->parameters.GetPSFRealImagCount());
            psfDimension.push_back(d->parameters.GetPSFPatternNumber());
            psfDimension.push_back(d->parameters.GetPSFSizeY());
            psfDimension.push_back(d->parameters.GetPSFSizeX());
            psfDimension.push_back(d->parameters.GetPSFSizeZ());

            auto supportDimension = matlab::data::ArrayDimensions{};
            supportDimension.push_back(d->parameters.GetSupportSizeY());
            supportDimension.push_back(d->parameters.GetSupportSizeX());
            supportDimension.push_back(d->parameters.GetSupportSizeZ());

            matlab::data::ArrayFactory factory;
            auto psfDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(psfDimension));
            auto supportDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(supportDimension));

            std::copy_n(psfData.get(), matlab::data::getNumElements(psfDimension), psfDataBuffer.get());
            std::copy_n(supportData.get(), matlab::data::getNumElements(supportDimension), supportDataBuffer.get());

            const auto sampleFolderPath = MatlabSharedLibrary::ToMatlabInput(sampleFolderPathValue);
            const auto backgroundFolderPath = MatlabSharedLibrary::ToMatlabInput(backgroundFolderPathValue);
            const auto mediumRI = MatlabSharedLibrary::ToMatlabInput(mediumRIValue);
            const auto naCond = MatlabSharedLibrary::ToMatlabInput(naCondValue);
            const auto voxelSizeX = MatlabSharedLibrary::ToMatlabInput(voxelSizeXValue);
            const auto voxelSizeY = MatlabSharedLibrary::ToMatlabInput(voxelSizeYValue);
            const auto voxelSizeZ = MatlabSharedLibrary::ToMatlabInput(voxelSizeZValue);
            const auto sampleCropOffsetX = MatlabSharedLibrary::ToMatlabInput(sampleCropOffsetXValue);
            const auto sampleCropOffsetY = MatlabSharedLibrary::ToMatlabInput(sampleCropOffsetYValue);
            const auto backgroundCropOffsetX = MatlabSharedLibrary::ToMatlabInput(backgroundCropOffsetXValue);
            const auto backgroundCropOffsetY = MatlabSharedLibrary::ToMatlabInput(backgroundCropOffsetYValue);
            const auto cropSizeX = MatlabSharedLibrary::ToMatlabInput(cropSizeXValue);
            const auto cropSizeY = MatlabSharedLibrary::ToMatlabInput(cropSizeYValue);
            const auto psf =
                static_cast<matlab::data::Array>(factory.createArrayFromBuffer(psfDimension, std::move(psfDataBuffer)));
            const auto support =
                static_cast<matlab::data::Array>(factory.createArrayFromBuffer(supportDimension, std::move(supportDataBuffer)));
            const auto kResX = MatlabSharedLibrary::ToMatlabInput(kResXValue);
            const auto kResY = MatlabSharedLibrary::ToMatlabInput(kResYValue);
            const auto kResZ = MatlabSharedLibrary::ToMatlabInput(kResZValue);
            const auto beadCenterPositionXInMicrometer = MatlabSharedLibrary::ToMatlabInput(beadCenterPositionXInMicrometerValue);
            const auto beadCenterPositionYInMicrometer = MatlabSharedLibrary::ToMatlabInput(beadCenterPositionYInMicrometerValue);
            const auto beadCropSizeXInMicrometer = MatlabSharedLibrary::ToMatlabInput(beadCropSizeXInMicrometerValue);
            const auto beadCropSizeYInMicrometer = MatlabSharedLibrary::ToMatlabInput(beadCropSizeYInMicrometerValue);

            const std::vector inputs{ sampleFolderPath, backgroundFolderPath, mediumRI, naCond,
                voxelSizeX, voxelSizeY, voxelSizeZ, sampleCropOffsetX, sampleCropOffsetY, backgroundCropOffsetX,
                backgroundCropOffsetY, cropSizeX, cropSizeY, psf, support, kResX, kResY, kResZ,
                beadCenterPositionXInMicrometer, beadCenterPositionYInMicrometer,
                beadCropSizeXInMicrometer, beadCropSizeYInMicrometer };

            constexpr auto numberOfOutputs = 13;

            const auto beadEvaluationResultArray =
                matlabLibrary->feval(beadEvaluationFunctionName.toStdU16String(), numberOfOutputs, inputs);
            
            auto correlationResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[0]);
            auto dryMassResult = static_cast<matlab::data::TypedArray<float>>(beadEvaluationResultArray[1]);
            auto meanDeltaRIResult = static_cast<matlab::data::TypedArray<float>>(beadEvaluationResultArray[2]);
            auto volumeResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[3]);
            auto deconBeadTomogramResult = static_cast<matlab::data::TypedArray<float>>(beadEvaluationResultArray[4]);
            auto simulatedBeadTomogramResult = static_cast<matlab::data::TypedArray<float>>(beadEvaluationResultArray[5]);
            auto reconBeadTomogramResult = static_cast<matlab::data::TypedArray<float>>(beadEvaluationResultArray[6]);
            auto deconVoxelSizeXResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[7]);
            auto deconVoxelSizeYResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[8]);
            auto deconVoxelSizeZResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[9]);
            auto reconVoxelSizeXResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[10]);
            auto reconVoxelSizeYResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[11]);
            auto reconVoxelSizeZResult = static_cast<matlab::data::TypedArray<double>>(beadEvaluationResultArray[12]);

            d->correlation = correlationResult[0];
            d->dryMass = dryMassResult[0];
            d->meanDeltaRI = meanDeltaRIResult[0];
            d->volume = volumeResult[0];

            {
                const auto deconBeadDataElements = deconBeadTomogramResult.getNumberOfElements();
                const auto deconBeadUniquePtr = deconBeadTomogramResult.release();

                d->deconBeadTomogram = std::shared_ptr<float[]>{ new float[deconBeadDataElements]() };
                std::copy_n(deconBeadUniquePtr.get(), deconBeadDataElements, d->deconBeadTomogram.get());

                const auto deconBeadTomogramDimension = deconBeadTomogramResult.getDimensions();
                const auto deconBeadTomogramSizeX = static_cast<int32_t>(deconBeadTomogramDimension[1]);
                const auto deconBeadTomogramSizeY = static_cast<int32_t>(deconBeadTomogramDimension[0]);
                const auto deconBeadTomogramSizeZ = static_cast<int32_t>(deconBeadTomogramDimension[2]);
                d->deconBeadTomogramSize = TomogramSize{ deconBeadTomogramSizeX, deconBeadTomogramSizeY ,deconBeadTomogramSizeZ };
                d->deconBeadTomogramVoxelSize = VoxelSize{ deconVoxelSizeXResult[0], deconVoxelSizeYResult[0], deconVoxelSizeZResult[0] };
            }

            {
                const auto reconBeadDataElements = reconBeadTomogramResult.getNumberOfElements();
                const auto reconBeadUniquePtr = reconBeadTomogramResult.release();

                d->reconBeadTomogram = std::shared_ptr<float[]>{ new float[reconBeadDataElements]() };
                std::copy_n(reconBeadUniquePtr.get(), reconBeadDataElements, d->reconBeadTomogram.get());

                const auto reconBeadTomogramDimension = reconBeadTomogramResult.getDimensions();
                const auto reconBeadTomogramSizeX = static_cast<int32_t>(reconBeadTomogramDimension[1]);
                const auto reconBeadTomogramSizeY = static_cast<int32_t>(reconBeadTomogramDimension[0]);
                const auto reconBeadTomogramSizeZ = static_cast<int32_t>(reconBeadTomogramDimension[2]);
                d->reconBeadTomogramSize = TomogramSize{ reconBeadTomogramSizeX, reconBeadTomogramSizeY ,reconBeadTomogramSizeZ };
                d->reconBeadTomogramVoxelSize = VoxelSize{ reconVoxelSizeXResult[0], reconVoxelSizeYResult[0], reconVoxelSizeZResult[0] };
            }

            {
                const auto simulatedBeadDataElements = simulatedBeadTomogramResult.getNumberOfElements();
                const auto simulatedBeadUniquePtr = simulatedBeadTomogramResult.release();

                d->simulatedBeadTomogram = std::shared_ptr<float[]>{ new float[simulatedBeadDataElements]() };
                std::copy_n(simulatedBeadUniquePtr.get(), simulatedBeadDataElements, d->simulatedBeadTomogram.get());

                const auto simulatedBeadTomogramDimension = simulatedBeadTomogramResult.getDimensions();
                const auto simulatedBeadTomogramSizeX = static_cast<int32_t>(simulatedBeadTomogramDimension[1]);
                const auto simulatedBeadTomogramSizeY = static_cast<int32_t>(simulatedBeadTomogramDimension[0]);
                const auto simulatedBeadTomogramSizeZ = static_cast<int32_t>(simulatedBeadTomogramDimension[2]);
                d->simulatedBeadTomogramSize = TomogramSize{ simulatedBeadTomogramSizeX, simulatedBeadTomogramSizeY ,simulatedBeadTomogramSizeZ };
                d->simulatedBeadTomogramVoxelSize = VoxelSize{ deconVoxelSizeXResult[0], deconVoxelSizeYResult[0], deconVoxelSizeZResult[0] };
            }
        } catch (const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }


        return true;
    }

    auto BeadEvaluationModule_v1_4_1::GetVolume() const -> double {
        return d->volume;
    }

    auto BeadEvaluationModule_v1_4_1::GetDryMass() const -> double {
        return d->dryMass;
    }

    auto BeadEvaluationModule_v1_4_1::GetCorrelation() const -> double {
        return d->correlation;
    }

    auto BeadEvaluationModule_v1_4_1::GetMeanDeltaRI() const -> double {
        return d->meanDeltaRI;
    }

    auto BeadEvaluationModule_v1_4_1::GetReconBeadTomogram() const -> std::shared_ptr<float[]> {
        return d->reconBeadTomogram;
    }

    auto BeadEvaluationModule_v1_4_1::GetReconBeadTomogramSize() const -> TomogramSize {
        return d->reconBeadTomogramSize;
    }

    auto BeadEvaluationModule_v1_4_1::GetReconBeadTomogramVoxelSize() const -> VoxelSize {
        return d->reconBeadTomogramVoxelSize;
    }

    auto BeadEvaluationModule_v1_4_1::GetDeconBeadTomogram() const -> std::shared_ptr<float[]> {
        return d->deconBeadTomogram;
    }

    auto BeadEvaluationModule_v1_4_1::GetDeconBeadTomogramSize() const -> TomogramSize {
        return d->deconBeadTomogramSize;
    }

    auto BeadEvaluationModule_v1_4_1::GetDeconBeadTomogramVoxelSize() const -> VoxelSize {
        return d->deconBeadTomogramVoxelSize;
    }

    auto BeadEvaluationModule_v1_4_1::GetSimulatedBeadTomogram() const -> std::shared_ptr<float[]> {
        return d->simulatedBeadTomogram;
    }

    auto BeadEvaluationModule_v1_4_1::GetSimulatedBeadTomogramSize() const -> TomogramSize {
        return d->simulatedBeadTomogramSize;
    }

    auto BeadEvaluationModule_v1_4_1::GetSimulatedBeadTomogramVoxelSize() const -> VoxelSize {
        return d->simulatedBeadTomogramVoxelSize;
    }
}
