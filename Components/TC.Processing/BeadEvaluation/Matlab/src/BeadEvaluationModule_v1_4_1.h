#pragma once

#include <memory>
#include <QString>

#include "BeadEvaluationParameters_v1_4_1.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluationModule_v1_4_1 {
    public:
        struct VoxelSize {
            double x{};
            double y{};
            double z{};
        };

        struct TomogramSize {
            int32_t x{};
            int32_t y{};
            int32_t z{};
        };

        BeadEvaluationModule_v1_4_1();
        ~BeadEvaluationModule_v1_4_1();

        auto SetBeadEvaluationModuleFilePath(const QString& beadEvaluationModuleFilePath)->void;
        auto SetBeadEvaluationModuleParameters(const BeadEvaluationParameters_v1_4_1& parameters)->void;

        auto Evaluate()->bool;

        auto GetVolume()const->double;
        auto GetDryMass()const->double;
        auto GetCorrelation()const->double;
        auto GetMeanDeltaRI()const->double;

        auto GetReconBeadTomogram()const->std::shared_ptr<float[]>;
        auto GetReconBeadTomogramSize()const->TomogramSize;
        auto GetReconBeadTomogramVoxelSize()const->VoxelSize;

        auto GetDeconBeadTomogram()const->std::shared_ptr<float[]>;
        auto GetDeconBeadTomogramSize()const->TomogramSize;
        auto GetDeconBeadTomogramVoxelSize()const->VoxelSize;

        auto GetSimulatedBeadTomogram()const->std::shared_ptr<float[]>;
        auto GetSimulatedBeadTomogramSize()const->TomogramSize;
        auto GetSimulatedBeadTomogramVoxelSize()const->VoxelSize;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}