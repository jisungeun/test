#include "BeadEvaluatorMatlabOutput.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluatorMatlabOutput::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl& = default;

        double volume{};
        double dryMass{};
        double correlation{};
        double meanDeltaRI{};

        std::shared_ptr<float[]> reconBeadData{};
        DataSize reconBeadDataSize{};
        VoxelSize reconBeadVoxelSize{};

        std::shared_ptr<float[]> deconBeadData{};
        DataSize deconBeadDataSize{};
        VoxelSize deconBeadVoxelSize{};

        std::shared_ptr<float[]> simulatedBeadData{};
        DataSize simulatedBeadDataSize{};
        VoxelSize simulatedBeadVoxelSize{};
    };

    BeadEvaluatorMatlabOutput::BeadEvaluatorMatlabOutput() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluatorMatlabOutput::BeadEvaluatorMatlabOutput(const BeadEvaluatorMatlabOutput& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    BeadEvaluatorMatlabOutput::~BeadEvaluatorMatlabOutput() = default;

    auto BeadEvaluatorMatlabOutput::operator=(const BeadEvaluatorMatlabOutput& other) -> BeadEvaluatorMatlabOutput& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto BeadEvaluatorMatlabOutput::SetVolume(const double& volume) -> void {
        d->volume = volume;
    }

    auto BeadEvaluatorMatlabOutput::SetDryMass(const double& dryMass) -> void {
        d->dryMass = dryMass;
    }

    auto BeadEvaluatorMatlabOutput::SetCorrelation(const double& correlation) -> void {
        d->correlation = correlation;
    }

    auto BeadEvaluatorMatlabOutput::SetMeanDeltaRI(const double& meanDeltaRI) -> void {
        d->meanDeltaRI = meanDeltaRI;
    }

    auto BeadEvaluatorMatlabOutput::GetVolume() const -> const double& {
        return d->volume;
    }

    auto BeadEvaluatorMatlabOutput::GetDryMass() const -> const double& {
        return d->dryMass;
    }

    auto BeadEvaluatorMatlabOutput::GetCorrelation() const -> const double& {
        return d->correlation;
    }

    auto BeadEvaluatorMatlabOutput::GetMeanDeltaRI() const -> const double& {
        return d->meanDeltaRI;
    }

    auto BeadEvaluatorMatlabOutput::SetReconBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize,
        const VoxelSize& voxelSize) -> void {
        d->reconBeadData = data;
        d->reconBeadDataSize = dataSize;
        d->reconBeadVoxelSize = voxelSize;
    }

    auto BeadEvaluatorMatlabOutput::SetDeconBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize,
        const VoxelSize& voxelSize) -> void {
        d->deconBeadData = data;
        d->deconBeadDataSize = dataSize;
        d->deconBeadVoxelSize = voxelSize;
    }

    auto BeadEvaluatorMatlabOutput::SetSimulatedBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize,
        const VoxelSize& voxelSize) -> void {
        d->simulatedBeadData = data;
        d->simulatedBeadDataSize = dataSize;
        d->simulatedBeadVoxelSize = voxelSize;
    }

    auto BeadEvaluatorMatlabOutput::GetReconBeadData() const -> const std::shared_ptr<float[]>& {
        return d->reconBeadData;
    }

    auto BeadEvaluatorMatlabOutput::GetReconBeadDataSize() const -> const DataSize& {
        return d->reconBeadDataSize;
    }

    auto BeadEvaluatorMatlabOutput::GetReconBeadVoxelSize() const -> const VoxelSize& {
        return d->reconBeadVoxelSize;
    }

    auto BeadEvaluatorMatlabOutput::GetDeconBeadData() const -> const std::shared_ptr<float[]>& {
        return d->deconBeadData;
    }

    auto BeadEvaluatorMatlabOutput::GetDeconBeadDataSize() const -> const DataSize& {
        return d->deconBeadDataSize;
    }

    auto BeadEvaluatorMatlabOutput::GetDeconBeadVoxelSize() const -> const VoxelSize& {
        return d->deconBeadVoxelSize;
    }

    auto BeadEvaluatorMatlabOutput::GetSimulatedBeadData() const -> const std::shared_ptr<float[]>& {
        return d->simulatedBeadData;
    }

    auto BeadEvaluatorMatlabOutput::GetSimulatedBeadDataSize() const -> const DataSize& {
        return d->simulatedBeadDataSize;
    }

    auto BeadEvaluatorMatlabOutput::GetSimulatedBeadVoxelSize() const -> const VoxelSize& {
        return d->simulatedBeadVoxelSize;
    }
}
