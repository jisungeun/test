#include "BeadEvaluatorMatlabInput.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluatorMatlabInput::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        ModuleFilePathInfo moduleFilePathInfo{};
        PathInfo pathInfo{};
        DeviceInfo deviceInfo{};
        ImageInfo imageInfo{};
        BeadInfo beadInfo{};
    };

    BeadEvaluatorMatlabInput::BeadEvaluatorMatlabInput() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluatorMatlabInput::BeadEvaluatorMatlabInput(const BeadEvaluatorMatlabInput& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    BeadEvaluatorMatlabInput::~BeadEvaluatorMatlabInput() = default;

    auto BeadEvaluatorMatlabInput::operator=(const BeadEvaluatorMatlabInput& other) -> BeadEvaluatorMatlabInput& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto BeadEvaluatorMatlabInput::SetModuleFilePathInfo(const ModuleFilePathInfo& moduleFilePathInfo)->void {
        d->moduleFilePathInfo = moduleFilePathInfo;
    }

    auto BeadEvaluatorMatlabInput::GetModuleFilePathInfo() const -> const ModuleFilePathInfo& {
        return d->moduleFilePathInfo;
    }

    auto BeadEvaluatorMatlabInput::SetPathInfo(const PathInfo& pathInfo) -> void {
        d->pathInfo = pathInfo;
    }

    auto BeadEvaluatorMatlabInput::GetPathInfo() const -> const PathInfo& {
        return d->pathInfo;
    }

    auto BeadEvaluatorMatlabInput::SetDeviceInfo(const DeviceInfo& deviceInfo) -> void {
        d->deviceInfo = deviceInfo;
    }

    auto BeadEvaluatorMatlabInput::GetDeviceInfo() const -> const DeviceInfo& {
        return d->deviceInfo;
    }

    auto BeadEvaluatorMatlabInput::SetImageInfo(const ImageInfo& imageInfo) -> void {
        d->imageInfo = imageInfo;
    }

    auto BeadEvaluatorMatlabInput::GetImageInfo() const -> const ImageInfo& {
        return d->imageInfo;
    }

    auto BeadEvaluatorMatlabInput::SetBeadInfo(const BeadInfo& beadInfo) -> void {
        d->beadInfo = beadInfo;
    }

    auto BeadEvaluatorMatlabInput::GetBeadInfo() const -> const BeadInfo& {
        return d->beadInfo;
    }
}
