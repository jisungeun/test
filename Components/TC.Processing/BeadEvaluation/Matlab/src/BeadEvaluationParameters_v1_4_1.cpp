#include "BeadEvaluationParameters_v1_4_1.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class BeadEvaluationParameters_v1_4_1::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl& = default;
        QString sampleFolderPath{};
        QString backgroundFolderPath{};
        double mediumRI{};
        double naCond{};
        double voxelSizeX{};
        double voxelSizeY{};
        double voxelSizeZ{};
        int32_t sampleCropOffsetX{};
        int32_t sampleCropOffsetY{};
        int32_t backgroundCropOffsetX{};
        int32_t backgroundCropOffsetY{};
        int32_t cropSizeX{};
        int32_t cropSizeY{};

        std::shared_ptr<float[]> psf{};
        int32_t psfRealImagCount{};
        int32_t psfPatternNumber{};
        int32_t psfSizeX{};
        int32_t psfSizeY{};
        int32_t psfSizeZ{};

        std::shared_ptr<float[]>support{};
        int32_t supportSizeX{};
        int32_t supportSizeY{};
        int32_t supportSizeZ{};
        
        double kResX{};
        double kResY{};
        double kResZ{};
        double beadCenterPositionXInMicrometer{};
        double beadCenterPositionYInMicrometer{};
        double beadCropSizeXInMicrometer{};
        double beadCropSizeYInMicrometer{};
    };

    BeadEvaluationParameters_v1_4_1::BeadEvaluationParameters_v1_4_1() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluationParameters_v1_4_1::BeadEvaluationParameters_v1_4_1(const BeadEvaluationParameters_v1_4_1& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    BeadEvaluationParameters_v1_4_1::~BeadEvaluationParameters_v1_4_1() = default;

    auto BeadEvaluationParameters_v1_4_1::operator=(const BeadEvaluationParameters_v1_4_1& other) -> BeadEvaluationParameters_v1_4_1& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto BeadEvaluationParameters_v1_4_1::SetSampleFolderPath(const QString& sampleFolderPath) -> void {
        d->sampleFolderPath = sampleFolderPath;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSampleFolderPath() const -> const QString& {
        return d->sampleFolderPath;
    }

    auto BeadEvaluationParameters_v1_4_1::SetBackgroundFolderPath(const QString& backgroundFolderPath) -> void {
        d->backgroundFolderPath = backgroundFolderPath;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBackgroundFolderPath() const -> const QString& {
        return d->backgroundFolderPath;
    }

    auto BeadEvaluationParameters_v1_4_1::SetMediumRI(const double& mediumRI) -> void {
        d->mediumRI = mediumRI;
    }

    auto BeadEvaluationParameters_v1_4_1::GetMediumRI() const -> const double& {
        return d->mediumRI;
    }

    auto BeadEvaluationParameters_v1_4_1::SetNACond(const double& naCond) -> void {
        d->naCond = naCond;
    }

    auto BeadEvaluationParameters_v1_4_1::GetNACond() const -> const double& {
        return d->naCond;
    }

    auto BeadEvaluationParameters_v1_4_1::SetVoxelSize(const double& voxelSizeX, const double& voxelSizeY,
        const double& voxelSizeZ) -> void {
        d->voxelSizeX = voxelSizeX;
        d->voxelSizeY = voxelSizeY;
        d->voxelSizeZ = voxelSizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::GetVoxelSizeX() const -> const double& {
        return d->voxelSizeX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetVoxelSizeY() const -> const double& {
        return d->voxelSizeY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetVoxelSizeZ() const -> const double& {
        return d->voxelSizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::SetSampleCropOffset(const int32_t& offsetX, const int32_t& offsetY) -> void {
        d->sampleCropOffsetX = offsetX;
        d->sampleCropOffsetY = offsetY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSampleCropOffsetX() const -> const int32_t& {
        return d->sampleCropOffsetX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSampleCropOffsetY() const -> const int32_t& {
        return d->sampleCropOffsetY;
    }

    auto BeadEvaluationParameters_v1_4_1::SetBackgroundCropOffset(const int32_t& offsetX, const int32_t& offsetY) -> void {
        d->backgroundCropOffsetX = offsetX;
        d->backgroundCropOffsetY = offsetY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBackgroundCropOffsetX() const -> const int32_t& {
        return d->backgroundCropOffsetX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBackgroundCropOffsetY() const -> const int32_t& {
        return d->backgroundCropOffsetY;
    }

    auto BeadEvaluationParameters_v1_4_1::SetCropSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->cropSizeX = sizeX;
        d->cropSizeY = sizeY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetCropSizeX() const -> const int32_t& {
        return d->cropSizeX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetCropSizeY() const -> const int32_t& {
        return d->cropSizeY;
    }

    auto BeadEvaluationParameters_v1_4_1::SetPSF(const std::shared_ptr<float[]>& psf) -> void {
        d->psf = psf;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSF() const -> const std::shared_ptr<float[]>& {
        return d->psf;
    }

    auto BeadEvaluationParameters_v1_4_1::SetPSFSize(const int32_t& realImagCount, const int32_t& patternNumber,
        const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
        d->psfRealImagCount = realImagCount;
        d->psfPatternNumber = patternNumber;
        d->psfSizeX = sizeX;
        d->psfSizeY = sizeY;
        d->psfSizeZ = sizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSFRealImagCount() const -> const int32_t& {
        return d->psfRealImagCount;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSFPatternNumber() const -> const int32_t& {
        return d->psfPatternNumber;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSFSizeX() const -> const int32_t& {
        return d->psfSizeX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSFSizeY() const -> const int32_t& {
        return d->psfSizeY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetPSFSizeZ() const -> const int32_t& {
        return d->psfSizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::SetSupport(const std::shared_ptr<float[]>& support) -> void {
        d->support = support;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSupport() const -> const std::shared_ptr<float[]>& {
        return d->support;
    }

    auto BeadEvaluationParameters_v1_4_1::SetSupportSize(const int32_t& sizeX, const int32_t& sizeY,
        const int32_t& sizeZ) -> void {
        d->supportSizeX = sizeX;
        d->supportSizeY = sizeY;
        d->supportSizeZ = sizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSupportSizeX() const -> const int32_t& {
        return d->supportSizeX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSupportSizeY() const -> const int32_t& {
        return d->supportSizeY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetSupportSizeZ() const -> const int32_t& {
        return d->supportSizeZ;
    }

    auto BeadEvaluationParameters_v1_4_1::SetKRes(const double& resX, const double& resY, const double& resZ) -> void {
        d->kResX = resX;
        d->kResY = resY;
        d->kResZ = resZ;
    }

    auto BeadEvaluationParameters_v1_4_1::GetKResX() const -> const double& {
        return d->kResX;
    }

    auto BeadEvaluationParameters_v1_4_1::GetKResY() const -> const double& {
        return d->kResY;
    }

    auto BeadEvaluationParameters_v1_4_1::GetKResZ() const -> const double& {
        return d->kResZ;
    }

    auto BeadEvaluationParameters_v1_4_1::SetBeadCenterPositionInMicrometer(const double& x, const double& y) -> void {
        d->beadCenterPositionXInMicrometer = x;
        d->beadCenterPositionYInMicrometer = y;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBeadCenterPositionXInMicrometer() const -> const double& {
        return d->beadCenterPositionXInMicrometer;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBeadCenterPositionYInMicrometer() const -> const double& {
        return d->beadCenterPositionYInMicrometer;
    }

    auto BeadEvaluationParameters_v1_4_1::SetBeadCropSizeInMicrometer(const double& x, const double& y) -> void {
        d->beadCropSizeXInMicrometer = x;
        d->beadCropSizeYInMicrometer = y;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBeadCropSizeXInMicrometer() const -> const double& {
        return d->beadCropSizeXInMicrometer;
    }

    auto BeadEvaluationParameters_v1_4_1::GetBeadCropSizeYInMicrometer() const -> const double& {
        return d->beadCropSizeYInMicrometer;
    }
}
