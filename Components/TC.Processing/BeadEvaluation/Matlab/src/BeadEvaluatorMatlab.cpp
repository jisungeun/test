#define LOGGER_TAG "[BeadEvaluatorMatlab]"

#include "BeadEvaluatorMatlab.h"

#include "BeadEvaluationModule_v1_4_1.h"
#include "PSFHandler_v1_4_1_c.h"
#include "TCLogger.h"

namespace TC::Processing::BeadEvaluationMatlab {
    using PSFBuildingParameters = PSFMatlab::PSFBuildingParameters_v1_4_1_c;
    using PSFHandler = PSFMatlab::PSFHandler_v1_4_1_c;
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    class BeadEvaluatorMatlab::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        BeadEvaluatorMatlabInput input{};
        Profile psfProfile{};
        IBeadEvaluatorMatlabOutputPort::Pointer outputPort{};
    };

    BeadEvaluatorMatlab::BeadEvaluatorMatlab() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluatorMatlab::~BeadEvaluatorMatlab() = default;

    auto BeadEvaluatorMatlab::SetInput(const BeadEvaluatorMatlabInput& input) -> void {
        d->input = input;
    }

    auto BeadEvaluatorMatlab::SetOutputPort(const IBeadEvaluatorMatlabOutputPort::Pointer& outputPort) -> void {
        d->outputPort = outputPort;
    }

    auto BeadEvaluatorMatlab::SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile) -> void {
        d->psfProfile = psfProfile;
    }

    auto BeadEvaluatorMatlab::Evaluate() -> bool {
        d->outputPort->SetProgress(0, "started");

        const auto beadEvaluationModuleFilePath = d->input.GetModuleFilePathInfo().beadEvaluation;
        const auto psfModuleFilePath = d->input.GetModuleFilePathInfo().psf;

        const auto pixelSize = d->input.GetDeviceInfo().pixelSize;
        const auto magnification = d->input.GetDeviceInfo().magnification;
        const auto zStepLength = d->input.GetDeviceInfo().zStepLength;

        const auto sampleFolderPath = d->input.GetPathInfo().sampleFolderPath;
        const auto backgroundFolderPath = d->input.GetPathInfo().backgroundFolderPath;
        const auto psfFolderPath = d->input.GetPathInfo().psfFolderPath;

        const auto mediumRI = d->input.GetDeviceInfo().mediumRI;
        const auto naCond = d->input.GetDeviceInfo().naCond;
        const auto naObj = d->input.GetDeviceInfo().naObj;
        const auto voxelSizeX = pixelSize / magnification;
        const auto voxelSizeY = pixelSize / magnification;
        const auto voxelSizeZ = mediumRI * zStepLength;

        const auto sampleCropOffsetX = d->input.GetImageInfo().sampleCropOffsetX;
        const auto sampleCropOffsetY = d->input.GetImageInfo().sampleCropOffsetY;

        const auto backgroundCropOffsetX = d->input.GetImageInfo().backgroundCropOffsetX;
        const auto backgroundCropOffsetY = d->input.GetImageInfo().backgroundCropOffsetY;

        const auto cropSizeX = d->input.GetImageInfo().imageSizeX;
        const auto cropSizeY = d->input.GetImageInfo().imageSizeY;

        const auto beadCenterPositionX = d->input.GetBeadInfo().beadCenterPositionXInMicrometer;
        const auto beadCenterPositionY = d->input.GetBeadInfo().beadCenterPositionYInMicrometer;

        const auto beadCropSizeX = d->input.GetBeadInfo().beadCropSizeXInMicrometer;
        const auto beadCropSizeY = d->input.GetBeadInfo().beadCropSizeYInMicrometer;

        d->outputPort->SetProgress(10, "psf setting");

        PSFBuildingParameters psfBuildingParameters;
        psfBuildingParameters.SetMediumRI(mediumRI);
        psfBuildingParameters.SetCondenserNA(naCond);
        psfBuildingParameters.SetObjectiveNA(naObj);
        psfBuildingParameters.SetVoxelSizeXY(voxelSizeX);
        psfBuildingParameters.SetVoxelSizeZ(voxelSizeZ);

        auto psfHandler = PSFHandler::GetInstance();
        psfHandler->SetPSFModuleFilePath(psfModuleFilePath);
        psfHandler->SetPSFFolderPath(psfFolderPath);
        psfHandler->SetPSFBuildingParameters(psfBuildingParameters);
        psfHandler->SetPSFProfile(d->psfProfile);

        if (!psfHandler->Update()) {
            QLOG_ERROR() << "psfSupportHandler->Update() in Evaluate()";
            return false;
        }

        QLOG_INFO() << "Evaluating";

        const auto psfData = psfHandler->GetPSFData();
        const auto psfImagRealCount = psfHandler->GetPSFImagRealCount();
        const auto psfPatternCount = psfHandler->GetPSFPatternCount();

        const auto psfSizeX = d->psfProfile.psfSizeX;
        const auto psfSizeY = d->psfProfile.psfSizeY;
        const auto psfSizeZ = d->psfProfile.psfSizeZ;

        const auto supportData = psfHandler->GetSupportData();
        const auto supportSizeX = psfHandler->GetSupportSizeX();
        const auto supportSizeY = psfHandler->GetSupportSizeY();
        const auto supportSizeZ = psfHandler->GetSupportSizeZ();

        const auto kResX = psfHandler->GetKResX();
        const auto kResY = psfHandler->GetKResY();
        const auto kResZ = psfHandler->GetKResZ();

        
        BeadEvaluationParameters_v1_4_1 beadEvaluationParameters{};
        beadEvaluationParameters.SetSampleFolderPath(sampleFolderPath);
        beadEvaluationParameters.SetBackgroundFolderPath(backgroundFolderPath);
        beadEvaluationParameters.SetMediumRI(mediumRI);
        beadEvaluationParameters.SetNACond(naCond);
        beadEvaluationParameters.SetVoxelSize(voxelSizeX, voxelSizeY, voxelSizeZ);
        beadEvaluationParameters.SetSampleCropOffset(sampleCropOffsetX, sampleCropOffsetY);
        beadEvaluationParameters.SetBackgroundCropOffset(backgroundCropOffsetX, backgroundCropOffsetY);
        beadEvaluationParameters.SetCropSize(cropSizeX, cropSizeY);
        beadEvaluationParameters.SetPSF(psfData);
        beadEvaluationParameters.SetPSFSize(psfImagRealCount, psfPatternCount, psfSizeX, psfSizeY, psfSizeZ);
        beadEvaluationParameters.SetSupport(supportData);
        beadEvaluationParameters.SetSupportSize(supportSizeX, supportSizeY, supportSizeZ);
        beadEvaluationParameters.SetKRes(kResX, kResY, kResZ);
        beadEvaluationParameters.SetBeadCenterPositionInMicrometer(beadCenterPositionX, beadCenterPositionY);
        beadEvaluationParameters.SetBeadCropSizeInMicrometer(beadCropSizeX, beadCropSizeY);

        BeadEvaluationModule_v1_4_1 beadEvaluationModule;
        beadEvaluationModule.SetBeadEvaluationModuleFilePath(beadEvaluationModuleFilePath);
        beadEvaluationModule.SetBeadEvaluationModuleParameters(beadEvaluationParameters);

        d->outputPort->SetProgress(40, "Bead Evaluating");

        if (!beadEvaluationModule.Evaluate()) {
            QLOG_ERROR() << "beadEvaluationModule.Evaluate() in Evaluate()";
            return false;
        }

        d->outputPort->SetProgress(90, "Bead Result Setting");

        const auto volume = beadEvaluationModule.GetVolume();
        const auto dryMass = beadEvaluationModule.GetDryMass();
        const auto correlation = beadEvaluationModule.GetCorrelation();
        const auto meanDeltaRI = beadEvaluationModule.GetMeanDeltaRI();

        const auto reconBeadData = beadEvaluationModule.GetReconBeadTomogram();
        const auto reconBeadDataSize = beadEvaluationModule.GetReconBeadTomogramSize();
        const auto reconBeadVoxelSize = beadEvaluationModule.GetReconBeadTomogramVoxelSize();

        const auto deconBeadData = beadEvaluationModule.GetDeconBeadTomogram();
        const auto deconBeadDataSize = beadEvaluationModule.GetDeconBeadTomogramSize();
        const auto deconBeadVoxelSize = beadEvaluationModule.GetDeconBeadTomogramVoxelSize();

        const auto simulatedBeadData = beadEvaluationModule.GetSimulatedBeadTomogram();
        const auto simulatedBeadDataSize = beadEvaluationModule.GetSimulatedBeadTomogramSize();
        const auto simulatedBeadVoxelSize = beadEvaluationModule.GetSimulatedBeadTomogramVoxelSize();


        BeadEvaluatorMatlabOutput output;
        output.SetVolume(volume);
        output.SetDryMass(dryMass);
        output.SetCorrelation(correlation);
        output.SetMeanDeltaRI(meanDeltaRI);
        output.SetReconBead(reconBeadData, { reconBeadDataSize.x, reconBeadDataSize.y, reconBeadDataSize.z },
            { reconBeadVoxelSize.x, reconBeadVoxelSize.y, reconBeadVoxelSize.z });
        output.SetDeconBead(deconBeadData, { deconBeadDataSize.x, deconBeadDataSize.y, deconBeadDataSize.z },
            { deconBeadVoxelSize.x, deconBeadVoxelSize.y, deconBeadVoxelSize.z });
        output.SetSimulatedBead(simulatedBeadData, { simulatedBeadDataSize.x, simulatedBeadDataSize.y, simulatedBeadDataSize.z },
            { simulatedBeadVoxelSize.x, simulatedBeadVoxelSize.y, simulatedBeadVoxelSize.z });
        
        d->outputPort->SetOutput(output);

        d->outputPort->SetProgress(100, "Bead Evaluation Done");

        QLOG_INFO() << "Bead Evaluation Done";
        QLOG_INFO() << "volume (" << volume << ") dryMass(" << dryMass << ") correlation(" << correlation << ") meanDeltaRI(" << meanDeltaRI << ")";

        return true;
    }
}
