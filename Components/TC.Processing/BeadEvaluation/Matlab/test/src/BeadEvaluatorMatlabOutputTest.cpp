#include <catch2/catch.hpp>

#include "BeadEvaluatorMatlabOutput.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace BeadEvaluatorMatlabOutputTest {
    TEST_CASE("BeadEvaluatorMatlabOutput : unit test") {
        SECTION("BeadEvaluatorMatlabOutput()") {
            BeadEvaluatorMatlabOutput output;
            CHECK(&output != nullptr);
        }
        SECTION("BeadEvaluatorMatlabOutput(other)") {
            BeadEvaluatorMatlabOutput srcOutput;
            srcOutput.SetCorrelation(1);

            const BeadEvaluatorMatlabOutput destOutput(srcOutput);
            CHECK(destOutput.GetCorrelation() == 1);
        }
        SECTION("operator=()") {
            BeadEvaluatorMatlabOutput srcOutput;
            srcOutput.SetCorrelation(1);

            BeadEvaluatorMatlabOutput destOutput;
            destOutput = srcOutput;
            CHECK(destOutput.GetCorrelation() == 1);
        }
        SECTION("SetVolume()") {
            BeadEvaluatorMatlabOutput output;
            output.SetVolume(1);
            CHECK(&output != nullptr);
        }
        SECTION("SetDryMass()"){
            BeadEvaluatorMatlabOutput output;
            output.SetDryMass(1);
            CHECK(&output != nullptr);
        }
        SECTION("SetCorrelation()"){
            BeadEvaluatorMatlabOutput output;
            output.SetCorrelation(1);
            CHECK(&output != nullptr);
        }
        SECTION("SetMeanDeltaRI()"){
            BeadEvaluatorMatlabOutput output;
            output.SetMeanDeltaRI(1);
            CHECK(&output != nullptr);
        }
        SECTION("GetVolume()") {
            BeadEvaluatorMatlabOutput output;
            output.SetVolume(1);
            CHECK(output.GetVolume() == 1);
        }
        SECTION("GetDryMass()") {
            BeadEvaluatorMatlabOutput output;
            output.SetDryMass(1);
            CHECK(output.GetDryMass() == 1);
        }
        SECTION("GetCorrelation()") {
            BeadEvaluatorMatlabOutput output;
            output.SetCorrelation(1);
            CHECK(output.GetCorrelation() == 1);
        }
        SECTION("GetMeanDeltaRI()") {
            BeadEvaluatorMatlabOutput output;
            output.SetMeanDeltaRI(1);
            CHECK(output.GetMeanDeltaRI() == 1);
        }
        SECTION("SetReconBead()"){
            BeadEvaluatorMatlabOutput output;
            output.SetReconBead({}, {}, {});
            CHECK(&output != nullptr);
        }
        SECTION("SetDeconBead()"){
            BeadEvaluatorMatlabOutput output;
            output.SetDeconBead({}, {}, {});
            CHECK(&output != nullptr);
        }
        SECTION("SetSimulatedBead()"){
            BeadEvaluatorMatlabOutput output;
            output.SetSimulatedBead({}, {}, {});
            CHECK(&output != nullptr);
        }
        SECTION("GetReconBeadData()") {
            std::shared_ptr<float[]> reconBeadData{ new float[1] };
            reconBeadData.get()[0] = 1;

            BeadEvaluatorMatlabOutput output;
            output.SetReconBead(reconBeadData, {}, {});

            CHECK(output.GetReconBeadData().get()[0] == 1);
        }
        SECTION("GetReconBeadDataSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetReconBead({}, { 1,2,3 }, {});
            CHECK(output.GetReconBeadDataSize().x == 1);
            CHECK(output.GetReconBeadDataSize().y == 2);
            CHECK(output.GetReconBeadDataSize().z == 3);
        }
        SECTION("GetReconBeadVoxelSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetReconBead({}, {}, { 1,2,3 });
            CHECK(output.GetReconBeadVoxelSize().x == 1);
            CHECK(output.GetReconBeadVoxelSize().y == 2);
            CHECK(output.GetReconBeadVoxelSize().z == 3);
        }
        SECTION("GetDeconBeadData()") {
            std::shared_ptr<float[]> deconBeadData{ new float[1] };
            deconBeadData.get()[0] = 1;

            BeadEvaluatorMatlabOutput output;
            output.SetDeconBead(deconBeadData, {}, {});

            CHECK(output.GetDeconBeadData().get()[0] == 1);
        }
        SECTION("GetDeconBeadDataSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetDeconBead({}, {1,2,3}, {});
            CHECK(output.GetDeconBeadDataSize().x == 1);
            CHECK(output.GetDeconBeadDataSize().y == 2);
            CHECK(output.GetDeconBeadDataSize().z == 3);
        }
        SECTION("GetDeconBeadVoxelSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetDeconBead({}, {}, { 1, 2, 3 });
            CHECK(output.GetDeconBeadVoxelSize().x == 1);
            CHECK(output.GetDeconBeadVoxelSize().y == 2);
            CHECK(output.GetDeconBeadVoxelSize().z == 3);
        }
        SECTION("GetSimulatedBeadData()") {
            std::shared_ptr<float[]> simulatedBeadData{ new float[1] };
            simulatedBeadData.get()[0] = 1;

            BeadEvaluatorMatlabOutput output;
            output.SetSimulatedBead(simulatedBeadData, {}, {});

            CHECK(output.GetSimulatedBeadData().get()[0] == 1);
        }
        SECTION("GetSimulatedBeadDataSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetSimulatedBead({}, { 1,2,3 }, {});
            CHECK(output.GetSimulatedBeadDataSize().x == 1);
            CHECK(output.GetSimulatedBeadDataSize().y == 2);
            CHECK(output.GetSimulatedBeadDataSize().z == 3);
        }
        SECTION("GetSimulatedBeadVoxelSize()") {
            BeadEvaluatorMatlabOutput output;
            output.SetSimulatedBead({}, {}, { 1,2,3 });
            CHECK(output.GetSimulatedBeadVoxelSize().x == 1);
            CHECK(output.GetSimulatedBeadVoxelSize().y == 2);
            CHECK(output.GetSimulatedBeadVoxelSize().z == 3);
        }
    }

    TEST_CASE("BeadEvaluatorMatlabOutput : practical test") {
        BeadEvaluatorMatlabOutput srcOutput;

        srcOutput.SetVolume(1);
        srcOutput.SetDryMass(2);
        srcOutput.SetCorrelation(3);
        srcOutput.SetMeanDeltaRI(4);

        std::shared_ptr<float[]> reconBead{ new float[1]() };
        std::shared_ptr<float[]> deconBead{ new float[1]() };
        std::shared_ptr<float[]> simulatedBead{ new float[1]() };

        reconBead.get()[0] = 5;
        deconBead.get()[0] = 6;
        simulatedBead.get()[0] = 7;

        srcOutput.SetReconBead(reconBead, { 8,9,10 }, { 11,12,13 });
        srcOutput.SetDeconBead(deconBead, { 14,15,16 }, { 17,18,19 });
        srcOutput.SetSimulatedBead(simulatedBead, { 20,21,22 }, { 23,24,25 });

        const BeadEvaluatorMatlabOutput destOutput{ srcOutput };

        CHECK(destOutput.GetVolume() == 1);
        CHECK(destOutput.GetDryMass() == 2);
        CHECK(destOutput.GetCorrelation() == 3);
        CHECK(destOutput.GetMeanDeltaRI() == 4);

        CHECK(destOutput.GetReconBeadData().get()[0] == 5);
        CHECK(destOutput.GetDeconBeadData().get()[0] == 6);
        CHECK(destOutput.GetSimulatedBeadData().get()[0] == 7);

        CHECK(destOutput.GetReconBeadDataSize().x == 8);
        CHECK(destOutput.GetReconBeadDataSize().y == 9);
        CHECK(destOutput.GetReconBeadDataSize().z == 10);

        CHECK(destOutput.GetReconBeadVoxelSize().x == 11);
        CHECK(destOutput.GetReconBeadVoxelSize().y == 12);
        CHECK(destOutput.GetReconBeadVoxelSize().z == 13);

        CHECK(destOutput.GetDeconBeadDataSize().x == 14);
        CHECK(destOutput.GetDeconBeadDataSize().y == 15);
        CHECK(destOutput.GetDeconBeadDataSize().z == 16);

        CHECK(destOutput.GetDeconBeadVoxelSize().x == 17);
        CHECK(destOutput.GetDeconBeadVoxelSize().y == 18);
        CHECK(destOutput.GetDeconBeadVoxelSize().z == 19);

        CHECK(destOutput.GetSimulatedBeadDataSize().x == 20);
        CHECK(destOutput.GetSimulatedBeadDataSize().y == 21);
        CHECK(destOutput.GetSimulatedBeadDataSize().z == 22);

        CHECK(destOutput.GetSimulatedBeadVoxelSize().x == 23);
        CHECK(destOutput.GetSimulatedBeadVoxelSize().y == 24);
        CHECK(destOutput.GetSimulatedBeadVoxelSize().z == 25);

    }
}