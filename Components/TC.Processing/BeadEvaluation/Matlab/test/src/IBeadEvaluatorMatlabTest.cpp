#include <catch2/catch.hpp>

#include "IBeadEvaluatorMatlab.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace IBeadEvaluatorMatlabTest {
    class BeadEvaluatorMatlabForTest final : public IBeadEvaluatorMatlab {
    public:
        BeadEvaluatorMatlabForTest() = default;
        ~BeadEvaluatorMatlabForTest() = default;

        auto SetInput(const BeadEvaluatorMatlabInput& input) -> void override {
            this->setInputTriggered = true;
        }

        auto SetOutputPort(const IBeadEvaluatorMatlabOutputPort::Pointer& outputPort) -> void override {
            this->setOutputPortTriggered = true;
        }

        auto Evaluate() -> bool override {
            this->evaluateTriggered = true;
            return false;
        }

        bool setInputTriggered{ false };
        bool setOutputPortTriggered{ false };
        bool evaluateTriggered{ false };
    };

    TEST_CASE("IBeadEvaluatorMatlab : unit test") {
        SECTION("SetInput()") {
            BeadEvaluatorMatlabForTest beadEvaluator;
            CHECK(beadEvaluator.setInputTriggered == false);
            beadEvaluator.SetInput({});
            CHECK(beadEvaluator.setInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            BeadEvaluatorMatlabForTest beadEvaluator;
            CHECK(beadEvaluator.setOutputPortTriggered == false);
            beadEvaluator.SetOutputPort({});
            CHECK(beadEvaluator.setOutputPortTriggered == true);
        }
        SECTION("Evaluate()") {
            BeadEvaluatorMatlabForTest beadEvaluator;
            CHECK(beadEvaluator.evaluateTriggered == false);
            beadEvaluator.Evaluate();
            CHECK(beadEvaluator.evaluateTriggered == true);
        }
    }
}