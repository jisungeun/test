#include <catch2/catch.hpp>

#include "BeadEvaluatorMatlabInput.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace BeadEvaluatorMatlabInputTest {
    TEST_CASE("BeadEvaluatorMatlabInput : unit test") {
        SECTION("BeadEvaluatorMatlabInput()") {
            BeadEvaluatorMatlabInput input;
            CHECK(&input != nullptr);
        }
        SECTION("BeadEvaluatorMatlabInput(other)") {
            BeadEvaluatorMatlabInput srcInput;
            srcInput.SetPathInfo({ "1","2","3" });

            const BeadEvaluatorMatlabInput destInput(srcInput);
            CHECK(destInput.GetPathInfo().sampleFolderPath == "1");
            CHECK(destInput.GetPathInfo().backgroundFolderPath == "2");
            CHECK(destInput.GetPathInfo().psfFolderPath == "3");
        }
        SECTION("operator=()") {
            BeadEvaluatorMatlabInput srcInput;
            srcInput.SetPathInfo({ "1","2","3" });

            BeadEvaluatorMatlabInput destInput;
            destInput = srcInput;
            CHECK(destInput.GetPathInfo().sampleFolderPath == "1");
            CHECK(destInput.GetPathInfo().backgroundFolderPath == "2");
            CHECK(destInput.GetPathInfo().psfFolderPath == "3");
        }
        SECTION("SetModuleFilePathInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetModuleFilePathInfo({});
            CHECK(&input != nullptr);
        }
        SECTION("GetModuleFilePathInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetModuleFilePathInfo({"1","2"});
            CHECK(input.GetModuleFilePathInfo().beadEvaluation == "1");
            CHECK(input.GetModuleFilePathInfo().psf == "2");
        }
        SECTION("SetPathInfo()"){
            BeadEvaluatorMatlabInput input;
            input.SetPathInfo({ "","","" });
            CHECK(&input != nullptr);
        }
        SECTION("GetPathInfo()"){
            BeadEvaluatorMatlabInput input;
            input.SetPathInfo({ "1","2","3" });
            CHECK(input.GetPathInfo().sampleFolderPath == "1");
            CHECK(input.GetPathInfo().backgroundFolderPath == "2");
            CHECK(input.GetPathInfo().psfFolderPath == "3");
        }
        SECTION("SetDeviceInfo()"){
            BeadEvaluatorMatlabInput input;
            input.SetDeviceInfo({ 1,2,3,4,5,6 });
            CHECK(&input != nullptr);
        }
        SECTION("GetDeviceInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetDeviceInfo({ 1,2,3,4,5,6 });
            CHECK(input.GetDeviceInfo().mediumRI == 1);
            CHECK(input.GetDeviceInfo().naCond == 2);
            CHECK(input.GetDeviceInfo().naObj == 3);
            CHECK(input.GetDeviceInfo().magnification == 4);
            CHECK(input.GetDeviceInfo().pixelSize == 5);
            CHECK(input.GetDeviceInfo().zStepLength == 6);
        }
        SECTION("SetImageInfo()"){
            BeadEvaluatorMatlabInput input;
            input.SetImageInfo({ 1,2,3,4,5,6 });
            CHECK(&input != nullptr);
        }
        SECTION("GetImageInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetImageInfo({ 1,2,3,4,5,6 });
            CHECK(input.GetImageInfo().sampleCropOffsetX == 1);
            CHECK(input.GetImageInfo().sampleCropOffsetY == 2);
            CHECK(input.GetImageInfo().backgroundCropOffsetX == 3);
            CHECK(input.GetImageInfo().backgroundCropOffsetY == 4);
            CHECK(input.GetImageInfo().imageSizeX == 5);
            CHECK(input.GetImageInfo().imageSizeY == 6);
        }
        SECTION("SetBeadInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetBeadInfo({ 1,2,3,4 });
            CHECK(&input != nullptr);
        }
        SECTION("GetBeadInfo()") {
            BeadEvaluatorMatlabInput input;
            input.SetBeadInfo({ 1,2,3,4 });
            CHECK(input.GetBeadInfo().beadCenterPositionXInMicrometer == 1);
            CHECK(input.GetBeadInfo().beadCenterPositionYInMicrometer == 2);
            CHECK(input.GetBeadInfo().beadCropSizeXInMicrometer == 3);
            CHECK(input.GetBeadInfo().beadCropSizeYInMicrometer == 4);
        }
    }

    TEST_CASE("BeadEvaluatorMatlabInput : practical test") {
        BeadEvaluatorMatlabInput srcInput;
        srcInput.SetPathInfo({ "1","2","3" });
        srcInput.SetDeviceInfo({ 4,5,6,7,8,9 });
        srcInput.SetImageInfo({ 9,10,11,12,13,14 });
        srcInput.SetBeadInfo({ 15,16,17,18 });

        const BeadEvaluatorMatlabInput destInput(srcInput);

        const auto pathInfo = destInput.GetPathInfo();
        const auto deviceInfo = destInput.GetDeviceInfo();
        const auto imageInfo = destInput.GetImageInfo();
        const auto beadInfo = destInput.GetBeadInfo();

        CHECK(pathInfo.sampleFolderPath == "1");
        CHECK(pathInfo.backgroundFolderPath == "2");
        CHECK(pathInfo.psfFolderPath == "3");

        CHECK(deviceInfo.mediumRI == 4);
        CHECK(deviceInfo.naCond == 5);
        CHECK(deviceInfo.naObj == 6);
        CHECK(deviceInfo.magnification == 7);
        CHECK(deviceInfo.pixelSize == 8);
        CHECK(deviceInfo.zStepLength == 9);

        CHECK(imageInfo.sampleCropOffsetX == 9);
        CHECK(imageInfo.sampleCropOffsetY == 10);
        CHECK(imageInfo.backgroundCropOffsetX == 11);
        CHECK(imageInfo.backgroundCropOffsetY == 12);
        CHECK(imageInfo.imageSizeX == 13);
        CHECK(imageInfo.imageSizeY == 14);

        CHECK(beadInfo.beadCenterPositionXInMicrometer == 15);
        CHECK(beadInfo.beadCenterPositionYInMicrometer == 16);
        CHECK(beadInfo.beadCropSizeXInMicrometer == 17);
        CHECK(beadInfo.beadCropSizeYInMicrometer == 18);
    }
}