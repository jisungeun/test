#include <iostream>
#include <catch2/catch.hpp>

#include "BeadEvaluatorMatlab.h"

#include "PSFProfileFactory.h"

using namespace TC::Processing::BeadEvaluationMatlab;
using namespace TC::PSFProfile;

namespace BeadEvaluatorMatlabTest {
    class BeadEvaluatorMatlabOutputPortForTest final : public IBeadEvaluatorMatlabOutputPort {
    public:
        BeadEvaluatorMatlabOutputPortForTest() = default;
        ~BeadEvaluatorMatlabOutputPortForTest() = default;

        auto SetProgress(const float& percentage, const QString& description) -> void override {
            this->percentage = percentage;
            this->description = description;
        }
        auto SetOutput(const BeadEvaluatorMatlabOutput& output) -> void override {
            this->output = output;
        }
        
        float percentage{};
        QString description{};
        BeadEvaluatorMatlabOutput output{};
    };

    TEST_CASE("BeadEvaluatorMatlab : unit test") {
        
    }
    TEST_CASE("BeadEvaulatorMatlab : practical test") {
        BeadEvaluatorMatlabInput::ModuleFilePathInfo moduleFilePathInfo;
        moduleFilePathInfo.beadEvaluation = "D:/Work/SourceTreeRepository/HTX/Components/TC.Processing.BeadEvaluationMatlab/extern/BeadModule.ctf";
        moduleFilePathInfo.psf = "D:/Work/SourceTreeRepository/HTX/Components/TC.Processing.PSFMatlab/extern/PSFModule_v1_4_1_c.ctf";
        
        BeadEvaluatorMatlabInput::PathInfo pathInfo;
        pathInfo.sampleFolderPath = "E:/00_Data/20221020 BeadScoring/beadSample/data";
        pathInfo.backgroundFolderPath = "E:/00_Data/20221020 BeadScoring/beadSample/bg";
        pathInfo.psfFolderPath = "C:/Users/Johnny/AppData/Local/Tomocube, Inc/TomoStudioX/PSF";
        
        BeadEvaluatorMatlabInput::DeviceInfo deviceInfo;
        deviceInfo.mediumRI = 1.5734;
        deviceInfo.naCond = 0.72;
        deviceInfo.naObj = 0.92;
        deviceInfo.magnification = 40;
        deviceInfo.pixelSize = 4.5;
        deviceInfo.zStepLength = 20*0.039;

        BeadEvaluatorMatlabInput::ImageInfo imageInfo;
        imageInfo.sampleCropOffsetX = 0;
        imageInfo.sampleCropOffsetY = 0;
        imageInfo.backgroundCropOffsetX = 0;
        imageInfo.backgroundCropOffsetY = 0;
        imageInfo.imageSizeX = 1936;
        imageInfo.imageSizeY = 1464;

        BeadEvaluatorMatlabInput::BeadInfo beadInfo;
        beadInfo.beadCenterPositionXInMicrometer = 109.35;
        beadInfo.beadCenterPositionYInMicrometer = 81;
        beadInfo.beadCropSizeXInMicrometer = 56.25;
        beadInfo.beadCropSizeYInMicrometer = 56.25;

        BeadEvaluatorMatlabInput input;
        input.SetModuleFilePathInfo(moduleFilePathInfo);
        input.SetPathInfo(pathInfo);
        input.SetDeviceInfo(deviceInfo);
        input.SetImageInfo(imageInfo);
        input.SetBeadInfo(beadInfo);

        PSFProfileFactory::Input_v1_4_1_c psfProfileFactoryInput;
        psfProfileFactoryInput.imageSizeX = 1936;
        psfProfileFactoryInput.imageSizeY = 1464;
        psfProfileFactoryInput.naCond = 0.72;

        const auto psfProfile = PSFProfileFactory::Generate_v1_4_1_c(psfProfileFactoryInput);

        IBeadEvaluatorMatlabOutputPort::Pointer outputPortPointer{ new BeadEvaluatorMatlabOutputPortForTest };

        BeadEvaluatorMatlab beadEvaluator;
        beadEvaluator.SetInput(input);
        beadEvaluator.SetPSFProfile(psfProfile);
        beadEvaluator.SetOutputPort(outputPortPointer);
        CHECK(beadEvaluator.Evaluate() == true);

        const auto outputPort = std::dynamic_pointer_cast<BeadEvaluatorMatlabOutputPortForTest>(outputPortPointer);

        CHECK(outputPort->percentage == 100);
        CHECK(outputPort->description == "Bead Evaluation Done");

        std::cout << "correlation : " << outputPort->output.GetCorrelation() << std::endl;
        std::cout << "drymass : " << outputPort->output.GetDryMass() << std::endl;
        std::cout << "volume : " << outputPort->output.GetVolume() << std::endl;
        std::cout << "meanDeltaRI : " << outputPort->output.GetMeanDeltaRI() << std::endl;
    }
}