#include <catch2/catch.hpp>

#include "BeadEvaluationParameters_v1_4_1.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace BeadEvaluationParametersTest {
    TEST_CASE("unit test") {
        SECTION("BeadEvaluationParameters()") {
            BeadEvaluationParameters_v1_4_1 beadEvaluationParameters;
            CHECK(&beadEvaluationParameters != nullptr);
        }
        SECTION("BeadEvaluationParameters_v1_4_1(other)") {
            BeadEvaluationParameters_v1_4_1 srcParameters;
            srcParameters.SetMediumRI(1);

            const BeadEvaluationParameters_v1_4_1 destParameters{ srcParameters };
            CHECK(destParameters.GetMediumRI() == 1);
        }
        SECTION("operator=()") {
            BeadEvaluationParameters_v1_4_1 srcParameters;
            srcParameters.SetMediumRI(1);

            BeadEvaluationParameters_v1_4_1 destParameters;
            destParameters = srcParameters;
            CHECK(destParameters.GetMediumRI() == 1);
        }
        SECTION("SetSampleFolderPath()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSampleFolderPath("");
            CHECK(&parameters != nullptr);
        }
        SECTION("GetSampleFolderPath()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSampleFolderPath("1");
            CHECK(parameters.GetSampleFolderPath() == "1");
        }
        SECTION("SetBackgroundFolderPath()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBackgroundFolderPath("1");
            CHECK(&parameters != nullptr);
        }
        SECTION("GetBackgroundFolderPath()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBackgroundFolderPath("1");
            CHECK(parameters.GetBackgroundFolderPath() == "1");
        }
        SECTION("SetMediumRI()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetMediumRI(1);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetMediumRI()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetMediumRI(1);
            CHECK(parameters.GetMediumRI() == 1);
        }
        SECTION("SetNACond()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetNACond(1);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetNACond()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetNACond(1);
            CHECK(parameters.GetNACond() == 1);
        }
        SECTION("SetVoxelSize()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetVoxelSize(1, 2, 3);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetVolxeSizeX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetVoxelSize(1, 2, 3);
            CHECK(parameters.GetVoxelSizeX() == 1);
        }
        SECTION("GetVolxeSizeY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetVoxelSize(1, 2, 3);
            CHECK(parameters.GetVoxelSizeY() == 2);
        }
        SECTION("GetVolxeSizeZ()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetVoxelSize(1, 2, 3);
            CHECK(parameters.GetVoxelSizeZ() == 3);
        }
        SECTION("SetSampleCropOffset()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSampleCropOffset(1, 2);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetSampleCropOffsetX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSampleCropOffset(1, 2);
            CHECK(parameters.GetSampleCropOffsetX() == 1);
        }
        SECTION("GetSampleCropOffsetY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSampleCropOffset(1, 2);
            CHECK(parameters.GetSampleCropOffsetY() == 2);
        }
        SECTION("SetBackgroundCropOffset()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBackgroundCropOffset(1, 2);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetBackgroundCropOffsetX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBackgroundCropOffset(1, 2);
            CHECK(parameters.GetBackgroundCropOffsetX() == 1);
        }
        SECTION("GetBackgroundCropOffsetY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBackgroundCropOffset(1, 2);
            CHECK(parameters.GetBackgroundCropOffsetY() == 2);
        }
        SECTION("SetCropSize()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetCropSize(1, 2);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetCropSizeX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetCropSize(1, 2);
            CHECK(parameters.GetCropSizeX() == 1);
        }
        SECTION("GetCropSizeY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetCropSize(1, 2);
            CHECK(parameters.GetCropSizeY() == 2);
        }
        SECTION("SetPSF()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSF(nullptr);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetPSF()") {
            std::shared_ptr<float[]> psf{ new float[1] };
            psf.get()[0] = 1;

            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSF(psf);
            CHECK(parameters.GetPSF().get()[0] == 1);
        }
        SECTION("SetPSFSize()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetPSFRealImagCount()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(parameters.GetPSFRealImagCount() == 1);
        }
        SECTION("GetPSFPatternNumber()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(parameters.GetPSFPatternNumber() == 2);
        }
        SECTION("GetPSFSizeX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(parameters.GetPSFSizeX() == 3);
        }
        SECTION("GetPSFSizeY()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(parameters.GetPSFSizeY() == 4);
        }
        SECTION("GetPSFSizeZ()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetPSFSize(1, 2, 3, 4, 5);
            CHECK(parameters.GetPSFSizeZ() == 5);
        }
        SECTION("SetSupport()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupport(nullptr);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetSupport()") {
            std::shared_ptr<float[]> support{ new float[1] };
            support.get()[0] = 1;

            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupport(support);
            CHECK(parameters.GetSupport().get()[0] == 1);
        }
        SECTION("SetSupportSize()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupportSize(1, 2, 3);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetSupportSizeX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupportSize(1, 2, 3);
            CHECK(parameters.GetSupportSizeX() == 1);
        }
        SECTION("GetSupportSizeY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupportSize(1, 2, 3);
            CHECK(parameters.GetSupportSizeY() == 2);
        }
        SECTION("GetSupportSizeZ()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetSupportSize(1, 2, 3);
            CHECK(parameters.GetSupportSizeZ() == 3);
        }
        SECTION("SetKRes()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetKRes(1, 2, 3);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetKResX()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetKRes(1, 2, 3);
            CHECK(parameters.GetKResX() == 1);
        }
        SECTION("GetKResY()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetKRes(1, 2, 3);
            CHECK(parameters.GetKResY() == 2);
        }
        SECTION("GetKResZ()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetKRes(1, 2, 3);
            CHECK(parameters.GetKResZ() == 3);
        }
        SECTION("SetBeadCenterPositionInMicrometer()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCenterPositionInMicrometer(1, 2);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetBeadCenterPositionXInMicrometer()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCenterPositionInMicrometer(1, 2);
            CHECK(parameters.GetBeadCenterPositionXInMicrometer() == 1);
        }
        SECTION("GetBeadCenterPositionYInMicrometer()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCenterPositionInMicrometer(1, 2);
            CHECK(parameters.GetBeadCenterPositionYInMicrometer() == 2);
        }
        SECTION("SetBeadCropSizeInMicrometer()"){
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCropSizeInMicrometer(1, 2);
            CHECK(&parameters != nullptr);
        }
        SECTION("GetBeadCropSizeXInMicrometer()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCropSizeInMicrometer(1, 2);
            CHECK(parameters.GetBeadCropSizeXInMicrometer() == 1);
        }
        SECTION("GetBeadCropSizeYInMicrometer()") {
            BeadEvaluationParameters_v1_4_1 parameters;
            parameters.SetBeadCropSizeInMicrometer(1, 2);
            CHECK(parameters.GetBeadCropSizeYInMicrometer() == 2);
        }
    }
    TEST_CASE("practical test") {
        BeadEvaluationParameters_v1_4_1 srcParameters;

        std::shared_ptr<float[]> psf{ new float[1] };
        std::shared_ptr<float[]> support{ new float[1] };
        
        srcParameters.SetSampleFolderPath("1");
        srcParameters.SetBackgroundFolderPath("2");
        srcParameters.SetMediumRI(3);
        srcParameters.SetNACond(4);
        srcParameters.SetVoxelSize(5, 6, 7);
        srcParameters.SetSampleCropOffset(8, 9);
        srcParameters.SetBackgroundCropOffset(10, 11);
        srcParameters.SetCropSize(12, 13);
        
        psf.get()[0] = 14;
        srcParameters.SetPSF(psf);
        srcParameters.SetPSFSize(15, 16, 17, 18, 19);

        support.get()[0] = 18;
        srcParameters.SetSupport(support);

        srcParameters.SetSupportSize(19, 20, 21);
        srcParameters.SetKRes(22, 23, 24);
        srcParameters.SetBeadCenterPositionInMicrometer(25, 26);
        srcParameters.SetBeadCropSizeInMicrometer(27, 28);

        BeadEvaluationParameters_v1_4_1 destParameters;
        destParameters = srcParameters;

        CHECK(destParameters.GetSampleFolderPath() == "1");
        CHECK(destParameters.GetBackgroundFolderPath() == "2");
        CHECK(destParameters.GetMediumRI() == 3);
        CHECK(destParameters.GetNACond() == 4);
        CHECK(destParameters.GetVoxelSizeX() == 5);
        CHECK(destParameters.GetVoxelSizeY() == 6);
        CHECK(destParameters.GetVoxelSizeZ() == 7);
        CHECK(destParameters.GetSampleCropOffsetX() == 8);
        CHECK(destParameters.GetSampleCropOffsetY() == 9);
        CHECK(destParameters.GetBackgroundCropOffsetX() == 10);
        CHECK(destParameters.GetBackgroundCropOffsetY() == 11);
        CHECK(destParameters.GetCropSizeX() == 12);
        CHECK(destParameters.GetCropSizeY() == 13);
        CHECK(destParameters.GetPSF().get()[0] == 14);
        CHECK(destParameters.GetPSFRealImagCount() == 15);
        CHECK(destParameters.GetPSFPatternNumber() == 16);
        CHECK(destParameters.GetPSFSizeX() == 17);
        CHECK(destParameters.GetPSFSizeY() == 18);
        CHECK(destParameters.GetPSFSizeZ() == 19);
        CHECK(destParameters.GetSupport().get()[0] == 18);
        CHECK(destParameters.GetSupportSizeX() == 19);
        CHECK(destParameters.GetSupportSizeY() == 20);
        CHECK(destParameters.GetSupportSizeZ() == 21);
        CHECK(destParameters.GetKResX() == 22);
        CHECK(destParameters.GetKResY() == 23);
        CHECK(destParameters.GetKResZ() == 24);
        CHECK(destParameters.GetBeadCenterPositionXInMicrometer() == 25);
        CHECK(destParameters.GetBeadCenterPositionYInMicrometer() == 26);
        CHECK(destParameters.GetBeadCropSizeXInMicrometer() == 27);
        CHECK(destParameters.GetBeadCropSizeYInMicrometer() == 28);
    }
}