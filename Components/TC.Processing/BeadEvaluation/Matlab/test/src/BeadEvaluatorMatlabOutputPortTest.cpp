#include <catch2/catch.hpp>

#include "BeadEvaluatorMatlabOutputPort.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace BeadEvaluatorMatlabOutputPortTest {
    TEST_CASE("BeadEvaluatorMatlabOutputPort : unit test") {
        SECTION("BeadEvaluatorMatlabOutputPort()") {
            BeadEvaluatorMatlabOutputPort outputPort;
            CHECK(&outputPort != nullptr);
        }
        SECTION("SetProgress()") {
            BeadEvaluatorMatlabOutputPort outputPort;
            outputPort.SetProgress(1, "2");
            CHECK(&outputPort != nullptr);
        }
        SECTION("SetOutput()") {
            BeadEvaluatorMatlabOutputPort outputPort;
            outputPort.SetOutput({});
            CHECK(&outputPort != nullptr);
        }
        SECTION("GetProgress()") {
            BeadEvaluatorMatlabOutputPort outputPort;
            SECTION("default description") {
                outputPort.SetProgress(1);
                CHECK(outputPort.GetProgress().percentage == 1);
                CHECK(outputPort.GetProgress().description == "");
            }
            SECTION("setting description") {
                outputPort.SetProgress(1, "2");
                CHECK(outputPort.GetProgress().percentage == 1);
                CHECK(outputPort.GetProgress().description == "2");
            }
        }
        SECTION("GetOutput()") {
            BeadEvaluatorMatlabOutput output;
            output.SetCorrelation(1);

            BeadEvaluatorMatlabOutputPort outputPort;
            outputPort.SetOutput(output);
            CHECK(outputPort.GetOutput().GetCorrelation() == 1);
        }
    }
    TEST_CASE("BeadEvaluatorMatlabOutputPort : practical test") {
        BeadEvaluatorMatlabOutput output;
        output.SetCorrelation(1);


        BeadEvaluatorMatlabOutputPort outputPort;
        outputPort.SetProgress(2, "3");
        outputPort.SetOutput(output);

        CHECK(outputPort.GetOutput().GetCorrelation() == 1);
        CHECK(outputPort.GetProgress().percentage == 2);
        CHECK(outputPort.GetProgress().description == "3");
    }
}