#include <catch2/catch.hpp>

#include "IBeadEvaluatorMatlabOutputPort.h"

using namespace TC::Processing::BeadEvaluationMatlab;

namespace IBeadEvaluatorMatlabOutputPortTest {
    class BeadEvaluatorMatlabOutputPortForTest final : public IBeadEvaluatorMatlabOutputPort {
    public:
        BeadEvaluatorMatlabOutputPortForTest() = default;
        ~BeadEvaluatorMatlabOutputPortForTest() = default;

        auto SetOutput(const BeadEvaluatorMatlabOutput& output) -> void override {
            this->setOutputTriggered = true;
        }

        auto SetProgress(const float& percentage, const QString& description = "") -> void override {
            this->setProgressTriggered = true;
        }

        bool setProgressTriggered{ false };
        bool setOutputTriggered{ false };
    };

    TEST_CASE("IBeadEvaluatorMatlabOutputPort : unit test") {
        SECTION("SetProgress()") {
            BeadEvaluatorMatlabOutputPortForTest outputPort;
            CHECK(outputPort.setProgressTriggered== false);
            outputPort.SetProgress(0);
            CHECK(outputPort.setProgressTriggered == true);
        }

        SECTION("SetOutput()") {
            BeadEvaluatorMatlabOutputPortForTest outputPort;
            CHECK(outputPort.setOutputTriggered == false);
            outputPort.SetOutput({});
            CHECK(outputPort.setOutputTriggered == true);
        }
    }
}