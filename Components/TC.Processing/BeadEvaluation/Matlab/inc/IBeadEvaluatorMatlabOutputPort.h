#pragma once

#include <QString>
#include "TCBeadEvaluationMatlabExport.h"
#include "BeadEvaluatorMatlabOutput.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API IBeadEvaluatorMatlabOutputPort {
    public:
        using Pointer = std::shared_ptr<IBeadEvaluatorMatlabOutputPort>;

        virtual ~IBeadEvaluatorMatlabOutputPort();
        virtual auto SetProgress(const float& percentage, const QString& description = "")->void = 0;
        virtual auto SetOutput(const BeadEvaluatorMatlabOutput& output)->void = 0;
    };
}