#pragma once

#include "TCBeadEvaluationMatlabExport.h"
#include "BeadEvaluatorMatlabInput.h"
#include "IBeadEvaluatorMatlabOutputPort.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API IBeadEvaluatorMatlab {
    public:
        using Pointer = std::shared_ptr<IBeadEvaluatorMatlab>;

        virtual ~IBeadEvaluatorMatlab();

        virtual auto SetInput(const BeadEvaluatorMatlabInput& input)->void = 0;
        virtual auto SetOutputPort(const IBeadEvaluatorMatlabOutputPort::Pointer& outputPort)->void = 0;

        virtual auto Evaluate()->bool = 0;
    };
}