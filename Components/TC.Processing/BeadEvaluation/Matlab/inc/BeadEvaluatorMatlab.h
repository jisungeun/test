#pragma once

#include <memory>
#include <QString>
#include "TCBeadEvaluationMatlabExport.h"
#include "BeadEvaluatorMatlabInput.h"
#include "IBeadEvaluatorMatlab.h"
#include "PSFProfile_v1_4_1_c.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API BeadEvaluatorMatlab final : public IBeadEvaluatorMatlab {
    public:
        BeadEvaluatorMatlab();
        ~BeadEvaluatorMatlab();

        auto SetInput(const BeadEvaluatorMatlabInput& input)->void override;
        auto SetOutputPort(const IBeadEvaluatorMatlabOutputPort::Pointer& outputPort)->void override;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void;
        auto Evaluate()->bool override;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
