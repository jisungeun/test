#pragma once

#include <memory>

#include "TCBeadEvaluationMatlabExport.h"
#include "IBeadEvaluatorMatlabOutputPort.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API BeadEvaluatorMatlabOutputPort final : public IBeadEvaluatorMatlabOutputPort {
    public:
        struct Progress {
            float percentage{};
            QString description{};
        };

        BeadEvaluatorMatlabOutputPort();
        ~BeadEvaluatorMatlabOutputPort();

        auto SetProgress(const float& percentage, const QString& description = "") -> void override;
        auto SetOutput(const BeadEvaluatorMatlabOutput& output) -> void override;

        auto GetProgress()const->Progress;
        auto GetOutput()const->BeadEvaluatorMatlabOutput;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}