#pragma once

#include <memory>
#include <QString>

#include "TCBeadEvaluationMatlabExport.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API BeadEvaluatorMatlabInput {
    public:
        struct ModuleFilePathInfo {
            QString beadEvaluation{};
            QString psf{};
        };

        struct PathInfo {
            QString sampleFolderPath{};
            QString backgroundFolderPath{};
            QString psfFolderPath{};
        };

        struct DeviceInfo {
            double mediumRI{};
            double naCond{};
            double naObj{};
            double magnification{};
            double pixelSize{};
            double zStepLength{};
        };

        struct ImageInfo {
            int32_t sampleCropOffsetX{};
            int32_t sampleCropOffsetY{};
            int32_t backgroundCropOffsetX{};
            int32_t backgroundCropOffsetY{};
            int32_t imageSizeX{};
            int32_t imageSizeY{};
        };

        struct BeadInfo {
            double beadCenterPositionXInMicrometer{};
            double beadCenterPositionYInMicrometer{};
            double beadCropSizeXInMicrometer{};
            double beadCropSizeYInMicrometer{};
        };

        BeadEvaluatorMatlabInput();
        BeadEvaluatorMatlabInput(const BeadEvaluatorMatlabInput& other);
        ~BeadEvaluatorMatlabInput();

        auto operator=(const BeadEvaluatorMatlabInput& other)->BeadEvaluatorMatlabInput&;

        auto SetModuleFilePathInfo(const ModuleFilePathInfo& moduleFilePathInfo)->void;
        auto GetModuleFilePathInfo()const->const ModuleFilePathInfo&;

        auto SetPathInfo(const PathInfo& pathInfo)->void;
        auto GetPathInfo()const->const PathInfo&;

        auto SetDeviceInfo(const DeviceInfo& deviceInfo)->void;
        auto GetDeviceInfo()const->const DeviceInfo&;

        auto SetImageInfo(const ImageInfo& imageInfo)->void;
        auto GetImageInfo()const->const ImageInfo&;

        auto SetBeadInfo(const BeadInfo& beadInfo)->void;
        auto GetBeadInfo()const->const BeadInfo&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}