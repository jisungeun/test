#pragma once

#include <memory>
#include "TCBeadEvaluationMatlabExport.h"

namespace TC::Processing::BeadEvaluationMatlab {
    class TCBeadEvaluationMatlab_API BeadEvaluatorMatlabOutput {
    public:
        struct DataSize {
            int32_t x{};
            int32_t y{};
            int32_t z{};
        };

        struct VoxelSize {
            double x{};
            double y{};
            double z{};
        };

        BeadEvaluatorMatlabOutput();
        BeadEvaluatorMatlabOutput(const BeadEvaluatorMatlabOutput& other);
        ~BeadEvaluatorMatlabOutput();

        auto operator=(const BeadEvaluatorMatlabOutput& other)->BeadEvaluatorMatlabOutput&;

        auto SetVolume(const double& volume)->void;
        auto SetDryMass(const double& dryMass)->void;
        auto SetCorrelation(const double& correlation)->void;
        auto SetMeanDeltaRI(const double& meanDeltaRI)->void;

        auto GetVolume()const->const double&;
        auto GetDryMass()const->const double&;
        auto GetCorrelation()const->const double&;
        auto GetMeanDeltaRI()const->const double&;
        
        auto SetReconBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize, const VoxelSize& voxelSize)->void;
        auto SetDeconBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize, const VoxelSize& voxelSize)->void;
        auto SetSimulatedBead(const std::shared_ptr<float[]>& data, const DataSize& dataSize, const VoxelSize& voxelSize)->void;

        auto GetReconBeadData()const->const std::shared_ptr<float[]>&;
        auto GetReconBeadDataSize()const->const DataSize&;
        auto GetReconBeadVoxelSize()const->const VoxelSize&;

        auto GetDeconBeadData()const->const std::shared_ptr<float[]>&;
        auto GetDeconBeadDataSize()const->const DataSize&;
        auto GetDeconBeadVoxelSize()const->const VoxelSize&;

        auto GetSimulatedBeadData()const->const std::shared_ptr<float[]>&;
        auto GetSimulatedBeadDataSize()const->const DataSize&;
        auto GetSimulatedBeadVoxelSize()const->const VoxelSize&;
        
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}