#define LOGGER_TAG "[main]"

#include <fstream>
#include <iostream>

#include <QThread>
#include <QDir>
#include <QStandardPaths>

#include <QString>

#include "BeadEvaluatorMatlab.h"
#include "BeadEvaluatorMatlabOutputPort.h"
#include "InputReader.h"
#include "Outputs.h"
#include "OutputWriter.h"
#include "PSFProfileFactory.h"
#include "PSFProfileVersion.h"

#include "TCLogger.h"

using namespace TC::Processing::BeadEvaluationMatlab;
using namespace TC::Processing::BeadEvaluationMatlabAppComponent;

auto ToBeadEvaluatorMatlabInput(const Inputs& inputs)->BeadEvaluatorMatlabInput {
    BeadEvaluatorMatlabInput::ModuleFilePathInfo moduleFilePathInfo;
    moduleFilePathInfo.beadEvaluation = inputs.beadModuleFilePath;
    moduleFilePathInfo.psf = inputs.psfModuleFilePath;
    
    BeadEvaluatorMatlabInput::PathInfo pathInfo;
    pathInfo.sampleFolderPath = inputs.sampleFolderPath;
    pathInfo.backgroundFolderPath = inputs.backgroundFolderPath;
    pathInfo.psfFolderPath = inputs.psfFolderPath;

    BeadEvaluatorMatlabInput::DeviceInfo deviceInfo;
    deviceInfo.mediumRI = inputs.mediumRI;
    deviceInfo.naCond = inputs.naCond;
    deviceInfo.naObj = inputs.naObj;
    deviceInfo.magnification = inputs.magnification;
    deviceInfo.pixelSize = inputs.pixelSize;
    deviceInfo.zStepLength = inputs.zStepLength;

    BeadEvaluatorMatlabInput::ImageInfo imageInfo;
    imageInfo.sampleCropOffsetX = inputs.sampleCropOffsetX;
    imageInfo.sampleCropOffsetY = inputs.sampleCropOffsetY;
    imageInfo.backgroundCropOffsetX = inputs.backgroundCropOffsetX;
    imageInfo.backgroundCropOffsetY = inputs.backgroundCropOffsetY;
    imageInfo.imageSizeX = inputs.imageSizeX;
    imageInfo.imageSizeY = inputs.imageSizeY;

    BeadEvaluatorMatlabInput::BeadInfo beadInfo;
    beadInfo.beadCenterPositionXInMicrometer = inputs.beadCenterPositionXInMicrometer;
    beadInfo.beadCenterPositionYInMicrometer = inputs.beadCenterPositionYInMicrometer;
    beadInfo.beadCropSizeXInMicrometer = inputs.beadCropSizeXInMicrometer;
    beadInfo.beadCropSizeYInMicrometer = inputs.beadCropSizeYInMicrometer;
    
    BeadEvaluatorMatlabInput input;
    input.SetModuleFilePathInfo(moduleFilePathInfo);
    input.SetPathInfo(pathInfo);
    input.SetDeviceInfo(deviceInfo);
    input.SetImageInfo(imageInfo);
    input.SetBeadInfo(beadInfo);

    return input;
}

auto LogInput(const Inputs& inputs)->void {
    QLOG_INFO() << "---inputs---";
    QLOG_INFO() << "bead Module : " << inputs.beadModuleFilePath.toStdString().c_str();
    QLOG_INFO() << "psf Module : " << inputs.psfModuleFilePath.toStdString().c_str();
    QLOG_INFO() << "sampleFolderPath : " << inputs.sampleFolderPath.toStdString().c_str();
    QLOG_INFO() << "backgroundFolderPath : " << inputs.backgroundFolderPath.toStdString().c_str();
    QLOG_INFO() << "psfFolderPath : " << inputs.psfFolderPath.toStdString().c_str();
    QLOG_INFO() << "sampleCrop : X(" << inputs.sampleCropOffsetX << "~" << inputs.sampleCropOffsetX + inputs.imageSizeX - 1 << "),Y(" << inputs.sampleCropOffsetY << "~" << inputs.sampleCropOffsetY + inputs.imageSizeY - 1 << ")";
    QLOG_INFO() << "backgroundCrop : X(" << inputs.backgroundCropOffsetX << "~" << inputs.backgroundCropOffsetX + inputs.imageSizeX - 1 << "),Y(" << inputs.backgroundCropOffsetY << "~" << inputs.backgroundCropOffsetY + inputs.imageSizeY - 1 << ")";
    QLOG_INFO() << "mediumRi : " << inputs.mediumRI;
    QLOG_INFO() << "objectiveNA : " << inputs.naObj;
    QLOG_INFO() << "condenserNA : " << inputs.naCond;
    QLOG_INFO() << "magnification : " << inputs.magnification;
    QLOG_INFO() << "pixelSize : " << inputs.pixelSize;
    QLOG_INFO() << "zStepLength : " << inputs.zStepLength;
    QLOG_INFO() << "beadCenter : [ x" << inputs.beadCenterPositionXInMicrometer << "um, y" << inputs.beadCenterPositionYInMicrometer << "um ]";
    QLOG_INFO() << "beadCropSize : [ x" << inputs.beadCropSizeXInMicrometer << "um, y" << inputs.beadCropSizeYInMicrometer << "um ]";
    QLOG_INFO() << "-----------";
}

constexpr auto scanningTimeInSec = 1;

using namespace TC::PSFProfile;

int main(int argc, char* argv[]){
    const QString scanningFolderPath = QString::fromLocal8Bit(argv[1]);

    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    TC::Logger::Initialize(QString("%1/TomoCube, Inc/TCBeadEvaluationMatlabApp/TCBeadEvaluationMatlabApp.log").arg(appDataPath), QString("%1/TomoCube, Inc/TCBeadEvaluationMatlabApp/TCBeadEvaluationMatlabAppError.log").arg(appDataPath));

    QLOG_INFO() << "=============started==============";
    QLOG_INFO() << "Base Algorithm v1.4.1";
    QLOG_INFO() << "scanning Folder : " << argv[1];

    while (true) {
        const QString inputFilePath = QString("%1/inputFile").arg(scanningFolderPath);
        const QString inputDoneFilePath = QString("%1/inputFile_Done").arg(scanningFolderPath);

        bool evaluationFail = false;

        if (QFile::exists(inputDoneFilePath)) {
            try {
                InputReader inputReader;
                inputReader.SetInputFilePath(inputFilePath);
                inputReader.Read();

                QFile::remove(inputDoneFilePath);

                const auto inputs = inputReader.GetInput();
                LogInput(inputs);
                const auto beadEvaluationMatlabInput = ToBeadEvaluatorMatlabInput(inputs);

                PSFProfileFactory::Input_v1_4_1_c psfProfileFactoryInput;
                psfProfileFactoryInput.imageSizeX = inputs.imageSizeX;
                psfProfileFactoryInput.imageSizeY = inputs.imageSizeY;
                psfProfileFactoryInput.naCond = inputs.naCond;

                const auto psfProfile = PSFProfileFactory::Generate_v1_4_1_c(psfProfileFactoryInput);

                const auto beadEvaluationMatlabOutputPortPointer = IBeadEvaluatorMatlabOutputPort::Pointer{ new BeadEvaluatorMatlabOutputPort };

                //run process
                BeadEvaluatorMatlab beadEvaluator;
                beadEvaluator.SetInput(beadEvaluationMatlabInput);
                beadEvaluator.SetPSFProfile(psfProfile);
                beadEvaluator.SetOutputPort(beadEvaluationMatlabOutputPortPointer);

                if (!beadEvaluator.Evaluate()) {
                    QLOG_ERROR() << "beadEvaluator.Evaluate() fails";
                    evaluationFail = true;
                } else {
                    //get results
                    const auto outputPresenter = 
                        std::dynamic_pointer_cast<BeadEvaluatorMatlabOutputPort>(beadEvaluationMatlabOutputPortPointer);

                    const auto beadEvaluationOutput = outputPresenter->GetOutput();
                    const auto outputFilePath = QString("%1/outputFile").arg(scanningFolderPath);

                    Outputs outputs;
                    outputs.volume = beadEvaluationOutput.GetVolume();
                    outputs.dryMass = beadEvaluationOutput.GetDryMass();
                    outputs.correlation = beadEvaluationOutput.GetCorrelation();
                    outputs.meanDeltaRI = beadEvaluationOutput.GetMeanDeltaRI();

                    outputs.reconBeadData = beadEvaluationOutput.GetReconBeadData();
                    outputs.reconBeadSizeX = beadEvaluationOutput.GetReconBeadDataSize().x;
                    outputs.reconBeadSizeY = beadEvaluationOutput.GetReconBeadDataSize().y;
                    outputs.reconBeadSizeZ = beadEvaluationOutput.GetReconBeadDataSize().z;
                    outputs.reconBeadVoxelSizeX = beadEvaluationOutput.GetReconBeadVoxelSize().x;
                    outputs.reconBeadVoxelSizeY = beadEvaluationOutput.GetReconBeadVoxelSize().y;
                    outputs.reconBeadVoxelSizeZ = beadEvaluationOutput.GetReconBeadVoxelSize().z;

                    outputs.deconBeadData = beadEvaluationOutput.GetDeconBeadData();
                    outputs.deconBeadSizeX = beadEvaluationOutput.GetDeconBeadDataSize().x;
                    outputs.deconBeadSizeY = beadEvaluationOutput.GetDeconBeadDataSize().y;
                    outputs.deconBeadSizeZ = beadEvaluationOutput.GetDeconBeadDataSize().z;
                    outputs.deconBeadVoxelSizeX = beadEvaluationOutput.GetDeconBeadVoxelSize().x;
                    outputs.deconBeadVoxelSizeY = beadEvaluationOutput.GetDeconBeadVoxelSize().y;
                    outputs.deconBeadVoxelSizeZ = beadEvaluationOutput.GetDeconBeadVoxelSize().z;

                    outputs.simulatedBeadData = beadEvaluationOutput.GetSimulatedBeadData();
                    outputs.simulatedBeadSizeX = beadEvaluationOutput.GetSimulatedBeadDataSize().x;
                    outputs.simulatedBeadSizeY = beadEvaluationOutput.GetSimulatedBeadDataSize().y;
                    outputs.simulatedBeadSizeZ = beadEvaluationOutput.GetSimulatedBeadDataSize().z;
                    outputs.simulatedBeadVoxelSizeX = beadEvaluationOutput.GetSimulatedBeadVoxelSize().x;
                    outputs.simulatedBeadVoxelSizeY = beadEvaluationOutput.GetSimulatedBeadVoxelSize().y;
                    outputs.simulatedBeadVoxelSizeZ = beadEvaluationOutput.GetSimulatedBeadVoxelSize().z;
                    OutputWriter outputWriter;
                    outputWriter.SetOutput(outputs);
                    outputWriter.SetOutputFilePath(outputFilePath);
                    outputWriter.SetTempFolderPath(scanningFolderPath);

                    if (!outputWriter.Write()) {
                        QLOG_ERROR() << "outputWriter.Write() fails";
                        evaluationFail = true;
                    }
                }
            } catch(std::exception& exception) {
                QLOG_ERROR() << exception.what();
                evaluationFail = true;
            }

        } 
        if (evaluationFail) {
            const auto errorFilePath = scanningFolderPath + "/error";

            std::ofstream errorFile(errorFilePath.toStdString(), std::ios::binary);
            errorFile.write("error", 5);
            errorFile.close();
        }

        QThread::sleep(scanningTimeInSec);

        const QString finishFilePath = QString("%1/finish").arg(scanningFolderPath);

        if (QFile::exists(finishFilePath)) {
            QLOG_INFO() << "Program Closing";
            QFile::remove(finishFilePath);
            break;
        }
    }

    return 0;
}
