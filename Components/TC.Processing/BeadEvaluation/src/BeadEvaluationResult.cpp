#include "BeadEvaluationResult.h"

namespace TC::Processing::BeadEvaluation {
    class BeadEvaluationResult::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        double volume{};
        double dryMass{};
        double correlation{};
        double meanDeltaRI{};

        QString reconBeadFilePath{};
        QString deconBeadFilePath{};
        QString simulatedBeadFilePath{};
    };

    BeadEvaluationResult::BeadEvaluationResult() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluationResult::BeadEvaluationResult(const BeadEvaluationResult& other)
        : d(std::make_unique<Impl>(*other.d)){
    }

    BeadEvaluationResult::~BeadEvaluationResult() = default;

    auto BeadEvaluationResult::operator=(const BeadEvaluationResult& other) -> BeadEvaluationResult& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto BeadEvaluationResult::SetVolume(const double& volume) -> void {
        d->volume = volume;
    }

    auto BeadEvaluationResult::GetVolume() const -> const double& {
        return d->volume;
    }

    auto BeadEvaluationResult::SetDryMass(const double& dryMass) -> void {
        d->dryMass = dryMass;
    }

    auto BeadEvaluationResult::GetDryMass() const -> const double& {
        return d->dryMass;
    }

    auto BeadEvaluationResult::SetCorrelation(const double& correlation) -> void {
        d->correlation = correlation;
    }

    auto BeadEvaluationResult::GetCorrelation() const -> const double& {
        return d->correlation;
    }

    auto BeadEvaluationResult::SetMeanDeltaRI(const double& meanDeltaRI) -> void {
        d->meanDeltaRI = meanDeltaRI;
    }

    auto BeadEvaluationResult::GetMeanDeltaRI() const -> const double& {
        return d->meanDeltaRI;
    }

    auto BeadEvaluationResult::SetReconBeadFilePath(const QString& reconBeadFilePath) -> void {
        d->reconBeadFilePath = reconBeadFilePath;
    }

    auto BeadEvaluationResult::GetReconBeadFilePath() const -> const QString& {
        return d->reconBeadFilePath;
    }

    auto BeadEvaluationResult::SetDeconBeadFilePath(const QString& deconBeadFilePath) -> void {
        d->deconBeadFilePath = deconBeadFilePath;
    }

    auto BeadEvaluationResult::GetDeconBeadFilePath() const -> const QString& {
        return d->deconBeadFilePath;
    }

    auto BeadEvaluationResult::SetSimulatedBeadFilePath(const QString& simulatedBeadFilePath) -> void {
        d->simulatedBeadFilePath = simulatedBeadFilePath;
    }

    auto BeadEvaluationResult::GetSimulatedBeadFilePath() const -> const QString& {
        return d->simulatedBeadFilePath;
    }
}
