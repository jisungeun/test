#include "AcquisitionConfig.h"

namespace TC::Processing::BeadEvaluation {
    class AcquisitionConfig::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        JobInfo jobInfo{};
        AcquisitionCount acquisitionCount{};
        AcquisitionSetting acquisitionSetting{};
        AcquisitionPosition acquisitionPosition{};
        DeviceInfo deviceInfo{};
        ImageInfo imageInfo{};
        TileInfo tileInfo{};

        bool jobInfoValid{ false };
        bool acquisitionCountValid{ false };
        bool acquisitionSettingValid{ false };
        bool acquisitionPositionValid{ false };
        bool deviceInfoValid{ false };
        bool imageInfoValid{ false };
        bool tileInfoValid{ false };
    };

    AcquisitionConfig::AcquisitionConfig() : d(std::make_unique<Impl>()) {
    }

    AcquisitionConfig::AcquisitionConfig(const AcquisitionConfig& other) : d(std::make_unique<Impl>(*other.d)) {
    }

    AcquisitionConfig::~AcquisitionConfig() = default;

    auto AcquisitionConfig::operator=(const AcquisitionConfig& other) -> AcquisitionConfig& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto AcquisitionConfig::SetJobInfo(const JobInfo& jobInfo) -> void {
        d->jobInfo = jobInfo;
        d->jobInfoValid = true;
    }

    auto AcquisitionConfig::GetJobInfo() const -> const JobInfo& {
        return d->jobInfo;
    }

    auto AcquisitionConfig::IsJobInfoValid() const -> const bool& {
        return d->jobInfoValid;
    }

    auto AcquisitionConfig::SetAcquisitionCount(const AcquisitionCount& acquisitionCount) -> void {
        d->acquisitionCount = acquisitionCount;
        d->acquisitionCountValid = true;
    }

    auto AcquisitionConfig::GetAcquisitionCount() const -> const AcquisitionCount& {
        return d->acquisitionCount;
    }

    auto AcquisitionConfig::IsAcquisitionCountValid() const -> const bool& {
        return d->acquisitionCountValid;
    }

    auto AcquisitionConfig::SetAcquisitionSetting(const AcquisitionSetting& acquisitionSetting) -> void {
        d->acquisitionSetting = acquisitionSetting;
        d->acquisitionSettingValid = true;
    }

    auto AcquisitionConfig::GetAcquisitionSetting() const -> const AcquisitionSetting& {
        return d->acquisitionSetting;
    }

    auto AcquisitionConfig::IsAcquisitionSettingValid() const -> const bool& {
        return d->acquisitionSettingValid;
    }

    auto AcquisitionConfig::SetAcquisitionPosition(const AcquisitionPosition& acquisitionPosition) -> void {
        d->acquisitionPosition = acquisitionPosition;
        d->acquisitionPositionValid = true;
    }

    auto AcquisitionConfig::GetAcquisitionPosition() const -> const AcquisitionPosition& {
        return d->acquisitionPosition;
    }

    auto AcquisitionConfig::IsAcquisitionPositionValid() const -> const bool& {
        return d->acquisitionPositionValid;
    }

    auto AcquisitionConfig::SetDeviceInfo(const DeviceInfo& deviceInfo) -> void {
        d->deviceInfo = deviceInfo;
        d->deviceInfoValid = true;
    }

    auto AcquisitionConfig::GetDeviceInfo() const -> const DeviceInfo& {
        return d->deviceInfo;
    }

    auto AcquisitionConfig::IsDeviceInfoValid() const -> const bool& {
        return d->deviceInfoValid;
    }

    auto AcquisitionConfig::SetImageInfo(const ImageInfo& imageInfo) -> void {
        d->imageInfo = imageInfo;
        d->imageInfoValid = true;
    }

    auto AcquisitionConfig::GetImageInfo() const -> const ImageInfo& {
        return d->imageInfo;
    }

    auto AcquisitionConfig::IsImageInfoValid() const -> const bool& {
        return d->imageInfoValid;
    }

    auto AcquisitionConfig::SetTileInfo(const TileInfo& tileInfo) -> void {
        d->tileInfo = tileInfo;
        d->tileInfoValid = true;
    }

    auto AcquisitionConfig::GetTileInfo() const -> const TileInfo& {
        return d->tileInfo;
    }

    auto AcquisitionConfig::IsTileInfoValid() const -> const bool& {
        return d->tileInfoValid;
    }
}