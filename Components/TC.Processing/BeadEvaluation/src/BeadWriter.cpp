#include "BeadWriter.h"

#include <QSettings>
#include <fstream>

namespace TC::Processing::BeadEvaluation {
    class BeadWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString filePath{};
        std::shared_ptr<float[]> data{};
        int32_t dataSizeX{};
        int32_t dataSizeY{};
        int32_t dataSizeZ{};

        double voxelSizeX{};
        double voxelSizeY{};
        double voxelSizeZ{};
    };

    BeadWriter::BeadWriter() : d(std::make_unique<Impl>()) {
    }

    BeadWriter::~BeadWriter() = default;

    auto BeadWriter::SetFilePath(const QString& filePath) -> void {
        d->filePath = filePath;
    }

    auto BeadWriter::SetData(const std::shared_ptr<float[]>& data) -> void {
        d->data = data;
    }

    auto BeadWriter::SetDataSize(const int32_t& x, const int32_t& y, const int32_t& z) -> void {
        d->dataSizeX = x;
        d->dataSizeY = y;
        d->dataSizeZ = z;
    }

    auto BeadWriter::SetVoxelSize(const double& x, const double& y, const double& z) -> void {
        d->voxelSizeX = x;
        d->voxelSizeY = y;
        d->voxelSizeZ = z;
    }

    auto BeadWriter::Write() -> bool {
        const auto rawFilePath = QString("%1.raw").arg(d->filePath);

        std::ofstream fileStream{ rawFilePath.toStdString(), std::ios::binary };
        const auto byteCount = d->dataSizeX * d->dataSizeY * d->dataSizeZ * static_cast<int32_t>(sizeof(float));
        fileStream.write(reinterpret_cast<const char*>(d->data.get()), byteCount);
        fileStream.close();
        
        QSettings beadMetaFile(d->filePath, QSettings::IniFormat);
        beadMetaFile.setValue("rawFilePath", rawFilePath);

        beadMetaFile.setValue("dataSizeX", QString::number(d->dataSizeX));
        beadMetaFile.setValue("dataSizeY", QString::number(d->dataSizeY));
        beadMetaFile.setValue("dataSizeZ", QString::number(d->dataSizeZ));

        beadMetaFile.setValue("voxelSizeX", QString::number(d->voxelSizeX));
        beadMetaFile.setValue("voxelSizeY", QString::number(d->voxelSizeY));
        beadMetaFile.setValue("voxelSizeZ", QString::number(d->voxelSizeZ));

        return true;
    }
}
