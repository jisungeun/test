#include "BeadEvaluationInput.h"

namespace TC::Processing::BeadEvaluation {
    class BeadEvaluationInput::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        QString rootFolderPath{};
        QString psfFolderPath{};
        QString psfModuleFilePath{};
        QString beadModuleFilePath{};

        int32_t beadCenterPositionXInPixel{};
        int32_t beadCenterPositionYInPixel{};

        QString outputFolderPath{};
    };

    BeadEvaluationInput::BeadEvaluationInput() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluationInput::BeadEvaluationInput(const BeadEvaluationInput& other) : d(std::make_unique<Impl>(*other.d)) {
    }

    BeadEvaluationInput::~BeadEvaluationInput() = default;

    auto BeadEvaluationInput::operator=(const BeadEvaluationInput& other) -> BeadEvaluationInput& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto BeadEvaluationInput::SetRootFolderPath(const QString& rootFolderPath) -> void {
        d->rootFolderPath = rootFolderPath;
    }

    auto BeadEvaluationInput::GetRootFolderPath() const -> const QString& {
        return d->rootFolderPath;
    }

    auto BeadEvaluationInput::SetPSFFolderPath(const QString& psfFolderPath) -> void {
        d->psfFolderPath = psfFolderPath;
    }

    auto BeadEvaluationInput::GetPSFFolderPath() const -> const QString& {
        return d->psfFolderPath;
    }

    auto BeadEvaluationInput::SetPSFModuleFilePath(const QString& psfModuleFilePath) -> void {
        d->psfModuleFilePath = psfModuleFilePath;
    }

    auto BeadEvaluationInput::GetPSFModuleFilePath() const -> const QString& {
        return d->psfModuleFilePath;
    }

    auto BeadEvaluationInput::SetBeadModuleFilePath(const QString& beadModuleFilePath) -> void {
        d->beadModuleFilePath = beadModuleFilePath;
    }

    auto BeadEvaluationInput::GetBeadModuleFilePath() const -> const QString& {
        return d->beadModuleFilePath;
    }

    auto BeadEvaluationInput::SetBeadCenterPosition(const int32_t& positionXInPixel,
        const int32_t& positionYInPixel) -> void {
        d->beadCenterPositionXInPixel = positionXInPixel;
        d->beadCenterPositionYInPixel = positionYInPixel;
    }

    auto BeadEvaluationInput::GetBeadCenterPositionXInPixel() const -> const int32_t& {
        return d->beadCenterPositionXInPixel;
    }

    auto BeadEvaluationInput::GetBeadCenterPositionYInPixel() const -> const int32_t& {
        return d->beadCenterPositionYInPixel;
    }

    auto BeadEvaluationInput::SetOutputFolderPath(const QString& outputFolderPath) -> void {
        d->outputFolderPath = outputFolderPath;
    }

    auto BeadEvaluationInput::GetOutputFolderPath() const -> const QString& {
        return d->outputFolderPath;
    }
}
