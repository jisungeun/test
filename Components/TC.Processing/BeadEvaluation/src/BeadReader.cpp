#include "BeadReader.h"

#include <QFile>
#include <QSettings>
#include <fstream>

namespace TC::Processing::BeadEvaluation {
    class BeadReader::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString beadFilePath{};

        std::shared_ptr<float[]> beadData{};
        int32_t beadDataSizeX{};
        int32_t beadDataSizeY{};
        int32_t beadDataSizeZ{};
        
        double beadVoxelSizeX{};
        double beadVoxelSizeY{};
        double beadVoxelSizeZ{};
    };

    BeadReader::BeadReader() : d(std::make_unique<Impl>()) {
    }

    BeadReader::~BeadReader() = default;

    auto BeadReader::SetBeadFilePath(const QString& beadFilePath) -> void {
        d->beadFilePath = beadFilePath;
    }

    auto BeadReader::Read() -> bool {
        if (!QFile::exists(d->beadFilePath)) {
            return false;
        }
        
        const QSettings beadMetaFile(d->beadFilePath, QSettings::IniFormat);

        bool sizeXOk{ false }, sizeYOk{ false }, sizeZOk{ false };
        d->beadDataSizeX = beadMetaFile.value("dataSizeX").toString().toInt(&sizeXOk);
        d->beadDataSizeY = beadMetaFile.value("dataSizeY").toString().toInt(&sizeYOk);
        d->beadDataSizeZ = beadMetaFile.value("dataSizeZ").toString().toInt(&sizeZOk);

        bool voxelSizeXOk{ false }, voxelSizeYOk{ false }, voxelSizeZOk{ false };
        d->beadVoxelSizeX = beadMetaFile.value("voxelSizeX").toString().toDouble(&voxelSizeXOk);
        d->beadVoxelSizeY = beadMetaFile.value("voxelSizeY").toString().toDouble(&voxelSizeYOk);
        d->beadVoxelSizeZ = beadMetaFile.value("voxelSizeZ").toString().toDouble(&voxelSizeZOk);

        if (!(sizeXOk && sizeYOk && sizeZOk && voxelSizeXOk && voxelSizeYOk & voxelSizeZOk)) {
            return false;
        }

        const auto rawFilePath = beadMetaFile.value("rawFilePath").toString();

        if (!QFile::exists(rawFilePath)) {
            return false;
        }

        const auto numberOfElements = d->beadDataSizeX * d->beadDataSizeY * d->beadDataSizeZ;
        const std::shared_ptr<float[]> beadData{ new float[numberOfElements] };

        std::ifstream fileStream{ rawFilePath.toStdString(), std::ios::binary };
        const auto byteCount = numberOfElements * static_cast<int32_t>(sizeof(float));
        fileStream.read(reinterpret_cast<char*>(beadData.get()), byteCount);
        fileStream.close();

        d->beadData = beadData;

        return true;
    }

    auto BeadReader::GetBeadData() const -> std::shared_ptr<float[]> {
        return d->beadData;
    }

    auto BeadReader::GetBeadDataSizeX() const -> int32_t {
        return d->beadDataSizeX;
    }

    auto BeadReader::GetBeadDataSizeY() const -> int32_t {
        return d->beadDataSizeY;
    }

    auto BeadReader::GetBeadDataSizeZ() const -> int32_t {
        return d->beadDataSizeZ;
    }

    auto BeadReader::GetBeadVoxelSizeX() const -> double {
        return d->beadVoxelSizeX;
    }

    auto BeadReader::GetBeadVoxelSizeY() const -> double {
        return d->beadVoxelSizeY;
    }

    auto BeadReader::GetBeadVoxelSizeZ() const -> double {
        return d->beadVoxelSizeZ;
    }
}
