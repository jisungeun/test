#include "BeadEvaluationMatlabApp.h"

#include <QFile>
#include <QThread>

#include "AcquisitionConfigReader.h"
#include "InputWriter.h"
#include "OutputReader.h"
#include "Outputs.h"
#include "ProcessHandler.h"
#include "BeadWriter.h"

#include "TCLogger.h"

constexpr auto scanningPeriodInSec = 1;
constexpr auto retryThreshold = 5;

namespace TC::Processing::BeadEvaluation {
    using namespace BeadEvaluationMatlabAppComponent;

    class BeadEvaluationMatlabApp::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        BeadEvaluationInput input{};

        QString processingAppFilePath{};
        QString processingTempFolderPath{};

        BeadEvaluationResult result{};
    };

    BeadEvaluationMatlabApp::BeadEvaluationMatlabApp() : d(std::make_unique<Impl>()) {
    }

    BeadEvaluationMatlabApp::~BeadEvaluationMatlabApp() = default;

    auto BeadEvaluationMatlabApp::SetInput(const BeadEvaluationInput& input) -> void {
        d->input = input;
    }

    auto BeadEvaluationMatlabApp::SetProcessingAppFilePath(const QString& processingAppFilePath) -> void {
        d->processingAppFilePath = processingAppFilePath;
    }

    auto BeadEvaluationMatlabApp::SetProcessingTempFolderPath(const QString& processingTempFolderPath) -> void {
        d->processingTempFolderPath = processingTempFolderPath;
    }

    auto BeadEvaluationMatlabApp::Evaluate() -> bool {
        const auto rootFolderPath = d->input.GetRootFolderPath();

        const auto configFilePath = QString("%1/config.dat").arg(rootFolderPath);
        AcquisitionConfigReader configReader;
        configReader.SetFilePath(configFilePath);
        if (!configReader.Read()) {
            return false;
        }

        const auto config = configReader.GetAcquisitionConfig();

        constexpr double beadCropSizeXInMicrometer = 56.25;
        constexpr double beadCropSizeYInMicrometer = 56.25;

        const auto pixelSize = config.GetDeviceInfo().pixelSizeMicrometer;
        const auto magnification = config.GetDeviceInfo().magnification;
        const auto voxelSize = pixelSize / magnification;

        Inputs componentInput;
        componentInput.beadModuleFilePath = d->input.GetBeadModuleFilePath();
        componentInput.psfModuleFilePath = d->input.GetPSFModuleFilePath();
        componentInput.sampleFolderPath = QString("%1/data/0000/HT3D/0000").arg(rootFolderPath);
        componentInput.backgroundFolderPath = QString("%1/bgImages").arg(rootFolderPath);;
        componentInput.psfFolderPath = d->input.GetPSFFolderPath();
        componentInput.mediumRI = config.GetDeviceInfo().mediumRI;
        componentInput.naCond = config.GetDeviceInfo().condenserNA;
        componentInput.naObj = config.GetDeviceInfo().objectiveNA;
        componentInput.magnification = magnification;
        componentInput.pixelSize = pixelSize;
        componentInput.zStepLength = config.GetAcquisitionSetting().htZStepLengthMicrometer;
        componentInput.sampleCropOffsetX = 0;
        componentInput.sampleCropOffsetY = 0;
        componentInput.backgroundCropOffsetX = config.GetImageInfo().roiOffsetXPixels;
        componentInput.backgroundCropOffsetY = config.GetImageInfo().roiOffsetYPixels;
        componentInput.imageSizeX = config.GetImageInfo().roiSizeXPixels;
        componentInput.imageSizeY = config.GetImageInfo().roiSizeYPixels;
        componentInput.beadCenterPositionXInMicrometer = static_cast<double>(d->input.GetBeadCenterPositionXInPixel()) * voxelSize;
        componentInput.beadCenterPositionYInMicrometer = static_cast<double>(d->input.GetBeadCenterPositionYInPixel()) * voxelSize;
        componentInput.beadCropSizeXInMicrometer = beadCropSizeXInMicrometer;
        componentInput.beadCropSizeYInMicrometer = beadCropSizeYInMicrometer;

        const QString inputFilePath = QString("%1/inputFile").arg(d->processingTempFolderPath);

        InputWriter inputWriter;
        inputWriter.SetWritingFilePath(inputFilePath);
        inputWriter.SetInputs(componentInput);
        if (!inputWriter.Write()) {
            return false;
        }

        const QString outputFilePath = QString("%1/outputFile").arg(d->processingTempFolderPath);
        const QString outputDoneFilePath = QString("%1/outputFile_done").arg(d->processingTempFolderPath);
        const QString errorFilePath = QString("%1/error").arg(d->processingTempFolderPath);

        if (QFile::exists(outputFilePath)) { QFile::remove(outputFilePath); }
        if (QFile::exists(outputDoneFilePath)) { QFile::remove(outputDoneFilePath); }
        if (QFile::exists(errorFilePath)) { QFile::remove(errorFilePath); }

        auto processHandler = ProcessHandler::GetInstance();
        if (!ProcessHandler::GetInstance()->IsStarted()) {
            processHandler->ForceClose();
            processHandler->SetAppPath(d->processingAppFilePath);
            processHandler->SetTempFolderPath(d->processingTempFolderPath);
            processHandler->Start();
        }

        // scanning output file
        Outputs componentOutput;

        auto retryCount = 0;
        while (true) {
            if (QFile::exists(errorFilePath)) {
                QFile::remove(errorFilePath);
                processHandler->ForceClose();
                return false;
            }

            if (QFile::exists(outputDoneFilePath)) {
                OutputReader outputReader;
                outputReader.SetOutputFilePath(outputFilePath);

                if (!outputReader.Read()) {
                    return false;
                }

                QFile::remove(outputDoneFilePath);

                componentOutput = outputReader.GetOutput();
                break;
            }
            QThread::sleep(scanningPeriodInSec);

            if (!processHandler->IsRunning()) {
                retryCount++;
                processHandler->ForceClose();
                if (retryCount > retryThreshold) {
                    processHandler->ForceClose();
                    QLOG_ERROR() << "retry count is over 5";
                    return false;
                }
                QLOG_INFO() << "retry (" << retryCount << ")";
                InputWriter inputWriter;
                inputWriter.SetWritingFilePath(inputFilePath);
                inputWriter.SetInputs(componentInput);
                if (!inputWriter.Write()) {
                    return false;
                }

                processHandler->SetTempFolderPath(d->processingTempFolderPath);
                processHandler->Start();
            }
        }

        const auto deconBeadFilePath = QString("%1/deconBead").arg(d->input.GetOutputFolderPath());
        const auto reconBeadFilePath = QString("%1/reconBead").arg(d->input.GetOutputFolderPath());
        const auto simulatedBeadFilePath = QString("%1/simulatedBead").arg(d->input.GetOutputFolderPath());
        
        {
            BeadWriter writer;
            writer.SetFilePath(deconBeadFilePath);
            writer.SetData(componentOutput.deconBeadData);
            writer.SetDataSize(componentOutput.deconBeadSizeX, componentOutput.deconBeadSizeY, componentOutput.deconBeadSizeZ);
            writer.SetVoxelSize(componentOutput.deconBeadVoxelSizeX, componentOutput.deconBeadVoxelSizeY, componentOutput.deconBeadVoxelSizeZ);
            writer.Write();
        }
        {
            BeadWriter writer;
            writer.SetFilePath(reconBeadFilePath);
            writer.SetData(componentOutput.reconBeadData);
            writer.SetDataSize(componentOutput.reconBeadSizeX, componentOutput.reconBeadSizeY, componentOutput.reconBeadSizeZ);
            writer.SetVoxelSize(componentOutput.reconBeadVoxelSizeX, componentOutput.reconBeadVoxelSizeY, componentOutput.reconBeadVoxelSizeZ);
            writer.Write();
        }
        {
            BeadWriter writer;
            writer.SetFilePath(simulatedBeadFilePath);
            writer.SetData(componentOutput.simulatedBeadData);
            writer.SetDataSize(componentOutput.simulatedBeadSizeX, componentOutput.simulatedBeadSizeY, componentOutput.simulatedBeadSizeZ);
            writer.SetVoxelSize(componentOutput.simulatedBeadVoxelSizeX, componentOutput.simulatedBeadVoxelSizeY, componentOutput.simulatedBeadVoxelSizeZ);
            writer.Write();
        }
        

        //get results
        BeadEvaluationResult result;
        result.SetVolume(componentOutput.volume);
        result.SetDryMass(componentOutput.dryMass);
        result.SetCorrelation(componentOutput.correlation);
        result.SetMeanDeltaRI(componentOutput.meanDeltaRI);
        result.SetDeconBeadFilePath(deconBeadFilePath);
        result.SetReconBeadFilePath(reconBeadFilePath);
        result.SetSimulatedBeadFilePath(simulatedBeadFilePath);

        d->result = result;

        return true;
    }

    auto BeadEvaluationMatlabApp::GetResult() const -> BeadEvaluationResult {
        return d->result;
    }
}
