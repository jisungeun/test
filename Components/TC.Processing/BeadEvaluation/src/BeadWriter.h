#pragma once

#include <memory>
#include <QString>

namespace TC::Processing::BeadEvaluation {
    class BeadWriter {
    public:
        BeadWriter();
        ~BeadWriter();

        auto SetFilePath(const QString& filePath)->void;
        auto SetData(const std::shared_ptr<float[]>& data)->void;
        auto SetDataSize(const int32_t& x, const int32_t& y, const int32_t& z)->void;
        auto SetVoxelSize(const double& x, const double& y, const double& z)->void;

        auto Write()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}