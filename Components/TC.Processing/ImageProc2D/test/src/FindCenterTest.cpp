#include <catch2/catch.hpp>
#include <opencv2/highgui.hpp>

#include <Image2DProc.h>

namespace TC::Processing::OpenCVLearningTest {
    const QString datapath_01 = QString("%1/crossmark01.png").arg(_TEST_DATA);

    TEST_CASE("Find center") {
        QImage input;
        input.load(datapath_01);

        auto [xpos, ypos] = Image2DProc::FindCenterOfCross(input, false);

        CHECK(xpos == 982);
        CHECK(ypos == 758);
    }
}