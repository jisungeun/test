#include <catch2/catch.hpp>
#include <opencv2/highgui.hpp>

#include <Image2DProc.h>

namespace TC::Processing::CalcUniformityCVTest {
    const QString datapath_01 = QString("%1/intensity01.png").arg(_TEST_DATA);

    TEST_CASE("Unifomity CV") {
        QImage input;
        input.load(datapath_01);

        auto cv = Image2DProc::CalcUnifomityCV(input, 10, 10);

        CHECK(Approx(cv) == 0.5716);
    }
}