#include <catch2/catch.hpp>

#include <memory>

#include <QString>
#include <QImage>
#include <QDebug>

#include <opencv2/highgui.hpp>

#include <Image2DProc.h>

namespace TC::Processing::OpenCVLearningTest {
    const QString filePath = QString("%1/cells01.png").arg(_TEST_DATA);
        auto ImageCleanup(void* imageInfo) -> void {
            delete []static_cast<unsigned char*>(imageInfo);
        }

    TEST_CASE("Mat"){
        auto grayscale_img = cv::imread(filePath.toStdString(), cv::IMREAD_GRAYSCALE);

        uint8_t threshold = 250;

        cv::Mat mask = grayscale_img > threshold;

        (void)mask;
    }

    TEST_CASE("QImage data memory release") {
        auto cvImg = cv::imread(filePath.toStdString(), cv::IMREAD_GRAYSCALE);

        auto bytes = static_cast<unsigned>(cvImg.total() * cvImg.elemSize());
        auto pData = new uchar[bytes];
        auto pData2 = new uchar[bytes];

        memcpy_s(pData, bytes, cvImg.data, bytes);
        memcpy_s(pData2, bytes, cvImg.data, bytes);

        qDebug() << "pDataADDR" << pData;
        qDebug() << "pData2 ADDR1:" << pData2;

        auto cols = cvImg.cols;
        auto rows = cvImg.rows;
        auto step = static_cast<int>(cvImg.step);

        QImage *pQImg = new QImage(pData, cols, rows, step, QImage::Format_RGB888, ImageCleanup, pData);

        CHECK(pData != nullptr);
        CHECK(pData2 != nullptr);

        pQImg = new QImage(pData2, cols, rows, step, QImage::Format_RGB888, ImageCleanup, pData2);
        CHECK(pData != nullptr);
        CHECK(pData2 != nullptr);
    }
}