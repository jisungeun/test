#include <QDebug>

#include "Utility.h"

namespace TC::Processing::Image2DProc {
    auto ImageCleanup(void* imageInfo) -> void {
        delete []static_cast<unsigned char*>(imageInfo);
    }

    auto QImageToCvMat(QImage& inImage, bool inCloneImageData) -> cv::Mat {
        switch (inImage.format()) {
                // 8-bit, 4 channel
            case QImage::Format_RGB32: {
                cv::Mat mat(inImage.height(), inImage.width(), CV_8UC4, (inImage.bits()), inImage.bytesPerLine());
                return (inCloneImageData ? mat.clone() : mat);
            }

                // 8-bit, 3 channel`
            case QImage::Format_RGB888: {
                if (!inCloneImageData) {
                    qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";
                }

                QImage swapped = inImage.rgbSwapped();
                return cv::Mat(swapped.height(), swapped.width(), CV_8UC3, swapped.bits(), swapped.bytesPerLine()).clone();
            }

                // 8-bit, 1 channel
            case QImage::Format_Indexed8: {
                cv::Mat mat(inImage.height(), inImage.width(), CV_8UC1, (inImage.bits()), inImage.bytesPerLine());
                return (inCloneImageData ? mat.clone() : mat);
            }

            case QImage::Format_Grayscale8: {
                cv::Mat mat(inImage.height(), inImage.width(), CV_8UC1, (inImage.bits()), inImage.bytesPerLine());
                return (inCloneImageData ? mat.clone() : mat);
            }

            default: qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
                break;
        }

        return cv::Mat();
    }

    auto Create(const cv::Mat& cvImg) -> std::shared_ptr<QImage> {
        std::shared_ptr<QImage> p;

        auto bytes = static_cast<unsigned>(cvImg.total() * cvImg.elemSize());
        auto pData = new uchar[bytes];
        memcpy_s(pData, bytes, cvImg.data, bytes);

        auto cols = cvImg.cols;
        auto rows = cvImg.rows;
        auto step = static_cast<int>(cvImg.step);

        switch (cvImg.type()) {
            case CV_8UC4: {
                p.reset(new QImage(pData, cols, rows, step, QImage::Format_ARGB32, ImageCleanup, pData));
                break;
            }
            case CV_8UC3: {
                p.reset(new QImage(pData, cols, rows, step, QImage::Format_RGB888, ImageCleanup, pData));
                break;
            }
            case CV_8UC1: {
                p.reset(new QImage(cols, rows, QImage::Format_Grayscale8));

                auto* data = p->bits();
                const int step = p->bytesPerLine();
                for (int y = 0; y < rows; ++y) {
                    const auto* const scanLine = cvImg.ptr<uint8_t>(y);
                    for (int x = 0; x < cols; ++x) {
                        data[y * step + x] = scanLine[x];
                    }
                }
                break;
            }
            default:
                //TODO error handling...
                break;
        }

        return p;
    }
}