#include <QDebug>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "Utility.h"
#include "Image2DProc.h"

namespace TC::Processing::Image2DProc {
    auto CalcUnifomityCV(QImage& image, int32_t cols, int32_t rows)->double {
        auto cvImg = QImageToCvMat(image);

        const auto deltaCol = cvImg.cols / cols;
        const auto deltaRow = cvImg.rows / rows;

        QList<double> blockAverages;
        double sumOfAverages = 0;

        for(auto row=0; row<rows; row++) {
            for(auto col=0; col<cols; col++) {
                auto colRange = cv::Range(deltaCol*col, std::min(cvImg.cols-1, deltaCol*(col+1)-1));
                auto rowRange = cv::Range(deltaRow*row, std::min(cvImg.rows-1, deltaRow*(row+1)-1));

                auto roiImg = cvImg(rowRange, colRange);
                auto average = cv::mean(roiImg)[0];

                blockAverages.push_back(average);
                sumOfAverages += average;
            }
        }

        auto mean = sumOfAverages / (cols * rows);
        auto standardDeviation = 0.0;

        if(mean == 0) return -1;

        for(auto value : blockAverages) {
            standardDeviation += std::pow(value - mean, 2);
        }

        return std::sqrt(standardDeviation / (cols * rows)) / mean;
    }
}