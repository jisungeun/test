﻿#include <QDebug>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/types_c.h>

#include "Utility.h"
#include "Image2DProc.h"

namespace TC::Processing::Image2DProc {
    /// <summary>
    /// maxth 값 보다 큰 픽셀에 대해 빨간색 표시 -> oversaturation\n
    /// minth 값 보다 작은 픽셀에 대해 파랑색 표시 -> undersaturation
    /// </summary>
    /// <param name="srcImg">source image</param>
    /// <param name="outImg">saturation 적용 후 image</param>
    /// <param name="minth">undersaturation minimum threshold, 사용하지 않을 경우 0보다 작은 값 할당</param>
    /// <param name="maxth">oversaturation maximum threshold, 사용하지 않을 경우 255보다 큰 값 할당</param>
    /// <returns>
    /// 이미지에 saturation 없으면 true 반환
    /// otherwise false 반환
    /// </returns>
    auto ColorizeOutOfRange(std::shared_ptr<QImage>& srcImg, std::shared_ptr<QImage>& outImg, int minth, int maxth) -> bool {
        bool bGood = true;

        cv::Mat img = QImageToCvMat(*(srcImg.get()));
        cv::Mat dst;
        cv::Mat mask;

        const auto depth = [=]()-> int {
            auto cv_depth = img.depth();
            return 8 * ((cv_depth / 2) + 1);
        }();
        const double maxPixel = std::pow(2, depth) - 1;
        const auto bitDepth = srcImg->depth();

        if (bitDepth == 8) cvtColor(img, dst, CV_GRAY2RGB);
        else {
            const auto range = std::pow(2, bitDepth) - 1;
            img = img - std::pow(2, depth - bitDepth);
            img.convertTo(dst, CV_8U, 1.0 / range);
        }

        // oversaturation
        // 조건 만족 시 해당 픽셀 빨강색으로 처리
        if (maxth <= maxPixel) {
            inRange(img, cv::Scalar(maxth), cv::Scalar(maxPixel), mask);
            dst.setTo(cv::Scalar(255, 0, 0), mask);
            bGood &= (countNonZero(mask) == 0);
        }

        // undersaturation, 0보다 작은 값일 경우 무시
        // 조건 만족 시 해당 픽셀 파랑색으로 처리
        if (minth >= 0) {
            inRange(img, cv::Scalar(0), cv::Scalar(minth), mask);
            dst.setTo(cv::Scalar(0, 0, 255), mask);
            bGood &= (countNonZero(mask) == 0);
        }

        outImg.swap(Create(dst));

        return bGood;
    }

    auto CalculateAverage(QImage& image) -> double {
        cv::Mat img = QImageToCvMat(image);
        return cv::mean(img)[0];
    }

    auto CalculateAverage(const std::shared_ptr<uint8_t[]>& image, int32_t cols, int32_t rows) -> double {
        cv::Mat mat(rows, cols, CV_8UC1, image.get(), cols);
        return cv::mean(mat)[0];
    }

    auto Convert(const cv::Mat& cvImg) -> std::shared_ptr<QImage> {
        return Create(cvImg);
    }
}
