#include <QDebug>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "Utility.h"
#include "Image2DProc.h"

namespace TC::Processing::Image2DProc {
    auto FindCenterOfCross(QImage& image, bool isWhite, double ratio)->std::tuple<double,double> {
        auto cvImg = QImageToCvMat(image);

        auto type = (isWhite) ? (CV_THRESH_BINARY | CV_THRESH_OTSU) : (CV_THRESH_BINARY_INV | CV_THRESH_OTSU);

        cv::Mat mask;
        cv::threshold(cvImg, mask, 0, 255, type);

        auto findPeak = [](cv::Mat& data, bool isHorizontal, double ratio)->int32_t {
            int32_t first = -1;
            int32_t second = -1;

            const auto dataCount = isHorizontal ? data.cols : data.rows;
            const auto pixelCount = isHorizontal ? data.rows : data.cols;
            const auto threshold = static_cast<int32_t>(pixelCount * ratio);

            for(int i=0; i<dataCount; i++) {
                const auto count = isHorizontal ? cv::countNonZero(data(cv::Rect(i, 0, 1, pixelCount))) :
                                                  cv::countNonZero(data(cv::Rect(0, i, pixelCount, 1)));
                if(first == -1) {
                    if(count > threshold) first = i;
                } else if(second == -1) {
                    if(count < threshold) second = i;
                } else {
                    break;
                }
            }

            if((first < 0) || (second < 0)) return -1;

            return (first + second) / 2;
        };
        
        auto centerX = findPeak(mask, true, ratio);
        auto centerY = findPeak(mask, false, ratio);

        return std::make_tuple(centerX, centerY);
    }
}