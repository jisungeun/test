#pragma once
#include <memory>

#include <QImage>

#include <opencv2/core/mat.hpp>

namespace TC::Processing::Image2DProc {
    auto ImageCleanup(void* imageInfo) -> void;
    auto QImageToCvMat(QImage& inImage, bool inCloneImageData = false) -> cv::Mat;
    auto Create(const cv::Mat& cvImg) -> std::shared_ptr<QImage>;
}

