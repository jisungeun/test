﻿#pragma once

#include <memory>

#include <QImage>

#include <opencv2/core/mat.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>

#include "TCImageProc2DExport.h"

namespace TC::Processing::Image2DProc {
    TCImageProc2D_API auto ColorizeOutOfRange(std::shared_ptr<QImage>& srcImg, std::shared_ptr<QImage>& outImg, int minth, int maxth) -> bool;
    TCImageProc2D_API auto CalculateAverage(QImage& image)->double;
    TCImageProc2D_API auto CalculateAverage(const std::shared_ptr<uint8_t[]>& image, int32_t cols, int32_t rows)->double;
    TCImageProc2D_API auto FindCenterOfCross(QImage& image, bool isWhite = true, double ratio = 0.5)->std::tuple<double,double>;
    TCImageProc2D_API auto CalcUnifomityCV(QImage& image, int32_t cols=1, int32_t rows=1)->double;
    TCImageProc2D_API auto Convert(const cv::Mat& cvImg)->std::shared_ptr<QImage>;
}

