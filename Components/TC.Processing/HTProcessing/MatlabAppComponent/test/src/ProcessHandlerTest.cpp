#include <catch2/catch.hpp>

#include "ProcessHandler.h"

namespace ProcessHandlerTest {
    TEST_CASE("ProcessHandler : practical test") {
        const QString appPath = "D:/Work/ProjectBuild/HTX_Release/bin/Release/TCHTProcessingMatlabApp.exe";
        const QString tempFolderPath = "C:/Temp/scanningTest";

        auto instance = ProcessHandler::GetInstance();

        instance->SetAppPath(appPath);
        instance->SetTempFolderPath(tempFolderPath);

        instance->Start();
        CHECK(instance->IsStarted() == true);
    }
}