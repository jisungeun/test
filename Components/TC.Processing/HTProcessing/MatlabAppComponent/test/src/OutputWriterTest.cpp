#include <catch2/catch.hpp>

#include <fstream>
#include <QSettings>

#include "OutputWriter.h"
#include "CompareArray.h"

namespace OutputWriterTest {
    TEST_CASE("OutputWriter : practical test") {
        const QString outputFilePath = "outputWriterTest";
        const QString tempFolderPath = "C:/Temp";

        constexpr auto sizeX = 2;
        constexpr auto sizeY = 2;
        constexpr auto sizeZ = 3;
        constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

        constexpr float resolutionX = 0.1f;
        constexpr float resolutionY = 0.2f;
        constexpr float resolutionZ = 0.3f;

        const float rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

        std::shared_ptr<float[]> tomogram{ new float[numberOfElements]() };
        std::copy_n(rawData, numberOfElements, tomogram.get());
        
        Outputs outputs;
        outputs.sizeX = sizeX;
        outputs.sizeY = sizeY;
        outputs.sizeZ = sizeZ;
        outputs.resolutionX = resolutionX;
        outputs.resolutionY = resolutionY;
        outputs.resolutionZ = resolutionZ;
        outputs.tomogram = tomogram;
        
        OutputWriter outputWriter;
        outputWriter.SetTempFolderPath(tempFolderPath);
        outputWriter.SetOutputFilePath(outputFilePath);
        outputWriter.SetOutput(outputs);

        CHECK(outputWriter.Write());

        const auto globalFileIndex = outputWriter.GetGlobalFileIndex();

        const auto tomogramFilePath = QString("%1/%2").arg(tempFolderPath).arg(globalFileIndex - 1);

        const std::shared_ptr<float[]> resultTomogram{ new float[sizeX * sizeY * sizeZ]() };

        std::ifstream fileStream{ tomogramFilePath.toStdString(), std::ios::binary };
        const auto byteCount = sizeX * sizeY * sizeZ * static_cast<int32_t>(sizeof(float));
        fileStream.read(reinterpret_cast<char*>(resultTomogram.get()), byteCount);
        fileStream.close();

        QSettings outputFile(outputFilePath, QSettings::IniFormat);
        CHECK(sizeX == outputFile.value("sizeX").toInt());
        CHECK(sizeY == outputFile.value("sizeY").toInt());
        CHECK(sizeZ == outputFile.value("sizeZ").toInt());
        CHECK(resolutionX == outputFile.value("resolutionX").toFloat());
        CHECK(resolutionY == outputFile.value("resolutionY").toFloat());
        CHECK(resolutionZ == outputFile.value("resolutionZ").toFloat());
        CHECK(tomogramFilePath == outputFile.value("tomogramFilePath").toString());

        CHECK(CompareArray(resultTomogram.get(), tomogram.get(), numberOfElements));
    }
}