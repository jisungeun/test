#include <catch2/catch.hpp>
#include <QFile>
#include "CompareArray.h"
#include "OutputWriter.h"
#include "OutputReader.h"

namespace OutputReaderTest {
    TEST_CASE("OutputReader : practical test") {
        const QString outputFilePath = "outputReaderTest";
        const QString tempFolderPath = "C:/Temp";

        constexpr auto sizeX = 2;
        constexpr auto sizeY = 2;
        constexpr auto sizeZ = 3;
        constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

        constexpr float resolutionX = 0.1f;
        constexpr float resolutionY = 0.2f;
        constexpr float resolutionZ = 0.3f;

        const float rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

        std::shared_ptr<float[]> tomogram{ new float[numberOfElements]() };
        std::copy_n(rawData, numberOfElements, tomogram.get());

        Outputs outputs;
        outputs.sizeX = sizeX;
        outputs.sizeY = sizeY;
        outputs.sizeZ = sizeZ;
        outputs.resolutionX = resolutionX;
        outputs.resolutionY = resolutionY;
        outputs.resolutionZ = resolutionZ;
        outputs.tomogram = tomogram;

        OutputWriter outputWriter;
        outputWriter.SetTempFolderPath(tempFolderPath);
        outputWriter.SetOutputFilePath(outputFilePath);
        outputWriter.SetOutput(outputs);

        outputWriter.Write();

        const auto tomogramFilePath = QString("%1/%2").arg(tempFolderPath).arg(outputWriter.GetGlobalFileIndex());

        OutputReader outputReader;
        outputReader.SetOutputFilePath(outputFilePath);
        CHECK(outputReader.Read());
        const auto resultOutput = outputReader.GetOutput();

        CHECK(resultOutput.sizeX == sizeX);
        CHECK(resultOutput.sizeY == sizeY);
        CHECK(resultOutput.sizeZ == sizeZ);
        CHECK(resultOutput.resolutionX == resolutionX);
        CHECK(resultOutput.resolutionY == resolutionY);
        CHECK(resultOutput.resolutionZ == resolutionZ);
        CHECK(CompareArray(resultOutput.tomogram.get(), tomogram.get(), numberOfElements));
        CHECK(!QFile::exists(outputFilePath));
        CHECK(!QFile::exists(tomogramFilePath));
    }
}