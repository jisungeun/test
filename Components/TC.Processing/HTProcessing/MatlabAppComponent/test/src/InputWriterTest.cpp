#include <QSettings>
#include <catch2/catch.hpp>

#include "InputWriter.h"

namespace InputWriterTest {
    TEST_CASE("InputWriter : unit test") {
        const QString writingFilePath = "inputWriter.ini";

        const QString htProcessingModuleFilePath = "1";
        const QString psfModuleFilePath = "2";
        const QString htProcessingProfileFilePath = "3";
        const QString psfProfileFilePath = "4";
        const QString sampleImageFolderPath = "5";
        const QString backgroundImageFolderPath = "6";
        const QString psfFolderPath = "7";
        const int32_t sampleDataCropStartIndexX = 8;
        const int32_t sampleDataCropStartIndexY = 9;
        const int32_t backgroundDataCropStartIndexX = 10;
        const int32_t backgroundDataCropStartIndexY = 11;
        const int32_t cropSizeX = 12;
        const int32_t cropSizeY = 13;
        const float mediumRi = 14.f;
        const float objectiveNA = 15.f;
        const float condenserNA = 16.f;
        const float voxelSizeXY = 17.f;
        const float voxelSizeZ = 18.f;

        Inputs inputs;
        inputs.htProcessingModuleFilePath = htProcessingModuleFilePath;
        inputs.psfModuleFilePath = psfModuleFilePath;
        inputs.htProcessingProfileFilePath = htProcessingProfileFilePath;
        inputs.psfProfileFilePath = psfProfileFilePath;
        inputs.sampleImageFolderPath = sampleImageFolderPath;
        inputs.backgroundImageFolderPath = backgroundImageFolderPath;
        inputs.psfFolderPath = psfFolderPath;
        inputs.sampleDataCropStartIndexX = sampleDataCropStartIndexX;
        inputs.sampleDataCropStartIndexY = sampleDataCropStartIndexY;
        inputs.backgroundDataCropStartIndexX = backgroundDataCropStartIndexX;
        inputs.backgroundDataCropStartIndexY = backgroundDataCropStartIndexY;
        inputs.cropSizeX = cropSizeX;
        inputs.cropSizeY = cropSizeY;
        inputs.mediumRi = mediumRi;
        inputs.objectiveNA = objectiveNA;
        inputs.condenserNA = condenserNA;
        inputs.voxelSizeXY = voxelSizeXY;
        inputs.voxelSizeZ = voxelSizeZ;

        InputWriter inputWriter;
        inputWriter.SetInputs(inputs);
        inputWriter.SetWritingFilePath(writingFilePath);

        CHECK(inputWriter.Write());

        QSettings inputFile(writingFilePath, QSettings::IniFormat);
        CHECK(htProcessingModuleFilePath == inputFile.value("htProcessingModuleFilePath").toString());
        CHECK(psfModuleFilePath == inputFile.value("psfModuleFilePath").toString());
        CHECK(htProcessingProfileFilePath == inputFile.value("htProcessingProfileFilePath").toString());
        CHECK(psfProfileFilePath == inputFile.value("psfProfileFilePath").toString());
        CHECK(sampleImageFolderPath == inputFile.value("sampleImageFolderPath").toString());
        CHECK(backgroundImageFolderPath == inputFile.value("backgroundImageFolderPath").toString());
        CHECK(psfFolderPath == inputFile.value("psfFolderPath").toString());

        CHECK(sampleDataCropStartIndexX == inputFile.value("sampleDataCropStartIndexX").toInt());
        CHECK(sampleDataCropStartIndexY == inputFile.value("sampleDataCropStartIndexY").toInt());

        CHECK(backgroundDataCropStartIndexX == inputFile.value("backgroundDataCropStartIndexX").toInt());
        CHECK(backgroundDataCropStartIndexY == inputFile.value("backgroundDataCropStartIndexY").toInt());

        CHECK(cropSizeX == inputFile.value("cropSizeX").toInt());
        CHECK(cropSizeY == inputFile.value("cropSizeY").toInt());
        CHECK(mediumRi == inputFile.value("mediumRi").toFloat());
        CHECK(objectiveNA == inputFile.value("objectiveNA").toFloat());
        CHECK(condenserNA == inputFile.value("condenserNA").toFloat());
        CHECK(voxelSizeXY == inputFile.value("voxelSizeXY").toFloat());
        CHECK(voxelSizeZ == inputFile.value("voxelSizeZ").toFloat());
    }
}