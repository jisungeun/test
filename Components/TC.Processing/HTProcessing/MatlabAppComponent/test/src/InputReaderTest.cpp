#include <catch2/catch.hpp>

#include <QFile>
#include "InputReader.h"
#include "InputWriter.h"

namespace InputReaderTest {
    TEST_CASE("InputReader : practical test") {
        const QString writingFilePath = "inputReader.ini";

        const QString htProcessingModuleFilePath = "1";
        const QString psfModuleFilePath = "2";
        const QString htProcessingProfileFilePath = "3";
        const QString psfProfileFilePath = "4";
        const QString sampleImageFolderPath = "5";
        const QString backgroundImageFolderPath = "6";
        const QString psfFolderPath = "7";
        const int32_t sampleDataCropStartIndexX = 8;
        const int32_t sampleDataCropStartIndexY = 9;
        const int32_t backgroundDataCropStartIndexX = 10;
        const int32_t backgroundDataCropStartIndexY = 11;
        const int32_t cropSizeX = 12;
        const int32_t cropSizeY = 13;
        const float mediumRi = 14.f;
        const float objectiveNA = 15.f;
        const float condenserNA = 16.f;
        const float voxelSizeXY = 17.f;
        const float voxelSizeZ = 18.f;

        Inputs inputs;
        inputs.htProcessingModuleFilePath = htProcessingModuleFilePath;
        inputs.psfModuleFilePath = psfModuleFilePath;
        inputs.htProcessingProfileFilePath = htProcessingProfileFilePath;
        inputs.psfProfileFilePath = psfProfileFilePath;
        inputs.sampleImageFolderPath = sampleImageFolderPath;
        inputs.backgroundImageFolderPath = backgroundImageFolderPath;
        inputs.psfFolderPath = psfFolderPath;
        inputs.sampleDataCropStartIndexX = sampleDataCropStartIndexX;
        inputs.sampleDataCropStartIndexY = sampleDataCropStartIndexY;
        inputs.backgroundDataCropStartIndexX = backgroundDataCropStartIndexX;
        inputs.backgroundDataCropStartIndexY = backgroundDataCropStartIndexY;
        inputs.cropSizeX = cropSizeX;
        inputs.cropSizeY = cropSizeY;
        inputs.mediumRi = mediumRi;
        inputs.objectiveNA = objectiveNA;
        inputs.condenserNA = condenserNA;
        inputs.voxelSizeXY = voxelSizeXY;
        inputs.voxelSizeZ = voxelSizeZ;

        InputWriter inputWriter;
        inputWriter.SetInputs(inputs);
        inputWriter.SetWritingFilePath(writingFilePath);

        inputWriter.Write();

        InputReader inputReader;
        inputReader.SetInputFilePath(writingFilePath);
        CHECK(inputReader.Read());

        const auto resultInput = inputReader.GetInput();

        CHECK(resultInput.htProcessingModuleFilePath == htProcessingModuleFilePath);
        CHECK(resultInput.psfModuleFilePath == psfModuleFilePath);
        CHECK(resultInput.htProcessingProfileFilePath == htProcessingProfileFilePath);
        CHECK(resultInput.psfProfileFilePath == psfProfileFilePath);
        CHECK(resultInput.sampleImageFolderPath == sampleImageFolderPath);
        CHECK(resultInput.backgroundImageFolderPath == backgroundImageFolderPath);
        CHECK(resultInput.psfFolderPath == psfFolderPath);
        CHECK(resultInput.sampleDataCropStartIndexX == sampleDataCropStartIndexX);
        CHECK(resultInput.sampleDataCropStartIndexY == sampleDataCropStartIndexY);
        CHECK(resultInput.backgroundDataCropStartIndexX == backgroundDataCropStartIndexX);
        CHECK(resultInput.backgroundDataCropStartIndexY == backgroundDataCropStartIndexY);
        CHECK(resultInput.cropSizeX == cropSizeX);
        CHECK(resultInput.cropSizeY == cropSizeY);
        CHECK(resultInput.mediumRi == mediumRi);
        CHECK(resultInput.objectiveNA == objectiveNA);
        CHECK(resultInput.condenserNA == condenserNA);
        CHECK(resultInput.voxelSizeXY == voxelSizeXY);
        CHECK(resultInput.voxelSizeZ == voxelSizeZ);

        CHECK(!QFile::exists(writingFilePath));
    }
}