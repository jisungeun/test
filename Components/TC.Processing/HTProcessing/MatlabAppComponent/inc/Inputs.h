#pragma once

#include <QString>

#include "TCHTProcessingMatlabAppComponentExport.h"

struct TCHTProcessingMatlabAppComponent_API Inputs {
    QString htProcessingModuleFilePath{};
    QString psfModuleFilePath{};

    QString htProcessingProfileFilePath{};
    QString psfProfileFilePath{};

    QString sampleImageFolderPath{};
    QString backgroundImageFolderPath{};
    QString psfFolderPath{};

    int32_t sampleDataCropStartIndexX{};
    int32_t sampleDataCropStartIndexY{};

    int32_t backgroundDataCropStartIndexX{};
    int32_t backgroundDataCropStartIndexY{};

    int32_t cropSizeX{};
    int32_t cropSizeY{};

    float mediumRi{};
    float objectiveNA{};
    float condenserNA{};
    float voxelSizeXY{};
    float voxelSizeZ{};
};

