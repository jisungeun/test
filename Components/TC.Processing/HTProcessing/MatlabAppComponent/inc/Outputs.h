#pragma once

#include <QString>

#include "TCHTProcessingMatlabAppComponentExport.h"

struct TCHTProcessingMatlabAppComponent_API Outputs {
    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};
    float resolutionX{};
    float resolutionY{};
    float resolutionZ{};
    std::shared_ptr<float[]> tomogram{};
};