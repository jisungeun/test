#pragma once

#include <memory>

#include "TCHTProcessingMatlabAppComponentExport.h"

#include "Inputs.h"

class TCHTProcessingMatlabAppComponent_API InputReader {
public:
    InputReader();
    ~InputReader();

    auto SetInputFilePath(const QString& inputFilePath)->void;
    auto Read()->bool;

    auto GetInput()const->const Inputs&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};