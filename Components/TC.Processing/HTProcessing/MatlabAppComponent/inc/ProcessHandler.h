#pragma once

#include <memory>
#include <QString>
#include "TCHTProcessingMatlabAppComponentExport.h"

class TCHTProcessingMatlabAppComponent_API ProcessHandler {
public:
    ~ProcessHandler();

    static auto GetInstance()->std::shared_ptr<ProcessHandler>;

    auto IsStarted()->bool;
    auto SetAppPath(const QString& appPath)->void;
    auto SetTempFolderPath(const QString& tempFolderPath)->void;
    auto Start()->void;
    auto ForceClose()->void;
    auto IsRunning()->bool;

protected:
    ProcessHandler();
private:
    class Impl;
    std::unique_ptr<Impl> d;
};