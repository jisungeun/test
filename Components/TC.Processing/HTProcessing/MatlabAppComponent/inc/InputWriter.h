#pragma once

#include <memory>
#include <QString>

#include "Inputs.h"

#include "TCHTProcessingMatlabAppComponentExport.h"

class TCHTProcessingMatlabAppComponent_API InputWriter {
public:
    InputWriter();
    ~InputWriter();

    auto SetWritingFilePath(const QString& writingFilePath)->void;
    auto SetInputs(const Inputs& inputs)->void;
    auto Write()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};