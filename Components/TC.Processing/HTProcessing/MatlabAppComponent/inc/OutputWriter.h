#pragma once

#include <memory>
#include <QString>

#include "TCHTProcessingMatlabAppComponentExport.h"
#include "Outputs.h"

class TCHTProcessingMatlabAppComponent_API OutputWriter {
public:
    OutputWriter();
    ~OutputWriter();

    auto SetTempFolderPath(const QString& tempFolderPath)->void;
    auto SetOutputFilePath(const QString& outputFilePath)->void;
    auto SetOutput(const Outputs& outputs)->void;

    auto Write()->bool;
    auto GetGlobalFileIndex()const->const int32_t&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};