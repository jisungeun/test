#include "OutputReader.h"

#include <fstream>
#include <QFile>
#include <QSettings>

class OutputReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString outputFilePath{};
    Outputs outputs;
};

OutputReader::OutputReader() : d(new Impl()) {
}

OutputReader::~OutputReader() = default;

auto OutputReader::SetOutputFilePath(const QString& outputFilePath) -> void {
    d->outputFilePath = outputFilePath;
}

auto OutputReader::Read() -> bool {
    if (!QFile::exists(d->outputFilePath)) {
        return false;
    }

    QSettings outputFile(d->outputFilePath, QSettings::IniFormat);
    const auto sizeX = outputFile.value("sizeX").toInt();
    const auto sizeY = outputFile.value("sizeY").toInt();
    const auto sizeZ = outputFile.value("sizeZ").toInt();
    const auto resolutionX = outputFile.value("resolutionX").toFloat();
    const auto resolutionY = outputFile.value("resolutionY").toFloat();
    const auto resolutionZ = outputFile.value("resolutionZ").toFloat();
    const auto tomogramFilePath = outputFile.value("tomogramFilePath").toString();

    const std::shared_ptr<float[]> tomogram{ new float[sizeX * sizeY * sizeZ]() };

    std::ifstream fileStream{ tomogramFilePath.toStdString(), std::ios::binary };
    const auto byteCount = sizeX * sizeY * sizeZ * static_cast<int32_t>(sizeof(float));
    fileStream.read(reinterpret_cast<char*>(tomogram.get()), byteCount);
    fileStream.close();

    QFile::remove(d->outputFilePath);
    QFile::remove(tomogramFilePath);

    Outputs outputs;
    outputs.sizeX = sizeX;
    outputs.sizeY = sizeY;
    outputs.sizeZ = sizeZ;
    outputs.resolutionX = resolutionX;
    outputs.resolutionY = resolutionY;
    outputs.resolutionZ = resolutionZ;
    outputs.tomogram = tomogram;

    d->outputs = outputs;

    return true;
}

auto OutputReader::GetOutput() const -> const Outputs& {
    return d->outputs;
}
