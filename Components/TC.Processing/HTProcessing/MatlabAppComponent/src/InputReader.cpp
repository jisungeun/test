#include "InputReader.h"

#include <QFile>
#include <QSettings>

class InputReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString inputFilePath{};
    Inputs readInput{};
};

InputReader::InputReader() : d(new Impl()) {
}

InputReader::~InputReader() = default;

auto InputReader::SetInputFilePath(const QString & inputFilePath) -> void {
    d->inputFilePath = inputFilePath;
}

auto InputReader::Read() -> bool {
    QSettings inputFile(d->inputFilePath, QSettings::IniFormat);
    const auto htProcessingModuleFilePath = inputFile.value("htProcessingModuleFilePath").toString();
    const auto psfModuleFilePath = inputFile.value("psfModuleFilePath").toString();
    const auto htProcessingProfileFilePath = inputFile.value("htProcessingProfileFilePath").toString();
    const auto psfProfileFilePath = inputFile.value("psfProfileFilePath").toString();
    const auto sampleImageFolderPath = inputFile.value("sampleImageFolderPath").toString();
    const auto backgroundImageFolderPath = inputFile.value("backgroundImageFolderPath").toString();
    const auto psfSupportFolderPath = inputFile.value("psfFolderPath").toString();
    const auto sampleDataCropStartIndexX = inputFile.value("sampleDataCropStartIndexX").toInt();
    const auto sampleDataCropStartIndexY = inputFile.value("sampleDataCropStartIndexY").toInt();
    const auto backgroundDataCropStartIndexX = inputFile.value("backgroundDataCropStartIndexX").toInt();
    const auto backgroundDataCropStartIndexY = inputFile.value("backgroundDataCropStartIndexY").toInt();
    const auto cropSizeX = inputFile.value("cropSizeX").toInt();
    const auto cropSizeY = inputFile.value("cropSizeY").toInt();

    const auto mediumRi = inputFile.value("mediumRi").toFloat();
    const auto objectiveNA = inputFile.value("objectiveNA").toFloat();
    const auto condenserNA = inputFile.value("condenserNA").toFloat();
    const auto voxelSizeXY = inputFile.value("voxelSizeXY").toFloat();
    const auto voxelSizeZ = inputFile.value("voxelSizeZ").toFloat();

    QFile::remove(d->inputFilePath);

    Inputs inputs;
    inputs.htProcessingModuleFilePath = htProcessingModuleFilePath;
    inputs.psfModuleFilePath = psfModuleFilePath;
    inputs.htProcessingProfileFilePath = htProcessingProfileFilePath;
    inputs.psfProfileFilePath = psfProfileFilePath;
    inputs.sampleImageFolderPath = sampleImageFolderPath;
    inputs.backgroundImageFolderPath = backgroundImageFolderPath;
    inputs.psfFolderPath = psfSupportFolderPath;
    inputs.sampleDataCropStartIndexX = sampleDataCropStartIndexX;
    inputs.sampleDataCropStartIndexY = sampleDataCropStartIndexY;
    inputs.backgroundDataCropStartIndexX = backgroundDataCropStartIndexX;
    inputs.backgroundDataCropStartIndexY = backgroundDataCropStartIndexY;
    inputs.cropSizeX = cropSizeX;
    inputs.cropSizeY = cropSizeY;
    inputs.mediumRi = mediumRi;
    inputs.objectiveNA = objectiveNA;
    inputs.condenserNA = condenserNA;
    inputs.voxelSizeXY = voxelSizeXY;
    inputs.voxelSizeZ = voxelSizeZ;
    
    d->readInput = inputs;

    return true;
}

auto InputReader::GetInput() const -> const Inputs& {
    return d->readInput;
}