#include "InputWriter.h"

#include <QSettings>

class InputWriter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString writingFilePath{};
    Inputs inputs{};
};

InputWriter::InputWriter() : d(new Impl()) {
}

InputWriter::~InputWriter() = default;

auto InputWriter::SetWritingFilePath(const QString & writingFilePath) -> void {
    d->writingFilePath = writingFilePath;
}

auto InputWriter::SetInputs(const Inputs & inputs) -> void {
    d->inputs = inputs;
}

auto InputWriter::Write() -> bool {
    {
        const auto htProcessingModuleFilePath = d->inputs.htProcessingModuleFilePath;
        const auto psfModuleFilePath = d->inputs.psfModuleFilePath;

        const auto htProcessingProfileFilePath = d->inputs.htProcessingProfileFilePath;
        const auto psfProfileFilePath = d->inputs.psfProfileFilePath;

        const auto sampleImageFolderPath = d->inputs.sampleImageFolderPath;
        const auto backgroundImageFolderPath = d->inputs.backgroundImageFolderPath;
        const auto psfSupportFolderPath = d->inputs.psfFolderPath;

        const auto sampleDataCropStartIndexX = d->inputs.sampleDataCropStartIndexX;
        const auto sampleDataCropStartIndexY = d->inputs.sampleDataCropStartIndexY;

        const auto backgroundDataCropStartIndexX = d->inputs.backgroundDataCropStartIndexX;
        const auto backgroundDataCropStartIndexY = d->inputs.backgroundDataCropStartIndexY;

        const auto cropSizeX = d->inputs.cropSizeX;
        const auto cropSizeY = d->inputs.cropSizeY;

        const auto mediumRi = d->inputs.mediumRi;
        const auto objectiveNA = d->inputs.objectiveNA;
        const auto condenserNA = d->inputs.condenserNA;
        const auto voxelSizeXY = d->inputs.voxelSizeXY;
        const auto voxelSizeZ = d->inputs.voxelSizeZ;
        
        QSettings inputFile(d->writingFilePath, QSettings::IniFormat);
        inputFile.setValue("htProcessingModuleFilePath", htProcessingModuleFilePath);
        inputFile.setValue("psfModuleFilePath", psfModuleFilePath);
        inputFile.setValue("htProcessingProfileFilePath", htProcessingProfileFilePath);
        inputFile.setValue("psfProfileFilePath", psfProfileFilePath);

        inputFile.setValue("sampleImageFolderPath", sampleImageFolderPath);
        inputFile.setValue("backgroundImageFolderPath", backgroundImageFolderPath);
        inputFile.setValue("psfFolderPath", psfSupportFolderPath);

        inputFile.setValue("sampleDataCropStartIndexX", sampleDataCropStartIndexX);
        inputFile.setValue("sampleDataCropStartIndexY", sampleDataCropStartIndexY);
        inputFile.setValue("backgroundDataCropStartIndexX", backgroundDataCropStartIndexX);
        inputFile.setValue("backgroundDataCropStartIndexY", backgroundDataCropStartIndexY);

        inputFile.setValue("cropSizeX", cropSizeX);
        inputFile.setValue("cropSizeY", cropSizeY);

        inputFile.setValue("mediumRi", mediumRi);
        inputFile.setValue("objectiveNA", objectiveNA);
        inputFile.setValue("condenserNA", condenserNA);
        inputFile.setValue("voxelSizeXY", voxelSizeXY);
        inputFile.setValue("voxelSizeZ", voxelSizeZ);
    }
    {
        QSettings inputWritingDoneFile(d->writingFilePath + "_done", QSettings::IniFormat);
        inputWritingDoneFile.setValue("flag", "done");
    }

    return true;
}
