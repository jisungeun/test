#include "OutputWriter.h"

#include <fstream>
#include <QSettings>

static int32_t globalDataIndex = 0;

class OutputWriter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString tempFolderPath{};
    QString outputFilePath{};
    Outputs outputs{};
};

OutputWriter::OutputWriter() : d(new Impl()) {
}

OutputWriter::~OutputWriter() = default;

auto OutputWriter::SetTempFolderPath(const QString& tempFolderPath) -> void {
    d->tempFolderPath = tempFolderPath;
}

auto OutputWriter::SetOutputFilePath(const QString& outputFilePath) -> void {
    d->outputFilePath = outputFilePath;
}

auto OutputWriter::SetOutput(const Outputs& outputs) -> void {
    d->outputs = outputs;
}

auto OutputWriter::Write() -> bool {
    {
        const auto tomogram = d->outputs.tomogram;
        const auto sizeX = d->outputs.sizeX;
        const auto sizeY = d->outputs.sizeY;
        const auto sizeZ = d->outputs.sizeZ;

        const auto resolutionX = d->outputs.resolutionX;
        const auto resolutionY = d->outputs.resolutionY;
        const auto resolutionZ = d->outputs.resolutionZ;

        const QString tomogramFilePath = QString("%1/%2").arg(d->tempFolderPath).arg(globalDataIndex);

        QSettings outputFile(d->outputFilePath, QSettings::IniFormat);
        outputFile.setValue("sizeX", sizeX);
        outputFile.setValue("sizeY", sizeY);
        outputFile.setValue("sizeZ", sizeZ);
        outputFile.setValue("resolutionX", resolutionX);
        outputFile.setValue("resolutionY", resolutionY);
        outputFile.setValue("resolutionZ", resolutionZ);
        outputFile.setValue("tomogramFilePath", tomogramFilePath);

        std::ofstream fileStream{ tomogramFilePath.toStdString(), std::ios::binary };
        const auto byteCount = sizeX * sizeY * sizeZ * static_cast<int32_t>(sizeof(float));
        fileStream.write(reinterpret_cast<const char*>(tomogram.get()), byteCount);
        fileStream.close();
    }
    {
        QSettings outputFile(d->outputFilePath+"_done", QSettings::IniFormat);
        outputFile.setValue("e", "e");
    }
    globalDataIndex++;
    return true;
}

auto OutputWriter::GetGlobalFileIndex() const -> const int32_t& {
    return globalDataIndex;
}
