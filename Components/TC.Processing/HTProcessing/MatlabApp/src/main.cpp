#define LOGGER_TAG "[main]"

#include <fstream>
#include <iostream>

#include <QThread>
#include <QDir>
#include <QStandardPaths>

#include <QString>

#include <AlgorithmVersionReader.h>

#include <HTProcessingProfileReader_v1_4_1_c.h>
#include <HTProcessingProfileReader_v1_4_1_d.h>
#include <PSFProfileReader_v1_4_1_c.h>

#include <Processor_v1_4_1_c.h>
#include <Processor_v1_4_1_d.h>

#include <PSFProfileFactory.h>
#include <HTProcessingProfileFactory.h>

#include "HTProcessingAlgorithmVersion.h"
#include "IHTProcessingMatlabOutputPort.h"
#include "HTProcessingMatlabInput.h"

#include "InputReader.h"
#include "OutputWriter.h"
#include "HTProcessingMatlabOutputPort.h"
#include "PSFAlgorithmVersion.h"
#include "TCLogger.h"

using namespace TC::HTProcessingMatlab;

auto ToHTProcessingMatlabInput(const Inputs& inputs)->HTProcessingMatlabInput {
    PathInfo pathInfo;
    pathInfo.sampleImageFolderPath = inputs.sampleImageFolderPath;
    pathInfo.backgroundImageFolderPath = inputs.backgroundImageFolderPath;
    pathInfo.psfFolderPath = inputs.psfFolderPath;

    ImageCropInfo imageCropInfo;
    imageCropInfo.sampleCropStartIndexX = inputs.sampleDataCropStartIndexX;
    imageCropInfo.sampleCropStartIndexY = inputs.sampleDataCropStartIndexY;

    imageCropInfo.backgroundCropStartIndexX = inputs.backgroundDataCropStartIndexX;
    imageCropInfo.backgroundCropStartIndexY = inputs.backgroundDataCropStartIndexY;

    imageCropInfo.cropSizeX = inputs.cropSizeX;
    imageCropInfo.cropSizeY = inputs.cropSizeY;

    AcquisitionInfo acquisitionInfo;
    acquisitionInfo.mediumRi = inputs.mediumRi;
    acquisitionInfo.objectiveNA = inputs.objectiveNA;
    acquisitionInfo.condenserNA = inputs.condenserNA;
    acquisitionInfo.voxelSizeXY = inputs.voxelSizeXY;
    acquisitionInfo.voxelSizeZ = inputs.voxelSizeZ;

    MatlabModuleFilePath matlabModuleFilePath;
    matlabModuleFilePath.htProcessingFilePath = inputs.htProcessingModuleFilePath;
    matlabModuleFilePath.psfModuleFilePath = inputs.psfModuleFilePath;

    HTProcessingMatlabInput input;
    input.SetPathInfo(pathInfo);
    input.SetImageCropInfo(imageCropInfo);
    input.SetAcquisitionInfo(acquisitionInfo);
    input.SetMatlabModuleFilePath(matlabModuleFilePath);

    return input;
}

auto LogInput(const Inputs& inputs)->void {
    QLOG_INFO() << "---inputs---";
    QLOG_INFO() << "htProcessingModuleFilePath : " << inputs.htProcessingModuleFilePath;
    QLOG_INFO() << "psfModuleFilePath : " << inputs.psfModuleFilePath;
    QLOG_INFO() << "htProcessingProfileFilePath : " << inputs.psfProfileFilePath;
    QLOG_INFO() << "psfProfileFilePath : " << inputs.psfProfileFilePath;
    QLOG_INFO() << "sampleImageFolderPath : " << inputs.sampleImageFolderPath;
    QLOG_INFO() << "backgroundImageFolderPath : " << inputs.backgroundImageFolderPath;
    QLOG_INFO() << "psfFolderPath : " << inputs.psfFolderPath;
    QLOG_INFO() << "sampleCrop : X(" << inputs.sampleDataCropStartIndexX << "~" << inputs.sampleDataCropStartIndexX + inputs.cropSizeX - 1 << "),Y(" << inputs.sampleDataCropStartIndexY << "~" << inputs.sampleDataCropStartIndexY + inputs.cropSizeY - 1 << ")";
    QLOG_INFO() << "backgroundCrop : X(" << inputs.backgroundDataCropStartIndexX << "~" << inputs.backgroundDataCropStartIndexX + inputs.cropSizeX - 1 << "),Y(" << inputs.backgroundDataCropStartIndexY << "~" << inputs.backgroundDataCropStartIndexY + inputs.cropSizeY - 1 << ")";
    QLOG_INFO() << "mediumRi : " << inputs.mediumRi;
    QLOG_INFO() << "objectiveNA : " << inputs.objectiveNA;
    QLOG_INFO() << "condenserNA : " << inputs.condenserNA;
    QLOG_INFO() << "voxelSizeXY : " << inputs.voxelSizeXY;
    QLOG_INFO() << "voxelSizeZ : " << inputs.voxelSizeZ;
    QLOG_INFO() << "-----------";
}

auto ToPSFAlgorithmVersion(const QString& versionString)->PSFAlgorithmVersion {
    if (versionString == "1.4.1c") {
        return PSFAlgorithmVersion::v1_4_1_c;
    } 

    return PSFAlgorithmVersion::none;
}

auto ToHTAlgorithmVersion(const QString& versionString)->HTProcessingAlgorithmVersion {
    if (versionString == "1.4.1c") {
        return HTProcessingAlgorithmVersion::v1_4_1_c;
    } else if (versionString == "1.4.1d") {
        return HTProcessingAlgorithmVersion::v1_4_1_d;
    }

    return HTProcessingAlgorithmVersion::none;
}

constexpr auto scanningTimeInSec = 1;

int main(int argc, char* argv[]){
    const QString scanningFolderPath = QString::fromLocal8Bit(argv[1]);

    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    TC::Logger::Initialize(QString("%1/TomoCube, Inc/TCHTProcessingMatlabApp/TCHTProcessingMatlabApp.log").arg(appDataPath), QString("%1/TomoCube, Inc/TCHTProcessingMatlabApp/TCHTProcessingMatlabAppError.log").arg(appDataPath));

    QLOG_INFO() << "=============started==============";
    QLOG_INFO() << "scanning Folder : " << argv[1];

    while (true) {
        const QString inputFilePath = QString("%1/inputFile").arg(scanningFolderPath);
        const QString inputDoneFilePath = QString("%1/inputFile_Done").arg(scanningFolderPath);

        bool processingFail = false;

        if (QFile::exists(inputDoneFilePath)) {
            try {
                InputReader inputReader;
                inputReader.SetInputFilePath(inputFilePath);
                inputReader.Read();

                QFile::remove(inputDoneFilePath);

                const auto inputs = inputReader.GetInput();
                LogInput(inputs);
                const auto htProcessingMatlabInput = ToHTProcessingMatlabInput(inputs);

                const auto htProcessingMatlabOutputPortPresenter = new HTProcessingMatlabOutputPort;
                const auto htProcessingMatlabOutputPort = IHTProcessingMatlabOutputPort::Pointer{ htProcessingMatlabOutputPortPresenter };

                const auto htProcessingProfileFilePath = inputs.htProcessingProfileFilePath;
                const auto psfProfileFilePath = inputs.psfProfileFilePath;

                HTProcessingAlgorithmVersion htVersion;
                if (htProcessingProfileFilePath.isEmpty()) {
                    htVersion = HTProcessingAlgorithmVersion::v1_4_1_d;
                } else {
                    TC::IO::ProfileIO::AlgorithmVersionReader versionReaderHT;
                    versionReaderHT.SetFilePath(htProcessingProfileFilePath);
                    if (!versionReaderHT.Read()) {
                        QLOG_ERROR() << "versionReaderHT.Read() fails";
                        processingFail = true;
                    }
                    htVersion = ToHTAlgorithmVersion(versionReaderHT.GetVersion());
                }

                PSFAlgorithmVersion psfVersion;
                if (psfProfileFilePath.isEmpty()) {
                    psfVersion = PSFAlgorithmVersion::v1_4_1_c;
                } else {
                    TC::IO::ProfileIO::AlgorithmVersionReader versionReaderPSF;
                    versionReaderPSF.SetFilePath(psfProfileFilePath);
                    if (!versionReaderPSF.Read()) {
                        QLOG_ERROR() << "versionReaderPSF.Read() fails";
                        processingFail = true;
                    }
                    psfVersion = ToPSFAlgorithmVersion(versionReaderPSF.GetVersion());
                }
                
                TC::PSFProfile::PSFProfile_v1_4_1_c psfProfile;
                if (psfVersion == PSFAlgorithmVersion::v1_4_1_c) {
                    QLOG_INFO() << "PSF v1.4.1c";

                    TC::IO::ProfileIO::PSFProfileReader_v1_4_1_c psfProfileReader;
                    psfProfileReader.SetPath(psfProfileFilePath);
                    if (psfProfileReader.Read()) {
                        psfProfile = psfProfileReader.GetProfile(inputs.condenserNA);
                    } else {
                        TC::PSFProfile::PSFProfileFactory::Input_v1_4_1_c factoryInput;
                        factoryInput.naCond = inputs.condenserNA;
                        factoryInput.imageSizeX = inputs.cropSizeX;
                        factoryInput.imageSizeY = inputs.cropSizeY;
                        psfProfile = TC::PSFProfile::PSFProfileFactory::Generate_v1_4_1_c(factoryInput);
                    }
                }
                
                IHTProcessingMatlabProcessor::Pointer processor;
                if (htVersion == HTProcessingAlgorithmVersion::v1_4_1_c) {
                    QLOG_INFO() << "ProcessingAlgorithm v1.4.1c";
                    processor = std::make_shared<Processor_v1_4_1_c>();

                    processor->SetInput(htProcessingMatlabInput);
                    processor->SetOutputPort(htProcessingMatlabOutputPort);

                    TC::IO::ProfileIO::HTProcessingProfileReader_v1_4_1_c htProfileReader;
                    htProfileReader.SetPath(htProcessingProfileFilePath);

                    TC::HTProcessingProfile::HTProcessingProfile_v1_4_1_c htProcessingProfile;
                    if (htProfileReader.Read()) {
                        htProcessingProfile = htProfileReader.GetProfile();
                    } else {
                        TC::HTProcessingProfile::HTProcessingProfileFactory::Input_v1_4_1_c factoryInput;
                        factoryInput.naCond = inputs.condenserNA;
                        htProcessingProfile = TC::HTProcessingProfile::HTProcessingProfileFactory::Generate_v1_4_1_c(factoryInput);
                    }
                    
                    std::dynamic_pointer_cast<Processor_v1_4_1_c>(processor)->SetHTProcessingProfile(htProcessingProfile);
                    std::dynamic_pointer_cast<Processor_v1_4_1_c>(processor)->SetPSFProfile(psfProfile);

                } else if (htVersion == HTProcessingAlgorithmVersion::v1_4_1_d) {
                    QLOG_INFO() << "ProcessingAlgorithm v1.4.1d";
                    processor = std::make_shared<Processor_v1_4_1_d>();

                    processor->SetInput(htProcessingMatlabInput);
                    processor->SetOutputPort(htProcessingMatlabOutputPort);

                    TC::IO::ProfileIO::HTProcessingProfileReader_v1_4_1_d htProfileReader;
                    htProfileReader.SetPath(htProcessingProfileFilePath);

                    TC::HTProcessingProfile::HTProcessingProfile_v1_4_1_d htProcessingProfile;
                    if (htProfileReader.Read()) {
                        htProcessingProfile = htProfileReader.GetProfile(inputs.condenserNA);
                    } else {
                        TC::HTProcessingProfile::HTProcessingProfileFactory::Input_v1_4_1_d factoryInput;
                        factoryInput.enableRegularization = true;
                        factoryInput.naCond = inputs.condenserNA;
                        htProcessingProfile = TC::HTProcessingProfile::HTProcessingProfileFactory::Generate_v1_4_1_d(factoryInput);
                    }

                    std::dynamic_pointer_cast<Processor_v1_4_1_d>(processor)->SetHTProcessingProfile(htProcessingProfile);
                    std::dynamic_pointer_cast<Processor_v1_4_1_d>(processor)->SetPSFProfile(psfProfile);
                }

                //run process
                if (!processor->Process()) {
                    QLOG_ERROR() << "processor.Process() fails";
                    processingFail = true;
                } else {
                    //get results
                    const auto htProcessingMatlabOutput = htProcessingMatlabOutputPortPresenter->GetOutput();
                    const auto outputFilePath = QString("%1/outputFile").arg(scanningFolderPath);

                    Outputs outputs;
                    outputs.sizeX = htProcessingMatlabOutput.GetTomogramSizeX();
                    outputs.sizeY = htProcessingMatlabOutput.GetTomogramSizeY();
                    outputs.sizeZ = htProcessingMatlabOutput.GetTomogramSizeZ();
                    outputs.resolutionX = htProcessingMatlabOutput.GetResolutionX();
                    outputs.resolutionY = htProcessingMatlabOutput.GetResolutionY();
                    outputs.resolutionZ = htProcessingMatlabOutput.GetResolutionZ();
                    outputs.tomogram = htProcessingMatlabOutput.GetTomogram();

                    OutputWriter outputWriter;
                    outputWriter.SetOutput(outputs);
                    outputWriter.SetOutputFilePath(outputFilePath);
                    outputWriter.SetTempFolderPath(scanningFolderPath);

                    if (!outputWriter.Write()) {
                        QLOG_ERROR() << "outputWriter.Write() fails";
                        processingFail = true;
                    }
                }
            } catch(std::exception& exception) {
                QLOG_ERROR() << exception.what();
                processingFail = true;
            }
        }

        if (processingFail) {
            const auto errorFilePath = scanningFolderPath + "/error";

            std::ofstream errorFile(errorFilePath.toStdString(), std::ios::binary);
            errorFile.write("error", 5);
            errorFile.close();
        }

        QThread::sleep(scanningTimeInSec);

        const QString finishFilePath = QString("%1/finish").arg(scanningFolderPath);

        if (QFile::exists(finishFilePath)) {
            QLOG_INFO() << "Program Closing";
            QFile::remove(finishFilePath);
            break;
        }
    }

    return 0;
}
