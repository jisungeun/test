#pragma once

#include <memory>
#include <QString>

class MatlabProcessingParameters_v1_4_1_c {
public:
    MatlabProcessingParameters_v1_4_1_c();
    MatlabProcessingParameters_v1_4_1_c(const MatlabProcessingParameters_v1_4_1_c& other);
    ~MatlabProcessingParameters_v1_4_1_c();

    auto operator=(const MatlabProcessingParameters_v1_4_1_c& other)->MatlabProcessingParameters_v1_4_1_c&;

    auto SetMediumRI(const double& mediumRI)->void;
    auto SetNACond(const double& naCond)->void;
    auto SetVoxelSize(const double& voxelSizeXY, const double& voxelSizeZ)->void;
    auto SetSampleFolderPath(const QString& sampleFolderPath)->void;
    auto SetBackgroundFolderPath(const QString& backgroundFolderPath)->void;
    auto SetCropOffset(const int32_t& sampleCropOffsetX, const int32_t& sampleCropOffsetY, const int32_t& backgroundCropOffsetX, const int32_t& backgroundCropOffsetY)->void;
    auto SetCropSize(const int32_t& cropSizeX, const int32_t& cropSizeY)->void;
    auto SetPSF(const std::shared_ptr<float[]>& psfData, const int32_t& imagRealCount, const int32_t& patternCount, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetSupport(const std::shared_ptr<float[]>& supportData, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetKResXYZ(const double& x, const double& y, const double& z)->void;

    auto GetMediumRI()const->double;
    auto GetNACond()const->double;

    auto GetVoxelSizeXY()const->double;
    auto GetVoxelSizeZ()const->double;

    auto GetSampleFolderPath()const->QString;
    auto GetBackgroundFolderPath()const->QString;

    auto GetSampleCropOffsetX()const->int32_t;
    auto GetSampleCropOffsetY()const->int32_t;
    auto GetBackgroundCropOffsetX()const->int32_t;
    auto GetBackgroundCropOffsetY()const->int32_t;

    auto GetCropSizeX()const->int32_t;
    auto GetCropSizeY()const->int32_t;

    auto GetPSFData()const->std::shared_ptr<float[]>;
    auto GetPSFImagRealCount()const->int32_t;
    auto GetPSFPatternCount()const->int32_t;
    auto GetPSFSizeX()const->int32_t;
    auto GetPSFSizeY()const->int32_t;
    auto GetPSFSizeZ()const->int32_t;

    auto GetSupportData()const->std::shared_ptr<float[]>;
    auto GetSupportSizeX()const->int32_t;
    auto GetSupportSizeY()const->int32_t;
    auto GetSupportSizeZ()const->int32_t;

    auto GetKResX()const->double;
    auto GetKResY()const->double;
    auto GetKResZ()const->double;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};