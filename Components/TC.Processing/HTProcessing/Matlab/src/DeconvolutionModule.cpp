#define LOGGER_TAG "[DeconvolutionModule]"

#include "DeconvolutionModule.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"
#include "TCLogger.h"

namespace TC::HTProcessingMatlab {
    const QString htProcessingFunctionName = "DeconTomogram_SharedLibrary";

    class DeconvolutionModule::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString htProcessingModuleFilePath{};
        DeconvolutionParameters deconvolutionParameters{};

        std::shared_ptr<float[]> tomogram{};
        int32_t tomogramSizeX{};
        int32_t tomogramSizeY{};
        int32_t tomogramSizeZ{};

        double tomogramVoxelSizeX{};
        double tomogramVoxelSizeY{};
        double tomogramVoxelSizeZ{};
    };

    DeconvolutionModule::DeconvolutionModule() : d(new Impl()) {
    }

    DeconvolutionModule::~DeconvolutionModule() = default;

    auto DeconvolutionModule::SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath) -> void {
        d->htProcessingModuleFilePath = htProcessingModuleFilePath;
    }

    auto DeconvolutionModule:: SetDeconvolutionParameters(const DeconvolutionParameters& deconvolutionParameters)
        -> void {
        d->deconvolutionParameters = deconvolutionParameters;
    }

    auto DeconvolutionModule::Deconvolute() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->htProcessingModuleFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto mediumRIValue = d->deconvolutionParameters.GetMediumRI();
            const auto naCondValue = d->deconvolutionParameters.GetNACond();
            const auto voxelSizeXYValue = d->deconvolutionParameters.GetVoxelSizeXY();
            const auto voxelSizeZValue = d->deconvolutionParameters.GetVoxelSizeZ();
            const auto sampleFolderPathValue = d->deconvolutionParameters.GetSampleFolderPath();
            const auto backgroundFolderPathValue = d->deconvolutionParameters.GetBackgroundFolderPath();
            const auto sampleCropOffsetXValue = static_cast<int64_t>(d->deconvolutionParameters.GetSampleCropOffsetX() + 1);
            const auto sampleCropOffsetYValue = static_cast<int64_t>(d->deconvolutionParameters.GetSampleCropOffsetY() + 1);
            const auto backgroundCropOffsetXValue = static_cast<int64_t>(d->deconvolutionParameters.GetBackgroundCropOffsetX() + 1);
            const auto backgroundCropOffsetYValue = static_cast<int64_t>(d->deconvolutionParameters.GetBackgroundCropOffsetY() + 1);
            const auto cropSizeXValue = static_cast<int64_t>(d->deconvolutionParameters.GetCropSizeX());
            const auto cropSizeYValue = static_cast<int64_t>(d->deconvolutionParameters.GetCropSizeY());

            auto psfDimension = matlab::data::ArrayDimensions{};
            psfDimension.push_back(d->deconvolutionParameters.GetPSFImagRealCount());
            psfDimension.push_back(d->deconvolutionParameters.GetPSFPatternCount());
            psfDimension.push_back(d->deconvolutionParameters.GetPSFSizeY());
            psfDimension.push_back(d->deconvolutionParameters.GetPSFSizeX());
            psfDimension.push_back(d->deconvolutionParameters.GetPSFSizeZ());

            auto supportDimension = matlab::data::ArrayDimensions{};
            supportDimension.push_back(d->deconvolutionParameters.GetSupportSizeY());
            supportDimension.push_back(d->deconvolutionParameters.GetSupportSizeX());
            supportDimension.push_back(d->deconvolutionParameters.GetSupportSizeZ());

            const auto kResXValue = d->deconvolutionParameters.GetKResX();
            const auto kResYValue = d->deconvolutionParameters.GetKResY();
            const auto kResZValue = d->deconvolutionParameters.GetKResZ();
            
            matlab::data::ArrayFactory factory;
            auto psfDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(psfDimension));
            auto supportDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(supportDimension));

            std::copy_n(d->deconvolutionParameters.GetPSFData().get(), matlab::data::getNumElements(psfDimension), psfDataBuffer.get());
            std::copy_n(d->deconvolutionParameters.GetSupportData().get(), matlab::data::getNumElements(supportDimension), supportDataBuffer.get());

            const auto mediumRI = MatlabSharedLibrary::ToMatlabInput(mediumRIValue);
            const auto naCond = MatlabSharedLibrary::ToMatlabInput(naCondValue);
            const auto voxelSizeXY = MatlabSharedLibrary::ToMatlabInput(voxelSizeXYValue);
            const auto voxelSizeZ = MatlabSharedLibrary::ToMatlabInput(voxelSizeZValue);
            const auto sampleFolderPath = MatlabSharedLibrary::ToMatlabInput(sampleFolderPathValue);
            const auto backgroundFolderPath = MatlabSharedLibrary::ToMatlabInput(backgroundFolderPathValue);
            const auto sampleCropOffsetX = MatlabSharedLibrary::ToMatlabInput(sampleCropOffsetXValue);
            const auto sampleCropOffsetY = MatlabSharedLibrary::ToMatlabInput(sampleCropOffsetYValue);
            const auto backgroundCropOffsetX = MatlabSharedLibrary::ToMatlabInput(backgroundCropOffsetXValue);
            const auto backgroundCropOffsetY = MatlabSharedLibrary::ToMatlabInput(backgroundCropOffsetYValue);
            const auto cropSizeX = MatlabSharedLibrary::ToMatlabInput(cropSizeXValue);
            const auto cropSizeY = MatlabSharedLibrary::ToMatlabInput(cropSizeYValue);
            const auto psf =
                static_cast<matlab::data::Array>(factory.createArrayFromBuffer(psfDimension, std::move(psfDataBuffer)));
            const auto support =
                static_cast<matlab::data::Array>(factory.createArrayFromBuffer(supportDimension, std::move(supportDataBuffer)));
            const auto kResX = MatlabSharedLibrary::ToMatlabInput(kResXValue);
            const auto kResY = MatlabSharedLibrary::ToMatlabInput(kResYValue);
            const auto kResZ = MatlabSharedLibrary::ToMatlabInput(kResZValue);
                
            const std::vector inputs{ mediumRI, naCond, voxelSizeXY, voxelSizeZ, sampleFolderPath, backgroundFolderPath,
                sampleCropOffsetX, sampleCropOffsetY, backgroundCropOffsetX, backgroundCropOffsetY, cropSizeX, cropSizeY,
                psf, support, kResX, kResY, kResZ};

            constexpr auto numberOfOutputs = 3;

            const auto deconvolutionResultArray =
                matlabLibrary->feval(htProcessingFunctionName.toStdU16String(), numberOfOutputs, inputs);

            auto tomogramResult = static_cast<matlab::data::TypedArray<float>>(deconvolutionResultArray[0]);
            auto tomogramVoxelSizeXYResult = static_cast<matlab::data::TypedArray<double>>(deconvolutionResultArray[1]);
            auto tomogramVoxelSizeZResult = static_cast<matlab::data::TypedArray<double>>(deconvolutionResultArray[2]);

            const auto tomogramDimension = tomogramResult.getDimensions();

            const auto tomogramSizeX = tomogramDimension[1];
            const auto tomogramSizeY = tomogramDimension[0];
            const auto tomogramSizeZ = tomogramDimension[2];

            const auto tomogramVoxelSizeX = tomogramVoxelSizeXYResult[0];
            const auto tomogramVoxelSizeY = tomogramVoxelSizeXYResult[0];
            const auto tomogramVoxelSizeZ = tomogramVoxelSizeZResult[0];

            const auto numberOfElements = tomogramResult.getNumberOfElements();
            const auto tomogramUniquePtr = tomogramResult.release();

            d->tomogram = std::shared_ptr<float[]>{ new float[numberOfElements]() };
            std::copy_n(tomogramUniquePtr.get(), numberOfElements, d->tomogram.get());

            d->tomogramSizeX = static_cast<int32_t>(tomogramSizeX);
            d->tomogramSizeY = static_cast<int32_t>(tomogramSizeY);
            d->tomogramSizeZ = static_cast<int32_t>(tomogramSizeZ);

            d->tomogramVoxelSizeX = tomogramVoxelSizeX;
            d->tomogramVoxelSizeY = tomogramVoxelSizeY;
            d->tomogramVoxelSizeZ = tomogramVoxelSizeZ;
        }
        catch (const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }

        return true;
    }

    auto DeconvolutionModule::GetTomogram() const -> std::shared_ptr<float[]> {
        return d->tomogram;
    }

    auto DeconvolutionModule::GetTomogramSizeX() const -> int32_t {
        return d->tomogramSizeX;
    }

    auto DeconvolutionModule::GetTomogramSizeY() const -> int32_t {
        return d->tomogramSizeY;
    }

    auto DeconvolutionModule::GetTomogramSizeZ() const -> int32_t {
        return d->tomogramSizeZ;
    }

    auto DeconvolutionModule::GetTomogramVoxelSizeX() const -> double {
        return d->tomogramVoxelSizeX;
    }

    auto DeconvolutionModule::GetTomogramVoxelSizeY() const -> double {
        return d->tomogramVoxelSizeY;
    }

    auto DeconvolutionModule::GetTomogramVoxelSizeZ() const -> double {
        return d->tomogramVoxelSizeZ;
    }
}
