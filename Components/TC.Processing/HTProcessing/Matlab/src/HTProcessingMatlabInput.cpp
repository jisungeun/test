#include "HTProcessingMatlabInput.h"

namespace TC::HTProcessingMatlab {
    class HTProcessingMatlabInput::Impl {
    public:
        Impl() = default;
        Impl(const Impl&) = default;
        ~Impl() = default;

        auto operator=(const Impl&)->Impl & = default;

        PathInfo pathInfo{};
        ImageCropInfo imageCropInfo{};
        AcquisitionInfo acquisitionInfo{};
        MatlabModuleFilePath matlabModuleFilePath{};
    };

    HTProcessingMatlabInput::HTProcessingMatlabInput() : d(new Impl()) {
    }

    HTProcessingMatlabInput::HTProcessingMatlabInput(const HTProcessingMatlabInput& rhs) : d(new Impl(*rhs.d)) {
    }

    HTProcessingMatlabInput::~HTProcessingMatlabInput() = default;

    auto HTProcessingMatlabInput::operator=(const HTProcessingMatlabInput& rhs) -> HTProcessingMatlabInput& {
        *this->d = *rhs.d;
        return *this;
    }

    auto HTProcessingMatlabInput::SetPathInfo(const PathInfo& pathInfo) -> void {
        d->pathInfo = pathInfo;
    }

    auto HTProcessingMatlabInput::SetImageCropInfo(const ImageCropInfo& imageCropInfo) -> void {
        d->imageCropInfo = imageCropInfo;
    }

    auto HTProcessingMatlabInput::SetAcquisitionInfo(const AcquisitionInfo& acquisitionInfo) -> void {
        d->acquisitionInfo = acquisitionInfo;
    }

    auto HTProcessingMatlabInput::SetMatlabModuleFilePath(const MatlabModuleFilePath& matlabModuleFilePath) -> void {
        d->matlabModuleFilePath = matlabModuleFilePath;
    }

    auto HTProcessingMatlabInput::GetPathInfo() const -> const PathInfo& {
        return d->pathInfo;
    }

    auto HTProcessingMatlabInput::GetImageCropInfo() const -> const ImageCropInfo& {
        return d->imageCropInfo;
    }

    auto HTProcessingMatlabInput::GetAcquisitionInfo() const -> const AcquisitionInfo& {
        return d->acquisitionInfo;
    }

    auto HTProcessingMatlabInput::GetMatlabModuleFilePath() const -> const MatlabModuleFilePath& {
        return d->matlabModuleFilePath;
    }
}
