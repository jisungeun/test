#include "DeconvolutionParameters.h"

class DeconvolutionParameters::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    double mediumRI{};
    double naCond{};
    double voxelSizeXY{};
    double voxleSizeZ{};

    QString sampleFolderPath{};
    QString backgroundFolderPath{};

    int32_t sampleCropOffsetX{};
    int32_t sampleCropOffsetY{};
    int32_t backgroundCropOffsetX{};
    int32_t backgroundCropOffsetY{};

    int32_t cropSizeX{};
    int32_t cropSizeY{};

    std::shared_ptr<float[]> psfData{};
    int32_t psfImagRealCount{};
    int32_t psfPatternCount{};
    int32_t psfSizeX{};
    int32_t psfSizeY{};
    int32_t psfSizeZ{};

    std::shared_ptr<float[]> supportData{};
    int32_t supportSizeX{};
    int32_t supportSizeY{};
    int32_t supportSizeZ{};

    double kResX{};
    double kResY{};
    double kResZ{};
};

DeconvolutionParameters::DeconvolutionParameters() : d(new Impl()) {
}

DeconvolutionParameters::DeconvolutionParameters(const DeconvolutionParameters& other) : d(new Impl(*other.d)) {
}

DeconvolutionParameters::~DeconvolutionParameters() = default;

auto DeconvolutionParameters::operator=(const DeconvolutionParameters& other) -> DeconvolutionParameters& {
    *(this->d) = *(other.d);
    return *this;
}

auto DeconvolutionParameters::SetMediumRI(const double& mediumRI) -> void {
    d->mediumRI = mediumRI;
}

auto DeconvolutionParameters::SetNACond(const double& naCond) -> void {
    d->naCond = naCond;
}

auto DeconvolutionParameters::SetVoxelSize(const double& voxelSizeXY, const double& voxelSizeZ) -> void {
    d->voxelSizeXY = voxelSizeXY;
    d->voxleSizeZ = voxelSizeZ;
}

auto DeconvolutionParameters::SetSampleFolderPath(const QString& sampleFolderPath) -> void {
    d->sampleFolderPath = sampleFolderPath;
}

auto DeconvolutionParameters::SetBackgroundFolderPath(const QString& backgroundFolderPath) -> void {
    d->backgroundFolderPath = backgroundFolderPath;
}

auto DeconvolutionParameters::SetCropOffset(const int32_t& sampleCropOffsetX, const int32_t& sampleCropOffsetY,
    const int32_t& backgroundCropOffsetX, const int32_t& backgroundCropOffsetY) -> void {
    d->sampleCropOffsetX = sampleCropOffsetX;
    d->sampleCropOffsetY = sampleCropOffsetY;
    d->backgroundCropOffsetX = backgroundCropOffsetX;
    d->backgroundCropOffsetY = backgroundCropOffsetY;
}

auto DeconvolutionParameters::SetCropSize(const int32_t& cropSizeX, const int32_t& cropSizeY) -> void {
    d->cropSizeX = cropSizeX;
    d->cropSizeY = cropSizeY;
}

auto DeconvolutionParameters::SetPSF(const std::shared_ptr<float[]>& psfData, const int32_t& imagRealCount,
    const int32_t& patternCount, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->psfData = psfData;
    d->psfImagRealCount = imagRealCount;
    d->psfPatternCount = patternCount;
    d->psfSizeX = sizeX;
    d->psfSizeY = sizeY;
    d->psfSizeZ = sizeZ;
}

auto DeconvolutionParameters::SetSupport(const std::shared_ptr<float[]>& supportData, const int32_t& sizeX,
    const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->supportData = supportData;
    d->supportSizeX = sizeX;
    d->supportSizeY = sizeY;
    d->supportSizeZ = sizeZ;
}

auto DeconvolutionParameters::SetKResXYZ(const double& x, const double& y, const double& z) -> void {
    d->kResX = x;
    d->kResY = y;
    d->kResZ = z;
}

auto DeconvolutionParameters::GetMediumRI() const -> double {
    return d->mediumRI;
}

auto DeconvolutionParameters::GetNACond() const -> double {
    return d->naCond;
}

auto DeconvolutionParameters::GetVoxelSizeXY() const -> double {
    return d->voxelSizeXY;
}

auto DeconvolutionParameters::GetVoxelSizeZ() const -> double {
    return d->voxleSizeZ;
}

auto DeconvolutionParameters::GetSampleFolderPath() const -> QString {
    return d->sampleFolderPath;
}

auto DeconvolutionParameters::GetBackgroundFolderPath() const -> QString {
    return d->backgroundFolderPath;
}

auto DeconvolutionParameters::GetSampleCropOffsetX() const -> int32_t {
    return d->sampleCropOffsetX;
}

auto DeconvolutionParameters::GetSampleCropOffsetY() const -> int32_t {
    return d->sampleCropOffsetY;
}

auto DeconvolutionParameters::GetBackgroundCropOffsetX() const -> int32_t {
    return d->backgroundCropOffsetX;
}

auto DeconvolutionParameters::GetBackgroundCropOffsetY() const -> int32_t {
    return d->backgroundCropOffsetY;
}

auto DeconvolutionParameters::GetCropSizeX() const -> int32_t {
    return d->cropSizeX;
}

auto DeconvolutionParameters::GetCropSizeY() const -> int32_t {
    return d->cropSizeY;
}

auto DeconvolutionParameters::GetPSFData() const -> std::shared_ptr<float[]> {
    return d->psfData;
}

auto DeconvolutionParameters::GetPSFImagRealCount() const -> int32_t {
    return d->psfImagRealCount;
}

auto DeconvolutionParameters::GetPSFPatternCount() const -> int32_t {
    return d->psfPatternCount;
}

auto DeconvolutionParameters::GetPSFSizeX() const -> int32_t {
    return d->psfSizeX;
}

auto DeconvolutionParameters::GetPSFSizeY() const -> int32_t {
    return d->psfSizeY;
}

auto DeconvolutionParameters::GetPSFSizeZ() const -> int32_t {
    return d->psfSizeZ;
}

auto DeconvolutionParameters::GetSupportData() const -> std::shared_ptr<float[]> {
    return d->supportData;
}

auto DeconvolutionParameters::GetSupportSizeX() const -> int32_t {
    return d->supportSizeX;
}

auto DeconvolutionParameters::GetSupportSizeY() const -> int32_t {
    return d->supportSizeY;
}

auto DeconvolutionParameters::GetSupportSizeZ() const -> int32_t {
    return d->supportSizeZ;
}

auto DeconvolutionParameters::GetKResX() const -> double {
    return d->kResX;
}
auto DeconvolutionParameters::GetKResY() const -> double {
    return d->kResY;
}
auto DeconvolutionParameters::GetKResZ() const -> double {
    return d->kResZ;
}
