#include "HTProcessingMatlabOutput.h"

namespace TC::HTProcessingMatlab {
    class HTProcessingMatlabOutput::Impl {
    public:
        Impl() = default;
        Impl(const Impl& rhs) = default;
        ~Impl() = default;
        auto operator=(const Impl&)->Impl & = default;

        std::shared_ptr<float[]> tomogram{};
        int32_t sizeX;
        int32_t sizeY;
        int32_t sizeZ;

        float resolutionX;
        float resolutionY;
        float resolutionZ;
    };

    HTProcessingMatlabOutput::HTProcessingMatlabOutput() : d(new Impl()) {
    }

    HTProcessingMatlabOutput::HTProcessingMatlabOutput(const HTProcessingMatlabOutput& rhs) : d(new Impl(*rhs.d)) {
    }

    HTProcessingMatlabOutput::~HTProcessingMatlabOutput() = default;

    auto HTProcessingMatlabOutput::operator=(const HTProcessingMatlabOutput& rhs) -> HTProcessingMatlabOutput& {
        *this->d = *rhs.d;
        return *this;
    }

    auto HTProcessingMatlabOutput::SetTomogramInfo(const std::shared_ptr<float[]>& tomogram, const int32_t& sizeX,
        const int32_t& sizeY, const int32_t& sizeZ) -> void {
        d->tomogram = tomogram;
        d->sizeX = sizeX;
        d->sizeY = sizeY;
        d->sizeZ = sizeZ;
    }

    auto HTProcessingMatlabOutput::SetResolutionInfo(const float& resolutionX, const float& resolutionY,
        const float& resolutionZ) -> void {
        d->resolutionX = resolutionX;
        d->resolutionY = resolutionY;
        d->resolutionZ = resolutionZ;
    }

    auto HTProcessingMatlabOutput::GetTomogram() const -> const std::shared_ptr<float[]>& {
        return d->tomogram;
    }

    auto HTProcessingMatlabOutput::GetTomogramSizeX() const -> const int32_t& {
        return d->sizeX;
    }

    auto HTProcessingMatlabOutput::GetTomogramSizeY() const -> const int32_t& {
        return d->sizeY;
    }

    auto HTProcessingMatlabOutput::GetTomogramSizeZ() const -> const int32_t& {
        return d->sizeZ;
    }

    auto HTProcessingMatlabOutput::GetResolutionX() const -> const float& {
        return d->resolutionX;
    }

    auto HTProcessingMatlabOutput::GetResolutionY() const -> const float& {
        return d->resolutionY;
    }

    auto HTProcessingMatlabOutput::GetResolutionZ() const -> const float& {
        return d->resolutionZ;
    }
}
