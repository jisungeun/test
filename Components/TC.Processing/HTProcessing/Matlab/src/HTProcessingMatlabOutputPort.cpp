#include "HTProcessingMatlabOutputPort.h"

namespace TC::HTProcessingMatlab {
    class HTProcessingMatlabOutputPort::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        HTProcessingMatlabOutput htProcessingMatlabOutput;
    };

    HTProcessingMatlabOutputPort::HTProcessingMatlabOutputPort() : d(new Impl()) {
    }

    HTProcessingMatlabOutputPort::~HTProcessingMatlabOutputPort() = default;

    auto HTProcessingMatlabOutputPort::SetOutput(const HTProcessingMatlabOutput& htProcessingMatlabOutput) -> void {
        d->htProcessingMatlabOutput = htProcessingMatlabOutput;
    }

    auto HTProcessingMatlabOutputPort::GetOutput() const -> const HTProcessingMatlabOutput& {
        return d->htProcessingMatlabOutput;
    }
}
