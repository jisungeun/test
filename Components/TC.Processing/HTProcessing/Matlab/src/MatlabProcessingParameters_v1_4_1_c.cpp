#include "MatlabProcessingParameters_v1_4_1_c.h"

class MatlabProcessingParameters_v1_4_1_c::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    double mediumRI{};
    double naCond{};
    double voxelSizeXY{};
    double voxleSizeZ{};

    QString sampleFolderPath{};
    QString backgroundFolderPath{};

    int32_t sampleCropOffsetX{};
    int32_t sampleCropOffsetY{};
    int32_t backgroundCropOffsetX{};
    int32_t backgroundCropOffsetY{};

    int32_t cropSizeX{};
    int32_t cropSizeY{};

    std::shared_ptr<float[]> psfData{};
    int32_t psfImagRealCount{};
    int32_t psfPatternCount{};
    int32_t psfSizeX{};
    int32_t psfSizeY{};
    int32_t psfSizeZ{};

    std::shared_ptr<float[]> supportData{};
    int32_t supportSizeX{};
    int32_t supportSizeY{};
    int32_t supportSizeZ{};

    double kResX{};
    double kResY{};
    double kResZ{};
};

MatlabProcessingParameters_v1_4_1_c::MatlabProcessingParameters_v1_4_1_c() : d(new Impl()) {
}

MatlabProcessingParameters_v1_4_1_c::MatlabProcessingParameters_v1_4_1_c(
    const MatlabProcessingParameters_v1_4_1_c& other) : d(new Impl(*other.d)) {
}

MatlabProcessingParameters_v1_4_1_c::~MatlabProcessingParameters_v1_4_1_c() = default;

auto MatlabProcessingParameters_v1_4_1_c::operator=(const MatlabProcessingParameters_v1_4_1_c& other) -> MatlabProcessingParameters_v1_4_1_c& {
    *(this->d) = *(other.d);
    return *this;
}

auto MatlabProcessingParameters_v1_4_1_c::SetMediumRI(const double& mediumRI) -> void {
    d->mediumRI = mediumRI;
}

auto MatlabProcessingParameters_v1_4_1_c::SetNACond(const double& naCond) -> void {
    d->naCond = naCond;
}

auto MatlabProcessingParameters_v1_4_1_c::SetVoxelSize(const double& voxelSizeXY, const double& voxelSizeZ) -> void {
    d->voxelSizeXY = voxelSizeXY;
    d->voxleSizeZ = voxelSizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::SetSampleFolderPath(const QString& sampleFolderPath) -> void {
    d->sampleFolderPath = sampleFolderPath;
}

auto MatlabProcessingParameters_v1_4_1_c::SetBackgroundFolderPath(const QString& backgroundFolderPath) -> void {
    d->backgroundFolderPath = backgroundFolderPath;
}

auto MatlabProcessingParameters_v1_4_1_c::SetCropOffset(const int32_t& sampleCropOffsetX, const int32_t& sampleCropOffsetY,
    const int32_t& backgroundCropOffsetX, const int32_t& backgroundCropOffsetY) -> void {
    d->sampleCropOffsetX = sampleCropOffsetX;
    d->sampleCropOffsetY = sampleCropOffsetY;
    d->backgroundCropOffsetX = backgroundCropOffsetX;
    d->backgroundCropOffsetY = backgroundCropOffsetY;
}

auto MatlabProcessingParameters_v1_4_1_c::SetCropSize(const int32_t& cropSizeX, const int32_t& cropSizeY) -> void {
    d->cropSizeX = cropSizeX;
    d->cropSizeY = cropSizeY;
}

auto MatlabProcessingParameters_v1_4_1_c::SetPSF(const std::shared_ptr<float[]>& psfData, const int32_t& imagRealCount,
    const int32_t& patternCount, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->psfData = psfData;
    d->psfImagRealCount = imagRealCount;
    d->psfPatternCount = patternCount;
    d->psfSizeX = sizeX;
    d->psfSizeY = sizeY;
    d->psfSizeZ = sizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::SetSupport(const std::shared_ptr<float[]>& supportData, const int32_t& sizeX,
    const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->supportData = supportData;
    d->supportSizeX = sizeX;
    d->supportSizeY = sizeY;
    d->supportSizeZ = sizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::SetKResXYZ(const double& x, const double& y, const double& z) -> void {
    d->kResX = x;
    d->kResY = y;
    d->kResZ = z;
}

auto MatlabProcessingParameters_v1_4_1_c::GetMediumRI() const -> double {
    return d->mediumRI;
}

auto MatlabProcessingParameters_v1_4_1_c::GetNACond() const -> double {
    return d->naCond;
}

auto MatlabProcessingParameters_v1_4_1_c::GetVoxelSizeXY() const -> double {
    return d->voxelSizeXY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetVoxelSizeZ() const -> double {
    return d->voxleSizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSampleFolderPath() const -> QString {
    return d->sampleFolderPath;
}

auto MatlabProcessingParameters_v1_4_1_c::GetBackgroundFolderPath() const -> QString {
    return d->backgroundFolderPath;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSampleCropOffsetX() const -> int32_t {
    return d->sampleCropOffsetX;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSampleCropOffsetY() const -> int32_t {
    return d->sampleCropOffsetY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetBackgroundCropOffsetX() const -> int32_t {
    return d->backgroundCropOffsetX;
}

auto MatlabProcessingParameters_v1_4_1_c::GetBackgroundCropOffsetY() const -> int32_t {
    return d->backgroundCropOffsetY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetCropSizeX() const -> int32_t {
    return d->cropSizeX;
}

auto MatlabProcessingParameters_v1_4_1_c::GetCropSizeY() const -> int32_t {
    return d->cropSizeY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFData() const -> std::shared_ptr<float[]> {
    return d->psfData;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFImagRealCount() const -> int32_t {
    return d->psfImagRealCount;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFPatternCount() const -> int32_t {
    return d->psfPatternCount;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFSizeX() const -> int32_t {
    return d->psfSizeX;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFSizeY() const -> int32_t {
    return d->psfSizeY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetPSFSizeZ() const -> int32_t {
    return d->psfSizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSupportData() const -> std::shared_ptr<float[]> {
    return d->supportData;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSupportSizeX() const -> int32_t {
    return d->supportSizeX;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSupportSizeY() const -> int32_t {
    return d->supportSizeY;
}

auto MatlabProcessingParameters_v1_4_1_c::GetSupportSizeZ() const -> int32_t {
    return d->supportSizeZ;
}

auto MatlabProcessingParameters_v1_4_1_c::GetKResX() const -> double {
    return d->kResX;
}
auto MatlabProcessingParameters_v1_4_1_c::GetKResY() const -> double {
    return d->kResY;
}
auto MatlabProcessingParameters_v1_4_1_c::GetKResZ() const -> double {
    return d->kResZ;
}
