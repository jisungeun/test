#pragma once

#include <memory>
#include <QString>

#include "MatlabProcessingParameters_v1_4_1_c.h"
#include "HTProcessingProfile_v1_4_1_c.h"

namespace TC::HTProcessingMatlab {
    class MatlabProcessingModule_v1_4_1_c {
    public:
        MatlabProcessingModule_v1_4_1_c();
        ~MatlabProcessingModule_v1_4_1_c();

        auto SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath)->void;
        auto SetDeconvolutionParameters(const MatlabProcessingParameters_v1_4_1_c& parameters)->void;
        auto SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_c& profile)->void;

        auto Process()->bool;

        auto GetTomogram()const->std::shared_ptr<float[]>;

        auto GetTomogramSizeX()const->int32_t;
        auto GetTomogramSizeY()const->int32_t;
        auto GetTomogramSizeZ()const->int32_t;

        auto GetTomogramVoxelSizeX()const->double;
        auto GetTomogramVoxelSizeY()const->double;
        auto GetTomogramVoxelSizeZ()const->double;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
