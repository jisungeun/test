#pragma once

#include <memory>
#include <QString>

#include "MatlabProcessingParameters_v1_4_1_d.h"
#include "HTProcessingProfile_v1_4_1_d.h"

namespace TC::HTProcessingMatlab {
    class MatlabProcessingModule_v1_4_1_d {
    public:
        MatlabProcessingModule_v1_4_1_d();
        ~MatlabProcessingModule_v1_4_1_d();

        auto SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath)->void;
        auto SetDeconvolutionParameters(const MatlabProcessingParameters_v1_4_1_d& parameters)->void;
        auto SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_d& profile)->void;

        auto Process()->bool;

        auto GetTomogram()const->std::shared_ptr<float[]>;

        auto GetTomogramSizeX()const->int32_t;
        auto GetTomogramSizeY()const->int32_t;
        auto GetTomogramSizeZ()const->int32_t;

        auto GetTomogramVoxelSizeX()const->double;
        auto GetTomogramVoxelSizeY()const->double;
        auto GetTomogramVoxelSizeZ()const->double;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
