#define LOGGER_TAG "[Processor_v1_4_1_d]"

#include "Processor_v1_4_1_d.h"

#include "MatlabProcessingModule_v1_4_1_d.h"
#include "MatlabProcessingParameters_v1_4_1_d.h"
#include "PSFHandler_v1_4_1_c.h"

#include "TCLogger.h"

namespace TC::HTProcessingMatlab {
    using PsfProfile = PSFProfile::PSFProfile_v1_4_1_c;
    using ProcessingProfile = HTProcessingProfile::HTProcessingProfile_v1_4_1_d;
    using PSFBuildingParameters = PSFMatlab::PSFBuildingParameters_v1_4_1_c;
    using PSFHandler = PSFMatlab::PSFHandler_v1_4_1_c;
    using MatlabProcessingParameters = MatlabProcessingParameters_v1_4_1_d;
    using MatlabProcessingModule = MatlabProcessingModule_v1_4_1_d;

    class Processor_v1_4_1_d::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        HTProcessingMatlabInput input{};
        IHTProcessingMatlabOutputPort::Pointer output{};
        PsfProfile psfProfile{};
        ProcessingProfile htProcessingProfile{};
    };

    Processor_v1_4_1_d::Processor_v1_4_1_d() : d(new Impl()) {
    }

    Processor_v1_4_1_d::~Processor_v1_4_1_d() = default;

    auto Processor_v1_4_1_d::SetInput(const HTProcessingMatlabInput& input) -> void {
        d->input = input;
    }

    auto Processor_v1_4_1_d::SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile) -> void {
        d->psfProfile = psfProfile;
    }

    auto Processor_v1_4_1_d::SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_d& profile)
        -> void {
        d->htProcessingProfile = profile;
    }

    auto Processor_v1_4_1_d::SetOutputPort(const IHTProcessingMatlabOutputPort::Pointer& outputPort)
        -> void {
        d->output = outputPort;
    }

    auto Processor_v1_4_1_d::Process() -> bool {
        const auto htProcessingModuleFilePath = d->input.GetMatlabModuleFilePath().htProcessingFilePath;
        const auto psfModuleFilePath = d->input.GetMatlabModuleFilePath().psfModuleFilePath;

        const auto mediumRI = d->input.GetAcquisitionInfo().mediumRi;
        const auto naCond = d->input.GetAcquisitionInfo().condenserNA;
        const auto naObj = d->input.GetAcquisitionInfo().objectiveNA;
        const auto voxelSizeXY = d->input.GetAcquisitionInfo().voxelSizeXY;
        const auto voxelSizeZ = d->input.GetAcquisitionInfo().voxelSizeZ;

        QLOG_INFO() << "PSF Handling";
        const auto psfFolderPath = d->input.GetPathInfo().psfFolderPath;

        const auto imageSizeX = d->input.GetImageCropInfo().cropSizeX;
        const auto imageSizeY = d->input.GetImageCropInfo().cropSizeY;

        PSFBuildingParameters psfBuildingParameters;
        psfBuildingParameters.SetMediumRI(mediumRI);
        psfBuildingParameters.SetCondenserNA(naCond);
        psfBuildingParameters.SetObjectiveNA(naObj);
        psfBuildingParameters.SetVoxelSizeXY(voxelSizeXY);
        psfBuildingParameters.SetVoxelSizeZ(voxelSizeZ);

        auto psfHandler = PSFHandler::GetInstance();
        psfHandler->SetPSFModuleFilePath(psfModuleFilePath);
        psfHandler->SetPSFFolderPath(psfFolderPath);
        psfHandler->SetPSFBuildingParameters(psfBuildingParameters);
        psfHandler->SetPSFProfile(d->psfProfile);

        if (!psfHandler->Update()) {
            QLOG_ERROR() << "psfHandler->Update() in Process()";
            return false;
        }

        const auto psfData = psfHandler->GetPSFData();
        const auto psfImagRealCount = psfHandler->GetPSFImagRealCount();
        const auto psfPatternCount = psfHandler->GetPSFPatternCount();

        const auto psfSizeX = d->psfProfile.psfSizeX;
        const auto psfSizeY = d->psfProfile.psfSizeY;
        const auto psfSizeZ = d->psfProfile.psfSizeZ;

        const auto supportData = psfHandler->GetSupportData();
        const auto supportSizeX = psfHandler->GetSupportSizeX();
        const auto supportSizeY = psfHandler->GetSupportSizeY();
        const auto supportSizeZ = psfHandler->GetSupportSizeZ();

        const auto kResX = psfHandler->GetKResX();
        const auto kResY = psfHandler->GetKResY();
        const auto kResZ = psfHandler->GetKResZ();

        QLOG_INFO() << "Processing";

        const auto sampleFolderPath = d->input.GetPathInfo().sampleImageFolderPath;
        const auto backgroundFolderPath = d->input.GetPathInfo().backgroundImageFolderPath;

        const auto sampleCropOffsetX = d->input.GetImageCropInfo().sampleCropStartIndexX;
        const auto sampleCropOffsetY = d->input.GetImageCropInfo().sampleCropStartIndexY;

        const auto backgroundCropOffsetX = d->input.GetImageCropInfo().backgroundCropStartIndexX;
        const auto backgroundCropOffsetY = d->input.GetImageCropInfo().backgroundCropStartIndexY;

        MatlabProcessingParameters parameters;
        parameters.SetMediumRI(mediumRI);
        parameters.SetNACond(naCond);
        parameters.SetVoxelSize(voxelSizeXY, voxelSizeZ);
        parameters.SetSampleFolderPath(sampleFolderPath);
        parameters.SetBackgroundFolderPath(backgroundFolderPath);
        parameters.SetCropOffset(sampleCropOffsetX, sampleCropOffsetY, backgroundCropOffsetX, backgroundCropOffsetY);
        parameters.SetCropSize(imageSizeX, imageSizeY);
        parameters.SetPSF(psfData, psfImagRealCount, psfPatternCount, psfSizeX, psfSizeY, psfSizeZ);
        parameters.SetSupport(supportData, supportSizeX, supportSizeY, supportSizeZ);
        parameters.SetKResXYZ(kResX, kResY, kResZ);

        MatlabProcessingModule processingModule;
        processingModule.SetHTProcessingModuleFilePath(htProcessingModuleFilePath);
        processingModule.SetDeconvolutionParameters(parameters);
        processingModule.SetHTProcessingProfile(d->htProcessingProfile);

        if (!processingModule.Process()) {
            QLOG_ERROR() << "processingModule.Process() in Process()";
            return false;
        }

        const auto tomogram = processingModule.GetTomogram();
        const auto tomogramSizeX = processingModule.GetTomogramSizeX();
        const auto tomogramSizeY = processingModule.GetTomogramSizeY();
        const auto tomogramSizeZ = processingModule.GetTomogramSizeZ();

        const auto tomogramVoxelSizeX = processingModule.GetTomogramVoxelSizeX();
        const auto tomogramVoxelSizeY = processingModule.GetTomogramVoxelSizeY();
        const auto tomogramVoxelSizeZ = processingModule.GetTomogramVoxelSizeZ();

        QLOG_INFO() << "Processing Done";

        HTProcessingMatlabOutput htProcessingMatlabOutput;
        htProcessingMatlabOutput.SetTomogramInfo(tomogram, tomogramSizeX, tomogramSizeY, tomogramSizeZ);
        htProcessingMatlabOutput.SetResolutionInfo(tomogramVoxelSizeX, tomogramVoxelSizeY, tomogramVoxelSizeZ);

        d->output->SetOutput(htProcessingMatlabOutput);

        return true;
    }
}
