#define LOGGER_TAG "[MatlabProcessingModule_v1_4_1_c]"

#include "MatlabProcessingModule_v1_4_1_c.h"

#include "MatlabLibRepository.h"
#include "MatlabSharedLibraryUtilities.h"
#include "TCLogger.h"

namespace TC::HTProcessingMatlab {
    const QString htProcessingFunctionName = "DeconTomogram_SharedLibrary";
    using MatlabProcessingParameters = MatlabProcessingParameters_v1_4_1_c;
    using ProcessingProfile = HTProcessingProfile::HTProcessingProfile_v1_4_1_c;

    class MatlabProcessingModule_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        auto GenerateProcessingInputArray()->matlab::data::Array;
        auto GenerateRegularizationProfileArray()->matlab::data::Array;

        QString htProcessingModuleFilePath{};
        MatlabProcessingParameters parameters{};
        ProcessingProfile htProcessingProfile{};
        
        std::shared_ptr<float[]> tomogram{};
        int32_t tomogramSizeX{};
        int32_t tomogramSizeY{};
        int32_t tomogramSizeZ{};

        double tomogramVoxelSizeX{};
        double tomogramVoxelSizeY{};
        double tomogramVoxelSizeZ{};
    };

    auto MatlabProcessingModule_v1_4_1_c::Impl::GenerateProcessingInputArray() -> matlab::data::Array {
        auto psfDimension = matlab::data::ArrayDimensions{};
        psfDimension.push_back(this->parameters.GetPSFImagRealCount());
        psfDimension.push_back(this->parameters.GetPSFPatternCount());
        psfDimension.push_back(this->parameters.GetPSFSizeY());
        psfDimension.push_back(this->parameters.GetPSFSizeX());
        psfDimension.push_back(this->parameters.GetPSFSizeZ());

        auto supportDimension = matlab::data::ArrayDimensions{};
        supportDimension.push_back(this->parameters.GetSupportSizeY());
        supportDimension.push_back(this->parameters.GetSupportSizeX());
        supportDimension.push_back(this->parameters.GetSupportSizeZ());

        matlab::data::ArrayFactory factory;
        auto psfDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(psfDimension));
        auto supportDataBuffer = factory.createBuffer<float>(matlab::data::getNumElements(supportDimension));

        std::copy_n(this->parameters.GetPSFData().get(), matlab::data::getNumElements(psfDimension), psfDataBuffer.get());
        std::copy_n(this->parameters.GetSupportData().get(), matlab::data::getNumElements(supportDimension), supportDataBuffer.get());

        const auto mediumRI = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetMediumRI());
        const auto naCond = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetNACond());
        const auto voxelSizeXY = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetVoxelSizeXY());
        const auto voxelSizeZ = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetVoxelSizeZ());
        const auto sampleFolderPath = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetSampleFolderPath());
        const auto backgroundFolderPath = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetBackgroundFolderPath());
        const auto sampleCropOffsetX = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetSampleCropOffsetX() + 1));
        const auto sampleCropOffsetY = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetSampleCropOffsetY() + 1));
        const auto backgroundCropOffsetX = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetBackgroundCropOffsetX() + 1));
        const auto backgroundCropOffsetY = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetBackgroundCropOffsetY() + 1));
        const auto cropSizeX = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetCropSizeX()));
        const auto cropSizeY = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->parameters.GetCropSizeY()));
        const auto psf =
            static_cast<matlab::data::Array>(factory.createArrayFromBuffer(psfDimension, std::move(psfDataBuffer)));
        const auto support =
            static_cast<matlab::data::Array>(factory.createArrayFromBuffer(supportDimension, std::move(supportDataBuffer)));
        const auto kResX = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetKResX());
        const auto kResY = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetKResY());
        const auto kResZ = MatlabSharedLibrary::ToMatlabInput(this->parameters.GetKResZ());

        auto processingInputStruct = factory.createStructArray({ 1,1 }, 
            { "mediumRI", "naCond", "voxelSizeXY", "voxelSizeZ", "sampleFolderPath", "backgroundFolderPath",
                "sampleCropOffsetX", "sampleCropOffsetY", "backgroundCropOffsetX", "backgroundCropOffsetY",
                "cropSizeX", "cropSizeY", "psf", "support", "kResX", "kResY", "kResZ" });

        processingInputStruct[0]["mediumRI"] = mediumRI;
        processingInputStruct[0]["naCond"] = naCond;
        processingInputStruct[0]["voxelSizeXY"] = voxelSizeXY;
        processingInputStruct[0]["voxelSizeZ"] = voxelSizeZ;
        processingInputStruct[0]["sampleFolderPath"] = sampleFolderPath;
        processingInputStruct[0]["backgroundFolderPath"] = backgroundFolderPath;
        processingInputStruct[0]["sampleCropOffsetX"] = sampleCropOffsetX;
        processingInputStruct[0]["sampleCropOffsetY"] = sampleCropOffsetY;
        processingInputStruct[0]["backgroundCropOffsetX"] = backgroundCropOffsetX;
        processingInputStruct[0]["backgroundCropOffsetY"] = backgroundCropOffsetY;
        processingInputStruct[0]["cropSizeX"] = cropSizeX;
        processingInputStruct[0]["cropSizeY"] = cropSizeY;
        processingInputStruct[0]["psf"] = psf;
        processingInputStruct[0]["support"] = support;
        processingInputStruct[0]["kResX"] = kResX;
        processingInputStruct[0]["kResY"] = kResY;
        processingInputStruct[0]["kResZ"] = kResZ;

        return static_cast<matlab::data::Array>(processingInputStruct);
    }

    auto MatlabProcessingModule_v1_4_1_c::Impl::GenerateRegularizationProfileArray() -> matlab::data::Array {
        const auto p01SupportDC = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01SupportDC));
        const auto p02SupportDC = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p02SupportDC));
        const auto p01NonNegRef = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01NonNegRef));
        const auto p02NonNegRef = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p02NonNegRef));
        const auto p01OuterIterNum = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.p01OuterIterNum));
        const auto p01InnerIterNum = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.p01InnerIterNum));
        const auto p02OuterIterNum = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.p02OuterIterNum));
        const auto p02InnerIterNum = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.p02InnerIterNum));
        const auto p01TVParam = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01TvParam));
        const auto p02TVParam = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p02TvParam));
        const auto resetGpParam4LastIteration = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.resetGpParam4LastIter));
        const auto p01SzRatioAxi = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01SzRatioAxi));
        const auto p01SzRatioLat = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01SzRatioLat));
        const auto lateralMinSizeOffset = MatlabSharedLibrary::ToMatlabInput(static_cast<int64_t>(this->htProcessingProfile.lateralMinSizeOffset));
        const auto sfcGap01 = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.sfcGap01));
        const auto sfcGap02 = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.sfcGap02));
        const auto sfcGap03 = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.sfcGap03));
        const auto p01NormalizeFactor = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p01NormalizationFactor));
        const auto p02NormalizeFactor = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.p02NormalizationFactor));
        const auto memoryUtilizationRatio = MatlabSharedLibrary::ToMatlabInput(static_cast<double>(this->htProcessingProfile.memoryUtilizationRatio));

        matlab::data::ArrayFactory factory;
        auto regularizationProfileStruct = factory.createStructArray({ 1,1 },
            { "p01_supportDC", "p02_supportDC", "p01_NonNegRef", "p02_NonNegRef", "p01_OuterIterNum", "p01_InnerIterNum",
                "p02_OuterIterNum", "p02_InnerIterNum", "p01_TvParam", "p02_TvParam", "resetGPparam4LastIteration",
                "p01_szRatioAxi", "p01_SzRatioLat", "lateralMinSizeOffset", "sfcGap01", "sfcGap02", "sfcGap03",
                "p01_normalizeFactor", "p02_normalizeFactor", "memoryUtilizationRatio" });

        regularizationProfileStruct[0]["p01_supportDC"] = p01SupportDC;
        regularizationProfileStruct[0]["p02_supportDC"] = p02SupportDC;
        regularizationProfileStruct[0]["p01_NonNegRef"] = p01NonNegRef;
        regularizationProfileStruct[0]["p02_NonNegRef"] = p02NonNegRef;
        regularizationProfileStruct[0]["p01_OuterIterNum"] = p01OuterIterNum;
        regularizationProfileStruct[0]["p01_InnerIterNum"] = p01InnerIterNum;
        regularizationProfileStruct[0]["p02_OuterIterNum"] = p02OuterIterNum;
        regularizationProfileStruct[0]["p02_InnerIterNum"] = p02InnerIterNum;
        regularizationProfileStruct[0]["p01_TvParam"] = p01TVParam;
        regularizationProfileStruct[0]["p02_TvParam"] = p02TVParam;
        regularizationProfileStruct[0]["resetGPparam4LastIteration"] = resetGpParam4LastIteration;
        regularizationProfileStruct[0]["p01_szRatioAxi"] = p01SzRatioAxi;
        regularizationProfileStruct[0]["p01_SzRatioLat"] = p01SzRatioLat;
        regularizationProfileStruct[0]["lateralMinSizeOffset"] = lateralMinSizeOffset;
        regularizationProfileStruct[0]["sfcGap01"] = sfcGap01;
        regularizationProfileStruct[0]["sfcGap02"] = sfcGap02;
        regularizationProfileStruct[0]["sfcGap03"] = sfcGap03;
        regularizationProfileStruct[0]["p01_normalizeFactor"] = p01NormalizeFactor;
        regularizationProfileStruct[0]["p02_normalizeFactor"] = p02NormalizeFactor;
        regularizationProfileStruct[0]["memoryUtilizationRatio"] = memoryUtilizationRatio;

        return static_cast<matlab::data::Array>(regularizationProfileStruct);
    }

    MatlabProcessingModule_v1_4_1_c::MatlabProcessingModule_v1_4_1_c() : d(new Impl()) {
    }

    MatlabProcessingModule_v1_4_1_c::~MatlabProcessingModule_v1_4_1_c() = default;

    auto MatlabProcessingModule_v1_4_1_c::SetHTProcessingModuleFilePath(const QString& htProcessingModuleFilePath) -> void {
        d->htProcessingModuleFilePath = htProcessingModuleFilePath;
    }

    auto MatlabProcessingModule_v1_4_1_c:: SetDeconvolutionParameters(const MatlabProcessingParameters_v1_4_1_c& parameters)
        -> void {
        d->parameters = parameters;
    }

    auto MatlabProcessingModule_v1_4_1_c::SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_c& profile)
        -> void {
        d->htProcessingProfile = profile;
    }

    auto MatlabProcessingModule_v1_4_1_c::Process() -> bool {
        const auto matlabLibrary = MatlabSharedLibrary::GetMatlabLibrary(d->htProcessingModuleFilePath.toStdU16String());
        if (matlabLibrary == nullptr) {
            return false;
        }

        try {
            const auto processingInput = d->GenerateProcessingInputArray();
            const auto regularizationProfile = d->GenerateRegularizationProfileArray();

            const std::vector inputs{ processingInput, regularizationProfile };

            constexpr auto numberOfOutputs = 3;

            const auto deconvolutionResultArray =
                matlabLibrary->feval(htProcessingFunctionName.toStdU16String(), numberOfOutputs, inputs);

            auto tomogramResult = static_cast<matlab::data::TypedArray<float>>(deconvolutionResultArray[0]);
            auto tomogramVoxelSizeXYResult = static_cast<matlab::data::TypedArray<double>>(deconvolutionResultArray[1]);
            auto tomogramVoxelSizeZResult = static_cast<matlab::data::TypedArray<double>>(deconvolutionResultArray[2]);

            const auto tomogramDimension = tomogramResult.getDimensions();

            const auto tomogramSizeX = tomogramDimension[1];
            const auto tomogramSizeY = tomogramDimension[0];
            const auto tomogramSizeZ = tomogramDimension[2];

            const auto tomogramVoxelSizeX = tomogramVoxelSizeXYResult[0];
            const auto tomogramVoxelSizeY = tomogramVoxelSizeXYResult[0];
            const auto tomogramVoxelSizeZ = tomogramVoxelSizeZResult[0];

            const auto numberOfElements = tomogramResult.getNumberOfElements();
            const auto tomogramUniquePtr = tomogramResult.release();

            d->tomogram = std::shared_ptr<float[]>{ new float[numberOfElements]() };
            std::copy_n(tomogramUniquePtr.get(), numberOfElements, d->tomogram.get());

            d->tomogramSizeX = static_cast<int32_t>(tomogramSizeX);
            d->tomogramSizeY = static_cast<int32_t>(tomogramSizeY);
            d->tomogramSizeZ = static_cast<int32_t>(tomogramSizeZ);

            d->tomogramVoxelSizeX = tomogramVoxelSizeX;
            d->tomogramVoxelSizeY = tomogramVoxelSizeY;
            d->tomogramVoxelSizeZ = tomogramVoxelSizeZ;
        }
        catch (const matlab::Exception& exception) {
            QLOG_ERROR() << exception.what();
            return false;
        }

        return true;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogram() const -> std::shared_ptr<float[]> {
        return d->tomogram;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramSizeX() const -> int32_t {
        return d->tomogramSizeX;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramSizeY() const -> int32_t {
        return d->tomogramSizeY;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramSizeZ() const -> int32_t {
        return d->tomogramSizeZ;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramVoxelSizeX() const -> double {
        return d->tomogramVoxelSizeX;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramVoxelSizeY() const -> double {
        return d->tomogramVoxelSizeY;
    }

    auto MatlabProcessingModule_v1_4_1_c::GetTomogramVoxelSizeZ() const -> double {
        return d->tomogramVoxelSizeZ;
    }
}
