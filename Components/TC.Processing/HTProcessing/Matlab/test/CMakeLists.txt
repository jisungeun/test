project(TCHTProcessingMatlabTest)

ADD_DEFINITIONS(-DTEST_DATA_FOLDR_PATH=\"${TEST_DATA_FOLDER_PATH}\")

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

set(TEST_SOURCE
)

#Sources
set(SOURCES
	src/TestMain.cpp
    src/HTProcessingMatlabInputTest.cpp
    src/HTProcessingMatlabOutputTest.cpp
    src/IHTProcessingMatlabOutputPortTest.cpp
    src/IHTProcessingMatlabProcessorTest.cpp
    src/HTProcessingMatlabOutputPortTest.cpp
	src/Processor_v1_4_1_dTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${TEST_SOURCE}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CMAKE_CURRENT_SOURCE_DIR}/../src
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE 
		cxx_std_17
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
		TC::Components::HTProcessingMatlab
        TC::Common::TestUtility
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases")

foreach(FILE ${TEST_SOURCE})
    # Remove common directory prefix to make the group
    string(REPLACE "src" "" GROUP "TEST_SOURCE")

    # Make sure we are using windows slashes
    string(REPLACE "/" "\\" GROUP "${GROUP}")
	
	source_group("${GROUP}" FILES "${FILE}")
endforeach()
