#include <catch2/catch.hpp>

#include "HTProcessingMatlabInput.h"

using namespace TC::HTProcessingMatlab;

namespace HTProcessingMatlabInputTest {
    TEST_CASE("HTProcessingMatlabInput : unit test") {
        SECTION("HTProcessingMatlabInput(HTProcessingMatlabInput)") {
            PathInfo pathInfo;
            pathInfo.sampleImageFolderPath = "sample";
            pathInfo.backgroundImageFolderPath = "back";
            
            HTProcessingMatlabInput srcHTProcessingMatlabInput;
            srcHTProcessingMatlabInput.SetPathInfo(pathInfo);

            HTProcessingMatlabInput destHTProcessingMatlabInput(srcHTProcessingMatlabInput);
            CHECK(destHTProcessingMatlabInput.GetPathInfo().sampleImageFolderPath == "sample");
            CHECK(destHTProcessingMatlabInput.GetPathInfo().backgroundImageFolderPath == "back");
        }
        SECTION("operator=()") {
            PathInfo pathInfo;
            pathInfo.sampleImageFolderPath = "sample";
            pathInfo.backgroundImageFolderPath = "back";

            HTProcessingMatlabInput srcHTProcessingMatlabInput;
            srcHTProcessingMatlabInput.SetPathInfo(pathInfo);

            HTProcessingMatlabInput destHTProcessingMatlabInput;
            destHTProcessingMatlabInput = srcHTProcessingMatlabInput;
            CHECK(destHTProcessingMatlabInput.GetPathInfo().sampleImageFolderPath == "sample");
            CHECK(destHTProcessingMatlabInput.GetPathInfo().backgroundImageFolderPath == "back");
        }
        SECTION("SetPathInfo() / GetPathInfo()") {
            PathInfo pathInfo;
            pathInfo.sampleImageFolderPath = "1";
            pathInfo.backgroundImageFolderPath = "2";
            
            HTProcessingMatlabInput htProcessingMatlabInput;
            htProcessingMatlabInput.SetPathInfo(pathInfo);

            CHECK(htProcessingMatlabInput.GetPathInfo().sampleImageFolderPath == "1");
            CHECK(htProcessingMatlabInput.GetPathInfo().backgroundImageFolderPath == "2");
        }
        SECTION("SetImageCropInfo() / GetImageCropInfo()") {
            ImageCropInfo imageCropInfo;
            imageCropInfo.backgroundCropStartIndexX = 1;
            imageCropInfo.backgroundCropStartIndexY = 2;
            imageCropInfo.cropSizeX = 3;
            imageCropInfo.cropSizeY = 4;
            imageCropInfo.sampleCropStartIndexX = 5;
            imageCropInfo.sampleCropStartIndexY = 6;

            HTProcessingMatlabInput htProcessingMatlabInput;
            htProcessingMatlabInput.SetImageCropInfo(imageCropInfo);

            CHECK(htProcessingMatlabInput.GetImageCropInfo().backgroundCropStartIndexX == 1);
            CHECK(htProcessingMatlabInput.GetImageCropInfo().backgroundCropStartIndexY == 2);
            CHECK(htProcessingMatlabInput.GetImageCropInfo().cropSizeX == 3);
            CHECK(htProcessingMatlabInput.GetImageCropInfo().cropSizeY == 4);
            CHECK(htProcessingMatlabInput.GetImageCropInfo().sampleCropStartIndexX == 5);
            CHECK(htProcessingMatlabInput.GetImageCropInfo().sampleCropStartIndexY == 6);
        }
        SECTION("SetAcquisitionInfo() / GetAcquisitionInfo()") {
            AcquisitionInfo acquisitionInfo;
            acquisitionInfo.mediumRi = 1;
            acquisitionInfo.objectiveNA = 2;
            acquisitionInfo.condenserNA = 3;
            acquisitionInfo.voxelSizeXY = 4;
            acquisitionInfo.voxelSizeZ = 5;
            
            HTProcessingMatlabInput htProcessingMatlabInput;
            htProcessingMatlabInput.SetAcquisitionInfo(acquisitionInfo);

            CHECK(htProcessingMatlabInput.GetAcquisitionInfo().mediumRi == 1);
            CHECK(htProcessingMatlabInput.GetAcquisitionInfo().objectiveNA == 2);
            CHECK(htProcessingMatlabInput.GetAcquisitionInfo().condenserNA == 3);
            CHECK(htProcessingMatlabInput.GetAcquisitionInfo().voxelSizeXY == 4);
            CHECK(htProcessingMatlabInput.GetAcquisitionInfo().voxelSizeZ == 5);
        }
        SECTION("SetMatlabModuleFilePath() / GetMatlabModuleFilePath()") {
            MatlabModuleFilePath matlabModuleFilePath;
            matlabModuleFilePath.htProcessingFilePath = "1";
            matlabModuleFilePath.psfModuleFilePath = "2";

            HTProcessingMatlabInput htProcessingMatlabInput;
            htProcessingMatlabInput.SetMatlabModuleFilePath(matlabModuleFilePath);

            CHECK(htProcessingMatlabInput.GetMatlabModuleFilePath().htProcessingFilePath == "1");
            CHECK(htProcessingMatlabInput.GetMatlabModuleFilePath().psfModuleFilePath == "2");
        }
    }
}

