#include <catch2/catch.hpp>

#include "HTProcessingMatlabOutputPort.h"

using namespace TC::HTProcessingMatlab;

namespace HTProcessingMatlabOutputPortTest {
    TEST_CASE("HTProcessingMatlabOutputPort") {
        SECTION("HTProcessingMatlabOutputPort()") {
            HTProcessingMatlabOutputPort htProcessingMatlabOutputPort;
            CHECK(&htProcessingMatlabOutputPort != nullptr);
        }
        SECTION("SetOutput()") {
            HTProcessingMatlabOutputPort htProcessingMatlabOutputPort;
            htProcessingMatlabOutputPort.SetOutput({});
            CHECK(&htProcessingMatlabOutputPort != nullptr);
        }
        SECTION("GetOutput()") {
            std::shared_ptr<float[]> tomogram{ new float[1]() };

            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(tomogram, 1, 2, 3);
            htProcessingMatlabOutput.SetResolutionInfo(1, 2, 3);

            HTProcessingMatlabOutputPort htProcessingMatlabOutputPort;
            htProcessingMatlabOutputPort.SetOutput(htProcessingMatlabOutput);
            const auto resultOutput = htProcessingMatlabOutputPort.GetOutput();

            CHECK(resultOutput.GetTomogramSizeX() == 1);
            CHECK(resultOutput.GetTomogramSizeY() == 2);
            CHECK(resultOutput.GetTomogramSizeZ() == 3);
            CHECK(resultOutput.GetResolutionX() == 1);
            CHECK(resultOutput.GetResolutionY() == 2);
            CHECK(resultOutput.GetResolutionZ() == 3);
        }
    }
}