#include <catch2/catch.hpp>

#include <BinaryFileIO.h>
#include <iostream>

#include "Processor_v1_4_1_d.h"
#include "HTProcessingMatlabOutputPort.h"
#include "HTProcessingProfileFactory.h"
#include "PSFProfileFactory.h"

namespace Processor_v1_4_1_dTest {
    using namespace TC::HTProcessingMatlab;
    using namespace TC::PSFProfile;
    using namespace TC::HTProcessingProfile;

    TEST_CASE("Processor_v1_4_1_d") {

        HTProcessingMatlabInput input;
        {
            PathInfo pathInfo;
            pathInfo.backgroundImageFolderPath = "E:/00_Data/20220928 HT Algorithm 1.4.1 verification/formatted/Square FOV/0.72 bg";
            pathInfo.sampleImageFolderPath = "E:/00_Data/20220928 HT Algorithm 1.4.1 verification/formatted/Square FOV/0.72";
            pathInfo.psfFolderPath = "D:/temp2/psf";

            ImageCropInfo imageCropInfo;
            imageCropInfo.backgroundCropStartIndexX = 260;
            imageCropInfo.backgroundCropStartIndexY = 24;
            imageCropInfo.cropSizeX = 1416;
            imageCropInfo.cropSizeY = 1416;
            imageCropInfo.sampleCropStartIndexX = 0;
            imageCropInfo.sampleCropStartIndexY = 0;

            AcquisitionInfo acquisitionInfo;
            acquisitionInfo.mediumRi = 1.337;
            acquisitionInfo.objectiveNA = 0.95;
            acquisitionInfo.condenserNA = 0.72;
            acquisitionInfo.voxelSizeXY = 0.1125;
            acquisitionInfo.voxelSizeZ = 1.0429;

            MatlabModuleFilePath matlabModuleFilePath;
            matlabModuleFilePath.psfModuleFilePath = "D:/HTX_R_B/bin/Release/HTProc/PSFModule_v1_4_1_c.ctf";
            matlabModuleFilePath.htProcessingFilePath = "D:/HTX_R_B/bin/Release/HTProc/HTModule_v1_4_1_d.ctf";

            input.SetPathInfo(pathInfo);
            input.SetImageCropInfo(imageCropInfo);
            input.SetAcquisitionInfo(acquisitionInfo);
            input.SetMatlabModuleFilePath(matlabModuleFilePath);
        }

        PSFProfile_v1_4_1_c psfProfile;
        {
            PSFProfileFactory::Input_v1_4_1_c psfFactoryInput;
            psfFactoryInput.naCond = 0.72;
            psfFactoryInput.imageSizeX = 1416;
            psfFactoryInput.imageSizeY = 1416;

            psfProfile = PSFProfileFactory::Generate_v1_4_1_c(psfFactoryInput);
        }

        HTProcessingProfile_v1_4_1_d htProcessingProfile;
        {
            HTProcessingProfileFactory::Input_v1_4_1_d processingFactoryInput;
            processingFactoryInput.enableRegularization = true;
            processingFactoryInput.naCond = 0.72;

            htProcessingProfile = HTProcessingProfileFactory::Generate_v1_4_1_d(processingFactoryInput);
        }

        std::shared_ptr<HTProcessingMatlabOutputPort> outputPort{ new HTProcessingMatlabOutputPort };
        
        Processor_v1_4_1_d processor;
        processor.SetInput(input);
        processor.SetPSFProfile(psfProfile);
        processor.SetHTProcessingProfile(htProcessingProfile);
        processor.SetOutputPort(outputPort);

        processor.Process();

        const auto output = outputPort->GetOutput();
        const auto resultTomogram = output.GetTomogram();
        const auto tomogramSizeX = static_cast<size_t>(output.GetTomogramSizeX());
        const auto tomogramSizeY = static_cast<size_t>(output.GetTomogramSizeY());
        const auto tomogramSizeZ = static_cast<size_t>(output.GetTomogramSizeZ());

        const auto tomogramResolutionX = output.GetResolutionX();
        const auto tomogramResolutionY = output.GetResolutionY();
        const auto tomogramResolutionZ = output.GetResolutionZ();

        std::cout << tomogramSizeX << std::endl;
        std::cout << tomogramSizeY << std::endl;
        std::cout << tomogramSizeZ << std::endl;

        std::cout << tomogramResolutionX << std::endl;
        std::cout << tomogramResolutionY << std::endl;
        std::cout << tomogramResolutionZ << std::endl;

        WriteFile("D:/temp2/tomogramResult", resultTomogram, tomogramSizeX * tomogramSizeY * tomogramSizeZ);
    }
}