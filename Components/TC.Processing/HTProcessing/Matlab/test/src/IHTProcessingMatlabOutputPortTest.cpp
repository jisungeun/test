#include <catch2/catch.hpp>

#include "IHTProcessingMatlabOutputPort.h"

using namespace TC::HTProcessingMatlab;

namespace IHTProcessingMatlabOutputPortTest {
    class HTProcessingMatlabOutputPortForTest final : public IHTProcessingMatlabOutputPort {
    public:
        HTProcessingMatlabOutputPortForTest() : IHTProcessingMatlabOutputPort() {}
        ~HTProcessingMatlabOutputPortForTest() = default;

        auto SetOutput(const HTProcessingMatlabOutput& htProcessingMatlabOutput) -> void override {
            this->outputIsSet = true;
        }
        auto IsOutputSet()->const bool& { return outputIsSet; }

    private:
        bool outputIsSet{ false };
    };

    TEST_CASE("IHTProcessingMatlabOutputPortTest") {
        SECTION("SetOutput()") {
            HTProcessingMatlabOutputPortForTest htProcessingMatlabOutputPort;
            htProcessingMatlabOutputPort.SetOutput(HTProcessingMatlabOutput{});

            CHECK(htProcessingMatlabOutputPort.IsOutputSet() == true);
        }
    }
}