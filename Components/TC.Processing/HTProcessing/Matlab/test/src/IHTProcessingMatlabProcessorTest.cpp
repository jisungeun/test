#include <catch2/catch.hpp>

#include "IHTProcessingMatlabProcessor.h"

using namespace TC::HTProcessingMatlab;

namespace IHTProcessingMatlabProcessorTest {
    class HTProcessingMatlabProcessorForTest final : public IHTProcessingMatlabProcessor {
    public:
        HTProcessingMatlabProcessorForTest() : IHTProcessingMatlabProcessor() {}
        ~HTProcessingMatlabProcessorForTest() = default;
        auto SetInput(const HTProcessingMatlabInput& input) -> void override {
            inputIsSet = true;
        }
        auto SetOutputPort(const IHTProcessingMatlabOutputPort::Pointer& outputPort) -> void override {
            outputPortIsSet = true;
        }
        auto Process() -> bool override {
            processIsTriggered = true;
            return true;
        }

        auto IsInputSet()->bool { return inputIsSet; }
        auto IsOutputPortSet()->bool { return outputPortIsSet; }
        auto IsProcessTriggered()->bool { return processIsTriggered; }
    private:
        bool inputIsSet{ false };
        bool outputPortIsSet{ false };
        bool processIsTriggered{ false };
    };

    TEST_CASE("IHTProcessingMatlabProcessorTest") {
        SECTION("SetInput()") {
            HTProcessingMatlabProcessorForTest iHTProcessingMatlabProcessor;
            iHTProcessingMatlabProcessor.SetInput(HTProcessingMatlabInput{});
            CHECK(iHTProcessingMatlabProcessor.IsInputSet() == true);
        }
        SECTION("SetOutputPort()") {
            HTProcessingMatlabProcessorForTest iHTProcessingMatlabProcessor;
            iHTProcessingMatlabProcessor.SetOutputPort(IHTProcessingMatlabOutputPort::Pointer{});
            CHECK(iHTProcessingMatlabProcessor.IsOutputPortSet() == true);
        }
        SECTION("Process()") {
            HTProcessingMatlabProcessorForTest iHTProcessingMatlabProcessor;
            iHTProcessingMatlabProcessor.Process();
            CHECK(iHTProcessingMatlabProcessor.IsProcessTriggered() == true);
        }
    }
}