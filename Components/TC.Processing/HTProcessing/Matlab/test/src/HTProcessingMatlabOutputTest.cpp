#include <catch2/catch.hpp>

#include "HTProcessingMatlabOutput.h"

using namespace TC::HTProcessingMatlab;

namespace HTProcessingMatlabOutputTest {
    TEST_CASE("HTProcessingMatlabOutputTest") {
        SECTION("HTProcessingMatlabOutput()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            CHECK(&htProcessingMatlabOutput != nullptr);
        }
        SECTION("HTProcessingMatlabOutput(HTProcessingMatlabOutput)") {
            const std::shared_ptr<float[]> tomogram{ new float[1] };

            HTProcessingMatlabOutput srcHTProcessingMatlabOutput;
            srcHTProcessingMatlabOutput.SetTomogramInfo(tomogram, 1, 1, 1);

            HTProcessingMatlabOutput destHTProcessingMatlabOutput(srcHTProcessingMatlabOutput);
            const auto& resultTomogram = destHTProcessingMatlabOutput.GetTomogram();

            CHECK(resultTomogram == tomogram);
        }
        SECTION("operator=") {
            const std::shared_ptr<float[]> tomogram{ new float[1] };

            HTProcessingMatlabOutput srcHTProcessingMatlabOutput;
            srcHTProcessingMatlabOutput.SetTomogramInfo(tomogram, 1, 1, 1);

            HTProcessingMatlabOutput destHTProcessingMatlabOutput;
            destHTProcessingMatlabOutput = srcHTProcessingMatlabOutput;
            const auto& resultTomogram = destHTProcessingMatlabOutput.GetTomogram();

            CHECK(resultTomogram == tomogram);
        }
        SECTION("SetTomogramInfo()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(std::shared_ptr<float[]>{}, 1, 1, 1);
            CHECK(&htProcessingMatlabOutput != nullptr);
        }
        SECTION("SetResolutionInfo()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetResolutionInfo(1, 1, 1);
            CHECK(&htProcessingMatlabOutput != nullptr);
        }
        SECTION("GetTomogram()") {
            const std::shared_ptr<float[]> tomogram{ new float[1] };
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(tomogram, 1, 1, 1);

            const auto& resultTomogram = htProcessingMatlabOutput.GetTomogram();
            CHECK(resultTomogram == tomogram);
        }
        SECTION("GetTomogramSizeX()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(std::shared_ptr<float[]>{}, 1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetTomogramSizeX() == 1);
        }
        SECTION("GetTomogramSizeY()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(std::shared_ptr<float[]>{}, 1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetTomogramSizeY() == 2);
        }
        SECTION("GetTomogramSizeZ()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetTomogramInfo(std::shared_ptr<float[]>{}, 1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetTomogramSizeZ() == 3);
        }
        SECTION("GetResolutionX()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetResolutionInfo(1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetResolutionX() == 1);
        }
        SECTION("GetResolutionY()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetResolutionInfo(1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetResolutionY() == 2);
        }
        SECTION("GetResolutionZ()") {
            HTProcessingMatlabOutput htProcessingMatlabOutput;
            htProcessingMatlabOutput.SetResolutionInfo(1, 2, 3);
            CHECK(htProcessingMatlabOutput.GetResolutionZ() == 3);
        }
    }
}
