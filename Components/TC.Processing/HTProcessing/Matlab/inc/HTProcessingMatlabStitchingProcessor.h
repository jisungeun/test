#pragma once

#include <memory>

#include "IPepsiODTMatlabProcessor.h"

#include "TCPepsiODTMatlabExport.h"

namespace TC::PepsiODTMatlab {
    class TCPepsiODTMatlab_API PepsiODTMatlabStitchingProcessor final : public IPepsiODTMatlabProcessor{
    public:
        PepsiODTMatlabStitchingProcessor();
        ~PepsiODTMatlabStitchingProcessor();

        auto SetProcessingOptions(const double& nonNegativeReference, const bool& useThresMapFlag, 
            const bool& onlyRealFlag, const SFCMethod& sfcMethod)->void;

        auto SetInput(const PepsiODTMatlabInput& input) -> void override;
        auto SetOutputPort(const IPepsiODTMatlabOutputPort::Pointer& outputPort) -> void override;
        auto Process() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
