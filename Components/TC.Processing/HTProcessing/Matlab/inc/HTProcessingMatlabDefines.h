#pragma once

#include <QMap>
#include <QString>
#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    struct TCHTProcessingMatlab_API ImageCropInfo {
        int32_t sampleCropStartIndexX{};
        int32_t sampleCropStartIndexY{};

        int32_t backgroundCropStartIndexX{};
        int32_t backgroundCropStartIndexY{};

        int32_t cropSizeX{};
        int32_t cropSizeY{};
    };

    struct TCHTProcessingMatlab_API AcquisitionInfo {
        double mediumRi{};
        double objectiveNA{};
        double condenserNA{};
        double voxelSizeXY{};
        double voxelSizeZ{};
    };

    struct PathInfo {
        QString sampleImageFolderPath{};
        QString backgroundImageFolderPath{};
        QString psfFolderPath{};
    };

    struct MatlabModuleFilePath {
        QString psfModuleFilePath{};
        QString htProcessingFilePath{};
    };
}