#pragma once
#pragma warning(disable : 4251)

#include <memory>

#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API HTProcessingMatlabOutput {
    public:
        HTProcessingMatlabOutput();
        HTProcessingMatlabOutput(const HTProcessingMatlabOutput& rhs);
        ~HTProcessingMatlabOutput();

        auto operator=(const HTProcessingMatlabOutput& rhs)->HTProcessingMatlabOutput&;

        auto SetTomogramInfo(const std::shared_ptr<float[]>& tomogram, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
        auto SetResolutionInfo(const float& resolutionX, const float& resolutionY, const float& resolutionZ)->void;

        auto GetTomogram()const ->const std::shared_ptr<float[]>&;
        auto GetTomogramSizeX()const->const int32_t&;
        auto GetTomogramSizeY()const->const int32_t&;
        auto GetTomogramSizeZ()const->const int32_t&;

        auto GetResolutionX()const->const float&;
        auto GetResolutionY()const->const float&;
        auto GetResolutionZ()const->const float&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
