#pragma once

#include <memory>
#include "IHTProcessingMatlabProcessor.h"
#include "PSFProfile_v1_4_1_c.h"
#include "HTProcessingProfile_v1_4_1_c.h"

#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API Processor_v1_4_1_c final : public IHTProcessingMatlabProcessor{
    public:
        Processor_v1_4_1_c();
        ~Processor_v1_4_1_c();

        auto SetInput(const HTProcessingMatlabInput& input)->void override;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void;
        auto SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_c& profile)->void;

        auto SetOutputPort(const IHTProcessingMatlabOutputPort::Pointer& outputPort) -> void override;
        auto Process() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}