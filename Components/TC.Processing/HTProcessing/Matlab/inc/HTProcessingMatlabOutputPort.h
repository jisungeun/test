#pragma once

#include <memory>

#include "IHTProcessingMatlabOutputPort.h"

#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API HTProcessingMatlabOutputPort final : public IHTProcessingMatlabOutputPort {
    public:
        HTProcessingMatlabOutputPort();
        ~HTProcessingMatlabOutputPort();

        auto SetOutput(const HTProcessingMatlabOutput& htProcessingMatlabOutput) -> void override;
        auto GetOutput()const -> const HTProcessingMatlabOutput&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}