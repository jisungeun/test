#pragma once

#include <memory>

#include "TCHTProcessingMatlabExport.h"

#include "HTProcessingMatlabInput.h"
#include "IHTProcessingMatlabOutputPort.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API IHTProcessingMatlabProcessor {
    public:
        using Pointer = std::shared_ptr<IHTProcessingMatlabProcessor>;

        IHTProcessingMatlabProcessor() = default;
        virtual ~IHTProcessingMatlabProcessor() = default;

        virtual auto SetInput(const HTProcessingMatlabInput& input)->void = 0;
        virtual auto SetOutputPort(const IHTProcessingMatlabOutputPort::Pointer& outputPort)->void = 0;

        virtual auto Process()->bool = 0;
    };
}