#pragma once

#include <memory>

#include <QString>

#include "HTProcessingMatlabDefines.h"

#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API HTProcessingMatlabInput {
    public:
        HTProcessingMatlabInput();
        HTProcessingMatlabInput(const HTProcessingMatlabInput& rhs);
        ~HTProcessingMatlabInput();

        auto operator=(const HTProcessingMatlabInput& rhs)->HTProcessingMatlabInput&;

        auto SetPathInfo(const PathInfo& pathInfo)->void;
        auto SetImageCropInfo(const ImageCropInfo& imageCropInfo)->void;
        auto SetAcquisitionInfo(const AcquisitionInfo& acquisitionInfo)->void;
        auto SetMatlabModuleFilePath(const MatlabModuleFilePath& matlabModuleFilePath)->void;
        
        auto GetPathInfo()const->const PathInfo&;
        auto GetImageCropInfo()const->const ImageCropInfo&;
        auto GetAcquisitionInfo()const->const AcquisitionInfo&;
        auto GetMatlabModuleFilePath()const->const MatlabModuleFilePath&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}