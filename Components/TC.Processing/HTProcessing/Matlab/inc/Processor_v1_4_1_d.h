#pragma once

#include <memory>
#include "IHTProcessingMatlabProcessor.h"
#include "PSFProfile_v1_4_1_c.h"
#include "HTProcessingProfile_v1_4_1_d.h"

#include "TCHTProcessingMatlabExport.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API Processor_v1_4_1_d final : public IHTProcessingMatlabProcessor{
    public:
        Processor_v1_4_1_d();
        ~Processor_v1_4_1_d();

        auto SetInput(const HTProcessingMatlabInput& input)->void override;
        auto SetPSFProfile(const PSFProfile::PSFProfile_v1_4_1_c& psfProfile)->void; // Processor v1.4.1d uses PSFModule v1.4.1c
        auto SetHTProcessingProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_d& profile)->void;

        auto SetOutputPort(const IHTProcessingMatlabOutputPort::Pointer& outputPort) -> void override;
        auto Process() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}