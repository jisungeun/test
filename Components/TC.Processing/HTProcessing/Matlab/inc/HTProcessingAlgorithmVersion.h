#pragma once

enum class HTProcessingAlgorithmVersion { none, v1_4_1_c, v1_4_1_d };
