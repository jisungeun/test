#pragma once

#include <memory>

#include "TCHTProcessingMatlabExport.h"

#include "HTProcessingMatlabOutput.h"

namespace TC::HTProcessingMatlab {
    class TCHTProcessingMatlab_API IHTProcessingMatlabOutputPort {
    public:
        typedef IHTProcessingMatlabOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        IHTProcessingMatlabOutputPort() = default;
        virtual ~IHTProcessingMatlabOutputPort() = default;

        virtual auto SetOutput(const HTProcessingMatlabOutput& htProcessingMatlabOutput)->void = 0;
    };
}