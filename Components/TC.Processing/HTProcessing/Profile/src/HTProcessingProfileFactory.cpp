#include "HTProcessingProfileFactory.h"
#include <limits>

namespace TC::HTProcessingProfile {
    auto HTProcessingProfileFactory::Generate_v1_4_1_c(const Input_v1_4_1_c& input) -> HTProcessingProfile_v1_4_1_c {
        HTProcessingProfile_v1_4_1_c profile;

        const auto& naCond = input.naCond;
        if (naCond > 0.6) {
            profile.p01SupportDC = 0.2f;
            profile.p02SupportDC = 1;
        } else if (naCond > 0.5) {
            profile.p01SupportDC = 0.2f;
            profile.p02SupportDC = 1;
        } else {
            profile.p01SupportDC = 0.8f;
            profile.p02SupportDC = 1;
        }

        if (naCond < 0.4) {
            profile.p01OuterIterNum = 100;
            profile.p01InnerIterNum = 50;
            profile.p01NormalizationFactor = 0.1f;
            profile.p02NormalizationFactor = 0.15f;
            profile.p01SzRatioAxi = 0.8f;
            profile.p01SzRatioLat = 0.4f;
        } else if (naCond < 0.5) {
            profile.p01OuterIterNum = 100;
            profile.p01InnerIterNum = 50;
            profile.p01NormalizationFactor = 0.1f;
            profile.p02NormalizationFactor = 0.15f;
            profile.p01SzRatioAxi = 0.8f;
            profile.p01SzRatioLat = 0.35f;
        } else if (naCond < 0.6) {
            profile.p01OuterIterNum = 100;
            profile.p01InnerIterNum = 50;
            profile.p01NormalizationFactor = 0.15f;
            profile.p02NormalizationFactor = 0.15f;
            profile.p01SzRatioAxi = 0.8f;
            profile.p01SzRatioLat = 0.3f;
        } else {
            profile.p01OuterIterNum = 80;
            profile.p01InnerIterNum = 40;
            profile.p01NormalizationFactor = 0.2f;
            profile.p02NormalizationFactor = 0.2f;
            profile.p01SzRatioAxi = 0.8f;
            profile.p01SzRatioLat = 0.3f;
        }

        profile.p01TvParam = 0.00024f;
        profile.p01NonNegRef = 0;

        profile.p02OuterIterNum = 8;
        profile.p02InnerIterNum = 5;
        profile.p02TvParam = 3.6e-3f;
        profile.p02NonNegRef = -std::numeric_limits<float>::infinity();

        profile.resetGpParam4LastIter = true;

        profile.lateralMinSizeOffset = -30;
        profile.sfcGap01 = 0.1137f;
        profile.sfcGap02 = 0.1137f;
        profile.sfcGap03 = 0.1065f;

        profile.p02PreserveOriginalSfcData = true;
        profile.memoryUtilizationRatio = 1.7f;

        return profile;
    }

    auto HTProcessingProfileFactory::Generate_v1_4_1_d(const Input_v1_4_1_d& input) -> HTProcessingProfile_v1_4_1_d {
        const auto profile_v1_4_1_c = Generate_v1_4_1_c(Input_v1_4_1_c{ input.naCond });

        // only difference with v1.4.1c is enableRegularization parameter
        HTProcessingProfile_v1_4_1_d profile;
        profile.enableRegularization = input.enableRegularization;
        profile.p01SupportDC = profile_v1_4_1_c.p01SupportDC;
        profile.p02SupportDC = profile_v1_4_1_c.p02SupportDC;
        profile.p01NonNegRef = profile_v1_4_1_c.p01NonNegRef;
        profile.p02NonNegRef = profile_v1_4_1_c.p02NonNegRef;
        profile.p01OuterIterNum = profile_v1_4_1_c.p01OuterIterNum;
        profile.p01InnerIterNum = profile_v1_4_1_c.p01InnerIterNum;
        profile.p02OuterIterNum = profile_v1_4_1_c.p02OuterIterNum;
        profile.p02InnerIterNum = profile_v1_4_1_c.p02InnerIterNum;
        profile.p01TvParam = profile_v1_4_1_c.p01TvParam;
        profile.p02TvParam = profile_v1_4_1_c.p02TvParam;
        profile.resetGpParam4LastIter = profile_v1_4_1_c.resetGpParam4LastIter;
        profile.p01SzRatioAxi = profile_v1_4_1_c.p01SzRatioAxi;
        profile.p01SzRatioLat = profile_v1_4_1_c.p01SzRatioLat;
        profile.lateralMinSizeOffset = profile_v1_4_1_c.lateralMinSizeOffset;
        profile.sfcGap01 = profile_v1_4_1_c.sfcGap01;
        profile.sfcGap02 = profile_v1_4_1_c.sfcGap02;
        profile.sfcGap03 = profile_v1_4_1_c.sfcGap03;
        profile.p01NormalizationFactor = profile_v1_4_1_c.p01NormalizationFactor;
        profile.p02NormalizationFactor = profile_v1_4_1_c.p02NormalizationFactor;
        profile.p02PreserveOriginalSfcData = profile_v1_4_1_c.p02PreserveOriginalSfcData;
        profile.memoryUtilizationRatio = profile_v1_4_1_c.memoryUtilizationRatio;

        return profile;
    }
}
