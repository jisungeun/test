#pragma once

#include "TCHTProcessingProfileExport.h"

#include "HTProcessingProfile_v1_4_1_c.h"
#include "HTProcessingProfile_v1_4_1_d.h"

namespace TC::HTProcessingProfile {
    class TCHTProcessingProfile_API HTProcessingProfileFactory {
    public:
        struct Input_v1_4_1_c {
            double naCond{};
        };

        struct Input_v1_4_1_d {
            bool enableRegularization{};
            double naCond{};
        };

        static auto Generate_v1_4_1_c(const Input_v1_4_1_c& input)->HTProcessingProfile_v1_4_1_c;
        static auto Generate_v1_4_1_d(const Input_v1_4_1_d& input)->HTProcessingProfile_v1_4_1_d;
    };
}
