#pragma once

#include <enum.h>

namespace TC::HTProcessingProfile {
    BETTER_ENUM(HTProcessingProfileVersion, uint8_t, v1_4_1_c, v1_4_1_d);
}

