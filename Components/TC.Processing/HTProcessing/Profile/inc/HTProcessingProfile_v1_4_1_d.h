#pragma once

#include <cstdint>
#include "TCHTProcessingProfileExport.h"

namespace TC::HTProcessingProfile {
    struct TCHTProcessingProfile_API HTProcessingProfile_v1_4_1_d {
        bool enableRegularization{};
        float p01SupportDC{};
        float p02SupportDC{};
        float p01NonNegRef{};
        float p02NonNegRef{};
        int32_t p01OuterIterNum{};
        int32_t p01InnerIterNum{};
        int32_t p02OuterIterNum{};
        int32_t p02InnerIterNum{};
        float p01TvParam{};
        float p02TvParam{};
        bool resetGpParam4LastIter{};
        float p01SzRatioAxi{};
        float p01SzRatioLat{};
        int32_t lateralMinSizeOffset{};
        float sfcGap01{};
        float sfcGap02{};
        float sfcGap03{};
        float p01NormalizationFactor{};
        float p02NormalizationFactor{};
        bool p02PreserveOriginalSfcData{};
        float memoryUtilizationRatio{};
    };
}