#include "TCNiftiWriter.h"

#include <iostream>
#include <sstream>
#include <iomanip>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkNiftiImageIO.h>
#include <itkImportImageFilter.h>
#include <itkLabelImageToLabelMapFilter.h>
#include <itkLabelMapToLabelImageFilter.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkResampleImageFilter.h>
#include <itkScaleTransform.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkExtractImageFilter.h>
#include <itkChangeInformationImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkConstantPadImageFilter.h>

#include <direct.h>

#include "H5Cpp.h"

namespace TC::IO {

	using ScalarType = double;

	using PixelType = unsigned short;
	constexpr unsigned int Dimension = 3;
	using ImageType = itk::Image<PixelType, Dimension>;

	using FloatPixelType = float;
	using FloatType = itk::Image < FloatPixelType, Dimension>;

	using LongPixelType = long long;
	using LongType = itk::Image <LongPixelType, Dimension>;		

	using ByteType = itk::Image <uint8_t, Dimension>;

	using ImportFilterType = itk::ImportImageFilter<PixelType, Dimension>;
	using ImportFloatType = itk::ImportImageFilter<FloatPixelType, Dimension>;
	using ImportLongType = itk::ImportImageFilter<LongPixelType, Dimension>;
	using ImportByteType = itk::ImportImageFilter <uint8_t, Dimension>;

	using LabelObjectType = itk::LabelObject<PixelType, Dimension>;
	using LabelMapType = itk::LabelMap<LabelObjectType>;

	using LabelImageToLabelMapFilterType = itk::LabelImageToLabelMapFilter<FloatType, LabelMapType>;


	using LabelMapToLabelImageFilterType = itk::LabelMapToLabelImageFilter<LabelMapType, ImageType>;

	using LabelWriterType = itk::ImageFileWriter<ImageType>;

	using LongInterpolatorType = itk::LinearInterpolateImageFunction<LongType, double>;
	using FloatInterpolatorType = itk::LinearInterpolateImageFunction<FloatType, double>;
	using LinearInterpolatorType = itk::LinearInterpolateImageFunction<ImageType, double>;

	using ThresholdType = itk::BinaryThresholdImageFilter<FloatType, ImageType>;

	using ResampleLongType = itk::ResampleImageFilter<LongType, LongType>;
	using ResampleFloatType = itk::ResampleImageFilter<FloatType, FloatType>;
	//do resample for Tcf data only
	using ResampleFilterType = itk::ResampleImageFilter<ImageType, ImageType>;
	using TransformPrecisionType = double;
	using TransformType = itk::ScaleTransform<TransformPrecisionType, Dimension>;
	using CropType = itk::ExtractImageFilter<FloatType, FloatType>;
	using InfoType = itk::ChangeInformationImageFilter<FloatType>;
	using InfoZType = itk::ChangeInformationImageFilter<ImageType>;
	using IntensityType = itk::RescaleIntensityImageFilter<ImageType, ImageType>;

	using CropZType = itk::ExtractImageFilter<ImageType, ImageType>;
	using PadZType = itk::ConstantPadImageFilter<ImageType, ImageType>;
	using PadZTypef = itk::ConstantPadImageFilter<FloatType, FloatType>;
	using PadZTypel = itk::ConstantPadImageFilter<LongType, FloatType>;

	struct TCNiftiWriter::Impl {
		std::string image_path;
		std::string image_name;

		//import usigned short type tcf image
		ImportFilterType::Pointer importFilter;
		//import float AI generated image
		ImportFloatType::Pointer originFilter;
		//import long AI generated image
		ImportLongType::Pointer longFilter;

		ResampleFilterType::Pointer resizeFilter;

		IntensityType::Pointer intensityFilter;

		ThresholdType::Pointer thresholdFilter;

		CropType::Pointer cropFilter;
		CropZType::Pointer cropZFilter;
		InfoZType::Pointer infoZFilter;
		PadZType::Pointer padZFilter;
		PadZTypef::Pointer padZFilterf;
		PadZTypel::Pointer padZFilterl;

		InfoType::Pointer infoFilter;
		InfoZType::Pointer infoFFilter;

		int dimension[3];
		double origin[3];
		double spacing[3];
		int st_dim[3];

		int rsource_dim[3];
		int rtarget_dim[3];

		int time_step = { 0 };
		int data_count = { 1 };

		int cur_cropZ;

		unsigned short* original_image;
	};

	template <typename T>
	T loadAttribute(H5::Group& group, const std::string& name)
	{
		H5::Attribute attr = group.openAttribute(name);
		H5::DataType type = attr.getDataType();

		T value;
		attr.read(type, &value);
		return value;
	}

	template <typename RIDataType>
	auto readArray(H5::PredType h5ReadType, H5::DataSet& data, int64_t* const shape)->RIDataType* {
		H5::IntType intype = data.getIntType();
		H5std_string order_string;
		H5T_order_t order = intype.getOrder(order_string);
		H5T_class_t type_class = data.getTypeClass();
		std::cout << "getTypeClass : " << type_class << " Memory Order : " << order_string << "\n";

		// This block is get shape(dimension) from Dataset.
		// if meta-data(attr from data_group) is always correct,
		// this block is non-necessary.
		H5::DataSpace dataspace = data.getSpace();
		int rank = dataspace.getSimpleExtentNdims();
		hsize_t* dims_out = new hsize_t[rank];
		int ndims = dataspace.getSimpleExtentDims(dims_out, NULL);
		std::cout << "Rank : " << rank << " Ndim : " << ndims << "\n";
		std::cout << "Shape / Dims_out : ";
		for (int i = 0; i < rank; i++) {
			// if meta-data and dims_out is different,
			// it must be error. But it was replace to simple print.
			std::cout << shape[i] << "/" << dims_out[i] << " ";
			shape[i] = dims_out[i];
		}
		std::cout << "\n";

		RIDataType* H5_output = new RIDataType[shape[0] * shape[1] * shape[2]];
		data.read(H5_output, h5ReadType);
		return H5_output;
	}

	extern auto readTCF_dw(const std::string& path, int64_t* const shape, int ts)->unsigned short* {
		H5::H5File file(path, H5F_ACC_RDONLY);
		auto data_group = file.openGroup("Data/3D");

		shape[0] = loadAttribute<int64_t>(data_group, "SizeZ");
		shape[1] = loadAttribute<int64_t>(data_group, "SizeX");
		shape[2] = loadAttribute<int64_t>(data_group, "SizeY");
		std::cout << "Shape(Z, X, Y) : " << shape[0] << shape[1] << shape[0] << "\n";

		//const std::string s = "000000";
		std::stringstream ss;
		ss << std::setw(6) << std::setfill('0') << ts;
		const std::string s = ss.str();
		std::cout << s << std::endl;
		H5::DataSet data_3d = data_group.openDataSet(s);
		auto img_arr = readArray<uint16_t>(H5::PredType::NATIVE_USHORT, data_3d, shape);
		std::cout << "Shape(Z, X, Y) : " << shape[0] << shape[1] << shape[0] << "\n";

		return img_arr;
	}


	TCNiftiWriter::TCNiftiWriter()
		:d(new Impl()) {
		for (int i = 0; i < 3; i++) {
			d->dimension[i] = 1;
			d->origin[i] = 0.0;
			d->spacing[i] = 1.0;
		}
	}

	TCNiftiWriter::~TCNiftiWriter() {
	}

	auto TCNiftiWriter::setImageName(std::string name) -> void {
		std::string fileWOext = name.substr(0, name.length() - 4);
		d->image_name = fileWOext;
	}

	auto TCNiftiWriter::setResizeSourceInfo(int dims[3])->void {
		for (int i = 0; i < 3; i++) d->rsource_dim[i] = dims[i];
	}

	auto TCNiftiWriter::setResizeTargetInfo(int dims[3])->void {
		for (int i = 0; i < 3; i++) d->rtarget_dim[i] = dims[i];
	}

	auto TCNiftiWriter::importImageTest(void* buf) -> void {
		d->importFilter = ImportFilterType::New();
		ImportFloatType::SizeType size;

		size[0] = d->dimension[0];
		size[1] = d->dimension[1];
		size[2] = d->dimension[2];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		d->importFilter->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = {0,0,0};
		d->importFilter->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		d->importFilter->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		d->importFilter->SetImportPointer((unsigned short*)buf, numberOfPixels, true);
		LabelWriterType::Pointer writer =
			LabelWriterType::New();
		writer->SetFileName("test.nii.gz");			
		writer->SetInput(d->importFilter->GetOutput());
	    writer->Update();		

    }
	auto TCNiftiWriter::importImageTest2(void* buf) -> void {
		auto import = ImportLongType::New();

		ImportFloatType::SizeType size;

		size[0] = d->dimension[0];
		size[1] = d->dimension[1];
		size[2] = d->dimension[2];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		import->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { 0,0,0 };
		import->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		import->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		import->SetImportPointer((long long*)buf, numberOfPixels, true);
		
		auto writer = itk::ImageFileWriter<LongType>::New();
		writer->SetFileName("test2.nii.gz");
		writer->SetInput(import->GetOutput());
		writer->Update();

	}
	auto TCNiftiWriter::importImageTest3(void* buf,std::string nams) -> void {
		auto import = ImportFloatType::New();

		ImportFloatType::SizeType size;

		size[0] = d->dimension[0];
		size[1] = d->dimension[1];
		size[2] = d->dimension[2];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		import->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { 0,0,0 };
		import->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		import->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		import->SetImportPointer((float*)buf, numberOfPixels, true);
		auto writer = itk::ImageFileWriter < FloatType > ::New();
		auto name = nams + ".nii.gz";
	    writer->SetFileName(name);
		writer->SetInput(import->GetOutput());
		writer->Update();		
	}

	auto TCNiftiWriter::importImageTest4(void* buf, std::string name) -> void {
		auto import = ImportByteType::New();

		ImportByteType::SizeType size;

		size[0] = d->dimension[0];
		size[1] = d->dimension[1];
		size[2] = d->dimension[2];

		ImportByteType::IndexType start;
		start.Fill(0);

		ImportByteType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		import->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { 0,0,0 };
		import->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		import->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		import->SetImportPointer((uint8_t*)buf, numberOfPixels, true);
		auto writer = itk::ImageFileWriter < ByteType > ::New();
		auto n = name + ".nii.gz";
		writer->SetFileName(n);		
		writer->SetInput(import->GetOutput());
		writer->Update();
    }


	auto TCNiftiWriter::rescaleImage(void) -> void {
		d->intensityFilter = IntensityType::New();
		d->intensityFilter->SetInput(d->importFilter->GetOutput());
		d->intensityFilter->SetOutputMinimum(0);
		d->intensityFilter->SetOutputMaximum(10000);

		d->intensityFilter->Update();
	}

	auto TCNiftiWriter::setTimeStep(int s) -> void {
		d->time_step = s;
	}

	auto TCNiftiWriter::resizeImage(bool isZCrop)->void {
		LinearInterpolatorType::Pointer linear = LinearInterpolatorType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->rtarget_dim[i];

		d->resizeFilter = ResampleFilterType::New();
		d->resizeFilter->SetInterpolator(linear);
		if (isZCrop)
			d->resizeFilter->SetInput(d->infoZFilter->GetOutput());
		else
			d->resizeFilter->SetInput(d->importFilter->GetOutput());
		d->resizeFilter->SetSize(size);
		d->resizeFilter->SetOutputSpacing(d->spacing);
		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		for (int i = 0; i < 3; i++) param[i] = (double)d->dimension[i] / (double)d->rtarget_dim[i];
		if (isZCrop)
			param[2] = (double)d->cur_cropZ / (double)d->rtarget_dim[2];

		transform->SetParameters(param);
		d->resizeFilter->SetTransform(transform);
		d->resizeFilter->Update();
		/*
		constexpr char* fname = "D:/tss_build/bin/Debug/data/FloatImage.nii.gz";
		itk::ImageFileReader<FloatType>::Pointer ww =
			itk::ImageFileReader<FloatType>::New();
		//ww->SetInput(d->resizeFilter->GetOutput());
		ww->SetFileName(fname);
		ww->Update();
		std::cout << ww->GetOutput()->GetSpacing() << std::endl;*/

		d->intensityFilter = IntensityType::New();
		d->intensityFilter->SetInput(d->resizeFilter->GetOutput());
		d->intensityFilter->SetOutputMinimum(0);
		d->intensityFilter->SetOutputMaximum(10000);

		d->intensityFilter->Update();
	}

	auto TCNiftiWriter::setStitchDimension(int dim[3]) -> void {
		d->st_dim[0] = dim[0];
		d->st_dim[1] = dim[1];
		d->st_dim[2] = dim[2];
	}


	auto TCNiftiWriter::getResizedImageArr(void) -> unsigned short* {
		return d->intensityFilter->GetOutput()->GetBufferPointer();
	}

	auto TCNiftiWriter::readOriginalTcf(std::string path) -> void {
		int64_t shape[3];
		d->importFilter = ImportFilterType::New();
		ImportFloatType::SizeType size;

		size[0] = d->dimension[0];
		size[1] = d->dimension[1];
		size[2] = d->dimension[2];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		d->importFilter->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { d->origin[0],d->origin[1], d->origin[2] };
		d->importFilter->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		d->importFilter->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];

		const bool importImageFilterWillOwnTheBuffer = true;
		auto buf = readTCF_dw(path, shape, d->time_step);
		d->importFilter->SetImportPointer(buf, numberOfPixels, importImageFilterWillOwnTheBuffer);
		LabelWriterType::Pointer writer =
			LabelWriterType::New();
		_mkdir(d->image_path.c_str());
		if (d->data_count > 1) {
			writer->SetFileName(d->image_path + d->image_name +
								std::string("-ts") +
								std::to_string(d->time_step) +
								std::string(".nii.gz"));
		} else {
			writer->SetFileName(d->image_path + d->image_name + std::string(".nii.gz"));
		}
		writer->SetInput(d->importFilter->GetOutput());
		try {
			writer->Update();
		} catch (itk::ExceptionObject& error)
		{
			std::cerr << "Error: " << error << std::endl;
			return;
		}
	}


	auto TCNiftiWriter::setImagePath(std::string path) -> void {
		d->image_path = path;
	}

	auto TCNiftiWriter::setDimension(int dims[3])->void {
		for (int i = 0; i < 3; i++) d->dimension[i] = dims[i];
	}
	auto TCNiftiWriter::setSpacing(double spacing[3])->void {
		for (int i = 0; i < 3; i++) d->spacing[i] = spacing[i];
	}
	auto TCNiftiWriter::setOrigin(double origin[3])->void {
		for (int i = 0; i < 3; i++) d->origin[i] = origin[i];
	}

	auto TCNiftiWriter::convertLabelImage(std::string name, bool isZCrop)->void {
		LabelImageToLabelMapFilterType::Pointer toLabelMap =
			LabelImageToLabelMapFilterType::New();
		if (isZCrop) {
			toLabelMap->SetInput(d->infoFilter->GetOutput());
		} else
			toLabelMap->SetInput(d->originFilter->GetOutput());
		toLabelMap->SetBackgroundValue(itk::NumericTraits<PixelType>::Zero);
		//toLabelMap->Update();

		LabelMapToLabelImageFilterType::Pointer labelImageConverter =
			LabelMapToLabelImageFilterType::New();
		labelImageConverter->SetInput(toLabelMap->GetOutput());

		//upsample size into original image
		LinearInterpolatorType::Pointer linear = LinearInterpolatorType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->dimension[i];
		ResampleFilterType::Pointer resizeFilter = ResampleFilterType::New();
		resizeFilter->SetInterpolator(linear);
		resizeFilter->SetInput(labelImageConverter->GetOutput());
		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		for (int i = 0; i < 3; i++) param[i] = (double)d->rtarget_dim[i] / (double)d->dimension[i];
		if (isZCrop) param[2] = 1.0;
		transform->SetParameters(param);
		resizeFilter->SetTransform(transform);
		resizeFilter->SetOutputOrigin(d->origin);
		resizeFilter->SetOutputSpacing(d->spacing);
		resizeFilter->SetSize(size);

		//write image
		LabelWriterType::Pointer writer =
			LabelWriterType::New();
		_mkdir(d->image_path.c_str());
		if (d->data_count > 1) {
			writer->SetFileName(d->image_path + d->image_name + std::string("-ts") +
								std::to_string(d->time_step) + std::string("_seg_") +
								name + std::string(".nii.gz"));
		} else {
			writer->SetFileName(d->image_path + d->image_name + std::string("_seg_") +
								name + std::string(".nii.gz"));
		}
		writer->SetInput(resizeFilter->GetOutput());
		try {
			writer->Update();
		} catch (itk::ExceptionObject& error)
		{
			std::cerr << "Error: " << error << std::endl;
			return;
		}
	}

	auto TCNiftiWriter::padSingleLongZ(int zCrop, bool isTarget)->void {
		LongInterpolatorType::Pointer linear = LongInterpolatorType::New();
		ImageType::SizeType size;
		if (isTarget)
			for (int i = 0; i < 3; i++) size[i] = d->rtarget_dim[i];
		else
			for (int i = 0; i < 3; i++) size[i] = d->dimension[i];
		size[2] = zCrop;
		ResampleLongType::Pointer resizeFilter = ResampleLongType::New();
		resizeFilter->SetInterpolator(linear);
		resizeFilter->SetInput(d->longFilter->GetOutput());
		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		//for (int i = 0; i < 3; i++) param[i] = (double)d->rtarget_dim[i] / (double)d->dimension[i];
		//if (isZCrop) param[2] = 1.0;
		param[0] = param[1] = 1.0;
		param[2] = (double)d->rtarget_dim[2] / (double)zCrop;
		transform->SetParameters(param);
		resizeFilter->SetTransform(transform);
		resizeFilter->SetOutputOrigin(d->origin);
		resizeFilter->SetOutputSpacing(d->spacing);
		resizeFilter->SetSize(size);

		d->padZFilterl = PadZTypel::New();
		ImageType::SizeType lower;
		lower[0] = lower[1] = 0;
		lower[2] = (d->dimension[2] - zCrop) / 2;

		ImageType::SizeType upper;
		upper[0] = upper[1] = 0;
		upper[2] = (d->dimension[2] - zCrop) / 2;

		d->padZFilterl->SetConstant(0);
		d->padZFilterl->SetPadUpperBound(upper);
		d->padZFilterl->SetPadLowerBound(lower);
		d->padZFilterl->SetInput(resizeFilter->GetOutput());

		d->infoFilter = InfoType::New();
		ImageType::PointType origin;
		for (int i = 0; i < 3; i++) origin[i] = d->origin[i];

		ImageType::PointType::VectorType translation;
		translation[0] = 0;
		translation[1] = 0;
		translation[2] = -(d->dimension[2] - zCrop) / 2 * d->spacing[2];

		for (int i = 0; i < 3; i++) origin[i] -= translation[i];

		d->infoFilter->SetInput(d->padZFilterl->GetOutput());
		d->infoFilter->SetChangeOrigin(true);
		d->infoFilter->SetOutputOrigin(origin);
		d->infoFilter->Update();
	}

	auto TCNiftiWriter::padSingleImageZ(int zCrop) -> void {
		FloatInterpolatorType::Pointer linear = FloatInterpolatorType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->dimension[i];
		size[2] = zCrop;
		ResampleFloatType::Pointer resizeFilter = ResampleFloatType::New();
		resizeFilter->SetInterpolator(linear);
		resizeFilter->SetInput(d->originFilter->GetOutput());
		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		//for (int i = 0; i < 3; i++) param[i] = (double)d->rtarget_dim[i] / (double)d->dimension[i];
		//if (isZCrop) param[2] = 1.0;
		param[0] = param[1] = 1.0;
		param[2] = (double)d->rtarget_dim[2] / (double)zCrop;
		transform->SetParameters(param);
		resizeFilter->SetTransform(transform);
		resizeFilter->SetOutputOrigin(d->origin);
		resizeFilter->SetOutputSpacing(d->spacing);
		resizeFilter->SetSize(size);

		d->padZFilterf = PadZTypef::New();
		ImageType::SizeType lower;
		lower[0] = lower[1] = 0;
		lower[2] = (d->dimension[2] - zCrop) / 2;

		ImageType::SizeType upper;
		upper[0] = upper[1] = 0;
		upper[2] = (d->dimension[2] - zCrop) / 2;

		d->padZFilterf->SetConstant(0);
		d->padZFilterf->SetPadUpperBound(upper);
		d->padZFilterf->SetPadLowerBound(lower);
		d->padZFilterf->SetInput(resizeFilter->GetOutput());

		d->infoFilter = InfoType::New();
		ImageType::PointType origin;
		for (int i = 0; i < 3; i++) origin[i] = d->origin[i];

		ImageType::PointType::VectorType translation;
		translation[0] = 0;
		translation[1] = 0;
		translation[2] = -(d->dimension[2] - zCrop) / 2 * d->spacing[2];

		for (int i = 0; i < 3; i++) origin[i] -= translation[i];

		d->infoFilter->SetInput(d->padZFilterf->GetOutput());
		d->infoFilter->SetChangeOrigin(true);
		d->infoFilter->SetOutputOrigin(origin);
		d->infoFilter->Update();
	}


	auto TCNiftiWriter::padImageZ(int zCrop)->void {
		LinearInterpolatorType::Pointer linear = LinearInterpolatorType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->dimension[i];
		size[2] = zCrop;
		ResampleFilterType::Pointer resizeFilter = ResampleFilterType::New();
		resizeFilter->SetInterpolator(linear);
		resizeFilter->SetInput(d->thresholdFilter->GetOutput());
		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		for (int i = 0; i < 3; i++) param[i] = (double)d->rtarget_dim[i] / (double)d->dimension[i];
		//if (isZCrop) param[2] = 1.0;	
		param[2] = (double)d->rtarget_dim[2] / (double)zCrop;
		transform->SetParameters(param);
		resizeFilter->SetTransform(transform);
		resizeFilter->SetOutputOrigin(d->origin);
		resizeFilter->SetOutputSpacing(d->spacing);
		resizeFilter->SetSize(size);

		d->padZFilter = PadZType::New();
		ImageType::SizeType lower;
		lower[0] = lower[1] = 0;
		lower[2] = (d->dimension[2] - zCrop) / 2;

		ImageType::SizeType upper;
		upper[0] = upper[1] = 0;
		upper[2] = (d->dimension[2] - zCrop) / 2;

		d->padZFilter->SetConstant(0);
		d->padZFilter->SetPadUpperBound(upper);
		d->padZFilter->SetPadLowerBound(lower);
		//d->padZFilter->SetInput(d->thresholdFilter->GetOutput());
		d->padZFilter->SetInput(resizeFilter->GetOutput());

		/*itk::ImageFileWriter<ImageType>::Pointer ff =
			itk::ImageFileWriter<ImageType>::New();
		ff->SetInput(d->padZFilter->GetOutput());
		ff->SetFileName("Padded.nii.gz");
		ff->Update();*/

		d->infoFFilter = InfoZType::New();
		//translate image to original iamge
		ImageType::PointType origin;
		for (int i = 0; i < 3; i++) origin[i] = d->origin[i];

		ImageType::PointType::VectorType translation;
		//translation[0] = (d->st_dim[0] - d->rtarget_dim[0]) / 2 * d->spacing[0];
		//translation[1] = (d->st_dim[1] - d->rtarget_dim[1]) / 2 * d->spacing[1];
		translation[0] = 0;// (d->dimension[0] - d->rtarget_dim[0]) / 2 * d->spacing[0];
		translation[1] = 0;// (d->dimension[1] - d->rtarget_dim[1]) / 2 * d->spacing[1];
		translation[2] = -(d->dimension[2] - zCrop) / 2 * d->spacing[2];

		for (int i = 0; i < 3; i++) origin[i] -= translation[i];

		d->infoFFilter->SetInput(d->padZFilter->GetOutput());
		d->infoFFilter->SetChangeOrigin(true);
		d->infoFFilter->SetOutputOrigin(origin);

		itk::ImageFileWriter<ImageType>::Pointer ssf =
			itk::ImageFileWriter<ImageType>::New();
		ssf->SetInput(d->infoFFilter->GetOutput());
		ssf->SetFileName("padded.nii.gz");
		ssf->Update();
	}

	auto TCNiftiWriter::cropImageZ(int zCrop) ->void {
		d->cur_cropZ = zCrop;
		d->cropZFilter = CropZType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 2; i++) size[i] = d->dimension[i];
		size[2] = zCrop;
		ImageType::IndexType start;
		start[0] = start[1] = 0;
		start[2] = (d->dimension[2] - zCrop) / 2;

		ImageType::RegionType region;
		region.SetSize(size);
		region.SetIndex(start);

		d->cropZFilter->SetInput(d->importFilter->GetOutput());
		d->cropZFilter->SetExtractionRegion(region);
		d->cropZFilter->SetDirectionCollapseToSubmatrix();

		d->infoZFilter = InfoZType::New();
		//translate image to original iamge
		ImageType::PointType origin;
		for (int i = 0; i < 3; i++) origin[i] = d->origin[i];
		//std::cout << d->cropFilter->GetOutput()->GetOrigin() << std::endl;;

		ImageType::PointType::VectorType translation;
		translation[0] = 0;
		translation[1] = 0;
		translation[2] = (d->dimension[2] - zCrop) / 2 * d->spacing[2];

		for (int i = 0; i < 3; i++) origin[i] -= translation[i];

		d->infoZFilter->SetInput(d->cropZFilter->GetOutput());
		d->infoZFilter->SetChangeOrigin(true);
		d->infoZFilter->SetOutputOrigin(origin);
	}
	auto TCNiftiWriter::cropImage() -> void {
		d->cropFilter = CropType::New();
		FloatType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->rtarget_dim[i];
		FloatType::IndexType start;
		start[0] = (d->st_dim[0] - d->rtarget_dim[0]) / 2;//(d->st_dim[0] - d->dimension[0]) / 2;
		start[1] = (d->st_dim[1] - d->rtarget_dim[1]) / 2;
		start[2] = 0;

		FloatType::RegionType region;
		region.SetSize(size);
		region.SetIndex(start);

		d->cropFilter->SetDirectionCollapseToSubmatrix();
		d->cropFilter->SetInput(d->originFilter->GetOutput());
		d->cropFilter->SetExtractionRegion(region);

		//translate image to original iamge
		FloatType::PointType origin;
		for (int i = 0; i < 3; i++) origin[i] = d->origin[i];
		d->infoFilter = InfoType::New();
		std::cout << d->cropFilter->GetOutput()->GetOrigin() << std::endl;;

		ImageType::PointType::VectorType translation;
		translation[0] = (d->st_dim[0] - d->rtarget_dim[0]) / 2 * d->spacing[0];
		translation[1] = (d->st_dim[1] - d->rtarget_dim[1]) / 2 * d->spacing[1];
		translation[2] = 0;// (d->st_dim[2] - d->rtarget_dim[2]) / 2 * d->spacing[2];

		for (int i = 0; i < 3; i++) origin[i] -= translation[i];

		d->infoFilter->SetInput(d->cropFilter->GetOutput());
		d->infoFilter->SetChangeOrigin(true);
		d->infoFilter->SetOutputOrigin(origin);

		itk::ImageFileWriter<FloatType>::Pointer ssf =
			itk::ImageFileWriter<FloatType>::New();
		ssf->SetInput(d->infoFilter->GetOutput());
		ssf->SetFileName("cropped.nii.gz");
		ssf->Update();
	}


	auto TCNiftiWriter::thresholdImage() -> void {
		d->thresholdFilter = ThresholdType::New();
		//d->thresholdFilter->SetInput(d->originFilter->GetOutput());
		d->thresholdFilter->SetInput(d->infoFilter->GetOutput());
		d->thresholdFilter->SetInsideValue(1);
		d->thresholdFilter->SetOutsideValue(0);
		d->thresholdFilter->SetLowerThreshold(0.5);

		itk::ImageFileWriter<ImageType>::Pointer ssf =
			itk::ImageFileWriter<ImageType>::New();
		ssf->SetInput(d->thresholdFilter->GetOutput());
		ssf->SetFileName("thresh.nii.gz");
		ssf->Update();
	}

	auto TCNiftiWriter::importImage2(void* buf)->void {//import original float image
		d->originFilter = ImportFloatType::New();//use custom dimension
		ImportFloatType::SizeType size;

		for (int i = 0; i < 3; i++) size[i] = d->st_dim[i];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		d->originFilter->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { d->origin[0],d->origin[1], d->origin[2] };
		d->originFilter->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		d->originFilter->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		const bool importImageFilterWillOwnTheBuffer = true;
		d->originFilter->SetImportPointer((float*)buf, numberOfPixels, importImageFilterWillOwnTheBuffer);
	}

	auto TCNiftiWriter::importImageLong(void* buf) -> void {
		d->longFilter = ImportLongType::New();
		ImportFloatType::SizeType size;

		for (int i = 0; i < 3; i++) size[i] = d->rtarget_dim[i];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		d->longFilter->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { d->origin[0],d->origin[1], d->origin[2] };
		d->longFilter->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		d->longFilter->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		const bool importImageFilterWillOwnTheBuffer = true;
		d->longFilter->SetImportPointer((long long*)buf, numberOfPixels, importImageFilterWillOwnTheBuffer);
	}


	auto TCNiftiWriter::importImage(void* buf)->void {//import original float image
		d->originFilter = ImportFloatType::New();
		ImportFloatType::SizeType size;

		for (int i = 0; i < 3; i++) size[i] = d->rtarget_dim[i];

		ImportFloatType::IndexType start;
		start.Fill(0);

		ImportFloatType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);

		d->originFilter->SetRegion(region);

		const itk::SpacePrecisionType origin[Dimension] = { d->origin[0],d->origin[1], d->origin[2] };
		d->originFilter->SetOrigin(origin);
		const itk::SpacePrecisionType spacing[Dimension] = { d->spacing[0],d->spacing[1],d->spacing[2] };
		d->originFilter->SetSpacing(spacing);

		const unsigned int numberOfPixels = size[0] * size[1] * size[2];
		const bool importImageFilterWillOwnTheBuffer = true;
		d->originFilter->SetImportPointer((float*)buf, numberOfPixels, importImageFilterWillOwnTheBuffer);

		/*itk::ImageFileWriter<FloatType>::Pointer writer =
			itk::ImageFileWriter<FloatType>::New();
		writer->SetImageIO(itk::NiftiImageIO::New());
		writer->SetFileName("Sample.nii.gz");
		writer->SetInput(d->originFilter->GetOutput());

		writer->Update();*/
	}

	auto TCNiftiWriter::writeImage(std::string name) -> void {//write unsinged short image
		//write image for multi organ segmentation
		_mkdir(d->image_path.c_str());
		std::string full_path;//
		if (d->data_count > 1) {
			full_path = d->image_path + d->image_name + "-ts" +
				std::to_string(d->time_step) + "_seg_" + name + ".nii.gz";
		} else {
			full_path = d->image_path + d->image_name + "_seg_" + name + ".nii.gz";
		}

		//upsample size into original image
		LinearInterpolatorType::Pointer linear = LinearInterpolatorType::New();
		ImageType::SizeType size;
		for (int i = 0; i < 3; i++) size[i] = d->dimension[i];
		ResampleFilterType::Pointer resizeFilter = ResampleFilterType::New();
		resizeFilter->SetInterpolator(linear);
		//resizeFilter->SetInput(d->thresholdFilter->GetOutput());
		//resizeFilter->SetInput(d->padZFilter->GetOutput());
		resizeFilter->SetInput(d->infoFFilter->GetOutput());

		TransformType::Pointer transform = TransformType::New();
		TransformType::ParametersType param = transform->GetParameters();
		for (int i = 0; i < 3; i++) param[i] = (double)d->rtarget_dim[i] / (double)d->dimension[i];
		param[2] = 1.0;

		ImageType::PointType origin;
		origin[0] = d->origin[0];
		origin[1] = d->origin[1];
		origin[2] = d->origin[2];

		transform->SetParameters(param);
		resizeFilter->SetTransform(transform);
		resizeFilter->SetOutputOrigin(origin);
		resizeFilter->SetOutputSpacing(d->spacing);
		resizeFilter->SetSize(size);

		itk::ImageFileWriter<ImageType>::Pointer writer =
			itk::ImageFileWriter<ImageType>::New();
		writer->SetImageIO(itk::NiftiImageIO::New());
		writer->SetFileName(full_path);
		//writer->SetInput(resizeFilter->GetOutput());
		writer->SetInput(d->infoFFilter->GetOutput());

		try {
			writer->Update();
		} catch (itk::ExceptionObject& error)
		{
			std::cerr << "Error: " << error << std::endl;
			return;
		}
	}
	auto TCNiftiWriter::setDataCount(int c) -> void {
		d->data_count = c;
	}
}