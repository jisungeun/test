#pragma once


#include <string>

#include <memory>

#include "TCNiftiIOExport.h"

namespace TC::IO {
	class TCNiftiIO_API TCNiftiWriter {
	public:
		TCNiftiWriter();
		~TCNiftiWriter();

		auto writeImage(std::string name)->void;
		auto importImage(void* buf)->void;
		auto importImage2(void* buf)->void;
		auto importImageLong(void* buf)->void;
		auto setDimension(int dims[3])->void;
		auto setStitchDimension(int dim[3])->void;
		auto setSpacing(double spacing[3])->void;
		auto setOrigin(double origin[3])->void;

		auto setImagePath(std::string path)->void;
		auto setImageName(std::string name)->void;
		auto convertLabelImage(std::string name, bool isZCrop)->void;

		auto setResizeSourceInfo(int dims[3])->void;
		auto setResizeTargetInfo(int dims[3])->void;
		auto getResizedImageArr(void)->unsigned short*;

		auto readOriginalTcf(std::string path)->void;
		auto resizeImage(bool isZCrop)->void;
		auto rescaleImage(void)->void;

		auto cropImageZ(int zCrop)->void;
		auto padImageZ(int zCrop)->void;
		auto padSingleImageZ(int zCrop)->void;
		auto padSingleLongZ(int zCrop, bool isTarget)->void;

		auto cropImage(void)->void;;
		auto thresholdImage(void)->void;

		auto setTimeStep(int s)->void;
		auto setDataCount(int c)->void;

		auto importImageTest(void* buf)->void;
		auto importImageTest2(void* buf)->void;
		auto importImageTest3(void* buf,std::string nams)->void;
		auto importImageTest4(void* buf, std::string name)->void;

	private:

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}