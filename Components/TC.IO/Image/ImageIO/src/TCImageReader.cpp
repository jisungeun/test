#include "TCImageReader.h"

#include <iostream>

#include "TCFMetaReader.h"
#include "TCFSimpleReader.h"

namespace TC::IO {
    TCImageReader::TCImageReader() {
        
    }
    TCImageReader::~TCImageReader() {
        
    }    
    auto TCImageReader::Read(const QString& path,int time_step) const -> TCImage::Pointer {
        auto metaReader = new TCFMetaReader;
        auto meta = metaReader->Read(path);
        auto vers = meta->common.formatVersion.split(".");
        auto minor_ver = vers[1].toInt();
        auto isFloat = (minor_ver < 3);

        std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

        auto reader = new TCFSimpleReader;
        reader->SetFilePath(path);        
        auto result = reader->ReadHT3D(time_step, isFloat);
        auto dim = result.GetDimension();
        auto res = result.GetResolution();

        int cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

        std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
        if (isFloat) {
            auto base = result.GetDataAsDouble();
            for (auto i = 0; i < cnt; i++) {
                arr.get()[i] = base.get()[i] * 10000.0;
            }
        }
        else {
            auto base = result.GetDataAsUInt16();
            memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));
        }

        img->SetImageVolume(arr);

        double rres[3];
        rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
        img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
        img->SetResolution(rres);

        delete metaReader;
        delete reader;

        return img;
    }
    auto TCImageReader::ReadMIP(const QString& path, int time_step) const -> TCImage::Pointer {
        auto metaReader = new TCFMetaReader;
        auto meta = metaReader->Read(path);
        auto vers = meta->common.formatVersion.split(".");
        auto minor_ver = vers[1].toInt();
        auto isFloat = (minor_ver < 3);

        std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

        auto reader = new TCFSimpleReader;
        reader->SetFilePath(path);
        auto result = reader->ReadHTMIP(time_step, isFloat);
        auto dim = result.GetDimension();
        auto res = result.GetResolution();

        int cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

        std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
        if (isFloat) {
            auto base = result.GetDataAsDouble();
            for (auto i = 0; i < cnt; i++) {
                arr.get()[i] = base.get()[i] * 10000.0;
            }
        }
        else {
            auto base = result.GetDataAsUInt16();
            memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));
        }

        img->SetImageVolume(arr);

        double rres[3];
        rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
        img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
        img->SetResolution(rres);

        delete metaReader;
        delete reader;

        return img;
    }
    auto TCImageReader::ReadFL(const QString& path, int ch, int time_step) const -> TCImage::Pointer {
        std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

        auto reader = new TCFSimpleReader;
        reader->SetFilePath(path);
        auto result = reader->ReadFL3D(ch, time_step);        
        auto dim = result.GetDimension();
        auto res = result.GetResolution();

        auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

        std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

        auto base = result.GetDataAsUInt16();
        memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));        

        img->SetImageVolume(arr);

        double rres[3];
        rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
        img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
        img->SetResolution(rres);        
                
        delete reader;

        return img;
    }
    auto TCImageReader::ReadFLMIP(const QString& path, int ch, int time_step) const -> TCImage::Pointer {
        std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

        auto reader = new TCFSimpleReader;
        reader->SetFilePath(path);
        auto result = reader->ReadFLMIP(ch, time_step);
        auto dim = result.GetDimension();
        auto res = result.GetResolution();

        auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

        std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

        auto base = result.GetDataAsUInt16();
        memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));

        img->SetImageVolume(arr);

        double rres[3];
        rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
        img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
        img->SetResolution(rres);
        
        delete reader;

        return img;
    }

}
