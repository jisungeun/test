#pragma once

#include <memory>

#include "TCImageIOExport.h"

namespace TC::IO {

	class TCImageIO_API TCImageWriter {
	public:
		explicit TCImageWriter();
		virtual ~TCImageWriter();
	};
}