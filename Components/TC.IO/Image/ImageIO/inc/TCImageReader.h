#pragma once

#include <memory>
#include <tuple>

#include <QStringList>

#include "TCImage.h"
#include "TCImageIOExport.h"

namespace TC::IO {
    class TCImageIO_API TCImageReader {
    public:
        explicit  TCImageReader();
        virtual ~TCImageReader();
                ;
        auto Read(const QString& path,int time_step = 0) const->TCImage::Pointer;
        auto ReadMIP(const QString& path, int time_step = 0) const->TCImage::Pointer;
        auto ReadFL(const QString& path, int ch, int time_step = 0)const->TCImage::Pointer;
        auto ReadFLMIP(const QString& path, int ch, int time_step = 0)const->TCImage::Pointer;
    };
}