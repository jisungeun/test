#include <catch2/catch.hpp>
#include "TCFThumbnailWriter.h"
namespace TCFThumbnailWriterTest {
    TEST_CASE("TCFThumbnailWriterTest") {
        SECTION("practical example") {
            //std::string tcfDataPath = "E:/00_Data/20210217_ TomoProcessingServer Validation/3.2.0a/20190827.135012.356.FullFOV-001/20190827.135012.356.FullFOV-001.TCF";
            //std::string tcfDataPath = "E:/00_Data/20210217_ TomoProcessingServer Validation/3.2.0a/20200602.135721.755.PoP_Exp2_WBC-008/20200602.135721.755.PoP_Exp2_WBC-008.TCF";
            //std::string tcfDataPath = "E:/00_Data/20210217_ TomoProcessingServer Validation/3.2.0a/20200604.165219.298.K562_exOVIS CART-002P008/20200604.165219.298.K562_exOVIS CART-002P008.TCF";
            //std::string tcfDataPath = "E:/00_Data/20210217_ TomoProcessingServer Validation/3.2.0a/20200624.154512.208.Single_WBC-035/20200624.154512.208.Single_WBC-035.TCF";
            //std::string tcfDataPath = "E:/00_Data/20210217_ TomoProcessingServer Validation/3.2.0a/20200918.162205.858.HeLa_dead-002/20200918.162205.858.HeLa_dead-002.TCF";
            //std::string tcfDataPath = "F:/00_Data/20190308_StitchedData25/20190219.132809.088.BrainTissue5x5HTHigh-002Stitching.TCF";
            //std::string tcfDataPath = "F:/00_Data/20190308_StitchedData25/20190219.132809.088.BrainTissue5x5HTHigh-002T001/20190219.132809.088.BrainTissue5x5HTHigh-002T001.TCF";
            std::string tcfDataPath = "C:/Users/HanghunJo/Downloads/20190906.154723.198.A549-actin-mCherry-2-004.TCF";

            std::string outputFolderPath = "C:/Users/HanghunJo/Desktop/oneMore";

            TC::IO::TCFThumbnailWriter tcfThumbnailWriter;
            tcfThumbnailWriter.SetTcfFilePath(tcfDataPath);
            tcfThumbnailWriter.SetOutputFolderPath(outputFolderPath);
            const auto writingResult = tcfThumbnailWriter.Write();

            CHECK(writingResult);
        }
    }
}
