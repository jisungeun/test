#pragma once
#include <iostream>
#include <memory>

class WritingImageFilePathGenerator {
public:
    WritingImageFilePathGenerator();
    ~WritingImageFilePathGenerator();

    auto SetOutputFolderPath(const std::string& outputFolderPath)->void;
    auto SetTcfFileName(const std::string& tcfFileName)->void;
    auto SetDataTypeName(const std::string& dataTypeName)->void;
    auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;

    auto Generate()->std::string;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};