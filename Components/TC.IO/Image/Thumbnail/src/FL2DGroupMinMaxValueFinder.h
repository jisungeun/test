#pragma once
#include <memory>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

class FL2DGroupMinMaxValueFinder {
public:
    FL2DGroupMinMaxValueFinder();
    ~FL2DGroupMinMaxValueFinder();

    auto SetFL2DGroup(H5::Group& fl2DGroup)->void;

    auto Find()->bool;

    auto GetMinValue()->float;
    auto GetMaxValue()->float;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};