#include "ThumbnailImageWriter.h"

#include "HDF5DataSetToOpenCVMatConverter.h"
#include "MatImageWriter.h"
#include "Utilities.h"
#include "WritingImageFilePathGenerator.h"

struct ThumbnailImageWriter::Impl {
	std::string tcfFileName;
	H5::Group* group = nullptr;
	std::string outputFolderPath;
	std::string type;

	auto GetImageSize() const -> unsigned long long {
		return group->getNumObjs();
	}

	auto GetImage(unsigned long long index) const -> cv::Mat {
		const auto timeframe = GenerateTimeFrameString(static_cast<int>(index));
		auto dataset = group->openDataSet(timeframe);

		HDF5DataSetToOpenCVMatConverter converter;
		converter.SetDataSet(dataset);
		converter.SetCustomNormalizationFlag(true);
		converter.SetCustomNormalizationValues(0, 255);

		auto result = converter.Convert();
		dataset.close();

		return result;
	}
};

ThumbnailImageWriter::ThumbnailImageWriter() : d(new Impl) {}

ThumbnailImageWriter::~ThumbnailImageWriter() = default;

auto ThumbnailImageWriter::SetTcfFileName(const std::string & tcfFileName) -> void {
	d->tcfFileName = tcfFileName;
}

auto ThumbnailImageWriter::SetGroup(H5::Group & Thumbnail) -> void {
	d->group = &Thumbnail;
}

auto ThumbnailImageWriter::SetOutputFolderPath(const std::string & outputFolderPath) -> void {
	d->outputFolderPath = outputFolderPath;
}

auto ThumbnailImageWriter::SetDataType(const std::string & type) -> void {
	d->type = type;
}

auto ThumbnailImageWriter::Write() -> bool {
	WritingImageFilePathGenerator pathGenerator;
	pathGenerator.SetTcfFileName(d->tcfFileName);
	pathGenerator.SetDataTypeName(d->type);
	pathGenerator.SetOutputFolderPath(d->outputFolderPath);

	if (d->group) {
		const auto size = d->GetImageSize();
		auto result = true;

		for (auto i = 0ull; i < size; i++) {
			auto mat = d->GetImage(i);
			const auto timeframe = GenerateTimeFrameString(static_cast<int>(i));
			auto dataset = d->group->openDataSet(timeframe);
			pathGenerator.SetTimeFrameIndex(i);

			MatImageWriter writer;
			writer.SetColor(COLOR);
			writer.SetMatImage(mat);
			writer.SetImageFilePath(pathGenerator.Generate());

			result = result && writer.Write();
		}

		return result;
	}

	return false;
}
