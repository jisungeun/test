#include "DataSetUpperGroupImageWriter.h"

#include "CvMatResizer.h"
#include "HDF5DataSetToOpenCVMatConverter.h"
#include "ImageBaseColor.h"
#include "MatImageWriter.h"
#include "WritingImageFilePathGenerator.h"
#include "DataSetUpperGroupMinMaxValueFinder.h"

struct DataSetUpperGroupImageWriter::Impl {
    Impl() = default;
    ~Impl() = default;

    std::string tcfFileName{};
    H5::Group* dataSet2DUpperGroup{};
    std::string outputFolderPath{};

    auto GenerateSuffix()->std::string;
    auto GenerateWritingImageFilePath(const std::string& dataSetName)->std::string;
    auto GenerateImageBaseColor()->ImageBaseColor;
};

auto DataSetUpperGroupImageWriter::Impl::GenerateSuffix() -> std::string {
    const auto dataSetName = this->dataSet2DUpperGroup->getObjName();

    std::string suffix;
    if (dataSetName == "/Data/2DMIP") {
        suffix = "HT";
    } else if (dataSetName == "/Data/2DFLMIP/CH0") {
        suffix = "FL-BLUE";
    } else if (dataSetName == "/Data/2DFLMIP/CH1") {
        suffix = "FL-GREEN";
    } else if (dataSetName == "/Data/2DFLMIP/CH2") {
        suffix = "FL-RED";
    } else if (dataSetName == "/Data/2D"){
        suffix = "PC";
    } else if (dataSetName == "/Data/BF") {
        suffix = "BF";
    } else {
        suffix = "SomethingWrong";
    }

    return suffix;
}

auto DataSetUpperGroupImageWriter::Impl::GenerateWritingImageFilePath(const std::string& dataSetName) -> std::string {
    WritingImageFilePathGenerator writingImageFilePathGenerator;
    writingImageFilePathGenerator.SetOutputFolderPath(outputFolderPath);
    writingImageFilePathGenerator.SetTcfFileName(tcfFileName);
    writingImageFilePathGenerator.SetDataTypeName(this->GenerateSuffix());
    writingImageFilePathGenerator.SetTimeFrameIndex(std::stoi(dataSetName));

    const auto writingImageFilePath = writingImageFilePathGenerator.Generate();
    return writingImageFilePath;
}

auto DataSetUpperGroupImageWriter::Impl::GenerateImageBaseColor() -> ImageBaseColor {
    const auto dataSetName = this->dataSet2DUpperGroup->getObjName();

    ImageBaseColor imageBaseColor;
    if (dataSetName == "/Data/2DMIP") {
        imageBaseColor = GRAY;
    } else if (dataSetName == "/Data/2DFLMIP/CH0") {
        imageBaseColor = BLUE;
    } else if (dataSetName == "/Data/2DFLMIP/CH1") {
        imageBaseColor = GREEN;
    } else if (dataSetName == "/Data/2DFLMIP/CH2") {
        imageBaseColor = RED;
    } else if (dataSetName == "/Data/2DFLMIP/BF") {
        imageBaseColor = COLOR;
    } else if (dataSetName == "/Data/2D"){
        imageBaseColor = GRAY;
    } else {
        imageBaseColor = GRAY;
    }

    return imageBaseColor;
}

DataSetUpperGroupImageWriter::DataSetUpperGroupImageWriter()
    : d(new Impl()) {
}

DataSetUpperGroupImageWriter::~DataSetUpperGroupImageWriter() = default;

auto DataSetUpperGroupImageWriter::SetTcfFileName(const std::string& tcfFileName) -> void {
    d->tcfFileName = tcfFileName;
}

auto DataSetUpperGroupImageWriter::SetDataSet2DUpperGroup(H5::Group& dataSet2DUpperGroup) -> void {
    d->dataSet2DUpperGroup = &dataSet2DUpperGroup;
}

auto DataSetUpperGroupImageWriter::SetOutputFolderPath(const std::string& outputFolderPath) -> void {
    d->outputFolderPath = outputFolderPath;
}

auto DataSetUpperGroupImageWriter::Write() -> bool {    
    auto dataSet2DUpperGroup = d->dataSet2DUpperGroup;

    HDF5DataSetToOpenCVMatConverter hdf5DataSetToOpenCvMatConverter;

    const auto imageBaseColor = d->GenerateImageBaseColor();

    MatImageWriter matImageWriter;
    matImageWriter.SetColor(imageBaseColor);

    auto writeResult = true;

    DataSetUpperGroupMinMaxValueFinder dataSetUpperGroupMinMaxValueFinder;
    dataSetUpperGroupMinMaxValueFinder.SetUpperGroup(*dataSet2DUpperGroup);
    const auto minMaxFindingIsGood = dataSetUpperGroupMinMaxValueFinder.Find();
    const auto minValue = dataSetUpperGroupMinMaxValueFinder.GetMinValue();
    const auto maxValue = dataSetUpperGroupMinMaxValueFinder.GetMaxValue();

    hdf5DataSetToOpenCvMatConverter.SetCustomNormalizationFlag(minMaxFindingIsGood);
    hdf5DataSetToOpenCvMatConverter.SetCustomNormalizationValues(minValue, maxValue);

    const auto objectNumber = dataSet2DUpperGroup->getNumObjs();
    for (auto objectIndex = 0; objectIndex < objectNumber; ++objectIndex) {
        const auto objectType = dataSet2DUpperGroup->getObjTypeByIdx(objectIndex);
        if (objectType != H5G_DATASET) {
            continue;
        }

        const auto dataSetName = dataSet2DUpperGroup->getObjnameByIdx(objectIndex);
        const auto writingImageFilePath = d->GenerateWritingImageFilePath(dataSetName);

        auto dataSet = dataSet2DUpperGroup->openDataSet(dataSetName);
        hdf5DataSetToOpenCvMatConverter.SetDataSet(dataSet);
        auto imageMat = hdf5DataSetToOpenCvMatConverter.Convert();
        dataSet.close();
        
        CvMatResizer resizer;
        resizer.SetMat(imageMat);
        resizer.SetMaxSize(1024);
        auto resized = resizer.Resize();

        matImageWriter.SetMatImage(resized);
        matImageWriter.SetImageFilePath(writingImageFilePath);
        
        const auto timelapseImageWritingResult = matImageWriter.Write();
        writeResult = writeResult && timelapseImageWritingResult;
    }

    dataSet2DUpperGroup->close();

    return writeResult;
}
