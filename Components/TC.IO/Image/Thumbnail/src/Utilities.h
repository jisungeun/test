#pragma once
#include <iostream>
#pragma warning(push)
#pragma warning(disable:4268)
#include "H5Cpp.h"
#pragma warning(pop)

struct DataSetSize {
    size_t x;
    size_t y;
    size_t z;
};

struct MinMaxValue {
    float minValue{};
    float maxValue{};
    bool valid{ false };
};

struct MinValue {
    float minValue{};
    bool valid{ false };
};

struct MaxValue {
    float maxValue{};
    bool valid{ false };
};

auto GetDataSetSize(const H5::DataSet& dataSet)->DataSetSize;

auto GenerateTimeFrameString(const int32_t& timeFrameIndex)->std::string;

auto FindMinMaxValue(const std::shared_ptr<uint16_t[]>& inputData, const size_t& numberOfElements)->MinMaxValue;
auto FindMinMaxValue(const std::shared_ptr<float[]>& inputData, const size_t& numberOfElements)->MinMaxValue;
auto FindMinMaxValue(const std::shared_ptr<uint8_t[]>& inputData, const size_t& numberOfElements)->MinMaxValue;

auto ReadMinValueAttribute(const H5::H5Object& targetObject, const std::string& minValueAttributeName) -> MinValue;
auto ReadMaxValueAttribute(const H5::H5Object& targetObject, const std::string& maxValueAttributeName) -> MaxValue;