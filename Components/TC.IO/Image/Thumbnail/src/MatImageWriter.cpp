#include "MatImageWriter.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core.hpp>
#pragma warning(pop)
#include "MatDataConverterToCV8UC3.h"

const int32_t channels = 3;

struct MatImageWriter::Impl {
    Impl() = default;
    ~Impl() = default;

    cv::Mat matImage{};
    ImageBaseColor imageBaseColor{};
    std::string imageFilePath{};

    auto MatchColor(const cv::Mat& imageMatCV8UC3)->cv::Mat;
};

auto MatImageWriter::Impl::MatchColor(const cv::Mat& imageMatCV8UC3) -> cv::Mat {
    cv::Mat channelsMat[channels];
    split(imageMatCV8UC3, channelsMat);

    auto& redMat = channelsMat[0];
    auto& greenMat = channelsMat[1];
    auto& blueMat = channelsMat[2];
    auto zeroMat = cv::Mat(channelsMat->rows, channelsMat->cols, channelsMat->type(), cv::Scalar{0});

    cv::Mat matchedMat;

    if(this->imageBaseColor == GRAY) {
        matchedMat = imageMatCV8UC3;
    } else if (this->imageBaseColor == BLUE) {
        cv::Mat mergingMat[channels] = { zeroMat, zeroMat, blueMat };
        merge(mergingMat, channels, matchedMat);
    } else if (this->imageBaseColor == GREEN) {
        cv::Mat mergingMat[channels] = { zeroMat, greenMat, zeroMat };
        merge(mergingMat, channels, matchedMat);
    } else if (this->imageBaseColor == RED) {
        cv::Mat mergingMat[channels] = { redMat, zeroMat, zeroMat };
        merge(mergingMat, channels, matchedMat);
    } else if (this->imageBaseColor == COLOR) {
        matchedMat = imageMatCV8UC3;
    } else {
        matchedMat = cv::Mat{};
    }

    return matchedMat;
}

MatImageWriter::MatImageWriter()
    : d(new Impl()) {
}

MatImageWriter::~MatImageWriter() = default;


auto MatImageWriter::SetMatImage(const cv::Mat& matImage) -> void {
    d->matImage = matImage;
}

auto MatImageWriter::SetColor(const ImageBaseColor& imageBaseColor) -> void {
    d->imageBaseColor = imageBaseColor;
}

auto MatImageWriter::SetImageFilePath(const std::string& imageFilePath) -> void {
    d->imageFilePath = imageFilePath;
}

auto MatImageWriter::Write() -> bool {
    MatDataConverterToCV8UC3 matDataConverterToCv8Uc3;
    matDataConverterToCv8Uc3.SetImageMat(d->matImage);

    const auto convertedImage = matDataConverterToCv8Uc3.Convert();
    const auto colorMatchedImage = d->MatchColor(convertedImage);

    return cv::imwrite(d->imageFilePath, colorMatchedImage);
}
