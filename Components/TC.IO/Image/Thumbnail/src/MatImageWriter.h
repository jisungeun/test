#pragma once
#include <memory>
#include <string>
#include "ImageBaseColor.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core/mat.hpp>
#pragma warning(pop)

class MatImageWriter {
public:
    MatImageWriter();
    ~MatImageWriter();

    auto SetMatImage(const cv::Mat& matImage)->void;
    auto SetColor(const ImageBaseColor& imageBaseColor)->void;
    auto SetImageFilePath(const std::string& imageFilePath)->void;

    auto Write()->bool;
public:
    struct Impl;
    std::unique_ptr<Impl> d;
};