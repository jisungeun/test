#include "TCFThumbnailWriter.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core.hpp>
#pragma warning(pop)

#include "DataSetUpperGroupImageWriter.h"
#include "FL2DGroupImageWriter.h"
#include "ThumbnailImageWriter.h"

namespace TC::IO {
	const std::string slashString = "/";

	const std::string htMipGroupName = "2DMIP";
	const std::string flMipGroupName = "2DFLMIP";
	const std::string bfGroupName = "BF";
	const std::string group2DName = "2D";

	const std::string thumbBfGroupName = "BF";
	const std::string thumbFlGroupName = "FL";
	const std::string thumbHtGroupName = "HT";

	const std::string blueGroupName = "CH0";
	const std::string greenGroupName = "CH1";
	const std::string redGroupName = "CH2";

	struct TCFThumbnailWriter::Impl {
		Impl() = default;
		~Impl() = default;

		std::string tcfFilePath{};
		std::string outputFolderPath{};

		auto CheckHdf5FileFormat()->bool;
		auto GetTCFFileNameFromTCFFilePath() const->std::string;
		static auto ReplaceBackslashToSlash(const std::string& filePath)->std::string;
		auto WriteHtMip(const H5::Group& dataGroup) -> bool;
		auto WriteFlMip(const H5::Group& dataGroup) -> bool;
		auto WriteBf(const H5::Group& dataGroup) -> bool;
		auto Write2D(const H5::Group& dataGroup) -> bool;

		auto WriteThumbBf(const H5::Group& dataGroup) -> bool;
		auto WriteThumbFl(const H5::Group& dataGroup) -> bool;
		auto WriteThumbHt(const H5::Group& dataGroup) -> bool;
	};

	auto TCFThumbnailWriter::Impl::CheckHdf5FileFormat() -> bool {
		return H5::H5File::isHdf5(this->tcfFilePath);
	}

	auto TCFThumbnailWriter::Impl::GetTCFFileNameFromTCFFilePath() const -> std::string {
		const auto m_tcfFilePath = this->ReplaceBackslashToSlash(this->tcfFilePath);
		const auto tcfFileIsInSubFolder = (m_tcfFilePath.find(slashString) != std::string::npos);

		std::string tcfFileName;

		if (tcfFileIsInSubFolder) {
			const auto lastSlashPosition = m_tcfFilePath.find_last_of(slashString);
			const auto tcfFileNameLength = m_tcfFilePath.length() - lastSlashPosition - 1;

			tcfFileName = m_tcfFilePath.substr(lastSlashPosition + 1, tcfFileNameLength - 4);
		} else {
			tcfFileName = this->tcfFilePath;
		}

		return tcfFileName;
	}

	auto TCFThumbnailWriter::Impl::ReplaceBackslashToSlash(const std::string& filePath) -> std::string {
		auto backslashReplacedFilePath = filePath;

		const std::string backslashString = "\\";
		auto backslashPosition = backslashReplacedFilePath.find(backslashString);

		while (backslashPosition != std::string::npos) {
			backslashReplacedFilePath.replace(backslashPosition, 1, slashString);
			backslashPosition = backslashReplacedFilePath.find(backslashString, backslashPosition);
		}

		return backslashReplacedFilePath;
	}

	auto TCFThumbnailWriter::Impl::WriteHtMip(const H5::Group& dataGroup) -> bool {
		auto htMipGroup = dataGroup.openGroup(htMipGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		DataSetUpperGroupImageWriter dataSetUpperGroupImageWriter;
		dataSetUpperGroupImageWriter.SetOutputFolderPath(outputFolderPath);
		dataSetUpperGroupImageWriter.SetTcfFileName(tcfFileName);
		dataSetUpperGroupImageWriter.SetDataSet2DUpperGroup(htMipGroup);
		const auto writingResult = dataSetUpperGroupImageWriter.Write();

		htMipGroup.close();

		return writingResult;
	}

	auto TCFThumbnailWriter::Impl::WriteFlMip(const H5::Group& dataGroup) -> bool {
		auto flMipGroup = dataGroup.openGroup(flMipGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		FL2DGroupImageWriter fl2DGroupImageWriter;
		fl2DGroupImageWriter.SetOutputFolderPath(outputFolderPath);
		fl2DGroupImageWriter.SetFL2DGroup(flMipGroup);
		fl2DGroupImageWriter.SetTcfFileName(tcfFileName);
		const auto writingResult = fl2DGroupImageWriter.Write();

		flMipGroup.close();

		return writingResult;
	}

	auto TCFThumbnailWriter::Impl::WriteBf(const H5::Group& dataGroup) -> bool {
		auto htMipGroup = dataGroup.openGroup(bfGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		DataSetUpperGroupImageWriter dataSetUpperGroupImageWriter;
		dataSetUpperGroupImageWriter.SetOutputFolderPath(outputFolderPath);
		dataSetUpperGroupImageWriter.SetTcfFileName(tcfFileName);
		dataSetUpperGroupImageWriter.SetDataSet2DUpperGroup(htMipGroup);
		const auto bfWritingResult = dataSetUpperGroupImageWriter.Write();

		htMipGroup.close();

		return bfWritingResult;
	}

	auto TCFThumbnailWriter::Impl::Write2D(const H5::Group& dataGroup) -> bool {
		auto group2D = dataGroup.openGroup(group2DName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		DataSetUpperGroupImageWriter dataSetUpperGroupImageWriter;
		dataSetUpperGroupImageWriter.SetOutputFolderPath(outputFolderPath);
		dataSetUpperGroupImageWriter.SetTcfFileName(tcfFileName);
		dataSetUpperGroupImageWriter.SetDataSet2DUpperGroup(group2D);
		const auto group2DWritingResult = dataSetUpperGroupImageWriter.Write();

		group2D.close();

		return group2DWritingResult;
	}

	auto TCFThumbnailWriter::Impl::WriteThumbBf(const H5::Group& dataGroup) -> bool {
		auto bfGroup = dataGroup.openGroup(thumbBfGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();


		ThumbnailImageWriter writer;
		writer.SetOutputFolderPath(outputFolderPath);
		writer.SetGroup(bfGroup);
		writer.SetTcfFileName(tcfFileName);
		writer.SetDataType(thumbBfGroupName);
		const auto writingResult = writer.Write();

		bfGroup.close();

		return writingResult;
	}

	auto TCFThumbnailWriter::Impl::WriteThumbFl(const H5::Group& dataGroup) -> bool {
		auto flMipGroup = dataGroup.openGroup(thumbFlGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		ThumbnailImageWriter writer;
		writer.SetOutputFolderPath(outputFolderPath);
		writer.SetGroup(flMipGroup);
		writer.SetTcfFileName(tcfFileName);
		writer.SetDataType(thumbFlGroupName);
		const auto writingResult = writer.Write();

		flMipGroup.close();

		return writingResult;
	}

	auto TCFThumbnailWriter::Impl::WriteThumbHt(const H5::Group& dataGroup) -> bool {
		auto htMipGroup = dataGroup.openGroup(thumbHtGroupName);
		const auto tcfFileName = GetTCFFileNameFromTCFFilePath();

		ThumbnailImageWriter writer;
		writer.SetOutputFolderPath(outputFolderPath);
		writer.SetGroup(htMipGroup);
		writer.SetTcfFileName(tcfFileName);
		writer.SetDataType(thumbHtGroupName);
		const auto writingResult = writer.Write();

		htMipGroup.close();

		return writingResult;
	}

	TCFThumbnailWriter::TCFThumbnailWriter()
		: d(new Impl()) {}

	TCFThumbnailWriter::~TCFThumbnailWriter() = default;

	auto TCFThumbnailWriter::SetTcfFilePath(const std::string& tcfFilePath) -> void {
		d->tcfFilePath = tcfFilePath;
	}

	auto TCFThumbnailWriter::SetOutputFolderPath(const std::string& outputFolderPath) -> void {
		d->outputFolderPath = d->ReplaceBackslashToSlash(outputFolderPath);
	}

	auto TCFThumbnailWriter::Write() -> bool {
		auto writingResult = true;
		try {
			if (!d->CheckHdf5FileFormat()) {
				return false;
			}

			H5::H5File tcfFile(d->tcfFilePath, H5F_ACC_RDONLY);

			auto dataGroup = tcfFile.openGroup("/Data");
			H5::Group thumbnailGroup;
			if (tcfFile.exists("Thumbnail"))
				thumbnailGroup = tcfFile.openGroup("/Thumbnail");


			const auto htMipExists = dataGroup.exists(htMipGroupName);
			if (htMipExists) {
				const auto thumbHtMipExists = thumbnailGroup.getId() > 0 && thumbnailGroup.exists(thumbHtGroupName);

				if (thumbHtMipExists) {
					const auto htWritingResult = d->WriteThumbHt(thumbnailGroup);
					writingResult = writingResult && htWritingResult;
				} else {
					const auto htWritingResult = d->WriteHtMip(dataGroup);
					writingResult = writingResult && htWritingResult;
				}
			}

			const auto flMipExists = dataGroup.exists(flMipGroupName);
			if (flMipExists) {
				const auto thumbFlMipExists = thumbnailGroup.getId() > 0 && thumbnailGroup.exists(thumbFlGroupName);

				if (thumbFlMipExists) {
					const auto flWritingResult = d->WriteThumbFl(thumbnailGroup);
					writingResult = writingResult && flWritingResult;
				} else {
					const auto flWritingResult = d->WriteFlMip(dataGroup);
					writingResult = writingResult && flWritingResult;
				}
			}

			const auto bfExists = dataGroup.exists(bfGroupName);
			if (bfExists) {
				const auto thumbBfExists = thumbnailGroup.getId() > 0 && thumbnailGroup.exists(thumbBfGroupName);

				if(thumbBfExists) {
					const auto bfWritingResult = d->WriteThumbBf(thumbnailGroup);
					writingResult = writingResult && bfWritingResult;
				} else {
					const auto bfWritingResult = d->WriteBf(dataGroup);
					writingResult = writingResult && bfWritingResult;
				}
			}

			const auto group2DExists = dataGroup.exists(group2DName);
			if (group2DExists) {
				const auto group2DWritingResult = d->Write2D(dataGroup);
				writingResult = writingResult && group2DWritingResult;
			}

			dataGroup.close();
			tcfFile.close();

		} catch (H5::Exception& ex) {
			std::cout << ex.getCDetailMsg() << std::endl;
			writingResult = false;
		} catch (cv::Exception& ex) {
			std::cout << ex.msg << std::endl;
			writingResult = false;
		}

		return writingResult;
	}
}
