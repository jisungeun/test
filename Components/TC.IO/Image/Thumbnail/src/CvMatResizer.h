#pragma once

#include <memory>

#include <opencv2/core/mat.hpp>

class CvMatResizer {
public:
	CvMatResizer();
	~CvMatResizer();

	auto SetMat(const cv::Mat& mat) -> void;
	auto SetMaxSize(int size) -> void;

	auto Resize() -> cv::Mat;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
