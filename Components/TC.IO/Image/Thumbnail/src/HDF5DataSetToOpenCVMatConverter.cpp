#include "HDF5DataSetToOpenCVMatConverter.h"
#include <tuple>
#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core.hpp>
#pragma warning(pop)
#include "Utilities.h"

struct HDF5DataSetToOpenCVMatConverter::Impl{
    Impl() = default;
    ~Impl() = default;

    H5::DataSet* dataSet;
    bool customNormalizationFlag{ false };
    float normalizationMinValue{ 0 };
    float normalizationMaxValue{ 0 };

    auto NormalizeData(const std::shared_ptr<uint16_t[]>& inputData, const size_t& numberOfElements)
        ->std::shared_ptr<uint8_t[]>;

    auto NormalizeData(const std::shared_ptr<float[]>& inputData, const size_t& numberOfElements)
        ->std::shared_ptr<uint8_t[]>;

    auto GetRGBMatImage(const int32_t& sizeX, const int32_t& sizeY, const H5::DataType& dataType)->cv::Mat;
};



auto HDF5DataSetToOpenCVMatConverter::Impl::GetRGBMatImage(const int32_t& sizeX, const int32_t& sizeY, 
    const H5::DataType& dataType) -> cv::Mat {
    const auto numberOfSliceImage = static_cast<size_t>(sizeX * sizeY);

    std::shared_ptr<uint8_t[]> dataBlue(new uint8_t[numberOfSliceImage]());
    std::shared_ptr<uint8_t[]> dataGreen(new uint8_t[numberOfSliceImage]());
    std::shared_ptr<uint8_t[]> dataRed(new uint8_t[numberOfSliceImage]());

    const auto inputRank = 3;
    auto selectedSpaceBlue = dataSet->getSpace();
    auto selectedSpaceGreen = dataSet->getSpace();
    auto selectedSpaceRed = dataSet->getSpace();
    const hsize_t offsetBlue[inputRank] = { 2, 0, 0 };
    const hsize_t offsetGreen[inputRank] = { 1, 0, 0 };
    const hsize_t offsetRed[inputRank] = { 0, 0, 0 };
    const hsize_t count[inputRank] = { 1, static_cast<hsize_t>(sizeY), static_cast<hsize_t>(sizeX) };
    selectedSpaceBlue.selectHyperslab(H5S_SELECT_SET, count, offsetBlue);
    selectedSpaceGreen.selectHyperslab(H5S_SELECT_SET, count, offsetGreen);
    selectedSpaceRed.selectHyperslab(H5S_SELECT_SET, count, offsetRed);

    const auto outputRank = 2;
    hsize_t outDims[outputRank] = { static_cast<hsize_t>(sizeY), static_cast<hsize_t>(sizeX) };
    H5::DataSpace outDataSpace(outputRank, outDims);

    dataSet->read(dataBlue.get(), dataType, outDataSpace, selectedSpaceBlue);
    dataSet->read(dataGreen.get(), dataType, outDataSpace, selectedSpaceGreen);
    dataSet->read(dataRed.get(), dataType, outDataSpace, selectedSpaceRed);

    selectedSpaceBlue.close();
    selectedSpaceGreen.close();
    selectedSpaceRed.close();
    outDataSpace.close();

    auto blue = cv::Mat(sizeY, sizeX, CV_8U, dataBlue.get()).clone();
    auto green = cv::Mat(sizeY, sizeX, CV_8U, dataGreen.get()).clone();
    auto red = cv::Mat(sizeY, sizeX, CV_8U, dataRed.get()).clone();

    cv::Mat bgr[inputRank] = { blue, green, red };

    cv::Mat convertedMat;
    merge(bgr, 3, convertedMat);
    return convertedMat;
}

auto HDF5DataSetToOpenCVMatConverter::Impl::NormalizeData(const std::shared_ptr<uint16_t[]>& inputData,
    const size_t& numberOfElements) -> std::shared_ptr<uint8_t[]> {
    float minValue, maxValue;

    if (customNormalizationFlag) {
        minValue = normalizationMinValue;
        maxValue = normalizationMaxValue;
    } else {
        const auto minMaxValue = FindMinMaxValue(inputData, numberOfElements);

        minValue = minMaxValue.minValue;
        maxValue = minMaxValue.maxValue;
    }

    const auto contrast = maxValue - minValue;

    std::shared_ptr<uint8_t[]> normalizedData(new uint8_t[numberOfElements]());
    for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
        const auto voxelValue = static_cast<float>(inputData.get()[dataIndex]);
        const auto normalizedValue = (voxelValue > maxValue) ? (255.f) : ((voxelValue - minValue) / contrast * 255.f);

        normalizedData.get()[dataIndex] = static_cast<uint8_t>(normalizedValue);
    }

    return normalizedData;
}

auto HDF5DataSetToOpenCVMatConverter::Impl::NormalizeData(const std::shared_ptr<float[]>& inputData,
    const size_t& numberOfElements) -> std::shared_ptr<uint8_t[]> {
    float minValue, maxValue;

    if (customNormalizationFlag) {
        minValue = normalizationMinValue;
        maxValue = normalizationMaxValue;
    } else {
        const auto minMaxValue = FindMinMaxValue(inputData, numberOfElements);

        minValue = minMaxValue.minValue;
        maxValue = minMaxValue.maxValue;
    }

    const auto contrast = maxValue - minValue;

    std::shared_ptr<uint8_t[]> normalizedData(new uint8_t[numberOfElements]());
    for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
        const auto voxelValue = static_cast<float>(inputData.get()[dataIndex]);
        const auto normalizedValue = (voxelValue - minValue) / contrast * 255.f;

        normalizedData.get()[dataIndex] = static_cast<uint8_t>(normalizedValue);
    }

    return normalizedData;
}

HDF5DataSetToOpenCVMatConverter::HDF5DataSetToOpenCVMatConverter()
    : d(new Impl()) {
}

HDF5DataSetToOpenCVMatConverter::~HDF5DataSetToOpenCVMatConverter() = default;

auto HDF5DataSetToOpenCVMatConverter::SetDataSet(H5::DataSet& dataSet2D) -> void {
    d->dataSet = &dataSet2D;
}

auto HDF5DataSetToOpenCVMatConverter::Convert() -> cv::Mat {
    const auto& dataSet = *d->dataSet;

    const auto size = GetDataSetSize(dataSet);
    const auto sizeX = static_cast<int32_t>(size.x);
    const auto sizeY = static_cast<int32_t>(size.y);
    const auto sizeZ = static_cast<int32_t>(size.z);
    const auto numberOfElements = sizeX * sizeY * sizeZ;

    cv::Mat convertedMat;
    auto dataType = dataSet.getDataType();

    const auto dataSetIs2D = sizeZ == 1;
    const auto dataSetIs3D = sizeZ > 1;

    if (dataSetIs2D) {
        if (dataType == H5::PredType::NATIVE_UINT16) {
            std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]());
            dataSet.read(data.get(), dataType);
            const auto normalizedData = d->NormalizeData(data, numberOfElements);

            convertedMat = cv::Mat(sizeY, sizeX, CV_8U, normalizedData.get()).clone();
        } else if (dataType == H5::PredType::NATIVE_FLOAT) {
            std::shared_ptr<float[]> data(new float[numberOfElements]());
            dataSet.read(data.get(), dataType);
            const auto normalizedData = d->NormalizeData(data, numberOfElements);

            convertedMat = cv::Mat(sizeY, sizeX, CV_8U, normalizedData.get()).clone();
        } else if (dataType == H5::PredType::NATIVE_UINT8) {
            std::shared_ptr<uint8_t[]> data(new uint8_t[numberOfElements]());
            dataSet.read(data.get(), dataType);

            convertedMat = cv::Mat(sizeY, sizeX, CV_8U, data.get()).clone();
        } else {
            convertedMat = cv::Mat{};
        }
    } else if (dataSetIs3D) {
        if (dataType == H5::PredType::NATIVE_FLOAT) {
            std::shared_ptr<float[]> data(new float[numberOfElements]());

            const hsize_t offset[3] = { 0,0,0 };
            const hsize_t count[3] = { 1, static_cast<hsize_t>(sizeY), static_cast<hsize_t>(sizeX) };

            auto selectedSpace = dataSet.getSpace();
            selectedSpace.selectHyperslab(H5S_SELECT_SET, count, offset);

            const hsize_t dims[2] = { static_cast<hsize_t>(sizeY), static_cast<hsize_t>(sizeX) };
            H5::DataSpace outputSpace(2, dims);

            dataSet.read(data.get(), dataType, outputSpace, selectedSpace);
            selectedSpace.close();
            outputSpace.close();

            const auto normalizedData = d->NormalizeData(data, sizeX * sizeY);

            convertedMat = cv::Mat(sizeY, sizeX, CV_8U, normalizedData.get()).clone();
        } else if (dataType == H5::PredType::NATIVE_UINT8) {
            convertedMat = d->GetRGBMatImage(sizeX, sizeY, dataType);
        } else {
            convertedMat = cv::Mat{};
        }
    } else {
        convertedMat = cv::Mat{};
    }
    dataType.close();

    return convertedMat;
}

auto HDF5DataSetToOpenCVMatConverter::SetCustomNormalizationFlag(const bool& normalizationFlag) -> void {
    d->customNormalizationFlag = normalizationFlag;
}

auto HDF5DataSetToOpenCVMatConverter::SetCustomNormalizationValues(const float& minValue, const float& maxValue)
    -> void {
    d->normalizationMinValue = minValue;
    d->normalizationMaxValue = maxValue;
}
