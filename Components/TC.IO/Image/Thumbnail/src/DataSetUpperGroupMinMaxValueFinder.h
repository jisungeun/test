#pragma once
#include <memory>
#pragma warning(push)
#pragma warning(disable:4268)
#include "H5Cpp.h"
#pragma warning(pop)

class DataSetUpperGroupMinMaxValueFinder {
public:
    DataSetUpperGroupMinMaxValueFinder();
    ~DataSetUpperGroupMinMaxValueFinder();

    auto SetUpperGroup(H5::Group& dataSetUpperGroup)->void;

    auto Find()->bool;

    auto GetMinValue()->float;
    auto GetMaxValue()->float;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};