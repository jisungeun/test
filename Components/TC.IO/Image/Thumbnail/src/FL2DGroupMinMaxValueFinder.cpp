#include "FL2DGroupMinMaxValueFinder.h"

#include <algorithm>
#include <iostream>

#include "DataSetUpperGroupMinMaxValueFinder.h"
#include "Utilities.h"

const std::string flMinAttributeName = "MinIntensity";
const std::string flMaxAttributeName = "MaxIntensity";

struct FL2DGroupMinMaxValueFinder::Impl {
    Impl() = default;
    ~Impl() = default;

    H5::Group* fl2DGroup{ nullptr };
    float minValue{};
    float maxValue{};
    auto GetMinMaxValueFromAttribute() const ->MinMaxValue;
    auto GetMinMaxValueFromEachChannels() const ->MinMaxValue;
};

auto FL2DGroupMinMaxValueFinder::Impl::GetMinMaxValueFromAttribute() const -> MinMaxValue {
    const auto minValueAttributeName = flMinAttributeName;
    const auto maxValueAttributeName = flMaxAttributeName;

    const auto minValueFromAttribute = ReadMinValueAttribute(*this->fl2DGroup, minValueAttributeName);
    const auto maxValueFromAttribute = ReadMaxValueAttribute(*this->fl2DGroup, maxValueAttributeName);

    MinMaxValue minMaxValue;

    if (minValueFromAttribute.valid && maxValueFromAttribute.valid) {
        minMaxValue.minValue = minValueFromAttribute.minValue;
        minMaxValue.maxValue = maxValueFromAttribute.maxValue;
        minMaxValue.valid = true;
    }

    return minMaxValue;
}

auto FL2DGroupMinMaxValueFinder::Impl::GetMinMaxValueFromEachChannels() const -> MinMaxValue {
    MinMaxValue minMaxValue;
    minMaxValue.minValue = std::numeric_limits<float>::max();
    minMaxValue.maxValue = std::numeric_limits<float>::lowest();
    minMaxValue.valid = true;

    auto channelInfoExists = false;

    const auto numberOfObject = this->fl2DGroup->getNumObjs();
    for (auto objectIndex = 0; objectIndex < numberOfObject; ++objectIndex) {
        const auto objectType = this->fl2DGroup->getObjTypeByIdx(objectIndex);
        if (objectType == H5G_GROUP) {
            channelInfoExists = true;

            const auto objectName = this->fl2DGroup->getObjnameByIdx(objectIndex);
            auto channelGroup = this->fl2DGroup->openGroup(objectName);

            DataSetUpperGroupMinMaxValueFinder dataSetUpperGroupMinMaxValueFinder;
            dataSetUpperGroupMinMaxValueFinder.SetUpperGroup(channelGroup);
            dataSetUpperGroupMinMaxValueFinder.Find();

            const auto channelMinValue = dataSetUpperGroupMinMaxValueFinder.GetMinValue();
            const auto channelMaxValue = dataSetUpperGroupMinMaxValueFinder.GetMaxValue();

            minMaxValue.minValue = std::min<float>(minMaxValue.minValue, channelMinValue);
            minMaxValue.maxValue = std::max<float>(minMaxValue.maxValue, channelMaxValue);

            channelGroup.close();
        }
    }

    if (!channelInfoExists) {
        minMaxValue.valid = false;
    }

    return minMaxValue;
}

FL2DGroupMinMaxValueFinder::FL2DGroupMinMaxValueFinder()
    : d(new Impl()) {
}

FL2DGroupMinMaxValueFinder::~FL2DGroupMinMaxValueFinder() = default;

auto FL2DGroupMinMaxValueFinder::SetFL2DGroup(H5::Group& fl2DGroup) -> void {
    d->fl2DGroup = &fl2DGroup;
}

auto FL2DGroupMinMaxValueFinder::Find() -> bool {
    auto findingResult = true;

    try {
        const auto minMaxValueFromAttribute = d->GetMinMaxValueFromAttribute();

        float minValue, maxValue;

        if (minMaxValueFromAttribute.valid) {
            minValue = minMaxValueFromAttribute.minValue;
            maxValue = minMaxValueFromAttribute.maxValue;
        } else {
            const auto minMaxValueFromEachChannels = d->GetMinMaxValueFromEachChannels();
            if (minMaxValueFromEachChannels.valid) {
                minValue = minMaxValueFromEachChannels.minValue;
                maxValue = minMaxValueFromEachChannels.maxValue;
            } else {
                minValue = std::numeric_limits<float>::lowest();
                maxValue = std::numeric_limits<float>::max();
                findingResult = false;
            }
        }

        d->minValue = minValue;
        d->maxValue = maxValue;

    } catch (H5::Exception& exception) {
        std::cout << exception.getCDetailMsg() << std::endl;
        findingResult = false;
    }

    return findingResult;
}

auto FL2DGroupMinMaxValueFinder::GetMinValue() -> float {
    return d->minValue;
}

auto FL2DGroupMinMaxValueFinder::GetMaxValue() -> float {
    return d->maxValue;
}
