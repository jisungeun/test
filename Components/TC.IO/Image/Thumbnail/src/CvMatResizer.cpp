#include <opencv2/imgproc.hpp>

#include "CvMatResizer.h"

struct CvMatResizer::Impl {
	const cv::Mat* mat = nullptr;
	int size = 1024;
};

CvMatResizer::CvMatResizer() : d(new Impl) {}

CvMatResizer::~CvMatResizer() = default;

auto CvMatResizer::SetMat(const cv::Mat & mat) -> void {
	d->mat = &mat;
}

auto CvMatResizer::SetMaxSize(int size) -> void {
	d->size = size;
}

auto CvMatResizer::Resize() -> cv::Mat {
	if (d->mat && (d->mat->size().width > d->size || d->mat->size().height > d->size)) {
		cv::Mat mat;

		const double max = std::max(d->mat->size().width, d->mat->size().height);
		const auto ratio = d->size / max;

		auto width = static_cast<int>(d->mat->size().width * ratio);
		auto height = static_cast<int>(d->mat->size().height * ratio);

		cv::resize(*d->mat, mat, { width, height });

		return mat;
	}

	return *d->mat;
}
