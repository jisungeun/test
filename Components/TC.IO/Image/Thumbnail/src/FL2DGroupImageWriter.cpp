#include "FL2DGroupImageWriter.h"
#include <algorithm>
#include <opencv2/core.hpp>

#include "CvMatResizer.h"
#include "FL2DGroupMinMaxValueFinder.h"
#include "HDF5DataSetToOpenCVMatConverter.h"
#include "WritingImageFilePathGenerator.h"
#include "MatImageWriter.h"
#include "Utilities.h"

const int32_t rgbChannels = 3;

struct FL2DGroupImageWriter::Impl {
    Impl() = default;
    ~Impl() = default;

    std::string tcfFileName{};
    H5::Group* fl2DGroup{};
    std::string outputFolderPath{};

    auto GetNumberOfImages()->int32_t;
    auto GetFlMipImage(const std::string& channelName, const int32_t& imageIndex, const MinMaxValue& normalizingValue)
        ->cv::Mat;
    static auto GetImageSize(const cv::Mat& red, const cv::Mat& green, const cv::Mat& blue) -> cv::Size2i;
    static auto PrepareMat(const cv::Mat& image, const cv::Size2i& imageSize)->cv::Mat;
    auto MergeRGB(const cv::Mat& red, const cv::Mat& green, const cv::Mat& blue)->cv::Mat;
};

auto FL2DGroupImageWriter::Impl::GetNumberOfImages() -> int32_t {
    const auto channelNumber = fl2DGroup->getNumObjs();

    auto numberOfImages = 0;

    for (auto channelIndex = 0; channelIndex < channelNumber; ++channelIndex) {
        const auto channelName = fl2DGroup->getObjnameByIdx(channelIndex);
        auto channelGroup = fl2DGroup->openGroup(channelName);
        const auto numberOfTimeFrames = channelGroup.getNumObjs();
        channelGroup.close();

        numberOfImages = std::max<int32_t>(numberOfImages, static_cast<int32_t>(numberOfTimeFrames));
    }

    return numberOfImages;
}

auto FL2DGroupImageWriter::Impl::GetFlMipImage(const std::string& channelName, const int32_t& imageIndex, 
    const MinMaxValue& normalizingValue)-> cv::Mat {
    const auto channelGroupExists = fl2DGroup->exists(channelName);

    cv::Mat mipImage;
    if (channelGroupExists) {
        auto channelGroup = fl2DGroup->openGroup(channelName);

        const auto timeFrameString = GenerateTimeFrameString(imageIndex);
        auto timeFrameDataSet = channelGroup.openDataSet(timeFrameString);

        HDF5DataSetToOpenCVMatConverter hdf5DataSetToOpenCvMatConverter;
        hdf5DataSetToOpenCvMatConverter.SetDataSet(timeFrameDataSet);

        hdf5DataSetToOpenCvMatConverter.SetCustomNormalizationFlag(normalizingValue.valid);
        hdf5DataSetToOpenCvMatConverter.SetCustomNormalizationValues(
            normalizingValue.minValue, normalizingValue.maxValue);

        mipImage = hdf5DataSetToOpenCvMatConverter.Convert();

        timeFrameDataSet.close();
        channelGroup.close();
    } else {
        mipImage = cv::Mat{};
    }

    return mipImage;
}

auto FL2DGroupImageWriter::Impl::GetImageSize(const cv::Mat& red, const cv::Mat& green, const cv::Mat& blue)
    -> cv::Size2i {
    const auto redSize = red.size();
    const auto greenSize = green.size();
    const auto blueSize = blue.size();

    const auto redWidth = redSize.width;
    const auto redHeight = redSize.height;

    const auto greenWidth = greenSize.width;
    const auto greenHeight = greenSize.height;

    const auto blueWidth = blueSize.width;
    const auto blueHeight = blueSize.height;

    const auto width = std::max<int32_t>(std::max<int32_t>(redWidth, greenWidth), blueWidth);
    const auto height = std::max<int32_t>(std::max<int32_t>(redHeight, greenHeight), blueHeight);

    return cv::Size2i{ width,height };
}

auto FL2DGroupImageWriter::Impl::PrepareMat(const cv::Mat& image, const cv::Size2i& imageSize) -> cv::Mat {
    cv::Mat preparedMat;
    if (image.empty()) {
        preparedMat = cv::Mat(imageSize, CV_8U, cv::Scalar(0));
    } else {
        preparedMat = image;
    }
    return preparedMat;
}

auto FL2DGroupImageWriter::Impl::MergeRGB(const cv::Mat& red, const cv::Mat& green, const cv::Mat& blue) -> cv::Mat {
    const auto imageSize = GetImageSize(red, green, blue);

    const auto preparedRedImage = PrepareMat(red, imageSize);
    const auto preparedGreenImage = PrepareMat(green, imageSize);
    const auto preparedBlueImage = PrepareMat(blue, imageSize);

    cv::Mat bgr[rgbChannels] = { preparedBlueImage, preparedGreenImage, preparedRedImage };

    cv::Mat mergedMat;
    merge(bgr, rgbChannels, mergedMat);
    return mergedMat;
}

FL2DGroupImageWriter::FL2DGroupImageWriter()
    : d(new Impl()) {
}

FL2DGroupImageWriter::~FL2DGroupImageWriter() = default;

auto FL2DGroupImageWriter::SetTcfFileName(const std::string& tcfFileName) -> void {
    d->tcfFileName = tcfFileName;
}

auto FL2DGroupImageWriter::SetFL2DGroup(H5::Group& fl2DGroup) -> void {
    d->fl2DGroup = &fl2DGroup;
}

auto FL2DGroupImageWriter::SetOutputFolderPath(const std::string& outputFolderPath) -> void {
    d->outputFolderPath = outputFolderPath;
}

auto FL2DGroupImageWriter::Write() -> bool {
    WritingImageFilePathGenerator writingImageFilePathGenerator;
    writingImageFilePathGenerator.SetTcfFileName(d->tcfFileName);
    writingImageFilePathGenerator.SetDataTypeName("FL");
    writingImageFilePathGenerator.SetOutputFolderPath(d->outputFolderPath);

    MatImageWriter matImageWriter;
    matImageWriter.SetColor(ImageBaseColor::COLOR);

    auto writingResult = true;

    FL2DGroupMinMaxValueFinder fl2DGroupMinMaxValueFinder;
    fl2DGroupMinMaxValueFinder.SetFL2DGroup(*d->fl2DGroup);
    const auto minMaxFindingIsGood = fl2DGroupMinMaxValueFinder.Find();
    const auto minValue = fl2DGroupMinMaxValueFinder.GetMinValue();
    const auto maxValue = fl2DGroupMinMaxValueFinder.GetMaxValue();

    MinMaxValue normalizingValues;
    normalizingValues.minValue = minValue;
    normalizingValues.maxValue = maxValue;
    normalizingValues.valid = minMaxFindingIsGood;

    const auto numberOfImage = d->GetNumberOfImages();
    for (auto imageIndex = 0; imageIndex < numberOfImage; ++imageIndex) {
        const auto blueMipImage = d->GetFlMipImage("CH0", imageIndex, normalizingValues);
        const auto greenMipImage = d->GetFlMipImage("CH1", imageIndex, normalizingValues);
        const auto redMipImage = d->GetFlMipImage("CH2", imageIndex, normalizingValues);

        cv::Mat convertedMat = d->MergeRGB(redMipImage, greenMipImage, blueMipImage);

        writingImageFilePathGenerator.SetTimeFrameIndex(imageIndex);
        const auto writingImageFilePath = writingImageFilePathGenerator.Generate();
        matImageWriter.SetImageFilePath(writingImageFilePath);
        
        CvMatResizer resizer;
        resizer.SetMat(convertedMat);
        resizer.SetMaxSize(1024);
        auto resized = resizer.Resize();

        matImageWriter.SetMatImage(resized);
        const auto timeFrameImageWritingResult = matImageWriter.Write();
        writingResult = writingResult && timeFrameImageWritingResult;
    }

    return writingResult;
}
