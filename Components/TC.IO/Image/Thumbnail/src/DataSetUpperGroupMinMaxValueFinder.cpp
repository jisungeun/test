#include <algorithm>

#include "DataSetUpperGroupMinMaxValueFinder.h"
#include <iostream>

#include "Utilities.h"

const std::string htMinAttributeName = "RIMin";
const std::string htMaxAttributeName = "RIMax";

const std::string flMinAttributeName = "MaxIntensity";
const std::string flMaxAttributeName = "MinIntensity";

const std::string phaseMinAttributeName = "MinPhase";
const std::string phaseMaxAttributeName = "MaxPhase";

struct DataSetUpperGroupMinMaxValueFinder::Impl {
    Impl() = default;
    ~Impl() = default;

    H5::Group* dataSetUpperGroup{nullptr};
    float minValue{};
    float maxValue{};

    auto GetMinMaxValueFromAttribute()->MinMaxValue;
    auto GetMinValueAttributeName() const ->std::string;
    auto GetMaxValueAttributeName() const ->std::string;

    auto GetMinMaxValueFromDataSetAttribute()->MinMaxValue;
    auto GetMinMaxValueFromDataSet()->MinMaxValue;

    auto ReadMinMaxValue(const H5::DataSet& dataSet)->MinMaxValue;

    auto DecodeMinMaxValue(const float& minVal, const float& maxVal)->MinMaxValue;

};

auto DataSetUpperGroupMinMaxValueFinder::Impl::GetMinMaxValueFromAttribute() -> MinMaxValue {
    const auto minValueAttributeName = GetMinValueAttributeName();
    const auto maxValueAttributeName = GetMaxValueAttributeName();

    const auto minValueFromAttribute = ReadMinValueAttribute(*dataSetUpperGroup, minValueAttributeName);
    const auto maxValueFromAttribute = ReadMaxValueAttribute(*dataSetUpperGroup, maxValueAttributeName);

    MinMaxValue minMaxValue;

    if (minValueFromAttribute.valid && maxValueFromAttribute.valid) {
        minMaxValue.minValue = minValueFromAttribute.minValue;
        minMaxValue.maxValue = maxValueFromAttribute.maxValue;
        minMaxValue.valid = true;
    }

    return minMaxValue;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::GetMinValueAttributeName() const -> std::string {
    const auto dataSetUpperGroupName = this->dataSetUpperGroup->getObjName();

    std::string minValueAttributeName;
    if (dataSetUpperGroupName.find("2DMIP") != std::string::npos) {
        minValueAttributeName = htMinAttributeName;
    } else if (dataSetUpperGroupName.find("2DFLMIP") != std::string::npos) {
        minValueAttributeName = flMinAttributeName;
    } else if (dataSetUpperGroupName.find("2D") != std::string::npos){
        minValueAttributeName = phaseMinAttributeName;
    } else {
        minValueAttributeName = "DataSetUpperGroupMinMaxValueFinder::SomethingWrong";
    }

    return minValueAttributeName;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::GetMaxValueAttributeName() const -> std::string {
    const auto dataSetUpperGroupName = this->dataSetUpperGroup->getObjName();

    std::string maxValueAttributeName;
    if (dataSetUpperGroupName.find("2DMIP") != std::string::npos) {
        maxValueAttributeName = htMaxAttributeName;
    } else if (dataSetUpperGroupName.find("2DFLMIP") != std::string::npos) {
        maxValueAttributeName = flMaxAttributeName;
    } else if (dataSetUpperGroupName.find("2D") != std::string::npos){
        maxValueAttributeName = phaseMaxAttributeName;
    } else {
        maxValueAttributeName = "DataSetUpperGroupMinMaxValueFinder::SomthingWrong";
    }

    return maxValueAttributeName;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::GetMinMaxValueFromDataSetAttribute() -> MinMaxValue {
    const auto numberOfObject = this->dataSetUpperGroup->getNumObjs();

    const auto minValueAttributeName = GetMinValueAttributeName();
    const auto maxValueAttributeName = GetMaxValueAttributeName();

    MinMaxValue minMaxValueFromDataSetAttribute;
    minMaxValueFromDataSetAttribute.minValue = std::numeric_limits<float>::max();
    minMaxValueFromDataSetAttribute.maxValue = std::numeric_limits<float>::lowest();
    minMaxValueFromDataSetAttribute.valid = true;

    for (auto objectIndex = 0; objectIndex < numberOfObject; ++objectIndex) {
        const auto objectType = this->dataSetUpperGroup->getObjTypeByIdx(objectIndex);

        if (objectType == H5G_DATASET) {
            const auto dataSetName = this->dataSetUpperGroup->getObjnameByIdx(objectIndex);

            auto dataSet = this->dataSetUpperGroup->openDataSet(dataSetName);

            const auto minValueFromAttribute = ReadMinValueAttribute(dataSet, minValueAttributeName);
            const auto maxValueFromAttribute = ReadMaxValueAttribute(dataSet, maxValueAttributeName);

            minMaxValueFromDataSetAttribute.minValue =
                std::min<float>(minMaxValueFromDataSetAttribute.minValue, minValueFromAttribute.minValue);
            minMaxValueFromDataSetAttribute.maxValue = 
                std::max<float>(minMaxValueFromDataSetAttribute.maxValue, maxValueFromAttribute.maxValue);
            minMaxValueFromDataSetAttribute.valid = minMaxValueFromDataSetAttribute.valid &&
                minValueFromAttribute.valid && maxValueFromAttribute.valid;

            dataSet.close();
        }

        if (minMaxValueFromDataSetAttribute.valid == false) {
            break;
        }
    }
    return minMaxValueFromDataSetAttribute;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::GetMinMaxValueFromDataSet() -> MinMaxValue {
    const auto numberOfObject = this->dataSetUpperGroup->getNumObjs();

    MinMaxValue minMaxValueFromDataSet;
    minMaxValueFromDataSet.minValue = std::numeric_limits<float>::max();
    minMaxValueFromDataSet.maxValue = std::numeric_limits<float>::lowest();
    minMaxValueFromDataSet.valid = true;

    for (auto objectIndex = 0; objectIndex < numberOfObject; ++objectIndex) {
        const auto objectType = this->dataSetUpperGroup->getObjTypeByIdx(objectIndex);

        if (objectType == H5G_DATASET) {
            const auto dataSetName = this->dataSetUpperGroup->getObjnameByIdx(objectIndex);

            auto dataSet = this->dataSetUpperGroup->openDataSet(dataSetName);

            const auto subMinMaxValueDataSet = ReadMinMaxValue(dataSet);

            minMaxValueFromDataSet.minValue =
                std::min<float>(minMaxValueFromDataSet.minValue, subMinMaxValueDataSet.minValue);
            minMaxValueFromDataSet.maxValue =
                std::max<float>(minMaxValueFromDataSet.maxValue, subMinMaxValueDataSet.maxValue);
            minMaxValueFromDataSet.valid = 
                minMaxValueFromDataSet.valid && subMinMaxValueDataSet.valid;

            dataSet.close();
        }

        if (minMaxValueFromDataSet.valid == false) {
            break;
        }
    }
    return minMaxValueFromDataSet;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::ReadMinMaxValue(const H5::DataSet& dataSet) -> MinMaxValue {
    const auto size = GetDataSetSize(dataSet);
    const auto sizeX = size.x;
    const auto sizeY = size.y;
    const auto sizeZ = size.z;
    const auto numberOfElements = sizeX * sizeY * sizeZ;

    auto dataType = dataSet.getDataType();

    const auto dataSetIs2D = sizeZ == 1;

    MinMaxValue minMaxValue;

    if (dataSetIs2D) {
        if (dataType == H5::PredType::NATIVE_UINT16) {
            std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]());
            dataSet.read(data.get(), dataType);
            minMaxValue = FindMinMaxValue(data, numberOfElements);
        } else if (dataType == H5::PredType::NATIVE_FLOAT) {
            std::shared_ptr<float[]> data(new float[numberOfElements]());
            dataSet.read(data.get(), dataType);
            minMaxValue = FindMinMaxValue(data, numberOfElements);
        } else if (dataType == H5::PredType::NATIVE_UINT8) {
            std::shared_ptr<uint8_t[]> data(new uint8_t[numberOfElements]());
            dataSet.read(data.get(), dataType);
            minMaxValue = FindMinMaxValue(data, numberOfElements);
        } else {
            minMaxValue.minValue = std::numeric_limits<float>::max();
            minMaxValue.maxValue = std::numeric_limits<float>::lowest();
        }
    } else {
        minMaxValue.minValue = std::numeric_limits<float>::max();
        minMaxValue.maxValue = std::numeric_limits<float>::lowest();
    }
    dataType.close();

    return minMaxValue;
}

auto DataSetUpperGroupMinMaxValueFinder::Impl::DecodeMinMaxValue(const float& minVal, const float& maxVal)
    -> MinMaxValue {
    const auto dataSetUpperGroupName = this->dataSetUpperGroup->getObjName();

    MinMaxValue minMaxValue;
    if (dataSetUpperGroupName == "/Data/2DMIP") {
        if (minVal < 10000) {
            minMaxValue.minValue = minVal * 10000;
            minMaxValue.maxValue = maxVal * 10000;
        } else {
            minMaxValue.minValue = minVal;
            minMaxValue.maxValue = maxVal;
        }

    } else {
        minMaxValue.minValue = minVal;
        minMaxValue.maxValue = maxVal;
    }

    return minMaxValue;
}

DataSetUpperGroupMinMaxValueFinder::DataSetUpperGroupMinMaxValueFinder()
    : d(new Impl()) {
}

DataSetUpperGroupMinMaxValueFinder::~DataSetUpperGroupMinMaxValueFinder() = default;

auto DataSetUpperGroupMinMaxValueFinder::SetUpperGroup(H5::Group& dataSetUpperGroup) -> void {
    d->dataSetUpperGroup = &dataSetUpperGroup;
}

auto DataSetUpperGroupMinMaxValueFinder::Find() -> bool {
    auto findingResult = true;

    try {
        const auto minMaxValueFromAttribute = d->GetMinMaxValueFromAttribute();

        float minValue, maxValue;

        if (minMaxValueFromAttribute.valid) {
            minValue = minMaxValueFromAttribute.minValue;
            maxValue = minMaxValueFromAttribute.maxValue;
        } else {
            const auto minMaxValueFromDataSetAttribute = d->GetMinMaxValueFromDataSetAttribute();
            if (minMaxValueFromDataSetAttribute.valid) {
                minValue = minMaxValueFromDataSetAttribute.minValue;
                maxValue = minMaxValueFromDataSetAttribute.maxValue;
            } else {
                const auto minMaxValueFromDataSet = d->GetMinMaxValueFromDataSet();
                if (minMaxValueFromDataSet.valid) {
                    minValue = minMaxValueFromDataSet.minValue;
                    maxValue = minMaxValueFromDataSet.maxValue;
                } else {
                    minValue = std::numeric_limits<float>::lowest();
                    maxValue = std::numeric_limits<float>::max();
                    findingResult = false;
                }
            }
        }

        const auto decodedMinMaxValue = d->DecodeMinMaxValue(minValue, maxValue);

        d->minValue = decodedMinMaxValue.minValue;
        d->maxValue = decodedMinMaxValue.maxValue;

    } catch (H5::Exception & exception) {
        std::cout << exception.getCDetailMsg() << std::endl;
        findingResult = false;
    }
    return findingResult;
}

auto DataSetUpperGroupMinMaxValueFinder::GetMinValue() -> float {
    return d->minValue;
}

auto DataSetUpperGroupMinMaxValueFinder::GetMaxValue() -> float {
    return d->maxValue;
}
