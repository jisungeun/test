#pragma once
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <memory>

class DataSetUpperGroupImageWriter {
public:
    DataSetUpperGroupImageWriter();
    ~DataSetUpperGroupImageWriter();

    auto SetTcfFileName(const std::string& tcfFileName)->void;
    auto SetDataSet2DUpperGroup(H5::Group& dataSet2DUpperGroup)->void;
    auto SetOutputFolderPath(const std::string& outputFolderPath)->void;
    auto Write()->bool;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
