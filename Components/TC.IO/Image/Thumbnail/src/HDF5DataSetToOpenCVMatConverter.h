#pragma once
#include <memory>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core/mat.hpp>
#pragma warning(pop)


class HDF5DataSetToOpenCVMatConverter {
public:
    HDF5DataSetToOpenCVMatConverter();
    ~HDF5DataSetToOpenCVMatConverter();

    auto SetDataSet(H5::DataSet& dataSet2D)->void;
    auto Convert()->cv::Mat;

    auto SetCustomNormalizationFlag(const bool& normalizationFlag)->void;
    auto SetCustomNormalizationValues(const float& minValue, const float& maxValue)->void;

private:
    struct Impl;
    std::unique_ptr<Impl> d;

};