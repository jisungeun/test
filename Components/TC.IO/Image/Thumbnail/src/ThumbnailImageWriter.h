#pragma once

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <memory>

class ThumbnailImageWriter {
public:
    ThumbnailImageWriter();
    ~ThumbnailImageWriter();

    auto SetTcfFileName(const std::string& tcfFileName)->void;
    auto SetGroup(H5::Group& Thumbnail)->void;
    auto SetOutputFolderPath(const std::string& outputFolderPath)->void;
    auto SetDataType(const std::string& type) -> void;
    auto Write()->bool;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
