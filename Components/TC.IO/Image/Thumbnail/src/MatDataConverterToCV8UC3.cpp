#include "MatDataConverterToCV8UC3.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core.hpp>
#pragma warning(pop)

const int32_t channels = 3;

struct MatDataConverterToCV8UC3::Impl {
    Impl() = default;
    ~Impl() = default;

    cv::Mat imageMat{};
};

MatDataConverterToCV8UC3::MatDataConverterToCV8UC3()
    : d(new Impl()) {
}

MatDataConverterToCV8UC3::~MatDataConverterToCV8UC3() = default;

auto MatDataConverterToCV8UC3::SetImageMat(const cv::Mat& imageMat) -> void {
    d->imageMat = imageMat;
}

auto MatDataConverterToCV8UC3::Convert() -> cv::Mat {
    auto inputMat = d->imageMat;

    const auto numberOfChannel = inputMat.channels();

    cv::Mat convertedImageMat;

    if (numberOfChannel == 1) {
        cv::Mat mergingData[channels] = { inputMat, inputMat, inputMat };
        cv::Mat mergedImage{};
        merge(mergingData, 3, mergedImage);

        convertedImageMat = mergedImage;
    } else if (numberOfChannel == 3) {
        convertedImageMat = inputMat;
    } else {
        convertedImageMat = cv::Mat{};
    }

    return convertedImageMat;
}
