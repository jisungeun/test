#pragma once
#include <memory>

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/core/mat.hpp>
#pragma warning(pop)


class MatDataConverterToCV8UC3 {
public:
    MatDataConverterToCV8UC3();
    ~MatDataConverterToCV8UC3();

    auto SetImageMat(const cv::Mat& imageMat)->void;
    auto Convert()->cv::Mat;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
