#include "WritingImageFilePathGenerator.h"
#include "Utilities.h"

struct WritingImageFilePathGenerator::Impl {
    Impl() = default;
    ~Impl() = default;

    std::string outputFolderPath{};
    std::string tcfFileName{};
    std::string dataTypeName{};
    int32_t timeFrameIndex{};
};

WritingImageFilePathGenerator::WritingImageFilePathGenerator()
    : d(new Impl()) {
}

WritingImageFilePathGenerator::~WritingImageFilePathGenerator() = default;

auto WritingImageFilePathGenerator::SetOutputFolderPath(const std::string& outputFolderPath) -> void {
    d->outputFolderPath = outputFolderPath;
}

auto WritingImageFilePathGenerator::SetTcfFileName(const std::string& tcfFileName) -> void {
    d->tcfFileName = tcfFileName;
}

auto WritingImageFilePathGenerator::SetDataTypeName(const std::string& dataTypeName) -> void {
    d->dataTypeName = dataTypeName;
}

auto WritingImageFilePathGenerator::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
    d->timeFrameIndex = timeFrameIndex;
}

auto WritingImageFilePathGenerator::Generate() -> std::string {
    const auto timeFrameString =  GenerateTimeFrameString(d->timeFrameIndex);

    const auto imageFilePath = 
        d->outputFolderPath + "/" + d->tcfFileName + "_" + d->dataTypeName + "_" + timeFrameString + ".png";
    return imageFilePath;
}
