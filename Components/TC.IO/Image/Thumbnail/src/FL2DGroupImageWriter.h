#pragma once
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <memory>

class FL2DGroupImageWriter {
public:
    FL2DGroupImageWriter();
    ~FL2DGroupImageWriter();

    auto SetTcfFileName(const std::string& tcfFileName)->void;
    auto SetFL2DGroup(H5::Group& fl2DGroup)->void;
    auto SetOutputFolderPath(const std::string& outputFolderPath)->void;
    auto Write()->bool;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
