#include "Utilities.h"
#include <iomanip>
#include <sstream>
#include <algorithm>

auto GetDataSetSize(const H5::DataSet& dataSet) -> DataSetSize {
    auto space = dataSet.getSpace();

    const auto rank = 3;
    hsize_t dims[rank] = { 0,0,0 };
    const auto dimensinality = space.getSimpleExtentDims(dims);

    space.close();

    DataSetSize dataSetSize = DataSetSize();
    if (dimensinality == 2) {
        dataSetSize.x = static_cast<int32_t>(dims[1]);
        dataSetSize.y = static_cast<int32_t>(dims[0]);
        dataSetSize.z = 1;
    } else if (dimensinality == 3) {
        dataSetSize.x = static_cast<int32_t>(dims[2]);
        dataSetSize.y = static_cast<int32_t>(dims[1]);
        dataSetSize.z = static_cast<int32_t>(dims[0]);
    }

    return dataSetSize;
}

auto GenerateTimeFrameString(const int32_t& timeFrameIndex) -> std::string {
    const int32_t length = 6;
    std::ostringstream oStringStream;
    oStringStream << std::setfill('0') << std::setw(length) << timeFrameIndex;
    return oStringStream.str();
}

auto FindMinMaxValue(const std::shared_ptr<uint16_t[]>& inputData, const size_t& numberOfElements) -> MinMaxValue {
    auto minValue = std::numeric_limits<uint16_t>::max();
    auto maxValue = std::numeric_limits<uint16_t>::lowest();

    for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
        minValue = std::min<uint16_t>(inputData.get()[dataIndex], minValue);
        maxValue = std::max<uint16_t>(inputData.get()[dataIndex], maxValue);
    }

    MinMaxValue minMaxValue;
    minMaxValue.minValue = static_cast<float>(minValue);
    minMaxValue.maxValue = static_cast<float>(maxValue);
    minMaxValue.valid = true;

    return minMaxValue;
}

auto FindMinMaxValue(const std::shared_ptr<float[]>& inputData, const size_t& numberOfElements) -> MinMaxValue {
    auto minValue = std::numeric_limits<float>::max();
    auto maxValue = std::numeric_limits<float>::lowest();

    for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
        minValue = std::min<float>(inputData.get()[dataIndex], minValue);
        maxValue = std::max<float>(inputData.get()[dataIndex], maxValue);
    }

    MinMaxValue minMaxValue;
    minMaxValue.minValue = static_cast<float>(minValue);
    minMaxValue.maxValue = static_cast<float>(maxValue);

    return minMaxValue;
}

auto FindMinMaxValue(const std::shared_ptr<uint8_t[]>& inputData, const size_t& numberOfElements) -> MinMaxValue {
    auto minValue = std::numeric_limits<float>::max();
    auto maxValue = std::numeric_limits<float>::lowest();

    for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
        minValue = std::min<float>(inputData.get()[dataIndex], minValue);
        maxValue = std::max<float>(inputData.get()[dataIndex], maxValue);
    }

    MinMaxValue minMaxValue;
    minMaxValue.minValue = static_cast<float>(minValue);
    minMaxValue.maxValue = static_cast<float>(maxValue);

    return minMaxValue;
}

auto ReadMinValueAttribute(const H5::H5Object& targetObject, const std::string& minValueAttributeName) -> MinValue {
    MinValue minValueFromAttribute;

    if (targetObject.attrExists(minValueAttributeName)) {
        auto minValueAttribute = targetObject.openAttribute(minValueAttributeName);
        auto dataType = minValueAttribute.getDataType();

        if (dataType.getClass() == H5T_INTEGER) {
            int64_t valueFromAttribute;
            minValueAttribute.read(dataType, &valueFromAttribute);
            minValueFromAttribute.minValue = static_cast<float>(valueFromAttribute);
            minValueFromAttribute.valid = true;
        } else if (dataType.getClass() == H5T_FLOAT) {
            double valueFromAttribute;
            minValueAttribute.read(dataType, &valueFromAttribute);
            minValueFromAttribute.minValue = static_cast<float>(valueFromAttribute);
            minValueFromAttribute.valid = true;
        } else {
            minValueFromAttribute.minValue = std::numeric_limits<float>::max();
            minValueFromAttribute.valid = false;
        }

        dataType.close();
        minValueAttribute.close();
    }

    return minValueFromAttribute;
}

auto ReadMaxValueAttribute(const H5::H5Object& targetObject, const std::string& maxValueAttributeName) -> MaxValue {
    MaxValue maxValueFromAttribute;

    if (targetObject.attrExists(maxValueAttributeName)) {
        auto maxValueAttribute = targetObject.openAttribute(maxValueAttributeName);
        auto dataType = maxValueAttribute.getDataType();

        if (dataType.getClass() == H5T_INTEGER) {
            int64_t valueFromAttribute;
            maxValueAttribute.read(dataType, &valueFromAttribute);
            maxValueFromAttribute.maxValue = static_cast<float>(valueFromAttribute);
            maxValueFromAttribute.valid = true;
        } else if (dataType.getClass() == H5T_FLOAT) {
            double valueFromAttribute;
            maxValueAttribute.read(dataType, &valueFromAttribute);
            maxValueFromAttribute.maxValue = static_cast<float>(valueFromAttribute);
            maxValueFromAttribute.valid = true;
        } else {
            maxValueFromAttribute.maxValue = std::numeric_limits<float>::lowest();
            maxValueFromAttribute.valid = false;
        }

        dataType.close();
        maxValueAttribute.close();
    }

    return maxValueFromAttribute;
}
