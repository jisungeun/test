#pragma once
#include <TCTCFThumbnailExport.h>
#include <iostream>
#include <memory>

namespace TC::IO {
    class TCTCFThumbnail_API TCFThumbnailWriter {
    public:
        TCFThumbnailWriter();
        ~TCFThumbnailWriter();

        auto SetTcfFilePath(const std::string& tcfFilePath)->void;
        auto SetOutputFolderPath(const std::string& outputFolderPath)->void;
        auto Write()->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
