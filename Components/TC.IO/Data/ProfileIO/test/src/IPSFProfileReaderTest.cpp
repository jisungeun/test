#include <catch2/catch.hpp>

#include "IPSFProfileReader.h"

using namespace TC::IO::ProfileIO;

namespace IPSFProfileReaderTest {
    class PSFProfileReaderForTest final : public IPSFProfileReader {
    public:
        PSFProfileReaderForTest() = default;
        ~PSFProfileReaderForTest() = default;
        auto SetPath(const QString& path) -> void override {
            this->setPathTriggered = true;
        }
        auto Read() -> bool override {
            this->readTriggered = true;
            return false;
        }

        bool setPathTriggered{ false };
        bool readTriggered{ false };
    };

    TEST_CASE("IPSFProfileReader : unit test") {
        PSFProfileReaderForTest reader;

        SECTION("SetPath()") {
            CHECK(reader.setPathTriggered == false);
            reader.SetPath("");
            CHECK(reader.setPathTriggered == true);
        }
        SECTION("Read()") {
            CHECK(reader.readTriggered == false);
            reader.Read();
            CHECK(reader.readTriggered == true);
        }
    }
}
