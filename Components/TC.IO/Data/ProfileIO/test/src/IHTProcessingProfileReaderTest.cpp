#include <catch2/catch.hpp>

#include "IHTProcessingProfileReader.h"

using namespace TC::IO::ProfileIO;

namespace IHTProcessingProfileReaderTest {
    class HTProcessingProfileReaderForTest final : public IHTProcessingProfileReader {
    public:
        HTProcessingProfileReaderForTest() = default;
        ~HTProcessingProfileReaderForTest() = default;

        auto SetPath(const QString& path) -> void override {
            setPathTriggered = true;
        }
        auto Read() -> bool override {
            this->readTriggered = true;
            return false;
        }

        bool setPathTriggered{ false };
        bool readTriggered{ false };
    };

    TEST_CASE("IHTProcessingProfileReader : unit test") {
        SECTION("SetPath()") {
            HTProcessingProfileReaderForTest reader;
            CHECK(reader.setPathTriggered == false);
            reader.SetPath("");
            CHECK(reader.setPathTriggered == true);
        }
        SECTION("Read()") {
            HTProcessingProfileReaderForTest reader;
            CHECK(reader.readTriggered == false);
            reader.Read();
            CHECK(reader.readTriggered == true);
        }
    }
}