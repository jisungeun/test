#include <catch2/catch.hpp>

#include "ImagingProfileWriter_v0_0_1.h"

namespace ImagingProfileWriter_v0_0_1Test {
    using namespace TC::ImagingProfile;
    using namespace TC::IO::ProfileIO;

    TEST_CASE("ImagingProfileWriter_v0_0_1Test") {
        ImagingProfile_v0_0_1 profileDefault;
        profileDefault.slice = 60;
        profileDefault.step = 20;

        ImagingProfile_v0_0_1 profile038;
        profile038.slice = 60;
        profile038.step = 40;

        ImagingProfile_v0_0_1 profile054;
        profile054.slice = 60;
        profile054.step = 30;

        ImagingProfile_v0_0_1 profile068;
        profile068.slice = 70;
        profile068.step = 20;

        ImagingProfile_v0_0_1 profile072;
        profile072.slice = 50;
        profile072.step = 20;


        ImagingProfileWriter_v0_0_1 writer;
        writer.SetPath("imagingProfileTest");
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        CHECK(writer.Write() == true);
    }
}