#include <catch2/catch.hpp>

#include "HTProcessingProfileWriter_v1_4_1_c.h"

using namespace TC::IO::ProfileIO;

namespace HTProcessingProfileWriter_v1_4_1_cTest {
    TEST_CASE("HTProcessingProfileWriter_v1_4_1_c : unit test") {
        SECTION("SetPath() / SetProfile() / Write()") {
            HTProcessingProfileWriter_v1_4_1_c htProcessingProfileWriter;

            SECTION("Write() without path") {
                htProcessingProfileWriter.SetProfile({});
                CHECK(htProcessingProfileWriter.Write() == false);
            }

            SECTION("Write() without profile") {
                htProcessingProfileWriter.SetPath("test");
                CHECK(htProcessingProfileWriter.Write() == false);
            }

            SECTION("Write()") {
                TC::HTProcessingProfile::HTProcessingProfile_v1_4_1_c profile;
                profile.p01SupportDC = 1;
                profile.p02SupportDC = 2;
                profile.p01NonNegRef = 3;
                profile.p02NonNegRef = 4;
                profile.p01OuterIterNum = 5;
                profile.p01InnerIterNum = 6;
                profile.p02OuterIterNum = 7;
                profile.p02InnerIterNum = 8;
                profile.p01TvParam = 9;
                profile.p02TvParam = 10;
                profile.resetGpParam4LastIter = true;
                profile.p01SzRatioAxi = 11;
                profile.p01SzRatioLat = 12;
                profile.lateralMinSizeOffset = 13;
                profile.sfcGap01 = 14;
                profile.sfcGap02 = 15;
                profile.sfcGap03 = 16;
                profile.p01NormalizationFactor = 17;
                profile.p02NormalizationFactor = 18;
                profile.p02PreserveOriginalSfcData = false;
                profile.memoryUtilizationRatio = 19;


                htProcessingProfileWriter.SetPath("HTProcessingProfileWriter_v1_4_1_c.ini");
                htProcessingProfileWriter.SetProfile(profile);

                CHECK(htProcessingProfileWriter.Write() == true);
            }
        }
    }
}