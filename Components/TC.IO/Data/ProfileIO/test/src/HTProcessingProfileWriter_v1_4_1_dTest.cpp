#include <catch2/catch.hpp>

#include "HTProcessingProfileWriter_v1_4_1_d.h"

namespace HTProcessingProfileWriter_v1_4_1_dTest {
    using namespace TC::HTProcessingProfile;
    using namespace TC::IO::ProfileIO;

    TEST_CASE("HTProcessingProfileWriter_v1_4_1_dTest") {
        HTProcessingProfile_v1_4_1_d profileDefault;
        profileDefault.enableRegularization = true;
        profileDefault.p01SupportDC = 1;
        profileDefault.p02SupportDC = 2;
        profileDefault.p01NonNegRef = 3;
        profileDefault.p02NonNegRef = 4;
        profileDefault.p01OuterIterNum = 5;
        profileDefault.p01InnerIterNum = 6;
        profileDefault.p02OuterIterNum = 7;
        profileDefault.p02InnerIterNum = 8;
        profileDefault.p01TvParam = 9;
        profileDefault.p02TvParam = 10;
        profileDefault.resetGpParam4LastIter = false;
        profileDefault.p01SzRatioAxi = 12;
        profileDefault.p01SzRatioLat = 13;
        profileDefault.lateralMinSizeOffset = 14;
        profileDefault.sfcGap01 = 15;
        profileDefault.sfcGap02 = 16;
        profileDefault.sfcGap03 = 17;
        profileDefault.p01NormalizationFactor = 18;
        profileDefault.p02NormalizationFactor = 19;
        profileDefault.p02PreserveOriginalSfcData = false;
        profileDefault.memoryUtilizationRatio = 21;

        HTProcessingProfile_v1_4_1_d profile038 = profileDefault;
        profile038.p01SupportDC = 10;
        profile038.p02SupportDC = 20;

        HTProcessingProfile_v1_4_1_d profile054 = profileDefault;
        profile054.p01OuterIterNum = 50;
        profile054.p01InnerIterNum = 60;

        HTProcessingProfile_v1_4_1_d profile068 = profileDefault;
        profile068.p01SzRatioAxi = 120;
        profile068.p01SzRatioLat = 130;

        HTProcessingProfile_v1_4_1_d profile072 = profileDefault;
        profile072.p02PreserveOriginalSfcData = true;
        profile072.memoryUtilizationRatio = 210;


        HTProcessingProfileWriter_v1_4_1_d writer;
        writer.SetPath("htProcessingProfileTest");
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        CHECK(writer.Write() == true);
    }
}