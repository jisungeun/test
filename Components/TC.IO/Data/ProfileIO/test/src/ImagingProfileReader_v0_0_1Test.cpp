#include <QList>
#include <catch2/catch.hpp>

#include <QFile>

#include "ImagingProfileWriter_v0_0_1.h"
#include "ImagingProfileReader_v0_0_1.h"

namespace ImagingProfileWriter_v0_0_1Test {
    using namespace TC::ImagingProfile;
    using namespace TC::IO::ProfileIO;

    auto WriteTestFile(const QString& path) {
        ImagingProfile_v0_0_1 profileDefault;
        profileDefault.slice = 60;
        profileDefault.step = 20;

        ImagingProfile_v0_0_1 profile038;
        profile038.slice = 60;
        profile038.step = 40;

        ImagingProfile_v0_0_1 profile054;
        profile054.slice = 60;
        profile054.step = 30;

        ImagingProfile_v0_0_1 profile068;
        profile068.slice = 70;
        profile068.step = 20;

        ImagingProfile_v0_0_1 profile072;
        profile072.slice = 50;
        profile072.step = 20;


        ImagingProfileWriter_v0_0_1 writer;
        writer.SetPath(path);
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        writer.Write();
    }

    TEST_CASE("ImagingProfileReader_v0_0_1Test") {
        const auto path = "imagingProfileReading";
        if (QFile::exists(path)) {
            QFile::remove(path);
        }

        WriteTestFile(path);

        ImagingProfileReader_v0_0_1 reader;
        reader.SetPath(path);
        CHECK(reader.Read() == true);

        const auto supportedNAList = reader.GetSupportedNAList();
        CHECK(supportedNAList.size() == 4);
        CHECK(supportedNAList.contains(0.38) == true);
        CHECK(supportedNAList.contains(0.54) == true);
        CHECK(supportedNAList.contains(0.68) == true);
        CHECK(supportedNAList.contains(0.72) == true);

        const auto defaultProfile = reader.GetProfile();
        CHECK(defaultProfile.slice == 60);
        CHECK(defaultProfile.step == 20);

        const auto profile038 = reader.GetProfile(0.38);
        CHECK(profile038.slice == 60);
        CHECK(profile038.step == 40);

        const auto profile054 = reader.GetProfile(0.54);
        CHECK(profile054.slice == 60);
        CHECK(profile054.step == 30);

        const auto profile068 = reader.GetProfile(0.68);
        CHECK(profile068.slice == 70);
        CHECK(profile068.step == 20);

        const auto profile072 = reader.GetProfile(0.72);
        CHECK(profile072.slice == 50);
        CHECK(profile072.step == 20);
    }
}