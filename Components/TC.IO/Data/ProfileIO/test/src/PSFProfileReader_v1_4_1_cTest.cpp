#include <QFile>
#include <catch2/catch.hpp>

#include "PSFProfileReader_v1_4_1_c.h"
#include "PSFProfileWriter_v1_4_1_c.h"

namespace PSFProfileReader_v1_4_1_cTest {
    using namespace TC::PSFProfile;
    using namespace TC::IO::ProfileIO;

    auto WriteTestFile(const QString& path)->bool {
        PSFProfile_v1_4_1_c profileDefault;
        profileDefault.useRealPartOnly = true;
        profileDefault.psfSizeX = 1;
        profileDefault.psfSizeY = 2;
        profileDefault.psfSizeZ = 3;
        profileDefault.denoiseThres = "4";
        profileDefault.zComputationSplit = 5;
        profileDefault.secondCropNaObj = 6.66666666f;
        profileDefault.secondCropNaCond = 7.77777777f;
        profileDefault.secondCropNAObj02 = 8.88888888f;
        profileDefault.secondCropNACond02 = 9.99999999f;
        profileDefault.spectrumSize = 10;
        profileDefault.filterSnrSupport = false;
        profileDefault.axialUpSampleRatio = 11.11111111f;
        profileDefault.lateralVoxelCount = 12;

        PSFProfile_v1_4_1_c profile038 = profileDefault;
        profile038.useRealPartOnly = false;
        profile038.psfSizeX = 10;
        profile038.psfSizeY = 20;

        PSFProfile_v1_4_1_c profile054 = profileDefault;
        profile054.psfSizeZ = 30;
        profile054.denoiseThres = "40";
        profile054.zComputationSplit = 50;

        PSFProfile_v1_4_1_c profile068 = profileDefault;
        profile068.secondCropNaObj = 60.606060f;
        profile068.secondCropNaCond = 70.707070f;
        profile068.secondCropNAObj02 = 80.808080f;
        profile068.secondCropNACond02 = 90.909090f;

        PSFProfile_v1_4_1_c profile072 = profileDefault;
        profile072.spectrumSize = 100;
        profile072.filterSnrSupport = true;
        profile072.axialUpSampleRatio = 110.110110110f;
        profile072.lateralVoxelCount = 120;
        profile072.psfSizeX = 20;

        PSFProfileWriter_v1_4_1_c writer;
        writer.SetPath(path);
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        return writer.Write();
    }

    TEST_CASE("PSFProfileReader_v1_4_1_cTest") {
        const auto path = "psfProfileReading";

        if (QFile::exists(path)) {
            QFile::remove(path);
        }

        CHECK(WriteTestFile(path) == true);

        PSFProfileReader_v1_4_1_c reader;
        reader.SetPath(path);
        CHECK(reader.Read() == true);

        const auto supportedNAList = reader.GetSupportedNAList();
        CHECK(supportedNAList.size() == 4);
        CHECK(supportedNAList.contains(0.38) == true);
        CHECK(supportedNAList.contains(0.54) == true);
        CHECK(supportedNAList.contains(0.68) == true);
        CHECK(supportedNAList.contains(0.72) == true);

        const auto defaultProfile = reader.GetProfile();
        CHECK(defaultProfile.useRealPartOnly == true);
        CHECK(defaultProfile.psfSizeX == 1);
        CHECK(defaultProfile.psfSizeY == 2);
        CHECK(defaultProfile.psfSizeZ == 3);
        CHECK(defaultProfile.denoiseThres == "4");
        CHECK(defaultProfile.zComputationSplit == 5);
        CHECK(defaultProfile.secondCropNaObj == 6.66666666f);
        CHECK(defaultProfile.secondCropNaCond == 7.77777777f);
        CHECK(defaultProfile.secondCropNAObj02 == 8.88888888f);
        CHECK(defaultProfile.secondCropNACond02 == 9.99999999f);
        CHECK(defaultProfile.spectrumSize == 10);
        CHECK(defaultProfile.filterSnrSupport == false);
        CHECK(defaultProfile.axialUpSampleRatio == 11.11111111f);
        CHECK(defaultProfile.lateralVoxelCount == 12);

        const auto profile038 = reader.GetProfile(0.38);
        CHECK(profile038.useRealPartOnly == false);
        CHECK(profile038.psfSizeX == 10);
        CHECK(profile038.psfSizeY == 20);
        CHECK(profile038.zComputationSplit == 5);

        const auto profile054 = reader.GetProfile(0.54);
        CHECK(profile054.psfSizeZ == 30);
        CHECK(profile054.denoiseThres == "40");
        CHECK(profile054.zComputationSplit == 50);
        CHECK(profile054.psfSizeX == 1);

        const auto profile068 = reader.GetProfile(0.68);
        CHECK(profile068.secondCropNaObj == 60.606060f);
        CHECK(profile068.secondCropNaCond == 70.707070f);
        CHECK(profile068.secondCropNAObj02 == 80.808080f);
        CHECK(profile068.secondCropNACond02 == 90.909090f);
        CHECK(profile068.psfSizeX == 1);

        const auto profile072 = reader.GetProfile(0.72);
        CHECK(profile072.spectrumSize == 100);
        CHECK(profile072.filterSnrSupport == true);
        CHECK(profile072.axialUpSampleRatio == 110.110110110f);
        CHECK(profile072.lateralVoxelCount == 120);
        CHECK(profile072.psfSizeX == 20);
        CHECK(profile072.psfSizeY == 2);

    }
}
