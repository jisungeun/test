#include <catch2/catch.hpp>

#include "PSFProfileWriter_v1_4_1_c.h"

namespace PSFProfileWriter_v1_4_1_cTest {
    using namespace TC::PSFProfile;
    using namespace TC::IO::ProfileIO;

    TEST_CASE("PSFProfileWriter_v1_4_1_cTest") {
        PSFProfile_v1_4_1_c profileDefault;
        profileDefault.useRealPartOnly = true;
        profileDefault.psfSizeX = 1;
        profileDefault.psfSizeY = 2;
        profileDefault.psfSizeZ = 3;
        profileDefault.denoiseThres = "4";
        profileDefault.zComputationSplit = 5;
        profileDefault.secondCropNaObj = 6.66666666f;
        profileDefault.secondCropNaCond = 7.77777777f;
        profileDefault.secondCropNAObj02 = 8.88888888f;
        profileDefault.secondCropNACond02 = 9.99999999f;
        profileDefault.spectrumSize = 10;
        profileDefault.filterSnrSupport = false;
        profileDefault.axialUpSampleRatio = 11.11111111f;
        profileDefault.lateralVoxelCount = 12;

        PSFProfile_v1_4_1_c profile038 = profileDefault;
        profile038.useRealPartOnly = false;
        profile038.psfSizeX = 10;
        profile038.psfSizeY = 20;

        PSFProfile_v1_4_1_c profile054 = profileDefault;
        profile054.psfSizeZ = 30;
        profile054.denoiseThres = "40";
        profile054.zComputationSplit = 50;

        PSFProfile_v1_4_1_c profile068 = profileDefault;
        profile068.secondCropNaObj = 60.606060f;
        profile068.secondCropNaCond = 70.707070f;
        profile068.secondCropNAObj02 = 80.808080f;
        profile068.secondCropNACond02 = 90.909090f;

        PSFProfile_v1_4_1_c profile072 = profileDefault;
        profile072.spectrumSize = 100;
        profile072.filterSnrSupport = true;
        profile072.axialUpSampleRatio = 110.110110110f;
        profile072.lateralVoxelCount = 120;
        profile072.psfSizeX = 20;

        PSFProfileWriter_v1_4_1_c writer;
        writer.SetPath("psfProfileTest");
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        CHECK(writer.Write() == true);
    }
}
