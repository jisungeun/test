#include <QFile>
#include <catch2/catch.hpp>

#include <QList>

#include "HTProcessingProfileWriter_v1_4_1_d.h"
#include "HTProcessingProfileReader_v1_4_1_d.h"

namespace HTProcessingProfileReader_v1_4_1_dTest {
    using namespace TC::HTProcessingProfile;
    using namespace TC::IO::ProfileIO;

    auto WriteTestFile(const QString& path) ->void {
        HTProcessingProfile_v1_4_1_d profileDefault;
        profileDefault.enableRegularization = true;
        profileDefault.p01SupportDC = 1.11111111f;
        profileDefault.p02SupportDC = 2.22222222f;
        profileDefault.p01NonNegRef = 3.33333333f;
        profileDefault.p02NonNegRef = 4.44444444f;
        profileDefault.p01OuterIterNum = 5;
        profileDefault.p01InnerIterNum = 6;
        profileDefault.p02OuterIterNum = 7;
        profileDefault.p02InnerIterNum = 8;
        profileDefault.p01TvParam = 9.99999999f;
        profileDefault.p02TvParam = 10.10101010f;
        profileDefault.resetGpParam4LastIter = false;
        profileDefault.p01SzRatioAxi = 12.12121212f;
        profileDefault.p01SzRatioLat = 13.13131313f;
        profileDefault.lateralMinSizeOffset = 14;
        profileDefault.sfcGap01 = 15.15151515f;
        profileDefault.sfcGap02 = 16.16161616f;
        profileDefault.sfcGap03 = 17.17171717f;
        profileDefault.p01NormalizationFactor = 18.18181818f;
        profileDefault.p02NormalizationFactor = 19.19191919f;
        profileDefault.p02PreserveOriginalSfcData = false;
        profileDefault.memoryUtilizationRatio = 21.21212121f;

        HTProcessingProfile_v1_4_1_d profile038 = profileDefault;
        profile038.p01SupportDC = 10.10101010f;
        profile038.p02SupportDC = 20.20202020f;

        HTProcessingProfile_v1_4_1_d profile054 = profileDefault;
        profile054.p01OuterIterNum = 50;
        profile054.p01InnerIterNum = 60;

        HTProcessingProfile_v1_4_1_d profile068 = profileDefault;
        profile068.p01SzRatioAxi = 120.12012012f;
        profile068.p01SzRatioLat = 130.13013013f;

        HTProcessingProfile_v1_4_1_d profile072 = profileDefault;
        profile072.p02PreserveOriginalSfcData = true;
        profile072.memoryUtilizationRatio = 210.21021021f;


        HTProcessingProfileWriter_v1_4_1_d writer;
        writer.SetPath(path);
        writer.SetDefaultProfile(profileDefault);
        writer.AddProfile(0.38, profile038);
        writer.AddProfile(0.54, profile054);
        writer.AddProfile(0.68, profile068);
        writer.AddProfile(0.72, profile072);

        CHECK(writer.Write() == true);
    }

    TEST_CASE("HTProcessingProfileReader_v1_4_1_dTest") {
        const auto path = "htProcessingProfileReading";
        if (QFile::exists(path)) {
            QFile::remove(path);
        }

        WriteTestFile(path);

        HTProcessingProfileReader_v1_4_1_d reader;
        reader.SetPath(path);
        CHECK(reader.Read() == true);

        const auto supportedNAList = reader.GetSupportedNAList();
        CHECK(supportedNAList.size() == 4);
        CHECK(supportedNAList.contains(0.38) == true);
        CHECK(supportedNAList.contains(0.54) == true);
        CHECK(supportedNAList.contains(0.68) == true);
        CHECK(supportedNAList.contains(0.72) == true);

        const auto defaultProfile = reader.GetProfile();
        CHECK(defaultProfile.enableRegularization == true);
        CHECK(defaultProfile.p01SupportDC == 1.11111111f);
        CHECK(defaultProfile.p02SupportDC == 2.22222222f);
        CHECK(defaultProfile.p01NonNegRef == 3.33333333f);
        CHECK(defaultProfile.p02NonNegRef == 4.44444444f);
        CHECK(defaultProfile.p01OuterIterNum == 5);
        CHECK(defaultProfile.p01InnerIterNum == 6);
        CHECK(defaultProfile.p02OuterIterNum == 7);
        CHECK(defaultProfile.p02InnerIterNum == 8);
        CHECK(defaultProfile.p01TvParam == 9.99999999f);
        CHECK(defaultProfile.p02TvParam == 10.10101010f);
        CHECK(defaultProfile.resetGpParam4LastIter == false);
        CHECK(defaultProfile.p01SzRatioAxi == 12.12121212f);
        CHECK(defaultProfile.p01SzRatioLat == 13.13131313f);
        CHECK(defaultProfile.lateralMinSizeOffset == 14);
        CHECK(defaultProfile.sfcGap01 == 15.15151515f);
        CHECK(defaultProfile.sfcGap02 == 16.16161616f);
        CHECK(defaultProfile.sfcGap03 == 17.17171717f);
        CHECK(defaultProfile.p01NormalizationFactor == 18.18181818f);
        CHECK(defaultProfile.p02NormalizationFactor == 19.19191919f);
        CHECK(defaultProfile.p02PreserveOriginalSfcData == false);
        CHECK(defaultProfile.memoryUtilizationRatio == 21.21212121f);

        const auto profile038 = reader.GetProfile(0.38);
        CHECK(profile038.p01SupportDC == 10.10101010f);
        CHECK(profile038.p02SupportDC == 20.20202020f);

        const auto profile054 = reader.GetProfile(0.54);
        CHECK(profile054.p01OuterIterNum == 50);
        CHECK(profile054.p01InnerIterNum == 60);

        const auto profile068 = reader.GetProfile(0.68);
        CHECK(profile068.p01SzRatioAxi == 120.12012012f);
        CHECK(profile068.p01SzRatioLat == 130.13013013f);

        const auto profile072 = reader.GetProfile(0.72);
        CHECK(profile072.p02PreserveOriginalSfcData == true);
        CHECK(profile072.memoryUtilizationRatio == 210.21021021f);
    }
}