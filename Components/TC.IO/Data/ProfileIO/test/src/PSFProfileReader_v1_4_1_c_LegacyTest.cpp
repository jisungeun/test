#include <QFile>
#include <catch2/catch.hpp>

#include "PSFProfileReader_v1_4_1_c_Legacy.h"
#include "PSFProfileWriter_v1_4_1_c_Legacy.h"

using namespace TC::IO::ProfileIO;

namespace PSFProfileReader_v1_4_1_cTest {
    TEST_CASE("PSFProfileReader_v1_4_1_c : unit test") {
        SECTION("SetPath() / Read() / GetProfile()") {
            PSFProfileReader_v1_4_1_c_Legacy reader;

            SECTION("Read() without setting path") {
                CHECK(reader.Read() == false);
            }

            SECTION("SetPath() / Read() / GetProfile() with normal input") {
                const QString profilePath = "profile_v1_4_1_c.ini";
                if (QFile::exists(profilePath)) {
                    QFile::remove(profilePath);
                }

                TC::PSFProfile::PSFProfile_v1_4_1_c profile;
                profile.useRealPartOnly = true;
                profile.psfSizeX = 1;
                profile.psfSizeY = 2;
                profile.psfSizeZ = 3;
                profile.denoiseThres = "4";
                profile.zComputationSplit = 5;
                profile.secondCropNaObj = 6;
                profile.secondCropNaCond = 7;
                profile.secondCropNAObj02 = 8;
                profile.secondCropNACond02 = 9;
                profile.spectrumSize = 10;
                profile.filterSnrSupport = true;
                profile.axialUpSampleRatio = 11;
                profile.lateralVoxelCount = 12;

                PSFProfileWriter_v1_4_1_c_Legacy writer;
                writer.SetPath(profilePath);
                writer.SetProfile(profile);
                REQUIRE(writer.Write() == true);

                reader.SetPath(profilePath);
                CHECK(reader.Read() == true);

                const auto resultProfile = reader.GetProfile();
                CHECK(resultProfile.useRealPartOnly == true);
                CHECK(resultProfile.psfSizeX == 1);
                CHECK(resultProfile.psfSizeY == 2);
                CHECK(resultProfile.psfSizeZ == 3);
                CHECK(resultProfile.denoiseThres == "4");
                CHECK(resultProfile.zComputationSplit == 5);
                CHECK(resultProfile.secondCropNaObj == 6);
                CHECK(resultProfile.secondCropNaCond == 7);
                CHECK(resultProfile.secondCropNAObj02 == 8);
                CHECK(resultProfile.secondCropNACond02 == 9);
                CHECK(resultProfile.spectrumSize == 10);
                CHECK(resultProfile.filterSnrSupport == true);
                CHECK(resultProfile.axialUpSampleRatio == 11);
                CHECK(resultProfile.lateralVoxelCount == 12);
            }
        }
    }
}