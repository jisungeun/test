#include <catch2/catch.hpp>

#include "IPSFProfileWriter.h"

using namespace TC::IO::ProfileIO;

namespace IPSFProfileWriterTest {
    class HTProcessingProfileWriterForTest final : public IPSFProfileWriter {
    public:
        HTProcessingProfileWriterForTest() = default;
        ~HTProcessingProfileWriterForTest() override = default;

        auto SetPath(const QString&) -> void override {
            this->setPathTriggered = true;
        }
        auto Write() -> bool override {
            this->writeTriggered = true;
            return true;
        }

        bool setPathTriggered{ false };
        bool writeTriggered{ false };
    };

    TEST_CASE("IPSFProfileWriter : unit test") {
        SECTION("SetPath()") {
            HTProcessingProfileWriterForTest psfProfileWriter;
            CHECK(psfProfileWriter.setPathTriggered == false);
            psfProfileWriter.SetPath("");
            CHECK(psfProfileWriter.setPathTriggered == true);
        }

        SECTION("Write()") {
            HTProcessingProfileWriterForTest psfProfileWriter;
            CHECK(psfProfileWriter.writeTriggered == false);
            psfProfileWriter.Write();
            CHECK(psfProfileWriter.writeTriggered == true);
        }
    }
}