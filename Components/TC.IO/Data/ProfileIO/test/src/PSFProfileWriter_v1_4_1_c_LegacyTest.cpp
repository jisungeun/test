#include <catch2/catch.hpp>

#include "PSFProfileWriter_v1_4_1_c_Legacy.h"

using namespace TC::IO::ProfileIO;

namespace PSFProfileWriter_v1_4_1_cTest {
    TEST_CASE("PSFProfileWriter_v1_4_1_c : unit test") {
        SECTION("SetPath() / SetProfile() / Write()") {
            PSFProfileWriter_v1_4_1_c_Legacy psfProfileWriter;

            SECTION("Write() without path") {
                psfProfileWriter.SetProfile({});
                CHECK(psfProfileWriter.Write() == false);
            }

            SECTION("Write() without profile") {
                psfProfileWriter.SetPath("test");
                CHECK(psfProfileWriter.Write() == false);
            }

            SECTION("Write()") {
                TC::PSFProfile::PSFProfile_v1_4_1_c profile;
                profile.useRealPartOnly = true;
                profile.psfSizeX = 1;
                profile.psfSizeY = 2;
                profile.psfSizeZ = 3;
                profile.denoiseThres = "4";
                profile.zComputationSplit = 5;
                profile.secondCropNaObj = 6;
                profile.secondCropNaCond = 7;
                profile.secondCropNAObj02 = 8;
                profile.secondCropNACond02 = 9;
                profile.spectrumSize = 10;
                profile.filterSnrSupport = true;
                profile.axialUpSampleRatio = 11;
                profile.lateralVoxelCount = 12;

                psfProfileWriter.SetPath("psfProfile_v1_4_1_c.ini");
                psfProfileWriter.SetProfile(profile);

                CHECK(psfProfileWriter.Write() == true);
            }
        }
    }
}