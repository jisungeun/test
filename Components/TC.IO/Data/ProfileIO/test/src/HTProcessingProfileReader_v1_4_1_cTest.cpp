#include <catch2/catch.hpp>

#include <QFile>

#include "HTProcessingProfileReader_v1_4_1_c.h"
#include "HTProcessingProfileWriter_v1_4_1_c.h"

using namespace TC::IO::ProfileIO;

namespace HTProcessingProfileReader_v1_4_1_cTest {
    TEST_CASE("HTProcessingProfileReader_v1_4_1_c : unit test") {
        SECTION("SetPath() / Read() / GetProfile()") {
            HTProcessingProfileReader_v1_4_1_c reader;

            SECTION("Read() without setting path") {
                CHECK(reader.Read() == false);
            }

            SECTION("SetPath() / Read() / GetProfile() with normal input") {
                const QString profilePath = "profile_v1_4_1_c.ini";
                if (QFile::exists(profilePath)) {
                    QFile::remove(profilePath);
                }

                TC::HTProcessingProfile::HTProcessingProfile_v1_4_1_c profile;
                profile.p01SupportDC = 1;
                profile.p02SupportDC = 2;
                profile.p01NonNegRef = 3;
                profile.p02NonNegRef = 4;
                profile.p01OuterIterNum = 5;
                profile.p01InnerIterNum = 6;
                profile.p02OuterIterNum = 7;
                profile.p02InnerIterNum = 8;
                profile.p01TvParam = 9;
                profile.p02TvParam = 10;
                profile.resetGpParam4LastIter = true;
                profile.p01SzRatioAxi = 11;
                profile.p01SzRatioLat = 12;
                profile.lateralMinSizeOffset = 13;
                profile.sfcGap01 = 14;
                profile.sfcGap02 = 15;
                profile.sfcGap03 = 16;
                profile.p01NormalizationFactor = 17;
                profile.p02NormalizationFactor = 18;
                profile.p02PreserveOriginalSfcData = false;
                profile.memoryUtilizationRatio = 19;
                
                HTProcessingProfileWriter_v1_4_1_c writer;
                writer.SetPath(profilePath);
                writer.SetProfile(profile);
                REQUIRE(writer.Write() == true);

                reader.SetPath(profilePath);
                CHECK(reader.Read() == true);

                const auto resultProfile = reader.GetProfile();
                CHECK(resultProfile.p01SupportDC == 1);
                CHECK(resultProfile.p02SupportDC == 2);
                CHECK(resultProfile.p01NonNegRef == 3);
                CHECK(resultProfile.p02NonNegRef == 4);
                CHECK(resultProfile.p01OuterIterNum == 5);
                CHECK(resultProfile.p01InnerIterNum == 6);
                CHECK(resultProfile.p02OuterIterNum == 7);
                CHECK(resultProfile.p02InnerIterNum == 8);
                CHECK(resultProfile.p01TvParam == 9);
                CHECK(resultProfile.p02TvParam == 10);
                CHECK(resultProfile.resetGpParam4LastIter == true);
                CHECK(resultProfile.p01SzRatioAxi == 11);
                CHECK(resultProfile.p01SzRatioLat == 12);
                CHECK(resultProfile.lateralMinSizeOffset == 13);
                CHECK(resultProfile.sfcGap01 == 14);
                CHECK(resultProfile.sfcGap02 == 15);
                CHECK(resultProfile.sfcGap03 == 16);
                CHECK(resultProfile.p01NormalizationFactor == 17);
                CHECK(resultProfile.p02NormalizationFactor == 18);
                CHECK(resultProfile.p02PreserveOriginalSfcData == false);
                CHECK(resultProfile.memoryUtilizationRatio == 19);
            }
        }
    }
}