#pragma once

#include <memory>

#include <QString>

#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API IPSFProfileWriter {
    public:
        using Pointer = std::shared_ptr<IPSFProfileWriter>;
        virtual ~IPSFProfileWriter();

        virtual auto SetPath(const QString& path)->void = 0;
        virtual auto Write()->bool = 0;
    };
}