#pragma once

#include <memory>

#include <HTProcessingProfile_v1_4_1_c.h>

#include "TCProfileIOExport.h"
#include "IHTProcessingProfileWriter.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API HTProcessingProfileWriter_v1_4_1_c final : public IHTProcessingProfileWriter{
    public:
        HTProcessingProfileWriter_v1_4_1_c();
        ~HTProcessingProfileWriter_v1_4_1_c();

        auto SetPath(const QString& path) -> void override;
        auto SetProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_c& profile)->void;
        auto Write() -> bool override;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}