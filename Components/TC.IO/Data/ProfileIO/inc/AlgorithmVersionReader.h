#pragma once

#include <memory>
#include <QString>

#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API AlgorithmVersionReader {
    public:
        AlgorithmVersionReader();
        ~AlgorithmVersionReader();

        auto SetFilePath(const QString& profileFilePath)->void;
        auto Read()->bool;
        auto GetVersion()->QString;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}