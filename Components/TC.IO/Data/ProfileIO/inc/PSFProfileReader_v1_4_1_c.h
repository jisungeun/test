#pragma once

#include <memory>

#include <QList>

#include <PSFProfile_v1_4_1_c.h>

#include "IPSFProfileReader.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API PSFProfileReader_v1_4_1_c : public IPSFProfileReader {
    public:
        PSFProfileReader_v1_4_1_c();
        ~PSFProfileReader_v1_4_1_c();

        auto SetPath(const QString& path) -> void override;
        auto Read() -> bool override;
        auto GetSupportedNAList()->QList<float>;

        auto GetProfile(const float& na = 0)const->PSFProfile::PSFProfile_v1_4_1_c;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}