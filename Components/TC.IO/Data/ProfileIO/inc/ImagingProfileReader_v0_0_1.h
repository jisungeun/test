#pragma once

#include <memory>

#include "IImagingProfileReader.h"
#include "ImagingProfile_v0_0_1.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API ImagingProfileReader_v0_0_1 final : public IImagingProfileReader {
    public:
        ImagingProfileReader_v0_0_1();
        ~ImagingProfileReader_v0_0_1();

        auto SetPath(const QString& path) -> void override;
        auto Read() -> bool override;
        auto GetSupportedNAList()->QList<float>;

        auto GetProfile(const float& na = 0)const->ImagingProfile::ImagingProfile_v0_0_1;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}