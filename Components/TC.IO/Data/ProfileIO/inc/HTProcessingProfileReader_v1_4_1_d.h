#pragma once

#include <memory>

#include "IHTProcessingProfileReader.h"
#include "HTProcessingProfile_v1_4_1_d.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API HTProcessingProfileReader_v1_4_1_d final : public IHTProcessingProfileReader{
    public:
        HTProcessingProfileReader_v1_4_1_d();
        ~HTProcessingProfileReader_v1_4_1_d();

        auto SetPath(const QString& path) -> void override;
        auto Read() -> bool override;
        auto GetSupportedNAList()->QList<float>;

        auto GetProfile(const float& na = 0)const-> HTProcessingProfile::HTProcessingProfile_v1_4_1_d;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}