#pragma once

#include <memory>

#include <PSFProfile_v1_4_1_c.h>

#include "IPSFProfileWriter.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API PSFProfileWriter_v1_4_1_c : public IPSFProfileWriter {
    public:
        PSFProfileWriter_v1_4_1_c();
        ~PSFProfileWriter_v1_4_1_c();

        auto SetPath(const QString& path) -> void override;

        auto SetDefaultProfile(const PSFProfile::PSFProfile_v1_4_1_c& profile)->void;
        auto AddProfile(const float& condenserNA, const PSFProfile::PSFProfile_v1_4_1_c& profile)->void;

        auto Write() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}