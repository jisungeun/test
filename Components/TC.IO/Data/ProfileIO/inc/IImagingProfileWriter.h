#pragma once

#include <memory>

#include <QString>

#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API IImagingProfileWriter {
    public:
        using Pointer = std::shared_ptr<IImagingProfileWriter>;
        virtual ~IImagingProfileWriter();

        virtual auto SetPath(const QString& path)->void = 0;
        virtual auto Write()->bool = 0;
    };
}