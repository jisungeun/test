#pragma once

#include <memory>

#include <ImagingProfile_v0_0_1.h>

#include "IImagingProfileWriter.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API ImagingProfileWriter_v0_0_1 : public IImagingProfileWriter {
    public:
        ImagingProfileWriter_v0_0_1();
        ~ImagingProfileWriter_v0_0_1();

        auto SetPath(const QString& path) -> void override;

        auto SetDefaultProfile(const ImagingProfile::ImagingProfile_v0_0_1& profile)->void;
        auto AddProfile(const float& condenserNA, const ImagingProfile::ImagingProfile_v0_0_1& profile)->void;

        auto Write() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
