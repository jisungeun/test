#pragma once

#include <memory>

#include <QString>

#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API IHTProcessingProfileWriter {
    public:
        using Pointer = std::shared_ptr<IHTProcessingProfileWriter>;
        virtual ~IHTProcessingProfileWriter();

        virtual auto SetPath(const QString& path)->void = 0;
        virtual auto Write()->bool = 0;
    };
}