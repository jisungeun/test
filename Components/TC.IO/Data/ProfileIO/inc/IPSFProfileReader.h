#pragma once

#include <memory>

#include <QString>

#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API IPSFProfileReader {
    public:
        using Pointer = std::shared_ptr<IPSFProfileReader>;
        virtual ~IPSFProfileReader();

        virtual auto SetPath(const QString& path)->void = 0;
        virtual auto Read()->bool = 0;
    };
}