#pragma once

#include <memory>

#include <PSFProfile_v1_4_1_c.h>

#include "IPSFProfileReader.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API PSFProfileReader_v1_4_1_c_Legacy final : public IPSFProfileReader{
    public:
        PSFProfileReader_v1_4_1_c_Legacy();
        ~PSFProfileReader_v1_4_1_c_Legacy();

        auto SetPath(const QString& path) -> void override;
        auto Read() -> bool override;
        auto GetProfile()-> PSFProfile::PSFProfile_v1_4_1_c;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}