#pragma once

#include <memory>

#include <HTProcessingProfile_v1_4_1_d.h>

#include "IHTProcessingProfileWriter.h"
#include "TCProfileIOExport.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API HTProcessingProfileWriter_v1_4_1_d : public IHTProcessingProfileWriter {
    public:
        HTProcessingProfileWriter_v1_4_1_d();
        ~HTProcessingProfileWriter_v1_4_1_d();

        auto SetPath(const QString& path) -> void override;

        auto SetDefaultProfile(const HTProcessingProfile::HTProcessingProfile_v1_4_1_d& profile)->void;
        auto AddProfile(const float& condenserNA, const HTProcessingProfile::HTProcessingProfile_v1_4_1_d& profile)->void;

        auto Write() -> bool override;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
