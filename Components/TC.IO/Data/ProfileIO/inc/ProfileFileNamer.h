#pragma once

#include <memory>
#include <QString>

#include "TCProfileIOExport.h"

#include "PSFProfileVersion.h"
#include "HTProcessingProfileVersion.h"
#include "ImagingProfileVersion.h"

namespace TC::ProfileIO {
    class TCProfileIO_API ProfileFileNamer {
    public:
        static auto GetExtension(const PSFProfile::PSFProfileVersion& version)->QString;
        static auto GetExtension(const HTProcessingProfile::HTProcessingProfileVersion& version)->QString;
        static auto GetExtension(const ImagingProfile::ImagingProfileVersion& version)->QString;

        static auto AppendExtension(const QString& fileName, const PSFProfile::PSFProfileVersion& version)->QString;
        static auto AppendExtension(const QString& fileName, const HTProcessingProfile::HTProcessingProfileVersion& version)->QString;
        static auto AppendExtension(const QString& fileName, const ImagingProfile::ImagingProfileVersion& version)->QString;
    };
}