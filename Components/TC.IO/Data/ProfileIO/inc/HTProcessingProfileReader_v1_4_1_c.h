#pragma once

#include <QString>

#include <HTProcessingProfile_v1_4_1_c.h>

#include "TCProfileIOExport.h"
#include "IHTProcessingProfileReader.h"

namespace TC::IO::ProfileIO {
    class TCProfileIO_API HTProcessingProfileReader_v1_4_1_c final : public IHTProcessingProfileReader{
    public:
        HTProcessingProfileReader_v1_4_1_c();
        ~HTProcessingProfileReader_v1_4_1_c();
        
        auto SetPath(const QString& path) -> void override;
        auto Read() -> bool override;
        auto GetProfile()->HTProcessingProfile::HTProcessingProfile_v1_4_1_c;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
