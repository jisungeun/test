project(TCProfileIO)

#Header files for external use
set(INTERFACE_HEADERS
	inc/IPSFProfileWriter.h
	inc/PSFProfileWriter_v1_4_1_c.h
	inc/PSFProfileWriter_v1_4_1_c_Legacy.h

	inc/IHTProcessingProfileWriter.h
	inc/HTProcessingProfileWriter_v1_4_1_c.h
	inc/HTProcessingProfileWriter_v1_4_1_d.h

	inc/IPSFProfileReader.h
	inc/PSFProfileReader_v1_4_1_c.h
	inc/PSFProfileReader_v1_4_1_c_Legacy.h

	inc/IHTProcessingProfileReader.h
	inc/HTProcessingProfileReader_v1_4_1_c.h
	inc/HTProcessingProfileReader_v1_4_1_d.h

	inc/IImagingProfileWriter.h
	inc/ImagingProfileWriter_v0_0_1.h

	inc/IImagingProfileReader.h
	inc/ImagingProfileReader_v0_0_1.h

	inc/AlgorithmVersionReader.h
	inc/AlgorithmVersionReader_Legacy.h

	inc/ProfileFileNamer.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/IPSFProfileWriter.cpp
	src/PSFProfileWriter_v1_4_1_c.cpp
	src/PSFProfileWriter_v1_4_1_c_Legacy.cpp

	src/IHTProcessingProfileWriter.cpp
	src/HTProcessingProfileWriter_v1_4_1_c.cpp
	src/HTProcessingProfileWriter_v1_4_1_d.cpp

	src/IPSFProfileReader.cpp
	src/PSFProfileReader_v1_4_1_c.cpp
	src/PSFProfileReader_v1_4_1_c_Legacy.cpp

	src/IHTProcessingProfileReader.cpp
	src/HTProcessingProfileReader_v1_4_1_c.cpp
	src/HTProcessingProfileReader_v1_4_1_d.cpp

	src/IImagingProfileWriter.cpp
	src/ImagingProfileWriter_v0_0_1.cpp

	src/IImagingProfileReader.cpp
	src/ImagingProfileReader_v0_0_1.cpp

	src/AlgorithmVersionReader.cpp
	src/AlgorithmVersionReader_Legacy.cpp

	src/ProfileFileNamer.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TC::Components::ProfileIO ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
        Qt5::Core
		TC::Components::HTProcessingProfile
		TC::Components::PSFProfile
		TC::Components::ImagingProfile
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		$<$<COMPILE_LANGUAGE:C>:/wd4251>
		$<$<COMPILE_LANGUAGE:CXX>:/wd4251>
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/IO/Data")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT io)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)