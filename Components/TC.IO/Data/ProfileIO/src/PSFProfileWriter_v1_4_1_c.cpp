#include "PSFProfileWriter_v1_4_1_c.h"

#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QMap>
#include <QSettings>

using namespace TC::PSFProfile;

namespace TC::IO::ProfileIO {
    using NA = float;
    using NameString = QString;
    using Value = QVariant;

    using Profile = PSFProfile_v1_4_1_c;

    const QString version = "1.4.1c";

    const QStringList parameterNameList{
        "useRealPartOnly",     // 0
        "psfSizeX",            // 1
        "psfSizeY",            // 2
        "psfSizeZ",            // 3
        "denoiseThres",        // 4
        "zComputationSplit",   // 5
        "secondCropNaObj",     // 6
        "secondCropNaCond",    // 7
        "secondCropNAObj02",   // 8
        "secondCropNACond02",  // 9
        "spectrumSize",        // 10
        "filterSnrSupport",    // 11
        "axialUpSampleRatio",  // 12
        "lateralVoxelCount"    // 13
    };

    class PSFProfileWriter_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        Profile defaultProfile{};
        bool defaultSet{ false };

        QMap<NA, Profile> profileMap{};

        auto CheckInputs()->bool;
        auto GetDifferentParameters()->QMap<NA, QList<QPair<NameString, Value>>>;
    };

    auto PSFProfileWriter_v1_4_1_c::Impl::CheckInputs() -> bool {
        if (this->path.isEmpty()) {
            return false;
        }

        if (!this->defaultSet) {
            return false;
        }

        if (QFile::exists(this->path)) {
            return false;
        }

        if (!QFileInfo(this->path).dir().exists()) {
            return false;
        }

        return true;
    }

    auto PSFProfileWriter_v1_4_1_c::Impl::GetDifferentParameters() -> QMap<NA, QList<QPair<NameString, Value>>> {
        QMap<NA, QList<QPair<NameString, Value>>> diffParameters{};

        for (auto profileIter = this->profileMap.cbegin(); profileIter != this->profileMap.cend(); ++profileIter) {
            const auto na = profileIter.key();
            const auto profile = profileIter.value();

            QList<QPair<NameString, Value>> diffList;
            if (this->defaultProfile.useRealPartOnly != profile.useRealPartOnly) {
                diffList.push_back(QPair{ parameterNameList[0], profile.useRealPartOnly });
            }
            if (this->defaultProfile.psfSizeX != profile.psfSizeX) {
                diffList.push_back(QPair{ parameterNameList[1], profile.psfSizeX });
            }
            if (this->defaultProfile.psfSizeY != profile.psfSizeY) {
                diffList.push_back(QPair{ parameterNameList[2], profile.psfSizeY });
            }
            if (this->defaultProfile.psfSizeZ != profile.psfSizeZ) {
                diffList.push_back(QPair{ parameterNameList[3], profile.psfSizeZ });
            }
            if (this->defaultProfile.denoiseThres != profile.denoiseThres) {
                diffList.push_back(QPair{ parameterNameList[4], profile.denoiseThres });
            }
            if (this->defaultProfile.zComputationSplit != profile.zComputationSplit) {
                diffList.push_back(QPair{ parameterNameList[5], profile.zComputationSplit });
            }
            if (this->defaultProfile.secondCropNaObj != profile.secondCropNaObj) {
                diffList.push_back(QPair{ parameterNameList[6], static_cast<double>(profile.secondCropNaObj) });
            }
            if (this->defaultProfile.secondCropNaCond != profile.secondCropNaCond) {
                diffList.push_back(QPair{ parameterNameList[7], static_cast<double>(profile.secondCropNaCond) });
            }
            if (this->defaultProfile.secondCropNAObj02 != profile.secondCropNAObj02) {
                diffList.push_back(QPair{ parameterNameList[8], static_cast<double>(profile.secondCropNAObj02) });
            }
            if (this->defaultProfile.secondCropNACond02 != profile.secondCropNACond02) {
                diffList.push_back(QPair{ parameterNameList[9], static_cast<double>(profile.secondCropNACond02) });
            }
            if (this->defaultProfile.spectrumSize != profile.spectrumSize) {
                diffList.push_back(QPair{ parameterNameList[10], profile.spectrumSize });
            }
            if (this->defaultProfile.filterSnrSupport != profile.filterSnrSupport) {
                diffList.push_back(QPair{ parameterNameList[11], profile.filterSnrSupport });
            }
            if (this->defaultProfile.axialUpSampleRatio != profile.axialUpSampleRatio) {
                diffList.push_back(QPair{ parameterNameList[12], static_cast<double>(profile.axialUpSampleRatio) });
            }
            if (this->defaultProfile.lateralVoxelCount != profile.lateralVoxelCount) {
                diffList.push_back(QPair{ parameterNameList[13], profile.lateralVoxelCount });
            }
            diffParameters[na] = diffList;
        }

        return diffParameters;
    }

    PSFProfileWriter_v1_4_1_c::PSFProfileWriter_v1_4_1_c() : d{ std::make_unique<Impl>() } {
    }

    PSFProfileWriter_v1_4_1_c::~PSFProfileWriter_v1_4_1_c() = default;

    auto PSFProfileWriter_v1_4_1_c::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto PSFProfileWriter_v1_4_1_c::SetDefaultProfile(const Profile& profile) -> void {
        d->defaultProfile = profile;
        d->defaultSet = true;
    }

    auto PSFProfileWriter_v1_4_1_c::AddProfile(const float& condenserNA, const Profile& profile) -> void {
        if (d->profileMap.contains(condenserNA)) {
            d->profileMap.remove(condenserNA);
        }

        d->profileMap[condenserNA] = profile;
    }

    auto PSFProfileWriter_v1_4_1_c::Write() -> bool {
        if (!d->CheckInputs()) {
            return false;
        }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("AlgorithmVersion");
            profileFile.setValue("version", version);
            profileFile.endGroup();
        }
        {
            const auto naList = d->profileMap.keys();

            profileFile.beginGroup("SupportedNA");

            int32_t index = 0;
            for (const auto na : naList) {
                profileFile.beginGroup(QString::number(index));
                profileFile.setValue("NA", QString::number(na, 'g', 2));
                profileFile.endGroup();
                index++;
            }

            profileFile.setValue("size", naList.count());

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");
            profileFile.setValue(parameterNameList[0], d->defaultProfile.useRealPartOnly);
            profileFile.setValue(parameterNameList[1], d->defaultProfile.psfSizeX);
            profileFile.setValue(parameterNameList[2], d->defaultProfile.psfSizeY);
            profileFile.setValue(parameterNameList[3], d->defaultProfile.psfSizeZ);
            profileFile.setValue(parameterNameList[4], d->defaultProfile.denoiseThres);
            profileFile.setValue(parameterNameList[5], d->defaultProfile.zComputationSplit);
            profileFile.setValue(parameterNameList[6], static_cast<double>(d->defaultProfile.secondCropNaObj));
            profileFile.setValue(parameterNameList[7], static_cast<double>(d->defaultProfile.secondCropNaCond));
            profileFile.setValue(parameterNameList[8], static_cast<double>(d->defaultProfile.secondCropNAObj02));
            profileFile.setValue(parameterNameList[9], static_cast<double>(d->defaultProfile.secondCropNACond02));
            profileFile.setValue(parameterNameList[10], d->defaultProfile.spectrumSize);
            profileFile.setValue(parameterNameList[11], d->defaultProfile.filterSnrSupport);
            profileFile.setValue(parameterNameList[12], static_cast<double>(d->defaultProfile.axialUpSampleRatio));
            profileFile.setValue(parameterNameList[13], d->defaultProfile.lateralVoxelCount);

            profileFile.endGroup();
        }
        {
            const auto differentParameters = d->GetDifferentParameters();
            profileFile.beginGroup("VariableParametersPerNA");

            for (auto diffIter = differentParameters.cbegin(); diffIter != differentParameters.cend(); ++diffIter) {
                const auto na = diffIter.key();
                const auto differentPairList = diffIter.value();

                profileFile.beginGroup(QString::number(na, 'g', 2));
                for (const auto nameValuePair : differentPairList) {
                    const auto name = nameValuePair.first;
                    const auto value = nameValuePair.second;

                    profileFile.setValue(name, value);
                }
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }


        return true;
    }
}
