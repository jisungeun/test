#include "ImagingProfileWriter_v0_0_1.h"

#include <QFile>
#include <QMap>
#include <QSettings>

using namespace TC::ImagingProfile;

namespace TC::IO::ProfileIO {
    using NA = float;
    using NameString = QString;
    using Value = QString;

    const QString version = "0.0.1";

    const QStringList parameterNameList{ "Step", "Slices" };

    class ImagingProfileWriter_v0_0_1::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        ImagingProfile_v0_0_1 defaultProfile{};
        bool defaultSet{false};

        QMap<NA, ImagingProfile_v0_0_1> profileMap{};

        auto CheckInputs()->bool;
        auto GetDifferentParameters()->QMap<NA, QList<QPair<NameString, Value>>>;
    };

    auto ImagingProfileWriter_v0_0_1::Impl::CheckInputs() -> bool {
        if (this->path.isEmpty()) {
            return false;
        }

        if (!this->defaultSet) {
            return false;
        }

        if (QFile::exists(this->path)) {
            return false;
        }

        return true;
    }

    auto ImagingProfileWriter_v0_0_1::Impl::GetDifferentParameters() -> QMap<NA, QList<QPair<NameString, Value>>> {
        QMap<NA, QList<QPair<NameString, Value>>> diffParameters{};

        for (auto profileIter = this->profileMap.cbegin(); profileIter != this->profileMap.cend(); ++profileIter) {
            const auto na = profileIter.key();
            const auto profile = profileIter.value();

            QList<QPair<NameString, Value>> diffList;

            if (this->defaultProfile.step != profile.step) {
                diffList.push_back(QPair{ parameterNameList[0], QString::number(profile.step) });
            }

            if (this->defaultProfile.slice != profile.slice) {
                diffList.push_back(QPair{ parameterNameList[1], QString::number(profile.slice) });
            }

            if (!diffList.isEmpty()) {
                diffParameters[na] = diffList;
            }
        }

        return diffParameters;
    }

    ImagingProfileWriter_v0_0_1::ImagingProfileWriter_v0_0_1() : d{ std::make_unique<Impl>() } {
    }

    ImagingProfileWriter_v0_0_1::~ImagingProfileWriter_v0_0_1() = default;

    auto ImagingProfileWriter_v0_0_1::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto ImagingProfileWriter_v0_0_1::SetDefaultProfile(const ImagingProfile_v0_0_1& profile) -> void {
        d->defaultProfile = profile;
        d->defaultSet = true;
    }

    auto ImagingProfileWriter_v0_0_1::AddProfile(const float& condenserNA, const ImagingProfile_v0_0_1& profile) -> void {
        if (d->profileMap.contains(condenserNA)) {
            d->profileMap.remove(condenserNA);
        }

        d->profileMap[condenserNA] = profile;
    }

    auto ImagingProfileWriter_v0_0_1::Write() -> bool {
        if (!d->CheckInputs()) {
            return false;
        }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("ImagingVersion");
            profileFile.setValue("version", version);
            profileFile.endGroup();
        }
        {
            const auto naList = d->profileMap.keys();

            profileFile.beginGroup("SupportedNA");

            int32_t index = 0;
            for (const auto na : naList) {
                profileFile.beginGroup(QString::number(index));
                profileFile.setValue("NA", QString::number(na, 'g', 2));
                profileFile.endGroup();
                index++;
            }

            profileFile.setValue("size", naList.count());

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");
            profileFile.setValue(parameterNameList[0], d->defaultProfile.step);
            profileFile.setValue(parameterNameList[1], d->defaultProfile.slice);
            profileFile.endGroup();
        }
        {
            const auto differentParameters = d->GetDifferentParameters();
            profileFile.beginGroup("VariableParametersPerNA");

            for (auto diffIter = differentParameters.cbegin(); diffIter != differentParameters.cend(); ++diffIter) {
                const auto na = diffIter.key();
                const auto differentPairList = diffIter.value();

                profileFile.beginGroup(QString::number(na, 'g', 2));
                for (const auto nameValuePair : differentPairList) {
                    const auto name = nameValuePair.first;
                    const auto value = nameValuePair.second;

                    profileFile.setValue(name, value);
                }
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }
        

        return true;
    }
}
