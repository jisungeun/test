#include <QSettings>
#include <QFile>

#include "HTProcessingProfileReader_v1_4_1_c.h"

namespace TC::IO::ProfileIO {
    using Profile = HTProcessingProfile::HTProcessingProfile_v1_4_1_c;

    class HTProcessingProfileReader_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};
        Profile profile{};
    };

    HTProcessingProfileReader_v1_4_1_c::HTProcessingProfileReader_v1_4_1_c() : d{ std::make_unique<Impl>() } {
    }

    HTProcessingProfileReader_v1_4_1_c::~HTProcessingProfileReader_v1_4_1_c() = default;

    auto HTProcessingProfileReader_v1_4_1_c::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto HTProcessingProfileReader_v1_4_1_c::Read() -> bool {
        if (d->path.isEmpty()) { return false; }
        if (!QFile::exists(d->path)) { return false; }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };

        {
            profileFile.beginGroup("Version");

            if (!profileFile.contains("version")) { return false; }
            const auto version = profileFile.value("version", "").toString();
            if (version != "1.4.1c") { return false; }

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("Variables");

            if (!profileFile.contains("p01SupportDC")) { return false; }
            if (!profileFile.contains("p02SupportDC")) { return false; }
            if (!profileFile.contains("p01NonNegRef")) { return false; }
            if (!profileFile.contains("p02NonNegRef")) { return false; }
            if (!profileFile.contains("p01OuterIterNum")) { return false; }
            if (!profileFile.contains("p01InnerIterNum")) { return false; }
            if (!profileFile.contains("p02OuterIterNum")) { return false; }
            if (!profileFile.contains("p02InnerIterNum")) { return false; }
            if (!profileFile.contains("p01TvParam")) { return false; }
            if (!profileFile.contains("p02TvParam")) { return false; }
            if (!profileFile.contains("resetGpParam4LastIter")) { return false; }
            if (!profileFile.contains("p01SzRatioAxi")) { return false; }
            if (!profileFile.contains("p01SzRatioLat")) { return false; }
            if (!profileFile.contains("lateralMinSizeOffset")) { return false; }
            if (!profileFile.contains("sfcGap01")) { return false; }
            if (!profileFile.contains("sfcGap02")) { return false; }
            if (!profileFile.contains("sfcGap03")) { return false; }
            if (!profileFile.contains("p01NormalizationFactor")) { return false; }
            if (!profileFile.contains("p02NormalizationFactor")) { return false; }
            if (!profileFile.contains("p02PreserveOriginalSfcData")) { return false; }
            if (!profileFile.contains("memoryUtilizationRatio")) { return false; }

            bool ok{ true };
            d->profile.p01SupportDC = profileFile.value("p01SupportDC").toFloat(&ok); if (!ok) { return false; }
            d->profile.p02SupportDC = profileFile.value("p02SupportDC").toFloat(&ok); if (!ok) { return false; }
            d->profile.p01NonNegRef = profileFile.value("p01NonNegRef").toFloat(&ok); if (!ok) { return false; }
            d->profile.p02NonNegRef = profileFile.value("p02NonNegRef").toFloat(&ok); if (!ok) { return false; }
            d->profile.p01OuterIterNum = profileFile.value("p01OuterIterNum").toInt(&ok); if (!ok) { return false; }
            d->profile.p01InnerIterNum = profileFile.value("p01InnerIterNum").toInt(&ok); if (!ok) { return false; }
            d->profile.p02OuterIterNum = profileFile.value("p02OuterIterNum").toInt(&ok); if (!ok) { return false; }
            d->profile.p02InnerIterNum = profileFile.value("p02InnerIterNum").toInt(&ok); if (!ok) { return false; }
            d->profile.p01TvParam = profileFile.value("p01TvParam").toFloat(&ok); if (!ok) { return false; }
            d->profile.p02TvParam = profileFile.value("p02TvParam").toFloat(&ok); if (!ok) { return false; }
            d->profile.resetGpParam4LastIter = profileFile.value("resetGpParam4LastIter").toBool();
            d->profile.p01SzRatioAxi = profileFile.value("p01SzRatioAxi").toFloat(&ok); if (!ok) { return false; }
            d->profile.p01SzRatioLat = profileFile.value("p01SzRatioLat").toFloat(&ok); if (!ok) { return false; }
            d->profile.lateralMinSizeOffset = profileFile.value("lateralMinSizeOffset").toInt(&ok); if (!ok) { return false; }
            d->profile.sfcGap01 = profileFile.value("sfcGap01").toFloat(&ok); if (!ok) { return false; }
            d->profile.sfcGap02 = profileFile.value("sfcGap02").toFloat(&ok); if (!ok) { return false; }
            d->profile.sfcGap03 = profileFile.value("sfcGap03").toFloat(&ok); if (!ok) { return false; }
            d->profile.p01NormalizationFactor = profileFile.value("p01NormalizationFactor").toFloat(&ok); if (!ok) { return false; }
            d->profile.p02NormalizationFactor = profileFile.value("p02NormalizationFactor").toFloat(&ok); if (!ok) { return false; }
            d->profile.p02PreserveOriginalSfcData = profileFile.value("p02PreserveOriginalSfcData").toBool();
            d->profile.memoryUtilizationRatio = profileFile.value("memoryUtilizationRatio").toFloat(&ok); if (!ok) { return false; }

            profileFile.endGroup();
        }

        return true;
    }

    auto HTProcessingProfileReader_v1_4_1_c::GetProfile() -> Profile {
        return d->profile;
    }
}
