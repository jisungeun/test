#include "PSFProfileReader_v1_4_1_c.h"

#include <QFile>
#include <QMap>

#include <QSettings>

using namespace TC::PSFProfile;

namespace TC::IO::ProfileIO {
    using NA = float;
    using Profile = PSFProfile_v1_4_1_c;

    const QString version = "1.4.1c";

    const QStringList parameterNameList{
        "useRealPartOnly",     // 0
        "psfSizeX",            // 1
        "psfSizeY",            // 2
        "psfSizeZ",            // 3
        "denoiseThres",        // 4
        "zComputationSplit",   // 5
        "secondCropNaObj",     // 6
        "secondCropNaCond",    // 7
        "secondCropNAObj02",   // 8
        "secondCropNACond02",  // 9
        "spectrumSize",        // 10
        "filterSnrSupport",    // 11
        "axialUpSampleRatio",  // 12
        "lateralVoxelCount"    // 13
    };

    class PSFProfileReader_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        Profile defaultProfile{};
        QMap<NA, Profile> profileMap{};
        QList<float> supportedNAList{};
    };

    PSFProfileReader_v1_4_1_c::PSFProfileReader_v1_4_1_c() : d{ std::make_unique<Impl>() } {
    }

    PSFProfileReader_v1_4_1_c::~PSFProfileReader_v1_4_1_c() = default;

    auto PSFProfileReader_v1_4_1_c::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto PSFProfileReader_v1_4_1_c::Read() -> bool {
        if (!QFile::exists(d->path)) {
            return false;
        }

        d->profileMap.clear();

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };

        const auto childGroupList = profileFile.childGroups();
        if (!childGroupList.contains("AlgorithmVersion")) { return false; }
        if (!childGroupList.contains("SupportedNA")) { return false; }
        if (!childGroupList.contains("DefaultParameters")) { return false; }

        {
            profileFile.beginGroup("AlgorithmVersion");
            const auto readVersion = profileFile.value("version").toString();
            profileFile.endGroup();

            if (readVersion != version) {
                return false;
            }
        }

        QList<NA> naList;
        {
            profileFile.beginGroup("SupportedNA");

            const auto numberOfNA = profileFile.value("size").toInt();

            bool ok{ true };
            for (auto index = 0; index < numberOfNA; ++index) {
                profileFile.beginGroup(QString::number(index));
                const auto na = profileFile.value("NA").toFloat(&ok); if (!ok) { return false; }
                naList.push_back(na);
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");

            bool ok{ true };
            d->defaultProfile.useRealPartOnly = profileFile.value(parameterNameList[0]).toBool();
            d->defaultProfile.psfSizeX = profileFile.value(parameterNameList[1]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.psfSizeY = profileFile.value(parameterNameList[2]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.psfSizeZ = profileFile.value(parameterNameList[3]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.denoiseThres = profileFile.value(parameterNameList[4]).toString();
            d->defaultProfile.zComputationSplit = profileFile.value(parameterNameList[5]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.secondCropNaObj = profileFile.value(parameterNameList[6]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.secondCropNaCond = profileFile.value(parameterNameList[7]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.secondCropNAObj02 = profileFile.value(parameterNameList[8]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.secondCropNACond02 = profileFile.value(parameterNameList[9]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.spectrumSize = profileFile.value(parameterNameList[10]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.filterSnrSupport = profileFile.value(parameterNameList[11]).toBool();
            d->defaultProfile.axialUpSampleRatio = profileFile.value(parameterNameList[12]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.lateralVoxelCount = profileFile.value(parameterNameList[13]).toInt(&ok); if (!ok) { return false; }

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("VariableParametersPerNA");

            for (const auto na : naList) {
                auto profile = d->defaultProfile;

                const auto naName = QString::number(na, 'g', 2);
                profileFile.beginGroup(naName);

                const auto readParameterNameList = profileFile.allKeys();

                bool ok{ true };
                for (const auto readParameterName : readParameterNameList) {
                    if (readParameterName == parameterNameList[0]) {
                        profile.useRealPartOnly = profileFile.value(readParameterName).toBool();
                    } else if (readParameterName == parameterNameList[1]) {
                        profile.psfSizeX = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[2]) {
                        profile.psfSizeY = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[3]) {
                        profile.psfSizeZ = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[4]) {
                        profile.denoiseThres = profileFile.value(readParameterName).toString();
                    } else if (readParameterName == parameterNameList[5]) {
                        profile.zComputationSplit = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[6]) {
                        profile.secondCropNaObj = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[7]) {
                        profile.secondCropNaCond = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[8]) {
                        profile.secondCropNAObj02 = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[9]) {
                        profile.secondCropNACond02 = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[10]) {
                        profile.spectrumSize = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[11]) {
                        profile.filterSnrSupport = profileFile.value(readParameterName).toBool();
                    } else if (readParameterName == parameterNameList[12]) {
                        profile.axialUpSampleRatio = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[13]) {
                        profile.lateralVoxelCount = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    }
                }

                profileFile.endGroup();

                d->profileMap[na] = profile;
            }

            profileFile.endGroup();
        }

        d->supportedNAList = naList;

        return true;
    }

    auto PSFProfileReader_v1_4_1_c::GetSupportedNAList() -> QList<float> {
        return d->supportedNAList;
    }

    auto PSFProfileReader_v1_4_1_c::GetProfile(const float& na) const -> Profile {
        if (d->profileMap.contains(na)) {
            return d->profileMap[na];
        }

        return d->defaultProfile;
    }
}
