#include "ProfileFileNamer.h"

using namespace TC::PSFProfile;
using namespace TC::HTProcessingProfile;
using namespace TC::ImagingProfile;

namespace TC::ProfileIO {
    auto ProfileFileNamer::GetExtension(const PSFProfileVersion& version) -> QString {
        QString extension;

        switch (version) {
        case PSFProfileVersion::v1_4_1_c:
            extension = "psf.prf";
            break;
        default: 
            extension = "";
        }

        return extension;
    }

    auto ProfileFileNamer::GetExtension(const HTProcessingProfileVersion& version) -> QString {
        QString extension;

        switch (version) {
        case HTProcessingProfileVersion::v1_4_1_c:
            extension = "";
            break;
        case HTProcessingProfileVersion::v1_4_1_d:
            extension = "prc.ht.prf";
            break;
        default:
            extension = "";
        }

        return extension;
    }

    auto ProfileFileNamer::GetExtension(const ImagingProfileVersion& version) -> QString {
        QString extension;

        switch (version) {
        case ImagingProfileVersion::v0_0_1:
            extension = "img.prf";
            break;
        default:
            extension = "";
        }

        return extension;
    }

    auto ProfileFileNamer::AppendExtension(const QString& fileName, const PSFProfileVersion& version) -> QString {
        return QString("%1.%2").arg(fileName).arg(GetExtension(version));
    }

    auto ProfileFileNamer::AppendExtension(const QString& fileName, const HTProcessingProfileVersion& version)
        -> QString {
        return QString("%1.%2").arg(fileName).arg(GetExtension(version));
    }

    auto ProfileFileNamer::AppendExtension(const QString& fileName, const ImagingProfileVersion& version) -> QString {
        return QString("%1.%2").arg(fileName).arg(GetExtension(version));
    }
}
