#include "ImagingProfileReader_v0_0_1.h"

#include <QFile>
#include <QMap>

#include <QSettings>

using namespace TC::ImagingProfile;

namespace TC::IO::ProfileIO {
    using NA = float;

    const QString version = "0.0.1";

    const QStringList parameterNameList{ "Step", "Slices" };

    class ImagingProfileReader_v0_0_1::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        ImagingProfile_v0_0_1 defaultProfile{};
        QMap<NA, ImagingProfile_v0_0_1> profileMap{};
        QList<float> supportedNAList{};
    };

    ImagingProfileReader_v0_0_1::ImagingProfileReader_v0_0_1() : d{ std::make_unique<Impl>() } {
    }

    ImagingProfileReader_v0_0_1::~ImagingProfileReader_v0_0_1() = default;

    auto ImagingProfileReader_v0_0_1::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto ImagingProfileReader_v0_0_1::Read() -> bool {
        if (!QFile::exists(d->path)) {
            return false;
        }

        d->profileMap.clear();

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };

        const auto childGroupList = profileFile.childGroups();
        if (!childGroupList.contains("ImagingVersion")) { return false; }
        if (!childGroupList.contains("SupportedNA")) { return false; }
        if (!childGroupList.contains("DefaultParameters")) { return false; }

        {
            profileFile.beginGroup("ImagingVersion");
            const auto readVersion = profileFile.value("version").toString();
            profileFile.endGroup();

            if (readVersion != version) {
                return false;
            }
        }

        QList<NA> naList;
        {
            profileFile.beginGroup("SupportedNA");

            const auto numberOfNA = profileFile.value("size").toInt();

            bool ok{ true };
            for (auto index = 0; index < numberOfNA; ++index) {
                profileFile.beginGroup(QString::number(index));
                const auto na = profileFile.value("NA").toFloat(&ok); if (!ok) { return false; }
                naList.push_back(na);
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");

            for (const auto parameterName : parameterNameList) {
                if (!profileFile.contains(parameterName)) { return false; }
            }

            bool ok{ true };
            d->defaultProfile.step = profileFile.value(parameterNameList[0]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.slice = profileFile.value(parameterNameList[1]).toInt(&ok); if (!ok) { return false; }

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("VariableParametersPerNA");

            for (const auto na : naList) {
                auto profile = d->defaultProfile;

                profileFile.beginGroup(QString::number(na, 'g', 2));

                const auto readParameterNameList = profileFile.allKeys();

                bool ok{ true };
                for (const auto readParameterName : readParameterNameList) {
                    if (readParameterName == parameterNameList[0]) {
                        profile.step = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[1]) {
                        profile.slice = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    }
                }

                profileFile.endGroup();
                
                d->profileMap[na] = profile;
            }

            profileFile.endGroup();
        }

        d->supportedNAList = naList;

        return true;
    }

    auto ImagingProfileReader_v0_0_1::GetSupportedNAList() -> QList<float> {
        return d->supportedNAList;
    }

    auto ImagingProfileReader_v0_0_1::GetProfile(const float& na) const -> ImagingProfile_v0_0_1 {
        if (d->profileMap.contains(na)) {
            return d->profileMap[na];
        }

        return d->defaultProfile;
    }
}
