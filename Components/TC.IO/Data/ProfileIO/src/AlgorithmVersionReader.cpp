#include <QFile>
#include <QSettings>

#include "AlgorithmVersionReader.h"

namespace TC::IO::ProfileIO {
    class AlgorithmVersionReader::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString profileFilePath{};
        QString version{};
    };

    AlgorithmVersionReader::AlgorithmVersionReader() : d{ std::make_unique<Impl>() } {
    }

    AlgorithmVersionReader::~AlgorithmVersionReader() = default;

    auto AlgorithmVersionReader::SetFilePath(const QString& profileFilePath) -> void {
        d->profileFilePath = profileFilePath;
    }

    auto AlgorithmVersionReader::Read() -> bool {
        if (d->profileFilePath.isEmpty()) { return false; }
        if (!QFile::exists(d->profileFilePath)) { return false; }

        QSettings profileFile{ d->profileFilePath, QSettings::IniFormat };
        profileFile.beginGroup("AlgorithmVersion");
        const auto version = profileFile.value("version").toString();
        profileFile.endGroup();

        if (version.isEmpty()) { return false; }

        d->version = version;

        return true;
    }

    auto AlgorithmVersionReader::GetVersion() -> QString {
        return d->version;
    }
}
