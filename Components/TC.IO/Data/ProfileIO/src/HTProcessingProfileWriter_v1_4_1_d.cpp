#include "HTProcessingProfileWriter_v1_4_1_d.h"

#include <QFile>
#include <QMap>
#include <QSettings>

using namespace TC::HTProcessingProfile;

namespace TC::IO::ProfileIO {
    using NA = float;
    using NameString = QString;
    using Value = QVariant;

    using Profile = HTProcessingProfile_v1_4_1_d;

    const QString version = "1.4.1d";

    const QStringList parameterNameList{
        "enableRegularization",          // 0
        "p01SupportDC",                  // 1
        "p02SupportDC",                  // 2
        "p01NonNegRef",                  // 3
        "p02NonNegRef",                  // 4
        "p01OuterIterNum",               // 5
        "p01InnerIterNum",               // 6
        "p02OuterIterNum",               // 7
        "p02InnerIterNum",               // 8
        "p01TvParam",                    // 9
        "p02TvParam",                    // 10
        "resetGpParam4LastIter",         // 11
        "p01SzRatioAxi",                 // 12
        "p01SzRatioLat",                 // 13
        "lateralMinSizeOffset",          // 14
        "sfcGap01",                      // 15
        "sfcGap02",                      // 16
        "sfcGap03",                      // 17
        "p01NormalizationFactor",        // 18
        "p02NormalizationFactor",        // 19
        "p02PreserveOriginalSfcData",    // 20
        "memoryUtilizationRatio"         // 21
    };

    class HTProcessingProfileWriter_v1_4_1_d::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        Profile defaultProfile{};
        bool defaultSet{ false };

        QMap<NA, HTProcessingProfile_v1_4_1_d> profileMap{};

        auto CheckInputs()->bool;
        auto GetDifferentParameters()->QMap<NA, QList<QPair<NameString, Value>>>;

        auto ToBoolString(const bool& flag)->QString;
    };

    auto HTProcessingProfileWriter_v1_4_1_d::Impl::CheckInputs() -> bool {
        if (this->path.isEmpty()) {
            return false;
        }

        if (!this->defaultSet) {
            return false;
        }

        if (QFile::exists(this->path)) {
            return false;
        }

        return true;
    }

    auto HTProcessingProfileWriter_v1_4_1_d::Impl::GetDifferentParameters()
        -> QMap<NA, QList<QPair<NameString, Value>>> {
        QMap<NA, QList<QPair<NameString, Value>>> diffParameters{};

        for (auto profileIter = this->profileMap.cbegin(); profileIter != this->profileMap.cend(); ++profileIter) {
            const auto na = profileIter.key();
            const auto profile = profileIter.value();

            QList<QPair<NameString, Value>> diffList;
            if (this->defaultProfile.enableRegularization != profile.enableRegularization) {
                diffList.push_back(QPair{ parameterNameList[0], profile.enableRegularization });
            }
            if (this->defaultProfile.p01SupportDC != profile.p01SupportDC) {
                diffList.push_back(QPair{ parameterNameList[1], static_cast<double>(profile.p01SupportDC )});
            }
            if (this->defaultProfile.p02SupportDC != profile.p02SupportDC) {
                diffList.push_back(QPair{ parameterNameList[2], static_cast<double>(profile.p02SupportDC )});
            }
            if (this->defaultProfile.p01NonNegRef != profile.p01NonNegRef) {
                diffList.push_back(QPair{ parameterNameList[3], static_cast<double>(profile.p01NonNegRef )});
            }
            if (this->defaultProfile.p02NonNegRef != profile.p02NonNegRef) {
                diffList.push_back(QPair{ parameterNameList[4], static_cast<double>(profile.p02NonNegRef )});
            }
            if (this->defaultProfile.p01OuterIterNum != profile.p01OuterIterNum) {
                diffList.push_back(QPair{ parameterNameList[5], profile.p01OuterIterNum });
            }
            if (this->defaultProfile.p01InnerIterNum != profile.p01InnerIterNum) {
                diffList.push_back(QPair{ parameterNameList[6], profile.p01InnerIterNum });
            }
            if (this->defaultProfile.p02OuterIterNum != profile.p02OuterIterNum) {
                diffList.push_back(QPair{ parameterNameList[7], profile.p02OuterIterNum });
            }
            if (this->defaultProfile.p02InnerIterNum != profile.p02InnerIterNum) {
                diffList.push_back(QPair{ parameterNameList[8], profile.p02InnerIterNum });
            }
            if (this->defaultProfile.p01TvParam != profile.p01TvParam) {
                diffList.push_back(QPair{ parameterNameList[9], static_cast<double>(profile.p01TvParam )});
            }
            if (this->defaultProfile.p02TvParam != profile.p02TvParam) {
                diffList.push_back(QPair{ parameterNameList[10], static_cast<double>(profile.p02TvParam )});
            }
            if (this->defaultProfile.resetGpParam4LastIter != profile.resetGpParam4LastIter) {
                diffList.push_back(QPair{ parameterNameList[11], profile.resetGpParam4LastIter });
            }
            if (this->defaultProfile.p01SzRatioAxi != profile.p01SzRatioAxi) {
                diffList.push_back(QPair{ parameterNameList[12], static_cast<double>(profile.p01SzRatioAxi )});
            }
            if (this->defaultProfile.p01SzRatioLat != profile.p01SzRatioLat) {
                diffList.push_back(QPair{ parameterNameList[13], static_cast<double>(profile.p01SzRatioLat )});
            }
            if (this->defaultProfile.lateralMinSizeOffset != profile.lateralMinSizeOffset) {
                diffList.push_back(QPair{ parameterNameList[14], profile.lateralMinSizeOffset });
            }
            if (this->defaultProfile.sfcGap01 != profile.sfcGap01) {
                diffList.push_back(QPair{ parameterNameList[15], static_cast<double>(profile.sfcGap01 )});
            }
            if (this->defaultProfile.sfcGap02 != profile.sfcGap02) {
                diffList.push_back(QPair{ parameterNameList[16], static_cast<double>(profile.sfcGap02 )});
            }
            if (this->defaultProfile.sfcGap03 != profile.sfcGap03) {
                diffList.push_back(QPair{ parameterNameList[17], static_cast<double>(profile.sfcGap03 )});
            }
            if (this->defaultProfile.p01NormalizationFactor != profile.p01NormalizationFactor) {
                diffList.push_back(QPair{ parameterNameList[18], static_cast<double>(profile.p01NormalizationFactor )});
            }
            if (this->defaultProfile.p02NormalizationFactor != profile.p02NormalizationFactor) {
                diffList.push_back(QPair{ parameterNameList[19], static_cast<double>(profile.p02NormalizationFactor )});
            }
            if (this->defaultProfile.p02PreserveOriginalSfcData != profile.p02PreserveOriginalSfcData) {
                diffList.push_back(QPair{ parameterNameList[20], profile.p02PreserveOriginalSfcData });
            }
            if (this->defaultProfile.memoryUtilizationRatio != profile.memoryUtilizationRatio) {
                diffList.push_back(QPair{ parameterNameList[21], static_cast<double>(profile.memoryUtilizationRatio )});
            }


            diffParameters[na] = diffList;
        }

        return diffParameters;
    }

    auto HTProcessingProfileWriter_v1_4_1_d::Impl::ToBoolString(const bool& flag) -> QString {
        return flag ? "true" : "false";
    }

    HTProcessingProfileWriter_v1_4_1_d::HTProcessingProfileWriter_v1_4_1_d() : d{ std::make_unique<Impl>() } {
    }

    HTProcessingProfileWriter_v1_4_1_d::~HTProcessingProfileWriter_v1_4_1_d() = default;

    auto HTProcessingProfileWriter_v1_4_1_d::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto HTProcessingProfileWriter_v1_4_1_d::SetDefaultProfile(const Profile& profile) -> void {
        d->defaultProfile = profile;
        d->defaultSet = true;
    }

    auto HTProcessingProfileWriter_v1_4_1_d::AddProfile(const float& condenserNA, const Profile& profile) -> void {
        if (d->profileMap.contains(condenserNA)) {
            d->profileMap.remove(condenserNA);
        }

        d->profileMap[condenserNA] = profile;
    }

    auto HTProcessingProfileWriter_v1_4_1_d::Write() -> bool {
        if (!d->CheckInputs()) {
            return false;
        }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("AlgorithmVersion");
            profileFile.setValue("version", version);
            profileFile.endGroup();
        }
        {
            const auto naList = d->profileMap.keys();

            profileFile.beginGroup("SupportedNA");

            int32_t index = 0;
            for (const auto na : naList) {
                profileFile.beginGroup(QString::number(index));
                profileFile.setValue("NA", QString::number(na, 'g', 2));
                profileFile.endGroup();
                index++;
            }

            profileFile.setValue("size", naList.count());

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");
            profileFile.setValue(parameterNameList[0], d->defaultProfile.enableRegularization);
            profileFile.setValue(parameterNameList[1], static_cast<double>(d->defaultProfile.p01SupportDC));
            profileFile.setValue(parameterNameList[2], static_cast<double>(d->defaultProfile.p02SupportDC));
            profileFile.setValue(parameterNameList[3], static_cast<double>(d->defaultProfile.p01NonNegRef));
            profileFile.setValue(parameterNameList[4], static_cast<double>(d->defaultProfile.p02NonNegRef));
            profileFile.setValue(parameterNameList[5], d->defaultProfile.p01OuterIterNum);
            profileFile.setValue(parameterNameList[6], d->defaultProfile.p01InnerIterNum);
            profileFile.setValue(parameterNameList[7], d->defaultProfile.p02OuterIterNum);
            profileFile.setValue(parameterNameList[8], d->defaultProfile.p02InnerIterNum);
            profileFile.setValue(parameterNameList[9], static_cast<double>(d->defaultProfile.p01TvParam));
            profileFile.setValue(parameterNameList[10], static_cast<double>(d->defaultProfile.p02TvParam));
            profileFile.setValue(parameterNameList[11], d->defaultProfile.resetGpParam4LastIter);
            profileFile.setValue(parameterNameList[12], static_cast<double>(d->defaultProfile.p01SzRatioAxi));
            profileFile.setValue(parameterNameList[13], static_cast<double>(d->defaultProfile.p01SzRatioLat));
            profileFile.setValue(parameterNameList[14], d->defaultProfile.lateralMinSizeOffset);
            profileFile.setValue(parameterNameList[15], static_cast<double>(d->defaultProfile.sfcGap01));
            profileFile.setValue(parameterNameList[16], static_cast<double>(d->defaultProfile.sfcGap02));
            profileFile.setValue(parameterNameList[17], static_cast<double>(d->defaultProfile.sfcGap03));
            profileFile.setValue(parameterNameList[18], static_cast<double>(d->defaultProfile.p01NormalizationFactor));
            profileFile.setValue(parameterNameList[19], static_cast<double>(d->defaultProfile.p02NormalizationFactor));
            profileFile.setValue(parameterNameList[20], d->defaultProfile.p02PreserveOriginalSfcData);
            profileFile.setValue(parameterNameList[21], static_cast<double>(d->defaultProfile.memoryUtilizationRatio));
            profileFile.endGroup();
        }
        {
            const auto differentParameters = d->GetDifferentParameters();
            profileFile.beginGroup("VariableParametersPerNA");

            for (auto diffIter = differentParameters.cbegin(); diffIter != differentParameters.cend(); ++diffIter) {
                const auto na = diffIter.key();
                const auto differentPairList = diffIter.value();

                profileFile.beginGroup(QString::number(na, 'g', 2));
                for (const auto nameValuePair : differentPairList) {
                    const auto name = nameValuePair.first;
                    const auto value = nameValuePair.second;

                    profileFile.setValue(name, value);
                }
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }


        return true;
    }
}
