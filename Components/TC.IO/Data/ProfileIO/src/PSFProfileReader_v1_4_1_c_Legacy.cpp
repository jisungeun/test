#include <QFile>
#include <QSettings>

#include "PSFProfileReader_v1_4_1_c_Legacy.h"

namespace TC::IO::ProfileIO {
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    class PSFProfileReader_v1_4_1_c_Legacy::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};
        Profile profile{};
    };

    PSFProfileReader_v1_4_1_c_Legacy::PSFProfileReader_v1_4_1_c_Legacy() : d{ std::make_unique<Impl>() } {
    }

    PSFProfileReader_v1_4_1_c_Legacy::~PSFProfileReader_v1_4_1_c_Legacy() = default;

    auto PSFProfileReader_v1_4_1_c_Legacy::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto PSFProfileReader_v1_4_1_c_Legacy::Read() -> bool {
        if (d->path.isEmpty()) { return false; }
        if (!QFile::exists(d->path)) { return false; }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("Version");

            if (!profileFile.contains("version")) { return false; }
            const auto version = profileFile.value("version", "").toString();
            if (version != "1.4.1c") { return false; }

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("Variables");

            if (!profileFile.contains("useRealPartOnly")) { return false; }
            if (!profileFile.contains("psfSizeX")) { return false; }
            if (!profileFile.contains("psfSizeY")) { return false; }
            if (!profileFile.contains("psfSizeZ")) { return false; }
            if (!profileFile.contains("denoiseThres")) { return false; }
            if (!profileFile.contains("zComputationSplit")) { return false; }
            if (!profileFile.contains("secondCropNaObj")) { return false; }
            if (!profileFile.contains("secondCropNaCond")) { return false; }
            if (!profileFile.contains("secondCropNAObj02")) { return false; }
            if (!profileFile.contains("secondCropNACond02")) { return false; }
            if (!profileFile.contains("spectrumSize")) { return false; }
            if (!profileFile.contains("filterSnrSupport")) { return false; }
            if (!profileFile.contains("axialUpSampleRatio")) { return false; }
            if (!profileFile.contains("lateralVoxelCount")) { return false; }

            bool ok{ true };
            d->profile.useRealPartOnly = profileFile.value("useRealPartOnly").toBool();
            d->profile.psfSizeX = profileFile.value("psfSizeX").toInt(&ok); if (!ok) { return false; }
            d->profile.psfSizeY = profileFile.value("psfSizeY").toInt(&ok); if (!ok) { return false; }
            d->profile.psfSizeZ = profileFile.value("psfSizeZ").toInt(&ok); if (!ok) { return false; }
            d->profile.denoiseThres = profileFile.value("denoiseThres", "").toString(); if (d->profile.denoiseThres == "") { return false; }
            d->profile.zComputationSplit = profileFile.value("zComputationSplit").toInt(&ok); if (!ok) { return false; }
            d->profile.secondCropNaObj = profileFile.value("secondCropNaObj").toFloat(&ok); if (!ok) { return false; }
            d->profile.secondCropNaCond = profileFile.value("secondCropNaCond").toFloat(&ok); if (!ok) { return false; }
            d->profile.secondCropNAObj02 = profileFile.value("secondCropNAObj02").toFloat(&ok); if (!ok) { return false; }
            d->profile.secondCropNACond02 = profileFile.value("secondCropNACond02").toFloat(&ok); if (!ok) { return false; }
            d->profile.spectrumSize = profileFile.value("spectrumSize").toInt(&ok); if (!ok) { return false; }
            d->profile.filterSnrSupport = profileFile.value("filterSnrSupport").toBool();
            d->profile.axialUpSampleRatio = profileFile.value("axialUpSampleRatio").toFloat(&ok); if (!ok) { return false; }
            d->profile.lateralVoxelCount = profileFile.value("lateralVoxelCount").toInt(&ok); if (!ok) { return false; }
        }
        
        return true;
    }

    auto PSFProfileReader_v1_4_1_c_Legacy::GetProfile() -> Profile {
        return d->profile;
    }
}
