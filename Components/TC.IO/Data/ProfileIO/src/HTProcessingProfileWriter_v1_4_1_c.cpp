#include "HTProcessingProfileWriter_v1_4_1_c.h"

#include <QSettings>

namespace TC::IO::ProfileIO {
    const QString version = "1.4.1c";
    using Profile = HTProcessingProfile::HTProcessingProfile_v1_4_1_c;

    class HTProcessingProfileWriter_v1_4_1_c::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};
        Profile profile{};
        bool profileSet{ false };
    };

    HTProcessingProfileWriter_v1_4_1_c::HTProcessingProfileWriter_v1_4_1_c() : d{ std::make_unique<Impl>() } {
    }

    HTProcessingProfileWriter_v1_4_1_c::~HTProcessingProfileWriter_v1_4_1_c() = default;

    auto HTProcessingProfileWriter_v1_4_1_c::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto HTProcessingProfileWriter_v1_4_1_c::SetProfile(const Profile& profile) -> void {
        d->profile = profile;
        d->profileSet = true;
    }

    auto HTProcessingProfileWriter_v1_4_1_c::Write() -> bool {
        if (d->path.isEmpty()) { return false; }
        if (!d->profileSet) { return false; }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("Version");

            profileFile.setValue("version", version);

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("Variables");

            profileFile.setValue("p01SupportDC", static_cast<double>(d->profile.p01SupportDC));
            profileFile.setValue("p02SupportDC", static_cast<double>(d->profile.p02SupportDC));
            profileFile.setValue("p01NonNegRef", static_cast<double>(d->profile.p01NonNegRef));
            profileFile.setValue("p02NonNegRef", static_cast<double>(d->profile.p02NonNegRef));
            profileFile.setValue("p01OuterIterNum", d->profile.p01OuterIterNum);
            profileFile.setValue("p01InnerIterNum", d->profile.p01InnerIterNum);
            profileFile.setValue("p02OuterIterNum", d->profile.p02OuterIterNum);
            profileFile.setValue("p02InnerIterNum", d->profile.p02InnerIterNum);
            profileFile.setValue("p01TvParam", static_cast<double>(d->profile.p01TvParam));
            profileFile.setValue("p02TvParam", static_cast<double>(d->profile.p02TvParam));
            profileFile.setValue("resetGpParam4LastIter", d->profile.resetGpParam4LastIter);
            profileFile.setValue("p01SzRatioAxi", static_cast<double>(d->profile.p01SzRatioAxi));
            profileFile.setValue("p01SzRatioLat", static_cast<double>(d->profile.p01SzRatioLat));
            profileFile.setValue("lateralMinSizeOffset", d->profile.lateralMinSizeOffset);
            profileFile.setValue("sfcGap01", static_cast<double>(d->profile.sfcGap01));
            profileFile.setValue("sfcGap02", static_cast<double>(d->profile.sfcGap02));
            profileFile.setValue("sfcGap03", static_cast<double>(d->profile.sfcGap03));
            profileFile.setValue("p01NormalizationFactor", static_cast<double>(d->profile.p01NormalizationFactor));
            profileFile.setValue("p02NormalizationFactor", static_cast<double>(d->profile.p02NormalizationFactor));
            profileFile.setValue("p02PreserveOriginalSfcData", d->profile.p02PreserveOriginalSfcData);
            profileFile.setValue("memoryUtilizationRatio", static_cast<double>(d->profile.memoryUtilizationRatio));

            profileFile.endGroup();
        }

        return true;
    }
}
