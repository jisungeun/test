#include "HTProcessingProfileReader_v1_4_1_d.h"

#include <QFile>
#include <QMap>

#include <QSettings>

using namespace TC::HTProcessingProfile;

namespace TC::IO::ProfileIO {
    using NA = float;
    using Profile = HTProcessingProfile_v1_4_1_d;

    const QString version = "1.4.1d";

    const QStringList parameterNameList{
        "enableRegularization",          // 0
        "p01SupportDC",                  // 1
        "p02SupportDC",                  // 2
        "p01NonNegRef",                  // 3
        "p02NonNegRef",                  // 4
        "p01OuterIterNum",               // 5
        "p01InnerIterNum",               // 6
        "p02OuterIterNum",               // 7
        "p02InnerIterNum",               // 8
        "p01TvParam",                    // 9
        "p02TvParam",                    // 10
        "resetGpParam4LastIter",         // 11
        "p01SzRatioAxi",                 // 12
        "p01SzRatioLat",                 // 13
        "lateralMinSizeOffset",          // 14
        "sfcGap01",                      // 15
        "sfcGap02",                      // 16
        "sfcGap03",                      // 17
        "p01NormalizationFactor",        // 18
        "p02NormalizationFactor",        // 19
        "p02PreserveOriginalSfcData",    // 20
        "memoryUtilizationRatio"         // 21
    };

    class HTProcessingProfileReader_v1_4_1_d::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString path{};

        Profile defaultProfile{};
        QMap<NA, Profile> profileMap{};
        QList<float> supportedNAList{};
    };

    HTProcessingProfileReader_v1_4_1_d::HTProcessingProfileReader_v1_4_1_d() : d{ std::make_unique<Impl>() } {
    }

    HTProcessingProfileReader_v1_4_1_d::~HTProcessingProfileReader_v1_4_1_d() = default;

    auto HTProcessingProfileReader_v1_4_1_d::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto HTProcessingProfileReader_v1_4_1_d::Read() -> bool {
        if (!QFile::exists(d->path)) {
            return false;
        }

        d->profileMap.clear();

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };

        const auto childGroupList = profileFile.childGroups();
        if (!childGroupList.contains("AlgorithmVersion")) { return false; }
        if (!childGroupList.contains("SupportedNA")) { return false; }
        if (!childGroupList.contains("DefaultParameters")) { return false; }

        {
            profileFile.beginGroup("AlgorithmVersion");
            const auto readVersion = profileFile.value("version").toString();
            profileFile.endGroup();

            if (readVersion != version) {
                return false;
            }
        }

        QList<NA> naList;
        {
            profileFile.beginGroup("SupportedNA");

            const auto numberOfNA = profileFile.value("size").toInt();

            bool ok{ true };
            for (auto index = 0; index < numberOfNA; ++index) {
                profileFile.beginGroup(QString::number(index));
                const auto na = profileFile.value("NA").toFloat(&ok); if (!ok) { return false; }
                naList.push_back(na);
                profileFile.endGroup();
            }
            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("DefaultParameters");

            for (const auto parameterName : parameterNameList) {
                if (!profileFile.contains(parameterName)) { return false; }
            }

            bool ok{ true };
            d->defaultProfile.enableRegularization = profileFile.value(parameterNameList[0]).toBool();
            d->defaultProfile.p01SupportDC = profileFile.value(parameterNameList[1]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p02SupportDC = profileFile.value(parameterNameList[2]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p01NonNegRef = profileFile.value(parameterNameList[3]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p02NonNegRef = profileFile.value(parameterNameList[4]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p01OuterIterNum = profileFile.value(parameterNameList[5]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.p01InnerIterNum = profileFile.value(parameterNameList[6]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.p02OuterIterNum = profileFile.value(parameterNameList[7]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.p02InnerIterNum = profileFile.value(parameterNameList[8]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.p01TvParam = profileFile.value(parameterNameList[9]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p02TvParam = profileFile.value(parameterNameList[10]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.resetGpParam4LastIter = profileFile.value(parameterNameList[11]).toBool();
            d->defaultProfile.p01SzRatioAxi = profileFile.value(parameterNameList[12]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p01SzRatioLat = profileFile.value(parameterNameList[13]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.lateralMinSizeOffset = profileFile.value(parameterNameList[14]).toInt(&ok); if (!ok) { return false; }
            d->defaultProfile.sfcGap01 = profileFile.value(parameterNameList[15]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.sfcGap02 = profileFile.value(parameterNameList[16]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.sfcGap03 = profileFile.value(parameterNameList[17]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p01NormalizationFactor = profileFile.value(parameterNameList[18]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p02NormalizationFactor = profileFile.value(parameterNameList[19]).toFloat(&ok); if (!ok) { return false; }
            d->defaultProfile.p02PreserveOriginalSfcData = profileFile.value(parameterNameList[20]).toBool();
            d->defaultProfile.memoryUtilizationRatio = profileFile.value(parameterNameList[21]).toFloat(&ok); if (!ok) { return false; }

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("VariableParametersPerNA");

            for (const auto na : naList) {
                auto profile = d->defaultProfile;

                profileFile.beginGroup(QString::number(na, 'g', 2));

                const auto readParameterNameList = profileFile.allKeys();

                bool ok{ true };
                for (const auto readParameterName : readParameterNameList) {
                    if (readParameterName == parameterNameList[0]) {
                        profile.enableRegularization = profileFile.value(readParameterName).toBool();
                    } else if (readParameterName == parameterNameList[1]) {
                        profile.p01SupportDC = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[2]) {
                        profile.p02SupportDC = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[3]) {
                        profile.p01NonNegRef = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[4]) {
                        profile.p02NonNegRef = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[5]) {
                        profile.p01OuterIterNum = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[6]) {
                        profile.p01InnerIterNum = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[7]) {
                        profile.p02OuterIterNum = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[8]) {
                        profile.p02InnerIterNum = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[9]) {
                        profile.p01TvParam = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[10]) {
                        profile.p02TvParam = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[11]) {
                        profile.resetGpParam4LastIter = profileFile.value(readParameterName).toBool();
                    } else if (readParameterName == parameterNameList[12]) {
                        profile.p01SzRatioAxi = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[13]) {
                        profile.p01SzRatioLat = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[14]) {
                        profile.lateralMinSizeOffset = profileFile.value(readParameterName).toInt(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[15]) {
                        profile.sfcGap01 = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[16]) {
                        profile.sfcGap02 = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[17]) {
                        profile.sfcGap03 = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[18]) {
                        profile.p01NormalizationFactor = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[19]) {
                        profile.p02NormalizationFactor = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    } else if (readParameterName == parameterNameList[20]) {
                        profile.p02PreserveOriginalSfcData = profileFile.value(readParameterName).toBool();
                    } else if (readParameterName == parameterNameList[21]) {
                        profile.memoryUtilizationRatio = profileFile.value(readParameterName).toFloat(&ok); if (!ok) { return false; }
                    }
                }

                profileFile.endGroup();

                d->profileMap[na] = profile;
            }

            profileFile.endGroup();
        }

        d->supportedNAList = naList;

        return true;
    }

    auto HTProcessingProfileReader_v1_4_1_d::GetSupportedNAList() -> QList<float> {
        return d->supportedNAList;
    }

    auto HTProcessingProfileReader_v1_4_1_d::GetProfile(const float& na) const -> Profile {
        if (d->profileMap.contains(na)) {
            return d->profileMap[na];
        }

        return d->defaultProfile;
    }
}
