#include <QFile>
#include <QSettings>

#include "AlgorithmVersionReader_Legacy.h"

namespace TC::IO::ProfileIO {
    class AlgorithmVersionReader_Legacy::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString profileFilePath{};
        QString version{};
    };

    AlgorithmVersionReader_Legacy::AlgorithmVersionReader_Legacy() : d{ std::make_unique<Impl>() } {
    }

    AlgorithmVersionReader_Legacy::~AlgorithmVersionReader_Legacy() = default;

    auto AlgorithmVersionReader_Legacy::SetFilePath(const QString& profileFilePath) -> void {
        d->profileFilePath = profileFilePath;
    }

    auto AlgorithmVersionReader_Legacy::Read() -> bool {
        if (d->profileFilePath.isEmpty()) { return false; }
        if (!QFile::exists(d->profileFilePath)) { return false; }

        QSettings profileFile{ d->profileFilePath, QSettings::IniFormat };
        profileFile.beginGroup("Version");
        const auto version = profileFile.value("version").toString();
        profileFile.endGroup();

        if (version.isEmpty()) { return false; }

        d->version = version;

        return true;
    }

    auto AlgorithmVersionReader_Legacy::GetVersion() -> QString {
        return d->version;
    }
}
