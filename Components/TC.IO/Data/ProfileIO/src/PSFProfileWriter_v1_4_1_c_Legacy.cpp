#include "PSFProfileWriter_v1_4_1_c_Legacy.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QSettings>

namespace TC::IO::ProfileIO {
    const QString version = "1.4.1c";
    using Profile = PSFProfile::PSFProfile_v1_4_1_c;

    class PSFProfileWriter_v1_4_1_c_Legacy::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        auto CheckInputs()->bool;

        QString path{};
        Profile profile{};
        bool profileSet{ false };
    };

    auto PSFProfileWriter_v1_4_1_c_Legacy::Impl::CheckInputs() -> bool {
        if (this->path.isEmpty()) {
            return false;
        }
        if (!this->profileSet) {
            return false;
        }
        if (QFile::exists(this->path)) {
            return false;
        }
        if (!QFileInfo(this->path).dir().exists()) {
            return false;
        }

        return true;
    }

    PSFProfileWriter_v1_4_1_c_Legacy::PSFProfileWriter_v1_4_1_c_Legacy() : d{ std::make_unique<Impl>() } {
    }

    PSFProfileWriter_v1_4_1_c_Legacy::~PSFProfileWriter_v1_4_1_c_Legacy() = default;

    auto PSFProfileWriter_v1_4_1_c_Legacy::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto PSFProfileWriter_v1_4_1_c_Legacy::SetProfile(const PSFProfile::PSFProfile_v1_4_1_c& profile) -> void {
        d->profile = profile;
        d->profileSet = true;
    }

    auto PSFProfileWriter_v1_4_1_c_Legacy::Write() -> bool {
        if (!d->CheckInputs()) {
            return false;
        }

        QSettings profileFile{ d->path, QSettings::Format::IniFormat };
        {
            profileFile.beginGroup("Version");

            profileFile.setValue("version", version);

            profileFile.endGroup();
        }
        {
            profileFile.beginGroup("Variables");

            profileFile.setValue("useRealPartOnly", d->profile.useRealPartOnly);
            profileFile.setValue("psfSizeX", d->profile.psfSizeX);
            profileFile.setValue("psfSizeY", d->profile.psfSizeY);
            profileFile.setValue("psfSizeZ", d->profile.psfSizeZ);
            profileFile.setValue("denoiseThres", d->profile.denoiseThres);
            profileFile.setValue("zComputationSplit", d->profile.zComputationSplit);
            profileFile.setValue("secondCropNaObj", static_cast<double>(d->profile.secondCropNaObj));
            profileFile.setValue("secondCropNaCond", static_cast<double>(d->profile.secondCropNaCond));
            profileFile.setValue("secondCropNAObj02", static_cast<double>(d->profile.secondCropNAObj02));
            profileFile.setValue("secondCropNACond02", static_cast<double>(d->profile.secondCropNACond02));
            profileFile.setValue("spectrumSize", d->profile.spectrumSize);
            profileFile.setValue("filterSnrSupport", d->profile.filterSnrSupport);
            profileFile.setValue("axialUpSampleRatio", static_cast<double>(d->profile.axialUpSampleRatio));
            profileFile.setValue("lateralVoxelCount", d->profile.lateralVoxelCount);

            profileFile.endGroup();
        }

        return true;
    }
}
