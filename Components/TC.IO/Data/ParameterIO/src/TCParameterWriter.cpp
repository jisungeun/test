#include "TCParameterWriter.h"

#include <iostream>
#include <QDir>

#include "ParameterWriter.h"


namespace TC::IO {
    struct TCParameterWriter::Impl {
        QString playPath;
    };
    TCParameterWriter::TCParameterWriter(const QString& parentPath) : d{ new Impl } {
        d->playPath = parentPath;
    }
    TCParameterWriter::~TCParameterWriter() = default;
    auto TCParameterWriter::ForceWrite(IParameter::Pointer param, QString appName, QString reportName, QString hyperName, QString procName, QString timeStamp) -> void {
        auto writer = std::make_shared<ParameterWriter>();
        writer->WriteProp("AppName", appName);
        writer->WriteProp("Processor", procName);
        writer->WriteProp("ReportName", reportName);
        auto parent_folder = d->playPath;

        auto file_path = parent_folder + "/" + hyperName + "_" + timeStamp + ".param";
        
        writer->SetPath(file_path);
        writer->Write(param);
    }

    auto TCParameterWriter::Write(IParameter::Pointer param, QString appName,QString reportName, QString hyperName, QString procName,QString timeStamp) -> void {
        auto writer = std::make_shared<ParameterWriter>();

        auto parent_folder = d->playPath.chopped(5);
        QDir pdir(parent_folder);
        if(false == pdir.exists()) {
            QDir().mkdir(parent_folder);
        }        

        writer->WriteProp("AppName", appName);
        writer->WriteProp("Processor", procName);
        writer->WriteProp("ReportName",reportName);

        if(false == param->GetVersion().isEmpty()) {
            writer->WriteProp("ParameterVersion", param->GetVersion());
        }
        
        if(timeStamp.isEmpty()) {            
            auto file_path = parent_folder + "/" + hyperName + ".param";
            std::cout << file_path.toStdString() << std::endl;
            writer->SetPath(file_path);
            writer->Write(param);
        }else {
            pdir.cdUp();//playground folder
            if (false == pdir.exists("history")) {
                pdir.mkdir("history");
            }
            pdir.cd("history");
            parent_folder = pdir.path();
            //craete empty folder for parameter            
            if (false == pdir.exists(hyperName)) {
                pdir.mkdir(hyperName);
            }
            pdir.cd(hyperName);
            if (false == pdir.exists(timeStamp)) {
                pdir.mkdir(timeStamp);
            }            
            auto file_path = parent_folder + "/" + hyperName +"_"+ timeStamp + ".param";
            writer->SetPath(file_path);
            writer->Write(param);                        
        }
    }
    auto TCParameterWriter::Modify(IParameter::Pointer param) -> void {
        
    }
}