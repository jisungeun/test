#include "TCParameterReader.h"

#include <iostream>
#include <QDir>

#include "ParameterReader.h"

namespace TC::IO {
    struct TCParameterReader::Impl {
        QString parentPath;
        bool isHistory;
    };
    TCParameterReader::TCParameterReader(const QString& parentPath,bool isHistory) :d{ new Impl } {
        d->parentPath = parentPath;
        d->isHistory = isHistory;
    }
    TCParameterReader::~TCParameterReader() {

    }
    auto TCParameterReader::ReadHistory(QString hyperName, QString timeStamp) -> std::tuple<QString,QString,QString, IParameter::Pointer> {
        auto reader = std::make_shared<ParameterReader>();
        QString parent_folder;
        if(d->isHistory) {
            parent_folder = d->parentPath;
        }else {
            parent_folder = d->parentPath.chopped(5);
        }
        //auto parent_folder = d->parentPath.chopped(5);//parent folder dir

        QDir pdir(parent_folder);
        if (false == pdir.exists()) {
            return std::make_tuple(QString(),QString(),QString(), nullptr);
        }
        QString file_path = parent_folder + "/" + hyperName;
        if (!timeStamp.isEmpty()) {
            file_path += "_";
            file_path += timeStamp;
        }
        file_path += ".param";
        reader->SetPath(file_path);                

        auto appProp = reader->ReadProp("AppName");
        
        auto procProp = reader->ReadProp("Processor");

        auto reportProp = reader->ReadProp("ReportName");                

        auto param = reader->Read();

        auto paramVersion = reader->ReadProp("ParameterVersion");
        if (false == paramVersion.isEmpty()) {
            param->SetVersion(paramVersion);
        }

        return std::make_tuple(appProp,procProp,reportProp, param);
    }

    auto TCParameterReader::Read(QString appName, QString hyperName, QString timeStamp) -> std::tuple<QString, IParameter::Pointer> {       
        auto reader = std::make_shared<ParameterReader>();        
        auto parent_folder = d->parentPath.chopped(5);//parent folder dir        
        QDir pdir(parent_folder);
        if(false == pdir.exists()) {
            return std::make_tuple(QString(),nullptr);
        }

        QString file_path = parent_folder + "/" + hyperName;
        if(!timeStamp.isEmpty()) {
            file_path += "_";
            file_path += timeStamp;
        }
        file_path += ".param";        
        reader->SetPath(file_path);

        auto appProp = reader->ReadProp("AppName");
        if(appName.compare(appProp)!=0) {
            return std::make_tuple(QString(), nullptr);
        }

        auto procProp = reader->ReadProp("Processor");        

        auto param = reader->Read();

        auto paramVersion = reader->ReadProp("ParameterVersion");
        
        if(false == paramVersion.isEmpty()) {
            param->SetVersion(paramVersion);
        }

        return std::make_tuple(procProp,param);
    }
}