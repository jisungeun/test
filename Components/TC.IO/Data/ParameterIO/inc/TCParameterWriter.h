#pragma once

#include <memory>
#include <QString>

#include "IParameter.h"

#include "TCParameterIOExport.h"

namespace TC::IO {
    class TCParameterIO_API TCParameterWriter {
    public:
        explicit TCParameterWriter(const QString& parentPath);
        virtual ~TCParameterWriter();

        auto Write(IParameter::Pointer param, QString appName,QString reportName,            
            QString hyperName, QString procName,QString timeStamp = QString())->void;
        auto ForceWrite(IParameter::Pointer param, QString appName, QString reportName, QString hyperName, QString procName, QString timeStamp = QString())->void;
        auto Modify(IParameter::Pointer param)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}