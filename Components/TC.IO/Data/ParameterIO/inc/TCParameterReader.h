#pragma once

#include <memory>
#include <tuple>

#include "IParameter.h"

#include "TCParameterIOExport.h"

namespace TC::IO {
    class TCParameterIO_API TCParameterReader {
    public:
        explicit TCParameterReader(const QString& parentPath,bool isHistory= false);
        virtual ~TCParameterReader();

        auto Read(QString appName, QString hyperName, QString timeStamp = QString())->std::tuple<QString,IParameter::Pointer>;
        auto ReadHistory(QString hyperName, QString timeStamp)->std::tuple<QString,QString,QString, IParameter::Pointer>;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}