#include "ROI.h"

namespace TC::IO {
    struct ROI::Impl {
        QString id;
        ROIShapeType shapeType{ROIShapeType::SQUARE};
        std::tuple<Point3I, Point3I> bbox;
        QList<Point3D> vertices;
        std::tuple<int16_t, int16_t> zRange;
        int16_t zIndex;

        auto operator=(const Impl& other) -> Impl&;
        auto operator==(const Impl& other) const -> bool;
        auto operator!=(const Impl& other) const -> bool;
    };

    auto ROI::Impl::operator=(const Impl& other) -> Impl& {
	    id = other.id;
        shapeType = other.shapeType;
        bbox = {std::get<0>(other.bbox), std::get<1>(other.bbox)};
        vertices = other.vertices;
        zRange = other.zRange;
        zIndex = other.zIndex;

        return *this;
    }

    auto ROI::Impl::operator==(const Impl& other) const -> bool {
        if (id != other.id) return false;
        if (shapeType != other.shapeType) return false;
        if (bbox != other.bbox) return false;
        if (vertices != other.vertices) return false;
        if (zRange != other.zRange) return false;
        if (zIndex != other.zIndex) return false;

        return true;
    }

    auto ROI::Impl::operator!=(const Impl& other) const -> bool {
        if (id == other.id) return false;
        if (shapeType == other.shapeType) return false;
        if (bbox == other.bbox) return false;
        if (vertices == other.vertices) return false;
        if (zRange == other.zRange) return false;
        if (zIndex == other.zIndex) return false;

        return true;
    }

    ROI::ROI() : d{std::make_unique<Impl>()} {}

    ROI::ROI(const ROI& other) : d{std::make_unique<Impl>()} {
	    *d = *other.d;
    }

    ROI::~ROI() {}

    auto ROI::SetID(const QString& id) -> void {
	    d->id = id;
    }

    auto ROI::GetID() const -> QString {
	    return d->id;
    }

    auto ROI::SetShapeType(ROIShapeType shape) -> void {
	    d->shapeType = shape;
    }

    auto ROI::GetShapeType() const -> ROIShapeType {
	    return d->shapeType;
    }

    auto ROI::SetBoundingBox(const Point3I& p1, const Point3I& p2) -> void {
	    d->bbox = std::make_tuple(p1, p2);
    }

    auto ROI::GetBoundingBox() const -> std::tuple<Point3I, Point3I> {
	    return d->bbox;
    }

    auto ROI::SetVertices(const QList<Point3D>& vertices) -> void {
	    d->vertices = vertices;
    }

    auto ROI::GetVertices() const -> QList<Point3D> {
	    return d->vertices;
    }

    auto ROI::SetZRange(int16_t min, int16_t max) -> void {
	    d->zRange = std::make_tuple(min, max);
    }

    auto ROI::GetZRange() const -> std::tuple<int16_t, int16_t> {
	    return d->zRange;
    }

    auto ROI::SetZIndex(int16_t index) -> void {
	    d->zIndex = index;
    }

    auto ROI::GetZIndex() const -> int16_t {
	    return d->zIndex;
    }

    auto ROI::operator=(const ROI& other) -> ROI& {
	    *d = *other.d;
        return *this;
    }

    auto ROI::operator==(const ROI& other) const -> bool {
	    return *d == *other.d;
    }

    auto ROI::operator!=(const ROI& other) const -> bool {
	    return *d != *other.d;
    }

}