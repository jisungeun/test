#include "ROISet.h"

namespace TC::IO {
    struct ROISet::Impl {
        QString imageId;
        std::tuple<uint16_t, uint16_t, uint16_t> imageSize;
        std::tuple<double, double, double> imageResolution;
        QList<ROI> rois;

        auto operator=(const Impl& other) -> Impl&;
        auto operator==(const Impl& other) const -> bool;
        auto operator!=(const Impl& other) const -> bool;
    };

    auto ROISet::Impl::operator=(const Impl& other) -> Impl& {
	    imageId = other.imageId;
        imageSize = other.imageSize;
        rois = other.rois;
        imageResolution = other.imageResolution;
        return *this;
    }

    auto ROISet::Impl::operator==(const Impl& other) const -> bool {
	    if (imageId != other.imageId) return false;
        if (imageSize != other.imageSize) return false;
        if (rois != other.rois) return false;
        if (imageResolution != other.imageResolution) return false;

        return true;
    }

    auto ROISet::Impl::operator!=(const Impl& other) const -> bool {
	    if (imageId == other.imageId) return false;
        if (imageSize == other.imageSize) return false;
        if (rois == other.rois) return false;
        if (imageResolution == other.imageResolution) return false;

        return true;
    }

    ROISet::ROISet() : d{std::make_unique<Impl>()} {}

    ROISet::ROISet(const ROISet &other) : d{std::make_unique<Impl>()} {
	    *d = *other.d;
    }

    ROISet::~ROISet() {}

    auto ROISet::SetImage(const QString& id) -> void{
	    d->imageId = id;
    }

    auto ROISet::GetImage() const -> QString {
	    return d->imageId;
    }

    auto ROISet::SetImageSize(uint16_t x, uint16_t y, uint16_t z) -> void {
	    d->imageSize = std::make_tuple(x, y, z);
    }

    auto ROISet::GetImageSize() const -> std::tuple<uint16_t, uint16_t, uint16_t> {
	    return d->imageSize;
    }

    auto ROISet::SetImageResolution(double rx, double ry, double rz) -> void {
        d->imageResolution = std::make_tuple(rx, ry, rz);
	}

    auto ROISet::GetImageResolution()const -> std::tuple<double, double, double> {
        return d->imageResolution;
	}

    auto ROISet::AddROI(const ROI& roi) -> void {
	    d->rois << roi;
    }

    auto ROISet::GetROIs() const -> QList<ROI> {
	    return d->rois;
    }

    auto ROISet::operator=(const ROISet& other) -> ROISet& {
        *d = *other.d;
        return *this;
    }

    auto ROISet::operator==(const ROISet& other) const -> bool {
	    return *d == *other.d;
    }

    auto ROISet::operator!=(const ROISet& other) const -> bool {
	    return *d != *other.d;
    }

}