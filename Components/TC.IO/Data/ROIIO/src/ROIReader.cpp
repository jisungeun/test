#include <QFile>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "ROIReader.h"

namespace TC::IO {
	struct ROIReader::Impl {
		QString path;
	};

	ROIReader::ROIReader(const QString& path) : d{std::make_unique<Impl>()} {
		d->path = path;
	}

	ROIReader::~ROIReader() {
		
	}

	auto ROIReader::Read() const -> ROISet {
		if (d->path.isEmpty()) return ROISet();

		auto file = QFile(d->path);
		if (!file.open(QIODevice::ReadOnly)) return ROISet();

		const auto jsonData = file.readAll();
		file.close();

		const auto doc = QJsonDocument::fromJson(jsonData);
		const auto rootObj = doc.object();

		ROISet roiSet;

		// read image info
		if (rootObj.contains(ROI_IMAGE.data()) && rootObj[ROI_IMAGE.data()].isObject()) {
			const auto imageObj = rootObj[ROI_IMAGE.data()].toObject();
			if (imageObj.contains(ROI_IMAGE_ID.data()) && imageObj[ROI_IMAGE_ID.data()].isString())
				roiSet.SetImage(imageObj[ROI_IMAGE_ID.data()].toString());

			if (imageObj.contains(ROI_IMAGE_SIZE.data()) && imageObj[ROI_IMAGE_SIZE.data()].isArray()) {
				const auto imageSize = imageObj[ROI_IMAGE_SIZE.data()].toArray();
				if (imageSize.size() == 3)
					roiSet.SetImageSize(imageSize[0].toInt(), imageSize[1].toInt(), imageSize[2].toInt());
			}
			if (imageObj.contains(ROI_IMAGE_RES.data()) && imageObj[ROI_IMAGE_RES.data()].isArray()) {
				const auto imageRes = imageObj[ROI_IMAGE_RES.data()].toArray();
				if(imageRes.size() == 3) {
					roiSet.SetImageResolution(imageRes[0].toDouble(), imageRes[1].toDouble(), imageRes[2].toDouble());
				}
			}
		}

		// read ROIs
		if (rootObj.contains(ROI_ROIS.data()) && rootObj[ROI_ROIS.data()].isArray()) {
			const auto roiObjs = rootObj[ROI_ROIS.data()].toArray();
			for (auto& elem : roiObjs) {
				ROI roi;

				const auto roiObj = elem.toObject();
				if (roiObj.contains(ROI_ID.data()) && roiObj[ROI_ID.data()].isString())
					roi.SetID(roiObj[ROI_ID.data()].toString());

				if (roiObj.contains(ROI_SHAPE_TYPE.data()) && roiObj[ROI_SHAPE_TYPE.data()].isString()) {
					const auto shapeType = ROIShapeType::_from_string(roiObj[ROI_SHAPE_TYPE.data()].toString().toUtf8());
					roi.SetShapeType(shapeType);
				}

				if (roiObj.contains(ROI_BBOX.data()) && roiObj[ROI_BBOX.data()].isArray()) {
					const auto bboxArr = roiObj[ROI_BBOX.data()].toArray();
					if (bboxArr.size() == 6) {
						roi.SetBoundingBox(
							{static_cast<int16_t>(bboxArr[0].toInt()), static_cast<int16_t>(bboxArr[1].toInt()), static_cast<int16_t>(bboxArr[2].toInt())},
							{static_cast<int16_t>(bboxArr[3].toInt()), static_cast<int16_t>(bboxArr[4].toInt()), static_cast<int16_t>(bboxArr[5].toInt())}
						);
					}
				}

				if (roiObj.contains(ROI_VERTICES.data()) && roiObj[ROI_VERTICES.data()].isArray()) {
					const auto vertexArr = roiObj[ROI_VERTICES.data()].toArray();
					QList<ROI::Point3D> vertices;
					for (auto& elem : vertexArr) {
						const auto vertex = elem.toArray();
						vertices << ROI::Point3D{vertex[0].toDouble(), vertex[1].toDouble(), vertex[2].toDouble()};
					}

					if (!vertices.isEmpty()) roi.SetVertices(vertices);
				}

				if (roiObj.contains(ROI_Z_RANGE.data()) && roiObj[ROI_Z_RANGE.data()].isArray()) {
					const auto zRangeArr = roiObj[ROI_Z_RANGE.data()].toArray();
					if (zRangeArr.size() == 2) {
						roi.SetZRange(zRangeArr[0].toInt(), zRangeArr[1].toInt());
					}
				}

				if (roiObj.contains(ROI_DRAWING_Z_INDEX.data()))
					roi.SetZIndex(roiObj[ROI_DRAWING_Z_INDEX.data()].toInt());

				roiSet.AddROI(roi);
			}
		}

		return roiSet;
	}

}