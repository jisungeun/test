#include "ROIWriter.h"

#include <QFile>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace TC::IO {
	struct ROIWriter::Impl {
		QString path;
	};

	ROIWriter::ROIWriter(const QString& path) : d{std::make_unique<Impl>()} {
		d->path = path;
	}

	ROIWriter::~ROIWriter() {}

	auto ROIWriter::Write(const ROISet& roiSet) -> bool {
		if (d->path.isEmpty()) return false;

		QJsonObject rootObj;

		// write image info
		QJsonObject imageObj;

		imageObj[ROI_IMAGE_ID.data()] = roiSet.GetImage();

		auto [sizeX, sizeY, sizeZ] = roiSet.GetImageSize();
		imageObj[ROI_IMAGE_SIZE.data()] = QJsonArray({sizeX, sizeY, sizeZ});

		auto [resX, resY, resZ] = roiSet.GetImageResolution();
		imageObj[ROI_IMAGE_RES.data()] = QJsonArray({ resX,resY,resZ });

		rootObj[ROI_IMAGE.data()] = imageObj;

		// write ROI info
		QJsonArray roiObjs;
		for (auto &roi : roiSet.GetROIs()) {
			QJsonObject roiObj;

			roiObj[ROI_ID.data()] = roi.GetID();
			roiObj[ROI_SHAPE_TYPE.data()] = roi.GetShapeType()._to_string();

			auto [p1, p2] = roi.GetBoundingBox();
			roiObj[ROI_BBOX.data()] = QJsonArray({
				std::get<0>(p1), std::get<1>(p1), std::get<2>(p1),
				std::get<0>(p2), std::get<1>(p2), std::get<2>(p2)
			});

			QJsonArray vertices;
			for (auto &vertex : roi.GetVertices())
				vertices << QJsonArray({std::get<0>(vertex), std::get<1>(vertex), std::get<2>(vertex)});

			roiObj[ROI_VERTICES.data()] = vertices;

			auto [zMin, zMax] = roi.GetZRange();
			roiObj[ROI_Z_RANGE.data()] = QJsonArray({zMin, zMax});

			roiObj[ROI_DRAWING_Z_INDEX.data()] = roi.GetZIndex();

			roiObjs << roiObj;
		}

		rootObj[ROI_ROIS.data()] = roiObjs;

		// write into a file
		auto doc = QJsonDocument(rootObj);

		auto file = QFile(d->path);
		if (!file.open(QIODevice::WriteOnly)) return false;

		file.write(doc.toJson());
		file.close();

		return true;
	}

}