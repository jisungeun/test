#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#include <QDir>
#include <QTemporaryFile>

#include <ROIWriter.h>
#include <ROIReader.h>

using namespace TC::IO;

TEST_CASE("ROI Read/Write") {
	// create dummy ROISet
	QList<ROI::Point3D> dummyVertices;
	dummyVertices << ROI::Point3D{0., 0., 0.};
	dummyVertices << ROI::Point3D{0.1, 0.1, 0.1};
	dummyVertices << ROI::Point3D{0.2, 0.2, 0.2};

	QList<ROI> dummyROIs;
	for (auto i = 0; i < 5; i++) {
		ROI roi;
		roi.SetID(QString("TEST_ROI_%1").arg(i));
		roi.SetShapeType(ROIShapeType::_from_integral(i % ROIShapeType::_size()));
		roi.SetBoundingBox({0, 0, 0}, {1, 1, 1});
		roi.SetVertices(dummyVertices);
		roi.SetZRange(i, i*10);
		roi.SetZIndex(i*10 / 2);

		dummyROIs << roi;
	}

	ROISet dummyROISet;
	dummyROISet.SetImage("ABCDEF");
	dummyROISet.SetImageSize(1024, 1024, 60);
	for (auto &roi : dummyROIs)
		dummyROISet.AddROI(roi);

	
	QTemporaryFile testFile;
	testFile.open();
	testFile.close();

	auto tempFilePath = testFile.fileName();

	auto writer = ROIWriter(tempFilePath);
	REQUIRE(writer.Write(dummyROISet));

	auto reader = ROIReader(tempFilePath);
	auto readROISet = reader.Read();

	REQUIRE(dummyROISet == readROISet);
}