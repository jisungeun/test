#pragma once

#include <memory>

#include <enum.h>

#include <QList>

#include "TCROIIOExport.h"


namespace TC::IO {
    constexpr std::string_view ROI_ID{"id"};
    constexpr std::string_view ROI_SHAPE_TYPE{"shapeType"};
    constexpr std::string_view ROI_BBOX{"bbox"};
    constexpr std::string_view ROI_VERTICES{"vertices"};
    constexpr std::string_view ROI_Z_RANGE{"zRange"};
    constexpr std::string_view ROI_DRAWING_Z_INDEX{"zIndex"};

    BETTER_ENUM(ROIShapeType, uint16_t,
        SQUARE,
        CIRCLE,
        ELLIPSOID,
        POLYGON,
        LASSO
    );

    class TCROIIO_API ROI {
    public:
        using Point3I = std::tuple<int16_t, int16_t, int16_t>;
		using Point3D = std::tuple<double, double, double>;

        ROI();
        ROI(const ROI &other);
        virtual ~ROI();

        auto SetID(const QString& id) -> void;
        auto GetID() const -> QString;

        auto SetShapeType(ROIShapeType shape) -> void;
        auto GetShapeType() const -> ROIShapeType;

        auto SetBoundingBox(const Point3I& p1, const Point3I& p2) -> void;
        auto GetBoundingBox() const -> std::tuple<Point3I, Point3I>;

        auto SetVertices(const QList<Point3D>& vertices) -> void;
        auto GetVertices() const -> QList<Point3D>;

        auto SetZRange(int16_t min, int16_t max) -> void;
        auto GetZRange() const -> std::tuple<int16_t, int16_t>;

        auto SetZIndex(int16_t index) -> void;
        auto GetZIndex() const -> int16_t;

        auto operator=(const ROI &other) -> ROI&;
        auto operator==(const ROI &other) const -> bool;
        auto operator!=(const ROI &other) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}