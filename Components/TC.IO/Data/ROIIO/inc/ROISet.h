#pragma once

#include <memory>

#include <QList>

#include "ROI.h"

#include "TCROIIOExport.h"

namespace TC::IO {
    constexpr std::string_view ROI_IMAGE{"image"};
    constexpr std::string_view ROI_IMAGE_ID{"id"};
    constexpr std::string_view ROI_IMAGE_SIZE{"size"};
    constexpr std::string_view ROI_IMAGE_RES{ "resolution" };
    constexpr std::string_view ROI_ROIS{"rois"};    

    class TCROIIO_API ROISet {
    public:
        ROISet();
        ROISet(const ROISet &other);
        virtual ~ROISet();

        auto SetImage(const QString& id) -> void;
        auto GetImage() const -> QString;

        auto SetImageSize(uint16_t x, uint16_t y, uint16_t z) -> void;
        auto GetImageSize() const -> std::tuple<uint16_t, uint16_t, uint16_t>;

        auto SetImageResolution(double rx, double ry, double rz)->void;
        auto GetImageResolution()const->std::tuple<double, double, double>;

        auto AddROI(const ROI& roi) -> void;
        auto GetROIs() const -> QList<ROI>;

        auto operator=(const ROISet& other) -> ROISet&;
        auto operator==(const ROISet& other) const -> bool;
        auto operator!=(const ROISet& other) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}