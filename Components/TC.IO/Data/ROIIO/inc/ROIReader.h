#pragma once

#include <memory>
#include <optional>

#include "ROISet.h"

#include "TCROIIOExport.h"

namespace TC::IO {
    class TCROIIO_API ROIReader {
    public:
        explicit ROIReader(const QString& path);
        virtual ~ROIReader();

        auto Read() const -> ROISet;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}