#pragma once

#include <memory>

#include "ROISet.h"

#include "TCROIIOExport.h"


namespace TC::IO {
    class TCROIIO_API ROIWriter {
    public:
        explicit ROIWriter(const QString& path);
        virtual ~ROIWriter();

        auto Write(const ROISet& roiSet) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}