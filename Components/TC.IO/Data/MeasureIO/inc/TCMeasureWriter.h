#pragma once

#include <memory>
#include <QString>
#include <QVector>
#include "TCMeasureIOExport.h"


namespace TC::IO {
    class TCMeasureIO_API TCMeasureWriter {
    public:
        explicit TCMeasureWriter(const QString& path,int max_cnt,int columns);
        virtual ~TCMeasureWriter();

        /**
         * \brief Measures 그룹의 메타 정보 기록
         * \return true/false
         */
        auto WriteMeasuresMetaInfo(double timeInterval)->bool;

        /**
         * \brief 단일 Meausre 정보를 기록
         * \param dataID 데이타 그룹         
         * \param cellKey cell 번호
         * \param time_idx 시간축
         * \param val measurement scalar값
         * \return true/false
         */
        auto WriteMeasures(const QString& dataID, int cellKey, int time_idx,double time_point,QList<double> val)->bool;
        /**
         * \brief Update measure contents
         * \param dataID 데이타 그룹
         * \param cellKey cell 번호
         * \param time_idx 시간축
         * \param val measurement scalar값
         * \return true/false
         */
        auto UpdateMeasures(const QString& dataID, int cellKey, int time_idx,double time_point, QList<double> val)->bool;        
        /**
         * \brief Measure 항목을 기록         
         * \param names measurement name list
         * \return true/false
         */
        auto WriteMeasureNames(QStringList names)->bool;
        /**
         * \brief Measure count를 초기화
         * \param dataID 데이터 그룹
         * \param time_idx 시간축
         * \return true/false
         */
        auto ClearMeausreCount(const QString& dataID, int time_idx)->bool;

        auto SetCellCount(int cell_count)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}