#pragma once

#include <memory>
#include <tuple>

#include <QStringList>

#include "TCMeasureIOExport.h"

namespace TC::IO {
    class TCMeasureIO_API TCMeasureReader {
    public:
        explicit TCMeasureReader(const QString& path);
        virtual ~TCMeasureReader();

        /**
         * \brief HDF5 포맷 파일 존재하는지 여부 확인
         * \return true/false
         */
        auto Exist() const->bool;
        /**
         * \brief cell organ 목록
         * \return 측정에서 organ 목록
         */
        auto GetOrganNames() const->QStringList;
        /**
         * \brief 특정 데이타 그룹의 Time index 개수
         * \param dataID 데이타 그룹
         * \return time index 개수
         */
        auto GetTimeIndexCount(const QString& dataID) const->int32_t;
        /**
         * \brief 특정 데이타의 Cell 개수
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \return blob 개수
         */
        auto GetCellCount(const QString& dataID, int timeIndex) const->int32_t;
        /**
         * \brief 특정 time step의 특정 cell의 측정 결과
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param cellKey cell index
         * \return Measure list
         */
        auto ReadMeasures(const QString& dataID,int timeIndex,int cellIndex)const->QList<double>;
        /**
         * \brief Measure 항목 리스트         
         * \return Measure 항목
         */
        auto GetMeasureNames()const->QStringList;

        /**
         * \brief Time point list
         * \param dataID 데이타 그룹
         * \return Measure 항목
         */
        auto GetTimePoints(const QString& dataID)const->QList<int>;

        /**
         * \brief Time interval
         * \param dataID 데이타 그룹
         * \return Measure의 TimeInterval attribute 값
         */
        auto GetTimeInterval()const->double;
        
        /**
         * \brief real Time point
         * \param dataID
         * \param timeIndex
         * \return Measure time point attribute
         */
        auto GetTimePoint(const QString& dataID,int timeIndex)const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}