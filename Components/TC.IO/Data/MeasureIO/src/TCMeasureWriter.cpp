#include "TCMeasureWriter.h"
#include <QStringList>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <QFileInfo>

#include <HDF5Mutex.h>

namespace TC::IO {
    struct TCMeasureWriter::Impl {
        QString path;
        uint32_t maxCounts = 100;
        uint32_t columns;
        auto OpenGroup(H5::H5File& file, const QString strName)->H5::Group {
            H5::Group group;
            if(file.exists(strName.toStdString())) {
                group = file.openGroup(strName.toStdString());
            }else {
                group = file.createGroup(strName.toStdString());
            }            
            return group;
        }
        auto OpenGroup(H5::Group& parentGroup, const QString strName)->H5::Group {
            H5::Group group;
            if(parentGroup.exists(strName.toStdString())){
                group = parentGroup.openGroup(strName.toStdString());
            }else {
                group = parentGroup.createGroup(strName.toStdString());
            }            
            return group;
        }
        auto CreateGroupTree(H5::Group& group, const QString& dataID)->H5::Group {
            H5::Group blobGroup = group;
            QStringList names = dataID.split("/");
            for (auto name : names) {
                blobGroup = OpenGroup(blobGroup, name);
            }
            return blobGroup;
        }
        auto OpenOrCreateNameGroup(H5::H5File& file)->H5::Group {
            H5::Group group = OpenGroup(file, "Names");
            return group;
        }
        auto OpenOrCreateMeasureGroup(H5::H5File& file, const QString dataID)->H5::Group {
            QStringList names = dataID.split("/");
            H5::Group group = OpenGroup(file, "Measures");            
            for(auto i=0; i<names.size();i++) {
                group = OpenGroup(group, names[i]);
            }
            return group;
        }
        auto ForceCreateMeasureDataSet(H5::Group& group,int timeIndex)->H5::DataSet {
            std::cout << "Force Create Meausre Data Set" << std::endl;
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            H5::DataSet dataSet;
            if(group.exists(name.toStdString())) {
                std::string channelName = name.toStdString();
                H5Ldelete(group.getId(), channelName.data(), H5P_DEFAULT);
            }
            hsize_t dims[2] = { maxCounts,columns };
            auto space = H5::DataSpace(2, dims);

            H5::DSetCreatPropList cparams;
            double fillValue = 0.0;
            cparams.setFillValue(H5::PredType::NATIVE_DOUBLE, &fillValue);

            dataSet = group.createDataSet(name.toStdString(), H5::PredType::NATIVE_DOUBLE, space, cparams);
            space.close();

            return dataSet;
        }
        auto OpenOrCreateMeasureDataSet(H5::Group& group, int timeIndex)->H5::DataSet {
            const QString name = QString("%1").arg(timeIndex,6,10, QLatin1Char('0'));
            H5::DataSet dataSet;
            if(group.exists(name.toStdString())) {
                dataSet = group.openDataSet(name.toStdString());
            }else {
                hsize_t dims[2] = { maxCounts,columns };
                auto space = H5::DataSpace(2, dims);

                H5::DSetCreatPropList cparams;
                double fillValue = 0.0;
                cparams.setFillValue(H5::PredType::NATIVE_DOUBLE, &fillValue);

                dataSet = group.createDataSet(name.toStdString(), H5::PredType::NATIVE_DOUBLE, space, cparams);
                cparams.close();
                space.close();                
            }            
            return dataSet;
        }
        auto OpenOrCreateAttribute(
            const H5::H5Object& parentObject
            , const QString& attributeName
            , const H5::DataType& dataType
        )->H5::Attribute {
            H5::Attribute attribute;
            try {
                const auto cStrName = std::string(attributeName.toLocal8Bit().constData());
                if (parentObject.attrExists(cStrName)) {
                    attribute = parentObject.openAttribute(cStrName);
                }
                else {
                    const auto rank = 1;
                    const hsize_t dims[1] = { 1 };
                    auto strDataSpace = H5::DataSpace(rank, dims);

                    attribute = parentObject.createAttribute(cStrName, dataType, strDataSpace);

                    strDataSpace.close();
                }
            } catch (H5::Exception& ) {
            }

            return attribute;
        }
        auto WriteAttributeDouble(const H5::H5Object& target, const QString& name, const double& contents)->void {
            auto doubleDataType{ H5::PredType::NATIVE_DOUBLE };
            H5::Attribute attribute;
            try {
                doubleDataType = H5::PredType::NATIVE_DOUBLE;
                attribute = OpenOrCreateAttribute(target, name, doubleDataType);
                attribute.write(doubleDataType, &contents);
            }
            catch (H5::Exception& ) {
            }

            attribute.close();
            doubleDataType.close();
        }
        auto WriteAttributeInt64(const H5::H5Object& target, const QString& name, const int64_t& contents)->void {
            auto int64DataType{ H5::PredType::NATIVE_INT64 };
            H5::Attribute attribute;
            try {
                int64DataType = H5::PredType::NATIVE_INT64;
                attribute = OpenOrCreateAttribute(target, name, int64DataType);
                attribute.write(int64DataType, &contents);
            }
            catch (H5::Exception& ) {
            }

            attribute.close();
            int64DataType.close();
        }        
        auto SetTimePoint(H5::DataSet dataset, double time_point)->void {
            double data[1]{ time_point };
            H5::Attribute attr;
            auto doubleDataType = H5::PredType::NATIVE_DOUBLE;
            try {                
                auto attr = OpenOrCreateAttribute(dataset, "TimePoint", doubleDataType);
                attr.write(doubleDataType, data);
            }
            catch (H5::Exception&) {

            }
            attr.close();
            doubleDataType.close();
        }
        auto SetCellCount(H5::DataSet dataset, int count)->void {
            int32_t data[1]{ count };
            auto attr = dataset.openAttribute("Count");
            attr.write(H5::PredType::NATIVE_INT32, data);
            attr.close();
        }
        auto GetCellCount(H5::DataSet data)->uint32_t {
            uint32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = data.createAttribute("Count", H5::PredType::NATIVE_INT32, space);

                int32_t ddata[1]{ 0 };
                attr.write(H5::PredType::NATIVE_INT32, ddata);
                space.close();
                attr.close();                
            }
            return count;
        }
    };

    TCMeasureWriter::TCMeasureWriter(const QString& path, int max_cnt, int columns) : d{ new Impl } {
        d->path = path;
        d->maxCounts = max_cnt;
        d->columns = columns+1;

        if (!QFileInfo::exists(d->path)) {
            HDF5MutexLocker lock(HDF5Mutex::GetInstance());
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_CREAT | H5F_ACC_RDWR);
            file.close();
        }
    }
    TCMeasureWriter::~TCMeasureWriter() {
        
    }
    auto TCMeasureWriter::WriteMeasureNames(QStringList names) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());        
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto group = d->OpenOrCreateNameGroup(file);
            {
                H5::StrType strdatatype(H5::PredType::C_S1, 256);
                for(uint32_t i=0;i<d->columns-1;i++) {
                    std::string attrName = "measure" + std::to_string(i);
                    if(group.attrExists(attrName)) {
                        auto attr = group.openAttribute(attrName);
                        attr.write(strdatatype, names[i].toStdString());
                        attr.close();
                    }else {
                        H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
                        auto attr = group.createAttribute(attrName, strdatatype, attr_dataspace);
                        attr.write(strdatatype, names[i].toStdString());
                        attr_dataspace.close();
                        attr.close();                        
                    }
                }
            }
            group.close();
            file.close();
        }catch(...){
            return false;
        }
        return true;
    }
    auto TCMeasureWriter::SetCellCount(int cell_count) -> void {
        d->maxCounts = cell_count;
    }

    auto TCMeasureWriter::ClearMeausreCount(const QString& dataID, int time_idx) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());        
        H5::Exception::dontPrint();        
        try {            
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);            
            auto measureGroup = d->OpenGroup(file, "Measures");
            auto group = d->OpenGroup(measureGroup, dataID);
            const QString name = QString("%1").arg(time_idx, 6, 10, QLatin1Char('0'));
            auto dataSet = d->OpenOrCreateMeasureDataSet(group, time_idx);            
            int32_t data[1]{ 0 };
            if(dataSet.attrExists("Count")) {                
                auto attr = dataSet.openAttribute("Count");
                attr.write(H5::PredType::NATIVE_INT32, data);
                attr.close();
            }else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = dataSet.createAttribute("Count", H5::PredType::NATIVE_INT32, space);
                attr.write(H5::PredType::NATIVE_INT32, data);
                attr.close();
            }
            dataSet.close();
            group.close();
            measureGroup.close();
            file.close();
        }catch(H5::Exception& /*ex*/) {
            return false;            
        }
        return true;
    }
    auto TCMeasureWriter::WriteMeasuresMetaInfo(double timeInterval) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();        
        bool result = true;        
        H5::H5File file;
        H5::Group measureGroup;
        try {
            file = H5::H5File(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            measureGroup = d->OpenGroup(file, "Measures");

            d->WriteAttributeDouble(measureGroup, "TimeInterval", timeInterval);
            
        } catch (H5::Exception& ) {
            result = false;
        }

        measureGroup.close();
        file.close();

        return result;
    }
    auto TCMeasureWriter::UpdateMeasures(const QString& dataID, int cellKey, int time_idx,double time_point, QList<double> val) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();        
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto group = d->OpenOrCreateMeasureGroup(file, dataID);//Lipid
            {
                //auto dataSet = d->OpenOrCreateMeasureDataSet(group, time_idx);
                auto dataSet = d->ForceCreateMeasureDataSet(group, time_idx);
                auto cells = d->GetCellCount(dataSet);                
                auto fileSpace = dataSet.getSpace();
                hsize_t size[2]{ 1, d->columns };
                hsize_t offset[2]{ cells ,0 };
                fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

                auto memSpace = H5::DataSpace(2, size);

                auto data = new double[val.size() + 1];
                data[0] = cellKey;//
                for (auto i = 1; i < val.size() + 1; i++) {
                    data[i] = val[i - 1];                    
                }

                dataSet.write(data, H5::PredType::NATIVE_DOUBLE, memSpace, fileSpace);
                delete[] data;
                d->SetCellCount(dataSet, cells + 1);
                d->SetTimePoint(dataSet, time_point);
                memSpace.close();
                fileSpace.close();
                dataSet.close();
            }
            group.close();
            file.close();
        }
        catch (H5::Exception& /*ex*/) {
            return false;
        }
        return true;
    }

    auto TCMeasureWriter::WriteMeasures(const QString& dataID, int cellKey, int time_idx,double time_point, QList<double> val) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();        
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);            
            auto group = d->OpenOrCreateMeasureGroup(file, dataID);//Lipid
            {
                auto dataSet = d->OpenOrCreateMeasureDataSet(group, time_idx);
                auto cells = d->GetCellCount(dataSet);

                auto fileSpace = dataSet.getSpace();
                hsize_t size[2] { 1, d->columns };
                hsize_t offset[2]{ cells ,0 };
                fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);
                
                auto memSpace = H5::DataSpace(2, size);

                auto data = new double[val.size()+1];
                data[0] = cellKey;//
                for(auto i=1;i<val.size()+1;i++) {
                    data[i] = val[i-1];
                    //*(data+i) = val[i];
                }

                dataSet.write(data, H5::PredType::NATIVE_DOUBLE, memSpace, fileSpace);                
                delete[] data;
                d->SetCellCount(dataSet, cells + 1);
                d->SetTimePoint(dataSet, time_point);
                memSpace.close();
                fileSpace.close();
                dataSet.close();
            }
            group.close();
            file.close();
        }catch(H5::Exception& ) {            
            return false;
        }
        return true;
    }   
}
