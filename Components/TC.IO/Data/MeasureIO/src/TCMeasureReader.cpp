#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include <HDF5Mutex.h>
#include "TCMeasureReader.h"

#include <QFileInfo>

namespace TC::IO {
	struct TCMeasureReader::Impl {
		QString path;
	};
	TCMeasureReader::TCMeasureReader(const QString& path) : d{ new Impl } {
		d->path = path;
	}
	TCMeasureReader::~TCMeasureReader() {

	}
	auto TCMeasureReader::Exist() const -> bool {
		return QFileInfo::exists(d->path);
		/*
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());

		try {
			H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
		}
		catch (...) {
			return false;
		}

		return true;*/
	}
	auto TCMeasureReader::GetMeasureNames()const -> QStringList {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		QStringList measures;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists("Names")) {
					auto group = file.openGroup("Names");
					auto attrN = group.getNumAttrs();
					H5::StrType strdatatype(H5::PredType::C_S1, 256);
					for (auto i = 0; i < attrN; i++) {
						H5std_string strreadbuf("");
						auto name = "measure" + std::to_string(i);
						auto attr = group.openAttribute(name);
						attr.read(strdatatype, strreadbuf);
						measures.push_back(QString(strreadbuf.c_str()));
						attr.close();
					}
					strdatatype.close();
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return measures;
	}
	auto TCMeasureReader::GetOrganNames() const -> QStringList {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		QStringList org_names;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists("Measures")) {
					auto group = file.openGroup("Measures");
					auto organs = group.getNumObjs();
					for (auto i = 0; i < organs; i++) {
						auto organName = QString::fromStdString(group.getObjnameByIdx(i));
						org_names.append(organName);
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return org_names;
	}
	auto TCMeasureReader::GetTimeIndexCount(const QString& dataID) const -> int32_t {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		int32_t count = 0;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists(dataID.toStdString())) {
					auto group = file.openGroup(dataID.toStdString());
					count = group.getNumObjs();
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return count;
	}
	auto TCMeasureReader::GetTimePoints(const QString& dataID) const -> QList<int> {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		QList<int> result;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists(dataID.toStdString())) {
					auto group = file.openGroup(dataID.toStdString());
					auto cnt = group.getNumObjs();
					for (auto i = 0; i < cnt; i++) {
						auto val = QString(group.getObjnameByIdx(i).c_str()).toInt();
						result.push_back(val);
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return result;
	}
	auto TCMeasureReader::GetTimePoint(const QString& dataID, int timeindex)const->double {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		double point = -1.0;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists(dataID.toStdString())) {
					auto group = file.openGroup(dataID.toStdString());
					const QString name = QString("%1").arg(timeindex, 6, 10, QLatin1Char('0'));
					if (group.exists(name.toStdString())) {
						auto dataSet = group.openDataSet(name.toStdString());
						if (dataSet.attrExists("TimePoint")) {
							auto attr = dataSet.openAttribute("TimePoint");
							attr.read(H5::PredType::NATIVE_DOUBLE, &point);
							attr.close();
						}
						dataSet.close();
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return point;
	}
	auto TCMeasureReader::GetCellCount(const QString& dataID, int timeIndex) const -> int32_t {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		int32_t count = 0;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists(dataID.toStdString())) {
					auto group = file.openGroup(dataID.toStdString());
					const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
					if (group.exists(name.toStdString())) {
						auto dataSet = group.openDataSet(name.toStdString());
						if (dataSet.attrExists("Count")) {
							auto attr = dataSet.openAttribute("Count");
							attr.read(H5::PredType::NATIVE_INT32, &count);
							attr.close();
						}
						dataSet.close();
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return count;
	}
	auto TCMeasureReader::ReadMeasures(const QString& dataID, int timeIndex, int cellIndex) const -> QList<double> {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		QList<double> measures;
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists(dataID.toStdString())) {
					auto group = file.openGroup(dataID.toStdString());
					const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
					if (group.exists(name.toStdString())) {
						auto dataSet = group.openDataSet(name.toStdString());

						//auto measure_names = GetMeasureNames();
						QVector<QString> measure_names;
						auto fgroup = file.openGroup("Names");
						auto attrN = fgroup.getNumAttrs();
						H5::StrType strdatatype(H5::PredType::C_S1, 256);
						for (auto i = 0; i < attrN; i++) {
							H5std_string strreadbuf("");
							auto nname = "measure" + std::to_string(i);
							auto attr = fgroup.openAttribute(nname);
							attr.read(strdatatype, strreadbuf);
							measure_names.push_back(QString(strreadbuf.c_str()));
							attr.close();
						}
						strdatatype.close();
						fgroup.close();
						hsize_t msize = measure_names.count() + 1;
						auto fileSpace = dataSet.getSpace();
						hsize_t size[]{ 1,msize };
						hsize_t offset[]{ static_cast<hsize_t>(cellIndex),0 };
						fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

						auto memSpace = H5::DataSpace(2, size);

						auto data = new double[msize];
						dataSet.read(data, H5::PredType::NATIVE_DOUBLE, memSpace, fileSpace);						
						for (auto i = 0; i < msize - 1; i++) {
							measures.push_back(data[i + 1]);							
						}
						memSpace.close();
						fileSpace.close();
						dataSet.close();
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}
		return measures;

	}
	auto TCMeasureReader::GetTimeInterval() const -> double {
		double interval = 0.0;

		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		if (Exist()) {
			try {
				H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
				if (file.exists("Measures")) {
					auto group = file.openGroup("Measures");
					if (group.attrExists("TimeInterval")) {
						auto attr = group.openAttribute("TimeInterval");
						attr.read(H5::PredType::NATIVE_DOUBLE, &interval);
						attr.close();
					}
					group.close();
				}
				file.close();
			}
			catch (...) {

			}
		}

		return interval;
	}
}
