#pragma once
#include <memory>

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#include <H5DataSpace.h>
#pragma warning(pop)

#include "TCHDF5UtilitiesExport.h"
#include "MemoryChunk.h"
#include "DataRange.h"

namespace TC::HDF5Utilities {
    class TCHDF5Utilities_API Hdf5DataSetRoiReader {
    public:
        explicit Hdf5DataSetRoiReader(H5::DataSet& dataSet);
        ~Hdf5DataSetRoiReader();
        static auto ReadAll(H5::DataSet& dataSet)-> IO::MemoryChunk::Pointer;
        auto Read(const IO::DataRange& dataRange)->IO::MemoryChunk::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto CheckDataRangeIsValid(const IO::DataRange& dataRange) const ->bool;
        auto CheckDimensionality(const IO::DataRange& dataRange) const ->bool;
        auto CheckOffsetPoint(const IO::Point& offsetPoint) const ->bool;
        auto CheckLastPoint(const IO::Point& lastPoint) const ->bool;

        auto AllocateData(const IO::Count& count) const->std::any;
        auto AllocateData(const size_t& countElements) const->std::any;

        auto ReadRoiMemory(const IO::DataRange& dataRange, const std::any& data) const ->void;

        auto GetSelectedSpace(const IO::DataRange& selectedDataRange) const ->H5::DataSpace;
        static auto GetOutputSpace(const IO::Count& count) ->H5::DataSpace;
        auto ReadRoiMemoryFromDataSet(const H5::DataSpace& outSpace, const H5::DataSpace& selectedSpace,
            const std::any& data) const ->void;
        auto GetDataPointer(const std::any& data) const ->void*;
        auto GenerateMemoryChunkData(const std::any& data, const IO::Count& count) const
            ->IO::MemoryChunk::Pointer;
    };
}
