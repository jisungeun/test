#pragma once
#include <memory>

#include "TCHDF5UtilitiesExport.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <QStringList>

namespace TC::HDF5Utilities {
    class TCHDF5Utilities_API Hdf5DataSetPathScanner {
    public:
        Hdf5DataSetPathScanner();
        ~Hdf5DataSetPathScanner();

        auto Scan(const H5::H5File& srcFile)->QStringList;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto ScanRecursively(const H5::Group& srcGroup)->void;
    };
}
