#pragma once

#include "TCHDF5UtilitiesExport.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include "Dimension.h"
#include "MemoryChunk.h"

namespace TC::HDF5Utilities {
    auto TCHDF5Utilities_API GetDimension(const H5::DataSet& dataSet)-> IO::Dimension; 
    auto TCHDF5Utilities_API GetDimension(const H5::DataSpace& dataSpace)->IO::Dimension;
    auto TCHDF5Utilities_API GetChunkDataType(const H5::DataSet& dataSet)->IO::ChunkDataType;
    auto TCHDF5Utilities_API ToChunkDataType(const H5::DataType& h5DataType)->IO::ChunkDataType;
    auto TCHDF5Utilities_API ToH5DataType(const IO::ChunkDataType& chunkDataType)->H5::DataType;
    auto TCHDF5Utilities_API AllocateRawData(const H5::DataType& dataType, const size_t& numberOfElements)->void*; 
    auto TCHDF5Utilities_API ReadDataSet(const H5::DataSet& dataSet)->IO::MemoryChunk::Pointer;
    auto TCHDF5Utilities_API GetDataSpace(const IO::Dimension& dimension)->H5::DataSpace;

    auto TCHDF5Utilities_API GetHDF5StringDataType(const std::string& string)->H5::StrType;
    auto TCHDF5Utilities_API SimpleDataSpace()->H5::DataSpace; 
    auto TCHDF5Utilities_API CreateAndWriteAttributeInt64(const H5::H5Object& object, const std::string& attributeName,
        const int64_t& value)->void; 
    auto TCHDF5Utilities_API CreateAndWriteAttributeDouble(const H5::H5Object& object, const std::string& attributeName,
        const double& value)->void; 
    auto TCHDF5Utilities_API CreateAndWriteAttributeString(const H5::H5Object& object, const std::string& attributeName,
        const std::string& string)->void;

    auto TCHDF5Utilities_API ReadAttributeInt64(const H5::H5Object& object, const std::string& attributeName)->int64_t;
    auto TCHDF5Utilities_API ReadAttributeDouble(const H5::H5Object& object, const std::string& attributeName)->double;
    auto TCHDF5Utilities_API ReadAttributeString(const H5::H5Object& object, const std::string& attributeName)->std::string;

    auto TCHDF5Utilities_API CopyAttribute(const H5::Attribute& srcAttribute, const H5::H5Object& destObject)->void;
    auto TCHDF5Utilities_API CopyAllAttributes(const H5::H5Object& srcObject, const H5::H5Object& destObject)->void;

    auto TCHDF5Utilities_API GetGroupName(const H5::Group& group)->std::string;
    auto TCHDF5Utilities_API CopyGroupOnly(const H5::Group& srcGroup, const H5::Group& destPositionGroup)->void;
}