#include "Hdf5Utilities.h"
#include "CommonUtilities.h"
#include <QStringList>

namespace TC::HDF5Utilities {
    using namespace IO;

    auto GetDimension(const H5::DataSet& dataSet) -> Dimension {
        auto targetDataSpace = dataSet.getSpace();
        const auto dataSetDimension = GetDimension(targetDataSpace);
        targetDataSpace.close();
        return dataSetDimension;
    }

    auto GetDimension(const H5::DataSpace& dataSpace) -> Dimension {
        const auto dimensionality = dataSpace.getSimpleExtentNdims();
        const std::shared_ptr<hsize_t[]> dataSetSizes(new hsize_t[dimensionality]());
        dataSpace.getSimpleExtentDims(dataSetSizes.get());
        Dimension dimension;
        if (dimensionality == 1) {
            dimension = Dimension(
                static_cast<size_t>(dataSetSizes[0]),
                static_cast<size_t>(1));
        } else if (dimensionality == 2) {
            dimension = Dimension(
                static_cast<size_t>(dataSetSizes[1]),
                static_cast<size_t>(dataSetSizes[0]));
        } else if (dimensionality == 3) {
            dimension = Dimension(
                static_cast<size_t>(dataSetSizes[2]),
                static_cast<size_t>(dataSetSizes[1]),
                static_cast<size_t>(dataSetSizes[0]));
        }

        return dimension;
    }

    auto GetChunkDataType(const H5::DataSet& dataSet) -> ChunkDataType {
        const auto h5DataType = dataSet.getDataType();
        return ToChunkDataType(h5DataType);
    }

    auto ToChunkDataType(const H5::DataType& h5DataType) -> ChunkDataType {
        ChunkDataType chunkDataType{ ChunkDataType::Int8Type };
        if (h5DataType == H5::DataType(H5::PredType::NATIVE_INT8)) {
            chunkDataType = ChunkDataType::Int8Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_UINT8)) {
            chunkDataType = ChunkDataType::UInt8Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_INT16)) {
            chunkDataType = ChunkDataType::Int16Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_UINT16)) {
            chunkDataType = ChunkDataType::UInt16Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_INT32)) {
            chunkDataType = ChunkDataType::Int32Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_UINT32)) {
            chunkDataType = ChunkDataType::UInt32Type;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_FLOAT)) {
            chunkDataType = ChunkDataType::FloatType;
        } else if (h5DataType == H5::DataType(H5::PredType::NATIVE_DOUBLE)) {
            chunkDataType = ChunkDataType::DoubleType;
        } else {
        }
        return chunkDataType;
    }

    auto ToH5DataType(const ChunkDataType& chunkDataType) -> H5::DataType {
        H5::DataType dataType;
        if (chunkDataType == +ChunkDataType::Int8Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_INT8);
        } else if (chunkDataType == +ChunkDataType::UInt8Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT8);
        } else if (chunkDataType == +ChunkDataType::Int16Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_INT16);
        } else if (chunkDataType == +ChunkDataType::UInt16Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);
        } else if (chunkDataType == +ChunkDataType::Int32Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_INT32);
        } else if (chunkDataType == +ChunkDataType::UInt32Type) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT32);
        } else if (chunkDataType == +ChunkDataType::FloatType) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT);
        } else if (chunkDataType == +ChunkDataType::DoubleType) {
            dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_DOUBLE);
        } else {
        }
        return dataType;
    }

    auto AllocateRawData(const H5::DataType& dataType, const size_t& numberOfElements) -> void* {
        return AllocateRawData(ToChunkDataType(dataType), numberOfElements);
    }

    auto ReadDataSet(const H5::DataSet& dataSet) -> MemoryChunk::Pointer {
        const auto dataSetDimension = GetDimension(dataSet);
        const auto chunkDataType = GetChunkDataType(dataSet);

        const auto numberOfElements = dataSetDimension.GetNumberOfElements();
        const auto tileData = AllocateRawData(chunkDataType, numberOfElements);
        dataSet.read(tileData, dataSet.getDataType());

        auto dataSetMemoryChunk = GenerateMemoryChunk(tileData, dataSetDimension, chunkDataType);
        return dataSetMemoryChunk;
    }

    auto GetDataSpace(const Dimension& dimension) -> H5::DataSpace {
        const auto dimensionIs2D = (dimension.GetDimensionality() == 2);
        const auto dimensionIs3D = (dimension.GetDimensionality() == 3);

        const auto dimensionX = static_cast<hsize_t>(dimension.X());
        const auto dimensionY = static_cast<hsize_t>(dimension.Y());
        const auto dimensionZ = static_cast<hsize_t>(dimension.Z());

        H5::DataSpace dataSpace;
        if (dimensionIs2D) {
            const size_t rank = 2;
            const hsize_t dims[rank] = { dimensionY, dimensionX };
            dataSpace = H5::DataSpace(rank, dims);
        } else if (dimensionIs3D) {
            const size_t rank = 3;
            const hsize_t dims[rank] = { dimensionZ, dimensionY, dimensionX };
            dataSpace = H5::DataSpace(rank, dims);
        }

        return dataSpace;
    }

    auto GetHDF5StringDataType(const std::string& string) -> H5::StrType {
        const auto strDataType = H5::StrType(H5::PredType::C_S1, string.size());
        strDataType.setStrpad(H5T_STR_NULLPAD);
        return strDataType;
    }

    auto SimpleDataSpace() -> H5::DataSpace {
        const auto rank = 1;
        const hsize_t dims[1] = { 1 };
        return H5::DataSpace(rank, dims);
    }

    auto CreateAndWriteAttributeInt64(const H5::H5Object& object, const std::string& attributeName,
        const int64_t& value) -> void {
        auto attribute = object.createAttribute(attributeName, H5::PredType::NATIVE_INT64, SimpleDataSpace());
        attribute.write(H5::PredType::NATIVE_INT64, &value);
        attribute.close();
    }

    auto CreateAndWriteAttributeDouble(const H5::H5Object& object, const std::string& attributeName,
        const double& value) -> void {
        auto attribute = object.createAttribute(attributeName, H5::PredType::NATIVE_DOUBLE, SimpleDataSpace());
        attribute.write(H5::PredType::NATIVE_DOUBLE, &value);
        attribute.close();
    }

    auto CreateAndWriteAttributeString(const H5::H5Object& object, const std::string& attributeName,
        const std::string& string) -> void {
        auto strDataType = GetHDF5StringDataType(string);
        auto attribute = object.createAttribute(attributeName, strDataType, SimpleDataSpace());
        attribute.write(strDataType, string);
        strDataType.close();
        attribute.close();
    }

    auto ReadAttributeInt64(const H5::H5Object& object, const std::string& attributeName) -> int64_t {
        auto readingAttribute = object.openAttribute(attributeName);
        int64_t attributeValue{};
        readingAttribute.read(H5::PredType::NATIVE_INT64, &attributeValue);
        readingAttribute.close();
        return attributeValue;
    }

    auto ReadAttributeDouble(const H5::H5Object& object, const std::string& attributeName) -> double {
        auto readingAttribute = object.openAttribute(attributeName);
        double attributeValue{};
        readingAttribute.read(H5::PredType::NATIVE_DOUBLE, &attributeValue);
        readingAttribute.close();
        return attributeValue;
    }

    auto ReadAttributeString(const H5::H5Object& object, const std::string& attributeName) -> std::string {
        auto readingAttribute = object.openAttribute(attributeName);
        std::string attributeValue;
        readingAttribute.read(readingAttribute.getDataType(), attributeValue);
        readingAttribute.close();
        return attributeValue;
    }

    auto CopyAttribute(const H5::Attribute& srcAttribute, const H5::H5Object& destObject) -> void { //TODO Implement Test
        std::string attributeName{};
        srcAttribute.getName(attributeName);

        const auto dataSizeOfAttribute = srcAttribute.getInMemDataSize();
        const auto srcAttributeData = new int8_t[dataSizeOfAttribute]();

        auto dataType = srcAttribute.getDataType();

        srcAttribute.read(dataType, srcAttributeData);

        auto dataSpace = srcAttribute.getSpace();

        auto destAttribute = destObject.createAttribute(attributeName, dataType, dataSpace);
        destAttribute.write(dataType, srcAttributeData);
        delete[] srcAttributeData;

        dataType.close();
        dataSpace.close();
        destAttribute.close();
    }

    auto CopyAllAttributes(const H5::H5Object& srcObject, const H5::H5Object& destObject) -> void { //TODO Implement Test
        const auto numberOfAttributes = srcObject.getNumAttrs();
        for (hsize_t i = 0; i < numberOfAttributes; ++i) {
            auto attribute = srcObject.openAttribute(static_cast<uint32_t>(i));

            CopyAttribute(attribute, destObject);
            attribute.close();
        }
    }

    auto GetGroupName(const H5::Group& group) -> std::string {
        std::string objName;
        group.getObjName(objName);

        const auto objectName = QString::fromStdString(objName);
        const auto objectSplitList = objectName.split("/");

        return objectSplitList.last().toStdString();
    }

    auto CopyGroupOnly(const H5::Group& srcGroup, const H5::Group& destPositionGroup)->void {
        const auto groupName = GetGroupName(srcGroup);

        auto copiedGroup = destPositionGroup.createGroup(groupName);
        copiedGroup.close();
    }
}
