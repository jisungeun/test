#include "Hdf5DataSetRoiReader.h"

using namespace TC::IO;

namespace TC::HDF5Utilities {
    struct Hdf5DataSetRoiReader::Impl {
        Impl() = default;
        ~Impl() {
            dataSetSpace.close();
            dataType.close();
        };

        H5::DataSet* dataSet = nullptr;
        H5::DataSpace dataSetSpace{};
        H5::DataType dataType{};
    };

    Hdf5DataSetRoiReader::Hdf5DataSetRoiReader(H5::DataSet& dataSet)
        : d(new Impl()) {
        d->dataSet = &dataSet;
        d->dataSetSpace = dataSet.getSpace();
        d->dataType = dataSet.getDataType();
    }

    Hdf5DataSetRoiReader::~Hdf5DataSetRoiReader() = default;

    auto Hdf5DataSetRoiReader::ReadAll(H5::DataSet& dataSet) -> MemoryChunk::Pointer {
        MemoryChunk::Pointer resultMemoryChunk;
        Hdf5DataSetRoiReader hdf5DataSetRoiReader(dataSet);
        auto srcDataSpace = dataSet.getSpace();

        const auto numberOfSrcDataDimension = srcDataSpace.getSimpleExtentNdims();
        const auto dataIs2D = numberOfSrcDataDimension == 2;
        const auto dataIs3D = numberOfSrcDataDimension == 3;
        DataRange allDataRange;
        if (dataIs2D) {
            const auto rank = 2;
            hsize_t dimension[rank]{ 0,0 };
            srcDataSpace.getSimpleExtentDims(dimension);

            const auto dimensionX = static_cast<size_t>(dimension[1]);
            const auto dimensionY = static_cast<size_t>(dimension[0]);

            allDataRange = DataRange(Point(0, 0), Count(dimensionX, dimensionY));
        } else if (dataIs3D) {
            const auto rank = 3;
            hsize_t dimension[rank]{ 0,0,0 };
            srcDataSpace.getSimpleExtentDims(dimension);

            const auto dimensionX = static_cast<size_t>(dimension[2]);
            const auto dimensionY = static_cast<size_t>(dimension[1]);
            const auto dimensionZ = static_cast<size_t>(dimension[0]);

            allDataRange = DataRange(Point(0, 0, 0), Count(dimensionX, dimensionY, dimensionZ));
        }

        resultMemoryChunk = hdf5DataSetRoiReader.Read(allDataRange);
        srcDataSpace.close();
        return resultMemoryChunk;
    }

    auto Hdf5DataSetRoiReader::Read(const DataRange& dataRange) -> MemoryChunk::Pointer {        
        MemoryChunk::Pointer resultMemoryChunk{ nullptr };
        if (CheckDataRangeIsValid(dataRange)) {
            auto data = AllocateData(dataRange.GetCount());
            ReadRoiMemory(dataRange, data);
            resultMemoryChunk = GenerateMemoryChunkData(data, dataRange.GetCount());
        }
        return resultMemoryChunk;
    }

    auto Hdf5DataSetRoiReader::CheckDataRangeIsValid(const DataRange& dataRange) const -> bool {
        auto validity = true;
        if (!CheckDimensionality(dataRange)) {
            validity = false;
        }
        if (!CheckOffsetPoint(dataRange.GetOffsetPoint())) {
            validity = false;
        }
        if (!CheckLastPoint(dataRange.GetLastPoint())) {
            validity = false;
        }
        return validity;
    }

    auto Hdf5DataSetRoiReader::CheckDimensionality(const DataRange& dataRange) const ->bool {
        const auto dataRangeDimensionality = dataRange.GetDimensionality();
        const auto dataSetRank = d->dataSetSpace.getSimpleExtentNdims();
        if (dataSetRank != dataRangeDimensionality) {
            return false;
        }

        return true;
    }

    auto Hdf5DataSetRoiReader::CheckOffsetPoint(const Point& offsetPoint) const -> bool {
        size_t dataSpaceDimensionX{ 0 }, dataSpaceDimensionY{ 0 }, dataSpaceDimensionZ{ 0 };
        if (offsetPoint.GetDimensionality() == 3) {
            hsize_t dataSpaceDimension[3]{ 0,0,0 };
            d->dataSetSpace.getSimpleExtentDims(dataSpaceDimension);

            dataSpaceDimensionX = static_cast<size_t>(dataSpaceDimension[2]);
            dataSpaceDimensionY = static_cast<size_t>(dataSpaceDimension[1]);
            dataSpaceDimensionZ = static_cast<size_t>(dataSpaceDimension[0]);
        } else if (offsetPoint.GetDimensionality() == 2) {
            hsize_t dataSpaceDimension[2]{ 0,0 };
            d->dataSetSpace.getSimpleExtentDims(dataSpaceDimension);

            dataSpaceDimensionX = static_cast<size_t>(dataSpaceDimension[1]);
            dataSpaceDimensionY = static_cast<size_t>(dataSpaceDimension[0]);
        }

        const auto outOfRangeX = (offsetPoint.X() >= static_cast<int64_t>(dataSpaceDimensionX));
        const auto outOfRangeY = (offsetPoint.Y() >= static_cast<int64_t>(dataSpaceDimensionY));

        if (outOfRangeX || outOfRangeY) {
            return false;
        }

        if (offsetPoint.GetDimensionality() == 3) {
            const auto outOfRangeZ = (offsetPoint.Z() >= static_cast<int64_t>(dataSpaceDimensionZ));
            if (outOfRangeZ) {
                return false;
            }
        }
        return true;
    }

    auto Hdf5DataSetRoiReader::CheckLastPoint(const Point& lastPoint) const -> bool {
        size_t dataSpaceDimensionX{ 0 }, dataSpaceDimensionY{ 0 }, dataSpaceDimensionZ{ 0 };
        if (lastPoint.GetDimensionality() == 3) {
            hsize_t dataSpaceDimension[3]{ 0,0,0 };
            d->dataSetSpace.getSimpleExtentDims(dataSpaceDimension);

            dataSpaceDimensionX = static_cast<size_t>(dataSpaceDimension[2]);
            dataSpaceDimensionY = static_cast<size_t>(dataSpaceDimension[1]);
            dataSpaceDimensionZ = static_cast<size_t>(dataSpaceDimension[0]);
        } else if (lastPoint.GetDimensionality() == 2) {
            hsize_t dataSpaceDimension[2]{ 0,0 };
            d->dataSetSpace.getSimpleExtentDims(dataSpaceDimension);

            dataSpaceDimensionX = static_cast<size_t>(dataSpaceDimension[1]);
            dataSpaceDimensionY = static_cast<size_t>(dataSpaceDimension[0]);
        }

        const auto outOfRangeX = (lastPoint.X() >= static_cast<int64_t>(dataSpaceDimensionX));
        const auto outOfRangeY = (lastPoint.Y() >= static_cast<int64_t>(dataSpaceDimensionY));

        if (outOfRangeX || outOfRangeY) {
            return false;
        }

        if (lastPoint.GetDimensionality() == 3) {
            const auto outOfRangeZ = (lastPoint.Z() >= static_cast<int64_t>(dataSpaceDimensionZ));
            if (outOfRangeZ) {
                return false;
            }
        }

        return true;
    }

    auto Hdf5DataSetRoiReader::ReadRoiMemory(const DataRange& dataRange, const std::any& data) const -> void {
        auto selectedSpace = GetSelectedSpace(dataRange);
        auto outSpace = GetOutputSpace(dataRange.GetCount());
        ReadRoiMemoryFromDataSet(outSpace, selectedSpace, data);
        selectedSpace.close();
        outSpace.close();
    }

    auto Hdf5DataSetRoiReader::AllocateData(const Count& count) const -> std::any {
        const auto countElements = count.GetTotalNumberOfCount();
        return AllocateData(countElements);
    }

    auto Hdf5DataSetRoiReader::AllocateData(const size_t& countElements) const -> std::any {
        std::any data;
        if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT8)) {
            data = std::shared_ptr<int8_t[]>(new int8_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT8)) {
            data = std::shared_ptr<uint8_t[]>(new uint8_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT16)) {
            data = std::shared_ptr<int16_t[]>(new int16_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT16)) {
            data = std::shared_ptr<uint16_t[]>(new uint16_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT32)) {
            data = std::shared_ptr<int32_t[]>(new int32_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT32)) {
            data = std::shared_ptr<uint32_t[]>(new uint32_t[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_FLOAT)) {
            data = std::shared_ptr<float[]>(new float[countElements]());

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_DOUBLE)) {
            data = std::shared_ptr<double[]>(new double[countElements]());

        } else {
        }
        return data;
    }

    auto Hdf5DataSetRoiReader::GetSelectedSpace(const DataRange& selectedDataRange) const
        -> H5::DataSpace {
        H5::DataSpace selectedSpace;
        const auto offsetX = static_cast<hsize_t>(selectedDataRange.GetOffsetPoint().X());
        const auto offsetY = static_cast<hsize_t>(selectedDataRange.GetOffsetPoint().Y());
        const auto offsetZ = static_cast<hsize_t>(selectedDataRange.GetOffsetPoint().Z());

        const auto countX = static_cast<hsize_t>(selectedDataRange.GetCount().X());
        const auto countY = static_cast<hsize_t>(selectedDataRange.GetCount().Y());
        const auto countZ = static_cast<hsize_t>(selectedDataRange.GetCount().Z());

        if (selectedDataRange.GetDimensionality() == 2) {
            hsize_t h5Count[2]{ countY, countX };
            hsize_t h5Offset[2]{ offsetY, offsetX };

            selectedSpace = d->dataSetSpace;
            selectedSpace.selectHyperslab(H5S_SELECT_SET, h5Count, h5Offset);
        } else if (selectedDataRange.GetDimensionality() == 3) {
            hsize_t h5Count[3]{ countZ, countY,countX };
            hsize_t h5Offset[3]{ offsetZ, offsetY, offsetX };

            selectedSpace = d->dataSetSpace;
            selectedSpace.selectHyperslab(H5S_SELECT_SET, h5Count, h5Offset);
        }

        return selectedSpace;
    }

    auto Hdf5DataSetRoiReader::GetOutputSpace(const Count& count) -> H5::DataSpace {
        H5::DataSpace outputSpace;

        const auto countX = static_cast<hsize_t>(count.X());
        const auto countY = static_cast<hsize_t>(count.Y());
        const auto countZ = static_cast<hsize_t>(count.Z());

        if (count.GetDimensionality() == 2) {
            hsize_t h5Count[2]{ countY, countX };
            outputSpace = H5::DataSpace(2, h5Count);
        } else if (count.GetDimensionality() == 3) {
            hsize_t h5Count[3]{ countZ, countY, countX };
            outputSpace = H5::DataSpace(3, h5Count);
        }

        return outputSpace;
    }

    auto Hdf5DataSetRoiReader::ReadRoiMemoryFromDataSet(const H5::DataSpace& outSpace,
        const H5::DataSpace& selectedSpace, const std::any& data) const -> void {
        const auto dataPointer = GetDataPointer(data);
        d->dataSet->read(dataPointer, d->dataSet->getDataType(), outSpace, selectedSpace);
    }

    auto Hdf5DataSetRoiReader::GetDataPointer(const std::any& data) const -> void* {
        void* dataPointer;
        if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT8)) {
            dataPointer = std::any_cast<std::shared_ptr<int8_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT8)) {
            dataPointer = std::any_cast<std::shared_ptr<uint8_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT16)) {
            dataPointer = std::any_cast<std::shared_ptr<int16_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT16)) {
            dataPointer = std::any_cast<std::shared_ptr<uint16_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT32)) {
            dataPointer = std::any_cast<std::shared_ptr<int32_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT32)) {
            dataPointer = std::any_cast<std::shared_ptr<uint32_t[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_FLOAT)) {
            dataPointer = std::any_cast<std::shared_ptr<float[]>>(data).get();

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_DOUBLE)) {
            dataPointer = std::any_cast<std::shared_ptr<double[]>>(data).get();
        } else {
            dataPointer = nullptr;
        }

        return dataPointer;
    }

    auto Hdf5DataSetRoiReader::GenerateMemoryChunkData(const std::any& data, const Count& count) const
        -> MemoryChunk::Pointer {
        Dimension dimension;
        if (count.GetDimensionality() == 2) {
            dimension = Dimension(count.X(), count.Y());
        } else if (count.GetDimensionality() == 3) {
            dimension = Dimension(count.X(), count.Y(), count.Z());
        }

        MemoryChunk::Pointer memoryChunk(new MemoryChunk);
        if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT8)) {
            const auto rawData = std::any_cast<std::shared_ptr<int8_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT8)) {
            const auto rawData = std::any_cast<std::shared_ptr<uint8_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT16)) {
            const auto rawData = std::any_cast<std::shared_ptr<int16_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT16)) {
            const auto rawData = std::any_cast<std::shared_ptr<uint16_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_INT32)) {
            const auto rawData = std::any_cast<std::shared_ptr<int32_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_UINT32)) {
            const auto rawData = std::any_cast<std::shared_ptr<uint32_t[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_FLOAT)) {
            const auto rawData = std::any_cast<std::shared_ptr<float[]>>(data);
            memoryChunk->SetData(rawData, dimension);

        } else if (d->dataType == H5::DataType(H5::PredType::NATIVE_DOUBLE)) {
            const auto rawData = std::any_cast<std::shared_ptr<double[]>>(data);
            memoryChunk->SetData(rawData, dimension);
        }

        return memoryChunk;
    }
}
