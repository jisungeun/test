#include "Hdf5DataSetPathScanner.h"

namespace TC::HDF5Utilities {
    struct Hdf5DataSetPathScanner::Impl {
        Impl() = default;
        ~Impl() = default;

        QStringList dataSetPathList{};
    };

    Hdf5DataSetPathScanner::Hdf5DataSetPathScanner()
        : d(new Impl()) {
    }

    Hdf5DataSetPathScanner::~Hdf5DataSetPathScanner() = default;

    auto Hdf5DataSetPathScanner::Scan(const H5::H5File& srcFile) -> QStringList {
        ScanRecursively(srcFile);
        return d->dataSetPathList;
    }

    auto Hdf5DataSetPathScanner::ScanRecursively(const H5::Group& srcGroup) -> void {
        const auto numberOfObject = srcGroup.getNumObjs();
        for (hsize_t i = 0; i < numberOfObject; ++i) {
            const auto objectIsGroup = (srcGroup.getObjTypeByIdx(i) == H5G_GROUP);
            const auto objectIsDataSet = (srcGroup.getObjTypeByIdx(i) == H5G_DATASET);

            if (objectIsGroup) {
                const auto innerGroupName = srcGroup.getObjnameByIdx(i);
                auto srcInnerGroup = srcGroup.openGroup(innerGroupName);

                ScanRecursively(srcInnerGroup);
                srcInnerGroup.close();
            } else if (objectIsDataSet) {

                const auto groupPath = QString::fromStdString(srcGroup.getObjName());
                const auto dataSetPath = QString::fromStdString(srcGroup.getObjnameByIdx(i));
                d->dataSetPathList.push_back(groupPath + "/" + dataSetPath);
            }
        }
    }
}
