#include <catch2/catch.hpp>

#include "Hdf5Utilities.h"
#include "CommonUtilities.h"

using namespace TC::IO;
using namespace TC::HDF5Utilities;

namespace Hdf5UtilitiesTest {
    auto CheckMemoryChunk(const MemoryChunk::Pointer& memoryChunk, const ChunkDataType& ansChunkDataType,
        const Dimension& ansDimension) -> bool {
        const auto dataTypeSame = memoryChunk->GetDataType() == +ansChunkDataType;
        const auto dimensionSame = memoryChunk->GetDimension() == ansDimension;
        return dataTypeSame && dimensionSame;
    }

    TEST_CASE("Hdf5Utilities") {
        SECTION("GetDimension(dataSet)") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto dataSpace = GetDataSpace(Dimension(1, 2, 3));
            auto dataSet = file.createDataSet("dataSet", H5::PredType::NATIVE_UINT16, dataSpace);
            dataSpace.close();

            const auto dimensionCheck = (GetDimension(dataSet) == Dimension(1, 2, 3));
            CHECK(dimensionCheck);
            file.close();
        }

        SECTION("GetDimension(dataSpace)") {
            auto dataSpace = GetDataSpace(Dimension(1, 2, 3));
            const auto dimensionCheck = (GetDimension(dataSpace) == Dimension(1, 2, 3));
            dataSpace.close();
            CHECK(dimensionCheck);
        }

        SECTION("GetChunkDataType()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto dataSpace = GetDataSpace(Dimension(1, 2, 3));
            auto int8DataSet = file.createDataSet("int8", H5::PredType::NATIVE_INT8, dataSpace);
            auto uint8DataSet = file.createDataSet("uint8", H5::PredType::NATIVE_UINT8, dataSpace);
            auto int16DataSet = file.createDataSet("int16", H5::PredType::NATIVE_INT16, dataSpace);
            auto uint16DataSet = file.createDataSet("uint16", H5::PredType::NATIVE_UINT16, dataSpace);
            auto int32DataSet = file.createDataSet("int32", H5::PredType::NATIVE_INT32, dataSpace);
            auto uint32DataSet = file.createDataSet("uint32", H5::PredType::NATIVE_UINT32, dataSpace);
            auto floatDataSet = file.createDataSet("float", H5::PredType::NATIVE_FLOAT, dataSpace);
            auto doubleDataSet = file.createDataSet("double", H5::PredType::NATIVE_DOUBLE, dataSpace);

            CHECK(GetChunkDataType(int8DataSet) == +ChunkDataType::Int8Type);
            CHECK(GetChunkDataType(uint8DataSet) == +ChunkDataType::UInt8Type);
            CHECK(GetChunkDataType(int16DataSet) == +ChunkDataType::Int16Type);
            CHECK(GetChunkDataType(uint16DataSet) == +ChunkDataType::UInt16Type);
            CHECK(GetChunkDataType(int32DataSet) == +ChunkDataType::Int32Type);
            CHECK(GetChunkDataType(uint32DataSet) == +ChunkDataType::UInt32Type);
            CHECK(GetChunkDataType(floatDataSet) == +ChunkDataType::FloatType);
            CHECK(GetChunkDataType(doubleDataSet) == +ChunkDataType::DoubleType);

            dataSpace.close();
            int8DataSet.close();
            uint8DataSet.close();
            int16DataSet.close();
            uint16DataSet.close();
            int32DataSet.close();
            uint32DataSet.close();
            floatDataSet.close();
            doubleDataSet.close();
            file.close();
        }

        SECTION("ToChunkDataType()") {
            CHECK(ToChunkDataType(H5::PredType::NATIVE_INT8) == +ChunkDataType::Int8Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_UINT8) == +ChunkDataType::UInt8Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_INT16) == +ChunkDataType::Int16Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_UINT16) == +ChunkDataType::UInt16Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_INT32) == +ChunkDataType::Int32Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_UINT32) == +ChunkDataType::UInt32Type);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_FLOAT) == +ChunkDataType::FloatType);
            CHECK(ToChunkDataType(H5::PredType::NATIVE_DOUBLE) == +ChunkDataType::DoubleType);
        }

        SECTION("ToH5DataType()") {
            CHECK(ToH5DataType(ChunkDataType::Int8Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_INT8));
            CHECK(ToH5DataType(ChunkDataType::UInt8Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_UINT8));
            CHECK(ToH5DataType(ChunkDataType::Int16Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_INT16));
            CHECK(ToH5DataType(ChunkDataType::UInt16Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16));
            CHECK(ToH5DataType(ChunkDataType::Int32Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_INT32));
            CHECK(ToH5DataType(ChunkDataType::UInt32Type) == static_cast<H5::DataType>(H5::PredType::NATIVE_UINT32));
            CHECK(ToH5DataType(ChunkDataType::FloatType) == static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT));
            CHECK(ToH5DataType(ChunkDataType::DoubleType) == static_cast<H5::DataType>(H5::PredType::NATIVE_DOUBLE));
        }

        SECTION("AllocateRawData(ChunkDataType)") {
            const size_t numberOfElements = 10;
            auto int8Pointer = AllocateRawData(+ChunkDataType::Int8Type, numberOfElements);
            auto uint8Pointer = AllocateRawData(+ChunkDataType::UInt8Type, numberOfElements);
            auto int16Pointer = AllocateRawData(+ChunkDataType::Int16Type, numberOfElements);
            auto uint16Pointer = AllocateRawData(+ChunkDataType::UInt16Type, numberOfElements);
            auto int32Pointer = AllocateRawData(+ChunkDataType::Int32Type, numberOfElements);
            auto uint32Pointer = AllocateRawData(+ChunkDataType::UInt32Type, numberOfElements);
            auto floatPointer = AllocateRawData(+ChunkDataType::FloatType, numberOfElements);
            auto doublePointer = AllocateRawData(+ChunkDataType::DoubleType, numberOfElements);

            CHECK(int8Pointer != nullptr);
            CHECK(uint8Pointer != nullptr);
            CHECK(int16Pointer != nullptr);
            CHECK(uint16Pointer != nullptr);
            CHECK(int32Pointer != nullptr);
            CHECK(uint32Pointer != nullptr);
            CHECK(floatPointer != nullptr);
            CHECK(doublePointer != nullptr);

            delete[] static_cast<int8_t*>(int8Pointer);
            delete[] static_cast<uint8_t*>(uint8Pointer);
            delete[] static_cast<int16_t*>(int16Pointer);
            delete[] static_cast<uint16_t*>(uint16Pointer);
            delete[] static_cast<int32_t*>(int32Pointer);
            delete[] static_cast<uint32_t*>(uint32Pointer);
            delete[] static_cast<float*>(floatPointer);
            delete[] static_cast<double*>(doublePointer);
        }

        SECTION("ReadDataSet()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);
            const auto dimension = Dimension(1, 2, 3);
            auto dataSpace = GetDataSpace(dimension);
            auto int8DataSet = file.createDataSet("int8", H5::PredType::NATIVE_INT8, dataSpace);
            auto uint8DataSet = file.createDataSet("uint8", H5::PredType::NATIVE_UINT8, dataSpace);
            auto int16DataSet = file.createDataSet("int16", H5::PredType::NATIVE_INT16, dataSpace);
            auto uint16DataSet = file.createDataSet("uint16", H5::PredType::NATIVE_UINT16, dataSpace);
            auto int32DataSet = file.createDataSet("int32", H5::PredType::NATIVE_INT32, dataSpace);
            auto uint32DataSet = file.createDataSet("uint32", H5::PredType::NATIVE_UINT32, dataSpace);
            auto floatDataSet = file.createDataSet("float", H5::PredType::NATIVE_FLOAT, dataSpace);
            auto doubleDataSet = file.createDataSet("double", H5::PredType::NATIVE_DOUBLE, dataSpace);

            auto int8Chunk = ReadDataSet(int8DataSet);
            auto uint8Chunk = ReadDataSet(int8DataSet);
            auto int16Chunk = ReadDataSet(int8DataSet);
            auto uint16Chunk = ReadDataSet(int8DataSet);
            auto int32Chunk = ReadDataSet(int8DataSet);
            auto uint32Chunk = ReadDataSet(int8DataSet);
            auto floatChunk = ReadDataSet(int8DataSet);
            auto doubleChunk = ReadDataSet(int8DataSet);

            CheckMemoryChunk(int8Chunk, ChunkDataType::Int8Type, dimension);
            CheckMemoryChunk(uint8Chunk, ChunkDataType::UInt8Type, dimension);
            CheckMemoryChunk(int16Chunk, ChunkDataType::Int16Type, dimension);
            CheckMemoryChunk(uint16Chunk, ChunkDataType::UInt16Type, dimension);
            CheckMemoryChunk(int32Chunk, ChunkDataType::Int32Type, dimension);
            CheckMemoryChunk(uint32Chunk, ChunkDataType::UInt32Type, dimension);
            CheckMemoryChunk(floatChunk, ChunkDataType::FloatType, dimension);
            CheckMemoryChunk(doubleChunk, ChunkDataType::DoubleType, dimension);

            dataSpace.close();
            int8DataSet.close();
            uint8DataSet.close();
            int16DataSet.close();
            uint16DataSet.close();
            int32DataSet.close();
            uint32DataSet.close();
            floatDataSet.close();
            doubleDataSet.close();
            file.close();
        }

        SECTION("GetDataSpace()") {
            auto dataSpace = GetDataSpace(Dimension(1, 2, 3));

            hsize_t dims[3] = {};
            const auto dimensionality = dataSpace.getSimpleExtentDims(dims);

            CHECK(dimensionality == 3);
            CHECK(dims[0] == 3);
            CHECK(dims[1] == 2);
            CHECK(dims[2] == 1);
        }

        SECTION("GetHDF5StringDataType()") {
            const std::string answerString = "string";
            auto hdf5StringDataType = GetHDF5StringDataType(answerString);
            CHECK(hdf5StringDataType.getSize() == answerString.size());
            CHECK(hdf5StringDataType.getStrpad() == H5T_STR_NULLPAD);
        }

        SECTION("SimpleDataSpace()") {
            auto simpleDataSpace = SimpleDataSpace();

            hsize_t dims[3] = { 0,0,0 };
            const auto rank = simpleDataSpace.getSimpleExtentDims(dims);

            CHECK(rank == 1);
            CHECK(dims[0] == 1);
            CHECK(dims[1] == 0);
            CHECK(dims[2] == 0);
        }

        SECTION("CreateAndWriteAttributeInt64()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto group = file.createGroup("testGroup");

            std::string attributeName = "testAttribute";
            int64_t answerAttributeValue = 1;
            CreateAndWriteAttributeInt64(group, attributeName, answerAttributeValue);

            const auto attributeIsCreated = (group.attrExists(attributeName) == true);
            CHECK(attributeIsCreated);

            auto resultAttribute = group.openAttribute(attributeName);

            int64_t resultAttributeValue;
            resultAttribute.read(H5::PredType::NATIVE_INT64, &resultAttributeValue);
            resultAttribute.close();

            const auto attributeValueIsRight = (resultAttributeValue == answerAttributeValue);
            CHECK(attributeValueIsRight);

            group.close();
            file.close();
        }

        SECTION("CreateAndWriteAttributeDouble()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto group = file.createGroup("testGroup");

            std::string attributeName = "testAttribute";
            double answerAttributeValue = 1;
            CreateAndWriteAttributeDouble(group, attributeName, answerAttributeValue);

            const auto attributeIsCreated = (group.attrExists(attributeName) == true);
            CHECK(attributeIsCreated);

            auto resultAttribute = group.openAttribute(attributeName);

            double resultAttributeValue;
            resultAttribute.read(H5::PredType::NATIVE_DOUBLE, &resultAttributeValue);
            resultAttribute.close();

            const auto attributeValueIsRight = (resultAttributeValue == answerAttributeValue);
            CHECK(attributeValueIsRight);

            group.close();
            file.close();
        }

        SECTION("CreateAndWriteAttributeString()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto group = file.createGroup("testGroup");

            std::string attributeName = "testAttribute";
            std::string answerAttributeValue = "Attribute";
            CreateAndWriteAttributeString(group, attributeName, answerAttributeValue);

            const auto attributeIsCreated = (group.attrExists(attributeName) == true);
            CHECK(attributeIsCreated);

            auto resultAttribute = group.openAttribute(attributeName);

            auto strDataType = H5::StrType(H5::PredType::C_S1, answerAttributeValue.size());
            strDataType.setStrpad(H5T_STR_NULLPAD);

            std::string resultAttributeValue;
            resultAttribute.read(strDataType, resultAttributeValue);
            resultAttribute.close();

            const auto attributeValueIsRight = (resultAttributeValue == answerAttributeValue);
            CHECK(attributeValueIsRight);

            group.close();
            file.close();
        }

        SECTION("CopyAttribute()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            const std::string int64AttributeName = "int64Attribute";
            const std::string doubleAttributeName = "doubleAttribute";
            const std::string stringAttributeName = "stringAttribute";

            const int64_t int64AttributeValue = 1;
            const double doubleAttributeValue = 2;
            const std::string stringAttributeValue = "abc";

            CreateAndWriteAttributeInt64(file, int64AttributeName, int64AttributeValue);
            CreateAndWriteAttributeDouble(file, doubleAttributeName, doubleAttributeValue);
            CreateAndWriteAttributeString(file, stringAttributeName, stringAttributeValue);

            auto int64Attribute = file.openAttribute(int64AttributeName);
            auto doubleAttribute = file.openAttribute(doubleAttributeName);
            auto stringAttribute = file.openAttribute(stringAttributeName);

            auto group = file.createGroup("Group");
            CopyAttribute(int64Attribute, group);
            CopyAttribute(doubleAttribute, group);
            CopyAttribute(stringAttribute, group);

            int64Attribute.close();
            doubleAttribute.close();
            stringAttribute.close();

            CHECK(ReadAttributeInt64(group, int64AttributeName) == int64AttributeValue);
            CHECK(ReadAttributeDouble(group, doubleAttributeName) == doubleAttributeValue);
            CHECK(ReadAttributeString(group, stringAttributeName) == stringAttributeValue);

            group.close();
            file.close();
        }

        SECTION("CopyAllAttributes()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            const std::string int64AttributeName = "int64Attribute";
            const std::string doubleAttributeName = "doubleAttribute";
            const std::string stringAttributeName = "stringAttribute";

            const int64_t int64AttributeValue = 1;
            const double doubleAttributeValue = 2;
            const std::string stringAttributeValue = "abc";

            CreateAndWriteAttributeInt64(file, int64AttributeName, int64AttributeValue);
            CreateAndWriteAttributeDouble(file, doubleAttributeName, doubleAttributeValue);
            CreateAndWriteAttributeString(file, stringAttributeName, stringAttributeValue);

            auto group = file.createGroup("Group");
            CopyAllAttributes(file, group);

            CHECK(ReadAttributeInt64(group, int64AttributeName) == int64AttributeValue);
            CHECK(ReadAttributeDouble(group, doubleAttributeName) == doubleAttributeValue);
            CHECK(ReadAttributeString(group, stringAttributeName) == stringAttributeValue);

            group.close();
            file.close();
        }

        SECTION("GetGroupName()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto srcGroups = file.createGroup("srcGroups");
            auto group1 = srcGroups.createGroup("group1");
            auto group2 = srcGroups.createGroup("group2");
            auto group3 = srcGroups.createGroup("group3");
            auto group4 = srcGroups.createGroup("group4");

            CHECK(GetGroupName(group1) == "group1");
            CHECK(GetGroupName(group2) == "group2");
            CHECK(GetGroupName(group3) == "group3");
            CHECK(GetGroupName(group4) == "group4");

            group1.close();
            group2.close();
            group3.close();
            group4.close();

            file.close();
        }

        SECTION("CopyGroupOnly()") {
            H5::H5File file("Hdf5UtilitiesTest.h5", H5F_ACC_TRUNC);

            auto srcGroups = file.createGroup("srcGroups");
            auto group1 = srcGroups.createGroup("group1");
            auto group2 = srcGroups.createGroup("group2");
            auto group3 = srcGroups.createGroup("group3");
            auto group4 = srcGroups.createGroup("group4");

            auto destGroups = file.createGroup("destGroups");

            SECTION("Copy()") {
                CopyGroupOnly(srcGroups, destGroups);
                CopyGroupOnly(group1, destGroups);
                CopyGroupOnly(group2, destGroups);
                CopyGroupOnly(group3, destGroups);
                CopyGroupOnly(group4, destGroups);

                CHECK(destGroups.getNumObjs() == 5);
                CHECK(destGroups.exists("srcGroups"));
                CHECK(destGroups.exists("group1"));
                CHECK(destGroups.exists("group2"));
                CHECK(destGroups.exists("group3"));
                CHECK(destGroups.exists("group4"));
            }

            group1.close();
            group2.close();
            group3.close();
            group4.close();

            srcGroups.close();
            destGroups.close();
            file.close();
        }

    }
}