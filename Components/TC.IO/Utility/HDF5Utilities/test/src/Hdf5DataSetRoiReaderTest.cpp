#include <catch2/catch.hpp>

#include <random>

#include "Hdf5DataSetRoiReader.h"

#include "H5Cpp.h"

#include "MemoryCheckUtility.h"

using namespace TC::HDF5Utilities;
using namespace TC::IO;

namespace Hdf5DataSetRoiReaderTest {
    const size_t sizeX = 100;
    const size_t sizeY = 110;
    const size_t sizeZ = 120;

    auto GenerateRandomData3D() -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;

        std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]);

        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto GenerateRandomData2D() -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY;

        std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]);

        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto Sample3D(uint16_t* srcData, const DataRange& dataRange) -> std::shared_ptr<uint16_t[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto sampleSizeZ = dataRange.GetCount().Z();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY * sampleSizeZ;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();
        const auto offsetZ = dataRange.GetOffsetPoint().Z();

        std::shared_ptr<uint16_t[]> sampledData(new uint16_t[numberOfSampleElements]());

        for (auto k = 0; k < sampleSizeZ; ++k) {
            for (auto j = 0; j < sampleSizeY; ++j) {
                for (auto i = 0; i < sampleSizeX; ++i) {
                    const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX + (offsetZ + k) * sizeX * sizeY;
                    const auto sampleIndex = i + j * sampleSizeX + k * sampleSizeX * sampleSizeY;
                    sampledData[sampleIndex] = srcData[srcIndex];
                }
            }
        }
        return sampledData;
    }

    auto Sample2D(uint16_t* srcData, const DataRange& dataRange) -> std::shared_ptr<uint16_t[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();

        std::shared_ptr<uint16_t[]> sampledData(new uint16_t[numberOfSampleElements]());

        for (auto j = 0; j < sampleSizeY; ++j) {
            for (auto i = 0; i < sampleSizeX; ++i) {
                const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX;
                const auto sampleIndex = i + j * sampleSizeX;
                sampledData[sampleIndex] = srcData[srcIndex];
            }
        }
        return sampledData;
    }

    auto CompareData(uint16_t* data, uint16_t* answerData, const size_t& numberOfElements) ->bool {
        auto sameData{ true };
        for(auto i = 0 ; i< numberOfElements; ++i) {
            if(data[i] != answerData[i]) {
                sameData = false;
                break;
            }
        }
        return sameData;
    }

    TEST_CASE("Hdf5DataSetRoiReader") {
        const std::string fileName = "Hdf5DataSetRoiReaderTest.h5";
        H5::H5File file(fileName, H5F_ACC_TRUNC);

        hsize_t dims2D[2] = { sizeY, sizeX };
        hsize_t dims3D[3] = { sizeZ, sizeY, sizeX };
        H5::DataSpace dataSpace2D(2, dims2D);
        H5::DataSpace dataSpace3D(3, dims3D);

        auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);

        auto dataSet2D = file.createDataSet("2D", dataType, dataSpace2D);
        auto dataSet3D = file.createDataSet("3D", dataType, dataSpace3D);

        const auto data2D = GenerateRandomData2D();
        const auto data3D = GenerateRandomData3D();

        dataSet2D.write(data2D.get(), dataType);
        dataSet3D.write(data3D.get(), dataType);

        dataSet2D.close();
        dataSet3D.close();
        dataType.close();
        dataSpace2D.close();
        dataSpace3D.close();
        file.close();

        H5::H5File readingFile(fileName, H5F_ACC_RDONLY);
        auto readingDataSet2D = readingFile.openDataSet("2D");
        auto readingDataSet3D = readingFile.openDataSet("3D");

        SECTION("Read() 2D") {
            Hdf5DataSetRoiReader hdf5DataSetRoiReader(readingDataSet2D);
            DataRange dataRange(Point(10, 11), Count(20, 21));
            const auto memoryChunk = hdf5DataSetRoiReader.Read(dataRange);
            auto uint16MemoryChunk = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());

            auto answerData = Sample2D(data2D.get(), dataRange);

            CHECK(CompareData(uint16MemoryChunk.get(), answerData.get(), 20 * 21));
        }

        SECTION("Read() 3D") {
            Hdf5DataSetRoiReader hdf5DataSetRoiReader(readingDataSet3D);
            DataRange dataRange(Point(10, 11, 12), Count(20, 21, 22));
            const auto memoryChunk = hdf5DataSetRoiReader.Read(dataRange);
            auto uint16MemoryChunk = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());

            auto answerData = Sample3D(data3D.get(), dataRange);

            CHECK(CompareData(uint16MemoryChunk.get(), answerData.get(), 20 * 21 * 22));
        }

        SECTION("ReadAll() 2D") {
            const auto memoryChunk = Hdf5DataSetRoiReader::ReadAll(readingDataSet2D);
            auto uint16MemoryChunk = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());

            CHECK(CompareData(uint16MemoryChunk.get(), data2D.get(), sizeX*sizeY));
        }

        SECTION("ReadAll() 3D") {
            const auto memoryChunk = Hdf5DataSetRoiReader::ReadAll(readingDataSet3D);
            auto uint16MemoryChunk = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());

            CHECK(CompareData(uint16MemoryChunk.get(), data3D.get(), sizeX * sizeY));
        }

        readingDataSet2D.close();
        readingDataSet3D.close();
        readingFile.close();
    }

    TEST_CASE("Hdf5DataSetRoiReader memory leak") {
        const std::string fileName = "Hdf5DataSetRoiReaderTest.h5";
        {
            H5::H5File file(fileName, H5F_ACC_TRUNC);

            const size_t dimX = 1024 / 2;
            const size_t dimY = 1024 / 2;
            const size_t dimZ = 1024;
            hsize_t dims3D[3] = { dimZ, dimY, dimX };
            H5::DataSpace dataSpace3D(3, dims3D);

            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT8);
            auto dataSet3D = file.createDataSet("3D", dataType, dataSpace3D);

            const auto data3D = std::shared_ptr<uint8_t[]>(new uint8_t[dimX * dimY * dimZ]());
            dataSet3D.write(data3D.get(), dataType);

            dataSet3D.close();
            dataType.close();
            dataSpace3D.close();
            file.close();
        }

        H5::H5File file(fileName, H5F_ACC_RDONLY);
        auto dataSet = file.openDataSet("3D");
        const auto before = TC::GetAvailableMemoryInBytes();
        {
            const auto beforeMemory = TC::GetAvailableMemoryInBytes();
            const auto memoryChunk = Hdf5DataSetRoiReader::ReadAll(dataSet);
            const auto afterMemory = TC::GetAvailableMemoryInBytes();
            const auto increasedMemory = std::round(TC::GetIncreasedMemoryInMB(beforeMemory, afterMemory));
            CHECK(increasedMemory > 200);
            CHECK(increasedMemory < 300);
        }
        const auto after = TC::GetAvailableMemoryInBytes();
        const auto increased = (before - after) / 1024.f / 1024.f;
        CHECK(increased < 100);

        dataSet.close();
        file.close();
    }
}
