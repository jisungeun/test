#include <catch2/catch.hpp>

#include "Hdf5DataSetPathScanner.h"
#include "H5Cpp.h"

using namespace TC::HDF5Utilities;

namespace Hdf5DataSetPathScannerTest {
    auto CreateDataSetAt(H5::Group& dataGroup, const std::string& name)->void {
        hsize_t dims[2] = { 1,1 };
        H5::DataSpace dataSpace(2, dims);

        auto dataSet = dataGroup.createDataSet(name, H5::PredType::NATIVE_UINT16, dataSpace);
        dataSet.close();
    }

    TEST_CASE("Hdf5DataSetPathScanner") {
        H5::H5File file("Hdf5DataSetPathScannerTest.h5", H5F_ACC_TRUNC);
        auto group1 = file.createGroup("group1");

        auto group1_1 = group1.createGroup("group1_1");
        CreateDataSetAt(group1_1, "dataSet1_1-1");
        auto group1_2 = group1.createGroup("group1_2");
        CreateDataSetAt(group1_2, "dataSet1_2-1");
        auto group1_3 = group1.createGroup("group1_3");
        CreateDataSetAt(group1_3, "dataSet1_3-1");

        auto group2 = file.createGroup("group2");
        CreateDataSetAt(group2, "dataSet2-1");
        CreateDataSetAt(group2, "dataSet2-2");
        CreateDataSetAt(group2, "dataSet2-3");

        group1_1.close();
        group1_2.close();
        group1_3.close();
        group1.close();
        group2.close();
        
        SECTION("Scan()") {
            Hdf5DataSetPathScanner hdf5DataSetPathScanner;
            const auto dataSetPathList = hdf5DataSetPathScanner.Scan(file);

            CHECK(dataSetPathList.size() == 6);

            const auto check0 = dataSetPathList.at(0) == "/group1/group1_1/dataSet1_1-1";
            const auto check1 = dataSetPathList.at(1) == "/group1/group1_2/dataSet1_2-1";
            const auto check2 = dataSetPathList.at(2) == "/group1/group1_3/dataSet1_3-1";
            const auto check3 = dataSetPathList.at(3) == "/group2/dataSet2-1";
            const auto check4 = dataSetPathList.at(4) == "/group2/dataSet2-2";
            const auto check5 = dataSetPathList.at(5) == "/group2/dataSet2-3";

            CHECK(check0);
            CHECK(check1);
            CHECK(check2);
            CHECK(check3);
            CHECK(check4);
            CHECK(check5);
        }
    }
}
