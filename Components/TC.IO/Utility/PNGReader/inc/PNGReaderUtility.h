#pragma once

#include <QString>
#include <QStringList>
#include "TCPNGReaderExport.h"

auto TCPNGReader_API GetPngFilePathList(const QString& folderPath)->QStringList;
