#pragma once

#include <memory>
#include <QStringList>

#include "TCPNGReaderExport.h"

class TCPNGReader_API PNGReader3DArrayFireSquare {
public:
    PNGReader3DArrayFireSquare();
    ~PNGReader3DArrayFireSquare();

    auto SetInputFilePathList(const QStringList& inputFilePathList)->void;
    auto Read()->bool;

    auto GetData()const->const std::shared_ptr<uint8_t[]>&;
    auto GetSizeX()const->const int32_t&;
    auto GetSizeY()const->const int32_t&;
    auto GetSizeZ()const->const int32_t&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};