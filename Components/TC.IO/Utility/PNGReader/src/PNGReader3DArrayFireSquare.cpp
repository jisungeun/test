#define NOMINMAX
#include "PNGReader3DArrayFireSquare.h"

#include "arrayfire.h"

struct SquareCropRange {
    af::seq xRange;
    af::seq yRange;
};

class PNGReader3DArrayFireSquare::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QStringList inputFilePathList{};

    std::shared_ptr<uint8_t[]> data{};

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};

    int32_t squareSize{};

    auto GenerateSquareCropRange()->SquareCropRange;
};

auto PNGReader3DArrayFireSquare::Impl::GenerateSquareCropRange() -> SquareCropRange {
    af::seq xRange, yRange;

    if (this->sizeX > this->sizeY) {
        const auto xStartPoint = (this->sizeX - this->sizeY) / 2;
        const auto xEndPoint = xStartPoint + this->sizeY - 1;

        xRange = af::seq(xStartPoint, xEndPoint);
        yRange = af::seq(0, this->sizeY - 1);
    } else {
        const auto yStartPoint = (this->sizeY - this->sizeX) / 2;
        const auto yEndPoint = yStartPoint + this->sizeX - 1;

        xRange = af::seq(0, this->sizeX - 1);
        yRange = af::seq(yStartPoint, yEndPoint);
    }

    return { xRange, yRange };
}

PNGReader3DArrayFireSquare::PNGReader3DArrayFireSquare() : d(new Impl()) {
}

PNGReader3DArrayFireSquare::~PNGReader3DArrayFireSquare() = default;

auto PNGReader3DArrayFireSquare::SetInputFilePathList(const QStringList& inputFilePathList) -> void {
    d->inputFilePathList = inputFilePathList;
}

auto PNGReader3DArrayFireSquare::Read() -> bool {
    if (d->inputFilePathList.isEmpty()) {
        return false;
    }

    try {
        const auto firstImageFilePath = d->inputFilePathList.first();

        const auto firstImageData = af::loadImageNative(firstImageFilePath.toStdString().c_str());
        const auto numberOfImages = d->inputFilePathList.size();

        d->sizeY = firstImageData.dims(0);
        d->sizeX = firstImageData.dims(1);
        d->sizeZ = numberOfImages;

        auto [xRange, yRange] = d->GenerateSquareCropRange();
        d->squareSize = std::min(d->sizeX, d->sizeY);

        af::array data3d = af::constant(0, d->squareSize, d->squareSize, d->sizeZ, u8);

        for (auto imageIndex = 0; imageIndex < numberOfImages; ++imageIndex) {
            const auto zSliceImageFilePath = d->inputFilePathList[imageIndex];
            const auto sliceData = af::loadImageNative(zSliceImageFilePath.toStdString().c_str());
                
            const auto& zIndex = imageIndex;
            data3d(af::span, af::span, zIndex) = sliceData(yRange, xRange);
        }
        data3d.eval();

        const auto numberOfElements = data3d.elements();
        std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };
        data3d.host(readData.get());

        d->data = readData;
    } catch (const af::exception&) {
        return false;
    }

    return true;
}

auto PNGReader3DArrayFireSquare::GetData() const -> const std::shared_ptr<uint8_t[]>& {
    return d->data;
}

auto PNGReader3DArrayFireSquare::GetSizeX() const -> const int32_t& {
    return d->squareSize;
}

auto PNGReader3DArrayFireSquare::GetSizeY() const -> const int32_t& {
    return d->squareSize;
}

auto PNGReader3DArrayFireSquare::GetSizeZ() const -> const int32_t& {
    return d->sizeZ;
}


