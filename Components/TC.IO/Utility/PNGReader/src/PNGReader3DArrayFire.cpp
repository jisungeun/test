#include "PNGReader3DArrayFire.h"

#include "arrayfire.h"

class PNGReader3DArrayFire::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QStringList inputFilePathList{};

    std::shared_ptr<uint8_t[]> data{};

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};
};

PNGReader3DArrayFire::PNGReader3DArrayFire() : d(new Impl()) {
}

PNGReader3DArrayFire::~PNGReader3DArrayFire() = default;

auto PNGReader3DArrayFire::SetInputFilePathList(const QStringList& inputFilePathList) -> void {
    d->inputFilePathList = inputFilePathList;
}

auto PNGReader3DArrayFire::Read() -> bool {
    if (d->inputFilePathList.isEmpty()) {
        return false;
    }

    try {
        const auto firstImageFilePath = d->inputFilePathList.first();

        const auto firstImageData = af::loadImageNative(firstImageFilePath.toStdString().c_str());
        const auto numberOfImages = d->inputFilePathList.size();

        d->sizeY = firstImageData.dims(0);
        d->sizeX = firstImageData.dims(1);
        d->sizeZ = numberOfImages;

        af::array data3d = af::constant(0, d->sizeY, d->sizeX, d->sizeZ, u8);

        for (auto imageIndex = 0; imageIndex < numberOfImages; ++imageIndex) {
            const auto zSliceImageFilePath = d->inputFilePathList[imageIndex];
            const auto sliceData = af::loadImageNative(zSliceImageFilePath.toStdString().c_str());
                
            const auto& zIndex = imageIndex;
            data3d(af::span, af::span, zIndex) = sliceData;
        }
        data3d.eval();

        const auto numberOfElements = data3d.elements();
        std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };
        data3d.host(readData.get());

        d->data = readData;
    } catch (const af::exception&) {
        return false;
    }

    return true;
}

auto PNGReader3DArrayFire::GetData() const -> const std::shared_ptr<uint8_t[]>& {
    return d->data;
}

auto PNGReader3DArrayFire::GetSizeX() const -> const int32_t& {
    return d->sizeX;
}

auto PNGReader3DArrayFire::GetSizeY() const -> const int32_t& {
    return d->sizeY;
}

auto PNGReader3DArrayFire::GetSizeZ() const -> const int32_t& {
    return d->sizeZ;
}


