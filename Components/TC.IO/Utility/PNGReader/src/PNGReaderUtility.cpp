#include "PNGReaderUtility.h"

#include <QDir>

auto GetPngFilePathList(const QString& folderPath) -> QStringList {
    const QDir folderDir(folderPath);
    const auto pngFileNameList = folderDir.entryList({ "*.png" }, QDir::Files, QDir::Name);

    QStringList pngFilePathList;
    for (const auto& pngFileName : pngFileNameList) {
        const auto pngFilePath = folderPath + "/" + pngFileName;
        pngFilePathList.push_back(pngFilePath);
    }

    return pngFilePathList;
}
