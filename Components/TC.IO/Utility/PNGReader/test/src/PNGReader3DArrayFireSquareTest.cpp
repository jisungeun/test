#include <catch2/catch.hpp>
#include <fstream>

#include "PNGReader3DArrayFireSquare.h"

#include "CompareArray.h"
#include "BinaryFileIO.h"

namespace PNGReader3DArrayFireSquareTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCPNGReaderTest";
    const QString imageFolderPath = testFolderPath + "/SamplePNGImage/Set2";

    constexpr auto sizeX = 256;
    constexpr auto sizeY = 256;
    constexpr auto squareSize = 200;
    constexpr auto sizeZ = 3;

    constexpr auto numberOfElements = squareSize * squareSize * sizeZ;

    TEST_CASE("PNGReader3DArrayFireSquare : unit test") {
        SECTION("PNGReader3DArrayFireSquare()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            CHECK(&pngReader3DArrayFireSquare != nullptr);
        }
        SECTION("SetInputFilePathList()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({});
            CHECK(&pngReader3DArrayFireSquare != nullptr);
        }
        SECTION("Read()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
            });

            const auto readResult = pngReader3DArrayFireSquare.Read();
            CHECK(readResult == true);
        }
        SECTION("GetData()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
                });

            pngReader3DArrayFireSquare.Read();

            const auto answerDataPath = imageFolderPath + "/imageXCropDataAnswer";

            const auto resultData = pngReader3DArrayFireSquare.GetData();
            const auto answerData = ReadFile_uint8_t(answerDataPath.toStdString(), numberOfElements);
            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));
        }
        SECTION("GetSizeX()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
                });

            pngReader3DArrayFireSquare.Read();
            CHECK(pngReader3DArrayFireSquare.GetSizeX() == squareSize);
        }
        SECTION("GetSizeY()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
                });

            pngReader3DArrayFireSquare.Read();
            CHECK(pngReader3DArrayFireSquare.GetSizeY() == squareSize);
        }
        SECTION("GetSizeZ()") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
                });

            pngReader3DArrayFireSquare.Read();
            CHECK(pngReader3DArrayFireSquare.GetSizeZ() == sizeZ);
        }
    }
    TEST_CASE("PNGReader3DArrayFireSquare : practical test") {
        SECTION("rectangular : X long") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageYCrop1.png",
                imageFolderPath + "/imageYCrop2.png",
                imageFolderPath + "/imageYCrop3.png"
                });

            const auto readResult = pngReader3DArrayFireSquare.Read();
            CHECK(readResult == true);

            const auto answerDataPath = imageFolderPath + "/imageYCropDataAnswer";

            const auto resultData = pngReader3DArrayFireSquare.GetData();
            const auto answerData = ReadFile_uint8_t(answerDataPath.toStdString(), numberOfElements);
            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));
            CHECK(pngReader3DArrayFireSquare.GetSizeX() == squareSize);
            CHECK(pngReader3DArrayFireSquare.GetSizeY() == squareSize);
            CHECK(pngReader3DArrayFireSquare.GetSizeZ() == sizeZ);
        }

        SECTION("rectangular : Y long") {
            PNGReader3DArrayFireSquare pngReader3DArrayFireSquare;
            pngReader3DArrayFireSquare.SetInputFilePathList({
                imageFolderPath + "/imageXCrop1.png",
                imageFolderPath + "/imageXCrop2.png",
                imageFolderPath + "/imageXCrop3.png"
                });

            const auto readResult = pngReader3DArrayFireSquare.Read();
            CHECK(readResult == true);

            const auto answerDataPath = imageFolderPath + "/imageXCropDataAnswer";

            const auto resultData = pngReader3DArrayFireSquare.GetData();
            const auto answerData = ReadFile_uint8_t(answerDataPath.toStdString(), numberOfElements);
            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));
            CHECK(pngReader3DArrayFireSquare.GetSizeX() == squareSize);
            CHECK(pngReader3DArrayFireSquare.GetSizeY() == squareSize);
            CHECK(pngReader3DArrayFireSquare.GetSizeZ() == sizeZ);
        }
    }
}