#include <catch2/catch.hpp>

#include "PNGReaderUtility.h"

namespace PNGReaderUtilityTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCPNGReaderTest";
    const QString imageFolderPath = testFolderPath + "/SamplePNGImage/Set1";

    TEST_CASE("PNGReaderUtility : unit test") {
        SECTION("GetPngFilePathList") {
            const auto pngFilePathList = GetPngFilePathList(imageFolderPath);
            CHECK(pngFilePathList.size() == 3);
            CHECK(pngFilePathList[0] == imageFolderPath + "/image1.png");
            CHECK(pngFilePathList[1] == imageFolderPath + "/image2.png");
            CHECK(pngFilePathList[2] == imageFolderPath + "/image3.png");
        }
    }
}