#include <catch2/catch.hpp>
#include <fstream>

#include "PNGReader3DArrayFire.h"

namespace PNGReader3DArrayFireTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCPNGReaderTest";
    const QString imageFolderPath = testFolderPath + "/SamplePNGImage/Set1";

    constexpr auto sizeX = 256;
    constexpr auto sizeY = 256;
    constexpr auto sizeZ = 3;

    constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

    auto ReadAnswer()->std::shared_ptr<uint8_t[]> {
        const auto answerFilePath = imageFolderPath + "/image3d";

        std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };
        const auto byteCount = numberOfElements;

        std::ifstream fileStream(answerFilePath.toStdString(), std::ios::binary);
        fileStream.read(reinterpret_cast<char*>(readData.get()), byteCount);

        fileStream.close();

        return readData;
    }

    auto CompareArray(const uint8_t* result, const uint8_t* answer, const size_t& dataCount)->bool {
        for (auto index = 0; index < dataCount; ++index) {
            if (result[index]!=answer[index]) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("PNGReader3DArrayFire : unit test") {
        SECTION("PNGReader3DArrayFire()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            CHECK(&pngReader3DArrayFire != nullptr);
        }
        SECTION("SetInputFilePathList()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({});
            CHECK(&pngReader3DArrayFire != nullptr);
        }
        SECTION("Read()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({
                imageFolderPath + "/image1.png",
                imageFolderPath + "/image2.png",
                imageFolderPath + "/image3.png"
            });

            const auto readResult = pngReader3DArrayFire.Read();
            CHECK(readResult == true);
        }
        SECTION("GetData()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({
                imageFolderPath + "/image1.png",
                imageFolderPath + "/image2.png",
                imageFolderPath + "/image3.png"
                });

            pngReader3DArrayFire.Read();

            const auto resultData = pngReader3DArrayFire.GetData();
            const auto answerData = ReadAnswer();
            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));
        }
        SECTION("GetSizeX()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({
                imageFolderPath + "/image1.png",
                imageFolderPath + "/image2.png",
                imageFolderPath + "/image3.png"
                });

            pngReader3DArrayFire.Read();
            CHECK(pngReader3DArrayFire.GetSizeX() == sizeX);
        }
        SECTION("GetSizeY()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({
                imageFolderPath + "/image1.png",
                imageFolderPath + "/image2.png",
                imageFolderPath + "/image3.png"
                });

            pngReader3DArrayFire.Read();
            CHECK(pngReader3DArrayFire.GetSizeY() == sizeY);
        }
        SECTION("GetSizeZ()") {
            PNGReader3DArrayFire pngReader3DArrayFire;
            pngReader3DArrayFire.SetInputFilePathList({
                imageFolderPath + "/image1.png",
                imageFolderPath + "/image2.png",
                imageFolderPath + "/image3.png"
                });

            pngReader3DArrayFire.Read();
            CHECK(pngReader3DArrayFire.GetSizeZ() == sizeZ);
        }
    }
    TEST_CASE("PNGReader3DArrayFire : practical test") {
        PNGReader3DArrayFire pngReader3DArrayFire;
        pngReader3DArrayFire.SetInputFilePathList({
            imageFolderPath + "/image1.png",
            imageFolderPath + "/image2.png",
            imageFolderPath + "/image3.png"
            });

        const auto readResult = pngReader3DArrayFire.Read();
        CHECK(readResult == true);

        const auto resultData = pngReader3DArrayFire.GetData();
        const auto answerData = ReadAnswer();
        CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements));
        CHECK(pngReader3DArrayFire.GetSizeX() == sizeX);
        CHECK(pngReader3DArrayFire.GetSizeY() == sizeY);
        CHECK(pngReader3DArrayFire.GetSizeZ() == sizeZ);
    }
}