#include "npy.hpp"

#include "PythonNumpyIO.h"

namespace TC::IO {
    struct TCNumpyReader::Impl {
        
    };
    TCNumpyReader::TCNumpyReader() : d{ new Impl } {
        
    }
    TCNumpyReader::~TCNumpyReader() {

    }

    auto TCNumpyReader::Read(std::any& arr, const QString& path, NpyArrType type) const -> std::tuple<bool,std::vector<unsigned long>> {        
        std::vector<unsigned long> shape{};
        bool fortran_order;        
        try {
            switch(type) {
            case NpyArrType::arrUBYTE:
            {
                std::vector<uint8_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrBYTE:
            {
                std::vector<int8_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrUSHORT:
            {
                std::vector<uint16_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrSHORT: 
            {
                std::vector<int16_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrUINT: 
            {
                std::vector<uint32_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrINT: 
            {
                std::vector<int32_t> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrFLOAT: 
            {
                std::vector<float> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            case NpyArrType::arrDOUBLE: 
            {
                std::vector<double> data;
                npy::LoadArrayFromNumpy(path.toStdString(), shape, fortran_order, data);
                arr = data;
            }
                break;
            }            
        }catch(std::runtime_error& e) {
            std::cout << e.what() << std::endl;
            return std::make_tuple(false, std::vector<unsigned long>());
        }

        return std::make_tuple(fortran_order, shape);                
    }

    ///////////////////////////////////////////////////////////

    struct TCNumpyWriter::Impl {
        
    };
    TCNumpyWriter::TCNumpyWriter() : d{ new Impl } {
        
    }
    TCNumpyWriter::~TCNumpyWriter() {
        
    }

    auto TCNumpyWriter::Write(std::any anyArr, const QString& path, NpyArrType type,std::vector<unsigned long> shape) -> bool {
        try {
            switch (type) {
            case NpyArrType::arrUBYTE:
            {
                auto arr = std::any_cast<std::vector<uint8_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrBYTE:
            {
                auto arr = std::any_cast<std::vector<int8_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrUSHORT:
            {
                auto arr = std::any_cast<std::vector<uint16_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrSHORT:
            {
                auto arr = std::any_cast<std::vector<int16_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrUINT:
            {
                auto arr = std::any_cast<std::vector<uint32_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrINT: 
            {
                auto arr = std::any_cast<std::vector<int32_t>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrFLOAT:
            {
                auto arr = std::any_cast<std::vector<float>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            case NpyArrType::arrDOUBLE:
            {
                auto arr = std::any_cast<std::vector<double>>(anyArr);
                npy::SaveArrayAsNumpy(path.toStdString(), false, static_cast<unsigned int>(shape.size()), shape.data(), arr);
            }
            break;
            }
        }
        catch (std::runtime_error& e) {
            std::cout << e.what() << std::endl;
            return false;
        }
        return true;
    }
}