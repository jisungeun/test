#pragma once

#include <enum.h>

#include <any>
#include <memory>
#include <tuple>

#include <QString>

#include "TCNumpyIOExport.h"

namespace TC::IO {
	BETTER_ENUM(NpyArrType, int,
		arrUSHORT = 0, arrDOUBLE = 1, arrFLOAT = 2,
		arrSHORT = 3, arrBYTE = 4, arrINT = 5,
		arrUBYTE = 6, arrUINT = 7);
	class TCNumpyIO_API TCNumpyReader {
	public:
		explicit TCNumpyReader();
		virtual ~TCNumpyReader();

		auto Read(std::any& arr,const QString& path, NpyArrType type) const->std::tuple<bool, std::vector<unsigned long>>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	class TCNumpyIO_API TCNumpyWriter {
	public:		
		explicit TCNumpyWriter();
		virtual ~TCNumpyWriter();

		auto Write(std::any anyArr, const QString& path, NpyArrType type,std::vector<unsigned long> shape)->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}