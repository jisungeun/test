#include <any>

#include <iostream>

#include <QApplication>
#include <QSettings>
#include <QFileDialog>

//Open Inventor
#include <Inventor/ViewerComponents/Qt/Viewers/ViewerExaminer.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoMaterial.h>
#include <ImageViz/SoImageViz.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Medical/helpers/MedicalHelper.h>
////////

#include <OivHdf5Reader.h>
#include <PythonNumpyIO.h>
#include <QVBoxLayout>

float color_table[20][3]{
	230,25,75, //red
	60,180,75, //green
	255,255,25, //yellow
	0,130,200, //blue
	245,130,48, //Orange
	145,30,180, //Purple
	70,240,240, //Cyan
	240,50,230, //Magenta
	210,245,60, //Lime
	250,190,212, //Pink
	0,128,128, //Teal
	220,190,255, //Lavender
	170,110,40, //Brown
	255,250,200, //Beige
	128,0,0,//Maroon
	170,255,195, //Mint
	128,128,0, //Olive
	255,215,180, //Apricot
	0,0,128, //Navy
	128,128,128 //Gray
};

void main(int argc, char** argv) {
	QApplication app(argc, argv);
	QSettings qs("Test/NumpyIO");
	auto prevPath = qs.value("prevPath").toString();
	if (prevPath.isEmpty()) {
		prevPath = qApp->applicationDirPath();
	}
	const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select NPY", prevPath, "Numpy Array (*.npy)");

	if(fileName.isEmpty()) {
		std::cout << "empty file name" << std::endl;
		return;
	}

	qs.setValue("prevPath", fileName);

	auto tcfName = fileName.chopped(3) + "TCF";
	std::cout << tcfName.toStdString() << std::endl;

	SoDB::init();
	SoImageViz::init();
	SoVolumeRendering::init();

	auto reader = new TC::IO::TCNumpyReader;		
	std::any arr;
	auto meta = reader->Read(arr,fileName, TC::IO::NpyArrType::arrUSHORT);

	auto isFortranOrder = std::get<0>(meta);
	auto shape = std::get<1>(meta);

    if (isFortranOrder) {
		std::cout << "is fortran order" << std::endl;
	}
	else {
		std::cout << "is NOT fortran order" << std::endl;
	}

	std::cout << "Shape : ";
	for (auto i = 0; i < shape.size()-1; i++) {
		std::cout << shape[i] << ",";
	}
	std::cout << shape[shape.size()-1]<<std::endl;


	auto result = std::any_cast<std::vector<uint16_t>>(arr);
	std::cout << "final array size: " << result.size() << std::endl;

	QWidget* mainWidget = new QWidget;
	mainWidget->setMinimumSize(800, 800);

	QVBoxLayout* layout = new QVBoxLayout;
	mainWidget->setLayout(layout);

	ViewerExaminer* examiner = new ViewerExaminer(mainWidget);
	examiner->setCameraType(SoOrthographicCamera::getClassTypeId());
	layout->addWidget(examiner);
	examiner->setNavigationMode(SceneExaminer::PLANE);

	//Create Simple Scene Graph for mask
	SoRef<SoSeparator> root = new SoSeparator;	
	examiner->setSceneGraph(root.ptr());	

	SoRef<SoSeparator> maskRoot = new SoSeparator;
	SoRef<SoSeparator> imageRoot = new SoSeparator;

	root->addChild(maskRoot.ptr());
	root->addChild(imageRoot.ptr());

	SoRef<SoMaterial> matl = new SoMaterial;
	matl->transparency.setValue(0.3);
	imageRoot->addChild(matl.ptr());

	SoRef<OivHdf5Reader> oreader = new OivHdf5Reader;
	oreader->setTileName("000000");
	oreader->setDataGroupPath("/Data/2DMIP");
	oreader->setTileDimension(256);
	oreader->setFilename(tcfName.toStdString());

	SoRef<SoVolumeData> imageData = new SoVolumeData;
	imageData->setReader(*oreader, TRUE);
	imageRoot->addChild(imageData.ptr());

	SoRef<SoDataRange> imgRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(imgRange.ptr(), imageData.ptr());
	imageRoot->addChild(imgRange.ptr());

	SoRef<SoTransferFunction> imgTF = new SoTransferFunction;
	imgTF->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
	imageRoot->addChild(imgTF.ptr());
	
	MedicalHelper::dicomCheckMonochrome1(imgTF.ptr(), imageData.ptr());

	SoRef<SoOrthoSlice> imgSlice = new SoOrthoSlice;
	imageRoot->addChild(imgSlice.ptr());		

	SoRef<SoVolumeData> volData = new SoVolumeData;
	volData->data.setValue(SbVec3i32(shape[0], shape[1], 1), SoSFArray::DataType::UNSIGNED_SHORT, 16, result.data(), SoSFArray::COPY);
	volData->extent.connectFrom(&imageData->extent);
	maskRoot->addChild(volData.ptr());

	SoRef<SoDataRange> range = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(range.ptr(), volData.ptr());
	maskRoot->addChild(range.ptr());

	SoRef<SoTransferFunction> tf = new SoTransferFunction;
	tf->predefColorMap = SoTransferFunction::PredefColorMap::NONE;
	maskRoot->addChild(tf.ptr());

	auto maxIdx = static_cast<int>(range->max.getValue());

	tf->colorMap.setNum( maxIdx* 4);
	auto max = 100;
	float* p = tf->colorMap.startEditing();
	for (auto i = 0; i < maxIdx; ++i) {
		int idx = (float)i / 255.0 * max;
		if (idx < 1) {
			*p++ = 0.0;
			*p++ = 0.0;
			*p++ = 0.0;
			*p++ = 0.0;
		}
		else {
			float r = color_table[(idx - 1) % 20][0] / 255.0;
			float g = color_table[(idx - 1) % 20][1] / 255.0;
			float b = color_table[(idx - 1) % 20][2] / 255.0;
			*p++ = r;
			*p++ = g;
			*p++ = b;
			*p++ = 1.0;
		}
	}
	tf->colorMap.finishEditing();
	//MedicalHelper::dicomCheckMonochrome1(tf.ptr(), volData.ptr());

	SoRef<SoOrthoSlice> slice = new SoOrthoSlice;
	slice->interpolation = SoOrthoSlice::Interpolation::NEAREST;
	maskRoot->addChild(slice.ptr());
	
	MedicalHelper::orientView(MedicalHelper::AXIAL, examiner->getRenderArea()->getSceneInteractor()->getCamera(), volData.ptr());
	
	mainWidget->show();
	
	app.exec();
		
	SoVolumeRendering::finish();
	SoImageViz::finish();
	SoDB::finish();

	delete reader;
	delete mainWidget;
}
