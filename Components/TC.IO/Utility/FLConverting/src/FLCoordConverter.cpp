#include <TCFMetaReader.h>
#include "FLCoordConverter.h"

namespace TC::IO::FLConverting {
    struct FLConverter::Impl {
        bool isHT3DInfo{ false };
        bool isFL3DInfo{ false };
        bool isHT2DInfo{ false };
        bool isFL2DInfo{ false };
        hGeo ht3DGeometry;
        fGeo fl3DGeometry;
        hGeo ht2DGeometry;
        fGeo fl2DGeometry;
    };
    FLConverter::FLConverter() : d{ new Impl } {
        d->ht2DGeometry.is2D = true;
        d->fl2DGeometry.is2D = true;
    }
    FLConverter::~FLConverter() {        

    }    
    auto FLConverter::SetHTGeometry(const hGeo& geometry) -> void {
        if (geometry.is2D) {
            d->ht2DGeometry = geometry;
            d->isHT2DInfo = true;
        }
        else {
            d->ht3DGeometry = geometry;
            d->isHT3DInfo = true;
        }
    }    
    auto FLConverter::SetFLGeometry(const fGeo& geometry) -> void {
        if (geometry.is2D) {
            d->fl2DGeometry = geometry;
            d->isFL2DInfo = true;
        }
        else {
            d->fl3DGeometry = geometry;
            d->isFL3DInfo = true;
        }
    }
    auto FLConverter::SetGeometryFromFile(const QString& tcfPath) -> void {
        auto reader = new TCFMetaReader;
        auto metaInfo = reader->Read(tcfPath);
        //Retrieve HT2D info
        if (metaInfo->data.data2DMIP.exist) {
            d->isHT2DInfo = true;
            d->ht2DGeometry.dimension[0] = metaInfo->data.data2DMIP.sizeX;
            d->ht2DGeometry.dimension[1] = metaInfo->data.data2DMIP.sizeY;
            d->ht2DGeometry.spacing[0] = metaInfo->data.data2DMIP.resolutionX;
            d->ht2DGeometry.spacing[1] = metaInfo->data.data2DMIP.resolutionY;
        }
        //Retrieve HT3D info
        if (metaInfo->data.data3D.exist) {
            d->isHT3DInfo = true;
            d->ht3DGeometry.dimension[0] = metaInfo->data.data3D.sizeX;
            d->ht3DGeometry.dimension[1] = metaInfo->data.data3D.sizeY;
            d->ht3DGeometry.dimension[2] = metaInfo->data.data3D.sizeZ;
            d->ht3DGeometry.spacing[0] = metaInfo->data.data3D.resolutionX;
            d->ht3DGeometry.spacing[1] = metaInfo->data.data3D.resolutionY;
            d->ht3DGeometry.spacing[2] = metaInfo->data.data3D.resolutionZ;
        }
        //Retrieve FL2D info
        if (metaInfo->data.data2DFLMIP.exist) {
            d->isFL2DInfo = true;
            d->fl2DGeometry.dimension[0] = metaInfo->data.data2DFLMIP.sizeX;
            d->fl2DGeometry.dimension[1] = metaInfo->data.data2DFLMIP.sizeY;
            d->fl2DGeometry.spacing[0] = metaInfo->data.data2DFLMIP.resolutionX;
            d->fl2DGeometry.spacing[1] = metaInfo->data.data2DFLMIP.resolutionY;            
        }
        //Retrieve HT2D info
        if (metaInfo->data.data3DFL.exist) {
            d->isFL3DInfo = true;
            d->fl3DGeometry.dimension[0] = metaInfo->data.data3DFL.sizeX;
            d->fl3DGeometry.dimension[1] = metaInfo->data.data3DFL.sizeY;
            d->fl3DGeometry.dimension[2] = metaInfo->data.data3DFL.sizeZ;
            d->fl3DGeometry.spacing[0] = metaInfo->data.data3DFL.resolutionX;
            d->fl3DGeometry.spacing[1] = metaInfo->data.data3DFL.resolutionY;
            d->fl3DGeometry.spacing[2] = metaInfo->data.data3DFL.resolutionZ;
            d->fl3DGeometry.zOffset = metaInfo->data.data3DFL.offsetZ;
        }
    }
    auto FLConverter::isValid2d()->bool {
        return d->isHT2DInfo && d->isFL2DInfo;
    }
    auto FLConverter::isValid3d() -> bool {
        return d->isHT3DInfo && d->isFL3DInfo;
    }

    auto FLConverter::GetConvertedFL3DCoord(int x, int y, int z) -> std::tuple<int, int, int> {
        if (false == (x<d->fl3DGeometry.dimension[0]&&y<d->fl3DGeometry.dimension[1]&&z<d->fl3DGeometry.dimension[2])) {
            return std::make_tuple(-1, -1, -1);
        }
        //calculate physical position of input fl volume
        auto phyX = static_cast<float>(x) * d->fl3DGeometry.spacing[0] - d->fl3DGeometry.spacing[0] * d->fl3DGeometry.dimension[0] / 2.f;
        auto phyY = static_cast<float>(y) * d->fl3DGeometry.spacing[1] - d->fl3DGeometry.spacing[1] * d->fl3DGeometry.dimension[1] / 2.f;
        auto phyZ = static_cast<float>(z) * d->fl3DGeometry.spacing[2] - d->fl3DGeometry.spacing[2] * d->fl3DGeometry.dimension[2] / 2.f + d->fl3DGeometry.zOffset;

        int htX;
        int htY;
        int htZ;

        auto phyXMax = d->ht3DGeometry.spacing[0] * d->ht3DGeometry.dimension[0] / 2;
        auto phyYMax = d->ht3DGeometry.spacing[1] * d->ht3DGeometry.dimension[1] / 2;
        auto phyZMax = d->ht3DGeometry.spacing[2] * d->ht3DGeometry.dimension[2] / 2;

        if(abs(phyX) > phyXMax) {
            htX = -1;
        }else {
            htX = static_cast<int>((phyX + phyXMax) / d->ht3DGeometry.spacing[0]);
        }

        if(abs(phyY) > phyYMax) {
            htY = -1;
        }else {
            htY = static_cast<int>((phyY + phyYMax) / d->ht3DGeometry.spacing[1]);
        }

        if(abs(phyZ) > phyZMax) {
            htZ = -1;
        }else {
            htZ = static_cast<int>((phyZ + phyZMax) / d->ht3DGeometry.spacing[2]);
        }

        return std::make_tuple(htX, htY, htZ);
    }

    auto FLConverter::GetConvertedFL2DCoord(int x, int y) -> std::tuple<int, int> {
        if (false == (x < d->fl3DGeometry.dimension[0] && y < d->fl3DGeometry.dimension[1])) {
            return std::make_tuple(-1, -1);
        }
        //calculate physical position of input fl volume
        auto phyX = static_cast<float>(x) * d->fl3DGeometry.spacing[0] - d->fl3DGeometry.spacing[0] * d->fl3DGeometry.dimension[0] / 2.f;
        auto phyY = static_cast<float>(y) * d->fl3DGeometry.spacing[1] - d->fl3DGeometry.spacing[1] * d->fl3DGeometry.dimension[1] / 2.f;        

        int htX;
        int htY;        

        auto phyXMax = d->ht3DGeometry.spacing[0] * d->ht3DGeometry.dimension[0] / 2;
        auto phyYMax = d->ht3DGeometry.spacing[1] * d->ht3DGeometry.dimension[1] / 2;        

        if (abs(phyX) > phyXMax) {
            htX = -1;
        }
        else {
            htX = static_cast<int>((phyX + phyXMax) / d->ht3DGeometry.spacing[0]);
        }

        if (abs(phyY) > phyYMax) {
            htY = -1;
        }
        else {
            htY = static_cast<int>((phyY + phyYMax) / d->ht3DGeometry.spacing[1]);
        }        

        return std::make_tuple(htX, htY);
    }

    auto FLConverter::GetConvertedFL2D(VolumePtr flVolume,BoundingBox bbox) -> std::tuple<VolumePtr, BoundingBox> {
        if(false == isValid2d()) {
            return std::make_tuple(nullptr,BoundingBox());
        }
        if(nullptr == flVolume) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if(bbox.GetVolume() == 0) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        auto offset = bbox.GetOffset();
        auto size = bbox.GetSize();

        auto x1 = offset.x0 + size.d0 - 1;
        auto y1 = offset.y0 + size.d1 - 1;

        if(offset.x0 < 0 || static_cast<int>(x1 +1) >= d->fl2DGeometry.dimension[0]) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if(offset.y0 < 0 || static_cast<int>(y1 + 1) >= d->fl2DGeometry.dimension[1]) {
            return std::make_tuple(nullptr, BoundingBox());
        }

        VolumePtr vPtr = std::shared_ptr<unsigned short[]>(new unsigned short[d->ht2DGeometry.dimension[0] * d->ht2DGeometry.dimension[1]](), std::default_delete<unsigned short[]>());

		for (auto i = offset.x0; i < offset.x0 + size.d0; i++) {
			for (auto j = offset.y0; j < offset.y0 + size.d1; j++) {
				auto idx = GetConvertedFL2DCoord(i, j);
				auto idxX = std::get<0>(idx);
				auto idxY = std::get<1>(idx);
				if (idxX > 0 && idxY > 0) {
					auto val = *(flVolume.get() + i * d->fl3DGeometry.dimension[1] + j);
					*(vPtr.get() + idxX * d->ht3DGeometry.dimension[1] + idxY) = val;
				}
			}
		}

        auto offsetHT = GetConvertedFL2DCoord(offset.x0, offset.y0);
        auto offsetX = std::get<0>(offsetHT);
        if (offsetX < 0) {
            offsetX = 0;
        }
        auto offsetY = std::get<1>(offsetHT);
        if (offsetY < 0) {
            offsetY = 0;
        }        

        auto endPointHT = GetConvertedFL2DCoord(offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1);
        auto endPointX = std::get<0>(endPointHT);
        if (endPointX < 0) {
            endPointX = d->ht3DGeometry.dimension[0] - 1;
        }
        auto endPointY = std::get<1>(endPointHT);
        if (endPointY < 0) {
            endPointY = d->ht3DGeometry.dimension[1] - 1;
        }       
        BoundingBox hBound;
        hBound.SetOffset(offsetX, offsetY, 0);
        hBound.SetSize(endPointX - offsetX + 1, endPointY - offsetY + 1,0);

        return std::make_tuple(vPtr, hBound);
    }

    auto FLConverter::GetConvertedFL3D(VolumePtr flVolume,BoundingBox bbox) -> std::tuple<VolumePtr, BoundingBox> {
        if (false == isValid3d()) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if (nullptr == flVolume) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if (bbox.GetVolume() == 0) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        auto offset = bbox.GetOffset();
        auto size = bbox.GetSize();

        auto x1 = offset.x0 + size.d0 - 1;
        auto y1 = offset.y0 + size.d1 - 1;
        auto z1 = offset.z0 + size.d2 - 1;

        if (offset.x0 < 0 || static_cast<int>(x1 + 1) >= d->fl3DGeometry.dimension[0]) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if (offset.y0 < 0 || static_cast<int>(y1 + 1) >= d->fl3DGeometry.dimension[1]) {
            return std::make_tuple(nullptr, BoundingBox());
        }
        if (offset.z0 < 0 || static_cast<int>(z1 + 1)>=d->fl3DGeometry.dimension[2]) {
            return std::make_tuple(nullptr, BoundingBox());
        }

        VolumePtr vPtr = std::shared_ptr<unsigned short[]>(new unsigned short[d->ht3DGeometry.dimension[0] * d->ht3DGeometry.dimension[1]* d->ht3DGeometry.dimension[2]](), std::default_delete<unsigned short[]>());
                
        for(auto i=offset.x0;i<offset.x0+size.d0;i++) {
            for(auto j=offset.y0;j<offset.y0+size.d1;j++) {
                for(auto k=offset.z0;k<offset.z0+size.d2;k++) {
                    auto idx = GetConvertedFL3DCoord(i, j, k);
                    auto idxX = std::get<0>(idx);
                    auto idxY = std::get<1>(idx);
                    auto idxZ = std::get<2>(idx);
                    if(idxX > 0 && idxY >0 && idxZ >0) {                        
                        auto val = *(flVolume.get() + i * d->fl3DGeometry.dimension[1] * d->fl3DGeometry.dimension[2] + j * d->fl3DGeometry.dimension[2] + k);
                        *(vPtr.get() + idxX * d->ht3DGeometry.dimension[1] * d->ht3DGeometry.dimension[2] + idxY * d->ht3DGeometry.dimension[2] + idxZ) = val;
                    }
                }
            }
        }

        auto offsetHT = GetConvertedFL3DCoord(offset.x0, offset.y0, offset.z0);
        auto offsetX = std::get<0>(offsetHT);
        if(offsetX <0) {
            offsetX = 0;
        }
        auto offsetY = std::get<1>(offsetHT);
        if(offsetY < 0) {
            offsetY = 0;
        }
        auto offsetZ = std::get<2>(offsetHT);
        if(offsetZ <0) {
            offsetZ = 0;
        }

        auto endPointHT = GetConvertedFL3DCoord(offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
        auto endPointX = std::get<0>(endPointHT);
        if(endPointX<0) {
            endPointX = d->ht3DGeometry.dimension[0] - 1;
        }
        auto endPointY = std::get<1>(endPointHT);
        if(endPointY<0) {
            endPointY = d->ht3DGeometry.dimension[1] - 1;
        }
        auto endPointZ = std::get<2>(endPointHT);
        if(endPointZ<0) {
            endPointZ = d->ht3DGeometry.dimension[2] - 1;
        }
        BoundingBox hBound;
        hBound.SetOffset(offsetX, offsetY, offsetZ);
        hBound.SetSize(endPointX - offsetX + 1, endPointY - offsetY + 1, endPointZ - offsetZ + 1);

        return std::make_tuple(vPtr, hBound);
    }

}