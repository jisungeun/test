#pragma once

#include <memory>
#include <QString>

#include <BoundingBox.h>
#include "TCFLConvertingExport.h"

namespace TC::IO::FLConverting {
	typedef struct HTGeometry {
		int dimension[3]{0,0,0};
		float spacing[3]{1.0,1.0,1.0};		
		bool is2D{ false };
	}hGeo;
	typedef struct FLGeometry {
		int dimension[3]{0,0,0};
		float spacing[3]{1.0,1.0,1.0};		
		float zOffset{0.0};
		bool is2D{ false };
	}fGeo;
	
	class TCFLConverting_API FLConverter final {
		typedef std::shared_ptr<unsigned short[]> VolumePtr;
	public:
		FLConverter();
		~FLConverter();

		auto SetHTGeometry(const hGeo& geometry)->void;
		auto SetFLGeometry(const fGeo& geometry)->void;
		auto SetGeometryFromFile(const QString& tcfPath)->void;

		auto isValid3d()->bool;
		auto isValid2d()->bool;

		//cooridnate conversion
		auto GetConvertedFL2DCoord(int x, int y)->std::tuple<int, int>;
		auto GetConvertedFL3DCoord(int x, int y, int z)->std::tuple<int, int, int>;

		//volume conversion
		auto GetConvertedFL2D(VolumePtr flVolume, BoundingBox bbox)->std::tuple<VolumePtr,BoundingBox>;
		auto GetConvertedFL3D(VolumePtr flVolume,BoundingBox bbox)->std::tuple<VolumePtr,BoundingBox>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}