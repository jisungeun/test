#include <catch2/catch.hpp>

#include <QFile>

#include <H5Cpp.h>

#include "FLCoordConverter.h"

using namespace TC::IO::FLConverting;

namespace FLCoordConverterTest {
    auto CreateAttribute(H5::H5Object& object, const std::string& name)->void {
        auto attribute = object.createAttribute(name, H5::PredType::NATIVE_INT64, H5S_SCALAR);
        const int64_t attributeValue = 1;
        attribute.write(H5::PredType::NATIVE_INT64, &attributeValue);
        attribute.close();
    }       

    auto GenerateDummyTCF()->QString {
        if (QFile::exists("dummyTcf.h5")) return "dummyTcf.h5";

        H5::H5File dummyTcf("dummyTcf.h5", H5F_ACC_TRUNC);
        CreateAttribute(dummyTcf, "topAttribute");

        auto groupData = dummyTcf.createGroup("Data");
        {
            auto group3D = groupData.createGroup("3D");
            {
                CreateAttribute(group3D, "group3DAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 210,200,200 };
                H5::DataSpace dataSpace3D(rank, dims);
                auto dataSet000000 = group3D.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace3D);
                auto dataSet000001 = group3D.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpace3D);
                dataSpace3D.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group3D.close();

            auto group2DMIP = groupData.createGroup("2DMIP");
            {
                CreateAttribute(group2DMIP, "group2DMIPAttribute");

                const hsize_t rank = 2;
                const hsize_t dims[rank] = { 200,200 };
                H5::DataSpace dataSpace2DMIP(rank, dims);
                auto dataSet000000 = group2DMIP.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace2DMIP);
                auto dataSet000001 = group2DMIP.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpace2DMIP);
                dataSpace2DMIP.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group2DMIP.close();

            auto group3DFL = groupData.createGroup("3DFL");
            {
                CreateAttribute(group3DFL, "group3DFLAttribute");

                auto groupCH0 = group3DFL.createGroup("CH0");
                {
                    CreateAttribute(groupCH0, "groupCH0Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH0(rank, dims);
                    auto dataSet000000 = groupCH0.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    auto dataSet000001 = groupCH0.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    dataSpaceCH0.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH0.close();

                auto groupCH1 = group3DFL.createGroup("CH1");
                {
                    CreateAttribute(groupCH1, "groupCH1Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH1(rank, dims);
                    auto dataSet000000 = groupCH1.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    auto dataSet000001 = groupCH1.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    dataSpaceCH1.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH1.close();

                auto groupCH2 = group3DFL.createGroup("CH2");
                {
                    CreateAttribute(groupCH2, "groupCH2Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH2(rank, dims);
                    auto dataSet000000 = groupCH2.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    auto dataSet000001 = groupCH2.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    dataSpaceCH2.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH2.close();


            }
            group3DFL.close();

            auto group2DFLMIP = groupData.createGroup("2DFLMIP");
            {
                CreateAttribute(group2DFLMIP, "group2DFLMIPAttribute");

                auto groupCH0 = group2DFLMIP.createGroup("CH0");
                {
                    CreateAttribute(groupCH0, "groupCH0Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH0(rank, dims);
                    auto dataSet000000 =
                        groupCH0.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    auto dataSet000001 =
                        groupCH0.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    groupCH0.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }

                auto groupCH1 = group2DFLMIP.createGroup("CH1");
                {
                    CreateAttribute(groupCH1, "groupCH1Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH1(rank, dims);
                    auto dataSet000000 =
                        groupCH1.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    auto dataSet000001 =
                        groupCH1.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    dataSpaceCH1.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }

                auto groupCH2 = group2DFLMIP.createGroup("CH2");
                {
                    CreateAttribute(groupCH2, "groupCH2Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH2(rank, dims);
                    auto dataSet000000 =
                        groupCH2.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    auto dataSet000001 =
                        groupCH2.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    dataSpaceCH2.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
            }
            group2DFLMIP.close();

            auto group2D = groupData.createGroup("2D");
            {
                CreateAttribute(group2D, "group2DAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 60, 100, 100 };
                H5::DataSpace dataSpace2D(rank, dims);
                auto dataSet000000 = group2D.createDataSet("000000", H5::PredType::NATIVE_FLOAT, dataSpace2D);
                auto dataSet000001 = group2D.createDataSet("000001", H5::PredType::NATIVE_FLOAT, dataSpace2D);
                dataSpace2D.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group2D.close();

            auto groupBF = groupData.createGroup("BF");
            {
                CreateAttribute(groupBF, "groupBFAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 3, 210, 210 };
                H5::DataSpace dataSpaceBF(rank, dims);
                auto dataSet000000 = groupBF.createDataSet("000000", H5::PredType::NATIVE_UINT8, dataSpaceBF);
                auto dataSet000001 = groupBF.createDataSet("000001", H5::PredType::NATIVE_UINT8, dataSpaceBF);
                dataSpaceBF.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            groupBF.close();
        }
        groupData.close();

        auto groupInfo = dummyTcf.createGroup("Info");
        {
            auto groupAnnotation = groupInfo.createGroup("Annotation");
            {
                CreateAttribute(groupAnnotation, "groupAnnotationAttribute");
            }
            groupAnnotation.close();

            auto groupDevice = groupInfo.createGroup("Device");
            {
                CreateAttribute(groupDevice, "groupDeviceAttribute");
            }
            groupDevice.close();

            auto groupImaging = groupInfo.createGroup("Imaging");
            {
                CreateAttribute(groupImaging, "groupImagingAttribute");
            }
            groupImaging.close();

            auto groupTile = groupInfo.createGroup("Tile");
            {
                CreateAttribute(groupTile, "groupTileAttribute");
            }
            groupTile.close();
        }
        groupInfo.close();

        auto groupView = dummyTcf.createGroup("View");
        {
            auto level01Group = groupView.createGroup("Level01");
            level01Group.close();
        }
        groupView.close();

        dummyTcf.close();

        return "dummyTcf.h5";
    }

    TEST_CASE("FLConverter") {
        const auto inputTCFPath = GenerateDummyTCF();
        auto converter = new FLConverter;
        SECTION("Exceptions") {
            REQUIRE_FALSE(converter->isValid2d());            

            auto convertedXY = converter->GetConvertedFL2DCoord(0, 0);
            REQUIRE(-1 == std::get<0>(convertedXY));
            REQUIRE(-1 ==std::get<1>(convertedXY));

            auto convertedMIP = converter->GetConvertedFL2D(nullptr, TC::IO::BoundingBox());
            REQUIRE(nullptr == std::get<0>(convertedMIP));
            REQUIRE(0 == std::get<1>(convertedMIP).GetVolume());

            REQUIRE_FALSE(converter->isValid3d());

            auto convertedXYZ = converter->GetConvertedFL3DCoord(0, 0, 0);
            REQUIRE(-1 == std::get<0>(convertedXYZ));
            REQUIRE(-1 == std::get<1>(convertedXYZ));
            REQUIRE(-1 == std::get<2>(convertedXYZ));

            auto convertedVolume = converter->GetConvertedFL3D(nullptr, TC::IO::BoundingBox());
            REQUIRE(nullptr == std::get<0>(convertedVolume));
            REQUIRE(0 == std::get<1>(convertedVolume).GetVolume());
        }
        SECTION("2DConversion-with-custom-geometry") {
            
        }            
        SECTION("2DConversion-with-geomtery-from-tcf") {
            
        }
        SECTION("3DConversion-with-custom-geometry") {
            
        }
        SECTION("3DConversion-with-geometry-from-tcf") {
            
        }
    }
}