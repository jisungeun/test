#pragma once

#include <memory>
#include "TCProcessedDataWritingExport.h"

#include "IBFWriter.h"

class TCProcessedDataWriting_API BFWriterHDF5 final : public IBFWriter{
public:
    BFWriterHDF5();
    ~BFWriterHDF5();

    auto SetTargetFilePath(const QString& targetFilePath) -> void override;
    auto SetInput(const BFWriterInput& input) -> void override;
    auto SetOutputPort(const IBFWriterOutput::Pointer& outputPort) -> void override;
    auto Write() -> bool override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};