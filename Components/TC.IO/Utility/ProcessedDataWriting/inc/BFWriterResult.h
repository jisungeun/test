#pragma once

#include <memory>

#include <QString>
#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API BFWriterResult {
public:
    BFWriterResult();
    BFWriterResult(const BFWriterResult& other);
    ~BFWriterResult();

    auto operator=(const BFWriterResult& other)->BFWriterResult&;

    auto SetSuccessFlag(const bool& successFlag)->void;
    auto GetSuccessFlag()const->const bool&;

    auto SetWrittenFilePath(const QString& writtenFilePath)->void;
    auto GetWrittenFilePath()const->const QString&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
