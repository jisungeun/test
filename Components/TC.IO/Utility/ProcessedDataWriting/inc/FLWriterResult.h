#pragma once

#include <memory>

#include <QString>
#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API FLWriterResult {
public:
    FLWriterResult();
    FLWriterResult(const FLWriterResult& other);
    ~FLWriterResult();

    auto operator=(const FLWriterResult& other)->FLWriterResult&;

    auto SetSuccessFlag(const bool& successFlag)->void;
    auto GetSuccessFlag()const->const bool&;

    auto SetWrittenFilePath(const QString& writtenFilePath)->void;
    auto GetWrittenFilePath()const->const QString&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
