#pragma once

#include <memory>
#include "TCProcessedDataWritingExport.h"

#include "IFLWriter.h"

class TCProcessedDataWriting_API FLWriterHDF5 final : public IFLWriter{
public:
    FLWriterHDF5();
    ~FLWriterHDF5();

    auto SetTargetFilePath(const QString& targetFilePath) -> void override;
    auto SetInput(const FLWriterInput& input) -> void override;
    auto SetOutputPort(const IFLWriterOutput::Pointer& outputPort) -> void override;
    auto Write() -> bool override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};