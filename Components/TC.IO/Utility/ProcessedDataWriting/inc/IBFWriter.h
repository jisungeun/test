#pragma once

#include <memory>
#include <QString>

#include "BFWriterInput.h"
#include "IBFWriterOutput.h"
#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API IBFWriter {
public:
    typedef std::shared_ptr<IBFWriter> Pointer;
    virtual ~IBFWriter() = default;

    virtual auto SetTargetFilePath(const QString& targetFilePath)->void = 0;
    virtual auto SetInput(const BFWriterInput& input)->void = 0;
    virtual auto SetOutputPort(const IBFWriterOutput::Pointer& outputPort) -> void = 0;
    virtual auto Write()->bool = 0;
};