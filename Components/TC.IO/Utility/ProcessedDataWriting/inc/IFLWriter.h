#pragma once

#include <memory>
#include <QString>

#include "FLWriterInput.h"
#include "IFLWriterOutput.h"

#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API IFLWriter {
public:
    typedef std::shared_ptr<IFLWriter> Pointer;
    virtual ~IFLWriter() = default;

    virtual auto SetTargetFilePath(const QString& targetFilePath)->void = 0;
    virtual auto SetInput(const FLWriterInput& input)->void = 0;
    virtual auto SetOutputPort(const IFLWriterOutput::Pointer& outputPort) -> void = 0;
    virtual auto Write()->bool = 0;
};