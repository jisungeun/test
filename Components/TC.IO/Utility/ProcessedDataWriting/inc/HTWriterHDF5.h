#pragma once

#include <memory>
#include "TCProcessedDataWritingExport.h"

#include "IHTWriter.h"

class TCProcessedDataWriting_API HTWriterHDF5 final : public IHTWriter{
public:
    HTWriterHDF5();
    ~HTWriterHDF5();

    auto SetTargetFilePath(const QString& targetFilePath) -> void override;
    auto SetInput(const HTWriterInput& input) -> void override;
    auto SetOutputPort(const IHTWriterOutput::Pointer& outputPort) -> void override;
    auto Write() -> bool override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};