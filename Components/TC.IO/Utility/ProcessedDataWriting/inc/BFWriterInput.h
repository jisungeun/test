#pragma once

#include <memory>

#include "SIUnit.h"

#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API BFWriterInput {
public:
    BFWriterInput();
    BFWriterInput(const BFWriterInput& other);
    ~BFWriterInput();

    auto operator=(const BFWriterInput& other)->BFWriterInput&;

    auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY)->void;
    auto SetChannelCount(const int32_t& channelCount)->void;

    auto GetDataSizeX()const->const int32_t&;
    auto GetDataSizeY()const->const int32_t&;
    auto GetChannelCount()const->const int32_t&;

    auto SetData(const std::shared_ptr<uint8_t[]>& data)->void;
    auto GetData()const->const std::shared_ptr<uint8_t[]>&;

    auto SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY, const LengthUnit& unit)->void;
    auto GetPixelWorldSizeX(const LengthUnit& unit)const->float;
    auto GetPixelWorldSizeY(const LengthUnit& unit)const->float;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};