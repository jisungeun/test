#pragma once

#include <memory>

#include "SIUnit.h"

#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API FLWriterInput {
public:
    FLWriterInput();
    FLWriterInput(const FLWriterInput& other);
    ~FLWriterInput();

    auto operator=(const FLWriterInput& other)->FLWriterInput&;

    auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto GetDataSizeX()const->const int32_t&;
    auto GetDataSizeY()const->const int32_t&;
    auto GetDataSizeZ()const->const int32_t&;

    auto SetData(const std::shared_ptr<float[]>& data)->void;
    auto GetData()const->const std::shared_ptr<float[]>&;

    auto SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY, const float& pixelWorldSizeZ,
        const LengthUnit& unit)->void;
    auto GetPixelWorldSizeX(const LengthUnit& unit)const->float;
    auto GetPixelWorldSizeY(const LengthUnit& unit)const->float;
    auto GetPixelWorldSizeZ(const LengthUnit& unit)const->float;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};