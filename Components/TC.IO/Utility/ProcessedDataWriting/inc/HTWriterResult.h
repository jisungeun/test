#pragma once

#include <memory>

#include <QString>
#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API HTWriterResult {
public:
    HTWriterResult();
    HTWriterResult(const HTWriterResult& other);
    ~HTWriterResult();

    auto operator=(const HTWriterResult& other)->HTWriterResult&;

    auto SetSuccessFlag(const bool& successFlag)->void;
    auto GetSuccessFlag()const->const bool&;

    auto SetWrittenFilePath(const QString& writtenFilePath)->void;
    auto GetWrittenFilePath()const->const QString&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
