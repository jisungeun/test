#pragma once

#include <memory>

#include "TCProcessedDataWritingExport.h"

#include "HTWriterResult.h"

class TCProcessedDataWriting_API IHTWriterOutput {
public:
    typedef std::shared_ptr<IHTWriterOutput> Pointer;
    virtual ~IHTWriterOutput() = default;

    virtual auto SetHTWriterResult(const HTWriterResult& htWriterResult)->void = 0;
};