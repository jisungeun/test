#pragma once

#include <memory>

#include "TCProcessedDataWritingExport.h"

#include "BFWriterResult.h"

class TCProcessedDataWriting_API IBFWriterOutput {
public:
    typedef std::shared_ptr<IBFWriterOutput> Pointer;
    virtual ~IBFWriterOutput() = default;

    virtual auto SetBFWriterResult(const BFWriterResult& bfWriterResult)->void = 0;
};