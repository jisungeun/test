#pragma once

#include <memory>
#include <QString>

#include "HTWriterInput.h"
#include "IHTWriterOutput.h"
#include "TCProcessedDataWritingExport.h"

class TCProcessedDataWriting_API IHTWriter {
public:
    typedef std::shared_ptr<IHTWriter> Pointer;
    virtual ~IHTWriter() = default;

    virtual auto SetTargetFilePath(const QString& targetFilePath)->void = 0;
    virtual auto SetInput(const HTWriterInput& input)->void = 0;
    virtual auto SetOutputPort(const IHTWriterOutput::Pointer& outputPort)->void = 0;
    virtual auto Write()->bool = 0;
};