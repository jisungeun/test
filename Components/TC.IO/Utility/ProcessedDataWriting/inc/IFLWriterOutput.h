#pragma once

#include <memory>

#include "TCProcessedDataWritingExport.h"

#include "FLWriterResult.h"

class TCProcessedDataWriting_API IFLWriterOutput {
public:
    typedef std::shared_ptr<IFLWriterOutput> Pointer;
    virtual ~IFLWriterOutput() = default;

    virtual auto SetFLWriterResult(const FLWriterResult& flWriterResult)->void = 0;
};