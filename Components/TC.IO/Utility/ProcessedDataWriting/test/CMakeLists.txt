project(TCProcessedDataWritingTest)

ADD_DEFINITIONS(-DTEST_DATA_FOLDR_PATH=\"${TEST_DATA_FOLDER_PATH}\")

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

set(TEST_SOURCE
)

#Sources
set(SOURCES
	src/TestMain.cpp
	src/HTWriterInputTest.cpp
	src/HTWriterResultTest.cpp
	src/IHTWriterOutputTest.cpp
	src/IHTWriterTest.cpp
	src/FLWriterInputTest.cpp
	src/FLWriterResultTest.cpp
	src/IFLWriterOutputTest.cpp
	src/IFLWriterTest.cpp
	src/BFWriterInputTest.cpp
	src/BFWriterResultTest.cpp
	src/IBFWriterOutputTest.cpp
	src/IBFWriterTest.cpp
	src/HTWriterHDF5Test.cpp
	src/FLWriterHDF5Test.cpp
	src/BFWriterHDF5Test.cpp
	src/MakingTestFile.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${TEST_SOURCE}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
		TC::Components::ProcessedDataWriting
		TC::Common::TestUtility
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases")
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)

foreach(FILE ${TEST_SOURCE})
    # Remove common directory prefix to make the group
    string(REPLACE "src" "" GROUP "TEST_SOURCE")

    # Make sure we are using windows slashes
    string(REPLACE "/" "\\" GROUP "${GROUP}")
	
	source_group("${GROUP}" FILES "${FILE}")
endforeach()


