#include <catch2/catch.hpp>

#include "HTWriterResult.h"

namespace HTWriterResultTest {
    TEST_CASE("HTWriterResultTest") {
        SECTION("HTWriterResult()") {
            HTWriterResult htWriterResult;
            CHECK(&htWriterResult != nullptr);
        }
        SECTION("HTWriterResult(other)") {
            HTWriterResult srcHTWriterResult;
            srcHTWriterResult.SetSuccessFlag(true);

            HTWriterResult destHTWriterResult(srcHTWriterResult);
            CHECK(destHTWriterResult.GetSuccessFlag() == true);
        }
        SECTION("operator=()") {
            HTWriterResult srcHTWriterResult;
            srcHTWriterResult.SetSuccessFlag(true);

            HTWriterResult destHTWriterResult;
            destHTWriterResult = srcHTWriterResult;
            CHECK(destHTWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetSuccessFlag()") {
            HTWriterResult htWriterResult;
            htWriterResult.SetSuccessFlag(true);
            CHECK(&htWriterResult != nullptr);
        }
        SECTION("GetSuccessFlag()") {
            HTWriterResult htWriterResult;
            htWriterResult.SetSuccessFlag(true);
            CHECK(htWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetWrittenFilePath()") {
            HTWriterResult htWriterResult;
            htWriterResult.SetWrittenFilePath("");
            CHECK(&htWriterResult != nullptr);
        }
        SECTION("GetWrittenFilePath()") {
            HTWriterResult htWriterResult;
            htWriterResult.SetWrittenFilePath("test");
            CHECK(htWriterResult.GetWrittenFilePath() == "test");
        }

    }
}