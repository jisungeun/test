#include <catch2/catch.hpp>

#include "IHTWriter.h"

namespace IHTWriterTest {
    class HTWriterForTest final : public IHTWriter {
    public:
        HTWriterForTest() = default;
        ~HTWriterForTest() = default;

        auto SetTargetFilePath(const QString& targetFilePath) -> void override {
            setTargetFilePathTriggered = true;
        }
        auto SetInput(const HTWriterInput& input) -> void override {
            setInputTriggered = true;
        }
        auto Write() -> bool override {
            writeTriggered = true;
            return false;
        }

        auto SetOutputPort(const IHTWriterOutput::Pointer& outputPort) -> void override {
            setOutputPortTriggered = true;
        }

        bool setTargetFilePathTriggered{ false };
        bool setInputTriggered{ false };
        bool writeTriggered{ false };
        bool setOutputPortTriggered{ false };
    };

    TEST_CASE("IHTWriterTest") {
        SECTION("SetTargetFilePath()") {
            HTWriterForTest htWriter;
            CHECK(htWriter.setTargetFilePathTriggered == false);
            htWriter.SetTargetFilePath("");
            CHECK(htWriter.setTargetFilePathTriggered == true);
        }
        SECTION("SetInput()") {
            HTWriterForTest htWriter;
            CHECK(htWriter.setInputTriggered == false);
            htWriter.SetInput({});
            CHECK(htWriter.setInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            HTWriterForTest htWriter;
            CHECK(htWriter.setOutputPortTriggered == false);
            htWriter.SetOutputPort({});
            CHECK(htWriter.setOutputPortTriggered == true);
        }
        SECTION("Writer()") {
            HTWriterForTest htWriter;
            CHECK(htWriter.writeTriggered == false);
            htWriter.Write();
            CHECK(htWriter.writeTriggered == true);
        }
    }
}