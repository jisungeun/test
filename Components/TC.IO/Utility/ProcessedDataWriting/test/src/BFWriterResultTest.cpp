#include <catch2/catch.hpp>

#include "BFWriterResult.h"

namespace BFWriterResultTest {
    TEST_CASE("BFWriterResultTest") {
        SECTION("BFWriterResult()") {
            BFWriterResult bfWriterResult;
            CHECK(&bfWriterResult != nullptr);
        }
        SECTION("BFWriterResult(other)") {
            BFWriterResult srcBFWriterResult;
            srcBFWriterResult.SetSuccessFlag(true);

            BFWriterResult destBFWriterResult(srcBFWriterResult);
            CHECK(destBFWriterResult.GetSuccessFlag() == true);
        }
        SECTION("operator=()") {
            BFWriterResult srcBFWriterResult;
            srcBFWriterResult.SetSuccessFlag(true);

            BFWriterResult destBFWriterResult;
            destBFWriterResult = srcBFWriterResult;
            CHECK(destBFWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetSuccessFlag()") {
            BFWriterResult bfWriterResult;
            bfWriterResult.SetSuccessFlag(true);
            CHECK(&bfWriterResult != nullptr);
        }
        SECTION("GetSuccessFlag()") {
            BFWriterResult bfWriterResult;
            bfWriterResult.SetSuccessFlag(true);
            CHECK(bfWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetWrittenFilePath()") {
            BFWriterResult bfWriterResult;
            bfWriterResult.SetWrittenFilePath("");
            CHECK(&bfWriterResult != nullptr);
        }
        SECTION("GetWrittenFilePath()") {
            BFWriterResult bfWriterResult;
            bfWriterResult.SetWrittenFilePath("test");
            CHECK(bfWriterResult.GetWrittenFilePath() == "test");
        }

    }
}