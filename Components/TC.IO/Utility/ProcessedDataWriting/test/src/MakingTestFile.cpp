//#include <catch2/catch.hpp>
//
//#include "HTWriterHDF5.h"
//#include "FLWriterHDF5.h"
//#include "BFWriterHDF5.h"
//
//#include "BinaryFileIO.h"
//
//namespace MakingTestFile {
//    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCProcessedDataWritingTest";
//    const QString dataFolderPath = testFolderPath + "/MakingTestFile";
//
//    class HTWriterOutputForTest final : public IHTWriterOutput {
//    public:
//        HTWriterOutputForTest() = default;
//        ~HTWriterOutputForTest() = default;
//
//        auto SetHTWriterResult(const HTWriterResult& htWriterResult) -> void override {
//            this->result = htWriterResult;
//        }
//
//        HTWriterResult result;
//    };
//
//    class FLWriterOutputForTest final : public IFLWriterOutput {
//    public:
//        FLWriterOutputForTest() = default;
//        ~FLWriterOutputForTest() = default;
//
//        auto SetFLWriterResult(const FLWriterResult& flWriterResult) -> void override {
//            this->result = flWriterResult;
//        }
//
//        FLWriterResult result;
//    };
//
//    class BFWriterOutputForTest final : public IBFWriterOutput {
//    public:
//        BFWriterOutputForTest() = default;
//        ~BFWriterOutputForTest() = default;
//
//        auto SetBFWriterResult(const BFWriterResult& bfWriterResult) -> void override {
//            this->result = bfWriterResult;
//        }
//
//        BFWriterResult result;
//    };
//
//    TEST_CASE("TCProcessedDataWritingTest MakingTestFile") {
//        SECTION("HTWriterHDF5") {
//            constexpr auto sizeX = 256;
//            constexpr auto sizeY = 256;
//            constexpr auto sizeZ = 3;
//            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;
//
//            const std::string rawDataFilePath = dataFolderPath.toStdString() + "/htRawData";
//            const auto rawData = ReadFile_float(rawDataFilePath, numberOfElements);
//
//            std::shared_ptr<float[]> data{ new float[numberOfElements]() };
//            std::copy_n(rawData.get(), numberOfElements, data.get());
//
//            constexpr float pixelWorldSizeX = 0.1f;
//            constexpr float pixelWorldSizeY = 0.1f;
//            constexpr float pixelWorldSizeZ = 0.1f;
//            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;
//
//            HTWriterInput input;
//            input.SetData(data);
//            input.SetDataSize(sizeX, sizeY, sizeZ);
//            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
//
//            const QString targetFilePath = "C:/Temp/htDataForTileDataGetterProcessedData.h5";
//
//            const auto output = new HTWriterOutputForTest;
//            const IHTWriterOutput::Pointer outputPort{ output };
//
//            HTWriterHDF5 htWriterHdf5;
//            htWriterHdf5.SetInput(input);
//            htWriterHdf5.SetTargetFilePath(targetFilePath);
//            htWriterHdf5.SetOutputPort(outputPort);
//
//            htWriterHdf5.Write();
//        }
//
//        SECTION("FLWriterHDF5") {
//            constexpr auto sizeX = 256;
//            constexpr auto sizeY = 256;
//            constexpr auto sizeZ = 3;
//            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;
//
//            const std::string rawDataFilePath = dataFolderPath.toStdString() + "/flRawData";
//            const auto rawData = ReadFile_float(rawDataFilePath, numberOfElements);
//
//            std::shared_ptr<float[]> data{ new float[numberOfElements]() };
//            std::copy_n(rawData.get(), numberOfElements, data.get());
//
//            constexpr float pixelWorldSizeX = 0.1f;
//            constexpr float pixelWorldSizeY = 0.1f;
//            constexpr float pixelWorldSizeZ = 0.1f;
//            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;
//
//            FLWriterInput input;
//            input.SetData(data);
//            input.SetDataSize(sizeX, sizeY, sizeZ);
//            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
//
//            const QString targetFilePath = "C:/Temp/flDataForTileDataGetterProcessedData.h5";
//
//            const auto output = new FLWriterOutputForTest;
//            const IFLWriterOutput::Pointer outputPort{ output };
//
//            FLWriterHDF5 flWriterHdf5;
//            flWriterHdf5.SetInput(input);
//            flWriterHdf5.SetTargetFilePath(targetFilePath);
//            flWriterHdf5.SetOutputPort(outputPort);
//
//            flWriterHdf5.Write();
//        }
//
//        SECTION("HTWriterHDF5") {
//            constexpr auto sizeX = 256;
//            constexpr auto sizeY = 256;
//            constexpr auto sizeZ = 3;
//            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;
//
//            const std::string rawDataFilePath = dataFolderPath.toStdString() + "/bfRawData";
//            const auto rawData = ReadFile_uint8_t(rawDataFilePath, numberOfElements);
//
//            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
//            std::copy_n(rawData.get(), numberOfElements, data.get());
//
//            constexpr float pixelWorldSizeX = 0.1f;
//            constexpr float pixelWorldSizeY = 0.1f;
//            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;
//
//            BFWriterInput input;
//            input.SetData(data);
//            input.SetDataSize(sizeX, sizeY);
//            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
//
//            const QString targetFilePath = "C:/Temp/bfDataForTileDataGetterProcessedData.h5";
//
//            const auto output = new BFWriterOutputForTest;
//            const IBFWriterOutput::Pointer outputPort{ output };
//
//            BFWriterHDF5 bfWriterHdf5;
//            bfWriterHdf5.SetInput(input);
//            bfWriterHdf5.SetTargetFilePath(targetFilePath);
//            bfWriterHdf5.SetOutputPort(outputPort);
//
//            bfWriterHdf5.Write();
//        }
//    }
//}