#include <catch2/catch.hpp>

#include "IHTWriterOutput.h"

namespace IHTWriterOutputTest {
    class HTWriterOutputForTest final : public IHTWriterOutput {
    public:
        HTWriterOutputForTest() = default;
        ~HTWriterOutputForTest() = default;

        auto SetHTWriterResult(const HTWriterResult& htWriterResult) -> void override {
            this->setHTWriterResultTriggered = true;
        }

        bool setHTWriterResultTriggered{ false };
    };

    TEST_CASE("IHTWriterOutputTest") {
        SECTION("SetHTWriterResult()") {
            HTWriterOutputForTest htWriterOutput;
            CHECK(htWriterOutput.setHTWriterResultTriggered == false);
            htWriterOutput.SetHTWriterResult({});
            CHECK(htWriterOutput.setHTWriterResultTriggered == true);
        }
    }
}