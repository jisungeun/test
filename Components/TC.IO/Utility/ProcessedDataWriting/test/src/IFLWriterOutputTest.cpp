#include <catch2/catch.hpp>

#include "IFLWriterOutput.h"

namespace IFLWriterOutputTest {
    class FLWriterOutputForTest final : public IFLWriterOutput {
    public:
        FLWriterOutputForTest() = default;
        ~FLWriterOutputForTest() = default;

        auto SetFLWriterResult(const FLWriterResult& htWriterResult) -> void override {
            this->setFLWriterResultTriggered = true;
        }

        bool setFLWriterResultTriggered{ false };
    };

    TEST_CASE("IFLWriterOutputTest") {
        SECTION("SetFLWriterResult()") {
            FLWriterOutputForTest htWriterOutput;
            CHECK(htWriterOutput.setFLWriterResultTriggered == false);
            htWriterOutput.SetFLWriterResult({});
            CHECK(htWriterOutput.setFLWriterResultTriggered == true);
        }
    }
}