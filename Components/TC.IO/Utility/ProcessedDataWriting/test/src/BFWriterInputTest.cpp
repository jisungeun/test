#include <catch2/catch.hpp>

#include "BFWriterInput.h"

namespace BFWriterInputTest {
    TEST_CASE("BFWriterInputTest") {
        SECTION("BFWriterInput()") {
            BFWriterInput bfWriterInput;
            CHECK(&bfWriterInput != nullptr);
        }
        SECTION("BFWriterInput(other)") {
            BFWriterInput srcBFWriterInput;
            srcBFWriterInput.SetDataSize(1, 2);

            BFWriterInput destBFWriterInput(srcBFWriterInput);
            CHECK(destBFWriterInput.GetDataSizeX() == 1);
            CHECK(destBFWriterInput.GetDataSizeY() == 2);
        }
        SECTION("operator=()") {
            BFWriterInput srcBFWriterInput;
            srcBFWriterInput.SetDataSize(1, 2);

            BFWriterInput destBFWriterInput;
            destBFWriterInput = srcBFWriterInput;
            CHECK(destBFWriterInput.GetDataSizeX() == 1);
            CHECK(destBFWriterInput.GetDataSizeY() == 2);
        }
        SECTION("SetDataSize()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetDataSize(1, 2);
            CHECK(&bfWriterInput != nullptr);
        }
        SECTION("GetDataSizeX()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetDataSize(1, 2);
            CHECK(bfWriterInput.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetDataSize(1, 2);
            CHECK(bfWriterInput.GetDataSizeY() == 2);
        }
        SECTION("SetData()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetData({});
            CHECK(&bfWriterInput != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<uint8_t[]> data{ new uint8_t[1]() };

            BFWriterInput bfWriterInput;
            bfWriterInput.SetData(data);
            CHECK(bfWriterInput.GetData() == data);
        }
        SECTION("SetPixelWorldSize()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(&bfWriterInput != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(bfWriterInput.GetPixelWorldSizeX(LengthUnit::Millimenter) == 1000);
        }
        SECTION("GetPixelWorldSizeY()") {
            BFWriterInput bfWriterInput;
            bfWriterInput.SetPixelWorldSize(1, 2, LengthUnit::Meter);
            CHECK(bfWriterInput.GetPixelWorldSizeY(LengthUnit::Millimenter) == 2000);
        }
    }
}