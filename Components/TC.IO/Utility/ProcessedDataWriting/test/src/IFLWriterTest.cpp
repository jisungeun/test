#include <catch2/catch.hpp>

#include "IFLWriter.h"

namespace IFLWriterTest {
    class FLWriterForTest final : public IFLWriter {
    public:
        FLWriterForTest() = default;
        ~FLWriterForTest() = default;

        auto SetTargetFilePath(const QString& targetFilePath) -> void override {
            setTargetFilePathTriggered = true;
        }
        auto SetInput(const FLWriterInput& input) -> void override {
            setInputTriggered = true;
        }
        auto SetOutputPort(const IFLWriterOutput::Pointer& outputPort) -> void override {
            setOutputPortTriggered = true;
        }
        auto Write() -> bool override {
            writeTriggered = true;
            return false;
        }

        bool setTargetFilePathTriggered{ false };
        bool setInputTriggered{ false };
        bool writeTriggered{ false };
        bool setOutputPortTriggered{ false };
    };

    TEST_CASE("IFLWriterTest") {
        SECTION("SetTargetFilePath()") {
            FLWriterForTest flWriter;
            CHECK(flWriter.setTargetFilePathTriggered == false);
            flWriter.SetTargetFilePath("");
            CHECK(flWriter.setTargetFilePathTriggered == true);
        }
        SECTION("SetInput()") {
            FLWriterForTest flWriter;
            CHECK(flWriter.setInputTriggered == false);
            flWriter.SetInput({});
            CHECK(flWriter.setInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            FLWriterForTest flWriter;
            CHECK(flWriter.setOutputPortTriggered == false);
            flWriter.SetOutputPort({});
            CHECK(flWriter.setOutputPortTriggered == true);
        }
        SECTION("Writer()") {
            FLWriterForTest flWriter;
            CHECK(flWriter.writeTriggered == false);
            flWriter.Write();
            CHECK(flWriter.writeTriggered == true);
        }
    }
}