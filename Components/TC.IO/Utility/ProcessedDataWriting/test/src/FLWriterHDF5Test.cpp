#include <catch2/catch.hpp>
#include <QFile>

#include "H5Cpp.h"
#include "CompareArray.h"

#include "FLWriterHDF5.h"
namespace FLWriterHDF5Test {
    class FLWriterOutputForTest final : public IFLWriterOutput {
    public:
        FLWriterOutputForTest() = default;
        ~FLWriterOutputForTest() = default;

        auto SetFLWriterResult(const FLWriterResult& htWriterResult) -> void override {
            this->result = htWriterResult;
        }

        FLWriterResult result;
    };

    TEST_CASE("FLWriterHDF5 : unit test") {
        SECTION("FLWriterHDF5()") {
            FLWriterHDF5 writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            FLWriterHDF5 writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetInput()") {
            FLWriterHDF5 writer;
            writer.SetInput({});
            CHECK(&writer != nullptr);
        }
        SECTION("SetOutputPort()") {
            FLWriterHDF5 writer;
            writer.SetOutputPort({});
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "C:/Temp/FLResult.h5";

            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const float rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<float[]> data{ new float[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());

            constexpr float pixelWorldSizeX = 1.f;
            constexpr float pixelWorldSizeY = 2.f;
            constexpr float pixelWorldSizeZ = 3.f;

            FLWriterInput input;
            input.SetData(data);
            input.SetDataSize(sizeX, sizeY, sizeZ);
            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);

            auto output = new FLWriterOutputForTest;
            IFLWriterOutput::Pointer outputPort{ output };

            FLWriterHDF5 writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetInput(input);
            writer.SetOutputPort(outputPort);

            const auto writingResult = writer.Write();
            CHECK(writingResult == true);
        }
    }
    TEST_CASE("FLWriterHDF5 : practical test") {
        const QString targetFilePath = "C:/Temp/FLResult.h5";

        constexpr auto sizeX = 2;
        constexpr auto sizeY = 3;
        constexpr auto sizeZ = 2;
        constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

        const float rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

        std::shared_ptr<float[]> data{ new float[numberOfElements]() };
        std::copy_n(rawData, numberOfElements, data.get());

        constexpr float pixelWorldSizeX = 1.f;
        constexpr float pixelWorldSizeY = 2.f;
        constexpr float pixelWorldSizeZ = 3.f;

        FLWriterInput input;
        input.SetData(data);
        input.SetDataSize(sizeX, sizeY, sizeZ);
        input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);

        auto output = new FLWriterOutputForTest;
        IFLWriterOutput::Pointer outputPort{ output };

        FLWriterHDF5 writer;
        writer.SetTargetFilePath(targetFilePath);
        writer.SetInput(input);
        writer.SetOutputPort(outputPort);

        const auto writingResult = writer.Write();
        CHECK(writingResult == true);

        CHECK(QFile::exists(targetFilePath));
        CHECK(H5::H5File::isHdf5(targetFilePath.toStdString()));

        std::shared_ptr<float[]> resultData{ new float[numberOfElements] };

        int32_t resultSizeX{}, resultSizeY{}, resultSizeZ{};
        float resultPixelWorldSizeX{}, resultPixelWorldSizeY{}, resultPixelWorldSizeZ{};
        {
            H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);
            auto dataSet = file.openDataSet("Data");
            dataSet.read(resultData.get(), dataSet.getDataType());

            auto attrSizeX = dataSet.openAttribute("dataSizeX");
            auto attrSizeY = dataSet.openAttribute("dataSizeY");
            auto attrSizeZ = dataSet.openAttribute("dataSizeZ");

            auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
            auto attrPixelWorldSizeY = dataSet.openAttribute("pixelWorldSizeY");
            auto attrPixelWorldSizeZ = dataSet.openAttribute("pixelWorldSizeZ");

            attrSizeX.read(H5::PredType::NATIVE_INT32, &resultSizeX);
            attrSizeY.read(H5::PredType::NATIVE_INT32, &resultSizeY);
            attrSizeZ.read(H5::PredType::NATIVE_INT32, &resultSizeZ);

            attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &resultPixelWorldSizeX);
            attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &resultPixelWorldSizeY);
            attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &resultPixelWorldSizeZ);

            attrSizeX.close(); attrSizeY.close(); attrSizeZ.close();
            attrPixelWorldSizeX.close(); attrPixelWorldSizeY.close(); attrPixelWorldSizeZ.close();

            dataSet.close();
            file.close();
        }

        CHECK(CompareArray(resultData.get(), rawData, numberOfElements));

        CHECK(resultSizeX == sizeX);
        CHECK(resultSizeY == sizeY);
        CHECK(resultSizeZ == sizeZ);

        CHECK(resultPixelWorldSizeX == pixelWorldSizeX);
        CHECK(resultPixelWorldSizeY == pixelWorldSizeY);
        CHECK(resultPixelWorldSizeZ == pixelWorldSizeZ);

        CHECK(QFile::remove(targetFilePath));
    }
}
