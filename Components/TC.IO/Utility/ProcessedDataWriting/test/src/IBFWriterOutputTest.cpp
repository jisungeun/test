#include <catch2/catch.hpp>

#include "IBFWriterOutput.h"

namespace IBFWriterOutputTest {
    class BFWriterOutputForTest final : public IBFWriterOutput {
    public:
        BFWriterOutputForTest() = default;
        ~BFWriterOutputForTest() = default;

        auto SetBFWriterResult(const BFWriterResult& htWriterResult) -> void override {
            this->setBFWriterResultTriggered = true;
        }

        bool setBFWriterResultTriggered{ false };
    };

    TEST_CASE("IBFWriterOutputTest") {
        SECTION("SetBFWriterResult()") {
            BFWriterOutputForTest htWriterOutput;
            CHECK(htWriterOutput.setBFWriterResultTriggered == false);
            htWriterOutput.SetBFWriterResult({});
            CHECK(htWriterOutput.setBFWriterResultTriggered == true);
        }
    }
}