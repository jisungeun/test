#include <catch2/catch.hpp>

#include "IBFWriter.h"

namespace IBFWriterTest {
    class BFWriterForTest final : public IBFWriter {
    public:
        BFWriterForTest() = default;
        ~BFWriterForTest() = default;

        auto SetTargetFilePath(const QString& targetFilePath) -> void override {
            setTargetFilePathTriggered = true;
        }
        auto SetInput(const BFWriterInput& input) -> void override {
            setInputTriggered = true;
        }
        auto SetOutputPort(const IBFWriterOutput::Pointer& outputPort) -> void override {
            setOutputPortTriggered = true;
        }
        auto Write() -> bool override {
            writeTriggered = true;
            return false;
        }

        bool setTargetFilePathTriggered{ false };
        bool setInputTriggered{ false };
        bool setOutputPortTriggered{ false };
        bool writeTriggered{ false };
    };

    TEST_CASE("IBFWriterTest") {
        SECTION("SetTargetFilePath()") {
            BFWriterForTest bfWriter;
            CHECK(bfWriter.setTargetFilePathTriggered == false);
            bfWriter.SetTargetFilePath("");
            CHECK(bfWriter.setTargetFilePathTriggered == true);
        }
        SECTION("SetInput()") {
            BFWriterForTest bfWriter;
            CHECK(bfWriter.setInputTriggered == false);
            bfWriter.SetInput({});
            CHECK(bfWriter.setInputTriggered == true);
        }
        SECTION("SetOutputPort()") {
            BFWriterForTest bfWriter;
            CHECK(bfWriter.setOutputPortTriggered == false);
            bfWriter.SetOutputPort({});
            CHECK(bfWriter.setOutputPortTriggered == true);
        }
        SECTION("Writer()") {
            BFWriterForTest bfWriter;
            CHECK(bfWriter.writeTriggered == false);
            bfWriter.Write();
            CHECK(bfWriter.writeTriggered == true);
        }
    }
}