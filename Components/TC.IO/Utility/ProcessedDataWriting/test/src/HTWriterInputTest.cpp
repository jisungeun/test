#include <catch2/catch.hpp>

#include "HTWriterInput.h"

namespace HTWriterInputTest {
    TEST_CASE("HTWriterInputTest") {
        SECTION("HTWriterInput()") {
            HTWriterInput htWriterInput;
            CHECK(&htWriterInput != nullptr);
        }
        SECTION("HTWriterInput(other)") {
            HTWriterInput srcHTWriterInput;
            srcHTWriterInput.SetDataSize(1, 2, 3);

            HTWriterInput destHTWriterInput(srcHTWriterInput);
            CHECK(destHTWriterInput.GetDataSizeX() == 1);
            CHECK(destHTWriterInput.GetDataSizeY() == 2);
            CHECK(destHTWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("operator=()") {
            HTWriterInput srcHTWriterInput;
            srcHTWriterInput.SetDataSize(1, 2, 3);

            HTWriterInput destHTWriterInput;
            destHTWriterInput = srcHTWriterInput;
            CHECK(destHTWriterInput.GetDataSizeX() == 1);
            CHECK(destHTWriterInput.GetDataSizeY() == 2);
            CHECK(destHTWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("SetDataSize()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetDataSize(1, 2, 3);
            CHECK(&htWriterInput != nullptr);
        }
        SECTION("GetDataSizeX()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetDataSize(1, 2, 3);
            CHECK(htWriterInput.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetDataSize(1, 2, 3);
            CHECK(htWriterInput.GetDataSizeY() == 2);
        }
        SECTION("GetDataSizeZ()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetDataSize(1, 2, 3);
            CHECK(htWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("SetData()"){
            HTWriterInput htWriterInput;
            htWriterInput.SetData({});
            CHECK(&htWriterInput != nullptr);
        }
        SECTION("GetData()"){
            std::shared_ptr<float[]> data{ new float[1]() };

            HTWriterInput htWriterInput;
            htWriterInput.SetData(data);
            CHECK(htWriterInput.GetData() == data);
        }
        SECTION("SetPixelWorldSize()"){
            HTWriterInput htWriterInput;
            htWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(&htWriterInput != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htWriterInput.GetPixelWorldSizeX(LengthUnit::Meter) == 1);
        }
        SECTION("GetPixelWorldSizeY()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htWriterInput.GetPixelWorldSizeY(LengthUnit::Meter) == 2);
        }
        SECTION("GetPixelWorldSizeZ()") {
            HTWriterInput htWriterInput;
            htWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(htWriterInput.GetPixelWorldSizeZ(LengthUnit::Meter) == 3);
        }
    }
}