#include <catch2/catch.hpp>

#include "FLWriterInput.h"

namespace FLWriterInputTest {
    TEST_CASE("FLWriterInputTest") {
        SECTION("FLWriterInput()") {
            FLWriterInput flWriterInput;
            CHECK(&flWriterInput != nullptr);
        }
        SECTION("FLWriterInput(other)") {
            FLWriterInput srcFLWriterInput;
            srcFLWriterInput.SetDataSize(1, 2, 3);

            FLWriterInput destFLWriterInput(srcFLWriterInput);
            CHECK(destFLWriterInput.GetDataSizeX() == 1);
            CHECK(destFLWriterInput.GetDataSizeY() == 2);
            CHECK(destFLWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("operator=()") {
            FLWriterInput srcFLWriterInput;
            srcFLWriterInput.SetDataSize(1, 2, 3);

            FLWriterInput destFLWriterInput;
            destFLWriterInput = srcFLWriterInput;
            CHECK(destFLWriterInput.GetDataSizeX() == 1);
            CHECK(destFLWriterInput.GetDataSizeY() == 2);
            CHECK(destFLWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("SetDataSize()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetDataSize(1, 2, 3);
            CHECK(&flWriterInput != nullptr);
        }
        SECTION("GetDataSizeX()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetDataSize(1, 2, 3);
            CHECK(flWriterInput.GetDataSizeX() == 1);
        }
        SECTION("GetDataSizeY()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetDataSize(1, 2, 3);
            CHECK(flWriterInput.GetDataSizeY() == 2);
        }
        SECTION("GetDataSizeZ()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetDataSize(1, 2, 3);
            CHECK(flWriterInput.GetDataSizeZ() == 3);
        }
        SECTION("SetData()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetData({});
            CHECK(&flWriterInput != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<float[]> data{ new float[1]() };

            FLWriterInput flWriterInput;
            flWriterInput.SetData(data);
            CHECK(flWriterInput.GetData() == data);
        }
        SECTION("SetPixelWorldSize()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(&flWriterInput != nullptr);
        }
        SECTION("GetPixelWorldSizeX()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flWriterInput.GetPixelWorldSizeX(LengthUnit::Millimenter) == 1000);
        }
        SECTION("GetPixelWorldSizeY()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flWriterInput.GetPixelWorldSizeY(LengthUnit::Millimenter) == 2000);
        }
        SECTION("GetPixelWorldSizeZ()") {
            FLWriterInput flWriterInput;
            flWriterInput.SetPixelWorldSize(1, 2, 3, LengthUnit::Meter);
            CHECK(flWriterInput.GetPixelWorldSizeZ(LengthUnit::Millimenter) == 3000);
        }
    }
}