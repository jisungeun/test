#include <catch2/catch.hpp>
#include <QFile>

#include "H5Cpp.h"
#include "CompareArray.h"

#include "BFWriterHDF5.h"
namespace BFWriterHDF5Test {
    class BFWriterOutputForTest final : public IBFWriterOutput {
    public:
        BFWriterOutputForTest() = default;
        ~BFWriterOutputForTest() = default;

        auto SetBFWriterResult(const BFWriterResult& htWriterResult) -> void override {
            this->result = htWriterResult;
        }

        BFWriterResult result;
    };

    TEST_CASE("BFWriterHDF5 : unit test") {
        SECTION("BFWriterHDF5()") {
            BFWriterHDF5 writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            BFWriterHDF5 writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetInput()") {
            BFWriterHDF5 writer;
            writer.SetInput({});
            CHECK(&writer != nullptr);
        }
        SECTION("SetOutputPort()") {
            BFWriterHDF5 writer;
            writer.SetOutputPort({});
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "C:/Temp/BFResult.h5";

            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            constexpr uint8_t rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());

            constexpr float pixelWorldSizeX = 1.f;
            constexpr float pixelWorldSizeY = 2.f;

            BFWriterInput input;
            input.SetData(data);
            input.SetDataSize(sizeX, sizeY);
            input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);

            auto output = new BFWriterOutputForTest;
            IBFWriterOutput::Pointer outputPort{ output };

            BFWriterHDF5 writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetInput(input);
            writer.SetOutputPort(outputPort);

            const auto writingResult = writer.Write();
            CHECK(writingResult == true);
        }
    }
    TEST_CASE("BFWriterHDF5 : practical test") {
        const QString targetFilePath = "C:/Temp/BFResult.h5";

        constexpr auto sizeX = 2;
        constexpr auto sizeY = 3;
        constexpr auto numberOfElements = sizeX * sizeY;

        constexpr uint8_t rawData[numberOfElements] = { 1,2,3,4,5,6 };

        std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
        std::copy_n(rawData, numberOfElements, data.get());

        constexpr float pixelWorldSizeX = 1.f;
        constexpr float pixelWorldSizeY = 2.f;

        BFWriterInput input;
        input.SetData(data);
        input.SetDataSize(sizeX, sizeY);
        input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);

        auto output = new BFWriterOutputForTest;
        IBFWriterOutput::Pointer outputPort{ output };

        BFWriterHDF5 writer;
        writer.SetTargetFilePath(targetFilePath);
        writer.SetInput(input);
        writer.SetOutputPort(outputPort);

        const auto writingResult = writer.Write();
        CHECK(writingResult == true);

        CHECK(QFile::exists(targetFilePath));
        CHECK(H5::H5File::isHdf5(targetFilePath.toStdString()));

        std::shared_ptr<uint8_t[]> resultData{ new uint8_t[numberOfElements] };

        int32_t resultSizeX{}, resultSizeY{};
        float resultPixelWorldSizeX{}, resultPixelWorldSizeY{};
        {
            H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);
            auto dataSet = file.openDataSet("Data");
            dataSet.read(resultData.get(), dataSet.getDataType());

            auto attrSizeX = dataSet.openAttribute("dataSizeX");
            auto attrSizeY = dataSet.openAttribute("dataSizeY");

            auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
            auto attrPixelWorldSizeY = dataSet.openAttribute("pixelWorldSizeY");

            attrSizeX.read(H5::PredType::NATIVE_INT32, &resultSizeX);
            attrSizeY.read(H5::PredType::NATIVE_INT32, &resultSizeY);

            attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &resultPixelWorldSizeX);
            attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &resultPixelWorldSizeY);

            attrSizeX.close(); attrSizeY.close();
            attrPixelWorldSizeX.close(); attrPixelWorldSizeY.close();

            dataSet.close();
            file.close();
        }

        CHECK(CompareArray(resultData.get(), rawData, numberOfElements));

        CHECK(resultSizeX == sizeX);
        CHECK(resultSizeY == sizeY);

        CHECK(resultPixelWorldSizeX == pixelWorldSizeX);
        CHECK(resultPixelWorldSizeY == pixelWorldSizeY);

        CHECK(QFile::remove(targetFilePath));
    }
}
