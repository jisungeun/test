#include <catch2/catch.hpp>

#include "FLWriterResult.h"

namespace FLWriterResultTest {
    TEST_CASE("FLWriterResultTest") {
        SECTION("FLWriterResult()") {
            FLWriterResult flWriterResult;
            CHECK(&flWriterResult != nullptr);
        }
        SECTION("FLWriterResult(other)") {
            FLWriterResult srcFLWriterResult;
            srcFLWriterResult.SetSuccessFlag(true);

            FLWriterResult destFLWriterResult(srcFLWriterResult);
            CHECK(destFLWriterResult.GetSuccessFlag() == true);
        }
        SECTION("operator=()") {
            FLWriterResult srcFLWriterResult;
            srcFLWriterResult.SetSuccessFlag(true);

            FLWriterResult destFLWriterResult;
            destFLWriterResult = srcFLWriterResult;
            CHECK(destFLWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetSuccessFlag()") {
            FLWriterResult flWriterResult;
            flWriterResult.SetSuccessFlag(true);
            CHECK(&flWriterResult != nullptr);
        }
        SECTION("GetSuccessFlag()") {
            FLWriterResult flWriterResult;
            flWriterResult.SetSuccessFlag(true);
            CHECK(flWriterResult.GetSuccessFlag() == true);
        }
        SECTION("SetWrittenFilePath()") {
            FLWriterResult flWriterResult;
            flWriterResult.SetWrittenFilePath("");
            CHECK(&flWriterResult != nullptr);
        }
        SECTION("GetWrittenFilePath()") {
            FLWriterResult flWriterResult;
            flWriterResult.SetWrittenFilePath("test");
            CHECK(flWriterResult.GetWrittenFilePath() == "test");
        }

    }
}