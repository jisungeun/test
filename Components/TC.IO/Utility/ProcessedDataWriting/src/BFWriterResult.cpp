#include "BFWriterResult.h"


class BFWriterResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool successFlag{ false };
    QString writtenFilePath{};
};

BFWriterResult::BFWriterResult() : d(new Impl()) {
}

BFWriterResult::BFWriterResult(const BFWriterResult& other) : d(new Impl(*other.d)) {
}

BFWriterResult::~BFWriterResult() = default;

auto BFWriterResult::operator=(const BFWriterResult & other)->BFWriterResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto BFWriterResult::SetSuccessFlag(const bool& successFlag) -> void {
    d->successFlag = successFlag;
}

auto BFWriterResult::GetSuccessFlag() const -> const bool& {
    return d->successFlag;
}

auto BFWriterResult::SetWrittenFilePath(const QString & writtenFilePath) -> void {
    d->writtenFilePath = writtenFilePath;
}

auto BFWriterResult::GetWrittenFilePath() const -> const QString& {
    return d->writtenFilePath;
}
