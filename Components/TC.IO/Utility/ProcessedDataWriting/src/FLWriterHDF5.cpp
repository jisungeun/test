#include "FLWriterHDF5.h"

#include "H5Cpp.h"
#include "HDF5Mutex.h"

class FLWriterHDF5::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetFilePath{};
    FLWriterInput input{};
    IFLWriterOutput::Pointer outputPort{};
};

FLWriterHDF5::FLWriterHDF5() : d(new Impl()) {
}

FLWriterHDF5::~FLWriterHDF5() = default;

auto FLWriterHDF5::SetTargetFilePath(const QString & targetFilePath) -> void {
    d->targetFilePath = targetFilePath;
}

auto FLWriterHDF5::SetInput(const FLWriterInput & input) -> void {
    d->input = input;
}

auto FLWriterHDF5::SetOutputPort(const IFLWriterOutput::Pointer & outputPort) -> void {
    d->outputPort = outputPort;
}

auto FLWriterHDF5::Write() -> bool {
    FLWriterResult result;
    try {
        const H5::H5File file(d->targetFilePath.toStdString(), H5F_ACC_TRUNC);

        constexpr auto rank = 3;
        std::shared_ptr<hsize_t[]> dimensions{ new hsize_t[rank]() };
        dimensions[0] = d->input.GetDataSizeX();
        dimensions[1] = d->input.GetDataSizeY();
        dimensions[2] = d->input.GetDataSizeZ();

        auto dataSetDataType = H5::PredType::NATIVE_FLOAT;
        auto dataSetDataSpace = H5::DataSpace(rank, dimensions.get());

        const auto dataSet = file.createDataSet("Data", dataSetDataType, dataSetDataSpace);
        dataSet.write(d->input.GetData().get(), dataSetDataType);

        dataSetDataType.close();
        dataSetDataSpace.close();

        const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);

        auto attDataSizeX = dataSet.createAttribute("dataSizeX", H5::PredType::NATIVE_INT32, attributeDataSpace);
        auto attDataSizeY = dataSet.createAttribute("dataSizeY", H5::PredType::NATIVE_INT32, attributeDataSpace);
        auto attDataSizeZ = dataSet.createAttribute("dataSizeZ", H5::PredType::NATIVE_INT32, attributeDataSpace);

        attDataSizeX.write(H5::PredType::NATIVE_INT32, &d->input.GetDataSizeX());
        attDataSizeY.write(H5::PredType::NATIVE_INT32, &d->input.GetDataSizeY());
        attDataSizeZ.write(H5::PredType::NATIVE_INT32, &d->input.GetDataSizeZ());

        attDataSizeX.close();
        attDataSizeY.close();
        attDataSizeZ.close();

        const auto attPixelWorldSizeX = dataSet.createAttribute("pixelWorldSizeX", H5::PredType::NATIVE_FLOAT, attributeDataSpace);
        const auto attPixelWorldSizeY = dataSet.createAttribute("pixelWorldSizeY", H5::PredType::NATIVE_FLOAT, attributeDataSpace);
        const auto attPixelWorldSizeZ = dataSet.createAttribute("pixelWorldSizeZ", H5::PredType::NATIVE_FLOAT, attributeDataSpace);

        const auto pixelWorldSizeX = d->input.GetPixelWorldSizeX(LengthUnit::Micrometer);
        const auto pixelWorldSizeY = d->input.GetPixelWorldSizeY(LengthUnit::Micrometer);
        const auto pixelWorldSizeZ = d->input.GetPixelWorldSizeZ(LengthUnit::Micrometer);

        attPixelWorldSizeX.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attPixelWorldSizeY.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
        attPixelWorldSizeZ.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);

        const auto attDone = dataSet.createAttribute("done", H5::PredType::NATIVE_INT, attributeDataSpace);
        constexpr auto doneFlag = 1;
        attDone.write(H5::PredType::NATIVE_INT, &doneFlag);

        result.SetSuccessFlag(true);
        result.SetWrittenFilePath(d->targetFilePath);

        d->outputPort->SetFLWriterResult(result);
    } catch (const H5::Exception&) {
        result.SetSuccessFlag(false);

        d->outputPort->SetFLWriterResult(result);
        return false;
    }

    return true;
}
