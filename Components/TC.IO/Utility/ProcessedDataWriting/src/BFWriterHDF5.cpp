#include "BFWriterHDF5.h"

#include "H5Cpp.h"

class BFWriterHDF5::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetFilePath{};
    BFWriterInput input{};
    IBFWriterOutput::Pointer outputPort{};
};

BFWriterHDF5::BFWriterHDF5() : d(new Impl()) {
}

BFWriterHDF5::~BFWriterHDF5() = default;

auto BFWriterHDF5::SetTargetFilePath(const QString & targetFilePath) -> void {
    d->targetFilePath = targetFilePath;
}

auto BFWriterHDF5::SetInput(const BFWriterInput & input) -> void {
    d->input = input;
}

auto BFWriterHDF5::SetOutputPort(const IBFWriterOutput::Pointer & outputPort) -> void {
    d->outputPort = outputPort;
}

auto BFWriterHDF5::Write() -> bool {
    BFWriterResult result;
    try {
        const H5::H5File file(d->targetFilePath.toStdString(), H5F_ACC_TRUNC);

        constexpr auto rank = 3;
        const std::shared_ptr<hsize_t[]> dimensions{ new hsize_t[rank]() };
        dimensions[0] = d->input.GetDataSizeX();
        dimensions[1] = d->input.GetDataSizeY();
        dimensions[2] = d->input.GetChannelCount();

        const auto dataSetDataType = H5::PredType::NATIVE_UINT8;
        const auto dataSetDataSpace = H5::DataSpace(rank, dimensions.get());

        const auto dataSet = file.createDataSet("Data", dataSetDataType, dataSetDataSpace);
        dataSet.write(d->input.GetData().get(), dataSetDataType);

        const auto attributeDataSpace = H5::DataSpace(H5S_SCALAR);

        const auto attDataSizeX = dataSet.createAttribute("dataSizeX", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attDataSizeY = dataSet.createAttribute("dataSizeY", H5::PredType::NATIVE_INT32, attributeDataSpace);
        const auto attChannelCount = dataSet.createAttribute("channelCount", H5::PredType::NATIVE_INT32, attributeDataSpace);

        attDataSizeX.write(H5::PredType::NATIVE_INT32, &d->input.GetDataSizeX());
        attDataSizeY.write(H5::PredType::NATIVE_INT32, &d->input.GetDataSizeY());
        attChannelCount.write(H5::PredType::NATIVE_INT32, &d->input.GetChannelCount());

        const auto attPixelWorldSizeX = dataSet.createAttribute("pixelWorldSizeX", H5::PredType::NATIVE_FLOAT, attributeDataSpace);
        const auto attPixelWorldSizeY = dataSet.createAttribute("pixelWorldSizeY", H5::PredType::NATIVE_FLOAT, attributeDataSpace);

        const auto pixelWorldSizeX = d->input.GetPixelWorldSizeX(LengthUnit::Micrometer);
        const auto pixelWorldSizeY = d->input.GetPixelWorldSizeY(LengthUnit::Micrometer);

        attPixelWorldSizeX.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attPixelWorldSizeY.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);

        const auto attDone = dataSet.createAttribute("done", H5::PredType::NATIVE_INT, attributeDataSpace);
        constexpr auto doneFlag = 1;
        attDone.write(H5::PredType::NATIVE_INT, &doneFlag);

        result.SetSuccessFlag(true);
        result.SetWrittenFilePath(d->targetFilePath);

        d->outputPort->SetBFWriterResult(result);
    } catch (const H5::Exception&) {
        result.SetSuccessFlag(false);

        d->outputPort->SetBFWriterResult(result);
        return false;
    }

    return true;
}
