#include "FLWriterResult.h"

class FLWriterResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool successFlag{ false };
    QString writtenFilePath{};
};

FLWriterResult::FLWriterResult() : d(new Impl()) {
}

FLWriterResult::FLWriterResult(const FLWriterResult& other) : d(new Impl(*other.d)) {
}

FLWriterResult::~FLWriterResult() = default;

auto FLWriterResult::operator=(const FLWriterResult & other)->FLWriterResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLWriterResult::SetSuccessFlag(const bool& successFlag) -> void {
    d->successFlag = successFlag;
}

auto FLWriterResult::GetSuccessFlag() const -> const bool& {
    return d->successFlag;
}

auto FLWriterResult::SetWrittenFilePath(const QString & writtenFilePath) -> void {
    d->writtenFilePath = writtenFilePath;
}

auto FLWriterResult::GetWrittenFilePath() const -> const QString& {
    return d->writtenFilePath;
}
