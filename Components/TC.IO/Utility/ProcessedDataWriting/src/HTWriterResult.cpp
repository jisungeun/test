#include "HTWriterResult.h"


class HTWriterResult::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool successFlag{ false };
    QString writtenFilePath{};
};

HTWriterResult::HTWriterResult() : d(new Impl()) {
}

HTWriterResult::HTWriterResult(const HTWriterResult& other) : d(new Impl(*other.d)) {
}

HTWriterResult::~HTWriterResult() = default;

auto HTWriterResult::operator=(const HTWriterResult& other)->HTWriterResult& {
    *(this->d) = *(other.d);
    return *this;
}

auto HTWriterResult::SetSuccessFlag(const bool& successFlag) -> void {
    d->successFlag = successFlag;
}

auto HTWriterResult::GetSuccessFlag() const -> const bool& {
    return d->successFlag;
}

auto HTWriterResult::SetWrittenFilePath(const QString& writtenFilePath) -> void {
    d->writtenFilePath = writtenFilePath;
}

auto HTWriterResult::GetWrittenFilePath() const -> const QString& {
    return d->writtenFilePath;
}
