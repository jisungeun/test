#include "HTWriterInput.h"

class HTWriterInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    std::shared_ptr<float[]> data;

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    float pixelWorldSizeZ{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };
};

HTWriterInput::HTWriterInput() : d(new Impl()) {
}

HTWriterInput::HTWriterInput(const HTWriterInput& other) : d(new Impl(*other.d)) {
}

HTWriterInput::~HTWriterInput() = default;

auto HTWriterInput::operator=(const HTWriterInput& other) -> HTWriterInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto HTWriterInput::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
    d->dataSizeZ = sizeZ;
}

auto HTWriterInput::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto HTWriterInput::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto HTWriterInput::GetDataSizeZ() const -> const int32_t& {
    return d->dataSizeZ;
}

auto HTWriterInput::SetData(const std::shared_ptr<float[]>& data) -> void {
    d->data = data;
}

auto HTWriterInput::GetData() const -> const std::shared_ptr<float[]>& {
    return d->data;
}

auto HTWriterInput::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const float& pixelWorldSizeZ, const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeZ = pixelWorldSizeZ;
    d->pixelWorldSizeUnit = unit;
}

auto HTWriterInput::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto HTWriterInput::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}

auto HTWriterInput::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit));
}
