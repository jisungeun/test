#include "BFWriterInput.h"

class BFWriterInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t channelCount{};

    std::shared_ptr<uint8_t[]> data;

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };
};

BFWriterInput::BFWriterInput() : d(new Impl()) {
}

BFWriterInput::BFWriterInput(const BFWriterInput& other) : d(new Impl(*other.d)) {
}

BFWriterInput::~BFWriterInput() = default;

auto BFWriterInput::operator=(const BFWriterInput & other) -> BFWriterInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto BFWriterInput::SetDataSize(const int32_t & sizeX, const int32_t & sizeY) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
}

auto BFWriterInput::SetChannelCount(const int32_t& channelCount) -> void {
    d->channelCount = channelCount;
}

auto BFWriterInput::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto BFWriterInput::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto BFWriterInput::GetChannelCount() const -> const int32_t& {
    return d->channelCount;
}

auto BFWriterInput::SetData(const std::shared_ptr<uint8_t[]>&data) -> void {
    d->data = data;
}

auto BFWriterInput::GetData() const -> const std::shared_ptr<uint8_t[]>& {
    return d->data;
}

auto BFWriterInput::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY, 
    const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeUnit = unit;
}

auto BFWriterInput::GetPixelWorldSizeX(const LengthUnit & unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto BFWriterInput::GetPixelWorldSizeY(const LengthUnit & unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}
