#include "FLWriterInput.h"

class FLWriterInput::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    std::shared_ptr<float[]> data;

    float pixelWorldSizeX{};
    float pixelWorldSizeY{};
    float pixelWorldSizeZ{};
    LengthUnit pixelWorldSizeUnit{ LengthUnit::Meter };
};

FLWriterInput::FLWriterInput() : d(new Impl()) {
}

FLWriterInput::FLWriterInput(const FLWriterInput& other) : d(new Impl(*other.d)) {
}

FLWriterInput::~FLWriterInput() = default;

auto FLWriterInput::operator=(const FLWriterInput& other) -> FLWriterInput& {
    *(this->d) = *(other.d);
    return *this;
}

auto FLWriterInput::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->dataSizeX = sizeX;
    d->dataSizeY = sizeY;
    d->dataSizeZ = sizeZ;
}

auto FLWriterInput::GetDataSizeX() const -> const int32_t& {
    return d->dataSizeX;
}

auto FLWriterInput::GetDataSizeY() const -> const int32_t& {
    return d->dataSizeY;
}

auto FLWriterInput::GetDataSizeZ() const -> const int32_t& {
    return d->dataSizeZ;
}

auto FLWriterInput::SetData(const std::shared_ptr<float[]>& data) -> void {
    d->data = data;
}

auto FLWriterInput::GetData() const -> const std::shared_ptr<float[]>& {
    return d->data;
}

auto FLWriterInput::SetPixelWorldSize(const float& pixelWorldSizeX, const float& pixelWorldSizeY,
    const float& pixelWorldSizeZ, const LengthUnit& unit) -> void {
    d->pixelWorldSizeX = pixelWorldSizeX;
    d->pixelWorldSizeY = pixelWorldSizeY;
    d->pixelWorldSizeZ = pixelWorldSizeZ;
    d->pixelWorldSizeUnit = unit;
}

auto FLWriterInput::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit));
}

auto FLWriterInput::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit));
}

auto FLWriterInput::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
    return static_cast<float>(ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit));
}
