#include <catch2/catch.hpp>

#include "AcquisitionSequenceWriterIni.h"
#include "AcquisitionSequenceReaderIni.h"

namespace AcquisitionSequenceReaderIniTest {
    using namespace TC::IO::AcquisitionSequence;

    auto WriteAcquisitionSequence(const QString& iniFilePath, const AcquisitionSequence& acquisitionSequence)->void {
        AcquisitionSequenceWriterIni acquisitionSequenceWriterIni;
        acquisitionSequenceWriterIni.SetIniFilePath(iniFilePath);
        acquisitionSequenceWriterIni.SetAcquisitionSequence(acquisitionSequence);

        acquisitionSequenceWriterIni.Write();
    }

    TEST_CASE("AcquisitionSequenceReaderIni : unit test") {
        SECTION("AcquisitionSequenceIni()") {
            AcquisitionSequenceReaderIni acquisitionSequenceReaderIni;
            CHECK(&acquisitionSequenceReaderIni != nullptr);
        }
        SECTION("SetIniFilePath()") {
            AcquisitionSequenceReaderIni acquisitionSequenceReaderIni;
            acquisitionSequenceReaderIni.SetIniFilePath("");
            CHECK(&acquisitionSequenceReaderIni != nullptr);
        }
        SECTION("Read()") {
            AcquisitionFrame acquisitionFrame0;
            acquisitionFrame0.AddInfo({ Modality::HT, AcquisitionType::Dimension3 });
            acquisitionFrame0.AddInfo({ Modality::BF, AcquisitionType::Color });

            AcquisitionFrame acquisitionFrame2;
            acquisitionFrame2.AddInfo({ Modality::FLCH0, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH1, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH2, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH3, AcquisitionType::Dimension2 });

            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.SetTimeFrameCount(3);
            acquisitionSequence.AddFrame(0, acquisitionFrame0);
            acquisitionSequence.AddFrame(2, acquisitionFrame2);
            
            const QString iniFilePath = "AcquisitionSequenceReader.ini";

            WriteAcquisitionSequence(iniFilePath, acquisitionSequence);
            
            AcquisitionSequenceReaderIni acquisitionSequenceReaderIni;
            acquisitionSequenceReaderIni.SetIniFilePath(iniFilePath);
            CHECK(acquisitionSequenceReaderIni.Read() == true);
        }
        SECTION("GetAcquisitionSequence()") {
            AcquisitionFrame acquisitionFrame0;
            acquisitionFrame0.AddInfo({ Modality::HT, AcquisitionType::Dimension3 });
            acquisitionFrame0.AddInfo({ Modality::BF, AcquisitionType::Color });

            AcquisitionFrame acquisitionFrame2;
            acquisitionFrame2.AddInfo({ Modality::FLCH0, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH1, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH2, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH3, AcquisitionType::Dimension2 });

            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.SetTimeFrameCount(3);
            acquisitionSequence.AddFrame(0, acquisitionFrame0);
            acquisitionSequence.AddFrame(2, acquisitionFrame2);

            const QString iniFilePath = "AcquisitionSequenceReader.ini";

            WriteAcquisitionSequence(iniFilePath, acquisitionSequence);

            AcquisitionSequenceReaderIni acquisitionSequenceReaderIni;
            acquisitionSequenceReaderIni.SetIniFilePath(iniFilePath);
            acquisitionSequenceReaderIni.Read();

            const auto resultAcquisitionSequence = acquisitionSequenceReaderIni.GetAcquisitionSequence();
            CHECK(resultAcquisitionSequence.GetTimeFrameCount() == 3);
            CHECK(resultAcquisitionSequence.ExistFrame(0) == true);
            CHECK(resultAcquisitionSequence.ExistFrame(1) == false);
            CHECK(resultAcquisitionSequence.ExistFrame(2) == true);

            const auto resultAcquisitionFrame0 = resultAcquisitionSequence.GetFrame(0);
            const auto& acquisitionInfoList0 = resultAcquisitionFrame0.GetInfoList();
            CHECK(acquisitionInfoList0.size() == 2);
            CHECK(acquisitionInfoList0.at(0).modality == +Modality::HT);
            CHECK(acquisitionInfoList0.at(0).type == +AcquisitionType::Dimension3);
            CHECK(acquisitionInfoList0.at(1).modality == +Modality::BF);
            CHECK(acquisitionInfoList0.at(1).type == +AcquisitionType::Color);

            const auto resultAcquisitionFrame2 = resultAcquisitionSequence.GetFrame(2);
            const auto& acquisitionInfoList2 = resultAcquisitionFrame2.GetInfoList();
            CHECK(acquisitionInfoList2.size() == 4);
            CHECK(acquisitionInfoList2.at(0).modality == +Modality::FLCH0);
            CHECK(acquisitionInfoList2.at(0).type == +AcquisitionType::Dimension2);
            CHECK(acquisitionInfoList2.at(1).modality == +Modality::FLCH1);
            CHECK(acquisitionInfoList2.at(1).type == +AcquisitionType::Dimension2);
            CHECK(acquisitionInfoList2.at(2).modality == +Modality::FLCH2);
            CHECK(acquisitionInfoList2.at(2).type == +AcquisitionType::Dimension2);
            CHECK(acquisitionInfoList2.at(3).modality == +Modality::FLCH3);
            CHECK(acquisitionInfoList2.at(3).type == +AcquisitionType::Dimension2);
        }
    }

    TEST_CASE("AcquisitionSequenceReaderIni : practical test") {
        //TODO Implement test
    }
}
