#include <catch2/catch.hpp>

#include "IAcquisitionSequenceWriter.h"

namespace IAcquisitionSequenceWriterTest {
    using namespace TC::IO::AcquisitionSequence;

    class AcquisitionSequenceWriterForTest final : public IAcquisitionSequenceWriter {
    public:
        AcquisitionSequenceWriterForTest() = default;
        ~AcquisitionSequenceWriterForTest() = default;

        auto SetAcquisitionSequence(const AcquisitionSequence& acquisitionSequence) -> void override {
            this->setAcquisitionSequenceTriggered = true;
        }
        auto Write() -> bool override {
            this->writeTriggered = true;
            return false;
        }

        bool setAcquisitionSequenceTriggered{ false };
        bool writeTriggered{ false };
    };

    TEST_CASE("IAcquisitionSequenceWriter : unit test") {
        SECTION("SetAcquisitionSequence()") {
            AcquisitionSequenceWriterForTest acquisitionSequenceWriter;
            CHECK(acquisitionSequenceWriter.setAcquisitionSequenceTriggered == false);
            acquisitionSequenceWriter.SetAcquisitionSequence({});
            CHECK(acquisitionSequenceWriter.setAcquisitionSequenceTriggered == true);
        }
        SECTION("Write()") {
            AcquisitionSequenceWriterForTest acquisitionSequenceWriter;
            CHECK(acquisitionSequenceWriter.writeTriggered == false);
            const auto result = acquisitionSequenceWriter.Write();
            CHECK(acquisitionSequenceWriter.writeTriggered == true);
        }
    }
}