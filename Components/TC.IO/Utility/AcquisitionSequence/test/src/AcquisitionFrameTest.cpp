#include <catch2/catch.hpp>

#include "AcquisitionFrame.h"

namespace AcquisitionFrameTest {
    using namespace TC::IO::AcquisitionSequence;

    TEST_CASE("Modality enum") {
        CHECK(Modality::_size() == 6);
        CHECK(Modality::_from_index(0) == +Modality::HT);
        CHECK(Modality::_from_index(1) == +Modality::FLCH0);
        CHECK(Modality::_from_index(2) == +Modality::FLCH1);
        CHECK(Modality::_from_index(3) == +Modality::FLCH2);
        CHECK(Modality::_from_index(4) == +Modality::FLCH3);
        CHECK(Modality::_from_index(5) == +Modality::BF);
    }

    TEST_CASE("AcquisitionType enum") {
        CHECK(AcquisitionType::_size() == 4);
        CHECK(AcquisitionType::_from_index(0) == +AcquisitionType::Dimension3);
        CHECK(AcquisitionType::_from_index(1) == +AcquisitionType::Dimension2);
        CHECK(AcquisitionType::_from_index(2) == +AcquisitionType::Gray);
        CHECK(AcquisitionType::_from_index(3) == +AcquisitionType::Color);
    }

    TEST_CASE("AcquisitionFrame : unit test") {
        SECTION("AcquisitionInfo : unit test") {
            SECTION("operator==()") {
                AcquisitionFrame::AcquisitionInfo acquisitionInfo1;
                acquisitionInfo1.modality = Modality::BF;
                acquisitionInfo1.type = AcquisitionType::Gray;

                AcquisitionFrame::AcquisitionInfo acquisitionInfo2;
                acquisitionInfo2.modality = Modality::BF;
                acquisitionInfo2.type = AcquisitionType::Gray;

                CHECK((acquisitionInfo1 == acquisitionInfo2) == true);
            }
            SECTION("operator!=()") {
                AcquisitionFrame::AcquisitionInfo acquisitionInfo1;
                acquisitionInfo1.modality = Modality::BF;
                acquisitionInfo1.type = AcquisitionType::Gray;

                SECTION("same") {
                    AcquisitionFrame::AcquisitionInfo acquisitionInfo2;
                    acquisitionInfo2.modality = Modality::BF;
                    acquisitionInfo2.type = AcquisitionType::Gray;
                    CHECK((acquisitionInfo1 != acquisitionInfo2) == false);
                }
                SECTION("modality different") {
                    AcquisitionFrame::AcquisitionInfo acquisitionInfo2;
                    acquisitionInfo2.modality = Modality::HT;
                    acquisitionInfo2.type = AcquisitionType::Gray;
                    CHECK((acquisitionInfo1 != acquisitionInfo2) == true);
                }
                SECTION("type different") {
                    AcquisitionFrame::AcquisitionInfo acquisitionInfo2;
                    acquisitionInfo2.modality = Modality::BF;
                    acquisitionInfo2.type = AcquisitionType::Color;
                    CHECK((acquisitionInfo1 != acquisitionInfo2) == true);
                }
            }
        }

        SECTION("AcquisitionFrame()") {
            AcquisitionFrame acquisitionFrame;
            CHECK(&acquisitionFrame != nullptr);
        }
        SECTION("AcquisitionFrame(other)") {
            AcquisitionFrame srcAcquisitionFrame;
            srcAcquisitionFrame.AddInfo(AcquisitionFrame::AcquisitionInfo{ Modality::BF,AcquisitionType::Color });

            AcquisitionFrame destAcquisitionFrame(srcAcquisitionFrame);
            const auto& frameInfoList = destAcquisitionFrame.GetInfoList();

            CHECK(frameInfoList.size() == 1);
            CHECK(frameInfoList.first().modality == +Modality::BF);
            CHECK(frameInfoList.first().type == +AcquisitionType::Color);
        }
        SECTION("AcquisitionFrame(acquisitionInfoList)") {
            QList<AcquisitionFrame::AcquisitionInfo> srcInfoList{{Modality::BF, AcquisitionType::Color}};

            AcquisitionFrame acquisitionFrame(srcInfoList);
            const auto& frameInfoList = acquisitionFrame.GetInfoList();

            CHECK(frameInfoList.size() == 1);
            CHECK(frameInfoList.first().modality == +Modality::BF);
            CHECK(frameInfoList.first().type == +AcquisitionType::Color);
        }
        SECTION("operator=()") {
            AcquisitionFrame srcAcquisitionFrame;
            srcAcquisitionFrame.AddInfo(AcquisitionFrame::AcquisitionInfo{ Modality::BF, AcquisitionType::Color });

            AcquisitionFrame destAcquisitionFrame;
            destAcquisitionFrame = srcAcquisitionFrame;
            const auto& infoList = destAcquisitionFrame.GetInfoList();

            CHECK(infoList.size() == 1);
            CHECK(infoList.first().modality == +Modality::BF);
            CHECK(infoList.first().type == +AcquisitionType::Color);
        }
        SECTION("AddInfo()") {
            AcquisitionFrame::AcquisitionInfo acquisitionInfo{ Modality::BF,AcquisitionType::Color };

            AcquisitionFrame acquisitionFrame;
            acquisitionFrame.AddInfo(acquisitionInfo);

            CHECK(&acquisitionFrame != nullptr);
        }
        SECTION("SetInfoList()") {
            QList<AcquisitionFrame::AcquisitionInfo> infoList{ {Modality::BF, AcquisitionType::Color} };

            AcquisitionFrame acquisitionFrame;
            acquisitionFrame.SetInfoList(infoList);

            CHECK(&acquisitionFrame != nullptr);
        }
        SECTION("GetInfoList()") {
            QList<AcquisitionFrame::AcquisitionInfo> srcInfoList{ {Modality::BF, AcquisitionType::Color} };

            AcquisitionFrame acquisitionFrame;
            acquisitionFrame.SetInfoList(srcInfoList);

            const auto& infoList = acquisitionFrame.GetInfoList();

            CHECK(infoList.size() == 1);
            CHECK(infoList.first().modality == +Modality::BF);
            CHECK(infoList.first().type == +AcquisitionType::Color);
        }
    }

    TEST_CASE("AcquisitionFrame : practical test") {
        QList<AcquisitionFrame::AcquisitionInfo> infoList{
            {Modality::HT, AcquisitionType::Dimension3},
            {Modality::FLCH0, AcquisitionType::Dimension2},
            {Modality::BF, AcquisitionType::Gray}
        };

        SECTION("method 1 : using constructor") {
            AcquisitionFrame acquisitionFrame{ infoList };

            const auto& resultInfoList = acquisitionFrame.GetInfoList();
            CHECK(resultInfoList.size() == 3);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::HT, AcquisitionType::Dimension3 }) == true);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::FLCH0, AcquisitionType::Dimension2 }) == true);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::BF, AcquisitionType::Gray }) == true);
        }

        SECTION("method 2 : setting info") {
            AcquisitionFrame acquisitionFrame;
            for(const auto& info : infoList) {
                acquisitionFrame.AddInfo(info);
            }

            const auto& resultInfoList = acquisitionFrame.GetInfoList();
            CHECK(resultInfoList.size() == 3);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::HT, AcquisitionType::Dimension3 }) == true);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::FLCH0, AcquisitionType::Dimension2 }) == true);
            CHECK(resultInfoList.contains(AcquisitionFrame::AcquisitionInfo{ Modality::BF, AcquisitionType::Gray }) == true);
        }
    }
}