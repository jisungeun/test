#include <catch2/catch.hpp>

#include <QFile>
#include <QTextStream>
#include <QSettings>

#include "AcquisitionSequenceWriterIni.h"

namespace AcquisitionSequenceWriterIniTest {
    using namespace TC::IO::AcquisitionSequence;

    TEST_CASE("AcquisitionSequenceWriterIni : unit test") {
        SECTION("AcquisitionSequenceWriterIni()") {
            AcquisitionSequenceWriterIni acquisitionSequenceWriterIni;
            CHECK(&acquisitionSequenceWriterIni != nullptr);
        }
        SECTION("SetIniFilePath()") {
            AcquisitionSequenceWriterIni acquisitionSequenceWriterIni;
            acquisitionSequenceWriterIni.SetIniFilePath("");
            CHECK(&acquisitionSequenceWriterIni != nullptr);
        }
        SECTION("SetAcquisitionSequence()") {
            AcquisitionSequenceWriterIni acquisitionSequenceWriterIni;
            acquisitionSequenceWriterIni.SetAcquisitionSequence({});
            CHECK(&acquisitionSequenceWriterIni != nullptr);
        }
        SECTION("Write()") {
            const QString iniFilePath = "AcquisitionSequenceWriterTest.ini";

            AcquisitionFrame acquisitionFrame0;
            acquisitionFrame0.AddInfo({ Modality::HT, AcquisitionType::Dimension3 });
            acquisitionFrame0.AddInfo({ Modality::BF, AcquisitionType::Color });

            AcquisitionFrame acquisitionFrame2;
            acquisitionFrame2.AddInfo({ Modality::FLCH0, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH1, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH2, AcquisitionType::Dimension2 });
            acquisitionFrame2.AddInfo({ Modality::FLCH3, AcquisitionType::Dimension2 });

            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.SetTimeFrameCount(3);
            acquisitionSequence.AddFrame(0, acquisitionFrame0);
            acquisitionSequence.AddFrame(2, acquisitionFrame2);
            
            AcquisitionSequenceWriterIni acquisitionSequenceWriterIni;
            acquisitionSequenceWriterIni.SetIniFilePath(iniFilePath);
            acquisitionSequenceWriterIni.SetAcquisitionSequence(acquisitionSequence);
            CHECK(acquisitionSequenceWriterIni.Write());

            CHECK(QFile::exists(iniFilePath) == true);


            QFile file(iniFilePath);
            file.open(QIODevice::ReadOnly);
            QTextStream in(&file);

            QStringList contentsByLine;
            while (!in.atEnd()) {
                QString line = in.readLine();
                contentsByLine.push_back(line);
            }

            file.close();

            CHECK(contentsByLine.size() == 11);
            CHECK(contentsByLine[0] == "[AcquisitionSequence]");
            CHECK(contentsByLine[1] == "TimeFrameCount=3");
            CHECK(contentsByLine[2] == "0000\\InfoCount=2");
            CHECK(contentsByLine[3] == "0000\\0=HT:Dimension3");
            CHECK(contentsByLine[4] == "0000\\1=BF:Color");
            CHECK(contentsByLine[5] == "0001\\InfoCount=0");
            CHECK(contentsByLine[6] == "0002\\InfoCount=4");
            CHECK(contentsByLine[7] == "0002\\0=FLCH0:Dimension2");
            CHECK(contentsByLine[8] == "0002\\1=FLCH1:Dimension2");
            CHECK(contentsByLine[9] == "0002\\2=FLCH2:Dimension2");
            CHECK(contentsByLine[10] == "0002\\3=FLCH3:Dimension2");
            
            CHECK(QFile::remove(iniFilePath) == true);
        }
    }
    TEST_CASE("AcquisitionSequenceWriterIni : practical test") {
        //TODO Implement test
    }
}