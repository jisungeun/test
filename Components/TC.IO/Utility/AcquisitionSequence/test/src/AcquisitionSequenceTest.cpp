#include <catch2/catch.hpp>

#include "AcquisitionSequence.h"

namespace AcquisitionSequenceTest {
    using namespace TC::IO::AcquisitionSequence;

    TEST_CASE("AcquisitionSequence : unit test") {
        SECTION("AcquisitionSequence()") {
            AcquisitionSequence acquisitionSequence;
            CHECK(&acquisitionSequence != nullptr);
        }
        SECTION("AcquisitionSequence(other)") {
            AcquisitionSequence srcAcquisitionSequence;
            srcAcquisitionSequence.SetTimeFrameCount(1);

            AcquisitionSequence destAcquisitionSequence(srcAcquisitionSequence);
            CHECK(destAcquisitionSequence.GetTimeFrameCount() == 1);
        }
        SECTION("operator=()") {
            AcquisitionSequence srcAcquisitionSequence;
            srcAcquisitionSequence.SetTimeFrameCount(1);

            AcquisitionSequence destAcquisitionSequence;
            destAcquisitionSequence = srcAcquisitionSequence;
            CHECK(destAcquisitionSequence.GetTimeFrameCount() == 1);
        }
        SECTION("SetTimeFrameCount()") {
            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.SetTimeFrameCount(1);
            CHECK(&acquisitionSequence != nullptr);
        }
        SECTION("GetTimeFrameCount()") {
            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.SetTimeFrameCount(1);
            CHECK(acquisitionSequence.GetTimeFrameCount() == 1);
        }
        SECTION("AddFrame()") {
            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.AddFrame(0, {});
            CHECK(&acquisitionSequence != nullptr);
        }
        SECTION("ExistFrame()") {
            AcquisitionSequence acquisitionSequence;
            CHECK(acquisitionSequence.ExistFrame(0) == false);
            acquisitionSequence.AddFrame(0, {});
            CHECK(acquisitionSequence.ExistFrame(0) == true);
        }
        SECTION("GetFrame()") {
            AcquisitionSequence acquisitionSequence;
            acquisitionSequence.AddFrame(0, AcquisitionFrame{ {{Modality::BF, AcquisitionType::Color}} });

            const auto resultFrame = acquisitionSequence.GetFrame(0);
            const auto& infoList = resultFrame.GetInfoList();

            CHECK(infoList.size() == 1);
            CHECK(infoList.first().modality == +Modality::BF);
            CHECK(infoList.first().type == +AcquisitionType::Color);
        }
    }
    TEST_CASE("AcquisitionSequence : practical test") {
        constexpr auto timeFrameCount = 5;

        AcquisitionFrame acquisitionFrame0;
        acquisitionFrame0.AddInfo({ Modality::HT, AcquisitionType::Dimension3 });
        acquisitionFrame0.AddInfo({ Modality::FLCH0, AcquisitionType::Dimension2 });
        acquisitionFrame0.AddInfo({ Modality::BF, AcquisitionType::Color });

        AcquisitionFrame acquisitionFrame2;
        acquisitionFrame2.AddInfo({ Modality::FLCH1, AcquisitionType::Dimension2 });
        acquisitionFrame2.AddInfo({ Modality::BF, AcquisitionType::Color });

        AcquisitionFrame acquisitionFrame4;
        acquisitionFrame4.AddInfo({ Modality::FLCH2, AcquisitionType::Dimension2 });
        acquisitionFrame4.AddInfo({ Modality::BF, AcquisitionType::Color });

        AcquisitionSequence acquisitionSequence;
        acquisitionSequence.SetTimeFrameCount(timeFrameCount);
        acquisitionSequence.AddFrame(0, acquisitionFrame0);
        acquisitionSequence.AddFrame(2, acquisitionFrame2);
        acquisitionSequence.AddFrame(4, acquisitionFrame4);

        CHECK(acquisitionSequence.GetTimeFrameCount() == 5);
        CHECK(acquisitionSequence.ExistFrame(0) == true);
        CHECK(acquisitionSequence.ExistFrame(1) == false);
        CHECK(acquisitionSequence.ExistFrame(2) == true);
        CHECK(acquisitionSequence.ExistFrame(3) == false);
        CHECK(acquisitionSequence.ExistFrame(4) == true);

        const auto resultAcquisitionFrame0 = acquisitionSequence.GetFrame(0);
        CHECK(resultAcquisitionFrame0.GetInfoList().size() == 3);
        CHECK(resultAcquisitionFrame0.GetInfoList().contains({Modality::HT, AcquisitionType::Dimension3}) == true);
        CHECK(resultAcquisitionFrame0.GetInfoList().contains({Modality::FLCH0, AcquisitionType::Dimension2}) == true);
        CHECK(resultAcquisitionFrame0.GetInfoList().contains({Modality::BF, AcquisitionType::Color}) == true);

        const auto resultAcquisitionFrame2 = acquisitionSequence.GetFrame(2);
        CHECK(resultAcquisitionFrame2.GetInfoList().size() == 2);
        CHECK(resultAcquisitionFrame2.GetInfoList().contains({ Modality::FLCH1, AcquisitionType::Dimension2 }) == true);
        CHECK(resultAcquisitionFrame2.GetInfoList().contains({ Modality::BF, AcquisitionType::Color }) == true);

        const auto resultAcquisitionFrame4 = acquisitionSequence.GetFrame(4);
        CHECK(resultAcquisitionFrame4.GetInfoList().size() == 2);
        CHECK(resultAcquisitionFrame4.GetInfoList().contains({ Modality::FLCH2, AcquisitionType::Dimension2 }) == true);
        CHECK(resultAcquisitionFrame4.GetInfoList().contains({ Modality::BF, AcquisitionType::Color }) == true);
    }
}
