#include <catch2/catch.hpp>

#include "IAcquisitionSequenceReader.h"

namespace IAcquisitionSequenceReaderTest {
    using namespace TC::IO::AcquisitionSequence;

    class AcquisitionSequenceReaderForTest final : public IAcquisitionSequenceReader {
    public:
        AcquisitionSequenceReaderForTest() = default;
        ~AcquisitionSequenceReaderForTest() = default;

        auto Read() -> bool override {
            this->readTriggered = true;
            return false;
        }
        auto GetAcquisitionSequence() const -> AcquisitionSequence override {
            this->getAcquisitionSequenceTriggered = true;
            return {};
        }

        bool readTriggered{ false };
        mutable bool getAcquisitionSequenceTriggered{ false };
    };

    TEST_CASE("IAcquisitionSequenceReader : unit test") {
        SECTION("Read()") {
            AcquisitionSequenceReaderForTest acquisitionSequenceReader;
            CHECK(acquisitionSequenceReader.readTriggered == false);
            const auto result = acquisitionSequenceReader.Read();
            CHECK(acquisitionSequenceReader.readTriggered == true);
        }
        SECTION("GetAcquisitionSequence()") {
            AcquisitionSequenceReaderForTest acquisitionSequenceReader;
            CHECK(acquisitionSequenceReader.getAcquisitionSequenceTriggered == false);
            const auto result = acquisitionSequenceReader.GetAcquisitionSequence();
            CHECK(acquisitionSequenceReader.getAcquisitionSequenceTriggered == true);
        }
    }
}