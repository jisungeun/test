#include "AcquisitionSequenceWriterIni.h"

#include <QFile>
#include <QSettings>

namespace TC::IO::AcquisitionSequence {
    class AcquisitionSequenceWriterIni::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString iniFilePath{};

        AcquisitionSequence acquisitionSequence{};
    };

    AcquisitionSequenceWriterIni::AcquisitionSequenceWriterIni() : d(new Impl()) {
    }

    AcquisitionSequenceWriterIni::~AcquisitionSequenceWriterIni() = default;

    auto AcquisitionSequenceWriterIni::SetIniFilePath(const QString& iniFilePath) -> void {
        d->iniFilePath = iniFilePath;
    }

    auto AcquisitionSequenceWriterIni::SetAcquisitionSequence(const AcquisitionSequence& acquisitionSequence) -> void {
        d->acquisitionSequence = acquisitionSequence;
    }

    auto AcquisitionSequenceWriterIni::Write() -> bool {
        if (d->iniFilePath.isEmpty()) { return false; }

        if (QFile::exists(d->iniFilePath)) {
            if (!QFile::remove(d->iniFilePath)) { return false; }
        }

        const auto timeFrameCount = d->acquisitionSequence.GetTimeFrameCount();
        if (timeFrameCount == 0) { return false; }

        QSettings iniFile(d->iniFilePath, QSettings::IniFormat);

        iniFile.beginGroup("AcquisitionSequence");
        iniFile.setValue("TimeFrameCount", QString("%1").arg(timeFrameCount));

        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            const auto timeIndexString = QString("%1").arg(timeIndex, 4, 10, QChar('0'));
            iniFile.beginGroup(timeIndexString);

            const auto frameExists = d->acquisitionSequence.ExistFrame(timeIndex);
            if (frameExists) {
                const auto acquisitionFrame = d->acquisitionSequence.GetFrame(timeIndex);
                const auto& infoList = acquisitionFrame.GetInfoList();

                const auto infoCount = infoList.size();

                iniFile.setValue("InfoCount", QString("%1").arg(infoCount));

                for (auto infoIndex = 0; infoIndex < infoCount; ++infoIndex) {
                    const auto& info = infoList[infoIndex];

                    const auto modalityString = QString(info.modality._to_string());
                    const auto typeString = QString(info.type._to_string());

                    const auto infoString = QString("%1:%2").arg(modalityString).arg(typeString);

                    iniFile.setValue(QString("%1").arg(infoIndex), infoString);
                }
            } else {
                iniFile.setValue("InfoCount", "0");
            }

            iniFile.endGroup();
        }
        iniFile.endGroup();

        return true;
    }
}
