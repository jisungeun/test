#include "AcquisitionSequence.h"

#include <QMap>

namespace TC::IO::AcquisitionSequence {
    class AcquisitionSequence::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        int32_t timeFrameCount{};
        QMap<int32_t, AcquisitionFrame> acquisitionFrameMap{};
    };

    AcquisitionSequence::AcquisitionSequence() : d(new Impl()) {
    }

    AcquisitionSequence::AcquisitionSequence(const AcquisitionSequence& other) : d(new Impl(*other.d)) {
    }

    AcquisitionSequence::~AcquisitionSequence() = default;

    auto AcquisitionSequence::operator=(const AcquisitionSequence& other) -> AcquisitionSequence& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto AcquisitionSequence::SetTimeFrameCount(const int32_t& timeFrameCount) -> void {
        d->timeFrameCount = timeFrameCount;
    }

    auto AcquisitionSequence::GetTimeFrameCount() const -> const int32_t& {
        return d->timeFrameCount;
    }

    auto AcquisitionSequence::AddFrame(const int32_t& timeFrameIndex, const AcquisitionFrame& acquisitionFrame)
        -> void {
        if (d->acquisitionFrameMap.contains(timeFrameIndex)) {
            d->acquisitionFrameMap.remove(timeFrameIndex);
        }
        d->acquisitionFrameMap[timeFrameIndex] = acquisitionFrame;
    }

    auto AcquisitionSequence::ExistFrame(const int32_t& timeFrameIndex)const -> bool {
        return d->acquisitionFrameMap.contains(timeFrameIndex);
    }

    auto AcquisitionSequence::GetFrame(const int32_t& timeFrameIndex)const -> AcquisitionFrame {
        return d->acquisitionFrameMap[timeFrameIndex];
    }
}
