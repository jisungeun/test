#include "AcquisitionFrame.h"

namespace TC::IO::AcquisitionSequence {
    class AcquisitionFrame::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        QList<AcquisitionInfo> acquisitionTypeList{};
    };

    auto AcquisitionFrame::AcquisitionInfo::operator==(const AcquisitionInfo& other) -> bool {
        if (this->modality != other.modality) { return false; }
        if (this->type != other.type) { return false; }
        return true;
    }

    auto AcquisitionFrame::AcquisitionInfo::operator!=(const AcquisitionInfo& other) -> bool {
        return !(*this == other);
    }

    AcquisitionFrame::AcquisitionFrame() : d(new Impl()) {
    }

    AcquisitionFrame::AcquisitionFrame(const AcquisitionFrame& other) : d(new Impl(*other.d)) {
    }

    AcquisitionFrame::AcquisitionFrame(const QList<AcquisitionInfo>& acquisitionInfoList) : d(new Impl()) {
        this->SetInfoList(acquisitionInfoList);
    }

    AcquisitionFrame::~AcquisitionFrame() = default;

    auto AcquisitionFrame::operator=(const AcquisitionFrame& other) -> AcquisitionFrame& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto AcquisitionFrame::AddInfo(const AcquisitionInfo& acquisitionInfo) ->void {
        if (!d->acquisitionTypeList.contains(acquisitionInfo)) {
            d->acquisitionTypeList.push_back(acquisitionInfo);
        } 
    }

    auto AcquisitionFrame::SetInfoList(const QList<AcquisitionInfo>& acquisitionInfoList) ->void {
        d->acquisitionTypeList = acquisitionInfoList;
    }

    auto AcquisitionFrame::GetInfoList() const -> const QList<AcquisitionInfo>& {
        return d->acquisitionTypeList;
    }
}
