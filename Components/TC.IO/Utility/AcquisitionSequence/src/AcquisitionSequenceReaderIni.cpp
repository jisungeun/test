#include "AcquisitionSequenceReaderIni.h"

#include <QFile>
#include <QSettings>

namespace TC::IO::AcquisitionSequence {
    class AcquisitionSequenceReaderIni::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString iniFilePath{};

        AcquisitionSequence acquisitionSequence{};
    };

    AcquisitionSequenceReaderIni::AcquisitionSequenceReaderIni() : d(new Impl()) {
    }

    AcquisitionSequenceReaderIni::~AcquisitionSequenceReaderIni() = default;

    auto AcquisitionSequenceReaderIni::SetIniFilePath(const QString& iniFilePath) -> void {
        d->iniFilePath = iniFilePath;
    }

    auto AcquisitionSequenceReaderIni::Read() -> bool {
        if (d->iniFilePath.isEmpty()) { return false; }
        if (!QFile::exists(d->iniFilePath)) { return false; }

        AcquisitionSequence acquisitionSequence;

        QSettings iniFile(d->iniFilePath, QSettings::IniFormat);

        iniFile.beginGroup("AcquisitionSequence");
        const auto timeFrameCount = iniFile.value("TimeFrameCount", QString("0")).toInt();

        acquisitionSequence.SetTimeFrameCount(timeFrameCount);

        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            const auto timeIndexString = QString("%1").arg(timeIndex, 4, 10, QChar('0'));
            iniFile.beginGroup(timeIndexString);

            const auto infoCount = iniFile.value("InfoCount", QString("0")).toInt();
            if (infoCount > 0) {
                AcquisitionFrame acquisitionFrame;
                for (auto infoIndex = 0; infoIndex < infoCount; ++infoIndex) {
                    const auto infoIndexString = QString("%1").arg(infoIndex);
                    const auto infoString = iniFile.value(infoIndexString).toString();

                    const auto infoStringList = infoString.split(":");

                    AcquisitionFrame::AcquisitionInfo acquisitionInfo;
                    acquisitionInfo.modality = Modality::_from_string(infoStringList.first().toStdString().c_str());
                    acquisitionInfo.type = AcquisitionType::_from_string(infoStringList.last().toStdString().c_str());

                    acquisitionFrame.AddInfo(acquisitionInfo);
                }

                acquisitionSequence.AddFrame(timeIndex, acquisitionFrame);
            }
            
            iniFile.endGroup();
        }

        iniFile.endGroup();

        d->acquisitionSequence = acquisitionSequence;

        return true;
    }

    auto AcquisitionSequenceReaderIni::GetAcquisitionSequence() const -> AcquisitionSequence {
        return d->acquisitionSequence;
    }
}
