project(TCAcquisitionSequence C CXX)

#Header files for external use
set(INTERFACE_HEADERS
    inc/AcquisitionSequence.h
    inc/AcquisitionFrame.h
    inc/IAcquisitionSequenceWriter.h
    inc/IAcquisitionSequenceReader.h
    inc/AcquisitionSequenceWriterIni.h
    inc/AcquisitionSequenceReaderIni.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
    src/AcquisitionSequence.cpp
    src/AcquisitionFrame.cpp
    src/AcquisitionSequenceWriterIni.cpp
    src/AcquisitionSequenceReaderIni.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TC::Components::AcquisitionSequence ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
        Qt5::Core
        Extern::BetterEnums
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/IO/Utility")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT io)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)