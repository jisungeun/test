#pragma once

#include <memory>
#include "TCAcquisitionSequenceExport.h"

#include "AcquisitionSequence.h"

namespace TC::IO::AcquisitionSequence {
    class TCAcquisitionSequence_API IAcquisitionSequenceReader {
    public:
        using Pointer = std::shared_ptr<IAcquisitionSequenceReader>;

        virtual ~IAcquisitionSequenceReader() = default;

        virtual auto Read()->bool = 0;
        virtual auto GetAcquisitionSequence()const->AcquisitionSequence = 0;
    };
}