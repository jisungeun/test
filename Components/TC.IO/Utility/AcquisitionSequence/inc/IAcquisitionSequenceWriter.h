#pragma once

#include <memory>
#include "TCAcquisitionSequenceExport.h"

#include "AcquisitionSequence.h"

namespace TC::IO::AcquisitionSequence {
    class TCAcquisitionSequence_API IAcquisitionSequenceWriter {
    public:
        using Pointer = std::shared_ptr<IAcquisitionSequenceWriter>;

        virtual ~IAcquisitionSequenceWriter() = default;

        virtual auto SetAcquisitionSequence(const AcquisitionSequence& acquisitionSequence)->void = 0;
        virtual auto Write()->bool = 0;
    };
}