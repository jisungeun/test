#pragma once

#include <memory>
#include <QString>
#include "TCAcquisitionSequenceExport.h"

#include "IAcquisitionSequenceWriter.h"

namespace TC::IO::AcquisitionSequence {
    class TCAcquisitionSequence_API AcquisitionSequenceWriterIni final : public IAcquisitionSequenceWriter{
    public:
        AcquisitionSequenceWriterIni();
        ~AcquisitionSequenceWriterIni();

        auto SetIniFilePath(const QString& iniFilePath)->void;
        auto SetAcquisitionSequence(const AcquisitionSequence& acquisitionSequence) -> void override;
        auto Write() -> bool override;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
    
}
