#pragma once

#include <memory>
#include "TCAcquisitionSequenceExport.h"

#include "AcquisitionFrame.h"

namespace TC::IO::AcquisitionSequence {
    class TCAcquisitionSequence_API AcquisitionSequence {
    public:
        AcquisitionSequence();
        AcquisitionSequence(const AcquisitionSequence& other);
        ~AcquisitionSequence();

        auto operator=(const AcquisitionSequence& other)->AcquisitionSequence&;

        auto SetTimeFrameCount(const int32_t& timeFrameCount)->void;
        auto GetTimeFrameCount()const->const int32_t&;

        auto AddFrame(const int32_t& timeFrameIndex, const AcquisitionFrame& acquisitionFrame)->void;
        auto ExistFrame(const int32_t& timeFrameIndex)const->bool;
        auto GetFrame(const int32_t& timeFrameIndex)const->AcquisitionFrame;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
