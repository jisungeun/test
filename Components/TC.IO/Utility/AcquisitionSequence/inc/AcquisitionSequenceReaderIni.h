#pragma once

#include <memory>
#include <QString>
#include "TCAcquisitionSequenceExport.h"

#include "IAcquisitionSequenceReader.h"

namespace TC::IO::AcquisitionSequence {
    class TCAcquisitionSequence_API AcquisitionSequenceReaderIni final : public IAcquisitionSequenceReader{
    public:
        AcquisitionSequenceReaderIni();
        ~AcquisitionSequenceReaderIni() override;

        auto SetIniFilePath(const QString& iniFilePath)->void;
        auto Read() -> bool override;
        auto GetAcquisitionSequence() const -> AcquisitionSequence override;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
