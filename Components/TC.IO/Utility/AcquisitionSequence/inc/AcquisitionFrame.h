#pragma once

#include <memory>
#include <QList>

#include "enum.h"

#include "TCAcquisitionSequenceExport.h"

namespace TC::IO::AcquisitionSequence {
    BETTER_ENUM(Modality, uint8_t, HT, FLCH0, FLCH1, FLCH2, FLCH3, BF);
    BETTER_ENUM(AcquisitionType, uint8_t, Dimension3, Dimension2, Gray, Color);

    class TCAcquisitionSequence_API AcquisitionFrame {
    public:
        class TCAcquisitionSequence_API AcquisitionInfo {
        public:
            auto operator==(const AcquisitionInfo& other)->bool;
            auto operator!=(const AcquisitionInfo& other)->bool;

            Modality modality{ Modality::HT };
            AcquisitionType type{ AcquisitionType::Dimension3 };
        };

        AcquisitionFrame();
        AcquisitionFrame(const AcquisitionFrame& other);
        explicit AcquisitionFrame(const QList<AcquisitionInfo>& acquisitionInfoList);
        ~AcquisitionFrame();

        auto operator=(const AcquisitionFrame& other)->AcquisitionFrame&;

        auto AddInfo(const AcquisitionInfo& acquisitionInfo)->void;
        auto SetInfoList(const QList<AcquisitionInfo>& acquisitionInfoList)->void;

        auto GetInfoList()const->const QList<AcquisitionInfo>&;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}