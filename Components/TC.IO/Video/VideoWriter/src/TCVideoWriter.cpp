#include "TCVideoWriter.h"

namespace TC::IO {
    struct TCVideoWriter::Impl {
        Impl() = default;
        ~Impl() = default;
    };

    TCVideoWriter::TCVideoWriter() = default;
    TCVideoWriter::~TCVideoWriter() = default;

    auto TCVideoWriter::Write(const std::string& path, const std::list<cv::Mat>& buffer, const float& fps)->bool {
        if(true == path.empty()) {
            return false;
        }

        if(true == buffer.empty()) {
            return true;
        }

        auto result = true;
        cv::VideoWriter writer;
        
        try {
            //const auto codec = cv::VideoWriter::fourcc('m', 'p', '4', 'v');
            const auto codec = cv::VideoWriter::fourcc('a', 'v', 'c', '1');
            //if (false == writer.open(path, codec, fps, buffer.front().size(), true)) {
            if (false == writer.open(path, codec, fps, buffer.front().size(), true)) {
                throw std::exception();
            }

            for (const auto& frame : buffer) {
                writer.write(frame);
            }
        }
        catch (cv::Exception& ex) {
            std::cout << ex.msg << std::endl;
            result = false;
        }
        catch (std::exception& ex) {
            std::cout << ex.what() << std::endl;
            result = false;
        }
        catch (...) {
            result = false;
        }

        writer.release();

        return result;
    }
}
