#pragma once
#include <TCVideoWriterExport.h>
#include <memory>
#include <string>
#include <list>

#include <opencv2/opencv.hpp>

namespace TC::IO {
    class TCVideoWriter_API TCVideoWriter {
    public:
        TCVideoWriter();
        ~TCVideoWriter();

        auto Write(const std::string& path, const std::list<cv::Mat>& buffer, const float& fps = 30.f)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
