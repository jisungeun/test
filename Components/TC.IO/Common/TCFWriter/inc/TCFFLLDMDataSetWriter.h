#pragma once

#include <memory>

#include <H5Cpp.h>

#include "TCFFLLDMDataSet.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFFLLDMDataSetWriter {
    public:
        TCFFLLDMDataSetWriter();
        ~TCFFLLDMDataSetWriter();

        auto SetTCFFLLDMDataSet(const TCFFLLDMDataSet& dataSet)->void;

        auto SetDestLDMGroup(H5::Group* ldmGroup3D, H5::Group* ldmGroupMIP)->void;

        auto SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY, const int64_t& dataSizeZ)->void;

        auto Write()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}