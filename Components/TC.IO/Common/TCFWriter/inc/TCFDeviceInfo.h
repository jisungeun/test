#pragma once

#include <memory>
#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFDeviceInfo {
public:
    TCFDeviceInfo();
    TCFDeviceInfo(const TCFDeviceInfo& other);
    ~TCFDeviceInfo();

    auto operator=(const TCFDeviceInfo& other)->TCFDeviceInfo&;

    auto SetMagnification(const double& magnification)->void;
    auto GetMagnification()const->const double&;

    auto SetNA(const double& na)->void;
    auto GetNA()const->const double&;

    auto SetRI(const double& ri)->void;
    auto GetRI()const->const double&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
