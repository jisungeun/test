#pragma once

#include <memory>

#include <QString>
#include "TCFCommonInfo.h"
#include "TCFDeviceInfo.h"
#include "TCFStructureInfo.h"
#include "TCFTileInfo.h"
#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFCompleter {
public:
    TCFCompleter();
    ~TCFCompleter();

    auto SetTargetTCFFilePath(const QString& targetTCFFilePath)->void;
    auto SetTCFCommonInfo(const TCFCommonInfo& tcfCommonInfo)->void;
    auto SetTCFDeviceInfo(const TCFDeviceInfo& tcfDeviceInfo)->void;
    auto SetTCFTileInfo(const TCFTileInfo& tcfTileInfo)->void;
    auto SetTCFStructureInfo(const TCFStructureInfo& tcfStructureInfo)->void;

    auto MakeTCFComplete()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};