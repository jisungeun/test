#pragma once

#include <memory>

#include <LdmConfiguration.h>

#include "IHTLDMDataSetGetter.h"
#include "TCFHTDatasetMetaInfo.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFHTLDMDataSet {
    public:
        TCFHTLDMDataSet();
        TCFHTLDMDataSet(const TCFHTLDMDataSet& other);
        ~TCFHTLDMDataSet();

        auto operator=(const TCFHTLDMDataSet& other)->TCFHTLDMDataSet&;

        auto SetDataGetter(const IHTLDMDataSetGetter::Pointer& dataGetter)->void;

        auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration3D, const IO::LdmCore::LdmConfiguration& ldmConfigurationMip)->void;
        auto GetLdmConfiguration3D()->IO::LdmCore::LdmConfiguration;
        auto GetLdmConfigurationMIP()->IO::LdmCore::LdmConfiguration;

        auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<float[]>;
        auto GetTileDataMIP(const int32_t& tileIndex)->std::shared_ptr<float[]>;

        auto SetMetaInfo(const TCFHTDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()const->TCFHTDataSetMetaInfo;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}