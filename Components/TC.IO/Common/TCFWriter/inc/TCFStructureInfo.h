#pragma once

#include <memory>
#include <enum.h>

#include "TCTCFWriterExport.h"

BETTER_ENUM(StructureType, int32_t, Default = 0, LDM = 1);

class TCTCFWriter_API TCFStructureInfo {
public:
    TCFStructureInfo();
    TCFStructureInfo(const TCFStructureInfo& other);
    ~TCFStructureInfo();

    auto operator=(const TCFStructureInfo& other)->TCFStructureInfo&;

    auto SetStructureType(const StructureType& structureType)->void;
    auto GetStructureType()const->StructureType;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};