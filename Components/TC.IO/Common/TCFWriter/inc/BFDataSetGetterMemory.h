#pragma once

#include <memory>

#include "IBFDataSetGetter.h"
#include "TCTCFWriterExport.h"

class TCTCFWriter_API BFDataSetGetterMemory final : public IBFDataSetGetter{
public:
    BFDataSetGetterMemory();
    ~BFDataSetGetterMemory();

    auto SetData(const std::shared_ptr<uint8_t[]>& data, const MemoryOrder3D& memoryOrder)->void;
    auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& channelCount)->void;

    auto GetData() const -> std::shared_ptr<uint8_t[]> override;
    auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
        -> std::shared_ptr<uint8_t[]> override;
    auto GetColorChannelCount() const -> int32_t override;
    auto GetDataMemoryOrder() const -> MemoryOrder3D override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};