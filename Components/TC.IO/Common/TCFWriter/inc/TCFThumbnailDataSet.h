#pragma once

#include <memory>

#include "SIUnit.h"
#include "TCFWriterDefines.h"
#include "TCTCFWriterExport.h"

namespace TC::IO::TCFWriter {
    class TCTCFWriter_API TCFThumbnailDataSet {
    public:
        TCFThumbnailDataSet();
        TCFThumbnailDataSet(const TCFThumbnailDataSet& other);
        ~TCFThumbnailDataSet();

        auto operator=(const TCFThumbnailDataSet& other)->TCFThumbnailDataSet&;

        auto SetData(const std::shared_ptr<uint8_t[]>& data)->void;
        auto GetData()const->std::shared_ptr<uint8_t[]>;

        auto SetDataMemoryOrder(const MemoryOrder3D& memoryOrder)->void;
        auto GetDataMemoryOrder()const->MemoryOrder3D;

        auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto GetSizeX()const->int32_t;
        auto GetSizeY()const->int32_t;

        auto SetColorFlag(const bool& colorFlag)->void;
        auto GetColorFlag()const->bool;

        auto SetVoxelSize(const float& voxelSizeX, const float& voxelSizeY, const LengthUnit& unit)->void;
        auto GetVoxelSizeX(const LengthUnit& unit)const->float;
        auto GetVoxelSizeY(const LengthUnit& unit)const->float;

        auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
        auto GetTimeFrameIndex()const->int32_t;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
