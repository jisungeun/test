#pragma once

#include <memory>
#include <QDateTime>

#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFCommonInfo {
public:
    TCFCommonInfo();
    TCFCommonInfo(const TCFCommonInfo& other);
    ~TCFCommonInfo();

    auto operator=(const TCFCommonInfo& other)->TCFCommonInfo&;

    auto SetCreateDate(const QDateTime& createDate)->void;
    auto GetCreateDate()const -> const QDateTime&;

    auto SetDataID(const QString& dataID)->void;
    auto GetDataID()const ->const QString&;

    auto SetDescription(const QString& description)->void;
    auto GetDescription()const->const QString&;

    auto SetDeviceHost(const QString& deviceHost)->void;
    auto GetDeviceHost()const->const QString&;

    auto SetDeviceSerial(const QString& deviceSerial)->void;
    auto GetDeviceSerial()const->const QString&;

    auto SetDeviceModelType(const QString& deviceModelType)->void;
    auto GetDeviceModelType()const->const QString&;

    auto SetDeviceSoftwareVersion(const QString& deviceSoftwareVersion)->void;
    auto GetDeviceSoftwareVersion()const->const QString&;

    auto SetFormatVersion(const QString& formatVersion)->void;
    auto GetFormatVersion()const->const QString&;

    auto SetRecordingTime(const QDateTime& recordingTime)->void;
    auto GetRecordingTime()const->const QDateTime&;

    auto SetSoftwareVersion(const QString& softwareVersion)->void;
    auto GetSoftwareVersion()const->const QString&;

    auto SetTitle(const QString& title)->void;
    auto GetTitle()const->const QString&;

    auto SetUniqueID(const QString& uniqueID)->void;
    auto GetUniqueID()const->const QString&;

    auto SetUserID(const QString& userID)->void;
    auto GetUserID()const->const QString&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};