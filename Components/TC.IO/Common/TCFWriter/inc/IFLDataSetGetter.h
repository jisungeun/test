#pragma once

#include "TCTCFWriterExport.h"
#include "TCFWriterDefines.h"

#include <memory>

class TCTCFWriter_API IFLDataSetGetter {
public:
    typedef std::shared_ptr<IFLDataSetGetter> Pointer;
    virtual ~IFLDataSetGetter() = default;

    virtual auto GetData()const->std::shared_ptr<float[]> = 0;
    virtual auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
        const int32_t& z0, const int32_t& z1)const->std::shared_ptr<float[]> = 0;
    virtual auto GetDataMemoryOrder()const->MemoryOrder3D = 0;

    virtual auto GetMIPData()const ->std::shared_ptr<float[]> = 0;
    virtual auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
        const->std::shared_ptr<float[]> = 0;
    virtual auto GetMIPDataMemoryOrder()const->MemoryOrder2D = 0;
};