#pragma once

#include <memory>
#include <QString>

#include "TCTCFWriterExport.h"
#include "SIUnit.h"

class TCTCFWriter_API TCFHTCompleter {
public:
    TCFHTCompleter();
    ~TCFHTCompleter();

    auto SetTargetTCFFilePath(const QString& targetTCFFilePath)->void;

    auto SetLdmOptionFlag(const bool& ldmOptionFlag)->void;
    auto SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY, const float& worldPixelSizeZ, 
        const LengthUnit& unit)->void;

    auto MakeCompleteGroup()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};