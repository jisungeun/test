#pragma once

#include <memory>

#include "LdmConfiguration.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API IFLLDMDataSetGetter {
    public:
        using Pointer = std::shared_ptr<IFLLDMDataSetGetter>;
        virtual ~IFLLDMDataSetGetter();

        virtual auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration3D,
            const IO::LdmCore::LdmConfiguration& ldmConfigurationMIP)->void = 0;
        virtual auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> = 0;
        virtual auto GetMipTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> = 0;
    };
}

