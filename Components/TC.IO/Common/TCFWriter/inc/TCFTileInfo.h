#pragma once

#include <memory>

#include "TCFDataSetRecordedPosition.h"
#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFTileInfo {
public:
    TCFTileInfo();
    TCFTileInfo(const TCFTileInfo& other);
    ~TCFTileInfo();

    auto operator=(const TCFTileInfo& other)->TCFTileInfo&;

    auto SetOverlapLength(const double& overlapH, const double& overlapV, const LengthUnit& unit)->void;
    auto GetOverlapLengthH(const LengthUnit& unit)const->double;
    auto GetOverlapLengthV(const LengthUnit& unit)const->double;

    auto SetTCFDataSetRecordedPosition(const TCFDataSetRecordedPosition& position)->void;
    auto GetTCFDataSetRecordedPosition()const->const TCFDataSetRecordedPosition&;

    auto SetTileNumber(const int32_t& tileNumberH, const int32_t& tileNumberV)->void;
    auto GetTileNumberH()const->const int32_t&;
    auto GetTileNumberV()const->const int32_t&;

    auto SetTileIndex(const int32_t& tileIndexColumn, const int32_t& tileIndexRow)->void;
    auto GetTileIndexColumn()const->const int32_t&;
    auto GetTileIndexRow()const->const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
