#pragma once

#include <memory>
#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFSkeletonConfig {
public:
    TCFSkeletonConfig();
    TCFSkeletonConfig(const TCFSkeletonConfig& other);
    ~TCFSkeletonConfig();

    auto operator=(const TCFSkeletonConfig& other)->TCFSkeletonConfig&;

    auto SetDataIncludingFlag(const bool& htIncludingFlag, const bool& flIncludingFlag, const bool& bfIncludingFlag)->void;

    auto GetHTIncludingFlag()const->const bool&;
    auto GetFLIncludingFlag()const->const bool&;
    auto GetBFIncludingFlag()const->const bool&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};