#pragma once

#include "TCTCFWriterExport.h"
#include "TCFWriterDefines.h"

#include <memory>

class TCTCFWriter_API IBFDataSetGetter {
public:
    typedef std::shared_ptr<IBFDataSetGetter> Pointer;
    virtual ~IBFDataSetGetter() = default;

    virtual auto GetData()const->std::shared_ptr<uint8_t[]> = 0;
    virtual auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)const
        ->std::shared_ptr<uint8_t[]> = 0;
    virtual auto GetColorChannelCount()const->int32_t = 0;
    virtual auto GetDataMemoryOrder()const->MemoryOrder3D = 0;
};