#pragma once

#include <memory>
#include <QDateTime>

#include "TCTCFWriterExport.h"
#include "IBFDataSetGetter.h"
#include "TCFBFDataSetMetaInfo.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFBFDataSet {
    public:
        TCFBFDataSet();
        TCFBFDataSet(const TCFBFDataSet& other);
        ~TCFBFDataSet();

        auto operator=(const TCFBFDataSet& other)->TCFBFDataSet&;

        auto SetDataSetGetter(const IBFDataSetGetter::Pointer& dataSetGetter)->void;
        auto GetData()const->std::shared_ptr<uint8_t[]>;
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)const
            ->std::shared_ptr<uint8_t[]>;
        auto GetDataMemoryOrder()const->MemoryOrder3D;

        auto GetChannelCount()const->int32_t;

        auto SetMetaInfo(const TCFBFDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()->TCFBFDataSetMetaInfo;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
