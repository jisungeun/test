#pragma once

#include <memory>

#include <H5Cpp.h>

#include "TCFBFLDMDataSet.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFBFLDMDataSetWriter {
    public:
        TCFBFLDMDataSetWriter();
        ~TCFBFLDMDataSetWriter();

        auto SetTCFBFLDMDataSet(const TCFBFLDMDataSet& dataSet)->void;

        auto SetDestLDMGroup(H5::Group* ldmGroup)->void;

        auto SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY)->void;

        auto Write()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}