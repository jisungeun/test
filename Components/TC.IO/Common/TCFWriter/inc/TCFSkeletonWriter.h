#pragma once

#include <memory>
#include <QString>

#include "TCTCFWriterExport.h"

#include "TCFSkeletonConfig.h"

class TCTCFWriter_API TCFSkeletonWriter {
public:
    TCFSkeletonWriter();
    ~TCFSkeletonWriter();

    auto SetTargetFilePath(const QString& tcfFilePath)->void;
    auto SetConfig(const TCFSkeletonConfig& config)->void;

    auto Write()->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};