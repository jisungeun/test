#pragma once

#include <memory>

#include "LdmConfiguration.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API IBFLDMDataSetGetter {
    public:
        using Pointer = std::shared_ptr<IBFLDMDataSetGetter>;
        virtual ~IBFLDMDataSetGetter();

        virtual auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration)->void = 0;
        virtual auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<uint8_t[]> = 0;
        virtual auto GetChannelCount()->int32_t = 0;
    };
}

