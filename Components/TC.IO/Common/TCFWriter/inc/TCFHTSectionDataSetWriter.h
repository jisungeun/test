#pragma once

#include <memory>
#include <QString>

#include "TCFHTDataSet.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFHTSectionDataSetWriter {
    public:
        TCFHTSectionDataSetWriter();
        ~TCFHTSectionDataSetWriter();

        auto SetTargetFilePath(const QString& targetFilePath)->void;
        auto SetTCFHTDataSet(const TCFHTDataSet& dataSet)->void;

        auto SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY, const int64_t& dataSizeZ)->void;
        auto SetWritingStartIndex(const int64_t& indexX, const int64_t& indexY, const int64_t& indexZ = 0)->void;

        auto Write()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}