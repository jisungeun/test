#pragma once

#include <memory>

#include <LdmConfiguration.h>

#include "IFLLDMDataSetGetter.h"
#include "TCFFLDataSetMetaInfo.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFFLLDMDataSet {
    public:
        TCFFLLDMDataSet();
        TCFFLLDMDataSet(const TCFFLLDMDataSet& other);
        ~TCFFLLDMDataSet();

        auto operator=(const TCFFLLDMDataSet& other)->TCFFLLDMDataSet&;

        auto SetDataGetter(const IFLLDMDataSetGetter::Pointer& dataGetter)->void;

        auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration3D, const IO::LdmCore::LdmConfiguration& ldmConfigurationMip)->void;
        auto GetLdmConfiguration3D()->IO::LdmCore::LdmConfiguration;
        auto GetLdmConfigurationMIP()->IO::LdmCore::LdmConfiguration;

        auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<float[]>;
        auto GetTileDataMIP(const int32_t& tileIndex)->std::shared_ptr<float[]>;

        auto SetMetaInfo(const TCFFLDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()const->TCFFLDataSetMetaInfo;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}