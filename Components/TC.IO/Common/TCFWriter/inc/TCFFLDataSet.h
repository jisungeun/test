#pragma once

#include <memory>

#include "IFLDataSetGetter.h"
#include "TCTCFWriterExport.h"
#include "TCFWriterDefines.h"
#include "TCFFLDataSetMetaInfo.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFFLDataSet {
    public:
        TCFFLDataSet();
        TCFFLDataSet(const TCFFLDataSet& other);
        ~TCFFLDataSet();

        auto operator=(const TCFFLDataSet& other)->TCFFLDataSet&;

        auto SetDataSetGetter(const IFLDataSetGetter::Pointer& dataSetGetter)->void;

        auto GetData()const->std::shared_ptr<float[]>;
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
            const int32_t& z0, const int32_t& z1)const->std::shared_ptr<float[]>;
        auto GetDataMemoryOrder()const->MemoryOrder3D;

        auto GetMIPData()const->std::shared_ptr<float[]>;
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
            ->std::shared_ptr<float[]>;
        auto GetMIPDataMemoryOrder()const->MemoryOrder2D;

        auto SetMetaInfo(const TCFFLDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()->TCFFLDataSetMetaInfo;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}