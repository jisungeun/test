#pragma once

#include <memory>
#include <QString>

#include "TCTCFWriterExport.h"

namespace TC::IO::TCFWriter {
    enum class Modality { HT, FL, BF };

    class TCFThumbnailDataSet;

    class TCTCFWriter_API TCFThumbnailWriter {
    public:
        TCFThumbnailWriter();
        ~TCFThumbnailWriter();

        auto SetTargetFilePath(const QString& targetFilePath)->void;
        auto SetTCFThumbnailDataSet(const TCFThumbnailDataSet& tcfThumbnailDataSet)->void;
        auto SetModality(const Modality& modality)->void;

        auto Write()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}