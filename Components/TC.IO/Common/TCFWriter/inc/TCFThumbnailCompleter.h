#pragma once

#include <memory>
#include <QString>

#include "TCTCFWriterExport.h"

namespace TC::IO::TCFWriter {
    class TCTCFWriter_API TCFThumbnailCompleter {
    public:
        TCFThumbnailCompleter();
        ~TCFThumbnailCompleter();

        auto SetTargetTCFFilePath(const QString& targetTCFFilePath)->void;

        auto MakeCompleteGroup()->bool;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}