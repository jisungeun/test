#pragma once

#include <memory>

#include "IHTDataSetGetter.h"
#include "TCTCFWriterExport.h"
#include "TCFWriterDefines.h"
#include "TCFHTDatasetMetaInfo.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFHTDataSet {
    public:
        TCFHTDataSet();
        TCFHTDataSet(const TCFHTDataSet& other);
        ~TCFHTDataSet();

        auto operator=(const TCFHTDataSet& other)->TCFHTDataSet&;

        auto SetDataSetGetter(const IHTDataSetGetter::Pointer& dataSetGetter)->void;

        auto GetData()const->std::shared_ptr<float[]>;
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
            const int32_t& z0, const int32_t& z1)const->std::shared_ptr<float[]>;
        auto GetDataMemoryOrder()const->MemoryOrder3D;

        auto GetMIPData()const->std::shared_ptr<float[]>;
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
            ->std::shared_ptr<float[]>;
        auto GetMIPDataMemoryOrder()const->MemoryOrder2D;

        auto SetMetaInfo(const TCFHTDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()->TCFHTDataSetMetaInfo;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}