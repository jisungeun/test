#pragma once

#include <memory>
#include <QString>
#include "TCTCFWriterExport.h"
#include "TCFBFDataSet.h"

class TCTCFWriter_API TCFBFSectionDataSetWriter {
public:
    TCFBFSectionDataSetWriter();
    ~TCFBFSectionDataSetWriter();

    auto SetTargetFilePath(const QString& targetFilePath)->void;
    auto SetTCFBFDataSet(const TC::TCFWriter::TCFBFDataSet& dataSet)->void;

    auto SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY)->void;
    auto SetWritingStartIndex(const int64_t& indexX, const int64_t& indexY)->void;

    auto Write()->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};