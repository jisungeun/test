#pragma once

#include "enum.h"

BETTER_ENUM(MemoryOrder3D, int32_t, XYZ, YXZ, ZXY, ZYX);
BETTER_ENUM(MemoryOrder2D, uint8_t, XY, YX);
