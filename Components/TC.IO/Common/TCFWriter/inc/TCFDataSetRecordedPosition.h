#pragma once

#include <memory>
#include "TCTCFWriterExport.h"
#include "SIUnit.h"

class TCTCFWriter_API TCFDataSetRecordedPosition{
public:
    TCFDataSetRecordedPosition();
    TCFDataSetRecordedPosition(const TCFDataSetRecordedPosition& other);
    ~TCFDataSetRecordedPosition();

    auto operator=(const TCFDataSetRecordedPosition& other)->TCFDataSetRecordedPosition&;

    auto SetPositions(const double& positionX, const double& positionY, const double& positionZ, 
        const double& positionC, const LengthUnit& unit)->void;

    auto GetPositionX(const LengthUnit& unit)const->double;
    auto GetPositionY(const LengthUnit& unit)const->double;
    auto GetPositionZ(const LengthUnit& unit)const->double;
    auto GetPositionC(const LengthUnit& unit)const->double;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};