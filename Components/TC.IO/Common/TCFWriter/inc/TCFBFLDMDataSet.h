#pragma once

#include <memory>

#include <LdmConfiguration.h>

#include "IBFLDMDataSetGetter.h"
#include "TCFBFDataSetMetaInfo.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFBFLDMDataSet {
    public:
        TCFBFLDMDataSet();
        TCFBFLDMDataSet(const TCFBFLDMDataSet& other);
        ~TCFBFLDMDataSet();

        auto operator=(const TCFBFLDMDataSet& other)->TCFBFLDMDataSet&;

        auto SetDataGetter(const IBFLDMDataSetGetter::Pointer& dataGetter)->void;

        auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration)->void;
        auto GetLdmConfiguration()->IO::LdmCore::LdmConfiguration;

        auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<uint8_t[]>;
        auto GetChannelCount()->int32_t;

        auto SetMetaInfo(const TCFBFDataSetMetaInfo& metaInfo)->void;
        auto GetMetaInfo()const->TCFBFDataSetMetaInfo;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}