#pragma once

#include <memory>
#include <QString>
#include "TCTCFWriterExport.h"
#include "TCFFLDataSet.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFFLSectionDataSetWriter {
    public:
        TCFFLSectionDataSetWriter();
        ~TCFFLSectionDataSetWriter();

        auto SetTargetFilePath(const QString& targetFilePath)->void;
        auto SetTCFFLDataSet(const TCFFLDataSet& dataSet)->void;

        auto SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY, const int64_t& dataSizeZ)->void;
        auto SetWritingStartIndex(const int64_t& indexX, const int64_t& indexY, const int64_t& indexZ = 0)->void;

        auto Write()->bool;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}