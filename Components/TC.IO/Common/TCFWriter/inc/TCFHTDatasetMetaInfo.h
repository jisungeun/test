#pragma once

#include <memory>
#include <QDateTime>

#include <SIUnit.h>

#include "TCFDataSetRecordedPosition.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API TCFHTDataSetMetaInfo {
    public:
        TCFHTDataSetMetaInfo();
        TCFHTDataSetMetaInfo(const TCFHTDataSetMetaInfo& other);
        ~TCFHTDataSetMetaInfo();

        auto operator=(const TCFHTDataSetMetaInfo& other)->TCFHTDataSetMetaInfo&;

        auto SetDataMinMaxValue(const float& minValue, const float& maxValue)->void;

        auto GetDataMinValue()const ->const float&;
        auto GetDataMaxValue()const ->const float&;

        auto SetMIPDataMinMaxValue(const float& minValue, const float& maxValue)->void;

        auto GetDataMIPMinValue()const ->const float&;
        auto GetDataMIPMaxValue()const ->const float&;

        auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
        auto GetSizeX()const->const int32_t&;
        auto GetSizeY()const->const int32_t&;
        auto GetSizeZ()const->const int32_t&;

        auto SetPixelWorldSize(const float& sizeX, const float& sizeY, const float& sizeZ, const LengthUnit& unit)->void;
        auto GetPixelWorldSizeX(const LengthUnit& unit)const->float;
        auto GetPixelWorldSizeY(const LengthUnit& unit)const->float;
        auto GetPixelWorldSizeZ(const LengthUnit& unit)const->float;

        auto SetRecordedTime(const QDateTime& time)->void;
        auto GetRecordedTime()const ->const QDateTime&;

        auto SetElapsedTime(const double& elapsedTime, const TimeUnit& unit)->void;
        auto GetElapsedTime(const TimeUnit& unit)const->double;

        auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
        auto GetTimeFrameIndex()const->const int32_t&;

        auto SetPosition(const TCFDataSetRecordedPosition& position)->void;
        auto GetPosition()const->const TCFDataSetRecordedPosition&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}