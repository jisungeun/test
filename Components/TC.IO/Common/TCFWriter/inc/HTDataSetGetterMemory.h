#pragma once

#include <memory>

#include "IHTDataSetGetter.h"
#include "TCTCFWriterExport.h"

class TCTCFWriter_API HTDataSetGetterMemory final : public IHTDataSetGetter {
public:
    HTDataSetGetterMemory();
    ~HTDataSetGetterMemory();

    auto SetData(const std::shared_ptr<float[]>& data, const MemoryOrder3D& memoryOrder)->void;
    auto SetMIPData(const std::shared_ptr<float[]>& data, const MemoryOrder2D& memoryOrder)->void;
    auto SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;

    auto GetData() const -> std::shared_ptr<float[]> override;
    auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
        const int32_t& z1) const -> std::shared_ptr<float[]> override;
    auto GetDataMemoryOrder() const->MemoryOrder3D override;

    auto GetMIPData() const -> std::shared_ptr<float[]> override;
    auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
        const->std::shared_ptr<float[]> override;
    auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override;
private:
    class Impl;
    std::unique_ptr<Impl> d;

};
