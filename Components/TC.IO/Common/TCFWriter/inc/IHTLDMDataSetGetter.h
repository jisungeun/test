#pragma once

#include <memory>

#include "LdmConfiguration.h"
#include "TCTCFWriterExport.h"

namespace TC::TCFWriter {
    class TCTCFWriter_API IHTLDMDataSetGetter {
    public:
        using Pointer = std::shared_ptr<IHTLDMDataSetGetter>;
        virtual ~IHTLDMDataSetGetter();

        virtual auto SetLdmConfiguration(const IO::LdmCore::LdmConfiguration& ldmConfiguration3D,
            const IO::LdmCore::LdmConfiguration& ldmConfigurationMIP)->void = 0;
        virtual auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> = 0;
        virtual auto GetMipTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> = 0;
    };
}

