#pragma once

#include <memory>
#include <QString>

#include "SIUnit.h"
#include "TCTCFWriterExport.h"

class TCTCFWriter_API TCFBFCompleter {
public:
    TCFBFCompleter();
    ~TCFBFCompleter();

    auto SetTargetTCFFilePath(const QString& targetTCFFilePath)->void;
    auto SetSize(const int32_t& sizeX, const int32_t& sizeY)->void;
    auto SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY, const LengthUnit& unit)->void;

    auto MakeCompleteGroup()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};