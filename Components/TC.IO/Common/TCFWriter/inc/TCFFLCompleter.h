#pragma once

#include <memory>
#include <QString>
#include <QMap>

#include "TCTCFWriterExport.h"
#include "SIUnit.h"

class TCTCFWriter_API TCFFLCompleter {
public:
    typedef int32_t ChannelIndex;
    typedef std::tuple<uint8_t, uint8_t, uint8_t> RGB;
    typedef std::tuple<double, double> EmissionExcitationWaveLength;

    TCFFLCompleter();
    ~TCFFLCompleter();

    auto SetLdmOptionFlag(const bool& ldmOptionFlag)->void;
    auto SetTargetTCFFilePath(const QString& targetTCFFilePath)->void;
    auto SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
    auto SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY, const float& worldPixelSizeZ,
        const LengthUnit& unit)->void;
    auto SetOffsetZ(const double& offsetZ, const LengthUnit& unit)->void;
    auto SetOffsetZCompensation(const double& offsetZCompensation, const LengthUnit& unit)->void;
    auto SetChannelColorMap(const QMap<ChannelIndex, RGB>& channelColorMap)->void;
    auto SetIntensityMap(const QMap<ChannelIndex, int32_t>& intensityMap)->void;
    auto SetExposureTimeMap(const QMap<ChannelIndex, int32_t>& exposureTimeMap)->void;
    auto SetWaveLengthMap(const QMap<ChannelIndex, EmissionExcitationWaveLength>& waveLengthMap)->void;

    auto MakeCompleteGroup()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};