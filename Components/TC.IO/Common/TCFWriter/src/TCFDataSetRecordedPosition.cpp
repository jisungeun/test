#include "TCFDataSetRecordedPosition.h"

class TCFDataSetRecordedPosition::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    double positionX{};
    double positionY{};
    double positionZ{};
    double positionC{};

    LengthUnit positionUnit{LengthUnit::Millimenter};
};

TCFDataSetRecordedPosition::TCFDataSetRecordedPosition() : d(new Impl()) {
}

TCFDataSetRecordedPosition::TCFDataSetRecordedPosition(const TCFDataSetRecordedPosition& other)
    : d(new Impl(*other.d)) {
}

TCFDataSetRecordedPosition::~TCFDataSetRecordedPosition() = default;

auto TCFDataSetRecordedPosition::operator=(const TCFDataSetRecordedPosition& other) -> TCFDataSetRecordedPosition& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFDataSetRecordedPosition::SetPositions(const double& positionX, const double& positionY, const double& positionZ,
    const double& positionC, const LengthUnit& unit) -> void {
    d->positionX = positionX;
    d->positionY = positionY;
    d->positionZ = positionZ;
    d->positionC = positionC;
    d->positionUnit = unit;
}

auto TCFDataSetRecordedPosition::GetPositionX(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionX, d->positionUnit, unit);
}

auto TCFDataSetRecordedPosition::GetPositionY(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionY, d->positionUnit, unit);
}

auto TCFDataSetRecordedPosition::GetPositionZ(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionZ, d->positionUnit, unit);
}

auto TCFDataSetRecordedPosition::GetPositionC(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionC, d->positionUnit, unit);
}
