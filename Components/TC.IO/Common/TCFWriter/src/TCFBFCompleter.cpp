#include "TCFBFCompleter.h"

#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"

class TCFBFCompleter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetTCFFilePath{};
    int32_t sizeX{};
    int32_t sizeY{};

    float worldPixelSizeX{};
    float worldPixelSizeY{};
    LengthUnit worldPixelSizeUnit{LengthUnit::Micrometer};
};

TCFBFCompleter::TCFBFCompleter() : d(new Impl()) {
}

TCFBFCompleter::~TCFBFCompleter() = default;

auto TCFBFCompleter::SetTargetTCFFilePath(const QString & targetTCFFilePath) -> void {
    d->targetTCFFilePath = targetTCFFilePath;
}

auto TCFBFCompleter::SetSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
}

auto TCFBFCompleter::SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY,
    const LengthUnit& unit) -> void {
    d->worldPixelSizeX = worldPixelSizeX;
    d->worldPixelSizeY = worldPixelSizeY;
    d->worldPixelSizeUnit = unit;
}

auto TCFBFCompleter::MakeCompleteGroup() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    try {
        H5::H5File file(d->targetTCFFilePath.toStdString(), H5F_ACC_RDWR);
        {
            auto dataBFGroup = file.openGroup("/Data/BF");

            const auto dataCount = static_cast<int64_t>(dataBFGroup.getNumObjs());
            const auto resolutionX = ConvertUnit(d->worldPixelSizeX, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionY = ConvertUnit(d->worldPixelSizeY, d->worldPixelSizeUnit, LengthUnit::Micrometer);

            WriteAttributeInt64(dataBFGroup, "DataCount", dataCount);
            WriteAttributeDouble(dataBFGroup, "ResolutionX", resolutionX);
            WriteAttributeDouble(dataBFGroup, "ResolutionY", resolutionY);
            WriteAttributeInt64(dataBFGroup, "SizeX", static_cast<int64_t>(d->sizeX));
            WriteAttributeInt64(dataBFGroup, "SizeY", static_cast<int64_t>(d->sizeY));

            dataBFGroup.close();
        }

        file.close();
    } catch (const H5::Exception&) {
    }

    return true;
}
