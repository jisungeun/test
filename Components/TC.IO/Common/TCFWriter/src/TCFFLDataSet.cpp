#include "TCFFLDataSet.h"

namespace TC::TCFWriter {
    class TCFFLDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IFLDataSetGetter::Pointer dataSetGetter{ nullptr };

        TCFFLDataSetMetaInfo metaInfo{};
    };

    TCFFLDataSet::TCFFLDataSet() : d(new Impl()) {
    }

    TCFFLDataSet::TCFFLDataSet(const TCFFLDataSet& other) : d(new Impl(*other.d)) {
    }

    TCFFLDataSet::~TCFFLDataSet() = default;

    auto TCFFLDataSet::operator=(const TCFFLDataSet& other) -> TCFFLDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFFLDataSet::SetDataSetGetter(const IFLDataSetGetter::Pointer& dataSetGetter) -> void {
        d->dataSetGetter = dataSetGetter;
    }

    auto TCFFLDataSet::GetData() const -> std::shared_ptr<float[]> {
        return d->dataSetGetter->GetData();
    }

    auto TCFFLDataSet::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
        const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> {
        return d->dataSetGetter->GetData(x0, x1, y0, y1, z0, z1);
    }

    auto TCFFLDataSet::GetDataMemoryOrder() const -> MemoryOrder3D {
        return d->dataSetGetter->GetDataMemoryOrder();
    }

    auto TCFFLDataSet::GetMIPData() const -> std::shared_ptr<float[]> {
        return d->dataSetGetter->GetMIPData();
    }

    auto TCFFLDataSet::GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
        const int32_t& y1) -> std::shared_ptr<float[]> {
        return d->dataSetGetter->GetMIPData(x0, x1, y0, y1);
    }

    auto TCFFLDataSet::GetMIPDataMemoryOrder() const -> MemoryOrder2D {
        return d->dataSetGetter->GetMIPDataMemoryOrder();
    }

    auto TCFFLDataSet::SetMetaInfo(const TCFFLDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFFLDataSet::GetMetaInfo() -> TCFFLDataSetMetaInfo {
        return d->metaInfo;
    }
}
