#include "TCFFLCompleter.h"

#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"

class TCFFLCompleter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetTCFFilePath{};

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};

    float worldPixelSizeX{};
    float worldPixelSizeY{};
    float worldPixelSizeZ{};
    LengthUnit worldPixelSizeUnit{ LengthUnit::Micrometer };

    double offsetZ{};
    LengthUnit offsetZUnit{ LengthUnit::Micrometer };

    double offsetZCompensation{};
    LengthUnit offsetZCompensationUnit{ LengthUnit::Micrometer };

    QMap<ChannelIndex, RGB> channelColorMap{};
    QMap<ChannelIndex, EmissionExcitationWaveLength> waveLengthMap{};
    QMap<ChannelIndex, int32_t> intensityMap{};
    QMap<ChannelIndex, int32_t> exposureTimeMap{};

    bool ldmOptionFlag{ true };
};

TCFFLCompleter::TCFFLCompleter() : d(new Impl()) {
}

TCFFLCompleter::~TCFFLCompleter() = default;

auto TCFFLCompleter::SetLdmOptionFlag(const bool& ldmOptionFlag) -> void {
    d->ldmOptionFlag = ldmOptionFlag;
}

auto TCFFLCompleter::SetTargetTCFFilePath(const QString & targetTCFFilePath) -> void {
    d->targetTCFFilePath = targetTCFFilePath;
}

auto TCFFLCompleter::SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;
}

auto TCFFLCompleter::SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY,
    const float& worldPixelSizeZ, const LengthUnit& unit) -> void {
    d->worldPixelSizeX = worldPixelSizeX;
    d->worldPixelSizeY = worldPixelSizeY;
    d->worldPixelSizeZ = worldPixelSizeZ;
    d->worldPixelSizeUnit = unit;
}

auto TCFFLCompleter::SetOffsetZ(const double& offsetZ, const LengthUnit& unit) -> void {
    d->offsetZ = offsetZ;
    d->offsetZUnit = unit;
}

auto TCFFLCompleter::SetOffsetZCompensation(const double& offsetZCompensation, const LengthUnit& unit) -> void {
    d->offsetZCompensation = offsetZCompensation;
    d->offsetZCompensationUnit = unit;
}

auto TCFFLCompleter::SetChannelColorMap(const QMap<ChannelIndex, RGB>& channelColorMap) -> void {
    d->channelColorMap = channelColorMap;
}

auto TCFFLCompleter::SetIntensityMap(const QMap<ChannelIndex, int32_t>& intensityMap) -> void {
    d->intensityMap = intensityMap;
}

auto TCFFLCompleter::SetExposureTimeMap(const QMap<ChannelIndex, int32_t>& exposureTimeMap) -> void {
    d->exposureTimeMap = exposureTimeMap;
}

auto TCFFLCompleter::SetWaveLengthMap(const QMap<ChannelIndex, EmissionExcitationWaveLength>& waveLengthMap) -> void {
    d->waveLengthMap = waveLengthMap;
}

auto TCFFLCompleter::MakeCompleteGroup() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };

    try {
        H5::H5File file(d->targetTCFFilePath.toStdString(), H5F_ACC_RDWR);
        {
            const auto data3dFLGroup = file.openGroup("/Data/3DFL");

            const auto numberOfChannels = static_cast<int64_t>(data3dFLGroup.getNumObjs());
            auto numberOfTotalData = 0;
            for (auto groupIndex = 0; groupIndex < numberOfChannels; ++groupIndex) {
                const auto channelGroupName = data3dFLGroup.getObjnameByIdx(groupIndex);
                const auto channelIndex = std::stoi(channelGroupName.substr(channelGroupName.size() - 1, 1));

                const auto colorRGB = d->channelColorMap[channelIndex];
                const auto r = std::get<0>(colorRGB);
                const auto g = std::get<1>(colorRGB);
                const auto b = std::get<2>(colorRGB);

                const auto emissionWaveLength = std::get<0>(d->waveLengthMap[channelIndex]);
                const auto excitationWaveLength = std::get<1>(d->waveLengthMap[channelIndex]);

                const auto intensity = d->intensityMap[channelIndex];
                const auto exposureTime = d->exposureTimeMap[channelIndex];

                const auto channelGroup = data3dFLGroup.openGroup(channelGroupName);
                if (!channelGroup.attrExists("Excitation")) {
                    WriteAttributeUint8(channelGroup, "ColorR", r);
                    WriteAttributeUint8(channelGroup, "ColorG", g);
                    WriteAttributeUint8(channelGroup, "ColorB", b);

                    WriteAttributeDouble(channelGroup, "Emission", emissionWaveLength);
                    WriteAttributeDouble(channelGroup, "Excitation", excitationWaveLength);

                    WriteAttributeInt64(channelGroup, "Intensity", intensity);
                    WriteAttributeInt64(channelGroup, "ExposureTime", exposureTime);
                }
                const auto dataNumber = channelGroup.getNumObjs();
               
                numberOfTotalData += dataNumber;
            }
            const auto dataCount = static_cast<int64_t>(static_cast<float>(numberOfTotalData) / static_cast<float>(numberOfChannels));

            const auto resolutionX = ConvertUnit(d->worldPixelSizeX, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionY = ConvertUnit(d->worldPixelSizeY, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionZ = ConvertUnit(d->worldPixelSizeZ, d->worldPixelSizeUnit, LengthUnit::Micrometer);

            double minValueForAllDataSet = std::numeric_limits<double>::max();
            double maxValueForAllDataSet = std::numeric_limits<double>::lowest();
            for (auto channelIndex = 0; channelIndex < numberOfChannels; ++channelIndex) {
                const auto channelGroupName = data3dFLGroup.getObjnameByIdx(channelIndex);

                const auto channelGroup = data3dFLGroup.openGroup(channelGroupName);
                const auto dataNumber = channelGroup.getNumObjs();

                for (auto dataSetIndex = 0; dataSetIndex < dataNumber; ++dataSetIndex) {
                    const auto dataSetName = channelGroup.getObjnameByIdx(dataSetIndex);

                    double minValue, maxValue;

                    if (d->ldmOptionFlag) {
                        const auto ldmGroup = channelGroup.openGroup(dataSetName);

                        minValue = ReadAttributeDouble(ldmGroup, "MinIntensity");
                        maxValue = ReadAttributeDouble(ldmGroup, "MaxIntensity");
                    } else {
                        const auto dataSet = channelGroup.openDataSet(dataSetName);

                        minValue = ReadAttributeDouble(dataSet, "MinIntensity");
                        maxValue = ReadAttributeDouble(dataSet, "MaxIntensity");
                    }

                    if (minValue < minValueForAllDataSet) {
                        minValueForAllDataSet = minValue;
                    }
                    if (maxValue > maxValueForAllDataSet) {
                        maxValueForAllDataSet = maxValue;
                    }
                }
            }

            const auto offsetZ = ConvertUnit(d->offsetZ, d->offsetZUnit, LengthUnit::Micrometer);
            const auto offsetZCompensation = ConvertUnit(d->offsetZCompensation, d->offsetZCompensationUnit, LengthUnit::Micrometer);

            if (!data3dFLGroup.attrExists("SizeZ")) {
                WriteAttributeInt64(data3dFLGroup, "Channels", numberOfChannels);
                WriteAttributeInt64(data3dFLGroup, "DataCount", dataCount);
                WriteAttributeDouble(data3dFLGroup, "OffsetZ", offsetZ);
                WriteAttributeDouble(data3dFLGroup, "OffsetZCompensation", offsetZCompensation);
                WriteAttributeDouble(data3dFLGroup, "ResolutionX", resolutionX);
                WriteAttributeDouble(data3dFLGroup, "ResolutionY", resolutionY);
                WriteAttributeDouble(data3dFLGroup, "ResolutionZ", resolutionZ);
                WriteAttributeDouble(data3dFLGroup, "MinIntensity", minValueForAllDataSet);
                WriteAttributeDouble(data3dFLGroup, "MaxIntensity", maxValueForAllDataSet);
                WriteAttributeInt64(data3dFLGroup, "SizeX", static_cast<int64_t>(d->sizeX));
                WriteAttributeInt64(data3dFLGroup, "SizeY", static_cast<int64_t>(d->sizeY));
                WriteAttributeInt64(data3dFLGroup, "SizeZ", static_cast<int64_t>(d->sizeZ));
            }
        }

        {
            const auto data2dFLMIPGroup = file.openGroup("/Data/2DFLMIP");

            const auto numberOfChannels = static_cast<int64_t>(data2dFLMIPGroup.getNumObjs());
            auto numberOfTotalData = 0;
            for (auto groupIndex = 0; groupIndex < numberOfChannels; ++groupIndex) {
                const auto channelGroupName = data2dFLMIPGroup.getObjnameByIdx(groupIndex);
                const auto channelIndex = std::stoi(channelGroupName.substr(channelGroupName.size() - 1, 1));

                const auto colorRGB = d->channelColorMap[channelIndex];
                const auto r = std::get<0>(colorRGB);
                const auto g = std::get<1>(colorRGB);
                const auto b = std::get<2>(colorRGB);

                const auto emissionWaveLength = std::get<0>(d->waveLengthMap[channelIndex]);
                const auto excitationWaveLength = std::get<1>(d->waveLengthMap[channelIndex]);

                const auto intensity = d->intensityMap[channelIndex];
                const auto exposureTime = d->exposureTimeMap[channelIndex];

                auto channelGroup = data2dFLMIPGroup.openGroup(channelGroupName);
                if (!channelGroup.attrExists("Excitation")) {
                    WriteAttributeUint8(channelGroup, "ColorR", r);
                    WriteAttributeUint8(channelGroup, "ColorG", g);
                    WriteAttributeUint8(channelGroup, "ColorB", b);

                    WriteAttributeDouble(channelGroup, "Emission", emissionWaveLength);
                    WriteAttributeDouble(channelGroup, "Excitation", excitationWaveLength);

                    WriteAttributeInt64(channelGroup, "Intensity", intensity);
                    WriteAttributeInt64(channelGroup, "ExposureTime", exposureTime);
                }
                const auto dataNumber = channelGroup.getNumObjs();

                channelGroup.close();

                numberOfTotalData += dataNumber;
            }
            const auto dataCount = static_cast<int64_t>(static_cast<float>(numberOfTotalData) / static_cast<float>(numberOfChannels));

            const auto resolutionX = ConvertUnit(d->worldPixelSizeX, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionY = ConvertUnit(d->worldPixelSizeY, d->worldPixelSizeUnit, LengthUnit::Micrometer);

            double minValueForAllDataSet = std::numeric_limits<double>::max();
            double maxValueForAllDataSet = std::numeric_limits<double>::lowest();
            for (auto channelIndex = 0; channelIndex < numberOfChannels; ++channelIndex) {
                const auto channelGroupName = data2dFLMIPGroup.getObjnameByIdx(channelIndex);

                const auto channelGroup = data2dFLMIPGroup.openGroup(channelGroupName);
                const auto dataNumber = channelGroup.getNumObjs();

                for (auto dataSetIndex = 0; dataSetIndex < dataNumber; ++dataSetIndex) {
                    const auto dataSetName = channelGroup.getObjnameByIdx(dataSetIndex);

                    double minValue, maxValue;

                    if (d->ldmOptionFlag) {
                        const auto ldmGroup = channelGroup.openGroup(dataSetName);

                        minValue = ReadAttributeDouble(ldmGroup, "MinIntensity");
                        maxValue = ReadAttributeDouble(ldmGroup, "MaxIntensity");
                    } else {
                        const auto dataSet = channelGroup.openDataSet(dataSetName);

                        minValue = ReadAttributeDouble(dataSet, "MinIntensity");
                        maxValue = ReadAttributeDouble(dataSet, "MaxIntensity");
                    }

                    if (minValue < minValueForAllDataSet) {
                        minValueForAllDataSet = minValue;
                    }
                    if (maxValue > maxValueForAllDataSet) {
                        maxValueForAllDataSet = maxValue;
                    }
                }
            }
            if (!data2dFLMIPGroup.attrExists("SizeY")) {
                WriteAttributeInt64(data2dFLMIPGroup, "Channels", numberOfChannels);
                WriteAttributeInt64(data2dFLMIPGroup, "DataCount", dataCount);
                WriteAttributeDouble(data2dFLMIPGroup, "ResolutionX", resolutionX);
                WriteAttributeDouble(data2dFLMIPGroup, "ResolutionY", resolutionY);
                WriteAttributeDouble(data2dFLMIPGroup, "MinIntensity", minValueForAllDataSet);
                WriteAttributeDouble(data2dFLMIPGroup, "MaxIntensity", maxValueForAllDataSet);
                WriteAttributeInt64(data2dFLMIPGroup, "SizeX", static_cast<int64_t>(d->sizeX));
                WriteAttributeInt64(data2dFLMIPGroup, "SizeY", static_cast<int64_t>(d->sizeY));
            }
        }

        file.close();
    } catch (const H5::Exception&) {
        return false;
    }


    return true;
}
