#include "TCFBFDataSet.h"

namespace TC::TCFWriter {
    class TCFBFDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IBFDataSetGetter::Pointer dataSetGetter{ nullptr };

        TCFBFDataSetMetaInfo metaInfo{};
    };

    TCFBFDataSet::TCFBFDataSet() : d(new Impl()) {
    }

    TCFBFDataSet::TCFBFDataSet(const TCFBFDataSet& other) : d(new Impl(*other.d)) {
    }

    TCFBFDataSet::~TCFBFDataSet() = default;

    auto TCFBFDataSet::operator=(const TCFBFDataSet& other) -> TCFBFDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFBFDataSet::SetDataSetGetter(const IBFDataSetGetter::Pointer& dataSetGetter) -> void {
        d->dataSetGetter = dataSetGetter;
    }

    auto TCFBFDataSet::GetData() const -> std::shared_ptr<uint8_t[]> {
        return d->dataSetGetter->GetData();
    }

    auto TCFBFDataSet::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
        const int32_t& y1) const -> std::shared_ptr<uint8_t[]> {
        return d->dataSetGetter->GetData(x0, x1, y0, y1);
    }

    auto TCFBFDataSet::GetDataMemoryOrder() const -> MemoryOrder3D {
        return d->dataSetGetter->GetDataMemoryOrder();
    }

    auto TCFBFDataSet::GetChannelCount() const -> int32_t {
        return d->dataSetGetter->GetColorChannelCount();
    }

    auto TCFBFDataSet::SetMetaInfo(const TCFBFDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFBFDataSet::GetMetaInfo() -> TCFBFDataSetMetaInfo {
        return d->metaInfo;
    }
}
