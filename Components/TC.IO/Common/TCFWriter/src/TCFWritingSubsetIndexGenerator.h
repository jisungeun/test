#pragma once

#include <memory>

#include "SIUnit.h"

class TCFWritingSubsetIndexGenerator {
public:
    TCFWritingSubsetIndexGenerator();
    ~TCFWritingSubsetIndexGenerator();

    auto SetWholeSize(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ)->void;
    auto SetSubsetOffset(const int64_t& offsetX, const int64_t& offsetY, const int64_t& offsetZ)->void;

    auto SetSubsetDataLimitationSize(const double& size, const DataSizeUnit& dataSizeUnit)->void;
    auto SetElementSize(const double& size, const DataSizeUnit& dataSizeUnit)->void;

    auto Generate()->bool;

    auto GetSubsetDataCount()const->int32_t;

    auto GetWritingDataIndexStartX(const int32_t& subsetDataIndex)const->int64_t;
    auto GetWritingDataIndexEndX(const int32_t& subsetDataIndex)const->int64_t;
    auto GetWritingDataIndexStartY(const int32_t& subsetDataIndex)const->int64_t;
    auto GetWritingDataIndexEndY(const int32_t& subsetDataIndex)const->int64_t;
    auto GetWritingDataIndexStartZ(const int32_t& subsetDataIndex)const->int64_t;
    auto GetWritingDataIndexEndZ(const int32_t& subsetDataIndex)const->int64_t;

    auto GetSubsetIndexStartX(const int32_t& subsetDataIndex)const->int64_t;
    auto GetSubsetIndexEndX(const int32_t& subsetDataIndex)const->int64_t;
    auto GetSubsetIndexStartY(const int32_t& subsetDataIndex)const->int64_t;
    auto GetSubsetIndexEndY(const int32_t& subsetDataIndex)const->int64_t;
    auto GetSubsetIndexStartZ(const int32_t& subsetDataIndex)const->int64_t;
    auto GetSubsetIndexEndZ(const int32_t& subsetDataIndex)const->int64_t;
    
private:
    class Impl;
    std::unique_ptr<Impl> d;
};