#include "MemoryDataCropper.h"

class MemoryDataCropper::Impl {
public:
    Impl() = default;
    ~Impl() {
        delete[] this->cropMemory;
    }

    const uint8_t* memory{ nullptr };
    int64_t elementSizeInByte{ 0 };

    int64_t sizeX{};
    int64_t sizeY{};
    int64_t sizeZ{};
    MemoryOrder3D memoryOrder{ MemoryOrder3D::XYZ };

    int64_t x0{};
    int64_t x1{};
    int64_t y0{};
    int64_t y1{};
    int64_t z0{};
    int64_t z1{};

    uint8_t* cropMemory{ nullptr };
};

MemoryDataCropper::MemoryDataCropper() : d(new Impl()) {
}

MemoryDataCropper::~MemoryDataCropper() = default;

auto MemoryDataCropper::SetMemory(const uint8_t* memory, const int32_t& elementSizeInByte) -> void {
    d->memory = memory;
    d->elementSizeInByte = static_cast<int64_t>(elementSizeInByte);
}

auto MemoryDataCropper::SetMemoryInfo(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ,
    const MemoryOrder3D& memoryOrder) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;

    d->memoryOrder = memoryOrder;
}

auto MemoryDataCropper::SetCropInfo(const int64_t& x0, const int64_t& x1, const int64_t& y0, const int64_t& y1,
    const int64_t& z0, const int64_t& z1) -> void {
    d->x0 = x0;
    d->x1 = x1;
    d->y0 = y0;
    d->y1 = y1;
    d->z0 = z0;
    d->z1 = z1;
}

auto MemoryDataCropper::Crop() -> bool {
    const auto cropSizeX = d->x1 - d->x0 + 1;
    const auto cropSizeY = d->y1 - d->y0 + 1;
    const auto cropSizeZ = d->z1 - d->z0 + 1;

    const auto numberOfElements = cropSizeX * cropSizeY * cropSizeZ;

    delete[] d->cropMemory;
    d->cropMemory = new uint8_t[numberOfElements * d->elementSizeInByte]();

    int64_t firstOrderCropSize, secondOrderCropSize, thirdOrderCropSize;
    int64_t firstOrderOffset, secondOrderOffset, thirdOrderOffset;
    int64_t firstOrderSize, secondOrderSize;
    
    if (d->memoryOrder == +MemoryOrder3D::XYZ) {
        firstOrderCropSize = cropSizeX; secondOrderCropSize = cropSizeY; thirdOrderCropSize = cropSizeZ;
        firstOrderOffset = d->x0; secondOrderOffset = d->y0; thirdOrderOffset = d->z0;
        firstOrderSize = d->sizeX; secondOrderSize = d->sizeY;
    } else if (d->memoryOrder == +MemoryOrder3D::YXZ) {
        firstOrderCropSize = cropSizeY; secondOrderCropSize = cropSizeX; thirdOrderCropSize = cropSizeZ;
        firstOrderOffset = d->y0; secondOrderOffset = d->x0; thirdOrderOffset = d->z0;
        firstOrderSize = d->sizeY; secondOrderSize = d->sizeX;
    } else if (d->memoryOrder == +MemoryOrder3D::ZXY) {
        firstOrderCropSize = cropSizeZ; secondOrderCropSize = cropSizeX; thirdOrderCropSize = cropSizeY;
        firstOrderOffset = d->z0; secondOrderOffset = d->x0; thirdOrderOffset = d->y0;
        firstOrderSize = d->sizeZ; secondOrderSize = d->sizeX;
    } else if (d->memoryOrder == +MemoryOrder3D::ZYX) {
        firstOrderCropSize = cropSizeZ; secondOrderCropSize = cropSizeY; thirdOrderCropSize = cropSizeX;
        firstOrderOffset = d->z0; secondOrderOffset = d->y0; thirdOrderOffset = d->x0;
        firstOrderSize = d->sizeZ; secondOrderSize = d->sizeY;
    } else {
        return false;
    }

    for (int64_t thirdOrderIndex = 0; thirdOrderIndex < thirdOrderCropSize; ++thirdOrderIndex) {
        for (int64_t secondOrderIndex = 0; secondOrderIndex < secondOrderCropSize; ++secondOrderIndex) {
            const auto srcIndex = (firstOrderOffset + (secondOrderOffset + secondOrderIndex) * firstOrderSize + 
                (thirdOrderOffset + thirdOrderIndex) * firstOrderSize * secondOrderSize) * d->elementSizeInByte;
            const auto destIndex = (secondOrderIndex * firstOrderCropSize +
                thirdOrderIndex * firstOrderCropSize * secondOrderCropSize) * d->elementSizeInByte;

            std::copy_n(&d->memory[srcIndex], firstOrderCropSize * d->elementSizeInByte, &d->cropMemory[destIndex]);
        }
    }
    
    return true;
}

auto MemoryDataCropper::GetCroppedMemory(uint8_t* copyDestMemory) const -> void {
    const auto cropSizeX = d->x1 - d->x0 + 1;
    const auto cropSizeY = d->y1 - d->y0 + 1;
    const auto cropSizeZ = d->z1 - d->z0 + 1;

    const auto numberOfElements = cropSizeX * cropSizeY * cropSizeZ;

    std::copy_n(d->cropMemory, numberOfElements * d->elementSizeInByte, copyDestMemory);
}
