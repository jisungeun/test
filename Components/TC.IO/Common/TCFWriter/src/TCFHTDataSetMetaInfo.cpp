#include "TCFHTDataSetMetaInfo.h"

namespace TC::TCFWriter {
    class TCFHTDataSetMetaInfo::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        float dataMinValue{};
        float dataMaxValue{};

        float dataMIPMinValue{};
        float dataMIPMaxValue{};

        int32_t dataSizeX{};
        int32_t dataSizeY{};
        int32_t dataSizeZ{};

        float pixelWorldSizeX{};
        float pixelWorldSizeY{};
        float pixelWorldSizeZ{};
        LengthUnit pixelWorldSizeUnit{ LengthUnit::Micrometer };

        QDateTime recordedTime{};
        double elapsedTime{};
        TimeUnit elapsedTimeUnit{ TimeUnit::Second };

        int32_t timeFrameIndex{};

        TCFDataSetRecordedPosition position{};
    };

    TCFHTDataSetMetaInfo::TCFHTDataSetMetaInfo() : d(std::make_unique<Impl>()) {
    }

    TCFHTDataSetMetaInfo::TCFHTDataSetMetaInfo(const TCFHTDataSetMetaInfo& other) : d(std::make_unique<Impl>(*other.d)) {
    }

    TCFHTDataSetMetaInfo::~TCFHTDataSetMetaInfo() = default;

    auto TCFHTDataSetMetaInfo::operator=(const TCFHTDataSetMetaInfo& other) -> TCFHTDataSetMetaInfo& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFHTDataSetMetaInfo::SetDataMinMaxValue(const float& minValue, const float& maxValue) -> void {
        d->dataMinValue = minValue;
        d->dataMaxValue = maxValue;
    }

    auto TCFHTDataSetMetaInfo::GetDataMinValue() const -> const float& {
        return d->dataMinValue;
    }

    auto TCFHTDataSetMetaInfo::GetDataMaxValue() const -> const float& {
        return d->dataMaxValue;
    }

    auto TCFHTDataSetMetaInfo::SetMIPDataMinMaxValue(const float& minValue, const float& maxValue) -> void {
        d->dataMIPMinValue = minValue;
        d->dataMIPMaxValue = maxValue;
    }

    auto TCFHTDataSetMetaInfo::GetDataMIPMinValue() const -> const float& {
        return d->dataMIPMinValue;
    }

    auto TCFHTDataSetMetaInfo::GetDataMIPMaxValue() const -> const float& {
        return d->dataMIPMaxValue;
    }

    auto TCFHTDataSetMetaInfo::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
        d->dataSizeX = sizeX;
        d->dataSizeY = sizeY;
        d->dataSizeZ = sizeZ;
    }

    auto TCFHTDataSetMetaInfo::GetSizeX() const -> const int32_t& {
        return d->dataSizeX;
    }

    auto TCFHTDataSetMetaInfo::GetSizeY() const -> const int32_t& {
        return d->dataSizeY;
    }

    auto TCFHTDataSetMetaInfo::GetSizeZ() const -> const int32_t& {
        return d->dataSizeZ;
    }

    auto TCFHTDataSetMetaInfo::SetPixelWorldSize(const float& sizeX, const float& sizeY, const float& sizeZ,
        const LengthUnit& unit) -> void {
        d->pixelWorldSizeX = sizeX;
        d->pixelWorldSizeY = sizeY;
        d->pixelWorldSizeZ = sizeZ;
        d->pixelWorldSizeUnit = unit;
    }

    auto TCFHTDataSetMetaInfo::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit);
    }

    auto TCFHTDataSetMetaInfo::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit);
    }

    auto TCFHTDataSetMetaInfo::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit);
    }

    auto TCFHTDataSetMetaInfo::SetRecordedTime(const QDateTime& time) -> void {
        d->recordedTime = time;
    }

    auto TCFHTDataSetMetaInfo::GetRecordedTime() const -> const QDateTime& {
        return d->recordedTime;
    }

    auto TCFHTDataSetMetaInfo::SetElapsedTime(const double& elapsedTime, const TimeUnit& unit) -> void {
        d->elapsedTime = elapsedTime;
        d->elapsedTimeUnit = unit;
    }

    auto TCFHTDataSetMetaInfo::GetElapsedTime(const TimeUnit& unit) const -> double {
        return ConvertUnit(d->elapsedTime, d->elapsedTimeUnit, unit);
    }

    auto TCFHTDataSetMetaInfo::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto TCFHTDataSetMetaInfo::GetTimeFrameIndex() const -> const int32_t& {
        return d->timeFrameIndex;
    }

    auto TCFHTDataSetMetaInfo::SetPosition(const TCFDataSetRecordedPosition& position) -> void {
        d->position = position;
    }

    auto TCFHTDataSetMetaInfo::GetPosition() const -> const TCFDataSetRecordedPosition& {
        return d->position;
    }
}
