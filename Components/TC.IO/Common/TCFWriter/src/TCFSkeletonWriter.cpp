#define LOGGER_TAG "[TCFSkeletonWriter]"
#include "TCFSkeletonWriter.h"

#include "H5Cpp.h"
#include "HDF5Mutex.h"

#include "TCLogger.h"

class TCFSkeletonWriter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString tcfFilePath{};
    TCFSkeletonConfig config{};
};

TCFSkeletonWriter::TCFSkeletonWriter() : d(new Impl()) {
}

TCFSkeletonWriter::~TCFSkeletonWriter() = default;

auto TCFSkeletonWriter::SetTargetFilePath(const QString & tcfFilePath) -> void {
    d->tcfFilePath = tcfFilePath;
}

auto TCFSkeletonWriter::SetConfig(const TCFSkeletonConfig & config) -> void {
    d->config = config;
}

auto TCFSkeletonWriter::Write() -> bool {
    try {
        H5::H5File file(d->tcfFilePath.toStdString(), H5F_ACC_TRUNC);
        {
            const auto dataGroup = file.createGroup("Data");
            if (d->config.GetHTIncludingFlag()) {
                const auto dataHTGroup = dataGroup.createGroup("3D");
                const auto dataHTMIPGroup = dataGroup.createGroup("2DMIP");
            }
            if (d->config.GetFLIncludingFlag()) {
                const auto dataFLGroup = dataGroup.createGroup("3DFL");
                const auto dataFLMIPGroup = dataGroup.createGroup("2DFLMIP");
            }
            if (d->config.GetBFIncludingFlag()) {
                const auto dataBFGroup = dataGroup.createGroup("BF");
            }
        }
        {
            const auto infoGroup = file.createGroup("Info");
        }
        {
            const auto thumbnailGroup = file.createGroup("Thumbnail");
            if (d->config.GetHTIncludingFlag()) {
                const auto thumbnailHTGroup = thumbnailGroup.createGroup("HT");
            }
            if (d->config.GetFLIncludingFlag()) {
                const auto thumbnailFLGroup = thumbnailGroup.createGroup("FL");
            }
            if (d->config.GetBFIncludingFlag()) {
                const auto thumbnailBFGroup = thumbnailGroup.createGroup("BF");
            }
        }

        file.close();
    } catch (const H5::Exception& exception) {
        QLOG_ERROR() << exception.getCDetailMsg();
        return false;
    }

    return true;
}
