#pragma once

#include <memory>

#include "TCFWriterDefines.h"

class MemoryDataCropper {
public:
    MemoryDataCropper();
    ~MemoryDataCropper();

    auto SetMemory(const uint8_t* memory, const int32_t& elementSizeInByte)->void;
    auto SetMemoryInfo(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ, const MemoryOrder3D& memoryOrder)->void;
    auto SetCropInfo(const int64_t& x0, const int64_t& x1, const int64_t& y0, const int64_t& y1, const int64_t& z0, const int64_t& z1)->void;

    auto Crop()->bool;

    auto GetCroppedMemory(uint8_t* copyDestMemory)const->void;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};