#include "TCFCommonInfo.h"

class TCFCommonInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    QDateTime createDate{};
    QString dataID{};
    QString description{};
    QString deviceHost{};
    QString deviceSerial{};
    QString deviceModelType{};
    QString deviceSoftwareVersion{};
    QString formatVersion{};
    QDateTime recordingTime{};
    QString softwareVersion{};
    QString title{};
    QString uniqueID{};
    QString userID{};
};

TCFCommonInfo::TCFCommonInfo() : d(new Impl()) {
}

TCFCommonInfo::TCFCommonInfo(const TCFCommonInfo& other) : d(new Impl(*other.d)) {
}

TCFCommonInfo::~TCFCommonInfo() = default;

auto TCFCommonInfo::operator=(const TCFCommonInfo& other) -> TCFCommonInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFCommonInfo::SetCreateDate(const QDateTime& createDate) -> void {
    d->createDate = createDate;
}

auto TCFCommonInfo::GetCreateDate() const -> const QDateTime& {
    return d->createDate;
}

auto TCFCommonInfo::SetDataID(const QString& dataID) -> void {
    d->dataID = dataID;
}

auto TCFCommonInfo::GetDataID() const -> const QString& {
    return d->dataID;
}

auto TCFCommonInfo::SetDescription(const QString& description) -> void {
    d->description = description;
}

auto TCFCommonInfo::GetDescription() const -> const QString& {
    return d->description;
}

auto TCFCommonInfo::SetDeviceHost(const QString& deviceHost) -> void {
    d->deviceHost = deviceHost;
}

auto TCFCommonInfo::GetDeviceHost() const -> const QString& {
    return d->deviceHost;
}

auto TCFCommonInfo::SetDeviceSerial(const QString& deviceSerial) -> void {
    d->deviceSerial = deviceSerial;
}

auto TCFCommonInfo::GetDeviceSerial() const -> const QString& {
    return d->deviceSerial;
}

auto TCFCommonInfo::SetDeviceModelType(const QString& deviceModelType) -> void {
    d->deviceModelType = deviceModelType;
}

auto TCFCommonInfo::GetDeviceModelType() const -> const QString& {
    return d->deviceModelType;
}

auto TCFCommonInfo::SetDeviceSoftwareVersion(const QString& deviceSoftwareVersion) -> void {
    d->deviceSoftwareVersion = deviceSoftwareVersion;
}

auto TCFCommonInfo::GetDeviceSoftwareVersion() const -> const QString& {
    return d->deviceSoftwareVersion;
}

auto TCFCommonInfo::SetFormatVersion(const QString& formatVersion) -> void {
    d->formatVersion = formatVersion;
}

auto TCFCommonInfo::GetFormatVersion() const -> const QString& {
    return d->formatVersion;
}

auto TCFCommonInfo::SetRecordingTime(const QDateTime& recordingTime) -> void {
    d->recordingTime = recordingTime;
}

auto TCFCommonInfo::GetRecordingTime() const -> const QDateTime& {
    return d->recordingTime;
}

auto TCFCommonInfo::SetSoftwareVersion(const QString& softwareVersion) -> void {
    d->softwareVersion = softwareVersion;
}

auto TCFCommonInfo::GetSoftwareVersion() const -> const QString& {
    return d->softwareVersion;
}

auto TCFCommonInfo::SetTitle(const QString& title) -> void {
    d->title = title;
}

auto TCFCommonInfo::GetTitle() const -> const QString& {
    return d->title;
}

auto TCFCommonInfo::SetUniqueID(const QString& uniqueID) -> void {
    d->uniqueID = uniqueID;
}

auto TCFCommonInfo::GetUniqueID() const -> const QString& {
    return d->uniqueID;
}

auto TCFCommonInfo::SetUserID(const QString& userID) -> void {
    d->userID = userID;
}

auto TCFCommonInfo::GetUserID() const -> const QString& {
    return d->userID;
}
