#include "TCFTileInfo.h"

class TCFTileInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    double overlapLengthH{};
    double overlapLengthV{};
    LengthUnit overlapLengthUnit{LengthUnit::Millimenter};

    TCFDataSetRecordedPosition position{};

    int32_t tileNumberH{};
    int32_t tileNumberV{};

    int32_t tileIndexColumn{};
    int32_t tileIndexRow{};
};

TCFTileInfo::TCFTileInfo() : d(new Impl()) {
}

TCFTileInfo::TCFTileInfo(const TCFTileInfo& other) : d(new Impl(*other.d)) {
}

TCFTileInfo::~TCFTileInfo() = default;

auto TCFTileInfo::operator=(const TCFTileInfo & other) -> TCFTileInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFTileInfo::SetOverlapLength(const double& overlapH, const double& overlapV, const LengthUnit& unit) -> void {
    d->overlapLengthH = overlapH;
    d->overlapLengthV = overlapV;
    d->overlapLengthUnit = unit;
}

auto TCFTileInfo::GetOverlapLengthH(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->overlapLengthH, d->overlapLengthUnit, unit);
}

auto TCFTileInfo::GetOverlapLengthV(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->overlapLengthV, d->overlapLengthUnit, unit);
}

auto TCFTileInfo::SetTCFDataSetRecordedPosition(const TCFDataSetRecordedPosition& position) -> void {
    d->position = position;
}

auto TCFTileInfo::GetTCFDataSetRecordedPosition() const -> const TCFDataSetRecordedPosition& {
    return d->position;
}

auto TCFTileInfo::SetTileNumber(const int32_t& tileNumberH, const int32_t& tileNumberV) -> void {
    d->tileNumberH = tileNumberH;
    d->tileNumberV = tileNumberV;
}

auto TCFTileInfo::GetTileNumberH() const -> const int32_t& {
    return d->tileNumberH;
}

auto TCFTileInfo::GetTileNumberV() const -> const int32_t& {
    return d->tileNumberV;
}

auto TCFTileInfo::SetTileIndex(const int32_t& tileIndexColumn, const int32_t& tileIndexRow) -> void {
    d->tileIndexColumn = tileIndexColumn;
    d->tileIndexRow = tileIndexRow;
}

auto TCFTileInfo::GetTileIndexColumn() const -> const int32_t& {
    return d->tileIndexColumn;
}

auto TCFTileInfo::GetTileIndexRow() const -> const int32_t& {
    return d->tileIndexRow;
}
