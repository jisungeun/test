#define LOGGER_TAG "[TCFThumbnailCompleter]"

#include "TCFThumbnailCompleter.h"

#include <QFile>

#include "HDF5Mutex.h"
#include "H5Cpp.h"

#include "TCFWriterUtility.h"
#include "TCLogger.h"

namespace TC::IO::TCFWriter {
    class TCFThumbnailCompleter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString targetTCFFilePath{};
    };

    TCFThumbnailCompleter::TCFThumbnailCompleter() : d(new Impl()) {
    }

    TCFThumbnailCompleter::~TCFThumbnailCompleter() = default;

    auto TCFThumbnailCompleter::SetTargetTCFFilePath(const QString& targetTCFFilePath) -> void {
        d->targetTCFFilePath = targetTCFFilePath;
    }

    auto TCFThumbnailCompleter::MakeCompleteGroup() -> bool {
        if (d->targetTCFFilePath.isEmpty()) {
            return false;
        }

        if (!QFile::exists(d->targetTCFFilePath)) {
            return false;
        }
        
        HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };
        try {
            if (!H5::H5File::isHdf5(d->targetTCFFilePath.toStdString())) {
                return false;
            }

            H5::H5File file(d->targetTCFFilePath.toStdString(), H5F_ACC_RDWR);

            if (!file.exists("Thumbnail")) {
                return false;
            }

            const auto thumbnailGroup = file.openGroup("Thumbnail");

            if (thumbnailGroup.exists("HT")) {
                const auto htThumbnailGroup = thumbnailGroup.openGroup("HT");
                const auto htDataCount = htThumbnailGroup.getNumObjs();

                WriteAttributeInt64(htThumbnailGroup, "DataCount", htDataCount);
            }

            if (thumbnailGroup.exists("FL")) {
                const auto flThumbnailGroup = thumbnailGroup.openGroup("FL");
                const auto flDataCount = flThumbnailGroup.getNumObjs();

                WriteAttributeInt64(flThumbnailGroup, "DataCount", flDataCount);
            }

            if (thumbnailGroup.exists("BF")) {
                const auto bfThumbnailGroup = thumbnailGroup.openGroup("BF");
                const auto bfDataCount = bfThumbnailGroup.getNumObjs();

                WriteAttributeInt64(bfThumbnailGroup, "DataCount", bfDataCount);
            }
            
        } catch (const H5::Exception& exception) {
            QLOG_ERROR() << "TCFThumbnailCompleter Error";
            QLOG_ERROR() << exception.getCDetailMsg();
            QLOG_ERROR() << "targetTCFFilePath : " << d->targetTCFFilePath.toStdString().c_str();

            return false;
        }

        return true;
    }
}
