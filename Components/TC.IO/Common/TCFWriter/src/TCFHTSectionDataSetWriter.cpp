#include "TCFHTSectionDataSetWriter.h"

#include <QFile>

#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"
#include "TCFWritingSubsetIndexGenerator.h"

namespace TC::TCFWriter {

    class TCFHTSectionDataSetWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString targetFilePath{};
        TCFHTDataSet tcfHTDataSet{};

        int64_t wholeDataSizeX{};
        int64_t wholeDataSizeY{};
        int64_t wholeDataSizeZ{};

        int64_t writingStartIndexX{};
        int64_t writingStartIndexY{};
        int64_t writingStartIndexZ{};

        auto IsNarrowDataStep()->bool;

        auto ConvertData3DToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY, 
            const int64_t& sizeZ) ->std::shared_ptr<uint16_t[]>;
        auto ConvertData3DToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY,
            const int64_t& sizeZ)->std::shared_ptr<uint8_t[]>;
        auto ConvertDataMIPToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY)
            ->std::shared_ptr<uint16_t[]>;
        auto ConvertDataMIPToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY)
            ->std::shared_ptr<uint8_t[]>;
    };

    auto TCFHTSectionDataSetWriter::Impl::IsNarrowDataStep() -> bool {
        const auto minValue = this->tcfHTDataSet.GetMetaInfo().GetDataMinValue();
        const auto maxValue = this->tcfHTDataSet.GetMetaInfo().GetDataMaxValue();

        const auto narrowDataStep = std::round((maxValue - minValue) * 1000) < 256;
        return narrowDataStep;
    }
    auto TCFHTSectionDataSetWriter::Impl::ConvertData3DToUint16(const std::shared_ptr<float[]>& data,
        const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ)
        -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;
        std::shared_ptr<uint16_t[]> typeConvertedData{ new uint16_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint16_t>(std::round(data.get()[index] * 10000));
        }

        const auto hdfOrderedData =
            ConvertMemoryOrder(typeConvertedData, sizeX, sizeY, sizeZ, this->tcfHTDataSet.GetDataMemoryOrder(), MemoryOrder3D::XYZ);

        return hdfOrderedData;
    }

    auto TCFHTSectionDataSetWriter::Impl::ConvertData3DToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY, const int64_t& sizeZ) -> std::shared_ptr<uint8_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;
        const auto minValue = this->tcfHTDataSet.GetMetaInfo().GetDataMinValue();
        std::shared_ptr<uint8_t[]> typeConvertedData{ new uint8_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint8_t>(std::round((data.get()[index]-minValue) * 1000));
        }

        const auto hdfOrderedData =
            ConvertMemoryOrder(typeConvertedData, sizeX, sizeY, sizeZ, this->tcfHTDataSet.GetDataMemoryOrder(), MemoryOrder3D::XYZ);

        return hdfOrderedData;
    }

    auto TCFHTSectionDataSetWriter::Impl::ConvertDataMIPToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY)-> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY;
        std::shared_ptr<uint16_t[]> typeConvertedData{ new uint16_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint16_t>(std::round(data.get()[index] * 10000));
        }

        const auto hdfOrderedData =
            ConvertMemoryOrder(typeConvertedData, sizeX, sizeY, this->tcfHTDataSet.GetMIPDataMemoryOrder(), MemoryOrder2D::XY);

        return hdfOrderedData;
    }

    auto TCFHTSectionDataSetWriter::Impl::ConvertDataMIPToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY) -> std::shared_ptr<uint8_t[]> {
        const auto numberOfElements = sizeX * sizeY;
        const auto minValue = this->tcfHTDataSet.GetMetaInfo().GetDataMIPMinValue();
        std::shared_ptr<uint8_t[]> typeConvertedData{ new uint8_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint8_t>(std::round((data.get()[index] - minValue) * 1000));
        }

        const auto hdfOrderedData =
            ConvertMemoryOrder(typeConvertedData, sizeX, sizeY, this->tcfHTDataSet.GetMIPDataMemoryOrder(), MemoryOrder2D::XY);

        return hdfOrderedData;
    }

    TCFHTSectionDataSetWriter::TCFHTSectionDataSetWriter() : d(new Impl()) {
    }

    TCFHTSectionDataSetWriter::~TCFHTSectionDataSetWriter() = default;

    auto TCFHTSectionDataSetWriter::SetTargetFilePath(const QString& targetFilePath) -> void {
        d->targetFilePath = targetFilePath;
    }

    auto TCFHTSectionDataSetWriter::SetTCFHTDataSet(const TCFHTDataSet& dataSet) -> void {
        d->tcfHTDataSet = dataSet;
    }

    auto TCFHTSectionDataSetWriter::SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY,
        const int64_t& dataSizeZ) -> void {
        d->wholeDataSizeX = dataSizeX;
        d->wholeDataSizeY = dataSizeY;
        d->wholeDataSizeZ = dataSizeZ;
    }

    auto TCFHTSectionDataSetWriter::SetWritingStartIndex(const int64_t& indexX, const int64_t& indexY, const int64_t& indexZ)
        -> void {
        d->writingStartIndexX = indexX;
        d->writingStartIndexY = indexY;
        d->writingStartIndexZ = indexZ;
    }

    auto TCFHTSectionDataSetWriter::Write() -> bool {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        if (!QFile::exists(d->targetFilePath)) {
            return false;
        }

        try {
            if (!H5::H5File::isHdf5(d->targetFilePath.toStdString())) {
                return false;
            }

            H5::H5File file(d->targetFilePath.toStdString(), H5F_ACC_RDWR);
            if (!file.nameExists("/Data/3D")) {
                return false;
            }
            if (!file.nameExists("/Data/2DMIP")) {
                return false;
            }

            //const auto narrowDataStep = d->IsNarrowDataStep(); // this could be retrieved after HT Data precision is studied
            constexpr auto narrowDataStep = false;

            const auto dataType = narrowDataStep ? (H5::PredType::NATIVE_UINT8) : (H5::PredType::NATIVE_UINT16);
            const auto metaInfo = d->tcfHTDataSet.GetMetaInfo();

            { // HT 3D
                const auto data3dGroup = file.openGroup("/Data/3D");
                const auto timeFrameName = TimeFrameIndexName(metaInfo.GetTimeFrameIndex());

                const auto wholeDataSpace = MakeDataSpace(d->wholeDataSizeX, d->wholeDataSizeY, d->wholeDataSizeZ);
                const auto dataSet = data3dGroup.createDataSet(timeFrameName.toStdString(), dataType, wholeDataSpace);

                TCFWritingSubsetIndexGenerator indexGenerator;
                indexGenerator.SetWholeSize(d->wholeDataSizeX, d->wholeDataSizeY, d->wholeDataSizeZ);
                indexGenerator.SetSubsetOffset(d->writingStartIndexX, d->writingStartIndexY, d->writingStartIndexZ);
                indexGenerator.SetSubsetDataLimitationSize(5, DataSizeUnit::Gigabyte);
                indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

                indexGenerator.Generate();

                const auto subsetDataCount = indexGenerator.GetSubsetDataCount();
                const WholeSize wholeSize{ d->wholeDataSizeX, d->wholeDataSizeY, d->wholeDataSizeZ };

                for (auto subsetDataIndex = 0; subsetDataIndex < subsetDataCount; ++subsetDataIndex) {
                    const auto writingDataIndexX0 = indexGenerator.GetWritingDataIndexStartX(subsetDataIndex);
                    const auto writingDataIndexX1 = indexGenerator.GetWritingDataIndexEndX(subsetDataIndex);
                    const auto writingDataIndexY0 = indexGenerator.GetWritingDataIndexStartY(subsetDataIndex);
                    const auto writingDataIndexY1 = indexGenerator.GetWritingDataIndexEndY(subsetDataIndex);
                    const auto writingDataIndexZ0 = indexGenerator.GetWritingDataIndexStartZ(subsetDataIndex);
                    const auto writingDataIndexZ1 = indexGenerator.GetWritingDataIndexEndZ(subsetDataIndex);

                    const auto subsetIndexX0 = indexGenerator.GetSubsetIndexStartX(subsetDataIndex);
                    const auto subsetIndexX1 = indexGenerator.GetSubsetIndexEndX(subsetDataIndex);
                    const auto subsetIndexY0 = indexGenerator.GetSubsetIndexStartY(subsetDataIndex);
                    const auto subsetIndexY1 = indexGenerator.GetSubsetIndexEndY(subsetDataIndex);
                    const auto subsetIndexZ0 = indexGenerator.GetSubsetIndexStartZ(subsetDataIndex);
                    const auto subsetIndexZ1 = indexGenerator.GetSubsetIndexEndZ(subsetDataIndex);

                    const auto subsetData = d->tcfHTDataSet.GetData(writingDataIndexX0, writingDataIndexX1,
                        writingDataIndexY0, writingDataIndexY1, writingDataIndexZ0, writingDataIndexZ1);

                    const auto subsetDataSizeX = writingDataIndexX1 - writingDataIndexX0 + 1;
                    const auto subsetDataSizeY = writingDataIndexY1 - writingDataIndexY0 + 1;
                    const auto subsetDataSizeZ = writingDataIndexZ1 - writingDataIndexZ0 + 1;

                    const SubsetIndex subsetIndex{ subsetIndexX0, subsetIndexX1,
                        subsetIndexY0, subsetIndexY1,
                        subsetIndexZ0, subsetIndexZ1 };

                    const auto subsetDataSpace = MakeDataSpace(subsetDataSizeX, subsetDataSizeY, subsetDataSizeZ);
                    const auto selectedDataSpace = MakeSelectedDataSpace(wholeSize, subsetIndex);

                    if (narrowDataStep) {
                        const auto convertedData = d->ConvertData3DToUint8(subsetData, subsetDataSizeX, subsetDataSizeY, subsetDataSizeZ);
                        dataSet.write(convertedData.get(), dataSet.getDataType(), subsetDataSpace, selectedDataSpace);
                    } else {
                        const auto convertedData = d->ConvertData3DToUint16(subsetData, subsetDataSizeX, subsetDataSizeY, subsetDataSizeZ);
                        dataSet.write(convertedData.get(), dataSet.getDataType(), subsetDataSpace, selectedDataSpace);
                    }
                }

                const auto position = metaInfo.GetPosition();
                const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
                const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
                const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
                const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
                const auto minValue = static_cast<double>(metaInfo.GetDataMinValue());
                const auto maxValue = static_cast<double>(metaInfo.GetDataMaxValue());
                const auto recordedTime = metaInfo.GetRecordedTime();
                const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
                const auto time = metaInfo.GetElapsedTime(TimeUnit::Second);
                const auto scalarType = narrowDataStep ? (1) : (0);

                WriteAttributeDouble(dataSet, "PositionX", positionX);
                WriteAttributeDouble(dataSet, "PositionY", positionY);
                WriteAttributeDouble(dataSet, "PositionZ", positionZ);
                WriteAttributeDouble(dataSet, "PositionC", positionC);
                WriteAttributeDouble(dataSet, "RIMin", minValue);
                WriteAttributeDouble(dataSet, "RIMax", maxValue);
                WriteAttributeString(dataSet, "RecordingTime", recordedTimeString);
                WriteAttributeDouble(dataSet, "Time", time);
                WriteAttributeInt64(dataSet, "ScalarType", scalarType);
            }

            { // HT 2DMIP
                const auto data2dMIPGroup = file.openGroup("/Data/2DMIP");
                const auto timeFrameName = TimeFrameIndexName(metaInfo.GetTimeFrameIndex());

                const auto wholeDataSpace = MakeDataSpace(d->wholeDataSizeX, d->wholeDataSizeY);
                const auto dataSet = data2dMIPGroup.createDataSet(timeFrameName.toStdString(), dataType, wholeDataSpace);

                TCFWritingSubsetIndexGenerator indexGenerator;
                indexGenerator.SetWholeSize(d->wholeDataSizeX, d->wholeDataSizeY, d->wholeDataSizeZ);
                indexGenerator.SetSubsetOffset(d->writingStartIndexX, d->writingStartIndexY, d->writingStartIndexZ);
                indexGenerator.SetSubsetDataLimitationSize(5, DataSizeUnit::Gigabyte);
                indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

                indexGenerator.Generate();

                const auto subsetDataCount = indexGenerator.GetSubsetDataCount();
                const WholeSize wholeSize{ d->wholeDataSizeX, d->wholeDataSizeY };

                for (auto subsetDataIndex = 0; subsetDataIndex < subsetDataCount; ++subsetDataIndex) {
                    const auto writingDataIndexX0 = indexGenerator.GetWritingDataIndexStartX(subsetDataIndex);
                    const auto writingDataIndexX1 = indexGenerator.GetWritingDataIndexEndX(subsetDataIndex);
                    const auto writingDataIndexY0 = indexGenerator.GetWritingDataIndexStartY(subsetDataIndex);
                    const auto writingDataIndexY1 = indexGenerator.GetWritingDataIndexEndY(subsetDataIndex);

                    const auto subsetIndexX0 = indexGenerator.GetSubsetIndexStartX(subsetDataIndex);
                    const auto subsetIndexX1 = indexGenerator.GetSubsetIndexEndX(subsetDataIndex);
                    const auto subsetIndexY0 = indexGenerator.GetSubsetIndexStartY(subsetDataIndex);
                    const auto subsetIndexY1 = indexGenerator.GetSubsetIndexEndY(subsetDataIndex);

                    const auto subsetData = d->tcfHTDataSet.GetMIPData(writingDataIndexX0, writingDataIndexX1,
                        writingDataIndexY0, writingDataIndexY1);

                    const auto subsetDataSizeX = writingDataIndexX1 - writingDataIndexX0 + 1;
                    const auto subsetDataSizeY = writingDataIndexY1 - writingDataIndexY0 + 1;

                    const SubsetIndex subsetIndex{ subsetIndexX0, subsetIndexX1,
                        subsetIndexY0, subsetIndexY1 };

                    const auto subsetDataSpace = MakeDataSpace(subsetDataSizeX, subsetDataSizeY);
                    const auto selectedDataSpace = MakeSelectedDataSpace(wholeSize, subsetIndex);
                    if (narrowDataStep) {
                        const auto convertedData = d->ConvertDataMIPToUint8(subsetData, subsetDataSizeX, subsetDataSizeY);
                        dataSet.write(convertedData.get(), dataSet.getDataType(), subsetDataSpace, selectedDataSpace);
                    } else {
                        const auto convertedData = d->ConvertDataMIPToUint16(subsetData, subsetDataSizeX, subsetDataSizeY);
                        dataSet.write(convertedData.get(), dataSet.getDataType(), subsetDataSpace, selectedDataSpace);
                    }
                }

                const auto position = metaInfo.GetPosition();
                const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
                const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
                const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
                const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
                const auto minValue = static_cast<double>(metaInfo.GetDataMIPMinValue());
                const auto maxValue = static_cast<double>(metaInfo.GetDataMIPMaxValue());
                const auto recordedTime = metaInfo.GetRecordedTime();
                const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
                const auto time = metaInfo.GetElapsedTime(TimeUnit::Second);
                const auto scalarType = narrowDataStep ? (1) : (0);

                WriteAttributeDouble(dataSet, "PositionX", positionX);
                WriteAttributeDouble(dataSet, "PositionY", positionY);
                WriteAttributeDouble(dataSet, "PositionZ", positionZ);
                WriteAttributeDouble(dataSet, "PositionC", positionC);
                WriteAttributeDouble(dataSet, "RIMin", minValue);
                WriteAttributeDouble(dataSet, "RIMax", maxValue);
                WriteAttributeString(dataSet, "RecordingTime", recordedTimeString);
                WriteAttributeDouble(dataSet, "Time", time);
                WriteAttributeInt64(dataSet, "ScalarType", scalarType);
            }

            file.close();
        } catch (const H5::Exception&) {
            return false;
        }

        return true;
    }
}
