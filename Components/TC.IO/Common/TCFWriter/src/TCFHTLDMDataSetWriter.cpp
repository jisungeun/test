#include "TCFHTLDMDataSetWriter.h"

#include "HDF5Mutex.h"

#include "Hdf5DataSetTileWriter.h"
#include "TCFWriterUtility.h"

using namespace TC::IO;
using namespace TC::IO::LdmWriting;

namespace TC::TCFWriter {
    class TCFHTLDMDataSetWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        TCFHTLDMDataSet dataSet{};

        int64_t dataSizeX{};
        int64_t dataSizeY{};
        int64_t dataSizeZ{};

        H5::Group* ldmGroup3D{};
        H5::Group* ldmGroupMIP{};

        auto IsNarrowDataStep()->bool;

        auto ConvertData3DToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY,
            const int64_t& sizeZ)->std::shared_ptr<uint16_t[]>;
        auto ConvertData3DToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY,
            const int64_t& sizeZ)->std::shared_ptr<uint8_t[]>;
        auto ConvertDataMIPToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY)
            ->std::shared_ptr<uint16_t[]>;
        auto ConvertDataMIPToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX, const int64_t& sizeY)
            ->std::shared_ptr<uint8_t[]>;
    };

    auto TCFHTLDMDataSetWriter::Impl::IsNarrowDataStep() -> bool {
        const auto minValue = this->dataSet.GetMetaInfo().GetDataMinValue();
        const auto maxValue = this->dataSet.GetMetaInfo().GetDataMaxValue();

        const auto narrowDataStep = std::round((maxValue - minValue) * 1000) < 256;
        return narrowDataStep;
    }

    auto TCFHTLDMDataSetWriter::Impl::ConvertData3DToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY, const int64_t& sizeZ) -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;
        std::shared_ptr<uint16_t[]> typeConvertedData{ new uint16_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint16_t>(std::round(data.get()[index] * 10000));
        }

        return typeConvertedData;
    }

    auto TCFHTLDMDataSetWriter::Impl::ConvertData3DToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY, const int64_t& sizeZ) -> std::shared_ptr<uint8_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;
        const auto minValue = this->dataSet.GetMetaInfo().GetDataMinValue();
        std::shared_ptr<uint8_t[]> typeConvertedData{ new uint8_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint8_t>(std::round((data.get()[index] - minValue) * 1000));
        }

        return typeConvertedData;
    }

    auto TCFHTLDMDataSetWriter::Impl::ConvertDataMIPToUint16(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY) -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY;
        std::shared_ptr<uint16_t[]> typeConvertedData{ new uint16_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint16_t>(std::round(data.get()[index] * 10000));
        }

        return typeConvertedData;
    }

    auto TCFHTLDMDataSetWriter::Impl::ConvertDataMIPToUint8(const std::shared_ptr<float[]>& data, const int64_t& sizeX,
        const int64_t& sizeY) -> std::shared_ptr<uint8_t[]> {
        const auto numberOfElements = sizeX * sizeY;
        const auto minValue = this->dataSet.GetMetaInfo().GetDataMIPMinValue();
        std::shared_ptr<uint8_t[]> typeConvertedData{ new uint8_t[numberOfElements]() };
        for (int64_t index = 0; index < numberOfElements; ++index) {
            typeConvertedData.get()[index] = static_cast<uint8_t>(std::round((data.get()[index] - minValue) * 1000));
        }

        return typeConvertedData;
    }

    TCFHTLDMDataSetWriter::TCFHTLDMDataSetWriter() : d{ std::make_unique<Impl>() } {
    }

    TCFHTLDMDataSetWriter::~TCFHTLDMDataSetWriter() = default;

    auto TCFHTLDMDataSetWriter::SetTCFHTLDMDataSet(const TCFHTLDMDataSet& dataSet) -> void {
        d->dataSet = dataSet;
    }

    auto TCFHTLDMDataSetWriter::SetDestLDMGroup(H5::Group* ldmGroup3D, H5::Group* ldmGroupMIP) -> void {
        d->ldmGroup3D = ldmGroup3D;
        d->ldmGroupMIP = ldmGroupMIP;
    }

    auto TCFHTLDMDataSetWriter::SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY,
        const int64_t& dataSizeZ) -> void {
        d->dataSizeX = dataSizeX;
        d->dataSizeY = dataSizeY;
        d->dataSizeZ = dataSizeZ;
    }

    auto TCFHTLDMDataSetWriter::Write() -> bool {
        HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };

        try {
            const auto metaInfo = d->dataSet.GetMetaInfo();

            //const auto narrowDataStep = d->IsNarrowDataStep(); // this could be retrieved after HT Data precision is studied
            constexpr auto narrowDataStep = false;

            { // HT 3D
                const auto ldmConfiguration3D = d->dataSet.GetLdmConfiguration3D();
                const auto numberOfTiles3D = static_cast<int32_t>(ldmConfiguration3D.GetNumberOfTiles());

                const auto tileUnitDimension = ldmConfiguration3D.GetTileUnitDimension();
                const auto tileSizeX = tileUnitDimension.X();
                const auto tileSizeY = tileUnitDimension.Y();
                const auto tileSizeZ = tileUnitDimension.Z();

                Hdf5DataSetTileWriter hdf5DataSetTileWriter3D(*d->ldmGroup3D);
                hdf5DataSetTileWriter3D.SetChunkSize(tileSizeX, tileSizeY, 1);
                hdf5DataSetTileWriter3D.SetCompressionFlag(true);
                for (auto tileIndex = 0; tileIndex < numberOfTiles3D; ++tileIndex) {
                    const auto tileIndex3D = (numberOfTiles3D - 1) - tileIndex;

                    const auto tileData = d->dataSet.GetTileData(tileIndex3D);

                    MemoryChunk::Pointer tileDataChunk{ new MemoryChunk };

                    if (narrowDataStep) {
                        const auto convertedTileData = d->ConvertData3DToUint8(tileData, tileSizeX, tileSizeY, tileSizeZ);
                        tileDataChunk->SetData(convertedTileData, tileUnitDimension);
                    } else {
                        const auto convertedTileData = d->ConvertData3DToUint16(tileData, tileSizeX, tileSizeY, tileSizeZ);
                        tileDataChunk->SetData(convertedTileData, tileUnitDimension);
                    }

                    hdf5DataSetTileWriter3D.WriteTile(ldmConfiguration3D, tileIndex3D, tileDataChunk);
                }

                const auto position = metaInfo.GetPosition();
                const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
                const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
                const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
                const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
                const auto minValue = static_cast<double>(metaInfo.GetDataMinValue());
                const auto maxValue = static_cast<double>(metaInfo.GetDataMaxValue());
                const auto recordedTime = metaInfo.GetRecordedTime();
                const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
                const auto time = metaInfo.GetElapsedTime(TimeUnit::Second);
                const auto scalarType = narrowDataStep ? (1) : (0);

                WriteAttributeDouble(*d->ldmGroup3D, "PositionX", positionX);
                WriteAttributeDouble(*d->ldmGroup3D, "PositionY", positionY);
                WriteAttributeDouble(*d->ldmGroup3D, "PositionZ", positionZ);
                WriteAttributeDouble(*d->ldmGroup3D, "PositionC", positionC);
                WriteAttributeDouble(*d->ldmGroup3D, "RIMin", minValue);
                WriteAttributeDouble(*d->ldmGroup3D, "RIMax", maxValue);
                WriteAttributeString(*d->ldmGroup3D, "RecordingTime", recordedTimeString);
                WriteAttributeDouble(*d->ldmGroup3D, "Time", time);
                WriteAttributeInt64(*d->ldmGroup3D, "ScalarType", scalarType);

                WriteAttributeInt64(*d->ldmGroup3D, "NumberOfTiles", numberOfTiles3D);
                WriteAttributeInt64(*d->ldmGroup3D, "TileSizeX", static_cast<int64_t>(tileSizeX));
                WriteAttributeInt64(*d->ldmGroup3D, "TileSizeY", static_cast<int64_t>(tileSizeY));
                WriteAttributeInt64(*d->ldmGroup3D, "TileSizeZ", static_cast<int64_t>(tileSizeZ));

                WriteAttributeInt64(*d->ldmGroup3D, "DataSizeX", d->dataSizeX);
                WriteAttributeInt64(*d->ldmGroup3D, "DataSizeY", d->dataSizeY);
                WriteAttributeInt64(*d->ldmGroup3D, "DataSizeZ", d->dataSizeZ);
            }


            { // HT 2DMIP
                const auto ldmConfigurationMIP = d->dataSet.GetLdmConfigurationMIP();
                const auto numberOfTilesMIP = static_cast<int32_t>(ldmConfigurationMIP.GetNumberOfTiles());

                const auto tileUnitDimension = ldmConfigurationMIP.GetTileUnitDimension();
                const auto tileSizeX = tileUnitDimension.X();
                const auto tileSizeY = tileUnitDimension.Y();

                Hdf5DataSetTileWriter hdf5DataSetTileWriterMIP(*d->ldmGroupMIP);
                hdf5DataSetTileWriterMIP.SetCompressionFlag(true);
                for (auto tileIndex = 0; tileIndex < numberOfTilesMIP; ++tileIndex) {
                    const auto tileIndexMIP = (numberOfTilesMIP - 1) - tileIndex;

                    const auto tileData = d->dataSet.GetTileDataMIP(tileIndexMIP);

                    MemoryChunk::Pointer tileDataChunk{ new MemoryChunk };

                    if (narrowDataStep) {
                        const auto convertedTileData = d->ConvertDataMIPToUint8(tileData, tileSizeX, tileSizeY);
                        tileDataChunk->SetData(convertedTileData, tileUnitDimension);
                    } else {
                        const auto convertedTileData = d->ConvertDataMIPToUint16(tileData, tileSizeX, tileSizeY);
                        tileDataChunk->SetData(convertedTileData, tileUnitDimension);
                    }

                    hdf5DataSetTileWriterMIP.WriteTile(ldmConfigurationMIP, tileIndexMIP, tileDataChunk);
                }

                const auto position = metaInfo.GetPosition();
                const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
                const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
                const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
                const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
                const auto minValue = static_cast<double>(metaInfo.GetDataMIPMinValue());
                const auto maxValue = static_cast<double>(metaInfo.GetDataMIPMaxValue());
                const auto recordedTime = metaInfo.GetRecordedTime();
                const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
                const auto time = metaInfo.GetElapsedTime(TimeUnit::Second);
                const auto scalarType = narrowDataStep ? (1) : (0);

                WriteAttributeDouble(*d->ldmGroupMIP, "PositionX", positionX);
                WriteAttributeDouble(*d->ldmGroupMIP, "PositionY", positionY);
                WriteAttributeDouble(*d->ldmGroupMIP, "PositionZ", positionZ);
                WriteAttributeDouble(*d->ldmGroupMIP, "PositionC", positionC);
                WriteAttributeDouble(*d->ldmGroupMIP, "RIMin", minValue);
                WriteAttributeDouble(*d->ldmGroupMIP, "RIMax", maxValue);
                WriteAttributeString(*d->ldmGroupMIP, "RecordingTime", recordedTimeString);
                WriteAttributeDouble(*d->ldmGroupMIP, "Time", time);
                WriteAttributeInt64(*d->ldmGroupMIP, "ScalarType", scalarType);

                WriteAttributeInt64(*d->ldmGroupMIP, "NumberOfTiles", numberOfTilesMIP);
                WriteAttributeInt64(*d->ldmGroupMIP, "TileSizeX", static_cast<int64_t>(tileSizeX));
                WriteAttributeInt64(*d->ldmGroupMIP, "TileSizeY", static_cast<int64_t>(tileSizeY));

                WriteAttributeInt64(*d->ldmGroupMIP, "DataSizeX", d->dataSizeX);
                WriteAttributeInt64(*d->ldmGroupMIP, "DataSizeY", d->dataSizeY);
            }
        } catch (const H5::Exception&) {
            return false;
        }

        return true;
    }
}
