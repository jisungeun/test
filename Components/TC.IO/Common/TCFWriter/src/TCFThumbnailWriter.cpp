#include "TCFThumbnailWriter.h"

#include <QFile>
#include "H5Cpp.h"

#include "HDF5Mutex.h"
#include "TCFThumbnailDataSet.h"
#include "TCFWriterUtility.h"

namespace TC::IO::TCFWriter {
    class TCFThumbnailWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString targetFilePath{};
        TCFThumbnailDataSet tcfThumbnailDataSet{};
        Modality modality{Modality::HT};

        auto ModalityToThumbnailPath(const Modality& modality)->QString;
        auto ToThumbanilDataSpace(const TCFThumbnailDataSet& tcfThumbnailDataSet)->H5::DataSpace;
        auto GetConvertedData(const TCFThumbnailDataSet& tcfThumbnailDataSet)->std::shared_ptr<uint8_t[]>;
    };

    auto TCFThumbnailWriter::Impl::ModalityToThumbnailPath(const Modality& modality) -> QString {
        if (modality == Modality::HT) {
            return "/Thumbnail/HT";
        }
        if (modality == Modality::FL) {
            return "/Thumbnail/FL";
        }
        if (modality == Modality::BF) {
            return "/Thumbnail/BF";
        }
        return "";
    }

    auto TCFThumbnailWriter::Impl::ToThumbanilDataSpace(const TCFThumbnailDataSet& tcfThumbnailDataSet)
        -> H5::DataSpace {
        const auto sizeX = tcfThumbnailDataSet.GetSizeX();
        const auto sizeY = tcfThumbnailDataSet.GetSizeY();
        const auto colorFlag = tcfThumbnailDataSet.GetColorFlag();

        if (colorFlag == true) {
            return MakeDataSpace(sizeX, sizeY, 3);
        } else {
            return MakeDataSpace(sizeX, sizeY);
        }
    }

    auto TCFThumbnailWriter::Impl::GetConvertedData(const TCFThumbnailDataSet& tcfThumbnailDataSet)
        -> std::shared_ptr<uint8_t[]> {
        if (tcfThumbnailDataSet.GetColorFlag()) {
            return ConvertMemoryOrder(tcfThumbnailDataSet.GetData(), tcfThumbnailDataSet.GetSizeX(),
                tcfThumbnailDataSet.GetSizeY(), 3, tcfThumbnailDataSet.GetDataMemoryOrder(), MemoryOrder3D::XYZ);
        } else {
            return ConvertMemoryOrder(tcfThumbnailDataSet.GetData(), tcfThumbnailDataSet.GetSizeX(),
                tcfThumbnailDataSet.GetSizeY(), 1, tcfThumbnailDataSet.GetDataMemoryOrder(), MemoryOrder3D::XYZ);
        }

    }

    TCFThumbnailWriter::TCFThumbnailWriter() : d(new Impl()) {
    }

    TCFThumbnailWriter::~TCFThumbnailWriter() = default;

    auto TCFThumbnailWriter::SetTargetFilePath(const QString& targetFilePath) -> void {
        d->targetFilePath = targetFilePath;
    }

    auto TCFThumbnailWriter::SetTCFThumbnailDataSet(const TCFThumbnailDataSet& tcfThumbnailDataSet) -> void {
        d->tcfThumbnailDataSet = tcfThumbnailDataSet;
    }

    auto TCFThumbnailWriter::SetModality(const Modality& modality) -> void {
        d->modality = modality;
    }

    auto TCFThumbnailWriter::Write() -> bool {
        HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };
        if (!QFile::exists(d->targetFilePath)) {
            return false;
        }

        try {
            if (!H5::H5File::isHdf5(d->targetFilePath.toStdString())) {
                return false;
            }

            H5::H5File file(d->targetFilePath.toStdString(), H5F_ACC_RDWR);

            const auto thumbnailPath = d->ModalityToThumbnailPath(d->modality);
            if (thumbnailPath.isEmpty()) {
                return false;
            }

            if (!file.nameExists(thumbnailPath.toStdString())) {
                return false;
            }

            const auto thumbnailGroup = file.openGroup(thumbnailPath.toStdString());
            const auto dataSetName = TimeFrameIndexName(d->tcfThumbnailDataSet.GetTimeFrameIndex());

            if (thumbnailGroup.exists(dataSetName.toStdString())) {
                return false;
            }

            const auto thumbnailDataSpace = d->ToThumbanilDataSpace(d->tcfThumbnailDataSet);
            const auto thumbnailDataSet = thumbnailGroup.createDataSet(dataSetName.toStdString(), 
                H5::PredType::NATIVE_UINT8, thumbnailDataSpace);

            const auto data = d->GetConvertedData(d->tcfThumbnailDataSet);

            thumbnailDataSet.write(data.get(), H5::PredType::NATIVE_UINT8);

            const auto resolutionX = static_cast<double>(d->tcfThumbnailDataSet.GetVoxelSizeX(LengthUnit::Micrometer));
            const auto resolutionY = static_cast<double>(d->tcfThumbnailDataSet.GetVoxelSizeY(LengthUnit::Micrometer));
            const auto isColor = static_cast<int64_t>(d->tcfThumbnailDataSet.GetColorFlag());
            const auto sizeX = d->tcfThumbnailDataSet.GetSizeX();
            const auto sizeY = d->tcfThumbnailDataSet.GetSizeY();

            WriteAttributeDouble(thumbnailDataSet, "ResolutionX", resolutionX);
            WriteAttributeDouble(thumbnailDataSet, "ResolutionY", resolutionY);
            WriteAttributeInt64(thumbnailDataSet, "IsColor", isColor);
            WriteAttributeInt64(thumbnailDataSet, "SizeX", sizeX);
            WriteAttributeInt64(thumbnailDataSet, "SizeY", sizeY);
        } catch (const H5::Exception&) {
            return false;
        }

        return true;
    }
}
