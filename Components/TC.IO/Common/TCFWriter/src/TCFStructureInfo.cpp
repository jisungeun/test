#include "TCFStructureInfo.h"

class TCFStructureInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    StructureType structureType{ StructureType::Default };
};

TCFStructureInfo::TCFStructureInfo() : d{ std::make_unique<Impl>() } {
}

TCFStructureInfo::TCFStructureInfo(const TCFStructureInfo& other) : d{ std::make_unique<Impl>(*other.d) } {
}

TCFStructureInfo::~TCFStructureInfo() = default;

auto TCFStructureInfo::operator=(const TCFStructureInfo& other) -> TCFStructureInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFStructureInfo::SetStructureType(const StructureType& structureType) -> void {
    d->structureType = structureType;
}

auto TCFStructureInfo::GetStructureType() const -> StructureType {
    return d->structureType;
}
