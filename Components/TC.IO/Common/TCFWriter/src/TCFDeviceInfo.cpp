#include "TCFDeviceInfo.h"

class TCFDeviceInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    double zp1{};
    double zp2{};
    double zp3{};

    double iteration{};
    double magnification{};
    double na{};
    double rawSize{};
    double ri{};
    double waveLength{};
};

TCFDeviceInfo::TCFDeviceInfo() : d(new Impl()) {
}

TCFDeviceInfo::TCFDeviceInfo(const TCFDeviceInfo& other) : d(new Impl(*other.d)) {
}

TCFDeviceInfo::~TCFDeviceInfo() = default;

auto TCFDeviceInfo::operator=(const TCFDeviceInfo & other) -> TCFDeviceInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFDeviceInfo::SetMagnification(const double& magnification) -> void {
    d->magnification = magnification;
}

auto TCFDeviceInfo::GetMagnification() const -> const double& {
    return d->magnification;
}

auto TCFDeviceInfo::SetNA(const double& na) -> void {
    d->na = na;
}

auto TCFDeviceInfo::GetNA() const -> const double& {
    return d->na;
}

auto TCFDeviceInfo::SetRI(const double& ri) -> void {
    d->ri = ri;
}

auto TCFDeviceInfo::GetRI() const -> const double& {
    return d->ri;
}
