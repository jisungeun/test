#include "TCFWritingSubsetIndexGenerator.h"

#include <QMap>

struct DataRange {
    int64_t x0{}; int64_t x1{};
    int64_t y0{}; int64_t y1{};
    int64_t z0{}; int64_t z1{};
};

struct SubsetDataSize {
    int64_t sizeX{};
    int64_t sizeY{};
    int64_t sizeZ{};
};

class TCFWritingSubsetIndexGenerator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    int64_t wholeSizeX{};
    int64_t wholeSizeY{};
    int64_t wholeSizeZ{};

    int64_t subsetOffsetX{};
    int64_t subsetOffsetY{};
    int64_t subsetOffsetZ{};

    double subsetDataLimitationSize{};
    DataSizeUnit dataSizeUnit{ DataSizeUnit::Byte };

    double elementSize{};
    DataSizeUnit elementSizeUnit{ DataSizeUnit::Byte };

    QMap<int32_t, DataRange> subsetRangeMap{};
    QMap<int32_t, DataRange> writingRangeMap{};

    auto CalculateSubsetDataSize()->SubsetDataSize;
};

auto TCFWritingSubsetIndexGenerator::Impl::CalculateSubsetDataSize() -> SubsetDataSize {
    const auto subsetDataLimitSizeInByte = ConvertUnit(this->subsetDataLimitationSize, this->dataSizeUnit, DataSizeUnit::Byte);
    const auto elementSizeInByte = ConvertUnit(this->elementSize, this->elementSizeUnit, DataSizeUnit::Byte);

    const auto zSize = static_cast<double>(this->wholeSizeZ);

    const auto squareSizeInBounded = static_cast<int64_t>(std::floor(std::sqrt(subsetDataLimitSizeInByte / elementSizeInByte / zSize)));

    const auto xSize = squareSizeInBounded;
    const auto ySize = squareSizeInBounded;

    return { xSize, ySize, static_cast<int64_t>(zSize) };
}

TCFWritingSubsetIndexGenerator::TCFWritingSubsetIndexGenerator() : d(new Impl()) {
}

TCFWritingSubsetIndexGenerator::~TCFWritingSubsetIndexGenerator() = default;

auto TCFWritingSubsetIndexGenerator::SetWholeSize(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ)
    -> void {
    d->wholeSizeX = sizeX;
    d->wholeSizeY = sizeY;
    d->wholeSizeZ = sizeZ;
}

auto TCFWritingSubsetIndexGenerator::SetSubsetOffset(const int64_t& offsetX, const int64_t& offsetY,
    const int64_t& offsetZ) -> void {
    d->subsetOffsetX = offsetX;
    d->subsetOffsetY = offsetY;
    d->subsetOffsetZ = offsetZ;
}

auto TCFWritingSubsetIndexGenerator::SetSubsetDataLimitationSize(const double& size, const DataSizeUnit& dataSizeUnit)
    -> void {
    d->subsetDataLimitationSize = size;
    d->dataSizeUnit = dataSizeUnit;
}

auto TCFWritingSubsetIndexGenerator::SetElementSize(const double& size, const DataSizeUnit& dataSizeUnit) -> void {
    d->elementSize = size;
    d->elementSizeUnit = dataSizeUnit;
}

auto TCFWritingSubsetIndexGenerator::Generate() -> bool {
    d->writingRangeMap.clear();
    d->subsetRangeMap.clear();

    const auto [subsetDataSizeX, subsetDataSizeY, subsetDataSizeZ] = d->CalculateSubsetDataSize();

    const auto subsetCountX = static_cast<int64_t>(std::ceil(static_cast<double>(d->wholeSizeX) / static_cast<double>(subsetDataSizeX)));
    const auto subsetCountY = static_cast<int64_t>(std::ceil(static_cast<double>(d->wholeSizeY) / static_cast<double>(subsetDataSizeY)));
    const auto subsetCountZ = static_cast<int64_t>(std::ceil(static_cast<double>(d->wholeSizeZ) / static_cast<double>(subsetDataSizeZ)));

    for (auto subsetIndexZ = 0; subsetIndexZ < subsetCountZ; ++subsetIndexZ) {
        for (auto subsetIndexY = 0; subsetIndexY < subsetCountY; ++subsetIndexY) {
            for (auto subsetIndexX = 0; subsetIndexX < subsetCountX; ++subsetIndexX) {
                const auto subsetIndex = subsetIndexX + subsetIndexY * subsetCountX + subsetIndexZ * subsetCountX * subsetCountY;

                const auto subsetX0 = subsetIndexX * subsetDataSizeX;
                const auto subsetX1 = std::min(subsetX0 + subsetDataSizeX - 1, d->wholeSizeX - 1);
                const auto subsetY0 = subsetIndexY * subsetDataSizeY;
                const auto subsetY1 = std::min(subsetY0 + subsetDataSizeY - 1, d->wholeSizeY - 1);
                const auto subsetZ0 = subsetIndexZ * subsetDataSizeZ;
                const auto subsetZ1 = std::min(subsetZ0 + subsetDataSizeZ - 1, d->wholeSizeZ - 1);

                const auto writingX0 = subsetX0 + d->subsetOffsetX;
                const auto writingX1 = subsetX1 + d->subsetOffsetX;
                const auto writingY0 = subsetY0 + d->subsetOffsetY;
                const auto writingY1 = subsetY1 + d->subsetOffsetY;
                const auto writingZ0 = subsetZ0 + d->subsetOffsetZ;
                const auto writingZ1 = subsetZ1 + d->subsetOffsetZ;

                d->writingRangeMap[subsetIndex] = { writingX0, writingX1, writingY0, writingY1, writingZ0, writingZ1 };
                d->subsetRangeMap[subsetIndex] = { subsetX0, subsetX1, subsetY0, subsetY1, subsetZ0, subsetZ1 };
            }
        }
    }
    
    return true;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetDataCount() const -> int32_t {
    return d->subsetRangeMap.count();
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexStartX(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].x0;
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexEndX(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].x1;
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexStartY(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].y0;
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexEndY(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].y1;
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexStartZ(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].z0;
}

auto TCFWritingSubsetIndexGenerator::GetWritingDataIndexEndZ(const int32_t& subsetDataIndex) const -> int64_t {
    return d->writingRangeMap[subsetDataIndex].z1;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexStartX(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].x0;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexEndX(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].x1;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexStartY(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].y0;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexEndY(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].y1;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexStartZ(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].z0;
}

auto TCFWritingSubsetIndexGenerator::GetSubsetIndexEndZ(const int32_t& subsetDataIndex) const -> int64_t {
    return d->subsetRangeMap[subsetDataIndex].z1;
}
