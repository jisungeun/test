#pragma once

#include <cstdint>
#include <QDateTime>
#include <QString>

#include "TCFWriterDefines.h"

#include "H5Cpp.h"

struct WholeSize {
    int64_t sizeX{};
    int64_t sizeY{};
    int64_t sizeZ{};
};

struct SubsetIndex {
    int64_t startX{};
    int64_t endX{};
    int64_t startY{};
    int64_t endY{};
    int64_t startZ{};
    int64_t endZ{};
};

auto CreateCompPropList2D(const int32_t& dataSizeX, const int32_t& dataSizeY)->H5::DSetCreatPropList;
auto CreateCompPropListBF(const int32_t& dataSizeX, const int32_t& dataSizeY)->H5::DSetCreatPropList;
auto CreateCompPropList3D(const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ)->H5::DSetCreatPropList;

auto TimeFrameIndexName(const int32_t& timeFrameIndex)->QString;
auto MakeDataSpace(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ)->H5::DataSpace;
auto MakeDataSpace(const int64_t& sizeX, const int64_t& sizeY)->H5::DataSpace;
auto MakeDataSpace(const int64_t& length)->H5::DataSpace;

auto MakeSelectedDataSpace(const WholeSize& wholeSize, const SubsetIndex& selectedIndex)->H5::DataSpace;

auto WriteAttributeString(const H5::H5Object& target, const QString& name, const QString& contents,
    const int32_t& fixedSpace = 0) -> void;
auto WriteAttributeDouble(const H5::H5Object& target, const QString& name, const double& contents)
-> void;
auto WriteAttributeInt64(const H5::H5Object& target, const QString& name, const int64_t& contents)
-> void;
auto WriteAttributeUint8(const H5::H5Object& target, const QString& name, const uint8_t& contents)
-> void;

auto ReadAttributeInt64(const H5::H5Object& object, const std::string& attributeName)->int64_t;
auto ReadAttributeDouble(const H5::H5Object& object, const std::string& attributeName)->double;
auto ReadAttributeUint8(const H5::H5Object& object, const std::string& attributeName)->uint8_t;
auto ReadAttributeString(const H5::H5Object& object, const std::string& attributeName)->std::string;


auto ToTimeString(const QDateTime& dateTime)->QString;

auto ConvertMemoryOrder(const std::shared_ptr<uint16_t[]>& data, const int64_t& sizeX, const int64_t& sizeY, 
    const int64_t& sizeZ, const MemoryOrder3D& from, const MemoryOrder3D& to)->std::shared_ptr<uint16_t[]>;
auto ConvertMemoryOrder(const std::shared_ptr<uint16_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const MemoryOrder2D& from, const MemoryOrder2D& to)->std::shared_ptr<uint16_t[]>;

auto ConvertMemoryOrder(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const int64_t& sizeZ, const MemoryOrder3D& from, const MemoryOrder3D& to)->std::shared_ptr<uint8_t[]>;
auto ConvertMemoryOrder(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const MemoryOrder2D& from, const MemoryOrder2D& to)->std::shared_ptr<uint8_t[]>;


auto GenerateUniqueID(const QString& deviceName, const QDateTime& dataRecordedDateTime, 
    const QDateTime& dataWritingDateTime)->QString;
auto GenerateDataID(const QString& deviceName, const QDateTime& dataRecordedDateTime)->QString;