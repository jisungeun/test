#include "TCFBFSectionDataSetWriter.h"

#include <QFile>

#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"
#include "TCFWritingSubsetIndexGenerator.h"

using namespace TC::TCFWriter;

class TCFBFSectionDataSetWriter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetFilePath{};
    TCFBFDataSet tcfBFDataSet{};

    int64_t wholeDataSizeX{};
    int64_t wholeDataSizeY{};

    int64_t writingStartIndexX{};
    int64_t writingStartIndexY{};

    auto ConvertTCFBFArray(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
        const int64_t& sizeZ, const int32_t& targetChannelCount)->std::shared_ptr<uint8_t[]>;
};

auto TCFBFSectionDataSetWriter::Impl::ConvertTCFBFArray(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX,
    const int64_t& sizeY, const int64_t& sizeZ, const int32_t& targetChannelCount) -> std::shared_ptr<uint8_t[]> {
    std::shared_ptr<uint8_t[]> bfArrayData{ nullptr };

    if (sizeZ == targetChannelCount) {
        bfArrayData =
            ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, this->tcfBFDataSet.GetDataMemoryOrder(), MemoryOrder3D::XYZ);
    } else {
        if (targetChannelCount == 3 && sizeZ == 1) {
            const auto numberOfSliceElements = sizeX * sizeY;
            const auto targetNumberOfElements = numberOfSliceElements * targetChannelCount;

            std::shared_ptr<uint8_t[]> bfData{ new uint8_t[targetNumberOfElements]() };
            for (int64_t channelIndex = 0; channelIndex < targetChannelCount; ++channelIndex) {
                const int64_t offset = channelIndex * sizeX * sizeY;
                std::copy_n(data.get(), numberOfSliceElements, bfData.get() + offset);
            }

            bfArrayData =
                ConvertMemoryOrder(bfData, sizeX, sizeY, targetChannelCount, this->tcfBFDataSet.GetDataMemoryOrder(), 
                    MemoryOrder3D::XYZ);
        }
    }
    
    return bfArrayData;
}

TCFBFSectionDataSetWriter::TCFBFSectionDataSetWriter() : d(new Impl()) {
}

TCFBFSectionDataSetWriter::~TCFBFSectionDataSetWriter() = default;

auto TCFBFSectionDataSetWriter::SetTargetFilePath(const QString & targetFilePath) -> void {
    d->targetFilePath = targetFilePath;
}

auto TCFBFSectionDataSetWriter::SetTCFBFDataSet(const TC::TCFWriter::TCFBFDataSet & dataSet) -> void {
    d->tcfBFDataSet = dataSet;
}

auto TCFBFSectionDataSetWriter::SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY) -> void {
    d->wholeDataSizeX = dataSizeX;
    d->wholeDataSizeY = dataSizeY;
}

auto TCFBFSectionDataSetWriter::SetWritingStartIndex(const int64_t& indexX, const int64_t& indexY) -> void {
    d->writingStartIndexX = indexX;
    d->writingStartIndexY = indexY;
}

auto TCFBFSectionDataSetWriter::Write() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    if (!QFile::exists(d->targetFilePath)) {
        return false;
    }

    try {
        if (!H5::H5File::isHdf5(d->targetFilePath.toStdString())) {
            return false;
        }

        H5::H5File file(d->targetFilePath.toStdString(), H5F_ACC_RDWR);
        if (!file.nameExists("/Data/BF")) {
            return false;
        }
        { // BF 3D
            const auto dataBFGroup = file.openGroup("/Data/BF");
            const auto timeFrameName = TimeFrameIndexName(d->tcfBFDataSet.GetMetaInfo().GetTimeFrameIndex());

            constexpr auto sizeZ = 3;

            const auto channelCount = static_cast<int64_t>(d->tcfBFDataSet.GetChannelCount());

            const auto wholeDataSpace = MakeDataSpace(d->wholeDataSizeX, d->wholeDataSizeY, sizeZ);
            const auto dataSet = dataBFGroup.createDataSet(timeFrameName.toStdString(), H5::PredType::NATIVE_UCHAR, wholeDataSpace);

            TCFWritingSubsetIndexGenerator indexGenerator;
            indexGenerator.SetWholeSize(d->wholeDataSizeX, d->wholeDataSizeY, sizeZ);
            indexGenerator.SetSubsetOffset(d->writingStartIndexX, d->writingStartIndexY, 0);
            indexGenerator.SetSubsetDataLimitationSize(5, DataSizeUnit::Gigabyte);
            indexGenerator.SetElementSize(1, DataSizeUnit::Byte);

            indexGenerator.Generate();

            const auto subsetDataCount = indexGenerator.GetSubsetDataCount();
            const WholeSize wholeSize{ d->wholeDataSizeX, d->wholeDataSizeY, sizeZ };

            for (auto subsetDataIndex = 0; subsetDataIndex < subsetDataCount; ++subsetDataIndex) {
                const auto writingDataIndexX0 = indexGenerator.GetWritingDataIndexStartX(subsetDataIndex);
                const auto writingDataIndexX1 = indexGenerator.GetWritingDataIndexEndX(subsetDataIndex);
                const auto writingDataIndexY0 = indexGenerator.GetWritingDataIndexStartY(subsetDataIndex);
                const auto writingDataIndexY1 = indexGenerator.GetWritingDataIndexEndY(subsetDataIndex);
                const auto writingDataIndexZ0 = indexGenerator.GetWritingDataIndexStartZ(subsetDataIndex);
                const auto writingDataIndexZ1 = indexGenerator.GetWritingDataIndexEndZ(subsetDataIndex);

                const auto subsetIndexX0 = indexGenerator.GetSubsetIndexStartX(subsetDataIndex);
                const auto subsetIndexX1 = indexGenerator.GetSubsetIndexEndX(subsetDataIndex);
                const auto subsetIndexY0 = indexGenerator.GetSubsetIndexStartY(subsetDataIndex);
                const auto subsetIndexY1 = indexGenerator.GetSubsetIndexEndY(subsetDataIndex);
                const auto subsetIndexZ0 = indexGenerator.GetSubsetIndexStartZ(subsetDataIndex);
                const auto subsetIndexZ1 = indexGenerator.GetSubsetIndexEndZ(subsetDataIndex);

                const auto subsetData = d->tcfBFDataSet.GetData(writingDataIndexX0, writingDataIndexX1,
                    writingDataIndexY0, writingDataIndexY1);

                const auto subsetDataSizeX = writingDataIndexX1 - writingDataIndexX0 + 1;
                const auto subsetDataSizeY = writingDataIndexY1 - writingDataIndexY0 + 1;
                const auto subsetDataSizeZ = writingDataIndexZ1 - writingDataIndexZ0 + 1;

                const SubsetIndex subsetIndex{ subsetIndexX0, subsetIndexX1,
                    subsetIndexY0, subsetIndexY1,
                    subsetIndexZ0, subsetIndexZ1 };

                const auto convertedData =
                    d->ConvertTCFBFArray(subsetData, subsetDataSizeX, subsetDataSizeY, channelCount, sizeZ);

                const auto subsetDataSpace = MakeDataSpace(subsetDataSizeX, subsetDataSizeY, subsetDataSizeZ);
                const auto selectedDataSpace = MakeSelectedDataSpace(wholeSize, subsetIndex);
                dataSet.write(convertedData.get(), dataSet.getDataType(), subsetDataSpace, selectedDataSpace);
            }

            const auto position = d->tcfBFDataSet.GetMetaInfo().GetPosition();
            const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
            const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
            const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
            const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
            const auto recordedTime = d->tcfBFDataSet.GetMetaInfo().GetRecordedTime();
            const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
            const auto time = d->tcfBFDataSet.GetMetaInfo().GetElapsedTime(TimeUnit::Second);

            WriteAttributeDouble(dataSet, "PositionX", positionX);
            WriteAttributeDouble(dataSet, "PositionY", positionY);
            WriteAttributeDouble(dataSet, "PositionZ", positionZ);
            WriteAttributeDouble(dataSet, "PositionC", positionC);
            WriteAttributeString(dataSet, "RecordingTime", recordedTimeString);
            WriteAttributeDouble(dataSet, "Time", time);
        }

        file.close();
    } catch (const H5::Exception&) {
        return false;
    }

    return true;
}
