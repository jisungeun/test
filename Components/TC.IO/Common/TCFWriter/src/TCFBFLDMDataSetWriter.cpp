#include "TCFBFLDMDataSetWriter.h"

#include <HDF5Mutex.h>

#include "Hdf5DataSetTileWriter.h"
#include "TCFWriterUtility.h"

using namespace TC::IO;
using namespace TC::IO::LdmWriting;

namespace TC::TCFWriter {
    class TCFBFLDMDataSetWriter::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        TCFBFLDMDataSet dataSet{};

        int64_t dataSizeX{};
        int64_t dataSizeY{};

        H5::Group* ldmGroup{};

        auto ConvertDataTo3ZDimension(const std::shared_ptr<uint8_t[]>& data, const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ)
            ->std::shared_ptr<uint8_t[]>;
    };

    auto TCFBFLDMDataSetWriter::Impl::ConvertDataTo3ZDimension(const std::shared_ptr<uint8_t[]>& data,
        const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ) -> std::shared_ptr<uint8_t[]> {
        if (dataSizeZ == 3) {
            return data;
        }

        constexpr size_t sizeZ = 3;

        const auto convertedNumberOfElements = static_cast<size_t>(dataSizeX) * static_cast<size_t>(dataSizeY) * sizeZ;
        std::shared_ptr<uint8_t[]> convertedData{ new uint8_t[convertedNumberOfElements] };

        for (size_t indexX = 0; indexX < dataSizeX; ++indexX) {
            for (size_t indexY = 0; indexY < dataSizeY; ++indexY) {
                for (size_t indexZ = 0; indexZ < sizeZ; ++indexZ) {
                    const auto srcIndex = indexX + indexY * dataSizeX;
                    const auto destIndex = indexX + indexY * dataSizeX + indexZ * dataSizeX * dataSizeY;

                    convertedData[destIndex] = data[srcIndex];
                }
            }
        }

        return convertedData;
    }

    TCFBFLDMDataSetWriter::TCFBFLDMDataSetWriter() : d{ std::make_unique<Impl>() } {
    }

    TCFBFLDMDataSetWriter::~TCFBFLDMDataSetWriter() = default;

    auto TCFBFLDMDataSetWriter::SetTCFBFLDMDataSet(const TCFBFLDMDataSet& dataSet) -> void {
        d->dataSet = dataSet;
    }

    auto TCFBFLDMDataSetWriter::SetDestLDMGroup(H5::Group* ldmGroup) -> void {
        d->ldmGroup = ldmGroup;
    }

    auto TCFBFLDMDataSetWriter::SetWholeDataSize(const int64_t& dataSizeX, const int64_t& dataSizeY) -> void {
        d->dataSizeX = dataSizeX;
        d->dataSizeY = dataSizeY;
    }

    auto TCFBFLDMDataSetWriter::Write() -> bool {
        HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };

        try {
            constexpr auto bfDataSizeZ = 3;
            const auto metaInfo = d->dataSet.GetMetaInfo();

            const auto ldmConfiguration = d->dataSet.GetLdmConfiguration();
            const auto numberOfTiles = ldmConfiguration.GetNumberOfTiles();

            const auto tileUnitDimension = ldmConfiguration.GetTileUnitDimension();
            const auto tileSizeX = tileUnitDimension.X();
            const auto tileSizeY = tileUnitDimension.Y();
            const auto tileSizeZ = tileUnitDimension.Z();

            Dimension dataDimension{ tileSizeX, tileSizeY, bfDataSizeZ };

            Hdf5DataSetTileWriter hdf5DataSetTileWriter(*d->ldmGroup);
            hdf5DataSetTileWriter.SetChunkSize(tileSizeX, tileSizeY, 1);
            hdf5DataSetTileWriter.SetCompressionFlag(true);
            for (auto index = 0; index < numberOfTiles; ++index) {
                const auto tileIndex = (numberOfTiles - 1) - index;

                const auto tileData = d->dataSet.GetTileData(tileIndex);
                const auto convertedTileData = d->ConvertDataTo3ZDimension(tileData, tileSizeX, tileSizeY, tileSizeZ);

                MemoryChunk::Pointer tileDataChunk{ new MemoryChunk };
                tileDataChunk->SetData(convertedTileData, dataDimension);

                hdf5DataSetTileWriter.WriteTile(ldmConfiguration, tileIndex, tileDataChunk);
            }

            const auto position = d->dataSet.GetMetaInfo().GetPosition();
            const auto positionX = position.GetPositionX(LengthUnit::Millimenter);
            const auto positionY = position.GetPositionY(LengthUnit::Millimenter);
            const auto positionZ = position.GetPositionZ(LengthUnit::Millimenter);
            const auto positionC = position.GetPositionC(LengthUnit::Millimenter);
            const auto recordedTime = d->dataSet.GetMetaInfo().GetRecordedTime();
            const auto recordedTimeString = recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
            const auto time = d->dataSet.GetMetaInfo().GetElapsedTime(TimeUnit::Second);

            WriteAttributeDouble(*d->ldmGroup, "PositionX", positionX);
            WriteAttributeDouble(*d->ldmGroup, "PositionY", positionY);
            WriteAttributeDouble(*d->ldmGroup, "PositionZ", positionZ);
            WriteAttributeDouble(*d->ldmGroup, "PositionC", positionC);
            WriteAttributeString(*d->ldmGroup, "RecordingTime", recordedTimeString);
            WriteAttributeDouble(*d->ldmGroup, "Time", time);

            WriteAttributeInt64(*d->ldmGroup, "NumberOfTiles", numberOfTiles);
            WriteAttributeInt64(*d->ldmGroup, "TileSizeX", static_cast<int64_t>(tileSizeX));
            WriteAttributeInt64(*d->ldmGroup, "TileSizeY", static_cast<int64_t>(tileSizeY));
            WriteAttributeInt64(*d->ldmGroup, "TileSizeZ", static_cast<int64_t>(tileSizeZ));

            WriteAttributeInt64(*d->ldmGroup, "DataSizeX", d->dataSizeX);
            WriteAttributeInt64(*d->ldmGroup, "DataSizeY", d->dataSizeY);
            WriteAttributeInt64(*d->ldmGroup, "DataSizeZ", bfDataSizeZ);

        } catch (const H5::Exception&) {
            return false;
        }

        return true;
    }
}
