#include "TCFFLDataSetMetaInfo.h"

namespace TC::TCFWriter {
    class TCFFLDataSetMetaInfo::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        float dataMinValue{};
        float dataMaxValue{};

        float dataMIPMinValue{};
        float dataMIPMaxValue{};

        int32_t dataSizeX{};
        int32_t dataSizeY{};
        int32_t dataSizeZ{};

        float pixelWorldSizeX{};
        float pixelWorldSizeY{};
        float pixelWorldSizeZ{};
        LengthUnit pixelWorldSizeUnit{ LengthUnit::Micrometer };

        QDateTime recordedTime{};
        double elapsedTime{};
        TimeUnit elapsedTimeUnit{ TimeUnit::Second };

        int32_t timeFrameIndex{};

        TCFDataSetRecordedPosition position{};

        int32_t channelIndex{};
    };

    TCFFLDataSetMetaInfo::TCFFLDataSetMetaInfo() : d{ std::make_unique<Impl>() } {
    }

    TCFFLDataSetMetaInfo::TCFFLDataSetMetaInfo(const TCFFLDataSetMetaInfo& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    TCFFLDataSetMetaInfo::~TCFFLDataSetMetaInfo() = default;

    auto TCFFLDataSetMetaInfo::operator=(const TCFFLDataSetMetaInfo& other) -> TCFFLDataSetMetaInfo& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFFLDataSetMetaInfo::SetDataMinMaxValue(const float& minValue, const float& maxValue) -> void {
        d->dataMinValue = minValue;
        d->dataMaxValue = maxValue;
    }

    auto TCFFLDataSetMetaInfo::GetDataMinValue() const -> const float& {
        return d->dataMinValue;
    }

    auto TCFFLDataSetMetaInfo::GetDataMaxValue() const -> const float& {
        return d->dataMaxValue;
    }

    auto TCFFLDataSetMetaInfo::SetMIPDataMinMaxValue(const float& minValue, const float& maxValue) -> void {
        d->dataMIPMinValue = minValue;
        d->dataMIPMaxValue = maxValue;
    }

    auto TCFFLDataSetMetaInfo::GetDataMIPMinValue() const -> const float& {
        return d->dataMIPMinValue;
    }

    auto TCFFLDataSetMetaInfo::GetDataMIPMaxValue() const -> const float& {
        return d->dataMIPMaxValue;
    }

    auto TCFFLDataSetMetaInfo::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
        d->dataSizeX = sizeX;
        d->dataSizeY = sizeY;
        d->dataSizeZ = sizeZ;
    }

    auto TCFFLDataSetMetaInfo::GetSizeX() const -> const int32_t& {
        return d->dataSizeX;
    }

    auto TCFFLDataSetMetaInfo::GetSizeY() const -> const int32_t& {
        return d->dataSizeY;
    }

    auto TCFFLDataSetMetaInfo::GetSizeZ() const -> const int32_t& {
        return d->dataSizeZ;
    }

    auto TCFFLDataSetMetaInfo::SetPixelWorldSize(const float& sizeX, const float& sizeY, const float& sizeZ,
        const LengthUnit& unit) -> void {
        d->pixelWorldSizeX = sizeX;
        d->pixelWorldSizeY = sizeY;
        d->pixelWorldSizeZ = sizeZ;
        d->pixelWorldSizeUnit = unit;
    }

    auto TCFFLDataSetMetaInfo::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit);
    }

    auto TCFFLDataSetMetaInfo::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit);
    }

    auto TCFFLDataSetMetaInfo::GetPixelWorldSizeZ(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeZ, d->pixelWorldSizeUnit, unit);
    }

    auto TCFFLDataSetMetaInfo::SetRecordedTime(const QDateTime& time) -> void {
        d->recordedTime = time;
    }

    auto TCFFLDataSetMetaInfo::GetRecordedTime() const -> const QDateTime& {
        return d->recordedTime;
    }

    auto TCFFLDataSetMetaInfo::SetElapsedTime(const double& elapsedTime, const TimeUnit& unit) -> void {
        d->elapsedTime = elapsedTime;
        d->elapsedTimeUnit = unit;
    }

    auto TCFFLDataSetMetaInfo::GetElapsedTime(const TimeUnit& unit) const -> double {
        return ConvertUnit(d->elapsedTime, d->elapsedTimeUnit, unit);
    }

    auto TCFFLDataSetMetaInfo::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto TCFFLDataSetMetaInfo::GetTimeFrameIndex() const -> const int32_t& {
        return d->timeFrameIndex;
    }

    auto TCFFLDataSetMetaInfo::SetPosition(const TCFDataSetRecordedPosition& position) -> void {
        d->position = position;
    }

    auto TCFFLDataSetMetaInfo::GetPosition() const -> const TCFDataSetRecordedPosition& {
        return d->position;
    }

    auto TCFFLDataSetMetaInfo::SetChannelIndex(const int32_t& channelIndex) -> void {
        d->channelIndex = channelIndex;
    }

    auto TCFFLDataSetMetaInfo::GetChannelIndex() const -> const int32_t& {
        return d->channelIndex;
    }
}
