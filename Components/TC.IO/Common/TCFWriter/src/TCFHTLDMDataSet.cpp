#include "TCFHTLDMDataSet.h"

namespace TC::TCFWriter {
    using namespace IO::LdmCore;

    class TCFHTLDMDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IHTLDMDataSetGetter::Pointer dataGetter{};
        TCFHTDataSetMetaInfo metaInfo{};

        LdmConfiguration ldmConfiguration3D{};
        LdmConfiguration ldmConfigurationMIP{};
    };

    TCFHTLDMDataSet::TCFHTLDMDataSet() : d(std::make_unique<Impl>()) {
    }

    TCFHTLDMDataSet::TCFHTLDMDataSet(const TCFHTLDMDataSet& other) : d(std::make_unique<Impl>(*other.d)) {
    }

    TCFHTLDMDataSet::~TCFHTLDMDataSet() = default;

    auto TCFHTLDMDataSet::operator=(const TCFHTLDMDataSet& other) -> TCFHTLDMDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFHTLDMDataSet::SetDataGetter(const IHTLDMDataSetGetter::Pointer& dataGetter) -> void {
        d->dataGetter = dataGetter;
    }

    auto TCFHTLDMDataSet::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration3D, 
        const LdmConfiguration& ldmConfigurationMip) -> void {
        d->ldmConfiguration3D = ldmConfiguration3D;
        d->ldmConfigurationMIP = ldmConfigurationMip;
    }

    auto TCFHTLDMDataSet::GetLdmConfiguration3D() -> LdmConfiguration {
        return d->ldmConfiguration3D;
    }

    auto TCFHTLDMDataSet::GetLdmConfigurationMIP() -> LdmConfiguration {
        return d->ldmConfigurationMIP;
    }

    auto TCFHTLDMDataSet::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
        const auto tileData = d->dataGetter->GetTileData(tileIndex);
        return tileData;
    }

    auto TCFHTLDMDataSet::GetTileDataMIP(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
        const auto tileDataMip = d->dataGetter->GetMipTileData(tileIndex);
        return tileDataMip;
    }

    auto TCFHTLDMDataSet::SetMetaInfo(const TCFHTDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFHTLDMDataSet::GetMetaInfo() const -> TCFHTDataSetMetaInfo {
        return d->metaInfo;
    }
}
