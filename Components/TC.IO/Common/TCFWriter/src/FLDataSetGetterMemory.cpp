#include "FLDataSetGetterMemory.h"

#include "MemoryDataCropper.h"

class FLDataSetGetterMemory::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    std::shared_ptr<float[]> data{ nullptr };
    MemoryOrder3D memoryOrder3D{ MemoryOrder3D::XYZ };

    std::shared_ptr<float[]> dataMIP{ nullptr };
    MemoryOrder2D memoryOrder2D{ MemoryOrder2D::XY };

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};
};

FLDataSetGetterMemory::FLDataSetGetterMemory() : d(new Impl()) {
}

FLDataSetGetterMemory::~FLDataSetGetterMemory() = default;

auto FLDataSetGetterMemory::SetData(const std::shared_ptr<float[]>&data, const MemoryOrder3D & memoryOrder) -> void {
    d->data = data;
    d->memoryOrder3D = memoryOrder;
}

auto FLDataSetGetterMemory::SetMIPData(const std::shared_ptr<float[]>&data, const MemoryOrder2D & memoryOrder) -> void {
    d->dataMIP = data;
    d->memoryOrder2D = memoryOrder;
}

auto FLDataSetGetterMemory::SetDataSize(const int32_t & sizeX, const int32_t & sizeY, const int32_t & sizeZ) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;
}

auto FLDataSetGetterMemory::GetData() const -> std::shared_ptr<float[]> {
    return d->data;
}

auto FLDataSetGetterMemory::GetData(const int32_t & x0, const int32_t & x1, const int32_t & y0, const int32_t & y1,
    const int32_t & z0, const int32_t & z1) const -> std::shared_ptr<float[]> {
    MemoryDataCropper memoryDataCropper;
    memoryDataCropper.SetMemoryInfo(d->sizeX, d->sizeY, d->sizeZ, d->memoryOrder3D);
    memoryDataCropper.SetMemory(reinterpret_cast<const uint8_t*>(d->data.get()), sizeof(float));
    memoryDataCropper.SetCropInfo(x0, x1, y0, y1, z0, z1);

    memoryDataCropper.Crop();

    const auto cropSizeX = x1 - x0 + 1;
    const auto cropSizeY = y1 - y0 + 1;
    const auto cropSizeZ = z1 - z0 + 1;

    const auto numberOfCropElements = cropSizeX * cropSizeY * cropSizeZ;

    std::shared_ptr<float[]> croppedData{ new float[numberOfCropElements]() };
    memoryDataCropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedData.get()));

    return croppedData;
}

auto FLDataSetGetterMemory::GetDataMemoryOrder() const -> MemoryOrder3D {
    return d->memoryOrder3D;
}

auto FLDataSetGetterMemory::GetMIPData() const -> std::shared_ptr<float[]> {
    return d->dataMIP;
}

auto FLDataSetGetterMemory::GetMIPData(const int32_t & x0, const int32_t & x1, const int32_t & y0, const int32_t & y1)
const -> std::shared_ptr<float[]> {
    MemoryOrder3D memoryOrder{ MemoryOrder3D::XYZ };
    if (d->memoryOrder2D == +MemoryOrder2D::XY) {
        memoryOrder = MemoryOrder3D::XYZ;
    } else { //YX
        memoryOrder = MemoryOrder3D::YXZ;
    }

    MemoryDataCropper memoryDataCropper;
    memoryDataCropper.SetMemoryInfo(d->sizeX, d->sizeY, 1, memoryOrder);
    memoryDataCropper.SetMemory(reinterpret_cast<const uint8_t*>(d->dataMIP.get()), sizeof(float));
    memoryDataCropper.SetCropInfo(x0, x1, y0, y1, 0, 0);

    memoryDataCropper.Crop();

    const auto cropSizeX = x1 - x0 + 1;
    const auto cropSizeY = y1 - y0 + 1;
    constexpr auto cropSizeZ = 1;

    const auto numberOfCropElements = cropSizeX * cropSizeY * cropSizeZ;

    std::shared_ptr<float[]> croppedData{ new float[numberOfCropElements]() };
    memoryDataCropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedData.get()));

    return croppedData;
}

auto FLDataSetGetterMemory::GetMIPDataMemoryOrder() const -> MemoryOrder2D {
    return d->memoryOrder2D;
}
