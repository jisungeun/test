#include "TCFHTCompleter.h"

#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"

#include "TCLogger.h"

class TCFHTCompleter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetTCFFilePath{};

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t sizeZ{};

    float worldPixelSizeX{};
    float worldPixelSizeY{};
    float worldPixelSizeZ{};
    LengthUnit worldPixelSizeUnit{ LengthUnit::Micrometer };

    TimeUnit timeIntervalUnit{ TimeUnit::Second };

    bool ldmOptionFlag{ true };
};

TCFHTCompleter::TCFHTCompleter() : d(new Impl()) {
}

TCFHTCompleter::~TCFHTCompleter() = default;

auto TCFHTCompleter::SetTargetTCFFilePath(const QString& targetTCFFilePath) -> void {
    d->targetTCFFilePath = targetTCFFilePath;
}

auto TCFHTCompleter::SetLdmOptionFlag(const bool& ldmOptionFlag) -> void {
    d->ldmOptionFlag = ldmOptionFlag;
}

auto TCFHTCompleter::SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->sizeZ = sizeZ;
}

auto TCFHTCompleter::SetPixelWorldSize(const float& worldPixelSizeX, const float& worldPixelSizeY,
    const float& worldPixelSizeZ, const LengthUnit& unit)->void {
    d->worldPixelSizeX = worldPixelSizeX;
    d->worldPixelSizeY = worldPixelSizeY;
    d->worldPixelSizeZ = worldPixelSizeZ;
    d->worldPixelSizeUnit = unit;
}


auto TCFHTCompleter::MakeCompleteGroup() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };

    try {
        H5::H5File file(d->targetTCFFilePath.toStdString(), H5F_ACC_RDWR);
        {
            const auto data3dGroup = file.openGroup("/Data/3D");

            const auto dataCount = static_cast<int64_t>(data3dGroup.getNumObjs());
            const auto resolutionX = ConvertUnit(d->worldPixelSizeX, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionY = ConvertUnit(d->worldPixelSizeY, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionZ = ConvertUnit(d->worldPixelSizeZ, d->worldPixelSizeUnit, LengthUnit::Micrometer);

            double minValueForAllDataSet = std::numeric_limits<double>::max();
            double maxValueForAllDataSet = std::numeric_limits<double>::lowest();
            for (auto dataSetIndex = 0; dataSetIndex < dataCount; ++dataSetIndex) {
                const auto timeFrameName = data3dGroup.getObjnameByIdx(dataSetIndex);

                double minValue, maxValue;

                if (d->ldmOptionFlag) {
                    const auto ldmGroup = data3dGroup.openGroup(timeFrameName);

                    minValue = ReadAttributeDouble(ldmGroup, "RIMin");
                    maxValue = ReadAttributeDouble(ldmGroup, "RIMax");
                } else {
                    const auto dataSet = data3dGroup.openDataSet(timeFrameName);

                    minValue = ReadAttributeDouble(dataSet, "RIMin");
                    maxValue = ReadAttributeDouble(dataSet, "RIMax");
                }

                if (minValue < minValueForAllDataSet) {
                    minValueForAllDataSet = minValue;
                }
                if (maxValue > maxValueForAllDataSet) {
                    maxValueForAllDataSet = maxValue;
                }
            }

            if (!data3dGroup.attrExists("SizeZ")) {
                WriteAttributeInt64(data3dGroup, "DataCount", dataCount);
                WriteAttributeDouble(data3dGroup, "ResolutionX", resolutionX);
                WriteAttributeDouble(data3dGroup, "ResolutionY", resolutionY);
                WriteAttributeDouble(data3dGroup, "ResolutionZ", resolutionZ);
                WriteAttributeDouble(data3dGroup, "RIMin", minValueForAllDataSet);
                WriteAttributeDouble(data3dGroup, "RIMax", maxValueForAllDataSet);
                WriteAttributeInt64(data3dGroup, "SizeX", static_cast<int64_t>(d->sizeX));
                WriteAttributeInt64(data3dGroup, "SizeY", static_cast<int64_t>(d->sizeY));
                WriteAttributeInt64(data3dGroup, "SizeZ", static_cast<int64_t>(d->sizeZ));
            }
        }

        {
            const auto data2dMIPGroup = file.openGroup("/Data/2DMIP");

            const auto dataCount = static_cast<int64_t>(data2dMIPGroup.getNumObjs());
            const auto resolutionX = ConvertUnit(d->worldPixelSizeX, d->worldPixelSizeUnit, LengthUnit::Micrometer);
            const auto resolutionY = ConvertUnit(d->worldPixelSizeY, d->worldPixelSizeUnit, LengthUnit::Micrometer);

            double minValueForAllDataSet = std::numeric_limits<double>::max();
            double maxValueForAllDataSet = std::numeric_limits<double>::lowest();
            for (auto dataSetIndex = 0; dataSetIndex < dataCount; ++dataSetIndex) {
                const auto timeFrameName = data2dMIPGroup.getObjnameByIdx(dataSetIndex);

                double minValue, maxValue;

                if (d->ldmOptionFlag) {
                    const auto ldmGroup = data2dMIPGroup.openGroup(timeFrameName);

                    minValue = ReadAttributeDouble(ldmGroup, "RIMin");
                    maxValue = ReadAttributeDouble(ldmGroup, "RIMax");
                } else {
                    const auto dataSet = data2dMIPGroup.openDataSet(timeFrameName);

                    minValue = ReadAttributeDouble(dataSet, "RIMin");
                    maxValue = ReadAttributeDouble(dataSet, "RIMax");
                }

                if (minValue < minValueForAllDataSet) {
                    minValueForAllDataSet = minValue;
                }
                if (maxValue > maxValueForAllDataSet) {
                    maxValueForAllDataSet = maxValue;
                }
            }
            if (!data2dMIPGroup.attrExists("SizeY")) {
                WriteAttributeInt64(data2dMIPGroup, "DataCount", dataCount);
                WriteAttributeDouble(data2dMIPGroup, "ResolutionX", resolutionX);
                WriteAttributeDouble(data2dMIPGroup, "ResolutionY", resolutionY);
                WriteAttributeDouble(data2dMIPGroup, "RIMin", minValueForAllDataSet);
                WriteAttributeDouble(data2dMIPGroup, "RIMax", maxValueForAllDataSet);
                WriteAttributeInt64(data2dMIPGroup, "SizeX", static_cast<int64_t>(d->sizeX));
                WriteAttributeInt64(data2dMIPGroup, "SizeY", static_cast<int64_t>(d->sizeY));
            }
        }

        file.close();
    }
    catch (const H5::Exception& exception) {
        QLOG_ERROR() << "TCFHTCompleter Error";
        QLOG_ERROR() << exception.getCDetailMsg();
        QLOG_ERROR() << "targetTCFFilePath : " << d->targetTCFFilePath.toStdString().c_str();
        QLOG_ERROR() << "sizeX, sizeY, sizeZ : [" << d->sizeX << ", " << d->sizeY << ", " << d->sizeZ << "]";
        QLOG_ERROR() << "pixelSizeX, pixelSizeY, pixelSizeZ : [" << d->worldPixelSizeX<< ", " << d->worldPixelSizeX << ", " << d->worldPixelSizeX << "] in "<< d->worldPixelSizeUnit._to_string();

        return false;
    }

    return true;
}
