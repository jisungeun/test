#include "BFDataSetGetterMemory.h"

#include "MemoryDataCropper.h"

class BFDataSetGetterMemory::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    std::shared_ptr<uint8_t[]> data{ nullptr };
    MemoryOrder3D memoryOrder{ MemoryOrder3D::XYZ };

    int32_t sizeX{};
    int32_t sizeY{};
    int32_t channelCount{};
};

BFDataSetGetterMemory::BFDataSetGetterMemory() : d(new Impl()) {
}

BFDataSetGetterMemory::~BFDataSetGetterMemory() = default;

auto BFDataSetGetterMemory::SetData(const std::shared_ptr<uint8_t[]>& data, const MemoryOrder3D& memoryOrder) -> void {
    d->data = data;
    d->memoryOrder = memoryOrder;
}

auto BFDataSetGetterMemory::SetDataSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& channelCount)
    -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
    d->channelCount = channelCount;
}

auto BFDataSetGetterMemory::GetData() const -> std::shared_ptr<uint8_t[]> {
    return d->data;
}

auto BFDataSetGetterMemory::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
    const int32_t& y1) const -> std::shared_ptr<uint8_t[]> {
    MemoryDataCropper memoryDataCropper;
    memoryDataCropper.SetMemoryInfo(d->sizeX, d->sizeY, d->channelCount, d->memoryOrder);
    memoryDataCropper.SetMemory(d->data.get(), sizeof(uint8_t));
    memoryDataCropper.SetCropInfo(x0, x1, y0, y1, 0, static_cast<int64_t>(d->channelCount - 1));

    memoryDataCropper.Crop();

    const auto cropSizeX = x1 - x0 + 1;
    const auto cropSizeY = y1 - y0 + 1;
    const auto cropSizeZ = d->channelCount;

    const auto numberOfCropElements = cropSizeX * cropSizeY * cropSizeZ;

    std::shared_ptr<uint8_t[]> croppedData{ new uint8_t[numberOfCropElements]() };
    memoryDataCropper.GetCroppedMemory(croppedData.get());

    return croppedData;
}

auto BFDataSetGetterMemory::GetColorChannelCount() const -> int32_t {
    return d->channelCount;
}

auto BFDataSetGetterMemory::GetDataMemoryOrder() const -> MemoryOrder3D {
    return d->memoryOrder;
}
