#include "TCFSkeletonConfig.h"

class TCFSkeletonConfig::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    bool htIncludingFlag{ false };
    bool flIncludingFlag{ false };
    bool bfIncludingFlag{ false };
};

TCFSkeletonConfig::TCFSkeletonConfig() : d(new Impl()) {
}

TCFSkeletonConfig::TCFSkeletonConfig(const TCFSkeletonConfig& other) : d(new Impl(*other.d)) {
}

TCFSkeletonConfig::~TCFSkeletonConfig() = default;

auto TCFSkeletonConfig::operator=(const TCFSkeletonConfig& other) -> TCFSkeletonConfig& {
    *(this->d) = *(other.d);
    return *this;
}

auto TCFSkeletonConfig::SetDataIncludingFlag(const bool& htIncludingFlag, const bool& flIncludingFlag,
    const bool& bfIncludingFlag) -> void {
    d->htIncludingFlag = htIncludingFlag;
    d->flIncludingFlag = flIncludingFlag;
    d->bfIncludingFlag = bfIncludingFlag;
}

auto TCFSkeletonConfig::GetHTIncludingFlag() const -> const bool& {
    return d->htIncludingFlag;
}

auto TCFSkeletonConfig::GetFLIncludingFlag() const -> const bool& {
    return d->flIncludingFlag;
}

auto TCFSkeletonConfig::GetBFIncludingFlag() const -> const bool& {
    return d->bfIncludingFlag;
}
