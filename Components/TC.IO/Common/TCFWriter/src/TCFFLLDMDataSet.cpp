#include "TCFFLLDMDataSet.h"

namespace TC::TCFWriter {
    using namespace IO::LdmCore;

    class TCFFLLDMDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IFLLDMDataSetGetter::Pointer dataGetter{};
        TCFFLDataSetMetaInfo metaInfo{};

        LdmConfiguration ldmConfiguration3D{};
        LdmConfiguration ldmConfigurationMIP{};
    };

    TCFFLLDMDataSet::TCFFLLDMDataSet() : d{ std::make_unique<Impl>() } {
    }

    TCFFLLDMDataSet::TCFFLLDMDataSet(const TCFFLLDMDataSet& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    TCFFLLDMDataSet::~TCFFLLDMDataSet() = default;

    auto TCFFLLDMDataSet::operator=(const TCFFLLDMDataSet& other) -> TCFFLLDMDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFFLLDMDataSet::SetDataGetter(const IFLLDMDataSetGetter::Pointer& dataGetter) -> void {
        d->dataGetter = dataGetter;
    }

    auto TCFFLLDMDataSet::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration3D,
        const LdmConfiguration& ldmConfigurationMip) -> void {
        d->ldmConfiguration3D = ldmConfiguration3D;
        d->ldmConfigurationMIP = ldmConfigurationMip;
    }

    auto TCFFLLDMDataSet::GetLdmConfiguration3D() -> LdmConfiguration {
        return d->ldmConfiguration3D;
    }

    auto TCFFLLDMDataSet::GetLdmConfigurationMIP() -> LdmConfiguration {
        return d->ldmConfigurationMIP;
    }

    auto TCFFLLDMDataSet::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
        const auto tileData = d->dataGetter->GetTileData(tileIndex);
        return tileData;
    }

    auto TCFFLLDMDataSet::GetTileDataMIP(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
        const auto tileDataMip = d->dataGetter->GetMipTileData(tileIndex);
        return tileDataMip;
    }

    auto TCFFLLDMDataSet::SetMetaInfo(const TCFFLDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFFLLDMDataSet::GetMetaInfo() const -> TCFFLDataSetMetaInfo {
        return d->metaInfo;
    }
}
