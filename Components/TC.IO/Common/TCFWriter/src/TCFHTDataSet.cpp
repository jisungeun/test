#include "TCFHTDataSet.h"

namespace TC::TCFWriter {
    class TCFHTDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        IHTDataSetGetter::Pointer htDataSetGetter{ nullptr };

        TCFHTDataSetMetaInfo metaInfo{};
    };

    TCFHTDataSet::TCFHTDataSet() : d(new Impl()) {
    }

    TCFHTDataSet::TCFHTDataSet(const TCFHTDataSet& other) : d(new Impl(*other.d)) {
    }

    TCFHTDataSet::~TCFHTDataSet() = default;

    auto TCFHTDataSet::operator=(const TCFHTDataSet& other) -> TCFHTDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFHTDataSet::SetDataSetGetter(const IHTDataSetGetter::Pointer& dataSetGetter) -> void {
        d->htDataSetGetter = dataSetGetter;
    }

    auto TCFHTDataSet::GetData() const -> std::shared_ptr<float[]> {
        return d->htDataSetGetter->GetData();
    }

    auto TCFHTDataSet::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
        const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> {
        return d->htDataSetGetter->GetData(x0, x1, y0, y1, z0, z1);
    }

    auto TCFHTDataSet::GetDataMemoryOrder() const -> MemoryOrder3D {
        return d->htDataSetGetter->GetDataMemoryOrder();
    }

    auto TCFHTDataSet::GetMIPData() const -> std::shared_ptr<float[]> {
        return d->htDataSetGetter->GetMIPData();
    }

    auto TCFHTDataSet::GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
        -> std::shared_ptr<float[]> {
        return d->htDataSetGetter->GetMIPData(x0, x1, y0, y1);
    }

    auto TCFHTDataSet::GetMIPDataMemoryOrder() const -> MemoryOrder2D {
        return d->htDataSetGetter->GetMIPDataMemoryOrder();
    }

    auto TCFHTDataSet::SetMetaInfo(const TCFHTDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFHTDataSet::GetMetaInfo() -> TCFHTDataSetMetaInfo {
        return d->metaInfo;
    }
}
