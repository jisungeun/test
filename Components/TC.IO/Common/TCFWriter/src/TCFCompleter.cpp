#include "TCFCompleter.h"

#include "HDF5Mutex.h"
#include "TCFWriterUtility.h"
class TCFCompleter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString targetTCFFilePath{};

    TCFCommonInfo tcfCommonInfo{};
    TCFDeviceInfo tcfDeviceInfo{};

    TCFTileInfo tcfTileInfo{};
    bool tileInfoSet{ false };

    TCFStructureInfo tcfStructureInfo{};
};

TCFCompleter::TCFCompleter() : d(new Impl()) {
}

TCFCompleter::~TCFCompleter() = default;

auto TCFCompleter::SetTargetTCFFilePath(const QString& targetTCFFilePath) -> void {
    d->targetTCFFilePath = targetTCFFilePath;
}

auto TCFCompleter::SetTCFCommonInfo(const TCFCommonInfo& tcfCommonInfo) -> void {
    d->tcfCommonInfo = tcfCommonInfo;
}

auto TCFCompleter::SetTCFDeviceInfo(const TCFDeviceInfo& tcfDeviceInfo) -> void {
    d->tcfDeviceInfo = tcfDeviceInfo;
}

auto TCFCompleter::SetTCFTileInfo(const TCFTileInfo& tcfTileInfo) -> void {
    d->tcfTileInfo = tcfTileInfo;
    d->tileInfoSet = true;
}

auto TCFCompleter::SetTCFStructureInfo(const TCFStructureInfo& tcfStructureInfo) -> void {
    d->tcfStructureInfo = tcfStructureInfo;
}

auto TCFCompleter::MakeTCFComplete() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    try {
        const H5::H5File file(d->targetTCFFilePath.toStdString(), H5F_ACC_RDWR);
        
        WriteAttributeString(file, "CreateDate", d->tcfCommonInfo.GetCreateDate().toString("yyyy-MM-dd HH:mm:ss"));
        WriteAttributeString(file, "DataID", d->tcfCommonInfo.GetDataID());
        WriteAttributeString(file, "Description", d->tcfCommonInfo.GetDescription());
        WriteAttributeString(file, "DeviceHost", d->tcfCommonInfo.GetDeviceHost());
        WriteAttributeString(file, "DeviceSerial", d->tcfCommonInfo.GetDeviceSerial());
        WriteAttributeString(file, "DeviceModelType", d->tcfCommonInfo.GetDeviceModelType());
        WriteAttributeString(file, "DeviceSoftwareVersion", d->tcfCommonInfo.GetDeviceSoftwareVersion());
        WriteAttributeString(file, "FormatVersion", d->tcfCommonInfo.GetFormatVersion());
        WriteAttributeString(file, "RecordingTime", d->tcfCommonInfo.GetRecordingTime().toString("yyyy-MM-dd HH:mm:ss.zzz"));
        WriteAttributeString(file, "SoftwareVersion", d->tcfCommonInfo.GetSoftwareVersion());
        WriteAttributeString(file, "Title", d->tcfCommonInfo.GetTitle());
        WriteAttributeString(file, "UniqueID", d->tcfCommonInfo.GetUniqueID());
        WriteAttributeString(file, "UserID", d->tcfCommonInfo.GetUserID());

        const auto dataGroup = file.openGroup("/Data");
        WriteAttributeInt64(dataGroup, "StructureType", d->tcfStructureInfo.GetStructureType()._to_index());
        WriteAttributeString(dataGroup, "StructureTypeName", d->tcfStructureInfo.GetStructureType()._to_string());

        const auto infoGroup = file.openGroup("/Info");

        const auto annotationGroup = infoGroup.createGroup("Annotation");

        const auto deviceGroup = infoGroup.createGroup("Device");
        WriteAttributeDouble(deviceGroup, "Magnification", d->tcfDeviceInfo.GetMagnification());
        WriteAttributeDouble(deviceGroup, "NA", d->tcfDeviceInfo.GetNA());
        WriteAttributeDouble(deviceGroup, "RI", d->tcfDeviceInfo.GetRI());

        if (d->tileInfoSet) {
            const auto tileGroup = infoGroup.createGroup("Tile");
            WriteAttributeDouble(tileGroup, "Overlap_H", d->tcfTileInfo.GetOverlapLengthH(LengthUnit::Micrometer));
            WriteAttributeDouble(tileGroup, "Overlap_V", d->tcfTileInfo.GetOverlapLengthV(LengthUnit::Micrometer));
            WriteAttributeDouble(tileGroup, "C", d->tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionC(LengthUnit::Millimenter));
            WriteAttributeDouble(tileGroup, "X", d->tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionX(LengthUnit::Millimenter));
            WriteAttributeDouble(tileGroup, "Y", d->tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionY(LengthUnit::Millimenter));
            WriteAttributeDouble(tileGroup, "Z", d->tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionZ(LengthUnit::Millimenter));
            WriteAttributeInt64(tileGroup, "Tiles_H", d->tcfTileInfo.GetTileNumberH());
            WriteAttributeInt64(tileGroup, "Tiles_V", d->tcfTileInfo.GetTileNumberV());
            WriteAttributeInt64(tileGroup, "Tiles", static_cast<int64_t>(d->tcfTileInfo.GetTileNumberH() * d->tcfTileInfo.GetTileNumberV()));
            WriteAttributeInt64(tileGroup, "ColumnIndex", d->tcfTileInfo.GetTileIndexColumn());
            WriteAttributeInt64(tileGroup, "RowIndex", d->tcfTileInfo.GetTileIndexRow());
        }
    } catch (const H5::Exception&) {
        return false;
    }
    return true;
}
