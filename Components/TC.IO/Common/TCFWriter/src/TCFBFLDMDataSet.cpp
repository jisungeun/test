#include "TCFBFLDMDataSet.h"

namespace TC::TCFWriter {
    using namespace IO::LdmCore;

    class TCFBFLDMDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl & other) = default;
        ~Impl() = default;

        auto operator=(const Impl & other)->Impl & = default;

        IBFLDMDataSetGetter::Pointer dataGetter{};
        TCFBFDataSetMetaInfo metaInfo{};

        LdmConfiguration ldmConfiguration;
    };

    TCFBFLDMDataSet::TCFBFLDMDataSet() : d{ std::make_unique<Impl>() } {
    }

    TCFBFLDMDataSet::TCFBFLDMDataSet(const TCFBFLDMDataSet& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    TCFBFLDMDataSet::~TCFBFLDMDataSet() = default;

    auto TCFBFLDMDataSet::operator=(const TCFBFLDMDataSet& other) -> TCFBFLDMDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFBFLDMDataSet::SetDataGetter(const IBFLDMDataSetGetter::Pointer& dataGetter) -> void {
        d->dataGetter = dataGetter;
    }

    auto TCFBFLDMDataSet::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration) -> void {
        d->ldmConfiguration = ldmConfiguration;
    }

    auto TCFBFLDMDataSet::GetLdmConfiguration() -> LdmConfiguration {
        return d->ldmConfiguration;
    }

    auto TCFBFLDMDataSet::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<uint8_t[]> {
        const auto tileData = d->dataGetter->GetTileData(tileIndex);
        return tileData;
    }

    auto TCFBFLDMDataSet::GetChannelCount() -> int32_t {
        return d->dataGetter->GetChannelCount();
    }

    auto TCFBFLDMDataSet::SetMetaInfo(const TCFBFDataSetMetaInfo& metaInfo) -> void {
        d->metaInfo = metaInfo;
    }

    auto TCFBFLDMDataSet::GetMetaInfo() const -> TCFBFDataSetMetaInfo {
        return d->metaInfo;
    }
}
