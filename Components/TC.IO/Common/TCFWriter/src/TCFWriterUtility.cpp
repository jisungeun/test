#include <memory>
#include <QUuid>

#include "TCFWriterUtility.h"

auto CalculatePowerOf2LessThan(const int32_t& value)->int32_t {
    const auto exponential = std::floor(std::log2(static_cast<double>(value)));
    const auto powerOf2 = static_cast<int32_t>(std::pow(2, exponential));

    return powerOf2;
}

auto CalculateChunkSize(const int32_t& dataSizeX, const int32_t& dataSizeY)->int32_t {
    const auto biggerOne = std::max(dataSizeX, dataSizeY);
    const auto chunkSize = CalculatePowerOf2LessThan(biggerOne);

    return chunkSize;
}

auto CreateCompPropList2D(const int32_t& dataSizeX, const int32_t& dataSizeY) -> H5::DSetCreatPropList {
    constexpr int32_t rank = 2;
    const auto chunkSize = std::min(512, CalculateChunkSize(dataSizeX, dataSizeY));

    std::shared_ptr<hsize_t[]> chunkDims{ new hsize_t[rank] };
    chunkDims[0] = chunkSize;
    chunkDims[1] = chunkSize;

    H5::DSetCreatPropList propList;
    propList.setChunk(rank, chunkDims.get());
    propList.setDeflate(1);

    return propList;
}

auto CreateCompPropListBF(const int32_t& dataSizeX, const int32_t& dataSizeY) -> H5::DSetCreatPropList {
    constexpr int32_t rank = 3;
    const auto chunkSize = std::min(512, CalculateChunkSize(dataSizeX, dataSizeY));

    std::shared_ptr<hsize_t[]> chunkDims{ new hsize_t[rank] };
    chunkDims[0] = 1;
    chunkDims[1] = chunkSize;
    chunkDims[2] = chunkSize;

    H5::DSetCreatPropList propList;
    propList.setChunk(rank, chunkDims.get());
    propList.setDeflate(1);

    return propList;
}

auto CreateCompPropList3D(const int32_t& dataSizeX, const int32_t& dataSizeY, const int32_t& dataSizeZ) -> H5::DSetCreatPropList {
    constexpr int32_t rank = 3;
    const auto chunkSizeXY = std::min(128, CalculateChunkSize(dataSizeX, dataSizeY));
    const auto chunkSizeZ = std::min(64, CalculatePowerOf2LessThan(dataSizeZ));
    
    std::shared_ptr<hsize_t[]> chunkDims{ new hsize_t[rank] };
    chunkDims[0] = chunkSizeZ;
    chunkDims[1] = chunkSizeXY;
    chunkDims[2] = chunkSizeXY;

    H5::DSetCreatPropList propList;
    propList.setChunk(rank, chunkDims.get());
    propList.setDeflate(1);

    return propList;
}

auto TimeFrameIndexName(const int32_t& timeFrameIndex) -> QString {
    return QString("%1").arg(timeFrameIndex, 6, 10, QLatin1Char('0'));
}

auto MakeDataSpace(const int64_t& sizeX, const int64_t& sizeY, const int64_t& sizeZ) -> H5::DataSpace {
    constexpr auto rank = 3;
    const std::shared_ptr<hsize_t[]> dims{ new hsize_t[rank]() };
    dims.get()[0] = sizeZ;
    dims.get()[1] = sizeY;
    dims.get()[2] = sizeX;
    
    H5::DataSpace dataSpace(rank, dims.get());
    return dataSpace;
}

auto MakeDataSpace(const int64_t& sizeX, const int64_t& sizeY) -> H5::DataSpace {
    constexpr auto rank = 2;
    const std::shared_ptr<hsize_t[]> dims{ new hsize_t[rank]() };
    dims.get()[0] = sizeY;
    dims.get()[1] = sizeX;

    H5::DataSpace dataSpace(rank, dims.get());
    return dataSpace;
}

auto MakeDataSpace(const int64_t& length) -> H5::DataSpace {
    constexpr auto rank = 1;
    const std::shared_ptr<hsize_t[]> dims{ new hsize_t[rank]() };
    dims.get()[0] = length;

    H5::DataSpace dataSpace(rank, dims.get());
    return dataSpace;
}

auto MakeSelectedDataSpace(const WholeSize& wholeSize, const SubsetIndex& selectedIndex) -> H5::DataSpace {
    H5::DataSpace dataSpace;
    if (wholeSize.sizeZ == 0) {
        constexpr auto rank = 2;

        const std::shared_ptr<hsize_t[]> wholeDims{ new hsize_t[rank]() };
        wholeDims.get()[0] = wholeSize.sizeY;
        wholeDims.get()[1] = wholeSize.sizeX;

        dataSpace = H5::DataSpace(rank, wholeDims.get());

        const std::shared_ptr<hsize_t[]> start{ new hsize_t[rank] };
        start.get()[0] = selectedIndex.startY;
        start.get()[1] = selectedIndex.startX;

        const std::shared_ptr<hsize_t[]> count{ new hsize_t[rank] };
        count.get()[0] = selectedIndex.endY - selectedIndex.startY + 1;
        count.get()[1] = selectedIndex.endX - selectedIndex.startX + 1;

        dataSpace.selectHyperslab(H5S_SELECT_SET, count.get(), start.get());
    } else {
        constexpr auto rank = 3;

        const std::shared_ptr<hsize_t[]> wholeDims{ new hsize_t[rank]() };
        wholeDims.get()[0] = wholeSize.sizeZ;
        wholeDims.get()[1] = wholeSize.sizeY;
        wholeDims.get()[2] = wholeSize.sizeX;

        dataSpace = H5::DataSpace(rank, wholeDims.get());

        const std::shared_ptr<hsize_t[]> start{ new hsize_t[rank] };
        start.get()[0] = selectedIndex.startZ;
        start.get()[1] = selectedIndex.startY;
        start.get()[2] = selectedIndex.startX;

        const std::shared_ptr<hsize_t[]> count{ new hsize_t[rank] };
        count.get()[0] = selectedIndex.endZ - selectedIndex.startZ + 1;
        count.get()[1] = selectedIndex.endY - selectedIndex.startY + 1;
        count.get()[2] = selectedIndex.endX - selectedIndex.startX + 1;

        dataSpace.selectHyperslab(H5S_SELECT_SET, count.get(), start.get());
    }

    return dataSpace;
}

auto WriteAttributeString(const H5::H5Object& target, const QString& name, const QString& contents,
    const int32_t& fixedSpace) -> void {
    const auto stringLength = (fixedSpace == 0) ? (contents.size() + 2) : (fixedSpace);

    auto strDataType = H5::StrType(H5::PredType::C_S1, stringLength);
    strDataType.setStrpad(H5T_STR_NULLPAD);

    auto strDataSpace = MakeDataSpace(1);
    auto attribute = target.createAttribute(name.toStdString(), strDataType, strDataSpace);

    QByteArray ba = contents.toUtf8();
    const std::unique_ptr<uint8_t> charBuffer(new uint8_t[stringLength]());
    memcpy_s(charBuffer.get(), stringLength, ba.data(), contents.size());

    attribute.write(strDataType, charBuffer.get());

    attribute.close();
    strDataSpace.close();
    strDataType.close();
}

auto WriteAttributeDouble(const H5::H5Object& target, const QString& name, const double& contents) -> void {
    auto dataSpace = MakeDataSpace(1);
    auto attribute = target.createAttribute(name.toStdString(), H5::PredType::NATIVE_DOUBLE, dataSpace);

    attribute.write(H5::PredType::NATIVE_DOUBLE, &contents);

    attribute.close();
    dataSpace.close();
}

auto WriteAttributeInt64(const H5::H5Object& target, const QString& name, const int64_t& contents) -> void {
    auto dataSpace = MakeDataSpace(1);
    auto attribute = target.createAttribute(name.toStdString(), H5::PredType::NATIVE_INT64, dataSpace);

    attribute.write(H5::PredType::NATIVE_INT64, &contents);

    attribute.close();
    dataSpace.close();
}

auto WriteAttributeUint8(const H5::H5Object& target, const QString& name, const uint8_t& contents) -> void {
    auto dataSpace = MakeDataSpace(1);
    auto attribute = target.createAttribute(name.toStdString(), H5::PredType::NATIVE_UINT8, dataSpace);

    attribute.write(H5::PredType::NATIVE_UINT8, &contents);

    attribute.close();
    dataSpace.close();
}

auto ReadAttributeInt64(const H5::H5Object& object, const std::string& attributeName) -> int64_t {
    auto readingAttribute = object.openAttribute(attributeName);
    int64_t attributeValue{};
    readingAttribute.read(H5::PredType::NATIVE_INT64, &attributeValue);
    readingAttribute.close();
    return attributeValue;
}

auto ReadAttributeDouble(const H5::H5Object& object, const std::string& attributeName) -> double {
    auto readingAttribute = object.openAttribute(attributeName);
    double attributeValue{};
    readingAttribute.read(H5::PredType::NATIVE_DOUBLE, &attributeValue);
    readingAttribute.close();
    return attributeValue;
}

auto ReadAttributeUint8(const H5::H5Object& object, const std::string& attributeName) -> uint8_t {
    auto readingAttribute = object.openAttribute(attributeName);
    uint8_t attributeValue{};
    readingAttribute.read(H5::PredType::NATIVE_UINT8, &attributeValue);
    readingAttribute.close();
    return attributeValue;
}

auto ReadAttributeString(const H5::H5Object& object, const std::string& attributeName) -> std::string {
    auto readingAttribute = object.openAttribute(attributeName);
    std::string attributeValue;
    readingAttribute.read(readingAttribute.getDataType(), attributeValue);
    readingAttribute.close();
    return attributeValue;
}

auto ToTimeString(const QDateTime& dateTime) -> QString {
    return dateTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
}

template<typename T>
auto ConvertMemoryOrder(const T* fromData, T* toData, const int64_t& sizeX, const int64_t& sizeY,
    const int64_t& sizeZ, const MemoryOrder3D& from, const MemoryOrder3D& to)->void {

    if ((from == +MemoryOrder3D::XYZ) && (to == +MemoryOrder3D::YXZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;
                    const auto toIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;

                   toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::XYZ) && (to == +MemoryOrder3D::ZXY)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;
                    const auto toIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::XYZ) && (to == +MemoryOrder3D::ZYX)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;
                    const auto toIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::YXZ) && (to == +MemoryOrder3D::XYZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;
                    const auto toIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::YXZ) && (to == +MemoryOrder3D::ZXY)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;
                    const auto toIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::YXZ) && (to == +MemoryOrder3D::ZYX)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;
                    const auto toIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZXY) && (to == +MemoryOrder3D::XYZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;
                    const auto toIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZXY) && (to == +MemoryOrder3D::YXZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;
                    const auto toIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZXY) && (to == +MemoryOrder3D::ZYX)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;
                    const auto toIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZYX) && (to == +MemoryOrder3D::XYZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;
                    const auto toIndex = xIndex + yIndex * sizeX + zIndex * sizeX * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZYX) && (to == +MemoryOrder3D::YXZ)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;
                    const auto toIndex = yIndex + xIndex * sizeY + zIndex * sizeX * sizeY;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    } else if ((from == +MemoryOrder3D::ZYX) && (to == +MemoryOrder3D::ZXY)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                for (int64_t zIndex = 0; zIndex < sizeZ; ++zIndex) {
                    const auto fromIndex = zIndex + yIndex * sizeZ + xIndex * sizeZ * sizeY;
                    const auto toIndex = zIndex + xIndex * sizeZ + yIndex * sizeZ * sizeX;

                    toData[toIndex] = fromData[fromIndex];
                }
            }
        }
    }
}

template<typename T>
auto ConvertMemoryOrder(const T* fromData, T* toData, const int64_t& sizeX, const int64_t& sizeY,
    const MemoryOrder2D& from, const MemoryOrder2D& to)->void {
    if ((from == +MemoryOrder2D::XY) && (to == +MemoryOrder2D::YX)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                const auto fromIndex = xIndex + yIndex * sizeX;
                const auto toIndex = yIndex + xIndex * sizeY;

                toData[toIndex] = fromData[fromIndex];
            }
        }
    } else if ((from == +MemoryOrder2D::YX) && (to == +MemoryOrder2D::XY)) {
        for (int64_t xIndex = 0; xIndex < sizeX; ++xIndex) {
            for (int64_t yIndex = 0; yIndex < sizeY; ++yIndex) {
                const auto fromIndex = yIndex + xIndex * sizeY;
                const auto toIndex = xIndex + yIndex * sizeX;

                toData[toIndex] = fromData[fromIndex];
            }
        }
    }
}

auto ConvertMemoryOrder(const std::shared_ptr<uint16_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const int64_t& sizeZ, const MemoryOrder3D& from, const MemoryOrder3D& to) -> std::shared_ptr<uint16_t[]> {
    if (from == to) {
        return data;
    }

    const auto numberOfElements = sizeX * sizeY * sizeZ;
    std::shared_ptr<uint16_t[]> convertedMemory{ new uint16_t[numberOfElements]() };
    ConvertMemoryOrder<uint16_t>(data.get(), convertedMemory.get(), sizeX, sizeY, sizeZ, from, to);

    return convertedMemory;
}

auto ConvertMemoryOrder(const std::shared_ptr<uint16_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const MemoryOrder2D& from, const MemoryOrder2D& to) -> std::shared_ptr<uint16_t[]> {
    if (from == to) {
        return data;
    }

    const auto numberOfElements = sizeX * sizeY;
    std::shared_ptr<uint16_t[]> convertedMemory{ new uint16_t[numberOfElements]() };
    ConvertMemoryOrder<uint16_t>(data.get(), convertedMemory.get(), sizeX, sizeY, from, to);

    return convertedMemory;
}

auto ConvertMemoryOrder(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const int64_t& sizeZ, const MemoryOrder3D& from, const MemoryOrder3D& to) -> std::shared_ptr<uint8_t[]> {
    if (from == to) {
        return data;
    }

    const auto numberOfElements = sizeX * sizeY * sizeZ;
    std::shared_ptr<uint8_t[]> convertedMemory{ new uint8_t[numberOfElements]() };
    ConvertMemoryOrder<uint8_t>(data.get(), convertedMemory.get(), sizeX, sizeY, sizeZ, from, to);

    return convertedMemory;
}

auto ConvertMemoryOrder(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
    const MemoryOrder2D& from, const MemoryOrder2D& to) -> std::shared_ptr<uint8_t[]> {
    if (from == to) {
        return data;
    }

    const auto numberOfElements = sizeX * sizeY;
    std::shared_ptr<uint8_t[]> convertedMemory{ new uint8_t[numberOfElements]() };
    ConvertMemoryOrder<uint8_t>(data.get(), convertedMemory.get(), sizeX, sizeY, from, to);

    return convertedMemory;
}

auto GenerateUniqueID(const QString& deviceName, const QDateTime& dataRecordedDateTime,
    const QDateTime& dataWritingDateTime) -> QString {
    const auto dataRecordedDateTimeString = dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");
    const auto dataWritingDateTimeString = dataWritingDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");

    QString uuid = QUuid::createUuid().toString();
    uuid.replace(QRegExp("[\\{\\-\\}]"), "");

    const QStringList uniqueIDComponents{ deviceName, dataRecordedDateTimeString, dataWritingDateTimeString, uuid };

    return uniqueIDComponents.join("-");

}

auto GenerateDataID(const QString& deviceName, const QDateTime& dataRecordedDateTime) -> QString {
    const auto dataRecordedDateTimeString = dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");
    const QStringList dataIDComponents{ deviceName, dataRecordedDateTimeString };

    return dataIDComponents.join("-");
}

