#include "TCFBFDataSetMetaInfo.h"

namespace TC::TCFWriter {
    class TCFBFDataSetMetaInfo::Impl {
    public:
        Impl() = default;
        Impl(const Impl & other) = default;
        ~Impl() = default;

        auto operator=(const Impl & other)->Impl & = default;

        int32_t dataSizeX{};
        int32_t dataSizeY{};

        float pixelWorldSizeX{};
        float pixelWorldSizeY{};
        LengthUnit pixelWorldSizeUnit{ LengthUnit::Micrometer };

        QDateTime recordedTime{};
        double elapsedTime{};
        TimeUnit elapsedTimeUnit{ TimeUnit::Second };

        int32_t timeFrameIndex{};

        int32_t channelCount{};

        TCFDataSetRecordedPosition position{};
    };

    TCFBFDataSetMetaInfo::TCFBFDataSetMetaInfo() : d{ std::make_unique<Impl>() } {
    }

    TCFBFDataSetMetaInfo::TCFBFDataSetMetaInfo(const TCFBFDataSetMetaInfo& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    TCFBFDataSetMetaInfo::~TCFBFDataSetMetaInfo() = default;

    auto TCFBFDataSetMetaInfo::operator=(const TCFBFDataSetMetaInfo& other) -> TCFBFDataSetMetaInfo& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFBFDataSetMetaInfo::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->dataSizeX = sizeX;
        d->dataSizeY = sizeY;
    }

    auto TCFBFDataSetMetaInfo::GetSizeX() const -> const int32_t& {
        return d->dataSizeX;
    }

    auto TCFBFDataSetMetaInfo::GetSizeY() const -> const int32_t& {
        return d->dataSizeY;
    }

    auto TCFBFDataSetMetaInfo:: SetPixelWorldSize(const float& sizeX, const float& sizeY,
        const LengthUnit& unit) -> void {
        d->pixelWorldSizeX = sizeX;
        d->pixelWorldSizeY = sizeY;
        d->pixelWorldSizeUnit = unit;
    }

    auto TCFBFDataSetMetaInfo::GetPixelWorldSizeX(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeX, d->pixelWorldSizeUnit, unit);
    }

    auto TCFBFDataSetMetaInfo::GetPixelWorldSizeY(const LengthUnit& unit) const -> float {
        return ConvertUnit(d->pixelWorldSizeY, d->pixelWorldSizeUnit, unit);
    }

    auto TCFBFDataSetMetaInfo::SetRecordedTime(const QDateTime& time) -> void {
        d->recordedTime = time;
    }

    auto TCFBFDataSetMetaInfo::GetRecordedTime() const -> const QDateTime& {
        return d->recordedTime;
    }

    auto TCFBFDataSetMetaInfo::SetElapsedTime(const double& elapsedTime, const TimeUnit& unit) -> void {
        d->elapsedTime = elapsedTime;
        d->elapsedTimeUnit = unit;
    }

    auto TCFBFDataSetMetaInfo::GetElapsedTime(const TimeUnit& unit) const -> double {
        return ConvertUnit(d->elapsedTime, d->elapsedTimeUnit, unit);
    }

    auto TCFBFDataSetMetaInfo::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto TCFBFDataSetMetaInfo::GetTimeFrameIndex() const -> const int32_t& {
        return d->timeFrameIndex;
    }

    auto TCFBFDataSetMetaInfo::SetPosition(const TCFDataSetRecordedPosition& position) -> void {
        d->position = position;
    }

    auto TCFBFDataSetMetaInfo::GetPosition() const -> const TCFDataSetRecordedPosition& {
        return d->position;
    }
}
