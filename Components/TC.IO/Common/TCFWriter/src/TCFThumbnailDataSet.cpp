#include "TCFThumbnailDataSet.h"

namespace TC::IO::TCFWriter {
    class TCFThumbnailDataSet::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        std::shared_ptr<uint8_t[]> data{};
        MemoryOrder3D memoryOrder3D{ MemoryOrder3D::YXZ };

        int32_t sizeX{};
        int32_t sizeY{};

        bool colorFlag{ false };

        float voxelSizeX{};
        float voxelSizeY{};
        LengthUnit voxelSizeUnit{LengthUnit::Micrometer};

        int32_t timeFrameIndex{};
    };

    TCFThumbnailDataSet::TCFThumbnailDataSet() : d(new Impl()) {
    }

    TCFThumbnailDataSet::TCFThumbnailDataSet(const TCFThumbnailDataSet& other) : d(new Impl(*other.d)) {
    }

    TCFThumbnailDataSet::~TCFThumbnailDataSet() = default;

    auto TCFThumbnailDataSet::operator=(const TCFThumbnailDataSet& other) -> TCFThumbnailDataSet& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto TCFThumbnailDataSet::SetData(const std::shared_ptr<uint8_t[]>& data) -> void {
        d->data = data;
    }

    auto TCFThumbnailDataSet::GetData() const -> std::shared_ptr<uint8_t[]> {
        return d->data;
    }

    auto TCFThumbnailDataSet::SetDataMemoryOrder(const MemoryOrder3D& memoryOrder) -> void {
        d->memoryOrder3D = memoryOrder;
    }

    auto TCFThumbnailDataSet::GetDataMemoryOrder() const -> MemoryOrder3D {
        return d->memoryOrder3D;
    }

    auto TCFThumbnailDataSet::SetDataSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
    }

    auto TCFThumbnailDataSet::GetSizeX() const -> int32_t {
        return d->sizeX;
    }

    auto TCFThumbnailDataSet::GetSizeY() const -> int32_t {
        return d->sizeY;
    }

    auto TCFThumbnailDataSet::SetColorFlag(const bool& colorFlag) -> void {
        d->colorFlag = colorFlag;
    }

    auto TCFThumbnailDataSet::GetColorFlag() const -> bool {
        return d->colorFlag;
    }

    auto TCFThumbnailDataSet::SetVoxelSize(const float& voxelSizeX, const float& voxelSizeY,
        const LengthUnit& unit) -> void {
        d->voxelSizeX = voxelSizeX;
        d->voxelSizeY = voxelSizeY;
        d->voxelSizeUnit = unit;
    }

    auto TCFThumbnailDataSet::GetVoxelSizeX(const LengthUnit& unit) const -> float {
        return static_cast<float>(ConvertUnit(static_cast<double>(d->voxelSizeX), d->voxelSizeUnit, unit));
    }

    auto TCFThumbnailDataSet::GetVoxelSizeY(const LengthUnit& unit) const -> float {
        return static_cast<float>(ConvertUnit(static_cast<double>(d->voxelSizeY), d->voxelSizeUnit, unit));
    }

    auto TCFThumbnailDataSet::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto TCFThumbnailDataSet::GetTimeFrameIndex() const -> int32_t {
        return d->timeFrameIndex;
    }
}
