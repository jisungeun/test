#include <catch2/catch.hpp>

#include <QFile>

#include "H5Cpp.h"
#include "TCFSkeletonWriter.h"

namespace TCFSkeletonWriterTest {
    TEST_CASE("TCFSkeletonWriter : unit test") {
        SECTION("TCFSkeletonWriter()") {
            TCFSkeletonWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            TCFSkeletonWriter writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetConfig()") {
            TCFSkeletonWriter writer;
            writer.SetConfig({});
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "tcfSkeletonWriterTest.h5";
            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, true, true);

            TCFSkeletonWriter writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetConfig(config);

            CHECK(writer.Write() == true);
            CHECK(QFile::exists(targetFilePath));
            CHECK(QFile::remove(targetFilePath));
        }
    }
    TEST_CASE("TCFSkeletonWriter : practical test") {
        SECTION("Check TCF Groups are written") {
            SECTION("All Modality") {
                const QString targetFilePath = "tcfSkeletonWriterTest.h5";
                TCFSkeletonConfig config;
                config.SetDataIncludingFlag(true, true, true);

                TCFSkeletonWriter writer;
                writer.SetTargetFilePath(targetFilePath);
                writer.SetConfig(config);

                CHECK(writer.Write() == true);
                CHECK(QFile::exists(targetFilePath));

                H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);

                CHECK(file.nameExists("Data/3D") == true);
                CHECK(file.nameExists("Data/2DMIP") == true);
                CHECK(file.nameExists("Data/3DFL") == true);
                CHECK(file.nameExists("Data/2DFLMIP") == true);
                CHECK(file.nameExists("Data/BF") == true);
                CHECK(file.nameExists("Info") == true);

                file.close();

                CHECK(QFile::remove(targetFilePath));
            }
            SECTION("Only HT Modality") {
                const QString targetFilePath = "tcfSkeletonWriterTest.h5";
                TCFSkeletonConfig config;
                config.SetDataIncludingFlag(true, false, false);

                TCFSkeletonWriter writer;
                writer.SetTargetFilePath(targetFilePath);
                writer.SetConfig(config);

                CHECK(writer.Write() == true);
                CHECK(QFile::exists(targetFilePath));

                H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);

                CHECK(file.nameExists("Data/3D") == true);
                CHECK(file.nameExists("Data/2DMIP") == true);
                CHECK(file.nameExists("Data/3DFL") == false);
                CHECK(file.nameExists("Data/2DFLMIP") == false);
                CHECK(file.nameExists("Data/BF") == false);
                CHECK(file.nameExists("Info") == true);

                file.close();

                CHECK(QFile::remove(targetFilePath));
            }
            SECTION("Only FL Modality") {
                const QString targetFilePath = "tcfSkeletonWriterTest.h5";
                TCFSkeletonConfig config;
                config.SetDataIncludingFlag(false, true, false);

                TCFSkeletonWriter writer;
                writer.SetTargetFilePath(targetFilePath);
                writer.SetConfig(config);

                CHECK(writer.Write() == true);
                CHECK(QFile::exists(targetFilePath));

                H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);

                CHECK(file.nameExists("Data/3D") == false);
                CHECK(file.nameExists("Data/2DMIP") == false);
                CHECK(file.nameExists("Data/3DFL") == true);
                CHECK(file.nameExists("Data/2DFLMIP") == true);
                CHECK(file.nameExists("Data/BF") == false);
                CHECK(file.nameExists("Info") == true);

                file.close();

                CHECK(QFile::remove(targetFilePath));
            }
            SECTION("Only BF Modality") {
                const QString targetFilePath = "tcfSkeletonWriterTest.h5";
                TCFSkeletonConfig config;
                config.SetDataIncludingFlag(false, false, true);

                TCFSkeletonWriter writer;
                writer.SetTargetFilePath(targetFilePath);
                writer.SetConfig(config);

                CHECK(writer.Write() == true);
                CHECK(QFile::exists(targetFilePath));

                H5::H5File file(targetFilePath.toStdString(), H5F_ACC_RDONLY);

                CHECK(file.nameExists("Data/3D") == false);
                CHECK(file.nameExists("Data/2DMIP") == false);
                CHECK(file.nameExists("Data/3DFL") == false);
                CHECK(file.nameExists("Data/2DFLMIP") == false);
                CHECK(file.nameExists("Data/BF") == true);
                CHECK(file.nameExists("Info") == true);

                file.close();

                CHECK(QFile::remove(targetFilePath));
            }
        }
    }
}