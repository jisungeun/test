#include <catch2/catch.hpp>

#include "TCFThumbnailDataSet.h"

namespace TCFThumbnailDataSetTest {
    using namespace TC::IO::TCFWriter;

    TEST_CASE("TCFThumbnailDataSet : unit test") {
        SECTION("TCFThumbnailDataSet()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("TCFThumbnailDataSet(other)") {
            TCFThumbnailDataSet srcTCFThumbnailDataSet;
            srcTCFThumbnailDataSet.SetDataSize(1, 2);

            TCFThumbnailDataSet destTCFThumbnailDataSet(srcTCFThumbnailDataSet);
            CHECK(destTCFThumbnailDataSet.GetSizeX() == 1);
            CHECK(destTCFThumbnailDataSet.GetSizeY() == 2);
        }
        SECTION("operator=()") {
            TCFThumbnailDataSet srcTCFThumbnailDataSet;
            srcTCFThumbnailDataSet.SetDataSize(1, 2);

            TCFThumbnailDataSet destTCFThumbnailDataSet;
            destTCFThumbnailDataSet = srcTCFThumbnailDataSet;
            CHECK(destTCFThumbnailDataSet.GetSizeX() == 1);
            CHECK(destTCFThumbnailDataSet.GetSizeY() == 2);
        }
        SECTION("SetData()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetData({});
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetData()") {
            std::shared_ptr<uint8_t[]> data{ new uint8_t[1] };
            data.get()[0] = 2;

            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetData(data);
            CHECK(tcfThumbnailDataSet.GetData().get()[0] == 2);
        }
        SECTION("SetDataMemoryOrder()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetDataMemoryOrder(MemoryOrder3D::XYZ);
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetDataMemoryOrder()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetDataMemoryOrder(MemoryOrder3D::XYZ);
            CHECK(tcfThumbnailDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        }
        SECTION("SetDataSize()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetDataSize(1, 2);
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetSizeX()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetDataSize(1, 2);
            CHECK(tcfThumbnailDataSet.GetSizeX() == 1);
        }
        SECTION("GetSizeY()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetDataSize(1, 2);
            CHECK(tcfThumbnailDataSet.GetSizeY() == 2);
        }
        SECTION("SetColorFlag()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetColorFlag(true);
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetColorFlag()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetColorFlag(true);
            CHECK(tcfThumbnailDataSet.GetColorFlag() == true);
        }
        SECTION("SetVoxelSize()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetVoxelSize(1, 2, LengthUnit::Millimenter);
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetVoxelSizeX()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetVoxelSize(1, 2, LengthUnit::Millimenter);
            CHECK(tcfThumbnailDataSet.GetVoxelSizeX(LengthUnit::Micrometer) == 1000);
        }
        SECTION("GetVoxelSizeY()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetVoxelSize(1, 2, LengthUnit::Millimenter);
            CHECK(tcfThumbnailDataSet.GetVoxelSizeY(LengthUnit::Micrometer) == 2000);
        }
        SECTION("SetTimeFrameIndex()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetTimeFrameIndex(1);
            CHECK(&tcfThumbnailDataSet != nullptr);
        }
        SECTION("GetTimeFrameIndex()") {
            TCFThumbnailDataSet tcfThumbnailDataSet;
            tcfThumbnailDataSet.SetTimeFrameIndex(1);
            CHECK(tcfThumbnailDataSet.GetTimeFrameIndex() == 1);
        }
    }
    TEST_CASE("TCFThumbnailDataSet : practical test") {
        //TODO Implement practical test
    }
}