#include <catch2/catch.hpp>
#include <QFile>
#include "TCFWriterUtility.h"
#include "CompareArray.h"

namespace TCFWriterUtilityTest {
    TEST_CASE("TCFWriterUtility") {
        SECTION("TimeFrameIndexName()") {
            CHECK(TimeFrameIndexName(0) == "000000");
        }

        SECTION("MakeDataSpace(sizeX, sizeY, sizeZ)") {
            constexpr auto sizeX = 10;
            constexpr auto sizeY = 11;
            constexpr auto sizeZ = 12;

            auto dataSpace = MakeDataSpace(sizeX, sizeY, sizeZ);

            hsize_t resultDims[3] = { 0,0,0 };

            CHECK(dataSpace.getSimpleExtentNdims() == 3);
            CHECK(dataSpace.getSimpleExtentNpoints() == sizeX * sizeY * sizeZ);

            dataSpace.getSimpleExtentDims(resultDims);
            CHECK(resultDims[0] == sizeZ);
            CHECK(resultDims[1] == sizeY);
            CHECK(resultDims[2] == sizeX);
        }

        SECTION("MakeDataSpace(sizeX, sizeY)") {
            constexpr auto sizeX = 10;
            constexpr auto sizeY = 11;

            auto dataSpace = MakeDataSpace(sizeX, sizeY);

            hsize_t resultDims[2] = { 0,0 };

            CHECK(dataSpace.getSimpleExtentNdims() == 2);
            CHECK(dataSpace.getSimpleExtentNpoints() == sizeX * sizeY);

            dataSpace.getSimpleExtentDims(resultDims);
            CHECK(resultDims[0] == sizeY);
            CHECK(resultDims[1] == sizeX);
        }

        SECTION("MakeDataSpace(length)") {
            constexpr auto length = 3;

            auto dataSpace = MakeDataSpace(length);

            hsize_t resultDims[1] = { 0 };

            CHECK(dataSpace.getSimpleExtentNdims() == 1);
            CHECK(dataSpace.getSimpleExtentNpoints() == length);

            dataSpace.getSimpleExtentDims(resultDims);
            CHECK(resultDims[0] == length);
        }

        SECTION("WriteAttributeString()") {
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeString(file, "stringAttribute", "stringContents");

            CHECK(file.attrExists("stringAttribute") == true);

            auto resultAttribute = file.openAttribute("stringAttribute");
            auto resultAttributeDataType = resultAttribute.getDataType();

            CHECK(resultAttributeDataType.getClass() == H5T_STRING);
            
            std::string resultString{};
            resultAttribute.read(resultAttribute.getDataType(), resultString);

            resultAttribute.close();
            file.close();

            CHECK(resultString == "stringContents");
            CHECK(QFile::remove("test.h5"));
        }
        SECTION("WriteAttributeDouble()") {
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeDouble(file, "doubleAttribute", 10);

            CHECK(file.attrExists("doubleAttribute") == true);

            auto resultAttribute = file.openAttribute("doubleAttribute");
            auto resultAttributeDataType = resultAttribute.getDataType();

            CHECK(resultAttributeDataType.getClass() == H5T_FLOAT);

            double resultValue{};
            resultAttribute.read(resultAttribute.getDataType(), &resultValue);

            resultAttribute.close();
            file.close();

            CHECK(resultValue == 10);
            CHECK(QFile::remove("test.h5"));
        }
        SECTION("WriteAttributeInt64()") {
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeInt64(file, "int64Attribute", 10);

            CHECK(file.attrExists("int64Attribute") == true);

            auto resultAttribute = file.openAttribute("int64Attribute");
            auto resultAttributeDataType = resultAttribute.getDataType();

            CHECK(resultAttributeDataType.getClass() == H5T_INTEGER);

            int64_t resultValue{};
            resultAttribute.read(resultAttribute.getDataType(), &resultValue);

            resultAttribute.close();
            file.close();

            CHECK(resultValue == 10);
            CHECK(QFile::remove("test.h5"));
        }
        SECTION("ReadAttributeDouble()"){
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeDouble(file, "doubleAttribute", 10);
            CHECK(ReadAttributeDouble(file, "doubleAttribute") == 10);
            file.close();
        }
        SECTION("ReadAttributeInt64()"){
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeInt64(file, "int64Attribute", 10);
            CHECK(ReadAttributeInt64(file, "int64Attribute") == 10);
            file.close();
        }
        SECTION("ReadAttributeString()") {
            H5::H5File file("test.h5", H5F_ACC_TRUNC);
            WriteAttributeString(file, "stringAttribute", "stringContents");
            CHECK(ReadAttributeString(file, "stringAttribute") == "stringContents");
            file.close();
        }

        SECTION("ToTimeString()") {
            QDate date(2022, 3, 19);
            QTime time(19, 24, 10, 100);

            QDateTime dateTime(date, time);

            CHECK(ToTimeString(dateTime) == "2022-03-19 19:24:10.100");
        }
        SECTION("ConvertMemoryOrder(uint16_t, 3D)") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;
            constexpr auto sizeZ = 4;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const uint16_t rawData[numberOfElements] = 
            { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 };

            std::shared_ptr<uint16_t[]> data{ new uint16_t[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());

            SECTION("XYZ -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::XYZ);
                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> YXZ") {
                auto convertedData = 
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::YXZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,3,5,2,4,6,7,9,11,8,10,12,13,15,17,14,16,18,19,21,23,20,22,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::ZXY);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::ZYX);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,7,13,19,3,9,15,21,5,11,17,23,2,8,14,20,4,10,16,22,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::XYZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,4,2,5,3,6,7,10,8,11,9,12,13,16,14,17,15,18,19,22,20,23,21,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::YXZ);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::ZXY);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,7,13,19,4,10,16,22,2,8,14,20,5,11,17,23,3,9,15,21,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::ZYX);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }

            SECTION("ZXY -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::XYZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,5,9,13,17,21,2,6,10,14,18,22,3,7,11,15,19,23,4,8,12,16,20,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::YXZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,9,17,5,13,21,2,10,18,6,14,22,3,11,19,7,15,23,4,12,20,8,16,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::ZXY);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::ZYX);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,2,3,4,9,10,11,12,17,18,19,20,5,6,7,8,13,14,15,16,21,22,23,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }

            SECTION("ZYX -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::XYZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,13,5,17,9,21,2,14,6,18,10,22,3,15,7,19,11,23,4,16,8,20,12,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::YXZ);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,5,9,13,17,21,2,6,10,14,18,22,3,7,11,15,19,23,4,8,12,16,20,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::ZXY);

                constexpr uint16_t answerData[numberOfElements] =
                { 1,2,3,4,13,14,15,16,5,6,7,8,17,18,19,20,9,10,11,12,21,22,23,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::ZYX);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
        }
        SECTION("ConvertMemoryOrder(uint16_t, 2D)") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;
            constexpr auto numberOfElements = sizeX * sizeY;

            const uint16_t rawData[numberOfElements] = { 1,2,3,4,5,6 };

            std::shared_ptr<uint16_t[]> data{ new uint16_t[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());
            SECTION("XY -> XY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, MemoryOrder2D::XY, MemoryOrder2D::XY);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XY -> YX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, MemoryOrder2D::XY, MemoryOrder2D::YX);

                constexpr uint16_t answerData[numberOfElements] = { 1,3,5,2,4,6 };
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YX -> XY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, MemoryOrder2D::YX, MemoryOrder2D::XY);

                constexpr uint16_t answerData[numberOfElements] = { 1,4,2,5,3,6 };
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YX -> YX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, MemoryOrder2D::YX, MemoryOrder2D::YX);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
        }
        SECTION("ConvertMemoryOrder(uint8_t, 3D)") {
            constexpr auto sizeX = 2;
            constexpr auto sizeY = 3;
            constexpr auto sizeZ = 4;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const uint8_t rawData[numberOfElements] =
            { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 };

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());

            SECTION("XYZ -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::XYZ);
                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::YXZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,3,5,2,4,6,7,9,11,8,10,12,13,15,17,14,16,18,19,21,23,20,22,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::ZXY);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("XYZ -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ, MemoryOrder3D::ZYX);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,7,13,19,3,9,15,21,5,11,17,23,2,8,14,20,4,10,16,22,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::XYZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,4,2,5,3,6,7,10,8,11,9,12,13,16,14,17,15,18,19,22,20,23,21,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::YXZ);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::ZXY);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,7,13,19,4,10,16,22,2,8,14,20,5,11,17,23,3,9,15,21,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("YXZ -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ, MemoryOrder3D::ZYX);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23,6,12,18,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }

            SECTION("ZXY -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::XYZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,5,9,13,17,21,2,6,10,14,18,22,3,7,11,15,19,23,4,8,12,16,20,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::YXZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,9,17,5,13,21,2,10,18,6,14,22,3,11,19,7,15,23,4,12,20,8,16,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::ZXY);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZXY -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY, MemoryOrder3D::ZYX);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,2,3,4,9,10,11,12,17,18,19,20,5,6,7,8,13,14,15,16,21,22,23,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }

            SECTION("ZYX -> XYZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::XYZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,13,5,17,9,21,2,14,6,18,10,22,3,15,7,19,11,23,4,16,8,20,12,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> YXZ") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::YXZ);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,5,9,13,17,21,2,6,10,14,18,22,3,7,11,15,19,23,4,8,12,16,20,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> ZXY") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::ZXY);

                constexpr uint8_t answerData[numberOfElements] =
                { 1,2,3,4,13,14,15,16,5,6,7,8,17,18,19,20,9,10,11,12,21,22,23,24 };

                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
            SECTION("ZYX -> ZYX") {
                auto convertedData =
                    ConvertMemoryOrder(data, sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX, MemoryOrder3D::ZYX);

                const auto& answerData = rawData;
                CHECK(CompareArray(convertedData.get(), answerData, numberOfElements) == true);
            }
        }

        SECTION("GenerateUniqueID()") {
            const QString deviceName = "deviceName";
            const auto dataRecordedDateTime = QDateTime::currentDateTime();
            const auto dataWritingDateTime = QDateTime::currentDateTime();

            const auto uniqueID = GenerateUniqueID(deviceName, dataRecordedDateTime, dataWritingDateTime);
            const QString uiqueIDForamt = "deviceName-YYYY:MM:dd:HH:mm:ss:zzz-YYYY:MM:dd:HH:mm:ss:zzz-01234567890123456789012345678901";

            CHECK(uniqueID.length() == uiqueIDForamt.length());

            constexpr auto dateTimeStringLength = 23;
            CHECK(uniqueID.left(10).toStdString() == deviceName.toStdString());
            CHECK(uniqueID.mid(11, dateTimeStringLength) == dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz"));
            CHECK(uniqueID.mid(35, dateTimeStringLength) == dataWritingDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz"));
        }

        SECTION("GenerateDataID()") {
            const QString deviceName = "deviceName";
            const auto dataRecordedDateTime = QDateTime::currentDateTime();

            const auto dataID = GenerateDataID(deviceName, dataRecordedDateTime);
            const QString dataIDForamt = "deviceName-YYYY:MM:dd:HH:mm:ss:zzz";

            CHECK(dataID.length() == dataIDForamt.length());

            constexpr auto dateTimeStringLength = 23;
            CHECK(dataID.left(10).toStdString() == deviceName.toStdString());
            CHECK(dataID.mid(11, dateTimeStringLength) == dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz"));
        }

    }
}