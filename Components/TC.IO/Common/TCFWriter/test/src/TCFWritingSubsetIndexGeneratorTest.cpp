//#include <iostream>
#include <catch2/catch.hpp>

#include "TCFWritingSubsetIndexGenerator.h"

namespace TCFWritingSubsetIndexGeneratorTest {
    TEST_CASE("TCFWritingSubsetIndexGenerator : unit test") {
        SECTION("TCFWritingSubsetIndexGenerator()") {
            TCFWritingSubsetIndexGenerator tcfWritingSubsetIndexGenerator;
            CHECK(&tcfWritingSubsetIndexGenerator != nullptr);
        }
        SECTION("SetWholeSize()") {
            TCFWritingSubsetIndexGenerator tcfWritingSubsetIndexGenerator;
            tcfWritingSubsetIndexGenerator.SetWholeSize(1, 2, 3);
            CHECK(&tcfWritingSubsetIndexGenerator != nullptr);
        }
        SECTION("SetSubsetOffset()") {
            TCFWritingSubsetIndexGenerator tcfWritingSubsetIndexGenerator;
            tcfWritingSubsetIndexGenerator.SetSubsetOffset(1, 2, 3);
            CHECK(&tcfWritingSubsetIndexGenerator != nullptr);
        }
        SECTION("SetSubsetDataLimitationSize()") {
            TCFWritingSubsetIndexGenerator tcfWritingSubsetIndexGenerator;
            tcfWritingSubsetIndexGenerator.SetSubsetDataLimitationSize(1, DataSizeUnit::Byte);
            CHECK(&tcfWritingSubsetIndexGenerator != nullptr);
        }
        SECTION("SetElementSize()") {
            TCFWritingSubsetIndexGenerator tcfWritingSubsetIndexGenerator;
            tcfWritingSubsetIndexGenerator.SetElementSize(1, DataSizeUnit::Byte);
            CHECK(&tcfWritingSubsetIndexGenerator != nullptr);
        }
        SECTION("Generate()") {
            constexpr int64_t wholeSizeX = 100;
            constexpr int64_t wholeSizeY = 100;
            constexpr int64_t wholeSizeZ = 60;

            constexpr int64_t subsetOffsetX = 1;
            constexpr int64_t subsetOffsetY = 2;
            constexpr int64_t subsetOffsetZ = 0;

            constexpr double subsetDataLimitationSize = 10;

            TCFWritingSubsetIndexGenerator indexGenerator;
            indexGenerator.SetWholeSize(wholeSizeX, wholeSizeY, wholeSizeZ);
            indexGenerator.SetSubsetOffset(subsetOffsetX, subsetOffsetY, subsetOffsetZ);
            indexGenerator.SetSubsetDataLimitationSize(subsetDataLimitationSize, DataSizeUnit::Gigabyte);
            indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

            const auto generationResult = indexGenerator.Generate();
            CHECK(generationResult == true);
        }
        SECTION("GetSubsetDataCount()") {
            constexpr int64_t wholeSizeX = 10000;
            constexpr int64_t wholeSizeY = 10000;
            constexpr int64_t wholeSizeZ = 60;

            constexpr int64_t subsetOffsetX = 10;
            constexpr int64_t subsetOffsetY = 20;
            constexpr int64_t subsetOffsetZ = 0;

            constexpr double subsetDataLimitationSize = 10;

            TCFWritingSubsetIndexGenerator indexGenerator;
            indexGenerator.SetWholeSize(wholeSizeX, wholeSizeY, wholeSizeZ);
            indexGenerator.SetSubsetOffset(subsetOffsetX, subsetOffsetY, subsetOffsetZ);
            indexGenerator.SetSubsetDataLimitationSize(subsetDataLimitationSize, DataSizeUnit::Gigabyte);
            indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

            indexGenerator.Generate();

            CHECK(indexGenerator.GetSubsetDataCount() == 4);
        }

        SECTION("GetWritingDataIndex") {
            constexpr int64_t wholeSizeX = 10000;
            constexpr int64_t wholeSizeY = 10000;
            constexpr int64_t wholeSizeZ = 60;

            constexpr int64_t subsetOffsetX = 10;
            constexpr int64_t subsetOffsetY = 20;
            constexpr int64_t subsetOffsetZ = 0;

            constexpr double subsetDataLimitationSize = 10;

            TCFWritingSubsetIndexGenerator indexGenerator;
            indexGenerator.SetWholeSize(wholeSizeX, wholeSizeY, wholeSizeZ);
            indexGenerator.SetSubsetOffset(subsetOffsetX, subsetOffsetY, subsetOffsetZ);
            indexGenerator.SetSubsetDataLimitationSize(subsetDataLimitationSize, DataSizeUnit::Gigabyte);
            indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

            indexGenerator.Generate();

            SECTION("GetWritingDataIndexStartX()") {
                CHECK(indexGenerator.GetWritingDataIndexStartX(0) == 10);
                CHECK(indexGenerator.GetWritingDataIndexStartX(1) == 9138);
                CHECK(indexGenerator.GetWritingDataIndexStartX(2) == 10);
                CHECK(indexGenerator.GetWritingDataIndexStartX(3) == 9138);
            }
            SECTION("GetWritingDataIndexEndX()") {
                CHECK(indexGenerator.GetWritingDataIndexEndX(0) == 9137);
                CHECK(indexGenerator.GetWritingDataIndexEndX(1) == 10009);
                CHECK(indexGenerator.GetWritingDataIndexEndX(2) == 9137);
                CHECK(indexGenerator.GetWritingDataIndexEndX(3) == 10009);
            }
            SECTION("GetWritingDataIndexStartY()") {
                CHECK(indexGenerator.GetWritingDataIndexStartY(0) == 20);
                CHECK(indexGenerator.GetWritingDataIndexStartY(1) == 20);
                CHECK(indexGenerator.GetWritingDataIndexStartY(2) == 9148);
                CHECK(indexGenerator.GetWritingDataIndexStartY(3) == 9148);
            }
            SECTION("GetWritingDataIndexEndY()") {
                CHECK(indexGenerator.GetWritingDataIndexEndY(0) == 9147);
                CHECK(indexGenerator.GetWritingDataIndexEndY(1) == 9147);
                CHECK(indexGenerator.GetWritingDataIndexEndY(2) == 10019);
                CHECK(indexGenerator.GetWritingDataIndexEndY(3) == 10019);
            }
            SECTION("GetWritingDataIndexStartZ()") {
                CHECK(indexGenerator.GetWritingDataIndexStartZ(0) == 0);
                CHECK(indexGenerator.GetWritingDataIndexStartZ(1) == 0);
                CHECK(indexGenerator.GetWritingDataIndexStartZ(2) == 0);
                CHECK(indexGenerator.GetWritingDataIndexStartZ(3) == 0);
            }
            SECTION("GetWritingDataIndexEndZ()") {
                CHECK(indexGenerator.GetWritingDataIndexEndZ(0) == 59);
                CHECK(indexGenerator.GetWritingDataIndexEndZ(1) == 59);
                CHECK(indexGenerator.GetWritingDataIndexEndZ(2) == 59);
                CHECK(indexGenerator.GetWritingDataIndexEndZ(3) == 59);
            }
        }

        SECTION("GetSubsetIndex") {
            constexpr int64_t wholeSizeX = 10000;
            constexpr int64_t wholeSizeY = 10000;
            constexpr int64_t wholeSizeZ = 60;

            constexpr int64_t subsetOffsetX = 10;
            constexpr int64_t subsetOffsetY = 20;
            constexpr int64_t subsetOffsetZ = 0;

            constexpr double subsetDataLimitationSize = 10;

            TCFWritingSubsetIndexGenerator indexGenerator;
            indexGenerator.SetWholeSize(wholeSizeX, wholeSizeY, wholeSizeZ);
            indexGenerator.SetSubsetOffset(subsetOffsetX, subsetOffsetY, subsetOffsetZ);
            indexGenerator.SetSubsetDataLimitationSize(subsetDataLimitationSize, DataSizeUnit::Gigabyte);
            indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

            indexGenerator.Generate();

            SECTION("GetSubsetIndexStartX()") {
                CHECK(indexGenerator.GetSubsetIndexStartX(0) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartX(1) == 9128);
                CHECK(indexGenerator.GetSubsetIndexStartX(2) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartX(3) == 9128);
            }
            SECTION("GetSubsetIndexEndX()") {
                CHECK(indexGenerator.GetSubsetIndexEndX(0) == 9127);
                CHECK(indexGenerator.GetSubsetIndexEndX(1) == 9999);
                CHECK(indexGenerator.GetSubsetIndexEndX(2) == 9127);
                CHECK(indexGenerator.GetSubsetIndexEndX(3) == 9999);
            }
            SECTION("GetSubsetIndexStartY()") {
                CHECK(indexGenerator.GetSubsetIndexStartY(0) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartY(1) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartY(2) == 9128);
                CHECK(indexGenerator.GetSubsetIndexStartY(3) == 9128);
            }
            SECTION("GetSubsetIndexEndY()") {
                CHECK(indexGenerator.GetSubsetIndexEndY(0) == 9127);
                CHECK(indexGenerator.GetSubsetIndexEndY(1) == 9127);
                CHECK(indexGenerator.GetSubsetIndexEndY(2) == 9999);
                CHECK(indexGenerator.GetSubsetIndexEndY(3) == 9999);
            }
            SECTION("GetSubsetIndexStartZ()") {
                CHECK(indexGenerator.GetSubsetIndexStartZ(0) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartZ(1) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartZ(2) == 0);
                CHECK(indexGenerator.GetSubsetIndexStartZ(3) == 0);
            }
            SECTION("GetSubsetIndexEndZ()") {
                CHECK(indexGenerator.GetSubsetIndexEndZ(0) == 59);
                CHECK(indexGenerator.GetSubsetIndexEndZ(1) == 59);
                CHECK(indexGenerator.GetSubsetIndexEndZ(2) == 59);
                CHECK(indexGenerator.GetSubsetIndexEndZ(3) == 59);
            }
        }

    }
    TEST_CASE("TCFWritingSubsetIndexGenerator : practical test") {
        constexpr int64_t wholeSizeX = 3000;
        constexpr int64_t wholeSizeY = 3000;
        constexpr int64_t wholeSizeZ = 60;

        constexpr int64_t subsetOffsetX = 10;
        constexpr int64_t subsetOffsetY = 20;
        constexpr int64_t subsetOffsetZ = 0;

        constexpr double subsetDataLimitationSize = 200;

        TCFWritingSubsetIndexGenerator indexGenerator;
        indexGenerator.SetWholeSize(wholeSizeX, wholeSizeY, wholeSizeZ);
        indexGenerator.SetSubsetOffset(subsetOffsetX, subsetOffsetY, subsetOffsetZ);
        indexGenerator.SetSubsetDataLimitationSize(subsetDataLimitationSize, DataSizeUnit::Megabyte);
        indexGenerator.SetElementSize(2, DataSizeUnit::Byte);

        indexGenerator.Generate();

        const auto subsetDataCount = indexGenerator.GetSubsetDataCount();
        CHECK(subsetDataCount == 9);

        for (auto subsetDataIndex = 0; subsetDataIndex < subsetDataCount; ++subsetDataIndex) {
            const auto writingDataIndexX0 = indexGenerator.GetWritingDataIndexStartX(subsetDataIndex);
            const auto writingDataIndexX1 = indexGenerator.GetWritingDataIndexEndX(subsetDataIndex);
            const auto writingDataIndexY0 = indexGenerator.GetWritingDataIndexStartY(subsetDataIndex);
            const auto writingDataIndexY1 = indexGenerator.GetWritingDataIndexEndY(subsetDataIndex);
            const auto writingDataIndexZ0 = indexGenerator.GetWritingDataIndexStartZ(subsetDataIndex);
            const auto writingDataIndexZ1 = indexGenerator.GetWritingDataIndexEndZ(subsetDataIndex);

            const auto subsetIndexX0 = indexGenerator.GetSubsetIndexStartX(subsetDataIndex);
            const auto subsetIndexX1 = indexGenerator.GetSubsetIndexEndX(subsetDataIndex);
            const auto subsetIndexY0 = indexGenerator.GetSubsetIndexStartY(subsetDataIndex);
            const auto subsetIndexY1 = indexGenerator.GetSubsetIndexEndY(subsetDataIndex);
            const auto subsetIndexZ0 = indexGenerator.GetSubsetIndexStartZ(subsetDataIndex);
            const auto subsetIndexZ1 = indexGenerator.GetSubsetIndexEndZ(subsetDataIndex);

            //std::cout << "0 : writing[" << writingDataIndexX0 << "~" << writingDataIndexX1 << ", " << writingDataIndexY0 << "~" << writingDataIndexY1 << ", " << writingDataIndexZ0 << "~" << writingDataIndexZ1 << "]" << std::endl;
            //std::cout << "0 : subset[" << subsetIndexX0 << "~" << subsetIndexX1 << ", " << subsetIndexY0 << "~" << subsetIndexY1 << ", " << subsetIndexZ0 << "~" << subsetIndexZ1 << "]" << std::endl;
            //std::cout << std::endl;
        }
    }
}