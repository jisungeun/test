#include <catch2/catch.hpp>

#include "TCFTileInfo.h"

namespace TCFTileInfoTest {
    TEST_CASE("TCFTileInfo : unit test") {
        SECTION("TCFTileInfo()") {
            TCFTileInfo tcfTileInfo;
            CHECK(&tcfTileInfo != nullptr);
        }
        SECTION("TCFTileInfo(other)") {
            TCFTileInfo srcTCFTileInfo;
            srcTCFTileInfo.SetTileNumber(1, 2);

            TCFTileInfo destTCFTileInfo(srcTCFTileInfo);
            CHECK(destTCFTileInfo.GetTileNumberH() == 1);
            CHECK(destTCFTileInfo.GetTileNumberV() == 2);
        }
        SECTION("operator=()") {
            TCFTileInfo srcTCFTileInfo;
            srcTCFTileInfo.SetTileNumber(1, 2);

            TCFTileInfo destTCFTileInfo;
            destTCFTileInfo = srcTCFTileInfo;
            CHECK(destTCFTileInfo.GetTileNumberH() == 1);
            CHECK(destTCFTileInfo.GetTileNumberV() == 2);
        }
        SECTION("SetOverlapLength()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetOverlapLength(1, 2, LengthUnit::Millimenter);
            CHECK(&tcfTileInfo != nullptr);
        }
        SECTION("GetOverlapLengthH()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetOverlapLength(1, 2, LengthUnit::Millimenter);
            CHECK(tcfTileInfo.GetOverlapLengthH(LengthUnit::Micrometer) == 1000);

        }
        SECTION("GetOverlapLengthV()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetOverlapLength(1, 2, LengthUnit::Millimenter);
            CHECK(tcfTileInfo.GetOverlapLengthV(LengthUnit::Micrometer) == 2000);
        }
        SECTION("SetTCFDataSetRecordedPosition()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTCFDataSetRecordedPosition({});
            CHECK(&tcfTileInfo != nullptr);
        }
        SECTION("GetTCFDataSetRecordedPosition()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Micrometer);

            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTCFDataSetRecordedPosition(position);
            CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionX(LengthUnit::Micrometer) == 1);
            CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionY(LengthUnit::Micrometer) == 2);
            CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionZ(LengthUnit::Micrometer) == 3);
            CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionC(LengthUnit::Micrometer) == 4);
        }
        SECTION("SetTileNumber()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileNumber(1, 2);
            CHECK(&tcfTileInfo != nullptr);
        }
        SECTION("GetTileNumberH()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileNumber(1, 2);
            CHECK(tcfTileInfo.GetTileNumberH() == 1);
        }
        SECTION("GetTileNumberV()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileNumber(1, 2);
            CHECK(tcfTileInfo.GetTileNumberV() == 2);
        }
        SECTION("SetTileIndex()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileIndex(1,2);
            CHECK(&tcfTileInfo != nullptr);
        }
        SECTION("GetTileIndexColumn()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileIndex(1, 2);
            CHECK(tcfTileInfo.GetTileIndexColumn() == 1);
        }
        SECTION("GetTileIndexRow()") {
            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetTileIndex(1, 2);
            CHECK(tcfTileInfo.GetTileIndexRow() == 2);
        }
    }
    TEST_CASE("TCFTileInfo : practical test") {
        constexpr double overlapLengthH = 15;
        constexpr double overlapLengthV = 15;
        constexpr auto overlapLengthUnit = LengthUnit::Micrometer;

        constexpr double positionX = 1.2;
        constexpr double positionY = 2.1;
        constexpr double positionZ = 3.1;
        constexpr double positionC = 0.2;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, LengthUnit::Millimenter);

        constexpr int32_t tileNumberH = 3;
        constexpr int32_t tileNumberV = 3;

        constexpr int32_t tileIndexColumn = 1;
        constexpr int32_t tileIndexRow = 2;

        TCFTileInfo tcfTileInfo;
        tcfTileInfo.SetOverlapLength(overlapLengthH, overlapLengthV, overlapLengthUnit);
        tcfTileInfo.SetTCFDataSetRecordedPosition(position);
        tcfTileInfo.SetTileNumber(tileNumberH, tileNumberV);
        tcfTileInfo.SetTileIndex(tileIndexColumn, tileIndexRow);

        CHECK(tcfTileInfo.GetOverlapLengthH(LengthUnit::Micrometer) == 15);
        CHECK(tcfTileInfo.GetOverlapLengthV(LengthUnit::Micrometer) == 15);
        CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionX(LengthUnit::Millimenter) == positionX);
        CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionY(LengthUnit::Millimenter) == positionY);
        CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionZ(LengthUnit::Millimenter) == positionZ);
        CHECK(tcfTileInfo.GetTCFDataSetRecordedPosition().GetPositionC(LengthUnit::Millimenter) == positionC);
        CHECK(tcfTileInfo.GetTileNumberH() == tileNumberH);
        CHECK(tcfTileInfo.GetTileNumberV() == tileNumberV);
        CHECK(tcfTileInfo.GetTileIndexColumn() == tileIndexColumn);
        CHECK(tcfTileInfo.GetTileIndexRow() == tileIndexRow);
    }
}