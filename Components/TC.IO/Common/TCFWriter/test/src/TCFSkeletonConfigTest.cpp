#include <catch2/catch.hpp>

#include "TCFSkeletonConfig.h"

namespace TCFSkeletonConfigTest {
    TEST_CASE("TCFSkeletonConfig : unit test") {
        SECTION("TCFSkeletonConfg()") {
            TCFSkeletonConfig config;
            CHECK(&config != nullptr);
        }
        SECTION("TCFSkeletonConfg(other)") {
            TCFSkeletonConfig srcConfig;
            srcConfig.SetDataIncludingFlag(true, true, false);

            TCFSkeletonConfig destConfig(srcConfig);
            CHECK(destConfig.GetHTIncludingFlag() == true);
            CHECK(destConfig.GetFLIncludingFlag() == true);
            CHECK(destConfig.GetBFIncludingFlag() == false);
        }
        SECTION("operator=()") {
            TCFSkeletonConfig srcConfig;
            srcConfig.SetDataIncludingFlag(true, true, false);

            TCFSkeletonConfig destConfig;
            destConfig = srcConfig;
            CHECK(destConfig.GetHTIncludingFlag() == true);
            CHECK(destConfig.GetFLIncludingFlag() == true);
            CHECK(destConfig.GetBFIncludingFlag() == false);
        }
        SECTION("SetDataIncludingFlag()") {
            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, true, true);
            CHECK(&config != nullptr);
        }
        SECTION("GetHTIncludingFlag()"){
            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, true, true);
            CHECK(config.GetHTIncludingFlag() == true);
        }
        SECTION("GetFLIncludingFlag()"){
            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, true, true);
            CHECK(config.GetFLIncludingFlag() == true);
        }
        SECTION("GetBFIncludingFlag()"){
            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, true, true);
            CHECK(config.GetBFIncludingFlag() == true);
        }
    }
    TEST_CASE("TCFSkeletonConfig : practical test") {
        TCFSkeletonConfig config;
        config.SetDataIncludingFlag(true, true, true);
        CHECK(config.GetHTIncludingFlag() == true);
        CHECK(config.GetFLIncludingFlag() == true);
        CHECK(config.GetBFIncludingFlag() == true);
    }
}