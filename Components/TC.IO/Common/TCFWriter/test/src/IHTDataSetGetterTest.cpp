#include <catch2/catch.hpp>

#include "IHTDataSetGetter.h"

namespace IHTDataSetGetterTest {
    class HTDataSetGetterForTest final : public IHTDataSetGetter {
    public:
        HTDataSetGetterForTest() = default;
        ~HTDataSetGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            this->getDataTriggered = true;
            return nullptr;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
            const int32_t& z1) const -> std::shared_ptr<float[]> override {
            this->getDataXYZTriggered = true;
            return nullptr;
        }
        auto GetDataMemoryOrder() const->MemoryOrder3D override {
            this->getDataMemoryOrderTriggered = true;
            return MemoryOrder3D::XYZ;
        }
        auto GetMIPData() const -> std::shared_ptr<float[]> override {
            this->getMIPDataTriggered = true;
            return nullptr;
        }
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
            const -> std::shared_ptr<float[]> override {
            this->getMIPDataXYZTriggered = true;
            return nullptr;
        }
        auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override {
            this->getMIPDataMemoryOrderTriggered = true;
            return MemoryOrder2D::XY;
        }

        mutable bool getDataTriggered{ false };
        mutable bool getDataXYZTriggered{ false };
        mutable bool getDataMemoryOrderTriggered{ false };

        mutable bool getMIPDataTriggered{ false };
        mutable bool getMIPDataXYZTriggered{ false };
        mutable bool getMIPDataMemoryOrderTriggered{ false };
    };

    TEST_CASE("IHTDataSetGetter : unit test") {
        SECTION("GetData()") {
            HTDataSetGetterForTest htDataSetGetterForTest;

            CHECK(htDataSetGetterForTest.getDataTriggered == false);
            htDataSetGetterForTest.GetData();
            CHECK(htDataSetGetterForTest.getDataTriggered == true);
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1)") {
            HTDataSetGetterForTest htDataSetGetterForTest;
            CHECK(htDataSetGetterForTest.getDataXYZTriggered == false);
            htDataSetGetterForTest.GetData(0, 1, 2, 3, 4, 5);
            CHECK(htDataSetGetterForTest.getDataXYZTriggered == true);
        }
        SECTION("GetDataMemoryOrder()") {
            HTDataSetGetterForTest htDataSetGetterForTest;
            CHECK(htDataSetGetterForTest.getDataMemoryOrderTriggered == false);
            htDataSetGetterForTest.GetDataMemoryOrder();
            CHECK(htDataSetGetterForTest.getDataMemoryOrderTriggered == true);
        }
        SECTION("GetMIPData()") {
            HTDataSetGetterForTest htDataSetGetterForTest;
            CHECK(htDataSetGetterForTest.getMIPDataTriggered == false);
            htDataSetGetterForTest.GetMIPData();
            CHECK(htDataSetGetterForTest.getMIPDataTriggered == true);
        }
        SECTION("GetMIPData(x0,x1,y0,y1)") {
            HTDataSetGetterForTest htDataSetGetterForTest;
            CHECK(htDataSetGetterForTest.getMIPDataXYZTriggered == false);
            htDataSetGetterForTest.GetMIPData(0, 1, 2, 3);
            CHECK(htDataSetGetterForTest.getMIPDataXYZTriggered == true);
        }
        SECTION("GetMIPDataMemoryOrder()") {
            HTDataSetGetterForTest htDataSetGetterForTest;
            CHECK(htDataSetGetterForTest.getMIPDataMemoryOrderTriggered == false);
            htDataSetGetterForTest.GetMIPDataMemoryOrder();
            CHECK(htDataSetGetterForTest.getMIPDataMemoryOrderTriggered == true);
        }
    }
}
