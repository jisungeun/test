#include <catch2/catch.hpp>

#include "TCFBFSectionDataSetWriter.h"
#include "TCFSkeletonWriter.h"

#include "H5Cpp.h"

#include <fstream>

#include "CompareArray.h"
#include "BinaryFileIO.h"
#include "BFDataSetGetterMemory.h"
#include "TCFWriterUtility.h"

using namespace TC::TCFWriter;

namespace TCFBFSectionDataSetWriterTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCTCFWriterTest";
    const QString cellFolderPath = testFolderPath + "/TCFCellData";

    TEST_CASE("TCFBFSectionDataSetWriter : unit test") {
        SECTION("TCFBFDataSetWriter()") {
            TCFBFSectionDataSetWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            TCFBFSectionDataSetWriter writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetTCFBFDataSet()") {
            TCFBFSectionDataSetWriter writer;
            writer.SetTCFBFDataSet({});
            CHECK(&writer != nullptr);
        }
        SECTION("SetWholeDataSize()") {
            TCFBFSectionDataSetWriter writer;
            writer.SetWholeDataSize(1, 2);
            CHECK(&writer != nullptr);
        }
        SECTION("SetWritingStartIndex()") {
            TCFBFSectionDataSetWriter writer;
            writer.SetWritingStartIndex(1, 2);
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "tcfBFDataSetWriter.h5";

            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(false, false, true);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetTargetFilePath(targetFilePath);
            skeletonWriter.SetConfig(config);

            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto dataSizeZ = 3;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            constexpr auto memoryOrder = MemoryOrder3D::XYZ;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 0;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            auto bfDataSetGetterMemory = new BFDataSetGetterMemory;
            bfDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            bfDataSetGetterMemory->SetData(data, memoryOrder);
            
            IBFDataSetGetter::Pointer dataSetGetterPointer{bfDataSetGetterMemory};

            TCFBFDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(dataSetGetterPointer);
            tcfBFDataSet.SetMetaInfo(metaInfo);

            TCFBFSectionDataSetWriter writer;
            writer.SetWholeDataSize(dataSizeX, dataSizeY);
            writer.SetWritingStartIndex(0, 0);
            writer.SetTargetFilePath(targetFilePath);
            writer.SetTCFBFDataSet(tcfBFDataSet);

            const auto writingResult = writer.Write();

            CHECK(writingResult == true);
        }
    }

    TEST_CASE("TCFBFSectionDataSetWriter : practical test") {
        const QString targetFilePath = "tcfBFDataSetWriter.h5";

        TCFSkeletonConfig config;
        config.SetDataIncludingFlag(false, false, true);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetTargetFilePath(targetFilePath);
        skeletonWriter.SetConfig(config);
        skeletonWriter.Write();

        constexpr auto dataSizeX = 908;
        constexpr auto dataSizeY = 908;
        constexpr auto dataSizeZ = 3;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

        const auto dataBF = ReadFile_uint8_t((cellFolderPath + "/dataBF_YXZOrder").toStdString(), numberOfElements);
        constexpr auto memoryOrder3D = MemoryOrder3D::YXZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 1;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
        {
            auto bfDataSetGetterMemory = new BFDataSetGetterMemory;
            bfDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            bfDataSetGetterMemory->SetData(dataBF, memoryOrder3D);

            IBFDataSetGetter::Pointer dataSetGetterPointer{ bfDataSetGetterMemory };

            TCFBFDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(dataSetGetterPointer);
            tcfBFDataSet.SetMetaInfo(metaInfo);

            TCFBFSectionDataSetWriter writer;
            writer.SetWholeDataSize(950, 940);
            writer.SetWritingStartIndex(1, 2);
            writer.SetTargetFilePath(targetFilePath);
            writer.SetTCFBFDataSet(tcfBFDataSet);

            const auto writingResult = writer.Write();

            CHECK(writingResult == true);
        }

        H5::H5File writtenFile{ targetFilePath.toStdString(), H5F_ACC_RDONLY };

        CHECK(writtenFile.nameExists("/Data/BF/000001"));

        auto dataSetBF = writtenFile.openDataSet("/Data/BF/000001");

        CHECK(dataSetBF.attrExists("PositionX"));
        CHECK(dataSetBF.attrExists("PositionY"));
        CHECK(dataSetBF.attrExists("PositionZ"));
        CHECK(dataSetBF.attrExists("PositionC"));
        CHECK(dataSetBF.attrExists("RecordingTime"));
        CHECK(dataSetBF.attrExists("Time"));

        CHECK(ReadAttributeDouble(dataSetBF, "PositionX") == positionX);
        CHECK(ReadAttributeDouble(dataSetBF, "PositionY") == positionY);
        CHECK(ReadAttributeDouble(dataSetBF, "PositionZ") == positionZ);
        CHECK(ReadAttributeDouble(dataSetBF, "PositionC") == positionC);
        CHECK(ReadAttributeString(dataSetBF, "RecordingTime") == recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toStdString());
        CHECK(ReadAttributeDouble(dataSetBF, "Time") == elapsedTime);

        std::shared_ptr<uint8_t[]> resultDataBF{ new uint8_t[950*940*3]() };

        dataSetBF.read(resultDataBF.get(), H5::PredType::NATIVE_UCHAR);

        const auto answerDataBF = ReadFile_uint8_t((cellFolderPath + "/binDataBFExtended").toStdString(), 950 * 940 * 3);

        CHECK(CompareArray(resultDataBF.get(), answerDataBF.get(), numberOfElements));

        dataSetBF.close();
        writtenFile.close();
    }
}