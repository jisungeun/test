#include <catch2/catch.hpp>

#include "IBFDataSetGetter.h"

namespace IBFDataSetGetterTest {
    class BFDataSetGetterForTest final : public IBFDataSetGetter {
    public:
        BFDataSetGetterForTest() = default;
        ~BFDataSetGetterForTest() = default;
        auto GetData() const -> std::shared_ptr<uint8_t[]> override {
            this->getDataTriggered = true;
            return nullptr;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
            -> std::shared_ptr<uint8_t[]> override {
            this->getDataXYTriggered = true;
            return nullptr;
        }
        auto GetDataMemoryOrder() const->MemoryOrder3D override {
            this->getDataMemoryOrderTriggered = true;
            return MemoryOrder3D::XYZ;
        }

        auto GetColorChannelCount() const -> int32_t override {
            this->getColorChannelCountTriggered = true;
            return 1;
        }

        mutable bool getDataTriggered{ false };
        mutable bool getDataXYTriggered{ false };
        mutable bool getDataMemoryOrderTriggered{ false };
        mutable bool getColorChannelCountTriggered{ false };
    };

    TEST_CASE("IBFDataSetGetter : unit test") {
        SECTION("GetData()") {
            BFDataSetGetterForTest bfDataSetGetter;
            CHECK(bfDataSetGetter.getDataTriggered == false);
            bfDataSetGetter.GetData();
            CHECK(bfDataSetGetter.getDataTriggered == true);
        }
        SECTION("GetData(x0,x1,y0,y1)") {
            BFDataSetGetterForTest bfDataSetGetter;
            CHECK(bfDataSetGetter.getDataXYTriggered == false);
            bfDataSetGetter.GetData(0,1,2,3);
            CHECK(bfDataSetGetter.getDataXYTriggered == true);
        }
        SECTION("GetColorChannelCount()") {
            BFDataSetGetterForTest bfDataSetGetter;
            CHECK(bfDataSetGetter.getColorChannelCountTriggered == false);
            bfDataSetGetter.GetColorChannelCount();
            CHECK(bfDataSetGetter.getColorChannelCountTriggered == true);
        }
        SECTION("GetDataMemoryOrder()") {
            BFDataSetGetterForTest bfDataSetGetter;
            CHECK(bfDataSetGetter.getDataMemoryOrderTriggered == false);
            bfDataSetGetter.GetDataMemoryOrder();
            CHECK(bfDataSetGetter.getDataMemoryOrderTriggered == true);
        }
    }
}