#include <catch2/catch.hpp>

#include "TCFDataSetRecordedPosition.h"

namespace TCFDataSetRecordedPositionTest {
    TEST_CASE("TCFDataSetRecordedPosition : unit test") {
        SECTION("TCFDataSetRecordedPosition()") {
            TCFDataSetRecordedPosition position;
            CHECK(&position != nullptr);
        }
        SECTION("TCFDataSetRecordedPosition(other)") {
            TCFDataSetRecordedPosition srcPosition;
            srcPosition.SetPositions(1, 2, 3, 4, LengthUnit::Meter);

            TCFDataSetRecordedPosition destPosition(srcPosition);
            CHECK(destPosition.GetPositionX(LengthUnit::Meter) == 1);
            CHECK(destPosition.GetPositionY(LengthUnit::Meter) == 2);
            CHECK(destPosition.GetPositionZ(LengthUnit::Meter) == 3);
            CHECK(destPosition.GetPositionC(LengthUnit::Meter) == 4);
        }
        SECTION("operator=()") {
            TCFDataSetRecordedPosition srcPosition;
            srcPosition.SetPositions(1, 2, 3, 4, LengthUnit::Meter);

            TCFDataSetRecordedPosition destPosition;
            destPosition = srcPosition;
            CHECK(destPosition.GetPositionX(LengthUnit::Meter) == 1);
            CHECK(destPosition.GetPositionY(LengthUnit::Meter) == 2);
            CHECK(destPosition.GetPositionZ(LengthUnit::Meter) == 3);
            CHECK(destPosition.GetPositionC(LengthUnit::Meter) == 4);
        }
        SECTION("SetPositions()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Meter);
            CHECK(&position != nullptr);
        }
        SECTION("GetPositionX()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Meter);
            CHECK(position.GetPositionX(LengthUnit::Millimenter) == 1000);
        }
        SECTION("GetPositionY()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Meter);
            CHECK(position.GetPositionY(LengthUnit::Millimenter) == 2000);
        }
        SECTION("GetPositionZ()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Meter);
            CHECK(position.GetPositionZ(LengthUnit::Millimenter) == 3000);
        }
        SECTION("GetPositionC()") {
            TCFDataSetRecordedPosition position;
            position.SetPositions(1, 2, 3, 4, LengthUnit::Meter);
            CHECK(position.GetPositionC(LengthUnit::Millimenter) == 4000);
        }
    }
    TEST_CASE("TCFDataSetRecordedPosition : practical test") {
        constexpr double positionX = 123;
        constexpr double positionY = 456;
        constexpr double positionZ = 789;
        constexpr double positionC = 321;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, LengthUnit::Micrometer);

        CHECK(position.GetPositionX(LengthUnit::Millimenter) == positionX / 1000);
        CHECK(position.GetPositionY(LengthUnit::Millimenter) == positionY / 1000);
        CHECK(position.GetPositionZ(LengthUnit::Millimenter) == positionZ / 1000);
        CHECK(position.GetPositionC(LengthUnit::Millimenter) == positionC / 1000);
    }
}