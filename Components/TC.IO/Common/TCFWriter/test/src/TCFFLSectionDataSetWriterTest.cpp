#include <catch2/catch.hpp>

#include "TCFFLSectionDataSetWriter.h"
#include "TCFSkeletonWriter.h"

#include "H5Cpp.h"

#include <fstream>

#include "CompareArray.h"
#include "BinaryFileIO.h"
#include "FLDataSetGetterMemory.h"
#include "TCFWriterUtility.h"

using namespace TC::TCFWriter;

namespace TCFFLSectionDataSetWriterTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCTCFWriterTest";
    const QString cellFolderPath = testFolderPath + "/TCFCellData";

    TEST_CASE("TCFFLSectionDataSetWriter : unit test") {
        SECTION("TCFFLSectionDataSetWriter()") {
            TCFFLSectionDataSetWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            TCFFLSectionDataSetWriter writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetTCFFLDataSet()") {
            TCFFLSectionDataSetWriter writer;
            writer.SetTCFFLDataSet({});
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "tcfFLDataSetWriter.h5";

            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(false, true, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetTargetFilePath(targetFilePath);
            skeletonWriter.SetConfig(config);

            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto dataSizeZ = 3;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            std::shared_ptr<float[]> data{ new float[numberOfElements]() };
            constexpr auto memoryOrder = MemoryOrder3D::XYZ;

            std::shared_ptr<float[]> mipData{ new float[numberOfElements]() };
            constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr float pixelWorldSizeZ = 0.2f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 0;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            auto flDataSetGetterMemory = new FLDataSetGetterMemory;
            flDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            flDataSetGetterMemory->SetData(data, memoryOrder);
            flDataSetGetterMemory->SetMIPData(mipData, mipMemoryOrder);

            IFLDataSetGetter::Pointer flDataSetGetter{ flDataSetGetterMemory };

            TCFFLDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);
            metaInfo.SetChannelIndex(0);

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetter);
            
            TCFFLSectionDataSetWriter writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetTCFFLDataSet(tcfFLDataSet);
            writer.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
            writer.SetWritingStartIndex(0, 0, 0);

            const auto writingResult = writer.Write();

            CHECK(writingResult == true);
        }
    }

    TEST_CASE("TCFFLSectionDataSetWriter : practical test") {
        SECTION("only one data") {
            const QString targetFilePath = "tcfFLDataSetWriter.h5";

            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(false, true, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetTargetFilePath(targetFilePath);
            skeletonWriter.SetConfig(config);
            skeletonWriter.Write();

            constexpr auto dataSizeX = 1032;
            constexpr auto dataSizeY = 1032;
            constexpr auto dataSizeZ = 57;
            constexpr auto numberOfElements2D = dataSizeX * dataSizeY;
            constexpr auto numberOfElements3D = dataSizeX * dataSizeY * dataSizeZ;

            const auto data2DMIP = ReadFile_uint16_t((cellFolderPath + "/data2DFLMIP_YXOrder").toStdString(), numberOfElements2D);
            const auto data2DMIPFloat = std::shared_ptr<float[]>{ new float[numberOfElements2D]() };
            for (auto index = 0; index < numberOfElements2D; ++index) {
                data2DMIPFloat.get()[index] = static_cast<float>(data2DMIP.get()[index]);
            }
            constexpr auto memoryOrder2D = MemoryOrder2D::YX;

            const auto data3D = ReadFile_uint16_t((cellFolderPath + "/data3DFL_YXZOrder").toStdString(), numberOfElements3D);
            const auto data3DFloat = std::shared_ptr<float[]>{ new float[numberOfElements3D]() };
            for (auto index = 0; index < numberOfElements3D; ++index) {
                data3DFloat.get()[index] = static_cast<float>(data3D.get()[index]);
            }
            constexpr auto memoryOrder3D = MemoryOrder3D::YXZ;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr float pixelWorldSizeZ = 0.2f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 1;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            constexpr auto wholeDataSizeX = 1100;
            constexpr auto wholeDataSizeY = 1130;
            constexpr auto wholeDataSizeZ = dataSizeZ;

            constexpr auto writingStartIndexX = 50;
            constexpr auto writingStartIndexY = 60;
            constexpr auto writingStartIndexZ = 0;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
            {
                auto flDataSetGetterMemory = new FLDataSetGetterMemory;
                flDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
                flDataSetGetterMemory->SetData(data3DFloat, memoryOrder3D);
                flDataSetGetterMemory->SetMIPData(data2DMIPFloat, memoryOrder2D);

                IFLDataSetGetter::Pointer flDataSetGetter{ flDataSetGetterMemory };

                TCFFLDataSetMetaInfo metaInfo;
                metaInfo.SetDataMinMaxValue(2, 100);
                metaInfo.SetMIPDataMinMaxValue(10, 100);
                metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
                metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
                metaInfo.SetRecordedTime(recordedTime);
                metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
                metaInfo.SetTimeFrameIndex(timeFrameIndex);
                metaInfo.SetPosition(position);
                metaInfo.SetChannelIndex(1);

                TCFFLDataSet tcfFLDataSet;
                tcfFLDataSet.SetDataSetGetter(flDataSetGetter);

                TCFFLSectionDataSetWriter writer;
                writer.SetTargetFilePath(targetFilePath);
                writer.SetTCFFLDataSet(tcfFLDataSet);
                writer.SetWholeDataSize(wholeDataSizeX, wholeDataSizeY, wholeDataSizeZ);
                writer.SetWritingStartIndex(writingStartIndexX, writingStartIndexY, writingStartIndexZ);

                const auto writingResult = writer.Write();

                CHECK(writingResult == true);
            }

            H5::H5File writtenFile{ targetFilePath.toStdString(), H5F_ACC_RDONLY };

            CHECK(writtenFile.nameExists("/Data/2DFLMIP/CH1/000001"));
            CHECK(writtenFile.nameExists("/Data/3DFL/CH1/000001"));

            auto dataSet2DFLMIP = writtenFile.openDataSet("/Data/2DFLMIP/CH1/000001");
            auto dataSet3DFL = writtenFile.openDataSet("/Data/3DFL/CH1/000001");

            const auto dataSpace2DMIP = dataSet2DFLMIP.getSpace();
            const auto dataSpace3D = dataSet3DFL.getSpace();

            hsize_t dataSpace2DMIPDim[3], dataSpace3DDims[3];

            CHECK(dataSpace2DMIP.getSimpleExtentDims(dataSpace2DMIPDim) == 2);
            CHECK(dataSpace2DMIPDim[0] == wholeDataSizeY);
            CHECK(dataSpace2DMIPDim[1] == wholeDataSizeX);

            CHECK(dataSpace3D.getSimpleExtentDims(dataSpace3DDims) == 3);
            CHECK(dataSpace3DDims[0] == wholeDataSizeZ);
            CHECK(dataSpace3DDims[1] == wholeDataSizeY);
            CHECK(dataSpace3DDims[2] == wholeDataSizeX);

            CHECK(dataSet2DFLMIP.attrExists("PositionX"));
            CHECK(dataSet2DFLMIP.attrExists("PositionY"));
            CHECK(dataSet2DFLMIP.attrExists("PositionZ"));
            CHECK(dataSet2DFLMIP.attrExists("PositionC"));
            CHECK(dataSet2DFLMIP.attrExists("MinIntensity"));
            CHECK(dataSet2DFLMIP.attrExists("MaxIntensity"));
            CHECK(dataSet2DFLMIP.attrExists("RecordingTime"));
            CHECK(dataSet2DFLMIP.attrExists("Time"));

            CHECK(ReadAttributeDouble(dataSet2DFLMIP, "PositionX") == positionX);
            CHECK(ReadAttributeDouble(dataSet2DFLMIP, "PositionY") == positionY);
            CHECK(ReadAttributeDouble(dataSet2DFLMIP, "PositionZ") == positionZ);
            CHECK(ReadAttributeDouble(dataSet2DFLMIP, "PositionC") == positionC);
            CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet2DFLMIP, "MaxIntensity"))) == 100);
            CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet2DFLMIP, "MinIntensity"))) == 10);
            CHECK(ReadAttributeString(dataSet2DFLMIP, "RecordingTime") == recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toStdString());
            CHECK(ReadAttributeDouble(dataSet2DFLMIP, "Time") == elapsedTime);

            CHECK(dataSet3DFL.attrExists("PositionX"));
            CHECK(dataSet3DFL.attrExists("PositionY"));
            CHECK(dataSet3DFL.attrExists("PositionZ"));
            CHECK(dataSet3DFL.attrExists("PositionC"));
            CHECK(dataSet3DFL.attrExists("MinIntensity"));
            CHECK(dataSet3DFL.attrExists("MaxIntensity"));
            CHECK(dataSet3DFL.attrExists("RecordingTime"));
            CHECK(dataSet3DFL.attrExists("Time"));

            CHECK(ReadAttributeDouble(dataSet3DFL, "PositionX") == positionX);
            CHECK(ReadAttributeDouble(dataSet3DFL, "PositionY") == positionY);
            CHECK(ReadAttributeDouble(dataSet3DFL, "PositionZ") == positionZ);
            CHECK(ReadAttributeDouble(dataSet3DFL, "PositionC") == positionC);
            CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet3DFL, "MaxIntensity"))) == 100);
            CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet3DFL, "MinIntensity"))) == 2);
            CHECK(ReadAttributeString(dataSet3DFL, "RecordingTime") == recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toStdString());
            CHECK(ReadAttributeDouble(dataSet3DFL, "Time") == elapsedTime);

            constexpr auto numberOfWholeElements2D = wholeDataSizeX * wholeDataSizeY;
            constexpr auto numberOfWholeElements3D = wholeDataSizeX * wholeDataSizeY * wholeDataSizeZ;

            std::shared_ptr<uint16_t[]> resultData3D{ new uint16_t[numberOfWholeElements3D]() };
            std::shared_ptr<uint16_t[]> resultData2D{ new uint16_t[numberOfWholeElements2D]() };

            dataSet3DFL.read(resultData3D.get(), H5::PredType::NATIVE_UINT16);
            dataSet2DFLMIP.read(resultData2D.get(), H5::PredType::NATIVE_UINT16);

            const auto answerData3D = ReadFile_uint16_t((cellFolderPath + "/binData3DFLExtended").toStdString(), numberOfWholeElements3D);
            const auto answerData2D = ReadFile_uint16_t((cellFolderPath + "/binData2DFLMIPExtended").toStdString(), numberOfWholeElements2D);

            CHECK(CompareArray(resultData3D.get(), answerData3D.get(), numberOfWholeElements3D));
            CHECK(CompareArray(resultData2D.get(), answerData2D.get(), numberOfWholeElements2D));

            dataSet2DFLMIP.close();
            dataSet3DFL.close();
            writtenFile.close();
        }
    }
}
