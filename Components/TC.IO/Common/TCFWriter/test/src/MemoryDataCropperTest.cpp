#include <catch2/catch.hpp>

#include "MemoryDataCropper.h"

namespace MemoryDataCropperTest {
    TEST_CASE("MemoryDataCropper : unit test") {
        SECTION("MemoryOrderCropper()") {
            MemoryDataCropper cropper{};
            CHECK(&cropper != nullptr);
        }
        SECTION("SetMemory()") {
            MemoryDataCropper cropper{};
            cropper.SetMemory(nullptr, 1);
            CHECK(&cropper != nullptr);
        }
        SECTION("SetMemoryInfo()") {
            MemoryDataCropper cropper{};
            cropper.SetMemoryInfo(1, 2, 3, MemoryOrder3D::XYZ);
            CHECK(&cropper != nullptr);
        }
        SECTION("SetCropInfo()") {
            MemoryDataCropper cropper{};
            cropper.SetCropInfo(1, 2, 3, 4, 5, 6);
            CHECK(&cropper != nullptr);
        }
        SECTION("Crop()") {
            constexpr int64_t sizeX = 3;
            constexpr int64_t sizeY = 3;
            constexpr int64_t sizeZ = 3;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;
            
            auto srcMemory = new int32_t[numberOfElements]();
            for (auto i = 0; i < numberOfElements; ++i) {
                srcMemory[i] = i;
            }

            constexpr int64_t x0 = 1;
            constexpr int64_t x1 = 2;
            constexpr int64_t y0 = 1;
            constexpr int64_t y1 = 2;
            constexpr int64_t z0 = 1;
            constexpr int64_t z1 = 2;

            MemoryDataCropper cropper;
            cropper.SetMemory(reinterpret_cast<const uint8_t*>(srcMemory), sizeof(int32_t));
            cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ);
            cropper.SetCropInfo(x0, x1, y0, y1, z0, z1);

            CHECK(cropper.Crop() == true);
            delete[] srcMemory;
        }
        SECTION("GetCroppedMemory()") {
            constexpr int64_t sizeX = 3;
            constexpr int64_t sizeY = 3;
            constexpr int64_t sizeZ = 3;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            auto srcMemory = new int32_t[numberOfElements]();
            for (auto i = 0; i < numberOfElements; ++i) {
                srcMemory[i] = i;
            }

            constexpr int64_t x0 = 1;
            constexpr int64_t x1 = 2;
            constexpr int64_t y0 = 1;
            constexpr int64_t y1 = 2;
            constexpr int64_t z0 = 1;
            constexpr int64_t z1 = 2;

            MemoryDataCropper cropper;
            cropper.SetMemory(reinterpret_cast<const uint8_t*>(srcMemory), sizeof(int32_t));
            cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ);
            cropper.SetCropInfo(x0, x1, y0, y1, z0, z1);

            cropper.Crop();
            delete[] srcMemory;

            auto croppedMemory = new int32_t[2 * 2 * 2]();

            cropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedMemory));

            CHECK(croppedMemory[0] == 13);
            CHECK(croppedMemory[1] == 14);
            CHECK(croppedMemory[2] == 16);
            CHECK(croppedMemory[3] == 17);
            CHECK(croppedMemory[4] == 22);
            CHECK(croppedMemory[5] == 23);
            CHECK(croppedMemory[6] == 25);
            CHECK(croppedMemory[7] == 26);

            delete[] croppedMemory;
        }
    }
    TEST_CASE("MemoryDataCropper : practical test") {
        SECTION("MemoryOrder3D Changes") {
            constexpr int64_t sizeX = 3;
            constexpr int64_t sizeY = 3;
            constexpr int64_t sizeZ = 3;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            auto srcMemory = new int32_t[numberOfElements]();
            for (auto i = 0; i < numberOfElements; ++i) {
                srcMemory[i] = i;
            }

            constexpr int64_t x0 = 1;
            constexpr int64_t x1 = 2;
            constexpr int64_t y0 = 1;
            constexpr int64_t y1 = 2;
            constexpr int64_t z0 = 1;
            constexpr int64_t z1 = 2;

            MemoryDataCropper cropper;
            cropper.SetMemory(reinterpret_cast<const uint8_t*>(srcMemory), sizeof(int32_t));
            cropper.SetCropInfo(x0, x1, y0, y1, z0, z1);

            auto croppedMemory = new int32_t[2 * 2 * 2]();

            SECTION("XYZ") {
                cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::XYZ);
                cropper.Crop();

                cropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedMemory));
                CHECK(croppedMemory[0] == 13);
                CHECK(croppedMemory[1] == 14);
                CHECK(croppedMemory[2] == 16);
                CHECK(croppedMemory[3] == 17);
                CHECK(croppedMemory[4] == 22);
                CHECK(croppedMemory[5] == 23);
                CHECK(croppedMemory[6] == 25);
                CHECK(croppedMemory[7] == 26);
            }
            SECTION("YXZ") {
                cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::YXZ);
                cropper.Crop();

                cropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedMemory));
                CHECK(croppedMemory[0] == 13);
                CHECK(croppedMemory[1] == 14);
                CHECK(croppedMemory[2] == 16);
                CHECK(croppedMemory[3] == 17);
                CHECK(croppedMemory[4] == 22);
                CHECK(croppedMemory[5] == 23);
                CHECK(croppedMemory[6] == 25);
                CHECK(croppedMemory[7] == 26);
            }
            SECTION("ZXY") {
                cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::ZXY);
                cropper.Crop();

                cropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedMemory));
                CHECK(croppedMemory[0] == 13);
                CHECK(croppedMemory[1] == 14);
                CHECK(croppedMemory[2] == 16);
                CHECK(croppedMemory[3] == 17);
                CHECK(croppedMemory[4] == 22);
                CHECK(croppedMemory[5] == 23);
                CHECK(croppedMemory[6] == 25);
                CHECK(croppedMemory[7] == 26);
            }
            SECTION("ZYX") {
                cropper.SetMemoryInfo(sizeX, sizeY, sizeZ, MemoryOrder3D::ZYX);
                cropper.Crop();

                cropper.GetCroppedMemory(reinterpret_cast<uint8_t*>(croppedMemory));
                CHECK(croppedMemory[0] == 13);
                CHECK(croppedMemory[1] == 14);
                CHECK(croppedMemory[2] == 16);
                CHECK(croppedMemory[3] == 17);
                CHECK(croppedMemory[4] == 22);
                CHECK(croppedMemory[5] == 23);
                CHECK(croppedMemory[6] == 25);
                CHECK(croppedMemory[7] == 26);
            }

            delete[] srcMemory;
            delete[] croppedMemory;
        }
    }
}