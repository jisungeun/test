#include <catch2/catch.hpp>

#include "TCFDeviceInfo.h"

namespace TCFDeviceInfoTest {
    TEST_CASE("TCFDeviceInfo : unit test") {
        SECTION("TCFDeviceInfo()") {
            TCFDeviceInfo tcfDeviceInfo;
            CHECK(&tcfDeviceInfo != nullptr);
        }
        SECTION("TCFDeviceInfo(other)") {
            TCFDeviceInfo srcTCFDeviceInfo;
            srcTCFDeviceInfo.SetMagnification(1);

            TCFDeviceInfo destTCFDeviceInfo(srcTCFDeviceInfo);
            CHECK(destTCFDeviceInfo.GetMagnification() == 1);
        }
        SECTION("operator=()") {
            TCFDeviceInfo srcTCFDeviceInfo;
            srcTCFDeviceInfo.SetMagnification(1);

            TCFDeviceInfo destTCFDeviceInfo;
            destTCFDeviceInfo = srcTCFDeviceInfo;
            CHECK(destTCFDeviceInfo.GetMagnification() == 1);
        }
        SECTION("SetMagnification()") {
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetMagnification(1);
            CHECK(&tcfDeviceInfo != nullptr);
        }
        SECTION("GetMagnification()") {
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetMagnification(1);
            CHECK(tcfDeviceInfo.GetMagnification() == 1);
        }
        SECTION("SetNA()") {
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetNA(1);
            CHECK(&tcfDeviceInfo != nullptr);
        }
        SECTION("GetNA()"){
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetNA(1);
            CHECK(tcfDeviceInfo.GetNA() == 1);
        }
        SECTION("SetRI()") {
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetRI(1);
            CHECK(&tcfDeviceInfo != nullptr);
        }
        SECTION("GetRI()") {
            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetRI(1);
            CHECK(tcfDeviceInfo.GetRI() == 1);
        }
    }
    TEST_CASE("TCFDeviceInfo : practical test") {
        constexpr double magnification = 40;
        constexpr double na = 1.2;
        constexpr double ri = 1.337;

        TCFDeviceInfo tcfDeviceInfo;
        tcfDeviceInfo.SetMagnification(magnification);
        tcfDeviceInfo.SetNA(na);
        tcfDeviceInfo.SetRI(ri);

        CHECK(tcfDeviceInfo.GetMagnification() == magnification);
        CHECK(tcfDeviceInfo.GetNA() == na);
        CHECK(tcfDeviceInfo.GetRI() == ri);
    }
}
