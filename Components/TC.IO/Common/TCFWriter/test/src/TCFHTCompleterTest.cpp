#include <catch2/catch.hpp>

#include "TCFHTCompleter.h"
#include "TCFSkeletonWriter.h"
#include "TCFHTSectionDataSetWriter.h"
#include "TCFWriterUtility.h"
#include "H5Cpp.h"
#include "HTDataSetGetterMemory.h"

namespace TCFHTCompleterTest {
    using namespace TC::TCFWriter;

    TEST_CASE("TCFHTCompleter : unit test") {
        SECTION("TCFHTCompleter()") {
            TCFHTCompleter completer;
            CHECK(&completer != nullptr);
        }
        SECTION("SetTargetTCFFilePath()") {
            TCFHTCompleter completer;
            completer.SetTargetTCFFilePath("");
            CHECK(&completer != nullptr);
        }
        SECTION("SetSize()") {
            TCFHTCompleter completer;
            completer.SetSize(1, 2, 3);
            CHECK(&completer != nullptr);
        }
        SECTION("SetPixelWorldSize()") {
            TCFHTCompleter completer;
            completer.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(&completer != nullptr);
        }
        SECTION("MakeCompleteGroup()") {
            const QString targetTCFFilePath = "tcfHTCompleterTest.h5";

            TCFSkeletonConfig skeletonConfig;
            skeletonConfig.SetDataIncludingFlag(true, false, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetConfig(skeletonConfig);
            skeletonWriter.SetTargetFilePath(targetTCFFilePath);
            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto dataSizeZ = 3;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr float pixelWorldSizeZ = 0.2f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            constexpr float rawData1[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
            constexpr float rawMIPData1[dataSizeX * dataSizeY] = { 2,3,4,5 };

            constexpr float rawData2[numberOfElements] = { 3,4,5,6,7,8,9,10,11,12,13,14 };
            constexpr float rawMIPData2[dataSizeX * dataSizeY] = { 3,4,5,6 };

            constexpr auto memoryOrder = MemoryOrder3D::XYZ;
            constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

            std::shared_ptr<float[]> data1{ new float[numberOfElements]() };
            std::shared_ptr<float[]> mipData1{ new float[numberOfElements]() };

            std::copy_n(rawData1, numberOfElements, data1.get());
            std::copy_n(rawMIPData1, dataSizeX * dataSizeY, mipData1.get());

            std::shared_ptr<float[]> data2{ new float[numberOfElements]() };
            std::shared_ptr<float[]> mipData2{ new float[numberOfElements]() };

            std::copy_n(rawData2, numberOfElements, data2.get());
            std::copy_n(rawMIPData2, dataSizeX * dataSizeY, mipData2.get());

            TCFHTDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetPosition(position);

            auto htDataSetGetterMemory1 = new HTDataSetGetterMemory;
            htDataSetGetterMemory1->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            htDataSetGetterMemory1->SetData(data1, memoryOrder);
            htDataSetGetterMemory1->SetMIPData(mipData1, mipMemoryOrder);

            IHTDataSetGetter::Pointer htDataSetGetter1{ htDataSetGetterMemory1 };

            auto htDataSetGetterMemory2 = new HTDataSetGetterMemory;
            htDataSetGetterMemory2->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            htDataSetGetterMemory2->SetData(data1, memoryOrder);
            htDataSetGetterMemory2->SetMIPData(mipData1, mipMemoryOrder);

            IHTDataSetGetter::Pointer htDataSetGetter2{ htDataSetGetterMemory2 };

            auto metaInfo1 = metaInfo;
            metaInfo1.SetTimeFrameIndex(0);

            auto metaInfo2 = metaInfo;
            metaInfo2.SetTimeFrameIndex(1);

            TCFHTDataSet dataSet1;
            dataSet1.SetDataSetGetter(htDataSetGetter1);
            dataSet1.SetMetaInfo(metaInfo1);

            TCFHTDataSet dataSet2;
            dataSet2.SetDataSetGetter(htDataSetGetter2);
            dataSet2.SetMetaInfo(metaInfo2);

            TCFHTSectionDataSetWriter dataSetWriter;
            dataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
            dataSetWriter.SetWritingStartIndex(0, 0, 0);
            dataSetWriter.SetTargetFilePath(targetTCFFilePath);

            dataSetWriter.SetTCFHTDataSet(dataSet1);
            dataSetWriter.Write();

            dataSetWriter.SetTCFHTDataSet(dataSet2);
            dataSetWriter.Write();

            constexpr double timeInterval = 5;
            constexpr auto timeIntervalUnit = TimeUnit::Second;

            TCFHTCompleter completer;
            completer.SetTargetTCFFilePath(targetTCFFilePath);
            completer.SetSize(dataSizeX, dataSizeY, dataSizeZ);
            completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);

            const auto completionResult = completer.MakeCompleteGroup();
            CHECK(completionResult == true);
        }
    }
    TEST_CASE("TCFHTCompleter : practical test") {
        const QString targetTCFFilePath = "tcfHTCompleterTest.h5";

        TCFSkeletonConfig skeletonConfig;
        skeletonConfig.SetDataIncludingFlag(true, false, false);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetConfig(skeletonConfig);
        skeletonWriter.SetTargetFilePath(targetTCFFilePath);
        skeletonWriter.Write();

        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto dataSizeZ = 3;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr float pixelWorldSizeZ = 0.2f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        constexpr float rawData1[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
        constexpr float rawMIPData1[dataSizeX * dataSizeY] = { 2,3,4,5 };

        constexpr float rawData2[numberOfElements] = { 3,4,5,6,7,8,9,10,11,12,13,14 };
        constexpr float rawMIPData2[dataSizeX * dataSizeY] = { 3,4,5,6 };

        constexpr auto minValue3D = 1.f;
        constexpr auto maxValue3D = 14.f;

        constexpr auto minValue2DMIP = 2.f;
        constexpr auto maxValue2DMIP = 6.f;
        
        constexpr auto memoryOrder = MemoryOrder3D::XYZ;
        constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

        std::shared_ptr<float[]> data1{ new float[numberOfElements]() };
        std::shared_ptr<float[]> mipData1{ new float[numberOfElements]() };

        std::copy_n(rawData1, numberOfElements, data1.get());
        std::copy_n(rawMIPData1, dataSizeX * dataSizeY, mipData1.get());

        std::shared_ptr<float[]> data2{ new float[numberOfElements]() };
        std::shared_ptr<float[]> mipData2{ new float[numberOfElements]() };

        std::copy_n(rawData2, numberOfElements, data2.get());
        std::copy_n(rawMIPData2, dataSizeX * dataSizeY, mipData2.get());

        TCFHTDataSetMetaInfo metaInfo;
        metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        metaInfo.SetDataMinMaxValue(minValue3D, maxValue3D);
        metaInfo.SetMIPDataMinMaxValue(minValue2DMIP, maxValue2DMIP);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetPosition(position);

        auto htDataSetGetterMemory1 = new HTDataSetGetterMemory;
        htDataSetGetterMemory1->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        htDataSetGetterMemory1->SetData(data1, memoryOrder);
        htDataSetGetterMemory1->SetMIPData(mipData1, mipMemoryOrder);

        IHTDataSetGetter::Pointer htDataSetGetter1{ htDataSetGetterMemory1 };

        auto htDataSetGetterMemory2 = new HTDataSetGetterMemory;
        htDataSetGetterMemory2->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        htDataSetGetterMemory2->SetData(data1, memoryOrder);
        htDataSetGetterMemory2->SetMIPData(mipData1, mipMemoryOrder);

        IHTDataSetGetter::Pointer htDataSetGetter2{ htDataSetGetterMemory2 };

        auto metaInfo1 = metaInfo;
        metaInfo1.SetTimeFrameIndex(0);

        auto metaInfo2 = metaInfo;
        metaInfo2.SetTimeFrameIndex(1);

        TCFHTDataSet dataSet1;
        dataSet1.SetDataSetGetter(htDataSetGetter1);
        dataSet1.SetMetaInfo(metaInfo1);

        TCFHTDataSet dataSet2;
        dataSet2.SetDataSetGetter(htDataSetGetter2);
        dataSet2.SetMetaInfo(metaInfo2);

        TCFHTSectionDataSetWriter dataSetWriter;
        dataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
        dataSetWriter.SetWritingStartIndex(0, 0, 0);
        dataSetWriter.SetTargetFilePath(targetTCFFilePath);

        dataSetWriter.SetTCFHTDataSet(dataSet1);
        dataSetWriter.Write();

        dataSetWriter.SetTCFHTDataSet(dataSet2);
        dataSetWriter.Write();

        TCFHTCompleter completer;
        completer.SetTargetTCFFilePath(targetTCFFilePath);
        completer.SetSize(dataSizeX, dataSizeY, dataSizeZ);
        completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);

        completer.MakeCompleteGroup();
        

        H5::H5File file(targetTCFFilePath.toStdString(), H5F_ACC_RDONLY);
        auto data3dGroup = file.openGroup("/Data/3D");
        auto data2DMIPGroup = file.openGroup("/Data/2DMIP");

        CHECK(ReadAttributeInt64(data3dGroup, "DataCount") == 2);
        CHECK(ReadAttributeDouble(data3dGroup, "ResolutionX") == pixelWorldSizeX);
        CHECK(ReadAttributeDouble(data3dGroup, "ResolutionY") == pixelWorldSizeY);
        CHECK(ReadAttributeDouble(data3dGroup, "ResolutionZ") == pixelWorldSizeZ);
        CHECK(ReadAttributeDouble(data3dGroup, "RIMin") == minValue3D);
        CHECK(ReadAttributeDouble(data3dGroup, "RIMax") == maxValue3D);
        CHECK(ReadAttributeInt64(data3dGroup, "SizeX") == dataSizeX);
        CHECK(ReadAttributeInt64(data3dGroup, "SizeY") == dataSizeY);
        CHECK(ReadAttributeInt64(data3dGroup, "SizeZ") == dataSizeZ);

        CHECK(ReadAttributeInt64(data2DMIPGroup, "DataCount") == 2);
        CHECK(ReadAttributeDouble(data2DMIPGroup, "ResolutionX") == pixelWorldSizeX);
        CHECK(ReadAttributeDouble(data2DMIPGroup, "ResolutionY") == pixelWorldSizeY);
        CHECK(ReadAttributeDouble(data2DMIPGroup, "RIMin") == minValue2DMIP);
        CHECK(ReadAttributeDouble(data2DMIPGroup, "RIMax") == maxValue2DMIP);
        CHECK(ReadAttributeInt64(data2DMIPGroup, "SizeX") == dataSizeX);
        CHECK(ReadAttributeInt64(data2DMIPGroup, "SizeY") == dataSizeY);

        data3dGroup.close();
        data2DMIPGroup.close();

        file.close();
    }
}
