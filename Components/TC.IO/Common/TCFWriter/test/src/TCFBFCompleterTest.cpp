#include <catch2/catch.hpp>

#include "H5Cpp.h"

#include "TCFBFCompleter.h"
#include "TCFBFDataSet.h"
#include "TCFBFSectionDataSetWriter.h"
#include "TCFSkeletonWriter.h"
#include "TCFWriterUtility.h"
#include "BFDataSetGetterMemory.h"

using namespace TC::TCFWriter;

namespace TCFBFCompleterTest {
    TEST_CASE("TCFBFCompleter : unit test") {
        SECTION("TCFBFCompleter()") {
            TCFBFCompleter completer;
            CHECK(&completer != nullptr);
        }
        SECTION("SetTargetTCFFilePath()") {
            TCFBFCompleter completer;
            completer.SetTargetTCFFilePath("");
            CHECK(&completer != nullptr);
        }
        SECTION("SetSize()"){
            TCFBFCompleter completer;
            completer.SetSize(1, 2);
            CHECK(&completer != nullptr);
        }
        SECTION("SetPixelWorldSize()"){
            TCFBFCompleter completer;
            completer.SetPixelWorldSize(1, 2, LengthUnit::Micrometer);
            CHECK(&completer != nullptr);
        }
        SECTION("MakeCompleteGroup()") {
            const QString targetTCFFilePath = "tcfBFCompleterTest.h5";

            TCFSkeletonConfig skeletonConfig;
            skeletonConfig.SetDataIncludingFlag(false, false, true);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetConfig(skeletonConfig);
            skeletonWriter.SetTargetFilePath(targetTCFFilePath);
            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * 3; //rgb color

            constexpr uint8_t rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
            std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            std::copy_n(rawData, numberOfElements, data.get());
            constexpr auto memoryOrder = MemoryOrder3D::XYZ;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 0;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            auto bfDataSetGetterMemory = new BFDataSetGetterMemory;
            bfDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, 3);
            bfDataSetGetterMemory->SetData(data, memoryOrder);

            BFDataSetGetterMemory::Pointer bfDataSetGetterPointer{ bfDataSetGetterMemory };

            TCFBFDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);
            tcfBFDataSet.SetMetaInfo(metaInfo);

            TCFBFSectionDataSetWriter tcfBFSectionDataSetWriter;
            tcfBFSectionDataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY);
            tcfBFSectionDataSetWriter.SetWritingStartIndex(0, 0);
            tcfBFSectionDataSetWriter.SetTCFBFDataSet(tcfBFDataSet);
            tcfBFSectionDataSetWriter.SetTargetFilePath(targetTCFFilePath);
            tcfBFSectionDataSetWriter.Write();

            TCFBFCompleter completer;
            completer.SetTargetTCFFilePath(targetTCFFilePath);
            completer.SetSize(dataSizeX, dataSizeY);
            completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);

            const auto result = completer.MakeCompleteGroup();
            CHECK(result == true);
        }
    }
    TEST_CASE("TCFBFCompleter : practical test") {
        const QString targetTCFFilePath = "tcfBFCompleterTest.h5";

        TCFSkeletonConfig skeletonConfig;
        skeletonConfig.SetDataIncludingFlag(false, false, true);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetConfig(skeletonConfig);
        skeletonWriter.SetTargetFilePath(targetTCFFilePath);
        skeletonWriter.Write();

        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * 3; //rgb color

        constexpr uint8_t rawData[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
        std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
        std::copy_n(rawData, numberOfElements, data.get());
        constexpr auto memoryOrder = MemoryOrder3D::XYZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 0;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        auto bfDataSetGetterMemory = new BFDataSetGetterMemory;
        bfDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, 3);
        bfDataSetGetterMemory->SetData(data, memoryOrder);

        BFDataSetGetterMemory::Pointer bfDataSetGetterPointer{ bfDataSetGetterMemory };

        TCFBFDataSetMetaInfo metaInfo;
        metaInfo.SetDataSize(dataSizeX, dataSizeY);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetTimeFrameIndex(timeFrameIndex);
        metaInfo.SetPosition(position);

        TCFBFDataSet tcfBFDataSet;
        tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);
        tcfBFDataSet.SetMetaInfo(metaInfo);

        TCFBFSectionDataSetWriter tcfBFSectionDataSetWriter;
        tcfBFSectionDataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY);
        tcfBFSectionDataSetWriter.SetWritingStartIndex(0, 0);
        tcfBFSectionDataSetWriter.SetTCFBFDataSet(tcfBFDataSet);
        tcfBFSectionDataSetWriter.SetTargetFilePath(targetTCFFilePath);
        tcfBFSectionDataSetWriter.Write();

        TCFBFCompleter completer;
        completer.SetTargetTCFFilePath(targetTCFFilePath);
        completer.SetSize(dataSizeX, dataSizeY);
        completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);

        const auto result = completer.MakeCompleteGroup();
        CHECK(result == true);

        H5::H5File file(targetTCFFilePath.toStdString(), H5F_ACC_RDONLY);

        auto dataBFGroup = file.openGroup("/Data/BF");

        CHECK(ReadAttributeInt64(dataBFGroup, "DataCount") == 1);
        CHECK(ReadAttributeDouble(dataBFGroup, "ResolutionX") == pixelWorldSizeX);
        CHECK(ReadAttributeDouble(dataBFGroup, "ResolutionY") == pixelWorldSizeY);
        CHECK(ReadAttributeInt64(dataBFGroup, "SizeX") == static_cast<int64_t>(dataSizeX));
        CHECK(ReadAttributeInt64(dataBFGroup, "SizeY") == static_cast<int64_t>(dataSizeY));

        dataBFGroup.close();
        file.close();

    }

}
