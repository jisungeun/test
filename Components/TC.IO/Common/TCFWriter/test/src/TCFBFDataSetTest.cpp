#include <catch2/catch.hpp>

#include "TCFBFDataSet.h"

using namespace TC::TCFWriter;

namespace TCFBFDataSetTest {
    class BFDataSetGetterForTest final : public IBFDataSetGetter {
    public:
        BFDataSetGetterForTest() = default;
        ~BFDataSetGetterForTest() = default;
        auto GetData() const -> std::shared_ptr<uint8_t[]> override {
            std::shared_ptr<uint8_t[]> data{ new uint8_t };
            data.get()[0] = 1;
            return data;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
            -> std::shared_ptr<uint8_t[]> override {
            std::shared_ptr<uint8_t[]> data{ new uint8_t };
            data.get()[0] = 1;
            return data;
        }

        auto GetColorChannelCount() const -> int32_t override {
            return 1;
        }
        auto GetDataMemoryOrder() const -> MemoryOrder3D override {
            return MemoryOrder3D::XYZ;
        }
    };

    TEST_CASE("TCFBFDataSet : unit test") {
        SECTION("TCFBFDataSet()") {
            TCFBFDataSet tcfBFDataSet;
            CHECK(&tcfBFDataSet != nullptr);
        }
        SECTION("TCFBFDataSet(other)") {
            TCFBFDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(1, 2);

            TCFBFDataSet srcTCFBFDataSet;
            srcTCFBFDataSet.SetMetaInfo(metaInfo);

            TCFBFDataSet destTCFBFDataSet(srcTCFBFDataSet);
            CHECK(destTCFBFDataSet.GetMetaInfo().GetSizeX() == 1);
            CHECK(destTCFBFDataSet.GetMetaInfo().GetSizeY() == 2);
        }
        SECTION("operator=()") {
            TCFBFDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(1, 2);

            TCFBFDataSet srcTCFBFDataSet;
            srcTCFBFDataSet.SetMetaInfo(metaInfo);

            TCFBFDataSet destTCFBFDataSet;
            destTCFBFDataSet = srcTCFBFDataSet;
            CHECK(destTCFBFDataSet.GetMetaInfo().GetSizeX() == 1);
            CHECK(destTCFBFDataSet.GetMetaInfo().GetSizeY() == 2);
        }
        SECTION("SetDataSetGetter()") {
            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter({});
            CHECK(&tcfBFDataSet != nullptr);
        }
        SECTION("GetData()") {
            auto bfDataSetGetter = new BFDataSetGetterForTest;
            IBFDataSetGetter::Pointer bfDataSetGetterPointer{ bfDataSetGetter };

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);

            CHECK(tcfBFDataSet.GetData().get()[0] == 1);
        }
        SECTION("GetData(x0,x1,y0,y1)") {
            auto bfDataSetGetter = new BFDataSetGetterForTest;
            IBFDataSetGetter::Pointer bfDataSetGetterPointer{ bfDataSetGetter };

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);

            CHECK(tcfBFDataSet.GetData(0, 1, 2, 3).get()[0] == 1);
        }
        SECTION("GetDataMemoryOrder()") {
            auto bfDataSetGetter = new BFDataSetGetterForTest;
            IBFDataSetGetter::Pointer bfDataSetGetterPointer{ bfDataSetGetter };

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);

            CHECK(tcfBFDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        }
        SECTION("GetChannelCount()") {
            auto bfDataSetGetter = new BFDataSetGetterForTest;
            IBFDataSetGetter::Pointer bfDataSetGetterPointer{ bfDataSetGetter };

            TCFBFDataSet tcfBFDataSet;
            tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);

            CHECK(tcfBFDataSet.GetChannelCount() == 1);
        }
    }
    TEST_CASE("TCFBFDataSet : practical test") {
        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * 3; //rgb color

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 0;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        auto bfDataSetGetter = new BFDataSetGetterForTest;
        IBFDataSetGetter::Pointer bfDataSetGetterPointer{ bfDataSetGetter };

        TCFBFDataSetMetaInfo metaInfo;
        metaInfo.SetDataSize(dataSizeX, dataSizeY);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetTimeFrameIndex(timeFrameIndex);
        metaInfo.SetPosition(position);

        TCFBFDataSet tcfBFDataSet;
        tcfBFDataSet.SetDataSetGetter(bfDataSetGetterPointer);
        tcfBFDataSet.SetMetaInfo(metaInfo);

        CHECK(tcfBFDataSet.GetData().get()[0] == 1);
        CHECK(tcfBFDataSet.GetData(0, 1, 2, 3).get()[0] == 1);
        CHECK(tcfBFDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        CHECK(tcfBFDataSet.GetMetaInfo().GetSizeX() == dataSizeX);
        CHECK(tcfBFDataSet.GetMetaInfo().GetSizeY() == dataSizeY);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPixelWorldSizeX(LengthUnit::Micrometer) == pixelWorldSizeX);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPixelWorldSizeY(LengthUnit::Micrometer) == pixelWorldSizeY);
        CHECK(tcfBFDataSet.GetMetaInfo().GetRecordedTime() == recordedTime);
        CHECK(tcfBFDataSet.GetMetaInfo().GetElapsedTime(elapsedTimeUnit) == elapsedTime);
        CHECK(tcfBFDataSet.GetMetaInfo().GetTimeFrameIndex() == timeFrameIndex);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPosition().GetPositionX(LengthUnit::Millimenter) == positionX);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPosition().GetPositionY(LengthUnit::Millimenter) == positionY);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPosition().GetPositionZ(LengthUnit::Millimenter) == positionZ);
        CHECK(tcfBFDataSet.GetMetaInfo().GetPosition().GetPositionC(LengthUnit::Millimenter) == positionC);
    }
}