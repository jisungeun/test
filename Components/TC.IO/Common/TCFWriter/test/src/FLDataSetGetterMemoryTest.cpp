#include <catch2/catch.hpp>

#include "FLDataSetGetterMemory.h"
#include "CompareArray.h"

namespace FLDataSetGetterMemoryTest {
    TEST_CASE("FLDataSetGetterMemory : unit test") {
        SECTION("FLDataSetGetterMemory()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            CHECK(&flDataSetGetterMemory != nullptr);
        }
        SECTION("SetData()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetData(nullptr, MemoryOrder3D::XYZ);
            CHECK(&flDataSetGetterMemory != nullptr);
        }
        SECTION("SetMIPData()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetMIPData(nullptr, MemoryOrder2D::XY);
            CHECK(&flDataSetGetterMemory != nullptr);
        }
        SECTION("SetDataSize()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetDataSize(1, 2, 3);
            CHECK(&flDataSetGetterMemory != nullptr);
        }
        SECTION("GetData()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const std::shared_ptr<float[]> data{ new float[numberOfElements]() };
            for (auto index = 0; index < numberOfElements; ++index) {
                data.get()[index] = static_cast<float>(index);
            }

            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetDataSize(sizeX, sizeY, sizeZ);
            flDataSetGetterMemory.SetData(data, MemoryOrder3D::XYZ);

            const auto resultData = flDataSetGetterMemory.GetData();
            CHECK(CompareArray(data.get(), resultData.get(), numberOfElements));
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1)") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const std::shared_ptr<float[]> data{ new float[numberOfElements]() };
            for (auto index = 0; index < numberOfElements; ++index) {
                data.get()[index] = static_cast<float>(index);
            }

            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetDataSize(sizeX, sizeY, sizeZ);
            flDataSetGetterMemory.SetData(data, MemoryOrder3D::YXZ);

            constexpr auto x0 = 1;
            constexpr auto x1 = 2;
            constexpr auto y0 = 1;
            constexpr auto y1 = 2;
            constexpr auto z0 = 1;
            constexpr auto z1 = 1;

            const auto resultData = flDataSetGetterMemory.GetData(x0, x1, y0, y1, z0, z1);

            constexpr auto cropSizeX = x1 - x0 + 1;
            constexpr auto cropSizeY = y1 - y0 + 1;
            constexpr auto cropSizeZ = z1 - z0 + 1;
            constexpr auto numberOfCropElements = cropSizeX * cropSizeY * cropSizeZ;

            const std::shared_ptr<float[]> answerData{ new float[numberOfCropElements]() };
            answerData.get()[0] = 17;
            answerData.get()[1] = 18;
            answerData.get()[2] = 21;
            answerData.get()[3] = 22;

            CHECK(CompareArray(answerData.get(), resultData.get(), numberOfCropElements));
        }
        SECTION("GetDataMemoryOrder()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetData(nullptr, MemoryOrder3D::YXZ);

            CHECK(flDataSetGetterMemory.GetDataMemoryOrder() == +MemoryOrder3D::YXZ);
        }
        SECTION("GetMIPData()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfMIPElements = sizeX * sizeY;

            const std::shared_ptr<float[]> mipData{ new float[numberOfMIPElements]() };
            for (auto index = 0; index < numberOfMIPElements; ++index) {
                mipData.get()[index] = static_cast<float>(index);
            }

            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetDataSize(sizeX, sizeY, sizeZ);
            flDataSetGetterMemory.SetMIPData(mipData, MemoryOrder2D::XY);

            const auto resultData = flDataSetGetterMemory.GetMIPData();
            CHECK(CompareArray(mipData.get(), resultData.get(), numberOfMIPElements));

        }
        SECTION("GetMIPData(x0,x1,y0,y1)") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfMIPElements = sizeX * sizeY;

            const std::shared_ptr<float[]> data{ new float[numberOfMIPElements]() };
            for (auto index = 0; index < numberOfMIPElements; ++index) {
                data.get()[index] = static_cast<float>(index);
            }

            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetDataSize(sizeX, sizeY, sizeZ);
            flDataSetGetterMemory.SetMIPData(data, MemoryOrder2D::YX);

            constexpr auto x0 = 1;
            constexpr auto x1 = 2;
            constexpr auto y0 = 1;
            constexpr auto y1 = 2;

            const auto resultData = flDataSetGetterMemory.GetMIPData(x0, x1, y0, y1);

            constexpr auto cropSizeX = x1 - x0 + 1;
            constexpr auto cropSizeY = y1 - y0 + 1;
            constexpr auto numberOfCropElements = cropSizeX * cropSizeY;

            const std::shared_ptr<float[]> answerData{ new float[numberOfCropElements]() };
            answerData.get()[0] = 5;
            answerData.get()[1] = 6;
            answerData.get()[2] = 9;
            answerData.get()[3] = 10;

            CHECK(CompareArray(answerData.get(), resultData.get(), numberOfCropElements));
        }
        SECTION("GetMIPDataMemoryOrder()") {
            FLDataSetGetterMemory flDataSetGetterMemory;
            flDataSetGetterMemory.SetMIPData(nullptr, MemoryOrder2D::YX);

            CHECK(flDataSetGetterMemory.GetMIPDataMemoryOrder() == +MemoryOrder2D::YX);
        }
    }

    TEST_CASE("FLDataSetGetterMemory : practical test") {
        //TODO Implement practical test with data
    }
}