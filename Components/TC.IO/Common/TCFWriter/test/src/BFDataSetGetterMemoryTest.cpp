#include <catch2/catch.hpp>

#include "BFDataSetGetterMemory.h"
#include "CompareArray.h"

namespace BFDataSetGetterMemoryTest {
    TEST_CASE("BFDataSetGetterMemory : unit test") {
        SECTION("BFDataSetGetterMemory()") {
            BFDataSetGetterMemory bfDataSetGetterMemory;
            CHECK(&bfDataSetGetterMemory != nullptr);
        }
        SECTION("SetData()") {
            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetData(nullptr, MemoryOrder3D::XYZ);
            CHECK(&bfDataSetGetterMemory != nullptr);
        }
        SECTION("SetDataSize()") {
            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetDataSize(1, 2, 3);
            CHECK(&bfDataSetGetterMemory != nullptr);
        }
        SECTION("GetData()") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto channelCount = 2;
            constexpr auto numberOfElements = sizeX * sizeY * channelCount;

            const std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            for (auto index = 0; index < numberOfElements; ++index) {
                data.get()[index] = static_cast<uint8_t>(index);
            }

            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetDataSize(sizeX, sizeY, channelCount);
            bfDataSetGetterMemory.SetData(data, MemoryOrder3D::XYZ);

            const auto resultData = bfDataSetGetterMemory.GetData();
            CHECK(CompareArray(data.get(), resultData.get(), numberOfElements));
        }
        SECTION("GetData(x0,x1,y0,y1)") {
            constexpr auto sizeX = 3;
            constexpr auto sizeY = 4;
            constexpr auto sizeZ = 2;
            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements]() };
            for (auto index = 0; index < numberOfElements; ++index) {
                data.get()[index] = static_cast<uint8_t>(index);
            }

            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetDataSize(sizeX, sizeY, sizeZ);
            bfDataSetGetterMemory.SetData(data, MemoryOrder3D::YXZ);

            constexpr auto x0 = 1;
            constexpr auto x1 = 2;
            constexpr auto y0 = 1;
            constexpr auto y1 = 2;
            constexpr auto z0 = 0;
            constexpr auto z1 = 1;

            const auto resultData = bfDataSetGetterMemory.GetData(x0, x1, y0, y1);

            constexpr auto cropSizeX = x1 - x0 + 1;
            constexpr auto cropSizeY = y1 - y0 + 1;
            constexpr auto cropSizeZ = z1 - z0 + 1;
            constexpr auto numberOfCropElements = cropSizeX * cropSizeY * cropSizeZ;

            const std::shared_ptr<uint8_t[]> answerData{ new uint8_t[numberOfCropElements]() };
            answerData.get()[0] = 5;
            answerData.get()[1] = 6;
            answerData.get()[2] = 9;
            answerData.get()[3] = 10;
            answerData.get()[4] = 17;
            answerData.get()[5] = 18;
            answerData.get()[6] = 21;
            answerData.get()[7] = 22;

            CHECK(CompareArray(answerData.get(), resultData.get(), numberOfCropElements));
        }
        SECTION("GetColorChannelCount()") {
            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetDataSize(1, 2, 3);
            CHECK(bfDataSetGetterMemory.GetColorChannelCount() == 3);
        }
        SECTION("GetDataMemoryOrder()") {
            BFDataSetGetterMemory bfDataSetGetterMemory;
            bfDataSetGetterMemory.SetData(nullptr, MemoryOrder3D::YXZ);

            CHECK(bfDataSetGetterMemory.GetDataMemoryOrder() == +MemoryOrder3D::YXZ);
        }
    }
    TEST_CASE("BFDataSetGetterMemory : practical test") {
        //TODO Implement practical test with data
    }
}