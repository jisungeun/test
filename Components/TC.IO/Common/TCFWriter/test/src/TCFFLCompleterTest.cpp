#include <catch2/catch.hpp>

#include "H5Cpp.h"

#include "TCFDataSetRecordedPosition.h"
#include "TCFFLCompleter.h"
#include "TCFFLDataSet.h"
#include "TCFFLSectionDataSetWriter.h"
#include "TCFSkeletonConfig.h"
#include "TCFSkeletonWriter.h"
#include "TCFWriterUtility.h"

using namespace TC::TCFWriter;

namespace TCFFLCompleterTest {
    TEST_CASE("TCFFLCompleter : unit test") {
        SECTION("TCFFLCompleter()") {
            TCFFLCompleter tcfFLCompleter;
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetTargetTCFFilePath()"){
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetTargetTCFFilePath("");
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetSize()"){
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetSize(1, 2, 3);
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetPixelWorldSize()"){
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetPixelWorldSize(1, 2, 3, LengthUnit::Micrometer);
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetOffsetZ()"){
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetOffsetZ(1, LengthUnit::Micrometer);
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetOffsetZCompensation()"){
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetOffsetZCompensation(1, LengthUnit::Micrometer);
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetChannelColorMap()") {
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetChannelColorMap({});
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("SetWaveLengthMap()") {
            TCFFLCompleter tcfFLCompleter;
            tcfFLCompleter.SetWaveLengthMap({});
            CHECK(&tcfFLCompleter != nullptr);
        }
        SECTION("MakeCompleteGroup()") {
            const QString targetTCFFilePath = "tcfFLCompleterTest.h5";

            TCFSkeletonConfig skeletonConfig;
            skeletonConfig.SetDataIncludingFlag(false, true, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetConfig(skeletonConfig);
            skeletonWriter.SetTargetFilePath(targetTCFFilePath);
            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto dataSizeZ = 3;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr float pixelWorldSizeZ = 0.2f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 0;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            constexpr float rawBlueData1[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
            constexpr float rawBlueMIPData1[dataSizeX * dataSizeY] = { 1,2,3,4 };

            constexpr float rawBlueData2[numberOfElements] = { 4,5,6,7,8,9,10,11,12,13,14,15 };
            constexpr float rawBlueMIPData2[dataSizeX * dataSizeY] = { 3,4,5,6 };

            constexpr float rawGreenData1[numberOfElements] = { 0,1,2,3,4,5,6,7,8,9,10,11 };
            constexpr float rawGreenMIPData1[dataSizeX * dataSizeY] = { 2,3,4,5 };

            constexpr float rawGreenData2[numberOfElements] = { 3,4,5,6,7,8,9,10,11,12,13,14 };
            constexpr float rawGreenMIPData2[dataSizeX * dataSizeY] = { 4,5,6,7 };

            constexpr auto memoryOrder = MemoryOrder3D::XYZ;
            constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

            std::shared_ptr<float[]> blueData1{ new float[numberOfElements]() };
            std::shared_ptr<float[]> blueMIPData1{ new float[numberOfElements]() };

            std::copy_n(rawBlueData1, numberOfElements, blueData1.get());
            std::copy_n(rawBlueMIPData1, dataSizeX * dataSizeY, blueMIPData1.get());

            std::shared_ptr<float[]> blueData2{ new float[numberOfElements]() };
            std::shared_ptr<float[]> blueMIPData2{ new float[numberOfElements]() };

            std::copy_n(rawBlueData2, numberOfElements, blueData2.get());
            std::copy_n(rawBlueMIPData2, dataSizeX * dataSizeY, blueMIPData2.get());

            std::shared_ptr<float[]> greenData1{ new float[numberOfElements]() };
            std::shared_ptr<float[]> greenMIPData1{ new float[numberOfElements]() };

            std::copy_n(rawGreenData1, numberOfElements, greenData1.get());
            std::copy_n(rawGreenMIPData1, dataSizeX * dataSizeY, greenMIPData1.get());

            std::shared_ptr<float[]> greenData2{ new float[numberOfElements]() };
            std::shared_ptr<float[]> greenMIPData2{ new float[numberOfElements]() };

            std::copy_n(rawGreenData2, numberOfElements, greenData2.get());
            std::copy_n(rawGreenMIPData2, dataSizeX * dataSizeY, greenMIPData2.get());

            TCFFLDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);

            TCFFLDataSet dataSetBlue1, dataSetBlue2;

            auto metaInfoBlue1 = metaInfo;
            auto metaInfoBlue2 = metaInfo;

            metaInfoBlue1.SetChannelIndex(0);
            //dataSetBlue1.SetData(blueData1, memoryOrder);
            //dataSetBlue1.SetMIPData(blueMIPData1, mipMemoryOrder);
            //TODO Implement this
            metaInfoBlue1.SetTimeFrameIndex(0);

            metaInfoBlue2.SetChannelIndex(0);
            //dataSetBlue2.SetData(blueData2, memoryOrder);
            //dataSetBlue2.SetMIPData(blueMIPData2, mipMemoryOrder);
            //TODO Implement this
            metaInfoBlue2.SetTimeFrameIndex(1);

            dataSetBlue1.SetMetaInfo(metaInfoBlue1);
            dataSetBlue2.SetMetaInfo(metaInfoBlue2);

            TCFFLDataSet dataSetGreen1, dataSetGreen2;

            auto metaInfoGreen1 = metaInfo;
            auto metaInfoGreen2 = metaInfo;

            metaInfoGreen1.SetChannelIndex(1);
            //dataSetGreen1.SetData(greenData1, memoryOrder);
            //dataSetGreen1.SetMIPData(greenMIPData1, mipMemoryOrder);
            //TODO Implement this
            metaInfoGreen1.SetTimeFrameIndex(0);

            metaInfoGreen2.SetChannelIndex(1);
            //dataSetGreen2.SetData(greenData2, memoryOrder);
            //dataSetGreen2.SetMIPData(greenMIPData2, mipMemoryOrder);
            //TODO Implement this
            metaInfoGreen2.SetTimeFrameIndex(1);

            dataSetGreen1.SetMetaInfo(metaInfoGreen1);
            dataSetGreen2.SetMetaInfo(metaInfoGreen2);

            TCFFLSectionDataSetWriter tcfFLSectionDataSetWriter;
            tcfFLSectionDataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
            tcfFLSectionDataSetWriter.SetWritingStartIndex(0, 0, 0);
            tcfFLSectionDataSetWriter.SetTargetFilePath(targetTCFFilePath);

            tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetBlue1);
            tcfFLSectionDataSetWriter.Write();
            tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetBlue2);
            tcfFLSectionDataSetWriter.Write();

            tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetGreen1);
            tcfFLSectionDataSetWriter.Write();
            tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetGreen2);
            tcfFLSectionDataSetWriter.Write();

            constexpr double offsetZ = 10;
            constexpr double offsetZCompensation = 0.1;

            QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::RGB> channelColorMap;
            channelColorMap[0] = { 1, 2, 3 };
            channelColorMap[1] = { 4, 5, 6 };
            channelColorMap[2] = { 7, 8, 9 };

            QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::EmissionExcitationWaveLength> waveLengthMap;
            waveLengthMap[0] = { 1,2 };
            waveLengthMap[1] = { 3,4 };
            waveLengthMap[2] = { 5,6 };

            QMap<TCFFLCompleter::ChannelIndex, int32_t> intensityMap;
            intensityMap[0] = 1;
            intensityMap[1] = 2;
            intensityMap[2] = 3;

            QMap<TCFFLCompleter::ChannelIndex, int32_t> exposureTimeMap;
            exposureTimeMap[0] = 1;
            exposureTimeMap[1] = 2;
            exposureTimeMap[2] = 3;

            TCFFLCompleter completer;
            completer.SetTargetTCFFilePath(targetTCFFilePath);
            completer.SetSize(dataSizeX, dataSizeY, dataSizeZ);
            completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            completer.SetOffsetZ(offsetZ, LengthUnit::Micrometer);
            completer.SetOffsetZCompensation(offsetZCompensation, LengthUnit::Micrometer);
            completer.SetChannelColorMap(channelColorMap);
            completer.SetIntensityMap(intensityMap);
            completer.SetExposureTimeMap(exposureTimeMap);
            completer.SetWaveLengthMap(waveLengthMap);

            const auto completionResult = completer.MakeCompleteGroup();
            CHECK(completionResult == true);
        }
    }
    TEST_CASE("TCFFLCompleter : practical test") {
        const QString targetTCFFilePath = "tcfFLCompleterTest.h5";

        TCFSkeletonConfig skeletonConfig;
        skeletonConfig.SetDataIncludingFlag(false, true, false);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetConfig(skeletonConfig);
        skeletonWriter.SetTargetFilePath(targetTCFFilePath);
        skeletonWriter.Write();

        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto dataSizeZ = 3;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr float pixelWorldSizeZ = 0.2f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 0;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        constexpr float rawBlueData1[numberOfElements] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
        constexpr float rawBlueMIPData1[dataSizeX * dataSizeY] = { 1,2,3,4 };

        constexpr float rawBlueData2[numberOfElements] = { 4,5,6,7,8,9,10,11,12,13,14,15 };
        constexpr float rawBlueMIPData2[dataSizeX * dataSizeY] = { 3,4,5,6 };

        constexpr float rawGreenData1[numberOfElements] = { 0,1,2,3,4,5,6,7,8,9,10,11 };
        constexpr float rawGreenMIPData1[dataSizeX * dataSizeY] = { 2,3,4,5 };

        constexpr float rawGreenData2[numberOfElements] = { 3,4,5,6,7,8,9,10,11,12,13,14 };
        constexpr float rawGreenMIPData2[dataSizeX * dataSizeY] = { 4,5,6,7 };

        constexpr auto minValue3D = 0.f;
        constexpr auto maxValue3D = 15.f;

        constexpr auto minValue2DMIP = 1.f;
        constexpr auto maxValue2DMIP = 7.f;

        constexpr auto memoryOrder = MemoryOrder3D::XYZ;
        constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

        std::shared_ptr<float[]> blueData1{ new float[numberOfElements]() };
        std::shared_ptr<float[]> blueMIPData1{ new float[numberOfElements]() };

        std::copy_n(rawBlueData1, numberOfElements, blueData1.get());
        std::copy_n(rawBlueMIPData1, dataSizeX * dataSizeY, blueMIPData1.get());

        std::shared_ptr<float[]> blueData2{ new float[numberOfElements]() };
        std::shared_ptr<float[]> blueMIPData2{ new float[numberOfElements]() };

        std::copy_n(rawBlueData2, numberOfElements, blueData2.get());
        std::copy_n(rawBlueMIPData2, dataSizeX * dataSizeY, blueMIPData2.get());

        std::shared_ptr<float[]> greenData1{ new float[numberOfElements]() };
        std::shared_ptr<float[]> greenMIPData1{ new float[numberOfElements]() };

        std::copy_n(rawGreenData1, numberOfElements, greenData1.get());
        std::copy_n(rawGreenMIPData1, dataSizeX * dataSizeY, greenMIPData1.get());

        std::shared_ptr<float[]> greenData2{ new float[numberOfElements]() };
        std::shared_ptr<float[]> greenMIPData2{ new float[numberOfElements]() };

        std::copy_n(rawGreenData2, numberOfElements, greenData2.get());
        std::copy_n(rawGreenMIPData2, dataSizeX * dataSizeY, greenMIPData2.get());

        TCFFLDataSetMetaInfo metaInfo;
        metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetTimeFrameIndex(timeFrameIndex);
        metaInfo.SetPosition(position);

        TCFFLDataSet dataSetBlue1, dataSetBlue2;

        auto metaInfoBlue1 = metaInfo;
        auto metaInfoBlue2 = metaInfo;

        metaInfoBlue1.SetChannelIndex(0);
        //dataSetBlue1.SetData(blueData1, memoryOrder);
        //dataSetBlue1.SetMIPData(blueMIPData1, mipMemoryOrder);
        //TODO Implement this
        metaInfoBlue1.SetTimeFrameIndex(0);

        metaInfoBlue2.SetChannelIndex(0);
        //dataSetBlue2.SetData(blueData2, memoryOrder);
        //dataSetBlue2.SetMIPData(blueMIPData2, mipMemoryOrder);
        //TODO Implement this
        metaInfoBlue2.SetTimeFrameIndex(1);

        dataSetBlue1.SetMetaInfo(metaInfoBlue1);
        dataSetBlue2.SetMetaInfo(metaInfoBlue2);

        TCFFLDataSet dataSetGreen1, dataSetGreen2;

        auto metaInfoGreen1 = metaInfo;
        auto metaInfoGreen2 = metaInfo;

        metaInfoGreen1.SetChannelIndex(1);
        //dataSetGreen1.SetData(greenData1, memoryOrder);
        //dataSetGreen1.SetMIPData(greenMIPData1, mipMemoryOrder);
        //TODO Implement this
        metaInfoGreen1.SetTimeFrameIndex(0);

        metaInfoGreen2.SetChannelIndex(1);
        //dataSetGreen2.SetData(greenData2, memoryOrder);
        //dataSetGreen2.SetMIPData(greenMIPData2, mipMemoryOrder);
        //TODO Implement this
        metaInfoGreen2.SetTimeFrameIndex(1);

        dataSetGreen1.SetMetaInfo(metaInfoGreen1);
        dataSetGreen2.SetMetaInfo(metaInfoGreen2);

        TCFFLSectionDataSetWriter tcfFLSectionDataSetWriter;
        tcfFLSectionDataSetWriter.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
        tcfFLSectionDataSetWriter.SetWritingStartIndex(0, 0, 0);
        tcfFLSectionDataSetWriter.SetTargetFilePath(targetTCFFilePath);

        tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetBlue1);
        tcfFLSectionDataSetWriter.Write();
        tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetBlue2);
        tcfFLSectionDataSetWriter.Write();

        tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetGreen1);
        tcfFLSectionDataSetWriter.Write();
        tcfFLSectionDataSetWriter.SetTCFFLDataSet(dataSetGreen2);
        tcfFLSectionDataSetWriter.Write();

        constexpr double offsetZ = 10;
        constexpr double offsetZCompensation = 0.1;

        QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::RGB> channelColorMap;
        channelColorMap[0] = { 1, 2, 3 };
        channelColorMap[1] = { 4, 5, 6 };
        channelColorMap[2] = { 7, 8, 9 };

        QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::EmissionExcitationWaveLength> waveLengthMap;
        waveLengthMap[0] = { 1,2 };
        waveLengthMap[1] = { 3,4 };
        waveLengthMap[2] = { 5,6 };

        QMap<TCFFLCompleter::ChannelIndex, int32_t> intensityMap;
        intensityMap[0] = 1;
        intensityMap[1] = 2;
        intensityMap[2] = 3;

        QMap<TCFFLCompleter::ChannelIndex, int32_t> exposureTimeMap;
        exposureTimeMap[0] = 1;
        exposureTimeMap[1] = 2;
        exposureTimeMap[2] = 3;

        TCFFLCompleter completer;
        completer.SetTargetTCFFilePath(targetTCFFilePath);
        completer.SetSize(dataSizeX, dataSizeY, dataSizeZ);
        completer.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        completer.SetOffsetZ(offsetZ, LengthUnit::Micrometer);
        completer.SetOffsetZCompensation(offsetZCompensation, LengthUnit::Micrometer);
        completer.SetChannelColorMap(channelColorMap);
        completer.SetIntensityMap(intensityMap);
        completer.SetExposureTimeMap(exposureTimeMap);
        completer.SetWaveLengthMap(waveLengthMap);

        completer.MakeCompleteGroup();


        const H5::H5File file(targetTCFFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto data3dFLGroup = file.openGroup("/Data/3DFL");
        const auto data3dFLCH0Group = data3dFLGroup.openGroup("CH0");
        const auto data3dFLCH1Group = data3dFLGroup.openGroup("CH1");
        const auto data2dFLMIPGroup = file.openGroup("/Data/2DFLMIP");
        const auto data2dFLCH0MIPGroup = data2dFLMIPGroup.openGroup("CH0");
        const auto data2dFLCH1MIPGroup = data2dFLMIPGroup.openGroup("CH1");

        CHECK(ReadAttributeInt64(data3dFLGroup, "Channels") == 2);
        CHECK(ReadAttributeInt64(data3dFLGroup, "DataCount") == 2);
        CHECK(ReadAttributeDouble(data3dFLGroup, "OffsetZ") == offsetZ);
        CHECK(ReadAttributeDouble(data3dFLGroup, "OffsetZCompensation") == offsetZCompensation);
        CHECK(ReadAttributeDouble(data3dFLGroup, "ResolutionX") == pixelWorldSizeX);
        CHECK(ReadAttributeDouble(data3dFLGroup, "ResolutionY") == pixelWorldSizeY);
        CHECK(ReadAttributeDouble(data3dFLGroup, "ResolutionZ") == pixelWorldSizeZ);
        CHECK(ReadAttributeDouble(data3dFLGroup, "MinIntensity") == minValue3D);
        CHECK(ReadAttributeDouble(data3dFLGroup, "MaxIntensity") == maxValue3D);
        CHECK(ReadAttributeInt64(data3dFLGroup, "SizeX") == dataSizeX);
        CHECK(ReadAttributeInt64(data3dFLGroup, "SizeY") == dataSizeY);
        CHECK(ReadAttributeInt64(data3dFLGroup, "SizeZ") == dataSizeZ);
        CHECK(ReadAttributeUint8(data3dFLCH0Group, "ColorR") == 1);
        CHECK(ReadAttributeUint8(data3dFLCH0Group, "ColorG") == 2);
        CHECK(ReadAttributeUint8(data3dFLCH0Group, "ColorB") == 3);
        CHECK(ReadAttributeUint8(data3dFLCH1Group, "ColorR") == 4);
        CHECK(ReadAttributeUint8(data3dFLCH1Group, "ColorG") == 5);
        CHECK(ReadAttributeUint8(data3dFLCH1Group, "ColorB") == 6);
        CHECK(ReadAttributeDouble(data3dFLCH0Group, "Emission") == 1);
        CHECK(ReadAttributeDouble(data3dFLCH0Group, "Excitation") == 2);
        CHECK(ReadAttributeDouble(data3dFLCH1Group, "Emission") == 3);
        CHECK(ReadAttributeDouble(data3dFLCH1Group, "Excitation") == 4);
        CHECK(ReadAttributeInt64(data3dFLCH0Group, "Intensity") == 1);
        CHECK(ReadAttributeInt64(data3dFLCH1Group, "Intensity") == 2);
        CHECK(ReadAttributeInt64(data3dFLCH0Group, "ExposureTime") == 1);
        CHECK(ReadAttributeInt64(data3dFLCH1Group, "ExposureTime") == 2);
        
        CHECK(ReadAttributeInt64(data2dFLMIPGroup, "Channels") == 2);
        CHECK(ReadAttributeInt64(data2dFLMIPGroup, "DataCount") == 2);
        CHECK(ReadAttributeDouble(data2dFLMIPGroup, "ResolutionX") == pixelWorldSizeX);
        CHECK(ReadAttributeDouble(data2dFLMIPGroup, "ResolutionY") == pixelWorldSizeY);
        CHECK(ReadAttributeDouble(data2dFLMIPGroup, "MinIntensity") == minValue2DMIP);
        CHECK(ReadAttributeDouble(data2dFLMIPGroup, "MaxIntensity") == maxValue2DMIP);
        CHECK(ReadAttributeInt64(data2dFLMIPGroup, "SizeX") == dataSizeX);
        CHECK(ReadAttributeInt64(data2dFLMIPGroup, "SizeY") == dataSizeY);
        CHECK(ReadAttributeUint8(data2dFLCH0MIPGroup, "ColorR") == 1);
        CHECK(ReadAttributeUint8(data2dFLCH0MIPGroup, "ColorG") == 2);
        CHECK(ReadAttributeUint8(data2dFLCH0MIPGroup, "ColorB") == 3);
        CHECK(ReadAttributeUint8(data2dFLCH1MIPGroup, "ColorR") == 4);
        CHECK(ReadAttributeUint8(data2dFLCH1MIPGroup, "ColorG") == 5);
        CHECK(ReadAttributeUint8(data2dFLCH1MIPGroup, "ColorB") == 6);
        CHECK(ReadAttributeDouble(data2dFLCH0MIPGroup, "Emission") == 1);
        CHECK(ReadAttributeDouble(data2dFLCH0MIPGroup, "Excitation") == 2);
        CHECK(ReadAttributeDouble(data2dFLCH1MIPGroup, "Emission") == 3);
        CHECK(ReadAttributeDouble(data2dFLCH1MIPGroup, "Excitation") == 4);
        CHECK(ReadAttributeInt64(data2dFLCH0MIPGroup, "Intensity") == 1);
        CHECK(ReadAttributeInt64(data2dFLCH1MIPGroup, "Intensity") == 2);
        CHECK(ReadAttributeInt64(data2dFLCH0MIPGroup, "ExposureTime") == 1);
        CHECK(ReadAttributeInt64(data2dFLCH1MIPGroup, "ExposureTime") == 2);
        
    }
}
