#include <catch2/catch.hpp>

#include "TCFCommonInfo.h"
#include "TCFWriterUtility.h"

namespace TCFCommonInfoTest {
    TEST_CASE("TCFCommonInfo : unit test") {
        SECTION("TCFCommonInfo()") {
            TCFCommonInfo tcfCommonInfo;
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("TCFCommonInfo(other)") {
            TCFCommonInfo srcTCFCommonInfo;
            srcTCFCommonInfo.SetTitle("test");

            TCFCommonInfo destTCFCommonInfo{ srcTCFCommonInfo };
            CHECK(destTCFCommonInfo.GetTitle() == "test");
        }
        SECTION("operator=()") {
            TCFCommonInfo srcTCFCommonInfo;
            srcTCFCommonInfo.SetTitle("test");

            TCFCommonInfo destTCFCommonInfo;
            destTCFCommonInfo = srcTCFCommonInfo;
            CHECK(destTCFCommonInfo.GetTitle() == "test");
        }
        SECTION("SetCreateDate()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetCreateDate({});
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetCreateDate()") {
            const auto createDate = QDateTime::currentDateTime();

            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetCreateDate(createDate);
            CHECK(tcfCommonInfo.GetCreateDate() == createDate);
        }
        SECTION("SetDataID()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDataID("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDataID()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDataID("dataID");
            CHECK(tcfCommonInfo.GetDataID() == "dataID");
        }
        SECTION("SetDescription()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDescription("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDescription()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDescription("description");
            CHECK(tcfCommonInfo.GetDescription() == "description");
        }
        SECTION("SetDeviceHost()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceHost("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDeviceHost()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceHost("deviceHost");
            CHECK(tcfCommonInfo.GetDeviceHost() == "deviceHost");
        }
        SECTION("SetDeviceSerial()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceSerial("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDeviceSerial()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceSerial("deviceSerial");
            CHECK(tcfCommonInfo.GetDeviceSerial() == "deviceSerial");
        }
        SECTION("SetDeviceModelType()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceModelType("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDeviceModelType()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceModelType("deviceModelType");
            CHECK(tcfCommonInfo.GetDeviceModelType() == "deviceModelType");
        }
        SECTION("SetDeviceSoftwareVersion()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceSoftwareVersion("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetDeviceSoftwareVersion()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetDeviceSoftwareVersion("deviceSoftwareVersion");
            CHECK(tcfCommonInfo.GetDeviceSoftwareVersion() == "deviceSoftwareVersion");
        }
        SECTION("SetFormatVersion()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetFormatVersion("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetFormatVersion()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetFormatVersion("formatVersion");
            CHECK(tcfCommonInfo.GetFormatVersion() == "formatVersion");
        }
        SECTION("SetRecordingTime()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetRecordingTime({});
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetRecordingTime()") {
            const auto recordingTime = QDateTime::currentDateTime();

            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetRecordingTime(recordingTime);
            CHECK(tcfCommonInfo.GetRecordingTime() == recordingTime);
        }
        SECTION("SetSoftwareVersion()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetSoftwareVersion("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetSoftwareVersion()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetSoftwareVersion("softwareVersion");
            CHECK(tcfCommonInfo.GetSoftwareVersion() == "softwareVersion");
        }
        SECTION("SetTitle()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetTitle("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetTitle()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetTitle("title");
            CHECK(tcfCommonInfo.GetTitle() == "title");
        }
        SECTION("SetUniqueID()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetUniqueID("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetUniqueID()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetUniqueID("uniqueID");
            CHECK(tcfCommonInfo.GetUniqueID() == "uniqueID");
        }
        SECTION("SetUserID()"){
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetUserID("");
            CHECK(&tcfCommonInfo != nullptr);
        }
        SECTION("GetUserID()") {
            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetUserID("userID");
            CHECK(tcfCommonInfo.GetUserID() == "userID");
        }
    }
    TEST_CASE("TCFCommonInfo : practical test") {
        const QString deviceName = "CSN19472093";
        const auto dataRecordedDateTime = QDateTime::currentDateTime();
        const auto dataWritingDateTime = QDateTime::currentDateTime();

        const auto createDate = QDateTime::currentDateTime();
        const auto dataID = GenerateDataID(deviceName, dataRecordedDateTime);
        const QString description = "";
        const QString deviceHost = "TomoPC-018";
        const QString deviceSerial = deviceName;
        const QString deviceModelType = "HTX";
        const QString deviceSoftwareVersion = "3.3.0";
        const QString formatVersion = "1.4.0";
        const auto recordingTime = dataRecordedDateTime;
        const QString softwareVersion = "ProcessingServer 0.1.0";
        const QString title = "default test";
        const auto uniqueID = GenerateUniqueID(deviceName, dataRecordedDateTime, dataWritingDateTime);
        const auto userID = "Johnny";

        TCFCommonInfo tcfCommonInfo;
        tcfCommonInfo.SetCreateDate(createDate);
        tcfCommonInfo.SetDataID(dataID);
        tcfCommonInfo.SetDescription(description);
        tcfCommonInfo.SetDeviceHost(deviceHost);
        tcfCommonInfo.SetDeviceSerial(deviceSerial);
        tcfCommonInfo.SetDeviceModelType(deviceModelType);
        tcfCommonInfo.SetDeviceSoftwareVersion(deviceSoftwareVersion);
        tcfCommonInfo.SetFormatVersion(formatVersion);
        tcfCommonInfo.SetRecordingTime(recordingTime);
        tcfCommonInfo.SetSoftwareVersion(softwareVersion);
        tcfCommonInfo.SetTitle(title);
        tcfCommonInfo.SetUniqueID(uniqueID);
        tcfCommonInfo.SetUserID(userID);

        CHECK(tcfCommonInfo.GetCreateDate() == createDate);
        CHECK(tcfCommonInfo.GetDataID() == dataID);
        CHECK(tcfCommonInfo.GetDescription() == description);
        CHECK(tcfCommonInfo.GetDeviceHost() == deviceHost);
        CHECK(tcfCommonInfo.GetDeviceSerial() == deviceSerial);
        CHECK(tcfCommonInfo.GetDeviceModelType() == deviceModelType);
        CHECK(tcfCommonInfo.GetDeviceSoftwareVersion() == deviceSoftwareVersion);
        CHECK(tcfCommonInfo.GetFormatVersion() == formatVersion);
        CHECK(tcfCommonInfo.GetRecordingTime() == recordingTime);
        CHECK(tcfCommonInfo.GetSoftwareVersion() == softwareVersion);
        CHECK(tcfCommonInfo.GetTitle() == title);
        CHECK(tcfCommonInfo.GetUniqueID() == uniqueID);
        CHECK(tcfCommonInfo.GetUserID() == userID);
    }
}
