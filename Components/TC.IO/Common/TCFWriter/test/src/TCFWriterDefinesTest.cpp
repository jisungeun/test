#include <catch2/catch.hpp>

#include "TCFWriterDefines.h"

namespace TCFWriterDefinesTest {
    TEST_CASE("TCFWriterDefines") {
        SECTION("MemoryOrder3D") {
            CHECK(MemoryOrder3D::_size() == 4);
            CHECK((+MemoryOrder3D::XYZ)._to_index() == 0);
            CHECK((+MemoryOrder3D::YXZ)._to_index() == 1);
            CHECK((+MemoryOrder3D::ZXY)._to_index() == 2);
            CHECK((+MemoryOrder3D::ZYX)._to_index() == 3);
        }
        SECTION("MemoryOrder2D") {
            CHECK(MemoryOrder2D::_size() == 2);
            CHECK((+MemoryOrder2D::XY)._to_index() == 0);
            CHECK((+MemoryOrder2D::YX)._to_index() == 1);
        }
    }
}