#include <catch2/catch.hpp>

#include "TCFHTDataSet.h"

namespace TCFHTDataSetTest {
    using namespace TC::TCFWriter;

    class HTDataSetGetterForTest final : public IHTDataSetGetter {
    public:
        HTDataSetGetterForTest() = default;
        ~HTDataSetGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
            const int32_t& z1) const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetDataMemoryOrder() const -> MemoryOrder3D override {
            return MemoryOrder3D::XYZ;
        }
        auto GetMIPData() const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
            -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }

        auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override {
            return MemoryOrder2D::XY;
        }
    };

    TEST_CASE("TCFHTDataSet : unit test") {
        SECTION("TCFHTDataSet()") {
            TCFHTDataSet tcfHTDataSet;
            CHECK(&tcfHTDataSet != nullptr);
        }
        SECTION("TCFHTDataSet(other)") {
            TCFHTDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(1, 2, 3);

            TCFHTDataSet srcTCFHTDataSet;
            srcTCFHTDataSet.SetMetaInfo(metaInfo);

            TCFHTDataSet destTCFHTDataSet(srcTCFHTDataSet);
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeX() == 1);
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeY() == 2);
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeZ() == 3);
        }
        SECTION("operator=()") {
            TCFHTDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(1, 2, 3);

            TCFHTDataSet srcTCFHTDataSet;
            srcTCFHTDataSet.SetMetaInfo(metaInfo);

            TCFHTDataSet destTCFHTDataSet;
            destTCFHTDataSet = srcTCFHTDataSet;
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeX() == 1);
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeY() == 2);
            CHECK(destTCFHTDataSet.GetMetaInfo().GetSizeZ() == 3);
        }
        SECTION("SetDataSetGetter()") {
            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter({});
            CHECK(&tcfHTDataSet != nullptr);
        }
        SECTION("GetData()") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };
            
            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);

            const auto resultData = tcfHTDataSet.GetData();
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);

            const auto resultData = tcfHTDataSet.GetData(0, 1, 2, 3, 4, 5);
            CHECK(resultData.get()[0] == 1);
        }

        SECTION("GetDataMemoryOrder()") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);

            CHECK(tcfHTDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        }
        SECTION("GetMIPData()") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);

            const auto resultData = tcfHTDataSet.GetMIPData();
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetMIPData(x0,x1,y0,y1)") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);

            const auto resultData = tcfHTDataSet.GetMIPData(0,1,2,3);
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetMIPDataMemoryOrder()") {
            auto htDataSetGetter = new HTDataSetGetterForTest;
            IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);
            CHECK(tcfHTDataSet.GetMIPDataMemoryOrder() == +MemoryOrder2D::XY);
        }
    }
    TEST_CASE("TCFHTDataSet : practical test") {
        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto dataSizeZ = 3;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr float pixelWorldSizeZ = 0.2f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 0;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        auto htDataSetGetter = new HTDataSetGetterForTest;
        IHTDataSetGetter::Pointer htDataSetGetterPointer{ htDataSetGetter };

        TCFHTDataSetMetaInfo metaInfo;
        metaInfo.SetDataMinMaxValue(1, 2);
        metaInfo.SetMIPDataMinMaxValue(1, 2);
        metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetTimeFrameIndex(timeFrameIndex);
        metaInfo.SetPosition(position);

        TCFHTDataSet tcfHTDataSet;
        tcfHTDataSet.SetDataSetGetter(htDataSetGetterPointer);
        tcfHTDataSet.SetMetaInfo(metaInfo);

        CHECK(tcfHTDataSet.GetData().get()[0] == 1);
        CHECK(tcfHTDataSet.GetData(0, 1, 2, 3, 4, 5).get()[0] == 1);
        CHECK(tcfHTDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        CHECK(tcfHTDataSet.GetMetaInfo().GetDataMinValue() == 1);
        CHECK(tcfHTDataSet.GetMetaInfo().GetDataMaxValue() == 2);
        CHECK(tcfHTDataSet.GetMIPData().get()[0] == 1);
        CHECK(tcfHTDataSet.GetMIPData(0, 1, 2, 3).get()[0] == 1);
        CHECK(tcfHTDataSet.GetMIPDataMemoryOrder() == +MemoryOrder2D::XY);
        CHECK(tcfHTDataSet.GetMetaInfo().GetDataMIPMinValue() == 1);
        CHECK(tcfHTDataSet.GetMetaInfo().GetDataMIPMaxValue() == 2);
        CHECK(tcfHTDataSet.GetMetaInfo().GetSizeX() == dataSizeX);
        CHECK(tcfHTDataSet.GetMetaInfo().GetSizeY() == dataSizeY);
        CHECK(tcfHTDataSet.GetMetaInfo().GetSizeZ() == dataSizeZ);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPixelWorldSizeX(LengthUnit::Micrometer) == pixelWorldSizeX);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPixelWorldSizeY(LengthUnit::Micrometer) == pixelWorldSizeY);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPixelWorldSizeZ(LengthUnit::Micrometer) == pixelWorldSizeZ);
        CHECK(tcfHTDataSet.GetMetaInfo().GetRecordedTime() == recordedTime);
        CHECK(tcfHTDataSet.GetMetaInfo().GetElapsedTime(elapsedTimeUnit) == elapsedTime);
        CHECK(tcfHTDataSet.GetMetaInfo().GetTimeFrameIndex() == timeFrameIndex);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPosition().GetPositionX(LengthUnit::Millimenter) == positionX);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPosition().GetPositionY(LengthUnit::Millimenter) == positionY);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPosition().GetPositionZ(LengthUnit::Millimenter) == positionZ);
        CHECK(tcfHTDataSet.GetMetaInfo().GetPosition().GetPositionC(LengthUnit::Millimenter) == positionC);
    }
}