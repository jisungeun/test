#include <catch2/catch.hpp>

#include "IFLDataSetGetter.h"

namespace IFLDataSetGetterTest {
    class FLDataSetGetterForTest final : public IFLDataSetGetter {
    public:
        FLDataSetGetterForTest() = default;
        ~FLDataSetGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            this->getDataTriggered = true;
            return nullptr;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
            const int32_t& z1) const -> std::shared_ptr<float[]> override {
            this->getDataXYZTriggered = true;
            return nullptr;
        }
        auto GetDataMemoryOrder() const->MemoryOrder3D override {
            this->getDataMemoryOrderTriggered = true;
            return MemoryOrder3D::XYZ;
        }
        auto GetMIPData() const -> std::shared_ptr<float[]> override {
            this->getMIPDataTriggered = true;
            return nullptr;
        }
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1)
            const -> std::shared_ptr<float[]> override {
            this->getMIPDataXYZTriggered = true;
            return nullptr;
        }
        auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override {
            this->getMIPDataMemoryOrderTriggered = true;
            return MemoryOrder2D::XY;
        }

        mutable bool getDataTriggered{ false };
        mutable bool getDataXYZTriggered{ false };
        mutable bool getDataMemoryOrderTriggered{ false };

        mutable bool getMIPDataTriggered{ false };
        mutable bool getMIPDataXYZTriggered{ false };
        mutable bool getMIPDataMemoryOrderTriggered{ false };
    };

    TEST_CASE("IFLDataSetGetter : unit test") {
        SECTION("GetData()") {
            FLDataSetGetterForTest flDataSetGetterForTest;

            CHECK(flDataSetGetterForTest.getDataTriggered == false);
            flDataSetGetterForTest.GetData();
            CHECK(flDataSetGetterForTest.getDataTriggered == true);
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1)") {
            FLDataSetGetterForTest flDataSetGetterForTest;
            CHECK(flDataSetGetterForTest.getDataXYZTriggered == false);
            flDataSetGetterForTest.GetData(0, 1, 2, 3, 4, 5);
            CHECK(flDataSetGetterForTest.getDataXYZTriggered == true);
        }
        SECTION("GetDataMemoryOrder()") {
            FLDataSetGetterForTest flDataSetGetterForTest;
            CHECK(flDataSetGetterForTest.getDataMemoryOrderTriggered == false);
            flDataSetGetterForTest.GetDataMemoryOrder();
            CHECK(flDataSetGetterForTest.getDataMemoryOrderTriggered == true);
        }
        SECTION("GetMIPData()") {
            FLDataSetGetterForTest flDataSetGetterForTest;
            CHECK(flDataSetGetterForTest.getMIPDataTriggered == false);
            flDataSetGetterForTest.GetMIPData();
            CHECK(flDataSetGetterForTest.getMIPDataTriggered == true);
        }
        SECTION("GetMIPData(x0,x1,y0,y1)") {
            FLDataSetGetterForTest flDataSetGetterForTest;
            CHECK(flDataSetGetterForTest.getMIPDataXYZTriggered == false);
            flDataSetGetterForTest.GetMIPData(0, 1, 2, 3);
            CHECK(flDataSetGetterForTest.getMIPDataXYZTriggered == true);
        }
        SECTION("GetMIPDataMemoryOrder()") {
            FLDataSetGetterForTest flDataSetGetterForTest;
            CHECK(flDataSetGetterForTest.getMIPDataMemoryOrderTriggered == false);
            flDataSetGetterForTest.GetMIPDataMemoryOrder();
            CHECK(flDataSetGetterForTest.getMIPDataMemoryOrderTriggered == true);
        }
    }
}
