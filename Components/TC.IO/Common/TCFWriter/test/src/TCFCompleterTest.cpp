#include <catch2/catch.hpp>

#include "H5Cpp.h"
#include "TCFCompleter.h"
#include "TCFSkeletonWriter.h"
#include "TCFWriterUtility.h"

namespace TCFCompleterTest {
    TEST_CASE("TCFCompleter : unit test") {
        SECTION("TCFCompleter()") {
            TCFCompleter completer;
            CHECK(&completer != nullptr);
        }
        SECTION("SetTargetTCFFilePath()") {
            TCFCompleter completer;
            completer.SetTargetTCFFilePath("");
            CHECK(&completer != nullptr);
        }
        SECTION("SetTCFCommonInfo()") {
            TCFCompleter completer;
            completer.SetTCFCommonInfo({});
            CHECK(&completer != nullptr);
        }
        SECTION("SetTCFDeviceInfo()") {
            TCFCompleter completer;
            completer.SetTCFDeviceInfo({});
            CHECK(&completer != nullptr);
        }
        SECTION("SetTCFTileInfo()") {
            TCFCompleter completer;
            completer.SetTCFTileInfo({});
            CHECK(&completer != nullptr);
        }
        SECTION("MakeTCFComplete()") {
            const QString targetTCFFilePath = "tcfCompleterTest.h5";

            TCFSkeletonConfig skeletonConfig;
            skeletonConfig.SetDataIncludingFlag(false, false, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetConfig(skeletonConfig);
            skeletonWriter.SetTargetFilePath(targetTCFFilePath);
            skeletonWriter.Write();

            const QString deviceName = "CSN19472093";
            const auto dataRecordedDateTime = QDateTime::currentDateTime();
            const auto dataWritingDateTime = QDateTime::currentDateTime();

            const auto createDate = QDateTime::currentDateTime();
            const auto dataID = GenerateDataID(deviceName, dataRecordedDateTime);
            const QString description = "";
            const QString deviceHost = "TomoPC-018";
            const QString deviceSerial = deviceName;
            const QString deviceModelType = "HTX";
            const QString deviceSoftwareVersion = "3.3.0";
            const QString formatVersion = "1.4.0";
            const auto recordingTime = dataRecordedDateTime;
            const QString softwareVersion = "ProcessingServer 0.1.0";
            const QString title = "default test";
            const auto uniqueID = GenerateUniqueID(deviceName, dataRecordedDateTime, dataWritingDateTime);
            const auto userID = "Johnny";

            TCFCommonInfo tcfCommonInfo;
            tcfCommonInfo.SetCreateDate(createDate);
            tcfCommonInfo.SetDataID(dataID);
            tcfCommonInfo.SetDescription(description);
            tcfCommonInfo.SetDeviceHost(deviceHost);
            tcfCommonInfo.SetDeviceSerial(deviceSerial);
            tcfCommonInfo.SetDeviceModelType(deviceModelType);
            tcfCommonInfo.SetDeviceSoftwareVersion(deviceSoftwareVersion);
            tcfCommonInfo.SetFormatVersion(formatVersion);
            tcfCommonInfo.SetRecordingTime(recordingTime);
            tcfCommonInfo.SetSoftwareVersion(softwareVersion);
            tcfCommonInfo.SetTitle(title);
            tcfCommonInfo.SetUniqueID(uniqueID);
            tcfCommonInfo.SetUserID(userID);

            constexpr double magnification = 40;
            constexpr double na = 1.2;
            constexpr double ri = 1.337;

            TCFDeviceInfo tcfDeviceInfo;
            tcfDeviceInfo.SetMagnification(magnification);
            tcfDeviceInfo.SetNA(na);
            tcfDeviceInfo.SetRI(ri);

            constexpr double overlapLengthH = 15;
            constexpr double overlapLengthV = 15;
            constexpr auto overlapLengthUnit = LengthUnit::Micrometer;

            constexpr double positionX = 1.2;
            constexpr double positionY = 2.1;
            constexpr double positionZ = 3.1;
            constexpr double positionC = 0.2;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, LengthUnit::Millimenter);

            constexpr int32_t tileNumberH = 3;
            constexpr int32_t tileNumberV = 3;

            constexpr int32_t tileIndexColumn = 1;
            constexpr int32_t tileIndexRow = 2;

            TCFTileInfo tcfTileInfo;
            tcfTileInfo.SetOverlapLength(overlapLengthH, overlapLengthV, overlapLengthUnit);
            tcfTileInfo.SetTCFDataSetRecordedPosition(position);
            tcfTileInfo.SetTileNumber(tileNumberH, tileNumberV);
            tcfTileInfo.SetTileIndex(tileIndexColumn, tileIndexRow);
            

            TCFCompleter completer;
            completer.SetTargetTCFFilePath(targetTCFFilePath);
            completer.SetTCFCommonInfo(tcfCommonInfo);
            completer.SetTCFDeviceInfo(tcfDeviceInfo);
            completer.SetTCFTileInfo(tcfTileInfo);

            const auto result = completer.MakeTCFComplete();
            CHECK(result == true);
        }
    }
    TEST_CASE("TCFCompleter : practical test") {
        const QString targetTCFFilePath = "tcfCompleterTest.h5";

        TCFSkeletonConfig skeletonConfig;
        skeletonConfig.SetDataIncludingFlag(false, false, false);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetConfig(skeletonConfig);
        skeletonWriter.SetTargetFilePath(targetTCFFilePath);
        skeletonWriter.Write();

        const QString deviceName = "CSN19472093";
        const auto dataRecordedDateTime = QDateTime::currentDateTime();
        const auto dataWritingDateTime = QDateTime::currentDateTime();

        const auto createDate = QDateTime::currentDateTime();
        const auto dataID = GenerateDataID(deviceName, dataRecordedDateTime);
        const QString description = "";
        const QString deviceHost = "TomoPC-018";
        const QString deviceSerial = deviceName;
        const QString deviceModelType = "HTX";
        const QString deviceSoftwareVersion = "3.3.0";
        const QString formatVersion = "1.4.0";
        const auto recordingTime = dataRecordedDateTime;
        const QString softwareVersion = "ProcessingServer 0.1.0";
        const QString title = "default test";
        const auto uniqueID = GenerateUniqueID(deviceName, dataRecordedDateTime, dataWritingDateTime);
        const auto userID = "Johnny";

        TCFCommonInfo tcfCommonInfo;
        tcfCommonInfo.SetCreateDate(createDate);
        tcfCommonInfo.SetDataID(dataID);
        tcfCommonInfo.SetDescription(description);
        tcfCommonInfo.SetDeviceHost(deviceHost);
        tcfCommonInfo.SetDeviceSerial(deviceSerial);
        tcfCommonInfo.SetDeviceModelType(deviceModelType);
        tcfCommonInfo.SetDeviceSoftwareVersion(deviceSoftwareVersion);
        tcfCommonInfo.SetFormatVersion(formatVersion);
        tcfCommonInfo.SetRecordingTime(recordingTime);
        tcfCommonInfo.SetSoftwareVersion(softwareVersion);
        tcfCommonInfo.SetTitle(title);
        tcfCommonInfo.SetUniqueID(uniqueID);
        tcfCommonInfo.SetUserID(userID);

        constexpr double magnification = 40;
        constexpr double na = 1.2;
        constexpr double ri = 1.337;

        TCFDeviceInfo tcfDeviceInfo;
        tcfDeviceInfo.SetMagnification(magnification);
        tcfDeviceInfo.SetNA(na);
        tcfDeviceInfo.SetRI(ri);

        constexpr double overlapLengthH = 15;
        constexpr double overlapLengthV = 15;
        constexpr auto overlapLengthUnit = LengthUnit::Micrometer;

        constexpr double positionX = 1.2;
        constexpr double positionY = 2.1;
        constexpr double positionZ = 3.1;
        constexpr double positionC = 0.2;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, LengthUnit::Millimenter);

        constexpr int32_t tileNumberH = 3;
        constexpr int32_t tileNumberV = 3;

        constexpr int32_t tileIndexColumn = 1;
        constexpr int32_t tileIndexRow = 2;

        TCFTileInfo tcfTileInfo;
        tcfTileInfo.SetOverlapLength(overlapLengthH, overlapLengthV, overlapLengthUnit);
        tcfTileInfo.SetTCFDataSetRecordedPosition(position);
        tcfTileInfo.SetTileNumber(tileNumberH, tileNumberV);
        tcfTileInfo.SetTileIndex(tileIndexColumn, tileIndexRow);


        TCFCompleter completer;
        completer.SetTargetTCFFilePath(targetTCFFilePath);
        completer.SetTCFCommonInfo(tcfCommonInfo);
        completer.SetTCFDeviceInfo(tcfDeviceInfo);
        completer.SetTCFTileInfo(tcfTileInfo);

        completer.MakeTCFComplete();


        H5::H5File file(targetTCFFilePath.toStdString(), H5F_ACC_RDONLY);

        CHECK(ReadAttributeString(file, "CreateDate") == createDate.toString("yyyy-MM-dd HH:mm:ss").toStdString());
        CHECK(ReadAttributeString(file, "DataID") == dataID.toStdString());
        CHECK(ReadAttributeString(file, "Description") == "");
        CHECK(ReadAttributeString(file, "DeviceHost") == deviceHost.toStdString());
        CHECK(ReadAttributeString(file, "DeviceSerial") == deviceSerial.toStdString());
        CHECK(ReadAttributeString(file, "DeviceModelType") == "HTX");
        CHECK(ReadAttributeString(file, "DeviceSoftwareVersion") == deviceSoftwareVersion.toStdString());
        CHECK(ReadAttributeString(file, "FormatVersion") == formatVersion.toStdString());
        CHECK(ReadAttributeString(file, "RecordingTime") == recordingTime.toString("yyyy-MM-dd HH:mm:ss.zzz").toStdString());
        CHECK(ReadAttributeString(file, "SoftwareVersion") == softwareVersion.toStdString());
        CHECK(ReadAttributeString(file, "Title") == title.toStdString());
        CHECK(ReadAttributeString(file, "UniqueID") == uniqueID.toStdString());
        CHECK(ReadAttributeString(file, "UserID") == userID);


        auto infoGroup = file.openGroup("/Info");

        auto annotationGroup = infoGroup.openGroup("Annotation");
        annotationGroup.close();

        auto deviceGroup = infoGroup.openGroup("Device");
        CHECK(ReadAttributeDouble(deviceGroup, "Magnification") == magnification);
        CHECK(ReadAttributeDouble(deviceGroup, "NA") == na);
        CHECK(ReadAttributeDouble(deviceGroup, "RI") == ri);
        deviceGroup.close();

        auto tileGroup = infoGroup.openGroup("Tile");
        CHECK(ReadAttributeDouble(tileGroup, "Overlap_H") == overlapLengthH);
        CHECK(ReadAttributeDouble(tileGroup, "Overlap_V") == overlapLengthV);
        CHECK(ReadAttributeDouble(tileGroup, "C") == positionC);
        CHECK(ReadAttributeDouble(tileGroup, "X") == positionX);
        CHECK(ReadAttributeDouble(tileGroup, "Y") == positionY);
        CHECK(ReadAttributeDouble(tileGroup, "Z") == positionZ);
        CHECK(ReadAttributeInt64(tileGroup, "Tiles_H") == tileNumberH);
        CHECK(ReadAttributeInt64(tileGroup, "Tiles_V") == tileNumberV);
        CHECK(ReadAttributeInt64(tileGroup, "Tiles") == tileNumberH*tileNumberV);
        CHECK(ReadAttributeInt64(tileGroup, "ColumnIndex") == tileIndexColumn);
        CHECK(ReadAttributeInt64(tileGroup, "RowIndex") == tileIndexRow);
        tileGroup.close();

        infoGroup.close();

        file.close();
    }
}
