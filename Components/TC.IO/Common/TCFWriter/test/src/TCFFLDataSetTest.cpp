#include <catch2/catch.hpp>

#include "TCFFLDataSet.h"

using namespace TC::TCFWriter;

namespace TCFFLDataSetTest {
    class FLDataSetGetterForTest final : public IFLDataSetGetter {
    public:
        FLDataSetGetterForTest() = default;
        ~FLDataSetGetterForTest() = default;

        auto GetData() const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
            const int32_t& z1) const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetDataMemoryOrder() const -> MemoryOrder3D override {
            return MemoryOrder3D::XYZ;
        }
        auto GetMIPData() const -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }
        auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
            -> std::shared_ptr<float[]> override {
            std::shared_ptr<float[]> data{ new float[1]() };
            data.get()[0] = 1;
            return data;
        }

        auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override {
            return MemoryOrder2D::XY;
        }
    };

    TEST_CASE("TCFFLDataSet : unit test") {
        SECTION("TCFFLDataSet()") {
            TCFFLDataSet tcfFLDataSet;
            CHECK(&tcfFLDataSet != nullptr);
        }
        SECTION("TCFFLDataSet(other)") {
            TCFFLDataSetMetaInfo metaInfo;
            metaInfo.SetChannelIndex(1);

            TCFFLDataSet srcTCFFLDataSet;
            srcTCFFLDataSet.SetMetaInfo(metaInfo);

            TCFFLDataSet destTCFFLDataSet(srcTCFFLDataSet);
            CHECK(destTCFFLDataSet.GetMetaInfo().GetChannelIndex() == 1);
        }
        SECTION("operator=()") {
            TCFFLDataSetMetaInfo metaInfo;
            metaInfo.SetChannelIndex(1);

            TCFFLDataSet srcTCFFLDataSet;
            srcTCFFLDataSet.SetMetaInfo(metaInfo);

            TCFFLDataSet destTCFFLDataSet;
            destTCFFLDataSet = srcTCFFLDataSet;
            CHECK(destTCFFLDataSet.GetMetaInfo().GetChannelIndex() == 1);
        }
        SECTION("SetDataSetGetter()") {
            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(nullptr);
            CHECK(&tcfFLDataSet != nullptr);
        }
        SECTION("GetData()") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            const auto resultData = tcfFLDataSet.GetData();
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1)") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            const auto resultData = tcfFLDataSet.GetData(0, 1, 2, 3, 4, 5);
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetDataMemoryOrder()") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            CHECK(tcfFLDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        }
        SECTION("GetMIPData()") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            const auto resultData = tcfFLDataSet.GetMIPData();
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetMIPData(x0,x1,y0,y1)") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            const auto resultData = tcfFLDataSet.GetMIPData(0, 1, 2, 3);
            CHECK(resultData.get()[0] == 1);
        }
        SECTION("GetMIPDataMemoryOrder()") {
            auto flDataSetGetter = new FLDataSetGetterForTest;
            IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

            TCFFLDataSet tcfFLDataSet;
            tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

            CHECK(tcfFLDataSet.GetMIPDataMemoryOrder() == +MemoryOrder2D::XY);
        }
    }
    TEST_CASE("TCFFLDataSet : practical test") {
        constexpr auto dataSizeX = 2;
        constexpr auto dataSizeY = 2;
        constexpr auto dataSizeZ = 3;
        constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr float pixelWorldSizeZ = 0.2f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 0;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

        auto flDataSetGetter = new FLDataSetGetterForTest;
        IFLDataSetGetter::Pointer flDataSetGetterPointer{ flDataSetGetter };

        TCFFLDataSetMetaInfo metaInfo;
        metaInfo.SetDataMinMaxValue(1, 2);
        metaInfo.SetMIPDataMinMaxValue(1, 2);
        metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
        metaInfo.SetRecordedTime(recordedTime);
        metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
        metaInfo.SetTimeFrameIndex(timeFrameIndex);
        metaInfo.SetPosition(position);
        metaInfo.SetChannelIndex(1);

        TCFFLDataSet tcfFLDataSet;
        tcfFLDataSet.SetDataSetGetter(flDataSetGetterPointer);

        CHECK(tcfFLDataSet.GetData().get()[0] == 1);
        CHECK(tcfFLDataSet.GetData(0, 1, 2, 3, 4, 5).get()[0] == 1);
        CHECK(tcfFLDataSet.GetDataMemoryOrder() == +MemoryOrder3D::XYZ);
        CHECK(tcfFLDataSet.GetMIPData().get()[0] == 1);
        CHECK(tcfFLDataSet.GetMIPData(0, 1, 2, 3).get()[0] == 1);
        CHECK(tcfFLDataSet.GetMIPDataMemoryOrder() == +MemoryOrder2D::XY);

        const auto resultMetaInfo = tcfFLDataSet.GetMetaInfo();
        CHECK(resultMetaInfo.GetDataMinValue() == 1);
        CHECK(resultMetaInfo.GetDataMaxValue() == 2);
        CHECK(resultMetaInfo.GetDataMIPMinValue() == 1);
        CHECK(resultMetaInfo.GetDataMIPMaxValue() == 2);
        CHECK(resultMetaInfo.GetSizeX() == dataSizeX);
        CHECK(resultMetaInfo.GetSizeY() == dataSizeY);
        CHECK(resultMetaInfo.GetSizeZ() == dataSizeZ);
        CHECK(resultMetaInfo.GetPixelWorldSizeX(LengthUnit::Micrometer) == pixelWorldSizeX);
        CHECK(resultMetaInfo.GetPixelWorldSizeY(LengthUnit::Micrometer) == pixelWorldSizeY);
        CHECK(resultMetaInfo.GetPixelWorldSizeZ(LengthUnit::Micrometer) == pixelWorldSizeZ);
        CHECK(resultMetaInfo.GetRecordedTime() == recordedTime);
        CHECK(resultMetaInfo.GetElapsedTime(elapsedTimeUnit) == elapsedTime);
        CHECK(resultMetaInfo.GetTimeFrameIndex() == timeFrameIndex);
        CHECK(resultMetaInfo.GetPosition().GetPositionX(LengthUnit::Millimenter) == positionX);
        CHECK(resultMetaInfo.GetPosition().GetPositionY(LengthUnit::Millimenter) == positionY);
        CHECK(resultMetaInfo.GetPosition().GetPositionZ(LengthUnit::Millimenter) == positionZ);
        CHECK(resultMetaInfo.GetPosition().GetPositionC(LengthUnit::Millimenter) == positionC);
        CHECK(resultMetaInfo.GetChannelIndex() == 1);
    }
}