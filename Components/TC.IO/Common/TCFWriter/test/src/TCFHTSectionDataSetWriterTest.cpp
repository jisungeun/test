#include <catch2/catch.hpp>

#include "TCFHTSectionDataSetWriter.h"
#include "TCFSkeletonWriter.h"

#include "H5Cpp.h"

#include <fstream>

#include "CompareArray.h"
#include "BinaryFileIO.h"
#include "HTDataSetGetterMemory.h"
#include "TCFWriterUtility.h"

using namespace TC::TCFWriter;

namespace TCFHTSectionDataSetWriterTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/TCTCFWriterTest";
    const QString cellFolderPath = testFolderPath + "/TCFCellData";

    TEST_CASE("TCFHTSectionDataSetWriter : unit test") {
        SECTION("TCFHTSectionDataSetWriter()") {
            TCFHTSectionDataSetWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            TCFHTSectionDataSetWriter writer;
            writer.SetTargetFilePath("");
            CHECK(&writer != nullptr);
        }
        SECTION("SetTCFHTDataSet()") {
            TCFHTSectionDataSetWriter writer;
            writer.SetTCFHTDataSet({});
            CHECK(&writer != nullptr);
        }
        SECTION("SetWholeDataSize()") {
            TCFHTSectionDataSetWriter writer;
            writer.SetWholeDataSize(1, 2, 3);
            CHECK(&writer != nullptr);

        }
        SECTION("SetWritingStartIndex()") {
            TCFHTSectionDataSetWriter writer;
            writer.SetWritingStartIndex(1, 2, 3);
            CHECK(&writer != nullptr);
        }
        SECTION("Write()") {
            const QString targetFilePath = "tcfHTDataSetWriter.h5";

            TCFSkeletonConfig config;
            config.SetDataIncludingFlag(true, false, false);

            TCFSkeletonWriter skeletonWriter;
            skeletonWriter.SetTargetFilePath(targetFilePath);
            skeletonWriter.SetConfig(config);

            skeletonWriter.Write();

            constexpr auto dataSizeX = 2;
            constexpr auto dataSizeY = 2;
            constexpr auto dataSizeZ = 3;
            constexpr auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            std::shared_ptr<float[]> data{ new float[numberOfElements]() };
            constexpr auto memoryOrder = MemoryOrder3D::XYZ;

            std::shared_ptr<float[]> mipData{ new float[numberOfElements]() };
            constexpr auto mipMemoryOrder = MemoryOrder2D::XY;

            constexpr float pixelWorldSizeX = 0.1f;
            constexpr float pixelWorldSizeY = 0.1f;
            constexpr float pixelWorldSizeZ = 0.2f;
            constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

            const auto recordedTime = QDateTime::currentDateTime();

            constexpr auto elapsedTime = 10.f;
            constexpr auto elapsedTimeUnit = TimeUnit::Second;

            constexpr auto timeFrameIndex = 0;

            constexpr auto positionX = 1.f;
            constexpr auto positionY = 2.f;
            constexpr auto positionZ = 10.f;
            constexpr auto positionC = 3.f;
            constexpr auto positionUnit = LengthUnit::Millimenter;

            TCFDataSetRecordedPosition position;
            position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

            auto htDataSetGetterMemory = new HTDataSetGetterMemory;
            htDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            htDataSetGetterMemory->SetData(data, memoryOrder);
            htDataSetGetterMemory->SetMIPData(mipData, mipMemoryOrder);

            IHTDataSetGetter::Pointer htDataSetGetter{ htDataSetGetterMemory };

            TCFHTDataSetMetaInfo metaInfo;
            metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            metaInfo.SetDataMinMaxValue(1, 2);
            metaInfo.SetMIPDataMinMaxValue(1, 2);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);

            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetter);
            tcfHTDataSet.SetMetaInfo(metaInfo);

            TCFHTSectionDataSetWriter writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetTCFHTDataSet(tcfHTDataSet);
            writer.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
            writer.SetWritingStartIndex(0, 0, 0);

            const auto writingResult = writer.Write();

            CHECK(writingResult == true);
        }
    }

    TEST_CASE("TCFHTSectionDataSetWriter : practical test") {
        const QString targetFilePath = "tcfHTDataSetWriter.h5";

        TCFSkeletonConfig config;
        config.SetDataIncludingFlag(true, false, false);

        TCFSkeletonWriter skeletonWriter;
        skeletonWriter.SetTargetFilePath(targetFilePath);
        skeletonWriter.SetConfig(config);
        skeletonWriter.Write();

        constexpr auto dataSizeX = 488;
        constexpr auto dataSizeY = 488;
        constexpr auto dataSizeZ = 202;
        constexpr auto numberOfElements2D = dataSizeX * dataSizeY;
        constexpr auto numberOfElements3D = dataSizeX * dataSizeY * dataSizeZ;

        const auto data2DMIP = ReadFile_uint16_t((cellFolderPath + "/data2DMIP_YXOrder").toStdString(), numberOfElements2D);
        const auto data2DMIPFloat = std::shared_ptr<float[]>{ new float[numberOfElements2D]() };
        for (auto index = 0; index < numberOfElements2D; ++index) {
            data2DMIPFloat.get()[index] = static_cast<float>(data2DMIP.get()[index]) / 10000;
        }
        constexpr auto memoryOrder2D = MemoryOrder2D::YX;

        const auto data3D = ReadFile_uint16_t((cellFolderPath + "/data3D_YXZOrder").toStdString(), numberOfElements3D);
        const auto data3DFloat = std::shared_ptr<float[]>{ new float[numberOfElements3D]() };
        for (auto index = 0; index < numberOfElements3D; ++index) {
            data3DFloat.get()[index] = static_cast<float>(data3D.get()[index]) / 10000;
        }
        constexpr auto memoryOrder3D = MemoryOrder3D::YXZ;

        constexpr float pixelWorldSizeX = 0.1f;
        constexpr float pixelWorldSizeY = 0.1f;
        constexpr float pixelWorldSizeZ = 0.2f;
        constexpr auto pixelWorldSizeUnit = LengthUnit::Micrometer;

        const auto recordedTime = QDateTime::currentDateTime();

        constexpr auto elapsedTime = 10.f;
        constexpr auto elapsedTimeUnit = TimeUnit::Second;

        constexpr auto timeFrameIndex = 1;

        constexpr auto positionX = 1.f;
        constexpr auto positionY = 2.f;
        constexpr auto positionZ = 10.f;
        constexpr auto positionC = 3.f;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        constexpr auto wholeDataSizeX = 500;
        constexpr auto wholeDataSizeY = 520;
        constexpr auto wholeDataSizeZ = dataSizeZ;

        constexpr auto writingStartIndexX = 1;
        constexpr auto writingStartIndexY = 2;
        constexpr auto writingStartIndexZ = 0;

        TCFDataSetRecordedPosition position;
        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
        {
            auto htDataSetGetterMemory = new HTDataSetGetterMemory;
            htDataSetGetterMemory->SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            htDataSetGetterMemory->SetData(data3DFloat, memoryOrder3D);
            htDataSetGetterMemory->SetMIPData(data2DMIPFloat, memoryOrder2D);

            IHTDataSetGetter::Pointer htDataSetGetter{ htDataSetGetterMemory };

            TCFHTDataSetMetaInfo metaInfo;
            metaInfo.SetDataMinMaxValue(1.3f, 1.4f);
            metaInfo.SetMIPDataMinMaxValue(1.31f, 1.4f);
            metaInfo.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
            metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, pixelWorldSizeUnit);
            metaInfo.SetRecordedTime(recordedTime);
            metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
            metaInfo.SetTimeFrameIndex(timeFrameIndex);
            metaInfo.SetPosition(position);
            
            TCFHTDataSet tcfHTDataSet;
            tcfHTDataSet.SetDataSetGetter(htDataSetGetter);

            TCFHTSectionDataSetWriter writer;
            writer.SetTargetFilePath(targetFilePath);
            writer.SetTCFHTDataSet(tcfHTDataSet);
            writer.SetWholeDataSize(wholeDataSizeX, wholeDataSizeY, wholeDataSizeZ);
            writer.SetWritingStartIndex(writingStartIndexX, writingStartIndexY, writingStartIndexZ);

            const auto writingResult = writer.Write();

            CHECK(writingResult == true);
        }

        const H5::H5File writtenFile{ targetFilePath.toStdString(), H5F_ACC_RDONLY };

        CHECK(writtenFile.nameExists("/Data/2DMIP/000001"));
        CHECK(writtenFile.nameExists("/Data/3D/000001"));

        const auto dataSet2DMIP = writtenFile.openDataSet("/Data/2DMIP/000001");
        const auto dataSet3D = writtenFile.openDataSet("/Data/3D/000001");

        const auto dataSpace2DMIP = dataSet2DMIP.getSpace();
        const auto dataSpace3D = dataSet3D.getSpace();

        hsize_t dataSpace2DMIPDim[3], dataSpace3DDims[3];

        CHECK(dataSpace2DMIP.getSimpleExtentDims(dataSpace2DMIPDim) == 2);
        CHECK(dataSpace2DMIPDim[0] == wholeDataSizeY);
        CHECK(dataSpace2DMIPDim[1] == wholeDataSizeX);

        CHECK(dataSpace3D.getSimpleExtentDims(dataSpace3DDims) == 3);
        CHECK(dataSpace3DDims[0] == wholeDataSizeZ);
        CHECK(dataSpace3DDims[1] == wholeDataSizeY);
        CHECK(dataSpace3DDims[2] == wholeDataSizeX);

        CHECK(dataSet2DMIP.attrExists("PositionX"));
        CHECK(dataSet2DMIP.attrExists("PositionY"));
        CHECK(dataSet2DMIP.attrExists("PositionZ"));
        CHECK(dataSet2DMIP.attrExists("PositionC"));
        CHECK(dataSet2DMIP.attrExists("RIMax"));
        CHECK(dataSet2DMIP.attrExists("RIMin"));
        CHECK(dataSet2DMIP.attrExists("RecordingTime"));
        CHECK(dataSet2DMIP.attrExists("Time"));

        CHECK(ReadAttributeDouble(dataSet2DMIP, "PositionX") == positionX);
        CHECK(ReadAttributeDouble(dataSet2DMIP, "PositionY") == positionY);
        CHECK(ReadAttributeDouble(dataSet2DMIP, "PositionZ") == positionZ);
        CHECK(ReadAttributeDouble(dataSet2DMIP, "PositionC") == positionC);
        CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet2DMIP, "RIMax") * 10000)) == 14000);
        CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet2DMIP, "RIMin") * 10000)) == 13100);
        CHECK(ReadAttributeString(dataSet2DMIP, "RecordingTime") == recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toStdString());
        CHECK(ReadAttributeDouble(dataSet2DMIP, "Time") == elapsedTime);

        CHECK(dataSet3D.attrExists("PositionX"));
        CHECK(dataSet3D.attrExists("PositionY"));
        CHECK(dataSet3D.attrExists("PositionZ"));
        CHECK(dataSet3D.attrExists("PositionC"));
        CHECK(dataSet3D.attrExists("RIMax"));
        CHECK(dataSet3D.attrExists("RIMin"));
        CHECK(dataSet3D.attrExists("RecordingTime"));
        CHECK(dataSet3D.attrExists("Time"));

        CHECK(ReadAttributeDouble(dataSet3D, "PositionX") == positionX);
        CHECK(ReadAttributeDouble(dataSet3D, "PositionY") == positionY);
        CHECK(ReadAttributeDouble(dataSet3D, "PositionZ") == positionZ);
        CHECK(ReadAttributeDouble(dataSet3D, "PositionC") == positionC);
        CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet3D, "RIMax") * 10000)) == 14000);
        CHECK(static_cast<int32_t>(std::round(ReadAttributeDouble(dataSet3D, "RIMin") * 10000)) == 13000);
        CHECK(ReadAttributeString(dataSet3D, "RecordingTime") == recordedTime.toString("yyyy-MM-dd hh:mm:ss.zzz").toStdString());
        CHECK(ReadAttributeDouble(dataSet3D, "Time") == elapsedTime);

        constexpr auto numberOfWholeElements2D = wholeDataSizeX * wholeDataSizeY;
        constexpr auto numberOfWholeElements3D = wholeDataSizeX * wholeDataSizeY * wholeDataSizeZ;

        std::shared_ptr<uint16_t[]> resultData3D{ new uint16_t[numberOfWholeElements3D]() };
        std::shared_ptr<uint16_t[]> resultData2D{ new uint16_t[numberOfWholeElements2D]() };

        dataSet3D.read(resultData3D.get(), H5::PredType::NATIVE_UINT16);
        dataSet2DMIP.read(resultData2D.get(), H5::PredType::NATIVE_UINT16);

        const auto answerData3D = ReadFile_uint16_t((cellFolderPath + "/binData3DExtended").toStdString(), numberOfWholeElements3D);
        const auto answerData2D = ReadFile_uint16_t((cellFolderPath + "/binData2DMIPExtended").toStdString(), numberOfWholeElements2D);

        CHECK(CompareArray(resultData3D.get(), answerData3D.get(), numberOfWholeElements3D));
        CHECK(CompareArray(resultData2D.get(), answerData2D.get(), numberOfWholeElements2D));
    }
}
