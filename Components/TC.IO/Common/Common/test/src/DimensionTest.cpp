#include <catch2/catch.hpp>

#include "Dimension.h"
#include "Count.h"

using namespace TC::IO;

namespace DimensionTest {
    TEST_CASE("Dimension") {
        SECTION("Dimension()") {
            Dimension dimension;
            CHECK(dimension.GetDimensionality() == 0);
            CHECK(dimension.X() == 0);
            CHECK(dimension.Y() == 0);
            CHECK(dimension.Z() == 0);
            CHECK(dimension.GetNumberOfElements() == 0);
        }
        SECTION("Dimension(Dimension)") {
            Dimension srcDimension(1, 2, 3);
            const auto destDimension(srcDimension);
            CHECK(destDimension.GetDimensionality() == 3);
            CHECK(destDimension.X() == 1);
            CHECK(destDimension.Y() == 2);
            CHECK(destDimension.Z() == 3);
            CHECK(destDimension.GetNumberOfElements() == 6);
        }
        SECTION("Dimension(Count)") {
            Count count(1, 2, 3);
            Dimension dimension(count);
            CHECK(dimension.GetDimensionality() == 3);
            CHECK(dimension.X() == 1);
            CHECK(dimension.Y() == 2);
            CHECK(dimension.Z() == 3);
            CHECK(dimension.GetNumberOfElements() == 6);

            Count count2D(1, 2);
            Dimension dimension2(count2D);
            CHECK(dimension2.GetDimensionality() == 2);
            CHECK(dimension2.X() == 1);
            CHECK(dimension2.Y() == 2);
            CHECK(dimension2.Z() == 1);
            CHECK(dimension2 .GetNumberOfElements() == 2);

        }
        SECTION("Dimension(x,y)") {
            Dimension dimension(1, 2);
            CHECK(dimension.GetDimensionality() == 2);
            CHECK(dimension.X() == 1);
            CHECK(dimension.Y() == 2);
            CHECK(dimension.Z() == 1);
            CHECK(dimension.GetNumberOfElements() == 2);
        }
        SECTION("Dimension(x,y,z)") {
            Dimension dimension(1, 2, 3);
            CHECK(dimension.GetDimensionality() == 3);
            CHECK(dimension.X() == 1);
            CHECK(dimension.Y() == 2);
            CHECK(dimension.Z() == 3);
            CHECK(dimension.GetNumberOfElements() == 6);
        }
        SECTION("operator=") {
            Dimension srcDimension(1, 2, 3);
            Dimension destDimension{};
            destDimension = srcDimension;
            CHECK(destDimension.GetDimensionality() == 3);
            CHECK(destDimension.X() == 1);
            CHECK(destDimension.Y() == 2);
            CHECK(destDimension.Z() == 3);
            CHECK(destDimension.GetNumberOfElements() == 6);
        }
        SECTION("operator==") {
            Dimension dimension1(1, 2, 3);
            Dimension dimension2(1, 2, 3);
            const auto result1 = dimension1 == dimension2;
            CHECK(result1 == true);

            Dimension dimension3(1, 2, 0);
            Dimension dimension4(1, 0, 3);
            Dimension dimension5(0, 2, 3);

            const auto result2 = dimension1 == dimension3;
            CHECK(result2 == false);
            const auto result3 = dimension1 == dimension4;
            CHECK(result3 == false);
            const auto result4 = dimension1 == dimension5;
            CHECK(result4 == false);
        }
        SECTION("operator!=") {
            Dimension dimension1(1, 2, 3);
            Dimension dimension2(1, 2, 3);
            const auto result1 = dimension1 != dimension2;
            CHECK(result1 == false);

            Dimension dimension3(1, 2, 0);
            Dimension dimension4(1, 0, 3);
            Dimension dimension5(0, 2, 3);

            const auto result2 = dimension1 != dimension3;
            CHECK(result2 == true);
            const auto result3 = dimension1 != dimension4;
            CHECK(result3 == true);
            const auto result4 = dimension1 != dimension5;
            CHECK(result4 == true);
        }
        SECTION("GetDimensionality()") {
            Dimension dimension2D(1, 2);
            Dimension dimension3D(1, 2, 3);
            CHECK(dimension2D.GetDimensionality() == 2);
            CHECK(dimension3D.GetDimensionality() == 3);
        }
        SECTION("X()") {
            Dimension dimension(1, 2, 3);
            CHECK(dimension.X() == 1);
        }
        SECTION("Y()") {
            Dimension dimension(1, 2, 3);
            CHECK(dimension.Y() == 2);
        }
        SECTION("Z()") {
            Dimension dimension(1, 2, 3);
            CHECK(dimension.Z() == 3);
        }
        SECTION("GetNumberOfElements()") {
            Dimension dimension2D(1, 2);
            Dimension dimension3D(1, 2, 3);
            CHECK(dimension2D.GetNumberOfElements() == 2);
            CHECK(dimension3D.GetNumberOfElements() == 6);
        }
    }
}
