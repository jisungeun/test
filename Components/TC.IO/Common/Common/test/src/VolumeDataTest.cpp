#include <catch2/catch.hpp>

#include <VolumeData.h>

using namespace TC::IO;

namespace _Test {
    template<typename Type, uint32_t xDim, uint32_t yDim, uint32_t zDim>
    auto CreateArray()->std::shared_ptr<Type[]> {
        const auto elements = xDim * yDim * zDim;
        auto rawData = std::shared_ptr<Type[]>(new Type[elements]);
        for (uint32_t idx = 0; idx < elements; idx++) rawData.get()[idx] = static_cast<Type>(idx);
        return rawData;
    }
}

TEST_CASE("VolumeData General", "[VolumeData]") {
    namespace test = _Test;

    SECTION("Dimension") {
        VolumeData volumeData;
        volumeData.SetDimension(10, 10, 1);

        REQUIRE(volumeData.GetElements() == 10 * 10);

        auto dims = volumeData.GetDimension();
        REQUIRE(std::get<0>(dims) == 10);
        REQUIRE(std::get<1>(dims) == 10);
        REQUIRE(std::get<2>(dims) == 1);
    }

    SECTION("Resolution") {
        VolumeData volumeData;
        volumeData.SetResolution(0.1, 0.1, 0);

        auto resolutions = volumeData.GetResolution();
        REQUIRE(std::get<0>(resolutions) == 0.1);
        REQUIRE(std::get<1>(resolutions) == 0.1);
        REQUIRE(std::get<2>(resolutions) == 0);
    }
}

TEST_CASE("VolumeData with data types", "[VolumeData]") {
    namespace test = _Test;
    const uint32_t xDim = 4;
    const uint32_t yDim = 4;
    const uint32_t zDim = 2;
    const uint32_t elements = xDim * yDim * zDim;

    SECTION("int8_t") {
        auto rawdata = test::CreateArray<int8_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::Int8Type);

        auto data = volumeData.GetDataAsInt8();
        REQUIRE(data.get() != nullptr);

        for(uint32_t idx=0; idx<elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("uint8_t") {
        auto rawdata = test::CreateArray<uint8_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::UInt8Type);

        auto data = volumeData.GetDataAsUInt8();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("int16_t") {
        auto rawdata = test::CreateArray<int16_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::Int16Type);

        auto data = volumeData.GetDataAsInt16();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("uint16_t") {
        auto rawdata = test::CreateArray<uint16_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::UInt16Type);

        auto data = volumeData.GetDataAsUInt16();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("int32_t") {
        auto rawdata = test::CreateArray<int32_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::Int32Type);

        auto data = volumeData.GetDataAsInt32();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("uint32_t") {
        auto rawdata = test::CreateArray<uint32_t, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::UInt32Type);

        auto data = volumeData.GetDataAsUInt32();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("float") {
        auto rawdata = test::CreateArray<float, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::FloatType);

        auto data = volumeData.GetDataAsFloat();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }

    SECTION("double") {
        auto rawdata = test::CreateArray<double, xDim, yDim, zDim>();

        VolumeData volumeData;
        volumeData.SetDimension(xDim, yDim, zDim);
        volumeData.SetRawData(rawdata.get(), elements);

        auto type = volumeData.GetDataType();
        REQUIRE(type == VolumeData::Type::DoubleType);

        auto data = volumeData.GetDataAsDouble();
        REQUIRE(data.get() != nullptr);

        for (uint32_t idx = 0; idx < elements; idx++) {
            REQUIRE(data.get()[idx] == idx);
        }
    }
}