#include <catch2/catch.hpp>

#include <BoundingBox.h>

using namespace TC::IO;

namespace _Test {
}

TEST_CASE("BoudingBox", "[BoundingBox]") {
    namespace test = _Test;

    SECTION("Creation") {
        BoundingBox bbox;

        bbox.SetOffset(0, 0, 0);
        bbox.SetSize(10, 20, 30);

        auto offset = bbox.GetOffset();
        auto size = bbox.GetSize();
        auto center = bbox.GetCenter();

        REQUIRE(offset.x0 == 0);
        REQUIRE(offset.y0 == 0);
        REQUIRE(offset.z0 == 0);

        REQUIRE(size.d0 == 10);
        REQUIRE(size.d1 == 20);
        REQUIRE(size.d2 == 30);

        REQUIRE(center.x0 == 5);
        REQUIRE(center.y0 == 10);
        REQUIRE(center.z0 == 15);
    }

    SECTION("Contain") {
        BoundingBox bbox;

        bbox.SetOffset(0, 0, 0);
        bbox.SetSize(10, 20, 30);

        REQUIRE(bbox.Contain(0, 0, 0) == true);
        REQUIRE(bbox.Contain(9, 0, 0) == true);
        REQUIRE(bbox.Contain(9, 19, 0) == true);
        REQUIRE(bbox.Contain(0, 19, 0) == true);
        REQUIRE(bbox.Contain(0, 0, 29) == true);
        REQUIRE(bbox.Contain(9, 0, 29) == true);
        REQUIRE(bbox.Contain(9, 19, 29) == true);
        REQUIRE(bbox.Contain(0, 19, 29) == true);

        REQUIRE(bbox.Contain(10, 20, 30) == false);
    }
}

