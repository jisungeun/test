#include <catch2/catch.hpp>
#include "Count.h"

using namespace TC::IO;

namespace CountTest {
    TEST_CASE("Count") {
        SECTION("Count()") {
            Count count;
            CHECK(count.GetDimensionality() == 0);
            CHECK(count.X() == 0);
            CHECK(count.Y() == 0);
            CHECK(count.Z() == 0);
        }

        SECTION("Count(Count)") {
            Count srcCount(1, 2, 3);
            const auto destCount1(srcCount);
            CHECK(destCount1.GetDimensionality() == 3);
            CHECK(destCount1.X() == 1);
            CHECK(destCount1.Y() == 2);
            CHECK(destCount1.Z() == 3);

            const auto destCount2 = srcCount;
            CHECK(destCount2.GetDimensionality() == 3);
            CHECK(destCount2.X() == 1);
            CHECK(destCount2.Y() == 2);
            CHECK(destCount2.Z() == 3);
        }

        SECTION("Count(x,y)") {
            Count count(1, 2);
            CHECK(count.GetDimensionality() == 2);
            CHECK(count.X() == 1);
            CHECK(count.Y() == 2);
            CHECK(count.Z() == 0);
        }

        SECTION("Count(x,y,z)") {
            Count count(1, 2, 3);
            CHECK(count.GetDimensionality() == 3);
            CHECK(count.X() == 1);
            CHECK(count.Y() == 2);
            CHECK(count.Z() == 3);
        }

        SECTION("Count(Dimension)") {
            Dimension dimension1(1, 2);
            Count count1(dimension1);
            CHECK(count1.GetDimensionality() == 2);
            CHECK(count1.X() == 1);
            CHECK(count1.Y() == 2);
            CHECK(count1.Z() == 0);

            Dimension dimension2(1, 2, 3);
            Count count2(dimension2);
            CHECK(count2.GetDimensionality() == 3);
            CHECK(count2.X() == 1);
            CHECK(count2.Y() == 2);
            CHECK(count2.Z() == 3);
        }

        SECTION("operator=") {
            Count srcCount(1, 2, 3);
            Count destCount{};
            destCount = srcCount;
            CHECK(destCount.GetDimensionality() == 3);
            CHECK(destCount.X() == 1);
            CHECK(destCount.Y() == 2);
            CHECK(destCount.Z() == 3);
        }

        SECTION("operator==") {
            Count count1(1, 2, 3);
            Count count2(1, 2, 3);
            Count count3(1, 2, 0);
            Count count4(0, 2, 3);
            Count count5(1, 0, 3);

            const auto result1 = (count1 == count2);
            const auto result2 = (count1 == count3);
            const auto result3 = (count1 == count4);
            const auto result4 = (count1 == count5);
            CHECK(result1 == true);
            CHECK(result2 == false);
            CHECK(result3 == false);
            CHECK(result4 == false);
        }

        SECTION("operator!=") {
            Count count1(1, 2, 3);
            Count count2(1, 2, 3);
            Count count3(1, 2, 0);
            Count count4(0, 2, 3);
            Count count5(1, 0, 3);

            const auto result1 = (count1 != count2);
            const auto result2 = (count1 != count3);
            const auto result3 = (count1 != count4);
            const auto result4 = (count1 != count5);
            CHECK(result1 == false);
            CHECK(result2 == true);
            CHECK(result3 == true);
            CHECK(result4 == true);
        }

        SECTION("operator/") {
            Count count(10, 10, 10);

            const auto result1 = count / 1;
            const auto result2 = count / 2;
            const auto result3 = count / 3;
            const auto result4 = count / 4;
            const auto result5 = count / 5;
            const auto result6 = count / 10;
            const auto result7 = count / 11;

            CHECK(result1 == Count(10, 10, 10));
            CHECK(result2 == Count(5, 5, 5));
            CHECK(result3 == Count(3, 3, 3));
            CHECK(result4 == Count(2, 2, 2));
            CHECK(result5 == Count(2, 2, 2));
            CHECK(result6 == Count(1, 1, 1));
            CHECK(result7 == Count(0, 0, 0));
        }

        SECTION("GetDimensionality()") {
            Count count2D(1, 2);
            Count count3D(1, 2, 3);

            CHECK(count2D.GetDimensionality() == 2);
            CHECK(count3D.GetDimensionality() == 3);
        }

        SECTION("X()") {
            Count count2D(1, 2);
            Count count3D(1, 2, 3);

            CHECK(count2D.X() == 1);
            CHECK(count3D.X() == 1);
        }

        SECTION("Y()") {
            Count count2D(1, 2);
            Count count3D(1, 2, 3);

            CHECK(count2D.Y() == 2);
            CHECK(count3D.Y() == 2);
        }

        SECTION("Z()") {
            Count count2D(1, 2);
            Count count3D(1, 2, 3);

            CHECK(count2D.Z() == 0);
            CHECK(count3D.Z() == 3);
        }

        SECTION("GetTotalNumberOfCount()") {
            Count count2D(1, 2);
            Count count3D(1, 2, 3);

            CHECK(count2D.GetTotalNumberOfCount() == 2);
            CHECK(count3D.GetTotalNumberOfCount() == 6);
        }
    }
}
