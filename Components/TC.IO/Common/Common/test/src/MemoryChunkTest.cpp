#include <catch2/catch.hpp>

#include "MemoryChunk.h"
#include "MemoryCheckUtility.h"

using namespace TC::IO;
using namespace TC;

namespace MemoryChunkTest {
    TEST_CASE("MemoryChunk") {
        MemoryChunk memoryChunk;

        SECTION("MemoryChunk()") {
            CHECK(memoryChunk.GetDimension() == Dimension());
            CHECK(memoryChunk.GetDataType()._value == ChunkDataType::Int8Type);
        }

        SECTION("GetDataType()") {
            auto data = std::shared_ptr<int16_t[]>(new int16_t[1]{ 1 });
            memoryChunk.SetData(data, Dimension());

            const auto chunkDataType = memoryChunk.GetDataType();
            CHECK(chunkDataType._value == ChunkDataType::Int16Type);
        }

        SECTION("GetData()") {
            auto data = std::shared_ptr<float[]>(new float[1]{ 1.f });
            memoryChunk.SetData(data, Dimension(1, 1));

            const auto chunkData = memoryChunk.GetData();
            CHECK(*std::any_cast<std::shared_ptr<float[]>>(chunkData).get() == 1.f);
        }

        SECTION("GetDimension()") {
            auto data = std::shared_ptr<float[]>(new float[12]());
            memoryChunk.SetData(data, Dimension(2, 3, 2));

            const auto dimension = memoryChunk.GetDimension();
            CHECK(dimension == Dimension(2, 3, 2));
        }

        SECTION("SetData()", "int8_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<int8_t[]>(new int8_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<int8_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "uint8_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<uint8_t[]>(new uint8_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<uint8_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "int16_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<int16_t[]>(new int16_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<int16_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "uint16_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<uint16_t[]>(new uint16_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<uint16_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "int32_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<int32_t[]>(new int32_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<int32_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "uint32_t") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<uint32_t[]>(new uint32_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<uint32_t[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "float") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<float[]>(new float[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = static_cast<float>(i); }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<float[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("SetData()", "double") {
            const auto dimension = Dimension(2, 3, 4);
            const auto numberOfElements = dimension.GetNumberOfElements();
            auto data = std::shared_ptr<double[]>(new double[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) { data.get()[i] = i; }
            memoryChunk.SetData(data, dimension);

            const auto chunkDimension = memoryChunk.GetDimension();
            CHECK(chunkDimension == dimension);

            const auto chunkData = memoryChunk.GetData();
            const auto castChunkData = std::any_cast<std::shared_ptr<double[]>>(chunkData).get();
            auto dataSame = true;
            for (auto i = 0; i < numberOfElements; ++i) {
                if (data.get()[i] != castChunkData[i]) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame == true);
        }

        SECTION("ZeroPadding()") {
            auto srcData = std::shared_ptr<uint8_t[]>(new uint8_t[8]{ 1,2,3,4,5,6,7,8 });
            const Dimension srcDimension = { 2,2,2 };
            MemoryChunk memoryChunk;
            memoryChunk.SetData(srcData, srcDimension);

            const Dimension paddedDimension = { 3,4,5 };
            memoryChunk.ZeroPadding(paddedDimension);

            const auto paddedData = std::any_cast<std::shared_ptr<uint8_t[]>>(memoryChunk.GetData()).get();

            CHECK(srcData[0] == paddedData[0]);
            CHECK(srcData[1] == paddedData[1]);
            CHECK(srcData[2] == paddedData[3]);
            CHECK(srcData[3] == paddedData[4]);
            CHECK(srcData[4] == paddedData[12]);
            CHECK(srcData[5] == paddedData[13]);
            CHECK(srcData[6] == paddedData[15]);
            CHECK(srcData[7] == paddedData[16]);
        }
    }

    TEST_CASE("MemoryChunk memory leak") {
        const Dimension dimension(1024 / 2, 1024 / 2, 1024);

        float memoryBeforeTest{}, memoryAfterTest{}, increasedMemoryInGB{};
        memoryBeforeTest = GetAvailableMemoryInBytes();
        SECTION("SetData()") {
            float memoryBeforeAllocation{}, memoryAfterAllocation{}, increasedMemoryInMB{};

            memoryBeforeAllocation = GetAvailableMemoryInBytes();
            auto data = std::shared_ptr<uint8_t[]>(new uint8_t[dimension.GetNumberOfElements()]());
            memoryAfterAllocation = GetAvailableMemoryInBytes();

            increasedMemoryInMB = 
                std::round(GetIncreasedMemoryInMB(memoryBeforeAllocation, memoryAfterAllocation));
            CHECK(increasedMemoryInMB > 200);
            CHECK(increasedMemoryInMB < 300);

            MemoryChunk memoryChunk;
            memoryBeforeAllocation = GetAvailableMemoryInBytes();
            memoryChunk.SetData(data, dimension);
            memoryAfterAllocation = GetAvailableMemoryInBytes();

            increasedMemoryInMB =
                std::round(GetIncreasedMemoryInMB(memoryBeforeAllocation, memoryAfterAllocation));
            CHECK(increasedMemoryInMB < 100);
        }
        memoryAfterTest = GetAvailableMemoryInBytes();
        increasedMemoryInGB =
            std::round(GetIncreasedMemoryInGB(memoryBeforeTest, memoryAfterTest));
        CHECK(increasedMemoryInGB == 0);


        memoryBeforeTest = GetAvailableMemoryInBytes();
        SECTION("ZeroPadding()") {
            float memoryBeforeAllocation{}, memoryAfterAllocation{}, increasedMemoryInMB{};

            auto data = std::shared_ptr<uint8_t[]>(new uint8_t[dimension.GetNumberOfElements()]());
            MemoryChunk memoryChunk;
            memoryChunk.SetData(data, dimension);

            const Dimension paddingDimension(1024, 1024 / 2, 1024);
            memoryBeforeAllocation = GetAvailableMemoryInBytes();
            memoryChunk.ZeroPadding(paddingDimension);
            memoryAfterAllocation = GetAvailableMemoryInBytes();

            increasedMemoryInMB =
                std::round(GetIncreasedMemoryInMB(memoryBeforeAllocation, memoryAfterAllocation));
            CHECK(increasedMemoryInMB > 512.f*0.9f);
            CHECK(increasedMemoryInMB < 512.f*1.1f);

        }
        memoryAfterTest = GetAvailableMemoryInBytes();
        increasedMemoryInGB =
            std::round(GetIncreasedMemoryInGB(memoryBeforeTest, memoryAfterTest));
        CHECK(increasedMemoryInGB == 0);
    }
}

