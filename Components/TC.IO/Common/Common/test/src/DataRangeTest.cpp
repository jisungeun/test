#include <catch2/catch.hpp>

#include "DataRange.h"

using namespace TC::IO;

namespace DataRangeTest {
    TEST_CASE("DataRange") {
        SECTION("DataRange()") {
            DataRange dataRange;
            CHECK(dataRange.GetOffsetPoint() == Point{});
            CHECK(dataRange.GetLastPoint() == Point{});
            CHECK(dataRange.IsEmpty());
        }

        SECTION("DataRange(DataRange)") {
            DataRange srcDataRange(Point(1, 2, 3), Count(3, 2, 1));
            const auto destDataRange(srcDataRange);
            CHECK(destDataRange.GetDimensionality() == 3);
            CHECK(destDataRange.GetCount() == Count(3, 2, 1));
            CHECK(destDataRange.GetLastPoint() == Point(3, 3, 3));
            CHECK(destDataRange.GetOffsetPoint() == Point(1, 2, 3));
        }

        SECTION("DataRange(Point,Point)") {
            DataRange dataRange(Point(1, 2, 3), Point(3, 3, 3));
            CHECK(dataRange.GetDimensionality() == 3);
            CHECK(dataRange.GetCount() == Count(3, 2, 1));
            CHECK(dataRange.GetLastPoint() == Point(3, 3, 3));
            CHECK(dataRange.GetOffsetPoint() == Point(1, 2, 3));
        }

        SECTION("DataRange(Point,Count)") {
            DataRange dataRange(Point(1, 2, 3), Count(3, 2, 1));
            CHECK(dataRange.GetDimensionality() == 3);
            CHECK(dataRange.GetCount() == Count(3, 2, 1));
            CHECK(dataRange.GetLastPoint() == Point(3, 3, 3));
            CHECK(dataRange.GetOffsetPoint() == Point(1, 2, 3));
        }

        SECTION("GetDimensionality()") {
            DataRange dataRange2D(Point(0, 0), Count(1, 1));
            DataRange dataRange3D(Point(0, 0, 0), Count(1, 1, 1));

            CHECK(dataRange2D.GetDimensionality() == 2);
            CHECK(dataRange3D.GetDimensionality() == 3);
        }

        SECTION("GetOffsetPoint()") {
            DataRange dataRange(Point(1, 2, 3), Count(1, 2, 3));
            CHECK(dataRange.GetOffsetPoint() == Point(1, 2, 3));
        }

        SECTION("GetLastPoint()") {
            DataRange dataRange1(Point(1, 2, 3), Point(3, 3, 3));
            DataRange dataRange2(Point(0, 1, 2), Count(3, 2, 1));

            CHECK(dataRange1.GetLastPoint() == Point(3, 3, 3));
            CHECK(dataRange2.GetLastPoint() == Point(2, 2, 2));
        }

        SECTION("GetCount()") {
            DataRange dataRange1(Point(1, 2, 3), Point(3, 3, 3));
            DataRange dataRange2(Point(0, 1, 2), Count(3, 2, 1));

            CHECK(dataRange1.GetCount() == Count(3, 2, 1));
            CHECK(dataRange2.GetCount() == Count(3, 2, 1));
        }

        SECTION("IsEmpty()") {
            DataRange dataRange1(Point(0, 0, 0), Count(1, 1, 1));
            CHECK(dataRange1.IsEmpty() == false);

            DataRange dataRange2(Point(0, 0, 0), Count(1, 1, 0));
            CHECK(dataRange2.IsEmpty() == true);
            DataRange dataRange3(Point(0, 0, 0), Count(1, 0, 1));
            CHECK(dataRange3.IsEmpty() == true);
            DataRange dataRange4(Point(0, 0, 0), Count(0, 1, 1));
            CHECK(dataRange4.IsEmpty() == true);

            DataRange dataRange5(Point(1, 1, 1), Point(1, 1, 1));
            CHECK(dataRange5.IsEmpty() == false);
            DataRange dataRange6(Point(1, 1, 1), Point(1, 1, 0));
            CHECK(dataRange6.IsEmpty() == true);
            DataRange dataRange7(Point(1, 1, 1), Point(1, 0, 1));
            CHECK(dataRange7.IsEmpty() == true);
            DataRange dataRange8(Point(1, 1, 1), Point(0, 1, 1));
            CHECK(dataRange8.IsEmpty() == true);
        }

        SECTION("GetOverlapRange()") {
            DataRange dataRange1(Point(0, 0, 0), Point(3, 3, 3));
            DataRange dataRange2(Point(1, 1, 1), Point(4, 4, 4));

            const auto overlapDataRange1 = dataRange1.GetOverlapRange(dataRange2);
            CHECK(!overlapDataRange1.IsEmpty());
            CHECK(overlapDataRange1.GetOffsetPoint() == Point(1, 1, 1));
            CHECK(overlapDataRange1.GetLastPoint() == Point(3, 3, 3));

            const auto overlapDataRange2 = dataRange2.GetOverlapRange(dataRange1);
            CHECK(!overlapDataRange2.IsEmpty());
            CHECK(overlapDataRange2.GetOffsetPoint() == Point(1, 1, 1));
            CHECK(overlapDataRange2.GetLastPoint() == Point(3, 3, 3));

            DataRange dataRange3(Point(0, 0), Point(1, 1));
            const auto overlapDataRange3 = dataRange1.GetOverlapRange(dataRange3);
            CHECK(overlapDataRange3.IsEmpty());
        }
    }
}
