#include <catch2/catch.hpp>

#include "Point.h"

using namespace TC::IO;

namespace PointTest {
    TEST_CASE("Point") {
        SECTION("Point()") {
            Point point{};
            CHECK(point.X() == 0);
            CHECK(point.Y() == 0);
            CHECK(point.Z() == 0);
            CHECK(point.GetDimensionality() == 0);
        }
        SECTION("Point(Point)") {
            Point srcPoint(1, 2, 3);
            const auto destPoint(srcPoint);
            CHECK(destPoint.X() == 1);
            CHECK(destPoint.Y() == 2);
            CHECK(destPoint.Z() == 3);
            CHECK(destPoint.GetDimensionality() == 3);
        }
        SECTION("Point(x,y)") {
            Point point(1, 2);
            CHECK(point.X() == 1);
            CHECK(point.Y() == 2);
            CHECK(point.Z() == 0);
            CHECK(point.GetDimensionality() == 2);
        }
        SECTION("Point(x,y,z)") {
            Point point(1, 2, 3);
            CHECK(point.X() == 1);
            CHECK(point.Y() == 2);
            CHECK(point.Z() == 3);
            CHECK(point.GetDimensionality() == 3);
        }
        SECTION("operator=") {
            Point srcPoint(1, 2, 3);
            Point destPoint{};
            destPoint = srcPoint;
            CHECK(destPoint.X() == 1);
            CHECK(destPoint.Y() == 2);
            CHECK(destPoint.Z() == 3);
            CHECK(destPoint.GetDimensionality() == 3);
        }
        SECTION("operator==") {
            Point point1(1, 2, 3);
            Point point2(1, 2, 3);
            const auto result1 = (point1 == point2);
            CHECK(result1 == true);

            Point point3(1, 2, 0);
            const auto result2 = (point1 == point3);
            CHECK(result2 == false);

            Point point4(1, 0, 3);
            const auto result3 = (point1 == point4);
            CHECK(result3 == false);

            Point point5(0, 2, 3);
            const auto result4 = (point1 == point5);
            CHECK(result4 == false);
        }
        SECTION("operator!=") {
            Point point1(1, 2, 3);
            Point point2(1, 2, 3);
            const auto result1 = (point1 != point2);
            CHECK(result1 == false);

            Point point3(1, 2, 0);
            const auto result2 = (point1 != point3);
            CHECK(result2 == true);

            Point point4(1, 0, 3);
            const auto result3 = (point1 != point4);
            CHECK(result3 == true);

            Point point5(0, 2, 3);
            const auto result4 = (point1 != point5);
            CHECK(result4 == true);
        }
        SECTION("operator-") {
            Point point1(1, 2, 3);
            Point point2(3, 2, 1);
            CHECK((point1 - point2) == Point(-2, 0, 2));
        }
        SECTION("operator+") {
            Point point1(1, 2, 3);
            Point point2(1, 1, 1);
            CHECK((point1 + point2) == Point(2, 3, 4));
        }
        SECTION("operator+(Count)") {
            Point point(1, 2, 3);
            Count count(1, 1, 1);
            CHECK((point + count) == Point(1, 2, 3));
        }
        SECTION("operator/") {
            Point point(10, 20, 30);

            const auto result1 = point / 1;
            const auto result2 = point / 2;
            const auto result3 = point / 3;
            const auto result4 = point / 4;
            const auto result5 = point / 5;
            const auto result6 = point / 10;
            const auto result7 = point / 20;

            CHECK(result1 == Point(10, 20, 30));
            CHECK(result2 == Point(5, 10, 15));
            CHECK(result3 == Point(3, 6, 10));
            CHECK(result4 == Point(2, 5, 7));
            CHECK(result5 == Point(2, 4, 6));
            CHECK(result6 == Point(1, 2, 3));
            CHECK(result7 == Point(0, 1, 1));
        }
        SECTION("SetPoint(x,y)") {
            Point point{};
            point.SetPoint(1, 2);

            CHECK(point.X() == 1);
            CHECK(point.Y() == 2);
            CHECK(point.Z() == 0);
            CHECK(point.GetDimensionality() == 2);

            Point point2(1, 2, 3);

            point2.SetPoint(3, 4);
            CHECK(point2.X() == 3);
            CHECK(point2.Y() == 4);
            CHECK(point2.Z() == 0);
            CHECK(point2.GetDimensionality() == 2);
        }
        SECTION("SetPoint(x,y,z)") {
            Point point{};
            point.SetPoint(1, 2, 3);

            CHECK(point.X() == 1);
            CHECK(point.Y() == 2);
            CHECK(point.Z() == 3);
            CHECK(point.GetDimensionality() == 3);
        }
        SECTION("GetDimensionality()") {
            Point point2D(1, 2);
            Point point3D(1, 2, 3);

            CHECK(point2D.GetDimensionality() == 2);
            CHECK(point3D.GetDimensionality() == 3);
        }
        SECTION("X()") {
            Point point(1, 2, 3);
            CHECK(point.X() == 1);
        }
        SECTION("Y()") {
            Point point(1, 2, 3);
            CHECK(point.Y() == 2);
        }
        SECTION("Z()") {
            Point point(1, 2, 3);
            CHECK(point.Z() == 3);
        }
        SECTION("Min()") {
            Point point1(1, 2, 3);
            Point point2(3, 2, 1);

            CHECK(Point::Min(point1, point2) == Point(1,2,1));
        }
        SECTION("Max()") {
            Point point1(1, 2, 3);
            Point point2(3, 2, 1);

            CHECK(Point::Max(point1, point2) == Point(3, 2, 3));
        }
    }
}
