#include <catch2/catch.hpp>

#include "CommonUtilities.h"

using namespace TC::IO;

namespace CommonUtilitiesTest {
    auto CheckMemoryChunk(const MemoryChunk::Pointer& memoryChunk, const ChunkDataType& ansChunkDataType,
        void* ansDataPointer, const Dimension& ansDimension) -> bool {
        const auto dataTypeSame = memoryChunk->GetDataType() == +ansChunkDataType;
        const auto pointerSame = memoryChunk->GetDataPointer() == ansDataPointer;
        const auto dimensionSame = memoryChunk->GetDimension() == ansDimension;
        return dataTypeSame && pointerSame && dimensionSame;
    }

    TEST_CASE("CommonUtilities") {
        SECTION("AllocateRawData(ChunkDataType)") {
            const size_t numberOfElements = 10;
            auto int8Pointer = AllocateRawData(+ChunkDataType::Int8Type, numberOfElements);
            auto uint8Pointer = AllocateRawData(+ChunkDataType::UInt8Type, numberOfElements);
            auto int16Pointer = AllocateRawData(+ChunkDataType::Int16Type, numberOfElements);
            auto uint16Pointer = AllocateRawData(+ChunkDataType::UInt16Type, numberOfElements);
            auto int32Pointer = AllocateRawData(+ChunkDataType::Int32Type, numberOfElements);
            auto uint32Pointer = AllocateRawData(+ChunkDataType::UInt32Type, numberOfElements);
            auto floatPointer = AllocateRawData(+ChunkDataType::FloatType, numberOfElements);
            auto doublePointer = AllocateRawData(+ChunkDataType::DoubleType, numberOfElements);

            CHECK(int8Pointer != nullptr);
            CHECK(uint8Pointer != nullptr);
            CHECK(int16Pointer != nullptr);
            CHECK(uint16Pointer != nullptr);
            CHECK(int32Pointer != nullptr);
            CHECK(uint32Pointer != nullptr);
            CHECK(floatPointer != nullptr);
            CHECK(doublePointer != nullptr);

            delete[] static_cast<int8_t*>(int8Pointer);
            delete[] static_cast<uint8_t*>(uint8Pointer);
            delete[] static_cast<int16_t*>(int16Pointer);
            delete[] static_cast<uint16_t*>(uint16Pointer);
            delete[] static_cast<int32_t*>(int32Pointer);
            delete[] static_cast<uint32_t*>(uint32Pointer);
            delete[] static_cast<float*>(floatPointer);
            delete[] static_cast<double*>(doublePointer);
        }

        SECTION("GenerateMemoryChunk()") {
            const size_t numberOfElements = 10;
            const Dimension dimension(2, 5);
            auto int8Pointer = AllocateRawData(+ChunkDataType::Int8Type, numberOfElements);
            auto uint8Pointer = AllocateRawData(+ChunkDataType::UInt8Type, numberOfElements);
            auto int16Pointer = AllocateRawData(+ChunkDataType::Int16Type, numberOfElements);
            auto uint16Pointer = AllocateRawData(+ChunkDataType::UInt16Type, numberOfElements);
            auto int32Pointer = AllocateRawData(+ChunkDataType::Int32Type, numberOfElements);
            auto uint32Pointer = AllocateRawData(+ChunkDataType::UInt32Type, numberOfElements);
            auto floatPointer = AllocateRawData(+ChunkDataType::FloatType, numberOfElements);
            auto doublePointer = AllocateRawData(+ChunkDataType::DoubleType, numberOfElements);

            auto int8Chunk = GenerateMemoryChunk(int8Pointer, dimension, ChunkDataType::Int8Type);
            auto uint8Chunk = GenerateMemoryChunk(uint8Pointer, dimension, ChunkDataType::UInt8Type);
            auto int16Chunk = GenerateMemoryChunk(int16Pointer, dimension, ChunkDataType::Int16Type);
            auto uint16Chunk = GenerateMemoryChunk(uint16Pointer, dimension, ChunkDataType::UInt16Type);
            auto int32Chunk = GenerateMemoryChunk(int32Pointer, dimension, ChunkDataType::Int32Type);
            auto uint32Chunk = GenerateMemoryChunk(uint32Pointer, dimension, ChunkDataType::UInt32Type);
            auto floatChunk = GenerateMemoryChunk(floatPointer, dimension, ChunkDataType::FloatType);
            auto doubleChunk = GenerateMemoryChunk(doublePointer, dimension, ChunkDataType::DoubleType);

            CheckMemoryChunk(int8Chunk, ChunkDataType::Int8Type, int8Pointer, dimension);
            CheckMemoryChunk(uint8Chunk, ChunkDataType::UInt8Type, uint8Pointer, dimension);
            CheckMemoryChunk(int16Chunk, ChunkDataType::Int16Type, int16Pointer, dimension);
            CheckMemoryChunk(uint16Chunk, ChunkDataType::UInt16Type, uint16Pointer, dimension);
            CheckMemoryChunk(int32Chunk, ChunkDataType::Int32Type, int32Pointer, dimension);
            CheckMemoryChunk(uint32Chunk, ChunkDataType::UInt32Type, uint32Pointer, dimension);
            CheckMemoryChunk(floatChunk, ChunkDataType::FloatType, floatPointer, dimension);
            CheckMemoryChunk(doubleChunk, ChunkDataType::DoubleType, doublePointer, dimension);
        }

        SECTION("CheckPointIsIncludedInDimension()") {
            SECTION("2D") {
                const Dimension dimension{ 5,5 };
                SECTION("out bounded") {
                    SECTION("small part") {
                        SECTION("x target") {
                            const Point point{ -1,0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("y target") {
                            const Point point{ 0,-1 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }
                    }
                    SECTION("large part") {
                        SECTION("x target") {
                            const Point point{ 5,0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("y target") {
                            const Point point{ 0,5 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }
                    }
                }
                SECTION("in bounded") {
                    const Point point{ 0,0 };
                    const auto pointIsIncluded = CheckPointIsIncludedInDimension(point, dimension);
                    CHECK(pointIsIncluded);
                }
            }
            SECTION("3D") {
                const Dimension dimension{ 5,5,5 };
                SECTION("out bounded") {
                    SECTION("small part") {
                        SECTION("x target") {
                            const Point point{ -1, 0, 0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("y target") {
                            const Point point{ 0,-1, 0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("z target") {
                            const Point point{ 0, 0,-1 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }
                    }
                    SECTION("large part") {
                        SECTION("x target") {
                            const Point point{ 5, 0, 0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("y target") {
                            const Point point{ 0, 5, 0 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }

                        SECTION("z target") {
                            const Point point{ 0, 0, 5 };
                            const auto pointIsNotIncluded = !CheckPointIsIncludedInDimension(point, dimension);
                            CHECK(pointIsNotIncluded);
                        }
                    }
                }
                SECTION("in bounded") {
                    const Point point{ 0,0,0 };
                    const auto pointIsIncluded = CheckPointIsIncludedInDimension(point, dimension);
                    CHECK(pointIsIncluded);
                }
            }
        }

        SECTION("CheckReadingRangeIsIncludedInDimension()") {
            SECTION("2D") {
                const Dimension dimension{ 5,5 };
                SECTION("out bounded") {
                    SECTION("offset Point target") {
                        const DataRange readingRange{ Point{-1,0}, Point{1,1} };
                        const auto dataRangeIsNotIncluded = !CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsNotIncluded);
                    }
                    SECTION("last Point target") {
                        const DataRange readingRange{ Point{0,0}, Point{1,5} };
                        const auto dataRangeIsNotIncluded = !CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsNotIncluded);
                    }
                }

                SECTION("in bounded") {
                    SECTION("offset Point target") {
                        const DataRange readingRange{ Point{0,0}, Point{1,1} };
                        const auto dataRangeIsIncluded = CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsIncluded);
                    }
                }
            }
            SECTION("3D") {
                const Dimension dimension{ 5,5,5 };
                SECTION("out bounded") {
                    SECTION("offset Point target") {
                        const DataRange readingRange{ Point{-1,0,0}, Point{1,1,1} };
                        const auto dataRangeIsNotIncluded = !CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsNotIncluded);
                    }
                    SECTION("last Point target") {
                        const DataRange readingRange{ Point{0,0,0}, Point{1,1,5} };
                        const auto dataRangeIsNotIncluded = !CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsNotIncluded);
                    }
                }

                SECTION("in bounded") {
                    SECTION("offset Point target") {
                        const DataRange readingRange{ Point{0,0,0}, Point{1,1,1} };
                        const auto dataRangeIsIncluded = CheckReadingRangeIsIncludedInDimension(readingRange, dimension);
                        CHECK(dataRangeIsIncluded);
                    }
                }
            }
        }
    }
}