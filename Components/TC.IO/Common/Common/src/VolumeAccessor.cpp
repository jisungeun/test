#include <tuple>

#include "VolumeAccessor.h"

namespace TC::IO {    
    struct IVolumeAccessor::Impl {
        int32_t start[3] = {-1,-1,-1};
        uint32_t size[3] = {1,1,1};

        uint32_t dimension = { 2 };
        uint32_t dims[3] = { 1,1,1 };

        QString datasetName;        
    };
    
    IVolumeAccessor::IVolumeAccessor() : d{ new Impl } {
        
    }

    IVolumeAccessor::IVolumeAccessor(const IVolumeAccessor& other) : d{ new Impl } {
        *d = *(other.d);
    }
    
    IVolumeAccessor::~IVolumeAccessor() {
        
    }
    
    IVolumeAccessor& IVolumeAccessor::operator=(const IVolumeAccessor& other) {
        *d = *(other.d);
        return (*this);
    }    
    
    auto IVolumeAccessor::SetDimensions(uint32_t xDim, uint32_t yDim, uint32_t zDim) -> void {
        d->dims[0] = xDim;
        d->dims[1] = yDim;
        d->dims[2] = zDim;

        d->dimension = (zDim == 1) ? 2 : 3;        
    }
    
    auto IVolumeAccessor::GetDimensions() const -> std::tuple<uint32_t, uint32_t, uint32_t> {
        return std::make_tuple(d->dims[0], d->dims[1], d->dims[2]);
    }
    
    auto IVolumeAccessor::SetRegion(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void {
        d->start[0] = xMin;
        d->start[1] = yMin;
        d->start[2] = zMin;

        d->size[0] = xMax - xMin + 1;
        d->size[1] = yMax - yMin + 1;
        d->size[2] = zMax - zMin + 1;
    }
    
    auto IVolumeAccessor::GetRegion() -> const std::tuple<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t> {
        return std::make_tuple(d->start[0], d->start[0] + d->size[0] - 1,
                               d->start[1], d->start[1] + d->size[1] - 1,
                               d->start[2], d->start[2] + d->size[2] - 1);
    }

    auto IVolumeAccessor::GetStart() -> const std::tuple<uint32_t, uint32_t, uint32_t> {
        return std::make_tuple(d->start[0], d->start[1], d->start[2]);
    }

    auto IVolumeAccessor::GetSize() -> const std::tuple<uint32_t, uint32_t, uint32_t> {
        return std::make_tuple(d->size[0], d->size[1], d->size[2]);
    }


    auto IVolumeAccessor::isValidCoord(uint32_t x, uint32_t y, uint32_t z) -> bool {
        if(d->dimension == 2) {
            return d->dims[0] > x && d->dims[1] > y;
        }else {
            return d->dims[0] > x && d->dims[1] > y && d->dims[2] > z;
        }
    }
    auto IVolumeAccessor::isValidRegion() -> bool {
        if(d->dimension == 2) {
            return d->dims[0] > (d->start[0] + d->size[0] - 1) &&
                d->dims[1] > (d->start[1] + d->size[1] - 1);
        }else {
            return d->dims[0] > (d->start[0] + d->size[0] - 1) &&
                d->dims[1] > (d->start[1] + d->size[1] - 1) &&
                d->dims[2] > (d->start[2] + d->size[2] - 1);
        }
    }
    auto IVolumeAccessor::SetDataSetName(QString dset) -> void {
        d->datasetName = dset;
    }
    auto IVolumeAccessor::GetDataSetName() -> QString {
        return d->datasetName;
    }
    auto IVolumeAccessor::GetDimension() -> uint32_t {
        return d->dimension;
    }
}