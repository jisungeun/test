#include <any>
#include <tuple>
#include <QMap>

#include "VolumeData.h"

namespace TC::IO {
    struct VolumeData::Impl {
        bool valid{ false };
        size_t dimension{ 2 };
        size_t dims[3]{ 1, 1, 1 };
        double resolution[3]{ 1.0, 1.0, 1.0 };
        double range[2]{ 0.0, 0.0 };
        std::any rawdata;        

        QMap<Type, QString> typeString{
            {Type::Int8Type,    "class std::shared_ptr<signed char [0]>"},
            {Type::UInt8Type,   "class std::shared_ptr<unsigned char [0]>"},
            {Type::Int16Type,   "class std::shared_ptr<short [0]>"},
            {Type::UInt16Type,  "class std::shared_ptr<unsigned short [0]>"},
            {Type::Int32Type,   "class std::shared_ptr<int [0]>"},
            {Type::UInt32Type,  "class std::shared_ptr<unsigned int [0]>"},
            {Type::FloatType,   "class std::shared_ptr<float [0]>"},
            {Type::DoubleType,  "class std::shared_ptr<double [0]>"}
        };

        auto elements() const->size_t { return dims[0] * dims[1] * dims[2]; }
        auto typeName() const->const char* { return rawdata.type().name(); }
        auto isType(Type type) const->bool {
            const auto name = typeString[type];
            const auto currentType = QString::fromLatin1(typeName());
            return name == currentType;
        }
        auto type()->Type {
            for(auto itr=typeString.begin(); itr!=typeString.end(); ++itr) {
                if (isType(itr.key())) return itr.key();
            }
            return Type::Int8Type;
        }
    };

    VolumeData::VolumeData() : d{ new Impl } {
    }

    VolumeData::VolumeData(const VolumeData& other) : d{ new Impl } {
        *d = *(other.d);
    }

    VolumeData::~VolumeData() {
        
    }

    VolumeData& VolumeData::operator=(const VolumeData& other) {
        *d = *(other.d);
        return (*this);
    }

    auto VolumeData::SetValid(bool bValid) -> void {
        d->valid = bValid;
    }

    auto VolumeData::IsValid() const -> bool {
        return d->valid;
    }

    auto VolumeData::SetDimension(size_t xDim, size_t yDim, size_t zDim) -> void {
        d->dims[0] = xDim;
        d->dims[1] = yDim;
        d->dims[2] = zDim;

        d->dimension = (zDim == 1) ? 2 : 3;
        d->rawdata.reset();
    }

    auto VolumeData::GetDimension() const -> std::tuple<size_t, size_t, size_t> {
        return std::make_tuple(d->dims[0], d->dims[1], d->dims[2]);
    }

    auto VolumeData::GetElements() const -> size_t {
        return d->elements();
    }

    auto VolumeData::GetXYElements() const -> size_t {
        return d->dims[0] * d->dims[1];
    }

    auto VolumeData::SetResolution(double xResolution, double yResolution, double zResolution) -> void {
        d->resolution[0] = xResolution;
        d->resolution[1] = yResolution;
        d->resolution[2] = zResolution;
    }

    auto VolumeData::GetResolution() -> std::tuple<double, double, double> {
        return std::make_tuple(d->resolution[0], d->resolution[1], d->resolution[2]);
    }

    auto VolumeData::SetRange(double minValue, double maxValue) -> void {
        d->range[0] = minValue;
        d->range[1] = maxValue;
    }

    auto VolumeData::GetRange() const -> std::tuple<double, double> {
        return std::make_tuple(d->range[0], d->range[1]);
    }

    auto VolumeData::SetRawData(int8_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<int8_t[]>(new int8_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(int8_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(uint8_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<uint8_t[]>(new uint8_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(uint8_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(int16_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<int16_t[]>(new int16_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(int16_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(uint16_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<uint16_t[]>(new uint16_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(uint16_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(int32_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<int32_t[]>(new int32_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(int32_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(uint32_t* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<uint32_t[]>(new uint32_t[elements]);
        std::memcpy(rawData.get(), data, sizeof(uint32_t) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(float* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<float[]>(new float[elements]);
        std::memcpy(rawData.get(), data, sizeof(float) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::SetRawData(double* data, size_t elements) -> void {
        auto rawData = std::shared_ptr<double[]>(new double[elements]);
        std::memcpy(rawData.get(), data, sizeof(double) * elements);
        d->rawdata = rawData;
    }

    auto VolumeData::GetDataType() const -> Type {
        return d->type();
    }

    auto VolumeData::GetRawData() -> std::any {
        return d->rawdata;
    }

    auto VolumeData::GetDataAsInt8() -> std::shared_ptr<int8_t[]> {
        if (!d->isType(Type::Int8Type)) return nullptr;
        return std::any_cast<std::shared_ptr<int8_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsUInt8() -> std::shared_ptr<uint8_t[]> {
        if (!d->isType(Type::UInt8Type)) return nullptr;
        return std::any_cast<std::shared_ptr<uint8_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsInt16() -> std::shared_ptr<int16_t[]> {
        if (!d->isType(Type::Int16Type)) return nullptr;
        return std::any_cast<std::shared_ptr<int16_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsUInt16() -> std::shared_ptr<uint16_t[]> {
        if (!d->isType(Type::UInt16Type)) return nullptr;
        return std::any_cast<std::shared_ptr<uint16_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsInt32() -> std::shared_ptr<int32_t[]> {
        if (!d->isType(Type::Int32Type)) return nullptr;
        return std::any_cast<std::shared_ptr<int32_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsUInt32() -> std::shared_ptr<uint32_t[]> {
        if (!d->isType(Type::UInt32Type)) return nullptr;
        return std::any_cast<std::shared_ptr<uint32_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsFloat() -> std::shared_ptr<float[]> {
        if (!d->isType(Type::FloatType)) return nullptr;
        return std::any_cast<std::shared_ptr<float_t[]>>(d->rawdata);
    }

    auto VolumeData::GetDataAsDouble() -> std::shared_ptr<double[]> {
        if (!d->isType(Type::DoubleType)) return nullptr;
        return std::any_cast<std::shared_ptr<double_t[]>>(d->rawdata);
    }

    auto VolumeData::AllocateAsInt8(size_t elements) -> std::shared_ptr<int8_t[]> {
        auto rawdata = std::shared_ptr<int8_t[]>(new int8_t[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsUInt8(size_t elements) -> std::shared_ptr<uint8_t[]> {
        auto rawdata = std::shared_ptr<uint8_t[]>(new uint8_t[elements]);
        d->rawdata = rawdata;
        return rawdata;

    }

    auto VolumeData::AllocateAsInt16(size_t elements) -> std::shared_ptr<int16_t[]> {
        auto rawdata = std::shared_ptr<int16_t[]>(new int16_t[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsUInt16(size_t elements) -> std::shared_ptr<uint16_t[]> {
        auto rawdata = std::shared_ptr<uint16_t[]>(new uint16_t[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsInt32(size_t elements) -> std::shared_ptr<int32_t[]> {
        auto rawdata = std::shared_ptr<int32_t[]>(new int32_t[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsUInt32(size_t elements) -> std::shared_ptr<uint32_t[]> {
        auto rawdata = std::shared_ptr<uint32_t[]>(new uint32_t[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsFloat(size_t elements) -> std::shared_ptr<float[]> {
        auto rawdata = std::shared_ptr<float[]>(new float[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }

    auto VolumeData::AllocateAsDouble(size_t elements) -> std::shared_ptr<double[]> {
        auto rawdata = std::shared_ptr<double[]>(new double[elements]);
        d->rawdata = rawdata;
        return rawdata;
    }
}
