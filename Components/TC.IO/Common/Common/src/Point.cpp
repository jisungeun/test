#include "Point.h"

#include <algorithm>

namespace TC::IO {
    struct Point::Impl {
        Impl() = default;
        ~Impl() = default;

        uint64_t dimensionality{ 0 };
        int64_t x{ 0 };
        int64_t y{ 0 };
        int64_t z{ 0 };
    };

    Point::Point()
        : d(new Impl()) {
    };

    Point::Point(const Point& other)
        : d(new Impl(*other.d)){
    }

    Point::Point(const int64_t& x, const int64_t& y)
        : d (new Impl()) {
        SetPoint(x, y);
    }

    Point::Point(const int64_t& x, const int64_t& y, const int64_t& z)
        : d (new Impl()) {
        SetPoint(x, y, z);
    }

    Point::~Point() = default;

    auto Point::operator=(const Point& other) -> Point& {
        *d = *(other.d);
        return *this;
    }

    auto Point::operator==(const Point& other) const -> bool {
        const auto dimensionalitySame = d->dimensionality == other.d->dimensionality;
        const auto xSame = d->x == other.d->x;
        const auto ySame = d->y == other.d->y;
        const auto zSame = d->z == other.d->z;
        return dimensionalitySame && xSame && ySame && zSame;
    }

    auto Point::operator!=(const Point& other) const -> bool {
        return !(*this == other);
    }

    auto Point::operator-(const Point& other) const -> Point {
        if (d->dimensionality != other.d->dimensionality) {
            return Point{};
        }

        const auto x = d->x - other.d->x;
        const auto y = d->y - other.d->y;
        const auto z = d->z - other.d->z;

        const auto is2D = (d->dimensionality == 2);
        const auto is3D = (d->dimensionality == 3);

        Point point;
        if (is2D) {
            point = Point(x, y);
        } else if (is3D) {
            point = Point(x, y, z);
        }

        return point;
    }

    auto Point::operator+(const Point& other) const -> Point {
        if (d->dimensionality != other.d->dimensionality) {
            return Point{};
        }

        const auto x = d->x + other.d->x;
        const auto y = d->y + other.d->y;
        const auto z = d->z + other.d->z;

        const auto is2D = (d->dimensionality == 2);
        const auto is3D = (d->dimensionality == 3);

        Point point;
        if (is2D) {
            point = Point(x, y);
        } else if (is3D) {
            point = Point(x, y, z);
        }

        return point;

    }

    auto Point::operator+(const Count& count) const -> Point {
        if (d->dimensionality == 2) {
            const auto pointX = d->x + static_cast<int64_t>(count.X()) - 1;
            const auto pointY = d->y + static_cast<int64_t>(count.Y()) - 1;
            return Point(pointX, pointY);
        } else if (d->dimensionality == 3) {
            const auto pointX = d->x + static_cast<int64_t>(count.X()) - 1;
            const auto pointY = d->y + static_cast<int64_t>(count.Y()) - 1;
            auto pointZ = d->z;
            if (count.GetDimensionality() == 3) {
                pointZ = d->z + static_cast<int64_t>(count.Z()) - 1;
            }
            return Point(pointX, pointY, pointZ);
        }
        return Point{};
    }

    auto Point::operator/(const int64_t& divider) const -> Point {
        const auto pointX = d->x / divider;
        const auto pointY = d->y / divider;
        const auto pointZ = d->z / divider;
        return Point(pointX, pointY, pointZ);
    }

    auto Point::SetPoint(const int64_t& x, const int64_t& y) ->void {
        d->dimensionality = 2;
        d->x = x;
        d->y = y;
        d->z = 0;
    }

    auto Point::SetPoint(const int64_t& x, const int64_t& y, const int64_t& z) ->void {
        d->dimensionality = 3;
        d->x = x;
        d->y = y;
        d->z = z;
    }

    auto Point::GetDimensionality() const -> uint64_t {
        return d->dimensionality;
    }

    auto Point::X() const -> int64_t {
        return d->x;
    }

    auto Point::Y() const -> int64_t {
        return d->y;
    }

    auto Point::Z() const -> int64_t {
        return d->z;
    }

    auto Point::Min(const Point & point1, const Point & point2) -> Point {
        Point minPoint{};
        if (point1.GetDimensionality() == point2.GetDimensionality()) {
            const auto pointX = std::min<int64_t>(point1.X(), point2.X());
            const auto pointY = std::min<int64_t>(point1.Y(), point2.Y());

            int64_t pointZ = 0;
            if (point1.GetDimensionality() == 3) {
                pointZ = std::min<int64_t>(point1.Z(), point2.Z());;
                minPoint = Point(pointX, pointY, pointZ);
            } else {
                minPoint = Point(pointX, pointY);
            }
        }
        return minPoint;
    }

    auto Point::Max(const Point& point1, const Point& point2) -> Point {
        Point maxPoint{};
        if (point1.GetDimensionality() == point2.GetDimensionality()) {
            const auto pointX = std::max<int64_t>(point1.X(), point2.X());
            const auto pointY = std::max<int64_t>(point1.Y(), point2.Y());

            if (point1.GetDimensionality() == 3) {
                const auto pointZ = std::max<int64_t>(point1.Z(), point2.Z());;
                maxPoint = Point(pointX, pointY, pointZ);
            } else {
                maxPoint = Point(pointX, pointY);
            }
        }
        return maxPoint;
    }
}
