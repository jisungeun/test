#include "Dimension.h"
#include "Count.h"
namespace TC::IO {
    struct Dimension::Impl {
        Impl() = default;
        ~Impl() = default;

        size_t dimensionality{ 0 };
        size_t x{ 0 };
        size_t y{ 0 };
        size_t z{ 0 };
    };

    Dimension::Dimension()
        : d(new Impl()) {
    }

    Dimension::Dimension(const Dimension& other)
        : d(new Impl(*other.d)) {
    }

    Dimension::Dimension(const Count& count)
        : d(new Impl()) {
        d->dimensionality = count.GetDimensionality();
        d->x = count.X();
        d->y = count.Y();

        if (d->dimensionality == 3) {
            d->z = count.Z();
        } else if (d->dimensionality == 2) {
            d->z = 1;
        }
    }

    Dimension::Dimension(const size_t& x, const size_t& y)
        : d(new Impl()) {
        d->dimensionality = 2;
        d->x = x;
        d->y = y;
        d->z = 1;
    }

    Dimension::Dimension(const size_t& x, const size_t& y, const size_t& z)
        : d(new Impl()){
        d->dimensionality = 3;
        d->x = x;
        d->y = y;
        d->z = z;
    }

    Dimension::~Dimension() = default;

    auto Dimension::operator=(const Dimension& other) -> Dimension& {
        *d = *(other.d);
        return *this;
    }

    auto Dimension::operator==(const Dimension& other) const -> bool {
        const auto dimensionalitySame = d->dimensionality == other.d->dimensionality;
        const auto xSame = d->x == other.d->x;
        const auto ySame = d->y == other.d->y;
        const auto zSame = d->z == other.d->z;
        return dimensionalitySame && xSame && ySame && zSame;
    }

    auto Dimension::operator!=(const Dimension& other) const -> bool {
        return !(*this == other);
    }

    auto Dimension::GetDimensionality() const -> size_t {
        return d->dimensionality;
    }

    auto Dimension::X() const -> size_t {
        return d->x;
    }

    auto Dimension::Y() const -> size_t {
        return d->y;
    }

    auto Dimension::Z() const -> size_t {
        return d->z;
    }

    auto Dimension::GetNumberOfElements() const -> size_t {
        const auto dimensionIs2D = (d->dimensionality == 2);
        const auto dimensionIs3D = (d->dimensionality == 3);

        if (dimensionIs2D) {
            return (d->x * d->y);
        } else if (dimensionIs3D) {
            return (d->x * d->y * d->z);
        } else {
            return 0;
        }
    }
}
