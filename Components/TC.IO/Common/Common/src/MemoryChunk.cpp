#include "MemoryChunk.h"

#include <QString>
#include <QMap>

namespace TC::IO {
    struct MemoryChunk::Impl {
        Impl() = default;
        ~Impl() = default;

        Dimension dimension{};
        std::any data{};
        ChunkDataType dataType{ ChunkDataType::Int8Type };
    };

    MemoryChunk::MemoryChunk()
        : d(new Impl()) {
    }

    MemoryChunk::~MemoryChunk() = default;

    auto MemoryChunk::SetData(const std::shared_ptr<int8_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::Int8Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<uint8_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::UInt8Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<int16_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::Int16Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<uint16_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::UInt16Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<int32_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::Int32Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<uint32_t[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::UInt32Type;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<float[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::FloatType;
        d->data = data;
    }

    auto MemoryChunk::SetData(const std::shared_ptr<double[]>& data, const Dimension& dimension) -> void {
        d->dimension = dimension;
        d->dataType = ChunkDataType::DoubleType;
        d->data = data;
    }

    auto MemoryChunk::GetDataType() const -> ChunkDataType {
        return d->dataType;
    }

    auto MemoryChunk::GetData() const -> std::any {
        return d->data;
    }

    auto MemoryChunk::GetDataPointer() const -> void* {
        void* rawDataPointer{};
        if (d->dataType == +ChunkDataType::Int8Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<int8_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::UInt8Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<uint8_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::Int16Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<int16_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::UInt16Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<uint16_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::Int32Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<int32_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::UInt32Type) {
            rawDataPointer = std::any_cast<std::shared_ptr<uint32_t[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::FloatType) {
            rawDataPointer = std::any_cast<std::shared_ptr<float[]>>(d->data).get();
        } else if (d->dataType == +ChunkDataType::DoubleType) {
            rawDataPointer = std::any_cast<std::shared_ptr<double[]>>(d->data).get();
        }
        return rawDataPointer;
    }

    auto MemoryChunk::GetDimension() const -> Dimension {
        return d->dimension;
    }

    auto MemoryChunk::GetSizeInBytes() const -> size_t {
        const auto elementSize = [&]()->size_t {
            switch(d->dataType) {
            case ChunkDataType::Int8Type:
            case ChunkDataType::UInt8Type:
                return 1;
            case ChunkDataType::Int16Type:
            case ChunkDataType::UInt16Type:
                return 2;
            case ChunkDataType::Int32Type:
            case ChunkDataType::UInt32Type:
            case ChunkDataType::FloatType:
                return 4;
            case ChunkDataType::DoubleType:
                return 8;
            }
            return 1;
        }();
        const auto elements = d->dimension.GetNumberOfElements();
        return elementSize * elements;
    }

    auto MemoryChunk::ZeroPadding(const Dimension& targetDimension) -> void {
        if (d->dimension == targetDimension) {
            return;
        }

        const auto numberOfTargetElements = targetDimension.GetNumberOfElements();

        if (d->dataType._value == +ChunkDataType::Int8Type) {
            const auto paddedData = std::shared_ptr<int8_t[]>(new int8_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<int8_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::UInt8Type) {
            const auto paddedData = std::shared_ptr<uint8_t[]>(new uint8_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<uint8_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::Int16Type) {
            const auto paddedData = std::shared_ptr<int16_t[]>(new int16_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<int16_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::UInt16Type) {
            const auto paddedData = std::shared_ptr<uint16_t[]>(new uint16_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<uint16_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::Int32Type) {
            const auto paddedData = std::shared_ptr<int32_t[]>(new int32_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<int32_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::UInt32Type) {
            const auto paddedData = std::shared_ptr<uint32_t[]>(new uint32_t[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<uint32_t[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::FloatType) {
            const auto paddedData = std::shared_ptr<float[]>(new float[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<float[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        } else if (d->dataType._value == +ChunkDataType::DoubleType) {
            const auto paddedData = std::shared_ptr<double[]>(new double[numberOfTargetElements]());
            const auto srcData = std::any_cast<std::shared_ptr<double[]>>(d->data);
            CopyMemory(srcData.get(), paddedData.get(), d->dimension, targetDimension);
            d->data = paddedData;

        }

        d->dimension = targetDimension;
    }

    template <class T>
    auto MemoryChunk::CopyMemory(T* srcData, T* destData, const Dimension& srcDimension,
        const Dimension& destDimension) -> void {
        for (size_t k = 0; k < srcDimension.Z(); ++k) {
            for (size_t j = 0; j < srcDimension.Y(); ++j) {
                const auto srcIndex = j * srcDimension.X() + k * srcDimension.X() * srcDimension.Y();
                const auto destIndex = j * destDimension.X() + k * destDimension.X() * destDimension.Y();
                std::copy(&(srcData[srcIndex]), &(srcData[srcIndex]) + srcDimension.X(), &(destData[destIndex]));
            }
        }
    }
}
