#include <tuple>
#include <any>
#include <QMap>

#include "VolumeMetaInfo.h"

namespace TC::IO {
    struct VolumeMetaInfo::Impl {
        bool valid{ false };
        uint32_t dimension{ 2 };
        uint32_t dims[3]{ 1,1,1 };
        double resolution[3]{ 1.0,1.0,1.0 };
        FType type{FType::TCFType};
        QString file_path;

        auto elements() const->uint32_t { return dims[0] * dims[1] * dims[2]; }
        auto isType(FType t) const->bool {
            return type == t;
        }        
    };
    VolumeMetaInfo::VolumeMetaInfo() : d{ new Impl } {
        
    }
    VolumeMetaInfo::VolumeMetaInfo(const VolumeMetaInfo& other) : d{ new Impl } {
        *d = *(other.d);
    }
    VolumeMetaInfo::~VolumeMetaInfo() {
        
    }
    VolumeMetaInfo& VolumeMetaInfo::operator=(const VolumeMetaInfo& other) {
        *d = *(other.d);
        return (*this);
    }
    auto VolumeMetaInfo::SetValid(bool bValid) -> void {
        d->valid = bValid;
    }
    auto VolumeMetaInfo::IsValid() const -> bool {
        return d->valid;
    }
    auto VolumeMetaInfo::SetDimension(uint32_t xDim, uint32_t yDim, uint32_t zDim) -> void {
        d->dims[0] = xDim;
        d->dims[1] = yDim;
        d->dims[2] = zDim;

        d->dimension = (zDim == 1) ? 2 : 3;
        
    }
    auto VolumeMetaInfo::GetDimension() const -> std::tuple<uint32_t, uint32_t, uint32_t> {
        return std::make_tuple(d->dims[0], d->dims[1], d->dims[2]);
    }
    auto VolumeMetaInfo::GetElements() const -> uint32_t {
        return d->elements();
    }
    auto VolumeMetaInfo::GetXYElements() const -> uint32_t {
        return d->dims[0] * d->dims[1];
    }
    auto VolumeMetaInfo::SetResolution(double xResolution, double yResolution, double zResolution) -> void {
        d->resolution[0] = xResolution;
        d->resolution[1] = yResolution;
        d->resolution[2] = zResolution;
    }
    auto VolumeMetaInfo::GetResolution() -> std::tuple<double, double, double> {
        return std::make_tuple(d->resolution[0], d->resolution[1], d->resolution[2]);
    }
    auto VolumeMetaInfo::SetFileType(FType type) -> void {
        d->type = type;
    }
    auto VolumeMetaInfo::GetFileType() -> FType {
        return d->type;
    }
    auto VolumeMetaInfo::SetFilePath(QString path) -> void {
        d->file_path = path;
    }
    auto VolumeMetaInfo::GetFilePath() -> QString {
        return d->file_path;
    }
}