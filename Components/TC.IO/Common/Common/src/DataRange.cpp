#include "DataRange.h"
#include <algorithm>
namespace TC::IO {
    struct DataRange::Impl {
        Impl() = default;
        ~Impl() = default;

        Point offsetPoint{};
        Count count{};
    };

    DataRange::DataRange()
        : d(new Impl()) {
    }

    DataRange::DataRange(const DataRange& other)
        : d(new Impl(*other.d)) {
    }

    DataRange::DataRange(const Point& offsetPoint, const Point& lastPoint)
        : d(new Impl()) {
        if (offsetPoint.GetDimensionality() != lastPoint.GetDimensionality()) {
            return;
        }

        d->offsetPoint = offsetPoint;
        size_t countX{ 0 }, countY{ 0 }, countZ{ 0 };
        if (offsetPoint.X() <= lastPoint.X()) {
            countX = lastPoint.X() - offsetPoint.X() + 1;
        }
        if (offsetPoint.Y() <= lastPoint.Y()) {
            countY = lastPoint.Y() - offsetPoint.Y() + 1;
        }
        if (offsetPoint.Z() <= lastPoint.Z()) {
            countZ = lastPoint.Z() - offsetPoint.Z() + 1;
        }

        const auto rangeIs2D = (offsetPoint.GetDimensionality() == 2);
        const auto rangeIs3D = (offsetPoint.GetDimensionality() == 3);

        if (rangeIs2D) {
            d->count = Count(countX, countY);
        } else if (rangeIs3D) {
            d->count = Count(countX, countY, countZ);
        }
    }

    DataRange::DataRange(const Point& offsetPoint, const Count& count)
        : d(new Impl()) {
        d->offsetPoint = offsetPoint;
        d->count = count;
    }

    DataRange::~DataRange() = default;

    auto DataRange::operator=(const DataRange& other) -> DataRange& {
        *d = *(other.d);
        return *this;
    }

    auto DataRange::operator==(const DataRange& other) const -> bool {
        const auto offsetPointSame = (d->offsetPoint == other.d->offsetPoint);
        const auto countSame = (d->count == other.d->count);
        return offsetPointSame && countSame;
    }

    auto DataRange::operator!=(const DataRange& other) const -> bool {
        return !(*this == other);
    }

    auto DataRange::GetDimensionality() const -> size_t {
        return d->count.GetDimensionality();
    }

    auto DataRange::GetOffsetPoint() const -> Point {
        return d->offsetPoint;
    }

    auto DataRange::GetLastPoint() const -> Point {
        const auto rangeIs2D = (d->offsetPoint.GetDimensionality() == 2);
        const auto rangeIs3D = (d->offsetPoint.GetDimensionality() == 3);

        Point lastPoint;

        if (rangeIs2D) {
            const auto lastPointX = d->offsetPoint.X() + d->count.X() - 1;
            const auto lastPointY = d->offsetPoint.Y() + d->count.Y() - 1;

            lastPoint = Point(lastPointX, lastPointY);
        } else if (rangeIs3D) {
            const auto lastPointX = d->offsetPoint.X() + d->count.X() - 1;
            const auto lastPointY = d->offsetPoint.Y() + d->count.Y() - 1;
            const auto lastPointZ = d->offsetPoint.Z() + d->count.Z() - 1;

            lastPoint = Point(lastPointX, lastPointY, lastPointZ);
        }
        return lastPoint;
    }

    auto DataRange::GetCount() const -> Count {
        return d->count;
    }

    auto DataRange::IsEmpty() const -> bool {
        const auto countIsEmpty = d->count.GetTotalNumberOfCount() == 0;
        return countIsEmpty;
    }

    auto DataRange::GetOverlapRange(const DataRange& dataRange) const -> DataRange {
        if(GetDimensionality() != dataRange.GetDimensionality()) {
            return DataRange{};
        }
        const auto overlapOffsetPoint = Point::Max(GetOffsetPoint(), dataRange.GetOffsetPoint());
        const auto overlapLastPoint = Point::Min(GetLastPoint(), dataRange.GetLastPoint());
        return DataRange(overlapOffsetPoint, overlapLastPoint);
    }

    auto DataRange::GetOverlapOffsetPoint(const size_t& offsetPoint1, const size_t& offsetPoint2) -> size_t {
        size_t overlapOffsetPoint;
        if (offsetPoint1 < offsetPoint2) {
            overlapOffsetPoint = offsetPoint2;
        } else {
            overlapOffsetPoint = offsetPoint1;
        }
        return overlapOffsetPoint;
    }

    auto DataRange::GetOverlapLastPoint(const size_t& lastPoint1, const size_t& lastPoint2) -> size_t {
        size_t overlapLastPoint;
        if (lastPoint1 < lastPoint2) {
            overlapLastPoint = lastPoint1;
        } else {
            overlapLastPoint = lastPoint2;
        }
        return overlapLastPoint;
    }
}
