#include "CommonUtilities.h"

namespace TC::IO {
    auto AllocateRawData(const ChunkDataType& dataType, const size_t& numberOfElements) -> void* {        
        void* rawData;
        if (dataType._value == ChunkDataType::Int8Type) {
            rawData = new int8_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::UInt8Type) {
            rawData = new uint8_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::Int16Type) {
            rawData = new int16_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::UInt16Type) {
            rawData = new uint16_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::Int32Type) {
            rawData = new int32_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::UInt32Type) {
            rawData = new uint32_t[numberOfElements]();
        } else if (dataType._value == ChunkDataType::FloatType) {
            rawData = new float[numberOfElements]();
        } else if (dataType._value == ChunkDataType::DoubleType) {
            rawData = new double[numberOfElements]();
        } else {
            rawData = nullptr;
        }
        return rawData;
    }

    auto GenerateMemoryChunk(void* tileData, const Dimension& dataDimension, const ChunkDataType& dataType)
        -> MemoryChunk::Pointer {
        MemoryChunk::Pointer memoryChunk(new MemoryChunk);

        if (dataType._value == ChunkDataType::Int8Type) {
            const auto data = std::shared_ptr<int8_t[]>(static_cast<int8_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::UInt8Type) {
            const auto data = std::shared_ptr<uint8_t[]>(static_cast<uint8_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::Int16Type) {
            const auto data = std::shared_ptr<int16_t[]>(static_cast<int16_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::UInt16Type) {
            const auto data = std::shared_ptr<uint16_t[]>(static_cast<uint16_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::Int32Type) {
            const auto data = std::shared_ptr<int32_t[]>(static_cast<int32_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::UInt32Type) {
            const auto data = std::shared_ptr<uint32_t[]>(static_cast<uint32_t*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::FloatType) {
            const auto data = std::shared_ptr<float[]>(static_cast<float*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else if (dataType._value == ChunkDataType::DoubleType) {
            const auto data = std::shared_ptr<double[]>(static_cast<double*>(tileData));
            memoryChunk->SetData(data, dataDimension);
        } else {
        }

        return memoryChunk;
    }

    auto CheckPointIsIncludedInDimension(const Point& point, const Dimension& dimension) -> bool {
        const auto pointXIncluded = (point.X() >= 0) && (point.X() < static_cast<int64_t>(dimension.X()));
        const auto pointYIncluded = (point.Y() >= 0) && (point.Y() < static_cast<int64_t>(dimension.Y()));
        auto pointIncluded = pointXIncluded && pointYIncluded;

        const auto dimensionIs3D = (dimension.GetDimensionality() == 3);
        if (dimensionIs3D) {
            const auto pointZIncluded = (point.Z() >= 0) && (point.Z() < static_cast<int64_t>(dimension.Z()));
            pointIncluded = pointIncluded && pointZIncluded;
        }
        return pointIncluded;
    }

    auto CheckReadingRangeIsIncludedInDimension(const DataRange& readingRange, const Dimension& dimension) -> bool {
        const auto dimensionalitySame = (readingRange.GetDimensionality() == dimension.GetDimensionality());
        const auto offsetPointIncluded = CheckPointIsIncludedInDimension(readingRange.GetOffsetPoint(), dimension);
        const auto lastPointIncluded = CheckPointIsIncludedInDimension(readingRange.GetLastPoint(), dimension);

        const auto readingRangeIsIncludedInDimension = 
            (dimensionalitySame && offsetPointIncluded && lastPointIncluded);

        return readingRangeIsIncludedInDimension;
    }
}
