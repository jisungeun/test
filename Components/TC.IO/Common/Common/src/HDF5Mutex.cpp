#include <atomic>
#include <QMutex>
#include "HDF5Mutex.h"

namespace TC::IO {
    struct HDF5Mutex::Impl {
        QMutex* mutex{ nullptr };
    };
    
    HDF5Mutex::HDF5Mutex() : d{ new Impl } {
        d->mutex = new QMutex();
    }
    
    HDF5Mutex::~HDF5Mutex() {
    }
    
    auto HDF5Mutex::GetInstance(void)->Pointer {
        static Pointer theInstance{new HDF5Mutex()};
        return theInstance;
    }
        
    auto HDF5Mutex::Lock()->void {
        d->mutex->lock();
    }
    
    auto HDF5Mutex::Unlock()->void {
        d->mutex->unlock();
    }
    
    struct HDF5MutexLocker::Impl {
        HDF5Mutex::Pointer mutex{ nullptr };
        std::atomic<bool> locked;
    };
    
    HDF5MutexLocker::HDF5MutexLocker(HDF5Mutex::Pointer m) : d{ new Impl } {
        d->mutex = m;
        d->locked = false;
        
        if(d->mutex.get()) {
            d->mutex->Lock();
            d->locked = true;
        }
    }
    
    HDF5MutexLocker::~HDF5MutexLocker() {
        Unlock();
    }
        
    auto HDF5MutexLocker::Unlock()->void {
        if (d->mutex.get() && d->locked) {
            d->mutex->Unlock();
            d->locked = false;
        }
    }
    
    auto HDF5MutexLocker::Relock()->void {
        if (d->mutex.get() && !d->locked) {
            d->mutex->Lock();
            d->locked = true;
        }
    }
}