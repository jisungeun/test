#include "Count.h"

namespace TC::IO {
    struct Count::Impl {
        Impl() = default;
        ~Impl() = default;

        /**
         * \brief Count 클래스 멤버 변수
         * member dimensionality : 차원 정보
         * member x : x축 개수 정보
         * member y : y축 개수 정보
         * member z : z축 개수 정보
         */
        size_t dimensionality{ 0 };
        size_t x{ 0 };
        size_t y{ 0 };
        size_t z{ 0 };
    };

    Count::Count()
        : d(new Impl()) {
    }

    Count::Count(const Count& other)
        : d(new Impl(*other.d)) {
    }

    Count::Count(const size_t& x, const size_t& y)
        : d(new Impl()) {
        d->dimensionality = 2;
        d->x = x;
        d->y = y;
    }

    Count::Count(const size_t& x, const size_t& y, const size_t& z)
        : d(new Impl()) {
        d->dimensionality = 3;
        d->x = x;
        d->y = y;
        d->z = z;
    }

    Count::Count(const Dimension& dimension)
        : d(new Impl()) {
        d->dimensionality = dimension.GetDimensionality();
        d->x = dimension.X();
        d->y = dimension.Y();

        if (d->dimensionality == 3) {
            d->z = dimension.Z();
        } else if (d->dimensionality == 2) {
            d->z = 0;
        }
    }

    Count::~Count() = default;

    auto Count::operator=(const Count& other) -> Count& {
        *d = *(other.d);
        return *this;
    }

    auto Count::operator==(const Count& other) const -> bool {
        const auto dimensionalitySame = d->dimensionality == other.d->dimensionality;
        const auto xSame = d->x == other.d->x;
        const auto ySame = d->y == other.d->y;
        const auto zSame = d->z == other.d->z;
        return dimensionalitySame && xSame && ySame && zSame;
    }

    auto Count::operator!=(const Count& other) const -> bool {
        return !(*this == other);
    }

    auto Count::operator/(const int32_t& divider) const -> Count {
        return Count(d->x / divider, d->y / divider, d->z / divider);
    }

    auto Count::GetDimensionality() const -> size_t {
        return d->dimensionality;
    }

    auto Count::X() const -> size_t {
        return d->x;
    }

    auto Count::Y() const -> size_t {
        return d->y;
    }

    auto Count::Z() const -> size_t {
        return d->z;
    }

    auto Count::GetTotalNumberOfCount() const -> size_t {
        size_t totalNumberOfCount{};
        if(d->dimensionality == 2) {
            totalNumberOfCount = d->x * d->y;
        } else if(d->dimensionality == 3) {
            totalNumberOfCount = d->x * d->y * d->z;
        }
        return totalNumberOfCount;
    }
}
