#include <algorithm>

#include "BoundingBox.h"

namespace TC::IO {
    struct BoundingBox::Impl {
        Offset offset{ 0, 0, 0 };
        Size size{ 0, 0 , 0 };

        bool operator==(const Impl& other) {
            if (offset != other.offset) return false;
            if (size != other.size) return false;
            return true;
        }
    };

    BoundingBox::BoundingBox() : d{ new Impl } {
    }

    BoundingBox::BoundingBox(const BoundingBox& other) : d{ new Impl } {
        *d = *(other.d);
    }

    BoundingBox::~BoundingBox() {
    }

    auto BoundingBox::operator=(const BoundingBox& other) -> BoundingBox& {
        *d = *(other.d);
        return *this;
    }

    auto BoundingBox::operator==(const BoundingBox& other) const-> bool {
        return (*d == *other.d);
    }

    auto BoundingBox::operator!=(const BoundingBox& other) const-> bool {
        return !(*this == other);
    }

    auto BoundingBox::SetOffset(int x0, int y0, int z0) -> void {
        d->offset.x0 = x0;
        d->offset.y0 = y0;
        d->offset.z0 = z0;
    }

    auto BoundingBox::SetSize(int d0, int d1, int d2) -> void {
        d->size.d0 = d0;
        d->size.d1 = d1;
        d->size.d2 = d2;
    }

    auto BoundingBox::GetOffset() const -> Offset {
        return d->offset;
    }

    auto BoundingBox::GetSize() const -> Size {
        return d->size;
    }

    auto BoundingBox::GetCenter() const -> Offset {
        Offset center;
        center.x0 = (d->offset.x0 + d->size.d0) / 2;
        center.y0 = (d->offset.y0 + d->size.d1) / 2;
        center.z0 = (d->offset.z0 + d->size.d2) / 2;
        return center;
    }

    auto BoundingBox::Contain(int x0, int y0, int z0) const -> bool {
        auto checkInRange = [](int offset, int size, int pos)->bool {
            const auto minVal = std::min<int>(offset, offset + size - 1);
            const auto maxVal = std::max<int>(offset, offset + size - 1);
            return (pos >= minVal) && (pos <= maxVal);
        };

        if (!checkInRange(d->offset.x0, d->size.d0, x0)) return false;
        if (!checkInRange(d->offset.y0, d->size.d1, y0)) return false;
        if (!checkInRange(d->offset.z0, d->size.d2, z0)) return false;

        return true;
    }

    auto BoundingBox::GetVolume() const -> uint64_t {
        return (static_cast<uint64_t>(d->size.d0) * d->size.d1 * d->size.d2);
    }
}

std::ostream& operator << (std::ostream& out, const TC::IO::BoundingBox& box) {
    const auto offset = box.GetOffset();
    const auto size = box.GetSize();
    out << "[Offset=(" << offset.x0 << "," << offset.y0 << "," << offset.z0 << ") Size=(" << size.d0 << "," << size.d1 << "," << size.d2 << ")]";
    return out;
}