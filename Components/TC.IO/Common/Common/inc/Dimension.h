#pragma once
#pragma warning (disable : 4251)

#include <memory>

#include "TCIOCommonExport.h"

namespace TC::IO {
    class Count;
    class TCIOCommon_API Dimension {
    public:
        Dimension();
        Dimension(const Dimension& other);
        explicit Dimension(const Count& count);
        Dimension(const size_t& x, const size_t& y);
        Dimension(const size_t& x, const size_t& y, const size_t& z);
        ~Dimension();
        auto operator=(const Dimension& other)->Dimension&;
        auto operator==(const Dimension& other) const ->bool;
        auto operator!=(const Dimension& other) const ->bool;

        auto GetDimensionality() const->size_t;
        auto X() const->size_t;
        auto Y() const->size_t;
        auto Z() const->size_t;
        auto GetNumberOfElements() const->size_t;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}