#pragma once
#include <memory>
#include <any>

#include <enum.h>

#include "TCIOCommonExport.h"

#include "Dimension.h"

namespace TC::IO {
    BETTER_ENUM(ChunkDataType, uint8_t, 
        Int8Type, UInt8Type, Int16Type, UInt16Type, Int32Type, UInt32Type, FloatType, DoubleType)
    class TCIOCommon_API MemoryChunk {
    public:
        using Self = MemoryChunk;
        using Pointer = std::shared_ptr<Self>;

        MemoryChunk();
        ~MemoryChunk();

        auto SetData(const std::shared_ptr<int8_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<uint8_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<int16_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<uint16_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<int32_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<uint32_t[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<float[]>& data, const Dimension& dimension)->void;
        auto SetData(const std::shared_ptr<double[]>& data, const Dimension& dimension)->void;

        auto GetDataType() const ->ChunkDataType;
        auto GetData() const ->std::any;
        auto GetDataPointer() const -> void*;

        template <class T>
        auto GetDataPointerAs() const->T* {
            return static_cast<T*>(GetDataPointer());
        }

        auto GetDimension() const ->Dimension;
        auto GetSizeInBytes() const->size_t;

        auto ZeroPadding(const Dimension& targetDimension)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        template <class T>
        static auto CopyMemory(T* srcData, T* destData, const Dimension& srcDimension, const Dimension& destDimension)->void;
    };
}
