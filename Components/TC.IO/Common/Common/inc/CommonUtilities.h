#pragma once
#include "TCIOCommonExport.h"
#include "MemoryChunk.h"
#include "DataRange.h"

namespace TC::IO {
    auto TCIOCommon_API AllocateRawData(const ChunkDataType& dataType, const size_t& numberOfElements)->void*;
    auto TCIOCommon_API GenerateMemoryChunk(void* tileData, const Dimension& dataDimension, const ChunkDataType& dataType)
        ->MemoryChunk::Pointer;
    auto TCIOCommon_API CheckPointIsIncludedInDimension(const Point& point, const Dimension& dimension)->bool;
    auto TCIOCommon_API CheckReadingRangeIsIncludedInDimension(const DataRange& readingRange, const Dimension& dimension)
        ->bool;
}