#pragma once

#include <memory>

#include <QString>

#include "TCIOCommonExport.h"

namespace TC::IO {
    /**
     * \brief 2D/3D Volume data 정보를 저장하는 클래스
     */
    class TCIOCommon_API VolumeMetaInfo {
    public:
        using Self = VolumeMetaInfo;
        using Pointer = std::shared_ptr<Self>;

        enum class FType {
            TCFType,
            LDMTCFType,
            RAWType
        };

    public:
        explicit VolumeMetaInfo();
        VolumeMetaInfo(const VolumeMetaInfo& other);
        virtual ~VolumeMetaInfo();

        VolumeMetaInfo& operator=(const VolumeMetaInfo& other);

        /**
         * \brief Valid 여부 설정
         * \param bValid true/false
         */
        auto SetValid(bool bValid)->void;
        /**
         * \brief Valid 여부 확인
         * \return true/false
         */
        auto IsValid(void) const->bool;

        /**
        * \brief Volume 크기 설정
        * \param xDim X 방향 voxel 개수
        * \param yDim Y 방향 voxel 개수
        * \param zDim Z 방향 voxel 개수
        */
        auto SetDimension(uint32_t xDim, uint32_t yDim, uint32_t zDim = 1)->void;
        /**
         * \brief Volume 크기 얻기
         * \return tuple 타입으로 Volume 크기 반환
         */
        auto GetDimension(void) const->std::tuple<uint32_t, uint32_t, uint32_t>;
        /**
         * \brief 전체 voxel 개수 반환
         * \return Voxel 개수
         */
        auto GetElements(void) const->uint32_t;
        /**
         * \brief XY Slice를 구성하는 voxel 개수 반환
         * \return Voxel 개수
         */
        auto GetXYElements(void) const->uint32_t;
        /**
         * \brief Voxel 해상도 설정
         * \param xResolution Voxel의 X 방향 크기
         * \param yResolution Voxel의 Y 방향 크기
         * \param zResolution Voxel의 Z 방향 크기
         */
        auto SetResolution(double xResolution, double yResolution, double zResolution = 0.0)->void;
        /**
         * \brief Voxel 해상도 얻기
         * \return tuple 타입으로 voxel 해상도 반환
         */
        auto GetResolution(void)->std::tuple<double, double, double>;
        /**
         * \brief Image file 위치 설정
         * \param Image file의 절대 위치
         */
        auto SetFilePath(QString path)->void;
        /**
         * \brief Image file 위치 얻기
         * \return Image file의 절대 위치
         */
        auto GetFilePath(void)->QString;
        /**
         * \brief Image file의 Type 설정
         * \param FType으로 file Type 설정
         */
        auto SetFileType(FType type)->void;
        /**
         * \brief Image file의 Type 얻기
         * \return FType
         */
        auto GetFileType(void)->FType;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}