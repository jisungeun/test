#pragma once
#include "Count.h"
#include "TCIOCommonExport.h"

namespace TC::IO {
    class TCIOCommon_API Point {
    public:
        Point();
        Point(const Point& other);
        Point(const int64_t& x, const int64_t& y);
        Point(const int64_t& x, const int64_t& y, const int64_t& z);
        ~Point();
        auto operator=(const Point& other)->Point&;
        auto operator==(const Point& other) const ->bool;
        auto operator!=(const Point& other) const ->bool;
        auto operator-(const Point& other) const->Point;
        auto operator+(const Point& other) const->Point;
        auto operator+(const Count& count) const->Point;
        auto operator/(const int64_t& divider) const->Point;

        auto SetPoint(const int64_t& x, const int64_t& y)->void;
        auto SetPoint(const int64_t& x, const int64_t& y, const int64_t& z)->void;
        auto GetDimensionality() const->uint64_t;
        auto X() const->int64_t;
        auto Y() const->int64_t;
        auto Z() const->int64_t;

        static auto Min(const Point& point1, const Point& point2)->Point;
        static auto Max(const Point& point1, const Point& point2)->Point;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
