#pragma once
#include <memory>

#include "TCIOCommonExport.h"

#include "Dimension.h"

namespace TC::IO {
    /**
     * \brief 2D / 3D 개수를 나타내는 클래스
     */
    class TCIOCommon_API Count {
    public:
        /**
         * \brief 비어있는 기본 생성자,
         */
        Count();

        /**
         * \brief 복사 생성자
         * \param other 복사하고 싶은 정보가 담긴 Count 인스턴스
         */
        Count(const Count& other);

        /**
         * \brief 2차원 데이터를 이용한 생성자
         * \param x x축 개수
         * \param y y축 개수
         */
        Count(const size_t& x, const size_t& y);

        /**
         * \brief 3차원 데이터를 이용한 생성자
         * \param x x축 개수
         * \param y y축 개수
         * \param z z축 개수
         */
        Count(const size_t& x, const size_t& y, const size_t& z);

        /**
         * \brief Dimension 인스턴스를 이용한 변환 생성자
         * \param dimension 변환 정보를 담고있는 Dimension 인스턴스 (Count 개수 = Dimension 크기)
         */
        explicit Count(const Dimension& dimension);
        ~Count();

        /**
         * \brief 할당 연산자
         * \param other 할당하기 원하는 정보를 갖는 Count 인스턴스
         * \return Count 자기 자신의 레퍼런스
         */
        auto operator=(const Count& other)->Count&;

        auto operator==(const Count& other) const ->bool;
        auto operator!=(const Count& other) const ->bool;

        auto operator/(const int32_t& divider) const ->Count;

        /**
         * \brief Count 인스턴스가 갖는 차원 정보를 조회
         * \return 차원 수 (2 or 3)
         */
        auto GetDimensionality() const->size_t;

        /**
         * \brief Count 인스턴스가 갖는 X축 개수 조회
         * \return X축 개수
         */
        auto X() const->size_t;

        /**
         * \brief Count 인스턴스가 갖는 Y축 개수 조회
         * \return Y축 개수
         */
        auto Y() const->size_t;

        /**
         * \brief Count 인스턴스가 갖는 Z축 개수 조회
         * \return Z축 개수
         */
        auto Z() const->size_t;

        auto GetTotalNumberOfCount() const ->size_t;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
