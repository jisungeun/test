#pragma once

#include <memory>
#include <any>

#include "TCIOCommonExport.h"

namespace TC {
    namespace IO {
        /**
         * \brief 2D/3D Volume data를 저장하는 클래스
         */
        class TCIOCommon_API VolumeData {
        public:
            typedef VolumeData Self;
            typedef std::shared_ptr<Self> Pointer;

            enum class Type {
                Int8Type,
                UInt8Type,
                Int16Type,
                UInt16Type,
                Int32Type,
                UInt32Type,
                FloatType,
                DoubleType
            };

        public:
            explicit VolumeData();
            VolumeData(const VolumeData& other);
            virtual ~VolumeData();

            VolumeData& operator=(const VolumeData& other);

            /**
             * \brief Valid 여부 설정
             * \param bValid true/false
             */
            auto SetValid(bool bValid)->void;
            /**
             * \brief Valid 여부 확인
             * \return true/false
             */
            auto IsValid(void) const->bool;

            /**
             * \brief Volume 크기 설정
             * \param xDim X 방향 voxel 개수
             * \param yDim Y 방향 voxel 개수
             * \param zDim Z 방향 voxel 개수
             */
            auto SetDimension(size_t xDim, size_t yDim, size_t zDim = 1)->void;
            /**
             * \brief Volume 크기 얻기
             * \return tuple 타입으로 Volume 크기 반환
             */
            auto GetDimension(void) const->std::tuple<size_t, size_t, size_t>;
            /**
             * \brief 전체 voxel 개수 반환
             * \return voxel 개수
             */
            auto GetElements(void) const->size_t;
            /**
             * \brief XY Slice를 구성하는 voxel 개수 반환
             * \return voxel 개수
             */
            auto GetXYElements(void) const->size_t;
            /**
             * \brief Voxel 해상도 설정
             * \param xResolution Voxel의 X 방향 크기
             * \param yResolution Voxel의 Y 방향 크기
             * \param zResolution Voxel의 Z 방향 크기
             */
            auto SetResolution(double xResolution, double yResolution, double zResolution = 1.0)->void;
            /**
             * \brief Voxle 해상도 얻기
             * \return tuple 타입으로 voxel 해상도 반환
             */
            auto GetResolution(void)->std::tuple<double, double, double>;
            /**
             * \brief 값의 범위 설정
             * \param minValue 최소 값
             * \param maxValue 최대 값
             */
            auto SetRange(double minValue, double maxValue)->void;
            /**
             * \brief 값의 범위 얻기
             * \return tuple<double,double> 형태로 최소/최대 값을 반환
             */
            auto GetRange() const->std::tuple<double, double>;

            /**
             * \brief Raw pointer와 voxel 개수 정보를 전달하여 voxel 데이타를 저장하도록 함
             * \param data raw pointer to data
             * \param elements voxel 개수
             */
            auto SetRawData(int8_t* data, size_t elements)->void;
            auto SetRawData(uint8_t* data, size_t elements)->void;
            auto SetRawData(int16_t* data, size_t elements)->void;
            auto SetRawData(uint16_t* data, size_t elements)->void;
            auto SetRawData(int32_t* data, size_t elements)->void;
            auto SetRawData(uint32_t* data, size_t elements)->void;
            auto SetRawData(float* data, size_t elements)->void;
            auto SetRawData(double* data, size_t elements)->void;

            /**
             * \brief Voxel 타입 반환
             * \return Type
             */
            auto GetDataType(void) const->Type;
            /**
             * \brief raw data를 std::any 타입으로 반환
             * \return std::any 객체
             */
            auto GetRawData(void)->std::any;

            /**
             * \brief raw data를 smart pointer 로 반환
             * \return std::shared_ptr<type[]> 형태로 반환
             */
            auto GetDataAsInt8(void)->std::shared_ptr<int8_t[]>;
            auto GetDataAsUInt8(void)->std::shared_ptr<uint8_t[]>;
            auto GetDataAsInt16(void)->std::shared_ptr<int16_t[]>;
            auto GetDataAsUInt16(void)->std::shared_ptr<uint16_t[]>;
            auto GetDataAsInt32(void)->std::shared_ptr<int32_t[]>;
            auto GetDataAsUInt32(void)->std::shared_ptr<uint32_t[]>;
            auto GetDataAsFloat(void)->std::shared_ptr<float[]>;
            auto GetDataAsDouble(void)->std::shared_ptr<double[]>;

            /**
             * \brief raw data를 smart pointer 로 반환
             * \return std::shared_ptr<type[]> 형태로 반환
             */
            auto AllocateAsInt8(size_t elements)->std::shared_ptr<int8_t[]>;
            auto AllocateAsUInt8(size_t elements)->std::shared_ptr<uint8_t[]>;
            auto AllocateAsInt16(size_t elements)->std::shared_ptr<int16_t[]>;
            auto AllocateAsUInt16(size_t elements)->std::shared_ptr<uint16_t[]>;
            auto AllocateAsInt32(size_t elements)->std::shared_ptr<int32_t[]>;
            auto AllocateAsUInt32(size_t elements)->std::shared_ptr<uint32_t[]>;
            auto AllocateAsFloat(size_t elements)->std::shared_ptr<float[]>;
            auto AllocateAsDouble(size_t elements)->std::shared_ptr<double[]>;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };
    }
}