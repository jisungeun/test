#pragma once

#include <memory>
#include <iostream>

#include "TCIOCommonExport.h"

namespace TC::IO {
    class TCIOCommon_API HDF5Mutex {
    public:
        typedef HDF5Mutex Self;
        typedef std::shared_ptr<Self> Pointer;
        
    public:
        virtual ~HDF5Mutex();
        static auto GetInstance(void)->Pointer;
        
        auto Lock()->void;
        auto Unlock()->void;
        
    private:
        HDF5Mutex();
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
    
    class TCIOCommon_API HDF5MutexLocker {
    public:
        explicit HDF5MutexLocker(HDF5Mutex::Pointer m);
        virtual ~HDF5MutexLocker();
        
        auto Unlock()->void;
        auto Relock()->void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}