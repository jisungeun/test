#pragma once

#include <memory>

#include <QString>

#include "TCIOCommonExport.h"

namespace TC {
    namespace IO {
        /**
         * \brief 2D/3D Volume data 정보에 접근하는 클래스
        */
        class TCIOCommon_API IVolumeAccessor {
        public:
            //using Self = IVolumeAccessor;
            //using Pointer = std::shared_ptr<Self>;

        public:
            explicit  IVolumeAccessor();
            IVolumeAccessor(const IVolumeAccessor& other);
            virtual ~IVolumeAccessor();

            IVolumeAccessor& operator = (const IVolumeAccessor& other);

            /**
             * \brief 원본 Volume 크기 설정
             * \param xDim X 방향 voxel 개수
             * \param yDim Y 방향 voxel 개수
             * \param zDim Z 방향 voxel 개수
             */
            auto SetDimensions(uint32_t xDim, uint32_t yDim, uint32_t zDim = 1)->void;
            /**
             * \brief 원본 Volume 크기 얻기
             * \return tuple 타입으로 Volume 크기 반환
             */
            auto GetDimensions(void) const->std::tuple<uint32_t, uint32_t, uint32_t>;
            /**
             * \brief 특정 위치의 voxel 값 float으로 얻기
             * \param xCoord X Coordinate
             * \param yCoord Y Coordiante
             * \param zCoord Z Coordinate
             * \return 해당 Coordinate의 Voxel Scalar Value
             */
            virtual auto GetVoxelAsFloat(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1)->float = 0;
            /**
             * \brief 특정 위치의 voxel 값 unsigned short로 얻기
             * \param xCoord X Coordinate
             * \param yCoord Y Coordiante
             * \param zCoord Z Coordinate
             * \return 해당 Coordinate의 Voxel Scalar Value
             */
            virtual auto GetVoxelAsUShort(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1)->uint32_t = 0;

            /**
             * \brief 특정 위치의 float voxel 값 바꾸기
             * \param val scalar value
             * \param xCoord X Coordinate
             * \param yCoord Y Coordiante
             * \param zCoord Z Coordinate
             */
            virtual auto SetVoxelWithFloat(float val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1)->void = 0;
            /**
             * \brief 특정 위치의 unsigned short voxel 값 바꾸기
             * \param val scalar value
             * \param xCoord X Coordinate
             * \param yCoord Y Coordiante
             * \param zCoord Z Coordinate
             */
            virtual auto SetVoxelWithUShort(uint32_t val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1)->void = 0;

            /**
             * \brief 이미지의 특정 영역만을 받아오거나 관리하기 위한 영역 정의
             * \param xMin minimum x-axis border of desired region
             * \param yMin minimum y-axis border of desired region
             * \param zMin minimum z-axis border of desired region
             * \param xMax maximum x-axis border of desired region
             * \param yMax maximum y-axis border of desired region
             * \param zMax maximum z-axis border of desired region
             */
            auto SetRegion(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin = 1, uint32_t zMax = 1)->void;

            /**
             * \brief 현재 정의된 ROI 정보 얻기
             * \return tuple 타입으로 ROI boundary 정보 반환
             */
            auto GetRegion(void)->const std::tuple<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t>;

            /**
             * \brief 현재 정의된 ROI 의 start 좌표 얻기
             * \return tuple 타입으로 ROI start coordinate 반환
             */
            auto GetStart(void)->const std::tuple<uint32_t, uint32_t, uint32_t>;

            /**
             * \brief 현재 정의된 ROI 의 size 정보 얻기
             * \return tuple 타입으로 ROI size 정보 반환
             */
            auto GetSize(void)->const std::tuple < uint32_t, uint32_t, uint32_t >;


            /**
             * \brief 현재 정의된 ROI에 해당하는 Buffer 얻기
             * \return Float 타입의 buffer 반환
             */
            virtual auto GetBufferAsFloat(void)->std::shared_ptr<float> = 0;
            /**
             * \brief 현재 정의된 ROI에 해당하는 Buffer 얻기
             * \return Unsigned short 타입의 buffer 반환
             */
            virtual auto GetBufferAsUShort(void)->std::shared_ptr<unsigned short> = 0;

            /**
            * \brief float 배열로 해당 영역에 해당하는 Buffer 값 바꾸기
            * \param buf Scalar 값 정보들을 지니고 있는 buffer pointer
            */
            virtual auto SetBufferWithFloat(float* buf)->void = 0;
            /**
            * \brief unsigned short 배열로 해당 영역에 해당하는 Buffer 값 바꾸기
            * \param buf Scalar 값 정보들을 지니고 있는 buffer pointer
            */
            virtual auto SetBufferWithUShort(unsigned short* buf)->void = 0;

            /**
            * \brief 접근하고자 하는 Data set의 이름 설정 (for direct TCF Access)
            * \param HDF5 데이터의 dataset name
            */
            auto SetDataSetName(QString dset)->void;
            /**
            * \brief 현재 설정된 dataset 이름 얻기
            * \return HDF5 데이터의 dataset name
            */
            auto GetDataSetName(void)->QString;

            /**
             * \brief 주어진 volume 좌표의 validity check
             * \param xCoord X Coordinate
             * \param yCoord Y Coordiante
             * \param zCoord Z Coordinate
             * \return bool
             */

            auto GetDimension(void)->uint32_t;

            auto isValidCoord(uint32_t x, uint32_t y, uint32_t z)->bool;

            /**
             * \brief 설정된 volume subregion의 validity check
             * \return bool
             */
            auto isValidRegion(void)->bool;

        private:

            struct Impl;
            std::unique_ptr<Impl> d;
        };
    }
}