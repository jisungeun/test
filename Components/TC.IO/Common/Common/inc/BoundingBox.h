#pragma once

#include <memory>
#include <iostream>

#include "TCIOCommonExport.h"

namespace TC::IO {
    /**
     * \brief 3D BoundingBox
     */
    class TCIOCommon_API BoundingBox {
    public:
        class Offset {
        public:
            int x0, y0, z0;

            bool operator==(const Offset& other) const {
                if (x0 != other.x0) return false;
                if (y0 != other.y0) return false;
                if (z0 != other.z0) return false;
                return true;
            }
            bool operator!=(const Offset& other) const {
                return !(*this == other);
            }
        };

        class Size {
        public:
            uint32_t d0, d1, d2;

            bool operator==(const Size& other) const {
                if (d0 != other.d0) return false;
                if (d1 != other.d1) return false;
                if (d2 != other.d2) return false;
                return true;
            }

            bool operator!=(const Size & other) const {
                return !(*this == other);
            }
        };

    public:
        BoundingBox();
        BoundingBox(const BoundingBox& other);
        virtual ~BoundingBox();

        auto operator=(const BoundingBox& other)->BoundingBox&;
        auto operator==(const BoundingBox& other) const->bool;
        auto operator!=(const BoundingBox& other) const->bool;

        /**
         * \brief 기준 위치 설정
         * \param x0 X축 기준 위치
         * \param y0 Y축 기준 위치
         * \param z0 Z축 기준 위치
         */
        auto SetOffset(int x0, int y0, int z0)->void;
        /**
         * \brief 축 별 길이 설정
         * \param d0 X축 길이
         * \param d1 Y축 길이
         * \param d2 Z축 길이
         */
        auto SetSize(int d0, int d1, int d2)->void;

        /**
         * \brief 기준 위치 얻기
         * \return 기준 위치
         */
        auto GetOffset() const->Offset;
        /**
         * \brief 축 별 길이 얻기
         * \return 축 별 길이
         */
        auto GetSize() const->Size;
        /**
         * \brief 축 별 중앙 위치 얻기
         * \return 축 별 중앙 위치
         */
        auto GetCenter() const->Offset;

        /**
         * \brief 특정 점이 Box 내부에 존재하는지 확인
         * \param x0 비교할 위치
         * \param y0 비교할 위치
         * \param z0 비교할 위치
         * \return true/false
         */
        auto Contain(int x0, int y0, int z0) const->bool;

        /**
         * \brief Bounding box의 volume을 계산함
         * \return volume
         */
        auto GetVolume() const->uint64_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

TCIOCommon_API std::ostream& operator << (std::ostream& out, const TC::IO::BoundingBox& box);