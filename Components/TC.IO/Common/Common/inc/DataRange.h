#pragma once
#include <memory>
#include "Count.h"
#include "Point.h"
#include "TCIOCommonExport.h"

namespace TC::IO {
    class TCIOCommon_API DataRange {
    public:
        DataRange();
        DataRange(const DataRange& other);
        DataRange(const Point& offsetPoint, const Point& lastPoint);
        DataRange(const Point& offsetPoint, const Count& count);
        ~DataRange();
        auto operator=(const DataRange& other)->DataRange&;
        auto operator==(const DataRange& other) const ->bool;
        auto operator!=(const DataRange& other) const ->bool;

        auto GetDimensionality()const->size_t;
        auto GetOffsetPoint() const->Point;
        auto GetLastPoint() const->Point;
        auto GetCount() const->Count;
        auto IsEmpty() const->bool;

        auto GetOverlapRange(const DataRange& dataRange) const ->DataRange;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GetOverlapOffsetPoint(const size_t& offsetPoint1, const size_t& offsetPoint2)->size_t;
        static auto GetOverlapLastPoint(const size_t& lastPoint1, const size_t& lastPoint2)->size_t;
    };
}