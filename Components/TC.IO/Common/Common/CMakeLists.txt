project(TCIOCommon)

FIND_PACKAGE(HDF5 COMPONENTS CXX REQUIRED)

#Header files for external use
set(INTERFACE_HEADERS
	inc/BoundingBox.h
	inc/CommonUtilities.h
	inc/Count.h
	inc/DataRange.h
	inc/Dimension.h
	inc/HDF5Mutex.h
	inc/MemoryChunk.h
	inc/Point.h
	inc/VolumeAccessor.h
	inc/VolumeData.h
	inc/VolumeMetaInfo.h	
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/BoundingBox.cpp
	src/CommonUtilities.cpp
	src/Count.cpp
	src/DataRange.cpp
	src/Dimension.cpp
	src/HDF5Mutex.cpp
	src/MemoryChunk.cpp
	src/Point.cpp
	src/VolumeAccessor.cpp
	src/VolumeData.cpp
	src/VolumeMetaInfo.cpp	
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TC::Components::IOCommon ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/Dependency/hdf5/include>		
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
    PUBLIC
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5_D.lib
		Extern::BetterEnums
        Qt5::Core		
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PRIVATE
		$<$<COMPILE_LANGUAGE:C>:/wd4251>
		$<$<COMPILE_LANGUAGE:CXX>:/wd4251>
        /W4 /WX
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/IO/Common")
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT io)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)
add_subdirectory(examples)
