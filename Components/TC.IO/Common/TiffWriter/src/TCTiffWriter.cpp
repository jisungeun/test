#include <optional>

#include <tiffio.h>

#include "TCTiffWriter.h"

namespace TC::IO {
	struct TCTiffWriter::Impl {
		std::optional<TiffMetadata3D> meta3d = std::nullopt;
		std::optional<TiffMetadata2D> meta2d = std::nullopt;
		std::optional<TiffMetadataOption> option = std::nullopt;
		std::optional<Intensity> intensity = std::nullopt;

		TIFF* tiff = nullptr;
		int index = 0;
	};

	TCTiffWriter::TCTiffWriter(const TiffMetadata3D& metadata) : d(new Impl) {
		d->meta3d = metadata;
		d->tiff = TIFFOpen(d->meta3d->filepath.c_str(), (d->meta3d->x * d->meta3d->y * d->meta3d->z > 3758100000ULL) ? "w8" : "w");
	}

	TCTiffWriter::TCTiffWriter(const TiffMetadata2D& metadata) : d(new Impl) {
		d->meta2d = metadata;
		d->tiff = TIFFOpen(d->meta2d->filepath.c_str(), (d->meta2d->x * d->meta2d->y > 3758100000ULL) ? "w8" : "w");
	}

	TCTiffWriter::~TCTiffWriter() {
		if (d->tiff)
			TIFFClose(d->tiff);
	}

	auto TCTiffWriter::SetOption(const TiffMetadataOption& option) -> void {
		d->option = option;
	}

	auto TCTiffWriter::SetIntensity(const Intensity& intensity) -> void {
		d->intensity = intensity;
	}

	auto TCTiffWriter::WriteLine(void* buffer) -> bool {
		if (!d->tiff)
			return false;

		const auto x = d->meta2d ? d->meta2d->x : d->meta3d->x;
		const auto y = d->meta2d ? d->meta2d->y : d->meta3d->y;
		const auto z = d->meta2d ? 1 : d->meta3d->z;
		const auto byte = d->meta2d ? d->meta2d->byte : d->meta3d->byte;
		const auto rgb = d->meta2d ? d->meta2d->isRGB : d->meta3d->isRGB;

		if (d->index % y == 0) {
			if (d->meta3d) {
				TIFFSetField(d->tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
				TIFFSetField(d->tiff, TIFFTAG_PAGENUMBER, z);
				TIFFSetField(d->tiff, TIFFTAG_IMAGEDEPTH, z);
			}

			TIFFSetField(d->tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
			TIFFSetField(d->tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
			TIFFSetField(d->tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
			TIFFSetField(d->tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
			TIFFSetField(d->tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
			TIFFSetField(d->tiff, TIFFTAG_IMAGEWIDTH, x);
			TIFFSetField(d->tiff, TIFFTAG_IMAGELENGTH, y);
			TIFFSetField(d->tiff, TIFFTAG_BITSPERSAMPLE, byte * 8);
			TIFFSetField(d->tiff, TIFFTAG_PHOTOMETRIC, rgb ? PHOTOMETRIC_RGB : PHOTOMETRIC_MINISBLACK);
			TIFFSetField(d->tiff, TIFFTAG_SAMPLESPERPIXEL, rgb ? 3 : 1);
			TIFFSetField(d->tiff, TIFFTAG_XPOSITION, std::max(d->option->positionX, 0.0));
			TIFFSetField(d->tiff, TIFFTAG_YPOSITION, std::max(d->option->positionY, 0.0));
			TIFFSetField(d->tiff, TIFFTAG_IMAGEDESCRIPTION, d->option->desc.c_str());
			TIFFSetField(d->tiff, TIFFTAG_ARTIST, d->option->artist.c_str());
			TIFFSetField(d->tiff, TIFFTAG_HOSTCOMPUTER, d->option->host.c_str());
			TIFFSetField(d->tiff, TIFFTAG_MODEL, d->option->model.c_str());
			TIFFSetField(d->tiff, TIFFTAG_SOFTWARE, d->option->software.c_str());
			TIFFSetField(d->tiff, TIFFTAG_DATETIME, d->option->datetime.c_str());
			TIFFSetField(d->tiff, TIFFTAG_XRESOLUTION, d->option->resolutionX);
			TIFFSetField(d->tiff, TIFFTAG_YRESOLUTION, d->option->resolutionY);

			if (d->intensity) {
				TIFFSetField(d->tiff, TIFFTAG_MINSAMPLEVALUE, d->intensity->min);
				TIFFSetField(d->tiff, TIFFTAG_MAXSAMPLEVALUE, d->intensity->max);
			}

			TIFFSetField(d->tiff, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(d->tiff, 0));
		}

		if (TIFFWriteScanline(d->tiff, buffer, d->index % y) < 0)
			return false;

		if (++d->index % y == 0 && d->meta3d)
			TIFFWriteDirectory(d->tiff);

		return true;
	}
}
