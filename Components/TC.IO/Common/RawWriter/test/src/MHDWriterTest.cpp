#include <catch2/catch.hpp>

#include <QFile>

#include "MHDWriter.h"

namespace MHDWriterTest {
    using namespace TC::IO::RawWriter;

    TEST_CASE("MHDWriter : unit test") {}
    TEST_CASE("MHDWriter : practical test") {
        const QString mhdFilePath = "mhdWriterFilePath";

        MHDWriter writer;
        writer.SetPath(mhdFilePath);
        writer.SetSize({ 1,2,3 });
        writer.SetSpacing({ 5,6,7 });
        writer.SetDataType(Writer::Type::DOUBLE);

        if (QFile::exists(mhdFilePath) == true) {
            REQUIRE(QFile::remove(mhdFilePath) == true);
        }

        REQUIRE(QFile::exists(mhdFilePath) == false);

        CHECK(writer.Write() == true);
        CHECK(QFile::exists(mhdFilePath) == true);
    }
}