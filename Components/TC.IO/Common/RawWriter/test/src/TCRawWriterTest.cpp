#include <catch2/catch.hpp>

#include <QFile>

#include "TCRawWriter.h"

namespace TCRawWriterTest {
    using namespace TC::IO::RawWriter;

    TEST_CASE("TCRawWriter : unit test") {}
    TEST_CASE("TCRawWriter : practical test") {
        SECTION("bug case [filename.TCF.raw.raw]") {
            const QString rawFilePath = "filename.TCF.raw.raw";

            const QString resultFilePath1 = "filename.TCF.raw.raw";
            const QString resultFilePath2 = "filename.TCF.raw.mhd";

            if (QFile::exists(resultFilePath1) == true) {
                REQUIRE(QFile::remove(resultFilePath1) == true);
            }

            if (QFile::exists(resultFilePath2) == true) {
                REQUIRE(QFile::remove(resultFilePath2) == true);
            }

            REQUIRE(QFile::exists(resultFilePath1) == false);
            REQUIRE(QFile::exists(resultFilePath2) == false);

            constexpr auto dataLength = 1;
            std::shared_ptr<uint8_t[]> rawData{ new uint8_t[dataLength]() };

            {
                Writer writer;
                writer.SetPath(rawFilePath);
                writer.SetSize({ 1,2,3 });
                writer.SetSpacing({ 4,5,6 });
                writer.SetDataType(Writer::Type::DOUBLE);

                CHECK(writer.Append(reinterpret_cast<const char*>(rawData.get()), dataLength) == true);
            }

            CHECK(QFile::exists(resultFilePath1) == true);
            CHECK(QFile::exists(resultFilePath2) == true);
        }
    }
}