#pragma once
#include <memory>
#include <vector>
#include <QString>

#include "TCRawWriterExport.h"

namespace TC::IO::RawWriter {
    /**
     * \brief Raw 파일과 메타 파일인 MHD 파일을 작성하는 클랙스
     */
    class TCRawWriter_API Writer {
    public:
        enum class Type {
            NOTDEFINED,
            INT8,
            UINT8,
            INT16,
            UINT16,
            INT32,
            UINT32,
            INT64,
            UINT64,
            FLOAT,
            DOUBLE
        };

    public:
        Writer();
        Writer(const QString& path, const std::vector<size_t>& size, const std::vector<float>& spacing, Type type);

        ~Writer();

        auto SetPath(const QString& path)->void;
        auto SetSize(const std::vector<size_t>& size)->void;
        auto SetSpacing(const std::vector<float>& spacing)->void;
        auto SetDataType(Type type)->void;

        auto Append(const char* data, int64_t length, bool append = false)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}