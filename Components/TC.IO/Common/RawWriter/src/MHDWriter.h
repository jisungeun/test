#pragma once
#include <memory>
#include "TCRawWriter.h"

namespace TC::IO::RawWriter {
    class MHDWriter {
    public:
        MHDWriter();
        ~MHDWriter();

        auto SetPath(const QString& path)->void;
        auto SetSize(const std::vector<size_t>& size)->void;
        auto SetSpacing(const std::vector<float>& spacing)->void;
        auto SetDataType(Writer::Type type)->void;

        auto Write()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}