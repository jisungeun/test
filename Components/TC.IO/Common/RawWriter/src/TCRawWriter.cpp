#include <QDir>
#include <QFile>
#include <QFileInfo>

#include "MHDWriter.h"
#include "TCRawWriter.h"

namespace TC::IO::RawWriter {
    struct Writer::Impl {
        QString path;
        MHDWriter mhdWriter;

        auto mhdPath(const QString& path)->const QString;
    };

    auto Writer::Impl::mhdPath(const QString& path) -> const QString {
        const QFileInfo info(path);
        return QDir(info.absolutePath()).filePath(info.completeBaseName()) + ".mhd";
    }

    Writer::Writer() : d{new Impl} {
    }

    Writer::Writer(const QString& path, const std::vector<size_t>& size, const std::vector<float>& spacing, Type type) : d{new Impl} {
        d->path = path;

        d->mhdWriter.SetPath(d->mhdPath(path));
        d->mhdWriter.SetSize(size);
        d->mhdWriter.SetSpacing(spacing);
        d->mhdWriter.SetDataType(type);
    }

    Writer::~Writer() {
        d->mhdWriter.Write();
    }

    auto Writer::SetPath(const QString& path) -> void {
        d->path = path;
        d->mhdWriter.SetPath(d->mhdPath(path));
    }

    auto Writer::SetSize(const std::vector<size_t>& size) -> void {
        d->mhdWriter.SetSize(size);
    }

    auto Writer::SetSpacing(const std::vector<float>& spacing) -> void {
        d->mhdWriter.SetSpacing(spacing);
    }

    auto Writer::SetDataType(Type type) -> void {
        d->mhdWriter.SetDataType(type);
    }

    auto Writer::Append(const char* data, int64_t length, bool append) -> bool {
        if(d->path.isEmpty()) return false;
        if(length < 1) return false;

        const auto openMode = [&]()->QIODevice::OpenMode {
            if(append) return QIODevice::WriteOnly | QIODevice::Append;
            return QIODevice::WriteOnly | QIODevice::Truncate;
        }();

        QFile file(d->path);
        if(!file.open(openMode)) {
            return false;
        }

        return length == file.write(data, length);
    }
}
