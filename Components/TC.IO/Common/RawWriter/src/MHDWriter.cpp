#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include "MHDWriter.h"

namespace TC::IO::RawWriter {
    struct MHDWriter::Impl {
        QString path;
        std::vector<size_t> size;
        std::vector<float> spacing;
        Writer::Type type{ Writer::Type::NOTDEFINED };

        auto size2str() -> QString {
            QString str;
            std::for_each(size.begin(), size.end(), [&str](size_t val) {
                str.push_back(QString::number(val));
                str.push_back(" ");
            });
            return str.trimmed();
        }

        auto spacing2str() -> QString {
            QString str;
            std::for_each(spacing.begin(), spacing.end(), [&str](float val) {
                str.push_back(QString::number(val));
                str.push_back(" ");
            });
            return str.trimmed();
        }

        auto type2str() -> QString {
            switch(type) {
            case Writer::Type::INT8:
                return "MET_CHAR";
            case Writer::Type::UINT8:
                return "MET_UCHAR";
            case Writer::Type::INT16:
                return "MET_SHORT";
            case Writer::Type::UINT16:
                return "MET_USHORT";
            case Writer::Type::INT32:
                return "MET_INT";
            case Writer::Type::UINT32:
                return "MET_UINT";
            case Writer::Type::INT64:
                return "MET_LONG_LONG";
            case Writer::Type::UINT64:
                return "MET_ULONG_LONG";
            case Writer::Type::FLOAT:
                return "MET_FLOAT";
            case Writer::Type::DOUBLE:
                return "MET_DOUBLE";
            default:
                return "";
            }
            return "";
        }

        auto path2datafile() -> QString {
            const QFileInfo info(path);
            return info.completeBaseName() + ".raw";
        }
    };

    MHDWriter::MHDWriter() : d{new Impl} {
    }

    MHDWriter::~MHDWriter() {
    }

    auto MHDWriter::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto MHDWriter::SetSize(const std::vector<size_t>& size) -> void {
        d->size = size;
    }

    auto MHDWriter::SetSpacing(const std::vector<float>& spacing) -> void {
        d->spacing = spacing;
    }

    auto MHDWriter::SetDataType(Writer::Type type) -> void {
        d->type = type;
    }

    auto MHDWriter::Write() -> bool {
        QFile file(d->path);
        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            return false;
        }

        QTextStream out(&file);
        out << "ObjectType = Image" << Qt::endl;
        out << "NDims = " << d->size.size() << Qt::endl;
        out << "BinaryData = True" << Qt::endl;
        out << "BinaryDataByteOrderMSB = False" << Qt::endl;
        out << "CompressedData = False" << Qt::endl;
        out << "TransformMatrix = 1 0 0 0 1 0 0 0 1" << Qt::endl;
        out << "Offset = 0 0 0" << Qt::endl;
        out << "CenterOfRotation = 0 0 0" << Qt::endl;
        out << "AnatomicalOrientation = RAI" << Qt::endl;
        out << "ElementSpacing = " << d->spacing2str() << Qt::endl;
        out << "DimSize = " << d->size2str() << Qt::endl;
        out << "ElementType = " << d->type2str() << Qt::endl;
        out << "ElementDataFile = " << d->path2datafile() << Qt::endl;

        return true;
    }
}
