#pragma once

#include <memory>
#include <QString>

#include "TCFThumbnail.h"
#include "TCTCFIOExport.h"

namespace TC::TCFIO {
    class TCTCFIO_API TCFThumbnailReader {
    public:
        enum class Type { HT, FL, BF };

        TCFThumbnailReader();
        ~TCFThumbnailReader();

        auto SetTCFFilePath(const QString& tcfFilePath)->void;
        auto SetTarget(const Type& type, const int32_t& timeFrameIndex)->void;

        auto Read()->bool;

        auto GetThumbnail()->TCFThumbnail;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
