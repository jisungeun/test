#pragma once

#include <memory>

#include <QString>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include "TCTCFIOExport.h"

// TCF 이해 후 TCFMetaReader와 TCFRawDataReader를 합친 reader를 만들어야겠다. by yugun.hwang

namespace TC::IO {
	class TCTCFIO_API TCFRawDataReader {
	public:
		enum RawDataType { // 3D는 아직 지원 안함
		    HT_3D, // 지원 안함
			HT_MIP,
			FL_3D, // 지원 안함
			FL_MIP,
			PHASE,
			BF,
		};

        struct FLChannelData {
			std::shared_ptr<uint16_t[]> data = nullptr;
			uint8_t r = 0;
			uint8_t g = 0;
			uint8_t b = 0;
        };

	public:
		TCFRawDataReader();
		~TCFRawDataReader();

		bool Open(const QString& path);
		bool Exist(const RawDataType& type);

		bool ReadHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data) const;
		bool ReadLdmHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data,int &x,int &y)const;
		bool ReadFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data)const;
		bool ReadLdmFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data,int&x,int&y)const;
		bool ReadBF(const int& index, std::shared_ptr<uint32_t[]>& data) const;
		bool ReadLdmBF(const int& index, std::shared_ptr<uint32_t[]>& data,int&x,int&y)const;

		bool GetHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data) const;
		bool GetLdmHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data, int& x, int& y)const;
		bool GetFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data)const;
		bool GetLdmFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data, int& x, int& y)const;
		bool GetBF(const int& index, std::shared_ptr<uint32_t[]>& data) const;
		bool GetLdmBF(const int& index, std::shared_ptr<uint32_t[]>& data, int& x, int& y)const;

	private:
		auto GetGroupPathFromType(const RawDataType& type)->QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}