#pragma once
#include <memory>

#include <QString>
#include <QMap>

#include "TCTCFIOExport.h"

namespace TC::IO {
    /**
     * \brief TCF를 작성하기 위해 필요한 정보 및 현미경 정보를 갖는 클래스
     */
    class TCTCFIO_API TCFMetaInfo {
    public:
        /**
         * \brief ID 정보를 갖는 구조체
         * \member dataID : data id (내용 보완 필요)
         * \member uniqueID : unique id (내용 보완 필요)
         * \member userID : user id (내용 보완 필요)
         */
        struct IDs {
            QString dataID{ " " };
            QString uniqueID{ " " };
            QString userID{ " " };
        };

        /**
         * \brief 현미경에 관련된 정보를 갖는 구조체
         * \member deviceHost : 현미경을 사용한 PC의 이름
         * \member deviceSerial : 현미경의 일련 번호
         * \member deviceSoftwareVersion : 현미경 사용시 이용한 프로그램 버전
         */
        struct Devices {
            QString deviceHost{ " " };
            QString deviceSerial{ " " };
            QString deviceSoftwareVersion{ " " };
        };

        /**
         * \brief Zero Padding 정보를 갖는 구조체
         * \member zp : hologram 이미지의 zero padding 길이 (내용 보완 필요)
         * \member zp2 : HT데이터에서 x, y축 zero padding 길이
         * \member zp3 : HT데이터에서 z축 zero padding 길이
         */
        struct ZPs {
            double zp{ 0 };
            double zp2{ 0 };
            double zp3{ 0 };
        };

        /**
         * \brief 데이터가 획득된 현미경 위치 정보를 갖는 구조체
         * \member x : raw데이터가 획득된 X축 위치 (mm)
         * \member y : raw데이터가 획득된 Y축 위치 (mm)
         * \member z : raw데이터가 획득된 Z축 위치 (mm)
         * \member c : raw데이터가 획득된 C축 위치 (mm) (C축 : Focus?, 내용 보완 필요)
         */
        struct Positions {
            double x{ 0 };
            double y{ 0 };
            double z{ 0 };
            double c{ 0 };
        };

        /**
         * \brief Tile 크기 정보를 갖는 구조체
         * \member tileNumber : 전체 tile의 개수
         * \member tileHorizontal : 가로 방향 tile의 개수
         * \member tileVertical : 세로 방향 tile의 개수
         */
        struct TileSizes {
            int64_t tileNumber{ 0 };
            int64_t tileHorizontal{ 0 };
            int64_t tileVertical{ 0 };
        };

        /**
         * \brief Tile 위치 정보를 갖는 구조체
         * \member columnIndex : tile의 열 위치
         * \member rowIndex : tile의 횡 위치
         */
        struct TilePositions {
            int64_t columnIndex{ 0 };
            int64_t rowIndex{ 0 };
        };

        /**
         * \brief Tile의 overlap 길이 정보를 갖는 구조체
         * \member horizontalLengthInMicroMeter : 가로방향 겹쳐진 길이 (um)
         * \member verticalLengthInMicroMeter : 세로방향 겹쳐진 길이 (um)
         */
        struct OverlapLengths {
            double horizontalLengthInMicroMeter{ 0 };
            double verticalLengthInMicroMeter{ 0 };
        };

        /**
         * \brief TCF 일반 정보를 갖는 구조체
         * \member ids : ID 정보
         * \member devices : device 정보
         * \member formatVersion : TCF 포맷 버전 (N.N.N)
         * \member softwareVersion : TCF를 생성한 소프트웨어의 버전 (N.N.N )
         * \member createDate : TCF가 생성된 시각
         * \member description : 설명 (내용 보완 필요)
         * \member recordingTime : 데이터가 획득된 시각
         * \member title : TCF 이름
         */
        struct Common {
            IDs ids{ " "," "," " };
            Devices devices{ " "," "," " };
            QString formatVersion{ " " };
            QString softwareVersion{ " " };
            QString createDate{ " " };
            QString description{ " " };
            QString recordingTime{ " " };
            QString title{ " " };
        };

        /**
         * \brief TCF에서 Info/Annotation 그룹에 적히게 될 내용을 갖는 구조체
         * \member annotations : annotation 정보
         */
        struct Annotation {
            QMap<QString, QString> annotations;
        };

        /**
         * \brief TCF에서 Info/Device 그룹에 적히게 될 내용을 갖는 구조체
         * \member iteration : HT 데이터 reconstruction 반복 횟수
         * \member magnification : 렌즈 배율
         * \member NA : 렌즈의 NA 값
         * \member RI : medium RI 값
         * \member RawSize : ?? (내용 보완 필요)
         * \member waveLength : 레이저의 파장 (um)
         * \member zps : zero padding 정보
         */
        struct Device {
            double iteration{ 0 };
            double magnification{ 0 };
            double na{ 0 };
            double ri{ 0 };
            double rawSize{ 0 };
            double waveLength{ 0 };
            ZPs zps{ 0,0,0 };
        };

        /**
         * \brief TCF에서 Info/Imaging 그룹에 적히게 될 내용을 갖는 구조체
         * \member cameraGain : 카메라 이득 값
         * \member cameraShutter : 카메라 셔터 값 (ms)
         */
        struct Imaging {
            double cameraGain{ 0 };
            double cameraShutter{ 0 };
        };

        /**
         * \brief TCF에서 Info/Tile 그룹에 적히게 될 내용을 갖는 구조체
         * \member tilePositions : tile 위치 정보
         * \member overlapLength : tile 간의 겹친 길이 정보
         * \member tileSizes : 전체 타일 크기에 관한 정보
         * \member positions : 타일 위치 정보
         */
        struct Tile {
            TilePositions tilePositions{ 0,0 };
            OverlapLengths overlapLengths{ 0,0 };
            TileSizes tileSizes{ 0,0,0 };
            Positions positions{ 0,0,0,0 };
        };

        TCFMetaInfo();
        TCFMetaInfo(const TCFMetaInfo& other);
        ~TCFMetaInfo();
        auto operator=(const TCFMetaInfo& other)->TCFMetaInfo;

        auto SetCommon(const Common& common)->void;
        auto SetAnnotation(const Annotation& annotation)->void;
        auto SetDevice(const Device& device)->void;
        auto SetImaging(const Imaging& imaging)->void;
        auto SetTile(const Tile& tile)->void;

        auto IsTileIncluded() const->bool;

        auto GetCommon() const->Common;
        auto GetAnnotation() const->Annotation;
        auto GetDevice() const->Device;
        auto GetImaging() const->Imaging;
        auto GetTile() const->Tile;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
