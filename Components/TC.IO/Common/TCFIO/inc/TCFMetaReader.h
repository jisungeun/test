#pragma once

#include <memory>
#include <vector>

#include <QString>
#include <QMap>
#include <QList>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include "TCTCFIOExport.h"

// TCF 이해 후 TCFMetaReader와 TCFRawDataReader를 합친 reader를 만들어야겠다. by yugun.hwang

namespace TC::IO {
	class TCTCFIO_API TCFMetaReader {
	public:
		struct Dimension {
			int x = 0;
			int y = 0;
			int z = 0;
		};

		struct CommonMeta {
			QString title;

			QString uniqueID;
			QString dataID;
			QString userID;

			QString createDate;
			QString recordingTime;

			QString deviceModelType;
			QString deviceSerial;
			QString softwareVersion;
			QString formatVersion;

			QString description;
		};

		struct AnnotationMeta {
			QString cellType;
			QString tfPreset;
		};

		struct DeviceMeta {
			double iteration = 0.0;
			double magnification = 0.0;
			double na = 0.0;
			double ri = 0.0;
			double rawSize = 0.0;
			double waveLength = 0.0;
			double zp = 0.0;
			double zp2 = 0.0;
			double zp3 = 0.0;
		};

		struct ImagingMeta {
			double cameraGain = 0.0;
			double cameraShutter = 0.0;
		};

		struct TileMeta {
			bool exist = false;

			int tiles = 0;
			int tilesH = 0;
			int tilesV = 0;
			int columnIndex = 0;
			int rowIndex = 0;

			double overlapH = 0.0;
			double overlapV = 0.0;

			double c = 0.0;
			double x = 0.0;
			double y = 0.0;
			double z = 0.0;
		};

		struct Data3D {
			bool exist = false;

			int dataCount = 0;
			double riMax = 0.0;
			double riMin = 0.0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			double resolutionZ = 0.0;
			int sizeX = 0;
			int sizeY = 0;
			int sizeZ = 0;
			int tileSizeX = 256;
			int tileSizeY = 256;
			int tileSizeZ = 256;
			double timeInterval = 0.0;

			int sampleStep = 1;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;

			QList<int> scalarType;
			QList<double> timePoints;
            QList<double> riMinList;
			QList<double> riMaxList;
		};

		struct Data2D {
			bool exist = false;

			int dataCount = 0;
			double maxPhase = 0.0;
			double minPhase = 0.0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			double resolutionT = 0.0;
			int sizeX = 0;
			int sizeY = 0;
			int sizeT = 0;
			double timeInterval = 0.0;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;
		};

		struct Data2DMIP {
			bool exist = false;

			int dataCount = 0;
			double riMax = 0.0;
			double riMin = 0.0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			int sizeX = 0;
			int sizeY = 0;
			double timeInterval = 0.0;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;

			int tileSizeX = 256;
			int tileSizeY = 256;

			QList<int> scalarType;
		    QList<double> timePoints;
            QList<double> riMinList;
			QList<double> riMaxList;
		};

		struct Data3DFL {
			bool exist = false;

			int channels = 0;
			std::vector<int32_t> channelList;

			int dataCount = 0;
			double maxIntensity = 0.0;
			double minIntensity = 0.0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			double resolutionZ = 0.0;
			int sizeX = 0;
			int sizeY = 0;
			int sizeZ = 0;
			double timeInterval = 0.0;

			double offsetZ = 0.0;
			double offsetZCompensation = 0.0;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;

			int tileSizeX = 256;
			int tileSizeY = 256;
			int tileSizeZ = 256;

			QString name[3];
			int r[3]{ 0, };
			int g[3]{ 0, };
			int b[3]{ 0, };

			QList<int> scalarType[3];
			QList<double> timePoints[3];
            QMap<int32_t, QList<double>> minIntensityList;
			QMap<int32_t, QList<double>> maxIntensityList;

			bool valid[3] = { false, };
			double max[3] = { 0.0, };
			double min[3] = { 0.0, };

			double emission[3] = { 0.0, };
			double excitation[3] = { 0.0, };
			int exposureTime[3] = { 0, };
			int intensity[3] = { 0, };
		};

		struct Data2DFLMIP {
			bool exist = false;

			int channels = 0;
			std::vector<int32_t> channelList;

			int dataCount = 0;
			double maxIntensity = 0.0;
			double minIntensity = 0.0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			int sizeX = 0;
			int sizeY = 0;
			double timeInterval = 0.0;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;

			int tileSizeX = 256;
			int tileSizeY = 256;

			QList<int> scalarType[3];
			QList<double> timePoints[3];
            QMap<int32_t, QList<double>> minIntensityList;
			QMap<int32_t, QList<double>> maxIntensityList;

			QString name[3];
			int r[3];
			int g[3];
			int b[3];

			bool valid[3] = { false, };
			double max[3] = { 0.0, };
			double min[3] = { 0.0, };

			double emission[3] = { 0.0, };
			double excitation[3] = { 0.0, };
			int exposureTime[3] = { 0, };
			int intensity[3] = { 0, };
		};

		struct DataBF {
			bool exist = false;

			int dataCount = 0;
			double resolutionX = 0.0;
			double resolutionY = 0.0;
			int sizeX = 0;
			int sizeY = 0;

			int tileSizeX = 256;
			int tileSizeY = 256;

			double timeInterval = 0.0;

			double positionX = 0.0;
			double positionY = 0.0;
			double positionZ = 0.0;
			double positionC = 0.0;

			QList<double> timePoints;
		};

		struct ThumbnailMetaData {
			bool exist{ false };
			bool isColor{ false };

			int32_t dataCount{};

			double resolutionX{};
			double resolutionY{};

			int32_t sizeX{};
			int32_t sizeY{};
		};

		struct InfoMeta {
			AnnotationMeta annotation;
			DeviceMeta device;
			ImagingMeta imaging;
			TileMeta tile;
		};

		struct DataMeta {
			Data3D data3D;
			Data2D data2D;
			Data2DMIP data2DMIP;
			Data3DFL data3DFL;
			Data2DFLMIP data2DFLMIP;
			DataBF dataBF;
			bool isLDM;
			QList<double> total_time;
		};

		struct ThumbnailMeta {
			ThumbnailMetaData ht{};
			ThumbnailMetaData fl{};
			ThumbnailMetaData bf{};
		};

		struct Meta {
			CommonMeta common;
			InfoMeta info;
			DataMeta data;
			ThumbnailMeta thumbnail{};

			typedef std::shared_ptr<Meta> Pointer;
		};

	public:
		TCFMetaReader();
		~TCFMetaReader();

		auto Read(const QString& path)->Meta::Pointer;
		auto ReadArrSize(const QString& path)->uint64_t;
		auto ReadIsLDM(const QString& path)->bool;
		auto ReadMediumRI(const QString& path)->double;

	private:
		// Components/IO/TCTCFIO/TCFSimpleReader와 중복(구현 후 통합 필요)
		auto ReadAttribute(H5::H5Object& object, const char* name, QString& value) const->bool;
		auto ReadAttribute(H5::H5Object& object, const char* name, double& value) const->bool;
		auto ReadAttribute(H5::H5Object& object, const char* name, int& value) const->bool;
		auto ReadAttribute(H5::H5Object& object, const char* name, int64_t& value) const->bool;

		auto ReadAttribute(const H5::H5Object& object, const char* name, QString& value) const->bool;
		auto ReadAttribute(const H5::H5Object& object, const char* name, double& value) const->bool;
		auto ReadAttribute(const H5::H5Object& object, const char* name, int& value) const->bool;
		auto ReadAttribute(const H5::H5Object& object, const char* name, int64_t& value) const->bool;

		auto ReadAttribute(H5::Attribute& attr, QString& value) const->bool;
		auto ReadAttribute(H5::Attribute& attr, double& value) const->bool;
		auto ReadAttribute(H5::Attribute& attr, int& value) const->bool;
		auto ReadAttribute(H5::Attribute& attr, int64_t& value) const->bool;

		auto GetDimension(const H5::DataSet& dataSet)->Dimension;
		auto GetThumbnailMetaData(const H5::H5File& file, const char* thumbnailGroupPath) const->ThumbnailMetaData;
	};
}