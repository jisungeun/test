#pragma once
#include <memory>

#include "TCTCFIOExport.h"

#include "DataInfo.h"
#include "TCFMetaInfo.h"

namespace TC::IO {
    class TCTCFIO_API TCFWritingData {
    public:
        typedef TCFWritingData Self;
        typedef std::shared_ptr<Self> Pointer;

        TCFWritingData();
        TCFWritingData(const TCFWritingData& other);
        ~TCFWritingData();
        auto operator=(const TCFWritingData& other)->TCFWritingData&;

        /**
         * \brief 인스턴스에 정보를 넣어두는 함수
         * \param dataInfo raw data와 그에 관련된 attribute를 담고 있는 정보
         * \param metaInfo raw data 및 attribute를 제외한 TCF를 작성하기 위한 정보
         */
        auto SetInfo(const DataInfo& dataInfo, const TCFMetaInfo& metaInfo)->void;
        /**
         * \brief dataInfo를 참조하기 위한 함수
         * \return 인스턴스의 dataInfo
         */
        auto GetDataInfo() const ->DataInfo;
        /**
         * \brief metaInfo를 참조하기 위한 함수
         * \return 인스턴스의 metaInfo
         */
        auto GetMetaInfo() const ->TCFMetaInfo;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
