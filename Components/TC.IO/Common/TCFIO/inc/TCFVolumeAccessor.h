#pragma once

#include <memory>

#include <QString>

#include "VolumeAccessor.h"

#include "TCTCFIOExport.h"

namespace TC {
    namespace IO {
        class TCTCFIO_API TCFVolumeAccessor : IVolumeAccessor {
        public:
            explicit TCFVolumeAccessor();
            TCFVolumeAccessor(const TCFVolumeAccessor& other);
            virtual ~TCFVolumeAccessor();

            auto GetVoxelAsUShort(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1)->uint32_t override;
            auto GetVoxelAsFloat(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1) -> float override;
            auto SetVoxelWithUShort(uint32_t val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1) -> void override;
            auto SetVoxelWithFloat(float val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord = 1) -> void override;

            auto GetBufferAsUShort() -> std::shared_ptr<unsigned short> override;
            auto GetBufferAsFloat() -> std::shared_ptr<float> override;

            auto SetBufferWithUShort(unsigned short* buf) -> void override;
            auto SetBufferWithFloat(float* buf) -> void override;

            auto SetFilePath(QString path)->void;
            auto GetFilePath(void)->QString;

            auto OpenFile()->void;
            auto CloseFile()->void;

            auto SetDimension(uint32_t xDim, uint32_t yDim, uint32_t zDim = 1)->void;
            auto SetROI(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax)->void;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };
    }
}