#pragma once
#include <memory>
#include <any>

#include <QString>

#include <enum.h>

#include "TCTCFIOExport.h"

namespace TC::IO {
    BETTER_ENUM(ImageType, uint8_t, HT, FLBLUE, FLGREEN, FLRED, PHASE, BF)

    /**
     * \brief raw 데이터 및 그에 관련된 Attribute를 포함하는 정보를 갖는 클래스
     */
    class TCTCFIO_API DataInfo {
    public:
        /**
         * \brief
         * \member data3d : 3d 데이터
         * \member dataMip : 3d 데이터의 Mip 데이터
         */
        struct RawData {
            std::any data3d{};
            std::any dataMip{};
        };

        /**
         * \brief raw 데이터의 해상도를 갖는 구조체
         * \member resolutionX : X축 해상도
         * \member resolutionY : Y축 해상도
         * \member resolutionZ : Z축 해상도
         * \member resolutionT : phase에서의 해상도 (설명 보완 필요)
         */
        struct Resolutions {
            double resolutionX{ 0 };
            double resolutionY{ 0 };
            double resolutionZ{ 0 };
            double resolutionT{ 0 };
        };

        /**
         * \brief raw 데이터의 최대/최소 값을 갖는 구조체
         * \member min : raw 데이터의 최소값
         * \member max : raw 데이터의 최대값
         */
        struct MinMax {
            double min{ 0 };
            double max{ 0 };
        };

        /**
         * \brief raw 데이터의 각 축 크기를 갖는 구조체
         * \member sizeX : raw 데이터의 X축 길이
         * \member sizeY : raw 데이터의 Y축 길이
         * \member sizeZ : raw 데이터의 Z축 길이
         * \member sizeT : phase에서의 Time축(Z축) 길이
         */
        struct Sizes {
            int64_t sizeX{ 0 };
            int64_t sizeY{ 0 };
            int64_t sizeZ{ 0 };
            int64_t sizeT{ 0 };
        };

        /**
         * \brief raw 데이터가 획득된 현미경 위치 정보를 갖는 구조체
         * \member positionX : raw데이터가 획득된 X축 위치 (mm)
         * \member positionY : raw데이터가 획득된 Y축 위치 (mm)
         * \member positionZ : raw데이터가 획득된 Z축 위치 (mm)
         * \member positionC : raw데이터가 획득된 C축 위치 (mm) (C축 : Focus?, 내용 보완 필요)
         */
        struct Positions {
            double positionX{ 0 };
            double positionY{ 0 };
            double positionZ{ 0 };
            double positionC{ 0 };
        };

        /**
         * \brief FL 데이터와 HT 데이터의 상대적 거리 정보를 갖는 구조체
         * \member offsetZ : HT 데이터의 바닥 기준 FL 데이터의 중앙까지의 거리 (um)
         * \member offsetZCompensation : 현미경의 오차 범위 보정 (um) (내용 보완 필요)
         */
        struct OffsetZs {
            double offsetZ{ 0 };
            double offsetZCompensation{ 0 };
        };

        DataInfo();
        DataInfo(const DataInfo& other);
        ~DataInfo();
        auto operator=(const DataInfo& other)->DataInfo;

        auto SetRawData(const RawData& rawData)->void;
        auto SetImageType(const ImageType& imageType)->void;
        auto SetMinMax3d(const MinMax& minMax3d)->void;
        auto SetMinMaxMip(const MinMax& minMaxMip)->void;
        auto SetPositions(const Positions& positions)->void;
        auto SetRecordingTime(const QString& recordingTime)->void;
        auto SetChannels(const int64_t& channels)->void;
        auto SetOffsetZs(const OffsetZs& offsetZs)->void;
        auto SetDataCount(const int64_t& dataCount)->void;
        auto SetResolutions(const Resolutions& resolutions)->void;
        auto SetSizes(const Sizes& sizes)->void;
        auto SetTimeInterval(const double& timeIntervalInSecs)->void;
        auto SetTimelapseIndex(const uint32_t& timelapseIndex)->void;

        auto GetRawData() const ->RawData;
        auto GetImageType() const ->ImageType;
        auto GetMinMax3d() const ->MinMax;
        auto GetMinMaxMip() const ->MinMax;
        auto GetPositions() const ->Positions;
        auto GetRecordingTime() const ->QString;
        auto GetChannels() const ->int64_t;
        auto GetOffsetZs() const ->OffsetZs;
        auto GetDataCount() const ->int64_t;
        auto GetResolutions() const ->Resolutions;
        auto GetSizes() const ->Sizes;
        auto GetTimeIntervalInSecs() const ->double;
        auto GetTimelapseIndex() const->uint32_t;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
