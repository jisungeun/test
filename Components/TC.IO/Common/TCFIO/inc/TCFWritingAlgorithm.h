#pragma once

#include <memory>

#include "TCTCFIOExport.h"
#include "TCFWritingData.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

namespace TC::IO {
    class TCTCFIO_API TCFWritingAlgorithm {
    public:
        TCFWritingAlgorithm();
        ~TCFWritingAlgorithm();

        /**
         * \brief 1개의 datatype, 1개의 timeframe에 해당하는 data를 TCF 포맷에 맞춰 파일을 쓴다.
         * \param tcfPath TCF 경로
         * \param data 쓰는 내용
         */
        auto Write(const QString& tcfPath, const TCFWritingData& data)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto FileExists(const QString& tcfPath) ->bool;

        auto WriteMetaInfo(const TCFMetaInfo& metaInfo) const->void;
        auto WriteDataInfo(const DataInfo& dataInfo) const->void;

        auto WriteHt(const DataInfo& dataInfo) const->void;
        auto WriteFl(const DataInfo& dataInfo, const uint32_t& channelIndex) const ->void;
        auto WriteFlBlue(const DataInfo& dataInfo) const->void;
        auto WriteFlGreen(const DataInfo& dataInfo) const->void;
        auto WriteFlRed(const DataInfo& dataInfo) const->void;
        auto WritePhase(const DataInfo& dataInfo) const->void;
        auto WriteBf(const DataInfo& dataInfo) const->void;

        static auto WriteAttributeString(const H5::H5Object& target, const QString& name, const QString& contents)
            -> void;
        static auto WriteAttributeDouble(const H5::H5Object& target, const QString& name, const double& contents)
            -> void;
        static auto WriteAttributeInt64(const H5::H5Object& target, const QString& name, const int64_t& contents)
            -> void;

        auto WriteCommonAttributes(const TCFMetaInfo& metaInfo) const ->void;
        auto WriteAnnotationAttributes(const TCFMetaInfo& metaInfo) const->void;
        auto WriteDeviceAttributes(const TCFMetaInfo& metaInfo) const->void;
        auto WriteImagingAttributes(const TCFMetaInfo& metaInfo) const->void;
        auto WriteTileAttributes(const TCFMetaInfo& metaInfo) const->void;

        static auto OpenOrCreateGroupIfNotExists(const H5::Group& parentGroup, const std::string& groupName)
            ->H5::Group;
        static auto OpenOrCreateAttributeIfNotExists(const H5::H5Object& parentObject, const QString& attributeName,
            const H5::DataType& dataType) -> H5::Attribute;
        static auto OpenOrCreateDataSet3dIfNotExists(const H5::Group& parentGroup, const uint32_t& timelapseIndex,
            const DataInfo::Sizes& sizes) -> H5::DataSet;
        static auto OpenOrCreateDataSetMipIfNotExists(const H5::Group& parentGroup, const uint32_t& timelapseIndex,
            const DataInfo::Sizes& sizes)->H5::DataSet;
        static auto OpenOrCreateDataSetPhaseIfNotExists(const H5::Group& parentGroup, const uint32_t& timelapseIndex,
            const DataInfo::Sizes& sizes)->H5::DataSet;
        static auto OpenOrCreateDataSetBfIfNotExists(const H5::Group& parentGroup, const uint32_t& timelapseIndex,
            const DataInfo::Sizes& sizes)->H5::DataSet;
        static auto CreateSimpleDataSpace()->H5::DataSpace;
    };
}
