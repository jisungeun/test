#pragma once

#include <memory>

#include "TCTCFIOExport.h"

namespace TC::TCFIO {
    class TCTCFIO_API TCFThumbnail {
    public:
        TCFThumbnail();
        TCFThumbnail(const TCFThumbnail& rhs);
        ~TCFThumbnail();

        auto operator=(const TCFThumbnail& rhs)->TCFThumbnail&;

        auto SetData(const std::shared_ptr<uint8_t[]>& data)->void;
        auto GetData()const->const std::shared_ptr<uint8_t[]>&;

        auto SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->void;
        auto GetSizeX()const->const int32_t&;
        auto GetSizeY()const->const int32_t&;
        auto GetSizeZ()const->const int32_t&;

        auto SetResolution(const double& resolutionX, const double& resolutionY)->void;
        auto GetResolutionX()const->const double&;
        auto GetResolutionY()const->const double&;

        auto SetColorFlag(const bool& colorFlag)->void;
        auto GetColorFlag()const->const bool&;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
