#pragma once

#include <memory>

#include <QString>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include "VolumeData.h"
#include "TCTCFIOExport.h"

namespace TC::IO {
    /**
     * \brief TCF로 부터 1개의 VolumeData를 읽어들이는 단순한 형태의 Reader 구현
     */
    class TCTCFIO_API TCFSimpleReader {
    public:
        explicit TCFSimpleReader(const QString& path = QString());
        virtual ~TCFSimpleReader();

        /**
         * \brief 파일 경로 설정
         * \param path 파일 경로
         */
        auto SetFilePath(const QString& path)->void;
        /**
         * \brief FL channel 이미지가 존재하는지 확인
         * \param ch 채널 번호
         * \return true/false
         */
        auto FLExist(int ch)const->bool;
        /**
         * \brief BF 이미지가 존재하는지 확인
         * \return true/false
         */        
        auto BFExists(void) const->bool;

        /**
         * \brief HT MIP 이미지 얻기
         * \param index Time index
         * \return VolumeData
         */
        auto ReadHTMIP(uint32_t index = 0,bool readAdFloat = false) const->VolumeData;
        /**
         * \brief HT 3D 이미지 얻기
         * \param index Time index
         * \param readAsFloat 강제로 Float Type으로 읽어 들이기
         * \return VolumeData
         */
        auto ReadHT3D(uint32_t index = 0, bool readAsFloat=false) const->VolumeData;
        /**
         * \brief FL MIP 이미지 얻기
         * \param ch 채널 번호
         * \param index Time index
         * \return VolumeData
         */
        auto ReadFLMIP(int ch,uint32_t index = 0)const->VolumeData;
        /**
         * \brief HT 3D 이미지 얻기
         * \param ch 채널 번호
         * \param index Time index         
         * \return VolumeData
         */
        auto ReadFL3D(int ch, uint32_t index = 0)const->VolumeData;
        /**
         * \brief BF 이미지 얻기
         * \param index Time index
         * \return VolumeData
         */
        auto ReadBF(uint32_t index = 0) const->VolumeData;

    private:
        auto ReadAttribute(H5::Group& group, const char* name, QString& strString) const->bool;
        auto ReadAttribute(H5::Group& group, const char* name, double& dValue) const->bool;
        auto ReadAttribute(H5::Group& group, const char* name, int& nValue) const->bool;

        auto ReadAttribute(H5::DataSet& dataset, const char* name, QString& strString) const->bool;
        auto ReadAttribute(H5::DataSet& dataset, const char* name, double& dValue) const->bool;
        auto ReadAttribute(H5::DataSet& dataset, const char* name, int& nValue) const->bool;

        auto ReadAttribute(H5::Attribute& attr, QString& strString) const->bool;
        auto ReadAttribute(H5::Attribute& attr, double& dValue) const->bool;
        auto ReadAttribute(H5::Attribute& attr, int& nValue) const->bool;

        auto ReadAsFloat(H5::DataSet& dataSet, VolumeData& volumeData, const double numerator = 1.0) const->bool;
        auto ReadAsUInt16(H5::DataSet& dataSet, VolumeData& volumeData) const->bool;
        auto ReadAsRBG32(H5::DataSet& dataSet, VolumeData& volumeData) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}