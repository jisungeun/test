#include "TCFThumbnail.h"

namespace TC::TCFIO {
    class TCFThumbnail::Impl {
    public:
        Impl() = default;
        Impl(const Impl& rhs) = default;
        ~Impl() = default;

        auto operator=(const Impl& rhs)->Impl & = default;

        std::shared_ptr<uint8_t[]> data{};

        int32_t sizeX{};
        int32_t sizeY{};
        int32_t sizeZ{};

        double resolutionX{};
        double resolutionY{};

        bool colorFlag{false};
    };

    TCFThumbnail::TCFThumbnail() : d(std::make_unique<Impl>()) {
    }

    TCFThumbnail::TCFThumbnail(const TCFThumbnail& rhs) : d(std::make_unique<Impl>(*rhs.d)) {
    }

    TCFThumbnail::~TCFThumbnail() = default;

    auto TCFThumbnail::operator=(const TCFThumbnail& rhs) -> TCFThumbnail& {
        *(this->d) = *(rhs.d);
        return *this;
    }

    auto TCFThumbnail::SetData(const std::shared_ptr<uint8_t[]>& data) -> void {
        d->data = data;
    }

    auto TCFThumbnail::GetData() const -> const std::shared_ptr<uint8_t[]>& {
        return d->data;
    }

    auto TCFThumbnail::SetSize(const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
        d->sizeZ = sizeZ;
    }

    auto TCFThumbnail::GetSizeX() const -> const int32_t& {
        return d->sizeX;
    }

    auto TCFThumbnail::GetSizeY() const -> const int32_t& {
        return d->sizeY;
    }

    auto TCFThumbnail::GetSizeZ() const -> const int32_t& {
        return d->sizeZ;
    }

    auto TCFThumbnail::SetResolution(const double& resolutionX, const double& resolutionY) -> void {
        d->resolutionX = resolutionX;
        d->resolutionY = resolutionY;
    }

    auto TCFThumbnail::GetResolutionX() const -> const double& {
        return d->resolutionX;
    }

    auto TCFThumbnail::GetResolutionY() const -> const double& {
        return d->resolutionY;
    }

    auto TCFThumbnail::SetColorFlag(const bool& colorFlag) -> void {
        d->colorFlag = colorFlag;
    }

    auto TCFThumbnail::GetColorFlag() const -> const bool& {
        return d->colorFlag;
    }
}
