#include "DataInfo.h"

namespace TC::IO {
    struct DataInfo::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        /**
         * \brief rawData 및 그에 관련된 정보들
         * \member rawData : 저장하는 데이터
         * \member imageType : 저장하는 데이터의 이미지 타입
         * \member minMax : 데이터의 최대 최소값
         * \member position : 데이터가 획득된, 현미경의 위치
         * \member recordingTime : 데이터가 획득된 시각 (YYYY-MM-DD hh:mm:ss.mss)
         * \member channels : FL 데이터의 전체 채널 수
         * \member offsetZ : HT데이터 바닥 기준 FL 중앙까지의 거리 (um)
         * \member dataCount : timelapse 전체 수
         * \member resolutions : 데이터의 해상도 정보 (um)
         * \member sizes : 데이터의 각 축 픽셀 길이 (pixel)
         * \member timeInterval : timelapse 획득 시 시간 간격 (sec)
         * \member timelapseIndex : timelapse 순서 정보
         */
        RawData rawData{};
        ImageType imageType{ ImageType::HT };

        MinMax minMax3d{ 0,0 };
        MinMax minMaxMip{ 0,0 };
        Positions positions{ 0,0,0,0 };
        QString recordingTime{ " " };

        int64_t channels{ 0 };
        OffsetZs offsetZs{ 0,0 };

        int64_t dataCount{ 0 };
        Resolutions resolutions{ 0,0,0,0 };
        Sizes sizes{ 0,0,0,0 };
        double timeInterval{0};

        uint32_t timelapseIndex{ 0 };
    };

    DataInfo::DataInfo()
        : d(new Impl()) {
    }

    DataInfo::DataInfo(const DataInfo& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    DataInfo::~DataInfo() = default;

    auto DataInfo::operator=(const DataInfo& other) -> DataInfo {
        d = std::make_unique<Impl>(*other.d);
        return *this;
    }

    auto DataInfo::SetRawData(const RawData& rawData) -> void {
        d->rawData = rawData;
    }

    auto DataInfo::SetImageType(const ImageType& imageType) -> void {
        d->imageType = imageType;
    }

    auto DataInfo::SetMinMax3d(const MinMax& minMax3d) -> void {
        d->minMax3d = minMax3d;
    }

    auto DataInfo::SetMinMaxMip(const MinMax& minMaxMip) -> void {
        d->minMaxMip = minMaxMip;
    }

    auto DataInfo::SetPositions(const Positions& positions) -> void {
        d->positions = positions;
    }

    auto DataInfo::SetRecordingTime(const QString& recordingTime) -> void {
        d->recordingTime = recordingTime;
    }

    auto DataInfo::SetChannels(const int64_t& channels) -> void {
        d->channels = channels;
    }

    auto DataInfo::SetOffsetZs(const OffsetZs& offsetZs) -> void {
        d->offsetZs = offsetZs;
    }

    auto DataInfo::SetDataCount(const int64_t& dataCount) -> void {
        d->dataCount = dataCount;
    }

    auto DataInfo::SetResolutions(const Resolutions& resolutions) -> void {
        d->resolutions = resolutions;
    }

    auto DataInfo::SetSizes(const Sizes& sizes) -> void {
        d->sizes = sizes;
    }

    auto DataInfo::SetTimeInterval(const double& timeIntervalInSecs) -> void {
        d->timeInterval = timeIntervalInSecs;
    }

    auto DataInfo::SetTimelapseIndex(const uint32_t& timelapseIndex) -> void {
        d->timelapseIndex = timelapseIndex;
    }

    auto DataInfo::GetRawData() const -> RawData {
        return d->rawData;
    }

    auto DataInfo::GetImageType() const -> ImageType {
        return d->imageType;
    }

    auto DataInfo::GetMinMax3d() const -> MinMax {
        return d->minMax3d;
    }

    auto DataInfo::GetMinMaxMip() const -> MinMax {
        return d->minMaxMip;
    }

    auto DataInfo::GetPositions() const -> Positions {
        return d->positions;
    }

    auto DataInfo::GetRecordingTime() const -> QString {
        return d->recordingTime;
    }

    auto DataInfo::GetChannels() const -> int64_t {
        return d->channels;
    }

    auto DataInfo::GetOffsetZs() const -> OffsetZs {
        return d->offsetZs;
    }

    auto DataInfo::GetDataCount() const -> int64_t {
        return d->dataCount;
    }

    auto DataInfo::GetResolutions() const -> Resolutions {
        return d->resolutions;
    }

    auto DataInfo::GetSizes() const -> Sizes {
        return d->sizes;
    }

    auto DataInfo::GetTimeIntervalInSecs() const -> double {
        return d->timeInterval;
    }

    auto DataInfo::GetTimelapseIndex() const -> uint32_t {
        return d->timelapseIndex;
    }
}
