#define LOGGER_TAG "[TCFIO]"
#include <TCLogger.h>

#include "TCFMetaReader.h"

#include <iostream>

#include "HDF5Mutex.h"

namespace TC::IO {
	TCFMetaReader::TCFMetaReader() {

	}

	TCFMetaReader::~TCFMetaReader() = default;

	auto TCFMetaReader::ReadIsLDM(const QString& path) -> bool {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		if (true == path.isEmpty()) {
			return 0;
		}
		auto ReadToInt = [=](H5::Group& group, const char* name, int& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = 0;
			}
		};
		auto ReadToString = [=](H5::Group& group, const char* name, QString& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = "N/A";
			}

			if (true == value.isEmpty()) {
				value = "N/A";
			}
		};
		try {
			auto isLdm = false;
			H5::H5File file(path.toStdString().c_str(), H5F_ACC_RDONLY);
			auto rootGroup = file.openGroup("/");
			if (false == rootGroup.attrExists("FormatVersion")) {
				file.close();
				return isLdm;
			}
			QString formatVersion;
			ReadToString(rootGroup, "FormatVersion", formatVersion);
			rootGroup.close();

			auto minor_version = 0;
			if (formatVersion.isEmpty()) {
				file.close();
				return isLdm;
			}
			auto fv_split = formatVersion.split(".");
			if (fv_split.count() == 3) {
				minor_version = fv_split[1].toInt();
			}

			auto groupPath = "/Data";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);
				int val = 0;
				if (minor_version > 5) {
					ReadToInt(group, "StructureType", val);
				}
				else {
					ReadToInt(group, "isLDM", val);
				}
				if (val == 1) {
					isLdm = true;
				}
				group.close();
			}
			file.close();
			return isLdm;
		}
		catch (...) {
		}
		return false;
	}
	auto TCFMetaReader::ReadMediumRI(const QString& path) -> double {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		if (true == path.isEmpty()) {
			return 0;
		}
		auto ReadToDouble = [=](H5::Group& group, const char* name, double& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = 0.0;
			}
		};
		try {
			H5::H5File file(path.toStdString().c_str(), H5F_ACC_RDONLY);
			auto groupPath = "/Info/Device";
			double val = 0.0;
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);
				ReadToDouble(group, "RI", val);
				group.close();
			}
			file.close();
			return val;
		}
		catch (...) {
		}
		return 0.0;
	}

	auto TCFMetaReader::ReadArrSize(const QString& path) -> uint64_t {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		if (true == path.isEmpty()) {
			return 0;
		}
		auto ReadToInt = [=](H5::Group& group, const char* name, int& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = 0;
			}
		};

		try {
			H5::H5File file(path.toStdString().c_str(), H5F_ACC_RDONLY);

			int sizeX;
			int sizeY;
			int sizeZ;
			// common			
			auto groupPath = "/Data/3D";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);
				ReadToInt(group, "SizeX", sizeX);
				ReadToInt(group, "SizeY", sizeY);
				ReadToInt(group, "SizeZ", sizeZ);
				group.close();
				file.close();
				return static_cast<uint64_t>(sizeX) * static_cast<uint64_t>(sizeY) * static_cast<uint64_t>(sizeZ);
			}
			file.close();
			return 0;
		}
		catch (...) {
			return 0;
		}
	}

	auto TCFMetaReader::Read(const QString& path)->Meta::Pointer {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();
		if (true == path.isEmpty()) {
			return nullptr;
		}

		auto isLdm = false;

		auto ReadToString = [=](H5::Group& group, const char* name, QString& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = "N/A";
			}

			if (true == value.isEmpty()) {
				value = "N/A";
			}
		};

		auto ReadToDouble = [=](H5::Group& group, const char* name, double& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = 0.0;
			}
		};

		auto ReadToInt = [=](H5::Group& group, const char* name, int& value) {
			if (false == ReadAttribute(group, name, value)) {
				value = 0;
			}
		};

		auto ReadToIntDS = [=](H5::DataSet& dataset, const char* name, int& value) {
			if (false == ReadAttribute(dataset, name, value)) {
				value = 0;
			}
		};

		auto meta = std::make_shared<Meta>();
		try {
			H5::H5File file(path.toStdString().c_str(), H5F_ACC_RDONLY);

			// common
			auto groupPath = "/";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				CommonMeta info;
				ReadToString(group, "Title", info.title);
				ReadToString(group, "UniqueID", info.uniqueID);
				ReadToString(group, "DataID", info.dataID);
				ReadToString(group, "UserID", info.userID);
				ReadToString(group, "DeviceModelType", info.deviceModelType);
				ReadToString(group, "DeviceSerial", info.deviceSerial);
				ReadToString(group, "FormatVersion", info.formatVersion);
				ReadToString(group, "SoftwareVersion", info.softwareVersion);
				ReadToString(group, "CreateDate", info.createDate);
				ReadToString(group, "RecordingTime", info.recordingTime);
				ReadToString(group, "Description", info.description);

				meta->common = info;

				group.close();
			}

			auto minor_version = 0;
			if (false == meta->common.formatVersion.isEmpty()) {
				auto fv_split = meta->common.formatVersion.split(".");
				if (fv_split.count() == 3) {
					minor_version = fv_split[1].toInt();
				}
			}
			//LdmCheck /Data
			groupPath = "/Data";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);
				int val = 0;
				if (minor_version > 5) {
					ReadToInt(group, "StructureType", val);
				}
				else {
					ReadToInt(group, "isLDM", val);
				}
				if (val == 1) {
					isLdm = true;
				}
				group.close();
			}
			meta->data.isLDM = isLdm;

			// /Info/Annotation
			groupPath = "/Info/Annotation";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				AnnotationMeta info;
				ReadToString(group, "Cell Type", info.cellType);
				ReadToString(group, "TF Preset", info.tfPreset);

				meta->info.annotation = info;

				group.close();
			}

			// /Info/Device
			groupPath = "/Info/Device";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				DeviceMeta info;
				ReadToDouble(group, "Iteration", info.iteration);
				ReadToDouble(group, "Magnification", info.magnification);
				ReadToDouble(group, "NA", info.na);
				ReadToDouble(group, "RI", info.ri);
				ReadToDouble(group, "Rawsize", info.rawSize);
				ReadToDouble(group, "Wavelength", info.waveLength);
				ReadToDouble(group, "ZP", info.zp);
				ReadToDouble(group, "ZP2", info.zp2);
				ReadToDouble(group, "ZP3", info.zp3);

				meta->info.device = info;

				group.close();
			}

			// /Info/Imaging
			groupPath = "/Info/Imaging";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				ImagingMeta info;
				ReadToDouble(group, "CameraGain", info.cameraGain);
				ReadToDouble(group, "CameraShutter", info.cameraShutter);

				meta->info.imaging = info;

				group.close();
			}

			// /Info/Imaging
			groupPath = "/Info/Tile";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				TileMeta info;
				info.exist = true;

				ReadToInt(group, "Tiles", info.tiles);
				ReadToInt(group, "Tiles_H", info.tilesH);
				ReadToInt(group, "Tiles_V", info.tilesV);
				ReadToInt(group, "ColumnIndex", info.columnIndex);
				ReadToInt(group, "RowIndex", info.rowIndex);

				ReadToDouble(group, "Overlap_H", info.overlapH);
				ReadToDouble(group, "Overlap_V", info.overlapV);

				ReadToDouble(group, "C", info.c);
				ReadToDouble(group, "X", info.x);
				ReadToDouble(group, "Y", info.y);
				ReadToDouble(group, "Z", info.z);

				meta->info.tile = info;

				group.close();
			}

			// /Data/3D
			groupPath = "/Data/3D";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				Data3D info;

				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "RIMax", info.riMax);
				ReadToDouble(group, "RIMin", info.riMin);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToDouble(group, "ResolutionZ", info.resolutionZ);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);
				ReadToInt(group, "SizeZ", info.sizeZ);				
				ReadToDouble(group, "TimeInterval", info.timeInterval);

				if (true == group.exists("000000")) {
					info.exist = true;
					if (isLdm) {
						auto gg = group.openGroup("000000");
						ReadAttribute(gg, "PositionX", info.positionX);
						ReadAttribute(gg, "PositionY", info.positionY);
						ReadAttribute(gg, "PositionZ", info.positionZ);
						ReadAttribute(gg, "PositionC", info.positionC);

						ReadAttribute(gg, "TileSizeX", info.tileSizeX);
						ReadAttribute(gg, "TileSizeY", info.tileSizeY);
						ReadAttribute(gg, "TileSizeZ", info.tileSizeZ);

						if (0.0 == info.riMax && 0.0 == info.riMin) {
							ReadAttribute(gg, "RIMax", info.riMax);
							ReadAttribute(gg, "RIMin", info.riMin);
						}
						auto objName = gg.getObjnameByIdx(0);
						auto dd = gg.openDataSet(objName);
						int sampleStep;
						ReadToIntDS(dd, "SamplingStep", sampleStep);
						info.sampleStep = sampleStep;
						dd.close();
						gg.close();
					}
					else {
						H5::DataSet dataSet = group.openDataSet("000000");
						ReadAttribute(dataSet, "PositionX", info.positionX);
						ReadAttribute(dataSet, "PositionY", info.positionY);
						ReadAttribute(dataSet, "PositionZ", info.positionZ);
						ReadAttribute(dataSet, "PositionC", info.positionC);

						auto dimension = GetDimension(dataSet);
						info.sizeX = dimension.x;
						info.sizeY = dimension.y;
						info.sizeZ = dimension.z;

						if (0.0 == info.riMax && 0.0 == info.riMin) {
							ReadAttribute(dataSet, "RIMax", info.riMax);
							ReadAttribute(dataSet, "RIMin", info.riMin);
						}
						dataSet.close();
					}
					//read time points
					int tpIter = 0;
					while (true) {
						QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
						if (false == group.exists(tileName.toStdString())) {
							break;
						}
						if (isLdm) {
							auto dGroup = group.openGroup(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								ReadToInt(dGroup, "ScalarType", val);
								info.scalarType.push_back(val);
							}
							else {
								info.scalarType.push_back(0);
							}
							if (false == dGroup.attrExists("Time")) {
								info.timePoints.push_back(tpIter * info.timeInterval);
							}
							else {
								auto tmpAttr = dGroup.openAttribute("Time");
								H5::DataType dt = tmpAttr.getDataType();
								tmpAttr.close();
								if (H5::PredType::NATIVE_DOUBLE == dt) {
									double timeVal;
									ReadAttribute(dGroup, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
								else {
									int64_t timeVal;
									ReadAttribute(dGroup, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
							}

                            if (dGroup.attrExists("RIMin") == true) {
                                double riMin{};
                                ReadAttribute(dGroup, "RIMin", riMin);
                                info.riMinList.push_back(riMin);
                            }
							if(dGroup.attrExists("RIMax") == true) {
								double riMax{};
								ReadAttribute(dGroup, "RIMax", riMax);
								info.riMaxList.push_back(riMax);
							}
							dGroup.close();
						}
						else {
							H5::DataSet dSet = group.openDataSet(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								if (ReadAttribute(dSet, "ScalarType", val)) {
									info.scalarType.push_back(val);
								}
								else {
									info.scalarType.push_back(0);
								}
							}
							else {
								info.scalarType.push_back(0);
							}
							if (false == dSet.attrExists("Time")) {
								info.timePoints.push_back(tpIter * info.timeInterval);
							}
							else {
								auto tmpAttr = dSet.openAttribute("Time");
								H5::DataType dt = tmpAttr.getDataType();
								tmpAttr.close();
								if (H5::PredType::NATIVE_DOUBLE == dt) {
									double timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
								else {
									int64_t timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
							}

                            if (dSet.attrExists("RIMin") == true) {
                                double riMin{};
                                ReadAttribute(dSet, "RIMin", riMin);
                                info.riMinList.push_back(riMin);
                            }else {
								info.riMinList.push_back(info.riMin);
                            }
							if (dSet.attrExists("RIMax") == true) {
								double riMax{};
								ReadAttribute(dSet, "RIMax", riMax);
								info.riMaxList.push_back(riMax);
							}
							else {
								info.riMaxList.push_back(info.riMax);
							}
							dSet.close();
						}
						tpIter++;
					}
				}
				if (0 == info.timeInterval && info.timePoints.count() > 1) {
					info.timeInterval = info.timePoints[1] - info.timePoints[0];
				}
				meta->data.data3D = info;

				group.close();
			}

			// /Data/2D
			groupPath = "/Data/2D";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				Data2D info;

				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "MaxPhase", info.maxPhase);
				ReadToDouble(group, "MinPhase", info.minPhase);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToDouble(group, "ResolutionT", info.resolutionT);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);
				ReadToInt(group, "SizeT", info.sizeT);
				ReadToDouble(group, "TimeInterval", info.timeInterval);

				if (true == group.exists("000000")) {
					info.exist = true;

					H5::DataSet dataSet = group.openDataSet("000000");
					ReadAttribute(dataSet, "PositionX", info.positionX);
					ReadAttribute(dataSet, "PositionY", info.positionY);
					ReadAttribute(dataSet, "PositionZ", info.positionZ);
					ReadAttribute(dataSet, "PositionC", info.positionC);

					auto dimension = GetDimension(dataSet);
					info.sizeX = dimension.x;
					info.sizeY = dimension.y;
					info.sizeT = dimension.z;

					if (0.0 == info.maxPhase && 0.0 == info.minPhase) {
						ReadAttribute(dataSet, "MaxPhase", info.maxPhase);
						ReadAttribute(dataSet, "MinPhase", info.minPhase);
					}
					dataSet.close();
				}

				meta->data.data2D = info;

				group.close();
			}

			// /Data/Data2DMIP
			groupPath = "/Data/2DMIP";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				Data2DMIP info;

				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "RIMax", info.riMax);
				ReadToDouble(group, "RIMin", info.riMin);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);				
				ReadToDouble(group, "TimeInterval", info.timeInterval);

				if (true == group.exists("000000")) {
					info.exist = true;
					if (isLdm) {
						auto gg = group.openGroup("000000");
						ReadAttribute(gg, "PositionX", info.positionX);
						ReadAttribute(gg, "PositionY", info.positionY);
						ReadAttribute(gg, "PositionZ", info.positionZ);
						ReadAttribute(gg, "PositionC", info.positionC);
						ReadAttribute(gg, "TileSizeX", info.tileSizeX);
						ReadAttribute(gg, "TileSizeY", info.tileSizeY);						

						if (0.0 == info.riMax && 0.0 == info.riMin) {
							ReadAttribute(gg, "RIMax", info.riMax);
							ReadAttribute(gg, "RIMin", info.riMin);
						}
						gg.close();
					}
					else {
						H5::DataSet dataSet = group.openDataSet("000000");
						ReadAttribute(dataSet, "PositionX", info.positionX);
						ReadAttribute(dataSet, "PositionY", info.positionY);
						ReadAttribute(dataSet, "PositionZ", info.positionZ);
						ReadAttribute(dataSet, "PositionC", info.positionC);

						auto dimension = GetDimension(dataSet);
						info.sizeX = dimension.x;
						info.sizeY = dimension.y;

						if (0.0 == info.riMax && 0.0 == info.riMin) {
							ReadAttribute(dataSet, "RIMax", info.riMax);
							ReadAttribute(dataSet, "RIMin", info.riMin);
						}
						dataSet.close();
					}

					int tpIter = 0;
					while (true) {
						QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
						if (false == group.exists(tileName.toStdString())) {
							break;
						}
						if (isLdm) {
							auto dGroup = group.openGroup(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								ReadToInt(dGroup, "ScalarType", val);
								info.scalarType.push_back(val);
							}
							else {
								info.scalarType.push_back(0);
							}
							if (false == dGroup.attrExists("Time")) {
								info.timePoints.push_back(tpIter * info.timeInterval);
							}
							else {
								auto tmpAttr = dGroup.openAttribute("Time");
								H5::DataType dt = tmpAttr.getDataType();
								tmpAttr.close();
								if (H5::PredType::NATIVE_DOUBLE == dt) {
									double timeVal;
									ReadAttribute(dGroup, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
								else {
									int64_t timeVal;
									ReadAttribute(dGroup, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
							}


                            if (dGroup.attrExists("RIMin") == true) {
                                double riMin{};
                                ReadAttribute(dGroup, "RIMin", riMin);
                                info.riMinList.push_back(riMin);
                            }else {
								info.riMinList.push_back(info.riMin);
                            }
							if (dGroup.attrExists("RIMax") == true) {
								double riMax{};
								ReadAttribute(dGroup, "RIMax", riMax);
								info.riMaxList.push_back(riMax);
							}
							else {
								info.riMaxList.push_back(info.riMax);
							}
							dGroup.close();
						}
						else {
							H5::DataSet dSet = group.openDataSet(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								if (ReadAttribute(dSet, "ScalarType", val)) {
									info.scalarType.push_back(val);
								}
								else {
									info.scalarType.push_back(0);
								}
							}
							else {
								info.scalarType.push_back(0);
							}
							if (false == dSet.attrExists("Time")) {
								info.timePoints.push_back(tpIter * info.timeInterval);
							}
							else {
								auto tmpAttr = dSet.openAttribute("Time");
								H5::DataType dt = tmpAttr.getDataType();
								tmpAttr.close();
								if (H5::PredType::NATIVE_DOUBLE == dt) {
									double timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
								else {
									int64_t timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints.push_back(timeVal);
								}
							}

                            if (dSet.attrExists("RIMin") == true) {
                                double riMin{};
                                ReadAttribute(dSet, "RIMin", riMin);
                                info.riMinList.push_back(riMin);
                            }
							if (dSet.attrExists("RIMax") == true) {
								double riMax{};
								ReadAttribute(dSet, "RIMax", riMax);
								info.riMaxList.push_back(riMax);
							}
							dSet.close();
						}
						tpIter++;
					}
				}
				if (0 == info.timeInterval && info.timePoints.count() > 1) {
					info.timeInterval = info.timePoints[1] - info.timePoints[0];
				}

				meta->data.data2DMIP = info;

				group.close();
			}

			// /Data/Data3DFL
			groupPath = "/Data/3DFL";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				Data3DFL info;

				ReadToInt(group, "Channels", info.channels);
				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "MaxIntensity", info.maxIntensity);
				ReadToDouble(group, "MinIntensity", info.minIntensity);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToDouble(group, "ResolutionZ", info.resolutionZ);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);
				ReadToInt(group, "SizeZ", info.sizeZ);

				ReadToDouble(group, "TimeInterval", info.timeInterval);

				ReadToDouble(group, "OffsetZ", info.offsetZ);
				ReadToDouble(group, "OffsetZCompensation", info.offsetZCompensation);

				QStringList defaultNames{ "CH1","CH2","CH3" };
				int defaultR[3]{ 0,0,255 };
				int defaultG[3]{ 0,255,0 };
				int defaultB[3]{ 255,0,0 };

				for (auto i = 0; i < 3; i++) {
					auto channelIndex = QString("CH%1").arg(i);
					if (group.exists(channelIndex.toStdString().c_str())) {
						info.channelList.push_back(i);
					}
				}

				for (const auto& i : info.channelList) {
					auto channelIndex = QString("CH%1").arg(i);
					auto channelGroup = group.openGroup(channelIndex.toStdString().c_str());

					if (channelGroup.attrExists("Name")) {
						ReadToString(channelGroup, "Name", info.name[i]);
					}
					else {
						info.name[i] = defaultNames[i];
					}
					if (channelGroup.attrExists("ColorR")) {
						ReadToInt(channelGroup, "ColorR", info.r[i]);
					}
					else {
						info.r[i] = defaultR[i];
					}
					if (channelGroup.attrExists("ColorG")) {
						ReadToInt(channelGroup, "ColorG", info.g[i]);
					}
					else {
						info.g[i] = defaultG[i];
					}
					if (channelGroup.attrExists("ColorB")) {
						ReadToInt(channelGroup, "ColorB", info.b[i]);
					}
					else {
						info.b[i] = defaultB[i];
					}

					if (channelGroup.attrExists("Emission")) {
						ReadToDouble(channelGroup, "Emission", info.emission[i]);
					}

					if (channelGroup.attrExists("Excitation")) {
						ReadToDouble(channelGroup, "Excitation", info.excitation[i]);
					}

					if (channelGroup.attrExists("ExposureTime")) {
						ReadToInt(channelGroup, "ExposureTime", info.exposureTime[i]);
					}

					if (channelGroup.attrExists("Intensity")) {
						ReadToInt(channelGroup, "Intensity", info.intensity[i]);
					}

					if (false == channelGroup.exists("000000")) {
						channelGroup.close();
						continue;
					}

					if (isLdm) {
						auto interG = channelGroup.openGroup("000000");
						ReadAttribute(interG, "MaxIntensity", info.max[i]);
						ReadAttribute(interG, "MinIntensity", info.min[i]);
						interG.close();
					}
					else {
						auto dSet = channelGroup.openDataSet("000000");
						ReadAttribute(dSet, "MaxIntensity", info.max[i]);
						ReadAttribute(dSet, "MinIntensity", info.min[i]);
						dSet.close();
					}

					info.valid[i] = true;
					channelGroup.close();
				}

				if (0.0 == info.maxIntensity && 0.0 == info.minIntensity) {
					auto max = 0;
					auto min = 0;

					for (auto i = 0; i < 3; ++i) {
						if (info.max[i] > max) {
							max = info.max[i];
						}

						if (info.min[i] < min) {
							min = info.min[i];
						}
					}

					info.maxIntensity = max;
					info.minIntensity = min;
				}
				if (info.minIntensity < 1) {
					info.minIntensity = 0;
				}				
				for (const auto& i : info.channelList) {
					auto channelIndex = QString("CH%1").arg(i);
					auto channelGroup = group.openGroup(channelIndex.toStdString().c_str());

					if (false == channelGroup.exists("000000")) {
						channelGroup.close();
						continue;
					}

					if (isLdm) {
						H5::Group interGroup = channelGroup.openGroup("000000");
						ReadAttribute(interGroup, "PositionX", info.positionX);
						ReadAttribute(interGroup, "PositionY", info.positionY);
						ReadAttribute(interGroup, "PositionZ", info.positionZ);
						ReadAttribute(interGroup, "PositionC", info.positionC);
						ReadAttribute(interGroup, "TileSizeX", info.tileSizeX);
						ReadAttribute(interGroup, "TileSizeY", info.tileSizeY);
						ReadAttribute(interGroup, "TileSizeZ", info.tileSizeZ);
						//read time points
						int tpIter = 0;
						while (true) {
							QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
							if (false == channelGroup.exists(tileName.toStdString())) {
								break;
							}
							H5::Group dSet = channelGroup.openGroup(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								ReadToInt(dSet, "ScalarType", val);
								info.scalarType[i].push_back(val);
							}
							else {
								info.scalarType[i].push_back(0);
							}

							if (false == dSet.attrExists("Time")) {
								info.timePoints[i].push_back(tpIter * info.timeInterval);
							}
							else {
								auto tmpAttr = dSet.openAttribute("Time");
								H5::DataType dt = tmpAttr.getDataType();

								tmpAttr.close();
								if (H5::PredType::NATIVE_DOUBLE == dt) {
									double timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints[i].push_back(timeVal);
								}
								else {
									int64_t timeVal;
									ReadAttribute(dSet, "Time", timeVal);
									info.timePoints[i].push_back(timeVal);
								}
							}							
                            if (dSet.attrExists("MinIntensity") == true) {
                            	double minIntensity{};								
                                ReadAttribute(dSet, "MinIntensity", minIntensity);
                                info.minIntensityList[i].push_back(minIntensity);	
                            }else {
								info.minIntensityList[i].push_back(info.min[i]);
                            }
							if (dSet.attrExists("MaxIntensity") == true) {
								double maxIntensity{};
								ReadAttribute(dSet, "MaxIntensity", maxIntensity);
								info.maxIntensityList[i].push_back(maxIntensity);
							}
							else {
								info.maxIntensityList[i].push_back(info.max[i]);
							}
							tpIter++;
							dSet.close();
						}
						info.exist = true;
						interGroup.close();
						channelGroup.close();						
					}
					else {
						H5::DataSet dataSet = channelGroup.openDataSet("000000");
						ReadAttribute(dataSet, "PositionX", info.positionX);
						ReadAttribute(dataSet, "PositionY", info.positionY);
						ReadAttribute(dataSet, "PositionZ", info.positionZ);
						ReadAttribute(dataSet, "PositionC", info.positionC);

						//read time points
						int tpIter = 0;
						while (true) {
							QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
							if (false == channelGroup.exists(tileName.toStdString())) {
								break;
							}
							H5::DataSet dSet = channelGroup.openDataSet(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								if (ReadAttribute(dSet, "ScalarType", val)) {
									info.scalarType[i].push_back(val);
								}
								else {
									info.scalarType[i].push_back(0);
								}
							}
							else {
								info.scalarType[i].push_back(0);
							}

							if (false == dSet.attrExists("Time")) {
								info.timePoints[i].push_back(tpIter * info.timeInterval);
							}
							else {
									auto tmpAttr = dSet.openAttribute("Time");
									H5::DataType dt = tmpAttr.getDataType();
									tmpAttr.close();
									if (H5::PredType::NATIVE_DOUBLE == dt) {
										double timeVal;
										ReadAttribute(dSet, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
									else {
										int64_t timeVal;
										ReadAttribute(dSet, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
								}
							
                            if (dSet.attrExists("MinIntensity") == true) {
                                double minIntensity{};
                                ReadAttribute(dSet, "MinIntensity", minIntensity);
                                info.minIntensityList[i].push_back(minIntensity);
                            }else {
								info.minIntensityList[i].push_back(info.min[i]);
                            }
							if (dSet.attrExists("MaxIntensity") == true) {
								double maxIntensity{};
								ReadAttribute(dSet, "MaxIntensity", maxIntensity);
								info.maxIntensityList[i].push_back(maxIntensity);
							}
							else {
								info.maxIntensityList[i].push_back(info.max[i]);
							}
							dSet.close();
							tpIter++;
						}
						auto dimension = GetDimension(dataSet);
						info.sizeX = dimension.x;
						info.sizeY = dimension.y;
						info.sizeZ = dimension.z;

						info.exist = true;

						dataSet.close();
						channelGroup.close();						
					}
					if (0 == info.timeInterval && info.timePoints[i].count() > 1) {
						info.timeInterval = info.timePoints[i][1] - info.timePoints[i][0];
					}
				}							
				
				meta->data.data3DFL = info;

				group.close();
			}

			// /Data/Data2DFLMIP
			groupPath = "/Data/2DFLMIP";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				Data2DFLMIP info;

				ReadToInt(group, "Channels", info.channels);
				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "MaxIntensity", info.maxIntensity);
				ReadToDouble(group, "MinIntensity", info.minIntensity);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);				
				ReadToDouble(group, "TimeInterval", info.timeInterval);
				QStringList defaultNames{ "CH1","CH2","CH3" };
				int defaultR[3]{ 0,0,255 };
				int defaultG[3]{ 0,255,0 };
				int defaultB[3]{ 255,0,0 };

				for (auto i = 0; i < 3; i++) {
					auto channelIndex = QString("CH%1").arg(i);
					if (group.exists(channelIndex.toStdString().c_str())) {						
						info.channelList.push_back(i);
					}
				}

				for (const auto& i : info.channelList) {
					auto channelIndex = QString("CH%1").arg(i);
					auto channelGroup = group.openGroup(channelIndex.toStdString().c_str());

					if (channelGroup.attrExists("Name")) {
						ReadToString(channelGroup, "Name", info.name[i]);
					}
					else {
						info.name[i] = defaultNames[i];
					}
					if (channelGroup.attrExists("ColorR")) {
						ReadToInt(channelGroup, "ColorR", info.r[i]);
					}
					else {
						info.r[i] = defaultR[i];
					}
					if (channelGroup.attrExists("ColorG")) {
						ReadToInt(channelGroup, "ColorG", info.g[i]);
					}
					else {
						info.g[i] = defaultG[i];
					}
					if (channelGroup.attrExists("ColorB")) {
						ReadToInt(channelGroup, "ColorB", info.b[i]);
					}
					else {
						info.b[i] = defaultB[i];
					}

					if (channelGroup.attrExists("Emission")) {
						ReadToDouble(channelGroup, "Emission", info.emission[i]);
					}

					if (channelGroup.attrExists("Excitation")) {
						ReadToDouble(channelGroup, "Excitation", info.excitation[i]);
					}

					if (channelGroup.attrExists("ExposureTime")) {
						ReadToInt(channelGroup, "ExposureTime", info.exposureTime[i]);
					}

					if (channelGroup.attrExists("Intensity")) {
						ReadToInt(channelGroup, "Intensity", info.intensity[i]);
					}

					if (false == channelGroup.exists("000000")) {
						channelGroup.close();
						continue;
					}

					info.valid[i] = true;

					if (isLdm) {
						auto dGroup = channelGroup.openGroup("000000");
						ReadAttribute(dGroup, "MaxIntensity", info.max[i]);
						ReadAttribute(dGroup, "MinIntensity", info.min[i]);
						dGroup.close();
					}
					else {
						auto dSet = channelGroup.openDataSet("000000");
						ReadAttribute(dSet, "MaxIntensity", info.max[i]);
						ReadAttribute(dSet, "MinIntensity", info.min[i]);
						dSet.close();
					}
					channelGroup.close();
				}
				if (0.0 == info.maxIntensity && 0.0 == info.minIntensity) {
					auto max = 0;
					auto min = 0;

					for (auto i = 0; i < 3; ++i) {
						if (info.max[i] > max) {
							max = info.max[i];
						}

						if (info.min[i] < min) {
							min = info.min[i];
						}
					}

					info.maxIntensity = max;
					info.minIntensity = min;
				}
				
				for (const auto& i : info.channelList) {
					auto channelIndex = QString("CH%1").arg(i);
					auto channelGroup = group.openGroup(channelIndex.toStdString().c_str());

					if (false == channelGroup.exists("000000")) {
						channelGroup.close();
						continue;
					}
					if (isLdm) {
						H5::Group dataGroup = channelGroup.openGroup("000000");
						ReadAttribute(dataGroup, "PositionX", info.positionX);
						ReadAttribute(dataGroup, "PositionY", info.positionY);
						ReadAttribute(dataGroup, "PositionZ", info.positionZ);
						ReadAttribute(dataGroup, "PositionC", info.positionC);
						ReadAttribute(dataGroup, "TileSizeX", info.tileSizeX);
						ReadAttribute(dataGroup, "TileSizeY", info.tileSizeY);
						dataGroup.close();
					}
					else {
						H5::DataSet dataSet = channelGroup.openDataSet("000000");
						ReadAttribute(dataSet, "PositionX", info.positionX);
						ReadAttribute(dataSet, "PositionY", info.positionY);
						ReadAttribute(dataSet, "PositionZ", info.positionZ);
						ReadAttribute(dataSet, "PositionC", info.positionC);

						auto dimension = GetDimension(dataSet);
						info.sizeX = dimension.x;
						info.sizeY = dimension.y;
						dataSet.close();
					}
					int tpIter = 0;
					while (true) {
						QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
						if (false == channelGroup.exists(tileName.toStdString())) {
							break;
						}
						if (isLdm) {
							H5::Group dGroup = channelGroup.openGroup(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								ReadToInt(dGroup, "ScalarType", val);
								info.scalarType[i].push_back(val);
							}
							else {
								info.scalarType[i].push_back(0);
							}
							
								if (false == dGroup.attrExists("Time")) {
									info.timePoints[i].push_back(tpIter * info.timeInterval);
								}
								else {
									auto tmpAttr = dGroup.openAttribute("Time");
									H5::DataType dt = tmpAttr.getDataType();
									tmpAttr.close();
									if (H5::PredType::NATIVE_DOUBLE == dt) {
										double timeVal;
										ReadAttribute(dGroup, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
									else {
										int64_t timeVal;
										ReadAttribute(dGroup, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
								}
							
                            if (dGroup.attrExists("MinIntensity") == true) {
                                double minIntensity{};								
                                ReadAttribute(dGroup, "MinIntensity", minIntensity);
                                info.minIntensityList[i].push_back(minIntensity);								
                            }else {
								info.minIntensityList[i].push_back(info.min[i]);
                            }
							if (dGroup.attrExists("MaxIntensity") == true) {
								double maxIntensity{};
								ReadAttribute(dGroup, "MaxIntensity", maxIntensity);
								info.maxIntensityList[i].push_back(maxIntensity);
							}
							else {
								info.maxIntensityList[i].push_back(info.max[i]);
							}
							dGroup.close();
						}
						else {
							H5::DataSet dSet = channelGroup.openDataSet(tileName.toStdString());
							if (minor_version > 5) {
								int val;
								if (ReadAttribute(dSet, "ScalarType", val)) {
									info.scalarType[i].push_back(val);
								}
								else {
									info.scalarType[i].push_back(0);
								}
							}
							else {
								info.scalarType[i].push_back(0);
							}
							
								if (false == dSet.attrExists("Time")) {
									info.timePoints[i].push_back(tpIter * info.timeInterval);
								}
								else {
									auto tmpAttr = dSet.openAttribute("Time");
									H5::DataType dt = tmpAttr.getDataType();
									tmpAttr.close();
									if (H5::PredType::NATIVE_DOUBLE == dt) {
										double timeVal;
										ReadAttribute(dSet, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
									else {
										int64_t timeVal;
										ReadAttribute(dSet, "Time", timeVal);
										info.timePoints[i].push_back(timeVal);
									}
								}
							
                            if (dSet.attrExists("MinIntensity") == true) {
                                double minIntensity{};
                                ReadAttribute(dSet, "MinIntensity", minIntensity);
                                info.minIntensityList[i].push_back(minIntensity);
                            }else {
								info.minIntensityList[i].push_back(info.min[i]);
                            }
							if (dSet.attrExists("MaxIntensity") == true) {
								double maxIntensity{};
								ReadAttribute(dSet, "MaxIntensity", maxIntensity);
								info.maxIntensityList[i].push_back(maxIntensity);
							}
							else {
								info.maxIntensityList[i].push_back(info.max[i]);								
							}
							dSet.close();
						}
						tpIter++;
					}

					info.exist = true;
					channelGroup.close();					
					if (0 == info.timeInterval && info.timePoints[i].count() > 1) {
						info.timeInterval = info.timePoints[i][1] - info.timePoints[i][0];
					}
				}				
								
				if (info.minIntensity < 1) {
					info.minIntensity = 0;
				}
				meta->data.data2DFLMIP = info;

				group.close();
			}

			// /Data/DataBF
			groupPath = "/Data/BF";
			if (true == file.exists(groupPath)) {
				H5::Group group = file.openGroup(groupPath);

				DataBF info;

				ReadToInt(group, "DataCount", info.dataCount);
				ReadToDouble(group, "ResolutionX", info.resolutionX);
				ReadToDouble(group, "ResolutionY", info.resolutionY);
				ReadToInt(group, "SizeX", info.sizeX);
				ReadToInt(group, "SizeY", info.sizeY);				
				ReadToDouble(group, "TimeInterval", info.timeInterval);

				if (true == group.exists("000000")) {
					info.exist = true;
					if (isLdm) {
						auto gg = group.openGroup("000000");
						ReadAttribute(gg, "PositionX", info.positionX);
						ReadAttribute(gg, "PositionY", info.positionY);
						ReadAttribute(gg, "PositionZ", info.positionZ);
						ReadAttribute(gg, "PositionC", info.positionC);
						ReadAttribute(gg, "TileSizeX", info.tileSizeX);
						ReadAttribute(gg, "TileSizeY", info.tileSizeY);

						gg.close();
					}
					else {
						H5::DataSet dataSet = group.openDataSet("000000");
						ReadAttribute(dataSet, "PositionX", info.positionX);
						ReadAttribute(dataSet, "PositionY", info.positionY);
						ReadAttribute(dataSet, "PositionZ", info.positionZ);
						ReadAttribute(dataSet, "PositionC", info.positionC);

						auto dimension = GetDimension(dataSet);
						info.sizeX = dimension.x;
						info.sizeY = dimension.y;

						dataSet.close();
					}
				}

				int tpIter = 0;
				while (true) {
					QString tileName = QString("%1").arg(tpIter, 6, 10, QLatin1Char('0'));
					if (false == group.exists(tileName.toStdString())) {
						break;
					}
					if (isLdm) {
						auto dGroup = group.openGroup(tileName.toStdString());
						if (false == dGroup.attrExists("Time")) {
							info.timePoints.push_back(tpIter * info.timeInterval);
						}
						else {
							auto tmpAttr = dGroup.openAttribute("Time");
							H5::DataType dt = tmpAttr.getDataType();
							tmpAttr.close();
							if (H5::PredType::NATIVE_DOUBLE == dt) {
								double timeVal;
								ReadAttribute(dGroup, "Time", timeVal);
								info.timePoints.push_back(timeVal);
							}
							else {
								int64_t timeVal;
								ReadAttribute(dGroup, "Time", timeVal);
								info.timePoints.push_back(timeVal);
							}
						}
					}
					else {
						H5::DataSet dSet = group.openDataSet(tileName.toStdString());
						if (false == dSet.attrExists("Time")) {
							info.timePoints.push_back(tpIter * info.timeInterval);
						}
						else {
							auto tmpAttr = dSet.openAttribute("Time");
							H5::DataType dt = tmpAttr.getDataType();
							tmpAttr.close();
							if (H5::PredType::NATIVE_DOUBLE == dt) {
								double timeVal;
								ReadAttribute(dSet, "Time", timeVal);
								info.timePoints.push_back(timeVal);
							}
							else {
								int64_t timeVal;
								ReadAttribute(dSet, "Time", timeVal);
								info.timePoints.push_back(timeVal);
							}

						}
					}
					tpIter++;
				}
				group.close();
				if (0 == info.timeInterval && info.timePoints.count() > 1) {
					info.timeInterval = info.timePoints[1] - info.timePoints[0];
				}
				meta->data.dataBF = info;
			}

			meta->thumbnail.ht = GetThumbnailMetaData(file, "HT");
			meta->thumbnail.fl = GetThumbnailMetaData(file, "FL");
			meta->thumbnail.bf = GetThumbnailMetaData(file, "BF");

			file.close();
		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();

			return nullptr;
		}

		//calculate total time points
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		std::list<double> global_time_point;
		for (auto t : meta->data.data3D.timePoints) {			
			global_time_point.push_back(t);
		}
		auto temp_fl = 0;		
		for (auto ch = 0; ch < 3; ch++) {
			std::list<double>::iterator insertPos = global_time_point.begin();			
			for (auto s = 0; s < meta->data.data3DFL.timePoints[ch].size() && insertPos != global_time_point.end();) {
				auto lval = meta->data.data3DFL.timePoints[ch][s];
				auto gval = *insertPos;
				if (gval > lval) {
					global_time_point.insert(insertPos, lval);
					s++;
				}
				if (AreSame(gval , lval)) {
					insertPos++;
					s++;
					temp_fl = s;
				}
				else {
					insertPos++;
					temp_fl = s;
				}
			}
			for (auto ss = temp_fl; ss < meta->data.data3DFL.timePoints[ch].size(); ss++) {
				global_time_point.push_back(meta->data.data3DFL.timePoints[ch][ss]);
			}
		}		

		auto temp_bf = 0;
		std::list<double>::iterator insertPos2 = global_time_point.begin();
		for (auto s = 0; s < meta->data.dataBF.timePoints.size() && insertPos2 != global_time_point.end();) {
			auto lval = meta->data.dataBF.timePoints[s];
			auto gval = *insertPos2;
			if (gval > lval) {
				global_time_point.insert(insertPos2, lval);
				s++;
			}
			else if (gval == lval) {
				insertPos2++;
				s++;
				temp_bf = s;
			}
			else {
				insertPos2++;
				s++;
				temp_bf = s;
			}
		}

		for (auto ss = temp_bf; ss < meta->data.dataBF.timePoints.size(); ss++) {
			global_time_point.push_back(meta->data.dataBF.timePoints[ss]);
		}

		for (auto l : global_time_point) {
			meta->data.total_time.push_back(l);
		}

		return meta;
	}

	//private
	auto TCFMetaReader::ReadAttribute(H5::H5Object& object, const char* name, QString& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(H5::H5Object& object, const char* name, double& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(H5::H5Object& object, const char* name, int& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(H5::H5Object& object, const char* name, int64_t& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(const H5::H5Object& object, const char* name, QString& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(const H5::H5Object& object, const char* name, double& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(const H5::H5Object& object, const char* name, int& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(const H5::H5Object& object, const char* name, int64_t& value) const -> bool {
		if (false == object.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = object.openAttribute(name);
		const bool result = ReadAttribute(attr, value);
		attr.close();

		return result;
	}

	auto TCFMetaReader::ReadAttribute(H5::Attribute& attr, QString& value) const -> bool {
		H5std_string h5str;

		H5::DataType dt = attr.getDataType();
		attr.read(dt, h5str);

		value = QString::fromStdString(h5str);

		return true;
	}

	auto TCFMetaReader::ReadAttribute(H5::Attribute& attr, int64_t& value) const -> bool {
		H5::DataType dt = attr.getDataType();
		attr.read(dt, &value);

		return true;
	}

	auto TCFMetaReader::ReadAttribute(H5::Attribute& attr, double& value) const -> bool {
		H5::DataType dt = attr.getDataType();
		attr.read(dt, &value);

		return true;
	}

	auto TCFMetaReader::ReadAttribute(H5::Attribute& attr, int& value) const -> bool {
		H5Aread(attr.getId(), H5T_NATIVE_INT, &value);
		return true;
	}

	auto TCFMetaReader::GetDimension(const H5::DataSet& dataSet)->Dimension {
		auto dataSpace = dataSet.getSpace();

		const auto dimensionality = dataSpace.getSimpleExtentNdims();
		const std::shared_ptr<hsize_t[]> dataSetSizes(new hsize_t[dimensionality]());
		dataSpace.getSimpleExtentDims(dataSetSizes.get());

		Dimension dimension;
		if (dimensionality == 2) {
			dimension.x = dataSetSizes[1];
			dimension.y = dataSetSizes[0];
		}
		else if (dimensionality == 3) {
			dimension.x = dataSetSizes[2];
			dimension.y = dataSetSizes[1];
			dimension.z = dataSetSizes[0];
		}

		dataSpace.close();
		return dimension;
	}

	auto TCFMetaReader::GetThumbnailMetaData(const H5::H5File& file, const char* thumbnailGroupPath) const
		-> ThumbnailMetaData {
		ThumbnailMetaData thumbnailData{};

		if (false == file.exists("Thumbnail")) {
			return thumbnailData;
		}
		auto thumbnailParent = file.openGroup("Thumbnail");
		if (false == thumbnailParent.exists(thumbnailGroupPath)) {
			thumbnailParent.close();
			return thumbnailData;
		}
		const auto thumbnailGroup = thumbnailParent.openGroup(thumbnailGroupPath);
		int64_t dataCount{};
		ReadAttribute(thumbnailGroup, "DataCount", dataCount);

		if (dataCount > 0) {
			const auto thumbnailFirstDataSetName = thumbnailGroup.getObjnameByIdx(0);
			const auto thumbnailDataSet = thumbnailGroup.openDataSet(thumbnailFirstDataSetName);

			int64_t colorFlagNumber{};
			ReadAttribute(thumbnailDataSet, "IsColor", colorFlagNumber);

			double resolutionX{}, resolutionY{};
			ReadAttribute(thumbnailDataSet, "ResolutionX", resolutionX);
			ReadAttribute(thumbnailDataSet, "ResolutionY", resolutionY);

			int64_t sizeX{}, sizeY{};
			ReadAttribute(thumbnailDataSet, "SizeX", sizeX);
			ReadAttribute(thumbnailDataSet, "SizeY", sizeY);

			thumbnailData.exist = true;
			thumbnailData.dataCount = dataCount;
			thumbnailData.isColor = (colorFlagNumber == 1);
			thumbnailData.resolutionX = resolutionX;
			thumbnailData.resolutionY = resolutionY;
			thumbnailData.sizeX = sizeX;
			thumbnailData.sizeY = sizeY;
		}
		thumbnailParent.close();
		return thumbnailData;
	}
}
