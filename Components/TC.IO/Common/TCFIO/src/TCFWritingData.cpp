#include "TCFWritingData.h"

#include "DataInfo.h"
#include "TCFMetaInfo.h"

namespace TC::IO {
    struct TCFWritingData::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        /**
         * \brief TCFWritingData를 구성하는 멤버 변수
         * \member dataInfo : raw data와 그에 관련된 attribute 정보
         * \member tcfMetaInfo : TCF meta 정보
         */
        DataInfo dataInfo;
        TCFMetaInfo tcfMetaInfo;
    };

    TCFWritingData::TCFWritingData()
        : d(new Impl()){
    }

    TCFWritingData::TCFWritingData(const TCFWritingData& other)
        : d(new Impl(*other.d)) {
    }

    TCFWritingData::~TCFWritingData() = default;

    auto TCFWritingData::operator=(const TCFWritingData& other)->TCFWritingData& {
        *d = *other.d;
        return *this;
    }

    auto TCFWritingData::SetInfo(const DataInfo& dataInfo, const TCFMetaInfo& metaInfo) -> void {
        d->dataInfo = dataInfo;
        d->tcfMetaInfo = metaInfo;
    }

    auto TCFWritingData::GetDataInfo() const -> DataInfo {
        return d->dataInfo;
    }

    auto TCFWritingData::GetMetaInfo() const -> TCFMetaInfo {
        return d->tcfMetaInfo;
    }
}
