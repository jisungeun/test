#include <tuple>

#include "TCFVolumeAccessor.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include <HDF5Mutex.h>

namespace TC::IO {    
    struct TCFVolumeAccessor::Impl {
        QString path;//TCF file path
        H5::H5File file;        
    };

    
    TCFVolumeAccessor::TCFVolumeAccessor() : d{ new Impl } {
        
    }

    
    TCFVolumeAccessor::TCFVolumeAccessor(const TCFVolumeAccessor& other) : d{ new Impl } {
        *d = *(other.d);
    }

    
    TCFVolumeAccessor::~TCFVolumeAccessor() {
        
    }

    auto TCFVolumeAccessor::GetBufferAsFloat() -> std::shared_ptr<float> {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        if (isValidRegion()) {
            auto start = GetStart();
            auto size = GetSize();
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                auto cnt = std::get<0>(size) * std::get<1>(size);
                std::shared_ptr<float> rdata(new float[cnt], std::default_delete<float[]>());
                dataset.read(rdata.get(), H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                //have to free memory out size of the class
                return rdata;
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);
                offset[2] = std::get<2>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);
                count[2] = std::get<2>(size);

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);
                dimsm[2] = std::get<2>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
                auto cnt = std::get<0>(size) * std::get<1>(size) * std::get<2>(size);
                std::shared_ptr<float> rdata(new float[cnt], std::default_delete<float[]>());
                //float* rdata = new float[std::get<0>(size)* std::get<1>(size)* std::get<2>(size)];
                dataset.read(rdata.get(), H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                //have to free memory out size of the class
                return rdata;
            }
        }
        return nullptr;
    }

    auto TCFVolumeAccessor::GetBufferAsUShort()->std::shared_ptr<unsigned short> {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        if (isValidRegion()) {
            auto start = GetStart();
            auto size = GetSize();
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                //unsigned short* rdata;
                //rdata = new unsigned short[std::get<0>(size) * std::get<1>(size)];
                auto cnt = std::get<0>(size) * std::get<1>(size);
                std::shared_ptr<unsigned short> rdata(new unsigned short[cnt], std::default_delete<unsigned short[]>());
                dataset.read(rdata.get(), H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                //have to free memory out size of the class
                return rdata;
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);
                offset[2] = std::get<2>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);
                count[2] = std::get<2>(size);

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);
                dimsm[2] = std::get<2>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                //unsigned short* rdata = new unsigned short[std::get<0>(size) * std::get<1>(size) * std::get<2>(size)];
                auto cnt = std::get<0>(size) * std::get<1>(size) * std::get<2>(size);
                std::shared_ptr<unsigned short> rdata(new unsigned short[cnt], std::default_delete<unsigned short[]>());
                dataset.read(rdata.get(), H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                //have to free memory out size of the class
                return rdata;
            }
        }
        return nullptr;
    }

    auto TCFVolumeAccessor::SetBufferWithFloat(float* buf) -> void {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        if (isValidRegion()) {
            auto start = GetStart();
            auto size = GetSize();
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                dataset.write(buf, H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);
                offset[2] = std::get<2>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);
                count[2] = std::get<2>(size);

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);
                dimsm[2] = std::get<2>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                dataset.write(buf, H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            }        
        }
    }

    auto TCFVolumeAccessor::SetBufferWithUShort(unsigned short* buf) -> void {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        if (isValidRegion()) {
            auto start = GetStart();
            auto size = GetSize();
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
                                
                dataset.write(buf, H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = std::get<0>(start);
                offset[1] = std::get<1>(start);
                offset[2] = std::get<2>(start);

                count[0] = std::get<0>(size);
                count[1] = std::get<1>(size);
                count[2] = std::get<2>(size);

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = std::get<0>(size);
                dimsm[1] = std::get<1>(size);
                dimsm[2] = std::get<2>(size);

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
                
                dataset.write(buf, H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            }
        }
    }

    auto TCFVolumeAccessor::GetVoxelAsFloat(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord) -> float {
        if(isValidCoord(xCoord, yCoord, zCoord)) {
            HDF5MutexLocker locker(HDF5Mutex::GetInstance());
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());            

            auto dim = GetDimension();
            if (dim == 2) {               
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = xCoord;
                offset[1] = yCoord;                

                count[0] = 1;
                count[1] = 1;                

                stride[0] = 1;
                stride[1] = 1;                

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                float rdata[1][1];
                dataset.read(rdata, H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                return rdata[0][0];

            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = xCoord;
                offset[1] = yCoord;
                offset[2] = zCoord;

                count[0] = 1;
                count[1] = 1;
                count[2] = 1;

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;
                dimsm[2] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                float rdata[1][1][1];
                dataset.read(rdata, H5::PredType::NATIVE_FLOAT, memspace,dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                return rdata[0][0][0];
            }
        }
        return -1.0;
    }

    auto TCFVolumeAccessor::GetVoxelAsUShort(uint32_t xCoord, uint32_t yCoord, uint32_t zCoord) -> uint32_t {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        if (isValidCoord(xCoord, yCoord, zCoord)) {
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = xCoord;
                offset[1] = yCoord;

                count[0] = 1;
                count[1] = 1;

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;                

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                unsigned short rdata[1][1];
                dataset.read(rdata, H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();

                return rdata[0][0];

            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = xCoord;
                offset[1] = yCoord;
                offset[2] = zCoord;

                count[0] = 1;
                count[1] = 1;
                count[2] = 1;

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;
                dimsm[2] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                unsigned short rdata[1][1][1];
                dataset.read(rdata, H5::PredType::NATIVE_USHORT,memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();                

                return rdata[0][0][0];
            }        
        }
        return 0;
    }

    auto TCFVolumeAccessor::SetVoxelWithFloat(float val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord) -> void {
        if (isValidCoord(xCoord, yCoord, zCoord)) {
            HDF5MutexLocker locker(HDF5Mutex::GetInstance());
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = xCoord;
                offset[1] = yCoord;

                count[0] = 1;
                count[1] = 1;

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                float rdata[1][1] = {val};
                //[Be careful] will change original data 
                dataset.write(rdata, H5::PredType::NATIVE_FLOAT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = xCoord;
                offset[1] = yCoord;
                offset[2] = zCoord;

                count[0] = 1;
                count[1] = 1;
                count[2] = 1;

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;
                dimsm[2] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                float rdata[1][1][1];
                rdata[0][0][0] = val;
                //[Be careful] will change original data 
                dataset.write(rdata, H5::PredType::NATIVE_FLOAT,memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            }
        }
    }
    auto TCFVolumeAccessor::SetDimension(uint32_t xDim, uint32_t yDim, uint32_t zDim) -> void {
        SetDimensions(xDim, yDim, zDim);
    }
    auto TCFVolumeAccessor::SetROI(uint32_t xMin, uint32_t xMax, uint32_t yMin, uint32_t yMax, uint32_t zMin, uint32_t zMax) -> void {
        SetRegion(xMin, xMax, yMin, yMax, zMin, zMax);
    }

    auto TCFVolumeAccessor::SetVoxelWithUShort(uint32_t val, uint32_t xCoord, uint32_t yCoord, uint32_t zCoord) -> void {
        if (isValidCoord(xCoord, yCoord, zCoord)) {
            HDF5MutexLocker locker(HDF5Mutex::GetInstance());
            auto dsetName = GetDataSetName();
            auto dataset = d->file.openDataSet(dsetName.toStdString());

            auto dim = GetDimension();
            if (dim == 2) {
                hsize_t offset[2], count[2], stride[2], block[2];
                hsize_t dimsm[2];

                offset[0] = xCoord;
                offset[1] = yCoord;

                count[0] = 1;
                count[1] = 1;

                stride[0] = 1;
                stride[1] = 1;

                block[0] = 1;
                block[1] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                unsigned short wdata[1][1] = { val };
                //[Be careful] will change original data 
                dataset.write(wdata, H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            } else {//dim == 3
                hsize_t offset[3], count[3], stride[3], block[3];
                hsize_t dimsm[3];

                offset[0] = xCoord;
                offset[1] = yCoord;
                offset[2] = zCoord;

                count[0] = 1;
                count[1] = 1;
                count[2] = 1;

                stride[0] = 1;
                stride[1] = 1;
                stride[2] = 1;

                block[0] = 1;
                block[1] = 1;
                block[2] = 1;

                dimsm[0] = 1;
                dimsm[1] = 1;
                dimsm[2] = 1;

                H5::DataSpace memspace(dim, dimsm, NULL);

                auto dataspace = dataset.getSpace();
                dataspace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

                unsigned short wdata[1][1][1];
                wdata[0][0][0] = val;
                //[Be careful] will change original data 
                dataset.write(wdata, H5::PredType::NATIVE_USHORT, memspace, dataspace);

                dataspace.close();
                memspace.close();
                dataset.close();
            }        
        }
    }
    
    auto TCFVolumeAccessor::OpenFile() -> void {
        d->file.openFile(d->path.toStdString(), H5F_ACC_RDONLY);
    }

    
    auto TCFVolumeAccessor::CloseFile() -> void {
        d->file.close();
    }
    
    auto TCFVolumeAccessor::SetFilePath(QString path) -> void {
        d->path = path;
    }

    
    auto TCFVolumeAccessor::GetFilePath() -> QString {
        return d->path;
    }


}