#include "TCFThumbnailReader.h"

#include <QFile>
#include <H5Cpp.h>

#include <QMap>

namespace TC::TCFIO {
    const QMap<TCFThumbnailReader::Type, QString> thumbnailGroupPathMap{
    {TCFThumbnailReader::Type::HT, "/Thumbnail/HT"},
    {TCFThumbnailReader::Type::BF, "/Thumbnail/BF"},
    {TCFThumbnailReader::Type::FL, "/Thumbnail/FL"}};

    class TCFThumbnailReader::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        QString tcfFilePath{};
        Type targetType{ Type::HT };
        int32_t targetTimeFrameIndex{};

        TCFThumbnail thumbnail;

        auto GetThumbnailGroupPath()const->QString;
        auto GetDataSetName()const->QString;

        static auto ReadAttribute(const H5::H5Object& object, const std::string& name, double& value) ->bool;
        static auto ReadAttribute(const H5::H5Object& object, const std::string& name, int64_t& value) ->bool;
    };

    auto TCFThumbnailReader::Impl::GetThumbnailGroupPath()const-> QString {
        if (thumbnailGroupPathMap.contains(this->targetType)) {
            return thumbnailGroupPathMap[this->targetType];
        } 
        return "";
    }

    auto TCFThumbnailReader::Impl::GetDataSetName() const -> QString {
        return QString("%1").arg(this->targetTimeFrameIndex, 6, 10, QChar('0'));
    }

    auto TCFThumbnailReader::Impl::ReadAttribute(const H5::H5Object& object, const std::string& name,
        double& value) -> bool {
        if (!object.attrExists(name)) { return false; }

        const auto attribute = object.openAttribute(name);
        attribute.read(H5::PredType::NATIVE_DOUBLE, &value);

        return true;
    }

    auto TCFThumbnailReader::Impl::ReadAttribute(const H5::H5Object& object, const std::string& name,
        int64_t& value) -> bool {
        if (!object.attrExists(name)) { return false; }

        const auto attribute = object.openAttribute(name);
        attribute.read(H5::PredType::NATIVE_INT64, &value);

        return true;
    }

    TCFThumbnailReader::TCFThumbnailReader() : d(std::make_unique<Impl>()) {
    }

    TCFThumbnailReader::~TCFThumbnailReader() = default;

    auto TCFThumbnailReader::SetTCFFilePath(const QString& tcfFilePath) -> void {
        d->tcfFilePath = tcfFilePath;
    }

    auto TCFThumbnailReader::SetTarget(const Type& type, const int32_t& timeFrameIndex) -> void {
        d->targetType = type;
        d->targetTimeFrameIndex = timeFrameIndex;
    }

    auto TCFThumbnailReader::Read() -> bool {
        try {
            if (!QFile::exists(d->tcfFilePath)) { return false; }

            const H5::H5File file{ d->tcfFilePath.toStdString(), H5F_ACC_RDONLY };

            const auto dataGroupPath = d->GetThumbnailGroupPath();
            if (!file.exists(dataGroupPath.toStdString())) { return false; }

            const auto dataGroup = file.openGroup(dataGroupPath.toStdString());
            int64_t dataCount{};
            if (!d->ReadAttribute(dataGroup, "DataCount", dataCount)) { return false; }
            if (dataCount <= 0) { return false; }

            const auto dataSetName = d->GetDataSetName();
            if (!dataGroup.exists(dataSetName.toStdString())) { return false; }

            const auto dataSet = dataGroup.openDataSet(dataSetName.toStdString());

            int64_t isColorNumber{};
            double resolutionX{}, resolutionY{};
            int64_t sizeX{}, sizeY{}, sizeZ{};

            if (!d->ReadAttribute(dataSet, "IsColor", isColorNumber)) { return false; }
            if (!d->ReadAttribute(dataSet, "ResolutionX", resolutionX)) { return false; }
            if (!d->ReadAttribute(dataSet, "ResolutionY", resolutionY)) { return false; }
            if (!d->ReadAttribute(dataSet, "SizeX", sizeX)) { return false; }
            if (!d->ReadAttribute(dataSet, "SizeY", sizeY)) { return false; }

            const auto colorFlag = (isColorNumber == 1);

            if (colorFlag) { sizeZ = 3; }
            else { sizeZ = 1; }

            std::shared_ptr<uint8_t[]> thumbnailData{ new uint8_t[sizeX * sizeY * sizeZ]() };
            dataSet.read(thumbnailData.get(), H5::PredType::NATIVE_UINT8);

            TCFThumbnail tcfThumbnail;
            tcfThumbnail.SetColorFlag(colorFlag);
            tcfThumbnail.SetData(thumbnailData);
            tcfThumbnail.SetSize(sizeX, sizeY, sizeZ);
            tcfThumbnail.SetResolution(resolutionX, resolutionY);

            d->thumbnail = tcfThumbnail;
        } catch (const H5::Exception& ) {
            return false;
        }

        return true;
    }

    auto TCFThumbnailReader::GetThumbnail() -> TCFThumbnail {
        return d->thumbnail;
    }
}
