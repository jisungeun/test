#include <QFileInfo>

#include <HDF5Mutex.h>

#include "TCFSimpleReader.h"

namespace TC::IO {
    struct TCFSimpleReader::Impl {
        QString filePath;        
    };

    TCFSimpleReader::TCFSimpleReader(const QString& path) : d{new Impl} {
        d->filePath = path;
    }

    TCFSimpleReader::~TCFSimpleReader() {
        
    }

    auto TCFSimpleReader::SetFilePath(const QString& path) -> void {
        d->filePath = path;
    }
    auto TCFSimpleReader::FLExist(int ) const -> bool {
        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return false;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto exist = (file.exists("/Data/2DFLMIP") || file.exists("Data/3DFL"));
            file.close();
            return exist;
        }
        catch (H5::Exception ) {
            return false;
        }

    }
    auto TCFSimpleReader::BFExists() const -> bool {
        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return false;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            H5::Group group = file.openGroup("/Data/BF");
            group.close();
            file.close();
        } catch (H5::Exception ex) {
            return false;
        }

        return true;
    }

    auto TCFSimpleReader::ReadHTMIP(uint32_t index,bool readAsFloat) const -> VolumeData {
        bool hasNoRangeInformation = false;
        VolumeData volumeData;

        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return volumeData;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            H5::Group group = file.openGroup("/Data/2DMIP");
            {
                double res[2]{0, 0};
                int dim[2]{0, 0};
                double range[2]{0, 0};

                if (!ReadAttribute(group, "ResolutionX", res[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "ResolutionY", res[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeX", dim[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeY", dim[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "RIMin", range[0])) {
                    group.close();
                    file.close();
                    hasNoRangeInformation = true;
                }
                if (!ReadAttribute(group, "RIMax", range[1])) {
                    group.close();
                    file.close();
                    hasNoRangeInformation = true;
                }

                volumeData.SetDimension(dim[0], dim[1]);
                volumeData.SetResolution(res[0], res[1]);
                volumeData.SetRange(range[0], range[1]);
            }

            //group = file.openGroup("Data/2DMIP");
            {
                const auto nElements = group.getNumObjs();
                if (index >= nElements) {
                    group.close();
                    file.close();
                    return volumeData;
                }

                const auto name = QString("%1").arg(index, 6, 10, QChar('0')).toStdString();

                auto dataSet = group.openDataSet(name.c_str());

                if(readAsFloat) {
                    if (!ReadAsFloat(dataSet, volumeData, 10000.0)) {
                        dataSet.close();
                        group.close();
                        file.close();
                        return volumeData;
                    }
                }
                else {
                    if (!ReadAsUInt16(dataSet, volumeData)) {
                        dataSet.close();
                        group.close();
                        file.close();
                        return volumeData;
                    }
                }
                dataSet.close();
            }            
            group.close();
            file.close();
            if (hasNoRangeInformation) {
                if (readAsFloat) {
                    auto data = volumeData.GetDataAsFloat();
                    auto count = volumeData.GetElements();

                    float minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                    for (uint32_t idx = 0; idx < count; idx++) {
                        const auto val = data.get()[idx];
                        if (val < minVal) minVal = val;
                        else if (val > maxVal) maxVal = val;
                    }

                    volumeData.SetRange(minVal, maxVal);
                }
                else {
                    auto data = volumeData.GetDataAsUInt16();
                    auto count = volumeData.GetElements();

                    uint16_t minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                    for (uint32_t idx = 0; idx < count; idx++) {
                        const auto val = data.get()[idx];
                        if (val < minVal) minVal = val;
                        else if (val > maxVal) maxVal = val;
                    }

                    volumeData.SetRange(minVal, maxVal);
                }
            }
        } catch(H5::Exception ex) {
            return volumeData;
        }

        volumeData.SetValid(true);

        return volumeData;
    }
    auto TCFSimpleReader::ReadFLMIP(int ch, uint32_t index) const -> VolumeData {
        bool hasNoRangeInformation = false;
        VolumeData volumeData;

        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return volumeData;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto rootName = QString("/Data/2DFLMIP");
            auto chname = QString("CH%1").arg(ch);

            H5::Group root_group = file.openGroup(rootName.toStdString());
            {
                double res[2];
                int dim[2];
                if (!ReadAttribute(root_group, "ResolutionX", res[0])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "ResolutionY", res[1])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "SizeX", dim[0])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "SizeY", dim[1])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                H5::Group group = root_group.openGroup(chname.toStdString());
                {                 
                    double range[2]{0, 0};
                                     
                    if (!ReadAttribute(group, "MinIntensity", range[0])) hasNoRangeInformation = true;
                    if (!ReadAttribute(group, "MaxIntensity", range[1])) hasNoRangeInformation = true;

                    volumeData.SetDimension(dim[0], dim[1]);
                    volumeData.SetResolution(res[0], res[1]);
                    volumeData.SetRange(range[0], range[1]);
                
                    const auto nElements = group.getNumObjs();
                    if (index >= nElements) {
                        group.close();
                        root_group.close();
                        file.close();
                        return volumeData;
                    }

                    const auto name = QString("%1").arg(index, 6, 10, QChar('0')).toStdString();

                    auto dataSet = group.openDataSet(name.c_str());
                    //if (!ReadAsFloat(dataSet, volumeData, 10000.0)) return volumeData;
                    if (!ReadAsUInt16(dataSet, volumeData)) {
                        dataSet.close();
                        group.close();
                        root_group.close();
                        file.close();
                        return volumeData;
                    }
                    dataSet.close();
                }
                group.close();
                root_group.close();
                file.close();
                if (hasNoRangeInformation) {
                    //auto data = volumeData.GetDataAsFloat();
                    auto data = volumeData.GetDataAsUInt16();
                    auto count = volumeData.GetElements();

                    uint16_t minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                    for (uint32_t idx = 0; idx < count; idx++) {
                        const auto val = data.get()[idx];
                        if (val < minVal) minVal = val;
                        else if (val > maxVal) maxVal = val;
                    }

                    volumeData.SetRange(minVal, maxVal);
                }
            }
        }
        catch (H5::Exception ex) {
            return volumeData;
        }

        volumeData.SetValid(true);

        return volumeData;
    }
    auto TCFSimpleReader::ReadHT3D(uint32_t index, bool readAsFloat) const -> VolumeData {
        bool hasNoRangeInformation = false;
        VolumeData volumeData;

        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return volumeData;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            H5::Group group = file.openGroup("/Data/3D");
            {
                double res[3];
                int dim[3];
                double range[2];

                if (!ReadAttribute(group, "ResolutionX", res[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "ResolutionY", res[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "ResolutionZ", res[2])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeX", dim[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeY", dim[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeZ", dim[2])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "RIMin", range[0])) hasNoRangeInformation = true;
                if (!ReadAttribute(group, "RIMax", range[1])) hasNoRangeInformation = true;

                volumeData.SetDimension(dim[0], dim[1], dim[2]);
                volumeData.SetResolution(res[0], res[1], res[2]);                
                volumeData.SetRange(range[0], range[1]);
            }

            //group = file.openGroup("Data/3D");
            {                
                const auto nElements = group.getNumObjs();                
                if (index >= nElements) {
                    group.close();
                    file.close();
                    return volumeData;
                }

                const auto name = QString("%1").arg(index, 6, 10, QChar('0')).toStdString();

                auto dataSet = group.openDataSet(name.c_str());

                double drange[2];
                if (ReadAttribute(dataSet,"RIMin",drange[0])&&ReadAttribute(dataSet,"RIMax",drange[1])) {                    
                    volumeData.SetRange(drange[0], drange[1]);
                }

                auto type = dataSet.getDataType();                
                if (readAsFloat) {
                    if (!ReadAsFloat(dataSet, volumeData, 10000.0)) {
                        type.close();
                        dataSet.close();
                        group.close();
                        file.close();
                        return volumeData;
                    }
                } else {
                    if (type == H5::PredType::NATIVE_DOUBLE) {
                        if (!ReadAsFloat(dataSet, volumeData, 10000.0)) {
                            type.close();
                            dataSet.close();
                            group.close();
                            file.close();
                            return volumeData;
                        }
                    }
                    else if (type == H5::PredType::NATIVE_USHORT) {
                        if (!ReadAsUInt16(dataSet, volumeData)) {
                            type.close();
                            dataSet.close();
                            group.close();
                            file.close();
                            return volumeData;
                        }
                    }
                    else {
                        if (!ReadAsFloat(dataSet, volumeData, 10000.0)) {
                            type.close();
                            dataSet.close();
                            group.close();
                            file.close();
                            return volumeData;
                        }
                    }
                }
                type.close();
                dataSet.close();
                group.close();
                file.close();
            }

            if(hasNoRangeInformation && (volumeData.GetDataType()==VolumeData::Type::FloatType)) {
                auto data = volumeData.GetDataAsFloat();
                auto count = volumeData.GetElements();

                double minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                for(uint32_t idx=0; idx<count; idx++) {
                    const auto val = data.get()[idx];
                    if (val < minVal) minVal = val;
                    else if (val > maxVal) maxVal = val;
                }

                volumeData.SetRange(minVal, maxVal);
            } else if (hasNoRangeInformation && (volumeData.GetDataType() == VolumeData::Type::UInt16Type)) {
                auto data = volumeData.GetDataAsUInt16();
                auto count = volumeData.GetElements();

                uint16_t minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                for (uint32_t idx = 0; idx < count; idx++) {
                    const auto val = data.get()[idx];
                    if (val < minVal) minVal = val;
                    else if (val > maxVal) maxVal = val;
                }
                volumeData.SetRange(minVal/10000.0, maxVal/10000.0);
            }
        }
        catch (H5::Exception ex) {
            return volumeData;
        }

        volumeData.SetValid(true);

        return volumeData;
    }

    auto TCFSimpleReader::ReadFL3D(int ch, uint32_t index) const -> VolumeData {        
        bool hasNoRangeInformation = false;
        VolumeData volumeData;

        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return volumeData;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            auto chname = QString("CH%1").arg(ch);

            H5::Group root_group = file.openGroup("/Data/3DFL");
            {
                double res[3];
                int dim[3];
                if (!ReadAttribute(root_group, "ResolutionX", res[0])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "ResolutionY", res[1])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "ResolutionZ", res[2])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "SizeX", dim[0])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "SizeY", dim[1])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(root_group, "SizeZ", dim[2])) {
                    root_group.close();
                    file.close();
                    return volumeData;
                }
                H5::Group group = root_group.openGroup(chname.toStdString());
                {
                    double range[2];

                    if (!ReadAttribute(group, "MinIntensity", range[0])) hasNoRangeInformation = true;
                    if (!ReadAttribute(group, "MaxIntensity", range[1])) hasNoRangeInformation = true;                                        
                    
                    volumeData.SetDimension(dim[0], dim[1], dim[2]);
                    volumeData.SetResolution(res[0], res[1], res[2]);
                    volumeData.SetRange(range[0], range[1]);

                    const auto nElements = group.getNumObjs();
                    if (index >= nElements) {
                        group.close();
                        root_group.close();
                        file.close();
                        return volumeData;
                    }

                    const auto name = QString("%1").arg(index, 6, 10, QChar('0')).toStdString();

                    auto dataSet = group.openDataSet(name.c_str());
                    auto type = dataSet.getDataType();


                    if (type == H5::PredType::NATIVE_DOUBLE) {
                        if (!ReadAsFloat(dataSet, volumeData, 10000.0)) {                            
                            dataSet.close();
                            type.close();                            
                            group.close();
                            root_group.close();
                            file.close();
                            return volumeData;
                        }
                    }
                    else if (type == H5::PredType::NATIVE_USHORT) {
                        if (!ReadAsUInt16(dataSet, volumeData)) {
                            dataSet.close();
                            type.close();
                            group.close();
                            root_group.close();
                            file.close();
                            return volumeData;
                        }
                    }
                    dataSet.close();
                    type.close();
                }
                group.close();
                root_group.close();
                file.close();
                if (hasNoRangeInformation && (volumeData.GetDataType() == VolumeData::Type::FloatType)) {
                    auto data = volumeData.GetDataAsFloat();
                    auto count = volumeData.GetElements();

                    double minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                    for (uint32_t idx = 0; idx < count; idx++) {
                        const auto val = data.get()[idx];
                        if (val < minVal) minVal = val;
                        else if (val > maxVal) maxVal = val;
                    }

                    volumeData.SetRange(minVal, maxVal);
                }
                else if (hasNoRangeInformation && (volumeData.GetDataType() == VolumeData::Type::UInt16Type)) {
                    auto data = volumeData.GetDataAsUInt16();
                    auto count = volumeData.GetElements();

                    uint16_t minVal{ data.get()[0] }, maxVal{ data.get()[0] };

                    for (uint32_t idx = 0; idx < count; idx++) {
                        const auto val = data.get()[idx];
                        if (val < minVal) minVal = val;
                        else if (val > maxVal) maxVal = val;
                    }

                    volumeData.SetRange(minVal / 10000.0, maxVal / 10000.0);
                }
            }
        }
        catch (H5::Exception ex) {
            return volumeData;
        }

        volumeData.SetValid(true);

        return volumeData;
    }

    auto TCFSimpleReader::ReadBF(uint32_t index) const -> VolumeData {
        VolumeData volumeData;

        QFileInfo fileInfo(d->filePath);
        if (!fileInfo.exists() || fileInfo.isDir()) return volumeData;

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();

        try {
            H5::H5File file(d->filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            H5::Group group = file.openGroup("/Data/BF");
            {
                double res[2];
                int dim[2];
                double range[2];

                if (!ReadAttribute(group, "ResolutionX", res[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "ResolutionY", res[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeX", dim[0])) {
                    group.close();
                    file.close();
                    return volumeData;
                }
                if (!ReadAttribute(group, "SizeY", dim[1])) {
                    group.close();
                    file.close();
                    return volumeData;
                }

                volumeData.SetDimension(dim[0], dim[1]);
                volumeData.SetResolution(res[0], res[1]);
                volumeData.SetRange(range[0], range[1]);
            }
            
            //group = file.openGroup("Data/BF");
            {
                const auto nElements = group.getNumObjs();
                if (index >= nElements) {
                    group.close();
                    file.close();
                    return volumeData;
                }

                const auto name = QString("%1").arg(index, 6, 10, QChar('0')).toStdString();

                auto dataSet = group.openDataSet(name.c_str());
                if (!ReadAsRBG32(dataSet, volumeData)) {
                    dataSet.close();
                    group.close();
                    file.close();
                    return volumeData;
                }
                dataSet.close();
                group.close();
                file.close();
            }
        }
        catch (H5::Exception ex) {
            return volumeData;
        }

        volumeData.SetValid(true);

        return volumeData;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Group& group, const char* name, QString& strString) const -> bool {
        if (!group.attrExists(name)) return false;
        H5::Attribute attr = group.openAttribute(name);
        const bool bRet = ReadAttribute(attr, strString);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Group& group, const char* name, double& dValue) const -> bool {
        if (!group.attrExists(name)) return false;
        H5::Attribute attr = group.openAttribute(name);
        const bool bRet = ReadAttribute(attr, dValue);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Group& group, const char* name, int& nValue) const -> bool {
        if (!group.attrExists(name)) return false;
        H5::Attribute attr = group.openAttribute(name);
        const bool bRet = ReadAttribute(attr, nValue);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::DataSet& dataset, const char* name, QString& strString) const -> bool {
        if (!dataset.attrExists(name)) return false;
        H5::Attribute attr = dataset.openAttribute(name);
        const bool bRet = ReadAttribute(attr, strString);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::DataSet& dataset, const char* name, double& dValue) const -> bool {
        if (!dataset.attrExists(name)) return false;
        H5::Attribute attr = dataset.openAttribute(name);
        const bool bRet = ReadAttribute(attr, dValue);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::DataSet& dataset, const char* name, int& nValue) const -> bool {
        if (!dataset.attrExists(name)) return false;
        H5::Attribute attr = dataset.openAttribute(name);
        const bool bRet = ReadAttribute(attr, nValue);
        attr.close();
        return bRet;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Attribute& attr, QString& strString) const -> bool {
        H5std_string h5str;

        H5::DataType dt = attr.getDataType();
        attr.read(dt, h5str);

        strString = QString::fromStdString(h5str);

        dt.close();

        return true;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Attribute& attr, double& dValue) const -> bool {
        H5::DataType dt = attr.getDataType();
        attr.read(dt, &dValue);
        dt.close();

        return true;
    }

    auto TCFSimpleReader::ReadAttribute(H5::Attribute& attr, int& nValue) const -> bool {
        //TODO Need to fix TCFWriter::WriteAttributeInteger, but need to support old version TCF files....
        H5Aread(attr.getId(), H5T_NATIVE_INT, &nValue);
        return true;
    }

    auto TCFSimpleReader::ReadAsFloat(H5::DataSet& dataSet, VolumeData& volumeData, const double numerator) const -> bool {
        try {
            auto dataSpace = dataSet.getSpace();
            auto dataType = dataSet.getDataType();

            const auto dimensionality = dataSpace.getSimpleExtentNdims();
            hsize_t* dims_out = new hsize_t[dimensionality];
            dataSpace.getSimpleExtentDims(dims_out, NULL);
            size_t elements = 1;
            for(size_t i=0; i<dimensionality; i++) {
                elements = elements * dims_out[i];
            }

            auto rawdata = volumeData.AllocateAsDouble(elements);

            if (dataType == H5::PredType::NATIVE_USHORT) {
                unsigned short* pTemp = new unsigned short[elements];
                dataSet.read(pTemp, H5::PredType::NATIVE_USHORT);                

                for (size_t kk = 0; kk < elements; kk++) rawdata.get()[kk] = pTemp[kk] / numerator;
                delete[] pTemp;
            } else {
                dataSet.read(rawdata.get(), H5::PredType::NATIVE_DOUBLE);
            }
            dataSpace.close();
            dataType.close();
        } catch(H5::Exception /*ex*/) {
            return false;
        }

        return true;
    }

    auto TCFSimpleReader::ReadAsUInt16(H5::DataSet& dataSet, VolumeData& volumeData) const -> bool {
        try {
            auto dataSpace = dataSet.getSpace();
            auto dataType = dataSet.getDataType();

            const auto dimensionality = dataSpace.getSimpleExtentNdims();
            hsize_t* dims_out = new hsize_t[dimensionality];
            dataSpace.getSimpleExtentDims(dims_out, NULL);
            size_t elements = 1;
            for (size_t i = 0; i < dimensionality; i++) {
                elements = elements * dims_out[i];                
            }            
            auto rawdata = volumeData.AllocateAsUInt16(elements);
            dataSet.read(rawdata.get(), H5::PredType::NATIVE_USHORT);
            dataSpace.close();
            dataType.close();
        }
        catch (H5::Exception /*ex*/) {
            return false;
        }

        return true;
    }

    auto TCFSimpleReader::ReadAsRBG32(H5::DataSet& dataSet, VolumeData& volumeData) const -> bool {
        try {
            auto dataSpace = dataSet.getSpace();
            auto dataType = dataSet.getDataType();

            const auto dimensionality = dataSpace.getSimpleExtentNdims();
            hsize_t* dims_out = new hsize_t[dimensionality];
            dataSpace.getSimpleExtentDims(dims_out, NULL);
            size_t elements = 1;
            for (size_t i = 0; i < dimensionality; i++) {
                elements = elements * dims_out[i];
            }
            const uint32_t color_dims = volumeData.GetElements();
            if (elements != (color_dims * 3)) return false;

            auto rawdata = volumeData.AllocateAsUInt32(color_dims);

            if (dataType == H5::PredType::NATIVE_UCHAR) {
                auto pTemp = new unsigned char[elements];
                dataSet.read(pTemp, H5::PredType::NATIVE_UCHAR);

                for (size_t kk = 0; kk < color_dims; kk++) {
                    const auto rv = pTemp[(color_dims * 0) + kk];
                    const auto gv = pTemp[(color_dims * 1) + kk];
                    const auto bv = pTemp[(color_dims * 2) + kk];

                    const auto r = static_cast<uint32_t>(rv);
                    const auto g = static_cast<uint32_t>(gv);
                    const auto b = static_cast<uint32_t>(bv);

                    const uint32_t color = 0xFF000000 + (0x00FF0000 & (r << 16)) + (0x0000FF00 & (g << 8)) + (0x000000FF & (b));
                    rawdata.get()[kk] = color;
                }
                delete[] pTemp;
                dataSpace.close();
                dataType.close();
            } else {
                dataSpace.close();
                dataType.close();
                return false;
            }            
        }
        catch (H5::Exception /*ex*/) {
            return false;
        }

        return true;
    }
}
