#include "TCFMetaInfo.h"

namespace TC::IO {
    struct TCFMetaInfo::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        /**
         * \brief TCFMetaInfo 인스턴스를 구성하는 멤버 변수
         * \member common : TCF 일반 내용
         * \member annotation : TCF annotation 관련 내용
         * \member device : TCF device 관련 내용
         * \member imaging : TCF 이미징 관련 내용
         * \member tile : TCF tile 관련 내용
         * \member isTileData : tile인지 구분하기 위한 멤버변수
         */
        Common common;
        Annotation annotation;
        Device device;
        Imaging imaging;
        Tile tile;

        bool isTileData{ false };
    };

    TCFMetaInfo::TCFMetaInfo()
        : d(new Impl()) {
    }

    TCFMetaInfo::TCFMetaInfo(const TCFMetaInfo& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    TCFMetaInfo::~TCFMetaInfo() = default;

    auto TCFMetaInfo::operator=(const TCFMetaInfo& other) -> TCFMetaInfo {
        d = std::make_unique<Impl>(*other.d);
        return *this;
    }

    auto TCFMetaInfo::SetCommon(const Common& common) -> void {
        d->common = common;
    }

    auto TCFMetaInfo::SetAnnotation(const Annotation& annotation) -> void {
        d->annotation = annotation;
    }

    auto TCFMetaInfo::SetDevice(const Device& device) -> void {
        d->device = device;
    }

    auto TCFMetaInfo::SetImaging(const Imaging& imaging) -> void {
        d->imaging = imaging;
    }

    auto TCFMetaInfo::SetTile(const Tile& tile) -> void {
        d->tile = tile;
        d->isTileData = true;
    }

    auto TCFMetaInfo::IsTileIncluded() const-> bool {
        return d->isTileData;
    }

    auto TCFMetaInfo::GetCommon() const -> Common {
        return d->common;
    }

    auto TCFMetaInfo::GetAnnotation() const -> Annotation {
        return d->annotation;
    }

    auto TCFMetaInfo::GetDevice() const -> Device {
        return d->device;
    }

    auto TCFMetaInfo::GetImaging() const -> Imaging {
        return d->imaging;
    }

    auto TCFMetaInfo::GetTile() const -> Tile {
        return d->tile;
    }
}
