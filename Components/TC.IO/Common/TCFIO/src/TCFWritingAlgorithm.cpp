#define LOGGER_TAG "TCTCFIO::TCFWritingAlgorithm"

#include "TCFWritingAlgorithm.h"

#include <iostream>
#include <QFile>

#include "TCLogger.h"
#include "HDF5Mutex.h"

namespace TC::IO {
    struct TCFWritingAlgorithm::Impl {
        Impl() = default;
        ~Impl() = default;

        H5::H5File file;
        QString tcfFormatVersion = "1.3.6";
    };

    TCFWritingAlgorithm::TCFWritingAlgorithm()
        : d(new Impl()) {
    }

    TCFWritingAlgorithm::~TCFWritingAlgorithm() {
    }

    auto TCFWritingAlgorithm::Write(const QString& tcfPath, const TCFWritingData& data) -> void {
        HDF5MutexLocker locker(HDF5Mutex::GetInstance());
        try {
            if (FileExists(tcfPath)) {
                d->file = H5::H5File(tcfPath.toLocal8Bit().constData(), H5F_ACC_RDWR);
            } else {
                d->file = H5::H5File(tcfPath.toLocal8Bit().constData(), H5F_ACC_TRUNC);
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
            return;
        }

        WriteMetaInfo(data.GetMetaInfo());
        WriteDataInfo(data.GetDataInfo());

        d->file.close();
    }

    auto TCFWritingAlgorithm::FileExists(const QString& tcfPath) -> bool {
        return QFile::exists(tcfPath);
    }

    auto TCFWritingAlgorithm::WriteMetaInfo(const TCFMetaInfo& metaInfo) const->void {
        WriteCommonAttributes(metaInfo);
        WriteAnnotationAttributes(metaInfo);
        WriteDeviceAttributes(metaInfo);
        WriteImagingAttributes(metaInfo);

        if (metaInfo.IsTileIncluded()) {
            WriteTileAttributes(metaInfo);
        }
    }

    auto TCFWritingAlgorithm::WriteDataInfo(const DataInfo& dataInfo) const -> void {
        const auto imageType = dataInfo.GetImageType();
        try {
            if (imageType._value == ImageType::HT) {
                WriteHt(dataInfo);
            } else if (imageType._value == ImageType::FLBLUE) {
                WriteFlBlue(dataInfo);
            } else if (imageType._value == ImageType::FLGREEN) {
                WriteFlGreen(dataInfo);
            } else if (imageType._value == ImageType::FLRED) {
                WriteFlRed(dataInfo);
            } else if (imageType._value == ImageType::PHASE) {
                WritePhase(dataInfo);
            } else if (imageType._value == ImageType::BF) {
                WriteBf(dataInfo);
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
    }

    auto TCFWritingAlgorithm::WriteHt(const DataInfo& dataInfo) const -> void {
        auto dataGroup = OpenOrCreateGroupIfNotExists(d->file, "Data");
        {
            auto group3D = OpenOrCreateGroupIfNotExists(dataGroup, "3D");

            auto currentRiMin{ DBL_MAX }, currentRiMax{ -DBL_MAX };
            if (group3D.attrExists("RIMax")) {
                auto riMaxAttribute = group3D.openAttribute("RIMax");
                riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentRiMax);
                riMaxAttribute.close();

                auto riMinAttribute = group3D.openAttribute("RIMin");
                riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentRiMin);
                riMinAttribute.close();
            }

            WriteAttributeInt64(group3D, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(group3D, "RIMax",
                std::max(dataInfo.GetMinMax3d().max, currentRiMax));
            WriteAttributeDouble(group3D, "RIMin",
                std::min(dataInfo.GetMinMax3d().min, currentRiMin));
            WriteAttributeDouble(group3D, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(group3D, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeDouble(group3D, "ResolutionZ", dataInfo.GetResolutions().resolutionZ);
            WriteAttributeInt64(group3D, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(group3D, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeInt64(group3D, "SizeZ", dataInfo.GetSizes().sizeZ);
            WriteAttributeDouble(group3D, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto dataSet3D =
                OpenOrCreateDataSet3dIfNotExists(group3D, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());


            const auto data3dTypeName = std::string(dataInfo.GetRawData().data3d.type().name());

            if (data3dTypeName == "class std::shared_ptr<unsigned short [0]>") {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint16_t[]>>();
                dataSet3D.write(rawData3D.get(), H5::PredType::NATIVE_UINT16);
            } else {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint16_t>>();
                dataSet3D.write(rawData3D.get(), H5::PredType::NATIVE_UINT16);
            }



            WriteAttributeDouble(dataSet3D, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSet3D, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSet3D, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSet3D, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeDouble(dataSet3D, "RIMax", dataInfo.GetMinMax3d().max);
            WriteAttributeDouble(dataSet3D, "RIMin", dataInfo.GetMinMax3d().min);
            WriteAttributeString(dataSet3D, "RecordingTime", dataInfo.GetRecordingTime());

            dataSet3D.close();
            group3D.close();
        }

        {
            auto groupMip = OpenOrCreateGroupIfNotExists(dataGroup, "2DMIP");
            auto currentRiMin{ DBL_MAX }, currentRiMax{ -DBL_MAX };
            if (dataGroup.attrExists("RIMax")) {
                auto riMaxAttribute = groupMip.openAttribute("RIMax");
                riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentRiMax);
                riMaxAttribute.close();

                auto riMinAttribute = groupMip.openAttribute("RIMin");
                riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentRiMin);
                riMinAttribute.close();
            }

            WriteAttributeInt64(groupMip, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(groupMip, "RIMax",
                std::max(dataInfo.GetMinMaxMip().max, currentRiMax));
            WriteAttributeDouble(groupMip, "RIMin",
                std::min(dataInfo.GetMinMaxMip().min, currentRiMin));
            WriteAttributeDouble(groupMip, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(groupMip, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeInt64(groupMip, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(groupMip, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeDouble(groupMip, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto dataSetMip =
                OpenOrCreateDataSetMipIfNotExists(groupMip, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());

            const auto dataMipTypeName = std::string(dataInfo.GetRawData().dataMip.type().name());

            if (dataMipTypeName == "class std::shared_ptr<unsigned short [0]>") {
                const auto rawDataMip =
                    *dataInfo.GetRawData().dataMip._Cast<std::shared_ptr<uint16_t[]>>();
                dataSetMip.write(rawDataMip.get(), H5::PredType::NATIVE_UINT16);
            } else {
                const auto rawDataMip =
                    *dataInfo.GetRawData().dataMip._Cast<std::shared_ptr<uint16_t>>();
                dataSetMip.write(rawDataMip.get(), H5::PredType::NATIVE_UINT16);
            }

            WriteAttributeDouble(dataSetMip, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSetMip, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSetMip, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSetMip, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeDouble(dataSetMip, "RIMax", dataInfo.GetMinMaxMip().max);
            WriteAttributeDouble(dataSetMip, "RIMin", dataInfo.GetMinMaxMip().min);
            WriteAttributeString(dataSetMip, "RecordingTime", dataInfo.GetRecordingTime());

            dataSetMip.close();
            groupMip.close();
        }

        dataGroup.close();
    }

    auto TCFWritingAlgorithm::WriteFl(const DataInfo& dataInfo, const uint32_t& channelIndex) const -> void {
        const std::string channelString = QString("CH%1").arg(channelIndex).toLocal8Bit().constData();

        auto dataGroup = OpenOrCreateGroupIfNotExists(d->file, "Data");
        {
            auto group3dFl = OpenOrCreateGroupIfNotExists(dataGroup, "3DFL");

            auto currentMinIntensity{ DBL_MAX }, currentMaxIntensity{ -DBL_MAX };
            if (group3dFl.attrExists("MaxIntensity")) {
                auto riMaxAttribute = group3dFl.openAttribute("MaxIntensity");
                riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMaxIntensity);
                riMaxAttribute.close();

                auto riMinAttribute = group3dFl.openAttribute("MinIntensity");
                riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMinIntensity);
                riMinAttribute.close();
            }

            WriteAttributeInt64(group3dFl, "Channels", dataInfo.GetChannels());
            WriteAttributeInt64(group3dFl, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(group3dFl, "MaxIntensity",
                std::max(dataInfo.GetMinMax3d().max, currentMaxIntensity));
            WriteAttributeDouble(group3dFl, "MinIntensity",
                std::min(dataInfo.GetMinMax3d().min, currentMinIntensity));
            WriteAttributeDouble(group3dFl, "OffsetZ", dataInfo.GetOffsetZs().offsetZ);
            WriteAttributeDouble(group3dFl, "OffsetZCompensation", dataInfo.GetOffsetZs().offsetZCompensation);
            WriteAttributeDouble(group3dFl, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(group3dFl, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeDouble(group3dFl, "ResolutionZ", dataInfo.GetResolutions().resolutionZ);
            WriteAttributeInt64(group3dFl, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(group3dFl, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeInt64(group3dFl, "SizeZ", dataInfo.GetSizes().sizeZ);
            WriteAttributeDouble(group3dFl, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto channelGroup = OpenOrCreateGroupIfNotExists(group3dFl, channelString);

            auto dataSet3dFl =
                OpenOrCreateDataSet3dIfNotExists(channelGroup, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());

            const auto data3dTypeName = std::string(dataInfo.GetRawData().data3d.type().name());

            if (data3dTypeName == "class std::shared_ptr<unsigned short [0]>") {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint16_t[]>>();
                dataSet3dFl.write(rawData3D.get(), H5::PredType::NATIVE_UINT16);
            } else {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint16_t>>();
                dataSet3dFl.write(rawData3D.get(), H5::PredType::NATIVE_UINT16);
            }

            WriteAttributeDouble(dataSet3dFl, "MaxIntensity", dataInfo.GetMinMax3d().max);
            WriteAttributeDouble(dataSet3dFl, "MinIntensity", dataInfo.GetMinMax3d().min);
            WriteAttributeDouble(dataSet3dFl, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSet3dFl, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSet3dFl, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSet3dFl, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeString(dataSet3dFl, "RecordingTime", dataInfo.GetRecordingTime());

            dataSet3dFl.close();
            channelGroup.close();
            group3dFl.close();
        }

        {
            auto groupFlMip = OpenOrCreateGroupIfNotExists(dataGroup, "2DFLMIP");
            auto currentMinIntensity{ DBL_MAX }, currentMaxIntensity{ -DBL_MAX };
            if (groupFlMip.attrExists("MaxIntensity")) {
                auto riMaxAttribute = groupFlMip.openAttribute("MaxIntensity");
                riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMaxIntensity);
                riMaxAttribute.close();

                auto riMinAttribute = groupFlMip.openAttribute("MinIntensity");
                riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMinIntensity);
                riMinAttribute.close();
            }

            WriteAttributeInt64(groupFlMip, "Channels", dataInfo.GetChannels());
            WriteAttributeInt64(groupFlMip, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(groupFlMip, "MaxIntensity",
                std::max(dataInfo.GetMinMaxMip().max, currentMaxIntensity));
            WriteAttributeDouble(groupFlMip, "MinIntensity",
                std::min(dataInfo.GetMinMaxMip().min, currentMinIntensity));
            WriteAttributeDouble(groupFlMip, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(groupFlMip, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeInt64(groupFlMip, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(groupFlMip, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeDouble(groupFlMip, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto channelGroup = OpenOrCreateGroupIfNotExists(groupFlMip, channelString);
            auto dataSetMip =
                OpenOrCreateDataSetMipIfNotExists(channelGroup, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());

            const auto dataMipTypeName = std::string(dataInfo.GetRawData().dataMip.type().name());

            if (dataMipTypeName == "class std::shared_ptr<unsigned short [0]>") {
                const auto rawDataMip =
                    *dataInfo.GetRawData().dataMip._Cast<std::shared_ptr<uint16_t[]>>();
                dataSetMip.write(rawDataMip.get(), H5::PredType::NATIVE_UINT16);
            } else {
                const auto rawDataMip =
                    *dataInfo.GetRawData().dataMip._Cast<std::shared_ptr<uint16_t>>();
                dataSetMip.write(rawDataMip.get(), H5::PredType::NATIVE_UINT16);
            }

            WriteAttributeDouble(dataSetMip, "MaxIntensity", dataInfo.GetMinMaxMip().max);
            WriteAttributeDouble(dataSetMip, "MinIntensity", dataInfo.GetMinMaxMip().min);
            WriteAttributeDouble(dataSetMip, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSetMip, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSetMip, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSetMip, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeString(dataSetMip, "RecordingTime", dataInfo.GetRecordingTime());

            dataSetMip.close();
            channelGroup.close();
            groupFlMip.close();
        }

        dataGroup.close();
    }

    auto TCFWritingAlgorithm::WriteFlBlue(const DataInfo& dataInfo) const -> void {
        WriteFl(dataInfo, 0);
    }

    auto TCFWritingAlgorithm::WriteFlGreen(const DataInfo& dataInfo) const -> void {
        WriteFl(dataInfo, 1);
    }

    auto TCFWritingAlgorithm::WriteFlRed(const DataInfo& dataInfo) const -> void {
        WriteFl(dataInfo, 2);
    }

    auto TCFWritingAlgorithm::WritePhase(const DataInfo& dataInfo) const -> void {
        auto dataGroup = OpenOrCreateGroupIfNotExists(d->file, "Data");
        {
            auto group2D = OpenOrCreateGroupIfNotExists(dataGroup, "2D");

            auto currentMinPhase{ DBL_MAX }, currentMaxPhase{ -DBL_MAX };
            if (dataGroup.attrExists("MaxPhase")) {
                auto riMaxAttribute = group2D.openAttribute("MaxPhase");
                riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMaxPhase);
                riMaxAttribute.close();

                auto riMinAttribute = group2D.openAttribute("MinPhase");
                riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &currentMinPhase);
                riMinAttribute.close();
            }

            WriteAttributeInt64(group2D, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(group2D, "MaxPhase",
                std::max(dataInfo.GetMinMax3d().max, currentMaxPhase));
            WriteAttributeDouble(group2D, "MinPhase",
                std::min(dataInfo.GetMinMax3d().min, currentMinPhase));
            WriteAttributeDouble(group2D, "ResolutionT", dataInfo.GetResolutions().resolutionT);
            WriteAttributeDouble(group2D, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(group2D, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeInt64(group2D, "SizeT", dataInfo.GetSizes().sizeT);
            WriteAttributeInt64(group2D, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(group2D, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeDouble(group2D, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto dataSet2D =
                OpenOrCreateDataSetPhaseIfNotExists(group2D, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());

            const auto data2dTypeName = std::string(dataInfo.GetRawData().data3d.type().name());

            if (data2dTypeName == "class std::shared_ptr<float [0]>") {
                const auto rawData2D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<float[]>>();
                dataSet2D.write(rawData2D.get(), H5::PredType::NATIVE_FLOAT);
            } else {
                const auto rawData2D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<float>>();
                dataSet2D.write(rawData2D.get(), H5::PredType::NATIVE_FLOAT);
            }

            WriteAttributeDouble(dataSet2D, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSet2D, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSet2D, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSet2D, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeDouble(dataSet2D, "MaxPhase", dataInfo.GetMinMax3d().max);
            WriteAttributeDouble(dataSet2D, "MinPhase", dataInfo.GetMinMax3d().min);
            WriteAttributeString(dataSet2D, "RecordingTime", dataInfo.GetRecordingTime());

            dataSet2D.close();
            group2D.close();
        }

        dataGroup.close();
    }

    auto TCFWritingAlgorithm::WriteBf(const DataInfo& dataInfo) const -> void {
        auto dataGroup = OpenOrCreateGroupIfNotExists(d->file, "Data");
        {
            auto groupBf = OpenOrCreateGroupIfNotExists(dataGroup, "BF");

            WriteAttributeInt64(groupBf, "DataCount", dataInfo.GetDataCount());
            WriteAttributeDouble(groupBf, "ResolutionX", dataInfo.GetResolutions().resolutionX);
            WriteAttributeDouble(groupBf, "ResolutionY", dataInfo.GetResolutions().resolutionY);
            WriteAttributeInt64(groupBf, "SizeX", dataInfo.GetSizes().sizeX);
            WriteAttributeInt64(groupBf, "SizeY", dataInfo.GetSizes().sizeY);
            WriteAttributeDouble(groupBf, "TimeInterval", dataInfo.GetTimeIntervalInSecs());

            auto dataSetBf =
                OpenOrCreateDataSetBfIfNotExists(groupBf, dataInfo.GetTimelapseIndex(), dataInfo.GetSizes());

            const auto data3dTypeName = std::string(dataInfo.GetRawData().data3d.type().name());

            if (data3dTypeName == "class std::shared_ptr<unsigned char [0]>") {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint8_t[]>>();
                dataSetBf.write(rawData3D.get(), H5::PredType::NATIVE_UCHAR);
            } else {
                const auto rawData3D =
                    *dataInfo.GetRawData().data3d._Cast<std::shared_ptr<uint8_t>>();
                dataSetBf.write(rawData3D.get(), H5::PredType::NATIVE_UCHAR);
            }

            WriteAttributeDouble(dataSetBf, "PositionC", dataInfo.GetPositions().positionC);
            WriteAttributeDouble(dataSetBf, "PositionX", dataInfo.GetPositions().positionX);
            WriteAttributeDouble(dataSetBf, "PositionY", dataInfo.GetPositions().positionY);
            WriteAttributeDouble(dataSetBf, "PositionZ", dataInfo.GetPositions().positionZ);
            WriteAttributeString(dataSetBf, "RecordingTime", dataInfo.GetRecordingTime());

            dataSetBf.close();
            groupBf.close();
        }

        dataGroup.close();
    }

    auto TCFWritingAlgorithm::WriteAttributeString(const H5::H5Object& target, const QString& name,
        const QString& contents) -> void {
        H5::StrType strDataType;
        H5::Attribute attribute;
        try {
            strDataType = H5::StrType(H5::PredType::C_S1, contents.size());
            strDataType.setStrpad(H5T_STR_NULLPAD);
            attribute = OpenOrCreateAttributeIfNotExists(target, name, strDataType);
            attribute.write(strDataType, contents.toLocal8Bit().constData());
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        attribute.close();
        strDataType.close();
    }

    auto TCFWritingAlgorithm::WriteAttributeDouble(const H5::H5Object& target, const QString& name,
        const double& contents) -> void {
        auto doubleDataType{ H5::PredType::NATIVE_DOUBLE };
        H5::Attribute attribute;
        try {
            doubleDataType = H5::PredType::NATIVE_DOUBLE;
            attribute = OpenOrCreateAttributeIfNotExists(target, name, doubleDataType);
            attribute.write(doubleDataType, &contents);
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        attribute.close();
        doubleDataType.close();
    }

    auto TCFWritingAlgorithm::WriteAttributeInt64(const H5::H5Object& target, const QString& name,
        const int64_t& contents) -> void {
        auto int64DataType{ H5::PredType::NATIVE_INT64 };
        H5::Attribute attribute;
        try {
            int64DataType = H5::PredType::NATIVE_INT64;
            attribute = OpenOrCreateAttributeIfNotExists(target, name, int64DataType);
            attribute.write(int64DataType, &contents);
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        attribute.close();
        int64DataType.close();
    }

    auto TCFWritingAlgorithm::WriteCommonAttributes(const TCFMetaInfo& metaInfo) const -> void {
        const auto common = metaInfo.GetCommon();

        const auto ids = common.ids;
        WriteAttributeString(d->file, "DataID", ids.dataID);
        WriteAttributeString(d->file, "UniqueID", ids.uniqueID);
        WriteAttributeString(d->file, "UserID", ids.userID);

        const auto devices = common.devices;
        WriteAttributeString(d->file, "DeviceHost", devices.deviceHost);
        WriteAttributeString(d->file, "DeviceSerial", devices.deviceSerial);
        WriteAttributeString(d->file, "DeviceSoftwareVersion", devices.deviceSoftwareVersion);

        WriteAttributeString(d->file, "FormatVersion", d->tcfFormatVersion);
        WriteAttributeString(d->file, "SoftwareVersion", common.softwareVersion);
        WriteAttributeString(d->file, "CreateDate", common.createDate);
        WriteAttributeString(d->file, "Description", common.description);
        WriteAttributeString(d->file, "RecordingTime", common.recordingTime);
        WriteAttributeString(d->file, "Title", common.title);
    }

    auto TCFWritingAlgorithm::WriteAnnotationAttributes(const TCFMetaInfo& metaInfo) const -> void {
        auto infoGroup = OpenOrCreateGroupIfNotExists(d->file, "Info");
        auto annotationGroup = OpenOrCreateGroupIfNotExists(infoGroup, "Annotation");
        infoGroup.close();

        const auto annotation = metaInfo.GetAnnotation();
        const auto annotationMap = annotation.annotations;

        for (auto iter = annotationMap.begin(); iter != annotationMap.end(); ++iter) {
            const auto name = iter.key();
            const auto& contents = iter.value();

            WriteAttributeString(annotationGroup, name, contents);
        }

        annotationGroup.close();
    }

    auto TCFWritingAlgorithm::WriteDeviceAttributes(const TCFMetaInfo& metaInfo) const -> void {
        auto infoGroup = OpenOrCreateGroupIfNotExists(d->file, "Info");
        auto deviceGroup = OpenOrCreateGroupIfNotExists(infoGroup, "Device");
        infoGroup.close();

        const auto device = metaInfo.GetDevice();
        const auto zps = device.zps;
        WriteAttributeDouble(deviceGroup, "ZP", zps.zp);
        WriteAttributeDouble(deviceGroup, "ZP2", zps.zp2);
        WriteAttributeDouble(deviceGroup, "ZP3", zps.zp3);

        WriteAttributeDouble(deviceGroup, "Iteration", device.iteration);
        WriteAttributeDouble(deviceGroup, "Magnification", device.magnification);
        WriteAttributeDouble(deviceGroup, "NA", device.na);
        WriteAttributeDouble(deviceGroup, "Rawsize", device.rawSize);
        WriteAttributeDouble(deviceGroup, "RI", device.ri);
        WriteAttributeDouble(deviceGroup, "Wavelength", device.waveLength);

        deviceGroup.close();
    }

    auto TCFWritingAlgorithm::WriteImagingAttributes(const TCFMetaInfo& metaInfo) const -> void {
        auto infoGroup = OpenOrCreateGroupIfNotExists(d->file, "Info");
        auto imagingGroup = OpenOrCreateGroupIfNotExists(infoGroup, "Imaging");
        infoGroup.close();

        const auto imaging = metaInfo.GetImaging();
        WriteAttributeDouble(imagingGroup, "CameraGain", imaging.cameraGain);
        WriteAttributeDouble(imagingGroup, "CameraShutter", imaging.cameraShutter);

        imagingGroup.close();
    }

    auto TCFWritingAlgorithm::WriteTileAttributes(const TCFMetaInfo& metaInfo) const -> void {
        auto infoGroup = OpenOrCreateGroupIfNotExists(d->file, "Info");
        auto tileGroup = OpenOrCreateGroupIfNotExists(infoGroup, "Tile");
        infoGroup.close();

        const auto tile = metaInfo.GetTile();
        const auto overlapLengths = tile.overlapLengths;
        WriteAttributeDouble(tileGroup, "Overlap_H", overlapLengths.horizontalLengthInMicroMeter);
        WriteAttributeDouble(tileGroup, "Overlap_V", overlapLengths.verticalLengthInMicroMeter);

        const auto positions = tile.positions;
        WriteAttributeDouble(tileGroup, "C", positions.c);
        WriteAttributeDouble(tileGroup, "X", positions.x);
        WriteAttributeDouble(tileGroup, "Y", positions.y);
        WriteAttributeDouble(tileGroup, "Z", positions.z);

        const auto tileSizes = tile.tileSizes;
        WriteAttributeInt64(tileGroup, "Tiles_H", tileSizes.tileHorizontal);
        WriteAttributeInt64(tileGroup, "Tiles_V", tileSizes.tileVertical);
        WriteAttributeInt64(tileGroup, "Tiles", tileSizes.tileNumber);

        const auto tilePositions = tile.tilePositions;
        WriteAttributeInt64(tileGroup, "ColumnIndex", tilePositions.columnIndex);
        WriteAttributeInt64(tileGroup, "RowIndex", tilePositions.rowIndex);

        tileGroup.close();
    }

    auto TCFWritingAlgorithm::OpenOrCreateGroupIfNotExists(const H5::Group& parentGroup,
        const std::string& groupName) -> H5::Group {
        H5::Group createdGroup;
        try {
            if (parentGroup.exists(groupName)) {
                createdGroup = parentGroup.openGroup(groupName);
            } else {
                createdGroup = parentGroup.createGroup(groupName);
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return createdGroup;
    }

    auto TCFWritingAlgorithm::OpenOrCreateAttributeIfNotExists(const H5::H5Object& parentObject,
        const QString& attributeName, const H5::DataType& dataType) -> H5::Attribute {
        H5::Attribute attribute;
        try {
            const auto cStrName = std::string(attributeName.toLocal8Bit().constData());
            if (parentObject.attrExists(cStrName)) {
                attribute = parentObject.openAttribute(cStrName);
            } else {
                auto strDataSpace = CreateSimpleDataSpace();
                attribute = parentObject.createAttribute(cStrName, dataType, strDataSpace);
                strDataSpace.close();
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return attribute;
    }

    auto TCFWritingAlgorithm::OpenOrCreateDataSet3dIfNotExists(const H5::Group& parentGroup,
        const uint32_t& timelapseIndex, const DataInfo::Sizes& sizes) -> H5::DataSet {
        H5::DataSet dataSet;
        const std::string timelapseString =
            QString("%1").arg(timelapseIndex, 6, 10, QLatin1Char('0')).toLocal8Bit().constData();
        try {
            if (parentGroup.exists(timelapseString)) {
                dataSet = parentGroup.openDataSet(timelapseString);
            } else {
                const auto rank = 3;
                hsize_t dimensions[rank] =
                { static_cast<hsize_t>(sizes.sizeZ),
                    static_cast<hsize_t>(sizes.sizeY),
                    static_cast<hsize_t>(sizes.sizeX) };
                H5::DataSpace dataSpace(rank, dimensions);
                dataSet = parentGroup.createDataSet(timelapseString, H5::PredType::NATIVE_UINT16, dataSpace);

                dataSpace.close();
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        return dataSet;
    }

    auto TCFWritingAlgorithm::OpenOrCreateDataSetMipIfNotExists(const H5::Group& parentGroup,
        const uint32_t& timelapseIndex, const DataInfo::Sizes& sizes) -> H5::DataSet {
        H5::DataSet dataSet;
        const std::string timelapseString =
            QString("%1").arg(timelapseIndex, 6, 10, QLatin1Char('0')).toLocal8Bit().constData();
        try {
            if (parentGroup.exists(timelapseString)) {
                dataSet = parentGroup.openDataSet(timelapseString);
            } else {
                const auto rank = 2;
                hsize_t dimensions[rank] =
                { static_cast<hsize_t>(sizes.sizeY),
                    static_cast<hsize_t>(sizes.sizeX) };
                H5::DataSpace dataSpace(rank, dimensions);
                dataSet = parentGroup.createDataSet(timelapseString, H5::PredType::NATIVE_UINT16, dataSpace);

                dataSpace.close();
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return dataSet;
    }

    auto TCFWritingAlgorithm::OpenOrCreateDataSetPhaseIfNotExists(const H5::Group& parentGroup,
        const uint32_t& timelapseIndex, const DataInfo::Sizes& sizes) -> H5::DataSet {
        H5::DataSet dataSet2d;
        const std::string timelapseString =
            QString("%1").arg(timelapseIndex, 6, 10, QLatin1Char('0')).toLocal8Bit().constData();
        try {
            if (parentGroup.exists(timelapseString)) {
                dataSet2d = parentGroup.openDataSet(timelapseString);
            } else {
                const auto rank = 3;
                hsize_t dimensions[rank] =
                { static_cast<hsize_t>(sizes.sizeT),
                    static_cast<hsize_t>(sizes.sizeY),
                    static_cast<hsize_t>(sizes.sizeX) };
                H5::DataSpace dataSpace(rank, dimensions);
                dataSet2d = parentGroup.createDataSet(timelapseString, H5::PredType::NATIVE_FLOAT, dataSpace);

                dataSpace.close();
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return dataSet2d;
    }

    auto TCFWritingAlgorithm::OpenOrCreateDataSetBfIfNotExists(const H5::Group& parentGroup,
        const uint32_t& timelapseIndex, const DataInfo::Sizes& sizes) -> H5::DataSet {
        H5::DataSet dataSetBf;
        const std::string timelapseString =
            QString("%1").arg(timelapseIndex, 6, 10, QLatin1Char('0')).toLocal8Bit().constData();
        try {
            if (parentGroup.exists(timelapseString)) {
                dataSetBf = parentGroup.openDataSet(timelapseString);
            } else {
                const auto rank = 3;
                hsize_t dimensions[rank] =
                { static_cast<hsize_t>(3),
                    static_cast<hsize_t>(sizes.sizeY),
                    static_cast<hsize_t>(sizes.sizeX) };
                H5::DataSpace dataSpace(rank, dimensions);
                dataSetBf = parentGroup.createDataSet(timelapseString, H5::PredType::NATIVE_UCHAR, dataSpace);

                dataSpace.close();
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return dataSetBf;
    }

    auto TCFWritingAlgorithm::CreateSimpleDataSpace() -> H5::DataSpace {
        const auto rank = 1;
        const hsize_t dims[1] = { 1 };
        return H5::DataSpace(rank, dims);
    }
}
