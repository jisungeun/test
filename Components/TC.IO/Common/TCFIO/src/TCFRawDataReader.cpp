#define LOGGER_TAG "[TCFIO]"
#include <TCLogger.h>

#include <HDF5Mutex.h>

#include <iostream>
#include <QFileInfo>

#include "TCFRawDataReader.h"
#include "TcfLdmThumbnailReader.h"

namespace TC::IO {
	struct TCFRawDataReader::Impl {
		QString path;
		H5::H5File file;

		const double numerator = 10000.0;

	    struct Color {
			int r = 0;
			int g = 0;
			int b = 0;
		};

		auto ReadAttribute(H5::Group& group, const char* name, int& value) const->bool;
		auto ReadColor(H5::Group& group, const int& ch) const->Color;
	};

	auto TCFRawDataReader::Impl::ReadAttribute(H5::Group& group, const char* name, int& value) const->bool {
		if (false == group.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = group.openAttribute(name);
		H5Aread(attr.getId(), H5T_NATIVE_INT, &value);
		attr.close();

		return true;
	}

	auto TCFRawDataReader::Impl::ReadColor(H5::Group& group, const int& ch) const ->Color {
		const auto redName = "ColorR";
		const auto greenName = "ColorG";
		const auto blueName = "ColorB";

		Color defaultColor[3] = { {0, 0, 255}, {0, 255, 0}, {255, 0, 0} };

		auto red = defaultColor[ch].r;
		auto green = defaultColor[ch].g;
		auto blue = defaultColor[ch].b;

		if (group.attrExists(redName)) ReadAttribute(group, redName, red);
		if (group.attrExists(greenName)) ReadAttribute(group, greenName, green);
		if (group.attrExists(blueName)) ReadAttribute(group, blueName, blue);

		return { red, green, blue };
	}

	TCFRawDataReader::TCFRawDataReader() : d{ new Impl } {
		
	}

	TCFRawDataReader::~TCFRawDataReader() {
		
	}

	bool TCFRawDataReader::Open(const QString& path) {				
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		if(true == path.isEmpty()) {
			return false;
		}

        const QFileInfo fileInfo(path);
		if (false == fileInfo.exists() || true == fileInfo.isDir()) {
			return false;
		}

		try {
			d->file.openFile(path.toStdString().c_str(), H5F_ACC_RDONLY);
			d->path = path;
		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}
		
		return true;
	}

	bool TCFRawDataReader::Exist(const RawDataType& type) {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		try {
			const auto groupPath = GetGroupPathFromType(type);
			if (true == groupPath.isEmpty()) {
				return false;
			}

			return d->file.exists(groupPath.toStdString().c_str());
		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
		}

		return false;
	}
	bool TCFRawDataReader::ReadLdmHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data,int &x,int &y) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/2DMIP";
		if (false == d->file.exists(groupPath)) {
			return false;
		}
		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));
		try {
			H5::Group group = d->file.openGroup(groupPath);

			auto ldmDataGroup = group.openGroup(frameIndex.toStdString().c_str());

		    const auto targetImageSizeX = 1024;
			const auto targetImageSizeY = 1024;

            LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
			tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
			tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

			const auto memoryChunk = tcfLdmThumbnailReader.Read();

			const auto imageDimension = memoryChunk->GetDimension();			
			x = imageDimension.X();
			y = imageDimension.Y();

			const auto numberOfElements = imageDimension.GetNumberOfElements();

		    data = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

			for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
				data[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
			}
		}catch(H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}
		return true;
    }

	bool TCFRawDataReader::ReadHTMIP( const int& index, std::shared_ptr<unsigned short[]>& data) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		auto attr = d->file.openAttribute("FormatVersion");		
		H5std_string h5str;

		H5::DataType dt = attr.getDataType();
		attr.read(dt, h5str);
		attr.close();

		auto version_split = QString(h5str.c_str()).split(".");		
		auto minor_version = version_split[1].toInt();		

	    const auto groupPath = "/Data/2DMIP";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		try {
			H5::Group group = d->file.openGroup(groupPath);

			const auto dataSet = group.openDataSet(frameIndex.toStdString().c_str());
			const auto dataSpace = dataSet.getSpace();

			const auto dimensionality = dataSpace.getSimpleExtentNdims();
			const auto dimsOut = new hsize_t[dimensionality];
			dataSpace.getSimpleExtentDims(dimsOut, nullptr);

			uint32_t elements = 1;
			for (auto i = 0; i < dimensionality; i++) {
				elements = elements * dimsOut[i];
			}

			data = std::shared_ptr<unsigned short[]>(new unsigned short[elements]);

			if(minor_version<3) {
				auto double_data = std::shared_ptr<double[]>(new double[elements]);
				dataSet.read(double_data.get(), H5::PredType::NATIVE_DOUBLE);

				for(auto i=0;i<elements;i++) {
					data.get()[i] = double_data.get()[i] * 10000.0f;
				}
				double_data = nullptr;
			}else {
				dataSet.read(data.get(), H5::PredType::NATIVE_USHORT);
			}			
		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
	}
	bool TCFRawDataReader::ReadLdmFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data,int&x,int&y) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/2DFLMIP";

		if(false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		auto blueData = std::shared_ptr<unsigned short[]>();
		auto greenData = std::shared_ptr<unsigned short[]>();
		auto redData = std::shared_ptr<unsigned short[]>();

		try {
			uint32_t length = 1;
		    H5::Group group = d->file.openGroup(groupPath);
			if (true == group.exists("CH0")) {
				auto channelGroup = group.openGroup("CH0");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					blueData = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						blueData[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}
				}
			}
			if (true == group.exists("CH1")) {
				auto channelGroup = group.openGroup("CH1");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					greenData = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						greenData[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}
				}
			}
			if(true == group.exists("CH2")) {
				auto channelGroup = group.openGroup("CH2");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					redData = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						redData[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}
				}
			}

			length = x * y;
			data = std::shared_ptr<uint32_t[]>(new uint32_t[length]);

			for (uint32_t i = 0; i < length; ++i) {
				uint32_t b = 0;
				if (nullptr != blueData) {
					b = std::max<uint32_t>(blueData.get()[i], 0);
					b = std::min<uint32_t>(b, 255);
				}

				uint32_t g = 0;
				if (nullptr != greenData) {
					g = std::max<uint32_t>(greenData.get()[i], 0);
					g = std::min<uint32_t>(g, 255);
				}

				uint32_t r = 0;
				if (nullptr != redData) {
					r = std::max<uint32_t>(redData.get()[i], 0);
					r = std::min<uint32_t>(r, 255);
				}

				const uint32_t color = 0xFF000000 + (0x00FF0000 & (r << 16)) + (0x0000FF00 & (g << 8)) + (0x000000FF & (b));
				data.get()[i] = color;
			}
		}catch(H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
    }

	bool TCFRawDataReader::ReadFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data)const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/2DFLMIP";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		auto blueData = std::shared_ptr<unsigned short[]>();
		auto greenData = std::shared_ptr<unsigned short[]>();
		auto redData = std::shared_ptr<unsigned short[]>();

		try {
			uint32_t length = 1;

			H5::Group group = d->file.openGroup(groupPath);

			if (true == group.exists("CH0")) {
				auto channelGroup = group.openGroup("CH0");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					blueData = std::shared_ptr<unsigned short[]>(new unsigned short[elements]);
					dataSet.read(blueData.get(), H5::PredType::NATIVE_USHORT);
				}
			}
			
			if (true == group.exists("CH1")) {
				auto channelGroup = group.openGroup("CH1");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					greenData = std::shared_ptr<unsigned short[]>(new unsigned short[elements]{ 0, });
					dataSet.read(greenData.get(), H5::PredType::NATIVE_USHORT);
				}
			}

			if (true == group.exists("CH2")) {
				auto channelGroup = group.openGroup("CH2");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					redData = std::shared_ptr<unsigned short[]>(new unsigned short[elements]{ 0, });
					dataSet.read(redData.get(), H5::PredType::NATIVE_USHORT);
				}
			}

			data = std::shared_ptr<uint32_t[]>(new uint32_t[length]);

			for (uint32_t i = 0; i < length; ++i) {
				uint32_t b = 0;
				if(nullptr != blueData) {
					b = std::max<uint32_t>(blueData.get()[i], 0);
					b = std::min<uint32_t>(b, 255);
				}

				uint32_t g = 0;
				if (nullptr != greenData) {
					g = std::max<uint32_t>(greenData.get()[i], 0);
					g = std::min<uint32_t>(g, 255);
				}

				uint32_t r = 0;
				if (nullptr != redData) {
					r = std::max<uint32_t>(redData.get()[i], 0);
					r = std::min<uint32_t>(r, 255);
				}
				
				const uint32_t color = 0xFF000000 + (0x00FF0000 & (r << 16)) + (0x0000FF00 & (g << 8)) + (0x000000FF & (b));
				data.get()[i] = color;
			}
		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
	}

	bool TCFRawDataReader::ReadLdmBF(const int& index, std::shared_ptr<uint32_t[]>& data,int &x,int &y) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/BF";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));
		try {
			H5::Group group = d->file.openGroup(groupPath);

			auto ldmDataGroup = group.openGroup(frameIndex.toStdString().c_str());
			const auto targetImageSizeX = 1024;
			const auto targetImageSizeY = 1024;

            LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
			tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
			tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);
			tcfLdmThumbnailReader.SetColorFlag(true);

			const auto memoryChunk = tcfLdmThumbnailReader.Read();

			const auto imageDimension = memoryChunk->GetDimension();
			x = static_cast<int32_t>(imageDimension.X());
			y = static_cast<int32_t>(imageDimension.Y());

			auto imageRawData = memoryChunk->GetDataPointerAs<uint8_t>();

			auto elements = imageDimension.GetNumberOfElements();

			data = std::shared_ptr<uint32_t[]>(new uint32_t[elements]);

			for (size_t kk = 0; kk < elements / 3; kk++) {
				const auto rv = imageRawData[(elements / 3 * 0) + kk];
				const auto gv = imageRawData[(elements / 3 * 1) + kk];
				const auto bv = imageRawData[(elements / 3 * 2) + kk];

				const auto r = static_cast<uint32_t>(rv);
				const auto g = static_cast<uint32_t>(gv);
				const auto b = static_cast<uint32_t>(bv);

				const uint32_t color = 0xFF000000 + (0x00FF0000 & (r << 16)) + (0x0000FF00 & (g << 8)) + (0x000000FF & (b));
				data.get()[kk] = color;
			}
		}catch(H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}
		return true;
    }


	bool TCFRawDataReader::ReadBF(const int& index, std::shared_ptr<uint32_t[]>& data) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/BF";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		try {
			H5::Group group = d->file.openGroup(groupPath);

			const auto dataSet = group.openDataSet(frameIndex.toStdString().c_str());
			const auto dataSpace = dataSet.getSpace();

			const auto dimensionality = dataSpace.getSimpleExtentNdims();
			const auto dimsOut = new hsize_t[dimensionality];
			dataSpace.getSimpleExtentDims(dimsOut, nullptr);

			uint32_t elements = 1;
			for (auto i = 0; i < dimensionality; i++) {
				elements = elements * dimsOut[i];
			}

			data = std::shared_ptr<uint32_t[]>(new uint32_t[elements]);
			dataSet.read(data.get(), H5::PredType::NATIVE_UCHAR);

			if (dataSet.getDataType() != H5::PredType::NATIVE_UCHAR) {
				return false;
			}

			auto pTemp = new unsigned char[elements];
			dataSet.read(pTemp, H5::PredType::NATIVE_UCHAR);

			for (size_t kk = 0; kk < elements / 3; kk++) {
				const auto rv = pTemp[(elements / 3 * 0) + kk];
				const auto gv = pTemp[(elements / 3 * 1) + kk];
				const auto bv = pTemp[(elements / 3 * 2) + kk];

				const auto r = static_cast<uint32_t>(rv);
				const auto g = static_cast<uint32_t>(gv);
				const auto b = static_cast<uint32_t>(bv);

				const uint32_t color = 0xFF000000 + (0x00FF0000 & (r << 16)) + (0x0000FF00 & (g << 8)) + (0x000000FF & (b));
				data.get()[kk] = color;
			}

			delete[] pTemp;

		}
		catch (H5::Exception& e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
	}

	auto TCFRawDataReader::GetGroupPathFromType(const RawDataType& type)->QString {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		QString groupPath;

	    switch (type) {
			case RawDataType::HT_3D: {
				groupPath = "/Data/3D";
				break;
			}
			case RawDataType::HT_MIP: {
				groupPath = "/Data/2DMIP";
				break;
			}
			case RawDataType::FL_3D: {
				groupPath = "/Data/3DFL";
				break;
			}
			case RawDataType::FL_MIP: {
				groupPath = "/Data/2DFLMIP";
				break;
			}
			case RawDataType::PHASE: {
				groupPath = "/Data/2D";
				break;
			}
			case RawDataType::BF: {
				groupPath = "/Data/BF";
				break;
			}
			default: break;
        }

		return groupPath;
	}

	bool TCFRawDataReader::GetHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data) const {
		return ReadHTMIP(index, data);
	}

	bool TCFRawDataReader::GetLdmHTMIP(const int& index, std::shared_ptr<unsigned short[]>& data, int& x, int& y) const {
		return ReadLdmHTMIP(index, data, x, y);
	}

	bool TCFRawDataReader::GetFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data)const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/2DFLMIP";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		auto dataCh0 = std::shared_ptr<unsigned short[]>();
		auto dataCh1 = std::shared_ptr<unsigned short[]>();
		auto dataCh2 = std::shared_ptr<unsigned short[]>();

		Impl::Color colorCh0;
		Impl::Color colorCh1;
		Impl::Color colorCh2;

		try {
			uint32_t length = 1;

			H5::Group group = d->file.openGroup(groupPath);

			if (true == group.exists("CH0")) {
				auto channelGroup = group.openGroup("CH0");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					dataCh0 = std::shared_ptr<unsigned short[]>(new unsigned short[elements]);
					dataSet.read(dataCh0.get(), H5::PredType::NATIVE_USHORT);

					colorCh0 = d->ReadColor(channelGroup, 0);
				}
			}

			if (true == group.exists("CH1")) {
				auto channelGroup = group.openGroup("CH1");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					dataCh1 = std::shared_ptr<unsigned short[]>(new unsigned short[elements] { 0, });
					dataSet.read(dataCh1.get(), H5::PredType::NATIVE_USHORT);

					colorCh1 = d->ReadColor(channelGroup, 1);
				}
			}

			if (true == group.exists("CH2")) {
				auto channelGroup = group.openGroup("CH2");

				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					// get size
					const auto dataSet = channelGroup.openDataSet(frameIndex.toStdString().c_str());
					const auto dataSpace = dataSet.getSpace();

					const auto dimensionality = dataSpace.getSimpleExtentNdims();
					const auto dimsOut = new hsize_t[dimensionality];
					dataSpace.getSimpleExtentDims(dimsOut, nullptr);

					uint32_t elements = 1;
					for (auto i = 0; i < dimensionality; i++) {
						elements = elements * dimsOut[i];
					}

					length = elements;

					dataCh2 = std::shared_ptr<unsigned short[]>(new unsigned short[elements] { 0, });
					dataSet.read(dataCh2.get(), H5::PredType::NATIVE_USHORT);

					colorCh2 = d->ReadColor(channelGroup, 2);
				}
			}

			data = std::shared_ptr<uint32_t[]>(new uint32_t[length]);

			for (uint32_t i = 0; i < length; ++i) {
				uint32_t ch0 = 0;
				if (nullptr != dataCh0) {
					ch0 = std::max<uint32_t>(dataCh0.get()[i], 0);
					ch0 = std::min<uint32_t>(ch0, 255);
				}

				uint32_t ch1 = 0;
				if (nullptr != dataCh1) {
					ch1 = std::max<uint32_t>(dataCh1.get()[i], 0);
					ch1 = std::min<uint32_t>(ch1, 255);
				}

				uint32_t ch2 = 0;
				if (nullptr != dataCh2) {
					ch2 = std::max<uint32_t>(dataCh2.get()[i], 0);
					ch2 = std::min<uint32_t>(ch2, 255);
				}

				uint32_t r = (ch0 & colorCh0.r) + (ch1 & colorCh1.r) + (ch2 & colorCh2.r);
				uint32_t g = (ch0 & colorCh0.g) + (ch1 & colorCh1.g) + (ch2 & colorCh2.g);
				uint32_t b = (ch0 & colorCh0.b) + (ch1 & colorCh1.b) + (ch2 & colorCh2.b);

				r = std::max<uint32_t>(r, 0);
				r = std::min<uint32_t>(r, 255);

				g = std::max<uint32_t>(g, 0);
				g = std::min<uint32_t>(g, 255);

				b = std::max<uint32_t>(b, 0);
				b = std::min<uint32_t>(b, 255);

				const uint32_t color = 0xFF000000 + (r << 16) + (g << 8) + (b);
				data.get()[i] = color;
			}
		}
		catch (H5::Exception & e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
	}

	bool TCFRawDataReader::GetLdmFLMIP(const int& index, std::shared_ptr<uint32_t[]>& data, int& x, int& y) const {
		HDF5MutexLocker lock(HDF5Mutex::GetInstance());
		const auto groupPath = "/Data/2DFLMIP";

		if (false == d->file.exists(groupPath)) {
			return false;
		}

		const auto frameIndex = QString("%1").arg(index, 6, 10, QChar('0'));

		auto dataCh0 = std::shared_ptr<unsigned short[]>();
		auto dataCh1 = std::shared_ptr<unsigned short[]>();
		auto dataCh2 = std::shared_ptr<unsigned short[]>();

		Impl::Color colorCh0;
		Impl::Color colorCh1;
		Impl::Color colorCh2;

		try {
			uint32_t length = 1;
			H5::Group group = d->file.openGroup(groupPath);
			if (true == group.exists("CH0")) {
				auto channelGroup = group.openGroup("CH0");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					dataCh0 = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						dataCh0[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}

					colorCh0 = d->ReadColor(channelGroup, 0);
				}
			}
			if (true == group.exists("CH1")) {
				auto channelGroup = group.openGroup("CH1");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					dataCh1 = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						dataCh1[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}

					colorCh1 = d->ReadColor(channelGroup, 1);
				}
			}
			if (true == group.exists("CH2")) {
				auto channelGroup = group.openGroup("CH2");
				if (true == channelGroup.exists(frameIndex.toStdString().c_str())) {
					auto ldmDataGroup = channelGroup.openGroup(frameIndex.toStdString().c_str());
					const auto targetImageSizeX = 1024;
					const auto targetImageSizeY = 1024;

					LdmReading::TcfLdmThumbnailReader tcfLdmThumbnailReader;
					tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
					tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

					const auto memoryChunk = tcfLdmThumbnailReader.Read();

					const auto imageDimension = memoryChunk->GetDimension();
					x = imageDimension.X();
					y = imageDimension.Y();

					const auto numberOfElements = imageDimension.GetNumberOfElements();

					dataCh2 = std::shared_ptr<unsigned short[]>(new unsigned short[numberOfElements]);

					for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
						dataCh2[i] = memoryChunk->GetDataPointerAs<uint16_t>()[i];
					}

					colorCh2 = d->ReadColor(channelGroup, 2);
				}
			}

			length = x * y;
			data = std::shared_ptr<uint32_t[]>(new uint32_t[length]);

			for (uint32_t i = 0; i < length; ++i) {
				uint32_t ch0 = 0;
				if (nullptr != dataCh0) {
					ch0 = std::max<uint32_t>(dataCh0.get()[i], 0);
					ch0 = std::min<uint32_t>(ch0, 255);
				}

				uint32_t ch1 = 0;
				if (nullptr != dataCh1) {
					ch1 = std::max<uint32_t>(dataCh1.get()[i], 0);
					ch1 = std::min<uint32_t>(ch1, 255);
				}

				uint32_t ch2 = 0;
				if (nullptr != dataCh2) {
					ch2 = std::max<uint32_t>(dataCh2.get()[i], 0);
					ch2 = std::min<uint32_t>(ch2, 255);
				}

				uint32_t r = (ch0 & colorCh0.r) + (ch1 & colorCh1.r) + (ch2 & colorCh2.r);
				uint32_t g = (ch0 & colorCh0.g) + (ch1 & colorCh1.g) + (ch2 & colorCh2.g);
				uint32_t b = (ch0 & colorCh0.b) + (ch1 & colorCh1.b) + (ch2 & colorCh2.b);

				r = std::max<uint32_t>(r, 0);
				r = std::min<uint32_t>(r, 255);

				g = std::max<uint32_t>(g, 0);
				g = std::min<uint32_t>(g, 255);

				b = std::max<uint32_t>(b, 0);
				b = std::min<uint32_t>(b, 255);

				const uint32_t color = 0xFF000000 + (r << 16) + (g << 8) + (b);
				data.get()[i] = color;
			}
		}
		catch (H5::Exception & e) {
			QLOG_ERROR() << e.getCDetailMsg();
			return false;
		}

		return true;
	}
	
	bool TCFRawDataReader::GetBF(const int& index, std::shared_ptr<uint32_t[]>& data) const {
		return ReadBF(index, data);
	}

	bool TCFRawDataReader::GetLdmBF(const int& index, std::shared_ptr<uint32_t[]>& data, int& x, int& y) const {
		return ReadLdmBF(index, data, x, y);
	}

}
