#include <catch2/catch.hpp>
#include "TCFWritingAlgorithm.h"

namespace _TCFWritingAlgorithmTest {
    using namespace TC::IO;

    auto CheckAttribute(const H5::H5Object& object, const std::string& name, const int64_t&answer)->void {
        int64_t output;
        auto int64DataType = H5::PredType::NATIVE_INT64;
        auto targetAttribute = object.openAttribute(name);
        targetAttribute.read(int64DataType, &output);

        CHECK(output == answer);

        targetAttribute.close();
        int64DataType.close();
    }

    auto CheckAttribute(const H5::H5Object& object, const std::string& name, const std::string& answer)->void {
        std::string output;
        auto strDataType = H5::StrType(H5::PredType::C_S1, answer.size() + 1);
        auto targetAttribute = object.openAttribute(name);
        targetAttribute.read(strDataType, output);

        CHECK(output == answer);

        targetAttribute.close();
        strDataType.close();
    }

    auto CheckAttribute(const H5::H5Object& object, const std::string& name, const double& answer)->void {
        double output;
        auto int64DataType = H5::PredType::NATIVE_DOUBLE;
        auto targetAttribute = object.openAttribute(name);
        targetAttribute.read(int64DataType, &output);

        CHECK(output == answer);

        targetAttribute.close();
        int64DataType.close();
    }

    auto GenerateDummyDataInfo()->DataInfo {
        DataInfo dataInfo;
        DataInfo::RawData dummyRawData;
        dummyRawData.data3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
        dummyRawData.dataMip = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
        dataInfo.SetRawData(dummyRawData);

        DataInfo::Sizes sizes;
        sizes.sizeX = 2;
        sizes.sizeY = 3;
        sizes.sizeZ = 4;
        dataInfo.SetSizes(sizes);

        return dataInfo;
    }

    auto GenerateDataInfo(const ImageType& imageType, const DataInfo::RawData& rawData,
        const uint32_t& timelapseIndex, const DataInfo::Sizes& sizes)->DataInfo {

        DataInfo dataInfo;
        dataInfo.SetImageType(imageType);
        dataInfo.SetRawData(rawData);
        dataInfo.SetTimelapseIndex(timelapseIndex);
        dataInfo.SetSizes(sizes);

        dataInfo.SetMinMax3d(DataInfo::MinMax{ 1,2 });
        dataInfo.SetMinMaxMip(DataInfo::MinMax{ 3,4 });
        dataInfo.SetPositions(DataInfo::Positions{ 5,6,7,8 });
        dataInfo.SetRecordingTime("9");
        dataInfo.SetChannels(10);
        dataInfo.SetOffsetZs(DataInfo::OffsetZs{ 11,12 });
        dataInfo.SetDataCount(13);
        dataInfo.SetResolutions(DataInfo::Resolutions{ 14,15,16,17 });
        dataInfo.SetTimeInterval(18);
        return dataInfo;
    }

    TEST_CASE("TCFWritingAlgorithm") {
        const std::string tempFilePath = "temp.h5";

        struct stat buff;
        const auto tempFileExists = (stat(tempFilePath.c_str(), &buff) == 0);

        if(tempFileExists) {
            const auto existFileIsRemoved = std::remove(tempFilePath.c_str()) == 0;
            CHECK(existFileIsRemoved);
        }


        SECTION("CommonAttributes") {
            TCFWritingAlgorithm algorithm;
            TCFMetaInfo::Common common;
            common.ids.dataID = "DataID";
            common.ids.uniqueID = "UniqueID";
            common.ids.userID = "UserID";
            common.devices.deviceSerial = "DeviceSerial";
            common.devices.deviceSoftwareVersion = "DeviceSoftwareVersion";
            common.devices.deviceHost = "DeviceHost";
            common.formatVersion = "FormatVersion";
            common.softwareVersion = "SoftwareVersion";
            common.createDate = "CreateDate";
            common.description = "Description";
            common.recordingTime = "RecordingTime";
            common.title = "Title";

            TCFMetaInfo commonMetaInfo;
            commonMetaInfo.SetCommon(common);

            const auto dummyDataInfo = GenerateDummyDataInfo();

            TCFWritingData data;
            data.SetInfo(dummyDataInfo, commonMetaInfo);

            algorithm.Write(QString::fromStdString(tempFilePath), data);

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CheckAttribute(file, "DataID", "DataID");
            CheckAttribute(file, "UniqueID", "UniqueID");
            CheckAttribute(file, "UserID", "UserID");
            CheckAttribute(file, "DeviceSerial", "DeviceSerial");
            CheckAttribute(file, "DeviceSoftwareVersion", "DeviceSoftwareVersion");
            CheckAttribute(file, "DeviceHost", "DeviceHost");
            CheckAttribute(file, "FormatVersion", "1.3.6");
            CheckAttribute(file, "SoftwareVersion", "SoftwareVersion");
            CheckAttribute(file, "CreateDate", "CreateDate");
            CheckAttribute(file, "Description", "Description");
            CheckAttribute(file, "RecordingTime", "RecordingTime");
            CheckAttribute(file, "Title", "Title");

            CHECK(file.exists("/Info/Tile") == false);
            
            file.close();
        }

        SECTION("Annotation") {
            TCFWritingAlgorithm algorithm;
            TCFMetaInfo::Annotation annotation;

            QString annotation1 = "Annotation1";
            QString annotation2 = "Annotation2";

            annotation.annotations[annotation1] = annotation1;
            annotation.annotations[annotation2] = annotation2;

            TCFMetaInfo annotationMetaInfo;
            annotationMetaInfo.SetAnnotation(annotation);

            const auto dummyDataInfo = GenerateDummyDataInfo();

            TCFWritingData data;
            data.SetInfo(dummyDataInfo, annotationMetaInfo);

            algorithm.Write(QString::fromStdString(tempFilePath), data);

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            auto annotationGroup = file.openGroup("/Info/Annotation");
            CheckAttribute(annotationGroup, "Annotation1", "Annotation1");
            CheckAttribute(annotationGroup, "Annotation2", "Annotation2");

            CHECK(file.exists("/Info/Tile") == false);
            annotationGroup.close();
            file.close();
        }

        SECTION("DeviceAttributes") {
            TCFWritingAlgorithm algorithm;
            TCFMetaInfo::Device device;
            device.iteration = 1;
            device.magnification = 2;
            device.na = 3;
            device.ri = 4;
            device.rawSize = 5;
            device.waveLength = 6;
            device.zps.zp = 7;
            device.zps.zp2 = 8;
            device.zps.zp3 = 9;

            TCFMetaInfo deviceMetaInfo;
            deviceMetaInfo.SetDevice(device);

            const auto dummyDataInfo = GenerateDummyDataInfo();
            TCFWritingData data;
            data.SetInfo(dummyDataInfo, deviceMetaInfo);
            algorithm.Write(QString::fromStdString(tempFilePath), data);

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            auto deviceGroup = file.openGroup("/Info/Device");

            CheckAttribute(deviceGroup, "Iteration", double(1));
            CheckAttribute(deviceGroup, "Magnification", double(2));
            CheckAttribute(deviceGroup, "NA", double(3));
            CheckAttribute(deviceGroup, "RI", double(4));
            CheckAttribute(deviceGroup, "Rawsize", double(5));
            CheckAttribute(deviceGroup, "Wavelength", double(6));
            CheckAttribute(deviceGroup, "ZP", double(7));
            CheckAttribute(deviceGroup, "ZP2", double(8));
            CheckAttribute(deviceGroup, "ZP3", double(9));

            CHECK(file.exists("/Info/Tile") == false);
            deviceGroup.close();
            file.close();
        }

        SECTION("ImagingAttributes") {
            TCFWritingAlgorithm algorithm;
            TCFMetaInfo::Imaging imaging;
            imaging.cameraShutter = 1;
            imaging.cameraGain = 2;

            TCFMetaInfo imagingMetaInfo;
            imagingMetaInfo.SetImaging(imaging);

            const auto dummyDataInfo = GenerateDummyDataInfo();
            TCFWritingData data;
            data.SetInfo(dummyDataInfo, imagingMetaInfo);
            algorithm.Write(QString::fromStdString(tempFilePath), data);

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            auto imagingGroup = file.openGroup("/Info/Imaging");
            CheckAttribute(imagingGroup, "CameraShutter", double(1));
            CheckAttribute(imagingGroup, "CameraGain", double(2));

            CHECK(file.exists("/Info/Tile") == false);
            imagingGroup.close();
            file.close();
        }

        SECTION("TileAttributes") {
            TCFWritingAlgorithm algorithm;
            TCFMetaInfo::Tile tile;
            tile.tilePositions.rowIndex = 1;
            tile.tilePositions.columnIndex = 2;
            tile.overlapLengths.verticalLengthInMicroMeter = 3;
            tile.overlapLengths.horizontalLengthInMicroMeter = 4;
            tile.tileSizes.tileVertical = 5;
            tile.tileSizes.tileHorizontal = 6;
            tile.tileSizes.tileNumber = 7;
            tile.positions.c = 8;
            tile.positions.x = 9;
            tile.positions.y = 10;
            tile.positions.z = 11;

            TCFMetaInfo tileMetaInfo;
            tileMetaInfo.SetTile(tile);

            const auto dummyDataInfo = GenerateDummyDataInfo();
            TCFWritingData data;
            data.SetInfo(dummyDataInfo, tileMetaInfo);
            algorithm.Write(QString::fromStdString(tempFilePath), data);

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            auto tileGroup = file.openGroup("/Info/Tile");
            CheckAttribute(tileGroup, "RowIndex", int64_t(1));
            CheckAttribute(tileGroup, "ColumnIndex", int64_t(2));
            CheckAttribute(tileGroup, "Overlap_V", double(3));
            CheckAttribute(tileGroup, "Overlap_H", double(4));
            CheckAttribute(tileGroup, "Tiles_V", int64_t(5));
            CheckAttribute(tileGroup, "Tiles_H", int64_t(6));
            CheckAttribute(tileGroup, "Tiles", int64_t(7));
            CheckAttribute(tileGroup, "C", double(8));
            CheckAttribute(tileGroup, "X", double(9));
            CheckAttribute(tileGroup, "Y", double(10));
            CheckAttribute(tileGroup, "Z", double(11));

            CHECK(file.exists("/Info/Tile") == true);
            tileGroup.close();
            file.close();
        }

        SECTION("Write HT") {
            auto hostData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3d[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMip[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::HT, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo1 =
                    GenerateDataInfo(ImageType::HT, rawData, 1,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo1, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }


            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == true);
            CHECK(file.exists("Data/2DMIP") == true);
            CHECK(file.exists("Data/3DFL") == false);
            CHECK(file.exists("Data/2DFLMIP") == false);
            CHECK(file.exists("Data/2D") == false);
            CHECK(file.exists("Data/BF") == false);

            auto group3d = file.openGroup("Data/3D");
            auto groupMip = file.openGroup("Data/2DMIP");

            CHECK(group3d.exists("000000") == true);
            CHECK(groupMip.exists("000000") == true);
            CHECK(group3d.exists("000001") == true);
            CHECK(groupMip.exists("000001") == true);

            auto dataSet3d_0 = group3d.openDataSet("000000");
            auto dataSetMip_0 = groupMip.openDataSet("000000");
            auto dataSet3d_1 = group3d.openDataSet("000001");
            auto dataSetMip_1 = groupMip.openDataSet("000001");

            auto outputData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto outputDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());
            dataSet3d_0.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            auto difference3d = false;
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);

            dataSet3d_1.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);


            dataSetMip_0.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            auto differenceMip = false;
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            dataSetMip_1.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            hsize_t group3dDims[3]{ 0,0,0 }, groupMipDims[2]{ 0,0 };
            dataSet3d_0.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_0.getSpace().getSimpleExtentDims(groupMipDims);

            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            dataSet3d_1.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_1.getSpace().getSimpleExtentDims(groupMipDims);
            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            CheckAttribute(group3d, "RIMin", double(1));
            CheckAttribute(dataSet3d_0, "RIMin", double(1));
            CheckAttribute(dataSet3d_1, "RIMin", double(1));
            CheckAttribute(group3d, "RIMax", double(2));
            CheckAttribute(dataSet3d_0, "RIMax", double(2));
            CheckAttribute(dataSet3d_1, "RIMax", double(2));
            CheckAttribute(groupMip, "RIMin", double(3));
            CheckAttribute(dataSetMip_0, "RIMin", double(3));
            CheckAttribute(dataSetMip_1, "RIMin", double(3));
            CheckAttribute(groupMip, "RIMax", double(4));
            CheckAttribute(dataSetMip_0, "RIMax", double(4));
            CheckAttribute(dataSetMip_1, "RIMax", double(4));

            CheckAttribute(dataSet3d_0, "PositionX", double(5));
            CheckAttribute(dataSet3d_1, "PositionX", double(5));
            CheckAttribute(dataSetMip_0, "PositionX", double(5));
            CheckAttribute(dataSetMip_1, "PositionX", double(5));
            CheckAttribute(dataSet3d_0, "PositionY", double(6));
            CheckAttribute(dataSet3d_1, "PositionY", double(6));
            CheckAttribute(dataSetMip_0, "PositionY", double(6));
            CheckAttribute(dataSetMip_1, "PositionY", double(6));
            CheckAttribute(dataSet3d_0, "PositionZ", double(7));
            CheckAttribute(dataSet3d_1, "PositionZ", double(7));
            CheckAttribute(dataSetMip_0, "PositionZ", double(7));
            CheckAttribute(dataSetMip_1, "PositionZ", double(7));
            CheckAttribute(dataSet3d_0, "PositionC", double(8));
            CheckAttribute(dataSet3d_1, "PositionC", double(8));
            CheckAttribute(dataSetMip_0, "PositionC", double(8));
            CheckAttribute(dataSetMip_1, "PositionC", double(8));

            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");

            CheckAttribute(group3d, "DataCount", int64_t(13));
            CheckAttribute(groupMip, "DataCount", int64_t(13));

            CheckAttribute(group3d, "ResolutionX", double(14));
            CheckAttribute(groupMip, "ResolutionX", double(14));
            CheckAttribute(group3d, "ResolutionY", double(15));
            CheckAttribute(groupMip, "ResolutionY", double(15));
            CheckAttribute(group3d, "ResolutionZ", double(16));
            CheckAttribute(group3d, "TimeInterval", double(18));

            group3d.close();
            groupMip.close();
            dataSet3d_0.close();
            dataSetMip_0.close();
            dataSet3d_1.close();
            dataSetMip_1.close();
            file.close();
        }

        SECTION("Write FL Blue") {
            auto hostData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3d[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMip[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLBLUE, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo1 =
                    GenerateDataInfo(ImageType::FLBLUE, rawData, 1,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo1, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }


            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == false);
            CHECK(file.exists("Data/2DMIP") == false);
            CHECK(file.exists("Data/3DFL") == true);
            CHECK(file.exists("Data/2DFLMIP") == true);
            CHECK(file.exists("Data/3DFL/CH0") == true);
            CHECK(file.exists("Data/2DFLMIP/CH0") == true);
            CHECK(file.exists("Data/3DFL/CH1") == false);
            CHECK(file.exists("Data/2DFLMIP/CH1") == false);
            CHECK(file.exists("Data/3DFL/CH2") == false);
            CHECK(file.exists("Data/2DFLMIP/CH2") == false);
            CHECK(file.exists("Data/2D") == false);
            CHECK(file.exists("Data/BF") == false);


            auto group3dFl = file.openGroup("Data/3DFL");
            auto groupMipFl = file.openGroup("Data/2DFLMIP");

            auto group3dFlSub = group3dFl.openGroup("CH0");
            auto groupMipFlSub = groupMipFl.openGroup("CH0");

            CHECK(group3dFlSub.exists("000000") == true);
            CHECK(groupMipFlSub.exists("000000") == true);
            CHECK(group3dFlSub.exists("000001") == true);
            CHECK(groupMipFlSub.exists("000001") == true);

            auto dataSet3d_0 = group3dFlSub.openDataSet("000000");
            auto dataSetMip_0 = groupMipFlSub.openDataSet("000000");
            auto dataSet3d_1 = group3dFlSub.openDataSet("000001");
            auto dataSetMip_1 = groupMipFlSub.openDataSet("000001");

            auto outputData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto outputDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());
            dataSet3d_0.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            auto difference3d = false;
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);

            dataSet3d_1.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);


            dataSetMip_0.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            auto differenceMip = false;
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            dataSetMip_1.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            hsize_t group3dDims[3]{ 0,0,0 }, groupMipDims[2]{ 0,0 };
            dataSet3d_0.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_0.getSpace().getSimpleExtentDims(groupMipDims);

            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            dataSet3d_1.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_1.getSpace().getSimpleExtentDims(groupMipDims);
            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            CheckAttribute(group3dFl, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_0, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_1, "MinIntensity", double(1));
            CheckAttribute(group3dFl, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_0, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_1, "MaxIntensity", double(2));
            CheckAttribute(groupMipFl, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_0, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_1, "MinIntensity", double(3));
            CheckAttribute(groupMipFl, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_0, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_1, "MaxIntensity", double(4));

            CheckAttribute(dataSet3d_0, "PositionX", double(5));
            CheckAttribute(dataSet3d_1, "PositionX", double(5));
            CheckAttribute(dataSetMip_0, "PositionX", double(5));
            CheckAttribute(dataSetMip_1, "PositionX", double(5));
            CheckAttribute(dataSet3d_0, "PositionY", double(6));
            CheckAttribute(dataSet3d_1, "PositionY", double(6));
            CheckAttribute(dataSetMip_0, "PositionY", double(6));
            CheckAttribute(dataSetMip_1, "PositionY", double(6));
            CheckAttribute(dataSet3d_0, "PositionZ", double(7));
            CheckAttribute(dataSet3d_1, "PositionZ", double(7));
            CheckAttribute(dataSetMip_0, "PositionZ", double(7));
            CheckAttribute(dataSetMip_1, "PositionZ", double(7));
            CheckAttribute(dataSet3d_0, "PositionC", double(8));
            CheckAttribute(dataSet3d_1, "PositionC", double(8));
            CheckAttribute(dataSetMip_0, "PositionC", double(8));
            CheckAttribute(dataSetMip_1, "PositionC", double(8));

            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");

            CheckAttribute(group3dFl, "Channels", int64_t(10));
            CheckAttribute(groupMipFl, "Channels", int64_t(10));

            CheckAttribute(group3dFl, "OffsetZ", double(11));
            CheckAttribute(group3dFl, "OffsetZCompensation", double(12));

            CheckAttribute(group3dFl, "DataCount", int64_t(13));
            CheckAttribute(groupMipFl, "DataCount", int64_t(13));

            CheckAttribute(group3dFl, "ResolutionX", double(14));
            CheckAttribute(groupMipFl, "ResolutionX", double(14));
            CheckAttribute(group3dFl, "ResolutionY", double(15));
            CheckAttribute(groupMipFl, "ResolutionY", double(15));
            CheckAttribute(group3dFl, "ResolutionZ", double(16));
            CheckAttribute(group3dFl, "TimeInterval", double(18));

            group3dFl.close();
            groupMipFl.close();
            group3dFlSub.close();
            groupMipFlSub.close();
            dataSet3d_0.close();
            dataSetMip_0.close();
            dataSet3d_1.close();
            dataSetMip_1.close();
            file.close();
        }

        SECTION("Write FL Green") {
            auto hostData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3d[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMip[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLGREEN, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo1 =
                    GenerateDataInfo(ImageType::FLGREEN, rawData, 1,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo1, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }


            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == false);
            CHECK(file.exists("Data/2DMIP") == false);
            CHECK(file.exists("Data/3DFL") == true);
            CHECK(file.exists("Data/2DFLMIP") == true);
            CHECK(file.exists("Data/3DFL/CH0") == false);
            CHECK(file.exists("Data/2DFLMIP/CH0") == false);
            CHECK(file.exists("Data/3DFL/CH1") == true);
            CHECK(file.exists("Data/2DFLMIP/CH1") == true);
            CHECK(file.exists("Data/3DFL/CH2") == false);
            CHECK(file.exists("Data/2DFLMIP/CH2") == false);
            CHECK(file.exists("Data/2D") == false);
            CHECK(file.exists("Data/BF") == false);


            auto group3dFl = file.openGroup("Data/3DFL");
            auto groupMipFl = file.openGroup("Data/2DFLMIP");

            auto group3dFlSub = group3dFl.openGroup("CH1");
            auto groupMipFlSub = groupMipFl.openGroup("CH1");

            CHECK(group3dFlSub.exists("000000") == true);
            CHECK(groupMipFlSub.exists("000000") == true);
            CHECK(group3dFlSub.exists("000001") == true);
            CHECK(groupMipFlSub.exists("000001") == true);

            auto dataSet3d_0 = group3dFlSub.openDataSet("000000");
            auto dataSetMip_0 = groupMipFlSub.openDataSet("000000");
            auto dataSet3d_1 = group3dFlSub.openDataSet("000001");
            auto dataSetMip_1 = groupMipFlSub.openDataSet("000001");

            auto outputData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto outputDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());
            dataSet3d_0.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            auto difference3d = false;
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);

            dataSet3d_1.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);


            dataSetMip_0.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            auto differenceMip = false;
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            dataSetMip_1.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            hsize_t group3dDims[3]{ 0,0,0 }, groupMipDims[2]{ 0,0 };
            dataSet3d_0.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_0.getSpace().getSimpleExtentDims(groupMipDims);

            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            dataSet3d_1.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_1.getSpace().getSimpleExtentDims(groupMipDims);
            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            CheckAttribute(group3dFl, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_0, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_1, "MinIntensity", double(1));
            CheckAttribute(group3dFl, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_0, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_1, "MaxIntensity", double(2));
            CheckAttribute(groupMipFl, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_0, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_1, "MinIntensity", double(3));
            CheckAttribute(groupMipFl, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_0, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_1, "MaxIntensity", double(4));

            CheckAttribute(dataSet3d_0, "PositionX", double(5));
            CheckAttribute(dataSet3d_1, "PositionX", double(5));
            CheckAttribute(dataSetMip_0, "PositionX", double(5));
            CheckAttribute(dataSetMip_1, "PositionX", double(5));
            CheckAttribute(dataSet3d_0, "PositionY", double(6));
            CheckAttribute(dataSet3d_1, "PositionY", double(6));
            CheckAttribute(dataSetMip_0, "PositionY", double(6));
            CheckAttribute(dataSetMip_1, "PositionY", double(6));
            CheckAttribute(dataSet3d_0, "PositionZ", double(7));
            CheckAttribute(dataSet3d_1, "PositionZ", double(7));
            CheckAttribute(dataSetMip_0, "PositionZ", double(7));
            CheckAttribute(dataSetMip_1, "PositionZ", double(7));
            CheckAttribute(dataSet3d_0, "PositionC", double(8));
            CheckAttribute(dataSet3d_1, "PositionC", double(8));
            CheckAttribute(dataSetMip_0, "PositionC", double(8));
            CheckAttribute(dataSetMip_1, "PositionC", double(8));

            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");

            CheckAttribute(group3dFl, "Channels", int64_t(10));
            CheckAttribute(groupMipFl, "Channels", int64_t(10));

            CheckAttribute(group3dFl, "OffsetZ", double(11));
            CheckAttribute(group3dFl, "OffsetZCompensation", double(12));

            CheckAttribute(group3dFl, "DataCount", int64_t(13));
            CheckAttribute(groupMipFl, "DataCount", int64_t(13));

            CheckAttribute(group3dFl, "ResolutionX", double(14));
            CheckAttribute(groupMipFl, "ResolutionX", double(14));
            CheckAttribute(group3dFl, "ResolutionY", double(15));
            CheckAttribute(groupMipFl, "ResolutionY", double(15));
            CheckAttribute(group3dFl, "ResolutionZ", double(16));
            CheckAttribute(group3dFl, "TimeInterval", double(18));

            group3dFl.close();
            groupMipFl.close();
            group3dFlSub.close();
            groupMipFlSub.close();
            dataSet3d_0.close();
            dataSetMip_0.close();
            dataSet3d_1.close();
            dataSetMip_1.close();
            file.close();
        }

        SECTION("Write FL Red") {
            auto hostData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3d[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMip[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLRED, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;
                rawData.dataMip = hostDataMip;

                auto dataInfo1 =
                    GenerateDataInfo(ImageType::FLRED, rawData, 1,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo1, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }


            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == false);
            CHECK(file.exists("Data/2DMIP") == false);
            CHECK(file.exists("Data/3DFL") == true);
            CHECK(file.exists("Data/2DFLMIP") == true);
            CHECK(file.exists("Data/3DFL/CH0") == false);
            CHECK(file.exists("Data/2DFLMIP/CH0") == false);
            CHECK(file.exists("Data/3DFL/CH1") == false);
            CHECK(file.exists("Data/2DFLMIP/CH1") == false);
            CHECK(file.exists("Data/3DFL/CH2") == true);
            CHECK(file.exists("Data/2DFLMIP/CH2") == true);
            CHECK(file.exists("Data/2D") == false);
            CHECK(file.exists("Data/BF") == false);


            auto group3dFl = file.openGroup("Data/3DFL");
            auto groupMipFl = file.openGroup("Data/2DFLMIP");

            auto group3dFlSub = group3dFl.openGroup("CH2");
            auto groupMipFlSub = groupMipFl.openGroup("CH2");

            CHECK(group3dFlSub.exists("000000") == true);
            CHECK(groupMipFlSub.exists("000000") == true);
            CHECK(group3dFlSub.exists("000001") == true);
            CHECK(groupMipFlSub.exists("000001") == true);

            auto dataSet3d_0 = group3dFlSub.openDataSet("000000");
            auto dataSetMip_0 = groupMipFlSub.openDataSet("000000");
            auto dataSet3d_1 = group3dFlSub.openDataSet("000001");
            auto dataSetMip_1 = groupMipFlSub.openDataSet("000001");

            auto outputData3d = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto outputDataMip = std::shared_ptr<uint16_t[]>(new uint16_t[6]());
            dataSet3d_0.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            auto difference3d = false;
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);

            dataSet3d_1.read(outputData3d.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 24; ++i) {
                if (outputData3d.get()[i] != hostData3d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);


            dataSetMip_0.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            auto differenceMip = false;
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            dataSetMip_1.read(outputDataMip.get(), H5::PredType::NATIVE_UINT16);
            for (auto i = 0; i < 6; ++i) {
                if (outputDataMip.get()[i] != hostDataMip.get()[i]) {
                    differenceMip = true;
                    break;
                }
            }
            CHECK(differenceMip == false);

            hsize_t group3dDims[3]{ 0,0,0 }, groupMipDims[2]{ 0,0 };
            dataSet3d_0.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_0.getSpace().getSimpleExtentDims(groupMipDims);

            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            dataSet3d_1.getSpace().getSimpleExtentDims(group3dDims);
            dataSetMip_1.getSpace().getSimpleExtentDims(groupMipDims);
            CHECK(group3dDims[0] == 4);
            CHECK(group3dDims[1] == 3);
            CHECK(group3dDims[2] == 2);
            CHECK(groupMipDims[0] == 3);
            CHECK(groupMipDims[1] == 2);

            CheckAttribute(group3dFl, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_0, "MinIntensity", double(1));
            CheckAttribute(dataSet3d_1, "MinIntensity", double(1));
            CheckAttribute(group3dFl, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_0, "MaxIntensity", double(2));
            CheckAttribute(dataSet3d_1, "MaxIntensity", double(2));
            CheckAttribute(groupMipFl, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_0, "MinIntensity", double(3));
            CheckAttribute(dataSetMip_1, "MinIntensity", double(3));
            CheckAttribute(groupMipFl, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_0, "MaxIntensity", double(4));
            CheckAttribute(dataSetMip_1, "MaxIntensity", double(4));

            CheckAttribute(dataSet3d_0, "PositionX", double(5));
            CheckAttribute(dataSet3d_1, "PositionX", double(5));
            CheckAttribute(dataSetMip_0, "PositionX", double(5));
            CheckAttribute(dataSetMip_1, "PositionX", double(5));
            CheckAttribute(dataSet3d_0, "PositionY", double(6));
            CheckAttribute(dataSet3d_1, "PositionY", double(6));
            CheckAttribute(dataSetMip_0, "PositionY", double(6));
            CheckAttribute(dataSetMip_1, "PositionY", double(6));
            CheckAttribute(dataSet3d_0, "PositionZ", double(7));
            CheckAttribute(dataSet3d_1, "PositionZ", double(7));
            CheckAttribute(dataSetMip_0, "PositionZ", double(7));
            CheckAttribute(dataSetMip_1, "PositionZ", double(7));
            CheckAttribute(dataSet3d_0, "PositionC", double(8));
            CheckAttribute(dataSet3d_1, "PositionC", double(8));
            CheckAttribute(dataSetMip_0, "PositionC", double(8));
            CheckAttribute(dataSetMip_1, "PositionC", double(8));

            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");
            CheckAttribute(dataSetMip_0, "RecordingTime", "9");

            CheckAttribute(group3dFl, "Channels", int64_t(10));
            CheckAttribute(groupMipFl, "Channels", int64_t(10));

            CheckAttribute(group3dFl, "OffsetZ", double(11));
            CheckAttribute(group3dFl, "OffsetZCompensation", double(12));

            CheckAttribute(group3dFl, "DataCount", int64_t(13));
            CheckAttribute(groupMipFl, "DataCount", int64_t(13));

            CheckAttribute(group3dFl, "ResolutionX", double(14));
            CheckAttribute(groupMipFl, "ResolutionX", double(14));
            CheckAttribute(group3dFl, "ResolutionY", double(15));
            CheckAttribute(groupMipFl, "ResolutionY", double(15));
            CheckAttribute(group3dFl, "ResolutionZ", double(16));
            CheckAttribute(group3dFl, "TimeInterval", double(18));

            group3dFl.close();
            groupMipFl.close();
            group3dFlSub.close();
            groupMipFlSub.close();
            dataSet3d_0.close();
            dataSetMip_0.close();
            dataSet3d_1.close();
            dataSetMip_1.close();
            file.close();
        }

        SECTION("Write Phase") {
            auto hostData2d = std::shared_ptr<float[]>(new float[6]());

            for (auto i = 0; i < 6; ++i) { hostData2d[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData2d;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::PHASE, rawData, 0,
                        DataInfo::Sizes{ 2,3,0,2 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == false);
            CHECK(file.exists("Data/2DMIP") == false);
            CHECK(file.exists("Data/3DFL") == false);
            CHECK(file.exists("Data/2DFLMIP") == false);
            CHECK(file.exists("Data/2D") == true);
            CHECK(file.exists("Data/BF") == false);


            auto group2d = file.openGroup("Data/2D");

            CHECK(group2d.exists("000000") == true);

            auto dataSet2d = group2d.openDataSet("000000");

            auto outputData2d = std::shared_ptr<float[]>(new float[12]());
            dataSet2d.read(outputData2d.get(), H5::PredType::NATIVE_FLOAT);
            auto difference3d = false;
            for (auto i = 0; i < 6; ++i) {
                if (outputData2d.get()[i] != hostData2d.get()[i]) {
                    difference3d = true;
                    break;
                }
            }
            CHECK(difference3d == false);

            hsize_t group2dDims[3]{ 0,0,0 };
            dataSet2d.getSpace().getSimpleExtentDims(group2dDims);

            CHECK(group2dDims[0] == 2);
            CHECK(group2dDims[1] == 3);
            CHECK(group2dDims[2] == 2);

            CheckAttribute(group2d, "SizeX", int64_t(2));
            CheckAttribute(group2d, "SizeY", int64_t(3));
            CheckAttribute(group2d, "SizeT", int64_t(2));

            CheckAttribute(group2d, "MinPhase", double(1));
            CheckAttribute(dataSet2d, "MinPhase", double(1));
            CheckAttribute(group2d, "MaxPhase", double(2));
            CheckAttribute(dataSet2d, "MaxPhase", double(2));

            CheckAttribute(dataSet2d, "PositionX", double(5));
            CheckAttribute(dataSet2d, "PositionY", double(6));
            CheckAttribute(dataSet2d, "PositionZ", double(7));
            CheckAttribute(dataSet2d, "PositionC", double(8));

            CheckAttribute(dataSet2d, "RecordingTime", "9");
            CheckAttribute(dataSet2d, "RecordingTime", "9");

            CheckAttribute(group2d, "DataCount", int64_t(13));

            CheckAttribute(group2d, "ResolutionX", double(14));
            CheckAttribute(group2d, "ResolutionY", double(15));
            CheckAttribute(group2d, "ResolutionT", double(17));
            CheckAttribute(group2d, "TimeInterval", double(18));

            group2d.close();
            dataSet2d.close();
            file.close();
        }

        SECTION("Write BF") {
            auto hostData3d = std::shared_ptr<uint8_t[]>(new uint8_t[18]());

            for (auto i = 0; i < 6; ++i) { hostData3d[i] = uint8_t(255); }
            for (auto i = 6; i < 12; ++i) { hostData3d[i] = uint8_t(51); }
            for (auto i = 12; i < 18; ++i) { hostData3d[i] = uint8_t(153); }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::BF, rawData, 0,
                        DataInfo::Sizes{ 2,3,0,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3d;

                auto dataInfo1 =
                    GenerateDataInfo(ImageType::BF, rawData, 1,
                        DataInfo::Sizes{ 2,3,0,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo1, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }


            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == false);
            CHECK(file.exists("Data/2DMIP") == false);
            CHECK(file.exists("Data/3DFL") == false);
            CHECK(file.exists("Data/2DFLMIP") == false);
            CHECK(file.exists("Data/2D") == false);
            CHECK(file.exists("Data/BF") == true);


            auto groupBf = file.openGroup("Data/BF");

            CHECK(groupBf.exists("000000") == true);
            CHECK(groupBf.exists("000001") == true);

            auto dataSet3d_0 = groupBf.openDataSet("000000");
            auto dataSet3d_1 = groupBf.openDataSet("000001");

            auto outputData3d_0 = std::shared_ptr<uint8_t[]>(new uint8_t[18]());
            auto outputData3d_1 = std::shared_ptr<uint8_t[]>(new uint8_t[18]());
            dataSet3d_0.read(outputData3d_0.get(), H5::PredType::NATIVE_UINT8);
            dataSet3d_1.read(outputData3d_1.get(), H5::PredType::NATIVE_UINT8);
            auto difference3d_0 = false;
            auto difference3d_1 = false;
            for (auto i = 0; i < 18; ++i) {
                if (i < 6) {
                    if (outputData3d_0.get()[i] != 255) {
                        difference3d_0 = true;
                        break;
                    }
                } else if ((i >= 6) && (i < 12)) {
                    if (outputData3d_0.get()[i] != 51) {
                        difference3d_0 = true;
                        break;
                    }
                } else if ((i >= 12) && (i < 18)) {
                    if (outputData3d_0.get()[i] != 153) {
                        difference3d_0 = true;
                        break;
                    }
                }
            }
            for (auto i = 0; i < 18; ++i) {
                if (i < 6) {
                    if (outputData3d_1.get()[i] != 255) {
                        difference3d_1 = true;
                        break;
                    }
                } else if ((i >= 6) && (i < 12)) {
                    if (outputData3d_1.get()[i] != 51) {
                        difference3d_1 = true;
                        break;
                    }
                } else if ((i >= 12) && (i < 18)) {
                    if (outputData3d_1.get()[i] != 153) {
                        difference3d_1 = true;
                        break;
                    }
                }
            }
            CHECK(difference3d_0 == false);
            CHECK(difference3d_1 == false);

            hsize_t group3dDims_0[3]{ 0,0,0 }, group3dDims_1[3]{ 0,0,0 };
            dataSet3d_0.getSpace().getSimpleExtentDims(group3dDims_0);
            dataSet3d_1.getSpace().getSimpleExtentDims(group3dDims_1);

            CHECK(group3dDims_0[0] == 3);
            CHECK(group3dDims_0[1] == 3);
            CHECK(group3dDims_0[2] == 2);

            CHECK(group3dDims_1[0] == 3);
            CHECK(group3dDims_1[1] == 3);
            CHECK(group3dDims_1[2] == 2);

            CheckAttribute(groupBf, "SizeX", int64_t(2));
            CheckAttribute(groupBf, "SizeY", int64_t(3));

            CheckAttribute(dataSet3d_0, "PositionX", double(5));
            CheckAttribute(dataSet3d_1, "PositionX", double(5));
            CheckAttribute(dataSet3d_0, "PositionY", double(6));
            CheckAttribute(dataSet3d_1, "PositionY", double(6));
            CheckAttribute(dataSet3d_0, "PositionZ", double(7));
            CheckAttribute(dataSet3d_1, "PositionZ", double(7));
            CheckAttribute(dataSet3d_0, "PositionC", double(8));
            CheckAttribute(dataSet3d_1, "PositionC", double(8));

            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_1, "RecordingTime", "9");
            CheckAttribute(dataSet3d_0, "RecordingTime", "9");
            CheckAttribute(dataSet3d_1, "RecordingTime", "9");

            CheckAttribute(groupBf, "DataCount", int64_t(13));

            CheckAttribute(groupBf, "ResolutionX", double(14));
            CheckAttribute(groupBf, "ResolutionY", double(15));
            CheckAttribute(groupBf, "TimeInterval", double(18));

            groupBf.close();
            dataSet3d_0.close();
            dataSet3d_1.close();
            file.close();
        }

        SECTION("Mutiple Image Types") {
            auto hostData3dHt = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMipHt = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3dHt[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMipHt[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dHt;
                rawData.dataMip = hostDataMipHt;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::HT, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }

            auto hostData3dFl = std::shared_ptr<uint16_t[]>(new uint16_t[24]());
            auto hostDataMipFl = std::shared_ptr<uint16_t[]>(new uint16_t[6]());

            for (auto i = 0; i < 24; ++i) { hostData3dFl[i] = i; }
            for (auto i = 0; i < 6; ++i) { hostDataMipFl[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dFl;
                rawData.dataMip = hostDataMipFl;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLBLUE, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dFl;
                rawData.dataMip = hostDataMipFl;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLGREEN, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dFl;
                rawData.dataMip = hostDataMipFl;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::FLRED, rawData, 0,
                        DataInfo::Sizes{ 2,3,4,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }

            auto hostData3dPhase = std::shared_ptr<float[]>(new float[6]());

            for (auto i = 0; i < 6; ++i) { hostData3dPhase[i] = i; }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dPhase;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::PHASE, rawData, 0,
                        DataInfo::Sizes{ 2,3,0,2 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }

            auto hostData3dBf = std::shared_ptr<uint8_t[]>(new uint8_t[18]());

            for (auto i = 0; i < 6; ++i) { hostData3dBf[i] = uint8_t(255); }
            for (auto i = 6; i < 12; ++i) { hostData3dBf[i] = uint8_t(51); }
            for (auto i = 12; i < 18; ++i) { hostData3dBf[i] = uint8_t(153); }
            {
                TCFWritingAlgorithm algorithm;
                const TCFMetaInfo dummyMetaInfo;

                DataInfo::RawData rawData;
                rawData.data3d = hostData3dBf;

                auto dataInfo0 =
                    GenerateDataInfo(ImageType::BF, rawData, 0,
                        DataInfo::Sizes{ 2,3,0,0 });

                TCFWritingData data;
                data.SetInfo(dataInfo0, dummyMetaInfo);
                algorithm.Write(QString::fromStdString(tempFilePath), data);
            }

            H5::H5File file(tempFilePath, H5F_ACC_RDONLY);
            CHECK(file.exists("Data/3D") == true);
            CHECK(file.exists("Data/2DMIP") == true);
            CHECK(file.exists("Data/3DFL") == true);
            CHECK(file.exists("Data/2DFLMIP") == true);
            CHECK(file.exists("Data/3DFL/CH0") == true);
            CHECK(file.exists("Data/2DFLMIP/CH0") == true);
            CHECK(file.exists("Data/3DFL/CH1") == true);
            CHECK(file.exists("Data/2DFLMIP/CH1") == true);
            CHECK(file.exists("Data/3DFL/CH2") == true);
            CHECK(file.exists("Data/2DFLMIP/CH2") == true);
            CHECK(file.exists("Data/2D") == true);
            CHECK(file.exists("Data/BF") == true);

            file.close();
        }

        const auto fileIsRemoved = std::remove(tempFilePath.c_str()) == 0;
        CHECK(fileIsRemoved);
    }
}
