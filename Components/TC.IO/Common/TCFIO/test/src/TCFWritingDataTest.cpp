#include <catch2/catch.hpp>
#include "TCFWritingData.h"

namespace _TCFWritingDataTest {
    using namespace TC::IO;

    TEST_CASE("TCFWritingData") {
        TCFWritingData tcfWritingData;

        SECTION("GetDataInfo()") {
            DataInfo dataInfo;
            dataInfo.SetChannels(1);

            TCFMetaInfo dummyMetaInfo;

            tcfWritingData.SetInfo(dataInfo, dummyMetaInfo);
            
            const auto outputDataInfo = tcfWritingData.GetDataInfo();
            CHECK(outputDataInfo.GetChannels() == 1);
        }

        SECTION("GetMetaInfo") {
            DataInfo dummyDataInfo;

            TCFMetaInfo metaInfo;
            TCFMetaInfo::Tile tile;
            tile.tileSizes.tileNumber = 1;
            metaInfo.SetTile(tile);

            tcfWritingData.SetInfo(dummyDataInfo, metaInfo);

            const auto outputMetaInfo = tcfWritingData.GetMetaInfo();
            CHECK(outputMetaInfo.GetTile().tileSizes.tileNumber == 1);
        }
    }
}


