#include <catch2/catch.hpp>
#include "DataInfo.h"

namespace _DataInfoTest {
    using namespace TC::IO;

    TEST_CASE("DataInfo") {
        DataInfo dataInfo;
        SECTION("SetRawData() / GetRawData()") {
            std::shared_ptr<uint16_t[]> data3d(new uint16_t[10]());
            std::shared_ptr<uint16_t[]> dataMip(new uint16_t[10]());

            DataInfo::RawData rawData;
            rawData.data3d = data3d;
            rawData.dataMip = dataMip;

            dataInfo.SetRawData(rawData);

            const auto outputRawData = dataInfo.GetRawData();

            const auto outputData3d = 
                *outputRawData.data3d._Cast<std::shared_ptr<uint16_t[]>>();
            const auto outputDataMip = 
                *outputRawData.dataMip._Cast<std::shared_ptr<uint16_t[]>>();

            CHECK(outputData3d.get() == data3d.get());
            CHECK(outputDataMip.get() == dataMip.get());
        }

        SECTION("SetImageType() / GetImageType()") {
            ImageType imageType = ImageType::BF;

            dataInfo.SetImageType(imageType);

            const auto outputImageType = dataInfo.GetImageType();

            CHECK(outputImageType._value == ImageType::BF);
        }

        SECTION("SetMinMax3d() / GetMinMax3d()") {
            DataInfo::MinMax minMax3d;
            minMax3d.max = 1;
            minMax3d.min = 2;

            dataInfo.SetMinMax3d(minMax3d);

            const auto outputMinMax = dataInfo.GetMinMax3d();

            CHECK(outputMinMax.max == 1);
            CHECK(outputMinMax.min == 2);
        }

        SECTION("SetMinMaxMip() / GetMinMaxMip()") {
            DataInfo::MinMax minMaxMip;
            minMaxMip.max = 1;
            minMaxMip.min = 2;

            dataInfo.SetMinMaxMip(minMaxMip);

            const auto outputMinMax = dataInfo.GetMinMaxMip();

            CHECK(outputMinMax.max == 1);
            CHECK(outputMinMax.min == 2);
        }

        SECTION("SetPositions() / GetPositions()") {
            DataInfo::Positions positions;
            positions.positionC = 1;
            positions.positionX = 2;
            positions.positionY = 3;
            positions.positionZ = 4;

            dataInfo.SetPositions(positions);

            const auto outputPositions = dataInfo.GetPositions();

            CHECK(outputPositions.positionC == 1);
            CHECK(outputPositions.positionX == 2);
            CHECK(outputPositions.positionY == 3);
            CHECK(outputPositions.positionZ == 4);
        }

        SECTION("SetRecordingTime() / GetRecordingTime()") {
            const QString recordingTime = "recordingTime";

            dataInfo.SetRecordingTime(recordingTime);

            const auto outputRecordingTime = dataInfo.GetRecordingTime();

            CHECK(outputRecordingTime == recordingTime );
        }

        SECTION("SetChannels() / GetChannels()") {
            const int64_t channels = 3;

            dataInfo.SetChannels(channels);

            const auto outputChannels = dataInfo.GetChannels();

            CHECK(outputChannels == channels);
        }

        SECTION("SetOffsetZs() / GetOffsetZs()") {
            DataInfo::OffsetZs offsetZs;
            offsetZs.offsetZ = 1;
            offsetZs.offsetZCompensation = 2;

            dataInfo.SetOffsetZs(offsetZs);

            const auto outputOffsetZs = dataInfo.GetOffsetZs();

            CHECK(outputOffsetZs.offsetZ == offsetZs.offsetZ);
            CHECK(outputOffsetZs.offsetZCompensation == offsetZs.offsetZCompensation);
        }

        SECTION("SetDataCount() / GetDataCount()") {
            const int64_t dataCount = 10;

            dataInfo.SetDataCount(dataCount);

            const auto outputDataCount = dataInfo.GetDataCount();

            CHECK(outputDataCount == dataCount);
        }

        SECTION("SetResolutions() / GetResolutions()") {
            DataInfo::Resolutions resolutions;
            resolutions.resolutionT = 1;
            resolutions.resolutionX = 2;
            resolutions.resolutionY = 3;
            resolutions.resolutionZ = 4;

            dataInfo.SetResolutions(resolutions);

            const auto outputResolution = dataInfo.GetResolutions();

            CHECK(outputResolution.resolutionT == 1);
            CHECK(outputResolution.resolutionX == 2);
            CHECK(outputResolution.resolutionY == 3);
            CHECK(outputResolution.resolutionZ == 4);
        }

        SECTION("SetSizes() / GetSizes()") {
            DataInfo::Sizes sizes;
            sizes.sizeT = 1;
            sizes.sizeX = 2;
            sizes.sizeY = 3;
            sizes.sizeZ = 4;

            dataInfo.SetSizes(sizes);

            const auto outputSizes = dataInfo.GetSizes();

            CHECK(outputSizes.sizeT == 1);
            CHECK(outputSizes.sizeX == 2);
            CHECK(outputSizes.sizeY == 3);
            CHECK(outputSizes.sizeZ == 4);
        }

        SECTION("SetTimeInterval() / GetTimeIntervalInSecs()") {
            const double timeInterval = 1;

            dataInfo.SetTimeInterval(timeInterval);

            const auto outputTimeInterval = dataInfo.GetTimeIntervalInSecs();

            CHECK(outputTimeInterval == timeInterval);
        }

        SECTION("SetTimelapseIndex() / GetTimelapseIndex()") {
            const uint32_t timelapseIndex = 1;

            dataInfo.SetTimelapseIndex(timelapseIndex);

            const auto outputTimelapseIndex = dataInfo.GetTimelapseIndex();

            CHECK(outputTimelapseIndex == timelapseIndex);
        }
    }
}
