#include <catch2/catch.hpp>

#include "TCFMetaReader.h"

namespace TCFMetaReaderTest {
    TEST_CASE("TCFMetaReader : unit test") {
        //TODO Implement unit test
    }
    TEST_CASE("TCFMetaReader : practical test") {
        SECTION("Read()") {
            SECTION("FL Channel 3") {
                TC::IO::TCFMetaReader reader;
                const QString tcfFilePath = "F:/TempFdrive/DataPath/admin/adpro/adExp/221021.155344.adExp.002.Group1.A1.S002/221021.155344.adExp.002.Group1.A1.S002.TCF";
                const auto metaResult = reader.Read(tcfFilePath);

                {
                    const auto result3DFLChannelList = metaResult->data.data3DFL.channelList;

                    CHECK(result3DFLChannelList.size() == 3);
                    CHECK(result3DFLChannelList[0] == 0);
                    CHECK(result3DFLChannelList[1] == 1);
                    CHECK(result3DFLChannelList[2] == 2);
                }
                {
                    const auto result2DFLMIPChannelList = metaResult->data.data2DFLMIP.channelList;

                    CHECK(result2DFLMIPChannelList.size() == 3);
                    CHECK(result2DFLMIPChannelList[0] == 0);
                    CHECK(result2DFLMIPChannelList[1] == 1);
                    CHECK(result2DFLMIPChannelList[2] == 2);
                }
            }

            SECTION("FL Channel 1") {
                TC::IO::TCFMetaReader reader;
                const QString tcfFilePath = "F:/TempFdrive/DataPath/admin/adpro/adExp/221021.154503.adExp.001.Group1.A1.S001/221021.154503.adExp.001.Group1.A1.S001.TCF";
                reader.Read(tcfFilePath);

                const auto metaResult = reader.Read(tcfFilePath);

                {
                    const auto result3DFLChannelList = metaResult->data.data3DFL.channelList;

                    CHECK(result3DFLChannelList.size() == 1);
                    CHECK(result3DFLChannelList[0] == 2);
                }
                {
                    const auto result2DFLMIPChannelList = metaResult->data.data2DFLMIP.channelList;

                    CHECK(result2DFLMIPChannelList.size() == 1);
                    CHECK(result2DFLMIPChannelList[0] == 2);
                }
            }

            SECTION("Thumbnail") {
                SECTION("data including Thumbnail") {
                    TC::IO::TCFMetaReader reader;
                    const QString tcfFilePath = "E:/00_Data/20221110 TSX v1.3.2a Verification/20221108 HT FL 3x3/20221108 HT FL 3x3.TCF";
                    const auto metaResult = reader.Read(tcfFilePath);

                    CHECK(metaResult->thumbnail.ht.exist == true);
                    CHECK(metaResult->thumbnail.ht.isColor == false);
                    CHECK(metaResult->thumbnail.ht.dataCount == 1);
                    CHECK(metaResult->thumbnail.ht.resolutionX == 0.4394477903842926);
                    CHECK(metaResult->thumbnail.ht.resolutionY == 0.4394477903842926);
                    CHECK(metaResult->thumbnail.ht.sizeX == 1024);
                    CHECK(metaResult->thumbnail.ht.sizeY == 1024);

                    CHECK(metaResult->thumbnail.fl.exist == true);
                    CHECK(metaResult->thumbnail.fl.isColor == true);
                    CHECK(metaResult->thumbnail.fl.dataCount == 1);
                    CHECK(metaResult->thumbnail.fl.resolutionX == 0.439453125);
                    CHECK(metaResult->thumbnail.fl.resolutionY == 0.439453125);
                    CHECK(metaResult->thumbnail.fl.sizeX == 1024);
                    CHECK(metaResult->thumbnail.fl.sizeY == 1024);

                    CHECK(metaResult->thumbnail.bf.exist == false);
                }
            }
        }

    }
}