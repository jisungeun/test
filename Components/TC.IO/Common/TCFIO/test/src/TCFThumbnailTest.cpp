#include <catch2/catch.hpp>

#include "TCFThumbnail.h"

using namespace TC::TCFIO;

namespace TCFThumbnailTest {
    TEST_CASE("TCFThumbnail : unit test") {
        SECTION("TCFThumbnail(rhs)") {
            TCFThumbnail srcThumbnail;
            srcThumbnail.SetColorFlag(true);

            const TCFThumbnail destThumbnail(srcThumbnail);
            CHECK(destThumbnail.GetColorFlag() == true);
        }

        SECTION("operator=()") {
            TCFThumbnail srcThumbnail;
            srcThumbnail.SetColorFlag(true);

            TCFThumbnail destThumbnail;
            destThumbnail = srcThumbnail;
            CHECK(destThumbnail.GetColorFlag() == true);
        }

        SECTION("SetData(), GetData()") {
            const std::shared_ptr<uint8_t[]> data{ new uint8_t[1] };
            data.get()[0] = 1;

            TCFThumbnail thumbnail;
            thumbnail.SetData(data);

            CHECK(thumbnail.GetData().get()[0] == 1);
        }

        SECTION("SetSize(), GetSizeX(), GetSizeY(), GetSizeZ()") {
            TCFThumbnail thumbnail;
            thumbnail.SetSize(1, 2, 3);

            CHECK(thumbnail.GetSizeX() == 1);
            CHECK(thumbnail.GetSizeY() == 2);
            CHECK(thumbnail.GetSizeZ() == 3);
        }

        SECTION("SetResolution(), GetResolutionX(), GetResolutionY()") {
            TCFThumbnail thumbnail;
            thumbnail.SetResolution(1, 2);
            CHECK(thumbnail.GetResolutionX() == 1);
            CHECK(thumbnail.GetResolutionY() == 2);
        }

        SECTION("SetColorFlag(), GetColorFlag()") {
            TCFThumbnail thumbnail;
            thumbnail.SetColorFlag(true);
            CHECK(thumbnail.GetColorFlag() == true);
        }
    }

    TEST_CASE("TCFThumbnail : practical test") {
        const std::shared_ptr<uint8_t[]> data{ new uint8_t[1] };
        data.get()[0] = 1;

        TCFThumbnail srcThumbnail;
        srcThumbnail.SetData(data);
        srcThumbnail.SetSize(1, 2, 3);
        srcThumbnail.SetResolution(3, 4);
        srcThumbnail.SetColorFlag(true);

        TCFThumbnail destThumbnail = srcThumbnail;

        CHECK(destThumbnail.GetData().get()[0] == 1);
        CHECK(destThumbnail.GetSizeX() == 1);
        CHECK(destThumbnail.GetSizeY() == 2);
        CHECK(destThumbnail.GetSizeZ() == 3);
        CHECK(destThumbnail.GetResolutionX() == 3);
        CHECK(destThumbnail.GetResolutionY() == 4);
        CHECK(destThumbnail.GetColorFlag() == true);
    }
}
