#include <catch2/catch.hpp>
#include "TCFMetaInfo.h"

namespace _TCFMetaInfoTest {
    using namespace TC::IO;

    TEST_CASE("TCFMetaInfo") {
        TCFMetaInfo metaInfo;

        SECTION("SetCommon() / GetCommon()") {
            TCFMetaInfo::Common common;
            common.devices.deviceSerial = "1";
            common.devices.deviceSoftwareVersion = "2";
            common.devices.deviceHost = "3";
            common.ids.uniqueID = "4";
            common.ids.userID = "5";
            common.ids.dataID = "6";
            common.formatVersion = "7";
            common.softwareVersion = "8";
            common.createDate = "9";
            common.description = "10";
            common.recordingTime = "11";
            common.title = "12";

            metaInfo.SetCommon(common);

            const auto outputCommon = metaInfo.GetCommon();
            CHECK(outputCommon.devices.deviceSerial == "1");
            CHECK(outputCommon.devices.deviceSoftwareVersion == "2");
            CHECK(outputCommon.devices.deviceHost == "3");
            CHECK(outputCommon.ids.uniqueID == "4");
            CHECK(outputCommon.ids.userID == "5");
            CHECK(outputCommon.ids.dataID == "6");
            CHECK(outputCommon.formatVersion == "7");
            CHECK(outputCommon.softwareVersion == "8");
            CHECK(outputCommon.createDate == "9");
            CHECK(outputCommon.description == "10");
            CHECK(outputCommon.recordingTime == "11");
            CHECK(outputCommon.title == "12");
        }

        SECTION("SetAnnotation() / GetAnnotation()") {
            TCFMetaInfo::Annotation annotation;

            annotation.annotations["1"] = "1";
            annotation.annotations["2"] = "2";

            metaInfo.SetAnnotation(annotation);

            const auto outputAnnotation = metaInfo.GetAnnotation();
            CHECK(outputAnnotation.annotations["1"] == "1");
            CHECK(outputAnnotation.annotations["2"] == "2");
        }

        SECTION("SetDevice() / GetDevice()") {
            TCFMetaInfo::Device device;

            device.iteration = 1;
            device.magnification = 2;
            device.na = 3;
            device.ri = 4;
            device.rawSize = 5;
            device.waveLength = 6;
            device.zps.zp = 7;
            device.zps.zp2 = 8;
            device.zps.zp3 = 9;

            metaInfo.SetDevice(device);

            const auto outputDevice = metaInfo.GetDevice();
            CHECK(device.iteration == 1);
            CHECK(device.magnification == 2);
            CHECK(device.na == 3);
            CHECK(device.ri == 4);
            CHECK(device.rawSize == 5);
            CHECK(device.waveLength == 6);
            CHECK(device.zps.zp == 7);
            CHECK(device.zps.zp2 == 8);
            CHECK(device.zps.zp3 == 9);
        }

        SECTION("SetImaging() / GetImaging()") {
            TCFMetaInfo::Imaging imaging;

            imaging.cameraGain = 1;
            imaging.cameraShutter = 2;

            metaInfo.SetImaging(imaging);

            const auto outputImaging = metaInfo.GetImaging();
            CHECK(outputImaging.cameraGain == 1);
            CHECK(outputImaging.cameraShutter == 2);
        }

        SECTION("SetTile() / GetTile()") {
            TCFMetaInfo::Tile tile;

            tile.tilePositions.columnIndex = 1;
            tile.tilePositions.rowIndex = 2;
            tile.overlapLengths.horizontalLengthInMicroMeter = 3;
            tile.overlapLengths.verticalLengthInMicroMeter = 4;
            tile.tileSizes.tileNumber = 5;
            tile.tileSizes.tileHorizontal = 6;
            tile.tileSizes.tileVertical = 7;
            tile.positions.x = 8;
            tile.positions.y = 9;
            tile.positions.z = 10;
            tile.positions.c = 11;

            metaInfo.SetTile(tile);

            const auto outputTile = metaInfo.GetTile();
            CHECK(tile.tilePositions.columnIndex == 1);
            CHECK(tile.tilePositions.rowIndex == 2);
            CHECK(tile.overlapLengths.horizontalLengthInMicroMeter == 3);
            CHECK(tile.overlapLengths.verticalLengthInMicroMeter == 4);
            CHECK(tile.tileSizes.tileNumber == 5);
            CHECK(tile.tileSizes.tileHorizontal == 6);
            CHECK(tile.tileSizes.tileVertical == 7);
            CHECK(tile.positions.x == 8);
            CHECK(tile.positions.y == 9);
            CHECK(tile.positions.z == 10);
            CHECK(tile.positions.c == 11);
        }

        SECTION("IsTileIncluded()") {
            TCFMetaInfo::Common common;
            TCFMetaInfo::Annotation annotation;
            TCFMetaInfo::Device device;
            TCFMetaInfo::Imaging imaging;
            TCFMetaInfo::Tile tile;

            metaInfo.SetCommon(common);
            metaInfo.SetAnnotation(annotation);
            metaInfo.SetDevice(device);
            metaInfo.SetImaging(imaging);
            CHECK(metaInfo.IsTileIncluded() == false);

            metaInfo.SetTile(tile);
            CHECK(metaInfo.IsTileIncluded() == true);
        }
    }
}
