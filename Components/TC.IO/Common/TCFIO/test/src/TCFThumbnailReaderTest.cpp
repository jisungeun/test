#include <catch2/catch.hpp>

#include "TCFThumbnailReader.h"

namespace TC::TCFIO {
    TEST_CASE("TCFThumbnailReader : unit test") {
        //TODO Implement unit test
    }

    TEST_CASE("TCFThumbnailReader : practical test") {
        const QString tcfFilePath = "E:/00_Data/20221110 TSX v1.3.2a Verification/20221108 HT FL 3x3/20221108 HT FL 3x3.TCF";

        TCFThumbnailReader tcfThumbnailReader;
        tcfThumbnailReader.SetTCFFilePath(tcfFilePath);

        SECTION("HT") {
            tcfThumbnailReader.SetTarget(TCFThumbnailReader::Type::HT, 0);
            CHECK(tcfThumbnailReader.Read() == true);

            const auto resultThumbnail = tcfThumbnailReader.GetThumbnail();

            CHECK(resultThumbnail.GetColorFlag() == false);
            CHECK(resultThumbnail.GetResolutionX() == 0.4394477903842926);
            CHECK(resultThumbnail.GetResolutionY() == 0.4394477903842926);
            CHECK(resultThumbnail.GetSizeX() == 1024);
            CHECK(resultThumbnail.GetSizeY() == 1024);
            CHECK(resultThumbnail.GetSizeZ() == 1);
            CHECK(resultThumbnail.GetData().get()[0] >= 0);
        }

        SECTION("FL") {
            tcfThumbnailReader.SetTarget(TCFThumbnailReader::Type::FL, 0);
            CHECK(tcfThumbnailReader.Read() == true);

            const auto resultThumbnail = tcfThumbnailReader.GetThumbnail();

            CHECK(resultThumbnail.GetColorFlag() == true);
            CHECK(resultThumbnail.GetResolutionX() == 0.439453125);
            CHECK(resultThumbnail.GetResolutionY() == 0.439453125);
            CHECK(resultThumbnail.GetSizeX() == 1024);
            CHECK(resultThumbnail.GetSizeY() == 1024);
            CHECK(resultThumbnail.GetSizeZ() == 3);
            CHECK(resultThumbnail.GetData().get()[0] >= 0);
        }
    }
}