#include <catch2/catch.hpp>

#include "H5Cpp.h"

TEST_CASE("H5::H5File") {
    SECTION("H5F_ACC_CREAT") {
        //char path[] = "temp.h5";
        //const std::string tempFilePath = std::string(path);

        //struct stat buff;
        //const auto tempFileExists = (stat(tempFilePath.c_str(), &buff) == 0);

        //if (tempFileExists) {
        //    const auto existFileIsRemoved = std::remove(tempFilePath.c_str()) == 0;
        //    CHECK(existFileIsRemoved);
        //}

        //auto file = H5::H5File(path, H5F_ACC_CREAT); //H5F_ACC_CREAT is not supported.
        //file.close();

        //struct stat buffEnd;
        //const auto tempFileExistsEnd = (stat(tempFilePath.c_str(), &buffEnd) == 0);

        //if (tempFileExistsEnd) {
        //    const auto existFileIsRemoved = std::remove(tempFilePath.c_str()) == 0;
        //    CHECK(existFileIsRemoved);
        //}
    }
}

TEST_CASE("H5::H5Location") {
    std::string tempFilePath = "temp.h5";

    auto file = H5::H5File(tempFilePath, H5F_ACC_TRUNC);
    SECTION("exists() couldn't detect attributes") {
        const std::string attributeName = "AttributeName";
        const std::string attributeContents = "AttributeContents";

        H5::StrType strDataType(H5::PredType::C_S1, attributeContents.size());
        H5::DataSpace strDataSpace(H5S_SCALAR);

        auto attribute = file.createAttribute(attributeName, strDataType, strDataSpace);
        attribute.write(strDataType, attributeContents);

        attribute.close();
        strDataType.close();
        strDataSpace.close();

        const auto existsFunctionCouldNotDetectAttributes = !file.exists(attributeName);
        CHECK(existsFunctionCouldNotDetectAttributes);
        CHECK(file.attrExists(attributeName));
    }

    SECTION("exists() can detect group") {
        const std::string groupName = "GroupName";
        auto group = file.createGroup(groupName);
        group.close();

        CHECK(file.exists(groupName));
    }

    SECTION("exists() can detect dataset") {
        const std::string datasetName = "DataSetName";

        auto dataset = file.createDataSet(datasetName, H5::PredType::NATIVE_UINT16, H5::DataSpace(H5S_SCALAR));
        dataset.close();

        CHECK(file.exists(datasetName));
    }

    SECTION("exists() can detect group/dataset path") {
        const std::string groupName = "GroupName";
        const std::string innerGroupName = "InnerGroupName";
        const std::string innerDataSetName = "InnerDataSetName";

        hsize_t dims[3]{ 10,10,10 };

        auto group = file.createGroup(groupName);
        auto innerGroup = group.createGroup(innerGroupName);
        auto dataSet = innerGroup.createDataSet(innerDataSetName, H5::PredType::NATIVE_INT, H5::DataSpace(3, dims));
        dataSet.close();
        group.close();
        innerGroup.close();

        auto groupForReading = file.openGroup(groupName);

        CHECK(groupForReading.exists(std::string(innerGroupName + "/" + innerDataSetName)));

        groupForReading.close();
    }

    SECTION("exists() detects inner group") {
        const std::string groupName = "GroupName";
        const std::string innerGroupName = "InnerGroupName";

        auto group = file.createGroup(groupName);
        auto innerGroup = group.createGroup(innerGroupName);
        group.close();
        innerGroup.close();

        CHECK(file.exists(std::string(groupName + "/" + innerGroupName)));
        CHECK(file.exists(std::string("/" + groupName + "/" + innerGroupName)));
    }

    SECTION("createGroup() couldn't create inner group at once") {
        const std::string groupName = "GroupName";
        const std::string innerGroupName = "InnerGroupName";

        //error
        //auto innerGroup = file.createGroup(groupName + "/" + innerGroupName);
        //error
        //auto innerGroup = file.createGroup("/" + groupName + "/" + innerGroupName);
        //innerGroup.close();

        //CHECK(file.exists(std::string(groupName + "/" + innerGroupName)));
    }

    SECTION("openGroup() open inner group at once") {
        const std::string groupName = "GroupName";
        const std::string innerGroupName = "InnerGroupName";

        auto group = file.createGroup(groupName);
        auto innerGroup = group.createGroup(innerGroupName);
        group.close();
        innerGroup.close();

        auto method1 = file.openGroup(std::string(groupName + "/" + innerGroupName));
        CHECK(method1.getId() > 0);
        method1.close();
        auto method2 = file.openGroup(std::string("/" + groupName + "/" + innerGroupName));
        CHECK(method2.getId() > 0);
        method2.close();
    }



    file.close();
    const auto fileIsRemoved = std::remove(tempFilePath.c_str()) == 0;
    CHECK(fileIsRemoved);
}