#include <QFile>
#include <catch2/catch.hpp>

#include <TCMaskWriter.h>
#include <TCMaskReader.h>

using namespace TC::IO;

namespace _Test {
}

TEST_CASE("Write Mask Blobs", "[TCMaskWriter]") {
    namespace test = _Test;

    SECTION("Write and Read") {
        constexpr  char* fname = "tc_mask_writer_test_01.msk";

        QFile::remove(fname);
        TCMaskWriter writer(fname);

        BoundingBox bbox1;
        bbox1.SetOffset(0, 0, 0);
        bbox1.SetSize(10, 10, 10);
        REQUIRE(writer.WriteBlob("HT", 0, 100, bbox1, 0) == true);

        BoundingBox bbox2;
        bbox2.SetOffset(20, 30, 40);
        bbox2.SetSize(20, 30, 40);
        REQUIRE(writer.WriteBlob("HT", 0, 101, bbox2, 0) == true);

        REQUIRE(writer.GetBlobCount("HT", 0) == 2);

        BoundingBox bbox3;
        bbox3.SetOffset(0, 0, 0);
        bbox3.SetSize(10, 10, 10);
        REQUIRE(writer.WriteBlob("HT", 1, 0, bbox3, 0) == true);

        REQUIRE(writer.WriteBlob("FL/CH0", 0, 0, bbox3, 0) == true);

        TCMaskReader reader(fname);

        auto groups = reader.GetDataGroups();
        REQUIRE(groups.contains("HT") == true);
        REQUIRE(groups.contains("FL/CH0") == true);

        REQUIRE(reader.GetTimeIndexCount("HT") == 2);

        REQUIRE(reader.GetBlobCount("HT", 0) == 2);

        auto ret = reader.ReadBlob("HT", 0, 0);
        REQUIRE(std::get<0>(ret) == 100);
        REQUIRE(std::get<1>(ret) == bbox1);

        ret = reader.ReadBlob("HT", 0, 1);
        REQUIRE(std::get<0>(ret) == 101);
        REQUIRE(std::get<1>(ret) == bbox2);

        ret = reader.ReadBlob("FL/CH0", 0, 0);
        REQUIRE(std::get<0>(ret) == 0);
        REQUIRE(std::get<1>(ret) == bbox3);
    }
}