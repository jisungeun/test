#include <QFileInfo>
#pragma warning(push)
#pragma warning(disable : 4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#pragma warning(pop)

#include "TCMaskWriter.h"

#include <QDir>

namespace TC::IO {
    struct TCMaskWriter::Impl {
        QString path;
        uint32_t maxCounts = 100;
        const uint32_t columns = 8;

        auto OpenGroup(H5::H5File& file, const QString strName)->H5::Group {
            H5::Group group;
            try{
            if(file.exists(strName.toStdString())) {
                group = file.openGroup(strName.toStdString());
            }else {
                group = file.createGroup(strName.toStdString());
            }
            }catch (H5::Exception&) {
                
            }
            return group;
        }

        auto OpenGroup(H5::Group& parentGroup, const QString strName)->H5::Group {
            H5::Group group;
            try {
                if(parentGroup.exists(strName.toStdString())) {
                    group = parentGroup.openGroup(strName.toStdString());
                }else {
                    group = parentGroup.createGroup(strName.toStdString());
                }
            }
            catch (H5::Exception&) {                
            }
            return group;
        }                

        auto CreateGroupTree(H5::Group& group, const QString& dataID)->H5::Group {
            H5::Group blobGroup = group;
            QStringList names = dataID.split("/");
            for (auto name : names) {
                blobGroup = OpenGroup(blobGroup, name);
            }
            return blobGroup;
        }

        auto OpenOrCreateBlobGroup(H5::H5File& file, const QString dataID)->H5::Group {
            H5::Group group = OpenGroup(file, "Blobs");
            return CreateGroupTree(group, dataID);
        }

        auto OpenOrCreateBlobDataSet(H5::Group& group, int timeIndex)->H5::DataSet {
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            H5::DataSet dataSet;
            try {
                if(group.exists(name.toStdString())) {
                    dataSet = group.openDataSet(name.toStdString());
                }else {
                    hsize_t dims[2]{ maxCounts, columns };
                    auto space = H5::DataSpace(2, dims);

                    H5::DSetCreatPropList cparams;
                    int32_t fillValue = 0;
                    cparams.setFillValue(H5::PredType::NATIVE_INT32, &fillValue);

                    dataSet = group.createDataSet(name.toStdString(), H5::PredType::NATIVE_INT32, space, cparams);
                }
            } catch(H5::Exception&) {                
            }

            return dataSet;
        }

        auto GetBlobCount(H5::DataSet data)->uint32_t {
            uint32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            } else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = data.createAttribute("Count", H5::PredType::NATIVE_INT32, space);

                int32_t ddata[1]{ 0 };
                attr.write(H5::PredType::NATIVE_INT32, ddata);
                attr.close();
            }
            return count;
        }
        auto GetBlobName(H5::Group group,int nameIdx)->QString {
            std::string attrName = "organ_name" + std::to_string(nameIdx);
            H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
            if(group.attrExists(attrName)) {
                H5std_string strreadbuf("");
                auto attr_out = group.openAttribute(attrName);
                attr_out.read(strdatatype, strreadbuf);
                return QString(strreadbuf.c_str());
            }else {
                return QString();
            }
        }
        auto SetBlobName(H5::Group group,int nameIdx,const QString& name) ->void{
            std::string attrName = "organ_name" + std::to_string(nameIdx);
            H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
            if(group.attrExists(attrName)) {
                auto attr = group.openAttribute(attrName);
                attr.write(strdatatype, name.toStdString());
                attr.close();
            }else {
                H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
                // Create new string datatype for attribute                
                auto attr = group.createAttribute(attrName, strdatatype, attr_dataspace);
                attr.write(strdatatype, name.toStdString());
                attr.close();
            }
        }
        auto SetBlobCount(H5::DataSet dataset, int count)->void {
            int32_t data[1]{ count };
            auto attr = dataset.openAttribute("Count");
            attr.write(H5::PredType::NATIVE_INT32, data);
            attr.close();
        }
        auto WriteResolution(H5::H5File& file,double res,int index)->void {
            double value[1] {res};
            std::string attrName;
            if(index == 0) attrName = "ResX";
            else if(index == 1) attrName = "ResY";
            else if(index == 2) attrName = "ResZ";

            H5::Attribute attr;
            try {
                if(file.attrExists(attrName)) {
                    attr = file.openAttribute(attrName);
                }else {
                    hsize_t dims[1]{ 1 };
                    auto space = H5::DataSpace(1, dims);
                    attr = file.createAttribute(attrName, H5::PredType::NATIVE_DOUBLE,space);
                }                
            }catch(H5::Exception&) {
                
            }
            attr.write(H5::PredType::NATIVE_DOUBLE,value);
            attr.close();
        }
        auto WriteFullSize(H5::H5File& file, int32_t size, int index)->void {
            int32_t value[1]{ size };
            std::string attrName;
            if (index == 0) attrName = "SizeX";
            else if (index == 1) attrName = "SizeY";
            else if (index == 2) attrName = "SizeZ";
            H5::Attribute attr;
            try {
                if(file.attrExists(attrName)) {
                    attr = file.openAttribute(attrName);
                }else {
                    hsize_t dims[1]{ 1 };
                    auto space = H5::DataSpace(1, dims);
                    attr = file.createAttribute(attrName, H5::PredType::NATIVE_INT32,space);
                }
            }catch(H5::Exception&) {                
            }
            attr.write(H5::PredType::NATIVE_INT32, value);
            attr.close();
        }

        auto OpenOrCreateMaskGroup(H5::H5File& file, const QString dataID)->H5::Group {
            H5::Group group = OpenGroup(file, "Masks");
            return CreateGroupTree(group, dataID);
        }
        auto ForceCreateMaskDataSet(H5::Group& group,int timeIndex,int blobKey,hsize_t dims[3])->H5::DataSet {
            const QString groupName = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            const QString dataName = QString("%1").arg(blobKey, 6, 10, QLatin1Char('0'));

            auto dataGroup = OpenGroup(group, groupName);

            H5::DataSet dataSet;
            try {
                if (dataGroup.exists(dataName.toStdString())) {                    
                    std::string channelName = dataName.toStdString();
                    H5Ldelete(dataGroup.getId(), channelName.c_str(), H5P_DEFAULT);
                }
                auto space = H5::DataSpace(3, dims);

                H5::DSetCreatPropList cparams;
                int32_t fillValue = 0;
                cparams.setFillValue(H5::PredType::NATIVE_UINT8, &fillValue);
                                
                dataSet = dataGroup.createDataSet(dataName.toStdString(), H5::PredType::NATIVE_UINT8, space, cparams);
            }
            catch (H5::Exception&) {
            }
            return dataSet;

        }
        auto OpenOrCreateMaskDataSet(H5::Group& group, int timeIndex, int blobKey, hsize_t dims[3])->H5::DataSet {
            const QString groupName = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            const QString dataName = QString("%1").arg(blobKey, 6, 10, QLatin1Char('0'));

            auto dataGroup = OpenGroup(group, groupName);

            H5::DataSet dataSet;
            try {
                if(dataGroup.exists(dataName.toStdString())){
                    dataSet = dataGroup.openDataSet(dataName.toStdString());
                }else {
                    auto space = H5::DataSpace(3, dims);

                    H5::DSetCreatPropList cparams;
                    int32_t fillValue = 0;
                    cparams.setFillValue(H5::PredType::NATIVE_UINT8, &fillValue);

                    dataSet = dataGroup.createDataSet(dataName.toStdString(), H5::PredType::NATIVE_UINT8, space, cparams);
                }
            } catch (H5::Exception& ) {                
            }

            return dataSet;
        }
    };

    TCMaskWriter::TCMaskWriter(const QString& path, uint32_t maxCounts) :d{ new Impl } {
        d->path = path;
        d->maxCounts = maxCounts;                        
        QFileInfo info(d->path);        
        auto dir = info.dir();
        if (!dir.exists()) {            
            dir.mkpath(info.dir().path());
        }
        if(!QFileInfo::exists(d->path)) {
            HDF5MutexLocker lock(HDF5Mutex::GetInstance());            
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_CREAT|H5F_ACC_RDWR);
            file.close();
        }
    }

    TCMaskWriter::~TCMaskWriter() {
    }
    auto TCMaskWriter::WriteResolution(double resX, double resY, double resZ) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            d->WriteResolution(file, resX, 0);
            d->WriteResolution(file, resY, 1);
            d->WriteResolution(file, resZ, 2);
            file.close();
        }catch(H5::Exception&) {
            return false;
        }
        return true;
    }

    auto TCMaskWriter::WriteSize(int32_t sizeX, int32_t sizeY, int32_t sizeZ) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            d->WriteFullSize(file, sizeX, 0);
            d->WriteFullSize(file, sizeY, 1);
            d->WriteFullSize(file, sizeZ, 2);
            file.close();
        }catch(H5::Exception&) {
            return false;
        }
        return true;
    }        
    auto TCMaskWriter::ClearBlobCount(const QString& dataID, int timeIndex) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);            

            auto blobGroup = d->OpenGroup(file, "Blobs");
            auto group = d->OpenGroup(blobGroup, dataID);

            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            if(group.exists(name.toStdString())){
                auto dataSet = group.openDataSet(name.toStdString());
                if(dataSet.attrExists("Count")) {
                    int32_t data[1]{ 0 };
                    auto attr = dataSet.openAttribute("Count");
                    attr.write(H5::PredType::NATIVE_INT32, data);
                    attr.close();
                }else {
                    return false;
                }
            }else {
                return false;
            }
            group.close();
            blobGroup.close();
            file.close();
        }        
        catch (H5::Exception& /*ex*/) {
            return false;
        }
        return false;
    }
    auto TCMaskWriter::WriteName(const QString& dataID, int nameKey, const QString& name)->bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto group = d->OpenOrCreateBlobGroup(file, dataID);
            d->SetBlobName(group, nameKey, name);
            group.close();
            file.close();
        }
        catch (H5::Exception& /*ex*/) {
            return false;
        }
        return true;
    }
    auto TCMaskWriter::WriteBlob(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, int code) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);

            auto group = d->OpenOrCreateBlobGroup(file, dataID);


            auto dataSet = d->OpenOrCreateBlobDataSet(group, timeIndex);
            auto blobs = d->GetBlobCount(dataSet);

            auto fileSpace = dataSet.getSpace();
            hsize_t size[2]{ 1, d->columns };
            hsize_t offset[2]{ blobs, 0 };
            fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

            auto memSpace = H5::DataSpace(2, size);

            auto bBoxOffset = bBox.GetOffset();
            auto bBoxSize = bBox.GetSize();
            int data[8]{ blobKey, bBoxOffset.x0, bBoxOffset.y0, bBoxOffset.z0,
                static_cast<int>(bBoxSize.d0),
                static_cast<int>(bBoxSize.d1),
                static_cast<int>(bBoxSize.d2),
                code };
            dataSet.write(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);            
            d->SetBlobCount(dataSet, blobs + 1);
            dataSet.close();
            file.close();
        } catch (H5::Exception& /*ex*/) {
            return false;
        }

        return true;
    }

    auto TCMaskWriter::UpdateBlob(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox,
        int code) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        if(!QFileInfo::exists(d->path))
            return false;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);

            auto group = d->OpenOrCreateBlobGroup(file, dataID);

            auto dataSet = d->OpenOrCreateBlobDataSet(group, timeIndex);

            auto fileSpace = dataSet.getSpace();
            hsize_t size[2]{ 1, d->columns };
            hsize_t offset[2]{ static_cast<hsize_t>(blobKey), 0 };
            fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

            auto memSpace = H5::DataSpace(2, size);

            auto bBoxOffset = bBox.GetOffset();
            auto bBoxSize = bBox.GetSize();
            int data[8]{ blobKey, bBoxOffset.x0, bBoxOffset.y0, bBoxOffset.z0,
                static_cast<int>(bBoxSize.d0),
                static_cast<int>(bBoxSize.d1),
                static_cast<int>(bBoxSize.d2),
                code };
            dataSet.write(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);

            dataSet.close();
            group.close();
            file.close();
        }
        catch (H5::Exception& /*ex*/) {
            return false;
        }

        return true;
    }
    auto TCMaskWriter::ClearWhole(const QString& dataID, int timeIndex) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {            
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);            
            auto group = d->OpenOrCreateMaskGroup(file, dataID);            
            {
                const QString groupName = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
                if(group.exists(groupName.toStdString())) {
                    std::string gname = groupName.toStdString();
                    auto grp = group.openGroup(gname);                    
                    auto obj_cnt = grp.getNumObjs();
                    for(auto i=0;i<obj_cnt;i++) {                        
                        const QString dataName = QString("%1").arg(i, 6, 10, QLatin1Char('0'));                        
                        H5Ldelete(grp.getId(), dataName.toStdString().c_str(), H5P_DEFAULT);
                    }
                    H5Ldelete(group.getId(), gname.c_str(), H5P_DEFAULT);
                }                
            }
            group.close();
            file.close();            
        }
        catch (H5::Exception& /*ex*/) {
            return false;
        }

        return true;
    }
    auto TCMaskWriter::UpdateMask(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t> mask) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);

            auto group = d->OpenOrCreateMaskGroup(file, dataID);
            {
                const auto bBoxSize = bBox.GetSize();

                hsize_t size[3]{ bBoxSize.d2, bBoxSize.d1, bBoxSize.d0 };
                //auto dataSet = d->OpenOrCreateMaskDataSet(group, timeIndex, blobKey, size);
                auto dataSet = d->ForceCreateMaskDataSet(group, timeIndex, blobKey, size);

                auto dataSpace = H5::DataSpace(3, size);

                hsize_t sss[3];                
                dataSet.getSpace().getSimpleExtentDims(sss);
                dataSet.write(mask.get(), H5::PredType::NATIVE_UINT8, dataSpace);
                dataSet.close();
            }
            group.close();
            file.close();
        }
        catch (H5::Exception& /*ex*/) {
            return false;
        }

        return true;
    }

    auto TCMaskWriter::WriteMask(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox,
        std::shared_ptr<uint8_t> mask) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);

            auto group = d->OpenOrCreateMaskGroup(file, dataID);
            {
                const auto bBoxSize = bBox.GetSize();

                hsize_t size[3]{ bBoxSize.d2, bBoxSize.d1, bBoxSize.d0 };
                auto dataSet = d->OpenOrCreateMaskDataSet(group, timeIndex, blobKey, size);

                auto dataSpace = H5::DataSpace(3, size);

                hsize_t sss[3];
                dataSet.getSpace().getSimpleExtentDims(sss);
                dataSet.write(mask.get(), H5::PredType::NATIVE_UINT8, dataSpace);
                dataSet.close();
            }
            group.close();
            file.close();
        } catch (H5::Exception& /*ex*/) {
            return false;
        }

        return true;
    }

    auto TCMaskWriter::GetBlobCount(const QString& dataID, int timeIndex) -> int32_t {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t count = 0;
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = file.openGroup(QString("Blobs/%1").arg(dataID).toStdString());
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            auto dataSet = group.openDataSet(name.toStdString());
            count = d->GetBlobCount(dataSet);

            dataSet.close();
            group.close();
            file.close();
        } catch (H5::Exception& /*ex*/) {
        }
        return count;
    }
}
